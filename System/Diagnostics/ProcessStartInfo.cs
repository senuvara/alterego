﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;

namespace System.Diagnostics
{
	/// <summary>Specifies a set of values that are used when you start a process.</summary>
	// Token: 0x0200010D RID: 269
	[TypeConverter(typeof(ExpandableObjectConverter))]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true, SelfAffectingProcessMgmt = true)]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class ProcessStartInfo
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.ProcessStartInfo" /> class without specifying a file name with which to start the process.</summary>
		// Token: 0x0600087B RID: 2171 RVA: 0x00029404 File Offset: 0x00027604
		public ProcessStartInfo()
		{
		}

		// Token: 0x0600087C RID: 2172 RVA: 0x00029413 File Offset: 0x00027613
		internal ProcessStartInfo(Process parent)
		{
			this.weakParentProcess = new WeakReference(parent);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.ProcessStartInfo" /> class and specifies a file name such as an application or document with which to start the process.</summary>
		/// <param name="fileName">An application or document with which to start a process. </param>
		// Token: 0x0600087D RID: 2173 RVA: 0x0002942E File Offset: 0x0002762E
		public ProcessStartInfo(string fileName)
		{
			this.fileName = fileName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.ProcessStartInfo" /> class, specifies an application file name with which to start the process, and specifies a set of command-line arguments to pass to the application.</summary>
		/// <param name="fileName">An application with which to start a process. </param>
		/// <param name="arguments">Command-line arguments to pass to the application when the process starts. </param>
		// Token: 0x0600087E RID: 2174 RVA: 0x00029444 File Offset: 0x00027644
		public ProcessStartInfo(string fileName, string arguments)
		{
			this.fileName = fileName;
			this.arguments = arguments;
		}

		/// <summary>Gets or sets the verb to use when opening the application or document specified by the <see cref="P:System.Diagnostics.ProcessStartInfo.FileName" /> property.</summary>
		/// <returns>The action to take with the file that the process opens. The default is an empty string (""), which signifies no action.</returns>
		// Token: 0x17000192 RID: 402
		// (get) Token: 0x0600087F RID: 2175 RVA: 0x00029461 File Offset: 0x00027661
		// (set) Token: 0x06000880 RID: 2176 RVA: 0x00029477 File Offset: 0x00027677
		[NotifyParentProperty(true)]
		[TypeConverter("System.Diagnostics.Design.VerbConverter, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		[DefaultValue("")]
		[MonitoringDescription("The verb to apply to the document specified by the FileName property.")]
		public string Verb
		{
			get
			{
				if (this.verb == null)
				{
					return string.Empty;
				}
				return this.verb;
			}
			set
			{
				this.verb = value;
			}
		}

		/// <summary>Gets or sets the set of command-line arguments to use when starting the application.</summary>
		/// <returns>A single string containing the arguments to pass to the target application specified in the <see cref="P:System.Diagnostics.ProcessStartInfo.FileName" /> property. The default is an empty string (""). On Windows Vista and earlier versions of the Windows operating system, the length of the arguments added to the length of the full path to the process must be less than 2080. On Windows 7 and later versions, the length must be less than 32699.Arguments are parsed and interpreted by the target application, so must align with the expectations of that application. For.NET applications as demonstrated in the Examples below, spaces are interpreted as a separator between multiple arguments. A single argument that includes spaces must be surrounded by quotation marks, but those quotation marks are not carried through to the target application. In include quotation marks in the final parsed argument, triple-escape each mark.</returns>
		// Token: 0x17000193 RID: 403
		// (get) Token: 0x06000881 RID: 2177 RVA: 0x00029480 File Offset: 0x00027680
		// (set) Token: 0x06000882 RID: 2178 RVA: 0x00029496 File Offset: 0x00027696
		[DefaultValue("")]
		[MonitoringDescription("Command line arguments that will be passed to the application specified by the FileName property.")]
		[SettingsBindable(true)]
		[TypeConverter("System.Diagnostics.Design.StringValueConverter, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		[NotifyParentProperty(true)]
		public string Arguments
		{
			get
			{
				if (this.arguments == null)
				{
					return string.Empty;
				}
				return this.arguments;
			}
			set
			{
				this.arguments = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to start the process in a new window.</summary>
		/// <returns>
		///     <see langword="true" /> if the process should be started without creating a new window to contain it; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000194 RID: 404
		// (get) Token: 0x06000883 RID: 2179 RVA: 0x0002949F File Offset: 0x0002769F
		// (set) Token: 0x06000884 RID: 2180 RVA: 0x000294A7 File Offset: 0x000276A7
		[DefaultValue(false)]
		[MonitoringDescription("Whether to start the process without creating a new window to contain it.")]
		[NotifyParentProperty(true)]
		public bool CreateNoWindow
		{
			get
			{
				return this.createNoWindow;
			}
			set
			{
				this.createNoWindow = value;
			}
		}

		/// <summary>Gets search paths for files, directories for temporary files, application-specific options, and other similar information.</summary>
		/// <returns>A string dictionary that provides environment variables that apply to this process and child processes. The default is <see langword="null" />.</returns>
		// Token: 0x17000195 RID: 405
		// (get) Token: 0x06000885 RID: 2181 RVA: 0x000294B0 File Offset: 0x000276B0
		[NotifyParentProperty(true)]
		[MonitoringDescription("Set of environment variables that apply to this process and child processes.")]
		[DefaultValue(null)]
		[Editor("System.Diagnostics.Design.StringDictionaryEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public StringDictionary EnvironmentVariables
		{
			get
			{
				if (this.environmentVariables == null)
				{
					this.environmentVariables = new CaseSensitiveStringDictionary();
					if (this.weakParentProcess == null || !this.weakParentProcess.IsAlive || ((Component)this.weakParentProcess.Target).Site == null || !((Component)this.weakParentProcess.Target).Site.DesignMode)
					{
						foreach (object obj in System.Environment.GetEnvironmentVariables())
						{
							DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
							this.environmentVariables.Add((string)dictionaryEntry.Key, (string)dictionaryEntry.Value);
						}
					}
				}
				return this.environmentVariables;
			}
		}

		/// <summary>Gets the environment variables that apply to this process and its child processes.</summary>
		/// <returns>A generic dictionary containing the environment variables that apply to this process and its child processes. The default is <see langword="null" />.</returns>
		// Token: 0x17000196 RID: 406
		// (get) Token: 0x06000886 RID: 2182 RVA: 0x00029588 File Offset: 0x00027788
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DefaultValue(null)]
		[NotifyParentProperty(true)]
		public IDictionary<string, string> Environment
		{
			get
			{
				if (this.environment == null)
				{
					this.environment = this.EnvironmentVariables.AsGenericDictionary();
				}
				return this.environment;
			}
		}

		/// <summary>Gets or sets a value indicating whether the input for an application is read from the <see cref="P:System.Diagnostics.Process.StandardInput" /> stream.</summary>
		/// <returns>
		///     <see langword="true" /> if input should be read from <see cref="P:System.Diagnostics.Process.StandardInput" />; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000197 RID: 407
		// (get) Token: 0x06000887 RID: 2183 RVA: 0x000295A9 File Offset: 0x000277A9
		// (set) Token: 0x06000888 RID: 2184 RVA: 0x000295B1 File Offset: 0x000277B1
		[MonitoringDescription("Whether the process command input is read from the Process instance's StandardInput member.")]
		[DefaultValue(false)]
		[NotifyParentProperty(true)]
		public bool RedirectStandardInput
		{
			get
			{
				return this.redirectStandardInput;
			}
			set
			{
				this.redirectStandardInput = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the textual output of an application is written to the <see cref="P:System.Diagnostics.Process.StandardOutput" /> stream.</summary>
		/// <returns>
		///     <see langword="true" /> if output should be written to <see cref="P:System.Diagnostics.Process.StandardOutput" />; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000198 RID: 408
		// (get) Token: 0x06000889 RID: 2185 RVA: 0x000295BA File Offset: 0x000277BA
		// (set) Token: 0x0600088A RID: 2186 RVA: 0x000295C2 File Offset: 0x000277C2
		[MonitoringDescription("Whether the process output is written to the Process instance's StandardOutput member.")]
		[NotifyParentProperty(true)]
		[DefaultValue(false)]
		public bool RedirectStandardOutput
		{
			get
			{
				return this.redirectStandardOutput;
			}
			set
			{
				this.redirectStandardOutput = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the error output of an application is written to the <see cref="P:System.Diagnostics.Process.StandardError" /> stream.</summary>
		/// <returns>
		///     <see langword="true" /> if error output should be written to <see cref="P:System.Diagnostics.Process.StandardError" />; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000199 RID: 409
		// (get) Token: 0x0600088B RID: 2187 RVA: 0x000295CB File Offset: 0x000277CB
		// (set) Token: 0x0600088C RID: 2188 RVA: 0x000295D3 File Offset: 0x000277D3
		[NotifyParentProperty(true)]
		[DefaultValue(false)]
		[MonitoringDescription("Whether the process's error output is written to the Process instance's StandardError member.")]
		public bool RedirectStandardError
		{
			get
			{
				return this.redirectStandardError;
			}
			set
			{
				this.redirectStandardError = value;
			}
		}

		/// <summary>Gets or sets the preferred encoding for error output.</summary>
		/// <returns>An object that represents the preferred encoding for error output. The default is <see langword="null" />.</returns>
		// Token: 0x1700019A RID: 410
		// (get) Token: 0x0600088D RID: 2189 RVA: 0x000295DC File Offset: 0x000277DC
		// (set) Token: 0x0600088E RID: 2190 RVA: 0x000295E4 File Offset: 0x000277E4
		public Encoding StandardErrorEncoding
		{
			get
			{
				return this.standardErrorEncoding;
			}
			set
			{
				this.standardErrorEncoding = value;
			}
		}

		/// <summary>Gets or sets the preferred encoding for standard output.</summary>
		/// <returns>An object that represents the preferred encoding for standard output. The default is <see langword="null" />.</returns>
		// Token: 0x1700019B RID: 411
		// (get) Token: 0x0600088F RID: 2191 RVA: 0x000295ED File Offset: 0x000277ED
		// (set) Token: 0x06000890 RID: 2192 RVA: 0x000295F5 File Offset: 0x000277F5
		public Encoding StandardOutputEncoding
		{
			get
			{
				return this.standardOutputEncoding;
			}
			set
			{
				this.standardOutputEncoding = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to use the operating system shell to start the process.</summary>
		/// <returns>
		///     <see langword="true" /> if the shell should be used when starting the process; <see langword="false" /> if the process should be created directly from the executable file. The default is <see langword="true" />.</returns>
		// Token: 0x1700019C RID: 412
		// (get) Token: 0x06000891 RID: 2193 RVA: 0x000295FE File Offset: 0x000277FE
		// (set) Token: 0x06000892 RID: 2194 RVA: 0x00029606 File Offset: 0x00027806
		[DefaultValue(true)]
		[NotifyParentProperty(true)]
		[MonitoringDescription("Whether to use the operating system shell to start the process.")]
		public bool UseShellExecute
		{
			get
			{
				return this.useShellExecute;
			}
			set
			{
				this.useShellExecute = value;
			}
		}

		/// <summary>Gets or sets the user name to be used when starting the process.</summary>
		/// <returns>The user name to use when starting the process.</returns>
		// Token: 0x1700019D RID: 413
		// (get) Token: 0x06000893 RID: 2195 RVA: 0x0002960F File Offset: 0x0002780F
		// (set) Token: 0x06000894 RID: 2196 RVA: 0x00029625 File Offset: 0x00027825
		[NotifyParentProperty(true)]
		public string UserName
		{
			get
			{
				if (this.userName == null)
				{
					return string.Empty;
				}
				return this.userName;
			}
			set
			{
				this.userName = value;
			}
		}

		/// <summary>Gets or sets a secure string that contains the user password to use when starting the process.</summary>
		/// <returns>The user password to use when starting the process.</returns>
		// Token: 0x1700019E RID: 414
		// (get) Token: 0x06000895 RID: 2197 RVA: 0x0002962E File Offset: 0x0002782E
		// (set) Token: 0x06000896 RID: 2198 RVA: 0x00029636 File Offset: 0x00027836
		public SecureString Password
		{
			get
			{
				return this.password;
			}
			set
			{
				this.password = value;
			}
		}

		/// <summary>Gets or sets the user password in clear text to use when starting the process.</summary>
		/// <returns>The user password in clear text.</returns>
		// Token: 0x1700019F RID: 415
		// (get) Token: 0x06000897 RID: 2199 RVA: 0x0002963F File Offset: 0x0002783F
		// (set) Token: 0x06000898 RID: 2200 RVA: 0x00029647 File Offset: 0x00027847
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public string PasswordInClearText
		{
			get
			{
				return this.passwordInClearText;
			}
			set
			{
				this.passwordInClearText = value;
			}
		}

		/// <summary>Gets or sets a value that identifies the domain to use when starting the process. </summary>
		/// <returns>The Active Directory domain to use when starting the process. The domain property is primarily of interest to users within enterprise environments that use Active Directory.</returns>
		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x06000899 RID: 2201 RVA: 0x00029650 File Offset: 0x00027850
		// (set) Token: 0x0600089A RID: 2202 RVA: 0x00029666 File Offset: 0x00027866
		[NotifyParentProperty(true)]
		public string Domain
		{
			get
			{
				if (this.domain == null)
				{
					return string.Empty;
				}
				return this.domain;
			}
			set
			{
				this.domain = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the Windows user profile is to be loaded from the registry. </summary>
		/// <returns>
		///     <see langword="true" /> if the Windows user profile should be loaded; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x0600089B RID: 2203 RVA: 0x0002966F File Offset: 0x0002786F
		// (set) Token: 0x0600089C RID: 2204 RVA: 0x00029677 File Offset: 0x00027877
		[NotifyParentProperty(true)]
		public bool LoadUserProfile
		{
			get
			{
				return this.loadUserProfile;
			}
			set
			{
				this.loadUserProfile = value;
			}
		}

		/// <summary>Gets or sets the application or document to start.</summary>
		/// <returns>The name of the application to start, or the name of a document of a file type that is associated with an application and that has a default open action available to it. The default is an empty string ("").</returns>
		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x0600089D RID: 2205 RVA: 0x00029680 File Offset: 0x00027880
		// (set) Token: 0x0600089E RID: 2206 RVA: 0x00029696 File Offset: 0x00027896
		[NotifyParentProperty(true)]
		[SettingsBindable(true)]
		[MonitoringDescription("The name of the application, document or URL to start.")]
		[Editor("System.Diagnostics.Design.StartFileNameEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		[DefaultValue("")]
		[TypeConverter("System.Diagnostics.Design.StringValueConverter, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		public string FileName
		{
			get
			{
				if (this.fileName == null)
				{
					return string.Empty;
				}
				return this.fileName;
			}
			set
			{
				this.fileName = value;
			}
		}

		/// <summary>When the <see cref="P:System.Diagnostics.ProcessStartInfo.UseShellExecute" /> property is <see langword="false" />, gets or sets the working directory for the process to be started. When <see cref="P:System.Diagnostics.ProcessStartInfo.UseShellExecute" /> is <see langword="true" />, gets or sets the directory that contains the process to be started.</summary>
		/// <returns>When <see cref="P:System.Diagnostics.ProcessStartInfo.UseShellExecute" /> is <see langword="true" />, the fully qualified name of the directory that contains the process to be started. When the <see cref="P:System.Diagnostics.ProcessStartInfo.UseShellExecute" /> property is <see langword="false" />, the working directory for the process to be started. The default is an empty string ("").</returns>
		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x0600089F RID: 2207 RVA: 0x0002969F File Offset: 0x0002789F
		// (set) Token: 0x060008A0 RID: 2208 RVA: 0x000296B5 File Offset: 0x000278B5
		[MonitoringDescription("The initial working directory for the process.")]
		[Editor("System.Diagnostics.Design.WorkingDirectoryEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		[NotifyParentProperty(true)]
		[TypeConverter("System.Diagnostics.Design.StringValueConverter, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		[SettingsBindable(true)]
		[DefaultValue("")]
		public string WorkingDirectory
		{
			get
			{
				if (this.directory == null)
				{
					return string.Empty;
				}
				return this.directory;
			}
			set
			{
				this.directory = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether an error dialog box is displayed to the user if the process cannot be started.</summary>
		/// <returns>
		///     <see langword="true" /> if an error dialog box should be displayed on the screen if the process cannot be started; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x060008A1 RID: 2209 RVA: 0x000296BE File Offset: 0x000278BE
		// (set) Token: 0x060008A2 RID: 2210 RVA: 0x000296C6 File Offset: 0x000278C6
		[MonitoringDescription("Whether to show an error dialog to the user if there is an error.")]
		[DefaultValue(false)]
		[NotifyParentProperty(true)]
		public bool ErrorDialog
		{
			get
			{
				return this.errorDialog;
			}
			set
			{
				this.errorDialog = value;
			}
		}

		/// <summary>Gets or sets the window handle to use when an error dialog box is shown for a process that cannot be started.</summary>
		/// <returns>A pointer to the handle of the error dialog box that results from a process start failure.</returns>
		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x060008A3 RID: 2211 RVA: 0x000296CF File Offset: 0x000278CF
		// (set) Token: 0x060008A4 RID: 2212 RVA: 0x000296D7 File Offset: 0x000278D7
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public IntPtr ErrorDialogParentHandle
		{
			get
			{
				return this.errorDialogParentHandle;
			}
			set
			{
				this.errorDialogParentHandle = value;
			}
		}

		/// <summary>Gets or sets the window state to use when the process is started.</summary>
		/// <returns>One of the enumeration values that indicates whether the process is started in a window that is maximized, minimized, normal (neither maximized nor minimized), or not visible. The default is <see langword="Normal" />.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">The window style is not one of the <see cref="T:System.Diagnostics.ProcessWindowStyle" /> enumeration members. </exception>
		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x060008A5 RID: 2213 RVA: 0x000296E0 File Offset: 0x000278E0
		// (set) Token: 0x060008A6 RID: 2214 RVA: 0x000296E8 File Offset: 0x000278E8
		[DefaultValue(ProcessWindowStyle.Normal)]
		[MonitoringDescription("How the main window should be created when the process starts.")]
		[NotifyParentProperty(true)]
		public ProcessWindowStyle WindowStyle
		{
			get
			{
				return this.windowStyle;
			}
			set
			{
				if (!Enum.IsDefined(typeof(ProcessWindowStyle), value))
				{
					throw new InvalidEnumArgumentException("value", (int)value, typeof(ProcessWindowStyle));
				}
				this.windowStyle = value;
			}
		}

		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x060008A7 RID: 2215 RVA: 0x0002971E File Offset: 0x0002791E
		internal bool HaveEnvVars
		{
			get
			{
				return this.environmentVariables != null;
			}
		}

		/// <summary>Gets the set of verbs associated with the type of file specified by the <see cref="P:System.Diagnostics.ProcessStartInfo.FileName" /> property.</summary>
		/// <returns>The actions that the system can apply to the file indicated by the <see cref="P:System.Diagnostics.ProcessStartInfo.FileName" /> property.</returns>
		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x060008A8 RID: 2216 RVA: 0x00029729 File Offset: 0x00027929
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public string[] Verbs
		{
			get
			{
				return ProcessStartInfo.empty;
			}
		}

		// Token: 0x060008A9 RID: 2217 RVA: 0x00029730 File Offset: 0x00027930
		// Note: this type is marked as 'beforefieldinit'.
		static ProcessStartInfo()
		{
		}

		// Token: 0x04000BB3 RID: 2995
		private string fileName;

		// Token: 0x04000BB4 RID: 2996
		private string arguments;

		// Token: 0x04000BB5 RID: 2997
		private string directory;

		// Token: 0x04000BB6 RID: 2998
		private string verb;

		// Token: 0x04000BB7 RID: 2999
		private ProcessWindowStyle windowStyle;

		// Token: 0x04000BB8 RID: 3000
		private bool errorDialog;

		// Token: 0x04000BB9 RID: 3001
		private IntPtr errorDialogParentHandle;

		// Token: 0x04000BBA RID: 3002
		private bool useShellExecute = true;

		// Token: 0x04000BBB RID: 3003
		private string userName;

		// Token: 0x04000BBC RID: 3004
		private string domain;

		// Token: 0x04000BBD RID: 3005
		private SecureString password;

		// Token: 0x04000BBE RID: 3006
		private string passwordInClearText;

		// Token: 0x04000BBF RID: 3007
		private bool loadUserProfile;

		// Token: 0x04000BC0 RID: 3008
		private bool redirectStandardInput;

		// Token: 0x04000BC1 RID: 3009
		private bool redirectStandardOutput;

		// Token: 0x04000BC2 RID: 3010
		private bool redirectStandardError;

		// Token: 0x04000BC3 RID: 3011
		private Encoding standardOutputEncoding;

		// Token: 0x04000BC4 RID: 3012
		private Encoding standardErrorEncoding;

		// Token: 0x04000BC5 RID: 3013
		private bool createNoWindow;

		// Token: 0x04000BC6 RID: 3014
		private WeakReference weakParentProcess;

		// Token: 0x04000BC7 RID: 3015
		internal StringDictionary environmentVariables;

		// Token: 0x04000BC8 RID: 3016
		private IDictionary<string, string> environment;

		// Token: 0x04000BC9 RID: 3017
		private static readonly string[] empty = new string[0];
	}
}
