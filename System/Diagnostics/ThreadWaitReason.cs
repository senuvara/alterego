﻿using System;

namespace System.Diagnostics
{
	/// <summary>Specifies the reason a thread is waiting.</summary>
	// Token: 0x0200011D RID: 285
	public enum ThreadWaitReason
	{
		/// <summary>The thread is waiting for event pair high.</summary>
		// Token: 0x04000C16 RID: 3094
		EventPairHigh = 7,
		/// <summary>The thread is waiting for event pair low.</summary>
		// Token: 0x04000C17 RID: 3095
		EventPairLow,
		/// <summary>Thread execution is delayed.</summary>
		// Token: 0x04000C18 RID: 3096
		ExecutionDelay = 4,
		/// <summary>The thread is waiting for the scheduler.</summary>
		// Token: 0x04000C19 RID: 3097
		Executive = 0,
		/// <summary>The thread is waiting for a free virtual memory page.</summary>
		// Token: 0x04000C1A RID: 3098
		FreePage,
		/// <summary>The thread is waiting for a local procedure call to arrive.</summary>
		// Token: 0x04000C1B RID: 3099
		LpcReceive = 9,
		/// <summary>The thread is waiting for reply to a local procedure call to arrive.</summary>
		// Token: 0x04000C1C RID: 3100
		LpcReply,
		/// <summary>The thread is waiting for a virtual memory page to arrive in memory.</summary>
		// Token: 0x04000C1D RID: 3101
		PageIn = 2,
		/// <summary>The thread is waiting for a virtual memory page to be written to disk.</summary>
		// Token: 0x04000C1E RID: 3102
		PageOut = 12,
		/// <summary>Thread execution is suspended.</summary>
		// Token: 0x04000C1F RID: 3103
		Suspended = 5,
		/// <summary>The thread is waiting for system allocation.</summary>
		// Token: 0x04000C20 RID: 3104
		SystemAllocation = 3,
		/// <summary>The thread is waiting for an unknown reason.</summary>
		// Token: 0x04000C21 RID: 3105
		Unknown = 13,
		/// <summary>The thread is waiting for a user request.</summary>
		// Token: 0x04000C22 RID: 3106
		UserRequest = 6,
		/// <summary>The thread is waiting for the system to allocate virtual memory.</summary>
		// Token: 0x04000C23 RID: 3107
		VirtualMemory = 11
	}
}
