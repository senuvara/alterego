﻿using System;

namespace System.Diagnostics
{
	/// <summary>Identifies the level type for a switch. </summary>
	// Token: 0x020000F4 RID: 244
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class SwitchLevelAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.SwitchLevelAttribute" /> class, specifying the type that determines whether a trace should be written.</summary>
		/// <param name="switchLevelType">The <see cref="T:System.Type" /> that determines whether a trace should be written.</param>
		// Token: 0x060006EE RID: 1774 RVA: 0x000239B2 File Offset: 0x00021BB2
		public SwitchLevelAttribute(Type switchLevelType)
		{
			this.SwitchLevelType = switchLevelType;
		}

		/// <summary>Gets or sets the type that determines whether a trace should be written.</summary>
		/// <returns>The <see cref="T:System.Type" /> that determines whether a trace should be written.</returns>
		/// <exception cref="T:System.ArgumentNullException">The set operation failed because the value is <see langword="null" />.</exception>
		// Token: 0x17000127 RID: 295
		// (get) Token: 0x060006EF RID: 1775 RVA: 0x000239C1 File Offset: 0x00021BC1
		// (set) Token: 0x060006F0 RID: 1776 RVA: 0x000239C9 File Offset: 0x00021BC9
		public Type SwitchLevelType
		{
			get
			{
				return this.type;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.type = value;
			}
		}

		// Token: 0x04000B22 RID: 2850
		private Type type;
	}
}
