﻿using System;
using System.Collections;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Provides a strongly typed collection of <see cref="T:System.Diagnostics.InstanceData" /> objects.</summary>
	// Token: 0x02000670 RID: 1648
	public class InstanceDataCollection : DictionaryBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.InstanceDataCollection" /> class, using the specified performance counter (which defines a performance instance).</summary>
		/// <param name="counterName">The name of the counter, which often describes the quantity that is being counted. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="counterName" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06003495 RID: 13461 RVA: 0x000092E2 File Offset: 0x000074E2
		[Obsolete("This constructor has been deprecated.  Please use System.Diagnostics.InstanceDataCollectionCollection.get_Item to get an instance of this collection instead.  http://go.microsoft.com/fwlink/?linkid=14202")]
		public InstanceDataCollection(string counterName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the name of the performance counter whose instance data you want to get.</summary>
		/// <returns>The performance counter name.</returns>
		// Token: 0x17000D55 RID: 3413
		// (get) Token: 0x06003496 RID: 13462 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string CounterName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the instance data associated with this counter. This is typically a set of raw counter values.</summary>
		/// <param name="instanceName">The name of the performance counter category instance, or an empty string ("") if the category contains a single instance. </param>
		/// <returns>An <see cref="T:System.Diagnostics.InstanceData" /> item, by which the <see cref="T:System.Diagnostics.InstanceDataCollection" /> object is indexed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="instanceName" /> parameter is <see langword="null" />. </exception>
		// Token: 0x17000D56 RID: 3414
		public InstanceData this[string instanceName]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the object and counter registry keys for the objects associated with this instance data.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> that represents a set of object-specific registry keys.</returns>
		// Token: 0x17000D57 RID: 3415
		// (get) Token: 0x06003498 RID: 13464 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ICollection Keys
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the raw counter values that comprise the instance data for the counter.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> that represents the counter's raw data values.</returns>
		// Token: 0x17000D58 RID: 3416
		// (get) Token: 0x06003499 RID: 13465 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ICollection Values
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Determines whether a performance instance with a specified name (identified by one of the indexed <see cref="T:System.Diagnostics.InstanceData" /> objects) exists in the collection.</summary>
		/// <param name="instanceName">The name of the instance to find in this collection. </param>
		/// <returns>
		///     <see langword="true" /> if the instance exists in the collection; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="instanceName" /> parameter is <see langword="null" />. </exception>
		// Token: 0x0600349A RID: 13466 RVA: 0x000A56A8 File Offset: 0x000A38A8
		public bool Contains(string instanceName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the items in the collection to the specified one-dimensional array at the specified index.</summary>
		/// <param name="instances">The one-dimensional <see cref="T:System.Array" /> that is the destination of the values copied from the collection. </param>
		/// <param name="index">The zero-based index value at which to add the new instances. </param>
		// Token: 0x0600349B RID: 13467 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(InstanceData[] instances, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
