﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Directs tracing or debugging output to either the standard output or the standard error stream.</summary>
	// Token: 0x02000664 RID: 1636
	[HostProtection(SecurityAction.LinkDemand, Synchronization = true)]
	public class ConsoleTraceListener : TextWriterTraceListener
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.ConsoleTraceListener" /> class with trace output written to the standard output stream.</summary>
		// Token: 0x0600345A RID: 13402 RVA: 0x000092E2 File Offset: 0x000074E2
		public ConsoleTraceListener()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.ConsoleTraceListener" /> class with an option to write trace output to the standard output stream or the standard error stream.</summary>
		/// <param name="useErrorStream">
		///       <see langword="true" /> to write tracing and debugging output to the standard error stream; <see langword="false" /> to write tracing and debugging output to the standard output stream.</param>
		// Token: 0x0600345B RID: 13403 RVA: 0x000092E2 File Offset: 0x000074E2
		public ConsoleTraceListener(bool useErrorStream)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
