﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Threading;

namespace System.Diagnostics
{
	// Token: 0x02000103 RID: 259
	internal class AsyncStreamReader : IDisposable
	{
		// Token: 0x060007D8 RID: 2008 RVA: 0x00026D1D File Offset: 0x00024F1D
		internal AsyncStreamReader(Process process, Stream stream, UserCallBack callback, Encoding encoding) : this(process, stream, callback, encoding, 1024)
		{
		}

		// Token: 0x060007D9 RID: 2009 RVA: 0x00026D2F File Offset: 0x00024F2F
		internal AsyncStreamReader(Process process, Stream stream, UserCallBack callback, Encoding encoding, int bufferSize)
		{
			this.Init(process, stream, callback, encoding, bufferSize);
			this.messageQueue = new Queue();
		}

		// Token: 0x060007DA RID: 2010 RVA: 0x00026D5C File Offset: 0x00024F5C
		private void Init(Process process, Stream stream, UserCallBack callback, Encoding encoding, int bufferSize)
		{
			this.process = process;
			this.stream = stream;
			this.encoding = encoding;
			this.userCallBack = callback;
			this.decoder = encoding.GetDecoder();
			if (bufferSize < 128)
			{
				bufferSize = 128;
			}
			this.byteBuffer = new byte[bufferSize];
			this._maxCharsPerBuffer = encoding.GetMaxCharCount(bufferSize);
			this.charBuffer = new char[this._maxCharsPerBuffer];
			this.cancelOperation = false;
			this.eofEvent = new ManualResetEvent(false);
			this.sb = null;
			this.bLastCarriageReturn = false;
		}

		// Token: 0x060007DB RID: 2011 RVA: 0x00026DF1 File Offset: 0x00024FF1
		public virtual void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x060007DC RID: 2012 RVA: 0x00026DFA File Offset: 0x00024FFA
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060007DD RID: 2013 RVA: 0x00026E0C File Offset: 0x0002500C
		protected virtual void Dispose(bool disposing)
		{
			object obj = this.syncObject;
			lock (obj)
			{
				if (disposing && this.stream != null)
				{
					this.stream.Close();
				}
				if (this.stream != null)
				{
					this.stream = null;
					this.encoding = null;
					this.decoder = null;
					this.byteBuffer = null;
					this.charBuffer = null;
				}
				if (this.eofEvent != null)
				{
					this.eofEvent.Close();
					this.eofEvent = null;
				}
			}
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060007DE RID: 2014 RVA: 0x00026EA4 File Offset: 0x000250A4
		public virtual Encoding CurrentEncoding
		{
			get
			{
				return this.encoding;
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060007DF RID: 2015 RVA: 0x00026EAC File Offset: 0x000250AC
		public virtual Stream BaseStream
		{
			get
			{
				return this.stream;
			}
		}

		// Token: 0x060007E0 RID: 2016 RVA: 0x00026EB4 File Offset: 0x000250B4
		internal void BeginReadLine()
		{
			if (this.cancelOperation)
			{
				this.cancelOperation = false;
			}
			if (this.sb == null)
			{
				this.sb = new StringBuilder(1024);
				this.stream.BeginRead(this.byteBuffer, 0, this.byteBuffer.Length, new AsyncCallback(this.ReadBuffer), null);
				return;
			}
			this.FlushMessageQueue();
		}

		// Token: 0x060007E1 RID: 2017 RVA: 0x00026F17 File Offset: 0x00025117
		internal void CancelOperation()
		{
			this.cancelOperation = true;
		}

		// Token: 0x060007E2 RID: 2018 RVA: 0x00026F20 File Offset: 0x00025120
		private void ReadBuffer(IAsyncResult ar)
		{
			int num;
			try
			{
				Stream stream = this.stream;
				if (stream == null)
				{
					num = 0;
				}
				else
				{
					num = stream.EndRead(ar);
				}
			}
			catch (IOException)
			{
				num = 0;
			}
			catch (OperationCanceledException)
			{
				num = 0;
			}
			for (;;)
			{
				object obj2;
				if (num == 0)
				{
					Queue obj = this.messageQueue;
					lock (obj)
					{
						if (this.sb.Length != 0)
						{
							this.messageQueue.Enqueue(this.sb.ToString());
							this.sb.Length = 0;
						}
						this.messageQueue.Enqueue(null);
					}
					try
					{
						this.FlushMessageQueue();
						break;
					}
					finally
					{
						obj2 = this.syncObject;
						lock (obj2)
						{
							if (this.eofEvent != null)
							{
								try
								{
									this.eofEvent.Set();
								}
								catch (ObjectDisposedException)
								{
								}
							}
						}
					}
				}
				obj2 = this.syncObject;
				lock (obj2)
				{
					if (this.decoder == null)
					{
						num = 0;
						continue;
					}
					int chars = this.decoder.GetChars(this.byteBuffer, 0, num, this.charBuffer, 0);
					this.sb.Append(this.charBuffer, 0, chars);
				}
				this.GetLinesFromStringBuilder();
				obj2 = this.syncObject;
				lock (obj2)
				{
					if (this.stream == null)
					{
						num = 0;
						continue;
					}
					this.stream.BeginRead(this.byteBuffer, 0, this.byteBuffer.Length, new AsyncCallback(this.ReadBuffer), null);
				}
				break;
			}
		}

		// Token: 0x060007E3 RID: 2019 RVA: 0x00027114 File Offset: 0x00025314
		private void GetLinesFromStringBuilder()
		{
			int i = this.currentLinePos;
			int num = 0;
			int length = this.sb.Length;
			if (this.bLastCarriageReturn && length > 0 && this.sb[0] == '\n')
			{
				i = 1;
				num = 1;
				this.bLastCarriageReturn = false;
			}
			while (i < length)
			{
				char c = this.sb[i];
				if (c == '\r' || c == '\n')
				{
					string obj = this.sb.ToString(num, i - num);
					num = i + 1;
					if (c == '\r' && num < length && this.sb[num] == '\n')
					{
						num++;
						i++;
					}
					Queue obj2 = this.messageQueue;
					lock (obj2)
					{
						this.messageQueue.Enqueue(obj);
					}
				}
				i++;
			}
			if (this.sb[length - 1] == '\r')
			{
				this.bLastCarriageReturn = true;
			}
			if (num < length)
			{
				if (num == 0)
				{
					this.currentLinePos = i;
				}
				else
				{
					this.sb.Remove(0, num);
					this.currentLinePos = 0;
				}
			}
			else
			{
				this.sb.Length = 0;
				this.currentLinePos = 0;
			}
			this.FlushMessageQueue();
		}

		// Token: 0x060007E4 RID: 2020 RVA: 0x0002725C File Offset: 0x0002545C
		private void FlushMessageQueue()
		{
			while (this.messageQueue.Count > 0)
			{
				Queue obj = this.messageQueue;
				lock (obj)
				{
					if (this.messageQueue.Count > 0)
					{
						string data = (string)this.messageQueue.Dequeue();
						if (!this.cancelOperation)
						{
							this.userCallBack(data);
						}
					}
					continue;
				}
				break;
			}
		}

		// Token: 0x060007E5 RID: 2021 RVA: 0x000272D8 File Offset: 0x000254D8
		internal void WaitUtilEOF()
		{
			if (this.eofEvent != null)
			{
				this.eofEvent.WaitOne();
				this.eofEvent.Close();
				this.eofEvent = null;
			}
		}

		// Token: 0x04000B5D RID: 2909
		internal const int DefaultBufferSize = 1024;

		// Token: 0x04000B5E RID: 2910
		private const int MinBufferSize = 128;

		// Token: 0x04000B5F RID: 2911
		private Stream stream;

		// Token: 0x04000B60 RID: 2912
		private Encoding encoding;

		// Token: 0x04000B61 RID: 2913
		private Decoder decoder;

		// Token: 0x04000B62 RID: 2914
		private byte[] byteBuffer;

		// Token: 0x04000B63 RID: 2915
		private char[] charBuffer;

		// Token: 0x04000B64 RID: 2916
		private int _maxCharsPerBuffer;

		// Token: 0x04000B65 RID: 2917
		private Process process;

		// Token: 0x04000B66 RID: 2918
		private UserCallBack userCallBack;

		// Token: 0x04000B67 RID: 2919
		private bool cancelOperation;

		// Token: 0x04000B68 RID: 2920
		private ManualResetEvent eofEvent;

		// Token: 0x04000B69 RID: 2921
		private Queue messageQueue;

		// Token: 0x04000B6A RID: 2922
		private StringBuilder sb;

		// Token: 0x04000B6B RID: 2923
		private bool bLastCarriageReturn;

		// Token: 0x04000B6C RID: 2924
		private int currentLinePos;

		// Token: 0x04000B6D RID: 2925
		private object syncObject = new object();
	}
}
