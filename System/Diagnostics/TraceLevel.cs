﻿using System;

namespace System.Diagnostics
{
	/// <summary>Specifies what messages to output for the <see cref="T:System.Diagnostics.Debug" />, <see cref="T:System.Diagnostics.Trace" /> and <see cref="T:System.Diagnostics.TraceSwitch" /> classes.</summary>
	// Token: 0x020000FB RID: 251
	public enum TraceLevel
	{
		/// <summary>Output no tracing and debugging messages.</summary>
		// Token: 0x04000B3F RID: 2879
		Off,
		/// <summary>Output error-handling messages.</summary>
		// Token: 0x04000B40 RID: 2880
		Error,
		/// <summary>Output warnings and error-handling messages.</summary>
		// Token: 0x04000B41 RID: 2881
		Warning,
		/// <summary>Output informational messages, warnings, and error-handling messages.</summary>
		// Token: 0x04000B42 RID: 2882
		Info,
		/// <summary>Output all debugging and tracing messages.</summary>
		// Token: 0x04000B43 RID: 2883
		Verbose
	}
}
