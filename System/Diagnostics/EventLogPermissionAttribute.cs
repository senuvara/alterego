﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Allows declaritive permission checks for event logging. </summary>
	// Token: 0x0200066C RID: 1644
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Event, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public class EventLogPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventLogPermissionAttribute" /> class.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x06003482 RID: 13442 RVA: 0x0000232D File Offset: 0x0000052D
		public EventLogPermissionAttribute(SecurityAction action)
		{
		}

		/// <summary>Gets or sets the name of the computer on which events might be read.</summary>
		/// <returns>The name of the computer on which events might be read. The default is ".".</returns>
		/// <exception cref="T:System.ArgumentException">The computer name is invalid. </exception>
		// Token: 0x17000D4F RID: 3407
		// (get) Token: 0x06003483 RID: 13443 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003484 RID: 13444 RVA: 0x000092E2 File Offset: 0x000074E2
		public string MachineName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the access levels used in the permissions request.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Diagnostics.EventLogPermissionAccess" /> values. The default is <see cref="F:System.Diagnostics.EventLogPermissionAccess.Write" />.</returns>
		// Token: 0x17000D50 RID: 3408
		// (get) Token: 0x06003485 RID: 13445 RVA: 0x000A5654 File Offset: 0x000A3854
		// (set) Token: 0x06003486 RID: 13446 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLogPermissionAccess PermissionAccess
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return EventLogPermissionAccess.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates the permission based on the <see cref="P:System.Diagnostics.EventLogPermissionAttribute.MachineName" /> property and the requested access levels that are set through the <see cref="P:System.Diagnostics.EventLogPermissionAttribute.PermissionAccess" /> property on the attribute.</summary>
		/// <returns>An <see cref="T:System.Security.IPermission" /> that represents the created permission.</returns>
		// Token: 0x06003487 RID: 13447 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission CreatePermission()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
