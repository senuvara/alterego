﻿using System;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Represents language-neutral information for an event log entry.</summary>
	// Token: 0x02000620 RID: 1568
	public class EventInstance
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventInstance" /> class using the specified resource identifiers for the localized message and category text of the event entry.</summary>
		/// <param name="instanceId">A resource identifier that corresponds to a string defined in the message resource file of the event source.</param>
		/// <param name="categoryId">A resource identifier that corresponds to a string defined in the category resource file of the event source, or zero to specify no category for the event. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="instanceId" /> parameter is a negative value or a value larger than <see cref="F:System.UInt32.MaxValue" />.-or- The <paramref name="categoryId" /> parameter is a negative value or a value larger than <see cref="F:System.UInt16.MaxValue" />. </exception>
		// Token: 0x0600326E RID: 12910 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventInstance(long instanceId, int categoryId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventInstance" /> class using the specified resource identifiers for the localized message and category text of the event entry and the specified event log entry type.</summary>
		/// <param name="instanceId">A resource identifier that corresponds to a string defined in the message resource file of the event source. </param>
		/// <param name="categoryId">A resource identifier that corresponds to a string defined in the category resource file of the event source, or zero to specify no category for the event. </param>
		/// <param name="entryType">An <see cref="T:System.Diagnostics.EventLogEntryType" /> value that indicates the event type. </param>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">
		///         <paramref name="entryType" /> is not a valid <see cref="T:System.Diagnostics.EventLogEntryType" /> value. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="instanceId" /> is a negative value or a value larger than <see cref="F:System.UInt32.MaxValue" />.-or- 
		///         <paramref name="categoryId" /> is a negative value or a value larger than <see cref="F:System.UInt16.MaxValue" />. </exception>
		// Token: 0x0600326F RID: 12911 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventInstance(long instanceId, int categoryId, EventLogEntryType entryType)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the resource identifier that specifies the application-defined category of the event entry.</summary>
		/// <returns>A numeric category value or resource identifier that corresponds to a string defined in the category resource file of the event source. The default is zero, which signifies that no category will be displayed for the event entry.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property is set to a negative value or to a value larger than <see cref="F:System.UInt16.MaxValue" />. </exception>
		// Token: 0x17000C93 RID: 3219
		// (get) Token: 0x06003270 RID: 12912 RVA: 0x000A4870 File Offset: 0x000A2A70
		// (set) Token: 0x06003271 RID: 12913 RVA: 0x000092E2 File Offset: 0x000074E2
		public int CategoryId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the event type of the event log entry.</summary>
		/// <returns>An <see cref="T:System.Diagnostics.EventLogEntryType" /> value that indicates the event entry type. The default value is <see cref="F:System.Diagnostics.EventLogEntryType.Information" />.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">The property is not set to a valid <see cref="T:System.Diagnostics.EventLogEntryType" /> value. </exception>
		// Token: 0x17000C94 RID: 3220
		// (get) Token: 0x06003272 RID: 12914 RVA: 0x000A488C File Offset: 0x000A2A8C
		// (set) Token: 0x06003273 RID: 12915 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLogEntryType EntryType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (EventLogEntryType)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the resource identifier that designates the message text of the event entry.</summary>
		/// <returns>A resource identifier that corresponds to a string defined in the message resource file of the event source.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property is set to a negative value or to a value larger than <see cref="F:System.UInt32.MaxValue" />. </exception>
		// Token: 0x17000C95 RID: 3221
		// (get) Token: 0x06003274 RID: 12916 RVA: 0x000A48A8 File Offset: 0x000A2AA8
		// (set) Token: 0x06003275 RID: 12917 RVA: 0x000092E2 File Offset: 0x000074E2
		public long InstanceId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
