﻿using System;
using System.Collections;
using System.Reflection;

namespace System.Diagnostics
{
	/// <summary>Identifies a switch used in an assembly, class, or member.</summary>
	// Token: 0x020000F3 RID: 243
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event)]
	public sealed class SwitchAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.SwitchAttribute" /> class, specifying the name and the type of the switch. </summary>
		/// <param name="switchName">The display name of the switch.</param>
		/// <param name="switchType">The type of the switch.</param>
		// Token: 0x060006E4 RID: 1764 RVA: 0x00023830 File Offset: 0x00021A30
		public SwitchAttribute(string switchName, Type switchType)
		{
			this.SwitchName = switchName;
			this.SwitchType = switchType;
		}

		/// <summary>Gets or sets the display name of the switch.</summary>
		/// <returns>The display name of the switch.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <see cref="P:System.Diagnostics.SwitchAttribute.SwitchName" /> is set to <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <see cref="P:System.Diagnostics.SwitchAttribute.SwitchName" /> is set to an empty string.</exception>
		// Token: 0x17000124 RID: 292
		// (get) Token: 0x060006E5 RID: 1765 RVA: 0x00023846 File Offset: 0x00021A46
		// (set) Token: 0x060006E6 RID: 1766 RVA: 0x00023850 File Offset: 0x00021A50
		public string SwitchName
		{
			get
			{
				return this.name;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (value.Length == 0)
				{
					throw new ArgumentException(SR.GetString("Argument {0} cannot be null or zero-length.", new object[]
					{
						"value"
					}), "value");
				}
				this.name = value;
			}
		}

		/// <summary>Gets or sets the type of the switch.</summary>
		/// <returns>The type of the switch.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <see cref="P:System.Diagnostics.SwitchAttribute.SwitchType" /> is set to <see langword="null" />.</exception>
		// Token: 0x17000125 RID: 293
		// (get) Token: 0x060006E7 RID: 1767 RVA: 0x0002389D File Offset: 0x00021A9D
		// (set) Token: 0x060006E8 RID: 1768 RVA: 0x000238A5 File Offset: 0x00021AA5
		public Type SwitchType
		{
			get
			{
				return this.type;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.type = value;
			}
		}

		/// <summary>Gets or sets the description of the switch.</summary>
		/// <returns>The description of the switch.</returns>
		// Token: 0x17000126 RID: 294
		// (get) Token: 0x060006E9 RID: 1769 RVA: 0x000238C2 File Offset: 0x00021AC2
		// (set) Token: 0x060006EA RID: 1770 RVA: 0x000238CA File Offset: 0x00021ACA
		public string SwitchDescription
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		/// <summary>Returns all switch attributes for the specified assembly.</summary>
		/// <param name="assembly">The assembly to check for switch attributes.</param>
		/// <returns>An array that contains all the switch attributes for the assembly.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="assembly" /> is <see langword="null" />.</exception>
		// Token: 0x060006EB RID: 1771 RVA: 0x000238D4 File Offset: 0x00021AD4
		public static SwitchAttribute[] GetAll(Assembly assembly)
		{
			if (assembly == null)
			{
				throw new ArgumentNullException("assembly");
			}
			ArrayList arrayList = new ArrayList();
			object[] customAttributes = assembly.GetCustomAttributes(typeof(SwitchAttribute), false);
			arrayList.AddRange(customAttributes);
			Type[] types = assembly.GetTypes();
			for (int i = 0; i < types.Length; i++)
			{
				SwitchAttribute.GetAllRecursive(types[i], arrayList);
			}
			SwitchAttribute[] array = new SwitchAttribute[arrayList.Count];
			arrayList.CopyTo(array, 0);
			return array;
		}

		// Token: 0x060006EC RID: 1772 RVA: 0x0002394C File Offset: 0x00021B4C
		private static void GetAllRecursive(Type type, ArrayList switchAttribs)
		{
			SwitchAttribute.GetAllRecursive(type, switchAttribs);
			MemberInfo[] members = type.GetMembers(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			for (int i = 0; i < members.Length; i++)
			{
				if (!(members[i] is Type))
				{
					SwitchAttribute.GetAllRecursive(members[i], switchAttribs);
				}
			}
		}

		// Token: 0x060006ED RID: 1773 RVA: 0x0002398C File Offset: 0x00021B8C
		private static void GetAllRecursive(MemberInfo member, ArrayList switchAttribs)
		{
			object[] customAttributes = member.GetCustomAttributes(typeof(SwitchAttribute), false);
			switchAttribs.AddRange(customAttributes);
		}

		// Token: 0x04000B1F RID: 2847
		private Type type;

		// Token: 0x04000B20 RID: 2848
		private string name;

		// Token: 0x04000B21 RID: 2849
		private string description;
	}
}
