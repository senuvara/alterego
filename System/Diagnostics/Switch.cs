﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Threading;
using System.Xml.Serialization;

namespace System.Diagnostics
{
	/// <summary>Provides an abstract base class to create new debugging and tracing switches.</summary>
	// Token: 0x020000F2 RID: 242
	public abstract class Switch
	{
		// Token: 0x1700011E RID: 286
		// (get) Token: 0x060006D0 RID: 1744 RVA: 0x00023408 File Offset: 0x00021608
		private object IntializedLock
		{
			get
			{
				if (this.m_intializedLock == null)
				{
					object value = new object();
					Interlocked.CompareExchange<object>(ref this.m_intializedLock, value, null);
				}
				return this.m_intializedLock;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Switch" /> class.</summary>
		/// <param name="displayName">The name of the switch. </param>
		/// <param name="description">The description for the switch. </param>
		// Token: 0x060006D1 RID: 1745 RVA: 0x00023437 File Offset: 0x00021637
		protected Switch(string displayName, string description) : this(displayName, description, "0")
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Switch" /> class, specifying the display name, description, and default value for the switch. </summary>
		/// <param name="displayName">The name of the switch. </param>
		/// <param name="description">The description of the switch. </param>
		/// <param name="defaultSwitchValue">The default value for the switch.</param>
		// Token: 0x060006D2 RID: 1746 RVA: 0x00023448 File Offset: 0x00021648
		protected Switch(string displayName, string description, string defaultSwitchValue)
		{
			if (displayName == null)
			{
				displayName = string.Empty;
			}
			this.displayName = displayName;
			this.description = description;
			List<WeakReference> obj = Switch.switches;
			lock (obj)
			{
				Switch._pruneCachedSwitches();
				Switch.switches.Add(new WeakReference(this));
			}
			this.defaultValue = defaultSwitchValue;
		}

		// Token: 0x060006D3 RID: 1747 RVA: 0x000234C8 File Offset: 0x000216C8
		private static void _pruneCachedSwitches()
		{
			List<WeakReference> obj = Switch.switches;
			lock (obj)
			{
				if (Switch.s_LastCollectionCount != GC.CollectionCount(2))
				{
					List<WeakReference> list = new List<WeakReference>(Switch.switches.Count);
					for (int i = 0; i < Switch.switches.Count; i++)
					{
						if ((Switch)Switch.switches[i].Target != null)
						{
							list.Add(Switch.switches[i]);
						}
					}
					if (list.Count < Switch.switches.Count)
					{
						Switch.switches.Clear();
						Switch.switches.AddRange(list);
						Switch.switches.TrimExcess();
					}
					Switch.s_LastCollectionCount = GC.CollectionCount(2);
				}
			}
		}

		/// <summary>Gets the custom switch attributes defined in the application configuration file.</summary>
		/// <returns>A <see cref="T:System.Collections.Specialized.StringDictionary" /> containing the case-insensitive custom attributes for the trace switch.</returns>
		// Token: 0x1700011F RID: 287
		// (get) Token: 0x060006D4 RID: 1748 RVA: 0x0002359C File Offset: 0x0002179C
		[XmlIgnore]
		public StringDictionary Attributes
		{
			get
			{
				this.Initialize();
				if (this.attributes == null)
				{
					this.attributes = new StringDictionary();
				}
				return this.attributes;
			}
		}

		/// <summary>Gets a name used to identify the switch.</summary>
		/// <returns>The name used to identify the switch. The default value is an empty string ("").</returns>
		// Token: 0x17000120 RID: 288
		// (get) Token: 0x060006D5 RID: 1749 RVA: 0x000235BD File Offset: 0x000217BD
		public string DisplayName
		{
			get
			{
				return this.displayName;
			}
		}

		/// <summary>Gets a description of the switch.</summary>
		/// <returns>The description of the switch. The default value is an empty string ("").</returns>
		// Token: 0x17000121 RID: 289
		// (get) Token: 0x060006D6 RID: 1750 RVA: 0x000235C5 File Offset: 0x000217C5
		public string Description
		{
			get
			{
				if (this.description != null)
				{
					return this.description;
				}
				return string.Empty;
			}
		}

		/// <summary>Gets or sets the current setting for this switch.</summary>
		/// <returns>The current setting for this switch. The default is zero.</returns>
		// Token: 0x17000122 RID: 290
		// (get) Token: 0x060006D7 RID: 1751 RVA: 0x000235DB File Offset: 0x000217DB
		// (set) Token: 0x060006D8 RID: 1752 RVA: 0x000235FC File Offset: 0x000217FC
		protected int SwitchSetting
		{
			get
			{
				if (!this.initialized && this.InitializeWithStatus())
				{
					this.OnSwitchSettingChanged();
				}
				return this.switchSetting;
			}
			set
			{
				bool flag = false;
				object intializedLock = this.IntializedLock;
				lock (intializedLock)
				{
					this.initialized = true;
					if (this.switchSetting != value)
					{
						this.switchSetting = value;
						flag = true;
					}
				}
				if (flag)
				{
					this.OnSwitchSettingChanged();
				}
			}
		}

		/// <summary>Gets or sets the value of the switch.</summary>
		/// <returns>A string representing the value of the switch.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The value is <see langword="null" />.-or-The value does not consist solely of an optional negative sign followed by a sequence of digits ranging from 0 to 9.-or-The value represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />.</exception>
		// Token: 0x17000123 RID: 291
		// (get) Token: 0x060006D9 RID: 1753 RVA: 0x0002365C File Offset: 0x0002185C
		// (set) Token: 0x060006DA RID: 1754 RVA: 0x0002366C File Offset: 0x0002186C
		protected string Value
		{
			get
			{
				this.Initialize();
				return this.switchValueString;
			}
			set
			{
				this.Initialize();
				this.switchValueString = value;
				this.OnValueChanged();
			}
		}

		// Token: 0x060006DB RID: 1755 RVA: 0x00023683 File Offset: 0x00021883
		private void Initialize()
		{
			this.InitializeWithStatus();
		}

		// Token: 0x060006DC RID: 1756 RVA: 0x0002368C File Offset: 0x0002188C
		private bool InitializeWithStatus()
		{
			if (!this.initialized)
			{
				object intializedLock = this.IntializedLock;
				lock (intializedLock)
				{
					if (this.initialized || this.initializing)
					{
						return false;
					}
					this.initializing = true;
					if (this.switchSettings == null && !this.InitializeConfigSettings())
					{
						this.initialized = true;
						this.initializing = false;
						return false;
					}
					this.switchValueString = this.defaultValue;
					this.OnValueChanged();
					this.initialized = true;
					this.initializing = false;
				}
				return true;
			}
			return true;
		}

		// Token: 0x060006DD RID: 1757 RVA: 0x0002373C File Offset: 0x0002193C
		private bool InitializeConfigSettings()
		{
			object obj = this.switchSettings;
			return true;
		}

		/// <summary>Gets the custom attributes supported by the switch.</summary>
		/// <returns>A string array that contains the names of the custom attributes supported by the switch, or <see langword="null" /> if there no custom attributes are supported.</returns>
		// Token: 0x060006DE RID: 1758 RVA: 0x00008B3F File Offset: 0x00006D3F
		protected internal virtual string[] GetSupportedAttributes()
		{
			return null;
		}

		/// <summary>Invoked when the <see cref="P:System.Diagnostics.Switch.SwitchSetting" /> property is changed.</summary>
		// Token: 0x060006DF RID: 1759 RVA: 0x0000232D File Offset: 0x0000052D
		protected virtual void OnSwitchSettingChanged()
		{
		}

		/// <summary>Invoked when the <see cref="P:System.Diagnostics.Switch.Value" /> property is changed.</summary>
		// Token: 0x060006E0 RID: 1760 RVA: 0x00023746 File Offset: 0x00021946
		protected virtual void OnValueChanged()
		{
			this.SwitchSetting = int.Parse(this.Value, CultureInfo.InvariantCulture);
		}

		// Token: 0x060006E1 RID: 1761 RVA: 0x00023760 File Offset: 0x00021960
		internal static void RefreshAll()
		{
			List<WeakReference> obj = Switch.switches;
			lock (obj)
			{
				Switch._pruneCachedSwitches();
				for (int i = 0; i < Switch.switches.Count; i++)
				{
					Switch @switch = (Switch)Switch.switches[i].Target;
					if (@switch != null)
					{
						@switch.Refresh();
					}
				}
			}
		}

		// Token: 0x060006E2 RID: 1762 RVA: 0x000237D4 File Offset: 0x000219D4
		internal void Refresh()
		{
			object intializedLock = this.IntializedLock;
			lock (intializedLock)
			{
				this.initialized = false;
				this.switchSettings = null;
				this.Initialize();
			}
		}

		// Token: 0x060006E3 RID: 1763 RVA: 0x00023824 File Offset: 0x00021A24
		// Note: this type is marked as 'beforefieldinit'.
		static Switch()
		{
		}

		// Token: 0x04000B13 RID: 2835
		private object switchSettings;

		// Token: 0x04000B14 RID: 2836
		private readonly string description;

		// Token: 0x04000B15 RID: 2837
		private readonly string displayName;

		// Token: 0x04000B16 RID: 2838
		private int switchSetting;

		// Token: 0x04000B17 RID: 2839
		private volatile bool initialized;

		// Token: 0x04000B18 RID: 2840
		private bool initializing;

		// Token: 0x04000B19 RID: 2841
		private volatile string switchValueString = string.Empty;

		// Token: 0x04000B1A RID: 2842
		private StringDictionary attributes;

		// Token: 0x04000B1B RID: 2843
		private string defaultValue;

		// Token: 0x04000B1C RID: 2844
		private object m_intializedLock;

		// Token: 0x04000B1D RID: 2845
		private static List<WeakReference> switches = new List<WeakReference>();

		// Token: 0x04000B1E RID: 2846
		private static int s_LastCollectionCount;
	}
}
