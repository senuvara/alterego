﻿using System;

namespace System.Diagnostics
{
	/// <summary>Specifies the current execution state of the thread.</summary>
	// Token: 0x0200011C RID: 284
	public enum ThreadState
	{
		/// <summary>A state that indicates the thread has been initialized, but has not yet started.</summary>
		// Token: 0x04000C0D RID: 3085
		Initialized,
		/// <summary>A state that indicates the thread is waiting to use a processor because no processor is free. The thread is prepared to run on the next available processor.</summary>
		// Token: 0x04000C0E RID: 3086
		Ready,
		/// <summary>A state that indicates the thread is currently using a processor.</summary>
		// Token: 0x04000C0F RID: 3087
		Running,
		/// <summary>A state that indicates the thread is about to use a processor. Only one thread can be in this state at a time.</summary>
		// Token: 0x04000C10 RID: 3088
		Standby,
		/// <summary>A state that indicates the thread has finished executing and has exited.</summary>
		// Token: 0x04000C11 RID: 3089
		Terminated,
		/// <summary>A state that indicates the thread is waiting for a resource, other than the processor, before it can execute. For example, it might be waiting for its execution stack to be paged in from disk.</summary>
		// Token: 0x04000C12 RID: 3090
		Transition = 6,
		/// <summary>The state of the thread is unknown.</summary>
		// Token: 0x04000C13 RID: 3091
		Unknown,
		/// <summary>A state that indicates the thread is not ready to use the processor because it is waiting for a peripheral operation to complete or a resource to become free. When the thread is ready, it will be rescheduled.</summary>
		// Token: 0x04000C14 RID: 3092
		Wait = 5
	}
}
