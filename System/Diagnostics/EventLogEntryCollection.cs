﻿using System;
using System.Collections;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Defines size and enumerators for a collection of <see cref="T:System.Diagnostics.EventLogEntry" /> instances.</summary>
	// Token: 0x02000619 RID: 1561
	public class EventLogEntryCollection : ICollection, IEnumerable
	{
		// Token: 0x0600323F RID: 12863 RVA: 0x000092E2 File Offset: 0x000074E2
		internal EventLogEntryCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the number of entries in the event log (that is, the number of elements in the <see cref="T:System.Diagnostics.EventLogEntry" /> collection).</summary>
		/// <returns>The number of entries currently in the event log.</returns>
		// Token: 0x17000C7B RID: 3195
		// (get) Token: 0x06003240 RID: 12864 RVA: 0x000A473C File Offset: 0x000A293C
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets an entry in the event log, based on an index that starts at 0 (zero).</summary>
		/// <param name="index">The zero-based index that is associated with the event log entry. </param>
		/// <returns>The event log entry at the location that is specified by the <paramref name="index" /> parameter.</returns>
		// Token: 0x17000C7C RID: 3196
		public virtual EventLogEntry this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x06003242 RID: 12866 RVA: 0x000A4758 File Offset: 0x000A2958
		bool ICollection.get_IsSynchronized()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06003243 RID: 12867 RVA: 0x00043C3C File Offset: 0x00041E3C
		object ICollection.get_SyncRoot()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Copies the elements of the <see cref="T:System.Diagnostics.EventLogEntryCollection" /> to an array of <see cref="T:System.Diagnostics.EventLogEntry" /> instances, starting at a particular array index.</summary>
		/// <param name="entries">The one-dimensional array of <see cref="T:System.Diagnostics.EventLogEntry" /> instances that is the destination of the elements copied from the collection. The array must have zero-based indexing. </param>
		/// <param name="index">The zero-based index in the array at which copying begins. </param>
		// Token: 0x06003244 RID: 12868 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(EventLogEntry[] entries, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Supports a simple iteration over the <see cref="T:System.Diagnostics.EventLogEntryCollection" /> object.</summary>
		/// <returns>An object that can be used to iterate over the collection.</returns>
		// Token: 0x06003245 RID: 12869 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Copies the elements of the collection to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements that are copied from the collection. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		// Token: 0x06003246 RID: 12870 RVA: 0x000092E2 File Offset: 0x000074E2
		void ICollection.CopyTo(Array array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
