﻿using System;

namespace System.Diagnostics.CodeAnalysis
{
	/// <summary>Specifies that the attributed code should be excluded from code coverage information. </summary>
	// Token: 0x0200011E RID: 286
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event, Inherited = false, AllowMultiple = false)]
	public sealed class ExcludeFromCodeCoverageAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute" /> class. </summary>
		// Token: 0x06000917 RID: 2327 RVA: 0x000020AE File Offset: 0x000002AE
		public ExcludeFromCodeCoverageAttribute()
		{
		}
	}
}
