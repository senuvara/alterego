﻿using System;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Holds instance data associated with a performance counter sample.</summary>
	// Token: 0x0200066F RID: 1647
	public class InstanceData
	{
		/// <summary>Initializes a new instance of the InstanceData class, using the specified sample and performance counter instance.</summary>
		/// <param name="instanceName">The name of an instance associated with the performance counter. </param>
		/// <param name="sample">A <see cref="T:System.Diagnostics.CounterSample" /> taken from the instance specified by the <paramref name="instanceName" /> parameter. </param>
		// Token: 0x06003491 RID: 13457 RVA: 0x000092E2 File Offset: 0x000074E2
		public InstanceData(string instanceName, CounterSample sample)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the instance name associated with this instance data.</summary>
		/// <returns>The name of an instance associated with the performance counter.</returns>
		// Token: 0x17000D52 RID: 3410
		// (get) Token: 0x06003492 RID: 13458 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string InstanceName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the raw data value associated with the performance counter sample.</summary>
		/// <returns>The raw value read by the performance counter sample associated with the <see cref="P:System.Diagnostics.InstanceData.Sample" /> property.</returns>
		// Token: 0x17000D53 RID: 3411
		// (get) Token: 0x06003493 RID: 13459 RVA: 0x000A5670 File Offset: 0x000A3870
		public long RawValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Gets the performance counter sample that generated this data.</summary>
		/// <returns>A <see cref="T:System.Diagnostics.CounterSample" /> taken from the instance specified by the <see cref="P:System.Diagnostics.InstanceData.InstanceName" /> property.</returns>
		// Token: 0x17000D54 RID: 3412
		// (get) Token: 0x06003494 RID: 13460 RVA: 0x000A568C File Offset: 0x000A388C
		public CounterSample Sample
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(CounterSample);
			}
		}
	}
}
