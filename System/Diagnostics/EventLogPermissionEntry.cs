﻿using System;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Defines the smallest unit of a code access security permission that is set for an <see cref="T:System.Diagnostics.EventLog" />.</summary>
	// Token: 0x0200066A RID: 1642
	[Serializable]
	public class EventLogPermissionEntry
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventLogPermissionEntry" /> class.</summary>
		/// <param name="permissionAccess">A bitwise combination of the <see cref="T:System.Diagnostics.EventLogPermissionAccess" /> values. The <see cref="P:System.Diagnostics.EventLogPermissionEntry.PermissionAccess" /> property is set to this value. </param>
		/// <param name="machineName">The name of the computer on which to read or write events. The <see cref="P:System.Diagnostics.EventLogPermissionEntry.MachineName" /> property is set to this value. </param>
		/// <exception cref="T:System.ArgumentException">The computer name is invalid. </exception>
		// Token: 0x06003474 RID: 13428 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLogPermissionEntry(EventLogPermissionAccess permissionAccess, string machineName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the name of the computer on which to read or write events.</summary>
		/// <returns>The name of the computer on which to read or write events.</returns>
		// Token: 0x17000D4C RID: 3404
		// (get) Token: 0x06003475 RID: 13429 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string MachineName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the permission access levels used in the permissions request.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Diagnostics.EventLogPermissionAccess" /> values.</returns>
		// Token: 0x17000D4D RID: 3405
		// (get) Token: 0x06003476 RID: 13430 RVA: 0x000A55E4 File Offset: 0x000A37E4
		public EventLogPermissionAccess PermissionAccess
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return EventLogPermissionAccess.None;
			}
		}
	}
}
