﻿using System;

namespace System.Diagnostics
{
	/// <summary>Indicates the priority that the system associates with a process. This value, together with the priority value of each thread of the process, determines each thread's base priority level.</summary>
	// Token: 0x02000117 RID: 279
	public enum ProcessPriorityClass
	{
		/// <summary>Specifies that the process has priority above <see langword="Normal" /> but below <see cref="F:System.Diagnostics.ProcessPriorityClass.High" />.</summary>
		// Token: 0x04000BF4 RID: 3060
		AboveNormal = 32768,
		/// <summary>Specifies that the process has priority above <see langword="Idle" /> but below <see langword="Normal" />.</summary>
		// Token: 0x04000BF5 RID: 3061
		BelowNormal = 16384,
		/// <summary>Specifies that the process performs time-critical tasks that must be executed immediately, such as the <see langword="Task List" /> dialog, which must respond quickly when called by the user, regardless of the load on the operating system. The threads of the process preempt the threads of normal or idle priority class processes.</summary>
		// Token: 0x04000BF6 RID: 3062
		High = 128,
		/// <summary>Specifies that the threads of this process run only when the system is idle, such as a screen saver. The threads of the process are preempted by the threads of any process running in a higher priority class.</summary>
		// Token: 0x04000BF7 RID: 3063
		Idle = 64,
		/// <summary>Specifies that the process has no special scheduling needs.</summary>
		// Token: 0x04000BF8 RID: 3064
		Normal = 32,
		/// <summary>Specifies that the process has the highest possible priority.</summary>
		// Token: 0x04000BF9 RID: 3065
		RealTime = 256
	}
}
