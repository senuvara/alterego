﻿using System;
using System.Collections;
using System.Runtime.Remoting.Messaging;

namespace System.Diagnostics
{
	/// <summary>Correlates traces that are part of a logical transaction.</summary>
	// Token: 0x020000EB RID: 235
	public class CorrelationManager
	{
		// Token: 0x06000685 RID: 1669 RVA: 0x0000232F File Offset: 0x0000052F
		internal CorrelationManager()
		{
		}

		/// <summary>Gets or sets the identity for a global activity.</summary>
		/// <returns>A <see cref="T:System.Guid" /> structure that identifies the global activity.</returns>
		// Token: 0x17000114 RID: 276
		// (get) Token: 0x06000686 RID: 1670 RVA: 0x00022AD0 File Offset: 0x00020CD0
		// (set) Token: 0x06000687 RID: 1671 RVA: 0x00022AF7 File Offset: 0x00020CF7
		public Guid ActivityId
		{
			get
			{
				object obj = CallContext.LogicalGetData("E2ETrace.ActivityID");
				if (obj != null)
				{
					return (Guid)obj;
				}
				return Guid.Empty;
			}
			set
			{
				CallContext.LogicalSetData("E2ETrace.ActivityID", value);
			}
		}

		/// <summary>Gets the logical operation stack from the call context.</summary>
		/// <returns>A <see cref="T:System.Collections.Stack" /> object that represents the logical operation stack for the call context.</returns>
		// Token: 0x17000115 RID: 277
		// (get) Token: 0x06000688 RID: 1672 RVA: 0x00022B09 File Offset: 0x00020D09
		public Stack LogicalOperationStack
		{
			get
			{
				return this.GetLogicalOperationStack();
			}
		}

		/// <summary>Starts a logical operation with the specified identity on a thread.</summary>
		/// <param name="operationId">An object identifying the operation.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="operationId" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06000689 RID: 1673 RVA: 0x00022B11 File Offset: 0x00020D11
		public void StartLogicalOperation(object operationId)
		{
			if (operationId == null)
			{
				throw new ArgumentNullException("operationId");
			}
			this.GetLogicalOperationStack().Push(operationId);
		}

		/// <summary>Starts a logical operation on a thread.</summary>
		// Token: 0x0600068A RID: 1674 RVA: 0x00022B2D File Offset: 0x00020D2D
		public void StartLogicalOperation()
		{
			this.StartLogicalOperation(Guid.NewGuid());
		}

		/// <summary>Stops the current logical operation.</summary>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Diagnostics.CorrelationManager.LogicalOperationStack" /> property is an empty stack.</exception>
		// Token: 0x0600068B RID: 1675 RVA: 0x00022B3F File Offset: 0x00020D3F
		public void StopLogicalOperation()
		{
			this.GetLogicalOperationStack().Pop();
		}

		// Token: 0x0600068C RID: 1676 RVA: 0x00022B50 File Offset: 0x00020D50
		private Stack GetLogicalOperationStack()
		{
			Stack stack = CallContext.LogicalGetData("System.Diagnostics.Trace.CorrelationManagerSlot") as Stack;
			if (stack == null)
			{
				stack = new Stack();
				CallContext.LogicalSetData("System.Diagnostics.Trace.CorrelationManagerSlot", stack);
			}
			return stack;
		}

		// Token: 0x04000B03 RID: 2819
		private const string transactionSlotName = "System.Diagnostics.Trace.CorrelationManagerSlot";

		// Token: 0x04000B04 RID: 2820
		private const string activityIdSlotName = "E2ETrace.ActivityID";
	}
}
