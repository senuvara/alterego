﻿using System;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Provides data for the <see cref="E:System.Diagnostics.Process.OutputDataReceived" /> and <see cref="E:System.Diagnostics.Process.ErrorDataReceived" /> events.</summary>
	// Token: 0x02000111 RID: 273
	public class DataReceivedEventArgs : EventArgs
	{
		// Token: 0x060008B6 RID: 2230 RVA: 0x000297D4 File Offset: 0x000279D4
		internal DataReceivedEventArgs(string data)
		{
			this.data = data;
		}

		/// <summary>Gets the line of characters that was written to a redirected <see cref="T:System.Diagnostics.Process" /> output stream.</summary>
		/// <returns>The line that was written by an associated <see cref="T:System.Diagnostics.Process" /> to its redirected <see cref="P:System.Diagnostics.Process.StandardOutput" /> or <see cref="P:System.Diagnostics.Process.StandardError" /> stream.</returns>
		// Token: 0x170001AA RID: 426
		// (get) Token: 0x060008B7 RID: 2231 RVA: 0x000297E3 File Offset: 0x000279E3
		public string Data
		{
			get
			{
				return this.data;
			}
		}

		// Token: 0x060008B8 RID: 2232 RVA: 0x000092E2 File Offset: 0x000074E2
		internal DataReceivedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000BCA RID: 3018
		private string data;
	}
}
