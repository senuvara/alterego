﻿using System;
using System.Collections;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Contains a strongly typed collection of <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> objects.</summary>
	// Token: 0x02000679 RID: 1657
	[Serializable]
	public class PerformanceCounterPermissionEntryCollection : CollectionBase
	{
		// Token: 0x060034EA RID: 13546 RVA: 0x000092E2 File Offset: 0x000074E2
		internal PerformanceCounterPermissionEntryCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the object at a specified index.</summary>
		/// <param name="index">The zero-based index into the collection. </param>
		/// <returns>The <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> object that exists at the specified index.</returns>
		// Token: 0x17000D6D RID: 3437
		public PerformanceCounterPermissionEntry this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds a specified <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> to this collection.</summary>
		/// <param name="value">The <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> object to add. </param>
		/// <returns>The zero-based index of the added <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> object.</returns>
		// Token: 0x060034ED RID: 13549 RVA: 0x000A58F4 File Offset: 0x000A3AF4
		public int Add(PerformanceCounterPermissionEntry value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Appends a set of specified permission entries to this collection.</summary>
		/// <param name="value">A <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntryCollection" /> that contains the permission entries to add. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x060034EE RID: 13550 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(PerformanceCounterPermissionEntryCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends a set of specified permission entries to this collection.</summary>
		/// <param name="value">An array of type <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> objects that contains the permission entries to add. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x060034EF RID: 13551 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(PerformanceCounterPermissionEntry[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether this collection contains a specified <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> object.</summary>
		/// <param name="value">The <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> object to find. </param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> object belongs to this collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x060034F0 RID: 13552 RVA: 0x000A5910 File Offset: 0x000A3B10
		public bool Contains(PerformanceCounterPermissionEntry value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the permission entries from this collection to an array, starting at a particular index of the array.</summary>
		/// <param name="array">An array of type <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> that receives this collection's permission entries. </param>
		/// <param name="index">The zero-based index at which to begin copying the permission entries. </param>
		// Token: 0x060034F1 RID: 13553 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(PerformanceCounterPermissionEntry[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines the index of a specified permission entry in this collection.</summary>
		/// <param name="value">The permission entry for which to search. </param>
		/// <returns>The zero-based index of the specified permission entry, or -1 if the permission entry was not found in the collection.</returns>
		// Token: 0x060034F2 RID: 13554 RVA: 0x000A592C File Offset: 0x000A3B2C
		public int IndexOf(PerformanceCounterPermissionEntry value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Inserts a permission entry into this collection at a specified index.</summary>
		/// <param name="index">The zero-based index of the collection at which to insert the permission entry. </param>
		/// <param name="value">The permission entry to insert into this collection. </param>
		// Token: 0x060034F3 RID: 13555 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Insert(int index, PerformanceCounterPermissionEntry value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes a specified permission entry from this collection.</summary>
		/// <param name="value">The permission entry to remove. </param>
		// Token: 0x060034F4 RID: 13556 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(PerformanceCounterPermissionEntry value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
