﻿using System;
using System.Runtime.CompilerServices;

namespace System.Diagnostics
{
	/// <summary>Provides a set of methods and properties that you can use to accurately measure elapsed time.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x0200011A RID: 282
	public class Stopwatch
	{
		/// <summary>Gets the current number of ticks in the timer mechanism.</summary>
		/// <returns>A long integer representing the tick counter value of the underlying timer mechanism.</returns>
		// Token: 0x0600090B RID: 2315
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetTimestamp();

		/// <summary>Initializes a new <see cref="T:System.Diagnostics.Stopwatch" /> instance, sets the elapsed time property to zero, and starts measuring elapsed time.</summary>
		/// <returns>A <see cref="T:System.Diagnostics.Stopwatch" /> that has just begun measuring elapsed time.</returns>
		// Token: 0x0600090C RID: 2316 RVA: 0x00029EB3 File Offset: 0x000280B3
		public static Stopwatch StartNew()
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			return stopwatch;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Stopwatch" /> class.</summary>
		// Token: 0x0600090D RID: 2317 RVA: 0x0000232F File Offset: 0x0000052F
		public Stopwatch()
		{
		}

		/// <summary>Gets the total elapsed time measured by the current instance.</summary>
		/// <returns>A read-only <see cref="T:System.TimeSpan" /> representing the total elapsed time measured by the current instance.</returns>
		// Token: 0x170001DD RID: 477
		// (get) Token: 0x0600090E RID: 2318 RVA: 0x00029EC0 File Offset: 0x000280C0
		public TimeSpan Elapsed
		{
			get
			{
				if (Stopwatch.IsHighResolution)
				{
					return TimeSpan.FromTicks(this.ElapsedTicks / (Stopwatch.Frequency / 10000000L));
				}
				return TimeSpan.FromTicks(this.ElapsedTicks);
			}
		}

		/// <summary>Gets the total elapsed time measured by the current instance, in milliseconds.</summary>
		/// <returns>A read-only long integer representing the total number of milliseconds measured by the current instance.</returns>
		// Token: 0x170001DE RID: 478
		// (get) Token: 0x0600090F RID: 2319 RVA: 0x00029EF0 File Offset: 0x000280F0
		public long ElapsedMilliseconds
		{
			get
			{
				if (Stopwatch.IsHighResolution)
				{
					return this.ElapsedTicks / (Stopwatch.Frequency / 1000L);
				}
				return checked((long)this.Elapsed.TotalMilliseconds);
			}
		}

		/// <summary>Gets the total elapsed time measured by the current instance, in timer ticks.</summary>
		/// <returns>A read-only long integer representing the total number of timer ticks measured by the current instance.</returns>
		// Token: 0x170001DF RID: 479
		// (get) Token: 0x06000910 RID: 2320 RVA: 0x00029F27 File Offset: 0x00028127
		public long ElapsedTicks
		{
			get
			{
				if (!this.is_running)
				{
					return this.elapsed;
				}
				return Stopwatch.GetTimestamp() - this.started + this.elapsed;
			}
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Diagnostics.Stopwatch" /> timer is running.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Diagnostics.Stopwatch" /> instance is currently running and measuring elapsed time for an interval; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x06000911 RID: 2321 RVA: 0x00029F4B File Offset: 0x0002814B
		public bool IsRunning
		{
			get
			{
				return this.is_running;
			}
		}

		/// <summary>Stops time interval measurement and resets the elapsed time to zero.</summary>
		// Token: 0x06000912 RID: 2322 RVA: 0x00029F53 File Offset: 0x00028153
		public void Reset()
		{
			this.elapsed = 0L;
			this.is_running = false;
		}

		/// <summary>Starts, or resumes, measuring elapsed time for an interval.</summary>
		// Token: 0x06000913 RID: 2323 RVA: 0x00029F64 File Offset: 0x00028164
		public void Start()
		{
			if (this.is_running)
			{
				return;
			}
			this.started = Stopwatch.GetTimestamp();
			this.is_running = true;
		}

		/// <summary>Stops measuring elapsed time for an interval.</summary>
		// Token: 0x06000914 RID: 2324 RVA: 0x00029F81 File Offset: 0x00028181
		public void Stop()
		{
			if (!this.is_running)
			{
				return;
			}
			this.elapsed += Stopwatch.GetTimestamp() - this.started;
			if (this.elapsed < 0L)
			{
				this.elapsed = 0L;
			}
			this.is_running = false;
		}

		/// <summary>Stops time interval measurement, resets the elapsed time to zero, and starts measuring elapsed time.</summary>
		// Token: 0x06000915 RID: 2325 RVA: 0x00029FBE File Offset: 0x000281BE
		public void Restart()
		{
			this.started = Stopwatch.GetTimestamp();
			this.elapsed = 0L;
			this.is_running = true;
		}

		// Token: 0x06000916 RID: 2326 RVA: 0x00029FDA File Offset: 0x000281DA
		// Note: this type is marked as 'beforefieldinit'.
		static Stopwatch()
		{
		}

		/// <summary>Gets the frequency of the timer as the number of ticks per second. This field is read-only.</summary>
		// Token: 0x04000BFF RID: 3071
		public static readonly long Frequency = 10000000L;

		/// <summary>Indicates whether the timer is based on a high-resolution performance counter. This field is read-only.</summary>
		// Token: 0x04000C00 RID: 3072
		public static readonly bool IsHighResolution = true;

		// Token: 0x04000C01 RID: 3073
		private long elapsed;

		// Token: 0x04000C02 RID: 3074
		private long started;

		// Token: 0x04000C03 RID: 3075
		private bool is_running;
	}
}
