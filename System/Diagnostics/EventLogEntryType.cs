﻿using System;

namespace System.Diagnostics
{
	/// <summary>Specifies the event type of an event log entry.</summary>
	// Token: 0x0200061B RID: 1563
	public enum EventLogEntryType
	{
		/// <summary>An error event. This indicates a significant problem the user should know about; usually a loss of functionality or data.</summary>
		// Token: 0x040024C8 RID: 9416
		Error = 1,
		/// <summary>A failure audit event. This indicates a security event that occurs when an audited access attempt fails; for example, a failed attempt to open a file.</summary>
		// Token: 0x040024C9 RID: 9417
		FailureAudit = 16,
		/// <summary>An information event. This indicates a significant, successful operation.</summary>
		// Token: 0x040024CA RID: 9418
		Information = 4,
		/// <summary>A success audit event. This indicates a security event that occurs when an audited access attempt is successful; for example, logging on successfully.</summary>
		// Token: 0x040024CB RID: 9419
		SuccessAudit = 8,
		/// <summary>A warning event. This indicates a problem that is not immediately significant, but that may signify conditions that could cause future problems.</summary>
		// Token: 0x040024CC RID: 9420
		Warning = 2
	}
}
