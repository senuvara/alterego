﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Provides a simple listener that directs tracing or debugging output to an <see cref="T:System.Diagnostics.EventLog" />.</summary>
	// Token: 0x0200066D RID: 1645
	[HostProtection(SecurityAction.LinkDemand, Synchronization = true)]
	public sealed class EventLogTraceListener : TraceListener
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventLogTraceListener" /> class without a trace listener.</summary>
		// Token: 0x06003488 RID: 13448 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLogTraceListener()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventLogTraceListener" /> class using the specified event log.</summary>
		/// <param name="eventLog">The event log to write to. </param>
		// Token: 0x06003489 RID: 13449 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLogTraceListener(EventLog eventLog)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventLogTraceListener" /> class using the specified source.</summary>
		/// <param name="source">The name of an existing event log source. </param>
		// Token: 0x0600348A RID: 13450 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLogTraceListener(string source)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the event log to write to.</summary>
		/// <returns>The event log to write to.</returns>
		// Token: 0x17000D51 RID: 3409
		// (get) Token: 0x0600348B RID: 13451 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600348C RID: 13452 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLog EventLog
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Writes a message to the event log for this instance.</summary>
		/// <param name="message">The message to write. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="message" /> exceeds 32,766 characters.</exception>
		// Token: 0x0600348D RID: 13453 RVA: 0x000092E2 File Offset: 0x000074E2
		public override void Write(string message)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes a message to the event log for this instance.</summary>
		/// <param name="message">The message to write. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="message" /> exceeds 32,766 characters.</exception>
		// Token: 0x0600348E RID: 13454 RVA: 0x000092E2 File Offset: 0x000074E2
		public override void WriteLine(string message)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
