﻿using System;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Provides data for the <see cref="E:System.Diagnostics.EventLog.EntryWritten" /> event.</summary>
	// Token: 0x0200061E RID: 1566
	public class EntryWrittenEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EntryWrittenEventArgs" /> class.</summary>
		// Token: 0x0600325C RID: 12892 RVA: 0x000092E2 File Offset: 0x000074E2
		public EntryWrittenEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EntryWrittenEventArgs" /> class with the specified event log entry.</summary>
		/// <param name="entry">An <see cref="T:System.Diagnostics.EventLogEntry" /> that represents the entry that was written. </param>
		// Token: 0x0600325D RID: 12893 RVA: 0x000092E2 File Offset: 0x000074E2
		public EntryWrittenEventArgs(EventLogEntry entry)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the event log entry that was written to the log.</summary>
		/// <returns>An <see cref="T:System.Diagnostics.EventLogEntry" /> that represents the entry that was written to the event log.</returns>
		// Token: 0x17000C8B RID: 3211
		// (get) Token: 0x0600325E RID: 12894 RVA: 0x00043C3C File Offset: 0x00041E3C
		public EventLogEntry Entry
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
