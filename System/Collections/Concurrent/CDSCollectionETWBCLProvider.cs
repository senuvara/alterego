﻿using System;
using System.Diagnostics.Tracing;

namespace System.Collections.Concurrent
{
	// Token: 0x02000586 RID: 1414
	[EventSource(Name = "System.Collections.Concurrent.ConcurrentCollectionsEventSource", Guid = "35167F8E-49B2-4b96-AB86-435B59336B5E")]
	internal sealed class CDSCollectionETWBCLProvider : EventSource
	{
		// Token: 0x06002D11 RID: 11537 RVA: 0x0004DE29 File Offset: 0x0004C029
		private CDSCollectionETWBCLProvider()
		{
		}

		// Token: 0x06002D12 RID: 11538 RVA: 0x0009BED5 File Offset: 0x0009A0D5
		[Event(1, Level = EventLevel.Warning)]
		public void ConcurrentStack_FastPushFailed(int spinCount)
		{
			if (base.IsEnabled(EventLevel.Warning, EventKeywords.All))
			{
				base.WriteEvent(1, spinCount);
			}
		}

		// Token: 0x06002D13 RID: 11539 RVA: 0x0009BEEA File Offset: 0x0009A0EA
		[Event(2, Level = EventLevel.Warning)]
		public void ConcurrentStack_FastPopFailed(int spinCount)
		{
			if (base.IsEnabled(EventLevel.Warning, EventKeywords.All))
			{
				base.WriteEvent(2, spinCount);
			}
		}

		// Token: 0x06002D14 RID: 11540 RVA: 0x0009BEFF File Offset: 0x0009A0FF
		[Event(3, Level = EventLevel.Warning)]
		public void ConcurrentDictionary_AcquiringAllLocks(int numOfBuckets)
		{
			if (base.IsEnabled(EventLevel.Warning, EventKeywords.All))
			{
				base.WriteEvent(3, numOfBuckets);
			}
		}

		// Token: 0x06002D15 RID: 11541 RVA: 0x0009BF14 File Offset: 0x0009A114
		[Event(4, Level = EventLevel.Verbose)]
		public void ConcurrentBag_TryTakeSteals()
		{
			if (base.IsEnabled(EventLevel.Verbose, EventKeywords.All))
			{
				base.WriteEvent(4);
			}
		}

		// Token: 0x06002D16 RID: 11542 RVA: 0x0009BF28 File Offset: 0x0009A128
		[Event(5, Level = EventLevel.Verbose)]
		public void ConcurrentBag_TryPeekSteals()
		{
			if (base.IsEnabled(EventLevel.Verbose, EventKeywords.All))
			{
				base.WriteEvent(5);
			}
		}

		// Token: 0x06002D17 RID: 11543 RVA: 0x0009BF3C File Offset: 0x0009A13C
		// Note: this type is marked as 'beforefieldinit'.
		static CDSCollectionETWBCLProvider()
		{
		}

		// Token: 0x04002375 RID: 9077
		public static CDSCollectionETWBCLProvider Log = new CDSCollectionETWBCLProvider();

		// Token: 0x04002376 RID: 9078
		private const EventKeywords ALL_KEYWORDS = EventKeywords.All;

		// Token: 0x04002377 RID: 9079
		private const int CONCURRENTSTACK_FASTPUSHFAILED_ID = 1;

		// Token: 0x04002378 RID: 9080
		private const int CONCURRENTSTACK_FASTPOPFAILED_ID = 2;

		// Token: 0x04002379 RID: 9081
		private const int CONCURRENTDICTIONARY_ACQUIRINGALLLOCKS_ID = 3;

		// Token: 0x0400237A RID: 9082
		private const int CONCURRENTBAG_TRYTAKESTEALS_ID = 4;

		// Token: 0x0400237B RID: 9083
		private const int CONCURRENTBAG_TRYPEEKSTEALS_ID = 5;
	}
}
