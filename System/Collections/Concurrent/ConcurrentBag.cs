﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace System.Collections.Concurrent
{
	/// <summary>Represents a thread-safe, unordered collection of objects.</summary>
	/// <typeparam name="T">The type of the elements to be stored in the collection.</typeparam>
	// Token: 0x02000587 RID: 1415
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(IProducerConsumerCollectionDebugView<>))]
	[Serializable]
	public class ConcurrentBag<T> : IProducerConsumerCollection<T>, IEnumerable<!0>, IEnumerable, ICollection, IReadOnlyCollection<T>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" /> class.</summary>
		// Token: 0x06002D18 RID: 11544 RVA: 0x0009BF48 File Offset: 0x0009A148
		public ConcurrentBag()
		{
			this._locals = new ThreadLocal<ConcurrentBag<T>.WorkStealingQueue>();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" /> class that contains elements copied from the specified collection.</summary>
		/// <param name="collection">The collection whose elements are copied to the new <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="collection" /> is a null reference (Nothing in Visual Basic).</exception>
		// Token: 0x06002D19 RID: 11545 RVA: 0x0009BF5C File Offset: 0x0009A15C
		public ConcurrentBag(IEnumerable<T> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection", "The collection argument is null.");
			}
			this._locals = new ThreadLocal<ConcurrentBag<T>.WorkStealingQueue>();
			ConcurrentBag<T>.WorkStealingQueue currentThreadWorkStealingQueue = this.GetCurrentThreadWorkStealingQueue(true);
			foreach (T item in collection)
			{
				currentThreadWorkStealingQueue.LocalPush(item);
			}
		}

		/// <summary>Adds an object to the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />.</summary>
		/// <param name="item">The object to be added to the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />. The value can be a null reference (Nothing in Visual Basic) for reference types.</param>
		// Token: 0x06002D1A RID: 11546 RVA: 0x0009BFD0 File Offset: 0x0009A1D0
		public void Add(T item)
		{
			this.GetCurrentThreadWorkStealingQueue(true).LocalPush(item);
		}

		// Token: 0x06002D1B RID: 11547 RVA: 0x0009BFDF File Offset: 0x0009A1DF
		bool IProducerConsumerCollection<!0>.TryAdd(T item)
		{
			this.Add(item);
			return true;
		}

		/// <summary>Attempts to remove and return an object from the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />.</summary>
		/// <param name="result">When this method returns, <paramref name="result" /> contains the object removed from the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" /> or the default value of <paramref name="T" /> if the bag is empty.</param>
		/// <returns>true if an object was removed successfully; otherwise, false.</returns>
		// Token: 0x06002D1C RID: 11548 RVA: 0x0009BFEC File Offset: 0x0009A1EC
		public bool TryTake(out T result)
		{
			ConcurrentBag<T>.WorkStealingQueue currentThreadWorkStealingQueue = this.GetCurrentThreadWorkStealingQueue(false);
			return (currentThreadWorkStealingQueue != null && currentThreadWorkStealingQueue.TryLocalPop(out result)) || this.TrySteal(out result, true);
		}

		/// <summary>Attempts to return an object from the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" /> without removing it.</summary>
		/// <param name="result">When this method returns, <paramref name="result" /> contains an object from the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" /> or the default value of <paramref name="T" /> if the operation failed.</param>
		/// <returns>true if and object was returned successfully; otherwise, false.</returns>
		// Token: 0x06002D1D RID: 11549 RVA: 0x0009C018 File Offset: 0x0009A218
		public bool TryPeek(out T result)
		{
			ConcurrentBag<T>.WorkStealingQueue currentThreadWorkStealingQueue = this.GetCurrentThreadWorkStealingQueue(false);
			return (currentThreadWorkStealingQueue != null && currentThreadWorkStealingQueue.TryLocalPeek(out result)) || this.TrySteal(out result, false);
		}

		// Token: 0x06002D1E RID: 11550 RVA: 0x0009C043 File Offset: 0x0009A243
		private ConcurrentBag<T>.WorkStealingQueue GetCurrentThreadWorkStealingQueue(bool forceCreate)
		{
			ConcurrentBag<T>.WorkStealingQueue result;
			if ((result = this._locals.Value) == null)
			{
				if (!forceCreate)
				{
					return null;
				}
				result = this.CreateWorkStealingQueueForCurrentThread();
			}
			return result;
		}

		// Token: 0x06002D1F RID: 11551 RVA: 0x0009C060 File Offset: 0x0009A260
		private ConcurrentBag<T>.WorkStealingQueue CreateWorkStealingQueueForCurrentThread()
		{
			object globalQueuesLock = this.GlobalQueuesLock;
			ConcurrentBag<T>.WorkStealingQueue result;
			lock (globalQueuesLock)
			{
				ConcurrentBag<T>.WorkStealingQueue workStealingQueues = this._workStealingQueues;
				ConcurrentBag<T>.WorkStealingQueue workStealingQueue = (workStealingQueues != null) ? this.GetUnownedWorkStealingQueue() : null;
				if (workStealingQueue == null)
				{
					workStealingQueue = (this._workStealingQueues = new ConcurrentBag<T>.WorkStealingQueue(workStealingQueues));
				}
				this._locals.Value = workStealingQueue;
				result = workStealingQueue;
			}
			return result;
		}

		// Token: 0x06002D20 RID: 11552 RVA: 0x0009C0D4 File Offset: 0x0009A2D4
		private ConcurrentBag<T>.WorkStealingQueue GetUnownedWorkStealingQueue()
		{
			int currentManagedThreadId = Environment.CurrentManagedThreadId;
			for (ConcurrentBag<T>.WorkStealingQueue workStealingQueue = this._workStealingQueues; workStealingQueue != null; workStealingQueue = workStealingQueue._nextQueue)
			{
				if (workStealingQueue._ownerThreadId == currentManagedThreadId)
				{
					return workStealingQueue;
				}
			}
			return null;
		}

		// Token: 0x06002D21 RID: 11553 RVA: 0x0009C108 File Offset: 0x0009A308
		private bool TrySteal(out T result, bool take)
		{
			if (take)
			{
				CDSCollectionETWBCLProvider.Log.ConcurrentBag_TryTakeSteals();
			}
			else
			{
				CDSCollectionETWBCLProvider.Log.ConcurrentBag_TryPeekSteals();
			}
			ConcurrentBag<T>.WorkStealingQueue currentThreadWorkStealingQueue = this.GetCurrentThreadWorkStealingQueue(false);
			if (currentThreadWorkStealingQueue == null)
			{
				return this.TryStealFromTo(this._workStealingQueues, null, out result, take);
			}
			return this.TryStealFromTo(currentThreadWorkStealingQueue._nextQueue, null, out result, take) || this.TryStealFromTo(this._workStealingQueues, currentThreadWorkStealingQueue, out result, take);
		}

		// Token: 0x06002D22 RID: 11554 RVA: 0x0009C170 File Offset: 0x0009A370
		private bool TryStealFromTo(ConcurrentBag<T>.WorkStealingQueue startInclusive, ConcurrentBag<T>.WorkStealingQueue endExclusive, out T result, bool take)
		{
			for (ConcurrentBag<T>.WorkStealingQueue workStealingQueue = startInclusive; workStealingQueue != endExclusive; workStealingQueue = workStealingQueue._nextQueue)
			{
				if (workStealingQueue.TrySteal(out result, take))
				{
					return true;
				}
			}
			result = default(T);
			return false;
		}

		/// <summary>Copies the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" /> elements to an existing one-dimensional <see cref="T:System.Array" />, starting at the specified array index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is a null reference (Nothing in Visual Basic).</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="index" /> is equal to or greater than the length of the <paramref name="array" /> -or- the number of elements in the source <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" /> is greater than the available space from <paramref name="index" /> to the end of the destination <paramref name="array" />.</exception>
		// Token: 0x06002D23 RID: 11555 RVA: 0x0009C1A4 File Offset: 0x0009A3A4
		public void CopyTo(T[] array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array", "The array argument is null.");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", "The index argument must be greater than or equal zero.");
			}
			if (this._workStealingQueues == null)
			{
				return;
			}
			bool lockTaken = false;
			try
			{
				this.FreezeBag(ref lockTaken);
				int dangerousCount = this.DangerousCount;
				if (index > array.Length - dangerousCount)
				{
					throw new ArgumentException("The number of elements in the collection is greater than the available space from index to the end of the destination array.", "index");
				}
				try
				{
					this.CopyFromEachQueueToArray(array, index);
				}
				catch (ArrayTypeMismatchException ex)
				{
					throw new InvalidCastException(ex.Message, ex);
				}
			}
			finally
			{
				this.UnfreezeBag(lockTaken);
			}
		}

		// Token: 0x06002D24 RID: 11556 RVA: 0x0009C24C File Offset: 0x0009A44C
		private int CopyFromEachQueueToArray(T[] array, int index)
		{
			int num = index;
			for (ConcurrentBag<T>.WorkStealingQueue workStealingQueue = this._workStealingQueues; workStealingQueue != null; workStealingQueue = workStealingQueue._nextQueue)
			{
				num += workStealingQueue.DangerousCopyTo(array, num);
			}
			return num - index;
		}

		/// <summary>Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is a null reference (Nothing in Visual Basic).</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="array" /> is multidimensional. -or- <paramref name="array" /> does not have zero-based indexing. -or- <paramref name="index" /> is equal to or greater than the length of the <paramref name="array" /> -or- The number of elements in the source <see cref="T:System.Collections.ICollection" /> is greater than the available space from <paramref name="index" /> to the end of the destination <paramref name="array" />. -or- The type of the source <see cref="T:System.Collections.ICollection" /> cannot be cast automatically to the type of the destination <paramref name="array" />.</exception>
		// Token: 0x06002D25 RID: 11557 RVA: 0x0009C280 File Offset: 0x0009A480
		void ICollection.CopyTo(Array array, int index)
		{
			T[] array2 = array as T[];
			if (array2 != null)
			{
				this.CopyTo(array2, index);
				return;
			}
			if (array == null)
			{
				throw new ArgumentNullException("array", "The array argument is null.");
			}
			this.ToArray().CopyTo(array, index);
		}

		/// <summary>Copies the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" /> elements to a new array.</summary>
		/// <returns>A new array containing a snapshot of elements copied from the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />.</returns>
		// Token: 0x06002D26 RID: 11558 RVA: 0x0009C2C0 File Offset: 0x0009A4C0
		public T[] ToArray()
		{
			if (this._workStealingQueues != null)
			{
				bool lockTaken = false;
				try
				{
					this.FreezeBag(ref lockTaken);
					int dangerousCount = this.DangerousCount;
					if (dangerousCount > 0)
					{
						T[] array = new T[dangerousCount];
						this.CopyFromEachQueueToArray(array, 0);
						return array;
					}
				}
				finally
				{
					this.UnfreezeBag(lockTaken);
				}
			}
			return Array.Empty<T>();
		}

		// Token: 0x06002D27 RID: 11559 RVA: 0x0009C324 File Offset: 0x0009A524
		public void Clear()
		{
			if (this._workStealingQueues == null)
			{
				return;
			}
			ConcurrentBag<T>.WorkStealingQueue currentThreadWorkStealingQueue = this.GetCurrentThreadWorkStealingQueue(false);
			if (currentThreadWorkStealingQueue != null)
			{
				currentThreadWorkStealingQueue.LocalClear();
				if (currentThreadWorkStealingQueue._nextQueue == null && currentThreadWorkStealingQueue == this._workStealingQueues)
				{
					return;
				}
			}
			bool lockTaken = false;
			try
			{
				this.FreezeBag(ref lockTaken);
				for (ConcurrentBag<T>.WorkStealingQueue workStealingQueue = this._workStealingQueues; workStealingQueue != null; workStealingQueue = workStealingQueue._nextQueue)
				{
					T t;
					while (workStealingQueue.TrySteal(out t, true))
					{
					}
				}
			}
			finally
			{
				this.UnfreezeBag(lockTaken);
			}
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />.</summary>
		/// <returns>An enumerator for the contents of the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />.</returns>
		// Token: 0x06002D28 RID: 11560 RVA: 0x0009C3A8 File Offset: 0x0009A5A8
		public IEnumerator<T> GetEnumerator()
		{
			return new ConcurrentBag<T>.Enumerator(this.ToArray());
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />.</summary>
		/// <returns>An enumerator for the contents of the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />.</returns>
		// Token: 0x06002D29 RID: 11561 RVA: 0x0009C3B5 File Offset: 0x0009A5B5
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		/// <summary>Gets the number of elements contained in the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />.</summary>
		/// <returns>The number of elements contained in the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />.</returns>
		// Token: 0x17000B39 RID: 2873
		// (get) Token: 0x06002D2A RID: 11562 RVA: 0x0009C3C0 File Offset: 0x0009A5C0
		public int Count
		{
			get
			{
				if (this._workStealingQueues == null)
				{
					return 0;
				}
				bool lockTaken = false;
				int dangerousCount;
				try
				{
					this.FreezeBag(ref lockTaken);
					dangerousCount = this.DangerousCount;
				}
				finally
				{
					this.UnfreezeBag(lockTaken);
				}
				return dangerousCount;
			}
		}

		// Token: 0x17000B3A RID: 2874
		// (get) Token: 0x06002D2B RID: 11563 RVA: 0x0009C408 File Offset: 0x0009A608
		private int DangerousCount
		{
			get
			{
				int num = 0;
				checked
				{
					for (ConcurrentBag<T>.WorkStealingQueue workStealingQueue = this._workStealingQueues; workStealingQueue != null; workStealingQueue = workStealingQueue._nextQueue)
					{
						num += workStealingQueue.DangerousCount;
					}
					return num;
				}
			}
		}

		/// <summary>Gets a value that indicates whether the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" /> is empty.</summary>
		/// <returns>true if the <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" /> is empty; otherwise, false.</returns>
		// Token: 0x17000B3B RID: 2875
		// (get) Token: 0x06002D2C RID: 11564 RVA: 0x0009C438 File Offset: 0x0009A638
		public bool IsEmpty
		{
			get
			{
				ConcurrentBag<T>.WorkStealingQueue currentThreadWorkStealingQueue = this.GetCurrentThreadWorkStealingQueue(false);
				if (currentThreadWorkStealingQueue != null)
				{
					if (!currentThreadWorkStealingQueue.IsEmpty)
					{
						return false;
					}
					if (currentThreadWorkStealingQueue._nextQueue == null && currentThreadWorkStealingQueue == this._workStealingQueues)
					{
						return true;
					}
				}
				bool lockTaken = false;
				try
				{
					this.FreezeBag(ref lockTaken);
					for (ConcurrentBag<T>.WorkStealingQueue workStealingQueue = this._workStealingQueues; workStealingQueue != null; workStealingQueue = workStealingQueue._nextQueue)
					{
						if (!workStealingQueue.IsEmpty)
						{
							return false;
						}
					}
				}
				finally
				{
					this.UnfreezeBag(lockTaken);
				}
				return true;
			}
		}

		/// <summary>Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized with the SyncRoot.</summary>
		/// <returns>true if access to the <see cref="T:System.Collections.ICollection" /> is synchronized with the SyncRoot; otherwise, false. For <see cref="T:System.Collections.Concurrent.ConcurrentBag`1" />, this property always returns false.</returns>
		// Token: 0x17000B3C RID: 2876
		// (get) Token: 0x06002D2D RID: 11565 RVA: 0x00005AFA File Offset: 0x00003CFA
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />. This property is not supported.</summary>
		/// <returns>Returns null  (Nothing in Visual Basic).</returns>
		/// <exception cref="T:System.NotSupportedException">The SyncRoot property is not supported.</exception>
		// Token: 0x17000B3D RID: 2877
		// (get) Token: 0x06002D2E RID: 11566 RVA: 0x0009AFBF File Offset: 0x000991BF
		object ICollection.SyncRoot
		{
			get
			{
				throw new NotSupportedException("The SyncRoot property may not be used for the synchronization of concurrent collections.");
			}
		}

		// Token: 0x17000B3E RID: 2878
		// (get) Token: 0x06002D2F RID: 11567 RVA: 0x0009C4B8 File Offset: 0x0009A6B8
		private object GlobalQueuesLock
		{
			get
			{
				return this._locals;
			}
		}

		// Token: 0x06002D30 RID: 11568 RVA: 0x0009C4C0 File Offset: 0x0009A6C0
		private void FreezeBag(ref bool lockTaken)
		{
			Monitor.Enter(this.GlobalQueuesLock, ref lockTaken);
			ConcurrentBag<T>.WorkStealingQueue workStealingQueues = this._workStealingQueues;
			for (ConcurrentBag<T>.WorkStealingQueue workStealingQueue = workStealingQueues; workStealingQueue != null; workStealingQueue = workStealingQueue._nextQueue)
			{
				Monitor.Enter(workStealingQueue, ref workStealingQueue._frozen);
			}
			Interlocked.MemoryBarrier();
			for (ConcurrentBag<T>.WorkStealingQueue workStealingQueue2 = workStealingQueues; workStealingQueue2 != null; workStealingQueue2 = workStealingQueue2._nextQueue)
			{
				if (workStealingQueue2._currentOp != 0)
				{
					SpinWait spinWait = default(SpinWait);
					do
					{
						spinWait.SpinOnce();
					}
					while (workStealingQueue2._currentOp != 0);
				}
			}
		}

		// Token: 0x06002D31 RID: 11569 RVA: 0x0009C534 File Offset: 0x0009A734
		private void UnfreezeBag(bool lockTaken)
		{
			if (lockTaken)
			{
				for (ConcurrentBag<T>.WorkStealingQueue workStealingQueue = this._workStealingQueues; workStealingQueue != null; workStealingQueue = workStealingQueue._nextQueue)
				{
					if (workStealingQueue._frozen)
					{
						workStealingQueue._frozen = false;
						Monitor.Exit(workStealingQueue);
					}
				}
				Monitor.Exit(this.GlobalQueuesLock);
			}
		}

		// Token: 0x0400237C RID: 9084
		private ThreadLocal<ConcurrentBag<T>.WorkStealingQueue> _locals;

		// Token: 0x0400237D RID: 9085
		private volatile ConcurrentBag<T>.WorkStealingQueue _workStealingQueues;

		// Token: 0x02000588 RID: 1416
		private sealed class WorkStealingQueue
		{
			// Token: 0x06002D32 RID: 11570 RVA: 0x0009C579 File Offset: 0x0009A779
			internal WorkStealingQueue(ConcurrentBag<T>.WorkStealingQueue nextQueue)
			{
				this._ownerThreadId = Environment.CurrentManagedThreadId;
				this._nextQueue = nextQueue;
			}

			// Token: 0x17000B3F RID: 2879
			// (get) Token: 0x06002D33 RID: 11571 RVA: 0x0009C5AC File Offset: 0x0009A7AC
			internal bool IsEmpty
			{
				get
				{
					return this._headIndex >= this._tailIndex;
				}
			}

			// Token: 0x06002D34 RID: 11572 RVA: 0x0009C5C4 File Offset: 0x0009A7C4
			internal void LocalPush(T item)
			{
				bool flag = false;
				try
				{
					Interlocked.Exchange(ref this._currentOp, 1);
					int num = this._tailIndex;
					if (num == 2147483647)
					{
						this._currentOp = 0;
						lock (this)
						{
							this._headIndex &= this._mask;
							num = (this._tailIndex &= this._mask);
							this._currentOp = 1;
						}
					}
					if (!this._frozen && num < this._headIndex + this._mask)
					{
						this._array[num & this._mask] = item;
						this._tailIndex = num + 1;
					}
					else
					{
						this._currentOp = 0;
						Monitor.Enter(this, ref flag);
						int headIndex = this._headIndex;
						int num2 = this._tailIndex - this._headIndex;
						if (num2 >= this._mask)
						{
							T[] array = new T[this._array.Length << 1];
							int num3 = headIndex & this._mask;
							if (num3 == 0)
							{
								Array.Copy(this._array, 0, array, 0, this._array.Length);
							}
							else
							{
								Array.Copy(this._array, num3, array, 0, this._array.Length - num3);
								Array.Copy(this._array, 0, array, this._array.Length - num3, num3);
							}
							this._array = array;
							this._headIndex = 0;
							num = (this._tailIndex = num2);
							this._mask = (this._mask << 1 | 1);
						}
						this._array[num & this._mask] = item;
						this._tailIndex = num + 1;
						this._addTakeCount -= this._stealCount;
						this._stealCount = 0;
					}
					checked
					{
						this._addTakeCount++;
					}
				}
				finally
				{
					this._currentOp = 0;
					if (flag)
					{
						Monitor.Exit(this);
					}
				}
			}

			// Token: 0x06002D35 RID: 11573 RVA: 0x0009C818 File Offset: 0x0009AA18
			internal void LocalClear()
			{
				lock (this)
				{
					if (this._headIndex < this._tailIndex)
					{
						this._headIndex = (this._tailIndex = 0);
						this._addTakeCount = (this._stealCount = 0);
						Array.Clear(this._array, 0, this._array.Length);
					}
				}
			}

			// Token: 0x06002D36 RID: 11574 RVA: 0x0009C89C File Offset: 0x0009AA9C
			internal bool TryLocalPop(out T result)
			{
				int num = this._tailIndex;
				if (this._headIndex >= num)
				{
					result = default(T);
					return false;
				}
				bool flag = false;
				bool result2;
				try
				{
					this._currentOp = 2;
					Interlocked.Exchange(ref this._tailIndex, --num);
					if (!this._frozen && this._headIndex < num)
					{
						int num2 = num & this._mask;
						result = this._array[num2];
						this._array[num2] = default(T);
						this._addTakeCount--;
						result2 = true;
					}
					else
					{
						this._currentOp = 0;
						Monitor.Enter(this, ref flag);
						if (this._headIndex <= num)
						{
							int num3 = num & this._mask;
							result = this._array[num3];
							this._array[num3] = default(T);
							this._addTakeCount--;
							result2 = true;
						}
						else
						{
							this._tailIndex = num + 1;
							result = default(T);
							result2 = false;
						}
					}
				}
				finally
				{
					this._currentOp = 0;
					if (flag)
					{
						Monitor.Exit(this);
					}
				}
				return result2;
			}

			// Token: 0x06002D37 RID: 11575 RVA: 0x0009C9E8 File Offset: 0x0009ABE8
			internal bool TryLocalPeek(out T result)
			{
				int tailIndex = this._tailIndex;
				if (this._headIndex < tailIndex)
				{
					lock (this)
					{
						if (this._headIndex < tailIndex)
						{
							result = this._array[tailIndex - 1 & this._mask];
							return true;
						}
					}
				}
				result = default(T);
				return false;
			}

			// Token: 0x06002D38 RID: 11576 RVA: 0x0009CA6C File Offset: 0x0009AC6C
			internal bool TrySteal(out T result, bool take)
			{
				if (this._headIndex < this._tailIndex)
				{
					lock (this)
					{
						int headIndex = this._headIndex;
						if (take)
						{
							Interlocked.Exchange(ref this._headIndex, headIndex + 1);
							if (headIndex < this._tailIndex)
							{
								int num = headIndex & this._mask;
								result = this._array[num];
								this._array[num] = default(T);
								this._stealCount++;
								return true;
							}
							this._headIndex = headIndex;
						}
						else if (headIndex < this._tailIndex)
						{
							result = this._array[headIndex & this._mask];
							return true;
						}
					}
				}
				result = default(T);
				return false;
			}

			// Token: 0x06002D39 RID: 11577 RVA: 0x0009CB6C File Offset: 0x0009AD6C
			internal int DangerousCopyTo(T[] array, int arrayIndex)
			{
				int headIndex = this._headIndex;
				int dangerousCount = this.DangerousCount;
				for (int i = arrayIndex + dangerousCount - 1; i >= arrayIndex; i--)
				{
					array[i] = this._array[headIndex++ & this._mask];
				}
				return dangerousCount;
			}

			// Token: 0x17000B40 RID: 2880
			// (get) Token: 0x06002D3A RID: 11578 RVA: 0x0009CBBC File Offset: 0x0009ADBC
			internal int DangerousCount
			{
				get
				{
					return this._addTakeCount - this._stealCount;
				}
			}

			// Token: 0x0400237E RID: 9086
			private const int InitialSize = 32;

			// Token: 0x0400237F RID: 9087
			private const int StartIndex = 0;

			// Token: 0x04002380 RID: 9088
			private volatile int _headIndex;

			// Token: 0x04002381 RID: 9089
			private volatile int _tailIndex;

			// Token: 0x04002382 RID: 9090
			private volatile T[] _array = new T[32];

			// Token: 0x04002383 RID: 9091
			private volatile int _mask = 31;

			// Token: 0x04002384 RID: 9092
			private int _addTakeCount;

			// Token: 0x04002385 RID: 9093
			private int _stealCount;

			// Token: 0x04002386 RID: 9094
			internal volatile int _currentOp;

			// Token: 0x04002387 RID: 9095
			internal bool _frozen;

			// Token: 0x04002388 RID: 9096
			internal readonly ConcurrentBag<T>.WorkStealingQueue _nextQueue;

			// Token: 0x04002389 RID: 9097
			internal readonly int _ownerThreadId;
		}

		// Token: 0x02000589 RID: 1417
		internal enum Operation
		{
			// Token: 0x0400238B RID: 9099
			None,
			// Token: 0x0400238C RID: 9100
			Add,
			// Token: 0x0400238D RID: 9101
			Take
		}

		// Token: 0x0200058A RID: 1418
		[Serializable]
		private sealed class Enumerator : IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06002D3B RID: 11579 RVA: 0x0009CBCB File Offset: 0x0009ADCB
			public Enumerator(T[] array)
			{
				this._array = array;
			}

			// Token: 0x06002D3C RID: 11580 RVA: 0x0009CBDC File Offset: 0x0009ADDC
			public bool MoveNext()
			{
				if (this._index < this._array.Length)
				{
					T[] array = this._array;
					int index = this._index;
					this._index = index + 1;
					this._current = array[index];
					return true;
				}
				this._index = this._array.Length + 1;
				return false;
			}

			// Token: 0x17000B41 RID: 2881
			// (get) Token: 0x06002D3D RID: 11581 RVA: 0x0009CC2E File Offset: 0x0009AE2E
			public T Current
			{
				get
				{
					return this._current;
				}
			}

			// Token: 0x17000B42 RID: 2882
			// (get) Token: 0x06002D3E RID: 11582 RVA: 0x0009CC36 File Offset: 0x0009AE36
			object IEnumerator.Current
			{
				get
				{
					if (this._index == 0 || this._index == this._array.Length + 1)
					{
						throw new InvalidOperationException("Enumeration has either not started or has already finished.");
					}
					return this.Current;
				}
			}

			// Token: 0x06002D3F RID: 11583 RVA: 0x0009CC68 File Offset: 0x0009AE68
			public void Reset()
			{
				this._index = 0;
				this._current = default(T);
			}

			// Token: 0x06002D40 RID: 11584 RVA: 0x0000232D File Offset: 0x0000052D
			public void Dispose()
			{
			}

			// Token: 0x0400238E RID: 9102
			private readonly T[] _array;

			// Token: 0x0400238F RID: 9103
			private T _current;

			// Token: 0x04002390 RID: 9104
			private int _index;
		}
	}
}
