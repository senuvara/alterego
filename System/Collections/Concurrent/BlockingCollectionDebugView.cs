﻿using System;
using System.Diagnostics;

namespace System.Collections.Concurrent
{
	// Token: 0x02000585 RID: 1413
	internal sealed class BlockingCollectionDebugView<T>
	{
		// Token: 0x06002D0F RID: 11535 RVA: 0x0009BEAB File Offset: 0x0009A0AB
		public BlockingCollectionDebugView(BlockingCollection<T> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			this._blockingCollection = collection;
		}

		// Token: 0x17000B38 RID: 2872
		// (get) Token: 0x06002D10 RID: 11536 RVA: 0x0009BEC8 File Offset: 0x0009A0C8
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				return this._blockingCollection.ToArray();
			}
		}

		// Token: 0x04002374 RID: 9076
		private readonly BlockingCollection<T> _blockingCollection;
	}
}
