﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x020005BF RID: 1471
	internal sealed class SortedSetEqualityComparer<T> : IEqualityComparer<SortedSet<T>>
	{
		// Token: 0x06002F45 RID: 12101 RVA: 0x000A335B File Offset: 0x000A155B
		public SortedSetEqualityComparer(IEqualityComparer<T> memberEqualityComparer) : this(null, memberEqualityComparer)
		{
		}

		// Token: 0x06002F46 RID: 12102 RVA: 0x000A3365 File Offset: 0x000A1565
		private SortedSetEqualityComparer(IComparer<T> comparer, IEqualityComparer<T> memberEqualityComparer)
		{
			this._comparer = (comparer ?? Comparer<T>.Default);
			this._memberEqualityComparer = (memberEqualityComparer ?? EqualityComparer<T>.Default);
		}

		// Token: 0x06002F47 RID: 12103 RVA: 0x000A338D File Offset: 0x000A158D
		public bool Equals(SortedSet<T> x, SortedSet<T> y)
		{
			return SortedSet<T>.SortedSetEquals(x, y, this._comparer);
		}

		// Token: 0x06002F48 RID: 12104 RVA: 0x000A339C File Offset: 0x000A159C
		public int GetHashCode(SortedSet<T> obj)
		{
			int num = 0;
			if (obj != null)
			{
				foreach (T obj2 in obj)
				{
					num ^= (this._memberEqualityComparer.GetHashCode(obj2) & int.MaxValue);
				}
			}
			return num;
		}

		// Token: 0x06002F49 RID: 12105 RVA: 0x000A3400 File Offset: 0x000A1600
		public override bool Equals(object obj)
		{
			SortedSetEqualityComparer<T> sortedSetEqualityComparer = obj as SortedSetEqualityComparer<T>;
			return sortedSetEqualityComparer != null && this._comparer == sortedSetEqualityComparer._comparer;
		}

		// Token: 0x06002F4A RID: 12106 RVA: 0x000A3427 File Offset: 0x000A1627
		public override int GetHashCode()
		{
			return this._comparer.GetHashCode() ^ this._memberEqualityComparer.GetHashCode();
		}

		// Token: 0x0400243F RID: 9279
		private readonly IComparer<T> _comparer;

		// Token: 0x04002440 RID: 9280
		private readonly IEqualityComparer<T> _memberEqualityComparer;
	}
}
