﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x020005BC RID: 1468
	internal enum NodeColor : byte
	{
		// Token: 0x04002438 RID: 9272
		Black,
		// Token: 0x04002439 RID: 9273
		Red
	}
}
