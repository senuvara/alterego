﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x020005BD RID: 1469
	// (Invoke) Token: 0x06002F42 RID: 12098
	internal delegate bool TreeWalkPredicate<T>(SortedSet<T>.Node node);
}
