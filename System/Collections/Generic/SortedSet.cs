﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security;
using System.Threading;

namespace System.Collections.Generic
{
	/// <summary>Represents a collection of objects that is maintained in sorted order.</summary>
	/// <typeparam name="T">The type of elements in the set.</typeparam>
	// Token: 0x020005B1 RID: 1457
	[DebuggerTypeProxy(typeof(ICollectionDebugView<>))]
	[DebuggerDisplay("Count = {Count}")]
	[Serializable]
	public class SortedSet<T> : ISet<T>, ICollection<!0>, IEnumerable<!0>, IEnumerable, ICollection, IReadOnlyCollection<T>, ISerializable, IDeserializationCallback
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Generic.SortedSet`1" /> class. </summary>
		// Token: 0x06002EA9 RID: 11945 RVA: 0x000A09EE File Offset: 0x0009EBEE
		public SortedSet()
		{
			this.comparer = Comparer<T>.Default;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Generic.SortedSet`1" /> class that uses a specified comparer.</summary>
		/// <param name="comparer">The default comparer to use for comparing objects. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="comparer" /> is <see langword="null" />.</exception>
		// Token: 0x06002EAA RID: 11946 RVA: 0x000A0A01 File Offset: 0x0009EC01
		public SortedSet(IComparer<T> comparer)
		{
			this.comparer = (comparer ?? Comparer<T>.Default);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Generic.SortedSet`1" /> class that contains elements copied from a specified enumerable collection.</summary>
		/// <param name="collection">The enumerable collection to be copied. </param>
		// Token: 0x06002EAB RID: 11947 RVA: 0x000A0A19 File Offset: 0x0009EC19
		public SortedSet(IEnumerable<T> collection) : this(collection, Comparer<T>.Default)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Generic.SortedSet`1" /> class that contains elements copied from a specified enumerable collection and that uses a specified comparer.</summary>
		/// <param name="collection">The enumerable collection to be copied. </param>
		/// <param name="comparer">The default comparer to use for comparing objects. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="collection" /> is <see langword="null" />.</exception>
		// Token: 0x06002EAC RID: 11948 RVA: 0x000A0A28 File Offset: 0x0009EC28
		public SortedSet(IEnumerable<T> collection, IComparer<T> comparer) : this(comparer)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			SortedSet<T> sortedSet = collection as SortedSet<T>;
			if (sortedSet != null && !(sortedSet is SortedSet<T>.TreeSubSet) && this.HasEqualComparer(sortedSet))
			{
				if (sortedSet.Count > 0)
				{
					this.count = sortedSet.count;
					this.root = sortedSet.root.DeepClone(this.count);
				}
				return;
			}
			int num;
			T[] array = EnumerableHelpers.ToArray<T>(collection, out num);
			if (num > 0)
			{
				comparer = this.comparer;
				Array.Sort<T>(array, 0, num, comparer);
				int num2 = 1;
				for (int i = 1; i < num; i++)
				{
					if (comparer.Compare(array[i], array[i - 1]) != 0)
					{
						array[num2++] = array[i];
					}
				}
				num = num2;
				this.root = SortedSet<T>.ConstructRootFromSortedArray(array, 0, num - 1, null);
				this.count = num;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Generic.SortedSet`1" /> class that contains serialized data.</summary>
		/// <param name="info">The object that contains the information that is required to serialize the <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <param name="context">The structure that contains the source and destination of the serialized stream associated with the <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		// Token: 0x06002EAD RID: 11949 RVA: 0x000A0B09 File Offset: 0x0009ED09
		protected SortedSet(SerializationInfo info, StreamingContext context)
		{
			this.siInfo = info;
		}

		// Token: 0x06002EAE RID: 11950 RVA: 0x000A0B18 File Offset: 0x0009ED18
		private void AddAllElements(IEnumerable<T> collection)
		{
			foreach (T item in collection)
			{
				if (!this.Contains(item))
				{
					this.Add(item);
				}
			}
		}

		// Token: 0x06002EAF RID: 11951 RVA: 0x000A0B6C File Offset: 0x0009ED6C
		private void RemoveAllElements(IEnumerable<T> collection)
		{
			T min = this.Min;
			T max = this.Max;
			foreach (T t in collection)
			{
				if (this.comparer.Compare(t, min) >= 0 && this.comparer.Compare(t, max) <= 0 && this.Contains(t))
				{
					this.Remove(t);
				}
			}
		}

		// Token: 0x06002EB0 RID: 11952 RVA: 0x000A0BEC File Offset: 0x0009EDEC
		private bool ContainsAllElements(IEnumerable<T> collection)
		{
			foreach (T item in collection)
			{
				if (!this.Contains(item))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06002EB1 RID: 11953 RVA: 0x000A0C40 File Offset: 0x0009EE40
		internal virtual bool InOrderTreeWalk(TreeWalkPredicate<T> action)
		{
			if (this.root == null)
			{
				return true;
			}
			Stack<SortedSet<T>.Node> stack = new Stack<SortedSet<T>.Node>(2 * SortedSet<T>.Log2(this.Count + 1));
			for (SortedSet<T>.Node node = this.root; node != null; node = node.Left)
			{
				stack.Push(node);
			}
			while (stack.Count != 0)
			{
				SortedSet<T>.Node node = stack.Pop();
				if (!action(node))
				{
					return false;
				}
				for (SortedSet<T>.Node node2 = node.Right; node2 != null; node2 = node2.Left)
				{
					stack.Push(node2);
				}
			}
			return true;
		}

		// Token: 0x06002EB2 RID: 11954 RVA: 0x000A0CC0 File Offset: 0x0009EEC0
		internal virtual bool BreadthFirstTreeWalk(TreeWalkPredicate<T> action)
		{
			if (this.root == null)
			{
				return true;
			}
			Queue<SortedSet<T>.Node> queue = new Queue<SortedSet<T>.Node>();
			queue.Enqueue(this.root);
			while (queue.Count != 0)
			{
				SortedSet<T>.Node node = queue.Dequeue();
				if (!action(node))
				{
					return false;
				}
				if (node.Left != null)
				{
					queue.Enqueue(node.Left);
				}
				if (node.Right != null)
				{
					queue.Enqueue(node.Right);
				}
			}
			return true;
		}

		/// <summary>Gets the number of elements in the <see cref="T:System.Collections.Generic.SortedSet`1" />.</summary>
		/// <returns>The number of elements in the <see cref="T:System.Collections.Generic.SortedSet`1" />.</returns>
		// Token: 0x17000BA9 RID: 2985
		// (get) Token: 0x06002EB3 RID: 11955 RVA: 0x000A0D2E File Offset: 0x0009EF2E
		public int Count
		{
			get
			{
				this.VersionCheck();
				return this.count;
			}
		}

		/// <summary>Gets the <see cref="T:System.Collections.Generic.IComparer`1" /> object that is used to order the values in the <see cref="T:System.Collections.Generic.SortedSet`1" />.</summary>
		/// <returns>The comparer that is used to order the values in the <see cref="T:System.Collections.Generic.SortedSet`1" />.</returns>
		// Token: 0x17000BAA RID: 2986
		// (get) Token: 0x06002EB4 RID: 11956 RVA: 0x000A0D3C File Offset: 0x0009EF3C
		public IComparer<T> Comparer
		{
			get
			{
				return this.comparer;
			}
		}

		// Token: 0x17000BAB RID: 2987
		// (get) Token: 0x06002EB5 RID: 11957 RVA: 0x00005AFA File Offset: 0x00003CFA
		bool ICollection<!0>.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value that indicates whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).</summary>
		/// <returns>
		///     <see langword="true" /> if access to the <see cref="T:System.Collections.ICollection" /> is synchronized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000BAC RID: 2988
		// (get) Token: 0x06002EB6 RID: 11958 RVA: 0x00005AFA File Offset: 0x00003CFA
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.</summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />. In the default implementation of <see cref="T:System.Collections.Generic.Dictionary`2.KeyCollection" />, this property always returns the current instance.</returns>
		// Token: 0x17000BAD RID: 2989
		// (get) Token: 0x06002EB7 RID: 11959 RVA: 0x000A0D44 File Offset: 0x0009EF44
		object ICollection.SyncRoot
		{
			get
			{
				if (this._syncRoot == null)
				{
					Interlocked.CompareExchange(ref this._syncRoot, new object(), null);
				}
				return this._syncRoot;
			}
		}

		// Token: 0x06002EB8 RID: 11960 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void VersionCheck()
		{
		}

		// Token: 0x06002EB9 RID: 11961 RVA: 0x00003298 File Offset: 0x00001498
		internal virtual bool IsWithinRange(T item)
		{
			return true;
		}

		/// <summary>Adds an element to the set and returns a value that indicates if it was successfully added.</summary>
		/// <param name="item">The element to add to the set.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="item" /> is added to the set; otherwise, <see langword="false" />. </returns>
		// Token: 0x06002EBA RID: 11962 RVA: 0x000A0D66 File Offset: 0x0009EF66
		public bool Add(T item)
		{
			return this.AddIfNotPresent(item);
		}

		// Token: 0x06002EBB RID: 11963 RVA: 0x000A0D6F File Offset: 0x0009EF6F
		void ICollection<!0>.Add(T item)
		{
			this.Add(item);
		}

		// Token: 0x06002EBC RID: 11964 RVA: 0x000A0D7C File Offset: 0x0009EF7C
		internal virtual bool AddIfNotPresent(T item)
		{
			if (this.root == null)
			{
				this.root = new SortedSet<T>.Node(item, NodeColor.Black);
				this.count = 1;
				this.version++;
				return true;
			}
			SortedSet<T>.Node node = this.root;
			SortedSet<T>.Node node2 = null;
			SortedSet<T>.Node node3 = null;
			SortedSet<T>.Node greatGrandParent = null;
			this.version++;
			int num = 0;
			while (node != null)
			{
				num = this.comparer.Compare(item, node.Item);
				if (num == 0)
				{
					this.root.ColorBlack();
					return false;
				}
				if (node.Is4Node)
				{
					node.Split4Node();
					if (SortedSet<T>.Node.IsNonNullRed(node2))
					{
						this.InsertionBalance(node, ref node2, node3, greatGrandParent);
					}
				}
				greatGrandParent = node3;
				node3 = node2;
				node2 = node;
				node = ((num < 0) ? node.Left : node.Right);
			}
			SortedSet<T>.Node node4 = new SortedSet<T>.Node(item, NodeColor.Red);
			if (num > 0)
			{
				node2.Right = node4;
			}
			else
			{
				node2.Left = node4;
			}
			if (node2.IsRed)
			{
				this.InsertionBalance(node4, ref node2, node3, greatGrandParent);
			}
			this.root.ColorBlack();
			this.count++;
			return true;
		}

		/// <summary>Removes a specified item from the <see cref="T:System.Collections.Generic.SortedSet`1" />.</summary>
		/// <param name="item">The element to remove.</param>
		/// <returns>
		///     <see langword="true" /> if the element is found and successfully removed; otherwise, <see langword="false" />. </returns>
		// Token: 0x06002EBD RID: 11965 RVA: 0x000A0E86 File Offset: 0x0009F086
		public bool Remove(T item)
		{
			return this.DoRemove(item);
		}

		// Token: 0x06002EBE RID: 11966 RVA: 0x000A0E90 File Offset: 0x0009F090
		internal virtual bool DoRemove(T item)
		{
			if (this.root == null)
			{
				return false;
			}
			this.version++;
			SortedSet<T>.Node node = this.root;
			SortedSet<T>.Node node2 = null;
			SortedSet<T>.Node node3 = null;
			SortedSet<T>.Node node4 = null;
			SortedSet<T>.Node parentOfMatch = null;
			bool flag = false;
			while (node != null)
			{
				if (node.Is2Node)
				{
					if (node2 == null)
					{
						node.ColorRed();
					}
					else
					{
						SortedSet<T>.Node sibling = node2.GetSibling(node);
						if (sibling.IsRed)
						{
							if (node2.Right == sibling)
							{
								node2.RotateLeft();
							}
							else
							{
								node2.RotateRight();
							}
							node2.ColorRed();
							sibling.ColorBlack();
							this.ReplaceChildOrRoot(node3, node2, sibling);
							node3 = sibling;
							if (node2 == node4)
							{
								parentOfMatch = sibling;
							}
							sibling = node2.GetSibling(node);
						}
						if (sibling.Is2Node)
						{
							node2.Merge2Nodes();
						}
						else
						{
							SortedSet<T>.Node node5 = node2.Rotate(node2.GetRotation(node, sibling));
							node5.Color = node2.Color;
							node2.ColorBlack();
							node.ColorRed();
							this.ReplaceChildOrRoot(node3, node2, node5);
							if (node2 == node4)
							{
								parentOfMatch = node5;
							}
						}
					}
				}
				int num = flag ? -1 : this.comparer.Compare(item, node.Item);
				if (num == 0)
				{
					flag = true;
					node4 = node;
					parentOfMatch = node2;
				}
				node3 = node2;
				node2 = node;
				node = ((num < 0) ? node.Left : node.Right);
			}
			if (node4 != null)
			{
				this.ReplaceNode(node4, parentOfMatch, node2, node3);
				this.count--;
			}
			SortedSet<T>.Node node6 = this.root;
			if (node6 != null)
			{
				node6.ColorBlack();
			}
			return flag;
		}

		/// <summary>Removes all elements from the set.</summary>
		// Token: 0x06002EBF RID: 11967 RVA: 0x000A0FFC File Offset: 0x0009F1FC
		public virtual void Clear()
		{
			this.root = null;
			this.count = 0;
			this.version++;
		}

		/// <summary>Determines whether the set contains a specific element.</summary>
		/// <param name="item">The element to locate in the set.</param>
		/// <returns>
		///     <see langword="true" /> if the set contains <paramref name="item" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002EC0 RID: 11968 RVA: 0x000A101A File Offset: 0x0009F21A
		public virtual bool Contains(T item)
		{
			return this.FindNode(item) != null;
		}

		/// <summary>Copies the complete <see cref="T:System.Collections.Generic.SortedSet`1" /> to a compatible one-dimensional array, starting at the beginning of the target array.</summary>
		/// <param name="array">A one-dimensional array that is the destination of the elements copied from the <see cref="T:System.Collections.Generic.SortedSet`1" />.</param>
		/// <exception cref="T:System.ArgumentException">The number of elements in the source <see cref="T:System.Collections.Generic.SortedSet`1" /> exceeds the number of elements that the destination array can contain. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		// Token: 0x06002EC1 RID: 11969 RVA: 0x000A1026 File Offset: 0x0009F226
		public void CopyTo(T[] array)
		{
			this.CopyTo(array, 0, this.Count);
		}

		/// <summary>Copies the complete <see cref="T:System.Collections.Generic.SortedSet`1" /> to a compatible one-dimensional array, starting at the specified array index.</summary>
		/// <param name="array">A one-dimensional array that is the destination of the elements copied from the <see cref="T:System.Collections.Generic.SortedSet`1" />. The array must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		/// <exception cref="T:System.ArgumentException">The number of elements in the source array is greater than the available space from <paramref name="index" /> to the end of the destination array.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.</exception>
		// Token: 0x06002EC2 RID: 11970 RVA: 0x000A1036 File Offset: 0x0009F236
		public void CopyTo(T[] array, int index)
		{
			this.CopyTo(array, index, this.Count);
		}

		/// <summary>Copies a specified number of elements from <see cref="T:System.Collections.Generic.SortedSet`1" /> to a compatible one-dimensional array, starting at the specified array index.</summary>
		/// <param name="array">A one-dimensional array that is the destination of the elements copied from the <see cref="T:System.Collections.Generic.SortedSet`1" />. The array must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		/// <param name="count">The number of elements to copy.</param>
		/// <exception cref="T:System.ArgumentException">The number of elements in the source array is greater than the available space from <paramref name="index" /> to the end of the destination array.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.-or-
		///         <paramref name="count" /> is less than zero.</exception>
		// Token: 0x06002EC3 RID: 11971 RVA: 0x000A1048 File Offset: 0x0009F248
		public void CopyTo(T[] array, int index, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", index, "Non-negative number required.");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "Non-negative number required.");
			}
			if (count > array.Length - index)
			{
				throw new ArgumentException("Destination array is not long enough to copy all the items in the collection. Check array index and length.");
			}
			count += index;
			this.InOrderTreeWalk(delegate(SortedSet<T>.Node node)
			{
				if (index >= count)
				{
					return false;
				}
				T[] array2 = array;
				int index2 = index;
				index = index2 + 1;
				array2[index2] = node.Item;
				return true;
			});
		}

		/// <summary>Copies the complete <see cref="T:System.Collections.Generic.SortedSet`1" /> to a compatible one-dimensional array, starting at the specified array index.</summary>
		/// <param name="array">A one-dimensional array that is the destination of the elements copied from the <see cref="T:System.Collections.Generic.SortedSet`1" />. The array must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		/// <exception cref="T:System.ArgumentException">The number of elements in the source array is greater than the available space from <paramref name="index" /> to the end of the destination array. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero.</exception>
		// Token: 0x06002EC4 RID: 11972 RVA: 0x000A1108 File Offset: 0x0009F308
		void ICollection.CopyTo(Array array, int index)
		{
			SortedSet<T>.<>c__DisplayClass53_0 CS$<>8__locals1 = new SortedSet<T>.<>c__DisplayClass53_0();
			CS$<>8__locals1.index = index;
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank != 1)
			{
				throw new ArgumentException("Only single dimensional arrays are supported for the requested action.", "array");
			}
			if (array.GetLowerBound(0) != 0)
			{
				throw new ArgumentException("The lower bound of target array must be zero.", "array");
			}
			if (CS$<>8__locals1.index < 0)
			{
				throw new ArgumentOutOfRangeException("index", CS$<>8__locals1.index, "Non-negative number required.");
			}
			if (array.Length - CS$<>8__locals1.index < this.Count)
			{
				throw new ArgumentException("Destination array is not long enough to copy all the items in the collection. Check array index and length.");
			}
			T[] array2 = array as T[];
			if (array2 != null)
			{
				this.CopyTo(array2, CS$<>8__locals1.index);
				return;
			}
			object[] objects = array as object[];
			if (objects == null)
			{
				throw new ArgumentException("Target array type is not compatible with the type of items in the collection.", "array");
			}
			try
			{
				this.InOrderTreeWalk(delegate(SortedSet<T>.Node node)
				{
					object[] objects = objects;
					int index2 = CS$<>8__locals1.index;
					CS$<>8__locals1.index = index2 + 1;
					objects[index2] = node.Item;
					return true;
				});
			}
			catch (ArrayTypeMismatchException)
			{
				throw new ArgumentException("Target array type is not compatible with the type of items in the collection.", "array");
			}
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Collections.Generic.SortedSet`1" />.</summary>
		/// <returns>An enumerator that iterates through the <see cref="T:System.Collections.Generic.SortedSet`1" /> in sorted order.</returns>
		// Token: 0x06002EC5 RID: 11973 RVA: 0x000A1228 File Offset: 0x0009F428
		public SortedSet<T>.Enumerator GetEnumerator()
		{
			return new SortedSet<T>.Enumerator(this);
		}

		// Token: 0x06002EC6 RID: 11974 RVA: 0x000A1230 File Offset: 0x0009F430
		IEnumerator<T> IEnumerable<!0>.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		/// <summary>Returns an enumerator that iterates through a collection.</summary>
		/// <returns>An enumerator that can be used to iterate through the collection.</returns>
		// Token: 0x06002EC7 RID: 11975 RVA: 0x000A1230 File Offset: 0x0009F430
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06002EC8 RID: 11976 RVA: 0x000A1240 File Offset: 0x0009F440
		private void InsertionBalance(SortedSet<T>.Node current, ref SortedSet<T>.Node parent, SortedSet<T>.Node grandParent, SortedSet<T>.Node greatGrandParent)
		{
			bool flag = grandParent.Right == parent;
			bool flag2 = parent.Right == current;
			SortedSet<T>.Node node;
			if (flag == flag2)
			{
				node = (flag2 ? grandParent.RotateLeft() : grandParent.RotateRight());
			}
			else
			{
				node = (flag2 ? grandParent.RotateLeftRight() : grandParent.RotateRightLeft());
				parent = greatGrandParent;
			}
			grandParent.ColorRed();
			node.ColorBlack();
			this.ReplaceChildOrRoot(greatGrandParent, grandParent, node);
		}

		// Token: 0x06002EC9 RID: 11977 RVA: 0x000A12A5 File Offset: 0x0009F4A5
		private void ReplaceChildOrRoot(SortedSet<T>.Node parent, SortedSet<T>.Node child, SortedSet<T>.Node newChild)
		{
			if (parent != null)
			{
				parent.ReplaceChild(child, newChild);
				return;
			}
			this.root = newChild;
		}

		// Token: 0x06002ECA RID: 11978 RVA: 0x000A12BC File Offset: 0x0009F4BC
		private void ReplaceNode(SortedSet<T>.Node match, SortedSet<T>.Node parentOfMatch, SortedSet<T>.Node successor, SortedSet<T>.Node parentOfSuccessor)
		{
			if (successor == match)
			{
				successor = match.Left;
			}
			else
			{
				SortedSet<T>.Node right = successor.Right;
				if (right != null)
				{
					right.ColorBlack();
				}
				if (parentOfSuccessor != match)
				{
					parentOfSuccessor.Left = successor.Right;
					successor.Right = match.Right;
				}
				successor.Left = match.Left;
			}
			if (successor != null)
			{
				successor.Color = match.Color;
			}
			this.ReplaceChildOrRoot(parentOfMatch, match, successor);
		}

		// Token: 0x06002ECB RID: 11979 RVA: 0x000A132C File Offset: 0x0009F52C
		internal virtual SortedSet<T>.Node FindNode(T item)
		{
			int num;
			for (SortedSet<T>.Node node = this.root; node != null; node = ((num < 0) ? node.Left : node.Right))
			{
				num = this.comparer.Compare(item, node.Item);
				if (num == 0)
				{
					return node;
				}
			}
			return null;
		}

		// Token: 0x06002ECC RID: 11980 RVA: 0x000A1374 File Offset: 0x0009F574
		internal virtual int InternalIndexOf(T item)
		{
			SortedSet<T>.Node node = this.root;
			int num = 0;
			while (node != null)
			{
				int num2 = this.comparer.Compare(item, node.Item);
				if (num2 == 0)
				{
					return num;
				}
				node = ((num2 < 0) ? node.Left : node.Right);
				num = ((num2 < 0) ? (2 * num + 1) : (2 * num + 2));
			}
			return -1;
		}

		// Token: 0x06002ECD RID: 11981 RVA: 0x000A13CC File Offset: 0x0009F5CC
		internal SortedSet<T>.Node FindRange(T from, T to)
		{
			return this.FindRange(from, to, true, true);
		}

		// Token: 0x06002ECE RID: 11982 RVA: 0x000A13D8 File Offset: 0x0009F5D8
		internal SortedSet<T>.Node FindRange(T from, T to, bool lowerBoundActive, bool upperBoundActive)
		{
			SortedSet<T>.Node node = this.root;
			while (node != null)
			{
				if (lowerBoundActive && this.comparer.Compare(from, node.Item) > 0)
				{
					node = node.Right;
				}
				else
				{
					if (!upperBoundActive || this.comparer.Compare(to, node.Item) >= 0)
					{
						return node;
					}
					node = node.Left;
				}
			}
			return null;
		}

		// Token: 0x06002ECF RID: 11983 RVA: 0x000A1437 File Offset: 0x0009F637
		internal void UpdateVersion()
		{
			this.version++;
		}

		/// <summary>Returns an <see cref="T:System.Collections.IEqualityComparer" /> object that can be used to create a collection that contains individual sets.</summary>
		/// <returns>A comparer for creating a collection of sets.</returns>
		// Token: 0x06002ED0 RID: 11984 RVA: 0x000A1447 File Offset: 0x0009F647
		public static IEqualityComparer<SortedSet<T>> CreateSetComparer()
		{
			return SortedSet<T>.CreateSetComparer(null);
		}

		/// <summary>Returns an <see cref="T:System.Collections.IEqualityComparer" /> object, according to a specified comparer, that can be used to create a collection that contains individual sets.</summary>
		/// <param name="memberEqualityComparer">The comparer to use for creating the returned comparer.</param>
		/// <returns>A comparer for creating a collection of sets.</returns>
		// Token: 0x06002ED1 RID: 11985 RVA: 0x000A144F File Offset: 0x0009F64F
		public static IEqualityComparer<SortedSet<T>> CreateSetComparer(IEqualityComparer<T> memberEqualityComparer)
		{
			return new SortedSetEqualityComparer<T>(memberEqualityComparer);
		}

		// Token: 0x06002ED2 RID: 11986 RVA: 0x000A1458 File Offset: 0x0009F658
		internal static bool SortedSetEquals(SortedSet<T> set1, SortedSet<T> set2, IComparer<T> comparer)
		{
			if (set1 == null)
			{
				return set2 == null;
			}
			if (set2 == null)
			{
				return false;
			}
			if (set1.HasEqualComparer(set2))
			{
				return set1.Count == set2.Count && set1.SetEquals(set2);
			}
			bool flag = false;
			foreach (T x in set1)
			{
				flag = false;
				foreach (T y in set2)
				{
					if (comparer.Compare(x, y) == 0)
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06002ED3 RID: 11987 RVA: 0x000A1524 File Offset: 0x0009F724
		private bool HasEqualComparer(SortedSet<T> other)
		{
			return this.Comparer == other.Comparer || this.Comparer.Equals(other.Comparer);
		}

		/// <summary>Modifies the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object so that it contains all elements that are present in either the current object or the specified collection.</summary>
		/// <param name="other">The collection to compare to the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="other" /> is <see langword="null" />.</exception>
		// Token: 0x06002ED4 RID: 11988 RVA: 0x000A1548 File Offset: 0x0009F748
		public void UnionWith(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			SortedSet<T> sortedSet = other as SortedSet<T>;
			SortedSet<T>.TreeSubSet treeSubSet = this as SortedSet<T>.TreeSubSet;
			if (treeSubSet != null)
			{
				this.VersionCheck();
			}
			if (sortedSet != null && treeSubSet == null && this.count == 0)
			{
				SortedSet<T> sortedSet2 = new SortedSet<T>(sortedSet, this.comparer);
				this.root = sortedSet2.root;
				this.count = sortedSet2.count;
				this.version++;
				return;
			}
			if (sortedSet != null && treeSubSet == null && this.HasEqualComparer(sortedSet) && sortedSet.Count > this.Count / 2)
			{
				T[] array = new T[sortedSet.Count + this.Count];
				int num = 0;
				SortedSet<T>.Enumerator enumerator = this.GetEnumerator();
				SortedSet<T>.Enumerator enumerator2 = sortedSet.GetEnumerator();
				bool flag = !enumerator.MoveNext();
				bool flag2 = !enumerator2.MoveNext();
				while (!flag && !flag2)
				{
					int num2 = this.Comparer.Compare(enumerator.Current, enumerator2.Current);
					if (num2 < 0)
					{
						array[num++] = enumerator.Current;
						flag = !enumerator.MoveNext();
					}
					else if (num2 == 0)
					{
						array[num++] = enumerator2.Current;
						flag = !enumerator.MoveNext();
						flag2 = !enumerator2.MoveNext();
					}
					else
					{
						array[num++] = enumerator2.Current;
						flag2 = !enumerator2.MoveNext();
					}
				}
				if (!flag || !flag2)
				{
					SortedSet<T>.Enumerator enumerator3 = flag ? enumerator2 : enumerator;
					do
					{
						array[num++] = enumerator3.Current;
					}
					while (enumerator3.MoveNext());
				}
				this.root = null;
				this.root = SortedSet<T>.ConstructRootFromSortedArray(array, 0, num - 1, null);
				this.count = num;
				this.version++;
				return;
			}
			this.AddAllElements(other);
		}

		// Token: 0x06002ED5 RID: 11989 RVA: 0x000A1734 File Offset: 0x0009F934
		private static SortedSet<T>.Node ConstructRootFromSortedArray(T[] arr, int startIndex, int endIndex, SortedSet<T>.Node redNode)
		{
			int num = endIndex - startIndex + 1;
			SortedSet<T>.Node node;
			switch (num)
			{
			case 0:
				return null;
			case 1:
				node = new SortedSet<T>.Node(arr[startIndex], NodeColor.Black);
				if (redNode != null)
				{
					node.Left = redNode;
				}
				break;
			case 2:
				node = new SortedSet<T>.Node(arr[startIndex], NodeColor.Black);
				node.Right = new SortedSet<T>.Node(arr[endIndex], NodeColor.Black);
				node.Right.ColorRed();
				if (redNode != null)
				{
					node.Left = redNode;
				}
				break;
			case 3:
				node = new SortedSet<T>.Node(arr[startIndex + 1], NodeColor.Black);
				node.Left = new SortedSet<T>.Node(arr[startIndex], NodeColor.Black);
				node.Right = new SortedSet<T>.Node(arr[endIndex], NodeColor.Black);
				if (redNode != null)
				{
					node.Left.Left = redNode;
				}
				break;
			default:
			{
				int num2 = (startIndex + endIndex) / 2;
				node = new SortedSet<T>.Node(arr[num2], NodeColor.Black);
				node.Left = SortedSet<T>.ConstructRootFromSortedArray(arr, startIndex, num2 - 1, redNode);
				node.Right = ((num % 2 == 0) ? SortedSet<T>.ConstructRootFromSortedArray(arr, num2 + 2, endIndex, new SortedSet<T>.Node(arr[num2 + 1], NodeColor.Red)) : SortedSet<T>.ConstructRootFromSortedArray(arr, num2 + 1, endIndex, null));
				break;
			}
			}
			return node;
		}

		/// <summary>Modifies the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object so that it contains only elements that are also in a specified collection.</summary>
		/// <param name="other">The collection to compare to the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="other" /> is <see langword="null" />.</exception>
		// Token: 0x06002ED6 RID: 11990 RVA: 0x000A1860 File Offset: 0x0009FA60
		public virtual void IntersectWith(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			if (this.Count == 0)
			{
				return;
			}
			if (other == this)
			{
				return;
			}
			SortedSet<T> sortedSet = other as SortedSet<T>;
			SortedSet<T>.TreeSubSet treeSubSet = this as SortedSet<T>.TreeSubSet;
			if (treeSubSet != null)
			{
				this.VersionCheck();
			}
			if (sortedSet != null && treeSubSet == null && this.HasEqualComparer(sortedSet))
			{
				T[] array = new T[this.Count];
				int num = 0;
				SortedSet<T>.Enumerator enumerator = this.GetEnumerator();
				SortedSet<T>.Enumerator enumerator2 = sortedSet.GetEnumerator();
				bool flag = !enumerator.MoveNext();
				bool flag2 = !enumerator2.MoveNext();
				T max = this.Max;
				T min = this.Min;
				while (!flag && !flag2 && this.Comparer.Compare(enumerator2.Current, max) <= 0)
				{
					int num2 = this.Comparer.Compare(enumerator.Current, enumerator2.Current);
					if (num2 < 0)
					{
						flag = !enumerator.MoveNext();
					}
					else if (num2 == 0)
					{
						array[num++] = enumerator2.Current;
						flag = !enumerator.MoveNext();
						flag2 = !enumerator2.MoveNext();
					}
					else
					{
						flag2 = !enumerator2.MoveNext();
					}
				}
				this.root = null;
				this.root = SortedSet<T>.ConstructRootFromSortedArray(array, 0, num - 1, null);
				this.count = num;
				this.version++;
				return;
			}
			this.IntersectWithEnumerable(other);
		}

		// Token: 0x06002ED7 RID: 11991 RVA: 0x000A19C0 File Offset: 0x0009FBC0
		internal virtual void IntersectWithEnumerable(IEnumerable<T> other)
		{
			List<T> list = new List<T>(this.Count);
			foreach (T item in other)
			{
				if (this.Contains(item))
				{
					list.Add(item);
				}
			}
			this.Clear();
			foreach (T item2 in list)
			{
				this.Add(item2);
			}
		}

		/// <summary>Removes all elements that are in a specified collection from the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</summary>
		/// <param name="other">The collection of items to remove from the <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="other" /> is <see langword="null" />.</exception>
		// Token: 0x06002ED8 RID: 11992 RVA: 0x000A1A64 File Offset: 0x0009FC64
		public void ExceptWith(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			if (this.count == 0)
			{
				return;
			}
			if (other == this)
			{
				this.Clear();
				return;
			}
			SortedSet<T> sortedSet = other as SortedSet<T>;
			if (sortedSet != null && this.HasEqualComparer(sortedSet))
			{
				if (this.comparer.Compare(sortedSet.Max, this.Min) < 0 || this.comparer.Compare(sortedSet.Min, this.Max) > 0)
				{
					return;
				}
				T min = this.Min;
				T max = this.Max;
				using (IEnumerator<T> enumerator = other.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						T t = enumerator.Current;
						if (this.comparer.Compare(t, min) >= 0)
						{
							if (this.comparer.Compare(t, max) > 0)
							{
								break;
							}
							this.Remove(t);
						}
					}
					return;
				}
			}
			this.RemoveAllElements(other);
		}

		/// <summary>Modifies the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object so that it contains only elements that are present either in the current object or in the specified collection, but not both.</summary>
		/// <param name="other">The collection to compare to the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="other" /> is <see langword="null" />.</exception>
		// Token: 0x06002ED9 RID: 11993 RVA: 0x000A1B5C File Offset: 0x0009FD5C
		public void SymmetricExceptWith(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			if (this.Count == 0)
			{
				this.UnionWith(other);
				return;
			}
			if (other == this)
			{
				this.Clear();
				return;
			}
			SortedSet<T> sortedSet = other as SortedSet<T>;
			if (sortedSet != null && this.HasEqualComparer(sortedSet))
			{
				this.SymmetricExceptWithSameComparer(sortedSet);
				return;
			}
			int length;
			T[] array = EnumerableHelpers.ToArray<T>(other, out length);
			Array.Sort<T>(array, 0, length, this.Comparer);
			this.SymmetricExceptWithSameComparer(array, length);
		}

		// Token: 0x06002EDA RID: 11994 RVA: 0x000A1BCC File Offset: 0x0009FDCC
		private void SymmetricExceptWithSameComparer(SortedSet<T> other)
		{
			foreach (T item in other)
			{
				if (!this.Contains(item))
				{
					this.Add(item);
				}
				else
				{
					this.Remove(item);
				}
			}
		}

		// Token: 0x06002EDB RID: 11995 RVA: 0x000A1C30 File Offset: 0x0009FE30
		private void SymmetricExceptWithSameComparer(T[] other, int count)
		{
			if (count == 0)
			{
				return;
			}
			T y = other[0];
			for (int i = 0; i < count; i++)
			{
				while (i < count && i != 0 && this.comparer.Compare(other[i], y) == 0)
				{
					i++;
				}
				if (i >= count)
				{
					break;
				}
				T t = other[i];
				if (!this.Contains(t))
				{
					this.Add(t);
				}
				else
				{
					this.Remove(t);
				}
				y = t;
			}
		}

		/// <summary>Determines whether a <see cref="T:System.Collections.Generic.SortedSet`1" /> object is a subset of the specified collection.</summary>
		/// <param name="other">The collection to compare to the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object is a subset of <paramref name="other" />; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="other" /> is <see langword="null" />.</exception>
		// Token: 0x06002EDC RID: 11996 RVA: 0x000A1CA0 File Offset: 0x0009FEA0
		[SecuritySafeCritical]
		public bool IsSubsetOf(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			if (this.Count == 0)
			{
				return true;
			}
			SortedSet<T> sortedSet = other as SortedSet<T>;
			if (sortedSet != null && this.HasEqualComparer(sortedSet))
			{
				return this.Count <= sortedSet.Count && this.IsSubsetOfSortedSetWithSameComparer(sortedSet);
			}
			SortedSet<T>.ElementCount elementCount = this.CheckUniqueAndUnfoundElements(other, false);
			return elementCount.UniqueCount == this.Count && elementCount.UnfoundCount >= 0;
		}

		// Token: 0x06002EDD RID: 11997 RVA: 0x000A1D18 File Offset: 0x0009FF18
		private bool IsSubsetOfSortedSetWithSameComparer(SortedSet<T> asSorted)
		{
			SortedSet<T> viewBetween = asSorted.GetViewBetween(this.Min, this.Max);
			foreach (T item in this)
			{
				if (!viewBetween.Contains(item))
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>Determines whether a <see cref="T:System.Collections.Generic.SortedSet`1" /> object is a proper subset of the specified collection.</summary>
		/// <param name="other">The collection to compare to the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.Generic.SortedSet`1" /> object is a proper subset of <paramref name="other" />; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="other" /> is <see langword="null" />.</exception>
		// Token: 0x06002EDE RID: 11998 RVA: 0x000A1D84 File Offset: 0x0009FF84
		[SecuritySafeCritical]
		public bool IsProperSubsetOf(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			if (other is ICollection && this.Count == 0)
			{
				return (other as ICollection).Count > 0;
			}
			SortedSet<T> sortedSet = other as SortedSet<T>;
			if (sortedSet != null && this.HasEqualComparer(sortedSet))
			{
				return this.Count < sortedSet.Count && this.IsSubsetOfSortedSetWithSameComparer(sortedSet);
			}
			SortedSet<T>.ElementCount elementCount = this.CheckUniqueAndUnfoundElements(other, false);
			return elementCount.UniqueCount == this.Count && elementCount.UnfoundCount > 0;
		}

		/// <summary>Determines whether a <see cref="T:System.Collections.Generic.SortedSet`1" /> object is a superset of the specified collection.</summary>
		/// <param name="other">The collection to compare to the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object. </param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.Generic.SortedSet`1" /> object is a superset of <paramref name="other" />; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="other" /> is <see langword="null" />.</exception>
		// Token: 0x06002EDF RID: 11999 RVA: 0x000A1E0C File Offset: 0x000A000C
		public bool IsSupersetOf(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			if (other is ICollection && (other as ICollection).Count == 0)
			{
				return true;
			}
			SortedSet<T> sortedSet = other as SortedSet<T>;
			if (sortedSet == null || !this.HasEqualComparer(sortedSet))
			{
				return this.ContainsAllElements(other);
			}
			if (this.Count < sortedSet.Count)
			{
				return false;
			}
			SortedSet<T> viewBetween = this.GetViewBetween(sortedSet.Min, sortedSet.Max);
			foreach (T item in sortedSet)
			{
				if (!viewBetween.Contains(item))
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>Determines whether a <see cref="T:System.Collections.Generic.SortedSet`1" /> object is a proper superset of the specified collection.</summary>
		/// <param name="other">The collection to compare to the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object. </param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.Generic.SortedSet`1" /> object is a proper superset of <paramref name="other" />; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="other" /> is <see langword="null" />.</exception>
		// Token: 0x06002EE0 RID: 12000 RVA: 0x000A1EC8 File Offset: 0x000A00C8
		[SecuritySafeCritical]
		public bool IsProperSupersetOf(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			if (this.Count == 0)
			{
				return false;
			}
			if (other is ICollection && (other as ICollection).Count == 0)
			{
				return true;
			}
			SortedSet<T> sortedSet = other as SortedSet<T>;
			if (sortedSet == null || !this.HasEqualComparer(sortedSet))
			{
				SortedSet<T>.ElementCount elementCount = this.CheckUniqueAndUnfoundElements(other, true);
				return elementCount.UniqueCount < this.Count && elementCount.UnfoundCount == 0;
			}
			if (sortedSet.Count >= this.Count)
			{
				return false;
			}
			SortedSet<T> viewBetween = this.GetViewBetween(sortedSet.Min, sortedSet.Max);
			foreach (T item in sortedSet)
			{
				if (!viewBetween.Contains(item))
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>Determines whether the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object and the specified collection contain the same elements.</summary>
		/// <param name="other">The collection to compare to the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object is equal to <paramref name="other" />; otherwise, false.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="other" /> is <see langword="null" />.</exception>
		// Token: 0x06002EE1 RID: 12001 RVA: 0x000A1FAC File Offset: 0x000A01AC
		[SecuritySafeCritical]
		public bool SetEquals(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			SortedSet<T> sortedSet = other as SortedSet<T>;
			if (sortedSet != null && this.HasEqualComparer(sortedSet))
			{
				SortedSet<T>.Enumerator enumerator = this.GetEnumerator();
				SortedSet<T>.Enumerator enumerator2 = sortedSet.GetEnumerator();
				bool flag = !enumerator.MoveNext();
				bool flag2 = !enumerator2.MoveNext();
				while (!flag && !flag2)
				{
					if (this.Comparer.Compare(enumerator.Current, enumerator2.Current) != 0)
					{
						return false;
					}
					flag = !enumerator.MoveNext();
					flag2 = !enumerator2.MoveNext();
				}
				return flag && flag2;
			}
			SortedSet<T>.ElementCount elementCount = this.CheckUniqueAndUnfoundElements(other, true);
			return elementCount.UniqueCount == this.Count && elementCount.UnfoundCount == 0;
		}

		/// <summary>Determines whether the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object and a specified collection share common elements.</summary>
		/// <param name="other">The collection to compare to the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.Generic.SortedSet`1" /> object and <paramref name="other" /> share at least one common element; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="other" /> is <see langword="null" />.</exception>
		// Token: 0x06002EE2 RID: 12002 RVA: 0x000A2068 File Offset: 0x000A0268
		public bool Overlaps(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			if (this.Count == 0)
			{
				return false;
			}
			if (other is ICollection<!0> && (other as ICollection<!0>).Count == 0)
			{
				return false;
			}
			SortedSet<T> sortedSet = other as SortedSet<T>;
			if (sortedSet != null && this.HasEqualComparer(sortedSet) && (this.comparer.Compare(this.Min, sortedSet.Max) > 0 || this.comparer.Compare(this.Max, sortedSet.Min) < 0))
			{
				return false;
			}
			foreach (T item in other)
			{
				if (this.Contains(item))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002EE3 RID: 12003 RVA: 0x000A2134 File Offset: 0x000A0334
		private unsafe SortedSet<T>.ElementCount CheckUniqueAndUnfoundElements(IEnumerable<T> other, bool returnIfUnfound)
		{
			SortedSet<T>.ElementCount result;
			if (this.Count == 0)
			{
				int num = 0;
				using (IEnumerator<T> enumerator = other.GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						!0 ! = enumerator.Current;
						num++;
					}
				}
				result.UniqueCount = 0;
				result.UnfoundCount = num;
				return result;
			}
			int num2 = BitHelper.ToIntArrayLength(this.Count);
			BitHelper bitHelper;
			if (num2 <= 100)
			{
				bitHelper = new BitHelper(stackalloc int[checked(unchecked((UIntPtr)num2) * 4)], num2);
			}
			else
			{
				bitHelper = new BitHelper(new int[num2], num2);
			}
			int num3 = 0;
			int num4 = 0;
			foreach (T item in other)
			{
				int num5 = this.InternalIndexOf(item);
				if (num5 >= 0)
				{
					if (!bitHelper.IsMarked(num5))
					{
						bitHelper.MarkBit(num5);
						num4++;
					}
				}
				else
				{
					num3++;
					if (returnIfUnfound)
					{
						break;
					}
				}
			}
			result.UniqueCount = num4;
			result.UnfoundCount = num3;
			return result;
		}

		/// <summary>Removes all elements that match the conditions defined by the specified predicate from a <see cref="T:System.Collections.Generic.SortedSet`1" />.</summary>
		/// <param name="match">The delegate that defines the conditions of the elements to remove.</param>
		/// <returns>The number of elements that were removed from the <see cref="T:System.Collections.Generic.SortedSet`1" /> collection.. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="match" /> is <see langword="null" />.</exception>
		// Token: 0x06002EE4 RID: 12004 RVA: 0x000A224C File Offset: 0x000A044C
		public int RemoveWhere(Predicate<T> match)
		{
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
			List<T> matches = new List<T>(this.Count);
			this.BreadthFirstTreeWalk(delegate(SortedSet<T>.Node n)
			{
				if (match(n.Item))
				{
					matches.Add(n.Item);
				}
				return true;
			});
			int num = 0;
			for (int i = matches.Count - 1; i >= 0; i--)
			{
				if (this.Remove(matches[i]))
				{
					num++;
				}
			}
			return num;
		}

		/// <summary>Gets the minimum value in the <see cref="T:System.Collections.Generic.SortedSet`1" />, as defined by the comparer.</summary>
		/// <returns>The minimum value in the set.</returns>
		// Token: 0x17000BAE RID: 2990
		// (get) Token: 0x06002EE5 RID: 12005 RVA: 0x000A22D0 File Offset: 0x000A04D0
		public T Min
		{
			get
			{
				return this.MinInternal;
			}
		}

		// Token: 0x17000BAF RID: 2991
		// (get) Token: 0x06002EE6 RID: 12006 RVA: 0x000A22D8 File Offset: 0x000A04D8
		internal virtual T MinInternal
		{
			get
			{
				if (this.root == null)
				{
					return default(T);
				}
				SortedSet<T>.Node left = this.root;
				while (left.Left != null)
				{
					left = left.Left;
				}
				return left.Item;
			}
		}

		/// <summary>Gets the maximum value in the <see cref="T:System.Collections.Generic.SortedSet`1" />, as defined by the comparer.</summary>
		/// <returns>The maximum value in the set.</returns>
		// Token: 0x17000BB0 RID: 2992
		// (get) Token: 0x06002EE7 RID: 12007 RVA: 0x000A2315 File Offset: 0x000A0515
		public T Max
		{
			get
			{
				return this.MaxInternal;
			}
		}

		// Token: 0x17000BB1 RID: 2993
		// (get) Token: 0x06002EE8 RID: 12008 RVA: 0x000A2320 File Offset: 0x000A0520
		internal virtual T MaxInternal
		{
			get
			{
				if (this.root == null)
				{
					return default(T);
				}
				SortedSet<T>.Node right = this.root;
				while (right.Right != null)
				{
					right = right.Right;
				}
				return right.Item;
			}
		}

		/// <summary>Returns an <see cref="T:System.Collections.Generic.IEnumerable`1" /> that iterates over the <see cref="T:System.Collections.Generic.SortedSet`1" /> in reverse order.</summary>
		/// <returns>An enumerator that iterates over the <see cref="T:System.Collections.Generic.SortedSet`1" /> in reverse order.</returns>
		// Token: 0x06002EE9 RID: 12009 RVA: 0x000A235D File Offset: 0x000A055D
		public IEnumerable<T> Reverse()
		{
			SortedSet<T>.Enumerator e = new SortedSet<T>.Enumerator(this, true);
			while (e.MoveNext())
			{
				T t = e.Current;
				yield return t;
			}
			yield break;
		}

		/// <summary>Returns a view of a subset in a <see cref="T:System.Collections.Generic.SortedSet`1" />.</summary>
		/// <param name="lowerValue">The lowest desired value in the view.</param>
		/// <param name="upperValue">The highest desired value in the view. </param>
		/// <returns>A subset view that contains only the values in the specified range.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="lowerValue" /> is more than <paramref name="upperValue" /> according to the comparer.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">A tried operation on the view was outside the range specified by <paramref name="lowerValue" /> and <paramref name="upperValue" />.</exception>
		// Token: 0x06002EEA RID: 12010 RVA: 0x000A236D File Offset: 0x000A056D
		public virtual SortedSet<T> GetViewBetween(T lowerValue, T upperValue)
		{
			if (this.Comparer.Compare(lowerValue, upperValue) > 0)
			{
				throw new ArgumentException("Must be less than or equal to upperValue.", "lowerValue");
			}
			return new SortedSet<T>.TreeSubSet(this, lowerValue, upperValue, true, true);
		}

		/// <summary>Implements the <see cref="T:System.Runtime.Serialization.ISerializable" /> interface, and returns the data that you need to serialize the <see cref="T:System.Collections.Generic.SortedSet`1" /> instance.</summary>
		/// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object that contains the information that is required to serialize the <see cref="T:System.Collections.Generic.SortedSet`1" /> instance.</param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> structure that contains the source and destination of the serialized stream associated with the <see cref="T:System.Collections.Generic.SortedSet`1" /> instance.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info" /> is <see langword="null" />.</exception>
		// Token: 0x06002EEB RID: 12011 RVA: 0x000A2399 File Offset: 0x000A0599
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			this.GetObjectData(info, context);
		}

		/// <summary>Implements the <see cref="T:System.Runtime.Serialization.ISerializable" /> interface and returns the data that you must have to serialize a <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</summary>
		/// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object that contains the information that is required to serialize the <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> structure that contains the source and destination of the serialized stream associated with the <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info" /> is <see langword="null" />.</exception>
		// Token: 0x06002EEC RID: 12012 RVA: 0x000A23A4 File Offset: 0x000A05A4
		protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("Count", this.count);
			info.AddValue("Comparer", this.comparer, typeof(IComparer<T>));
			info.AddValue("Version", this.version);
			if (this.root != null)
			{
				T[] array = new T[this.Count];
				this.CopyTo(array, 0);
				info.AddValue("Items", array, typeof(T[]));
			}
		}

		/// <summary>Implements the <see cref="T:System.Runtime.Serialization.IDeserializationCallback" /> interface, and raises the deserialization event when the deserialization is completed.</summary>
		/// <param name="sender">The source of the deserialization event.</param>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object associated with the current <see cref="T:System.Collections.Generic.SortedSet`1" /> instance is invalid.</exception>
		// Token: 0x06002EED RID: 12013 RVA: 0x000A242E File Offset: 0x000A062E
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			this.OnDeserialization(sender);
		}

		/// <summary>Implements the <see cref="T:System.Runtime.Serialization.ISerializable" /> interface, and raises the deserialization event when the deserialization is completed.</summary>
		/// <param name="sender">The source of the deserialization event.</param>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object associated with the current <see cref="T:System.Collections.Generic.SortedSet`1" /> object is invalid.</exception>
		// Token: 0x06002EEE RID: 12014 RVA: 0x000A2438 File Offset: 0x000A0638
		protected virtual void OnDeserialization(object sender)
		{
			if (this.comparer != null)
			{
				return;
			}
			if (this.siInfo == null)
			{
				throw new SerializationException("OnDeserialization method was called while the object was not being deserialized.");
			}
			this.comparer = (IComparer<T>)this.siInfo.GetValue("Comparer", typeof(IComparer<T>));
			int @int = this.siInfo.GetInt32("Count");
			if (@int != 0)
			{
				T[] array = (T[])this.siInfo.GetValue("Items", typeof(T[]));
				if (array == null)
				{
					throw new SerializationException("The values for this dictionary are missing.");
				}
				for (int i = 0; i < array.Length; i++)
				{
					this.Add(array[i]);
				}
			}
			this.version = this.siInfo.GetInt32("Version");
			if (this.count != @int)
			{
				throw new SerializationException("The serialized Count information doesn't match the number of items.");
			}
			this.siInfo = null;
		}

		// Token: 0x06002EEF RID: 12015 RVA: 0x000A2518 File Offset: 0x000A0718
		public bool TryGetValue(T equalValue, out T actualValue)
		{
			SortedSet<T>.Node node = this.FindNode(equalValue);
			if (node != null)
			{
				actualValue = node.Item;
				return true;
			}
			actualValue = default(T);
			return false;
		}

		// Token: 0x06002EF0 RID: 12016 RVA: 0x000A2548 File Offset: 0x000A0748
		private static int Log2(int value)
		{
			int num = 0;
			while (value > 0)
			{
				num++;
				value >>= 1;
			}
			return num;
		}

		// Token: 0x04002404 RID: 9220
		private SortedSet<T>.Node root;

		// Token: 0x04002405 RID: 9221
		private IComparer<T> comparer;

		// Token: 0x04002406 RID: 9222
		private int count;

		// Token: 0x04002407 RID: 9223
		private int version;

		// Token: 0x04002408 RID: 9224
		[NonSerialized]
		private object _syncRoot;

		// Token: 0x04002409 RID: 9225
		private SerializationInfo siInfo;

		// Token: 0x0400240A RID: 9226
		private const string ComparerName = "Comparer";

		// Token: 0x0400240B RID: 9227
		private const string CountName = "Count";

		// Token: 0x0400240C RID: 9228
		private const string ItemsName = "Items";

		// Token: 0x0400240D RID: 9229
		private const string VersionName = "Version";

		// Token: 0x0400240E RID: 9230
		private const string TreeName = "Tree";

		// Token: 0x0400240F RID: 9231
		private const string NodeValueName = "Item";

		// Token: 0x04002410 RID: 9232
		private const string EnumStartName = "EnumStarted";

		// Token: 0x04002411 RID: 9233
		private const string ReverseName = "Reverse";

		// Token: 0x04002412 RID: 9234
		private const string EnumVersionName = "EnumVersion";

		// Token: 0x04002413 RID: 9235
		private const string MinName = "Min";

		// Token: 0x04002414 RID: 9236
		private const string MaxName = "Max";

		// Token: 0x04002415 RID: 9237
		private const string LowerBoundActiveName = "lBoundActive";

		// Token: 0x04002416 RID: 9238
		private const string UpperBoundActiveName = "uBoundActive";

		// Token: 0x04002417 RID: 9239
		internal const int StackAllocThreshold = 100;

		// Token: 0x020005B2 RID: 1458
		[Serializable]
		internal sealed class TreeSubSet : SortedSet<T>, ISerializable, IDeserializationCallback
		{
			// Token: 0x06002EF1 RID: 12017 RVA: 0x000A2568 File Offset: 0x000A0768
			public TreeSubSet(SortedSet<T> Underlying, T Min, T Max, bool lowerBoundActive, bool upperBoundActive) : base(Underlying.Comparer)
			{
				this._underlying = Underlying;
				this._min = Min;
				this._max = Max;
				this._lBoundActive = lowerBoundActive;
				this._uBoundActive = upperBoundActive;
				this.root = this._underlying.FindRange(this._min, this._max, this._lBoundActive, this._uBoundActive);
				this.count = 0;
				this.version = -1;
				this.VersionCheckImpl();
			}

			// Token: 0x06002EF2 RID: 12018 RVA: 0x000A25E3 File Offset: 0x000A07E3
			internal override bool AddIfNotPresent(T item)
			{
				if (!this.IsWithinRange(item))
				{
					throw new ArgumentOutOfRangeException("item");
				}
				bool result = this._underlying.AddIfNotPresent(item);
				this.VersionCheck();
				return result;
			}

			// Token: 0x06002EF3 RID: 12019 RVA: 0x000A260B File Offset: 0x000A080B
			public override bool Contains(T item)
			{
				this.VersionCheck();
				return base.Contains(item);
			}

			// Token: 0x06002EF4 RID: 12020 RVA: 0x000A261A File Offset: 0x000A081A
			internal override bool DoRemove(T item)
			{
				if (!this.IsWithinRange(item))
				{
					return false;
				}
				bool result = this._underlying.Remove(item);
				this.VersionCheck();
				return result;
			}

			// Token: 0x06002EF5 RID: 12021 RVA: 0x000A263C File Offset: 0x000A083C
			public override void Clear()
			{
				if (this.count == 0)
				{
					return;
				}
				List<T> toRemove = new List<T>();
				this.BreadthFirstTreeWalk(delegate(SortedSet<T>.Node n)
				{
					toRemove.Add(n.Item);
					return true;
				});
				while (toRemove.Count != 0)
				{
					this._underlying.Remove(toRemove[toRemove.Count - 1]);
					toRemove.RemoveAt(toRemove.Count - 1);
				}
				this.root = null;
				this.count = 0;
				this.version = this._underlying.version;
			}

			// Token: 0x06002EF6 RID: 12022 RVA: 0x000A26E0 File Offset: 0x000A08E0
			internal override bool IsWithinRange(T item)
			{
				return (this._lBoundActive ? base.Comparer.Compare(this._min, item) : -1) <= 0 && (this._uBoundActive ? base.Comparer.Compare(this._max, item) : 1) >= 0;
			}

			// Token: 0x17000BB2 RID: 2994
			// (get) Token: 0x06002EF7 RID: 12023 RVA: 0x000A2734 File Offset: 0x000A0934
			internal override T MinInternal
			{
				get
				{
					SortedSet<T>.Node node = this.root;
					T result = default(T);
					while (node != null)
					{
						int num = this._lBoundActive ? base.Comparer.Compare(this._min, node.Item) : -1;
						if (num == 1)
						{
							node = node.Right;
						}
						else
						{
							result = node.Item;
							if (num == 0)
							{
								break;
							}
							node = node.Left;
						}
					}
					return result;
				}
			}

			// Token: 0x17000BB3 RID: 2995
			// (get) Token: 0x06002EF8 RID: 12024 RVA: 0x000A2798 File Offset: 0x000A0998
			internal override T MaxInternal
			{
				get
				{
					SortedSet<T>.Node node = this.root;
					T result = default(T);
					while (node != null)
					{
						int num = this._uBoundActive ? base.Comparer.Compare(this._max, node.Item) : 1;
						if (num == -1)
						{
							node = node.Left;
						}
						else
						{
							result = node.Item;
							if (num == 0)
							{
								break;
							}
							node = node.Right;
						}
					}
					return result;
				}
			}

			// Token: 0x06002EF9 RID: 12025 RVA: 0x000A27FC File Offset: 0x000A09FC
			internal override bool InOrderTreeWalk(TreeWalkPredicate<T> action)
			{
				this.VersionCheck();
				if (this.root == null)
				{
					return true;
				}
				Stack<SortedSet<T>.Node> stack = new Stack<SortedSet<T>.Node>(2 * SortedSet<T>.Log2(this.count + 1));
				SortedSet<T>.Node node = this.root;
				while (node != null)
				{
					if (this.IsWithinRange(node.Item))
					{
						stack.Push(node);
						node = node.Left;
					}
					else if (this._lBoundActive && base.Comparer.Compare(this._min, node.Item) > 0)
					{
						node = node.Right;
					}
					else
					{
						node = node.Left;
					}
				}
				while (stack.Count != 0)
				{
					node = stack.Pop();
					if (!action(node))
					{
						return false;
					}
					SortedSet<T>.Node node2 = node.Right;
					while (node2 != null)
					{
						if (this.IsWithinRange(node2.Item))
						{
							stack.Push(node2);
							node2 = node2.Left;
						}
						else if (this._lBoundActive && base.Comparer.Compare(this._min, node2.Item) > 0)
						{
							node2 = node2.Right;
						}
						else
						{
							node2 = node2.Left;
						}
					}
				}
				return true;
			}

			// Token: 0x06002EFA RID: 12026 RVA: 0x000A2904 File Offset: 0x000A0B04
			internal override bool BreadthFirstTreeWalk(TreeWalkPredicate<T> action)
			{
				this.VersionCheck();
				if (this.root == null)
				{
					return true;
				}
				Queue<SortedSet<T>.Node> queue = new Queue<SortedSet<T>.Node>();
				queue.Enqueue(this.root);
				while (queue.Count != 0)
				{
					SortedSet<T>.Node node = queue.Dequeue();
					if (this.IsWithinRange(node.Item) && !action(node))
					{
						return false;
					}
					if (node.Left != null && (!this._lBoundActive || base.Comparer.Compare(this._min, node.Item) < 0))
					{
						queue.Enqueue(node.Left);
					}
					if (node.Right != null && (!this._uBoundActive || base.Comparer.Compare(this._max, node.Item) > 0))
					{
						queue.Enqueue(node.Right);
					}
				}
				return true;
			}

			// Token: 0x06002EFB RID: 12027 RVA: 0x000A29D0 File Offset: 0x000A0BD0
			internal override SortedSet<T>.Node FindNode(T item)
			{
				if (!this.IsWithinRange(item))
				{
					return null;
				}
				this.VersionCheck();
				return base.FindNode(item);
			}

			// Token: 0x06002EFC RID: 12028 RVA: 0x000A29EC File Offset: 0x000A0BEC
			internal override int InternalIndexOf(T item)
			{
				int num = -1;
				foreach (T y in this)
				{
					num++;
					if (base.Comparer.Compare(item, y) == 0)
					{
						return num;
					}
				}
				return -1;
			}

			// Token: 0x06002EFD RID: 12029 RVA: 0x000A2A50 File Offset: 0x000A0C50
			internal override void VersionCheck()
			{
				this.VersionCheckImpl();
			}

			// Token: 0x06002EFE RID: 12030 RVA: 0x000A2A58 File Offset: 0x000A0C58
			private void VersionCheckImpl()
			{
				if (this.version != this._underlying.version)
				{
					this.root = this._underlying.FindRange(this._min, this._max, this._lBoundActive, this._uBoundActive);
					this.version = this._underlying.version;
					this.count = 0;
					this.InOrderTreeWalk(delegate(SortedSet<T>.Node n)
					{
						this.count++;
						return true;
					});
				}
			}

			// Token: 0x06002EFF RID: 12031 RVA: 0x000A2ACC File Offset: 0x000A0CCC
			public override SortedSet<T> GetViewBetween(T lowerValue, T upperValue)
			{
				if (this._lBoundActive && base.Comparer.Compare(this._min, lowerValue) > 0)
				{
					throw new ArgumentOutOfRangeException("lowerValue");
				}
				if (this._uBoundActive && base.Comparer.Compare(this._max, upperValue) < 0)
				{
					throw new ArgumentOutOfRangeException("upperValue");
				}
				return (SortedSet<T>.TreeSubSet)this._underlying.GetViewBetween(lowerValue, upperValue);
			}

			// Token: 0x06002F00 RID: 12032 RVA: 0x000A2399 File Offset: 0x000A0599
			void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
			{
				this.GetObjectData(info, context);
			}

			// Token: 0x06002F01 RID: 12033 RVA: 0x0004B800 File Offset: 0x00049A00
			protected override void GetObjectData(SerializationInfo info, StreamingContext context)
			{
				throw new PlatformNotSupportedException();
			}

			// Token: 0x06002F02 RID: 12034 RVA: 0x0004B800 File Offset: 0x00049A00
			void IDeserializationCallback.OnDeserialization(object sender)
			{
				throw new PlatformNotSupportedException();
			}

			// Token: 0x06002F03 RID: 12035 RVA: 0x0004B800 File Offset: 0x00049A00
			protected override void OnDeserialization(object sender)
			{
				throw new PlatformNotSupportedException();
			}

			// Token: 0x06002F04 RID: 12036 RVA: 0x000A2B3B File Offset: 0x000A0D3B
			[CompilerGenerated]
			private bool <VersionCheckImpl>b__20_0(SortedSet<T>.Node n)
			{
				this.count++;
				return true;
			}

			// Token: 0x04002418 RID: 9240
			private SortedSet<T> _underlying;

			// Token: 0x04002419 RID: 9241
			private T _min;

			// Token: 0x0400241A RID: 9242
			private T _max;

			// Token: 0x0400241B RID: 9243
			private bool _lBoundActive;

			// Token: 0x0400241C RID: 9244
			private bool _uBoundActive;

			// Token: 0x020005B3 RID: 1459
			[CompilerGenerated]
			private sealed class <>c__DisplayClass9_0
			{
				// Token: 0x06002F05 RID: 12037 RVA: 0x0000232F File Offset: 0x0000052F
				public <>c__DisplayClass9_0()
				{
				}

				// Token: 0x06002F06 RID: 12038 RVA: 0x000A2B4C File Offset: 0x000A0D4C
				internal bool <Clear>b__0(SortedSet<T>.Node n)
				{
					this.toRemove.Add(n.Item);
					return true;
				}

				// Token: 0x0400241D RID: 9245
				public List<T> toRemove;
			}
		}

		// Token: 0x020005B4 RID: 1460
		[Serializable]
		internal sealed class Node
		{
			// Token: 0x06002F07 RID: 12039 RVA: 0x000A2B60 File Offset: 0x000A0D60
			public Node(T item, NodeColor color)
			{
				this.Item = item;
				this.Color = color;
			}

			// Token: 0x06002F08 RID: 12040 RVA: 0x000A2B76 File Offset: 0x000A0D76
			public static bool IsNonNullBlack(SortedSet<T>.Node node)
			{
				return node != null && node.IsBlack;
			}

			// Token: 0x06002F09 RID: 12041 RVA: 0x000A2B83 File Offset: 0x000A0D83
			public static bool IsNonNullRed(SortedSet<T>.Node node)
			{
				return node != null && node.IsRed;
			}

			// Token: 0x06002F0A RID: 12042 RVA: 0x000A2B90 File Offset: 0x000A0D90
			public static bool IsNullOrBlack(SortedSet<T>.Node node)
			{
				return node == null || node.IsBlack;
			}

			// Token: 0x17000BB4 RID: 2996
			// (get) Token: 0x06002F0B RID: 12043 RVA: 0x000A2B9D File Offset: 0x000A0D9D
			// (set) Token: 0x06002F0C RID: 12044 RVA: 0x000A2BA5 File Offset: 0x000A0DA5
			public T Item
			{
				[CompilerGenerated]
				get
				{
					return this.<Item>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<Item>k__BackingField = value;
				}
			}

			// Token: 0x17000BB5 RID: 2997
			// (get) Token: 0x06002F0D RID: 12045 RVA: 0x000A2BAE File Offset: 0x000A0DAE
			// (set) Token: 0x06002F0E RID: 12046 RVA: 0x000A2BB6 File Offset: 0x000A0DB6
			public SortedSet<T>.Node Left
			{
				[CompilerGenerated]
				get
				{
					return this.<Left>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<Left>k__BackingField = value;
				}
			}

			// Token: 0x17000BB6 RID: 2998
			// (get) Token: 0x06002F0F RID: 12047 RVA: 0x000A2BBF File Offset: 0x000A0DBF
			// (set) Token: 0x06002F10 RID: 12048 RVA: 0x000A2BC7 File Offset: 0x000A0DC7
			public SortedSet<T>.Node Right
			{
				[CompilerGenerated]
				get
				{
					return this.<Right>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<Right>k__BackingField = value;
				}
			}

			// Token: 0x17000BB7 RID: 2999
			// (get) Token: 0x06002F11 RID: 12049 RVA: 0x000A2BD0 File Offset: 0x000A0DD0
			// (set) Token: 0x06002F12 RID: 12050 RVA: 0x000A2BD8 File Offset: 0x000A0DD8
			public NodeColor Color
			{
				[CompilerGenerated]
				get
				{
					return this.<Color>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<Color>k__BackingField = value;
				}
			}

			// Token: 0x17000BB8 RID: 3000
			// (get) Token: 0x06002F13 RID: 12051 RVA: 0x000A2BE1 File Offset: 0x000A0DE1
			public bool IsBlack
			{
				get
				{
					return this.Color == NodeColor.Black;
				}
			}

			// Token: 0x17000BB9 RID: 3001
			// (get) Token: 0x06002F14 RID: 12052 RVA: 0x000A2BEC File Offset: 0x000A0DEC
			public bool IsRed
			{
				get
				{
					return this.Color == NodeColor.Red;
				}
			}

			// Token: 0x17000BBA RID: 3002
			// (get) Token: 0x06002F15 RID: 12053 RVA: 0x000A2BF7 File Offset: 0x000A0DF7
			public bool Is2Node
			{
				get
				{
					return this.IsBlack && SortedSet<T>.Node.IsNullOrBlack(this.Left) && SortedSet<T>.Node.IsNullOrBlack(this.Right);
				}
			}

			// Token: 0x17000BBB RID: 3003
			// (get) Token: 0x06002F16 RID: 12054 RVA: 0x000A2C1B File Offset: 0x000A0E1B
			public bool Is4Node
			{
				get
				{
					return SortedSet<T>.Node.IsNonNullRed(this.Left) && SortedSet<T>.Node.IsNonNullRed(this.Right);
				}
			}

			// Token: 0x06002F17 RID: 12055 RVA: 0x000A2C37 File Offset: 0x000A0E37
			public void ColorBlack()
			{
				this.Color = NodeColor.Black;
			}

			// Token: 0x06002F18 RID: 12056 RVA: 0x000A2C40 File Offset: 0x000A0E40
			public void ColorRed()
			{
				this.Color = NodeColor.Red;
			}

			// Token: 0x06002F19 RID: 12057 RVA: 0x000A2C4C File Offset: 0x000A0E4C
			public SortedSet<T>.Node DeepClone(int count)
			{
				Stack<SortedSet<T>.Node> stack = new Stack<SortedSet<T>.Node>(2 * SortedSet<T>.Log2(count) + 2);
				Stack<SortedSet<T>.Node> stack2 = new Stack<SortedSet<T>.Node>(2 * SortedSet<T>.Log2(count) + 2);
				SortedSet<T>.Node node = this.ShallowClone();
				SortedSet<T>.Node node2 = this;
				SortedSet<T>.Node node3 = node;
				while (node2 != null)
				{
					stack.Push(node2);
					stack2.Push(node3);
					SortedSet<T>.Node node4 = node3;
					SortedSet<T>.Node left = node2.Left;
					node4.Left = ((left != null) ? left.ShallowClone() : null);
					node2 = node2.Left;
					node3 = node3.Left;
				}
				while (stack.Count != 0)
				{
					node2 = stack.Pop();
					node3 = stack2.Pop();
					SortedSet<T>.Node node5 = node2.Right;
					SortedSet<T>.Node node6 = (node5 != null) ? node5.ShallowClone() : null;
					node3.Right = node6;
					while (node5 != null)
					{
						stack.Push(node5);
						stack2.Push(node6);
						SortedSet<T>.Node node7 = node6;
						SortedSet<T>.Node left2 = node5.Left;
						node7.Left = ((left2 != null) ? left2.ShallowClone() : null);
						node5 = node5.Left;
						node6 = node6.Left;
					}
				}
				return node;
			}

			// Token: 0x06002F1A RID: 12058 RVA: 0x000A2D40 File Offset: 0x000A0F40
			public TreeRotation GetRotation(SortedSet<T>.Node current, SortedSet<T>.Node sibling)
			{
				bool flag = this.Left == current;
				if (!SortedSet<T>.Node.IsNonNullRed(sibling.Left))
				{
					if (!flag)
					{
						return TreeRotation.LeftRight;
					}
					return TreeRotation.Left;
				}
				else
				{
					if (!flag)
					{
						return TreeRotation.Right;
					}
					return TreeRotation.RightLeft;
				}
			}

			// Token: 0x06002F1B RID: 12059 RVA: 0x000A2D71 File Offset: 0x000A0F71
			public SortedSet<T>.Node GetSibling(SortedSet<T>.Node node)
			{
				if (node != this.Left)
				{
					return this.Left;
				}
				return this.Right;
			}

			// Token: 0x06002F1C RID: 12060 RVA: 0x000A2D89 File Offset: 0x000A0F89
			public SortedSet<T>.Node ShallowClone()
			{
				return new SortedSet<T>.Node(this.Item, this.Color);
			}

			// Token: 0x06002F1D RID: 12061 RVA: 0x000A2D9C File Offset: 0x000A0F9C
			public void Split4Node()
			{
				this.ColorRed();
				this.Left.ColorBlack();
				this.Right.ColorBlack();
			}

			// Token: 0x06002F1E RID: 12062 RVA: 0x000A2DBC File Offset: 0x000A0FBC
			public SortedSet<T>.Node Rotate(TreeRotation rotation)
			{
				switch (rotation)
				{
				case TreeRotation.Left:
					this.Right.Right.ColorBlack();
					return this.RotateLeft();
				case TreeRotation.LeftRight:
					return this.RotateLeftRight();
				case TreeRotation.Right:
					this.Left.Left.ColorBlack();
					return this.RotateRight();
				case TreeRotation.RightLeft:
					return this.RotateRightLeft();
				default:
					return null;
				}
			}

			// Token: 0x06002F1F RID: 12063 RVA: 0x000A2E20 File Offset: 0x000A1020
			public SortedSet<T>.Node RotateLeft()
			{
				SortedSet<T>.Node right = this.Right;
				this.Right = right.Left;
				right.Left = this;
				return right;
			}

			// Token: 0x06002F20 RID: 12064 RVA: 0x000A2E48 File Offset: 0x000A1048
			public SortedSet<T>.Node RotateLeftRight()
			{
				SortedSet<T>.Node left = this.Left;
				SortedSet<T>.Node right = left.Right;
				this.Left = right.Right;
				right.Right = this;
				left.Right = right.Left;
				right.Left = left;
				return right;
			}

			// Token: 0x06002F21 RID: 12065 RVA: 0x000A2E8C File Offset: 0x000A108C
			public SortedSet<T>.Node RotateRight()
			{
				SortedSet<T>.Node left = this.Left;
				this.Left = left.Right;
				left.Right = this;
				return left;
			}

			// Token: 0x06002F22 RID: 12066 RVA: 0x000A2EB4 File Offset: 0x000A10B4
			public SortedSet<T>.Node RotateRightLeft()
			{
				SortedSet<T>.Node right = this.Right;
				SortedSet<T>.Node left = right.Left;
				this.Right = left.Left;
				left.Left = this;
				right.Left = left.Right;
				left.Right = right;
				return left;
			}

			// Token: 0x06002F23 RID: 12067 RVA: 0x000A2EF6 File Offset: 0x000A10F6
			public void Merge2Nodes()
			{
				this.ColorBlack();
				this.Left.ColorRed();
				this.Right.ColorRed();
			}

			// Token: 0x06002F24 RID: 12068 RVA: 0x000A2F14 File Offset: 0x000A1114
			public void ReplaceChild(SortedSet<T>.Node child, SortedSet<T>.Node newChild)
			{
				if (this.Left == child)
				{
					this.Left = newChild;
					return;
				}
				this.Right = newChild;
			}

			// Token: 0x0400241E RID: 9246
			[CompilerGenerated]
			private T <Item>k__BackingField;

			// Token: 0x0400241F RID: 9247
			[CompilerGenerated]
			private SortedSet<T>.Node <Left>k__BackingField;

			// Token: 0x04002420 RID: 9248
			[CompilerGenerated]
			private SortedSet<T>.Node <Right>k__BackingField;

			// Token: 0x04002421 RID: 9249
			[CompilerGenerated]
			private NodeColor <Color>k__BackingField;
		}

		/// <summary>Enumerates the elements of a <see cref="T:System.Collections.Generic.SortedSet`1" /> object.</summary>
		// Token: 0x020005B5 RID: 1461
		[Serializable]
		public struct Enumerator : IEnumerator<!0>, IDisposable, IEnumerator, ISerializable, IDeserializationCallback
		{
			// Token: 0x06002F25 RID: 12069 RVA: 0x000A2F2E File Offset: 0x000A112E
			internal Enumerator(SortedSet<T> set)
			{
				this = new SortedSet<T>.Enumerator(set, false);
			}

			// Token: 0x06002F26 RID: 12070 RVA: 0x000A2F38 File Offset: 0x000A1138
			internal Enumerator(SortedSet<T> set, bool reverse)
			{
				this._tree = set;
				set.VersionCheck();
				this._version = set.version;
				this._stack = new Stack<SortedSet<T>.Node>(2 * SortedSet<T>.Log2(set.Count + 1));
				this._current = null;
				this._reverse = reverse;
				this.Initialize();
			}

			/// <summary>Implements the <see cref="T:System.Runtime.Serialization.ISerializable" /> interface and returns the data needed to serialize the <see cref="T:System.Collections.Generic.SortedSet`1" /> instance.</summary>
			/// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object that contains the information required to serialize the <see cref="T:System.Collections.Generic.SortedSet`1" /> instance.</param>
			/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> object that contains the source and destination of the serialized stream associated with the <see cref="T:System.Collections.Generic.SortedSet`1" /> instance.</param>
			/// <exception cref="T:System.ArgumentNullException">
			///         <paramref name="info" /> is <see langword="null" />.</exception>
			// Token: 0x06002F27 RID: 12071 RVA: 0x0004B800 File Offset: 0x00049A00
			void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
			{
				throw new PlatformNotSupportedException();
			}

			/// <summary>Implements the <see cref="T:System.Runtime.Serialization.ISerializable" /> interface and raises the deserialization event when the deserialization is complete.</summary>
			/// <param name="sender">The source of the deserialization event.</param>
			/// <exception cref="T:System.Runtime.Serialization.SerializationException">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object associated with the current <see cref="T:System.Collections.Generic.SortedSet`1" /> instance is invalid.</exception>
			// Token: 0x06002F28 RID: 12072 RVA: 0x0004B800 File Offset: 0x00049A00
			void IDeserializationCallback.OnDeserialization(object sender)
			{
				throw new PlatformNotSupportedException();
			}

			// Token: 0x06002F29 RID: 12073 RVA: 0x000A2F8C File Offset: 0x000A118C
			private void Initialize()
			{
				this._current = null;
				SortedSet<T>.Node node = this._tree.root;
				while (node != null)
				{
					SortedSet<T>.Node node2 = this._reverse ? node.Right : node.Left;
					SortedSet<T>.Node node3 = this._reverse ? node.Left : node.Right;
					if (this._tree.IsWithinRange(node.Item))
					{
						this._stack.Push(node);
						node = node2;
					}
					else if (node2 == null || !this._tree.IsWithinRange(node2.Item))
					{
						node = node3;
					}
					else
					{
						node = node2;
					}
				}
			}

			/// <summary>Advances the enumerator to the next element of the <see cref="T:System.Collections.Generic.SortedSet`1" /> collection.</summary>
			/// <returns>
			///     <see langword="true" /> if the enumerator was successfully advanced to the next element; <see langword="false" /> if the enumerator has passed the end of the collection.</returns>
			/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
			// Token: 0x06002F2A RID: 12074 RVA: 0x000A3024 File Offset: 0x000A1224
			public bool MoveNext()
			{
				this._tree.VersionCheck();
				if (this._version != this._tree.version)
				{
					throw new InvalidOperationException("Collection was modified; enumeration operation may not execute.");
				}
				if (this._stack.Count == 0)
				{
					this._current = null;
					return false;
				}
				this._current = this._stack.Pop();
				SortedSet<T>.Node node = this._reverse ? this._current.Left : this._current.Right;
				while (node != null)
				{
					SortedSet<T>.Node node2 = this._reverse ? node.Right : node.Left;
					SortedSet<T>.Node node3 = this._reverse ? node.Left : node.Right;
					if (this._tree.IsWithinRange(node.Item))
					{
						this._stack.Push(node);
						node = node2;
					}
					else if (node3 == null || !this._tree.IsWithinRange(node3.Item))
					{
						node = node2;
					}
					else
					{
						node = node3;
					}
				}
				return true;
			}

			/// <summary>Releases all resources used by the <see cref="T:System.Collections.Generic.SortedSet`1.Enumerator" />. </summary>
			// Token: 0x06002F2B RID: 12075 RVA: 0x0000232D File Offset: 0x0000052D
			public void Dispose()
			{
			}

			/// <summary>Gets the element at the current position of the enumerator.</summary>
			/// <returns>The element in the collection at the current position of the enumerator.</returns>
			// Token: 0x17000BBC RID: 3004
			// (get) Token: 0x06002F2C RID: 12076 RVA: 0x000A311C File Offset: 0x000A131C
			public T Current
			{
				get
				{
					if (this._current != null)
					{
						return this._current.Item;
					}
					return default(T);
				}
			}

			/// <summary>Gets the element at the current position of the enumerator.</summary>
			/// <returns>The element in the collection at the current position of the enumerator.</returns>
			/// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first element of the collection or after the last element. </exception>
			// Token: 0x17000BBD RID: 3005
			// (get) Token: 0x06002F2D RID: 12077 RVA: 0x000A3146 File Offset: 0x000A1346
			object IEnumerator.Current
			{
				get
				{
					if (this._current == null)
					{
						throw new InvalidOperationException("Enumeration has either not started or has already finished.");
					}
					return this._current.Item;
				}
			}

			// Token: 0x17000BBE RID: 3006
			// (get) Token: 0x06002F2E RID: 12078 RVA: 0x000A316B File Offset: 0x000A136B
			internal bool NotStartedOrEnded
			{
				get
				{
					return this._current == null;
				}
			}

			// Token: 0x06002F2F RID: 12079 RVA: 0x000A3176 File Offset: 0x000A1376
			internal void Reset()
			{
				if (this._version != this._tree.version)
				{
					throw new InvalidOperationException("Collection was modified; enumeration operation may not execute.");
				}
				this._stack.Clear();
				this.Initialize();
			}

			/// <summary>Sets the enumerator to its initial position, which is before the first element in the collection.</summary>
			/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
			// Token: 0x06002F30 RID: 12080 RVA: 0x000A31A7 File Offset: 0x000A13A7
			void IEnumerator.Reset()
			{
				this.Reset();
			}

			// Token: 0x06002F31 RID: 12081 RVA: 0x000A31B0 File Offset: 0x000A13B0
			// Note: this type is marked as 'beforefieldinit'.
			static Enumerator()
			{
			}

			// Token: 0x04002422 RID: 9250
			private static readonly SortedSet<T>.Node s_dummyNode = new SortedSet<T>.Node(default(T), NodeColor.Red);

			// Token: 0x04002423 RID: 9251
			private SortedSet<T> _tree;

			// Token: 0x04002424 RID: 9252
			private int _version;

			// Token: 0x04002425 RID: 9253
			private Stack<SortedSet<T>.Node> _stack;

			// Token: 0x04002426 RID: 9254
			private SortedSet<T>.Node _current;

			// Token: 0x04002427 RID: 9255
			private bool _reverse;
		}

		// Token: 0x020005B6 RID: 1462
		internal struct ElementCount
		{
			// Token: 0x04002428 RID: 9256
			internal int UniqueCount;

			// Token: 0x04002429 RID: 9257
			internal int UnfoundCount;
		}

		// Token: 0x020005B7 RID: 1463
		[CompilerGenerated]
		private sealed class <>c__DisplayClass52_0
		{
			// Token: 0x06002F32 RID: 12082 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass52_0()
			{
			}

			// Token: 0x06002F33 RID: 12083 RVA: 0x000A31D4 File Offset: 0x000A13D4
			internal bool <CopyTo>b__0(SortedSet<T>.Node node)
			{
				if (this.index >= this.count)
				{
					return false;
				}
				T[] array = this.array;
				int num = this.index;
				this.index = num + 1;
				array[num] = node.Item;
				return true;
			}

			// Token: 0x0400242A RID: 9258
			public int index;

			// Token: 0x0400242B RID: 9259
			public int count;

			// Token: 0x0400242C RID: 9260
			public T[] array;
		}

		// Token: 0x020005B8 RID: 1464
		[CompilerGenerated]
		private sealed class <>c__DisplayClass53_0
		{
			// Token: 0x06002F34 RID: 12084 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass53_0()
			{
			}

			// Token: 0x0400242D RID: 9261
			public int index;
		}

		// Token: 0x020005B9 RID: 1465
		[CompilerGenerated]
		private sealed class <>c__DisplayClass53_1
		{
			// Token: 0x06002F35 RID: 12085 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass53_1()
			{
			}

			// Token: 0x06002F36 RID: 12086 RVA: 0x000A3214 File Offset: 0x000A1414
			internal bool <System.Collections.ICollection.CopyTo>b__0(SortedSet<T>.Node node)
			{
				object[] array = this.objects;
				int index = this.CS$<>8__locals1.index;
				this.CS$<>8__locals1.index = index + 1;
				array[index] = node.Item;
				return true;
			}

			// Token: 0x0400242E RID: 9262
			public object[] objects;

			// Token: 0x0400242F RID: 9263
			public SortedSet<T>.<>c__DisplayClass53_0 CS$<>8__locals1;
		}

		// Token: 0x020005BA RID: 1466
		[CompilerGenerated]
		private sealed class <>c__DisplayClass85_0
		{
			// Token: 0x06002F37 RID: 12087 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass85_0()
			{
			}

			// Token: 0x06002F38 RID: 12088 RVA: 0x000A324F File Offset: 0x000A144F
			internal bool <RemoveWhere>b__0(SortedSet<T>.Node n)
			{
				if (this.match(n.Item))
				{
					this.matches.Add(n.Item);
				}
				return true;
			}

			// Token: 0x04002430 RID: 9264
			public Predicate<T> match;

			// Token: 0x04002431 RID: 9265
			public List<T> matches;
		}

		// Token: 0x020005BB RID: 1467
		[CompilerGenerated]
		private sealed class <Reverse>d__94 : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06002F39 RID: 12089 RVA: 0x000A3276 File Offset: 0x000A1476
			[DebuggerHidden]
			public <Reverse>d__94(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06002F3A RID: 12090 RVA: 0x0000232D File Offset: 0x0000052D
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06002F3B RID: 12091 RVA: 0x000A3290 File Offset: 0x000A1490
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				SortedSet<T> set = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
				}
				else
				{
					this.<>1__state = -1;
					e = new SortedSet<T>.Enumerator(set, true);
				}
				if (!e.MoveNext())
				{
					return false;
				}
				this.<>2__current = e.Current;
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000BBF RID: 3007
			// (get) Token: 0x06002F3C RID: 12092 RVA: 0x000A32F9 File Offset: 0x000A14F9
			T IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06002F3D RID: 12093 RVA: 0x00006740 File Offset: 0x00004940
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000BC0 RID: 3008
			// (get) Token: 0x06002F3E RID: 12094 RVA: 0x000A3301 File Offset: 0x000A1501
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06002F3F RID: 12095 RVA: 0x000A3310 File Offset: 0x000A1510
			[DebuggerHidden]
			IEnumerator<T> IEnumerable<!0>.GetEnumerator()
			{
				SortedSet<T>.<Reverse>d__94 <Reverse>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<Reverse>d__ = this;
				}
				else
				{
					<Reverse>d__ = new SortedSet<T>.<Reverse>d__94(0);
					<Reverse>d__.<>4__this = this;
				}
				return <Reverse>d__;
			}

			// Token: 0x06002F40 RID: 12096 RVA: 0x000A3353 File Offset: 0x000A1553
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<T>.GetEnumerator();
			}

			// Token: 0x04002432 RID: 9266
			private int <>1__state;

			// Token: 0x04002433 RID: 9267
			private T <>2__current;

			// Token: 0x04002434 RID: 9268
			private int <>l__initialThreadId;

			// Token: 0x04002435 RID: 9269
			public SortedSet<T> <>4__this;

			// Token: 0x04002436 RID: 9270
			private SortedSet<T>.Enumerator <e>5__1;
		}
	}
}
