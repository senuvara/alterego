﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000592 RID: 1426
	internal sealed class IDictionaryDebugView<K, V>
	{
		// Token: 0x06002D6A RID: 11626 RVA: 0x0009D504 File Offset: 0x0009B704
		public IDictionaryDebugView(IDictionary<K, V> dictionary)
		{
			if (dictionary == null)
			{
				throw new ArgumentNullException("dictionary");
			}
			this._dict = dictionary;
		}

		// Token: 0x17000B4C RID: 2892
		// (get) Token: 0x06002D6B RID: 11627 RVA: 0x0009D524 File Offset: 0x0009B724
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public KeyValuePair<K, V>[] Items
		{
			get
			{
				KeyValuePair<K, V>[] array = new KeyValuePair<K, V>[this._dict.Count];
				this._dict.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x040023A9 RID: 9129
		private readonly IDictionary<K, V> _dict;
	}
}
