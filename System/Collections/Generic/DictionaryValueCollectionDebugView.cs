﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000594 RID: 1428
	internal sealed class DictionaryValueCollectionDebugView<TKey, TValue>
	{
		// Token: 0x06002D6E RID: 11630 RVA: 0x0009D59C File Offset: 0x0009B79C
		public DictionaryValueCollectionDebugView(ICollection<TValue> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			this._collection = collection;
		}

		// Token: 0x17000B4E RID: 2894
		// (get) Token: 0x06002D6F RID: 11631 RVA: 0x0009D5BC File Offset: 0x0009B7BC
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public TValue[] Items
		{
			get
			{
				TValue[] array = new TValue[this._collection.Count];
				this._collection.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x040023AB RID: 9131
		private readonly ICollection<TValue> _collection;
	}
}
