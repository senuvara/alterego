﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x020005C2 RID: 1474
	internal sealed class StackDebugView<T>
	{
		// Token: 0x06002F67 RID: 12135 RVA: 0x000A3A43 File Offset: 0x000A1C43
		public StackDebugView(Stack<T> stack)
		{
			if (stack == null)
			{
				throw new ArgumentNullException("stack");
			}
			this._stack = stack;
		}

		// Token: 0x17000BC6 RID: 3014
		// (get) Token: 0x06002F68 RID: 12136 RVA: 0x000A3A60 File Offset: 0x000A1C60
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				return this._stack.ToArray();
			}
		}

		// Token: 0x0400244A RID: 9290
		private readonly Stack<T> _stack;
	}
}
