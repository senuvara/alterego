﻿using System;

namespace System.Collections.Generic
{
	/// <summary>Represents a node in a <see cref="T:System.Collections.Generic.LinkedList`1" />. This class cannot be inherited.</summary>
	/// <typeparam name="T">Specifies the element type of the linked list.</typeparam>
	// Token: 0x02000597 RID: 1431
	public sealed class LinkedListNode<T>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Generic.LinkedListNode`1" /> class, containing the specified value.</summary>
		/// <param name="value">The value to contain in the <see cref="T:System.Collections.Generic.LinkedListNode`1" />.</param>
		// Token: 0x06002D9F RID: 11679 RVA: 0x0009DF37 File Offset: 0x0009C137
		public LinkedListNode(T value)
		{
			this.item = value;
		}

		// Token: 0x06002DA0 RID: 11680 RVA: 0x0009DF46 File Offset: 0x0009C146
		internal LinkedListNode(LinkedList<T> list, T value)
		{
			this.list = list;
			this.item = value;
		}

		/// <summary>Gets the <see cref="T:System.Collections.Generic.LinkedList`1" /> that the <see cref="T:System.Collections.Generic.LinkedListNode`1" /> belongs to.</summary>
		/// <returns>A reference to the <see cref="T:System.Collections.Generic.LinkedList`1" /> that the <see cref="T:System.Collections.Generic.LinkedListNode`1" /> belongs to, or <see langword="null" /> if the <see cref="T:System.Collections.Generic.LinkedListNode`1" /> is not linked.</returns>
		// Token: 0x17000B57 RID: 2903
		// (get) Token: 0x06002DA1 RID: 11681 RVA: 0x0009DF5C File Offset: 0x0009C15C
		public LinkedList<T> List
		{
			get
			{
				return this.list;
			}
		}

		/// <summary>Gets the next node in the <see cref="T:System.Collections.Generic.LinkedList`1" />.</summary>
		/// <returns>A reference to the next node in the <see cref="T:System.Collections.Generic.LinkedList`1" />, or <see langword="null" /> if the current node is the last element (<see cref="P:System.Collections.Generic.LinkedList`1.Last" />) of the <see cref="T:System.Collections.Generic.LinkedList`1" />.</returns>
		// Token: 0x17000B58 RID: 2904
		// (get) Token: 0x06002DA2 RID: 11682 RVA: 0x0009DF64 File Offset: 0x0009C164
		public LinkedListNode<T> Next
		{
			get
			{
				if (this.next != null && this.next != this.list.head)
				{
					return this.next;
				}
				return null;
			}
		}

		/// <summary>Gets the previous node in the <see cref="T:System.Collections.Generic.LinkedList`1" />.</summary>
		/// <returns>A reference to the previous node in the <see cref="T:System.Collections.Generic.LinkedList`1" />, or <see langword="null" /> if the current node is the first element (<see cref="P:System.Collections.Generic.LinkedList`1.First" />) of the <see cref="T:System.Collections.Generic.LinkedList`1" />.</returns>
		// Token: 0x17000B59 RID: 2905
		// (get) Token: 0x06002DA3 RID: 11683 RVA: 0x0009DF89 File Offset: 0x0009C189
		public LinkedListNode<T> Previous
		{
			get
			{
				if (this.prev != null && this != this.list.head)
				{
					return this.prev;
				}
				return null;
			}
		}

		/// <summary>Gets the value contained in the node.</summary>
		/// <returns>The value contained in the node.</returns>
		// Token: 0x17000B5A RID: 2906
		// (get) Token: 0x06002DA4 RID: 11684 RVA: 0x0009DFA9 File Offset: 0x0009C1A9
		// (set) Token: 0x06002DA5 RID: 11685 RVA: 0x0009DFB1 File Offset: 0x0009C1B1
		public T Value
		{
			get
			{
				return this.item;
			}
			set
			{
				this.item = value;
			}
		}

		// Token: 0x06002DA6 RID: 11686 RVA: 0x0009DFBA File Offset: 0x0009C1BA
		internal void Invalidate()
		{
			this.list = null;
			this.next = null;
			this.prev = null;
		}

		// Token: 0x040023BD RID: 9149
		internal LinkedList<T> list;

		// Token: 0x040023BE RID: 9150
		internal LinkedListNode<T> next;

		// Token: 0x040023BF RID: 9151
		internal LinkedListNode<T> prev;

		// Token: 0x040023C0 RID: 9152
		internal T item;
	}
}
