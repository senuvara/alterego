﻿using System;
using System.Runtime.Serialization;

namespace System.Collections.Generic
{
	// Token: 0x020005AA RID: 1450
	[Serializable]
	internal sealed class TreeSet<T> : SortedSet<T>
	{
		// Token: 0x06002E34 RID: 11828 RVA: 0x0009F74E File Offset: 0x0009D94E
		public TreeSet()
		{
		}

		// Token: 0x06002E35 RID: 11829 RVA: 0x0009F756 File Offset: 0x0009D956
		public TreeSet(IComparer<T> comparer) : base(comparer)
		{
		}

		// Token: 0x06002E36 RID: 11830 RVA: 0x0009F75F File Offset: 0x0009D95F
		public TreeSet(SerializationInfo siInfo, StreamingContext context) : base(siInfo, context)
		{
		}

		// Token: 0x06002E37 RID: 11831 RVA: 0x0009F769 File Offset: 0x0009D969
		internal override bool AddIfNotPresent(T item)
		{
			bool flag = base.AddIfNotPresent(item);
			if (!flag)
			{
				throw new ArgumentException(SR.Format("An item with the same key has already been added. Key: {0}", item));
			}
			return flag;
		}
	}
}
