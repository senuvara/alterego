﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000591 RID: 1425
	internal sealed class ICollectionDebugView<T>
	{
		// Token: 0x06002D68 RID: 11624 RVA: 0x0009D4BA File Offset: 0x0009B6BA
		public ICollectionDebugView(ICollection<T> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			this._collection = collection;
		}

		// Token: 0x17000B4B RID: 2891
		// (get) Token: 0x06002D69 RID: 11625 RVA: 0x0009D4D8 File Offset: 0x0009B6D8
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				T[] array = new T[this._collection.Count];
				this._collection.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x040023A8 RID: 9128
		private readonly ICollection<T> _collection;
	}
}
