﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x0200059A RID: 1434
	internal sealed class QueueDebugView<T>
	{
		// Token: 0x06002DC5 RID: 11717 RVA: 0x0009E876 File Offset: 0x0009CA76
		public QueueDebugView(Queue<T> queue)
		{
			if (queue == null)
			{
				throw new ArgumentNullException("queue");
			}
			this._queue = queue;
		}

		// Token: 0x17000B60 RID: 2912
		// (get) Token: 0x06002DC6 RID: 11718 RVA: 0x0009E893 File Offset: 0x0009CA93
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				return this._queue.ToArray();
			}
		}

		// Token: 0x040023CD RID: 9165
		private readonly Queue<T> _queue;
	}
}
