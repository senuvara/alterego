﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Collections.Generic
{
	// Token: 0x0200058D RID: 1421
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	internal struct CopyPosition
	{
		// Token: 0x06002D50 RID: 11600 RVA: 0x0009CFAC File Offset: 0x0009B1AC
		internal CopyPosition(int row, int column)
		{
			this.Row = row;
			this.Column = column;
		}

		// Token: 0x17000B46 RID: 2886
		// (get) Token: 0x06002D51 RID: 11601 RVA: 0x0009CFBC File Offset: 0x0009B1BC
		public static CopyPosition Start
		{
			get
			{
				return default(CopyPosition);
			}
		}

		// Token: 0x17000B47 RID: 2887
		// (get) Token: 0x06002D52 RID: 11602 RVA: 0x0009CFD2 File Offset: 0x0009B1D2
		internal int Row
		{
			[CompilerGenerated]
			get
			{
				return this.<Row>k__BackingField;
			}
		}

		// Token: 0x17000B48 RID: 2888
		// (get) Token: 0x06002D53 RID: 11603 RVA: 0x0009CFDA File Offset: 0x0009B1DA
		internal int Column
		{
			[CompilerGenerated]
			get
			{
				return this.<Column>k__BackingField;
			}
		}

		// Token: 0x06002D54 RID: 11604 RVA: 0x0009CFE2 File Offset: 0x0009B1E2
		public CopyPosition Normalize(int endColumn)
		{
			if (this.Column != endColumn)
			{
				return this;
			}
			return new CopyPosition(this.Row + 1, 0);
		}

		// Token: 0x17000B49 RID: 2889
		// (get) Token: 0x06002D55 RID: 11605 RVA: 0x0009D002 File Offset: 0x0009B202
		private string DebuggerDisplay
		{
			get
			{
				return string.Format("[{0}, {1}]", this.Row, this.Column);
			}
		}

		// Token: 0x04002395 RID: 9109
		[CompilerGenerated]
		private readonly int <Row>k__BackingField;

		// Token: 0x04002396 RID: 9110
		[CompilerGenerated]
		private readonly int <Column>k__BackingField;
	}
}
