﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Collections.Generic
{
	// Token: 0x0200058E RID: 1422
	internal struct LargeArrayBuilder<T>
	{
		// Token: 0x06002D56 RID: 11606 RVA: 0x0009D024 File Offset: 0x0009B224
		public LargeArrayBuilder(bool initialize)
		{
			this = new LargeArrayBuilder<T>(int.MaxValue);
		}

		// Token: 0x06002D57 RID: 11607 RVA: 0x0009D034 File Offset: 0x0009B234
		public LargeArrayBuilder(int maxCapacity)
		{
			this = default(LargeArrayBuilder<T>);
			this._first = (this._current = Array.Empty<T>());
			this._maxCapacity = maxCapacity;
		}

		// Token: 0x17000B4A RID: 2890
		// (get) Token: 0x06002D58 RID: 11608 RVA: 0x0009D063 File Offset: 0x0009B263
		public int Count
		{
			get
			{
				return this._count;
			}
		}

		// Token: 0x06002D59 RID: 11609 RVA: 0x0009D06C File Offset: 0x0009B26C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Add(T item)
		{
			if (this._index == this._current.Length)
			{
				this.AllocateBuffer();
			}
			T[] current = this._current;
			int index = this._index;
			this._index = index + 1;
			current[index] = item;
			this._count++;
		}

		// Token: 0x06002D5A RID: 11610 RVA: 0x0009D0BC File Offset: 0x0009B2BC
		public void AddRange(IEnumerable<T> items)
		{
			using (IEnumerator<T> enumerator = items.GetEnumerator())
			{
				T[] current = this._current;
				int index = this._index;
				while (enumerator.MoveNext())
				{
					if (index == current.Length)
					{
						this._count += index - this._index;
						this._index = index;
						this.AllocateBuffer();
						current = this._current;
						index = this._index;
					}
					current[index++] = enumerator.Current;
				}
				this._count += index - this._index;
				this._index = index;
			}
		}

		// Token: 0x06002D5B RID: 11611 RVA: 0x0009D168 File Offset: 0x0009B368
		public void CopyTo(T[] array, int arrayIndex, int count)
		{
			int num = 0;
			while (count > 0)
			{
				T[] buffer = this.GetBuffer(num);
				int num2 = Math.Min(count, buffer.Length);
				Array.Copy(buffer, 0, array, arrayIndex, num2);
				count -= num2;
				arrayIndex += num2;
				num++;
			}
		}

		// Token: 0x06002D5C RID: 11612 RVA: 0x0009D1A8 File Offset: 0x0009B3A8
		public CopyPosition CopyTo(CopyPosition position, T[] array, int arrayIndex, int count)
		{
			LargeArrayBuilder<T>.<>c__DisplayClass15_0 CS$<>8__locals1;
			CS$<>8__locals1.count = count;
			CS$<>8__locals1.array = array;
			CS$<>8__locals1.arrayIndex = arrayIndex;
			int num = position.Row;
			int column = position.Column;
			T[] buffer = this.GetBuffer(num);
			int num2 = LargeArrayBuilder<T>.<CopyTo>g__CopyToCore|15_0(buffer, column, ref CS$<>8__locals1);
			if (CS$<>8__locals1.count == 0)
			{
				return new CopyPosition(num, column + num2).Normalize(buffer.Length);
			}
			do
			{
				buffer = this.GetBuffer(++num);
				num2 = LargeArrayBuilder<T>.<CopyTo>g__CopyToCore|15_0(buffer, column, ref CS$<>8__locals1);
			}
			while (CS$<>8__locals1.count > 0);
			return new CopyPosition(num, num2).Normalize(buffer.Length);
		}

		// Token: 0x06002D5D RID: 11613 RVA: 0x0009D244 File Offset: 0x0009B444
		public T[] GetBuffer(int index)
		{
			if (index == 0)
			{
				return this._first;
			}
			if (index > this._buffers.Count)
			{
				return this._current;
			}
			return this._buffers[index - 1];
		}

		// Token: 0x06002D5E RID: 11614 RVA: 0x0009D273 File Offset: 0x0009B473
		[MethodImpl(MethodImplOptions.NoInlining)]
		public void SlowAdd(T item)
		{
			this.Add(item);
		}

		// Token: 0x06002D5F RID: 11615 RVA: 0x0009D27C File Offset: 0x0009B47C
		public T[] ToArray()
		{
			T[] array;
			if (this.TryMove(out array))
			{
				return array;
			}
			array = new T[this._count];
			this.CopyTo(array, 0, this._count);
			return array;
		}

		// Token: 0x06002D60 RID: 11616 RVA: 0x0009D2B0 File Offset: 0x0009B4B0
		public bool TryMove(out T[] array)
		{
			array = this._first;
			return this._count == this._first.Length;
		}

		// Token: 0x06002D61 RID: 11617 RVA: 0x0009D2CC File Offset: 0x0009B4CC
		private void AllocateBuffer()
		{
			if (this._count < 8)
			{
				int num = Math.Min((this._count == 0) ? 4 : (this._count * 2), this._maxCapacity);
				this._current = new T[num];
				Array.Copy(this._first, 0, this._current, 0, this._count);
				this._first = this._current;
				return;
			}
			int num2;
			if (this._count == 8)
			{
				num2 = 8;
			}
			else
			{
				this._buffers.Add(this._current);
				num2 = Math.Min(this._count, this._maxCapacity - this._count);
			}
			this._current = new T[num2];
			this._index = 0;
		}

		// Token: 0x06002D62 RID: 11618 RVA: 0x0009D380 File Offset: 0x0009B580
		[CompilerGenerated]
		internal static int <CopyTo>g__CopyToCore|15_0(T[] sourceBuffer, int sourceIndex, ref LargeArrayBuilder<T>.<>c__DisplayClass15_0 A_2)
		{
			int num = Math.Min(sourceBuffer.Length - sourceIndex, A_2.count);
			Array.Copy(sourceBuffer, sourceIndex, A_2.array, A_2.arrayIndex, num);
			A_2.arrayIndex += num;
			A_2.count -= num;
			return num;
		}

		// Token: 0x04002397 RID: 9111
		private const int StartingCapacity = 4;

		// Token: 0x04002398 RID: 9112
		private const int ResizeLimit = 8;

		// Token: 0x04002399 RID: 9113
		private readonly int _maxCapacity;

		// Token: 0x0400239A RID: 9114
		private T[] _first;

		// Token: 0x0400239B RID: 9115
		private ArrayBuilder<T[]> _buffers;

		// Token: 0x0400239C RID: 9116
		private T[] _current;

		// Token: 0x0400239D RID: 9117
		private int _index;

		// Token: 0x0400239E RID: 9118
		private int _count;

		// Token: 0x0200058F RID: 1423
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <>c__DisplayClass15_0
		{
			// Token: 0x0400239F RID: 9119
			public int count;

			// Token: 0x040023A0 RID: 9120
			public T[] array;

			// Token: 0x040023A1 RID: 9121
			public int arrayIndex;
		}
	}
}
