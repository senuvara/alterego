﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x020005BE RID: 1470
	internal enum TreeRotation : byte
	{
		// Token: 0x0400243B RID: 9275
		Left,
		// Token: 0x0400243C RID: 9276
		LeftRight,
		// Token: 0x0400243D RID: 9277
		Right,
		// Token: 0x0400243E RID: 9278
		RightLeft
	}
}
