﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x0200058B RID: 1419
	internal struct ArrayBuilder<T>
	{
		// Token: 0x06002D41 RID: 11585 RVA: 0x0009CC7D File Offset: 0x0009AE7D
		public ArrayBuilder(int capacity)
		{
			this = default(ArrayBuilder<T>);
			if (capacity > 0)
			{
				this._array = new T[capacity];
			}
		}

		// Token: 0x17000B43 RID: 2883
		// (get) Token: 0x06002D42 RID: 11586 RVA: 0x0009CC96 File Offset: 0x0009AE96
		public int Capacity
		{
			get
			{
				T[] array = this._array;
				if (array == null)
				{
					return 0;
				}
				return array.Length;
			}
		}

		// Token: 0x17000B44 RID: 2884
		// (get) Token: 0x06002D43 RID: 11587 RVA: 0x0009CCA6 File Offset: 0x0009AEA6
		public int Count
		{
			get
			{
				return this._count;
			}
		}

		// Token: 0x17000B45 RID: 2885
		public T this[int index]
		{
			get
			{
				return this._array[index];
			}
			set
			{
				this._array[index] = value;
			}
		}

		// Token: 0x06002D46 RID: 11590 RVA: 0x0009CCCB File Offset: 0x0009AECB
		public void Add(T item)
		{
			if (this._count == this.Capacity)
			{
				this.EnsureCapacity(this._count + 1);
			}
			this.UncheckedAdd(item);
		}

		// Token: 0x06002D47 RID: 11591 RVA: 0x0009CCF0 File Offset: 0x0009AEF0
		public T First()
		{
			return this._array[0];
		}

		// Token: 0x06002D48 RID: 11592 RVA: 0x0009CCFE File Offset: 0x0009AEFE
		public T Last()
		{
			return this._array[this._count - 1];
		}

		// Token: 0x06002D49 RID: 11593 RVA: 0x0009CD14 File Offset: 0x0009AF14
		public T[] ToArray()
		{
			if (this._count == 0)
			{
				return Array.Empty<T>();
			}
			T[] array = this._array;
			if (this._count < array.Length)
			{
				array = new T[this._count];
				Array.Copy(this._array, 0, array, 0, this._count);
			}
			return array;
		}

		// Token: 0x06002D4A RID: 11594 RVA: 0x0009CD64 File Offset: 0x0009AF64
		public void UncheckedAdd(T item)
		{
			T[] array = this._array;
			int count = this._count;
			this._count = count + 1;
			array[count] = item;
		}

		// Token: 0x06002D4B RID: 11595 RVA: 0x0009CD90 File Offset: 0x0009AF90
		private void EnsureCapacity(int minimum)
		{
			int capacity = this.Capacity;
			int num = (capacity == 0) ? 4 : (2 * capacity);
			if (num > 2146435071)
			{
				num = Math.Max(capacity + 1, 2146435071);
			}
			num = Math.Max(num, minimum);
			T[] array = new T[num];
			if (this._count > 0)
			{
				Array.Copy(this._array, 0, array, 0, this._count);
			}
			this._array = array;
		}

		// Token: 0x04002391 RID: 9105
		private const int DefaultCapacity = 4;

		// Token: 0x04002392 RID: 9106
		private const int MaxCoreClrArrayLength = 2146435071;

		// Token: 0x04002393 RID: 9107
		private T[] _array;

		// Token: 0x04002394 RID: 9108
		private int _count;
	}
}
