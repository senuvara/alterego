﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000590 RID: 1424
	internal sealed class BitHelper
	{
		// Token: 0x06002D63 RID: 11619 RVA: 0x0009D3CF File Offset: 0x0009B5CF
		internal unsafe BitHelper(int* bitArrayPtr, int length)
		{
			this._arrayPtr = bitArrayPtr;
			this._length = length;
			this._useStackAlloc = true;
		}

		// Token: 0x06002D64 RID: 11620 RVA: 0x0009D3EC File Offset: 0x0009B5EC
		internal BitHelper(int[] bitArray, int length)
		{
			this._array = bitArray;
			this._length = length;
		}

		// Token: 0x06002D65 RID: 11621 RVA: 0x0009D404 File Offset: 0x0009B604
		internal unsafe void MarkBit(int bitPosition)
		{
			int num = bitPosition / 32;
			if (num < this._length && num >= 0)
			{
				int num2 = 1 << bitPosition % 32;
				if (this._useStackAlloc)
				{
					this._arrayPtr[num] |= num2;
					return;
				}
				this._array[num] |= num2;
			}
		}

		// Token: 0x06002D66 RID: 11622 RVA: 0x0009D458 File Offset: 0x0009B658
		internal unsafe bool IsMarked(int bitPosition)
		{
			int num = bitPosition / 32;
			if (num >= this._length || num < 0)
			{
				return false;
			}
			int num2 = 1 << bitPosition % 32;
			if (this._useStackAlloc)
			{
				return (this._arrayPtr[num] & num2) != 0;
			}
			return (this._array[num] & num2) != 0;
		}

		// Token: 0x06002D67 RID: 11623 RVA: 0x0009D4AA File Offset: 0x0009B6AA
		internal static int ToIntArrayLength(int n)
		{
			if (n <= 0)
			{
				return 0;
			}
			return (n - 1) / 32 + 1;
		}

		// Token: 0x040023A2 RID: 9122
		private const byte MarkedBitFlag = 1;

		// Token: 0x040023A3 RID: 9123
		private const byte IntSize = 32;

		// Token: 0x040023A4 RID: 9124
		private readonly int _length;

		// Token: 0x040023A5 RID: 9125
		private unsafe readonly int* _arrayPtr;

		// Token: 0x040023A6 RID: 9126
		private readonly int[] _array;

		// Token: 0x040023A7 RID: 9127
		private readonly bool _useStackAlloc;
	}
}
