﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000593 RID: 1427
	internal sealed class DictionaryKeyCollectionDebugView<TKey, TValue>
	{
		// Token: 0x06002D6C RID: 11628 RVA: 0x0009D550 File Offset: 0x0009B750
		public DictionaryKeyCollectionDebugView(ICollection<TKey> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			this._collection = collection;
		}

		// Token: 0x17000B4D RID: 2893
		// (get) Token: 0x06002D6D RID: 11629 RVA: 0x0009D570 File Offset: 0x0009B770
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public TKey[] Items
		{
			get
			{
				TKey[] array = new TKey[this._collection.Count];
				this._collection.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x040023AA RID: 9130
		private readonly ICollection<TKey> _collection;
	}
}
