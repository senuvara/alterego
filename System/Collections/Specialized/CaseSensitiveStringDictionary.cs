﻿using System;

namespace System.Collections.Specialized
{
	// Token: 0x02000561 RID: 1377
	internal class CaseSensitiveStringDictionary : StringDictionary
	{
		// Token: 0x06002B50 RID: 11088 RVA: 0x000974A7 File Offset: 0x000956A7
		public CaseSensitiveStringDictionary()
		{
		}

		// Token: 0x17000ACF RID: 2767
		public override string this[string key]
		{
			get
			{
				if (key == null)
				{
					throw new ArgumentNullException("key");
				}
				return (string)this.contents[key];
			}
			set
			{
				if (key == null)
				{
					throw new ArgumentNullException("key");
				}
				this.contents[key] = value;
			}
		}

		// Token: 0x06002B53 RID: 11091 RVA: 0x000974ED File Offset: 0x000956ED
		public override void Add(string key, string value)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.contents.Add(key, value);
		}

		// Token: 0x06002B54 RID: 11092 RVA: 0x0009750A File Offset: 0x0009570A
		public override bool ContainsKey(string key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			return this.contents.ContainsKey(key);
		}

		// Token: 0x06002B55 RID: 11093 RVA: 0x00097526 File Offset: 0x00095726
		public override void Remove(string key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.contents.Remove(key);
		}
	}
}
