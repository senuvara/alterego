﻿using System;
using System.Globalization;

namespace System.Collections.Specialized
{
	// Token: 0x0200056F RID: 1391
	[Serializable]
	internal class CompatibleComparer : IEqualityComparer
	{
		// Token: 0x06002BCF RID: 11215 RVA: 0x00098D7B File Offset: 0x00096F7B
		internal CompatibleComparer(IComparer comparer, IHashCodeProvider hashCodeProvider)
		{
			this._comparer = comparer;
			this._hcp = hashCodeProvider;
		}

		// Token: 0x06002BD0 RID: 11216 RVA: 0x00098D94 File Offset: 0x00096F94
		public bool Equals(object a, object b)
		{
			if (a == b)
			{
				return true;
			}
			if (a == null || b == null)
			{
				return false;
			}
			try
			{
				if (this._comparer != null)
				{
					return this._comparer.Compare(a, b) == 0;
				}
				IComparable comparable = a as IComparable;
				if (comparable != null)
				{
					return comparable.CompareTo(b) == 0;
				}
			}
			catch (ArgumentException)
			{
				return false;
			}
			return a.Equals(b);
		}

		// Token: 0x06002BD1 RID: 11217 RVA: 0x00098E04 File Offset: 0x00097004
		public int GetHashCode(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (this._hcp != null)
			{
				return this._hcp.GetHashCode(obj);
			}
			return obj.GetHashCode();
		}

		// Token: 0x17000AF5 RID: 2805
		// (get) Token: 0x06002BD2 RID: 11218 RVA: 0x00098E2F File Offset: 0x0009702F
		public IComparer Comparer
		{
			get
			{
				return this._comparer;
			}
		}

		// Token: 0x17000AF6 RID: 2806
		// (get) Token: 0x06002BD3 RID: 11219 RVA: 0x00098E37 File Offset: 0x00097037
		public IHashCodeProvider HashCodeProvider
		{
			get
			{
				return this._hcp;
			}
		}

		// Token: 0x17000AF7 RID: 2807
		// (get) Token: 0x06002BD4 RID: 11220 RVA: 0x00098E3F File Offset: 0x0009703F
		public static IComparer DefaultComparer
		{
			get
			{
				if (CompatibleComparer.defaultComparer == null)
				{
					CompatibleComparer.defaultComparer = new CaseInsensitiveComparer(CultureInfo.InvariantCulture);
				}
				return CompatibleComparer.defaultComparer;
			}
		}

		// Token: 0x17000AF8 RID: 2808
		// (get) Token: 0x06002BD5 RID: 11221 RVA: 0x00098E62 File Offset: 0x00097062
		public static IHashCodeProvider DefaultHashCodeProvider
		{
			get
			{
				if (CompatibleComparer.defaultHashProvider == null)
				{
					CompatibleComparer.defaultHashProvider = new CaseInsensitiveHashCodeProvider(CultureInfo.InvariantCulture);
				}
				return CompatibleComparer.defaultHashProvider;
			}
		}

		// Token: 0x04002324 RID: 8996
		private IComparer _comparer;

		// Token: 0x04002325 RID: 8997
		private static volatile IComparer defaultComparer;

		// Token: 0x04002326 RID: 8998
		private IHashCodeProvider _hcp;

		// Token: 0x04002327 RID: 8999
		private static volatile IHashCodeProvider defaultHashProvider;
	}
}
