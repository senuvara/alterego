﻿using System;
using Unity;

namespace System.Collections.Specialized
{
	/// <summary>Supports a simple iteration over a <see cref="T:System.Collections.Specialized.StringCollection" />.</summary>
	// Token: 0x02000578 RID: 1400
	public class StringEnumerator
	{
		// Token: 0x06002C57 RID: 11351 RVA: 0x0009A243 File Offset: 0x00098443
		internal StringEnumerator(StringCollection mappings)
		{
			this.temp = mappings;
			this.baseEnumerator = this.temp.GetEnumerator();
		}

		/// <summary>Gets the current element in the collection.</summary>
		/// <returns>The current element in the collection.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first element of the collection or after the last element. </exception>
		// Token: 0x17000B1B RID: 2843
		// (get) Token: 0x06002C58 RID: 11352 RVA: 0x0009A263 File Offset: 0x00098463
		public string Current
		{
			get
			{
				return (string)this.baseEnumerator.Current;
			}
		}

		/// <summary>Advances the enumerator to the next element of the collection.</summary>
		/// <returns>
		///     <see langword="true" /> if the enumerator was successfully advanced to the next element; <see langword="false" /> if the enumerator has passed the end of the collection.</returns>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
		// Token: 0x06002C59 RID: 11353 RVA: 0x0009A275 File Offset: 0x00098475
		public bool MoveNext()
		{
			return this.baseEnumerator.MoveNext();
		}

		/// <summary>Sets the enumerator to its initial position, which is before the first element in the collection.</summary>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
		// Token: 0x06002C5A RID: 11354 RVA: 0x0009A282 File Offset: 0x00098482
		public void Reset()
		{
			this.baseEnumerator.Reset();
		}

		// Token: 0x06002C5B RID: 11355 RVA: 0x000092E2 File Offset: 0x000074E2
		internal StringEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04002348 RID: 9032
		private IEnumerator baseEnumerator;

		// Token: 0x04002349 RID: 9033
		private IEnumerable temp;
	}
}
