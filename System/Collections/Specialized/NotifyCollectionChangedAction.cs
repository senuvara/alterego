﻿using System;

namespace System.Collections.Specialized
{
	/// <summary>Describes the action that caused a <see cref="E:System.Collections.Specialized.INotifyCollectionChanged.CollectionChanged" /> event. </summary>
	// Token: 0x02000571 RID: 1393
	public enum NotifyCollectionChangedAction
	{
		/// <summary>An item was added to the collection.</summary>
		// Token: 0x0400232B RID: 9003
		Add,
		/// <summary>An item was removed from the collection.</summary>
		// Token: 0x0400232C RID: 9004
		Remove,
		/// <summary>An item was replaced in the collection.</summary>
		// Token: 0x0400232D RID: 9005
		Replace,
		/// <summary>An item was moved within the collection.</summary>
		// Token: 0x0400232E RID: 9006
		Move,
		/// <summary>The content of the collection was cleared.</summary>
		// Token: 0x0400232F RID: 9007
		Reset
	}
}
