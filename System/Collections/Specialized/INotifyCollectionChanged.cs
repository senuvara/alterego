﻿using System;

namespace System.Collections.Specialized
{
	/// <summary>Notifies listeners of dynamic changes, such as when an item is added and removed or the whole list is cleared.</summary>
	// Token: 0x02000564 RID: 1380
	public interface INotifyCollectionChanged
	{
		/// <summary>Occurs when the collection changes.</summary>
		// Token: 0x1400004A RID: 74
		// (add) Token: 0x06002B71 RID: 11121
		// (remove) Token: 0x06002B72 RID: 11122
		event NotifyCollectionChangedEventHandler CollectionChanged;
	}
}
