﻿using System;

namespace System.Collections.Specialized
{
	/// <summary>Represents the method that handles the <see cref="E:System.Collections.Specialized.INotifyCollectionChanged.CollectionChanged" /> event. </summary>
	/// <param name="sender">The object that raised the event.</param>
	/// <param name="e">Information about the event.</param>
	// Token: 0x02000573 RID: 1395
	// (Invoke) Token: 0x06002C0A RID: 11274
	public delegate void NotifyCollectionChangedEventHandler(object sender, NotifyCollectionChangedEventArgs e);
}
