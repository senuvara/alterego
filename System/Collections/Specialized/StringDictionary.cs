﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace System.Collections.Specialized
{
	/// <summary>Implements a hash table with the key and the value strongly typed to be strings rather than objects.</summary>
	// Token: 0x02000579 RID: 1401
	[DesignerSerializer("System.Diagnostics.Design.StringDictionaryCodeDomSerializer, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.ComponentModel.Design.Serialization.CodeDomSerializer, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[Serializable]
	public class StringDictionary : IEnumerable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Specialized.StringDictionary" /> class.</summary>
		// Token: 0x06002C5C RID: 11356 RVA: 0x0009A28F File Offset: 0x0009848F
		public StringDictionary()
		{
		}

		/// <summary>Gets the number of key/value pairs in the <see cref="T:System.Collections.Specialized.StringDictionary" />.</summary>
		/// <returns>The number of key/value pairs in the <see cref="T:System.Collections.Specialized.StringDictionary" />.Retrieving the value of this property is an O(1) operation.</returns>
		// Token: 0x17000B1C RID: 2844
		// (get) Token: 0x06002C5D RID: 11357 RVA: 0x0009A2A2 File Offset: 0x000984A2
		public virtual int Count
		{
			get
			{
				return this.contents.Count;
			}
		}

		/// <summary>Gets a value indicating whether access to the <see cref="T:System.Collections.Specialized.StringDictionary" /> is synchronized (thread safe).</summary>
		/// <returns>
		///     <see langword="true" /> if access to the <see cref="T:System.Collections.Specialized.StringDictionary" /> is synchronized (thread safe); otherwise, <see langword="false" />.</returns>
		// Token: 0x17000B1D RID: 2845
		// (get) Token: 0x06002C5E RID: 11358 RVA: 0x0009A2AF File Offset: 0x000984AF
		public virtual bool IsSynchronized
		{
			get
			{
				return this.contents.IsSynchronized;
			}
		}

		/// <summary>Gets or sets the value associated with the specified key.</summary>
		/// <param name="key">The key whose value to get or set. </param>
		/// <returns>The value associated with the specified key. If the specified key is not found, Get returns <see langword="null" />, and Set creates a new entry with the specified key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />.</exception>
		// Token: 0x17000B1E RID: 2846
		public virtual string this[string key]
		{
			get
			{
				if (key == null)
				{
					throw new ArgumentNullException("key");
				}
				return (string)this.contents[key.ToLower(CultureInfo.InvariantCulture)];
			}
			set
			{
				if (key == null)
				{
					throw new ArgumentNullException("key");
				}
				this.contents[key.ToLower(CultureInfo.InvariantCulture)] = value;
			}
		}

		/// <summary>Gets a collection of keys in the <see cref="T:System.Collections.Specialized.StringDictionary" />.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> that provides the keys in the <see cref="T:System.Collections.Specialized.StringDictionary" />.</returns>
		// Token: 0x17000B1F RID: 2847
		// (get) Token: 0x06002C61 RID: 11361 RVA: 0x0009A30E File Offset: 0x0009850E
		public virtual ICollection Keys
		{
			get
			{
				return this.contents.Keys;
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.Specialized.StringDictionary" />.</summary>
		/// <returns>An <see cref="T:System.Object" /> that can be used to synchronize access to the <see cref="T:System.Collections.Specialized.StringDictionary" />.</returns>
		// Token: 0x17000B20 RID: 2848
		// (get) Token: 0x06002C62 RID: 11362 RVA: 0x0009A31B File Offset: 0x0009851B
		public virtual object SyncRoot
		{
			get
			{
				return this.contents.SyncRoot;
			}
		}

		/// <summary>Gets a collection of values in the <see cref="T:System.Collections.Specialized.StringDictionary" />.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> that provides the values in the <see cref="T:System.Collections.Specialized.StringDictionary" />.</returns>
		// Token: 0x17000B21 RID: 2849
		// (get) Token: 0x06002C63 RID: 11363 RVA: 0x0009A328 File Offset: 0x00098528
		public virtual ICollection Values
		{
			get
			{
				return this.contents.Values;
			}
		}

		/// <summary>Adds an entry with the specified key and value into the <see cref="T:System.Collections.Specialized.StringDictionary" />.</summary>
		/// <param name="key">The key of the entry to add. </param>
		/// <param name="value">The value of the entry to add. The value can be <see langword="null" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">An entry with the same key already exists in the <see cref="T:System.Collections.Specialized.StringDictionary" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Specialized.StringDictionary" /> is read-only. </exception>
		// Token: 0x06002C64 RID: 11364 RVA: 0x0009A335 File Offset: 0x00098535
		public virtual void Add(string key, string value)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.contents.Add(key.ToLower(CultureInfo.InvariantCulture), value);
		}

		/// <summary>Removes all entries from the <see cref="T:System.Collections.Specialized.StringDictionary" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Specialized.StringDictionary" /> is read-only. </exception>
		// Token: 0x06002C65 RID: 11365 RVA: 0x0009A35C File Offset: 0x0009855C
		public virtual void Clear()
		{
			this.contents.Clear();
		}

		/// <summary>Determines if the <see cref="T:System.Collections.Specialized.StringDictionary" /> contains a specific key.</summary>
		/// <param name="key">The key to locate in the <see cref="T:System.Collections.Specialized.StringDictionary" />. </param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.Specialized.StringDictionary" /> contains an entry with the specified key; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The key is <see langword="null" />. </exception>
		// Token: 0x06002C66 RID: 11366 RVA: 0x0009A369 File Offset: 0x00098569
		public virtual bool ContainsKey(string key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			return this.contents.ContainsKey(key.ToLower(CultureInfo.InvariantCulture));
		}

		/// <summary>Determines if the <see cref="T:System.Collections.Specialized.StringDictionary" /> contains a specific value.</summary>
		/// <param name="value">The value to locate in the <see cref="T:System.Collections.Specialized.StringDictionary" />. The value can be <see langword="null" />. </param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Collections.Specialized.StringDictionary" /> contains an element with the specified value; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002C67 RID: 11367 RVA: 0x0009A38F File Offset: 0x0009858F
		public virtual bool ContainsValue(string value)
		{
			return this.contents.ContainsValue(value);
		}

		/// <summary>Copies the string dictionary values to a one-dimensional <see cref="T:System.Array" /> instance at the specified index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the values copied from the <see cref="T:System.Collections.Specialized.StringDictionary" />. </param>
		/// <param name="index">The index in the array where copying begins. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="array" /> is multidimensional.-or- The number of elements in the <see cref="T:System.Collections.Specialized.StringDictionary" /> is greater than the available space from <paramref name="index" /> to the end of <paramref name="array" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than the lower bound of <paramref name="array" />. </exception>
		// Token: 0x06002C68 RID: 11368 RVA: 0x0009A39D File Offset: 0x0009859D
		public virtual void CopyTo(Array array, int index)
		{
			this.contents.CopyTo(array, index);
		}

		/// <summary>Returns an enumerator that iterates through the string dictionary.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that iterates through the string dictionary.</returns>
		// Token: 0x06002C69 RID: 11369 RVA: 0x0009A3AC File Offset: 0x000985AC
		public virtual IEnumerator GetEnumerator()
		{
			return this.contents.GetEnumerator();
		}

		/// <summary>Removes the entry with the specified key from the string dictionary.</summary>
		/// <param name="key">The key of the entry to remove. </param>
		/// <exception cref="T:System.ArgumentNullException">The key is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Specialized.StringDictionary" /> is read-only. </exception>
		// Token: 0x06002C6A RID: 11370 RVA: 0x0009A3B9 File Offset: 0x000985B9
		public virtual void Remove(string key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.contents.Remove(key.ToLower(CultureInfo.InvariantCulture));
		}

		// Token: 0x06002C6B RID: 11371 RVA: 0x0009A3DF File Offset: 0x000985DF
		internal void ReplaceHashtable(Hashtable useThisHashtableInstead)
		{
			this.contents = useThisHashtableInstead;
		}

		// Token: 0x06002C6C RID: 11372 RVA: 0x0009A3E8 File Offset: 0x000985E8
		internal IDictionary<string, string> AsGenericDictionary()
		{
			return new StringDictionary.GenericAdapter(this);
		}

		// Token: 0x0400234A RID: 9034
		internal Hashtable contents = new Hashtable();

		// Token: 0x0200057A RID: 1402
		private class GenericAdapter : IDictionary<string, string>, ICollection<KeyValuePair<string, string>>, IEnumerable<KeyValuePair<string, string>>, IEnumerable
		{
			// Token: 0x06002C6D RID: 11373 RVA: 0x0009A3F0 File Offset: 0x000985F0
			internal GenericAdapter(StringDictionary stringDictionary)
			{
				this.m_stringDictionary = stringDictionary;
			}

			// Token: 0x06002C6E RID: 11374 RVA: 0x0009A3FF File Offset: 0x000985FF
			public void Add(string key, string value)
			{
				this[key] = value;
			}

			// Token: 0x06002C6F RID: 11375 RVA: 0x0009A409 File Offset: 0x00098609
			public bool ContainsKey(string key)
			{
				return this.m_stringDictionary.ContainsKey(key);
			}

			// Token: 0x06002C70 RID: 11376 RVA: 0x0009A417 File Offset: 0x00098617
			public void Clear()
			{
				this.m_stringDictionary.Clear();
			}

			// Token: 0x17000B22 RID: 2850
			// (get) Token: 0x06002C71 RID: 11377 RVA: 0x0009A424 File Offset: 0x00098624
			public int Count
			{
				get
				{
					return this.m_stringDictionary.Count;
				}
			}

			// Token: 0x17000B23 RID: 2851
			public string this[string key]
			{
				get
				{
					if (key == null)
					{
						throw new ArgumentNullException("key");
					}
					if (!this.m_stringDictionary.ContainsKey(key))
					{
						throw new KeyNotFoundException();
					}
					return this.m_stringDictionary[key];
				}
				set
				{
					if (key == null)
					{
						throw new ArgumentNullException("key");
					}
					this.m_stringDictionary[key] = value;
				}
			}

			// Token: 0x17000B24 RID: 2852
			// (get) Token: 0x06002C74 RID: 11380 RVA: 0x0009A47E File Offset: 0x0009867E
			public ICollection<string> Keys
			{
				get
				{
					if (this._keys == null)
					{
						this._keys = new StringDictionary.GenericAdapter.ICollectionToGenericCollectionAdapter(this.m_stringDictionary, StringDictionary.GenericAdapter.KeyOrValue.Key);
					}
					return this._keys;
				}
			}

			// Token: 0x17000B25 RID: 2853
			// (get) Token: 0x06002C75 RID: 11381 RVA: 0x0009A4A0 File Offset: 0x000986A0
			public ICollection<string> Values
			{
				get
				{
					if (this._values == null)
					{
						this._values = new StringDictionary.GenericAdapter.ICollectionToGenericCollectionAdapter(this.m_stringDictionary, StringDictionary.GenericAdapter.KeyOrValue.Value);
					}
					return this._values;
				}
			}

			// Token: 0x06002C76 RID: 11382 RVA: 0x0009A4C2 File Offset: 0x000986C2
			public bool Remove(string key)
			{
				if (!this.m_stringDictionary.ContainsKey(key))
				{
					return false;
				}
				this.m_stringDictionary.Remove(key);
				return true;
			}

			// Token: 0x06002C77 RID: 11383 RVA: 0x0009A4E1 File Offset: 0x000986E1
			public bool TryGetValue(string key, out string value)
			{
				if (!this.m_stringDictionary.ContainsKey(key))
				{
					value = null;
					return false;
				}
				value = this.m_stringDictionary[key];
				return true;
			}

			// Token: 0x06002C78 RID: 11384 RVA: 0x0009A505 File Offset: 0x00098705
			void ICollection<KeyValuePair<string, string>>.Add(KeyValuePair<string, string> item)
			{
				this.m_stringDictionary.Add(item.Key, item.Value);
			}

			// Token: 0x06002C79 RID: 11385 RVA: 0x0009A520 File Offset: 0x00098720
			bool ICollection<KeyValuePair<string, string>>.Contains(KeyValuePair<string, string> item)
			{
				string text;
				return this.TryGetValue(item.Key, out text) && text.Equals(item.Value);
			}

			// Token: 0x06002C7A RID: 11386 RVA: 0x0009A550 File Offset: 0x00098750
			void ICollection<KeyValuePair<string, string>>.CopyTo(KeyValuePair<string, string>[] array, int arrayIndex)
			{
				if (array == null)
				{
					throw new ArgumentNullException("array", SR.GetString("Array cannot be null."));
				}
				if (arrayIndex < 0)
				{
					throw new ArgumentOutOfRangeException("arrayIndex", SR.GetString("Non-negative number required."));
				}
				if (array.Length - arrayIndex < this.Count)
				{
					throw new ArgumentException(SR.GetString("Destination array is not long enough to copy all the items in the collection. Check array index and length."));
				}
				int num = arrayIndex;
				foreach (object obj in this.m_stringDictionary)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
					array[num++] = new KeyValuePair<string, string>((string)dictionaryEntry.Key, (string)dictionaryEntry.Value);
				}
			}

			// Token: 0x17000B26 RID: 2854
			// (get) Token: 0x06002C7B RID: 11387 RVA: 0x00005AFA File Offset: 0x00003CFA
			bool ICollection<KeyValuePair<string, string>>.IsReadOnly
			{
				get
				{
					return false;
				}
			}

			// Token: 0x06002C7C RID: 11388 RVA: 0x0009A61C File Offset: 0x0009881C
			bool ICollection<KeyValuePair<string, string>>.Remove(KeyValuePair<string, string> item)
			{
				if (!((ICollection<KeyValuePair<string, string>>)this).Contains(item))
				{
					return false;
				}
				this.m_stringDictionary.Remove(item.Key);
				return true;
			}

			// Token: 0x06002C7D RID: 11389 RVA: 0x0009A63C File Offset: 0x0009883C
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x06002C7E RID: 11390 RVA: 0x0009A644 File Offset: 0x00098844
			public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
			{
				foreach (object obj in this.m_stringDictionary)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
					yield return new KeyValuePair<string, string>((string)dictionaryEntry.Key, (string)dictionaryEntry.Value);
				}
				IEnumerator enumerator = null;
				yield break;
				yield break;
			}

			// Token: 0x0400234B RID: 9035
			private StringDictionary m_stringDictionary;

			// Token: 0x0400234C RID: 9036
			private StringDictionary.GenericAdapter.ICollectionToGenericCollectionAdapter _values;

			// Token: 0x0400234D RID: 9037
			private StringDictionary.GenericAdapter.ICollectionToGenericCollectionAdapter _keys;

			// Token: 0x0200057B RID: 1403
			internal enum KeyOrValue
			{
				// Token: 0x0400234F RID: 9039
				Key,
				// Token: 0x04002350 RID: 9040
				Value
			}

			// Token: 0x0200057C RID: 1404
			private class ICollectionToGenericCollectionAdapter : ICollection<string>, IEnumerable<string>, IEnumerable
			{
				// Token: 0x06002C7F RID: 11391 RVA: 0x0009A653 File Offset: 0x00098853
				public ICollectionToGenericCollectionAdapter(StringDictionary source, StringDictionary.GenericAdapter.KeyOrValue keyOrValue)
				{
					if (source == null)
					{
						throw new ArgumentNullException("source");
					}
					this._internal = source;
					this._keyOrValue = keyOrValue;
				}

				// Token: 0x06002C80 RID: 11392 RVA: 0x0009A677 File Offset: 0x00098877
				public void Add(string item)
				{
					this.ThrowNotSupportedException();
				}

				// Token: 0x06002C81 RID: 11393 RVA: 0x0009A677 File Offset: 0x00098877
				public void Clear()
				{
					this.ThrowNotSupportedException();
				}

				// Token: 0x06002C82 RID: 11394 RVA: 0x0009A67F File Offset: 0x0009887F
				public void ThrowNotSupportedException()
				{
					if (this._keyOrValue == StringDictionary.GenericAdapter.KeyOrValue.Key)
					{
						throw new NotSupportedException(SR.GetString("Mutating a key collection derived from a dictionary is not allowed."));
					}
					throw new NotSupportedException(SR.GetString("Mutating a value collection derived from a dictionary is not allowed."));
				}

				// Token: 0x06002C83 RID: 11395 RVA: 0x0009A6A8 File Offset: 0x000988A8
				public bool Contains(string item)
				{
					if (this._keyOrValue == StringDictionary.GenericAdapter.KeyOrValue.Key)
					{
						return this._internal.ContainsKey(item);
					}
					return this._internal.ContainsValue(item);
				}

				// Token: 0x06002C84 RID: 11396 RVA: 0x0009A6CB File Offset: 0x000988CB
				public void CopyTo(string[] array, int arrayIndex)
				{
					this.GetUnderlyingCollection().CopyTo(array, arrayIndex);
				}

				// Token: 0x17000B27 RID: 2855
				// (get) Token: 0x06002C85 RID: 11397 RVA: 0x0009A6DA File Offset: 0x000988DA
				public int Count
				{
					get
					{
						return this._internal.Count;
					}
				}

				// Token: 0x17000B28 RID: 2856
				// (get) Token: 0x06002C86 RID: 11398 RVA: 0x00003298 File Offset: 0x00001498
				public bool IsReadOnly
				{
					get
					{
						return true;
					}
				}

				// Token: 0x06002C87 RID: 11399 RVA: 0x0009A6E7 File Offset: 0x000988E7
				public bool Remove(string item)
				{
					this.ThrowNotSupportedException();
					return false;
				}

				// Token: 0x06002C88 RID: 11400 RVA: 0x0009A6F0 File Offset: 0x000988F0
				private ICollection GetUnderlyingCollection()
				{
					if (this._keyOrValue == StringDictionary.GenericAdapter.KeyOrValue.Key)
					{
						return this._internal.Keys;
					}
					return this._internal.Values;
				}

				// Token: 0x06002C89 RID: 11401 RVA: 0x0009A711 File Offset: 0x00098911
				public IEnumerator<string> GetEnumerator()
				{
					ICollection underlyingCollection = this.GetUnderlyingCollection();
					foreach (object obj in underlyingCollection)
					{
						string text = (string)obj;
						yield return text;
					}
					IEnumerator enumerator = null;
					yield break;
					yield break;
				}

				// Token: 0x06002C8A RID: 11402 RVA: 0x0009A720 File Offset: 0x00098920
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.GetUnderlyingCollection().GetEnumerator();
				}

				// Token: 0x04002351 RID: 9041
				private StringDictionary _internal;

				// Token: 0x04002352 RID: 9042
				private StringDictionary.GenericAdapter.KeyOrValue _keyOrValue;

				// Token: 0x0200057D RID: 1405
				[CompilerGenerated]
				private sealed class <GetEnumerator>d__14 : IEnumerator<string>, IDisposable, IEnumerator
				{
					// Token: 0x06002C8B RID: 11403 RVA: 0x0009A72D File Offset: 0x0009892D
					[DebuggerHidden]
					public <GetEnumerator>d__14(int <>1__state)
					{
						this.<>1__state = <>1__state;
					}

					// Token: 0x06002C8C RID: 11404 RVA: 0x0009A73C File Offset: 0x0009893C
					[DebuggerHidden]
					void IDisposable.Dispose()
					{
						int num = this.<>1__state;
						if (num == -3 || num == 1)
						{
							try
							{
							}
							finally
							{
								this.<>m__Finally1();
							}
						}
					}

					// Token: 0x06002C8D RID: 11405 RVA: 0x0009A774 File Offset: 0x00098974
					bool IEnumerator.MoveNext()
					{
						bool result;
						try
						{
							int num = this.<>1__state;
							StringDictionary.GenericAdapter.ICollectionToGenericCollectionAdapter collectionToGenericCollectionAdapter = this;
							if (num != 0)
							{
								if (num != 1)
								{
									return false;
								}
								this.<>1__state = -3;
							}
							else
							{
								this.<>1__state = -1;
								ICollection underlyingCollection = collectionToGenericCollectionAdapter.GetUnderlyingCollection();
								enumerator = underlyingCollection.GetEnumerator();
								this.<>1__state = -3;
							}
							if (!enumerator.MoveNext())
							{
								this.<>m__Finally1();
								enumerator = null;
								result = false;
							}
							else
							{
								string text = (string)enumerator.Current;
								this.<>2__current = text;
								this.<>1__state = 1;
								result = true;
							}
						}
						catch
						{
							this.System.IDisposable.Dispose();
							throw;
						}
						return result;
					}

					// Token: 0x06002C8E RID: 11406 RVA: 0x0009A824 File Offset: 0x00098A24
					private void <>m__Finally1()
					{
						this.<>1__state = -1;
						IDisposable disposable = enumerator as IDisposable;
						if (disposable != null)
						{
							disposable.Dispose();
						}
					}

					// Token: 0x17000B29 RID: 2857
					// (get) Token: 0x06002C8F RID: 11407 RVA: 0x0009A84D File Offset: 0x00098A4D
					string IEnumerator<string>.Current
					{
						[DebuggerHidden]
						get
						{
							return this.<>2__current;
						}
					}

					// Token: 0x06002C90 RID: 11408 RVA: 0x00006740 File Offset: 0x00004940
					[DebuggerHidden]
					void IEnumerator.Reset()
					{
						throw new NotSupportedException();
					}

					// Token: 0x17000B2A RID: 2858
					// (get) Token: 0x06002C91 RID: 11409 RVA: 0x0009A84D File Offset: 0x00098A4D
					object IEnumerator.Current
					{
						[DebuggerHidden]
						get
						{
							return this.<>2__current;
						}
					}

					// Token: 0x04002353 RID: 9043
					private int <>1__state;

					// Token: 0x04002354 RID: 9044
					private string <>2__current;

					// Token: 0x04002355 RID: 9045
					public StringDictionary.GenericAdapter.ICollectionToGenericCollectionAdapter <>4__this;

					// Token: 0x04002356 RID: 9046
					private IEnumerator <>7__wrap1;
				}
			}

			// Token: 0x0200057E RID: 1406
			[CompilerGenerated]
			private sealed class <GetEnumerator>d__25 : IEnumerator<KeyValuePair<string, string>>, IDisposable, IEnumerator
			{
				// Token: 0x06002C92 RID: 11410 RVA: 0x0009A855 File Offset: 0x00098A55
				[DebuggerHidden]
				public <GetEnumerator>d__25(int <>1__state)
				{
					this.<>1__state = <>1__state;
				}

				// Token: 0x06002C93 RID: 11411 RVA: 0x0009A864 File Offset: 0x00098A64
				[DebuggerHidden]
				void IDisposable.Dispose()
				{
					int num = this.<>1__state;
					if (num == -3 || num == 1)
					{
						try
						{
						}
						finally
						{
							this.<>m__Finally1();
						}
					}
				}

				// Token: 0x06002C94 RID: 11412 RVA: 0x0009A89C File Offset: 0x00098A9C
				bool IEnumerator.MoveNext()
				{
					bool result;
					try
					{
						int num = this.<>1__state;
						StringDictionary.GenericAdapter genericAdapter = this;
						if (num != 0)
						{
							if (num != 1)
							{
								return false;
							}
							this.<>1__state = -3;
						}
						else
						{
							this.<>1__state = -1;
							enumerator = genericAdapter.m_stringDictionary.GetEnumerator();
							this.<>1__state = -3;
						}
						if (!enumerator.MoveNext())
						{
							this.<>m__Finally1();
							enumerator = null;
							result = false;
						}
						else
						{
							DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
							this.<>2__current = new KeyValuePair<string, string>((string)dictionaryEntry.Key, (string)dictionaryEntry.Value);
							this.<>1__state = 1;
							result = true;
						}
					}
					catch
					{
						this.System.IDisposable.Dispose();
						throw;
					}
					return result;
				}

				// Token: 0x06002C95 RID: 11413 RVA: 0x0009A964 File Offset: 0x00098B64
				private void <>m__Finally1()
				{
					this.<>1__state = -1;
					IDisposable disposable = enumerator as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}

				// Token: 0x17000B2B RID: 2859
				// (get) Token: 0x06002C96 RID: 11414 RVA: 0x0009A98D File Offset: 0x00098B8D
				KeyValuePair<string, string> IEnumerator<KeyValuePair<string, string>>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06002C97 RID: 11415 RVA: 0x00006740 File Offset: 0x00004940
				[DebuggerHidden]
				void IEnumerator.Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x17000B2C RID: 2860
				// (get) Token: 0x06002C98 RID: 11416 RVA: 0x0009A995 File Offset: 0x00098B95
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x04002357 RID: 9047
				private int <>1__state;

				// Token: 0x04002358 RID: 9048
				private KeyValuePair<string, string> <>2__current;

				// Token: 0x04002359 RID: 9049
				public StringDictionary.GenericAdapter <>4__this;

				// Token: 0x0400235A RID: 9050
				private IEnumerator <>7__wrap1;
			}
		}
	}
}
