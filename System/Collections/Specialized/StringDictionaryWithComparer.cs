﻿using System;

namespace System.Collections.Specialized
{
	// Token: 0x0200057F RID: 1407
	[Serializable]
	internal class StringDictionaryWithComparer : StringDictionary
	{
		// Token: 0x06002C99 RID: 11417 RVA: 0x0009A9A2 File Offset: 0x00098BA2
		public StringDictionaryWithComparer() : this(StringComparer.OrdinalIgnoreCase)
		{
		}

		// Token: 0x06002C9A RID: 11418 RVA: 0x0009A9AF File Offset: 0x00098BAF
		public StringDictionaryWithComparer(IEqualityComparer comparer)
		{
			base.ReplaceHashtable(new Hashtable(comparer));
		}

		// Token: 0x17000B2D RID: 2861
		public override string this[string key]
		{
			get
			{
				if (key == null)
				{
					throw new ArgumentNullException("key");
				}
				return (string)this.contents[key];
			}
			set
			{
				if (key == null)
				{
					throw new ArgumentNullException("key");
				}
				this.contents[key] = value;
			}
		}

		// Token: 0x06002C9D RID: 11421 RVA: 0x000974ED File Offset: 0x000956ED
		public override void Add(string key, string value)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.contents.Add(key, value);
		}

		// Token: 0x06002C9E RID: 11422 RVA: 0x0009750A File Offset: 0x0009570A
		public override bool ContainsKey(string key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			return this.contents.ContainsKey(key);
		}

		// Token: 0x06002C9F RID: 11423 RVA: 0x00097526 File Offset: 0x00095726
		public override void Remove(string key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.contents.Remove(key);
		}
	}
}
