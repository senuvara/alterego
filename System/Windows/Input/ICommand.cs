﻿using System;
using System.Runtime.CompilerServices;

namespace System.Windows.Input
{
	/// <summary>Defines a command.</summary>
	// Token: 0x020000BB RID: 187
	[TypeForwardedFrom("PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35")]
	public interface ICommand
	{
		/// <summary>Defines the method that determines whether the command can execute in its current state.</summary>
		/// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to <see langword="null" />.</param>
		/// <returns>
		///     <see langword="true" /> if this command can be executed; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000452 RID: 1106
		bool CanExecute(object parameter);

		/// <summary>Defines the method to be called when the command is invoked.</summary>
		/// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to <see langword="null" />.</param>
		// Token: 0x06000453 RID: 1107
		void Execute(object parameter);

		/// <summary>Occurs when changes occur that affect whether or not the command should execute.</summary>
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000454 RID: 1108
		// (remove) Token: 0x06000455 RID: 1109
		event EventHandler CanExecuteChanged;
	}
}
