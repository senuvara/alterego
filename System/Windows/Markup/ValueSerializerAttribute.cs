﻿using System;

namespace System.Windows.Markup
{
	/// <summary>Identifies the <see cref="T:System.Windows.Markup.ValueSerializer" /> class that a type or property should use when it is serialized.</summary>
	// Token: 0x020000BC RID: 188
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Interface, AllowMultiple = false, Inherited = true)]
	public sealed class ValueSerializerAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Windows.Markup.ValueSerializerAttribute" /> class, using the specified type.</summary>
		/// <param name="valueSerializerType">A type that represents the type of the <see cref="T:System.Windows.Markup.ValueSerializer" /> class.</param>
		// Token: 0x06000456 RID: 1110 RVA: 0x0001598A File Offset: 0x00013B8A
		public ValueSerializerAttribute(Type valueSerializerType)
		{
			this._valueSerializerType = valueSerializerType;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Windows.Markup.ValueSerializerAttribute" /> class, using an assembly qualified type name string.</summary>
		/// <param name="valueSerializerTypeName">The assembly qualified type name string for the <see cref="T:System.Windows.Markup.ValueSerializer" /> class to use.</param>
		// Token: 0x06000457 RID: 1111 RVA: 0x00015999 File Offset: 0x00013B99
		public ValueSerializerAttribute(string valueSerializerTypeName)
		{
			this._valueSerializerTypeName = valueSerializerTypeName;
		}

		/// <summary>Gets the type of the <see cref="T:System.Windows.Markup.ValueSerializer" /> class reported by this attribute.</summary>
		/// <returns>The type of the <see cref="T:System.Windows.Markup.ValueSerializer" />.</returns>
		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x06000458 RID: 1112 RVA: 0x000159A8 File Offset: 0x00013BA8
		public Type ValueSerializerType
		{
			get
			{
				if (this._valueSerializerType == null && this._valueSerializerTypeName != null)
				{
					this._valueSerializerType = Type.GetType(this._valueSerializerTypeName);
				}
				return this._valueSerializerType;
			}
		}

		/// <summary>Gets the assembly qualified name of the <see cref="T:System.Windows.Markup.ValueSerializer" /> type for this type or property.</summary>
		/// <returns>The assembly qualified name of the type.</returns>
		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x06000459 RID: 1113 RVA: 0x000159D7 File Offset: 0x00013BD7
		public string ValueSerializerTypeName
		{
			get
			{
				if (this._valueSerializerType != null)
				{
					return this._valueSerializerType.AssemblyQualifiedName;
				}
				return this._valueSerializerTypeName;
			}
		}

		// Token: 0x04000971 RID: 2417
		private Type _valueSerializerType;

		// Token: 0x04000972 RID: 2418
		private string _valueSerializerTypeName;
	}
}
