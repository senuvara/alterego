﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace System
{
	// Token: 0x020000B6 RID: 182
	[StructLayout(LayoutKind.Sequential)]
	internal class IOSelectorJob : IThreadPoolWorkItem
	{
		// Token: 0x06000440 RID: 1088 RVA: 0x000156E8 File Offset: 0x000138E8
		public IOSelectorJob(IOOperation operation, IOAsyncCallback callback, IOAsyncResult state)
		{
			this.operation = operation;
			this.callback = callback;
			this.state = state;
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x00015705 File Offset: 0x00013905
		void IThreadPoolWorkItem.ExecuteWorkItem()
		{
			this.callback(this.state);
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x0000232D File Offset: 0x0000052D
		void IThreadPoolWorkItem.MarkAborted(ThreadAbortException tae)
		{
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x00015718 File Offset: 0x00013918
		public void MarkDisposed()
		{
			this.state.CompleteDisposed();
		}

		// Token: 0x0400096A RID: 2410
		private IOOperation operation;

		// Token: 0x0400096B RID: 2411
		private IOAsyncCallback callback;

		// Token: 0x0400096C RID: 2412
		private IOAsyncResult state;
	}
}
