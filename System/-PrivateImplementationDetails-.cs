﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x020005C4 RID: 1476
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x06002F74 RID: 12148 RVA: 0x000A3A70 File Offset: 0x000A1C70
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x0400244B RID: 9291 RVA: 0x000A5FA8 File Offset: 0x000A41A8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 01B19A5288D07258B9C70A6B4C90BB92D2569631;

	// Token: 0x0400244C RID: 9292 RVA: 0x000A5FAE File Offset: 0x000A41AE
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=14 0283A6AF88802AB45989B29549915BEA0F6CD515;

	// Token: 0x0400244D RID: 9293 RVA: 0x000A5FBC File Offset: 0x000A41BC
	internal static readonly long 03F4297FCC30D0FD5E420E5D26E7FA711167C7EF;

	// Token: 0x0400244E RID: 9294 RVA: 0x000A5FC4 File Offset: 0x000A41C4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 1588174EA926BCCEB6275C029A42C7E3DBA4D523;

	// Token: 0x0400244F RID: 9295 RVA: 0x000A60C4 File Offset: 0x000A42C4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=9 1A39764B112685485A5BA7B2880D878B858C1A7A;

	// Token: 0x04002450 RID: 9296 RVA: 0x000A60CD File Offset: 0x000A42CD
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 1A84029C80CB5518379F199F53FF08A7B764F8FD;

	// Token: 0x04002451 RID: 9297 RVA: 0x000A60D0 File Offset: 0x000A42D0
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=65 1D278D3C888D1A2FA7EED622BFC02927CE4049AF;

	// Token: 0x04002452 RID: 9298 RVA: 0x000A6111 File Offset: 0x000A4311
	internal static readonly long 1E6397E00E59B10204D367C0CB912D81187ADD58;

	// Token: 0x04002453 RID: 9299 RVA: 0x000A6119 File Offset: 0x000A4319
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 2699ED079CDDEF57B48E372A3D47B5F3C372F78C;

	// Token: 0x04002454 RID: 9300 RVA: 0x000A611F File Offset: 0x000A431F
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 3AF058944BC1070D164143C7635654884348D0E1;

	// Token: 0x04002455 RID: 9301 RVA: 0x000A612B File Offset: 0x000A432B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 3BE77BF818331C2D8400FFFFF9FADD3F16AD89AC;

	// Token: 0x04002456 RID: 9302 RVA: 0x000A6137 File Offset: 0x000A4337
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 3CDA7449B0586AB873C75C04BB11D4864F5D7392;

	// Token: 0x04002457 RID: 9303 RVA: 0x000A6237 File Offset: 0x000A4437
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 469BD0E53254027ABBA74FE247B4773933E70F01;

	// Token: 0x04002458 RID: 9304 RVA: 0x000A623D File Offset: 0x000A443D
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=10 53437C3B2572EDB9B8640C3195DF3BC2729C5EA1;

	// Token: 0x04002459 RID: 9305 RVA: 0x000A6247 File Offset: 0x000A4447
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=506 5656FB1DA817C3001C6BA3848F54BB428C5815DF;

	// Token: 0x0400245A RID: 9306 RVA: 0x000A6441 File Offset: 0x000A4641
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 59F5BD34B6C013DEACC784F69C67E95150033A84;

	// Token: 0x0400245B RID: 9307 RVA: 0x000A6461 File Offset: 0x000A4661
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 5BC3486B05BA8CF4689C7BDB198B3F477BB4E20C;

	// Token: 0x0400245C RID: 9308 RVA: 0x000A6467 File Offset: 0x000A4667
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=9 6D49C9D487D7AD3491ECE08732D68A593CC2038D;

	// Token: 0x0400245D RID: 9309 RVA: 0x000A6470 File Offset: 0x000A4670
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=128 6F3AD3DC3AF8047587C4C9D696EB68A01FEF796E;

	// Token: 0x0400245E RID: 9310 RVA: 0x000A64F0 File Offset: 0x000A46F0
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 8821BF2BDFF225D177372C9F3E0A8286EE241FF4;

	// Token: 0x0400245F RID: 9311 RVA: 0x000A64F6 File Offset: 0x000A46F6
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=44 8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3;

	// Token: 0x04002460 RID: 9312 RVA: 0x000A6522 File Offset: 0x000A4722
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=2024 90503F4F223B979EDE436D1A66CF718DCED9FBC6;

	// Token: 0x04002461 RID: 9313 RVA: 0x000A6D0A File Offset: 0x000A4F0A
	internal static readonly long 98A44A6F8606AE6F23FE230286C1D6FBCC407226;

	// Token: 0x04002462 RID: 9314 RVA: 0x000A6D12 File Offset: 0x000A4F12
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536;

	// Token: 0x04002463 RID: 9315 RVA: 0x000A6D32 File Offset: 0x000A4F32
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=128 CCEEADA43268372341F81AE0C9208C6856441C04;

	// Token: 0x04002464 RID: 9316 RVA: 0x000A6DB2 File Offset: 0x000A4FB2
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=16 CE27CB141098FEB00714E758646BE3E99C185B71;

	// Token: 0x04002465 RID: 9317 RVA: 0x000A6DC2 File Offset: 0x000A4FC2
	internal static readonly long E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78;

	// Token: 0x04002466 RID: 9318 RVA: 0x000A6DCA File Offset: 0x000A4FCA
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 EC5842B3154E1AF94500B57220EB9F684BCCC42A;

	// Token: 0x04002467 RID: 9319 RVA: 0x000A6DEA File Offset: 0x000A4FEA
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 EEAFE8C6E1AB017237567305EE925C976CDB6458;

	// Token: 0x04002468 RID: 9320 RVA: 0x000A6EEA File Offset: 0x000A50EA
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=5 F6D0C643351580307B2EAA6A7560E76965496BC7;

	// Token: 0x020005C5 RID: 1477
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 3)]
	private struct __StaticArrayInitTypeSize=3
	{
	}

	// Token: 0x020005C6 RID: 1478
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 5)]
	private struct __StaticArrayInitTypeSize=5
	{
	}

	// Token: 0x020005C7 RID: 1479
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 6)]
	private struct __StaticArrayInitTypeSize=6
	{
	}

	// Token: 0x020005C8 RID: 1480
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 9)]
	private struct __StaticArrayInitTypeSize=9
	{
	}

	// Token: 0x020005C9 RID: 1481
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 10)]
	private struct __StaticArrayInitTypeSize=10
	{
	}

	// Token: 0x020005CA RID: 1482
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 12)]
	private struct __StaticArrayInitTypeSize=12
	{
	}

	// Token: 0x020005CB RID: 1483
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 14)]
	private struct __StaticArrayInitTypeSize=14
	{
	}

	// Token: 0x020005CC RID: 1484
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 16)]
	private struct __StaticArrayInitTypeSize=16
	{
	}

	// Token: 0x020005CD RID: 1485
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 32)]
	private struct __StaticArrayInitTypeSize=32
	{
	}

	// Token: 0x020005CE RID: 1486
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 44)]
	private struct __StaticArrayInitTypeSize=44
	{
	}

	// Token: 0x020005CF RID: 1487
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 65)]
	private struct __StaticArrayInitTypeSize=65
	{
	}

	// Token: 0x020005D0 RID: 1488
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 128)]
	private struct __StaticArrayInitTypeSize=128
	{
	}

	// Token: 0x020005D1 RID: 1489
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 256)]
	private struct __StaticArrayInitTypeSize=256
	{
	}

	// Token: 0x020005D2 RID: 1490
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 506)]
	private struct __StaticArrayInitTypeSize=506
	{
	}

	// Token: 0x020005D3 RID: 1491
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 2024)]
	private struct __StaticArrayInitTypeSize=2024
	{
	}
}
