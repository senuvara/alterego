﻿using System;
using System.ComponentModel;

namespace System
{
	// Token: 0x020000B9 RID: 185
	[AttributeUsage(AttributeTargets.All)]
	internal class SRDescriptionAttribute : DescriptionAttribute
	{
		// Token: 0x0600044A RID: 1098 RVA: 0x000157FB File Offset: 0x000139FB
		public SRDescriptionAttribute(string description) : base(description)
		{
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x0600044B RID: 1099 RVA: 0x00015804 File Offset: 0x00013A04
		public override string Description
		{
			get
			{
				if (!this.isReplaced)
				{
					this.isReplaced = true;
					base.DescriptionValue = Locale.GetText(base.DescriptionValue);
				}
				return base.DescriptionValue;
			}
		}

		// Token: 0x04000970 RID: 2416
		private bool isReplaced;
	}
}
