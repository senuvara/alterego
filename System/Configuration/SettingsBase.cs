﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides the base class used to support user property settings.</summary>
	// Token: 0x02000606 RID: 1542
	public abstract class SettingsBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsBase" /> class. </summary>
		// Token: 0x0600315D RID: 12637 RVA: 0x000092E2 File Offset: 0x000074E2
		protected SettingsBase()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the associated settings context.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsContext" /> associated with the settings instance.</returns>
		// Token: 0x17000C3A RID: 3130
		// (get) Token: 0x0600315E RID: 12638 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual SettingsContext Context
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value indicating whether access to the object is synchronized (thread safe). </summary>
		/// <returns>
		///     <see langword="true" /> if access to the <see cref="T:System.Configuration.SettingsBase" /> is synchronized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C3B RID: 3131
		// (get) Token: 0x0600315F RID: 12639 RVA: 0x000A43F4 File Offset: 0x000A25F4
		public bool IsSynchronized
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets the value of the specified settings property.</summary>
		/// <param name="propertyName">A <see cref="T:System.String" /> containing the name of the property to access.</param>
		/// <returns>If found, the value of the named settings property.</returns>
		/// <exception cref="T:System.Configuration.SettingsPropertyNotFoundException">There are no properties associated with the current object, or the specified property could not be found.</exception>
		/// <exception cref="T:System.Configuration.SettingsPropertyIsReadOnlyException">An attempt was made to set a read-only property.</exception>
		/// <exception cref="T:System.Configuration.SettingsPropertyWrongTypeException">The value supplied is of a type incompatible with the settings property, during a set operation.</exception>
		// Token: 0x17000C3C RID: 3132
		public virtual object this[string propertyName]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of settings properties.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsPropertyCollection" /> collection containing all the <see cref="T:System.Configuration.SettingsProperty" /> objects.</returns>
		// Token: 0x17000C3D RID: 3133
		// (get) Token: 0x06003162 RID: 12642 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual SettingsPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a collection of settings property values.</summary>
		/// <returns>A collection of <see cref="T:System.Configuration.SettingsPropertyValue" /> objects representing the actual data values for the properties managed by the <see cref="T:System.Configuration.SettingsBase" /> instance.</returns>
		// Token: 0x17000C3E RID: 3134
		// (get) Token: 0x06003163 RID: 12643 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual SettingsPropertyValueCollection PropertyValues
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a collection of settings providers.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsProviderCollection" /> containing <see cref="T:System.Configuration.SettingsProvider" /> objects.</returns>
		// Token: 0x17000C3F RID: 3135
		// (get) Token: 0x06003164 RID: 12644 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual SettingsProviderCollection Providers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Initializes internal properties used by <see cref="T:System.Configuration.SettingsBase" /> object.</summary>
		/// <param name="context">The settings context related to the settings properties.</param>
		/// <param name="properties">The settings properties that will be accessible from the <see cref="T:System.Configuration.SettingsBase" /> instance.</param>
		/// <param name="providers">The initialized providers that should be used when loading and saving property values.</param>
		// Token: 0x06003165 RID: 12645 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Initialize(SettingsContext context, SettingsPropertyCollection properties, SettingsProviderCollection providers)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Stores the current values of the settings properties.</summary>
		// Token: 0x06003166 RID: 12646 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual void Save()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides a <see cref="T:System.Configuration.SettingsBase" /> class that is synchronized (thread safe).</summary>
		/// <param name="settingsBase">The class used to support user property settings.</param>
		/// <returns>A <see cref="T:System.Configuration.SettingsBase" /> class that is synchronized.</returns>
		// Token: 0x06003167 RID: 12647 RVA: 0x00043C3C File Offset: 0x00041E3C
		public static SettingsBase Synchronized(SettingsBase settingsBase)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
