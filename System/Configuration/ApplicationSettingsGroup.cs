﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a grouping of related application settings sections within a configuration file. This class cannot be inherited.</summary>
	// Token: 0x02000685 RID: 1669
	public sealed class ApplicationSettingsGroup : ConfigurationSectionGroup
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ApplicationSettingsGroup" /> class.</summary>
		// Token: 0x0600353B RID: 13627 RVA: 0x000092E2 File Offset: 0x000074E2
		public ApplicationSettingsGroup()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
