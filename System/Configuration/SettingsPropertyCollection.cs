﻿using System;
using System.Collections;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains a collection of <see cref="T:System.Configuration.SettingsProperty" /> objects.</summary>
	// Token: 0x02000608 RID: 1544
	public class SettingsPropertyCollection : ICollection, IEnumerable, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsPropertyCollection" /> class.</summary>
		// Token: 0x06003169 RID: 12649 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsPropertyCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that specifies the number of <see cref="T:System.Configuration.SettingsProperty" /> objects in the collection.</summary>
		/// <returns>The number of <see cref="T:System.Configuration.SettingsProperty" /> objects in the collection.</returns>
		// Token: 0x17000C40 RID: 3136
		// (get) Token: 0x0600316A RID: 12650 RVA: 0x000A4410 File Offset: 0x000A2610
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value that indicates whether access to the collection is synchronized (thread safe).</summary>
		/// <returns>
		///     <see langword="true" /> if access to the <see cref="T:System.Configuration.SettingsPropertyCollection" /> is synchronized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C41 RID: 3137
		// (get) Token: 0x0600316B RID: 12651 RVA: 0x000A442C File Offset: 0x000A262C
		public bool IsSynchronized
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the collection item with the specified name.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		/// <returns>The <see cref="T:System.Configuration.SettingsProperty" /> object with the specified <paramref name="name" />.</returns>
		// Token: 0x17000C42 RID: 3138
		public SettingsProperty this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the object to synchronize access to the collection.</summary>
		/// <returns>The object to synchronize access to the collection.</returns>
		// Token: 0x17000C43 RID: 3139
		// (get) Token: 0x0600316D RID: 12653 RVA: 0x00043C3C File Offset: 0x00041E3C
		public object SyncRoot
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a <see cref="T:System.Configuration.SettingsProperty" /> object to the collection.</summary>
		/// <param name="property">A <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		/// <exception cref="T:System.NotSupportedException">The collection is read-only.</exception>
		// Token: 0x0600316E RID: 12654 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Add(SettingsProperty property)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all <see cref="T:System.Configuration.SettingsProperty" /> objects from the collection.</summary>
		/// <exception cref="T:System.NotSupportedException">The collection is read-only.</exception>
		// Token: 0x0600316F RID: 12655 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a copy of the existing collection.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsPropertyCollection" /> class.</returns>
		// Token: 0x06003170 RID: 12656 RVA: 0x00043C3C File Offset: 0x00041E3C
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Copies this <see cref="T:System.Configuration.SettingsPropertyCollection" /> object to an array.</summary>
		/// <param name="array">The array to copy the object to.</param>
		/// <param name="index">The index at which to begin copying.</param>
		// Token: 0x06003171 RID: 12657 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(Array array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Collections.IEnumerator" /> object as it applies to the collection.</summary>
		/// <returns>The <see cref="T:System.Collections.IEnumerator" /> object as it applies to the collection.</returns>
		// Token: 0x06003172 RID: 12658 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Performs additional, custom processing when adding to the contents of the <see cref="T:System.Configuration.SettingsPropertyCollection" /> instance.</summary>
		/// <param name="property">A <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		// Token: 0x06003173 RID: 12659 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnAdd(SettingsProperty property)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Performs additional, custom processing after adding to the contents of the <see cref="T:System.Configuration.SettingsPropertyCollection" /> instance.</summary>
		/// <param name="property">A <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		// Token: 0x06003174 RID: 12660 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnAddComplete(SettingsProperty property)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Performs additional, custom processing when clearing the contents of the <see cref="T:System.Configuration.SettingsPropertyCollection" /> instance.</summary>
		// Token: 0x06003175 RID: 12661 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnClear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Performs additional, custom processing after clearing the contents of the <see cref="T:System.Configuration.SettingsPropertyCollection" /> instance.</summary>
		// Token: 0x06003176 RID: 12662 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnClearComplete()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Performs additional, custom processing when removing the contents of the <see cref="T:System.Configuration.SettingsPropertyCollection" /> instance.</summary>
		/// <param name="property">A <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		// Token: 0x06003177 RID: 12663 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnRemove(SettingsProperty property)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Performs additional, custom processing after removing the contents of the <see cref="T:System.Configuration.SettingsPropertyCollection" /> instance.</summary>
		/// <param name="property">A <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		// Token: 0x06003178 RID: 12664 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnRemoveComplete(SettingsProperty property)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes a <see cref="T:System.Configuration.SettingsProperty" /> object from the collection.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		/// <exception cref="T:System.NotSupportedException">The collection is read-only.</exception>
		// Token: 0x06003179 RID: 12665 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the collection to be read-only.</summary>
		// Token: 0x0600317A RID: 12666 RVA: 0x000092E2 File Offset: 0x000074E2
		public void SetReadOnly()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
