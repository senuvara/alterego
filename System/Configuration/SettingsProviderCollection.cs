﻿using System;
using System.Configuration.Provider;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a collection of application settings providers.</summary>
	// Token: 0x0200060F RID: 1551
	public class SettingsProviderCollection : ProviderCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsProviderCollection" /> class.</summary>
		// Token: 0x060031AE RID: 12718 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsProviderCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the settings provider in the collection that matches the specified name.</summary>
		/// <param name="name">A <see cref="T:System.String" /> containing the friendly name of the settings provider.</param>
		/// <returns>If found, the <see cref="T:System.Configuration.SettingsProvider" /> whose name matches that specified by the name parameter; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="name" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.NotSupportedException">The collection is read-only when setting this value.</exception>
		// Token: 0x17000C59 RID: 3161
		public SettingsProvider this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a new settings provider to the collection.</summary>
		/// <param name="provider">A <see cref="T:System.Configuration.Provider.ProviderBase" /> to add to the collection.</param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="provider" /> parameter is not of type <see cref="T:System.Configuration.SettingsProvider" />. -or-The <see cref="P:System.Configuration.Provider.ProviderBase.Name" /> property of the provider parameter is null or an empty string.-or-A settings provider with the same <see cref="P:System.Configuration.Provider.ProviderBase.Name" /> already exists in the collection.</exception>
		/// <exception cref="T:System.NotSupportedException">The collection is read-only.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="provider" /> parameter is <see langword="null" />.</exception>
		// Token: 0x060031B0 RID: 12720 RVA: 0x000092E2 File Offset: 0x000074E2
		public override void Add(ProviderBase provider)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
