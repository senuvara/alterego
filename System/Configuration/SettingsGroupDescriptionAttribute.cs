﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides a string that describes an application settings property group. This class cannot be inherited.</summary>
	// Token: 0x0200069C RID: 1692
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class SettingsGroupDescriptionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsGroupDescriptionAttribute" /> class.</summary>
		/// <param name="description">A <see cref="T:System.String" /> containing the descriptive text for the application settings group.</param>
		// Token: 0x0600359D RID: 13725 RVA: 0x0000232D File Offset: 0x0000052D
		public SettingsGroupDescriptionAttribute(string description)
		{
		}

		/// <summary>The descriptive text for the application settings properties group.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the descriptive text for the application settings group.</returns>
		// Token: 0x17000D9B RID: 3483
		// (get) Token: 0x0600359E RID: 13726 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string Description
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
