﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides a method for reading values of a particular type from the configuration.</summary>
	// Token: 0x02000686 RID: 1670
	public class AppSettingsReader
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.AppSettingsReader" /> class.</summary>
		// Token: 0x0600353C RID: 13628 RVA: 0x000092E2 File Offset: 0x000074E2
		public AppSettingsReader()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the value for a specified key from the <see cref="P:System.Configuration.ConfigurationSettings.AppSettings" /> property and returns an object of the specified type containing the value from the configuration.</summary>
		/// <param name="key">The key for which to get the value.</param>
		/// <param name="type">The type of the object to return.</param>
		/// <returns>The value of the specified key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />.- or -
		///         <paramref name="type" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="key" /> does not exist in the <see langword="&lt;appSettings&gt;" /> configuration section.- or -The value in the <see langword="&lt;appSettings&gt;" /> configuration section for <paramref name="key" /> is not of type <paramref name="type" />.</exception>
		// Token: 0x0600353D RID: 13629 RVA: 0x00043C3C File Offset: 0x00041E3C
		public object GetValue(string key, Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
