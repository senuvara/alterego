﻿using System;

namespace System.Configuration
{
	/// <summary>Provides values to indicate which services should be made available to application settings.</summary>
	// Token: 0x0200069E RID: 1694
	public enum SettingsManageability
	{
		/// <summary>Enables application settings to be stored in roaming user profiles. For more information about roaming user profiles, see Isolated Storage and Roaming.</summary>
		// Token: 0x04002522 RID: 9506
		Roaming
	}
}
