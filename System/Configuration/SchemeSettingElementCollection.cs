﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a collection of <see cref="T:System.Configuration.SchemeSettingElement" /> objects.</summary>
	// Token: 0x0200069A RID: 1690
	[ConfigurationCollection(typeof(SchemeSettingElement), CollectionType = ConfigurationElementCollectionType.AddRemoveClearMap, AddItemName = "add", ClearItemsName = "clear", RemoveItemName = "remove")]
	public sealed class SchemeSettingElementCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SchemeSettingElementCollection" /> class. </summary>
		// Token: 0x06003594 RID: 13716 RVA: 0x000092E2 File Offset: 0x000074E2
		public SchemeSettingElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the default collection type of <see cref="T:System.Configuration.SchemeSettingElementCollection" />. </summary>
		/// <returns>The default collection type of <see cref="T:System.Configuration.SchemeSettingElementCollection" />.</returns>
		// Token: 0x17000D98 RID: 3480
		// (get) Token: 0x06003595 RID: 13717 RVA: 0x000A5B08 File Offset: 0x000A3D08
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConfigurationElementCollectionType.BasicMap;
			}
		}

		// Token: 0x06003596 RID: 13718 RVA: 0x00043C3C File Offset: 0x00041E3C
		public SchemeSettingElement get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets an item from the <see cref="T:System.Configuration.SchemeSettingElementCollection" /> collection.</summary>
		/// <param name="name">A string reference to the <see cref="T:System.Configuration.SchemeSettingElement" /> object within the collection.</param>
		/// <returns>A <see cref="T:System.Configuration.SchemeSettingElement" /> object contained in the collection.</returns>
		// Token: 0x17000D99 RID: 3481
		public SchemeSettingElement this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x06003598 RID: 13720 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06003599 RID: 13721 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>The index of the specified <see cref="T:System.Configuration.SchemeSettingElement" />.</summary>
		/// <param name="element">The <see cref="T:System.Configuration.SchemeSettingElement" /> for the specified index location.</param>
		/// <returns>The index of the specified <see cref="T:System.Configuration.SchemeSettingElement" />; otherwise, -1.</returns>
		// Token: 0x0600359A RID: 13722 RVA: 0x000A5B24 File Offset: 0x000A3D24
		public int IndexOf(SchemeSettingElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}
	}
}
