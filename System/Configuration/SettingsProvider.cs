﻿using System;
using System.Configuration.Provider;
using Unity;

namespace System.Configuration
{
	/// <summary>Acts as a base class for deriving custom settings providers in the application settings architecture.</summary>
	// Token: 0x0200060A RID: 1546
	public abstract class SettingsProvider : ProviderBase
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.SettingsProvider" /> class.</summary>
		// Token: 0x0600318F RID: 12687 RVA: 0x000092E2 File Offset: 0x000074E2
		protected SettingsProvider()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the currently running application.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the application's shortened name, which does not contain a full path or extension, for example, SimpleAppSettings.</returns>
		// Token: 0x17000C4D RID: 3149
		// (get) Token: 0x06003190 RID: 12688
		// (set) Token: 0x06003191 RID: 12689
		public abstract string ApplicationName { get; set; }

		/// <summary>Returns the collection of settings property values for the specified application instance and settings property group.</summary>
		/// <param name="context">A <see cref="T:System.Configuration.SettingsContext" /> describing the current application use.</param>
		/// <param name="collection">A <see cref="T:System.Configuration.SettingsPropertyCollection" /> containing the settings property group whose values are to be retrieved.</param>
		/// <returns>A <see cref="T:System.Configuration.SettingsPropertyValueCollection" /> containing the values for the specified settings property group.</returns>
		// Token: 0x06003192 RID: 12690
		public abstract SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection);

		/// <summary>Sets the values of the specified group of property settings.</summary>
		/// <param name="context">A <see cref="T:System.Configuration.SettingsContext" /> describing the current application usage.</param>
		/// <param name="collection">A <see cref="T:System.Configuration.SettingsPropertyValueCollection" /> representing the group of property settings to set.</param>
		// Token: 0x06003193 RID: 12691
		public abstract void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection);
	}
}
