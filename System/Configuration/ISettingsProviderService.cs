﻿using System;

namespace System.Configuration
{
	/// <summary>Provides an interface for defining an alternate application settings provider.</summary>
	// Token: 0x02000694 RID: 1684
	public interface ISettingsProviderService
	{
		/// <summary>Returns the settings provider compatible with the specified settings property.</summary>
		/// <param name="property">The <see cref="T:System.Configuration.SettingsProperty" /> that requires serialization.</param>
		/// <returns>If found, the <see cref="T:System.Configuration.SettingsProvider" /> that can persist the specified settings property; otherwise, <see langword="null" />.</returns>
		// Token: 0x0600357F RID: 13695
		SettingsProvider GetSettingsProvider(SettingsProperty property);
	}
}
