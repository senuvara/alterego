﻿using System;
using System.Collections;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides contextual information that the provider can use when persisting settings.</summary>
	// Token: 0x02000607 RID: 1543
	[Serializable]
	public class SettingsContext : Hashtable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsContext" /> class. </summary>
		// Token: 0x06003168 RID: 12648 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsContext()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
