﻿using System;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides name/value-pair configuration information from a configuration section.</summary>
	// Token: 0x02000697 RID: 1687
	public class NameValueSectionHandler : IConfigurationSectionHandler
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.NameValueSectionHandler" /> class.</summary>
		// Token: 0x0600358B RID: 13707 RVA: 0x000092E2 File Offset: 0x000074E2
		public NameValueSectionHandler()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the XML attribute name to use as the key in a key/value pair.</summary>
		/// <returns>A <see cref="T:System.String" /> value containing the name of the key attribute.</returns>
		// Token: 0x17000D93 RID: 3475
		// (get) Token: 0x0600358C RID: 13708 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual string KeyAttributeName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the XML attribute name to use as the value in a key/value pair.</summary>
		/// <returns>A <see cref="T:System.String" /> value containing the name of the value attribute.</returns>
		// Token: 0x17000D94 RID: 3476
		// (get) Token: 0x0600358D RID: 13709 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual string ValueAttributeName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Creates a new configuration handler and adds it to the section-handler collection based on the specified parameters.</summary>
		/// <param name="parent">Parent object.</param>
		/// <param name="context">Configuration context object.</param>
		/// <param name="section">Section XML node.</param>
		/// <returns>A configuration object.</returns>
		// Token: 0x0600358E RID: 13710 RVA: 0x00043C3C File Offset: 0x00041E3C
		public object Create(object parent, object context, XmlNode section)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
