﻿using System;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides access to a configuration file. This type supports the .NET Framework configuration infrastructure and is not intended to be used directly from your code.</summary>
	// Token: 0x02000696 RID: 1686
	public class NameValueFileSectionHandler : IConfigurationSectionHandler
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.NameValueFileSectionHandler" /> class.</summary>
		// Token: 0x06003589 RID: 13705 RVA: 0x000092E2 File Offset: 0x000074E2
		public NameValueFileSectionHandler()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new configuration handler and adds it to the section-handler collection based on the specified parameters.</summary>
		/// <param name="parent">The parent object.</param>
		/// <param name="configContext">The configuration context object.</param>
		/// <param name="section">The section XML node.</param>
		/// <returns>A configuration object.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The file specified in the <see langword="file" /> attribute of <paramref name="section" /> exists but cannot be loaded.- or -The <see langword="name" /> attribute of <paramref name="section" /> does not match the root element of the file specified in the <see langword="file" /> attribute.</exception>
		// Token: 0x0600358A RID: 13706 RVA: 0x00043C3C File Offset: 0x00041E3C
		public object Create(object parent, object configContext, XmlNode section)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
