﻿using System;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Handles configuration sections that are represented by a single XML tag in the .config file.</summary>
	// Token: 0x020006A5 RID: 1701
	public class SingleTagSectionHandler : IConfigurationSectionHandler
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SingleTagSectionHandler" /> class. </summary>
		// Token: 0x060035B4 RID: 13748 RVA: 0x000092E2 File Offset: 0x000074E2
		public SingleTagSectionHandler()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Used internally to create a new instance of this object.</summary>
		/// <param name="parent">The parent of this object.</param>
		/// <param name="context">The context of this object.</param>
		/// <param name="section">The <see cref="T:System.Xml.XmlNode" /> object in the configuration.</param>
		/// <returns>The created object handler.</returns>
		// Token: 0x060035B5 RID: 13749 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual object Create(object parent, object context, XmlNode section)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
