﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a group of user-scoped application settings in a configuration file.</summary>
	// Token: 0x02000687 RID: 1671
	public sealed class ClientSettingsSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ClientSettingsSection" /> class.</summary>
		// Token: 0x0600353E RID: 13630 RVA: 0x000092E2 File Offset: 0x000074E2
		public ClientSettingsSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x17000D7C RID: 3452
		// (get) Token: 0x0600353F RID: 13631 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of client settings for the section.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingElementCollection" /> containing all the client settings found in the current configuration section.</returns>
		// Token: 0x17000D7D RID: 3453
		// (get) Token: 0x06003540 RID: 13632 RVA: 0x00043C3C File Offset: 0x00041E3C
		public SettingElementCollection Settings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
