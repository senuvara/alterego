﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Configuration
{
	/// <summary>Acts as a base class for deriving concrete wrapper classes to implement the application settings feature in Window Forms applications.</summary>
	// Token: 0x0200067F RID: 1663
	public abstract class ApplicationSettingsBase : SettingsBase, INotifyPropertyChanged
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" /> class to its default state.</summary>
		// Token: 0x0600350B RID: 13579 RVA: 0x000092E2 File Offset: 0x000074E2
		protected ApplicationSettingsBase()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" /> class using the supplied owner component.</summary>
		/// <param name="owner">The component that will act as the owner of the application settings object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="owner" /> is <see langword="null" />.</exception>
		// Token: 0x0600350C RID: 13580 RVA: 0x000092E2 File Offset: 0x000074E2
		protected ApplicationSettingsBase(IComponent owner)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" /> class using the supplied owner component and settings key.</summary>
		/// <param name="owner">The component that will act as the owner of the application settings object.</param>
		/// <param name="settingsKey">A <see cref="T:System.String" /> that uniquely identifies separate instances of the wrapper class.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="owner" /> is <see langword="null" />.</exception>
		// Token: 0x0600350D RID: 13581 RVA: 0x000092E2 File Offset: 0x000074E2
		protected ApplicationSettingsBase(IComponent owner, string settingsKey)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" /> class using the supplied settings key.</summary>
		/// <param name="settingsKey">A <see cref="T:System.String" /> that uniquely identifies separate instances of the wrapper class.</param>
		// Token: 0x0600350E RID: 13582 RVA: 0x000092E2 File Offset: 0x000074E2
		protected ApplicationSettingsBase(string settingsKey)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the application settings context associated with the settings group.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsContext" /> associated with the settings group.</returns>
		// Token: 0x17000D71 RID: 3441
		// (get) Token: 0x0600350F RID: 13583 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override SettingsContext Context
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the value of the specified application settings property.</summary>
		/// <param name="propertyName">A <see cref="T:System.String" /> containing the name of the property to access.</param>
		/// <returns>If found, the value of the named settings property; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.Configuration.SettingsPropertyNotFoundException">There are no properties associated with the current wrapper or the specified property could not be found.</exception>
		/// <exception cref="T:System.Configuration.SettingsPropertyIsReadOnlyException">An attempt was made to set a read-only property.</exception>
		/// <exception cref="T:System.Configuration.SettingsPropertyWrongTypeException">The value supplied is of a type incompatible with the settings property, during a set operation.</exception>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration file could not be parsed.</exception>
		// Token: 0x17000D72 RID: 3442
		public override object this[string propertyName]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of settings properties in the wrapper.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsPropertyCollection" /> containing all the <see cref="T:System.Configuration.SettingsProperty" /> objects used in the current wrapper.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The associated settings provider could not be found or its instantiation failed. </exception>
		// Token: 0x17000D73 RID: 3443
		// (get) Token: 0x06003512 RID: 13586 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override SettingsPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a collection of property values.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsPropertyValueCollection" /> of property values.</returns>
		// Token: 0x17000D74 RID: 3444
		// (get) Token: 0x06003513 RID: 13587 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override SettingsPropertyValueCollection PropertyValues
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of application settings providers used by the wrapper.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsProviderCollection" /> containing all the <see cref="T:System.Configuration.SettingsProvider" /> objects used by the settings properties of the current settings wrapper.</returns>
		// Token: 0x17000D75 RID: 3445
		// (get) Token: 0x06003514 RID: 13588 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override SettingsProviderCollection Providers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the settings key for the application settings group.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the settings key for the current settings group.</returns>
		// Token: 0x17000D76 RID: 3446
		// (get) Token: 0x06003515 RID: 13589 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003516 RID: 13590 RVA: 0x000092E2 File Offset: 0x000074E2
		public string SettingsKey
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs after the value of an application settings property is changed.</summary>
		// Token: 0x14000061 RID: 97
		// (add) Token: 0x06003517 RID: 13591 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x06003518 RID: 13592 RVA: 0x000092E2 File Offset: 0x000074E2
		public event PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs before the value of an application settings property is changed.</summary>
		// Token: 0x14000062 RID: 98
		// (add) Token: 0x06003519 RID: 13593 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x0600351A RID: 13594 RVA: 0x000092E2 File Offset: 0x000074E2
		public event SettingChangingEventHandler SettingChanging
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs after the application settings are retrieved from storage.</summary>
		// Token: 0x14000063 RID: 99
		// (add) Token: 0x0600351B RID: 13595 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x0600351C RID: 13596 RVA: 0x000092E2 File Offset: 0x000074E2
		public event SettingsLoadedEventHandler SettingsLoaded
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs before values are saved to the data store.</summary>
		// Token: 0x14000064 RID: 100
		// (add) Token: 0x0600351D RID: 13597 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x0600351E RID: 13598 RVA: 0x000092E2 File Offset: 0x000074E2
		public event SettingsSavingEventHandler SettingsSaving
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Returns the value of the named settings property for the previous version of the same application.</summary>
		/// <param name="propertyName">A <see cref="T:System.String" /> containing the name of the settings property whose value is to be returned.</param>
		/// <returns>An <see cref="T:System.Object" /> containing the value of the specified <see cref="T:System.Configuration.SettingsProperty" /> if found; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.Configuration.SettingsPropertyNotFoundException">The property does not exist. The property count is zero or the property cannot be found in the data store.</exception>
		// Token: 0x0600351F RID: 13599 RVA: 0x00043C3C File Offset: 0x00041E3C
		public object GetPreviousVersion(string propertyName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Raises the <see cref="E:System.Configuration.ApplicationSettingsBase.PropertyChanged" /> event.</summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">A <see cref="T:System.ComponentModel.PropertyChangedEventArgs" /> that contains the event data.</param>
		// Token: 0x06003520 RID: 13600 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Configuration.ApplicationSettingsBase.SettingChanging" /> event.</summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">A <see cref="T:System.Configuration.SettingChangingEventArgs" /> that contains the event data.</param>
		// Token: 0x06003521 RID: 13601 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnSettingChanging(object sender, SettingChangingEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Configuration.ApplicationSettingsBase.SettingsLoaded" /> event.</summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">A <see cref="T:System.Configuration.SettingsLoadedEventArgs" /> that contains the event data.</param>
		// Token: 0x06003522 RID: 13602 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnSettingsLoaded(object sender, SettingsLoadedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Configuration.ApplicationSettingsBase.SettingsSaving" /> event.</summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the event data.</param>
		// Token: 0x06003523 RID: 13603 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnSettingsSaving(object sender, CancelEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Refreshes the application settings property values from persistent storage.</summary>
		// Token: 0x06003524 RID: 13604 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Reload()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Restores the persisted application settings values to their corresponding default properties.</summary>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration file could not be parsed.</exception>
		// Token: 0x06003525 RID: 13605 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Reset()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Stores the current values of the application settings properties.</summary>
		// Token: 0x06003526 RID: 13606 RVA: 0x000092E2 File Offset: 0x000074E2
		public override void Save()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates application settings to reflect a more recent installation of the application.</summary>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration file could not be parsed.</exception>
		// Token: 0x06003527 RID: 13607 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual void Upgrade()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
