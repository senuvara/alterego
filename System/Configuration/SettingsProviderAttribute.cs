﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Specifies the settings provider used to provide storage for the current application settings class or property. This class cannot be inherited.</summary>
	// Token: 0x020006A3 RID: 1699
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
	public sealed class SettingsProviderAttribute : Attribute
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.SettingsProviderAttribute" /> class.</summary>
		/// <param name="providerTypeName">A <see cref="T:System.String" /> containing the name of the settings provider.</param>
		// Token: 0x060035AF RID: 13743 RVA: 0x0000232D File Offset: 0x0000052D
		public SettingsProviderAttribute(string providerTypeName)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsProviderAttribute" /> class. </summary>
		/// <param name="providerType">A <see cref="T:System.Type" /> containing the settings provider type.</param>
		// Token: 0x060035B0 RID: 13744 RVA: 0x0000232D File Offset: 0x0000052D
		public SettingsProviderAttribute(Type providerType)
		{
		}

		/// <summary>Gets the type name of the settings provider.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the name of the settings provider.</returns>
		// Token: 0x17000D9E RID: 3486
		// (get) Token: 0x060035B1 RID: 13745 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string ProviderTypeName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
