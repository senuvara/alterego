﻿using System;

namespace System.Configuration
{
	/// <summary>Specifies that an application settings group or property contains distinct values for each user of an application. This class cannot be inherited.</summary>
	// Token: 0x020006A9 RID: 1705
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class UserScopedSettingAttribute : SettingAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.UserScopedSettingAttribute" /> class.</summary>
		// Token: 0x060035BD RID: 13757 RVA: 0x0000232D File Offset: 0x0000052D
		public UserScopedSettingAttribute()
		{
		}
	}
}
