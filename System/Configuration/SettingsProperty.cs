﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Used internally as the class that represents metadata about an individual configuration property.</summary>
	// Token: 0x02000609 RID: 1545
	public class SettingsProperty
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsProperty" /> class, based on the supplied parameter.</summary>
		/// <param name="propertyToCopy">Specifies a copy of an existing <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		// Token: 0x0600317B RID: 12667 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsProperty(SettingsProperty propertyToCopy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsProperty" /> class. based on the supplied parameter.</summary>
		/// <param name="name">Specifies the name of an existing <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		// Token: 0x0600317C RID: 12668 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsProperty(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Configuration.SettingsProperty" /> class based on the supplied parameters.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		/// <param name="propertyType">The type of <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		/// <param name="provider">A <see cref="T:System.Configuration.SettingsProvider" /> object to use for persistence.</param>
		/// <param name="isReadOnly">A <see cref="T:System.Boolean" /> value specifying whether the <see cref="T:System.Configuration.SettingsProperty" /> object is read-only.</param>
		/// <param name="defaultValue">The default value of the <see cref="T:System.Configuration.SettingsProperty" /> object.</param>
		/// <param name="serializeAs">A <see cref="T:System.Configuration.SettingsSerializeAs" /> object. This object is an enumeration used to set the serialization scheme for storing application settings.</param>
		/// <param name="attributes">A <see cref="T:System.Configuration.SettingsAttributeDictionary" /> object.</param>
		/// <param name="throwOnErrorDeserializing">A Boolean value specifying whether an error will be thrown when the property is unsuccessfully deserialized.</param>
		/// <param name="throwOnErrorSerializing">A Boolean value specifying whether an error will be thrown when the property is unsuccessfully serialized.</param>
		// Token: 0x0600317D RID: 12669 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsProperty(string name, Type propertyType, SettingsProvider provider, bool isReadOnly, object defaultValue, SettingsSerializeAs serializeAs, SettingsAttributeDictionary attributes, bool throwOnErrorDeserializing, bool throwOnErrorSerializing)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Configuration.SettingsAttributeDictionary" /> object containing the attributes of the <see cref="T:System.Configuration.SettingsProperty" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsAttributeDictionary" /> object.</returns>
		// Token: 0x17000C44 RID: 3140
		// (get) Token: 0x0600317E RID: 12670 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual SettingsAttributeDictionary Attributes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the default value of the <see cref="T:System.Configuration.SettingsProperty" /> object.</summary>
		/// <returns>An object containing the default value of the <see cref="T:System.Configuration.SettingsProperty" /> object.</returns>
		// Token: 0x17000C45 RID: 3141
		// (get) Token: 0x0600317F RID: 12671 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003180 RID: 12672 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual object DefaultValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value specifying whether a <see cref="T:System.Configuration.SettingsProperty" /> object is read-only. </summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.SettingsProperty" /> is read-only; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C46 RID: 3142
		// (get) Token: 0x06003181 RID: 12673 RVA: 0x000A4448 File Offset: 0x000A2648
		// (set) Token: 0x06003182 RID: 12674 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual bool IsReadOnly
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the <see cref="T:System.Configuration.SettingsProperty" />.</summary>
		/// <returns>The name of the <see cref="T:System.Configuration.SettingsProperty" />.</returns>
		// Token: 0x17000C47 RID: 3143
		// (get) Token: 0x06003183 RID: 12675 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003184 RID: 12676 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the type for the <see cref="T:System.Configuration.SettingsProperty" />.</summary>
		/// <returns>The type for the <see cref="T:System.Configuration.SettingsProperty" />.</returns>
		// Token: 0x17000C48 RID: 3144
		// (get) Token: 0x06003185 RID: 12677 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003186 RID: 12678 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual Type PropertyType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the provider for the <see cref="T:System.Configuration.SettingsProperty" />.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsProvider" /> object.</returns>
		// Token: 0x17000C49 RID: 3145
		// (get) Token: 0x06003187 RID: 12679 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003188 RID: 12680 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual SettingsProvider Provider
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Configuration.SettingsSerializeAs" /> object for the <see cref="T:System.Configuration.SettingsProperty" />.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsSerializeAs" /> object.</returns>
		// Token: 0x17000C4A RID: 3146
		// (get) Token: 0x06003189 RID: 12681 RVA: 0x000A4464 File Offset: 0x000A2664
		// (set) Token: 0x0600318A RID: 12682 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual SettingsSerializeAs SerializeAs
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SettingsSerializeAs.String;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value specifying whether an error will be thrown when the property is unsuccessfully deserialized.</summary>
		/// <returns>
		///     <see langword="true" /> if the error will be thrown when the property is unsuccessfully deserialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C4B RID: 3147
		// (get) Token: 0x0600318B RID: 12683 RVA: 0x000A4480 File Offset: 0x000A2680
		// (set) Token: 0x0600318C RID: 12684 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool ThrowOnErrorDeserializing
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value specifying whether an error will be thrown when the property is unsuccessfully serialized.</summary>
		/// <returns>
		///     <see langword="true" /> if the error will be thrown when the property is unsuccessfully serialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C4C RID: 3148
		// (get) Token: 0x0600318D RID: 12685 RVA: 0x000A449C File Offset: 0x000A269C
		// (set) Token: 0x0600318E RID: 12686 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool ThrowOnErrorSerializing
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
