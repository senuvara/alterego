﻿using System;
using System.Configuration.Internal;
using System.Security.Permissions;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Wraps the corresponding <see cref="T:System.Xml.XmlDocument" /> type and also carries the necessary information for reporting file-name and line numbers. </summary>
	// Token: 0x0200068C RID: 1676
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public sealed class ConfigXmlDocument : XmlDocument, IConfigErrorInfo
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigXmlDocument" /> class. </summary>
		// Token: 0x06003560 RID: 13664 RVA: 0x000092E2 File Offset: 0x000074E2
		public ConfigXmlDocument()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the configuration file name.</summary>
		/// <returns>The configuration file name.</returns>
		// Token: 0x17000D87 RID: 3463
		// (get) Token: 0x06003561 RID: 13665 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string Filename
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the current node line number.</summary>
		/// <returns>The line number for the current node.</returns>
		// Token: 0x17000D88 RID: 3464
		// (get) Token: 0x06003562 RID: 13666 RVA: 0x000A5A7C File Offset: 0x000A3C7C
		public int LineNumber
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		// Token: 0x06003563 RID: 13667 RVA: 0x00043C3C File Offset: 0x00041E3C
		string IConfigErrorInfo.get_Filename()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06003564 RID: 13668 RVA: 0x000A5A98 File Offset: 0x000A3C98
		int IConfigErrorInfo.get_LineNumber()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Loads a single configuration element.</summary>
		/// <param name="filename">The name of the file.</param>
		/// <param name="sourceReader">The source for the reader.</param>
		// Token: 0x06003565 RID: 13669 RVA: 0x000092E2 File Offset: 0x000074E2
		public void LoadSingleElement(string filename, XmlTextReader sourceReader)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
