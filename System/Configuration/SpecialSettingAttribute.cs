﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Indicates that an application settings property has a special significance. This class cannot be inherited.</summary>
	// Token: 0x020006A7 RID: 1703
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
	public sealed class SpecialSettingAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SpecialSettingAttribute" /> class.</summary>
		/// <param name="specialSetting">A <see cref="T:System.Configuration.SpecialSetting" /> enumeration value defining the category of the application settings property.</param>
		// Token: 0x060035B6 RID: 13750 RVA: 0x0000232D File Offset: 0x0000052D
		public SpecialSettingAttribute(SpecialSetting specialSetting)
		{
		}

		/// <summary>Gets the value describing the special setting category of the application settings property.</summary>
		/// <returns>A <see cref="T:System.Configuration.SpecialSetting" /> enumeration value defining the category of the application settings property.</returns>
		// Token: 0x17000DA0 RID: 3488
		// (get) Token: 0x060035B7 RID: 13751 RVA: 0x000A5B78 File Offset: 0x000A3D78
		public SpecialSetting SpecialSetting
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SpecialSetting.ConnectionString;
			}
		}
	}
}
