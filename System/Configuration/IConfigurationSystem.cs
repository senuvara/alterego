﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration
{
	/// <summary>Provides standard configuration methods.</summary>
	// Token: 0x0200068F RID: 1679
	[ComVisible(false)]
	public interface IConfigurationSystem
	{
		/// <summary>Gets the specified configuration.</summary>
		/// <param name="configKey">The configuration key.</param>
		/// <returns>The object representing the configuration.</returns>
		// Token: 0x0600356C RID: 13676
		object GetConfig(string configKey);

		/// <summary>Used for initialization.</summary>
		// Token: 0x0600356D RID: 13677
		void Init();
	}
}
