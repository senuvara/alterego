﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides the configuration setting for International Domain Name (IDN) processing in the <see cref="T:System.Uri" /> class.</summary>
	// Token: 0x02000690 RID: 1680
	public sealed class IdnElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.IdnElement" /> class.</summary>
		// Token: 0x0600356E RID: 13678 RVA: 0x000092E2 File Offset: 0x000074E2
		public IdnElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the value of the <see cref="T:System.Configuration.IdnElement" /> configuration setting. </summary>
		/// <returns>A <see cref="T:System.UriIdnScope" /> that contains the current configuration setting for IDN processing.</returns>
		// Token: 0x17000D8C RID: 3468
		// (get) Token: 0x0600356F RID: 13679 RVA: 0x000A5AB4 File Offset: 0x000A3CB4
		// (set) Token: 0x06003570 RID: 13680 RVA: 0x000092E2 File Offset: 0x000074E2
		public UriIdnScope Enabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return UriIdnScope.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000D8D RID: 3469
		// (get) Token: 0x06003571 RID: 13681 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
