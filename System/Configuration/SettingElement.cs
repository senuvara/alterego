﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a simplified configuration element used for updating elements in the configuration. This class cannot be inherited.</summary>
	// Token: 0x02000689 RID: 1673
	public sealed class SettingElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingElement" /> class.</summary>
		// Token: 0x0600354A RID: 13642 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingElement" /> class based on supplied parameters.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.SettingElement" /> object.</param>
		/// <param name="serializeAs">A <see cref="T:System.Configuration.SettingsSerializeAs" /> object. This object is an enumeration used as the serialization scheme to store configuration settings.</param>
		// Token: 0x0600354B RID: 13643 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingElement(string name, SettingsSerializeAs serializeAs)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the <see cref="T:System.Configuration.SettingElement" /> object.</summary>
		/// <returns>The name of the <see cref="T:System.Configuration.SettingElement" /> object.</returns>
		// Token: 0x17000D80 RID: 3456
		// (get) Token: 0x0600354C RID: 13644 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600354D RID: 13645 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000D81 RID: 3457
		// (get) Token: 0x0600354E RID: 13646 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the serialization mechanism used to persist the values of the <see cref="T:System.Configuration.SettingElement" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsSerializeAs" /> object.</returns>
		// Token: 0x17000D82 RID: 3458
		// (get) Token: 0x0600354F RID: 13647 RVA: 0x000A5A28 File Offset: 0x000A3C28
		// (set) Token: 0x06003550 RID: 13648 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsSerializeAs SerializeAs
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SettingsSerializeAs.String;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the value of a <see cref="T:System.Configuration.SettingElement" /> object by using a <see cref="T:System.Configuration.SettingValueElement" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingValueElement" /> object containing the value of the <see cref="T:System.Configuration.SettingElement" /> object.</returns>
		// Token: 0x17000D83 RID: 3459
		// (get) Token: 0x06003551 RID: 13649 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003552 RID: 13650 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingValueElement Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
