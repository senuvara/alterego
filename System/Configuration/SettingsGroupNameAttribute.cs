﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Specifies a name for application settings property group. This class cannot be inherited.</summary>
	// Token: 0x0200069D RID: 1693
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class SettingsGroupNameAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsGroupNameAttribute" /> class.</summary>
		/// <param name="groupName">A <see cref="T:System.String" /> containing the name of the application settings property group.</param>
		// Token: 0x0600359F RID: 13727 RVA: 0x0000232D File Offset: 0x0000052D
		public SettingsGroupNameAttribute(string groupName)
		{
		}

		/// <summary>Gets the name of the application settings property group.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the name of the application settings property group.</returns>
		// Token: 0x17000D9C RID: 3484
		// (get) Token: 0x060035A0 RID: 13728 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string GroupName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
