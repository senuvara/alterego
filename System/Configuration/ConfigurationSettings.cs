﻿using System;
using System.Collections.Specialized;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides runtime versions 1.0 and 1.1 support for reading configuration sections and common configuration settings.</summary>
	// Token: 0x0200068B RID: 1675
	public sealed class ConfigurationSettings
	{
		// Token: 0x0600355D RID: 13661 RVA: 0x000092E2 File Offset: 0x000074E2
		internal ConfigurationSettings()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a read-only <see cref="T:System.Collections.Specialized.NameValueCollection" /> of the application settings section of the configuration file.</summary>
		/// <returns>A read-only <see cref="T:System.Collections.Specialized.NameValueCollection" /> of the application settings section from the configuration file.</returns>
		// Token: 0x17000D86 RID: 3462
		// (get) Token: 0x0600355E RID: 13662 RVA: 0x00043C3C File Offset: 0x00041E3C
		public static NameValueCollection AppSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Returns the <see cref="T:System.Configuration.ConfigurationSection" /> object for the passed configuration section name and path.</summary>
		/// <param name="sectionName">A configuration name and path, such as "system.net/settings".</param>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationSection" /> object for the passed configuration section name and path.The <see cref="T:System.Configuration.ConfigurationSettings" /> class provides backward compatibility only. You should use the <see cref="T:System.Configuration.ConfigurationManager" /> class or <see cref="T:System.Web.Configuration.WebConfigurationManager" /> class instead.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationException">Unable to retrieve the requested section.</exception>
		// Token: 0x0600355F RID: 13663 RVA: 0x00043C3C File Offset: 0x00041E3C
		[Obsolete("This method is obsolete, it has been replaced by System.Configuration!System.Configuration.ConfigurationManager.GetSection")]
		public static object GetConfig(string sectionName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
