﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents an element in a <see cref="T:System.Configuration.SchemeSettingElementCollection" /> class.</summary>
	// Token: 0x02000699 RID: 1689
	public sealed class SchemeSettingElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SchemeSettingElement" /> class.</summary>
		// Token: 0x06003590 RID: 13712 RVA: 0x000092E2 File Offset: 0x000074E2
		public SchemeSettingElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the value of the GenericUriParserOptions entry from a <see cref="T:System.Configuration.SchemeSettingElement" /> instance.</summary>
		/// <returns>The value of GenericUriParserOptions entry.</returns>
		// Token: 0x17000D95 RID: 3477
		// (get) Token: 0x06003591 RID: 13713 RVA: 0x000A5AEC File Offset: 0x000A3CEC
		public GenericUriParserOptions GenericUriParserOptions
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return GenericUriParserOptions.Default;
			}
		}

		/// <summary>Gets the value of the Name entry from a <see cref="T:System.Configuration.SchemeSettingElement" /> instance.</summary>
		/// <returns>The protocol used by this schema setting. </returns>
		// Token: 0x17000D96 RID: 3478
		// (get) Token: 0x06003592 RID: 13714 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000D97 RID: 3479
		// (get) Token: 0x06003593 RID: 13715 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
