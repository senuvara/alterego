﻿using System;

namespace System.Configuration
{
	/// <summary>Specifies that a settings provider should disable any logic that gets invoked when an application upgrade is detected. This class cannot be inherited.</summary>
	// Token: 0x02000698 RID: 1688
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class NoSettingsVersionUpgradeAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.NoSettingsVersionUpgradeAttribute" /> class.</summary>
		// Token: 0x0600358F RID: 13711 RVA: 0x0000232D File Offset: 0x0000052D
		public NoSettingsVersionUpgradeAttribute()
		{
		}
	}
}
