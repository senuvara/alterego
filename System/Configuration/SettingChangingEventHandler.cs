﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Configuration.ApplicationSettingsBase.SettingChanging" /> event. </summary>
	/// <param name="sender">The source of the event, typically an application settings wrapper class derived from the <see cref="T:System.Configuration.ApplicationSettingsBase" /> class.</param>
	/// <param name="e">A <see cref="T:System.Configuration.SettingChangingEventArgs" /> containing the data for the event.</param>
	// Token: 0x02000680 RID: 1664
	public sealed class SettingChangingEventHandler : MulticastDelegate
	{
		// Token: 0x06003528 RID: 13608 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingChangingEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003529 RID: 13609 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, SettingChangingEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600352A RID: 13610 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, SettingChangingEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600352B RID: 13611 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
