﻿using System;

namespace System.Configuration
{
	/// <summary>Specifies that an application settings property has a common value for all users of an application. This class cannot be inherited.</summary>
	// Token: 0x0200067D RID: 1661
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class ApplicationScopedSettingAttribute : SettingAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ApplicationScopedSettingAttribute" /> class. </summary>
		// Token: 0x06003509 RID: 13577 RVA: 0x0000232D File Offset: 0x0000052D
		public ApplicationScopedSettingAttribute()
		{
		}
	}
}
