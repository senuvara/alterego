﻿using System;
using System.Collections;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a collection of key/value pairs used to describe a configuration object as well as a <see cref="T:System.Configuration.SettingsProperty" /> object.</summary>
	// Token: 0x0200060E RID: 1550
	[Serializable]
	public class SettingsAttributeDictionary : Hashtable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsAttributeDictionary" /> class. </summary>
		// Token: 0x060031AC RID: 12716 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsAttributeDictionary()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsAttributeDictionary" /> class. </summary>
		/// <param name="attributes">A collection of key/value pairs that are related to configuration settings.</param>
		// Token: 0x060031AD RID: 12717 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsAttributeDictionary(SettingsAttributeDictionary attributes)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
