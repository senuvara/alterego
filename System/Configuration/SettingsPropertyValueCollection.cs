﻿using System;
using System.Collections;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains a collection of settings property values that map <see cref="T:System.Configuration.SettingsProperty" /> objects to <see cref="T:System.Configuration.SettingsPropertyValue" /> objects.</summary>
	// Token: 0x0200060B RID: 1547
	public class SettingsPropertyValueCollection : ICollection, IEnumerable, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsPropertyValueCollection" /> class.</summary>
		// Token: 0x06003194 RID: 12692 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsPropertyValueCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that specifies the number of <see cref="T:System.Configuration.SettingsPropertyValue" /> objects in the collection.</summary>
		/// <returns>The number of <see cref="T:System.Configuration.SettingsPropertyValue" /> objects in the collection.</returns>
		// Token: 0x17000C4E RID: 3150
		// (get) Token: 0x06003195 RID: 12693 RVA: 0x000A44B8 File Offset: 0x000A26B8
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value that indicates whether access to the collection is synchronized (thread safe).</summary>
		/// <returns>
		///     <see langword="true" /> if access to the <see cref="T:System.Configuration.SettingsPropertyValueCollection" /> collection is synchronized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C4F RID: 3151
		// (get) Token: 0x06003196 RID: 12694 RVA: 0x000A44D4 File Offset: 0x000A26D4
		public bool IsSynchronized
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets an item from the collection.</summary>
		/// <param name="name">A <see cref="T:System.Configuration.SettingsPropertyValue" /> object.</param>
		/// <returns>The <see cref="T:System.Configuration.SettingsPropertyValue" /> object with the specified <paramref name="name" />.</returns>
		// Token: 0x17000C50 RID: 3152
		public SettingsPropertyValue this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the object to synchronize access to the collection.</summary>
		/// <returns>The object to synchronize access to the collection.</returns>
		// Token: 0x17000C51 RID: 3153
		// (get) Token: 0x06003198 RID: 12696 RVA: 0x00043C3C File Offset: 0x00041E3C
		public object SyncRoot
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a <see cref="T:System.Configuration.SettingsPropertyValue" /> object to the collection.</summary>
		/// <param name="property">A <see cref="T:System.Configuration.SettingsPropertyValue" /> object.</param>
		/// <exception cref="T:System.NotSupportedException">An attempt was made to add an item to the collection, but the collection was marked as read-only.</exception>
		// Token: 0x06003199 RID: 12697 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Add(SettingsPropertyValue property)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all <see cref="T:System.Configuration.SettingsPropertyValue" /> objects from the collection.</summary>
		// Token: 0x0600319A RID: 12698 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a copy of the existing collection.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsPropertyValueCollection" /> class.</returns>
		// Token: 0x0600319B RID: 12699 RVA: 0x00043C3C File Offset: 0x00041E3C
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Copies this <see cref="T:System.Configuration.SettingsPropertyValueCollection" /> collection to an array.</summary>
		/// <param name="array">The array to copy the collection to.</param>
		/// <param name="index">The index at which to begin copying.</param>
		// Token: 0x0600319C RID: 12700 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(Array array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Collections.IEnumerator" /> object as it applies to the collection.</summary>
		/// <returns>The <see cref="T:System.Collections.IEnumerator" /> object as it applies to the collection.</returns>
		// Token: 0x0600319D RID: 12701 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes a <see cref="T:System.Configuration.SettingsPropertyValue" /> object from the collection.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.SettingsPropertyValue" /> object.</param>
		/// <exception cref="T:System.NotSupportedException">An attempt was made to remove an item from the collection, but the collection was marked as read-only.</exception>
		// Token: 0x0600319E RID: 12702 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the collection to be read-only.</summary>
		// Token: 0x0600319F RID: 12703 RVA: 0x000092E2 File Offset: 0x000074E2
		public void SetReadOnly()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
