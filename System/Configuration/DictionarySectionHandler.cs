﻿using System;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides key/value pair configuration information from a configuration section.</summary>
	// Token: 0x0200068E RID: 1678
	public class DictionarySectionHandler : IConfigurationSectionHandler
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.DictionarySectionHandler" /> class. </summary>
		// Token: 0x06003568 RID: 13672 RVA: 0x000092E2 File Offset: 0x000074E2
		public DictionarySectionHandler()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the XML attribute name to use as the key in a key/value pair.</summary>
		/// <returns>A string value containing the name of the key attribute.</returns>
		// Token: 0x17000D8A RID: 3466
		// (get) Token: 0x06003569 RID: 13673 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual string KeyAttributeName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the XML attribute name to use as the value in a key/value pair.</summary>
		/// <returns>A string value containing the name of the value attribute.</returns>
		// Token: 0x17000D8B RID: 3467
		// (get) Token: 0x0600356A RID: 13674 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual string ValueAttributeName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Creates a new configuration handler and adds it to the section-handler collection based on the specified parameters.</summary>
		/// <param name="parent">Parent object.</param>
		/// <param name="context">Configuration context object.</param>
		/// <param name="section">Section XML node.</param>
		/// <returns>A configuration object.</returns>
		// Token: 0x0600356B RID: 13675 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual object Create(object parent, object context, XmlNode section)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
