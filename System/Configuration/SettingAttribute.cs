﻿using System;

namespace System.Configuration
{
	/// <summary>Represents a custom settings attribute used to associate settings information with a settings property.</summary>
	// Token: 0x0200067E RID: 1662
	[AttributeUsage(AttributeTargets.Property)]
	public class SettingAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingAttribute" /> class. </summary>
		// Token: 0x0600350A RID: 13578 RVA: 0x0000232D File Offset: 0x0000052D
		public SettingAttribute()
		{
		}
	}
}
