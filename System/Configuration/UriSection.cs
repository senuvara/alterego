﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents the Uri section within a configuration file.</summary>
	// Token: 0x020006A8 RID: 1704
	public sealed class UriSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.UriSection" /> class.</summary>
		// Token: 0x060035B8 RID: 13752 RVA: 0x000092E2 File Offset: 0x000074E2
		public UriSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an <see cref="T:System.Configuration.IdnElement" /> object that contains the configuration setting for International Domain Name (IDN) processing in the <see cref="T:System.Uri" /> class.</summary>
		/// <returns>The configuration setting for International Domain Name (IDN) processing in the <see cref="T:System.Uri" /> class.</returns>
		// Token: 0x17000DA1 RID: 3489
		// (get) Token: 0x060035B9 RID: 13753 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IdnElement Idn
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Configuration.IriParsingElement" /> object that contains the configuration setting for International Resource Identifiers (IRI) parsing in the <see cref="T:System.Uri" /> class.</summary>
		/// <returns>The configuration setting for International Resource Identifiers (IRI) parsing in the <see cref="T:System.Uri" /> class.</returns>
		// Token: 0x17000DA2 RID: 3490
		// (get) Token: 0x060035BA RID: 13754 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IriParsingElement IriParsing
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000DA3 RID: 3491
		// (get) Token: 0x060035BB RID: 13755 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Configuration.SchemeSettingElementCollection" /> object that contains the configuration settings for scheme parsing in the <see cref="T:System.Uri" /> class.</summary>
		/// <returns>The configuration settings for scheme parsing in the <see cref="T:System.Uri" /> class</returns>
		// Token: 0x17000DA4 RID: 3492
		// (get) Token: 0x060035BC RID: 13756 RVA: 0x00043C3C File Offset: 0x00041E3C
		public SchemeSettingElementCollection SchemeSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
