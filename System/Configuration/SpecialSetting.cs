﻿using System;

namespace System.Configuration
{
	/// <summary>Specifies the special setting category of a application settings property.</summary>
	// Token: 0x020006A6 RID: 1702
	public enum SpecialSetting
	{
		/// <summary>The configuration property represents a connection string, typically for a data store or network resource. </summary>
		// Token: 0x04002524 RID: 9508
		ConnectionString,
		/// <summary>The configuration property represents a Uniform Resource Locator (URL) to a Web service.</summary>
		// Token: 0x04002525 RID: 9509
		WebServiceUrl
	}
}
