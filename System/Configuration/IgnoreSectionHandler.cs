﻿using System;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides a legacy section-handler definition for configuration sections that are not handled by the <see cref="N:System.Configuration" /> types.</summary>
	// Token: 0x02000691 RID: 1681
	public class IgnoreSectionHandler : IConfigurationSectionHandler
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.IgnoreSectionHandler" /> class.</summary>
		// Token: 0x06003572 RID: 13682 RVA: 0x000092E2 File Offset: 0x000074E2
		public IgnoreSectionHandler()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new configuration handler and adds the specified configuration object to the section-handler collection.</summary>
		/// <param name="parent">The configuration settings in a corresponding parent configuration section. </param>
		/// <param name="configContext">The virtual path for which the configuration section handler computes configuration values. Normally this parameter is reserved and is <see langword="null" />. </param>
		/// <param name="section">An <see cref="T:System.Xml.XmlNode" /> that contains the configuration information to be handled. Provides direct access to the XML contents of the configuration section. </param>
		/// <returns>The created configuration handler object.</returns>
		// Token: 0x06003573 RID: 13683 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual object Create(object parent, object configContext, XmlNode section)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
