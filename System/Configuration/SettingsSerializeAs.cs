﻿using System;

namespace System.Configuration
{
	/// <summary>Determines the serialization scheme used to store application settings.</summary>
	// Token: 0x0200060D RID: 1549
	public enum SettingsSerializeAs
	{
		/// <summary>The settings property is serialized using binary object serialization.</summary>
		// Token: 0x040024C1 RID: 9409
		Binary = 2,
		/// <summary>The settings provider has implicit knowledge of the property or its type and picks an appropriate serialization mechanism. Often used for custom serialization.</summary>
		// Token: 0x040024C2 RID: 9410
		ProviderSpecific,
		/// <summary>The settings property is serialized as plain text.</summary>
		// Token: 0x040024C3 RID: 9411
		String = 0,
		/// <summary>The settings property is serialized as XML using XML serialization.</summary>
		// Token: 0x040024C4 RID: 9412
		Xml
	}
}
