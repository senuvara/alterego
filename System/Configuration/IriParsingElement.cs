﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides the configuration setting for International Resource Identifier (IRI) processing in the <see cref="T:System.Uri" /> class.</summary>
	// Token: 0x02000693 RID: 1683
	public sealed class IriParsingElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.IriParsingElement" /> class.</summary>
		// Token: 0x0600357B RID: 13691 RVA: 0x000092E2 File Offset: 0x000074E2
		public IriParsingElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the value of the <see cref="T:System.Configuration.IriParsingElement" /> configuration setting.</summary>
		/// <returns>A Boolean that indicates if International Resource Identifier (IRI) processing is enabled. </returns>
		// Token: 0x17000D90 RID: 3472
		// (get) Token: 0x0600357C RID: 13692 RVA: 0x000A5AD0 File Offset: 0x000A3CD0
		// (set) Token: 0x0600357D RID: 13693 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool Enabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000D91 RID: 3473
		// (get) Token: 0x0600357E RID: 13694 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
