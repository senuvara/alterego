﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Specifies special services for application settings properties. This class cannot be inherited.</summary>
	// Token: 0x0200069F RID: 1695
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
	public sealed class SettingsManageabilityAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsManageabilityAttribute" /> class.</summary>
		/// <param name="manageability">A <see cref="T:System.Configuration.SettingsManageability" /> value that enumerates the services being requested. </param>
		// Token: 0x060035A1 RID: 13729 RVA: 0x0000232D File Offset: 0x0000052D
		public SettingsManageabilityAttribute(SettingsManageability manageability)
		{
		}

		/// <summary>Gets the set of special services that have been requested.</summary>
		/// <returns>A value that results from using the logical <see langword="OR" /> operator to combine all the <see cref="T:System.Configuration.SettingsManageability" /> enumeration values corresponding to the requested services.</returns>
		// Token: 0x17000D9D RID: 3485
		// (get) Token: 0x060035A2 RID: 13730 RVA: 0x000A5B40 File Offset: 0x000A3D40
		public SettingsManageability Manageability
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SettingsManageability.Roaming;
			}
		}
	}
}
