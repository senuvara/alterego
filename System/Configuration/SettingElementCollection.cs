﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains a collection of <see cref="T:System.Configuration.SettingElement" /> objects. This class cannot be inherited.</summary>
	// Token: 0x02000688 RID: 1672
	public sealed class SettingElementCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingElementCollection" /> class.</summary>
		// Token: 0x06003541 RID: 13633 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the type of the configuration collection.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationElementCollectionType" /> object of the collection.</returns>
		// Token: 0x17000D7E RID: 3454
		// (get) Token: 0x06003542 RID: 13634 RVA: 0x000A5A0C File Offset: 0x000A3C0C
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConfigurationElementCollectionType.BasicMap;
			}
		}

		// Token: 0x17000D7F RID: 3455
		// (get) Token: 0x06003543 RID: 13635 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override string ElementName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a <see cref="T:System.Configuration.SettingElement" /> object to the collection.</summary>
		/// <param name="element">The <see cref="T:System.Configuration.SettingElement" /> object to add to the collection.</param>
		// Token: 0x06003544 RID: 13636 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Add(SettingElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all <see cref="T:System.Configuration.SettingElement" /> objects from the collection.</summary>
		// Token: 0x06003545 RID: 13637 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003546 RID: 13638 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets a <see cref="T:System.Configuration.SettingElement" /> object from the collection. </summary>
		/// <param name="elementKey">A string value representing the <see cref="T:System.Configuration.SettingElement" /> object in the collection.</param>
		/// <returns>A <see cref="T:System.Configuration.SettingElement" /> object.</returns>
		// Token: 0x06003547 RID: 13639 RVA: 0x00043C3C File Offset: 0x00041E3C
		public SettingElement Get(string elementKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06003548 RID: 13640 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes a <see cref="T:System.Configuration.SettingElement" /> object from the collection.</summary>
		/// <param name="element">A <see cref="T:System.Configuration.SettingElement" /> object.</param>
		// Token: 0x06003549 RID: 13641 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(SettingElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
