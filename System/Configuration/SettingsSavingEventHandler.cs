﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Configuration.ApplicationSettingsBase.SettingsSaving" /> event. </summary>
	/// <param name="sender">The source of the event, typically a data container or data-bound collection.</param>
	/// <param name="e">A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the event data.</param>
	// Token: 0x02000684 RID: 1668
	public sealed class SettingsSavingEventHandler : MulticastDelegate
	{
		// Token: 0x06003537 RID: 13623 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsSavingEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003538 RID: 13624 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, CancelEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003539 RID: 13625 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, CancelEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600353A RID: 13626 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
