﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Specifies the default value for an application settings property.</summary>
	// Token: 0x0200068D RID: 1677
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class DefaultSettingValueAttribute : Attribute
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.DefaultSettingValueAttribute" /> class.</summary>
		/// <param name="value">A <see cref="T:System.String" /> that represents the default value for the property. </param>
		// Token: 0x06003566 RID: 13670 RVA: 0x0000232D File Offset: 0x0000052D
		public DefaultSettingValueAttribute(string value)
		{
		}

		/// <summary>Gets the default value for the application settings property.</summary>
		/// <returns>A <see cref="T:System.String" /> that represents the default value for the property.</returns>
		// Token: 0x17000D89 RID: 3465
		// (get) Token: 0x06003567 RID: 13671 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
