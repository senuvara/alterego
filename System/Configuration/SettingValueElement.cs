﻿using System;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains the XML representing the serialized value of the setting. This class cannot be inherited.</summary>
	// Token: 0x0200068A RID: 1674
	public sealed class SettingValueElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingValueElement" /> class. </summary>
		// Token: 0x06003553 RID: 13651 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingValueElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x17000D84 RID: 3460
		// (get) Token: 0x06003554 RID: 13652 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the value of a <see cref="T:System.Configuration.SettingValueElement" /> object by using an <see cref="T:System.Xml.XmlNode" /> object.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlNode" /> object containing the value of a <see cref="T:System.Configuration.SettingElement" />.</returns>
		// Token: 0x17000D85 RID: 3461
		// (get) Token: 0x06003555 RID: 13653 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003556 RID: 13654 RVA: 0x000092E2 File Offset: 0x000074E2
		public XmlNode ValueXml
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x06003557 RID: 13655 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void DeserializeElement(XmlReader reader, bool serializeCollectionKey)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003558 RID: 13656 RVA: 0x000A5A44 File Offset: 0x000A3C44
		protected override bool IsModified()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06003559 RID: 13657 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void Reset(ConfigurationElement parentElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600355A RID: 13658 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void ResetModified()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600355B RID: 13659 RVA: 0x000A5A60 File Offset: 0x000A3C60
		protected override bool SerializeToXmlElement(XmlWriter writer, string elementName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x0600355C RID: 13660 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void Unmerge(ConfigurationElement sourceElement, ConfigurationElement parentElement, ConfigurationSaveMode saveMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
