﻿using System;
using System.Collections.Specialized;
using System.Security.Permissions;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides persistence for application settings classes.</summary>
	// Token: 0x02000695 RID: 1685
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class LocalFileSettingsProvider : SettingsProvider, IApplicationSettingsProvider
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.LocalFileSettingsProvider" /> class.</summary>
		// Token: 0x06003580 RID: 13696 RVA: 0x000092E2 File Offset: 0x000074E2
		public LocalFileSettingsProvider()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the currently running application.</summary>
		/// <returns>A string that contains the application's display name.</returns>
		// Token: 0x17000D92 RID: 3474
		// (get) Token: 0x06003581 RID: 13697 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003582 RID: 13698 RVA: 0x000092E2 File Offset: 0x000074E2
		public override string ApplicationName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Returns the value of the named settings property for the previous version of the same application. </summary>
		/// <param name="context">A <see cref="T:System.Configuration.SettingsContext" /> that describes where the application settings property is used.</param>
		/// <param name="property">The <see cref="T:System.Configuration.SettingsProperty" /> whose value is to be returned.</param>
		/// <returns>A <see cref="T:System.Configuration.SettingsPropertyValue" /> representing the application setting if found; otherwise, <see langword="null" />.</returns>
		// Token: 0x06003583 RID: 13699 RVA: 0x00043C3C File Offset: 0x00041E3C
		[FileIOPermission(SecurityAction.Assert, AllFiles = (FileIOPermissionAccess.Read | FileIOPermissionAccess.PathDiscovery))]
		[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
		[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
		public SettingsPropertyValue GetPreviousVersion(SettingsContext context, SettingsProperty property)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the collection of setting property values for the specified application instance and settings property group.</summary>
		/// <param name="context">A <see cref="T:System.Configuration.SettingsContext" /> describing the current application usage.</param>
		/// <param name="properties">A <see cref="T:System.Configuration.SettingsPropertyCollection" /> containing the settings property group whose values are to be retrieved.</param>
		/// <returns>A <see cref="T:System.Configuration.SettingsPropertyValueCollection" /> containing the values for the specified settings property group.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A user-scoped setting was encountered but the current configuration only supports application-scoped settings.</exception>
		// Token: 0x06003584 RID: 13700 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection properties)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Initializes the provider.</summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="values">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
		// Token: 0x06003585 RID: 13701 RVA: 0x000092E2 File Offset: 0x000074E2
		public override void Initialize(string name, NameValueCollection values)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets all application settings properties associated with the specified application to their default values.</summary>
		/// <param name="context">A <see cref="T:System.Configuration.SettingsContext" /> describing the current application usage.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A user-scoped setting was encountered but the current configuration only supports application-scoped settings.</exception>
		// Token: 0x06003586 RID: 13702 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Reset(SettingsContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the values of the specified group of property settings.</summary>
		/// <param name="context">A <see cref="T:System.Configuration.SettingsContext" /> describing the current application usage.</param>
		/// <param name="values">A <see cref="T:System.Configuration.SettingsPropertyValueCollection" /> representing the group of property settings to set.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A user-scoped setting was encountered but the current configuration only supports application-scoped settings.-or-There was a general failure saving the settings to the configuration file.</exception>
		// Token: 0x06003587 RID: 13703 RVA: 0x000092E2 File Offset: 0x000074E2
		public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection values)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Attempts to migrate previous user-scoped settings from a previous version of the same application.</summary>
		/// <param name="context">A <see cref="T:System.Configuration.SettingsContext" /> describing the current application usage. </param>
		/// <param name="properties">A <see cref="T:System.Configuration.SettingsPropertyCollection" /> containing the settings property group whose values are to be retrieved. </param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A user-scoped setting was encountered but the current configuration only supports application-scoped settings.-or-The previous version of the configuration file could not be accessed.</exception>
		// Token: 0x06003588 RID: 13704 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Upgrade(SettingsContext context, SettingsPropertyCollection properties)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
