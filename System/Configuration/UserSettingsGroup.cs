﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a grouping of related user settings sections within a configuration file. This class cannot be inherited.</summary>
	// Token: 0x020006AA RID: 1706
	public sealed class UserSettingsGroup : ConfigurationSectionGroup
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.UserSettingsGroup" /> class.</summary>
		// Token: 0x060035BE RID: 13758 RVA: 0x000092E2 File Offset: 0x000074E2
		public UserSettingsGroup()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
