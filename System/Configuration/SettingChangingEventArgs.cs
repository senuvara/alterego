﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides data for the <see cref="E:System.Configuration.ApplicationSettingsBase.SettingChanging" /> event.</summary>
	// Token: 0x02000681 RID: 1665
	public class SettingChangingEventArgs : CancelEventArgs
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.SettingChangingEventArgs" /> class.</summary>
		/// <param name="settingName">A <see cref="T:System.String" /> containing the name of the application setting.</param>
		/// <param name="settingClass">A <see cref="T:System.String" /> containing a category description of the setting. Often this parameter is set to the application settings group name.</param>
		/// <param name="settingKey">A <see cref="T:System.String" /> containing the application settings key.</param>
		/// <param name="newValue">An <see cref="T:System.Object" /> that contains the new value to be assigned to the application settings property.</param>
		/// <param name="cancel">
		///       <see langword="true" /> to cancel the event; otherwise, <see langword="false" />. </param>
		// Token: 0x0600352C RID: 13612 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingChangingEventArgs(string settingName, string settingClass, string settingKey, object newValue, bool cancel)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the new value being assigned to the application settings property.</summary>
		/// <returns>An <see cref="T:System.Object" /> that contains the new value to be assigned to the application settings property.</returns>
		// Token: 0x17000D77 RID: 3447
		// (get) Token: 0x0600352D RID: 13613 RVA: 0x00043C3C File Offset: 0x00041E3C
		public object NewValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the application settings property category.</summary>
		/// <returns>A <see cref="T:System.String" /> containing a category description of the setting. Typically, this parameter is set to the application settings group name.</returns>
		// Token: 0x17000D78 RID: 3448
		// (get) Token: 0x0600352E RID: 13614 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string SettingClass
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the application settings key associated with the property.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the application settings key.</returns>
		// Token: 0x17000D79 RID: 3449
		// (get) Token: 0x0600352F RID: 13615 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string SettingKey
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the application setting associated with the application settings property.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the name of the application setting. </returns>
		// Token: 0x17000D7A RID: 3450
		// (get) Token: 0x06003530 RID: 13616 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string SettingName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
