﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Specifies the serialization mechanism that the settings provider should use. This class cannot be inherited.</summary>
	// Token: 0x020006A4 RID: 1700
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
	public sealed class SettingsSerializeAsAttribute : Attribute
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.SettingsSerializeAsAttribute" /> class.</summary>
		/// <param name="serializeAs">A <see cref="T:System.Configuration.SettingsSerializeAs" /> enumerated value that specifies the serialization scheme.</param>
		// Token: 0x060035B2 RID: 13746 RVA: 0x0000232D File Offset: 0x0000052D
		public SettingsSerializeAsAttribute(SettingsSerializeAs serializeAs)
		{
		}

		/// <summary>Gets the <see cref="T:System.Configuration.SettingsSerializeAs" /> enumeration value that specifies the serialization scheme.</summary>
		/// <returns>A <see cref="T:System.Configuration.SettingsSerializeAs" /> enumerated value that specifies the serialization scheme.</returns>
		// Token: 0x17000D9F RID: 3487
		// (get) Token: 0x060035B3 RID: 13747 RVA: 0x000A5B5C File Offset: 0x000A3D5C
		public SettingsSerializeAs SerializeAs
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SettingsSerializeAs.String;
			}
		}
	}
}
