﻿using System;

namespace System.Configuration
{
	/// <summary>Defines standard functionality for controls or libraries that store and retrieve application settings.</summary>
	// Token: 0x02000692 RID: 1682
	public interface IPersistComponentSettings
	{
		/// <summary>Gets or sets a value indicating whether the control should automatically persist its application settings properties.</summary>
		/// <returns>
		///     <see langword="true" /> if the control should automatically persist its state; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000D8E RID: 3470
		// (get) Token: 0x06003574 RID: 13684
		// (set) Token: 0x06003575 RID: 13685
		bool SaveSettings { get; set; }

		/// <summary>Gets or sets the value of the application settings key for the current instance of the control.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the settings key for the current instance of the control.</returns>
		// Token: 0x17000D8F RID: 3471
		// (get) Token: 0x06003576 RID: 13686
		// (set) Token: 0x06003577 RID: 13687
		string SettingsKey { get; set; }

		/// <summary>Reads the control's application settings into their corresponding properties and updates the control's state.</summary>
		// Token: 0x06003578 RID: 13688
		void LoadComponentSettings();

		/// <summary>Resets the control's application settings properties to their default values.</summary>
		// Token: 0x06003579 RID: 13689
		void ResetComponentSettings();

		/// <summary>Persists the control's application settings properties.</summary>
		// Token: 0x0600357A RID: 13690
		void SaveComponentSettings();
	}
}
