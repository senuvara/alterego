﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides a string that describes an individual configuration property. This class cannot be inherited.</summary>
	// Token: 0x0200069B RID: 1691
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class SettingsDescriptionAttribute : Attribute
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.SettingsDescriptionAttribute" /> class.</summary>
		/// <param name="description">The <see cref="T:System.String" /> used as descriptive text.</param>
		// Token: 0x0600359B RID: 13723 RVA: 0x0000232D File Offset: 0x0000052D
		public SettingsDescriptionAttribute(string description)
		{
		}

		/// <summary>Gets the descriptive text for the associated configuration property.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the descriptive text for the associated configuration property.</returns>
		// Token: 0x17000D9A RID: 3482
		// (get) Token: 0x0600359C RID: 13724 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string Description
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
