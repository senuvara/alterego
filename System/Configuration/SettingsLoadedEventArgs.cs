﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides data for the <see cref="E:System.Configuration.ApplicationSettingsBase.SettingsLoaded" /> event.</summary>
	// Token: 0x02000683 RID: 1667
	public class SettingsLoadedEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SettingsLoadedEventArgs" /> class. </summary>
		/// <param name="provider">A <see cref="T:System.Configuration.SettingsProvider" /> object from which settings are loaded.</param>
		// Token: 0x06003535 RID: 13621 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsLoadedEventArgs(SettingsProvider provider)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the settings provider used to store configuration settings.</summary>
		/// <returns>A settings provider.</returns>
		// Token: 0x17000D7B RID: 3451
		// (get) Token: 0x06003536 RID: 13622 RVA: 0x00043C3C File Offset: 0x00041E3C
		public SettingsProvider Provider
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
