﻿using System;

namespace System
{
	// Token: 0x020000B0 RID: 176
	[Flags]
	internal enum UriSyntaxFlags
	{
		// Token: 0x04000944 RID: 2372
		None = 0,
		// Token: 0x04000945 RID: 2373
		MustHaveAuthority = 1,
		// Token: 0x04000946 RID: 2374
		OptionalAuthority = 2,
		// Token: 0x04000947 RID: 2375
		MayHaveUserInfo = 4,
		// Token: 0x04000948 RID: 2376
		MayHavePort = 8,
		// Token: 0x04000949 RID: 2377
		MayHavePath = 16,
		// Token: 0x0400094A RID: 2378
		MayHaveQuery = 32,
		// Token: 0x0400094B RID: 2379
		MayHaveFragment = 64,
		// Token: 0x0400094C RID: 2380
		AllowEmptyHost = 128,
		// Token: 0x0400094D RID: 2381
		AllowUncHost = 256,
		// Token: 0x0400094E RID: 2382
		AllowDnsHost = 512,
		// Token: 0x0400094F RID: 2383
		AllowIPv4Host = 1024,
		// Token: 0x04000950 RID: 2384
		AllowIPv6Host = 2048,
		// Token: 0x04000951 RID: 2385
		AllowAnInternetHost = 3584,
		// Token: 0x04000952 RID: 2386
		AllowAnyOtherHost = 4096,
		// Token: 0x04000953 RID: 2387
		FileLikeUri = 8192,
		// Token: 0x04000954 RID: 2388
		MailToLikeUri = 16384,
		// Token: 0x04000955 RID: 2389
		V1_UnknownUri = 65536,
		// Token: 0x04000956 RID: 2390
		SimpleUserSyntax = 131072,
		// Token: 0x04000957 RID: 2391
		BuiltInSyntax = 262144,
		// Token: 0x04000958 RID: 2392
		ParserSchemeOnly = 524288,
		// Token: 0x04000959 RID: 2393
		AllowDOSPath = 1048576,
		// Token: 0x0400095A RID: 2394
		PathIsRooted = 2097152,
		// Token: 0x0400095B RID: 2395
		ConvertPathSlashes = 4194304,
		// Token: 0x0400095C RID: 2396
		CompressPath = 8388608,
		// Token: 0x0400095D RID: 2397
		CanonicalizeAsFilePath = 16777216,
		// Token: 0x0400095E RID: 2398
		UnEscapeDotsAndSlashes = 33554432,
		// Token: 0x0400095F RID: 2399
		AllowIdn = 67108864,
		// Token: 0x04000960 RID: 2400
		AllowIriParsing = 268435456
	}
}
