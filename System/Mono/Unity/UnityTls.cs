﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Mono.Unity
{
	// Token: 0x02000006 RID: 6
	internal static class UnityTls
	{
		// Token: 0x06000010 RID: 16
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetUnityTlsInterface();

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000011 RID: 17 RVA: 0x000022E7 File Offset: 0x000004E7
		public static bool IsSupported
		{
			get
			{
				return UnityTls.NativeInterface != null;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000012 RID: 18 RVA: 0x000022F4 File Offset: 0x000004F4
		public static UnityTls.unitytls_interface_struct NativeInterface
		{
			get
			{
				if (UnityTls.marshalledInterface == null)
				{
					IntPtr unityTlsInterface = UnityTls.GetUnityTlsInterface();
					if (unityTlsInterface == IntPtr.Zero)
					{
						return null;
					}
					UnityTls.marshalledInterface = Marshal.PtrToStructure<UnityTls.unitytls_interface_struct>(unityTlsInterface);
				}
				return UnityTls.marshalledInterface;
			}
		}

		// Token: 0x06000013 RID: 19 RVA: 0x0000232D File Offset: 0x0000052D
		// Note: this type is marked as 'beforefieldinit'.
		static UnityTls()
		{
		}

		// Token: 0x0400069D RID: 1693
		private static UnityTls.unitytls_interface_struct marshalledInterface;

		// Token: 0x02000007 RID: 7
		public enum unitytls_error_code : uint
		{
			// Token: 0x0400069F RID: 1695
			UNITYTLS_SUCCESS,
			// Token: 0x040006A0 RID: 1696
			UNITYTLS_INVALID_ARGUMENT,
			// Token: 0x040006A1 RID: 1697
			UNITYTLS_INVALID_FORMAT,
			// Token: 0x040006A2 RID: 1698
			UNITYTLS_INVALID_PASSWORD,
			// Token: 0x040006A3 RID: 1699
			UNITYTLS_INVALID_STATE,
			// Token: 0x040006A4 RID: 1700
			UNITYTLS_BUFFER_OVERFLOW,
			// Token: 0x040006A5 RID: 1701
			UNITYTLS_OUT_OF_MEMORY,
			// Token: 0x040006A6 RID: 1702
			UNITYTLS_INTERNAL_ERROR,
			// Token: 0x040006A7 RID: 1703
			UNITYTLS_NOT_SUPPORTED,
			// Token: 0x040006A8 RID: 1704
			UNITYTLS_ENTROPY_SOURCE_FAILED,
			// Token: 0x040006A9 RID: 1705
			UNITYTLS_STREAM_CLOSED,
			// Token: 0x040006AA RID: 1706
			UNITYTLS_USER_CUSTOM_ERROR_START = 1048576U,
			// Token: 0x040006AB RID: 1707
			UNITYTLS_USER_WOULD_BLOCK,
			// Token: 0x040006AC RID: 1708
			UNITYTLS_USER_READ_FAILED,
			// Token: 0x040006AD RID: 1709
			UNITYTLS_USER_WRITE_FAILED,
			// Token: 0x040006AE RID: 1710
			UNITYTLS_USER_UNKNOWN_ERROR,
			// Token: 0x040006AF RID: 1711
			UNITYTLS_USER_CUSTOM_ERROR_END = 2097152U
		}

		// Token: 0x02000008 RID: 8
		public struct unitytls_errorstate
		{
			// Token: 0x040006B0 RID: 1712
			private uint magic;

			// Token: 0x040006B1 RID: 1713
			public UnityTls.unitytls_error_code code;

			// Token: 0x040006B2 RID: 1714
			private ulong reserved;
		}

		// Token: 0x02000009 RID: 9
		public struct unitytls_key
		{
		}

		// Token: 0x0200000A RID: 10
		public struct unitytls_key_ref
		{
			// Token: 0x040006B3 RID: 1715
			public ulong handle;
		}

		// Token: 0x0200000B RID: 11
		public struct unitytls_x509
		{
		}

		// Token: 0x0200000C RID: 12
		public struct unitytls_x509_ref
		{
			// Token: 0x040006B4 RID: 1716
			public ulong handle;
		}

		// Token: 0x0200000D RID: 13
		public struct unitytls_x509list
		{
		}

		// Token: 0x0200000E RID: 14
		public struct unitytls_x509list_ref
		{
			// Token: 0x040006B5 RID: 1717
			public ulong handle;
		}

		// Token: 0x0200000F RID: 15
		[Flags]
		public enum unitytls_x509verify_result : uint
		{
			// Token: 0x040006B7 RID: 1719
			UNITYTLS_X509VERIFY_SUCCESS = 0U,
			// Token: 0x040006B8 RID: 1720
			UNITYTLS_X509VERIFY_NOT_DONE = 2147483648U,
			// Token: 0x040006B9 RID: 1721
			UNITYTLS_X509VERIFY_FATAL_ERROR = 4294967295U,
			// Token: 0x040006BA RID: 1722
			UNITYTLS_X509VERIFY_FLAG_EXPIRED = 1U,
			// Token: 0x040006BB RID: 1723
			UNITYTLS_X509VERIFY_FLAG_REVOKED = 2U,
			// Token: 0x040006BC RID: 1724
			UNITYTLS_X509VERIFY_FLAG_CN_MISMATCH = 4U,
			// Token: 0x040006BD RID: 1725
			UNITYTLS_X509VERIFY_FLAG_NOT_TRUSTED = 8U,
			// Token: 0x040006BE RID: 1726
			UNITYTLS_X509VERIFY_FLAG_USER_ERROR1 = 65536U,
			// Token: 0x040006BF RID: 1727
			UNITYTLS_X509VERIFY_FLAG_USER_ERROR2 = 131072U,
			// Token: 0x040006C0 RID: 1728
			UNITYTLS_X509VERIFY_FLAG_USER_ERROR3 = 262144U,
			// Token: 0x040006C1 RID: 1729
			UNITYTLS_X509VERIFY_FLAG_USER_ERROR4 = 524288U,
			// Token: 0x040006C2 RID: 1730
			UNITYTLS_X509VERIFY_FLAG_USER_ERROR5 = 1048576U,
			// Token: 0x040006C3 RID: 1731
			UNITYTLS_X509VERIFY_FLAG_USER_ERROR6 = 2097152U,
			// Token: 0x040006C4 RID: 1732
			UNITYTLS_X509VERIFY_FLAG_USER_ERROR7 = 4194304U,
			// Token: 0x040006C5 RID: 1733
			UNITYTLS_X509VERIFY_FLAG_USER_ERROR8 = 8388608U,
			// Token: 0x040006C6 RID: 1734
			UNITYTLS_X509VERIFY_FLAG_UNKNOWN_ERROR = 134217728U
		}

		// Token: 0x02000010 RID: 16
		// (Invoke) Token: 0x06000015 RID: 21
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public unsafe delegate UnityTls.unitytls_x509verify_result unitytls_x509verify_callback(void* userData, UnityTls.unitytls_x509_ref cert, UnityTls.unitytls_x509verify_result result, UnityTls.unitytls_errorstate* errorState);

		// Token: 0x02000011 RID: 17
		public struct unitytls_tlsctx
		{
		}

		// Token: 0x02000012 RID: 18
		public struct unitytls_tlsctx_ref
		{
			// Token: 0x040006C7 RID: 1735
			public ulong handle;
		}

		// Token: 0x02000013 RID: 19
		public struct unitytls_x509name
		{
		}

		// Token: 0x02000014 RID: 20
		public enum unitytls_ciphersuite : uint
		{
			// Token: 0x040006C9 RID: 1737
			UNITYTLS_CIPHERSUITE_INVALID = 16777215U
		}

		// Token: 0x02000015 RID: 21
		public enum unitytls_protocol : uint
		{
			// Token: 0x040006CB RID: 1739
			UNITYTLS_PROTOCOL_TLS_1_0,
			// Token: 0x040006CC RID: 1740
			UNITYTLS_PROTOCOL_TLS_1_1,
			// Token: 0x040006CD RID: 1741
			UNITYTLS_PROTOCOL_TLS_1_2,
			// Token: 0x040006CE RID: 1742
			UNITYTLS_PROTOCOL_INVALID
		}

		// Token: 0x02000016 RID: 22
		public struct unitytls_tlsctx_protocolrange
		{
			// Token: 0x040006CF RID: 1743
			public UnityTls.unitytls_protocol min;

			// Token: 0x040006D0 RID: 1744
			public UnityTls.unitytls_protocol max;
		}

		// Token: 0x02000017 RID: 23
		// (Invoke) Token: 0x06000019 RID: 25
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public unsafe delegate IntPtr unitytls_tlsctx_write_callback(void* userData, byte* data, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState);

		// Token: 0x02000018 RID: 24
		// (Invoke) Token: 0x0600001D RID: 29
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public unsafe delegate IntPtr unitytls_tlsctx_read_callback(void* userData, byte* buffer, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState);

		// Token: 0x02000019 RID: 25
		// (Invoke) Token: 0x06000021 RID: 33
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public unsafe delegate void unitytls_tlsctx_trace_callback(void* userData, UnityTls.unitytls_tlsctx* ctx, byte* traceMessage, IntPtr traceMessageLen);

		// Token: 0x0200001A RID: 26
		// (Invoke) Token: 0x06000025 RID: 37
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public unsafe delegate void unitytls_tlsctx_certificate_callback(void* userData, UnityTls.unitytls_tlsctx* ctx, byte* cn, IntPtr cnLen, UnityTls.unitytls_x509name* caList, IntPtr caListLen, UnityTls.unitytls_x509list_ref* chain, UnityTls.unitytls_key_ref* key, UnityTls.unitytls_errorstate* errorState);

		// Token: 0x0200001B RID: 27
		// (Invoke) Token: 0x06000029 RID: 41
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public unsafe delegate UnityTls.unitytls_x509verify_result unitytls_tlsctx_x509verify_callback(void* userData, UnityTls.unitytls_x509list_ref chain, UnityTls.unitytls_errorstate* errorState);

		// Token: 0x0200001C RID: 28
		public struct unitytls_tlsctx_callbacks
		{
			// Token: 0x040006D1 RID: 1745
			public UnityTls.unitytls_tlsctx_read_callback read;

			// Token: 0x040006D2 RID: 1746
			public UnityTls.unitytls_tlsctx_write_callback write;

			// Token: 0x040006D3 RID: 1747
			public unsafe void* data;
		}

		// Token: 0x0200001D RID: 29
		[StructLayout(LayoutKind.Sequential)]
		public class unitytls_interface_struct
		{
			// Token: 0x0600002C RID: 44 RVA: 0x0000232F File Offset: 0x0000052F
			public unitytls_interface_struct()
			{
			}

			// Token: 0x040006D4 RID: 1748
			public readonly ulong UNITYTLS_INVALID_HANDLE;

			// Token: 0x040006D5 RID: 1749
			public readonly UnityTls.unitytls_tlsctx_protocolrange UNITYTLS_TLSCTX_PROTOCOLRANGE_DEFAULT;

			// Token: 0x040006D6 RID: 1750
			public UnityTls.unitytls_interface_struct.unitytls_errorstate_create_t unitytls_errorstate_create;

			// Token: 0x040006D7 RID: 1751
			public UnityTls.unitytls_interface_struct.unitytls_errorstate_raise_error_t unitytls_errorstate_raise_error;

			// Token: 0x040006D8 RID: 1752
			public UnityTls.unitytls_interface_struct.unitytls_key_get_ref_t unitytls_key_get_ref;

			// Token: 0x040006D9 RID: 1753
			public UnityTls.unitytls_interface_struct.unitytls_key_parse_der_t unitytls_key_parse_der;

			// Token: 0x040006DA RID: 1754
			public UnityTls.unitytls_interface_struct.unitytls_key_parse_pem_t unitytls_key_parse_pem;

			// Token: 0x040006DB RID: 1755
			public UnityTls.unitytls_interface_struct.unitytls_key_free_t unitytls_key_free;

			// Token: 0x040006DC RID: 1756
			public UnityTls.unitytls_interface_struct.unitytls_x509_export_der_t unitytls_x509_export_der;

			// Token: 0x040006DD RID: 1757
			public UnityTls.unitytls_interface_struct.unitytls_x509list_get_ref_t unitytls_x509list_get_ref;

			// Token: 0x040006DE RID: 1758
			public UnityTls.unitytls_interface_struct.unitytls_x509list_get_x509_t unitytls_x509list_get_x509;

			// Token: 0x040006DF RID: 1759
			public UnityTls.unitytls_interface_struct.unitytls_x509list_create_t unitytls_x509list_create;

			// Token: 0x040006E0 RID: 1760
			public UnityTls.unitytls_interface_struct.unitytls_x509list_append_t unitytls_x509list_append;

			// Token: 0x040006E1 RID: 1761
			public UnityTls.unitytls_interface_struct.unitytls_x509list_append_der_t unitytls_x509list_append_der;

			// Token: 0x040006E2 RID: 1762
			public UnityTls.unitytls_interface_struct.unitytls_x509list_append_der_t unitytls_x509list_append_pem;

			// Token: 0x040006E3 RID: 1763
			public UnityTls.unitytls_interface_struct.unitytls_x509list_free_t unitytls_x509list_free;

			// Token: 0x040006E4 RID: 1764
			public UnityTls.unitytls_interface_struct.unitytls_x509verify_default_ca_t unitytls_x509verify_default_ca;

			// Token: 0x040006E5 RID: 1765
			public UnityTls.unitytls_interface_struct.unitytls_x509verify_explicit_ca_t unitytls_x509verify_explicit_ca;

			// Token: 0x040006E6 RID: 1766
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_create_server_t unitytls_tlsctx_create_server;

			// Token: 0x040006E7 RID: 1767
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_create_client_t unitytls_tlsctx_create_client;

			// Token: 0x040006E8 RID: 1768
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_server_require_client_authentication_t unitytls_tlsctx_server_require_client_authentication;

			// Token: 0x040006E9 RID: 1769
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_set_certificate_callback_t unitytls_tlsctx_set_certificate_callback;

			// Token: 0x040006EA RID: 1770
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_set_trace_callback_t unitytls_tlsctx_set_trace_callback;

			// Token: 0x040006EB RID: 1771
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_set_x509verify_callback_t unitytls_tlsctx_set_x509verify_callback;

			// Token: 0x040006EC RID: 1772
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_set_supported_ciphersuites_t unitytls_tlsctx_set_supported_ciphersuites;

			// Token: 0x040006ED RID: 1773
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_get_ciphersuite_t unitytls_tlsctx_get_ciphersuite;

			// Token: 0x040006EE RID: 1774
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_get_protocol_t unitytls_tlsctx_get_protocol;

			// Token: 0x040006EF RID: 1775
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_process_handshake_t unitytls_tlsctx_process_handshake;

			// Token: 0x040006F0 RID: 1776
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_read_t unitytls_tlsctx_read;

			// Token: 0x040006F1 RID: 1777
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_write_t unitytls_tlsctx_write;

			// Token: 0x040006F2 RID: 1778
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_notify_close_t unitytls_tlsctx_notify_close;

			// Token: 0x040006F3 RID: 1779
			public UnityTls.unitytls_interface_struct.unitytls_tlsctx_free_t unitytls_tlsctx_free;

			// Token: 0x040006F4 RID: 1780
			public UnityTls.unitytls_interface_struct.unitytls_random_generate_bytes_t unitytls_random_generate_bytes;

			// Token: 0x0200001E RID: 30
			// (Invoke) Token: 0x0600002E RID: 46
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public delegate UnityTls.unitytls_errorstate unitytls_errorstate_create_t();

			// Token: 0x0200001F RID: 31
			// (Invoke) Token: 0x06000032 RID: 50
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_errorstate_raise_error_t(UnityTls.unitytls_errorstate* errorState, UnityTls.unitytls_error_code errorCode);

			// Token: 0x02000020 RID: 32
			// (Invoke) Token: 0x06000036 RID: 54
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_key_ref unitytls_key_get_ref_t(UnityTls.unitytls_key* key, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000021 RID: 33
			// (Invoke) Token: 0x0600003A RID: 58
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_key* unitytls_key_parse_der_t(byte* buffer, IntPtr bufferLen, byte* password, IntPtr passwordLen, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000022 RID: 34
			// (Invoke) Token: 0x0600003E RID: 62
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_key* unitytls_key_parse_pem_t(byte* buffer, IntPtr bufferLen, byte* password, IntPtr passwordLen, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000023 RID: 35
			// (Invoke) Token: 0x06000042 RID: 66
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_key_free_t(UnityTls.unitytls_key* key);

			// Token: 0x02000024 RID: 36
			// (Invoke) Token: 0x06000046 RID: 70
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate IntPtr unitytls_x509_export_der_t(UnityTls.unitytls_x509_ref cert, byte* buffer, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000025 RID: 37
			// (Invoke) Token: 0x0600004A RID: 74
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_x509list_ref unitytls_x509list_get_ref_t(UnityTls.unitytls_x509list* list, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000026 RID: 38
			// (Invoke) Token: 0x0600004E RID: 78
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_x509_ref unitytls_x509list_get_x509_t(UnityTls.unitytls_x509list_ref list, IntPtr index, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000027 RID: 39
			// (Invoke) Token: 0x06000052 RID: 82
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_x509list* unitytls_x509list_create_t(UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000028 RID: 40
			// (Invoke) Token: 0x06000056 RID: 86
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_x509list_append_t(UnityTls.unitytls_x509list* list, UnityTls.unitytls_x509_ref cert, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000029 RID: 41
			// (Invoke) Token: 0x0600005A RID: 90
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_x509list_append_der_t(UnityTls.unitytls_x509list* list, byte* buffer, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x0200002A RID: 42
			// (Invoke) Token: 0x0600005E RID: 94
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_x509list_append_pem_t(UnityTls.unitytls_x509list* list, byte* buffer, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x0200002B RID: 43
			// (Invoke) Token: 0x06000062 RID: 98
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_x509list_free_t(UnityTls.unitytls_x509list* list);

			// Token: 0x0200002C RID: 44
			// (Invoke) Token: 0x06000066 RID: 102
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_x509verify_result unitytls_x509verify_default_ca_t(UnityTls.unitytls_x509list_ref chain, byte* cn, IntPtr cnLen, UnityTls.unitytls_x509verify_callback cb, void* userData, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x0200002D RID: 45
			// (Invoke) Token: 0x0600006A RID: 106
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_x509verify_result unitytls_x509verify_explicit_ca_t(UnityTls.unitytls_x509list_ref chain, UnityTls.unitytls_x509list_ref trustCA, byte* cn, IntPtr cnLen, UnityTls.unitytls_x509verify_callback cb, void* userData, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x0200002E RID: 46
			// (Invoke) Token: 0x0600006E RID: 110
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_tlsctx* unitytls_tlsctx_create_server_t(UnityTls.unitytls_tlsctx_protocolrange supportedProtocols, UnityTls.unitytls_tlsctx_callbacks callbacks, ulong certChain, ulong leafCertificateKey, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x0200002F RID: 47
			// (Invoke) Token: 0x06000072 RID: 114
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_tlsctx* unitytls_tlsctx_create_client_t(UnityTls.unitytls_tlsctx_protocolrange supportedProtocols, UnityTls.unitytls_tlsctx_callbacks callbacks, byte* cn, IntPtr cnLen, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000030 RID: 48
			// (Invoke) Token: 0x06000076 RID: 118
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_tlsctx_server_require_client_authentication_t(UnityTls.unitytls_tlsctx* ctx, UnityTls.unitytls_x509list_ref clientAuthCAList, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000031 RID: 49
			// (Invoke) Token: 0x0600007A RID: 122
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_tlsctx_set_certificate_callback_t(UnityTls.unitytls_tlsctx* ctx, UnityTls.unitytls_tlsctx_certificate_callback cb, void* userData, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000032 RID: 50
			// (Invoke) Token: 0x0600007E RID: 126
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_tlsctx_set_trace_callback_t(UnityTls.unitytls_tlsctx* ctx, UnityTls.unitytls_tlsctx_trace_callback cb, void* userData, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000033 RID: 51
			// (Invoke) Token: 0x06000082 RID: 130
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_tlsctx_set_x509verify_callback_t(UnityTls.unitytls_tlsctx* ctx, UnityTls.unitytls_tlsctx_x509verify_callback cb, void* userData, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000034 RID: 52
			// (Invoke) Token: 0x06000086 RID: 134
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_tlsctx_set_supported_ciphersuites_t(UnityTls.unitytls_tlsctx* ctx, UnityTls.unitytls_ciphersuite* supportedCiphersuites, IntPtr supportedCiphersuitesLen, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000035 RID: 53
			// (Invoke) Token: 0x0600008A RID: 138
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_ciphersuite unitytls_tlsctx_get_ciphersuite_t(UnityTls.unitytls_tlsctx* ctx, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000036 RID: 54
			// (Invoke) Token: 0x0600008E RID: 142
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_protocol unitytls_tlsctx_get_protocol_t(UnityTls.unitytls_tlsctx* ctx, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000037 RID: 55
			// (Invoke) Token: 0x06000092 RID: 146
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate UnityTls.unitytls_x509verify_result unitytls_tlsctx_process_handshake_t(UnityTls.unitytls_tlsctx* ctx, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000038 RID: 56
			// (Invoke) Token: 0x06000096 RID: 150
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate IntPtr unitytls_tlsctx_read_t(UnityTls.unitytls_tlsctx* ctx, byte* buffer, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x02000039 RID: 57
			// (Invoke) Token: 0x0600009A RID: 154
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate IntPtr unitytls_tlsctx_write_t(UnityTls.unitytls_tlsctx* ctx, byte* data, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x0200003A RID: 58
			// (Invoke) Token: 0x0600009E RID: 158
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_tlsctx_notify_close_t(UnityTls.unitytls_tlsctx* ctx, UnityTls.unitytls_errorstate* errorState);

			// Token: 0x0200003B RID: 59
			// (Invoke) Token: 0x060000A2 RID: 162
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_tlsctx_free_t(UnityTls.unitytls_tlsctx* ctx);

			// Token: 0x0200003C RID: 60
			// (Invoke) Token: 0x060000A6 RID: 166
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			public unsafe delegate void unitytls_random_generate_bytes_t(byte* buffer, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState);
		}
	}
}
