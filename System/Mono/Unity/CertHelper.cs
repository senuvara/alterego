﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Mono.Unity
{
	// Token: 0x02000004 RID: 4
	internal static class CertHelper
	{
		// Token: 0x0600000B RID: 11 RVA: 0x000020B8 File Offset: 0x000002B8
		public unsafe static void AddCertificatesToNativeChain(UnityTls.unitytls_x509list* nativeCertificateChain, X509CertificateCollection certificates, UnityTls.unitytls_errorstate* errorState)
		{
			foreach (X509Certificate certificate in certificates)
			{
				CertHelper.AddCertificateToNativeChain(nativeCertificateChain, certificate, errorState);
			}
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002108 File Offset: 0x00000308
		public unsafe static void AddCertificateToNativeChain(UnityTls.unitytls_x509list* nativeCertificateChain, X509Certificate certificate, UnityTls.unitytls_errorstate* errorState)
		{
			byte[] rawCertData = certificate.GetRawCertData();
			byte[] array;
			byte* buffer;
			if ((array = rawCertData) == null || array.Length == 0)
			{
				buffer = null;
			}
			else
			{
				buffer = &array[0];
			}
			UnityTls.NativeInterface.unitytls_x509list_append_der(nativeCertificateChain, buffer, (IntPtr)rawCertData.Length, errorState);
			array = null;
			X509Certificate2Impl x509Certificate2Impl = certificate.Impl as X509Certificate2Impl;
			if (x509Certificate2Impl != null)
			{
				X509CertificateImplCollection intermediateCertificates = x509Certificate2Impl.IntermediateCertificates;
				if (intermediateCertificates != null && intermediateCertificates.Count > 0)
				{
					for (int i = 0; i < intermediateCertificates.Count; i++)
					{
						CertHelper.AddCertificateToNativeChain(nativeCertificateChain, new X509Certificate(intermediateCertificates[i]), errorState);
					}
				}
			}
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000021A0 File Offset: 0x000003A0
		public unsafe static X509CertificateCollection NativeChainToManagedCollection(UnityTls.unitytls_x509list_ref nativeCertificateChain, UnityTls.unitytls_errorstate* errorState)
		{
			X509CertificateCollection x509CertificateCollection = new X509CertificateCollection();
			UnityTls.unitytls_x509_ref unitytls_x509_ref = UnityTls.NativeInterface.unitytls_x509list_get_x509(nativeCertificateChain, (IntPtr)0, errorState);
			int num = 0;
			while (unitytls_x509_ref.handle != UnityTls.NativeInterface.UNITYTLS_INVALID_HANDLE)
			{
				IntPtr intPtr = UnityTls.NativeInterface.unitytls_x509_export_der(unitytls_x509_ref, null, (IntPtr)0, errorState);
				byte[] array = new byte[(int)intPtr];
				byte[] array2;
				byte* buffer;
				if ((array2 = array) == null || array2.Length == 0)
				{
					buffer = null;
				}
				else
				{
					buffer = &array2[0];
				}
				UnityTls.NativeInterface.unitytls_x509_export_der(unitytls_x509_ref, buffer, intPtr, errorState);
				array2 = null;
				x509CertificateCollection.Add(new X509Certificate(array));
				unitytls_x509_ref = UnityTls.NativeInterface.unitytls_x509list_get_x509(nativeCertificateChain, (IntPtr)num, errorState);
				num++;
			}
			return x509CertificateCollection;
		}
	}
}
