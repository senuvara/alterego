﻿using System;
using Mono.Security.Interface;

namespace Mono.Unity
{
	// Token: 0x02000005 RID: 5
	internal static class Debug
	{
		// Token: 0x0600000E RID: 14 RVA: 0x00002270 File Offset: 0x00000470
		public static void CheckAndThrow(UnityTls.unitytls_errorstate errorState, string context, AlertDescription defaultAlert = AlertDescription.InternalError)
		{
			if (errorState.code == UnityTls.unitytls_error_code.UNITYTLS_SUCCESS)
			{
				return;
			}
			string message = string.Format("{0} - error code: {1}", context, errorState.code);
			throw new TlsException(defaultAlert, message);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000022A4 File Offset: 0x000004A4
		public static void CheckAndThrow(UnityTls.unitytls_errorstate errorState, UnityTls.unitytls_x509verify_result verifyResult, string context, AlertDescription defaultAlert = AlertDescription.InternalError)
		{
			if (verifyResult == UnityTls.unitytls_x509verify_result.UNITYTLS_X509VERIFY_SUCCESS)
			{
				Debug.CheckAndThrow(errorState, context, defaultAlert);
				return;
			}
			AlertDescription description = UnityTlsConversions.VerifyResultToAlertDescription(verifyResult, defaultAlert);
			string message = string.Format("{0} - error code: {1}, verify result: {2}", context, errorState.code, verifyResult);
			throw new TlsException(description, message);
		}
	}
}
