﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Mono.Net.Security;
using Mono.Security.Cryptography;
using Mono.Security.Interface;
using Mono.Util;

namespace Mono.Unity
{
	// Token: 0x0200003D RID: 61
	internal class UnityTlsContext : MobileTlsContext
	{
		// Token: 0x060000A9 RID: 169 RVA: 0x00002338 File Offset: 0x00000538
		public unsafe UnityTlsContext(MobileAuthenticatedStream parent, bool serverMode, string targetHost, SslProtocols enabledProtocols, X509Certificate serverCertificate, X509CertificateCollection clientCertificates, bool askForClientCert) : base(parent, serverMode, targetHost, enabledProtocols, serverCertificate, clientCertificates, askForClientCert)
		{
			this.handle = GCHandle.Alloc(this);
			UnityTls.unitytls_errorstate errorState = UnityTls.NativeInterface.unitytls_errorstate_create();
			UnityTls.unitytls_tlsctx_protocolrange supportedProtocols = new UnityTls.unitytls_tlsctx_protocolrange
			{
				min = UnityTlsConversions.GetMinProtocol(enabledProtocols),
				max = UnityTlsConversions.GetMaxProtocol(enabledProtocols)
			};
			UnityTls.unitytls_tlsctx_callbacks callbacks = new UnityTls.unitytls_tlsctx_callbacks
			{
				write = new UnityTls.unitytls_tlsctx_write_callback(UnityTlsContext.WriteCallback),
				read = new UnityTls.unitytls_tlsctx_read_callback(UnityTlsContext.ReadCallback),
				data = (void*)((IntPtr)this.handle)
			};
			if (serverMode)
			{
				UnityTls.unitytls_x509list* list;
				UnityTls.unitytls_key* key;
				UnityTlsContext.ExtractNativeKeyAndChainFromManagedCertificate(serverCertificate, &errorState, out list, out key);
				try
				{
					UnityTls.unitytls_x509list_ref unitytls_x509list_ref = UnityTls.NativeInterface.unitytls_x509list_get_ref(list, &errorState);
					UnityTls.unitytls_key_ref unitytls_key_ref = UnityTls.NativeInterface.unitytls_key_get_ref(key, &errorState);
					Mono.Unity.Debug.CheckAndThrow(errorState, "Failed to parse server key/certificate", AlertDescription.InternalError);
					this.tlsContext = UnityTls.NativeInterface.unitytls_tlsctx_create_server(supportedProtocols, callbacks, unitytls_x509list_ref.handle, unitytls_key_ref.handle, &errorState);
					if (askForClientCert)
					{
						UnityTls.unitytls_x509list* list2 = null;
						try
						{
							list2 = UnityTls.NativeInterface.unitytls_x509list_create(&errorState);
							UnityTls.unitytls_x509list_ref clientAuthCAList = UnityTls.NativeInterface.unitytls_x509list_get_ref(list2, &errorState);
							UnityTls.NativeInterface.unitytls_tlsctx_server_require_client_authentication(this.tlsContext, clientAuthCAList, &errorState);
						}
						finally
						{
							UnityTls.NativeInterface.unitytls_x509list_free(list2);
						}
					}
					goto IL_23A;
				}
				finally
				{
					UnityTls.NativeInterface.unitytls_x509list_free(list);
					UnityTls.NativeInterface.unitytls_key_free(key);
				}
			}
			byte[] bytes = Encoding.UTF8.GetBytes(targetHost);
			byte[] array;
			byte* cn;
			if ((array = bytes) == null || array.Length == 0)
			{
				cn = null;
			}
			else
			{
				cn = &array[0];
			}
			this.tlsContext = UnityTls.NativeInterface.unitytls_tlsctx_create_client(supportedProtocols, callbacks, cn, (IntPtr)bytes.Length, &errorState);
			array = null;
			UnityTls.NativeInterface.unitytls_tlsctx_set_certificate_callback(this.tlsContext, new UnityTls.unitytls_tlsctx_certificate_callback(UnityTlsContext.CertificateCallback), (void*)((IntPtr)this.handle), &errorState);
			IL_23A:
			UnityTls.NativeInterface.unitytls_tlsctx_set_x509verify_callback(this.tlsContext, new UnityTls.unitytls_tlsctx_x509verify_callback(UnityTlsContext.VerifyCallback), (void*)((IntPtr)this.handle), &errorState);
			Mono.Unity.Debug.CheckAndThrow(errorState, "Failed to create UnityTls context", AlertDescription.InternalError);
			this.hasContext = true;
		}

		// Token: 0x060000AA RID: 170 RVA: 0x000025E4 File Offset: 0x000007E4
		private unsafe static void ExtractNativeKeyAndChainFromManagedCertificate(X509Certificate cert, UnityTls.unitytls_errorstate* errorState, out UnityTls.unitytls_x509list* nativeCertChain, out UnityTls.unitytls_key* nativeKey)
		{
			if (cert == null)
			{
				throw new ArgumentNullException("cert");
			}
			X509Certificate2 x509Certificate = cert as X509Certificate2;
			if (x509Certificate == null || x509Certificate.PrivateKey == null)
			{
				throw new ArgumentException("Certificate does not have a private key", "cert");
			}
			nativeCertChain = (IntPtr)((UIntPtr)0);
			nativeKey = (IntPtr)((UIntPtr)0);
			try
			{
				nativeCertChain = UnityTls.NativeInterface.unitytls_x509list_create(errorState);
				CertHelper.AddCertificateToNativeChain(nativeCertChain, cert, errorState);
				byte[] array = PKCS8.PrivateKeyInfo.Encode(x509Certificate.PrivateKey);
				try
				{
					byte[] array2;
					byte* buffer;
					if ((array2 = array) == null || array2.Length == 0)
					{
						buffer = null;
					}
					else
					{
						buffer = &array2[0];
					}
					nativeKey = UnityTls.NativeInterface.unitytls_key_parse_der(buffer, (IntPtr)array.Length, null, (IntPtr)0, errorState);
				}
				finally
				{
					byte[] array2 = null;
				}
			}
			catch
			{
				UnityTls.NativeInterface.unitytls_x509list_free(nativeCertChain);
				UnityTls.NativeInterface.unitytls_key_free(nativeKey);
				throw;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x060000AB RID: 171 RVA: 0x000026D4 File Offset: 0x000008D4
		public override bool HasContext
		{
			get
			{
				return this.hasContext;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x060000AC RID: 172 RVA: 0x000026DC File Offset: 0x000008DC
		public override bool IsAuthenticated
		{
			get
			{
				return this.isAuthenticated;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x060000AD RID: 173 RVA: 0x000026E4 File Offset: 0x000008E4
		public override MonoTlsConnectionInfo ConnectionInfo
		{
			get
			{
				return this.connectioninfo;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x060000AE RID: 174 RVA: 0x000026EC File Offset: 0x000008EC
		internal override bool IsRemoteCertificateAvailable
		{
			get
			{
				return this.remoteCertificate != null;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x060000AF RID: 175 RVA: 0x000026F7 File Offset: 0x000008F7
		internal override X509Certificate LocalClientCertificate
		{
			get
			{
				return this.localClientCertificate;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x060000B0 RID: 176 RVA: 0x000026FF File Offset: 0x000008FF
		public override X509Certificate RemoteCertificate
		{
			get
			{
				return this.remoteCertificate;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x060000B1 RID: 177 RVA: 0x00002707 File Offset: 0x00000907
		public override TlsProtocols NegotiatedProtocol
		{
			get
			{
				return this.ConnectionInfo.ProtocolVersion;
			}
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x0000232D File Offset: 0x0000052D
		public override void Flush()
		{
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x00002714 File Offset: 0x00000914
		[return: TupleElementNames(new string[]
		{
			"ret",
			"wantMore"
		})]
		public unsafe override ValueTuple<int, bool> Read(byte[] buffer, int offset, int count)
		{
			this.lastException = null;
			UnityTls.unitytls_errorstate unitytls_errorstate = UnityTls.NativeInterface.unitytls_errorstate_create();
			int num;
			fixed (byte[] array = buffer)
			{
				byte* ptr;
				if (buffer == null || array.Length == 0)
				{
					ptr = null;
				}
				else
				{
					ptr = &array[0];
				}
				num = (int)UnityTls.NativeInterface.unitytls_tlsctx_read(this.tlsContext, ptr + offset, (IntPtr)count, &unitytls_errorstate);
			}
			if (this.lastException != null)
			{
				throw this.lastException;
			}
			UnityTls.unitytls_error_code code = unitytls_errorstate.code;
			if (code == UnityTls.unitytls_error_code.UNITYTLS_SUCCESS)
			{
				return new ValueTuple<int, bool>(num, num < count);
			}
			if (code == UnityTls.unitytls_error_code.UNITYTLS_STREAM_CLOSED)
			{
				return new ValueTuple<int, bool>(0, false);
			}
			if (code != UnityTls.unitytls_error_code.UNITYTLS_USER_WOULD_BLOCK)
			{
				if (!this.closedGraceful)
				{
					Mono.Unity.Debug.CheckAndThrow(unitytls_errorstate, "Failed to read data to TLS context", AlertDescription.InternalError);
				}
				return new ValueTuple<int, bool>(0, false);
			}
			return new ValueTuple<int, bool>(num, true);
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x000027E0 File Offset: 0x000009E0
		[return: TupleElementNames(new string[]
		{
			"ret",
			"wantMore"
		})]
		public unsafe override ValueTuple<int, bool> Write(byte[] buffer, int offset, int count)
		{
			this.lastException = null;
			UnityTls.unitytls_errorstate unitytls_errorstate = UnityTls.NativeInterface.unitytls_errorstate_create();
			int num;
			fixed (byte[] array = buffer)
			{
				byte* ptr;
				if (buffer == null || array.Length == 0)
				{
					ptr = null;
				}
				else
				{
					ptr = &array[0];
				}
				num = (int)UnityTls.NativeInterface.unitytls_tlsctx_write(this.tlsContext, ptr + offset, (IntPtr)count, &unitytls_errorstate);
			}
			if (this.lastException != null)
			{
				throw this.lastException;
			}
			UnityTls.unitytls_error_code code = unitytls_errorstate.code;
			if (code == UnityTls.unitytls_error_code.UNITYTLS_SUCCESS)
			{
				return new ValueTuple<int, bool>(num, num < count);
			}
			if (code == UnityTls.unitytls_error_code.UNITYTLS_STREAM_CLOSED)
			{
				return new ValueTuple<int, bool>(0, false);
			}
			if (code != UnityTls.unitytls_error_code.UNITYTLS_USER_WOULD_BLOCK)
			{
				Mono.Unity.Debug.CheckAndThrow(unitytls_errorstate, "Failed to write data to TLS context", AlertDescription.InternalError);
				return new ValueTuple<int, bool>(0, false);
			}
			return new ValueTuple<int, bool>(num, true);
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x000028A4 File Offset: 0x00000AA4
		public unsafe override void Shutdown()
		{
			if (base.Settings != null && base.Settings.SendCloseNotify)
			{
				UnityTls.unitytls_errorstate unitytls_errorstate = UnityTls.NativeInterface.unitytls_errorstate_create();
				UnityTls.NativeInterface.unitytls_tlsctx_notify_close(this.tlsContext, &unitytls_errorstate);
			}
			UnityTls.NativeInterface.unitytls_x509list_free(this.requestedClientCertChain);
			UnityTls.NativeInterface.unitytls_key_free(this.requestedClientKey);
			UnityTls.NativeInterface.unitytls_tlsctx_free(this.tlsContext);
			this.tlsContext = null;
			this.hasContext = false;
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x0000293C File Offset: 0x00000B3C
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
					this.Shutdown();
					this.localClientCertificate = null;
					this.remoteCertificate = null;
					if (this.localClientCertificate != null)
					{
						this.localClientCertificate.Dispose();
						this.localClientCertificate = null;
					}
					if (this.remoteCertificate != null)
					{
						this.remoteCertificate.Dispose();
						this.remoteCertificate = null;
					}
					this.connectioninfo = null;
					this.isAuthenticated = false;
					this.hasContext = false;
				}
				this.handle.Free();
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x000029D0 File Offset: 0x00000BD0
		public unsafe override void StartHandshake()
		{
			if (base.Settings != null && base.Settings.EnabledCiphers != null)
			{
				UnityTls.unitytls_ciphersuite[] array = new UnityTls.unitytls_ciphersuite[base.Settings.EnabledCiphers.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = (UnityTls.unitytls_ciphersuite)base.Settings.EnabledCiphers[i];
				}
				UnityTls.unitytls_errorstate errorState = UnityTls.NativeInterface.unitytls_errorstate_create();
				UnityTls.unitytls_ciphersuite[] array2;
				UnityTls.unitytls_ciphersuite* supportedCiphersuites;
				if ((array2 = array) == null || array2.Length == 0)
				{
					supportedCiphersuites = null;
				}
				else
				{
					supportedCiphersuites = &array2[0];
				}
				UnityTls.NativeInterface.unitytls_tlsctx_set_supported_ciphersuites(this.tlsContext, supportedCiphersuites, (IntPtr)array.Length, &errorState);
				array2 = null;
				Mono.Unity.Debug.CheckAndThrow(errorState, "Failed to set list of supported ciphers", AlertDescription.HandshakeFailure);
			}
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00002A88 File Offset: 0x00000C88
		public unsafe override bool ProcessHandshake()
		{
			this.lastException = null;
			UnityTls.unitytls_errorstate unitytls_errorstate = UnityTls.NativeInterface.unitytls_errorstate_create();
			UnityTls.unitytls_x509verify_result unitytls_x509verify_result = UnityTls.NativeInterface.unitytls_tlsctx_process_handshake(this.tlsContext, &unitytls_errorstate);
			if (unitytls_errorstate.code == UnityTls.unitytls_error_code.UNITYTLS_USER_WOULD_BLOCK)
			{
				return false;
			}
			if (this.lastException != null)
			{
				throw this.lastException;
			}
			if (base.IsServer && unitytls_x509verify_result == (UnityTls.unitytls_x509verify_result)2147483648U)
			{
				Mono.Unity.Debug.CheckAndThrow(unitytls_errorstate, "Handshake failed", AlertDescription.HandshakeFailure);
				if (!base.ValidateCertificate(null, null))
				{
					throw new TlsException(AlertDescription.HandshakeFailure, "Verification failure during handshake");
				}
			}
			else
			{
				Mono.Unity.Debug.CheckAndThrow(unitytls_errorstate, unitytls_x509verify_result, "Handshake failed", AlertDescription.HandshakeFailure);
			}
			return true;
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00002B28 File Offset: 0x00000D28
		public unsafe override void FinishHandshake()
		{
			UnityTls.unitytls_errorstate unitytls_errorstate = UnityTls.NativeInterface.unitytls_errorstate_create();
			UnityTls.unitytls_ciphersuite unitytls_ciphersuite = UnityTls.NativeInterface.unitytls_tlsctx_get_ciphersuite(this.tlsContext, &unitytls_errorstate);
			UnityTls.unitytls_protocol protocol = UnityTls.NativeInterface.unitytls_tlsctx_get_protocol(this.tlsContext, &unitytls_errorstate);
			this.connectioninfo = new MonoTlsConnectionInfo
			{
				CipherSuiteCode = (CipherSuiteCode)unitytls_ciphersuite,
				ProtocolVersion = UnityTlsConversions.ConvertProtocolVersion(protocol),
				PeerDomainName = base.ServerName
			};
			this.isAuthenticated = true;
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00002BAC File Offset: 0x00000DAC
		[MonoPInvokeCallback(typeof(UnityTls.unitytls_tlsctx_write_callback))]
		private unsafe static IntPtr WriteCallback(void* userData, byte* data, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState)
		{
			return ((UnityTlsContext)((GCHandle)((IntPtr)userData)).Target).WriteCallback(data, bufferLen, errorState);
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00002BDC File Offset: 0x00000DDC
		private unsafe IntPtr WriteCallback(byte* data, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState)
		{
			IntPtr result;
			try
			{
				if (this.writeBuffer == null || this.writeBuffer.Length < (int)bufferLen)
				{
					this.writeBuffer = new byte[(int)bufferLen];
				}
				Marshal.Copy((IntPtr)((void*)data), this.writeBuffer, 0, (int)bufferLen);
				if (!base.Parent.InternalWrite(this.writeBuffer, 0, (int)bufferLen))
				{
					UnityTls.NativeInterface.unitytls_errorstate_raise_error(errorState, UnityTls.unitytls_error_code.UNITYTLS_USER_WRITE_FAILED);
					result = (IntPtr)0;
				}
				else
				{
					result = bufferLen;
				}
			}
			catch (Exception ex)
			{
				UnityTls.NativeInterface.unitytls_errorstate_raise_error(errorState, UnityTls.unitytls_error_code.UNITYTLS_USER_UNKNOWN_ERROR);
				if (this.lastException == null)
				{
					this.lastException = ex;
				}
				result = (IntPtr)0;
			}
			return result;
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00002CA8 File Offset: 0x00000EA8
		[MonoPInvokeCallback(typeof(UnityTls.unitytls_tlsctx_read_callback))]
		private unsafe static IntPtr ReadCallback(void* userData, byte* buffer, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState)
		{
			return ((UnityTlsContext)((GCHandle)((IntPtr)userData)).Target).ReadCallback(buffer, bufferLen, errorState);
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00002CD8 File Offset: 0x00000ED8
		private unsafe IntPtr ReadCallback(byte* buffer, IntPtr bufferLen, UnityTls.unitytls_errorstate* errorState)
		{
			IntPtr result;
			try
			{
				if (this.readBuffer == null || this.readBuffer.Length < (int)bufferLen)
				{
					this.readBuffer = new byte[(int)bufferLen];
				}
				bool flag;
				int num = base.Parent.InternalRead(this.readBuffer, 0, (int)bufferLen, out flag);
				if (num < 0)
				{
					UnityTls.NativeInterface.unitytls_errorstate_raise_error(errorState, UnityTls.unitytls_error_code.UNITYTLS_USER_READ_FAILED);
				}
				else if (num > 0)
				{
					Marshal.Copy(this.readBuffer, 0, (IntPtr)((void*)buffer), (int)bufferLen);
				}
				else if (flag)
				{
					UnityTls.NativeInterface.unitytls_errorstate_raise_error(errorState, UnityTls.unitytls_error_code.UNITYTLS_USER_WOULD_BLOCK);
				}
				else
				{
					this.closedGraceful = true;
					UnityTls.NativeInterface.unitytls_errorstate_raise_error(errorState, UnityTls.unitytls_error_code.UNITYTLS_USER_READ_FAILED);
				}
				result = (IntPtr)num;
			}
			catch (Exception ex)
			{
				UnityTls.NativeInterface.unitytls_errorstate_raise_error(errorState, UnityTls.unitytls_error_code.UNITYTLS_USER_UNKNOWN_ERROR);
				if (this.lastException == null)
				{
					this.lastException = ex;
				}
				result = (IntPtr)0;
			}
			return result;
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00002DE0 File Offset: 0x00000FE0
		[MonoPInvokeCallback(typeof(UnityTls.unitytls_tlsctx_x509verify_callback))]
		private unsafe static UnityTls.unitytls_x509verify_result VerifyCallback(void* userData, UnityTls.unitytls_x509list_ref chain, UnityTls.unitytls_errorstate* errorState)
		{
			return ((UnityTlsContext)((GCHandle)((IntPtr)userData)).Target).VerifyCallback(chain, errorState);
		}

		// Token: 0x060000BF RID: 191 RVA: 0x00002E0C File Offset: 0x0000100C
		private unsafe UnityTls.unitytls_x509verify_result VerifyCallback(UnityTls.unitytls_x509list_ref chain, UnityTls.unitytls_errorstate* errorState)
		{
			UnityTls.unitytls_x509verify_result result;
			try
			{
				X509CertificateCollection x509CertificateCollection = CertHelper.NativeChainToManagedCollection(chain, errorState);
				this.remoteCertificate = new X509Certificate(x509CertificateCollection[0]);
				if (base.ValidateCertificate(x509CertificateCollection))
				{
					result = UnityTls.unitytls_x509verify_result.UNITYTLS_X509VERIFY_SUCCESS;
				}
				else
				{
					result = UnityTls.unitytls_x509verify_result.UNITYTLS_X509VERIFY_FLAG_NOT_TRUSTED;
				}
			}
			catch (Exception ex)
			{
				if (this.lastException == null)
				{
					this.lastException = ex;
				}
				result = (UnityTls.unitytls_x509verify_result)4294967295U;
			}
			return result;
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00002E6C File Offset: 0x0000106C
		[MonoPInvokeCallback(typeof(UnityTls.unitytls_tlsctx_certificate_callback))]
		private unsafe static void CertificateCallback(void* userData, UnityTls.unitytls_tlsctx* ctx, byte* cn, IntPtr cnLen, UnityTls.unitytls_x509name* caList, IntPtr caListLen, UnityTls.unitytls_x509list_ref* chain, UnityTls.unitytls_key_ref* key, UnityTls.unitytls_errorstate* errorState)
		{
			((UnityTlsContext)((GCHandle)((IntPtr)userData)).Target).CertificateCallback(ctx, cn, cnLen, caList, caListLen, chain, key, errorState);
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00002EA4 File Offset: 0x000010A4
		private unsafe void CertificateCallback(UnityTls.unitytls_tlsctx* ctx, byte* cn, IntPtr cnLen, UnityTls.unitytls_x509name* caList, IntPtr caListLen, UnityTls.unitytls_x509list_ref* chain, UnityTls.unitytls_key_ref* key, UnityTls.unitytls_errorstate* errorState)
		{
			try
			{
				if (this.remoteCertificate == null)
				{
					throw new TlsException(AlertDescription.InternalError, "Cannot request client certificate before receiving one from the server.");
				}
				this.localClientCertificate = base.SelectClientCertificate(this.remoteCertificate, null);
				if (this.localClientCertificate == null)
				{
					*chain = new UnityTls.unitytls_x509list_ref
					{
						handle = UnityTls.NativeInterface.UNITYTLS_INVALID_HANDLE
					};
					*key = new UnityTls.unitytls_key_ref
					{
						handle = UnityTls.NativeInterface.UNITYTLS_INVALID_HANDLE
					};
				}
				else
				{
					UnityTls.NativeInterface.unitytls_x509list_free(this.requestedClientCertChain);
					UnityTls.NativeInterface.unitytls_key_free(this.requestedClientKey);
					UnityTlsContext.ExtractNativeKeyAndChainFromManagedCertificate(this.localClientCertificate, errorState, out this.requestedClientCertChain, out this.requestedClientKey);
					*chain = UnityTls.NativeInterface.unitytls_x509list_get_ref(this.requestedClientCertChain, errorState);
					*key = UnityTls.NativeInterface.unitytls_key_get_ref(this.requestedClientKey, errorState);
				}
				Mono.Unity.Debug.CheckAndThrow(*errorState, "Failed to retrieve certificates on request.", AlertDescription.HandshakeFailure);
			}
			catch (Exception ex)
			{
				UnityTls.NativeInterface.unitytls_errorstate_raise_error(errorState, UnityTls.unitytls_error_code.UNITYTLS_USER_UNKNOWN_ERROR);
				if (this.lastException == null)
				{
					this.lastException = ex;
				}
			}
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x00003000 File Offset: 0x00001200
		[MonoPInvokeCallback(typeof(UnityTls.unitytls_tlsctx_trace_callback))]
		private unsafe static void TraceCallback(void* userData, UnityTls.unitytls_tlsctx* ctx, byte* traceMessage, IntPtr traceMessageLen)
		{
			Console.Write(Encoding.UTF8.GetString(traceMessage, (int)traceMessageLen));
		}

		// Token: 0x040006F5 RID: 1781
		private const bool ActivateTracing = false;

		// Token: 0x040006F6 RID: 1782
		private unsafe UnityTls.unitytls_tlsctx* tlsContext = null;

		// Token: 0x040006F7 RID: 1783
		private unsafe UnityTls.unitytls_x509list* requestedClientCertChain = null;

		// Token: 0x040006F8 RID: 1784
		private unsafe UnityTls.unitytls_key* requestedClientKey = null;

		// Token: 0x040006F9 RID: 1785
		private X509Certificate localClientCertificate;

		// Token: 0x040006FA RID: 1786
		private X509Certificate remoteCertificate;

		// Token: 0x040006FB RID: 1787
		private MonoTlsConnectionInfo connectioninfo;

		// Token: 0x040006FC RID: 1788
		private bool isAuthenticated;

		// Token: 0x040006FD RID: 1789
		private bool hasContext;

		// Token: 0x040006FE RID: 1790
		private bool closedGraceful;

		// Token: 0x040006FF RID: 1791
		private byte[] writeBuffer;

		// Token: 0x04000700 RID: 1792
		private byte[] readBuffer;

		// Token: 0x04000701 RID: 1793
		private GCHandle handle;

		// Token: 0x04000702 RID: 1794
		private Exception lastException;
	}
}
