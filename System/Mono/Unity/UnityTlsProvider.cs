﻿using System;
using System.IO;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Mono.Net.Security;
using Mono.Security.Interface;

namespace Mono.Unity
{
	// Token: 0x0200003F RID: 63
	internal class UnityTlsProvider : MonoTlsProvider
	{
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x060000C8 RID: 200 RVA: 0x0000328A File Offset: 0x0000148A
		public override string Name
		{
			get
			{
				return "unitytls";
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x060000C9 RID: 201 RVA: 0x00003291 File Offset: 0x00001491
		public override Guid ID
		{
			get
			{
				return Mono.Net.Security.MonoTlsProviderFactory.UnityTlsId;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x060000CA RID: 202 RVA: 0x00003298 File Offset: 0x00001498
		public override bool SupportsSslStream
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x060000CB RID: 203 RVA: 0x00003298 File Offset: 0x00001498
		public override bool SupportsMonoExtensions
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x060000CC RID: 204 RVA: 0x00003298 File Offset: 0x00001498
		public override bool SupportsConnectionInfo
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x060000CD RID: 205 RVA: 0x00003298 File Offset: 0x00001498
		internal override bool SupportsCleanShutdown
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x060000CE RID: 206 RVA: 0x0000329B File Offset: 0x0000149B
		public override SslProtocols SupportedProtocols
		{
			get
			{
				return SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12;
			}
		}

		// Token: 0x060000CF RID: 207 RVA: 0x000032A2 File Offset: 0x000014A2
		public override IMonoSslStream CreateSslStream(Stream innerStream, bool leaveInnerStreamOpen, MonoTlsSettings settings = null)
		{
			return SslStream.CreateMonoSslStream(innerStream, leaveInnerStreamOpen, this, settings);
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x000032AD File Offset: 0x000014AD
		internal override IMonoSslStream CreateSslStreamInternal(SslStream sslStream, Stream innerStream, bool leaveInnerStreamOpen, MonoTlsSettings settings)
		{
			return new UnityTlsStream(innerStream, leaveInnerStreamOpen, sslStream, settings, this);
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x000032BC File Offset: 0x000014BC
		internal unsafe override bool ValidateCertificate(ICertificateValidator2 validator, string targetHost, bool serverMode, X509CertificateCollection certificates, bool wantsChain, ref X509Chain chain, ref MonoSslPolicyErrors errors, ref int status11)
		{
			if (certificates == null)
			{
				errors |= MonoSslPolicyErrors.RemoteCertificateNotAvailable;
				return false;
			}
			if (wantsChain)
			{
				chain = SystemCertificateValidator.CreateX509Chain(certificates);
			}
			if (certificates == null || certificates.Count == 0)
			{
				errors |= MonoSslPolicyErrors.RemoteCertificateNotAvailable;
				return false;
			}
			if (!string.IsNullOrEmpty(targetHost))
			{
				int num = targetHost.IndexOf(':');
				if (num > 0)
				{
					targetHost = targetHost.Substring(0, num);
				}
			}
			UnityTls.unitytls_errorstate unitytls_errorstate = UnityTls.NativeInterface.unitytls_errorstate_create();
			UnityTls.unitytls_x509list* ptr = UnityTls.NativeInterface.unitytls_x509list_create(&unitytls_errorstate);
			UnityTls.unitytls_x509verify_result unitytls_x509verify_result = (UnityTls.unitytls_x509verify_result)2147483648U;
			try
			{
				CertHelper.AddCertificatesToNativeChain(ptr, certificates, &unitytls_errorstate);
				UnityTls.unitytls_x509list_ref chain2 = UnityTls.NativeInterface.unitytls_x509list_get_ref(ptr, &unitytls_errorstate);
				byte[] bytes = Encoding.UTF8.GetBytes(targetHost);
				if (validator.Settings.TrustAnchors != null)
				{
					UnityTls.unitytls_x509list* ptr2 = UnityTls.NativeInterface.unitytls_x509list_create(&unitytls_errorstate);
					CertHelper.AddCertificatesToNativeChain(ptr2, validator.Settings.TrustAnchors, &unitytls_errorstate);
					UnityTls.unitytls_x509list_ref trustCA = UnityTls.NativeInterface.unitytls_x509list_get_ref(ptr, &unitytls_errorstate);
					try
					{
						byte[] array;
						byte* cn;
						if ((array = bytes) == null || array.Length == 0)
						{
							cn = null;
						}
						else
						{
							cn = &array[0];
						}
						unitytls_x509verify_result = UnityTls.NativeInterface.unitytls_x509verify_explicit_ca(chain2, trustCA, cn, (IntPtr)bytes.Length, null, null, &unitytls_errorstate);
					}
					finally
					{
						byte[] array = null;
					}
					UnityTls.NativeInterface.unitytls_x509list_free(ptr2);
				}
				else
				{
					try
					{
						byte[] array;
						byte* cn2;
						if ((array = bytes) == null || array.Length == 0)
						{
							cn2 = null;
						}
						else
						{
							cn2 = &array[0];
						}
						unitytls_x509verify_result = UnityTls.NativeInterface.unitytls_x509verify_default_ca(chain2, cn2, (IntPtr)bytes.Length, null, null, &unitytls_errorstate);
					}
					finally
					{
						byte[] array = null;
					}
				}
			}
			finally
			{
				UnityTls.NativeInterface.unitytls_x509list_free(ptr);
			}
			errors = UnityTlsConversions.VerifyResultToPolicyErrror(unitytls_x509verify_result);
			return unitytls_x509verify_result == UnityTls.unitytls_x509verify_result.UNITYTLS_X509VERIFY_SUCCESS && unitytls_errorstate.code == UnityTls.unitytls_error_code.UNITYTLS_SUCCESS;
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x000034D8 File Offset: 0x000016D8
		public UnityTlsProvider()
		{
		}
	}
}
