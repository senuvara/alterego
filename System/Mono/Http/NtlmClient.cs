﻿using System;
using System.Net;
using System.Runtime.CompilerServices;

namespace Mono.Http
{
	// Token: 0x02000082 RID: 130
	internal class NtlmClient : IAuthenticationModule
	{
		// Token: 0x060002DC RID: 732 RVA: 0x00008E9C File Offset: 0x0000709C
		public Authorization Authenticate(string challenge, WebRequest webRequest, ICredentials credentials)
		{
			if (credentials == null || challenge == null)
			{
				return null;
			}
			string text = challenge.Trim();
			int num = text.ToLower().IndexOf("ntlm");
			if (num == -1)
			{
				return null;
			}
			num = text.IndexOfAny(new char[]
			{
				' ',
				'\t'
			});
			if (num != -1)
			{
				text = text.Substring(num).Trim();
			}
			else
			{
				text = null;
			}
			HttpWebRequest httpWebRequest = webRequest as HttpWebRequest;
			if (httpWebRequest == null)
			{
				return null;
			}
			ConditionalWeakTable<HttpWebRequest, NtlmSession> obj = NtlmClient.cache;
			Authorization result;
			lock (obj)
			{
				result = NtlmClient.cache.GetValue(httpWebRequest, (HttpWebRequest x) => new NtlmSession()).Authenticate(text, webRequest, credentials);
			}
			return result;
		}

		// Token: 0x060002DD RID: 733 RVA: 0x00008B3F File Offset: 0x00006D3F
		public Authorization PreAuthenticate(WebRequest webRequest, ICredentials credentials)
		{
			return null;
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060002DE RID: 734 RVA: 0x00008F6C File Offset: 0x0000716C
		public string AuthenticationType
		{
			get
			{
				return "NTLM";
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060002DF RID: 735 RVA: 0x00005AFA File Offset: 0x00003CFA
		public bool CanPreAuthenticate
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x0000232F File Offset: 0x0000052F
		public NtlmClient()
		{
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x00008F73 File Offset: 0x00007173
		// Note: this type is marked as 'beforefieldinit'.
		static NtlmClient()
		{
		}

		// Token: 0x040007DA RID: 2010
		private static readonly ConditionalWeakTable<HttpWebRequest, NtlmSession> cache = new ConditionalWeakTable<HttpWebRequest, NtlmSession>();

		// Token: 0x02000083 RID: 131
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060002E2 RID: 738 RVA: 0x00008F7F File Offset: 0x0000717F
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060002E3 RID: 739 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c()
			{
			}

			// Token: 0x060002E4 RID: 740 RVA: 0x00008F8B File Offset: 0x0000718B
			internal NtlmSession <Authenticate>b__1_0(HttpWebRequest x)
			{
				return new NtlmSession();
			}

			// Token: 0x040007DB RID: 2011
			public static readonly NtlmClient.<>c <>9 = new NtlmClient.<>c();

			// Token: 0x040007DC RID: 2012
			public static ConditionalWeakTable<HttpWebRequest, NtlmSession>.CreateValueCallback <>9__1_0;
		}
	}
}
