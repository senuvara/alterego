﻿using System;
using System.Runtime.InteropServices;

namespace Mono.Net
{
	// Token: 0x02000044 RID: 68
	internal class CFNumber : CFObject
	{
		// Token: 0x060000F5 RID: 245 RVA: 0x00003658 File Offset: 0x00001858
		public CFNumber(IntPtr handle, bool own) : base(handle, own)
		{
		}

		// Token: 0x060000F6 RID: 246
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		[return: MarshalAs(UnmanagedType.I1)]
		private static extern bool CFNumberGetValue(IntPtr handle, IntPtr type, [MarshalAs(UnmanagedType.I1)] out bool value);

		// Token: 0x060000F7 RID: 247 RVA: 0x00003834 File Offset: 0x00001A34
		public static bool AsBool(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				return false;
			}
			bool result;
			CFNumber.CFNumberGetValue(handle, (IntPtr)1, out result);
			return result;
		}

		// Token: 0x060000F8 RID: 248 RVA: 0x00003860 File Offset: 0x00001A60
		public static implicit operator bool(CFNumber number)
		{
			return CFNumber.AsBool(number.Handle);
		}

		// Token: 0x060000F9 RID: 249
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		[return: MarshalAs(UnmanagedType.I1)]
		private static extern bool CFNumberGetValue(IntPtr handle, IntPtr type, out int value);

		// Token: 0x060000FA RID: 250 RVA: 0x00003870 File Offset: 0x00001A70
		public static int AsInt32(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				return 0;
			}
			int result;
			CFNumber.CFNumberGetValue(handle, (IntPtr)9, out result);
			return result;
		}

		// Token: 0x060000FB RID: 251
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFNumberCreate(IntPtr allocator, IntPtr theType, IntPtr valuePtr);

		// Token: 0x060000FC RID: 252 RVA: 0x0000389D File Offset: 0x00001A9D
		public static CFNumber FromInt32(int number)
		{
			return new CFNumber(CFNumber.CFNumberCreate(IntPtr.Zero, (IntPtr)9, (IntPtr)number), true);
		}

		// Token: 0x060000FD RID: 253 RVA: 0x000038BC File Offset: 0x00001ABC
		public static implicit operator int(CFNumber number)
		{
			return CFNumber.AsInt32(number.Handle);
		}
	}
}
