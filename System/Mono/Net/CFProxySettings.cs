﻿using System;

namespace Mono.Net
{
	// Token: 0x0200004F RID: 79
	internal class CFProxySettings
	{
		// Token: 0x0600013D RID: 317 RVA: 0x00003FB0 File Offset: 0x000021B0
		static CFProxySettings()
		{
			IntPtr handle = CFObject.dlopen("/System/Library/Frameworks/CoreServices.framework/Frameworks/CFNetwork.framework/CFNetwork", 0);
			CFProxySettings.kCFNetworkProxiesHTTPEnable = CFObject.GetCFObjectHandle(handle, "kCFNetworkProxiesHTTPEnable");
			CFProxySettings.kCFNetworkProxiesHTTPPort = CFObject.GetCFObjectHandle(handle, "kCFNetworkProxiesHTTPPort");
			CFProxySettings.kCFNetworkProxiesHTTPProxy = CFObject.GetCFObjectHandle(handle, "kCFNetworkProxiesHTTPProxy");
			CFProxySettings.kCFNetworkProxiesProxyAutoConfigEnable = CFObject.GetCFObjectHandle(handle, "kCFNetworkProxiesProxyAutoConfigEnable");
			CFProxySettings.kCFNetworkProxiesProxyAutoConfigJavaScript = CFObject.GetCFObjectHandle(handle, "kCFNetworkProxiesProxyAutoConfigJavaScript");
			CFProxySettings.kCFNetworkProxiesProxyAutoConfigURLString = CFObject.GetCFObjectHandle(handle, "kCFNetworkProxiesProxyAutoConfigURLString");
			CFObject.dlclose(handle);
		}

		// Token: 0x0600013E RID: 318 RVA: 0x0000402D File Offset: 0x0000222D
		public CFProxySettings(CFDictionary settings)
		{
			this.settings = settings;
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600013F RID: 319 RVA: 0x0000403C File Offset: 0x0000223C
		public CFDictionary Dictionary
		{
			get
			{
				return this.settings;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000140 RID: 320 RVA: 0x00004044 File Offset: 0x00002244
		public bool HTTPEnable
		{
			get
			{
				return !(CFProxySettings.kCFNetworkProxiesHTTPEnable == IntPtr.Zero) && CFNumber.AsBool(this.settings[CFProxySettings.kCFNetworkProxiesHTTPEnable]);
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000141 RID: 321 RVA: 0x0000406E File Offset: 0x0000226E
		public int HTTPPort
		{
			get
			{
				if (CFProxySettings.kCFNetworkProxiesHTTPPort == IntPtr.Zero)
				{
					return 0;
				}
				return CFNumber.AsInt32(this.settings[CFProxySettings.kCFNetworkProxiesHTTPPort]);
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000142 RID: 322 RVA: 0x00004098 File Offset: 0x00002298
		public string HTTPProxy
		{
			get
			{
				if (CFProxySettings.kCFNetworkProxiesHTTPProxy == IntPtr.Zero)
				{
					return null;
				}
				return CFString.AsString(this.settings[CFProxySettings.kCFNetworkProxiesHTTPProxy]);
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000143 RID: 323 RVA: 0x000040C2 File Offset: 0x000022C2
		public bool ProxyAutoConfigEnable
		{
			get
			{
				return !(CFProxySettings.kCFNetworkProxiesProxyAutoConfigEnable == IntPtr.Zero) && CFNumber.AsBool(this.settings[CFProxySettings.kCFNetworkProxiesProxyAutoConfigEnable]);
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000144 RID: 324 RVA: 0x000040EC File Offset: 0x000022EC
		public string ProxyAutoConfigJavaScript
		{
			get
			{
				if (CFProxySettings.kCFNetworkProxiesProxyAutoConfigJavaScript == IntPtr.Zero)
				{
					return null;
				}
				return CFString.AsString(this.settings[CFProxySettings.kCFNetworkProxiesProxyAutoConfigJavaScript]);
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000145 RID: 325 RVA: 0x00004116 File Offset: 0x00002316
		public string ProxyAutoConfigURLString
		{
			get
			{
				if (CFProxySettings.kCFNetworkProxiesProxyAutoConfigURLString == IntPtr.Zero)
				{
					return null;
				}
				return CFString.AsString(this.settings[CFProxySettings.kCFNetworkProxiesProxyAutoConfigURLString]);
			}
		}

		// Token: 0x04000727 RID: 1831
		private static IntPtr kCFNetworkProxiesHTTPEnable;

		// Token: 0x04000728 RID: 1832
		private static IntPtr kCFNetworkProxiesHTTPPort;

		// Token: 0x04000729 RID: 1833
		private static IntPtr kCFNetworkProxiesHTTPProxy;

		// Token: 0x0400072A RID: 1834
		private static IntPtr kCFNetworkProxiesProxyAutoConfigEnable;

		// Token: 0x0400072B RID: 1835
		private static IntPtr kCFNetworkProxiesProxyAutoConfigJavaScript;

		// Token: 0x0400072C RID: 1836
		private static IntPtr kCFNetworkProxiesProxyAutoConfigURLString;

		// Token: 0x0400072D RID: 1837
		private CFDictionary settings;
	}
}
