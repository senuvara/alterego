﻿using System;

namespace Mono.Net
{
	// Token: 0x0200004E RID: 78
	internal class CFProxy
	{
		// Token: 0x06000133 RID: 307 RVA: 0x00003D28 File Offset: 0x00001F28
		static CFProxy()
		{
			IntPtr handle = CFObject.dlopen("/System/Library/Frameworks/CoreServices.framework/Frameworks/CFNetwork.framework/CFNetwork", 0);
			CFProxy.kCFProxyAutoConfigurationJavaScriptKey = CFObject.GetCFObjectHandle(handle, "kCFProxyAutoConfigurationJavaScriptKey");
			CFProxy.kCFProxyAutoConfigurationURLKey = CFObject.GetCFObjectHandle(handle, "kCFProxyAutoConfigurationURLKey");
			CFProxy.kCFProxyHostNameKey = CFObject.GetCFObjectHandle(handle, "kCFProxyHostNameKey");
			CFProxy.kCFProxyPasswordKey = CFObject.GetCFObjectHandle(handle, "kCFProxyPasswordKey");
			CFProxy.kCFProxyPortNumberKey = CFObject.GetCFObjectHandle(handle, "kCFProxyPortNumberKey");
			CFProxy.kCFProxyTypeKey = CFObject.GetCFObjectHandle(handle, "kCFProxyTypeKey");
			CFProxy.kCFProxyUsernameKey = CFObject.GetCFObjectHandle(handle, "kCFProxyUsernameKey");
			CFProxy.kCFProxyTypeAutoConfigurationURL = CFObject.GetCFObjectHandle(handle, "kCFProxyTypeAutoConfigurationURL");
			CFProxy.kCFProxyTypeAutoConfigurationJavaScript = CFObject.GetCFObjectHandle(handle, "kCFProxyTypeAutoConfigurationJavaScript");
			CFProxy.kCFProxyTypeFTP = CFObject.GetCFObjectHandle(handle, "kCFProxyTypeFTP");
			CFProxy.kCFProxyTypeHTTP = CFObject.GetCFObjectHandle(handle, "kCFProxyTypeHTTP");
			CFProxy.kCFProxyTypeHTTPS = CFObject.GetCFObjectHandle(handle, "kCFProxyTypeHTTPS");
			CFProxy.kCFProxyTypeSOCKS = CFObject.GetCFObjectHandle(handle, "kCFProxyTypeSOCKS");
			CFObject.dlclose(handle);
		}

		// Token: 0x06000134 RID: 308 RVA: 0x00003E15 File Offset: 0x00002015
		internal CFProxy(CFDictionary settings)
		{
			this.settings = settings;
		}

		// Token: 0x06000135 RID: 309 RVA: 0x00003E24 File Offset: 0x00002024
		private static CFProxyType CFProxyTypeToEnum(IntPtr type)
		{
			if (type == CFProxy.kCFProxyTypeAutoConfigurationJavaScript)
			{
				return CFProxyType.AutoConfigurationJavaScript;
			}
			if (type == CFProxy.kCFProxyTypeAutoConfigurationURL)
			{
				return CFProxyType.AutoConfigurationUrl;
			}
			if (type == CFProxy.kCFProxyTypeFTP)
			{
				return CFProxyType.FTP;
			}
			if (type == CFProxy.kCFProxyTypeHTTP)
			{
				return CFProxyType.HTTP;
			}
			if (type == CFProxy.kCFProxyTypeHTTPS)
			{
				return CFProxyType.HTTPS;
			}
			if (type == CFProxy.kCFProxyTypeSOCKS)
			{
				return CFProxyType.SOCKS;
			}
			return CFProxyType.None;
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000136 RID: 310 RVA: 0x00003E8C File Offset: 0x0000208C
		public IntPtr AutoConfigurationJavaScript
		{
			get
			{
				if (CFProxy.kCFProxyAutoConfigurationJavaScriptKey == IntPtr.Zero)
				{
					return IntPtr.Zero;
				}
				return this.settings[CFProxy.kCFProxyAutoConfigurationJavaScriptKey];
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000137 RID: 311 RVA: 0x00003EB5 File Offset: 0x000020B5
		public IntPtr AutoConfigurationUrl
		{
			get
			{
				if (CFProxy.kCFProxyAutoConfigurationURLKey == IntPtr.Zero)
				{
					return IntPtr.Zero;
				}
				return this.settings[CFProxy.kCFProxyAutoConfigurationURLKey];
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000138 RID: 312 RVA: 0x00003EDE File Offset: 0x000020DE
		public string HostName
		{
			get
			{
				if (CFProxy.kCFProxyHostNameKey == IntPtr.Zero)
				{
					return null;
				}
				return CFString.AsString(this.settings[CFProxy.kCFProxyHostNameKey]);
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000139 RID: 313 RVA: 0x00003F08 File Offset: 0x00002108
		public string Password
		{
			get
			{
				if (CFProxy.kCFProxyPasswordKey == IntPtr.Zero)
				{
					return null;
				}
				return CFString.AsString(this.settings[CFProxy.kCFProxyPasswordKey]);
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600013A RID: 314 RVA: 0x00003F32 File Offset: 0x00002132
		public int Port
		{
			get
			{
				if (CFProxy.kCFProxyPortNumberKey == IntPtr.Zero)
				{
					return 0;
				}
				return CFNumber.AsInt32(this.settings[CFProxy.kCFProxyPortNumberKey]);
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600013B RID: 315 RVA: 0x00003F5C File Offset: 0x0000215C
		public CFProxyType ProxyType
		{
			get
			{
				if (CFProxy.kCFProxyTypeKey == IntPtr.Zero)
				{
					return CFProxyType.None;
				}
				return CFProxy.CFProxyTypeToEnum(this.settings[CFProxy.kCFProxyTypeKey]);
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600013C RID: 316 RVA: 0x00003F86 File Offset: 0x00002186
		public string Username
		{
			get
			{
				if (CFProxy.kCFProxyUsernameKey == IntPtr.Zero)
				{
					return null;
				}
				return CFString.AsString(this.settings[CFProxy.kCFProxyUsernameKey]);
			}
		}

		// Token: 0x04000719 RID: 1817
		private static IntPtr kCFProxyAutoConfigurationJavaScriptKey;

		// Token: 0x0400071A RID: 1818
		private static IntPtr kCFProxyAutoConfigurationURLKey;

		// Token: 0x0400071B RID: 1819
		private static IntPtr kCFProxyHostNameKey;

		// Token: 0x0400071C RID: 1820
		private static IntPtr kCFProxyPasswordKey;

		// Token: 0x0400071D RID: 1821
		private static IntPtr kCFProxyPortNumberKey;

		// Token: 0x0400071E RID: 1822
		private static IntPtr kCFProxyTypeKey;

		// Token: 0x0400071F RID: 1823
		private static IntPtr kCFProxyUsernameKey;

		// Token: 0x04000720 RID: 1824
		private static IntPtr kCFProxyTypeAutoConfigurationURL;

		// Token: 0x04000721 RID: 1825
		private static IntPtr kCFProxyTypeAutoConfigurationJavaScript;

		// Token: 0x04000722 RID: 1826
		private static IntPtr kCFProxyTypeFTP;

		// Token: 0x04000723 RID: 1827
		private static IntPtr kCFProxyTypeHTTP;

		// Token: 0x04000724 RID: 1828
		private static IntPtr kCFProxyTypeHTTPS;

		// Token: 0x04000725 RID: 1829
		private static IntPtr kCFProxyTypeSOCKS;

		// Token: 0x04000726 RID: 1830
		private CFDictionary settings;
	}
}
