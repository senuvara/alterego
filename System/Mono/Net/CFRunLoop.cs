﻿using System;
using System.Runtime.InteropServices;

namespace Mono.Net
{
	// Token: 0x0200004C RID: 76
	internal class CFRunLoop : CFObject
	{
		// Token: 0x06000128 RID: 296
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern void CFRunLoopAddSource(IntPtr rl, IntPtr source, IntPtr mode);

		// Token: 0x06000129 RID: 297
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern void CFRunLoopRemoveSource(IntPtr rl, IntPtr source, IntPtr mode);

		// Token: 0x0600012A RID: 298
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern int CFRunLoopRunInMode(IntPtr mode, double seconds, bool returnAfterSourceHandled);

		// Token: 0x0600012B RID: 299
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFRunLoopGetCurrent();

		// Token: 0x0600012C RID: 300
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern void CFRunLoopStop(IntPtr rl);

		// Token: 0x0600012D RID: 301 RVA: 0x00003658 File Offset: 0x00001858
		public CFRunLoop(IntPtr handle, bool own) : base(handle, own)
		{
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600012E RID: 302 RVA: 0x00003CD4 File Offset: 0x00001ED4
		public static CFRunLoop CurrentRunLoop
		{
			get
			{
				return new CFRunLoop(CFRunLoop.CFRunLoopGetCurrent(), false);
			}
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00003CE1 File Offset: 0x00001EE1
		public void AddSource(IntPtr source, CFString mode)
		{
			CFRunLoop.CFRunLoopAddSource(base.Handle, source, mode.Handle);
		}

		// Token: 0x06000130 RID: 304 RVA: 0x00003CF5 File Offset: 0x00001EF5
		public void RemoveSource(IntPtr source, CFString mode)
		{
			CFRunLoop.CFRunLoopRemoveSource(base.Handle, source, mode.Handle);
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00003D09 File Offset: 0x00001F09
		public int RunInMode(CFString mode, double seconds, bool returnAfterSourceHandled)
		{
			return CFRunLoop.CFRunLoopRunInMode(mode.Handle, seconds, returnAfterSourceHandled);
		}

		// Token: 0x06000132 RID: 306 RVA: 0x00003D18 File Offset: 0x00001F18
		public void Stop()
		{
			CFRunLoop.CFRunLoopStop(base.Handle);
		}
	}
}
