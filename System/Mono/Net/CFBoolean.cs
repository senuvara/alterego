﻿using System;
using System.Runtime.InteropServices;
using ObjCRuntimeInternal;

namespace Mono.Net
{
	// Token: 0x02000055 RID: 85
	internal class CFBoolean : INativeObject, IDisposable
	{
		// Token: 0x06000167 RID: 359 RVA: 0x00004878 File Offset: 0x00002A78
		static CFBoolean()
		{
			IntPtr value = CFObject.dlopen("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation", 0);
			if (value == IntPtr.Zero)
			{
				return;
			}
			try
			{
				CFBoolean.True = new CFBoolean(CFObject.GetCFObjectHandle(value, "kCFBooleanTrue"), false);
				CFBoolean.False = new CFBoolean(CFObject.GetCFObjectHandle(value, "kCFBooleanFalse"), false);
			}
			finally
			{
				CFObject.dlclose(value);
			}
		}

		// Token: 0x06000168 RID: 360 RVA: 0x000048E8 File Offset: 0x00002AE8
		internal CFBoolean(IntPtr handle, bool owns)
		{
			this.handle = handle;
			if (!owns)
			{
				CFObject.CFRetain(handle);
			}
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00004904 File Offset: 0x00002B04
		~CFBoolean()
		{
			this.Dispose(false);
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600016A RID: 362 RVA: 0x00004934 File Offset: 0x00002B34
		public IntPtr Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x0600016B RID: 363 RVA: 0x0000493C File Offset: 0x00002B3C
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600016C RID: 364 RVA: 0x0000494B File Offset: 0x00002B4B
		protected virtual void Dispose(bool disposing)
		{
			if (this.handle != IntPtr.Zero)
			{
				CFObject.CFRelease(this.handle);
				this.handle = IntPtr.Zero;
			}
		}

		// Token: 0x0600016D RID: 365 RVA: 0x00004975 File Offset: 0x00002B75
		public static implicit operator bool(CFBoolean value)
		{
			return value.Value;
		}

		// Token: 0x0600016E RID: 366 RVA: 0x0000497D File Offset: 0x00002B7D
		public static explicit operator CFBoolean(bool value)
		{
			return CFBoolean.FromBoolean(value);
		}

		// Token: 0x0600016F RID: 367 RVA: 0x00004985 File Offset: 0x00002B85
		public static CFBoolean FromBoolean(bool value)
		{
			if (!value)
			{
				return CFBoolean.False;
			}
			return CFBoolean.True;
		}

		// Token: 0x06000170 RID: 368
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		[return: MarshalAs(UnmanagedType.I1)]
		private static extern bool CFBooleanGetValue(IntPtr boolean);

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000171 RID: 369 RVA: 0x00004995 File Offset: 0x00002B95
		public bool Value
		{
			get
			{
				return CFBoolean.CFBooleanGetValue(this.handle);
			}
		}

		// Token: 0x06000172 RID: 370 RVA: 0x000049A2 File Offset: 0x00002BA2
		public static bool GetValue(IntPtr boolean)
		{
			return CFBoolean.CFBooleanGetValue(boolean);
		}

		// Token: 0x0400073B RID: 1851
		private IntPtr handle;

		// Token: 0x0400073C RID: 1852
		public static readonly CFBoolean True;

		// Token: 0x0400073D RID: 1853
		public static readonly CFBoolean False;
	}
}
