﻿using System;

namespace Mono.Net
{
	// Token: 0x0200004D RID: 77
	internal enum CFProxyType
	{
		// Token: 0x04000712 RID: 1810
		None,
		// Token: 0x04000713 RID: 1811
		AutoConfigurationUrl,
		// Token: 0x04000714 RID: 1812
		AutoConfigurationJavaScript,
		// Token: 0x04000715 RID: 1813
		FTP,
		// Token: 0x04000716 RID: 1814
		HTTP,
		// Token: 0x04000717 RID: 1815
		HTTPS,
		// Token: 0x04000718 RID: 1816
		SOCKS
	}
}
