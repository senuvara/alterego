﻿using System;
using System.Runtime.InteropServices;
using ObjCRuntimeInternal;

namespace Mono.Net
{
	// Token: 0x02000056 RID: 86
	internal class CFDate : INativeObject, IDisposable
	{
		// Token: 0x06000173 RID: 371 RVA: 0x000049AA File Offset: 0x00002BAA
		internal CFDate(IntPtr handle, bool owns)
		{
			this.handle = handle;
			if (!owns)
			{
				CFObject.CFRetain(handle);
			}
		}

		// Token: 0x06000174 RID: 372 RVA: 0x000049C4 File Offset: 0x00002BC4
		~CFDate()
		{
			this.Dispose(false);
		}

		// Token: 0x06000175 RID: 373
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFDateCreate(IntPtr allocator, double at);

		// Token: 0x06000176 RID: 374 RVA: 0x000049F4 File Offset: 0x00002BF4
		public static CFDate Create(DateTime date)
		{
			DateTime d = new DateTime(2001, 1, 1);
			double totalSeconds = (date - d).TotalSeconds;
			IntPtr value = CFDate.CFDateCreate(IntPtr.Zero, totalSeconds);
			if (value == IntPtr.Zero)
			{
				throw new NotSupportedException();
			}
			return new CFDate(value, true);
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000177 RID: 375 RVA: 0x00004A43 File Offset: 0x00002C43
		public IntPtr Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00004A4B File Offset: 0x00002C4B
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00004A5A File Offset: 0x00002C5A
		protected virtual void Dispose(bool disposing)
		{
			if (this.handle != IntPtr.Zero)
			{
				CFObject.CFRelease(this.handle);
				this.handle = IntPtr.Zero;
			}
		}

		// Token: 0x0400073E RID: 1854
		private IntPtr handle;
	}
}
