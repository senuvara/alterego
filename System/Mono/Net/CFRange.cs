﻿using System;

namespace Mono.Net
{
	// Token: 0x02000045 RID: 69
	internal struct CFRange
	{
		// Token: 0x060000FE RID: 254 RVA: 0x000038C9 File Offset: 0x00001AC9
		public CFRange(int loc, int len)
		{
			this.Location = (IntPtr)loc;
			this.Length = (IntPtr)len;
		}

		// Token: 0x04000707 RID: 1799
		public IntPtr Location;

		// Token: 0x04000708 RID: 1800
		public IntPtr Length;
	}
}
