﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Mono.Net
{
	// Token: 0x02000049 RID: 73
	internal class CFDictionary : CFObject
	{
		// Token: 0x06000114 RID: 276 RVA: 0x00003AD4 File Offset: 0x00001CD4
		static CFDictionary()
		{
			IntPtr intPtr = CFObject.dlopen("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation", 0);
			if (intPtr == IntPtr.Zero)
			{
				return;
			}
			try
			{
				CFDictionary.KeyCallbacks = CFObject.GetIndirect(intPtr, "kCFTypeDictionaryKeyCallBacks");
				CFDictionary.ValueCallbacks = CFObject.GetIndirect(intPtr, "kCFTypeDictionaryValueCallBacks");
			}
			finally
			{
				CFObject.dlclose(intPtr);
			}
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00003658 File Offset: 0x00001858
		public CFDictionary(IntPtr handle, bool own) : base(handle, own)
		{
		}

		// Token: 0x06000116 RID: 278 RVA: 0x00003B38 File Offset: 0x00001D38
		public static CFDictionary FromObjectAndKey(IntPtr obj, IntPtr key)
		{
			return new CFDictionary(CFDictionary.CFDictionaryCreate(IntPtr.Zero, new IntPtr[]
			{
				key
			}, new IntPtr[]
			{
				obj
			}, (IntPtr)1, CFDictionary.KeyCallbacks, CFDictionary.ValueCallbacks), true);
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00003B70 File Offset: 0x00001D70
		public static CFDictionary FromKeysAndObjects(IList<Tuple<IntPtr, IntPtr>> items)
		{
			IntPtr[] array = new IntPtr[items.Count];
			IntPtr[] array2 = new IntPtr[items.Count];
			for (int i = 0; i < items.Count; i++)
			{
				array[i] = items[i].Item1;
				array2[i] = items[i].Item2;
			}
			return new CFDictionary(CFDictionary.CFDictionaryCreate(IntPtr.Zero, array, array2, (IntPtr)items.Count, CFDictionary.KeyCallbacks, CFDictionary.ValueCallbacks), true);
		}

		// Token: 0x06000118 RID: 280
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFDictionaryCreate(IntPtr allocator, IntPtr[] keys, IntPtr[] vals, IntPtr len, IntPtr keyCallbacks, IntPtr valCallbacks);

		// Token: 0x06000119 RID: 281
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFDictionaryGetValue(IntPtr handle, IntPtr key);

		// Token: 0x0600011A RID: 282
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFDictionaryCreateCopy(IntPtr allocator, IntPtr handle);

		// Token: 0x0600011B RID: 283 RVA: 0x00003BEB File Offset: 0x00001DEB
		public CFDictionary Copy()
		{
			return new CFDictionary(CFDictionary.CFDictionaryCreateCopy(IntPtr.Zero, base.Handle), true);
		}

		// Token: 0x0600011C RID: 284 RVA: 0x00003C03 File Offset: 0x00001E03
		public CFMutableDictionary MutableCopy()
		{
			return new CFMutableDictionary(CFDictionary.CFDictionaryCreateMutableCopy(IntPtr.Zero, IntPtr.Zero, base.Handle), true);
		}

		// Token: 0x0600011D RID: 285
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFDictionaryCreateMutableCopy(IntPtr allocator, IntPtr capacity, IntPtr theDict);

		// Token: 0x0600011E RID: 286 RVA: 0x00003C20 File Offset: 0x00001E20
		public IntPtr GetValue(IntPtr key)
		{
			return CFDictionary.CFDictionaryGetValue(base.Handle, key);
		}

		// Token: 0x17000018 RID: 24
		public IntPtr this[IntPtr key]
		{
			get
			{
				return this.GetValue(key);
			}
		}

		// Token: 0x0400070F RID: 1807
		private static readonly IntPtr KeyCallbacks;

		// Token: 0x04000710 RID: 1808
		private static readonly IntPtr ValueCallbacks;
	}
}
