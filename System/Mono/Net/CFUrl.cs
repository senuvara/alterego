﻿using System;
using System.Runtime.InteropServices;

namespace Mono.Net
{
	// Token: 0x0200004B RID: 75
	internal class CFUrl : CFObject
	{
		// Token: 0x06000125 RID: 293 RVA: 0x00003658 File Offset: 0x00001858
		public CFUrl(IntPtr handle, bool own) : base(handle, own)
		{
		}

		// Token: 0x06000126 RID: 294
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFURLCreateWithString(IntPtr allocator, IntPtr str, IntPtr baseURL);

		// Token: 0x06000127 RID: 295 RVA: 0x00003C84 File Offset: 0x00001E84
		public static CFUrl Create(string absolute)
		{
			if (string.IsNullOrEmpty(absolute))
			{
				return null;
			}
			CFString cfstring = CFString.Create(absolute);
			IntPtr intPtr = CFUrl.CFURLCreateWithString(IntPtr.Zero, cfstring.Handle, IntPtr.Zero);
			cfstring.Dispose();
			if (intPtr == IntPtr.Zero)
			{
				return null;
			}
			return new CFUrl(intPtr, true);
		}
	}
}
