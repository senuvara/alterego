﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;

namespace Mono.Net.Security
{
	// Token: 0x0200005A RID: 90
	internal class AsyncProtocolResult
	{
		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000182 RID: 386 RVA: 0x00004C10 File Offset: 0x00002E10
		public int UserResult
		{
			[CompilerGenerated]
			get
			{
				return this.<UserResult>k__BackingField;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000183 RID: 387 RVA: 0x00004C18 File Offset: 0x00002E18
		public ExceptionDispatchInfo Error
		{
			[CompilerGenerated]
			get
			{
				return this.<Error>k__BackingField;
			}
		}

		// Token: 0x06000184 RID: 388 RVA: 0x00004C20 File Offset: 0x00002E20
		public AsyncProtocolResult(int result)
		{
			this.UserResult = result;
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00004C2F File Offset: 0x00002E2F
		public AsyncProtocolResult(ExceptionDispatchInfo error)
		{
			this.Error = error;
		}

		// Token: 0x0400074A RID: 1866
		[CompilerGenerated]
		private readonly int <UserResult>k__BackingField;

		// Token: 0x0400074B RID: 1867
		[CompilerGenerated]
		private readonly ExceptionDispatchInfo <Error>k__BackingField;
	}
}
