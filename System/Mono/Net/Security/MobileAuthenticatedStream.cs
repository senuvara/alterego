﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Mono.Security.Interface;

namespace Mono.Net.Security
{
	// Token: 0x02000067 RID: 103
	internal abstract class MobileAuthenticatedStream : AuthenticatedStream, IMonoSslStream, IDisposable
	{
		// Token: 0x060001CA RID: 458 RVA: 0x00005B4C File Offset: 0x00003D4C
		public MobileAuthenticatedStream(Stream innerStream, bool leaveInnerStreamOpen, SslStream owner, MonoTlsSettings settings, MonoTlsProvider provider) : base(innerStream, leaveInnerStreamOpen)
		{
			this.SslStream = owner;
			this.Settings = settings;
			this.Provider = provider;
			this.readBuffer = new BufferOffsetSize2(16834);
			this.writeBuffer = new BufferOffsetSize2(16384);
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060001CB RID: 459 RVA: 0x00005BB6 File Offset: 0x00003DB6
		public SslStream SslStream
		{
			[CompilerGenerated]
			get
			{
				return this.<SslStream>k__BackingField;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060001CC RID: 460 RVA: 0x00005BBE File Offset: 0x00003DBE
		public MonoTlsSettings Settings
		{
			[CompilerGenerated]
			get
			{
				return this.<Settings>k__BackingField;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060001CD RID: 461 RVA: 0x00005BC6 File Offset: 0x00003DC6
		public MonoTlsProvider Provider
		{
			[CompilerGenerated]
			get
			{
				return this.<Provider>k__BackingField;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060001CE RID: 462 RVA: 0x00005BCE File Offset: 0x00003DCE
		internal bool HasContext
		{
			get
			{
				return this.xobileTlsContext != null;
			}
		}

		// Token: 0x060001CF RID: 463 RVA: 0x00005BDC File Offset: 0x00003DDC
		internal void CheckThrow(bool authSuccessCheck, bool shutdownCheck = false)
		{
			if (this.lastException != null)
			{
				this.lastException.Throw();
			}
			if (authSuccessCheck && !this.IsAuthenticated)
			{
				throw new InvalidOperationException("This operation is only allowed using a successfully authenticated context.");
			}
			if (shutdownCheck && this.shutdown)
			{
				throw new InvalidOperationException("Write operations are not allowed after the channel was shutdown.");
			}
		}

		// Token: 0x060001D0 RID: 464 RVA: 0x00005C28 File Offset: 0x00003E28
		internal static Exception GetSSPIException(Exception e)
		{
			if (e is OperationCanceledException || e is IOException || e is ObjectDisposedException || e is AuthenticationException)
			{
				return e;
			}
			return new AuthenticationException("A call to SSPI failed, see inner exception.", e);
		}

		// Token: 0x060001D1 RID: 465 RVA: 0x00005C57 File Offset: 0x00003E57
		internal static Exception GetIOException(Exception e, string message)
		{
			if (e is OperationCanceledException || e is IOException || e is ObjectDisposedException || e is AuthenticationException)
			{
				return e;
			}
			return new IOException(message, e);
		}

		// Token: 0x060001D2 RID: 466 RVA: 0x00005C84 File Offset: 0x00003E84
		internal ExceptionDispatchInfo SetException(Exception e)
		{
			ExceptionDispatchInfo exceptionDispatchInfo = ExceptionDispatchInfo.Capture(e);
			return Interlocked.CompareExchange<ExceptionDispatchInfo>(ref this.lastException, exceptionDispatchInfo, null) ?? exceptionDispatchInfo;
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060001D3 RID: 467 RVA: 0x0000329B File Offset: 0x0000149B
		private SslProtocols DefaultProtocols
		{
			get
			{
				return SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12;
			}
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x00005CAA File Offset: 0x00003EAA
		public void AuthenticateAsClient(string targetHost)
		{
			this.AuthenticateAsClient(targetHost, new X509CertificateCollection(), this.DefaultProtocols, false);
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x00005CBF File Offset: 0x00003EBF
		public void AuthenticateAsClient(string targetHost, X509CertificateCollection clientCertificates, SslProtocols enabledSslProtocols, bool checkCertificateRevocation)
		{
			this.ProcessAuthentication(true, false, targetHost, enabledSslProtocols, null, clientCertificates, false).Wait();
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x00005CD3 File Offset: 0x00003ED3
		public IAsyncResult BeginAuthenticateAsClient(string targetHost, AsyncCallback asyncCallback, object asyncState)
		{
			return this.BeginAuthenticateAsClient(targetHost, new X509CertificateCollection(), this.DefaultProtocols, false, asyncCallback, asyncState);
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x00005CEA File Offset: 0x00003EEA
		public IAsyncResult BeginAuthenticateAsClient(string targetHost, X509CertificateCollection clientCertificates, SslProtocols enabledSslProtocols, bool checkCertificateRevocation, AsyncCallback asyncCallback, object asyncState)
		{
			return TaskToApm.Begin(this.ProcessAuthentication(false, false, targetHost, enabledSslProtocols, null, clientCertificates, false), asyncCallback, asyncState);
		}

		// Token: 0x060001D8 RID: 472 RVA: 0x00005D02 File Offset: 0x00003F02
		public void EndAuthenticateAsClient(IAsyncResult asyncResult)
		{
			TaskToApm.End(asyncResult);
		}

		// Token: 0x060001D9 RID: 473 RVA: 0x00005D0A File Offset: 0x00003F0A
		public void AuthenticateAsServer(X509Certificate serverCertificate)
		{
			this.AuthenticateAsServer(serverCertificate, false, this.DefaultProtocols, false);
		}

		// Token: 0x060001DA RID: 474 RVA: 0x00005D1B File Offset: 0x00003F1B
		public void AuthenticateAsServer(X509Certificate serverCertificate, bool clientCertificateRequired, SslProtocols enabledSslProtocols, bool checkCertificateRevocation)
		{
			this.ProcessAuthentication(true, true, string.Empty, enabledSslProtocols, serverCertificate, null, clientCertificateRequired).Wait();
		}

		// Token: 0x060001DB RID: 475 RVA: 0x00005D33 File Offset: 0x00003F33
		public IAsyncResult BeginAuthenticateAsServer(X509Certificate serverCertificate, AsyncCallback asyncCallback, object asyncState)
		{
			return this.BeginAuthenticateAsServer(serverCertificate, false, this.DefaultProtocols, false, asyncCallback, asyncState);
		}

		// Token: 0x060001DC RID: 476 RVA: 0x00005D46 File Offset: 0x00003F46
		public IAsyncResult BeginAuthenticateAsServer(X509Certificate serverCertificate, bool clientCertificateRequired, SslProtocols enabledSslProtocols, bool checkCertificateRevocation, AsyncCallback asyncCallback, object asyncState)
		{
			return TaskToApm.Begin(this.ProcessAuthentication(false, true, string.Empty, enabledSslProtocols, serverCertificate, null, clientCertificateRequired), asyncCallback, asyncState);
		}

		// Token: 0x060001DD RID: 477 RVA: 0x00005D02 File Offset: 0x00003F02
		public void EndAuthenticateAsServer(IAsyncResult asyncResult)
		{
			TaskToApm.End(asyncResult);
		}

		// Token: 0x060001DE RID: 478 RVA: 0x00005D62 File Offset: 0x00003F62
		public Task AuthenticateAsClientAsync(string targetHost)
		{
			return this.ProcessAuthentication(false, false, targetHost, this.DefaultProtocols, null, null, false);
		}

		// Token: 0x060001DF RID: 479 RVA: 0x00005D76 File Offset: 0x00003F76
		public Task AuthenticateAsClientAsync(string targetHost, X509CertificateCollection clientCertificates, SslProtocols enabledSslProtocols, bool checkCertificateRevocation)
		{
			return this.ProcessAuthentication(false, false, targetHost, enabledSslProtocols, null, clientCertificates, false);
		}

		// Token: 0x060001E0 RID: 480 RVA: 0x00005D85 File Offset: 0x00003F85
		public Task AuthenticateAsServerAsync(X509Certificate serverCertificate)
		{
			return this.AuthenticateAsServerAsync(serverCertificate, false, this.DefaultProtocols, false);
		}

		// Token: 0x060001E1 RID: 481 RVA: 0x00005D96 File Offset: 0x00003F96
		public Task AuthenticateAsServerAsync(X509Certificate serverCertificate, bool clientCertificateRequired, SslProtocols enabledSslProtocols, bool checkCertificateRevocation)
		{
			return this.ProcessAuthentication(false, true, string.Empty, enabledSslProtocols, serverCertificate, null, clientCertificateRequired);
		}

		// Token: 0x060001E2 RID: 482 RVA: 0x00005DAC File Offset: 0x00003FAC
		public Task ShutdownAsync()
		{
			AsyncShutdownRequest asyncRequest = new AsyncShutdownRequest(this);
			return this.StartOperation(MobileAuthenticatedStream.OperationType.Shutdown, asyncRequest, CancellationToken.None);
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060001E3 RID: 483 RVA: 0x00002068 File Offset: 0x00000268
		public AuthenticatedStream AuthenticatedStream
		{
			get
			{
				return this;
			}
		}

		// Token: 0x060001E4 RID: 484 RVA: 0x00005DD0 File Offset: 0x00003FD0
		private async Task ProcessAuthentication(bool runSynchronously, bool serverMode, string targetHost, SslProtocols enabledProtocols, X509Certificate serverCertificate, X509CertificateCollection clientCertificates, bool clientCertRequired)
		{
			if (serverMode)
			{
				if (serverCertificate == null)
				{
					throw new ArgumentException("serverCertificate");
				}
			}
			else
			{
				if (targetHost == null)
				{
					throw new ArgumentException("targetHost");
				}
				if (targetHost.Length == 0)
				{
					targetHost = "?" + Interlocked.Increment(ref MobileAuthenticatedStream.uniqueNameInteger).ToString(NumberFormatInfo.InvariantInfo);
				}
			}
			if (this.lastException != null)
			{
				this.lastException.Throw();
			}
			AsyncHandshakeRequest asyncHandshakeRequest = new AsyncHandshakeRequest(this, runSynchronously);
			if (Interlocked.CompareExchange<AsyncProtocolRequest>(ref this.asyncHandshakeRequest, asyncHandshakeRequest, null) != null)
			{
				throw new InvalidOperationException("Invalid nested call.");
			}
			if (Interlocked.CompareExchange<AsyncProtocolRequest>(ref this.asyncReadRequest, asyncHandshakeRequest, null) != null)
			{
				throw new InvalidOperationException("Invalid nested call.");
			}
			if (Interlocked.CompareExchange<AsyncProtocolRequest>(ref this.asyncWriteRequest, asyncHandshakeRequest, null) != null)
			{
				throw new InvalidOperationException("Invalid nested call.");
			}
			AsyncProtocolResult asyncProtocolResult;
			try
			{
				object obj = this.ioLock;
				lock (obj)
				{
					if (this.xobileTlsContext != null)
					{
						throw new InvalidOperationException();
					}
					this.readBuffer.Reset();
					this.writeBuffer.Reset();
					this.xobileTlsContext = this.CreateContext(serverMode, targetHost, enabledProtocols, serverCertificate, clientCertificates, clientCertRequired);
				}
				try
				{
					asyncProtocolResult = await asyncHandshakeRequest.StartOperation(CancellationToken.None).ConfigureAwait(false);
				}
				catch (Exception e)
				{
					asyncProtocolResult = new AsyncProtocolResult(this.SetException(MobileAuthenticatedStream.GetSSPIException(e)));
				}
			}
			finally
			{
				object obj = this.ioLock;
				bool flag = false;
				try
				{
					Monitor.Enter(obj, ref flag);
					this.readBuffer.Reset();
					this.writeBuffer.Reset();
					this.asyncWriteRequest = null;
					this.asyncReadRequest = null;
					this.asyncHandshakeRequest = null;
				}
				finally
				{
					int num;
					if (num < 0 && flag)
					{
						Monitor.Exit(obj);
					}
				}
			}
			if (asyncProtocolResult.Error != null)
			{
				asyncProtocolResult.Error.Throw();
			}
		}

		// Token: 0x060001E5 RID: 485
		protected abstract MobileTlsContext CreateContext(bool serverMode, string targetHost, SslProtocols enabledProtocols, X509Certificate serverCertificate, X509CertificateCollection clientCertificates, bool askForClientCert);

		// Token: 0x060001E6 RID: 486 RVA: 0x00005E54 File Offset: 0x00004054
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			AsyncReadRequest asyncRequest = new AsyncReadRequest(this, false, buffer, offset, count);
			return TaskToApm.Begin(this.StartOperation(MobileAuthenticatedStream.OperationType.Read, asyncRequest, CancellationToken.None), asyncCallback, asyncState);
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x00005E82 File Offset: 0x00004082
		public override int EndRead(IAsyncResult asyncResult)
		{
			return TaskToApm.End<int>(asyncResult);
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x00005E8C File Offset: 0x0000408C
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			AsyncWriteRequest asyncRequest = new AsyncWriteRequest(this, false, buffer, offset, count);
			return TaskToApm.Begin(this.StartOperation(MobileAuthenticatedStream.OperationType.Write, asyncRequest, CancellationToken.None), asyncCallback, asyncState);
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x00005D02 File Offset: 0x00003F02
		public override void EndWrite(IAsyncResult asyncResult)
		{
			TaskToApm.End(asyncResult);
		}

		// Token: 0x060001EA RID: 490 RVA: 0x00005EBC File Offset: 0x000040BC
		public override int Read(byte[] buffer, int offset, int count)
		{
			AsyncReadRequest asyncRequest = new AsyncReadRequest(this, true, buffer, offset, count);
			return this.StartOperation(MobileAuthenticatedStream.OperationType.Read, asyncRequest, CancellationToken.None).Result;
		}

		// Token: 0x060001EB RID: 491 RVA: 0x00005EE6 File Offset: 0x000040E6
		public void Write(byte[] buffer)
		{
			this.Write(buffer, 0, buffer.Length);
		}

		// Token: 0x060001EC RID: 492 RVA: 0x00005EF4 File Offset: 0x000040F4
		public override void Write(byte[] buffer, int offset, int count)
		{
			AsyncWriteRequest asyncRequest = new AsyncWriteRequest(this, true, buffer, offset, count);
			this.StartOperation(MobileAuthenticatedStream.OperationType.Write, asyncRequest, CancellationToken.None).Wait();
		}

		// Token: 0x060001ED RID: 493 RVA: 0x00005F20 File Offset: 0x00004120
		public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			AsyncReadRequest asyncRequest = new AsyncReadRequest(this, false, buffer, offset, count);
			return this.StartOperation(MobileAuthenticatedStream.OperationType.Read, asyncRequest, cancellationToken);
		}

		// Token: 0x060001EE RID: 494 RVA: 0x00005F44 File Offset: 0x00004144
		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			AsyncWriteRequest asyncRequest = new AsyncWriteRequest(this, false, buffer, offset, count);
			return this.StartOperation(MobileAuthenticatedStream.OperationType.Write, asyncRequest, cancellationToken);
		}

		// Token: 0x060001EF RID: 495 RVA: 0x00005F68 File Offset: 0x00004168
		private async Task<int> StartOperation(MobileAuthenticatedStream.OperationType type, AsyncProtocolRequest asyncRequest, CancellationToken cancellationToken)
		{
			this.CheckThrow(true, type > MobileAuthenticatedStream.OperationType.Read);
			if (type == MobileAuthenticatedStream.OperationType.Read)
			{
				if (Interlocked.CompareExchange<AsyncProtocolRequest>(ref this.asyncReadRequest, asyncRequest, null) != null)
				{
					throw new InvalidOperationException("Invalid nested call.");
				}
			}
			else if (Interlocked.CompareExchange<AsyncProtocolRequest>(ref this.asyncWriteRequest, asyncRequest, null) != null)
			{
				throw new InvalidOperationException("Invalid nested call.");
			}
			AsyncProtocolResult asyncProtocolResult;
			try
			{
				object obj = this.ioLock;
				lock (obj)
				{
					if (type == MobileAuthenticatedStream.OperationType.Read)
					{
						this.readBuffer.Reset();
					}
					else
					{
						this.writeBuffer.Reset();
					}
				}
				asyncProtocolResult = await asyncRequest.StartOperation(cancellationToken).ConfigureAwait(false);
			}
			catch (Exception e)
			{
				asyncProtocolResult = new AsyncProtocolResult(this.SetException(MobileAuthenticatedStream.GetIOException(e, asyncRequest.Name + " failed")));
			}
			finally
			{
				object obj = this.ioLock;
				bool flag = false;
				try
				{
					Monitor.Enter(obj, ref flag);
					if (type == MobileAuthenticatedStream.OperationType.Read)
					{
						this.readBuffer.Reset();
						this.asyncReadRequest = null;
					}
					else
					{
						this.writeBuffer.Reset();
						this.asyncWriteRequest = null;
					}
				}
				finally
				{
					int num;
					if (num < 0 && flag)
					{
						Monitor.Exit(obj);
					}
				}
			}
			if (asyncProtocolResult.Error != null)
			{
				asyncProtocolResult.Error.Throw();
			}
			return asyncProtocolResult.UserResult;
		}

		// Token: 0x060001F0 RID: 496 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("MONO_TLS_DEBUG")]
		protected internal void Debug(string message, params object[] args)
		{
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x00005FC8 File Offset: 0x000041C8
		internal int InternalRead(byte[] buffer, int offset, int size, out bool outWantMore)
		{
			int result;
			try
			{
				AsyncProtocolRequest asyncRequest = this.asyncHandshakeRequest ?? this.asyncReadRequest;
				ValueTuple<int, bool> valueTuple = this.InternalRead(asyncRequest, this.readBuffer, buffer, offset, size);
				int item = valueTuple.Item1;
				bool item2 = valueTuple.Item2;
				outWantMore = item2;
				result = item;
			}
			catch (Exception e)
			{
				this.SetException(MobileAuthenticatedStream.GetIOException(e, "InternalRead() failed"));
				outWantMore = false;
				result = -1;
			}
			return result;
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x0000603C File Offset: 0x0000423C
		private ValueTuple<int, bool> InternalRead(AsyncProtocolRequest asyncRequest, BufferOffsetSize internalBuffer, byte[] buffer, int offset, int size)
		{
			if (asyncRequest == null)
			{
				throw new InvalidOperationException();
			}
			if (internalBuffer.Size == 0 && !internalBuffer.Complete)
			{
				internalBuffer.Offset = (internalBuffer.Size = 0);
				asyncRequest.RequestRead(size);
				return new ValueTuple<int, bool>(0, true);
			}
			int num = Math.Min(internalBuffer.Size, size);
			Buffer.BlockCopy(internalBuffer.Buffer, internalBuffer.Offset, buffer, offset, num);
			internalBuffer.Offset += num;
			internalBuffer.Size -= num;
			return new ValueTuple<int, bool>(num, !internalBuffer.Complete && num < size);
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x000060D8 File Offset: 0x000042D8
		internal bool InternalWrite(byte[] buffer, int offset, int size)
		{
			bool result;
			try
			{
				AsyncProtocolRequest asyncRequest = this.asyncHandshakeRequest ?? this.asyncWriteRequest;
				result = this.InternalWrite(asyncRequest, this.writeBuffer, buffer, offset, size);
			}
			catch (Exception e)
			{
				this.SetException(MobileAuthenticatedStream.GetIOException(e, "InternalWrite() failed"));
				result = false;
			}
			return result;
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x00006134 File Offset: 0x00004334
		private bool InternalWrite(AsyncProtocolRequest asyncRequest, BufferOffsetSize2 internalBuffer, byte[] buffer, int offset, int size)
		{
			if (asyncRequest == null)
			{
				if (this.lastException != null)
				{
					return false;
				}
				if (Interlocked.Exchange(ref this.closeRequested, 1) == 0)
				{
					internalBuffer.Reset();
				}
				else if (internalBuffer.Remaining == 0)
				{
					throw new InvalidOperationException();
				}
			}
			internalBuffer.AppendData(buffer, offset, size);
			if (asyncRequest != null)
			{
				asyncRequest.RequestWrite();
			}
			return true;
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x00006188 File Offset: 0x00004388
		internal async Task<int> InnerRead(bool sync, int requestedSize, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			int len = Math.Min(this.readBuffer.Remaining, requestedSize);
			if (len == 0)
			{
				throw new InvalidOperationException();
			}
			Task<int> task;
			if (sync)
			{
				task = Task.Run<int>(() => this.InnerStream.Read(this.readBuffer.Buffer, this.readBuffer.EndOffset, len));
			}
			else
			{
				task = base.InnerStream.ReadAsync(this.readBuffer.Buffer, this.readBuffer.EndOffset, len, cancellationToken);
			}
			int num = await task.ConfigureAwait(false);
			if (num >= 0)
			{
				this.readBuffer.Size += num;
				this.readBuffer.TotalBytes += num;
			}
			if (num == 0)
			{
				this.readBuffer.Complete = true;
				if (this.readBuffer.TotalBytes > 0)
				{
					num = -1;
				}
			}
			return num;
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x000061E8 File Offset: 0x000043E8
		internal async Task InnerWrite(bool sync, CancellationToken cancellationToken)
		{
			cancellationToken.ThrowIfCancellationRequested();
			if (this.writeBuffer.Size != 0)
			{
				Task task;
				if (sync)
				{
					task = Task.Run(delegate()
					{
						base.InnerStream.Write(this.writeBuffer.Buffer, this.writeBuffer.Offset, this.writeBuffer.Size);
					});
				}
				else
				{
					task = base.InnerStream.WriteAsync(this.writeBuffer.Buffer, this.writeBuffer.Offset, this.writeBuffer.Size);
				}
				await task.ConfigureAwait(false);
				this.writeBuffer.TotalBytes += this.writeBuffer.Size;
				BufferOffsetSize bufferOffsetSize = this.writeBuffer;
				BufferOffsetSize bufferOffsetSize2 = this.writeBuffer;
				int num = 0;
				bufferOffsetSize2.Size = num;
				bufferOffsetSize.Offset = num;
			}
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x00006240 File Offset: 0x00004440
		internal AsyncOperationStatus ProcessHandshake(AsyncOperationStatus status)
		{
			object obj = this.ioLock;
			AsyncOperationStatus result;
			lock (obj)
			{
				if (status == AsyncOperationStatus.Initialize)
				{
					this.xobileTlsContext.StartHandshake();
					result = AsyncOperationStatus.Continue;
				}
				else
				{
					if (status == AsyncOperationStatus.ReadDone)
					{
						throw new IOException("Authentication failed because the remote party has closed the transport stream.");
					}
					if (status != AsyncOperationStatus.Continue)
					{
						throw new InvalidOperationException();
					}
					AsyncOperationStatus asyncOperationStatus = AsyncOperationStatus.Continue;
					if (this.xobileTlsContext.ProcessHandshake())
					{
						this.xobileTlsContext.FinishHandshake();
						asyncOperationStatus = AsyncOperationStatus.Complete;
					}
					if (this.lastException != null)
					{
						this.lastException.Throw();
					}
					result = asyncOperationStatus;
				}
			}
			return result;
		}

		// Token: 0x060001F8 RID: 504 RVA: 0x000062D8 File Offset: 0x000044D8
		internal ValueTuple<int, bool> ProcessRead(BufferOffsetSize userBuffer)
		{
			object obj = this.ioLock;
			ValueTuple<int, bool> result;
			lock (obj)
			{
				ValueTuple<int, bool> valueTuple = this.xobileTlsContext.Read(userBuffer.Buffer, userBuffer.Offset, userBuffer.Size);
				if (this.lastException != null)
				{
					this.lastException.Throw();
				}
				result = valueTuple;
			}
			return result;
		}

		// Token: 0x060001F9 RID: 505 RVA: 0x00006344 File Offset: 0x00004544
		internal ValueTuple<int, bool> ProcessWrite(BufferOffsetSize userBuffer)
		{
			object obj = this.ioLock;
			ValueTuple<int, bool> result;
			lock (obj)
			{
				ValueTuple<int, bool> valueTuple = this.xobileTlsContext.Write(userBuffer.Buffer, userBuffer.Offset, userBuffer.Size);
				if (this.lastException != null)
				{
					this.lastException.Throw();
				}
				result = valueTuple;
			}
			return result;
		}

		// Token: 0x060001FA RID: 506 RVA: 0x000063B0 File Offset: 0x000045B0
		internal AsyncOperationStatus ProcessShutdown(AsyncOperationStatus status)
		{
			object obj = this.ioLock;
			AsyncOperationStatus result;
			lock (obj)
			{
				this.xobileTlsContext.Shutdown();
				this.shutdown = true;
				result = AsyncOperationStatus.Complete;
			}
			return result;
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060001FB RID: 507 RVA: 0x00006400 File Offset: 0x00004600
		public override bool IsServer
		{
			get
			{
				this.CheckThrow(false, false);
				return this.xobileTlsContext != null && this.xobileTlsContext.IsServer;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060001FC RID: 508 RVA: 0x00006420 File Offset: 0x00004620
		public override bool IsAuthenticated
		{
			get
			{
				object obj = this.ioLock;
				bool result;
				lock (obj)
				{
					result = (this.xobileTlsContext != null && this.lastException == null && this.xobileTlsContext.IsAuthenticated);
				}
				return result;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060001FD RID: 509 RVA: 0x0000647C File Offset: 0x0000467C
		public override bool IsMutuallyAuthenticated
		{
			get
			{
				object obj = this.ioLock;
				bool result;
				lock (obj)
				{
					if (!this.IsAuthenticated)
					{
						result = false;
					}
					else if ((this.xobileTlsContext.IsServer ? this.xobileTlsContext.LocalServerCertificate : this.xobileTlsContext.LocalClientCertificate) == null)
					{
						result = false;
					}
					else
					{
						result = this.xobileTlsContext.IsRemoteCertificateAvailable;
					}
				}
				return result;
			}
		}

		// Token: 0x060001FE RID: 510 RVA: 0x000064FC File Offset: 0x000046FC
		protected override void Dispose(bool disposing)
		{
			try
			{
				object obj = this.ioLock;
				lock (obj)
				{
					this.lastException = ExceptionDispatchInfo.Capture(new ObjectDisposedException("MobileAuthenticatedStream"));
					if (this.xobileTlsContext != null)
					{
						this.xobileTlsContext.Dispose();
						this.xobileTlsContext = null;
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x060001FF RID: 511 RVA: 0x0000657C File Offset: 0x0000477C
		public override void Flush()
		{
			base.InnerStream.Flush();
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000200 RID: 512 RVA: 0x0000658C File Offset: 0x0000478C
		public SslProtocols SslProtocol
		{
			get
			{
				object obj = this.ioLock;
				SslProtocols negotiatedProtocol;
				lock (obj)
				{
					this.CheckThrow(true, false);
					negotiatedProtocol = (SslProtocols)this.xobileTlsContext.NegotiatedProtocol;
				}
				return negotiatedProtocol;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000201 RID: 513 RVA: 0x000065DC File Offset: 0x000047DC
		public X509Certificate RemoteCertificate
		{
			get
			{
				object obj = this.ioLock;
				X509Certificate remoteCertificate;
				lock (obj)
				{
					this.CheckThrow(true, false);
					remoteCertificate = this.xobileTlsContext.RemoteCertificate;
				}
				return remoteCertificate;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000202 RID: 514 RVA: 0x0000662C File Offset: 0x0000482C
		public X509Certificate LocalCertificate
		{
			get
			{
				object obj = this.ioLock;
				X509Certificate internalLocalCertificate;
				lock (obj)
				{
					this.CheckThrow(true, false);
					internalLocalCertificate = this.InternalLocalCertificate;
				}
				return internalLocalCertificate;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000203 RID: 515 RVA: 0x00006678 File Offset: 0x00004878
		public X509Certificate InternalLocalCertificate
		{
			get
			{
				object obj = this.ioLock;
				X509Certificate result;
				lock (obj)
				{
					this.CheckThrow(false, false);
					if (this.xobileTlsContext == null)
					{
						result = null;
					}
					else
					{
						result = (this.xobileTlsContext.IsServer ? this.xobileTlsContext.LocalServerCertificate : this.xobileTlsContext.LocalClientCertificate);
					}
				}
				return result;
			}
		}

		// Token: 0x06000204 RID: 516 RVA: 0x000066F0 File Offset: 0x000048F0
		public MonoTlsConnectionInfo GetConnectionInfo()
		{
			object obj = this.ioLock;
			MonoTlsConnectionInfo connectionInfo;
			lock (obj)
			{
				this.CheckThrow(true, false);
				connectionInfo = this.xobileTlsContext.ConnectionInfo;
			}
			return connectionInfo;
		}

		// Token: 0x06000205 RID: 517 RVA: 0x00006740 File Offset: 0x00004940
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000206 RID: 518 RVA: 0x00006747 File Offset: 0x00004947
		public override void SetLength(long value)
		{
			base.InnerStream.SetLength(value);
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000207 RID: 519 RVA: 0x00006740 File Offset: 0x00004940
		public TransportContext TransportContext
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000208 RID: 520 RVA: 0x00006755 File Offset: 0x00004955
		public override bool CanRead
		{
			get
			{
				return this.IsAuthenticated && base.InnerStream.CanRead;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000209 RID: 521 RVA: 0x0000676C File Offset: 0x0000496C
		public override bool CanTimeout
		{
			get
			{
				return base.InnerStream.CanTimeout;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x0600020A RID: 522 RVA: 0x00006779 File Offset: 0x00004979
		public override bool CanWrite
		{
			get
			{
				return (this.IsAuthenticated & base.InnerStream.CanWrite) && !this.shutdown;
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x0600020B RID: 523 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x0600020C RID: 524 RVA: 0x0000679A File Offset: 0x0000499A
		public override long Length
		{
			get
			{
				return base.InnerStream.Length;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x0600020D RID: 525 RVA: 0x000067A7 File Offset: 0x000049A7
		// (set) Token: 0x0600020E RID: 526 RVA: 0x00006740 File Offset: 0x00004940
		public override long Position
		{
			get
			{
				return base.InnerStream.Position;
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600020F RID: 527 RVA: 0x000067B4 File Offset: 0x000049B4
		public override bool IsEncrypted
		{
			get
			{
				return this.IsAuthenticated;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000210 RID: 528 RVA: 0x000067B4 File Offset: 0x000049B4
		public override bool IsSigned
		{
			get
			{
				return this.IsAuthenticated;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000211 RID: 529 RVA: 0x000067BC File Offset: 0x000049BC
		// (set) Token: 0x06000212 RID: 530 RVA: 0x000067C9 File Offset: 0x000049C9
		public override int ReadTimeout
		{
			get
			{
				return base.InnerStream.ReadTimeout;
			}
			set
			{
				base.InnerStream.ReadTimeout = value;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000213 RID: 531 RVA: 0x000067D7 File Offset: 0x000049D7
		// (set) Token: 0x06000214 RID: 532 RVA: 0x000067E4 File Offset: 0x000049E4
		public override int WriteTimeout
		{
			get
			{
				return base.InnerStream.WriteTimeout;
			}
			set
			{
				base.InnerStream.WriteTimeout = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000215 RID: 533 RVA: 0x000067F4 File Offset: 0x000049F4
		public System.Security.Authentication.CipherAlgorithmType CipherAlgorithm
		{
			get
			{
				this.CheckThrow(true, false);
				MonoTlsConnectionInfo connectionInfo = this.GetConnectionInfo();
				if (connectionInfo == null)
				{
					return System.Security.Authentication.CipherAlgorithmType.None;
				}
				switch (connectionInfo.CipherAlgorithmType)
				{
				case Mono.Security.Interface.CipherAlgorithmType.Aes128:
				case Mono.Security.Interface.CipherAlgorithmType.AesGcm128:
					return System.Security.Authentication.CipherAlgorithmType.Aes128;
				case Mono.Security.Interface.CipherAlgorithmType.Aes256:
				case Mono.Security.Interface.CipherAlgorithmType.AesGcm256:
					return System.Security.Authentication.CipherAlgorithmType.Aes256;
				default:
					return System.Security.Authentication.CipherAlgorithmType.None;
				}
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000216 RID: 534 RVA: 0x00006844 File Offset: 0x00004A44
		public System.Security.Authentication.HashAlgorithmType HashAlgorithm
		{
			get
			{
				this.CheckThrow(true, false);
				MonoTlsConnectionInfo connectionInfo = this.GetConnectionInfo();
				if (connectionInfo == null)
				{
					return System.Security.Authentication.HashAlgorithmType.None;
				}
				Mono.Security.Interface.HashAlgorithmType hashAlgorithmType = connectionInfo.HashAlgorithmType;
				if (hashAlgorithmType != Mono.Security.Interface.HashAlgorithmType.Md5)
				{
					if (hashAlgorithmType - Mono.Security.Interface.HashAlgorithmType.Sha1 <= 4)
					{
						return System.Security.Authentication.HashAlgorithmType.Sha1;
					}
					if (hashAlgorithmType != Mono.Security.Interface.HashAlgorithmType.Md5Sha1)
					{
						return System.Security.Authentication.HashAlgorithmType.None;
					}
				}
				return System.Security.Authentication.HashAlgorithmType.Md5;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000217 RID: 535 RVA: 0x0000688C File Offset: 0x00004A8C
		public System.Security.Authentication.ExchangeAlgorithmType KeyExchangeAlgorithm
		{
			get
			{
				this.CheckThrow(true, false);
				MonoTlsConnectionInfo connectionInfo = this.GetConnectionInfo();
				if (connectionInfo == null)
				{
					return System.Security.Authentication.ExchangeAlgorithmType.None;
				}
				switch (connectionInfo.ExchangeAlgorithmType)
				{
				case Mono.Security.Interface.ExchangeAlgorithmType.Dhe:
				case Mono.Security.Interface.ExchangeAlgorithmType.EcDhe:
					return System.Security.Authentication.ExchangeAlgorithmType.DiffieHellman;
				case Mono.Security.Interface.ExchangeAlgorithmType.Rsa:
					return System.Security.Authentication.ExchangeAlgorithmType.RsaSign;
				default:
					return System.Security.Authentication.ExchangeAlgorithmType.None;
				}
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000218 RID: 536 RVA: 0x000068D7 File Offset: 0x00004AD7
		public int CipherStrength
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000219 RID: 537 RVA: 0x000068D7 File Offset: 0x00004AD7
		public int HashStrength
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600021A RID: 538 RVA: 0x000068D7 File Offset: 0x00004AD7
		public int KeyExchangeStrength
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600021B RID: 539 RVA: 0x000068D7 File Offset: 0x00004AD7
		public bool CheckCertRevocationStatus
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x0600021C RID: 540 RVA: 0x000068DE File Offset: 0x00004ADE
		// Note: this type is marked as 'beforefieldinit'.
		static MobileAuthenticatedStream()
		{
		}

		// Token: 0x0600021D RID: 541 RVA: 0x000068E7 File Offset: 0x00004AE7
		[CompilerGenerated]
		private void <InnerWrite>b__67_0()
		{
			base.InnerStream.Write(this.writeBuffer.Buffer, this.writeBuffer.Offset, this.writeBuffer.Size);
		}

		// Token: 0x04000772 RID: 1906
		private MobileTlsContext xobileTlsContext;

		// Token: 0x04000773 RID: 1907
		private ExceptionDispatchInfo lastException;

		// Token: 0x04000774 RID: 1908
		private AsyncProtocolRequest asyncHandshakeRequest;

		// Token: 0x04000775 RID: 1909
		private AsyncProtocolRequest asyncReadRequest;

		// Token: 0x04000776 RID: 1910
		private AsyncProtocolRequest asyncWriteRequest;

		// Token: 0x04000777 RID: 1911
		private BufferOffsetSize2 readBuffer;

		// Token: 0x04000778 RID: 1912
		private BufferOffsetSize2 writeBuffer;

		// Token: 0x04000779 RID: 1913
		private object ioLock = new object();

		// Token: 0x0400077A RID: 1914
		private int closeRequested;

		// Token: 0x0400077B RID: 1915
		private bool shutdown;

		// Token: 0x0400077C RID: 1916
		private static int uniqueNameInteger = 123;

		// Token: 0x0400077D RID: 1917
		[CompilerGenerated]
		private readonly SslStream <SslStream>k__BackingField;

		// Token: 0x0400077E RID: 1918
		[CompilerGenerated]
		private readonly MonoTlsSettings <Settings>k__BackingField;

		// Token: 0x0400077F RID: 1919
		[CompilerGenerated]
		private readonly MonoTlsProvider <Provider>k__BackingField;

		// Token: 0x04000780 RID: 1920
		private static int nextId;

		// Token: 0x04000781 RID: 1921
		internal readonly int ID = ++MobileAuthenticatedStream.nextId;

		// Token: 0x02000068 RID: 104
		private enum OperationType
		{
			// Token: 0x04000783 RID: 1923
			Read,
			// Token: 0x04000784 RID: 1924
			Write,
			// Token: 0x04000785 RID: 1925
			Shutdown
		}

		// Token: 0x02000069 RID: 105
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ProcessAuthentication>d__47 : IAsyncStateMachine
		{
			// Token: 0x0600021E RID: 542 RVA: 0x00006918 File Offset: 0x00004B18
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				MobileAuthenticatedStream mobileAuthenticatedStream = this;
				try
				{
					AsyncHandshakeRequest asyncHandshakeRequest;
					if (num != 0)
					{
						if (serverMode)
						{
							if (serverCertificate == null)
							{
								throw new ArgumentException("serverCertificate");
							}
						}
						else
						{
							if (targetHost == null)
							{
								throw new ArgumentException("targetHost");
							}
							if (targetHost.Length == 0)
							{
								targetHost = "?" + Interlocked.Increment(ref MobileAuthenticatedStream.uniqueNameInteger).ToString(NumberFormatInfo.InvariantInfo);
							}
						}
						if (mobileAuthenticatedStream.lastException != null)
						{
							mobileAuthenticatedStream.lastException.Throw();
						}
						asyncHandshakeRequest = new AsyncHandshakeRequest(mobileAuthenticatedStream, runSynchronously);
						if (Interlocked.CompareExchange<AsyncProtocolRequest>(ref mobileAuthenticatedStream.asyncHandshakeRequest, asyncHandshakeRequest, null) != null)
						{
							throw new InvalidOperationException("Invalid nested call.");
						}
						if (Interlocked.CompareExchange<AsyncProtocolRequest>(ref mobileAuthenticatedStream.asyncReadRequest, asyncHandshakeRequest, null) != null)
						{
							throw new InvalidOperationException("Invalid nested call.");
						}
						if (Interlocked.CompareExchange<AsyncProtocolRequest>(ref mobileAuthenticatedStream.asyncWriteRequest, asyncHandshakeRequest, null) != null)
						{
							throw new InvalidOperationException("Invalid nested call.");
						}
					}
					AsyncProtocolResult asyncProtocolResult;
					try
					{
						if (num != 0)
						{
							object ioLock = mobileAuthenticatedStream.ioLock;
							bool flag = false;
							try
							{
								Monitor.Enter(ioLock, ref flag);
								if (mobileAuthenticatedStream.xobileTlsContext != null)
								{
									throw new InvalidOperationException();
								}
								mobileAuthenticatedStream.readBuffer.Reset();
								mobileAuthenticatedStream.writeBuffer.Reset();
								mobileAuthenticatedStream.xobileTlsContext = mobileAuthenticatedStream.CreateContext(serverMode, targetHost, enabledProtocols, serverCertificate, clientCertificates, clientCertRequired);
							}
							finally
							{
								if (num < 0 && flag)
								{
									Monitor.Exit(ioLock);
								}
							}
						}
						try
						{
							ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter configuredTaskAwaiter;
							if (num != 0)
							{
								configuredTaskAwaiter = asyncHandshakeRequest.StartOperation(CancellationToken.None).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num = (num2 = 0);
									ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter, MobileAuthenticatedStream.<ProcessAuthentication>d__47>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
							else
							{
								ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter);
								num = (num2 = -1);
							}
							asyncProtocolResult = configuredTaskAwaiter.GetResult();
						}
						catch (Exception e)
						{
							asyncProtocolResult = new AsyncProtocolResult(mobileAuthenticatedStream.SetException(MobileAuthenticatedStream.GetSSPIException(e)));
						}
					}
					finally
					{
						if (num < 0)
						{
							object ioLock = mobileAuthenticatedStream.ioLock;
							bool flag = false;
							try
							{
								Monitor.Enter(ioLock, ref flag);
								mobileAuthenticatedStream.readBuffer.Reset();
								mobileAuthenticatedStream.writeBuffer.Reset();
								mobileAuthenticatedStream.asyncWriteRequest = null;
								mobileAuthenticatedStream.asyncReadRequest = null;
								mobileAuthenticatedStream.asyncHandshakeRequest = null;
							}
							finally
							{
								if (num < 0 && flag)
								{
									Monitor.Exit(ioLock);
								}
							}
						}
					}
					if (asyncProtocolResult.Error != null)
					{
						asyncProtocolResult.Error.Throw();
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600021F RID: 543 RVA: 0x00006C28 File Offset: 0x00004E28
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000786 RID: 1926
			public int <>1__state;

			// Token: 0x04000787 RID: 1927
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000788 RID: 1928
			public bool serverMode;

			// Token: 0x04000789 RID: 1929
			public X509Certificate serverCertificate;

			// Token: 0x0400078A RID: 1930
			public string targetHost;

			// Token: 0x0400078B RID: 1931
			public MobileAuthenticatedStream <>4__this;

			// Token: 0x0400078C RID: 1932
			public bool runSynchronously;

			// Token: 0x0400078D RID: 1933
			public SslProtocols enabledProtocols;

			// Token: 0x0400078E RID: 1934
			public X509CertificateCollection clientCertificates;

			// Token: 0x0400078F RID: 1935
			public bool clientCertRequired;

			// Token: 0x04000790 RID: 1936
			private ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200006A RID: 106
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <StartOperation>d__58 : IAsyncStateMachine
		{
			// Token: 0x06000220 RID: 544 RVA: 0x00006C38 File Offset: 0x00004E38
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				MobileAuthenticatedStream mobileAuthenticatedStream = this;
				int userResult;
				try
				{
					if (num != 0)
					{
						mobileAuthenticatedStream.CheckThrow(true, type > MobileAuthenticatedStream.OperationType.Read);
						if (type == MobileAuthenticatedStream.OperationType.Read)
						{
							if (Interlocked.CompareExchange<AsyncProtocolRequest>(ref mobileAuthenticatedStream.asyncReadRequest, asyncRequest, null) != null)
							{
								throw new InvalidOperationException("Invalid nested call.");
							}
						}
						else if (Interlocked.CompareExchange<AsyncProtocolRequest>(ref mobileAuthenticatedStream.asyncWriteRequest, asyncRequest, null) != null)
						{
							throw new InvalidOperationException("Invalid nested call.");
						}
					}
					AsyncProtocolResult asyncProtocolResult;
					try
					{
						ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							object ioLock = mobileAuthenticatedStream.ioLock;
							bool flag = false;
							try
							{
								Monitor.Enter(ioLock, ref flag);
								if (type == MobileAuthenticatedStream.OperationType.Read)
								{
									mobileAuthenticatedStream.readBuffer.Reset();
								}
								else
								{
									mobileAuthenticatedStream.writeBuffer.Reset();
								}
							}
							finally
							{
								if (num < 0 && flag)
								{
									Monitor.Exit(ioLock);
								}
							}
							configuredTaskAwaiter = asyncRequest.StartOperation(cancellationToken).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 0);
								ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter, MobileAuthenticatedStream.<StartOperation>d__58>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						asyncProtocolResult = configuredTaskAwaiter.GetResult();
					}
					catch (Exception e)
					{
						asyncProtocolResult = new AsyncProtocolResult(mobileAuthenticatedStream.SetException(MobileAuthenticatedStream.GetIOException(e, asyncRequest.Name + " failed")));
					}
					finally
					{
						if (num < 0)
						{
							object ioLock = mobileAuthenticatedStream.ioLock;
							bool flag = false;
							try
							{
								Monitor.Enter(ioLock, ref flag);
								if (type == MobileAuthenticatedStream.OperationType.Read)
								{
									mobileAuthenticatedStream.readBuffer.Reset();
									mobileAuthenticatedStream.asyncReadRequest = null;
								}
								else
								{
									mobileAuthenticatedStream.writeBuffer.Reset();
									mobileAuthenticatedStream.asyncWriteRequest = null;
								}
							}
							finally
							{
								if (num < 0 && flag)
								{
									Monitor.Exit(ioLock);
								}
							}
						}
					}
					if (asyncProtocolResult.Error != null)
					{
						asyncProtocolResult.Error.Throw();
					}
					userResult = asyncProtocolResult.UserResult;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(userResult);
			}

			// Token: 0x06000221 RID: 545 RVA: 0x00006EBC File Offset: 0x000050BC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000791 RID: 1937
			public int <>1__state;

			// Token: 0x04000792 RID: 1938
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000793 RID: 1939
			public MobileAuthenticatedStream <>4__this;

			// Token: 0x04000794 RID: 1940
			public MobileAuthenticatedStream.OperationType type;

			// Token: 0x04000795 RID: 1941
			public AsyncProtocolRequest asyncRequest;

			// Token: 0x04000796 RID: 1942
			public CancellationToken cancellationToken;

			// Token: 0x04000797 RID: 1943
			private ConfiguredTaskAwaitable<AsyncProtocolResult>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200006B RID: 107
		[CompilerGenerated]
		private sealed class <>c__DisplayClass66_0
		{
			// Token: 0x06000222 RID: 546 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass66_0()
			{
			}

			// Token: 0x06000223 RID: 547 RVA: 0x00006ECA File Offset: 0x000050CA
			internal int <InnerRead>b__0()
			{
				return this.<>4__this.InnerStream.Read(this.<>4__this.readBuffer.Buffer, this.<>4__this.readBuffer.EndOffset, this.len);
			}

			// Token: 0x04000798 RID: 1944
			public MobileAuthenticatedStream <>4__this;

			// Token: 0x04000799 RID: 1945
			public int len;
		}

		// Token: 0x0200006C RID: 108
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InnerRead>d__66 : IAsyncStateMachine
		{
			// Token: 0x06000224 RID: 548 RVA: 0x00006F04 File Offset: 0x00005104
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				MobileAuthenticatedStream mobileAuthenticatedStream = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						MobileAuthenticatedStream.<>c__DisplayClass66_0 CS$<>8__locals1 = new MobileAuthenticatedStream.<>c__DisplayClass66_0();
						CS$<>8__locals1.<>4__this = this;
						cancellationToken.ThrowIfCancellationRequested();
						CS$<>8__locals1.len = Math.Min(mobileAuthenticatedStream.readBuffer.Remaining, requestedSize);
						if (CS$<>8__locals1.len == 0)
						{
							throw new InvalidOperationException();
						}
						Task<int> task;
						if (sync)
						{
							task = Task.Run<int>(new Func<int>(CS$<>8__locals1.<InnerRead>b__0));
						}
						else
						{
							task = mobileAuthenticatedStream.InnerStream.ReadAsync(mobileAuthenticatedStream.readBuffer.Buffer, mobileAuthenticatedStream.readBuffer.EndOffset, CS$<>8__locals1.len, cancellationToken);
						}
						configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, MobileAuthenticatedStream.<InnerRead>d__66>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int num3 = configuredTaskAwaiter.GetResult();
					if (num3 >= 0)
					{
						mobileAuthenticatedStream.readBuffer.Size += num3;
						mobileAuthenticatedStream.readBuffer.TotalBytes += num3;
					}
					if (num3 == 0)
					{
						mobileAuthenticatedStream.readBuffer.Complete = true;
						if (mobileAuthenticatedStream.readBuffer.TotalBytes > 0)
						{
							num3 = -1;
						}
					}
					result = num3;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000225 RID: 549 RVA: 0x000070BC File Offset: 0x000052BC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400079A RID: 1946
			public int <>1__state;

			// Token: 0x0400079B RID: 1947
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400079C RID: 1948
			public MobileAuthenticatedStream <>4__this;

			// Token: 0x0400079D RID: 1949
			public CancellationToken cancellationToken;

			// Token: 0x0400079E RID: 1950
			public int requestedSize;

			// Token: 0x0400079F RID: 1951
			public bool sync;

			// Token: 0x040007A0 RID: 1952
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200006D RID: 109
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InnerWrite>d__67 : IAsyncStateMachine
		{
			// Token: 0x06000226 RID: 550 RVA: 0x000070CC File Offset: 0x000052CC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				MobileAuthenticatedStream mobileAuthenticatedStream = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						cancellationToken.ThrowIfCancellationRequested();
						if (mobileAuthenticatedStream.writeBuffer.Size == 0)
						{
							goto IL_12E;
						}
						Task task;
						if (sync)
						{
							task = Task.Run(delegate()
							{
								base.InnerStream.Write(mobileAuthenticatedStream.writeBuffer.Buffer, mobileAuthenticatedStream.writeBuffer.Offset, mobileAuthenticatedStream.writeBuffer.Size);
							});
						}
						else
						{
							task = mobileAuthenticatedStream.InnerStream.WriteAsync(mobileAuthenticatedStream.writeBuffer.Buffer, mobileAuthenticatedStream.writeBuffer.Offset, mobileAuthenticatedStream.writeBuffer.Size);
						}
						configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, MobileAuthenticatedStream.<InnerWrite>d__67>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					mobileAuthenticatedStream.writeBuffer.TotalBytes += mobileAuthenticatedStream.writeBuffer.Size;
					mobileAuthenticatedStream.writeBuffer.Offset = (mobileAuthenticatedStream.writeBuffer.Size = 0);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_12E:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000227 RID: 551 RVA: 0x00007238 File Offset: 0x00005438
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007A1 RID: 1953
			public int <>1__state;

			// Token: 0x040007A2 RID: 1954
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040007A3 RID: 1955
			public CancellationToken cancellationToken;

			// Token: 0x040007A4 RID: 1956
			public MobileAuthenticatedStream <>4__this;

			// Token: 0x040007A5 RID: 1957
			public bool sync;

			// Token: 0x040007A6 RID: 1958
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
