﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Mono.Net.Security
{
	// Token: 0x0200005B RID: 91
	internal abstract class AsyncProtocolRequest
	{
		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000186 RID: 390 RVA: 0x00004C3E File Offset: 0x00002E3E
		public MobileAuthenticatedStream Parent
		{
			[CompilerGenerated]
			get
			{
				return this.<Parent>k__BackingField;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000187 RID: 391 RVA: 0x00004C46 File Offset: 0x00002E46
		public bool RunSynchronously
		{
			[CompilerGenerated]
			get
			{
				return this.<RunSynchronously>k__BackingField;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000188 RID: 392 RVA: 0x00004C4E File Offset: 0x00002E4E
		public int ID
		{
			get
			{
				return ++AsyncProtocolRequest.next_id;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000189 RID: 393 RVA: 0x00004C5D File Offset: 0x00002E5D
		public string Name
		{
			get
			{
				return base.GetType().Name;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600018A RID: 394 RVA: 0x00004C6A File Offset: 0x00002E6A
		// (set) Token: 0x0600018B RID: 395 RVA: 0x00004C72 File Offset: 0x00002E72
		public int UserResult
		{
			[CompilerGenerated]
			get
			{
				return this.<UserResult>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<UserResult>k__BackingField = value;
			}
		}

		// Token: 0x0600018C RID: 396 RVA: 0x00004C7B File Offset: 0x00002E7B
		public AsyncProtocolRequest(MobileAuthenticatedStream parent, bool sync)
		{
			this.Parent = parent;
			this.RunSynchronously = sync;
		}

		// Token: 0x0600018D RID: 397 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("MONO_TLS_DEBUG")]
		protected void Debug(string message, params object[] args)
		{
		}

		// Token: 0x0600018E RID: 398 RVA: 0x00004C9C File Offset: 0x00002E9C
		internal void RequestRead(int size)
		{
			object obj = this.locker;
			lock (obj)
			{
				this.RequestedSize += size;
			}
		}

		// Token: 0x0600018F RID: 399 RVA: 0x00004CE4 File Offset: 0x00002EE4
		internal void RequestWrite()
		{
			this.WriteRequested = 1;
		}

		// Token: 0x06000190 RID: 400 RVA: 0x00004CF0 File Offset: 0x00002EF0
		internal async Task<AsyncProtocolResult> StartOperation(CancellationToken cancellationToken)
		{
			if (Interlocked.CompareExchange(ref this.Started, 1, 0) != 0)
			{
				throw new InvalidOperationException();
			}
			AsyncProtocolResult result;
			try
			{
				await this.ProcessOperation(cancellationToken).ConfigureAwait(false);
				result = new AsyncProtocolResult(this.UserResult);
			}
			catch (Exception e)
			{
				result = new AsyncProtocolResult(this.Parent.SetException(MobileAuthenticatedStream.GetSSPIException(e)));
			}
			return result;
		}

		// Token: 0x06000191 RID: 401 RVA: 0x00004D40 File Offset: 0x00002F40
		private async Task ProcessOperation(CancellationToken cancellationToken)
		{
			AsyncOperationStatus newStatus;
			for (AsyncOperationStatus status = AsyncOperationStatus.Initialize; status != AsyncOperationStatus.Complete; status = newStatus)
			{
				cancellationToken.ThrowIfCancellationRequested();
				int? num = await this.InnerRead(cancellationToken).ConfigureAwait(false);
				if (num != null)
				{
					if (num == 0)
					{
						status = AsyncOperationStatus.ReadDone;
					}
					else if (num < 0)
					{
						throw new IOException("Remote prematurely closed connection.");
					}
				}
				if (status > AsyncOperationStatus.ReadDone)
				{
					throw new InvalidOperationException();
				}
				newStatus = this.Run(status);
				if (Interlocked.Exchange(ref this.WriteRequested, 0) != 0)
				{
					await this.Parent.InnerWrite(this.RunSynchronously, cancellationToken).ConfigureAwait(false);
				}
			}
		}

		// Token: 0x06000192 RID: 402 RVA: 0x00004D90 File Offset: 0x00002F90
		private async Task<int?> InnerRead(CancellationToken cancellationToken)
		{
			int? totalRead = null;
			int num2;
			for (int requestedSize = Interlocked.Exchange(ref this.RequestedSize, 0); requestedSize > 0; requestedSize += num2)
			{
				int num = await this.Parent.InnerRead(this.RunSynchronously, requestedSize, cancellationToken).ConfigureAwait(false);
				if (num <= 0)
				{
					return new int?(num);
				}
				if (num > requestedSize)
				{
					throw new InvalidOperationException();
				}
				totalRead += num;
				requestedSize -= num;
				num2 = Interlocked.Exchange(ref this.RequestedSize, 0);
			}
			return totalRead;
		}

		// Token: 0x06000193 RID: 403
		protected abstract AsyncOperationStatus Run(AsyncOperationStatus status);

		// Token: 0x06000194 RID: 404 RVA: 0x00004DDD File Offset: 0x00002FDD
		public override string ToString()
		{
			return string.Format("[{0}]", this.Name);
		}

		// Token: 0x0400074C RID: 1868
		[CompilerGenerated]
		private readonly MobileAuthenticatedStream <Parent>k__BackingField;

		// Token: 0x0400074D RID: 1869
		[CompilerGenerated]
		private readonly bool <RunSynchronously>k__BackingField;

		// Token: 0x0400074E RID: 1870
		[CompilerGenerated]
		private int <UserResult>k__BackingField;

		// Token: 0x0400074F RID: 1871
		private int Started;

		// Token: 0x04000750 RID: 1872
		private int RequestedSize;

		// Token: 0x04000751 RID: 1873
		private int WriteRequested;

		// Token: 0x04000752 RID: 1874
		private readonly object locker = new object();

		// Token: 0x04000753 RID: 1875
		private static int next_id;

		// Token: 0x0200005C RID: 92
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <StartOperation>d__23 : IAsyncStateMachine
		{
			// Token: 0x06000195 RID: 405 RVA: 0x00004DF0 File Offset: 0x00002FF0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				AsyncProtocolRequest asyncProtocolRequest = this;
				AsyncProtocolResult result;
				try
				{
					if (num != 0 && Interlocked.CompareExchange(ref asyncProtocolRequest.Started, 1, 0) != 0)
					{
						throw new InvalidOperationException();
					}
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							configuredTaskAwaiter = asyncProtocolRequest.ProcessOperation(cancellationToken).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, AsyncProtocolRequest.<StartOperation>d__23>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						result = new AsyncProtocolResult(asyncProtocolRequest.UserResult);
					}
					catch (Exception e)
					{
						result = new AsyncProtocolResult(asyncProtocolRequest.Parent.SetException(MobileAuthenticatedStream.GetSSPIException(e)));
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000196 RID: 406 RVA: 0x00004F04 File Offset: 0x00003104
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000754 RID: 1876
			public int <>1__state;

			// Token: 0x04000755 RID: 1877
			public AsyncTaskMethodBuilder<AsyncProtocolResult> <>t__builder;

			// Token: 0x04000756 RID: 1878
			public AsyncProtocolRequest <>4__this;

			// Token: 0x04000757 RID: 1879
			public CancellationToken cancellationToken;

			// Token: 0x04000758 RID: 1880
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200005D RID: 93
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ProcessOperation>d__24 : IAsyncStateMachine
		{
			// Token: 0x06000197 RID: 407 RVA: 0x00004F14 File Offset: 0x00003114
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				AsyncProtocolRequest asyncProtocolRequest = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int?>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num != 1)
						{
							status = AsyncOperationStatus.Initialize;
							goto IL_1A9;
						}
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_196;
					}
					else
					{
						ConfiguredTaskAwaitable<int?>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int?>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IL_93:
					int? result = configuredTaskAwaiter3.GetResult();
					if (result != null)
					{
						if (result == 0)
						{
							status = AsyncOperationStatus.ReadDone;
						}
						else if (result < 0)
						{
							throw new IOException("Remote prematurely closed connection.");
						}
					}
					AsyncOperationStatus asyncOperationStatus = status;
					if (asyncOperationStatus > AsyncOperationStatus.ReadDone)
					{
						throw new InvalidOperationException();
					}
					newStatus = asyncProtocolRequest.Run(status);
					if (Interlocked.Exchange(ref asyncProtocolRequest.WriteRequested, 0) == 0)
					{
						goto IL_19D;
					}
					configuredTaskAwaiter = asyncProtocolRequest.Parent.InnerWrite(asyncProtocolRequest.RunSynchronously, cancellationToken).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, AsyncProtocolRequest.<ProcessOperation>d__24>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_196:
					configuredTaskAwaiter.GetResult();
					IL_19D:
					status = newStatus;
					IL_1A9:
					if (status != AsyncOperationStatus.Complete)
					{
						cancellationToken.ThrowIfCancellationRequested();
						configuredTaskAwaiter3 = asyncProtocolRequest.InnerRead(cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int?>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int?>.ConfiguredTaskAwaiter, AsyncProtocolRequest.<ProcessOperation>d__24>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_93;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000198 RID: 408 RVA: 0x00005120 File Offset: 0x00003320
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000759 RID: 1881
			public int <>1__state;

			// Token: 0x0400075A RID: 1882
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400075B RID: 1883
			public CancellationToken cancellationToken;

			// Token: 0x0400075C RID: 1884
			public AsyncProtocolRequest <>4__this;

			// Token: 0x0400075D RID: 1885
			private AsyncOperationStatus <status>5__1;

			// Token: 0x0400075E RID: 1886
			private AsyncOperationStatus <newStatus>5__2;

			// Token: 0x0400075F RID: 1887
			private ConfiguredTaskAwaitable<int?>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000760 RID: 1888
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200005E RID: 94
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InnerRead>d__25 : IAsyncStateMachine
		{
			// Token: 0x06000199 RID: 409 RVA: 0x00005130 File Offset: 0x00003330
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				AsyncProtocolRequest asyncProtocolRequest = this;
				int? result2;
				try
				{
					if (num != 0)
					{
						totalRead = null;
						requestedSize = Interlocked.Exchange(ref asyncProtocolRequest.RequestedSize, 0);
						goto IL_133;
					}
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_AC:
					int result = configuredTaskAwaiter.GetResult();
					if (result <= 0)
					{
						result2 = new int?(result);
						goto IL_161;
					}
					if (result > requestedSize)
					{
						throw new InvalidOperationException();
					}
					totalRead += result;
					requestedSize -= result;
					int num3 = Interlocked.Exchange(ref asyncProtocolRequest.RequestedSize, 0);
					requestedSize += num3;
					IL_133:
					if (requestedSize <= 0)
					{
						result2 = totalRead;
					}
					else
					{
						configuredTaskAwaiter = asyncProtocolRequest.Parent.InnerRead(asyncProtocolRequest.RunSynchronously, requestedSize, cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, AsyncProtocolRequest.<InnerRead>d__25>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_AC;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_161:
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x0600019A RID: 410 RVA: 0x000052D0 File Offset: 0x000034D0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000761 RID: 1889
			public int <>1__state;

			// Token: 0x04000762 RID: 1890
			public AsyncTaskMethodBuilder<int?> <>t__builder;

			// Token: 0x04000763 RID: 1891
			public AsyncProtocolRequest <>4__this;

			// Token: 0x04000764 RID: 1892
			public CancellationToken cancellationToken;

			// Token: 0x04000765 RID: 1893
			private int <requestedSize>5__1;

			// Token: 0x04000766 RID: 1894
			private int? <totalRead>5__2;

			// Token: 0x04000767 RID: 1895
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
