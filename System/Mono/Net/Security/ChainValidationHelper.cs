﻿using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Mono.Net.Security.Private;
using Mono.Security.Interface;

namespace Mono.Net.Security
{
	// Token: 0x02000065 RID: 101
	internal class ChainValidationHelper : ICertificateValidator2, ICertificateValidator
	{
		// Token: 0x060001AC RID: 428 RVA: 0x00005489 File Offset: 0x00003689
		internal static ICertificateValidator GetInternalValidator(MonoTlsProvider provider, MonoTlsSettings settings)
		{
			if (settings == null)
			{
				return new ChainValidationHelper(provider, null, false, null, null);
			}
			if (settings.CertificateValidator != null)
			{
				return settings.CertificateValidator;
			}
			return new ChainValidationHelper(provider, settings, false, null, null);
		}

		// Token: 0x060001AD RID: 429 RVA: 0x000054B4 File Offset: 0x000036B4
		internal static ICertificateValidator GetDefaultValidator(MonoTlsSettings settings)
		{
			MonoTlsProvider monoTlsProvider = MonoTlsProviderFactory.GetProvider();
			if (settings == null)
			{
				return new ChainValidationHelper(monoTlsProvider, null, false, null, null);
			}
			if (settings.CertificateValidator != null)
			{
				throw new NotSupportedException();
			}
			return new ChainValidationHelper(monoTlsProvider, settings, false, null, null);
		}

		// Token: 0x060001AE RID: 430 RVA: 0x000054F0 File Offset: 0x000036F0
		internal static ChainValidationHelper CloneWithCallbackWrapper(MonoTlsProvider provider, ref MonoTlsSettings settings, ServerCertValidationCallbackWrapper wrapper)
		{
			ChainValidationHelper chainValidationHelper = (ChainValidationHelper)settings.CertificateValidator;
			if (chainValidationHelper == null)
			{
				chainValidationHelper = new ChainValidationHelper(provider, settings, true, null, wrapper);
			}
			else
			{
				chainValidationHelper = new ChainValidationHelper(chainValidationHelper, provider, settings, wrapper);
			}
			settings = chainValidationHelper.settings;
			return chainValidationHelper;
		}

		// Token: 0x060001AF RID: 431 RVA: 0x0000552F File Offset: 0x0000372F
		internal static bool InvokeCallback(ServerCertValidationCallback callback, object sender, X509Certificate certificate, X509Chain chain, MonoSslPolicyErrors sslPolicyErrors)
		{
			return callback.Invoke(sender, certificate, chain, (SslPolicyErrors)sslPolicyErrors);
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x0000553C File Offset: 0x0000373C
		private ChainValidationHelper(ChainValidationHelper other, MonoTlsProvider provider, MonoTlsSettings settings, ServerCertValidationCallbackWrapper callbackWrapper = null)
		{
			this.sender = other.sender;
			this.certValidationCallback = other.certValidationCallback;
			this.certSelectionCallback = other.certSelectionCallback;
			this.tlsStream = other.tlsStream;
			this.request = other.request;
			if (settings == null)
			{
				settings = MonoTlsSettings.DefaultSettings;
			}
			this.provider = provider;
			this.settings = settings.CloneWithValidator(this);
			this.callbackWrapper = callbackWrapper;
		}

		// Token: 0x060001B1 RID: 433 RVA: 0x000055B4 File Offset: 0x000037B4
		internal static ChainValidationHelper Create(MonoTlsProvider provider, ref MonoTlsSettings settings, MonoTlsStream stream)
		{
			ChainValidationHelper chainValidationHelper = new ChainValidationHelper(provider, settings, true, stream, null);
			settings = chainValidationHelper.settings;
			return chainValidationHelper;
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x000055D8 File Offset: 0x000037D8
		private ChainValidationHelper(MonoTlsProvider provider, MonoTlsSettings settings, bool cloneSettings, MonoTlsStream stream, ServerCertValidationCallbackWrapper callbackWrapper)
		{
			if (settings == null)
			{
				settings = MonoTlsSettings.CopyDefaultSettings();
			}
			if (cloneSettings)
			{
				settings = settings.CloneWithValidator(this);
			}
			if (provider == null)
			{
				provider = MonoTlsProviderFactory.GetProvider();
			}
			this.provider = provider;
			this.settings = settings;
			this.tlsStream = stream;
			this.callbackWrapper = callbackWrapper;
			bool flag = false;
			if (settings != null)
			{
				if (settings.RemoteCertificateValidationCallback != null)
				{
					RemoteCertificateValidationCallback validationCallback = CallbackHelpers.MonoToPublic(settings.RemoteCertificateValidationCallback);
					this.certValidationCallback = new ServerCertValidationCallback(validationCallback);
				}
				this.certSelectionCallback = CallbackHelpers.MonoToInternal(settings.ClientCertificateSelectionCallback);
				flag = (settings.UseServicePointManagerCallback ?? (stream != null));
			}
			if (stream != null)
			{
				this.request = stream.Request;
				this.sender = this.request;
				if (this.certValidationCallback == null)
				{
					this.certValidationCallback = this.request.ServerCertValidationCallback;
				}
				if (this.certSelectionCallback == null)
				{
					this.certSelectionCallback = new LocalCertSelectionCallback(ChainValidationHelper.DefaultSelectionCallback);
				}
				if (settings == null)
				{
					flag = true;
				}
			}
			if (flag && this.certValidationCallback == null)
			{
				this.certValidationCallback = ServicePointManager.ServerCertValidationCallback;
			}
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x000056EC File Offset: 0x000038EC
		private static X509Certificate DefaultSelectionCallback(string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
		{
			X509Certificate result;
			if (localCertificates == null || localCertificates.Count == 0)
			{
				result = null;
			}
			else
			{
				result = localCertificates[0];
			}
			return result;
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060001B4 RID: 436 RVA: 0x00005711 File Offset: 0x00003911
		public MonoTlsProvider Provider
		{
			get
			{
				return this.provider;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060001B5 RID: 437 RVA: 0x00005719 File Offset: 0x00003919
		public MonoTlsSettings Settings
		{
			get
			{
				return this.settings;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060001B6 RID: 438 RVA: 0x00005721 File Offset: 0x00003921
		public bool HasCertificateSelectionCallback
		{
			get
			{
				return this.certSelectionCallback != null;
			}
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x0000572C File Offset: 0x0000392C
		public bool SelectClientCertificate(string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers, out X509Certificate clientCertificate)
		{
			if (this.certSelectionCallback == null)
			{
				clientCertificate = null;
				return false;
			}
			clientCertificate = this.certSelectionCallback(targetHost, localCertificates, remoteCertificate, acceptableIssuers);
			return true;
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x00005750 File Offset: 0x00003950
		internal X509Certificate SelectClientCertificate(string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
		{
			if (this.certSelectionCallback == null)
			{
				return null;
			}
			return this.certSelectionCallback(targetHost, localCertificates, remoteCertificate, acceptableIssuers);
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x0000576C File Offset: 0x0000396C
		internal bool ValidateClientCertificate(X509Certificate certificate, MonoSslPolicyErrors errors)
		{
			X509CertificateCollection x509CertificateCollection = new X509CertificateCollection();
			x509CertificateCollection.Add(new X509Certificate2(certificate.GetRawCertData()));
			ValidationResult validationResult = this.ValidateChain(string.Empty, true, certificate, null, x509CertificateCollection, (SslPolicyErrors)errors);
			return validationResult != null && validationResult.Trusted && !validationResult.UserDenied;
		}

		// Token: 0x060001BA RID: 442 RVA: 0x000057BC File Offset: 0x000039BC
		public ValidationResult ValidateCertificate(string host, bool serverMode, X509CertificateCollection certs)
		{
			ValidationResult result;
			try
			{
				X509Certificate leaf;
				if (certs != null && certs.Count != 0)
				{
					leaf = certs[0];
				}
				else
				{
					leaf = null;
				}
				ValidationResult validationResult = this.ValidateChain(host, serverMode, leaf, null, certs, SslPolicyErrors.None);
				if (this.tlsStream != null)
				{
					this.tlsStream.CertificateValidationFailed = (validationResult == null || !validationResult.Trusted || validationResult.UserDenied);
				}
				result = validationResult;
			}
			catch
			{
				if (this.tlsStream != null)
				{
					this.tlsStream.CertificateValidationFailed = true;
				}
				throw;
			}
			return result;
		}

		// Token: 0x060001BB RID: 443 RVA: 0x00005840 File Offset: 0x00003A40
		public ValidationResult ValidateCertificate(string host, bool serverMode, X509Certificate leaf, X509Chain chain)
		{
			ValidationResult result;
			try
			{
				ValidationResult validationResult = this.ValidateChain(host, serverMode, leaf, chain, null, SslPolicyErrors.None);
				if (this.tlsStream != null)
				{
					this.tlsStream.CertificateValidationFailed = (validationResult == null || !validationResult.Trusted || validationResult.UserDenied);
				}
				result = validationResult;
			}
			catch
			{
				if (this.tlsStream != null)
				{
					this.tlsStream.CertificateValidationFailed = true;
				}
				throw;
			}
			return result;
		}

		// Token: 0x060001BC RID: 444 RVA: 0x000058B0 File Offset: 0x00003AB0
		private ValidationResult ValidateChain(string host, bool server, X509Certificate leaf, X509Chain chain, X509CertificateCollection certs, SslPolicyErrors errors)
		{
			X509Chain x509Chain = chain;
			bool flag = chain == null;
			ValidationResult result;
			try
			{
				ValidationResult validationResult = this.ValidateChain(host, server, leaf, ref chain, certs, errors);
				if (chain != x509Chain)
				{
					flag = true;
				}
				result = validationResult;
			}
			finally
			{
				if (flag && chain != null)
				{
					chain.Dispose();
				}
			}
			return result;
		}

		// Token: 0x060001BD RID: 445 RVA: 0x00005900 File Offset: 0x00003B00
		private ValidationResult ValidateChain(string host, bool server, X509Certificate leaf, ref X509Chain chain, X509CertificateCollection certs, SslPolicyErrors errors)
		{
			bool user_denied = false;
			bool flag = false;
			bool flag2 = this.certValidationCallback != null || this.callbackWrapper != null;
			if (this.tlsStream != null)
			{
				this.request.ServicePoint.UpdateServerCertificate(leaf);
			}
			if (leaf == null)
			{
				errors |= SslPolicyErrors.RemoteCertificateNotAvailable;
				if (flag2)
				{
					if (this.callbackWrapper != null)
					{
						flag = this.callbackWrapper(this.certValidationCallback, leaf, null, (MonoSslPolicyErrors)errors);
					}
					else
					{
						flag = this.certValidationCallback.Invoke(this.sender, leaf, null, errors);
					}
					user_denied = !flag;
				}
				return new ValidationResult(flag, user_denied, 0, new MonoSslPolicyErrors?((MonoSslPolicyErrors)errors));
			}
			if (!string.IsNullOrEmpty(host))
			{
				int num = host.IndexOf(':');
				if (num > 0)
				{
					host = host.Substring(0, num);
				}
			}
			ICertificatePolicy legacyCertificatePolicy = ServicePointManager.GetLegacyCertificatePolicy();
			int num2 = 0;
			bool flag3 = SystemCertificateValidator.NeedsChain(this.settings);
			if (!flag3 && flag2 && (this.settings == null || this.settings.CallbackNeedsCertificateChain))
			{
				flag3 = true;
			}
			MonoSslPolicyErrors monoSslPolicyErrors = (MonoSslPolicyErrors)errors;
			flag = this.provider.ValidateCertificate(this, host, server, certs, flag3, ref chain, ref monoSslPolicyErrors, ref num2);
			errors = (SslPolicyErrors)monoSslPolicyErrors;
			if (num2 == 0 && errors != SslPolicyErrors.None)
			{
				num2 = -2146762485;
			}
			if (legacyCertificatePolicy != null && (!(legacyCertificatePolicy is DefaultCertificatePolicy) || this.certValidationCallback == null))
			{
				ServicePoint srvPoint = null;
				if (this.request != null)
				{
					srvPoint = this.request.ServicePointNoLock;
				}
				flag = legacyCertificatePolicy.CheckValidationResult(srvPoint, leaf, this.request, num2);
				user_denied = (!flag && !(legacyCertificatePolicy is DefaultCertificatePolicy));
			}
			if (flag2)
			{
				if (this.callbackWrapper != null)
				{
					flag = this.callbackWrapper(this.certValidationCallback, leaf, chain, (MonoSslPolicyErrors)errors);
				}
				else
				{
					flag = this.certValidationCallback.Invoke(this.sender, leaf, chain, errors);
				}
				user_denied = !flag;
			}
			return new ValidationResult(flag, user_denied, num2, new MonoSslPolicyErrors?((MonoSslPolicyErrors)errors));
		}

		// Token: 0x060001BE RID: 446 RVA: 0x00005AC4 File Offset: 0x00003CC4
		private bool InvokeSystemValidator(string targetHost, bool serverMode, X509CertificateCollection certificates, X509Chain chain, ref MonoSslPolicyErrors xerrors, ref int status11)
		{
			SslPolicyErrors sslPolicyErrors = (SslPolicyErrors)xerrors;
			bool result = SystemCertificateValidator.Evaluate(this.settings, targetHost, certificates, chain, ref sslPolicyErrors, ref status11);
			xerrors = (MonoSslPolicyErrors)sslPolicyErrors;
			return result;
		}

		// Token: 0x0400076A RID: 1898
		private readonly object sender;

		// Token: 0x0400076B RID: 1899
		private readonly MonoTlsSettings settings;

		// Token: 0x0400076C RID: 1900
		private readonly MonoTlsProvider provider;

		// Token: 0x0400076D RID: 1901
		private readonly ServerCertValidationCallback certValidationCallback;

		// Token: 0x0400076E RID: 1902
		private readonly LocalCertSelectionCallback certSelectionCallback;

		// Token: 0x0400076F RID: 1903
		private readonly ServerCertValidationCallbackWrapper callbackWrapper;

		// Token: 0x04000770 RID: 1904
		private readonly MonoTlsStream tlsStream;

		// Token: 0x04000771 RID: 1905
		private readonly HttpWebRequest request;
	}
}
