﻿using System;
using System.IO;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using Mono.Net.Security.Private;
using Mono.Security.Interface;

namespace Mono.Net.Security
{
	// Token: 0x02000066 RID: 102
	internal class LegacyTlsProvider : MonoTlsProvider
	{
		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060001BF RID: 447 RVA: 0x00005AEC File Offset: 0x00003CEC
		public override Guid ID
		{
			get
			{
				return MonoTlsProviderFactory.LegacyId;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060001C0 RID: 448 RVA: 0x00005AF3 File Offset: 0x00003CF3
		public override string Name
		{
			get
			{
				return "legacy";
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060001C1 RID: 449 RVA: 0x00003298 File Offset: 0x00001498
		public override bool SupportsSslStream
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060001C2 RID: 450 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool SupportsConnectionInfo
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060001C3 RID: 451 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool SupportsMonoExtensions
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060001C4 RID: 452 RVA: 0x00005AFA File Offset: 0x00003CFA
		internal override bool SupportsCleanShutdown
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060001C5 RID: 453 RVA: 0x00005AFD File Offset: 0x00003CFD
		public override SslProtocols SupportedProtocols
		{
			get
			{
				return SslProtocols.Tls;
			}
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x000032A2 File Offset: 0x000014A2
		public override IMonoSslStream CreateSslStream(Stream innerStream, bool leaveInnerStreamOpen, MonoTlsSettings settings = null)
		{
			return SslStream.CreateMonoSslStream(innerStream, leaveInnerStreamOpen, this, settings);
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x00005B04 File Offset: 0x00003D04
		internal override IMonoSslStream CreateSslStreamInternal(SslStream sslStream, Stream innerStream, bool leaveInnerStreamOpen, MonoTlsSettings settings)
		{
			return new LegacySslStream(innerStream, leaveInnerStreamOpen, sslStream, this, settings);
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x00005B14 File Offset: 0x00003D14
		internal override bool ValidateCertificate(ICertificateValidator2 validator, string targetHost, bool serverMode, X509CertificateCollection certificates, bool wantsChain, ref X509Chain chain, ref MonoSslPolicyErrors errors, ref int status11)
		{
			if (wantsChain)
			{
				chain = SystemCertificateValidator.CreateX509Chain(certificates);
			}
			SslPolicyErrors sslPolicyErrors = (SslPolicyErrors)errors;
			bool result = SystemCertificateValidator.Evaluate(validator.Settings, targetHost, certificates, chain, ref sslPolicyErrors, ref status11);
			errors = (MonoSslPolicyErrors)sslPolicyErrors;
			return result;
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x000034D8 File Offset: 0x000016D8
		public LegacyTlsProvider()
		{
		}
	}
}
