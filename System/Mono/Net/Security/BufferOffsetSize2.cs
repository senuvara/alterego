﻿using System;

namespace Mono.Net.Security
{
	// Token: 0x02000058 RID: 88
	internal class BufferOffsetSize2 : BufferOffsetSize
	{
		// Token: 0x0600017E RID: 382 RVA: 0x00004B31 File Offset: 0x00002D31
		public BufferOffsetSize2(int size) : base(new byte[size], 0, 0)
		{
			this.InitialSize = size;
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00004B48 File Offset: 0x00002D48
		public void Reset()
		{
			this.Offset = (this.Size = 0);
			this.TotalBytes = 0;
			this.Buffer = new byte[this.InitialSize];
			this.Complete = false;
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00004B84 File Offset: 0x00002D84
		public void MakeRoom(int size)
		{
			if (base.Remaining >= size)
			{
				return;
			}
			int num = size - base.Remaining;
			if (this.Offset == 0 && this.Size == 0)
			{
				this.Buffer = new byte[size];
				return;
			}
			byte[] array = new byte[this.Buffer.Length + num];
			this.Buffer.CopyTo(array, 0);
			this.Buffer = array;
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00004BE5 File Offset: 0x00002DE5
		public void AppendData(byte[] buffer, int offset, int size)
		{
			this.MakeRoom(size);
			System.Buffer.BlockCopy(buffer, offset, this.Buffer, base.EndOffset, size);
			this.Size += size;
		}

		// Token: 0x04000744 RID: 1860
		public readonly int InitialSize;
	}
}
