﻿using System;

namespace Mono.Net.Security
{
	// Token: 0x0200005F RID: 95
	internal class AsyncHandshakeRequest : AsyncProtocolRequest
	{
		// Token: 0x0600019B RID: 411 RVA: 0x000052DE File Offset: 0x000034DE
		public AsyncHandshakeRequest(MobileAuthenticatedStream parent, bool sync) : base(parent, sync)
		{
		}

		// Token: 0x0600019C RID: 412 RVA: 0x000052E8 File Offset: 0x000034E8
		protected override AsyncOperationStatus Run(AsyncOperationStatus status)
		{
			return base.Parent.ProcessHandshake(status);
		}
	}
}
