﻿using System;

namespace Mono.Net.Security
{
	// Token: 0x02000057 RID: 87
	internal class BufferOffsetSize
	{
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600017A RID: 378 RVA: 0x00004A84 File Offset: 0x00002C84
		public int EndOffset
		{
			get
			{
				return this.Offset + this.Size;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600017B RID: 379 RVA: 0x00004A93 File Offset: 0x00002C93
		public int Remaining
		{
			get
			{
				return this.Buffer.Length - this.Offset - this.Size;
			}
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00004AAC File Offset: 0x00002CAC
		public BufferOffsetSize(byte[] buffer, int offset, int size)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (size < 0 || offset + size > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("size");
			}
			this.Buffer = buffer;
			this.Offset = offset;
			this.Size = size;
			this.Complete = false;
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00004B0F File Offset: 0x00002D0F
		public override string ToString()
		{
			return string.Format("[BufferOffsetSize: {0} {1}]", this.Offset, this.Size);
		}

		// Token: 0x0400073F RID: 1855
		public byte[] Buffer;

		// Token: 0x04000740 RID: 1856
		public int Offset;

		// Token: 0x04000741 RID: 1857
		public int Size;

		// Token: 0x04000742 RID: 1858
		public int TotalBytes;

		// Token: 0x04000743 RID: 1859
		public bool Complete;
	}
}
