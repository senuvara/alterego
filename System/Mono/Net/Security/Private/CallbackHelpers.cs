﻿using System;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using Mono.Security.Interface;

namespace Mono.Net.Security.Private
{
	// Token: 0x02000073 RID: 115
	internal static class CallbackHelpers
	{
		// Token: 0x06000277 RID: 631 RVA: 0x0000817C File Offset: 0x0000637C
		internal static MonoRemoteCertificateValidationCallback PublicToMono(RemoteCertificateValidationCallback callback)
		{
			if (callback == null)
			{
				return null;
			}
			return (string h, X509Certificate c, X509Chain ch, MonoSslPolicyErrors e) => callback(h, c, ch, (SslPolicyErrors)e);
		}

		// Token: 0x06000278 RID: 632 RVA: 0x000081AC File Offset: 0x000063AC
		internal static MonoLocalCertificateSelectionCallback PublicToMono(LocalCertificateSelectionCallback callback)
		{
			if (callback == null)
			{
				return null;
			}
			return (string t, X509CertificateCollection lc, X509Certificate rc, string[] ai) => callback(null, t, lc, rc, ai);
		}

		// Token: 0x06000279 RID: 633 RVA: 0x000081DC File Offset: 0x000063DC
		internal static MonoRemoteCertificateValidationCallback InternalToMono(RemoteCertValidationCallback callback)
		{
			if (callback == null)
			{
				return null;
			}
			return (string h, X509Certificate c, X509Chain ch, MonoSslPolicyErrors e) => callback(h, c, ch, (SslPolicyErrors)e);
		}

		// Token: 0x0600027A RID: 634 RVA: 0x0000820C File Offset: 0x0000640C
		internal static RemoteCertificateValidationCallback InternalToPublic(string hostname, RemoteCertValidationCallback callback)
		{
			if (callback == null)
			{
				return null;
			}
			return (object s, X509Certificate c, X509Chain ch, SslPolicyErrors e) => callback(hostname, c, ch, e);
		}

		// Token: 0x0600027B RID: 635 RVA: 0x00008244 File Offset: 0x00006444
		internal static MonoLocalCertificateSelectionCallback InternalToMono(LocalCertSelectionCallback callback)
		{
			if (callback == null)
			{
				return null;
			}
			return (string t, X509CertificateCollection lc, X509Certificate rc, string[] ai) => callback(t, lc, rc, ai);
		}

		// Token: 0x0600027C RID: 636 RVA: 0x00008274 File Offset: 0x00006474
		internal static RemoteCertificateValidationCallback MonoToPublic(MonoRemoteCertificateValidationCallback callback)
		{
			if (callback == null)
			{
				return null;
			}
			return (object t, X509Certificate c, X509Chain ch, SslPolicyErrors e) => callback(null, c, ch, (MonoSslPolicyErrors)e);
		}

		// Token: 0x0600027D RID: 637 RVA: 0x000082A4 File Offset: 0x000064A4
		internal static LocalCertificateSelectionCallback MonoToPublic(MonoLocalCertificateSelectionCallback callback)
		{
			if (callback == null)
			{
				return null;
			}
			return (object s, string t, X509CertificateCollection lc, X509Certificate rc, string[] ai) => callback(t, lc, rc, ai);
		}

		// Token: 0x0600027E RID: 638 RVA: 0x000082D4 File Offset: 0x000064D4
		internal static RemoteCertValidationCallback MonoToInternal(MonoRemoteCertificateValidationCallback callback)
		{
			if (callback == null)
			{
				return null;
			}
			return (string h, X509Certificate c, X509Chain ch, SslPolicyErrors e) => callback(h, c, ch, (MonoSslPolicyErrors)e);
		}

		// Token: 0x0600027F RID: 639 RVA: 0x00008304 File Offset: 0x00006504
		internal static LocalCertSelectionCallback MonoToInternal(MonoLocalCertificateSelectionCallback callback)
		{
			if (callback == null)
			{
				return null;
			}
			return (string t, X509CertificateCollection lc, X509Certificate rc, string[] ai) => callback(t, lc, rc, ai);
		}

		// Token: 0x02000074 RID: 116
		[CompilerGenerated]
		private sealed class <>c__DisplayClass0_0
		{
			// Token: 0x06000280 RID: 640 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass0_0()
			{
			}

			// Token: 0x06000281 RID: 641 RVA: 0x00008334 File Offset: 0x00006534
			internal bool <PublicToMono>b__0(string h, X509Certificate c, X509Chain ch, MonoSslPolicyErrors e)
			{
				return this.callback(h, c, ch, (SslPolicyErrors)e);
			}

			// Token: 0x040007C3 RID: 1987
			public RemoteCertificateValidationCallback callback;
		}

		// Token: 0x02000075 RID: 117
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1_0
		{
			// Token: 0x06000282 RID: 642 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass1_0()
			{
			}

			// Token: 0x06000283 RID: 643 RVA: 0x00008346 File Offset: 0x00006546
			internal X509Certificate <PublicToMono>b__0(string t, X509CertificateCollection lc, X509Certificate rc, string[] ai)
			{
				return this.callback(null, t, lc, rc, ai);
			}

			// Token: 0x040007C4 RID: 1988
			public LocalCertificateSelectionCallback callback;
		}

		// Token: 0x02000076 RID: 118
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2_0
		{
			// Token: 0x06000284 RID: 644 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass2_0()
			{
			}

			// Token: 0x06000285 RID: 645 RVA: 0x00008359 File Offset: 0x00006559
			internal bool <InternalToMono>b__0(string h, X509Certificate c, X509Chain ch, MonoSslPolicyErrors e)
			{
				return this.callback(h, c, ch, (SslPolicyErrors)e);
			}

			// Token: 0x040007C5 RID: 1989
			public RemoteCertValidationCallback callback;
		}

		// Token: 0x02000077 RID: 119
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x06000286 RID: 646 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x06000287 RID: 647 RVA: 0x0000836B File Offset: 0x0000656B
			internal bool <InternalToPublic>b__0(object s, X509Certificate c, X509Chain ch, SslPolicyErrors e)
			{
				return this.callback(this.hostname, c, ch, e);
			}

			// Token: 0x040007C6 RID: 1990
			public RemoteCertValidationCallback callback;

			// Token: 0x040007C7 RID: 1991
			public string hostname;
		}

		// Token: 0x02000078 RID: 120
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_0
		{
			// Token: 0x06000288 RID: 648 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass4_0()
			{
			}

			// Token: 0x06000289 RID: 649 RVA: 0x00008382 File Offset: 0x00006582
			internal X509Certificate <InternalToMono>b__0(string t, X509CertificateCollection lc, X509Certificate rc, string[] ai)
			{
				return this.callback(t, lc, rc, ai);
			}

			// Token: 0x040007C8 RID: 1992
			public LocalCertSelectionCallback callback;
		}

		// Token: 0x02000079 RID: 121
		[CompilerGenerated]
		private sealed class <>c__DisplayClass5_0
		{
			// Token: 0x0600028A RID: 650 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass5_0()
			{
			}

			// Token: 0x0600028B RID: 651 RVA: 0x00008394 File Offset: 0x00006594
			internal bool <MonoToPublic>b__0(object t, X509Certificate c, X509Chain ch, SslPolicyErrors e)
			{
				return this.callback(null, c, ch, (MonoSslPolicyErrors)e);
			}

			// Token: 0x040007C9 RID: 1993
			public MonoRemoteCertificateValidationCallback callback;
		}

		// Token: 0x0200007A RID: 122
		[CompilerGenerated]
		private sealed class <>c__DisplayClass6_0
		{
			// Token: 0x0600028C RID: 652 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass6_0()
			{
			}

			// Token: 0x0600028D RID: 653 RVA: 0x000083A6 File Offset: 0x000065A6
			internal X509Certificate <MonoToPublic>b__0(object s, string t, X509CertificateCollection lc, X509Certificate rc, string[] ai)
			{
				return this.callback(t, lc, rc, ai);
			}

			// Token: 0x040007CA RID: 1994
			public MonoLocalCertificateSelectionCallback callback;
		}

		// Token: 0x0200007B RID: 123
		[CompilerGenerated]
		private sealed class <>c__DisplayClass7_0
		{
			// Token: 0x0600028E RID: 654 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass7_0()
			{
			}

			// Token: 0x0600028F RID: 655 RVA: 0x000083B9 File Offset: 0x000065B9
			internal bool <MonoToInternal>b__0(string h, X509Certificate c, X509Chain ch, SslPolicyErrors e)
			{
				return this.callback(h, c, ch, (MonoSslPolicyErrors)e);
			}

			// Token: 0x040007CB RID: 1995
			public MonoRemoteCertificateValidationCallback callback;
		}

		// Token: 0x0200007C RID: 124
		[CompilerGenerated]
		private sealed class <>c__DisplayClass8_0
		{
			// Token: 0x06000290 RID: 656 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass8_0()
			{
			}

			// Token: 0x06000291 RID: 657 RVA: 0x000083CB File Offset: 0x000065CB
			internal X509Certificate <MonoToInternal>b__0(string t, X509CertificateCollection lc, X509Certificate rc, string[] ai)
			{
				return this.callback(t, lc, rc, ai);
			}

			// Token: 0x040007CC RID: 1996
			public MonoLocalCertificateSelectionCallback callback;
		}
	}
}
