﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Mono.Security.Interface;
using Mono.Security.Protocol.Tls;
using Mono.Security.X509;

namespace Mono.Net.Security.Private
{
	// Token: 0x0200007D RID: 125
	[MonoTODO("Non-X509Certificate2 certificate is not supported")]
	internal class LegacySslStream : AuthenticatedStream, IMonoSslStream, IDisposable
	{
		// Token: 0x06000292 RID: 658 RVA: 0x000083DD File Offset: 0x000065DD
		public LegacySslStream(Stream innerStream, bool leaveInnerStreamOpen, SslStream owner, MonoTlsProvider provider, MonoTlsSettings settings) : base(innerStream, leaveInnerStreamOpen)
		{
			this.SslStream = owner;
			this.Provider = provider;
			this.certificateValidator = ChainValidationHelper.GetInternalValidator(provider, settings);
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000293 RID: 659 RVA: 0x00008405 File Offset: 0x00006605
		public override bool CanRead
		{
			get
			{
				return base.InnerStream.CanRead;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000294 RID: 660 RVA: 0x00008412 File Offset: 0x00006612
		public override bool CanSeek
		{
			get
			{
				return base.InnerStream.CanSeek;
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000295 RID: 661 RVA: 0x0000676C File Offset: 0x0000496C
		public override bool CanTimeout
		{
			get
			{
				return base.InnerStream.CanTimeout;
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000296 RID: 662 RVA: 0x0000841F File Offset: 0x0000661F
		public override bool CanWrite
		{
			get
			{
				return base.InnerStream.CanWrite;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000297 RID: 663 RVA: 0x0000679A File Offset: 0x0000499A
		public override long Length
		{
			get
			{
				return base.InnerStream.Length;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000298 RID: 664 RVA: 0x000067A7 File Offset: 0x000049A7
		// (set) Token: 0x06000299 RID: 665 RVA: 0x0000842C File Offset: 0x0000662C
		public override long Position
		{
			get
			{
				return base.InnerStream.Position;
			}
			set
			{
				throw new NotSupportedException("This stream does not support seek operations");
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600029A RID: 666 RVA: 0x00008438 File Offset: 0x00006638
		public override bool IsAuthenticated
		{
			get
			{
				return this.ssl_stream != null;
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x0600029B RID: 667 RVA: 0x000067B4 File Offset: 0x000049B4
		public override bool IsEncrypted
		{
			get
			{
				return this.IsAuthenticated;
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x0600029C RID: 668 RVA: 0x00008443 File Offset: 0x00006643
		public override bool IsMutuallyAuthenticated
		{
			get
			{
				if (!this.IsAuthenticated)
				{
					return false;
				}
				if (!this.IsServer)
				{
					return this.LocalCertificate != null;
				}
				return this.RemoteCertificate != null;
			}
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x0600029D RID: 669 RVA: 0x0000846A File Offset: 0x0000666A
		public override bool IsServer
		{
			get
			{
				return this.ssl_stream is SslServerStream;
			}
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x0600029E RID: 670 RVA: 0x000067B4 File Offset: 0x000049B4
		public override bool IsSigned
		{
			get
			{
				return this.IsAuthenticated;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x0600029F RID: 671 RVA: 0x000067BC File Offset: 0x000049BC
		// (set) Token: 0x060002A0 RID: 672 RVA: 0x000067C9 File Offset: 0x000049C9
		public override int ReadTimeout
		{
			get
			{
				return base.InnerStream.ReadTimeout;
			}
			set
			{
				base.InnerStream.ReadTimeout = value;
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x060002A1 RID: 673 RVA: 0x000067D7 File Offset: 0x000049D7
		// (set) Token: 0x060002A2 RID: 674 RVA: 0x000067E4 File Offset: 0x000049E4
		public override int WriteTimeout
		{
			get
			{
				return base.InnerStream.WriteTimeout;
			}
			set
			{
				base.InnerStream.WriteTimeout = value;
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x060002A3 RID: 675 RVA: 0x0000847A File Offset: 0x0000667A
		public virtual bool CheckCertRevocationStatus
		{
			get
			{
				return this.IsAuthenticated && this.ssl_stream.CheckCertRevocationStatus;
			}
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x060002A4 RID: 676 RVA: 0x00008494 File Offset: 0x00006694
		public virtual System.Security.Authentication.CipherAlgorithmType CipherAlgorithm
		{
			get
			{
				this.CheckConnectionAuthenticated();
				switch (this.ssl_stream.CipherAlgorithm)
				{
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.Des:
					return System.Security.Authentication.CipherAlgorithmType.Des;
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.None:
					return System.Security.Authentication.CipherAlgorithmType.None;
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.Rc2:
					return System.Security.Authentication.CipherAlgorithmType.Rc2;
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.Rc4:
					return System.Security.Authentication.CipherAlgorithmType.Rc4;
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.Rijndael:
				{
					int cipherStrength = this.ssl_stream.CipherStrength;
					if (cipherStrength == 128)
					{
						return System.Security.Authentication.CipherAlgorithmType.Aes128;
					}
					if (cipherStrength == 192)
					{
						return System.Security.Authentication.CipherAlgorithmType.Aes192;
					}
					if (cipherStrength == 256)
					{
						return System.Security.Authentication.CipherAlgorithmType.Aes256;
					}
					break;
				}
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.TripleDes:
					return System.Security.Authentication.CipherAlgorithmType.TripleDes;
				}
				throw new InvalidOperationException("Not supported cipher algorithm is in use. It is likely a bug in SslStream.");
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x060002A5 RID: 677 RVA: 0x00008533 File Offset: 0x00006733
		public virtual int CipherStrength
		{
			get
			{
				this.CheckConnectionAuthenticated();
				return this.ssl_stream.CipherStrength;
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x060002A6 RID: 678 RVA: 0x00008548 File Offset: 0x00006748
		public virtual System.Security.Authentication.HashAlgorithmType HashAlgorithm
		{
			get
			{
				this.CheckConnectionAuthenticated();
				switch (this.ssl_stream.HashAlgorithm)
				{
				case Mono.Security.Protocol.Tls.HashAlgorithmType.Md5:
					return System.Security.Authentication.HashAlgorithmType.Md5;
				case Mono.Security.Protocol.Tls.HashAlgorithmType.None:
					return System.Security.Authentication.HashAlgorithmType.None;
				case Mono.Security.Protocol.Tls.HashAlgorithmType.Sha1:
					return System.Security.Authentication.HashAlgorithmType.Sha1;
				default:
					throw new InvalidOperationException("Not supported hash algorithm is in use. It is likely a bug in SslStream.");
				}
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x060002A7 RID: 679 RVA: 0x00008593 File Offset: 0x00006793
		public virtual int HashStrength
		{
			get
			{
				this.CheckConnectionAuthenticated();
				return this.ssl_stream.HashStrength;
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x060002A8 RID: 680 RVA: 0x000085A8 File Offset: 0x000067A8
		public virtual System.Security.Authentication.ExchangeAlgorithmType KeyExchangeAlgorithm
		{
			get
			{
				this.CheckConnectionAuthenticated();
				switch (this.ssl_stream.KeyExchangeAlgorithm)
				{
				case Mono.Security.Protocol.Tls.ExchangeAlgorithmType.DiffieHellman:
					return System.Security.Authentication.ExchangeAlgorithmType.DiffieHellman;
				case Mono.Security.Protocol.Tls.ExchangeAlgorithmType.None:
					return System.Security.Authentication.ExchangeAlgorithmType.None;
				case Mono.Security.Protocol.Tls.ExchangeAlgorithmType.RsaKeyX:
					return System.Security.Authentication.ExchangeAlgorithmType.RsaKeyX;
				case Mono.Security.Protocol.Tls.ExchangeAlgorithmType.RsaSign:
					return System.Security.Authentication.ExchangeAlgorithmType.RsaSign;
				}
				throw new InvalidOperationException("Not supported exchange algorithm is in use. It is likely a bug in SslStream.");
			}
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x060002A9 RID: 681 RVA: 0x00008601 File Offset: 0x00006801
		public virtual int KeyExchangeStrength
		{
			get
			{
				this.CheckConnectionAuthenticated();
				return this.ssl_stream.KeyExchangeStrength;
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x060002AA RID: 682 RVA: 0x00008614 File Offset: 0x00006814
		System.Security.Cryptography.X509Certificates.X509Certificate IMonoSslStream.InternalLocalCertificate
		{
			get
			{
				if (!this.IsServer)
				{
					return ((SslClientStream)this.ssl_stream).SelectedClientCertificate;
				}
				return this.ssl_stream.ServerCertificate;
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x060002AB RID: 683 RVA: 0x0000863A File Offset: 0x0000683A
		public virtual System.Security.Cryptography.X509Certificates.X509Certificate LocalCertificate
		{
			get
			{
				this.CheckConnectionAuthenticated();
				if (!this.IsServer)
				{
					return ((SslClientStream)this.ssl_stream).SelectedClientCertificate;
				}
				return this.ssl_stream.ServerCertificate;
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x060002AC RID: 684 RVA: 0x00008666 File Offset: 0x00006866
		public virtual System.Security.Cryptography.X509Certificates.X509Certificate RemoteCertificate
		{
			get
			{
				this.CheckConnectionAuthenticated();
				if (this.IsServer)
				{
					return ((SslServerStream)this.ssl_stream).ClientCertificate;
				}
				return this.ssl_stream.ServerCertificate;
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x060002AD RID: 685 RVA: 0x00008694 File Offset: 0x00006894
		public virtual SslProtocols SslProtocol
		{
			get
			{
				this.CheckConnectionAuthenticated();
				Mono.Security.Protocol.Tls.SecurityProtocolType securityProtocol = this.ssl_stream.SecurityProtocol;
				if (securityProtocol <= Mono.Security.Protocol.Tls.SecurityProtocolType.Ssl2)
				{
					if (securityProtocol == Mono.Security.Protocol.Tls.SecurityProtocolType.Default)
					{
						return SslProtocols.Default;
					}
					if (securityProtocol == Mono.Security.Protocol.Tls.SecurityProtocolType.Ssl2)
					{
						return SslProtocols.Ssl2;
					}
				}
				else
				{
					if (securityProtocol == Mono.Security.Protocol.Tls.SecurityProtocolType.Ssl3)
					{
						return SslProtocols.Ssl3;
					}
					if (securityProtocol == Mono.Security.Protocol.Tls.SecurityProtocolType.Tls)
					{
						return SslProtocols.Tls;
					}
				}
				throw new InvalidOperationException("Not supported SSL/TLS protocol is in use. It is likely a bug in SslStream.");
			}
		}

		// Token: 0x060002AE RID: 686 RVA: 0x000086F4 File Offset: 0x000068F4
		private System.Security.Cryptography.X509Certificates.X509Certificate OnCertificateSelection(System.Security.Cryptography.X509Certificates.X509CertificateCollection clientCerts, System.Security.Cryptography.X509Certificates.X509Certificate serverCert, string targetHost, System.Security.Cryptography.X509Certificates.X509CertificateCollection serverRequestedCerts)
		{
			string[] array = new string[(serverRequestedCerts != null) ? serverRequestedCerts.Count : 0];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = serverRequestedCerts[i].GetIssuerName();
			}
			System.Security.Cryptography.X509Certificates.X509Certificate result;
			this.certificateValidator.SelectClientCertificate(targetHost, clientCerts, serverCert, array, out result);
			return result;
		}

		// Token: 0x060002AF RID: 687 RVA: 0x00008746 File Offset: 0x00006946
		public virtual IAsyncResult BeginAuthenticateAsClient(string targetHost, AsyncCallback asyncCallback, object asyncState)
		{
			return this.BeginAuthenticateAsClient(targetHost, new System.Security.Cryptography.X509Certificates.X509CertificateCollection(), SslProtocols.Tls, false, asyncCallback, asyncState);
		}

		// Token: 0x060002B0 RID: 688 RVA: 0x0000875C File Offset: 0x0000695C
		public virtual IAsyncResult BeginAuthenticateAsClient(string targetHost, System.Security.Cryptography.X509Certificates.X509CertificateCollection clientCertificates, SslProtocols enabledSslProtocols, bool checkCertificateRevocation, AsyncCallback asyncCallback, object asyncState)
		{
			if (this.IsAuthenticated)
			{
				throw new InvalidOperationException("This SslStream is already authenticated");
			}
			SslClientStream sslClientStream = new SslClientStream(base.InnerStream, targetHost, !base.LeaveInnerStreamOpen, this.GetMonoSslProtocol(enabledSslProtocols), clientCertificates);
			sslClientStream.CheckCertRevocationStatus = checkCertificateRevocation;
			sslClientStream.PrivateKeyCertSelectionDelegate = delegate(System.Security.Cryptography.X509Certificates.X509Certificate cert, string host)
			{
				string certHashString = cert.GetCertHashString();
				foreach (System.Security.Cryptography.X509Certificates.X509Certificate x509Certificate in clientCertificates)
				{
					if (!(x509Certificate.GetCertHashString() != certHashString))
					{
						return ((x509Certificate as X509Certificate2) ?? new X509Certificate2(x509Certificate)).PrivateKey;
					}
				}
				return null;
			};
			sslClientStream.ServerCertValidation2 += delegate(Mono.Security.X509.X509CertificateCollection mcerts)
			{
				System.Security.Cryptography.X509Certificates.X509CertificateCollection x509CertificateCollection = null;
				if (mcerts != null)
				{
					x509CertificateCollection = new System.Security.Cryptography.X509Certificates.X509CertificateCollection();
					for (int i = 0; i < mcerts.Count; i++)
					{
						x509CertificateCollection.Add(new X509Certificate2(mcerts[i].RawData));
					}
				}
				return ((ChainValidationHelper)this.certificateValidator).ValidateCertificate(targetHost, false, x509CertificateCollection);
			};
			sslClientStream.ClientCertSelectionDelegate = new CertificateSelectionCallback(this.OnCertificateSelection);
			this.ssl_stream = sslClientStream;
			return this.BeginWrite(new byte[0], 0, 0, asyncCallback, asyncState);
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x00008816 File Offset: 0x00006A16
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			this.CheckConnectionAuthenticated();
			return this.ssl_stream.BeginRead(buffer, offset, count, asyncCallback, asyncState);
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x00008830 File Offset: 0x00006A30
		public virtual IAsyncResult BeginAuthenticateAsServer(System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate, AsyncCallback asyncCallback, object asyncState)
		{
			return this.BeginAuthenticateAsServer(serverCertificate, false, SslProtocols.Tls, false, asyncCallback, asyncState);
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x00008844 File Offset: 0x00006A44
		public virtual IAsyncResult BeginAuthenticateAsServer(System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate, bool clientCertificateRequired, SslProtocols enabledSslProtocols, bool checkCertificateRevocation, AsyncCallback asyncCallback, object asyncState)
		{
			if (this.IsAuthenticated)
			{
				throw new InvalidOperationException("This SslStream is already authenticated");
			}
			this.ssl_stream = new SslServerStream(base.InnerStream, serverCertificate, false, clientCertificateRequired, !base.LeaveInnerStreamOpen, this.GetMonoSslProtocol(enabledSslProtocols))
			{
				CheckCertRevocationStatus = checkCertificateRevocation,
				PrivateKeyCertSelectionDelegate = delegate(System.Security.Cryptography.X509Certificates.X509Certificate cert, string targetHost)
				{
					X509Certificate2 x509Certificate = (serverCertificate as X509Certificate2) ?? new X509Certificate2(serverCertificate);
					if (x509Certificate == null)
					{
						return null;
					}
					return x509Certificate.PrivateKey;
				},
				ClientCertValidationDelegate = delegate(System.Security.Cryptography.X509Certificates.X509Certificate cert, int[] certErrors)
				{
					MonoSslPolicyErrors errors = (certErrors.Length != 0) ? MonoSslPolicyErrors.RemoteCertificateChainErrors : MonoSslPolicyErrors.None;
					return ((ChainValidationHelper)this.certificateValidator).ValidateClientCertificate(cert, errors);
				}
			};
			return this.BeginWrite(new byte[0], 0, 0, asyncCallback, asyncState);
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x000088E1 File Offset: 0x00006AE1
		private Mono.Security.Protocol.Tls.SecurityProtocolType GetMonoSslProtocol(SslProtocols ms)
		{
			if (ms == SslProtocols.Ssl2)
			{
				return Mono.Security.Protocol.Tls.SecurityProtocolType.Ssl2;
			}
			if (ms == SslProtocols.Ssl3)
			{
				return Mono.Security.Protocol.Tls.SecurityProtocolType.Ssl3;
			}
			if (ms != SslProtocols.Tls)
			{
				return Mono.Security.Protocol.Tls.SecurityProtocolType.Default;
			}
			return Mono.Security.Protocol.Tls.SecurityProtocolType.Tls;
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x00008908 File Offset: 0x00006B08
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			this.CheckConnectionAuthenticated();
			return this.ssl_stream.BeginWrite(buffer, offset, count, asyncCallback, asyncState);
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x00008922 File Offset: 0x00006B22
		public virtual void AuthenticateAsClient(string targetHost)
		{
			this.AuthenticateAsClient(targetHost, new System.Security.Cryptography.X509Certificates.X509CertificateCollection(), SslProtocols.Tls, false);
		}

		// Token: 0x060002B7 RID: 695 RVA: 0x00008936 File Offset: 0x00006B36
		public virtual void AuthenticateAsClient(string targetHost, System.Security.Cryptography.X509Certificates.X509CertificateCollection clientCertificates, SslProtocols enabledSslProtocols, bool checkCertificateRevocation)
		{
			this.EndAuthenticateAsClient(this.BeginAuthenticateAsClient(targetHost, clientCertificates, enabledSslProtocols, checkCertificateRevocation, null, null));
		}

		// Token: 0x060002B8 RID: 696 RVA: 0x0000894B File Offset: 0x00006B4B
		public virtual void AuthenticateAsServer(System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate)
		{
			this.AuthenticateAsServer(serverCertificate, false, SslProtocols.Tls, false);
		}

		// Token: 0x060002B9 RID: 697 RVA: 0x0000895B File Offset: 0x00006B5B
		public virtual void AuthenticateAsServer(System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate, bool clientCertificateRequired, SslProtocols enabledSslProtocols, bool checkCertificateRevocation)
		{
			this.EndAuthenticateAsServer(this.BeginAuthenticateAsServer(serverCertificate, clientCertificateRequired, enabledSslProtocols, checkCertificateRevocation, null, null));
		}

		// Token: 0x060002BA RID: 698 RVA: 0x00008970 File Offset: 0x00006B70
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.ssl_stream != null)
				{
					this.ssl_stream.Dispose();
				}
				this.ssl_stream = null;
			}
			base.Dispose(disposing);
		}

		// Token: 0x060002BB RID: 699 RVA: 0x00008996 File Offset: 0x00006B96
		public virtual void EndAuthenticateAsClient(IAsyncResult asyncResult)
		{
			this.CheckConnectionAuthenticated();
			if (this.CanRead)
			{
				this.ssl_stream.EndRead(asyncResult);
				return;
			}
			this.ssl_stream.EndWrite(asyncResult);
		}

		// Token: 0x060002BC RID: 700 RVA: 0x00008996 File Offset: 0x00006B96
		public virtual void EndAuthenticateAsServer(IAsyncResult asyncResult)
		{
			this.CheckConnectionAuthenticated();
			if (this.CanRead)
			{
				this.ssl_stream.EndRead(asyncResult);
				return;
			}
			this.ssl_stream.EndWrite(asyncResult);
		}

		// Token: 0x060002BD RID: 701 RVA: 0x000089C0 File Offset: 0x00006BC0
		public override int EndRead(IAsyncResult asyncResult)
		{
			this.CheckConnectionAuthenticated();
			return this.ssl_stream.EndRead(asyncResult);
		}

		// Token: 0x060002BE RID: 702 RVA: 0x000089D4 File Offset: 0x00006BD4
		public override void EndWrite(IAsyncResult asyncResult)
		{
			this.CheckConnectionAuthenticated();
			this.ssl_stream.EndWrite(asyncResult);
		}

		// Token: 0x060002BF RID: 703 RVA: 0x000089E8 File Offset: 0x00006BE8
		public override void Flush()
		{
			this.CheckConnectionAuthenticated();
			base.InnerStream.Flush();
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x000089FB File Offset: 0x00006BFB
		public override int Read(byte[] buffer, int offset, int count)
		{
			return this.EndRead(this.BeginRead(buffer, offset, count, null, null));
		}

		// Token: 0x060002C1 RID: 705 RVA: 0x0000842C File Offset: 0x0000662C
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException("This stream does not support seek operations");
		}

		// Token: 0x060002C2 RID: 706 RVA: 0x00006747 File Offset: 0x00004947
		public override void SetLength(long value)
		{
			base.InnerStream.SetLength(value);
		}

		// Token: 0x060002C3 RID: 707 RVA: 0x00008A0E File Offset: 0x00006C0E
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.EndWrite(this.BeginWrite(buffer, offset, count, null, null));
		}

		// Token: 0x060002C4 RID: 708 RVA: 0x00005EE6 File Offset: 0x000040E6
		public void Write(byte[] buffer)
		{
			this.Write(buffer, 0, buffer.Length);
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x00008A21 File Offset: 0x00006C21
		private void CheckConnectionAuthenticated()
		{
			if (!this.IsAuthenticated)
			{
				throw new InvalidOperationException("This operation is invalid until it is successfully authenticated");
			}
		}

		// Token: 0x060002C6 RID: 710 RVA: 0x00008A36 File Offset: 0x00006C36
		public virtual Task AuthenticateAsClientAsync(string targetHost)
		{
			return Task.Factory.FromAsync<string>(new Func<string, AsyncCallback, object, IAsyncResult>(this.BeginAuthenticateAsClient), new Action<IAsyncResult>(this.EndAuthenticateAsClient), targetHost, null);
		}

		// Token: 0x060002C7 RID: 711 RVA: 0x00008A60 File Offset: 0x00006C60
		public virtual Task AuthenticateAsClientAsync(string targetHost, System.Security.Cryptography.X509Certificates.X509CertificateCollection clientCertificates, SslProtocols enabledSslProtocols, bool checkCertificateRevocation)
		{
			Tuple<string, System.Security.Cryptography.X509Certificates.X509CertificateCollection, SslProtocols, bool, LegacySslStream> state2 = Tuple.Create<string, System.Security.Cryptography.X509Certificates.X509CertificateCollection, SslProtocols, bool, LegacySslStream>(targetHost, clientCertificates, enabledSslProtocols, checkCertificateRevocation, this);
			return Task.Factory.FromAsync(delegate(AsyncCallback callback, object state)
			{
				Tuple<string, System.Security.Cryptography.X509Certificates.X509CertificateCollection, SslProtocols, bool, LegacySslStream> tuple = (Tuple<string, System.Security.Cryptography.X509Certificates.X509CertificateCollection, SslProtocols, bool, LegacySslStream>)state;
				return tuple.Item5.BeginAuthenticateAsClient(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4, callback, null);
			}, new Action<IAsyncResult>(this.EndAuthenticateAsClient), state2);
		}

		// Token: 0x060002C8 RID: 712 RVA: 0x00008AB0 File Offset: 0x00006CB0
		public virtual Task AuthenticateAsServerAsync(System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate)
		{
			return Task.Factory.FromAsync<System.Security.Cryptography.X509Certificates.X509Certificate>(new Func<System.Security.Cryptography.X509Certificates.X509Certificate, AsyncCallback, object, IAsyncResult>(this.BeginAuthenticateAsServer), new Action<IAsyncResult>(this.EndAuthenticateAsServer), serverCertificate, null);
		}

		// Token: 0x060002C9 RID: 713 RVA: 0x00008AD8 File Offset: 0x00006CD8
		public virtual Task AuthenticateAsServerAsync(System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate, bool clientCertificateRequired, SslProtocols enabledSslProtocols, bool checkCertificateRevocation)
		{
			Tuple<System.Security.Cryptography.X509Certificates.X509Certificate, bool, SslProtocols, bool, LegacySslStream> state2 = Tuple.Create<System.Security.Cryptography.X509Certificates.X509Certificate, bool, SslProtocols, bool, LegacySslStream>(serverCertificate, clientCertificateRequired, enabledSslProtocols, checkCertificateRevocation, this);
			return Task.Factory.FromAsync(delegate(AsyncCallback callback, object state)
			{
				Tuple<System.Security.Cryptography.X509Certificates.X509Certificate, bool, SslProtocols, bool, LegacySslStream> tuple = (Tuple<System.Security.Cryptography.X509Certificates.X509Certificate, bool, SslProtocols, bool, LegacySslStream>)state;
				return tuple.Item5.BeginAuthenticateAsServer(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4, callback, null);
			}, new Action<IAsyncResult>(this.EndAuthenticateAsServer), state2);
		}

		// Token: 0x060002CA RID: 714 RVA: 0x00008B28 File Offset: 0x00006D28
		Task IMonoSslStream.ShutdownAsync()
		{
			return Task.CompletedTask;
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060002CB RID: 715 RVA: 0x00002068 File Offset: 0x00000268
		AuthenticatedStream IMonoSslStream.AuthenticatedStream
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060002CC RID: 716 RVA: 0x00006740 File Offset: 0x00004940
		TransportContext IMonoSslStream.TransportContext
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060002CD RID: 717 RVA: 0x00008B2F File Offset: 0x00006D2F
		public SslStream SslStream
		{
			[CompilerGenerated]
			get
			{
				return this.<SslStream>k__BackingField;
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060002CE RID: 718 RVA: 0x00008B37 File Offset: 0x00006D37
		public MonoTlsProvider Provider
		{
			[CompilerGenerated]
			get
			{
				return this.<Provider>k__BackingField;
			}
		}

		// Token: 0x060002CF RID: 719 RVA: 0x00008B3F File Offset: 0x00006D3F
		public MonoTlsConnectionInfo GetConnectionInfo()
		{
			return null;
		}

		// Token: 0x040007CD RID: 1997
		private SslStreamBase ssl_stream;

		// Token: 0x040007CE RID: 1998
		private ICertificateValidator certificateValidator;

		// Token: 0x040007CF RID: 1999
		[CompilerGenerated]
		private readonly SslStream <SslStream>k__BackingField;

		// Token: 0x040007D0 RID: 2000
		[CompilerGenerated]
		private readonly MonoTlsProvider <Provider>k__BackingField;

		// Token: 0x0200007E RID: 126
		[CompilerGenerated]
		private sealed class <>c__DisplayClass56_0
		{
			// Token: 0x060002D0 RID: 720 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass56_0()
			{
			}

			// Token: 0x060002D1 RID: 721 RVA: 0x00008B44 File Offset: 0x00006D44
			internal AsymmetricAlgorithm <BeginAuthenticateAsClient>b__0(System.Security.Cryptography.X509Certificates.X509Certificate cert, string host)
			{
				string certHashString = cert.GetCertHashString();
				foreach (System.Security.Cryptography.X509Certificates.X509Certificate x509Certificate in this.clientCertificates)
				{
					if (!(x509Certificate.GetCertHashString() != certHashString))
					{
						return ((x509Certificate as X509Certificate2) ?? new X509Certificate2(x509Certificate)).PrivateKey;
					}
				}
				return null;
			}

			// Token: 0x060002D2 RID: 722 RVA: 0x00008BC4 File Offset: 0x00006DC4
			internal ValidationResult <BeginAuthenticateAsClient>b__1(Mono.Security.X509.X509CertificateCollection mcerts)
			{
				System.Security.Cryptography.X509Certificates.X509CertificateCollection x509CertificateCollection = null;
				if (mcerts != null)
				{
					x509CertificateCollection = new System.Security.Cryptography.X509Certificates.X509CertificateCollection();
					for (int i = 0; i < mcerts.Count; i++)
					{
						x509CertificateCollection.Add(new X509Certificate2(mcerts[i].RawData));
					}
				}
				return ((ChainValidationHelper)this.<>4__this.certificateValidator).ValidateCertificate(this.targetHost, false, x509CertificateCollection);
			}

			// Token: 0x040007D1 RID: 2001
			public System.Security.Cryptography.X509Certificates.X509CertificateCollection clientCertificates;

			// Token: 0x040007D2 RID: 2002
			public LegacySslStream <>4__this;

			// Token: 0x040007D3 RID: 2003
			public string targetHost;
		}

		// Token: 0x0200007F RID: 127
		[CompilerGenerated]
		private sealed class <>c__DisplayClass59_0
		{
			// Token: 0x060002D3 RID: 723 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass59_0()
			{
			}

			// Token: 0x060002D4 RID: 724 RVA: 0x00008C24 File Offset: 0x00006E24
			internal AsymmetricAlgorithm <BeginAuthenticateAsServer>b__0(System.Security.Cryptography.X509Certificates.X509Certificate cert, string targetHost)
			{
				X509Certificate2 x509Certificate = (this.serverCertificate as X509Certificate2) ?? new X509Certificate2(this.serverCertificate);
				if (x509Certificate == null)
				{
					return null;
				}
				return x509Certificate.PrivateKey;
			}

			// Token: 0x060002D5 RID: 725 RVA: 0x00008C58 File Offset: 0x00006E58
			internal bool <BeginAuthenticateAsServer>b__1(System.Security.Cryptography.X509Certificates.X509Certificate cert, int[] certErrors)
			{
				MonoSslPolicyErrors errors = (certErrors.Length != 0) ? MonoSslPolicyErrors.RemoteCertificateChainErrors : MonoSslPolicyErrors.None;
				return ((ChainValidationHelper)this.<>4__this.certificateValidator).ValidateClientCertificate(cert, errors);
			}

			// Token: 0x040007D4 RID: 2004
			public System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate;

			// Token: 0x040007D5 RID: 2005
			public LegacySslStream <>4__this;
		}

		// Token: 0x02000080 RID: 128
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060002D6 RID: 726 RVA: 0x00008C85 File Offset: 0x00006E85
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060002D7 RID: 727 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c()
			{
			}

			// Token: 0x060002D8 RID: 728 RVA: 0x00008C94 File Offset: 0x00006E94
			internal IAsyncResult <AuthenticateAsClientAsync>b__79_0(AsyncCallback callback, object state)
			{
				Tuple<string, System.Security.Cryptography.X509Certificates.X509CertificateCollection, SslProtocols, bool, LegacySslStream> tuple = (Tuple<string, System.Security.Cryptography.X509Certificates.X509CertificateCollection, SslProtocols, bool, LegacySslStream>)state;
				return tuple.Item5.BeginAuthenticateAsClient(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4, callback, null);
			}

			// Token: 0x060002D9 RID: 729 RVA: 0x00008CD0 File Offset: 0x00006ED0
			internal IAsyncResult <AuthenticateAsServerAsync>b__81_0(AsyncCallback callback, object state)
			{
				Tuple<System.Security.Cryptography.X509Certificates.X509Certificate, bool, SslProtocols, bool, LegacySslStream> tuple = (Tuple<System.Security.Cryptography.X509Certificates.X509Certificate, bool, SslProtocols, bool, LegacySslStream>)state;
				return tuple.Item5.BeginAuthenticateAsServer(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4, callback, null);
			}

			// Token: 0x040007D6 RID: 2006
			public static readonly LegacySslStream.<>c <>9 = new LegacySslStream.<>c();

			// Token: 0x040007D7 RID: 2007
			public static Func<AsyncCallback, object, IAsyncResult> <>9__79_0;

			// Token: 0x040007D8 RID: 2008
			public static Func<AsyncCallback, object, IAsyncResult> <>9__81_0;
		}
	}
}
