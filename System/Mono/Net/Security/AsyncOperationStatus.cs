﻿using System;

namespace Mono.Net.Security
{
	// Token: 0x02000059 RID: 89
	internal enum AsyncOperationStatus
	{
		// Token: 0x04000746 RID: 1862
		Initialize,
		// Token: 0x04000747 RID: 1863
		Continue,
		// Token: 0x04000748 RID: 1864
		ReadDone,
		// Token: 0x04000749 RID: 1865
		Complete
	}
}
