﻿using System;
using System.Globalization;
using System.IO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using Mono.Security.Interface;
using Mono.Security.X509;
using Mono.Security.X509.Extensions;

namespace Mono.Net.Security
{
	// Token: 0x02000072 RID: 114
	internal static class SystemCertificateValidator
	{
		// Token: 0x0600026C RID: 620 RVA: 0x00007CBA File Offset: 0x00005EBA
		static SystemCertificateValidator()
		{
			SystemCertificateValidator.is_macosx = (Environment.OSVersion.Platform != PlatformID.Win32NT && File.Exists("/System/Library/Frameworks/Security.framework/Security"));
		}

		// Token: 0x0600026D RID: 621 RVA: 0x00007CE5 File Offset: 0x00005EE5
		public static System.Security.Cryptography.X509Certificates.X509Chain CreateX509Chain(System.Security.Cryptography.X509Certificates.X509CertificateCollection certs)
		{
			return new System.Security.Cryptography.X509Certificates.X509Chain
			{
				ChainPolicy = new X509ChainPolicy((System.Security.Cryptography.X509Certificates.X509CertificateCollection)certs)
			};
		}

		// Token: 0x0600026E RID: 622 RVA: 0x00005AFA File Offset: 0x00003CFA
		private static bool BuildX509Chain(System.Security.Cryptography.X509Certificates.X509CertificateCollection certs, System.Security.Cryptography.X509Certificates.X509Chain chain, ref SslPolicyErrors errors, ref int status11)
		{
			return false;
		}

		// Token: 0x0600026F RID: 623 RVA: 0x00007D00 File Offset: 0x00005F00
		private static bool CheckUsage(System.Security.Cryptography.X509Certificates.X509CertificateCollection certs, string host, ref SslPolicyErrors errors, ref int status11)
		{
			X509Certificate2 x509Certificate = certs[0] as X509Certificate2;
			if (x509Certificate == null)
			{
				x509Certificate = new X509Certificate2(certs[0]);
			}
			if (!SystemCertificateValidator.is_macosx)
			{
				if (!SystemCertificateValidator.CheckCertificateUsage(x509Certificate))
				{
					errors |= SslPolicyErrors.RemoteCertificateChainErrors;
					status11 = -2146762490;
					return false;
				}
				if (!string.IsNullOrEmpty(host) && !SystemCertificateValidator.CheckServerIdentity(x509Certificate, host))
				{
					errors |= SslPolicyErrors.RemoteCertificateNameMismatch;
					status11 = -2146762481;
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000270 RID: 624 RVA: 0x00007D6C File Offset: 0x00005F6C
		private static bool EvaluateSystem(System.Security.Cryptography.X509Certificates.X509CertificateCollection certs, System.Security.Cryptography.X509Certificates.X509CertificateCollection anchors, string host, System.Security.Cryptography.X509Certificates.X509Chain chain, ref SslPolicyErrors errors, ref int status11)
		{
			System.Security.Cryptography.X509Certificates.X509Certificate x509Certificate = certs[0];
			bool flag;
			if (SystemCertificateValidator.is_macosx)
			{
				OSX509Certificates.SecTrustResult secTrustResult = OSX509Certificates.SecTrustResult.Deny;
				try
				{
					secTrustResult = OSX509Certificates.TrustEvaluateSsl(certs, anchors, host);
					flag = (secTrustResult == OSX509Certificates.SecTrustResult.Proceed || secTrustResult == OSX509Certificates.SecTrustResult.Unspecified);
				}
				catch
				{
					flag = false;
					errors |= SslPolicyErrors.RemoteCertificateChainErrors;
				}
				if (flag)
				{
					errors = SslPolicyErrors.None;
				}
				else
				{
					status11 = (int)secTrustResult;
					errors |= SslPolicyErrors.RemoteCertificateChainErrors;
				}
			}
			else
			{
				flag = SystemCertificateValidator.BuildX509Chain(certs, chain, ref errors, ref status11);
			}
			return flag;
		}

		// Token: 0x06000271 RID: 625 RVA: 0x00007DE4 File Offset: 0x00005FE4
		public static bool Evaluate(MonoTlsSettings settings, string host, System.Security.Cryptography.X509Certificates.X509CertificateCollection certs, System.Security.Cryptography.X509Certificates.X509Chain chain, ref SslPolicyErrors errors, ref int status11)
		{
			if (!SystemCertificateValidator.CheckUsage(certs, host, ref errors, ref status11))
			{
				return false;
			}
			if (settings != null && settings.SkipSystemValidators)
			{
				return false;
			}
			System.Security.Cryptography.X509Certificates.X509CertificateCollection anchors = (settings != null) ? settings.TrustAnchors : null;
			return SystemCertificateValidator.EvaluateSystem(certs, anchors, host, chain, ref errors, ref status11);
		}

		// Token: 0x06000272 RID: 626 RVA: 0x00005AFA File Offset: 0x00003CFA
		internal static bool NeedsChain(MonoTlsSettings settings)
		{
			return false;
		}

		// Token: 0x06000273 RID: 627 RVA: 0x00007E28 File Offset: 0x00006028
		private static bool CheckCertificateUsage(X509Certificate2 cert)
		{
			bool result;
			try
			{
				if (cert.Version < 3)
				{
					result = true;
				}
				else
				{
					X509KeyUsageExtension x509KeyUsageExtension = cert.Extensions["2.5.29.15"] as X509KeyUsageExtension;
					X509EnhancedKeyUsageExtension x509EnhancedKeyUsageExtension = cert.Extensions["2.5.29.37"] as X509EnhancedKeyUsageExtension;
					if (x509KeyUsageExtension != null && x509EnhancedKeyUsageExtension != null)
					{
						if ((x509KeyUsageExtension.KeyUsages & SystemCertificateValidator.s_flags) == X509KeyUsageFlags.None)
						{
							result = false;
						}
						else
						{
							result = (x509EnhancedKeyUsageExtension.EnhancedKeyUsages["1.3.6.1.5.5.7.3.1"] != null || x509EnhancedKeyUsageExtension.EnhancedKeyUsages["2.16.840.1.113730.4.1"] != null);
						}
					}
					else if (x509KeyUsageExtension != null)
					{
						result = ((x509KeyUsageExtension.KeyUsages & SystemCertificateValidator.s_flags) > X509KeyUsageFlags.None);
					}
					else if (x509EnhancedKeyUsageExtension != null)
					{
						result = (x509EnhancedKeyUsageExtension.EnhancedKeyUsages["1.3.6.1.5.5.7.3.1"] != null || x509EnhancedKeyUsageExtension.EnhancedKeyUsages["2.16.840.1.113730.4.1"] != null);
					}
					else
					{
						System.Security.Cryptography.X509Certificates.X509Extension x509Extension = cert.Extensions["2.16.840.1.113730.1.1"];
						if (x509Extension != null)
						{
							result = (x509Extension.NetscapeCertType(false).IndexOf("SSL Server Authentication", StringComparison.Ordinal) != -1);
						}
						else
						{
							result = true;
						}
					}
				}
			}
			catch (Exception arg)
			{
				Console.Error.WriteLine("ERROR processing certificate: {0}", arg);
				Console.Error.WriteLine("Please, report this problem to the Mono team");
				result = false;
			}
			return result;
		}

		// Token: 0x06000274 RID: 628 RVA: 0x00007F6C File Offset: 0x0000616C
		private static bool CheckServerIdentity(X509Certificate2 cert, string targetHost)
		{
			bool result;
			try
			{
				Mono.Security.X509.X509Certificate x509Certificate = new Mono.Security.X509.X509Certificate(cert.RawData);
				Mono.Security.X509.X509Extension x509Extension = x509Certificate.Extensions["2.5.29.17"];
				if (x509Extension != null)
				{
					SubjectAltNameExtension subjectAltNameExtension = new SubjectAltNameExtension(x509Extension);
					foreach (string pattern in subjectAltNameExtension.DNSNames)
					{
						if (SystemCertificateValidator.Match(targetHost, pattern))
						{
							return true;
						}
					}
					string[] array = subjectAltNameExtension.IPAddresses;
					for (int i = 0; i < array.Length; i++)
					{
						if (array[i] == targetHost)
						{
							return true;
						}
					}
				}
				result = SystemCertificateValidator.CheckDomainName(x509Certificate.SubjectName, targetHost);
			}
			catch (Exception arg)
			{
				Console.Error.WriteLine("ERROR processing certificate: {0}", arg);
				Console.Error.WriteLine("Please, report this problem to the Mono team");
				result = false;
			}
			return result;
		}

		// Token: 0x06000275 RID: 629 RVA: 0x00008044 File Offset: 0x00006244
		private static bool CheckDomainName(string subjectName, string targetHost)
		{
			string pattern = string.Empty;
			MatchCollection matchCollection = new Regex("CN\\s*=\\s*([^,]*)").Matches(subjectName);
			if (matchCollection.Count == 1 && matchCollection[0].Success)
			{
				pattern = matchCollection[0].Groups[1].Value.ToString();
			}
			return SystemCertificateValidator.Match(targetHost, pattern);
		}

		// Token: 0x06000276 RID: 630 RVA: 0x000080A4 File Offset: 0x000062A4
		private static bool Match(string hostname, string pattern)
		{
			int num = pattern.IndexOf('*');
			if (num == -1)
			{
				return string.Compare(hostname, pattern, true, CultureInfo.InvariantCulture) == 0;
			}
			if (num != pattern.Length - 1 && pattern[num + 1] != '.')
			{
				return false;
			}
			if (pattern.IndexOf('*', num + 1) != -1)
			{
				return false;
			}
			string text = pattern.Substring(num + 1);
			int num2 = hostname.Length - text.Length;
			if (num2 <= 0)
			{
				return false;
			}
			if (string.Compare(hostname, num2, text, 0, text.Length, true, CultureInfo.InvariantCulture) != 0)
			{
				return false;
			}
			if (num == 0)
			{
				int num3 = hostname.IndexOf('.');
				return num3 == -1 || num3 >= hostname.Length - text.Length;
			}
			string text2 = pattern.Substring(0, num);
			return string.Compare(hostname, 0, text2, 0, text2.Length, true, CultureInfo.InvariantCulture) == 0;
		}

		// Token: 0x040007C1 RID: 1985
		private static bool is_macosx;

		// Token: 0x040007C2 RID: 1986
		private static X509KeyUsageFlags s_flags = X509KeyUsageFlags.KeyAgreement | X509KeyUsageFlags.KeyEncipherment | X509KeyUsageFlags.DigitalSignature;
	}
}
