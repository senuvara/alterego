﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Security.Authentication;
using Mono.Security.Interface;

namespace Mono.Net.Security
{
	// Token: 0x02000070 RID: 112
	internal class MonoTlsStream
	{
		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000259 RID: 601 RVA: 0x00007A63 File Offset: 0x00005C63
		internal HttpWebRequest Request
		{
			get
			{
				return this.request;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x0600025A RID: 602 RVA: 0x00007A6B File Offset: 0x00005C6B
		internal IMonoSslStream SslStream
		{
			get
			{
				return this.sslStream;
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x0600025B RID: 603 RVA: 0x00007A73 File Offset: 0x00005C73
		internal WebExceptionStatus ExceptionStatus
		{
			get
			{
				return this.status;
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x0600025C RID: 604 RVA: 0x00007A7B File Offset: 0x00005C7B
		// (set) Token: 0x0600025D RID: 605 RVA: 0x00007A83 File Offset: 0x00005C83
		internal bool CertificateValidationFailed
		{
			[CompilerGenerated]
			get
			{
				return this.<CertificateValidationFailed>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CertificateValidationFailed>k__BackingField = value;
			}
		}

		// Token: 0x0600025E RID: 606 RVA: 0x00007A8C File Offset: 0x00005C8C
		public MonoTlsStream(HttpWebRequest request, NetworkStream networkStream)
		{
			this.request = request;
			this.networkStream = networkStream;
			this.settings = request.TlsSettings;
			this.provider = (request.TlsProvider ?? MonoTlsProviderFactory.GetProviderInternal());
			this.status = WebExceptionStatus.SecureChannelFailure;
			ChainValidationHelper.Create(this.provider, ref this.settings, this);
		}

		// Token: 0x0600025F RID: 607 RVA: 0x00007AEC File Offset: 0x00005CEC
		internal Stream CreateStream(byte[] buffer)
		{
			this.sslStream = this.provider.CreateSslStream(this.networkStream, false, this.settings);
			try
			{
				string text = this.request.Host;
				if (!string.IsNullOrEmpty(text))
				{
					int num = text.IndexOf(':');
					if (num > 0)
					{
						text = text.Substring(0, num);
					}
				}
				this.sslStream.AuthenticateAsClient(text, this.request.ClientCertificates, (SslProtocols)ServicePointManager.SecurityProtocol, ServicePointManager.CheckCertificateRevocationList);
				this.status = WebExceptionStatus.Success;
			}
			catch
			{
				this.status = WebExceptionStatus.SecureChannelFailure;
				throw;
			}
			finally
			{
				if (this.CertificateValidationFailed)
				{
					this.status = WebExceptionStatus.TrustFailure;
				}
				if (this.status == WebExceptionStatus.Success)
				{
					this.request.ServicePoint.UpdateClientCertificate(this.sslStream.InternalLocalCertificate);
				}
				else
				{
					this.request.ServicePoint.UpdateClientCertificate(null);
					this.sslStream = null;
				}
			}
			try
			{
				if (buffer != null)
				{
					this.sslStream.Write(buffer, 0, buffer.Length);
				}
			}
			catch
			{
				this.status = WebExceptionStatus.SendFailure;
				this.sslStream = null;
				throw;
			}
			return this.sslStream.AuthenticatedStream;
		}

		// Token: 0x040007BA RID: 1978
		private readonly MonoTlsProvider provider;

		// Token: 0x040007BB RID: 1979
		private readonly NetworkStream networkStream;

		// Token: 0x040007BC RID: 1980
		private readonly HttpWebRequest request;

		// Token: 0x040007BD RID: 1981
		private readonly MonoTlsSettings settings;

		// Token: 0x040007BE RID: 1982
		private IMonoSslStream sslStream;

		// Token: 0x040007BF RID: 1983
		private WebExceptionStatus status;

		// Token: 0x040007C0 RID: 1984
		[CompilerGenerated]
		private bool <CertificateValidationFailed>k__BackingField;
	}
}
