﻿using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Mono.Security.Interface;

namespace Mono.Net.Security
{
	// Token: 0x02000071 RID: 113
	internal static class NoReflectionHelper
	{
		// Token: 0x06000260 RID: 608 RVA: 0x00007C20 File Offset: 0x00005E20
		internal static object GetInternalValidator(object provider, object settings)
		{
			return ChainValidationHelper.GetInternalValidator((MonoTlsProvider)provider, (MonoTlsSettings)settings);
		}

		// Token: 0x06000261 RID: 609 RVA: 0x00007C33 File Offset: 0x00005E33
		internal static object GetDefaultValidator(object settings)
		{
			return ChainValidationHelper.GetDefaultValidator((MonoTlsSettings)settings);
		}

		// Token: 0x06000262 RID: 610 RVA: 0x00007C40 File Offset: 0x00005E40
		internal static object GetProvider()
		{
			return MonoTlsProviderFactory.GetProvider();
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x06000263 RID: 611 RVA: 0x00007C47 File Offset: 0x00005E47
		internal static bool IsInitialized
		{
			get
			{
				return MonoTlsProviderFactory.IsInitialized;
			}
		}

		// Token: 0x06000264 RID: 612 RVA: 0x00007C4E File Offset: 0x00005E4E
		internal static void Initialize()
		{
			MonoTlsProviderFactory.Initialize();
		}

		// Token: 0x06000265 RID: 613 RVA: 0x00007C55 File Offset: 0x00005E55
		internal static void Initialize(string provider)
		{
			MonoTlsProviderFactory.Initialize(provider);
		}

		// Token: 0x06000266 RID: 614 RVA: 0x00007C5D File Offset: 0x00005E5D
		internal static HttpWebRequest CreateHttpsRequest(Uri requestUri, object provider, object settings)
		{
			return new HttpWebRequest(requestUri, (MonoTlsProvider)provider, (MonoTlsSettings)settings);
		}

		// Token: 0x06000267 RID: 615 RVA: 0x00007C71 File Offset: 0x00005E71
		internal static object CreateHttpListener(object certificate, object provider, object settings)
		{
			return new HttpListener((X509Certificate)certificate, (MonoTlsProvider)provider, (MonoTlsSettings)settings);
		}

		// Token: 0x06000268 RID: 616 RVA: 0x00007C8A File Offset: 0x00005E8A
		internal static object GetMonoSslStream(SslStream stream)
		{
			return stream.Impl;
		}

		// Token: 0x06000269 RID: 617 RVA: 0x00007C92 File Offset: 0x00005E92
		internal static object GetMonoSslStream(HttpListenerContext context)
		{
			SslStream sslStream = context.Connection.SslStream;
			if (sslStream == null)
			{
				return null;
			}
			return sslStream.Impl;
		}

		// Token: 0x0600026A RID: 618 RVA: 0x00007CAA File Offset: 0x00005EAA
		internal static bool IsProviderSupported(string name)
		{
			return MonoTlsProviderFactory.IsProviderSupported(name);
		}

		// Token: 0x0600026B RID: 619 RVA: 0x00007CB2 File Offset: 0x00005EB2
		internal static object GetProvider(string name)
		{
			return MonoTlsProviderFactory.GetProvider(name);
		}
	}
}
