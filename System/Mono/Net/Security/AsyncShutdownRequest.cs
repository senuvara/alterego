﻿using System;

namespace Mono.Net.Security
{
	// Token: 0x02000063 RID: 99
	internal class AsyncShutdownRequest : AsyncProtocolRequest
	{
		// Token: 0x060001A6 RID: 422 RVA: 0x00005471 File Offset: 0x00003671
		public AsyncShutdownRequest(MobileAuthenticatedStream parent) : base(parent, false)
		{
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x0000547B File Offset: 0x0000367B
		protected override AsyncOperationStatus Run(AsyncOperationStatus status)
		{
			return base.Parent.ProcessShutdown(status);
		}
	}
}
