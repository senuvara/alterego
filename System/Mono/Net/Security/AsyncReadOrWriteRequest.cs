﻿using System;
using System.Runtime.CompilerServices;

namespace Mono.Net.Security
{
	// Token: 0x02000060 RID: 96
	internal abstract class AsyncReadOrWriteRequest : AsyncProtocolRequest
	{
		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600019D RID: 413 RVA: 0x000052F6 File Offset: 0x000034F6
		protected BufferOffsetSize UserBuffer
		{
			[CompilerGenerated]
			get
			{
				return this.<UserBuffer>k__BackingField;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600019E RID: 414 RVA: 0x000052FE File Offset: 0x000034FE
		// (set) Token: 0x0600019F RID: 415 RVA: 0x00005306 File Offset: 0x00003506
		protected int CurrentSize
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentSize>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CurrentSize>k__BackingField = value;
			}
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x0000530F File Offset: 0x0000350F
		public AsyncReadOrWriteRequest(MobileAuthenticatedStream parent, bool sync, byte[] buffer, int offset, int size) : base(parent, sync)
		{
			this.UserBuffer = new BufferOffsetSize(buffer, offset, size);
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x00005329 File Offset: 0x00003529
		public override string ToString()
		{
			return string.Format("[{0}: {1}]", base.Name, this.UserBuffer);
		}

		// Token: 0x04000768 RID: 1896
		[CompilerGenerated]
		private readonly BufferOffsetSize <UserBuffer>k__BackingField;

		// Token: 0x04000769 RID: 1897
		[CompilerGenerated]
		private int <CurrentSize>k__BackingField;
	}
}
