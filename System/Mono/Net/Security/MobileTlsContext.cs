﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using Mono.Security.Interface;

namespace Mono.Net.Security
{
	// Token: 0x0200006E RID: 110
	internal abstract class MobileTlsContext : IDisposable
	{
		// Token: 0x06000228 RID: 552 RVA: 0x00007248 File Offset: 0x00005448
		public MobileTlsContext(MobileAuthenticatedStream parent, bool serverMode, string targetHost, SslProtocols enabledProtocols, X509Certificate serverCertificate, X509CertificateCollection clientCertificates, bool askForClientCert)
		{
			this.parent = parent;
			this.serverMode = serverMode;
			this.targetHost = targetHost;
			this.enabledProtocols = enabledProtocols;
			this.serverCertificate = serverCertificate;
			this.clientCertificates = clientCertificates;
			this.askForClientCert = askForClientCert;
			this.serverName = targetHost;
			if (!string.IsNullOrEmpty(this.serverName))
			{
				int num = this.serverName.IndexOf(':');
				if (num > 0)
				{
					this.serverName = this.serverName.Substring(0, num);
				}
			}
			this.certificateValidator = CertificateValidationHelper.GetInternalValidator(parent.Settings, parent.Provider);
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000229 RID: 553 RVA: 0x000072E0 File Offset: 0x000054E0
		internal MobileAuthenticatedStream Parent
		{
			get
			{
				return this.parent;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x0600022A RID: 554 RVA: 0x000072E8 File Offset: 0x000054E8
		public MonoTlsSettings Settings
		{
			get
			{
				return this.parent.Settings;
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x0600022B RID: 555 RVA: 0x000072F5 File Offset: 0x000054F5
		public MonoTlsProvider Provider
		{
			get
			{
				return this.parent.Provider;
			}
		}

		// Token: 0x0600022C RID: 556 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("MONO_TLS_DEBUG")]
		protected void Debug(string message, params object[] args)
		{
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x0600022D RID: 557
		public abstract bool HasContext { get; }

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x0600022E RID: 558
		public abstract bool IsAuthenticated { get; }

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600022F RID: 559 RVA: 0x00007302 File Offset: 0x00005502
		public bool IsServer
		{
			get
			{
				return this.serverMode;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000230 RID: 560 RVA: 0x0000730A File Offset: 0x0000550A
		protected string TargetHost
		{
			get
			{
				return this.targetHost;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000231 RID: 561 RVA: 0x00007312 File Offset: 0x00005512
		protected string ServerName
		{
			get
			{
				return this.serverName;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000232 RID: 562 RVA: 0x0000731A File Offset: 0x0000551A
		protected bool AskForClientCertificate
		{
			get
			{
				return this.askForClientCert;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000233 RID: 563 RVA: 0x00007322 File Offset: 0x00005522
		protected SslProtocols EnabledProtocols
		{
			get
			{
				return this.enabledProtocols;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000234 RID: 564 RVA: 0x0000732A File Offset: 0x0000552A
		protected X509CertificateCollection ClientCertificates
		{
			get
			{
				return this.clientCertificates;
			}
		}

		// Token: 0x06000235 RID: 565 RVA: 0x00007334 File Offset: 0x00005534
		protected void GetProtocolVersions(out TlsProtocolCode min, out TlsProtocolCode max)
		{
			if ((this.enabledProtocols & SslProtocols.Tls) != SslProtocols.None)
			{
				min = TlsProtocolCode.Tls10;
			}
			else if ((this.enabledProtocols & SslProtocols.Tls11) != SslProtocols.None)
			{
				min = TlsProtocolCode.Tls11;
			}
			else
			{
				min = TlsProtocolCode.Tls12;
			}
			if ((this.enabledProtocols & SslProtocols.Tls12) != SslProtocols.None)
			{
				max = TlsProtocolCode.Tls12;
				return;
			}
			if ((this.enabledProtocols & SslProtocols.Tls11) != SslProtocols.None)
			{
				max = TlsProtocolCode.Tls11;
				return;
			}
			max = TlsProtocolCode.Tls10;
		}

		// Token: 0x06000236 RID: 566
		public abstract void StartHandshake();

		// Token: 0x06000237 RID: 567
		public abstract bool ProcessHandshake();

		// Token: 0x06000238 RID: 568
		public abstract void FinishHandshake();

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000239 RID: 569
		public abstract MonoTlsConnectionInfo ConnectionInfo { get; }

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x0600023A RID: 570 RVA: 0x000073A9 File Offset: 0x000055A9
		internal X509Certificate LocalServerCertificate
		{
			get
			{
				return this.serverCertificate;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x0600023B RID: 571
		internal abstract bool IsRemoteCertificateAvailable { get; }

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x0600023C RID: 572
		internal abstract X509Certificate LocalClientCertificate { get; }

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x0600023D RID: 573
		public abstract X509Certificate RemoteCertificate { get; }

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x0600023E RID: 574
		public abstract TlsProtocols NegotiatedProtocol { get; }

		// Token: 0x0600023F RID: 575
		public abstract void Flush();

		// Token: 0x06000240 RID: 576
		[return: TupleElementNames(new string[]
		{
			"ret",
			"wantMore"
		})]
		public abstract ValueTuple<int, bool> Read(byte[] buffer, int offset, int count);

		// Token: 0x06000241 RID: 577
		[return: TupleElementNames(new string[]
		{
			"ret",
			"wantMore"
		})]
		public abstract ValueTuple<int, bool> Write(byte[] buffer, int offset, int count);

		// Token: 0x06000242 RID: 578
		public abstract void Shutdown();

		// Token: 0x06000243 RID: 579 RVA: 0x000073B4 File Offset: 0x000055B4
		protected bool ValidateCertificate(X509Certificate leaf, X509Chain chain)
		{
			ValidationResult validationResult = this.certificateValidator.ValidateCertificate(this.TargetHost, this.IsServer, leaf, chain);
			return validationResult != null && validationResult.Trusted && !validationResult.UserDenied;
		}

		// Token: 0x06000244 RID: 580 RVA: 0x000073F4 File Offset: 0x000055F4
		protected bool ValidateCertificate(X509CertificateCollection certificates)
		{
			ValidationResult validationResult = this.certificateValidator.ValidateCertificate(this.TargetHost, this.IsServer, certificates);
			return validationResult != null && validationResult.Trusted && !validationResult.UserDenied;
		}

		// Token: 0x06000245 RID: 581 RVA: 0x00007430 File Offset: 0x00005630
		protected X509Certificate SelectClientCertificate(X509Certificate serverCertificate, string[] acceptableIssuers)
		{
			X509Certificate result;
			if (this.certificateValidator.SelectClientCertificate(this.TargetHost, this.ClientCertificates, serverCertificate, acceptableIssuers, out result))
			{
				return result;
			}
			if (this.clientCertificates == null || this.clientCertificates.Count == 0)
			{
				return null;
			}
			if (this.clientCertificates.Count == 1)
			{
				return this.clientCertificates[0];
			}
			throw new NotImplementedException();
		}

		// Token: 0x06000246 RID: 582 RVA: 0x00007493 File Offset: 0x00005693
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000247 RID: 583 RVA: 0x0000232D File Offset: 0x0000052D
		protected virtual void Dispose(bool disposing)
		{
		}

		// Token: 0x06000248 RID: 584 RVA: 0x000074A4 File Offset: 0x000056A4
		~MobileTlsContext()
		{
			this.Dispose(false);
		}

		// Token: 0x040007A7 RID: 1959
		private MobileAuthenticatedStream parent;

		// Token: 0x040007A8 RID: 1960
		private bool serverMode;

		// Token: 0x040007A9 RID: 1961
		private string targetHost;

		// Token: 0x040007AA RID: 1962
		private string serverName;

		// Token: 0x040007AB RID: 1963
		private SslProtocols enabledProtocols;

		// Token: 0x040007AC RID: 1964
		private X509Certificate serverCertificate;

		// Token: 0x040007AD RID: 1965
		private X509CertificateCollection clientCertificates;

		// Token: 0x040007AE RID: 1966
		private bool askForClientCert;

		// Token: 0x040007AF RID: 1967
		private ICertificateValidator2 certificateValidator;
	}
}
