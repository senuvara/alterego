﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;
using Mono.Security.Interface;
using Mono.Unity;

namespace Mono.Net.Security
{
	// Token: 0x0200006F RID: 111
	internal static class MonoTlsProviderFactory
	{
		// Token: 0x06000249 RID: 585 RVA: 0x000074D4 File Offset: 0x000056D4
		internal static MonoTlsProvider GetProviderInternal()
		{
			object obj = MonoTlsProviderFactory.locker;
			MonoTlsProvider result;
			lock (obj)
			{
				MonoTlsProviderFactory.InitializeInternal();
				result = MonoTlsProviderFactory.defaultProvider;
			}
			return result;
		}

		// Token: 0x0600024A RID: 586 RVA: 0x0000751C File Offset: 0x0000571C
		internal static void InitializeInternal()
		{
			object obj = MonoTlsProviderFactory.locker;
			lock (obj)
			{
				if (!MonoTlsProviderFactory.initialized)
				{
					MonoTlsProviderFactory.InitializeProviderRegistration();
					MonoTlsProvider monoTlsProvider;
					try
					{
						monoTlsProvider = MonoTlsProviderFactory.CreateDefaultProviderImpl();
					}
					catch (Exception innerException)
					{
						throw new NotSupportedException("TLS Support not available.", innerException);
					}
					if (monoTlsProvider == null)
					{
						throw new NotSupportedException("TLS Support not available.");
					}
					if (!MonoTlsProviderFactory.providerCache.ContainsKey(monoTlsProvider.ID))
					{
						MonoTlsProviderFactory.providerCache.Add(monoTlsProvider.ID, monoTlsProvider);
					}
					X509Helper2.Initialize();
					MonoTlsProviderFactory.defaultProvider = monoTlsProvider;
					MonoTlsProviderFactory.initialized = true;
				}
			}
		}

		// Token: 0x0600024B RID: 587 RVA: 0x000075C8 File Offset: 0x000057C8
		internal static void InitializeInternal(string provider)
		{
			object obj = MonoTlsProviderFactory.locker;
			lock (obj)
			{
				if (MonoTlsProviderFactory.initialized)
				{
					throw new NotSupportedException("TLS Subsystem already initialized.");
				}
				MonoTlsProviderFactory.defaultProvider = MonoTlsProviderFactory.LookupProvider(provider, true);
				X509Helper2.Initialize();
				MonoTlsProviderFactory.initialized = true;
			}
		}

		// Token: 0x0600024C RID: 588 RVA: 0x0000762C File Offset: 0x0000582C
		private static Type LookupProviderType(string name, bool throwOnError)
		{
			object obj = MonoTlsProviderFactory.locker;
			Type result;
			lock (obj)
			{
				MonoTlsProviderFactory.InitializeProviderRegistration();
				Tuple<Guid, string> tuple;
				if (!MonoTlsProviderFactory.providerRegistration.TryGetValue(name, out tuple))
				{
					if (throwOnError)
					{
						throw new NotSupportedException(string.Format("No such TLS Provider: `{0}'.", name));
					}
					result = null;
				}
				else
				{
					Type type = Type.GetType(tuple.Item2, false);
					if (type == null && throwOnError)
					{
						throw new NotSupportedException(string.Format("Could not find TLS Provider: `{0}'.", tuple.Item2));
					}
					result = type;
				}
			}
			return result;
		}

		// Token: 0x0600024D RID: 589 RVA: 0x000076C0 File Offset: 0x000058C0
		private static MonoTlsProvider LookupProvider(string name, bool throwOnError)
		{
			object obj = MonoTlsProviderFactory.locker;
			MonoTlsProvider result;
			lock (obj)
			{
				MonoTlsProviderFactory.InitializeProviderRegistration();
				Tuple<Guid, string> tuple;
				MonoTlsProvider monoTlsProvider;
				if (!MonoTlsProviderFactory.providerRegistration.TryGetValue(name, out tuple))
				{
					if (throwOnError)
					{
						throw new NotSupportedException(string.Format("No such TLS Provider: `{0}'.", name));
					}
					result = null;
				}
				else if (MonoTlsProviderFactory.providerCache.TryGetValue(tuple.Item1, out monoTlsProvider))
				{
					result = monoTlsProvider;
				}
				else
				{
					Type type = Type.GetType(tuple.Item2, false);
					if (type == null && throwOnError)
					{
						throw new NotSupportedException(string.Format("Could not find TLS Provider: `{0}'.", tuple.Item2));
					}
					try
					{
						monoTlsProvider = (MonoTlsProvider)Activator.CreateInstance(type, true);
					}
					catch (Exception innerException)
					{
						throw new NotSupportedException(string.Format("Unable to instantiate TLS Provider `{0}'.", type), innerException);
					}
					if (monoTlsProvider == null)
					{
						if (throwOnError)
						{
							throw new NotSupportedException(string.Format("No such TLS Provider: `{0}'.", name));
						}
						result = null;
					}
					else
					{
						MonoTlsProviderFactory.providerCache.Add(tuple.Item1, monoTlsProvider);
						result = monoTlsProvider;
					}
				}
			}
			return result;
		}

		// Token: 0x0600024E RID: 590 RVA: 0x000077DC File Offset: 0x000059DC
		[Conditional("MONO_TLS_DEBUG")]
		private static void InitializeDebug()
		{
			if (Environment.GetEnvironmentVariable("MONO_TLS_DEBUG") != null)
			{
				MonoTlsProviderFactory.enableDebug = true;
			}
		}

		// Token: 0x0600024F RID: 591 RVA: 0x000077F0 File Offset: 0x000059F0
		[Conditional("MONO_TLS_DEBUG")]
		internal static void Debug(string message, params object[] args)
		{
			if (MonoTlsProviderFactory.enableDebug)
			{
				Console.Error.WriteLine(message, args);
			}
		}

		// Token: 0x06000250 RID: 592 RVA: 0x00007808 File Offset: 0x00005A08
		private static void InitializeProviderRegistration()
		{
			object obj = MonoTlsProviderFactory.locker;
			lock (obj)
			{
				if (MonoTlsProviderFactory.providerRegistration == null)
				{
					MonoTlsProviderFactory.providerRegistration = new Dictionary<string, Tuple<Guid, string>>();
					MonoTlsProviderFactory.providerCache = new Dictionary<Guid, MonoTlsProvider>();
					if (UnityTls.IsSupported)
					{
						Tuple<Guid, string> value = new Tuple<Guid, string>(MonoTlsProviderFactory.UnityTlsId, "Mono.Unity.UnityTlsProvider");
						MonoTlsProviderFactory.providerRegistration.Add("default", value);
						MonoTlsProviderFactory.providerRegistration.Add("unitytls", value);
					}
					else
					{
						Tuple<Guid, string> value2 = new Tuple<Guid, string>(MonoTlsProviderFactory.AppleTlsId, "Mono.AppleTls.AppleTlsProvider");
						Tuple<Guid, string> value3 = new Tuple<Guid, string>(MonoTlsProviderFactory.LegacyId, "Mono.Net.Security.LegacyTlsProvider");
						MonoTlsProviderFactory.providerRegistration.Add("legacy", value3);
						Tuple<Guid, string> tuple = null;
						if (Platform.IsMacOS)
						{
							MonoTlsProviderFactory.providerRegistration.Add("default", value2);
						}
						else if (tuple != null)
						{
							MonoTlsProviderFactory.providerRegistration.Add("default", tuple);
						}
						else
						{
							MonoTlsProviderFactory.providerRegistration.Add("default", value3);
						}
						MonoTlsProviderFactory.providerRegistration.Add("apple", value2);
					}
				}
			}
		}

		// Token: 0x06000251 RID: 593 RVA: 0x00007924 File Offset: 0x00005B24
		private static MonoTlsProvider CreateDefaultProviderImpl()
		{
			string text = Environment.GetEnvironmentVariable("MONO_TLS_PROVIDER");
			if (string.IsNullOrEmpty(text))
			{
				text = "default";
			}
			return MonoTlsProviderFactory.LookupProvider(text, true);
		}

		// Token: 0x06000252 RID: 594 RVA: 0x00007951 File Offset: 0x00005B51
		internal static MonoTlsProvider GetProvider()
		{
			MonoTlsProvider providerInternal = MonoTlsProviderFactory.GetProviderInternal();
			if (providerInternal == null)
			{
				throw new NotSupportedException("No TLS Provider available.");
			}
			return providerInternal;
		}

		// Token: 0x06000253 RID: 595 RVA: 0x00007968 File Offset: 0x00005B68
		internal static bool IsProviderSupported(string name)
		{
			object obj = MonoTlsProviderFactory.locker;
			bool result;
			lock (obj)
			{
				MonoTlsProviderFactory.InitializeProviderRegistration();
				result = MonoTlsProviderFactory.providerRegistration.ContainsKey(name);
			}
			return result;
		}

		// Token: 0x06000254 RID: 596 RVA: 0x000079B4 File Offset: 0x00005BB4
		internal static MonoTlsProvider GetProvider(string name)
		{
			return MonoTlsProviderFactory.LookupProvider(name, false);
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000255 RID: 597 RVA: 0x000079C0 File Offset: 0x00005BC0
		internal static bool IsInitialized
		{
			get
			{
				object obj = MonoTlsProviderFactory.locker;
				bool result;
				lock (obj)
				{
					result = MonoTlsProviderFactory.initialized;
				}
				return result;
			}
		}

		// Token: 0x06000256 RID: 598 RVA: 0x00007A00 File Offset: 0x00005C00
		internal static void Initialize()
		{
			MonoTlsProviderFactory.InitializeInternal();
		}

		// Token: 0x06000257 RID: 599 RVA: 0x00007A07 File Offset: 0x00005C07
		internal static void Initialize(string provider)
		{
			MonoTlsProviderFactory.InitializeInternal(provider);
		}

		// Token: 0x06000258 RID: 600 RVA: 0x00007A10 File Offset: 0x00005C10
		// Note: this type is marked as 'beforefieldinit'.
		static MonoTlsProviderFactory()
		{
		}

		// Token: 0x040007B0 RID: 1968
		private static object locker = new object();

		// Token: 0x040007B1 RID: 1969
		private static bool initialized;

		// Token: 0x040007B2 RID: 1970
		private static MonoTlsProvider defaultProvider;

		// Token: 0x040007B3 RID: 1971
		private static Dictionary<string, Tuple<Guid, string>> providerRegistration;

		// Token: 0x040007B4 RID: 1972
		private static Dictionary<Guid, MonoTlsProvider> providerCache;

		// Token: 0x040007B5 RID: 1973
		private static bool enableDebug;

		// Token: 0x040007B6 RID: 1974
		internal static readonly Guid UnityTlsId = new Guid("06414A97-74F6-488F-877B-A6CA9BBEB82E");

		// Token: 0x040007B7 RID: 1975
		internal static readonly Guid AppleTlsId = new Guid("981af8af-a3a3-419a-9f01-a518e3a17c1c");

		// Token: 0x040007B8 RID: 1976
		internal static readonly Guid BtlsId = new Guid("432d18c9-9348-4b90-bfbf-9f2a10e1f15b");

		// Token: 0x040007B9 RID: 1977
		internal static readonly Guid LegacyId = new Guid("809e77d5-56cc-4da8-b9f0-45e65ba9cceb");
	}
}
