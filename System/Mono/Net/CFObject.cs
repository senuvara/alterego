﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using ObjCRuntimeInternal;

namespace Mono.Net
{
	// Token: 0x02000042 RID: 66
	internal class CFObject : IDisposable, INativeObject
	{
		// Token: 0x060000D7 RID: 215
		[DllImport("/usr/lib/libSystem.dylib")]
		public static extern IntPtr dlopen(string path, int mode);

		// Token: 0x060000D8 RID: 216
		[DllImport("/usr/lib/libSystem.dylib")]
		private static extern IntPtr dlsym(IntPtr handle, string symbol);

		// Token: 0x060000D9 RID: 217
		[DllImport("/usr/lib/libSystem.dylib")]
		public static extern void dlclose(IntPtr handle);

		// Token: 0x060000DA RID: 218 RVA: 0x00003500 File Offset: 0x00001700
		public static IntPtr GetIndirect(IntPtr handle, string symbol)
		{
			return CFObject.dlsym(handle, symbol);
		}

		// Token: 0x060000DB RID: 219 RVA: 0x0000350C File Offset: 0x0000170C
		public static CFString GetStringConstant(IntPtr handle, string symbol)
		{
			IntPtr intPtr = CFObject.dlsym(handle, symbol);
			if (intPtr == IntPtr.Zero)
			{
				return null;
			}
			IntPtr intPtr2 = Marshal.ReadIntPtr(intPtr);
			if (intPtr2 == IntPtr.Zero)
			{
				return null;
			}
			return new CFString(intPtr2, false);
		}

		// Token: 0x060000DC RID: 220 RVA: 0x00003550 File Offset: 0x00001750
		public static IntPtr GetIntPtr(IntPtr handle, string symbol)
		{
			IntPtr intPtr = CFObject.dlsym(handle, symbol);
			if (intPtr == IntPtr.Zero)
			{
				return IntPtr.Zero;
			}
			return Marshal.ReadIntPtr(intPtr);
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00003580 File Offset: 0x00001780
		public static IntPtr GetCFObjectHandle(IntPtr handle, string symbol)
		{
			IntPtr intPtr = CFObject.dlsym(handle, symbol);
			if (intPtr == IntPtr.Zero)
			{
				return IntPtr.Zero;
			}
			return Marshal.ReadIntPtr(intPtr);
		}

		// Token: 0x060000DE RID: 222 RVA: 0x000035AE File Offset: 0x000017AE
		public CFObject(IntPtr handle, bool own)
		{
			this.Handle = handle;
			if (!own)
			{
				this.Retain();
			}
		}

		// Token: 0x060000DF RID: 223 RVA: 0x000035C8 File Offset: 0x000017C8
		~CFObject()
		{
			this.Dispose(false);
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x060000E0 RID: 224 RVA: 0x000035F8 File Offset: 0x000017F8
		// (set) Token: 0x060000E1 RID: 225 RVA: 0x00003600 File Offset: 0x00001800
		public IntPtr Handle
		{
			[CompilerGenerated]
			get
			{
				return this.<Handle>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Handle>k__BackingField = value;
			}
		}

		// Token: 0x060000E2 RID: 226
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		internal static extern IntPtr CFRetain(IntPtr handle);

		// Token: 0x060000E3 RID: 227 RVA: 0x00003609 File Offset: 0x00001809
		private void Retain()
		{
			CFObject.CFRetain(this.Handle);
		}

		// Token: 0x060000E4 RID: 228
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		internal static extern void CFRelease(IntPtr handle);

		// Token: 0x060000E5 RID: 229 RVA: 0x00003617 File Offset: 0x00001817
		private void Release()
		{
			CFObject.CFRelease(this.Handle);
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00003624 File Offset: 0x00001824
		protected virtual void Dispose(bool disposing)
		{
			if (this.Handle != IntPtr.Zero)
			{
				this.Release();
				this.Handle = IntPtr.Zero;
			}
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x00003649 File Offset: 0x00001849
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x04000703 RID: 1795
		public const string CoreFoundationLibrary = "/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation";

		// Token: 0x04000704 RID: 1796
		private const string SystemLibrary = "/usr/lib/libSystem.dylib";

		// Token: 0x04000705 RID: 1797
		[CompilerGenerated]
		private IntPtr <Handle>k__BackingField;
	}
}
