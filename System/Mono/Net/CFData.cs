﻿using System;
using System.Runtime.InteropServices;

namespace Mono.Net
{
	// Token: 0x02000048 RID: 72
	internal class CFData : CFObject
	{
		// Token: 0x0600010A RID: 266 RVA: 0x00003658 File Offset: 0x00001858
		public CFData(IntPtr handle, bool own) : base(handle, own)
		{
		}

		// Token: 0x0600010B RID: 267
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFDataCreate(IntPtr allocator, IntPtr bytes, IntPtr length);

		// Token: 0x0600010C RID: 268 RVA: 0x00003A18 File Offset: 0x00001C18
		public unsafe static CFData FromData(byte[] buffer)
		{
			byte* value;
			if (buffer == null || buffer.Length == 0)
			{
				value = null;
			}
			else
			{
				value = &buffer[0];
			}
			return CFData.FromData((IntPtr)((void*)value), (IntPtr)buffer.Length);
		}

		// Token: 0x0600010D RID: 269 RVA: 0x00003A50 File Offset: 0x00001C50
		public static CFData FromData(IntPtr buffer, IntPtr length)
		{
			return new CFData(CFData.CFDataCreate(IntPtr.Zero, buffer, length), true);
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600010E RID: 270 RVA: 0x00003A64 File Offset: 0x00001C64
		public IntPtr Length
		{
			get
			{
				return CFData.CFDataGetLength(base.Handle);
			}
		}

		// Token: 0x0600010F RID: 271
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFDataGetLength(IntPtr theData);

		// Token: 0x06000110 RID: 272
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFDataGetBytePtr(IntPtr theData);

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000111 RID: 273 RVA: 0x00003A71 File Offset: 0x00001C71
		public IntPtr Bytes
		{
			get
			{
				return CFData.CFDataGetBytePtr(base.Handle);
			}
		}

		// Token: 0x17000017 RID: 23
		public byte this[long idx]
		{
			get
			{
				if (idx < 0L || idx > (long)this.Length)
				{
					throw new ArgumentException("idx");
				}
				return Marshal.ReadByte(new IntPtr(this.Bytes.ToInt64() + idx));
			}
			set
			{
				throw new NotImplementedException("NSData arrays can not be modified, use an NSMutableData instead");
			}
		}
	}
}
