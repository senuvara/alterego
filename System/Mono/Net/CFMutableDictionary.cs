﻿using System;
using System.Runtime.InteropServices;

namespace Mono.Net
{
	// Token: 0x0200004A RID: 74
	internal class CFMutableDictionary : CFDictionary
	{
		// Token: 0x06000120 RID: 288 RVA: 0x00003C37 File Offset: 0x00001E37
		public CFMutableDictionary(IntPtr handle, bool own) : base(handle, own)
		{
		}

		// Token: 0x06000121 RID: 289 RVA: 0x00003C41 File Offset: 0x00001E41
		public void SetValue(IntPtr key, IntPtr val)
		{
			CFMutableDictionary.CFDictionarySetValue(base.Handle, key, val);
		}

		// Token: 0x06000122 RID: 290 RVA: 0x00003C50 File Offset: 0x00001E50
		public static CFMutableDictionary Create()
		{
			IntPtr intPtr = CFMutableDictionary.CFDictionaryCreateMutable(IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
			if (intPtr == IntPtr.Zero)
			{
				throw new InvalidOperationException();
			}
			return new CFMutableDictionary(intPtr, true);
		}

		// Token: 0x06000123 RID: 291
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern void CFDictionarySetValue(IntPtr handle, IntPtr key, IntPtr val);

		// Token: 0x06000124 RID: 292
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFDictionaryCreateMutable(IntPtr allocator, IntPtr capacity, IntPtr keyCallback, IntPtr valueCallbacks);
	}
}
