﻿using System;
using System.Runtime.InteropServices;

namespace Mono.Net
{
	// Token: 0x02000041 RID: 65
	internal class CFType
	{
		// Token: 0x060000D5 RID: 213
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation", EntryPoint = "CFGetTypeID")]
		public static extern IntPtr GetTypeID(IntPtr typeRef);

		// Token: 0x060000D6 RID: 214 RVA: 0x0000232F File Offset: 0x0000052F
		public CFType()
		{
		}
	}
}
