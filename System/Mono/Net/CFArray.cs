﻿using System;
using System.Runtime.InteropServices;
using ObjCRuntimeInternal;

namespace Mono.Net
{
	// Token: 0x02000043 RID: 67
	internal class CFArray : CFObject
	{
		// Token: 0x060000E8 RID: 232 RVA: 0x00003658 File Offset: 0x00001858
		public CFArray(IntPtr handle, bool own) : base(handle, own)
		{
		}

		// Token: 0x060000E9 RID: 233
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFArrayCreate(IntPtr allocator, IntPtr values, IntPtr numValues, IntPtr callbacks);

		// Token: 0x060000EA RID: 234 RVA: 0x00003664 File Offset: 0x00001864
		static CFArray()
		{
			IntPtr intPtr = CFObject.dlopen("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation", 0);
			if (intPtr == IntPtr.Zero)
			{
				return;
			}
			try
			{
				CFArray.kCFTypeArrayCallbacks = CFObject.GetIndirect(intPtr, "kCFTypeArrayCallBacks");
			}
			finally
			{
				CFObject.dlclose(intPtr);
			}
		}

		// Token: 0x060000EB RID: 235 RVA: 0x000036B8 File Offset: 0x000018B8
		public static CFArray FromNativeObjects(params INativeObject[] values)
		{
			return new CFArray(CFArray.Create(values), true);
		}

		// Token: 0x060000EC RID: 236 RVA: 0x000036C8 File Offset: 0x000018C8
		public unsafe static IntPtr Create(params IntPtr[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			IntPtr* value;
			if (values == null || values.Length == 0)
			{
				value = null;
			}
			else
			{
				value = &values[0];
			}
			return CFArray.CFArrayCreate(IntPtr.Zero, (IntPtr)((void*)value), (IntPtr)values.Length, CFArray.kCFTypeArrayCallbacks);
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00003718 File Offset: 0x00001918
		internal unsafe static CFArray CreateArray(params IntPtr[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			IntPtr* value;
			if (values == null || values.Length == 0)
			{
				value = null;
			}
			else
			{
				value = &values[0];
			}
			return new CFArray(CFArray.CFArrayCreate(IntPtr.Zero, (IntPtr)((void*)value), (IntPtr)values.Length, CFArray.kCFTypeArrayCallbacks), false);
		}

		// Token: 0x060000EE RID: 238 RVA: 0x000036B8 File Offset: 0x000018B8
		public static CFArray CreateArray(params INativeObject[] values)
		{
			return new CFArray(CFArray.Create(values), true);
		}

		// Token: 0x060000EF RID: 239 RVA: 0x00003770 File Offset: 0x00001970
		public static IntPtr Create(params INativeObject[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			IntPtr[] array = new IntPtr[values.Length];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = values[i].Handle;
			}
			return CFArray.Create(array);
		}

		// Token: 0x060000F0 RID: 240
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFArrayGetCount(IntPtr handle);

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x060000F1 RID: 241 RVA: 0x000037B3 File Offset: 0x000019B3
		public int Count
		{
			get
			{
				return (int)CFArray.CFArrayGetCount(base.Handle);
			}
		}

		// Token: 0x060000F2 RID: 242
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFArrayGetValueAtIndex(IntPtr handle, IntPtr index);

		// Token: 0x17000013 RID: 19
		public IntPtr this[int index]
		{
			get
			{
				return CFArray.CFArrayGetValueAtIndex(base.Handle, (IntPtr)index);
			}
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x000037D8 File Offset: 0x000019D8
		public static T[] ArrayFromHandle<T>(IntPtr handle, Func<IntPtr, T> creation) where T : class, INativeObject
		{
			if (handle == IntPtr.Zero)
			{
				return null;
			}
			IntPtr value = CFArray.CFArrayGetCount(handle);
			T[] array = new T[(int)value];
			for (uint num = 0U; num < (uint)((int)value); num += 1U)
			{
				array[(int)num] = creation(CFArray.CFArrayGetValueAtIndex(handle, (IntPtr)((long)((ulong)num))));
			}
			return array;
		}

		// Token: 0x04000706 RID: 1798
		private static readonly IntPtr kCFTypeArrayCallbacks;
	}
}
