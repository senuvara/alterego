﻿using System;

namespace Mono.Net
{
	// Token: 0x02000046 RID: 70
	internal struct CFStreamClientContext
	{
		// Token: 0x04000709 RID: 1801
		public IntPtr Version;

		// Token: 0x0400070A RID: 1802
		public IntPtr Info;

		// Token: 0x0400070B RID: 1803
		public IntPtr Retain;

		// Token: 0x0400070C RID: 1804
		public IntPtr Release;

		// Token: 0x0400070D RID: 1805
		public IntPtr CopyDescription;
	}
}
