﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Mono.Net
{
	// Token: 0x02000047 RID: 71
	internal class CFString : CFObject
	{
		// Token: 0x060000FF RID: 255 RVA: 0x00003658 File Offset: 0x00001858
		public CFString(IntPtr handle, bool own) : base(handle, own)
		{
		}

		// Token: 0x06000100 RID: 256
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFStringCreateWithCharacters(IntPtr alloc, IntPtr chars, IntPtr length);

		// Token: 0x06000101 RID: 257 RVA: 0x000038E4 File Offset: 0x00001AE4
		public unsafe static CFString Create(string value)
		{
			IntPtr intPtr;
			fixed (string text = value)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				intPtr = CFString.CFStringCreateWithCharacters(IntPtr.Zero, (IntPtr)((void*)ptr), (IntPtr)value.Length);
			}
			if (intPtr == IntPtr.Zero)
			{
				return null;
			}
			return new CFString(intPtr, true);
		}

		// Token: 0x06000102 RID: 258
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFStringGetLength(IntPtr handle);

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000103 RID: 259 RVA: 0x00003935 File Offset: 0x00001B35
		public int Length
		{
			get
			{
				if (this.str != null)
				{
					return this.str.Length;
				}
				return (int)CFString.CFStringGetLength(base.Handle);
			}
		}

		// Token: 0x06000104 RID: 260
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFStringGetCharactersPtr(IntPtr handle);

		// Token: 0x06000105 RID: 261
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFStringGetCharacters(IntPtr handle, CFRange range, IntPtr buffer);

		// Token: 0x06000106 RID: 262 RVA: 0x0000395C File Offset: 0x00001B5C
		public unsafe static string AsString(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				return null;
			}
			int num = (int)CFString.CFStringGetLength(handle);
			if (num == 0)
			{
				return string.Empty;
			}
			IntPtr intPtr = CFString.CFStringGetCharactersPtr(handle);
			IntPtr intPtr2 = IntPtr.Zero;
			if (intPtr == IntPtr.Zero)
			{
				CFRange range = new CFRange(0, num);
				intPtr2 = Marshal.AllocHGlobal(num * 2);
				CFString.CFStringGetCharacters(handle, range, intPtr2);
				intPtr = intPtr2;
			}
			string result = new string((char*)((void*)intPtr), 0, num);
			if (intPtr2 != IntPtr.Zero)
			{
				Marshal.FreeHGlobal(intPtr2);
			}
			return result;
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000039E4 File Offset: 0x00001BE4
		public override string ToString()
		{
			if (this.str == null)
			{
				this.str = CFString.AsString(base.Handle);
			}
			return this.str;
		}

		// Token: 0x06000108 RID: 264 RVA: 0x00003A05 File Offset: 0x00001C05
		public static implicit operator string(CFString str)
		{
			return str.ToString();
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00003A0D File Offset: 0x00001C0D
		public static implicit operator CFString(string str)
		{
			return CFString.Create(str);
		}

		// Token: 0x0400070E RID: 1806
		private string str;
	}
}
