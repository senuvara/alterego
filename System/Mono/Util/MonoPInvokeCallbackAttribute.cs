﻿using System;
using System.Diagnostics;

namespace Mono.Util
{
	// Token: 0x02000003 RID: 3
	[Conditional("MONOTOUCH")]
	[Conditional("UNITY")]
	[Conditional("FULL_AOT_RUNTIME")]
	[AttributeUsage(AttributeTargets.Method)]
	internal sealed class MonoPInvokeCallbackAttribute : Attribute
	{
		// Token: 0x0600000A RID: 10 RVA: 0x000020AE File Offset: 0x000002AE
		public MonoPInvokeCallbackAttribute(Type t)
		{
		}
	}
}
