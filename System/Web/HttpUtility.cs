﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web.Util;

namespace System.Web
{
	// Token: 0x02000257 RID: 599
	public sealed class HttpUtility
	{
		// Token: 0x060011F1 RID: 4593 RVA: 0x0000232F File Offset: 0x0000052F
		public HttpUtility()
		{
		}

		// Token: 0x060011F2 RID: 4594 RVA: 0x000412CF File Offset: 0x0003F4CF
		public static void HtmlAttributeEncode(string s, TextWriter output)
		{
			if (output == null)
			{
				throw new ArgumentNullException("output");
			}
			HttpEncoder.Current.HtmlAttributeEncode(s, output);
		}

		// Token: 0x060011F3 RID: 4595 RVA: 0x000412EC File Offset: 0x0003F4EC
		public static string HtmlAttributeEncode(string s)
		{
			if (s == null)
			{
				return null;
			}
			string result;
			using (StringWriter stringWriter = new StringWriter())
			{
				HttpEncoder.Current.HtmlAttributeEncode(s, stringWriter);
				result = stringWriter.ToString();
			}
			return result;
		}

		// Token: 0x060011F4 RID: 4596 RVA: 0x00041334 File Offset: 0x0003F534
		public static string UrlDecode(string str)
		{
			return HttpUtility.UrlDecode(str, Encoding.UTF8);
		}

		// Token: 0x060011F5 RID: 4597 RVA: 0x00041341 File Offset: 0x0003F541
		private static char[] GetChars(MemoryStream b, Encoding e)
		{
			return e.GetChars(b.GetBuffer(), 0, (int)b.Length);
		}

		// Token: 0x060011F6 RID: 4598 RVA: 0x00041358 File Offset: 0x0003F558
		private static void WriteCharBytes(IList buf, char ch, Encoding e)
		{
			if (ch > 'ÿ')
			{
				foreach (byte b in e.GetBytes(new char[]
				{
					ch
				}))
				{
					buf.Add(b);
				}
				return;
			}
			buf.Add((byte)ch);
		}

		// Token: 0x060011F7 RID: 4599 RVA: 0x000413AC File Offset: 0x0003F5AC
		public static string UrlDecode(string str, Encoding e)
		{
			if (str == null)
			{
				return null;
			}
			if (str.IndexOf('%') == -1 && str.IndexOf('+') == -1)
			{
				return str;
			}
			if (e == null)
			{
				e = Encoding.UTF8;
			}
			long num = (long)str.Length;
			List<byte> list = new List<byte>();
			int num2 = 0;
			while ((long)num2 < num)
			{
				char c = str[num2];
				if (c == '%' && (long)(num2 + 2) < num && str[num2 + 1] != '%')
				{
					int @char;
					if (str[num2 + 1] == 'u' && (long)(num2 + 5) < num)
					{
						@char = HttpUtility.GetChar(str, num2 + 2, 4);
						if (@char != -1)
						{
							HttpUtility.WriteCharBytes(list, (char)@char, e);
							num2 += 5;
						}
						else
						{
							HttpUtility.WriteCharBytes(list, '%', e);
						}
					}
					else if ((@char = HttpUtility.GetChar(str, num2 + 1, 2)) != -1)
					{
						HttpUtility.WriteCharBytes(list, (char)@char, e);
						num2 += 2;
					}
					else
					{
						HttpUtility.WriteCharBytes(list, '%', e);
					}
				}
				else if (c == '+')
				{
					HttpUtility.WriteCharBytes(list, ' ', e);
				}
				else
				{
					HttpUtility.WriteCharBytes(list, c, e);
				}
				num2++;
			}
			byte[] bytes = list.ToArray();
			return e.GetString(bytes);
		}

		// Token: 0x060011F8 RID: 4600 RVA: 0x000414C4 File Offset: 0x0003F6C4
		public static string UrlDecode(byte[] bytes, Encoding e)
		{
			if (bytes == null)
			{
				return null;
			}
			return HttpUtility.UrlDecode(bytes, 0, bytes.Length, e);
		}

		// Token: 0x060011F9 RID: 4601 RVA: 0x000414D8 File Offset: 0x0003F6D8
		private static int GetInt(byte b)
		{
			if (b >= 48 && b <= 57)
			{
				return (int)(b - 48);
			}
			if (b >= 97 && b <= 102)
			{
				return (int)(b - 97 + 10);
			}
			if (b >= 65 && b <= 70)
			{
				return (int)(b - 65 + 10);
			}
			return -1;
		}

		// Token: 0x060011FA RID: 4602 RVA: 0x0004151C File Offset: 0x0003F71C
		private static int GetChar(byte[] bytes, int offset, int length)
		{
			int num = 0;
			int num2 = length + offset;
			for (int i = offset; i < num2; i++)
			{
				int @int = HttpUtility.GetInt(bytes[i]);
				if (@int == -1)
				{
					return -1;
				}
				num = (num << 4) + @int;
			}
			return num;
		}

		// Token: 0x060011FB RID: 4603 RVA: 0x00041554 File Offset: 0x0003F754
		private static int GetChar(string str, int offset, int length)
		{
			int num = 0;
			int num2 = length + offset;
			for (int i = offset; i < num2; i++)
			{
				char c = str[i];
				if (c > '\u007f')
				{
					return -1;
				}
				int @int = HttpUtility.GetInt((byte)c);
				if (@int == -1)
				{
					return -1;
				}
				num = (num << 4) + @int;
			}
			return num;
		}

		// Token: 0x060011FC RID: 4604 RVA: 0x0004159C File Offset: 0x0003F79C
		public static string UrlDecode(byte[] bytes, int offset, int count, Encoding e)
		{
			if (bytes == null)
			{
				return null;
			}
			if (count == 0)
			{
				return string.Empty;
			}
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (offset < 0 || offset > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (count < 0 || offset + count > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			StringBuilder stringBuilder = new StringBuilder();
			MemoryStream memoryStream = new MemoryStream();
			int num = count + offset;
			int i = offset;
			while (i < num)
			{
				if (bytes[i] != 37 || i + 2 >= count || bytes[i + 1] == 37)
				{
					goto IL_EE;
				}
				if (bytes[i + 1] == 117 && i + 5 < num)
				{
					if (memoryStream.Length > 0L)
					{
						stringBuilder.Append(HttpUtility.GetChars(memoryStream, e));
						memoryStream.SetLength(0L);
					}
					int @char = HttpUtility.GetChar(bytes, i + 2, 4);
					if (@char == -1)
					{
						goto IL_EE;
					}
					stringBuilder.Append((char)@char);
					i += 5;
				}
				else
				{
					int @char;
					if ((@char = HttpUtility.GetChar(bytes, i + 1, 2)) == -1)
					{
						goto IL_EE;
					}
					memoryStream.WriteByte((byte)@char);
					i += 2;
				}
				IL_12C:
				i++;
				continue;
				IL_EE:
				if (memoryStream.Length > 0L)
				{
					stringBuilder.Append(HttpUtility.GetChars(memoryStream, e));
					memoryStream.SetLength(0L);
				}
				if (bytes[i] == 43)
				{
					stringBuilder.Append(' ');
					goto IL_12C;
				}
				stringBuilder.Append((char)bytes[i]);
				goto IL_12C;
			}
			if (memoryStream.Length > 0L)
			{
				stringBuilder.Append(HttpUtility.GetChars(memoryStream, e));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060011FD RID: 4605 RVA: 0x00041703 File Offset: 0x0003F903
		public static byte[] UrlDecodeToBytes(byte[] bytes)
		{
			if (bytes == null)
			{
				return null;
			}
			return HttpUtility.UrlDecodeToBytes(bytes, 0, bytes.Length);
		}

		// Token: 0x060011FE RID: 4606 RVA: 0x00041714 File Offset: 0x0003F914
		public static byte[] UrlDecodeToBytes(string str)
		{
			return HttpUtility.UrlDecodeToBytes(str, Encoding.UTF8);
		}

		// Token: 0x060011FF RID: 4607 RVA: 0x00041721 File Offset: 0x0003F921
		public static byte[] UrlDecodeToBytes(string str, Encoding e)
		{
			if (str == null)
			{
				return null;
			}
			if (e == null)
			{
				throw new ArgumentNullException("e");
			}
			return HttpUtility.UrlDecodeToBytes(e.GetBytes(str));
		}

		// Token: 0x06001200 RID: 4608 RVA: 0x00041744 File Offset: 0x0003F944
		public static byte[] UrlDecodeToBytes(byte[] bytes, int offset, int count)
		{
			if (bytes == null)
			{
				return null;
			}
			if (count == 0)
			{
				return new byte[0];
			}
			int num = bytes.Length;
			if (offset < 0 || offset >= num)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (count < 0 || offset > num - count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			MemoryStream memoryStream = new MemoryStream();
			int num2 = offset + count;
			for (int i = offset; i < num2; i++)
			{
				char c = (char)bytes[i];
				if (c == '+')
				{
					c = ' ';
				}
				else if (c == '%' && i < num2 - 2)
				{
					int @char = HttpUtility.GetChar(bytes, i + 1, 2);
					if (@char != -1)
					{
						c = (char)@char;
						i += 2;
					}
				}
				memoryStream.WriteByte((byte)c);
			}
			return memoryStream.ToArray();
		}

		// Token: 0x06001201 RID: 4609 RVA: 0x000417E8 File Offset: 0x0003F9E8
		public static string UrlEncode(string str)
		{
			return HttpUtility.UrlEncode(str, Encoding.UTF8);
		}

		// Token: 0x06001202 RID: 4610 RVA: 0x000417F8 File Offset: 0x0003F9F8
		public static string UrlEncode(string str, Encoding e)
		{
			if (str == null)
			{
				return null;
			}
			if (str == string.Empty)
			{
				return string.Empty;
			}
			bool flag = false;
			int length = str.Length;
			for (int i = 0; i < length; i++)
			{
				char c = str[i];
				if ((c < '0' || (c < 'A' && c > '9') || (c > 'Z' && c < 'a') || c > 'z') && !HttpEncoder.NotEncoded(c))
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				return str;
			}
			byte[] bytes = new byte[e.GetMaxByteCount(str.Length)];
			int bytes2 = e.GetBytes(str, 0, str.Length, bytes, 0);
			return Encoding.ASCII.GetString(HttpUtility.UrlEncodeToBytes(bytes, 0, bytes2));
		}

		// Token: 0x06001203 RID: 4611 RVA: 0x000418AB File Offset: 0x0003FAAB
		public static string UrlEncode(byte[] bytes)
		{
			if (bytes == null)
			{
				return null;
			}
			if (bytes.Length == 0)
			{
				return string.Empty;
			}
			return Encoding.ASCII.GetString(HttpUtility.UrlEncodeToBytes(bytes, 0, bytes.Length));
		}

		// Token: 0x06001204 RID: 4612 RVA: 0x000418D0 File Offset: 0x0003FAD0
		public static string UrlEncode(byte[] bytes, int offset, int count)
		{
			if (bytes == null)
			{
				return null;
			}
			if (bytes.Length == 0)
			{
				return string.Empty;
			}
			return Encoding.ASCII.GetString(HttpUtility.UrlEncodeToBytes(bytes, offset, count));
		}

		// Token: 0x06001205 RID: 4613 RVA: 0x000418F3 File Offset: 0x0003FAF3
		public static byte[] UrlEncodeToBytes(string str)
		{
			return HttpUtility.UrlEncodeToBytes(str, Encoding.UTF8);
		}

		// Token: 0x06001206 RID: 4614 RVA: 0x00041900 File Offset: 0x0003FB00
		public static byte[] UrlEncodeToBytes(string str, Encoding e)
		{
			if (str == null)
			{
				return null;
			}
			if (str.Length == 0)
			{
				return new byte[0];
			}
			byte[] bytes = e.GetBytes(str);
			return HttpUtility.UrlEncodeToBytes(bytes, 0, bytes.Length);
		}

		// Token: 0x06001207 RID: 4615 RVA: 0x00041933 File Offset: 0x0003FB33
		public static byte[] UrlEncodeToBytes(byte[] bytes)
		{
			if (bytes == null)
			{
				return null;
			}
			if (bytes.Length == 0)
			{
				return new byte[0];
			}
			return HttpUtility.UrlEncodeToBytes(bytes, 0, bytes.Length);
		}

		// Token: 0x06001208 RID: 4616 RVA: 0x0004194F File Offset: 0x0003FB4F
		public static byte[] UrlEncodeToBytes(byte[] bytes, int offset, int count)
		{
			if (bytes == null)
			{
				return null;
			}
			return HttpEncoder.Current.UrlEncode(bytes, offset, count);
		}

		// Token: 0x06001209 RID: 4617 RVA: 0x00041963 File Offset: 0x0003FB63
		public static string UrlEncodeUnicode(string str)
		{
			if (str == null)
			{
				return null;
			}
			return Encoding.ASCII.GetString(HttpUtility.UrlEncodeUnicodeToBytes(str));
		}

		// Token: 0x0600120A RID: 4618 RVA: 0x0004197C File Offset: 0x0003FB7C
		public static byte[] UrlEncodeUnicodeToBytes(string str)
		{
			if (str == null)
			{
				return null;
			}
			if (str.Length == 0)
			{
				return new byte[0];
			}
			MemoryStream memoryStream = new MemoryStream(str.Length);
			for (int i = 0; i < str.Length; i++)
			{
				HttpEncoder.UrlEncodeChar(str[i], memoryStream, true);
			}
			return memoryStream.ToArray();
		}

		// Token: 0x0600120B RID: 4619 RVA: 0x000419D0 File Offset: 0x0003FBD0
		public static string HtmlDecode(string s)
		{
			if (s == null)
			{
				return null;
			}
			string result;
			using (StringWriter stringWriter = new StringWriter())
			{
				HttpEncoder.Current.HtmlDecode(s, stringWriter);
				result = stringWriter.ToString();
			}
			return result;
		}

		// Token: 0x0600120C RID: 4620 RVA: 0x00041A18 File Offset: 0x0003FC18
		public static void HtmlDecode(string s, TextWriter output)
		{
			if (output == null)
			{
				throw new ArgumentNullException("output");
			}
			if (!string.IsNullOrEmpty(s))
			{
				HttpEncoder.Current.HtmlDecode(s, output);
			}
		}

		// Token: 0x0600120D RID: 4621 RVA: 0x00041A3C File Offset: 0x0003FC3C
		public static string HtmlEncode(string s)
		{
			if (s == null)
			{
				return null;
			}
			string result;
			using (StringWriter stringWriter = new StringWriter())
			{
				HttpEncoder.Current.HtmlEncode(s, stringWriter);
				result = stringWriter.ToString();
			}
			return result;
		}

		// Token: 0x0600120E RID: 4622 RVA: 0x00041A84 File Offset: 0x0003FC84
		public static void HtmlEncode(string s, TextWriter output)
		{
			if (output == null)
			{
				throw new ArgumentNullException("output");
			}
			if (!string.IsNullOrEmpty(s))
			{
				HttpEncoder.Current.HtmlEncode(s, output);
			}
		}

		// Token: 0x0600120F RID: 4623 RVA: 0x00041AA8 File Offset: 0x0003FCA8
		public static string HtmlEncode(object value)
		{
			if (value == null)
			{
				return null;
			}
			return HttpUtility.HtmlEncode(value.ToString());
		}

		// Token: 0x06001210 RID: 4624 RVA: 0x00041ABA File Offset: 0x0003FCBA
		public static string JavaScriptStringEncode(string value)
		{
			return HttpUtility.JavaScriptStringEncode(value, false);
		}

		// Token: 0x06001211 RID: 4625 RVA: 0x00041AC4 File Offset: 0x0003FCC4
		public static string JavaScriptStringEncode(string value, bool addDoubleQuotes)
		{
			if (string.IsNullOrEmpty(value))
			{
				if (!addDoubleQuotes)
				{
					return string.Empty;
				}
				return "\"\"";
			}
			else
			{
				int length = value.Length;
				bool flag = false;
				for (int i = 0; i < length; i++)
				{
					char c = value[i];
					if ((c >= '\0' && c <= '\u001f') || c == '"' || c == '\'' || c == '<' || c == '>' || c == '\\')
					{
						flag = true;
						break;
					}
				}
				if (flag)
				{
					StringBuilder stringBuilder = new StringBuilder();
					if (addDoubleQuotes)
					{
						stringBuilder.Append('"');
					}
					for (int j = 0; j < length; j++)
					{
						char c = value[j];
						if ((c >= '\0' && c <= '\a') || (c == '\v' || (c >= '\u000e' && c <= '\u001f')) || c == '\'' || c == '<' || c == '>')
						{
							stringBuilder.AppendFormat("\\u{0:x4}", (int)c);
						}
						else
						{
							int num = (int)c;
							switch (num)
							{
							case 8:
								stringBuilder.Append("\\b");
								goto IL_174;
							case 9:
								stringBuilder.Append("\\t");
								goto IL_174;
							case 10:
								stringBuilder.Append("\\n");
								goto IL_174;
							case 11:
								break;
							case 12:
								stringBuilder.Append("\\f");
								goto IL_174;
							case 13:
								stringBuilder.Append("\\r");
								goto IL_174;
							default:
								if (num == 34)
								{
									stringBuilder.Append("\\\"");
									goto IL_174;
								}
								if (num == 92)
								{
									stringBuilder.Append("\\\\");
									goto IL_174;
								}
								break;
							}
							stringBuilder.Append(c);
						}
						IL_174:;
					}
					if (addDoubleQuotes)
					{
						stringBuilder.Append('"');
					}
					return stringBuilder.ToString();
				}
				if (!addDoubleQuotes)
				{
					return value;
				}
				return "\"" + value + "\"";
			}
		}

		// Token: 0x06001212 RID: 4626 RVA: 0x00041C65 File Offset: 0x0003FE65
		public static string UrlPathEncode(string str)
		{
			return HttpEncoder.Current.UrlPathEncode(str);
		}

		// Token: 0x06001213 RID: 4627 RVA: 0x00041C72 File Offset: 0x0003FE72
		public static NameValueCollection ParseQueryString(string query)
		{
			return HttpUtility.ParseQueryString(query, Encoding.UTF8);
		}

		// Token: 0x06001214 RID: 4628 RVA: 0x00041C80 File Offset: 0x0003FE80
		public static NameValueCollection ParseQueryString(string query, Encoding encoding)
		{
			if (query == null)
			{
				throw new ArgumentNullException("query");
			}
			if (encoding == null)
			{
				throw new ArgumentNullException("encoding");
			}
			if (query.Length == 0 || (query.Length == 1 && query[0] == '?'))
			{
				return new HttpUtility.HttpQSCollection();
			}
			if (query[0] == '?')
			{
				query = query.Substring(1);
			}
			NameValueCollection result = new HttpUtility.HttpQSCollection();
			HttpUtility.ParseQueryString(query, encoding, result);
			return result;
		}

		// Token: 0x06001215 RID: 4629 RVA: 0x00041CF0 File Offset: 0x0003FEF0
		internal static void ParseQueryString(string query, Encoding encoding, NameValueCollection result)
		{
			if (query.Length == 0)
			{
				return;
			}
			string text = HttpUtility.HtmlDecode(query);
			int length = text.Length;
			int i = 0;
			bool flag = true;
			while (i <= length)
			{
				int num = -1;
				int num2 = -1;
				for (int j = i; j < length; j++)
				{
					if (num == -1 && text[j] == '=')
					{
						num = j + 1;
					}
					else if (text[j] == '&')
					{
						num2 = j;
						break;
					}
				}
				if (flag)
				{
					flag = false;
					if (text[i] == '?')
					{
						i++;
					}
				}
				string name;
				if (num == -1)
				{
					name = null;
					num = i;
				}
				else
				{
					name = HttpUtility.UrlDecode(text.Substring(i, num - i - 1), encoding);
				}
				if (num2 < 0)
				{
					i = -1;
					num2 = text.Length;
				}
				else
				{
					i = num2 + 1;
				}
				string value = HttpUtility.UrlDecode(text.Substring(num, num2 - num), encoding);
				result.Add(name, value);
				if (i == -1)
				{
					break;
				}
			}
		}

		// Token: 0x02000258 RID: 600
		private sealed class HttpQSCollection : NameValueCollection
		{
			// Token: 0x06001216 RID: 4630 RVA: 0x00041DD4 File Offset: 0x0003FFD4
			public override string ToString()
			{
				int count = this.Count;
				if (count == 0)
				{
					return "";
				}
				StringBuilder stringBuilder = new StringBuilder();
				string[] allKeys = this.AllKeys;
				for (int i = 0; i < count; i++)
				{
					stringBuilder.AppendFormat("{0}={1}&", allKeys[i], HttpUtility.UrlEncode(base[allKeys[i]]));
				}
				if (stringBuilder.Length > 0)
				{
					StringBuilder stringBuilder2 = stringBuilder;
					int length = stringBuilder2.Length;
					stringBuilder2.Length = length - 1;
				}
				return stringBuilder.ToString();
			}

			// Token: 0x06001217 RID: 4631 RVA: 0x00041E48 File Offset: 0x00040048
			public HttpQSCollection()
			{
			}
		}
	}
}
