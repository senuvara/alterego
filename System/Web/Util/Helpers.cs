﻿using System;
using System.Globalization;

namespace System.Web.Util
{
	// Token: 0x02000259 RID: 601
	internal class Helpers
	{
		// Token: 0x06001218 RID: 4632 RVA: 0x0000232F File Offset: 0x0000052F
		public Helpers()
		{
		}

		// Token: 0x06001219 RID: 4633 RVA: 0x00041E50 File Offset: 0x00040050
		// Note: this type is marked as 'beforefieldinit'.
		static Helpers()
		{
		}

		// Token: 0x04001246 RID: 4678
		public static readonly CultureInfo InvariantCulture = CultureInfo.InvariantCulture;
	}
}
