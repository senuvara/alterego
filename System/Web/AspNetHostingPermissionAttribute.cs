﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Web
{
	/// <summary>Allows security actions for <see cref="T:System.Web.AspNetHostingPermission" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x02000626 RID: 1574
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class AspNetHostingPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Web.AspNetHostingPermissionAttribute" /> class.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> enumeration values. </param>
		// Token: 0x0600328B RID: 12939 RVA: 0x0000232D File Offset: 0x0000052D
		public AspNetHostingPermissionAttribute(SecurityAction action)
		{
		}

		/// <summary>Gets or sets the current hosting permission level.</summary>
		/// <returns>One of the <see cref="T:System.Web.AspNetHostingPermissionLevel" /> enumeration values.</returns>
		// Token: 0x17000C98 RID: 3224
		// (get) Token: 0x0600328C RID: 12940 RVA: 0x000A4950 File Offset: 0x000A2B50
		// (set) Token: 0x0600328D RID: 12941 RVA: 0x000092E2 File Offset: 0x000074E2
		public AspNetHostingPermissionLevel Level
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (AspNetHostingPermissionLevel)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates a new <see cref="T:System.Web.AspNetHostingPermission" /> with the permission level previously set by the <see cref="P:System.Web.AspNetHostingPermissionAttribute.Level" /> property.</summary>
		/// <returns>An <see cref="T:System.Security.IPermission" /> that is the new <see cref="T:System.Web.AspNetHostingPermission" />.</returns>
		// Token: 0x0600328E RID: 12942 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission CreatePermission()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
