﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Web
{
	/// <summary>Controls access permissions in ASP.NET hosted environments. This class cannot be inherited.</summary>
	// Token: 0x02000624 RID: 1572
	[Serializable]
	public sealed class AspNetHostingPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Web.AspNetHostingPermission" /> class with the specified <see cref="T:System.Security.Permissions.PermissionState" /> enumeration value.</summary>
		/// <param name="state">A <see cref="T:System.Security.Permissions.PermissionState" /> enumeration value. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="state" /> is not set to one of the <see cref="T:System.Security.Permissions.PermissionState" /> enumeration values.</exception>
		// Token: 0x06003281 RID: 12929 RVA: 0x000092E2 File Offset: 0x000074E2
		public AspNetHostingPermission(PermissionState state)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Web.AspNetHostingPermission" /> class with the specified permission level.</summary>
		/// <param name="level">An <see cref="T:System.Web.AspNetHostingPermissionLevel" /> enumeration value. </param>
		// Token: 0x06003282 RID: 12930 RVA: 0x000092E2 File Offset: 0x000074E2
		public AspNetHostingPermission(AspNetHostingPermissionLevel level)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the current hosting permission level for an ASP.NET application.</summary>
		/// <returns>One of the <see cref="T:System.Web.AspNetHostingPermissionLevel" /> enumeration values.</returns>
		// Token: 0x17000C97 RID: 3223
		// (get) Token: 0x06003283 RID: 12931 RVA: 0x000A48FC File Offset: 0x000A2AFC
		// (set) Token: 0x06003284 RID: 12932 RVA: 0x000092E2 File Offset: 0x000074E2
		public AspNetHostingPermissionLevel Level
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (AspNetHostingPermissionLevel)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>When implemented by a derived class, creates and returns an identical copy of the current permission object.</summary>
		/// <returns>A copy of the current permission object.</returns>
		// Token: 0x06003285 RID: 12933 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission Copy()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Reconstructs a permission object with a specified state from an XML encoding.</summary>
		/// <param name="securityElement">The <see cref="T:System.Security.SecurityElement" /> containing the XML encoding to use to reconstruct the permission object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="securityElement" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Security.SecurityElement.Tag" /> property of <paramref name="securityElement" /> is not equal to "IPermission". 
		///         <see langword="-" /> or- The class <see cref="M:System.Security.SecurityElement.Attribute(System.String)" /> of <paramref name="securityElement" /> is <see langword="null" /> or an empty string (""). </exception>
		// Token: 0x06003286 RID: 12934 RVA: 0x000092E2 File Offset: 0x000074E2
		public override void FromXml(SecurityElement securityElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>When implemented by a derived class, creates and returns a permission that is the intersection of the current permission and the specified permission.</summary>
		/// <param name="target">A permission to combine with the current permission. It must be of the same type as the current permission. </param>
		/// <returns>An <see cref="T:System.Security.IPermission" /> that represents the intersection of the current permission and the specified permission; otherwise, <see langword="null" /> if the intersection is empty.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> is not an <see cref="T:System.Web.AspNetHostingPermission" />. </exception>
		// Token: 0x06003287 RID: 12935 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission Intersect(IPermission target)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a value indicating whether the current permission is a subset of the specified permission.</summary>
		/// <param name="target">The <see cref="T:System.Security.IPermission" /> to combine with the current permission. It must be of the same type as the current <see cref="T:System.Security.IPermission" />. </param>
		/// <returns>
		///     <see langword="true" /> if the current <see cref="T:System.Security.IPermission" /> is a subset of the specified <see cref="T:System.Security.IPermission" />; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> is not an <see cref="T:System.Web.AspNetHostingPermission" />. </exception>
		// Token: 0x06003288 RID: 12936 RVA: 0x000A4918 File Offset: 0x000A2B18
		public override bool IsSubsetOf(IPermission target)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value indicating whether unrestricted access to the resource that is protected by the current permission is allowed.</summary>
		/// <returns>
		///     <see langword="true" /> if unrestricted use of the resource protected by the permission is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003289 RID: 12937 RVA: 0x000A4934 File Offset: 0x000A2B34
		public bool IsUnrestricted()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Creates an XML encoding of the permission object and its current state.</summary>
		/// <returns>A <see cref="T:System.Security.SecurityElement" /> containing the XML encoding of the permission object, including any state information.</returns>
		// Token: 0x0600328A RID: 12938 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override SecurityElement ToXml()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
