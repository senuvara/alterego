﻿using System;

namespace System
{
	/// <summary>A customizable parser based on the Lightweight Directory Access Protocol (LDAP) scheme.</summary>
	// Token: 0x020000A6 RID: 166
	public class LdapStyleUriParser : UriParser
	{
		/// <summary>Creates a customizable parser based on the Lightweight Directory Access Protocol (LDAP) scheme.</summary>
		// Token: 0x060003EC RID: 1004 RVA: 0x0001373F File Offset: 0x0001193F
		public LdapStyleUriParser() : base(UriParser.LdapUri.Flags)
		{
		}
	}
}
