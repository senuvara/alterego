﻿using System;

namespace Unity
{
	// Token: 0x020006F9 RID: 1785
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x060037E9 RID: 14313 RVA: 0x0004B800 File Offset: 0x00049A00
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
