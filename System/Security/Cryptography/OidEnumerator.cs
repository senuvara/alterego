﻿using System;
using System.Collections;

namespace System.Security.Cryptography
{
	/// <summary>Provides the ability to navigate through an <see cref="T:System.Security.Cryptography.OidCollection" /> object. This class cannot be inherited.</summary>
	// Token: 0x02000284 RID: 644
	public sealed class OidEnumerator : IEnumerator
	{
		// Token: 0x060012CD RID: 4813 RVA: 0x0000232F File Offset: 0x0000052F
		private OidEnumerator()
		{
		}

		// Token: 0x060012CE RID: 4814 RVA: 0x00044E66 File Offset: 0x00043066
		internal OidEnumerator(OidCollection oids)
		{
			this.m_oids = oids;
			this.m_current = -1;
		}

		/// <summary>Gets the current <see cref="T:System.Security.Cryptography.Oid" /> object in an <see cref="T:System.Security.Cryptography.OidCollection" /> object.</summary>
		/// <returns>The current <see cref="T:System.Security.Cryptography.Oid" /> object in the collection.</returns>
		// Token: 0x170003CE RID: 974
		// (get) Token: 0x060012CF RID: 4815 RVA: 0x00044E7C File Offset: 0x0004307C
		public Oid Current
		{
			get
			{
				return this.m_oids[this.m_current];
			}
		}

		/// <summary>Gets the current <see cref="T:System.Security.Cryptography.Oid" /> object in an <see cref="T:System.Security.Cryptography.OidCollection" /> object.</summary>
		/// <returns>The current <see cref="T:System.Security.Cryptography.Oid" /> object.</returns>
		// Token: 0x170003CF RID: 975
		// (get) Token: 0x060012D0 RID: 4816 RVA: 0x00044E7C File Offset: 0x0004307C
		object IEnumerator.Current
		{
			get
			{
				return this.m_oids[this.m_current];
			}
		}

		/// <summary>Advances to the next <see cref="T:System.Security.Cryptography.Oid" /> object in an <see cref="T:System.Security.Cryptography.OidCollection" /> object.</summary>
		/// <returns>
		///     <see langword="true" />, if the enumerator was successfully advanced to the next element; <see langword="false" />, if the enumerator has passed the end of the collection.</returns>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created.</exception>
		// Token: 0x060012D1 RID: 4817 RVA: 0x00044E8F File Offset: 0x0004308F
		public bool MoveNext()
		{
			if (this.m_current == this.m_oids.Count - 1)
			{
				return false;
			}
			this.m_current++;
			return true;
		}

		/// <summary>Sets an enumerator to its initial position.</summary>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created.</exception>
		// Token: 0x060012D2 RID: 4818 RVA: 0x00044EB7 File Offset: 0x000430B7
		public void Reset()
		{
			this.m_current = -1;
		}

		// Token: 0x040012D0 RID: 4816
		private OidCollection m_oids;

		// Token: 0x040012D1 RID: 4817
		private int m_current;
	}
}
