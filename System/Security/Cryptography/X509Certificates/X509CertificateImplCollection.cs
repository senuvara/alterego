﻿using System;
using System.Collections.Generic;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020002A4 RID: 676
	internal class X509CertificateImplCollection : IDisposable
	{
		// Token: 0x060013DA RID: 5082 RVA: 0x000492D1 File Offset: 0x000474D1
		public X509CertificateImplCollection()
		{
			this.list = new List<X509CertificateImpl>();
		}

		// Token: 0x060013DB RID: 5083 RVA: 0x000492E4 File Offset: 0x000474E4
		private X509CertificateImplCollection(X509CertificateImplCollection other)
		{
			this.list = new List<X509CertificateImpl>();
			foreach (X509CertificateImpl x509CertificateImpl in other.list)
			{
				this.list.Add(x509CertificateImpl.Clone());
			}
		}

		// Token: 0x17000411 RID: 1041
		// (get) Token: 0x060013DC RID: 5084 RVA: 0x00049354 File Offset: 0x00047554
		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000412 RID: 1042
		public X509CertificateImpl this[int index]
		{
			get
			{
				return this.list[index];
			}
		}

		// Token: 0x060013DE RID: 5086 RVA: 0x0004936F File Offset: 0x0004756F
		public void Add(X509CertificateImpl impl, bool takeOwnership)
		{
			if (!takeOwnership)
			{
				impl = impl.Clone();
			}
			this.list.Add(impl);
		}

		// Token: 0x060013DF RID: 5087 RVA: 0x00049388 File Offset: 0x00047588
		public X509CertificateImplCollection Clone()
		{
			return new X509CertificateImplCollection(this);
		}

		// Token: 0x060013E0 RID: 5088 RVA: 0x00049390 File Offset: 0x00047590
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060013E1 RID: 5089 RVA: 0x000493A0 File Offset: 0x000475A0
		protected virtual void Dispose(bool disposing)
		{
			foreach (X509CertificateImpl x509CertificateImpl in this.list)
			{
				try
				{
					x509CertificateImpl.Dispose();
				}
				catch
				{
				}
			}
			this.list.Clear();
		}

		// Token: 0x060013E2 RID: 5090 RVA: 0x00049410 File Offset: 0x00047610
		~X509CertificateImplCollection()
		{
			this.Dispose(false);
		}

		// Token: 0x04001388 RID: 5000
		private List<X509CertificateImpl> list;
	}
}
