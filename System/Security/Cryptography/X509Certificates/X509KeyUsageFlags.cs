﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Defines how the certificate key can be used. If this value is not defined, the key can be used for any purpose.</summary>
	// Token: 0x02000291 RID: 657
	[Flags]
	public enum X509KeyUsageFlags
	{
		/// <summary>No key usage parameters.</summary>
		// Token: 0x04001330 RID: 4912
		None = 0,
		/// <summary>The key can be used for encryption only.</summary>
		// Token: 0x04001331 RID: 4913
		EncipherOnly = 1,
		/// <summary>The key can be used to sign a certificate revocation list (CRL).</summary>
		// Token: 0x04001332 RID: 4914
		CrlSign = 2,
		/// <summary>The key can be used to sign certificates.</summary>
		// Token: 0x04001333 RID: 4915
		KeyCertSign = 4,
		/// <summary>The key can be used to determine key agreement, such as a key created using the Diffie-Hellman key agreement algorithm.</summary>
		// Token: 0x04001334 RID: 4916
		KeyAgreement = 8,
		/// <summary>The key can be used for data encryption.</summary>
		// Token: 0x04001335 RID: 4917
		DataEncipherment = 16,
		/// <summary>The key can be used for key encryption.</summary>
		// Token: 0x04001336 RID: 4918
		KeyEncipherment = 32,
		/// <summary>The key can be used for authentication.</summary>
		// Token: 0x04001337 RID: 4919
		NonRepudiation = 64,
		/// <summary>The key can be used as a digital signature.</summary>
		// Token: 0x04001338 RID: 4920
		DigitalSignature = 128,
		/// <summary>The key can be used for decryption only.</summary>
		// Token: 0x04001339 RID: 4921
		DecipherOnly = 32768
	}
}
