﻿using System;
using System.Collections;
using Unity;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Supports a simple iteration over an <see cref="T:System.Security.Cryptography.X509Certificates.X509ChainElementCollection" />. This class cannot be inherited.</summary>
	// Token: 0x020002A8 RID: 680
	public sealed class X509ChainElementEnumerator : IEnumerator
	{
		// Token: 0x0600140C RID: 5132 RVA: 0x00049944 File Offset: 0x00047B44
		internal X509ChainElementEnumerator(IEnumerable enumerable)
		{
			this.enumerator = enumerable.GetEnumerator();
		}

		/// <summary>Gets the current element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ChainElementCollection" />.</summary>
		/// <returns>The current element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ChainElementCollection" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first element of the collection or after the last element. </exception>
		// Token: 0x17000422 RID: 1058
		// (get) Token: 0x0600140D RID: 5133 RVA: 0x00049958 File Offset: 0x00047B58
		public X509ChainElement Current
		{
			get
			{
				return (X509ChainElement)this.enumerator.Current;
			}
		}

		/// <summary>Gets the current element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ChainElementCollection" />.</summary>
		/// <returns>The current element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ChainElementCollection" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first element of the collection or after the last element. </exception>
		// Token: 0x17000423 RID: 1059
		// (get) Token: 0x0600140E RID: 5134 RVA: 0x0004996A File Offset: 0x00047B6A
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		/// <summary>Advances the enumerator to the next element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ChainElementCollection" />.</summary>
		/// <returns>
		///     <see langword="true" /> if the enumerator was successfully advanced to the next element; <see langword="false" /> if the enumerator has passed the end of the collection.</returns>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
		// Token: 0x0600140F RID: 5135 RVA: 0x00049977 File Offset: 0x00047B77
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		/// <summary>Sets the enumerator to its initial position, which is before the first element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ChainElementCollection" />.</summary>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
		// Token: 0x06001410 RID: 5136 RVA: 0x00049984 File Offset: 0x00047B84
		public void Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x06001411 RID: 5137 RVA: 0x000092E2 File Offset: 0x000074E2
		internal X509ChainElementEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400138F RID: 5007
		private IEnumerator enumerator;
	}
}
