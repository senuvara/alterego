﻿using System;
using System.IO;
using Mono.Security.X509;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020002B1 RID: 689
	internal static class X509Helper2
	{
		// Token: 0x06001482 RID: 5250 RVA: 0x0004B75C File Offset: 0x0004995C
		internal static long GetSubjectNameHash(X509Certificate certificate)
		{
			return X509Helper2.GetSubjectNameHash(certificate.Impl);
		}

		// Token: 0x06001483 RID: 5251 RVA: 0x0004B76C File Offset: 0x0004996C
		internal static long GetSubjectNameHash(X509CertificateImpl impl)
		{
			long subjectNameHash;
			using (X509Certificate nativeInstance = X509Helper2.GetNativeInstance(impl))
			{
				subjectNameHash = X509Helper2.GetSubjectNameHash(nativeInstance);
			}
			return subjectNameHash;
		}

		// Token: 0x06001484 RID: 5252 RVA: 0x0004B7A4 File Offset: 0x000499A4
		internal static void ExportAsPEM(X509Certificate certificate, Stream stream, bool includeHumanReadableForm)
		{
			X509Helper2.ExportAsPEM(certificate.Impl, stream, includeHumanReadableForm);
		}

		// Token: 0x06001485 RID: 5253 RVA: 0x0004B7B4 File Offset: 0x000499B4
		internal static void ExportAsPEM(X509CertificateImpl impl, Stream stream, bool includeHumanReadableForm)
		{
			using (X509Certificate nativeInstance = X509Helper2.GetNativeInstance(impl))
			{
				X509Helper2.ExportAsPEM(nativeInstance, stream, includeHumanReadableForm);
			}
		}

		// Token: 0x06001486 RID: 5254 RVA: 0x0004B7EC File Offset: 0x000499EC
		internal static void Initialize()
		{
			X509Helper.InstallNativeHelper(new X509Helper2.MyNativeHelper());
		}

		// Token: 0x06001487 RID: 5255 RVA: 0x0004B7F8 File Offset: 0x000499F8
		internal static void ThrowIfContextInvalid(X509CertificateImpl impl)
		{
			X509Helper.ThrowIfContextInvalid(impl);
		}

		// Token: 0x06001488 RID: 5256 RVA: 0x0004B800 File Offset: 0x00049A00
		private static X509Certificate GetNativeInstance(X509CertificateImpl impl)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x06001489 RID: 5257 RVA: 0x0004B807 File Offset: 0x00049A07
		internal static X509Certificate2Impl Import(byte[] rawData, string password, X509KeyStorageFlags keyStorageFlags, bool disableProvider = false)
		{
			X509Certificate2ImplMono x509Certificate2ImplMono = new X509Certificate2ImplMono();
			x509Certificate2ImplMono.Import(rawData, password, keyStorageFlags);
			return x509Certificate2ImplMono;
		}

		// Token: 0x0600148A RID: 5258 RVA: 0x0004B818 File Offset: 0x00049A18
		internal static X509Certificate2Impl Import(X509Certificate cert, bool disableProvider = false)
		{
			X509Certificate2Impl x509Certificate2Impl = cert.Impl as X509Certificate2Impl;
			if (x509Certificate2Impl != null)
			{
				return (X509Certificate2Impl)x509Certificate2Impl.Clone();
			}
			return X509Helper2.Import(cert.GetRawCertData(), null, X509KeyStorageFlags.DefaultKeySet, false);
		}

		// Token: 0x0600148B RID: 5259 RVA: 0x0004B850 File Offset: 0x00049A50
		[MonoTODO("Investigate replacement; see comments in source.")]
		internal static X509Certificate GetMonoCertificate(X509Certificate2 certificate)
		{
			X509Certificate2Impl x509Certificate2Impl = certificate.Impl;
			if (x509Certificate2Impl == null)
			{
				x509Certificate2Impl = X509Helper2.Import(certificate, true);
			}
			X509Certificate2ImplMono x509Certificate2ImplMono = x509Certificate2Impl.FallbackImpl as X509Certificate2ImplMono;
			if (x509Certificate2ImplMono == null)
			{
				throw new NotSupportedException();
			}
			return x509Certificate2ImplMono.MonoCertificate;
		}

		// Token: 0x0600148C RID: 5260 RVA: 0x0004B888 File Offset: 0x00049A88
		internal static X509ChainImpl CreateChainImpl(bool useMachineContext)
		{
			return new X509ChainImplMono(useMachineContext);
		}

		// Token: 0x0600148D RID: 5261 RVA: 0x0004B890 File Offset: 0x00049A90
		public static bool IsValid(X509ChainImpl impl)
		{
			return impl != null && impl.IsValid;
		}

		// Token: 0x0600148E RID: 5262 RVA: 0x0004B89D File Offset: 0x00049A9D
		internal static void ThrowIfContextInvalid(X509ChainImpl impl)
		{
			if (!X509Helper2.IsValid(impl))
			{
				throw X509Helper2.GetInvalidChainContextException();
			}
		}

		// Token: 0x0600148F RID: 5263 RVA: 0x0004B8AD File Offset: 0x00049AAD
		internal static Exception GetInvalidChainContextException()
		{
			return new CryptographicException(Locale.GetText("Chain instance is empty."));
		}

		// Token: 0x020002B2 RID: 690
		private class MyNativeHelper : INativeCertificateHelper
		{
			// Token: 0x06001490 RID: 5264 RVA: 0x0004B8BE File Offset: 0x00049ABE
			public X509CertificateImpl Import(byte[] data, string password, X509KeyStorageFlags flags)
			{
				return X509Helper2.Import(data, password, flags, false);
			}

			// Token: 0x06001491 RID: 5265 RVA: 0x0004B8C9 File Offset: 0x00049AC9
			public X509CertificateImpl Import(X509Certificate cert)
			{
				return X509Helper2.Import(cert, false);
			}

			// Token: 0x06001492 RID: 5266 RVA: 0x0000232F File Offset: 0x0000052F
			public MyNativeHelper()
			{
			}
		}
	}
}
