﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000297 RID: 663
	internal class X509Utils
	{
		// Token: 0x060012FA RID: 4858 RVA: 0x0000232F File Offset: 0x0000052F
		private X509Utils()
		{
		}

		// Token: 0x060012FB RID: 4859 RVA: 0x0004618E File Offset: 0x0004438E
		internal static string FindOidInfo(uint keyType, string keyValue, OidGroup oidGroup)
		{
			if (keyValue == null)
			{
				throw new ArgumentNullException("keyValue");
			}
			if (keyValue.Length == 0)
			{
				return null;
			}
			if (keyType == 1U)
			{
				return CAPI.CryptFindOIDInfoNameFromKey(keyValue, oidGroup);
			}
			if (keyType != 2U)
			{
				throw new NotImplementedException(keyType.ToString());
			}
			return CAPI.CryptFindOIDInfoKeyFromName(keyValue, oidGroup);
		}

		// Token: 0x060012FC RID: 4860 RVA: 0x000461D0 File Offset: 0x000443D0
		internal static string FindOidInfoWithFallback(uint key, string value, OidGroup group)
		{
			string text = X509Utils.FindOidInfo(key, value, group);
			if (text == null && group != OidGroup.All)
			{
				text = X509Utils.FindOidInfo(key, value, OidGroup.All);
			}
			return text;
		}
	}
}
