﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using Mono.Security;
using Mono.Security.Cryptography;
using Mono.Security.X509;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020002A1 RID: 673
	internal class X509Certificate2ImplMono : X509Certificate2Impl
	{
		// Token: 0x17000400 RID: 1024
		// (get) Token: 0x06001393 RID: 5011 RVA: 0x00048188 File Offset: 0x00046388
		public override bool IsValid
		{
			get
			{
				return this._cert != null;
			}
		}

		// Token: 0x17000401 RID: 1025
		// (get) Token: 0x06001394 RID: 5012 RVA: 0x00048193 File Offset: 0x00046393
		public override IntPtr Handle
		{
			get
			{
				return IntPtr.Zero;
			}
		}

		// Token: 0x06001395 RID: 5013 RVA: 0x00048193 File Offset: 0x00046393
		public override IntPtr GetNativeAppleCertificate()
		{
			return IntPtr.Zero;
		}

		// Token: 0x06001396 RID: 5014 RVA: 0x0004819A File Offset: 0x0004639A
		private X509Certificate2ImplMono(X509Certificate cert)
		{
			this._cert = cert;
		}

		// Token: 0x06001397 RID: 5015 RVA: 0x000481A9 File Offset: 0x000463A9
		private X509Certificate2ImplMono(X509Certificate2ImplMono other)
		{
			this._cert = other._cert;
			if (other.intermediateCerts != null)
			{
				this.intermediateCerts = other.intermediateCerts.Clone();
			}
		}

		// Token: 0x06001398 RID: 5016 RVA: 0x000481D6 File Offset: 0x000463D6
		public override X509CertificateImpl Clone()
		{
			base.ThrowIfContextInvalid();
			return new X509Certificate2ImplMono(this);
		}

		// Token: 0x06001399 RID: 5017 RVA: 0x000481E4 File Offset: 0x000463E4
		public override string GetIssuerName(bool legacyV1Mode)
		{
			base.ThrowIfContextInvalid();
			if (legacyV1Mode)
			{
				return this._cert.IssuerName;
			}
			return X501.ToString(this._cert.GetIssuerName(), true, ", ", true);
		}

		// Token: 0x0600139A RID: 5018 RVA: 0x00048212 File Offset: 0x00046412
		public override string GetSubjectName(bool legacyV1Mode)
		{
			base.ThrowIfContextInvalid();
			if (legacyV1Mode)
			{
				return this._cert.SubjectName;
			}
			return X501.ToString(this._cert.GetSubjectName(), true, ", ", true);
		}

		// Token: 0x0600139B RID: 5019 RVA: 0x00048240 File Offset: 0x00046440
		public override byte[] GetRawCertData()
		{
			base.ThrowIfContextInvalid();
			return this._cert.RawData;
		}

		// Token: 0x0600139C RID: 5020 RVA: 0x00048253 File Offset: 0x00046453
		protected override byte[] GetCertHash(bool lazy)
		{
			base.ThrowIfContextInvalid();
			return SHA1.Create().ComputeHash(this._cert.RawData);
		}

		// Token: 0x0600139D RID: 5021 RVA: 0x00048270 File Offset: 0x00046470
		public override DateTime GetValidFrom()
		{
			base.ThrowIfContextInvalid();
			return this._cert.ValidFrom;
		}

		// Token: 0x0600139E RID: 5022 RVA: 0x00048283 File Offset: 0x00046483
		public override DateTime GetValidUntil()
		{
			base.ThrowIfContextInvalid();
			return this._cert.ValidUntil;
		}

		// Token: 0x0600139F RID: 5023 RVA: 0x00048296 File Offset: 0x00046496
		public override bool Equals(X509CertificateImpl other, out bool result)
		{
			result = false;
			return false;
		}

		// Token: 0x060013A0 RID: 5024 RVA: 0x0004829C File Offset: 0x0004649C
		public override string GetKeyAlgorithm()
		{
			base.ThrowIfContextInvalid();
			return this._cert.KeyAlgorithm;
		}

		// Token: 0x060013A1 RID: 5025 RVA: 0x000482AF File Offset: 0x000464AF
		public override byte[] GetKeyAlgorithmParameters()
		{
			base.ThrowIfContextInvalid();
			return this._cert.KeyAlgorithmParameters;
		}

		// Token: 0x060013A2 RID: 5026 RVA: 0x000482C2 File Offset: 0x000464C2
		public override byte[] GetPublicKey()
		{
			base.ThrowIfContextInvalid();
			return this._cert.PublicKey;
		}

		// Token: 0x060013A3 RID: 5027 RVA: 0x000482D5 File Offset: 0x000464D5
		public override byte[] GetSerialNumber()
		{
			base.ThrowIfContextInvalid();
			return this._cert.SerialNumber;
		}

		// Token: 0x060013A4 RID: 5028 RVA: 0x000482E8 File Offset: 0x000464E8
		public override byte[] Export(X509ContentType contentType, byte[] password)
		{
			base.ThrowIfContextInvalid();
			switch (contentType)
			{
			case X509ContentType.Cert:
				return this.GetRawCertData();
			case X509ContentType.SerializedCert:
				throw new NotSupportedException();
			case X509ContentType.Pfx:
				throw new NotSupportedException();
			default:
				throw new CryptographicException(Locale.GetText("This certificate format '{0}' cannot be exported.", new object[]
				{
					contentType
				}));
			}
		}

		// Token: 0x060013A5 RID: 5029 RVA: 0x00048342 File Offset: 0x00046542
		public X509Certificate2ImplMono()
		{
			this._cert = null;
		}

		// Token: 0x17000402 RID: 1026
		// (get) Token: 0x060013A6 RID: 5030 RVA: 0x00048351 File Offset: 0x00046551
		// (set) Token: 0x060013A7 RID: 5031 RVA: 0x0004836C File Offset: 0x0004656C
		public override bool Archived
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				return this._archived;
			}
			set
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				this._archived = value;
			}
		}

		// Token: 0x17000403 RID: 1027
		// (get) Token: 0x060013A8 RID: 5032 RVA: 0x00048388 File Offset: 0x00046588
		public override X509ExtensionCollection Extensions
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				if (this._extensions == null)
				{
					this._extensions = new X509ExtensionCollection(this._cert);
				}
				return this._extensions;
			}
		}

		// Token: 0x17000404 RID: 1028
		// (get) Token: 0x060013A9 RID: 5033 RVA: 0x000483BC File Offset: 0x000465BC
		public override bool HasPrivateKey
		{
			get
			{
				return this.PrivateKey != null;
			}
		}

		// Token: 0x17000405 RID: 1029
		// (get) Token: 0x060013AA RID: 5034 RVA: 0x000483C7 File Offset: 0x000465C7
		public override X500DistinguishedName IssuerName
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				if (this.issuer_name == null)
				{
					this.issuer_name = new X500DistinguishedName(this._cert.GetIssuerName().GetBytes());
				}
				return this.issuer_name;
			}
		}

		// Token: 0x17000406 RID: 1030
		// (get) Token: 0x060013AB RID: 5035 RVA: 0x00048408 File Offset: 0x00046608
		// (set) Token: 0x060013AC RID: 5036 RVA: 0x00048508 File Offset: 0x00046708
		public override AsymmetricAlgorithm PrivateKey
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				try
				{
					if (this._cert.RSA != null)
					{
						RSACryptoServiceProvider rsacryptoServiceProvider = this._cert.RSA as RSACryptoServiceProvider;
						if (rsacryptoServiceProvider != null)
						{
							return rsacryptoServiceProvider.PublicOnly ? null : rsacryptoServiceProvider;
						}
						RSAManaged rsamanaged = this._cert.RSA as RSAManaged;
						if (rsamanaged != null)
						{
							return rsamanaged.PublicOnly ? null : rsamanaged;
						}
						this._cert.RSA.ExportParameters(true);
						return this._cert.RSA;
					}
					else if (this._cert.DSA != null)
					{
						DSACryptoServiceProvider dsacryptoServiceProvider = this._cert.DSA as DSACryptoServiceProvider;
						if (dsacryptoServiceProvider != null)
						{
							return dsacryptoServiceProvider.PublicOnly ? null : dsacryptoServiceProvider;
						}
						this._cert.DSA.ExportParameters(true);
						return this._cert.DSA;
					}
				}
				catch
				{
				}
				return null;
			}
			set
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				if (value == null)
				{
					this._cert.RSA = null;
					this._cert.DSA = null;
					return;
				}
				if (value is RSA)
				{
					this._cert.RSA = (RSA)value;
					return;
				}
				if (value is DSA)
				{
					this._cert.DSA = (DSA)value;
					return;
				}
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x060013AD RID: 5037 RVA: 0x00048580 File Offset: 0x00046780
		public override PublicKey PublicKey
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				if (this._publicKey == null)
				{
					try
					{
						this._publicKey = new PublicKey(this._cert);
					}
					catch (Exception inner)
					{
						throw new CryptographicException(Locale.GetText("Unable to decode public key."), inner);
					}
				}
				return this._publicKey;
			}
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x060013AE RID: 5038 RVA: 0x000485E4 File Offset: 0x000467E4
		public override Oid SignatureAlgorithm
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				if (this.signature_algorithm == null)
				{
					this.signature_algorithm = new Oid(this._cert.SignatureAlgorithm);
				}
				return this.signature_algorithm;
			}
		}

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x060013AF RID: 5039 RVA: 0x0004861D File Offset: 0x0004681D
		public override X500DistinguishedName SubjectName
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				if (this.subject_name == null)
				{
					this.subject_name = new X500DistinguishedName(this._cert.GetSubjectName().GetBytes());
				}
				return this.subject_name;
			}
		}

		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x060013B0 RID: 5040 RVA: 0x0004865B File Offset: 0x0004685B
		public override int Version
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				return this._cert.Version;
			}
		}

		// Token: 0x060013B1 RID: 5041 RVA: 0x0004867C File Offset: 0x0004687C
		[MonoTODO("always return String.Empty for UpnName, DnsFromAlternativeName and UrlName")]
		public override string GetNameInfo(X509NameType nameType, bool forIssuer)
		{
			switch (nameType)
			{
			case X509NameType.SimpleName:
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2ImplMono.empty_error);
				}
				ASN1 asn = forIssuer ? this._cert.GetIssuerName() : this._cert.GetSubjectName();
				ASN1 asn2 = this.Find(X509Certificate2ImplMono.commonName, asn);
				if (asn2 != null)
				{
					return this.GetValueAsString(asn2);
				}
				if (asn.Count == 0)
				{
					return string.Empty;
				}
				ASN1 asn3 = asn[asn.Count - 1];
				if (asn3.Count == 0)
				{
					return string.Empty;
				}
				return this.GetValueAsString(asn3[0]);
			}
			case X509NameType.EmailName:
			{
				ASN1 asn4 = this.Find(X509Certificate2ImplMono.email, forIssuer ? this._cert.GetIssuerName() : this._cert.GetSubjectName());
				if (asn4 != null)
				{
					return this.GetValueAsString(asn4);
				}
				return string.Empty;
			}
			case X509NameType.UpnName:
				return string.Empty;
			case X509NameType.DnsName:
			{
				ASN1 asn5 = this.Find(X509Certificate2ImplMono.commonName, forIssuer ? this._cert.GetIssuerName() : this._cert.GetSubjectName());
				if (asn5 != null)
				{
					return this.GetValueAsString(asn5);
				}
				return string.Empty;
			}
			case X509NameType.DnsFromAlternativeName:
				return string.Empty;
			case X509NameType.UrlName:
				return string.Empty;
			default:
				throw new ArgumentException("nameType");
			}
		}

		// Token: 0x060013B2 RID: 5042 RVA: 0x000487BC File Offset: 0x000469BC
		private ASN1 Find(byte[] oid, ASN1 dn)
		{
			if (dn.Count == 0)
			{
				return null;
			}
			for (int i = 0; i < dn.Count; i++)
			{
				ASN1 asn = dn[i];
				for (int j = 0; j < asn.Count; j++)
				{
					ASN1 asn2 = asn[j];
					if (asn2.Count == 2)
					{
						ASN1 asn3 = asn2[0];
						if (asn3 != null && asn3.CompareValue(oid))
						{
							return asn2;
						}
					}
				}
			}
			return null;
		}

		// Token: 0x060013B3 RID: 5043 RVA: 0x00048828 File Offset: 0x00046A28
		private string GetValueAsString(ASN1 pair)
		{
			if (pair.Count != 2)
			{
				return string.Empty;
			}
			ASN1 asn = pair[1];
			if (asn.Value == null || asn.Length == 0)
			{
				return string.Empty;
			}
			if (asn.Tag == 30)
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 1; i < asn.Value.Length; i += 2)
				{
					stringBuilder.Append((char)asn.Value[i]);
				}
				return stringBuilder.ToString();
			}
			return Encoding.UTF8.GetString(asn.Value);
		}

		// Token: 0x060013B4 RID: 5044 RVA: 0x000488AC File Offset: 0x00046AAC
		private X509Certificate ImportPkcs12(byte[] rawData, string password)
		{
			PKCS12 pkcs = null;
			if (string.IsNullOrEmpty(password))
			{
				try
				{
					pkcs = new PKCS12(rawData, null);
					goto IL_2B;
				}
				catch
				{
					pkcs = new PKCS12(rawData, string.Empty);
					goto IL_2B;
				}
			}
			pkcs = new PKCS12(rawData, password);
			IL_2B:
			if (pkcs.Certificates.Count == 0)
			{
				return null;
			}
			if (pkcs.Keys.Count == 0)
			{
				return pkcs.Certificates[0];
			}
			X509Certificate x509Certificate = null;
			AsymmetricAlgorithm asymmetricAlgorithm = pkcs.Keys[0] as AsymmetricAlgorithm;
			string a = asymmetricAlgorithm.ToXmlString(false);
			foreach (X509Certificate x509Certificate2 in pkcs.Certificates)
			{
				if ((x509Certificate2.RSA != null && a == x509Certificate2.RSA.ToXmlString(false)) || (x509Certificate2.DSA != null && a == x509Certificate2.DSA.ToXmlString(false)))
				{
					x509Certificate = x509Certificate2;
					break;
				}
			}
			if (x509Certificate == null)
			{
				x509Certificate = pkcs.Certificates[0];
			}
			else
			{
				x509Certificate.RSA = (asymmetricAlgorithm as RSA);
				x509Certificate.DSA = (asymmetricAlgorithm as DSA);
			}
			if (pkcs.Certificates.Count > 1)
			{
				this.intermediateCerts = new X509CertificateImplCollection();
				foreach (X509Certificate x509Certificate3 in pkcs.Certificates)
				{
					if (x509Certificate3 != x509Certificate)
					{
						X509Certificate2ImplMono impl = new X509Certificate2ImplMono(x509Certificate3);
						this.intermediateCerts.Add(impl, true);
					}
				}
			}
			return x509Certificate;
		}

		// Token: 0x060013B5 RID: 5045 RVA: 0x00048A64 File Offset: 0x00046C64
		[MonoTODO("missing KeyStorageFlags support")]
		public override void Import(byte[] rawData, string password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Reset();
			X509Certificate cert = null;
			if (password == null)
			{
				try
				{
					cert = new X509Certificate(rawData);
					goto IL_4A;
				}
				catch (Exception inner)
				{
					try
					{
						cert = this.ImportPkcs12(rawData, null);
					}
					catch
					{
						throw new CryptographicException(Locale.GetText("Unable to decode certificate."), inner);
					}
					goto IL_4A;
				}
			}
			try
			{
				cert = this.ImportPkcs12(rawData, password);
			}
			catch
			{
				cert = new X509Certificate(rawData);
			}
			IL_4A:
			this._cert = cert;
		}

		// Token: 0x060013B6 RID: 5046 RVA: 0x00048AEC File Offset: 0x00046CEC
		[MonoTODO("X509ContentType.SerializedCert is not supported")]
		public override byte[] Export(X509ContentType contentType, string password)
		{
			if (this._cert == null)
			{
				throw new CryptographicException(X509Certificate2ImplMono.empty_error);
			}
			switch (contentType)
			{
			case X509ContentType.Cert:
				return this._cert.RawData;
			case X509ContentType.SerializedCert:
				throw new NotSupportedException();
			case X509ContentType.Pfx:
				return this.ExportPkcs12(password);
			default:
				throw new CryptographicException(Locale.GetText("This certificate format '{0}' cannot be exported.", new object[]
				{
					contentType
				}));
			}
		}

		// Token: 0x060013B7 RID: 5047 RVA: 0x00048B5C File Offset: 0x00046D5C
		private byte[] ExportPkcs12(string password)
		{
			PKCS12 pkcs = new PKCS12();
			byte[] bytes;
			try
			{
				Hashtable hashtable = new Hashtable();
				ArrayList arrayList = new ArrayList();
				ArrayList arrayList2 = arrayList;
				byte[] array = new byte[4];
				array[0] = 1;
				arrayList2.Add(array);
				hashtable.Add("1.2.840.113549.1.9.21", arrayList);
				if (password != null)
				{
					pkcs.Password = password;
				}
				pkcs.AddCertificate(this._cert, hashtable);
				AsymmetricAlgorithm privateKey = this.PrivateKey;
				if (privateKey != null)
				{
					pkcs.AddPkcs8ShroudedKeyBag(privateKey, hashtable);
				}
				bytes = pkcs.GetBytes();
			}
			finally
			{
				pkcs.Password = null;
			}
			return bytes;
		}

		// Token: 0x060013B8 RID: 5048 RVA: 0x00048BE8 File Offset: 0x00046DE8
		public override void Reset()
		{
			this._cert = null;
			this._archived = false;
			this._extensions = null;
			this._publicKey = null;
			this.issuer_name = null;
			this.subject_name = null;
			this.signature_algorithm = null;
			if (this.intermediateCerts != null)
			{
				this.intermediateCerts.Dispose();
				this.intermediateCerts = null;
			}
		}

		// Token: 0x060013B9 RID: 5049 RVA: 0x00048C40 File Offset: 0x00046E40
		public override string ToString()
		{
			if (this._cert == null)
			{
				return "System.Security.Cryptography.X509Certificates.X509Certificate2";
			}
			return this.ToString(true);
		}

		// Token: 0x060013BA RID: 5050 RVA: 0x00048C58 File Offset: 0x00046E58
		public override string ToString(bool verbose)
		{
			if (this._cert == null)
			{
				return "System.Security.Cryptography.X509Certificates.X509Certificate2";
			}
			string newLine = Environment.NewLine;
			StringBuilder stringBuilder = new StringBuilder();
			if (!verbose)
			{
				stringBuilder.AppendFormat("[Subject]{0}  {1}{0}{0}", newLine, this.GetSubjectName(false));
				stringBuilder.AppendFormat("[Issuer]{0}  {1}{0}{0}", newLine, this.GetIssuerName(false));
				stringBuilder.AppendFormat("[Not Before]{0}  {1}{0}{0}", newLine, this.GetValidFrom().ToLocalTime());
				stringBuilder.AppendFormat("[Not After]{0}  {1}{0}{0}", newLine, this.GetValidUntil().ToLocalTime());
				stringBuilder.AppendFormat("[Thumbprint]{0}  {1}{0}", newLine, X509Helper.ToHexString(base.GetCertHash()));
				stringBuilder.Append(newLine);
				return stringBuilder.ToString();
			}
			stringBuilder.AppendFormat("[Version]{0}  V{1}{0}{0}", newLine, this.Version);
			stringBuilder.AppendFormat("[Subject]{0}  {1}{0}{0}", newLine, this.GetSubjectName(false));
			stringBuilder.AppendFormat("[Issuer]{0}  {1}{0}{0}", newLine, this.GetIssuerName(false));
			stringBuilder.AppendFormat("[Serial Number]{0}  {1}{0}{0}", newLine, this.GetSerialNumber());
			stringBuilder.AppendFormat("[Not Before]{0}  {1}{0}{0}", newLine, this.GetValidFrom().ToLocalTime());
			stringBuilder.AppendFormat("[Not After]{0}  {1}{0}{0}", newLine, this.GetValidUntil().ToLocalTime());
			stringBuilder.AppendFormat("[Thumbprint]{0}  {1}{0}", newLine, X509Helper.ToHexString(base.GetCertHash()));
			stringBuilder.AppendFormat("[Signature Algorithm]{0}  {1}({2}){0}{0}", newLine, this.SignatureAlgorithm.FriendlyName, this.SignatureAlgorithm.Value);
			AsymmetricAlgorithm key = this.PublicKey.Key;
			stringBuilder.AppendFormat("[Public Key]{0}  Algorithm: ", newLine);
			if (key is RSA)
			{
				stringBuilder.Append("RSA");
			}
			else if (key is DSA)
			{
				stringBuilder.Append("DSA");
			}
			else
			{
				stringBuilder.Append(key.ToString());
			}
			stringBuilder.AppendFormat("{0}  Length: {1}{0}  Key Blob: ", newLine, key.KeySize);
			X509Certificate2ImplMono.AppendBuffer(stringBuilder, this.PublicKey.EncodedKeyValue.RawData);
			stringBuilder.AppendFormat("{0}  Parameters: ", newLine);
			X509Certificate2ImplMono.AppendBuffer(stringBuilder, this.PublicKey.EncodedParameters.RawData);
			stringBuilder.Append(newLine);
			return stringBuilder.ToString();
		}

		// Token: 0x060013BB RID: 5051 RVA: 0x00048E94 File Offset: 0x00047094
		private static void AppendBuffer(StringBuilder sb, byte[] buffer)
		{
			if (buffer == null)
			{
				return;
			}
			for (int i = 0; i < buffer.Length; i++)
			{
				sb.Append(buffer[i].ToString("x2"));
				if (i < buffer.Length - 1)
				{
					sb.Append(" ");
				}
			}
		}

		// Token: 0x060013BC RID: 5052 RVA: 0x00048EDF File Offset: 0x000470DF
		[MonoTODO("by default this depends on the incomplete X509Chain")]
		public override bool Verify(X509Certificate2 thisCertificate)
		{
			if (this._cert == null)
			{
				throw new CryptographicException(X509Certificate2ImplMono.empty_error);
			}
			return X509Chain.Create().Build(thisCertificate);
		}

		// Token: 0x060013BD RID: 5053 RVA: 0x00048F04 File Offset: 0x00047104
		[MonoTODO("Detection limited to Cert, Pfx, Pkcs12, Pkcs7 and Unknown")]
		public static X509ContentType GetCertContentType(byte[] rawData)
		{
			if (rawData == null || rawData.Length == 0)
			{
				throw new ArgumentException("rawData");
			}
			X509ContentType result = X509ContentType.Unknown;
			try
			{
				ASN1 asn = new ASN1(rawData);
				if (asn.Tag != 48)
				{
					throw new CryptographicException(Locale.GetText("Unable to decode certificate."));
				}
				if (asn.Count == 0)
				{
					return result;
				}
				if (asn.Count == 3)
				{
					byte tag = asn[0].Tag;
					if (tag != 2)
					{
						if (tag == 48 && asn[1].Tag == 48 && asn[2].Tag == 3)
						{
							result = X509ContentType.Cert;
						}
					}
					else if (asn[1].Tag == 48 && asn[2].Tag == 48)
					{
						result = X509ContentType.Pfx;
					}
				}
				if (asn[0].Tag == 6 && asn[0].CompareValue(X509Certificate2ImplMono.signedData))
				{
					result = X509ContentType.Pkcs7;
				}
			}
			catch (Exception inner)
			{
				throw new CryptographicException(Locale.GetText("Unable to decode certificate."), inner);
			}
			return result;
		}

		// Token: 0x060013BE RID: 5054 RVA: 0x00049008 File Offset: 0x00047208
		[MonoTODO("Detection limited to Cert, Pfx, Pkcs12 and Unknown")]
		public static X509ContentType GetCertContentType(string fileName)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			if (fileName.Length == 0)
			{
				throw new ArgumentException("fileName");
			}
			return X509Certificate2ImplMono.GetCertContentType(File.ReadAllBytes(fileName));
		}

		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x060013BF RID: 5055 RVA: 0x00049036 File Offset: 0x00047236
		internal override X509CertificateImplCollection IntermediateCertificates
		{
			get
			{
				return this.intermediateCerts;
			}
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x060013C0 RID: 5056 RVA: 0x0004903E File Offset: 0x0004723E
		internal X509Certificate MonoCertificate
		{
			get
			{
				return this._cert;
			}
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x060013C1 RID: 5057 RVA: 0x00002068 File Offset: 0x00000268
		internal override X509Certificate2Impl FallbackImpl
		{
			get
			{
				return this;
			}
		}

		// Token: 0x060013C2 RID: 5058 RVA: 0x00049048 File Offset: 0x00047248
		// Note: this type is marked as 'beforefieldinit'.
		static X509Certificate2ImplMono()
		{
		}

		// Token: 0x0400137B RID: 4987
		private bool _archived;

		// Token: 0x0400137C RID: 4988
		private X509ExtensionCollection _extensions;

		// Token: 0x0400137D RID: 4989
		private PublicKey _publicKey;

		// Token: 0x0400137E RID: 4990
		private X500DistinguishedName issuer_name;

		// Token: 0x0400137F RID: 4991
		private X500DistinguishedName subject_name;

		// Token: 0x04001380 RID: 4992
		private Oid signature_algorithm;

		// Token: 0x04001381 RID: 4993
		private X509CertificateImplCollection intermediateCerts;

		// Token: 0x04001382 RID: 4994
		private X509Certificate _cert;

		// Token: 0x04001383 RID: 4995
		private static string empty_error = Locale.GetText("Certificate instance is empty.");

		// Token: 0x04001384 RID: 4996
		private static byte[] commonName = new byte[]
		{
			85,
			4,
			3
		};

		// Token: 0x04001385 RID: 4997
		private static byte[] email = new byte[]
		{
			42,
			134,
			72,
			134,
			247,
			13,
			1,
			9,
			1
		};

		// Token: 0x04001386 RID: 4998
		private static byte[] signedData = new byte[]
		{
			42,
			134,
			72,
			134,
			247,
			13,
			1,
			7,
			2
		};
	}
}
