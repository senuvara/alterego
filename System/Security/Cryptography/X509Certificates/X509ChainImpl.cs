﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020002A9 RID: 681
	internal abstract class X509ChainImpl : IDisposable
	{
		// Token: 0x17000424 RID: 1060
		// (get) Token: 0x06001412 RID: 5138
		public abstract bool IsValid { get; }

		// Token: 0x17000425 RID: 1061
		// (get) Token: 0x06001413 RID: 5139
		public abstract IntPtr Handle { get; }

		// Token: 0x06001414 RID: 5140 RVA: 0x00049991 File Offset: 0x00047B91
		protected void ThrowIfContextInvalid()
		{
			if (!this.IsValid)
			{
				throw X509Helper2.GetInvalidChainContextException();
			}
		}

		// Token: 0x17000426 RID: 1062
		// (get) Token: 0x06001415 RID: 5141
		public abstract X509ChainElementCollection ChainElements { get; }

		// Token: 0x17000427 RID: 1063
		// (get) Token: 0x06001416 RID: 5142
		// (set) Token: 0x06001417 RID: 5143
		public abstract X509ChainPolicy ChainPolicy { get; set; }

		// Token: 0x17000428 RID: 1064
		// (get) Token: 0x06001418 RID: 5144
		public abstract X509ChainStatus[] ChainStatus { get; }

		// Token: 0x06001419 RID: 5145
		public abstract bool Build(X509Certificate2 certificate);

		// Token: 0x0600141A RID: 5146
		public abstract void Reset();

		// Token: 0x0600141B RID: 5147 RVA: 0x000499A1 File Offset: 0x00047BA1
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600141C RID: 5148 RVA: 0x0000232D File Offset: 0x0000052D
		protected virtual void Dispose(bool disposing)
		{
		}

		// Token: 0x0600141D RID: 5149 RVA: 0x000499B0 File Offset: 0x00047BB0
		~X509ChainImpl()
		{
			this.Dispose(false);
		}

		// Token: 0x0600141E RID: 5150 RVA: 0x0000232F File Offset: 0x0000052F
		protected X509ChainImpl()
		{
		}
	}
}
