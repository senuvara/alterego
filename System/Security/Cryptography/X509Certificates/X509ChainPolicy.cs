﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Represents the chain policy to be applied when building an X509 certificate chain. This class cannot be inherited.</summary>
	// Token: 0x020002AB RID: 683
	public sealed class X509ChainPolicy
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509ChainPolicy" /> class. </summary>
		// Token: 0x06001448 RID: 5192 RVA: 0x0004ABE1 File Offset: 0x00048DE1
		public X509ChainPolicy()
		{
			this.Reset();
		}

		// Token: 0x06001449 RID: 5193 RVA: 0x0004ABEF File Offset: 0x00048DEF
		internal X509ChainPolicy(X509CertificateCollection store)
		{
			this.store = store;
			this.Reset();
		}

		/// <summary>Gets a collection of object identifiers (OIDs) specifying which application policies or enhanced key usages (EKUs) the certificate supports.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.OidCollection" />  object.</returns>
		// Token: 0x17000435 RID: 1077
		// (get) Token: 0x0600144A RID: 5194 RVA: 0x0004AC04 File Offset: 0x00048E04
		public OidCollection ApplicationPolicy
		{
			get
			{
				return this.apps;
			}
		}

		/// <summary>Gets a collection of object identifiers (OIDs) specifying which certificate policies the certificate supports.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.OidCollection" /> object.</returns>
		// Token: 0x17000436 RID: 1078
		// (get) Token: 0x0600144B RID: 5195 RVA: 0x0004AC0C File Offset: 0x00048E0C
		public OidCollection CertificatePolicy
		{
			get
			{
				return this.cert;
			}
		}

		/// <summary>Represents an additional collection of certificates that can be searched by the chaining engine when validating a certificate chain.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2Collection" /> object.</returns>
		// Token: 0x17000437 RID: 1079
		// (get) Token: 0x0600144C RID: 5196 RVA: 0x0004AC14 File Offset: 0x00048E14
		// (set) Token: 0x0600144D RID: 5197 RVA: 0x0004AC9C File Offset: 0x00048E9C
		public X509Certificate2Collection ExtraStore
		{
			get
			{
				if (this.store2 != null)
				{
					return this.store2;
				}
				this.store2 = new X509Certificate2Collection();
				if (this.store != null)
				{
					foreach (X509Certificate certificate in this.store)
					{
						this.store2.Add(new X509Certificate2(certificate));
					}
				}
				return this.store2;
			}
			internal set
			{
				this.store2 = value;
			}
		}

		/// <summary>Gets or sets values for X509 revocation flags.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.X509Certificates.X509RevocationFlag" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Security.Cryptography.X509Certificates.X509RevocationFlag" /> value supplied is not a valid flag. </exception>
		// Token: 0x17000438 RID: 1080
		// (get) Token: 0x0600144E RID: 5198 RVA: 0x0004ACA5 File Offset: 0x00048EA5
		// (set) Token: 0x0600144F RID: 5199 RVA: 0x0004ACAD File Offset: 0x00048EAD
		public X509RevocationFlag RevocationFlag
		{
			get
			{
				return this.rflag;
			}
			set
			{
				if (value < X509RevocationFlag.EndCertificateOnly || value > X509RevocationFlag.ExcludeRoot)
				{
					throw new ArgumentException("RevocationFlag");
				}
				this.rflag = value;
			}
		}

		/// <summary>Gets or sets values for X509 certificate revocation mode.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.X509Certificates.X509RevocationMode" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Security.Cryptography.X509Certificates.X509RevocationMode" /> value supplied is not a valid flag. </exception>
		// Token: 0x17000439 RID: 1081
		// (get) Token: 0x06001450 RID: 5200 RVA: 0x0004ACC9 File Offset: 0x00048EC9
		// (set) Token: 0x06001451 RID: 5201 RVA: 0x0004ACD1 File Offset: 0x00048ED1
		public X509RevocationMode RevocationMode
		{
			get
			{
				return this.mode;
			}
			set
			{
				if (value < X509RevocationMode.NoCheck || value > X509RevocationMode.Offline)
				{
					throw new ArgumentException("RevocationMode");
				}
				this.mode = value;
			}
		}

		/// <summary>Gets the time span that elapsed during online revocation verification or downloading the certificate revocation list (CRL).</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> object.</returns>
		// Token: 0x1700043A RID: 1082
		// (get) Token: 0x06001452 RID: 5202 RVA: 0x0004ACED File Offset: 0x00048EED
		// (set) Token: 0x06001453 RID: 5203 RVA: 0x0004ACF5 File Offset: 0x00048EF5
		public TimeSpan UrlRetrievalTimeout
		{
			get
			{
				return this.timeout;
			}
			set
			{
				this.timeout = value;
			}
		}

		/// <summary>Gets verification flags for the certificate.</summary>
		/// <returns>A value from the <see cref="T:System.Security.Cryptography.X509Certificates.X509VerificationFlags" /> enumeration.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Security.Cryptography.X509Certificates.X509VerificationFlags" /> value supplied is not a valid flag. <see cref="F:System.Security.Cryptography.X509Certificates.X509VerificationFlags.NoFlag" /> is the default value. </exception>
		// Token: 0x1700043B RID: 1083
		// (get) Token: 0x06001454 RID: 5204 RVA: 0x0004ACFE File Offset: 0x00048EFE
		// (set) Token: 0x06001455 RID: 5205 RVA: 0x0004AD06 File Offset: 0x00048F06
		public X509VerificationFlags VerificationFlags
		{
			get
			{
				return this.vflags;
			}
			set
			{
				if ((value | X509VerificationFlags.AllFlags) != X509VerificationFlags.AllFlags)
				{
					throw new ArgumentException("VerificationFlags");
				}
				this.vflags = value;
			}
		}

		/// <summary>The time that the certificate was verified expressed in local time.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> object.</returns>
		// Token: 0x1700043C RID: 1084
		// (get) Token: 0x06001456 RID: 5206 RVA: 0x0004AD28 File Offset: 0x00048F28
		// (set) Token: 0x06001457 RID: 5207 RVA: 0x0004AD30 File Offset: 0x00048F30
		public DateTime VerificationTime
		{
			get
			{
				return this.vtime;
			}
			set
			{
				this.vtime = value;
			}
		}

		/// <summary>Resets the <see cref="T:System.Security.Cryptography.X509Certificates.X509ChainPolicy" /> members to their default values.</summary>
		// Token: 0x06001458 RID: 5208 RVA: 0x0004AD3C File Offset: 0x00048F3C
		public void Reset()
		{
			this.apps = new OidCollection();
			this.cert = new OidCollection();
			this.store2 = null;
			this.rflag = X509RevocationFlag.ExcludeRoot;
			this.mode = X509RevocationMode.Online;
			this.timeout = TimeSpan.Zero;
			this.vflags = X509VerificationFlags.NoFlag;
			this.vtime = DateTime.Now;
		}

		// Token: 0x040013A0 RID: 5024
		private OidCollection apps;

		// Token: 0x040013A1 RID: 5025
		private OidCollection cert;

		// Token: 0x040013A2 RID: 5026
		private X509CertificateCollection store;

		// Token: 0x040013A3 RID: 5027
		private X509Certificate2Collection store2;

		// Token: 0x040013A4 RID: 5028
		private X509RevocationFlag rflag;

		// Token: 0x040013A5 RID: 5029
		private X509RevocationMode mode;

		// Token: 0x040013A6 RID: 5030
		private TimeSpan timeout;

		// Token: 0x040013A7 RID: 5031
		private X509VerificationFlags vflags;

		// Token: 0x040013A8 RID: 5032
		private DateTime vtime;
	}
}
