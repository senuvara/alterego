﻿using System;
using System.Collections;
using Unity;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Supports a simple iteration over a <see cref="T:System.Security.Cryptography.X509Certificates.X509ExtensionCollection" />. This class cannot be inherited.</summary>
	// Token: 0x020002B0 RID: 688
	public sealed class X509ExtensionEnumerator : IEnumerator
	{
		// Token: 0x0600147C RID: 5244 RVA: 0x0004B70F File Offset: 0x0004990F
		internal X509ExtensionEnumerator(ArrayList list)
		{
			this.enumerator = list.GetEnumerator();
		}

		/// <summary>Gets the current element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ExtensionCollection" />.</summary>
		/// <returns>The current element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ExtensionCollection" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first element of the collection or after the last element. </exception>
		// Token: 0x17000446 RID: 1094
		// (get) Token: 0x0600147D RID: 5245 RVA: 0x0004B723 File Offset: 0x00049923
		public X509Extension Current
		{
			get
			{
				return (X509Extension)this.enumerator.Current;
			}
		}

		/// <summary>Gets an object from a collection.</summary>
		/// <returns>The current element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ExtensionCollection" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first element of the collection or after the last element. </exception>
		// Token: 0x17000447 RID: 1095
		// (get) Token: 0x0600147E RID: 5246 RVA: 0x0004B735 File Offset: 0x00049935
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		/// <summary>Advances the enumerator to the next element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ExtensionCollection" />.</summary>
		/// <returns>
		///     <see langword="true" /> if the enumerator was successfully advanced to the next element; <see langword="false" /> if the enumerator has passed the end of the collection.</returns>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
		// Token: 0x0600147F RID: 5247 RVA: 0x0004B742 File Offset: 0x00049942
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		/// <summary>Sets the enumerator to its initial position, which is before the first element in the <see cref="T:System.Security.Cryptography.X509Certificates.X509ExtensionCollection" />.</summary>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
		// Token: 0x06001480 RID: 5248 RVA: 0x0004B74F File Offset: 0x0004994F
		public void Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x06001481 RID: 5249 RVA: 0x000092E2 File Offset: 0x000074E2
		internal X509ExtensionEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040013B2 RID: 5042
		private IEnumerator enumerator;
	}
}
