﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Specifies how much of the X.509 certificate chain should be included in the X.509 data.</summary>
	// Token: 0x02000290 RID: 656
	public enum X509IncludeOption
	{
		/// <summary>No X.509 chain information is included.</summary>
		// Token: 0x0400132B RID: 4907
		None,
		/// <summary>The entire X.509 chain is included except for the root certificate.</summary>
		// Token: 0x0400132C RID: 4908
		ExcludeRoot,
		/// <summary>Only the end certificate is included in the X.509 chain information.</summary>
		// Token: 0x0400132D RID: 4909
		EndCertOnly,
		/// <summary>The entire X.509 chain is included.</summary>
		// Token: 0x0400132E RID: 4910
		WholeChain
	}
}
