﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020002A0 RID: 672
	internal abstract class X509Certificate2Impl : X509CertificateImpl
	{
		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x06001380 RID: 4992
		// (set) Token: 0x06001381 RID: 4993
		public abstract bool Archived { get; set; }

		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x06001382 RID: 4994
		public abstract X509ExtensionCollection Extensions { get; }

		// Token: 0x170003F7 RID: 1015
		// (get) Token: 0x06001383 RID: 4995
		public abstract bool HasPrivateKey { get; }

		// Token: 0x170003F8 RID: 1016
		// (get) Token: 0x06001384 RID: 4996
		public abstract X500DistinguishedName IssuerName { get; }

		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x06001385 RID: 4997
		// (set) Token: 0x06001386 RID: 4998
		public abstract AsymmetricAlgorithm PrivateKey { get; set; }

		// Token: 0x170003FA RID: 1018
		// (get) Token: 0x06001387 RID: 4999
		public abstract PublicKey PublicKey { get; }

		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x06001388 RID: 5000
		public abstract Oid SignatureAlgorithm { get; }

		// Token: 0x170003FC RID: 1020
		// (get) Token: 0x06001389 RID: 5001
		public abstract X500DistinguishedName SubjectName { get; }

		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x0600138A RID: 5002
		public abstract int Version { get; }

		// Token: 0x170003FE RID: 1022
		// (get) Token: 0x0600138B RID: 5003
		internal abstract X509CertificateImplCollection IntermediateCertificates { get; }

		// Token: 0x170003FF RID: 1023
		// (get) Token: 0x0600138C RID: 5004
		internal abstract X509Certificate2Impl FallbackImpl { get; }

		// Token: 0x0600138D RID: 5005
		public abstract string GetNameInfo(X509NameType nameType, bool forIssuer);

		// Token: 0x0600138E RID: 5006
		public abstract void Import(byte[] rawData, string password, X509KeyStorageFlags keyStorageFlags);

		// Token: 0x0600138F RID: 5007
		public abstract byte[] Export(X509ContentType contentType, string password);

		// Token: 0x06001390 RID: 5008
		public abstract bool Verify(X509Certificate2 thisCertificate);

		// Token: 0x06001391 RID: 5009
		public abstract void Reset();

		// Token: 0x06001392 RID: 5010 RVA: 0x00048180 File Offset: 0x00046380
		protected X509Certificate2Impl()
		{
		}
	}
}
