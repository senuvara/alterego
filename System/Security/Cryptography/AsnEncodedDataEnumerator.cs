﻿using System;
using System.Collections;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Provides the ability to navigate through an <see cref="T:System.Security.Cryptography.AsnEncodedDataCollection" /> object. This class cannot be inherited.</summary>
	// Token: 0x02000289 RID: 649
	public sealed class AsnEncodedDataEnumerator : IEnumerator
	{
		// Token: 0x060012F4 RID: 4852 RVA: 0x0004610A File Offset: 0x0004430A
		internal AsnEncodedDataEnumerator(AsnEncodedDataCollection collection)
		{
			this._collection = collection;
			this._position = -1;
		}

		/// <summary>Gets the current <see cref="T:System.Security.Cryptography.AsnEncodedData" /> object in an <see cref="T:System.Security.Cryptography.AsnEncodedDataCollection" /> object.</summary>
		/// <returns>The current <see cref="T:System.Security.Cryptography.AsnEncodedData" /> object in the collection.</returns>
		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x060012F5 RID: 4853 RVA: 0x00046120 File Offset: 0x00044320
		public AsnEncodedData Current
		{
			get
			{
				if (this._position < 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				return this._collection[this._position];
			}
		}

		/// <summary>Gets the current <see cref="T:System.Security.Cryptography.AsnEncodedData" /> object in an <see cref="T:System.Security.Cryptography.AsnEncodedDataCollection" /> object.</summary>
		/// <returns>The current <see cref="T:System.Security.Cryptography.AsnEncodedData" /> object.</returns>
		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x060012F6 RID: 4854 RVA: 0x00046120 File Offset: 0x00044320
		object IEnumerator.Current
		{
			get
			{
				if (this._position < 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				return this._collection[this._position];
			}
		}

		/// <summary>Advances to the next <see cref="T:System.Security.Cryptography.AsnEncodedData" /> object in an <see cref="T:System.Security.Cryptography.AsnEncodedDataCollection" /> object.</summary>
		/// <returns>
		///     <see langword="true" />, if the enumerator was successfully advanced to the next element; <see langword="false" />, if the enumerator has passed the end of the collection.</returns>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created.</exception>
		// Token: 0x060012F7 RID: 4855 RVA: 0x00046144 File Offset: 0x00044344
		public bool MoveNext()
		{
			int num = this._position + 1;
			this._position = num;
			if (num < this._collection.Count)
			{
				return true;
			}
			this._position = this._collection.Count - 1;
			return false;
		}

		/// <summary>Sets an enumerator to its initial position.</summary>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created.</exception>
		// Token: 0x060012F8 RID: 4856 RVA: 0x00046185 File Offset: 0x00044385
		public void Reset()
		{
			this._position = -1;
		}

		// Token: 0x060012F9 RID: 4857 RVA: 0x000092E2 File Offset: 0x000074E2
		internal AsnEncodedDataEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040012E0 RID: 4832
		private AsnEncodedDataCollection _collection;

		// Token: 0x040012E1 RID: 4833
		private int _position;
	}
}
