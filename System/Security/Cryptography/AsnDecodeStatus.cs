﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000286 RID: 646
	internal enum AsnDecodeStatus
	{
		// Token: 0x040012D7 RID: 4823
		NotDecoded = -1,
		// Token: 0x040012D8 RID: 4824
		Ok,
		// Token: 0x040012D9 RID: 4825
		BadAsn,
		// Token: 0x040012DA RID: 4826
		BadTag,
		// Token: 0x040012DB RID: 4827
		BadLength,
		// Token: 0x040012DC RID: 4828
		InformationNotAvailable
	}
}
