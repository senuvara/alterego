﻿using System;

namespace System.Security.Permissions
{
	/// <summary>Defines permission settings for type descriptors.</summary>
	// Token: 0x02000270 RID: 624
	[Flags]
	[Serializable]
	public enum TypeDescriptorPermissionFlags
	{
		/// <summary>No permission flags are set on the type descriptor.</summary>
		// Token: 0x0400128E RID: 4750
		NoFlags = 0,
		/// <summary>The type descriptor may be called from partially trusted code.</summary>
		// Token: 0x0400128F RID: 4751
		RestrictedRegistrationAccess = 1
	}
}
