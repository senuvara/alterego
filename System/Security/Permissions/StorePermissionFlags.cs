﻿using System;

namespace System.Security.Permissions
{
	/// <summary>Specifies the permitted access to X.509 certificate stores.</summary>
	// Token: 0x02000628 RID: 1576
	[Flags]
	[Serializable]
	public enum StorePermissionFlags
	{
		/// <summary>The ability to add a certificate to a store.</summary>
		// Token: 0x040024D9 RID: 9433
		AddToStore = 32,
		/// <summary>The ability to perform all certificate and store operations.</summary>
		// Token: 0x040024DA RID: 9434
		AllFlags = 247,
		/// <summary>The ability to create a new store.</summary>
		// Token: 0x040024DB RID: 9435
		CreateStore = 1,
		/// <summary>The ability to delete a store.</summary>
		// Token: 0x040024DC RID: 9436
		DeleteStore = 2,
		/// <summary>The ability to enumerate the certificates in a store.</summary>
		// Token: 0x040024DD RID: 9437
		EnumerateCertificates = 128,
		/// <summary>The ability to enumerate the stores on a computer.</summary>
		// Token: 0x040024DE RID: 9438
		EnumerateStores = 4,
		/// <summary>Permission is not given to perform any certificate or store operations.</summary>
		// Token: 0x040024DF RID: 9439
		NoFlags = 0,
		/// <summary>The ability to open a store.</summary>
		// Token: 0x040024E0 RID: 9440
		OpenStore = 16,
		/// <summary>The ability to remove a certificate from a store.</summary>
		// Token: 0x040024E1 RID: 9441
		RemoveFromStore = 64
	}
}
