﻿using System;
using Unity;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.StorePermission" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x02000629 RID: 1577
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class StorePermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.StorePermissionAttribute" /> class with the specified security action.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x06003299 RID: 12953 RVA: 0x0000232D File Offset: 0x0000052D
		public StorePermissionAttribute(SecurityAction action)
		{
		}

		/// <summary>Gets or sets a value indicating whether the code is permitted to add to a store.</summary>
		/// <returns>
		///     <see langword="true" /> if the ability to add to a store is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C9A RID: 3226
		// (get) Token: 0x0600329A RID: 12954 RVA: 0x000A49C0 File Offset: 0x000A2BC0
		// (set) Token: 0x0600329B RID: 12955 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool AddToStore
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the code is permitted to create a store.</summary>
		/// <returns>
		///     <see langword="true" /> if the ability to create a store is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C9B RID: 3227
		// (get) Token: 0x0600329C RID: 12956 RVA: 0x000A49DC File Offset: 0x000A2BDC
		// (set) Token: 0x0600329D RID: 12957 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool CreateStore
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the code is permitted to delete a store.</summary>
		/// <returns>
		///     <see langword="true" /> if the ability to delete a store is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C9C RID: 3228
		// (get) Token: 0x0600329E RID: 12958 RVA: 0x000A49F8 File Offset: 0x000A2BF8
		// (set) Token: 0x0600329F RID: 12959 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool DeleteStore
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the code is permitted to enumerate the certificates in a store.</summary>
		/// <returns>
		///     <see langword="true" /> if the ability to enumerate certificates is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C9D RID: 3229
		// (get) Token: 0x060032A0 RID: 12960 RVA: 0x000A4A14 File Offset: 0x000A2C14
		// (set) Token: 0x060032A1 RID: 12961 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool EnumerateCertificates
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the code is permitted to enumerate stores.</summary>
		/// <returns>
		///     <see langword="true" /> if the ability to enumerate stores is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C9E RID: 3230
		// (get) Token: 0x060032A2 RID: 12962 RVA: 0x000A4A30 File Offset: 0x000A2C30
		// (set) Token: 0x060032A3 RID: 12963 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool EnumerateStores
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the store permissions.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Security.Permissions.StorePermissionFlags" /> values. The default is <see cref="F:System.Security.Permissions.StorePermissionFlags.NoFlags" />.</returns>
		// Token: 0x17000C9F RID: 3231
		// (get) Token: 0x060032A4 RID: 12964 RVA: 0x000A4A4C File Offset: 0x000A2C4C
		// (set) Token: 0x060032A5 RID: 12965 RVA: 0x000092E2 File Offset: 0x000074E2
		public StorePermissionFlags Flags
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return StorePermissionFlags.NoFlags;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the code is permitted to open a store.</summary>
		/// <returns>
		///     <see langword="true" /> if the ability to open a store is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CA0 RID: 3232
		// (get) Token: 0x060032A6 RID: 12966 RVA: 0x000A4A68 File Offset: 0x000A2C68
		// (set) Token: 0x060032A7 RID: 12967 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool OpenStore
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the code is permitted to remove a certificate from a store.</summary>
		/// <returns>
		///     <see langword="true" /> if the ability to remove a certificate from a store is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CA1 RID: 3233
		// (get) Token: 0x060032A8 RID: 12968 RVA: 0x000A4A84 File Offset: 0x000A2C84
		// (set) Token: 0x060032A9 RID: 12969 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool RemoveFromStore
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates and returns a new <see cref="T:System.Security.Permissions.StorePermission" />.</summary>
		/// <returns>A <see cref="T:System.Security.Permissions.StorePermission" /> that corresponds to the attribute.</returns>
		// Token: 0x060032AA RID: 12970 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission CreatePermission()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
