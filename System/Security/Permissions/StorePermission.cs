﻿using System;
using Unity;

namespace System.Security.Permissions
{
	/// <summary>Controls access to stores containing X.509 certificates. This class cannot be inherited.</summary>
	// Token: 0x02000627 RID: 1575
	[Serializable]
	public sealed class StorePermission : CodeAccessPermission, IUnrestrictedPermission
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.StorePermission" /> class with either fully restricted or unrestricted permission state.</summary>
		/// <param name="state">One of the <see cref="T:System.Security.Permissions.PermissionState" /> values. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="state" /> is not a valid <see cref="T:System.Security.Permissions.PermissionState" /> value. </exception>
		// Token: 0x0600328F RID: 12943 RVA: 0x000092E2 File Offset: 0x000074E2
		public StorePermission(PermissionState state)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.StorePermission" /> class with the specified access.</summary>
		/// <param name="flag">A bitwise combination of the <see cref="T:System.Security.Permissions.StorePermissionFlags" /> values. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="flag" /> is not a valid combination of <see cref="T:System.Security.Permissions.StorePermissionFlags" /> values. </exception>
		// Token: 0x06003290 RID: 12944 RVA: 0x000092E2 File Offset: 0x000074E2
		public StorePermission(StorePermissionFlags flag)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the type of <see cref="T:System.Security.Cryptography.X509Certificates.X509Store" /> access allowed by the current permission.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Security.Permissions.StorePermissionFlags" /> values.</returns>
		/// <exception cref="T:System.ArgumentException">An attempt is made to set this property to an invalid value. See <see cref="T:System.Security.Permissions.StorePermissionFlags" /> for the valid values. </exception>
		// Token: 0x17000C99 RID: 3225
		// (get) Token: 0x06003291 RID: 12945 RVA: 0x000A496C File Offset: 0x000A2B6C
		// (set) Token: 0x06003292 RID: 12946 RVA: 0x000092E2 File Offset: 0x000074E2
		public StorePermissionFlags Flags
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return StorePermissionFlags.NoFlags;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates and returns an identical copy of the current permission.</summary>
		/// <returns>A copy of the current permission.</returns>
		// Token: 0x06003293 RID: 12947 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission Copy()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Reconstructs a permission with a specified state from an XML encoding.</summary>
		/// <param name="securityElement">A <see cref="T:System.Security.SecurityElement" /> that contains the XML encoding to use to reconstruct the permission. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="securityElement" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="securityElement" /> is not a valid permission element.-or- The version number in <paramref name="securityElement" /> is not valid. </exception>
		// Token: 0x06003294 RID: 12948 RVA: 0x000092E2 File Offset: 0x000074E2
		public override void FromXml(SecurityElement securityElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates and returns a permission that is the intersection of the current permission and the specified permission.</summary>
		/// <param name="target">A permission to intersect with the current permission. It must be of the same type as the current permission. </param>
		/// <returns>A new permission that represents the intersection of the current permission and the specified permission. This new permission is <see langword="null" /> if the intersection is empty.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> s not <see langword="null" /> and is not of the same type as the current permission. </exception>
		// Token: 0x06003295 RID: 12949 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission Intersect(IPermission target)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Determines whether the current permission is a subset of the specified permission.</summary>
		/// <param name="target">A permission to test for the subset relationship. This permission must be of the same type as the current permission. </param>
		/// <returns>
		///     <see langword="true" /> if the current permission is a subset of the specified permission; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> is not <see langword="null" /> and is not of the same type as the current permission. </exception>
		// Token: 0x06003296 RID: 12950 RVA: 0x000A4988 File Offset: 0x000A2B88
		public override bool IsSubsetOf(IPermission target)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value indicating whether the current permission is unrestricted.</summary>
		/// <returns>
		///     <see langword="true" /> if the current permission is unrestricted; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003297 RID: 12951 RVA: 0x000A49A4 File Offset: 0x000A2BA4
		public bool IsUnrestricted()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Creates an XML encoding of the permission and its current state.</summary>
		/// <returns>A <see cref="T:System.Security.SecurityElement" /> that contains an XML encoding of the permission, including any state information.</returns>
		// Token: 0x06003298 RID: 12952 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override SecurityElement ToXml()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
