﻿using System;
using Unity;

namespace System.Security.Permissions
{
	/// <summary>Determines the permission flags that apply to a <see cref="T:System.ComponentModel.TypeDescriptor" />.</summary>
	// Token: 0x0200062A RID: 1578
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class TypeDescriptorPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.TypeDescriptorPermissionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" />. </summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values.</param>
		// Token: 0x060032AB RID: 12971 RVA: 0x0000232D File Offset: 0x0000052D
		public TypeDescriptorPermissionAttribute(SecurityAction action)
		{
		}

		/// <summary>Gets or sets the <see cref="T:System.Security.Permissions.TypeDescriptorPermissionFlags" /> for the <see cref="T:System.ComponentModel.TypeDescriptor" />. </summary>
		/// <returns>The <see cref="T:System.Security.Permissions.TypeDescriptorPermissionFlags" /> for the <see cref="T:System.ComponentModel.TypeDescriptor" />.</returns>
		// Token: 0x17000CA2 RID: 3234
		// (get) Token: 0x060032AC RID: 12972 RVA: 0x000A4AA0 File Offset: 0x000A2CA0
		// (set) Token: 0x060032AD RID: 12973 RVA: 0x000092E2 File Offset: 0x000074E2
		public TypeDescriptorPermissionFlags Flags
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return TypeDescriptorPermissionFlags.NoFlags;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the type descriptor can be accessed from partial trust. </summary>
		/// <returns>
		///     <see langword="true" /> if the type descriptor can be accessed from partial trust; otherwise, <see langword="false" />. </returns>
		// Token: 0x17000CA3 RID: 3235
		// (get) Token: 0x060032AE RID: 12974 RVA: 0x000A4ABC File Offset: 0x000A2CBC
		// (set) Token: 0x060032AF RID: 12975 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool RestrictedRegistrationAccess
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>When overridden in a derived class, creates a permission object that can then be serialized into binary form and persistently stored along with the <see cref="T:System.Security.Permissions.SecurityAction" /> in an assembly's metadata.</summary>
		/// <returns>A serializable permission object.</returns>
		// Token: 0x060032B0 RID: 12976 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission CreatePermission()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
