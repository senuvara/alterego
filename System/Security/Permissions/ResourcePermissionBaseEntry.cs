﻿using System;
using Unity;

namespace System.Security.Permissions
{
	/// <summary>Defines the smallest unit of a code access security permission set.</summary>
	// Token: 0x02000614 RID: 1556
	[Serializable]
	public class ResourcePermissionBaseEntry
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.ResourcePermissionBaseEntry" /> class.</summary>
		// Token: 0x060031EA RID: 12778 RVA: 0x000092E2 File Offset: 0x000074E2
		public ResourcePermissionBaseEntry()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.ResourcePermissionBaseEntry" /> class with the specified permission access and permission access path.</summary>
		/// <param name="permissionAccess">The integer representation of the permission access level enumeration value. The <see cref="P:System.Security.Permissions.ResourcePermissionBaseEntry.PermissionAccess" /> property is set to this value. </param>
		/// <param name="permissionAccessPath">The array of strings that identify the resource you are protecting. The <see cref="P:System.Security.Permissions.ResourcePermissionBaseEntry.PermissionAccessPath" /> property is set to this value. </param>
		/// <exception cref="T:System.ArgumentNullException">The specified <paramref name="permissionAccessPath" /> is <see langword="null" />. </exception>
		// Token: 0x060031EB RID: 12779 RVA: 0x000092E2 File Offset: 0x000074E2
		public ResourcePermissionBaseEntry(int permissionAccess, string[] permissionAccessPath)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an integer representation of the access level enumeration value.</summary>
		/// <returns>The access level enumeration value.</returns>
		// Token: 0x17000C68 RID: 3176
		// (get) Token: 0x060031EC RID: 12780 RVA: 0x000A45EC File Offset: 0x000A27EC
		public int PermissionAccess
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets an array of strings that identify the resource you are protecting.</summary>
		/// <returns>An array of strings that identify the resource you are protecting.</returns>
		// Token: 0x17000C69 RID: 3177
		// (get) Token: 0x060031ED RID: 12781 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string[] PermissionAccessPath
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
