﻿using System;

namespace System.Security.Authentication
{
	/// <summary>Defines the possible cipher algorithms for the <see cref="T:System.Net.Security.SslStream" /> class.</summary>
	// Token: 0x02000273 RID: 627
	public enum CipherAlgorithmType
	{
		/// <summary>No encryption algorithm is used.</summary>
		// Token: 0x04001292 RID: 4754
		None,
		/// <summary>No encryption is used with a Null cipher algorithm. </summary>
		// Token: 0x04001293 RID: 4755
		Null = 24576,
		/// <summary>The Advanced Encryption Standard (AES) algorithm.</summary>
		// Token: 0x04001294 RID: 4756
		Aes = 26129,
		/// <summary>The Advanced Encryption Standard (AES) algorithm with a 128 bit key.</summary>
		// Token: 0x04001295 RID: 4757
		Aes128 = 26126,
		/// <summary>The Advanced Encryption Standard (AES) algorithm with a 192 bit key.</summary>
		// Token: 0x04001296 RID: 4758
		Aes192,
		/// <summary>The Advanced Encryption Standard (AES) algorithm with a 256 bit key.</summary>
		// Token: 0x04001297 RID: 4759
		Aes256,
		/// <summary>The Data Encryption Standard (DES) algorithm.</summary>
		// Token: 0x04001298 RID: 4760
		Des = 26113,
		/// <summary>Rivest's Code 2 (RC2) algorithm.</summary>
		// Token: 0x04001299 RID: 4761
		Rc2,
		/// <summary>Rivest's Code 4 (RC4) algorithm.</summary>
		// Token: 0x0400129A RID: 4762
		Rc4 = 26625,
		/// <summary>The Triple Data Encryption Standard (3DES) algorithm.</summary>
		// Token: 0x0400129B RID: 4763
		TripleDes = 26115
	}
}
