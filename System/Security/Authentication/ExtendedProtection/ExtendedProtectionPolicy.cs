﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Security.Authentication.ExtendedProtection
{
	/// <summary>The <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> class represents the extended protection policy used by the server to validate incoming client connections. </summary>
	// Token: 0x0200027D RID: 637
	[TypeConverter(typeof(ExtendedProtectionPolicyTypeConverter))]
	[MonoTODO]
	[Serializable]
	public class ExtendedProtectionPolicy : ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> class that specifies when the extended protection policy should be enforced.</summary>
		/// <param name="policyEnforcement">A <see cref="T:System.Security.Authentication.ExtendedProtection.PolicyEnforcement" /> value that indicates when the extended protection policy should be enforced.</param>
		// Token: 0x060012A7 RID: 4775 RVA: 0x0000232F File Offset: 0x0000052F
		[MonoTODO("Not implemented.")]
		public ExtendedProtectionPolicy(PolicyEnforcement policyEnforcement)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> class that specifies when the extended protection policy should be enforced and the channel binding token (CBT) to be used.</summary>
		/// <param name="policyEnforcement">A <see cref="T:System.Security.Authentication.ExtendedProtection.PolicyEnforcement" /> value that indicates when the extended protection policy should be enforced.</param>
		/// <param name="customChannelBinding">A <see cref="T:System.Security.Authentication.ExtendedProtection.ChannelBinding" /> that contains a custom channel binding to use for validation.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="policyEnforcement" /> is specified as <see cref="F:System.Security.Authentication.ExtendedProtection.PolicyEnforcement.Never" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="customChannelBinding " />is <see langword="null" />. </exception>
		// Token: 0x060012A8 RID: 4776 RVA: 0x00044B4D File Offset: 0x00042D4D
		public ExtendedProtectionPolicy(PolicyEnforcement policyEnforcement, ChannelBinding customChannelBinding)
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> class that specifies when the extended protection policy should be enforced, the kind of protection enforced by the policy, and a custom Service Provider Name (SPN) list that is used to match against a client's SPN.</summary>
		/// <param name="policyEnforcement">A <see cref="T:System.Security.Authentication.ExtendedProtection.PolicyEnforcement" /> value that indicates when the extended protection policy should be enforced.</param>
		/// <param name="protectionScenario">A <see cref="T:System.Security.Authentication.ExtendedProtection.ProtectionScenario" /> value that indicates the kind of protection enforced by the policy.</param>
		/// <param name="customServiceNames">A <see cref="T:System.Collections.ICollection" /> that contains the custom SPN list that is used to match against a client's SPN.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="policyEnforcement" /> is specified as <see cref="F:System.Security.Authentication.ExtendedProtection.PolicyEnforcement.Never" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="customServiceNames " />is <see langword="null" /> or an empty list. </exception>
		// Token: 0x060012A9 RID: 4777 RVA: 0x00044B4D File Offset: 0x00042D4D
		public ExtendedProtectionPolicy(PolicyEnforcement policyEnforcement, ProtectionScenario protectionScenario, ICollection customServiceNames)
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> class that specifies when the extended protection policy should be enforced, the kind of protection enforced by the policy, and a custom Service Provider Name (SPN) list that is used to match against a client's SPN.</summary>
		/// <param name="policyEnforcement">A <see cref="T:System.Security.Authentication.ExtendedProtection.PolicyEnforcement" /> value that indicates when the extended protection policy should be enforced.</param>
		/// <param name="protectionScenario">A <see cref="T:System.Security.Authentication.ExtendedProtection.ProtectionScenario" /> value that indicates the kind of protection enforced by the policy.</param>
		/// <param name="customServiceNames">A <see cref="T:System.Security.Authentication.ExtendedProtection.ServiceNameCollection" /> that contains the custom SPN list that is used to match against a client's SPN.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="policyEnforcement" /> is specified as <see cref="F:System.Security.Authentication.ExtendedProtection.PolicyEnforcement.Never" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="customServiceNames " />is <see langword="null" /> or an empty list. </exception>
		// Token: 0x060012AA RID: 4778 RVA: 0x00044B4D File Offset: 0x00042D4D
		public ExtendedProtectionPolicy(PolicyEnforcement policyEnforcement, ProtectionScenario protectionScenario, ServiceNameCollection customServiceNames)
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> class from a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object that contains the required data to populate the <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" />.</summary>
		/// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> instance that contains the information that is required to serialize the new <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> instance.</param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains the source of the serialized stream that is associated with the new <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> instance.</param>
		// Token: 0x060012AB RID: 4779 RVA: 0x00044B4D File Offset: 0x00042D4D
		protected ExtendedProtectionPolicy(SerializationInfo info, StreamingContext context)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a custom channel binding token (CBT) to use for validation.</summary>
		/// <returns>A <see cref="T:System.Security.Authentication.ExtendedProtection.ChannelBinding" /> that contains a custom channel binding to use for validation.</returns>
		// Token: 0x170003C2 RID: 962
		// (get) Token: 0x060012AC RID: 4780 RVA: 0x000068D7 File Offset: 0x00004AD7
		public ChannelBinding CustomChannelBinding
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the custom Service Provider Name (SPN) list used to match against a client's SPN.</summary>
		/// <returns>A <see cref="T:System.Security.Authentication.ExtendedProtection.ServiceNameCollection" /> that contains the custom SPN list that is used to match against a client's SPN.</returns>
		// Token: 0x170003C3 RID: 963
		// (get) Token: 0x060012AD RID: 4781 RVA: 0x000068D7 File Offset: 0x00004AD7
		public ServiceNameCollection CustomServiceNames
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Indicates whether the operating system supports integrated windows authentication with extended protection.</summary>
		/// <returns>
		///     <see langword="true" /> if the operating system supports integrated windows authentication with extended protection, otherwise <see langword="false" />.</returns>
		// Token: 0x170003C4 RID: 964
		// (get) Token: 0x060012AE RID: 4782 RVA: 0x000068D7 File Offset: 0x00004AD7
		public static bool OSSupportsExtendedProtection
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets when the extended protection policy should be enforced.</summary>
		/// <returns>A <see cref="T:System.Security.Authentication.ExtendedProtection.PolicyEnforcement" /> value that indicates when the extended protection policy should be enforced. </returns>
		// Token: 0x170003C5 RID: 965
		// (get) Token: 0x060012AF RID: 4783 RVA: 0x000068D7 File Offset: 0x00004AD7
		public PolicyEnforcement PolicyEnforcement
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the kind of protection enforced by the extended protection policy.</summary>
		/// <returns>A <see cref="T:System.Security.Authentication.ExtendedProtection.ProtectionScenario" /> value that indicates the kind of protection enforced by the policy. </returns>
		// Token: 0x170003C6 RID: 966
		// (get) Token: 0x060012B0 RID: 4784 RVA: 0x000068D7 File Offset: 0x00004AD7
		public ProtectionScenario ProtectionScenario
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a string representation for the extended protection policy instance.</summary>
		/// <returns>A <see cref="T:System.String" /> instance that contains the representation of the <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> instance.</returns>
		// Token: 0x060012B1 RID: 4785 RVA: 0x00044B5A File Offset: 0x00042D5A
		[MonoTODO]
		public override string ToString()
		{
			return base.ToString();
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the required data to serialize an <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> object.</summary>
		/// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object that holds the serialized data for an <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> object.</param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains the destination of the serialized stream that is associated with the new <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" />.</param>
		// Token: 0x060012B2 RID: 4786 RVA: 0x000068D7 File Offset: 0x00004AD7
		[SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotImplementedException();
		}
	}
}
