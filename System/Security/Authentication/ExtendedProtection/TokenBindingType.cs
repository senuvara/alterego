﻿using System;

namespace System.Security.Authentication.ExtendedProtection
{
	/// <summary>Represents types of token binding.</summary>
	// Token: 0x02000279 RID: 633
	public enum TokenBindingType
	{
		/// <summary>Used to establish a token binding when connecting to a server.</summary>
		// Token: 0x040012B1 RID: 4785
		Provided,
		/// <summary>Used when requesting tokens to be presented to a different server.</summary>
		// Token: 0x040012B2 RID: 4786
		Referred
	}
}
