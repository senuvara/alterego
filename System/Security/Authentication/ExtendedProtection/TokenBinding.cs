﻿using System;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Security.Authentication.ExtendedProtection
{
	/// <summary>Contains APIs used for token binding.</summary>
	// Token: 0x0200027A RID: 634
	public class TokenBinding
	{
		// Token: 0x0600129F RID: 4767 RVA: 0x00044AF8 File Offset: 0x00042CF8
		internal TokenBinding(TokenBindingType bindingType, byte[] rawData)
		{
			this.BindingType = bindingType;
			this._rawTokenBindingId = rawData;
		}

		/// <summary>Gets the raw token binding Id.</summary>
		/// <returns>The raw token binding Id. The first byte of the raw Id, which represents the binding type, is removed.</returns>
		// Token: 0x060012A0 RID: 4768 RVA: 0x00044B0E File Offset: 0x00042D0E
		public byte[] GetRawTokenBindingId()
		{
			if (this._rawTokenBindingId == null)
			{
				return null;
			}
			return (byte[])this._rawTokenBindingId.Clone();
		}

		/// <summary>Gets the token binding type.</summary>
		/// <returns>The token binding type.</returns>
		// Token: 0x170003C0 RID: 960
		// (get) Token: 0x060012A1 RID: 4769 RVA: 0x00044B2A File Offset: 0x00042D2A
		// (set) Token: 0x060012A2 RID: 4770 RVA: 0x00044B32 File Offset: 0x00042D32
		public TokenBindingType BindingType
		{
			[CompilerGenerated]
			get
			{
				return this.<BindingType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<BindingType>k__BackingField = value;
			}
		}

		// Token: 0x060012A3 RID: 4771 RVA: 0x000092E2 File Offset: 0x000074E2
		internal TokenBinding()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040012B3 RID: 4787
		private byte[] _rawTokenBindingId;

		// Token: 0x040012B4 RID: 4788
		[CompilerGenerated]
		private TokenBindingType <BindingType>k__BackingField;
	}
}
