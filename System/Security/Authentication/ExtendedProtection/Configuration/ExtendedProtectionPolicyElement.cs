﻿using System;
using System.Configuration;
using Unity;

namespace System.Security.Authentication.ExtendedProtection.Configuration
{
	/// <summary>The <see cref="T:System.Security.Authentication.ExtendedProtection.Configuration.ExtendedProtectionPolicyElement" /> class represents a configuration element for an <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" />.</summary>
	// Token: 0x02000615 RID: 1557
	public sealed class ExtendedProtectionPolicyElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Authentication.ExtendedProtection.Configuration.ExtendedProtectionPolicyElement" /> class.</summary>
		// Token: 0x060031EE RID: 12782 RVA: 0x000092E2 File Offset: 0x000074E2
		public ExtendedProtectionPolicyElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the custom Service Provider Name (SPN) list used to match against a client's SPN for this configuration policy element. </summary>
		/// <returns>Returns a <see cref="T:System.Security.Authentication.ExtendedProtection.Configuration.ServiceNameElementCollection" /> that includes the custom SPN list used to match against a client's SPN.</returns>
		// Token: 0x17000C6A RID: 3178
		// (get) Token: 0x060031EF RID: 12783 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ServiceNameElementCollection CustomServiceNames
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the policy enforcement value for this configuration policy element.</summary>
		/// <returns>Returns a <see cref="T:System.Security.Authentication.ExtendedProtection.PolicyEnforcement" /> value that indicates when the extended protection policy should be enforced.</returns>
		// Token: 0x17000C6B RID: 3179
		// (get) Token: 0x060031F0 RID: 12784 RVA: 0x000A4608 File Offset: 0x000A2808
		// (set) Token: 0x060031F1 RID: 12785 RVA: 0x000092E2 File Offset: 0x000074E2
		public PolicyEnforcement PolicyEnforcement
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PolicyEnforcement.Never;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000C6C RID: 3180
		// (get) Token: 0x060031F2 RID: 12786 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the kind of protection enforced by the extended protection policy for this configuration policy element.</summary>
		/// <returns>A <see cref="T:System.Security.Authentication.ExtendedProtection.ProtectionScenario" /> value that indicates the kind of protection enforced by the policy.</returns>
		// Token: 0x17000C6D RID: 3181
		// (get) Token: 0x060031F3 RID: 12787 RVA: 0x000A4624 File Offset: 0x000A2824
		// (set) Token: 0x060031F4 RID: 12788 RVA: 0x000092E2 File Offset: 0x000074E2
		public ProtectionScenario ProtectionScenario
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ProtectionScenario.TransportSelected;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>The <see cref="M:System.Security.Authentication.ExtendedProtection.Configuration.ExtendedProtectionPolicyElement.BuildPolicy" /> method builds a new <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> instance based on the properties set on the <see cref="T:System.Security.Authentication.ExtendedProtection.Configuration.ExtendedProtectionPolicyElement" /> class. </summary>
		/// <returns>A new <see cref="T:System.Security.Authentication.ExtendedProtection.ExtendedProtectionPolicy" /> instance that represents the extended protection policy created.</returns>
		// Token: 0x060031F5 RID: 12789 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ExtendedProtectionPolicy BuildPolicy()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
