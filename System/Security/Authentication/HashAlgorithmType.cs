﻿using System;

namespace System.Security.Authentication
{
	/// <summary>Specifies the algorithm used for generating message authentication codes (MACs).</summary>
	// Token: 0x02000275 RID: 629
	public enum HashAlgorithmType
	{
		/// <summary>No hashing algorithm is used.</summary>
		// Token: 0x040012A2 RID: 4770
		None,
		/// <summary>The Message Digest 5 (MD5) hashing algorithm.</summary>
		// Token: 0x040012A3 RID: 4771
		Md5 = 32771,
		/// <summary>The Secure Hashing Algorithm (SHA1).</summary>
		// Token: 0x040012A4 RID: 4772
		Sha1,
		/// <summary>The Secure Hashing Algorithm 2 (SHA-2), using a 256-bit digest.</summary>
		// Token: 0x040012A5 RID: 4773
		Sha256 = 32780,
		/// <summary>The Secure Hashing Algorithm 2 (SHA-2), using a 384-bit digest.</summary>
		// Token: 0x040012A6 RID: 4774
		Sha384,
		/// <summary>The Secure Hashing Algorithm 2 (SHA-2), using a 512-bit digest.</summary>
		// Token: 0x040012A7 RID: 4775
		Sha512
	}
}
