﻿using System;

namespace System
{
	/// <summary>A customizable parser based on the File scheme.</summary>
	// Token: 0x020000A3 RID: 163
	public class FileStyleUriParser : UriParser
	{
		/// <summary>Creates a customizable parser based on the File scheme.</summary>
		// Token: 0x060003E9 RID: 1001 RVA: 0x00013709 File Offset: 0x00011909
		public FileStyleUriParser() : base(UriParser.FileUri.Flags)
		{
		}
	}
}
