﻿using System;

namespace System
{
	/// <summary>A customizable parser based on the File Transfer Protocol (FTP) scheme.</summary>
	// Token: 0x020000A2 RID: 162
	public class FtpStyleUriParser : UriParser
	{
		/// <summary>Creates a customizable parser based on the File Transfer Protocol (FTP) scheme.</summary>
		// Token: 0x060003E8 RID: 1000 RVA: 0x000136F7 File Offset: 0x000118F7
		public FtpStyleUriParser() : base(UriParser.FtpUri.Flags)
		{
		}
	}
}
