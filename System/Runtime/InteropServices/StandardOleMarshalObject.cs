﻿using System;
using Unity;

namespace System.Runtime.InteropServices
{
	/// <summary>Replaces the standard common language runtime (CLR) free-threaded marshaler with the standard OLE STA marshaler.</summary>
	// Token: 0x02000604 RID: 1540
	[ComVisible(true)]
	public class StandardOleMarshalObject : MarshalByRefObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.StandardOleMarshalObject" /> class. </summary>
		// Token: 0x0600314E RID: 12622 RVA: 0x000092E2 File Offset: 0x000074E2
		protected StandardOleMarshalObject()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
