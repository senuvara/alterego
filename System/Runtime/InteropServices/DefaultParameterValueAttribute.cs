﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Sets the default value of a parameter when called from a language that supports default parameters. This class cannot be inherited. </summary>
	// Token: 0x0200025D RID: 605
	[AttributeUsage(AttributeTargets.Parameter)]
	public sealed class DefaultParameterValueAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.DefaultParameterValueAttribute" /> class with the default value of a parameter.</summary>
		/// <param name="value">An object that represents the default value of a parameter.</param>
		// Token: 0x06001242 RID: 4674 RVA: 0x00044025 File Offset: 0x00042225
		public DefaultParameterValueAttribute(object value)
		{
			this.value = value;
		}

		/// <summary>Gets the default value of a parameter.</summary>
		/// <returns>An object that represents the default value of a parameter.</returns>
		// Token: 0x170003B5 RID: 949
		// (get) Token: 0x06001243 RID: 4675 RVA: 0x00044034 File Offset: 0x00042234
		public object Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x04001257 RID: 4695
		private object value;
	}
}
