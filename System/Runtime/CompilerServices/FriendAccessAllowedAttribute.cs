﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200026A RID: 618
	internal class FriendAccessAllowedAttribute : Attribute
	{
		// Token: 0x06001262 RID: 4706 RVA: 0x000020AE File Offset: 0x000002AE
		public FriendAccessAllowedAttribute()
		{
		}
	}
}
