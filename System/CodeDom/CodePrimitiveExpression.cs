﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a primitive data type value.</summary>
	// Token: 0x020006CC RID: 1740
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodePrimitiveExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodePrimitiveExpression" /> class.</summary>
		// Token: 0x0600369E RID: 13982 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodePrimitiveExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodePrimitiveExpression" /> class using the specified object.</summary>
		/// <param name="value">The object to represent. </param>
		// Token: 0x0600369F RID: 13983 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodePrimitiveExpression(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the primitive data type to represent.</summary>
		/// <returns>The primitive data type instance to represent the value of.</returns>
		// Token: 0x17000DEC RID: 3564
		// (get) Token: 0x060036A0 RID: 13984 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036A1 RID: 13985 RVA: 0x000092E2 File Offset: 0x000074E2
		public object Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
