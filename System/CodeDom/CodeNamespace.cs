﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a namespace declaration.</summary>
	// Token: 0x020005EC RID: 1516
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeNamespace : CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeNamespace" /> class.</summary>
		// Token: 0x0600304D RID: 12365 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeNamespace()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeNamespace" /> class using the specified name.</summary>
		/// <param name="name">The name of the namespace being declared. </param>
		// Token: 0x0600304E RID: 12366 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeNamespace(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the comments for the namespace.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeCommentStatementCollection" /> that indicates the comments for the namespace.</returns>
		// Token: 0x17000BFB RID: 3067
		// (get) Token: 0x0600304F RID: 12367 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeCommentStatementCollection Comments
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of namespace import directives used by the namespace.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeNamespaceImportCollection" /> that indicates the namespace import directives used by the namespace.</returns>
		// Token: 0x17000BFC RID: 3068
		// (get) Token: 0x06003050 RID: 12368 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeNamespaceImportCollection Imports
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the name of the namespace.</summary>
		/// <returns>The name of the namespace.</returns>
		// Token: 0x17000BFD RID: 3069
		// (get) Token: 0x06003051 RID: 12369 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003052 RID: 12370 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of types that the namespace contains.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeDeclarationCollection" /> that indicates the types contained in the namespace.</returns>
		// Token: 0x17000BFE RID: 3070
		// (get) Token: 0x06003053 RID: 12371 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeDeclarationCollection Types
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>An event that will be raised the first time the <see cref="P:System.CodeDom.CodeNamespace.Comments" /> collection is accessed.</summary>
		// Token: 0x14000054 RID: 84
		// (add) Token: 0x06003054 RID: 12372 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x06003055 RID: 12373 RVA: 0x000092E2 File Offset: 0x000074E2
		public event EventHandler PopulateComments
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>An event that will be raised the first time the <see cref="P:System.CodeDom.CodeNamespace.Imports" /> collection is accessed.</summary>
		// Token: 0x14000055 RID: 85
		// (add) Token: 0x06003056 RID: 12374 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x06003057 RID: 12375 RVA: 0x000092E2 File Offset: 0x000074E2
		public event EventHandler PopulateImports
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>An event that will be raised the first time the <see cref="P:System.CodeDom.CodeNamespace.Types" /> collection is accessed.</summary>
		// Token: 0x14000056 RID: 86
		// (add) Token: 0x06003058 RID: 12376 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x06003059 RID: 12377 RVA: 0x000092E2 File Offset: 0x000074E2
		public event EventHandler PopulateTypes
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
