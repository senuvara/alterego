﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a return value statement.</summary>
	// Token: 0x020006CA RID: 1738
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeMethodReturnStatement : CodeStatement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMethodReturnStatement" /> class.</summary>
		// Token: 0x06003693 RID: 13971 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMethodReturnStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMethodReturnStatement" /> class using the specified expression.</summary>
		/// <param name="expression">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the return value. </param>
		// Token: 0x06003694 RID: 13972 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMethodReturnStatement(CodeExpression expression)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the return value.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the value to return for the return statement, or <see langword="null" /> if the statement is part of a subroutine.</returns>
		// Token: 0x17000DE9 RID: 3561
		// (get) Token: 0x06003695 RID: 13973 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003696 RID: 13974 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression Expression
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
