﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents an expression that invokes a method.</summary>
	// Token: 0x020006C8 RID: 1736
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeMethodInvokeExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMethodInvokeExpression" /> class.</summary>
		// Token: 0x06003685 RID: 13957 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMethodInvokeExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMethodInvokeExpression" /> class using the specified target object, method name, and parameters.</summary>
		/// <param name="targetObject">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the target object with the method to invoke. </param>
		/// <param name="methodName">The name of the method to invoke. </param>
		/// <param name="parameters">An array of <see cref="T:System.CodeDom.CodeExpression" /> objects that indicate the parameters to call the method with. </param>
		// Token: 0x06003686 RID: 13958 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMethodInvokeExpression(CodeExpression targetObject, string methodName, CodeExpression[] parameters)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMethodInvokeExpression" /> class using the specified method and parameters.</summary>
		/// <param name="method">A <see cref="T:System.CodeDom.CodeMethodReferenceExpression" /> that indicates the method to invoke. </param>
		/// <param name="parameters">An array of <see cref="T:System.CodeDom.CodeExpression" /> objects that indicate the parameters with which to invoke the method. </param>
		// Token: 0x06003687 RID: 13959 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMethodInvokeExpression(CodeMethodReferenceExpression method, CodeExpression[] parameters)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the method to invoke.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeMethodReferenceExpression" /> that indicates the method to invoke.</returns>
		// Token: 0x17000DE4 RID: 3556
		// (get) Token: 0x06003688 RID: 13960 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003689 RID: 13961 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMethodReferenceExpression Method
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the parameters to invoke the method with.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpressionCollection" /> that indicates the parameters to invoke the method with.</returns>
		// Token: 0x17000DE5 RID: 3557
		// (get) Token: 0x0600368A RID: 13962 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeExpressionCollection Parameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
