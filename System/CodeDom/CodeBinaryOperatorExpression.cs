﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents an expression that consists of a binary operation between two expressions.</summary>
	// Token: 0x020006B2 RID: 1714
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeBinaryOperatorExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeBinaryOperatorExpression" /> class.</summary>
		// Token: 0x060035F6 RID: 13814 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeBinaryOperatorExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeBinaryOperatorExpression" /> class using the specified parameters.</summary>
		/// <param name="left">The <see cref="T:System.CodeDom.CodeExpression" /> on the left of the operator. </param>
		/// <param name="op">A <see cref="T:System.CodeDom.CodeBinaryOperatorType" /> indicating the type of operator. </param>
		/// <param name="right">The <see cref="T:System.CodeDom.CodeExpression" /> on the right of the operator. </param>
		// Token: 0x060035F7 RID: 13815 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeBinaryOperatorExpression(CodeExpression left, CodeBinaryOperatorType op, CodeExpression right)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the code expression on the left of the operator.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the left operand.</returns>
		// Token: 0x17000DB2 RID: 3506
		// (get) Token: 0x060035F8 RID: 13816 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060035F9 RID: 13817 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression Left
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the operator in the binary operator expression.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeBinaryOperatorType" /> that indicates the type of operator in the expression.</returns>
		// Token: 0x17000DB3 RID: 3507
		// (get) Token: 0x060035FA RID: 13818 RVA: 0x000A5C04 File Offset: 0x000A3E04
		// (set) Token: 0x060035FB RID: 13819 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeBinaryOperatorType Operator
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return CodeBinaryOperatorType.Add;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the code expression on the right of the operator.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the right operand.</returns>
		// Token: 0x17000DB4 RID: 3508
		// (get) Token: 0x060035FC RID: 13820 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060035FD RID: 13821 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression Right
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
