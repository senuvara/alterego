﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a type parameter of a generic type or method.</summary>
	// Token: 0x020005E2 RID: 1506
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeTypeParameter : CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeParameter" /> class. </summary>
		// Token: 0x06002FF3 RID: 12275 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeParameter()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeParameter" /> class with the specified type parameter name. </summary>
		/// <param name="name">The name of the type parameter.</param>
		// Token: 0x06002FF4 RID: 12276 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeParameter(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the constraints for the type parameter.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReferenceCollection" /> object that contains the constraints for the type parameter.</returns>
		// Token: 0x17000BEB RID: 3051
		// (get) Token: 0x06002FF5 RID: 12277 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeReferenceCollection Constraints
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the custom attributes of the type parameter.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeAttributeDeclarationCollection" /> that indicates the custom attributes of the type parameter. The default is <see langword="null" />.</returns>
		// Token: 0x17000BEC RID: 3052
		// (get) Token: 0x06002FF6 RID: 12278 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeAttributeDeclarationCollection CustomAttributes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a value indicating whether the type parameter has a constructor constraint.</summary>
		/// <returns>
		///     <see langword="true" /> if the type parameter has a constructor constraint; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000BED RID: 3053
		// (get) Token: 0x06002FF7 RID: 12279 RVA: 0x000A3CD8 File Offset: 0x000A1ED8
		// (set) Token: 0x06002FF8 RID: 12280 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool HasConstructorConstraint
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the type parameter.</summary>
		/// <returns>The name of the type parameter. The default is an empty string ("").</returns>
		// Token: 0x17000BEE RID: 3054
		// (get) Token: 0x06002FF9 RID: 12281 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002FFA RID: 12282 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
