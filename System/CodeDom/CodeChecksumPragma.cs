﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a code checksum pragma code entity.  </summary>
	// Token: 0x020006B7 RID: 1719
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeChecksumPragma : CodeDirective
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeChecksumPragma" /> class. </summary>
		// Token: 0x0600361C RID: 13852 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeChecksumPragma()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeChecksumPragma" /> class using a file name, a GUID representing the checksum algorithm, and a byte stream representing the checksum data.</summary>
		/// <param name="fileName">The path to the checksum file.</param>
		/// <param name="checksumAlgorithmId">A <see cref="T:System.Guid" /> that identifies the checksum algorithm to use.</param>
		/// <param name="checksumData">A byte array that contains the checksum data.</param>
		// Token: 0x0600361D RID: 13853 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeChecksumPragma(string fileName, Guid checksumAlgorithmId, byte[] checksumData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a GUID that identifies the checksum algorithm to use.</summary>
		/// <returns>A <see cref="T:System.Guid" /> that identifies the checksum algorithm to use.</returns>
		// Token: 0x17000DBB RID: 3515
		// (get) Token: 0x0600361E RID: 13854 RVA: 0x000A5C74 File Offset: 0x000A3E74
		// (set) Token: 0x0600361F RID: 13855 RVA: 0x000092E2 File Offset: 0x000074E2
		public Guid ChecksumAlgorithmId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the value of the data for the checksum calculation.</summary>
		/// <returns>A byte array that contains the data for the checksum calculation.</returns>
		// Token: 0x17000DBC RID: 3516
		// (get) Token: 0x06003620 RID: 13856 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003621 RID: 13857 RVA: 0x000092E2 File Offset: 0x000074E2
		public byte[] ChecksumData
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the path to the checksum file.</summary>
		/// <returns>The path to the checksum file.</returns>
		// Token: 0x17000DBD RID: 3517
		// (get) Token: 0x06003622 RID: 13858 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003623 RID: 13859 RVA: 0x000092E2 File Offset: 0x000074E2
		public string FileName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
