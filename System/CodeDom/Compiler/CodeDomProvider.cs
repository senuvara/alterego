﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Provides a base class for <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> implementations. This class is abstract. </summary>
	// Token: 0x020005F3 RID: 1523
	[ToolboxItem(false)]
	[ComVisible(true)]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public abstract class CodeDomProvider : Component
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> class. </summary>
		// Token: 0x060030B7 RID: 12471 RVA: 0x000092E2 File Offset: 0x000074E2
		protected CodeDomProvider()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the default file name extension to use for source code files in the current language.</summary>
		/// <returns>A file name extension corresponding to the extension of the source files of the current language. The base implementation always returns <see cref="F:System.String.Empty" />.</returns>
		// Token: 0x17000C11 RID: 3089
		// (get) Token: 0x060030B8 RID: 12472 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual string FileExtension
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a language features identifier.</summary>
		/// <returns>A <see cref="T:System.CodeDom.Compiler.LanguageOptions" /> that indicates special features of the language.</returns>
		// Token: 0x17000C12 RID: 3090
		// (get) Token: 0x060030B9 RID: 12473 RVA: 0x000A411C File Offset: 0x000A231C
		public virtual LanguageOptions LanguageOptions
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return LanguageOptions.None;
			}
		}

		/// <summary>Compiles an assembly based on the <see cref="N:System.CodeDom" /> trees contained in the specified array of <see cref="T:System.CodeDom.CodeCompileUnit" /> objects, using the specified compiler settings.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the settings for the compilation.</param>
		/// <param name="compilationUnits">An array of type <see cref="T:System.CodeDom.CodeCompileUnit" /> that indicates the code to compile.</param>
		/// <returns>A <see cref="T:System.CodeDom.Compiler.CompilerResults" /> object that indicates the results of the compilation.</returns>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateCompiler" /> method is overridden in a derived class.</exception>
		// Token: 0x060030BA RID: 12474 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual CompilerResults CompileAssemblyFromDom(CompilerParameters options, CodeCompileUnit[] compilationUnits)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Compiles an assembly from the source code contained in the specified files, using the specified compiler settings.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the settings for the compilation. </param>
		/// <param name="fileNames">An array of the names of the files to compile. </param>
		/// <returns>A <see cref="T:System.CodeDom.Compiler.CompilerResults" /> object that indicates the results of compilation.</returns>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateCompiler" /> method is overridden in a derived class.</exception>
		// Token: 0x060030BB RID: 12475 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual CompilerResults CompileAssemblyFromFile(CompilerParameters options, string[] fileNames)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Compiles an assembly from the specified array of strings containing source code, using the specified compiler settings.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler settings for this compilation. </param>
		/// <param name="sources">An array of source code strings to compile. </param>
		/// <returns>A <see cref="T:System.CodeDom.Compiler.CompilerResults" /> object that indicates the results of compilation.</returns>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateCompiler" /> method is overridden in a derived class.</exception>
		// Token: 0x060030BC RID: 12476 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual CompilerResults CompileAssemblyFromSource(CompilerParameters options, string[] sources)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>When overridden in a derived class, creates a new code compiler. </summary>
		/// <returns>An <see cref="T:System.CodeDom.Compiler.ICodeCompiler" /> that can be used for compilation of <see cref="N:System.CodeDom" /> based source code representations. </returns>
		// Token: 0x060030BD RID: 12477
		[Obsolete("Callers should not use the ICodeCompiler interface and should instead use the methods directly on the CodeDomProvider class. Those inheriting from CodeDomProvider must still implement this interface, and should exclude this warning or also obsolete this method.")]
		public abstract ICodeCompiler CreateCompiler();

		/// <summary>Creates an escaped identifier for the specified value.</summary>
		/// <param name="value">The string for which to create an escaped identifier.</param>
		/// <returns>The escaped identifier for the value.</returns>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030BE RID: 12478 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual string CreateEscapedIdentifier(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>When overridden in a derived class, creates a new code generator.</summary>
		/// <returns>An <see cref="T:System.CodeDom.Compiler.ICodeGenerator" /> that can be used to generate <see cref="N:System.CodeDom" /> based source code representations.</returns>
		// Token: 0x060030BF RID: 12479
		[Obsolete("Callers should not use the ICodeGenerator interface and should instead use the methods directly on the CodeDomProvider class. Those inheriting from CodeDomProvider must still implement this interface, and should exclude this warning or also obsolete this method.")]
		public abstract ICodeGenerator CreateGenerator();

		/// <summary>When overridden in a derived class, creates a new code generator using the specified <see cref="T:System.IO.TextWriter" /> for output.</summary>
		/// <param name="output">A <see cref="T:System.IO.TextWriter" /> to use to output. </param>
		/// <returns>An <see cref="T:System.CodeDom.Compiler.ICodeGenerator" /> that can be used to generate <see cref="N:System.CodeDom" /> based source code representations.</returns>
		// Token: 0x060030C0 RID: 12480 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual ICodeGenerator CreateGenerator(TextWriter output)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>When overridden in a derived class, creates a new code generator using the specified file name for output.</summary>
		/// <param name="fileName">The file name to output to. </param>
		/// <returns>An <see cref="T:System.CodeDom.Compiler.ICodeGenerator" /> that can be used to generate <see cref="N:System.CodeDom" /> based source code representations.</returns>
		// Token: 0x060030C1 RID: 12481 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual ICodeGenerator CreateGenerator(string fileName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>When overridden in a derived class, creates a new code parser.</summary>
		/// <returns>An <see cref="T:System.CodeDom.Compiler.ICodeParser" /> that can be used to parse source code. The base implementation always returns <see langword="null" />.</returns>
		// Token: 0x060030C2 RID: 12482 RVA: 0x00043C3C File Offset: 0x00041E3C
		[Obsolete("Callers should not use the ICodeParser interface and should instead use the methods directly on the CodeDomProvider class. Those inheriting from CodeDomProvider must still implement this interface, and should exclude this warning or also obsolete this method.")]
		public virtual ICodeParser CreateParser()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets a <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> instance for the specified language.</summary>
		/// <param name="language">The language name. </param>
		/// <returns>A CodeDOM provider that is implemented for the specified language name.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The <paramref name="language" /> does not have a configured provider on this computer. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="language" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060030C3 RID: 12483 RVA: 0x00043C3C File Offset: 0x00041E3C
		[ComVisible(false)]
		public static CodeDomProvider CreateProvider(string language)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets a <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> instance for the specified language and provider options.</summary>
		/// <param name="language">The language name.</param>
		/// <param name="providerOptions">A collection of provider options from the configuration file.</param>
		/// <returns>A CodeDOM provider that is implemented for the specified language name and options.</returns>
		// Token: 0x060030C4 RID: 12484 RVA: 0x00043C3C File Offset: 0x00041E3C
		[ComVisible(false)]
		public static CodeDomProvider CreateProvider(string language, IDictionary<string, string> providerOptions)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a valid identifier for the specified value.</summary>
		/// <param name="value">The string for which to generate a valid identifier.</param>
		/// <returns>A valid identifier for the specified value.</returns>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030C5 RID: 12485 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual string CreateValidIdentifier(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) compilation unit and sends it to the specified text writer, using the specified options.</summary>
		/// <param name="compileUnit">A <see cref="T:System.CodeDom.CodeCompileUnit" /> for which to generate code. </param>
		/// <param name="writer">The <see cref="T:System.IO.TextWriter" /> to which the output code is sent. </param>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CodeGeneratorOptions" /> that indicates the options to use for generating code. </param>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030C6 RID: 12486 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual void GenerateCodeFromCompileUnit(CodeCompileUnit compileUnit, TextWriter writer, CodeGeneratorOptions options)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) expression and sends it to the specified text writer, using the specified options.</summary>
		/// <param name="expression">A <see cref="T:System.CodeDom.CodeExpression" /> object that indicates the expression for which to generate code. </param>
		/// <param name="writer">The <see cref="T:System.IO.TextWriter" /> to which output code is sent. </param>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CodeGeneratorOptions" /> that indicates the options to use for generating code. </param>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030C7 RID: 12487 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual void GenerateCodeFromExpression(CodeExpression expression, TextWriter writer, CodeGeneratorOptions options)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) member declaration and sends it to the specified text writer, using the specified options.</summary>
		/// <param name="member">A <see cref="T:System.CodeDom.CodeTypeMember" /> object that indicates the member for which to generate code. </param>
		/// <param name="writer">The <see cref="T:System.IO.TextWriter" /> to which output code is sent. </param>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CodeGeneratorOptions" /> that indicates the options to use for generating code. </param>
		/// <exception cref="T:System.NotImplementedException">This method is not overridden in a derived class.</exception>
		// Token: 0x060030C8 RID: 12488 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual void GenerateCodeFromMember(CodeTypeMember member, TextWriter writer, CodeGeneratorOptions options)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) namespace and sends it to the specified text writer, using the specified options.</summary>
		/// <param name="codeNamespace">A <see cref="T:System.CodeDom.CodeNamespace" /> object that indicates the namespace for which to generate code. </param>
		/// <param name="writer">The <see cref="T:System.IO.TextWriter" /> to which output code is sent. </param>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CodeGeneratorOptions" /> that indicates the options to use for generating code. </param>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030C9 RID: 12489 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual void GenerateCodeFromNamespace(CodeNamespace codeNamespace, TextWriter writer, CodeGeneratorOptions options)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) statement and sends it to the specified text writer, using the specified options.</summary>
		/// <param name="statement">A <see cref="T:System.CodeDom.CodeStatement" /> containing the CodeDOM elements for which to generate code. </param>
		/// <param name="writer">The <see cref="T:System.IO.TextWriter" /> to which output code is sent. </param>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CodeGeneratorOptions" /> that indicates the options to use for generating code. </param>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030CA RID: 12490 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual void GenerateCodeFromStatement(CodeStatement statement, TextWriter writer, CodeGeneratorOptions options)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) type declaration and sends it to the specified text writer, using the specified options.</summary>
		/// <param name="codeType">A <see cref="T:System.CodeDom.CodeTypeDeclaration" /> object that indicates the type for which to generate code. </param>
		/// <param name="writer">The <see cref="T:System.IO.TextWriter" /> to which output code is sent. </param>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CodeGeneratorOptions" /> that indicates the options to use for generating code. </param>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030CB RID: 12491 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual void GenerateCodeFromType(CodeTypeDeclaration codeType, TextWriter writer, CodeGeneratorOptions options)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns the language provider and compiler configuration settings for this computer.</summary>
		/// <returns>An array of type <see cref="T:System.CodeDom.Compiler.CompilerInfo" /> representing the settings of all configured <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> implementations.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060030CC RID: 12492 RVA: 0x00043C3C File Offset: 0x00041E3C
		[ComVisible(false)]
		public static CompilerInfo[] GetAllCompilerInfo()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the language provider and compiler configuration settings for the specified language.</summary>
		/// <param name="language">A language name. </param>
		/// <returns>A <see cref="T:System.CodeDom.Compiler.CompilerInfo" /> object populated with settings of the configured <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> implementation.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationException">The <paramref name="language" /> does not have a configured provider on this computer. </exception>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The <paramref name="language" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060030CD RID: 12493 RVA: 0x00043C3C File Offset: 0x00041E3C
		[ComVisible(false)]
		public static CompilerInfo GetCompilerInfo(string language)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.TypeConverter" /> for the specified data type.</summary>
		/// <param name="type">The type of object to retrieve a type converter for. </param>
		/// <returns>A <see cref="T:System.ComponentModel.TypeConverter" /> for the specified type, or <see langword="null" /> if a <see cref="T:System.ComponentModel.TypeConverter" /> for the specified type cannot be found.</returns>
		// Token: 0x060030CE RID: 12494 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual TypeConverter GetConverter(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a language name associated with the specified file name extension, as configured in the <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> compiler configuration section.</summary>
		/// <param name="extension">A file name extension. </param>
		/// <returns>A language name associated with the file name extension, as configured in the <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> compiler configuration settings.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationException">The <paramref name="extension" /> does not have a configured language provider on this computer. </exception>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The <paramref name="extension" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060030CF RID: 12495 RVA: 0x00043C3C File Offset: 0x00041E3C
		[ComVisible(false)]
		public static string GetLanguageFromExtension(string extension)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the type indicated by the specified <see cref="T:System.CodeDom.CodeTypeReference" />.</summary>
		/// <param name="type">A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the type to return.</param>
		/// <returns>A text representation of the specified type, formatted for the language in which code is generated by this code generator. In Visual Basic, for example, passing in a <see cref="T:System.CodeDom.CodeTypeReference" /> for the <see cref="T:System.Int32" /> type will return "Integer".</returns>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030D0 RID: 12496 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual string GetTypeOutput(CodeTypeReference type)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Tests whether a file name extension has an associated <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> implementation configured on the computer.</summary>
		/// <param name="extension">A file name extension. </param>
		/// <returns>
		///     <see langword="true" /> if a <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> implementation is configured for the specified file name extension; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="extension" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060030D1 RID: 12497 RVA: 0x000A4138 File Offset: 0x000A2338
		[ComVisible(false)]
		public static bool IsDefinedExtension(string extension)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether a language has a <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> implementation configured on the computer.</summary>
		/// <param name="language">The language name. </param>
		/// <returns>
		///     <see langword="true" /> if a <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> implementation is configured for the specified language; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="language" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060030D2 RID: 12498 RVA: 0x000A4154 File Offset: 0x000A2354
		[ComVisible(false)]
		public static bool IsDefinedLanguage(string language)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether the specified value is a valid identifier for the current language.</summary>
		/// <param name="value">The value to verify as a valid identifier.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="value" /> parameter is a valid identifier; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030D3 RID: 12499 RVA: 0x000A4170 File Offset: 0x000A2370
		public virtual bool IsValidIdentifier(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Compiles the code read from the specified text stream into a <see cref="T:System.CodeDom.CodeCompileUnit" />.</summary>
		/// <param name="codeStream">A <see cref="T:System.IO.TextReader" /> object that is used to read the code to be parsed. </param>
		/// <returns>A <see cref="T:System.CodeDom.CodeCompileUnit" /> that contains a representation of the parsed code.</returns>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030D4 RID: 12500 RVA: 0x00043C3C File Offset: 0x00041E3C
		public virtual CodeCompileUnit Parse(TextReader codeStream)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a value indicating whether the specified code generation support is provided.</summary>
		/// <param name="generatorSupport">A <see cref="T:System.CodeDom.Compiler.GeneratorSupport" /> object that indicates the type of code generation support to verify.</param>
		/// <returns>
		///     <see langword="true" /> if the specified code generation support is provided; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.NotImplementedException">Neither this method nor the <see cref="M:System.CodeDom.Compiler.CodeDomProvider.CreateGenerator" /> method is overridden in a derived class.</exception>
		// Token: 0x060030D5 RID: 12501 RVA: 0x000A418C File Offset: 0x000A238C
		public virtual bool Supports(GeneratorSupport generatorSupport)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
