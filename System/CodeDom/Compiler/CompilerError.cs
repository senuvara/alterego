﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Represents a compiler error or warning.</summary>
	// Token: 0x020005F2 RID: 1522
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[Serializable]
	public class CompilerError
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CompilerError" /> class.</summary>
		// Token: 0x060030A9 RID: 12457 RVA: 0x000092E2 File Offset: 0x000074E2
		public CompilerError()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CompilerError" /> class using the specified file name, line, column, error number, and error text.</summary>
		/// <param name="fileName">The file name of the file that the compiler was compiling when it encountered the error. </param>
		/// <param name="line">The line of the source of the error. </param>
		/// <param name="column">The column of the source of the error. </param>
		/// <param name="errorNumber">The error number of the error. </param>
		/// <param name="errorText">The error message text. </param>
		// Token: 0x060030AA RID: 12458 RVA: 0x000092E2 File Offset: 0x000074E2
		public CompilerError(string fileName, int line, int column, string errorNumber, string errorText)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the column number where the source of the error occurs.</summary>
		/// <returns>The column number of the source file where the compiler encountered the error.</returns>
		// Token: 0x17000C0B RID: 3083
		// (get) Token: 0x060030AB RID: 12459 RVA: 0x000A40C8 File Offset: 0x000A22C8
		// (set) Token: 0x060030AC RID: 12460 RVA: 0x000092E2 File Offset: 0x000074E2
		public int Column
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the error number.</summary>
		/// <returns>The error number as a string.</returns>
		// Token: 0x17000C0C RID: 3084
		// (get) Token: 0x060030AD RID: 12461 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030AE RID: 12462 RVA: 0x000092E2 File Offset: 0x000074E2
		public string ErrorNumber
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the text of the error message.</summary>
		/// <returns>The text of the error message.</returns>
		// Token: 0x17000C0D RID: 3085
		// (get) Token: 0x060030AF RID: 12463 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030B0 RID: 12464 RVA: 0x000092E2 File Offset: 0x000074E2
		public string ErrorText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the file name of the source file that contains the code which caused the error.</summary>
		/// <returns>The file name of the source file that contains the code which caused the error.</returns>
		// Token: 0x17000C0E RID: 3086
		// (get) Token: 0x060030B1 RID: 12465 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030B2 RID: 12466 RVA: 0x000092E2 File Offset: 0x000074E2
		public string FileName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the error is a warning.</summary>
		/// <returns>
		///     <see langword="true" /> if the error is a warning; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C0F RID: 3087
		// (get) Token: 0x060030B3 RID: 12467 RVA: 0x000A40E4 File Offset: 0x000A22E4
		// (set) Token: 0x060030B4 RID: 12468 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool IsWarning
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the line number where the source of the error occurs.</summary>
		/// <returns>The line number of the source file where the compiler encountered the error.</returns>
		// Token: 0x17000C10 RID: 3088
		// (get) Token: 0x060030B5 RID: 12469 RVA: 0x000A4100 File Offset: 0x000A2300
		// (set) Token: 0x060030B6 RID: 12470 RVA: 0x000092E2 File Offset: 0x000074E2
		public int Line
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
