﻿using System;
using System.Collections.Generic;
using System.Security.Permissions;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Represents the configuration settings of a language provider. This class cannot be inherited.</summary>
	// Token: 0x020005FC RID: 1532
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public sealed class CompilerInfo
	{
		// Token: 0x06003123 RID: 12579 RVA: 0x000092E2 File Offset: 0x000074E2
		internal CompilerInfo()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the type of the configured <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> implementation.</summary>
		/// <returns>A read-only <see cref="T:System.Type" /> instance that represents the configured language provider type.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationException">The language provider is not configured on this computer. </exception>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">Cannot locate the type because it is a <see langword="null" /> or empty string.-or-Cannot locate the type because the name for the <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> cannot be found in the configuration file.</exception>
		// Token: 0x17000C30 RID: 3120
		// (get) Token: 0x06003124 RID: 12580 RVA: 0x00043C3C File Offset: 0x00041E3C
		public Type CodeDomProviderType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Returns a value indicating whether the language provider implementation is configured on the computer.</summary>
		/// <returns>
		///     <see langword="true" /> if the language provider implementation type is configured on the computer; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C31 RID: 3121
		// (get) Token: 0x06003125 RID: 12581 RVA: 0x000A42C0 File Offset: 0x000A24C0
		public bool IsCodeDomProviderTypeValid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the configured compiler settings for the language provider implementation.</summary>
		/// <returns>A read-only <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> instance that contains the compiler options and settings configured for the language provider. </returns>
		// Token: 0x06003126 RID: 12582 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CompilerParameters CreateDefaultCompilerParameters()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> instance for the current language provider settings.</summary>
		/// <returns>A CodeDOM provider associated with the language provider configuration. </returns>
		// Token: 0x06003127 RID: 12583 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeDomProvider CreateProvider()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> instance for the current language provider settings and specified options.</summary>
		/// <param name="providerOptions">A collection of provider options from the configuration file.</param>
		/// <returns>A CodeDOM provider associated with the language provider configuration and specified options.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="providerOptions " />is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The provider does not support options.</exception>
		// Token: 0x06003128 RID: 12584 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeDomProvider CreateProvider(IDictionary<string, string> providerOptions)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the file name extensions supported by the language provider.</summary>
		/// <returns>An array of file name extensions supported by the language provider.</returns>
		// Token: 0x06003129 RID: 12585 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string[] GetExtensions()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the language names supported by the language provider.</summary>
		/// <returns>An array of language names supported by the language provider.</returns>
		// Token: 0x0600312A RID: 12586 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string[] GetLanguages()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
