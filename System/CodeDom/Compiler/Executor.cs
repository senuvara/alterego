﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Provides command execution functions for invoking compilers. This class cannot be inherited.</summary>
	// Token: 0x020006E2 RID: 1762
	[PermissionSet(SecurityAction.LinkDemand, Unrestricted = true)]
	public static class Executor
	{
		/// <summary>Executes the command using the specified temporary files and waits for the call to return.</summary>
		/// <param name="cmd">The command to execute. </param>
		/// <param name="tempFiles">A <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> with which to manage and store references to intermediate files generated during compilation. </param>
		// Token: 0x06003787 RID: 14215 RVA: 0x000092E2 File Offset: 0x000074E2
		public static void ExecWait(string cmd, TempFileCollection tempFiles)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Executes the specified command using the specified user token and temporary files, and waits for the call to return, storing output and error information from the compiler in the specified strings.</summary>
		/// <param name="userToken">The token to start the compiler process with. </param>
		/// <param name="cmd">The command to execute. </param>
		/// <param name="tempFiles">A <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> with which to manage and store references to intermediate files generated during compilation. </param>
		/// <param name="outputName">A reference to a string that will store the compiler's message output. </param>
		/// <param name="errorName">A reference to a string that will store the name of the error or errors encountered. </param>
		/// <returns>The return value from the compiler.</returns>
		// Token: 0x06003788 RID: 14216 RVA: 0x000A5DFC File Offset: 0x000A3FFC
		public static int ExecWaitWithCapture(IntPtr userToken, string cmd, TempFileCollection tempFiles, ref string outputName, ref string errorName)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Executes the specified command using the specified user token, current directory, and temporary files; then waits for the call to return, storing output and error information from the compiler in the specified strings.</summary>
		/// <param name="userToken">The token to start the compiler process with. </param>
		/// <param name="cmd">The command to execute. </param>
		/// <param name="currentDir">The directory to start the process in. </param>
		/// <param name="tempFiles">A <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> with which to manage and store references to intermediate files generated during compilation. </param>
		/// <param name="outputName">A reference to a string that will store the compiler's message output. </param>
		/// <param name="errorName">A reference to a string that will store the name of the error or errors encountered. </param>
		/// <returns>The return value from the compiler.</returns>
		// Token: 0x06003789 RID: 14217 RVA: 0x000A5E18 File Offset: 0x000A4018
		public static int ExecWaitWithCapture(IntPtr userToken, string cmd, string currentDir, TempFileCollection tempFiles, ref string outputName, ref string errorName)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Executes the specified command using the specified temporary files and waits for the call to return, storing output and error information from the compiler in the specified strings.</summary>
		/// <param name="cmd">The command to execute. </param>
		/// <param name="tempFiles">A <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> with which to manage and store references to intermediate files generated during compilation. </param>
		/// <param name="outputName">A reference to a string that will store the compiler's message output. </param>
		/// <param name="errorName">A reference to a string that will store the name of the error or errors encountered. </param>
		/// <returns>The return value from the compiler.</returns>
		// Token: 0x0600378A RID: 14218 RVA: 0x000A5E34 File Offset: 0x000A4034
		public static int ExecWaitWithCapture(string cmd, TempFileCollection tempFiles, ref string outputName, ref string errorName)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Executes the specified command using the specified current directory and temporary files, and waits for the call to return, storing output and error information from the compiler in the specified strings.</summary>
		/// <param name="cmd">The command to execute. </param>
		/// <param name="currentDir">The current directory. </param>
		/// <param name="tempFiles">A <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> with which to manage and store references to intermediate files generated during compilation. </param>
		/// <param name="outputName">A reference to a string that will store the compiler's message output. </param>
		/// <param name="errorName">A reference to a string that will store the name of the error or errors encountered. </param>
		/// <returns>The return value from the compiler.</returns>
		// Token: 0x0600378B RID: 14219 RVA: 0x000A5E50 File Offset: 0x000A4050
		public static int ExecWaitWithCapture(string cmd, string currentDir, TempFileCollection tempFiles, ref string outputName, ref string errorName)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}
	}
}
