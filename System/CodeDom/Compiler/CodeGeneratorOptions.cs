﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Represents a set of options used by a code generator.</summary>
	// Token: 0x020005F9 RID: 1529
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class CodeGeneratorOptions
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CodeGeneratorOptions" /> class.</summary>
		// Token: 0x06003115 RID: 12565 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeGeneratorOptions()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a value indicating whether to insert blank lines between members.</summary>
		/// <returns>
		///     <see langword="true" /> if blank lines should be inserted; otherwise, <see langword="false" />. By default, the value of this property is <see langword="true" />.</returns>
		// Token: 0x17000C2A RID: 3114
		// (get) Token: 0x06003116 RID: 12566 RVA: 0x000A426C File Offset: 0x000A246C
		// (set) Token: 0x06003117 RID: 12567 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool BlankLinesBetweenMembers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the style to use for bracing.</summary>
		/// <returns>A string containing the bracing style to use.</returns>
		// Token: 0x17000C2B RID: 3115
		// (get) Token: 0x06003118 RID: 12568 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003119 RID: 12569 RVA: 0x000092E2 File Offset: 0x000074E2
		public string BracingStyle
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether to append an <see langword="else" />, <see langword="catch" />, or <see langword="finally" /> block, including brackets, at the closing line of each previous <see langword="if" /> or <see langword="try" /> block.</summary>
		/// <returns>
		///     <see langword="true" /> if an else should be appended; otherwise, <see langword="false" />. The default value of this property is <see langword="false" />.</returns>
		// Token: 0x17000C2C RID: 3116
		// (get) Token: 0x0600311A RID: 12570 RVA: 0x000A4288 File Offset: 0x000A2488
		// (set) Token: 0x0600311B RID: 12571 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool ElseOnClosing
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the string to use for indentations.</summary>
		/// <returns>A string containing the characters to use for indentations.</returns>
		// Token: 0x17000C2D RID: 3117
		// (get) Token: 0x0600311C RID: 12572 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600311D RID: 12573 RVA: 0x000092E2 File Offset: 0x000074E2
		public string IndentString
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the object at the specified index.</summary>
		/// <param name="index">The name associated with the object to retrieve. </param>
		/// <returns>The object associated with the specified name. If no object associated with the specified name exists in the collection, <see langword="null" />.</returns>
		// Token: 0x17000C2E RID: 3118
		public object this[string index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether to generate members in the order in which they occur in member collections.</summary>
		/// <returns>
		///     <see langword="true" /> to generate the members in the order in which they occur in the member collection; otherwise, <see langword="false" />. The default value of this property is <see langword="false" />.</returns>
		// Token: 0x17000C2F RID: 3119
		// (get) Token: 0x06003120 RID: 12576 RVA: 0x000A42A4 File Offset: 0x000A24A4
		// (set) Token: 0x06003121 RID: 12577 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool VerbatimOrder
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
