﻿using System;
using System.Collections;
using System.Security.Permissions;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Represents a collection of <see cref="T:System.CodeDom.Compiler.CompilerError" /> objects.</summary>
	// Token: 0x020005F1 RID: 1521
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[Serializable]
	public class CompilerErrorCollection : CollectionBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" /> class.</summary>
		// Token: 0x0600309A RID: 12442 RVA: 0x000092E2 File Offset: 0x000074E2
		public CompilerErrorCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" /> class that contains the contents of the specified <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" />.</summary>
		/// <param name="value">A <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" /> object with which to initialize the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x0600309B RID: 12443 RVA: 0x000092E2 File Offset: 0x000074E2
		public CompilerErrorCollection(CompilerErrorCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" /> that contains the specified array of <see cref="T:System.CodeDom.Compiler.CompilerError" /> objects.</summary>
		/// <param name="value">An array of <see cref="T:System.CodeDom.Compiler.CompilerError" /> objects to initialize the collection with. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x0600309C RID: 12444 RVA: 0x000092E2 File Offset: 0x000074E2
		public CompilerErrorCollection(CompilerError[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates whether the collection contains errors.</summary>
		/// <returns>
		///     <see langword="true" /> if the collection contains errors; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C08 RID: 3080
		// (get) Token: 0x0600309D RID: 12445 RVA: 0x000A403C File Offset: 0x000A223C
		public bool HasErrors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value that indicates whether the collection contains warnings.</summary>
		/// <returns>
		///     <see langword="true" /> if the collection contains warnings; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C09 RID: 3081
		// (get) Token: 0x0600309E RID: 12446 RVA: 0x000A4058 File Offset: 0x000A2258
		public bool HasWarnings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.CodeDom.Compiler.CompilerError" /> at the specified index.</summary>
		/// <param name="index">The zero-based index of the entry to locate in the collection. </param>
		/// <returns>A <see cref="T:System.CodeDom.Compiler.CompilerError" /> at each valid index.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The index value indicated by the <paramref name="index" /> parameter is outside the valid range of indexes for the collection. </exception>
		// Token: 0x17000C0A RID: 3082
		public CompilerError this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds the specified <see cref="T:System.CodeDom.Compiler.CompilerError" /> object to the error collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.Compiler.CompilerError" /> object to add. </param>
		/// <returns>The index at which the new element was inserted.</returns>
		// Token: 0x060030A1 RID: 12449 RVA: 0x000A4074 File Offset: 0x000A2274
		public int Add(CompilerError value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Adds the contents of the specified compiler error collection to the end of the error collection.</summary>
		/// <param name="value">A <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" /> object that contains the objects to add to the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x060030A2 RID: 12450 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(CompilerErrorCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies the elements of an array to the end of the error collection.</summary>
		/// <param name="value">An array of type <see cref="T:System.CodeDom.Compiler.CompilerError" /> that contains the objects to add to the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x060030A3 RID: 12451 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(CompilerError[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates whether the collection contains the specified <see cref="T:System.CodeDom.Compiler.CompilerError" /> object.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.Compiler.CompilerError" /> to locate. </param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.CodeDom.Compiler.CompilerError" /> is contained in the collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x060030A4 RID: 12452 RVA: 0x000A4090 File Offset: 0x000A2290
		public bool Contains(CompilerError value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the collection values to a one-dimensional <see cref="T:System.Array" /> instance at the specified index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the values copied from <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" />. </param>
		/// <param name="index">The index in the array at which to start copying. </param>
		/// <exception cref="T:System.ArgumentException">The array indicated by the <paramref name="array" /> parameter is multidimensional.-or- The number of elements in the <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" /> is greater than the available space between the index value of the <paramref name="arrayIndex" /> parameter in the array indicated by the <paramref name="array" /> parameter and the end of the array indicated by the <paramref name="array" /> parameter. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="array" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="index" /> parameter is less than the lowbound of the array indicated by the <paramref name="array" /> parameter. </exception>
		// Token: 0x060030A5 RID: 12453 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(CompilerError[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the index of the specified <see cref="T:System.CodeDom.Compiler.CompilerError" /> object in the collection, if it exists in the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.Compiler.CompilerError" /> to locate. </param>
		/// <returns>The index of the specified <see cref="T:System.CodeDom.Compiler.CompilerError" /> in the <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" />, if found; otherwise, -1.</returns>
		// Token: 0x060030A6 RID: 12454 RVA: 0x000A40AC File Offset: 0x000A22AC
		public int IndexOf(CompilerError value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Inserts the specified <see cref="T:System.CodeDom.Compiler.CompilerError" /> into the collection at the specified index.</summary>
		/// <param name="index">The zero-based index where the compiler error should be inserted. </param>
		/// <param name="value">The <see cref="T:System.CodeDom.Compiler.CompilerError" /> to insert. </param>
		// Token: 0x060030A7 RID: 12455 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Insert(int index, CompilerError value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes a specific <see cref="T:System.CodeDom.Compiler.CompilerError" /> from the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.Compiler.CompilerError" /> to remove from the <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" />. </param>
		/// <exception cref="T:System.ArgumentException">The specified object is not found in the collection. </exception>
		// Token: 0x060030A8 RID: 12456 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(CompilerError value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
