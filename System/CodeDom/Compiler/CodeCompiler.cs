﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Provides an example implementation of the <see cref="T:System.CodeDom.Compiler.ICodeCompiler" /> interface.</summary>
	// Token: 0x020006DF RID: 1759
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public abstract class CodeCompiler : CodeGenerator, ICodeCompiler
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CodeCompiler" /> class. </summary>
		// Token: 0x060036F6 RID: 14070 RVA: 0x000092E2 File Offset: 0x000074E2
		protected CodeCompiler()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the name of the compiler executable.</summary>
		/// <returns>The name of the compiler executable.</returns>
		// Token: 0x17000E04 RID: 3588
		// (get) Token: 0x060036F7 RID: 14071
		protected abstract string CompilerName { get; }

		/// <summary>Gets the file name extension to use for source files.</summary>
		/// <returns>The file name extension to use for source files.</returns>
		// Token: 0x17000E05 RID: 3589
		// (get) Token: 0x060036F8 RID: 14072
		protected abstract string FileExtension { get; }

		/// <summary>Gets the command arguments to be passed to the compiler from the specified <see cref="T:System.CodeDom.Compiler.CompilerParameters" />.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> that indicates the compiler options. </param>
		/// <returns>The command arguments.</returns>
		// Token: 0x060036F9 RID: 14073
		protected abstract string CmdArgsFromParameters(CompilerParameters options);

		/// <summary>Compiles the specified compile unit using the specified options, and returns the results from the compilation.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeCompileUnit" /> object that indicates the source to compile. </param>
		/// <returns>The results of compilation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="options" /> is <see langword="null" />.</exception>
		// Token: 0x060036FA RID: 14074 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual CompilerResults FromDom(CompilerParameters options, CodeCompileUnit e)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Compiles the specified compile units using the specified options, and returns the results from the compilation.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="ea">An array of <see cref="T:System.CodeDom.CodeCompileUnit" /> objects that indicates the source to compile. </param>
		/// <returns>The results of compilation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="options" /> is <see langword="null" />.-or-
		///         <paramref name="ea" /> is <see langword="null" />.</exception>
		// Token: 0x060036FB RID: 14075 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual CompilerResults FromDomBatch(CompilerParameters options, CodeCompileUnit[] ea)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Compiles the specified file using the specified options, and returns the results from the compilation.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="fileName">The file name to compile. </param>
		/// <returns>The results of compilation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="options" /> is <see langword="null" />. -or-
		///         <paramref name="fileName" /> is <see langword="null" />.</exception>
		// Token: 0x060036FC RID: 14076 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual CompilerResults FromFile(CompilerParameters options, string fileName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Compiles the specified files using the specified options, and returns the results from the compilation.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="fileNames">An array of strings that indicates the file names of the files to compile. </param>
		/// <returns>The results of compilation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="options" /> is <see langword="null" />.-or-
		///         <paramref name="fileNames" /> is <see langword="null" />.</exception>
		// Token: 0x060036FD RID: 14077 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual CompilerResults FromFileBatch(CompilerParameters options, string[] fileNames)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Compiles the specified source code string using the specified options, and returns the results from the compilation.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="source">The source code string to compile. </param>
		/// <returns>The results of compilation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="options" /> is <see langword="null" />.</exception>
		// Token: 0x060036FE RID: 14078 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual CompilerResults FromSource(CompilerParameters options, string source)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Compiles the specified source code strings using the specified options, and returns the results from the compilation.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="sources">An array of strings containing the source code to compile. </param>
		/// <returns>The results of compilation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="options" /> is <see langword="null" />.-or-
		///         <paramref name="sources" /> is <see langword="null" />.</exception>
		// Token: 0x060036FF RID: 14079 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual CompilerResults FromSourceBatch(CompilerParameters options, string[] sources)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the command arguments to use when invoking the compiler to generate a response file.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="cmdArgs">A command arguments string. </param>
		/// <returns>The command arguments to use to generate a response file, or <see langword="null" /> if there are no response file arguments.</returns>
		// Token: 0x06003700 RID: 14080 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected virtual string GetResponseFileCmdArgs(CompilerParameters options, string cmdArgs)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Joins the specified string arrays.</summary>
		/// <param name="sa">The array of strings to join. </param>
		/// <param name="separator">The separator to use. </param>
		/// <returns>The concatenated string.</returns>
		// Token: 0x06003701 RID: 14081 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected static string JoinStringArray(string[] sa, string separator)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Processes the specified line from the specified <see cref="T:System.CodeDom.Compiler.CompilerResults" />.</summary>
		/// <param name="results">A <see cref="T:System.CodeDom.Compiler.CompilerResults" /> that indicates the results of compilation. </param>
		/// <param name="line">The line to process. </param>
		// Token: 0x06003702 RID: 14082
		protected abstract void ProcessCompilerOutputLine(CompilerResults results, string line);

		/// <summary>For a description of this member, see <see cref="M:System.CodeDom.Compiler.ICodeCompiler.CompileAssemblyFromDom(System.CodeDom.Compiler.CompilerParameters,System.CodeDom.CodeCompileUnit)" />. </summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeCompileUnit" /> that indicates the source to compile. </param>
		/// <returns>The results of compilation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="options" /> is <see langword="null" />.</exception>
		// Token: 0x06003703 RID: 14083 RVA: 0x00043C3C File Offset: 0x00041E3C
		CompilerResults ICodeCompiler.CompileAssemblyFromDom(CompilerParameters options, CodeCompileUnit e)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>For a description of this member, see <see cref="M:System.CodeDom.Compiler.ICodeCompiler.CompileAssemblyFromDomBatch(System.CodeDom.Compiler.CompilerParameters,System.CodeDom.CodeCompileUnit[])" />. </summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="ea">An array of <see cref="T:System.CodeDom.CodeCompileUnit" /> objects that indicates the source to compile. </param>
		/// <returns>The results of compilation.</returns>
		// Token: 0x06003704 RID: 14084 RVA: 0x00043C3C File Offset: 0x00041E3C
		CompilerResults ICodeCompiler.CompileAssemblyFromDomBatch(CompilerParameters options, CodeCompileUnit[] ea)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>For a description of this member, see <see cref="M:System.CodeDom.Compiler.ICodeCompiler.CompileAssemblyFromFile(System.CodeDom.Compiler.CompilerParameters,System.String)" />. </summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="fileName">The file name to compile. </param>
		/// <returns>The results of compilation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="options" /> is <see langword="null" />.</exception>
		// Token: 0x06003705 RID: 14085 RVA: 0x00043C3C File Offset: 0x00041E3C
		CompilerResults ICodeCompiler.CompileAssemblyFromFile(CompilerParameters options, string fileName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>For a description of this member, see <see cref="M:System.CodeDom.Compiler.ICodeCompiler.CompileAssemblyFromFileBatch(System.CodeDom.Compiler.CompilerParameters,System.String[])" />. </summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="fileNames">An array of strings that indicates the file names to compile. </param>
		/// <returns>The results of compilation.</returns>
		// Token: 0x06003706 RID: 14086 RVA: 0x00043C3C File Offset: 0x00041E3C
		CompilerResults ICodeCompiler.CompileAssemblyFromFileBatch(CompilerParameters options, string[] fileNames)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>For a description of this member, see <see cref="M:System.CodeDom.Compiler.ICodeCompiler.CompileAssemblyFromSource(System.CodeDom.Compiler.CompilerParameters,System.String)" />.</summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="source">A string that indicates the source code to compile. </param>
		/// <returns>The results of compilation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="options" /> is <see langword="null" />.</exception>
		// Token: 0x06003707 RID: 14087 RVA: 0x00043C3C File Offset: 0x00041E3C
		CompilerResults ICodeCompiler.CompileAssemblyFromSource(CompilerParameters options, string source)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>For a description of this member, see <see cref="M:System.CodeDom.Compiler.ICodeCompiler.CompileAssemblyFromSourceBatch(System.CodeDom.Compiler.CompilerParameters,System.String[])" />. </summary>
		/// <param name="options">A <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> object that indicates the compiler options. </param>
		/// <param name="sources">An array of strings that indicates the source code to compile. </param>
		/// <returns>The results of compilation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="options" /> is <see langword="null" />.</exception>
		// Token: 0x06003708 RID: 14088 RVA: 0x00043C3C File Offset: 0x00041E3C
		CompilerResults ICodeCompiler.CompileAssemblyFromSourceBatch(CompilerParameters options, string[] sources)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
