﻿using System;

namespace System.CodeDom.Compiler
{
	/// <summary>Defines identifiers used to determine whether a code generator supports certain types of code elements.</summary>
	// Token: 0x020005FA RID: 1530
	[Flags]
	[Serializable]
	public enum GeneratorSupport
	{
		/// <summary>Indicates the generator supports arrays of arrays.</summary>
		// Token: 0x04002481 RID: 9345
		ArraysOfArrays = 1,
		/// <summary>Indicates the generator supports assembly attributes.</summary>
		// Token: 0x04002482 RID: 9346
		AssemblyAttributes = 4096,
		/// <summary>Indicates the generator supports chained constructor arguments.</summary>
		// Token: 0x04002483 RID: 9347
		ChainedConstructorArguments = 32768,
		/// <summary>Indicates the generator supports complex expressions.</summary>
		// Token: 0x04002484 RID: 9348
		ComplexExpressions = 524288,
		/// <summary>Indicates the generator supports delegate declarations.</summary>
		// Token: 0x04002485 RID: 9349
		DeclareDelegates = 512,
		/// <summary>Indicates the generator supports enumeration declarations.</summary>
		// Token: 0x04002486 RID: 9350
		DeclareEnums = 256,
		/// <summary>Indicates the generator supports event declarations.</summary>
		// Token: 0x04002487 RID: 9351
		DeclareEvents = 2048,
		/// <summary>Indicates the generator supports the declaration of indexer properties.</summary>
		// Token: 0x04002488 RID: 9352
		DeclareIndexerProperties = 33554432,
		/// <summary>Indicates the generator supports interface declarations.</summary>
		// Token: 0x04002489 RID: 9353
		DeclareInterfaces = 1024,
		/// <summary>Indicates the generator supports value type declarations.</summary>
		// Token: 0x0400248A RID: 9354
		DeclareValueTypes = 128,
		/// <summary>Indicates the generator supports a program entry point method designation. This is used when building executables.</summary>
		// Token: 0x0400248B RID: 9355
		EntryPointMethod = 2,
		/// <summary>Indicates the generator supports generic type declarations.</summary>
		// Token: 0x0400248C RID: 9356
		GenericTypeDeclaration = 16777216,
		/// <summary>Indicates the generator supports generic type references.</summary>
		// Token: 0x0400248D RID: 9357
		GenericTypeReference = 8388608,
		/// <summary>Indicates the generator supports goto statements.</summary>
		// Token: 0x0400248E RID: 9358
		GotoStatements = 4,
		/// <summary>Indicates the generator supports referencing multidimensional arrays. Currently, the CodeDom cannot be used to instantiate multidimensional arrays.</summary>
		// Token: 0x0400248F RID: 9359
		MultidimensionalArrays = 8,
		/// <summary>Indicates the generator supports the declaration of members that implement multiple interfaces.</summary>
		// Token: 0x04002490 RID: 9360
		MultipleInterfaceMembers = 131072,
		/// <summary>Indicates the generator supports the declaration of nested types.</summary>
		// Token: 0x04002491 RID: 9361
		NestedTypes = 65536,
		/// <summary>Indicates the generator supports parameter attributes.</summary>
		// Token: 0x04002492 RID: 9362
		ParameterAttributes = 8192,
		/// <summary>Indicates the generator supports partial type declarations.</summary>
		// Token: 0x04002493 RID: 9363
		PartialTypes = 4194304,
		/// <summary>Indicates the generator supports public static members.</summary>
		// Token: 0x04002494 RID: 9364
		PublicStaticMembers = 262144,
		/// <summary>Indicates the generator supports reference and out parameters.</summary>
		// Token: 0x04002495 RID: 9365
		ReferenceParameters = 16384,
		/// <summary>Indicates the generator supports compilation with .NET Framework resources. These can be default resources compiled directly into an assembly, or resources referenced in a satellite assembly.</summary>
		// Token: 0x04002496 RID: 9366
		Resources = 2097152,
		/// <summary>Indicates the generator supports return type attribute declarations.</summary>
		// Token: 0x04002497 RID: 9367
		ReturnTypeAttributes = 64,
		/// <summary>Indicates the generator supports static constructors.</summary>
		// Token: 0x04002498 RID: 9368
		StaticConstructors = 16,
		/// <summary>Indicates the generator supports <see langword="try...catch" /> statements.</summary>
		// Token: 0x04002499 RID: 9369
		TryCatchStatements = 32,
		/// <summary>Indicates the generator supports compilation with Win32 resources.</summary>
		// Token: 0x0400249A RID: 9370
		Win32Resources = 1048576
	}
}
