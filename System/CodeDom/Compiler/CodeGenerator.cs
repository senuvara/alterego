﻿using System;
using System.IO;
using System.Reflection;
using System.Security.Permissions;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Provides an example implementation of the <see cref="T:System.CodeDom.Compiler.ICodeGenerator" /> interface. This class is abstract.</summary>
	// Token: 0x020006E0 RID: 1760
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public abstract class CodeGenerator : ICodeGenerator
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CodeGenerator" /> class. </summary>
		// Token: 0x06003709 RID: 14089 RVA: 0x000092E2 File Offset: 0x000074E2
		protected CodeGenerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the code type declaration for the current class.</summary>
		/// <returns>The code type declaration for the current class.</returns>
		// Token: 0x17000E06 RID: 3590
		// (get) Token: 0x0600370A RID: 14090 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected CodeTypeDeclaration CurrentClass
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the current member of the class.</summary>
		/// <returns>The current member of the class.</returns>
		// Token: 0x17000E07 RID: 3591
		// (get) Token: 0x0600370B RID: 14091 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected CodeTypeMember CurrentMember
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the current member name.</summary>
		/// <returns>The name of the current member.</returns>
		// Token: 0x17000E08 RID: 3592
		// (get) Token: 0x0600370C RID: 14092 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected string CurrentMemberName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the current class name.</summary>
		/// <returns>The current class name.</returns>
		// Token: 0x17000E09 RID: 3593
		// (get) Token: 0x0600370D RID: 14093 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected string CurrentTypeName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the amount of spaces to indent each indentation level.</summary>
		/// <returns>The number of spaces to indent for each indentation level.</returns>
		// Token: 0x17000E0A RID: 3594
		// (get) Token: 0x0600370E RID: 14094 RVA: 0x000A5D00 File Offset: 0x000A3F00
		// (set) Token: 0x0600370F RID: 14095 RVA: 0x000092E2 File Offset: 0x000074E2
		protected int Indent
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value indicating whether the current object being generated is a class.</summary>
		/// <returns>
		///     <see langword="true" /> if the current object is a class; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000E0B RID: 3595
		// (get) Token: 0x06003710 RID: 14096 RVA: 0x000A5D1C File Offset: 0x000A3F1C
		protected bool IsCurrentClass
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the current object being generated is a delegate.</summary>
		/// <returns>
		///     <see langword="true" /> if the current object is a delegate; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000E0C RID: 3596
		// (get) Token: 0x06003711 RID: 14097 RVA: 0x000A5D38 File Offset: 0x000A3F38
		protected bool IsCurrentDelegate
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the current object being generated is an enumeration.</summary>
		/// <returns>
		///     <see langword="true" /> if the current object is an enumeration; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000E0D RID: 3597
		// (get) Token: 0x06003712 RID: 14098 RVA: 0x000A5D54 File Offset: 0x000A3F54
		protected bool IsCurrentEnum
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the current object being generated is an interface.</summary>
		/// <returns>
		///     <see langword="true" /> if the current object is an interface; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000E0E RID: 3598
		// (get) Token: 0x06003713 RID: 14099 RVA: 0x000A5D70 File Offset: 0x000A3F70
		protected bool IsCurrentInterface
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the current object being generated is a value type or struct.</summary>
		/// <returns>
		///     <see langword="true" /> if the current object is a value type or struct; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000E0F RID: 3599
		// (get) Token: 0x06003714 RID: 14100 RVA: 0x000A5D8C File Offset: 0x000A3F8C
		protected bool IsCurrentStruct
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the token that represents <see langword="null" />.</summary>
		/// <returns>The token that represents <see langword="null" />.</returns>
		// Token: 0x17000E10 RID: 3600
		// (get) Token: 0x06003715 RID: 14101
		protected abstract string NullToken { get; }

		/// <summary>Gets the options to be used by the code generator.</summary>
		/// <returns>An object that indicates the options for the code generator to use.</returns>
		// Token: 0x17000E11 RID: 3601
		// (get) Token: 0x06003716 RID: 14102 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected CodeGeneratorOptions Options
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the text writer to use for output.</summary>
		/// <returns>The text writer to use for output.</returns>
		// Token: 0x17000E12 RID: 3602
		// (get) Token: 0x06003717 RID: 14103 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected TextWriter Output
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Generates a line-continuation character and outputs the specified string on a new line.</summary>
		/// <param name="st">The string to write on the new line. </param>
		// Token: 0x06003718 RID: 14104 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void ContinueOnNewLine(string st)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates an escaped identifier for the specified value.</summary>
		/// <param name="value">The string to create an escaped identifier for. </param>
		/// <returns>The escaped identifier for the value.</returns>
		// Token: 0x06003719 RID: 14105
		protected abstract string CreateEscapedIdentifier(string value);

		/// <summary>Creates a valid identifier for the specified value.</summary>
		/// <param name="value">A string to create a valid identifier for. </param>
		/// <returns>A valid identifier for the value.</returns>
		// Token: 0x0600371A RID: 14106
		protected abstract string CreateValidIdentifier(string value);

		/// <summary>Generates code for the specified argument reference expression.</summary>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeArgumentReferenceExpression" /> that indicates the expression to generate code for. </param>
		// Token: 0x0600371B RID: 14107
		protected abstract void GenerateArgumentReferenceExpression(CodeArgumentReferenceExpression e);

		/// <summary>Generates code for the specified array creation expression.</summary>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> that indicates the expression to generate code for. </param>
		// Token: 0x0600371C RID: 14108
		protected abstract void GenerateArrayCreateExpression(CodeArrayCreateExpression e);

		/// <summary>Generates code for the specified array indexer expression.</summary>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeArrayIndexerExpression" /> that indicates the expression to generate code for. </param>
		// Token: 0x0600371D RID: 14109
		protected abstract void GenerateArrayIndexerExpression(CodeArrayIndexerExpression e);

		/// <summary>Generates code for the specified assignment statement.</summary>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeAssignStatement" /> that indicates the statement to generate code for. </param>
		// Token: 0x0600371E RID: 14110
		protected abstract void GenerateAssignStatement(CodeAssignStatement e);

		/// <summary>Generates code for the specified attach event statement.</summary>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeAttachEventStatement" /> that indicates the statement to generate code for. </param>
		// Token: 0x0600371F RID: 14111
		protected abstract void GenerateAttachEventStatement(CodeAttachEventStatement e);

		/// <summary>Generates code for the specified attribute block end.</summary>
		/// <param name="attributes">A <see cref="T:System.CodeDom.CodeAttributeDeclarationCollection" /> that indicates the end of the attribute block to generate code for. </param>
		// Token: 0x06003720 RID: 14112
		protected abstract void GenerateAttributeDeclarationsEnd(CodeAttributeDeclarationCollection attributes);

		/// <summary>Generates code for the specified attribute block start.</summary>
		/// <param name="attributes">A <see cref="T:System.CodeDom.CodeAttributeDeclarationCollection" /> that indicates the start of the attribute block to generate code for. </param>
		// Token: 0x06003721 RID: 14113
		protected abstract void GenerateAttributeDeclarationsStart(CodeAttributeDeclarationCollection attributes);

		/// <summary>Generates code for the specified base reference expression.</summary>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeBaseReferenceExpression" /> that indicates the expression to generate code for. </param>
		// Token: 0x06003722 RID: 14114
		protected abstract void GenerateBaseReferenceExpression(CodeBaseReferenceExpression e);

		/// <summary>Generates code for the specified binary operator expression.</summary>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeBinaryOperatorExpression" /> that indicates the expression to generate code for. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="e" /> is <see langword="null" />.</exception>
		// Token: 0x06003723 RID: 14115 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateBinaryOperatorExpression(CodeBinaryOperatorExpression e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified cast expression.</summary>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeCastExpression" /> that indicates the expression to generate code for. </param>
		// Token: 0x06003724 RID: 14116
		protected abstract void GenerateCastExpression(CodeCastExpression e);

		/// <summary>Generates code for the specified class member using the specified text writer and code generator options.</summary>
		/// <param name="member">The class member to generate code for.</param>
		/// <param name="writer">The text writer to output code to.</param>
		/// <param name="options">The options to use when generating the code.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.CodeDom.Compiler.CodeGenerator.Output" /> property is not <see langword="null" />.</exception>
		// Token: 0x06003725 RID: 14117 RVA: 0x000092E2 File Offset: 0x000074E2
		public virtual void GenerateCodeFromMember(CodeTypeMember member, TextWriter writer, CodeGeneratorOptions options)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified comment.</summary>
		/// <param name="e">A <see cref="T:System.CodeDom.CodeComment" /> to generate code for. </param>
		// Token: 0x06003726 RID: 14118
		protected abstract void GenerateComment(CodeComment e);

		/// <summary>Generates code for the specified comment statement.</summary>
		/// <param name="e">The statement to generate code for.</param>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.CodeDom.CodeCommentStatement.Comment" /> property of <paramref name="e " />is not set.</exception>
		// Token: 0x06003727 RID: 14119 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateCommentStatement(CodeCommentStatement e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified comment statements.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003728 RID: 14120 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateCommentStatements(CodeCommentStatementCollection e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified compile unit.</summary>
		/// <param name="e">The compile unit to generate code for. </param>
		// Token: 0x06003729 RID: 14121 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateCompileUnit(CodeCompileUnit e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the end of a compile unit.</summary>
		/// <param name="e">The compile unit to generate code for. </param>
		// Token: 0x0600372A RID: 14122 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateCompileUnitEnd(CodeCompileUnit e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the start of a compile unit.</summary>
		/// <param name="e">The compile unit to generate code for. </param>
		// Token: 0x0600372B RID: 14123 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateCompileUnitStart(CodeCompileUnit e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified conditional statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		// Token: 0x0600372C RID: 14124
		protected abstract void GenerateConditionStatement(CodeConditionStatement e);

		/// <summary>Generates code for the specified constructor.</summary>
		/// <param name="e">The constructor to generate code for. </param>
		/// <param name="c">The type of the object that this constructor constructs. </param>
		// Token: 0x0600372D RID: 14125
		protected abstract void GenerateConstructor(CodeConstructor e, CodeTypeDeclaration c);

		/// <summary>Generates code for the specified decimal value.</summary>
		/// <param name="d">The decimal value to generate code for. </param>
		// Token: 0x0600372E RID: 14126 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateDecimalValue(decimal d)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified reference to a default value.</summary>
		/// <param name="e">The reference to generate code for.</param>
		// Token: 0x0600372F RID: 14127 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateDefaultValueExpression(CodeDefaultValueExpression e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified delegate creation expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003730 RID: 14128
		protected abstract void GenerateDelegateCreateExpression(CodeDelegateCreateExpression e);

		/// <summary>Generates code for the specified delegate invoke expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003731 RID: 14129
		protected abstract void GenerateDelegateInvokeExpression(CodeDelegateInvokeExpression e);

		/// <summary>Generates code for the specified direction expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003732 RID: 14130 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateDirectionExpression(CodeDirectionExpression e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified code directives.</summary>
		/// <param name="directives">The code directives to generate code for.</param>
		// Token: 0x06003733 RID: 14131 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateDirectives(CodeDirectiveCollection directives)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for a double-precision floating point number.</summary>
		/// <param name="d">The value to generate code for. </param>
		// Token: 0x06003734 RID: 14132 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateDoubleValue(double d)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified entry point method.</summary>
		/// <param name="e">The entry point for the code. </param>
		/// <param name="c">The code that declares the type. </param>
		// Token: 0x06003735 RID: 14133
		protected abstract void GenerateEntryPointMethod(CodeEntryPointMethod e, CodeTypeDeclaration c);

		/// <summary>Generates code for the specified event.</summary>
		/// <param name="e">The member event to generate code for. </param>
		/// <param name="c">The type of the object that this event occurs on. </param>
		// Token: 0x06003736 RID: 14134
		protected abstract void GenerateEvent(CodeMemberEvent e, CodeTypeDeclaration c);

		/// <summary>Generates code for the specified event reference expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003737 RID: 14135
		protected abstract void GenerateEventReferenceExpression(CodeEventReferenceExpression e);

		/// <summary>Generates code for the specified code expression.</summary>
		/// <param name="e">The code expression to generate code for. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="e" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="e" /> is not a valid <see cref="T:System.CodeDom.CodeStatement" />.</exception>
		// Token: 0x06003738 RID: 14136 RVA: 0x000092E2 File Offset: 0x000074E2
		protected void GenerateExpression(CodeExpression e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified expression statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		// Token: 0x06003739 RID: 14137
		protected abstract void GenerateExpressionStatement(CodeExpressionStatement e);

		/// <summary>Generates code for the specified member field.</summary>
		/// <param name="e">The field to generate code for. </param>
		// Token: 0x0600373A RID: 14138
		protected abstract void GenerateField(CodeMemberField e);

		/// <summary>Generates code for the specified field reference expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x0600373B RID: 14139
		protected abstract void GenerateFieldReferenceExpression(CodeFieldReferenceExpression e);

		/// <summary>Generates code for the specified <see langword="goto" /> statement.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x0600373C RID: 14140
		protected abstract void GenerateGotoStatement(CodeGotoStatement e);

		/// <summary>Generates code for the specified indexer expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x0600373D RID: 14141
		protected abstract void GenerateIndexerExpression(CodeIndexerExpression e);

		/// <summary>Generates code for the specified iteration statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		// Token: 0x0600373E RID: 14142
		protected abstract void GenerateIterationStatement(CodeIterationStatement e);

		/// <summary>Generates code for the specified labeled statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		// Token: 0x0600373F RID: 14143
		protected abstract void GenerateLabeledStatement(CodeLabeledStatement e);

		/// <summary>Generates code for the specified line pragma end.</summary>
		/// <param name="e">The end of the line pragma to generate code for. </param>
		// Token: 0x06003740 RID: 14144
		protected abstract void GenerateLinePragmaEnd(CodeLinePragma e);

		/// <summary>Generates code for the specified line pragma start.</summary>
		/// <param name="e">The start of the line pragma to generate code for. </param>
		// Token: 0x06003741 RID: 14145
		protected abstract void GenerateLinePragmaStart(CodeLinePragma e);

		/// <summary>Generates code for the specified method.</summary>
		/// <param name="e">The member method to generate code for. </param>
		/// <param name="c">The type of the object that this method occurs on. </param>
		// Token: 0x06003742 RID: 14146
		protected abstract void GenerateMethod(CodeMemberMethod e, CodeTypeDeclaration c);

		/// <summary>Generates code for the specified method invoke expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003743 RID: 14147
		protected abstract void GenerateMethodInvokeExpression(CodeMethodInvokeExpression e);

		/// <summary>Generates code for the specified method reference expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003744 RID: 14148
		protected abstract void GenerateMethodReferenceExpression(CodeMethodReferenceExpression e);

		/// <summary>Generates code for the specified method return statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		// Token: 0x06003745 RID: 14149
		protected abstract void GenerateMethodReturnStatement(CodeMethodReturnStatement e);

		/// <summary>Generates code for the specified namespace.</summary>
		/// <param name="e">The namespace to generate code for. </param>
		// Token: 0x06003746 RID: 14150 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateNamespace(CodeNamespace e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the end of a namespace.</summary>
		/// <param name="e">The namespace to generate code for. </param>
		// Token: 0x06003747 RID: 14151
		protected abstract void GenerateNamespaceEnd(CodeNamespace e);

		/// <summary>Generates code for the specified namespace import.</summary>
		/// <param name="e">The namespace import to generate code for. </param>
		// Token: 0x06003748 RID: 14152
		protected abstract void GenerateNamespaceImport(CodeNamespaceImport e);

		/// <summary>Generates code for the specified namespace import.</summary>
		/// <param name="e">The namespace import to generate code for. </param>
		// Token: 0x06003749 RID: 14153 RVA: 0x000092E2 File Offset: 0x000074E2
		protected void GenerateNamespaceImports(CodeNamespace e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the namespaces in the specified compile unit.</summary>
		/// <param name="e">The compile unit to generate namespaces for. </param>
		// Token: 0x0600374A RID: 14154 RVA: 0x000092E2 File Offset: 0x000074E2
		protected void GenerateNamespaces(CodeCompileUnit e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the start of a namespace.</summary>
		/// <param name="e">The namespace to generate code for. </param>
		// Token: 0x0600374B RID: 14155
		protected abstract void GenerateNamespaceStart(CodeNamespace e);

		/// <summary>Generates code for the specified object creation expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x0600374C RID: 14156
		protected abstract void GenerateObjectCreateExpression(CodeObjectCreateExpression e);

		/// <summary>Generates code for the specified parameter declaration expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x0600374D RID: 14157 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateParameterDeclarationExpression(CodeParameterDeclarationExpression e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified primitive expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="e" /> uses an invalid data type. Only the following data types are valid:stringcharbyteInt16Int32Int64SingleDoubleDecimal</exception>
		// Token: 0x0600374E RID: 14158 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GeneratePrimitiveExpression(CodePrimitiveExpression e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified property.</summary>
		/// <param name="e">The property to generate code for. </param>
		/// <param name="c">The type of the object that this property occurs on. </param>
		// Token: 0x0600374F RID: 14159
		protected abstract void GenerateProperty(CodeMemberProperty e, CodeTypeDeclaration c);

		/// <summary>Generates code for the specified property reference expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003750 RID: 14160
		protected abstract void GeneratePropertyReferenceExpression(CodePropertyReferenceExpression e);

		/// <summary>Generates code for the specified property set value reference expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003751 RID: 14161
		protected abstract void GeneratePropertySetValueReferenceExpression(CodePropertySetValueReferenceExpression e);

		/// <summary>Generates code for the specified remove event statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		// Token: 0x06003752 RID: 14162
		protected abstract void GenerateRemoveEventStatement(CodeRemoveEventStatement e);

		/// <summary>Generates code for a single-precision floating point number.</summary>
		/// <param name="s">The value to generate code for. </param>
		// Token: 0x06003753 RID: 14163 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateSingleFloatValue(float s)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Outputs the code of the specified literal code fragment compile unit.</summary>
		/// <param name="e">The literal code fragment compile unit to generate code for. </param>
		// Token: 0x06003754 RID: 14164 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateSnippetCompileUnit(CodeSnippetCompileUnit e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Outputs the code of the specified literal code fragment expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003755 RID: 14165
		protected abstract void GenerateSnippetExpression(CodeSnippetExpression e);

		/// <summary>Outputs the code of the specified literal code fragment class member.</summary>
		/// <param name="e">The member to generate code for. </param>
		// Token: 0x06003756 RID: 14166
		protected abstract void GenerateSnippetMember(CodeSnippetTypeMember e);

		/// <summary>Outputs the code of the specified literal code fragment statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		// Token: 0x06003757 RID: 14167 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateSnippetStatement(CodeSnippetStatement e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="e" /> is not a valid <see cref="T:System.CodeDom.CodeStatement" />.</exception>
		// Token: 0x06003758 RID: 14168 RVA: 0x000092E2 File Offset: 0x000074E2
		protected void GenerateStatement(CodeStatement e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified statement collection.</summary>
		/// <param name="stms">The statements to generate code for. </param>
		// Token: 0x06003759 RID: 14169 RVA: 0x000092E2 File Offset: 0x000074E2
		protected void GenerateStatements(CodeStatementCollection stms)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified this reference expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x0600375A RID: 14170
		protected abstract void GenerateThisReferenceExpression(CodeThisReferenceExpression e);

		/// <summary>Generates code for the specified throw exception statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		// Token: 0x0600375B RID: 14171
		protected abstract void GenerateThrowExceptionStatement(CodeThrowExceptionStatement e);

		/// <summary>Generates code for the specified <see langword="try...catch...finally" /> statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		// Token: 0x0600375C RID: 14172
		protected abstract void GenerateTryCatchFinallyStatement(CodeTryCatchFinallyStatement e);

		/// <summary>Generates code for the specified class constructor.</summary>
		/// <param name="e">The class constructor to generate code for. </param>
		// Token: 0x0600375D RID: 14173
		protected abstract void GenerateTypeConstructor(CodeTypeConstructor e);

		/// <summary>Generates code for the specified end of the class.</summary>
		/// <param name="e">The end of the class to generate code for. </param>
		// Token: 0x0600375E RID: 14174
		protected abstract void GenerateTypeEnd(CodeTypeDeclaration e);

		/// <summary>Generates code for the specified type of expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x0600375F RID: 14175 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateTypeOfExpression(CodeTypeOfExpression e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified type reference expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003760 RID: 14176 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void GenerateTypeReferenceExpression(CodeTypeReferenceExpression e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified namespace and the classes it contains.</summary>
		/// <param name="e">The namespace to generate classes for. </param>
		// Token: 0x06003761 RID: 14177 RVA: 0x000092E2 File Offset: 0x000074E2
		protected void GenerateTypes(CodeNamespace e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified start of the class.</summary>
		/// <param name="e">The start of the class to generate code for. </param>
		// Token: 0x06003762 RID: 14178
		protected abstract void GenerateTypeStart(CodeTypeDeclaration e);

		/// <summary>Generates code for the specified variable declaration statement.</summary>
		/// <param name="e">The statement to generate code for. </param>
		// Token: 0x06003763 RID: 14179
		protected abstract void GenerateVariableDeclarationStatement(CodeVariableDeclarationStatement e);

		/// <summary>Generates code for the specified variable reference expression.</summary>
		/// <param name="e">The expression to generate code for. </param>
		// Token: 0x06003764 RID: 14180
		protected abstract void GenerateVariableReferenceExpression(CodeVariableReferenceExpression e);

		/// <summary>Gets the name of the specified data type.</summary>
		/// <param name="value">The type whose name will be returned. </param>
		/// <returns>The name of the data type reference.</returns>
		// Token: 0x06003765 RID: 14181
		protected abstract string GetTypeOutput(CodeTypeReference value);

		/// <summary>Gets a value indicating whether the specified value is a valid identifier.</summary>
		/// <param name="value">The value to test for conflicts with valid identifiers. </param>
		/// <returns>
		///     <see langword="true" /> if the value is a valid identifier; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003766 RID: 14182
		protected abstract bool IsValidIdentifier(string value);

		/// <summary>Gets a value indicating whether the specified string is a valid identifier.</summary>
		/// <param name="value">The string to test for validity. </param>
		/// <returns>
		///     <see langword="true" /> if the specified string is a valid identifier; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003767 RID: 14183 RVA: 0x000A5DA8 File Offset: 0x000A3FA8
		public static bool IsValidLanguageIndependentIdentifier(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Outputs an argument in an attribute block.</summary>
		/// <param name="arg">The attribute argument to generate code for. </param>
		// Token: 0x06003768 RID: 14184 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputAttributeArgument(CodeAttributeArgument arg)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified attribute declaration collection.</summary>
		/// <param name="attributes">The attributes to generate code for. </param>
		// Token: 0x06003769 RID: 14185 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputAttributeDeclarations(CodeAttributeDeclarationCollection attributes)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified <see cref="T:System.CodeDom.FieldDirection" />.</summary>
		/// <param name="dir">One of the enumeration values that indicates the attribute of the field. </param>
		// Token: 0x0600376A RID: 14186 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputDirection(FieldDirection dir)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified expression list.</summary>
		/// <param name="expressions">The expressions to generate code for. </param>
		// Token: 0x0600376B RID: 14187 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputExpressionList(CodeExpressionCollection expressions)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified expression list.</summary>
		/// <param name="expressions">The expressions to generate code for. </param>
		/// <param name="newlineBetweenItems">
		///       <see langword="true" /> to insert a new line after each item; otherwise, <see langword="false" />. </param>
		// Token: 0x0600376C RID: 14188 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputExpressionList(CodeExpressionCollection expressions, bool newlineBetweenItems)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Outputs a field scope modifier that corresponds to the specified attributes.</summary>
		/// <param name="attributes">One of the enumeration values that specifies the attributes. </param>
		// Token: 0x0600376D RID: 14189 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputFieldScopeModifier(MemberAttributes attributes)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Outputs the specified identifier.</summary>
		/// <param name="ident">The identifier to output. </param>
		// Token: 0x0600376E RID: 14190 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputIdentifier(string ident)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified member access modifier.</summary>
		/// <param name="attributes">One of the enumeration values that indicates the member access modifier to generate code for. </param>
		// Token: 0x0600376F RID: 14191 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputMemberAccessModifier(MemberAttributes attributes)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified member scope modifier.</summary>
		/// <param name="attributes">One of the enumeration values that indicates the member scope modifier to generate code for. </param>
		// Token: 0x06003770 RID: 14192 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputMemberScopeModifier(MemberAttributes attributes)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified operator.</summary>
		/// <param name="op">The operator to generate code for. </param>
		// Token: 0x06003771 RID: 14193 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputOperator(CodeBinaryOperatorType op)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified parameters.</summary>
		/// <param name="parameters">The parameter declaration expressions to generate code for. </param>
		// Token: 0x06003772 RID: 14194 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputParameters(CodeParameterDeclarationExpressionCollection parameters)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified type.</summary>
		/// <param name="typeRef">The type to generate code for. </param>
		// Token: 0x06003773 RID: 14195
		protected abstract void OutputType(CodeTypeReference typeRef);

		/// <summary>Generates code for the specified type attributes.</summary>
		/// <param name="attributes">One of the enumeration values that indicates the type attributes to generate code for. </param>
		/// <param name="isStruct">
		///       <see langword="true" /> if the type is a struct; otherwise, <see langword="false" />. </param>
		/// <param name="isEnum">
		///       <see langword="true" /> if the type is an enum; otherwise, <see langword="false" />. </param>
		// Token: 0x06003774 RID: 14196 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputTypeAttributes(TypeAttributes attributes, bool isStruct, bool isEnum)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified object type and name pair.</summary>
		/// <param name="typeRef">The type. </param>
		/// <param name="name">The name for the object. </param>
		// Token: 0x06003775 RID: 14197 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OutputTypeNamePair(CodeTypeReference typeRef, string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Converts the specified string by formatting it with escape codes.</summary>
		/// <param name="value">The string to convert. </param>
		/// <returns>The converted string.</returns>
		// Token: 0x06003776 RID: 14198
		protected abstract string QuoteSnippetString(string value);

		/// <summary>Gets a value indicating whether the specified code generation support is provided.</summary>
		/// <param name="support">The type of code generation support to test for. </param>
		/// <returns>
		///     <see langword="true" /> if the specified code generation support is provided; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003777 RID: 14199
		protected abstract bool Supports(GeneratorSupport support);

		/// <summary>Creates an escaped identifier for the specified value.</summary>
		/// <param name="value">The string to create an escaped identifier for.</param>
		/// <returns>The escaped identifier for the value.</returns>
		// Token: 0x06003778 RID: 14200 RVA: 0x00043C3C File Offset: 0x00041E3C
		string ICodeGenerator.CreateEscapedIdentifier(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a valid identifier for the specified value.</summary>
		/// <param name="value">The string to generate a valid identifier for.</param>
		/// <returns>A valid identifier for the specified value.</returns>
		// Token: 0x06003779 RID: 14201 RVA: 0x00043C3C File Offset: 0x00041E3C
		string ICodeGenerator.CreateValidIdentifier(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) compilation unit and outputs it to the specified text writer using the specified options.</summary>
		/// <param name="e">The CodeDOM compilation unit to generate code for.</param>
		/// <param name="w">The text writer to output code to.</param>
		/// <param name="o">The options to use for generating code.</param>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="w" /> is not available. <paramref name="w" /> may have been closed before the method call was made.</exception>
		// Token: 0x0600377A RID: 14202 RVA: 0x000092E2 File Offset: 0x000074E2
		void ICodeGenerator.GenerateCodeFromCompileUnit(CodeCompileUnit e, TextWriter w, CodeGeneratorOptions o)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) expression and outputs it to the specified text writer.</summary>
		/// <param name="e">The expression to generate code for.</param>
		/// <param name="w">The text writer to output code to.</param>
		/// <param name="o">The options to use for generating code.</param>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="w" /> is not available. <paramref name="w" /> may have been closed before the method call was made.</exception>
		// Token: 0x0600377B RID: 14203 RVA: 0x000092E2 File Offset: 0x000074E2
		void ICodeGenerator.GenerateCodeFromExpression(CodeExpression e, TextWriter w, CodeGeneratorOptions o)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) namespace and outputs it to the specified text writer using the specified options.</summary>
		/// <param name="e">The namespace to generate code for.</param>
		/// <param name="w">The text writer to output code to.</param>
		/// <param name="o">The options to use for generating code.</param>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="w" /> is not available. <paramref name="w" /> may have been closed before the method call was made.</exception>
		// Token: 0x0600377C RID: 14204 RVA: 0x000092E2 File Offset: 0x000074E2
		void ICodeGenerator.GenerateCodeFromNamespace(CodeNamespace e, TextWriter w, CodeGeneratorOptions o)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) statement and outputs it to the specified text writer using the specified options.</summary>
		/// <param name="e">The statement that contains the CodeDOM elements to translate.</param>
		/// <param name="w">The text writer to output code to.</param>
		/// <param name="o">The options to use for generating code.</param>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="w" /> is not available. <paramref name="w" /> may have been closed before the method call was made.</exception>
		// Token: 0x0600377D RID: 14205 RVA: 0x000092E2 File Offset: 0x000074E2
		void ICodeGenerator.GenerateCodeFromStatement(CodeStatement e, TextWriter w, CodeGeneratorOptions o)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Generates code for the specified Code Document Object Model (CodeDOM) type declaration and outputs it to the specified text writer using the specified options.</summary>
		/// <param name="e">The type to generate code for.</param>
		/// <param name="w">The text writer to output code to.</param>
		/// <param name="o">The options to use for generating code.</param>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="w" /> is not available. <paramref name="w" /> may have been closed before the method call was made.</exception>
		// Token: 0x0600377E RID: 14206 RVA: 0x000092E2 File Offset: 0x000074E2
		void ICodeGenerator.GenerateCodeFromType(CodeTypeDeclaration e, TextWriter w, CodeGeneratorOptions o)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the type indicated by the specified <see cref="T:System.CodeDom.CodeTypeReference" />.</summary>
		/// <param name="type">The type to return.</param>
		/// <returns>The name of the data type reference.</returns>
		// Token: 0x0600377F RID: 14207 RVA: 0x00043C3C File Offset: 0x00041E3C
		string ICodeGenerator.GetTypeOutput(CodeTypeReference type)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets a value that indicates whether the specified value is a valid identifier for the current language.</summary>
		/// <param name="value">The value to test.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="value" /> parameter is a valid identifier; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003780 RID: 14208 RVA: 0x000A5DC4 File Offset: 0x000A3FC4
		bool ICodeGenerator.IsValidIdentifier(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets a value indicating whether the generator provides support for the language features represented by the specified <see cref="T:System.CodeDom.Compiler.GeneratorSupport" />  object.</summary>
		/// <param name="support">The capabilities to test the generator for.</param>
		/// <returns>
		///     <see langword="true" /> if the specified capabilities are supported; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003781 RID: 14209 RVA: 0x000A5DE0 File Offset: 0x000A3FE0
		bool ICodeGenerator.Supports(GeneratorSupport support)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Throws an exception if the specified value is not a valid identifier.</summary>
		/// <param name="value">The identifier to validate.</param>
		// Token: 0x06003782 RID: 14210 RVA: 0x000092E2 File Offset: 0x000074E2
		void ICodeGenerator.ValidateIdentifier(string value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Throws an exception if the specified string is not a valid identifier.</summary>
		/// <param name="value">The identifier to test for validity as an identifier. </param>
		/// <exception cref="T:System.ArgumentException">If the specified identifier is invalid or conflicts with reserved or language keywords. </exception>
		// Token: 0x06003783 RID: 14211 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void ValidateIdentifier(string value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Attempts to validate each identifier field contained in the specified <see cref="T:System.CodeDom.CodeObject" /> or <see cref="N:System.CodeDom" /> tree.</summary>
		/// <param name="e">An object to test for invalid identifiers. </param>
		/// <exception cref="T:System.ArgumentException">The specified <see cref="T:System.CodeDom.CodeObject" /> contains an invalid identifier. </exception>
		// Token: 0x06003784 RID: 14212 RVA: 0x000092E2 File Offset: 0x000074E2
		public static void ValidateIdentifiers(CodeObject e)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
