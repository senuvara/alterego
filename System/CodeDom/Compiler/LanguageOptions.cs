﻿using System;

namespace System.CodeDom.Compiler
{
	/// <summary>Defines identifiers that indicate special features of a language.</summary>
	// Token: 0x020005F4 RID: 1524
	[Flags]
	[Serializable]
	public enum LanguageOptions
	{
		/// <summary>The language is case-insensitive.</summary>
		// Token: 0x0400247E RID: 9342
		CaseInsensitive = 1,
		/// <summary>The language has default characteristics.</summary>
		// Token: 0x0400247F RID: 9343
		None = 0
	}
}
