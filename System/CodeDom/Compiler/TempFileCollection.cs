﻿using System;
using System.Collections;
using System.Security.Permissions;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Represents a collection of temporary files.</summary>
	// Token: 0x020005F0 RID: 1520
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[Serializable]
	public class TempFileCollection : ICollection, IEnumerable, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> class with default values.</summary>
		// Token: 0x06003085 RID: 12421 RVA: 0x000092E2 File Offset: 0x000074E2
		public TempFileCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> class using the specified temporary directory that is set to delete the temporary files after their generation and use, by default.</summary>
		/// <param name="tempDir">A path to the temporary directory to use for storing the temporary files. </param>
		// Token: 0x06003086 RID: 12422 RVA: 0x000092E2 File Offset: 0x000074E2
		public TempFileCollection(string tempDir)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> class using the specified temporary directory and specified value indicating whether to keep or delete the temporary files after their generation and use, by default.</summary>
		/// <param name="tempDir">A path to the temporary directory to use for storing the temporary files. </param>
		/// <param name="keepFiles">
		///       <see langword="true" /> if the temporary files should be kept after use; <see langword="false" /> if the temporary files should be deleted. </param>
		// Token: 0x06003087 RID: 12423 RVA: 0x000092E2 File Offset: 0x000074E2
		public TempFileCollection(string tempDir, bool keepFiles)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the full path to the base file name, without a file name extension, on the temporary directory path, that is used to generate temporary file names for the collection.</summary>
		/// <returns>The full path to the base file name, without a file name extension, on the temporary directory path, that is used to generate temporary file names for the collection.</returns>
		/// <exception cref="T:System.Security.SecurityException">If the <see cref="P:System.CodeDom.Compiler.TempFileCollection.BasePath" /> property has not been set or is set to <see langword="null" />, and <see cref="F:System.Security.Permissions.FileIOPermissionAccess.AllAccess" /> is not granted for the temporary directory indicated by the <see cref="P:System.CodeDom.Compiler.TempFileCollection.TempDir" /> property. </exception>
		// Token: 0x17000C04 RID: 3076
		// (get) Token: 0x06003088 RID: 12424 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string BasePath
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the number of files in the collection.</summary>
		/// <returns>The number of files in the collection.</returns>
		// Token: 0x17000C05 RID: 3077
		// (get) Token: 0x06003089 RID: 12425 RVA: 0x000A3FCC File Offset: 0x000A21CC
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets or sets a value indicating whether to keep the files, by default, when the <see cref="M:System.CodeDom.Compiler.TempFileCollection.Delete" /> method is called or the collection is disposed.</summary>
		/// <returns>
		///     <see langword="true" /> if the files should be kept; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C06 RID: 3078
		// (get) Token: 0x0600308A RID: 12426 RVA: 0x000A3FE8 File Offset: 0x000A21E8
		// (set) Token: 0x0600308B RID: 12427 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool KeepFiles
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x0600308C RID: 12428 RVA: 0x000A4004 File Offset: 0x000A2204
		int ICollection.get_Count()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		// Token: 0x0600308D RID: 12429 RVA: 0x000A4020 File Offset: 0x000A2220
		bool ICollection.get_IsSynchronized()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x0600308E RID: 12430 RVA: 0x00043C3C File Offset: 0x00041E3C
		object ICollection.get_SyncRoot()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the temporary directory to store the temporary files in.</summary>
		/// <returns>The temporary directory to store the temporary files in.</returns>
		// Token: 0x17000C07 RID: 3079
		// (get) Token: 0x0600308F RID: 12431 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string TempDir
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a file name with the specified file name extension to the collection.</summary>
		/// <param name="fileExtension">The file name extension for the auto-generated temporary file name to add to the collection. </param>
		/// <returns>A file name with the specified extension that was just added to the collection.</returns>
		// Token: 0x06003090 RID: 12432 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string AddExtension(string fileExtension)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds a file name with the specified file name extension to the collection, using the specified value indicating whether the file should be deleted or retained.</summary>
		/// <param name="fileExtension">The file name extension for the auto-generated temporary file name to add to the collection. </param>
		/// <param name="keepFile">
		///       <see langword="true" /> if the file should be kept after use; <see langword="false" /> if the file should be deleted. </param>
		/// <returns>A file name with the specified extension that was just added to the collection.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="fileExtension" /> is <see langword="null" /> or an empty string.</exception>
		// Token: 0x06003091 RID: 12433 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string AddExtension(string fileExtension, bool keepFile)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds the specified file to the collection, using the specified value indicating whether to keep the file after the collection is disposed or when the <see cref="M:System.CodeDom.Compiler.TempFileCollection.Delete" /> method is called.</summary>
		/// <param name="fileName">The name of the file to add to the collection. </param>
		/// <param name="keepFile">
		///       <see langword="true" /> if the file should be kept after use; <see langword="false" /> if the file should be deleted. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="fileName" /> is <see langword="null" /> or an empty string.-or-
		///         <paramref name="fileName" /> is a duplicate.</exception>
		// Token: 0x06003092 RID: 12434 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddFile(string fileName, bool keepFile)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies the members of the collection to the specified string, beginning at the specified index.</summary>
		/// <param name="fileNames">The array of strings to copy to. </param>
		/// <param name="start">The index of the array to begin copying to. </param>
		// Token: 0x06003093 RID: 12435 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(string[] fileNames, int start)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Deletes the temporary files within this collection that were not marked to be kept.</summary>
		// Token: 0x06003094 RID: 12436 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Delete()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06003095 RID: 12437 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an enumerator that can enumerate the members of the collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that contains the collection's members.</returns>
		// Token: 0x06003096 RID: 12438 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Copies the elements of the collection to an array, starting at the specified index of the target array. </summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
		/// <param name="start">The zero-based index in array at which copying begins.</param>
		// Token: 0x06003097 RID: 12439 RVA: 0x000092E2 File Offset: 0x000074E2
		void ICollection.CopyTo(Array array, int start)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns an enumerator that iterates through a collection. </summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through the collection.</returns>
		// Token: 0x06003098 RID: 12440 RVA: 0x00043C3C File Offset: 0x00041E3C
		IEnumerator IEnumerable.GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources. </summary>
		// Token: 0x06003099 RID: 12441 RVA: 0x000092E2 File Offset: 0x000074E2
		void IDisposable.Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
