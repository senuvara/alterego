﻿using System;
using System.Collections.Specialized;
using System.Security.Permissions;
using System.Security.Policy;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Represents the parameters used to invoke a compiler.</summary>
	// Token: 0x020005F5 RID: 1525
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[Serializable]
	public class CompilerParameters
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> class.</summary>
		// Token: 0x060030D6 RID: 12502 RVA: 0x000092E2 File Offset: 0x000074E2
		public CompilerParameters()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> class using the specified assembly names.</summary>
		/// <param name="assemblyNames">The names of the assemblies to reference. </param>
		// Token: 0x060030D7 RID: 12503 RVA: 0x000092E2 File Offset: 0x000074E2
		public CompilerParameters(string[] assemblyNames)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> class using the specified assembly names and output file name.</summary>
		/// <param name="assemblyNames">The names of the assemblies to reference. </param>
		/// <param name="outputName">The output file name. </param>
		// Token: 0x060030D8 RID: 12504 RVA: 0x000092E2 File Offset: 0x000074E2
		public CompilerParameters(string[] assemblyNames, string outputName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> class using the specified assembly names, output name, and a value indicating whether to include debug information.</summary>
		/// <param name="assemblyNames">The names of the assemblies to reference. </param>
		/// <param name="outputName">The output file name. </param>
		/// <param name="includeDebugInformation">
		///       <see langword="true" /> to include debug information; <see langword="false" /> to exclude debug information. </param>
		// Token: 0x060030D9 RID: 12505 RVA: 0x000092E2 File Offset: 0x000074E2
		public CompilerParameters(string[] assemblyNames, string outputName, bool includeDebugInformation)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets optional command-line arguments to use when invoking the compiler.</summary>
		/// <returns>Any additional command-line arguments for the compiler.</returns>
		// Token: 0x17000C13 RID: 3091
		// (get) Token: 0x060030DA RID: 12506 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030DB RID: 12507 RVA: 0x000092E2 File Offset: 0x000074E2
		public string CompilerOptions
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the core or standard assembly that contains basic types such as <see cref="T:System.Object" />, <see cref="T:System.String" />, or <see cref="T:System.Int32" />.</summary>
		/// <returns>The name of the core assembly that contains basic types.</returns>
		// Token: 0x17000C14 RID: 3092
		// (get) Token: 0x060030DC RID: 12508 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030DD RID: 12509 RVA: 0x000092E2 File Offset: 0x000074E2
		public string CoreAssemblyFileName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the .NET Framework resource files to include when compiling the assembly output.</summary>
		/// <returns>A collection that contains the file paths of .NET Framework resources to include in the generated assembly.</returns>
		// Token: 0x17000C15 RID: 3093
		// (get) Token: 0x060030DE RID: 12510 RVA: 0x00043C3C File Offset: 0x00041E3C
		public StringCollection EmbeddedResources
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Specifies an evidence object that represents the security policy permissions to grant the compiled assembly.</summary>
		/// <returns>An  object that represents the security policy permissions to grant the compiled assembly.</returns>
		// Token: 0x17000C16 RID: 3094
		// (get) Token: 0x060030DF RID: 12511 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030E0 RID: 12512 RVA: 0x000092E2 File Offset: 0x000074E2
		public Evidence Evidence
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true)]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether to generate an executable.</summary>
		/// <returns>
		///     <see langword="true" /> if an executable should be generated; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C17 RID: 3095
		// (get) Token: 0x060030E1 RID: 12513 RVA: 0x000A41A8 File Offset: 0x000A23A8
		// (set) Token: 0x060030E2 RID: 12514 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool GenerateExecutable
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether to generate the output in memory.</summary>
		/// <returns>
		///     <see langword="true" /> if the compiler should generate the output in memory; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C18 RID: 3096
		// (get) Token: 0x060030E3 RID: 12515 RVA: 0x000A41C4 File Offset: 0x000A23C4
		// (set) Token: 0x060030E4 RID: 12516 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool GenerateInMemory
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether to include debug information in the compiled executable.</summary>
		/// <returns>
		///     <see langword="true" /> if debug information should be generated; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C19 RID: 3097
		// (get) Token: 0x060030E5 RID: 12517 RVA: 0x000A41E0 File Offset: 0x000A23E0
		// (set) Token: 0x060030E6 RID: 12518 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool IncludeDebugInformation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the .NET Framework resource files that are referenced in the current source.</summary>
		/// <returns>A collection that contains the file paths of .NET Framework resources that are referenced by the source.</returns>
		// Token: 0x17000C1A RID: 3098
		// (get) Token: 0x060030E7 RID: 12519 RVA: 0x00043C3C File Offset: 0x00041E3C
		public StringCollection LinkedResources
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the name of the main class.</summary>
		/// <returns>The name of the main class.</returns>
		// Token: 0x17000C1B RID: 3099
		// (get) Token: 0x060030E8 RID: 12520 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030E9 RID: 12521 RVA: 0x000092E2 File Offset: 0x000074E2
		public string MainClass
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the output assembly.</summary>
		/// <returns>The name of the output assembly.</returns>
		// Token: 0x17000C1C RID: 3100
		// (get) Token: 0x060030EA RID: 12522 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030EB RID: 12523 RVA: 0x000092E2 File Offset: 0x000074E2
		public string OutputAssembly
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the assemblies referenced by the current project.</summary>
		/// <returns>A collection that contains the assembly names that are referenced by the source to compile.</returns>
		// Token: 0x17000C1D RID: 3101
		// (get) Token: 0x060030EC RID: 12524 RVA: 0x00043C3C File Offset: 0x00041E3C
		public StringCollection ReferencedAssemblies
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the collection that contains the temporary files.</summary>
		/// <returns>A collection that contains the temporary files.</returns>
		// Token: 0x17000C1E RID: 3102
		// (get) Token: 0x060030ED RID: 12525 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030EE RID: 12526 RVA: 0x000092E2 File Offset: 0x000074E2
		public TempFileCollection TempFiles
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether to treat warnings as errors.</summary>
		/// <returns>
		///     <see langword="true" /> if warnings should be treated as errors; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000C1F RID: 3103
		// (get) Token: 0x060030EF RID: 12527 RVA: 0x000A41FC File Offset: 0x000A23FC
		// (set) Token: 0x060030F0 RID: 12528 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool TreatWarningsAsErrors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the user token to use when creating the compiler process.</summary>
		/// <returns>The user token to use.</returns>
		// Token: 0x17000C20 RID: 3104
		// (get) Token: 0x060030F1 RID: 12529 RVA: 0x000A4218 File Offset: 0x000A2418
		// (set) Token: 0x060030F2 RID: 12530 RVA: 0x000092E2 File Offset: 0x000074E2
		public IntPtr UserToken
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the warning level at which the compiler aborts compilation.</summary>
		/// <returns>The warning level at which the compiler aborts compilation.</returns>
		// Token: 0x17000C21 RID: 3105
		// (get) Token: 0x060030F3 RID: 12531 RVA: 0x000A4234 File Offset: 0x000A2434
		// (set) Token: 0x060030F4 RID: 12532 RVA: 0x000092E2 File Offset: 0x000074E2
		public int WarningLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the file name of a Win32 resource file to link into the compiled assembly.</summary>
		/// <returns>A Win32 resource file that will be linked into the compiled assembly.</returns>
		// Token: 0x17000C22 RID: 3106
		// (get) Token: 0x060030F5 RID: 12533 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030F6 RID: 12534 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Win32Resource
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
