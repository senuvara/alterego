﻿using System;
using System.IO;
using System.Security.Permissions;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Provides an empty implementation of the <see cref="T:System.CodeDom.Compiler.ICodeParser" /> interface.</summary>
	// Token: 0x020006E1 RID: 1761
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public abstract class CodeParser : ICodeParser
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CodeParser" /> class. </summary>
		// Token: 0x06003785 RID: 14213 RVA: 0x000092E2 File Offset: 0x000074E2
		protected CodeParser()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Compiles the specified text stream into a <see cref="T:System.CodeDom.CodeCompileUnit" />.</summary>
		/// <param name="codeStream">A <see cref="T:System.IO.TextReader" /> that is used to read the code to be parsed. </param>
		/// <returns>A <see cref="T:System.CodeDom.CodeCompileUnit" /> containing the code model produced from parsing the code.</returns>
		// Token: 0x06003786 RID: 14214
		public abstract CodeCompileUnit Parse(TextReader codeStream);
	}
}
