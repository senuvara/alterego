﻿using System;
using System.Collections.Specialized;
using System.Reflection;
using System.Security.Permissions;
using System.Security.Policy;
using Unity;

namespace System.CodeDom.Compiler
{
	/// <summary>Represents the results of compilation that are returned from a compiler.</summary>
	// Token: 0x020005F6 RID: 1526
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[Serializable]
	public class CompilerResults
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.Compiler.CompilerResults" /> class that uses the specified temporary files.</summary>
		/// <param name="tempFiles">A <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> with which to manage and store references to intermediate files generated during compilation. </param>
		// Token: 0x060030F7 RID: 12535 RVA: 0x000092E2 File Offset: 0x000074E2
		[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
		public CompilerResults(TempFileCollection tempFiles)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the compiled assembly.</summary>
		/// <returns>An <see cref="T:System.Reflection.Assembly" /> that indicates the compiled assembly.</returns>
		// Token: 0x17000C23 RID: 3107
		// (get) Token: 0x060030F8 RID: 12536 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030F9 RID: 12537 RVA: 0x000092E2 File Offset: 0x000074E2
		public Assembly CompiledAssembly
		{
			[SecurityPermission(SecurityAction.Assert, Flags = SecurityPermissionFlag.ControlEvidence)]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of compiler errors and warnings.</summary>
		/// <returns>A <see cref="T:System.CodeDom.Compiler.CompilerErrorCollection" /> that indicates the errors and warnings resulting from compilation, if any.</returns>
		// Token: 0x17000C24 RID: 3108
		// (get) Token: 0x060030FA RID: 12538 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CompilerErrorCollection Errors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Indicates the evidence object that represents the security policy permissions of the compiled assembly.</summary>
		/// <returns>An <see cref="T:System.Security.Policy.Evidence" /> object that represents the security policy permissions of the compiled assembly.</returns>
		// Token: 0x17000C25 RID: 3109
		// (get) Token: 0x060030FB RID: 12539 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060030FC RID: 12540 RVA: 0x000092E2 File Offset: 0x000074E2
		public Evidence Evidence
		{
			[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true)]
			[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the compiler's return value.</summary>
		/// <returns>The compiler's return value.</returns>
		// Token: 0x17000C26 RID: 3110
		// (get) Token: 0x060030FD RID: 12541 RVA: 0x000A4250 File Offset: 0x000A2450
		// (set) Token: 0x060030FE RID: 12542 RVA: 0x000092E2 File Offset: 0x000074E2
		public int NativeCompilerReturnValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the compiler output messages.</summary>
		/// <returns>A <see cref="T:System.Collections.Specialized.StringCollection" /> that contains the output messages.</returns>
		// Token: 0x17000C27 RID: 3111
		// (get) Token: 0x060030FF RID: 12543 RVA: 0x00043C3C File Offset: 0x00041E3C
		public StringCollection Output
		{
			[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the path of the compiled assembly.</summary>
		/// <returns>The path of the assembly, or <see langword="null" /> if the assembly was generated in memory.</returns>
		// Token: 0x17000C28 RID: 3112
		// (get) Token: 0x06003100 RID: 12544 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003101 RID: 12545 RVA: 0x000092E2 File Offset: 0x000074E2
		public string PathToAssembly
		{
			[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the temporary file collection to use.</summary>
		/// <returns>A <see cref="T:System.CodeDom.Compiler.TempFileCollection" /> with which to manage and store references to intermediate files generated during compilation.</returns>
		// Token: 0x17000C29 RID: 3113
		// (get) Token: 0x06003102 RID: 12546 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003103 RID: 12547 RVA: 0x000092E2 File Offset: 0x000074E2
		public TempFileCollection TempFiles
		{
			[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
