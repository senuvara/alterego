﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents an expression that creates a delegate.</summary>
	// Token: 0x020006BB RID: 1723
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeDelegateCreateExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDelegateCreateExpression" /> class.</summary>
		// Token: 0x06003632 RID: 13874 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDelegateCreateExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDelegateCreateExpression" /> class.</summary>
		/// <param name="delegateType">A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type of the delegate. </param>
		/// <param name="targetObject">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the object containing the event-handler method. </param>
		/// <param name="methodName">The name of the event-handler method. </param>
		// Token: 0x06003633 RID: 13875 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDelegateCreateExpression(CodeTypeReference delegateType, CodeExpression targetObject, string methodName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the data type of the delegate.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type of the delegate.</returns>
		// Token: 0x17000DC4 RID: 3524
		// (get) Token: 0x06003634 RID: 13876 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003635 RID: 13877 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference DelegateType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the event handler method.</summary>
		/// <returns>The name of the event handler method.</returns>
		// Token: 0x17000DC5 RID: 3525
		// (get) Token: 0x06003636 RID: 13878 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003637 RID: 13879 RVA: 0x000092E2 File Offset: 0x000074E2
		public string MethodName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the object that contains the event-handler method.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the object containing the event-handler method.</returns>
		// Token: 0x17000DC6 RID: 3526
		// (get) Token: 0x06003638 RID: 13880 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003639 RID: 13881 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression TargetObject
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
