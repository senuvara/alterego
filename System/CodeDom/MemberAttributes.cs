﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	/// <summary>Defines member attribute identifiers for class members.</summary>
	// Token: 0x020005D7 RID: 1495
	[ComVisible(true)]
	[Serializable]
	public enum MemberAttributes
	{
		/// <summary>An abstract member.</summary>
		// Token: 0x0400246A RID: 9322
		Abstract = 1,
		/// <summary>An access mask.</summary>
		// Token: 0x0400246B RID: 9323
		AccessMask = 61440,
		/// <summary>A member that is accessible to any class within the same assembly.</summary>
		// Token: 0x0400246C RID: 9324
		Assembly = 4096,
		/// <summary>A constant member.</summary>
		// Token: 0x0400246D RID: 9325
		Const = 5,
		/// <summary>A member that is accessible within the family of its class and derived classes.</summary>
		// Token: 0x0400246E RID: 9326
		Family = 12288,
		/// <summary>A member that is accessible within its class, and derived classes in the same assembly.</summary>
		// Token: 0x0400246F RID: 9327
		FamilyAndAssembly = 8192,
		/// <summary>A member that is accessible within its class, its derived classes in any assembly, and any class in the same assembly.</summary>
		// Token: 0x04002470 RID: 9328
		FamilyOrAssembly = 16384,
		/// <summary>A member that cannot be overridden in a derived class.</summary>
		// Token: 0x04002471 RID: 9329
		Final = 2,
		/// <summary>A new member.</summary>
		// Token: 0x04002472 RID: 9330
		New = 16,
		/// <summary>An overloaded member. Some languages, such as Visual Basic, require overloaded members to be explicitly indicated.</summary>
		// Token: 0x04002473 RID: 9331
		Overloaded = 256,
		/// <summary>A member that overrides a base class member.</summary>
		// Token: 0x04002474 RID: 9332
		Override = 4,
		/// <summary>A private member.</summary>
		// Token: 0x04002475 RID: 9333
		Private = 20480,
		/// <summary>A public member.</summary>
		// Token: 0x04002476 RID: 9334
		Public = 24576,
		/// <summary>A scope mask.</summary>
		// Token: 0x04002477 RID: 9335
		ScopeMask = 15,
		/// <summary>A static member. In Visual Basic, this is equivalent to the <see langword="Shared" /> keyword.</summary>
		// Token: 0x04002478 RID: 9336
		Static = 3,
		/// <summary>A VTable mask.</summary>
		// Token: 0x04002479 RID: 9337
		VTableMask = 240
	}
}
