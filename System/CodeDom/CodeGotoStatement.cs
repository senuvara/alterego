﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a <see langword="goto" /> statement.</summary>
	// Token: 0x020006C1 RID: 1729
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeGotoStatement : CodeStatement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeGotoStatement" /> class. </summary>
		// Token: 0x06003651 RID: 13905 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeGotoStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeGotoStatement" /> class using the specified label name.</summary>
		/// <param name="label">The name of the label at which to continue program execution. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="Label" /> is <see langword="null" />.</exception>
		// Token: 0x06003652 RID: 13906 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeGotoStatement(string label)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the label at which to continue program execution.</summary>
		/// <returns>A string that indicates the name of the label at which to continue program execution.</returns>
		/// <exception cref="T:System.ArgumentNullException">The label cannot be set because<paramref name=" value" /> is <see langword="null" /> or an empty string.</exception>
		// Token: 0x17000DCE RID: 3534
		// (get) Token: 0x06003653 RID: 13907 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003654 RID: 13908 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Label
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
