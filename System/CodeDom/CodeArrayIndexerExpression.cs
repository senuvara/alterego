﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to an index of an array.</summary>
	// Token: 0x020006AD RID: 1709
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeArrayIndexerExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayIndexerExpression" /> class.</summary>
		// Token: 0x060035DD RID: 13789 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayIndexerExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayIndexerExpression" /> class using the specified target object and indexes.</summary>
		/// <param name="targetObject">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the array the indexer targets. </param>
		/// <param name="indices">The index or indexes to reference. </param>
		// Token: 0x060035DE RID: 13790 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayIndexerExpression(CodeExpression targetObject, CodeExpression[] indices)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the index or indexes of the indexer expression.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpressionCollection" /> that indicates the index or indexes of the indexer expression.</returns>
		// Token: 0x17000DAA RID: 3498
		// (get) Token: 0x060035DF RID: 13791 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeExpressionCollection Indices
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the target object of the array indexer.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that represents the array being indexed.</returns>
		// Token: 0x17000DAB RID: 3499
		// (get) Token: 0x060035E0 RID: 13792 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060035E1 RID: 13793 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression TargetObject
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
