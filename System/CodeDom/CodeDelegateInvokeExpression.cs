﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents an expression that raises an event.</summary>
	// Token: 0x020006BC RID: 1724
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeDelegateInvokeExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDelegateInvokeExpression" /> class.</summary>
		// Token: 0x0600363A RID: 13882 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDelegateInvokeExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDelegateInvokeExpression" /> class using the specified target object.</summary>
		/// <param name="targetObject">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the target object. </param>
		// Token: 0x0600363B RID: 13883 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDelegateInvokeExpression(CodeExpression targetObject)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDelegateInvokeExpression" /> class using the specified target object and parameters.</summary>
		/// <param name="targetObject">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the target object. </param>
		/// <param name="parameters">An array of <see cref="T:System.CodeDom.CodeExpression" /> objects that indicate the parameters. </param>
		// Token: 0x0600363C RID: 13884 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDelegateInvokeExpression(CodeExpression targetObject, CodeExpression[] parameters)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the parameters to pass to the event handling methods attached to the event.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the parameters to pass to the event handling methods attached to the event.</returns>
		// Token: 0x17000DC7 RID: 3527
		// (get) Token: 0x0600363D RID: 13885 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeExpressionCollection Parameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the event to invoke.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the event to invoke.</returns>
		// Token: 0x17000DC8 RID: 3528
		// (get) Token: 0x0600363E RID: 13886 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600363F RID: 13887 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression TargetObject
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
