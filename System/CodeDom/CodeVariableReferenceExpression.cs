﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to a local variable.</summary>
	// Token: 0x020006DE RID: 1758
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeVariableReferenceExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeVariableReferenceExpression" /> class.</summary>
		// Token: 0x060036F2 RID: 14066 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeVariableReferenceExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeVariableReferenceExpression" /> class using the specified local variable name.</summary>
		/// <param name="variableName">The name of the local variable to reference. </param>
		// Token: 0x060036F3 RID: 14067 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeVariableReferenceExpression(string variableName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the local variable to reference.</summary>
		/// <returns>The name of the local variable to reference.</returns>
		// Token: 0x17000E03 RID: 3587
		// (get) Token: 0x060036F4 RID: 14068 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036F5 RID: 14069 RVA: 0x000092E2 File Offset: 0x000074E2
		public string VariableName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
