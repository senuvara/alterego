﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a declaration for an instance constructor of a type.</summary>
	// Token: 0x020006B9 RID: 1721
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeConstructor : CodeMemberMethod
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeConstructor" /> class.</summary>
		// Token: 0x0600362B RID: 13867 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeConstructor()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the collection of base constructor arguments.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpressionCollection" /> that contains the base constructor arguments.</returns>
		// Token: 0x17000DC1 RID: 3521
		// (get) Token: 0x0600362C RID: 13868 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeExpressionCollection BaseConstructorArgs
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of chained constructor arguments.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpressionCollection" /> that contains the chained constructor arguments.</returns>
		// Token: 0x17000DC2 RID: 3522
		// (get) Token: 0x0600362D RID: 13869 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeExpressionCollection ChainedConstructorArgs
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
