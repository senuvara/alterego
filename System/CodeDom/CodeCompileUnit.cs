﻿using System;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Provides a container for a CodeDOM program graph.</summary>
	// Token: 0x020005EA RID: 1514
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeCompileUnit : CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCompileUnit" /> class.</summary>
		// Token: 0x0600303A RID: 12346 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCompileUnit()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a collection of custom attributes for the generated assembly.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeAttributeDeclarationCollection" /> that indicates the custom attributes for the generated assembly.</returns>
		// Token: 0x17000BF5 RID: 3061
		// (get) Token: 0x0600303B RID: 12347 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeAttributeDeclarationCollection AssemblyCustomAttributes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object containing end directives.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object containing end directives.</returns>
		// Token: 0x17000BF6 RID: 3062
		// (get) Token: 0x0600303C RID: 12348 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeDirectiveCollection EndDirectives
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of namespaces.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeNamespaceCollection" /> that indicates the namespaces that the compile unit uses.</returns>
		// Token: 0x17000BF7 RID: 3063
		// (get) Token: 0x0600303D RID: 12349 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeNamespaceCollection Namespaces
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the referenced assemblies.</summary>
		/// <returns>A <see cref="T:System.Collections.Specialized.StringCollection" /> that contains the file names of the referenced assemblies.</returns>
		// Token: 0x17000BF8 RID: 3064
		// (get) Token: 0x0600303E RID: 12350 RVA: 0x00043C3C File Offset: 0x00041E3C
		public StringCollection ReferencedAssemblies
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object containing start directives.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object containing start directives.</returns>
		// Token: 0x17000BF9 RID: 3065
		// (get) Token: 0x0600303F RID: 12351 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeDirectiveCollection StartDirectives
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
