﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a member of a type using a literal code fragment.</summary>
	// Token: 0x020006D5 RID: 1749
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeSnippetTypeMember : CodeTypeMember
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeSnippetTypeMember" /> class.</summary>
		// Token: 0x060036C4 RID: 14020 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeSnippetTypeMember()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeSnippetTypeMember" /> class using the specified text.</summary>
		/// <param name="text">The literal code fragment for the type member. </param>
		// Token: 0x060036C5 RID: 14021 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeSnippetTypeMember(string text)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the literal code fragment for the type member.</summary>
		/// <returns>The literal code fragment for the type member.</returns>
		// Token: 0x17000DF7 RID: 3575
		// (get) Token: 0x060036C6 RID: 14022 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036C7 RID: 14023 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Text
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
