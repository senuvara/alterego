﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a collection of <see cref="T:System.CodeDom.CodeDirective" /> objects.</summary>
	// Token: 0x020005DB RID: 1499
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeDirectiveCollection : CollectionBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDirectiveCollection" /> class. </summary>
		// Token: 0x06002FB0 RID: 12208 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDirectiveCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDirectiveCollection" /> class with the elements in the specified code directive collection.</summary>
		/// <param name="value">A <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object with which to initialize the collection.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06002FB1 RID: 12209 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDirectiveCollection(CodeDirectiveCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDirectiveCollection" /> class with the code directive objects in the specified array. </summary>
		/// <param name="value">An array of <see cref="T:System.CodeDom.CodeDirective" /> objects with which to initialize the collection.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06002FB2 RID: 12210 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDirectiveCollection(CodeDirective[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the <see cref="T:System.CodeDom.CodeDirective" /> object at the specified index in the collection.</summary>
		/// <param name="index">The index position to access.</param>
		/// <returns>The <see cref="T:System.CodeDom.CodeDirective" /> at the index position.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is outside the valid range of index positions for the collection. </exception>
		// Token: 0x17000BDD RID: 3037
		public CodeDirective this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds the specified <see cref="T:System.CodeDom.CodeDirective" /> object to the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeDirective" /> object to add.</param>
		/// <returns>The index position at which the new element was inserted.</returns>
		// Token: 0x06002FB5 RID: 12213 RVA: 0x000A3BC0 File Offset: 0x000A1DC0
		public int Add(CodeDirective value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Adds the contents of the specified <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object to the end of the collection.</summary>
		/// <param name="value">A <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object containing the <see cref="T:System.CodeDom.CodeDirective" /> objects to add to the collection.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06002FB6 RID: 12214 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(CodeDirectiveCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds an array of <see cref="T:System.CodeDom.CodeDirective" /> objects to the end of the collection.</summary>
		/// <param name="value">An array of <see cref="T:System.CodeDom.CodeDirective" /> objects to add to the collection.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06002FB7 RID: 12215 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(CodeDirective[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value indicating whether the collection contains the specified <see cref="T:System.CodeDom.CodeDirective" /> object.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeDirective" /> object to search for in the collection.</param>
		/// <returns>
		///     <see langword="true" /> if the collection contains the specified object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002FB8 RID: 12216 RVA: 0x000A3BDC File Offset: 0x000A1DDC
		public bool Contains(CodeDirective value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the contents of the collection to a one-dimensional array beginning at the specified index.</summary>
		/// <param name="array">An array of type <see cref="T:System.CodeDom.CodeDirective" /> that is the destination of the values copied from the collection. </param>
		/// <param name="index">The index in the array at which to begin inserting collection objects. </param>
		/// <exception cref="T:System.ArgumentException">The destination array is multidimensional.-or- The number of elements in the <see cref="T:System.CodeDom.CodeDirectiveCollection" /> is greater than the available space between the index of the target array specified by <paramref name="index" /> and the end of the target array. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than the target array's minimum index. </exception>
		// Token: 0x06002FB9 RID: 12217 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(CodeDirective[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the index in the collection of the specified <see cref="T:System.CodeDom.CodeDirective" /> object, if it exists in the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeDirective" /> object to locate in the collection.</param>
		/// <returns>The index position in the collection of the specified object, if found; otherwise, -1.</returns>
		// Token: 0x06002FBA RID: 12218 RVA: 0x000A3BF8 File Offset: 0x000A1DF8
		public int IndexOf(CodeDirective value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Inserts the specified <see cref="T:System.CodeDom.CodeDirective" /> object into the collection at the specified index.</summary>
		/// <param name="index">The zero-based index position where the specified object should be inserted. </param>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeDirective" /> object to insert. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than 0. -or-
		///         <paramref name="index" /> is greater than <see cref="P:System.Collections.CollectionBase.Count" />.</exception>
		// Token: 0x06002FBB RID: 12219 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Insert(int index, CodeDirective value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the specified <see cref="T:System.CodeDom.CodeDirective" /> object from the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeDirective" /> object to remove from the collection.</param>
		/// <exception cref="T:System.ArgumentException">The specified object is not found in the collection. </exception>
		// Token: 0x06002FBC RID: 12220 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(CodeDirective value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
