﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to the base class.</summary>
	// Token: 0x020006B1 RID: 1713
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeBaseReferenceExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeBaseReferenceExpression" /> class. </summary>
		// Token: 0x060035F5 RID: 13813 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeBaseReferenceExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
