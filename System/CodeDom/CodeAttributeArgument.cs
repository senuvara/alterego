﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents an argument used in a metadata attribute declaration.</summary>
	// Token: 0x020005E5 RID: 1509
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeAttributeArgument
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeArgument" /> class.</summary>
		// Token: 0x0600300A RID: 12298 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeArgument()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeArgument" /> class using the specified value.</summary>
		/// <param name="value">A <see cref="T:System.CodeDom.CodeExpression" /> that represents the value of the argument. </param>
		// Token: 0x0600300B RID: 12299 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeArgument(CodeExpression value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeArgument" /> class using the specified name and value.</summary>
		/// <param name="name">The name of the attribute property the argument applies to. </param>
		/// <param name="value">A <see cref="T:System.CodeDom.CodeExpression" /> that represents the value of the argument. </param>
		// Token: 0x0600300C RID: 12300 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeArgument(string name, CodeExpression value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the attribute.</summary>
		/// <returns>The name of the attribute property the argument is for.</returns>
		// Token: 0x17000BF0 RID: 3056
		// (get) Token: 0x0600300D RID: 12301 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600300E RID: 12302 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the value for the attribute argument.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the value for the attribute argument.</returns>
		// Token: 0x17000BF1 RID: 3057
		// (get) Token: 0x0600300F RID: 12303 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003010 RID: 12304 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
