﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a delegate declaration.</summary>
	// Token: 0x020006DA RID: 1754
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeTypeDelegate : CodeTypeDeclaration
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeDelegate" /> class.</summary>
		// Token: 0x060036D4 RID: 14036 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeDelegate()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeDelegate" /> class.</summary>
		/// <param name="name">The name of the delegate. </param>
		// Token: 0x060036D5 RID: 14037 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeDelegate(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the parameters of the delegate.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeParameterDeclarationExpressionCollection" /> that indicates the parameters of the delegate.</returns>
		// Token: 0x17000DFC RID: 3580
		// (get) Token: 0x060036D6 RID: 14038 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeParameterDeclarationExpressionCollection Parameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the return type of the delegate.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the return type of the delegate.</returns>
		// Token: 0x17000DFD RID: 3581
		// (get) Token: 0x060036D7 RID: 14039 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036D8 RID: 14040 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference ReturnType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
