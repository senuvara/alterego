﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Provides a common base class for most Code Document Object Model (CodeDOM) objects.</summary>
	// Token: 0x020005D6 RID: 1494
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeObject" /> class.</summary>
		// Token: 0x06002F96 RID: 12182 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeObject()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the user-definable data for the current object.</summary>
		/// <returns>An <see cref="T:System.Collections.IDictionary" /> containing user data for the current object.</returns>
		// Token: 0x17000BD7 RID: 3031
		// (get) Token: 0x06002F97 RID: 12183 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IDictionary UserData
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
