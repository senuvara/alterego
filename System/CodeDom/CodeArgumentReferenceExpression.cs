﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to the value of an argument passed to a method.</summary>
	// Token: 0x02000622 RID: 1570
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeArgumentReferenceExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArgumentReferenceExpression" /> class.</summary>
		// Token: 0x06003279 RID: 12921 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArgumentReferenceExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArgumentReferenceExpression" /> class using the specified parameter name.</summary>
		/// <param name="parameterName">The name of the parameter to reference. </param>
		// Token: 0x0600327A RID: 12922 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArgumentReferenceExpression(string parameterName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the parameter this expression references.</summary>
		/// <returns>The name of the parameter to reference.</returns>
		// Token: 0x17000C96 RID: 3222
		// (get) Token: 0x0600327B RID: 12923 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600327C RID: 12924 RVA: 0x000092E2 File Offset: 0x000074E2
		public string ParameterName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
