﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a declaration for a method of a type.</summary>
	// Token: 0x02000610 RID: 1552
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeMemberMethod : CodeTypeMember
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMemberMethod" /> class. </summary>
		// Token: 0x060031B1 RID: 12721 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMemberMethod()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the data types of the interfaces implemented by this method, unless it is a private method implementation, which is indicated by the <see cref="P:System.CodeDom.CodeMemberMethod.PrivateImplementationType" /> property.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReferenceCollection" /> that indicates the interfaces implemented by this method.</returns>
		// Token: 0x17000C5A RID: 3162
		// (get) Token: 0x060031B2 RID: 12722 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeReferenceCollection ImplementationTypes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the parameter declarations for the method.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeParameterDeclarationExpressionCollection" /> that indicates the method parameters.</returns>
		// Token: 0x17000C5B RID: 3163
		// (get) Token: 0x060031B3 RID: 12723 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeParameterDeclarationExpressionCollection Parameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the data type of the interface this method, if private, implements a method of, if any.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type of the interface with the method that the private method whose declaration is represented by this <see cref="T:System.CodeDom.CodeMemberMethod" /> implements.</returns>
		// Token: 0x17000C5C RID: 3164
		// (get) Token: 0x060031B4 RID: 12724 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060031B5 RID: 12725 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference PrivateImplementationType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the data type of the return value of the method.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type of the value returned by the method.</returns>
		// Token: 0x17000C5D RID: 3165
		// (get) Token: 0x060031B6 RID: 12726 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060031B7 RID: 12727 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference ReturnType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the custom attributes of the return type of the method.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeAttributeDeclarationCollection" /> that indicates the custom attributes.</returns>
		// Token: 0x17000C5E RID: 3166
		// (get) Token: 0x060031B8 RID: 12728 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeAttributeDeclarationCollection ReturnTypeCustomAttributes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the statements within the method.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeStatementCollection" /> that indicates the statements within the method.</returns>
		// Token: 0x17000C5F RID: 3167
		// (get) Token: 0x060031B9 RID: 12729 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeStatementCollection Statements
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the type parameters for the current generic method.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeParameterCollection" /> that contains the type parameters for the generic method.</returns>
		// Token: 0x17000C60 RID: 3168
		// (get) Token: 0x060031BA RID: 12730 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeParameterCollection TypeParameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>An event that will be raised the first time the <see cref="P:System.CodeDom.CodeMemberMethod.ImplementationTypes" /> collection is accessed.</summary>
		// Token: 0x14000057 RID: 87
		// (add) Token: 0x060031BB RID: 12731 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060031BC RID: 12732 RVA: 0x000092E2 File Offset: 0x000074E2
		public event EventHandler PopulateImplementationTypes
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>An event that will be raised the first time the <see cref="P:System.CodeDom.CodeMemberMethod.Parameters" /> collection is accessed.</summary>
		// Token: 0x14000058 RID: 88
		// (add) Token: 0x060031BD RID: 12733 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060031BE RID: 12734 RVA: 0x000092E2 File Offset: 0x000074E2
		public event EventHandler PopulateParameters
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>An event that will be raised the first time the <see cref="P:System.CodeDom.CodeMemberMethod.Statements" /> collection is accessed.</summary>
		// Token: 0x14000059 RID: 89
		// (add) Token: 0x060031BF RID: 12735 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060031C0 RID: 12736 RVA: 0x000092E2 File Offset: 0x000074E2
		public event EventHandler PopulateStatements
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
