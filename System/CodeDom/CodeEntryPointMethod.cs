﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents the entry point method of an executable.</summary>
	// Token: 0x020006BE RID: 1726
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeEntryPointMethod : CodeMemberMethod
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeEntryPointMethod" /> class.</summary>
		// Token: 0x06003646 RID: 13894 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeEntryPointMethod()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
