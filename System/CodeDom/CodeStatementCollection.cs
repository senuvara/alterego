﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a collection of <see cref="T:System.CodeDom.CodeStatement" /> objects.</summary>
	// Token: 0x02000605 RID: 1541
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeStatementCollection : CollectionBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeStatementCollection" /> class.</summary>
		// Token: 0x0600314F RID: 12623 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeStatementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeStatementCollection" /> class that contains the elements of the specified source collection.</summary>
		/// <param name="value">A <see cref="T:System.CodeDom.CodeStatementCollection" /> object with which to initialize the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06003150 RID: 12624 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeStatementCollection(CodeStatementCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeStatementCollection" /> class that contains the specified array of <see cref="T:System.CodeDom.CodeStatement" /> objects.</summary>
		/// <param name="value">An array of <see cref="T:System.CodeDom.CodeStatement" /> objects with which to initialize the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06003151 RID: 12625 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeStatementCollection(CodeStatement[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the <see cref="T:System.CodeDom.CodeStatement" /> object at the specified index in the collection.</summary>
		/// <param name="index">The index of the collection to access. </param>
		/// <returns>A <see cref="T:System.CodeDom.CodeStatement" /> at each valid index.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="index" /> parameter is outside the valid range of indexes for the collection. </exception>
		// Token: 0x17000C39 RID: 3129
		public CodeStatement this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds the specified <see cref="T:System.CodeDom.CodeExpression" /> object to the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeExpression" /> object to add. </param>
		/// <returns>The index at which the new element was inserted.</returns>
		// Token: 0x06003154 RID: 12628 RVA: 0x000A4384 File Offset: 0x000A2584
		public int Add(CodeExpression value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Adds the specified <see cref="T:System.CodeDom.CodeStatement" /> object to the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeStatement" /> object to add. </param>
		/// <returns>The index at which the new element was inserted.</returns>
		// Token: 0x06003155 RID: 12629 RVA: 0x000A43A0 File Offset: 0x000A25A0
		public int Add(CodeStatement value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Adds the contents of another <see cref="T:System.CodeDom.CodeStatementCollection" /> object to the end of the collection.</summary>
		/// <param name="value">A <see cref="T:System.CodeDom.CodeStatementCollection" /> object that contains the objects to add to the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06003156 RID: 12630 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(CodeStatementCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a set of <see cref="T:System.CodeDom.CodeStatement" /> objects to the collection.</summary>
		/// <param name="value">An array of <see cref="T:System.CodeDom.CodeStatement" /> objects to add to the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06003157 RID: 12631 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(CodeStatement[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates whether the collection contains the specified <see cref="T:System.CodeDom.CodeStatement" /> object.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeStatement" /> object to search for in the collection. </param>
		/// <returns>
		///     <see langword="true" /> if the collection contains the specified object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003158 RID: 12632 RVA: 0x000A43BC File Offset: 0x000A25BC
		public bool Contains(CodeStatement value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the elements of the <see cref="T:System.CodeDom.CodeStatementCollection" /> object to a one-dimensional <see cref="T:System.Array" /> instance, starting at the specified index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the values copied from the collection. </param>
		/// <param name="index">The index of the array at which to begin inserting. </param>
		/// <exception cref="T:System.ArgumentException">The destination array is multidimensional.-or- The number of elements in the <see cref="T:System.CodeDom.CodeStatementCollection" /> is greater than the available space between the index of the target array specified by the <paramref name="index" /> parameter and the end of the target array. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="array" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="index" /> parameter is less than the target array's minimum index. </exception>
		// Token: 0x06003159 RID: 12633 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(CodeStatement[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the index of the specified <see cref="T:System.CodeDom.CodeStatement" /> object in the <see cref="T:System.CodeDom.CodeStatementCollection" />, if it exists in the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeStatement" /> to locate in the collection. </param>
		/// <returns>The index of the specified object, if it is found, in the collection; otherwise, -1.</returns>
		// Token: 0x0600315A RID: 12634 RVA: 0x000A43D8 File Offset: 0x000A25D8
		public int IndexOf(CodeStatement value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Inserts the specified <see cref="T:System.CodeDom.CodeStatement" /> object into the collection at the specified index.</summary>
		/// <param name="index">The zero-based index where the specified object should be inserted. </param>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeStatement" /> object to insert. </param>
		// Token: 0x0600315B RID: 12635 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Insert(int index, CodeStatement value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the specified <see cref="T:System.CodeDom.CodeStatement" /> object from the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeStatement" /> to remove from the collection. </param>
		/// <exception cref="T:System.ArgumentException">The specified object is not found in the collection. </exception>
		// Token: 0x0600315C RID: 12636 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(CodeStatement value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
