﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to an indexer property of an object.</summary>
	// Token: 0x020006C2 RID: 1730
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeIndexerExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeIndexerExpression" /> class.</summary>
		// Token: 0x06003655 RID: 13909 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeIndexerExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeIndexerExpression" /> class using the specified target object and index.</summary>
		/// <param name="targetObject">The target object. </param>
		/// <param name="indices">The index or indexes of the indexer expression. </param>
		// Token: 0x06003656 RID: 13910 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeIndexerExpression(CodeExpression targetObject, CodeExpression[] indices)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the collection of indexes of the indexer expression.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpressionCollection" /> that indicates the index or indexes of the indexer expression.</returns>
		// Token: 0x17000DCF RID: 3535
		// (get) Token: 0x06003657 RID: 13911 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeExpressionCollection Indices
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the target object that can be indexed.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the indexer object.</returns>
		// Token: 0x17000DD0 RID: 3536
		// (get) Token: 0x06003658 RID: 13912 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003659 RID: 13913 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression TargetObject
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
