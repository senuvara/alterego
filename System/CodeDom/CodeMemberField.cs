﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a declaration for a field of a type.</summary>
	// Token: 0x020006C6 RID: 1734
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeMemberField : CodeTypeMember
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMemberField" /> class.</summary>
		// Token: 0x06003670 RID: 13936 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMemberField()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMemberField" /> class using the specified field type and field name.</summary>
		/// <param name="type">An object that indicates the type of the field. </param>
		/// <param name="name">The name of the field. </param>
		// Token: 0x06003671 RID: 13937 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMemberField(CodeTypeReference type, string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMemberField" /> class using the specified field type and field name.</summary>
		/// <param name="type">The type of the field. </param>
		/// <param name="name">The name of the field. </param>
		// Token: 0x06003672 RID: 13938 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMemberField(string type, string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMemberField" /> class using the specified field type and field name.</summary>
		/// <param name="type">The type of the field. </param>
		/// <param name="name">The name of the field. </param>
		// Token: 0x06003673 RID: 13939 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMemberField(Type type, string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the initialization expression for the field.</summary>
		/// <returns>The initialization expression for the field.</returns>
		// Token: 0x17000DDA RID: 3546
		// (get) Token: 0x06003674 RID: 13940 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003675 RID: 13941 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression InitExpression
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the type of the field.</summary>
		/// <returns>The type of the field.</returns>
		// Token: 0x17000DDB RID: 3547
		// (get) Token: 0x06003676 RID: 13942 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003677 RID: 13943 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
