﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a statement that consists of a single expression.</summary>
	// Token: 0x020006BF RID: 1727
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeExpressionStatement : CodeStatement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeExpressionStatement" /> class.</summary>
		// Token: 0x06003647 RID: 13895 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpressionStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeExpressionStatement" /> class by using the specified expression.</summary>
		/// <param name="expression">A <see cref="T:System.CodeDom.CodeExpression" /> for the statement. </param>
		// Token: 0x06003648 RID: 13896 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpressionStatement(CodeExpression expression)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the expression for the statement.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the expression for the statement.</returns>
		// Token: 0x17000DCB RID: 3531
		// (get) Token: 0x06003649 RID: 13897 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600364A RID: 13898 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression Expression
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
