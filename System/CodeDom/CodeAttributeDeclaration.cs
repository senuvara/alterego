﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents an attribute declaration.</summary>
	// Token: 0x020005E0 RID: 1504
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeAttributeDeclaration
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeDeclaration" /> class.</summary>
		// Token: 0x06002FD8 RID: 12248 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeDeclaration()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeDeclaration" /> class using the specified code type reference.</summary>
		/// <param name="attributeType">The <see cref="T:System.CodeDom.CodeTypeReference" /> that identifies the attribute.</param>
		// Token: 0x06002FD9 RID: 12249 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeDeclaration(CodeTypeReference attributeType)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeDeclaration" /> class using the specified code type reference and arguments.</summary>
		/// <param name="attributeType">The <see cref="T:System.CodeDom.CodeTypeReference" /> that identifies the attribute.</param>
		/// <param name="arguments">An array of type <see cref="T:System.CodeDom.CodeAttributeArgument" /> that contains the arguments for the attribute.</param>
		// Token: 0x06002FDA RID: 12250 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeDeclaration(CodeTypeReference attributeType, CodeAttributeArgument[] arguments)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeDeclaration" /> class using the specified name.</summary>
		/// <param name="name">The name of the attribute. </param>
		// Token: 0x06002FDB RID: 12251 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeDeclaration(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeDeclaration" /> class using the specified name and arguments.</summary>
		/// <param name="name">The name of the attribute. </param>
		/// <param name="arguments">An array of type <see cref="T:System.CodeDom.CodeAttributeArgument" />  that contains the arguments for the attribute. </param>
		// Token: 0x06002FDC RID: 12252 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeDeclaration(string name, CodeAttributeArgument[] arguments)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the arguments for the attribute.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeAttributeArgumentCollection" /> that contains the arguments for the attribute.</returns>
		// Token: 0x17000BE3 RID: 3043
		// (get) Token: 0x06002FDD RID: 12253 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeAttributeArgumentCollection Arguments
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the code type reference for the code attribute declaration.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that identifies the <see cref="T:System.CodeDom.CodeAttributeDeclaration" />.</returns>
		// Token: 0x17000BE4 RID: 3044
		// (get) Token: 0x06002FDE RID: 12254 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeReference AttributeType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the name of the attribute being declared.</summary>
		/// <returns>The name of the attribute.</returns>
		// Token: 0x17000BE5 RID: 3045
		// (get) Token: 0x06002FDF RID: 12255 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002FE0 RID: 12256 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
