﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Specifies the name and mode for a code region.</summary>
	// Token: 0x020006CF RID: 1743
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeRegionDirective : CodeDirective
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeRegionDirective" /> class with default values. </summary>
		// Token: 0x060036A9 RID: 13993 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeRegionDirective()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeRegionDirective" /> class, specifying its mode and name. </summary>
		/// <param name="regionMode">One of the <see cref="T:System.CodeDom.CodeRegionMode" /> values.</param>
		/// <param name="regionText">The name for the region.</param>
		// Token: 0x060036AA RID: 13994 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeRegionDirective(CodeRegionMode regionMode, string regionText)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the mode for the region directive.</summary>
		/// <returns>One of the <see cref="T:System.CodeDom.CodeRegionMode" /> values. The default is <see cref="F:System.CodeDom.CodeRegionMode.None" />.</returns>
		// Token: 0x17000DEF RID: 3567
		// (get) Token: 0x060036AB RID: 13995 RVA: 0x000A5CE4 File Offset: 0x000A3EE4
		// (set) Token: 0x060036AC RID: 13996 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeRegionMode RegionMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return CodeRegionMode.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the region.</summary>
		/// <returns>The name of the region.</returns>
		// Token: 0x17000DF0 RID: 3568
		// (get) Token: 0x060036AD RID: 13997 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036AE RID: 13998 RVA: 0x000092E2 File Offset: 0x000074E2
		public string RegionText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
