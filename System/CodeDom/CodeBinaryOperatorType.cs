﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	/// <summary>Defines identifiers for supported binary operators.</summary>
	// Token: 0x020006B3 RID: 1715
	[ComVisible(true)]
	[Serializable]
	public enum CodeBinaryOperatorType
	{
		/// <summary>Addition operator.</summary>
		// Token: 0x04002527 RID: 9511
		Add,
		/// <summary>Assignment operator.</summary>
		// Token: 0x04002528 RID: 9512
		Assign = 5,
		/// <summary>Bitwise and operator.</summary>
		// Token: 0x04002529 RID: 9513
		BitwiseAnd = 10,
		/// <summary>Bitwise or operator.</summary>
		// Token: 0x0400252A RID: 9514
		BitwiseOr = 9,
		/// <summary>Boolean and operator. This represents a short circuiting operator. A short circuiting operator will evaluate only as many expressions as necessary before returning a correct value.</summary>
		// Token: 0x0400252B RID: 9515
		BooleanAnd = 12,
		/// <summary>Boolean or operator. This represents a short circuiting operator. A short circuiting operator will evaluate only as many expressions as necessary before returning a correct value.</summary>
		// Token: 0x0400252C RID: 9516
		BooleanOr = 11,
		/// <summary>Division operator.</summary>
		// Token: 0x0400252D RID: 9517
		Divide = 3,
		/// <summary>Greater than operator.</summary>
		// Token: 0x0400252E RID: 9518
		GreaterThan = 15,
		/// <summary>Greater than or equal operator.</summary>
		// Token: 0x0400252F RID: 9519
		GreaterThanOrEqual,
		/// <summary>Identity equal operator.</summary>
		// Token: 0x04002530 RID: 9520
		IdentityEquality = 7,
		/// <summary>Identity not equal operator.</summary>
		// Token: 0x04002531 RID: 9521
		IdentityInequality = 6,
		/// <summary>Less than operator.</summary>
		// Token: 0x04002532 RID: 9522
		LessThan = 13,
		/// <summary>Less than or equal operator.</summary>
		// Token: 0x04002533 RID: 9523
		LessThanOrEqual,
		/// <summary>Modulus operator.</summary>
		// Token: 0x04002534 RID: 9524
		Modulus = 4,
		/// <summary>Multiplication operator.</summary>
		// Token: 0x04002535 RID: 9525
		Multiply = 2,
		/// <summary>Subtraction operator.</summary>
		// Token: 0x04002536 RID: 9526
		Subtract = 1,
		/// <summary>Value equal operator.</summary>
		// Token: 0x04002537 RID: 9527
		ValueEquality = 8
	}
}
