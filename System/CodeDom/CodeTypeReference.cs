﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to a type.</summary>
	// Token: 0x020005E1 RID: 1505
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeTypeReference : CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReference" /> class. </summary>
		// Token: 0x06002FE1 RID: 12257 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReference" /> class using the specified code type parameter. </summary>
		/// <param name="typeParameter">A <see cref="T:System.CodeDom.CodeTypeParameter" /> that represents the type of the type parameter.</param>
		// Token: 0x06002FE2 RID: 12258 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference(CodeTypeParameter typeParameter)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReference" /> class using the specified array type and rank.</summary>
		/// <param name="arrayType">A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the type of the array. </param>
		/// <param name="rank">The number of dimensions in the array. </param>
		// Token: 0x06002FE3 RID: 12259 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference(CodeTypeReference arrayType, int rank)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReference" /> class using the specified type name.</summary>
		/// <param name="typeName">The name of the type to reference. </param>
		// Token: 0x06002FE4 RID: 12260 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference(string typeName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReference" /> class using the specified type name and code type reference option.</summary>
		/// <param name="typeName">The name of the type to reference.</param>
		/// <param name="codeTypeReferenceOption">The code type reference option, one of the <see cref="T:System.CodeDom.CodeTypeReferenceOptions" /> values.</param>
		// Token: 0x06002FE5 RID: 12261 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference(string typeName, CodeTypeReferenceOptions codeTypeReferenceOption)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReference" /> class using the specified type name and type arguments.</summary>
		/// <param name="typeName">The name of the type to reference.</param>
		/// <param name="typeArguments">An array of <see cref="T:System.CodeDom.CodeTypeReference" /> values.</param>
		// Token: 0x06002FE6 RID: 12262 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference(string typeName, CodeTypeReference[] typeArguments)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReference" /> class using the specified array type name and rank.</summary>
		/// <param name="baseType">The name of the type of the elements of the array. </param>
		/// <param name="rank">The number of dimensions of the array. </param>
		// Token: 0x06002FE7 RID: 12263 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference(string baseType, int rank)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReference" /> class using the specified type.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> to reference. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type " />is <see langword="null" />.</exception>
		// Token: 0x06002FE8 RID: 12264 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReference" /> class using the specified type and code type reference.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> to reference.</param>
		/// <param name="codeTypeReferenceOption">The code type reference option, one of the <see cref="T:System.CodeDom.CodeTypeReferenceOptions" /> values. </param>
		// Token: 0x06002FE9 RID: 12265 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference(Type type, CodeTypeReferenceOptions codeTypeReferenceOption)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the type of the elements in the array.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the type of the array elements.</returns>
		// Token: 0x17000BE6 RID: 3046
		// (get) Token: 0x06002FEA RID: 12266 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002FEB RID: 12267 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference ArrayElementType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the array rank of the array.</summary>
		/// <returns>The number of dimensions of the array.</returns>
		// Token: 0x17000BE7 RID: 3047
		// (get) Token: 0x06002FEC RID: 12268 RVA: 0x000A3CA0 File Offset: 0x000A1EA0
		// (set) Token: 0x06002FED RID: 12269 RVA: 0x000092E2 File Offset: 0x000074E2
		public int ArrayRank
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the type being referenced.</summary>
		/// <returns>The name of the type being referenced.</returns>
		// Token: 0x17000BE8 RID: 3048
		// (get) Token: 0x06002FEE RID: 12270 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002FEF RID: 12271 RVA: 0x000092E2 File Offset: 0x000074E2
		public string BaseType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the code type reference option.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.CodeDom.CodeTypeReferenceOptions" /> values. </returns>
		// Token: 0x17000BE9 RID: 3049
		// (get) Token: 0x06002FF0 RID: 12272 RVA: 0x000A3CBC File Offset: 0x000A1EBC
		// (set) Token: 0x06002FF1 RID: 12273 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReferenceOptions Options
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (CodeTypeReferenceOptions)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the type arguments for the current generic type reference.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReferenceCollection" /> containing the type arguments for the current <see cref="T:System.CodeDom.CodeTypeReference" /> object.</returns>
		// Token: 0x17000BEA RID: 3050
		// (get) Token: 0x06002FF2 RID: 12274 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeReferenceCollection TypeArguments
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
