﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to a default value.</summary>
	// Token: 0x020006BA RID: 1722
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeDefaultValueExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDefaultValueExpression" /> class. </summary>
		// Token: 0x0600362E RID: 13870 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDefaultValueExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDefaultValueExpression" /> class using the specified code type reference.</summary>
		/// <param name="type">A <see cref="T:System.CodeDom.CodeTypeReference" /> that specifies the reference to a value type.</param>
		// Token: 0x0600362F RID: 13871 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDefaultValueExpression(CodeTypeReference type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the data type reference for a default value.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> object representing a data type that has a default value.</returns>
		// Token: 0x17000DC3 RID: 3523
		// (get) Token: 0x06003630 RID: 13872 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003631 RID: 13873 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
