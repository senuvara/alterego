﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents the value argument of a property set method call within a property set method.</summary>
	// Token: 0x020006CE RID: 1742
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodePropertySetValueReferenceExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodePropertySetValueReferenceExpression" /> class. </summary>
		// Token: 0x060036A8 RID: 13992 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodePropertySetValueReferenceExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
