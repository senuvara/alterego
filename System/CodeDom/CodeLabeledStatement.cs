﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a labeled statement or a stand-alone label.</summary>
	// Token: 0x020006C4 RID: 1732
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeLabeledStatement : CodeStatement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeLabeledStatement" /> class.</summary>
		// Token: 0x06003663 RID: 13923 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeLabeledStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeLabeledStatement" /> class using the specified label name.</summary>
		/// <param name="label">The name of the label. </param>
		// Token: 0x06003664 RID: 13924 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeLabeledStatement(string label)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeLabeledStatement" /> class using the specified label name and statement.</summary>
		/// <param name="label">The name of the label. </param>
		/// <param name="statement">The <see cref="T:System.CodeDom.CodeStatement" /> to associate with the label. </param>
		// Token: 0x06003665 RID: 13925 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeLabeledStatement(string label, CodeStatement statement)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the label.</summary>
		/// <returns>The name of the label.</returns>
		// Token: 0x17000DD5 RID: 3541
		// (get) Token: 0x06003666 RID: 13926 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003667 RID: 13927 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Label
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the optional associated statement.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeStatement" /> that indicates the statement associated with the label.</returns>
		// Token: 0x17000DD6 RID: 3542
		// (get) Token: 0x06003668 RID: 13928 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003669 RID: 13929 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeStatement Statement
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
