﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	/// <summary>Specifies how the code type reference is to be resolved.</summary>
	// Token: 0x020005E4 RID: 1508
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum CodeTypeReferenceOptions
	{
		/// <summary>Resolve the type from the type parameter.</summary>
		// Token: 0x0400247B RID: 9339
		GenericTypeParameter = 2,
		/// <summary>Resolve the type from the root namespace.</summary>
		// Token: 0x0400247C RID: 9340
		GlobalReference = 1
	}
}
