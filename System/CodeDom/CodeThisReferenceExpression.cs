﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to the current local class instance.</summary>
	// Token: 0x020006D6 RID: 1750
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeThisReferenceExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeThisReferenceExpression" /> class. </summary>
		// Token: 0x060036C8 RID: 14024 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeThisReferenceExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
