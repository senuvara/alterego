﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a collection of <see cref="T:System.CodeDom.CodeNamespaceImport" /> objects.</summary>
	// Token: 0x020005ED RID: 1517
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeNamespaceImportCollection : ICollection, IEnumerable, IList
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeNamespaceImportCollection" /> class. </summary>
		// Token: 0x0600305A RID: 12378 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeNamespaceImportCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the number of namespaces in the collection.</summary>
		/// <returns>The number of namespaces in the collection.</returns>
		// Token: 0x17000BFF RID: 3071
		// (get) Token: 0x0600305B RID: 12379 RVA: 0x000A3E98 File Offset: 0x000A2098
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.CodeDom.CodeNamespaceImport" /> object at the specified index in the collection.</summary>
		/// <param name="index">The index of the collection to access. </param>
		/// <returns>A <see cref="T:System.CodeDom.CodeNamespaceImport" /> object at each valid index.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="index" /> parameter is outside the valid range of indexes for the collection. </exception>
		// Token: 0x17000C00 RID: 3072
		public CodeNamespaceImport this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x0600305E RID: 12382 RVA: 0x000A3EB4 File Offset: 0x000A20B4
		int ICollection.get_Count()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		// Token: 0x0600305F RID: 12383 RVA: 0x000A3ED0 File Offset: 0x000A20D0
		bool ICollection.get_IsSynchronized()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06003060 RID: 12384 RVA: 0x00043C3C File Offset: 0x00041E3C
		object ICollection.get_SyncRoot()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06003061 RID: 12385 RVA: 0x000A3EEC File Offset: 0x000A20EC
		bool IList.get_IsFixedSize()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06003062 RID: 12386 RVA: 0x000A3F08 File Offset: 0x000A2108
		bool IList.get_IsReadOnly()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06003063 RID: 12387 RVA: 0x00043C3C File Offset: 0x00041E3C
		object IList.get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06003064 RID: 12388 RVA: 0x000092E2 File Offset: 0x000074E2
		void IList.set_Item(int index, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a <see cref="T:System.CodeDom.CodeNamespaceImport" /> object to the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeNamespaceImport" /> object to add to the collection. </param>
		// Token: 0x06003065 RID: 12389 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Add(CodeNamespaceImport value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a set of <see cref="T:System.CodeDom.CodeNamespaceImport" /> objects to the collection.</summary>
		/// <param name="value">An array of type <see cref="T:System.CodeDom.CodeNamespaceImport" /> that contains the objects to add to the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06003066 RID: 12390 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(CodeNamespaceImport[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the collection of members.</summary>
		// Token: 0x06003067 RID: 12391 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an enumerator that enumerates the collection members.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that indicates the collection members.</returns>
		// Token: 0x06003068 RID: 12392 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from the <see cref="T:System.Collections.ICollection" />. The array must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		// Token: 0x06003069 RID: 12393 RVA: 0x000092E2 File Offset: 0x000074E2
		void ICollection.CopyTo(Array array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns an enumerator that can iterate through a collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through the collection.</returns>
		// Token: 0x0600306A RID: 12394 RVA: 0x00043C3C File Offset: 0x00041E3C
		IEnumerator IEnumerable.GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds an object to the <see cref="T:System.Collections.IList" />.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to add to the <see cref="T:System.Collections.IList" />.</param>
		/// <returns>The position at which the new element was inserted.</returns>
		// Token: 0x0600306B RID: 12395 RVA: 0x000A3F24 File Offset: 0x000A2124
		int IList.Add(object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Removes all items from the <see cref="T:System.Collections.IList" />.</summary>
		// Token: 0x0600306C RID: 12396 RVA: 0x000092E2 File Offset: 0x000074E2
		void IList.Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether the <see cref="T:System.Collections.IList" /> contains a specific value.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.IList" />.</param>
		/// <returns>
		///     <see langword="true" /> if the value is in the list; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600306D RID: 12397 RVA: 0x000A3F40 File Offset: 0x000A2140
		bool IList.Contains(object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines the index of a specific item in the <see cref="T:System.Collections.IList" />. </summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.IList" />.</param>
		/// <returns>The index of <paramref name="value" /> if it is found in the list; otherwise, -1.</returns>
		// Token: 0x0600306E RID: 12398 RVA: 0x000A3F5C File Offset: 0x000A215C
		int IList.IndexOf(object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Inserts an item in the <see cref="T:System.Collections.IList" /> at the specified position. </summary>
		/// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
		/// <param name="value">The <see cref="T:System.Object" /> to insert into the <see cref="T:System.Collections.IList" />.</param>
		// Token: 0x0600306F RID: 12399 RVA: 0x000092E2 File Offset: 0x000074E2
		void IList.Insert(int index, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the first occurrence of a specific object from the <see cref="T:System.Collections.IList" />. </summary>
		/// <param name="value">The <see cref="T:System.Object" /> to remove from the <see cref="T:System.Collections.IList" />.</param>
		// Token: 0x06003070 RID: 12400 RVA: 0x000092E2 File Offset: 0x000074E2
		void IList.Remove(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element at the specified index of the <see cref="T:System.Collections.IList" />. </summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		// Token: 0x06003071 RID: 12401 RVA: 0x000092E2 File Offset: 0x000074E2
		void IList.RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
