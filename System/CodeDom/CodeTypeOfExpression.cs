﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a <see langword="typeof" /> expression, an expression that returns a <see cref="T:System.Type" /> for a specified type name.</summary>
	// Token: 0x020006DB RID: 1755
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeTypeOfExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeOfExpression" /> class.</summary>
		// Token: 0x060036D9 RID: 14041 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeOfExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeOfExpression" /> class.</summary>
		/// <param name="type">A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type for the <see langword="typeof" /> expression. </param>
		// Token: 0x060036DA RID: 14042 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeOfExpression(CodeTypeReference type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeOfExpression" /> class using the specified type.</summary>
		/// <param name="type">The name of the data type for the <see langword="typeof" /> expression. </param>
		// Token: 0x060036DB RID: 14043 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeOfExpression(string type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeOfExpression" /> class using the specified type.</summary>
		/// <param name="type">The data type of the data type of the <see langword="typeof" /> expression. </param>
		// Token: 0x060036DC RID: 14044 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeOfExpression(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the data type referenced by the <see langword="typeof" /> expression.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type referenced by the <see langword="typeof" /> expression. This property will never return <see langword="null" />, and defaults to the <see cref="T:System.Void" /> type.</returns>
		// Token: 0x17000DFE RID: 3582
		// (get) Token: 0x060036DD RID: 14045 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036DE RID: 14046 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
