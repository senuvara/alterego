﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a declaration for an event of a type.</summary>
	// Token: 0x020006C5 RID: 1733
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeMemberEvent : CodeTypeMember
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMemberEvent" /> class.</summary>
		// Token: 0x0600366A RID: 13930 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMemberEvent()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the data type that the member event implements.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReferenceCollection" /> that indicates the data type or types that the member event implements.</returns>
		// Token: 0x17000DD7 RID: 3543
		// (get) Token: 0x0600366B RID: 13931 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeReferenceCollection ImplementationTypes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the privately implemented data type, if any.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type that the event privately implements.</returns>
		// Token: 0x17000DD8 RID: 3544
		// (get) Token: 0x0600366C RID: 13932 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600366D RID: 13933 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference PrivateImplementationType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the data type of the delegate type that handles the event.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the delegate type that handles the event.</returns>
		// Token: 0x17000DD9 RID: 3545
		// (get) Token: 0x0600366E RID: 13934 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600366F RID: 13935 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
