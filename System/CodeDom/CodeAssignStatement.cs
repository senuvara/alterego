﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a simple assignment statement.</summary>
	// Token: 0x020006AE RID: 1710
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeAssignStatement : CodeStatement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAssignStatement" /> class.</summary>
		// Token: 0x060035E2 RID: 13794 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAssignStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAssignStatement" /> class using the specified expressions.</summary>
		/// <param name="left">The variable to assign to. </param>
		/// <param name="right">The value to assign. </param>
		// Token: 0x060035E3 RID: 13795 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAssignStatement(CodeExpression left, CodeExpression right)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the expression representing the object or reference to assign to.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the object or reference to assign to.</returns>
		// Token: 0x17000DAC RID: 3500
		// (get) Token: 0x060035E4 RID: 13796 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060035E5 RID: 13797 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression Left
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the expression representing the object or reference to assign.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the object or reference to assign.</returns>
		// Token: 0x17000DAD RID: 3501
		// (get) Token: 0x060035E6 RID: 13798 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060035E7 RID: 13799 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression Right
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
