﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Serves as the base class for code directive classes.</summary>
	// Token: 0x020005DC RID: 1500
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeDirective : CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDirective" /> class. </summary>
		// Token: 0x06002FBD RID: 12221 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDirective()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
