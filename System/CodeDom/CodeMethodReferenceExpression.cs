﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to a method.</summary>
	// Token: 0x020006C9 RID: 1737
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeMethodReferenceExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMethodReferenceExpression" /> class.</summary>
		// Token: 0x0600368B RID: 13963 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMethodReferenceExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMethodReferenceExpression" /> class using the specified target object and method name.</summary>
		/// <param name="targetObject">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the object to target. </param>
		/// <param name="methodName">The name of the method to call. </param>
		// Token: 0x0600368C RID: 13964 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMethodReferenceExpression(CodeExpression targetObject, string methodName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMethodReferenceExpression" /> class using the specified target object, method name, and generic type arguments.</summary>
		/// <param name="targetObject">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the object to target. </param>
		/// <param name="methodName">The name of the method to call. </param>
		/// <param name="typeParameters">An array of <see cref="T:System.CodeDom.CodeTypeReference" /> values that specify the <see cref="P:System.CodeDom.CodeMethodReferenceExpression.TypeArguments" /> for this <see cref="T:System.CodeDom.CodeMethodReferenceExpression" />.</param>
		// Token: 0x0600368D RID: 13965 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMethodReferenceExpression(CodeExpression targetObject, string methodName, CodeTypeReference[] typeParameters)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the method to reference.</summary>
		/// <returns>The name of the method to reference.</returns>
		// Token: 0x17000DE6 RID: 3558
		// (get) Token: 0x0600368E RID: 13966 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600368F RID: 13967 RVA: 0x000092E2 File Offset: 0x000074E2
		public string MethodName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the expression that indicates the method to reference.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that represents the method to reference.</returns>
		// Token: 0x17000DE7 RID: 3559
		// (get) Token: 0x06003690 RID: 13968 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003691 RID: 13969 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression TargetObject
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the type arguments for the current generic method reference expression.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReferenceCollection" /> containing the type arguments for the current code <see cref="T:System.CodeDom.CodeMethodReferenceExpression" />.</returns>
		// Token: 0x17000DE8 RID: 3560
		// (get) Token: 0x06003692 RID: 13970 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeReferenceCollection TypeArguments
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
