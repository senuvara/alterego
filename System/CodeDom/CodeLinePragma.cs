﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a specific location within a specific file.</summary>
	// Token: 0x020005DD RID: 1501
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeLinePragma
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeLinePragma" /> class. </summary>
		// Token: 0x06002FBE RID: 12222 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeLinePragma()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeLinePragma" /> class.</summary>
		/// <param name="fileName">The file name of the associated file. </param>
		/// <param name="lineNumber">The line number to store a reference to. </param>
		// Token: 0x06002FBF RID: 12223 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeLinePragma(string fileName, int lineNumber)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the associated file.</summary>
		/// <returns>The file name of the associated file.</returns>
		// Token: 0x17000BDE RID: 3038
		// (get) Token: 0x06002FC0 RID: 12224 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002FC1 RID: 12225 RVA: 0x000092E2 File Offset: 0x000074E2
		public string FileName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the line number of the associated reference.</summary>
		/// <returns>The line number.</returns>
		// Token: 0x17000BDF RID: 3039
		// (get) Token: 0x06002FC2 RID: 12226 RVA: 0x000A3C14 File Offset: 0x000A1E14
		// (set) Token: 0x06002FC3 RID: 12227 RVA: 0x000092E2 File Offset: 0x000074E2
		public int LineNumber
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
