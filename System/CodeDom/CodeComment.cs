﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a comment.</summary>
	// Token: 0x020005DE RID: 1502
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeComment : CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeComment" /> class.</summary>
		// Token: 0x06002FC4 RID: 12228 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeComment()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeComment" /> class with the specified text as contents.</summary>
		/// <param name="text">The contents of the comment. </param>
		// Token: 0x06002FC5 RID: 12229 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeComment(string text)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeComment" /> class using the specified text and documentation comment flag.</summary>
		/// <param name="text">The contents of the comment. </param>
		/// <param name="docComment">
		///       <see langword="true" /> if the comment is a documentation comment; otherwise, <see langword="false" />. </param>
		// Token: 0x06002FC6 RID: 12230 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeComment(string text, bool docComment)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a value that indicates whether the comment is a documentation comment.</summary>
		/// <returns>
		///     <see langword="true" /> if the comment is a documentation comment; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000BE0 RID: 3040
		// (get) Token: 0x06002FC7 RID: 12231 RVA: 0x000A3C30 File Offset: 0x000A1E30
		// (set) Token: 0x06002FC8 RID: 12232 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool DocComment
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the text of the comment.</summary>
		/// <returns>A string containing the comment text.</returns>
		// Token: 0x17000BE1 RID: 3041
		// (get) Token: 0x06002FC9 RID: 12233 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002FCA RID: 12234 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Text
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
