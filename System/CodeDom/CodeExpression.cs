﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a code expression. This is a base class for other code expression objects that is never instantiated.</summary>
	// Token: 0x020005E6 RID: 1510
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeExpression : CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeExpression" /> class. </summary>
		// Token: 0x06003011 RID: 12305 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
