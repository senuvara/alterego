﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents an expression that creates an array.</summary>
	// Token: 0x020006AB RID: 1707
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeArrayCreateExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> class.</summary>
		// Token: 0x060035BF RID: 13759 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayCreateExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> class using the specified array data type and code expression indicating the number of indexes for the array.</summary>
		/// <param name="createType">A <see cref="T:System.CodeDom.CodeTypeReference" /> indicating the data type of the array to create. </param>
		/// <param name="size">An expression that indicates the number of indexes of the array to create. </param>
		// Token: 0x060035C0 RID: 13760 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayCreateExpression(CodeTypeReference createType, CodeExpression size)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> class using the specified array data type and initialization expressions.</summary>
		/// <param name="createType">A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type of the array to create. </param>
		/// <param name="initializers">An array of expressions to use to initialize the array. </param>
		// Token: 0x060035C1 RID: 13761 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayCreateExpression(CodeTypeReference createType, CodeExpression[] initializers)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> class using the specified array data type and number of indexes for the array.</summary>
		/// <param name="createType">A <see cref="T:System.CodeDom.CodeTypeReference" /> indicating the data type of the array to create. </param>
		/// <param name="size">The number of indexes of the array to create. </param>
		// Token: 0x060035C2 RID: 13762 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayCreateExpression(CodeTypeReference createType, int size)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> class using the specified array data type name and code expression indicating the number of indexes for the array.</summary>
		/// <param name="createType">The name of the data type of the array to create. </param>
		/// <param name="size">An expression that indicates the number of indexes of the array to create. </param>
		// Token: 0x060035C3 RID: 13763 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayCreateExpression(string createType, CodeExpression size)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> class using the specified array data type name and initializers.</summary>
		/// <param name="createType">The name of the data type of the array to create. </param>
		/// <param name="initializers">An array of expressions to use to initialize the array. </param>
		// Token: 0x060035C4 RID: 13764 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayCreateExpression(string createType, CodeExpression[] initializers)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> class using the specified array data type name and number of indexes for the array.</summary>
		/// <param name="createType">The name of the data type of the array to create. </param>
		/// <param name="size">The number of indexes of the array to create. </param>
		// Token: 0x060035C5 RID: 13765 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayCreateExpression(string createType, int size)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> class using the specified array data type and code expression indicating the number of indexes for the array.</summary>
		/// <param name="createType">The data type of the array to create. </param>
		/// <param name="size">An expression that indicates the number of indexes of the array to create. </param>
		// Token: 0x060035C6 RID: 13766 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayCreateExpression(Type createType, CodeExpression size)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> class using the specified array data type and initializers.</summary>
		/// <param name="createType">The data type of the array to create. </param>
		/// <param name="initializers">An array of expressions to use to initialize the array. </param>
		// Token: 0x060035C7 RID: 13767 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayCreateExpression(Type createType, CodeExpression[] initializers)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeArrayCreateExpression" /> class using the specified array data type and number of indexes for the array.</summary>
		/// <param name="createType">The data type of the array to create. </param>
		/// <param name="size">The number of indexes of the array to create. </param>
		// Token: 0x060035C8 RID: 13768 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeArrayCreateExpression(Type createType, int size)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the type of array to create.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the type of the array.</returns>
		// Token: 0x17000DA5 RID: 3493
		// (get) Token: 0x060035C9 RID: 13769 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060035CA RID: 13770 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference CreateType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the initializers with which to initialize the array.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpressionCollection" /> that indicates the initialization values.</returns>
		// Token: 0x17000DA6 RID: 3494
		// (get) Token: 0x060035CB RID: 13771 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeExpressionCollection Initializers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the number of indexes in the array.</summary>
		/// <returns>The number of indexes in the array.</returns>
		// Token: 0x17000DA7 RID: 3495
		// (get) Token: 0x060035CC RID: 13772 RVA: 0x000A5B94 File Offset: 0x000A3D94
		// (set) Token: 0x060035CD RID: 13773 RVA: 0x000092E2 File Offset: 0x000074E2
		public int Size
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the expression that indicates the size of the array.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the size of the array.</returns>
		// Token: 0x17000DA8 RID: 3496
		// (get) Token: 0x060035CE RID: 13774 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060035CF RID: 13775 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression SizeExpression
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
