﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a parameter declaration for a method, property, or constructor.</summary>
	// Token: 0x02000612 RID: 1554
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeParameterDeclarationExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeParameterDeclarationExpression" /> class.</summary>
		// Token: 0x060031CE RID: 12750 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeParameterDeclarationExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeParameterDeclarationExpression" /> class using the specified parameter type and name.</summary>
		/// <param name="type">An object that indicates the type of the parameter to declare. </param>
		/// <param name="name">The name of the parameter to declare. </param>
		// Token: 0x060031CF RID: 12751 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeParameterDeclarationExpression(CodeTypeReference type, string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeParameterDeclarationExpression" /> class using the specified parameter type and name.</summary>
		/// <param name="type">The type of the parameter to declare. </param>
		/// <param name="name">The name of the parameter to declare. </param>
		// Token: 0x060031D0 RID: 12752 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeParameterDeclarationExpression(string type, string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeParameterDeclarationExpression" /> class using the specified parameter type and name.</summary>
		/// <param name="type">The type of the parameter to declare. </param>
		/// <param name="name">The name of the parameter to declare. </param>
		// Token: 0x060031D1 RID: 12753 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeParameterDeclarationExpression(Type type, string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the custom attributes for the parameter declaration.</summary>
		/// <returns>An object that indicates the custom attributes.</returns>
		// Token: 0x17000C62 RID: 3170
		// (get) Token: 0x060031D2 RID: 12754 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060031D3 RID: 12755 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeDeclarationCollection CustomAttributes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the direction of the field.</summary>
		/// <returns>An object that indicates the direction of the field.</returns>
		// Token: 0x17000C63 RID: 3171
		// (get) Token: 0x060031D4 RID: 12756 RVA: 0x000A4598 File Offset: 0x000A2798
		// (set) Token: 0x060031D5 RID: 12757 RVA: 0x000092E2 File Offset: 0x000074E2
		public FieldDirection Direction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return FieldDirection.In;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the parameter.</summary>
		/// <returns>The name of the parameter.</returns>
		// Token: 0x17000C64 RID: 3172
		// (get) Token: 0x060031D6 RID: 12758 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060031D7 RID: 12759 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the type of the parameter.</summary>
		/// <returns>The type of the parameter.</returns>
		// Token: 0x17000C65 RID: 3173
		// (get) Token: 0x060031D8 RID: 12760 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060031D9 RID: 12761 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
