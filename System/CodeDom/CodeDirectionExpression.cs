﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents an expression used as a method invoke parameter along with a reference direction indicator.</summary>
	// Token: 0x020006BD RID: 1725
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeDirectionExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDirectionExpression" /> class.</summary>
		// Token: 0x06003640 RID: 13888 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDirectionExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeDirectionExpression" /> class using the specified field direction and expression.</summary>
		/// <param name="direction">A <see cref="T:System.CodeDom.FieldDirection" /> that indicates the field direction of the expression. </param>
		/// <param name="expression">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the code expression to represent. </param>
		// Token: 0x06003641 RID: 13889 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeDirectionExpression(FieldDirection direction, CodeExpression expression)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the field direction for this direction expression.</summary>
		/// <returns>A <see cref="T:System.CodeDom.FieldDirection" /> that indicates the field direction for this direction expression.</returns>
		// Token: 0x17000DC9 RID: 3529
		// (get) Token: 0x06003642 RID: 13890 RVA: 0x000A5C90 File Offset: 0x000A3E90
		// (set) Token: 0x06003643 RID: 13891 RVA: 0x000092E2 File Offset: 0x000074E2
		public FieldDirection Direction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return FieldDirection.In;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the code expression to represent.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the expression to represent.</returns>
		// Token: 0x17000DCA RID: 3530
		// (get) Token: 0x06003644 RID: 13892 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003645 RID: 13893 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression Expression
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
