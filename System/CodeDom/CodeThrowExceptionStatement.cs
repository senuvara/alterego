﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a statement that throws an exception.</summary>
	// Token: 0x020006D7 RID: 1751
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeThrowExceptionStatement : CodeStatement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeThrowExceptionStatement" /> class.</summary>
		// Token: 0x060036C9 RID: 14025 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeThrowExceptionStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeThrowExceptionStatement" /> class with the specified exception type instance.</summary>
		/// <param name="toThrow">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the exception to throw. </param>
		// Token: 0x060036CA RID: 14026 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeThrowExceptionStatement(CodeExpression toThrow)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the exception to throw.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> representing an instance of the exception to throw.</returns>
		// Token: 0x17000DF8 RID: 3576
		// (get) Token: 0x060036CB RID: 14027 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036CC RID: 14028 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression ToThrow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
