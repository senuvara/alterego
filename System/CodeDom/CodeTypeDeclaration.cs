﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a type declaration for a class, structure, interface, or enumeration.</summary>
	// Token: 0x020005D4 RID: 1492
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeTypeDeclaration : CodeTypeMember
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeDeclaration" /> class.</summary>
		// Token: 0x06002F75 RID: 12149 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeDeclaration()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeDeclaration" /> class with the specified name.</summary>
		/// <param name="name">The name for the new type. </param>
		// Token: 0x06002F76 RID: 12150 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeDeclaration(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the base types of the type.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReferenceCollection" /> object that indicates the base types of the type.</returns>
		// Token: 0x17000BC7 RID: 3015
		// (get) Token: 0x06002F77 RID: 12151 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeReferenceCollection BaseTypes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a value indicating whether the type is a class or reference type.</summary>
		/// <returns>
		///     <see langword="true" /> if the type is a class or reference type; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000BC8 RID: 3016
		// (get) Token: 0x06002F78 RID: 12152 RVA: 0x000A3AA8 File Offset: 0x000A1CA8
		// (set) Token: 0x06002F79 RID: 12153 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool IsClass
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the type is an enumeration.</summary>
		/// <returns>
		///     <see langword="true" /> if the type is an enumeration; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000BC9 RID: 3017
		// (get) Token: 0x06002F7A RID: 12154 RVA: 0x000A3AC4 File Offset: 0x000A1CC4
		// (set) Token: 0x06002F7B RID: 12155 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool IsEnum
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the type is an interface.</summary>
		/// <returns>
		///     <see langword="true" /> if the type is an interface; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000BCA RID: 3018
		// (get) Token: 0x06002F7C RID: 12156 RVA: 0x000A3AE0 File Offset: 0x000A1CE0
		// (set) Token: 0x06002F7D RID: 12157 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool IsInterface
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the type declaration is complete or partial.</summary>
		/// <returns>
		///     <see langword="true" /> if the class or structure declaration is a partial representation of the implementation; <see langword="false" /> if the declaration is a complete implementation of the class or structure. The default is <see langword="false" />.</returns>
		// Token: 0x17000BCB RID: 3019
		// (get) Token: 0x06002F7E RID: 12158 RVA: 0x000A3AFC File Offset: 0x000A1CFC
		// (set) Token: 0x06002F7F RID: 12159 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool IsPartial
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the type is a value type (struct).</summary>
		/// <returns>
		///     <see langword="true" /> if the type is a value type; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000BCC RID: 3020
		// (get) Token: 0x06002F80 RID: 12160 RVA: 0x000A3B18 File Offset: 0x000A1D18
		// (set) Token: 0x06002F81 RID: 12161 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool IsStruct
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of class members for the represented type.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeMemberCollection" /> object that indicates the class members.</returns>
		// Token: 0x17000BCD RID: 3021
		// (get) Token: 0x06002F82 RID: 12162 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeMemberCollection Members
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the attributes of the type.</summary>
		/// <returns>A <see cref="T:System.Reflection.TypeAttributes" /> object that indicates the attributes of the type.</returns>
		// Token: 0x17000BCE RID: 3022
		// (get) Token: 0x06002F83 RID: 12163 RVA: 0x000A3B34 File Offset: 0x000A1D34
		// (set) Token: 0x06002F84 RID: 12164 RVA: 0x000092E2 File Offset: 0x000074E2
		public TypeAttributes TypeAttributes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return TypeAttributes.NotPublic;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the type parameters for the type declaration.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeParameterCollection" /> that contains the type parameters for the type declaration.</returns>
		// Token: 0x17000BCF RID: 3023
		// (get) Token: 0x06002F85 RID: 12165 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeParameterCollection TypeParameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Occurs when the <see cref="P:System.CodeDom.CodeTypeDeclaration.BaseTypes" /> collection is accessed for the first time.</summary>
		// Token: 0x14000052 RID: 82
		// (add) Token: 0x06002F86 RID: 12166 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x06002F87 RID: 12167 RVA: 0x000092E2 File Offset: 0x000074E2
		public event EventHandler PopulateBaseTypes
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the <see cref="P:System.CodeDom.CodeTypeDeclaration.Members" /> collection is accessed for the first time.</summary>
		// Token: 0x14000053 RID: 83
		// (add) Token: 0x06002F88 RID: 12168 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x06002F89 RID: 12169 RVA: 0x000092E2 File Offset: 0x000074E2
		public event EventHandler PopulateMembers
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
