﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents an expression cast to a data type or interface.</summary>
	// Token: 0x020006B4 RID: 1716
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeCastExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCastExpression" /> class.</summary>
		// Token: 0x060035FE RID: 13822 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCastExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCastExpression" /> class using the specified destination type and expression.</summary>
		/// <param name="targetType">A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the destination type of the cast. </param>
		/// <param name="expression">The <see cref="T:System.CodeDom.CodeExpression" /> to cast. </param>
		// Token: 0x060035FF RID: 13823 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCastExpression(CodeTypeReference targetType, CodeExpression expression)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCastExpression" /> class using the specified destination type and expression.</summary>
		/// <param name="targetType">The name of the destination type of the cast. </param>
		/// <param name="expression">The <see cref="T:System.CodeDom.CodeExpression" /> to cast. </param>
		// Token: 0x06003600 RID: 13824 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCastExpression(string targetType, CodeExpression expression)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCastExpression" /> class using the specified destination type and expression.</summary>
		/// <param name="targetType">The destination data type of the cast. </param>
		/// <param name="expression">The <see cref="T:System.CodeDom.CodeExpression" /> to cast. </param>
		// Token: 0x06003601 RID: 13825 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCastExpression(Type targetType, CodeExpression expression)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the expression to cast.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the code to cast.</returns>
		// Token: 0x17000DB5 RID: 3509
		// (get) Token: 0x06003602 RID: 13826 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003603 RID: 13827 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression Expression
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the destination type of the cast.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the destination type to cast to.</returns>
		// Token: 0x17000DB6 RID: 3510
		// (get) Token: 0x06003604 RID: 13828 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003605 RID: 13829 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference TargetType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
