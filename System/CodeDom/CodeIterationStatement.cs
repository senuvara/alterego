﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a <see langword="for" /> statement, or a loop through a block of statements, using a test expression as a condition for continuing to loop.</summary>
	// Token: 0x020006C3 RID: 1731
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeIterationStatement : CodeStatement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeIterationStatement" /> class.</summary>
		// Token: 0x0600365A RID: 13914 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeIterationStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeIterationStatement" /> class using the specified parameters.</summary>
		/// <param name="initStatement">A <see cref="T:System.CodeDom.CodeStatement" /> containing the loop initialization statement. </param>
		/// <param name="testExpression">A <see cref="T:System.CodeDom.CodeExpression" /> containing the expression to test for exit condition. </param>
		/// <param name="incrementStatement">A <see cref="T:System.CodeDom.CodeStatement" /> containing the per-cycle increment statement. </param>
		/// <param name="statements">An array of type <see cref="T:System.CodeDom.CodeStatement" /> containing the statements within the loop. </param>
		// Token: 0x0600365B RID: 13915 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeIterationStatement(CodeStatement initStatement, CodeExpression testExpression, CodeStatement incrementStatement, CodeStatement[] statements)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the statement that is called after each loop cycle.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeStatement" /> that indicates the per cycle increment statement.</returns>
		// Token: 0x17000DD1 RID: 3537
		// (get) Token: 0x0600365C RID: 13916 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600365D RID: 13917 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeStatement IncrementStatement
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the loop initialization statement.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeStatement" /> that indicates the loop initialization statement.</returns>
		// Token: 0x17000DD2 RID: 3538
		// (get) Token: 0x0600365E RID: 13918 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600365F RID: 13919 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeStatement InitStatement
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of statements to be executed within the loop.</summary>
		/// <returns>An array of type <see cref="T:System.CodeDom.CodeStatement" /> that indicates the statements within the loop.</returns>
		// Token: 0x17000DD3 RID: 3539
		// (get) Token: 0x06003660 RID: 13920 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeStatementCollection Statements
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the expression to test as the condition that continues the loop.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the expression to test.</returns>
		// Token: 0x17000DD4 RID: 3540
		// (get) Token: 0x06003661 RID: 13921 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003662 RID: 13922 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression TestExpression
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
