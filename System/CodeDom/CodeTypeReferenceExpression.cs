﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to a data type.</summary>
	// Token: 0x020006DC RID: 1756
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeTypeReferenceExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReferenceExpression" /> class.</summary>
		// Token: 0x060036DF RID: 14047 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReferenceExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReferenceExpression" /> class using the specified type.</summary>
		/// <param name="type">A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type to reference. </param>
		// Token: 0x060036E0 RID: 14048 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReferenceExpression(CodeTypeReference type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReferenceExpression" /> class using the specified data type name.</summary>
		/// <param name="type">The name of the data type to reference. </param>
		// Token: 0x060036E1 RID: 14049 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReferenceExpression(string type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeReferenceExpression" /> class using the specified data type.</summary>
		/// <param name="type">An instance of the data type to reference. </param>
		// Token: 0x060036E2 RID: 14050 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReferenceExpression(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the data type to reference.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type to reference.</returns>
		// Token: 0x17000DFF RID: 3583
		// (get) Token: 0x060036E3 RID: 14051 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036E4 RID: 14052 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
