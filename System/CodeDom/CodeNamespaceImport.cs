﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a namespace import directive that indicates a namespace to use.</summary>
	// Token: 0x020005EE RID: 1518
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeNamespaceImport : CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeNamespaceImport" /> class.</summary>
		// Token: 0x06003072 RID: 12402 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeNamespaceImport()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeNamespaceImport" /> class using the specified namespace to import.</summary>
		/// <param name="nameSpace">The name of the namespace to import. </param>
		// Token: 0x06003073 RID: 12403 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeNamespaceImport(string nameSpace)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the line and file the statement occurs on.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeLinePragma" /> that indicates the context of the statement.</returns>
		// Token: 0x17000C01 RID: 3073
		// (get) Token: 0x06003074 RID: 12404 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003075 RID: 12405 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeLinePragma LinePragma
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the namespace to import.</summary>
		/// <returns>The name of the namespace to import.</returns>
		// Token: 0x17000C02 RID: 3074
		// (get) Token: 0x06003076 RID: 12406 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003077 RID: 12407 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Namespace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
