﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Provides a base class for a member of a type. Type members include fields, methods, properties, constructors and nested types.</summary>
	// Token: 0x020005D5 RID: 1493
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeTypeMember : CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeMember" /> class. </summary>
		// Token: 0x06002F8A RID: 12170 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeMember()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the attributes of the member.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.CodeDom.MemberAttributes" /> values used to indicate the attributes of the member. The default value is <see cref="F:System.CodeDom.MemberAttributes.Private" /> | <see cref="F:System.CodeDom.MemberAttributes.Final" />. </returns>
		// Token: 0x17000BD0 RID: 3024
		// (get) Token: 0x06002F8B RID: 12171 RVA: 0x000A3B50 File Offset: 0x000A1D50
		// (set) Token: 0x06002F8C RID: 12172 RVA: 0x000092E2 File Offset: 0x000074E2
		public MemberAttributes Attributes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (MemberAttributes)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of comments for the type member.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeCommentStatementCollection" /> that indicates the comments for the member.</returns>
		// Token: 0x17000BD1 RID: 3025
		// (get) Token: 0x06002F8D RID: 12173 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeCommentStatementCollection Comments
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the custom attributes of the member.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeAttributeDeclarationCollection" /> that indicates the custom attributes of the member.</returns>
		// Token: 0x17000BD2 RID: 3026
		// (get) Token: 0x06002F8E RID: 12174 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002F8F RID: 12175 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeDeclarationCollection CustomAttributes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the end directives for the member.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object containing end directives.</returns>
		// Token: 0x17000BD3 RID: 3027
		// (get) Token: 0x06002F90 RID: 12176 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeDirectiveCollection EndDirectives
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the line on which the type member statement occurs.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeLinePragma" /> object that indicates the location of the type member declaration.</returns>
		// Token: 0x17000BD4 RID: 3028
		// (get) Token: 0x06002F91 RID: 12177 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002F92 RID: 12178 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeLinePragma LinePragma
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the member.</summary>
		/// <returns>The name of the member.</returns>
		// Token: 0x17000BD5 RID: 3029
		// (get) Token: 0x06002F93 RID: 12179 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002F94 RID: 12180 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the start directives for the member.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object containing start directives.</returns>
		// Token: 0x17000BD6 RID: 3030
		// (get) Token: 0x06002F95 RID: 12181 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeDirectiveCollection StartDirectives
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
