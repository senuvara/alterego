﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a <see langword="catch" /> exception block of a <see langword="try/catch" /> statement.</summary>
	// Token: 0x020006B5 RID: 1717
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeCatchClause
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCatchClause" /> class.</summary>
		// Token: 0x06003606 RID: 13830 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCatchClause()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCatchClause" /> class using the specified local variable name for the exception.</summary>
		/// <param name="localName">The name of the local variable declared in the catch clause for the exception. This is optional. </param>
		// Token: 0x06003607 RID: 13831 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCatchClause(string localName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCatchClause" /> class using the specified local variable name for the exception and exception type.</summary>
		/// <param name="localName">The name of the local variable declared in the catch clause for the exception. This is optional. </param>
		/// <param name="catchExceptionType">A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the type of exception to catch. </param>
		// Token: 0x06003608 RID: 13832 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCatchClause(string localName, CodeTypeReference catchExceptionType)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCatchClause" /> class using the specified local variable name for the exception, exception type and statement collection.</summary>
		/// <param name="localName">The name of the local variable declared in the catch clause for the exception. This is optional. </param>
		/// <param name="catchExceptionType">A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the type of exception to catch. </param>
		/// <param name="statements">An array of <see cref="T:System.CodeDom.CodeStatement" /> objects that represent the contents of the catch block. </param>
		// Token: 0x06003609 RID: 13833 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCatchClause(string localName, CodeTypeReference catchExceptionType, CodeStatement[] statements)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the type of the exception to handle with the catch block.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the type of the exception to handle.</returns>
		// Token: 0x17000DB7 RID: 3511
		// (get) Token: 0x0600360A RID: 13834 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600360B RID: 13835 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference CatchExceptionType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the variable name of the exception that the <see langword="catch" /> clause handles.</summary>
		/// <returns>The name for the exception variable that the <see langword="catch" /> clause handles.</returns>
		// Token: 0x17000DB8 RID: 3512
		// (get) Token: 0x0600360C RID: 13836 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600360D RID: 13837 RVA: 0x000092E2 File Offset: 0x000074E2
		public string LocalName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the statements within the catch block.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeStatementCollection" /> containing the statements within the catch block.</returns>
		// Token: 0x17000DB9 RID: 3513
		// (get) Token: 0x0600360E RID: 13838 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeStatementCollection Statements
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
