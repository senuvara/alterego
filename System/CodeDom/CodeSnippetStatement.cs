﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a statement using a literal code fragment.</summary>
	// Token: 0x020006D4 RID: 1748
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeSnippetStatement : CodeStatement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeSnippetStatement" /> class.</summary>
		// Token: 0x060036C0 RID: 14016 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeSnippetStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeSnippetStatement" /> class using the specified code fragment.</summary>
		/// <param name="value">The literal code fragment of the statement to represent. </param>
		// Token: 0x060036C1 RID: 14017 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeSnippetStatement(string value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the literal code fragment statement.</summary>
		/// <returns>The literal code fragment statement.</returns>
		// Token: 0x17000DF6 RID: 3574
		// (get) Token: 0x060036C2 RID: 14018 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036C3 RID: 14019 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
