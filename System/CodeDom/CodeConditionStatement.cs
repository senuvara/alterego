﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a conditional branch statement, typically represented as an <see langword="if" /> statement.</summary>
	// Token: 0x020006B8 RID: 1720
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeConditionStatement : CodeStatement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeConditionStatement" /> class.</summary>
		// Token: 0x06003624 RID: 13860 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeConditionStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeConditionStatement" /> class using the specified condition and statements.</summary>
		/// <param name="condition">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the expression to evaluate. </param>
		/// <param name="trueStatements">An array of type <see cref="T:System.CodeDom.CodeStatement" /> containing the statements to execute if the condition is <see langword="true" />. </param>
		// Token: 0x06003625 RID: 13861 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeConditionStatement(CodeExpression condition, CodeStatement[] trueStatements)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeConditionStatement" /> class using the specified condition and statements.</summary>
		/// <param name="condition">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the condition to evaluate. </param>
		/// <param name="trueStatements">An array of type <see cref="T:System.CodeDom.CodeStatement" /> containing the statements to execute if the condition is <see langword="true" />. </param>
		/// <param name="falseStatements">An array of type <see cref="T:System.CodeDom.CodeStatement" /> containing the statements to execute if the condition is <see langword="false" />. </param>
		// Token: 0x06003626 RID: 13862 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeConditionStatement(CodeExpression condition, CodeStatement[] trueStatements, CodeStatement[] falseStatements)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the expression to evaluate <see langword="true" /> or <see langword="false" />.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> to evaluate <see langword="true" /> or <see langword="false" />.</returns>
		// Token: 0x17000DBE RID: 3518
		// (get) Token: 0x06003627 RID: 13863 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003628 RID: 13864 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression Condition
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of statements to execute if the conditional expression evaluates to <see langword="false" />.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeStatementCollection" /> containing the statements to execute if the conditional expression evaluates to <see langword="false" />.</returns>
		// Token: 0x17000DBF RID: 3519
		// (get) Token: 0x06003629 RID: 13865 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeStatementCollection FalseStatements
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of statements to execute if the conditional expression evaluates to <see langword="true" />.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeStatementCollection" /> containing the statements to execute if the conditional expression evaluates to <see langword="true" />.</returns>
		// Token: 0x17000DC0 RID: 3520
		// (get) Token: 0x0600362A RID: 13866 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeStatementCollection TrueStatements
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
