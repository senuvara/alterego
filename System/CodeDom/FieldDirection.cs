﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	/// <summary>Defines identifiers used to indicate the direction of parameter and argument declarations.</summary>
	// Token: 0x02000603 RID: 1539
	[ComVisible(true)]
	[Serializable]
	public enum FieldDirection
	{
		/// <summary>An incoming field.</summary>
		// Token: 0x040024BD RID: 9405
		In,
		/// <summary>An outgoing field.</summary>
		// Token: 0x040024BE RID: 9406
		Out,
		/// <summary>A field by reference.</summary>
		// Token: 0x040024BF RID: 9407
		Ref
	}
}
