﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a reference to the value of a property.</summary>
	// Token: 0x020006CD RID: 1741
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodePropertyReferenceExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodePropertyReferenceExpression" /> class.</summary>
		// Token: 0x060036A2 RID: 13986 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodePropertyReferenceExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodePropertyReferenceExpression" /> class using the specified target object and property name.</summary>
		/// <param name="targetObject">A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the object that contains the property to reference. </param>
		/// <param name="propertyName">The name of the property to reference. </param>
		// Token: 0x060036A3 RID: 13987 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodePropertyReferenceExpression(CodeExpression targetObject, string propertyName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the property to reference.</summary>
		/// <returns>The name of the property to reference.</returns>
		// Token: 0x17000DED RID: 3565
		// (get) Token: 0x060036A4 RID: 13988 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036A5 RID: 13989 RVA: 0x000092E2 File Offset: 0x000074E2
		public string PropertyName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the object that contains the property to reference.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeExpression" /> that indicates the object that contains the property to reference.</returns>
		// Token: 0x17000DEE RID: 3566
		// (get) Token: 0x060036A6 RID: 13990 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036A7 RID: 13991 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeExpression TargetObject
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
