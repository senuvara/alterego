﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a collection of <see cref="T:System.CodeDom.CodeAttributeArgument" /> objects.</summary>
	// Token: 0x020005E7 RID: 1511
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeAttributeArgumentCollection : CollectionBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeArgumentCollection" /> class.</summary>
		// Token: 0x06003012 RID: 12306 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeArgumentCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeArgumentCollection" /> class containing the elements of the specified source collection.</summary>
		/// <param name="value">A <see cref="T:System.CodeDom.CodeAttributeArgumentCollection" /> with which to initialize the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06003013 RID: 12307 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeArgumentCollection(CodeAttributeArgumentCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeAttributeArgumentCollection" /> class containing the specified array of <see cref="T:System.CodeDom.CodeAttributeArgument" /> objects.</summary>
		/// <param name="value">An array of <see cref="T:System.CodeDom.CodeAttributeArgument" /> objects with which to initialize the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">One or more objects in the array are <see langword="null" />.</exception>
		// Token: 0x06003014 RID: 12308 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeAttributeArgumentCollection(CodeAttributeArgument[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the <see cref="T:System.CodeDom.CodeAttributeArgument" /> object at the specified index in the collection.</summary>
		/// <param name="index">The index of the collection to access. </param>
		/// <returns>A <see cref="T:System.CodeDom.CodeAttributeArgument" /> at each valid index.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="index" /> parameter is outside the valid range of indexes for the collection. </exception>
		// Token: 0x17000BF2 RID: 3058
		public CodeAttributeArgument this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds the specified <see cref="T:System.CodeDom.CodeAttributeArgument" /> object to the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeAttributeArgument" /> object to add. </param>
		/// <returns>The index at which the new element was inserted.</returns>
		// Token: 0x06003017 RID: 12311 RVA: 0x000A3D48 File Offset: 0x000A1F48
		public int Add(CodeAttributeArgument value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Copies the contents of another <see cref="T:System.CodeDom.CodeAttributeArgumentCollection" /> object to the end of the collection.</summary>
		/// <param name="value">A <see cref="T:System.CodeDom.CodeAttributeArgumentCollection" /> that contains the objects to add to the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06003018 RID: 12312 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(CodeAttributeArgumentCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies the elements of the specified <see cref="T:System.CodeDom.CodeAttributeArgument" /> array to the end of the collection.</summary>
		/// <param name="value">An array of type <see cref="T:System.CodeDom.CodeAttributeArgument" /> that contains the objects to add to the collection. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06003019 RID: 12313 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(CodeAttributeArgument[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates whether the collection contains the specified <see cref="T:System.CodeDom.CodeAttributeArgument" /> object.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeAttributeArgument" /> object to locate in the collection. </param>
		/// <returns>
		///     <see langword="true" /> if the collection contains the specified object; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600301A RID: 12314 RVA: 0x000A3D64 File Offset: 0x000A1F64
		public bool Contains(CodeAttributeArgument value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the collection objects to a one-dimensional <see cref="T:System.Array" /> instance beginning at the specified index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the values copied from the collection. </param>
		/// <param name="index">The index of the array at which to begin inserting. </param>
		/// <exception cref="T:System.ArgumentException">The destination array is multidimensional.-or- The number of elements in the <see cref="T:System.CodeDom.CodeAttributeArgumentCollection" /> is greater than the available space between the index of the target array specified by the <paramref name="index" /> parameter and the end of the target array. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="array" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="index" /> parameter is less than the target array's minimum index. </exception>
		// Token: 0x0600301B RID: 12315 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(CodeAttributeArgument[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the index of the specified <see cref="T:System.CodeDom.CodeAttributeArgument" /> object in the collection, if it exists in the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeAttributeArgument" /> object to locate in the collection. </param>
		/// <returns>The index of the specified object, if found, in the collection; otherwise, -1.</returns>
		// Token: 0x0600301C RID: 12316 RVA: 0x000A3D80 File Offset: 0x000A1F80
		public int IndexOf(CodeAttributeArgument value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Inserts the specified <see cref="T:System.CodeDom.CodeAttributeArgument" /> object into the collection at the specified index.</summary>
		/// <param name="index">The zero-based index where the specified object should be inserted. </param>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeAttributeArgument" /> object to insert. </param>
		// Token: 0x0600301D RID: 12317 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Insert(int index, CodeAttributeArgument value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the specified <see cref="T:System.CodeDom.CodeAttributeArgument" /> object from the collection.</summary>
		/// <param name="value">The <see cref="T:System.CodeDom.CodeAttributeArgument" /> object to remove from the collection. </param>
		/// <exception cref="T:System.ArgumentException">The specified object is not found in the collection. </exception>
		// Token: 0x0600301E RID: 12318 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(CodeAttributeArgument value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
