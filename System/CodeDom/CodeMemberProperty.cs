﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a declaration for a property of a type.</summary>
	// Token: 0x020006C7 RID: 1735
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeMemberProperty : CodeTypeMember
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeMemberProperty" /> class.</summary>
		// Token: 0x06003678 RID: 13944 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeMemberProperty()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the collection of <see langword="get" /> statements for the property.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeStatementCollection" /> that contains the <see langword="get" /> statements for the member property.</returns>
		// Token: 0x17000DDC RID: 3548
		// (get) Token: 0x06003679 RID: 13945 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeStatementCollection GetStatements
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a value indicating whether the property has a <see langword="get" /> method accessor.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="Count" /> property of the <see cref="P:System.CodeDom.CodeMemberProperty.GetStatements" /> collection is non-zero, or if the value of this property has been set to <see langword="true" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000DDD RID: 3549
		// (get) Token: 0x0600367A RID: 13946 RVA: 0x000A5CAC File Offset: 0x000A3EAC
		// (set) Token: 0x0600367B RID: 13947 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool HasGet
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the property has a <see langword="set" /> method accessor.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="P:System.Collections.CollectionBase.Count" /> property of the <see cref="P:System.CodeDom.CodeMemberProperty.SetStatements" /> collection is non-zero; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000DDE RID: 3550
		// (get) Token: 0x0600367C RID: 13948 RVA: 0x000A5CC8 File Offset: 0x000A3EC8
		// (set) Token: 0x0600367D RID: 13949 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool HasSet
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the data types of any interfaces that the property implements.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReferenceCollection" /> that indicates the data types the property implements.</returns>
		// Token: 0x17000DDF RID: 3551
		// (get) Token: 0x0600367E RID: 13950 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeTypeReferenceCollection ImplementationTypes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of declaration expressions for the property.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeParameterDeclarationExpressionCollection" /> that indicates the declaration expressions for the property.</returns>
		// Token: 0x17000DE0 RID: 3552
		// (get) Token: 0x0600367F RID: 13951 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeParameterDeclarationExpressionCollection Parameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the data type of the interface, if any, this property, if private, implements.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type of the interface, if any, the property, if private, implements.</returns>
		// Token: 0x17000DE1 RID: 3553
		// (get) Token: 0x06003680 RID: 13952 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003681 RID: 13953 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference PrivateImplementationType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of <see langword="set" /> statements for the property.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeStatementCollection" /> that contains the <see langword="set" /> statements for the member property.</returns>
		// Token: 0x17000DE2 RID: 3554
		// (get) Token: 0x06003682 RID: 13954 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeStatementCollection SetStatements
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the data type of the property.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that indicates the data type of the property.</returns>
		// Token: 0x17000DE3 RID: 3555
		// (get) Token: 0x06003683 RID: 13955 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003684 RID: 13956 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeReference Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
