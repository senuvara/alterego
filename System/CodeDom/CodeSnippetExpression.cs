﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a literal expression.</summary>
	// Token: 0x020006D3 RID: 1747
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeSnippetExpression : CodeExpression
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeSnippetExpression" /> class.</summary>
		// Token: 0x060036BC RID: 14012 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeSnippetExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeSnippetExpression" /> class using the specified literal expression.</summary>
		/// <param name="value">The literal expression to represent. </param>
		// Token: 0x060036BD RID: 14013 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeSnippetExpression(string value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the literal string of code.</summary>
		/// <returns>The literal string.</returns>
		// Token: 0x17000DF5 RID: 3573
		// (get) Token: 0x060036BE RID: 14014 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036BF RID: 14015 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
