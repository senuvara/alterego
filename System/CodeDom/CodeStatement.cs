﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents the <see langword="abstract" /> base class from which all code statements derive.</summary>
	// Token: 0x020005DA RID: 1498
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeStatement : CodeObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeStatement" /> class. </summary>
		// Token: 0x06002FAB RID: 12203 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object that contains end directives.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object containing end directives.</returns>
		// Token: 0x17000BDA RID: 3034
		// (get) Token: 0x06002FAC RID: 12204 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeDirectiveCollection EndDirectives
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the line on which the code statement occurs. </summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeLinePragma" /> object that indicates the context of the code statement.</returns>
		// Token: 0x17000BDB RID: 3035
		// (get) Token: 0x06002FAD RID: 12205 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002FAE RID: 12206 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeLinePragma LinePragma
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object that contains start directives.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeDirectiveCollection" /> object containing start directives.</returns>
		// Token: 0x17000BDC RID: 3036
		// (get) Token: 0x06002FAF RID: 12207 RVA: 0x00043C3C File Offset: 0x00041E3C
		public CodeDirectiveCollection StartDirectives
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
