﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a statement consisting of a single comment.</summary>
	// Token: 0x020005D9 RID: 1497
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeCommentStatement : CodeStatement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCommentStatement" /> class.</summary>
		// Token: 0x06002FA5 RID: 12197 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCommentStatement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCommentStatement" /> class using the specified comment.</summary>
		/// <param name="comment">A <see cref="T:System.CodeDom.CodeComment" /> that indicates the comment. </param>
		// Token: 0x06002FA6 RID: 12198 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCommentStatement(CodeComment comment)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCommentStatement" /> class using the specified text as contents.</summary>
		/// <param name="text">The contents of the comment. </param>
		// Token: 0x06002FA7 RID: 12199 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCommentStatement(string text)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeCommentStatement" /> class using the specified text and documentation comment flag.</summary>
		/// <param name="text">The contents of the comment. </param>
		/// <param name="docComment">
		///       <see langword="true" /> if the comment is a documentation comment; otherwise, <see langword="false" />. </param>
		// Token: 0x06002FA8 RID: 12200 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeCommentStatement(string text, bool docComment)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the contents of the comment.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeComment" /> that indicates the comment.</returns>
		// Token: 0x17000BD9 RID: 3033
		// (get) Token: 0x06002FA9 RID: 12201 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06002FAA RID: 12202 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeComment Comment
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
