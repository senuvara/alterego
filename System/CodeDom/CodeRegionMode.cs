﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	/// <summary>Specifies the start or end of a code region.</summary>
	// Token: 0x020006D0 RID: 1744
	[ComVisible(true)]
	[Serializable]
	public enum CodeRegionMode
	{
		/// <summary>End of the region.</summary>
		// Token: 0x04002539 RID: 9529
		End = 2,
		/// <summary>Not used.</summary>
		// Token: 0x0400253A RID: 9530
		None = 0,
		/// <summary>Start of the region.</summary>
		// Token: 0x0400253B RID: 9531
		Start
	}
}
