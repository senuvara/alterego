﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a static constructor for a class.</summary>
	// Token: 0x020006D9 RID: 1753
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeTypeConstructor : CodeMemberMethod
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeTypeConstructor" /> class.</summary>
		// Token: 0x060036D3 RID: 14035 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeTypeConstructor()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
