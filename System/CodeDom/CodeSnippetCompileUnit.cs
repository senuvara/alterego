﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.CodeDom
{
	/// <summary>Represents a literal code fragment that can be compiled.</summary>
	// Token: 0x020006D2 RID: 1746
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeSnippetCompileUnit : CodeCompileUnit
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeSnippetCompileUnit" /> class. </summary>
		// Token: 0x060036B6 RID: 14006 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeSnippetCompileUnit()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CodeDom.CodeSnippetCompileUnit" /> class.</summary>
		/// <param name="value">The literal code fragment to represent. </param>
		// Token: 0x060036B7 RID: 14007 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeSnippetCompileUnit(string value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the line and file information about where the code is located in a source code document.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeLinePragma" /> that indicates the position of the code fragment.</returns>
		// Token: 0x17000DF3 RID: 3571
		// (get) Token: 0x060036B8 RID: 14008 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036B9 RID: 14009 RVA: 0x000092E2 File Offset: 0x000074E2
		public CodeLinePragma LinePragma
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the literal code fragment to represent.</summary>
		/// <returns>The literal code fragment.</returns>
		// Token: 0x17000DF4 RID: 3572
		// (get) Token: 0x060036BA RID: 14010 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060036BB RID: 14011 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
