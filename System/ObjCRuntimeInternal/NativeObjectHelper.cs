﻿using System;

namespace ObjCRuntimeInternal
{
	// Token: 0x02000085 RID: 133
	internal static class NativeObjectHelper
	{
		// Token: 0x060002E6 RID: 742 RVA: 0x00008F92 File Offset: 0x00007192
		public static IntPtr GetHandle(this INativeObject self)
		{
			if (self != null)
			{
				return self.Handle;
			}
			return IntPtr.Zero;
		}
	}
}
