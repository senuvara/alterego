﻿using System;

namespace ObjCRuntimeInternal
{
	// Token: 0x02000084 RID: 132
	internal interface INativeObject
	{
		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060002E5 RID: 741
		IntPtr Handle { get; }
	}
}
