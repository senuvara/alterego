﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x020000B8 RID: 184
	internal static class Platform
	{
		// Token: 0x06000446 RID: 1094
		[DllImport("libc")]
		private static extern int uname(IntPtr buf);

		// Token: 0x06000447 RID: 1095 RVA: 0x00015728 File Offset: 0x00013928
		private static void CheckOS()
		{
			if (Environment.OSVersion.Platform != PlatformID.Unix)
			{
				Platform.checkedOS = true;
				return;
			}
			IntPtr intPtr = Marshal.AllocHGlobal(8192);
			try
			{
				if (Platform.uname(intPtr) == 0)
				{
					string a = Marshal.PtrToStringAnsi(intPtr);
					if (!(a == "Darwin"))
					{
						if (a == "FreeBSD")
						{
							Platform.isFreeBSD = true;
						}
					}
					else
					{
						Platform.isMacOS = true;
					}
				}
			}
			finally
			{
				Marshal.FreeHGlobal(intPtr);
				Platform.checkedOS = true;
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x06000448 RID: 1096 RVA: 0x000157AC File Offset: 0x000139AC
		public static bool IsMacOS
		{
			get
			{
				if (!Platform.checkedOS)
				{
					try
					{
						Platform.CheckOS();
					}
					catch (DllNotFoundException)
					{
						Platform.isMacOS = false;
					}
				}
				return Platform.isMacOS;
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x06000449 RID: 1097 RVA: 0x000157E8 File Offset: 0x000139E8
		public static bool IsFreeBSD
		{
			get
			{
				if (!Platform.checkedOS)
				{
					Platform.CheckOS();
				}
				return Platform.isFreeBSD;
			}
		}

		// Token: 0x0400096D RID: 2413
		private static bool checkedOS;

		// Token: 0x0400096E RID: 2414
		private static bool isMacOS;

		// Token: 0x0400096F RID: 2415
		private static bool isFreeBSD;
	}
}
