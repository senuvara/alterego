﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	/// <summary>Specifies the visibility a property has to the design-time serializer.</summary>
	// Token: 0x0200015D RID: 349
	[ComVisible(true)]
	public enum DesignerSerializationVisibility
	{
		/// <summary>The code generator does not produce code for the object.</summary>
		// Token: 0x04000CBF RID: 3263
		Hidden,
		/// <summary>The code generator produces code for the object.</summary>
		// Token: 0x04000CC0 RID: 3264
		Visible,
		/// <summary>The code generator produces code for the contents of the object, rather than for the object itself.</summary>
		// Token: 0x04000CC1 RID: 3265
		Content
	}
}
