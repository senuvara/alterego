﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Represents the method that will handle the <see cref="E:System.ComponentModel.IBindingList.ListChanged" /> event of the <see cref="T:System.ComponentModel.IBindingList" /> class.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">A <see cref="T:System.ComponentModel.ListChangedEventArgs" /> that contains the event data. </param>
	// Token: 0x020001A2 RID: 418
	// (Invoke) Token: 0x06000C5D RID: 3165
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public delegate void ListChangedEventHandler(object sender, ListChangedEventArgs e);
}
