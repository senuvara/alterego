﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a type converter to convert 64-bit unsigned integer objects to and from other representations.</summary>
	// Token: 0x020001EE RID: 494
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class UInt64Converter : BaseNumberConverter
	{
		// Token: 0x17000338 RID: 824
		// (get) Token: 0x06000FA3 RID: 4003 RVA: 0x0003DA21 File Offset: 0x0003BC21
		internal override Type TargetType
		{
			get
			{
				return typeof(ulong);
			}
		}

		// Token: 0x06000FA4 RID: 4004 RVA: 0x0003DA2D File Offset: 0x0003BC2D
		internal override object FromString(string value, int radix)
		{
			return Convert.ToUInt64(value, radix);
		}

		// Token: 0x06000FA5 RID: 4005 RVA: 0x0003DA3B File Offset: 0x0003BC3B
		internal override object FromString(string value, NumberFormatInfo formatInfo)
		{
			return ulong.Parse(value, NumberStyles.Integer, formatInfo);
		}

		// Token: 0x06000FA6 RID: 4006 RVA: 0x0003DA4A File Offset: 0x0003BC4A
		internal override object FromString(string value, CultureInfo culture)
		{
			return ulong.Parse(value, culture);
		}

		// Token: 0x06000FA7 RID: 4007 RVA: 0x0003DA58 File Offset: 0x0003BC58
		internal override string ToString(object value, NumberFormatInfo formatInfo)
		{
			return ((ulong)value).ToString("G", formatInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.UInt64Converter" /> class. </summary>
		// Token: 0x06000FA8 RID: 4008 RVA: 0x0002B691 File Offset: 0x00029891
		public UInt64Converter()
		{
		}
	}
}
