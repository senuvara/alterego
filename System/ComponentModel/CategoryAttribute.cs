﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies the name of the category in which to group the property or event when displayed in a <see cref="T:System.Windows.Forms.PropertyGrid" /> control set to Categorized mode.</summary>
	// Token: 0x02000138 RID: 312
	[AttributeUsage(AttributeTargets.All)]
	public class CategoryAttribute : Attribute
	{
		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Action category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the action category.</returns>
		// Token: 0x17000212 RID: 530
		// (get) Token: 0x060009E9 RID: 2537 RVA: 0x0002B6C2 File Offset: 0x000298C2
		public static CategoryAttribute Action
		{
			get
			{
				if (CategoryAttribute.action == null)
				{
					CategoryAttribute.action = new CategoryAttribute("Action");
				}
				return CategoryAttribute.action;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Appearance category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the appearance category.</returns>
		// Token: 0x17000213 RID: 531
		// (get) Token: 0x060009EA RID: 2538 RVA: 0x0002B6E5 File Offset: 0x000298E5
		public static CategoryAttribute Appearance
		{
			get
			{
				if (CategoryAttribute.appearance == null)
				{
					CategoryAttribute.appearance = new CategoryAttribute("Appearance");
				}
				return CategoryAttribute.appearance;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Asynchronous category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the asynchronous category.</returns>
		// Token: 0x17000214 RID: 532
		// (get) Token: 0x060009EB RID: 2539 RVA: 0x0002B708 File Offset: 0x00029908
		public static CategoryAttribute Asynchronous
		{
			get
			{
				if (CategoryAttribute.asynchronous == null)
				{
					CategoryAttribute.asynchronous = new CategoryAttribute("Asynchronous");
				}
				return CategoryAttribute.asynchronous;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Behavior category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the behavior category.</returns>
		// Token: 0x17000215 RID: 533
		// (get) Token: 0x060009EC RID: 2540 RVA: 0x0002B72B File Offset: 0x0002992B
		public static CategoryAttribute Behavior
		{
			get
			{
				if (CategoryAttribute.behavior == null)
				{
					CategoryAttribute.behavior = new CategoryAttribute("Behavior");
				}
				return CategoryAttribute.behavior;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Data category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the data category.</returns>
		// Token: 0x17000216 RID: 534
		// (get) Token: 0x060009ED RID: 2541 RVA: 0x0002B74E File Offset: 0x0002994E
		public static CategoryAttribute Data
		{
			get
			{
				if (CategoryAttribute.data == null)
				{
					CategoryAttribute.data = new CategoryAttribute("Data");
				}
				return CategoryAttribute.data;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Default category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the default category.</returns>
		// Token: 0x17000217 RID: 535
		// (get) Token: 0x060009EE RID: 2542 RVA: 0x0002B771 File Offset: 0x00029971
		public static CategoryAttribute Default
		{
			get
			{
				if (CategoryAttribute.defAttr == null)
				{
					CategoryAttribute.defAttr = new CategoryAttribute();
				}
				return CategoryAttribute.defAttr;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Design category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the design category.</returns>
		// Token: 0x17000218 RID: 536
		// (get) Token: 0x060009EF RID: 2543 RVA: 0x0002B78F File Offset: 0x0002998F
		public static CategoryAttribute Design
		{
			get
			{
				if (CategoryAttribute.design == null)
				{
					CategoryAttribute.design = new CategoryAttribute("Design");
				}
				return CategoryAttribute.design;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the DragDrop category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the drag-and-drop category.</returns>
		// Token: 0x17000219 RID: 537
		// (get) Token: 0x060009F0 RID: 2544 RVA: 0x0002B7B2 File Offset: 0x000299B2
		public static CategoryAttribute DragDrop
		{
			get
			{
				if (CategoryAttribute.dragDrop == null)
				{
					CategoryAttribute.dragDrop = new CategoryAttribute("DragDrop");
				}
				return CategoryAttribute.dragDrop;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Focus category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the focus category.</returns>
		// Token: 0x1700021A RID: 538
		// (get) Token: 0x060009F1 RID: 2545 RVA: 0x0002B7D5 File Offset: 0x000299D5
		public static CategoryAttribute Focus
		{
			get
			{
				if (CategoryAttribute.focus == null)
				{
					CategoryAttribute.focus = new CategoryAttribute("Focus");
				}
				return CategoryAttribute.focus;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Format category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the format category.</returns>
		// Token: 0x1700021B RID: 539
		// (get) Token: 0x060009F2 RID: 2546 RVA: 0x0002B7F8 File Offset: 0x000299F8
		public static CategoryAttribute Format
		{
			get
			{
				if (CategoryAttribute.format == null)
				{
					CategoryAttribute.format = new CategoryAttribute("Format");
				}
				return CategoryAttribute.format;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Key category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the key category.</returns>
		// Token: 0x1700021C RID: 540
		// (get) Token: 0x060009F3 RID: 2547 RVA: 0x0002B81B File Offset: 0x00029A1B
		public static CategoryAttribute Key
		{
			get
			{
				if (CategoryAttribute.key == null)
				{
					CategoryAttribute.key = new CategoryAttribute("Key");
				}
				return CategoryAttribute.key;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Layout category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the layout category.</returns>
		// Token: 0x1700021D RID: 541
		// (get) Token: 0x060009F4 RID: 2548 RVA: 0x0002B83E File Offset: 0x00029A3E
		public static CategoryAttribute Layout
		{
			get
			{
				if (CategoryAttribute.layout == null)
				{
					CategoryAttribute.layout = new CategoryAttribute("Layout");
				}
				return CategoryAttribute.layout;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the Mouse category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the mouse category.</returns>
		// Token: 0x1700021E RID: 542
		// (get) Token: 0x060009F5 RID: 2549 RVA: 0x0002B861 File Offset: 0x00029A61
		public static CategoryAttribute Mouse
		{
			get
			{
				if (CategoryAttribute.mouse == null)
				{
					CategoryAttribute.mouse = new CategoryAttribute("Mouse");
				}
				return CategoryAttribute.mouse;
			}
		}

		/// <summary>Gets a <see cref="T:System.ComponentModel.CategoryAttribute" /> representing the WindowStyle category.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.CategoryAttribute" /> for the window style category.</returns>
		// Token: 0x1700021F RID: 543
		// (get) Token: 0x060009F6 RID: 2550 RVA: 0x0002B884 File Offset: 0x00029A84
		public static CategoryAttribute WindowStyle
		{
			get
			{
				if (CategoryAttribute.windowStyle == null)
				{
					CategoryAttribute.windowStyle = new CategoryAttribute("WindowStyle");
				}
				return CategoryAttribute.windowStyle;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.CategoryAttribute" /> class using the category name Default.</summary>
		// Token: 0x060009F7 RID: 2551 RVA: 0x0002B8A7 File Offset: 0x00029AA7
		public CategoryAttribute() : this("Default")
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.CategoryAttribute" /> class using the specified category name.</summary>
		/// <param name="category">The name of the category. </param>
		// Token: 0x060009F8 RID: 2552 RVA: 0x0002B8B4 File Offset: 0x00029AB4
		public CategoryAttribute(string category)
		{
			this.categoryValue = category;
			this.localized = false;
		}

		/// <summary>Gets the name of the category for the property or event that this attribute is applied to.</summary>
		/// <returns>The name of the category for the property or event that this attribute is applied to.</returns>
		// Token: 0x17000220 RID: 544
		// (get) Token: 0x060009F9 RID: 2553 RVA: 0x0002B8CC File Offset: 0x00029ACC
		public string Category
		{
			get
			{
				if (!this.localized)
				{
					this.localized = true;
					string localizedString = this.GetLocalizedString(this.categoryValue);
					if (localizedString != null)
					{
						this.categoryValue = localizedString;
					}
				}
				return this.categoryValue;
			}
		}

		/// <summary>Returns whether the value of the given object is equal to the current <see cref="T:System.ComponentModel.CategoryAttribute" />..</summary>
		/// <param name="obj">The object to test the value equality of. </param>
		/// <returns>
		///     <see langword="true" /> if the value of the given object is equal to that of the current; otherwise, <see langword="false" />.</returns>
		// Token: 0x060009FA RID: 2554 RVA: 0x0002B905 File Offset: 0x00029B05
		public override bool Equals(object obj)
		{
			return obj == this || (obj is CategoryAttribute && this.Category.Equals(((CategoryAttribute)obj).Category));
		}

		/// <summary>Returns the hash code for this attribute.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x060009FB RID: 2555 RVA: 0x0002B92D File Offset: 0x00029B2D
		public override int GetHashCode()
		{
			return this.Category.GetHashCode();
		}

		/// <summary>Looks up the localized name of the specified category.</summary>
		/// <param name="value">The identifer for the category to look up. </param>
		/// <returns>The localized name of the category, or <see langword="null" /> if a localized name does not exist.</returns>
		// Token: 0x060009FC RID: 2556 RVA: 0x0002B93C File Offset: 0x00029B3C
		protected virtual string GetLocalizedString(string value)
		{
			uint num = <PrivateImplementationDetails>.ComputeStringHash(value);
			if (num <= 1762750224U)
			{
				if (num <= 723360612U)
				{
					if (num <= 521774151U)
					{
						if (num != 175614239U)
						{
							if (num == 521774151U)
							{
								if (value == "Behavior")
								{
									return "Behavior";
								}
							}
						}
						else if (value == "Action")
						{
							return "Action";
						}
					}
					else if (num != 676498961U)
					{
						if (num == 723360612U)
						{
							if (value == "Mouse")
							{
								return "Mouse";
							}
						}
					}
					else if (value == "Scale")
					{
						return "Scale";
					}
				}
				else if (num <= 1041509726U)
				{
					if (num != 822184863U)
					{
						if (num == 1041509726U)
						{
							if (value == "Text")
							{
								return "Text";
							}
						}
					}
					else if (value == "Appearance")
					{
						return "Appearance";
					}
				}
				else if (num != 1062369733U)
				{
					if (num == 1762750224U)
					{
						if (value == "DDE")
						{
							return "DDE";
						}
					}
				}
				else if (value == "Data")
				{
					return "Data";
				}
			}
			else if (num <= 3159863731U)
			{
				if (num <= 2368288673U)
				{
					if (num != 1779622119U)
					{
						if (num == 2368288673U)
						{
							if (value == "List")
							{
								return "List";
							}
						}
					}
					else if (value == "Config")
					{
						return "Configurations";
					}
				}
				else if (num != 2809814704U)
				{
					if (num == 3159863731U)
					{
						if (value == "Focus")
						{
							return "Focus";
						}
					}
				}
				else if (value == "Font")
				{
					return "Font";
				}
			}
			else if (num <= 3799987242U)
			{
				if (num != 3441084684U)
				{
					if (num == 3799987242U)
					{
						if (value == "Position")
						{
							return "Position";
						}
					}
				}
				else if (value == "Key")
				{
					return "Key";
				}
			}
			else if (num != 3901555439U)
			{
				if (num == 4152902175U)
				{
					if (value == "Layout")
					{
						return "Layout";
					}
				}
			}
			else if (value == "Design")
			{
				return "Design";
			}
			return value;
		}

		/// <summary>Determines if this attribute is the default.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute is the default value for this attribute class; otherwise, <see langword="false" />.</returns>
		// Token: 0x060009FD RID: 2557 RVA: 0x0002BBFC File Offset: 0x00029DFC
		public override bool IsDefaultAttribute()
		{
			return this.Category.Equals(CategoryAttribute.Default.Category);
		}

		// Token: 0x04000C65 RID: 3173
		private static volatile CategoryAttribute appearance;

		// Token: 0x04000C66 RID: 3174
		private static volatile CategoryAttribute asynchronous;

		// Token: 0x04000C67 RID: 3175
		private static volatile CategoryAttribute behavior;

		// Token: 0x04000C68 RID: 3176
		private static volatile CategoryAttribute data;

		// Token: 0x04000C69 RID: 3177
		private static volatile CategoryAttribute design;

		// Token: 0x04000C6A RID: 3178
		private static volatile CategoryAttribute action;

		// Token: 0x04000C6B RID: 3179
		private static volatile CategoryAttribute format;

		// Token: 0x04000C6C RID: 3180
		private static volatile CategoryAttribute layout;

		// Token: 0x04000C6D RID: 3181
		private static volatile CategoryAttribute mouse;

		// Token: 0x04000C6E RID: 3182
		private static volatile CategoryAttribute key;

		// Token: 0x04000C6F RID: 3183
		private static volatile CategoryAttribute focus;

		// Token: 0x04000C70 RID: 3184
		private static volatile CategoryAttribute windowStyle;

		// Token: 0x04000C71 RID: 3185
		private static volatile CategoryAttribute dragDrop;

		// Token: 0x04000C72 RID: 3186
		private static volatile CategoryAttribute defAttr;

		// Token: 0x04000C73 RID: 3187
		private bool localized;

		// Token: 0x04000C74 RID: 3188
		private string categoryValue;
	}
}
