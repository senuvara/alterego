﻿using System;
using System.Collections;

namespace System.ComponentModel
{
	/// <summary>Provides the features required to support both complex and simple scenarios when binding to a data source.</summary>
	// Token: 0x02000171 RID: 369
	public interface IBindingList : IList, ICollection, IEnumerable
	{
		/// <summary>Gets whether you can add items to the list using <see cref="M:System.ComponentModel.IBindingList.AddNew" />.</summary>
		/// <returns>
		///     <see langword="true" /> if you can add items to the list using <see cref="M:System.ComponentModel.IBindingList.AddNew" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000271 RID: 625
		// (get) Token: 0x06000B7B RID: 2939
		bool AllowNew { get; }

		/// <summary>Adds a new item to the list.</summary>
		/// <returns>The item added to the list.</returns>
		/// <exception cref="T:System.NotSupportedException">
		///         <see cref="P:System.ComponentModel.IBindingList.AllowNew" /> is <see langword="false" />. </exception>
		// Token: 0x06000B7C RID: 2940
		object AddNew();

		/// <summary>Gets whether you can update items in the list.</summary>
		/// <returns>
		///     <see langword="true" /> if you can update the items in the list; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000272 RID: 626
		// (get) Token: 0x06000B7D RID: 2941
		bool AllowEdit { get; }

		/// <summary>Gets whether you can remove items from the list, using <see cref="M:System.Collections.IList.Remove(System.Object)" /> or <see cref="M:System.Collections.IList.RemoveAt(System.Int32)" />.</summary>
		/// <returns>
		///     <see langword="true" /> if you can remove items from the list; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000273 RID: 627
		// (get) Token: 0x06000B7E RID: 2942
		bool AllowRemove { get; }

		/// <summary>Gets whether a <see cref="E:System.ComponentModel.IBindingList.ListChanged" /> event is raised when the list changes or an item in the list changes.</summary>
		/// <returns>
		///     <see langword="true" /> if a <see cref="E:System.ComponentModel.IBindingList.ListChanged" /> event is raised when the list changes or when an item changes; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06000B7F RID: 2943
		bool SupportsChangeNotification { get; }

		/// <summary>Gets whether the list supports searching using the <see cref="M:System.ComponentModel.IBindingList.Find(System.ComponentModel.PropertyDescriptor,System.Object)" /> method.</summary>
		/// <returns>
		///     <see langword="true" /> if the list supports searching using the <see cref="M:System.ComponentModel.IBindingList.Find(System.ComponentModel.PropertyDescriptor,System.Object)" /> method; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06000B80 RID: 2944
		bool SupportsSearching { get; }

		/// <summary>Gets whether the list supports sorting.</summary>
		/// <returns>
		///     <see langword="true" /> if the list supports sorting; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000276 RID: 630
		// (get) Token: 0x06000B81 RID: 2945
		bool SupportsSorting { get; }

		/// <summary>Gets whether the items in the list are sorted.</summary>
		/// <returns>
		///     <see langword="true" /> if <see cref="M:System.ComponentModel.IBindingList.ApplySort(System.ComponentModel.PropertyDescriptor,System.ComponentModel.ListSortDirection)" /> has been called and <see cref="M:System.ComponentModel.IBindingList.RemoveSort" /> has not been called; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.NotSupportedException">
		///         <see cref="P:System.ComponentModel.IBindingList.SupportsSorting" /> is <see langword="false" />. </exception>
		// Token: 0x17000277 RID: 631
		// (get) Token: 0x06000B82 RID: 2946
		bool IsSorted { get; }

		/// <summary>Gets the <see cref="T:System.ComponentModel.PropertyDescriptor" /> that is being used for sorting.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.PropertyDescriptor" /> that is being used for sorting.</returns>
		/// <exception cref="T:System.NotSupportedException">
		///         <see cref="P:System.ComponentModel.IBindingList.SupportsSorting" /> is <see langword="false" />. </exception>
		// Token: 0x17000278 RID: 632
		// (get) Token: 0x06000B83 RID: 2947
		PropertyDescriptor SortProperty { get; }

		/// <summary>Gets the direction of the sort.</summary>
		/// <returns>One of the <see cref="T:System.ComponentModel.ListSortDirection" /> values.</returns>
		/// <exception cref="T:System.NotSupportedException">
		///         <see cref="P:System.ComponentModel.IBindingList.SupportsSorting" /> is <see langword="false" />. </exception>
		// Token: 0x17000279 RID: 633
		// (get) Token: 0x06000B84 RID: 2948
		ListSortDirection SortDirection { get; }

		/// <summary>Occurs when the list changes or an item in the list changes.</summary>
		// Token: 0x1400000C RID: 12
		// (add) Token: 0x06000B85 RID: 2949
		// (remove) Token: 0x06000B86 RID: 2950
		event ListChangedEventHandler ListChanged;

		/// <summary>Adds the <see cref="T:System.ComponentModel.PropertyDescriptor" /> to the indexes used for searching.</summary>
		/// <param name="property">The <see cref="T:System.ComponentModel.PropertyDescriptor" /> to add to the indexes used for searching. </param>
		// Token: 0x06000B87 RID: 2951
		void AddIndex(PropertyDescriptor property);

		/// <summary>Sorts the list based on a <see cref="T:System.ComponentModel.PropertyDescriptor" /> and a <see cref="T:System.ComponentModel.ListSortDirection" />.</summary>
		/// <param name="property">The <see cref="T:System.ComponentModel.PropertyDescriptor" /> to sort by. </param>
		/// <param name="direction">One of the <see cref="T:System.ComponentModel.ListSortDirection" /> values. </param>
		/// <exception cref="T:System.NotSupportedException">
		///         <see cref="P:System.ComponentModel.IBindingList.SupportsSorting" /> is <see langword="false" />. </exception>
		// Token: 0x06000B88 RID: 2952
		void ApplySort(PropertyDescriptor property, ListSortDirection direction);

		/// <summary>Returns the index of the row that has the given <see cref="T:System.ComponentModel.PropertyDescriptor" />.</summary>
		/// <param name="property">The <see cref="T:System.ComponentModel.PropertyDescriptor" /> to search on. </param>
		/// <param name="key">The value of the <paramref name="property" /> parameter to search for. </param>
		/// <returns>The index of the row that has the given <see cref="T:System.ComponentModel.PropertyDescriptor" />.</returns>
		/// <exception cref="T:System.NotSupportedException">
		///         <see cref="P:System.ComponentModel.IBindingList.SupportsSearching" /> is <see langword="false" />. </exception>
		// Token: 0x06000B89 RID: 2953
		int Find(PropertyDescriptor property, object key);

		/// <summary>Removes the <see cref="T:System.ComponentModel.PropertyDescriptor" /> from the indexes used for searching.</summary>
		/// <param name="property">The <see cref="T:System.ComponentModel.PropertyDescriptor" /> to remove from the indexes used for searching. </param>
		// Token: 0x06000B8A RID: 2954
		void RemoveIndex(PropertyDescriptor property);

		/// <summary>Removes any sort applied using <see cref="M:System.ComponentModel.IBindingList.ApplySort(System.ComponentModel.PropertyDescriptor,System.ComponentModel.ListSortDirection)" />.</summary>
		/// <exception cref="T:System.NotSupportedException">
		///         <see cref="P:System.ComponentModel.IBindingList.SupportsSorting" /> is <see langword="false" />. </exception>
		// Token: 0x06000B8B RID: 2955
		void RemoveSort();
	}
}
