﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies whether a property can only be set at design time.</summary>
	// Token: 0x02000159 RID: 345
	[AttributeUsage(AttributeTargets.All)]
	public sealed class DesignOnlyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DesignOnlyAttribute" /> class.</summary>
		/// <param name="isDesignOnly">
		///       <see langword="true" /> if a property can be set only at design time; <see langword="false" /> if the property can be set at design time and at run time. </param>
		// Token: 0x06000AC6 RID: 2758 RVA: 0x0002ECFE File Offset: 0x0002CEFE
		public DesignOnlyAttribute(bool isDesignOnly)
		{
			this.isDesignOnly = isDesignOnly;
		}

		/// <summary>Gets a value indicating whether a property can be set only at design time.</summary>
		/// <returns>
		///     <see langword="true" /> if a property can be set only at design time; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000247 RID: 583
		// (get) Token: 0x06000AC7 RID: 2759 RVA: 0x0002ED0D File Offset: 0x0002CF0D
		public bool IsDesignOnly
		{
			get
			{
				return this.isDesignOnly;
			}
		}

		/// <summary>Determines if this attribute is the default.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute is the default value for this attribute class; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AC8 RID: 2760 RVA: 0x0002ED15 File Offset: 0x0002CF15
		public override bool IsDefaultAttribute()
		{
			return this.IsDesignOnly == DesignOnlyAttribute.Default.IsDesignOnly;
		}

		/// <summary>Returns whether the value of the given object is equal to the current <see cref="T:System.ComponentModel.DesignOnlyAttribute" />.</summary>
		/// <param name="obj">The object to test the value equality of. </param>
		/// <returns>
		///     <see langword="true" /> if the value of the given object is equal to that of the current; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AC9 RID: 2761 RVA: 0x0002ED2C File Offset: 0x0002CF2C
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			DesignOnlyAttribute designOnlyAttribute = obj as DesignOnlyAttribute;
			return designOnlyAttribute != null && designOnlyAttribute.isDesignOnly == this.isDesignOnly;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000ACA RID: 2762 RVA: 0x0002ED59 File Offset: 0x0002CF59
		public override int GetHashCode()
		{
			return this.isDesignOnly.GetHashCode();
		}

		// Token: 0x06000ACB RID: 2763 RVA: 0x0002ED66 File Offset: 0x0002CF66
		// Note: this type is marked as 'beforefieldinit'.
		static DesignOnlyAttribute()
		{
		}

		// Token: 0x04000CAD RID: 3245
		private bool isDesignOnly;

		/// <summary>Specifies that a property can be set only at design time. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000CAE RID: 3246
		public static readonly DesignOnlyAttribute Yes = new DesignOnlyAttribute(true);

		/// <summary>Specifies that a property can be set at design time or at run time. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000CAF RID: 3247
		public static readonly DesignOnlyAttribute No = new DesignOnlyAttribute(false);

		/// <summary>Specifies the default value for the <see cref="T:System.ComponentModel.DesignOnlyAttribute" />, which is <see cref="F:System.ComponentModel.DesignOnlyAttribute.No" />. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000CB0 RID: 3248
		public static readonly DesignOnlyAttribute Default = DesignOnlyAttribute.No;
	}
}
