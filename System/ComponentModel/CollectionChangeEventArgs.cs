﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides data for the <see cref="E:System.Data.DataColumnCollection.CollectionChanged" /> event.</summary>
	// Token: 0x0200013B RID: 315
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class CollectionChangeEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.CollectionChangeEventArgs" /> class.</summary>
		/// <param name="action">One of the <see cref="T:System.ComponentModel.CollectionChangeAction" /> values that specifies how the collection changed. </param>
		/// <param name="element">An <see cref="T:System.Object" /> that specifies the instance of the collection where the change occurred. </param>
		// Token: 0x06000A02 RID: 2562 RVA: 0x0002BCCC File Offset: 0x00029ECC
		public CollectionChangeEventArgs(CollectionChangeAction action, object element)
		{
			this.action = action;
			this.element = element;
		}

		/// <summary>Gets an action that specifies how the collection changed.</summary>
		/// <returns>One of the <see cref="T:System.ComponentModel.CollectionChangeAction" /> values.</returns>
		// Token: 0x17000221 RID: 545
		// (get) Token: 0x06000A03 RID: 2563 RVA: 0x0002BCE2 File Offset: 0x00029EE2
		public virtual CollectionChangeAction Action
		{
			get
			{
				return this.action;
			}
		}

		/// <summary>Gets the instance of the collection with the change.</summary>
		/// <returns>An <see cref="T:System.Object" /> that represents the instance of the collection with the change, or <see langword="null" /> if you refresh the collection.</returns>
		// Token: 0x17000222 RID: 546
		// (get) Token: 0x06000A04 RID: 2564 RVA: 0x0002BCEA File Offset: 0x00029EEA
		public virtual object Element
		{
			get
			{
				return this.element;
			}
		}

		// Token: 0x04000C79 RID: 3193
		private CollectionChangeAction action;

		// Token: 0x04000C7A RID: 3194
		private object element;
	}
}
