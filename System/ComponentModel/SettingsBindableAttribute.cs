﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies when a component property can be bound to an application setting.</summary>
	// Token: 0x020001CE RID: 462
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class SettingsBindableAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.SettingsBindableAttribute" /> class. </summary>
		/// <param name="bindable">
		///       <see langword="true" /> to specify that a property is appropriate to bind settings to; otherwise, <see langword="false" />.</param>
		// Token: 0x06000E61 RID: 3681 RVA: 0x00039371 File Offset: 0x00037571
		public SettingsBindableAttribute(bool bindable)
		{
			this._bindable = bindable;
		}

		/// <summary>Gets a value indicating whether a property is appropriate to bind settings to. </summary>
		/// <returns>
		///     <see langword="true" /> if the property is appropriate to bind settings to; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000320 RID: 800
		// (get) Token: 0x06000E62 RID: 3682 RVA: 0x00039380 File Offset: 0x00037580
		public bool Bindable
		{
			get
			{
				return this._bindable;
			}
		}

		/// <summary>Returns a value that indicates whether this instance is equal to a specified object.</summary>
		/// <param name="obj">An <see cref="T:System.Object" /> to compare with this instance or a null reference (<see langword="Nothing" /> in Visual Basic).</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> equals the type and value of this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000E63 RID: 3683 RVA: 0x00039388 File Offset: 0x00037588
		public override bool Equals(object obj)
		{
			return obj == this || (obj != null && obj is SettingsBindableAttribute && ((SettingsBindableAttribute)obj).Bindable == this._bindable);
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000E64 RID: 3684 RVA: 0x000393B0 File Offset: 0x000375B0
		public override int GetHashCode()
		{
			return this._bindable.GetHashCode();
		}

		// Token: 0x06000E65 RID: 3685 RVA: 0x000393BD File Offset: 0x000375BD
		// Note: this type is marked as 'beforefieldinit'.
		static SettingsBindableAttribute()
		{
		}

		/// <summary>Specifies that a property is appropriate to bind settings to.</summary>
		// Token: 0x04000DE3 RID: 3555
		public static readonly SettingsBindableAttribute Yes = new SettingsBindableAttribute(true);

		/// <summary>Specifies that a property is not appropriate to bind settings to.</summary>
		// Token: 0x04000DE4 RID: 3556
		public static readonly SettingsBindableAttribute No = new SettingsBindableAttribute(false);

		// Token: 0x04000DE5 RID: 3557
		private bool _bindable;
	}
}
