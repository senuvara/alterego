﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	/// <summary>Provides a simple implementation of the <see cref="T:System.ComponentModel.Design.IServiceContainer" /> interface. This class cannot be inherited.</summary>
	// Token: 0x0200023B RID: 571
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class ServiceContainer : IServiceContainer, IServiceProvider, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.ServiceContainer" /> class.</summary>
		// Token: 0x06001168 RID: 4456 RVA: 0x0000232F File Offset: 0x0000052F
		public ServiceContainer()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.ServiceContainer" /> class using the specified parent service provider.</summary>
		/// <param name="parentProvider">A parent service provider. </param>
		// Token: 0x06001169 RID: 4457 RVA: 0x0004007D File Offset: 0x0003E27D
		public ServiceContainer(IServiceProvider parentProvider)
		{
			this.parentProvider = parentProvider;
		}

		// Token: 0x17000390 RID: 912
		// (get) Token: 0x0600116A RID: 4458 RVA: 0x0004008C File Offset: 0x0003E28C
		private IServiceContainer Container
		{
			get
			{
				IServiceContainer result = null;
				if (this.parentProvider != null)
				{
					result = (IServiceContainer)this.parentProvider.GetService(typeof(IServiceContainer));
				}
				return result;
			}
		}

		/// <summary>Gets the default services implemented directly by <see cref="T:System.ComponentModel.Design.ServiceContainer" />.</summary>
		/// <returns>The default services.</returns>
		// Token: 0x17000391 RID: 913
		// (get) Token: 0x0600116B RID: 4459 RVA: 0x000400BF File Offset: 0x0003E2BF
		protected virtual Type[] DefaultServices
		{
			get
			{
				return ServiceContainer._defaultServices;
			}
		}

		// Token: 0x17000392 RID: 914
		// (get) Token: 0x0600116C RID: 4460 RVA: 0x000400C6 File Offset: 0x0003E2C6
		private ServiceContainer.ServiceCollection<object> Services
		{
			get
			{
				if (this.services == null)
				{
					this.services = new ServiceContainer.ServiceCollection<object>();
				}
				return this.services;
			}
		}

		/// <summary>Adds the specified service to the service container.</summary>
		/// <param name="serviceType">The type of service to add. </param>
		/// <param name="serviceInstance">An instance of the service to add. This object must implement or inherit from the type indicated by the <paramref name="serviceType" /> parameter. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="serviceType" /> or <paramref name="serviceInstance" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">A service of type <paramref name="serviceType" /> already exists in the container.</exception>
		// Token: 0x0600116D RID: 4461 RVA: 0x000400E1 File Offset: 0x0003E2E1
		public void AddService(Type serviceType, object serviceInstance)
		{
			this.AddService(serviceType, serviceInstance, false);
		}

		/// <summary>Adds the specified service to the service container.</summary>
		/// <param name="serviceType">The type of service to add. </param>
		/// <param name="serviceInstance">An instance of the service type to add. This object must implement or inherit from the type indicated by the <paramref name="serviceType" /> parameter. </param>
		/// <param name="promote">
		///       <see langword="true" /> if this service should be added to any parent service containers; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="serviceType" /> or <paramref name="serviceInstance" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">A service of type <paramref name="serviceType" /> already exists in the container.</exception>
		// Token: 0x0600116E RID: 4462 RVA: 0x000400EC File Offset: 0x0003E2EC
		public virtual void AddService(Type serviceType, object serviceInstance, bool promote)
		{
			if (promote)
			{
				IServiceContainer container = this.Container;
				if (container != null)
				{
					container.AddService(serviceType, serviceInstance, promote);
					return;
				}
			}
			if (serviceType == null)
			{
				throw new ArgumentNullException("serviceType");
			}
			if (serviceInstance == null)
			{
				throw new ArgumentNullException("serviceInstance");
			}
			if (!(serviceInstance is ServiceCreatorCallback) && !serviceInstance.GetType().IsCOMObject && !serviceType.IsAssignableFrom(serviceInstance.GetType()))
			{
				throw new ArgumentException(SR.GetString("The service instance must derive from or implement {0}.", new object[]
				{
					serviceType.FullName
				}));
			}
			if (this.Services.ContainsKey(serviceType))
			{
				throw new ArgumentException(SR.GetString("The service {0} already exists in the service container.", new object[]
				{
					serviceType.FullName
				}), "serviceType");
			}
			this.Services[serviceType] = serviceInstance;
		}

		/// <summary>Adds the specified service to the service container.</summary>
		/// <param name="serviceType">The type of service to add. </param>
		/// <param name="callback">A callback object that can create the service. This allows a service to be declared as available, but delays creation of the object until the service is requested. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="serviceType" /> or <paramref name="callback" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">A service of type <paramref name="serviceType" /> already exists in the container.</exception>
		// Token: 0x0600116F RID: 4463 RVA: 0x000401B3 File Offset: 0x0003E3B3
		public void AddService(Type serviceType, ServiceCreatorCallback callback)
		{
			this.AddService(serviceType, callback, false);
		}

		/// <summary>Adds the specified service to the service container.</summary>
		/// <param name="serviceType">The type of service to add. </param>
		/// <param name="callback">A callback object that can create the service. This allows a service to be declared as available, but delays creation of the object until the service is requested. </param>
		/// <param name="promote">
		///       <see langword="true" /> if this service should be added to any parent service containers; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="serviceType" /> or <paramref name="callback" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">A service of type <paramref name="serviceType" /> already exists in the container.</exception>
		// Token: 0x06001170 RID: 4464 RVA: 0x000401C0 File Offset: 0x0003E3C0
		public virtual void AddService(Type serviceType, ServiceCreatorCallback callback, bool promote)
		{
			if (promote)
			{
				IServiceContainer container = this.Container;
				if (container != null)
				{
					container.AddService(serviceType, callback, promote);
					return;
				}
			}
			if (serviceType == null)
			{
				throw new ArgumentNullException("serviceType");
			}
			if (callback == null)
			{
				throw new ArgumentNullException("callback");
			}
			if (this.Services.ContainsKey(serviceType))
			{
				throw new ArgumentException(SR.GetString("The service {0} already exists in the service container.", new object[]
				{
					serviceType.FullName
				}), "serviceType");
			}
			this.Services[serviceType] = callback;
		}

		/// <summary>Disposes this service container.</summary>
		// Token: 0x06001171 RID: 4465 RVA: 0x00040245 File Offset: 0x0003E445
		public void Dispose()
		{
			this.Dispose(true);
		}

		/// <summary>Disposes this service container.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> if the <see cref="T:System.ComponentModel.Design.ServiceContainer" /> is in the process of being disposed of; otherwise, <see langword="false" />.</param>
		// Token: 0x06001172 RID: 4466 RVA: 0x00040250 File Offset: 0x0003E450
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				ServiceContainer.ServiceCollection<object> serviceCollection = this.services;
				this.services = null;
				if (serviceCollection != null)
				{
					foreach (object obj in serviceCollection.Values)
					{
						if (obj is IDisposable)
						{
							((IDisposable)obj).Dispose();
						}
					}
				}
			}
		}

		/// <summary>Gets the requested service.</summary>
		/// <param name="serviceType">The type of service to retrieve. </param>
		/// <returns>An instance of the service if it could be found, or <see langword="null" /> if it could not be found.</returns>
		// Token: 0x06001173 RID: 4467 RVA: 0x000402C4 File Offset: 0x0003E4C4
		public virtual object GetService(Type serviceType)
		{
			object obj = null;
			Type[] defaultServices = this.DefaultServices;
			for (int i = 0; i < defaultServices.Length; i++)
			{
				if (serviceType.IsEquivalentTo(defaultServices[i]))
				{
					obj = this;
					break;
				}
			}
			if (obj == null)
			{
				this.Services.TryGetValue(serviceType, out obj);
			}
			if (obj is ServiceCreatorCallback)
			{
				obj = ((ServiceCreatorCallback)obj)(this, serviceType);
				if (obj != null && !obj.GetType().IsCOMObject && !serviceType.IsAssignableFrom(obj.GetType()))
				{
					obj = null;
				}
				this.Services[serviceType] = obj;
			}
			if (obj == null && this.parentProvider != null)
			{
				obj = this.parentProvider.GetService(serviceType);
			}
			return obj;
		}

		/// <summary>Removes the specified service type from the service container.</summary>
		/// <param name="serviceType">The type of service to remove. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="serviceType" /> is <see langword="null" />.</exception>
		// Token: 0x06001174 RID: 4468 RVA: 0x00040365 File Offset: 0x0003E565
		public void RemoveService(Type serviceType)
		{
			this.RemoveService(serviceType, false);
		}

		/// <summary>Removes the specified service type from the service container.</summary>
		/// <param name="serviceType">The type of service to remove. </param>
		/// <param name="promote">
		///       <see langword="true" /> if this service should be removed from any parent service containers; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="serviceType" /> is <see langword="null" />.</exception>
		// Token: 0x06001175 RID: 4469 RVA: 0x00040370 File Offset: 0x0003E570
		public virtual void RemoveService(Type serviceType, bool promote)
		{
			if (promote)
			{
				IServiceContainer container = this.Container;
				if (container != null)
				{
					container.RemoveService(serviceType, promote);
					return;
				}
			}
			if (serviceType == null)
			{
				throw new ArgumentNullException("serviceType");
			}
			this.Services.Remove(serviceType);
		}

		// Token: 0x06001176 RID: 4470 RVA: 0x000403B4 File Offset: 0x0003E5B4
		// Note: this type is marked as 'beforefieldinit'.
		static ServiceContainer()
		{
		}

		// Token: 0x04000E8F RID: 3727
		private ServiceContainer.ServiceCollection<object> services;

		// Token: 0x04000E90 RID: 3728
		private IServiceProvider parentProvider;

		// Token: 0x04000E91 RID: 3729
		private static Type[] _defaultServices = new Type[]
		{
			typeof(IServiceContainer),
			typeof(ServiceContainer)
		};

		// Token: 0x04000E92 RID: 3730
		private static TraceSwitch TRACESERVICE = new TraceSwitch("TRACESERVICE", "ServiceProvider: Trace service provider requests.");

		// Token: 0x0200023C RID: 572
		private sealed class ServiceCollection<T> : Dictionary<Type, T>
		{
			// Token: 0x06001177 RID: 4471 RVA: 0x000403EF File Offset: 0x0003E5EF
			public ServiceCollection() : base(ServiceContainer.ServiceCollection<T>.serviceTypeComparer)
			{
			}

			// Token: 0x06001178 RID: 4472 RVA: 0x000403FC File Offset: 0x0003E5FC
			// Note: this type is marked as 'beforefieldinit'.
			static ServiceCollection()
			{
			}

			// Token: 0x04000E93 RID: 3731
			private static ServiceContainer.ServiceCollection<T>.EmbeddedTypeAwareTypeComparer serviceTypeComparer = new ServiceContainer.ServiceCollection<T>.EmbeddedTypeAwareTypeComparer();

			// Token: 0x0200023D RID: 573
			private sealed class EmbeddedTypeAwareTypeComparer : IEqualityComparer<Type>
			{
				// Token: 0x06001179 RID: 4473 RVA: 0x00040408 File Offset: 0x0003E608
				public bool Equals(Type x, Type y)
				{
					return x.IsEquivalentTo(y);
				}

				// Token: 0x0600117A RID: 4474 RVA: 0x00040411 File Offset: 0x0003E611
				public int GetHashCode(Type obj)
				{
					return obj.FullName.GetHashCode();
				}

				// Token: 0x0600117B RID: 4475 RVA: 0x0000232F File Offset: 0x0000052F
				public EmbeddedTypeAwareTypeComparer()
				{
				}
			}
		}
	}
}
