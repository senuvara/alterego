﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	/// <summary>Provides data for the <see cref="E:System.ComponentModel.Design.IDesignerHost.TransactionClosed" /> and <see cref="E:System.ComponentModel.Design.IDesignerHost.TransactionClosing" /> events.</summary>
	// Token: 0x02000212 RID: 530
	[ComVisible(true)]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class DesignerTransactionCloseEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.DesignerTransactionCloseEventArgs" /> class, using the specified value that indicates whether the designer called <see cref="M:System.ComponentModel.Design.DesignerTransaction.Commit" /> on the transaction.</summary>
		/// <param name="commit">A value indicating whether the transaction was committed.</param>
		// Token: 0x06001082 RID: 4226 RVA: 0x0003F707 File Offset: 0x0003D907
		[Obsolete("This constructor is obsolete. Use DesignerTransactionCloseEventArgs(bool, bool) instead.  http://go.microsoft.com/fwlink/?linkid=14202")]
		public DesignerTransactionCloseEventArgs(bool commit) : this(commit, true)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.DesignerTransactionCloseEventArgs" /> class. </summary>
		/// <param name="commit">A value indicating whether the transaction was committed.</param>
		/// <param name="lastTransaction">
		///       <see langword="true" /> if this is the last transaction to close; otherwise, <see langword="false" />.</param>
		// Token: 0x06001083 RID: 4227 RVA: 0x0003F711 File Offset: 0x0003D911
		public DesignerTransactionCloseEventArgs(bool commit, bool lastTransaction)
		{
			this.commit = commit;
			this.lastTransaction = lastTransaction;
		}

		/// <summary>Indicates whether the designer called <see cref="M:System.ComponentModel.Design.DesignerTransaction.Commit" /> on the transaction.</summary>
		/// <returns>
		///     <see langword="true" /> if the designer called <see cref="M:System.ComponentModel.Design.DesignerTransaction.Commit" /> on the transaction; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06001084 RID: 4228 RVA: 0x0003F727 File Offset: 0x0003D927
		public bool TransactionCommitted
		{
			get
			{
				return this.commit;
			}
		}

		/// <summary>Gets a value indicating whether this is the last transaction to close.</summary>
		/// <returns>
		///     <see langword="true" />, if this is the last transaction to close; otherwise, <see langword="false" />. </returns>
		// Token: 0x1700036C RID: 876
		// (get) Token: 0x06001085 RID: 4229 RVA: 0x0003F72F File Offset: 0x0003D92F
		public bool LastTransaction
		{
			get
			{
				return this.lastTransaction;
			}
		}

		// Token: 0x04000E67 RID: 3687
		private bool commit;

		// Token: 0x04000E68 RID: 3688
		private bool lastTransaction;
	}
}
