﻿using System;
using System.Collections;
using System.Reflection;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	/// <summary>Represents a design-time license context that can support a license provider at design time.</summary>
	// Token: 0x02000216 RID: 534
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class DesigntimeLicenseContext : LicenseContext
	{
		/// <summary>Gets the license usage mode.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.LicenseUsageMode" /> indicating the licensing mode for the context.</returns>
		// Token: 0x17000370 RID: 880
		// (get) Token: 0x060010A1 RID: 4257 RVA: 0x00003298 File Offset: 0x00001498
		public override LicenseUsageMode UsageMode
		{
			get
			{
				return LicenseUsageMode.Designtime;
			}
		}

		/// <summary>Gets a saved license key.</summary>
		/// <param name="type">The type of the license key. </param>
		/// <param name="resourceAssembly">The assembly to get the key from. </param>
		/// <returns>The saved license key that matches the specified type.</returns>
		// Token: 0x060010A2 RID: 4258 RVA: 0x00008B3F File Offset: 0x00006D3F
		public override string GetSavedLicenseKey(Type type, Assembly resourceAssembly)
		{
			return null;
		}

		/// <summary>Sets a saved license key.</summary>
		/// <param name="type">The type of the license key. </param>
		/// <param name="key">The license key. </param>
		// Token: 0x060010A3 RID: 4259 RVA: 0x0003F920 File Offset: 0x0003DB20
		public override void SetSavedLicenseKey(Type type, string key)
		{
			this.savedLicenseKeys[type.AssemblyQualifiedName] = key;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.DesigntimeLicenseContext" /> class. </summary>
		// Token: 0x060010A4 RID: 4260 RVA: 0x0003F934 File Offset: 0x0003DB34
		public DesigntimeLicenseContext()
		{
		}

		// Token: 0x04000E69 RID: 3689
		internal Hashtable savedLicenseKeys = new Hashtable();
	}
}
