﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	/// <summary>Represents a unique command identifier that consists of a numeric command ID and a GUID menu group identifier.</summary>
	// Token: 0x02000203 RID: 515
	[ComVisible(true)]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class CommandID
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.CommandID" /> class using the specified menu group GUID and command ID number.</summary>
		/// <param name="menuGroup">The GUID of the group that this menu command belongs to. </param>
		/// <param name="commandID">The numeric identifier of this menu command. </param>
		// Token: 0x06001018 RID: 4120 RVA: 0x0003EE1D File Offset: 0x0003D01D
		public CommandID(Guid menuGroup, int commandID)
		{
			this.menuGroup = menuGroup;
			this.commandID = commandID;
		}

		/// <summary>Gets the numeric command ID.</summary>
		/// <returns>The command ID number.</returns>
		// Token: 0x17000349 RID: 841
		// (get) Token: 0x06001019 RID: 4121 RVA: 0x0003EE33 File Offset: 0x0003D033
		public virtual int ID
		{
			get
			{
				return this.commandID;
			}
		}

		/// <summary>Determines whether two <see cref="T:System.ComponentModel.Design.CommandID" /> instances are equal.</summary>
		/// <param name="obj">The object to compare. </param>
		/// <returns>
		///     <see langword="true" /> if the specified object is equivalent to this one; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600101A RID: 4122 RVA: 0x0003EE3C File Offset: 0x0003D03C
		public override bool Equals(object obj)
		{
			if (!(obj is CommandID))
			{
				return false;
			}
			CommandID commandID = (CommandID)obj;
			return commandID.menuGroup.Equals(this.menuGroup) && commandID.commandID == this.commandID;
		}

		/// <summary>Serves as a hash function for a particular type.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Object" />.</returns>
		// Token: 0x0600101B RID: 4123 RVA: 0x0003EE80 File Offset: 0x0003D080
		public override int GetHashCode()
		{
			return this.menuGroup.GetHashCode() << 2 | this.commandID;
		}

		/// <summary>Gets the GUID of the menu group that the menu command identified by this <see cref="T:System.ComponentModel.Design.CommandID" /> belongs to.</summary>
		/// <returns>The GUID of the command group for this command.</returns>
		// Token: 0x1700034A RID: 842
		// (get) Token: 0x0600101C RID: 4124 RVA: 0x0003EEAA File Offset: 0x0003D0AA
		public virtual Guid Guid
		{
			get
			{
				return this.menuGroup;
			}
		}

		/// <summary>Returns a <see cref="T:System.String" /> that represents the current object.</summary>
		/// <returns>A string that contains the command ID information, both the GUID and integer identifier. </returns>
		// Token: 0x0600101D RID: 4125 RVA: 0x0003EEB4 File Offset: 0x0003D0B4
		public override string ToString()
		{
			return this.menuGroup.ToString() + " : " + this.commandID.ToString(CultureInfo.CurrentCulture);
		}

		// Token: 0x04000E4D RID: 3661
		private readonly Guid menuGroup;

		// Token: 0x04000E4E RID: 3662
		private readonly int commandID;
	}
}
