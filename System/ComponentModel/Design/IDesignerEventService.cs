﻿using System;

namespace System.ComponentModel.Design
{
	/// <summary>Provides event notifications when root designers are added and removed, when a selected component changes, and when the current root designer changes.</summary>
	// Token: 0x02000223 RID: 547
	public interface IDesignerEventService
	{
		/// <summary>Gets the root designer for the currently active document.</summary>
		/// <returns>The currently active document, or <see langword="null" /> if there is no active document.</returns>
		// Token: 0x1700037A RID: 890
		// (get) Token: 0x060010DD RID: 4317
		IDesignerHost ActiveDesigner { get; }

		/// <summary>Gets a collection of root designers for design documents that are currently active in the development environment.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerCollection" /> containing the root designers that have been created and not yet disposed.</returns>
		// Token: 0x1700037B RID: 891
		// (get) Token: 0x060010DE RID: 4318
		DesignerCollection Designers { get; }

		/// <summary>Occurs when the current root designer changes.</summary>
		// Token: 0x1400001B RID: 27
		// (add) Token: 0x060010DF RID: 4319
		// (remove) Token: 0x060010E0 RID: 4320
		event ActiveDesignerEventHandler ActiveDesignerChanged;

		/// <summary>Occurs when a root designer is created.</summary>
		// Token: 0x1400001C RID: 28
		// (add) Token: 0x060010E1 RID: 4321
		// (remove) Token: 0x060010E2 RID: 4322
		event DesignerEventHandler DesignerCreated;

		/// <summary>Occurs when a root designer for a document is disposed.</summary>
		// Token: 0x1400001D RID: 29
		// (add) Token: 0x060010E3 RID: 4323
		// (remove) Token: 0x060010E4 RID: 4324
		event DesignerEventHandler DesignerDisposed;

		/// <summary>Occurs when the current design-view selection changes.</summary>
		// Token: 0x1400001E RID: 30
		// (add) Token: 0x060010E5 RID: 4325
		// (remove) Token: 0x060010E6 RID: 4326
		event EventHandler SelectionChanged;
	}
}
