﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	/// <summary>Provides an interface to add and remove the event handlers for events that add, change, remove or rename components, and provides methods to raise a <see cref="E:System.ComponentModel.Design.IComponentChangeService.ComponentChanged" /> or <see cref="E:System.ComponentModel.Design.IComponentChangeService.ComponentChanging" /> event.</summary>
	// Token: 0x0200021F RID: 543
	[ComVisible(true)]
	public interface IComponentChangeService
	{
		/// <summary>Occurs when a component has been added.</summary>
		// Token: 0x14000014 RID: 20
		// (add) Token: 0x060010C6 RID: 4294
		// (remove) Token: 0x060010C7 RID: 4295
		event ComponentEventHandler ComponentAdded;

		/// <summary>Occurs when a component is in the process of being added.</summary>
		// Token: 0x14000015 RID: 21
		// (add) Token: 0x060010C8 RID: 4296
		// (remove) Token: 0x060010C9 RID: 4297
		event ComponentEventHandler ComponentAdding;

		/// <summary>Occurs when a component has been changed.</summary>
		// Token: 0x14000016 RID: 22
		// (add) Token: 0x060010CA RID: 4298
		// (remove) Token: 0x060010CB RID: 4299
		event ComponentChangedEventHandler ComponentChanged;

		/// <summary>Occurs when a component is in the process of being changed.</summary>
		// Token: 0x14000017 RID: 23
		// (add) Token: 0x060010CC RID: 4300
		// (remove) Token: 0x060010CD RID: 4301
		event ComponentChangingEventHandler ComponentChanging;

		/// <summary>Occurs when a component has been removed.</summary>
		// Token: 0x14000018 RID: 24
		// (add) Token: 0x060010CE RID: 4302
		// (remove) Token: 0x060010CF RID: 4303
		event ComponentEventHandler ComponentRemoved;

		/// <summary>Occurs when a component is in the process of being removed.</summary>
		// Token: 0x14000019 RID: 25
		// (add) Token: 0x060010D0 RID: 4304
		// (remove) Token: 0x060010D1 RID: 4305
		event ComponentEventHandler ComponentRemoving;

		/// <summary>Occurs when a component is renamed.</summary>
		// Token: 0x1400001A RID: 26
		// (add) Token: 0x060010D2 RID: 4306
		// (remove) Token: 0x060010D3 RID: 4307
		event ComponentRenameEventHandler ComponentRename;

		/// <summary>Announces to the component change service that a particular component has changed.</summary>
		/// <param name="component">The component that has changed. </param>
		/// <param name="member">The member that has changed. This is <see langword="null" /> if this change is not related to a single member. </param>
		/// <param name="oldValue">The old value of the member. This is valid only if the member is not <see langword="null" />. </param>
		/// <param name="newValue">The new value of the member. This is valid only if the member is not <see langword="null" />. </param>
		// Token: 0x060010D4 RID: 4308
		void OnComponentChanged(object component, MemberDescriptor member, object oldValue, object newValue);

		/// <summary>Announces to the component change service that a particular component is changing.</summary>
		/// <param name="component">The component that is about to change. </param>
		/// <param name="member">The member that is changing. This is <see langword="null" /> if this change is not related to a single member. </param>
		// Token: 0x060010D5 RID: 4309
		void OnComponentChanging(object component, MemberDescriptor member);
	}
}
