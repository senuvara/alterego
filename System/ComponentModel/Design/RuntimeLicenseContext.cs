﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	// Token: 0x02000217 RID: 535
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	internal class RuntimeLicenseContext : LicenseContext
	{
		// Token: 0x060010A5 RID: 4261 RVA: 0x0003F948 File Offset: 0x0003DB48
		private string GetLocalPath(string fileName)
		{
			Uri uri = new Uri(fileName);
			return uri.LocalPath + uri.Fragment;
		}

		// Token: 0x060010A6 RID: 4262 RVA: 0x0003F970 File Offset: 0x0003DB70
		public override string GetSavedLicenseKey(Type type, Assembly resourceAssembly)
		{
			if (this.savedLicenseKeys == null || this.savedLicenseKeys[type.AssemblyQualifiedName] == null)
			{
				if (this.savedLicenseKeys == null)
				{
					this.savedLicenseKeys = new Hashtable();
				}
				Uri uri = null;
				if (resourceAssembly == null)
				{
					string licenseFile = AppDomain.CurrentDomain.SetupInformation.LicenseFile;
					string applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
					if (licenseFile != null && applicationBase != null)
					{
						uri = new Uri(new Uri(applicationBase), licenseFile);
					}
				}
				if (uri == null)
				{
					if (resourceAssembly == null)
					{
						resourceAssembly = Assembly.GetEntryAssembly();
					}
					if (resourceAssembly == null)
					{
						foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
						{
							if (!assembly.IsDynamic)
							{
								string text = this.GetLocalPath(assembly.EscapedCodeBase);
								text = new FileInfo(text).Name;
								Stream stream = assembly.GetManifestResourceStream(text + ".licenses");
								if (stream == null)
								{
									stream = this.CaseInsensitiveManifestResourceStreamLookup(assembly, text + ".licenses");
								}
								if (stream != null)
								{
									DesigntimeLicenseContextSerializer.Deserialize(stream, text.ToUpper(CultureInfo.InvariantCulture), this);
									break;
								}
							}
						}
					}
					else if (!resourceAssembly.IsDynamic)
					{
						string text2 = this.GetLocalPath(resourceAssembly.EscapedCodeBase);
						text2 = Path.GetFileName(text2);
						string text3 = text2 + ".licenses";
						Stream manifestResourceStream = resourceAssembly.GetManifestResourceStream(text3);
						if (manifestResourceStream == null)
						{
							string text4 = null;
							CompareInfo compareInfo = CultureInfo.InvariantCulture.CompareInfo;
							string name = resourceAssembly.GetName().Name;
							foreach (string text5 in resourceAssembly.GetManifestResourceNames())
							{
								if (compareInfo.Compare(text5, text3, CompareOptions.IgnoreCase) == 0 || compareInfo.Compare(text5, name + ".exe.licenses", CompareOptions.IgnoreCase) == 0 || compareInfo.Compare(text5, name + ".dll.licenses", CompareOptions.IgnoreCase) == 0)
								{
									text4 = text5;
									break;
								}
							}
							if (text4 != null)
							{
								manifestResourceStream = resourceAssembly.GetManifestResourceStream(text4);
							}
						}
						if (manifestResourceStream != null)
						{
							DesigntimeLicenseContextSerializer.Deserialize(manifestResourceStream, text2.ToUpper(CultureInfo.InvariantCulture), this);
						}
					}
				}
				if (uri != null)
				{
					Stream stream2 = RuntimeLicenseContext.OpenRead(uri);
					if (stream2 != null)
					{
						string[] segments = uri.Segments;
						string text6 = segments[segments.Length - 1];
						string text7 = text6.Substring(0, text6.LastIndexOf("."));
						DesigntimeLicenseContextSerializer.Deserialize(stream2, text7.ToUpper(CultureInfo.InvariantCulture), this);
					}
				}
			}
			return (string)this.savedLicenseKeys[type.AssemblyQualifiedName];
		}

		// Token: 0x060010A7 RID: 4263 RVA: 0x0003FC00 File Offset: 0x0003DE00
		private Stream CaseInsensitiveManifestResourceStreamLookup(Assembly satellite, string name)
		{
			CompareInfo compareInfo = CultureInfo.InvariantCulture.CompareInfo;
			string name2 = satellite.GetName().Name;
			foreach (string text in satellite.GetManifestResourceNames())
			{
				if (compareInfo.Compare(text, name, CompareOptions.IgnoreCase) == 0 || compareInfo.Compare(text, name2 + ".exe.licenses") == 0 || compareInfo.Compare(text, name2 + ".dll.licenses") == 0)
				{
					name = text;
					break;
				}
			}
			return satellite.GetManifestResourceStream(name);
		}

		// Token: 0x060010A8 RID: 4264 RVA: 0x0003FC84 File Offset: 0x0003DE84
		private static Stream OpenRead(Uri resourceUri)
		{
			Stream result = null;
			try
			{
				result = new WebClient
				{
					Credentials = CredentialCache.DefaultCredentials
				}.OpenRead(resourceUri.ToString());
			}
			catch (Exception)
			{
			}
			return result;
		}

		// Token: 0x060010A9 RID: 4265 RVA: 0x0003FCC8 File Offset: 0x0003DEC8
		public RuntimeLicenseContext()
		{
		}

		// Token: 0x060010AA RID: 4266 RVA: 0x0003FCD0 File Offset: 0x0003DED0
		// Note: this type is marked as 'beforefieldinit'.
		static RuntimeLicenseContext()
		{
		}

		// Token: 0x04000E6A RID: 3690
		private static TraceSwitch RuntimeLicenseContextSwitch = new TraceSwitch("RuntimeLicenseContextTrace", "RuntimeLicenseContext tracing");

		// Token: 0x04000E6B RID: 3691
		private const int ReadBlock = 400;

		// Token: 0x04000E6C RID: 3692
		internal Hashtable savedLicenseKeys;
	}
}
