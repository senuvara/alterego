﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	/// <summary>Represents a Windows menu or toolbar command item.</summary>
	// Token: 0x02000238 RID: 568
	[ComVisible(true)]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public class MenuCommand
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.MenuCommand" /> class.</summary>
		/// <param name="handler">The event to raise when the user selects the menu item or toolbar button. </param>
		/// <param name="command">The unique command ID that links this menu command to the environment's menu. </param>
		// Token: 0x06001151 RID: 4433 RVA: 0x0003FE7E File Offset: 0x0003E07E
		public MenuCommand(EventHandler handler, CommandID command)
		{
			this.execHandler = handler;
			this.commandID = command;
			this.status = 3;
		}

		/// <summary>Gets or sets a value indicating whether this menu item is checked.</summary>
		/// <returns>
		///     <see langword="true" /> if the item is checked; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000389 RID: 905
		// (get) Token: 0x06001152 RID: 4434 RVA: 0x0003FE9B File Offset: 0x0003E09B
		// (set) Token: 0x06001153 RID: 4435 RVA: 0x0003FEA8 File Offset: 0x0003E0A8
		public virtual bool Checked
		{
			get
			{
				return (this.status & 4) != 0;
			}
			set
			{
				this.SetStatus(4, value);
			}
		}

		/// <summary>Gets a value indicating whether this menu item is available.</summary>
		/// <returns>
		///     <see langword="true" /> if the item is enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700038A RID: 906
		// (get) Token: 0x06001154 RID: 4436 RVA: 0x0003FEB2 File Offset: 0x0003E0B2
		// (set) Token: 0x06001155 RID: 4437 RVA: 0x0003FEBF File Offset: 0x0003E0BF
		public virtual bool Enabled
		{
			get
			{
				return (this.status & 2) != 0;
			}
			set
			{
				this.SetStatus(2, value);
			}
		}

		// Token: 0x06001156 RID: 4438 RVA: 0x0003FECC File Offset: 0x0003E0CC
		private void SetStatus(int mask, bool value)
		{
			int num = this.status;
			if (value)
			{
				num |= mask;
			}
			else
			{
				num &= ~mask;
			}
			if (num != this.status)
			{
				this.status = num;
				this.OnCommandChanged(EventArgs.Empty);
			}
		}

		/// <summary>Gets the public properties associated with the <see cref="T:System.ComponentModel.Design.MenuCommand" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IDictionary" /> containing the public properties of the <see cref="T:System.ComponentModel.Design.MenuCommand" />. </returns>
		// Token: 0x1700038B RID: 907
		// (get) Token: 0x06001157 RID: 4439 RVA: 0x0003FF09 File Offset: 0x0003E109
		public virtual IDictionary Properties
		{
			get
			{
				if (this.properties == null)
				{
					this.properties = new HybridDictionary();
				}
				return this.properties;
			}
		}

		/// <summary>Gets or sets a value indicating whether this menu item is supported.</summary>
		/// <returns>
		///     <see langword="true" /> if the item is supported, which is the default; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700038C RID: 908
		// (get) Token: 0x06001158 RID: 4440 RVA: 0x0003FF24 File Offset: 0x0003E124
		// (set) Token: 0x06001159 RID: 4441 RVA: 0x0003FF31 File Offset: 0x0003E131
		public virtual bool Supported
		{
			get
			{
				return (this.status & 1) != 0;
			}
			set
			{
				this.SetStatus(1, value);
			}
		}

		/// <summary>Gets or sets a value indicating whether this menu item is visible.</summary>
		/// <returns>
		///     <see langword="true" /> if the item is visible; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700038D RID: 909
		// (get) Token: 0x0600115A RID: 4442 RVA: 0x0003FF3B File Offset: 0x0003E13B
		// (set) Token: 0x0600115B RID: 4443 RVA: 0x0003FF49 File Offset: 0x0003E149
		public virtual bool Visible
		{
			get
			{
				return (this.status & 16) == 0;
			}
			set
			{
				this.SetStatus(16, !value);
			}
		}

		/// <summary>Occurs when the menu command changes.</summary>
		// Token: 0x14000028 RID: 40
		// (add) Token: 0x0600115C RID: 4444 RVA: 0x0003FF57 File Offset: 0x0003E157
		// (remove) Token: 0x0600115D RID: 4445 RVA: 0x0003FF70 File Offset: 0x0003E170
		public event EventHandler CommandChanged
		{
			add
			{
				this.statusHandler = (EventHandler)Delegate.Combine(this.statusHandler, value);
			}
			remove
			{
				this.statusHandler = (EventHandler)Delegate.Remove(this.statusHandler, value);
			}
		}

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> associated with this menu command.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.CommandID" /> associated with the menu command.</returns>
		// Token: 0x1700038E RID: 910
		// (get) Token: 0x0600115E RID: 4446 RVA: 0x0003FF89 File Offset: 0x0003E189
		public virtual CommandID CommandID
		{
			get
			{
				return this.commandID;
			}
		}

		/// <summary>Invokes the command.</summary>
		// Token: 0x0600115F RID: 4447 RVA: 0x0003FF94 File Offset: 0x0003E194
		public virtual void Invoke()
		{
			if (this.execHandler != null)
			{
				try
				{
					this.execHandler(this, EventArgs.Empty);
				}
				catch (CheckoutException ex)
				{
					if (ex != CheckoutException.Canceled)
					{
						throw;
					}
				}
			}
		}

		/// <summary>Invokes the command with the given parameter.</summary>
		/// <param name="arg">An optional argument for use by the command.</param>
		// Token: 0x06001160 RID: 4448 RVA: 0x0003FFD8 File Offset: 0x0003E1D8
		public virtual void Invoke(object arg)
		{
			this.Invoke();
		}

		/// <summary>Gets the OLE command status code for this menu item.</summary>
		/// <returns>An integer containing a mixture of status flags that reflect the state of this menu item.</returns>
		// Token: 0x1700038F RID: 911
		// (get) Token: 0x06001161 RID: 4449 RVA: 0x0003FFE0 File Offset: 0x0003E1E0
		public virtual int OleStatus
		{
			get
			{
				return this.status;
			}
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.Design.MenuCommand.CommandChanged" /> event.</summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data. </param>
		// Token: 0x06001162 RID: 4450 RVA: 0x0003FFE8 File Offset: 0x0003E1E8
		protected virtual void OnCommandChanged(EventArgs e)
		{
			if (this.statusHandler != null)
			{
				this.statusHandler(this, e);
			}
		}

		/// <summary>Returns a string representation of this menu command.</summary>
		/// <returns>A string containing the value of the <see cref="P:System.ComponentModel.Design.MenuCommand.CommandID" /> property appended with the names of any flags that are set, separated by pipe bars (|). These flag properties include <see cref="P:System.ComponentModel.Design.MenuCommand.Checked" />, <see cref="P:System.ComponentModel.Design.MenuCommand.Enabled" />, <see cref="P:System.ComponentModel.Design.MenuCommand.Supported" />, and <see cref="P:System.ComponentModel.Design.MenuCommand.Visible" />.</returns>
		// Token: 0x06001163 RID: 4451 RVA: 0x00040000 File Offset: 0x0003E200
		public override string ToString()
		{
			string text = this.CommandID.ToString() + " : ";
			if ((this.status & 1) != 0)
			{
				text += "Supported";
			}
			if ((this.status & 2) != 0)
			{
				text += "|Enabled";
			}
			if ((this.status & 16) == 0)
			{
				text += "|Visible";
			}
			if ((this.status & 4) != 0)
			{
				text += "|Checked";
			}
			return text;
		}

		// Token: 0x04000E7A RID: 3706
		private EventHandler execHandler;

		// Token: 0x04000E7B RID: 3707
		private EventHandler statusHandler;

		// Token: 0x04000E7C RID: 3708
		private CommandID commandID;

		// Token: 0x04000E7D RID: 3709
		private int status;

		// Token: 0x04000E7E RID: 3710
		private IDictionary properties;

		// Token: 0x04000E7F RID: 3711
		private const int ENABLED = 2;

		// Token: 0x04000E80 RID: 3712
		private const int INVISIBLE = 16;

		// Token: 0x04000E81 RID: 3713
		private const int CHECKED = 4;

		// Token: 0x04000E82 RID: 3714
		private const int SUPPORTED = 1;
	}
}
