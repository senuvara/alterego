﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	/// <summary>Provides an interface for managing designer transactions and components.</summary>
	// Token: 0x02000225 RID: 549
	[ComVisible(true)]
	public interface IDesignerHost : IServiceContainer, IServiceProvider
	{
		/// <summary>Gets a value indicating whether the designer host is currently loading the document.</summary>
		/// <returns>
		///     <see langword="true" /> if the designer host is currently loading the document; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700037C RID: 892
		// (get) Token: 0x060010ED RID: 4333
		bool Loading { get; }

		/// <summary>Gets a value indicating whether the designer host is currently in a transaction.</summary>
		/// <returns>
		///     <see langword="true" /> if a transaction is in progress; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700037D RID: 893
		// (get) Token: 0x060010EE RID: 4334
		bool InTransaction { get; }

		/// <summary>Gets the container for this designer host.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.IContainer" /> for this host.</returns>
		// Token: 0x1700037E RID: 894
		// (get) Token: 0x060010EF RID: 4335
		IContainer Container { get; }

		/// <summary>Gets the instance of the base class used as the root component for the current design.</summary>
		/// <returns>The instance of the root component class.</returns>
		// Token: 0x1700037F RID: 895
		// (get) Token: 0x060010F0 RID: 4336
		IComponent RootComponent { get; }

		/// <summary>Gets the fully qualified name of the class being designed.</summary>
		/// <returns>The fully qualified name of the base component class.</returns>
		// Token: 0x17000380 RID: 896
		// (get) Token: 0x060010F1 RID: 4337
		string RootComponentClassName { get; }

		/// <summary>Gets the description of the current transaction.</summary>
		/// <returns>A description of the current transaction.</returns>
		// Token: 0x17000381 RID: 897
		// (get) Token: 0x060010F2 RID: 4338
		string TransactionDescription { get; }

		/// <summary>Occurs when this designer is activated.</summary>
		// Token: 0x1400001F RID: 31
		// (add) Token: 0x060010F3 RID: 4339
		// (remove) Token: 0x060010F4 RID: 4340
		event EventHandler Activated;

		/// <summary>Occurs when this designer is deactivated.</summary>
		// Token: 0x14000020 RID: 32
		// (add) Token: 0x060010F5 RID: 4341
		// (remove) Token: 0x060010F6 RID: 4342
		event EventHandler Deactivated;

		/// <summary>Occurs when this designer completes loading its document.</summary>
		// Token: 0x14000021 RID: 33
		// (add) Token: 0x060010F7 RID: 4343
		// (remove) Token: 0x060010F8 RID: 4344
		event EventHandler LoadComplete;

		/// <summary>Adds an event handler for the <see cref="E:System.ComponentModel.Design.IDesignerHost.TransactionClosed" /> event.</summary>
		// Token: 0x14000022 RID: 34
		// (add) Token: 0x060010F9 RID: 4345
		// (remove) Token: 0x060010FA RID: 4346
		event DesignerTransactionCloseEventHandler TransactionClosed;

		/// <summary>Adds an event handler for the <see cref="E:System.ComponentModel.Design.IDesignerHost.TransactionClosing" /> event.</summary>
		// Token: 0x14000023 RID: 35
		// (add) Token: 0x060010FB RID: 4347
		// (remove) Token: 0x060010FC RID: 4348
		event DesignerTransactionCloseEventHandler TransactionClosing;

		/// <summary>Adds an event handler for the <see cref="E:System.ComponentModel.Design.IDesignerHost.TransactionOpened" /> event.</summary>
		// Token: 0x14000024 RID: 36
		// (add) Token: 0x060010FD RID: 4349
		// (remove) Token: 0x060010FE RID: 4350
		event EventHandler TransactionOpened;

		/// <summary>Adds an event handler for the <see cref="E:System.ComponentModel.Design.IDesignerHost.TransactionOpening" /> event.</summary>
		// Token: 0x14000025 RID: 37
		// (add) Token: 0x060010FF RID: 4351
		// (remove) Token: 0x06001100 RID: 4352
		event EventHandler TransactionOpening;

		/// <summary>Activates the designer that this host is hosting.</summary>
		// Token: 0x06001101 RID: 4353
		void Activate();

		/// <summary>Creates a component of the specified type and adds it to the design document.</summary>
		/// <param name="componentClass">The type of the component to create. </param>
		/// <returns>The newly created component.</returns>
		// Token: 0x06001102 RID: 4354
		IComponent CreateComponent(Type componentClass);

		/// <summary>Creates a component of the specified type and name, and adds it to the design document.</summary>
		/// <param name="componentClass">The type of the component to create. </param>
		/// <param name="name">The name for the component. </param>
		/// <returns>The newly created component.</returns>
		// Token: 0x06001103 RID: 4355
		IComponent CreateComponent(Type componentClass, string name);

		/// <summary>Creates a <see cref="T:System.ComponentModel.Design.DesignerTransaction" /> that can encapsulate event sequences to improve performance and enable undo and redo support functionality.</summary>
		/// <returns>A new instance of <see cref="T:System.ComponentModel.Design.DesignerTransaction" />. When you complete the steps in your transaction, you should call <see cref="M:System.ComponentModel.Design.DesignerTransaction.Commit" /> on this object.</returns>
		// Token: 0x06001104 RID: 4356
		DesignerTransaction CreateTransaction();

		/// <summary>Creates a <see cref="T:System.ComponentModel.Design.DesignerTransaction" /> that can encapsulate event sequences to improve performance and enable undo and redo support functionality, using the specified transaction description.</summary>
		/// <param name="description">A title or description for the newly created transaction. </param>
		/// <returns>A new <see cref="T:System.ComponentModel.Design.DesignerTransaction" />. When you have completed the steps in your transaction, you should call <see cref="M:System.ComponentModel.Design.DesignerTransaction.Commit" /> on this object.</returns>
		// Token: 0x06001105 RID: 4357
		DesignerTransaction CreateTransaction(string description);

		/// <summary>Destroys the specified component and removes it from the designer container.</summary>
		/// <param name="component">The component to destroy. </param>
		// Token: 0x06001106 RID: 4358
		void DestroyComponent(IComponent component);

		/// <summary>Gets the designer instance that contains the specified component.</summary>
		/// <param name="component">The <see cref="T:System.ComponentModel.IComponent" /> to retrieve the designer for. </param>
		/// <returns>An <see cref="T:System.ComponentModel.Design.IDesigner" />, or <see langword="null" /> if there is no designer for the specified component.</returns>
		// Token: 0x06001107 RID: 4359
		IDesigner GetDesigner(IComponent component);

		/// <summary>Gets an instance of the specified, fully qualified type name.</summary>
		/// <param name="typeName">The name of the type to load. </param>
		/// <returns>The type object for the specified type name, or <see langword="null" /> if the type cannot be found.</returns>
		// Token: 0x06001108 RID: 4360
		Type GetType(string typeName);
	}
}
