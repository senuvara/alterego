﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	/// <summary>Provides a way to group a series of design-time actions to improve performance and enable most types of changes to be undone.</summary>
	// Token: 0x02000211 RID: 529
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public abstract class DesignerTransaction : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.DesignerTransaction" /> class with no description.</summary>
		// Token: 0x06001076 RID: 4214 RVA: 0x0003F62A File Offset: 0x0003D82A
		protected DesignerTransaction() : this("")
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.DesignerTransaction" /> class using the specified transaction description.</summary>
		/// <param name="description">A description for this transaction. </param>
		// Token: 0x06001077 RID: 4215 RVA: 0x0003F637 File Offset: 0x0003D837
		protected DesignerTransaction(string description)
		{
			this.desc = description;
		}

		/// <summary>Gets a value indicating whether the transaction was canceled.</summary>
		/// <returns>
		///     <see langword="true" /> if the transaction was canceled; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000368 RID: 872
		// (get) Token: 0x06001078 RID: 4216 RVA: 0x0003F646 File Offset: 0x0003D846
		public bool Canceled
		{
			get
			{
				return this.canceled;
			}
		}

		/// <summary>Gets a value indicating whether the transaction was committed.</summary>
		/// <returns>
		///     <see langword="true" /> if the transaction was committed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000369 RID: 873
		// (get) Token: 0x06001079 RID: 4217 RVA: 0x0003F64E File Offset: 0x0003D84E
		public bool Committed
		{
			get
			{
				return this.committed;
			}
		}

		/// <summary>Gets a description for the transaction.</summary>
		/// <returns>A description for the transaction.</returns>
		// Token: 0x1700036A RID: 874
		// (get) Token: 0x0600107A RID: 4218 RVA: 0x0003F656 File Offset: 0x0003D856
		public string Description
		{
			get
			{
				return this.desc;
			}
		}

		/// <summary>Cancels the transaction and attempts to roll back the changes made by the events of the transaction.</summary>
		// Token: 0x0600107B RID: 4219 RVA: 0x0003F65E File Offset: 0x0003D85E
		public void Cancel()
		{
			if (!this.canceled && !this.committed)
			{
				this.canceled = true;
				GC.SuppressFinalize(this);
				this.suppressedFinalization = true;
				this.OnCancel();
			}
		}

		/// <summary>Commits this transaction.</summary>
		// Token: 0x0600107C RID: 4220 RVA: 0x0003F68A File Offset: 0x0003D88A
		public void Commit()
		{
			if (!this.committed && !this.canceled)
			{
				this.committed = true;
				GC.SuppressFinalize(this);
				this.suppressedFinalization = true;
				this.OnCommit();
			}
		}

		/// <summary>Raises the <see langword="Cancel" /> event.</summary>
		// Token: 0x0600107D RID: 4221
		protected abstract void OnCancel();

		/// <summary>Performs the actual work of committing a transaction.</summary>
		// Token: 0x0600107E RID: 4222
		protected abstract void OnCommit();

		/// <summary>Releases the resources associated with this object. This override commits this transaction if it was not already committed.</summary>
		// Token: 0x0600107F RID: 4223 RVA: 0x0003F6B8 File Offset: 0x0003D8B8
		~DesignerTransaction()
		{
			this.Dispose(false);
		}

		/// <summary>Releases all resources used by the <see cref="T:System.ComponentModel.Design.DesignerTransaction" />. </summary>
		// Token: 0x06001080 RID: 4224 RVA: 0x0003F6E8 File Offset: 0x0003D8E8
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			if (!this.suppressedFinalization)
			{
				GC.SuppressFinalize(this);
			}
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Design.DesignerTransaction" /> and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06001081 RID: 4225 RVA: 0x0003F6FF File Offset: 0x0003D8FF
		protected virtual void Dispose(bool disposing)
		{
			this.Cancel();
		}

		// Token: 0x04000E63 RID: 3683
		private bool committed;

		// Token: 0x04000E64 RID: 3684
		private bool canceled;

		// Token: 0x04000E65 RID: 3685
		private bool suppressedFinalization;

		// Token: 0x04000E66 RID: 3686
		private string desc;
	}
}
