﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	/// <summary>Provides data for the <see cref="E:System.ComponentModel.Design.IComponentChangeService.ComponentChanged" /> event. This class cannot be inherited.</summary>
	// Token: 0x02000204 RID: 516
	[ComVisible(true)]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public sealed class ComponentChangedEventArgs : EventArgs
	{
		/// <summary>Gets the component that was modified.</summary>
		/// <returns>An <see cref="T:System.Object" /> that represents the component that was modified.</returns>
		// Token: 0x1700034B RID: 843
		// (get) Token: 0x0600101E RID: 4126 RVA: 0x0003EEF2 File Offset: 0x0003D0F2
		public object Component
		{
			get
			{
				return this.component;
			}
		}

		/// <summary>Gets the member that has been changed.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.MemberDescriptor" /> that indicates the member that has been changed.</returns>
		// Token: 0x1700034C RID: 844
		// (get) Token: 0x0600101F RID: 4127 RVA: 0x0003EEFA File Offset: 0x0003D0FA
		public MemberDescriptor Member
		{
			get
			{
				return this.member;
			}
		}

		/// <summary>Gets the new value of the changed member.</summary>
		/// <returns>The new value of the changed member. This property can be <see langword="null" />.</returns>
		// Token: 0x1700034D RID: 845
		// (get) Token: 0x06001020 RID: 4128 RVA: 0x0003EF02 File Offset: 0x0003D102
		public object NewValue
		{
			get
			{
				return this.newValue;
			}
		}

		/// <summary>Gets the old value of the changed member.</summary>
		/// <returns>The old value of the changed member. This property can be <see langword="null" />.</returns>
		// Token: 0x1700034E RID: 846
		// (get) Token: 0x06001021 RID: 4129 RVA: 0x0003EF0A File Offset: 0x0003D10A
		public object OldValue
		{
			get
			{
				return this.oldValue;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.ComponentChangedEventArgs" /> class.</summary>
		/// <param name="component">The component that was changed. </param>
		/// <param name="member">A <see cref="T:System.ComponentModel.MemberDescriptor" /> that represents the member that was changed. </param>
		/// <param name="oldValue">The old value of the changed member. </param>
		/// <param name="newValue">The new value of the changed member. </param>
		// Token: 0x06001022 RID: 4130 RVA: 0x0003EF12 File Offset: 0x0003D112
		public ComponentChangedEventArgs(object component, MemberDescriptor member, object oldValue, object newValue)
		{
			this.component = component;
			this.member = member;
			this.oldValue = oldValue;
			this.newValue = newValue;
		}

		// Token: 0x04000E4F RID: 3663
		private object component;

		// Token: 0x04000E50 RID: 3664
		private MemberDescriptor member;

		// Token: 0x04000E51 RID: 3665
		private object oldValue;

		// Token: 0x04000E52 RID: 3666
		private object newValue;
	}
}
