﻿using System;

namespace System.ComponentModel.Design
{
	/// <summary>Defines identifiers that indicate information about the context in which a request for Help information originated.</summary>
	// Token: 0x0200021C RID: 540
	public enum HelpContextType
	{
		/// <summary>A general context.</summary>
		// Token: 0x04000E70 RID: 3696
		Ambient,
		/// <summary>A window.</summary>
		// Token: 0x04000E71 RID: 3697
		Window,
		/// <summary>A selection.</summary>
		// Token: 0x04000E72 RID: 3698
		Selection,
		/// <summary>A tool window selection.</summary>
		// Token: 0x04000E73 RID: 3699
		ToolWindowSelection
	}
}
