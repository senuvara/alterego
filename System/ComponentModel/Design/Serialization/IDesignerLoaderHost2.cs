﻿using System;

namespace System.ComponentModel.Design.Serialization
{
	/// <summary>Provides an interface that extends <see cref="T:System.ComponentModel.Design.Serialization.IDesignerLoaderHost" /> to specify whether errors are tolerated while loading a design document.</summary>
	// Token: 0x02000249 RID: 585
	public interface IDesignerLoaderHost2 : IDesignerLoaderHost, IDesignerHost, IServiceContainer, IServiceProvider
	{
		/// <summary>Gets or sets a value indicating whether errors should be ignored when <see cref="M:System.ComponentModel.Design.Serialization.IDesignerLoaderHost.Reload" /> is called.</summary>
		/// <returns>
		///     <see langword="true" /> if the designer loader will ignore errors when it reloads; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700039B RID: 923
		// (get) Token: 0x060011A4 RID: 4516
		// (set) Token: 0x060011A5 RID: 4517
		bool IgnoreErrorsDuringReload { get; set; }

		/// <summary>Gets or sets a value indicating whether it is possible to reload with errors. </summary>
		/// <returns>
		///     <see langword="true" /> if the designer loader can reload the design document when errors are detected; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700039C RID: 924
		// (get) Token: 0x060011A6 RID: 4518
		// (set) Token: 0x060011A7 RID: 4519
		bool CanReloadWithErrors { get; set; }
	}
}
