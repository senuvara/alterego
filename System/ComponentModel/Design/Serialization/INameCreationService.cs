﻿using System;

namespace System.ComponentModel.Design.Serialization
{
	/// <summary>Provides a service that can generate unique names for objects.</summary>
	// Token: 0x0200024E RID: 590
	public interface INameCreationService
	{
		/// <summary>Creates a new name that is unique to all components in the specified container.</summary>
		/// <param name="container">The container where the new object is added. </param>
		/// <param name="dataType">The data type of the object that receives the name. </param>
		/// <returns>A unique name for the data type.</returns>
		// Token: 0x060011BD RID: 4541
		string CreateName(IContainer container, Type dataType);

		/// <summary>Gets a value indicating whether the specified name is valid.</summary>
		/// <param name="name">The name to validate. </param>
		/// <returns>
		///     <see langword="true" /> if the name is valid; otherwise, <see langword="false" />.</returns>
		// Token: 0x060011BE RID: 4542
		bool IsValidName(string name);

		/// <summary>Gets a value indicating whether the specified name is valid.</summary>
		/// <param name="name">The name to validate. </param>
		// Token: 0x060011BF RID: 4543
		void ValidateName(string name);
	}
}
