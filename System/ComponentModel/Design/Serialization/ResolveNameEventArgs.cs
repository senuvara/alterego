﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel.Design.Serialization
{
	/// <summary>Provides data for the <see cref="E:System.ComponentModel.Design.Serialization.IDesignerSerializationManager.ResolveName" /> event.</summary>
	// Token: 0x02000253 RID: 595
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public class ResolveNameEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.Serialization.ResolveNameEventArgs" /> class.</summary>
		/// <param name="name">The name to resolve. </param>
		// Token: 0x060011DC RID: 4572 RVA: 0x000411BC File Offset: 0x0003F3BC
		public ResolveNameEventArgs(string name)
		{
			this.name = name;
			this.value = null;
		}

		/// <summary>Gets the name of the object to resolve.</summary>
		/// <returns>The name of the object to resolve.</returns>
		// Token: 0x170003A7 RID: 935
		// (get) Token: 0x060011DD RID: 4573 RVA: 0x000411D2 File Offset: 0x0003F3D2
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		/// <summary>Gets or sets the object that matches the name.</summary>
		/// <returns>The object that the name is associated with.</returns>
		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x060011DE RID: 4574 RVA: 0x000411DA File Offset: 0x0003F3DA
		// (set) Token: 0x060011DF RID: 4575 RVA: 0x000411E2 File Offset: 0x0003F3E2
		public object Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		// Token: 0x04001240 RID: 4672
		private string name;

		// Token: 0x04001241 RID: 4673
		private object value;
	}
}
