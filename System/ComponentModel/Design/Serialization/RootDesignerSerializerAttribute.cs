﻿using System;

namespace System.ComponentModel.Design.Serialization
{
	/// <summary>Indicates the base serializer to use for a root designer object. This class cannot be inherited.</summary>
	// Token: 0x02000255 RID: 597
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, AllowMultiple = true, Inherited = true)]
	[Obsolete("This attribute has been deprecated. Use DesignerSerializerAttribute instead.  For example, to specify a root designer for CodeDom, use DesignerSerializerAttribute(...,typeof(TypeCodeDomSerializer)).  http://go.microsoft.com/fwlink/?linkid=14202")]
	public sealed class RootDesignerSerializerAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute" /> class using the specified attributes.</summary>
		/// <param name="serializerType">The data type of the serializer. </param>
		/// <param name="baseSerializerType">The base type of the serializer. A class can include multiple serializers as they all have different base types. </param>
		/// <param name="reloadable">
		///       <see langword="true" /> if this serializer supports dynamic reloading of the document; otherwise, <see langword="false" />. </param>
		// Token: 0x060011E4 RID: 4580 RVA: 0x000411EB File Offset: 0x0003F3EB
		public RootDesignerSerializerAttribute(Type serializerType, Type baseSerializerType, bool reloadable)
		{
			this.serializerTypeName = serializerType.AssemblyQualifiedName;
			this.serializerBaseTypeName = baseSerializerType.AssemblyQualifiedName;
			this.reloadable = reloadable;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute" /> class using the specified attributes.</summary>
		/// <param name="serializerTypeName">The fully qualified name of the data type of the serializer. </param>
		/// <param name="baseSerializerType">The name of the base type of the serializer. A class can include multiple serializers, as they all have different base types. </param>
		/// <param name="reloadable">
		///       <see langword="true" /> if this serializer supports dynamic reloading of the document; otherwise, <see langword="false" />. </param>
		// Token: 0x060011E5 RID: 4581 RVA: 0x00041212 File Offset: 0x0003F412
		public RootDesignerSerializerAttribute(string serializerTypeName, Type baseSerializerType, bool reloadable)
		{
			this.serializerTypeName = serializerTypeName;
			this.serializerBaseTypeName = baseSerializerType.AssemblyQualifiedName;
			this.reloadable = reloadable;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.Serialization.RootDesignerSerializerAttribute" /> class using the specified attributes.</summary>
		/// <param name="serializerTypeName">The fully qualified name of the data type of the serializer. </param>
		/// <param name="baseSerializerTypeName">The name of the base type of the serializer. A class can include multiple serializers as they all have different base types. </param>
		/// <param name="reloadable">
		///       <see langword="true" /> if this serializer supports dynamic reloading of the document; otherwise, <see langword="false" />. </param>
		// Token: 0x060011E6 RID: 4582 RVA: 0x00041234 File Offset: 0x0003F434
		public RootDesignerSerializerAttribute(string serializerTypeName, string baseSerializerTypeName, bool reloadable)
		{
			this.serializerTypeName = serializerTypeName;
			this.serializerBaseTypeName = baseSerializerTypeName;
			this.reloadable = reloadable;
		}

		/// <summary>Gets a value indicating whether the root serializer supports reloading of the design document without first disposing the designer host.</summary>
		/// <returns>
		///     <see langword="true" /> if the root serializer supports reloading; otherwise, <see langword="false" />.</returns>
		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x060011E7 RID: 4583 RVA: 0x00041251 File Offset: 0x0003F451
		public bool Reloadable
		{
			get
			{
				return this.reloadable;
			}
		}

		/// <summary>Gets the fully qualified type name of the serializer.</summary>
		/// <returns>The name of the type of the serializer.</returns>
		// Token: 0x170003AA RID: 938
		// (get) Token: 0x060011E8 RID: 4584 RVA: 0x00041259 File Offset: 0x0003F459
		public string SerializerTypeName
		{
			get
			{
				return this.serializerTypeName;
			}
		}

		/// <summary>Gets the fully qualified type name of the base type of the serializer.</summary>
		/// <returns>The name of the base type of the serializer.</returns>
		// Token: 0x170003AB RID: 939
		// (get) Token: 0x060011E9 RID: 4585 RVA: 0x00041261 File Offset: 0x0003F461
		public string SerializerBaseTypeName
		{
			get
			{
				return this.serializerBaseTypeName;
			}
		}

		/// <summary>Gets a unique ID for this attribute type.</summary>
		/// <returns>An object containing a unique ID for this attribute type.</returns>
		// Token: 0x170003AC RID: 940
		// (get) Token: 0x060011EA RID: 4586 RVA: 0x0004126C File Offset: 0x0003F46C
		public override object TypeId
		{
			get
			{
				if (this.typeId == null)
				{
					string text = this.serializerBaseTypeName;
					int num = text.IndexOf(',');
					if (num != -1)
					{
						text = text.Substring(0, num);
					}
					this.typeId = base.GetType().FullName + text;
				}
				return this.typeId;
			}
		}

		// Token: 0x04001242 RID: 4674
		private bool reloadable;

		// Token: 0x04001243 RID: 4675
		private string serializerTypeName;

		// Token: 0x04001244 RID: 4676
		private string serializerBaseTypeName;

		// Token: 0x04001245 RID: 4677
		private string typeId;
	}
}
