﻿using System;
using System.Collections;
using System.Security.Permissions;

namespace System.ComponentModel.Design.Serialization
{
	/// <summary>Provides a stack object that can be used by a serializer to make information available to nested serializers.</summary>
	// Token: 0x02000244 RID: 580
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public sealed class ContextStack
	{
		/// <summary>Gets the current object on the stack.</summary>
		/// <returns>The current object on the stack, or <see langword="null" /> if no objects were pushed.</returns>
		// Token: 0x17000393 RID: 915
		// (get) Token: 0x0600118D RID: 4493 RVA: 0x00040919 File Offset: 0x0003EB19
		public object Current
		{
			get
			{
				if (this.contextStack != null && this.contextStack.Count > 0)
				{
					return this.contextStack[this.contextStack.Count - 1];
				}
				return null;
			}
		}

		/// <summary>Gets the object on the stack at the specified level.</summary>
		/// <param name="level">The level of the object to retrieve on the stack. Level 0 is the top of the stack, level 1 is the next down, and so on. This level must be 0 or greater. If level is greater than the number of levels on the stack, it returns <see langword="null" />. </param>
		/// <returns>The object on the stack at the specified level, or <see langword="null" /> if no object exists at that level.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="level" /> is less than 0.</exception>
		// Token: 0x17000394 RID: 916
		public object this[int level]
		{
			get
			{
				if (level < 0)
				{
					throw new ArgumentOutOfRangeException("level");
				}
				if (this.contextStack != null && level < this.contextStack.Count)
				{
					return this.contextStack[this.contextStack.Count - 1 - level];
				}
				return null;
			}
		}

		/// <summary>Gets the first object on the stack that inherits from or implements the specified type.</summary>
		/// <param name="type">A type to retrieve from the context stack. </param>
		/// <returns>The first object on the stack that inherits from or implements the specified type, or <see langword="null" /> if no object on the stack implements the type.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> is <see langword="null" />.</exception>
		// Token: 0x17000395 RID: 917
		public object this[Type type]
		{
			get
			{
				if (type == null)
				{
					throw new ArgumentNullException("type");
				}
				if (this.contextStack != null)
				{
					int i = this.contextStack.Count;
					while (i > 0)
					{
						object obj = this.contextStack[--i];
						if (type.IsInstanceOfType(obj))
						{
							return obj;
						}
					}
				}
				return null;
			}
		}

		/// <summary>Appends an object to the end of the stack, rather than pushing it onto the top of the stack.</summary>
		/// <param name="context">A context object to append to the stack.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="context" /> is <see langword="null" />.</exception>
		// Token: 0x06001190 RID: 4496 RVA: 0x000409F4 File Offset: 0x0003EBF4
		public void Append(object context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			if (this.contextStack == null)
			{
				this.contextStack = new ArrayList();
			}
			this.contextStack.Insert(0, context);
		}

		/// <summary>Removes the current object off of the stack, returning its value.</summary>
		/// <returns>The object removed from the stack; <see langword="null" /> if no objects are on the stack.</returns>
		// Token: 0x06001191 RID: 4497 RVA: 0x00040A24 File Offset: 0x0003EC24
		public object Pop()
		{
			object result = null;
			if (this.contextStack != null && this.contextStack.Count > 0)
			{
				int index = this.contextStack.Count - 1;
				result = this.contextStack[index];
				this.contextStack.RemoveAt(index);
			}
			return result;
		}

		/// <summary>Pushes, or places, the specified object onto the stack.</summary>
		/// <param name="context">The context object to push onto the stack. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="context" /> is <see langword="null" />.</exception>
		// Token: 0x06001192 RID: 4498 RVA: 0x00040A71 File Offset: 0x0003EC71
		public void Push(object context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			if (this.contextStack == null)
			{
				this.contextStack = new ArrayList();
			}
			this.contextStack.Add(context);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.Serialization.ContextStack" /> class. </summary>
		// Token: 0x06001193 RID: 4499 RVA: 0x0000232F File Offset: 0x0000052F
		public ContextStack()
		{
		}

		// Token: 0x04001231 RID: 4657
		private ArrayList contextStack;
	}
}
