﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.ComponentModel.Design.Serialization
{
	/// <summary>Provides a basic designer loader interface that can be used to implement a custom designer loader.</summary>
	// Token: 0x02000246 RID: 582
	[ComVisible(true)]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public abstract class DesignerLoader
	{
		/// <summary>Gets a value indicating whether the loader is currently loading a document.</summary>
		/// <returns>
		///     <see langword="true" /> if the loader is currently loading a document; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000397 RID: 919
		// (get) Token: 0x06001197 RID: 4503 RVA: 0x00005AFA File Offset: 0x00003CFA
		public virtual bool Loading
		{
			get
			{
				return false;
			}
		}

		/// <summary>Begins loading a designer.</summary>
		/// <param name="host">The loader host through which this loader loads components. </param>
		// Token: 0x06001198 RID: 4504
		public abstract void BeginLoad(IDesignerLoaderHost host);

		/// <summary>Releases all resources used by the <see cref="T:System.ComponentModel.Design.Serialization.DesignerLoader" />.</summary>
		// Token: 0x06001199 RID: 4505
		public abstract void Dispose();

		/// <summary>Writes cached changes to the location that the designer was loaded from.</summary>
		// Token: 0x0600119A RID: 4506 RVA: 0x0000232D File Offset: 0x0000052D
		public virtual void Flush()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.Serialization.DesignerLoader" /> class. </summary>
		// Token: 0x0600119B RID: 4507 RVA: 0x0000232F File Offset: 0x0000052F
		protected DesignerLoader()
		{
		}
	}
}
