﻿using System;

namespace System.ComponentModel.Design
{
	/// <summary>Specifies methods for the designer host to report on the state of transactions.</summary>
	// Token: 0x02000226 RID: 550
	public interface IDesignerHostTransactionState
	{
		/// <summary>Gets a value indicating whether the designer host is closing a transaction. </summary>
		/// <returns>
		///     <see langword="true" /> if the designer is closing a transaction; otherwise, <see langword="false" />. </returns>
		// Token: 0x17000382 RID: 898
		// (get) Token: 0x06001109 RID: 4361
		bool IsClosingTransaction { get; }
	}
}
