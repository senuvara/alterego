﻿using System;
using System.Collections;
using System.Globalization;
using System.Security.Permissions;
using Unity;

namespace System.ComponentModel.Design
{
	/// <summary>Provides a base class for getting and setting option values for a designer.</summary>
	// Token: 0x0200020C RID: 524
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public abstract class DesignerOptionService : IDesignerOptionService
	{
		/// <summary>Gets the options collection for this service.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" /> populated with available designer options.</returns>
		// Token: 0x17000355 RID: 853
		// (get) Token: 0x0600103C RID: 4156 RVA: 0x0003EFA9 File Offset: 0x0003D1A9
		public DesignerOptionService.DesignerOptionCollection Options
		{
			get
			{
				if (this._options == null)
				{
					this._options = new DesignerOptionService.DesignerOptionCollection(this, null, string.Empty, null);
				}
				return this._options;
			}
		}

		/// <summary>Creates a new <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" /> with the given name and adds it to the given parent. </summary>
		/// <param name="parent">The parent designer option collection. All collections have a parent except the root object collection.</param>
		/// <param name="name">The name of this collection.</param>
		/// <param name="value">The object providing properties for this collection. Can be <see langword="null" /> if the collection should not provide any properties.</param>
		/// <returns>A new <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" /> with the given name. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="parent" /> or <paramref name="name" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is an empty string.</exception>
		// Token: 0x0600103D RID: 4157 RVA: 0x0003EFCC File Offset: 0x0003D1CC
		protected DesignerOptionService.DesignerOptionCollection CreateOptionCollection(DesignerOptionService.DesignerOptionCollection parent, string name, object value)
		{
			if (parent == null)
			{
				throw new ArgumentNullException("parent");
			}
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException(SR.GetString("'{1}' is not a valid value for '{0}'.", new object[]
				{
					name.Length.ToString(CultureInfo.CurrentCulture),
					0.ToString(CultureInfo.CurrentCulture)
				}), "name.Length");
			}
			return new DesignerOptionService.DesignerOptionCollection(this, parent, name, value);
		}

		// Token: 0x0600103E RID: 4158 RVA: 0x0003F048 File Offset: 0x0003D248
		private PropertyDescriptor GetOptionProperty(string pageName, string valueName)
		{
			if (pageName == null)
			{
				throw new ArgumentNullException("pageName");
			}
			if (valueName == null)
			{
				throw new ArgumentNullException("valueName");
			}
			string[] array = pageName.Split(new char[]
			{
				'\\'
			});
			DesignerOptionService.DesignerOptionCollection designerOptionCollection = this.Options;
			foreach (string name in array)
			{
				designerOptionCollection = designerOptionCollection[name];
				if (designerOptionCollection == null)
				{
					return null;
				}
			}
			return designerOptionCollection.Properties[valueName];
		}

		/// <summary>Populates a <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" />.</summary>
		/// <param name="options">The collection to populate.</param>
		// Token: 0x0600103F RID: 4159 RVA: 0x0000232D File Offset: 0x0000052D
		protected virtual void PopulateOptionCollection(DesignerOptionService.DesignerOptionCollection options)
		{
		}

		/// <summary>Shows the options dialog box for the given object.</summary>
		/// <param name="options">The options collection containing the object to be invoked.</param>
		/// <param name="optionObject">The actual options object.</param>
		/// <returns>
		///     <see langword="true" /> if the dialog box is shown; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001040 RID: 4160 RVA: 0x00005AFA File Offset: 0x00003CFA
		protected virtual bool ShowDialog(DesignerOptionService.DesignerOptionCollection options, object optionObject)
		{
			return false;
		}

		/// <summary>Gets the value of an option defined in this package.</summary>
		/// <param name="pageName">The page to which the option is bound.</param>
		/// <param name="valueName">The name of the option value.</param>
		/// <returns>The value of the option named <paramref name="valueName" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pageName" /> or <paramref name="valueName" /> is <see langword="null" />.</exception>
		// Token: 0x06001041 RID: 4161 RVA: 0x0003F0B8 File Offset: 0x0003D2B8
		object IDesignerOptionService.GetOptionValue(string pageName, string valueName)
		{
			PropertyDescriptor optionProperty = this.GetOptionProperty(pageName, valueName);
			if (optionProperty != null)
			{
				return optionProperty.GetValue(null);
			}
			return null;
		}

		/// <summary>Sets the value of an option defined in this package.</summary>
		/// <param name="pageName">The page to which the option is bound</param>
		/// <param name="valueName">The name of the option value.</param>
		/// <param name="value">The value of the option.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pageName" /> or <paramref name="valueName" /> is <see langword="null" />.</exception>
		// Token: 0x06001042 RID: 4162 RVA: 0x0003F0DC File Offset: 0x0003D2DC
		void IDesignerOptionService.SetOptionValue(string pageName, string valueName, object value)
		{
			PropertyDescriptor optionProperty = this.GetOptionProperty(pageName, valueName);
			if (optionProperty != null)
			{
				optionProperty.SetValue(null, value);
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.DesignerOptionService" /> class. </summary>
		// Token: 0x06001043 RID: 4163 RVA: 0x0000232F File Offset: 0x0000052F
		protected DesignerOptionService()
		{
		}

		// Token: 0x04000E59 RID: 3673
		private DesignerOptionService.DesignerOptionCollection _options;

		/// <summary>Contains a collection of designer options. This class cannot be inherited.</summary>
		// Token: 0x0200020D RID: 525
		[TypeConverter(typeof(DesignerOptionService.DesignerOptionConverter))]
		[Editor("", "System.Drawing.Design.UITypeEditor, System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		public sealed class DesignerOptionCollection : IList, ICollection, IEnumerable
		{
			// Token: 0x06001044 RID: 4164 RVA: 0x0003F100 File Offset: 0x0003D300
			internal DesignerOptionCollection(DesignerOptionService service, DesignerOptionService.DesignerOptionCollection parent, string name, object value)
			{
				this._service = service;
				this._parent = parent;
				this._name = name;
				this._value = value;
				if (this._parent != null)
				{
					if (this._parent._children == null)
					{
						this._parent._children = new ArrayList(1);
					}
					this._parent._children.Add(this);
				}
			}

			/// <summary>Gets the number of child option collections this <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" /> contains.</summary>
			/// <returns>The number of child option collections this <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" /> contains.</returns>
			// Token: 0x17000356 RID: 854
			// (get) Token: 0x06001045 RID: 4165 RVA: 0x0003F168 File Offset: 0x0003D368
			public int Count
			{
				get
				{
					this.EnsurePopulated();
					return this._children.Count;
				}
			}

			/// <summary>Gets the name of this <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" />.</summary>
			/// <returns>The name of this <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" />.</returns>
			// Token: 0x17000357 RID: 855
			// (get) Token: 0x06001046 RID: 4166 RVA: 0x0003F17B File Offset: 0x0003D37B
			public string Name
			{
				get
				{
					return this._name;
				}
			}

			/// <summary>Gets the parent collection object.</summary>
			/// <returns>The parent collection object, or <see langword="null" /> if there is no parent.</returns>
			// Token: 0x17000358 RID: 856
			// (get) Token: 0x06001047 RID: 4167 RVA: 0x0003F183 File Offset: 0x0003D383
			public DesignerOptionService.DesignerOptionCollection Parent
			{
				get
				{
					return this._parent;
				}
			}

			/// <summary>Gets the collection of properties offered by this <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" />, along with all of its children.</summary>
			/// <returns>The collection of properties offered by this <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" />, along with all of its children.</returns>
			// Token: 0x17000359 RID: 857
			// (get) Token: 0x06001048 RID: 4168 RVA: 0x0003F18C File Offset: 0x0003D38C
			public PropertyDescriptorCollection Properties
			{
				get
				{
					if (this._properties == null)
					{
						ArrayList arrayList;
						if (this._value != null)
						{
							PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this._value);
							arrayList = new ArrayList(properties.Count);
							using (IEnumerator enumerator = properties.GetEnumerator())
							{
								while (enumerator.MoveNext())
								{
									object obj = enumerator.Current;
									PropertyDescriptor property = (PropertyDescriptor)obj;
									arrayList.Add(new DesignerOptionService.DesignerOptionCollection.WrappedPropertyDescriptor(property, this._value));
								}
								goto IL_76;
							}
						}
						arrayList = new ArrayList(1);
						IL_76:
						this.EnsurePopulated();
						foreach (object obj2 in this._children)
						{
							DesignerOptionService.DesignerOptionCollection designerOptionCollection = (DesignerOptionService.DesignerOptionCollection)obj2;
							arrayList.AddRange(designerOptionCollection.Properties);
						}
						PropertyDescriptor[] properties2 = (PropertyDescriptor[])arrayList.ToArray(typeof(PropertyDescriptor));
						this._properties = new PropertyDescriptorCollection(properties2, true);
					}
					return this._properties;
				}
			}

			/// <summary>Gets the child collection at the given index.</summary>
			/// <param name="index">The zero-based index of the child collection to get.</param>
			/// <returns>The child collection at the specified index.</returns>
			// Token: 0x1700035A RID: 858
			public DesignerOptionService.DesignerOptionCollection this[int index]
			{
				get
				{
					this.EnsurePopulated();
					if (index < 0 || index >= this._children.Count)
					{
						throw new IndexOutOfRangeException("index");
					}
					return (DesignerOptionService.DesignerOptionCollection)this._children[index];
				}
			}

			/// <summary>Gets the child collection at the given name.</summary>
			/// <param name="name">The name of the child collection.</param>
			/// <returns>The child collection with the name specified by the <paramref name="name" /> parameter, or <see langword="null" /> if the name is not found.</returns>
			// Token: 0x1700035B RID: 859
			public DesignerOptionService.DesignerOptionCollection this[string name]
			{
				get
				{
					this.EnsurePopulated();
					foreach (object obj in this._children)
					{
						DesignerOptionService.DesignerOptionCollection designerOptionCollection = (DesignerOptionService.DesignerOptionCollection)obj;
						if (string.Compare(designerOptionCollection.Name, name, true, CultureInfo.InvariantCulture) == 0)
						{
							return designerOptionCollection;
						}
					}
					return null;
				}
			}

			/// <summary>Copies the entire collection to a compatible one-dimensional <see cref="T:System.Array" />, starting at the specified index of the target array.</summary>
			/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from the collection. The <paramref name="array" /> must have zero-based indexing.</param>
			/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
			// Token: 0x0600104B RID: 4171 RVA: 0x0003F34C File Offset: 0x0003D54C
			public void CopyTo(Array array, int index)
			{
				this.EnsurePopulated();
				this._children.CopyTo(array, index);
			}

			// Token: 0x0600104C RID: 4172 RVA: 0x0003F361 File Offset: 0x0003D561
			private void EnsurePopulated()
			{
				if (this._children == null)
				{
					this._service.PopulateOptionCollection(this);
					if (this._children == null)
					{
						this._children = new ArrayList(1);
					}
				}
			}

			/// <summary>Returns an <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate this collection.</summary>
			/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate this collection.</returns>
			// Token: 0x0600104D RID: 4173 RVA: 0x0003F38B File Offset: 0x0003D58B
			public IEnumerator GetEnumerator()
			{
				this.EnsurePopulated();
				return this._children.GetEnumerator();
			}

			/// <summary>Returns the index of the first occurrence of a given value in a range of this collection.</summary>
			/// <param name="value">The object to locate in the collection.</param>
			/// <returns>The index of the first occurrence of value within the entire collection, if found; otherwise, the lower bound of the collection minus 1.</returns>
			// Token: 0x0600104E RID: 4174 RVA: 0x0003F39E File Offset: 0x0003D59E
			public int IndexOf(DesignerOptionService.DesignerOptionCollection value)
			{
				this.EnsurePopulated();
				return this._children.IndexOf(value);
			}

			// Token: 0x0600104F RID: 4175 RVA: 0x0003F3B4 File Offset: 0x0003D5B4
			private static object RecurseFindValue(DesignerOptionService.DesignerOptionCollection options)
			{
				if (options._value != null)
				{
					return options._value;
				}
				foreach (object obj in options)
				{
					object obj2 = DesignerOptionService.DesignerOptionCollection.RecurseFindValue((DesignerOptionService.DesignerOptionCollection)obj);
					if (obj2 != null)
					{
						return obj2;
					}
				}
				return null;
			}

			/// <summary>Displays a dialog box user interface (UI) with which the user can configure the options in this <see cref="T:System.ComponentModel.Design.DesignerOptionService.DesignerOptionCollection" />.</summary>
			/// <returns>
			///     <see langword="true" /> if the dialog box can be displayed; otherwise, <see langword="false" />.</returns>
			// Token: 0x06001050 RID: 4176 RVA: 0x0003F420 File Offset: 0x0003D620
			public bool ShowDialog()
			{
				object obj = DesignerOptionService.DesignerOptionCollection.RecurseFindValue(this);
				return obj != null && this._service.ShowDialog(this, obj);
			}

			/// <summary>Gets a value indicating whether access to the collection is synchronized and, therefore, thread safe.</summary>
			/// <returns>
			///     <see langword="true" /> if the access to the collection is synchronized; otherwise, <see langword="false" />.</returns>
			// Token: 0x1700035C RID: 860
			// (get) Token: 0x06001051 RID: 4177 RVA: 0x00005AFA File Offset: 0x00003CFA
			bool ICollection.IsSynchronized
			{
				get
				{
					return false;
				}
			}

			/// <summary>Gets an object that can be used to synchronize access to the collection.</summary>
			/// <returns>An object that can be used to synchronize access to the collection.</returns>
			// Token: 0x1700035D RID: 861
			// (get) Token: 0x06001052 RID: 4178 RVA: 0x00002068 File Offset: 0x00000268
			object ICollection.SyncRoot
			{
				get
				{
					return this;
				}
			}

			/// <summary>Gets a value indicating whether the collection has a fixed size.</summary>
			/// <returns>
			///     <see langword="true" /> if the collection has a fixed size; otherwise, <see langword="false" />.</returns>
			// Token: 0x1700035E RID: 862
			// (get) Token: 0x06001053 RID: 4179 RVA: 0x00003298 File Offset: 0x00001498
			bool IList.IsFixedSize
			{
				get
				{
					return true;
				}
			}

			/// <summary>Gets a value indicating whether the collection is read-only.</summary>
			/// <returns>
			///     <see langword="true" /> if the collection is read-only; otherwise, <see langword="false" />.</returns>
			// Token: 0x1700035F RID: 863
			// (get) Token: 0x06001054 RID: 4180 RVA: 0x00003298 File Offset: 0x00001498
			bool IList.IsReadOnly
			{
				get
				{
					return true;
				}
			}

			/// <summary>Gets or sets the element at the specified index.</summary>
			/// <param name="index">The zero-based index of the element to get or set.</param>
			/// <returns>The element at the specified index.</returns>
			// Token: 0x17000360 RID: 864
			object IList.this[int index]
			{
				get
				{
					return this[index];
				}
				set
				{
					throw new NotSupportedException();
				}
			}

			/// <summary>Adds an item to the <see cref="T:System.Collections.IList" />.</summary>
			/// <param name="value">The <see cref="T:System.Object" /> to add to the <see cref="T:System.Collections.IList" />.</param>
			/// <returns>The position into which the new element was inserted.</returns>
			// Token: 0x06001057 RID: 4183 RVA: 0x00006740 File Offset: 0x00004940
			int IList.Add(object value)
			{
				throw new NotSupportedException();
			}

			/// <summary>Removes all items from the collection.</summary>
			// Token: 0x06001058 RID: 4184 RVA: 0x00006740 File Offset: 0x00004940
			void IList.Clear()
			{
				throw new NotSupportedException();
			}

			/// <summary>Determines whether the collection contains a specific value.</summary>
			/// <param name="value">The <see cref="T:System.Object" /> to locate in the collection</param>
			/// <returns>
			///     <see langword="true" /> if the <see cref="T:System.Object" /> is found in the collection; otherwise, <see langword="false" />. </returns>
			// Token: 0x06001059 RID: 4185 RVA: 0x0003F44F File Offset: 0x0003D64F
			bool IList.Contains(object value)
			{
				this.EnsurePopulated();
				return this._children.Contains(value);
			}

			/// <summary>Determines the index of a specific item in the collection.</summary>
			/// <param name="value">The <see cref="T:System.Object" /> to locate in the collection.</param>
			/// <returns>The index of <paramref name="value" /> if found in the list; otherwise, -1.</returns>
			// Token: 0x0600105A RID: 4186 RVA: 0x0003F39E File Offset: 0x0003D59E
			int IList.IndexOf(object value)
			{
				this.EnsurePopulated();
				return this._children.IndexOf(value);
			}

			/// <summary>Inserts an item into the collection at the specified index.</summary>
			/// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
			/// <param name="value">The <see cref="T:System.Object" /> to insert into the collection.</param>
			// Token: 0x0600105B RID: 4187 RVA: 0x00006740 File Offset: 0x00004940
			void IList.Insert(int index, object value)
			{
				throw new NotSupportedException();
			}

			/// <summary>Removes the first occurrence of a specific object from the collection.</summary>
			/// <param name="value">The <see cref="T:System.Object" /> to remove from the collection.</param>
			// Token: 0x0600105C RID: 4188 RVA: 0x00006740 File Offset: 0x00004940
			void IList.Remove(object value)
			{
				throw new NotSupportedException();
			}

			/// <summary>Removes the collection item at the specified index.</summary>
			/// <param name="index">The zero-based index of the item to remove.</param>
			// Token: 0x0600105D RID: 4189 RVA: 0x00006740 File Offset: 0x00004940
			void IList.RemoveAt(int index)
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600105E RID: 4190 RVA: 0x000092E2 File Offset: 0x000074E2
			internal DesignerOptionCollection()
			{
				ThrowStub.ThrowNotSupportedException();
			}

			// Token: 0x04000E5A RID: 3674
			private DesignerOptionService _service;

			// Token: 0x04000E5B RID: 3675
			private DesignerOptionService.DesignerOptionCollection _parent;

			// Token: 0x04000E5C RID: 3676
			private string _name;

			// Token: 0x04000E5D RID: 3677
			private object _value;

			// Token: 0x04000E5E RID: 3678
			private ArrayList _children;

			// Token: 0x04000E5F RID: 3679
			private PropertyDescriptorCollection _properties;

			// Token: 0x0200020E RID: 526
			private sealed class WrappedPropertyDescriptor : PropertyDescriptor
			{
				// Token: 0x0600105F RID: 4191 RVA: 0x0003F463 File Offset: 0x0003D663
				internal WrappedPropertyDescriptor(PropertyDescriptor property, object target) : base(property.Name, null)
				{
					this.property = property;
					this.target = target;
				}

				// Token: 0x17000361 RID: 865
				// (get) Token: 0x06001060 RID: 4192 RVA: 0x0003F480 File Offset: 0x0003D680
				public override AttributeCollection Attributes
				{
					get
					{
						return this.property.Attributes;
					}
				}

				// Token: 0x17000362 RID: 866
				// (get) Token: 0x06001061 RID: 4193 RVA: 0x0003F48D File Offset: 0x0003D68D
				public override Type ComponentType
				{
					get
					{
						return this.property.ComponentType;
					}
				}

				// Token: 0x17000363 RID: 867
				// (get) Token: 0x06001062 RID: 4194 RVA: 0x0003F49A File Offset: 0x0003D69A
				public override bool IsReadOnly
				{
					get
					{
						return this.property.IsReadOnly;
					}
				}

				// Token: 0x17000364 RID: 868
				// (get) Token: 0x06001063 RID: 4195 RVA: 0x0003F4A7 File Offset: 0x0003D6A7
				public override Type PropertyType
				{
					get
					{
						return this.property.PropertyType;
					}
				}

				// Token: 0x06001064 RID: 4196 RVA: 0x0003F4B4 File Offset: 0x0003D6B4
				public override bool CanResetValue(object component)
				{
					return this.property.CanResetValue(this.target);
				}

				// Token: 0x06001065 RID: 4197 RVA: 0x0003F4C7 File Offset: 0x0003D6C7
				public override object GetValue(object component)
				{
					return this.property.GetValue(this.target);
				}

				// Token: 0x06001066 RID: 4198 RVA: 0x0003F4DA File Offset: 0x0003D6DA
				public override void ResetValue(object component)
				{
					this.property.ResetValue(this.target);
				}

				// Token: 0x06001067 RID: 4199 RVA: 0x0003F4ED File Offset: 0x0003D6ED
				public override void SetValue(object component, object value)
				{
					this.property.SetValue(this.target, value);
				}

				// Token: 0x06001068 RID: 4200 RVA: 0x0003F501 File Offset: 0x0003D701
				public override bool ShouldSerializeValue(object component)
				{
					return this.property.ShouldSerializeValue(this.target);
				}

				// Token: 0x04000E60 RID: 3680
				private object target;

				// Token: 0x04000E61 RID: 3681
				private PropertyDescriptor property;
			}
		}

		// Token: 0x0200020F RID: 527
		internal sealed class DesignerOptionConverter : TypeConverter
		{
			// Token: 0x06001069 RID: 4201 RVA: 0x00003298 File Offset: 0x00001498
			public override bool GetPropertiesSupported(ITypeDescriptorContext cxt)
			{
				return true;
			}

			// Token: 0x0600106A RID: 4202 RVA: 0x0003F514 File Offset: 0x0003D714
			public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext cxt, object value, Attribute[] attributes)
			{
				PropertyDescriptorCollection propertyDescriptorCollection = new PropertyDescriptorCollection(null);
				DesignerOptionService.DesignerOptionCollection designerOptionCollection = value as DesignerOptionService.DesignerOptionCollection;
				if (designerOptionCollection == null)
				{
					return propertyDescriptorCollection;
				}
				foreach (object obj in designerOptionCollection)
				{
					DesignerOptionService.DesignerOptionCollection option = (DesignerOptionService.DesignerOptionCollection)obj;
					propertyDescriptorCollection.Add(new DesignerOptionService.DesignerOptionConverter.OptionPropertyDescriptor(option));
				}
				foreach (object obj2 in designerOptionCollection.Properties)
				{
					PropertyDescriptor value2 = (PropertyDescriptor)obj2;
					propertyDescriptorCollection.Add(value2);
				}
				return propertyDescriptorCollection;
			}

			// Token: 0x0600106B RID: 4203 RVA: 0x0003F5D4 File Offset: 0x0003D7D4
			public override object ConvertTo(ITypeDescriptorContext cxt, CultureInfo culture, object value, Type destinationType)
			{
				if (destinationType == typeof(string))
				{
					return SR.GetString("(Collection)");
				}
				return base.ConvertTo(cxt, culture, value, destinationType);
			}

			// Token: 0x0600106C RID: 4204 RVA: 0x0001582C File Offset: 0x00013A2C
			public DesignerOptionConverter()
			{
			}

			// Token: 0x02000210 RID: 528
			private class OptionPropertyDescriptor : PropertyDescriptor
			{
				// Token: 0x0600106D RID: 4205 RVA: 0x0003F5FF File Offset: 0x0003D7FF
				internal OptionPropertyDescriptor(DesignerOptionService.DesignerOptionCollection option) : base(option.Name, null)
				{
					this._option = option;
				}

				// Token: 0x17000365 RID: 869
				// (get) Token: 0x0600106E RID: 4206 RVA: 0x0003F615 File Offset: 0x0003D815
				public override Type ComponentType
				{
					get
					{
						return this._option.GetType();
					}
				}

				// Token: 0x17000366 RID: 870
				// (get) Token: 0x0600106F RID: 4207 RVA: 0x00003298 File Offset: 0x00001498
				public override bool IsReadOnly
				{
					get
					{
						return true;
					}
				}

				// Token: 0x17000367 RID: 871
				// (get) Token: 0x06001070 RID: 4208 RVA: 0x0003F615 File Offset: 0x0003D815
				public override Type PropertyType
				{
					get
					{
						return this._option.GetType();
					}
				}

				// Token: 0x06001071 RID: 4209 RVA: 0x00005AFA File Offset: 0x00003CFA
				public override bool CanResetValue(object component)
				{
					return false;
				}

				// Token: 0x06001072 RID: 4210 RVA: 0x0003F622 File Offset: 0x0003D822
				public override object GetValue(object component)
				{
					return this._option;
				}

				// Token: 0x06001073 RID: 4211 RVA: 0x0000232D File Offset: 0x0000052D
				public override void ResetValue(object component)
				{
				}

				// Token: 0x06001074 RID: 4212 RVA: 0x0000232D File Offset: 0x0000052D
				public override void SetValue(object component, object value)
				{
				}

				// Token: 0x06001075 RID: 4213 RVA: 0x00005AFA File Offset: 0x00003CFA
				public override bool ShouldSerializeValue(object component)
				{
					return false;
				}

				// Token: 0x04000E62 RID: 3682
				private DesignerOptionService.DesignerOptionCollection _option;
			}
		}
	}
}
