﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	/// <summary>Defines identifiers for the standard set of commands that are available to most applications.</summary>
	// Token: 0x0200023E RID: 574
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class StandardCommands
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.StandardCommands" /> class. </summary>
		// Token: 0x0600117C RID: 4476 RVA: 0x0000232F File Offset: 0x0000052F
		public StandardCommands()
		{
		}

		// Token: 0x0600117D RID: 4477 RVA: 0x00040420 File Offset: 0x0003E620
		// Note: this type is marked as 'beforefieldinit'.
		static StandardCommands()
		{
		}

		// Token: 0x04000E94 RID: 3732
		private static readonly Guid standardCommandSet = StandardCommands.ShellGuids.VSStandardCommandSet97;

		// Token: 0x04000E95 RID: 3733
		private static readonly Guid ndpCommandSet = new Guid("{74D21313-2AEE-11d1-8BFB-00A0C90F26F7}");

		// Token: 0x04000E96 RID: 3734
		private const int cmdidDesignerVerbFirst = 8192;

		// Token: 0x04000E97 RID: 3735
		private const int cmdidDesignerVerbLast = 8448;

		// Token: 0x04000E98 RID: 3736
		private const int cmdidArrangeIcons = 12298;

		// Token: 0x04000E99 RID: 3737
		private const int cmdidLineupIcons = 12299;

		// Token: 0x04000E9A RID: 3738
		private const int cmdidShowLargeIcons = 12300;

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the AlignBottom command. This field is read-only.</summary>
		// Token: 0x04000E9B RID: 3739
		public static readonly CommandID AlignBottom = new CommandID(StandardCommands.standardCommandSet, 1);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the AlignHorizontalCenters command. This field is read-only.</summary>
		// Token: 0x04000E9C RID: 3740
		public static readonly CommandID AlignHorizontalCenters = new CommandID(StandardCommands.standardCommandSet, 2);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the AlignLeft command. This field is read-only.</summary>
		// Token: 0x04000E9D RID: 3741
		public static readonly CommandID AlignLeft = new CommandID(StandardCommands.standardCommandSet, 3);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the AlignRight command. This field is read-only.</summary>
		// Token: 0x04000E9E RID: 3742
		public static readonly CommandID AlignRight = new CommandID(StandardCommands.standardCommandSet, 4);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the AlignToGrid command. This field is read-only.</summary>
		// Token: 0x04000E9F RID: 3743
		public static readonly CommandID AlignToGrid = new CommandID(StandardCommands.standardCommandSet, 5);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the AlignTop command. This field is read-only.</summary>
		// Token: 0x04000EA0 RID: 3744
		public static readonly CommandID AlignTop = new CommandID(StandardCommands.standardCommandSet, 6);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the AlignVerticalCenters command. This field is read-only.</summary>
		// Token: 0x04000EA1 RID: 3745
		public static readonly CommandID AlignVerticalCenters = new CommandID(StandardCommands.standardCommandSet, 7);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the ArrangeBottom command. This field is read-only.</summary>
		// Token: 0x04000EA2 RID: 3746
		public static readonly CommandID ArrangeBottom = new CommandID(StandardCommands.standardCommandSet, 8);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the ArrangeRight command. This field is read-only.</summary>
		// Token: 0x04000EA3 RID: 3747
		public static readonly CommandID ArrangeRight = new CommandID(StandardCommands.standardCommandSet, 9);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the BringForward command. This field is read-only.</summary>
		// Token: 0x04000EA4 RID: 3748
		public static readonly CommandID BringForward = new CommandID(StandardCommands.standardCommandSet, 10);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the BringToFront command. This field is read-only.</summary>
		// Token: 0x04000EA5 RID: 3749
		public static readonly CommandID BringToFront = new CommandID(StandardCommands.standardCommandSet, 11);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the CenterHorizontally command. This field is read-only.</summary>
		// Token: 0x04000EA6 RID: 3750
		public static readonly CommandID CenterHorizontally = new CommandID(StandardCommands.standardCommandSet, 12);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the CenterVertically command. This field is read-only.</summary>
		// Token: 0x04000EA7 RID: 3751
		public static readonly CommandID CenterVertically = new CommandID(StandardCommands.standardCommandSet, 13);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the ViewCode command. This field is read-only.</summary>
		// Token: 0x04000EA8 RID: 3752
		public static readonly CommandID ViewCode = new CommandID(StandardCommands.standardCommandSet, 333);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Document Outline command. This field is read-only.</summary>
		// Token: 0x04000EA9 RID: 3753
		public static readonly CommandID DocumentOutline = new CommandID(StandardCommands.standardCommandSet, 239);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Copy command. This field is read-only.</summary>
		// Token: 0x04000EAA RID: 3754
		public static readonly CommandID Copy = new CommandID(StandardCommands.standardCommandSet, 15);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Cut command. This field is read-only.</summary>
		// Token: 0x04000EAB RID: 3755
		public static readonly CommandID Cut = new CommandID(StandardCommands.standardCommandSet, 16);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Delete command. This field is read-only.</summary>
		// Token: 0x04000EAC RID: 3756
		public static readonly CommandID Delete = new CommandID(StandardCommands.standardCommandSet, 17);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Group command. This field is read-only.</summary>
		// Token: 0x04000EAD RID: 3757
		public static readonly CommandID Group = new CommandID(StandardCommands.standardCommandSet, 20);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the HorizSpaceConcatenate command. This field is read-only.</summary>
		// Token: 0x04000EAE RID: 3758
		public static readonly CommandID HorizSpaceConcatenate = new CommandID(StandardCommands.standardCommandSet, 21);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the HorizSpaceDecrease command. This field is read-only.</summary>
		// Token: 0x04000EAF RID: 3759
		public static readonly CommandID HorizSpaceDecrease = new CommandID(StandardCommands.standardCommandSet, 22);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the HorizSpaceIncrease command. This field is read-only.</summary>
		// Token: 0x04000EB0 RID: 3760
		public static readonly CommandID HorizSpaceIncrease = new CommandID(StandardCommands.standardCommandSet, 23);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the HorizSpaceMakeEqual command. This field is read-only.</summary>
		// Token: 0x04000EB1 RID: 3761
		public static readonly CommandID HorizSpaceMakeEqual = new CommandID(StandardCommands.standardCommandSet, 24);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Paste command. This field is read-only.</summary>
		// Token: 0x04000EB2 RID: 3762
		public static readonly CommandID Paste = new CommandID(StandardCommands.standardCommandSet, 26);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Properties command. This field is read-only.</summary>
		// Token: 0x04000EB3 RID: 3763
		public static readonly CommandID Properties = new CommandID(StandardCommands.standardCommandSet, 28);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Redo command. This field is read-only.</summary>
		// Token: 0x04000EB4 RID: 3764
		public static readonly CommandID Redo = new CommandID(StandardCommands.standardCommandSet, 29);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the MultiLevelRedo command. This field is read-only.</summary>
		// Token: 0x04000EB5 RID: 3765
		public static readonly CommandID MultiLevelRedo = new CommandID(StandardCommands.standardCommandSet, 30);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the SelectAll command. This field is read-only.</summary>
		// Token: 0x04000EB6 RID: 3766
		public static readonly CommandID SelectAll = new CommandID(StandardCommands.standardCommandSet, 31);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the SendBackward command. This field is read-only.</summary>
		// Token: 0x04000EB7 RID: 3767
		public static readonly CommandID SendBackward = new CommandID(StandardCommands.standardCommandSet, 32);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the SendToBack command. This field is read-only.</summary>
		// Token: 0x04000EB8 RID: 3768
		public static readonly CommandID SendToBack = new CommandID(StandardCommands.standardCommandSet, 33);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the SizeToControl command. This field is read-only.</summary>
		// Token: 0x04000EB9 RID: 3769
		public static readonly CommandID SizeToControl = new CommandID(StandardCommands.standardCommandSet, 35);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the SizeToControlHeight command. This field is read-only.</summary>
		// Token: 0x04000EBA RID: 3770
		public static readonly CommandID SizeToControlHeight = new CommandID(StandardCommands.standardCommandSet, 36);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the SizeToControlWidth command. This field is read-only.</summary>
		// Token: 0x04000EBB RID: 3771
		public static readonly CommandID SizeToControlWidth = new CommandID(StandardCommands.standardCommandSet, 37);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the SizeToFit command. This field is read-only.</summary>
		// Token: 0x04000EBC RID: 3772
		public static readonly CommandID SizeToFit = new CommandID(StandardCommands.standardCommandSet, 38);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the SizeToGrid command. This field is read-only.</summary>
		// Token: 0x04000EBD RID: 3773
		public static readonly CommandID SizeToGrid = new CommandID(StandardCommands.standardCommandSet, 39);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the SnapToGrid command. This field is read-only.</summary>
		// Token: 0x04000EBE RID: 3774
		public static readonly CommandID SnapToGrid = new CommandID(StandardCommands.standardCommandSet, 40);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the TabOrder command. This field is read-only.</summary>
		// Token: 0x04000EBF RID: 3775
		public static readonly CommandID TabOrder = new CommandID(StandardCommands.standardCommandSet, 41);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Undo command. This field is read-only.</summary>
		// Token: 0x04000EC0 RID: 3776
		public static readonly CommandID Undo = new CommandID(StandardCommands.standardCommandSet, 43);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the MultiLevelUndo command. This field is read-only.</summary>
		// Token: 0x04000EC1 RID: 3777
		public static readonly CommandID MultiLevelUndo = new CommandID(StandardCommands.standardCommandSet, 44);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Ungroup command. This field is read-only.</summary>
		// Token: 0x04000EC2 RID: 3778
		public static readonly CommandID Ungroup = new CommandID(StandardCommands.standardCommandSet, 45);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the VertSpaceConcatenate command. This field is read-only.</summary>
		// Token: 0x04000EC3 RID: 3779
		public static readonly CommandID VertSpaceConcatenate = new CommandID(StandardCommands.standardCommandSet, 46);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the VertSpaceDecrease command. This field is read-only.</summary>
		// Token: 0x04000EC4 RID: 3780
		public static readonly CommandID VertSpaceDecrease = new CommandID(StandardCommands.standardCommandSet, 47);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the VertSpaceIncrease command. This field is read-only.</summary>
		// Token: 0x04000EC5 RID: 3781
		public static readonly CommandID VertSpaceIncrease = new CommandID(StandardCommands.standardCommandSet, 48);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the VertSpaceMakeEqual command. This field is read-only.</summary>
		// Token: 0x04000EC6 RID: 3782
		public static readonly CommandID VertSpaceMakeEqual = new CommandID(StandardCommands.standardCommandSet, 49);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the ShowGrid command. This field is read-only.</summary>
		// Token: 0x04000EC7 RID: 3783
		public static readonly CommandID ShowGrid = new CommandID(StandardCommands.standardCommandSet, 103);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the ViewGrid command. This field is read-only.</summary>
		// Token: 0x04000EC8 RID: 3784
		public static readonly CommandID ViewGrid = new CommandID(StandardCommands.standardCommandSet, 125);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the Replace command. This field is read-only.</summary>
		// Token: 0x04000EC9 RID: 3785
		public static readonly CommandID Replace = new CommandID(StandardCommands.standardCommandSet, 230);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the PropertiesWindow command. This field is read-only.</summary>
		// Token: 0x04000ECA RID: 3786
		public static readonly CommandID PropertiesWindow = new CommandID(StandardCommands.standardCommandSet, 235);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the LockControls command. This field is read-only.</summary>
		// Token: 0x04000ECB RID: 3787
		public static readonly CommandID LockControls = new CommandID(StandardCommands.standardCommandSet, 369);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the F1Help command. This field is read-only.</summary>
		// Token: 0x04000ECC RID: 3788
		public static readonly CommandID F1Help = new CommandID(StandardCommands.standardCommandSet, 377);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the ArrangeIcons command. This field is read-only.</summary>
		// Token: 0x04000ECD RID: 3789
		public static readonly CommandID ArrangeIcons = new CommandID(StandardCommands.ndpCommandSet, 12298);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the LineupIcons command. This field is read-only.</summary>
		// Token: 0x04000ECE RID: 3790
		public static readonly CommandID LineupIcons = new CommandID(StandardCommands.ndpCommandSet, 12299);

		/// <summary>Gets the <see cref="T:System.ComponentModel.Design.CommandID" /> for the ShowLargeIcons command. This field is read-only.</summary>
		// Token: 0x04000ECF RID: 3791
		public static readonly CommandID ShowLargeIcons = new CommandID(StandardCommands.ndpCommandSet, 12300);

		/// <summary>Gets the first of a set of verbs. This field is read-only.</summary>
		// Token: 0x04000ED0 RID: 3792
		public static readonly CommandID VerbFirst = new CommandID(StandardCommands.ndpCommandSet, 8192);

		/// <summary>Gets the last of a set of verbs. This field is read-only.</summary>
		// Token: 0x04000ED1 RID: 3793
		public static readonly CommandID VerbLast = new CommandID(StandardCommands.ndpCommandSet, 8448);

		// Token: 0x0200023F RID: 575
		private static class VSStandardCommands
		{
			// Token: 0x04000ED2 RID: 3794
			internal const int cmdidAlignBottom = 1;

			// Token: 0x04000ED3 RID: 3795
			internal const int cmdidAlignHorizontalCenters = 2;

			// Token: 0x04000ED4 RID: 3796
			internal const int cmdidAlignLeft = 3;

			// Token: 0x04000ED5 RID: 3797
			internal const int cmdidAlignRight = 4;

			// Token: 0x04000ED6 RID: 3798
			internal const int cmdidAlignToGrid = 5;

			// Token: 0x04000ED7 RID: 3799
			internal const int cmdidAlignTop = 6;

			// Token: 0x04000ED8 RID: 3800
			internal const int cmdidAlignVerticalCenters = 7;

			// Token: 0x04000ED9 RID: 3801
			internal const int cmdidArrangeBottom = 8;

			// Token: 0x04000EDA RID: 3802
			internal const int cmdidArrangeRight = 9;

			// Token: 0x04000EDB RID: 3803
			internal const int cmdidBringForward = 10;

			// Token: 0x04000EDC RID: 3804
			internal const int cmdidBringToFront = 11;

			// Token: 0x04000EDD RID: 3805
			internal const int cmdidCenterHorizontally = 12;

			// Token: 0x04000EDE RID: 3806
			internal const int cmdidCenterVertically = 13;

			// Token: 0x04000EDF RID: 3807
			internal const int cmdidCode = 14;

			// Token: 0x04000EE0 RID: 3808
			internal const int cmdidCopy = 15;

			// Token: 0x04000EE1 RID: 3809
			internal const int cmdidCut = 16;

			// Token: 0x04000EE2 RID: 3810
			internal const int cmdidDelete = 17;

			// Token: 0x04000EE3 RID: 3811
			internal const int cmdidFontName = 18;

			// Token: 0x04000EE4 RID: 3812
			internal const int cmdidFontSize = 19;

			// Token: 0x04000EE5 RID: 3813
			internal const int cmdidGroup = 20;

			// Token: 0x04000EE6 RID: 3814
			internal const int cmdidHorizSpaceConcatenate = 21;

			// Token: 0x04000EE7 RID: 3815
			internal const int cmdidHorizSpaceDecrease = 22;

			// Token: 0x04000EE8 RID: 3816
			internal const int cmdidHorizSpaceIncrease = 23;

			// Token: 0x04000EE9 RID: 3817
			internal const int cmdidHorizSpaceMakeEqual = 24;

			// Token: 0x04000EEA RID: 3818
			internal const int cmdidLockControls = 369;

			// Token: 0x04000EEB RID: 3819
			internal const int cmdidInsertObject = 25;

			// Token: 0x04000EEC RID: 3820
			internal const int cmdidPaste = 26;

			// Token: 0x04000EED RID: 3821
			internal const int cmdidPrint = 27;

			// Token: 0x04000EEE RID: 3822
			internal const int cmdidProperties = 28;

			// Token: 0x04000EEF RID: 3823
			internal const int cmdidRedo = 29;

			// Token: 0x04000EF0 RID: 3824
			internal const int cmdidMultiLevelRedo = 30;

			// Token: 0x04000EF1 RID: 3825
			internal const int cmdidSelectAll = 31;

			// Token: 0x04000EF2 RID: 3826
			internal const int cmdidSendBackward = 32;

			// Token: 0x04000EF3 RID: 3827
			internal const int cmdidSendToBack = 33;

			// Token: 0x04000EF4 RID: 3828
			internal const int cmdidShowTable = 34;

			// Token: 0x04000EF5 RID: 3829
			internal const int cmdidSizeToControl = 35;

			// Token: 0x04000EF6 RID: 3830
			internal const int cmdidSizeToControlHeight = 36;

			// Token: 0x04000EF7 RID: 3831
			internal const int cmdidSizeToControlWidth = 37;

			// Token: 0x04000EF8 RID: 3832
			internal const int cmdidSizeToFit = 38;

			// Token: 0x04000EF9 RID: 3833
			internal const int cmdidSizeToGrid = 39;

			// Token: 0x04000EFA RID: 3834
			internal const int cmdidSnapToGrid = 40;

			// Token: 0x04000EFB RID: 3835
			internal const int cmdidTabOrder = 41;

			// Token: 0x04000EFC RID: 3836
			internal const int cmdidToolbox = 42;

			// Token: 0x04000EFD RID: 3837
			internal const int cmdidUndo = 43;

			// Token: 0x04000EFE RID: 3838
			internal const int cmdidMultiLevelUndo = 44;

			// Token: 0x04000EFF RID: 3839
			internal const int cmdidUngroup = 45;

			// Token: 0x04000F00 RID: 3840
			internal const int cmdidVertSpaceConcatenate = 46;

			// Token: 0x04000F01 RID: 3841
			internal const int cmdidVertSpaceDecrease = 47;

			// Token: 0x04000F02 RID: 3842
			internal const int cmdidVertSpaceIncrease = 48;

			// Token: 0x04000F03 RID: 3843
			internal const int cmdidVertSpaceMakeEqual = 49;

			// Token: 0x04000F04 RID: 3844
			internal const int cmdidZoomPercent = 50;

			// Token: 0x04000F05 RID: 3845
			internal const int cmdidBackColor = 51;

			// Token: 0x04000F06 RID: 3846
			internal const int cmdidBold = 52;

			// Token: 0x04000F07 RID: 3847
			internal const int cmdidBorderColor = 53;

			// Token: 0x04000F08 RID: 3848
			internal const int cmdidBorderDashDot = 54;

			// Token: 0x04000F09 RID: 3849
			internal const int cmdidBorderDashDotDot = 55;

			// Token: 0x04000F0A RID: 3850
			internal const int cmdidBorderDashes = 56;

			// Token: 0x04000F0B RID: 3851
			internal const int cmdidBorderDots = 57;

			// Token: 0x04000F0C RID: 3852
			internal const int cmdidBorderShortDashes = 58;

			// Token: 0x04000F0D RID: 3853
			internal const int cmdidBorderSolid = 59;

			// Token: 0x04000F0E RID: 3854
			internal const int cmdidBorderSparseDots = 60;

			// Token: 0x04000F0F RID: 3855
			internal const int cmdidBorderWidth1 = 61;

			// Token: 0x04000F10 RID: 3856
			internal const int cmdidBorderWidth2 = 62;

			// Token: 0x04000F11 RID: 3857
			internal const int cmdidBorderWidth3 = 63;

			// Token: 0x04000F12 RID: 3858
			internal const int cmdidBorderWidth4 = 64;

			// Token: 0x04000F13 RID: 3859
			internal const int cmdidBorderWidth5 = 65;

			// Token: 0x04000F14 RID: 3860
			internal const int cmdidBorderWidth6 = 66;

			// Token: 0x04000F15 RID: 3861
			internal const int cmdidBorderWidthHairline = 67;

			// Token: 0x04000F16 RID: 3862
			internal const int cmdidFlat = 68;

			// Token: 0x04000F17 RID: 3863
			internal const int cmdidForeColor = 69;

			// Token: 0x04000F18 RID: 3864
			internal const int cmdidItalic = 70;

			// Token: 0x04000F19 RID: 3865
			internal const int cmdidJustifyCenter = 71;

			// Token: 0x04000F1A RID: 3866
			internal const int cmdidJustifyGeneral = 72;

			// Token: 0x04000F1B RID: 3867
			internal const int cmdidJustifyLeft = 73;

			// Token: 0x04000F1C RID: 3868
			internal const int cmdidJustifyRight = 74;

			// Token: 0x04000F1D RID: 3869
			internal const int cmdidRaised = 75;

			// Token: 0x04000F1E RID: 3870
			internal const int cmdidSunken = 76;

			// Token: 0x04000F1F RID: 3871
			internal const int cmdidUnderline = 77;

			// Token: 0x04000F20 RID: 3872
			internal const int cmdidChiseled = 78;

			// Token: 0x04000F21 RID: 3873
			internal const int cmdidEtched = 79;

			// Token: 0x04000F22 RID: 3874
			internal const int cmdidShadowed = 80;

			// Token: 0x04000F23 RID: 3875
			internal const int cmdidCompDebug1 = 81;

			// Token: 0x04000F24 RID: 3876
			internal const int cmdidCompDebug2 = 82;

			// Token: 0x04000F25 RID: 3877
			internal const int cmdidCompDebug3 = 83;

			// Token: 0x04000F26 RID: 3878
			internal const int cmdidCompDebug4 = 84;

			// Token: 0x04000F27 RID: 3879
			internal const int cmdidCompDebug5 = 85;

			// Token: 0x04000F28 RID: 3880
			internal const int cmdidCompDebug6 = 86;

			// Token: 0x04000F29 RID: 3881
			internal const int cmdidCompDebug7 = 87;

			// Token: 0x04000F2A RID: 3882
			internal const int cmdidCompDebug8 = 88;

			// Token: 0x04000F2B RID: 3883
			internal const int cmdidCompDebug9 = 89;

			// Token: 0x04000F2C RID: 3884
			internal const int cmdidCompDebug10 = 90;

			// Token: 0x04000F2D RID: 3885
			internal const int cmdidCompDebug11 = 91;

			// Token: 0x04000F2E RID: 3886
			internal const int cmdidCompDebug12 = 92;

			// Token: 0x04000F2F RID: 3887
			internal const int cmdidCompDebug13 = 93;

			// Token: 0x04000F30 RID: 3888
			internal const int cmdidCompDebug14 = 94;

			// Token: 0x04000F31 RID: 3889
			internal const int cmdidCompDebug15 = 95;

			// Token: 0x04000F32 RID: 3890
			internal const int cmdidExistingSchemaEdit = 96;

			// Token: 0x04000F33 RID: 3891
			internal const int cmdidFind = 97;

			// Token: 0x04000F34 RID: 3892
			internal const int cmdidGetZoom = 98;

			// Token: 0x04000F35 RID: 3893
			internal const int cmdidQueryOpenDesign = 99;

			// Token: 0x04000F36 RID: 3894
			internal const int cmdidQueryOpenNew = 100;

			// Token: 0x04000F37 RID: 3895
			internal const int cmdidSingleTableDesign = 101;

			// Token: 0x04000F38 RID: 3896
			internal const int cmdidSingleTableNew = 102;

			// Token: 0x04000F39 RID: 3897
			internal const int cmdidShowGrid = 103;

			// Token: 0x04000F3A RID: 3898
			internal const int cmdidNewTable = 104;

			// Token: 0x04000F3B RID: 3899
			internal const int cmdidCollapsedView = 105;

			// Token: 0x04000F3C RID: 3900
			internal const int cmdidFieldView = 106;

			// Token: 0x04000F3D RID: 3901
			internal const int cmdidVerifySQL = 107;

			// Token: 0x04000F3E RID: 3902
			internal const int cmdidHideTable = 108;

			// Token: 0x04000F3F RID: 3903
			internal const int cmdidPrimaryKey = 109;

			// Token: 0x04000F40 RID: 3904
			internal const int cmdidSave = 110;

			// Token: 0x04000F41 RID: 3905
			internal const int cmdidSaveAs = 111;

			// Token: 0x04000F42 RID: 3906
			internal const int cmdidSortAscending = 112;

			// Token: 0x04000F43 RID: 3907
			internal const int cmdidSortDescending = 113;

			// Token: 0x04000F44 RID: 3908
			internal const int cmdidAppendQuery = 114;

			// Token: 0x04000F45 RID: 3909
			internal const int cmdidCrosstabQuery = 115;

			// Token: 0x04000F46 RID: 3910
			internal const int cmdidDeleteQuery = 116;

			// Token: 0x04000F47 RID: 3911
			internal const int cmdidMakeTableQuery = 117;

			// Token: 0x04000F48 RID: 3912
			internal const int cmdidSelectQuery = 118;

			// Token: 0x04000F49 RID: 3913
			internal const int cmdidUpdateQuery = 119;

			// Token: 0x04000F4A RID: 3914
			internal const int cmdidParameters = 120;

			// Token: 0x04000F4B RID: 3915
			internal const int cmdidTotals = 121;

			// Token: 0x04000F4C RID: 3916
			internal const int cmdidViewCollapsed = 122;

			// Token: 0x04000F4D RID: 3917
			internal const int cmdidViewFieldList = 123;

			// Token: 0x04000F4E RID: 3918
			internal const int cmdidViewKeys = 124;

			// Token: 0x04000F4F RID: 3919
			internal const int cmdidViewGrid = 125;

			// Token: 0x04000F50 RID: 3920
			internal const int cmdidInnerJoin = 126;

			// Token: 0x04000F51 RID: 3921
			internal const int cmdidRightOuterJoin = 127;

			// Token: 0x04000F52 RID: 3922
			internal const int cmdidLeftOuterJoin = 128;

			// Token: 0x04000F53 RID: 3923
			internal const int cmdidFullOuterJoin = 129;

			// Token: 0x04000F54 RID: 3924
			internal const int cmdidUnionJoin = 130;

			// Token: 0x04000F55 RID: 3925
			internal const int cmdidShowSQLPane = 131;

			// Token: 0x04000F56 RID: 3926
			internal const int cmdidShowGraphicalPane = 132;

			// Token: 0x04000F57 RID: 3927
			internal const int cmdidShowDataPane = 133;

			// Token: 0x04000F58 RID: 3928
			internal const int cmdidShowQBEPane = 134;

			// Token: 0x04000F59 RID: 3929
			internal const int cmdidSelectAllFields = 135;

			// Token: 0x04000F5A RID: 3930
			internal const int cmdidOLEObjectMenuButton = 136;

			// Token: 0x04000F5B RID: 3931
			internal const int cmdidObjectVerbList0 = 137;

			// Token: 0x04000F5C RID: 3932
			internal const int cmdidObjectVerbList1 = 138;

			// Token: 0x04000F5D RID: 3933
			internal const int cmdidObjectVerbList2 = 139;

			// Token: 0x04000F5E RID: 3934
			internal const int cmdidObjectVerbList3 = 140;

			// Token: 0x04000F5F RID: 3935
			internal const int cmdidObjectVerbList4 = 141;

			// Token: 0x04000F60 RID: 3936
			internal const int cmdidObjectVerbList5 = 142;

			// Token: 0x04000F61 RID: 3937
			internal const int cmdidObjectVerbList6 = 143;

			// Token: 0x04000F62 RID: 3938
			internal const int cmdidObjectVerbList7 = 144;

			// Token: 0x04000F63 RID: 3939
			internal const int cmdidObjectVerbList8 = 145;

			// Token: 0x04000F64 RID: 3940
			internal const int cmdidObjectVerbList9 = 146;

			// Token: 0x04000F65 RID: 3941
			internal const int cmdidConvertObject = 147;

			// Token: 0x04000F66 RID: 3942
			internal const int cmdidCustomControl = 148;

			// Token: 0x04000F67 RID: 3943
			internal const int cmdidCustomizeItem = 149;

			// Token: 0x04000F68 RID: 3944
			internal const int cmdidRename = 150;

			// Token: 0x04000F69 RID: 3945
			internal const int cmdidImport = 151;

			// Token: 0x04000F6A RID: 3946
			internal const int cmdidNewPage = 152;

			// Token: 0x04000F6B RID: 3947
			internal const int cmdidMove = 153;

			// Token: 0x04000F6C RID: 3948
			internal const int cmdidCancel = 154;

			// Token: 0x04000F6D RID: 3949
			internal const int cmdidFont = 155;

			// Token: 0x04000F6E RID: 3950
			internal const int cmdidExpandLinks = 156;

			// Token: 0x04000F6F RID: 3951
			internal const int cmdidExpandImages = 157;

			// Token: 0x04000F70 RID: 3952
			internal const int cmdidExpandPages = 158;

			// Token: 0x04000F71 RID: 3953
			internal const int cmdidRefocusDiagram = 159;

			// Token: 0x04000F72 RID: 3954
			internal const int cmdidTransitiveClosure = 160;

			// Token: 0x04000F73 RID: 3955
			internal const int cmdidCenterDiagram = 161;

			// Token: 0x04000F74 RID: 3956
			internal const int cmdidZoomIn = 162;

			// Token: 0x04000F75 RID: 3957
			internal const int cmdidZoomOut = 163;

			// Token: 0x04000F76 RID: 3958
			internal const int cmdidRemoveFilter = 164;

			// Token: 0x04000F77 RID: 3959
			internal const int cmdidHidePane = 165;

			// Token: 0x04000F78 RID: 3960
			internal const int cmdidDeleteTable = 166;

			// Token: 0x04000F79 RID: 3961
			internal const int cmdidDeleteRelationship = 167;

			// Token: 0x04000F7A RID: 3962
			internal const int cmdidRemove = 168;

			// Token: 0x04000F7B RID: 3963
			internal const int cmdidJoinLeftAll = 169;

			// Token: 0x04000F7C RID: 3964
			internal const int cmdidJoinRightAll = 170;

			// Token: 0x04000F7D RID: 3965
			internal const int cmdidAddToOutput = 171;

			// Token: 0x04000F7E RID: 3966
			internal const int cmdidOtherQuery = 172;

			// Token: 0x04000F7F RID: 3967
			internal const int cmdidGenerateChangeScript = 173;

			// Token: 0x04000F80 RID: 3968
			internal const int cmdidSaveSelection = 174;

			// Token: 0x04000F81 RID: 3969
			internal const int cmdidAutojoinCurrent = 175;

			// Token: 0x04000F82 RID: 3970
			internal const int cmdidAutojoinAlways = 176;

			// Token: 0x04000F83 RID: 3971
			internal const int cmdidEditPage = 177;

			// Token: 0x04000F84 RID: 3972
			internal const int cmdidViewLinks = 178;

			// Token: 0x04000F85 RID: 3973
			internal const int cmdidStop = 179;

			// Token: 0x04000F86 RID: 3974
			internal const int cmdidPause = 180;

			// Token: 0x04000F87 RID: 3975
			internal const int cmdidResume = 181;

			// Token: 0x04000F88 RID: 3976
			internal const int cmdidFilterDiagram = 182;

			// Token: 0x04000F89 RID: 3977
			internal const int cmdidShowAllObjects = 183;

			// Token: 0x04000F8A RID: 3978
			internal const int cmdidShowApplications = 184;

			// Token: 0x04000F8B RID: 3979
			internal const int cmdidShowOtherObjects = 185;

			// Token: 0x04000F8C RID: 3980
			internal const int cmdidShowPrimRelationships = 186;

			// Token: 0x04000F8D RID: 3981
			internal const int cmdidExpand = 187;

			// Token: 0x04000F8E RID: 3982
			internal const int cmdidCollapse = 188;

			// Token: 0x04000F8F RID: 3983
			internal const int cmdidRefresh = 189;

			// Token: 0x04000F90 RID: 3984
			internal const int cmdidLayout = 190;

			// Token: 0x04000F91 RID: 3985
			internal const int cmdidShowResources = 191;

			// Token: 0x04000F92 RID: 3986
			internal const int cmdidInsertHTMLWizard = 192;

			// Token: 0x04000F93 RID: 3987
			internal const int cmdidShowDownloads = 193;

			// Token: 0x04000F94 RID: 3988
			internal const int cmdidShowExternals = 194;

			// Token: 0x04000F95 RID: 3989
			internal const int cmdidShowInBoundLinks = 195;

			// Token: 0x04000F96 RID: 3990
			internal const int cmdidShowOutBoundLinks = 196;

			// Token: 0x04000F97 RID: 3991
			internal const int cmdidShowInAndOutBoundLinks = 197;

			// Token: 0x04000F98 RID: 3992
			internal const int cmdidPreview = 198;

			// Token: 0x04000F99 RID: 3993
			internal const int cmdidOpen = 261;

			// Token: 0x04000F9A RID: 3994
			internal const int cmdidOpenWith = 199;

			// Token: 0x04000F9B RID: 3995
			internal const int cmdidShowPages = 200;

			// Token: 0x04000F9C RID: 3996
			internal const int cmdidRunQuery = 201;

			// Token: 0x04000F9D RID: 3997
			internal const int cmdidClearQuery = 202;

			// Token: 0x04000F9E RID: 3998
			internal const int cmdidRecordFirst = 203;

			// Token: 0x04000F9F RID: 3999
			internal const int cmdidRecordLast = 204;

			// Token: 0x04000FA0 RID: 4000
			internal const int cmdidRecordNext = 205;

			// Token: 0x04000FA1 RID: 4001
			internal const int cmdidRecordPrevious = 206;

			// Token: 0x04000FA2 RID: 4002
			internal const int cmdidRecordGoto = 207;

			// Token: 0x04000FA3 RID: 4003
			internal const int cmdidRecordNew = 208;

			// Token: 0x04000FA4 RID: 4004
			internal const int cmdidInsertNewMenu = 209;

			// Token: 0x04000FA5 RID: 4005
			internal const int cmdidInsertSeparator = 210;

			// Token: 0x04000FA6 RID: 4006
			internal const int cmdidEditMenuNames = 211;

			// Token: 0x04000FA7 RID: 4007
			internal const int cmdidDebugExplorer = 212;

			// Token: 0x04000FA8 RID: 4008
			internal const int cmdidDebugProcesses = 213;

			// Token: 0x04000FA9 RID: 4009
			internal const int cmdidViewThreadsWindow = 214;

			// Token: 0x04000FAA RID: 4010
			internal const int cmdidWindowUIList = 215;

			// Token: 0x04000FAB RID: 4011
			internal const int cmdidNewProject = 216;

			// Token: 0x04000FAC RID: 4012
			internal const int cmdidOpenProject = 217;

			// Token: 0x04000FAD RID: 4013
			internal const int cmdidOpenSolution = 218;

			// Token: 0x04000FAE RID: 4014
			internal const int cmdidCloseSolution = 219;

			// Token: 0x04000FAF RID: 4015
			internal const int cmdidFileNew = 221;

			// Token: 0x04000FB0 RID: 4016
			internal const int cmdidFileOpen = 222;

			// Token: 0x04000FB1 RID: 4017
			internal const int cmdidFileClose = 223;

			// Token: 0x04000FB2 RID: 4018
			internal const int cmdidSaveSolution = 224;

			// Token: 0x04000FB3 RID: 4019
			internal const int cmdidSaveSolutionAs = 225;

			// Token: 0x04000FB4 RID: 4020
			internal const int cmdidSaveProjectItemAs = 226;

			// Token: 0x04000FB5 RID: 4021
			internal const int cmdidPageSetup = 227;

			// Token: 0x04000FB6 RID: 4022
			internal const int cmdidPrintPreview = 228;

			// Token: 0x04000FB7 RID: 4023
			internal const int cmdidExit = 229;

			// Token: 0x04000FB8 RID: 4024
			internal const int cmdidReplace = 230;

			// Token: 0x04000FB9 RID: 4025
			internal const int cmdidGoto = 231;

			// Token: 0x04000FBA RID: 4026
			internal const int cmdidPropertyPages = 232;

			// Token: 0x04000FBB RID: 4027
			internal const int cmdidFullScreen = 233;

			// Token: 0x04000FBC RID: 4028
			internal const int cmdidProjectExplorer = 234;

			// Token: 0x04000FBD RID: 4029
			internal const int cmdidPropertiesWindow = 235;

			// Token: 0x04000FBE RID: 4030
			internal const int cmdidTaskListWindow = 236;

			// Token: 0x04000FBF RID: 4031
			internal const int cmdidOutputWindow = 237;

			// Token: 0x04000FC0 RID: 4032
			internal const int cmdidObjectBrowser = 238;

			// Token: 0x04000FC1 RID: 4033
			internal const int cmdidDocOutlineWindow = 239;

			// Token: 0x04000FC2 RID: 4034
			internal const int cmdidImmediateWindow = 240;

			// Token: 0x04000FC3 RID: 4035
			internal const int cmdidWatchWindow = 241;

			// Token: 0x04000FC4 RID: 4036
			internal const int cmdidLocalsWindow = 242;

			// Token: 0x04000FC5 RID: 4037
			internal const int cmdidCallStack = 243;

			// Token: 0x04000FC6 RID: 4038
			internal const int cmdidAutosWindow = 747;

			// Token: 0x04000FC7 RID: 4039
			internal const int cmdidThisWindow = 748;

			// Token: 0x04000FC8 RID: 4040
			internal const int cmdidAddNewItem = 220;

			// Token: 0x04000FC9 RID: 4041
			internal const int cmdidAddExistingItem = 244;

			// Token: 0x04000FCA RID: 4042
			internal const int cmdidNewFolder = 245;

			// Token: 0x04000FCB RID: 4043
			internal const int cmdidSetStartupProject = 246;

			// Token: 0x04000FCC RID: 4044
			internal const int cmdidProjectSettings = 247;

			// Token: 0x04000FCD RID: 4045
			internal const int cmdidProjectReferences = 367;

			// Token: 0x04000FCE RID: 4046
			internal const int cmdidStepInto = 248;

			// Token: 0x04000FCF RID: 4047
			internal const int cmdidStepOver = 249;

			// Token: 0x04000FD0 RID: 4048
			internal const int cmdidStepOut = 250;

			// Token: 0x04000FD1 RID: 4049
			internal const int cmdidRunToCursor = 251;

			// Token: 0x04000FD2 RID: 4050
			internal const int cmdidAddWatch = 252;

			// Token: 0x04000FD3 RID: 4051
			internal const int cmdidEditWatch = 253;

			// Token: 0x04000FD4 RID: 4052
			internal const int cmdidQuickWatch = 254;

			// Token: 0x04000FD5 RID: 4053
			internal const int cmdidToggleBreakpoint = 255;

			// Token: 0x04000FD6 RID: 4054
			internal const int cmdidClearBreakpoints = 256;

			// Token: 0x04000FD7 RID: 4055
			internal const int cmdidShowBreakpoints = 257;

			// Token: 0x04000FD8 RID: 4056
			internal const int cmdidSetNextStatement = 258;

			// Token: 0x04000FD9 RID: 4057
			internal const int cmdidShowNextStatement = 259;

			// Token: 0x04000FDA RID: 4058
			internal const int cmdidEditBreakpoint = 260;

			// Token: 0x04000FDB RID: 4059
			internal const int cmdidDetachDebugger = 262;

			// Token: 0x04000FDC RID: 4060
			internal const int cmdidCustomizeKeyboard = 263;

			// Token: 0x04000FDD RID: 4061
			internal const int cmdidToolsOptions = 264;

			// Token: 0x04000FDE RID: 4062
			internal const int cmdidNewWindow = 265;

			// Token: 0x04000FDF RID: 4063
			internal const int cmdidSplit = 266;

			// Token: 0x04000FE0 RID: 4064
			internal const int cmdidCascade = 267;

			// Token: 0x04000FE1 RID: 4065
			internal const int cmdidTileHorz = 268;

			// Token: 0x04000FE2 RID: 4066
			internal const int cmdidTileVert = 269;

			// Token: 0x04000FE3 RID: 4067
			internal const int cmdidTechSupport = 270;

			// Token: 0x04000FE4 RID: 4068
			internal const int cmdidAbout = 271;

			// Token: 0x04000FE5 RID: 4069
			internal const int cmdidDebugOptions = 272;

			// Token: 0x04000FE6 RID: 4070
			internal const int cmdidDeleteWatch = 274;

			// Token: 0x04000FE7 RID: 4071
			internal const int cmdidCollapseWatch = 275;

			// Token: 0x04000FE8 RID: 4072
			internal const int cmdidPbrsToggleStatus = 282;

			// Token: 0x04000FE9 RID: 4073
			internal const int cmdidPropbrsHide = 283;

			// Token: 0x04000FEA RID: 4074
			internal const int cmdidDockingView = 284;

			// Token: 0x04000FEB RID: 4075
			internal const int cmdidHideActivePane = 285;

			// Token: 0x04000FEC RID: 4076
			internal const int cmdidPaneNextTab = 286;

			// Token: 0x04000FED RID: 4077
			internal const int cmdidPanePrevTab = 287;

			// Token: 0x04000FEE RID: 4078
			internal const int cmdidPaneCloseToolWindow = 288;

			// Token: 0x04000FEF RID: 4079
			internal const int cmdidPaneActivateDocWindow = 289;

			// Token: 0x04000FF0 RID: 4080
			internal const int cmdidDockingViewFloater = 291;

			// Token: 0x04000FF1 RID: 4081
			internal const int cmdidAutoHideWindow = 292;

			// Token: 0x04000FF2 RID: 4082
			internal const int cmdidMoveToDropdownBar = 293;

			// Token: 0x04000FF3 RID: 4083
			internal const int cmdidFindCmd = 294;

			// Token: 0x04000FF4 RID: 4084
			internal const int cmdidStart = 295;

			// Token: 0x04000FF5 RID: 4085
			internal const int cmdidRestart = 296;

			// Token: 0x04000FF6 RID: 4086
			internal const int cmdidAddinManager = 297;

			// Token: 0x04000FF7 RID: 4087
			internal const int cmdidMultiLevelUndoList = 298;

			// Token: 0x04000FF8 RID: 4088
			internal const int cmdidMultiLevelRedoList = 299;

			// Token: 0x04000FF9 RID: 4089
			internal const int cmdidToolboxAddTab = 300;

			// Token: 0x04000FFA RID: 4090
			internal const int cmdidToolboxDeleteTab = 301;

			// Token: 0x04000FFB RID: 4091
			internal const int cmdidToolboxRenameTab = 302;

			// Token: 0x04000FFC RID: 4092
			internal const int cmdidToolboxTabMoveUp = 303;

			// Token: 0x04000FFD RID: 4093
			internal const int cmdidToolboxTabMoveDown = 304;

			// Token: 0x04000FFE RID: 4094
			internal const int cmdidToolboxRenameItem = 305;

			// Token: 0x04000FFF RID: 4095
			internal const int cmdidToolboxListView = 306;

			// Token: 0x04001000 RID: 4096
			internal const int cmdidWindowUIGetList = 308;

			// Token: 0x04001001 RID: 4097
			internal const int cmdidInsertValuesQuery = 309;

			// Token: 0x04001002 RID: 4098
			internal const int cmdidShowProperties = 310;

			// Token: 0x04001003 RID: 4099
			internal const int cmdidThreadSuspend = 311;

			// Token: 0x04001004 RID: 4100
			internal const int cmdidThreadResume = 312;

			// Token: 0x04001005 RID: 4101
			internal const int cmdidThreadSetFocus = 313;

			// Token: 0x04001006 RID: 4102
			internal const int cmdidDisplayRadix = 314;

			// Token: 0x04001007 RID: 4103
			internal const int cmdidOpenProjectItem = 315;

			// Token: 0x04001008 RID: 4104
			internal const int cmdidPaneNextPane = 316;

			// Token: 0x04001009 RID: 4105
			internal const int cmdidPanePrevPane = 317;

			// Token: 0x0400100A RID: 4106
			internal const int cmdidClearPane = 318;

			// Token: 0x0400100B RID: 4107
			internal const int cmdidGotoErrorTag = 319;

			// Token: 0x0400100C RID: 4108
			internal const int cmdidTaskListSortByCategory = 320;

			// Token: 0x0400100D RID: 4109
			internal const int cmdidTaskListSortByFileLine = 321;

			// Token: 0x0400100E RID: 4110
			internal const int cmdidTaskListSortByPriority = 322;

			// Token: 0x0400100F RID: 4111
			internal const int cmdidTaskListSortByDefaultSort = 323;

			// Token: 0x04001010 RID: 4112
			internal const int cmdidTaskListFilterByNothing = 325;

			// Token: 0x04001011 RID: 4113
			internal const int cmdidTaskListFilterByCategoryCodeSense = 326;

			// Token: 0x04001012 RID: 4114
			internal const int cmdidTaskListFilterByCategoryCompiler = 327;

			// Token: 0x04001013 RID: 4115
			internal const int cmdidTaskListFilterByCategoryComment = 328;

			// Token: 0x04001014 RID: 4116
			internal const int cmdidToolboxAddItem = 329;

			// Token: 0x04001015 RID: 4117
			internal const int cmdidToolboxReset = 330;

			// Token: 0x04001016 RID: 4118
			internal const int cmdidSaveProjectItem = 331;

			// Token: 0x04001017 RID: 4119
			internal const int cmdidViewForm = 332;

			// Token: 0x04001018 RID: 4120
			internal const int cmdidViewCode = 333;

			// Token: 0x04001019 RID: 4121
			internal const int cmdidPreviewInBrowser = 334;

			// Token: 0x0400101A RID: 4122
			internal const int cmdidBrowseWith = 336;

			// Token: 0x0400101B RID: 4123
			internal const int cmdidSearchSetCombo = 307;

			// Token: 0x0400101C RID: 4124
			internal const int cmdidSearchCombo = 337;

			// Token: 0x0400101D RID: 4125
			internal const int cmdidEditLabel = 338;

			// Token: 0x0400101E RID: 4126
			internal const int cmdidExceptions = 339;

			// Token: 0x0400101F RID: 4127
			internal const int cmdidToggleSelMode = 341;

			// Token: 0x04001020 RID: 4128
			internal const int cmdidToggleInsMode = 342;

			// Token: 0x04001021 RID: 4129
			internal const int cmdidLoadUnloadedProject = 343;

			// Token: 0x04001022 RID: 4130
			internal const int cmdidUnloadLoadedProject = 344;

			// Token: 0x04001023 RID: 4131
			internal const int cmdidElasticColumn = 345;

			// Token: 0x04001024 RID: 4132
			internal const int cmdidHideColumn = 346;

			// Token: 0x04001025 RID: 4133
			internal const int cmdidTaskListPreviousView = 347;

			// Token: 0x04001026 RID: 4134
			internal const int cmdidZoomDialog = 348;

			// Token: 0x04001027 RID: 4135
			internal const int cmdidFindNew = 349;

			// Token: 0x04001028 RID: 4136
			internal const int cmdidFindMatchCase = 350;

			// Token: 0x04001029 RID: 4137
			internal const int cmdidFindWholeWord = 351;

			// Token: 0x0400102A RID: 4138
			internal const int cmdidFindSimplePattern = 276;

			// Token: 0x0400102B RID: 4139
			internal const int cmdidFindRegularExpression = 352;

			// Token: 0x0400102C RID: 4140
			internal const int cmdidFindBackwards = 353;

			// Token: 0x0400102D RID: 4141
			internal const int cmdidFindInSelection = 354;

			// Token: 0x0400102E RID: 4142
			internal const int cmdidFindStop = 355;

			// Token: 0x0400102F RID: 4143
			internal const int cmdidFindHelp = 356;

			// Token: 0x04001030 RID: 4144
			internal const int cmdidFindInFiles = 277;

			// Token: 0x04001031 RID: 4145
			internal const int cmdidReplaceInFiles = 278;

			// Token: 0x04001032 RID: 4146
			internal const int cmdidNextLocation = 279;

			// Token: 0x04001033 RID: 4147
			internal const int cmdidPreviousLocation = 280;

			// Token: 0x04001034 RID: 4148
			internal const int cmdidTaskListNextError = 357;

			// Token: 0x04001035 RID: 4149
			internal const int cmdidTaskListPrevError = 358;

			// Token: 0x04001036 RID: 4150
			internal const int cmdidTaskListFilterByCategoryUser = 359;

			// Token: 0x04001037 RID: 4151
			internal const int cmdidTaskListFilterByCategoryShortcut = 360;

			// Token: 0x04001038 RID: 4152
			internal const int cmdidTaskListFilterByCategoryHTML = 361;

			// Token: 0x04001039 RID: 4153
			internal const int cmdidTaskListFilterByCurrentFile = 362;

			// Token: 0x0400103A RID: 4154
			internal const int cmdidTaskListFilterByChecked = 363;

			// Token: 0x0400103B RID: 4155
			internal const int cmdidTaskListFilterByUnchecked = 364;

			// Token: 0x0400103C RID: 4156
			internal const int cmdidTaskListSortByDescription = 365;

			// Token: 0x0400103D RID: 4157
			internal const int cmdidTaskListSortByChecked = 366;

			// Token: 0x0400103E RID: 4158
			internal const int cmdidStartNoDebug = 368;

			// Token: 0x0400103F RID: 4159
			internal const int cmdidFindNext = 370;

			// Token: 0x04001040 RID: 4160
			internal const int cmdidFindPrev = 371;

			// Token: 0x04001041 RID: 4161
			internal const int cmdidFindSelectedNext = 372;

			// Token: 0x04001042 RID: 4162
			internal const int cmdidFindSelectedPrev = 373;

			// Token: 0x04001043 RID: 4163
			internal const int cmdidSearchGetList = 374;

			// Token: 0x04001044 RID: 4164
			internal const int cmdidInsertBreakpoint = 375;

			// Token: 0x04001045 RID: 4165
			internal const int cmdidEnableBreakpoint = 376;

			// Token: 0x04001046 RID: 4166
			internal const int cmdidF1Help = 377;

			// Token: 0x04001047 RID: 4167
			internal const int cmdidPropSheetOrProperties = 397;

			// Token: 0x04001048 RID: 4168
			internal const int cmdidTshellStep = 398;

			// Token: 0x04001049 RID: 4169
			internal const int cmdidTshellRun = 399;

			// Token: 0x0400104A RID: 4170
			internal const int cmdidMarkerCmd0 = 400;

			// Token: 0x0400104B RID: 4171
			internal const int cmdidMarkerCmd1 = 401;

			// Token: 0x0400104C RID: 4172
			internal const int cmdidMarkerCmd2 = 402;

			// Token: 0x0400104D RID: 4173
			internal const int cmdidMarkerCmd3 = 403;

			// Token: 0x0400104E RID: 4174
			internal const int cmdidMarkerCmd4 = 404;

			// Token: 0x0400104F RID: 4175
			internal const int cmdidMarkerCmd5 = 405;

			// Token: 0x04001050 RID: 4176
			internal const int cmdidMarkerCmd6 = 406;

			// Token: 0x04001051 RID: 4177
			internal const int cmdidMarkerCmd7 = 407;

			// Token: 0x04001052 RID: 4178
			internal const int cmdidMarkerCmd8 = 408;

			// Token: 0x04001053 RID: 4179
			internal const int cmdidMarkerCmd9 = 409;

			// Token: 0x04001054 RID: 4180
			internal const int cmdidMarkerLast = 409;

			// Token: 0x04001055 RID: 4181
			internal const int cmdidMarkerEnd = 410;

			// Token: 0x04001056 RID: 4182
			internal const int cmdidReloadProject = 412;

			// Token: 0x04001057 RID: 4183
			internal const int cmdidUnloadProject = 413;

			// Token: 0x04001058 RID: 4184
			internal const int cmdidDetachAttachOutline = 420;

			// Token: 0x04001059 RID: 4185
			internal const int cmdidShowHideOutline = 421;

			// Token: 0x0400105A RID: 4186
			internal const int cmdidSyncOutline = 422;

			// Token: 0x0400105B RID: 4187
			internal const int cmdidRunToCallstCursor = 423;

			// Token: 0x0400105C RID: 4188
			internal const int cmdidNoCmdsAvailable = 424;

			// Token: 0x0400105D RID: 4189
			internal const int cmdidContextWindow = 427;

			// Token: 0x0400105E RID: 4190
			internal const int cmdidAlias = 428;

			// Token: 0x0400105F RID: 4191
			internal const int cmdidGotoCommandLine = 429;

			// Token: 0x04001060 RID: 4192
			internal const int cmdidEvaluateExpression = 430;

			// Token: 0x04001061 RID: 4193
			internal const int cmdidImmediateMode = 431;

			// Token: 0x04001062 RID: 4194
			internal const int cmdidEvaluateStatement = 432;

			// Token: 0x04001063 RID: 4195
			internal const int cmdidFindResultWindow1 = 433;

			// Token: 0x04001064 RID: 4196
			internal const int cmdidFindResultWindow2 = 434;

			// Token: 0x04001065 RID: 4197
			internal const int cmdidWindow1 = 570;

			// Token: 0x04001066 RID: 4198
			internal const int cmdidWindow2 = 571;

			// Token: 0x04001067 RID: 4199
			internal const int cmdidWindow3 = 572;

			// Token: 0x04001068 RID: 4200
			internal const int cmdidWindow4 = 573;

			// Token: 0x04001069 RID: 4201
			internal const int cmdidWindow5 = 574;

			// Token: 0x0400106A RID: 4202
			internal const int cmdidWindow6 = 575;

			// Token: 0x0400106B RID: 4203
			internal const int cmdidWindow7 = 576;

			// Token: 0x0400106C RID: 4204
			internal const int cmdidWindow8 = 577;

			// Token: 0x0400106D RID: 4205
			internal const int cmdidWindow9 = 578;

			// Token: 0x0400106E RID: 4206
			internal const int cmdidWindow10 = 579;

			// Token: 0x0400106F RID: 4207
			internal const int cmdidWindow11 = 580;

			// Token: 0x04001070 RID: 4208
			internal const int cmdidWindow12 = 581;

			// Token: 0x04001071 RID: 4209
			internal const int cmdidWindow13 = 582;

			// Token: 0x04001072 RID: 4210
			internal const int cmdidWindow14 = 583;

			// Token: 0x04001073 RID: 4211
			internal const int cmdidWindow15 = 584;

			// Token: 0x04001074 RID: 4212
			internal const int cmdidWindow16 = 585;

			// Token: 0x04001075 RID: 4213
			internal const int cmdidWindow17 = 586;

			// Token: 0x04001076 RID: 4214
			internal const int cmdidWindow18 = 587;

			// Token: 0x04001077 RID: 4215
			internal const int cmdidWindow19 = 588;

			// Token: 0x04001078 RID: 4216
			internal const int cmdidWindow20 = 589;

			// Token: 0x04001079 RID: 4217
			internal const int cmdidWindow21 = 590;

			// Token: 0x0400107A RID: 4218
			internal const int cmdidWindow22 = 591;

			// Token: 0x0400107B RID: 4219
			internal const int cmdidWindow23 = 592;

			// Token: 0x0400107C RID: 4220
			internal const int cmdidWindow24 = 593;

			// Token: 0x0400107D RID: 4221
			internal const int cmdidWindow25 = 594;

			// Token: 0x0400107E RID: 4222
			internal const int cmdidMoreWindows = 595;

			// Token: 0x0400107F RID: 4223
			internal const int cmdidTaskListTaskHelp = 598;

			// Token: 0x04001080 RID: 4224
			internal const int cmdidClassView = 599;

			// Token: 0x04001081 RID: 4225
			internal const int cmdidMRUProj1 = 600;

			// Token: 0x04001082 RID: 4226
			internal const int cmdidMRUProj2 = 601;

			// Token: 0x04001083 RID: 4227
			internal const int cmdidMRUProj3 = 602;

			// Token: 0x04001084 RID: 4228
			internal const int cmdidMRUProj4 = 603;

			// Token: 0x04001085 RID: 4229
			internal const int cmdidMRUProj5 = 604;

			// Token: 0x04001086 RID: 4230
			internal const int cmdidMRUProj6 = 605;

			// Token: 0x04001087 RID: 4231
			internal const int cmdidMRUProj7 = 606;

			// Token: 0x04001088 RID: 4232
			internal const int cmdidMRUProj8 = 607;

			// Token: 0x04001089 RID: 4233
			internal const int cmdidMRUProj9 = 608;

			// Token: 0x0400108A RID: 4234
			internal const int cmdidMRUProj10 = 609;

			// Token: 0x0400108B RID: 4235
			internal const int cmdidMRUProj11 = 610;

			// Token: 0x0400108C RID: 4236
			internal const int cmdidMRUProj12 = 611;

			// Token: 0x0400108D RID: 4237
			internal const int cmdidMRUProj13 = 612;

			// Token: 0x0400108E RID: 4238
			internal const int cmdidMRUProj14 = 613;

			// Token: 0x0400108F RID: 4239
			internal const int cmdidMRUProj15 = 614;

			// Token: 0x04001090 RID: 4240
			internal const int cmdidMRUProj16 = 615;

			// Token: 0x04001091 RID: 4241
			internal const int cmdidMRUProj17 = 616;

			// Token: 0x04001092 RID: 4242
			internal const int cmdidMRUProj18 = 617;

			// Token: 0x04001093 RID: 4243
			internal const int cmdidMRUProj19 = 618;

			// Token: 0x04001094 RID: 4244
			internal const int cmdidMRUProj20 = 619;

			// Token: 0x04001095 RID: 4245
			internal const int cmdidMRUProj21 = 620;

			// Token: 0x04001096 RID: 4246
			internal const int cmdidMRUProj22 = 621;

			// Token: 0x04001097 RID: 4247
			internal const int cmdidMRUProj23 = 622;

			// Token: 0x04001098 RID: 4248
			internal const int cmdidMRUProj24 = 623;

			// Token: 0x04001099 RID: 4249
			internal const int cmdidMRUProj25 = 624;

			// Token: 0x0400109A RID: 4250
			internal const int cmdidSplitNext = 625;

			// Token: 0x0400109B RID: 4251
			internal const int cmdidSplitPrev = 626;

			// Token: 0x0400109C RID: 4252
			internal const int cmdidCloseAllDocuments = 627;

			// Token: 0x0400109D RID: 4253
			internal const int cmdidNextDocument = 628;

			// Token: 0x0400109E RID: 4254
			internal const int cmdidPrevDocument = 629;

			// Token: 0x0400109F RID: 4255
			internal const int cmdidTool1 = 630;

			// Token: 0x040010A0 RID: 4256
			internal const int cmdidTool2 = 631;

			// Token: 0x040010A1 RID: 4257
			internal const int cmdidTool3 = 632;

			// Token: 0x040010A2 RID: 4258
			internal const int cmdidTool4 = 633;

			// Token: 0x040010A3 RID: 4259
			internal const int cmdidTool5 = 634;

			// Token: 0x040010A4 RID: 4260
			internal const int cmdidTool6 = 635;

			// Token: 0x040010A5 RID: 4261
			internal const int cmdidTool7 = 636;

			// Token: 0x040010A6 RID: 4262
			internal const int cmdidTool8 = 637;

			// Token: 0x040010A7 RID: 4263
			internal const int cmdidTool9 = 638;

			// Token: 0x040010A8 RID: 4264
			internal const int cmdidTool10 = 639;

			// Token: 0x040010A9 RID: 4265
			internal const int cmdidTool11 = 640;

			// Token: 0x040010AA RID: 4266
			internal const int cmdidTool12 = 641;

			// Token: 0x040010AB RID: 4267
			internal const int cmdidTool13 = 642;

			// Token: 0x040010AC RID: 4268
			internal const int cmdidTool14 = 643;

			// Token: 0x040010AD RID: 4269
			internal const int cmdidTool15 = 644;

			// Token: 0x040010AE RID: 4270
			internal const int cmdidTool16 = 645;

			// Token: 0x040010AF RID: 4271
			internal const int cmdidTool17 = 646;

			// Token: 0x040010B0 RID: 4272
			internal const int cmdidTool18 = 647;

			// Token: 0x040010B1 RID: 4273
			internal const int cmdidTool19 = 648;

			// Token: 0x040010B2 RID: 4274
			internal const int cmdidTool20 = 649;

			// Token: 0x040010B3 RID: 4275
			internal const int cmdidTool21 = 650;

			// Token: 0x040010B4 RID: 4276
			internal const int cmdidTool22 = 651;

			// Token: 0x040010B5 RID: 4277
			internal const int cmdidTool23 = 652;

			// Token: 0x040010B6 RID: 4278
			internal const int cmdidTool24 = 653;

			// Token: 0x040010B7 RID: 4279
			internal const int cmdidExternalCommands = 654;

			// Token: 0x040010B8 RID: 4280
			internal const int cmdidPasteNextTBXCBItem = 655;

			// Token: 0x040010B9 RID: 4281
			internal const int cmdidToolboxShowAllTabs = 656;

			// Token: 0x040010BA RID: 4282
			internal const int cmdidProjectDependencies = 657;

			// Token: 0x040010BB RID: 4283
			internal const int cmdidCloseDocument = 658;

			// Token: 0x040010BC RID: 4284
			internal const int cmdidToolboxSortItems = 659;

			// Token: 0x040010BD RID: 4285
			internal const int cmdidViewBarView1 = 660;

			// Token: 0x040010BE RID: 4286
			internal const int cmdidViewBarView2 = 661;

			// Token: 0x040010BF RID: 4287
			internal const int cmdidViewBarView3 = 662;

			// Token: 0x040010C0 RID: 4288
			internal const int cmdidViewBarView4 = 663;

			// Token: 0x040010C1 RID: 4289
			internal const int cmdidViewBarView5 = 664;

			// Token: 0x040010C2 RID: 4290
			internal const int cmdidViewBarView6 = 665;

			// Token: 0x040010C3 RID: 4291
			internal const int cmdidViewBarView7 = 666;

			// Token: 0x040010C4 RID: 4292
			internal const int cmdidViewBarView8 = 667;

			// Token: 0x040010C5 RID: 4293
			internal const int cmdidViewBarView9 = 668;

			// Token: 0x040010C6 RID: 4294
			internal const int cmdidViewBarView10 = 669;

			// Token: 0x040010C7 RID: 4295
			internal const int cmdidViewBarView11 = 670;

			// Token: 0x040010C8 RID: 4296
			internal const int cmdidViewBarView12 = 671;

			// Token: 0x040010C9 RID: 4297
			internal const int cmdidViewBarView13 = 672;

			// Token: 0x040010CA RID: 4298
			internal const int cmdidViewBarView14 = 673;

			// Token: 0x040010CB RID: 4299
			internal const int cmdidViewBarView15 = 674;

			// Token: 0x040010CC RID: 4300
			internal const int cmdidViewBarView16 = 675;

			// Token: 0x040010CD RID: 4301
			internal const int cmdidViewBarView17 = 676;

			// Token: 0x040010CE RID: 4302
			internal const int cmdidViewBarView18 = 677;

			// Token: 0x040010CF RID: 4303
			internal const int cmdidViewBarView19 = 678;

			// Token: 0x040010D0 RID: 4304
			internal const int cmdidViewBarView20 = 679;

			// Token: 0x040010D1 RID: 4305
			internal const int cmdidViewBarView21 = 680;

			// Token: 0x040010D2 RID: 4306
			internal const int cmdidViewBarView22 = 681;

			// Token: 0x040010D3 RID: 4307
			internal const int cmdidViewBarView23 = 682;

			// Token: 0x040010D4 RID: 4308
			internal const int cmdidViewBarView24 = 683;

			// Token: 0x040010D5 RID: 4309
			internal const int cmdidSolutionCfg = 684;

			// Token: 0x040010D6 RID: 4310
			internal const int cmdidSolutionCfgGetList = 685;

			// Token: 0x040010D7 RID: 4311
			internal const int cmdidManageIndexes = 675;

			// Token: 0x040010D8 RID: 4312
			internal const int cmdidManageRelationships = 676;

			// Token: 0x040010D9 RID: 4313
			internal const int cmdidManageConstraints = 677;

			// Token: 0x040010DA RID: 4314
			internal const int cmdidTaskListCustomView1 = 678;

			// Token: 0x040010DB RID: 4315
			internal const int cmdidTaskListCustomView2 = 679;

			// Token: 0x040010DC RID: 4316
			internal const int cmdidTaskListCustomView3 = 680;

			// Token: 0x040010DD RID: 4317
			internal const int cmdidTaskListCustomView4 = 681;

			// Token: 0x040010DE RID: 4318
			internal const int cmdidTaskListCustomView5 = 682;

			// Token: 0x040010DF RID: 4319
			internal const int cmdidTaskListCustomView6 = 683;

			// Token: 0x040010E0 RID: 4320
			internal const int cmdidTaskListCustomView7 = 684;

			// Token: 0x040010E1 RID: 4321
			internal const int cmdidTaskListCustomView8 = 685;

			// Token: 0x040010E2 RID: 4322
			internal const int cmdidTaskListCustomView9 = 686;

			// Token: 0x040010E3 RID: 4323
			internal const int cmdidTaskListCustomView10 = 687;

			// Token: 0x040010E4 RID: 4324
			internal const int cmdidTaskListCustomView11 = 688;

			// Token: 0x040010E5 RID: 4325
			internal const int cmdidTaskListCustomView12 = 689;

			// Token: 0x040010E6 RID: 4326
			internal const int cmdidTaskListCustomView13 = 690;

			// Token: 0x040010E7 RID: 4327
			internal const int cmdidTaskListCustomView14 = 691;

			// Token: 0x040010E8 RID: 4328
			internal const int cmdidTaskListCustomView15 = 692;

			// Token: 0x040010E9 RID: 4329
			internal const int cmdidTaskListCustomView16 = 693;

			// Token: 0x040010EA RID: 4330
			internal const int cmdidTaskListCustomView17 = 694;

			// Token: 0x040010EB RID: 4331
			internal const int cmdidTaskListCustomView18 = 695;

			// Token: 0x040010EC RID: 4332
			internal const int cmdidTaskListCustomView19 = 696;

			// Token: 0x040010ED RID: 4333
			internal const int cmdidTaskListCustomView20 = 697;

			// Token: 0x040010EE RID: 4334
			internal const int cmdidTaskListCustomView21 = 698;

			// Token: 0x040010EF RID: 4335
			internal const int cmdidTaskListCustomView22 = 699;

			// Token: 0x040010F0 RID: 4336
			internal const int cmdidTaskListCustomView23 = 700;

			// Token: 0x040010F1 RID: 4337
			internal const int cmdidTaskListCustomView24 = 701;

			// Token: 0x040010F2 RID: 4338
			internal const int cmdidTaskListCustomView25 = 702;

			// Token: 0x040010F3 RID: 4339
			internal const int cmdidTaskListCustomView26 = 703;

			// Token: 0x040010F4 RID: 4340
			internal const int cmdidTaskListCustomView27 = 704;

			// Token: 0x040010F5 RID: 4341
			internal const int cmdidTaskListCustomView28 = 705;

			// Token: 0x040010F6 RID: 4342
			internal const int cmdidTaskListCustomView29 = 706;

			// Token: 0x040010F7 RID: 4343
			internal const int cmdidTaskListCustomView30 = 707;

			// Token: 0x040010F8 RID: 4344
			internal const int cmdidTaskListCustomView31 = 708;

			// Token: 0x040010F9 RID: 4345
			internal const int cmdidTaskListCustomView32 = 709;

			// Token: 0x040010FA RID: 4346
			internal const int cmdidTaskListCustomView33 = 710;

			// Token: 0x040010FB RID: 4347
			internal const int cmdidTaskListCustomView34 = 711;

			// Token: 0x040010FC RID: 4348
			internal const int cmdidTaskListCustomView35 = 712;

			// Token: 0x040010FD RID: 4349
			internal const int cmdidTaskListCustomView36 = 713;

			// Token: 0x040010FE RID: 4350
			internal const int cmdidTaskListCustomView37 = 714;

			// Token: 0x040010FF RID: 4351
			internal const int cmdidTaskListCustomView38 = 715;

			// Token: 0x04001100 RID: 4352
			internal const int cmdidTaskListCustomView39 = 716;

			// Token: 0x04001101 RID: 4353
			internal const int cmdidTaskListCustomView40 = 717;

			// Token: 0x04001102 RID: 4354
			internal const int cmdidTaskListCustomView41 = 718;

			// Token: 0x04001103 RID: 4355
			internal const int cmdidTaskListCustomView42 = 719;

			// Token: 0x04001104 RID: 4356
			internal const int cmdidTaskListCustomView43 = 720;

			// Token: 0x04001105 RID: 4357
			internal const int cmdidTaskListCustomView44 = 721;

			// Token: 0x04001106 RID: 4358
			internal const int cmdidTaskListCustomView45 = 722;

			// Token: 0x04001107 RID: 4359
			internal const int cmdidTaskListCustomView46 = 723;

			// Token: 0x04001108 RID: 4360
			internal const int cmdidTaskListCustomView47 = 724;

			// Token: 0x04001109 RID: 4361
			internal const int cmdidTaskListCustomView48 = 725;

			// Token: 0x0400110A RID: 4362
			internal const int cmdidTaskListCustomView49 = 726;

			// Token: 0x0400110B RID: 4363
			internal const int cmdidTaskListCustomView50 = 727;

			// Token: 0x0400110C RID: 4364
			internal const int cmdidObjectSearch = 728;

			// Token: 0x0400110D RID: 4365
			internal const int cmdidCommandWindow = 729;

			// Token: 0x0400110E RID: 4366
			internal const int cmdidCommandWindowMarkMode = 730;

			// Token: 0x0400110F RID: 4367
			internal const int cmdidLogCommandWindow = 731;

			// Token: 0x04001110 RID: 4368
			internal const int cmdidShell = 732;

			// Token: 0x04001111 RID: 4369
			internal const int cmdidSingleChar = 733;

			// Token: 0x04001112 RID: 4370
			internal const int cmdidZeroOrMore = 734;

			// Token: 0x04001113 RID: 4371
			internal const int cmdidOneOrMore = 735;

			// Token: 0x04001114 RID: 4372
			internal const int cmdidBeginLine = 736;

			// Token: 0x04001115 RID: 4373
			internal const int cmdidEndLine = 737;

			// Token: 0x04001116 RID: 4374
			internal const int cmdidBeginWord = 738;

			// Token: 0x04001117 RID: 4375
			internal const int cmdidEndWord = 739;

			// Token: 0x04001118 RID: 4376
			internal const int cmdidCharInSet = 740;

			// Token: 0x04001119 RID: 4377
			internal const int cmdidCharNotInSet = 741;

			// Token: 0x0400111A RID: 4378
			internal const int cmdidOr = 742;

			// Token: 0x0400111B RID: 4379
			internal const int cmdidEscape = 743;

			// Token: 0x0400111C RID: 4380
			internal const int cmdidTagExp = 744;

			// Token: 0x0400111D RID: 4381
			internal const int cmdidPatternMatchHelp = 745;

			// Token: 0x0400111E RID: 4382
			internal const int cmdidRegExList = 746;

			// Token: 0x0400111F RID: 4383
			internal const int cmdidDebugReserved1 = 747;

			// Token: 0x04001120 RID: 4384
			internal const int cmdidDebugReserved2 = 748;

			// Token: 0x04001121 RID: 4385
			internal const int cmdidDebugReserved3 = 749;

			// Token: 0x04001122 RID: 4386
			internal const int cmdidWildZeroOrMore = 754;

			// Token: 0x04001123 RID: 4387
			internal const int cmdidWildSingleChar = 755;

			// Token: 0x04001124 RID: 4388
			internal const int cmdidWildSingleDigit = 756;

			// Token: 0x04001125 RID: 4389
			internal const int cmdidWildCharInSet = 757;

			// Token: 0x04001126 RID: 4390
			internal const int cmdidWildCharNotInSet = 758;

			// Token: 0x04001127 RID: 4391
			internal const int cmdidFindWhatText = 759;

			// Token: 0x04001128 RID: 4392
			internal const int cmdidTaggedExp1 = 760;

			// Token: 0x04001129 RID: 4393
			internal const int cmdidTaggedExp2 = 761;

			// Token: 0x0400112A RID: 4394
			internal const int cmdidTaggedExp3 = 762;

			// Token: 0x0400112B RID: 4395
			internal const int cmdidTaggedExp4 = 763;

			// Token: 0x0400112C RID: 4396
			internal const int cmdidTaggedExp5 = 764;

			// Token: 0x0400112D RID: 4397
			internal const int cmdidTaggedExp6 = 765;

			// Token: 0x0400112E RID: 4398
			internal const int cmdidTaggedExp7 = 766;

			// Token: 0x0400112F RID: 4399
			internal const int cmdidTaggedExp8 = 767;

			// Token: 0x04001130 RID: 4400
			internal const int cmdidTaggedExp9 = 768;

			// Token: 0x04001131 RID: 4401
			internal const int cmdidEditorWidgetClick = 769;

			// Token: 0x04001132 RID: 4402
			internal const int cmdidCmdWinUpdateAC = 770;

			// Token: 0x04001133 RID: 4403
			internal const int cmdidSlnCfgMgr = 771;

			// Token: 0x04001134 RID: 4404
			internal const int cmdidAddNewProject = 772;

			// Token: 0x04001135 RID: 4405
			internal const int cmdidAddExistingProject = 773;

			// Token: 0x04001136 RID: 4406
			internal const int cmdidAddNewSolutionItem = 774;

			// Token: 0x04001137 RID: 4407
			internal const int cmdidAddExistingSolutionItem = 775;

			// Token: 0x04001138 RID: 4408
			internal const int cmdidAutoHideContext1 = 776;

			// Token: 0x04001139 RID: 4409
			internal const int cmdidAutoHideContext2 = 777;

			// Token: 0x0400113A RID: 4410
			internal const int cmdidAutoHideContext3 = 778;

			// Token: 0x0400113B RID: 4411
			internal const int cmdidAutoHideContext4 = 779;

			// Token: 0x0400113C RID: 4412
			internal const int cmdidAutoHideContext5 = 780;

			// Token: 0x0400113D RID: 4413
			internal const int cmdidAutoHideContext6 = 781;

			// Token: 0x0400113E RID: 4414
			internal const int cmdidAutoHideContext7 = 782;

			// Token: 0x0400113F RID: 4415
			internal const int cmdidAutoHideContext8 = 783;

			// Token: 0x04001140 RID: 4416
			internal const int cmdidAutoHideContext9 = 784;

			// Token: 0x04001141 RID: 4417
			internal const int cmdidAutoHideContext10 = 785;

			// Token: 0x04001142 RID: 4418
			internal const int cmdidAutoHideContext11 = 786;

			// Token: 0x04001143 RID: 4419
			internal const int cmdidAutoHideContext12 = 787;

			// Token: 0x04001144 RID: 4420
			internal const int cmdidAutoHideContext13 = 788;

			// Token: 0x04001145 RID: 4421
			internal const int cmdidAutoHideContext14 = 789;

			// Token: 0x04001146 RID: 4422
			internal const int cmdidAutoHideContext15 = 790;

			// Token: 0x04001147 RID: 4423
			internal const int cmdidAutoHideContext16 = 791;

			// Token: 0x04001148 RID: 4424
			internal const int cmdidAutoHideContext17 = 792;

			// Token: 0x04001149 RID: 4425
			internal const int cmdidAutoHideContext18 = 793;

			// Token: 0x0400114A RID: 4426
			internal const int cmdidAutoHideContext19 = 794;

			// Token: 0x0400114B RID: 4427
			internal const int cmdidAutoHideContext20 = 795;

			// Token: 0x0400114C RID: 4428
			internal const int cmdidAutoHideContext21 = 796;

			// Token: 0x0400114D RID: 4429
			internal const int cmdidAutoHideContext22 = 797;

			// Token: 0x0400114E RID: 4430
			internal const int cmdidAutoHideContext23 = 798;

			// Token: 0x0400114F RID: 4431
			internal const int cmdidAutoHideContext24 = 799;

			// Token: 0x04001150 RID: 4432
			internal const int cmdidAutoHideContext25 = 800;

			// Token: 0x04001151 RID: 4433
			internal const int cmdidAutoHideContext26 = 801;

			// Token: 0x04001152 RID: 4434
			internal const int cmdidAutoHideContext27 = 802;

			// Token: 0x04001153 RID: 4435
			internal const int cmdidAutoHideContext28 = 803;

			// Token: 0x04001154 RID: 4436
			internal const int cmdidAutoHideContext29 = 804;

			// Token: 0x04001155 RID: 4437
			internal const int cmdidAutoHideContext30 = 805;

			// Token: 0x04001156 RID: 4438
			internal const int cmdidAutoHideContext31 = 806;

			// Token: 0x04001157 RID: 4439
			internal const int cmdidAutoHideContext32 = 807;

			// Token: 0x04001158 RID: 4440
			internal const int cmdidAutoHideContext33 = 808;

			// Token: 0x04001159 RID: 4441
			internal const int cmdidShellNavBackward = 809;

			// Token: 0x0400115A RID: 4442
			internal const int cmdidShellNavForward = 810;

			// Token: 0x0400115B RID: 4443
			internal const int cmdidShellNavigate1 = 811;

			// Token: 0x0400115C RID: 4444
			internal const int cmdidShellNavigate2 = 812;

			// Token: 0x0400115D RID: 4445
			internal const int cmdidShellNavigate3 = 813;

			// Token: 0x0400115E RID: 4446
			internal const int cmdidShellNavigate4 = 814;

			// Token: 0x0400115F RID: 4447
			internal const int cmdidShellNavigate5 = 815;

			// Token: 0x04001160 RID: 4448
			internal const int cmdidShellNavigate6 = 816;

			// Token: 0x04001161 RID: 4449
			internal const int cmdidShellNavigate7 = 817;

			// Token: 0x04001162 RID: 4450
			internal const int cmdidShellNavigate8 = 818;

			// Token: 0x04001163 RID: 4451
			internal const int cmdidShellNavigate9 = 819;

			// Token: 0x04001164 RID: 4452
			internal const int cmdidShellNavigate10 = 820;

			// Token: 0x04001165 RID: 4453
			internal const int cmdidShellNavigate11 = 821;

			// Token: 0x04001166 RID: 4454
			internal const int cmdidShellNavigate12 = 822;

			// Token: 0x04001167 RID: 4455
			internal const int cmdidShellNavigate13 = 823;

			// Token: 0x04001168 RID: 4456
			internal const int cmdidShellNavigate14 = 824;

			// Token: 0x04001169 RID: 4457
			internal const int cmdidShellNavigate15 = 825;

			// Token: 0x0400116A RID: 4458
			internal const int cmdidShellNavigate16 = 826;

			// Token: 0x0400116B RID: 4459
			internal const int cmdidShellNavigate17 = 827;

			// Token: 0x0400116C RID: 4460
			internal const int cmdidShellNavigate18 = 828;

			// Token: 0x0400116D RID: 4461
			internal const int cmdidShellNavigate19 = 829;

			// Token: 0x0400116E RID: 4462
			internal const int cmdidShellNavigate20 = 830;

			// Token: 0x0400116F RID: 4463
			internal const int cmdidShellNavigate21 = 831;

			// Token: 0x04001170 RID: 4464
			internal const int cmdidShellNavigate22 = 832;

			// Token: 0x04001171 RID: 4465
			internal const int cmdidShellNavigate23 = 833;

			// Token: 0x04001172 RID: 4466
			internal const int cmdidShellNavigate24 = 834;

			// Token: 0x04001173 RID: 4467
			internal const int cmdidShellNavigate25 = 835;

			// Token: 0x04001174 RID: 4468
			internal const int cmdidShellNavigate26 = 836;

			// Token: 0x04001175 RID: 4469
			internal const int cmdidShellNavigate27 = 837;

			// Token: 0x04001176 RID: 4470
			internal const int cmdidShellNavigate28 = 838;

			// Token: 0x04001177 RID: 4471
			internal const int cmdidShellNavigate29 = 839;

			// Token: 0x04001178 RID: 4472
			internal const int cmdidShellNavigate30 = 840;

			// Token: 0x04001179 RID: 4473
			internal const int cmdidShellNavigate31 = 841;

			// Token: 0x0400117A RID: 4474
			internal const int cmdidShellNavigate32 = 842;

			// Token: 0x0400117B RID: 4475
			internal const int cmdidShellNavigate33 = 843;

			// Token: 0x0400117C RID: 4476
			internal const int cmdidShellWindowNavigate1 = 844;

			// Token: 0x0400117D RID: 4477
			internal const int cmdidShellWindowNavigate2 = 845;

			// Token: 0x0400117E RID: 4478
			internal const int cmdidShellWindowNavigate3 = 846;

			// Token: 0x0400117F RID: 4479
			internal const int cmdidShellWindowNavigate4 = 847;

			// Token: 0x04001180 RID: 4480
			internal const int cmdidShellWindowNavigate5 = 848;

			// Token: 0x04001181 RID: 4481
			internal const int cmdidShellWindowNavigate6 = 849;

			// Token: 0x04001182 RID: 4482
			internal const int cmdidShellWindowNavigate7 = 850;

			// Token: 0x04001183 RID: 4483
			internal const int cmdidShellWindowNavigate8 = 851;

			// Token: 0x04001184 RID: 4484
			internal const int cmdidShellWindowNavigate9 = 852;

			// Token: 0x04001185 RID: 4485
			internal const int cmdidShellWindowNavigate10 = 853;

			// Token: 0x04001186 RID: 4486
			internal const int cmdidShellWindowNavigate11 = 854;

			// Token: 0x04001187 RID: 4487
			internal const int cmdidShellWindowNavigate12 = 855;

			// Token: 0x04001188 RID: 4488
			internal const int cmdidShellWindowNavigate13 = 856;

			// Token: 0x04001189 RID: 4489
			internal const int cmdidShellWindowNavigate14 = 857;

			// Token: 0x0400118A RID: 4490
			internal const int cmdidShellWindowNavigate15 = 858;

			// Token: 0x0400118B RID: 4491
			internal const int cmdidShellWindowNavigate16 = 859;

			// Token: 0x0400118C RID: 4492
			internal const int cmdidShellWindowNavigate17 = 860;

			// Token: 0x0400118D RID: 4493
			internal const int cmdidShellWindowNavigate18 = 861;

			// Token: 0x0400118E RID: 4494
			internal const int cmdidShellWindowNavigate19 = 862;

			// Token: 0x0400118F RID: 4495
			internal const int cmdidShellWindowNavigate20 = 863;

			// Token: 0x04001190 RID: 4496
			internal const int cmdidShellWindowNavigate21 = 864;

			// Token: 0x04001191 RID: 4497
			internal const int cmdidShellWindowNavigate22 = 865;

			// Token: 0x04001192 RID: 4498
			internal const int cmdidShellWindowNavigate23 = 866;

			// Token: 0x04001193 RID: 4499
			internal const int cmdidShellWindowNavigate24 = 867;

			// Token: 0x04001194 RID: 4500
			internal const int cmdidShellWindowNavigate25 = 868;

			// Token: 0x04001195 RID: 4501
			internal const int cmdidShellWindowNavigate26 = 869;

			// Token: 0x04001196 RID: 4502
			internal const int cmdidShellWindowNavigate27 = 870;

			// Token: 0x04001197 RID: 4503
			internal const int cmdidShellWindowNavigate28 = 871;

			// Token: 0x04001198 RID: 4504
			internal const int cmdidShellWindowNavigate29 = 872;

			// Token: 0x04001199 RID: 4505
			internal const int cmdidShellWindowNavigate30 = 873;

			// Token: 0x0400119A RID: 4506
			internal const int cmdidShellWindowNavigate31 = 874;

			// Token: 0x0400119B RID: 4507
			internal const int cmdidShellWindowNavigate32 = 875;

			// Token: 0x0400119C RID: 4508
			internal const int cmdidShellWindowNavigate33 = 876;

			// Token: 0x0400119D RID: 4509
			internal const int cmdidOBSDoFind = 877;

			// Token: 0x0400119E RID: 4510
			internal const int cmdidOBSMatchCase = 878;

			// Token: 0x0400119F RID: 4511
			internal const int cmdidOBSMatchSubString = 879;

			// Token: 0x040011A0 RID: 4512
			internal const int cmdidOBSMatchWholeWord = 880;

			// Token: 0x040011A1 RID: 4513
			internal const int cmdidOBSMatchPrefix = 881;

			// Token: 0x040011A2 RID: 4514
			internal const int cmdidBuildSln = 882;

			// Token: 0x040011A3 RID: 4515
			internal const int cmdidRebuildSln = 883;

			// Token: 0x040011A4 RID: 4516
			internal const int cmdidDeploySln = 884;

			// Token: 0x040011A5 RID: 4517
			internal const int cmdidCleanSln = 885;

			// Token: 0x040011A6 RID: 4518
			internal const int cmdidBuildSel = 886;

			// Token: 0x040011A7 RID: 4519
			internal const int cmdidRebuildSel = 887;

			// Token: 0x040011A8 RID: 4520
			internal const int cmdidDeploySel = 888;

			// Token: 0x040011A9 RID: 4521
			internal const int cmdidCleanSel = 889;

			// Token: 0x040011AA RID: 4522
			internal const int cmdidCancelBuild = 890;

			// Token: 0x040011AB RID: 4523
			internal const int cmdidBatchBuildDlg = 891;

			// Token: 0x040011AC RID: 4524
			internal const int cmdidBuildCtx = 892;

			// Token: 0x040011AD RID: 4525
			internal const int cmdidRebuildCtx = 893;

			// Token: 0x040011AE RID: 4526
			internal const int cmdidDeployCtx = 894;

			// Token: 0x040011AF RID: 4527
			internal const int cmdidCleanCtx = 895;

			// Token: 0x040011B0 RID: 4528
			internal const int cmdidMRUFile1 = 900;

			// Token: 0x040011B1 RID: 4529
			internal const int cmdidMRUFile2 = 901;

			// Token: 0x040011B2 RID: 4530
			internal const int cmdidMRUFile3 = 902;

			// Token: 0x040011B3 RID: 4531
			internal const int cmdidMRUFile4 = 903;

			// Token: 0x040011B4 RID: 4532
			internal const int cmdidMRUFile5 = 904;

			// Token: 0x040011B5 RID: 4533
			internal const int cmdidMRUFile6 = 905;

			// Token: 0x040011B6 RID: 4534
			internal const int cmdidMRUFile7 = 906;

			// Token: 0x040011B7 RID: 4535
			internal const int cmdidMRUFile8 = 907;

			// Token: 0x040011B8 RID: 4536
			internal const int cmdidMRUFile9 = 908;

			// Token: 0x040011B9 RID: 4537
			internal const int cmdidMRUFile10 = 909;

			// Token: 0x040011BA RID: 4538
			internal const int cmdidMRUFile11 = 910;

			// Token: 0x040011BB RID: 4539
			internal const int cmdidMRUFile12 = 911;

			// Token: 0x040011BC RID: 4540
			internal const int cmdidMRUFile13 = 912;

			// Token: 0x040011BD RID: 4541
			internal const int cmdidMRUFile14 = 913;

			// Token: 0x040011BE RID: 4542
			internal const int cmdidMRUFile15 = 914;

			// Token: 0x040011BF RID: 4543
			internal const int cmdidMRUFile16 = 915;

			// Token: 0x040011C0 RID: 4544
			internal const int cmdidMRUFile17 = 916;

			// Token: 0x040011C1 RID: 4545
			internal const int cmdidMRUFile18 = 917;

			// Token: 0x040011C2 RID: 4546
			internal const int cmdidMRUFile19 = 918;

			// Token: 0x040011C3 RID: 4547
			internal const int cmdidMRUFile20 = 919;

			// Token: 0x040011C4 RID: 4548
			internal const int cmdidMRUFile21 = 920;

			// Token: 0x040011C5 RID: 4549
			internal const int cmdidMRUFile22 = 921;

			// Token: 0x040011C6 RID: 4550
			internal const int cmdidMRUFile23 = 922;

			// Token: 0x040011C7 RID: 4551
			internal const int cmdidMRUFile24 = 923;

			// Token: 0x040011C8 RID: 4552
			internal const int cmdidMRUFile25 = 924;

			// Token: 0x040011C9 RID: 4553
			internal const int cmdidGotoDefn = 925;

			// Token: 0x040011CA RID: 4554
			internal const int cmdidGotoDecl = 926;

			// Token: 0x040011CB RID: 4555
			internal const int cmdidBrowseDefn = 927;

			// Token: 0x040011CC RID: 4556
			internal const int cmdidShowMembers = 928;

			// Token: 0x040011CD RID: 4557
			internal const int cmdidShowBases = 929;

			// Token: 0x040011CE RID: 4558
			internal const int cmdidShowDerived = 930;

			// Token: 0x040011CF RID: 4559
			internal const int cmdidShowDefns = 931;

			// Token: 0x040011D0 RID: 4560
			internal const int cmdidShowRefs = 932;

			// Token: 0x040011D1 RID: 4561
			internal const int cmdidShowCallers = 933;

			// Token: 0x040011D2 RID: 4562
			internal const int cmdidShowCallees = 934;

			// Token: 0x040011D3 RID: 4563
			internal const int cmdidDefineSubset = 935;

			// Token: 0x040011D4 RID: 4564
			internal const int cmdidSetSubset = 936;

			// Token: 0x040011D5 RID: 4565
			internal const int cmdidCVGroupingNone = 950;

			// Token: 0x040011D6 RID: 4566
			internal const int cmdidCVGroupingSortOnly = 951;

			// Token: 0x040011D7 RID: 4567
			internal const int cmdidCVGroupingGrouped = 952;

			// Token: 0x040011D8 RID: 4568
			internal const int cmdidCVShowPackages = 953;

			// Token: 0x040011D9 RID: 4569
			internal const int cmdidQryManageIndexes = 954;

			// Token: 0x040011DA RID: 4570
			internal const int cmdidBrowseComponent = 955;

			// Token: 0x040011DB RID: 4571
			internal const int cmdidPrintDefault = 956;

			// Token: 0x040011DC RID: 4572
			internal const int cmdidBrowseDoc = 957;

			// Token: 0x040011DD RID: 4573
			internal const int cmdidStandardMax = 1000;

			// Token: 0x040011DE RID: 4574
			internal const int cmdidFormsFirst = 24576;

			// Token: 0x040011DF RID: 4575
			internal const int cmdidFormsLast = 28671;

			// Token: 0x040011E0 RID: 4576
			internal const int cmdidVBEFirst = 32768;

			// Token: 0x040011E1 RID: 4577
			internal const int msotcidBookmarkWellMenu = 32769;

			// Token: 0x040011E2 RID: 4578
			internal const int cmdidZoom200 = 32770;

			// Token: 0x040011E3 RID: 4579
			internal const int cmdidZoom150 = 32771;

			// Token: 0x040011E4 RID: 4580
			internal const int cmdidZoom100 = 32772;

			// Token: 0x040011E5 RID: 4581
			internal const int cmdidZoom75 = 32773;

			// Token: 0x040011E6 RID: 4582
			internal const int cmdidZoom50 = 32774;

			// Token: 0x040011E7 RID: 4583
			internal const int cmdidZoom25 = 32775;

			// Token: 0x040011E8 RID: 4584
			internal const int cmdidZoom10 = 32784;

			// Token: 0x040011E9 RID: 4585
			internal const int msotcidZoomWellMenu = 32785;

			// Token: 0x040011EA RID: 4586
			internal const int msotcidDebugPopWellMenu = 32786;

			// Token: 0x040011EB RID: 4587
			internal const int msotcidAlignWellMenu = 32787;

			// Token: 0x040011EC RID: 4588
			internal const int msotcidArrangeWellMenu = 32788;

			// Token: 0x040011ED RID: 4589
			internal const int msotcidCenterWellMenu = 32789;

			// Token: 0x040011EE RID: 4590
			internal const int msotcidSizeWellMenu = 32790;

			// Token: 0x040011EF RID: 4591
			internal const int msotcidHorizontalSpaceWellMenu = 32791;

			// Token: 0x040011F0 RID: 4592
			internal const int msotcidVerticalSpaceWellMenu = 32800;

			// Token: 0x040011F1 RID: 4593
			internal const int msotcidDebugWellMenu = 32801;

			// Token: 0x040011F2 RID: 4594
			internal const int msotcidDebugMenuVB = 32802;

			// Token: 0x040011F3 RID: 4595
			internal const int msotcidStatementBuilderWellMenu = 32803;

			// Token: 0x040011F4 RID: 4596
			internal const int msotcidProjWinInsertMenu = 32804;

			// Token: 0x040011F5 RID: 4597
			internal const int msotcidToggleMenu = 32805;

			// Token: 0x040011F6 RID: 4598
			internal const int msotcidNewObjInsertWellMenu = 32806;

			// Token: 0x040011F7 RID: 4599
			internal const int msotcidSizeToWellMenu = 32807;

			// Token: 0x040011F8 RID: 4600
			internal const int msotcidCommandBars = 32808;

			// Token: 0x040011F9 RID: 4601
			internal const int msotcidVBOrderMenu = 32809;

			// Token: 0x040011FA RID: 4602
			internal const int msotcidMSOnTheWeb = 32810;

			// Token: 0x040011FB RID: 4603
			internal const int msotcidVBDesignerMenu = 32816;

			// Token: 0x040011FC RID: 4604
			internal const int msotcidNewProjectWellMenu = 32817;

			// Token: 0x040011FD RID: 4605
			internal const int msotcidProjectWellMenu = 32818;

			// Token: 0x040011FE RID: 4606
			internal const int msotcidVBCode1ContextMenu = 32819;

			// Token: 0x040011FF RID: 4607
			internal const int msotcidVBCode2ContextMenu = 32820;

			// Token: 0x04001200 RID: 4608
			internal const int msotcidVBWatchContextMenu = 32821;

			// Token: 0x04001201 RID: 4609
			internal const int msotcidVBImmediateContextMenu = 32822;

			// Token: 0x04001202 RID: 4610
			internal const int msotcidVBLocalsContextMenu = 32823;

			// Token: 0x04001203 RID: 4611
			internal const int msotcidVBFormContextMenu = 32824;

			// Token: 0x04001204 RID: 4612
			internal const int msotcidVBControlContextMenu = 32825;

			// Token: 0x04001205 RID: 4613
			internal const int msotcidVBProjWinContextMenu = 32826;

			// Token: 0x04001206 RID: 4614
			internal const int msotcidVBProjWinContextBreakMenu = 32827;

			// Token: 0x04001207 RID: 4615
			internal const int msotcidVBPreviewWinContextMenu = 32828;

			// Token: 0x04001208 RID: 4616
			internal const int msotcidVBOBContextMenu = 32829;

			// Token: 0x04001209 RID: 4617
			internal const int msotcidVBForms3ContextMenu = 32830;

			// Token: 0x0400120A RID: 4618
			internal const int msotcidVBForms3ControlCMenu = 32831;

			// Token: 0x0400120B RID: 4619
			internal const int msotcidVBForms3ControlCMenuGroup = 32832;

			// Token: 0x0400120C RID: 4620
			internal const int msotcidVBForms3ControlPalette = 32833;

			// Token: 0x0400120D RID: 4621
			internal const int msotcidVBForms3ToolboxCMenu = 32834;

			// Token: 0x0400120E RID: 4622
			internal const int msotcidVBForms3MPCCMenu = 32835;

			// Token: 0x0400120F RID: 4623
			internal const int msotcidVBForms3DragDropCMenu = 32836;

			// Token: 0x04001210 RID: 4624
			internal const int msotcidVBToolBoxContextMenu = 32837;

			// Token: 0x04001211 RID: 4625
			internal const int msotcidVBToolBoxGroupContextMenu = 32838;

			// Token: 0x04001212 RID: 4626
			internal const int msotcidVBPropBrsHostContextMenu = 32839;

			// Token: 0x04001213 RID: 4627
			internal const int msotcidVBPropBrsContextMenu = 32840;

			// Token: 0x04001214 RID: 4628
			internal const int msotcidVBPalContextMenu = 32841;

			// Token: 0x04001215 RID: 4629
			internal const int msotcidVBProjWinProjectContextMenu = 32842;

			// Token: 0x04001216 RID: 4630
			internal const int msotcidVBProjWinFormContextMenu = 32843;

			// Token: 0x04001217 RID: 4631
			internal const int msotcidVBProjWinModClassContextMenu = 32844;

			// Token: 0x04001218 RID: 4632
			internal const int msotcidVBProjWinRelDocContextMenu = 32845;

			// Token: 0x04001219 RID: 4633
			internal const int msotcidVBDockedWindowContextMenu = 32846;

			// Token: 0x0400121A RID: 4634
			internal const int msotcidVBShortCutForms = 32847;

			// Token: 0x0400121B RID: 4635
			internal const int msotcidVBShortCutCodeWindows = 32848;

			// Token: 0x0400121C RID: 4636
			internal const int msotcidVBShortCutMisc = 32849;

			// Token: 0x0400121D RID: 4637
			internal const int msotcidVBBuiltInMenus = 32850;

			// Token: 0x0400121E RID: 4638
			internal const int msotcidPreviewWinFormPos = 32851;

			// Token: 0x0400121F RID: 4639
			internal const int msotcidVBAddinFirst = 33280;
		}

		// Token: 0x02000240 RID: 576
		private static class ShellGuids
		{
			// Token: 0x0600117E RID: 4478 RVA: 0x00040808 File Offset: 0x0003EA08
			// Note: this type is marked as 'beforefieldinit'.
			static ShellGuids()
			{
			}

			// Token: 0x04001220 RID: 4640
			internal static readonly Guid VSStandardCommandSet97 = new Guid("{5efc7975-14bc-11cf-9b2b-00aa00573819}");

			// Token: 0x04001221 RID: 4641
			internal static readonly Guid guidDsdCmdId = new Guid("{1F0FD094-8e53-11d2-8f9c-0060089fc486}");

			// Token: 0x04001222 RID: 4642
			internal static readonly Guid SID_SOleComponentUIManager = new Guid("{5efc7974-14bc-11cf-9b2b-00aa00573819}");

			// Token: 0x04001223 RID: 4643
			internal static readonly Guid GUID_VSTASKCATEGORY_DATADESIGNER = new Guid("{6B32EAED-13BB-11d3-A64F-00C04F683820}");

			// Token: 0x04001224 RID: 4644
			internal static readonly Guid GUID_PropertyBrowserToolWindow = new Guid(-285584864, -7528, 4560, new byte[]
			{
				143,
				120,
				0,
				160,
				201,
				17,
				0,
				87
			});
		}
	}
}
