﻿using System;

namespace System.ComponentModel.Design
{
	/// <summary>Defines identifiers that indicate the type of a Help keyword.</summary>
	// Token: 0x0200021E RID: 542
	public enum HelpKeywordType
	{
		/// <summary>A keyword that F1 was pressed to request help about.</summary>
		// Token: 0x04000E77 RID: 3703
		F1Keyword,
		/// <summary>A general keyword.</summary>
		// Token: 0x04000E78 RID: 3704
		GeneralKeyword,
		/// <summary>A filter keyword.</summary>
		// Token: 0x04000E79 RID: 3705
		FilterKeyword
	}
}
