﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	/// <summary>Provides an interface for a designer to select components.</summary>
	// Token: 0x02000232 RID: 562
	[ComVisible(true)]
	public interface ISelectionService
	{
		/// <summary>Gets the object that is currently the primary selected object.</summary>
		/// <returns>The object that is currently the primary selected object.</returns>
		// Token: 0x17000385 RID: 901
		// (get) Token: 0x06001134 RID: 4404
		object PrimarySelection { get; }

		/// <summary>Gets the count of selected objects.</summary>
		/// <returns>The number of selected objects.</returns>
		// Token: 0x17000386 RID: 902
		// (get) Token: 0x06001135 RID: 4405
		int SelectionCount { get; }

		/// <summary>Occurs when the current selection changes.</summary>
		// Token: 0x14000026 RID: 38
		// (add) Token: 0x06001136 RID: 4406
		// (remove) Token: 0x06001137 RID: 4407
		event EventHandler SelectionChanged;

		/// <summary>Occurs when the current selection is about to change.</summary>
		// Token: 0x14000027 RID: 39
		// (add) Token: 0x06001138 RID: 4408
		// (remove) Token: 0x06001139 RID: 4409
		event EventHandler SelectionChanging;

		/// <summary>Gets a value indicating whether the specified component is currently selected.</summary>
		/// <param name="component">The component to test. </param>
		/// <returns>
		///     <see langword="true" /> if the component is part of the user's current selection; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600113A RID: 4410
		bool GetComponentSelected(object component);

		/// <summary>Gets a collection of components that are currently selected.</summary>
		/// <returns>A collection that represents the current set of components that are selected.</returns>
		// Token: 0x0600113B RID: 4411
		ICollection GetSelectedComponents();

		/// <summary>Selects the specified collection of components.</summary>
		/// <param name="components">The collection of components to select. </param>
		// Token: 0x0600113C RID: 4412
		void SetSelectedComponents(ICollection components);

		/// <summary>Selects the components from within the specified collection of components that match the specified selection type.</summary>
		/// <param name="components">The collection of components to select. </param>
		/// <param name="selectionType">A value from the <see cref="T:System.ComponentModel.Design.SelectionTypes" /> enumeration. The default is <see cref="F:System.ComponentModel.Design.SelectionTypes.Normal" />. </param>
		// Token: 0x0600113D RID: 4413
		void SetSelectedComponents(ICollection components, SelectionTypes selectionType);
	}
}
