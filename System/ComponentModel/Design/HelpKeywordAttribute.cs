﻿using System;

namespace System.ComponentModel.Design
{
	/// <summary>Specifies the context keyword for a class or member. This class cannot be inherited.</summary>
	// Token: 0x0200021D RID: 541
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
	[Serializable]
	public sealed class HelpKeywordAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.HelpKeywordAttribute" /> class. </summary>
		// Token: 0x060010BE RID: 4286 RVA: 0x000020AE File Offset: 0x000002AE
		public HelpKeywordAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.HelpKeywordAttribute" /> class. </summary>
		/// <param name="keyword">The Help keyword value.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyword" /> is <see langword="null" />.</exception>
		// Token: 0x060010BF RID: 4287 RVA: 0x0003FDED File Offset: 0x0003DFED
		public HelpKeywordAttribute(string keyword)
		{
			if (keyword == null)
			{
				throw new ArgumentNullException("keyword");
			}
			this.contextKeyword = keyword;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.HelpKeywordAttribute" /> class from the given type. </summary>
		/// <param name="t">The type from which the Help keyword will be taken.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="t" /> is <see langword="null" />.</exception>
		// Token: 0x060010C0 RID: 4288 RVA: 0x0003FE0A File Offset: 0x0003E00A
		public HelpKeywordAttribute(Type t)
		{
			if (t == null)
			{
				throw new ArgumentNullException("t");
			}
			this.contextKeyword = t.FullName;
		}

		/// <summary>Gets the Help keyword supplied by this attribute.</summary>
		/// <returns>The Help keyword supplied by this attribute.</returns>
		// Token: 0x17000377 RID: 887
		// (get) Token: 0x060010C1 RID: 4289 RVA: 0x0003FE32 File Offset: 0x0003E032
		public string HelpKeyword
		{
			get
			{
				return this.contextKeyword;
			}
		}

		/// <summary>Determines whether two <see cref="T:System.ComponentModel.Design.HelpKeywordAttribute" /> instances are equal.</summary>
		/// <param name="obj">The <see cref="T:System.ComponentModel.Design.HelpKeywordAttribute" /> to compare with the current <see cref="T:System.ComponentModel.Design.HelpKeywordAttribute" />.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.ComponentModel.Design.HelpKeywordAttribute" /> is equal to the current <see cref="T:System.ComponentModel.Design.HelpKeywordAttribute" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060010C2 RID: 4290 RVA: 0x0003FE3A File Offset: 0x0003E03A
		public override bool Equals(object obj)
		{
			return obj == this || (obj != null && obj is HelpKeywordAttribute && ((HelpKeywordAttribute)obj).HelpKeyword == this.HelpKeyword);
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.Design.HelpKeywordAttribute" />.</returns>
		// Token: 0x060010C3 RID: 4291 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Determines whether the Help keyword is <see langword="null" />.</summary>
		/// <returns>
		///     <see langword="true" /> if the Help keyword is <see langword="null" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060010C4 RID: 4292 RVA: 0x0003FE65 File Offset: 0x0003E065
		public override bool IsDefaultAttribute()
		{
			return this.Equals(HelpKeywordAttribute.Default);
		}

		// Token: 0x060010C5 RID: 4293 RVA: 0x0003FE72 File Offset: 0x0003E072
		// Note: this type is marked as 'beforefieldinit'.
		static HelpKeywordAttribute()
		{
		}

		/// <summary>Represents the default value for <see cref="T:System.ComponentModel.Design.HelpKeywordAttribute" />. This field is read-only.</summary>
		// Token: 0x04000E74 RID: 3700
		public static readonly HelpKeywordAttribute Default = new HelpKeywordAttribute();

		// Token: 0x04000E75 RID: 3701
		private string contextKeyword;
	}
}
