﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	/// <summary>Provides data for the <see cref="E:System.ComponentModel.Design.IComponentChangeService.ComponentRename" /> event.</summary>
	// Token: 0x0200020A RID: 522
	[ComVisible(true)]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class ComponentRenameEventArgs : EventArgs
	{
		/// <summary>Gets the component that is being renamed.</summary>
		/// <returns>The component that is being renamed.</returns>
		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06001034 RID: 4148 RVA: 0x0003EF74 File Offset: 0x0003D174
		public object Component
		{
			get
			{
				return this.component;
			}
		}

		/// <summary>Gets the name of the component before the rename event.</summary>
		/// <returns>The previous name of the component.</returns>
		// Token: 0x17000353 RID: 851
		// (get) Token: 0x06001035 RID: 4149 RVA: 0x0003EF7C File Offset: 0x0003D17C
		public virtual string OldName
		{
			get
			{
				return this.oldName;
			}
		}

		/// <summary>Gets the name of the component after the rename event.</summary>
		/// <returns>The name of the component after the rename event.</returns>
		// Token: 0x17000354 RID: 852
		// (get) Token: 0x06001036 RID: 4150 RVA: 0x0003EF84 File Offset: 0x0003D184
		public virtual string NewName
		{
			get
			{
				return this.newName;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.ComponentRenameEventArgs" /> class.</summary>
		/// <param name="component">The component to be renamed. </param>
		/// <param name="oldName">The old name of the component. </param>
		/// <param name="newName">The new name of the component. </param>
		// Token: 0x06001037 RID: 4151 RVA: 0x0003EF8C File Offset: 0x0003D18C
		public ComponentRenameEventArgs(object component, string oldName, string newName)
		{
			this.oldName = oldName;
			this.newName = newName;
			this.component = component;
		}

		// Token: 0x04000E56 RID: 3670
		private object component;

		// Token: 0x04000E57 RID: 3671
		private string oldName;

		// Token: 0x04000E58 RID: 3672
		private string newName;
	}
}
