﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.ComponentModel.Design
{
	/// <summary>Provides data for the <see cref="E:System.ComponentModel.Design.IComponentChangeService.ComponentChanging" /> event. This class cannot be inherited.</summary>
	// Token: 0x02000206 RID: 518
	[ComVisible(true)]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public sealed class ComponentChangingEventArgs : EventArgs
	{
		/// <summary>Gets the component that is about to be changed or the component that is the parent container of the member that is about to be changed.</summary>
		/// <returns>The component that is about to have a member changed.</returns>
		// Token: 0x1700034F RID: 847
		// (get) Token: 0x06001027 RID: 4135 RVA: 0x0003EF37 File Offset: 0x0003D137
		public object Component
		{
			get
			{
				return this.component;
			}
		}

		/// <summary>Gets the member that is about to be changed.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.MemberDescriptor" /> indicating the member that is about to be changed, if known, or <see langword="null" /> otherwise.</returns>
		// Token: 0x17000350 RID: 848
		// (get) Token: 0x06001028 RID: 4136 RVA: 0x0003EF3F File Offset: 0x0003D13F
		public MemberDescriptor Member
		{
			get
			{
				return this.member;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Design.ComponentChangingEventArgs" /> class.</summary>
		/// <param name="component">The component that is about to be changed. </param>
		/// <param name="member">A <see cref="T:System.ComponentModel.MemberDescriptor" /> indicating the member of the component that is about to be changed. </param>
		// Token: 0x06001029 RID: 4137 RVA: 0x0003EF47 File Offset: 0x0003D147
		public ComponentChangingEventArgs(object component, MemberDescriptor member)
		{
			this.component = component;
			this.member = member;
		}

		// Token: 0x04000E53 RID: 3667
		private object component;

		// Token: 0x04000E54 RID: 3668
		private MemberDescriptor member;
	}
}
