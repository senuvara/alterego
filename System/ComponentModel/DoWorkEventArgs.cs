﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides data for the <see cref="E:System.ComponentModel.BackgroundWorker.DoWork" /> event handler.</summary>
	// Token: 0x02000160 RID: 352
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class DoWorkEventArgs : CancelEventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DoWorkEventArgs" /> class.</summary>
		/// <param name="argument">Specifies an argument for an asynchronous operation.</param>
		// Token: 0x06000AF4 RID: 2804 RVA: 0x0002F18E File Offset: 0x0002D38E
		public DoWorkEventArgs(object argument)
		{
			this.argument = argument;
		}

		/// <summary>Gets a value that represents the argument of an asynchronous operation.</summary>
		/// <returns>An <see cref="T:System.Object" /> representing the argument of an asynchronous operation.</returns>
		// Token: 0x17000251 RID: 593
		// (get) Token: 0x06000AF5 RID: 2805 RVA: 0x0002F19D File Offset: 0x0002D39D
		[SRDescription("Argument passed into the worker handler from BackgroundWorker.RunWorkerAsync.")]
		public object Argument
		{
			get
			{
				return this.argument;
			}
		}

		/// <summary>Gets or sets a value that represents the result of an asynchronous operation.</summary>
		/// <returns>An <see cref="T:System.Object" /> representing the result of an asynchronous operation.</returns>
		// Token: 0x17000252 RID: 594
		// (get) Token: 0x06000AF6 RID: 2806 RVA: 0x0002F1A5 File Offset: 0x0002D3A5
		// (set) Token: 0x06000AF7 RID: 2807 RVA: 0x0002F1AD File Offset: 0x0002D3AD
		[SRDescription("Result from the worker function.")]
		public object Result
		{
			get
			{
				return this.result;
			}
			set
			{
				this.result = value;
			}
		}

		// Token: 0x04000CC9 RID: 3273
		private object result;

		// Token: 0x04000CCA RID: 3274
		private object argument;
	}
}
