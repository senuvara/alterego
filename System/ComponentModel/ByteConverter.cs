﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a type converter to convert 8-bit unsigned integer objects to and from various other representations.</summary>
	// Token: 0x02000135 RID: 309
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class ByteConverter : BaseNumberConverter
	{
		// Token: 0x17000210 RID: 528
		// (get) Token: 0x060009DB RID: 2523 RVA: 0x0002B639 File Offset: 0x00029839
		internal override Type TargetType
		{
			get
			{
				return typeof(byte);
			}
		}

		// Token: 0x060009DC RID: 2524 RVA: 0x0002B645 File Offset: 0x00029845
		internal override object FromString(string value, int radix)
		{
			return Convert.ToByte(value, radix);
		}

		// Token: 0x060009DD RID: 2525 RVA: 0x0002B653 File Offset: 0x00029853
		internal override object FromString(string value, NumberFormatInfo formatInfo)
		{
			return byte.Parse(value, NumberStyles.Integer, formatInfo);
		}

		// Token: 0x060009DE RID: 2526 RVA: 0x0002B662 File Offset: 0x00029862
		internal override object FromString(string value, CultureInfo culture)
		{
			return byte.Parse(value, culture);
		}

		// Token: 0x060009DF RID: 2527 RVA: 0x0002B670 File Offset: 0x00029870
		internal override string ToString(object value, NumberFormatInfo formatInfo)
		{
			return ((byte)value).ToString("G", formatInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ByteConverter" /> class. </summary>
		// Token: 0x060009E0 RID: 2528 RVA: 0x0002B691 File Offset: 0x00029891
		public ByteConverter()
		{
		}
	}
}
