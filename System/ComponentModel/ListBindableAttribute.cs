﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies that a list can be used as a data source. A visual designer should use this attribute to determine whether to display a particular list in a data-binding picker. This class cannot be inherited.</summary>
	// Token: 0x020001A0 RID: 416
	[AttributeUsage(AttributeTargets.All)]
	public sealed class ListBindableAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ListBindableAttribute" /> class using a value to indicate whether the list is bindable.</summary>
		/// <param name="listBindable">
		///       <see langword="true" /> if the list is bindable; otherwise, <see langword="false" />. </param>
		// Token: 0x06000C4D RID: 3149 RVA: 0x0003114E File Offset: 0x0002F34E
		public ListBindableAttribute(bool listBindable)
		{
			this.listBindable = listBindable;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ListBindableAttribute" /> class using <see cref="T:System.ComponentModel.BindableSupport" /> to indicate whether the list is bindable.</summary>
		/// <param name="flags">A <see cref="T:System.ComponentModel.BindableSupport" /> that indicates whether the list is bindable. </param>
		// Token: 0x06000C4E RID: 3150 RVA: 0x0003115D File Offset: 0x0002F35D
		public ListBindableAttribute(BindableSupport flags)
		{
			this.listBindable = (flags > BindableSupport.No);
			this.isDefault = (flags == BindableSupport.Default);
		}

		/// <summary>Gets whether the list is bindable.</summary>
		/// <returns>
		///     <see langword="true" /> if the list is bindable; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000C4F RID: 3151 RVA: 0x00031179 File Offset: 0x0002F379
		public bool ListBindable
		{
			get
			{
				return this.listBindable;
			}
		}

		/// <summary>Returns whether the object passed is equal to this <see cref="T:System.ComponentModel.ListBindableAttribute" />.</summary>
		/// <param name="obj">The object to test equality with. </param>
		/// <returns>
		///     <see langword="true" /> if the object passed is equal to this <see cref="T:System.ComponentModel.ListBindableAttribute" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000C50 RID: 3152 RVA: 0x00031184 File Offset: 0x0002F384
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			ListBindableAttribute listBindableAttribute = obj as ListBindableAttribute;
			return listBindableAttribute != null && listBindableAttribute.ListBindable == this.listBindable;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.ListBindableAttribute" />.</returns>
		// Token: 0x06000C51 RID: 3153 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Returns whether <see cref="P:System.ComponentModel.ListBindableAttribute.ListBindable" /> is set to the default value.</summary>
		/// <returns>
		///     <see langword="true" /> if <see cref="P:System.ComponentModel.ListBindableAttribute.ListBindable" /> is set to the default value; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000C52 RID: 3154 RVA: 0x000311B1 File Offset: 0x0002F3B1
		public override bool IsDefaultAttribute()
		{
			return this.Equals(ListBindableAttribute.Default) || this.isDefault;
		}

		// Token: 0x06000C53 RID: 3155 RVA: 0x000311C8 File Offset: 0x0002F3C8
		// Note: this type is marked as 'beforefieldinit'.
		static ListBindableAttribute()
		{
		}

		/// <summary>Specifies that the list is bindable. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000D07 RID: 3335
		public static readonly ListBindableAttribute Yes = new ListBindableAttribute(true);

		/// <summary>Specifies that the list is not bindable. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000D08 RID: 3336
		public static readonly ListBindableAttribute No = new ListBindableAttribute(false);

		/// <summary>Represents the default value for <see cref="T:System.ComponentModel.ListBindableAttribute" />.</summary>
		// Token: 0x04000D09 RID: 3337
		public static readonly ListBindableAttribute Default = ListBindableAttribute.Yes;

		// Token: 0x04000D0A RID: 3338
		private bool listBindable;

		// Token: 0x04000D0B RID: 3339
		private bool isDefault;
	}
}
