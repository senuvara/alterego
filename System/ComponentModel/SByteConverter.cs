﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a type converter to convert 8-bit unsigned integer objects to and from a string.</summary>
	// Token: 0x020001CD RID: 461
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class SByteConverter : BaseNumberConverter
	{
		// Token: 0x1700031F RID: 799
		// (get) Token: 0x06000E5B RID: 3675 RVA: 0x00039318 File Offset: 0x00037518
		internal override Type TargetType
		{
			get
			{
				return typeof(sbyte);
			}
		}

		// Token: 0x06000E5C RID: 3676 RVA: 0x00039324 File Offset: 0x00037524
		internal override object FromString(string value, int radix)
		{
			return Convert.ToSByte(value, radix);
		}

		// Token: 0x06000E5D RID: 3677 RVA: 0x00039332 File Offset: 0x00037532
		internal override object FromString(string value, NumberFormatInfo formatInfo)
		{
			return sbyte.Parse(value, NumberStyles.Integer, formatInfo);
		}

		// Token: 0x06000E5E RID: 3678 RVA: 0x00039341 File Offset: 0x00037541
		internal override object FromString(string value, CultureInfo culture)
		{
			return sbyte.Parse(value, culture);
		}

		// Token: 0x06000E5F RID: 3679 RVA: 0x00039350 File Offset: 0x00037550
		internal override string ToString(object value, NumberFormatInfo formatInfo)
		{
			return ((sbyte)value).ToString("G", formatInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.SByteConverter" /> class. </summary>
		// Token: 0x06000E60 RID: 3680 RVA: 0x0002B691 File Offset: 0x00029891
		public SByteConverter()
		{
		}
	}
}
