﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies that this object supports a simple, transacted notification for batch initialization.</summary>
	// Token: 0x02000186 RID: 390
	[SRDescription("Specifies support for transacted initialization.")]
	public interface ISupportInitialize
	{
		/// <summary>Signals the object that initialization is starting.</summary>
		// Token: 0x06000BD2 RID: 3026
		void BeginInit();

		/// <summary>Signals the object that initialization is complete.</summary>
		// Token: 0x06000BD3 RID: 3027
		void EndInit();
	}
}
