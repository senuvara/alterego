﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Throws an exception for a Win32 error code.</summary>
	// Token: 0x020001F0 RID: 496
	[SuppressUnmanagedCodeSecurity]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[Serializable]
	public class Win32Exception : ExternalException, ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Win32Exception" /> class with the last Win32 error that occurred.</summary>
		// Token: 0x06000FB2 RID: 4018 RVA: 0x0003DB5B File Offset: 0x0003BD5B
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public Win32Exception() : this(Marshal.GetLastWin32Error())
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Win32Exception" /> class with the specified error.</summary>
		/// <param name="error">The Win32 error code associated with this exception. </param>
		// Token: 0x06000FB3 RID: 4019 RVA: 0x0003DB68 File Offset: 0x0003BD68
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public Win32Exception(int error) : this(error, Win32Exception.GetErrorMessage(error))
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Win32Exception" /> class with the specified error and the specified detailed description.</summary>
		/// <param name="error">The Win32 error code associated with this exception. </param>
		/// <param name="message">A detailed description of the error. </param>
		// Token: 0x06000FB4 RID: 4020 RVA: 0x0003DB77 File Offset: 0x0003BD77
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public Win32Exception(int error, string message) : base(message)
		{
			this.nativeErrorCode = error;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Win32Exception" /> class with the specified detailed description. </summary>
		/// <param name="message">A detailed description of the error.</param>
		// Token: 0x06000FB5 RID: 4021 RVA: 0x0003DB87 File Offset: 0x0003BD87
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public Win32Exception(string message) : this(Marshal.GetLastWin32Error(), message)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Win32Exception" /> class with the specified detailed description and the specified exception.</summary>
		/// <param name="message">A detailed description of the error.</param>
		/// <param name="innerException">A reference to the inner exception that is the cause of this exception.</param>
		// Token: 0x06000FB6 RID: 4022 RVA: 0x0003DB95 File Offset: 0x0003BD95
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public Win32Exception(string message, Exception innerException) : base(message, innerException)
		{
			this.nativeErrorCode = Marshal.GetLastWin32Error();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Win32Exception" /> class with the specified context and the serialization information.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> associated with this exception. </param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that represents the context of this exception. </param>
		// Token: 0x06000FB7 RID: 4023 RVA: 0x0003DBAA File Offset: 0x0003BDAA
		protected Win32Exception(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.nativeErrorCode = info.GetInt32("NativeErrorCode");
		}

		/// <summary>Gets the Win32 error code associated with this exception.</summary>
		/// <returns>The Win32 error code associated with this exception.</returns>
		// Token: 0x1700033B RID: 827
		// (get) Token: 0x06000FB8 RID: 4024 RVA: 0x0003DBC5 File Offset: 0x0003BDC5
		public int NativeErrorCode
		{
			get
			{
				return this.nativeErrorCode;
			}
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the file name and line number at which this <see cref="T:System.ComponentModel.Win32Exception" /> occurred.</summary>
		/// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo" />.</param>
		/// <param name="context">The contextual information about the source or destination.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info" /> is <see langword="null" />.</exception>
		// Token: 0x06000FB9 RID: 4025 RVA: 0x0003DBCD File Offset: 0x0003BDCD
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("NativeErrorCode", this.nativeErrorCode);
			base.GetObjectData(info, context);
		}

		// Token: 0x06000FBA RID: 4026 RVA: 0x0003DBF8 File Offset: 0x0003BDF8
		internal static string GetErrorMessage(int error)
		{
			if (!Win32Exception.s_ErrorMessagesInitialized)
			{
				Win32Exception.InitializeErrorMessages();
			}
			string result;
			if (Win32Exception.s_ErrorMessage.TryGetValue(error, out result))
			{
				return result;
			}
			return string.Format("mono-io-layer-error ({0})", error);
		}

		// Token: 0x06000FBB RID: 4027 RVA: 0x0003DC34 File Offset: 0x0003BE34
		private static void InitializeErrorMessages()
		{
			if (Win32Exception.s_ErrorMessagesInitialized)
			{
				return;
			}
			Dictionary<int, string> obj = Win32Exception.s_ErrorMessage;
			lock (obj)
			{
				if (!Win32Exception.s_ErrorMessagesInitialized)
				{
					Win32Exception.s_ErrorMessage.Add(0, "Success");
					Win32Exception.s_ErrorMessage.Add(2, "Cannot find the specified file");
					Win32Exception.s_ErrorMessage.Add(3, "Cannot find the specified file");
					Win32Exception.s_ErrorMessage.Add(4, "Too many open files");
					Win32Exception.s_ErrorMessage.Add(5, "Access denied");
					Win32Exception.s_ErrorMessage.Add(6, "Invalid handle");
					Win32Exception.s_ErrorMessage.Add(13, "Invalid data");
					Win32Exception.s_ErrorMessage.Add(14, "Out of memory");
					Win32Exception.s_ErrorMessage.Add(17, "Not same device");
					Win32Exception.s_ErrorMessage.Add(18, "No more files");
					Win32Exception.s_ErrorMessage.Add(24, "Bad length");
					Win32Exception.s_ErrorMessage.Add(31, "General failure");
					Win32Exception.s_ErrorMessage.Add(32, "Sharing violation");
					Win32Exception.s_ErrorMessage.Add(33, "Lock violation");
					Win32Exception.s_ErrorMessage.Add(50, "Operation not supported");
					Win32Exception.s_ErrorMessage.Add(55, "Device does not exist");
					Win32Exception.s_ErrorMessage.Add(87, "Invalid parameter");
					Win32Exception.s_ErrorMessage.Add(120, "Call not implemented");
					Win32Exception.s_ErrorMessage.Add(123, "Invalid name");
					Win32Exception.s_ErrorMessage.Add(127, "Process not found");
					Win32Exception.s_ErrorMessage.Add(183, "Already exists");
					Win32Exception.s_ErrorMessage.Add(267, "Is a directory");
					Win32Exception.s_ErrorMessage.Add(995, "Operation aborted");
					Win32Exception.s_ErrorMessage.Add(6000, "Encryption failed");
					Win32Exception.s_ErrorMessage.Add(10004, "interrupted");
					Win32Exception.s_ErrorMessage.Add(10009, "Bad file number");
					Win32Exception.s_ErrorMessage.Add(10013, "Access denied");
					Win32Exception.s_ErrorMessage.Add(10014, "Bad address");
					Win32Exception.s_ErrorMessage.Add(10022, "Invalid arguments");
					Win32Exception.s_ErrorMessage.Add(10024, "Too many open files");
					Win32Exception.s_ErrorMessage.Add(10035, "Operation on non-blocking socket would block");
					Win32Exception.s_ErrorMessage.Add(10036, "Operation in progress");
					Win32Exception.s_ErrorMessage.Add(10037, "Operation already in progress");
					Win32Exception.s_ErrorMessage.Add(10038, "The descriptor is not a socket");
					Win32Exception.s_ErrorMessage.Add(10039, "Destination address required");
					Win32Exception.s_ErrorMessage.Add(10040, "Message too long");
					Win32Exception.s_ErrorMessage.Add(10041, "Protocol wrong type for socket");
					Win32Exception.s_ErrorMessage.Add(10042, "Protocol option not supported");
					Win32Exception.s_ErrorMessage.Add(10043, "Protocol not supported");
					Win32Exception.s_ErrorMessage.Add(10044, "Socket not supported");
					Win32Exception.s_ErrorMessage.Add(10045, "Operation not supported");
					Win32Exception.s_ErrorMessage.Add(10046, "Protocol family not supported");
					Win32Exception.s_ErrorMessage.Add(10047, "An address incompatible with the requested protocol was used");
					Win32Exception.s_ErrorMessage.Add(10048, "Address already in use");
					Win32Exception.s_ErrorMessage.Add(10049, "The requested address is not valid in this context");
					Win32Exception.s_ErrorMessage.Add(10050, "Network subsystem is down");
					Win32Exception.s_ErrorMessage.Add(10051, "Network is unreachable");
					Win32Exception.s_ErrorMessage.Add(10052, "Connection broken, keep-alive detected a problem");
					Win32Exception.s_ErrorMessage.Add(10053, "An established connection was aborted in your host machine.");
					Win32Exception.s_ErrorMessage.Add(10054, "Connection reset by peer");
					Win32Exception.s_ErrorMessage.Add(10055, "Not enough buffer space is available");
					Win32Exception.s_ErrorMessage.Add(10056, "Socket is already connected");
					Win32Exception.s_ErrorMessage.Add(10057, "The socket is not connected");
					Win32Exception.s_ErrorMessage.Add(10058, "The socket has been shut down");
					Win32Exception.s_ErrorMessage.Add(10059, "Too many references: cannot splice");
					Win32Exception.s_ErrorMessage.Add(10060, "Connection timed out");
					Win32Exception.s_ErrorMessage.Add(10061, "Connection refused");
					Win32Exception.s_ErrorMessage.Add(10062, "Too many symbolic links encountered");
					Win32Exception.s_ErrorMessage.Add(10063, "File name too long");
					Win32Exception.s_ErrorMessage.Add(10064, "Host is down");
					Win32Exception.s_ErrorMessage.Add(10065, "No route to host");
					Win32Exception.s_ErrorMessage.Add(10066, "Directory not empty");
					Win32Exception.s_ErrorMessage.Add(10067, "EPROCLIM");
					Win32Exception.s_ErrorMessage.Add(10068, "Too many users");
					Win32Exception.s_ErrorMessage.Add(10069, "Quota exceeded");
					Win32Exception.s_ErrorMessage.Add(10070, "Stale NFS file handle");
					Win32Exception.s_ErrorMessage.Add(10071, "Object is remote");
					Win32Exception.s_ErrorMessage.Add(10091, "SYSNOTREADY");
					Win32Exception.s_ErrorMessage.Add(10092, "VERNOTSUPPORTED");
					Win32Exception.s_ErrorMessage.Add(10093, "Winsock not initialised");
					Win32Exception.s_ErrorMessage.Add(10101, "EDISCON");
					Win32Exception.s_ErrorMessage.Add(10102, "ENOMORE");
					Win32Exception.s_ErrorMessage.Add(10103, "Operation canceled");
					Win32Exception.s_ErrorMessage.Add(10104, "EINVALIDPROCTABLE");
					Win32Exception.s_ErrorMessage.Add(10105, "EINVALIDPROVIDER");
					Win32Exception.s_ErrorMessage.Add(10106, "EPROVIDERFAILEDINIT");
					Win32Exception.s_ErrorMessage.Add(10107, "System call failed");
					Win32Exception.s_ErrorMessage.Add(10108, "SERVICE_NOT_FOUND");
					Win32Exception.s_ErrorMessage.Add(10109, "TYPE_NOT_FOUND");
					Win32Exception.s_ErrorMessage.Add(10112, "EREFUSED");
					Win32Exception.s_ErrorMessage.Add(11001, "No such host is known");
					Win32Exception.s_ErrorMessage.Add(11002, "A temporary error occurred on an authoritative name server.  Try again later.");
					Win32Exception.s_ErrorMessage.Add(11003, "No recovery");
					Win32Exception.s_ErrorMessage.Add(11004, "No data");
					Win32Exception.s_ErrorMessagesInitialized = true;
				}
			}
		}

		// Token: 0x06000FBC RID: 4028 RVA: 0x0003E2E4 File Offset: 0x0003C4E4
		// Note: this type is marked as 'beforefieldinit'.
		static Win32Exception()
		{
		}

		// Token: 0x04000E20 RID: 3616
		private readonly int nativeErrorCode;

		// Token: 0x04000E21 RID: 3617
		private static bool s_ErrorMessagesInitialized = false;

		// Token: 0x04000E22 RID: 3618
		private static Dictionary<int, string> s_ErrorMessage = new Dictionary<int, string>();
	}
}
