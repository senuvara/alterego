﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides the base class for a custom component editor.</summary>
	// Token: 0x0200012E RID: 302
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public abstract class ComponentEditor
	{
		/// <summary>Edits the component and returns a value indicating whether the component was modified.</summary>
		/// <param name="component">The component to be edited. </param>
		/// <returns>
		///     <see langword="true" /> if the component was modified; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000987 RID: 2439 RVA: 0x0002AD6C File Offset: 0x00028F6C
		public bool EditComponent(object component)
		{
			return this.EditComponent(null, component);
		}

		/// <summary>Edits the component and returns a value indicating whether the component was modified based upon a given context.</summary>
		/// <param name="context">An optional context object that can be used to obtain further information about the edit. </param>
		/// <param name="component">The component to be edited. </param>
		/// <returns>
		///     <see langword="true" /> if the component was modified; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000988 RID: 2440
		public abstract bool EditComponent(ITypeDescriptorContext context, object component);

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ComponentEditor" /> class. </summary>
		// Token: 0x06000989 RID: 2441 RVA: 0x0000232F File Offset: 0x0000052F
		protected ComponentEditor()
		{
		}
	}
}
