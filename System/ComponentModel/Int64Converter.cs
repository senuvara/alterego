﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a type converter to convert 64-bit signed integer objects to and from various other representations.</summary>
	// Token: 0x02000191 RID: 401
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class Int64Converter : BaseNumberConverter
	{
		// Token: 0x17000298 RID: 664
		// (get) Token: 0x06000BFE RID: 3070 RVA: 0x00030659 File Offset: 0x0002E859
		internal override Type TargetType
		{
			get
			{
				return typeof(long);
			}
		}

		// Token: 0x06000BFF RID: 3071 RVA: 0x00030665 File Offset: 0x0002E865
		internal override object FromString(string value, int radix)
		{
			return Convert.ToInt64(value, radix);
		}

		// Token: 0x06000C00 RID: 3072 RVA: 0x00030673 File Offset: 0x0002E873
		internal override object FromString(string value, NumberFormatInfo formatInfo)
		{
			return long.Parse(value, NumberStyles.Integer, formatInfo);
		}

		// Token: 0x06000C01 RID: 3073 RVA: 0x00030682 File Offset: 0x0002E882
		internal override object FromString(string value, CultureInfo culture)
		{
			return long.Parse(value, culture);
		}

		// Token: 0x06000C02 RID: 3074 RVA: 0x00030690 File Offset: 0x0002E890
		internal override string ToString(object value, NumberFormatInfo formatInfo)
		{
			return ((long)value).ToString("G", formatInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Int64Converter" /> class. </summary>
		// Token: 0x06000C03 RID: 3075 RVA: 0x0002B691 File Offset: 0x00029891
		public Int64Converter()
		{
		}
	}
}
