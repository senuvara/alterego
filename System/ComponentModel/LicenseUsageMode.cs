﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies when the <see cref="T:System.ComponentModel.License" /> can be used.</summary>
	// Token: 0x0200019F RID: 415
	public enum LicenseUsageMode
	{
		/// <summary>Used during runtime.</summary>
		// Token: 0x04000D05 RID: 3333
		Runtime,
		/// <summary>Used during design time by a visual designer or the compiler.</summary>
		// Token: 0x04000D06 RID: 3334
		Designtime
	}
}
