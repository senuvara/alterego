﻿using System;

namespace System.ComponentModel
{
	/// <summary>Indicates that an object's text representation is obscured by characters such as asterisks. This class cannot be inherited.</summary>
	// Token: 0x020001B5 RID: 437
	[AttributeUsage(AttributeTargets.All)]
	public sealed class PasswordPropertyTextAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.PasswordPropertyTextAttribute" /> class. </summary>
		// Token: 0x06000D4A RID: 3402 RVA: 0x00034236 File Offset: 0x00032436
		public PasswordPropertyTextAttribute() : this(false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.PasswordPropertyTextAttribute" /> class, optionally showing password text. </summary>
		/// <param name="password">
		///       <see langword="true" /> to indicate that the property should be shown as password text; otherwise, <see langword="false" />. The default is <see langword="false" />.</param>
		// Token: 0x06000D4B RID: 3403 RVA: 0x0003423F File Offset: 0x0003243F
		public PasswordPropertyTextAttribute(bool password)
		{
			this._password = password;
		}

		/// <summary>Gets a value indicating if the property for which the <see cref="T:System.ComponentModel.PasswordPropertyTextAttribute" /> is defined should be shown as password text.</summary>
		/// <returns>
		///     <see langword="true" /> if the property should be shown as password text; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x06000D4C RID: 3404 RVA: 0x0003424E File Offset: 0x0003244E
		public bool Password
		{
			get
			{
				return this._password;
			}
		}

		/// <summary>Determines whether two <see cref="T:System.ComponentModel.PasswordPropertyTextAttribute" /> instances are equal.</summary>
		/// <param name="o">The <see cref="T:System.ComponentModel.PasswordPropertyTextAttribute" /> to compare with the current <see cref="T:System.ComponentModel.PasswordPropertyTextAttribute" />.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.ComponentModel.PasswordPropertyTextAttribute" /> is equal to the current <see cref="T:System.ComponentModel.PasswordPropertyTextAttribute" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000D4D RID: 3405 RVA: 0x00034256 File Offset: 0x00032456
		public override bool Equals(object o)
		{
			return o is PasswordPropertyTextAttribute && ((PasswordPropertyTextAttribute)o).Password == this._password;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.PasswordPropertyTextAttribute" />.</returns>
		// Token: 0x06000D4E RID: 3406 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Returns an indication whether the value of this instance is the default value.</summary>
		/// <returns>
		///     <see langword="true" /> if this instance is the default attribute for the class; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000D4F RID: 3407 RVA: 0x00034275 File Offset: 0x00032475
		public override bool IsDefaultAttribute()
		{
			return this.Equals(PasswordPropertyTextAttribute.Default);
		}

		// Token: 0x06000D50 RID: 3408 RVA: 0x00034282 File Offset: 0x00032482
		// Note: this type is marked as 'beforefieldinit'.
		static PasswordPropertyTextAttribute()
		{
		}

		/// <summary>Specifies that a text property is used as a password. This <see langword="static" /> (<see langword="Shared" /> in Visual Basic) field is read-only.</summary>
		// Token: 0x04000D7D RID: 3453
		public static readonly PasswordPropertyTextAttribute Yes = new PasswordPropertyTextAttribute(true);

		/// <summary>Specifies that a text property is not used as a password. This <see langword="static" /> (<see langword="Shared" /> in Visual Basic) field is read-only.</summary>
		// Token: 0x04000D7E RID: 3454
		public static readonly PasswordPropertyTextAttribute No = new PasswordPropertyTextAttribute(false);

		/// <summary>Specifies the default value for the <see cref="T:System.ComponentModel.PasswordPropertyTextAttribute" />.</summary>
		// Token: 0x04000D7F RID: 3455
		public static readonly PasswordPropertyTextAttribute Default = PasswordPropertyTextAttribute.No;

		// Token: 0x04000D80 RID: 3456
		private bool _password;
	}
}
