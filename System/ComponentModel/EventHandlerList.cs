﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a simple list of delegates. This class cannot be inherited.</summary>
	// Token: 0x02000169 RID: 361
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public sealed class EventHandlerList : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.EventHandlerList" /> class. </summary>
		// Token: 0x06000B4E RID: 2894 RVA: 0x0000232F File Offset: 0x0000052F
		public EventHandlerList()
		{
		}

		// Token: 0x06000B4F RID: 2895 RVA: 0x0002FFE3 File Offset: 0x0002E1E3
		internal EventHandlerList(Component parent)
		{
			this.parent = parent;
		}

		/// <summary>Gets or sets the delegate for the specified object.</summary>
		/// <param name="key">An object to find in the list. </param>
		/// <returns>The delegate for the specified key, or <see langword="null" /> if a delegate does not exist.</returns>
		// Token: 0x17000268 RID: 616
		public Delegate this[object key]
		{
			get
			{
				EventHandlerList.ListEntry listEntry = null;
				if (this.parent == null || this.parent.CanRaiseEventsInternal)
				{
					listEntry = this.Find(key);
				}
				if (listEntry != null)
				{
					return listEntry.handler;
				}
				return null;
			}
			set
			{
				EventHandlerList.ListEntry listEntry = this.Find(key);
				if (listEntry != null)
				{
					listEntry.handler = value;
					return;
				}
				this.head = new EventHandlerList.ListEntry(key, value, this.head);
			}
		}

		/// <summary>Adds a delegate to the list.</summary>
		/// <param name="key">The object that owns the event. </param>
		/// <param name="value">The delegate to add to the list. </param>
		// Token: 0x06000B52 RID: 2898 RVA: 0x00030060 File Offset: 0x0002E260
		public void AddHandler(object key, Delegate value)
		{
			EventHandlerList.ListEntry listEntry = this.Find(key);
			if (listEntry != null)
			{
				listEntry.handler = Delegate.Combine(listEntry.handler, value);
				return;
			}
			this.head = new EventHandlerList.ListEntry(key, value, this.head);
		}

		/// <summary>Adds a list of delegates to the current list.</summary>
		/// <param name="listToAddFrom">The list to add.</param>
		// Token: 0x06000B53 RID: 2899 RVA: 0x000300A0 File Offset: 0x0002E2A0
		public void AddHandlers(EventHandlerList listToAddFrom)
		{
			for (EventHandlerList.ListEntry next = listToAddFrom.head; next != null; next = next.next)
			{
				this.AddHandler(next.key, next.handler);
			}
		}

		/// <summary>Disposes the delegate list.</summary>
		// Token: 0x06000B54 RID: 2900 RVA: 0x000300D2 File Offset: 0x0002E2D2
		public void Dispose()
		{
			this.head = null;
		}

		// Token: 0x06000B55 RID: 2901 RVA: 0x000300DC File Offset: 0x0002E2DC
		private EventHandlerList.ListEntry Find(object key)
		{
			EventHandlerList.ListEntry next = this.head;
			while (next != null && next.key != key)
			{
				next = next.next;
			}
			return next;
		}

		/// <summary>Removes a delegate from the list.</summary>
		/// <param name="key">The object that owns the event. </param>
		/// <param name="value">The delegate to remove from the list. </param>
		// Token: 0x06000B56 RID: 2902 RVA: 0x00030108 File Offset: 0x0002E308
		public void RemoveHandler(object key, Delegate value)
		{
			EventHandlerList.ListEntry listEntry = this.Find(key);
			if (listEntry != null)
			{
				listEntry.handler = Delegate.Remove(listEntry.handler, value);
			}
		}

		// Token: 0x04000CDD RID: 3293
		private EventHandlerList.ListEntry head;

		// Token: 0x04000CDE RID: 3294
		private Component parent;

		// Token: 0x0200016A RID: 362
		private sealed class ListEntry
		{
			// Token: 0x06000B57 RID: 2903 RVA: 0x00030132 File Offset: 0x0002E332
			public ListEntry(object key, Delegate handler, EventHandlerList.ListEntry next)
			{
				this.next = next;
				this.key = key;
				this.handler = handler;
			}

			// Token: 0x04000CDF RID: 3295
			internal EventHandlerList.ListEntry next;

			// Token: 0x04000CE0 RID: 3296
			internal object key;

			// Token: 0x04000CE1 RID: 3297
			internal Delegate handler;
		}
	}
}
