﻿using System;
using System.Collections;
using System.Security.Permissions;

namespace System.ComponentModel
{
	// Token: 0x0200016C RID: 364
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	internal sealed class ExtendedPropertyDescriptor : PropertyDescriptor
	{
		// Token: 0x06000B5B RID: 2907 RVA: 0x00030150 File Offset: 0x0002E350
		public ExtendedPropertyDescriptor(ReflectPropertyDescriptor extenderInfo, Type receiverType, IExtenderProvider provider, Attribute[] attributes) : base(extenderInfo, attributes)
		{
			ArrayList arrayList = new ArrayList(this.AttributeArray);
			arrayList.Add(ExtenderProvidedPropertyAttribute.Create(extenderInfo, receiverType, provider));
			if (extenderInfo.IsReadOnly)
			{
				arrayList.Add(ReadOnlyAttribute.Yes);
			}
			Attribute[] array = new Attribute[arrayList.Count];
			arrayList.CopyTo(array, 0);
			this.AttributeArray = array;
			this.extenderInfo = extenderInfo;
			this.provider = provider;
		}

		// Token: 0x06000B5C RID: 2908 RVA: 0x000301C0 File Offset: 0x0002E3C0
		public ExtendedPropertyDescriptor(PropertyDescriptor extender, Attribute[] attributes) : base(extender, attributes)
		{
			ExtenderProvidedPropertyAttribute extenderProvidedPropertyAttribute = extender.Attributes[typeof(ExtenderProvidedPropertyAttribute)] as ExtenderProvidedPropertyAttribute;
			ReflectPropertyDescriptor reflectPropertyDescriptor = extenderProvidedPropertyAttribute.ExtenderProperty as ReflectPropertyDescriptor;
			this.extenderInfo = reflectPropertyDescriptor;
			this.provider = extenderProvidedPropertyAttribute.Provider;
		}

		// Token: 0x06000B5D RID: 2909 RVA: 0x0003020F File Offset: 0x0002E40F
		public override bool CanResetValue(object comp)
		{
			return this.extenderInfo.ExtenderCanResetValue(this.provider, comp);
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x06000B5E RID: 2910 RVA: 0x00030223 File Offset: 0x0002E423
		public override Type ComponentType
		{
			get
			{
				return this.extenderInfo.ComponentType;
			}
		}

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x06000B5F RID: 2911 RVA: 0x00030230 File Offset: 0x0002E430
		public override bool IsReadOnly
		{
			get
			{
				return this.Attributes[typeof(ReadOnlyAttribute)].Equals(ReadOnlyAttribute.Yes);
			}
		}

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x06000B60 RID: 2912 RVA: 0x00030251 File Offset: 0x0002E451
		public override Type PropertyType
		{
			get
			{
				return this.extenderInfo.ExtenderGetType(this.provider);
			}
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x06000B61 RID: 2913 RVA: 0x00030264 File Offset: 0x0002E464
		public override string DisplayName
		{
			get
			{
				string text = base.DisplayName;
				DisplayNameAttribute displayNameAttribute = this.Attributes[typeof(DisplayNameAttribute)] as DisplayNameAttribute;
				if (displayNameAttribute == null || displayNameAttribute.IsDefaultAttribute())
				{
					ISite site = MemberDescriptor.GetSite(this.provider);
					if (site != null)
					{
						string name = site.Name;
						if (name != null && name.Length > 0)
						{
							text = SR.GetString("{0} on {1}", new object[]
							{
								text,
								name
							});
						}
					}
				}
				return text;
			}
		}

		// Token: 0x06000B62 RID: 2914 RVA: 0x000302DA File Offset: 0x0002E4DA
		public override object GetValue(object comp)
		{
			return this.extenderInfo.ExtenderGetValue(this.provider, comp);
		}

		// Token: 0x06000B63 RID: 2915 RVA: 0x000302EE File Offset: 0x0002E4EE
		public override void ResetValue(object comp)
		{
			this.extenderInfo.ExtenderResetValue(this.provider, comp, this);
		}

		// Token: 0x06000B64 RID: 2916 RVA: 0x00030303 File Offset: 0x0002E503
		public override void SetValue(object component, object value)
		{
			this.extenderInfo.ExtenderSetValue(this.provider, component, value, this);
		}

		// Token: 0x06000B65 RID: 2917 RVA: 0x00030319 File Offset: 0x0002E519
		public override bool ShouldSerializeValue(object comp)
		{
			return this.extenderInfo.ExtenderShouldSerializeValue(this.provider, comp);
		}

		// Token: 0x04000CE2 RID: 3298
		private readonly ReflectPropertyDescriptor extenderInfo;

		// Token: 0x04000CE3 RID: 3299
		private readonly IExtenderProvider provider;
	}
}
