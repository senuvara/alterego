﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies values to indicate whether a property can be bound to a data element or another property.</summary>
	// Token: 0x02000130 RID: 304
	public enum BindableSupport
	{
		/// <summary>The property is not bindable at design time.</summary>
		// Token: 0x04000C4D RID: 3149
		No,
		/// <summary>The property is bindable at design time.</summary>
		// Token: 0x04000C4E RID: 3150
		Yes,
		/// <summary>The property is set to the default.</summary>
		// Token: 0x04000C4F RID: 3151
		Default
	}
}
