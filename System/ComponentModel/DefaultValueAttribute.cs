﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies the default value for a property.</summary>
	// Token: 0x02000156 RID: 342
	[AttributeUsage(AttributeTargets.All)]
	public class DefaultValueAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class, converting the specified value to the specified type, and using an invariant culture as the translation context.</summary>
		/// <param name="type">A <see cref="T:System.Type" /> that represents the type to convert the value to. </param>
		/// <param name="value">A <see cref="T:System.String" /> that can be converted to the type using the <see cref="T:System.ComponentModel.TypeConverter" /> for the type and the U.S. English culture. </param>
		// Token: 0x06000AA3 RID: 2723 RVA: 0x0002EA94 File Offset: 0x0002CC94
		public DefaultValueAttribute(Type type, string value)
		{
			try
			{
				this.value = TypeDescriptor.GetConverter(type).ConvertFromInvariantString(value);
			}
			catch
			{
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class using a Unicode character.</summary>
		/// <param name="value">A Unicode character that is the default value. </param>
		// Token: 0x06000AA4 RID: 2724 RVA: 0x0002EAD0 File Offset: 0x0002CCD0
		public DefaultValueAttribute(char value)
		{
			this.value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class using an 8-bit unsigned integer.</summary>
		/// <param name="value">An 8-bit unsigned integer that is the default value. </param>
		// Token: 0x06000AA5 RID: 2725 RVA: 0x0002EAE4 File Offset: 0x0002CCE4
		public DefaultValueAttribute(byte value)
		{
			this.value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class using a 16-bit signed integer.</summary>
		/// <param name="value">A 16-bit signed integer that is the default value. </param>
		// Token: 0x06000AA6 RID: 2726 RVA: 0x0002EAF8 File Offset: 0x0002CCF8
		public DefaultValueAttribute(short value)
		{
			this.value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class using a 32-bit signed integer.</summary>
		/// <param name="value">A 32-bit signed integer that is the default value. </param>
		// Token: 0x06000AA7 RID: 2727 RVA: 0x0002EB0C File Offset: 0x0002CD0C
		public DefaultValueAttribute(int value)
		{
			this.value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class using a 64-bit signed integer.</summary>
		/// <param name="value">A 64-bit signed integer that is the default value. </param>
		// Token: 0x06000AA8 RID: 2728 RVA: 0x0002EB20 File Offset: 0x0002CD20
		public DefaultValueAttribute(long value)
		{
			this.value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class using a single-precision floating point number.</summary>
		/// <param name="value">A single-precision floating point number that is the default value. </param>
		// Token: 0x06000AA9 RID: 2729 RVA: 0x0002EB34 File Offset: 0x0002CD34
		public DefaultValueAttribute(float value)
		{
			this.value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class using a double-precision floating point number.</summary>
		/// <param name="value">A double-precision floating point number that is the default value. </param>
		// Token: 0x06000AAA RID: 2730 RVA: 0x0002EB48 File Offset: 0x0002CD48
		public DefaultValueAttribute(double value)
		{
			this.value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class using a <see cref="T:System.Boolean" /> value.</summary>
		/// <param name="value">A <see cref="T:System.Boolean" /> that is the default value. </param>
		// Token: 0x06000AAB RID: 2731 RVA: 0x0002EB5C File Offset: 0x0002CD5C
		public DefaultValueAttribute(bool value)
		{
			this.value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class using a <see cref="T:System.String" />.</summary>
		/// <param name="value">A <see cref="T:System.String" /> that is the default value. </param>
		// Token: 0x06000AAC RID: 2732 RVA: 0x0002EB70 File Offset: 0x0002CD70
		public DefaultValueAttribute(string value)
		{
			this.value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultValueAttribute" /> class.</summary>
		/// <param name="value">An <see cref="T:System.Object" /> that represents the default value. </param>
		// Token: 0x06000AAD RID: 2733 RVA: 0x0002EB70 File Offset: 0x0002CD70
		public DefaultValueAttribute(object value)
		{
			this.value = value;
		}

		/// <summary>Gets the default value of the property this attribute is bound to.</summary>
		/// <returns>An <see cref="T:System.Object" /> that represents the default value of the property this attribute is bound to.</returns>
		// Token: 0x17000243 RID: 579
		// (get) Token: 0x06000AAE RID: 2734 RVA: 0x0002EB7F File Offset: 0x0002CD7F
		public virtual object Value
		{
			get
			{
				return this.value;
			}
		}

		/// <summary>Returns whether the value of the given object is equal to the current <see cref="T:System.ComponentModel.DefaultValueAttribute" />.</summary>
		/// <param name="obj">The object to test the value equality of. </param>
		/// <returns>
		///     <see langword="true" /> if the value of the given object is equal to that of the current; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AAF RID: 2735 RVA: 0x0002EB88 File Offset: 0x0002CD88
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			DefaultValueAttribute defaultValueAttribute = obj as DefaultValueAttribute;
			if (defaultValueAttribute == null)
			{
				return false;
			}
			if (this.Value != null)
			{
				return this.Value.Equals(defaultValueAttribute.Value);
			}
			return defaultValueAttribute.Value == null;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000AB0 RID: 2736 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Sets the default value for the property to which this attribute is bound.</summary>
		/// <param name="value">The default value.</param>
		// Token: 0x06000AB1 RID: 2737 RVA: 0x0002EBCA File Offset: 0x0002CDCA
		protected void SetValue(object value)
		{
			this.value = value;
		}

		// Token: 0x04000CA9 RID: 3241
		private object value;
	}
}
