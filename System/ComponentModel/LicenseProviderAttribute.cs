﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies the <see cref="T:System.ComponentModel.LicenseProvider" /> to use with a class. This class cannot be inherited.</summary>
	// Token: 0x0200019E RID: 414
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public sealed class LicenseProviderAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.LicenseProviderAttribute" /> class without a license provider.</summary>
		// Token: 0x06000C45 RID: 3141 RVA: 0x00031058 File Offset: 0x0002F258
		public LicenseProviderAttribute() : this(null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.LicenseProviderAttribute" /> class with the specified type.</summary>
		/// <param name="typeName">The fully qualified name of the license provider class. </param>
		// Token: 0x06000C46 RID: 3142 RVA: 0x00031061 File Offset: 0x0002F261
		public LicenseProviderAttribute(string typeName)
		{
			this.licenseProviderName = typeName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.LicenseProviderAttribute" /> class with the specified type of license provider.</summary>
		/// <param name="type">A <see cref="T:System.Type" /> that represents the type of the license provider class. </param>
		// Token: 0x06000C47 RID: 3143 RVA: 0x00031070 File Offset: 0x0002F270
		public LicenseProviderAttribute(Type type)
		{
			this.licenseProviderType = type;
		}

		/// <summary>Gets the license provider that must be used with the associated class.</summary>
		/// <returns>A <see cref="T:System.Type" /> that represents the type of the license provider. The default value is <see langword="null" />.</returns>
		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x06000C48 RID: 3144 RVA: 0x0003107F File Offset: 0x0002F27F
		public Type LicenseProvider
		{
			get
			{
				if (this.licenseProviderType == null && this.licenseProviderName != null)
				{
					this.licenseProviderType = Type.GetType(this.licenseProviderName);
				}
				return this.licenseProviderType;
			}
		}

		/// <summary>Indicates a unique ID for this attribute type.</summary>
		/// <returns>A unique ID for this attribute type.</returns>
		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000C49 RID: 3145 RVA: 0x000310B0 File Offset: 0x0002F2B0
		public override object TypeId
		{
			get
			{
				string fullName = this.licenseProviderName;
				if (fullName == null && this.licenseProviderType != null)
				{
					fullName = this.licenseProviderType.FullName;
				}
				return base.GetType().FullName + fullName;
			}
		}

		/// <summary>Indicates whether this instance and a specified object are equal.</summary>
		/// <param name="value">Another object to compare to. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="value" /> is equal to this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000C4A RID: 3146 RVA: 0x000310F4 File Offset: 0x0002F2F4
		public override bool Equals(object value)
		{
			if (value is LicenseProviderAttribute && value != null)
			{
				Type licenseProvider = ((LicenseProviderAttribute)value).LicenseProvider;
				if (licenseProvider == this.LicenseProvider)
				{
					return true;
				}
				if (licenseProvider != null && licenseProvider.Equals(this.LicenseProvider))
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.LicenseProviderAttribute" />.</returns>
		// Token: 0x06000C4B RID: 3147 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000C4C RID: 3148 RVA: 0x00031142 File Offset: 0x0002F342
		// Note: this type is marked as 'beforefieldinit'.
		static LicenseProviderAttribute()
		{
		}

		/// <summary>Specifies the default value, which is no provider. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000D01 RID: 3329
		public static readonly LicenseProviderAttribute Default = new LicenseProviderAttribute();

		// Token: 0x04000D02 RID: 3330
		private Type licenseProviderType;

		// Token: 0x04000D03 RID: 3331
		private string licenseProviderName;
	}
}
