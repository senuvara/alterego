﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides the <see langword="abstract" /> base class for all licenses. A license is granted to a specific instance of a component.</summary>
	// Token: 0x02000197 RID: 407
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public abstract class License : IDisposable
	{
		/// <summary>When overridden in a derived class, gets the license key granted to this component.</summary>
		/// <returns>A license key granted to this component.</returns>
		// Token: 0x1700029A RID: 666
		// (get) Token: 0x06000C15 RID: 3093
		public abstract string LicenseKey { get; }

		/// <summary>When overridden in a derived class, disposes of the resources used by the license.</summary>
		// Token: 0x06000C16 RID: 3094
		public abstract void Dispose();

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.License" /> class. </summary>
		// Token: 0x06000C17 RID: 3095 RVA: 0x0000232F File Offset: 0x0000052F
		protected License()
		{
		}
	}
}
