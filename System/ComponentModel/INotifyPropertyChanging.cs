﻿using System;

namespace System.ComponentModel
{
	/// <summary>Notifies clients that a property value is changing.</summary>
	// Token: 0x02000182 RID: 386
	public interface INotifyPropertyChanging
	{
		/// <summary>Occurs when a property value is changing.</summary>
		// Token: 0x14000010 RID: 16
		// (add) Token: 0x06000BC9 RID: 3017
		// (remove) Token: 0x06000BCA RID: 3018
		event PropertyChangingEventHandler PropertyChanging;
	}
}
