﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies the default property for a component.</summary>
	// Token: 0x02000155 RID: 341
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class DefaultPropertyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultPropertyAttribute" /> class.</summary>
		/// <param name="name">The name of the default property for the component this attribute is bound to. </param>
		// Token: 0x06000A9E RID: 2718 RVA: 0x0002EA43 File Offset: 0x0002CC43
		public DefaultPropertyAttribute(string name)
		{
			this.name = name;
		}

		/// <summary>Gets the name of the default property for the component this attribute is bound to.</summary>
		/// <returns>The name of the default property for the component this attribute is bound to. The default value is <see langword="null" />.</returns>
		// Token: 0x17000242 RID: 578
		// (get) Token: 0x06000A9F RID: 2719 RVA: 0x0002EA52 File Offset: 0x0002CC52
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		/// <summary>Returns whether the value of the given object is equal to the current <see cref="T:System.ComponentModel.DefaultPropertyAttribute" />.</summary>
		/// <param name="obj">The object to test the value equality of. </param>
		/// <returns>
		///     <see langword="true" /> if the value of the given object is equal to that of the current; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AA0 RID: 2720 RVA: 0x0002EA5C File Offset: 0x0002CC5C
		public override bool Equals(object obj)
		{
			DefaultPropertyAttribute defaultPropertyAttribute = obj as DefaultPropertyAttribute;
			return defaultPropertyAttribute != null && defaultPropertyAttribute.Name == this.name;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000AA1 RID: 2721 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000AA2 RID: 2722 RVA: 0x0002EA86 File Offset: 0x0002CC86
		// Note: this type is marked as 'beforefieldinit'.
		static DefaultPropertyAttribute()
		{
		}

		// Token: 0x04000CA7 RID: 3239
		private readonly string name;

		/// <summary>Specifies the default value for the <see cref="T:System.ComponentModel.DefaultPropertyAttribute" />, which is <see langword="null" />. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000CA8 RID: 3240
		public static readonly DefaultPropertyAttribute Default = new DefaultPropertyAttribute(null);
	}
}
