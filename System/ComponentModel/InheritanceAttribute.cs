﻿using System;

namespace System.ComponentModel
{
	/// <summary>Indicates whether the component associated with this attribute has been inherited from a base class. This class cannot be inherited.</summary>
	// Token: 0x020001F2 RID: 498
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event)]
	public sealed class InheritanceAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.InheritanceAttribute" /> class.</summary>
		// Token: 0x06000FC9 RID: 4041 RVA: 0x0003E4B0 File Offset: 0x0003C6B0
		public InheritanceAttribute()
		{
			this.inheritanceLevel = InheritanceAttribute.Default.inheritanceLevel;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.InheritanceAttribute" /> class with the specified inheritance level.</summary>
		/// <param name="inheritanceLevel">An <see cref="T:System.ComponentModel.InheritanceLevel" /> that indicates the level of inheritance to set this attribute to. </param>
		// Token: 0x06000FCA RID: 4042 RVA: 0x0003E4C8 File Offset: 0x0003C6C8
		public InheritanceAttribute(InheritanceLevel inheritanceLevel)
		{
			this.inheritanceLevel = inheritanceLevel;
		}

		/// <summary>Gets or sets the current inheritance level stored in this attribute.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.InheritanceLevel" /> stored in this attribute.</returns>
		// Token: 0x1700033E RID: 830
		// (get) Token: 0x06000FCB RID: 4043 RVA: 0x0003E4D7 File Offset: 0x0003C6D7
		public InheritanceLevel InheritanceLevel
		{
			get
			{
				return this.inheritanceLevel;
			}
		}

		/// <summary>Override to test for equality.</summary>
		/// <param name="value">The object to test. </param>
		/// <returns>
		///     <see langword="true" /> if the object is the same; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000FCC RID: 4044 RVA: 0x0003E4DF File Offset: 0x0003C6DF
		public override bool Equals(object value)
		{
			return value == this || (value is InheritanceAttribute && ((InheritanceAttribute)value).InheritanceLevel == this.inheritanceLevel);
		}

		/// <summary>Returns the hashcode for this object.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.InheritanceAttribute" />.</returns>
		// Token: 0x06000FCD RID: 4045 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Gets a value indicating whether the current value of the attribute is the default value for the attribute.</summary>
		/// <returns>
		///     <see langword="true" /> if the current value of the attribute is the default; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000FCE RID: 4046 RVA: 0x0003E504 File Offset: 0x0003C704
		public override bool IsDefaultAttribute()
		{
			return this.Equals(InheritanceAttribute.Default);
		}

		/// <summary>Converts this attribute to a string.</summary>
		/// <returns>A string that represents this <see cref="T:System.ComponentModel.InheritanceAttribute" />.</returns>
		// Token: 0x06000FCF RID: 4047 RVA: 0x0003E511 File Offset: 0x0003C711
		public override string ToString()
		{
			return TypeDescriptor.GetConverter(typeof(InheritanceLevel)).ConvertToString(this.InheritanceLevel);
		}

		// Token: 0x06000FD0 RID: 4048 RVA: 0x0003E532 File Offset: 0x0003C732
		// Note: this type is marked as 'beforefieldinit'.
		static InheritanceAttribute()
		{
		}

		// Token: 0x04000E23 RID: 3619
		private readonly InheritanceLevel inheritanceLevel;

		/// <summary>Specifies that the component is inherited. This field is read-only.</summary>
		// Token: 0x04000E24 RID: 3620
		public static readonly InheritanceAttribute Inherited = new InheritanceAttribute(InheritanceLevel.Inherited);

		/// <summary>Specifies that the component is inherited and is read-only. This field is read-only.</summary>
		// Token: 0x04000E25 RID: 3621
		public static readonly InheritanceAttribute InheritedReadOnly = new InheritanceAttribute(InheritanceLevel.InheritedReadOnly);

		/// <summary>Specifies that the component is not inherited. This field is read-only.</summary>
		// Token: 0x04000E26 RID: 3622
		public static readonly InheritanceAttribute NotInherited = new InheritanceAttribute(InheritanceLevel.NotInherited);

		/// <summary>Specifies that the default value for <see cref="T:System.ComponentModel.InheritanceAttribute" /> is <see cref="F:System.ComponentModel.InheritanceAttribute.NotInherited" />. This field is read-only.</summary>
		// Token: 0x04000E27 RID: 3623
		public static readonly InheritanceAttribute Default = InheritanceAttribute.NotInherited;
	}
}
