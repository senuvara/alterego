﻿using System;
using System.Collections;
using System.Security.Permissions;

namespace System.ComponentModel
{
	// Token: 0x02000124 RID: 292
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	internal class ArraySubsetEnumerator : IEnumerator
	{
		// Token: 0x06000935 RID: 2357 RVA: 0x0002A2D0 File Offset: 0x000284D0
		public ArraySubsetEnumerator(Array array, int count)
		{
			this.array = array;
			this.total = count;
			this.current = -1;
		}

		// Token: 0x06000936 RID: 2358 RVA: 0x0002A2ED File Offset: 0x000284ED
		public bool MoveNext()
		{
			if (this.current < this.total - 1)
			{
				this.current++;
				return true;
			}
			return false;
		}

		// Token: 0x06000937 RID: 2359 RVA: 0x0002A310 File Offset: 0x00028510
		public void Reset()
		{
			this.current = -1;
		}

		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x06000938 RID: 2360 RVA: 0x0002A319 File Offset: 0x00028519
		public object Current
		{
			get
			{
				if (this.current == -1)
				{
					throw new InvalidOperationException();
				}
				return this.array.GetValue(this.current);
			}
		}

		// Token: 0x04000C27 RID: 3111
		private Array array;

		// Token: 0x04000C28 RID: 3112
		private int total;

		// Token: 0x04000C29 RID: 3113
		private int current;
	}
}
