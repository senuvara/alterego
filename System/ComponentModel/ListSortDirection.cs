﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies the direction of a sort operation.</summary>
	// Token: 0x020001A6 RID: 422
	public enum ListSortDirection
	{
		/// <summary>Sorts in ascending order.</summary>
		// Token: 0x04000D1D RID: 3357
		Ascending,
		/// <summary>Sorts in descending order.</summary>
		// Token: 0x04000D1E RID: 3358
		Descending
	}
}
