﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a type converter to convert 32-bit signed integer objects to and from other representations.</summary>
	// Token: 0x02000190 RID: 400
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class Int32Converter : BaseNumberConverter
	{
		// Token: 0x17000297 RID: 663
		// (get) Token: 0x06000BF8 RID: 3064 RVA: 0x00030601 File Offset: 0x0002E801
		internal override Type TargetType
		{
			get
			{
				return typeof(int);
			}
		}

		// Token: 0x06000BF9 RID: 3065 RVA: 0x0003060D File Offset: 0x0002E80D
		internal override object FromString(string value, int radix)
		{
			return Convert.ToInt32(value, radix);
		}

		// Token: 0x06000BFA RID: 3066 RVA: 0x0003061B File Offset: 0x0002E81B
		internal override object FromString(string value, NumberFormatInfo formatInfo)
		{
			return int.Parse(value, NumberStyles.Integer, formatInfo);
		}

		// Token: 0x06000BFB RID: 3067 RVA: 0x0003062A File Offset: 0x0002E82A
		internal override object FromString(string value, CultureInfo culture)
		{
			return int.Parse(value, culture);
		}

		// Token: 0x06000BFC RID: 3068 RVA: 0x00030638 File Offset: 0x0002E838
		internal override string ToString(object value, NumberFormatInfo formatInfo)
		{
			return ((int)value).ToString("G", formatInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Int32Converter" /> class. </summary>
		// Token: 0x06000BFD RID: 3069 RVA: 0x0002B691 File Offset: 0x00029891
		public Int32Converter()
		{
		}
	}
}
