﻿using System;
using System.Globalization;

namespace System.ComponentModel
{
	/// <summary>Specifies what type to use as a converter for the object this attribute is bound to.</summary>
	// Token: 0x020001D8 RID: 472
	[AttributeUsage(AttributeTargets.All)]
	public sealed class TypeConverterAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.TypeConverterAttribute" /> class with the default type converter, which is an empty string ("").</summary>
		// Token: 0x06000EBC RID: 3772 RVA: 0x00039BCF File Offset: 0x00037DCF
		public TypeConverterAttribute()
		{
			this.typeName = string.Empty;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.TypeConverterAttribute" /> class, using the specified type as the data converter for the object this attribute is bound to.</summary>
		/// <param name="type">A <see cref="T:System.Type" /> that represents the type of the converter class to use for data conversion for the object this attribute is bound to. </param>
		// Token: 0x06000EBD RID: 3773 RVA: 0x00039BE2 File Offset: 0x00037DE2
		public TypeConverterAttribute(Type type)
		{
			this.typeName = type.AssemblyQualifiedName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.TypeConverterAttribute" /> class, using the specified type name as the data converter for the object this attribute is bound to.</summary>
		/// <param name="typeName">The fully qualified name of the class to use for data conversion for the object this attribute is bound to. </param>
		// Token: 0x06000EBE RID: 3774 RVA: 0x00039BF6 File Offset: 0x00037DF6
		public TypeConverterAttribute(string typeName)
		{
			typeName.ToUpper(CultureInfo.InvariantCulture);
			this.typeName = typeName;
		}

		/// <summary>Gets the fully qualified type name of the <see cref="T:System.Type" /> to use as a converter for the object this attribute is bound to.</summary>
		/// <returns>The fully qualified type name of the <see cref="T:System.Type" /> to use as a converter for the object this attribute is bound to, or an empty string ("") if none exists. The default value is an empty string ("").</returns>
		// Token: 0x1700032F RID: 815
		// (get) Token: 0x06000EBF RID: 3775 RVA: 0x00039C11 File Offset: 0x00037E11
		public string ConverterTypeName
		{
			get
			{
				return this.typeName;
			}
		}

		/// <summary>Returns whether the value of the given object is equal to the current <see cref="T:System.ComponentModel.TypeConverterAttribute" />.</summary>
		/// <param name="obj">The object to test the value equality of. </param>
		/// <returns>
		///     <see langword="true" /> if the value of the given object is equal to that of the current <see cref="T:System.ComponentModel.TypeConverterAttribute" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000EC0 RID: 3776 RVA: 0x00039C1C File Offset: 0x00037E1C
		public override bool Equals(object obj)
		{
			TypeConverterAttribute typeConverterAttribute = obj as TypeConverterAttribute;
			return typeConverterAttribute != null && typeConverterAttribute.ConverterTypeName == this.typeName;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.TypeConverterAttribute" />.</returns>
		// Token: 0x06000EC1 RID: 3777 RVA: 0x00039C46 File Offset: 0x00037E46
		public override int GetHashCode()
		{
			return this.typeName.GetHashCode();
		}

		// Token: 0x06000EC2 RID: 3778 RVA: 0x00039C53 File Offset: 0x00037E53
		// Note: this type is marked as 'beforefieldinit'.
		static TypeConverterAttribute()
		{
		}

		// Token: 0x04000DF4 RID: 3572
		private string typeName;

		/// <summary>Specifies the type to use as a converter for the object this attribute is bound to. </summary>
		// Token: 0x04000DF5 RID: 3573
		public static readonly TypeConverterAttribute Default = new TypeConverterAttribute();
	}
}
