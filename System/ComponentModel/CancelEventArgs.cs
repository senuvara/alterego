﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides data for a cancelable event.</summary>
	// Token: 0x02000136 RID: 310
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class CancelEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.CancelEventArgs" /> class with the <see cref="P:System.ComponentModel.CancelEventArgs.Cancel" /> property set to <see langword="false" />.</summary>
		// Token: 0x060009E1 RID: 2529 RVA: 0x0002B699 File Offset: 0x00029899
		public CancelEventArgs() : this(false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.CancelEventArgs" /> class with the <see cref="P:System.ComponentModel.CancelEventArgs.Cancel" /> property set to the given value.</summary>
		/// <param name="cancel">
		///       <see langword="true" /> to cancel the event; otherwise, <see langword="false" />. </param>
		// Token: 0x060009E2 RID: 2530 RVA: 0x0002B6A2 File Offset: 0x000298A2
		public CancelEventArgs(bool cancel)
		{
			this.cancel = cancel;
		}

		/// <summary>Gets or sets a value indicating whether the event should be canceled.</summary>
		/// <returns>
		///     <see langword="true" /> if the event should be canceled; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000211 RID: 529
		// (get) Token: 0x060009E3 RID: 2531 RVA: 0x0002B6B1 File Offset: 0x000298B1
		// (set) Token: 0x060009E4 RID: 2532 RVA: 0x0002B6B9 File Offset: 0x000298B9
		public bool Cancel
		{
			get
			{
				return this.cancel;
			}
			set
			{
				this.cancel = value;
			}
		}

		// Token: 0x04000C64 RID: 3172
		private bool cancel;
	}
}
