﻿using System;

namespace System.ComponentModel
{
	/// <summary>Defines identifiers for types of inheritance levels.</summary>
	// Token: 0x020001F3 RID: 499
	public enum InheritanceLevel
	{
		/// <summary>The object is inherited.</summary>
		// Token: 0x04000E29 RID: 3625
		Inherited = 1,
		/// <summary>The object is inherited, but has read-only access.</summary>
		// Token: 0x04000E2A RID: 3626
		InheritedReadOnly,
		/// <summary>The object is not inherited.</summary>
		// Token: 0x04000E2B RID: 3627
		NotInherited
	}
}
