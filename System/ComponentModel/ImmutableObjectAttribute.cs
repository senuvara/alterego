﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies that an object has no subproperties capable of being edited. This class cannot be inherited.</summary>
	// Token: 0x0200018B RID: 395
	[AttributeUsage(AttributeTargets.All)]
	public sealed class ImmutableObjectAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ImmutableObjectAttribute" /> class.</summary>
		/// <param name="immutable">
		///       <see langword="true" /> if the object is immutable; otherwise, <see langword="false" />. </param>
		// Token: 0x06000BE2 RID: 3042 RVA: 0x000304A7 File Offset: 0x0002E6A7
		public ImmutableObjectAttribute(bool immutable)
		{
			this.immutable = immutable;
		}

		/// <summary>Gets whether the object is immutable.</summary>
		/// <returns>
		///     <see langword="true" /> if the object is immutable; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000292 RID: 658
		// (get) Token: 0x06000BE3 RID: 3043 RVA: 0x000304BD File Offset: 0x0002E6BD
		public bool Immutable
		{
			get
			{
				return this.immutable;
			}
		}

		/// <summary>Returns a value that indicates whether this instance is equal to a specified object.</summary>
		/// <param name="obj">An <see cref="T:System.Object" /> to compare with this instance or a null reference (<see langword="Nothing" /> in Visual Basic).</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> equals the type and value of this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000BE4 RID: 3044 RVA: 0x000304C8 File Offset: 0x0002E6C8
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			ImmutableObjectAttribute immutableObjectAttribute = obj as ImmutableObjectAttribute;
			return immutableObjectAttribute != null && immutableObjectAttribute.Immutable == this.immutable;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.ImmutableObjectAttribute" />.</returns>
		// Token: 0x06000BE5 RID: 3045 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Indicates whether the value of this instance is the default value.</summary>
		/// <returns>
		///     <see langword="true" /> if this instance is the default attribute for the class; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000BE6 RID: 3046 RVA: 0x000304F5 File Offset: 0x0002E6F5
		public override bool IsDefaultAttribute()
		{
			return this.Equals(ImmutableObjectAttribute.Default);
		}

		// Token: 0x06000BE7 RID: 3047 RVA: 0x00030502 File Offset: 0x0002E702
		// Note: this type is marked as 'beforefieldinit'.
		static ImmutableObjectAttribute()
		{
		}

		/// <summary>Specifies that an object has no subproperties that can be edited. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000CE8 RID: 3304
		public static readonly ImmutableObjectAttribute Yes = new ImmutableObjectAttribute(true);

		/// <summary>Specifies that an object has at least one editable subproperty. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000CE9 RID: 3305
		public static readonly ImmutableObjectAttribute No = new ImmutableObjectAttribute(false);

		/// <summary>Represents the default value for <see cref="T:System.ComponentModel.ImmutableObjectAttribute" />.</summary>
		// Token: 0x04000CEA RID: 3306
		public static readonly ImmutableObjectAttribute Default = ImmutableObjectAttribute.No;

		// Token: 0x04000CEB RID: 3307
		private bool immutable = true;
	}
}
