﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies that the property can be used as an application setting.</summary>
	// Token: 0x020001C1 RID: 449
	[AttributeUsage(AttributeTargets.Property)]
	[Obsolete("Use System.ComponentModel.SettingsBindableAttribute instead to work with the new settings model.")]
	public class RecommendedAsConfigurableAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.RecommendedAsConfigurableAttribute" /> class.</summary>
		/// <param name="recommendedAsConfigurable">
		///       <see langword="true" /> if the property this attribute is bound to can be used as an application setting; otherwise, <see langword="false" />. </param>
		// Token: 0x06000DC9 RID: 3529 RVA: 0x00035333 File Offset: 0x00033533
		public RecommendedAsConfigurableAttribute(bool recommendedAsConfigurable)
		{
			this.recommendedAsConfigurable = recommendedAsConfigurable;
		}

		/// <summary>Gets a value indicating whether the property this attribute is bound to can be used as an application setting.</summary>
		/// <returns>
		///     <see langword="true" /> if the property this attribute is bound to can be used as an application setting; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06000DCA RID: 3530 RVA: 0x00035342 File Offset: 0x00033542
		public bool RecommendedAsConfigurable
		{
			get
			{
				return this.recommendedAsConfigurable;
			}
		}

		/// <summary>Indicates whether this instance and a specified object are equal.</summary>
		/// <param name="obj">Another object to compare to. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is equal to this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000DCB RID: 3531 RVA: 0x0003534C File Offset: 0x0003354C
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			RecommendedAsConfigurableAttribute recommendedAsConfigurableAttribute = obj as RecommendedAsConfigurableAttribute;
			return recommendedAsConfigurableAttribute != null && recommendedAsConfigurableAttribute.RecommendedAsConfigurable == this.recommendedAsConfigurable;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.RecommendedAsConfigurableAttribute" />.</returns>
		// Token: 0x06000DCC RID: 3532 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Indicates whether the value of this instance is the default value for the class.</summary>
		/// <returns>
		///     <see langword="true" /> if this instance is the default attribute for the class; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000DCD RID: 3533 RVA: 0x00035379 File Offset: 0x00033579
		public override bool IsDefaultAttribute()
		{
			return !this.recommendedAsConfigurable;
		}

		// Token: 0x06000DCE RID: 3534 RVA: 0x00035384 File Offset: 0x00033584
		// Note: this type is marked as 'beforefieldinit'.
		static RecommendedAsConfigurableAttribute()
		{
		}

		// Token: 0x04000D9C RID: 3484
		private bool recommendedAsConfigurable;

		/// <summary>Specifies that a property cannot be used as an application setting. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000D9D RID: 3485
		public static readonly RecommendedAsConfigurableAttribute No = new RecommendedAsConfigurableAttribute(false);

		/// <summary>Specifies that a property can be used as an application setting. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000D9E RID: 3486
		public static readonly RecommendedAsConfigurableAttribute Yes = new RecommendedAsConfigurableAttribute(true);

		/// <summary>Specifies the default value for the <see cref="T:System.ComponentModel.RecommendedAsConfigurableAttribute" />, which is <see cref="F:System.ComponentModel.RecommendedAsConfigurableAttribute.No" />. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000D9F RID: 3487
		public static readonly RecommendedAsConfigurableAttribute Default = RecommendedAsConfigurableAttribute.No;
	}
}
