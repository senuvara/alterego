﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides the base implementation for the <see cref="T:System.ComponentModel.INestedContainer" /> interface, which enables containers to have an owning component.</summary>
	// Token: 0x020001B2 RID: 434
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class NestedContainer : Container, INestedContainer, IContainer, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.NestedContainer" /> class.</summary>
		/// <param name="owner">The <see cref="T:System.ComponentModel.IComponent" /> that owns this nested container.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="owner" /> is <see langword="null" />.</exception>
		// Token: 0x06000D2B RID: 3371 RVA: 0x00033C93 File Offset: 0x00031E93
		public NestedContainer(IComponent owner)
		{
			if (owner == null)
			{
				throw new ArgumentNullException("owner");
			}
			this._owner = owner;
			this._owner.Disposed += this.OnOwnerDisposed;
		}

		/// <summary>Gets the owning component for this nested container.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.IComponent" /> that owns this nested container.</returns>
		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06000D2C RID: 3372 RVA: 0x00033CC7 File Offset: 0x00031EC7
		public IComponent Owner
		{
			get
			{
				return this._owner;
			}
		}

		/// <summary>Gets the name of the owning component.</summary>
		/// <returns>The name of the owning component.</returns>
		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06000D2D RID: 3373 RVA: 0x00033CD0 File Offset: 0x00031ED0
		protected virtual string OwnerName
		{
			get
			{
				string result = null;
				if (this._owner != null && this._owner.Site != null)
				{
					INestedSite nestedSite = this._owner.Site as INestedSite;
					if (nestedSite != null)
					{
						result = nestedSite.FullName;
					}
					else
					{
						result = this._owner.Site.Name;
					}
				}
				return result;
			}
		}

		/// <summary>Creates a site for the component within the container.</summary>
		/// <param name="component">The <see cref="T:System.ComponentModel.IComponent" /> to create a site for.</param>
		/// <param name="name">The name to assign to <paramref name="component" />, or <see langword="null" /> to skip the name assignment.</param>
		/// <returns>The newly created <see cref="T:System.ComponentModel.ISite" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="component" /> is <see langword="null" />.</exception>
		// Token: 0x06000D2E RID: 3374 RVA: 0x00033D23 File Offset: 0x00031F23
		protected override ISite CreateSite(IComponent component, string name)
		{
			if (component == null)
			{
				throw new ArgumentNullException("component");
			}
			return new NestedContainer.Site(component, this, name);
		}

		/// <summary>Releases the resources used by the nested container.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x06000D2F RID: 3375 RVA: 0x00033D3B File Offset: 0x00031F3B
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this._owner.Disposed -= this.OnOwnerDisposed;
			}
			base.Dispose(disposing);
		}

		/// <summary>Gets the service object of the specified type, if it is available.</summary>
		/// <param name="service">The <see cref="T:System.Type" /> of the service to retrieve.</param>
		/// <returns>An <see cref="T:System.Object" /> that implements the requested service, or <see langword="null" /> if the service cannot be resolved.</returns>
		// Token: 0x06000D30 RID: 3376 RVA: 0x00033D5E File Offset: 0x00031F5E
		protected override object GetService(Type service)
		{
			if (service == typeof(INestedContainer))
			{
				return this;
			}
			return base.GetService(service);
		}

		// Token: 0x06000D31 RID: 3377 RVA: 0x00033D7B File Offset: 0x00031F7B
		private void OnOwnerDisposed(object sender, EventArgs e)
		{
			base.Dispose();
		}

		// Token: 0x04000D76 RID: 3446
		private IComponent _owner;

		// Token: 0x020001B3 RID: 435
		private class Site : INestedSite, ISite, IServiceProvider
		{
			// Token: 0x06000D32 RID: 3378 RVA: 0x00033D83 File Offset: 0x00031F83
			internal Site(IComponent component, NestedContainer container, string name)
			{
				this.component = component;
				this.container = container;
				this.name = name;
			}

			// Token: 0x170002DC RID: 732
			// (get) Token: 0x06000D33 RID: 3379 RVA: 0x00033DA0 File Offset: 0x00031FA0
			public IComponent Component
			{
				get
				{
					return this.component;
				}
			}

			// Token: 0x170002DD RID: 733
			// (get) Token: 0x06000D34 RID: 3380 RVA: 0x00033DA8 File Offset: 0x00031FA8
			public IContainer Container
			{
				get
				{
					return this.container;
				}
			}

			// Token: 0x06000D35 RID: 3381 RVA: 0x00033DB0 File Offset: 0x00031FB0
			public object GetService(Type service)
			{
				if (!(service == typeof(ISite)))
				{
					return this.container.GetService(service);
				}
				return this;
			}

			// Token: 0x170002DE RID: 734
			// (get) Token: 0x06000D36 RID: 3382 RVA: 0x00033DD4 File Offset: 0x00031FD4
			public bool DesignMode
			{
				get
				{
					IComponent owner = this.container.Owner;
					return owner != null && owner.Site != null && owner.Site.DesignMode;
				}
			}

			// Token: 0x170002DF RID: 735
			// (get) Token: 0x06000D37 RID: 3383 RVA: 0x00033E08 File Offset: 0x00032008
			public string FullName
			{
				get
				{
					if (this.name != null)
					{
						string ownerName = this.container.OwnerName;
						string text = this.name;
						if (ownerName != null)
						{
							text = string.Format(CultureInfo.InvariantCulture, "{0}.{1}", ownerName, text);
						}
						return text;
					}
					return this.name;
				}
			}

			// Token: 0x170002E0 RID: 736
			// (get) Token: 0x06000D38 RID: 3384 RVA: 0x00033E4D File Offset: 0x0003204D
			// (set) Token: 0x06000D39 RID: 3385 RVA: 0x00033E55 File Offset: 0x00032055
			public string Name
			{
				get
				{
					return this.name;
				}
				set
				{
					if (value == null || this.name == null || !value.Equals(this.name))
					{
						this.container.ValidateName(this.component, value);
						this.name = value;
					}
				}
			}

			// Token: 0x04000D77 RID: 3447
			private IComponent component;

			// Token: 0x04000D78 RID: 3448
			private NestedContainer container;

			// Token: 0x04000D79 RID: 3449
			private string name;
		}
	}
}
