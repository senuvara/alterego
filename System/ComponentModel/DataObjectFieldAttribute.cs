﻿using System;

namespace System.ComponentModel
{
	/// <summary>Provides metadata for a property representing a data field. This class cannot be inherited.</summary>
	// Token: 0x0200014D RID: 333
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class DataObjectFieldAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DataObjectFieldAttribute" /> class and indicates whether the field is the primary key for the data row.</summary>
		/// <param name="primaryKey">
		///       <see langword="true" /> to indicate that the field is in the primary key of the data row; otherwise, <see langword="false" />.</param>
		// Token: 0x06000A6F RID: 2671 RVA: 0x0002E08D File Offset: 0x0002C28D
		public DataObjectFieldAttribute(bool primaryKey) : this(primaryKey, false, false, -1)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DataObjectFieldAttribute" /> class and indicates whether the field is the primary key for the data row, and whether the field is a database identity field.</summary>
		/// <param name="primaryKey">
		///       <see langword="true" /> to indicate that the field is in the primary key of the data row; otherwise, <see langword="false" />.</param>
		/// <param name="isIdentity">
		///       <see langword="true" /> to indicate that the field is an identity field that uniquely identifies the data row; otherwise, <see langword="false" />.</param>
		// Token: 0x06000A70 RID: 2672 RVA: 0x0002E099 File Offset: 0x0002C299
		public DataObjectFieldAttribute(bool primaryKey, bool isIdentity) : this(primaryKey, isIdentity, false, -1)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DataObjectFieldAttribute" /> class and indicates whether the field is the primary key for the data row, whether the field is a database identity field, and whether the field can be null.</summary>
		/// <param name="primaryKey">
		///       <see langword="true" /> to indicate that the field is in the primary key of the data row; otherwise, <see langword="false" />.</param>
		/// <param name="isIdentity">
		///       <see langword="true" /> to indicate that the field is an identity field that uniquely identifies the data row; otherwise, <see langword="false" />.</param>
		/// <param name="isNullable">
		///       <see langword="true" /> to indicate that the field can be null in the data store; otherwise, <see langword="false" />.</param>
		// Token: 0x06000A71 RID: 2673 RVA: 0x0002E0A5 File Offset: 0x0002C2A5
		public DataObjectFieldAttribute(bool primaryKey, bool isIdentity, bool isNullable) : this(primaryKey, isIdentity, isNullable, -1)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DataObjectFieldAttribute" /> class and indicates whether the field is the primary key for the data row, whether it is a database identity field, and whether it can be null and sets the length of the field.</summary>
		/// <param name="primaryKey">
		///       <see langword="true" /> to indicate that the field is in the primary key of the data row; otherwise, <see langword="false" />.</param>
		/// <param name="isIdentity">
		///       <see langword="true" /> to indicate that the field is an identity field that uniquely identifies the data row; otherwise, <see langword="false" />.</param>
		/// <param name="isNullable">
		///       <see langword="true" /> to indicate that the field can be null in the data store; otherwise, <see langword="false" />.</param>
		/// <param name="length">The length of the field in bytes.</param>
		// Token: 0x06000A72 RID: 2674 RVA: 0x0002E0B1 File Offset: 0x0002C2B1
		public DataObjectFieldAttribute(bool primaryKey, bool isIdentity, bool isNullable, int length)
		{
			this._primaryKey = primaryKey;
			this._isIdentity = isIdentity;
			this._isNullable = isNullable;
			this._length = length;
		}

		/// <summary>Gets a value indicating whether a property represents an identity field in the underlying data.</summary>
		/// <returns>
		///     <see langword="true" /> if the property represents an identity field in the underlying data; otherwise, <see langword="false" />. The default value is <see langword="false" />.</returns>
		// Token: 0x17000238 RID: 568
		// (get) Token: 0x06000A73 RID: 2675 RVA: 0x0002E0D6 File Offset: 0x0002C2D6
		public bool IsIdentity
		{
			get
			{
				return this._isIdentity;
			}
		}

		/// <summary>Gets a value indicating whether a property represents a field that can be null in the underlying data store.</summary>
		/// <returns>
		///     <see langword="true" /> if the property represents a field that can be null in the underlying data store; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000239 RID: 569
		// (get) Token: 0x06000A74 RID: 2676 RVA: 0x0002E0DE File Offset: 0x0002C2DE
		public bool IsNullable
		{
			get
			{
				return this._isNullable;
			}
		}

		/// <summary>Gets the length of the property in bytes.</summary>
		/// <returns>The length of the property in bytes, or -1 if not set.</returns>
		// Token: 0x1700023A RID: 570
		// (get) Token: 0x06000A75 RID: 2677 RVA: 0x0002E0E6 File Offset: 0x0002C2E6
		public int Length
		{
			get
			{
				return this._length;
			}
		}

		/// <summary>Gets a value indicating whether a property is in the primary key in the underlying data.</summary>
		/// <returns>
		///     <see langword="true" /> if the property is in the primary key of the data store; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700023B RID: 571
		// (get) Token: 0x06000A76 RID: 2678 RVA: 0x0002E0EE File Offset: 0x0002C2EE
		public bool PrimaryKey
		{
			get
			{
				return this._primaryKey;
			}
		}

		/// <summary>Returns a value indicating whether this instance is equal to a specified object.</summary>
		/// <param name="obj">An object to compare with this instance of <see cref="T:System.ComponentModel.DataObjectFieldAttribute" />.</param>
		/// <returns>
		///     <see langword="true" /> if this instance is the same as the instance specified by the <paramref name="obj" /> parameter; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000A77 RID: 2679 RVA: 0x0002E0F8 File Offset: 0x0002C2F8
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			DataObjectFieldAttribute dataObjectFieldAttribute = obj as DataObjectFieldAttribute;
			return dataObjectFieldAttribute != null && dataObjectFieldAttribute.IsIdentity == this.IsIdentity && dataObjectFieldAttribute.IsNullable == this.IsNullable && dataObjectFieldAttribute.Length == this.Length && dataObjectFieldAttribute.PrimaryKey == this.PrimaryKey;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000A78 RID: 2680 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x04000C97 RID: 3223
		private bool _primaryKey;

		// Token: 0x04000C98 RID: 3224
		private bool _isIdentity;

		// Token: 0x04000C99 RID: 3225
		private bool _isNullable;

		// Token: 0x04000C9A RID: 3226
		private int _length;
	}
}
