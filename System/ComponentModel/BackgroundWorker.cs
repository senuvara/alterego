﻿using System;
using System.Security.Permissions;
using System.Threading;

namespace System.ComponentModel
{
	/// <summary>Executes an operation on a separate thread.</summary>
	// Token: 0x0200012C RID: 300
	[SRDescription("Executes an operation on a separate thread.")]
	[DefaultEvent("DoWork")]
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class BackgroundWorker : Component
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.BackgroundWorker" /> class.</summary>
		// Token: 0x0600096A RID: 2410 RVA: 0x0002AA6F File Offset: 0x00028C6F
		public BackgroundWorker()
		{
			this.threadStart = new BackgroundWorker.WorkerThreadStartDelegate(this.WorkerThreadStart);
			this.operationCompleted = new SendOrPostCallback(this.AsyncOperationCompleted);
			this.progressReporter = new SendOrPostCallback(this.ProgressReporter);
		}

		// Token: 0x0600096B RID: 2411 RVA: 0x0002AAAD File Offset: 0x00028CAD
		private void AsyncOperationCompleted(object arg)
		{
			this.isRunning = false;
			this.cancellationPending = false;
			this.OnRunWorkerCompleted((RunWorkerCompletedEventArgs)arg);
		}

		/// <summary>Gets a value indicating whether the application has requested cancellation of a background operation.</summary>
		/// <returns>
		///     <see langword="true" /> if the application has requested cancellation of a background operation; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x0600096C RID: 2412 RVA: 0x0002AAC9 File Offset: 0x00028CC9
		[SRDescription("Has the user attempted to cancel the operation? To be accessed from DoWork event handler.")]
		[Browsable(false)]
		public bool CancellationPending
		{
			get
			{
				return this.cancellationPending;
			}
		}

		/// <summary>Requests cancellation of a pending background operation.</summary>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="P:System.ComponentModel.BackgroundWorker.WorkerSupportsCancellation" /> is <see langword="false" />. </exception>
		// Token: 0x0600096D RID: 2413 RVA: 0x0002AAD1 File Offset: 0x00028CD1
		public void CancelAsync()
		{
			if (!this.WorkerSupportsCancellation)
			{
				throw new InvalidOperationException(SR.GetString("This BackgroundWorker states that it doesn't support cancellation. Modify WorkerSupportsCancellation to state that it does support cancellation."));
			}
			this.cancellationPending = true;
		}

		/// <summary>Occurs when <see cref="M:System.ComponentModel.BackgroundWorker.RunWorkerAsync" /> is called.</summary>
		// Token: 0x14000006 RID: 6
		// (add) Token: 0x0600096E RID: 2414 RVA: 0x0002AAF2 File Offset: 0x00028CF2
		// (remove) Token: 0x0600096F RID: 2415 RVA: 0x0002AB05 File Offset: 0x00028D05
		[SRCategory("Asynchronous")]
		[SRDescription("Event handler to be run on a different thread when the operation begins.")]
		public event DoWorkEventHandler DoWork
		{
			add
			{
				base.Events.AddHandler(BackgroundWorker.doWorkKey, value);
			}
			remove
			{
				base.Events.RemoveHandler(BackgroundWorker.doWorkKey, value);
			}
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.ComponentModel.BackgroundWorker" /> is running an asynchronous operation.</summary>
		/// <returns>
		///     <see langword="true" />, if the <see cref="T:System.ComponentModel.BackgroundWorker" /> is running an asynchronous operation; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x06000970 RID: 2416 RVA: 0x0002AB18 File Offset: 0x00028D18
		[Browsable(false)]
		[SRDescription("Is the worker still currently working on a background operation?")]
		public bool IsBusy
		{
			get
			{
				return this.isRunning;
			}
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.BackgroundWorker.DoWork" /> event. </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
		// Token: 0x06000971 RID: 2417 RVA: 0x0002AB20 File Offset: 0x00028D20
		protected virtual void OnDoWork(DoWorkEventArgs e)
		{
			DoWorkEventHandler doWorkEventHandler = (DoWorkEventHandler)base.Events[BackgroundWorker.doWorkKey];
			if (doWorkEventHandler != null)
			{
				doWorkEventHandler(this, e);
			}
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.BackgroundWorker.RunWorkerCompleted" /> event.</summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data. </param>
		// Token: 0x06000972 RID: 2418 RVA: 0x0002AB50 File Offset: 0x00028D50
		protected virtual void OnRunWorkerCompleted(RunWorkerCompletedEventArgs e)
		{
			RunWorkerCompletedEventHandler runWorkerCompletedEventHandler = (RunWorkerCompletedEventHandler)base.Events[BackgroundWorker.runWorkerCompletedKey];
			if (runWorkerCompletedEventHandler != null)
			{
				runWorkerCompletedEventHandler(this, e);
			}
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.BackgroundWorker.ProgressChanged" /> event.</summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data. </param>
		// Token: 0x06000973 RID: 2419 RVA: 0x0002AB80 File Offset: 0x00028D80
		protected virtual void OnProgressChanged(ProgressChangedEventArgs e)
		{
			ProgressChangedEventHandler progressChangedEventHandler = (ProgressChangedEventHandler)base.Events[BackgroundWorker.progressChangedKey];
			if (progressChangedEventHandler != null)
			{
				progressChangedEventHandler(this, e);
			}
		}

		/// <summary>Occurs when <see cref="M:System.ComponentModel.BackgroundWorker.ReportProgress(System.Int32)" /> is called.</summary>
		// Token: 0x14000007 RID: 7
		// (add) Token: 0x06000974 RID: 2420 RVA: 0x0002ABAE File Offset: 0x00028DAE
		// (remove) Token: 0x06000975 RID: 2421 RVA: 0x0002ABC1 File Offset: 0x00028DC1
		[SRDescription("Raised when the worker thread indicates that some progress has been made.")]
		[SRCategory("Asynchronous")]
		public event ProgressChangedEventHandler ProgressChanged
		{
			add
			{
				base.Events.AddHandler(BackgroundWorker.progressChangedKey, value);
			}
			remove
			{
				base.Events.RemoveHandler(BackgroundWorker.progressChangedKey, value);
			}
		}

		// Token: 0x06000976 RID: 2422 RVA: 0x0002ABD4 File Offset: 0x00028DD4
		private void ProgressReporter(object arg)
		{
			this.OnProgressChanged((ProgressChangedEventArgs)arg);
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.BackgroundWorker.ProgressChanged" /> event.</summary>
		/// <param name="percentProgress">The percentage, from 0 to 100, of the background operation that is complete. </param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.ComponentModel.BackgroundWorker.WorkerReportsProgress" /> property is set to <see langword="false" />. </exception>
		// Token: 0x06000977 RID: 2423 RVA: 0x0002ABE2 File Offset: 0x00028DE2
		public void ReportProgress(int percentProgress)
		{
			this.ReportProgress(percentProgress, null);
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.BackgroundWorker.ProgressChanged" /> event.</summary>
		/// <param name="percentProgress">The percentage, from 0 to 100, of the background operation that is complete.</param>
		/// <param name="userState">The state object passed to <see cref="M:System.ComponentModel.BackgroundWorker.RunWorkerAsync(System.Object)" />.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.ComponentModel.BackgroundWorker.WorkerReportsProgress" /> property is set to <see langword="false" />. </exception>
		// Token: 0x06000978 RID: 2424 RVA: 0x0002ABEC File Offset: 0x00028DEC
		public void ReportProgress(int percentProgress, object userState)
		{
			if (!this.WorkerReportsProgress)
			{
				throw new InvalidOperationException(SR.GetString("This BackgroundWorker states that it doesn't report progress. Modify WorkerReportsProgress to state that it does report progress."));
			}
			ProgressChangedEventArgs progressChangedEventArgs = new ProgressChangedEventArgs(percentProgress, userState);
			if (this.asyncOperation != null)
			{
				this.asyncOperation.Post(this.progressReporter, progressChangedEventArgs);
				return;
			}
			this.progressReporter(progressChangedEventArgs);
		}

		/// <summary>Starts execution of a background operation.</summary>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="P:System.ComponentModel.BackgroundWorker.IsBusy" /> is <see langword="true" />.</exception>
		// Token: 0x06000979 RID: 2425 RVA: 0x0002AC40 File Offset: 0x00028E40
		public void RunWorkerAsync()
		{
			this.RunWorkerAsync(null);
		}

		/// <summary>Starts execution of a background operation.</summary>
		/// <param name="argument">A parameter for use by the background operation to be executed in the <see cref="E:System.ComponentModel.BackgroundWorker.DoWork" /> event handler. </param>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="P:System.ComponentModel.BackgroundWorker.IsBusy" /> is <see langword="true" />. </exception>
		// Token: 0x0600097A RID: 2426 RVA: 0x0002AC4C File Offset: 0x00028E4C
		public void RunWorkerAsync(object argument)
		{
			if (this.isRunning)
			{
				throw new InvalidOperationException(SR.GetString("This BackgroundWorker is currently busy and cannot run multiple tasks concurrently."));
			}
			this.isRunning = true;
			this.cancellationPending = false;
			this.asyncOperation = AsyncOperationManager.CreateOperation(null);
			this.threadStart.BeginInvoke(argument, null, null);
		}

		/// <summary>Occurs when the background operation has completed, has been canceled, or has raised an exception.</summary>
		// Token: 0x14000008 RID: 8
		// (add) Token: 0x0600097B RID: 2427 RVA: 0x0002AC9A File Offset: 0x00028E9A
		// (remove) Token: 0x0600097C RID: 2428 RVA: 0x0002ACAD File Offset: 0x00028EAD
		[SRCategory("Asynchronous")]
		[SRDescription("Raised when the worker has completed (either through success, failure, or cancellation).")]
		public event RunWorkerCompletedEventHandler RunWorkerCompleted
		{
			add
			{
				base.Events.AddHandler(BackgroundWorker.runWorkerCompletedKey, value);
			}
			remove
			{
				base.Events.RemoveHandler(BackgroundWorker.runWorkerCompletedKey, value);
			}
		}

		/// <summary>Gets or sets a value indicating whether the <see cref="T:System.ComponentModel.BackgroundWorker" /> can report progress updates.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.ComponentModel.BackgroundWorker" /> supports progress updates; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x0600097D RID: 2429 RVA: 0x0002ACC0 File Offset: 0x00028EC0
		// (set) Token: 0x0600097E RID: 2430 RVA: 0x0002ACC8 File Offset: 0x00028EC8
		[SRCategory("Asynchronous")]
		[DefaultValue(false)]
		[SRDescription("Whether the worker will report progress.")]
		public bool WorkerReportsProgress
		{
			get
			{
				return this.workerReportsProgress;
			}
			set
			{
				this.workerReportsProgress = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the <see cref="T:System.ComponentModel.BackgroundWorker" /> supports asynchronous cancellation.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.ComponentModel.BackgroundWorker" /> supports cancellation; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x0600097F RID: 2431 RVA: 0x0002ACD1 File Offset: 0x00028ED1
		// (set) Token: 0x06000980 RID: 2432 RVA: 0x0002ACD9 File Offset: 0x00028ED9
		[SRCategory("Asynchronous")]
		[DefaultValue(false)]
		[SRDescription("Whether the worker supports cancellation.")]
		public bool WorkerSupportsCancellation
		{
			get
			{
				return this.canCancelWorker;
			}
			set
			{
				this.canCancelWorker = value;
			}
		}

		// Token: 0x06000981 RID: 2433 RVA: 0x0002ACE4 File Offset: 0x00028EE4
		private void WorkerThreadStart(object argument)
		{
			object result = null;
			Exception error = null;
			bool cancelled = false;
			try
			{
				DoWorkEventArgs doWorkEventArgs = new DoWorkEventArgs(argument);
				this.OnDoWork(doWorkEventArgs);
				if (doWorkEventArgs.Cancel)
				{
					cancelled = true;
				}
				else
				{
					result = doWorkEventArgs.Result;
				}
			}
			catch (Exception error)
			{
			}
			RunWorkerCompletedEventArgs arg = new RunWorkerCompletedEventArgs(result, error, cancelled);
			this.asyncOperation.PostOperationCompleted(this.operationCompleted, arg);
		}

		// Token: 0x06000982 RID: 2434 RVA: 0x0002AD4C File Offset: 0x00028F4C
		// Note: this type is marked as 'beforefieldinit'.
		static BackgroundWorker()
		{
		}

		// Token: 0x04000C3B RID: 3131
		private static readonly object doWorkKey = new object();

		// Token: 0x04000C3C RID: 3132
		private static readonly object runWorkerCompletedKey = new object();

		// Token: 0x04000C3D RID: 3133
		private static readonly object progressChangedKey = new object();

		// Token: 0x04000C3E RID: 3134
		private bool canCancelWorker;

		// Token: 0x04000C3F RID: 3135
		private bool workerReportsProgress;

		// Token: 0x04000C40 RID: 3136
		private bool cancellationPending;

		// Token: 0x04000C41 RID: 3137
		private bool isRunning;

		// Token: 0x04000C42 RID: 3138
		private AsyncOperation asyncOperation;

		// Token: 0x04000C43 RID: 3139
		private readonly BackgroundWorker.WorkerThreadStartDelegate threadStart;

		// Token: 0x04000C44 RID: 3140
		private readonly SendOrPostCallback operationCompleted;

		// Token: 0x04000C45 RID: 3141
		private readonly SendOrPostCallback progressReporter;

		// Token: 0x0200012D RID: 301
		// (Invoke) Token: 0x06000984 RID: 2436
		private delegate void WorkerThreadStartDelegate(object argument);
	}
}
