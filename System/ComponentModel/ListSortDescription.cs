﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a description of the sort operation applied to a data source.</summary>
	// Token: 0x020001A4 RID: 420
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class ListSortDescription
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ListSortDescription" /> class with the specified property description and direction.</summary>
		/// <param name="property">The <see cref="T:System.ComponentModel.PropertyDescriptor" /> that describes the property by which the data source is sorted.</param>
		/// <param name="direction">One of the <see cref="T:System.ComponentModel.ListSortDescription" />  values.</param>
		// Token: 0x06000C60 RID: 3168 RVA: 0x00031260 File Offset: 0x0002F460
		public ListSortDescription(PropertyDescriptor property, ListSortDirection direction)
		{
			this.property = property;
			this.sortDirection = direction;
		}

		/// <summary>Gets or sets the abstract description of a class property associated with this <see cref="T:System.ComponentModel.ListSortDescription" /></summary>
		/// <returns>The <see cref="T:System.ComponentModel.PropertyDescriptor" /> associated with this <see cref="T:System.ComponentModel.ListSortDescription" />. </returns>
		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x06000C61 RID: 3169 RVA: 0x00031276 File Offset: 0x0002F476
		// (set) Token: 0x06000C62 RID: 3170 RVA: 0x0003127E File Offset: 0x0002F47E
		public PropertyDescriptor PropertyDescriptor
		{
			get
			{
				return this.property;
			}
			set
			{
				this.property = value;
			}
		}

		/// <summary>Gets or sets the direction of the sort operation associated with this <see cref="T:System.ComponentModel.ListSortDescription" />.</summary>
		/// <returns>One of the <see cref="T:System.ComponentModel.ListSortDirection" /> values. </returns>
		// Token: 0x170002A8 RID: 680
		// (get) Token: 0x06000C63 RID: 3171 RVA: 0x00031287 File Offset: 0x0002F487
		// (set) Token: 0x06000C64 RID: 3172 RVA: 0x0003128F File Offset: 0x0002F48F
		public ListSortDirection SortDirection
		{
			get
			{
				return this.sortDirection;
			}
			set
			{
				this.sortDirection = value;
			}
		}

		// Token: 0x04000D19 RID: 3353
		private PropertyDescriptor property;

		// Token: 0x04000D1A RID: 3354
		private ListSortDirection sortDirection;
	}
}
