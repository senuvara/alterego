﻿using System;
using System.IO;
using System.Security.Permissions;

namespace System.ComponentModel
{
	// Token: 0x02000192 RID: 402
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	internal static class IntSecurity
	{
		// Token: 0x06000C04 RID: 3076 RVA: 0x000306B1 File Offset: 0x0002E8B1
		public static string UnsafeGetFullPath(string fileName)
		{
			return Path.GetFullPath(fileName);
		}
	}
}
