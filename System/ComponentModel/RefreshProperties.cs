﻿using System;

namespace System.ComponentModel
{
	/// <summary>Defines identifiers that indicate the type of a refresh of the Properties window.</summary>
	// Token: 0x020001F8 RID: 504
	public enum RefreshProperties
	{
		/// <summary>No refresh is necessary.</summary>
		// Token: 0x04000E3B RID: 3643
		None,
		/// <summary>The properties should be requeried and the view should be refreshed.</summary>
		// Token: 0x04000E3C RID: 3644
		All,
		/// <summary>The view should be refreshed.</summary>
		// Token: 0x04000E3D RID: 3645
		Repaint
	}
}
