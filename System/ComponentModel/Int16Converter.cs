﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a type converter to convert 16-bit signed integer objects to and from other representations.</summary>
	// Token: 0x0200018F RID: 399
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class Int16Converter : BaseNumberConverter
	{
		// Token: 0x17000296 RID: 662
		// (get) Token: 0x06000BF2 RID: 3058 RVA: 0x000305A8 File Offset: 0x0002E7A8
		internal override Type TargetType
		{
			get
			{
				return typeof(short);
			}
		}

		// Token: 0x06000BF3 RID: 3059 RVA: 0x000305B4 File Offset: 0x0002E7B4
		internal override object FromString(string value, int radix)
		{
			return Convert.ToInt16(value, radix);
		}

		// Token: 0x06000BF4 RID: 3060 RVA: 0x000305C2 File Offset: 0x0002E7C2
		internal override object FromString(string value, CultureInfo culture)
		{
			return short.Parse(value, culture);
		}

		// Token: 0x06000BF5 RID: 3061 RVA: 0x000305D0 File Offset: 0x0002E7D0
		internal override object FromString(string value, NumberFormatInfo formatInfo)
		{
			return short.Parse(value, NumberStyles.Integer, formatInfo);
		}

		// Token: 0x06000BF6 RID: 3062 RVA: 0x000305E0 File Offset: 0x0002E7E0
		internal override string ToString(object value, NumberFormatInfo formatInfo)
		{
			return ((short)value).ToString("G", formatInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Int16Converter" /> class. </summary>
		// Token: 0x06000BF7 RID: 3063 RVA: 0x0002B691 File Offset: 0x00029891
		public Int16Converter()
		{
		}
	}
}
