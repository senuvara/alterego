﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Creates an instance of a particular type of property from a drop-down box within the <see cref="T:System.Windows.Forms.PropertyGrid" />. </summary>
	// Token: 0x0200018E RID: 398
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public abstract class InstanceCreationEditor
	{
		/// <summary>Gets the specified text.</summary>
		/// <returns>The specified text.</returns>
		// Token: 0x17000295 RID: 661
		// (get) Token: 0x06000BEF RID: 3055 RVA: 0x0003059C File Offset: 0x0002E79C
		public virtual string Text
		{
			get
			{
				return SR.GetString("(New...)");
			}
		}

		/// <summary>When overridden in a derived class, returns an instance of the specified type.</summary>
		/// <param name="context">The context information.</param>
		/// <param name="instanceType">The specified type.</param>
		/// <returns>An instance of the specified type or <see langword="null" />.</returns>
		// Token: 0x06000BF0 RID: 3056
		public abstract object CreateInstance(ITypeDescriptorContext context, Type instanceType);

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.InstanceCreationEditor" /> class.</summary>
		// Token: 0x06000BF1 RID: 3057 RVA: 0x0000232F File Offset: 0x0000052F
		protected InstanceCreationEditor()
		{
		}
	}
}
