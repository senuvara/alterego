﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020001FE RID: 510
	internal sealed class SRCategoryAttribute : CategoryAttribute
	{
		// Token: 0x06001007 RID: 4103 RVA: 0x0003EDA4 File Offset: 0x0003CFA4
		public SRCategoryAttribute(string category) : base(category)
		{
		}
	}
}
