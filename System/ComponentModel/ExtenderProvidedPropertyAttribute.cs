﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies a property that is offered by an extender provider. This class cannot be inherited.</summary>
	// Token: 0x0200016D RID: 365
	[AttributeUsage(AttributeTargets.All)]
	public sealed class ExtenderProvidedPropertyAttribute : Attribute
	{
		// Token: 0x06000B66 RID: 2918 RVA: 0x0003032D File Offset: 0x0002E52D
		internal static ExtenderProvidedPropertyAttribute Create(PropertyDescriptor extenderProperty, Type receiverType, IExtenderProvider provider)
		{
			return new ExtenderProvidedPropertyAttribute
			{
				extenderProperty = extenderProperty,
				receiverType = receiverType,
				provider = provider
			};
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ExtenderProvidedPropertyAttribute" /> class. </summary>
		// Token: 0x06000B67 RID: 2919 RVA: 0x000020AE File Offset: 0x000002AE
		public ExtenderProvidedPropertyAttribute()
		{
		}

		/// <summary>Gets the property that is being provided.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.PropertyDescriptor" /> encapsulating the property that is being provided.</returns>
		// Token: 0x1700026D RID: 621
		// (get) Token: 0x06000B68 RID: 2920 RVA: 0x00030349 File Offset: 0x0002E549
		public PropertyDescriptor ExtenderProperty
		{
			get
			{
				return this.extenderProperty;
			}
		}

		/// <summary>Gets the extender provider that is providing the property.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.IExtenderProvider" /> that is providing the property.</returns>
		// Token: 0x1700026E RID: 622
		// (get) Token: 0x06000B69 RID: 2921 RVA: 0x00030351 File Offset: 0x0002E551
		public IExtenderProvider Provider
		{
			get
			{
				return this.provider;
			}
		}

		/// <summary>Gets the type of object that can receive the property.</summary>
		/// <returns>A <see cref="T:System.Type" /> describing the type of object that can receive the property.</returns>
		// Token: 0x1700026F RID: 623
		// (get) Token: 0x06000B6A RID: 2922 RVA: 0x00030359 File Offset: 0x0002E559
		public Type ReceiverType
		{
			get
			{
				return this.receiverType;
			}
		}

		/// <summary>Returns a value that indicates whether this instance is equal to a specified object.</summary>
		/// <param name="obj">An <see cref="T:System.Object" /> to compare with this instance or a null reference (<see langword="Nothing" /> in Visual Basic).</param>
		/// <returns>
		///   <see langword="true" /> if <paramref name="obj" /> equals the type and value of this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000B6B RID: 2923 RVA: 0x00030364 File Offset: 0x0002E564
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			ExtenderProvidedPropertyAttribute extenderProvidedPropertyAttribute = obj as ExtenderProvidedPropertyAttribute;
			return extenderProvidedPropertyAttribute != null && extenderProvidedPropertyAttribute.extenderProperty.Equals(this.extenderProperty) && extenderProvidedPropertyAttribute.provider.Equals(this.provider) && extenderProvidedPropertyAttribute.receiverType.Equals(this.receiverType);
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000B6C RID: 2924 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Provides an indication whether the value of this instance is the default value for the derived class.</summary>
		/// <returns>
		///     <see langword="true" /> if this instance is the default attribute for the class; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000B6D RID: 2925 RVA: 0x000303BA File Offset: 0x0002E5BA
		public override bool IsDefaultAttribute()
		{
			return this.receiverType == null;
		}

		// Token: 0x04000CE4 RID: 3300
		private PropertyDescriptor extenderProperty;

		// Token: 0x04000CE5 RID: 3301
		private IExtenderProvider provider;

		// Token: 0x04000CE6 RID: 3302
		private Type receiverType;
	}
}
