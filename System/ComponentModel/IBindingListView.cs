﻿using System;
using System.Collections;

namespace System.ComponentModel
{
	/// <summary>Extends the <see cref="T:System.ComponentModel.IBindingList" /> interface by providing advanced sorting and filtering capabilities.</summary>
	// Token: 0x02000172 RID: 370
	public interface IBindingListView : IBindingList, IList, ICollection, IEnumerable
	{
		/// <summary>Sorts the data source based on the given <see cref="T:System.ComponentModel.ListSortDescriptionCollection" />.</summary>
		/// <param name="sorts">The <see cref="T:System.ComponentModel.ListSortDescriptionCollection" /> containing the sorts to apply to the data source.</param>
		// Token: 0x06000B8C RID: 2956
		void ApplySort(ListSortDescriptionCollection sorts);

		/// <summary>Gets or sets the filter to be used to exclude items from the collection of items returned by the data source</summary>
		/// <returns>The string used to filter items out in the item collection returned by the data source. </returns>
		// Token: 0x1700027A RID: 634
		// (get) Token: 0x06000B8D RID: 2957
		// (set) Token: 0x06000B8E RID: 2958
		string Filter { get; set; }

		/// <summary>Gets the collection of sort descriptions currently applied to the data source.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.ListSortDescriptionCollection" /> currently applied to the data source.</returns>
		// Token: 0x1700027B RID: 635
		// (get) Token: 0x06000B8F RID: 2959
		ListSortDescriptionCollection SortDescriptions { get; }

		/// <summary>Removes the current filter applied to the data source.</summary>
		// Token: 0x06000B90 RID: 2960
		void RemoveFilter();

		/// <summary>Gets a value indicating whether the data source supports advanced sorting. </summary>
		/// <returns>
		///     <see langword="true" /> if the data source supports advanced sorting; otherwise, <see langword="false" />. </returns>
		// Token: 0x1700027C RID: 636
		// (get) Token: 0x06000B91 RID: 2961
		bool SupportsAdvancedSorting { get; }

		/// <summary>Gets a value indicating whether the data source supports filtering. </summary>
		/// <returns>
		///     <see langword="true" /> if the data source supports filtering; otherwise, <see langword="false" />. </returns>
		// Token: 0x1700027D RID: 637
		// (get) Token: 0x06000B92 RID: 2962
		bool SupportsFiltering { get; }
	}
}
