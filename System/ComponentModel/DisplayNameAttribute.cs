﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies the display name for a property, event, or public void method which takes no arguments. </summary>
	// Token: 0x0200015F RID: 351
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event)]
	public class DisplayNameAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DisplayNameAttribute" /> class.</summary>
		// Token: 0x06000AEB RID: 2795 RVA: 0x0002F103 File Offset: 0x0002D303
		public DisplayNameAttribute() : this(string.Empty)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DisplayNameAttribute" /> class using the display name.</summary>
		/// <param name="displayName">The display name.</param>
		// Token: 0x06000AEC RID: 2796 RVA: 0x0002F110 File Offset: 0x0002D310
		public DisplayNameAttribute(string displayName)
		{
			this._displayName = displayName;
		}

		/// <summary>Gets the display name for a property, event, or public void method that takes no arguments stored in this attribute.</summary>
		/// <returns>The display name.</returns>
		// Token: 0x1700024F RID: 591
		// (get) Token: 0x06000AED RID: 2797 RVA: 0x0002F11F File Offset: 0x0002D31F
		public virtual string DisplayName
		{
			get
			{
				return this.DisplayNameValue;
			}
		}

		/// <summary>Gets or sets the display name.</summary>
		/// <returns>The display name.</returns>
		// Token: 0x17000250 RID: 592
		// (get) Token: 0x06000AEE RID: 2798 RVA: 0x0002F127 File Offset: 0x0002D327
		// (set) Token: 0x06000AEF RID: 2799 RVA: 0x0002F12F File Offset: 0x0002D32F
		protected string DisplayNameValue
		{
			get
			{
				return this._displayName;
			}
			set
			{
				this._displayName = value;
			}
		}

		/// <summary>Determines whether two <see cref="T:System.ComponentModel.DisplayNameAttribute" /> instances are equal.</summary>
		/// <param name="obj">The <see cref="T:System.ComponentModel.DisplayNameAttribute" /> to test the value equality of.</param>
		/// <returns>
		///     <see langword="true" /> if the value of the given object is equal to that of the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AF0 RID: 2800 RVA: 0x0002F138 File Offset: 0x0002D338
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			DisplayNameAttribute displayNameAttribute = obj as DisplayNameAttribute;
			return displayNameAttribute != null && displayNameAttribute.DisplayName == this.DisplayName;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.DisplayNameAttribute" />.</returns>
		// Token: 0x06000AF1 RID: 2801 RVA: 0x0002F168 File Offset: 0x0002D368
		public override int GetHashCode()
		{
			return this.DisplayName.GetHashCode();
		}

		/// <summary>Determines if this attribute is the default.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute is the default value for this attribute class; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AF2 RID: 2802 RVA: 0x0002F175 File Offset: 0x0002D375
		public override bool IsDefaultAttribute()
		{
			return this.Equals(DisplayNameAttribute.Default);
		}

		// Token: 0x06000AF3 RID: 2803 RVA: 0x0002F182 File Offset: 0x0002D382
		// Note: this type is marked as 'beforefieldinit'.
		static DisplayNameAttribute()
		{
		}

		/// <summary>Specifies the default value for the <see cref="T:System.ComponentModel.DisplayNameAttribute" />. This field is read-only.</summary>
		// Token: 0x04000CC7 RID: 3271
		public static readonly DisplayNameAttribute Default = new DisplayNameAttribute();

		// Token: 0x04000CC8 RID: 3272
		private string _displayName;
	}
}
