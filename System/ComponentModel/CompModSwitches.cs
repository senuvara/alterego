﻿using System;
using System.Diagnostics;
using System.Security.Permissions;

namespace System.ComponentModel
{
	// Token: 0x0200013E RID: 318
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	internal static class CompModSwitches
	{
		// Token: 0x17000223 RID: 547
		// (get) Token: 0x06000A0D RID: 2573 RVA: 0x0002BD47 File Offset: 0x00029F47
		public static BooleanSwitch CommonDesignerServices
		{
			get
			{
				if (CompModSwitches.commonDesignerServices == null)
				{
					CompModSwitches.commonDesignerServices = new BooleanSwitch("CommonDesignerServices", "Assert if any common designer service is not found.");
				}
				return CompModSwitches.commonDesignerServices;
			}
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x06000A0E RID: 2574 RVA: 0x0002BD6F File Offset: 0x00029F6F
		public static TraceSwitch EventLog
		{
			get
			{
				if (CompModSwitches.eventLog == null)
				{
					CompModSwitches.eventLog = new TraceSwitch("EventLog", "Enable tracing for the EventLog component.");
				}
				return CompModSwitches.eventLog;
			}
		}

		// Token: 0x04000C7B RID: 3195
		private static volatile BooleanSwitch commonDesignerServices;

		// Token: 0x04000C7C RID: 3196
		private static volatile TraceSwitch eventLog;
	}
}
