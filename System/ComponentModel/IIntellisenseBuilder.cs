﻿using System;

namespace System.ComponentModel
{
	/// <summary>Provides an interface to facilitate the retrieval of the builder's name and to display the builder.</summary>
	// Token: 0x0200017C RID: 380
	public interface IIntellisenseBuilder
	{
		/// <summary>Gets a localized name.</summary>
		/// <returns>A localized name.</returns>
		// Token: 0x17000283 RID: 643
		// (get) Token: 0x06000BBD RID: 3005
		string Name { get; }

		/// <summary>Shows the builder.</summary>
		/// <param name="language">The language service that is calling the builder.</param>
		/// <param name="value">The expression being edited.</param>
		/// <param name="newValue">The new value.</param>
		/// <returns>
		///     <see langword="true" /> if the value should be replaced with <paramref name="newValue" />; otherwise, <see langword="false" /> (if the user cancels, for example).</returns>
		// Token: 0x06000BBE RID: 3006
		bool Show(string language, string value, ref string newValue);
	}
}
