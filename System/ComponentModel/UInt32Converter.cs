﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a type converter to convert 32-bit unsigned integer objects to and from various other representations.</summary>
	// Token: 0x020001ED RID: 493
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class UInt32Converter : BaseNumberConverter
	{
		// Token: 0x17000337 RID: 823
		// (get) Token: 0x06000F9D RID: 3997 RVA: 0x0003D9C9 File Offset: 0x0003BBC9
		internal override Type TargetType
		{
			get
			{
				return typeof(uint);
			}
		}

		// Token: 0x06000F9E RID: 3998 RVA: 0x0003D9D5 File Offset: 0x0003BBD5
		internal override object FromString(string value, int radix)
		{
			return Convert.ToUInt32(value, radix);
		}

		// Token: 0x06000F9F RID: 3999 RVA: 0x0003D9E3 File Offset: 0x0003BBE3
		internal override object FromString(string value, NumberFormatInfo formatInfo)
		{
			return uint.Parse(value, NumberStyles.Integer, formatInfo);
		}

		// Token: 0x06000FA0 RID: 4000 RVA: 0x0003D9F2 File Offset: 0x0003BBF2
		internal override object FromString(string value, CultureInfo culture)
		{
			return uint.Parse(value, culture);
		}

		// Token: 0x06000FA1 RID: 4001 RVA: 0x0003DA00 File Offset: 0x0003BC00
		internal override string ToString(object value, NumberFormatInfo formatInfo)
		{
			return ((uint)value).ToString("G", formatInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.UInt32Converter" /> class. </summary>
		// Token: 0x06000FA2 RID: 4002 RVA: 0x0002B691 File Offset: 0x00029891
		public UInt32Converter()
		{
		}
	}
}
