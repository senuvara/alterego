﻿using System;
using System.Collections;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides supplemental metadata to the <see cref="T:System.ComponentModel.TypeDescriptor" />.</summary>
	// Token: 0x020001D9 RID: 473
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public abstract class TypeDescriptionProvider
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.TypeDescriptionProvider" /> class.</summary>
		// Token: 0x06000EC3 RID: 3779 RVA: 0x0000232F File Offset: 0x0000052F
		protected TypeDescriptionProvider()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.TypeDescriptionProvider" /> class using a parent type description provider.</summary>
		/// <param name="parent">The parent type description provider.</param>
		// Token: 0x06000EC4 RID: 3780 RVA: 0x00039C5F File Offset: 0x00037E5F
		protected TypeDescriptionProvider(TypeDescriptionProvider parent)
		{
			this._parent = parent;
		}

		/// <summary>Creates an object that can substitute for another data type.</summary>
		/// <param name="provider">An optional service provider.</param>
		/// <param name="objectType">The type of object to create. This parameter is never <see langword="null" />.</param>
		/// <param name="argTypes">An optional array of types that represent the parameter types to be passed to the object's constructor. This array can be <see langword="null" /> or of zero length.</param>
		/// <param name="args">An optional array of parameter values to pass to the object's constructor.</param>
		/// <returns>The substitute <see cref="T:System.Object" />.</returns>
		// Token: 0x06000EC5 RID: 3781 RVA: 0x00039C6E File Offset: 0x00037E6E
		public virtual object CreateInstance(IServiceProvider provider, Type objectType, Type[] argTypes, object[] args)
		{
			if (this._parent != null)
			{
				return this._parent.CreateInstance(provider, objectType, argTypes, args);
			}
			if (objectType == null)
			{
				throw new ArgumentNullException("objectType");
			}
			return SecurityUtils.SecureCreateInstance(objectType, args);
		}

		/// <summary>Gets a per-object cache, accessed as an <see cref="T:System.Collections.IDictionary" /> of key/value pairs.</summary>
		/// <param name="instance">The object for which to get the cache.</param>
		/// <returns>An <see cref="T:System.Collections.IDictionary" /> if the provided object supports caching; otherwise, <see langword="null" />.</returns>
		// Token: 0x06000EC6 RID: 3782 RVA: 0x00039CA5 File Offset: 0x00037EA5
		public virtual IDictionary GetCache(object instance)
		{
			if (this._parent != null)
			{
				return this._parent.GetCache(instance);
			}
			return null;
		}

		/// <summary>Gets an extended custom type descriptor for the given object.</summary>
		/// <param name="instance">The object for which to get the extended type descriptor.</param>
		/// <returns>An <see cref="T:System.ComponentModel.ICustomTypeDescriptor" /> that can provide extended metadata for the object.</returns>
		// Token: 0x06000EC7 RID: 3783 RVA: 0x00039CBD File Offset: 0x00037EBD
		public virtual ICustomTypeDescriptor GetExtendedTypeDescriptor(object instance)
		{
			if (this._parent != null)
			{
				return this._parent.GetExtendedTypeDescriptor(instance);
			}
			if (this._emptyDescriptor == null)
			{
				this._emptyDescriptor = new TypeDescriptionProvider.EmptyCustomTypeDescriptor();
			}
			return this._emptyDescriptor;
		}

		/// <summary>Gets the extender providers for the specified object.</summary>
		/// <param name="instance">The object to get extender providers for.</param>
		/// <returns>An array of extender providers for <paramref name="instance" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="instance" /> is <see langword="null" />.</exception>
		// Token: 0x06000EC8 RID: 3784 RVA: 0x00039CED File Offset: 0x00037EED
		protected internal virtual IExtenderProvider[] GetExtenderProviders(object instance)
		{
			if (this._parent != null)
			{
				return this._parent.GetExtenderProviders(instance);
			}
			if (instance == null)
			{
				throw new ArgumentNullException("instance");
			}
			return new IExtenderProvider[0];
		}

		/// <summary>Gets the name of the specified component, or <see langword="null" /> if the component has no name.</summary>
		/// <param name="component">The specified component.</param>
		/// <returns>The name of the specified component.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="component" /> is <see langword="null" />.</exception>
		// Token: 0x06000EC9 RID: 3785 RVA: 0x00039D18 File Offset: 0x00037F18
		public virtual string GetFullComponentName(object component)
		{
			if (this._parent != null)
			{
				return this._parent.GetFullComponentName(component);
			}
			return this.GetTypeDescriptor(component).GetComponentName();
		}

		/// <summary>Performs normal reflection against a type.</summary>
		/// <param name="objectType">The type of object for which to retrieve the <see cref="T:System.Reflection.IReflect" />.</param>
		/// <returns>The type of reflection for this <paramref name="objectType" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="objectType" /> is <see langword="null" />.</exception>
		// Token: 0x06000ECA RID: 3786 RVA: 0x00039D3B File Offset: 0x00037F3B
		public Type GetReflectionType(Type objectType)
		{
			return this.GetReflectionType(objectType, null);
		}

		/// <summary>Performs normal reflection against the given object.</summary>
		/// <param name="instance">An instance of the type (should not be <see langword="null" />).</param>
		/// <returns>The type of reflection for this <paramref name="instance" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="instance" /> is <see langword="null" />.</exception>
		// Token: 0x06000ECB RID: 3787 RVA: 0x00039D45 File Offset: 0x00037F45
		public Type GetReflectionType(object instance)
		{
			if (instance == null)
			{
				throw new ArgumentNullException("instance");
			}
			return this.GetReflectionType(instance.GetType(), instance);
		}

		/// <summary>Performs normal reflection against the given object with the given type.</summary>
		/// <param name="objectType">The type of object for which to retrieve the <see cref="T:System.Reflection.IReflect" />.</param>
		/// <param name="instance">An instance of the type. Can be <see langword="null" />.</param>
		/// <returns>The type of reflection for this <paramref name="objectType" />.</returns>
		// Token: 0x06000ECC RID: 3788 RVA: 0x00039D62 File Offset: 0x00037F62
		public virtual Type GetReflectionType(Type objectType, object instance)
		{
			if (this._parent != null)
			{
				return this._parent.GetReflectionType(objectType, instance);
			}
			return objectType;
		}

		/// <summary>Converts a reflection type into a runtime type.</summary>
		/// <param name="reflectionType">The type to convert to its runtime equivalent.</param>
		/// <returns>A <see cref="T:System.Type" /> that represents the runtime equivalent of <paramref name="reflectionType" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="reflectionType" /> is <see langword="null" />.</exception>
		// Token: 0x06000ECD RID: 3789 RVA: 0x00039D7C File Offset: 0x00037F7C
		public virtual Type GetRuntimeType(Type reflectionType)
		{
			if (this._parent != null)
			{
				return this._parent.GetRuntimeType(reflectionType);
			}
			if (reflectionType == null)
			{
				throw new ArgumentNullException("reflectionType");
			}
			if (reflectionType.GetType().Assembly == typeof(object).Assembly)
			{
				return reflectionType;
			}
			return reflectionType.UnderlyingSystemType;
		}

		/// <summary>Gets a custom type descriptor for the given type.</summary>
		/// <param name="objectType">The type of object for which to retrieve the type descriptor.</param>
		/// <returns>An <see cref="T:System.ComponentModel.ICustomTypeDescriptor" /> that can provide metadata for the type.</returns>
		// Token: 0x06000ECE RID: 3790 RVA: 0x00039DDB File Offset: 0x00037FDB
		public ICustomTypeDescriptor GetTypeDescriptor(Type objectType)
		{
			return this.GetTypeDescriptor(objectType, null);
		}

		/// <summary>Gets a custom type descriptor for the given object.</summary>
		/// <param name="instance">An instance of the type. Can be <see langword="null" /> if no instance was passed to the <see cref="T:System.ComponentModel.TypeDescriptor" />.</param>
		/// <returns>An <see cref="T:System.ComponentModel.ICustomTypeDescriptor" /> that can provide metadata for the type.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="instance" /> is <see langword="null" />.</exception>
		// Token: 0x06000ECF RID: 3791 RVA: 0x00039DE5 File Offset: 0x00037FE5
		public ICustomTypeDescriptor GetTypeDescriptor(object instance)
		{
			if (instance == null)
			{
				throw new ArgumentNullException("instance");
			}
			return this.GetTypeDescriptor(instance.GetType(), instance);
		}

		/// <summary>Gets a custom type descriptor for the given type and object.</summary>
		/// <param name="objectType">The type of object for which to retrieve the type descriptor.</param>
		/// <param name="instance">An instance of the type. Can be <see langword="null" /> if no instance was passed to the <see cref="T:System.ComponentModel.TypeDescriptor" />.</param>
		/// <returns>An <see cref="T:System.ComponentModel.ICustomTypeDescriptor" /> that can provide metadata for the type.</returns>
		// Token: 0x06000ED0 RID: 3792 RVA: 0x00039E02 File Offset: 0x00038002
		public virtual ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
		{
			if (this._parent != null)
			{
				return this._parent.GetTypeDescriptor(objectType, instance);
			}
			if (this._emptyDescriptor == null)
			{
				this._emptyDescriptor = new TypeDescriptionProvider.EmptyCustomTypeDescriptor();
			}
			return this._emptyDescriptor;
		}

		/// <summary>Gets a value that indicates whether the specified type is compatible with the type description and its chain of type description providers. </summary>
		/// <param name="type">The type to test for compatibility.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="type" /> is compatible with the type description and its chain of type description providers; otherwise, <see langword="false" />. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> is <see langword="null" />.</exception>
		// Token: 0x06000ED1 RID: 3793 RVA: 0x00039E33 File Offset: 0x00038033
		public virtual bool IsSupportedType(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			return this._parent == null || this._parent.IsSupportedType(type);
		}

		// Token: 0x04000DF6 RID: 3574
		private TypeDescriptionProvider _parent;

		// Token: 0x04000DF7 RID: 3575
		private TypeDescriptionProvider.EmptyCustomTypeDescriptor _emptyDescriptor;

		// Token: 0x020001DA RID: 474
		private sealed class EmptyCustomTypeDescriptor : CustomTypeDescriptor
		{
			// Token: 0x06000ED2 RID: 3794 RVA: 0x00039E5F File Offset: 0x0003805F
			public EmptyCustomTypeDescriptor()
			{
			}
		}
	}
}
