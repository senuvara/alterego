﻿using System;
using System.Collections;
using System.Security.Permissions;

namespace System.ComponentModel
{
	// Token: 0x02000157 RID: 343
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	internal sealed class DelegatingTypeDescriptionProvider : TypeDescriptionProvider
	{
		// Token: 0x06000AB2 RID: 2738 RVA: 0x0002EBD3 File Offset: 0x0002CDD3
		internal DelegatingTypeDescriptionProvider(Type type)
		{
			this._type = type;
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x06000AB3 RID: 2739 RVA: 0x0002EBE2 File Offset: 0x0002CDE2
		internal TypeDescriptionProvider Provider
		{
			get
			{
				return TypeDescriptor.GetProviderRecursive(this._type);
			}
		}

		// Token: 0x06000AB4 RID: 2740 RVA: 0x0002EBEF File Offset: 0x0002CDEF
		public override object CreateInstance(IServiceProvider provider, Type objectType, Type[] argTypes, object[] args)
		{
			return this.Provider.CreateInstance(provider, objectType, argTypes, args);
		}

		// Token: 0x06000AB5 RID: 2741 RVA: 0x0002EC01 File Offset: 0x0002CE01
		public override IDictionary GetCache(object instance)
		{
			return this.Provider.GetCache(instance);
		}

		// Token: 0x06000AB6 RID: 2742 RVA: 0x0002EC0F File Offset: 0x0002CE0F
		public override string GetFullComponentName(object component)
		{
			return this.Provider.GetFullComponentName(component);
		}

		// Token: 0x06000AB7 RID: 2743 RVA: 0x0002EC1D File Offset: 0x0002CE1D
		public override ICustomTypeDescriptor GetExtendedTypeDescriptor(object instance)
		{
			return this.Provider.GetExtendedTypeDescriptor(instance);
		}

		// Token: 0x06000AB8 RID: 2744 RVA: 0x0002EC2B File Offset: 0x0002CE2B
		protected internal override IExtenderProvider[] GetExtenderProviders(object instance)
		{
			return this.Provider.GetExtenderProviders(instance);
		}

		// Token: 0x06000AB9 RID: 2745 RVA: 0x0002EC39 File Offset: 0x0002CE39
		public override Type GetReflectionType(Type objectType, object instance)
		{
			return this.Provider.GetReflectionType(objectType, instance);
		}

		// Token: 0x06000ABA RID: 2746 RVA: 0x0002EC48 File Offset: 0x0002CE48
		public override Type GetRuntimeType(Type objectType)
		{
			return this.Provider.GetRuntimeType(objectType);
		}

		// Token: 0x06000ABB RID: 2747 RVA: 0x0002EC56 File Offset: 0x0002CE56
		public override ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
		{
			return this.Provider.GetTypeDescriptor(objectType, instance);
		}

		// Token: 0x06000ABC RID: 2748 RVA: 0x0002EC65 File Offset: 0x0002CE65
		public override bool IsSupportedType(Type type)
		{
			return this.Provider.IsSupportedType(type);
		}

		// Token: 0x04000CAA RID: 3242
		private Type _type;
	}
}
