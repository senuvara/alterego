﻿using System;

namespace System.ComponentModel
{
	/// <summary>Notifies clients that a property value has changed.</summary>
	// Token: 0x02000181 RID: 385
	public interface INotifyPropertyChanged
	{
		/// <summary>Occurs when a property value changes.</summary>
		// Token: 0x1400000F RID: 15
		// (add) Token: 0x06000BC7 RID: 3015
		// (remove) Token: 0x06000BC8 RID: 3016
		event PropertyChangedEventHandler PropertyChanged;
	}
}
