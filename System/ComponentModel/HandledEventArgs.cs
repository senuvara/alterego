﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides data for events that can be handled completely in an event handler. </summary>
	// Token: 0x0200016F RID: 367
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class HandledEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.HandledEventArgs" /> class with a default <see cref="P:System.ComponentModel.HandledEventArgs.Handled" /> property value of <see langword="false" />.</summary>
		// Token: 0x06000B73 RID: 2931 RVA: 0x0003047E File Offset: 0x0002E67E
		public HandledEventArgs() : this(false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.HandledEventArgs" /> class with the specified default value for the <see cref="P:System.ComponentModel.HandledEventArgs.Handled" /> property.</summary>
		/// <param name="defaultHandledValue">The default value for the <see cref="P:System.ComponentModel.HandledEventArgs.Handled" /> property.</param>
		// Token: 0x06000B74 RID: 2932 RVA: 0x00030487 File Offset: 0x0002E687
		public HandledEventArgs(bool defaultHandledValue)
		{
			this.handled = defaultHandledValue;
		}

		/// <summary>Gets or sets a value that indicates whether the event handler has completely handled the event or whether the system should continue its own processing.</summary>
		/// <returns>
		///     <see langword="true" /> if the event has been completely handled; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000270 RID: 624
		// (get) Token: 0x06000B75 RID: 2933 RVA: 0x00030496 File Offset: 0x0002E696
		// (set) Token: 0x06000B76 RID: 2934 RVA: 0x0003049E File Offset: 0x0002E69E
		public bool Handled
		{
			get
			{
				return this.handled;
			}
			set
			{
				this.handled = value;
			}
		}

		// Token: 0x04000CE7 RID: 3303
		private bool handled;
	}
}
