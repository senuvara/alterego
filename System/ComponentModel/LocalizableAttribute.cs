﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies whether a property should be localized. This class cannot be inherited.</summary>
	// Token: 0x020001A7 RID: 423
	[AttributeUsage(AttributeTargets.All)]
	public sealed class LocalizableAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.LocalizableAttribute" /> class.</summary>
		/// <param name="isLocalizable">
		///       <see langword="true" /> if a property should be localized; otherwise, <see langword="false" />. </param>
		// Token: 0x06000C79 RID: 3193 RVA: 0x0003135C File Offset: 0x0002F55C
		public LocalizableAttribute(bool isLocalizable)
		{
			this.isLocalizable = isLocalizable;
		}

		/// <summary>Gets a value indicating whether a property should be localized.</summary>
		/// <returns>
		///     <see langword="true" /> if a property should be localized; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002B0 RID: 688
		// (get) Token: 0x06000C7A RID: 3194 RVA: 0x0003136B File Offset: 0x0002F56B
		public bool IsLocalizable
		{
			get
			{
				return this.isLocalizable;
			}
		}

		/// <summary>Determines if this attribute is the default.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute is the default value for this attribute class; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000C7B RID: 3195 RVA: 0x00031373 File Offset: 0x0002F573
		public override bool IsDefaultAttribute()
		{
			return this.IsLocalizable == LocalizableAttribute.Default.IsLocalizable;
		}

		/// <summary>Returns whether the value of the given object is equal to the current <see cref="T:System.ComponentModel.LocalizableAttribute" />.</summary>
		/// <param name="obj">The object to test the value equality of. </param>
		/// <returns>
		///     <see langword="true" /> if the value of the given object is equal to that of the current; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000C7C RID: 3196 RVA: 0x00031388 File Offset: 0x0002F588
		public override bool Equals(object obj)
		{
			LocalizableAttribute localizableAttribute = obj as LocalizableAttribute;
			return localizableAttribute != null && localizableAttribute.IsLocalizable == this.isLocalizable;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.LocalizableAttribute" />.</returns>
		// Token: 0x06000C7D RID: 3197 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000C7E RID: 3198 RVA: 0x000313AF File Offset: 0x0002F5AF
		// Note: this type is marked as 'beforefieldinit'.
		static LocalizableAttribute()
		{
		}

		// Token: 0x04000D1F RID: 3359
		private bool isLocalizable;

		/// <summary>Specifies that a property should be localized. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000D20 RID: 3360
		public static readonly LocalizableAttribute Yes = new LocalizableAttribute(true);

		/// <summary>Specifies that a property should not be localized. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000D21 RID: 3361
		public static readonly LocalizableAttribute No = new LocalizableAttribute(false);

		/// <summary>Specifies the default value, which is <see cref="F:System.ComponentModel.LocalizableAttribute.No" />. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000D22 RID: 3362
		public static readonly LocalizableAttribute Default = LocalizableAttribute.No;
	}
}
