﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a base class for the container filter service.</summary>
	// Token: 0x02000146 RID: 326
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public abstract class ContainerFilterService
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ContainerFilterService" /> class.</summary>
		// Token: 0x06000A48 RID: 2632 RVA: 0x0000232F File Offset: 0x0000052F
		protected ContainerFilterService()
		{
		}

		/// <summary>Filters the component collection.</summary>
		/// <param name="components">The component collection to filter.</param>
		/// <returns>A <see cref="T:System.ComponentModel.ComponentCollection" /> that represents a modified collection.</returns>
		// Token: 0x06000A49 RID: 2633 RVA: 0x0000206B File Offset: 0x0000026B
		public virtual ComponentCollection FilterComponents(ComponentCollection components)
		{
			return components;
		}
	}
}
