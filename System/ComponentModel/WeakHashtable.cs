﻿using System;
using System.Collections;
using System.Security.Permissions;

namespace System.ComponentModel
{
	// Token: 0x020001FB RID: 507
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	internal sealed class WeakHashtable : Hashtable
	{
		// Token: 0x06000FFB RID: 4091 RVA: 0x0003EB6C File Offset: 0x0003CD6C
		internal WeakHashtable() : base(WeakHashtable._comparer)
		{
		}

		// Token: 0x06000FFC RID: 4092 RVA: 0x0003EB79 File Offset: 0x0003CD79
		public override void Clear()
		{
			base.Clear();
		}

		// Token: 0x06000FFD RID: 4093 RVA: 0x0003EB81 File Offset: 0x0003CD81
		public override void Remove(object key)
		{
			base.Remove(key);
		}

		// Token: 0x06000FFE RID: 4094 RVA: 0x0003EB8A File Offset: 0x0003CD8A
		public void SetWeak(object key, object value)
		{
			this.ScavengeKeys();
			this[new WeakHashtable.EqualityWeakReference(key)] = value;
		}

		// Token: 0x06000FFF RID: 4095 RVA: 0x0003EBA0 File Offset: 0x0003CDA0
		private void ScavengeKeys()
		{
			int count = this.Count;
			if (count == 0)
			{
				return;
			}
			if (this._lastHashCount == 0)
			{
				this._lastHashCount = count;
				return;
			}
			long totalMemory = GC.GetTotalMemory(false);
			if (this._lastGlobalMem == 0L)
			{
				this._lastGlobalMem = totalMemory;
				return;
			}
			float num = (float)(totalMemory - this._lastGlobalMem) / (float)this._lastGlobalMem;
			float num2 = (float)(count - this._lastHashCount) / (float)this._lastHashCount;
			if (num < 0f && num2 >= 0f)
			{
				ArrayList arrayList = null;
				foreach (object obj in this.Keys)
				{
					WeakReference weakReference = obj as WeakReference;
					if (weakReference != null && !weakReference.IsAlive)
					{
						if (arrayList == null)
						{
							arrayList = new ArrayList();
						}
						arrayList.Add(weakReference);
					}
				}
				if (arrayList != null)
				{
					foreach (object key in arrayList)
					{
						this.Remove(key);
					}
				}
			}
			this._lastGlobalMem = totalMemory;
			this._lastHashCount = count;
		}

		// Token: 0x06001000 RID: 4096 RVA: 0x0003ECE0 File Offset: 0x0003CEE0
		// Note: this type is marked as 'beforefieldinit'.
		static WeakHashtable()
		{
		}

		// Token: 0x04000E46 RID: 3654
		private static IEqualityComparer _comparer = new WeakHashtable.WeakKeyComparer();

		// Token: 0x04000E47 RID: 3655
		private long _lastGlobalMem;

		// Token: 0x04000E48 RID: 3656
		private int _lastHashCount;

		// Token: 0x020001FC RID: 508
		private class WeakKeyComparer : IEqualityComparer
		{
			// Token: 0x06001001 RID: 4097 RVA: 0x0003ECEC File Offset: 0x0003CEEC
			bool IEqualityComparer.Equals(object x, object y)
			{
				if (x == null)
				{
					return y == null;
				}
				if (y != null && x.GetHashCode() == y.GetHashCode())
				{
					WeakReference weakReference = x as WeakReference;
					WeakReference weakReference2 = y as WeakReference;
					if (weakReference != null)
					{
						if (!weakReference.IsAlive)
						{
							return false;
						}
						x = weakReference.Target;
					}
					if (weakReference2 != null)
					{
						if (!weakReference2.IsAlive)
						{
							return false;
						}
						y = weakReference2.Target;
					}
					return x == y;
				}
				return false;
			}

			// Token: 0x06001002 RID: 4098 RVA: 0x0003ED50 File Offset: 0x0003CF50
			int IEqualityComparer.GetHashCode(object obj)
			{
				return obj.GetHashCode();
			}

			// Token: 0x06001003 RID: 4099 RVA: 0x0000232F File Offset: 0x0000052F
			public WeakKeyComparer()
			{
			}
		}

		// Token: 0x020001FD RID: 509
		private sealed class EqualityWeakReference : WeakReference
		{
			// Token: 0x06001004 RID: 4100 RVA: 0x0003ED58 File Offset: 0x0003CF58
			internal EqualityWeakReference(object o) : base(o)
			{
				this._hashCode = o.GetHashCode();
			}

			// Token: 0x06001005 RID: 4101 RVA: 0x0003ED6D File Offset: 0x0003CF6D
			public override bool Equals(object o)
			{
				return o != null && o.GetHashCode() == this._hashCode && (o == this || (this.IsAlive && o == this.Target));
			}

			// Token: 0x06001006 RID: 4102 RVA: 0x0003ED9C File Offset: 0x0003CF9C
			public override int GetHashCode()
			{
				return this._hashCode;
			}

			// Token: 0x04000E49 RID: 3657
			private int _hashCode;
		}
	}
}
