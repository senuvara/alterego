﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies a description for a property or event.</summary>
	// Token: 0x02000158 RID: 344
	[AttributeUsage(AttributeTargets.All)]
	public class DescriptionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DescriptionAttribute" /> class with no parameters.</summary>
		// Token: 0x06000ABD RID: 2749 RVA: 0x0002EC73 File Offset: 0x0002CE73
		public DescriptionAttribute() : this(string.Empty)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DescriptionAttribute" /> class with a description.</summary>
		/// <param name="description">The description text. </param>
		// Token: 0x06000ABE RID: 2750 RVA: 0x0002EC80 File Offset: 0x0002CE80
		public DescriptionAttribute(string description)
		{
			this.description = description;
		}

		/// <summary>Gets the description stored in this attribute.</summary>
		/// <returns>The description stored in this attribute.</returns>
		// Token: 0x17000245 RID: 581
		// (get) Token: 0x06000ABF RID: 2751 RVA: 0x0002EC8F File Offset: 0x0002CE8F
		public virtual string Description
		{
			get
			{
				return this.DescriptionValue;
			}
		}

		/// <summary>Gets or sets the string stored as the description.</summary>
		/// <returns>The string stored as the description. The default value is an empty string ("").</returns>
		// Token: 0x17000246 RID: 582
		// (get) Token: 0x06000AC0 RID: 2752 RVA: 0x0002EC97 File Offset: 0x0002CE97
		// (set) Token: 0x06000AC1 RID: 2753 RVA: 0x0002EC9F File Offset: 0x0002CE9F
		protected string DescriptionValue
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		/// <summary>Returns whether the value of the given object is equal to the current <see cref="T:System.ComponentModel.DescriptionAttribute" />.</summary>
		/// <param name="obj">The object to test the value equality of. </param>
		/// <returns>
		///     <see langword="true" /> if the value of the given object is equal to that of the current; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AC2 RID: 2754 RVA: 0x0002ECA8 File Offset: 0x0002CEA8
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			DescriptionAttribute descriptionAttribute = obj as DescriptionAttribute;
			return descriptionAttribute != null && descriptionAttribute.Description == this.Description;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000AC3 RID: 2755 RVA: 0x0002ECD8 File Offset: 0x0002CED8
		public override int GetHashCode()
		{
			return this.Description.GetHashCode();
		}

		/// <summary>Returns a value indicating whether this is the default <see cref="T:System.ComponentModel.DescriptionAttribute" /> instance.</summary>
		/// <returns>
		///     <see langword="true" />, if this is the default <see cref="T:System.ComponentModel.DescriptionAttribute" /> instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AC4 RID: 2756 RVA: 0x0002ECE5 File Offset: 0x0002CEE5
		public override bool IsDefaultAttribute()
		{
			return this.Equals(DescriptionAttribute.Default);
		}

		// Token: 0x06000AC5 RID: 2757 RVA: 0x0002ECF2 File Offset: 0x0002CEF2
		// Note: this type is marked as 'beforefieldinit'.
		static DescriptionAttribute()
		{
		}

		/// <summary>Specifies the default value for the <see cref="T:System.ComponentModel.DescriptionAttribute" />, which is an empty string (""). This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000CAB RID: 3243
		public static readonly DescriptionAttribute Default = new DescriptionAttribute();

		// Token: 0x04000CAC RID: 3244
		private string description;
	}
}
