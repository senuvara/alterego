﻿using System;

namespace System.ComponentModel
{
	/// <summary>Indicates whether the name of the associated property is displayed with parentheses in the Properties window. This class cannot be inherited.</summary>
	// Token: 0x020001F5 RID: 501
	[AttributeUsage(AttributeTargets.All)]
	public sealed class ParenthesizePropertyNameAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ParenthesizePropertyNameAttribute" /> class that indicates that the associated property should not be shown with parentheses.</summary>
		// Token: 0x06000FD7 RID: 4055 RVA: 0x0003E5CD File Offset: 0x0003C7CD
		public ParenthesizePropertyNameAttribute() : this(false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ParenthesizePropertyNameAttribute" /> class, using the specified value to indicate whether the attribute is displayed with parentheses.</summary>
		/// <param name="needParenthesis">
		///       <see langword="true" /> if the name should be enclosed in parentheses; otherwise, <see langword="false" />. </param>
		// Token: 0x06000FD8 RID: 4056 RVA: 0x0003E5D6 File Offset: 0x0003C7D6
		public ParenthesizePropertyNameAttribute(bool needParenthesis)
		{
			this.needParenthesis = needParenthesis;
		}

		/// <summary>Gets a value indicating whether the Properties window displays the name of the property in parentheses in the Properties window.</summary>
		/// <returns>
		///     <see langword="true" /> if the property is displayed with parentheses; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000340 RID: 832
		// (get) Token: 0x06000FD9 RID: 4057 RVA: 0x0003E5E5 File Offset: 0x0003C7E5
		public bool NeedParenthesis
		{
			get
			{
				return this.needParenthesis;
			}
		}

		/// <summary>Compares the specified object to this object and tests for equality.</summary>
		/// <param name="o">The object to be compared. </param>
		/// <returns>
		///     <see langword="true" /> if equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000FDA RID: 4058 RVA: 0x0003E5ED File Offset: 0x0003C7ED
		public override bool Equals(object o)
		{
			return o is ParenthesizePropertyNameAttribute && ((ParenthesizePropertyNameAttribute)o).NeedParenthesis == this.needParenthesis;
		}

		/// <summary>Gets the hash code for this object.</summary>
		/// <returns>The hash code for the object the attribute belongs to.</returns>
		// Token: 0x06000FDB RID: 4059 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Gets a value indicating whether the current value of the attribute is the default value for the attribute.</summary>
		/// <returns>
		///     <see langword="true" /> if the current value of the attribute is the default value of the attribute; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000FDC RID: 4060 RVA: 0x0003E60C File Offset: 0x0003C80C
		public override bool IsDefaultAttribute()
		{
			return this.Equals(ParenthesizePropertyNameAttribute.Default);
		}

		// Token: 0x06000FDD RID: 4061 RVA: 0x0003E619 File Offset: 0x0003C819
		// Note: this type is marked as 'beforefieldinit'.
		static ParenthesizePropertyNameAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ParenthesizePropertyNameAttribute" /> class with a default value that indicates that the associated property should not be shown with parentheses. This field is read-only.</summary>
		// Token: 0x04000E30 RID: 3632
		public static readonly ParenthesizePropertyNameAttribute Default = new ParenthesizePropertyNameAttribute();

		// Token: 0x04000E31 RID: 3633
		private bool needParenthesis;
	}
}
