﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides data for the <see cref="E:System.ComponentModel.TypeDescriptor.Refreshed" /> event.</summary>
	// Token: 0x020001C8 RID: 456
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class RefreshEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.RefreshEventArgs" /> class with the component that has changed.</summary>
		/// <param name="componentChanged">The component that changed. </param>
		// Token: 0x06000E46 RID: 3654 RVA: 0x00039242 File Offset: 0x00037442
		public RefreshEventArgs(object componentChanged)
		{
			this.componentChanged = componentChanged;
			this.typeChanged = componentChanged.GetType();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.RefreshEventArgs" /> class with the type of component that has changed.</summary>
		/// <param name="typeChanged">The <see cref="T:System.Type" /> that changed. </param>
		// Token: 0x06000E47 RID: 3655 RVA: 0x0003925D File Offset: 0x0003745D
		public RefreshEventArgs(Type typeChanged)
		{
			this.typeChanged = typeChanged;
		}

		/// <summary>Gets the component that changed its properties, events, or extenders.</summary>
		/// <returns>The component that changed its properties, events, or extenders, or <see langword="null" /> if all components of the same type have changed.</returns>
		// Token: 0x1700031A RID: 794
		// (get) Token: 0x06000E48 RID: 3656 RVA: 0x0003926C File Offset: 0x0003746C
		public object ComponentChanged
		{
			get
			{
				return this.componentChanged;
			}
		}

		/// <summary>Gets the <see cref="T:System.Type" /> that changed its properties or events.</summary>
		/// <returns>The <see cref="T:System.Type" /> that changed its properties or events.</returns>
		// Token: 0x1700031B RID: 795
		// (get) Token: 0x06000E49 RID: 3657 RVA: 0x00039274 File Offset: 0x00037474
		public Type TypeChanged
		{
			get
			{
				return this.typeChanged;
			}
		}

		// Token: 0x04000DDC RID: 3548
		private object componentChanged;

		// Token: 0x04000DDD RID: 3549
		private Type typeChanged;
	}
}
