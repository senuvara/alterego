﻿using System;
using System.ComponentModel.Design;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	/// <summary>Implements <see cref="T:System.ComponentModel.IComponent" /> and provides the base implementation for remotable components that are marshaled by value (a copy of the serialized object is passed).</summary>
	// Token: 0x020001A9 RID: 425
	[TypeConverter(typeof(ComponentConverter))]
	[DesignerCategory("Component")]
	[Designer("System.Windows.Forms.Design.ComponentDocumentDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(IRootDesigner))]
	[ComVisible(true)]
	public class MarshalByValueComponent : IComponent, IDisposable, IServiceProvider
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.MarshalByValueComponent" /> class.</summary>
		// Token: 0x06000C88 RID: 3208 RVA: 0x0000232F File Offset: 0x0000052F
		public MarshalByValueComponent()
		{
		}

		/// <summary>Allows an object to try to free resources and perform other cleanup operations before it is reclaimed by garbage collection.</summary>
		// Token: 0x06000C89 RID: 3209 RVA: 0x000314AC File Offset: 0x0002F6AC
		~MarshalByValueComponent()
		{
			this.Dispose(false);
		}

		/// <summary>Adds an event handler to listen to the <see cref="E:System.ComponentModel.MarshalByValueComponent.Disposed" /> event on the component.</summary>
		// Token: 0x14000012 RID: 18
		// (add) Token: 0x06000C8A RID: 3210 RVA: 0x000314DC File Offset: 0x0002F6DC
		// (remove) Token: 0x06000C8B RID: 3211 RVA: 0x000314EF File Offset: 0x0002F6EF
		public event EventHandler Disposed
		{
			add
			{
				this.Events.AddHandler(MarshalByValueComponent.EventDisposed, value);
			}
			remove
			{
				this.Events.RemoveHandler(MarshalByValueComponent.EventDisposed, value);
			}
		}

		/// <summary>Gets the list of event handlers that are attached to this component.</summary>
		/// <returns>An <see cref="T:System.ComponentModel.EventHandlerList" /> that provides the delegates for this component.</returns>
		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x06000C8C RID: 3212 RVA: 0x00031502 File Offset: 0x0002F702
		protected EventHandlerList Events
		{
			get
			{
				if (this.events == null)
				{
					this.events = new EventHandlerList();
				}
				return this.events;
			}
		}

		/// <summary>Gets or sets the site of the component.</summary>
		/// <returns>An object implementing the <see cref="T:System.ComponentModel.ISite" /> interface that represents the site of the component.</returns>
		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x06000C8D RID: 3213 RVA: 0x0003151D File Offset: 0x0002F71D
		// (set) Token: 0x06000C8E RID: 3214 RVA: 0x00031525 File Offset: 0x0002F725
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public virtual ISite Site
		{
			get
			{
				return this.site;
			}
			set
			{
				this.site = value;
			}
		}

		/// <summary>Releases all resources used by the <see cref="T:System.ComponentModel.MarshalByValueComponent" />.</summary>
		// Token: 0x06000C8F RID: 3215 RVA: 0x0003152E File Offset: 0x0002F72E
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.MarshalByValueComponent" /> and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06000C90 RID: 3216 RVA: 0x00031540 File Offset: 0x0002F740
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				lock (this)
				{
					if (this.site != null && this.site.Container != null)
					{
						this.site.Container.Remove(this);
					}
					if (this.events != null)
					{
						EventHandler eventHandler = (EventHandler)this.events[MarshalByValueComponent.EventDisposed];
						if (eventHandler != null)
						{
							eventHandler(this, EventArgs.Empty);
						}
					}
				}
			}
		}

		/// <summary>Gets the container for the component.</summary>
		/// <returns>An object implementing the <see cref="T:System.ComponentModel.IContainer" /> interface that represents the component's container, or <see langword="null" /> if the component does not have a site.</returns>
		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x06000C91 RID: 3217 RVA: 0x000315CC File Offset: 0x0002F7CC
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public virtual IContainer Container
		{
			get
			{
				ISite site = this.site;
				if (site != null)
				{
					return site.Container;
				}
				return null;
			}
		}

		/// <summary>Gets the implementer of the <see cref="T:System.IServiceProvider" />.</summary>
		/// <param name="service">A <see cref="T:System.Type" /> that represents the type of service you want. </param>
		/// <returns>An <see cref="T:System.Object" /> that represents the implementer of the <see cref="T:System.IServiceProvider" />.</returns>
		// Token: 0x06000C92 RID: 3218 RVA: 0x000315EB File Offset: 0x0002F7EB
		public virtual object GetService(Type service)
		{
			if (this.site != null)
			{
				return this.site.GetService(service);
			}
			return null;
		}

		/// <summary>Gets a value indicating whether the component is currently in design mode.</summary>
		/// <returns>
		///     <see langword="true" /> if the component is in design mode; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002B8 RID: 696
		// (get) Token: 0x06000C93 RID: 3219 RVA: 0x00031604 File Offset: 0x0002F804
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public virtual bool DesignMode
		{
			get
			{
				ISite site = this.site;
				return site != null && site.DesignMode;
			}
		}

		/// <summary>Returns a <see cref="T:System.String" /> containing the name of the <see cref="T:System.ComponentModel.Component" />, if any. This method should not be overridden.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the name of the <see cref="T:System.ComponentModel.Component" />, if any.
		///     <see langword="null" /> if the <see cref="T:System.ComponentModel.Component" /> is unnamed.</returns>
		// Token: 0x06000C94 RID: 3220 RVA: 0x00031624 File Offset: 0x0002F824
		public override string ToString()
		{
			ISite site = this.site;
			if (site != null)
			{
				return site.Name + " [" + base.GetType().FullName + "]";
			}
			return base.GetType().FullName;
		}

		// Token: 0x06000C95 RID: 3221 RVA: 0x00031667 File Offset: 0x0002F867
		// Note: this type is marked as 'beforefieldinit'.
		static MarshalByValueComponent()
		{
		}

		// Token: 0x04000D28 RID: 3368
		private static readonly object EventDisposed = new object();

		// Token: 0x04000D29 RID: 3369
		private ISite site;

		// Token: 0x04000D2A RID: 3370
		private EventHandlerList events;
	}
}
