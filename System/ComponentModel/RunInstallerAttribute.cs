﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies whether the Visual Studio Custom Action Installer or the Installutil.exe (Installer Tool) should be invoked when the assembly is installed.</summary>
	// Token: 0x020001CA RID: 458
	[AttributeUsage(AttributeTargets.Class)]
	public class RunInstallerAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.RunInstallerAttribute" /> class.</summary>
		/// <param name="runInstaller">
		///       <see langword="true" /> if an installer should be invoked during installation of an assembly; otherwise, <see langword="false" />. </param>
		// Token: 0x06000E4E RID: 3662 RVA: 0x0003927C File Offset: 0x0003747C
		public RunInstallerAttribute(bool runInstaller)
		{
			this.runInstaller = runInstaller;
		}

		/// <summary>Gets a value indicating whether an installer should be invoked during installation of an assembly.</summary>
		/// <returns>
		///     <see langword="true" /> if an installer should be invoked during installation of an assembly; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700031C RID: 796
		// (get) Token: 0x06000E4F RID: 3663 RVA: 0x0003928B File Offset: 0x0003748B
		public bool RunInstaller
		{
			get
			{
				return this.runInstaller;
			}
		}

		/// <summary>Determines whether the value of the specified <see cref="T:System.ComponentModel.RunInstallerAttribute" /> is equivalent to the current <see cref="T:System.ComponentModel.RunInstallerAttribute" />.</summary>
		/// <param name="obj">The object to compare.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.ComponentModel.RunInstallerAttribute" /> is equal to the current <see cref="T:System.ComponentModel.RunInstallerAttribute" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000E50 RID: 3664 RVA: 0x00039294 File Offset: 0x00037494
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			RunInstallerAttribute runInstallerAttribute = obj as RunInstallerAttribute;
			return runInstallerAttribute != null && runInstallerAttribute.RunInstaller == this.runInstaller;
		}

		/// <summary>Generates a hash code for the current <see cref="T:System.ComponentModel.RunInstallerAttribute" />.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.RunInstallerAttribute" />.</returns>
		// Token: 0x06000E51 RID: 3665 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Determines if this attribute is the default.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute is the default value for this attribute class; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000E52 RID: 3666 RVA: 0x000392C1 File Offset: 0x000374C1
		public override bool IsDefaultAttribute()
		{
			return this.Equals(RunInstallerAttribute.Default);
		}

		// Token: 0x06000E53 RID: 3667 RVA: 0x000392CE File Offset: 0x000374CE
		// Note: this type is marked as 'beforefieldinit'.
		static RunInstallerAttribute()
		{
		}

		// Token: 0x04000DDE RID: 3550
		private bool runInstaller;

		/// <summary>Specifies that the Visual Studio Custom Action Installer or the Installutil.exe (Installer Tool) should be invoked when the assembly is installed. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000DDF RID: 3551
		public static readonly RunInstallerAttribute Yes = new RunInstallerAttribute(true);

		/// <summary>Specifies that the Visual Studio Custom Action Installer or the Installutil.exe (Installer Tool) should not be invoked when the assembly is installed. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000DE0 RID: 3552
		public static readonly RunInstallerAttribute No = new RunInstallerAttribute(false);

		/// <summary>Specifies the default visiblity, which is <see cref="F:System.ComponentModel.RunInstallerAttribute.No" />. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000DE1 RID: 3553
		public static readonly RunInstallerAttribute Default = RunInstallerAttribute.No;
	}
}
