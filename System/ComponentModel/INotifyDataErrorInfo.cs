﻿using System;
using System.Collections;

namespace System.ComponentModel
{
	/// <summary>Defines members that data entity classes can implement to provide custom synchronous and asynchronous validation support.</summary>
	// Token: 0x02000180 RID: 384
	public interface INotifyDataErrorInfo
	{
		/// <summary>Gets a value that indicates whether the entity has validation errors. </summary>
		/// <returns>
		///     <see langword="true" /> if the entity currently has validation errors; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000287 RID: 647
		// (get) Token: 0x06000BC3 RID: 3011
		bool HasErrors { get; }

		/// <summary>Gets the validation errors for a specified property or for the entire entity.</summary>
		/// <param name="propertyName">The name of the property to retrieve validation errors for; or <see langword="null" /> or <see cref="F:System.String.Empty" />, to retrieve entity-level errors.</param>
		/// <returns>The validation errors for the property or entity.</returns>
		// Token: 0x06000BC4 RID: 3012
		IEnumerable GetErrors(string propertyName);

		/// <summary>Occurs when the validation errors have changed for a property or for the entire entity. </summary>
		// Token: 0x1400000E RID: 14
		// (add) Token: 0x06000BC5 RID: 3013
		// (remove) Token: 0x06000BC6 RID: 3014
		event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
	}
}
