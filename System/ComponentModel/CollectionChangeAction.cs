﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies how the collection is changed.</summary>
	// Token: 0x0200013A RID: 314
	public enum CollectionChangeAction
	{
		/// <summary>Specifies that an element was added to the collection.</summary>
		// Token: 0x04000C76 RID: 3190
		Add = 1,
		/// <summary>Specifies that an element was removed from the collection.</summary>
		// Token: 0x04000C77 RID: 3191
		Remove,
		/// <summary>Specifies that the entire collection has changed. This is caused by using methods that manipulate the entire collection, such as <see cref="M:System.Collections.CollectionBase.Clear" />.</summary>
		// Token: 0x04000C78 RID: 3192
		Refresh
	}
}
