﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a type converter to convert 16-bit unsigned integer objects to and from other representations.</summary>
	// Token: 0x020001EC RID: 492
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class UInt16Converter : BaseNumberConverter
	{
		// Token: 0x17000336 RID: 822
		// (get) Token: 0x06000F97 RID: 3991 RVA: 0x0003D971 File Offset: 0x0003BB71
		internal override Type TargetType
		{
			get
			{
				return typeof(ushort);
			}
		}

		// Token: 0x06000F98 RID: 3992 RVA: 0x0003D97D File Offset: 0x0003BB7D
		internal override object FromString(string value, int radix)
		{
			return Convert.ToUInt16(value, radix);
		}

		// Token: 0x06000F99 RID: 3993 RVA: 0x0003D98B File Offset: 0x0003BB8B
		internal override object FromString(string value, NumberFormatInfo formatInfo)
		{
			return ushort.Parse(value, NumberStyles.Integer, formatInfo);
		}

		// Token: 0x06000F9A RID: 3994 RVA: 0x0003D99A File Offset: 0x0003BB9A
		internal override object FromString(string value, CultureInfo culture)
		{
			return ushort.Parse(value, culture);
		}

		// Token: 0x06000F9B RID: 3995 RVA: 0x0003D9A8 File Offset: 0x0003BBA8
		internal override string ToString(object value, NumberFormatInfo formatInfo)
		{
			return ((ushort)value).ToString("G", formatInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.UInt16Converter" /> class. </summary>
		// Token: 0x06000F9C RID: 3996 RVA: 0x0002B691 File Offset: 0x00029891
		public UInt16Converter()
		{
		}
	}
}
