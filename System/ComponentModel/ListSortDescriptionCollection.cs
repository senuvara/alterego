﻿using System;
using System.Collections;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Represents a collection of <see cref="T:System.ComponentModel.ListSortDescription" /> objects.</summary>
	// Token: 0x020001A5 RID: 421
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class ListSortDescriptionCollection : IList, ICollection, IEnumerable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ListSortDescriptionCollection" /> class. </summary>
		// Token: 0x06000C65 RID: 3173 RVA: 0x00031298 File Offset: 0x0002F498
		public ListSortDescriptionCollection()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.ListSortDescriptionCollection" /> class with the specified array of <see cref="T:System.ComponentModel.ListSortDescription" /> objects.</summary>
		/// <param name="sorts">The array of <see cref="T:System.ComponentModel.ListSortDescription" /> objects to be contained in the collection.</param>
		// Token: 0x06000C66 RID: 3174 RVA: 0x000312AC File Offset: 0x0002F4AC
		public ListSortDescriptionCollection(ListSortDescription[] sorts)
		{
			if (sorts != null)
			{
				for (int i = 0; i < sorts.Length; i++)
				{
					this.sorts.Add(sorts[i]);
				}
			}
		}

		/// <summary>Gets or sets the specified <see cref="T:System.ComponentModel.ListSortDescription" />.</summary>
		/// <param name="index">The zero-based index of the <see cref="T:System.ComponentModel.ListSortDescription" />  to get or set in the collection. </param>
		/// <returns>The <see cref="T:System.ComponentModel.ListSortDescription" /> with the specified index.</returns>
		/// <exception cref="T:System.InvalidOperationException">An item is set in the <see cref="T:System.ComponentModel.ListSortDescriptionCollection" />, which is read-only.</exception>
		// Token: 0x170002A9 RID: 681
		public ListSortDescription this[int index]
		{
			get
			{
				return (ListSortDescription)this.sorts[index];
			}
			set
			{
				throw new InvalidOperationException(SR.GetString("Once a ListSortDescriptionCollection has been created it can't be modified."));
			}
		}

		/// <summary>Gets a value indicating whether the collection has a fixed size.</summary>
		/// <returns>
		///     <see langword="true" /> in all cases.</returns>
		// Token: 0x170002AA RID: 682
		// (get) Token: 0x06000C69 RID: 3177 RVA: 0x00003298 File Offset: 0x00001498
		bool IList.IsFixedSize
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets a value indicating whether the collection is read-only.</summary>
		/// <returns>
		///     <see langword="true " />in all cases.</returns>
		// Token: 0x170002AB RID: 683
		// (get) Token: 0x06000C6A RID: 3178 RVA: 0x00003298 File Offset: 0x00001498
		bool IList.IsReadOnly
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets the specified <see cref="T:System.ComponentModel.ListSortDescription" />.</summary>
		/// <param name="index">The zero-based index of the <see cref="T:System.ComponentModel.ListSortDescription" />  to get in the collection </param>
		/// <returns>The <see cref="T:System.ComponentModel.ListSortDescription" /> with the specified index.</returns>
		// Token: 0x170002AC RID: 684
		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				throw new InvalidOperationException(SR.GetString("Once a ListSortDescriptionCollection has been created it can't be modified."));
			}
		}

		/// <summary>Adds an item to the collection.</summary>
		/// <param name="value">The item to add to the collection.</param>
		/// <returns>The position into which the new element was inserted.</returns>
		/// <exception cref="T:System.InvalidOperationException">In all cases.</exception>
		// Token: 0x06000C6D RID: 3181 RVA: 0x000312FD File Offset: 0x0002F4FD
		int IList.Add(object value)
		{
			throw new InvalidOperationException(SR.GetString("Once a ListSortDescriptionCollection has been created it can't be modified."));
		}

		/// <summary>Removes all items from the collection.</summary>
		/// <exception cref="T:System.InvalidOperationException">In all cases.</exception>
		// Token: 0x06000C6E RID: 3182 RVA: 0x000312FD File Offset: 0x0002F4FD
		void IList.Clear()
		{
			throw new InvalidOperationException(SR.GetString("Once a ListSortDescriptionCollection has been created it can't be modified."));
		}

		/// <summary>Determines if the <see cref="T:System.ComponentModel.ListSortDescriptionCollection" /> contains a specific value.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the collection.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Object" /> is found in the collection; otherwise, <see langword="false" />. </returns>
		// Token: 0x06000C6F RID: 3183 RVA: 0x00031317 File Offset: 0x0002F517
		public bool Contains(object value)
		{
			return ((IList)this.sorts).Contains(value);
		}

		/// <summary>Returns the index of the specified item in the collection.</summary>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the collection.</param>
		/// <returns>The index of <paramref name="value" /> if found in the list; otherwise, -1.</returns>
		// Token: 0x06000C70 RID: 3184 RVA: 0x00031325 File Offset: 0x0002F525
		public int IndexOf(object value)
		{
			return ((IList)this.sorts).IndexOf(value);
		}

		/// <summary>Inserts an item into the collection at a specified index.</summary>
		/// <param name="index">The zero-based index of the <see cref="T:System.ComponentModel.ListSortDescription" />  to get or set in the collection</param>
		/// <param name="value">The item to insert into the collection.</param>
		/// <exception cref="T:System.InvalidOperationException">In all cases.</exception>
		// Token: 0x06000C71 RID: 3185 RVA: 0x000312FD File Offset: 0x0002F4FD
		void IList.Insert(int index, object value)
		{
			throw new InvalidOperationException(SR.GetString("Once a ListSortDescriptionCollection has been created it can't be modified."));
		}

		/// <summary>Removes the first occurrence of an item from the collection.</summary>
		/// <param name="value">The item to remove from the collection.</param>
		/// <exception cref="T:System.InvalidOperationException">In all cases.</exception>
		// Token: 0x06000C72 RID: 3186 RVA: 0x000312FD File Offset: 0x0002F4FD
		void IList.Remove(object value)
		{
			throw new InvalidOperationException(SR.GetString("Once a ListSortDescriptionCollection has been created it can't be modified."));
		}

		/// <summary>Removes an item from the collection at a specified index.</summary>
		/// <param name="index">The zero-based index of the <see cref="T:System.ComponentModel.ListSortDescription" />  to remove from the collection</param>
		/// <exception cref="T:System.InvalidOperationException">In all cases.</exception>
		// Token: 0x06000C73 RID: 3187 RVA: 0x000312FD File Offset: 0x0002F4FD
		void IList.RemoveAt(int index)
		{
			throw new InvalidOperationException(SR.GetString("Once a ListSortDescriptionCollection has been created it can't be modified."));
		}

		/// <summary>Gets the number of items in the collection.</summary>
		/// <returns>The number of items in the collection.</returns>
		// Token: 0x170002AD RID: 685
		// (get) Token: 0x06000C74 RID: 3188 RVA: 0x00031333 File Offset: 0x0002F533
		public int Count
		{
			get
			{
				return this.sorts.Count;
			}
		}

		/// <summary>Gets a value indicating whether access to the collection is thread safe.</summary>
		/// <returns>
		///     <see langword="true " />in all cases.</returns>
		// Token: 0x170002AE RID: 686
		// (get) Token: 0x06000C75 RID: 3189 RVA: 0x00003298 File Offset: 0x00001498
		bool ICollection.IsSynchronized
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets the current instance that can be used to synchronize access to the collection.</summary>
		/// <returns>The current instance of the <see cref="T:System.ComponentModel.ListSortDescriptionCollection" />.</returns>
		// Token: 0x170002AF RID: 687
		// (get) Token: 0x06000C76 RID: 3190 RVA: 0x00002068 File Offset: 0x00000268
		object ICollection.SyncRoot
		{
			get
			{
				return this;
			}
		}

		/// <summary>Copies the contents of the collection to the specified array, starting at the specified destination array index.</summary>
		/// <param name="array">The destination array for the items copied from the collection.</param>
		/// <param name="index">The index of the destination array at which copying begins.</param>
		// Token: 0x06000C77 RID: 3191 RVA: 0x00031340 File Offset: 0x0002F540
		public void CopyTo(Array array, int index)
		{
			this.sorts.CopyTo(array, index);
		}

		/// <summary>Gets a <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through the collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through the collection.</returns>
		// Token: 0x06000C78 RID: 3192 RVA: 0x0003134F File Offset: 0x0002F54F
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.sorts.GetEnumerator();
		}

		// Token: 0x04000D1B RID: 3355
		private ArrayList sorts = new ArrayList();
	}
}
