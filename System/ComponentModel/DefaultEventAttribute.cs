﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies the default event for a component.</summary>
	// Token: 0x02000154 RID: 340
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class DefaultEventAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultEventAttribute" /> class.</summary>
		/// <param name="name">The name of the default event for the component this attribute is bound to. </param>
		// Token: 0x06000A99 RID: 2713 RVA: 0x0002E9F2 File Offset: 0x0002CBF2
		public DefaultEventAttribute(string name)
		{
			this.name = name;
		}

		/// <summary>Gets the name of the default event for the component this attribute is bound to.</summary>
		/// <returns>The name of the default event for the component this attribute is bound to. The default value is <see langword="null" />.</returns>
		// Token: 0x17000241 RID: 577
		// (get) Token: 0x06000A9A RID: 2714 RVA: 0x0002EA01 File Offset: 0x0002CC01
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		/// <summary>Returns whether the value of the given object is equal to the current <see cref="T:System.ComponentModel.DefaultEventAttribute" />.</summary>
		/// <param name="obj">The object to test the value equality of. </param>
		/// <returns>
		///     <see langword="true" /> if the value of the given object is equal to that of the current; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000A9B RID: 2715 RVA: 0x0002EA0C File Offset: 0x0002CC0C
		public override bool Equals(object obj)
		{
			DefaultEventAttribute defaultEventAttribute = obj as DefaultEventAttribute;
			return defaultEventAttribute != null && defaultEventAttribute.Name == this.name;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000A9C RID: 2716 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000A9D RID: 2717 RVA: 0x0002EA36 File Offset: 0x0002CC36
		// Note: this type is marked as 'beforefieldinit'.
		static DefaultEventAttribute()
		{
		}

		// Token: 0x04000CA5 RID: 3237
		private readonly string name;

		/// <summary>Specifies the default value for the <see cref="T:System.ComponentModel.DefaultEventAttribute" />, which is <see langword="null" />. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000CA6 RID: 3238
		public static readonly DefaultEventAttribute Default = new DefaultEventAttribute(null);
	}
}
