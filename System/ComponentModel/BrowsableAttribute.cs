﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies whether a property or event should be displayed in a Properties window.</summary>
	// Token: 0x02000134 RID: 308
	[AttributeUsage(AttributeTargets.All)]
	public sealed class BrowsableAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.BrowsableAttribute" /> class.</summary>
		/// <param name="browsable">
		///       <see langword="true" /> if a property or event can be modified at design time; otherwise, <see langword="false" />. The default is <see langword="true" />. </param>
		// Token: 0x060009D5 RID: 2517 RVA: 0x0002B5B2 File Offset: 0x000297B2
		public BrowsableAttribute(bool browsable)
		{
			this.browsable = browsable;
		}

		/// <summary>Gets a value indicating whether an object is browsable.</summary>
		/// <returns>
		///     <see langword="true" /> if the object is browsable; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700020F RID: 527
		// (get) Token: 0x060009D6 RID: 2518 RVA: 0x0002B5C8 File Offset: 0x000297C8
		public bool Browsable
		{
			get
			{
				return this.browsable;
			}
		}

		/// <summary>Indicates whether this instance and a specified object are equal.</summary>
		/// <param name="obj">Another object to compare to. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is equal to this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x060009D7 RID: 2519 RVA: 0x0002B5D0 File Offset: 0x000297D0
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			BrowsableAttribute browsableAttribute = obj as BrowsableAttribute;
			return browsableAttribute != null && browsableAttribute.Browsable == this.browsable;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x060009D8 RID: 2520 RVA: 0x0002B5FD File Offset: 0x000297FD
		public override int GetHashCode()
		{
			return this.browsable.GetHashCode();
		}

		/// <summary>Determines if this attribute is the default.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute is the default value for this attribute class; otherwise, <see langword="false" />.</returns>
		// Token: 0x060009D9 RID: 2521 RVA: 0x0002B60A File Offset: 0x0002980A
		public override bool IsDefaultAttribute()
		{
			return this.Equals(BrowsableAttribute.Default);
		}

		// Token: 0x060009DA RID: 2522 RVA: 0x0002B617 File Offset: 0x00029817
		// Note: this type is marked as 'beforefieldinit'.
		static BrowsableAttribute()
		{
		}

		/// <summary>Specifies that a property or event can be modified at design time. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000C60 RID: 3168
		public static readonly BrowsableAttribute Yes = new BrowsableAttribute(true);

		/// <summary>Specifies that a property or event cannot be modified at design time. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000C61 RID: 3169
		public static readonly BrowsableAttribute No = new BrowsableAttribute(false);

		/// <summary>Specifies the default value for the <see cref="T:System.ComponentModel.BrowsableAttribute" />, which is <see cref="F:System.ComponentModel.BrowsableAttribute.Yes" />. This <see langword="static" /> field is read-only.</summary>
		// Token: 0x04000C62 RID: 3170
		public static readonly BrowsableAttribute Default = BrowsableAttribute.Yes;

		// Token: 0x04000C63 RID: 3171
		private bool browsable = true;
	}
}
