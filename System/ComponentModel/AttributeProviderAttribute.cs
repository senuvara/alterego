﻿using System;

namespace System.ComponentModel
{
	/// <summary>Enables attribute redirection. This class cannot be inherited.</summary>
	// Token: 0x0200012B RID: 299
	[AttributeUsage(AttributeTargets.Property)]
	public class AttributeProviderAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.AttributeProviderAttribute" /> class with the given type name.</summary>
		/// <param name="typeName">The name of the type to specify.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="typeName" /> is <see langword="null" />.</exception>
		// Token: 0x06000965 RID: 2405 RVA: 0x0002A9E8 File Offset: 0x00028BE8
		public AttributeProviderAttribute(string typeName)
		{
			if (typeName == null)
			{
				throw new ArgumentNullException("typeName");
			}
			this._typeName = typeName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.AttributeProviderAttribute" /> class with the given type name and property name.</summary>
		/// <param name="typeName">The name of the type to specify.</param>
		/// <param name="propertyName">The name of the property for which attributes will be retrieved.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="typeName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="propertyName" /> is <see langword="null" />.</exception>
		// Token: 0x06000966 RID: 2406 RVA: 0x0002AA05 File Offset: 0x00028C05
		public AttributeProviderAttribute(string typeName, string propertyName)
		{
			if (typeName == null)
			{
				throw new ArgumentNullException("typeName");
			}
			if (propertyName == null)
			{
				throw new ArgumentNullException("propertyName");
			}
			this._typeName = typeName;
			this._propertyName = propertyName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.AttributeProviderAttribute" /> class with the given type.</summary>
		/// <param name="type">The type to specify.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> is <see langword="null" />.</exception>
		// Token: 0x06000967 RID: 2407 RVA: 0x0002AA37 File Offset: 0x00028C37
		public AttributeProviderAttribute(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			this._typeName = type.AssemblyQualifiedName;
		}

		/// <summary>Gets the assembly qualified type name passed into the constructor.</summary>
		/// <returns>The assembly qualified name of the type specified in the constructor.</returns>
		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x06000968 RID: 2408 RVA: 0x0002AA5F File Offset: 0x00028C5F
		public string TypeName
		{
			get
			{
				return this._typeName;
			}
		}

		/// <summary>Gets the name of the property for which attributes will be retrieved.</summary>
		/// <returns>The name of the property for which attributes will be retrieved.</returns>
		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x06000969 RID: 2409 RVA: 0x0002AA67 File Offset: 0x00028C67
		public string PropertyName
		{
			get
			{
				return this._propertyName;
			}
		}

		// Token: 0x04000C39 RID: 3129
		private string _typeName;

		// Token: 0x04000C3A RID: 3130
		private string _propertyName;
	}
}
