﻿using System;

namespace System.ComponentModel
{
	/// <summary>Allows coordination of initialization for a component and its dependent properties.</summary>
	// Token: 0x02000187 RID: 391
	public interface ISupportInitializeNotification : ISupportInitialize
	{
		/// <summary>Gets a value indicating whether the component is initialized.</summary>
		/// <returns>
		///     <see langword="true" /> to indicate the component has completed initialization; otherwise, <see langword="false" />. </returns>
		// Token: 0x1700028D RID: 653
		// (get) Token: 0x06000BD4 RID: 3028
		bool IsInitialized { get; }

		/// <summary>Occurs when initialization of the component is completed.</summary>
		// Token: 0x14000011 RID: 17
		// (add) Token: 0x06000BD5 RID: 3029
		// (remove) Token: 0x06000BD6 RID: 3030
		event EventHandler Initialized;
	}
}
