﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a type converter to convert double-precision, floating point number objects to and from various other representations.</summary>
	// Token: 0x02000162 RID: 354
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class DoubleConverter : BaseNumberConverter
	{
		// Token: 0x17000253 RID: 595
		// (get) Token: 0x06000AFC RID: 2812 RVA: 0x00005AFA File Offset: 0x00003CFA
		internal override bool AllowHex
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000254 RID: 596
		// (get) Token: 0x06000AFD RID: 2813 RVA: 0x0002F1B6 File Offset: 0x0002D3B6
		internal override Type TargetType
		{
			get
			{
				return typeof(double);
			}
		}

		// Token: 0x06000AFE RID: 2814 RVA: 0x0002F1C2 File Offset: 0x0002D3C2
		internal override object FromString(string value, int radix)
		{
			return Convert.ToDouble(value, CultureInfo.CurrentCulture);
		}

		// Token: 0x06000AFF RID: 2815 RVA: 0x0002F1D4 File Offset: 0x0002D3D4
		internal override object FromString(string value, NumberFormatInfo formatInfo)
		{
			return double.Parse(value, NumberStyles.Float, formatInfo);
		}

		// Token: 0x06000B00 RID: 2816 RVA: 0x0002F1E7 File Offset: 0x0002D3E7
		internal override object FromString(string value, CultureInfo culture)
		{
			return double.Parse(value, culture);
		}

		// Token: 0x06000B01 RID: 2817 RVA: 0x0002F1F8 File Offset: 0x0002D3F8
		internal override string ToString(object value, NumberFormatInfo formatInfo)
		{
			return ((double)value).ToString("R", formatInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DoubleConverter" /> class. </summary>
		// Token: 0x06000B02 RID: 2818 RVA: 0x0002B691 File Offset: 0x00029891
		public DoubleConverter()
		{
		}
	}
}
