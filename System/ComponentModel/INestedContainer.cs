﻿using System;

namespace System.ComponentModel
{
	/// <summary>Provides functionality for nested containers, which logically contain zero or more other components and are owned by a parent component.</summary>
	// Token: 0x0200017E RID: 382
	public interface INestedContainer : IContainer, IDisposable
	{
		/// <summary>Gets the owning component for the nested container.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.IComponent" /> that owns the nested container.</returns>
		// Token: 0x17000285 RID: 645
		// (get) Token: 0x06000BC1 RID: 3009
		IComponent Owner { get; }
	}
}
