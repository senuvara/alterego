﻿using System;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides data for the <see cref="E:System.Windows.Forms.BindingSource.AddingNew" /> event.</summary>
	// Token: 0x0200011F RID: 287
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class AddingNewEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.AddingNewEventArgs" /> class using no parameters.</summary>
		// Token: 0x06000918 RID: 2328 RVA: 0x00029FED File Offset: 0x000281ED
		public AddingNewEventArgs()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.AddingNewEventArgs" /> class using the specified object as the new item.</summary>
		/// <param name="newObject">An <see cref="T:System.Object" /> to use as the new item value.</param>
		// Token: 0x06000919 RID: 2329 RVA: 0x00029FF5 File Offset: 0x000281F5
		public AddingNewEventArgs(object newObject)
		{
			this.newObject = newObject;
		}

		/// <summary>Gets or sets the object to be added to the binding list. </summary>
		/// <returns>The <see cref="T:System.Object" /> to be added as a new item to the associated collection. </returns>
		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x0600091A RID: 2330 RVA: 0x0002A004 File Offset: 0x00028204
		// (set) Token: 0x0600091B RID: 2331 RVA: 0x0002A00C File Offset: 0x0002820C
		public object NewObject
		{
			get
			{
				return this.newObject;
			}
			set
			{
				this.newObject = value;
			}
		}

		// Token: 0x04000C24 RID: 3108
		private object newObject;
	}
}
