﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies which event is raised on initialization. This class cannot be inherited.</summary>
	// Token: 0x0200018C RID: 396
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class InitializationEventAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.InitializationEventAttribute" /> class.</summary>
		/// <param name="eventName">The name of the initialization event.</param>
		// Token: 0x06000BE8 RID: 3048 RVA: 0x00030524 File Offset: 0x0002E724
		public InitializationEventAttribute(string eventName)
		{
			this.eventName = eventName;
		}

		/// <summary>Gets the name of the initialization event.</summary>
		/// <returns>The name of the initialization event.</returns>
		// Token: 0x17000293 RID: 659
		// (get) Token: 0x06000BE9 RID: 3049 RVA: 0x00030533 File Offset: 0x0002E733
		public string EventName
		{
			get
			{
				return this.eventName;
			}
		}

		// Token: 0x04000CEC RID: 3308
		private string eventName;
	}
}
