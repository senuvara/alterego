﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies whether a member is typically used for binding. This class cannot be inherited.</summary>
	// Token: 0x0200012F RID: 303
	[AttributeUsage(AttributeTargets.All)]
	public sealed class BindableAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.BindableAttribute" /> class with a Boolean value.</summary>
		/// <param name="bindable">
		///       <see langword="true" /> to use property for binding; otherwise, <see langword="false" />.</param>
		// Token: 0x0600098A RID: 2442 RVA: 0x0002AD76 File Offset: 0x00028F76
		public BindableAttribute(bool bindable) : this(bindable, BindingDirection.OneWay)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.BindableAttribute" /> class.</summary>
		/// <param name="bindable">
		///       <see langword="true" /> to use property for binding; otherwise, <see langword="false" />.</param>
		/// <param name="direction">One of the <see cref="T:System.ComponentModel.BindingDirection" /> values.</param>
		// Token: 0x0600098B RID: 2443 RVA: 0x0002AD80 File Offset: 0x00028F80
		public BindableAttribute(bool bindable, BindingDirection direction)
		{
			this.bindable = bindable;
			this.direction = direction;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.BindableAttribute" /> class with one of the <see cref="T:System.ComponentModel.BindableSupport" /> values.</summary>
		/// <param name="flags">One of the <see cref="T:System.ComponentModel.BindableSupport" /> values. </param>
		// Token: 0x0600098C RID: 2444 RVA: 0x0002AD96 File Offset: 0x00028F96
		public BindableAttribute(BindableSupport flags) : this(flags, BindingDirection.OneWay)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.BindableAttribute" /> class.</summary>
		/// <param name="flags">One of the <see cref="T:System.ComponentModel.BindableSupport" /> values. </param>
		/// <param name="direction">One of the <see cref="T:System.ComponentModel.BindingDirection" /> values.</param>
		// Token: 0x0600098D RID: 2445 RVA: 0x0002ADA0 File Offset: 0x00028FA0
		public BindableAttribute(BindableSupport flags, BindingDirection direction)
		{
			this.bindable = (flags > BindableSupport.No);
			this.isDefault = (flags == BindableSupport.Default);
			this.direction = direction;
		}

		/// <summary>Gets a value indicating that a property is typically used for binding.</summary>
		/// <returns>
		///     <see langword="true" /> if the property is typically used for binding; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x0600098E RID: 2446 RVA: 0x0002ADC3 File Offset: 0x00028FC3
		public bool Bindable
		{
			get
			{
				return this.bindable;
			}
		}

		/// <summary>Gets a value indicating the direction or directions of this property's data binding.</summary>
		/// <returns>The direction of this property’s data binding.</returns>
		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x0600098F RID: 2447 RVA: 0x0002ADCB File Offset: 0x00028FCB
		public BindingDirection Direction
		{
			get
			{
				return this.direction;
			}
		}

		/// <summary>Determines whether two <see cref="T:System.ComponentModel.BindableAttribute" /> objects are equal.</summary>
		/// <param name="obj">The object to compare.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.ComponentModel.BindableAttribute" /> is equal to the current <see cref="T:System.ComponentModel.BindableAttribute" />; <see langword="false" /> if it is not equal.</returns>
		// Token: 0x06000990 RID: 2448 RVA: 0x0002ADD3 File Offset: 0x00028FD3
		public override bool Equals(object obj)
		{
			return obj == this || (obj != null && obj is BindableAttribute && ((BindableAttribute)obj).Bindable == this.bindable);
		}

		/// <summary>Serves as a hash function for the <see cref="T:System.ComponentModel.BindableAttribute" /> class.</summary>
		/// <returns>A hash code for the current <see cref="T:System.ComponentModel.BindableAttribute" />.</returns>
		// Token: 0x06000991 RID: 2449 RVA: 0x0002ADFB File Offset: 0x00028FFB
		public override int GetHashCode()
		{
			return this.bindable.GetHashCode();
		}

		/// <summary>Determines if this attribute is the default.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute is the default value for this attribute class; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000992 RID: 2450 RVA: 0x0002AE08 File Offset: 0x00029008
		public override bool IsDefaultAttribute()
		{
			return this.Equals(BindableAttribute.Default) || this.isDefault;
		}

		// Token: 0x06000993 RID: 2451 RVA: 0x0002AE1F File Offset: 0x0002901F
		// Note: this type is marked as 'beforefieldinit'.
		static BindableAttribute()
		{
		}

		/// <summary>Specifies that a property is typically used for binding. This field is read-only.</summary>
		// Token: 0x04000C46 RID: 3142
		public static readonly BindableAttribute Yes = new BindableAttribute(true);

		/// <summary>Specifies that a property is not typically used for binding. This field is read-only.</summary>
		// Token: 0x04000C47 RID: 3143
		public static readonly BindableAttribute No = new BindableAttribute(false);

		/// <summary>Specifies the default value for the <see cref="T:System.ComponentModel.BindableAttribute" />, which is <see cref="F:System.ComponentModel.BindableAttribute.No" />. This field is read-only.</summary>
		// Token: 0x04000C48 RID: 3144
		public static readonly BindableAttribute Default = BindableAttribute.No;

		// Token: 0x04000C49 RID: 3145
		private bool bindable;

		// Token: 0x04000C4A RID: 3146
		private bool isDefault;

		// Token: 0x04000C4B RID: 3147
		private BindingDirection direction;
	}
}
