﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies the browsable state of a property or method from within an editor.</summary>
	// Token: 0x02000165 RID: 357
	public enum EditorBrowsableState
	{
		/// <summary>The property or method is always browsable from within an editor.</summary>
		// Token: 0x04000CD0 RID: 3280
		Always,
		/// <summary>The property or method is never browsable from within an editor.</summary>
		// Token: 0x04000CD1 RID: 3281
		Never,
		/// <summary>The property or method is a feature that only advanced users should see. An editor can either show or hide such properties.</summary>
		// Token: 0x04000CD2 RID: 3282
		Advanced
	}
}
