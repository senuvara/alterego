﻿using System;
using System.Globalization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Provides a type converter to convert single-precision, floating point number objects to and from various other representations.</summary>
	// Token: 0x020001CF RID: 463
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public class SingleConverter : BaseNumberConverter
	{
		// Token: 0x17000321 RID: 801
		// (get) Token: 0x06000E66 RID: 3686 RVA: 0x00005AFA File Offset: 0x00003CFA
		internal override bool AllowHex
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000322 RID: 802
		// (get) Token: 0x06000E67 RID: 3687 RVA: 0x000393D5 File Offset: 0x000375D5
		internal override Type TargetType
		{
			get
			{
				return typeof(float);
			}
		}

		// Token: 0x06000E68 RID: 3688 RVA: 0x000393E1 File Offset: 0x000375E1
		internal override object FromString(string value, int radix)
		{
			return Convert.ToSingle(value, CultureInfo.CurrentCulture);
		}

		// Token: 0x06000E69 RID: 3689 RVA: 0x000393F3 File Offset: 0x000375F3
		internal override object FromString(string value, NumberFormatInfo formatInfo)
		{
			return float.Parse(value, NumberStyles.Float, formatInfo);
		}

		// Token: 0x06000E6A RID: 3690 RVA: 0x00039406 File Offset: 0x00037606
		internal override object FromString(string value, CultureInfo culture)
		{
			return float.Parse(value, culture);
		}

		// Token: 0x06000E6B RID: 3691 RVA: 0x00039414 File Offset: 0x00037614
		internal override string ToString(object value, NumberFormatInfo formatInfo)
		{
			return ((float)value).ToString("R", formatInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.SingleConverter" /> class. </summary>
		// Token: 0x06000E6C RID: 3692 RVA: 0x0002B691 File Offset: 0x00029891
		public SingleConverter()
		{
		}
	}
}
