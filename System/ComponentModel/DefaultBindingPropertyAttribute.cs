﻿using System;

namespace System.ComponentModel
{
	/// <summary>Specifies the default binding property for a component. This class cannot be inherited.</summary>
	// Token: 0x02000153 RID: 339
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class DefaultBindingPropertyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultBindingPropertyAttribute" /> class using no parameters. </summary>
		// Token: 0x06000A93 RID: 2707 RVA: 0x0002E995 File Offset: 0x0002CB95
		public DefaultBindingPropertyAttribute()
		{
			this.name = null;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.DefaultBindingPropertyAttribute" /> class using the specified property name.</summary>
		/// <param name="name">The name of the default binding property.</param>
		// Token: 0x06000A94 RID: 2708 RVA: 0x0002E9A4 File Offset: 0x0002CBA4
		public DefaultBindingPropertyAttribute(string name)
		{
			this.name = name;
		}

		/// <summary>Gets the name of the default binding property for the component to which the <see cref="T:System.ComponentModel.DefaultBindingPropertyAttribute" /> is bound.</summary>
		/// <returns>The name of the default binding property for the component to which the <see cref="T:System.ComponentModel.DefaultBindingPropertyAttribute" /> is bound.</returns>
		// Token: 0x17000240 RID: 576
		// (get) Token: 0x06000A95 RID: 2709 RVA: 0x0002E9B3 File Offset: 0x0002CBB3
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		/// <summary>Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.ComponentModel.DefaultBindingPropertyAttribute" /> instance. </summary>
		/// <param name="obj">The <see cref="T:System.Object" /> to compare with the current <see cref="T:System.ComponentModel.DefaultBindingPropertyAttribute" /> instance</param>
		/// <returns>
		///     <see langword="true" /> if the object is equal to the current instance; otherwise, <see langword="false" />, indicating they are not equal.</returns>
		// Token: 0x06000A96 RID: 2710 RVA: 0x0002E9BC File Offset: 0x0002CBBC
		public override bool Equals(object obj)
		{
			DefaultBindingPropertyAttribute defaultBindingPropertyAttribute = obj as DefaultBindingPropertyAttribute;
			return defaultBindingPropertyAttribute != null && defaultBindingPropertyAttribute.Name == this.name;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000A97 RID: 2711 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000A98 RID: 2712 RVA: 0x0002E9E6 File Offset: 0x0002CBE6
		// Note: this type is marked as 'beforefieldinit'.
		static DefaultBindingPropertyAttribute()
		{
		}

		// Token: 0x04000CA3 RID: 3235
		private readonly string name;

		/// <summary>Represents the default value for the <see cref="T:System.ComponentModel.DefaultBindingPropertyAttribute" /> class.</summary>
		// Token: 0x04000CA4 RID: 3236
		public static readonly DefaultBindingPropertyAttribute Default = new DefaultBindingPropertyAttribute();
	}
}
