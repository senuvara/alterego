﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.ComponentModel
{
	/// <summary>Specifies an exception that is handled as a warning instead of an error.</summary>
	// Token: 0x020001EF RID: 495
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	[Serializable]
	public class WarningException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.WarningException" /> class. </summary>
		// Token: 0x06000FA9 RID: 4009 RVA: 0x0003DA79 File Offset: 0x0003BC79
		public WarningException() : this(null, null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.WarningException" /> class with the specified message and no Help file.</summary>
		/// <param name="message">The message to display to the end user. </param>
		// Token: 0x06000FAA RID: 4010 RVA: 0x0003DA84 File Offset: 0x0003BC84
		public WarningException(string message) : this(message, null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.WarningException" /> class with the specified message, and with access to the specified Help file.</summary>
		/// <param name="message">The message to display to the end user. </param>
		/// <param name="helpUrl">The Help file to display if the user requests help. </param>
		// Token: 0x06000FAB RID: 4011 RVA: 0x0003DA8F File Offset: 0x0003BC8F
		public WarningException(string message, string helpUrl) : this(message, helpUrl, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.WarningException" /> class with the specified detailed description and the specified exception. </summary>
		/// <param name="message">A detailed description of the error.</param>
		/// <param name="innerException">A reference to the inner exception that is the cause of this exception.</param>
		// Token: 0x06000FAC RID: 4012 RVA: 0x0003DA9A File Offset: 0x0003BC9A
		public WarningException(string message, Exception innerException) : base(message, innerException)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.WarningException" /> class with the specified message, and with access to the specified Help file and topic.</summary>
		/// <param name="message">The message to display to the end user. </param>
		/// <param name="helpUrl">The Help file to display if the user requests help. </param>
		/// <param name="helpTopic">The Help topic to display if the user requests help. </param>
		// Token: 0x06000FAD RID: 4013 RVA: 0x0003DAA4 File Offset: 0x0003BCA4
		public WarningException(string message, string helpUrl, string helpTopic) : base(message)
		{
			this.helpUrl = helpUrl;
			this.helpTopic = helpTopic;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.WarningException" /> class using the specified serialization data and context.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to be used for deserialization.</param>
		/// <param name="context">The destination to be used for deserialization.</param>
		// Token: 0x06000FAE RID: 4014 RVA: 0x0003DABC File Offset: 0x0003BCBC
		protected WarningException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.helpUrl = (string)info.GetValue("helpUrl", typeof(string));
			this.helpTopic = (string)info.GetValue("helpTopic", typeof(string));
		}

		/// <summary>Gets the Help file associated with the warning.</summary>
		/// <returns>The Help file associated with the warning.</returns>
		// Token: 0x17000339 RID: 825
		// (get) Token: 0x06000FAF RID: 4015 RVA: 0x0003DB11 File Offset: 0x0003BD11
		public string HelpUrl
		{
			get
			{
				return this.helpUrl;
			}
		}

		/// <summary>Gets the Help topic associated with the warning.</summary>
		/// <returns>The Help topic associated with the warning.</returns>
		// Token: 0x1700033A RID: 826
		// (get) Token: 0x06000FB0 RID: 4016 RVA: 0x0003DB19 File Offset: 0x0003BD19
		public string HelpTopic
		{
			get
			{
				return this.helpTopic;
			}
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the parameter name and additional exception information.</summary>
		/// <param name="info">Stores the data that was being used to serialize or deserialize the object that the <see cref="T:System.ComponentModel.Design.Serialization.CodeDomSerializer" /> was serializing or deserializing. </param>
		/// <param name="context">Describes the source and destination of the stream that generated the exception, as well as a means for serialization to retain that context and an additional caller-defined context. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info" /> is <see langword="null" />.</exception>
		// Token: 0x06000FB1 RID: 4017 RVA: 0x0003DB21 File Offset: 0x0003BD21
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("helpUrl", this.helpUrl);
			info.AddValue("helpTopic", this.helpTopic);
			base.GetObjectData(info, context);
		}

		// Token: 0x04000E1E RID: 3614
		private readonly string helpUrl;

		// Token: 0x04000E1F RID: 3615
		private readonly string helpTopic;
	}
}
