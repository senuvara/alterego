﻿using System;

namespace System.ComponentModel
{
	/// <summary>Provides the ability to retrieve the full nested name of a component.</summary>
	// Token: 0x0200017F RID: 383
	public interface INestedSite : ISite, IServiceProvider
	{
		/// <summary>Gets the full name of the component in this site.</summary>
		/// <returns>The full name of the component in this site.</returns>
		// Token: 0x17000286 RID: 646
		// (get) Token: 0x06000BC2 RID: 3010
		string FullName { get; }
	}
}
