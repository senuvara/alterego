﻿using System;
using System.Security.Permissions;
using System.Threading;
using Unity;

namespace System.ComponentModel
{
	/// <summary>Tracks the lifetime of an asynchronous operation.</summary>
	// Token: 0x02000127 RID: 295
	[HostProtection(SecurityAction.LinkDemand, SharedState = true)]
	public sealed class AsyncOperation
	{
		// Token: 0x06000943 RID: 2371 RVA: 0x0002A3A8 File Offset: 0x000285A8
		private AsyncOperation(object userSuppliedState, SynchronizationContext syncContext)
		{
			this.userSuppliedState = userSuppliedState;
			this.syncContext = syncContext;
			this.alreadyCompleted = false;
			this.syncContext.OperationStarted();
		}

		/// <summary>Finalizes the asynchronous operation.</summary>
		// Token: 0x06000944 RID: 2372 RVA: 0x0002A3D0 File Offset: 0x000285D0
		~AsyncOperation()
		{
			if (!this.alreadyCompleted && this.syncContext != null)
			{
				this.syncContext.OperationCompleted();
			}
		}

		/// <summary>Gets or sets an object used to uniquely identify an asynchronous operation.</summary>
		/// <returns>The state object passed to the asynchronous method invocation.</returns>
		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06000945 RID: 2373 RVA: 0x0002A414 File Offset: 0x00028614
		public object UserSuppliedState
		{
			get
			{
				return this.userSuppliedState;
			}
		}

		/// <summary>Gets the <see cref="T:System.Threading.SynchronizationContext" /> object that was passed to the constructor.</summary>
		/// <returns>The <see cref="T:System.Threading.SynchronizationContext" /> object that was passed to the constructor.</returns>
		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x06000946 RID: 2374 RVA: 0x0002A41C File Offset: 0x0002861C
		public SynchronizationContext SynchronizationContext
		{
			get
			{
				return this.syncContext;
			}
		}

		/// <summary>Invokes a delegate on the thread or context appropriate for the application model.</summary>
		/// <param name="d">A <see cref="T:System.Threading.SendOrPostCallback" /> object that wraps the delegate to be called when the operation ends. </param>
		/// <param name="arg">An argument for the delegate contained in the <paramref name="d" /> parameter. </param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="M:System.ComponentModel.AsyncOperation.PostOperationCompleted(System.Threading.SendOrPostCallback,System.Object)" /> method has been called previously for this task. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="d" /> is <see langword="null" />. </exception>
		// Token: 0x06000947 RID: 2375 RVA: 0x0002A424 File Offset: 0x00028624
		public void Post(SendOrPostCallback d, object arg)
		{
			this.VerifyNotCompleted();
			this.VerifyDelegateNotNull(d);
			this.syncContext.Post(d, arg);
		}

		/// <summary>Ends the lifetime of an asynchronous operation.</summary>
		/// <param name="d">A <see cref="T:System.Threading.SendOrPostCallback" /> object that wraps the delegate to be called when the operation ends. </param>
		/// <param name="arg">An argument for the delegate contained in the <paramref name="d" /> parameter. </param>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.ComponentModel.AsyncOperation.OperationCompleted" /> has been called previously for this task. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="d" /> is <see langword="null" />. </exception>
		// Token: 0x06000948 RID: 2376 RVA: 0x0002A440 File Offset: 0x00028640
		public void PostOperationCompleted(SendOrPostCallback d, object arg)
		{
			this.Post(d, arg);
			this.OperationCompletedCore();
		}

		/// <summary>Ends the lifetime of an asynchronous operation.</summary>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.ComponentModel.AsyncOperation.OperationCompleted" /> has been called previously for this task. </exception>
		// Token: 0x06000949 RID: 2377 RVA: 0x0002A450 File Offset: 0x00028650
		public void OperationCompleted()
		{
			this.VerifyNotCompleted();
			this.OperationCompletedCore();
		}

		// Token: 0x0600094A RID: 2378 RVA: 0x0002A460 File Offset: 0x00028660
		private void OperationCompletedCore()
		{
			try
			{
				this.syncContext.OperationCompleted();
			}
			finally
			{
				this.alreadyCompleted = true;
				GC.SuppressFinalize(this);
			}
		}

		// Token: 0x0600094B RID: 2379 RVA: 0x0002A498 File Offset: 0x00028698
		private void VerifyNotCompleted()
		{
			if (this.alreadyCompleted)
			{
				throw new InvalidOperationException(SR.GetString("This operation has already had OperationCompleted called on it and further calls are illegal."));
			}
		}

		// Token: 0x0600094C RID: 2380 RVA: 0x0002A4B2 File Offset: 0x000286B2
		private void VerifyDelegateNotNull(SendOrPostCallback d)
		{
			if (d == null)
			{
				throw new ArgumentNullException(SR.GetString("A non-null SendOrPostCallback must be supplied."), "d");
			}
		}

		// Token: 0x0600094D RID: 2381 RVA: 0x0002A4CC File Offset: 0x000286CC
		internal static AsyncOperation CreateOperation(object userSuppliedState, SynchronizationContext syncContext)
		{
			return new AsyncOperation(userSuppliedState, syncContext);
		}

		// Token: 0x0600094E RID: 2382 RVA: 0x000092E2 File Offset: 0x000074E2
		internal AsyncOperation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000C2D RID: 3117
		private SynchronizationContext syncContext;

		// Token: 0x04000C2E RID: 3118
		private object userSuppliedState;

		// Token: 0x04000C2F RID: 3119
		private bool alreadyCompleted;
	}
}
