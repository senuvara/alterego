﻿using System;

namespace System.ComponentModel
{
	/// <summary>Indicates that the parent property is notified when the value of the property that this attribute is applied to is modified. This class cannot be inherited.</summary>
	// Token: 0x020001F4 RID: 500
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class NotifyParentPropertyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.NotifyParentPropertyAttribute" /> class, using the specified value to determine whether the parent property is notified of changes to the value of the property.</summary>
		/// <param name="notifyParent">
		///       <see langword="true" /> if the parent should be notified of changes; otherwise, <see langword="false" />. </param>
		// Token: 0x06000FD1 RID: 4049 RVA: 0x0003E55F File Offset: 0x0003C75F
		public NotifyParentPropertyAttribute(bool notifyParent)
		{
			this.notifyParent = notifyParent;
		}

		/// <summary>Gets or sets a value indicating whether the parent property should be notified of changes to the value of the property.</summary>
		/// <returns>
		///     <see langword="true" /> if the parent property should be notified of changes; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700033F RID: 831
		// (get) Token: 0x06000FD2 RID: 4050 RVA: 0x0003E56E File Offset: 0x0003C76E
		public bool NotifyParent
		{
			get
			{
				return this.notifyParent;
			}
		}

		/// <summary>Gets a value indicating whether the specified object is the same as the current object.</summary>
		/// <param name="obj">The object to test for equality. </param>
		/// <returns>
		///     <see langword="true" /> if the object is the same as this object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000FD3 RID: 4051 RVA: 0x0003E576 File Offset: 0x0003C776
		public override bool Equals(object obj)
		{
			return obj == this || (obj != null && obj is NotifyParentPropertyAttribute && ((NotifyParentPropertyAttribute)obj).NotifyParent == this.notifyParent);
		}

		/// <summary>Gets the hash code for this object.</summary>
		/// <returns>The hash code for the object the attribute belongs to.</returns>
		// Token: 0x06000FD4 RID: 4052 RVA: 0x0002A14E File Offset: 0x0002834E
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Gets a value indicating whether the current value of the attribute is the default value for the attribute.</summary>
		/// <returns>
		///     <see langword="true" /> if the current value of the attribute is the default value of the attribute; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000FD5 RID: 4053 RVA: 0x0003E59E File Offset: 0x0003C79E
		public override bool IsDefaultAttribute()
		{
			return this.Equals(NotifyParentPropertyAttribute.Default);
		}

		// Token: 0x06000FD6 RID: 4054 RVA: 0x0003E5AB File Offset: 0x0003C7AB
		// Note: this type is marked as 'beforefieldinit'.
		static NotifyParentPropertyAttribute()
		{
		}

		/// <summary>Indicates that the parent property is notified of changes to the value of the property. This field is read-only.</summary>
		// Token: 0x04000E2C RID: 3628
		public static readonly NotifyParentPropertyAttribute Yes = new NotifyParentPropertyAttribute(true);

		/// <summary>Indicates that the parent property is not be notified of changes to the value of the property. This field is read-only.</summary>
		// Token: 0x04000E2D RID: 3629
		public static readonly NotifyParentPropertyAttribute No = new NotifyParentPropertyAttribute(false);

		/// <summary>Indicates the default attribute state, that the property should not notify the parent property of changes to its value. This field is read-only.</summary>
		// Token: 0x04000E2E RID: 3630
		public static readonly NotifyParentPropertyAttribute Default = NotifyParentPropertyAttribute.No;

		// Token: 0x04000E2F RID: 3631
		private bool notifyParent;
	}
}
