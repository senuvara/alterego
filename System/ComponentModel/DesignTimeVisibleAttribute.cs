﻿using System;

namespace System.ComponentModel
{
	/// <summary>
	///     <see cref="T:System.ComponentModel.DesignTimeVisibleAttribute" /> marks a component's visibility. If <see cref="F:System.ComponentModel.DesignTimeVisibleAttribute.Yes" /> is present, a visual designer can show this component on a designer.</summary>
	// Token: 0x0200015A RID: 346
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
	public sealed class DesignTimeVisibleAttribute : Attribute
	{
		/// <summary>Creates a new <see cref="T:System.ComponentModel.DesignTimeVisibleAttribute" /> with the <see cref="P:System.ComponentModel.DesignTimeVisibleAttribute.Visible" /> property set to the given value in <paramref name="visible" />.</summary>
		/// <param name="visible">The value that the <see cref="P:System.ComponentModel.DesignTimeVisibleAttribute.Visible" /> property will be set against. </param>
		// Token: 0x06000ACC RID: 2764 RVA: 0x0002ED88 File Offset: 0x0002CF88
		public DesignTimeVisibleAttribute(bool visible)
		{
			this.visible = visible;
		}

		/// <summary>Creates a new <see cref="T:System.ComponentModel.DesignTimeVisibleAttribute" /> set to the default value of <see langword="false" />.</summary>
		// Token: 0x06000ACD RID: 2765 RVA: 0x000020AE File Offset: 0x000002AE
		public DesignTimeVisibleAttribute()
		{
		}

		/// <summary>Gets or sets whether the component should be shown at design time.</summary>
		/// <returns>
		///     <see langword="true" /> if this component should be shown at design time, or <see langword="false" /> if it shouldn't.</returns>
		// Token: 0x17000248 RID: 584
		// (get) Token: 0x06000ACE RID: 2766 RVA: 0x0002ED97 File Offset: 0x0002CF97
		public bool Visible
		{
			get
			{
				return this.visible;
			}
		}

		/// <summary>Returns a value that indicates whether this instance is equal to a specified object.</summary>
		/// <param name="obj">An Object to compare with this instance or a null reference (<see langword="Nothing" /> in Visual Basic).</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> equals the type and value of this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000ACF RID: 2767 RVA: 0x0002EDA0 File Offset: 0x0002CFA0
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			DesignTimeVisibleAttribute designTimeVisibleAttribute = obj as DesignTimeVisibleAttribute;
			return designTimeVisibleAttribute != null && designTimeVisibleAttribute.Visible == this.visible;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000AD0 RID: 2768 RVA: 0x0002EDCD File Offset: 0x0002CFCD
		public override int GetHashCode()
		{
			return typeof(DesignTimeVisibleAttribute).GetHashCode() ^ (this.visible ? -1 : 0);
		}

		/// <summary>Gets a value indicating if this instance is equal to the <see cref="F:System.ComponentModel.DesignTimeVisibleAttribute.Default" /> value.</summary>
		/// <returns>
		///     <see langword="true" />, if this instance is equal to the <see cref="F:System.ComponentModel.DesignTimeVisibleAttribute.Default" /> value; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AD1 RID: 2769 RVA: 0x0002EDEB File Offset: 0x0002CFEB
		public override bool IsDefaultAttribute()
		{
			return this.Visible == DesignTimeVisibleAttribute.Default.Visible;
		}

		// Token: 0x06000AD2 RID: 2770 RVA: 0x0002EDFF File Offset: 0x0002CFFF
		// Note: this type is marked as 'beforefieldinit'.
		static DesignTimeVisibleAttribute()
		{
		}

		// Token: 0x04000CB1 RID: 3249
		private bool visible;

		/// <summary>Marks a component as visible in a visual designer.</summary>
		// Token: 0x04000CB2 RID: 3250
		public static readonly DesignTimeVisibleAttribute Yes = new DesignTimeVisibleAttribute(true);

		/// <summary>Marks a component as not visible in a visual designer.</summary>
		// Token: 0x04000CB3 RID: 3251
		public static readonly DesignTimeVisibleAttribute No = new DesignTimeVisibleAttribute(false);

		/// <summary>The default visibility which is <see langword="Yes" />.</summary>
		// Token: 0x04000CB4 RID: 3252
		public static readonly DesignTimeVisibleAttribute Default = DesignTimeVisibleAttribute.Yes;
	}
}
