﻿using System;

namespace System
{
	/// <summary>A customizable parser based on the news scheme using the Network News Transfer Protocol (NNTP).</summary>
	// Token: 0x020000A4 RID: 164
	public class NewsStyleUriParser : UriParser
	{
		/// <summary>Create a customizable parser based on the news scheme using the Network News Transfer Protocol (NNTP).</summary>
		// Token: 0x060003EA RID: 1002 RVA: 0x0001371B File Offset: 0x0001191B
		public NewsStyleUriParser() : base(UriParser.NewsUri.Flags)
		{
		}
	}
}
