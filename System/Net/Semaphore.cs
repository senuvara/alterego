﻿using System;
using System.Threading;

namespace System.Net
{
	// Token: 0x0200035E RID: 862
	internal sealed class Semaphore : WaitHandle
	{
		// Token: 0x060018CF RID: 6351 RVA: 0x00059138 File Offset: 0x00057338
		internal Semaphore(int initialCount, int maxCount)
		{
			lock (this)
			{
				int num;
				this.Handle = Semaphore.CreateSemaphore_internal(initialCount, maxCount, null, out num);
			}
		}

		// Token: 0x060018D0 RID: 6352 RVA: 0x00059184 File Offset: 0x00057384
		internal bool ReleaseSemaphore()
		{
			int num;
			return Semaphore.ReleaseSemaphore_internal(this.Handle, 1, out num);
		}
	}
}
