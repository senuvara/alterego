﻿using System;
using System.Text;

namespace System.Net
{
	// Token: 0x020003F2 RID: 1010
	internal struct IPv6AddressFormatter
	{
		// Token: 0x06001F18 RID: 7960 RVA: 0x00070CD4 File Offset: 0x0006EED4
		public IPv6AddressFormatter(ushort[] addr, long scopeId)
		{
			this.address = addr;
			this.scopeId = scopeId;
		}

		// Token: 0x06001F19 RID: 7961 RVA: 0x00070CE4 File Offset: 0x0006EEE4
		private static ushort SwapUShort(ushort number)
		{
			return (ushort)((number >> 8 & 255) + ((int)number << 8 & 65280));
		}

		// Token: 0x06001F1A RID: 7962 RVA: 0x00070CFA File Offset: 0x0006EEFA
		private uint AsIPv4Int()
		{
			return (uint)(((int)IPv6AddressFormatter.SwapUShort(this.address[7]) << 16) + (int)IPv6AddressFormatter.SwapUShort(this.address[6]));
		}

		// Token: 0x06001F1B RID: 7963 RVA: 0x00070D1C File Offset: 0x0006EF1C
		private bool IsIPv4Compatible()
		{
			for (int i = 0; i < 6; i++)
			{
				if (this.address[i] != 0)
				{
					return false;
				}
			}
			return this.address[6] != 0 && this.AsIPv4Int() > 1U;
		}

		// Token: 0x06001F1C RID: 7964 RVA: 0x00070D58 File Offset: 0x0006EF58
		private bool IsIPv4Mapped()
		{
			for (int i = 0; i < 5; i++)
			{
				if (this.address[i] != 0)
				{
					return false;
				}
			}
			return this.address[6] != 0 && this.address[5] == ushort.MaxValue;
		}

		// Token: 0x06001F1D RID: 7965 RVA: 0x00070D98 File Offset: 0x0006EF98
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (this.IsIPv4Compatible() || this.IsIPv4Mapped())
			{
				stringBuilder.Append("::");
				if (this.IsIPv4Mapped())
				{
					stringBuilder.Append("ffff:");
				}
				stringBuilder.Append(new IPAddress((long)((ulong)this.AsIPv4Int())).ToString());
				return stringBuilder.ToString();
			}
			int num = -1;
			int num2 = 0;
			int num3 = 0;
			for (int i = 0; i < 8; i++)
			{
				if (this.address[i] != 0)
				{
					if (num3 > num2 && num3 > 1)
					{
						num2 = num3;
						num = i - num3;
					}
					num3 = 0;
				}
				else
				{
					num3++;
				}
			}
			if (num3 > num2 && num3 > 1)
			{
				num2 = num3;
				num = 8 - num3;
			}
			if (num == 0)
			{
				stringBuilder.Append(":");
			}
			for (int j = 0; j < 8; j++)
			{
				if (j == num)
				{
					stringBuilder.Append(":");
					j += num2 - 1;
				}
				else
				{
					stringBuilder.AppendFormat("{0:x}", this.address[j]);
					if (j < 7)
					{
						stringBuilder.Append(':');
					}
				}
			}
			if (this.scopeId != 0L)
			{
				stringBuilder.Append('%').Append(this.scopeId);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04001AB1 RID: 6833
		private ushort[] address;

		// Token: 0x04001AB2 RID: 6834
		private long scopeId;
	}
}
