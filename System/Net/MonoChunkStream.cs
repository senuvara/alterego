﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace System.Net
{
	// Token: 0x020003F7 RID: 1015
	internal class MonoChunkStream
	{
		// Token: 0x06001F3E RID: 7998 RVA: 0x00071620 File Offset: 0x0006F820
		public MonoChunkStream(byte[] buffer, int offset, int size, WebHeaderCollection headers) : this(headers)
		{
			this.Write(buffer, offset, size);
		}

		// Token: 0x06001F3F RID: 7999 RVA: 0x00071633 File Offset: 0x0006F833
		public MonoChunkStream(WebHeaderCollection headers)
		{
			this.headers = headers;
			this.saved = new StringBuilder();
			this.chunks = new ArrayList();
			this.chunkSize = -1;
			this.totalWritten = 0;
		}

		// Token: 0x06001F40 RID: 8000 RVA: 0x00071666 File Offset: 0x0006F866
		public void ResetBuffer()
		{
			this.chunkSize = -1;
			this.chunkRead = 0;
			this.totalWritten = 0;
			this.chunks.Clear();
		}

		// Token: 0x06001F41 RID: 8001 RVA: 0x00071688 File Offset: 0x0006F888
		public void WriteAndReadBack(byte[] buffer, int offset, int size, ref int read)
		{
			if (offset + read > 0)
			{
				this.Write(buffer, offset, offset + read);
			}
			read = this.Read(buffer, offset, size);
		}

		// Token: 0x06001F42 RID: 8002 RVA: 0x000716AB File Offset: 0x0006F8AB
		public int Read(byte[] buffer, int offset, int size)
		{
			return this.ReadFromChunks(buffer, offset, size);
		}

		// Token: 0x06001F43 RID: 8003 RVA: 0x000716B8 File Offset: 0x0006F8B8
		private int ReadFromChunks(byte[] buffer, int offset, int size)
		{
			int count = this.chunks.Count;
			int num = 0;
			List<MonoChunkStream.Chunk> list = new List<MonoChunkStream.Chunk>(count);
			for (int i = 0; i < count; i++)
			{
				MonoChunkStream.Chunk chunk = (MonoChunkStream.Chunk)this.chunks[i];
				if (chunk.Offset == chunk.Bytes.Length)
				{
					list.Add(chunk);
				}
				else
				{
					num += chunk.Read(buffer, offset + num, size - num);
					if (num == size)
					{
						break;
					}
				}
			}
			foreach (MonoChunkStream.Chunk obj in list)
			{
				this.chunks.Remove(obj);
			}
			return num;
		}

		// Token: 0x06001F44 RID: 8004 RVA: 0x00071774 File Offset: 0x0006F974
		public void Write(byte[] buffer, int offset, int size)
		{
			if (offset < size)
			{
				this.InternalWrite(buffer, ref offset, size);
			}
		}

		// Token: 0x06001F45 RID: 8005 RVA: 0x00071784 File Offset: 0x0006F984
		private void InternalWrite(byte[] buffer, ref int offset, int size)
		{
			if (this.state == MonoChunkStream.State.None || this.state == MonoChunkStream.State.PartialSize)
			{
				this.state = this.GetChunkSize(buffer, ref offset, size);
				if (this.state == MonoChunkStream.State.PartialSize)
				{
					return;
				}
				this.saved.Length = 0;
				this.sawCR = false;
				this.gotit = false;
			}
			if (this.state == MonoChunkStream.State.Body && offset < size)
			{
				this.state = this.ReadBody(buffer, ref offset, size);
				if (this.state == MonoChunkStream.State.Body)
				{
					return;
				}
			}
			if (this.state == MonoChunkStream.State.BodyFinished && offset < size)
			{
				this.state = this.ReadCRLF(buffer, ref offset, size);
				if (this.state == MonoChunkStream.State.BodyFinished)
				{
					return;
				}
				this.sawCR = false;
			}
			if (this.state == MonoChunkStream.State.Trailer && offset < size)
			{
				this.state = this.ReadTrailer(buffer, ref offset, size);
				if (this.state == MonoChunkStream.State.Trailer)
				{
					return;
				}
				this.saved.Length = 0;
				this.sawCR = false;
				this.gotit = false;
			}
			if (offset < size)
			{
				this.InternalWrite(buffer, ref offset, size);
			}
		}

		// Token: 0x170006B2 RID: 1714
		// (get) Token: 0x06001F46 RID: 8006 RVA: 0x00071879 File Offset: 0x0006FA79
		public bool WantMore
		{
			get
			{
				return this.chunkRead != this.chunkSize || this.chunkSize != 0 || this.state > MonoChunkStream.State.None;
			}
		}

		// Token: 0x170006B3 RID: 1715
		// (get) Token: 0x06001F47 RID: 8007 RVA: 0x0007189C File Offset: 0x0006FA9C
		public bool DataAvailable
		{
			get
			{
				int count = this.chunks.Count;
				for (int i = 0; i < count; i++)
				{
					MonoChunkStream.Chunk chunk = (MonoChunkStream.Chunk)this.chunks[i];
					if (chunk != null && chunk.Bytes != null && chunk.Bytes.Length != 0 && chunk.Offset < chunk.Bytes.Length)
					{
						return this.state != MonoChunkStream.State.Body;
					}
				}
				return false;
			}
		}

		// Token: 0x170006B4 RID: 1716
		// (get) Token: 0x06001F48 RID: 8008 RVA: 0x00071905 File Offset: 0x0006FB05
		public int TotalDataSize
		{
			get
			{
				return this.totalWritten;
			}
		}

		// Token: 0x170006B5 RID: 1717
		// (get) Token: 0x06001F49 RID: 8009 RVA: 0x0007190D File Offset: 0x0006FB0D
		public int ChunkLeft
		{
			get
			{
				return this.chunkSize - this.chunkRead;
			}
		}

		// Token: 0x06001F4A RID: 8010 RVA: 0x0007191C File Offset: 0x0006FB1C
		private MonoChunkStream.State ReadBody(byte[] buffer, ref int offset, int size)
		{
			if (this.chunkSize == 0)
			{
				return MonoChunkStream.State.BodyFinished;
			}
			int num = size - offset;
			if (num + this.chunkRead > this.chunkSize)
			{
				num = this.chunkSize - this.chunkRead;
			}
			byte[] array = new byte[num];
			Buffer.BlockCopy(buffer, offset, array, 0, num);
			this.chunks.Add(new MonoChunkStream.Chunk(array));
			offset += num;
			this.chunkRead += num;
			this.totalWritten += num;
			if (this.chunkRead != this.chunkSize)
			{
				return MonoChunkStream.State.Body;
			}
			return MonoChunkStream.State.BodyFinished;
		}

		// Token: 0x06001F4B RID: 8011 RVA: 0x000719B0 File Offset: 0x0006FBB0
		private MonoChunkStream.State GetChunkSize(byte[] buffer, ref int offset, int size)
		{
			this.chunkRead = 0;
			this.chunkSize = 0;
			char c = '\0';
			while (offset < size)
			{
				int num = offset;
				offset = num + 1;
				c = (char)buffer[num];
				if (c == '\r')
				{
					if (this.sawCR)
					{
						MonoChunkStream.ThrowProtocolViolation("2 CR found");
					}
					this.sawCR = true;
				}
				else
				{
					if (this.sawCR && c == '\n')
					{
						break;
					}
					if (c == ' ')
					{
						this.gotit = true;
					}
					if (!this.gotit)
					{
						this.saved.Append(c);
					}
					if (this.saved.Length > 20)
					{
						MonoChunkStream.ThrowProtocolViolation("chunk size too long.");
					}
				}
			}
			if (!this.sawCR || c != '\n')
			{
				if (offset < size)
				{
					MonoChunkStream.ThrowProtocolViolation("Missing \\n");
				}
				try
				{
					if (this.saved.Length > 0)
					{
						this.chunkSize = int.Parse(MonoChunkStream.RemoveChunkExtension(this.saved.ToString()), NumberStyles.HexNumber);
					}
				}
				catch (Exception)
				{
					MonoChunkStream.ThrowProtocolViolation("Cannot parse chunk size.");
				}
				return MonoChunkStream.State.PartialSize;
			}
			this.chunkRead = 0;
			try
			{
				this.chunkSize = int.Parse(MonoChunkStream.RemoveChunkExtension(this.saved.ToString()), NumberStyles.HexNumber);
			}
			catch (Exception)
			{
				MonoChunkStream.ThrowProtocolViolation("Cannot parse chunk size.");
			}
			if (this.chunkSize == 0)
			{
				this.trailerState = 2;
				return MonoChunkStream.State.Trailer;
			}
			return MonoChunkStream.State.Body;
		}

		// Token: 0x06001F4C RID: 8012 RVA: 0x00071B08 File Offset: 0x0006FD08
		private static string RemoveChunkExtension(string input)
		{
			int num = input.IndexOf(';');
			if (num == -1)
			{
				return input;
			}
			return input.Substring(0, num);
		}

		// Token: 0x06001F4D RID: 8013 RVA: 0x00071B2C File Offset: 0x0006FD2C
		private MonoChunkStream.State ReadCRLF(byte[] buffer, ref int offset, int size)
		{
			if (!this.sawCR)
			{
				int num = offset;
				offset = num + 1;
				if (buffer[num] != 13)
				{
					MonoChunkStream.ThrowProtocolViolation("Expecting \\r");
				}
				this.sawCR = true;
				if (offset == size)
				{
					return MonoChunkStream.State.BodyFinished;
				}
			}
			if (this.sawCR)
			{
				int num = offset;
				offset = num + 1;
				if (buffer[num] != 10)
				{
					MonoChunkStream.ThrowProtocolViolation("Expecting \\n");
				}
			}
			return MonoChunkStream.State.None;
		}

		// Token: 0x06001F4E RID: 8014 RVA: 0x00071B8C File Offset: 0x0006FD8C
		private MonoChunkStream.State ReadTrailer(byte[] buffer, ref int offset, int size)
		{
			if (this.trailerState == 2 && buffer[offset] == 13 && this.saved.Length == 0)
			{
				offset++;
				if (offset < size && buffer[offset] == 10)
				{
					offset++;
					return MonoChunkStream.State.None;
				}
				offset--;
			}
			int num = this.trailerState;
			string text = "\r\n\r";
			while (offset < size && num < 4)
			{
				int num2 = offset;
				offset = num2 + 1;
				char c = (char)buffer[num2];
				if ((num == 0 || num == 2) && c == '\r')
				{
					num++;
				}
				else if ((num == 1 || num == 3) && c == '\n')
				{
					num++;
				}
				else if (num > 0)
				{
					this.saved.Append(text.Substring(0, (this.saved.Length == 0) ? (num - 2) : num));
					num = 0;
					if (this.saved.Length > 4196)
					{
						MonoChunkStream.ThrowProtocolViolation("Error reading trailer (too long).");
					}
				}
			}
			if (num < 4)
			{
				this.trailerState = num;
				if (offset < size)
				{
					MonoChunkStream.ThrowProtocolViolation("Error reading trailer.");
				}
				return MonoChunkStream.State.Trailer;
			}
			StringReader stringReader = new StringReader(this.saved.ToString());
			string text2;
			while ((text2 = stringReader.ReadLine()) != null && text2 != "")
			{
				this.headers.Add(text2);
			}
			return MonoChunkStream.State.None;
		}

		// Token: 0x06001F4F RID: 8015 RVA: 0x00071CC7 File Offset: 0x0006FEC7
		private static void ThrowProtocolViolation(string message)
		{
			throw new WebException(message, null, WebExceptionStatus.ServerProtocolViolation, null);
		}

		// Token: 0x04001AC6 RID: 6854
		internal WebHeaderCollection headers;

		// Token: 0x04001AC7 RID: 6855
		private int chunkSize;

		// Token: 0x04001AC8 RID: 6856
		private int chunkRead;

		// Token: 0x04001AC9 RID: 6857
		private int totalWritten;

		// Token: 0x04001ACA RID: 6858
		private MonoChunkStream.State state;

		// Token: 0x04001ACB RID: 6859
		private StringBuilder saved;

		// Token: 0x04001ACC RID: 6860
		private bool sawCR;

		// Token: 0x04001ACD RID: 6861
		private bool gotit;

		// Token: 0x04001ACE RID: 6862
		private int trailerState;

		// Token: 0x04001ACF RID: 6863
		private ArrayList chunks;

		// Token: 0x020003F8 RID: 1016
		private enum State
		{
			// Token: 0x04001AD1 RID: 6865
			None,
			// Token: 0x04001AD2 RID: 6866
			PartialSize,
			// Token: 0x04001AD3 RID: 6867
			Body,
			// Token: 0x04001AD4 RID: 6868
			BodyFinished,
			// Token: 0x04001AD5 RID: 6869
			Trailer
		}

		// Token: 0x020003F9 RID: 1017
		private class Chunk
		{
			// Token: 0x06001F50 RID: 8016 RVA: 0x00071CD3 File Offset: 0x0006FED3
			public Chunk(byte[] chunk)
			{
				this.Bytes = chunk;
			}

			// Token: 0x06001F51 RID: 8017 RVA: 0x00071CE4 File Offset: 0x0006FEE4
			public int Read(byte[] buffer, int offset, int size)
			{
				int num = (size > this.Bytes.Length - this.Offset) ? (this.Bytes.Length - this.Offset) : size;
				Buffer.BlockCopy(this.Bytes, this.Offset, buffer, offset, num);
				this.Offset += num;
				return num;
			}

			// Token: 0x04001AD6 RID: 6870
			public byte[] Bytes;

			// Token: 0x04001AD7 RID: 6871
			public int Offset;
		}
	}
}
