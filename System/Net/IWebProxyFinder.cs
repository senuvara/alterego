﻿using System;
using System.Collections.Generic;

namespace System.Net
{
	// Token: 0x020002F7 RID: 759
	internal interface IWebProxyFinder : IDisposable
	{
		// Token: 0x0600167B RID: 5755
		bool GetProxies(Uri destination, out IList<string> proxyList);

		// Token: 0x0600167C RID: 5756
		void Abort();

		// Token: 0x0600167D RID: 5757
		void Reset();

		// Token: 0x1700049B RID: 1179
		// (get) Token: 0x0600167E RID: 5758
		bool IsValid { get; }
	}
}
