﻿using System;
using System.IO;

namespace System.Net
{
	// Token: 0x02000408 RID: 1032
	internal class WebAsyncResult : SimpleAsyncResult
	{
		// Token: 0x06002018 RID: 8216 RVA: 0x00074110 File Offset: 0x00072310
		public WebAsyncResult(AsyncCallback cb, object state) : base(cb, state)
		{
		}

		// Token: 0x06002019 RID: 8217 RVA: 0x0007411A File Offset: 0x0007231A
		public WebAsyncResult(HttpWebRequest request, AsyncCallback cb, object state) : base(cb, state)
		{
			this.AsyncObject = request;
		}

		// Token: 0x0600201A RID: 8218 RVA: 0x0007412B File Offset: 0x0007232B
		public WebAsyncResult(AsyncCallback cb, object state, byte[] buffer, int offset, int size) : base(cb, state)
		{
			this.buffer = buffer;
			this.offset = offset;
			this.size = size;
		}

		// Token: 0x0600201B RID: 8219 RVA: 0x0007414C File Offset: 0x0007234C
		internal void Reset()
		{
			this.nbytes = 0;
			this.response = null;
			this.buffer = null;
			this.offset = 0;
			this.size = 0;
			base.Reset_internal();
		}

		// Token: 0x0600201C RID: 8220 RVA: 0x00074177 File Offset: 0x00072377
		internal void SetCompleted(bool synch, int nbytes)
		{
			this.nbytes = nbytes;
			base.SetCompleted_internal(synch);
		}

		// Token: 0x0600201D RID: 8221 RVA: 0x00074187 File Offset: 0x00072387
		internal void SetCompleted(bool synch, Stream writeStream)
		{
			this.writeStream = writeStream;
			base.SetCompleted_internal(synch);
		}

		// Token: 0x0600201E RID: 8222 RVA: 0x00074197 File Offset: 0x00072397
		internal void SetCompleted(bool synch, HttpWebResponse response)
		{
			this.response = response;
			base.SetCompleted_internal(synch);
		}

		// Token: 0x0600201F RID: 8223 RVA: 0x000741A7 File Offset: 0x000723A7
		internal void DoCallback()
		{
			base.DoCallback_internal();
		}

		// Token: 0x170006F6 RID: 1782
		// (get) Token: 0x06002020 RID: 8224 RVA: 0x000741AF File Offset: 0x000723AF
		// (set) Token: 0x06002021 RID: 8225 RVA: 0x000741B7 File Offset: 0x000723B7
		internal int NBytes
		{
			get
			{
				return this.nbytes;
			}
			set
			{
				this.nbytes = value;
			}
		}

		// Token: 0x170006F7 RID: 1783
		// (get) Token: 0x06002022 RID: 8226 RVA: 0x000741C0 File Offset: 0x000723C0
		// (set) Token: 0x06002023 RID: 8227 RVA: 0x000741C8 File Offset: 0x000723C8
		internal IAsyncResult InnerAsyncResult
		{
			get
			{
				return this.innerAsyncResult;
			}
			set
			{
				this.innerAsyncResult = value;
			}
		}

		// Token: 0x170006F8 RID: 1784
		// (get) Token: 0x06002024 RID: 8228 RVA: 0x000741D1 File Offset: 0x000723D1
		internal Stream WriteStream
		{
			get
			{
				return this.writeStream;
			}
		}

		// Token: 0x170006F9 RID: 1785
		// (get) Token: 0x06002025 RID: 8229 RVA: 0x000741D9 File Offset: 0x000723D9
		internal HttpWebResponse Response
		{
			get
			{
				return this.response;
			}
		}

		// Token: 0x170006FA RID: 1786
		// (get) Token: 0x06002026 RID: 8230 RVA: 0x000741E1 File Offset: 0x000723E1
		internal byte[] Buffer
		{
			get
			{
				return this.buffer;
			}
		}

		// Token: 0x170006FB RID: 1787
		// (get) Token: 0x06002027 RID: 8231 RVA: 0x000741E9 File Offset: 0x000723E9
		internal int Offset
		{
			get
			{
				return this.offset;
			}
		}

		// Token: 0x170006FC RID: 1788
		// (get) Token: 0x06002028 RID: 8232 RVA: 0x000741F1 File Offset: 0x000723F1
		internal int Size
		{
			get
			{
				return this.size;
			}
		}

		// Token: 0x04001B29 RID: 6953
		private int nbytes;

		// Token: 0x04001B2A RID: 6954
		private IAsyncResult innerAsyncResult;

		// Token: 0x04001B2B RID: 6955
		private HttpWebResponse response;

		// Token: 0x04001B2C RID: 6956
		private Stream writeStream;

		// Token: 0x04001B2D RID: 6957
		private byte[] buffer;

		// Token: 0x04001B2E RID: 6958
		private int offset;

		// Token: 0x04001B2F RID: 6959
		private int size;

		// Token: 0x04001B30 RID: 6960
		public bool EndCalled;

		// Token: 0x04001B31 RID: 6961
		public bool AsyncWriteAll;

		// Token: 0x04001B32 RID: 6962
		public HttpWebRequest AsyncObject;
	}
}
