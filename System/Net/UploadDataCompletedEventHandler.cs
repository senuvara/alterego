﻿using System;

namespace System.Net
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Net.WebClient.UploadDataCompleted" /> event of a <see cref="T:System.Net.WebClient" />.</summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="T:System.Net.UploadDataCompletedEventArgs" /> containing event data.</param>
	// Token: 0x020003A9 RID: 937
	// (Invoke) Token: 0x06001BA5 RID: 7077
	public delegate void UploadDataCompletedEventHandler(object sender, UploadDataCompletedEventArgs e);
}
