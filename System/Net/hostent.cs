﻿using System;
using System.Runtime.InteropServices;

namespace System.Net
{
	// Token: 0x0200031C RID: 796
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	internal struct hostent
	{
		// Token: 0x04001656 RID: 5718
		public IntPtr h_name;

		// Token: 0x04001657 RID: 5719
		public IntPtr h_aliases;

		// Token: 0x04001658 RID: 5720
		public short h_addrtype;

		// Token: 0x04001659 RID: 5721
		public short h_length;

		// Token: 0x0400165A RID: 5722
		public IntPtr h_addr_list;
	}
}
