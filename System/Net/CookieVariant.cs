﻿using System;

namespace System.Net
{
	// Token: 0x02000372 RID: 882
	internal enum CookieVariant
	{
		// Token: 0x040017F5 RID: 6133
		Unknown,
		// Token: 0x040017F6 RID: 6134
		Plain,
		// Token: 0x040017F7 RID: 6135
		Rfc2109,
		// Token: 0x040017F8 RID: 6136
		Rfc2965,
		// Token: 0x040017F9 RID: 6137
		Default = 2
	}
}
