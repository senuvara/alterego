﻿using System;

namespace System.Net
{
	// Token: 0x0200032A RID: 810
	internal enum WebHeaderCollectionType : ushort
	{
		// Token: 0x04001697 RID: 5783
		Unknown,
		// Token: 0x04001698 RID: 5784
		WebRequest,
		// Token: 0x04001699 RID: 5785
		WebResponse,
		// Token: 0x0400169A RID: 5786
		HttpWebRequest,
		// Token: 0x0400169B RID: 5787
		HttpWebResponse,
		// Token: 0x0400169C RID: 5788
		HttpListenerRequest,
		// Token: 0x0400169D RID: 5789
		HttpListenerResponse,
		// Token: 0x0400169E RID: 5790
		FtpWebRequest,
		// Token: 0x0400169F RID: 5791
		FtpWebResponse,
		// Token: 0x040016A0 RID: 5792
		FileWebRequest,
		// Token: 0x040016A1 RID: 5793
		FileWebResponse
	}
}
