﻿using System;

namespace System.Net
{
	// Token: 0x0200035A RID: 858
	internal class DirectProxy : ProxyChain
	{
		// Token: 0x060018C3 RID: 6339 RVA: 0x00058EF6 File Offset: 0x000570F6
		internal DirectProxy(Uri destination) : base(destination)
		{
		}

		// Token: 0x060018C4 RID: 6340 RVA: 0x00058EFF File Offset: 0x000570FF
		protected override bool GetNextProxy(out Uri proxy)
		{
			proxy = null;
			if (this.m_ProxyRetrieved)
			{
				return false;
			}
			this.m_ProxyRetrieved = true;
			return true;
		}

		// Token: 0x0400179E RID: 6046
		private bool m_ProxyRetrieved;
	}
}
