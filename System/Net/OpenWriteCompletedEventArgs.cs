﻿using System;
using System.ComponentModel;
using System.IO;
using Unity;

namespace System.Net
{
	/// <summary>Provides data for the <see cref="E:System.Net.WebClient.OpenWriteCompleted" /> event.</summary>
	// Token: 0x020003A2 RID: 930
	public class OpenWriteCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06001B8C RID: 7052 RVA: 0x000649BC File Offset: 0x00062BBC
		internal OpenWriteCompletedEventArgs(Stream result, Exception exception, bool cancelled, object userToken) : base(exception, cancelled, userToken)
		{
			this.m_Result = result;
		}

		/// <summary>Gets a writable stream that is used to send data to a server.</summary>
		/// <returns>A <see cref="T:System.IO.Stream" /> where you can write data to be uploaded.</returns>
		// Token: 0x1700059D RID: 1437
		// (get) Token: 0x06001B8D RID: 7053 RVA: 0x000649CF File Offset: 0x00062BCF
		public Stream Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return this.m_Result;
			}
		}

		// Token: 0x06001B8E RID: 7054 RVA: 0x000092E2 File Offset: 0x000074E2
		internal OpenWriteCompletedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400191F RID: 6431
		private Stream m_Result;
	}
}
