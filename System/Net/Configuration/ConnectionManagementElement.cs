﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the maximum number of connections to a remote computer. This class cannot be inherited.</summary>
	// Token: 0x02000636 RID: 1590
	public sealed class ConnectionManagementElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.ConnectionManagementElement" /> class. </summary>
		// Token: 0x060032F5 RID: 13045 RVA: 0x000092E2 File Offset: 0x000074E2
		public ConnectionManagementElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.ConnectionManagementElement" /> class with the specified address and connection limit information.</summary>
		/// <param name="address">A string that identifies the address of a remote computer.</param>
		/// <param name="maxConnection">An integer that identifies the maximum number of connections allowed to <paramref name="address" /> from the local computer.</param>
		// Token: 0x060032F6 RID: 13046 RVA: 0x000092E2 File Offset: 0x000074E2
		public ConnectionManagementElement(string address, int maxConnection)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the address for remote computers.</summary>
		/// <returns>A string that contains a regular expression describing an IP address or DNS name.</returns>
		// Token: 0x17000CAF RID: 3247
		// (get) Token: 0x060032F7 RID: 13047 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060032F8 RID: 13048 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Address
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the maximum number of connections that can be made to a remote computer.</summary>
		/// <returns>An integer that specifies the maximum number of connections.</returns>
		// Token: 0x17000CB0 RID: 3248
		// (get) Token: 0x060032F9 RID: 13049 RVA: 0x000A4BB8 File Offset: 0x000A2DB8
		// (set) Token: 0x060032FA RID: 13050 RVA: 0x000092E2 File Offset: 0x000074E2
		public int MaxConnection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CB1 RID: 3249
		// (get) Token: 0x060032FB RID: 13051 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
