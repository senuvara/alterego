﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents information used to configure Web proxy scripts. This class cannot be inherited.</summary>
	// Token: 0x0200064F RID: 1615
	public sealed class WebProxyScriptElement : ConfigurationElement
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Net.Configuration.WebProxyScriptElement" /> class.</summary>
		// Token: 0x060033B6 RID: 13238 RVA: 0x000092E2 File Offset: 0x000074E2
		public WebProxyScriptElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the Web proxy script download timeout using the format hours:minutes:seconds.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> object that contains the timeout value. The default download timeout is one minute.</returns>
		// Token: 0x17000D12 RID: 3346
		// (get) Token: 0x060033B7 RID: 13239 RVA: 0x000A5088 File Offset: 0x000A3288
		// (set) Token: 0x060033B8 RID: 13240 RVA: 0x000092E2 File Offset: 0x000074E2
		public TimeSpan DownloadTimeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TimeSpan);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000D13 RID: 3347
		// (get) Token: 0x060033B9 RID: 13241 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x060033BA RID: 13242 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
