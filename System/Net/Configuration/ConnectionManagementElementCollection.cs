﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents a container for connection management configuration elements. This class cannot be inherited.</summary>
	// Token: 0x02000637 RID: 1591
	[ConfigurationCollection(typeof(ConnectionManagementElement))]
	public sealed class ConnectionManagementElementCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.ConnectionManagementElementCollection" /> class. </summary>
		// Token: 0x060032FC RID: 13052 RVA: 0x000092E2 File Offset: 0x000074E2
		public ConnectionManagementElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060032FD RID: 13053 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ConnectionManagementElement get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060032FE RID: 13054 RVA: 0x000092E2 File Offset: 0x000074E2
		public void set_Item(int index, ConnectionManagementElement value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the element with the specified key.</summary>
		/// <param name="name">The key for an element in the collection. </param>
		/// <returns>The <see cref="T:System.Net.Configuration.ConnectionManagementElement" /> with the specified key or <see langword="null" /> if there is no element with the specified key.</returns>
		// Token: 0x17000CB2 RID: 3250
		public ConnectionManagementElement this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds an element to the collection.</summary>
		/// <param name="element">The <see cref="T:System.Net.Configuration.ConnectionManagementElement" /> to add to the collection.</param>
		// Token: 0x06003301 RID: 13057 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Add(ConnectionManagementElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all elements from the collection.</summary>
		// Token: 0x06003302 RID: 13058 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003303 RID: 13059 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06003304 RID: 13060 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the index of the specified configuration element.</summary>
		/// <param name="element">A <see cref="T:System.Net.Configuration.ConnectionManagementElement" />.</param>
		/// <returns>The zero-based index of <paramref name="element" />.</returns>
		// Token: 0x06003305 RID: 13061 RVA: 0x000A4BD4 File Offset: 0x000A2DD4
		public int IndexOf(ConnectionManagementElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Removes the specified configuration element from the collection.</summary>
		/// <param name="element">The <see cref="T:System.Net.Configuration.ConnectionManagementElement" /> to remove.</param>
		// Token: 0x06003306 RID: 13062 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(ConnectionManagementElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element with the specified key.</summary>
		/// <param name="name">The key of the element to remove.</param>
		// Token: 0x06003307 RID: 13063 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element at the specified index.</summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		// Token: 0x06003308 RID: 13064 RVA: 0x000092E2 File Offset: 0x000074E2
		public void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
