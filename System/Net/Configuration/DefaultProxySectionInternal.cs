﻿using System;
using System.Threading;

namespace System.Net.Configuration
{
	// Token: 0x0200052D RID: 1325
	internal sealed class DefaultProxySectionInternal
	{
		// Token: 0x060029F4 RID: 10740 RVA: 0x00091C6E File Offset: 0x0008FE6E
		private static IWebProxy GetDefaultProxy_UsingOldMonoCode()
		{
			return DefaultProxySectionInternal.GetSystemWebProxy();
		}

		// Token: 0x060029F5 RID: 10741 RVA: 0x00056606 File Offset: 0x00054806
		private static IWebProxy GetSystemWebProxy()
		{
			return System.Net.WebProxy.CreateDefaultProxy();
		}

		// Token: 0x17000A68 RID: 2664
		// (get) Token: 0x060029F6 RID: 10742 RVA: 0x00091C78 File Offset: 0x0008FE78
		internal static object ClassSyncObject
		{
			get
			{
				if (DefaultProxySectionInternal.classSyncObject == null)
				{
					object value = new object();
					Interlocked.CompareExchange(ref DefaultProxySectionInternal.classSyncObject, value, null);
				}
				return DefaultProxySectionInternal.classSyncObject;
			}
		}

		// Token: 0x060029F7 RID: 10743 RVA: 0x00091CA4 File Offset: 0x0008FEA4
		internal static DefaultProxySectionInternal GetSection()
		{
			object obj = DefaultProxySectionInternal.ClassSyncObject;
			DefaultProxySectionInternal result;
			lock (obj)
			{
				result = new DefaultProxySectionInternal
				{
					webProxy = DefaultProxySectionInternal.GetDefaultProxy_UsingOldMonoCode()
				};
			}
			return result;
		}

		// Token: 0x17000A69 RID: 2665
		// (get) Token: 0x060029F8 RID: 10744 RVA: 0x00091CF0 File Offset: 0x0008FEF0
		internal IWebProxy WebProxy
		{
			get
			{
				return this.webProxy;
			}
		}

		// Token: 0x060029F9 RID: 10745 RVA: 0x0000232F File Offset: 0x0000052F
		public DefaultProxySectionInternal()
		{
		}

		// Token: 0x0400219C RID: 8604
		private IWebProxy webProxy;

		// Token: 0x0400219D RID: 8605
		private static object classSyncObject;
	}
}
