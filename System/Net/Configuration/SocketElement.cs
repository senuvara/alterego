﻿using System;
using System.Configuration;
using System.Net.Sockets;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents information used to configure <see cref="T:System.Net.Sockets.Socket" /> objects. This class cannot be inherited.</summary>
	// Token: 0x0200064E RID: 1614
	public sealed class SocketElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.SocketElement" /> class. </summary>
		// Token: 0x060033AD RID: 13229 RVA: 0x000092E2 File Offset: 0x000074E2
		public SocketElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a Boolean value that specifies whether completion ports are used when accepting connections.</summary>
		/// <returns>
		///     <see langword="true" /> to use completion ports; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000D0E RID: 3342
		// (get) Token: 0x060033AE RID: 13230 RVA: 0x000A5034 File Offset: 0x000A3234
		// (set) Token: 0x060033AF RID: 13231 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool AlwaysUseCompletionPortsForAccept
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a Boolean value that specifies whether completion ports are used when making connections.</summary>
		/// <returns>
		///     <see langword="true" /> to use completion ports; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000D0F RID: 3343
		// (get) Token: 0x060033B0 RID: 13232 RVA: 0x000A5050 File Offset: 0x000A3250
		// (set) Token: 0x060033B1 RID: 13233 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool AlwaysUseCompletionPortsForConnect
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that specifies the default <see cref="T:System.Net.Sockets.IPProtectionLevel" /> to use for a socket.</summary>
		/// <returns>The value of the <see cref="T:System.Net.Sockets.IPProtectionLevel" /> for the current instance.</returns>
		// Token: 0x17000D10 RID: 3344
		// (get) Token: 0x060033B2 RID: 13234 RVA: 0x000A506C File Offset: 0x000A326C
		// (set) Token: 0x060033B3 RID: 13235 RVA: 0x000092E2 File Offset: 0x000074E2
		public IPProtectionLevel IPProtectionLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (IPProtectionLevel)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000D11 RID: 3345
		// (get) Token: 0x060033B4 RID: 13236 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x060033B5 RID: 13237 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
