﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents a container for the addresses of resources that bypass the proxy server. This class cannot be inherited.</summary>
	// Token: 0x02000635 RID: 1589
	[ConfigurationCollection(typeof(BypassElement))]
	public sealed class BypassElementCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes an empty instance of the <see cref="T:System.Net.Configuration.BypassElementCollection" /> class. </summary>
		// Token: 0x060032E7 RID: 13031 RVA: 0x000092E2 File Offset: 0x000074E2
		public BypassElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060032E8 RID: 13032 RVA: 0x00043C3C File Offset: 0x00041E3C
		public BypassElement get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060032E9 RID: 13033 RVA: 0x000092E2 File Offset: 0x000074E2
		public void set_Item(int index, BypassElement value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the element with the specified key.</summary>
		/// <param name="name">The key for an element in the collection. </param>
		/// <returns>The <see cref="T:System.Net.Configuration.BypassElement" /> with the specified key, or <see langword="null" /> if there is no element with the specified key.</returns>
		// Token: 0x17000CAD RID: 3245
		public BypassElement this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CAE RID: 3246
		// (get) Token: 0x060032EC RID: 13036 RVA: 0x000A4B80 File Offset: 0x000A2D80
		protected override bool ThrowOnDuplicate
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Adds an element to the collection.</summary>
		/// <param name="element">The <see cref="T:System.Net.Configuration.BypassElement" /> to add to the collection.</param>
		// Token: 0x060032ED RID: 13037 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Add(BypassElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all elements from the collection.</summary>
		// Token: 0x060032EE RID: 13038 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060032EF RID: 13039 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060032F0 RID: 13040 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the index of the specified configuration element.</summary>
		/// <param name="element">A <see cref="T:System.Net.Configuration.BypassElement" />.</param>
		/// <returns>The zero-based index of <paramref name="element" />.</returns>
		// Token: 0x060032F1 RID: 13041 RVA: 0x000A4B9C File Offset: 0x000A2D9C
		public int IndexOf(BypassElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Removes the specified configuration element from the collection.</summary>
		/// <param name="element">The <see cref="T:System.Net.Configuration.BypassElement" /> to remove.</param>
		// Token: 0x060032F2 RID: 13042 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(BypassElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element with the specified key.</summary>
		/// <param name="name">The key of the element to remove.</param>
		// Token: 0x060032F3 RID: 13043 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element at the specified index.</summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		// Token: 0x060032F4 RID: 13044 RVA: 0x000092E2 File Offset: 0x000074E2
		public void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
