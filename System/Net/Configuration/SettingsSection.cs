﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the configuration section for sockets, IPv6, response headers, and service points. This class cannot be inherited.</summary>
	// Token: 0x0200064B RID: 1611
	public sealed class SettingsSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.ConnectionManagementSection" /> class. </summary>
		// Token: 0x0600338E RID: 13198 RVA: 0x000092E2 File Offset: 0x000074E2
		public SettingsSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the configuration element that controls the settings used by an <see cref="T:System.Net.HttpListener" /> object.</summary>
		/// <returns>An <see cref="T:System.Net.Configuration.HttpListenerElement" /> object.The configuration element that controls the settings used by an <see cref="T:System.Net.HttpListener" /> object.</returns>
		// Token: 0x17000CFB RID: 3323
		// (get) Token: 0x0600338F RID: 13199 RVA: 0x00043C3C File Offset: 0x00041E3C
		public HttpListenerElement HttpListener
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration element that controls the settings used by an <see cref="T:System.Net.HttpWebRequest" /> object.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.HttpWebRequestElement" /> object.The configuration element that controls the maximum response header length and other settings used by an <see cref="T:System.Net.HttpWebRequest" /> object.</returns>
		// Token: 0x17000CFC RID: 3324
		// (get) Token: 0x06003390 RID: 13200 RVA: 0x00043C3C File Offset: 0x00041E3C
		public HttpWebRequestElement HttpWebRequest
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration element that enables Internet Protocol version 6 (IPv6).</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.Ipv6Element" />.The configuration element that controls setting used by IPv6.</returns>
		// Token: 0x17000CFD RID: 3325
		// (get) Token: 0x06003391 RID: 13201 RVA: 0x00043C3C File Offset: 0x00041E3C
		public Ipv6Element Ipv6
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration element that controls whether network performance counters are enabled.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.PerformanceCountersElement" />.The configuration element that controls setting used network performance counters.</returns>
		// Token: 0x17000CFE RID: 3326
		// (get) Token: 0x06003392 RID: 13202 RVA: 0x00043C3C File Offset: 0x00041E3C
		public PerformanceCountersElement PerformanceCounters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000CFF RID: 3327
		// (get) Token: 0x06003393 RID: 13203 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration element that controls settings for connections to remote host computers.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.ServicePointManagerElement" /> object.The configuration element that that controls setting used network performance counters for connections to remote host computers.</returns>
		// Token: 0x17000D00 RID: 3328
		// (get) Token: 0x06003394 RID: 13204 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ServicePointManagerElement ServicePointManager
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration element that controls settings for sockets.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.SocketElement" /> object.The configuration element that controls settings for sockets.</returns>
		// Token: 0x17000D01 RID: 3329
		// (get) Token: 0x06003395 RID: 13205 RVA: 0x00043C3C File Offset: 0x00041E3C
		public SocketElement Socket
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration element that controls the execution timeout and download timeout of Web proxy scripts.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.WebProxyScriptElement" /> object.The configuration element that controls settings for the execution timeout and download timeout used by the Web proxy scripts.</returns>
		// Token: 0x17000D02 RID: 3330
		// (get) Token: 0x06003396 RID: 13206 RVA: 0x00043C3C File Offset: 0x00041E3C
		public WebProxyScriptElement WebProxyScript
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration element that controls the settings used by an <see cref="T:System.Net.WebUtility" /> object.</summary>
		/// <returns>Returns <see cref="T:System.Net.Configuration.WebUtilityElement" />.The configuration element that controls the settings used by an <see cref="T:System.Net.WebUtility" /> object.</returns>
		// Token: 0x17000D03 RID: 3331
		// (get) Token: 0x06003397 RID: 13207 RVA: 0x00043C3C File Offset: 0x00041E3C
		public WebUtilityElement WebUtility
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
