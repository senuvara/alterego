﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the network element in the SMTP configuration file. This class cannot be inherited.</summary>
	// Token: 0x02000647 RID: 1607
	public sealed class SmtpNetworkElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.SmtpNetworkElement" /> class.</summary>
		// Token: 0x06003360 RID: 13152 RVA: 0x000092E2 File Offset: 0x000074E2
		public SmtpNetworkElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the client domain name used in the initial SMTP protocol request to connect to an SMTP mail server.</summary>
		/// <returns>A string that represents the client domain name used in the initial SMTP protocol request to connect to an SMTP mail server.</returns>
		// Token: 0x17000CE2 RID: 3298
		// (get) Token: 0x06003361 RID: 13153 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003362 RID: 13154 RVA: 0x000092E2 File Offset: 0x000074E2
		public string ClientDomain
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Determines whether or not default user credentials are used to access an SMTP server. The default value is <see langword="false" />.</summary>
		/// <returns>
		///     <see langword="true" /> indicates that default user credentials will be used to access the SMTP server; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CE3 RID: 3299
		// (get) Token: 0x06003363 RID: 13155 RVA: 0x000A4E90 File Offset: 0x000A3090
		// (set) Token: 0x06003364 RID: 13156 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool DefaultCredentials
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets whether SSL is used to access an SMTP mail server. The default value is <see langword="false" />.</summary>
		/// <returns>
		///     <see langword="true" /> indicates that SSL will be used to access the SMTP mail server; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CE4 RID: 3300
		// (get) Token: 0x06003365 RID: 13157 RVA: 0x000A4EAC File Offset: 0x000A30AC
		// (set) Token: 0x06003366 RID: 13158 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool EnableSsl
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the SMTP server.</summary>
		/// <returns>A string that represents the name of the SMTP server to connect to.</returns>
		// Token: 0x17000CE5 RID: 3301
		// (get) Token: 0x06003367 RID: 13159 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003368 RID: 13160 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Host
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the user password to use to connect to an SMTP mail server.</summary>
		/// <returns>A string that represents the password to use to connect to an SMTP mail server.</returns>
		// Token: 0x17000CE6 RID: 3302
		// (get) Token: 0x06003369 RID: 13161 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600336A RID: 13162 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Password
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the port that SMTP clients use to connect to an SMTP mail server. The default value is 25.</summary>
		/// <returns>A string that represents the port to connect to an SMTP mail server.</returns>
		// Token: 0x17000CE7 RID: 3303
		// (get) Token: 0x0600336B RID: 13163 RVA: 0x000A4EC8 File Offset: 0x000A30C8
		// (set) Token: 0x0600336C RID: 13164 RVA: 0x000092E2 File Offset: 0x000074E2
		public int Port
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CE8 RID: 3304
		// (get) Token: 0x0600336D RID: 13165 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the Service Provider Name (SPN) to use for authentication when using extended protection to connect to an SMTP mail server.</summary>
		/// <returns>A string that represents the SPN to use for authentication when using extended protection to connect to an SMTP mail server.</returns>
		// Token: 0x17000CE9 RID: 3305
		// (get) Token: 0x0600336E RID: 13166 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600336F RID: 13167 RVA: 0x000092E2 File Offset: 0x000074E2
		public string TargetName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the user name to connect to an SMTP mail server.</summary>
		/// <returns>A string that represents the user name to connect to an SMTP mail server.</returns>
		// Token: 0x17000CEA RID: 3306
		// (get) Token: 0x06003370 RID: 13168 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003371 RID: 13169 RVA: 0x000092E2 File Offset: 0x000074E2
		public string UserName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x06003372 RID: 13170 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
