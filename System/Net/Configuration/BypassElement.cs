﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the address information for resources that are not retrieved using a proxy server. This class cannot be inherited.</summary>
	// Token: 0x02000634 RID: 1588
	public sealed class BypassElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.BypassElement" /> class. </summary>
		// Token: 0x060032E2 RID: 13026 RVA: 0x000092E2 File Offset: 0x000074E2
		public BypassElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.BypassElement" /> class with the specified type information.</summary>
		/// <param name="address">A string that identifies the address of a resource.</param>
		// Token: 0x060032E3 RID: 13027 RVA: 0x000092E2 File Offset: 0x000074E2
		public BypassElement(string address)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the addresses of resources that bypass the proxy server.</summary>
		/// <returns>A string that identifies a resource.</returns>
		// Token: 0x17000CAB RID: 3243
		// (get) Token: 0x060032E4 RID: 13028 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060032E5 RID: 13029 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Address
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CAC RID: 3244
		// (get) Token: 0x060032E6 RID: 13030 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
