﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the HttpListener element in the configuration file. This class cannot be inherited.</summary>
	// Token: 0x02000641 RID: 1601
	public sealed class HttpListenerElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.HttpListenerElement" /> class.</summary>
		// Token: 0x06003339 RID: 13113 RVA: 0x000092E2 File Offset: 0x000074E2
		public HttpListenerElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x17000CCA RID: 3274
		// (get) Token: 0x0600333A RID: 13114 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the default timeout elements used for an <see cref="T:System.Net.HttpListener" /> object.</summary>
		/// <returns>Returns <see cref="T:System.Net.Configuration.HttpListenerTimeoutsElement" />.The timeout elements used for an <see cref="T:System.Net.HttpListener" /> object.</returns>
		// Token: 0x17000CCB RID: 3275
		// (get) Token: 0x0600333B RID: 13115 RVA: 0x00043C3C File Offset: 0x00041E3C
		public HttpListenerTimeoutsElement Timeouts
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value that indicates if <see cref="T:System.Net.HttpListener" /> uses the raw unescaped URI instead of the converted URI.</summary>
		/// <returns>A Boolean value that indicates if <see cref="T:System.Net.HttpListener" /> uses the raw unescaped URI, rather than the converted URI.</returns>
		// Token: 0x17000CCC RID: 3276
		// (get) Token: 0x0600333C RID: 13116 RVA: 0x000A4D08 File Offset: 0x000A2F08
		public bool UnescapeRequestUrl
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
