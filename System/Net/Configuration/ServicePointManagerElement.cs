﻿using System;
using System.Configuration;
using System.Net.Security;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the default settings used to create connections to a remote computer. This class cannot be inherited.</summary>
	// Token: 0x0200064D RID: 1613
	public sealed class ServicePointManagerElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.ServicePointManagerElement" /> class. </summary>
		// Token: 0x0600339C RID: 13212 RVA: 0x000092E2 File Offset: 0x000074E2
		public ServicePointManagerElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a Boolean value that controls checking host name information in an X509 certificate.</summary>
		/// <returns>
		///     <see langword="true" /> to specify host name checking; otherwise, <see langword="false" />. </returns>
		// Token: 0x17000D06 RID: 3334
		// (get) Token: 0x0600339D RID: 13213 RVA: 0x000A4F70 File Offset: 0x000A3170
		// (set) Token: 0x0600339E RID: 13214 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool CheckCertificateName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a Boolean value that indicates whether the certificate is checked against the certificate authority revocation list.</summary>
		/// <returns>
		///     <see langword="true" /> if the certificate revocation list is checked; otherwise, <see langword="false" />.The default value is <see langword="false" />.</returns>
		// Token: 0x17000D07 RID: 3335
		// (get) Token: 0x0600339F RID: 13215 RVA: 0x000A4F8C File Offset: 0x000A318C
		// (set) Token: 0x060033A0 RID: 13216 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool CheckCertificateRevocationList
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the amount of time after which address information is refreshed.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that specifies when addresses are resolved using DNS.</returns>
		// Token: 0x17000D08 RID: 3336
		// (get) Token: 0x060033A1 RID: 13217 RVA: 0x000A4FA8 File Offset: 0x000A31A8
		// (set) Token: 0x060033A2 RID: 13218 RVA: 0x000092E2 File Offset: 0x000074E2
		public int DnsRefreshTimeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a Boolean value that controls using different IP addresses on connections to the same server.</summary>
		/// <returns>
		///     <see langword="true" /> to enable DNS round-robin behavior; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000D09 RID: 3337
		// (get) Token: 0x060033A3 RID: 13219 RVA: 0x000A4FC4 File Offset: 0x000A31C4
		// (set) Token: 0x060033A4 RID: 13220 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool EnableDnsRoundRobin
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Net.Security.EncryptionPolicy" /> to use.</summary>
		/// <returns>The encryption policy to use for a <see cref="T:System.Net.ServicePointManager" /> instance.</returns>
		// Token: 0x17000D0A RID: 3338
		// (get) Token: 0x060033A5 RID: 13221 RVA: 0x000A4FE0 File Offset: 0x000A31E0
		// (set) Token: 0x060033A6 RID: 13222 RVA: 0x000092E2 File Offset: 0x000074E2
		public EncryptionPolicy EncryptionPolicy
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return EncryptionPolicy.RequireEncryption;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a Boolean value that determines whether 100-Continue behavior is used.</summary>
		/// <returns>
		///     <see langword="true" /> to expect 100-Continue responses for <see langword="POST" /> requests; otherwise, <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x17000D0B RID: 3339
		// (get) Token: 0x060033A7 RID: 13223 RVA: 0x000A4FFC File Offset: 0x000A31FC
		// (set) Token: 0x060033A8 RID: 13224 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool Expect100Continue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000D0C RID: 3340
		// (get) Token: 0x060033A9 RID: 13225 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a Boolean value that determines whether the Nagle algorithm is used.</summary>
		/// <returns>
		///     <see langword="true" /> to use the Nagle algorithm; otherwise, <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x17000D0D RID: 3341
		// (get) Token: 0x060033AA RID: 13226 RVA: 0x000A5018 File Offset: 0x000A3218
		// (set) Token: 0x060033AB RID: 13227 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool UseNagleAlgorithm
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x060033AC RID: 13228 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
