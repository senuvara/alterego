﻿using System;
using System.Configuration;
using System.Net.Cache;
using System.Xml;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the default FTP cache policy for network resources. This class cannot be inherited.</summary>
	// Token: 0x0200063F RID: 1599
	public sealed class FtpCachePolicyElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.FtpCachePolicyElement" /> class.</summary>
		// Token: 0x06003327 RID: 13095 RVA: 0x000092E2 File Offset: 0x000074E2
		public FtpCachePolicyElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets FTP caching behavior for the local machine.</summary>
		/// <returns>A <see cref="T:System.Net.Cache.RequestCacheLevel" /> value that specifies the cache behavior.</returns>
		// Token: 0x17000CC3 RID: 3267
		// (get) Token: 0x06003328 RID: 13096 RVA: 0x000A4C7C File Offset: 0x000A2E7C
		// (set) Token: 0x06003329 RID: 13097 RVA: 0x000092E2 File Offset: 0x000074E2
		public RequestCacheLevel PolicyLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return RequestCacheLevel.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CC4 RID: 3268
		// (get) Token: 0x0600332A RID: 13098 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x0600332B RID: 13099 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void DeserializeElement(XmlReader reader, bool serializeCollectionKey)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600332C RID: 13100 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void Reset(ConfigurationElement parentElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
