﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents a container for authentication module configuration elements. This class cannot be inherited.</summary>
	// Token: 0x02000632 RID: 1586
	[ConfigurationCollection(typeof(AuthenticationModuleElement))]
	public sealed class AuthenticationModuleElementCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.AuthenticationModuleElementCollection" /> class. </summary>
		// Token: 0x060032D0 RID: 13008 RVA: 0x000092E2 File Offset: 0x000074E2
		public AuthenticationModuleElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060032D1 RID: 13009 RVA: 0x00043C3C File Offset: 0x00041E3C
		public AuthenticationModuleElement get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060032D2 RID: 13010 RVA: 0x000092E2 File Offset: 0x000074E2
		public void set_Item(int index, AuthenticationModuleElement value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the element with the specified key.</summary>
		/// <param name="name">The key for an element in the collection. </param>
		/// <returns>The <see cref="T:System.Net.Configuration.AuthenticationModuleElement" /> with the specified key or <see langword="null" /> if there is no element with the specified key.</returns>
		// Token: 0x17000CA8 RID: 3240
		public AuthenticationModuleElement this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds an element to the collection.</summary>
		/// <param name="element">The <see cref="T:System.Net.Configuration.AuthenticationModuleElement" /> to add to the collection.</param>
		// Token: 0x060032D5 RID: 13013 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Add(AuthenticationModuleElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all elements from the collection.</summary>
		// Token: 0x060032D6 RID: 13014 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060032D7 RID: 13015 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060032D8 RID: 13016 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the index of the specified configuration element.</summary>
		/// <param name="element">A <see cref="T:System.Net.Configuration.AuthenticationModuleElement" />.</param>
		/// <returns>The zero-based index of <paramref name="element" />.</returns>
		// Token: 0x060032D9 RID: 13017 RVA: 0x000A4B64 File Offset: 0x000A2D64
		public int IndexOf(AuthenticationModuleElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Removes the specified configuration element from the collection.</summary>
		/// <param name="element">The <see cref="T:System.Net.Configuration.AuthenticationModuleElement" /> to remove.</param>
		// Token: 0x060032DA RID: 13018 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(AuthenticationModuleElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element with the specified key.</summary>
		/// <param name="name">The key of the element to remove.</param>
		// Token: 0x060032DB RID: 13019 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element at the specified index.</summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		// Token: 0x060032DC RID: 13020 RVA: 0x000092E2 File Offset: 0x000074E2
		public void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
