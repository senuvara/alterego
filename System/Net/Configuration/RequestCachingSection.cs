﻿using System;
using System.Configuration;
using System.Net.Cache;
using System.Xml;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the configuration section for cache behavior. This class cannot be inherited.</summary>
	// Token: 0x0200064A RID: 1610
	public sealed class RequestCachingSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.RequestCachingSection" /> class. </summary>
		// Token: 0x06003380 RID: 13184 RVA: 0x000092E2 File Offset: 0x000074E2
		public RequestCachingSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the default FTP caching behavior for the local computer.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.FtpCachePolicyElement" /> that defines the default cache policy.</returns>
		// Token: 0x17000CF4 RID: 3316
		// (get) Token: 0x06003381 RID: 13185 RVA: 0x00043C3C File Offset: 0x00041E3C
		public FtpCachePolicyElement DefaultFtpCachePolicy
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the default caching behavior for the local computer.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.HttpCachePolicyElement" /> that defines the default cache policy.</returns>
		// Token: 0x17000CF5 RID: 3317
		// (get) Token: 0x06003382 RID: 13186 RVA: 0x00043C3C File Offset: 0x00041E3C
		public HttpCachePolicyElement DefaultHttpCachePolicy
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the default cache policy level.</summary>
		/// <returns>A <see cref="T:System.Net.Cache.RequestCacheLevel" /> enumeration value.</returns>
		// Token: 0x17000CF6 RID: 3318
		// (get) Token: 0x06003383 RID: 13187 RVA: 0x000A4EE4 File Offset: 0x000A30E4
		// (set) Token: 0x06003384 RID: 13188 RVA: 0x000092E2 File Offset: 0x000074E2
		public RequestCacheLevel DefaultPolicyLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return RequestCacheLevel.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a Boolean value that enables caching on the local computer.</summary>
		/// <returns>
		///     <see langword="true" /> if caching is disabled on the local computer; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CF7 RID: 3319
		// (get) Token: 0x06003385 RID: 13189 RVA: 0x000A4F00 File Offset: 0x000A3100
		// (set) Token: 0x06003386 RID: 13190 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool DisableAllCaching
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a Boolean value that indicates whether the local computer cache is private.</summary>
		/// <returns>
		///     <see langword="true" /> if the cache provides user isolation; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CF8 RID: 3320
		// (get) Token: 0x06003387 RID: 13191 RVA: 0x000A4F1C File Offset: 0x000A311C
		// (set) Token: 0x06003388 RID: 13192 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool IsPrivateCache
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CF9 RID: 3321
		// (get) Token: 0x06003389 RID: 13193 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a value used as the maximum age for cached resources that do not have expiration information.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that provides a default maximum age for cached resources.</returns>
		// Token: 0x17000CFA RID: 3322
		// (get) Token: 0x0600338A RID: 13194 RVA: 0x000A4F38 File Offset: 0x000A3138
		// (set) Token: 0x0600338B RID: 13195 RVA: 0x000092E2 File Offset: 0x000074E2
		public TimeSpan UnspecifiedMaximumAge
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TimeSpan);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x0600338C RID: 13196 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void DeserializeElement(XmlReader reader, bool serializeCollectionKey)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600338D RID: 13197 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
