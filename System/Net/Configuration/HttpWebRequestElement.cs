﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the maximum length for response headers. This class cannot be inherited.</summary>
	// Token: 0x02000643 RID: 1603
	public sealed class HttpWebRequestElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.HttpWebRequestElement" /> class. </summary>
		// Token: 0x06003345 RID: 13125 RVA: 0x000092E2 File Offset: 0x000074E2
		public HttpWebRequestElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the maximum allowed length of an error response.</summary>
		/// <returns>A 32-bit signed integer containing the maximum length in kilobytes (1024 bytes) of the error response. The default value is 64.</returns>
		// Token: 0x17000CD4 RID: 3284
		// (get) Token: 0x06003346 RID: 13126 RVA: 0x000A4DCC File Offset: 0x000A2FCC
		// (set) Token: 0x06003347 RID: 13127 RVA: 0x000092E2 File Offset: 0x000074E2
		public int MaximumErrorResponseLength
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the maximum allowed length of the response headers.</summary>
		/// <returns>A 32-bit signed integer containing the maximum length in kilobytes (1024 bytes) of the response headers. The default value is 64.</returns>
		// Token: 0x17000CD5 RID: 3285
		// (get) Token: 0x06003348 RID: 13128 RVA: 0x000A4DE8 File Offset: 0x000A2FE8
		// (set) Token: 0x06003349 RID: 13129 RVA: 0x000092E2 File Offset: 0x000074E2
		public int MaximumResponseHeadersLength
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the maximum length of an upload in response to an unauthorized error code.</summary>
		/// <returns>A 32-bit signed integer containing the maximum length (in multiple of 1,024 byte units) of an upload in response to an unauthorized error code. A value of -1 indicates that no size limit will be imposed on the upload. Setting the <see cref="P:System.Net.Configuration.HttpWebRequestElement.MaximumUnauthorizedUploadLength" /> property to any other value will only send the request body if it is smaller than the number of bytes specified. So a value of 1 would indicate to only send the request body if it is smaller than 1,024 bytes. The default value for this property is -1.</returns>
		// Token: 0x17000CD6 RID: 3286
		// (get) Token: 0x0600334A RID: 13130 RVA: 0x000A4E04 File Offset: 0x000A3004
		// (set) Token: 0x0600334B RID: 13131 RVA: 0x000092E2 File Offset: 0x000074E2
		public int MaximumUnauthorizedUploadLength
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CD7 RID: 3287
		// (get) Token: 0x0600334C RID: 13132 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Setting this property ignores validation errors that occur during HTTP parsing.</summary>
		/// <returns>Boolean that indicates whether this property has been set. </returns>
		// Token: 0x17000CD8 RID: 3288
		// (get) Token: 0x0600334D RID: 13133 RVA: 0x000A4E20 File Offset: 0x000A3020
		// (set) Token: 0x0600334E RID: 13134 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool UseUnsafeHeaderParsing
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x0600334F RID: 13135 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
