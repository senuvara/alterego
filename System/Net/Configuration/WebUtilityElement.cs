﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the WebUtility element in the configuration file.</summary>
	// Token: 0x02000650 RID: 1616
	public sealed class WebUtilityElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.WebUtilityElement" /> class.</summary>
		// Token: 0x060033BB RID: 13243 RVA: 0x000092E2 File Offset: 0x000074E2
		public WebUtilityElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x17000D14 RID: 3348
		// (get) Token: 0x060033BC RID: 13244 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the default Unicode decoding conformance behavior used for an <see cref="T:System.Net.WebUtility" /> object.</summary>
		/// <returns>Returns <see cref="T:System.Net.Configuration.UnicodeDecodingConformance" />.The default Unicode decoding behavior.</returns>
		// Token: 0x17000D15 RID: 3349
		// (get) Token: 0x060033BD RID: 13245 RVA: 0x000A50A4 File Offset: 0x000A32A4
		// (set) Token: 0x060033BE RID: 13246 RVA: 0x000092E2 File Offset: 0x000074E2
		public UnicodeDecodingConformance UnicodeDecodingConformance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return UnicodeDecodingConformance.Auto;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the default Unicode encoding conformance behavior used for an <see cref="T:System.Net.WebUtility" /> object.</summary>
		/// <returns>Returns <see cref="T:System.Net.Configuration.UnicodeEncodingConformance" />.The default Unicode encoding behavior.</returns>
		// Token: 0x17000D16 RID: 3350
		// (get) Token: 0x060033BF RID: 13247 RVA: 0x000A50C0 File Offset: 0x000A32C0
		// (set) Token: 0x060033C0 RID: 13248 RVA: 0x000092E2 File Offset: 0x000074E2
		public UnicodeEncodingConformance UnicodeEncodingConformance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return UnicodeEncodingConformance.Auto;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
