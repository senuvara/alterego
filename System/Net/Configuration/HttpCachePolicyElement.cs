﻿using System;
using System.Configuration;
using System.Net.Cache;
using System.Xml;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the default HTTP cache policy for network resources. This class cannot be inherited.</summary>
	// Token: 0x02000640 RID: 1600
	public sealed class HttpCachePolicyElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.HttpCachePolicyElement" /> class. </summary>
		// Token: 0x0600332D RID: 13101 RVA: 0x000092E2 File Offset: 0x000074E2
		public HttpCachePolicyElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the maximum age permitted for a resource returned from the cache.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> value that specifies the maximum age for cached resources specified in the configuration file.</returns>
		// Token: 0x17000CC5 RID: 3269
		// (get) Token: 0x0600332E RID: 13102 RVA: 0x000A4C98 File Offset: 0x000A2E98
		// (set) Token: 0x0600332F RID: 13103 RVA: 0x000092E2 File Offset: 0x000074E2
		public TimeSpan MaximumAge
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TimeSpan);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the maximum staleness value permitted for a resource returned from the cache.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> value that is set to the maximum staleness value specified in the configuration file.</returns>
		// Token: 0x17000CC6 RID: 3270
		// (get) Token: 0x06003330 RID: 13104 RVA: 0x000A4CB4 File Offset: 0x000A2EB4
		// (set) Token: 0x06003331 RID: 13105 RVA: 0x000092E2 File Offset: 0x000074E2
		public TimeSpan MaximumStale
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TimeSpan);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the minimum freshness permitted for a resource returned from the cache.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> value that specifies the minimum freshness specified in the configuration file.</returns>
		// Token: 0x17000CC7 RID: 3271
		// (get) Token: 0x06003332 RID: 13106 RVA: 0x000A4CD0 File Offset: 0x000A2ED0
		// (set) Token: 0x06003333 RID: 13107 RVA: 0x000092E2 File Offset: 0x000074E2
		public TimeSpan MinimumFresh
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TimeSpan);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets HTTP caching behavior for the local machine.</summary>
		/// <returns>A <see cref="T:System.Net.Cache.HttpRequestCacheLevel" /> value that specifies the cache behavior.</returns>
		// Token: 0x17000CC8 RID: 3272
		// (get) Token: 0x06003334 RID: 13108 RVA: 0x000A4CEC File Offset: 0x000A2EEC
		// (set) Token: 0x06003335 RID: 13109 RVA: 0x000092E2 File Offset: 0x000074E2
		public HttpRequestCacheLevel PolicyLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return HttpRequestCacheLevel.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CC9 RID: 3273
		// (get) Token: 0x06003336 RID: 13110 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x06003337 RID: 13111 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void DeserializeElement(XmlReader reader, bool serializeCollectionKey)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003338 RID: 13112 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void Reset(ConfigurationElement parentElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
