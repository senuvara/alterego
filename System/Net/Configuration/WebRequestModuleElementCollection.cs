﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents a container for Web request module configuration elements. This class cannot be inherited.</summary>
	// Token: 0x02000652 RID: 1618
	[ConfigurationCollection(typeof(WebRequestModuleElement))]
	public sealed class WebRequestModuleElementCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.WebRequestModuleElementCollection" /> class. </summary>
		// Token: 0x060033C6 RID: 13254 RVA: 0x000092E2 File Offset: 0x000074E2
		public WebRequestModuleElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060033C7 RID: 13255 RVA: 0x00043C3C File Offset: 0x00041E3C
		public WebRequestModuleElement get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060033C8 RID: 13256 RVA: 0x000092E2 File Offset: 0x000074E2
		public void set_Item(int index, WebRequestModuleElement value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the element with the specified key.</summary>
		/// <param name="name">The key for an element in the collection.</param>
		/// <returns>The <see cref="T:System.Net.Configuration.WebRequestModuleElement" /> with the specified key or <see langword="null" /> if there is no element with the specified key.</returns>
		// Token: 0x17000D19 RID: 3353
		public WebRequestModuleElement this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds an element to the collection.</summary>
		/// <param name="element">The <see cref="T:System.Net.Configuration.WebRequestModuleElement" /> to add to the collection.</param>
		// Token: 0x060033CB RID: 13259 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Add(WebRequestModuleElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all elements from the collection.</summary>
		// Token: 0x060033CC RID: 13260 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060033CD RID: 13261 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060033CE RID: 13262 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the index of the specified configuration element.</summary>
		/// <param name="element">A <see cref="T:System.Net.Configuration.WebRequestModuleElement" />.</param>
		/// <returns>The zero-based index of <paramref name="element" />.</returns>
		// Token: 0x060033CF RID: 13263 RVA: 0x000A50DC File Offset: 0x000A32DC
		public int IndexOf(WebRequestModuleElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Removes the specified configuration element from the collection.</summary>
		/// <param name="element">The <see cref="T:System.Net.Configuration.WebRequestModuleElement" /> to remove.</param>
		// Token: 0x060033D0 RID: 13264 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(WebRequestModuleElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element with the specified key.</summary>
		/// <param name="name">The key of the element to remove.</param>
		// Token: 0x060033D1 RID: 13265 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element at the specified index.</summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		// Token: 0x060033D2 RID: 13266 RVA: 0x000092E2 File Offset: 0x000074E2
		public void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
