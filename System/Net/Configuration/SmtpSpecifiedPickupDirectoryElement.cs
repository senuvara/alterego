﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents an SMTP pickup directory configuration element.</summary>
	// Token: 0x02000648 RID: 1608
	public sealed class SmtpSpecifiedPickupDirectoryElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement" /> class.</summary>
		// Token: 0x06003373 RID: 13171 RVA: 0x000092E2 File Offset: 0x000074E2
		public SmtpSpecifiedPickupDirectoryElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the folder where applications save mail messages to be processed by the SMTP server.</summary>
		/// <returns>A string that specifies the pickup directory for e-mail messages.</returns>
		// Token: 0x17000CEB RID: 3307
		// (get) Token: 0x06003374 RID: 13172 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003375 RID: 13173 RVA: 0x000092E2 File Offset: 0x000074E2
		public string PickupDirectoryLocation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CEC RID: 3308
		// (get) Token: 0x06003376 RID: 13174 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
