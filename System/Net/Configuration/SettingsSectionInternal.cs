﻿using System;
using System.Net.Security;
using System.Net.Sockets;
using System.Runtime.CompilerServices;

namespace System.Net.Configuration
{
	// Token: 0x02000530 RID: 1328
	internal sealed class SettingsSectionInternal
	{
		// Token: 0x17000A6A RID: 2666
		// (get) Token: 0x060029FA RID: 10746 RVA: 0x00091CF8 File Offset: 0x0008FEF8
		internal static SettingsSectionInternal Section
		{
			get
			{
				return SettingsSectionInternal.instance;
			}
		}

		// Token: 0x17000A6B RID: 2667
		// (get) Token: 0x060029FB RID: 10747 RVA: 0x00091CFF File Offset: 0x0008FEFF
		// (set) Token: 0x060029FC RID: 10748 RVA: 0x00091D07 File Offset: 0x0008FF07
		internal bool UseNagleAlgorithm
		{
			[CompilerGenerated]
			get
			{
				return this.<UseNagleAlgorithm>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<UseNagleAlgorithm>k__BackingField = value;
			}
		}

		// Token: 0x17000A6C RID: 2668
		// (get) Token: 0x060029FD RID: 10749 RVA: 0x00091D10 File Offset: 0x0008FF10
		// (set) Token: 0x060029FE RID: 10750 RVA: 0x00091D18 File Offset: 0x0008FF18
		internal bool Expect100Continue
		{
			[CompilerGenerated]
			get
			{
				return this.<Expect100Continue>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Expect100Continue>k__BackingField = value;
			}
		}

		// Token: 0x17000A6D RID: 2669
		// (get) Token: 0x060029FF RID: 10751 RVA: 0x00091D21 File Offset: 0x0008FF21
		// (set) Token: 0x06002A00 RID: 10752 RVA: 0x00091D29 File Offset: 0x0008FF29
		internal bool CheckCertificateName
		{
			[CompilerGenerated]
			get
			{
				return this.<CheckCertificateName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<CheckCertificateName>k__BackingField = value;
			}
		}

		// Token: 0x17000A6E RID: 2670
		// (get) Token: 0x06002A01 RID: 10753 RVA: 0x00091D32 File Offset: 0x0008FF32
		// (set) Token: 0x06002A02 RID: 10754 RVA: 0x00091D3A File Offset: 0x0008FF3A
		internal int DnsRefreshTimeout
		{
			[CompilerGenerated]
			get
			{
				return this.<DnsRefreshTimeout>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<DnsRefreshTimeout>k__BackingField = value;
			}
		}

		// Token: 0x17000A6F RID: 2671
		// (get) Token: 0x06002A03 RID: 10755 RVA: 0x00091D43 File Offset: 0x0008FF43
		// (set) Token: 0x06002A04 RID: 10756 RVA: 0x00091D4B File Offset: 0x0008FF4B
		internal bool EnableDnsRoundRobin
		{
			[CompilerGenerated]
			get
			{
				return this.<EnableDnsRoundRobin>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<EnableDnsRoundRobin>k__BackingField = value;
			}
		}

		// Token: 0x17000A70 RID: 2672
		// (get) Token: 0x06002A05 RID: 10757 RVA: 0x00091D54 File Offset: 0x0008FF54
		// (set) Token: 0x06002A06 RID: 10758 RVA: 0x00091D5C File Offset: 0x0008FF5C
		internal bool CheckCertificateRevocationList
		{
			[CompilerGenerated]
			get
			{
				return this.<CheckCertificateRevocationList>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CheckCertificateRevocationList>k__BackingField = value;
			}
		}

		// Token: 0x17000A71 RID: 2673
		// (get) Token: 0x06002A07 RID: 10759 RVA: 0x00091D65 File Offset: 0x0008FF65
		// (set) Token: 0x06002A08 RID: 10760 RVA: 0x00091D6D File Offset: 0x0008FF6D
		internal EncryptionPolicy EncryptionPolicy
		{
			[CompilerGenerated]
			get
			{
				return this.<EncryptionPolicy>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<EncryptionPolicy>k__BackingField = value;
			}
		}

		// Token: 0x17000A72 RID: 2674
		// (get) Token: 0x06002A09 RID: 10761 RVA: 0x00003298 File Offset: 0x00001498
		internal bool Ipv6Enabled
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06002A0A RID: 10762 RVA: 0x00091D76 File Offset: 0x0008FF76
		public SettingsSectionInternal()
		{
		}

		// Token: 0x06002A0B RID: 10763 RVA: 0x00091D8C File Offset: 0x0008FF8C
		// Note: this type is marked as 'beforefieldinit'.
		static SettingsSectionInternal()
		{
		}

		// Token: 0x040021A7 RID: 8615
		private static readonly SettingsSectionInternal instance = new SettingsSectionInternal();

		// Token: 0x040021A8 RID: 8616
		internal readonly bool HttpListenerUnescapeRequestUrl = true;

		// Token: 0x040021A9 RID: 8617
		internal readonly IPProtectionLevel IPProtectionLevel = IPProtectionLevel.Unspecified;

		// Token: 0x040021AA RID: 8618
		[CompilerGenerated]
		private bool <UseNagleAlgorithm>k__BackingField;

		// Token: 0x040021AB RID: 8619
		[CompilerGenerated]
		private bool <Expect100Continue>k__BackingField;

		// Token: 0x040021AC RID: 8620
		[CompilerGenerated]
		private bool <CheckCertificateName>k__BackingField;

		// Token: 0x040021AD RID: 8621
		[CompilerGenerated]
		private int <DnsRefreshTimeout>k__BackingField;

		// Token: 0x040021AE RID: 8622
		[CompilerGenerated]
		private bool <EnableDnsRoundRobin>k__BackingField;

		// Token: 0x040021AF RID: 8623
		[CompilerGenerated]
		private bool <CheckCertificateRevocationList>k__BackingField;

		// Token: 0x040021B0 RID: 8624
		[CompilerGenerated]
		private EncryptionPolicy <EncryptionPolicy>k__BackingField;
	}
}
