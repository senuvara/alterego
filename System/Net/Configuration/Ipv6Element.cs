﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Determines whether Internet Protocol version 6 is enabled on the local computer. This class cannot be inherited.</summary>
	// Token: 0x02000644 RID: 1604
	public sealed class Ipv6Element : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.Ipv6Element" /> class. </summary>
		// Token: 0x06003350 RID: 13136 RVA: 0x000092E2 File Offset: 0x000074E2
		public Ipv6Element()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a Boolean value that indicates whether Internet Protocol version 6 is enabled on the local computer.</summary>
		/// <returns>
		///     <see langword="true" /> if IPv6 is enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CD9 RID: 3289
		// (get) Token: 0x06003351 RID: 13137 RVA: 0x000A4E3C File Offset: 0x000A303C
		// (set) Token: 0x06003352 RID: 13138 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool Enabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CDA RID: 3290
		// (get) Token: 0x06003353 RID: 13139 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
