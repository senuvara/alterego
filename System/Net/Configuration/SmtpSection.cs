﻿using System;
using System.Configuration;
using System.Net.Mail;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the SMTP section in the <see langword="System.Net" /> configuration file.</summary>
	// Token: 0x02000646 RID: 1606
	public sealed class SmtpSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.SmtpSection" /> class.</summary>
		// Token: 0x06003356 RID: 13142 RVA: 0x000092E2 File Offset: 0x000074E2
		public SmtpSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the delivery format to use for sending outgoing e-mail using the Simple Mail Transport Protocol (SMTP).</summary>
		/// <returns>Returns <see cref="T:System.Net.Mail.SmtpDeliveryFormat" />.The delivery format to use for sending outgoing e-mail using SMTP.</returns>
		// Token: 0x17000CDC RID: 3292
		// (get) Token: 0x06003357 RID: 13143 RVA: 0x000A4E58 File Offset: 0x000A3058
		// (set) Token: 0x06003358 RID: 13144 RVA: 0x000092E2 File Offset: 0x000074E2
		public SmtpDeliveryFormat DeliveryFormat
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SmtpDeliveryFormat.SevenBit;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the Simple Mail Transport Protocol (SMTP) delivery method. The default delivery method is <see cref="F:System.Net.Mail.SmtpDeliveryMethod.Network" />.</summary>
		/// <returns>A string that represents the SMTP delivery method.</returns>
		// Token: 0x17000CDD RID: 3293
		// (get) Token: 0x06003359 RID: 13145 RVA: 0x000A4E74 File Offset: 0x000A3074
		// (set) Token: 0x0600335A RID: 13146 RVA: 0x000092E2 File Offset: 0x000074E2
		public SmtpDeliveryMethod DeliveryMethod
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SmtpDeliveryMethod.Network;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the default value that indicates who the email message is from.</summary>
		/// <returns>A string that represents the default value indicating who a mail message is from.</returns>
		// Token: 0x17000CDE RID: 3294
		// (get) Token: 0x0600335B RID: 13147 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600335C RID: 13148 RVA: 0x000092E2 File Offset: 0x000074E2
		public string From
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the configuration element that controls the network settings used by the Simple Mail Transport Protocol (SMTP). file.<see cref="T:System.Net.Configuration.SmtpNetworkElement" />.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.SmtpNetworkElement" /> object.The configuration element that controls the network settings used by SMTP.</returns>
		// Token: 0x17000CDF RID: 3295
		// (get) Token: 0x0600335D RID: 13149 RVA: 0x00043C3C File Offset: 0x00041E3C
		public SmtpNetworkElement Network
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000CE0 RID: 3296
		// (get) Token: 0x0600335E RID: 13150 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the pickup directory that will be used by the SMPT client.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.SmtpSpecifiedPickupDirectoryElement" /> object that specifies the pickup directory folder.</returns>
		// Token: 0x17000CE1 RID: 3297
		// (get) Token: 0x0600335F RID: 13151 RVA: 0x00043C3C File Offset: 0x00041E3C
		public SmtpSpecifiedPickupDirectoryElement SpecifiedPickupDirectory
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
