﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.MailSettingsSectionGroup" /> class.</summary>
	// Token: 0x02000645 RID: 1605
	public sealed class MailSettingsSectionGroup : ConfigurationSectionGroup
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.MailSettingsSectionGroup" /> class.</summary>
		// Token: 0x06003354 RID: 13140 RVA: 0x000092E2 File Offset: 0x000074E2
		public MailSettingsSectionGroup()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the SMTP settings for the local computer.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.SmtpSection" /> object that contains configuration information for the local computer.</returns>
		// Token: 0x17000CDB RID: 3291
		// (get) Token: 0x06003355 RID: 13141 RVA: 0x00043C3C File Offset: 0x00041E3C
		public SmtpSection Smtp
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
