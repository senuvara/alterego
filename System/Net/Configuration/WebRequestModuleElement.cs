﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents a URI prefix and the associated class that handles creating Web requests for the prefix. This class cannot be inherited.</summary>
	// Token: 0x02000653 RID: 1619
	public sealed class WebRequestModuleElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.WebRequestModuleElement" /> class. </summary>
		// Token: 0x060033D3 RID: 13267 RVA: 0x000092E2 File Offset: 0x000074E2
		public WebRequestModuleElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.WebRequestModuleElement" /> class using the specified URI prefix and type information. </summary>
		/// <param name="prefix">A string containing a URI prefix.</param>
		/// <param name="type">A string containing the type and assembly information for the class that handles creating requests for resources that use the <paramref name="prefix" /> URI prefix. For more information, see the Remarks section.</param>
		// Token: 0x060033D4 RID: 13268 RVA: 0x000092E2 File Offset: 0x000074E2
		public WebRequestModuleElement(string prefix, string type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.WebRequestModuleElement" /> class using the specified URI prefix and type identifier.</summary>
		/// <param name="prefix">A string containing a URI prefix.</param>
		/// <param name="type">A <see cref="T:System.Type" /> that identifies the class that handles creating requests for resources that use the <paramref name="prefix" /> URI prefix. </param>
		// Token: 0x060033D5 RID: 13269 RVA: 0x000092E2 File Offset: 0x000074E2
		public WebRequestModuleElement(string prefix, Type type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the URI prefix for the current Web request module.</summary>
		/// <returns>A string that contains a URI prefix.</returns>
		// Token: 0x17000D1A RID: 3354
		// (get) Token: 0x060033D6 RID: 13270 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060033D7 RID: 13271 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Prefix
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000D1B RID: 3355
		// (get) Token: 0x060033D8 RID: 13272 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a class that creates Web requests.</summary>
		/// <returns>A <see cref="T:System.Type" /> instance that identifies a Web request module.</returns>
		// Token: 0x17000D1C RID: 3356
		// (get) Token: 0x060033D9 RID: 13273 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060033DA RID: 13274 RVA: 0x000092E2 File Offset: 0x000074E2
		public Type Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
