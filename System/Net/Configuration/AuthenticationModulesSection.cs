﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the configuration section for authentication modules. This class cannot be inherited.</summary>
	// Token: 0x02000633 RID: 1587
	public sealed class AuthenticationModulesSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.AuthenticationModulesSection" /> class. </summary>
		// Token: 0x060032DD RID: 13021 RVA: 0x000092E2 File Offset: 0x000074E2
		public AuthenticationModulesSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the collection of authentication modules in the section.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.AuthenticationModuleElementCollection" /> that contains the registered authentication modules. </returns>
		// Token: 0x17000CA9 RID: 3241
		// (get) Token: 0x060032DE RID: 13022 RVA: 0x00043C3C File Offset: 0x00041E3C
		public AuthenticationModuleElementCollection AuthenticationModules
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000CAA RID: 3242
		// (get) Token: 0x060032DF RID: 13023 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x060032E0 RID: 13024 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void InitializeDefault()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060032E1 RID: 13025 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
