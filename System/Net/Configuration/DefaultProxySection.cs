﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the configuration section for Web proxy server usage. This class cannot be inherited.</summary>
	// Token: 0x02000639 RID: 1593
	public sealed class DefaultProxySection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.DefaultProxySection" /> class. </summary>
		// Token: 0x0600330C RID: 13068 RVA: 0x000092E2 File Offset: 0x000074E2
		public DefaultProxySection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the collection of resources that are not obtained using the Web proxy server.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.BypassElementCollection" /> that contains the addresses of resources that bypass the Web proxy server. </returns>
		// Token: 0x17000CB5 RID: 3253
		// (get) Token: 0x0600330D RID: 13069 RVA: 0x00043C3C File Offset: 0x00041E3C
		public BypassElementCollection BypassList
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets whether a Web proxy is used.</summary>
		/// <returns>
		///     <see langword="true" /> if a Web proxy will be used; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CB6 RID: 3254
		// (get) Token: 0x0600330E RID: 13070 RVA: 0x000A4BF0 File Offset: 0x000A2DF0
		// (set) Token: 0x0600330F RID: 13071 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool Enabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the type information for a custom Web proxy implementation.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.ModuleElement" />. The type information for a custom Web proxy implementation.</returns>
		// Token: 0x17000CB7 RID: 3255
		// (get) Token: 0x06003310 RID: 13072 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ModuleElement Module
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000CB8 RID: 3256
		// (get) Token: 0x06003311 RID: 13073 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the URI that identifies the Web proxy server to use.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.ProxyElement" />. The URI that identifies the Web proxy server.</returns>
		// Token: 0x17000CB9 RID: 3257
		// (get) Token: 0x06003312 RID: 13074 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ProxyElement Proxy
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets whether default credentials are to be used to access a Web proxy server.</summary>
		/// <returns>
		///     <see langword="true" /> if default credentials are to be used; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CBA RID: 3258
		// (get) Token: 0x06003313 RID: 13075 RVA: 0x000A4C0C File Offset: 0x000A2E0C
		// (set) Token: 0x06003314 RID: 13076 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool UseDefaultCredentials
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x06003315 RID: 13077 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003316 RID: 13078 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void Reset(ConfigurationElement parentElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
