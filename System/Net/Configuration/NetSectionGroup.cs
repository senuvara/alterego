﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Gets the section group information for the networking namespaces. This class cannot be inherited.</summary>
	// Token: 0x02000649 RID: 1609
	public sealed class NetSectionGroup : ConfigurationSectionGroup
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.NetSectionGroup" /> class. </summary>
		// Token: 0x06003377 RID: 13175 RVA: 0x000092E2 File Offset: 0x000074E2
		public NetSectionGroup()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the configuration section containing the authentication modules registered for the local computer.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.AuthenticationModulesSection" /> object.</returns>
		// Token: 0x17000CED RID: 3309
		// (get) Token: 0x06003378 RID: 13176 RVA: 0x00043C3C File Offset: 0x00041E3C
		public AuthenticationModulesSection AuthenticationModules
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration section containing the connection management settings for the local computer.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.ConnectionManagementSection" /> object.</returns>
		// Token: 0x17000CEE RID: 3310
		// (get) Token: 0x06003379 RID: 13177 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ConnectionManagementSection ConnectionManagement
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration section containing the default Web proxy server settings for the local computer.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.DefaultProxySection" /> object.</returns>
		// Token: 0x17000CEF RID: 3311
		// (get) Token: 0x0600337A RID: 13178 RVA: 0x00043C3C File Offset: 0x00041E3C
		public DefaultProxySection DefaultProxy
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration section containing the SMTP client e-mail settings for the local computer.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.MailSettingsSectionGroup" /> object.</returns>
		// Token: 0x17000CF0 RID: 3312
		// (get) Token: 0x0600337B RID: 13179 RVA: 0x00043C3C File Offset: 0x00041E3C
		public MailSettingsSectionGroup MailSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration section containing the cache configuration settings for the local computer.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.RequestCachingSection" /> object.</returns>
		// Token: 0x17000CF1 RID: 3313
		// (get) Token: 0x0600337C RID: 13180 RVA: 0x00043C3C File Offset: 0x00041E3C
		public RequestCachingSection RequestCaching
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration section containing the network settings for the local computer.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.SettingsSection" /> object.</returns>
		// Token: 0x17000CF2 RID: 3314
		// (get) Token: 0x0600337D RID: 13181 RVA: 0x00043C3C File Offset: 0x00041E3C
		public SettingsSection Settings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration section containing the modules registered for use with the <see cref="T:System.Net.WebRequest" /> class.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.WebRequestModulesSection" /> object.</returns>
		// Token: 0x17000CF3 RID: 3315
		// (get) Token: 0x0600337E RID: 13182 RVA: 0x00043C3C File Offset: 0x00041E3C
		public WebRequestModulesSection WebRequestModules
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the <see langword="System.Net" /> configuration section group from the specified configuration file.</summary>
		/// <param name="config">A <see cref="T:System.Configuration.Configuration" /> that represents a configuration file.</param>
		/// <returns>A <see cref="T:System.Net.Configuration.NetSectionGroup" /> that represents the <see langword="System.Net" /> settings in <paramref name="config" />.</returns>
		// Token: 0x0600337F RID: 13183 RVA: 0x00043C3C File Offset: 0x00041E3C
		public static NetSectionGroup GetSectionGroup(Configuration config)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
