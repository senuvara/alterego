﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the type information for an authentication module. This class cannot be inherited.</summary>
	// Token: 0x02000631 RID: 1585
	public sealed class AuthenticationModuleElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.AuthenticationModuleElement" /> class. </summary>
		// Token: 0x060032CB RID: 13003 RVA: 0x000092E2 File Offset: 0x000074E2
		public AuthenticationModuleElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.AuthenticationModuleElement" /> class with the specified type information.</summary>
		/// <param name="typeName">A string that identifies the type and the assembly that contains it.</param>
		// Token: 0x060032CC RID: 13004 RVA: 0x000092E2 File Offset: 0x000074E2
		public AuthenticationModuleElement(string typeName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x17000CA6 RID: 3238
		// (get) Token: 0x060032CD RID: 13005 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the type and assembly information for the current instance.</summary>
		/// <returns>A string that identifies a type that implements an authentication module or <see langword="null" /> if no value has been specified.</returns>
		// Token: 0x17000CA7 RID: 3239
		// (get) Token: 0x060032CE RID: 13006 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060032CF RID: 13007 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
