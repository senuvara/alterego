﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Identifies the configuration settings for Web proxy server. This class cannot be inherited.</summary>
	// Token: 0x0200063B RID: 1595
	public sealed class ProxyElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.ProxyElement" /> class. </summary>
		// Token: 0x0600331B RID: 13083 RVA: 0x000092E2 File Offset: 0x000074E2
		public ProxyElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an <see cref="T:System.Net.Configuration.ProxyElement.AutoDetectValues" /> value that controls whether the Web proxy is automatically detected.</summary>
		/// <returns>
		///     <see cref="F:System.Net.Configuration.ProxyElement.AutoDetectValues.True" /> if the <see cref="T:System.Net.WebProxy" /> is automatically detected; <see cref="F:System.Net.Configuration.ProxyElement.AutoDetectValues.False" /> if the <see cref="T:System.Net.WebProxy" /> is not automatically detected; or <see cref="F:System.Net.Configuration.ProxyElement.AutoDetectValues.Unspecified" />.</returns>
		// Token: 0x17000CBD RID: 3261
		// (get) Token: 0x0600331C RID: 13084 RVA: 0x000A4C28 File Offset: 0x000A2E28
		// (set) Token: 0x0600331D RID: 13085 RVA: 0x000092E2 File Offset: 0x000074E2
		public ProxyElement.AutoDetectValues AutoDetect
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ProxyElement.AutoDetectValues.False;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether local resources are retrieved by using a Web proxy server.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.ProxyElement.BypassOnLocalValues" />.Avalue that indicates whether local resources are retrieved by using a Web proxy server.</returns>
		// Token: 0x17000CBE RID: 3262
		// (get) Token: 0x0600331E RID: 13086 RVA: 0x000A4C44 File Offset: 0x000A2E44
		// (set) Token: 0x0600331F RID: 13087 RVA: 0x000092E2 File Offset: 0x000074E2
		public ProxyElement.BypassOnLocalValues BypassOnLocal
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ProxyElement.BypassOnLocalValues.False;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000CBF RID: 3263
		// (get) Token: 0x06003320 RID: 13088 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the URI that identifies the Web proxy server to use.</summary>
		/// <returns>A <see cref="T:System.String" /> containing a URI.</returns>
		// Token: 0x17000CC0 RID: 3264
		// (get) Token: 0x06003321 RID: 13089 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003322 RID: 13090 RVA: 0x000092E2 File Offset: 0x000074E2
		public Uri ProxyAddress
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an <see cref="T:System.Uri" /> value that specifies the location of the automatic proxy detection script.</summary>
		/// <returns>A <see cref="T:System.Uri" /> specifying the location of the automatic proxy detection script.</returns>
		// Token: 0x17000CC1 RID: 3265
		// (get) Token: 0x06003323 RID: 13091 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003324 RID: 13092 RVA: 0x000092E2 File Offset: 0x000074E2
		public Uri ScriptLocation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that controls whether the Internet Explorer Web proxy settings are used.</summary>
		/// <returns>
		///     <see langword="true" /> if the Internet Explorer LAN settings are used to detect and configure the default <see cref="T:System.Net.WebProxy" /> used for requests; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CC2 RID: 3266
		// (get) Token: 0x06003325 RID: 13093 RVA: 0x000A4C60 File Offset: 0x000A2E60
		// (set) Token: 0x06003326 RID: 13094 RVA: 0x000092E2 File Offset: 0x000074E2
		public ProxyElement.UseSystemDefaultValues UseSystemDefault
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ProxyElement.UseSystemDefaultValues.False;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Specifies whether the proxy is automatically detected.</summary>
		// Token: 0x0200063C RID: 1596
		public enum AutoDetectValues
		{
			/// <summary>The proxy is not automatically detected.</summary>
			// Token: 0x040024E3 RID: 9443
			False,
			/// <summary>The proxy is automatically detected.</summary>
			// Token: 0x040024E4 RID: 9444
			True,
			/// <summary>Unspecified.</summary>
			// Token: 0x040024E5 RID: 9445
			Unspecified = -1
		}

		/// <summary>Specifies whether the proxy is bypassed for local resources.</summary>
		// Token: 0x0200063D RID: 1597
		public enum BypassOnLocalValues
		{
			/// <summary>All requests for local resources should go through the proxy</summary>
			// Token: 0x040024E7 RID: 9447
			False,
			/// <summary>Access local resources directly.</summary>
			// Token: 0x040024E8 RID: 9448
			True,
			/// <summary>Unspecified.</summary>
			// Token: 0x040024E9 RID: 9449
			Unspecified = -1
		}

		/// <summary>Specifies whether to use the local system proxy settings to determine whether the proxy is bypassed for local resources.</summary>
		// Token: 0x0200063E RID: 1598
		public enum UseSystemDefaultValues
		{
			/// <summary>Do not use system default proxy setting values</summary>
			// Token: 0x040024EB RID: 9451
			False,
			/// <summary>Use system default proxy setting values.</summary>
			// Token: 0x040024EC RID: 9452
			True,
			/// <summary>The system default proxy setting is unspecified.</summary>
			// Token: 0x040024ED RID: 9453
			Unspecified = -1
		}
	}
}
