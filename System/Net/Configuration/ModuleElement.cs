﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the type information for a custom <see cref="T:System.Net.IWebProxy" /> module. This class cannot be inherited.</summary>
	// Token: 0x0200063A RID: 1594
	public sealed class ModuleElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.ModuleElement" /> class. </summary>
		// Token: 0x06003317 RID: 13079 RVA: 0x000092E2 File Offset: 0x000074E2
		public ModuleElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x17000CBB RID: 3259
		// (get) Token: 0x06003318 RID: 13080 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the type and assembly information for the current instance.</summary>
		/// <returns>A string that identifies a type that implements the <see cref="T:System.Net.IWebProxy" /> interface or <see langword="null" /> if no value has been specified.</returns>
		// Token: 0x17000CBC RID: 3260
		// (get) Token: 0x06003319 RID: 13081 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600331A RID: 13082 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
