﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the configuration section for Web request modules. This class cannot be inherited.</summary>
	// Token: 0x02000651 RID: 1617
	public sealed class WebRequestModulesSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.WebRequestModulesSection" /> class.</summary>
		// Token: 0x060033C1 RID: 13249 RVA: 0x000092E2 File Offset: 0x000074E2
		public WebRequestModulesSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x17000D17 RID: 3351
		// (get) Token: 0x060033C2 RID: 13250 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of Web request modules in the section.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.WebRequestModuleElementCollection" /> containing the registered Web request modules. </returns>
		// Token: 0x17000D18 RID: 3352
		// (get) Token: 0x060033C3 RID: 13251 RVA: 0x00043C3C File Offset: 0x00041E3C
		public WebRequestModuleElementCollection WebRequestModules
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x060033C4 RID: 13252 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void InitializeDefault()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060033C5 RID: 13253 RVA: 0x000092E2 File Offset: 0x000074E2
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
