﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the configuration section for connection management. This class cannot be inherited.</summary>
	// Token: 0x02000638 RID: 1592
	public sealed class ConnectionManagementSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Configuration.ConnectionManagementSection" /> class. </summary>
		// Token: 0x06003309 RID: 13065 RVA: 0x000092E2 File Offset: 0x000074E2
		public ConnectionManagementSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the collection of connection management objects in the section.</summary>
		/// <returns>A <see cref="T:System.Net.Configuration.ConnectionManagementElementCollection" /> that contains the connection management information for the local computer. </returns>
		// Token: 0x17000CB3 RID: 3251
		// (get) Token: 0x0600330A RID: 13066 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ConnectionManagementElementCollection ConnectionManagement
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000CB4 RID: 3252
		// (get) Token: 0x0600330B RID: 13067 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
