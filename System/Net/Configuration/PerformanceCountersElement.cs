﻿using System;
using System.Configuration;
using Unity;

namespace System.Net.Configuration
{
	/// <summary>Represents the performance counter element in the <see langword="System.Net" /> configuration file that determines whether networking performance counters are enabled. This class cannot be inherited.</summary>
	// Token: 0x0200064C RID: 1612
	public sealed class PerformanceCountersElement : ConfigurationElement
	{
		/// <summary>Instantiates a <see cref="T:System.Net.Configuration.PerformanceCountersElement" /> object.</summary>
		// Token: 0x06003398 RID: 13208 RVA: 0x000092E2 File Offset: 0x000074E2
		public PerformanceCountersElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets whether performance counters are enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if performance counters are enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000D04 RID: 3332
		// (get) Token: 0x06003399 RID: 13209 RVA: 0x000A4F54 File Offset: 0x000A3154
		// (set) Token: 0x0600339A RID: 13210 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool Enabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000D05 RID: 3333
		// (get) Token: 0x0600339B RID: 13211 RVA: 0x00043C3C File Offset: 0x00041E3C
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
