﻿using System;

namespace System.Net
{
	// Token: 0x02000311 RID: 785
	// (Invoke) Token: 0x060016B8 RID: 5816
	internal delegate bool HttpAbortDelegate(HttpWebRequest request, WebException webException);
}
