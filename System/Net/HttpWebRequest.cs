﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using Mono.Security.Interface;
using Unity;

namespace System.Net
{
	/// <summary>Provides an HTTP-specific implementation of the <see cref="T:System.Net.WebRequest" /> class.</summary>
	// Token: 0x020003EC RID: 1004
	[Serializable]
	public class HttpWebRequest : WebRequest, ISerializable
	{
		// Token: 0x06001E55 RID: 7765 RVA: 0x0006DA9F File Offset: 0x0006BC9F
		static HttpWebRequest()
		{
		}

		// Token: 0x06001E56 RID: 7766 RVA: 0x0006DAAC File Offset: 0x0006BCAC
		public HttpWebRequest(Uri uri)
		{
			this.allowAutoRedirect = true;
			this.allowBuffering = true;
			this.contentLength = -1L;
			this.keepAlive = true;
			this.maxAutoRedirect = 50;
			this.mediaType = string.Empty;
			this.method = "GET";
			this.initialMethod = "GET";
			this.pipelined = true;
			this.version = HttpVersion.Version11;
			this.timeout = 100000;
			this.locker = new object();
			this.readWriteTimeout = 300000;
			base..ctor();
			this.requestUri = uri;
			this.actualUri = uri;
			this.proxy = WebRequest.InternalDefaultWebProxy;
			this.webHeaders = new WebHeaderCollection(WebHeaderCollectionType.HttpWebRequest);
			this.ThrowOnError = true;
			this.ResetAuthorization();
		}

		// Token: 0x06001E57 RID: 7767 RVA: 0x0006DB6A File Offset: 0x0006BD6A
		internal HttpWebRequest(Uri uri, MonoTlsProvider tlsProvider, MonoTlsSettings settings = null) : this(uri)
		{
			this.tlsProvider = tlsProvider;
			this.tlsSettings = settings;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.HttpWebRequest" /> class from the specified instances of the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> and <see cref="T:System.Runtime.Serialization.StreamingContext" /> classes.</summary>
		/// <param name="serializationInfo">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object that contains the information required to serialize the new <see cref="T:System.Net.HttpWebRequest" /> object. </param>
		/// <param name="streamingContext">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> object that contains the source and destination of the serialized stream associated with the new <see cref="T:System.Net.HttpWebRequest" /> object. </param>
		// Token: 0x06001E58 RID: 7768 RVA: 0x0006DB84 File Offset: 0x0006BD84
		[Obsolete("Serialization is obsoleted for this type", false)]
		protected HttpWebRequest(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			this.allowAutoRedirect = true;
			this.allowBuffering = true;
			this.contentLength = -1L;
			this.keepAlive = true;
			this.maxAutoRedirect = 50;
			this.mediaType = string.Empty;
			this.method = "GET";
			this.initialMethod = "GET";
			this.pipelined = true;
			this.version = HttpVersion.Version11;
			this.timeout = 100000;
			this.locker = new object();
			this.readWriteTimeout = 300000;
			base..ctor();
			this.requestUri = (Uri)serializationInfo.GetValue("requestUri", typeof(Uri));
			this.actualUri = (Uri)serializationInfo.GetValue("actualUri", typeof(Uri));
			this.allowAutoRedirect = serializationInfo.GetBoolean("allowAutoRedirect");
			this.allowBuffering = serializationInfo.GetBoolean("allowBuffering");
			this.certificates = (X509CertificateCollection)serializationInfo.GetValue("certificates", typeof(X509CertificateCollection));
			this.connectionGroup = serializationInfo.GetString("connectionGroup");
			this.contentLength = serializationInfo.GetInt64("contentLength");
			this.webHeaders = (WebHeaderCollection)serializationInfo.GetValue("webHeaders", typeof(WebHeaderCollection));
			this.keepAlive = serializationInfo.GetBoolean("keepAlive");
			this.maxAutoRedirect = serializationInfo.GetInt32("maxAutoRedirect");
			this.mediaType = serializationInfo.GetString("mediaType");
			this.method = serializationInfo.GetString("method");
			this.initialMethod = serializationInfo.GetString("initialMethod");
			this.pipelined = serializationInfo.GetBoolean("pipelined");
			this.version = (Version)serializationInfo.GetValue("version", typeof(Version));
			this.proxy = (IWebProxy)serializationInfo.GetValue("proxy", typeof(IWebProxy));
			this.sendChunked = serializationInfo.GetBoolean("sendChunked");
			this.timeout = serializationInfo.GetInt32("timeout");
			this.redirects = serializationInfo.GetInt32("redirects");
			this.host = serializationInfo.GetString("host");
			this.ResetAuthorization();
		}

		// Token: 0x06001E59 RID: 7769 RVA: 0x0006DDC6 File Offset: 0x0006BFC6
		private void ResetAuthorization()
		{
			this.auth_state = new HttpWebRequest.AuthorizationState(this, false);
			this.proxy_auth_state = new HttpWebRequest.AuthorizationState(this, true);
		}

		// Token: 0x06001E5A RID: 7770 RVA: 0x0006DDE2 File Offset: 0x0006BFE2
		private void SetSpecialHeaders(string HeaderName, string value)
		{
			value = WebHeaderCollection.CheckBadChars(value, true);
			this.webHeaders.RemoveInternal(HeaderName);
			if (value.Length != 0)
			{
				this.webHeaders.AddInternal(HeaderName, value);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Accept" /> HTTP header.</summary>
		/// <returns>The value of the <see langword="Accept" /> HTTP header. The default value is <see langword="null" />.</returns>
		// Token: 0x17000658 RID: 1624
		// (get) Token: 0x06001E5B RID: 7771 RVA: 0x0006DE0E File Offset: 0x0006C00E
		// (set) Token: 0x06001E5C RID: 7772 RVA: 0x0006DE20 File Offset: 0x0006C020
		public string Accept
		{
			get
			{
				return this.webHeaders["Accept"];
			}
			set
			{
				this.CheckRequestStarted();
				this.SetSpecialHeaders("Accept", value);
			}
		}

		/// <summary>Gets the Uniform Resource Identifier (URI) of the Internet resource that actually responds to the request.</summary>
		/// <returns>A <see cref="T:System.Uri" /> that identifies the Internet resource that actually responds to the request. The default is the URI used by the <see cref="M:System.Net.WebRequest.Create(System.String)" /> method to initialize the request.</returns>
		// Token: 0x17000659 RID: 1625
		// (get) Token: 0x06001E5D RID: 7773 RVA: 0x0006DE34 File Offset: 0x0006C034
		// (set) Token: 0x06001E5E RID: 7774 RVA: 0x0006DE3C File Offset: 0x0006C03C
		public Uri Address
		{
			get
			{
				return this.actualUri;
			}
			internal set
			{
				this.actualUri = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the request should follow redirection responses.</summary>
		/// <returns>
		///     <see langword="true" /> if the request should automatically follow redirection responses from the Internet resource; otherwise, <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x1700065A RID: 1626
		// (get) Token: 0x06001E5F RID: 7775 RVA: 0x0006DE45 File Offset: 0x0006C045
		// (set) Token: 0x06001E60 RID: 7776 RVA: 0x0006DE4D File Offset: 0x0006C04D
		public virtual bool AllowAutoRedirect
		{
			get
			{
				return this.allowAutoRedirect;
			}
			set
			{
				this.allowAutoRedirect = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether to buffer the data sent to the Internet resource.</summary>
		/// <returns>
		///     <see langword="true" /> to enable buffering of the data sent to the Internet resource; <see langword="false" /> to disable buffering. The default is <see langword="true" />.</returns>
		// Token: 0x1700065B RID: 1627
		// (get) Token: 0x06001E61 RID: 7777 RVA: 0x0006DE56 File Offset: 0x0006C056
		// (set) Token: 0x06001E62 RID: 7778 RVA: 0x0006DE5E File Offset: 0x0006C05E
		public virtual bool AllowWriteStreamBuffering
		{
			get
			{
				return this.allowBuffering;
			}
			set
			{
				this.allowBuffering = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether to buffer the received from the Internet resource.</summary>
		/// <returns>
		///     <see langword="true" /> to buffer the received from the Internet resource; otherwise, <see langword="false" />.
		///     <see langword="true" /> to enable buffering of the data received from the Internet resource; <see langword="false" /> to disable buffering. The default is <see langword="false" />.</returns>
		// Token: 0x1700065C RID: 1628
		// (get) Token: 0x06001E63 RID: 7779 RVA: 0x00005AFA File Offset: 0x00003CFA
		// (set) Token: 0x06001E64 RID: 7780 RVA: 0x0006DE67 File Offset: 0x0006C067
		public virtual bool AllowReadStreamBuffering
		{
			get
			{
				return false;
			}
			set
			{
				if (value)
				{
					throw new InvalidOperationException();
				}
			}
		}

		// Token: 0x06001E65 RID: 7781 RVA: 0x000659A3 File Offset: 0x00063BA3
		private static Exception GetMustImplement()
		{
			return new NotImplementedException();
		}

		/// <summary>Gets or sets the type of decompression that is used.</summary>
		/// <returns>A T:System.Net.DecompressionMethods object that indicates the type of decompression that is used. </returns>
		/// <exception cref="T:System.InvalidOperationException">The object's current state does not allow this property to be set.</exception>
		// Token: 0x1700065D RID: 1629
		// (get) Token: 0x06001E66 RID: 7782 RVA: 0x0006DE72 File Offset: 0x0006C072
		// (set) Token: 0x06001E67 RID: 7783 RVA: 0x0006DE7A File Offset: 0x0006C07A
		public DecompressionMethods AutomaticDecompression
		{
			get
			{
				return this.auto_decomp;
			}
			set
			{
				this.CheckRequestStarted();
				this.auto_decomp = value;
			}
		}

		// Token: 0x1700065E RID: 1630
		// (get) Token: 0x06001E68 RID: 7784 RVA: 0x0006DE89 File Offset: 0x0006C089
		internal bool InternalAllowBuffering
		{
			get
			{
				return this.allowBuffering && this.MethodWithBuffer;
			}
		}

		// Token: 0x1700065F RID: 1631
		// (get) Token: 0x06001E69 RID: 7785 RVA: 0x0006DE9C File Offset: 0x0006C09C
		private bool MethodWithBuffer
		{
			get
			{
				return this.method != "HEAD" && this.method != "GET" && this.method != "MKCOL" && this.method != "CONNECT" && this.method != "TRACE";
			}
		}

		// Token: 0x17000660 RID: 1632
		// (get) Token: 0x06001E6A RID: 7786 RVA: 0x0006DF03 File Offset: 0x0006C103
		internal MonoTlsProvider TlsProvider
		{
			get
			{
				return this.tlsProvider;
			}
		}

		// Token: 0x17000661 RID: 1633
		// (get) Token: 0x06001E6B RID: 7787 RVA: 0x0006DF0B File Offset: 0x0006C10B
		internal MonoTlsSettings TlsSettings
		{
			get
			{
				return this.tlsSettings;
			}
		}

		/// <summary>Gets or sets the collection of security certificates that are associated with this request.</summary>
		/// <returns>The <see cref="T:System.Security.Cryptography.X509Certificates.X509CertificateCollection" /> that contains the security certificates associated with this request.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value specified for a set operation is <see langword="null" />. </exception>
		// Token: 0x17000662 RID: 1634
		// (get) Token: 0x06001E6C RID: 7788 RVA: 0x0006DF13 File Offset: 0x0006C113
		// (set) Token: 0x06001E6D RID: 7789 RVA: 0x0006DF2E File Offset: 0x0006C12E
		public X509CertificateCollection ClientCertificates
		{
			get
			{
				if (this.certificates == null)
				{
					this.certificates = new X509CertificateCollection();
				}
				return this.certificates;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.certificates = value;
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Connection" /> HTTP header.</summary>
		/// <returns>The value of the <see langword="Connection" /> HTTP header. The default value is <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentException">The value of <see cref="P:System.Net.HttpWebRequest.Connection" /> is set to Keep-alive or Close. </exception>
		// Token: 0x17000663 RID: 1635
		// (get) Token: 0x06001E6E RID: 7790 RVA: 0x0006DF45 File Offset: 0x0006C145
		// (set) Token: 0x06001E6F RID: 7791 RVA: 0x0006DF58 File Offset: 0x0006C158
		public string Connection
		{
			get
			{
				return this.webHeaders["Connection"];
			}
			set
			{
				this.CheckRequestStarted();
				if (string.IsNullOrEmpty(value))
				{
					this.webHeaders.RemoveInternal("Connection");
					return;
				}
				string text = value.ToLowerInvariant();
				if (text.Contains("keep-alive") || text.Contains("close"))
				{
					throw new ArgumentException("Keep-Alive and Close may not be set with this property");
				}
				if (this.keepAlive)
				{
					value += ", Keep-Alive";
				}
				this.webHeaders.CheckUpdate("Connection", value);
			}
		}

		/// <summary>Gets or sets the name of the connection group for the request.</summary>
		/// <returns>The name of the connection group for this request. The default value is <see langword="null" />.</returns>
		// Token: 0x17000664 RID: 1636
		// (get) Token: 0x06001E70 RID: 7792 RVA: 0x0006DFD6 File Offset: 0x0006C1D6
		// (set) Token: 0x06001E71 RID: 7793 RVA: 0x0006DFDE File Offset: 0x0006C1DE
		public override string ConnectionGroupName
		{
			get
			{
				return this.connectionGroup;
			}
			set
			{
				this.connectionGroup = value;
			}
		}

		/// <summary>Gets or sets the <see langword="Content-length" /> HTTP header.</summary>
		/// <returns>The number of bytes of data to send to the Internet resource. The default is -1, which indicates the property has not been set and that there is no request data to send.</returns>
		/// <exception cref="T:System.InvalidOperationException">The request has been started by calling the <see cref="M:System.Net.HttpWebRequest.GetRequestStream" />, <see cref="M:System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" />, <see cref="M:System.Net.HttpWebRequest.GetResponse" />, or <see cref="M:System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" /> method. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The new <see cref="P:System.Net.HttpWebRequest.ContentLength" /> value is less than 0. </exception>
		// Token: 0x17000665 RID: 1637
		// (get) Token: 0x06001E72 RID: 7794 RVA: 0x0006DFE7 File Offset: 0x0006C1E7
		// (set) Token: 0x06001E73 RID: 7795 RVA: 0x0006DFEF File Offset: 0x0006C1EF
		public override long ContentLength
		{
			get
			{
				return this.contentLength;
			}
			set
			{
				this.CheckRequestStarted();
				if (value < 0L)
				{
					throw new ArgumentOutOfRangeException("value", "Content-Length must be >= 0");
				}
				this.contentLength = value;
				this.haveContentLength = true;
			}
		}

		// Token: 0x17000666 RID: 1638
		// (set) Token: 0x06001E74 RID: 7796 RVA: 0x0006E01A File Offset: 0x0006C21A
		internal long InternalContentLength
		{
			set
			{
				this.contentLength = value;
			}
		}

		// Token: 0x17000667 RID: 1639
		// (get) Token: 0x06001E75 RID: 7797 RVA: 0x0006E023 File Offset: 0x0006C223
		// (set) Token: 0x06001E76 RID: 7798 RVA: 0x0006E02B File Offset: 0x0006C22B
		internal bool ThrowOnError
		{
			[CompilerGenerated]
			get
			{
				return this.<ThrowOnError>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ThrowOnError>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Content-type" /> HTTP header.</summary>
		/// <returns>The value of the <see langword="Content-type" /> HTTP header. The default value is <see langword="null" />.</returns>
		// Token: 0x17000668 RID: 1640
		// (get) Token: 0x06001E77 RID: 7799 RVA: 0x0006E034 File Offset: 0x0006C234
		// (set) Token: 0x06001E78 RID: 7800 RVA: 0x0006E046 File Offset: 0x0006C246
		public override string ContentType
		{
			get
			{
				return this.webHeaders["Content-Type"];
			}
			set
			{
				this.SetSpecialHeaders("Content-Type", value);
			}
		}

		/// <summary>Gets or sets the delegate method called when an HTTP 100-continue response is received from the Internet resource.</summary>
		/// <returns>A delegate that implements the callback method that executes when an HTTP Continue response is returned from the Internet resource. The default value is <see langword="null" />.</returns>
		// Token: 0x17000669 RID: 1641
		// (get) Token: 0x06001E79 RID: 7801 RVA: 0x0006E054 File Offset: 0x0006C254
		// (set) Token: 0x06001E7A RID: 7802 RVA: 0x0006E05C File Offset: 0x0006C25C
		public HttpContinueDelegate ContinueDelegate
		{
			get
			{
				return this.continueDelegate;
			}
			set
			{
				this.continueDelegate = value;
			}
		}

		/// <summary>Gets or sets the cookies associated with the request.</summary>
		/// <returns>A <see cref="T:System.Net.CookieContainer" /> that contains the cookies associated with this request.</returns>
		// Token: 0x1700066A RID: 1642
		// (get) Token: 0x06001E7B RID: 7803 RVA: 0x0006E065 File Offset: 0x0006C265
		// (set) Token: 0x06001E7C RID: 7804 RVA: 0x0006E06D File Offset: 0x0006C26D
		public virtual CookieContainer CookieContainer
		{
			get
			{
				return this.cookieContainer;
			}
			set
			{
				this.cookieContainer = value;
			}
		}

		/// <summary>Gets or sets authentication information for the request.</summary>
		/// <returns>An <see cref="T:System.Net.ICredentials" /> that contains the authentication credentials associated with the request. The default is <see langword="null" />.</returns>
		// Token: 0x1700066B RID: 1643
		// (get) Token: 0x06001E7D RID: 7805 RVA: 0x0006E076 File Offset: 0x0006C276
		// (set) Token: 0x06001E7E RID: 7806 RVA: 0x0006E07E File Offset: 0x0006C27E
		public override ICredentials Credentials
		{
			get
			{
				return this.credentials;
			}
			set
			{
				this.credentials = value;
			}
		}

		/// <summary>Get or set the <see langword="Date" /> HTTP header value to use in an HTTP request.</summary>
		/// <returns>The Date header value in the HTTP request.</returns>
		// Token: 0x1700066C RID: 1644
		// (get) Token: 0x06001E7F RID: 7807 RVA: 0x0006E088 File Offset: 0x0006C288
		// (set) Token: 0x06001E80 RID: 7808 RVA: 0x0006E0C7 File Offset: 0x0006C2C7
		public DateTime Date
		{
			get
			{
				string text = this.webHeaders["Date"];
				if (text == null)
				{
					return DateTime.MinValue;
				}
				return DateTime.ParseExact(text, "r", CultureInfo.InvariantCulture).ToLocalTime();
			}
			set
			{
				this.SetDateHeaderHelper("Date", value);
			}
		}

		// Token: 0x06001E81 RID: 7809 RVA: 0x0006E0D5 File Offset: 0x0006C2D5
		private void SetDateHeaderHelper(string headerName, DateTime dateTime)
		{
			if (dateTime == DateTime.MinValue)
			{
				this.SetSpecialHeaders(headerName, null);
				return;
			}
			this.SetSpecialHeaders(headerName, HttpProtocolUtils.date2string(dateTime));
		}

		/// <summary>Gets or sets the default maximum length of an HTTP error response.</summary>
		/// <returns>The default maximum length of an HTTP error response.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value is less than 0 and is not equal to -1. </exception>
		// Token: 0x1700066D RID: 1645
		// (get) Token: 0x06001E82 RID: 7810 RVA: 0x0006E0FA File Offset: 0x0006C2FA
		// (set) Token: 0x06001E83 RID: 7811 RVA: 0x0006E0FA File Offset: 0x0006C2FA
		[MonoTODO]
		public static int DefaultMaximumErrorResponseLength
		{
			get
			{
				throw HttpWebRequest.GetMustImplement();
			}
			set
			{
				throw HttpWebRequest.GetMustImplement();
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Expect" /> HTTP header.</summary>
		/// <returns>The contents of the <see langword="Expect" /> HTTP header. The default value is <see langword="null" />.The value for this property is stored in <see cref="T:System.Net.WebHeaderCollection" />. If WebHeaderCollection is set, the property value is lost.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <see langword="Expect" /> is set to a string that contains "100-continue" as a substring. </exception>
		// Token: 0x1700066E RID: 1646
		// (get) Token: 0x06001E84 RID: 7812 RVA: 0x0006E101 File Offset: 0x0006C301
		// (set) Token: 0x06001E85 RID: 7813 RVA: 0x0006E114 File Offset: 0x0006C314
		public string Expect
		{
			get
			{
				return this.webHeaders["Expect"];
			}
			set
			{
				this.CheckRequestStarted();
				string text = value;
				if (text != null)
				{
					text = text.Trim().ToLower();
				}
				if (text == null || text.Length == 0)
				{
					this.webHeaders.RemoveInternal("Expect");
					return;
				}
				if (text == "100-continue")
				{
					throw new ArgumentException("100-Continue cannot be set with this property.", "value");
				}
				this.webHeaders.CheckUpdate("Expect", value);
			}
		}

		/// <summary>Gets a value that indicates whether a response has been received from an Internet resource.</summary>
		/// <returns>
		///     <see langword="true" /> if a response has been received; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700066F RID: 1647
		// (get) Token: 0x06001E86 RID: 7814 RVA: 0x0006E182 File Offset: 0x0006C382
		public virtual bool HaveResponse
		{
			get
			{
				return this.haveResponse;
			}
		}

		/// <summary>Specifies a collection of the name/value pairs that make up the HTTP headers.</summary>
		/// <returns>A <see cref="T:System.Net.WebHeaderCollection" /> that contains the name/value pairs that make up the headers for the HTTP request.</returns>
		/// <exception cref="T:System.InvalidOperationException">The request has been started by calling the <see cref="M:System.Net.HttpWebRequest.GetRequestStream" />, <see cref="M:System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" />, <see cref="M:System.Net.HttpWebRequest.GetResponse" />, or <see cref="M:System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" /> method. </exception>
		// Token: 0x17000670 RID: 1648
		// (get) Token: 0x06001E87 RID: 7815 RVA: 0x0006E18A File Offset: 0x0006C38A
		// (set) Token: 0x06001E88 RID: 7816 RVA: 0x0006E194 File Offset: 0x0006C394
		public override WebHeaderCollection Headers
		{
			get
			{
				return this.webHeaders;
			}
			set
			{
				this.CheckRequestStarted();
				WebHeaderCollection webHeaderCollection = new WebHeaderCollection(WebHeaderCollectionType.HttpWebRequest);
				foreach (string name in value.AllKeys)
				{
					webHeaderCollection.Add(name, value[name]);
				}
				this.webHeaders = webHeaderCollection;
			}
		}

		/// <summary>Get or set the Host header value to use in an HTTP request independent from the request URI.</summary>
		/// <returns>The Host header value in the HTTP request.</returns>
		/// <exception cref="T:System.ArgumentNullException">The Host header cannot be set to <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The Host header cannot be set to an invalid value. </exception>
		/// <exception cref="T:System.InvalidOperationException">The Host header cannot be set after the <see cref="T:System.Net.HttpWebRequest" /> has already started to be sent. </exception>
		// Token: 0x17000671 RID: 1649
		// (get) Token: 0x06001E89 RID: 7817 RVA: 0x0006E1E1 File Offset: 0x0006C3E1
		// (set) Token: 0x06001E8A RID: 7818 RVA: 0x0006E1FD File Offset: 0x0006C3FD
		public string Host
		{
			get
			{
				if (this.host == null)
				{
					return this.actualUri.Authority;
				}
				return this.host;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (!HttpWebRequest.CheckValidHost(this.actualUri.Scheme, value))
				{
					throw new ArgumentException("Invalid host: " + value);
				}
				this.host = value;
			}
		}

		// Token: 0x06001E8B RID: 7819 RVA: 0x0006E238 File Offset: 0x0006C438
		private static bool CheckValidHost(string scheme, string val)
		{
			IPAddress ipaddress;
			return val.Length != 0 && val[0] != '.' && val.IndexOf('/') < 0 && (IPAddress.TryParse(val, out ipaddress) || Uri.IsWellFormedUriString(scheme + "://" + val + "/", UriKind.Absolute));
		}

		/// <summary>Gets or sets the value of the <see langword="If-Modified-Since" /> HTTP header.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> that contains the contents of the <see langword="If-Modified-Since" /> HTTP header. The default value is the current date and time.</returns>
		// Token: 0x17000672 RID: 1650
		// (get) Token: 0x06001E8C RID: 7820 RVA: 0x0006E28C File Offset: 0x0006C48C
		// (set) Token: 0x06001E8D RID: 7821 RVA: 0x0006E2D8 File Offset: 0x0006C4D8
		public DateTime IfModifiedSince
		{
			get
			{
				string text = this.webHeaders["If-Modified-Since"];
				if (text == null)
				{
					return DateTime.Now;
				}
				DateTime result;
				try
				{
					result = MonoHttpDate.Parse(text);
				}
				catch (Exception)
				{
					result = DateTime.Now;
				}
				return result;
			}
			set
			{
				this.CheckRequestStarted();
				this.webHeaders.SetInternal("If-Modified-Since", value.ToUniversalTime().ToString("r", null));
			}
		}

		/// <summary>Gets or sets a value that indicates whether to make a persistent connection to the Internet resource.</summary>
		/// <returns>
		///     <see langword="true" /> if the request to the Internet resource should contain a <see langword="Connection" /> HTTP header with the value Keep-alive; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000673 RID: 1651
		// (get) Token: 0x06001E8E RID: 7822 RVA: 0x0006E310 File Offset: 0x0006C510
		// (set) Token: 0x06001E8F RID: 7823 RVA: 0x0006E318 File Offset: 0x0006C518
		public bool KeepAlive
		{
			get
			{
				return this.keepAlive;
			}
			set
			{
				this.keepAlive = value;
			}
		}

		/// <summary>Gets or sets the maximum number of redirects that the request follows.</summary>
		/// <returns>The maximum number of redirection responses that the request follows. The default value is 50.</returns>
		/// <exception cref="T:System.ArgumentException">The value is set to 0 or less. </exception>
		// Token: 0x17000674 RID: 1652
		// (get) Token: 0x06001E90 RID: 7824 RVA: 0x0006E321 File Offset: 0x0006C521
		// (set) Token: 0x06001E91 RID: 7825 RVA: 0x0006E329 File Offset: 0x0006C529
		public int MaximumAutomaticRedirections
		{
			get
			{
				return this.maxAutoRedirect;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentException("Must be > 0", "value");
				}
				this.maxAutoRedirect = value;
			}
		}

		/// <summary>Gets or sets the maximum allowed length of the response headers.</summary>
		/// <returns>The length, in kilobytes (1024 bytes), of the response headers.</returns>
		/// <exception cref="T:System.InvalidOperationException">The property is set after the request has already been submitted. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value is less than 0 and is not equal to -1. </exception>
		// Token: 0x17000675 RID: 1653
		// (get) Token: 0x06001E92 RID: 7826 RVA: 0x0006E346 File Offset: 0x0006C546
		// (set) Token: 0x06001E93 RID: 7827 RVA: 0x0006E34E File Offset: 0x0006C54E
		[MonoTODO("Use this")]
		public int MaximumResponseHeadersLength
		{
			get
			{
				return this.maxResponseHeadersLength;
			}
			set
			{
				this.maxResponseHeadersLength = value;
			}
		}

		/// <summary>Gets or sets the default for the <see cref="P:System.Net.HttpWebRequest.MaximumResponseHeadersLength" /> property.</summary>
		/// <returns>The length, in kilobytes (1024 bytes), of the default maximum for response headers received. The default configuration file sets this value to 64 kilobytes.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value is not equal to -1 and is less than zero. </exception>
		// Token: 0x17000676 RID: 1654
		// (get) Token: 0x06001E94 RID: 7828 RVA: 0x0006E357 File Offset: 0x0006C557
		// (set) Token: 0x06001E95 RID: 7829 RVA: 0x0006E35E File Offset: 0x0006C55E
		[MonoTODO("Use this")]
		public static int DefaultMaximumResponseHeadersLength
		{
			get
			{
				return HttpWebRequest.defaultMaxResponseHeadersLength;
			}
			set
			{
				HttpWebRequest.defaultMaxResponseHeadersLength = value;
			}
		}

		/// <summary>Gets or sets a time-out in milliseconds when writing to or reading from a stream.</summary>
		/// <returns>The number of milliseconds before the writing or reading times out. The default value is 300,000 milliseconds (5 minutes).</returns>
		/// <exception cref="T:System.InvalidOperationException">The request has already been sent. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified for a set operation is less than or equal to zero and is not equal to <see cref="F:System.Threading.Timeout.Infinite" /></exception>
		// Token: 0x17000677 RID: 1655
		// (get) Token: 0x06001E96 RID: 7830 RVA: 0x0006E366 File Offset: 0x0006C566
		// (set) Token: 0x06001E97 RID: 7831 RVA: 0x0006E36E File Offset: 0x0006C56E
		public int ReadWriteTimeout
		{
			get
			{
				return this.readWriteTimeout;
			}
			set
			{
				if (this.requestSent)
				{
					throw new InvalidOperationException("The request has already been sent.");
				}
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException("value", "Must be >= -1");
				}
				this.readWriteTimeout = value;
			}
		}

		/// <summary>Gets or sets a timeout, in milliseconds, to wait until the 100-Continue is received from the server. </summary>
		/// <returns>The timeout, in milliseconds, to wait until the 100-Continue is received. </returns>
		// Token: 0x17000678 RID: 1656
		// (get) Token: 0x06001E98 RID: 7832 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001E99 RID: 7833 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public int ContinueTimeout
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the media type of the request.</summary>
		/// <returns>The media type of the request. The default value is <see langword="null" />.</returns>
		// Token: 0x17000679 RID: 1657
		// (get) Token: 0x06001E9A RID: 7834 RVA: 0x0006E39E File Offset: 0x0006C59E
		// (set) Token: 0x06001E9B RID: 7835 RVA: 0x0006E3A6 File Offset: 0x0006C5A6
		public string MediaType
		{
			get
			{
				return this.mediaType;
			}
			set
			{
				this.mediaType = value;
			}
		}

		/// <summary>Gets or sets the method for the request.</summary>
		/// <returns>The request method to use to contact the Internet resource. The default value is GET.</returns>
		/// <exception cref="T:System.ArgumentException">No method is supplied.-or- The method string contains invalid characters. </exception>
		// Token: 0x1700067A RID: 1658
		// (get) Token: 0x06001E9C RID: 7836 RVA: 0x0006E3AF File Offset: 0x0006C5AF
		// (set) Token: 0x06001E9D RID: 7837 RVA: 0x0006E3B8 File Offset: 0x0006C5B8
		public override string Method
		{
			get
			{
				return this.method;
			}
			set
			{
				if (value == null || value.Trim() == "")
				{
					throw new ArgumentException("not a valid method");
				}
				this.method = value.ToUpperInvariant();
				if (this.method != "HEAD" && this.method != "GET" && this.method != "POST" && this.method != "PUT" && this.method != "DELETE" && this.method != "CONNECT" && this.method != "TRACE" && this.method != "MKCOL")
				{
					this.method = value;
				}
			}
		}

		/// <summary>Gets or sets a value that indicates whether to pipeline the request to the Internet resource.</summary>
		/// <returns>
		///     <see langword="true" /> if the request should be pipelined; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x1700067B RID: 1659
		// (get) Token: 0x06001E9E RID: 7838 RVA: 0x0006E48B File Offset: 0x0006C68B
		// (set) Token: 0x06001E9F RID: 7839 RVA: 0x0006E493 File Offset: 0x0006C693
		public bool Pipelined
		{
			get
			{
				return this.pipelined;
			}
			set
			{
				this.pipelined = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether to send an Authorization header with the request.</summary>
		/// <returns>
		///     <see langword="true" /> to send an  HTTP Authorization header with requests after authentication has taken place; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700067C RID: 1660
		// (get) Token: 0x06001EA0 RID: 7840 RVA: 0x0006E49C File Offset: 0x0006C69C
		// (set) Token: 0x06001EA1 RID: 7841 RVA: 0x0006E4A4 File Offset: 0x0006C6A4
		public override bool PreAuthenticate
		{
			get
			{
				return this.preAuthenticate;
			}
			set
			{
				this.preAuthenticate = value;
			}
		}

		/// <summary>Gets or sets the version of HTTP to use for the request.</summary>
		/// <returns>The HTTP version to use for the request. The default is <see cref="F:System.Net.HttpVersion.Version11" />.</returns>
		/// <exception cref="T:System.ArgumentException">The HTTP version is set to a value other than 1.0 or 1.1. </exception>
		// Token: 0x1700067D RID: 1661
		// (get) Token: 0x06001EA2 RID: 7842 RVA: 0x0006E4AD File Offset: 0x0006C6AD
		// (set) Token: 0x06001EA3 RID: 7843 RVA: 0x0006E4B5 File Offset: 0x0006C6B5
		public Version ProtocolVersion
		{
			get
			{
				return this.version;
			}
			set
			{
				if (value != HttpVersion.Version10 && value != HttpVersion.Version11)
				{
					throw new ArgumentException("value");
				}
				this.force_version = true;
				this.version = value;
			}
		}

		/// <summary>Gets or sets proxy information for the request.</summary>
		/// <returns>The <see cref="T:System.Net.IWebProxy" /> object to use to proxy the request. The default value is set by calling the <see cref="P:System.Net.GlobalProxySelection.Select" /> property.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <see cref="P:System.Net.HttpWebRequest.Proxy" /> is set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The request has been started by calling <see cref="M:System.Net.HttpWebRequest.GetRequestStream" />, <see cref="M:System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" />, <see cref="M:System.Net.HttpWebRequest.GetResponse" />, or <see cref="M:System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have permission for the requested operation. </exception>
		// Token: 0x1700067E RID: 1662
		// (get) Token: 0x06001EA4 RID: 7844 RVA: 0x0006E4EA File Offset: 0x0006C6EA
		// (set) Token: 0x06001EA5 RID: 7845 RVA: 0x0006E4F2 File Offset: 0x0006C6F2
		public override IWebProxy Proxy
		{
			get
			{
				return this.proxy;
			}
			set
			{
				this.CheckRequestStarted();
				this.proxy = value;
				this.servicePoint = null;
				this.GetServicePoint();
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Referer" /> HTTP header.</summary>
		/// <returns>The value of the <see langword="Referer" /> HTTP header. The default value is <see langword="null" />.</returns>
		// Token: 0x1700067F RID: 1663
		// (get) Token: 0x06001EA6 RID: 7846 RVA: 0x0006E50F File Offset: 0x0006C70F
		// (set) Token: 0x06001EA7 RID: 7847 RVA: 0x0006E521 File Offset: 0x0006C721
		public string Referer
		{
			get
			{
				return this.webHeaders["Referer"];
			}
			set
			{
				this.CheckRequestStarted();
				if (value == null || value.Trim().Length == 0)
				{
					this.webHeaders.RemoveInternal("Referer");
					return;
				}
				this.webHeaders.SetInternal("Referer", value);
			}
		}

		/// <summary>Gets the original Uniform Resource Identifier (URI) of the request.</summary>
		/// <returns>A <see cref="T:System.Uri" /> that contains the URI of the Internet resource passed to the <see cref="M:System.Net.WebRequest.Create(System.String)" /> method.</returns>
		// Token: 0x17000680 RID: 1664
		// (get) Token: 0x06001EA8 RID: 7848 RVA: 0x0006E55B File Offset: 0x0006C75B
		public override Uri RequestUri
		{
			get
			{
				return this.requestUri;
			}
		}

		/// <summary>Gets or sets a value that indicates whether to send data in segments to the Internet resource.</summary>
		/// <returns>
		///     <see langword="true" /> to send data to the Internet resource in segments; otherwise, <see langword="false" />. The default value is <see langword="false" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The request has been started by calling the <see cref="M:System.Net.HttpWebRequest.GetRequestStream" />, <see cref="M:System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" />, <see cref="M:System.Net.HttpWebRequest.GetResponse" />, or <see cref="M:System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" /> method. </exception>
		// Token: 0x17000681 RID: 1665
		// (get) Token: 0x06001EA9 RID: 7849 RVA: 0x0006E563 File Offset: 0x0006C763
		// (set) Token: 0x06001EAA RID: 7850 RVA: 0x0006E56B File Offset: 0x0006C76B
		public bool SendChunked
		{
			get
			{
				return this.sendChunked;
			}
			set
			{
				this.CheckRequestStarted();
				this.sendChunked = value;
			}
		}

		/// <summary>Gets the service point to use for the request.</summary>
		/// <returns>A <see cref="T:System.Net.ServicePoint" /> that represents the network connection to the Internet resource.</returns>
		// Token: 0x17000682 RID: 1666
		// (get) Token: 0x06001EAB RID: 7851 RVA: 0x0006E57A File Offset: 0x0006C77A
		public ServicePoint ServicePoint
		{
			get
			{
				return this.GetServicePoint();
			}
		}

		// Token: 0x17000683 RID: 1667
		// (get) Token: 0x06001EAC RID: 7852 RVA: 0x0006E582 File Offset: 0x0006C782
		internal ServicePoint ServicePointNoLock
		{
			get
			{
				return this.servicePoint;
			}
		}

		/// <summary>Gets a value that indicates whether the request provides support for a <see cref="T:System.Net.CookieContainer" />.</summary>
		/// <returns>
		///     <see langword="true" /> if the request provides support for a <see cref="T:System.Net.CookieContainer" />; otherwise, <see langword="false" />.
		///     <see langword="true" /> if a <see cref="T:System.Net.CookieContainer" /> is supported; otherwise, <see langword="false" />. </returns>
		// Token: 0x17000684 RID: 1668
		// (get) Token: 0x06001EAD RID: 7853 RVA: 0x00003298 File Offset: 0x00001498
		public virtual bool SupportsCookieContainer
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets or sets the time-out value in milliseconds for the <see cref="M:System.Net.HttpWebRequest.GetResponse" /> and <see cref="M:System.Net.HttpWebRequest.GetRequestStream" /> methods.</summary>
		/// <returns>The number of milliseconds to wait before the request times out. The default value is 100,000 milliseconds (100 seconds).</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified is less than zero and is not <see cref="F:System.Threading.Timeout.Infinite" />.</exception>
		// Token: 0x17000685 RID: 1669
		// (get) Token: 0x06001EAE RID: 7854 RVA: 0x0006E58A File Offset: 0x0006C78A
		// (set) Token: 0x06001EAF RID: 7855 RVA: 0x0006E592 File Offset: 0x0006C792
		public override int Timeout
		{
			get
			{
				return this.timeout;
			}
			set
			{
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.timeout = value;
			}
		}

		/// <summary>Gets or sets the value of the <see langword="Transfer-encoding" /> HTTP header.</summary>
		/// <returns>The value of the <see langword="Transfer-encoding" /> HTTP header. The default value is <see langword="null" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="P:System.Net.HttpWebRequest.TransferEncoding" /> is set when <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <see cref="P:System.Net.HttpWebRequest.TransferEncoding" /> is set to the value "Chunked". </exception>
		// Token: 0x17000686 RID: 1670
		// (get) Token: 0x06001EB0 RID: 7856 RVA: 0x0006E5AA File Offset: 0x0006C7AA
		// (set) Token: 0x06001EB1 RID: 7857 RVA: 0x0006E5BC File Offset: 0x0006C7BC
		public string TransferEncoding
		{
			get
			{
				return this.webHeaders["Transfer-Encoding"];
			}
			set
			{
				this.CheckRequestStarted();
				string text = value;
				if (text != null)
				{
					text = text.Trim().ToLower();
				}
				if (text == null || text.Length == 0)
				{
					this.webHeaders.RemoveInternal("Transfer-Encoding");
					return;
				}
				if (text == "chunked")
				{
					throw new ArgumentException("Chunked encoding must be set with the SendChunked property");
				}
				if (!this.sendChunked)
				{
					throw new ArgumentException("SendChunked must be True", "value");
				}
				this.webHeaders.CheckUpdate("Transfer-Encoding", value);
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that controls whether default credentials are sent with requests.</summary>
		/// <returns>
		///     <see langword="true" /> if the default credentials are used; otherwise <see langword="false" />. The default value is <see langword="false" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">You attempted to set this property after the request was sent.</exception>
		// Token: 0x17000687 RID: 1671
		// (get) Token: 0x06001EB2 RID: 7858 RVA: 0x0006E63D File Offset: 0x0006C83D
		// (set) Token: 0x06001EB3 RID: 7859 RVA: 0x0006E64C File Offset: 0x0006C84C
		public override bool UseDefaultCredentials
		{
			get
			{
				return CredentialCache.DefaultCredentials == this.Credentials;
			}
			set
			{
				this.Credentials = (value ? CredentialCache.DefaultCredentials : null);
			}
		}

		/// <summary>Gets or sets the value of the <see langword="User-agent" /> HTTP header.</summary>
		/// <returns>The value of the <see langword="User-agent" /> HTTP header. The default value is <see langword="null" />.The value for this property is stored in <see cref="T:System.Net.WebHeaderCollection" />. If WebHeaderCollection is set, the property value is lost.</returns>
		// Token: 0x17000688 RID: 1672
		// (get) Token: 0x06001EB4 RID: 7860 RVA: 0x0006E65F File Offset: 0x0006C85F
		// (set) Token: 0x06001EB5 RID: 7861 RVA: 0x0006E671 File Offset: 0x0006C871
		public string UserAgent
		{
			get
			{
				return this.webHeaders["User-Agent"];
			}
			set
			{
				this.webHeaders.SetInternal("User-Agent", value);
			}
		}

		/// <summary>Gets or sets a value that indicates whether to allow high-speed NTLM-authenticated connection sharing.</summary>
		/// <returns>
		///     <see langword="true" /> to keep the authenticated connection open; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000689 RID: 1673
		// (get) Token: 0x06001EB6 RID: 7862 RVA: 0x0006E684 File Offset: 0x0006C884
		// (set) Token: 0x06001EB7 RID: 7863 RVA: 0x0006E68C File Offset: 0x0006C88C
		public bool UnsafeAuthenticatedConnectionSharing
		{
			get
			{
				return this.unsafe_auth_blah;
			}
			set
			{
				this.unsafe_auth_blah = value;
			}
		}

		// Token: 0x1700068A RID: 1674
		// (get) Token: 0x06001EB8 RID: 7864 RVA: 0x0006E695 File Offset: 0x0006C895
		internal bool GotRequestStream
		{
			get
			{
				return this.gotRequestStream;
			}
		}

		// Token: 0x1700068B RID: 1675
		// (get) Token: 0x06001EB9 RID: 7865 RVA: 0x0006E69D File Offset: 0x0006C89D
		// (set) Token: 0x06001EBA RID: 7866 RVA: 0x0006E6A5 File Offset: 0x0006C8A5
		internal bool ExpectContinue
		{
			get
			{
				return this.expectContinue;
			}
			set
			{
				this.expectContinue = value;
			}
		}

		// Token: 0x1700068C RID: 1676
		// (get) Token: 0x06001EBB RID: 7867 RVA: 0x0006DE34 File Offset: 0x0006C034
		internal Uri AuthUri
		{
			get
			{
				return this.actualUri;
			}
		}

		// Token: 0x1700068D RID: 1677
		// (get) Token: 0x06001EBC RID: 7868 RVA: 0x0006E6AE File Offset: 0x0006C8AE
		internal bool ProxyQuery
		{
			get
			{
				return this.servicePoint.UsesProxy && !this.servicePoint.UseConnect;
			}
		}

		// Token: 0x1700068E RID: 1678
		// (get) Token: 0x06001EBD RID: 7869 RVA: 0x0006E6CD File Offset: 0x0006C8CD
		internal ServerCertValidationCallback ServerCertValidationCallback
		{
			get
			{
				return this.certValidationCallback;
			}
		}

		/// <summary>Gets or sets a callback function to validate the server certificate.</summary>
		/// <returns>A callback function to validate the server certificate.A callback function to validate the server certificate.</returns>
		// Token: 0x1700068F RID: 1679
		// (get) Token: 0x06001EBE RID: 7870 RVA: 0x0006E6D5 File Offset: 0x0006C8D5
		// (set) Token: 0x06001EBF RID: 7871 RVA: 0x0006E6EC File Offset: 0x0006C8EC
		public RemoteCertificateValidationCallback ServerCertificateValidationCallback
		{
			get
			{
				if (this.certValidationCallback == null)
				{
					return null;
				}
				return this.certValidationCallback.ValidationCallback;
			}
			set
			{
				if (value == null)
				{
					this.certValidationCallback = null;
					return;
				}
				this.certValidationCallback = new ServerCertValidationCallback(value);
			}
		}

		// Token: 0x06001EC0 RID: 7872 RVA: 0x0006E708 File Offset: 0x0006C908
		internal ServicePoint GetServicePoint()
		{
			object obj = this.locker;
			lock (obj)
			{
				if (this.hostChanged || this.servicePoint == null)
				{
					this.servicePoint = ServicePointManager.FindServicePoint(this.actualUri, this.proxy);
					this.hostChanged = false;
				}
			}
			return this.servicePoint;
		}

		/// <summary>Adds a byte range header to a request for a specific range from the beginning or end of the requested data.</summary>
		/// <param name="range">The starting or ending point of the range. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rangeSpecifier" /> is invalid. </exception>
		/// <exception cref="T:System.InvalidOperationException">The range header could not be added. </exception>
		// Token: 0x06001EC1 RID: 7873 RVA: 0x0006E778 File Offset: 0x0006C978
		public void AddRange(int range)
		{
			this.AddRange("bytes", (long)range);
		}

		/// <summary>Adds a byte range header to the request for a specified range.</summary>
		/// <param name="from">The position at which to start sending data. </param>
		/// <param name="to">The position at which to stop sending data. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rangeSpecifier" /> is invalid. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="from" /> is greater than <paramref name="to" />-or- 
		///         <paramref name="from" /> or <paramref name="to" /> is less than 0. </exception>
		/// <exception cref="T:System.InvalidOperationException">The range header could not be added. </exception>
		// Token: 0x06001EC2 RID: 7874 RVA: 0x0006E787 File Offset: 0x0006C987
		public void AddRange(int from, int to)
		{
			this.AddRange("bytes", (long)from, (long)to);
		}

		/// <summary>Adds a Range header to a request for a specific range from the beginning or end of the requested data.</summary>
		/// <param name="rangeSpecifier">The description of the range. </param>
		/// <param name="range">The starting or ending point of the range. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="rangeSpecifier" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rangeSpecifier" /> is invalid. </exception>
		/// <exception cref="T:System.InvalidOperationException">The range header could not be added. </exception>
		// Token: 0x06001EC3 RID: 7875 RVA: 0x0006E798 File Offset: 0x0006C998
		public void AddRange(string rangeSpecifier, int range)
		{
			this.AddRange(rangeSpecifier, (long)range);
		}

		/// <summary>Adds a range header to a request for a specified range.</summary>
		/// <param name="rangeSpecifier">The description of the range. </param>
		/// <param name="from">The position at which to start sending data. </param>
		/// <param name="to">The position at which to stop sending data. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="rangeSpecifier" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="from" /> is greater than <paramref name="to" />-or- 
		///         <paramref name="from" /> or <paramref name="to" /> is less than 0. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rangeSpecifier" /> is invalid. </exception>
		/// <exception cref="T:System.InvalidOperationException">The range header could not be added. </exception>
		// Token: 0x06001EC4 RID: 7876 RVA: 0x0006E7A3 File Offset: 0x0006C9A3
		public void AddRange(string rangeSpecifier, int from, int to)
		{
			this.AddRange(rangeSpecifier, (long)from, (long)to);
		}

		/// <summary>Adds a byte range header to a request for a specific range from the beginning or end of the requested data.</summary>
		/// <param name="range">The starting or ending point of the range.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rangeSpecifier" /> is invalid. </exception>
		/// <exception cref="T:System.InvalidOperationException">The range header could not be added. </exception>
		// Token: 0x06001EC5 RID: 7877 RVA: 0x0006E7B0 File Offset: 0x0006C9B0
		public void AddRange(long range)
		{
			this.AddRange("bytes", range);
		}

		/// <summary>Adds a byte range header to the request for a specified range.</summary>
		/// <param name="from">The position at which to start sending data.</param>
		/// <param name="to">The position at which to stop sending data.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rangeSpecifier" /> is invalid. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="from" /> is greater than <paramref name="to" />-or- 
		///         <paramref name="from" /> or <paramref name="to" /> is less than 0. </exception>
		/// <exception cref="T:System.InvalidOperationException">The range header could not be added. </exception>
		// Token: 0x06001EC6 RID: 7878 RVA: 0x0006E7BE File Offset: 0x0006C9BE
		public void AddRange(long from, long to)
		{
			this.AddRange("bytes", from, to);
		}

		/// <summary>Adds a Range header to a request for a specific range from the beginning or end of the requested data.</summary>
		/// <param name="rangeSpecifier">The description of the range.</param>
		/// <param name="range">The starting or ending point of the range.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="rangeSpecifier" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rangeSpecifier" /> is invalid. </exception>
		/// <exception cref="T:System.InvalidOperationException">The range header could not be added. </exception>
		// Token: 0x06001EC7 RID: 7879 RVA: 0x0006E7D0 File Offset: 0x0006C9D0
		public void AddRange(string rangeSpecifier, long range)
		{
			if (rangeSpecifier == null)
			{
				throw new ArgumentNullException("rangeSpecifier");
			}
			if (!WebHeaderCollection.IsValidToken(rangeSpecifier))
			{
				throw new ArgumentException("Invalid range specifier", "rangeSpecifier");
			}
			string text = this.webHeaders["Range"];
			if (text == null)
			{
				text = rangeSpecifier + "=";
			}
			else
			{
				if (string.Compare(text.Substring(0, text.IndexOf('=')), rangeSpecifier, StringComparison.OrdinalIgnoreCase) != 0)
				{
					throw new InvalidOperationException("A different range specifier is already in use");
				}
				text += ",";
			}
			string text2 = range.ToString(CultureInfo.InvariantCulture);
			if (range < 0L)
			{
				text = text + "0" + text2;
			}
			else
			{
				text = text + text2 + "-";
			}
			this.webHeaders.ChangeInternal("Range", text);
		}

		/// <summary>Adds a range header to a request for a specified range.</summary>
		/// <param name="rangeSpecifier">The description of the range.</param>
		/// <param name="from">The position at which to start sending data.</param>
		/// <param name="to">The position at which to stop sending data.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="rangeSpecifier" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="from" /> is greater than <paramref name="to" />-or- 
		///         <paramref name="from" /> or <paramref name="to" /> is less than 0. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rangeSpecifier" /> is invalid. </exception>
		/// <exception cref="T:System.InvalidOperationException">The range header could not be added. </exception>
		// Token: 0x06001EC8 RID: 7880 RVA: 0x0006E894 File Offset: 0x0006CA94
		public void AddRange(string rangeSpecifier, long from, long to)
		{
			if (rangeSpecifier == null)
			{
				throw new ArgumentNullException("rangeSpecifier");
			}
			if (!WebHeaderCollection.IsValidToken(rangeSpecifier))
			{
				throw new ArgumentException("Invalid range specifier", "rangeSpecifier");
			}
			if (from > to || from < 0L)
			{
				throw new ArgumentOutOfRangeException("from");
			}
			if (to < 0L)
			{
				throw new ArgumentOutOfRangeException("to");
			}
			string text = this.webHeaders["Range"];
			if (text == null)
			{
				text = rangeSpecifier + "=";
			}
			else
			{
				text += ",";
			}
			text = string.Format("{0}{1}-{2}", text, from, to);
			this.webHeaders.ChangeInternal("Range", text);
		}

		/// <summary>Begins an asynchronous request for a <see cref="T:System.IO.Stream" /> object to use to write data.</summary>
		/// <param name="callback">The <see cref="T:System.AsyncCallback" /> delegate. </param>
		/// <param name="state">The state object for this request. </param>
		/// <returns>An <see cref="T:System.IAsyncResult" /> that references the asynchronous request.</returns>
		/// <exception cref="T:System.Net.ProtocolViolationException">The <see cref="P:System.Net.HttpWebRequest.Method" /> property is GET or HEAD.-or- 
		///         <see cref="P:System.Net.HttpWebRequest.KeepAlive" /> is <see langword="true" />, <see cref="P:System.Net.HttpWebRequest.AllowWriteStreamBuffering" /> is <see langword="false" />, <see cref="P:System.Net.HttpWebRequest.ContentLength" /> is -1, <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" />, and <see cref="P:System.Net.HttpWebRequest.Method" /> is POST or PUT. </exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is being used by a previous call to <see cref="M:System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" />-or- 
		///         <see cref="P:System.Net.HttpWebRequest.TransferEncoding" /> is set to a value and <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" />.-or- The thread pool is running out of threads. </exception>
		/// <exception cref="T:System.NotSupportedException">The request cache validator indicated that the response for this request can be served from the cache; however, requests that write data must not use the cache. This exception can occur if you are using a custom cache validator that is incorrectly implemented. </exception>
		/// <exception cref="T:System.Net.WebException">
		///         <see cref="M:System.Net.HttpWebRequest.Abort" /> was previously called. </exception>
		/// <exception cref="T:System.ObjectDisposedException">In a .NET Compact Framework application, a request stream with zero content length was not obtained and closed correctly. For more information about handling zero content length requests, see Network Programming in the .NET Compact Framework.</exception>
		// Token: 0x06001EC9 RID: 7881 RVA: 0x0006E944 File Offset: 0x0006CB44
		public override IAsyncResult BeginGetRequestStream(AsyncCallback callback, object state)
		{
			if (this.Aborted)
			{
				throw new WebException("The request was canceled.", WebExceptionStatus.RequestCanceled);
			}
			bool flag = !(this.method == "GET") && !(this.method == "CONNECT") && !(this.method == "HEAD") && !(this.method == "TRACE");
			if (this.method == null || !flag)
			{
				throw new ProtocolViolationException("Cannot send data when method is: " + this.method);
			}
			if (this.contentLength == -1L && !this.sendChunked && !this.allowBuffering && this.KeepAlive)
			{
				throw new ProtocolViolationException("Content-Length not set");
			}
			string transferEncoding = this.TransferEncoding;
			if (!this.sendChunked && transferEncoding != null && transferEncoding.Trim() != "")
			{
				throw new ProtocolViolationException("SendChunked should be true.");
			}
			object obj = this.locker;
			IAsyncResult result;
			lock (obj)
			{
				if (this.getResponseCalled)
				{
					throw new InvalidOperationException("The operation cannot be performed once the request has been submitted.");
				}
				if (this.asyncWrite != null)
				{
					throw new InvalidOperationException("Cannot re-call start of asynchronous method while a previous call is still in progress.");
				}
				this.asyncWrite = new WebAsyncResult(this, callback, state);
				this.initialMethod = this.method;
				if (this.haveRequest && this.writeStream != null)
				{
					this.asyncWrite.SetCompleted(true, this.writeStream);
					this.asyncWrite.DoCallback();
					result = this.asyncWrite;
				}
				else
				{
					this.gotRequestStream = true;
					IAsyncResult asyncResult = this.asyncWrite;
					if (!this.requestSent)
					{
						this.requestSent = true;
						this.redirects = 0;
						this.servicePoint = this.GetServicePoint();
						this.abortHandler = this.servicePoint.SendRequest(this, this.connectionGroup);
					}
					result = asyncResult;
				}
			}
			return result;
		}

		/// <summary>Ends an asynchronous request for a <see cref="T:System.IO.Stream" /> object to use to write data.</summary>
		/// <param name="asyncResult">The pending request for a stream. </param>
		/// <returns>A <see cref="T:System.IO.Stream" /> to use to write request data.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="asyncResult" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.IOException">The request did not complete, and no stream is available. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="asyncResult" /> was not returned by the current instance from a call to <see cref="M:System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">This method was called previously using <paramref name="asyncResult" />. </exception>
		/// <exception cref="T:System.Net.WebException">
		///         <see cref="M:System.Net.HttpWebRequest.Abort" /> was previously called.-or- An error occurred while processing the request. </exception>
		// Token: 0x06001ECA RID: 7882 RVA: 0x0006EB20 File Offset: 0x0006CD20
		public override Stream EndGetRequestStream(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			WebAsyncResult webAsyncResult = asyncResult as WebAsyncResult;
			if (webAsyncResult == null)
			{
				throw new ArgumentException("Invalid IAsyncResult");
			}
			this.asyncWrite = webAsyncResult;
			webAsyncResult.WaitUntilComplete();
			Exception exception = webAsyncResult.Exception;
			if (exception != null)
			{
				throw exception;
			}
			return webAsyncResult.WriteStream;
		}

		/// <summary>Gets a <see cref="T:System.IO.Stream" /> object to use to write request data.</summary>
		/// <returns>A <see cref="T:System.IO.Stream" /> to use to write request data.</returns>
		/// <exception cref="T:System.Net.ProtocolViolationException">The <see cref="P:System.Net.HttpWebRequest.Method" /> property is GET or HEAD.-or- 
		///         <see cref="P:System.Net.HttpWebRequest.KeepAlive" /> is <see langword="true" />, <see cref="P:System.Net.HttpWebRequest.AllowWriteStreamBuffering" /> is <see langword="false" />, <see cref="P:System.Net.HttpWebRequest.ContentLength" /> is -1, <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" />, and <see cref="P:System.Net.HttpWebRequest.Method" /> is POST or PUT. </exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="M:System.Net.HttpWebRequest.GetRequestStream" /> method is called more than once.-or- 
		///         <see cref="P:System.Net.HttpWebRequest.TransferEncoding" /> is set to a value and <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The request cache validator indicated that the response for this request can be served from the cache; however, requests that write data must not use the cache. This exception can occur if you are using a custom cache validator that is incorrectly implemented. </exception>
		/// <exception cref="T:System.Net.WebException">
		///         <see cref="M:System.Net.HttpWebRequest.Abort" /> was previously called.-or- The time-out period for the request expired.-or- An error occurred while processing the request. </exception>
		/// <exception cref="T:System.ObjectDisposedException">In a .NET Compact Framework application, a request stream with zero content length was not obtained and closed correctly. For more information about handling zero content length requests, see Network Programming in the .NET Compact Framework.</exception>
		// Token: 0x06001ECB RID: 7883 RVA: 0x0006EB70 File Offset: 0x0006CD70
		public override Stream GetRequestStream()
		{
			IAsyncResult asyncResult = this.asyncWrite;
			if (asyncResult == null)
			{
				asyncResult = this.BeginGetRequestStream(null, null);
				this.asyncWrite = (WebAsyncResult)asyncResult;
			}
			if (!asyncResult.IsCompleted && !asyncResult.AsyncWaitHandle.WaitOne(this.timeout, false))
			{
				this.Abort();
				throw new WebException("The request timed out", WebExceptionStatus.Timeout);
			}
			return this.EndGetRequestStream(asyncResult);
		}

		/// <summary>Gets a <see cref="T:System.IO.Stream" /> object to use to write request data and outputs the <see cref="T:System.Net.TransportContext" /> associated with the stream.</summary>
		/// <param name="context">The <see cref="T:System.Net.TransportContext" /> for the <see cref="T:System.IO.Stream" />.</param>
		/// <returns>A <see cref="T:System.IO.Stream" /> to use to write request data.</returns>
		/// <exception cref="T:System.Exception">The <see cref="M:System.Net.HttpWebRequest.GetRequestStream" /> method was unable to obtain the <see cref="T:System.IO.Stream" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="M:System.Net.HttpWebRequest.GetRequestStream" /> method is called more than once.-or- 
		///         <see cref="P:System.Net.HttpWebRequest.TransferEncoding" /> is set to a value and <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The request cache validator indicated that the response for this request can be served from the cache; however, requests that write data must not use the cache. This exception can occur if you are using a custom cache validator that is incorrectly implemented. </exception>
		/// <exception cref="T:System.Net.ProtocolViolationException">The <see cref="P:System.Net.HttpWebRequest.Method" /> property is GET or HEAD.-or- 
		///         <see cref="P:System.Net.HttpWebRequest.KeepAlive" /> is <see langword="true" />, <see cref="P:System.Net.HttpWebRequest.AllowWriteStreamBuffering" /> is <see langword="false" />, <see cref="P:System.Net.HttpWebRequest.ContentLength" /> is -1, <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" />, and <see cref="P:System.Net.HttpWebRequest.Method" /> is POST or PUT. </exception>
		/// <exception cref="T:System.Net.WebException">
		///         <see cref="M:System.Net.HttpWebRequest.Abort" /> was previously called.-or- The time-out period for the request expired.-or- An error occurred while processing the request. </exception>
		// Token: 0x06001ECC RID: 7884 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public Stream GetRequestStream(out TransportContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001ECD RID: 7885 RVA: 0x0006EBD4 File Offset: 0x0006CDD4
		private bool CheckIfForceWrite(SimpleAsyncResult result)
		{
			if (this.writeStream == null || this.writeStream.RequestWritten || !this.InternalAllowBuffering)
			{
				return false;
			}
			if (this.contentLength < 0L && this.writeStream.CanWrite && this.writeStream.WriteBufferLength < 0)
			{
				return false;
			}
			if (this.contentLength < 0L && this.writeStream.WriteBufferLength >= 0)
			{
				this.InternalContentLength = (long)this.writeStream.WriteBufferLength;
			}
			return ((long)this.writeStream.WriteBufferLength == this.contentLength || (this.contentLength == -1L && !this.writeStream.CanWrite)) && this.writeStream.WriteRequestAsync(result);
		}

		/// <summary>Begins an asynchronous request to an Internet resource.</summary>
		/// <param name="callback">The <see cref="T:System.AsyncCallback" /> delegate </param>
		/// <param name="state">The state object for this request. </param>
		/// <returns>An <see cref="T:System.IAsyncResult" /> that references the asynchronous request for a response.</returns>
		/// <exception cref="T:System.InvalidOperationException">The stream is already in use by a previous call to <see cref="M:System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" />-or- 
		///         <see cref="P:System.Net.HttpWebRequest.TransferEncoding" /> is set to a value and <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" />.-or- The thread pool is running out of threads. </exception>
		/// <exception cref="T:System.Net.ProtocolViolationException">
		///         <see cref="P:System.Net.HttpWebRequest.Method" /> is GET or HEAD, and either <see cref="P:System.Net.HttpWebRequest.ContentLength" /> is greater than zero or <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="true" />.-or- 
		///         <see cref="P:System.Net.HttpWebRequest.KeepAlive" /> is <see langword="true" />, <see cref="P:System.Net.HttpWebRequest.AllowWriteStreamBuffering" /> is <see langword="false" />, and either <see cref="P:System.Net.HttpWebRequest.ContentLength" /> is -1, <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" /> and <see cref="P:System.Net.HttpWebRequest.Method" /> is POST or PUT.-or- The <see cref="T:System.Net.HttpWebRequest" /> has an entity body but the <see cref="M:System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" /> method is called without calling the <see cref="M:System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" /> method. -or- The <see cref="P:System.Net.HttpWebRequest.ContentLength" /> is greater than zero, but the application does not write all of the promised data.</exception>
		/// <exception cref="T:System.Net.WebException">
		///         <see cref="M:System.Net.HttpWebRequest.Abort" /> was previously called. </exception>
		// Token: 0x06001ECE RID: 7886 RVA: 0x0006EC8C File Offset: 0x0006CE8C
		public override IAsyncResult BeginGetResponse(AsyncCallback callback, object state)
		{
			if (this.Aborted)
			{
				throw new WebException("The request was canceled.", WebExceptionStatus.RequestCanceled);
			}
			if (this.method == null)
			{
				throw new ProtocolViolationException("Method is null.");
			}
			string transferEncoding = this.TransferEncoding;
			if (!this.sendChunked && transferEncoding != null && transferEncoding.Trim() != "")
			{
				throw new ProtocolViolationException("SendChunked should be true.");
			}
			Monitor.Enter(this.locker);
			this.getResponseCalled = true;
			if (this.asyncRead != null && !this.haveResponse)
			{
				Monitor.Exit(this.locker);
				throw new InvalidOperationException("Cannot re-call start of asynchronous method while a previous call is still in progress.");
			}
			this.asyncRead = new WebAsyncResult(this, callback, state);
			WebAsyncResult aread = this.asyncRead;
			this.initialMethod = this.method;
			SimpleAsyncResult.RunWithLock(this.locker, new Func<SimpleAsyncResult, bool>(this.CheckIfForceWrite), delegate(SimpleAsyncResult inner)
			{
				bool completedSynchronouslyPeek = inner.CompletedSynchronouslyPeek;
				if (inner.GotException)
				{
					aread.SetCompleted(completedSynchronouslyPeek, inner.Exception);
					aread.DoCallback();
					return;
				}
				if (this.haveResponse)
				{
					Exception ex = this.saved_exc;
					if (this.webResponse != null)
					{
						if (ex == null)
						{
							aread.SetCompleted(completedSynchronouslyPeek, this.webResponse);
						}
						else
						{
							aread.SetCompleted(completedSynchronouslyPeek, ex);
						}
						aread.DoCallback();
						return;
					}
					if (ex != null)
					{
						aread.SetCompleted(completedSynchronouslyPeek, ex);
						aread.DoCallback();
						return;
					}
				}
				if (this.requestSent)
				{
					return;
				}
				try
				{
					this.requestSent = true;
					this.redirects = 0;
					this.servicePoint = this.GetServicePoint();
					this.abortHandler = this.servicePoint.SendRequest(this, this.connectionGroup);
				}
				catch (Exception e)
				{
					aread.SetCompleted(completedSynchronouslyPeek, e);
					aread.DoCallback();
				}
			});
			return aread;
		}

		/// <summary>Ends an asynchronous request to an Internet resource.</summary>
		/// <param name="asyncResult">The pending request for a response. </param>
		/// <returns>A <see cref="T:System.Net.WebResponse" /> that contains the response from the Internet resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="asyncResult" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">This method was called previously using <paramref name="asyncResult." />-or- The <see cref="P:System.Net.HttpWebRequest.ContentLength" /> property is greater than 0 but the data has not been written to the request stream. </exception>
		/// <exception cref="T:System.Net.WebException">
		///         <see cref="M:System.Net.HttpWebRequest.Abort" /> was previously called.-or- An error occurred while processing the request. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="asyncResult" /> was not returned by the current instance from a call to <see cref="M:System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" />. </exception>
		// Token: 0x06001ECF RID: 7887 RVA: 0x0006ED84 File Offset: 0x0006CF84
		public override WebResponse EndGetResponse(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			WebAsyncResult webAsyncResult = asyncResult as WebAsyncResult;
			if (webAsyncResult == null)
			{
				throw new ArgumentException("Invalid IAsyncResult", "asyncResult");
			}
			if (!webAsyncResult.WaitUntilComplete(this.timeout, false))
			{
				this.Abort();
				throw new WebException("The request timed out", WebExceptionStatus.Timeout);
			}
			if (webAsyncResult.GotException)
			{
				throw webAsyncResult.Exception;
			}
			return webAsyncResult.Response;
		}

		/// <summary>Ends an asynchronous request for a <see cref="T:System.IO.Stream" /> object to use to write data and outputs the <see cref="T:System.Net.TransportContext" /> associated with the stream.</summary>
		/// <param name="asyncResult">The pending request for a stream.</param>
		/// <param name="context">The <see cref="T:System.Net.TransportContext" /> for the <see cref="T:System.IO.Stream" />.</param>
		/// <returns>A <see cref="T:System.IO.Stream" /> to use to write request data.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="asyncResult" /> was not returned by the current instance from a call to <see cref="M:System.Net.HttpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="asyncResult" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">This method was called previously using <paramref name="asyncResult" />. </exception>
		/// <exception cref="T:System.IO.IOException">The request did not complete, and no stream is available. </exception>
		/// <exception cref="T:System.Net.WebException">
		///         <see cref="M:System.Net.HttpWebRequest.Abort" /> was previously called.-or- An error occurred while processing the request. </exception>
		// Token: 0x06001ED0 RID: 7888 RVA: 0x0006EDF0 File Offset: 0x0006CFF0
		public Stream EndGetRequestStream(IAsyncResult asyncResult, out TransportContext context)
		{
			context = null;
			return this.EndGetRequestStream(asyncResult);
		}

		/// <summary>Returns a response from an Internet resource.</summary>
		/// <returns>A <see cref="T:System.Net.WebResponse" /> that contains the response from the Internet resource.</returns>
		/// <exception cref="T:System.InvalidOperationException">The stream is already in use by a previous call to <see cref="M:System.Net.HttpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" />.-or- 
		///         <see cref="P:System.Net.HttpWebRequest.TransferEncoding" /> is set to a value and <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" />. </exception>
		/// <exception cref="T:System.Net.ProtocolViolationException">
		///         <see cref="P:System.Net.HttpWebRequest.Method" /> is GET or HEAD, and either <see cref="P:System.Net.HttpWebRequest.ContentLength" /> is greater or equal to zero or <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="true" />.-or- 
		///         <see cref="P:System.Net.HttpWebRequest.KeepAlive" /> is <see langword="true" />, <see cref="P:System.Net.HttpWebRequest.AllowWriteStreamBuffering" /> is <see langword="false" />, <see cref="P:System.Net.HttpWebRequest.ContentLength" /> is -1, <see cref="P:System.Net.HttpWebRequest.SendChunked" /> is <see langword="false" />, and <see cref="P:System.Net.HttpWebRequest.Method" /> is POST or PUT. -or- The <see cref="T:System.Net.HttpWebRequest" /> has an entity body but the <see cref="M:System.Net.HttpWebRequest.GetResponse" /> method is called without calling the <see cref="M:System.Net.HttpWebRequest.GetRequestStream" /> method. -or- The <see cref="P:System.Net.HttpWebRequest.ContentLength" /> is greater than zero, but the application does not write all of the promised data.</exception>
		/// <exception cref="T:System.NotSupportedException">The request cache validator indicated that the response for this request can be served from the cache; however, this request includes data to be sent to the server. Requests that send data must not use the cache. This exception can occur if you are using a custom cache validator that is incorrectly implemented. </exception>
		/// <exception cref="T:System.Net.WebException">
		///         <see cref="M:System.Net.HttpWebRequest.Abort" /> was previously called.-or- The time-out period for the request expired.-or- An error occurred while processing the request. </exception>
		// Token: 0x06001ED1 RID: 7889 RVA: 0x0006EDFC File Offset: 0x0006CFFC
		public override WebResponse GetResponse()
		{
			WebAsyncResult asyncResult = (WebAsyncResult)this.BeginGetResponse(null, null);
			return this.EndGetResponse(asyncResult);
		}

		// Token: 0x17000690 RID: 1680
		// (get) Token: 0x06001ED2 RID: 7890 RVA: 0x0006EE1E File Offset: 0x0006D01E
		// (set) Token: 0x06001ED3 RID: 7891 RVA: 0x0006EE26 File Offset: 0x0006D026
		internal bool FinishedReading
		{
			get
			{
				return this.finished_reading;
			}
			set
			{
				this.finished_reading = value;
			}
		}

		// Token: 0x17000691 RID: 1681
		// (get) Token: 0x06001ED4 RID: 7892 RVA: 0x0006EE2F File Offset: 0x0006D02F
		internal bool Aborted
		{
			get
			{
				return Interlocked.CompareExchange(ref this.aborted, 0, 0) == 1;
			}
		}

		/// <summary>Cancels a request to an Internet resource.</summary>
		// Token: 0x06001ED5 RID: 7893 RVA: 0x0006EE44 File Offset: 0x0006D044
		public override void Abort()
		{
			if (Interlocked.CompareExchange(ref this.aborted, 1, 0) == 1)
			{
				return;
			}
			if (this.haveResponse && this.finished_reading)
			{
				return;
			}
			this.haveResponse = true;
			if (this.abortHandler != null)
			{
				try
				{
					this.abortHandler(this, EventArgs.Empty);
				}
				catch (Exception)
				{
				}
				this.abortHandler = null;
			}
			if (this.asyncWrite != null)
			{
				WebAsyncResult webAsyncResult = this.asyncWrite;
				if (!webAsyncResult.IsCompleted)
				{
					try
					{
						WebException e = new WebException("Aborted.", WebExceptionStatus.RequestCanceled);
						webAsyncResult.SetCompleted(false, e);
						webAsyncResult.DoCallback();
					}
					catch
					{
					}
				}
				this.asyncWrite = null;
			}
			if (this.asyncRead != null)
			{
				WebAsyncResult webAsyncResult2 = this.asyncRead;
				if (!webAsyncResult2.IsCompleted)
				{
					try
					{
						WebException e2 = new WebException("Aborted.", WebExceptionStatus.RequestCanceled);
						webAsyncResult2.SetCompleted(false, e2);
						webAsyncResult2.DoCallback();
					}
					catch
					{
					}
				}
				this.asyncRead = null;
			}
			if (this.writeStream != null)
			{
				try
				{
					this.writeStream.Close();
					this.writeStream = null;
				}
				catch
				{
				}
			}
			if (this.webResponse != null)
			{
				try
				{
					this.webResponse.Close();
					this.webResponse = null;
				}
				catch
				{
				}
			}
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.</summary>
		/// <param name="serializationInfo">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data. </param>
		/// <param name="streamingContext">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that specifies the destination for this serialization.</param>
		// Token: 0x06001ED6 RID: 7894 RVA: 0x00056302 File Offset: 0x00054502
		void ISerializable.GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			this.GetObjectData(serializationInfo, streamingContext);
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data required to serialize the target object.</summary>
		/// <param name="serializationInfo">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data. </param>
		/// <param name="streamingContext">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that specifies the destination for this serialization.</param>
		// Token: 0x06001ED7 RID: 7895 RVA: 0x0006EF98 File Offset: 0x0006D198
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		protected override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			serializationInfo.AddValue("requestUri", this.requestUri, typeof(Uri));
			serializationInfo.AddValue("actualUri", this.actualUri, typeof(Uri));
			serializationInfo.AddValue("allowAutoRedirect", this.allowAutoRedirect);
			serializationInfo.AddValue("allowBuffering", this.allowBuffering);
			serializationInfo.AddValue("certificates", this.certificates, typeof(X509CertificateCollection));
			serializationInfo.AddValue("connectionGroup", this.connectionGroup);
			serializationInfo.AddValue("contentLength", this.contentLength);
			serializationInfo.AddValue("webHeaders", this.webHeaders, typeof(WebHeaderCollection));
			serializationInfo.AddValue("keepAlive", this.keepAlive);
			serializationInfo.AddValue("maxAutoRedirect", this.maxAutoRedirect);
			serializationInfo.AddValue("mediaType", this.mediaType);
			serializationInfo.AddValue("method", this.method);
			serializationInfo.AddValue("initialMethod", this.initialMethod);
			serializationInfo.AddValue("pipelined", this.pipelined);
			serializationInfo.AddValue("version", this.version, typeof(Version));
			serializationInfo.AddValue("proxy", this.proxy, typeof(IWebProxy));
			serializationInfo.AddValue("sendChunked", this.sendChunked);
			serializationInfo.AddValue("timeout", this.timeout);
			serializationInfo.AddValue("redirects", this.redirects);
			serializationInfo.AddValue("host", this.host);
		}

		// Token: 0x06001ED8 RID: 7896 RVA: 0x0006F135 File Offset: 0x0006D335
		private void CheckRequestStarted()
		{
			if (this.requestSent)
			{
				throw new InvalidOperationException("request started");
			}
		}

		// Token: 0x06001ED9 RID: 7897 RVA: 0x0006F14A File Offset: 0x0006D34A
		internal void DoContinueDelegate(int statusCode, WebHeaderCollection headers)
		{
			if (this.continueDelegate != null)
			{
				this.continueDelegate(statusCode, headers);
			}
		}

		// Token: 0x06001EDA RID: 7898 RVA: 0x0006F161 File Offset: 0x0006D361
		private void RewriteRedirectToGet()
		{
			this.method = "GET";
			this.webHeaders.RemoveInternal("Transfer-Encoding");
			this.sendChunked = false;
		}

		// Token: 0x06001EDB RID: 7899 RVA: 0x0006F188 File Offset: 0x0006D388
		private bool Redirect(WebAsyncResult result, HttpStatusCode code, WebResponse response)
		{
			this.redirects++;
			Exception ex = null;
			string text = null;
			switch (code)
			{
			case HttpStatusCode.MultipleChoices:
				ex = new WebException("Ambiguous redirect.");
				goto IL_94;
			case HttpStatusCode.MovedPermanently:
			case HttpStatusCode.Found:
				if (this.method == "POST")
				{
					this.RewriteRedirectToGet();
					goto IL_94;
				}
				goto IL_94;
			case HttpStatusCode.SeeOther:
				this.RewriteRedirectToGet();
				goto IL_94;
			case HttpStatusCode.NotModified:
				return false;
			case HttpStatusCode.UseProxy:
				ex = new NotImplementedException("Proxy support not available.");
				goto IL_94;
			case HttpStatusCode.TemporaryRedirect:
				goto IL_94;
			}
			ex = new ProtocolViolationException("Invalid status code: " + (int)code);
			IL_94:
			if (this.method != "GET" && !this.InternalAllowBuffering && (this.writeStream.WriteBufferLength > 0 || this.contentLength > 0L))
			{
				ex = new WebException("The request requires buffering data to succeed.", null, WebExceptionStatus.ProtocolError, this.webResponse);
			}
			if (ex != null)
			{
				throw ex;
			}
			if (this.AllowWriteStreamBuffering || this.method == "GET")
			{
				this.contentLength = -1L;
			}
			text = this.webResponse.Headers["Location"];
			if (text == null)
			{
				throw new WebException("No Location header found for " + (int)code, WebExceptionStatus.ProtocolError);
			}
			Uri uri = this.actualUri;
			try
			{
				this.actualUri = new Uri(this.actualUri, text);
			}
			catch (Exception)
			{
				throw new WebException(string.Format("Invalid URL ({0}) for {1}", text, (int)code), WebExceptionStatus.ProtocolError);
			}
			this.hostChanged = (this.actualUri.Scheme != uri.Scheme || this.Host != uri.Authority);
			return true;
		}

		// Token: 0x06001EDC RID: 7900 RVA: 0x0006F33C File Offset: 0x0006D53C
		private string GetHeaders()
		{
			bool flag = false;
			if (this.sendChunked)
			{
				flag = true;
				this.webHeaders.ChangeInternal("Transfer-Encoding", "chunked");
				this.webHeaders.RemoveInternal("Content-Length");
			}
			else if (this.contentLength != -1L)
			{
				if (this.auth_state.NtlmAuthState == HttpWebRequest.NtlmAuthState.Challenge || this.proxy_auth_state.NtlmAuthState == HttpWebRequest.NtlmAuthState.Challenge)
				{
					if (this.haveContentLength || this.gotRequestStream || this.contentLength > 0L)
					{
						this.webHeaders.SetInternal("Content-Length", "0");
					}
					else
					{
						this.webHeaders.RemoveInternal("Content-Length");
					}
				}
				else
				{
					if (this.contentLength > 0L)
					{
						flag = true;
					}
					if (this.haveContentLength || this.gotRequestStream || this.contentLength > 0L)
					{
						this.webHeaders.SetInternal("Content-Length", this.contentLength.ToString());
					}
				}
				this.webHeaders.RemoveInternal("Transfer-Encoding");
			}
			else
			{
				this.webHeaders.RemoveInternal("Content-Length");
			}
			if (this.actualVersion == HttpVersion.Version11 && flag && this.servicePoint.SendContinue)
			{
				this.webHeaders.ChangeInternal("Expect", "100-continue");
				this.expectContinue = true;
			}
			else
			{
				this.webHeaders.RemoveInternal("Expect");
				this.expectContinue = false;
			}
			bool proxyQuery = this.ProxyQuery;
			string name = proxyQuery ? "Proxy-Connection" : "Connection";
			this.webHeaders.RemoveInternal((!proxyQuery) ? "Proxy-Connection" : "Connection");
			Version protocolVersion = this.servicePoint.ProtocolVersion;
			bool flag2 = protocolVersion == null || protocolVersion == HttpVersion.Version10;
			if (this.keepAlive && (this.version == HttpVersion.Version10 || flag2))
			{
				if (this.webHeaders[name] == null || this.webHeaders[name].IndexOf("keep-alive", StringComparison.OrdinalIgnoreCase) == -1)
				{
					this.webHeaders.ChangeInternal(name, "keep-alive");
				}
			}
			else if (!this.keepAlive && this.version == HttpVersion.Version11)
			{
				this.webHeaders.ChangeInternal(name, "close");
			}
			this.webHeaders.SetInternal("Host", this.Host);
			if (this.cookieContainer != null)
			{
				string cookieHeader = this.cookieContainer.GetCookieHeader(this.actualUri);
				if (cookieHeader != "")
				{
					this.webHeaders.ChangeInternal("Cookie", cookieHeader);
				}
				else
				{
					this.webHeaders.RemoveInternal("Cookie");
				}
			}
			string text = null;
			if ((this.auto_decomp & DecompressionMethods.GZip) != DecompressionMethods.None)
			{
				text = "gzip";
			}
			if ((this.auto_decomp & DecompressionMethods.Deflate) != DecompressionMethods.None)
			{
				text = ((text != null) ? "gzip, deflate" : "deflate");
			}
			if (text != null)
			{
				this.webHeaders.ChangeInternal("Accept-Encoding", text);
			}
			if (!this.usedPreAuth && this.preAuthenticate)
			{
				this.DoPreAuthenticate();
			}
			return this.webHeaders.ToString();
		}

		// Token: 0x06001EDD RID: 7901 RVA: 0x0006F648 File Offset: 0x0006D848
		private void DoPreAuthenticate()
		{
			bool flag = this.proxy != null && !this.proxy.IsBypassed(this.actualUri);
			ICredentials credentials = (!flag || this.credentials != null) ? this.credentials : this.proxy.Credentials;
			Authorization authorization = AuthenticationManager.PreAuthenticate(this, credentials);
			if (authorization == null)
			{
				return;
			}
			this.webHeaders.RemoveInternal("Proxy-Authorization");
			this.webHeaders.RemoveInternal("Authorization");
			string name = (flag && this.credentials == null) ? "Proxy-Authorization" : "Authorization";
			this.webHeaders[name] = authorization.Message;
			this.usedPreAuth = true;
		}

		// Token: 0x06001EDE RID: 7902 RVA: 0x0006F6F4 File Offset: 0x0006D8F4
		internal void SetWriteStreamError(WebExceptionStatus status, Exception exc)
		{
			if (this.Aborted)
			{
				return;
			}
			WebAsyncResult webAsyncResult = this.asyncWrite;
			if (webAsyncResult == null)
			{
				webAsyncResult = this.asyncRead;
			}
			if (webAsyncResult != null)
			{
				WebException ex;
				if (exc == null)
				{
					ex = new WebException("Error: " + status, status);
				}
				else
				{
					ex = (exc as WebException);
					if (ex == null)
					{
						ex = new WebException(string.Format("Error: {0} ({1})", status, exc.Message), status, WebExceptionInternalStatus.RequestFatal, exc);
					}
				}
				webAsyncResult.SetCompleted(false, ex);
				webAsyncResult.DoCallback();
			}
		}

		// Token: 0x06001EDF RID: 7903 RVA: 0x0006F774 File Offset: 0x0006D974
		internal byte[] GetRequestHeaders()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string text;
			if (!this.ProxyQuery)
			{
				text = this.actualUri.PathAndQuery;
			}
			else
			{
				text = string.Format("{0}://{1}{2}", this.actualUri.Scheme, this.Host, this.actualUri.PathAndQuery);
			}
			if (!this.force_version && this.servicePoint.ProtocolVersion != null && this.servicePoint.ProtocolVersion < this.version)
			{
				this.actualVersion = this.servicePoint.ProtocolVersion;
			}
			else
			{
				this.actualVersion = this.version;
			}
			stringBuilder.AppendFormat("{0} {1} HTTP/{2}.{3}\r\n", new object[]
			{
				this.method,
				text,
				this.actualVersion.Major,
				this.actualVersion.Minor
			});
			stringBuilder.Append(this.GetHeaders());
			string s = stringBuilder.ToString();
			return Encoding.UTF8.GetBytes(s);
		}

		// Token: 0x06001EE0 RID: 7904 RVA: 0x0006F87C File Offset: 0x0006DA7C
		internal void SetWriteStream(WebConnectionStream stream)
		{
			if (this.Aborted)
			{
				return;
			}
			this.writeStream = stream;
			if (this.bodyBuffer != null)
			{
				this.webHeaders.RemoveInternal("Transfer-Encoding");
				this.contentLength = (long)this.bodyBufferLength;
				this.writeStream.SendChunked = false;
			}
			this.writeStream.SetHeadersAsync(false, delegate(SimpleAsyncResult result)
			{
				if (result.GotException)
				{
					this.SetWriteStreamError(result.Exception);
					return;
				}
				this.haveRequest = true;
				this.SetWriteStreamInner(delegate(SimpleAsyncResult inner)
				{
					if (inner.GotException)
					{
						this.SetWriteStreamError(inner.Exception);
						return;
					}
					if (this.asyncWrite != null)
					{
						this.asyncWrite.SetCompleted(inner.CompletedSynchronouslyPeek, this.writeStream);
						this.asyncWrite.DoCallback();
						this.asyncWrite = null;
					}
				});
			});
		}

		// Token: 0x06001EE1 RID: 7905 RVA: 0x0006F8E2 File Offset: 0x0006DAE2
		private void SetWriteStreamInner(SimpleAsyncCallback callback)
		{
			SimpleAsyncResult.Run(delegate(SimpleAsyncResult result)
			{
				if (this.bodyBuffer != null)
				{
					if (this.auth_state.NtlmAuthState != HttpWebRequest.NtlmAuthState.Challenge && this.proxy_auth_state.NtlmAuthState != HttpWebRequest.NtlmAuthState.Challenge)
					{
						this.writeStream.Write(this.bodyBuffer, 0, this.bodyBufferLength);
						this.bodyBuffer = null;
						this.writeStream.Close();
					}
				}
				else if (this.MethodWithBuffer && this.getResponseCalled && !this.writeStream.RequestWritten)
				{
					return this.writeStream.WriteRequestAsync(result);
				}
				return false;
			}, callback);
		}

		// Token: 0x06001EE2 RID: 7906 RVA: 0x0006F8F8 File Offset: 0x0006DAF8
		private void SetWriteStreamError(Exception exc)
		{
			WebException ex = exc as WebException;
			if (ex != null)
			{
				this.SetWriteStreamError(ex.Status, ex);
				return;
			}
			this.SetWriteStreamError(WebExceptionStatus.SendFailure, exc);
		}

		// Token: 0x06001EE3 RID: 7907 RVA: 0x0006F928 File Offset: 0x0006DB28
		internal void SetResponseError(WebExceptionStatus status, Exception e, string where)
		{
			if (this.Aborted)
			{
				return;
			}
			object obj = this.locker;
			lock (obj)
			{
				string message = string.Format("Error getting response stream ({0}): {1}", where, status);
				WebAsyncResult webAsyncResult = this.asyncRead;
				if (webAsyncResult == null)
				{
					webAsyncResult = this.asyncWrite;
				}
				WebException e2;
				if (e is WebException)
				{
					e2 = (WebException)e;
				}
				else
				{
					e2 = new WebException(message, e, status, null);
				}
				if (webAsyncResult != null)
				{
					if (!webAsyncResult.IsCompleted)
					{
						webAsyncResult.SetCompleted(false, e2);
						webAsyncResult.DoCallback();
					}
					else if (webAsyncResult == this.asyncWrite)
					{
						this.saved_exc = e2;
					}
					this.haveResponse = true;
					this.asyncRead = null;
					this.asyncWrite = null;
				}
				else
				{
					this.haveResponse = true;
					this.saved_exc = e2;
				}
			}
		}

		// Token: 0x06001EE4 RID: 7908 RVA: 0x0006FA00 File Offset: 0x0006DC00
		private void CheckSendError(WebConnectionData data)
		{
			int statusCode = data.StatusCode;
			if (statusCode < 400 || statusCode == 401 || statusCode == 407)
			{
				return;
			}
			if (this.writeStream != null && this.asyncRead == null && !this.writeStream.CompleteRequestWritten)
			{
				this.saved_exc = new WebException(data.StatusDescription, null, WebExceptionStatus.ProtocolError, this.webResponse);
				if (this.allowBuffering || this.sendChunked || this.writeStream.totalWritten >= this.contentLength)
				{
					this.webResponse.ReadAll();
					return;
				}
				this.writeStream.IgnoreIOErrors = true;
			}
		}

		// Token: 0x06001EE5 RID: 7909 RVA: 0x0006FAA0 File Offset: 0x0006DCA0
		private bool HandleNtlmAuth(WebAsyncResult r)
		{
			bool flag = this.webResponse.StatusCode == HttpStatusCode.ProxyAuthenticationRequired;
			if ((flag ? this.proxy_auth_state.NtlmAuthState : this.auth_state.NtlmAuthState) == HttpWebRequest.NtlmAuthState.None)
			{
				return false;
			}
			WebConnectionStream webConnectionStream = this.webResponse.GetResponseStream() as WebConnectionStream;
			if (webConnectionStream != null)
			{
				WebConnection connection = webConnectionStream.Connection;
				connection.PriorityRequest = this;
				ICredentials credentials = (!flag || this.proxy == null) ? this.credentials : this.proxy.Credentials;
				if (credentials != null)
				{
					connection.NtlmCredential = credentials.GetCredential(this.requestUri, "NTLM");
					connection.UnsafeAuthenticatedConnectionSharing = this.unsafe_auth_blah;
				}
			}
			r.Reset();
			this.finished_reading = false;
			this.haveResponse = false;
			this.webResponse.ReadAll();
			this.webResponse = null;
			return true;
		}

		// Token: 0x06001EE6 RID: 7910 RVA: 0x0006FB70 File Offset: 0x0006DD70
		internal void SetResponseData(WebConnectionData data)
		{
			object obj = this.locker;
			lock (obj)
			{
				if (this.Aborted)
				{
					if (data.stream != null)
					{
						data.stream.Close();
					}
				}
				else
				{
					WebException ex = null;
					try
					{
						this.webResponse = new HttpWebResponse(this.actualUri, this.method, data, this.cookieContainer);
					}
					catch (Exception ex2)
					{
						ex = new WebException(ex2.Message, ex2, WebExceptionStatus.ProtocolError, null);
						if (data.stream != null)
						{
							data.stream.Close();
						}
					}
					if (ex == null && (this.method == "POST" || this.method == "PUT"))
					{
						this.CheckSendError(data);
						if (this.saved_exc != null)
						{
							ex = (WebException)this.saved_exc;
						}
					}
					WebAsyncResult webAsyncResult = this.asyncRead;
					bool flag2 = false;
					if (webAsyncResult == null && this.webResponse != null)
					{
						flag2 = true;
						webAsyncResult = new WebAsyncResult(null, null);
						webAsyncResult.SetCompleted(false, this.webResponse);
					}
					if (webAsyncResult != null)
					{
						if (ex != null)
						{
							this.haveResponse = true;
							if (!webAsyncResult.IsCompleted)
							{
								webAsyncResult.SetCompleted(false, ex);
							}
							webAsyncResult.DoCallback();
						}
						else
						{
							bool flag3 = this.ProxyQuery && this.proxy != null && !this.proxy.IsBypassed(this.actualUri);
							try
							{
								if (!this.CheckFinalStatus(webAsyncResult))
								{
									if ((flag3 ? this.proxy_auth_state.IsNtlmAuthenticated : this.auth_state.IsNtlmAuthenticated) && this.webResponse != null && this.webResponse.StatusCode < HttpStatusCode.BadRequest)
									{
										WebConnectionStream webConnectionStream = this.webResponse.GetResponseStream() as WebConnectionStream;
										if (webConnectionStream != null)
										{
											webConnectionStream.Connection.NtlmAuthenticated = true;
										}
									}
									if (this.writeStream != null)
									{
										this.writeStream.KillBuffer();
									}
									this.haveResponse = true;
									webAsyncResult.SetCompleted(false, this.webResponse);
									webAsyncResult.DoCallback();
								}
								else
								{
									if (this.sendChunked)
									{
										this.sendChunked = false;
										this.webHeaders.RemoveInternal("Transfer-Encoding");
									}
									if (this.webResponse != null)
									{
										if (this.HandleNtlmAuth(webAsyncResult))
										{
											return;
										}
										this.webResponse.Close();
									}
									this.finished_reading = false;
									this.haveResponse = false;
									this.webResponse = null;
									webAsyncResult.Reset();
									this.servicePoint = this.GetServicePoint();
									this.abortHandler = this.servicePoint.SendRequest(this, this.connectionGroup);
								}
							}
							catch (WebException e)
							{
								if (flag2)
								{
									this.saved_exc = e;
									this.haveResponse = true;
								}
								webAsyncResult.SetCompleted(false, e);
								webAsyncResult.DoCallback();
							}
							catch (Exception ex3)
							{
								ex = new WebException(ex3.Message, ex3, WebExceptionStatus.ProtocolError, null);
								if (flag2)
								{
									this.saved_exc = ex;
									this.haveResponse = true;
								}
								webAsyncResult.SetCompleted(false, ex);
								webAsyncResult.DoCallback();
							}
						}
					}
				}
			}
		}

		// Token: 0x06001EE7 RID: 7911 RVA: 0x0006FEA0 File Offset: 0x0006E0A0
		private bool CheckAuthorization(WebResponse response, HttpStatusCode code)
		{
			if (code != HttpStatusCode.ProxyAuthenticationRequired)
			{
				return this.auth_state.CheckAuthorization(response, code);
			}
			return this.proxy_auth_state.CheckAuthorization(response, code);
		}

		// Token: 0x06001EE8 RID: 7912 RVA: 0x0006FEC8 File Offset: 0x0006E0C8
		private bool CheckFinalStatus(WebAsyncResult result)
		{
			if (result.GotException)
			{
				this.bodyBuffer = null;
				throw result.Exception;
			}
			Exception ex = result.Exception;
			HttpWebResponse response = result.Response;
			WebExceptionStatus status = WebExceptionStatus.ProtocolError;
			HttpStatusCode httpStatusCode = (HttpStatusCode)0;
			if (ex == null && this.webResponse != null)
			{
				httpStatusCode = this.webResponse.StatusCode;
				if (((!this.auth_state.IsCompleted && httpStatusCode == HttpStatusCode.Unauthorized && this.credentials != null) || (this.ProxyQuery && !this.proxy_auth_state.IsCompleted && httpStatusCode == HttpStatusCode.ProxyAuthenticationRequired)) && !this.usedPreAuth && this.CheckAuthorization(this.webResponse, httpStatusCode))
				{
					if (this.MethodWithBuffer)
					{
						if (this.AllowWriteStreamBuffering)
						{
							if (this.writeStream.WriteBufferLength > 0)
							{
								this.bodyBuffer = this.writeStream.WriteBuffer;
								this.bodyBufferLength = this.writeStream.WriteBufferLength;
							}
							return true;
						}
						if (this.ResendContentFactory != null)
						{
							using (MemoryStream memoryStream = new MemoryStream())
							{
								this.ResendContentFactory(memoryStream);
								this.bodyBuffer = memoryStream.ToArray();
								this.bodyBufferLength = this.bodyBuffer.Length;
							}
							return true;
						}
					}
					else if (this.method != "PUT" && this.method != "POST")
					{
						this.bodyBuffer = null;
						return true;
					}
					if (!this.ThrowOnError)
					{
						return false;
					}
					this.writeStream.InternalClose();
					this.writeStream = null;
					this.webResponse.Close();
					this.webResponse = null;
					this.bodyBuffer = null;
					throw new WebException("This request requires buffering of data for authentication or redirection to be sucessful.");
				}
				else
				{
					this.bodyBuffer = null;
					if (httpStatusCode >= HttpStatusCode.BadRequest)
					{
						ex = new WebException(string.Format("The remote server returned an error: ({0}) {1}.", (int)httpStatusCode, this.webResponse.StatusDescription), null, status, this.webResponse);
						this.webResponse.ReadAll();
					}
					else if (httpStatusCode == HttpStatusCode.NotModified && this.allowAutoRedirect)
					{
						ex = new WebException(string.Format("The remote server returned an error: ({0}) {1}.", (int)httpStatusCode, this.webResponse.StatusDescription), null, status, this.webResponse);
					}
					else if (httpStatusCode >= HttpStatusCode.MultipleChoices && this.allowAutoRedirect && this.redirects >= this.maxAutoRedirect)
					{
						ex = new WebException("Max. redirections exceeded.", null, status, this.webResponse);
						this.webResponse.ReadAll();
					}
				}
			}
			this.bodyBuffer = null;
			if (ex == null)
			{
				bool flag = false;
				int num = (int)httpStatusCode;
				if (this.allowAutoRedirect && num >= 300)
				{
					flag = this.Redirect(result, httpStatusCode, this.webResponse);
					if (this.InternalAllowBuffering && this.writeStream.WriteBufferLength > 0)
					{
						this.bodyBuffer = this.writeStream.WriteBuffer;
						this.bodyBufferLength = this.writeStream.WriteBufferLength;
					}
					if (flag && !this.unsafe_auth_blah)
					{
						this.auth_state.Reset();
						this.proxy_auth_state.Reset();
					}
				}
				if (response != null && num >= 300 && num != 304)
				{
					response.ReadAll();
				}
				return flag;
			}
			if (!this.ThrowOnError)
			{
				return false;
			}
			if (this.writeStream != null)
			{
				this.writeStream.InternalClose();
				this.writeStream = null;
			}
			this.webResponse = null;
			throw ex;
		}

		// Token: 0x17000692 RID: 1682
		// (get) Token: 0x06001EE9 RID: 7913 RVA: 0x00070218 File Offset: 0x0006E418
		// (set) Token: 0x06001EEA RID: 7914 RVA: 0x00070220 File Offset: 0x0006E420
		internal bool ReuseConnection
		{
			[CompilerGenerated]
			get
			{
				return this.<ReuseConnection>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ReuseConnection>k__BackingField = value;
			}
		}

		// Token: 0x06001EEB RID: 7915 RVA: 0x0007022C File Offset: 0x0006E42C
		internal static StringBuilder GenerateConnectionGroup(string connectionGroupName, bool unsafeConnectionGroup, bool isInternalGroup)
		{
			StringBuilder stringBuilder = new StringBuilder(connectionGroupName);
			stringBuilder.Append(unsafeConnectionGroup ? "U>" : "S>");
			if (isInternalGroup)
			{
				stringBuilder.Append("I>");
			}
			return stringBuilder;
		}

		// Token: 0x06001EEC RID: 7916 RVA: 0x00070266 File Offset: 0x0006E466
		[CompilerGenerated]
		private void <SetWriteStream>b__258_0(SimpleAsyncResult result)
		{
			if (result.GotException)
			{
				this.SetWriteStreamError(result.Exception);
				return;
			}
			this.haveRequest = true;
			this.SetWriteStreamInner(delegate(SimpleAsyncResult inner)
			{
				if (inner.GotException)
				{
					this.SetWriteStreamError(inner.Exception);
					return;
				}
				if (this.asyncWrite != null)
				{
					this.asyncWrite.SetCompleted(inner.CompletedSynchronouslyPeek, this.writeStream);
					this.asyncWrite.DoCallback();
					this.asyncWrite = null;
				}
			});
		}

		// Token: 0x06001EED RID: 7917 RVA: 0x00070298 File Offset: 0x0006E498
		[CompilerGenerated]
		private void <SetWriteStream>b__258_1(SimpleAsyncResult inner)
		{
			if (inner.GotException)
			{
				this.SetWriteStreamError(inner.Exception);
				return;
			}
			if (this.asyncWrite != null)
			{
				this.asyncWrite.SetCompleted(inner.CompletedSynchronouslyPeek, this.writeStream);
				this.asyncWrite.DoCallback();
				this.asyncWrite = null;
			}
		}

		// Token: 0x06001EEE RID: 7918 RVA: 0x000702EC File Offset: 0x0006E4EC
		[CompilerGenerated]
		private bool <SetWriteStreamInner>b__259_0(SimpleAsyncResult result)
		{
			if (this.bodyBuffer != null)
			{
				if (this.auth_state.NtlmAuthState != HttpWebRequest.NtlmAuthState.Challenge && this.proxy_auth_state.NtlmAuthState != HttpWebRequest.NtlmAuthState.Challenge)
				{
					this.writeStream.Write(this.bodyBuffer, 0, this.bodyBufferLength);
					this.bodyBuffer = null;
					this.writeStream.Close();
				}
			}
			else if (this.MethodWithBuffer && this.getResponseCalled && !this.writeStream.RequestWritten)
			{
				return this.writeStream.WriteRequestAsync(result);
			}
			return false;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.HttpWebRequest" /> class.</summary>
		// Token: 0x06001EEF RID: 7919 RVA: 0x000092E2 File Offset: 0x000074E2
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("This API supports the .NET Framework infrastructure and is not intended to be used directly from your code.", true)]
		public HttpWebRequest()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001A5D RID: 6749
		private Uri requestUri;

		// Token: 0x04001A5E RID: 6750
		private Uri actualUri;

		// Token: 0x04001A5F RID: 6751
		private bool hostChanged;

		// Token: 0x04001A60 RID: 6752
		private bool allowAutoRedirect;

		// Token: 0x04001A61 RID: 6753
		private bool allowBuffering;

		// Token: 0x04001A62 RID: 6754
		private X509CertificateCollection certificates;

		// Token: 0x04001A63 RID: 6755
		private string connectionGroup;

		// Token: 0x04001A64 RID: 6756
		private bool haveContentLength;

		// Token: 0x04001A65 RID: 6757
		private long contentLength;

		// Token: 0x04001A66 RID: 6758
		private HttpContinueDelegate continueDelegate;

		// Token: 0x04001A67 RID: 6759
		private CookieContainer cookieContainer;

		// Token: 0x04001A68 RID: 6760
		private ICredentials credentials;

		// Token: 0x04001A69 RID: 6761
		private bool haveResponse;

		// Token: 0x04001A6A RID: 6762
		private bool haveRequest;

		// Token: 0x04001A6B RID: 6763
		private bool requestSent;

		// Token: 0x04001A6C RID: 6764
		private WebHeaderCollection webHeaders;

		// Token: 0x04001A6D RID: 6765
		private bool keepAlive;

		// Token: 0x04001A6E RID: 6766
		private int maxAutoRedirect;

		// Token: 0x04001A6F RID: 6767
		private string mediaType;

		// Token: 0x04001A70 RID: 6768
		private string method;

		// Token: 0x04001A71 RID: 6769
		private string initialMethod;

		// Token: 0x04001A72 RID: 6770
		private bool pipelined;

		// Token: 0x04001A73 RID: 6771
		private bool preAuthenticate;

		// Token: 0x04001A74 RID: 6772
		private bool usedPreAuth;

		// Token: 0x04001A75 RID: 6773
		private Version version;

		// Token: 0x04001A76 RID: 6774
		private bool force_version;

		// Token: 0x04001A77 RID: 6775
		private Version actualVersion;

		// Token: 0x04001A78 RID: 6776
		private IWebProxy proxy;

		// Token: 0x04001A79 RID: 6777
		private bool sendChunked;

		// Token: 0x04001A7A RID: 6778
		private ServicePoint servicePoint;

		// Token: 0x04001A7B RID: 6779
		private int timeout;

		// Token: 0x04001A7C RID: 6780
		private WebConnectionStream writeStream;

		// Token: 0x04001A7D RID: 6781
		private HttpWebResponse webResponse;

		// Token: 0x04001A7E RID: 6782
		private WebAsyncResult asyncWrite;

		// Token: 0x04001A7F RID: 6783
		private WebAsyncResult asyncRead;

		// Token: 0x04001A80 RID: 6784
		private EventHandler abortHandler;

		// Token: 0x04001A81 RID: 6785
		private int aborted;

		// Token: 0x04001A82 RID: 6786
		private bool gotRequestStream;

		// Token: 0x04001A83 RID: 6787
		private int redirects;

		// Token: 0x04001A84 RID: 6788
		private bool expectContinue;

		// Token: 0x04001A85 RID: 6789
		private byte[] bodyBuffer;

		// Token: 0x04001A86 RID: 6790
		private int bodyBufferLength;

		// Token: 0x04001A87 RID: 6791
		private bool getResponseCalled;

		// Token: 0x04001A88 RID: 6792
		private Exception saved_exc;

		// Token: 0x04001A89 RID: 6793
		private object locker;

		// Token: 0x04001A8A RID: 6794
		private bool finished_reading;

		// Token: 0x04001A8B RID: 6795
		internal WebConnection WebConnection;

		// Token: 0x04001A8C RID: 6796
		private DecompressionMethods auto_decomp;

		// Token: 0x04001A8D RID: 6797
		private int maxResponseHeadersLength;

		// Token: 0x04001A8E RID: 6798
		private static int defaultMaxResponseHeadersLength = 65536;

		// Token: 0x04001A8F RID: 6799
		private int readWriteTimeout;

		// Token: 0x04001A90 RID: 6800
		private MonoTlsProvider tlsProvider;

		// Token: 0x04001A91 RID: 6801
		private MonoTlsSettings tlsSettings;

		// Token: 0x04001A92 RID: 6802
		private ServerCertValidationCallback certValidationCallback;

		// Token: 0x04001A93 RID: 6803
		private HttpWebRequest.AuthorizationState auth_state;

		// Token: 0x04001A94 RID: 6804
		private HttpWebRequest.AuthorizationState proxy_auth_state;

		// Token: 0x04001A95 RID: 6805
		private string host;

		// Token: 0x04001A96 RID: 6806
		[NonSerialized]
		internal Action<Stream> ResendContentFactory;

		// Token: 0x04001A97 RID: 6807
		[CompilerGenerated]
		private bool <ThrowOnError>k__BackingField;

		// Token: 0x04001A98 RID: 6808
		private bool unsafe_auth_blah;

		// Token: 0x04001A99 RID: 6809
		[CompilerGenerated]
		private bool <ReuseConnection>k__BackingField;

		// Token: 0x04001A9A RID: 6810
		internal WebConnection StoredConnection;

		// Token: 0x020003ED RID: 1005
		private enum NtlmAuthState
		{
			// Token: 0x04001A9C RID: 6812
			None,
			// Token: 0x04001A9D RID: 6813
			Challenge,
			// Token: 0x04001A9E RID: 6814
			Response
		}

		// Token: 0x020003EE RID: 1006
		private struct AuthorizationState
		{
			// Token: 0x17000693 RID: 1683
			// (get) Token: 0x06001EF0 RID: 7920 RVA: 0x00070374 File Offset: 0x0006E574
			public bool IsCompleted
			{
				get
				{
					return this.isCompleted;
				}
			}

			// Token: 0x17000694 RID: 1684
			// (get) Token: 0x06001EF1 RID: 7921 RVA: 0x0007037C File Offset: 0x0006E57C
			public HttpWebRequest.NtlmAuthState NtlmAuthState
			{
				get
				{
					return this.ntlm_auth_state;
				}
			}

			// Token: 0x17000695 RID: 1685
			// (get) Token: 0x06001EF2 RID: 7922 RVA: 0x00070384 File Offset: 0x0006E584
			public bool IsNtlmAuthenticated
			{
				get
				{
					return this.isCompleted && this.ntlm_auth_state > HttpWebRequest.NtlmAuthState.None;
				}
			}

			// Token: 0x06001EF3 RID: 7923 RVA: 0x00070399 File Offset: 0x0006E599
			public AuthorizationState(HttpWebRequest request, bool isProxy)
			{
				this.request = request;
				this.isProxy = isProxy;
				this.isCompleted = false;
				this.ntlm_auth_state = HttpWebRequest.NtlmAuthState.None;
			}

			// Token: 0x06001EF4 RID: 7924 RVA: 0x000703B8 File Offset: 0x0006E5B8
			public bool CheckAuthorization(WebResponse response, HttpStatusCode code)
			{
				this.isCompleted = false;
				if (code == HttpStatusCode.Unauthorized && this.request.credentials == null)
				{
					return false;
				}
				if (this.isProxy != (code == HttpStatusCode.ProxyAuthenticationRequired))
				{
					return false;
				}
				if (this.isProxy && (this.request.proxy == null || this.request.proxy.Credentials == null))
				{
					return false;
				}
				string[] values = response.Headers.GetValues(this.isProxy ? "Proxy-Authenticate" : "WWW-Authenticate");
				if (values == null || values.Length == 0)
				{
					return false;
				}
				ICredentials credentials = (!this.isProxy) ? this.request.credentials : this.request.proxy.Credentials;
				Authorization authorization = null;
				string[] array = values;
				for (int i = 0; i < array.Length; i++)
				{
					authorization = AuthenticationManager.Authenticate(array[i], this.request, credentials);
					if (authorization != null)
					{
						break;
					}
				}
				if (authorization == null)
				{
					return false;
				}
				this.request.webHeaders[this.isProxy ? "Proxy-Authorization" : "Authorization"] = authorization.Message;
				this.isCompleted = authorization.Complete;
				if (authorization.ModuleAuthenticationType == "NTLM")
				{
					this.ntlm_auth_state++;
				}
				return true;
			}

			// Token: 0x06001EF5 RID: 7925 RVA: 0x000704F3 File Offset: 0x0006E6F3
			public void Reset()
			{
				this.isCompleted = false;
				this.ntlm_auth_state = HttpWebRequest.NtlmAuthState.None;
				this.request.webHeaders.RemoveInternal(this.isProxy ? "Proxy-Authorization" : "Authorization");
			}

			// Token: 0x06001EF6 RID: 7926 RVA: 0x00070527 File Offset: 0x0006E727
			public override string ToString()
			{
				return string.Format("{0}AuthState [{1}:{2}]", this.isProxy ? "Proxy" : "", this.isCompleted, this.ntlm_auth_state);
			}

			// Token: 0x04001A9F RID: 6815
			private readonly HttpWebRequest request;

			// Token: 0x04001AA0 RID: 6816
			private readonly bool isProxy;

			// Token: 0x04001AA1 RID: 6817
			private bool isCompleted;

			// Token: 0x04001AA2 RID: 6818
			private HttpWebRequest.NtlmAuthState ntlm_auth_state;
		}

		// Token: 0x020003EF RID: 1007
		[CompilerGenerated]
		private sealed class <>c__DisplayClass238_0
		{
			// Token: 0x06001EF7 RID: 7927 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass238_0()
			{
			}

			// Token: 0x06001EF8 RID: 7928 RVA: 0x00070560 File Offset: 0x0006E760
			internal void <BeginGetResponse>b__0(SimpleAsyncResult inner)
			{
				bool completedSynchronouslyPeek = inner.CompletedSynchronouslyPeek;
				if (inner.GotException)
				{
					this.aread.SetCompleted(completedSynchronouslyPeek, inner.Exception);
					this.aread.DoCallback();
					return;
				}
				if (this.<>4__this.haveResponse)
				{
					Exception saved_exc = this.<>4__this.saved_exc;
					if (this.<>4__this.webResponse != null)
					{
						if (saved_exc == null)
						{
							this.aread.SetCompleted(completedSynchronouslyPeek, this.<>4__this.webResponse);
						}
						else
						{
							this.aread.SetCompleted(completedSynchronouslyPeek, saved_exc);
						}
						this.aread.DoCallback();
						return;
					}
					if (saved_exc != null)
					{
						this.aread.SetCompleted(completedSynchronouslyPeek, saved_exc);
						this.aread.DoCallback();
						return;
					}
				}
				if (this.<>4__this.requestSent)
				{
					return;
				}
				try
				{
					this.<>4__this.requestSent = true;
					this.<>4__this.redirects = 0;
					this.<>4__this.servicePoint = this.<>4__this.GetServicePoint();
					this.<>4__this.abortHandler = this.<>4__this.servicePoint.SendRequest(this.<>4__this, this.<>4__this.connectionGroup);
				}
				catch (Exception e)
				{
					this.aread.SetCompleted(completedSynchronouslyPeek, e);
					this.aread.DoCallback();
				}
			}

			// Token: 0x04001AA3 RID: 6819
			public WebAsyncResult aread;

			// Token: 0x04001AA4 RID: 6820
			public HttpWebRequest <>4__this;
		}
	}
}
