﻿using System;
using System.Collections;
using System.Collections.Specialized;

namespace System.Net
{
	/// <summary>Manages the authentication modules called during the client authentication process.</summary>
	// Token: 0x020003C1 RID: 961
	public class AuthenticationManager
	{
		// Token: 0x06001C28 RID: 7208 RVA: 0x0000232F File Offset: 0x0000052F
		private AuthenticationManager()
		{
		}

		// Token: 0x06001C29 RID: 7209 RVA: 0x00065918 File Offset: 0x00063B18
		private static void EnsureModules()
		{
			object obj = AuthenticationManager.locker;
			lock (obj)
			{
				if (AuthenticationManager.modules == null)
				{
					AuthenticationManager.modules = new ArrayList();
					AuthenticationManager.modules.Add(new NtlmClient());
					AuthenticationManager.modules.Add(new DigestClient());
					AuthenticationManager.modules.Add(new BasicClient());
				}
			}
		}

		/// <summary>Gets or sets the credential policy to be used for resource requests made using the <see cref="T:System.Net.HttpWebRequest" /> class.</summary>
		/// <returns>An object that implements the <see cref="T:System.Net.ICredentialPolicy" /> interface that determines whether credentials are sent with requests. The default value is <see langword="null" />.</returns>
		// Token: 0x170005B9 RID: 1465
		// (get) Token: 0x06001C2A RID: 7210 RVA: 0x00065994 File Offset: 0x00063B94
		// (set) Token: 0x06001C2B RID: 7211 RVA: 0x0006599B File Offset: 0x00063B9B
		public static ICredentialPolicy CredentialPolicy
		{
			get
			{
				return AuthenticationManager.credential_policy;
			}
			set
			{
				AuthenticationManager.credential_policy = value;
			}
		}

		// Token: 0x06001C2C RID: 7212 RVA: 0x000659A3 File Offset: 0x00063BA3
		private static Exception GetMustImplement()
		{
			return new NotImplementedException();
		}

		/// <summary>Gets the dictionary that contains Service Principal Names (SPNs) that are used to identify hosts during Kerberos authentication for requests made using <see cref="T:System.Net.WebRequest" /> and its derived classes.</summary>
		/// <returns>A writable <see cref="T:System.Collections.Specialized.StringDictionary" /> that contains the SPN values for keys composed of host information. </returns>
		// Token: 0x170005BA RID: 1466
		// (get) Token: 0x06001C2D RID: 7213 RVA: 0x000659AA File Offset: 0x00063BAA
		[MonoTODO]
		public static StringDictionary CustomTargetNameDictionary
		{
			get
			{
				throw AuthenticationManager.GetMustImplement();
			}
		}

		/// <summary>Gets a list of authentication modules that are registered with the authentication manager.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that enables the registered authentication modules to be read.</returns>
		// Token: 0x170005BB RID: 1467
		// (get) Token: 0x06001C2E RID: 7214 RVA: 0x000659B1 File Offset: 0x00063BB1
		public static IEnumerator RegisteredModules
		{
			get
			{
				AuthenticationManager.EnsureModules();
				return AuthenticationManager.modules.GetEnumerator();
			}
		}

		// Token: 0x170005BC RID: 1468
		// (get) Token: 0x06001C2F RID: 7215 RVA: 0x00005AFA File Offset: 0x00003CFA
		[MonoTODO]
		internal static bool OSSupportsExtendedProtection
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06001C30 RID: 7216 RVA: 0x000659C4 File Offset: 0x00063BC4
		internal static void Clear()
		{
			AuthenticationManager.EnsureModules();
			ArrayList obj = AuthenticationManager.modules;
			lock (obj)
			{
				AuthenticationManager.modules.Clear();
			}
		}

		/// <summary>Calls each registered authentication module to find the first module that can respond to the authentication request.</summary>
		/// <param name="challenge">The challenge returned by the Internet resource. </param>
		/// <param name="request">The <see cref="T:System.Net.WebRequest" /> that initiated the authentication challenge. </param>
		/// <param name="credentials">The <see cref="T:System.Net.ICredentials" /> associated with this request. </param>
		/// <returns>An instance of the <see cref="T:System.Net.Authorization" /> class containing the result of the authorization attempt. If there is no authentication module to respond to the challenge, this method returns <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="challenge" /> is <see langword="null" />.-or- 
		///         <paramref name="request" /> is <see langword="null" />.-or- 
		///         <paramref name="credentials" /> is <see langword="null" />. </exception>
		// Token: 0x06001C31 RID: 7217 RVA: 0x00065A0C File Offset: 0x00063C0C
		public static Authorization Authenticate(string challenge, WebRequest request, ICredentials credentials)
		{
			if (request == null)
			{
				throw new ArgumentNullException("request");
			}
			if (credentials == null)
			{
				throw new ArgumentNullException("credentials");
			}
			if (challenge == null)
			{
				throw new ArgumentNullException("challenge");
			}
			return AuthenticationManager.DoAuthenticate(challenge, request, credentials);
		}

		// Token: 0x06001C32 RID: 7218 RVA: 0x00065A40 File Offset: 0x00063C40
		private static Authorization DoAuthenticate(string challenge, WebRequest request, ICredentials credentials)
		{
			AuthenticationManager.EnsureModules();
			ArrayList obj = AuthenticationManager.modules;
			lock (obj)
			{
				foreach (object obj2 in AuthenticationManager.modules)
				{
					IAuthenticationModule authenticationModule = (IAuthenticationModule)obj2;
					Authorization authorization = authenticationModule.Authenticate(challenge, request, credentials);
					if (authorization != null)
					{
						authorization.ModuleAuthenticationType = authenticationModule.AuthenticationType;
						return authorization;
					}
				}
			}
			return null;
		}

		/// <summary>Preauthenticates a request.</summary>
		/// <param name="request">A <see cref="T:System.Net.WebRequest" /> to an Internet resource. </param>
		/// <param name="credentials">The <see cref="T:System.Net.ICredentials" /> associated with the request. </param>
		/// <returns>An instance of the <see cref="T:System.Net.Authorization" /> class if the request can be preauthenticated; otherwise, <see langword="null" />. If <paramref name="credentials" /> is <see langword="null" />, this method returns <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="request" /> is <see langword="null" />. </exception>
		// Token: 0x06001C33 RID: 7219 RVA: 0x00065AE8 File Offset: 0x00063CE8
		public static Authorization PreAuthenticate(WebRequest request, ICredentials credentials)
		{
			if (request == null)
			{
				throw new ArgumentNullException("request");
			}
			if (credentials == null)
			{
				return null;
			}
			AuthenticationManager.EnsureModules();
			ArrayList obj = AuthenticationManager.modules;
			lock (obj)
			{
				foreach (object obj2 in AuthenticationManager.modules)
				{
					IAuthenticationModule authenticationModule = (IAuthenticationModule)obj2;
					Authorization authorization = authenticationModule.PreAuthenticate(request, credentials);
					if (authorization != null)
					{
						authorization.ModuleAuthenticationType = authenticationModule.AuthenticationType;
						return authorization;
					}
				}
			}
			return null;
		}

		/// <summary>Registers an authentication module with the authentication manager.</summary>
		/// <param name="authenticationModule">The <see cref="T:System.Net.IAuthenticationModule" /> to register with the authentication manager. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="authenticationModule" /> is <see langword="null" />. </exception>
		// Token: 0x06001C34 RID: 7220 RVA: 0x00065BA0 File Offset: 0x00063DA0
		public static void Register(IAuthenticationModule authenticationModule)
		{
			if (authenticationModule == null)
			{
				throw new ArgumentNullException("authenticationModule");
			}
			AuthenticationManager.DoUnregister(authenticationModule.AuthenticationType, false);
			ArrayList obj = AuthenticationManager.modules;
			lock (obj)
			{
				AuthenticationManager.modules.Add(authenticationModule);
			}
		}

		/// <summary>Removes the specified authentication module from the list of registered modules.</summary>
		/// <param name="authenticationModule">The <see cref="T:System.Net.IAuthenticationModule" /> to remove from the list of registered modules. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="authenticationModule" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The specified <see cref="T:System.Net.IAuthenticationModule" /> is not registered. </exception>
		// Token: 0x06001C35 RID: 7221 RVA: 0x00065C00 File Offset: 0x00063E00
		public static void Unregister(IAuthenticationModule authenticationModule)
		{
			if (authenticationModule == null)
			{
				throw new ArgumentNullException("authenticationModule");
			}
			AuthenticationManager.DoUnregister(authenticationModule.AuthenticationType, true);
		}

		/// <summary>Removes authentication modules with the specified authentication scheme from the list of registered modules.</summary>
		/// <param name="authenticationScheme">The authentication scheme of the module to remove. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="authenticationScheme" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">A module for this authentication scheme is not registered. </exception>
		// Token: 0x06001C36 RID: 7222 RVA: 0x00065C1C File Offset: 0x00063E1C
		public static void Unregister(string authenticationScheme)
		{
			if (authenticationScheme == null)
			{
				throw new ArgumentNullException("authenticationScheme");
			}
			AuthenticationManager.DoUnregister(authenticationScheme, true);
		}

		// Token: 0x06001C37 RID: 7223 RVA: 0x00065C34 File Offset: 0x00063E34
		private static void DoUnregister(string authenticationScheme, bool throwEx)
		{
			AuthenticationManager.EnsureModules();
			ArrayList obj = AuthenticationManager.modules;
			lock (obj)
			{
				IAuthenticationModule authenticationModule = null;
				foreach (object obj2 in AuthenticationManager.modules)
				{
					IAuthenticationModule authenticationModule2 = (IAuthenticationModule)obj2;
					if (string.Compare(authenticationModule2.AuthenticationType, authenticationScheme, true) == 0)
					{
						authenticationModule = authenticationModule2;
						break;
					}
				}
				if (authenticationModule == null)
				{
					if (throwEx)
					{
						throw new InvalidOperationException("Scheme not registered.");
					}
				}
				else
				{
					AuthenticationManager.modules.Remove(authenticationModule);
				}
			}
		}

		// Token: 0x06001C38 RID: 7224 RVA: 0x00065CEC File Offset: 0x00063EEC
		// Note: this type is marked as 'beforefieldinit'.
		static AuthenticationManager()
		{
		}

		// Token: 0x0400196D RID: 6509
		private static ArrayList modules;

		// Token: 0x0400196E RID: 6510
		private static object locker = new object();

		// Token: 0x0400196F RID: 6511
		private static ICredentialPolicy credential_policy = null;
	}
}
