﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Security;
using System.Security.Authentication.ExtendedProtection;

namespace System.Net
{
	// Token: 0x0200035F RID: 863
	internal class ServiceNameStore
	{
		// Token: 0x17000520 RID: 1312
		// (get) Token: 0x060018D1 RID: 6353 RVA: 0x0005919F File Offset: 0x0005739F
		public ServiceNameCollection ServiceNames
		{
			get
			{
				if (this.serviceNameCollection == null)
				{
					this.serviceNameCollection = new ServiceNameCollection(this.serviceNames);
				}
				return this.serviceNameCollection;
			}
		}

		// Token: 0x060018D2 RID: 6354 RVA: 0x000591C0 File Offset: 0x000573C0
		public ServiceNameStore()
		{
			this.serviceNames = new List<string>();
			this.serviceNameCollection = null;
		}

		// Token: 0x060018D3 RID: 6355 RVA: 0x000591DA File Offset: 0x000573DA
		private bool AddSingleServiceName(string spn)
		{
			spn = ServiceNameCollection.NormalizeServiceName(spn);
			if (this.Contains(spn))
			{
				return false;
			}
			this.serviceNames.Add(spn);
			return true;
		}

		// Token: 0x060018D4 RID: 6356 RVA: 0x000591FC File Offset: 0x000573FC
		public bool Add(string uriPrefix)
		{
			string[] array = this.BuildServiceNames(uriPrefix);
			bool flag = false;
			foreach (string spn in array)
			{
				if (this.AddSingleServiceName(spn))
				{
					flag = true;
					bool on = Logging.On;
				}
			}
			if (flag)
			{
				this.serviceNameCollection = null;
			}
			else
			{
				bool on2 = Logging.On;
			}
			return flag;
		}

		// Token: 0x060018D5 RID: 6357 RVA: 0x0005924C File Offset: 0x0005744C
		public bool Remove(string uriPrefix)
		{
			string text = this.BuildSimpleServiceName(uriPrefix);
			text = ServiceNameCollection.NormalizeServiceName(text);
			bool flag = this.Contains(text);
			if (flag)
			{
				this.serviceNames.Remove(text);
				this.serviceNameCollection = null;
			}
			if (Logging.On)
			{
			}
			return flag;
		}

		// Token: 0x060018D6 RID: 6358 RVA: 0x00059291 File Offset: 0x00057491
		private bool Contains(string newServiceName)
		{
			return newServiceName != null && ServiceNameCollection.Contains(newServiceName, this.serviceNames);
		}

		// Token: 0x060018D7 RID: 6359 RVA: 0x000592A4 File Offset: 0x000574A4
		public void Clear()
		{
			this.serviceNames.Clear();
			this.serviceNameCollection = null;
		}

		// Token: 0x060018D8 RID: 6360 RVA: 0x000592B8 File Offset: 0x000574B8
		private string ExtractHostname(string uriPrefix, bool allowInvalidUriStrings)
		{
			if (Uri.IsWellFormedUriString(uriPrefix, UriKind.Absolute))
			{
				return new Uri(uriPrefix).Host;
			}
			if (allowInvalidUriStrings)
			{
				int num = uriPrefix.IndexOf("://") + 3;
				int num2 = num;
				bool flag = false;
				while (num2 < uriPrefix.Length && uriPrefix[num2] != '/' && (uriPrefix[num2] != ':' || flag))
				{
					if (uriPrefix[num2] == '[')
					{
						if (flag)
						{
							num2 = num;
							break;
						}
						flag = true;
					}
					if (flag && uriPrefix[num2] == ']')
					{
						flag = false;
					}
					num2++;
				}
				return uriPrefix.Substring(num, num2 - num);
			}
			return null;
		}

		// Token: 0x060018D9 RID: 6361 RVA: 0x0005934C File Offset: 0x0005754C
		public string BuildSimpleServiceName(string uriPrefix)
		{
			string text = this.ExtractHostname(uriPrefix, false);
			if (text != null)
			{
				return "HTTP/" + text;
			}
			return null;
		}

		// Token: 0x060018DA RID: 6362 RVA: 0x00059374 File Offset: 0x00057574
		public string[] BuildServiceNames(string uriPrefix)
		{
			string text = this.ExtractHostname(uriPrefix, true);
			IPAddress ipaddress = null;
			if (string.Compare(text, "*", StringComparison.InvariantCultureIgnoreCase) == 0 || string.Compare(text, "+", StringComparison.InvariantCultureIgnoreCase) == 0 || IPAddress.TryParse(text, out ipaddress))
			{
				try
				{
					string hostName = Dns.GetHostEntry(string.Empty).HostName;
					return new string[]
					{
						"HTTP/" + hostName
					};
				}
				catch (SocketException)
				{
					return new string[0];
				}
				catch (SecurityException)
				{
					return new string[0];
				}
			}
			if (!text.Contains("."))
			{
				try
				{
					string hostName2 = Dns.GetHostEntry(text).HostName;
					return new string[]
					{
						"HTTP/" + text,
						"HTTP/" + hostName2
					};
				}
				catch (SocketException)
				{
					return new string[]
					{
						"HTTP/" + text
					};
				}
				catch (SecurityException)
				{
					return new string[]
					{
						"HTTP/" + text
					};
				}
			}
			return new string[]
			{
				"HTTP/" + text
			};
		}

		// Token: 0x040017A8 RID: 6056
		private List<string> serviceNames;

		// Token: 0x040017A9 RID: 6057
		private ServiceNameCollection serviceNameCollection;
	}
}
