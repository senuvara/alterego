﻿using System;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using Mono.Net.Security;
using Mono.Security.Interface;
using Unity;

namespace System.Net
{
	/// <summary>Implements a File Transfer Protocol (FTP) client.</summary>
	// Token: 0x020003DA RID: 986
	public sealed class FtpWebRequest : WebRequest
	{
		// Token: 0x06001D02 RID: 7426 RVA: 0x00068B24 File Offset: 0x00066D24
		internal FtpWebRequest(Uri uri)
		{
			this.timeout = 100000;
			this.rwTimeout = 300000;
			this.binary = true;
			this.usePassive = true;
			this.method = "RETR";
			this.locker = new object();
			this.dataEncoding = Encoding.UTF8;
			base..ctor();
			this.requestUri = uri;
			this.proxy = GlobalProxySelection.Select;
		}

		// Token: 0x06001D03 RID: 7427 RVA: 0x000659A3 File Offset: 0x00063BA3
		private static Exception GetMustImplement()
		{
			return new NotImplementedException();
		}

		/// <summary>Gets or sets the certificates used for establishing an encrypted connection to the FTP server.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.X509Certificates.X509CertificateCollection" /> object that contains the client certificates.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value specified for a set operation is <see langword="null" />.</exception>
		// Token: 0x170005E3 RID: 1507
		// (get) Token: 0x06001D04 RID: 7428 RVA: 0x00068B8E File Offset: 0x00066D8E
		// (set) Token: 0x06001D05 RID: 7429 RVA: 0x00068B8E File Offset: 0x00066D8E
		[MonoTODO]
		public X509CertificateCollection ClientCertificates
		{
			get
			{
				throw FtpWebRequest.GetMustImplement();
			}
			set
			{
				throw FtpWebRequest.GetMustImplement();
			}
		}

		/// <summary>Gets or sets the name of the connection group that contains the service point used to send the current request.</summary>
		/// <returns>A <see cref="T:System.String" /> value that contains a connection group name.</returns>
		/// <exception cref="T:System.InvalidOperationException">A new value was specified for this property for a request that is already in progress. </exception>
		// Token: 0x170005E4 RID: 1508
		// (get) Token: 0x06001D06 RID: 7430 RVA: 0x00068B8E File Offset: 0x00066D8E
		// (set) Token: 0x06001D07 RID: 7431 RVA: 0x00068B8E File Offset: 0x00066D8E
		[MonoTODO]
		public override string ConnectionGroupName
		{
			get
			{
				throw FtpWebRequest.GetMustImplement();
			}
			set
			{
				throw FtpWebRequest.GetMustImplement();
			}
		}

		/// <summary>Always throws a <see cref="T:System.NotSupportedException" />.</summary>
		/// <returns>Always throws a <see cref="T:System.NotSupportedException" />.</returns>
		/// <exception cref="T:System.NotSupportedException">Content type information is not supported for FTP.</exception>
		// Token: 0x170005E5 RID: 1509
		// (get) Token: 0x06001D08 RID: 7432 RVA: 0x00006740 File Offset: 0x00004940
		// (set) Token: 0x06001D09 RID: 7433 RVA: 0x00006740 File Offset: 0x00004940
		public override string ContentType
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that is ignored by the <see cref="T:System.Net.FtpWebRequest" /> class.</summary>
		/// <returns>An <see cref="T:System.Int64" /> value that should be ignored.</returns>
		// Token: 0x170005E6 RID: 1510
		// (get) Token: 0x06001D0A RID: 7434 RVA: 0x0002873C File Offset: 0x0002693C
		// (set) Token: 0x06001D0B RID: 7435 RVA: 0x0000232D File Offset: 0x0000052D
		public override long ContentLength
		{
			get
			{
				return 0L;
			}
			set
			{
			}
		}

		/// <summary>Gets or sets a byte offset into the file being downloaded by this request.</summary>
		/// <returns>An <see cref="T:System.Int64" /> instance that specifies the file offset, in bytes. The default value is zero.</returns>
		/// <exception cref="T:System.InvalidOperationException">A new value was specified for this property for a request that is already in progress. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified for this property is less than zero. </exception>
		// Token: 0x170005E7 RID: 1511
		// (get) Token: 0x06001D0C RID: 7436 RVA: 0x00068B95 File Offset: 0x00066D95
		// (set) Token: 0x06001D0D RID: 7437 RVA: 0x00068B9D File Offset: 0x00066D9D
		public long ContentOffset
		{
			get
			{
				return this.offset;
			}
			set
			{
				this.CheckRequestStarted();
				if (value < 0L)
				{
					throw new ArgumentOutOfRangeException();
				}
				this.offset = value;
			}
		}

		/// <summary>Gets or sets the credentials used to communicate with the FTP server.</summary>
		/// <returns>An <see cref="T:System.Net.ICredentials" /> instance; otherwise, <see langword="null" /> if the property has not been set.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value specified for a set operation is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">An <see cref="T:System.Net.ICredentials" /> of a type other than <see cref="T:System.Net.NetworkCredential" /> was specified for a set operation.</exception>
		/// <exception cref="T:System.InvalidOperationException">A new value was specified for this property for a request that is already in progress. </exception>
		// Token: 0x170005E8 RID: 1512
		// (get) Token: 0x06001D0E RID: 7438 RVA: 0x00068BB7 File Offset: 0x00066DB7
		// (set) Token: 0x06001D0F RID: 7439 RVA: 0x00068BBF File Offset: 0x00066DBF
		public override ICredentials Credentials
		{
			get
			{
				return this.credentials;
			}
			set
			{
				this.CheckRequestStarted();
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				if (!(value is NetworkCredential))
				{
					throw new ArgumentException();
				}
				this.credentials = (value as NetworkCredential);
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> that specifies that an SSL connection should be used.</summary>
		/// <returns>
		///     <see langword="true" /> if control and data transmissions are encrypted; otherwise, <see langword="false" />. The default value is <see langword="false" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The connection to the FTP server has already been established.</exception>
		// Token: 0x170005E9 RID: 1513
		// (get) Token: 0x06001D10 RID: 7440 RVA: 0x00068BEA File Offset: 0x00066DEA
		// (set) Token: 0x06001D11 RID: 7441 RVA: 0x00068BF2 File Offset: 0x00066DF2
		public bool EnableSsl
		{
			get
			{
				return this.enableSsl;
			}
			set
			{
				this.CheckRequestStarted();
				this.enableSsl = value;
			}
		}

		/// <summary>Gets an empty <see cref="T:System.Net.WebHeaderCollection" /> object.</summary>
		/// <returns>An empty <see cref="T:System.Net.WebHeaderCollection" /> object.</returns>
		// Token: 0x170005EA RID: 1514
		// (get) Token: 0x06001D12 RID: 7442 RVA: 0x00068B8E File Offset: 0x00066D8E
		// (set) Token: 0x06001D13 RID: 7443 RVA: 0x00068B8E File Offset: 0x00066D8E
		[MonoTODO]
		public override WebHeaderCollection Headers
		{
			get
			{
				throw FtpWebRequest.GetMustImplement();
			}
			set
			{
				throw FtpWebRequest.GetMustImplement();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that specifies whether the control connection to the FTP server is closed after the request completes.</summary>
		/// <returns>
		///     <see langword="true" /> if the connection to the server should not be destroyed; otherwise, <see langword="false" />. The default value is <see langword="true" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">A new value was specified for this property for a request that is already in progress. </exception>
		// Token: 0x170005EB RID: 1515
		// (get) Token: 0x06001D14 RID: 7444 RVA: 0x00068C01 File Offset: 0x00066E01
		// (set) Token: 0x06001D15 RID: 7445 RVA: 0x00068C09 File Offset: 0x00066E09
		[MonoTODO("We don't support KeepAlive = true")]
		public bool KeepAlive
		{
			get
			{
				return this.keepAlive;
			}
			set
			{
				this.CheckRequestStarted();
			}
		}

		/// <summary>Gets or sets the command to send to the FTP server.</summary>
		/// <returns>A <see cref="T:System.String" /> value that contains the FTP command to send to the server. The default value is <see cref="F:System.Net.WebRequestMethods.Ftp.DownloadFile" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">A new value was specified for this property for a request that is already in progress. </exception>
		/// <exception cref="T:System.ArgumentException">The method is invalid.- or -The method is not supported.- or -Multiple methods were specified.</exception>
		// Token: 0x170005EC RID: 1516
		// (get) Token: 0x06001D16 RID: 7446 RVA: 0x00068C11 File Offset: 0x00066E11
		// (set) Token: 0x06001D17 RID: 7447 RVA: 0x00068C1C File Offset: 0x00066E1C
		public override string Method
		{
			get
			{
				return this.method;
			}
			set
			{
				this.CheckRequestStarted();
				if (value == null)
				{
					throw new ArgumentNullException("Method string cannot be null");
				}
				if (value.Length == 0 || Array.BinarySearch<string>(FtpWebRequest.supportedCommands, value) < 0)
				{
					throw new ArgumentException("Method not supported", "value");
				}
				this.method = value;
			}
		}

		/// <summary>Always throws a <see cref="T:System.NotSupportedException" />.</summary>
		/// <returns>Always throws a <see cref="T:System.NotSupportedException" />.</returns>
		/// <exception cref="T:System.NotSupportedException">Preauthentication is not supported for FTP.</exception>
		// Token: 0x170005ED RID: 1517
		// (get) Token: 0x06001D18 RID: 7448 RVA: 0x00006740 File Offset: 0x00004940
		// (set) Token: 0x06001D19 RID: 7449 RVA: 0x00006740 File Offset: 0x00004940
		public override bool PreAuthenticate
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Gets or sets the proxy used to communicate with the FTP server.</summary>
		/// <returns>An <see cref="T:System.Net.IWebProxy" /> instance responsible for communicating with the FTP server.</returns>
		/// <exception cref="T:System.ArgumentNullException">This property cannot be set to <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">A new value was specified for this property for a request that is already in progress. </exception>
		// Token: 0x170005EE RID: 1518
		// (get) Token: 0x06001D1A RID: 7450 RVA: 0x00068C6A File Offset: 0x00066E6A
		// (set) Token: 0x06001D1B RID: 7451 RVA: 0x00068C72 File Offset: 0x00066E72
		public override IWebProxy Proxy
		{
			get
			{
				return this.proxy;
			}
			set
			{
				this.CheckRequestStarted();
				this.proxy = value;
			}
		}

		/// <summary>Gets or sets a time-out when reading from or writing to a stream.</summary>
		/// <returns>The number of milliseconds before the reading or writing times out. The default value is 300,000 milliseconds (5 minutes).</returns>
		/// <exception cref="T:System.InvalidOperationException">The request has already been sent. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified for a set operation is less than or equal to zero and is not equal to <see cref="F:System.Threading.Timeout.Infinite" />. </exception>
		// Token: 0x170005EF RID: 1519
		// (get) Token: 0x06001D1C RID: 7452 RVA: 0x00068C81 File Offset: 0x00066E81
		// (set) Token: 0x06001D1D RID: 7453 RVA: 0x00068C89 File Offset: 0x00066E89
		public int ReadWriteTimeout
		{
			get
			{
				return this.rwTimeout;
			}
			set
			{
				this.CheckRequestStarted();
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException();
				}
				this.rwTimeout = value;
			}
		}

		/// <summary>Gets or sets the new name of a file being renamed.</summary>
		/// <returns>The new name of the file being renamed.</returns>
		/// <exception cref="T:System.ArgumentException">The value specified for a set operation is <see langword="null" /> or an empty string.</exception>
		/// <exception cref="T:System.InvalidOperationException">A new value was specified for this property for a request that is already in progress. </exception>
		// Token: 0x170005F0 RID: 1520
		// (get) Token: 0x06001D1E RID: 7454 RVA: 0x00068CA2 File Offset: 0x00066EA2
		// (set) Token: 0x06001D1F RID: 7455 RVA: 0x00068CAA File Offset: 0x00066EAA
		public string RenameTo
		{
			get
			{
				return this.renameTo;
			}
			set
			{
				this.CheckRequestStarted();
				if (value == null || value.Length == 0)
				{
					throw new ArgumentException("RenameTo value can't be null or empty", "RenameTo");
				}
				this.renameTo = value;
			}
		}

		/// <summary>Gets the URI requested by this instance.</summary>
		/// <returns>A <see cref="T:System.Uri" /> instance that identifies a resource that is accessed using the File Transfer Protocol.</returns>
		// Token: 0x170005F1 RID: 1521
		// (get) Token: 0x06001D20 RID: 7456 RVA: 0x00068CD4 File Offset: 0x00066ED4
		public override Uri RequestUri
		{
			get
			{
				return this.requestUri;
			}
		}

		/// <summary>Gets the <see cref="T:System.Net.ServicePoint" /> object used to connect to the FTP server.</summary>
		/// <returns>A <see cref="T:System.Net.ServicePoint" /> object that can be used to customize connection behavior.</returns>
		// Token: 0x170005F2 RID: 1522
		// (get) Token: 0x06001D21 RID: 7457 RVA: 0x00068CDC File Offset: 0x00066EDC
		public ServicePoint ServicePoint
		{
			get
			{
				return this.GetServicePoint();
			}
		}

		/// <summary>Gets or sets the behavior of a client application's data transfer process.</summary>
		/// <returns>
		///     <see langword="false" /> if the client application's data transfer process listens for a connection on the data port; otherwise, <see langword="true" /> if the client should initiate a connection on the data port. The default value is <see langword="true" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">A new value was specified for this property for a request that is already in progress. </exception>
		// Token: 0x170005F3 RID: 1523
		// (get) Token: 0x06001D22 RID: 7458 RVA: 0x00068CE4 File Offset: 0x00066EE4
		// (set) Token: 0x06001D23 RID: 7459 RVA: 0x00068CEC File Offset: 0x00066EEC
		public bool UsePassive
		{
			get
			{
				return this.usePassive;
			}
			set
			{
				this.CheckRequestStarted();
				this.usePassive = value;
			}
		}

		/// <summary>Always throws a <see cref="T:System.NotSupportedException" />.</summary>
		/// <returns>Always throws a <see cref="T:System.NotSupportedException" />.</returns>
		/// <exception cref="T:System.NotSupportedException">Default credentials are not supported for FTP.</exception>
		// Token: 0x170005F4 RID: 1524
		// (get) Token: 0x06001D24 RID: 7460 RVA: 0x00068B8E File Offset: 0x00066D8E
		// (set) Token: 0x06001D25 RID: 7461 RVA: 0x00068B8E File Offset: 0x00066D8E
		[MonoTODO]
		public override bool UseDefaultCredentials
		{
			get
			{
				throw FtpWebRequest.GetMustImplement();
			}
			set
			{
				throw FtpWebRequest.GetMustImplement();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that specifies the data type for file transfers.</summary>
		/// <returns>
		///     <see langword="true" /> to indicate to the server that the data to be transferred is binary; <see langword="false" /> to indicate that the data is text. The default value is <see langword="true" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">A new value was specified for this property for a request that is already in progress.</exception>
		// Token: 0x170005F5 RID: 1525
		// (get) Token: 0x06001D26 RID: 7462 RVA: 0x00068CFB File Offset: 0x00066EFB
		// (set) Token: 0x06001D27 RID: 7463 RVA: 0x00068D03 File Offset: 0x00066F03
		public bool UseBinary
		{
			get
			{
				return this.binary;
			}
			set
			{
				this.CheckRequestStarted();
				this.binary = value;
			}
		}

		/// <summary>Gets or sets the number of milliseconds to wait for a request.</summary>
		/// <returns>An <see cref="T:System.Int32" /> value that contains the number of milliseconds to wait before a request times out. The default value is <see cref="F:System.Threading.Timeout.Infinite" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified is less than zero and is not <see cref="F:System.Threading.Timeout.Infinite" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">A new value was specified for this property for a request that is already in progress. </exception>
		// Token: 0x170005F6 RID: 1526
		// (get) Token: 0x06001D28 RID: 7464 RVA: 0x00068D12 File Offset: 0x00066F12
		// (set) Token: 0x06001D29 RID: 7465 RVA: 0x00068D1A File Offset: 0x00066F1A
		public override int Timeout
		{
			get
			{
				return this.timeout;
			}
			set
			{
				this.CheckRequestStarted();
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException();
				}
				this.timeout = value;
			}
		}

		// Token: 0x170005F7 RID: 1527
		// (get) Token: 0x06001D2A RID: 7466 RVA: 0x00068D33 File Offset: 0x00066F33
		private string DataType
		{
			get
			{
				if (!this.binary)
				{
					return "A";
				}
				return "I";
			}
		}

		// Token: 0x170005F8 RID: 1528
		// (get) Token: 0x06001D2B RID: 7467 RVA: 0x00068D48 File Offset: 0x00066F48
		// (set) Token: 0x06001D2C RID: 7468 RVA: 0x00068D8C File Offset: 0x00066F8C
		private FtpWebRequest.RequestState State
		{
			get
			{
				object obj = this.locker;
				FtpWebRequest.RequestState result;
				lock (obj)
				{
					result = this.requestState;
				}
				return result;
			}
			set
			{
				object obj = this.locker;
				lock (obj)
				{
					this.CheckIfAborted();
					this.CheckFinalState();
					this.requestState = value;
				}
			}
		}

		/// <summary>Terminates an asynchronous FTP operation.</summary>
		// Token: 0x06001D2D RID: 7469 RVA: 0x00068DDC File Offset: 0x00066FDC
		public override void Abort()
		{
			object obj = this.locker;
			lock (obj)
			{
				if (this.State == FtpWebRequest.RequestState.TransferInProgress)
				{
					this.SendCommand(false, "ABOR", Array.Empty<string>());
				}
				if (!this.InFinalState())
				{
					this.State = FtpWebRequest.RequestState.Aborted;
					this.ftpResponse = new FtpWebResponse(this, this.requestUri, this.method, FtpStatusCode.FileActionAborted, "Aborted by request");
				}
			}
		}

		/// <summary>Begins sending a request and receiving a response from an FTP server asynchronously.</summary>
		/// <param name="callback">An <see cref="T:System.AsyncCallback" /> delegate that references the method to invoke when the operation is complete. </param>
		/// <param name="state">A user-defined object that contains information about the operation. This object is passed to the <paramref name="callback" /> delegate when the operation completes. </param>
		/// <returns>An <see cref="T:System.IAsyncResult" /> instance that indicates the status of the operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Net.FtpWebRequest.GetResponse" /> or <see cref="M:System.Net.FtpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" /> has already been called for this instance. </exception>
		// Token: 0x06001D2E RID: 7470 RVA: 0x00068E64 File Offset: 0x00067064
		public override IAsyncResult BeginGetResponse(AsyncCallback callback, object state)
		{
			if (this.asyncResult != null && !this.asyncResult.IsCompleted)
			{
				throw new InvalidOperationException("Cannot re-call BeginGetRequestStream/BeginGetResponse while a previous call is still in progress");
			}
			this.CheckIfAborted();
			this.asyncResult = new FtpAsyncResult(callback, state);
			object obj = this.locker;
			lock (obj)
			{
				if (this.InFinalState())
				{
					this.asyncResult.SetCompleted(true, this.ftpResponse);
				}
				else
				{
					if (this.State == FtpWebRequest.RequestState.Before)
					{
						this.State = FtpWebRequest.RequestState.Scheduled;
					}
					new Thread(new ThreadStart(this.ProcessRequest))
					{
						IsBackground = true
					}.Start();
				}
			}
			return this.asyncResult;
		}

		/// <summary>Ends a pending asynchronous operation started with <see cref="M:System.Net.FtpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" />.</summary>
		/// <param name="asyncResult">The <see cref="T:System.IAsyncResult" /> that was returned when the operation started. </param>
		/// <returns>A <see cref="T:System.Net.WebResponse" /> reference that contains an <see cref="T:System.Net.FtpWebResponse" /> instance. This object contains the FTP server's response to the request.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="asyncResult" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="asyncResult" /> was not obtained by calling <see cref="M:System.Net.FtpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">This method was already called for the operation identified by <paramref name="asyncResult" />. </exception>
		/// <exception cref="T:System.Net.WebException">An error occurred using an HTTP proxy. </exception>
		// Token: 0x06001D2F RID: 7471 RVA: 0x00068F20 File Offset: 0x00067120
		public override WebResponse EndGetResponse(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("AsyncResult cannot be null!");
			}
			if (!(asyncResult is FtpAsyncResult) || asyncResult != this.asyncResult)
			{
				throw new ArgumentException("AsyncResult is from another request!");
			}
			FtpAsyncResult ftpAsyncResult = (FtpAsyncResult)asyncResult;
			if (!ftpAsyncResult.WaitUntilComplete(this.timeout, false))
			{
				this.Abort();
				throw new WebException("Transfer timed out.", WebExceptionStatus.Timeout);
			}
			this.CheckIfAborted();
			asyncResult = null;
			if (ftpAsyncResult.GotException)
			{
				throw ftpAsyncResult.Exception;
			}
			return ftpAsyncResult.Response;
		}

		/// <summary>Returns the FTP server response.</summary>
		/// <returns>A <see cref="T:System.Net.WebResponse" /> reference that contains an <see cref="T:System.Net.FtpWebResponse" /> instance. This object contains the FTP server's response to the request.</returns>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Net.FtpWebRequest.GetResponse" /> or <see cref="M:System.Net.FtpWebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" /> has already been called for this instance.- or -An HTTP proxy is enabled, and you attempted to use an FTP command other than <see cref="F:System.Net.WebRequestMethods.Ftp.DownloadFile" />, <see cref="F:System.Net.WebRequestMethods.Ftp.ListDirectory" />, or <see cref="F:System.Net.WebRequestMethods.Ftp.ListDirectoryDetails" />.</exception>
		/// <exception cref="T:System.Net.WebException">
		///         <see cref="P:System.Net.FtpWebRequest.EnableSsl" /> is set to <see langword="true" />, but the server does not support this feature.- or -A <see cref="P:System.Net.FtpWebRequest.Timeout" /> was specified and the timeout has expired.</exception>
		// Token: 0x06001D30 RID: 7472 RVA: 0x00068FA0 File Offset: 0x000671A0
		public override WebResponse GetResponse()
		{
			IAsyncResult asyncResult = this.BeginGetResponse(null, null);
			return this.EndGetResponse(asyncResult);
		}

		/// <summary>Begins asynchronously opening a request's content stream for writing.</summary>
		/// <param name="callback">An <see cref="T:System.AsyncCallback" /> delegate that references the method to invoke when the operation is complete. </param>
		/// <param name="state">A user-defined object that contains information about the operation. This object is passed to the <paramref name="callback" /> delegate when the operation completes. </param>
		/// <returns>An <see cref="T:System.IAsyncResult" /> instance that indicates the status of the operation.</returns>
		/// <exception cref="T:System.InvalidOperationException">A previous call to this method or <see cref="M:System.Net.FtpWebRequest.GetRequestStream" /> has not yet completed. </exception>
		/// <exception cref="T:System.Net.WebException">A connection to the FTP server could not be established. </exception>
		/// <exception cref="T:System.Net.ProtocolViolationException">The <see cref="P:System.Net.FtpWebRequest.Method" /> property is not set to <see cref="F:System.Net.WebRequestMethods.Ftp.UploadFile" />. </exception>
		// Token: 0x06001D31 RID: 7473 RVA: 0x00068FC0 File Offset: 0x000671C0
		public override IAsyncResult BeginGetRequestStream(AsyncCallback callback, object state)
		{
			if (this.method != "STOR" && this.method != "STOU" && this.method != "APPE")
			{
				throw new ProtocolViolationException();
			}
			object obj = this.locker;
			lock (obj)
			{
				this.CheckIfAborted();
				if (this.State != FtpWebRequest.RequestState.Before)
				{
					throw new InvalidOperationException("Cannot re-call BeginGetRequestStream/BeginGetResponse while a previous call is still in progress");
				}
				this.State = FtpWebRequest.RequestState.Scheduled;
			}
			this.asyncResult = new FtpAsyncResult(callback, state);
			new Thread(new ThreadStart(this.ProcessRequest))
			{
				IsBackground = true
			}.Start();
			return this.asyncResult;
		}

		/// <summary>Ends a pending asynchronous operation started with <see cref="M:System.Net.FtpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" />.</summary>
		/// <param name="asyncResult">The <see cref="T:System.IAsyncResult" /> object that was returned when the operation started. </param>
		/// <returns>A writable <see cref="T:System.IO.Stream" /> instance associated with this instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="asyncResult" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="asyncResult" /> was not obtained by calling <see cref="M:System.Net.FtpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">This method was already called for the operation identified by <paramref name="asyncResult" />. </exception>
		// Token: 0x06001D32 RID: 7474 RVA: 0x00069088 File Offset: 0x00067288
		public override Stream EndGetRequestStream(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			if (!(asyncResult is FtpAsyncResult))
			{
				throw new ArgumentException("asyncResult");
			}
			if (this.State == FtpWebRequest.RequestState.Aborted)
			{
				throw new WebException("Request aborted", WebExceptionStatus.RequestCanceled);
			}
			if (asyncResult != this.asyncResult)
			{
				throw new ArgumentException("AsyncResult is from another request!");
			}
			FtpAsyncResult ftpAsyncResult = (FtpAsyncResult)asyncResult;
			if (!ftpAsyncResult.WaitUntilComplete(this.timeout, false))
			{
				this.Abort();
				throw new WebException("Request timed out");
			}
			if (ftpAsyncResult.GotException)
			{
				throw ftpAsyncResult.Exception;
			}
			return ftpAsyncResult.Stream;
		}

		/// <summary>Retrieves the stream used to upload data to an FTP server.</summary>
		/// <returns>A writable <see cref="T:System.IO.Stream" /> instance used to store data to be sent to the server by the current request.</returns>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Net.FtpWebRequest.BeginGetRequestStream(System.AsyncCallback,System.Object)" /> has been called and has not completed. - or -An HTTP proxy is enabled, and you attempted to use an FTP command other than <see cref="F:System.Net.WebRequestMethods.Ftp.DownloadFile" />, <see cref="F:System.Net.WebRequestMethods.Ftp.ListDirectory" />, or <see cref="F:System.Net.WebRequestMethods.Ftp.ListDirectoryDetails" />.</exception>
		/// <exception cref="T:System.Net.WebException">A connection to the FTP server could not be established. </exception>
		/// <exception cref="T:System.Net.ProtocolViolationException">The <see cref="P:System.Net.FtpWebRequest.Method" /> property is not set to <see cref="F:System.Net.WebRequestMethods.Ftp.UploadFile" /> or <see cref="F:System.Net.WebRequestMethods.Ftp.AppendFile" />. </exception>
		// Token: 0x06001D33 RID: 7475 RVA: 0x0006911C File Offset: 0x0006731C
		public override Stream GetRequestStream()
		{
			IAsyncResult asyncResult = this.BeginGetRequestStream(null, null);
			return this.EndGetRequestStream(asyncResult);
		}

		// Token: 0x06001D34 RID: 7476 RVA: 0x00069139 File Offset: 0x00067339
		private ServicePoint GetServicePoint()
		{
			if (this.servicePoint == null)
			{
				this.servicePoint = ServicePointManager.FindServicePoint(this.requestUri, this.proxy);
			}
			return this.servicePoint;
		}

		// Token: 0x06001D35 RID: 7477 RVA: 0x00069160 File Offset: 0x00067360
		private void ResolveHost()
		{
			this.CheckIfAborted();
			this.hostEntry = this.GetServicePoint().HostEntry;
			if (this.hostEntry == null)
			{
				this.ftpResponse.UpdateStatus(new FtpStatus(FtpStatusCode.ActionAbortedLocalProcessingError, "Cannot resolve server name"));
				throw new WebException("The remote server name could not be resolved: " + this.requestUri, null, WebExceptionStatus.NameResolutionFailure, this.ftpResponse);
			}
		}

		// Token: 0x06001D36 RID: 7478 RVA: 0x000691C4 File Offset: 0x000673C4
		private void ProcessRequest()
		{
			if (this.State == FtpWebRequest.RequestState.Scheduled)
			{
				this.ftpResponse = new FtpWebResponse(this, this.requestUri, this.method, this.keepAlive);
				try
				{
					this.ProcessMethod();
					this.asyncResult.SetCompleted(false, this.ftpResponse);
					return;
				}
				catch (Exception completeWithError)
				{
					if (!this.GetServicePoint().UsesProxy)
					{
						this.State = FtpWebRequest.RequestState.Error;
					}
					this.SetCompleteWithError(completeWithError);
					return;
				}
			}
			if (this.InProgress())
			{
				FtpStatus responseStatus = this.GetResponseStatus();
				this.ftpResponse.UpdateStatus(responseStatus);
				if (this.ftpResponse.IsFinal())
				{
					this.State = FtpWebRequest.RequestState.Finished;
				}
			}
			this.asyncResult.SetCompleted(false, this.ftpResponse);
		}

		// Token: 0x06001D37 RID: 7479 RVA: 0x00069284 File Offset: 0x00067484
		private void SetType()
		{
			if (this.binary)
			{
				FtpStatus ftpStatus = this.SendCommand("TYPE", new string[]
				{
					this.DataType
				});
				if (ftpStatus.StatusCode < FtpStatusCode.CommandOK || ftpStatus.StatusCode >= (FtpStatusCode)300)
				{
					throw this.CreateExceptionFromResponse(ftpStatus);
				}
			}
		}

		// Token: 0x06001D38 RID: 7480 RVA: 0x000692D8 File Offset: 0x000674D8
		private string GetRemoteFolderPath(Uri uri)
		{
			string text = Uri.UnescapeDataString(uri.LocalPath);
			string text2;
			if (this.initial_path == null || this.initial_path == "/")
			{
				text2 = text;
			}
			else
			{
				if (text[0] == '/')
				{
					text = text.Substring(1);
				}
				text2 = new Uri(new UriBuilder
				{
					Scheme = "ftp",
					Host = "dummy-host",
					Path = this.initial_path
				}.Uri, text).LocalPath;
			}
			int num = text2.LastIndexOf('/');
			if (num == -1)
			{
				return null;
			}
			return text2.Substring(0, num + 1);
		}

		// Token: 0x06001D39 RID: 7481 RVA: 0x00069374 File Offset: 0x00067574
		private void CWDAndSetFileName(Uri uri)
		{
			string remoteFolderPath = this.GetRemoteFolderPath(uri);
			if (remoteFolderPath != null)
			{
				FtpStatus ftpStatus = this.SendCommand("CWD", new string[]
				{
					remoteFolderPath
				});
				if (ftpStatus.StatusCode < FtpStatusCode.CommandOK || ftpStatus.StatusCode >= (FtpStatusCode)300)
				{
					throw this.CreateExceptionFromResponse(ftpStatus);
				}
				int num = uri.LocalPath.LastIndexOf('/');
				if (num >= 0)
				{
					this.file_name = Uri.UnescapeDataString(uri.LocalPath.Substring(num + 1));
				}
			}
		}

		// Token: 0x06001D3A RID: 7482 RVA: 0x000693F0 File Offset: 0x000675F0
		private void ProcessMethod()
		{
			if (!this.GetServicePoint().UsesProxy)
			{
				this.State = FtpWebRequest.RequestState.Connecting;
				this.ResolveHost();
				this.OpenControlConnection();
				this.CWDAndSetFileName(this.requestUri);
				this.SetType();
				string text = this.method;
				uint num = <PrivateImplementationDetails>.ComputeStringHash(text);
				if (num <= 1636987420U)
				{
					if (num <= 172932033U)
					{
						if (num != 61167622U)
						{
							if (num != 111500479U)
							{
								if (num != 172932033U)
								{
									goto IL_248;
								}
								if (!(text == "LIST"))
								{
									goto IL_248;
								}
							}
							else
							{
								if (!(text == "STOR"))
								{
									goto IL_248;
								}
								goto IL_238;
							}
						}
						else
						{
							if (!(text == "STOU"))
							{
								goto IL_248;
							}
							goto IL_238;
						}
					}
					else if (num != 540800083U)
					{
						if (num != 1414193175U)
						{
							if (num != 1636987420U)
							{
								goto IL_248;
							}
							if (!(text == "SIZE"))
							{
								goto IL_248;
							}
							goto IL_240;
						}
						else
						{
							if (!(text == "MKD"))
							{
								goto IL_248;
							}
							goto IL_240;
						}
					}
					else
					{
						if (!(text == "RENAME"))
						{
							goto IL_248;
						}
						goto IL_240;
					}
				}
				else if (num <= 2586094756U)
				{
					if (num != 2190452587U)
					{
						if (num != 2192893693U)
						{
							if (num != 2586094756U)
							{
								goto IL_248;
							}
							if (!(text == "PWD"))
							{
								goto IL_248;
							}
							goto IL_240;
						}
						else
						{
							if (!(text == "DELE"))
							{
								goto IL_248;
							}
							goto IL_240;
						}
					}
					else
					{
						if (!(text == "APPE"))
						{
							goto IL_248;
						}
						goto IL_238;
					}
				}
				else if (num != 3129138359U)
				{
					if (num != 3960558266U)
					{
						if (num != 4117911256U)
						{
							goto IL_248;
						}
						if (!(text == "NLST"))
						{
							goto IL_248;
						}
					}
					else if (!(text == "RETR"))
					{
						goto IL_248;
					}
				}
				else
				{
					if (!(text == "MDTM"))
					{
						goto IL_248;
					}
					goto IL_240;
				}
				this.DownloadData();
				goto IL_25E;
				IL_238:
				this.UploadData();
				goto IL_25E;
				IL_240:
				this.ProcessSimpleMethod();
				goto IL_25E;
				IL_248:
				throw new Exception(string.Format("Support for command {0} not implemented yet", this.method));
				IL_25E:
				this.CheckIfAborted();
				return;
			}
			if (this.method != "RETR")
			{
				throw new NotSupportedException("FTP+proxy only supports RETR");
			}
			HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(this.proxy.GetProxy(this.requestUri));
			httpWebRequest.Address = this.requestUri;
			this.requestState = FtpWebRequest.RequestState.Finished;
			WebResponse response = httpWebRequest.GetResponse();
			this.ftpResponse.Stream = new FtpDataStream(this, response.GetResponseStream(), true);
			this.ftpResponse.StatusCode = FtpStatusCode.CommandOK;
		}

		// Token: 0x06001D3B RID: 7483 RVA: 0x00069661 File Offset: 0x00067861
		private void CloseControlConnection()
		{
			if (this.controlStream != null)
			{
				this.SendCommand("QUIT", Array.Empty<string>());
				this.controlStream.Close();
				this.controlStream = null;
			}
		}

		// Token: 0x06001D3C RID: 7484 RVA: 0x0006968E File Offset: 0x0006788E
		internal void CloseDataConnection()
		{
			if (this.origDataStream != null)
			{
				this.origDataStream.Close();
				this.origDataStream = null;
			}
		}

		// Token: 0x06001D3D RID: 7485 RVA: 0x000696AA File Offset: 0x000678AA
		private void CloseConnection()
		{
			this.CloseControlConnection();
			this.CloseDataConnection();
		}

		// Token: 0x06001D3E RID: 7486 RVA: 0x000696B8 File Offset: 0x000678B8
		private void ProcessSimpleMethod()
		{
			this.State = FtpWebRequest.RequestState.TransferInProgress;
			if (this.method == "PWD")
			{
				this.method = "PWD";
			}
			if (this.method == "RENAME")
			{
				this.method = "RNFR";
			}
			FtpStatus ftpStatus = this.SendCommand(this.method, new string[]
			{
				this.file_name
			});
			this.ftpResponse.Stream = Stream.Null;
			string statusDescription = ftpStatus.StatusDescription;
			string a = this.method;
			if (!(a == "SIZE"))
			{
				if (!(a == "MDTM"))
				{
					if (!(a == "MKD"))
					{
						if (!(a == "CWD"))
						{
							if (!(a == "RNFR"))
							{
								if (a == "DELE")
								{
									if (ftpStatus.StatusCode != FtpStatusCode.FileActionOK)
									{
										throw this.CreateExceptionFromResponse(ftpStatus);
									}
								}
							}
							else
							{
								this.method = "RENAME";
								if (ftpStatus.StatusCode != FtpStatusCode.FileCommandPending)
								{
									throw this.CreateExceptionFromResponse(ftpStatus);
								}
								ftpStatus = this.SendCommand("RNTO", new string[]
								{
									(this.renameTo != null) ? this.renameTo : string.Empty
								});
								if (ftpStatus.StatusCode != FtpStatusCode.FileActionOK)
								{
									throw this.CreateExceptionFromResponse(ftpStatus);
								}
							}
						}
						else
						{
							this.method = "PWD";
							if (ftpStatus.StatusCode != FtpStatusCode.FileActionOK)
							{
								throw this.CreateExceptionFromResponse(ftpStatus);
							}
							ftpStatus = this.SendCommand(this.method, Array.Empty<string>());
							if (ftpStatus.StatusCode != FtpStatusCode.PathnameCreated)
							{
								throw this.CreateExceptionFromResponse(ftpStatus);
							}
						}
					}
					else if (ftpStatus.StatusCode != FtpStatusCode.PathnameCreated)
					{
						throw this.CreateExceptionFromResponse(ftpStatus);
					}
				}
				else
				{
					if (ftpStatus.StatusCode != FtpStatusCode.FileStatus)
					{
						throw this.CreateExceptionFromResponse(ftpStatus);
					}
					this.ftpResponse.LastModified = DateTime.ParseExact(statusDescription.Substring(4), "yyyyMMddHHmmss", null);
				}
			}
			else
			{
				if (ftpStatus.StatusCode != FtpStatusCode.FileStatus)
				{
					throw this.CreateExceptionFromResponse(ftpStatus);
				}
				int num = 4;
				int num2 = 0;
				while (num < statusDescription.Length && char.IsDigit(statusDescription[num]))
				{
					num++;
					num2++;
				}
				if (num2 == 0)
				{
					throw new WebException("Bad format for server response in " + this.method);
				}
				long contentLength;
				if (!long.TryParse(statusDescription.Substring(4, num2), out contentLength))
				{
					throw new WebException("Bad format for server response in " + this.method);
				}
				this.ftpResponse.contentLength = contentLength;
			}
			this.State = FtpWebRequest.RequestState.Finished;
		}

		// Token: 0x06001D3F RID: 7487 RVA: 0x00069944 File Offset: 0x00067B44
		private void UploadData()
		{
			this.State = FtpWebRequest.RequestState.OpeningData;
			this.OpenDataConnection();
			this.State = FtpWebRequest.RequestState.TransferInProgress;
			this.requestStream = new FtpDataStream(this, this.dataStream, false);
			this.asyncResult.Stream = this.requestStream;
		}

		// Token: 0x06001D40 RID: 7488 RVA: 0x0006997E File Offset: 0x00067B7E
		private void DownloadData()
		{
			this.State = FtpWebRequest.RequestState.OpeningData;
			this.OpenDataConnection();
			this.State = FtpWebRequest.RequestState.TransferInProgress;
			this.ftpResponse.Stream = new FtpDataStream(this, this.dataStream, true);
		}

		// Token: 0x06001D41 RID: 7489 RVA: 0x000699AC File Offset: 0x00067BAC
		private void CheckRequestStarted()
		{
			if (this.State != FtpWebRequest.RequestState.Before)
			{
				throw new InvalidOperationException("There is a request currently in progress");
			}
		}

		// Token: 0x06001D42 RID: 7490 RVA: 0x000699C4 File Offset: 0x00067BC4
		private void OpenControlConnection()
		{
			Exception innerException = null;
			Socket socket = null;
			foreach (IPAddress ipaddress in this.hostEntry.AddressList)
			{
				socket = new Socket(ipaddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
				this.remoteEndPoint = new IPEndPoint(ipaddress, this.requestUri.Port);
				if (!this.ServicePoint.CallEndPointDelegate(socket, this.remoteEndPoint))
				{
					socket.Close();
					socket = null;
				}
				else
				{
					try
					{
						socket.Connect(this.remoteEndPoint);
						this.localEndPoint = (IPEndPoint)socket.LocalEndPoint;
						break;
					}
					catch (SocketException innerException)
					{
						socket.Close();
						socket = null;
					}
				}
			}
			if (socket == null)
			{
				throw new WebException("Unable to connect to remote server", innerException, WebExceptionStatus.UnknownError, this.ftpResponse);
			}
			this.controlStream = new NetworkStream(socket);
			this.controlReader = new StreamReader(this.controlStream, Encoding.ASCII);
			this.State = FtpWebRequest.RequestState.Authenticating;
			this.Authenticate();
			FtpStatus ftpStatus = this.SendCommand("OPTS", new string[]
			{
				"utf8",
				"on"
			});
			if (ftpStatus.StatusCode < FtpStatusCode.CommandOK || ftpStatus.StatusCode > (FtpStatusCode)300)
			{
				this.dataEncoding = Encoding.Default;
			}
			else
			{
				this.dataEncoding = Encoding.UTF8;
			}
			ftpStatus = this.SendCommand("PWD", Array.Empty<string>());
			this.initial_path = FtpWebRequest.GetInitialPath(ftpStatus);
		}

		// Token: 0x06001D43 RID: 7491 RVA: 0x00069B34 File Offset: 0x00067D34
		private static string GetInitialPath(FtpStatus status)
		{
			int statusCode = (int)status.StatusCode;
			if (statusCode < 200 || statusCode > 300 || status.StatusDescription.Length <= 4)
			{
				throw new WebException("Error getting current directory: " + status.StatusDescription, null, WebExceptionStatus.UnknownError, null);
			}
			string text = status.StatusDescription.Substring(4);
			if (text[0] == '"')
			{
				int num = text.IndexOf('"', 1);
				if (num == -1)
				{
					throw new WebException("Error getting current directory: PWD -> " + status.StatusDescription, null, WebExceptionStatus.UnknownError, null);
				}
				text = text.Substring(1, num - 1);
			}
			if (!text.EndsWith("/"))
			{
				text += "/";
			}
			return text;
		}

		// Token: 0x06001D44 RID: 7492 RVA: 0x00069BE8 File Offset: 0x00067DE8
		private Socket SetupPassiveConnection(string statusDescription, bool ipv6)
		{
			if (statusDescription.Length < 4)
			{
				throw new WebException("Cannot open passive data connection");
			}
			int num = ipv6 ? this.GetPortV6(statusDescription) : this.GetPortV4(statusDescription);
			if (num < 0 || num > 65535)
			{
				throw new WebException("Cannot open passive data connection");
			}
			IPEndPoint ipendPoint = new IPEndPoint(this.remoteEndPoint.Address, num);
			Socket socket = new Socket(ipendPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			try
			{
				socket.Connect(ipendPoint);
			}
			catch (SocketException)
			{
				socket.Close();
				throw new WebException("Cannot open passive data connection");
			}
			return socket;
		}

		// Token: 0x06001D45 RID: 7493 RVA: 0x00069C84 File Offset: 0x00067E84
		private int GetPortV4(string responseString)
		{
			string[] array = responseString.Split(new char[]
			{
				' ',
				'(',
				',',
				')'
			});
			if (array.Length <= 7)
			{
				throw new FormatException(SR.GetString("The response string '{0}' has invalid format.", new object[]
				{
					responseString
				}));
			}
			int num = array.Length - 1;
			if (array[num] == "" || !char.IsNumber(array[num], 0))
			{
				num--;
			}
			return (int)Convert.ToByte(array[num--], NumberFormatInfo.InvariantInfo) | (int)Convert.ToByte(array[num--], NumberFormatInfo.InvariantInfo) << 8;
		}

		// Token: 0x06001D46 RID: 7494 RVA: 0x00069D14 File Offset: 0x00067F14
		private int GetPortV6(string responseString)
		{
			int num = responseString.LastIndexOf("(");
			int num2 = responseString.LastIndexOf(")");
			if (num == -1 || num2 <= num)
			{
				throw new FormatException(SR.GetString("The response string '{0}' has invalid format.", new object[]
				{
					responseString
				}));
			}
			string[] array = responseString.Substring(num + 1, num2 - num - 1).Split(new char[]
			{
				'|'
			});
			if (array.Length < 4)
			{
				throw new FormatException(SR.GetString("The response string '{0}' has invalid format.", new object[]
				{
					responseString
				}));
			}
			return Convert.ToInt32(array[3], NumberFormatInfo.InvariantInfo);
		}

		// Token: 0x06001D47 RID: 7495 RVA: 0x00069DA8 File Offset: 0x00067FA8
		private string FormatAddress(IPAddress address, int Port)
		{
			byte[] addressBytes = address.GetAddressBytes();
			StringBuilder stringBuilder = new StringBuilder(32);
			foreach (byte value in addressBytes)
			{
				stringBuilder.Append(value);
				stringBuilder.Append(',');
			}
			stringBuilder.Append(Port / 256);
			stringBuilder.Append(',');
			stringBuilder.Append(Port % 256);
			return stringBuilder.ToString();
		}

		// Token: 0x06001D48 RID: 7496 RVA: 0x00069E14 File Offset: 0x00068014
		private string FormatAddressV6(IPAddress address, int port)
		{
			StringBuilder stringBuilder = new StringBuilder(43);
			string value = address.ToString();
			stringBuilder.Append("|2|");
			stringBuilder.Append(value);
			stringBuilder.Append('|');
			stringBuilder.Append(port.ToString(NumberFormatInfo.InvariantInfo));
			stringBuilder.Append('|');
			return stringBuilder.ToString();
		}

		// Token: 0x06001D49 RID: 7497 RVA: 0x00069E70 File Offset: 0x00068070
		private Exception CreateExceptionFromResponse(FtpStatus status)
		{
			FtpWebResponse response = new FtpWebResponse(this, this.requestUri, this.method, status);
			return new WebException("Server returned an error: " + status.StatusDescription, null, WebExceptionStatus.ProtocolError, response);
		}

		// Token: 0x06001D4A RID: 7498 RVA: 0x00069EAC File Offset: 0x000680AC
		internal void SetTransferCompleted()
		{
			if (this.InFinalState())
			{
				return;
			}
			this.State = FtpWebRequest.RequestState.Finished;
			FtpStatus responseStatus = this.GetResponseStatus();
			this.ftpResponse.UpdateStatus(responseStatus);
			if (!this.keepAlive)
			{
				this.CloseConnection();
			}
		}

		// Token: 0x06001D4B RID: 7499 RVA: 0x00069EEA File Offset: 0x000680EA
		internal void OperationCompleted()
		{
			if (!this.keepAlive)
			{
				this.CloseConnection();
			}
		}

		// Token: 0x06001D4C RID: 7500 RVA: 0x00069EFA File Offset: 0x000680FA
		private void SetCompleteWithError(Exception exc)
		{
			if (this.asyncResult != null)
			{
				this.asyncResult.SetCompleted(false, exc);
			}
		}

		// Token: 0x06001D4D RID: 7501 RVA: 0x00069F14 File Offset: 0x00068114
		private Socket InitDataConnection()
		{
			bool flag = this.remoteEndPoint.AddressFamily == AddressFamily.InterNetworkV6;
			if (this.usePassive)
			{
				FtpStatus ftpStatus = this.SendCommand(flag ? "EPSV" : "PASV", Array.Empty<string>());
				if (ftpStatus.StatusCode != (flag ? ((FtpStatusCode)229) : FtpStatusCode.EnteringPassive))
				{
					throw this.CreateExceptionFromResponse(ftpStatus);
				}
				return this.SetupPassiveConnection(ftpStatus.StatusDescription, flag);
			}
			else
			{
				Socket socket = new Socket(this.remoteEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
				try
				{
					socket.Bind(new IPEndPoint(this.localEndPoint.Address, 0));
					socket.Listen(1);
				}
				catch (SocketException innerException)
				{
					socket.Close();
					throw new WebException("Couldn't open listening socket on client", innerException);
				}
				IPEndPoint ipendPoint = (IPEndPoint)socket.LocalEndPoint;
				string text = flag ? this.FormatAddressV6(ipendPoint.Address, ipendPoint.Port) : this.FormatAddress(ipendPoint.Address, ipendPoint.Port);
				FtpStatus ftpStatus = this.SendCommand(flag ? "EPRT" : "PORT", new string[]
				{
					text
				});
				if (ftpStatus.StatusCode != FtpStatusCode.CommandOK)
				{
					socket.Close();
					throw this.CreateExceptionFromResponse(ftpStatus);
				}
				return socket;
			}
		}

		// Token: 0x06001D4E RID: 7502 RVA: 0x0006A050 File Offset: 0x00068250
		private void OpenDataConnection()
		{
			Socket socket = this.InitDataConnection();
			FtpStatus ftpStatus;
			if (this.offset > 0L)
			{
				ftpStatus = this.SendCommand("REST", new string[]
				{
					this.offset.ToString()
				});
				if (ftpStatus.StatusCode != FtpStatusCode.FileCommandPending)
				{
					throw this.CreateExceptionFromResponse(ftpStatus);
				}
			}
			if (this.method != "NLST" && this.method != "LIST" && this.method != "STOU")
			{
				ftpStatus = this.SendCommand(this.method, new string[]
				{
					this.file_name
				});
			}
			else
			{
				ftpStatus = this.SendCommand(this.method, Array.Empty<string>());
			}
			if (ftpStatus.StatusCode != FtpStatusCode.OpeningData && ftpStatus.StatusCode != FtpStatusCode.DataAlreadyOpen)
			{
				throw this.CreateExceptionFromResponse(ftpStatus);
			}
			if (this.usePassive)
			{
				this.origDataStream = new NetworkStream(socket, true);
				this.dataStream = this.origDataStream;
				if (this.EnableSsl)
				{
					this.ChangeToSSLSocket(ref this.dataStream);
				}
			}
			else
			{
				Socket socket2 = null;
				try
				{
					socket2 = socket.Accept();
				}
				catch (SocketException)
				{
					socket.Close();
					if (socket2 != null)
					{
						socket2.Close();
					}
					throw new ProtocolViolationException("Server commited a protocol violation.");
				}
				socket.Close();
				this.origDataStream = new NetworkStream(socket2, true);
				this.dataStream = this.origDataStream;
				if (this.EnableSsl)
				{
					this.ChangeToSSLSocket(ref this.dataStream);
				}
			}
			this.ftpResponse.UpdateStatus(ftpStatus);
		}

		// Token: 0x06001D4F RID: 7503 RVA: 0x0006A1D8 File Offset: 0x000683D8
		private void Authenticate()
		{
			string text = null;
			string text2 = null;
			string text3 = null;
			if (this.credentials != null)
			{
				text = this.credentials.UserName;
				text2 = this.credentials.Password;
				text3 = this.credentials.Domain;
			}
			if (text == null)
			{
				text = "anonymous";
			}
			if (text2 == null)
			{
				text2 = "@anonymous";
			}
			if (!string.IsNullOrEmpty(text3))
			{
				text = text3 + "\\" + text;
			}
			FtpStatus ftpStatus = this.GetResponseStatus();
			this.ftpResponse.BannerMessage = ftpStatus.StatusDescription;
			if (this.EnableSsl)
			{
				this.InitiateSecureConnection(ref this.controlStream);
				this.controlReader = new StreamReader(this.controlStream, Encoding.ASCII);
				ftpStatus = this.SendCommand("PBSZ", new string[]
				{
					"0"
				});
				int statusCode = (int)ftpStatus.StatusCode;
				if (statusCode < 200 || statusCode >= 300)
				{
					throw this.CreateExceptionFromResponse(ftpStatus);
				}
				ftpStatus = this.SendCommand("PROT", new string[]
				{
					"P"
				});
				statusCode = (int)ftpStatus.StatusCode;
				if (statusCode < 200 || statusCode >= 300)
				{
					throw this.CreateExceptionFromResponse(ftpStatus);
				}
				ftpStatus = new FtpStatus(FtpStatusCode.SendUserCommand, "");
			}
			if (ftpStatus.StatusCode != FtpStatusCode.SendUserCommand)
			{
				throw this.CreateExceptionFromResponse(ftpStatus);
			}
			ftpStatus = this.SendCommand("USER", new string[]
			{
				text
			});
			FtpStatusCode statusCode2 = ftpStatus.StatusCode;
			if (statusCode2 != FtpStatusCode.LoggedInProceed)
			{
				if (statusCode2 != FtpStatusCode.SendPasswordCommand)
				{
					throw this.CreateExceptionFromResponse(ftpStatus);
				}
				ftpStatus = this.SendCommand("PASS", new string[]
				{
					text2
				});
				if (ftpStatus.StatusCode != FtpStatusCode.LoggedInProceed)
				{
					throw this.CreateExceptionFromResponse(ftpStatus);
				}
			}
			this.ftpResponse.WelcomeMessage = ftpStatus.StatusDescription;
			this.ftpResponse.UpdateStatus(ftpStatus);
		}

		// Token: 0x06001D50 RID: 7504 RVA: 0x0006A3A0 File Offset: 0x000685A0
		private FtpStatus SendCommand(string command, params string[] parameters)
		{
			return this.SendCommand(true, command, parameters);
		}

		// Token: 0x06001D51 RID: 7505 RVA: 0x0006A3AC File Offset: 0x000685AC
		private FtpStatus SendCommand(bool waitResponse, string command, params string[] parameters)
		{
			string text = command;
			if (parameters.Length != 0)
			{
				text = text + " " + string.Join(" ", parameters);
			}
			text += "\r\n";
			byte[] bytes = this.dataEncoding.GetBytes(text);
			try
			{
				this.controlStream.Write(bytes, 0, bytes.Length);
			}
			catch (IOException)
			{
				return new FtpStatus(FtpStatusCode.ServiceNotAvailable, "Write failed");
			}
			if (!waitResponse)
			{
				return null;
			}
			FtpStatus responseStatus = this.GetResponseStatus();
			if (this.ftpResponse != null)
			{
				this.ftpResponse.UpdateStatus(responseStatus);
			}
			return responseStatus;
		}

		// Token: 0x06001D52 RID: 7506 RVA: 0x0006A448 File Offset: 0x00068648
		internal static FtpStatus ServiceNotAvailable()
		{
			return new FtpStatus(FtpStatusCode.ServiceNotAvailable, Locale.GetText("Invalid response from server"));
		}

		// Token: 0x06001D53 RID: 7507 RVA: 0x0006A460 File Offset: 0x00068660
		internal FtpStatus GetResponseStatus()
		{
			string text = null;
			try
			{
				text = this.controlReader.ReadLine();
			}
			catch (IOException)
			{
			}
			if (text == null || text.Length < 3)
			{
				return FtpWebRequest.ServiceNotAvailable();
			}
			int statusCode;
			if (!int.TryParse(text.Substring(0, 3), out statusCode))
			{
				return FtpWebRequest.ServiceNotAvailable();
			}
			if (text.Length > 3 && text[3] == '-')
			{
				string text2 = null;
				string value = statusCode.ToString() + " ";
				for (;;)
				{
					text2 = null;
					try
					{
						text2 = this.controlReader.ReadLine();
					}
					catch (IOException)
					{
					}
					if (text2 == null)
					{
						break;
					}
					text = text + Environment.NewLine + text2;
					if (text2.StartsWith(value, StringComparison.Ordinal))
					{
						goto IL_97;
					}
				}
				return FtpWebRequest.ServiceNotAvailable();
			}
			IL_97:
			return new FtpStatus((FtpStatusCode)statusCode, text);
		}

		// Token: 0x06001D54 RID: 7508 RVA: 0x0006A528 File Offset: 0x00068728
		private void InitiateSecureConnection(ref Stream stream)
		{
			FtpStatus ftpStatus = this.SendCommand("AUTH", new string[]
			{
				"TLS"
			});
			if (ftpStatus.StatusCode != FtpStatusCode.ServerWantsSecureSession)
			{
				throw this.CreateExceptionFromResponse(ftpStatus);
			}
			this.ChangeToSSLSocket(ref stream);
		}

		// Token: 0x06001D55 RID: 7509 RVA: 0x0006A56C File Offset: 0x0006876C
		internal bool ChangeToSSLSocket(ref Stream stream)
		{
			MonoTlsProvider providerInternal = Mono.Net.Security.MonoTlsProviderFactory.GetProviderInternal();
			MonoTlsSettings monoTlsSettings = MonoTlsSettings.CopyDefaultSettings();
			monoTlsSettings.UseServicePointManagerCallback = new bool?(true);
			IMonoSslStream monoSslStream = providerInternal.CreateSslStream(stream, true, monoTlsSettings);
			monoSslStream.AuthenticateAsClient(this.requestUri.Host, null, SslProtocols.Default, false);
			stream = monoSslStream.AuthenticatedStream;
			return true;
		}

		// Token: 0x06001D56 RID: 7510 RVA: 0x0006A5BB File Offset: 0x000687BB
		private bool InFinalState()
		{
			return this.State == FtpWebRequest.RequestState.Aborted || this.State == FtpWebRequest.RequestState.Error || this.State == FtpWebRequest.RequestState.Finished;
		}

		// Token: 0x06001D57 RID: 7511 RVA: 0x0006A5DA File Offset: 0x000687DA
		private bool InProgress()
		{
			return this.State != FtpWebRequest.RequestState.Before && !this.InFinalState();
		}

		// Token: 0x06001D58 RID: 7512 RVA: 0x0006A5EF File Offset: 0x000687EF
		internal void CheckIfAborted()
		{
			if (this.State == FtpWebRequest.RequestState.Aborted)
			{
				throw new WebException("Request aborted", WebExceptionStatus.RequestCanceled);
			}
		}

		// Token: 0x06001D59 RID: 7513 RVA: 0x0006A606 File Offset: 0x00068806
		private void CheckFinalState()
		{
			if (this.InFinalState())
			{
				throw new InvalidOperationException("Cannot change final state");
			}
		}

		// Token: 0x06001D5A RID: 7514 RVA: 0x0006A61C File Offset: 0x0006881C
		// Note: this type is marked as 'beforefieldinit'.
		static FtpWebRequest()
		{
		}

		// Token: 0x06001D5B RID: 7515 RVA: 0x000092E2 File Offset: 0x000074E2
		internal FtpWebRequest()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040019AA RID: 6570
		private Uri requestUri;

		// Token: 0x040019AB RID: 6571
		private string file_name;

		// Token: 0x040019AC RID: 6572
		private ServicePoint servicePoint;

		// Token: 0x040019AD RID: 6573
		private Stream origDataStream;

		// Token: 0x040019AE RID: 6574
		private Stream dataStream;

		// Token: 0x040019AF RID: 6575
		private Stream controlStream;

		// Token: 0x040019B0 RID: 6576
		private StreamReader controlReader;

		// Token: 0x040019B1 RID: 6577
		private NetworkCredential credentials;

		// Token: 0x040019B2 RID: 6578
		private IPHostEntry hostEntry;

		// Token: 0x040019B3 RID: 6579
		private IPEndPoint localEndPoint;

		// Token: 0x040019B4 RID: 6580
		private IPEndPoint remoteEndPoint;

		// Token: 0x040019B5 RID: 6581
		private IWebProxy proxy;

		// Token: 0x040019B6 RID: 6582
		private int timeout;

		// Token: 0x040019B7 RID: 6583
		private int rwTimeout;

		// Token: 0x040019B8 RID: 6584
		private long offset;

		// Token: 0x040019B9 RID: 6585
		private bool binary;

		// Token: 0x040019BA RID: 6586
		private bool enableSsl;

		// Token: 0x040019BB RID: 6587
		private bool usePassive;

		// Token: 0x040019BC RID: 6588
		private bool keepAlive;

		// Token: 0x040019BD RID: 6589
		private string method;

		// Token: 0x040019BE RID: 6590
		private string renameTo;

		// Token: 0x040019BF RID: 6591
		private object locker;

		// Token: 0x040019C0 RID: 6592
		private FtpWebRequest.RequestState requestState;

		// Token: 0x040019C1 RID: 6593
		private FtpAsyncResult asyncResult;

		// Token: 0x040019C2 RID: 6594
		private FtpWebResponse ftpResponse;

		// Token: 0x040019C3 RID: 6595
		private Stream requestStream;

		// Token: 0x040019C4 RID: 6596
		private string initial_path;

		// Token: 0x040019C5 RID: 6597
		private const string ChangeDir = "CWD";

		// Token: 0x040019C6 RID: 6598
		private const string UserCommand = "USER";

		// Token: 0x040019C7 RID: 6599
		private const string PasswordCommand = "PASS";

		// Token: 0x040019C8 RID: 6600
		private const string TypeCommand = "TYPE";

		// Token: 0x040019C9 RID: 6601
		private const string PassiveCommand = "PASV";

		// Token: 0x040019CA RID: 6602
		private const string ExtendedPassiveCommand = "EPSV";

		// Token: 0x040019CB RID: 6603
		private const string PortCommand = "PORT";

		// Token: 0x040019CC RID: 6604
		private const string ExtendedPortCommand = "EPRT";

		// Token: 0x040019CD RID: 6605
		private const string AbortCommand = "ABOR";

		// Token: 0x040019CE RID: 6606
		private const string AuthCommand = "AUTH";

		// Token: 0x040019CF RID: 6607
		private const string RestCommand = "REST";

		// Token: 0x040019D0 RID: 6608
		private const string RenameFromCommand = "RNFR";

		// Token: 0x040019D1 RID: 6609
		private const string RenameToCommand = "RNTO";

		// Token: 0x040019D2 RID: 6610
		private const string QuitCommand = "QUIT";

		// Token: 0x040019D3 RID: 6611
		private const string EOL = "\r\n";

		// Token: 0x040019D4 RID: 6612
		private static readonly string[] supportedCommands = new string[]
		{
			"APPE",
			"DELE",
			"LIST",
			"MDTM",
			"MKD",
			"NLST",
			"PWD",
			"RENAME",
			"RETR",
			"RMD",
			"SIZE",
			"STOR",
			"STOU"
		};

		// Token: 0x040019D5 RID: 6613
		private Encoding dataEncoding;

		// Token: 0x020003DB RID: 987
		private enum RequestState
		{
			// Token: 0x040019D7 RID: 6615
			Before,
			// Token: 0x040019D8 RID: 6616
			Scheduled,
			// Token: 0x040019D9 RID: 6617
			Connecting,
			// Token: 0x040019DA RID: 6618
			Authenticating,
			// Token: 0x040019DB RID: 6619
			OpeningData,
			// Token: 0x040019DC RID: 6620
			TransferInProgress,
			// Token: 0x040019DD RID: 6621
			Finished,
			// Token: 0x040019DE RID: 6622
			Aborted,
			// Token: 0x040019DF RID: 6623
			Error
		}
	}
}
