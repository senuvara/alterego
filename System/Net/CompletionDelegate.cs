﻿using System;

namespace System.Net
{
	// Token: 0x0200039E RID: 926
	// (Invoke) Token: 0x06001B7E RID: 7038
	internal delegate void CompletionDelegate(byte[] responseBytes, Exception exception, object State);
}
