﻿using System;

namespace System.Net
{
	// Token: 0x02000382 RID: 898
	internal class FileWebRequestCreator : IWebRequestCreate
	{
		// Token: 0x060019FA RID: 6650 RVA: 0x0000232F File Offset: 0x0000052F
		internal FileWebRequestCreator()
		{
		}

		// Token: 0x060019FB RID: 6651 RVA: 0x0005EDE0 File Offset: 0x0005CFE0
		public WebRequest Create(Uri uri)
		{
			return new FileWebRequest(uri);
		}
	}
}
