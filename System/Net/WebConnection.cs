﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using Mono.Net.Security;

namespace System.Net
{
	// Token: 0x0200040A RID: 1034
	internal class WebConnection
	{
		// Token: 0x170006FD RID: 1789
		// (get) Token: 0x06002029 RID: 8233 RVA: 0x000741F9 File Offset: 0x000723F9
		internal MonoChunkStream MonoChunkStream
		{
			get
			{
				return this.chunkStream;
			}
		}

		// Token: 0x0600202A RID: 8234 RVA: 0x00074204 File Offset: 0x00072404
		public WebConnection(IWebConnectionState wcs, ServicePoint sPoint)
		{
			this.state = wcs;
			this.sPoint = sPoint;
			this.buffer = new byte[4096];
			this.Data = new WebConnectionData();
			this.queue = wcs.Group.Queue;
			this.abortHelper = new WebConnection.AbortHelper();
			this.abortHelper.Connection = this;
			this.abortHandler = new EventHandler(this.abortHelper.Abort);
		}

		// Token: 0x0600202B RID: 8235 RVA: 0x0007428A File Offset: 0x0007248A
		private bool CanReuse()
		{
			return !this.socket.Poll(0, SelectMode.SelectRead);
		}

		// Token: 0x0600202C RID: 8236 RVA: 0x0007429C File Offset: 0x0007249C
		private void Connect(HttpWebRequest request)
		{
			object obj = this.socketLock;
			lock (obj)
			{
				if (this.socket != null && this.socket.Connected && this.status == WebExceptionStatus.Success && this.CanReuse() && this.CompleteChunkedRead())
				{
					this.reused = true;
				}
				else
				{
					this.reused = false;
					if (this.socket != null)
					{
						this.socket.Close();
						this.socket = null;
					}
					this.chunkStream = null;
					IPHostEntry hostEntry = this.sPoint.HostEntry;
					if (hostEntry == null)
					{
						this.status = (this.sPoint.UsesProxy ? WebExceptionStatus.ProxyNameResolutionFailure : WebExceptionStatus.NameResolutionFailure);
					}
					else
					{
						foreach (IPAddress ipaddress in hostEntry.AddressList)
						{
							try
							{
								this.socket = new Socket(ipaddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
							}
							catch (Exception ex)
							{
								if (!request.Aborted)
								{
									this.status = WebExceptionStatus.ConnectFailure;
								}
								this.connect_exception = ex;
								break;
							}
							IPEndPoint ipendPoint = new IPEndPoint(ipaddress, this.sPoint.Address.Port);
							this.socket.NoDelay = !this.sPoint.UseNagleAlgorithm;
							try
							{
								this.sPoint.KeepAliveSetup(this.socket);
							}
							catch
							{
							}
							if (!this.sPoint.CallEndPointDelegate(this.socket, ipendPoint))
							{
								this.socket.Close();
								this.socket = null;
								this.status = WebExceptionStatus.ConnectFailure;
							}
							else
							{
								try
								{
									if (request.Aborted)
									{
										break;
									}
									this.socket.Connect(ipendPoint);
									this.status = WebExceptionStatus.Success;
									break;
								}
								catch (ThreadAbortException)
								{
									Socket socket = this.socket;
									this.socket = null;
									if (socket != null)
									{
										socket.Close();
									}
									break;
								}
								catch (ObjectDisposedException)
								{
									break;
								}
								catch (Exception ex2)
								{
									Socket socket2 = this.socket;
									this.socket = null;
									if (socket2 != null)
									{
										socket2.Close();
									}
									if (!request.Aborted)
									{
										this.status = WebExceptionStatus.ConnectFailure;
									}
									this.connect_exception = ex2;
								}
							}
						}
					}
				}
			}
		}

		// Token: 0x0600202D RID: 8237 RVA: 0x00074534 File Offset: 0x00072734
		private bool CreateTunnel(HttpWebRequest request, Uri connectUri, Stream stream, out byte[] buffer)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("CONNECT ");
			stringBuilder.Append(request.Address.Host);
			stringBuilder.Append(':');
			stringBuilder.Append(request.Address.Port);
			stringBuilder.Append(" HTTP/");
			if (request.ServicePoint.ProtocolVersion == HttpVersion.Version11)
			{
				stringBuilder.Append("1.1");
			}
			else
			{
				stringBuilder.Append("1.0");
			}
			stringBuilder.Append("\r\nHost: ");
			stringBuilder.Append(request.Address.Authority);
			bool flag = false;
			string[] challenge = this.Data.Challenge;
			this.Data.Challenge = null;
			string text = request.Headers["Proxy-Authorization"];
			bool flag2 = text != null;
			if (flag2)
			{
				stringBuilder.Append("\r\nProxy-Authorization: ");
				stringBuilder.Append(text);
				flag = text.ToUpper().Contains("NTLM");
			}
			else if (challenge != null && this.Data.StatusCode == 407)
			{
				ICredentials credentials = request.Proxy.Credentials;
				flag2 = true;
				if (this.connect_request == null)
				{
					this.connect_request = (HttpWebRequest)WebRequest.Create(string.Concat(new object[]
					{
						connectUri.Scheme,
						"://",
						connectUri.Host,
						":",
						connectUri.Port,
						"/"
					}));
					this.connect_request.Method = "CONNECT";
					this.connect_request.Credentials = credentials;
				}
				if (credentials != null)
				{
					for (int i = 0; i < challenge.Length; i++)
					{
						Authorization authorization = AuthenticationManager.Authenticate(challenge[i], this.connect_request, credentials);
						if (authorization != null)
						{
							flag = (authorization.ModuleAuthenticationType == "NTLM");
							stringBuilder.Append("\r\nProxy-Authorization: ");
							stringBuilder.Append(authorization.Message);
							break;
						}
					}
				}
			}
			if (flag)
			{
				stringBuilder.Append("\r\nProxy-Connection: keep-alive");
				this.connect_ntlm_auth_state++;
			}
			stringBuilder.Append("\r\n\r\n");
			this.Data.StatusCode = 0;
			byte[] bytes = Encoding.Default.GetBytes(stringBuilder.ToString());
			stream.Write(bytes, 0, bytes.Length);
			int num;
			WebHeaderCollection webHeaderCollection = this.ReadHeaders(stream, out buffer, out num);
			if ((!flag2 || this.connect_ntlm_auth_state == WebConnection.NtlmAuthState.Challenge) && webHeaderCollection != null && num == 407)
			{
				string text2 = webHeaderCollection["Connection"];
				if (this.socket != null && !string.IsNullOrEmpty(text2) && text2.ToLower() == "close")
				{
					this.socket.Close();
					this.socket = null;
				}
				this.Data.StatusCode = num;
				this.Data.Challenge = webHeaderCollection.GetValues("Proxy-Authenticate");
				this.Data.Headers = webHeaderCollection;
				return false;
			}
			if (num != 200)
			{
				this.Data.StatusCode = num;
				this.Data.Headers = webHeaderCollection;
				return false;
			}
			return webHeaderCollection != null;
		}

		// Token: 0x0600202E RID: 8238 RVA: 0x0007485C File Offset: 0x00072A5C
		private WebHeaderCollection ReadHeaders(Stream stream, out byte[] retBuffer, out int status)
		{
			retBuffer = null;
			status = 200;
			byte[] array = new byte[1024];
			MemoryStream memoryStream = new MemoryStream();
			int num2;
			WebHeaderCollection webHeaderCollection;
			for (;;)
			{
				int num = stream.Read(array, 0, 1024);
				if (num == 0)
				{
					break;
				}
				memoryStream.Write(array, 0, num);
				num2 = 0;
				string text = null;
				bool flag = false;
				webHeaderCollection = new WebHeaderCollection();
				while (WebConnection.ReadLine(memoryStream.GetBuffer(), ref num2, (int)memoryStream.Length, ref text))
				{
					if (text == null)
					{
						goto Block_2;
					}
					if (flag)
					{
						webHeaderCollection.Add(text);
					}
					else
					{
						string[] array2 = text.Split(new char[]
						{
							' '
						});
						if (array2.Length < 2)
						{
							goto Block_6;
						}
						if (string.Compare(array2[0], "HTTP/1.1", true) == 0)
						{
							this.Data.ProxyVersion = HttpVersion.Version11;
						}
						else
						{
							if (string.Compare(array2[0], "HTTP/1.0", true) != 0)
							{
								goto IL_153;
							}
							this.Data.ProxyVersion = HttpVersion.Version10;
						}
						status = (int)uint.Parse(array2[1]);
						if (array2.Length >= 3)
						{
							this.Data.StatusDescription = string.Join(" ", array2, 2, array2.Length - 2);
						}
						flag = true;
					}
				}
			}
			this.HandleError(WebExceptionStatus.ServerProtocolViolation, null, "ReadHeaders");
			return null;
			Block_2:
			int num3 = 0;
			try
			{
				num3 = int.Parse(webHeaderCollection["Content-Length"]);
			}
			catch
			{
				num3 = 0;
			}
			if (memoryStream.Length - (long)num2 - (long)num3 > 0L)
			{
				retBuffer = new byte[memoryStream.Length - (long)num2 - (long)num3];
				Buffer.BlockCopy(memoryStream.GetBuffer(), num2 + num3, retBuffer, 0, retBuffer.Length);
			}
			else
			{
				this.FlushContents(stream, num3 - (int)(memoryStream.Length - (long)num2));
			}
			return webHeaderCollection;
			Block_6:
			this.HandleError(WebExceptionStatus.ServerProtocolViolation, null, "ReadHeaders2");
			return null;
			IL_153:
			this.HandleError(WebExceptionStatus.ServerProtocolViolation, null, "ReadHeaders2");
			return null;
		}

		// Token: 0x0600202F RID: 8239 RVA: 0x00074A30 File Offset: 0x00072C30
		private void FlushContents(Stream stream, int contentLength)
		{
			while (contentLength > 0)
			{
				byte[] array = new byte[contentLength];
				int num = stream.Read(array, 0, contentLength);
				if (num <= 0)
				{
					break;
				}
				contentLength -= num;
			}
		}

		// Token: 0x06002030 RID: 8240 RVA: 0x00074A60 File Offset: 0x00072C60
		private bool CreateStream(HttpWebRequest request)
		{
			try
			{
				NetworkStream networkStream = new NetworkStream(this.socket, false);
				if (request.Address.Scheme == Uri.UriSchemeHttps)
				{
					if (!this.reused || this.nstream == null || this.tlsStream == null)
					{
						byte[] array = null;
						if (this.sPoint.UseConnect && !this.CreateTunnel(request, this.sPoint.Address, networkStream, out array))
						{
							return false;
						}
						this.tlsStream = new MonoTlsStream(request, networkStream);
						this.nstream = this.tlsStream.CreateStream(array);
					}
				}
				else
				{
					this.nstream = networkStream;
				}
			}
			catch (Exception ex)
			{
				if (this.tlsStream != null)
				{
					this.status = this.tlsStream.ExceptionStatus;
				}
				else if (!request.Aborted)
				{
					this.status = WebExceptionStatus.ConnectFailure;
				}
				this.connect_exception = ex;
				return false;
			}
			return true;
		}

		// Token: 0x06002031 RID: 8241 RVA: 0x00074B48 File Offset: 0x00072D48
		private void HandleError(WebExceptionStatus st, Exception ex, string where)
		{
			this.status = st;
			lock (this)
			{
				if (st == WebExceptionStatus.RequestCanceled)
				{
					this.Data = new WebConnectionData();
				}
			}
			if (ex == null)
			{
				try
				{
					throw new Exception(new StackTrace().ToString());
				}
				catch (Exception ex)
				{
				}
			}
			HttpWebRequest httpWebRequest = null;
			if (this.Data != null && this.Data.request != null)
			{
				httpWebRequest = this.Data.request;
			}
			this.Close(true);
			if (httpWebRequest != null)
			{
				httpWebRequest.FinishedReading = true;
				httpWebRequest.SetResponseError(st, ex, where);
			}
		}

		// Token: 0x06002032 RID: 8242 RVA: 0x00074BF4 File Offset: 0x00072DF4
		private void ReadDone(IAsyncResult result)
		{
			WebConnectionData data = this.Data;
			Stream stream = this.nstream;
			if (stream == null)
			{
				this.Close(true);
				return;
			}
			int num = -1;
			try
			{
				num = stream.EndRead(result);
			}
			catch (ObjectDisposedException)
			{
				return;
			}
			catch (Exception ex)
			{
				if (ex.InnerException is ObjectDisposedException)
				{
					return;
				}
				this.HandleError(WebExceptionStatus.ReceiveFailure, ex, "ReadDone1");
				return;
			}
			if (num == 0)
			{
				this.HandleError(WebExceptionStatus.ReceiveFailure, null, "ReadDone2");
				return;
			}
			if (num < 0)
			{
				this.HandleError(WebExceptionStatus.ServerProtocolViolation, null, "ReadDone3");
				return;
			}
			int num2 = -1;
			num += this.position;
			if (data.ReadState == ReadState.None)
			{
				Exception ex2 = null;
				try
				{
					num2 = WebConnection.GetResponse(data, this.sPoint, this.buffer, num);
				}
				catch (Exception ex2)
				{
				}
				if (ex2 != null || num2 == -1)
				{
					this.HandleError(WebExceptionStatus.ServerProtocolViolation, ex2, "ReadDone4");
					return;
				}
			}
			if (data.ReadState == ReadState.Aborted)
			{
				this.HandleError(WebExceptionStatus.RequestCanceled, null, "ReadDone");
				return;
			}
			if (data.ReadState != ReadState.Content)
			{
				int num3 = num * 2;
				byte[] dst = new byte[(num3 < this.buffer.Length) ? this.buffer.Length : num3];
				Buffer.BlockCopy(this.buffer, 0, dst, 0, num);
				this.buffer = dst;
				this.position = num;
				data.ReadState = ReadState.None;
				this.InitRead();
				return;
			}
			this.position = 0;
			WebConnectionStream webConnectionStream = new WebConnectionStream(this, data);
			bool flag = WebConnection.ExpectContent(data.StatusCode, data.request.Method);
			string text = null;
			if (flag)
			{
				text = data.Headers["Transfer-Encoding"];
			}
			this.chunkedRead = (text != null && text.IndexOf("chunked", StringComparison.OrdinalIgnoreCase) != -1);
			if (!this.chunkedRead)
			{
				webConnectionStream.ReadBuffer = this.buffer;
				webConnectionStream.ReadBufferOffset = num2;
				webConnectionStream.ReadBufferSize = num;
				try
				{
					webConnectionStream.CheckResponseInBuffer();
					goto IL_23A;
				}
				catch (Exception e)
				{
					this.HandleError(WebExceptionStatus.ReceiveFailure, e, "ReadDone7");
					goto IL_23A;
				}
			}
			if (this.chunkStream == null)
			{
				try
				{
					this.chunkStream = new MonoChunkStream(this.buffer, num2, num, data.Headers);
					goto IL_23A;
				}
				catch (Exception e2)
				{
					this.HandleError(WebExceptionStatus.ServerProtocolViolation, e2, "ReadDone5");
					return;
				}
			}
			this.chunkStream.ResetBuffer();
			try
			{
				this.chunkStream.Write(this.buffer, num2, num);
			}
			catch (Exception e3)
			{
				this.HandleError(WebExceptionStatus.ServerProtocolViolation, e3, "ReadDone6");
				return;
			}
			IL_23A:
			data.stream = webConnectionStream;
			if (!flag)
			{
				webConnectionStream.ForceCompletion();
			}
			data.request.SetResponseData(data);
		}

		// Token: 0x06002033 RID: 8243 RVA: 0x00074EA8 File Offset: 0x000730A8
		private static bool ExpectContent(int statusCode, string method)
		{
			return !(method == "HEAD") && (statusCode >= 200 && statusCode != 204) && statusCode != 304;
		}

		// Token: 0x06002034 RID: 8244 RVA: 0x00074ED8 File Offset: 0x000730D8
		internal void InitRead()
		{
			Stream stream = this.nstream;
			try
			{
				int count = this.buffer.Length - this.position;
				stream.BeginRead(this.buffer, this.position, count, new AsyncCallback(this.ReadDone), null);
			}
			catch (Exception e)
			{
				this.HandleError(WebExceptionStatus.ReceiveFailure, e, "InitRead");
			}
		}

		// Token: 0x06002035 RID: 8245 RVA: 0x00074F40 File Offset: 0x00073140
		private static int GetResponse(WebConnectionData data, ServicePoint sPoint, byte[] buffer, int max)
		{
			int num = 0;
			string text = null;
			bool flag = false;
			bool flag2 = false;
			while (data.ReadState != ReadState.Aborted)
			{
				if (data.ReadState != ReadState.None)
				{
					goto IL_DD;
				}
				if (!WebConnection.ReadLine(buffer, ref num, max, ref text))
				{
					return 0;
				}
				if (text == null)
				{
					flag2 = true;
				}
				else
				{
					flag2 = false;
					data.ReadState = ReadState.Status;
					string[] array = text.Split(new char[]
					{
						' '
					});
					if (array.Length < 2)
					{
						return -1;
					}
					if (string.Compare(array[0], "HTTP/1.1", true) == 0)
					{
						data.Version = HttpVersion.Version11;
						sPoint.SetVersion(HttpVersion.Version11);
					}
					else
					{
						data.Version = HttpVersion.Version10;
						sPoint.SetVersion(HttpVersion.Version10);
					}
					data.StatusCode = (int)uint.Parse(array[1]);
					if (array.Length >= 3)
					{
						data.StatusDescription = string.Join(" ", array, 2, array.Length - 2);
					}
					else
					{
						data.StatusDescription = "";
					}
					if (num >= max)
					{
						return num;
					}
					goto IL_DD;
				}
				IL_278:
				if (!flag2 && !flag)
				{
					return -1;
				}
				continue;
				IL_DD:
				flag2 = false;
				if (data.ReadState != ReadState.Status)
				{
					goto IL_278;
				}
				data.ReadState = ReadState.Headers;
				data.Headers = new WebHeaderCollection();
				ArrayList arrayList = new ArrayList();
				bool flag3 = false;
				while (!flag3 && WebConnection.ReadLine(buffer, ref num, max, ref text))
				{
					if (text == null)
					{
						flag3 = true;
					}
					else if (text.Length > 0 && (text[0] == ' ' || text[0] == '\t'))
					{
						int num2 = arrayList.Count - 1;
						if (num2 < 0)
						{
							break;
						}
						string value = (string)arrayList[num2] + text;
						arrayList[num2] = value;
					}
					else
					{
						arrayList.Add(text);
					}
				}
				if (!flag3)
				{
					return 0;
				}
				foreach (object obj in arrayList)
				{
					string text2 = (string)obj;
					int num3 = text2.IndexOf(':');
					if (num3 == -1)
					{
						throw new ArgumentException("no colon found", "header");
					}
					string name = text2.Substring(0, num3);
					string value2 = text2.Substring(num3 + 1).Trim();
					WebHeaderCollection headers = data.Headers;
					if (WebHeaderCollection.AllowMultiValues(name))
					{
						headers.AddInternal(name, value2);
					}
					else
					{
						headers.SetInternal(name, value2);
					}
				}
				if (data.StatusCode != 100)
				{
					data.ReadState = ReadState.Content;
					return num;
				}
				sPoint.SendContinue = true;
				if (num >= max)
				{
					return num;
				}
				if (data.request.ExpectContinue)
				{
					data.request.DoContinueDelegate(data.StatusCode, data.Headers);
					data.request.ExpectContinue = false;
				}
				data.ReadState = ReadState.None;
				flag = true;
				goto IL_278;
			}
			return -1;
		}

		// Token: 0x06002036 RID: 8246 RVA: 0x000751E0 File Offset: 0x000733E0
		private void InitConnection(HttpWebRequest request)
		{
			request.WebConnection = this;
			if (request.ReuseConnection)
			{
				request.StoredConnection = this;
			}
			if (request.Aborted)
			{
				return;
			}
			this.keepAlive = request.KeepAlive;
			this.Data = new WebConnectionData(request);
			WebExceptionStatus webExceptionStatus;
			for (;;)
			{
				this.Connect(request);
				if (request.Aborted)
				{
					break;
				}
				if (this.status != WebExceptionStatus.Success)
				{
					goto Block_4;
				}
				if (this.CreateStream(request))
				{
					goto IL_145;
				}
				if (request.Aborted)
				{
					return;
				}
				webExceptionStatus = this.status;
				if (this.Data.Challenge == null)
				{
					goto Block_8;
				}
			}
			return;
			Block_4:
			if (!request.Aborted)
			{
				request.SetWriteStreamError(this.status, this.connect_exception);
				this.Close(true);
			}
			return;
			Block_8:
			Exception ex = this.connect_exception;
			if (ex == null && (this.Data.StatusCode == 401 || this.Data.StatusCode == 407))
			{
				webExceptionStatus = WebExceptionStatus.ProtocolError;
				if (this.Data.Headers == null)
				{
					this.Data.Headers = new WebHeaderCollection();
				}
				HttpWebResponse response = new HttpWebResponse(this.sPoint.Address, "CONNECT", this.Data, null);
				ex = new WebException((this.Data.StatusCode == 407) ? "(407) Proxy Authentication Required" : "(401) Unauthorized", null, webExceptionStatus, response);
			}
			this.connect_exception = null;
			request.SetWriteStreamError(webExceptionStatus, ex);
			this.Close(true);
			return;
			IL_145:
			request.SetWriteStream(new WebConnectionStream(this, request));
		}

		// Token: 0x06002037 RID: 8247 RVA: 0x00075340 File Offset: 0x00073540
		internal EventHandler SendRequest(HttpWebRequest request)
		{
			if (request.Aborted)
			{
				return null;
			}
			lock (this)
			{
				if (this.state.TrySetBusy())
				{
					this.status = WebExceptionStatus.Success;
					ThreadPool.QueueUserWorkItem(delegate(object o)
					{
						try
						{
							this.InitConnection((HttpWebRequest)o);
						}
						catch
						{
						}
					}, request);
				}
				else
				{
					Queue obj = this.queue;
					lock (obj)
					{
						this.queue.Enqueue(request);
					}
				}
			}
			return this.abortHandler;
		}

		// Token: 0x06002038 RID: 8248 RVA: 0x000753E4 File Offset: 0x000735E4
		private void SendNext()
		{
			Queue obj = this.queue;
			lock (obj)
			{
				if (this.queue.Count > 0)
				{
					this.SendRequest((HttpWebRequest)this.queue.Dequeue());
				}
			}
		}

		// Token: 0x06002039 RID: 8249 RVA: 0x00075444 File Offset: 0x00073644
		internal void NextRead()
		{
			lock (this)
			{
				if (this.Data.request != null)
				{
					this.Data.request.FinishedReading = true;
				}
				string name = this.sPoint.UsesProxy ? "Proxy-Connection" : "Connection";
				string text = (this.Data.Headers != null) ? this.Data.Headers[name] : null;
				bool flag2 = this.Data.Version == HttpVersion.Version11 && this.keepAlive;
				if (this.Data.ProxyVersion != null && this.Data.ProxyVersion != HttpVersion.Version11)
				{
					flag2 = false;
				}
				if (text != null)
				{
					text = text.ToLower();
					flag2 = (this.keepAlive && text.IndexOf("keep-alive", StringComparison.Ordinal) != -1);
				}
				if ((this.socket != null && !this.socket.Connected) || !flag2 || (text != null && text.IndexOf("close", StringComparison.Ordinal) != -1))
				{
					this.Close(false);
				}
				this.state.SetIdle();
				if (this.priority_request != null)
				{
					this.SendRequest(this.priority_request);
					this.priority_request = null;
				}
				else
				{
					this.SendNext();
				}
			}
		}

		// Token: 0x0600203A RID: 8250 RVA: 0x000755B8 File Offset: 0x000737B8
		private static bool ReadLine(byte[] buffer, ref int start, int max, ref string output)
		{
			bool flag = false;
			StringBuilder stringBuilder = new StringBuilder();
			int num = 0;
			while (start < max)
			{
				int num2 = start;
				start = num2 + 1;
				num = (int)buffer[num2];
				if (num == 10)
				{
					if (stringBuilder.Length > 0 && stringBuilder[stringBuilder.Length - 1] == '\r')
					{
						StringBuilder stringBuilder2 = stringBuilder;
						num2 = stringBuilder2.Length;
						stringBuilder2.Length = num2 - 1;
					}
					flag = false;
					break;
				}
				if (flag)
				{
					StringBuilder stringBuilder3 = stringBuilder;
					num2 = stringBuilder3.Length;
					stringBuilder3.Length = num2 - 1;
					break;
				}
				if (num == 13)
				{
					flag = true;
				}
				stringBuilder.Append((char)num);
			}
			if (num != 10 && num != 13)
			{
				return false;
			}
			if (stringBuilder.Length == 0)
			{
				output = null;
				return num == 10 || num == 13;
			}
			if (flag)
			{
				StringBuilder stringBuilder4 = stringBuilder;
				int num2 = stringBuilder4.Length;
				stringBuilder4.Length = num2 - 1;
			}
			output = stringBuilder.ToString();
			return true;
		}

		// Token: 0x0600203B RID: 8251 RVA: 0x0007567C File Offset: 0x0007387C
		internal IAsyncResult BeginRead(HttpWebRequest request, byte[] buffer, int offset, int size, AsyncCallback cb, object state)
		{
			Stream stream = null;
			lock (this)
			{
				if (this.Data.request != request)
				{
					throw new ObjectDisposedException(typeof(NetworkStream).FullName);
				}
				if (this.nstream == null)
				{
					return null;
				}
				stream = this.nstream;
			}
			IAsyncResult asyncResult = null;
			if (!this.chunkedRead || (!this.chunkStream.DataAvailable && this.chunkStream.WantMore))
			{
				try
				{
					asyncResult = stream.BeginRead(buffer, offset, size, cb, state);
					cb = null;
				}
				catch (Exception)
				{
					this.HandleError(WebExceptionStatus.ReceiveFailure, null, "chunked BeginRead");
					throw;
				}
			}
			if (this.chunkedRead)
			{
				WebAsyncResult webAsyncResult = new WebAsyncResult(cb, state, buffer, offset, size);
				webAsyncResult.InnerAsyncResult = asyncResult;
				if (asyncResult == null)
				{
					webAsyncResult.SetCompleted(true, null);
					webAsyncResult.DoCallback();
				}
				return webAsyncResult;
			}
			return asyncResult;
		}

		// Token: 0x0600203C RID: 8252 RVA: 0x0007577C File Offset: 0x0007397C
		internal int EndRead(HttpWebRequest request, IAsyncResult result)
		{
			Stream stream = null;
			lock (this)
			{
				if (request.Aborted)
				{
					throw new WebException("Request aborted", WebExceptionStatus.RequestCanceled);
				}
				if (this.Data.request != request)
				{
					throw new ObjectDisposedException(typeof(NetworkStream).FullName);
				}
				if (this.nstream == null)
				{
					throw new ObjectDisposedException(typeof(NetworkStream).FullName);
				}
				stream = this.nstream;
			}
			int num = 0;
			bool flag2 = false;
			WebAsyncResult webAsyncResult = null;
			IAsyncResult innerAsyncResult = ((WebAsyncResult)result).InnerAsyncResult;
			if (this.chunkedRead && innerAsyncResult is WebAsyncResult)
			{
				webAsyncResult = (WebAsyncResult)innerAsyncResult;
				IAsyncResult innerAsyncResult2 = webAsyncResult.InnerAsyncResult;
				if (innerAsyncResult2 != null && !(innerAsyncResult2 is WebAsyncResult))
				{
					num = stream.EndRead(innerAsyncResult2);
					flag2 = (num == 0);
				}
			}
			else if (!(innerAsyncResult is WebAsyncResult))
			{
				num = stream.EndRead(innerAsyncResult);
				webAsyncResult = (WebAsyncResult)result;
				flag2 = (num == 0);
			}
			if (this.chunkedRead)
			{
				try
				{
					this.chunkStream.WriteAndReadBack(webAsyncResult.Buffer, webAsyncResult.Offset, webAsyncResult.Size, ref num);
					if (!flag2 && num == 0 && this.chunkStream.WantMore)
					{
						num = this.EnsureRead(webAsyncResult.Buffer, webAsyncResult.Offset, webAsyncResult.Size);
					}
				}
				catch (Exception ex)
				{
					if (ex is WebException)
					{
						throw ex;
					}
					throw new WebException("Invalid chunked data.", ex, WebExceptionStatus.ServerProtocolViolation, null);
				}
				if ((flag2 || num == 0) && this.chunkStream.ChunkLeft != 0)
				{
					this.HandleError(WebExceptionStatus.ReceiveFailure, null, "chunked EndRead");
					throw new WebException("Read error", null, WebExceptionStatus.ReceiveFailure, null);
				}
			}
			if (num == 0)
			{
				return -1;
			}
			return num;
		}

		// Token: 0x0600203D RID: 8253 RVA: 0x00075938 File Offset: 0x00073B38
		private int EnsureRead(byte[] buffer, int offset, int size)
		{
			byte[] array = null;
			int num = 0;
			while (num == 0 && this.chunkStream.WantMore)
			{
				int num2 = this.chunkStream.ChunkLeft;
				if (num2 <= 0)
				{
					num2 = 1024;
				}
				else if (num2 > 16384)
				{
					num2 = 16384;
				}
				if (array == null || array.Length < num2)
				{
					array = new byte[num2];
				}
				int num3 = this.nstream.Read(array, 0, num2);
				if (num3 <= 0)
				{
					return 0;
				}
				this.chunkStream.Write(array, 0, num3);
				num += this.chunkStream.Read(buffer, offset + num, size - num);
			}
			return num;
		}

		// Token: 0x0600203E RID: 8254 RVA: 0x000759CC File Offset: 0x00073BCC
		private bool CompleteChunkedRead()
		{
			if (!this.chunkedRead || this.chunkStream == null)
			{
				return true;
			}
			while (this.chunkStream.WantMore)
			{
				int num = this.nstream.Read(this.buffer, 0, this.buffer.Length);
				if (num <= 0)
				{
					return false;
				}
				this.chunkStream.Write(this.buffer, 0, num);
			}
			return true;
		}

		// Token: 0x0600203F RID: 8255 RVA: 0x00075A30 File Offset: 0x00073C30
		internal IAsyncResult BeginWrite(HttpWebRequest request, byte[] buffer, int offset, int size, AsyncCallback cb, object state)
		{
			Stream stream = null;
			WebConnection obj = this;
			lock (obj)
			{
				if (this.Data.request != request)
				{
					throw new ObjectDisposedException(typeof(NetworkStream).FullName);
				}
				if (this.nstream == null)
				{
					return null;
				}
				stream = this.nstream;
			}
			IAsyncResult result = null;
			try
			{
				result = stream.BeginWrite(buffer, offset, size, cb, state);
			}
			catch (ObjectDisposedException)
			{
				obj = this;
				lock (obj)
				{
					if (this.Data.request != request)
					{
						return null;
					}
				}
				throw;
			}
			catch (IOException ex)
			{
				SocketException ex2 = ex.InnerException as SocketException;
				if (ex2 != null && ex2.SocketErrorCode == SocketError.NotConnected)
				{
					return null;
				}
				throw;
			}
			catch (Exception)
			{
				this.status = WebExceptionStatus.SendFailure;
				throw;
			}
			return result;
		}

		// Token: 0x06002040 RID: 8256 RVA: 0x00075B48 File Offset: 0x00073D48
		internal bool EndWrite(HttpWebRequest request, bool throwOnError, IAsyncResult result)
		{
			Stream stream = null;
			lock (this)
			{
				if (this.status == WebExceptionStatus.RequestCanceled)
				{
					return true;
				}
				if (this.Data.request != request)
				{
					throw new ObjectDisposedException(typeof(NetworkStream).FullName);
				}
				if (this.nstream == null)
				{
					throw new ObjectDisposedException(typeof(NetworkStream).FullName);
				}
				stream = this.nstream;
			}
			bool result2;
			try
			{
				stream.EndWrite(result);
				result2 = true;
			}
			catch (Exception ex)
			{
				this.status = WebExceptionStatus.SendFailure;
				if (throwOnError && ex.InnerException != null)
				{
					throw ex.InnerException;
				}
				result2 = false;
			}
			return result2;
		}

		// Token: 0x06002041 RID: 8257 RVA: 0x00075C10 File Offset: 0x00073E10
		internal int Read(HttpWebRequest request, byte[] buffer, int offset, int size)
		{
			Stream stream = null;
			lock (this)
			{
				if (this.Data.request != request)
				{
					throw new ObjectDisposedException(typeof(NetworkStream).FullName);
				}
				if (this.nstream == null)
				{
					return 0;
				}
				stream = this.nstream;
			}
			int num = 0;
			try
			{
				bool flag2 = false;
				if (!this.chunkedRead)
				{
					num = stream.Read(buffer, offset, size);
					flag2 = (num == 0);
				}
				if (this.chunkedRead)
				{
					try
					{
						this.chunkStream.WriteAndReadBack(buffer, offset, size, ref num);
						if (!flag2 && num == 0 && this.chunkStream.WantMore)
						{
							num = this.EnsureRead(buffer, offset, size);
						}
					}
					catch (Exception e)
					{
						this.HandleError(WebExceptionStatus.ReceiveFailure, e, "chunked Read1");
						throw;
					}
					if ((flag2 || num == 0) && this.chunkStream.WantMore)
					{
						this.HandleError(WebExceptionStatus.ReceiveFailure, null, "chunked Read2");
						throw new WebException("Read error", null, WebExceptionStatus.ReceiveFailure, null);
					}
				}
			}
			catch (Exception e2)
			{
				this.HandleError(WebExceptionStatus.ReceiveFailure, e2, "Read");
			}
			return num;
		}

		// Token: 0x06002042 RID: 8258 RVA: 0x00075D4C File Offset: 0x00073F4C
		internal bool Write(HttpWebRequest request, byte[] buffer, int offset, int size, ref string err_msg)
		{
			err_msg = null;
			Stream stream = null;
			lock (this)
			{
				if (this.Data.request != request)
				{
					throw new ObjectDisposedException(typeof(NetworkStream).FullName);
				}
				stream = this.nstream;
				if (stream == null)
				{
					return false;
				}
			}
			try
			{
				stream.Write(buffer, offset, size);
			}
			catch (Exception ex)
			{
				err_msg = ex.Message;
				WebExceptionStatus st = WebExceptionStatus.SendFailure;
				string where = "Write: " + err_msg;
				WebException ex2 = ex as WebException;
				this.HandleError(st, ex, where);
				return false;
			}
			return true;
		}

		// Token: 0x06002043 RID: 8259 RVA: 0x00075E0C File Offset: 0x0007400C
		internal void Close(bool sendNext)
		{
			lock (this)
			{
				if (this.Data != null && this.Data.request != null && this.Data.request.ReuseConnection)
				{
					this.Data.request.ReuseConnection = false;
				}
				else
				{
					if (this.nstream != null)
					{
						try
						{
							this.nstream.Close();
						}
						catch
						{
						}
						this.nstream = null;
					}
					if (this.socket != null)
					{
						try
						{
							this.socket.Close();
						}
						catch
						{
						}
						this.socket = null;
					}
					if (this.ntlm_authenticated)
					{
						this.ResetNtlm();
					}
					if (this.Data != null)
					{
						WebConnectionData data = this.Data;
						lock (data)
						{
							this.Data.ReadState = ReadState.Aborted;
						}
					}
					this.state.SetIdle();
					this.Data = new WebConnectionData();
					if (sendNext)
					{
						this.SendNext();
					}
					this.connect_request = null;
					this.connect_ntlm_auth_state = WebConnection.NtlmAuthState.None;
				}
			}
		}

		// Token: 0x06002044 RID: 8260 RVA: 0x00075F4C File Offset: 0x0007414C
		private void Abort(object sender, EventArgs args)
		{
			lock (this)
			{
				Queue obj = this.queue;
				lock (obj)
				{
					HttpWebRequest httpWebRequest = (HttpWebRequest)sender;
					if (this.Data.request == httpWebRequest || this.Data.request == null)
					{
						if (!httpWebRequest.FinishedReading)
						{
							this.status = WebExceptionStatus.RequestCanceled;
							this.Close(false);
							if (this.queue.Count > 0)
							{
								this.Data.request = (HttpWebRequest)this.queue.Dequeue();
								this.SendRequest(this.Data.request);
							}
						}
					}
					else
					{
						httpWebRequest.FinishedReading = true;
						httpWebRequest.SetResponseError(WebExceptionStatus.RequestCanceled, null, "User aborted");
						if (this.queue.Count > 0 && this.queue.Peek() == sender)
						{
							this.queue.Dequeue();
						}
						else if (this.queue.Count > 0)
						{
							object[] array = this.queue.ToArray();
							this.queue.Clear();
							for (int i = array.Length - 1; i >= 0; i--)
							{
								if (array[i] != sender)
								{
									this.queue.Enqueue(array[i]);
								}
							}
						}
					}
				}
			}
		}

		// Token: 0x06002045 RID: 8261 RVA: 0x000760D0 File Offset: 0x000742D0
		internal void ResetNtlm()
		{
			this.ntlm_authenticated = false;
			this.ntlm_credentials = null;
			this.unsafe_sharing = false;
		}

		// Token: 0x170006FE RID: 1790
		// (get) Token: 0x06002046 RID: 8262 RVA: 0x000760E8 File Offset: 0x000742E8
		internal bool Connected
		{
			get
			{
				bool result;
				lock (this)
				{
					result = (this.socket != null && this.socket.Connected);
				}
				return result;
			}
		}

		// Token: 0x170006FF RID: 1791
		// (set) Token: 0x06002047 RID: 8263 RVA: 0x00076138 File Offset: 0x00074338
		internal HttpWebRequest PriorityRequest
		{
			set
			{
				this.priority_request = value;
			}
		}

		// Token: 0x17000700 RID: 1792
		// (get) Token: 0x06002048 RID: 8264 RVA: 0x00076141 File Offset: 0x00074341
		// (set) Token: 0x06002049 RID: 8265 RVA: 0x00076149 File Offset: 0x00074349
		internal bool NtlmAuthenticated
		{
			get
			{
				return this.ntlm_authenticated;
			}
			set
			{
				this.ntlm_authenticated = value;
			}
		}

		// Token: 0x17000701 RID: 1793
		// (get) Token: 0x0600204A RID: 8266 RVA: 0x00076152 File Offset: 0x00074352
		// (set) Token: 0x0600204B RID: 8267 RVA: 0x0007615A File Offset: 0x0007435A
		internal NetworkCredential NtlmCredential
		{
			get
			{
				return this.ntlm_credentials;
			}
			set
			{
				this.ntlm_credentials = value;
			}
		}

		// Token: 0x17000702 RID: 1794
		// (get) Token: 0x0600204C RID: 8268 RVA: 0x00076163 File Offset: 0x00074363
		// (set) Token: 0x0600204D RID: 8269 RVA: 0x0007616B File Offset: 0x0007436B
		internal bool UnsafeAuthenticatedConnectionSharing
		{
			get
			{
				return this.unsafe_sharing;
			}
			set
			{
				this.unsafe_sharing = value;
			}
		}

		// Token: 0x0600204E RID: 8270 RVA: 0x00076174 File Offset: 0x00074374
		[CompilerGenerated]
		private void <SendRequest>b__41_0(object o)
		{
			try
			{
				this.InitConnection((HttpWebRequest)o);
			}
			catch
			{
			}
		}

		// Token: 0x04001B39 RID: 6969
		private ServicePoint sPoint;

		// Token: 0x04001B3A RID: 6970
		private Stream nstream;

		// Token: 0x04001B3B RID: 6971
		internal Socket socket;

		// Token: 0x04001B3C RID: 6972
		private object socketLock = new object();

		// Token: 0x04001B3D RID: 6973
		private IWebConnectionState state;

		// Token: 0x04001B3E RID: 6974
		private WebExceptionStatus status;

		// Token: 0x04001B3F RID: 6975
		private bool keepAlive;

		// Token: 0x04001B40 RID: 6976
		private byte[] buffer;

		// Token: 0x04001B41 RID: 6977
		private EventHandler abortHandler;

		// Token: 0x04001B42 RID: 6978
		private WebConnection.AbortHelper abortHelper;

		// Token: 0x04001B43 RID: 6979
		internal WebConnectionData Data;

		// Token: 0x04001B44 RID: 6980
		private bool chunkedRead;

		// Token: 0x04001B45 RID: 6981
		private MonoChunkStream chunkStream;

		// Token: 0x04001B46 RID: 6982
		private Queue queue;

		// Token: 0x04001B47 RID: 6983
		private bool reused;

		// Token: 0x04001B48 RID: 6984
		private int position;

		// Token: 0x04001B49 RID: 6985
		private HttpWebRequest priority_request;

		// Token: 0x04001B4A RID: 6986
		private NetworkCredential ntlm_credentials;

		// Token: 0x04001B4B RID: 6987
		private bool ntlm_authenticated;

		// Token: 0x04001B4C RID: 6988
		private bool unsafe_sharing;

		// Token: 0x04001B4D RID: 6989
		private WebConnection.NtlmAuthState connect_ntlm_auth_state;

		// Token: 0x04001B4E RID: 6990
		private HttpWebRequest connect_request;

		// Token: 0x04001B4F RID: 6991
		private Exception connect_exception;

		// Token: 0x04001B50 RID: 6992
		private MonoTlsStream tlsStream;

		// Token: 0x0200040B RID: 1035
		private enum NtlmAuthState
		{
			// Token: 0x04001B52 RID: 6994
			None,
			// Token: 0x04001B53 RID: 6995
			Challenge,
			// Token: 0x04001B54 RID: 6996
			Response
		}

		// Token: 0x0200040C RID: 1036
		private class AbortHelper
		{
			// Token: 0x0600204F RID: 8271 RVA: 0x000761A4 File Offset: 0x000743A4
			public void Abort(object sender, EventArgs args)
			{
				WebConnection webConnection = ((HttpWebRequest)sender).WebConnection;
				if (webConnection == null)
				{
					webConnection = this.Connection;
				}
				webConnection.Abort(sender, args);
			}

			// Token: 0x06002050 RID: 8272 RVA: 0x0000232F File Offset: 0x0000052F
			public AbortHelper()
			{
			}

			// Token: 0x04001B55 RID: 6997
			public WebConnection Connection;
		}
	}
}
