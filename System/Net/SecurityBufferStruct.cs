﻿using System;

namespace System.Net
{
	// Token: 0x0200030A RID: 778
	internal struct SecurityBufferStruct
	{
		// Token: 0x060016AB RID: 5803 RVA: 0x00051740 File Offset: 0x0004F940
		// Note: this type is marked as 'beforefieldinit'.
		static SecurityBufferStruct()
		{
		}

		// Token: 0x040015B2 RID: 5554
		public int count;

		// Token: 0x040015B3 RID: 5555
		public BufferType type;

		// Token: 0x040015B4 RID: 5556
		public IntPtr token;

		// Token: 0x040015B5 RID: 5557
		public static readonly int Size = sizeof(SecurityBufferStruct);
	}
}
