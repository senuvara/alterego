﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using Unity;

namespace System.Net
{
	/// <summary>Provides connection management for HTTP connections.</summary>
	// Token: 0x020003FF RID: 1023
	public class ServicePoint
	{
		// Token: 0x06001F84 RID: 8068 RVA: 0x00072544 File Offset: 0x00070744
		internal ServicePoint(Uri uri, int connectionLimit, int maxIdleTime)
		{
			this.sendContinue = true;
			this.hostE = new object();
			base..ctor();
			this.uri = uri;
			this.connectionLimit = connectionLimit;
			this.maxIdleTime = maxIdleTime;
			this.currentConnections = 0;
			this.idleSince = DateTime.UtcNow;
		}

		/// <summary>Gets the Uniform Resource Identifier (URI) of the server that this <see cref="T:System.Net.ServicePoint" /> object connects to.</summary>
		/// <returns>An instance of the <see cref="T:System.Uri" /> class that contains the URI of the Internet server that this <see cref="T:System.Net.ServicePoint" /> object connects to.</returns>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Net.ServicePoint" /> is in host mode.</exception>
		// Token: 0x170006C2 RID: 1730
		// (get) Token: 0x06001F85 RID: 8069 RVA: 0x00072590 File Offset: 0x00070790
		public Uri Address
		{
			get
			{
				return this.uri;
			}
		}

		// Token: 0x06001F86 RID: 8070 RVA: 0x000659A3 File Offset: 0x00063BA3
		private static Exception GetMustImplement()
		{
			return new NotImplementedException();
		}

		/// <summary>Specifies the delegate to associate a local <see cref="T:System.Net.IPEndPoint" /> with a <see cref="T:System.Net.ServicePoint" />.</summary>
		/// <returns>A delegate that forces a <see cref="T:System.Net.ServicePoint" /> to use a particular local Internet Protocol (IP) address and port number. The default value is <see langword="null" />.</returns>
		// Token: 0x170006C3 RID: 1731
		// (get) Token: 0x06001F87 RID: 8071 RVA: 0x00072598 File Offset: 0x00070798
		// (set) Token: 0x06001F88 RID: 8072 RVA: 0x000725A0 File Offset: 0x000707A0
		public BindIPEndPoint BindIPEndPointDelegate
		{
			get
			{
				return this.endPointCallback;
			}
			set
			{
				this.endPointCallback = value;
			}
		}

		/// <summary>Gets or sets the number of milliseconds after which an active <see cref="T:System.Net.ServicePoint" /> connection is closed.</summary>
		/// <returns>A <see cref="T:System.Int32" /> that specifies the number of milliseconds that an active <see cref="T:System.Net.ServicePoint" /> connection remains open. The default is -1, which allows an active <see cref="T:System.Net.ServicePoint" /> connection to stay connected indefinitely. Set this property to 0 to force <see cref="T:System.Net.ServicePoint" /> connections to close after servicing a request.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified for a set operation is a negative number less than -1.</exception>
		// Token: 0x170006C4 RID: 1732
		// (get) Token: 0x06001F89 RID: 8073 RVA: 0x000725A9 File Offset: 0x000707A9
		// (set) Token: 0x06001F8A RID: 8074 RVA: 0x000725A9 File Offset: 0x000707A9
		[MonoTODO]
		public int ConnectionLeaseTimeout
		{
			get
			{
				throw ServicePoint.GetMustImplement();
			}
			set
			{
				throw ServicePoint.GetMustImplement();
			}
		}

		/// <summary>Gets or sets the maximum number of connections allowed on this <see cref="T:System.Net.ServicePoint" /> object.</summary>
		/// <returns>The maximum number of connections allowed on this <see cref="T:System.Net.ServicePoint" /> object.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The connection limit is equal to or less than 0. </exception>
		// Token: 0x170006C5 RID: 1733
		// (get) Token: 0x06001F8B RID: 8075 RVA: 0x000725B0 File Offset: 0x000707B0
		// (set) Token: 0x06001F8C RID: 8076 RVA: 0x000725B8 File Offset: 0x000707B8
		public int ConnectionLimit
		{
			get
			{
				return this.connectionLimit;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				this.connectionLimit = value;
			}
		}

		/// <summary>Gets the connection name. </summary>
		/// <returns>A <see cref="T:System.String" /> that represents the connection name. </returns>
		// Token: 0x170006C6 RID: 1734
		// (get) Token: 0x06001F8D RID: 8077 RVA: 0x000725CB File Offset: 0x000707CB
		public string ConnectionName
		{
			get
			{
				return this.uri.Scheme;
			}
		}

		/// <summary>Gets the number of open connections associated with this <see cref="T:System.Net.ServicePoint" /> object.</summary>
		/// <returns>The number of open connections associated with this <see cref="T:System.Net.ServicePoint" /> object.</returns>
		// Token: 0x170006C7 RID: 1735
		// (get) Token: 0x06001F8E RID: 8078 RVA: 0x000725D8 File Offset: 0x000707D8
		public int CurrentConnections
		{
			get
			{
				return this.currentConnections;
			}
		}

		/// <summary>Gets the date and time that the <see cref="T:System.Net.ServicePoint" /> object was last connected to a host.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> object that contains the date and time at which the <see cref="T:System.Net.ServicePoint" /> object was last connected.</returns>
		// Token: 0x170006C8 RID: 1736
		// (get) Token: 0x06001F8F RID: 8079 RVA: 0x000725E0 File Offset: 0x000707E0
		public DateTime IdleSince
		{
			get
			{
				return this.idleSince.ToLocalTime();
			}
		}

		/// <summary>Gets or sets the amount of time a connection associated with the <see cref="T:System.Net.ServicePoint" /> object can remain idle before the connection is closed.</summary>
		/// <returns>The length of time, in milliseconds, that a connection associated with the <see cref="T:System.Net.ServicePoint" /> object can remain idle before it is closed and reused for another connection.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <see cref="P:System.Net.ServicePoint.MaxIdleTime" /> is set to less than <see cref="F:System.Threading.Timeout.Infinite" /> or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		// Token: 0x170006C9 RID: 1737
		// (get) Token: 0x06001F90 RID: 8080 RVA: 0x000725ED File Offset: 0x000707ED
		// (set) Token: 0x06001F91 RID: 8081 RVA: 0x000725F8 File Offset: 0x000707F8
		public int MaxIdleTime
		{
			get
			{
				return this.maxIdleTime;
			}
			set
			{
				if (value < -1 || value > 2147483647)
				{
					throw new ArgumentOutOfRangeException();
				}
				lock (this)
				{
					this.maxIdleTime = value;
					if (this.idleTimer != null)
					{
						this.idleTimer.Change(this.maxIdleTime, this.maxIdleTime);
					}
				}
			}
		}

		/// <summary>Gets the version of the HTTP protocol that the <see cref="T:System.Net.ServicePoint" /> object uses.</summary>
		/// <returns>A <see cref="T:System.Version" /> object that contains the HTTP protocol version that the <see cref="T:System.Net.ServicePoint" /> object uses.</returns>
		// Token: 0x170006CA RID: 1738
		// (get) Token: 0x06001F92 RID: 8082 RVA: 0x00072668 File Offset: 0x00070868
		public virtual Version ProtocolVersion
		{
			get
			{
				return this.protocolVersion;
			}
		}

		/// <summary>Gets or sets the size of the receiving buffer for the socket used by this <see cref="T:System.Net.ServicePoint" />.</summary>
		/// <returns>A <see cref="T:System.Int32" /> that contains the size, in bytes, of the receive buffer. The default is 8192.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified for a set operation is greater than <see cref="F:System.Int32.MaxValue" />.</exception>
		// Token: 0x170006CB RID: 1739
		// (get) Token: 0x06001F93 RID: 8083 RVA: 0x000725A9 File Offset: 0x000707A9
		// (set) Token: 0x06001F94 RID: 8084 RVA: 0x000725A9 File Offset: 0x000707A9
		[MonoTODO]
		public int ReceiveBufferSize
		{
			get
			{
				throw ServicePoint.GetMustImplement();
			}
			set
			{
				throw ServicePoint.GetMustImplement();
			}
		}

		/// <summary>Indicates whether the <see cref="T:System.Net.ServicePoint" /> object supports pipelined connections.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Net.ServicePoint" /> object supports pipelined connections; otherwise, <see langword="false" />.</returns>
		// Token: 0x170006CC RID: 1740
		// (get) Token: 0x06001F95 RID: 8085 RVA: 0x00072670 File Offset: 0x00070870
		public bool SupportsPipelining
		{
			get
			{
				return HttpVersion.Version11.Equals(this.protocolVersion);
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that determines whether 100-Continue behavior is used.</summary>
		/// <returns>
		///     <see langword="true" /> to expect 100-Continue responses for <see langword="POST" /> requests; otherwise, <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x170006CD RID: 1741
		// (get) Token: 0x06001F96 RID: 8086 RVA: 0x00072682 File Offset: 0x00070882
		// (set) Token: 0x06001F97 RID: 8087 RVA: 0x0007268A File Offset: 0x0007088A
		public bool Expect100Continue
		{
			get
			{
				return this.SendContinue;
			}
			set
			{
				this.SendContinue = value;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that determines whether the Nagle algorithm is used on connections managed by this <see cref="T:System.Net.ServicePoint" /> object.</summary>
		/// <returns>
		///     <see langword="true" /> to use the Nagle algorithm; otherwise, <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x170006CE RID: 1742
		// (get) Token: 0x06001F98 RID: 8088 RVA: 0x00072693 File Offset: 0x00070893
		// (set) Token: 0x06001F99 RID: 8089 RVA: 0x0007269B File Offset: 0x0007089B
		public bool UseNagleAlgorithm
		{
			get
			{
				return this.useNagle;
			}
			set
			{
				this.useNagle = value;
			}
		}

		// Token: 0x170006CF RID: 1743
		// (get) Token: 0x06001F9A RID: 8090 RVA: 0x000726A4 File Offset: 0x000708A4
		// (set) Token: 0x06001F9B RID: 8091 RVA: 0x000726D0 File Offset: 0x000708D0
		internal bool SendContinue
		{
			get
			{
				return this.sendContinue && (this.protocolVersion == null || this.protocolVersion == HttpVersion.Version11);
			}
			set
			{
				this.sendContinue = value;
			}
		}

		/// <summary>Enables or disables the keep-alive option on a TCP connection.</summary>
		/// <param name="enabled">If set to true, then the TCP keep-alive option on a TCP connection will be enabled using the specified <paramref name="keepAliveTime " />and <paramref name="keepAliveInterval" /> values. If set to false, then the TCP keep-alive option is disabled and the remaining parameters are ignored.The default value is false.</param>
		/// <param name="keepAliveTime">Specifies the timeout, in milliseconds, with no activity until the first keep-alive packet is sent. The value must be greater than 0.  If a value of less than or equal to zero is passed an <see cref="T:System.ArgumentOutOfRangeException" /> is thrown.</param>
		/// <param name="keepAliveInterval">Specifies the interval, in milliseconds, between when successive keep-alive packets are sent if no acknowledgement is received.The value must be greater than 0.  If a value of less than or equal to zero is passed an <see cref="T:System.ArgumentOutOfRangeException" /> is thrown.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified for <paramref name="keepAliveTime" /> or <paramref name="keepAliveInterval" /> parameter is less than or equal to 0.</exception>
		// Token: 0x06001F9C RID: 8092 RVA: 0x000726DC File Offset: 0x000708DC
		public void SetTcpKeepAlive(bool enabled, int keepAliveTime, int keepAliveInterval)
		{
			if (enabled)
			{
				if (keepAliveTime <= 0)
				{
					throw new ArgumentOutOfRangeException("keepAliveTime", "Must be greater than 0");
				}
				if (keepAliveInterval <= 0)
				{
					throw new ArgumentOutOfRangeException("keepAliveInterval", "Must be greater than 0");
				}
			}
			this.tcp_keepalive = enabled;
			this.tcp_keepalive_time = keepAliveTime;
			this.tcp_keepalive_interval = keepAliveInterval;
		}

		// Token: 0x06001F9D RID: 8093 RVA: 0x0007272C File Offset: 0x0007092C
		internal void KeepAliveSetup(Socket socket)
		{
			if (!this.tcp_keepalive)
			{
				return;
			}
			byte[] array = new byte[12];
			ServicePoint.PutBytes(array, this.tcp_keepalive ? 1U : 0U, 0);
			ServicePoint.PutBytes(array, (uint)this.tcp_keepalive_time, 4);
			ServicePoint.PutBytes(array, (uint)this.tcp_keepalive_interval, 8);
			socket.IOControl((IOControlCode)((ulong)-1744830460), array, null);
		}

		// Token: 0x06001F9E RID: 8094 RVA: 0x00072788 File Offset: 0x00070988
		private static void PutBytes(byte[] bytes, uint v, int offset)
		{
			if (BitConverter.IsLittleEndian)
			{
				bytes[offset] = (byte)(v & 255U);
				bytes[offset + 1] = (byte)((v & 65280U) >> 8);
				bytes[offset + 2] = (byte)((v & 16711680U) >> 16);
				bytes[offset + 3] = (byte)((v & 4278190080U) >> 24);
				return;
			}
			bytes[offset + 3] = (byte)(v & 255U);
			bytes[offset + 2] = (byte)((v & 65280U) >> 8);
			bytes[offset + 1] = (byte)((v & 16711680U) >> 16);
			bytes[offset] = (byte)((v & 4278190080U) >> 24);
		}

		// Token: 0x170006D0 RID: 1744
		// (get) Token: 0x06001F9F RID: 8095 RVA: 0x00072811 File Offset: 0x00070A11
		// (set) Token: 0x06001FA0 RID: 8096 RVA: 0x00072819 File Offset: 0x00070A19
		internal bool UsesProxy
		{
			get
			{
				return this.usesProxy;
			}
			set
			{
				this.usesProxy = value;
			}
		}

		// Token: 0x170006D1 RID: 1745
		// (get) Token: 0x06001FA1 RID: 8097 RVA: 0x00072822 File Offset: 0x00070A22
		// (set) Token: 0x06001FA2 RID: 8098 RVA: 0x0007282A File Offset: 0x00070A2A
		internal bool UseConnect
		{
			get
			{
				return this.useConnect;
			}
			set
			{
				this.useConnect = value;
			}
		}

		// Token: 0x06001FA3 RID: 8099 RVA: 0x00072834 File Offset: 0x00070A34
		private WebConnectionGroup GetConnectionGroup(string name)
		{
			if (name == null)
			{
				name = "";
			}
			WebConnectionGroup webConnectionGroup;
			if (this.groups != null && this.groups.TryGetValue(name, out webConnectionGroup))
			{
				return webConnectionGroup;
			}
			webConnectionGroup = new WebConnectionGroup(this, name);
			webConnectionGroup.ConnectionClosed += delegate(object s, EventArgs e)
			{
				this.currentConnections--;
			};
			if (this.groups == null)
			{
				this.groups = new Dictionary<string, WebConnectionGroup>();
			}
			this.groups.Add(name, webConnectionGroup);
			return webConnectionGroup;
		}

		// Token: 0x06001FA4 RID: 8100 RVA: 0x000728A0 File Offset: 0x00070AA0
		private void RemoveConnectionGroup(WebConnectionGroup group)
		{
			if (this.groups == null || this.groups.Count == 0)
			{
				throw new InvalidOperationException();
			}
			this.groups.Remove(group.Name);
		}

		// Token: 0x06001FA5 RID: 8101 RVA: 0x000728D0 File Offset: 0x00070AD0
		private bool CheckAvailableForRecycling(out DateTime outIdleSince)
		{
			outIdleSince = DateTime.MinValue;
			List<WebConnectionGroup> list = null;
			List<WebConnectionGroup> list2 = null;
			ServicePoint obj = this;
			TimeSpan timeSpan;
			lock (obj)
			{
				if (this.groups == null || this.groups.Count == 0)
				{
					this.idleSince = DateTime.MinValue;
					return true;
				}
				timeSpan = TimeSpan.FromMilliseconds((double)this.maxIdleTime);
				list = new List<WebConnectionGroup>(this.groups.Values);
			}
			foreach (WebConnectionGroup webConnectionGroup in list)
			{
				if (webConnectionGroup.TryRecycle(timeSpan, ref outIdleSince))
				{
					if (list2 == null)
					{
						list2 = new List<WebConnectionGroup>();
					}
					list2.Add(webConnectionGroup);
				}
			}
			obj = this;
			bool result;
			lock (obj)
			{
				this.idleSince = outIdleSince;
				if (list2 != null && this.groups != null)
				{
					foreach (WebConnectionGroup webConnectionGroup2 in list2)
					{
						if (this.groups.ContainsKey(webConnectionGroup2.Name))
						{
							this.RemoveConnectionGroup(webConnectionGroup2);
						}
					}
				}
				if (this.groups != null && this.groups.Count == 0)
				{
					this.groups = null;
				}
				if (this.groups == null)
				{
					if (this.idleTimer != null)
					{
						this.idleTimer.Dispose();
						this.idleTimer = null;
					}
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06001FA6 RID: 8102 RVA: 0x00072A8C File Offset: 0x00070C8C
		private void IdleTimerCallback(object obj)
		{
			DateTime dateTime;
			this.CheckAvailableForRecycling(out dateTime);
		}

		// Token: 0x170006D2 RID: 1746
		// (get) Token: 0x06001FA7 RID: 8103 RVA: 0x00072AA4 File Offset: 0x00070CA4
		private bool HasTimedOut
		{
			get
			{
				int dnsRefreshTimeout = ServicePointManager.DnsRefreshTimeout;
				return dnsRefreshTimeout != -1 && this.lastDnsResolve + TimeSpan.FromMilliseconds((double)dnsRefreshTimeout) < DateTime.UtcNow;
			}
		}

		// Token: 0x170006D3 RID: 1747
		// (get) Token: 0x06001FA8 RID: 8104 RVA: 0x00072ADC File Offset: 0x00070CDC
		internal IPHostEntry HostEntry
		{
			get
			{
				object obj = this.hostE;
				lock (obj)
				{
					string text = this.uri.Host;
					if (this.uri.HostNameType == UriHostNameType.IPv6 || this.uri.HostNameType == UriHostNameType.IPv4)
					{
						if (this.host != null)
						{
							return this.host;
						}
						if (this.uri.HostNameType == UriHostNameType.IPv6)
						{
							text = text.Substring(1, text.Length - 2);
						}
						this.host = new IPHostEntry();
						this.host.AddressList = new IPAddress[]
						{
							IPAddress.Parse(text)
						};
						return this.host;
					}
					else
					{
						if (!this.HasTimedOut && this.host != null)
						{
							return this.host;
						}
						this.lastDnsResolve = DateTime.UtcNow;
						try
						{
							this.host = Dns.GetHostEntry(text);
						}
						catch
						{
							return null;
						}
					}
				}
				return this.host;
			}
		}

		// Token: 0x06001FA9 RID: 8105 RVA: 0x00072BE8 File Offset: 0x00070DE8
		internal void SetVersion(Version version)
		{
			this.protocolVersion = version;
		}

		// Token: 0x06001FAA RID: 8106 RVA: 0x00072BF4 File Offset: 0x00070DF4
		internal EventHandler SendRequest(HttpWebRequest request, string groupName)
		{
			WebConnection connection;
			lock (this)
			{
				bool flag2;
				connection = this.GetConnectionGroup(groupName).GetConnection(request, out flag2);
				if (flag2)
				{
					this.currentConnections++;
					if (this.idleTimer == null)
					{
						this.idleTimer = new Timer(new TimerCallback(this.IdleTimerCallback), null, this.maxIdleTime, this.maxIdleTime);
					}
				}
			}
			return connection.SendRequest(request);
		}

		/// <summary>Removes the specified connection group from this <see cref="T:System.Net.ServicePoint" /> object.</summary>
		/// <param name="connectionGroupName">The name of the connection group that contains the connections to close and remove from this service point. </param>
		/// <returns>A <see cref="T:System.Boolean" /> value that indicates whether the connection group was closed.</returns>
		// Token: 0x06001FAB RID: 8107 RVA: 0x00072C80 File Offset: 0x00070E80
		public bool CloseConnectionGroup(string connectionGroupName)
		{
			WebConnectionGroup webConnectionGroup = null;
			lock (this)
			{
				webConnectionGroup = this.GetConnectionGroup(connectionGroupName);
				if (webConnectionGroup != null)
				{
					this.RemoveConnectionGroup(webConnectionGroup);
				}
			}
			if (webConnectionGroup != null)
			{
				webConnectionGroup.Close();
				return true;
			}
			return false;
		}

		/// <summary>Gets the certificate received for this <see cref="T:System.Net.ServicePoint" /> object.</summary>
		/// <returns>An instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class that contains the security certificate received for this <see cref="T:System.Net.ServicePoint" /> object.</returns>
		// Token: 0x170006D4 RID: 1748
		// (get) Token: 0x06001FAC RID: 8108 RVA: 0x00072CD8 File Offset: 0x00070ED8
		public X509Certificate Certificate
		{
			get
			{
				object serverCertificateOrBytes = this.m_ServerCertificateOrBytes;
				if (serverCertificateOrBytes != null && serverCertificateOrBytes.GetType() == typeof(byte[]))
				{
					return (X509Certificate)(this.m_ServerCertificateOrBytes = new X509Certificate((byte[])serverCertificateOrBytes));
				}
				return serverCertificateOrBytes as X509Certificate;
			}
		}

		// Token: 0x06001FAD RID: 8109 RVA: 0x00072D26 File Offset: 0x00070F26
		internal void UpdateServerCertificate(X509Certificate certificate)
		{
			if (certificate != null)
			{
				this.m_ServerCertificateOrBytes = certificate.GetRawCertData();
				return;
			}
			this.m_ServerCertificateOrBytes = null;
		}

		/// <summary>Gets the last client certificate sent to the server.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object that contains the public values of the last client certificate sent to the server.</returns>
		// Token: 0x170006D5 RID: 1749
		// (get) Token: 0x06001FAE RID: 8110 RVA: 0x00072D40 File Offset: 0x00070F40
		public X509Certificate ClientCertificate
		{
			get
			{
				object clientCertificateOrBytes = this.m_ClientCertificateOrBytes;
				if (clientCertificateOrBytes != null && clientCertificateOrBytes.GetType() == typeof(byte[]))
				{
					return (X509Certificate)(this.m_ClientCertificateOrBytes = new X509Certificate((byte[])clientCertificateOrBytes));
				}
				return clientCertificateOrBytes as X509Certificate;
			}
		}

		// Token: 0x06001FAF RID: 8111 RVA: 0x00072D8E File Offset: 0x00070F8E
		internal void UpdateClientCertificate(X509Certificate certificate)
		{
			if (certificate != null)
			{
				this.m_ClientCertificateOrBytes = certificate.GetRawCertData();
				return;
			}
			this.m_ClientCertificateOrBytes = null;
		}

		// Token: 0x06001FB0 RID: 8112 RVA: 0x00072DA8 File Offset: 0x00070FA8
		internal bool CallEndPointDelegate(Socket sock, IPEndPoint remote)
		{
			if (this.endPointCallback == null)
			{
				return true;
			}
			int num = 0;
			checked
			{
				for (;;)
				{
					IPEndPoint ipendPoint = null;
					try
					{
						ipendPoint = this.endPointCallback(this, remote, num);
					}
					catch
					{
						return false;
					}
					if (ipendPoint == null)
					{
						break;
					}
					try
					{
						sock.Bind(ipendPoint);
					}
					catch (SocketException)
					{
						num++;
						continue;
					}
					return true;
				}
				return true;
			}
		}

		// Token: 0x06001FB1 RID: 8113 RVA: 0x000068D7 File Offset: 0x00004AD7
		internal Socket GetConnection(PooledStream PooledStream, object owner, bool async, out IPAddress address, ref Socket abortSocket, ref Socket abortSocket6)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001FB2 RID: 8114 RVA: 0x00072E10 File Offset: 0x00071010
		[CompilerGenerated]
		private void <GetConnectionGroup>b__66_0(object s, EventArgs e)
		{
			this.currentConnections--;
		}

		// Token: 0x06001FB3 RID: 8115 RVA: 0x000092E2 File Offset: 0x000074E2
		internal ServicePoint()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001AEB RID: 6891
		private readonly Uri uri;

		// Token: 0x04001AEC RID: 6892
		private int connectionLimit;

		// Token: 0x04001AED RID: 6893
		private int maxIdleTime;

		// Token: 0x04001AEE RID: 6894
		private int currentConnections;

		// Token: 0x04001AEF RID: 6895
		private DateTime idleSince;

		// Token: 0x04001AF0 RID: 6896
		private DateTime lastDnsResolve;

		// Token: 0x04001AF1 RID: 6897
		private Version protocolVersion;

		// Token: 0x04001AF2 RID: 6898
		private IPHostEntry host;

		// Token: 0x04001AF3 RID: 6899
		private bool usesProxy;

		// Token: 0x04001AF4 RID: 6900
		private Dictionary<string, WebConnectionGroup> groups;

		// Token: 0x04001AF5 RID: 6901
		private bool sendContinue;

		// Token: 0x04001AF6 RID: 6902
		private bool useConnect;

		// Token: 0x04001AF7 RID: 6903
		private object hostE;

		// Token: 0x04001AF8 RID: 6904
		private bool useNagle;

		// Token: 0x04001AF9 RID: 6905
		private BindIPEndPoint endPointCallback;

		// Token: 0x04001AFA RID: 6906
		private bool tcp_keepalive;

		// Token: 0x04001AFB RID: 6907
		private int tcp_keepalive_time;

		// Token: 0x04001AFC RID: 6908
		private int tcp_keepalive_interval;

		// Token: 0x04001AFD RID: 6909
		private Timer idleTimer;

		// Token: 0x04001AFE RID: 6910
		private object m_ServerCertificateOrBytes;

		// Token: 0x04001AFF RID: 6911
		private object m_ClientCertificateOrBytes;
	}
}
