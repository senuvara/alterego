﻿using System;
using System.Collections;
using System.Threading;

namespace System.Net
{
	// Token: 0x0200036E RID: 878
	internal class ConnectionPool
	{
		// Token: 0x1700052C RID: 1324
		// (get) Token: 0x0600191E RID: 6430 RVA: 0x0005A2BD File Offset: 0x000584BD
		private Mutex CreationMutex
		{
			get
			{
				return (Mutex)this.m_WaitHandles[2];
			}
		}

		// Token: 0x1700052D RID: 1325
		// (get) Token: 0x0600191F RID: 6431 RVA: 0x0005A2CC File Offset: 0x000584CC
		private ManualResetEvent ErrorEvent
		{
			get
			{
				return (ManualResetEvent)this.m_WaitHandles[1];
			}
		}

		// Token: 0x1700052E RID: 1326
		// (get) Token: 0x06001920 RID: 6432 RVA: 0x0005A2DB File Offset: 0x000584DB
		private Semaphore Semaphore
		{
			get
			{
				return (Semaphore)this.m_WaitHandles[0];
			}
		}

		// Token: 0x06001921 RID: 6433 RVA: 0x0005A2EC File Offset: 0x000584EC
		internal ConnectionPool(ServicePoint servicePoint, int maxPoolSize, int minPoolSize, int idleTimeout, CreateConnectionDelegate createConnectionCallback)
		{
			this.m_State = ConnectionPool.State.Initializing;
			this.m_CreateConnectionCallback = createConnectionCallback;
			this.m_MaxPoolSize = maxPoolSize;
			this.m_MinPoolSize = minPoolSize;
			this.m_ServicePoint = servicePoint;
			this.Initialize();
			if (idleTimeout > 0)
			{
				this.m_CleanupQueue = TimerThread.GetOrCreateQueue((idleTimeout == 1) ? 1 : (idleTimeout / 2));
				this.m_CleanupQueue.CreateTimer(ConnectionPool.s_CleanupCallback, this);
			}
		}

		// Token: 0x06001922 RID: 6434 RVA: 0x0005A358 File Offset: 0x00058558
		private void Initialize()
		{
			this.m_StackOld = new InterlockedStack();
			this.m_StackNew = new InterlockedStack();
			this.m_QueuedRequests = new Queue();
			this.m_WaitHandles = new WaitHandle[3];
			this.m_WaitHandles[0] = new Semaphore(0, 1048576);
			this.m_WaitHandles[1] = new ManualResetEvent(false);
			this.m_WaitHandles[2] = new Mutex();
			this.m_ErrorTimer = null;
			this.m_ObjectList = new ArrayList();
			this.m_State = ConnectionPool.State.Running;
		}

		// Token: 0x06001923 RID: 6435 RVA: 0x0005A3DC File Offset: 0x000585DC
		private void QueueRequest(ConnectionPool.AsyncConnectionPoolRequest asyncRequest)
		{
			Queue queuedRequests = this.m_QueuedRequests;
			lock (queuedRequests)
			{
				this.m_QueuedRequests.Enqueue(asyncRequest);
				if (this.m_AsyncThread == null)
				{
					this.m_AsyncThread = new Thread(new ThreadStart(this.AsyncThread));
					this.m_AsyncThread.IsBackground = true;
					this.m_AsyncThread.Start();
				}
			}
		}

		// Token: 0x06001924 RID: 6436 RVA: 0x0005A458 File Offset: 0x00058658
		private void AsyncThread()
		{
			for (;;)
			{
				Queue queuedRequests;
				if (this.m_QueuedRequests.Count <= 0)
				{
					Thread.Sleep(500);
					queuedRequests = this.m_QueuedRequests;
					lock (queuedRequests)
					{
						if (this.m_QueuedRequests.Count != 0)
						{
							continue;
						}
						this.m_AsyncThread = null;
					}
					break;
				}
				bool flag2 = true;
				ConnectionPool.AsyncConnectionPoolRequest asyncConnectionPoolRequest = null;
				queuedRequests = this.m_QueuedRequests;
				lock (queuedRequests)
				{
					asyncConnectionPoolRequest = (ConnectionPool.AsyncConnectionPoolRequest)this.m_QueuedRequests.Dequeue();
				}
				WaitHandle[] waitHandles = this.m_WaitHandles;
				PooledStream pooledStream = null;
				try
				{
					while (pooledStream == null && flag2)
					{
						int result = WaitHandle.WaitAny(waitHandles, asyncConnectionPoolRequest.CreationTimeout, false);
						pooledStream = this.Get(asyncConnectionPoolRequest.OwningObject, result, ref flag2, ref waitHandles);
					}
					pooledStream.Activate(asyncConnectionPoolRequest.OwningObject, asyncConnectionPoolRequest.AsyncCallback);
				}
				catch (Exception state)
				{
					if (pooledStream != null)
					{
						this.PutConnection(pooledStream, asyncConnectionPoolRequest.OwningObject, asyncConnectionPoolRequest.CreationTimeout, false);
					}
					asyncConnectionPoolRequest.AsyncCallback(asyncConnectionPoolRequest.OwningObject, state);
				}
			}
		}

		// Token: 0x1700052F RID: 1327
		// (get) Token: 0x06001925 RID: 6437 RVA: 0x0005A594 File Offset: 0x00058794
		internal int Count
		{
			get
			{
				return this.m_TotalObjects;
			}
		}

		// Token: 0x17000530 RID: 1328
		// (get) Token: 0x06001926 RID: 6438 RVA: 0x0005A59C File Offset: 0x0005879C
		internal ServicePoint ServicePoint
		{
			get
			{
				return this.m_ServicePoint;
			}
		}

		// Token: 0x17000531 RID: 1329
		// (get) Token: 0x06001927 RID: 6439 RVA: 0x0005A5A4 File Offset: 0x000587A4
		internal int MaxPoolSize
		{
			get
			{
				return this.m_MaxPoolSize;
			}
		}

		// Token: 0x17000532 RID: 1330
		// (get) Token: 0x06001928 RID: 6440 RVA: 0x0005A5AC File Offset: 0x000587AC
		internal int MinPoolSize
		{
			get
			{
				return this.m_MinPoolSize;
			}
		}

		// Token: 0x17000533 RID: 1331
		// (get) Token: 0x06001929 RID: 6441 RVA: 0x0005A5B4 File Offset: 0x000587B4
		private bool ErrorOccurred
		{
			get
			{
				return this.m_ErrorOccured;
			}
		}

		// Token: 0x0600192A RID: 6442 RVA: 0x0005A5C0 File Offset: 0x000587C0
		private static void CleanupCallbackWrapper(TimerThread.Timer timer, int timeNoticed, object context)
		{
			ConnectionPool connectionPool = (ConnectionPool)context;
			try
			{
				connectionPool.CleanupCallback();
			}
			finally
			{
				connectionPool.m_CleanupQueue.CreateTimer(ConnectionPool.s_CleanupCallback, context);
			}
		}

		// Token: 0x0600192B RID: 6443 RVA: 0x0005A600 File Offset: 0x00058800
		internal void ForceCleanup()
		{
			if (Logging.On)
			{
			}
			while (this.Count > 0 && this.Semaphore.WaitOne(0, false))
			{
				PooledStream pooledStream = (PooledStream)this.m_StackNew.Pop();
				if (pooledStream == null)
				{
					pooledStream = (PooledStream)this.m_StackOld.Pop();
				}
				this.Destroy(pooledStream);
			}
			bool on = Logging.On;
		}

		// Token: 0x0600192C RID: 6444 RVA: 0x0005A660 File Offset: 0x00058860
		private void CleanupCallback()
		{
			while (this.Count > this.MinPoolSize && this.Semaphore.WaitOne(0, false))
			{
				PooledStream pooledStream = (PooledStream)this.m_StackOld.Pop();
				if (pooledStream == null)
				{
					this.Semaphore.ReleaseSemaphore();
					break;
				}
				this.Destroy(pooledStream);
			}
			if (this.Semaphore.WaitOne(0, false))
			{
				for (;;)
				{
					PooledStream pooledStream2 = (PooledStream)this.m_StackNew.Pop();
					if (pooledStream2 == null)
					{
						break;
					}
					this.m_StackOld.Push(pooledStream2);
				}
				this.Semaphore.ReleaseSemaphore();
			}
		}

		// Token: 0x0600192D RID: 6445 RVA: 0x0005A6F4 File Offset: 0x000588F4
		private PooledStream Create(CreateConnectionDelegate createConnectionCallback)
		{
			PooledStream pooledStream = null;
			try
			{
				pooledStream = createConnectionCallback(this);
				if (pooledStream == null)
				{
					throw new InternalException();
				}
				if (!pooledStream.CanBePooled)
				{
					throw new InternalException();
				}
				pooledStream.PrePush(null);
				object syncRoot = this.m_ObjectList.SyncRoot;
				lock (syncRoot)
				{
					this.m_ObjectList.Add(pooledStream);
					this.m_TotalObjects = this.m_ObjectList.Count;
				}
			}
			catch (Exception resError)
			{
				pooledStream = null;
				this.m_ResError = resError;
				this.Abort();
			}
			return pooledStream;
		}

		// Token: 0x0600192E RID: 6446 RVA: 0x0005A79C File Offset: 0x0005899C
		private void Destroy(PooledStream pooledStream)
		{
			if (pooledStream != null)
			{
				try
				{
					object syncRoot = this.m_ObjectList.SyncRoot;
					lock (syncRoot)
					{
						this.m_ObjectList.Remove(pooledStream);
						this.m_TotalObjects = this.m_ObjectList.Count;
					}
				}
				finally
				{
					pooledStream.Dispose();
				}
			}
		}

		// Token: 0x0600192F RID: 6447 RVA: 0x0005A810 File Offset: 0x00058A10
		private static void CancelErrorCallbackWrapper(TimerThread.Timer timer, int timeNoticed, object context)
		{
			((ConnectionPool)context).CancelErrorCallback();
		}

		// Token: 0x06001930 RID: 6448 RVA: 0x0005A820 File Offset: 0x00058A20
		private void CancelErrorCallback()
		{
			TimerThread.Timer errorTimer = this.m_ErrorTimer;
			if (errorTimer != null && errorTimer.Cancel())
			{
				this.m_ErrorOccured = false;
				this.ErrorEvent.Reset();
				this.m_ErrorTimer = null;
				this.m_ResError = null;
			}
		}

		// Token: 0x06001931 RID: 6449 RVA: 0x0005A864 File Offset: 0x00058A64
		private PooledStream GetFromPool(object owningObject)
		{
			PooledStream pooledStream = (PooledStream)this.m_StackNew.Pop();
			if (pooledStream == null)
			{
				pooledStream = (PooledStream)this.m_StackOld.Pop();
			}
			if (pooledStream != null)
			{
				pooledStream.PostPop(owningObject);
			}
			return pooledStream;
		}

		// Token: 0x06001932 RID: 6450 RVA: 0x0005A8A4 File Offset: 0x00058AA4
		private PooledStream Get(object owningObject, int result, ref bool continueLoop, ref WaitHandle[] waitHandles)
		{
			PooledStream pooledStream = null;
			if (result != 1)
			{
				if (result != 2)
				{
					if (result == 258)
					{
						Interlocked.Decrement(ref this.m_WaitCount);
						continueLoop = false;
						throw new WebException(NetRes.GetWebStatusString("net_timeout", WebExceptionStatus.ConnectFailure), WebExceptionStatus.Timeout);
					}
				}
				else
				{
					try
					{
						continueLoop = true;
						pooledStream = this.UserCreateRequest();
						if (pooledStream != null)
						{
							pooledStream.PostPop(owningObject);
							Interlocked.Decrement(ref this.m_WaitCount);
							continueLoop = false;
							return pooledStream;
						}
						if (this.Count >= this.MaxPoolSize && this.MaxPoolSize != 0 && !this.ReclaimEmancipatedObjects())
						{
							waitHandles = new WaitHandle[2];
							waitHandles[0] = this.m_WaitHandles[0];
							waitHandles[1] = this.m_WaitHandles[1];
						}
						return pooledStream;
					}
					finally
					{
						this.CreationMutex.ReleaseMutex();
					}
				}
				Interlocked.Decrement(ref this.m_WaitCount);
				pooledStream = this.GetFromPool(owningObject);
				continueLoop = false;
				return pooledStream;
			}
			bool flag = Interlocked.Decrement(ref this.m_WaitCount) != 0;
			continueLoop = false;
			Exception resError = this.m_ResError;
			if (!flag)
			{
				this.CancelErrorCallback();
			}
			throw resError;
		}

		// Token: 0x06001933 RID: 6451 RVA: 0x0005A9A4 File Offset: 0x00058BA4
		internal void Abort()
		{
			if (this.m_ResError == null)
			{
				this.m_ResError = new WebException(NetRes.GetWebStatusString("net_requestaborted", WebExceptionStatus.RequestCanceled), WebExceptionStatus.RequestCanceled);
			}
			this.ErrorEvent.Set();
			this.m_ErrorOccured = true;
			this.m_ErrorTimer = ConnectionPool.s_CancelErrorQueue.CreateTimer(ConnectionPool.s_CancelErrorCallback, this);
		}

		// Token: 0x06001934 RID: 6452 RVA: 0x0005A9FC File Offset: 0x00058BFC
		internal PooledStream GetConnection(object owningObject, GeneralAsyncDelegate asyncCallback, int creationTimeout)
		{
			PooledStream pooledStream = null;
			bool flag = true;
			bool flag2 = asyncCallback != null;
			if (this.m_State != ConnectionPool.State.Running)
			{
				throw new InternalException();
			}
			Interlocked.Increment(ref this.m_WaitCount);
			WaitHandle[] waitHandles = this.m_WaitHandles;
			if (flag2)
			{
				int num = WaitHandle.WaitAny(waitHandles, 0, false);
				if (num != 258)
				{
					pooledStream = this.Get(owningObject, num, ref flag, ref waitHandles);
				}
				if (pooledStream == null)
				{
					ConnectionPool.AsyncConnectionPoolRequest asyncRequest = new ConnectionPool.AsyncConnectionPoolRequest(this, owningObject, asyncCallback, creationTimeout);
					this.QueueRequest(asyncRequest);
				}
			}
			else
			{
				while (pooledStream == null && flag)
				{
					int num = WaitHandle.WaitAny(waitHandles, creationTimeout, false);
					pooledStream = this.Get(owningObject, num, ref flag, ref waitHandles);
				}
			}
			if (pooledStream != null)
			{
				if (!pooledStream.IsInitalizing)
				{
					asyncCallback = null;
				}
				try
				{
					if (!pooledStream.Activate(owningObject, asyncCallback))
					{
						pooledStream = null;
					}
					return pooledStream;
				}
				catch
				{
					this.PutConnection(pooledStream, owningObject, creationTimeout, false);
					throw;
				}
			}
			if (!flag2)
			{
				throw new InternalException();
			}
			return pooledStream;
		}

		// Token: 0x06001935 RID: 6453 RVA: 0x0005AAD8 File Offset: 0x00058CD8
		internal void PutConnection(PooledStream pooledStream, object owningObject, int creationTimeout)
		{
			this.PutConnection(pooledStream, owningObject, creationTimeout, true);
		}

		// Token: 0x06001936 RID: 6454 RVA: 0x0005AAE4 File Offset: 0x00058CE4
		internal void PutConnection(PooledStream pooledStream, object owningObject, int creationTimeout, bool canReuse)
		{
			if (pooledStream == null)
			{
				throw new ArgumentNullException("pooledStream");
			}
			pooledStream.PrePush(owningObject);
			if (this.m_State != ConnectionPool.State.ShuttingDown)
			{
				pooledStream.Deactivate();
				if (this.m_WaitCount == 0)
				{
					this.CancelErrorCallback();
				}
				if (canReuse && pooledStream.CanBePooled)
				{
					this.PutNew(pooledStream);
					return;
				}
				try
				{
					this.Destroy(pooledStream);
					return;
				}
				finally
				{
					if (this.m_WaitCount > 0)
					{
						if (!this.CreationMutex.WaitOne(creationTimeout, false))
						{
							this.Abort();
						}
						else
						{
							try
							{
								pooledStream = this.UserCreateRequest();
								if (pooledStream != null)
								{
									this.PutNew(pooledStream);
								}
							}
							finally
							{
								this.CreationMutex.ReleaseMutex();
							}
						}
					}
				}
			}
			this.Destroy(pooledStream);
		}

		// Token: 0x06001937 RID: 6455 RVA: 0x0005ABA8 File Offset: 0x00058DA8
		private void PutNew(PooledStream pooledStream)
		{
			this.m_StackNew.Push(pooledStream);
			this.Semaphore.ReleaseSemaphore();
		}

		// Token: 0x06001938 RID: 6456 RVA: 0x0005ABC4 File Offset: 0x00058DC4
		private bool ReclaimEmancipatedObjects()
		{
			bool result = false;
			object syncRoot = this.m_ObjectList.SyncRoot;
			lock (syncRoot)
			{
				object[] array = this.m_ObjectList.ToArray();
				if (array != null)
				{
					foreach (PooledStream pooledStream in array)
					{
						if (pooledStream != null)
						{
							bool flag2 = false;
							try
							{
								Monitor.TryEnter(pooledStream, ref flag2);
								if (flag2 && pooledStream.IsEmancipated)
								{
									this.PutConnection(pooledStream, null, -1);
									result = true;
								}
							}
							finally
							{
								if (flag2)
								{
									Monitor.Exit(pooledStream);
								}
							}
						}
					}
				}
			}
			return result;
		}

		// Token: 0x06001939 RID: 6457 RVA: 0x0005AC78 File Offset: 0x00058E78
		private PooledStream UserCreateRequest()
		{
			PooledStream result = null;
			if (!this.ErrorOccurred && (this.Count < this.MaxPoolSize || this.MaxPoolSize == 0) && ((this.Count & 1) == 1 || !this.ReclaimEmancipatedObjects()))
			{
				result = this.Create(this.m_CreateConnectionCallback);
			}
			return result;
		}

		// Token: 0x0600193A RID: 6458 RVA: 0x0005ACC6 File Offset: 0x00058EC6
		// Note: this type is marked as 'beforefieldinit'.
		static ConnectionPool()
		{
		}

		// Token: 0x040017D0 RID: 6096
		private static TimerThread.Callback s_CleanupCallback = new TimerThread.Callback(ConnectionPool.CleanupCallbackWrapper);

		// Token: 0x040017D1 RID: 6097
		private static TimerThread.Callback s_CancelErrorCallback = new TimerThread.Callback(ConnectionPool.CancelErrorCallbackWrapper);

		// Token: 0x040017D2 RID: 6098
		private static TimerThread.Queue s_CancelErrorQueue = TimerThread.GetOrCreateQueue(5000);

		// Token: 0x040017D3 RID: 6099
		private const int MaxQueueSize = 1048576;

		// Token: 0x040017D4 RID: 6100
		private const int SemaphoreHandleIndex = 0;

		// Token: 0x040017D5 RID: 6101
		private const int ErrorHandleIndex = 1;

		// Token: 0x040017D6 RID: 6102
		private const int CreationHandleIndex = 2;

		// Token: 0x040017D7 RID: 6103
		private const int WaitTimeout = 258;

		// Token: 0x040017D8 RID: 6104
		private const int WaitAbandoned = 128;

		// Token: 0x040017D9 RID: 6105
		private const int ErrorWait = 5000;

		// Token: 0x040017DA RID: 6106
		private readonly TimerThread.Queue m_CleanupQueue;

		// Token: 0x040017DB RID: 6107
		private ConnectionPool.State m_State;

		// Token: 0x040017DC RID: 6108
		private InterlockedStack m_StackOld;

		// Token: 0x040017DD RID: 6109
		private InterlockedStack m_StackNew;

		// Token: 0x040017DE RID: 6110
		private int m_WaitCount;

		// Token: 0x040017DF RID: 6111
		private WaitHandle[] m_WaitHandles;

		// Token: 0x040017E0 RID: 6112
		private Exception m_ResError;

		// Token: 0x040017E1 RID: 6113
		private volatile bool m_ErrorOccured;

		// Token: 0x040017E2 RID: 6114
		private TimerThread.Timer m_ErrorTimer;

		// Token: 0x040017E3 RID: 6115
		private ArrayList m_ObjectList;

		// Token: 0x040017E4 RID: 6116
		private int m_TotalObjects;

		// Token: 0x040017E5 RID: 6117
		private Queue m_QueuedRequests;

		// Token: 0x040017E6 RID: 6118
		private Thread m_AsyncThread;

		// Token: 0x040017E7 RID: 6119
		private int m_MaxPoolSize;

		// Token: 0x040017E8 RID: 6120
		private int m_MinPoolSize;

		// Token: 0x040017E9 RID: 6121
		private ServicePoint m_ServicePoint;

		// Token: 0x040017EA RID: 6122
		private CreateConnectionDelegate m_CreateConnectionCallback;

		// Token: 0x0200036F RID: 879
		private enum State
		{
			// Token: 0x040017EC RID: 6124
			Initializing,
			// Token: 0x040017ED RID: 6125
			Running,
			// Token: 0x040017EE RID: 6126
			ShuttingDown
		}

		// Token: 0x02000370 RID: 880
		private class AsyncConnectionPoolRequest
		{
			// Token: 0x0600193B RID: 6459 RVA: 0x0005ACF9 File Offset: 0x00058EF9
			public AsyncConnectionPoolRequest(ConnectionPool pool, object owningObject, GeneralAsyncDelegate asyncCallback, int creationTimeout)
			{
				this.Pool = pool;
				this.OwningObject = owningObject;
				this.AsyncCallback = asyncCallback;
				this.CreationTimeout = creationTimeout;
			}

			// Token: 0x040017EF RID: 6127
			public object OwningObject;

			// Token: 0x040017F0 RID: 6128
			public GeneralAsyncDelegate AsyncCallback;

			// Token: 0x040017F1 RID: 6129
			public ConnectionPool Pool;

			// Token: 0x040017F2 RID: 6130
			public int CreationTimeout;
		}
	}
}
