﻿using System;

namespace System.Net
{
	/// <summary>Specifies the security protocols that are supported by the Schannel security package.</summary>
	// Token: 0x020002DB RID: 731
	[Flags]
	public enum SecurityProtocolType
	{
		/// <summary>Specifies the system default security protocol as defined by Schannel.</summary>
		// Token: 0x0400142F RID: 5167
		SystemDefault = 0,
		/// <summary>Specifies the Secure Socket Layer (SSL) 3.0 security protocol.</summary>
		// Token: 0x04001430 RID: 5168
		Ssl3 = 48,
		/// <summary>Specifies the Transport Layer Security (TLS) 1.0 security protocol.</summary>
		// Token: 0x04001431 RID: 5169
		Tls = 192,
		/// <summary>Specifies the Transport Layer Security (TLS) 1.1 security protocol.</summary>
		// Token: 0x04001432 RID: 5170
		Tls11 = 768,
		/// <summary>Specifies the Transport Layer Security (TLS) 1.2 security protocol.</summary>
		// Token: 0x04001433 RID: 5171
		Tls12 = 3072
	}
}
