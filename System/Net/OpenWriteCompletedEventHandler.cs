﻿using System;

namespace System.Net
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Net.WebClient.OpenWriteCompleted" /> event of a <see cref="T:System.Net.WebClient" />.</summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="T:System.Net.OpenWriteCompletedEventArgs" /> containing event data.</param>
	// Token: 0x020003A1 RID: 929
	// (Invoke) Token: 0x06001B89 RID: 7049
	public delegate void OpenWriteCompletedEventHandler(object sender, OpenWriteCompletedEventArgs e);
}
