﻿using System;
using System.ComponentModel;

namespace System.Net
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Net.WebClient.WriteStreamClosed" /> event of a <see cref="T:System.Net.WebClient" />.</summary>
	// Token: 0x02000342 RID: 834
	// (Invoke) Token: 0x0600181B RID: 6171
	[EditorBrowsable(EditorBrowsableState.Never)]
	public delegate void WriteStreamClosedEventHandler(object sender, WriteStreamClosedEventArgs e);
}
