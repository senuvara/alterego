﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Net
{
	// Token: 0x0200030C RID: 780
	[StructLayout(LayoutKind.Sequential)]
	internal class SecurityBufferDescriptor
	{
		// Token: 0x060016B0 RID: 5808 RVA: 0x00051832 File Offset: 0x0004FA32
		public SecurityBufferDescriptor(int count)
		{
			this.Version = 0;
			this.Count = count;
			this.UnmanagedPointer = null;
		}

		// Token: 0x060016B1 RID: 5809 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		internal void DebugDump()
		{
		}

		// Token: 0x040015BB RID: 5563
		public readonly int Version;

		// Token: 0x040015BC RID: 5564
		public readonly int Count;

		// Token: 0x040015BD RID: 5565
		public unsafe void* UnmanagedPointer;
	}
}
