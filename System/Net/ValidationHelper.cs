﻿using System;
using System.Globalization;

namespace System.Net
{
	// Token: 0x020002FE RID: 766
	internal static class ValidationHelper
	{
		// Token: 0x06001698 RID: 5784 RVA: 0x000514BE File Offset: 0x0004F6BE
		public static string[] MakeEmptyArrayNull(string[] stringArray)
		{
			if (stringArray == null || stringArray.Length == 0)
			{
				return null;
			}
			return stringArray;
		}

		// Token: 0x06001699 RID: 5785 RVA: 0x000514CA File Offset: 0x0004F6CA
		public static string MakeStringNull(string stringValue)
		{
			if (stringValue == null || stringValue.Length == 0)
			{
				return null;
			}
			return stringValue;
		}

		// Token: 0x0600169A RID: 5786 RVA: 0x000514DA File Offset: 0x0004F6DA
		public static string ExceptionMessage(Exception exception)
		{
			if (exception == null)
			{
				return string.Empty;
			}
			if (exception.InnerException == null)
			{
				return exception.Message;
			}
			return exception.Message + " (" + ValidationHelper.ExceptionMessage(exception.InnerException) + ")";
		}

		// Token: 0x0600169B RID: 5787 RVA: 0x00051514 File Offset: 0x0004F714
		public static string ToString(object objectValue)
		{
			if (objectValue == null)
			{
				return "(null)";
			}
			if (objectValue is string && ((string)objectValue).Length == 0)
			{
				return "(string.empty)";
			}
			if (objectValue is Exception)
			{
				return ValidationHelper.ExceptionMessage(objectValue as Exception);
			}
			if (objectValue is IntPtr)
			{
				return "0x" + ((IntPtr)objectValue).ToString("x");
			}
			return objectValue.ToString();
		}

		// Token: 0x0600169C RID: 5788 RVA: 0x00051588 File Offset: 0x0004F788
		public static string HashString(object objectValue)
		{
			if (objectValue == null)
			{
				return "(null)";
			}
			if (objectValue is string && ((string)objectValue).Length == 0)
			{
				return "(string.empty)";
			}
			return objectValue.GetHashCode().ToString(NumberFormatInfo.InvariantInfo);
		}

		// Token: 0x0600169D RID: 5789 RVA: 0x000515CC File Offset: 0x0004F7CC
		public static bool IsInvalidHttpString(string stringValue)
		{
			return stringValue.IndexOfAny(ValidationHelper.InvalidParamChars) != -1;
		}

		// Token: 0x0600169E RID: 5790 RVA: 0x000515DF File Offset: 0x0004F7DF
		public static bool IsBlankString(string stringValue)
		{
			return stringValue == null || stringValue.Length == 0;
		}

		// Token: 0x0600169F RID: 5791 RVA: 0x000515EF File Offset: 0x0004F7EF
		public static bool ValidateTcpPort(int port)
		{
			return port >= 0 && port <= 65535;
		}

		// Token: 0x060016A0 RID: 5792 RVA: 0x00051602 File Offset: 0x0004F802
		public static bool ValidateRange(int actual, int fromAllowed, int toAllowed)
		{
			return actual >= fromAllowed && actual <= toAllowed;
		}

		// Token: 0x060016A1 RID: 5793 RVA: 0x00051614 File Offset: 0x0004F814
		internal static void ValidateSegment(ArraySegment<byte> segment)
		{
			if (segment.Array == null)
			{
				throw new ArgumentNullException("segment");
			}
			if (segment.Offset < 0 || segment.Count < 0 || segment.Count > segment.Array.Length - segment.Offset)
			{
				throw new ArgumentOutOfRangeException("segment");
			}
		}

		// Token: 0x060016A2 RID: 5794 RVA: 0x0005166E File Offset: 0x0004F86E
		// Note: this type is marked as 'beforefieldinit'.
		static ValidationHelper()
		{
		}

		// Token: 0x04001539 RID: 5433
		public static string[] EmptyArray = new string[0];

		// Token: 0x0400153A RID: 5434
		internal static readonly char[] InvalidMethodChars = new char[]
		{
			' ',
			'\r',
			'\n',
			'\t'
		};

		// Token: 0x0400153B RID: 5435
		internal static readonly char[] InvalidParamChars = new char[]
		{
			'(',
			')',
			'<',
			'>',
			'@',
			',',
			';',
			':',
			'\\',
			'"',
			'\'',
			'/',
			'[',
			']',
			'?',
			'=',
			'{',
			'}',
			' ',
			'\t',
			'\r',
			'\n'
		};
	}
}
