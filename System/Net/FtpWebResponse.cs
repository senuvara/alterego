﻿using System;
using System.IO;
using Unity;

namespace System.Net
{
	/// <summary>Encapsulates a File Transfer Protocol (FTP) server's response to a request.</summary>
	// Token: 0x020003DC RID: 988
	public class FtpWebResponse : WebResponse
	{
		// Token: 0x06001D5C RID: 7516 RVA: 0x0006A6A4 File Offset: 0x000688A4
		internal FtpWebResponse(FtpWebRequest request, Uri uri, string method, bool keepAlive)
		{
			this.lastModified = DateTime.MinValue;
			this.bannerMessage = string.Empty;
			this.welcomeMessage = string.Empty;
			this.exitMessage = string.Empty;
			this.contentLength = -1L;
			base..ctor();
			this.request = request;
			this.uri = uri;
			this.method = method;
		}

		// Token: 0x06001D5D RID: 7517 RVA: 0x0006A700 File Offset: 0x00068900
		internal FtpWebResponse(FtpWebRequest request, Uri uri, string method, FtpStatusCode statusCode, string statusDescription)
		{
			this.lastModified = DateTime.MinValue;
			this.bannerMessage = string.Empty;
			this.welcomeMessage = string.Empty;
			this.exitMessage = string.Empty;
			this.contentLength = -1L;
			base..ctor();
			this.request = request;
			this.uri = uri;
			this.method = method;
			this.statusCode = statusCode;
			this.statusDescription = statusDescription;
		}

		// Token: 0x06001D5E RID: 7518 RVA: 0x0006A76C File Offset: 0x0006896C
		internal FtpWebResponse(FtpWebRequest request, Uri uri, string method, FtpStatus status) : this(request, uri, method, status.StatusCode, status.StatusDescription)
		{
		}

		/// <summary>Gets the length of the data received from the FTP server.</summary>
		/// <returns>An <see cref="T:System.Int64" /> value that contains the number of bytes of data received from the FTP server. </returns>
		// Token: 0x170005F9 RID: 1529
		// (get) Token: 0x06001D5F RID: 7519 RVA: 0x0006A785 File Offset: 0x00068985
		public override long ContentLength
		{
			get
			{
				return this.contentLength;
			}
		}

		/// <summary>Gets an empty <see cref="T:System.Net.WebHeaderCollection" /> object.</summary>
		/// <returns>An empty <see cref="T:System.Net.WebHeaderCollection" /> object.</returns>
		// Token: 0x170005FA RID: 1530
		// (get) Token: 0x06001D60 RID: 7520 RVA: 0x0006A78D File Offset: 0x0006898D
		public override WebHeaderCollection Headers
		{
			get
			{
				return new WebHeaderCollection();
			}
		}

		/// <summary>Gets the URI that sent the response to the request.</summary>
		/// <returns>A <see cref="T:System.Uri" /> instance that identifies the resource associated with this response.</returns>
		// Token: 0x170005FB RID: 1531
		// (get) Token: 0x06001D61 RID: 7521 RVA: 0x0006A794 File Offset: 0x00068994
		public override Uri ResponseUri
		{
			get
			{
				return this.uri;
			}
		}

		/// <summary>Gets the date and time that a file on an FTP server was last modified.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> that contains the last modified date and time for a file.</returns>
		// Token: 0x170005FC RID: 1532
		// (get) Token: 0x06001D62 RID: 7522 RVA: 0x0006A79C File Offset: 0x0006899C
		// (set) Token: 0x06001D63 RID: 7523 RVA: 0x0006A7A4 File Offset: 0x000689A4
		public DateTime LastModified
		{
			get
			{
				return this.lastModified;
			}
			internal set
			{
				this.lastModified = value;
			}
		}

		/// <summary>Gets the message sent by the FTP server when a connection is established prior to logon.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the banner message sent by the server; otherwise, <see cref="F:System.String.Empty" /> if no message is sent.</returns>
		// Token: 0x170005FD RID: 1533
		// (get) Token: 0x06001D64 RID: 7524 RVA: 0x0006A7AD File Offset: 0x000689AD
		// (set) Token: 0x06001D65 RID: 7525 RVA: 0x0006A7B5 File Offset: 0x000689B5
		public string BannerMessage
		{
			get
			{
				return this.bannerMessage;
			}
			internal set
			{
				this.bannerMessage = value;
			}
		}

		/// <summary>Gets the message sent by the FTP server when authentication is complete.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the welcome message sent by the server; otherwise, <see cref="F:System.String.Empty" /> if no message is sent.</returns>
		// Token: 0x170005FE RID: 1534
		// (get) Token: 0x06001D66 RID: 7526 RVA: 0x0006A7BE File Offset: 0x000689BE
		// (set) Token: 0x06001D67 RID: 7527 RVA: 0x0006A7C6 File Offset: 0x000689C6
		public string WelcomeMessage
		{
			get
			{
				return this.welcomeMessage;
			}
			internal set
			{
				this.welcomeMessage = value;
			}
		}

		/// <summary>Gets the message sent by the server when the FTP session is ending.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the exit message sent by the server; otherwise, <see cref="F:System.String.Empty" /> if no message is sent.</returns>
		// Token: 0x170005FF RID: 1535
		// (get) Token: 0x06001D68 RID: 7528 RVA: 0x0006A7CF File Offset: 0x000689CF
		// (set) Token: 0x06001D69 RID: 7529 RVA: 0x0006A7D7 File Offset: 0x000689D7
		public string ExitMessage
		{
			get
			{
				return this.exitMessage;
			}
			internal set
			{
				this.exitMessage = value;
			}
		}

		/// <summary>Gets the most recent status code sent from the FTP server.</summary>
		/// <returns>An <see cref="T:System.Net.FtpStatusCode" /> value that indicates the most recent status code returned with this response.</returns>
		// Token: 0x17000600 RID: 1536
		// (get) Token: 0x06001D6A RID: 7530 RVA: 0x0006A7E0 File Offset: 0x000689E0
		// (set) Token: 0x06001D6B RID: 7531 RVA: 0x0006A7E8 File Offset: 0x000689E8
		public FtpStatusCode StatusCode
		{
			get
			{
				return this.statusCode;
			}
			internal set
			{
				this.statusCode = value;
			}
		}

		/// <summary>Gets a value that indicates whether the <see cref="P:System.Net.FtpWebResponse.Headers" /> property is supported by the <see cref="T:System.Net.FtpWebResponse" /> instance.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.
		///     <see langword="true" /> if the <see cref="P:System.Net.FtpWebResponse.Headers" /> property is supported by the <see cref="T:System.Net.FtpWebResponse" /> instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000601 RID: 1537
		// (get) Token: 0x06001D6C RID: 7532 RVA: 0x00003298 File Offset: 0x00001498
		public override bool SupportsHeaders
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets text that describes a status code sent from the FTP server.</summary>
		/// <returns>A <see cref="T:System.String" /> instance that contains the status code and message returned with this response.</returns>
		// Token: 0x17000602 RID: 1538
		// (get) Token: 0x06001D6D RID: 7533 RVA: 0x0006A7F1 File Offset: 0x000689F1
		// (set) Token: 0x06001D6E RID: 7534 RVA: 0x0006A7F9 File Offset: 0x000689F9
		public string StatusDescription
		{
			get
			{
				return this.statusDescription;
			}
			internal set
			{
				this.statusDescription = value;
			}
		}

		/// <summary>Frees the resources held by the response.</summary>
		// Token: 0x06001D6F RID: 7535 RVA: 0x0006A804 File Offset: 0x00068A04
		public override void Close()
		{
			if (this.disposed)
			{
				return;
			}
			this.disposed = true;
			if (this.stream != null)
			{
				this.stream.Close();
				if (this.stream == Stream.Null)
				{
					this.request.OperationCompleted();
				}
			}
			this.stream = null;
		}

		/// <summary>Retrieves the stream that contains response data sent from an FTP server.</summary>
		/// <returns>A readable <see cref="T:System.IO.Stream" /> instance that contains data returned with the response; otherwise, <see cref="F:System.IO.Stream.Null" /> if no response data was returned by the server.</returns>
		/// <exception cref="T:System.InvalidOperationException">The response did not return a data stream. </exception>
		// Token: 0x06001D70 RID: 7536 RVA: 0x0006A853 File Offset: 0x00068A53
		public override Stream GetResponseStream()
		{
			if (this.stream == null)
			{
				return Stream.Null;
			}
			if (this.method != "RETR" && this.method != "NLST")
			{
				this.CheckDisposed();
			}
			return this.stream;
		}

		// Token: 0x17000603 RID: 1539
		// (get) Token: 0x06001D72 RID: 7538 RVA: 0x0006A89C File Offset: 0x00068A9C
		// (set) Token: 0x06001D71 RID: 7537 RVA: 0x0006A893 File Offset: 0x00068A93
		internal Stream Stream
		{
			get
			{
				return this.stream;
			}
			set
			{
				this.stream = value;
			}
		}

		// Token: 0x06001D73 RID: 7539 RVA: 0x0006A8A4 File Offset: 0x00068AA4
		internal void UpdateStatus(FtpStatus status)
		{
			this.statusCode = status.StatusCode;
			this.statusDescription = status.StatusDescription;
		}

		// Token: 0x06001D74 RID: 7540 RVA: 0x0006A8BE File Offset: 0x00068ABE
		private void CheckDisposed()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
		}

		// Token: 0x06001D75 RID: 7541 RVA: 0x0006A8D9 File Offset: 0x00068AD9
		internal bool IsFinal()
		{
			return this.statusCode >= FtpStatusCode.CommandOK;
		}

		// Token: 0x06001D76 RID: 7542 RVA: 0x000092E2 File Offset: 0x000074E2
		internal FtpWebResponse()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040019E0 RID: 6624
		private Stream stream;

		// Token: 0x040019E1 RID: 6625
		private Uri uri;

		// Token: 0x040019E2 RID: 6626
		private FtpStatusCode statusCode;

		// Token: 0x040019E3 RID: 6627
		private DateTime lastModified;

		// Token: 0x040019E4 RID: 6628
		private string bannerMessage;

		// Token: 0x040019E5 RID: 6629
		private string welcomeMessage;

		// Token: 0x040019E6 RID: 6630
		private string exitMessage;

		// Token: 0x040019E7 RID: 6631
		private string statusDescription;

		// Token: 0x040019E8 RID: 6632
		private string method;

		// Token: 0x040019E9 RID: 6633
		private bool disposed;

		// Token: 0x040019EA RID: 6634
		private FtpWebRequest request;

		// Token: 0x040019EB RID: 6635
		internal long contentLength;
	}
}
