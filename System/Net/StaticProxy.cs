﻿using System;

namespace System.Net
{
	// Token: 0x0200035B RID: 859
	internal class StaticProxy : ProxyChain
	{
		// Token: 0x060018C5 RID: 6341 RVA: 0x00058F16 File Offset: 0x00057116
		internal StaticProxy(Uri destination, Uri proxy) : base(destination)
		{
			if (proxy == null)
			{
				throw new ArgumentNullException("proxy");
			}
			this.m_Proxy = proxy;
		}

		// Token: 0x060018C6 RID: 6342 RVA: 0x00058F3A File Offset: 0x0005713A
		protected override bool GetNextProxy(out Uri proxy)
		{
			proxy = this.m_Proxy;
			if (proxy == null)
			{
				return false;
			}
			this.m_Proxy = null;
			return true;
		}

		// Token: 0x0400179F RID: 6047
		private Uri m_Proxy;
	}
}
