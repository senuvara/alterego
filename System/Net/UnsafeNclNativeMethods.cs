﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

namespace System.Net
{
	// Token: 0x020003B6 RID: 950
	internal static class UnsafeNclNativeMethods
	{
		// Token: 0x020003B7 RID: 951
		internal static class HttpApi
		{
			// Token: 0x06001C09 RID: 7177 RVA: 0x00065484 File Offset: 0x00063684
			// Note: this type is marked as 'beforefieldinit'.
			static HttpApi()
			{
			}

			// Token: 0x0400193D RID: 6461
			private const int HttpHeaderRequestMaximum = 41;

			// Token: 0x0400193E RID: 6462
			private const int HttpHeaderResponseMaximum = 30;

			// Token: 0x0400193F RID: 6463
			private static string[] m_Strings = new string[]
			{
				"Cache-Control",
				"Connection",
				"Date",
				"Keep-Alive",
				"Pragma",
				"Trailer",
				"Transfer-Encoding",
				"Upgrade",
				"Via",
				"Warning",
				"Allow",
				"Content-Length",
				"Content-Type",
				"Content-Encoding",
				"Content-Language",
				"Content-Location",
				"Content-MD5",
				"Content-Range",
				"Expires",
				"Last-Modified",
				"Accept-Ranges",
				"Age",
				"ETag",
				"Location",
				"Proxy-Authenticate",
				"Retry-After",
				"Server",
				"Set-Cookie",
				"Vary",
				"WWW-Authenticate"
			};

			// Token: 0x020003B8 RID: 952
			internal static class HTTP_REQUEST_HEADER_ID
			{
				// Token: 0x06001C0A RID: 7178 RVA: 0x000655A2 File Offset: 0x000637A2
				internal static string ToString(int position)
				{
					return UnsafeNclNativeMethods.HttpApi.HTTP_REQUEST_HEADER_ID.m_Strings[position];
				}

				// Token: 0x06001C0B RID: 7179 RVA: 0x000655AC File Offset: 0x000637AC
				// Note: this type is marked as 'beforefieldinit'.
				static HTTP_REQUEST_HEADER_ID()
				{
				}

				// Token: 0x04001940 RID: 6464
				private static string[] m_Strings = new string[]
				{
					"Cache-Control",
					"Connection",
					"Date",
					"Keep-Alive",
					"Pragma",
					"Trailer",
					"Transfer-Encoding",
					"Upgrade",
					"Via",
					"Warning",
					"Allow",
					"Content-Length",
					"Content-Type",
					"Content-Encoding",
					"Content-Language",
					"Content-Location",
					"Content-MD5",
					"Content-Range",
					"Expires",
					"Last-Modified",
					"Accept",
					"Accept-Charset",
					"Accept-Encoding",
					"Accept-Language",
					"Authorization",
					"Cookie",
					"Expect",
					"From",
					"Host",
					"If-Match",
					"If-Modified-Since",
					"If-None-Match",
					"If-Range",
					"If-Unmodified-Since",
					"Max-Forwards",
					"Proxy-Authorization",
					"Referer",
					"Range",
					"Te",
					"Translate",
					"User-Agent"
				};
			}

			// Token: 0x020003B9 RID: 953
			internal static class HTTP_RESPONSE_HEADER_ID
			{
				// Token: 0x06001C0C RID: 7180 RVA: 0x00065730 File Offset: 0x00063930
				static HTTP_RESPONSE_HEADER_ID()
				{
					for (int i = 0; i < 30; i++)
					{
						UnsafeNclNativeMethods.HttpApi.HTTP_RESPONSE_HEADER_ID.m_Hashtable.Add(UnsafeNclNativeMethods.HttpApi.m_Strings[i], i);
					}
				}

				// Token: 0x06001C0D RID: 7181 RVA: 0x00065770 File Offset: 0x00063970
				internal static int IndexOfKnownHeader(string HeaderName)
				{
					object obj = UnsafeNclNativeMethods.HttpApi.HTTP_RESPONSE_HEADER_ID.m_Hashtable[HeaderName];
					if (obj != null)
					{
						return (int)obj;
					}
					return -1;
				}

				// Token: 0x06001C0E RID: 7182 RVA: 0x00065794 File Offset: 0x00063994
				internal static string ToString(int position)
				{
					return UnsafeNclNativeMethods.HttpApi.m_Strings[position];
				}

				// Token: 0x04001941 RID: 6465
				private static Hashtable m_Hashtable = new Hashtable(30);
			}

			// Token: 0x020003BA RID: 954
			internal enum Enum
			{
				// Token: 0x04001943 RID: 6467
				HttpHeaderCacheControl,
				// Token: 0x04001944 RID: 6468
				HttpHeaderConnection,
				// Token: 0x04001945 RID: 6469
				HttpHeaderDate,
				// Token: 0x04001946 RID: 6470
				HttpHeaderKeepAlive,
				// Token: 0x04001947 RID: 6471
				HttpHeaderPragma,
				// Token: 0x04001948 RID: 6472
				HttpHeaderTrailer,
				// Token: 0x04001949 RID: 6473
				HttpHeaderTransferEncoding,
				// Token: 0x0400194A RID: 6474
				HttpHeaderUpgrade,
				// Token: 0x0400194B RID: 6475
				HttpHeaderVia,
				// Token: 0x0400194C RID: 6476
				HttpHeaderWarning,
				// Token: 0x0400194D RID: 6477
				HttpHeaderAllow,
				// Token: 0x0400194E RID: 6478
				HttpHeaderContentLength,
				// Token: 0x0400194F RID: 6479
				HttpHeaderContentType,
				// Token: 0x04001950 RID: 6480
				HttpHeaderContentEncoding,
				// Token: 0x04001951 RID: 6481
				HttpHeaderContentLanguage,
				// Token: 0x04001952 RID: 6482
				HttpHeaderContentLocation,
				// Token: 0x04001953 RID: 6483
				HttpHeaderContentMd5,
				// Token: 0x04001954 RID: 6484
				HttpHeaderContentRange,
				// Token: 0x04001955 RID: 6485
				HttpHeaderExpires,
				// Token: 0x04001956 RID: 6486
				HttpHeaderLastModified,
				// Token: 0x04001957 RID: 6487
				HttpHeaderAcceptRanges,
				// Token: 0x04001958 RID: 6488
				HttpHeaderAge,
				// Token: 0x04001959 RID: 6489
				HttpHeaderEtag,
				// Token: 0x0400195A RID: 6490
				HttpHeaderLocation,
				// Token: 0x0400195B RID: 6491
				HttpHeaderProxyAuthenticate,
				// Token: 0x0400195C RID: 6492
				HttpHeaderRetryAfter,
				// Token: 0x0400195D RID: 6493
				HttpHeaderServer,
				// Token: 0x0400195E RID: 6494
				HttpHeaderSetCookie,
				// Token: 0x0400195F RID: 6495
				HttpHeaderVary,
				// Token: 0x04001960 RID: 6496
				HttpHeaderWwwAuthenticate,
				// Token: 0x04001961 RID: 6497
				HttpHeaderResponseMaximum,
				// Token: 0x04001962 RID: 6498
				HttpHeaderMaximum = 41
			}
		}

		// Token: 0x020003BB RID: 955
		internal static class SecureStringHelper
		{
			// Token: 0x06001C0F RID: 7183 RVA: 0x000657A0 File Offset: 0x000639A0
			internal static string CreateString(SecureString secureString)
			{
				IntPtr intPtr = IntPtr.Zero;
				if (secureString == null || secureString.Length == 0)
				{
					return string.Empty;
				}
				string result;
				try
				{
					intPtr = Marshal.SecureStringToGlobalAllocUnicode(secureString);
					result = Marshal.PtrToStringUni(intPtr);
				}
				finally
				{
					if (intPtr != IntPtr.Zero)
					{
						Marshal.ZeroFreeGlobalAllocUnicode(intPtr);
					}
				}
				return result;
			}

			// Token: 0x06001C10 RID: 7184 RVA: 0x000657FC File Offset: 0x000639FC
			internal unsafe static SecureString CreateSecureString(string plainString)
			{
				if (plainString == null || plainString.Length == 0)
				{
					return new SecureString();
				}
				SecureString result;
				fixed (string text = plainString)
				{
					char* ptr = text;
					if (ptr != null)
					{
						ptr += RuntimeHelpers.OffsetToStringData / 2;
					}
					result = new SecureString(ptr, plainString.Length);
				}
				return result;
			}
		}
	}
}
