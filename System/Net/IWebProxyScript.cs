﻿using System;

namespace System.Net
{
	/// <summary>Provides the base interface to load and execute scripts for automatic proxy detection.</summary>
	// Token: 0x020003F4 RID: 1012
	public interface IWebProxyScript
	{
		/// <summary>Closes a script.</summary>
		// Token: 0x06001F24 RID: 7972
		void Close();

		/// <summary>Loads a script.</summary>
		/// <param name="scriptLocation">Internal only.</param>
		/// <param name="script">Internal only.</param>
		/// <param name="helperType">Internal only.</param>
		/// <returns>A <see cref="T:System.Boolean" /> indicating whether the script was successfully loaded.</returns>
		// Token: 0x06001F25 RID: 7973
		bool Load(Uri scriptLocation, string script, Type helperType);

		/// <summary>Runs a script.</summary>
		/// <param name="url">Internal only.</param>
		/// <param name="host">Internal only.</param>
		/// <returns>A <see cref="T:System.String" />.An internal-only value returned.</returns>
		// Token: 0x06001F26 RID: 7974
		string Run(string url, string host);
	}
}
