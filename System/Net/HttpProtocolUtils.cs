﻿using System;
using System.Globalization;

namespace System.Net
{
	// Token: 0x02000319 RID: 793
	internal class HttpProtocolUtils
	{
		// Token: 0x060016C7 RID: 5831 RVA: 0x0000232F File Offset: 0x0000052F
		private HttpProtocolUtils()
		{
		}

		// Token: 0x060016C8 RID: 5832 RVA: 0x00051AF0 File Offset: 0x0004FCF0
		internal static DateTime string2date(string S)
		{
			DateTime result;
			if (HttpDateParse.ParseHttpDate(S, out result))
			{
				return result;
			}
			throw new ProtocolViolationException(SR.GetString("The value of the date string in the header is invalid."));
		}

		// Token: 0x060016C9 RID: 5833 RVA: 0x00051B18 File Offset: 0x0004FD18
		internal static string date2string(DateTime D)
		{
			DateTimeFormatInfo provider = new DateTimeFormatInfo();
			return D.ToUniversalTime().ToString("R", provider);
		}
	}
}
