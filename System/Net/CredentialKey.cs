﻿using System;
using System.Globalization;

namespace System.Net
{
	// Token: 0x020002E3 RID: 739
	internal class CredentialKey
	{
		// Token: 0x0600160E RID: 5646 RVA: 0x0004F84D File Offset: 0x0004DA4D
		internal CredentialKey(Uri uriPrefix, string authenticationType)
		{
			this.UriPrefix = uriPrefix;
			this.UriPrefixLength = this.UriPrefix.ToString().Length;
			this.AuthenticationType = authenticationType;
		}

		// Token: 0x0600160F RID: 5647 RVA: 0x0004F880 File Offset: 0x0004DA80
		internal bool Match(Uri uri, string authenticationType)
		{
			return !(uri == null) && authenticationType != null && string.Compare(authenticationType, this.AuthenticationType, StringComparison.OrdinalIgnoreCase) == 0 && this.IsPrefix(uri, this.UriPrefix);
		}

		// Token: 0x06001610 RID: 5648 RVA: 0x0004F8B0 File Offset: 0x0004DAB0
		internal bool IsPrefix(Uri uri, Uri prefixUri)
		{
			if (prefixUri.Scheme != uri.Scheme || prefixUri.Host != uri.Host || prefixUri.Port != uri.Port)
			{
				return false;
			}
			int num = prefixUri.AbsolutePath.LastIndexOf('/');
			return num <= uri.AbsolutePath.LastIndexOf('/') && string.Compare(uri.AbsolutePath, 0, prefixUri.AbsolutePath, 0, num, StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x06001611 RID: 5649 RVA: 0x0004F92B File Offset: 0x0004DB2B
		public override int GetHashCode()
		{
			if (!this.m_ComputedHashCode)
			{
				this.m_HashCode = this.AuthenticationType.ToUpperInvariant().GetHashCode() + this.UriPrefixLength + this.UriPrefix.GetHashCode();
				this.m_ComputedHashCode = true;
			}
			return this.m_HashCode;
		}

		// Token: 0x06001612 RID: 5650 RVA: 0x0004F96C File Offset: 0x0004DB6C
		public override bool Equals(object comparand)
		{
			CredentialKey credentialKey = comparand as CredentialKey;
			return comparand != null && string.Compare(this.AuthenticationType, credentialKey.AuthenticationType, StringComparison.OrdinalIgnoreCase) == 0 && this.UriPrefix.Equals(credentialKey.UriPrefix);
		}

		// Token: 0x06001613 RID: 5651 RVA: 0x0004F9AC File Offset: 0x0004DBAC
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				"[",
				this.UriPrefixLength.ToString(NumberFormatInfo.InvariantInfo),
				"]:",
				ValidationHelper.ToString(this.UriPrefix),
				":",
				ValidationHelper.ToString(this.AuthenticationType)
			});
		}

		// Token: 0x04001450 RID: 5200
		internal Uri UriPrefix;

		// Token: 0x04001451 RID: 5201
		internal int UriPrefixLength = -1;

		// Token: 0x04001452 RID: 5202
		internal string AuthenticationType;

		// Token: 0x04001453 RID: 5203
		private int m_HashCode;

		// Token: 0x04001454 RID: 5204
		private bool m_ComputedHashCode;
	}
}
