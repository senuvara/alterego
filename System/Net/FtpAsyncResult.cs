﻿using System;
using System.IO;
using System.Threading;

namespace System.Net
{
	// Token: 0x020003D4 RID: 980
	internal class FtpAsyncResult : IAsyncResult
	{
		// Token: 0x06001CCB RID: 7371 RVA: 0x00068497 File Offset: 0x00066697
		public FtpAsyncResult(AsyncCallback callback, object state)
		{
			this.callback = callback;
			this.state = state;
		}

		// Token: 0x170005D3 RID: 1491
		// (get) Token: 0x06001CCC RID: 7372 RVA: 0x000684B8 File Offset: 0x000666B8
		public object AsyncState
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x170005D4 RID: 1492
		// (get) Token: 0x06001CCD RID: 7373 RVA: 0x000684C0 File Offset: 0x000666C0
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				object obj = this.locker;
				lock (obj)
				{
					if (this.waitHandle == null)
					{
						this.waitHandle = new ManualResetEvent(false);
					}
				}
				return this.waitHandle;
			}
		}

		// Token: 0x170005D5 RID: 1493
		// (get) Token: 0x06001CCE RID: 7374 RVA: 0x00068514 File Offset: 0x00066714
		public bool CompletedSynchronously
		{
			get
			{
				return this.synch;
			}
		}

		// Token: 0x170005D6 RID: 1494
		// (get) Token: 0x06001CCF RID: 7375 RVA: 0x0006851C File Offset: 0x0006671C
		public bool IsCompleted
		{
			get
			{
				object obj = this.locker;
				bool result;
				lock (obj)
				{
					result = this.completed;
				}
				return result;
			}
		}

		// Token: 0x170005D7 RID: 1495
		// (get) Token: 0x06001CD0 RID: 7376 RVA: 0x00068560 File Offset: 0x00066760
		internal bool GotException
		{
			get
			{
				return this.exception != null;
			}
		}

		// Token: 0x170005D8 RID: 1496
		// (get) Token: 0x06001CD1 RID: 7377 RVA: 0x0006856B File Offset: 0x0006676B
		internal Exception Exception
		{
			get
			{
				return this.exception;
			}
		}

		// Token: 0x170005D9 RID: 1497
		// (get) Token: 0x06001CD2 RID: 7378 RVA: 0x00068573 File Offset: 0x00066773
		// (set) Token: 0x06001CD3 RID: 7379 RVA: 0x0006857B File Offset: 0x0006677B
		internal FtpWebResponse Response
		{
			get
			{
				return this.response;
			}
			set
			{
				this.response = value;
			}
		}

		// Token: 0x170005DA RID: 1498
		// (get) Token: 0x06001CD4 RID: 7380 RVA: 0x00068584 File Offset: 0x00066784
		// (set) Token: 0x06001CD5 RID: 7381 RVA: 0x0006858C File Offset: 0x0006678C
		internal Stream Stream
		{
			get
			{
				return this.stream;
			}
			set
			{
				this.stream = value;
			}
		}

		// Token: 0x06001CD6 RID: 7382 RVA: 0x00068595 File Offset: 0x00066795
		internal void WaitUntilComplete()
		{
			if (this.IsCompleted)
			{
				return;
			}
			this.AsyncWaitHandle.WaitOne();
		}

		// Token: 0x06001CD7 RID: 7383 RVA: 0x000685AC File Offset: 0x000667AC
		internal bool WaitUntilComplete(int timeout, bool exitContext)
		{
			return this.IsCompleted || this.AsyncWaitHandle.WaitOne(timeout, exitContext);
		}

		// Token: 0x06001CD8 RID: 7384 RVA: 0x000685C8 File Offset: 0x000667C8
		internal void SetCompleted(bool synch, Exception exc, FtpWebResponse response)
		{
			this.synch = synch;
			this.exception = exc;
			this.response = response;
			object obj = this.locker;
			lock (obj)
			{
				this.completed = true;
				if (this.waitHandle != null)
				{
					this.waitHandle.Set();
				}
			}
			this.DoCallback();
		}

		// Token: 0x06001CD9 RID: 7385 RVA: 0x00068638 File Offset: 0x00066838
		internal void SetCompleted(bool synch, FtpWebResponse response)
		{
			this.SetCompleted(synch, null, response);
		}

		// Token: 0x06001CDA RID: 7386 RVA: 0x00068643 File Offset: 0x00066843
		internal void SetCompleted(bool synch, Exception exc)
		{
			this.SetCompleted(synch, exc, null);
		}

		// Token: 0x06001CDB RID: 7387 RVA: 0x00068650 File Offset: 0x00066850
		internal void DoCallback()
		{
			if (this.callback != null)
			{
				try
				{
					this.callback(this);
				}
				catch (Exception)
				{
				}
			}
		}

		// Token: 0x06001CDC RID: 7388 RVA: 0x00068688 File Offset: 0x00066888
		internal void Reset()
		{
			this.exception = null;
			this.synch = false;
			this.response = null;
			this.state = null;
			object obj = this.locker;
			lock (obj)
			{
				this.completed = false;
				if (this.waitHandle != null)
				{
					this.waitHandle.Reset();
				}
			}
		}

		// Token: 0x0400199A RID: 6554
		private FtpWebResponse response;

		// Token: 0x0400199B RID: 6555
		private ManualResetEvent waitHandle;

		// Token: 0x0400199C RID: 6556
		private Exception exception;

		// Token: 0x0400199D RID: 6557
		private AsyncCallback callback;

		// Token: 0x0400199E RID: 6558
		private Stream stream;

		// Token: 0x0400199F RID: 6559
		private object state;

		// Token: 0x040019A0 RID: 6560
		private bool completed;

		// Token: 0x040019A1 RID: 6561
		private bool synch;

		// Token: 0x040019A2 RID: 6562
		private object locker = new object();
	}
}
