﻿using System;

namespace System.Net
{
	// Token: 0x02000327 RID: 807
	internal enum WebExceptionInternalStatus
	{
		// Token: 0x0400167B RID: 5755
		RequestFatal,
		// Token: 0x0400167C RID: 5756
		ServicePointFatal,
		// Token: 0x0400167D RID: 5757
		Recoverable,
		// Token: 0x0400167E RID: 5758
		Isolated
	}
}
