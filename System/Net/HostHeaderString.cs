﻿using System;
using System.Text;

namespace System.Net
{
	// Token: 0x0200032F RID: 815
	internal class HostHeaderString
	{
		// Token: 0x06001760 RID: 5984 RVA: 0x000543F2 File Offset: 0x000525F2
		internal HostHeaderString()
		{
			this.Init(null);
		}

		// Token: 0x06001761 RID: 5985 RVA: 0x00054401 File Offset: 0x00052601
		internal HostHeaderString(string s)
		{
			this.Init(s);
		}

		// Token: 0x06001762 RID: 5986 RVA: 0x00054410 File Offset: 0x00052610
		private void Init(string s)
		{
			this.m_String = s;
			this.m_Converted = false;
			this.m_Bytes = null;
		}

		// Token: 0x06001763 RID: 5987 RVA: 0x00054428 File Offset: 0x00052628
		private void Convert()
		{
			if (this.m_String != null && !this.m_Converted)
			{
				this.m_Bytes = Encoding.Default.GetBytes(this.m_String);
				string @string = Encoding.Default.GetString(this.m_Bytes);
				if (string.Compare(this.m_String, @string, StringComparison.Ordinal) != 0)
				{
					this.m_Bytes = Encoding.UTF8.GetBytes(this.m_String);
				}
			}
		}

		// Token: 0x170004C9 RID: 1225
		// (get) Token: 0x06001764 RID: 5988 RVA: 0x00054491 File Offset: 0x00052691
		// (set) Token: 0x06001765 RID: 5989 RVA: 0x00054499 File Offset: 0x00052699
		internal string String
		{
			get
			{
				return this.m_String;
			}
			set
			{
				this.Init(value);
			}
		}

		// Token: 0x170004CA RID: 1226
		// (get) Token: 0x06001766 RID: 5990 RVA: 0x000544A2 File Offset: 0x000526A2
		internal int ByteCount
		{
			get
			{
				this.Convert();
				return this.m_Bytes.Length;
			}
		}

		// Token: 0x170004CB RID: 1227
		// (get) Token: 0x06001767 RID: 5991 RVA: 0x000544B2 File Offset: 0x000526B2
		internal byte[] Bytes
		{
			get
			{
				this.Convert();
				return this.m_Bytes;
			}
		}

		// Token: 0x06001768 RID: 5992 RVA: 0x000544C0 File Offset: 0x000526C0
		internal void Copy(byte[] destBytes, int destByteIndex)
		{
			this.Convert();
			Array.Copy(this.m_Bytes, 0, destBytes, destByteIndex, this.m_Bytes.Length);
		}

		// Token: 0x040016CA RID: 5834
		private bool m_Converted;

		// Token: 0x040016CB RID: 5835
		private string m_String;

		// Token: 0x040016CC RID: 5836
		private byte[] m_Bytes;
	}
}
