﻿using System;

namespace System.Net
{
	// Token: 0x02000309 RID: 777
	internal enum CertUsage
	{
		// Token: 0x040015B0 RID: 5552
		MatchTypeAnd,
		// Token: 0x040015B1 RID: 5553
		MatchTypeOr
	}
}
