﻿using System;

namespace System.Net
{
	// Token: 0x0200036C RID: 876
	// (Invoke) Token: 0x06001917 RID: 6423
	internal delegate void GeneralAsyncDelegate(object request, object state);
}
