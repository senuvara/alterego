﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace System.Net
{
	// Token: 0x02000410 RID: 1040
	internal class WebConnectionStream : Stream
	{
		// Token: 0x0600206D RID: 8301 RVA: 0x000768BC File Offset: 0x00074ABC
		public WebConnectionStream(WebConnection cnc, WebConnectionData data)
		{
			if (data == null)
			{
				throw new InvalidOperationException("data was not initialized");
			}
			if (data.Headers == null)
			{
				throw new InvalidOperationException("data.Headers was not initialized");
			}
			if (data.request == null)
			{
				throw new InvalidOperationException("data.request was not initialized");
			}
			this.isRead = true;
			this.cb_wrapper = new AsyncCallback(this.ReadCallbackWrapper);
			this.pending = new ManualResetEvent(true);
			this.request = data.request;
			this.read_timeout = this.request.ReadWriteTimeout;
			this.write_timeout = this.read_timeout;
			this.cnc = cnc;
			string text = data.Headers["Transfer-Encoding"];
			bool flag = text != null && text.IndexOf("chunked", StringComparison.OrdinalIgnoreCase) != -1;
			string text2 = data.Headers["Content-Length"];
			if (!flag && text2 != null && text2 != "")
			{
				try
				{
					this.contentLength = (long)int.Parse(text2);
					if (this.contentLength == 0L && !this.IsNtlmAuth())
					{
						this.ReadAll();
					}
					goto IL_12C;
				}
				catch
				{
					this.contentLength = long.MaxValue;
					goto IL_12C;
				}
			}
			this.contentLength = long.MaxValue;
			IL_12C:
			if (!int.TryParse(text2, out this.stream_length))
			{
				this.stream_length = -1;
			}
		}

		// Token: 0x0600206E RID: 8302 RVA: 0x00076A1C File Offset: 0x00074C1C
		public WebConnectionStream(WebConnection cnc, HttpWebRequest request)
		{
			this.read_timeout = request.ReadWriteTimeout;
			this.write_timeout = this.read_timeout;
			this.isRead = false;
			this.cb_wrapper = new AsyncCallback(this.WriteCallbackWrapper);
			this.cnc = cnc;
			this.request = request;
			this.allowBuffering = request.InternalAllowBuffering;
			this.sendChunked = request.SendChunked;
			if (this.sendChunked)
			{
				this.pending = new ManualResetEvent(true);
				return;
			}
			if (this.allowBuffering)
			{
				this.writeBuffer = new MemoryStream();
			}
		}

		// Token: 0x0600206F RID: 8303 RVA: 0x00076ABC File Offset: 0x00074CBC
		private bool CheckAuthHeader(string headerName)
		{
			string text = this.cnc.Data.Headers[headerName];
			return text != null && text.IndexOf("NTLM", StringComparison.Ordinal) != -1;
		}

		// Token: 0x06002070 RID: 8304 RVA: 0x00076AF8 File Offset: 0x00074CF8
		private bool IsNtlmAuth()
		{
			return (this.request.Proxy != null && !this.request.Proxy.IsBypassed(this.request.Address) && this.CheckAuthHeader("Proxy-Authenticate")) || this.CheckAuthHeader("WWW-Authenticate");
		}

		// Token: 0x06002071 RID: 8305 RVA: 0x00076B4F File Offset: 0x00074D4F
		internal void CheckResponseInBuffer()
		{
			if (this.contentLength > 0L && (long)(this.readBufferSize - this.readBufferOffset) >= this.contentLength && !this.IsNtlmAuth())
			{
				this.ReadAll();
			}
		}

		// Token: 0x1700070C RID: 1804
		// (get) Token: 0x06002072 RID: 8306 RVA: 0x00076B7F File Offset: 0x00074D7F
		internal HttpWebRequest Request
		{
			get
			{
				return this.request;
			}
		}

		// Token: 0x1700070D RID: 1805
		// (get) Token: 0x06002073 RID: 8307 RVA: 0x00076B87 File Offset: 0x00074D87
		internal WebConnection Connection
		{
			get
			{
				return this.cnc;
			}
		}

		// Token: 0x1700070E RID: 1806
		// (get) Token: 0x06002074 RID: 8308 RVA: 0x00003298 File Offset: 0x00001498
		public override bool CanTimeout
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700070F RID: 1807
		// (get) Token: 0x06002075 RID: 8309 RVA: 0x00076B8F File Offset: 0x00074D8F
		// (set) Token: 0x06002076 RID: 8310 RVA: 0x00076B97 File Offset: 0x00074D97
		public override int ReadTimeout
		{
			get
			{
				return this.read_timeout;
			}
			set
			{
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.read_timeout = value;
			}
		}

		// Token: 0x17000710 RID: 1808
		// (get) Token: 0x06002077 RID: 8311 RVA: 0x00076BAF File Offset: 0x00074DAF
		// (set) Token: 0x06002078 RID: 8312 RVA: 0x00076BB7 File Offset: 0x00074DB7
		public override int WriteTimeout
		{
			get
			{
				return this.write_timeout;
			}
			set
			{
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.write_timeout = value;
			}
		}

		// Token: 0x17000711 RID: 1809
		// (get) Token: 0x06002079 RID: 8313 RVA: 0x00076BCF File Offset: 0x00074DCF
		internal bool CompleteRequestWritten
		{
			get
			{
				return this.complete_request_written;
			}
		}

		// Token: 0x17000712 RID: 1810
		// (set) Token: 0x0600207A RID: 8314 RVA: 0x00076BD7 File Offset: 0x00074DD7
		internal bool SendChunked
		{
			set
			{
				this.sendChunked = value;
			}
		}

		// Token: 0x17000713 RID: 1811
		// (set) Token: 0x0600207B RID: 8315 RVA: 0x00076BE0 File Offset: 0x00074DE0
		internal byte[] ReadBuffer
		{
			set
			{
				this.readBuffer = value;
			}
		}

		// Token: 0x17000714 RID: 1812
		// (set) Token: 0x0600207C RID: 8316 RVA: 0x00076BE9 File Offset: 0x00074DE9
		internal int ReadBufferOffset
		{
			set
			{
				this.readBufferOffset = value;
			}
		}

		// Token: 0x17000715 RID: 1813
		// (set) Token: 0x0600207D RID: 8317 RVA: 0x00076BF2 File Offset: 0x00074DF2
		internal int ReadBufferSize
		{
			set
			{
				this.readBufferSize = value;
			}
		}

		// Token: 0x17000716 RID: 1814
		// (get) Token: 0x0600207E RID: 8318 RVA: 0x00076BFB File Offset: 0x00074DFB
		internal byte[] WriteBuffer
		{
			get
			{
				return this.writeBuffer.GetBuffer();
			}
		}

		// Token: 0x17000717 RID: 1815
		// (get) Token: 0x0600207F RID: 8319 RVA: 0x00076C08 File Offset: 0x00074E08
		internal int WriteBufferLength
		{
			get
			{
				if (this.writeBuffer == null)
				{
					return -1;
				}
				return (int)this.writeBuffer.Length;
			}
		}

		// Token: 0x06002080 RID: 8320 RVA: 0x00076C20 File Offset: 0x00074E20
		internal void ForceCompletion()
		{
			if (!this.nextReadCalled)
			{
				if (this.contentLength == 9223372036854775807L)
				{
					this.contentLength = 0L;
				}
				this.nextReadCalled = true;
				this.cnc.NextRead();
			}
		}

		// Token: 0x06002081 RID: 8321 RVA: 0x00076C55 File Offset: 0x00074E55
		internal void CheckComplete()
		{
			if (!this.nextReadCalled && (long)(this.readBufferSize - this.readBufferOffset) == this.contentLength)
			{
				this.nextReadCalled = true;
				this.cnc.NextRead();
			}
		}

		// Token: 0x06002082 RID: 8322 RVA: 0x00076C88 File Offset: 0x00074E88
		internal void ReadAll()
		{
			if (!this.isRead || this.read_eof || this.totalRead >= this.contentLength || this.nextReadCalled)
			{
				if (this.isRead && !this.nextReadCalled)
				{
					this.nextReadCalled = true;
					this.cnc.NextRead();
				}
				return;
			}
			if (!this.pending.WaitOne(this.ReadTimeout))
			{
				throw new WebException("The operation has timed out.", WebExceptionStatus.Timeout);
			}
			object obj = this.locker;
			lock (obj)
			{
				if (this.totalRead >= this.contentLength)
				{
					return;
				}
				int num = this.readBufferSize - this.readBufferOffset;
				byte[] array2;
				int num2;
				if (this.contentLength == 9223372036854775807L)
				{
					MemoryStream memoryStream = new MemoryStream();
					byte[] array = null;
					if (this.readBuffer != null && num > 0)
					{
						memoryStream.Write(this.readBuffer, this.readBufferOffset, num);
						if (this.readBufferSize >= 8192)
						{
							array = this.readBuffer;
						}
					}
					if (array == null)
					{
						array = new byte[8192];
					}
					int count;
					while ((count = this.cnc.Read(this.request, array, 0, array.Length)) != 0)
					{
						memoryStream.Write(array, 0, count);
					}
					array2 = memoryStream.GetBuffer();
					num2 = (int)memoryStream.Length;
					this.contentLength = (long)num2;
				}
				else
				{
					num2 = (int)(this.contentLength - this.totalRead);
					array2 = new byte[num2];
					if (this.readBuffer != null && num > 0)
					{
						if (num > num2)
						{
							num = num2;
						}
						Buffer.BlockCopy(this.readBuffer, this.readBufferOffset, array2, 0, num);
					}
					int num3 = num2 - num;
					int num4 = -1;
					while (num3 > 0 && num4 != 0)
					{
						num4 = this.cnc.Read(this.request, array2, num, num3);
						num3 -= num4;
						num += num4;
					}
				}
				this.readBuffer = array2;
				this.readBufferOffset = 0;
				this.readBufferSize = num2;
				this.totalRead = 0L;
				this.nextReadCalled = true;
			}
			this.cnc.NextRead();
		}

		// Token: 0x06002083 RID: 8323 RVA: 0x00076EAC File Offset: 0x000750AC
		private void WriteCallbackWrapper(IAsyncResult r)
		{
			WebAsyncResult webAsyncResult = r as WebAsyncResult;
			if (webAsyncResult != null && webAsyncResult.AsyncWriteAll)
			{
				return;
			}
			if (r.AsyncState != null)
			{
				webAsyncResult = (WebAsyncResult)r.AsyncState;
				webAsyncResult.InnerAsyncResult = r;
				webAsyncResult.DoCallback();
				return;
			}
			try
			{
				this.EndWrite(r);
			}
			catch
			{
			}
		}

		// Token: 0x06002084 RID: 8324 RVA: 0x00076F0C File Offset: 0x0007510C
		private void ReadCallbackWrapper(IAsyncResult r)
		{
			if (r.AsyncState != null)
			{
				WebAsyncResult webAsyncResult = (WebAsyncResult)r.AsyncState;
				webAsyncResult.InnerAsyncResult = r;
				webAsyncResult.DoCallback();
				return;
			}
			try
			{
				this.EndRead(r);
			}
			catch
			{
			}
		}

		// Token: 0x06002085 RID: 8325 RVA: 0x00076F58 File Offset: 0x00075158
		public override int Read(byte[] buffer, int offset, int size)
		{
			AsyncCallback callback = this.cb_wrapper;
			WebAsyncResult webAsyncResult = (WebAsyncResult)this.BeginRead(buffer, offset, size, callback, null);
			if (!webAsyncResult.IsCompleted && !webAsyncResult.WaitUntilComplete(this.ReadTimeout, false))
			{
				this.nextReadCalled = true;
				this.cnc.Close(true);
				throw new WebException("The operation has timed out.", WebExceptionStatus.Timeout);
			}
			return this.EndRead(webAsyncResult);
		}

		// Token: 0x06002086 RID: 8326 RVA: 0x00076FBC File Offset: 0x000751BC
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int size, AsyncCallback cb, object state)
		{
			if (!this.isRead)
			{
				throw new NotSupportedException("this stream does not allow reading");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			int num = buffer.Length;
			if (offset < 0 || num < offset)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (size < 0 || num - offset < size)
			{
				throw new ArgumentOutOfRangeException("size");
			}
			object obj = this.locker;
			lock (obj)
			{
				this.pendingReads++;
				this.pending.Reset();
			}
			WebAsyncResult webAsyncResult = new WebAsyncResult(cb, state, buffer, offset, size);
			if (this.totalRead >= this.contentLength)
			{
				webAsyncResult.SetCompleted(true, -1);
				webAsyncResult.DoCallback();
				return webAsyncResult;
			}
			int num2 = this.readBufferSize - this.readBufferOffset;
			if (num2 > 0)
			{
				int num3 = (num2 > size) ? size : num2;
				Buffer.BlockCopy(this.readBuffer, this.readBufferOffset, buffer, offset, num3);
				this.readBufferOffset += num3;
				offset += num3;
				size -= num3;
				this.totalRead += (long)num3;
				if (size == 0 || this.totalRead >= this.contentLength)
				{
					webAsyncResult.SetCompleted(true, num3);
					webAsyncResult.DoCallback();
					return webAsyncResult;
				}
				webAsyncResult.NBytes = num3;
			}
			if (cb != null)
			{
				cb = this.cb_wrapper;
			}
			if (this.contentLength != 9223372036854775807L && this.contentLength - this.totalRead < (long)size)
			{
				size = (int)(this.contentLength - this.totalRead);
			}
			if (!this.read_eof)
			{
				webAsyncResult.InnerAsyncResult = this.cnc.BeginRead(this.request, buffer, offset, size, cb, webAsyncResult);
			}
			else
			{
				webAsyncResult.SetCompleted(true, webAsyncResult.NBytes);
				webAsyncResult.DoCallback();
			}
			return webAsyncResult;
		}

		// Token: 0x06002087 RID: 8327 RVA: 0x0007718C File Offset: 0x0007538C
		public override int EndRead(IAsyncResult r)
		{
			WebAsyncResult webAsyncResult = (WebAsyncResult)r;
			if (webAsyncResult.EndCalled)
			{
				int nbytes = webAsyncResult.NBytes;
				if (nbytes < 0)
				{
					return 0;
				}
				return nbytes;
			}
			else
			{
				webAsyncResult.EndCalled = true;
				object obj;
				if (!webAsyncResult.IsCompleted)
				{
					int num = -1;
					try
					{
						num = this.cnc.EndRead(this.request, webAsyncResult);
					}
					catch (Exception e)
					{
						obj = this.locker;
						lock (obj)
						{
							this.pendingReads--;
							if (this.pendingReads == 0)
							{
								this.pending.Set();
							}
						}
						this.nextReadCalled = true;
						this.cnc.Close(true);
						webAsyncResult.SetCompleted(false, e);
						webAsyncResult.DoCallback();
						throw;
					}
					if (num < 0)
					{
						num = 0;
						this.read_eof = true;
					}
					this.totalRead += (long)num;
					webAsyncResult.SetCompleted(false, num + webAsyncResult.NBytes);
					webAsyncResult.DoCallback();
					if (num == 0)
					{
						this.contentLength = this.totalRead;
					}
				}
				obj = this.locker;
				lock (obj)
				{
					this.pendingReads--;
					if (this.pendingReads == 0)
					{
						this.pending.Set();
					}
				}
				if (this.totalRead >= this.contentLength && !this.nextReadCalled)
				{
					this.ReadAll();
				}
				int nbytes2 = webAsyncResult.NBytes;
				if (nbytes2 < 0)
				{
					return 0;
				}
				return nbytes2;
			}
		}

		// Token: 0x06002088 RID: 8328 RVA: 0x00077320 File Offset: 0x00075520
		private void WriteAsyncCB(IAsyncResult r)
		{
			WebAsyncResult webAsyncResult = (WebAsyncResult)r.AsyncState;
			webAsyncResult.InnerAsyncResult = null;
			try
			{
				this.cnc.EndWrite(this.request, true, r);
				webAsyncResult.SetCompleted(false, 0);
				if (!this.initRead)
				{
					this.initRead = true;
					this.cnc.InitRead();
				}
			}
			catch (Exception ex)
			{
				this.KillBuffer();
				this.nextReadCalled = true;
				this.cnc.Close(true);
				if (ex is SocketException)
				{
					ex = new IOException("Error writing request", ex);
				}
				webAsyncResult.SetCompleted(false, ex);
			}
			if (this.allowBuffering && !this.sendChunked && this.request.ContentLength > 0L && this.totalWritten == this.request.ContentLength)
			{
				this.complete_request_written = true;
			}
			webAsyncResult.DoCallback();
		}

		// Token: 0x06002089 RID: 8329 RVA: 0x00077400 File Offset: 0x00075600
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int size, AsyncCallback cb, object state)
		{
			if (this.request.Aborted)
			{
				throw new WebException("The request was canceled.", WebExceptionStatus.RequestCanceled);
			}
			if (this.isRead)
			{
				throw new NotSupportedException("this stream does not allow writing");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			int num = buffer.Length;
			if (offset < 0 || num < offset)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (size < 0 || num - offset < size)
			{
				throw new ArgumentOutOfRangeException("size");
			}
			if (this.sendChunked)
			{
				object obj = this.locker;
				lock (obj)
				{
					this.pendingWrites++;
					this.pending.Reset();
				}
			}
			WebAsyncResult webAsyncResult = new WebAsyncResult(cb, state);
			AsyncCallback cb2 = new AsyncCallback(this.WriteAsyncCB);
			if (this.sendChunked)
			{
				this.requestWritten = true;
				string s = string.Format("{0:X}\r\n", size);
				byte[] bytes = Encoding.ASCII.GetBytes(s);
				int num2 = 2 + size + bytes.Length;
				byte[] array = new byte[num2];
				Buffer.BlockCopy(bytes, 0, array, 0, bytes.Length);
				Buffer.BlockCopy(buffer, offset, array, bytes.Length, size);
				Buffer.BlockCopy(WebConnectionStream.crlf, 0, array, bytes.Length + size, WebConnectionStream.crlf.Length);
				if (this.allowBuffering)
				{
					if (this.writeBuffer == null)
					{
						this.writeBuffer = new MemoryStream();
					}
					this.writeBuffer.Write(buffer, offset, size);
					this.totalWritten += (long)size;
				}
				buffer = array;
				offset = 0;
				size = num2;
			}
			else
			{
				this.CheckWriteOverflow(this.request.ContentLength, this.totalWritten, (long)size);
				if (this.allowBuffering)
				{
					if (this.writeBuffer == null)
					{
						this.writeBuffer = new MemoryStream();
					}
					this.writeBuffer.Write(buffer, offset, size);
					this.totalWritten += (long)size;
					if (this.request.ContentLength <= 0L || this.totalWritten < this.request.ContentLength)
					{
						webAsyncResult.SetCompleted(true, 0);
						webAsyncResult.DoCallback();
						return webAsyncResult;
					}
					webAsyncResult.AsyncWriteAll = true;
					this.requestWritten = true;
					buffer = this.writeBuffer.GetBuffer();
					offset = 0;
					size = (int)this.totalWritten;
				}
			}
			try
			{
				webAsyncResult.InnerAsyncResult = this.cnc.BeginWrite(this.request, buffer, offset, size, cb2, webAsyncResult);
				if (webAsyncResult.InnerAsyncResult == null)
				{
					if (!webAsyncResult.IsCompleted)
					{
						webAsyncResult.SetCompleted(true, 0);
					}
					webAsyncResult.DoCallback();
				}
			}
			catch (Exception)
			{
				if (!this.IgnoreIOErrors)
				{
					throw;
				}
				webAsyncResult.SetCompleted(true, 0);
				webAsyncResult.DoCallback();
			}
			this.totalWritten += (long)size;
			return webAsyncResult;
		}

		// Token: 0x0600208A RID: 8330 RVA: 0x000776BC File Offset: 0x000758BC
		private void CheckWriteOverflow(long contentLength, long totalWritten, long size)
		{
			if (contentLength == -1L)
			{
				return;
			}
			long num = contentLength - totalWritten;
			if (size > num)
			{
				this.KillBuffer();
				this.nextReadCalled = true;
				this.cnc.Close(true);
				throw new ProtocolViolationException("The number of bytes to be written is greater than the specified ContentLength.");
			}
		}

		// Token: 0x0600208B RID: 8331 RVA: 0x000776FC File Offset: 0x000758FC
		public override void EndWrite(IAsyncResult r)
		{
			if (r == null)
			{
				throw new ArgumentNullException("r");
			}
			WebAsyncResult webAsyncResult = r as WebAsyncResult;
			if (webAsyncResult == null)
			{
				throw new ArgumentException("Invalid IAsyncResult");
			}
			if (webAsyncResult.EndCalled)
			{
				return;
			}
			if (this.sendChunked)
			{
				object obj = this.locker;
				lock (obj)
				{
					this.pendingWrites--;
					if (this.pendingWrites <= 0)
					{
						this.pending.Set();
					}
				}
			}
			webAsyncResult.EndCalled = true;
			if (webAsyncResult.AsyncWriteAll)
			{
				webAsyncResult.WaitUntilComplete();
				if (webAsyncResult.GotException)
				{
					throw webAsyncResult.Exception;
				}
				return;
			}
			else
			{
				if (this.allowBuffering && !this.sendChunked)
				{
					return;
				}
				if (webAsyncResult.GotException)
				{
					throw webAsyncResult.Exception;
				}
				return;
			}
		}

		// Token: 0x0600208C RID: 8332 RVA: 0x000777D4 File Offset: 0x000759D4
		public override void Write(byte[] buffer, int offset, int size)
		{
			AsyncCallback callback = this.cb_wrapper;
			WebAsyncResult webAsyncResult = (WebAsyncResult)this.BeginWrite(buffer, offset, size, callback, null);
			if (!webAsyncResult.IsCompleted && !webAsyncResult.WaitUntilComplete(this.WriteTimeout, false))
			{
				this.KillBuffer();
				this.nextReadCalled = true;
				this.cnc.Close(true);
				throw new IOException("Write timed out.");
			}
			this.EndWrite(webAsyncResult);
		}

		// Token: 0x0600208D RID: 8333 RVA: 0x0000232D File Offset: 0x0000052D
		public override void Flush()
		{
		}

		// Token: 0x0600208E RID: 8334 RVA: 0x0007783B File Offset: 0x00075A3B
		internal void SetHeadersAsync(bool setInternalLength, SimpleAsyncCallback callback)
		{
			SimpleAsyncResult.Run((SimpleAsyncResult r) => this.SetHeadersAsync(r, setInternalLength), callback);
		}

		// Token: 0x0600208F RID: 8335 RVA: 0x00077864 File Offset: 0x00075A64
		private bool SetHeadersAsync(SimpleAsyncResult result, bool setInternalLength)
		{
			if (this.headersSent)
			{
				return false;
			}
			string method = this.request.Method;
			bool flag = method == "GET" || method == "CONNECT" || method == "HEAD" || method == "TRACE";
			bool flag2 = method == "PROPFIND" || method == "PROPPATCH" || method == "MKCOL" || method == "COPY" || method == "MOVE" || method == "LOCK" || method == "UNLOCK";
			if (setInternalLength && !flag && this.writeBuffer != null)
			{
				this.request.InternalContentLength = this.writeBuffer.Length;
			}
			bool flag3 = !flag && (this.writeBuffer == null || this.request.ContentLength > -1L);
			if (!this.sendChunked && !flag3 && !flag && !flag2)
			{
				return false;
			}
			this.headersSent = true;
			this.headers = this.request.GetRequestHeaders();
			return this.cnc.BeginWrite(this.request, this.headers, 0, this.headers.Length, delegate(IAsyncResult r)
			{
				try
				{
					this.cnc.EndWrite(this.request, true, r);
					if (!this.initRead)
					{
						this.initRead = true;
						this.cnc.InitRead();
					}
					long num = this.request.ContentLength;
					if (!this.sendChunked && num == 0L)
					{
						this.requestWritten = true;
					}
					result.SetCompleted(false);
				}
				catch (WebException e)
				{
					result.SetCompleted(false, e);
				}
				catch (Exception innerException)
				{
					result.SetCompleted(false, new WebException("Error writing headers", WebExceptionStatus.SendFailure, WebExceptionInternalStatus.RequestFatal, innerException));
				}
			}, null) != null;
		}

		// Token: 0x17000718 RID: 1816
		// (get) Token: 0x06002090 RID: 8336 RVA: 0x000779CD File Offset: 0x00075BCD
		internal bool RequestWritten
		{
			get
			{
				return this.requestWritten;
			}
		}

		// Token: 0x06002091 RID: 8337 RVA: 0x000779D8 File Offset: 0x00075BD8
		internal SimpleAsyncResult WriteRequestAsync(SimpleAsyncCallback callback)
		{
			SimpleAsyncResult simpleAsyncResult = this.WriteRequestAsync(callback);
			try
			{
				if (!this.WriteRequestAsync(simpleAsyncResult))
				{
					simpleAsyncResult.SetCompleted(true);
				}
			}
			catch (Exception e)
			{
				simpleAsyncResult.SetCompleted(true, e);
			}
			return simpleAsyncResult;
		}

		// Token: 0x06002092 RID: 8338 RVA: 0x00077A1C File Offset: 0x00075C1C
		internal bool WriteRequestAsync(SimpleAsyncResult result)
		{
			if (this.requestWritten)
			{
				return false;
			}
			this.requestWritten = true;
			if (this.sendChunked || !this.allowBuffering || this.writeBuffer == null)
			{
				return false;
			}
			byte[] bytes = this.writeBuffer.GetBuffer();
			int length = (int)this.writeBuffer.Length;
			if (this.request.ContentLength != -1L && this.request.ContentLength < (long)length)
			{
				this.nextReadCalled = true;
				this.cnc.Close(true);
				throw new WebException("Specified Content-Length is less than the number of bytes to write", null, WebExceptionStatus.ServerProtocolViolation, null);
			}
			AsyncCallback <>9__1;
			this.SetHeadersAsync(true, delegate(SimpleAsyncResult inner)
			{
				if (inner.GotException)
				{
					result.SetCompleted(inner.CompletedSynchronouslyPeek, inner.Exception);
					return;
				}
				if (this.cnc.Data.StatusCode != 0 && this.cnc.Data.StatusCode != 100)
				{
					result.SetCompleted(inner.CompletedSynchronouslyPeek);
					return;
				}
				if (!this.initRead)
				{
					this.initRead = true;
					this.cnc.InitRead();
				}
				int length;
				if (length == 0)
				{
					this.complete_request_written = true;
					result.SetCompleted(inner.CompletedSynchronouslyPeek);
					return;
				}
				WebConnection webConnection = this.cnc;
				HttpWebRequest httpWebRequest = this.request;
				byte[] bytes = bytes;
				int offset = 0;
				length = length;
				AsyncCallback cb;
				if ((cb = <>9__1) == null)
				{
					cb = (<>9__1 = delegate(IAsyncResult r)
					{
						try
						{
							this.complete_request_written = this.cnc.EndWrite(this.request, false, r);
							result.SetCompleted(false);
						}
						catch (Exception e)
						{
							result.SetCompleted(false, e);
						}
					});
				}
				webConnection.BeginWrite(httpWebRequest, bytes, offset, length, cb, null);
			});
			return true;
		}

		// Token: 0x06002093 RID: 8339 RVA: 0x00077AE4 File Offset: 0x00075CE4
		internal void InternalClose()
		{
			this.disposed = true;
		}

		// Token: 0x17000719 RID: 1817
		// (get) Token: 0x06002094 RID: 8340 RVA: 0x00077AED File Offset: 0x00075CED
		// (set) Token: 0x06002095 RID: 8341 RVA: 0x00077AF5 File Offset: 0x00075CF5
		internal bool GetResponseOnClose
		{
			[CompilerGenerated]
			get
			{
				return this.<GetResponseOnClose>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<GetResponseOnClose>k__BackingField = value;
			}
		}

		// Token: 0x06002096 RID: 8342 RVA: 0x00077B00 File Offset: 0x00075D00
		public override void Close()
		{
			if (this.GetResponseOnClose)
			{
				if (this.disposed)
				{
					return;
				}
				this.disposed = true;
				HttpWebResponse httpWebResponse = (HttpWebResponse)this.request.GetResponse();
				httpWebResponse.ReadAll();
				httpWebResponse.Close();
				return;
			}
			else if (this.sendChunked)
			{
				if (this.disposed)
				{
					return;
				}
				this.disposed = true;
				if (!this.pending.WaitOne(this.WriteTimeout))
				{
					throw new WebException("The operation has timed out.", WebExceptionStatus.Timeout);
				}
				byte[] bytes = Encoding.ASCII.GetBytes("0\r\n\r\n");
				string text = null;
				this.cnc.Write(this.request, bytes, 0, bytes.Length, ref text);
				return;
			}
			else
			{
				if (this.isRead)
				{
					if (!this.nextReadCalled)
					{
						this.CheckComplete();
						if (!this.nextReadCalled)
						{
							this.nextReadCalled = true;
							this.cnc.Close(true);
						}
					}
					return;
				}
				if (!this.allowBuffering)
				{
					this.complete_request_written = true;
					if (!this.initRead)
					{
						this.initRead = true;
						this.cnc.InitRead();
					}
					return;
				}
				if (this.disposed || this.requestWritten)
				{
					return;
				}
				long num = this.request.ContentLength;
				if (!this.sendChunked && num != -1L && this.totalWritten != num)
				{
					IOException innerException = new IOException("Cannot close the stream until all bytes are written");
					this.nextReadCalled = true;
					this.cnc.Close(true);
					throw new WebException("Request was cancelled.", WebExceptionStatus.RequestCanceled, WebExceptionInternalStatus.RequestFatal, innerException);
				}
				this.disposed = true;
				return;
			}
		}

		// Token: 0x06002097 RID: 8343 RVA: 0x00077C67 File Offset: 0x00075E67
		internal void KillBuffer()
		{
			this.writeBuffer = null;
		}

		// Token: 0x06002098 RID: 8344 RVA: 0x00006740 File Offset: 0x00004940
		public override long Seek(long a, SeekOrigin b)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002099 RID: 8345 RVA: 0x00006740 File Offset: 0x00004940
		public override void SetLength(long a)
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700071A RID: 1818
		// (get) Token: 0x0600209A RID: 8346 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700071B RID: 1819
		// (get) Token: 0x0600209B RID: 8347 RVA: 0x00077C70 File Offset: 0x00075E70
		public override bool CanRead
		{
			get
			{
				return !this.disposed && this.isRead;
			}
		}

		// Token: 0x1700071C RID: 1820
		// (get) Token: 0x0600209C RID: 8348 RVA: 0x00077C82 File Offset: 0x00075E82
		public override bool CanWrite
		{
			get
			{
				return !this.disposed && !this.isRead;
			}
		}

		// Token: 0x1700071D RID: 1821
		// (get) Token: 0x0600209D RID: 8349 RVA: 0x00077C97 File Offset: 0x00075E97
		public override long Length
		{
			get
			{
				if (!this.isRead)
				{
					throw new NotSupportedException();
				}
				return (long)this.stream_length;
			}
		}

		// Token: 0x1700071E RID: 1822
		// (get) Token: 0x0600209E RID: 8350 RVA: 0x00006740 File Offset: 0x00004940
		// (set) Token: 0x0600209F RID: 8351 RVA: 0x00006740 File Offset: 0x00004940
		public override long Position
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x060020A0 RID: 8352 RVA: 0x00077CAE File Offset: 0x00075EAE
		// Note: this type is marked as 'beforefieldinit'.
		static WebConnectionStream()
		{
		}

		// Token: 0x04001B69 RID: 7017
		private static byte[] crlf = new byte[]
		{
			13,
			10
		};

		// Token: 0x04001B6A RID: 7018
		private bool isRead;

		// Token: 0x04001B6B RID: 7019
		private WebConnection cnc;

		// Token: 0x04001B6C RID: 7020
		private HttpWebRequest request;

		// Token: 0x04001B6D RID: 7021
		private byte[] readBuffer;

		// Token: 0x04001B6E RID: 7022
		private int readBufferOffset;

		// Token: 0x04001B6F RID: 7023
		private int readBufferSize;

		// Token: 0x04001B70 RID: 7024
		private int stream_length;

		// Token: 0x04001B71 RID: 7025
		private long contentLength;

		// Token: 0x04001B72 RID: 7026
		private long totalRead;

		// Token: 0x04001B73 RID: 7027
		internal long totalWritten;

		// Token: 0x04001B74 RID: 7028
		private bool nextReadCalled;

		// Token: 0x04001B75 RID: 7029
		private int pendingReads;

		// Token: 0x04001B76 RID: 7030
		private int pendingWrites;

		// Token: 0x04001B77 RID: 7031
		private ManualResetEvent pending;

		// Token: 0x04001B78 RID: 7032
		private bool allowBuffering;

		// Token: 0x04001B79 RID: 7033
		private bool sendChunked;

		// Token: 0x04001B7A RID: 7034
		private MemoryStream writeBuffer;

		// Token: 0x04001B7B RID: 7035
		private bool requestWritten;

		// Token: 0x04001B7C RID: 7036
		private byte[] headers;

		// Token: 0x04001B7D RID: 7037
		private bool disposed;

		// Token: 0x04001B7E RID: 7038
		private bool headersSent;

		// Token: 0x04001B7F RID: 7039
		private object locker = new object();

		// Token: 0x04001B80 RID: 7040
		private bool initRead;

		// Token: 0x04001B81 RID: 7041
		private bool read_eof;

		// Token: 0x04001B82 RID: 7042
		private bool complete_request_written;

		// Token: 0x04001B83 RID: 7043
		private int read_timeout;

		// Token: 0x04001B84 RID: 7044
		private int write_timeout;

		// Token: 0x04001B85 RID: 7045
		private AsyncCallback cb_wrapper;

		// Token: 0x04001B86 RID: 7046
		internal bool IgnoreIOErrors;

		// Token: 0x04001B87 RID: 7047
		[CompilerGenerated]
		private bool <GetResponseOnClose>k__BackingField;

		// Token: 0x02000411 RID: 1041
		[CompilerGenerated]
		private sealed class <>c__DisplayClass75_0
		{
			// Token: 0x060020A1 RID: 8353 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass75_0()
			{
			}

			// Token: 0x060020A2 RID: 8354 RVA: 0x00077CC5 File Offset: 0x00075EC5
			internal bool <SetHeadersAsync>b__0(SimpleAsyncResult r)
			{
				return this.<>4__this.SetHeadersAsync(r, this.setInternalLength);
			}

			// Token: 0x04001B88 RID: 7048
			public WebConnectionStream <>4__this;

			// Token: 0x04001B89 RID: 7049
			public bool setInternalLength;
		}

		// Token: 0x02000412 RID: 1042
		[CompilerGenerated]
		private sealed class <>c__DisplayClass76_0
		{
			// Token: 0x060020A3 RID: 8355 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass76_0()
			{
			}

			// Token: 0x060020A4 RID: 8356 RVA: 0x00077CDC File Offset: 0x00075EDC
			internal void <SetHeadersAsync>b__0(IAsyncResult r)
			{
				try
				{
					this.<>4__this.cnc.EndWrite(this.<>4__this.request, true, r);
					if (!this.<>4__this.initRead)
					{
						this.<>4__this.initRead = true;
						this.<>4__this.cnc.InitRead();
					}
					long contentLength = this.<>4__this.request.ContentLength;
					if (!this.<>4__this.sendChunked && contentLength == 0L)
					{
						this.<>4__this.requestWritten = true;
					}
					this.result.SetCompleted(false);
				}
				catch (WebException e)
				{
					this.result.SetCompleted(false, e);
				}
				catch (Exception innerException)
				{
					this.result.SetCompleted(false, new WebException("Error writing headers", WebExceptionStatus.SendFailure, WebExceptionInternalStatus.RequestFatal, innerException));
				}
			}

			// Token: 0x04001B8A RID: 7050
			public WebConnectionStream <>4__this;

			// Token: 0x04001B8B RID: 7051
			public SimpleAsyncResult result;
		}

		// Token: 0x02000413 RID: 1043
		[CompilerGenerated]
		private sealed class <>c__DisplayClass80_0
		{
			// Token: 0x060020A5 RID: 8357 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass80_0()
			{
			}

			// Token: 0x060020A6 RID: 8358 RVA: 0x00077DB4 File Offset: 0x00075FB4
			internal void <WriteRequestAsync>b__0(SimpleAsyncResult inner)
			{
				if (inner.GotException)
				{
					this.result.SetCompleted(inner.CompletedSynchronouslyPeek, inner.Exception);
					return;
				}
				if (this.<>4__this.cnc.Data.StatusCode != 0 && this.<>4__this.cnc.Data.StatusCode != 100)
				{
					this.result.SetCompleted(inner.CompletedSynchronouslyPeek);
					return;
				}
				if (!this.<>4__this.initRead)
				{
					this.<>4__this.initRead = true;
					this.<>4__this.cnc.InitRead();
				}
				if (this.length == 0)
				{
					this.<>4__this.complete_request_written = true;
					this.result.SetCompleted(inner.CompletedSynchronouslyPeek);
					return;
				}
				WebConnection cnc = this.<>4__this.cnc;
				HttpWebRequest request = this.<>4__this.request;
				byte[] buffer = this.bytes;
				int offset = 0;
				int size = this.length;
				AsyncCallback cb;
				if ((cb = this.<>9__1) == null)
				{
					cb = (this.<>9__1 = delegate(IAsyncResult r)
					{
						try
						{
							this.<>4__this.complete_request_written = this.<>4__this.cnc.EndWrite(this.<>4__this.request, false, r);
							this.result.SetCompleted(false);
						}
						catch (Exception e)
						{
							this.result.SetCompleted(false, e);
						}
					});
				}
				cnc.BeginWrite(request, buffer, offset, size, cb, null);
			}

			// Token: 0x060020A7 RID: 8359 RVA: 0x00077EBC File Offset: 0x000760BC
			internal void <WriteRequestAsync>b__1(IAsyncResult r)
			{
				try
				{
					this.<>4__this.complete_request_written = this.<>4__this.cnc.EndWrite(this.<>4__this.request, false, r);
					this.result.SetCompleted(false);
				}
				catch (Exception e)
				{
					this.result.SetCompleted(false, e);
				}
			}

			// Token: 0x04001B8C RID: 7052
			public SimpleAsyncResult result;

			// Token: 0x04001B8D RID: 7053
			public WebConnectionStream <>4__this;

			// Token: 0x04001B8E RID: 7054
			public int length;

			// Token: 0x04001B8F RID: 7055
			public byte[] bytes;

			// Token: 0x04001B90 RID: 7056
			public AsyncCallback <>9__1;
		}
	}
}
