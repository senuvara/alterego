﻿using System;

namespace System.Net
{
	// Token: 0x02000304 RID: 772
	internal enum Endianness
	{
		// Token: 0x04001583 RID: 5507
		Network,
		// Token: 0x04001584 RID: 5508
		Native = 16
	}
}
