﻿using System;
using Mono.Http;

namespace System.Net
{
	// Token: 0x020003FC RID: 1020
	internal class NtlmClient : IAuthenticationModule
	{
		// Token: 0x06001F57 RID: 8023 RVA: 0x00071DCA File Offset: 0x0006FFCA
		public NtlmClient()
		{
			this.authObject = new NtlmClient();
		}

		// Token: 0x06001F58 RID: 8024 RVA: 0x00071DDD File Offset: 0x0006FFDD
		public Authorization Authenticate(string challenge, WebRequest webRequest, ICredentials credentials)
		{
			if (this.authObject == null)
			{
				return null;
			}
			return this.authObject.Authenticate(challenge, webRequest, credentials);
		}

		// Token: 0x06001F59 RID: 8025 RVA: 0x00008B3F File Offset: 0x00006D3F
		public Authorization PreAuthenticate(WebRequest webRequest, ICredentials credentials)
		{
			return null;
		}

		// Token: 0x170006B6 RID: 1718
		// (get) Token: 0x06001F5A RID: 8026 RVA: 0x00008F6C File Offset: 0x0000716C
		public string AuthenticationType
		{
			get
			{
				return "NTLM";
			}
		}

		// Token: 0x170006B7 RID: 1719
		// (get) Token: 0x06001F5B RID: 8027 RVA: 0x00005AFA File Offset: 0x00003CFA
		public bool CanPreAuthenticate
		{
			get
			{
				return false;
			}
		}

		// Token: 0x04001ADE RID: 6878
		private IAuthenticationModule authObject;
	}
}
