﻿using System;
using System.Collections;

namespace System.Net
{
	// Token: 0x020003D2 RID: 978
	internal sealed class EndPointManager
	{
		// Token: 0x06001CB0 RID: 7344 RVA: 0x0000232F File Offset: 0x0000052F
		private EndPointManager()
		{
		}

		// Token: 0x06001CB1 RID: 7345 RVA: 0x00067A08 File Offset: 0x00065C08
		public static void AddListener(HttpListener listener)
		{
			ArrayList arrayList = new ArrayList();
			try
			{
				Hashtable obj = EndPointManager.ip_to_endpoints;
				lock (obj)
				{
					foreach (string text in listener.Prefixes)
					{
						EndPointManager.AddPrefixInternal(text, listener);
						arrayList.Add(text);
					}
				}
			}
			catch
			{
				foreach (object obj2 in arrayList)
				{
					EndPointManager.RemovePrefix((string)obj2, listener);
				}
				throw;
			}
		}

		// Token: 0x06001CB2 RID: 7346 RVA: 0x00067AE8 File Offset: 0x00065CE8
		public static void AddPrefix(string prefix, HttpListener listener)
		{
			Hashtable obj = EndPointManager.ip_to_endpoints;
			lock (obj)
			{
				EndPointManager.AddPrefixInternal(prefix, listener);
			}
		}

		// Token: 0x06001CB3 RID: 7347 RVA: 0x00067B28 File Offset: 0x00065D28
		private static void AddPrefixInternal(string p, HttpListener listener)
		{
			ListenerPrefix listenerPrefix = new ListenerPrefix(p);
			if (listenerPrefix.Path.IndexOf('%') != -1)
			{
				throw new HttpListenerException(400, "Invalid path.");
			}
			if (listenerPrefix.Path.IndexOf("//", StringComparison.Ordinal) != -1)
			{
				throw new HttpListenerException(400, "Invalid path.");
			}
			EndPointManager.GetEPListener(listenerPrefix.Host, listenerPrefix.Port, listener, listenerPrefix.Secure).AddPrefix(listenerPrefix, listener);
		}

		// Token: 0x06001CB4 RID: 7348 RVA: 0x00067BA0 File Offset: 0x00065DA0
		private static EndPointListener GetEPListener(string host, int port, HttpListener listener, bool secure)
		{
			IPAddress ipaddress;
			if (host == "*")
			{
				ipaddress = IPAddress.Any;
			}
			else if (!IPAddress.TryParse(host, out ipaddress))
			{
				try
				{
					IPHostEntry hostByName = Dns.GetHostByName(host);
					if (hostByName != null)
					{
						ipaddress = hostByName.AddressList[0];
					}
					else
					{
						ipaddress = IPAddress.Any;
					}
				}
				catch
				{
					ipaddress = IPAddress.Any;
				}
			}
			Hashtable hashtable;
			if (EndPointManager.ip_to_endpoints.ContainsKey(ipaddress))
			{
				hashtable = (Hashtable)EndPointManager.ip_to_endpoints[ipaddress];
			}
			else
			{
				hashtable = new Hashtable();
				EndPointManager.ip_to_endpoints[ipaddress] = hashtable;
			}
			EndPointListener endPointListener;
			if (hashtable.ContainsKey(port))
			{
				endPointListener = (EndPointListener)hashtable[port];
			}
			else
			{
				endPointListener = new EndPointListener(listener, ipaddress, port, secure);
				hashtable[port] = endPointListener;
			}
			return endPointListener;
		}

		// Token: 0x06001CB5 RID: 7349 RVA: 0x00067C74 File Offset: 0x00065E74
		public static void RemoveEndPoint(EndPointListener epl, IPEndPoint ep)
		{
			Hashtable obj = EndPointManager.ip_to_endpoints;
			lock (obj)
			{
				Hashtable hashtable = (Hashtable)EndPointManager.ip_to_endpoints[ep.Address];
				hashtable.Remove(ep.Port);
				if (hashtable.Count == 0)
				{
					EndPointManager.ip_to_endpoints.Remove(ep.Address);
				}
				epl.Close();
			}
		}

		// Token: 0x06001CB6 RID: 7350 RVA: 0x00067CF0 File Offset: 0x00065EF0
		public static void RemoveListener(HttpListener listener)
		{
			Hashtable obj = EndPointManager.ip_to_endpoints;
			lock (obj)
			{
				foreach (string prefix in listener.Prefixes)
				{
					EndPointManager.RemovePrefixInternal(prefix, listener);
				}
			}
		}

		// Token: 0x06001CB7 RID: 7351 RVA: 0x00067D64 File Offset: 0x00065F64
		public static void RemovePrefix(string prefix, HttpListener listener)
		{
			Hashtable obj = EndPointManager.ip_to_endpoints;
			lock (obj)
			{
				EndPointManager.RemovePrefixInternal(prefix, listener);
			}
		}

		// Token: 0x06001CB8 RID: 7352 RVA: 0x00067DA4 File Offset: 0x00065FA4
		private static void RemovePrefixInternal(string prefix, HttpListener listener)
		{
			ListenerPrefix listenerPrefix = new ListenerPrefix(prefix);
			if (listenerPrefix.Path.IndexOf('%') != -1)
			{
				return;
			}
			if (listenerPrefix.Path.IndexOf("//", StringComparison.Ordinal) != -1)
			{
				return;
			}
			EndPointManager.GetEPListener(listenerPrefix.Host, listenerPrefix.Port, listener, listenerPrefix.Secure).RemovePrefix(listenerPrefix, listener);
		}

		// Token: 0x06001CB9 RID: 7353 RVA: 0x00067DFD File Offset: 0x00065FFD
		// Note: this type is marked as 'beforefieldinit'.
		static EndPointManager()
		{
		}

		// Token: 0x04001992 RID: 6546
		private static Hashtable ip_to_endpoints = new Hashtable();
	}
}
