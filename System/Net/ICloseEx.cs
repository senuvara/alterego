﻿using System;

namespace System.Net
{
	// Token: 0x0200034E RID: 846
	internal interface ICloseEx
	{
		// Token: 0x0600182D RID: 6189
		void CloseEx(CloseExState closeState);
	}
}
