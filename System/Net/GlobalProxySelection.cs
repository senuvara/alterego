﻿using System;

namespace System.Net
{
	/// <summary>Contains a global default proxy instance for all HTTP requests.</summary>
	// Token: 0x020002E7 RID: 743
	[Obsolete("This class has been deprecated. Please use WebRequest.DefaultWebProxy instead to access and set the global default proxy. Use 'null' instead of GetEmptyWebProxy. http://go.microsoft.com/fwlink/?linkid=14202")]
	public class GlobalProxySelection
	{
		/// <summary>Gets or sets the global HTTP proxy.</summary>
		/// <returns>An <see cref="T:System.Net.IWebProxy" /> that every call to <see cref="M:System.Net.HttpWebRequest.GetResponse" /> uses.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value specified for a set operation was <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have permission for the requested operation. </exception>
		// Token: 0x17000488 RID: 1160
		// (get) Token: 0x06001620 RID: 5664 RVA: 0x0004FB7C File Offset: 0x0004DD7C
		// (set) Token: 0x06001621 RID: 5665 RVA: 0x0004FBAA File Offset: 0x0004DDAA
		public static IWebProxy Select
		{
			get
			{
				IWebProxy defaultWebProxy = WebRequest.DefaultWebProxy;
				if (defaultWebProxy == null)
				{
					return GlobalProxySelection.GetEmptyWebProxy();
				}
				WebRequest.WebProxyWrapper webProxyWrapper = defaultWebProxy as WebRequest.WebProxyWrapper;
				if (webProxyWrapper != null)
				{
					return webProxyWrapper.WebProxy;
				}
				return defaultWebProxy;
			}
			set
			{
				WebRequest.DefaultWebProxy = value;
			}
		}

		/// <summary>Returns an empty proxy instance.</summary>
		/// <returns>An <see cref="T:System.Net.IWebProxy" /> that contains no information.</returns>
		// Token: 0x06001622 RID: 5666 RVA: 0x0004FBB2 File Offset: 0x0004DDB2
		public static IWebProxy GetEmptyWebProxy()
		{
			return new EmptyWebProxy();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.GlobalProxySelection" /> class.</summary>
		// Token: 0x06001623 RID: 5667 RVA: 0x0000232F File Offset: 0x0000052F
		public GlobalProxySelection()
		{
		}
	}
}
