﻿using System;
using System.Collections;

namespace System.Net
{
	// Token: 0x020003B3 RID: 947
	internal class WebProxyData
	{
		// Token: 0x06001BCB RID: 7115 RVA: 0x0000232F File Offset: 0x0000052F
		public WebProxyData()
		{
		}

		// Token: 0x0400192C RID: 6444
		internal bool bypassOnLocal;

		// Token: 0x0400192D RID: 6445
		internal bool automaticallyDetectSettings;

		// Token: 0x0400192E RID: 6446
		internal Uri proxyAddress;

		// Token: 0x0400192F RID: 6447
		internal Hashtable proxyHostAddresses;

		// Token: 0x04001930 RID: 6448
		internal Uri scriptLocation;

		// Token: 0x04001931 RID: 6449
		internal ArrayList bypassList;
	}
}
