﻿using System;

namespace System.Net
{
	/// <summary>Contains an authentication message for an Internet server.</summary>
	// Token: 0x020002DE RID: 734
	public class Authorization
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Net.Authorization" /> class with the specified authorization message.</summary>
		/// <param name="token">The encrypted authorization message expected by the server. </param>
		// Token: 0x060015EC RID: 5612 RVA: 0x0004F129 File Offset: 0x0004D329
		public Authorization(string token)
		{
			this.m_Message = ValidationHelper.MakeStringNull(token);
			this.m_Complete = true;
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Net.Authorization" /> class with the specified authorization message and completion status.</summary>
		/// <param name="token">The encrypted authorization message expected by the server. </param>
		/// <param name="finished">The completion status of the authorization attempt. <see langword="true" /> if the authorization attempt is complete; otherwise, <see langword="false" />. </param>
		// Token: 0x060015ED RID: 5613 RVA: 0x0004F144 File Offset: 0x0004D344
		public Authorization(string token, bool finished)
		{
			this.m_Message = ValidationHelper.MakeStringNull(token);
			this.m_Complete = finished;
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Net.Authorization" /> class with the specified authorization message, completion status, and connection group identifier.</summary>
		/// <param name="token">The encrypted authorization message expected by the server. </param>
		/// <param name="finished">The completion status of the authorization attempt. <see langword="true" /> if the authorization attempt is complete; otherwise, <see langword="false" />. </param>
		/// <param name="connectionGroupId">A unique identifier that can be used to create private client-server connections that are bound only to this authentication scheme. </param>
		// Token: 0x060015EE RID: 5614 RVA: 0x0004F15F File Offset: 0x0004D35F
		public Authorization(string token, bool finished, string connectionGroupId) : this(token, finished, connectionGroupId, false)
		{
		}

		// Token: 0x060015EF RID: 5615 RVA: 0x0004F16B File Offset: 0x0004D36B
		internal Authorization(string token, bool finished, string connectionGroupId, bool mutualAuth)
		{
			this.m_Message = ValidationHelper.MakeStringNull(token);
			this.m_ConnectionGroupId = ValidationHelper.MakeStringNull(connectionGroupId);
			this.m_Complete = finished;
			this.m_MutualAuth = mutualAuth;
		}

		/// <summary>Gets the message returned to the server in response to an authentication challenge.</summary>
		/// <returns>The message that will be returned to the server in response to an authentication challenge.</returns>
		// Token: 0x1700047B RID: 1147
		// (get) Token: 0x060015F0 RID: 5616 RVA: 0x0004F19A File Offset: 0x0004D39A
		public string Message
		{
			get
			{
				return this.m_Message;
			}
		}

		/// <summary>Gets a unique identifier for user-specific connections.</summary>
		/// <returns>A unique string that associates a connection with an authenticating entity.</returns>
		// Token: 0x1700047C RID: 1148
		// (get) Token: 0x060015F1 RID: 5617 RVA: 0x0004F1A2 File Offset: 0x0004D3A2
		public string ConnectionGroupId
		{
			get
			{
				return this.m_ConnectionGroupId;
			}
		}

		/// <summary>Gets the completion status of the authorization.</summary>
		/// <returns>
		///     <see langword="true" /> if the authentication process is complete; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700047D RID: 1149
		// (get) Token: 0x060015F2 RID: 5618 RVA: 0x0004F1AA File Offset: 0x0004D3AA
		public bool Complete
		{
			get
			{
				return this.m_Complete;
			}
		}

		// Token: 0x060015F3 RID: 5619 RVA: 0x0004F1B2 File Offset: 0x0004D3B2
		internal void SetComplete(bool complete)
		{
			this.m_Complete = complete;
		}

		/// <summary>Gets or sets the prefix for Uniform Resource Identifiers (URIs) that can be authenticated with the <see cref="P:System.Net.Authorization.Message" /> property.</summary>
		/// <returns>An array of strings that contains URI prefixes.</returns>
		// Token: 0x1700047E RID: 1150
		// (get) Token: 0x060015F4 RID: 5620 RVA: 0x0004F1BB File Offset: 0x0004D3BB
		// (set) Token: 0x060015F5 RID: 5621 RVA: 0x0004F1C4 File Offset: 0x0004D3C4
		public string[] ProtectionRealm
		{
			get
			{
				return this.m_ProtectionRealm;
			}
			set
			{
				string[] protectionRealm = ValidationHelper.MakeEmptyArrayNull(value);
				this.m_ProtectionRealm = protectionRealm;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that indicates whether mutual authentication occurred.</summary>
		/// <returns>
		///     <see langword="true" /> if both client and server were authenticated; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700047F RID: 1151
		// (get) Token: 0x060015F6 RID: 5622 RVA: 0x0004F1DF File Offset: 0x0004D3DF
		// (set) Token: 0x060015F7 RID: 5623 RVA: 0x0004F1F1 File Offset: 0x0004D3F1
		public bool MutuallyAuthenticated
		{
			get
			{
				return this.Complete && this.m_MutualAuth;
			}
			set
			{
				this.m_MutualAuth = value;
			}
		}

		// Token: 0x0400143C RID: 5180
		private string m_Message;

		// Token: 0x0400143D RID: 5181
		private bool m_Complete;

		// Token: 0x0400143E RID: 5182
		private string[] m_ProtectionRealm;

		// Token: 0x0400143F RID: 5183
		private string m_ConnectionGroupId;

		// Token: 0x04001440 RID: 5184
		private bool m_MutualAuth;

		// Token: 0x04001441 RID: 5185
		internal string ModuleAuthenticationType;
	}
}
