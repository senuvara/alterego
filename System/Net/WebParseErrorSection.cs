﻿using System;

namespace System.Net
{
	// Token: 0x02000346 RID: 838
	internal enum WebParseErrorSection
	{
		// Token: 0x04001721 RID: 5921
		Generic,
		// Token: 0x04001722 RID: 5922
		ResponseHeader,
		// Token: 0x04001723 RID: 5923
		ResponseStatusLine,
		// Token: 0x04001724 RID: 5924
		ResponseBody
	}
}
