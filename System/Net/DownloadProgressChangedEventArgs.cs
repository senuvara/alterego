﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Net
{
	/// <summary>Provides data for the <see cref="E:System.Net.WebClient.DownloadProgressChanged" /> event of a <see cref="T:System.Net.WebClient" />.</summary>
	// Token: 0x020003B0 RID: 944
	public class DownloadProgressChangedEventArgs : ProgressChangedEventArgs
	{
		// Token: 0x06001BBD RID: 7101 RVA: 0x00064AA3 File Offset: 0x00062CA3
		internal DownloadProgressChangedEventArgs(int progressPercentage, object userToken, long bytesReceived, long totalBytesToReceive) : base(progressPercentage, userToken)
		{
			this.m_BytesReceived = bytesReceived;
			this.m_TotalBytesToReceive = totalBytesToReceive;
		}

		/// <summary>Gets the number of bytes received.</summary>
		/// <returns>An <see cref="T:System.Int64" /> value that indicates the number of bytes received.</returns>
		// Token: 0x170005A4 RID: 1444
		// (get) Token: 0x06001BBE RID: 7102 RVA: 0x00064ABC File Offset: 0x00062CBC
		public long BytesReceived
		{
			get
			{
				return this.m_BytesReceived;
			}
		}

		/// <summary>Gets the total number of bytes in a <see cref="T:System.Net.WebClient" /> data download operation.</summary>
		/// <returns>An <see cref="T:System.Int64" /> value that indicates the number of bytes that will be received.</returns>
		// Token: 0x170005A5 RID: 1445
		// (get) Token: 0x06001BBF RID: 7103 RVA: 0x00064AC4 File Offset: 0x00062CC4
		public long TotalBytesToReceive
		{
			get
			{
				return this.m_TotalBytesToReceive;
			}
		}

		// Token: 0x06001BC0 RID: 7104 RVA: 0x000092E2 File Offset: 0x000074E2
		internal DownloadProgressChangedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001926 RID: 6438
		private long m_BytesReceived;

		// Token: 0x04001927 RID: 6439
		private long m_TotalBytesToReceive;
	}
}
