﻿using System;
using System.Globalization;
using System.Reflection;

namespace System.Net
{
	// Token: 0x02000310 RID: 784
	internal class WebRequestPrefixElement
	{
		// Token: 0x170004A8 RID: 1192
		// (get) Token: 0x060016B3 RID: 5811 RVA: 0x00051850 File Offset: 0x0004FA50
		// (set) Token: 0x060016B4 RID: 5812 RVA: 0x000518D0 File Offset: 0x0004FAD0
		public IWebRequestCreate Creator
		{
			get
			{
				if (this.creator == null && this.creatorType != null)
				{
					lock (this)
					{
						if (this.creator == null)
						{
							this.creator = (IWebRequestCreate)Activator.CreateInstance(this.creatorType, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.CreateInstance, null, new object[0], CultureInfo.InvariantCulture);
						}
					}
				}
				return this.creator;
			}
			set
			{
				this.creator = value;
			}
		}

		// Token: 0x060016B5 RID: 5813 RVA: 0x000518DC File Offset: 0x0004FADC
		public WebRequestPrefixElement(string P, Type creatorType)
		{
			if (!typeof(IWebRequestCreate).IsAssignableFrom(creatorType))
			{
				throw new InvalidCastException(SR.GetString("Invalid cast from {0} to {1}.", new object[]
				{
					creatorType.AssemblyQualifiedName,
					"IWebRequestCreate"
				}));
			}
			this.Prefix = P;
			this.creatorType = creatorType;
		}

		// Token: 0x060016B6 RID: 5814 RVA: 0x00051936 File Offset: 0x0004FB36
		public WebRequestPrefixElement(string P, IWebRequestCreate C)
		{
			this.Prefix = P;
			this.Creator = C;
		}

		// Token: 0x040015ED RID: 5613
		public string Prefix;

		// Token: 0x040015EE RID: 5614
		internal IWebRequestCreate creator;

		// Token: 0x040015EF RID: 5615
		internal Type creatorType;
	}
}
