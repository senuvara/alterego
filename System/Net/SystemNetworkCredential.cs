﻿using System;

namespace System.Net
{
	// Token: 0x020002E1 RID: 737
	internal class SystemNetworkCredential : NetworkCredential
	{
		// Token: 0x06001607 RID: 5639 RVA: 0x0004F6A9 File Offset: 0x0004D8A9
		private SystemNetworkCredential() : base(string.Empty, string.Empty, string.Empty)
		{
		}

		// Token: 0x06001608 RID: 5640 RVA: 0x0004F6C0 File Offset: 0x0004D8C0
		// Note: this type is marked as 'beforefieldinit'.
		static SystemNetworkCredential()
		{
		}

		// Token: 0x0400144A RID: 5194
		internal static readonly SystemNetworkCredential defaultCredential = new SystemNetworkCredential();
	}
}
