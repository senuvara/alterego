﻿using System;

namespace System.Net
{
	// Token: 0x02000303 RID: 771
	internal enum ContextAttribute
	{
		// Token: 0x04001571 RID: 5489
		Sizes,
		// Token: 0x04001572 RID: 5490
		Names,
		// Token: 0x04001573 RID: 5491
		Lifespan,
		// Token: 0x04001574 RID: 5492
		DceInfo,
		// Token: 0x04001575 RID: 5493
		StreamSizes,
		// Token: 0x04001576 RID: 5494
		Authority = 6,
		// Token: 0x04001577 RID: 5495
		PackageInfo = 10,
		// Token: 0x04001578 RID: 5496
		NegotiationInfo = 12,
		// Token: 0x04001579 RID: 5497
		UniqueBindings = 25,
		// Token: 0x0400157A RID: 5498
		EndpointBindings,
		// Token: 0x0400157B RID: 5499
		ClientSpecifiedSpn,
		// Token: 0x0400157C RID: 5500
		RemoteCertificate = 83,
		// Token: 0x0400157D RID: 5501
		LocalCertificate,
		// Token: 0x0400157E RID: 5502
		RootStore,
		// Token: 0x0400157F RID: 5503
		IssuerListInfoEx = 89,
		// Token: 0x04001580 RID: 5504
		ConnectionInfo,
		// Token: 0x04001581 RID: 5505
		UiInfo = 104
	}
}
