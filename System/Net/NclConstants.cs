﻿using System;

namespace System.Net
{
	// Token: 0x020002FC RID: 764
	internal static class NclConstants
	{
		// Token: 0x06001690 RID: 5776 RVA: 0x00051388 File Offset: 0x0004F588
		// Note: this type is marked as 'beforefieldinit'.
		static NclConstants()
		{
		}

		// Token: 0x0400152D RID: 5421
		internal static readonly object Sentinel = new object();

		// Token: 0x0400152E RID: 5422
		internal static readonly object[] EmptyObjectArray = new object[0];

		// Token: 0x0400152F RID: 5423
		internal static readonly Uri[] EmptyUriArray = new Uri[0];

		// Token: 0x04001530 RID: 5424
		internal static readonly byte[] CRLF = new byte[]
		{
			13,
			10
		};

		// Token: 0x04001531 RID: 5425
		internal static readonly byte[] ChunkTerminator = new byte[]
		{
			48,
			13,
			10,
			13,
			10
		};
	}
}
