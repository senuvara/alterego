﻿using System;

namespace System.Net
{
	// Token: 0x02000409 RID: 1033
	internal enum ReadState
	{
		// Token: 0x04001B34 RID: 6964
		None,
		// Token: 0x04001B35 RID: 6965
		Status,
		// Token: 0x04001B36 RID: 6966
		Headers,
		// Token: 0x04001B37 RID: 6967
		Content,
		// Token: 0x04001B38 RID: 6968
		Aborted
	}
}
