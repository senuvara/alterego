﻿using System;

namespace System.Net
{
	// Token: 0x0200030D RID: 781
	internal enum CertificateEncoding
	{
		// Token: 0x040015BF RID: 5567
		Zero,
		// Token: 0x040015C0 RID: 5568
		X509AsnEncoding,
		// Token: 0x040015C1 RID: 5569
		X509NdrEncoding,
		// Token: 0x040015C2 RID: 5570
		Pkcs7AsnEncoding = 65536,
		// Token: 0x040015C3 RID: 5571
		Pkcs7NdrEncoding = 131072,
		// Token: 0x040015C4 RID: 5572
		AnyAsnEncoding = 65537
	}
}
