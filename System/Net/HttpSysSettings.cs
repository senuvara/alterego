﻿using System;

namespace System.Net
{
	// Token: 0x020003BC RID: 956
	internal static class HttpSysSettings
	{
		// Token: 0x04001963 RID: 6499
		public const bool EnableNonUtf8 = true;

		// Token: 0x04001964 RID: 6500
		public const bool FavorUtf8 = true;
	}
}
