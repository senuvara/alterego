﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Net
{
	// Token: 0x020002FB RID: 763
	internal static class NclUtilities
	{
		// Token: 0x06001684 RID: 5764 RVA: 0x0005106C File Offset: 0x0004F26C
		internal static bool IsThreadPoolLow()
		{
			int num;
			int num2;
			ThreadPool.GetAvailableThreads(out num, out num2);
			return num < 2;
		}

		// Token: 0x1700049C RID: 1180
		// (get) Token: 0x06001685 RID: 5765 RVA: 0x00051086 File Offset: 0x0004F286
		internal static bool HasShutdownStarted
		{
			get
			{
				return Environment.HasShutdownStarted || AppDomain.CurrentDomain.IsFinalizingForUnload();
			}
		}

		// Token: 0x06001686 RID: 5766 RVA: 0x0005109C File Offset: 0x0004F29C
		internal static bool IsCredentialFailure(SecurityStatus error)
		{
			return error == SecurityStatus.LogonDenied || error == SecurityStatus.UnknownCredentials || error == SecurityStatus.NoImpersonation || error == SecurityStatus.NoAuthenticatingAuthority || error == SecurityStatus.UntrustedRoot || error == SecurityStatus.CertExpired || error == SecurityStatus.SmartcardLogonRequired || error == SecurityStatus.BadBinding;
		}

		// Token: 0x06001687 RID: 5767 RVA: 0x000510EC File Offset: 0x0004F2EC
		internal static bool IsClientFault(SecurityStatus error)
		{
			return error == SecurityStatus.InvalidToken || error == SecurityStatus.CannotPack || error == SecurityStatus.QopNotSupported || error == SecurityStatus.NoCredentials || error == SecurityStatus.MessageAltered || error == SecurityStatus.OutOfSequence || error == SecurityStatus.IncompleteMessage || error == SecurityStatus.IncompleteCredentials || error == SecurityStatus.WrongPrincipal || error == SecurityStatus.TimeSkew || error == SecurityStatus.IllegalMessage || error == SecurityStatus.CertUnknown || error == SecurityStatus.AlgorithmMismatch || error == SecurityStatus.SecurityQosFailed || error == SecurityStatus.UnsupportedPreauth;
		}

		// Token: 0x1700049D RID: 1181
		// (get) Token: 0x06001688 RID: 5768 RVA: 0x00051173 File Offset: 0x0004F373
		internal static ContextCallback ContextRelativeDemandCallback
		{
			get
			{
				if (NclUtilities.s_ContextRelativeDemandCallback == null)
				{
					NclUtilities.s_ContextRelativeDemandCallback = new ContextCallback(NclUtilities.DemandCallback);
				}
				return NclUtilities.s_ContextRelativeDemandCallback;
			}
		}

		// Token: 0x06001689 RID: 5769 RVA: 0x0000232D File Offset: 0x0000052D
		private static void DemandCallback(object state)
		{
		}

		// Token: 0x0600168A RID: 5770 RVA: 0x00051198 File Offset: 0x0004F398
		internal static bool GuessWhetherHostIsLoopback(string host)
		{
			string a = host.ToLowerInvariant();
			return a == "localhost" || a == "loopback";
		}

		// Token: 0x0600168B RID: 5771 RVA: 0x000511C9 File Offset: 0x0004F3C9
		internal static bool IsFatal(Exception exception)
		{
			return exception != null && (exception is OutOfMemoryException || exception is StackOverflowException || exception is ThreadAbortException);
		}

		// Token: 0x0600168C RID: 5772 RVA: 0x000511EC File Offset: 0x0004F3EC
		internal static bool IsAddressLocal(IPAddress ipAddress)
		{
			IPAddress[] localAddresses = NclUtilities.LocalAddresses;
			for (int i = 0; i < localAddresses.Length; i++)
			{
				if (ipAddress.Equals(localAddresses[i], false))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600168D RID: 5773 RVA: 0x0005121C File Offset: 0x0004F41C
		private static IPHostEntry GetLocalHost()
		{
			return Dns.GetHostByName(Dns.GetHostName());
		}

		// Token: 0x1700049E RID: 1182
		// (get) Token: 0x0600168E RID: 5774 RVA: 0x00051228 File Offset: 0x0004F428
		internal static IPAddress[] LocalAddresses
		{
			get
			{
				IPAddress[] array = NclUtilities._LocalAddresses;
				if (array != null)
				{
					return array;
				}
				object localAddressesLock = NclUtilities.LocalAddressesLock;
				IPAddress[] result;
				lock (localAddressesLock)
				{
					array = NclUtilities._LocalAddresses;
					if (array != null)
					{
						result = array;
					}
					else
					{
						List<IPAddress> list = new List<IPAddress>();
						try
						{
							IPHostEntry localHost = NclUtilities.GetLocalHost();
							if (localHost != null)
							{
								if (localHost.HostName != null)
								{
									int num = localHost.HostName.IndexOf('.');
									if (num != -1)
									{
										NclUtilities._LocalDomainName = localHost.HostName.Substring(num);
									}
								}
								IPAddress[] addressList = localHost.AddressList;
								if (addressList != null)
								{
									foreach (IPAddress item in addressList)
									{
										list.Add(item);
									}
								}
							}
						}
						catch
						{
						}
						array = new IPAddress[list.Count];
						int num2 = 0;
						foreach (IPAddress ipaddress in list)
						{
							array[num2] = ipaddress;
							num2++;
						}
						NclUtilities._LocalAddresses = array;
						result = array;
					}
				}
				return result;
			}
		}

		// Token: 0x1700049F RID: 1183
		// (get) Token: 0x0600168F RID: 5775 RVA: 0x00051368 File Offset: 0x0004F568
		private static object LocalAddressesLock
		{
			get
			{
				if (NclUtilities._LocalAddressesLock == null)
				{
					Interlocked.CompareExchange(ref NclUtilities._LocalAddressesLock, new object(), null);
				}
				return NclUtilities._LocalAddressesLock;
			}
		}

		// Token: 0x04001528 RID: 5416
		private static volatile ContextCallback s_ContextRelativeDemandCallback;

		// Token: 0x04001529 RID: 5417
		private static volatile IPAddress[] _LocalAddresses;

		// Token: 0x0400152A RID: 5418
		private static object _LocalAddressesLock;

		// Token: 0x0400152B RID: 5419
		private const int HostNameBufferLength = 256;

		// Token: 0x0400152C RID: 5420
		internal static string _LocalDomainName;
	}
}
