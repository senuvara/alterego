﻿using System;

namespace System.Net
{
	// Token: 0x02000306 RID: 774
	internal enum BufferType
	{
		// Token: 0x0400158A RID: 5514
		Empty,
		// Token: 0x0400158B RID: 5515
		Data,
		// Token: 0x0400158C RID: 5516
		Token,
		// Token: 0x0400158D RID: 5517
		Parameters,
		// Token: 0x0400158E RID: 5518
		Missing,
		// Token: 0x0400158F RID: 5519
		Extra,
		// Token: 0x04001590 RID: 5520
		Trailer,
		// Token: 0x04001591 RID: 5521
		Header,
		// Token: 0x04001592 RID: 5522
		Padding = 9,
		// Token: 0x04001593 RID: 5523
		Stream,
		// Token: 0x04001594 RID: 5524
		ChannelBindings = 14,
		// Token: 0x04001595 RID: 5525
		TargetHost = 16,
		// Token: 0x04001596 RID: 5526
		ReadOnlyFlag = -2147483648,
		// Token: 0x04001597 RID: 5527
		ReadOnlyWithChecksum = 268435456
	}
}
