﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Net
{
	// Token: 0x020003FE RID: 1022
	internal class ResponseStream : Stream
	{
		// Token: 0x06001F6F RID: 8047 RVA: 0x00072103 File Offset: 0x00070303
		internal ResponseStream(Stream stream, HttpListenerResponse response, bool ignore_errors)
		{
			this.response = response;
			this.ignore_errors = ignore_errors;
			this.stream = stream;
		}

		// Token: 0x170006BD RID: 1725
		// (get) Token: 0x06001F70 RID: 8048 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool CanRead
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170006BE RID: 1726
		// (get) Token: 0x06001F71 RID: 8049 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170006BF RID: 1727
		// (get) Token: 0x06001F72 RID: 8050 RVA: 0x00003298 File Offset: 0x00001498
		public override bool CanWrite
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170006C0 RID: 1728
		// (get) Token: 0x06001F73 RID: 8051 RVA: 0x00006740 File Offset: 0x00004940
		public override long Length
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x170006C1 RID: 1729
		// (get) Token: 0x06001F74 RID: 8052 RVA: 0x00006740 File Offset: 0x00004940
		// (set) Token: 0x06001F75 RID: 8053 RVA: 0x00006740 File Offset: 0x00004940
		public override long Position
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x06001F76 RID: 8054 RVA: 0x00072120 File Offset: 0x00070320
		public override void Close()
		{
			if (!this.disposed)
			{
				this.disposed = true;
				MemoryStream headers = this.GetHeaders(true);
				bool sendChunked = this.response.SendChunked;
				if (this.stream.CanWrite)
				{
					try
					{
						if (headers != null)
						{
							long position = headers.Position;
							if (sendChunked && !this.trailer_sent)
							{
								byte[] chunkSizeBytes = ResponseStream.GetChunkSizeBytes(0, true);
								headers.Position = headers.Length;
								headers.Write(chunkSizeBytes, 0, chunkSizeBytes.Length);
							}
							this.InternalWrite(headers.GetBuffer(), (int)position, (int)(headers.Length - position));
							this.trailer_sent = true;
						}
						else if (sendChunked && !this.trailer_sent)
						{
							byte[] chunkSizeBytes = ResponseStream.GetChunkSizeBytes(0, true);
							this.InternalWrite(chunkSizeBytes, 0, chunkSizeBytes.Length);
							this.trailer_sent = true;
						}
					}
					catch (IOException)
					{
					}
				}
				this.response.Close();
			}
		}

		// Token: 0x06001F77 RID: 8055 RVA: 0x000721FC File Offset: 0x000703FC
		private MemoryStream GetHeaders(bool closing)
		{
			object headers_lock = this.response.headers_lock;
			MemoryStream result;
			lock (headers_lock)
			{
				if (this.response.HeadersSent)
				{
					result = null;
				}
				else
				{
					MemoryStream memoryStream = new MemoryStream();
					this.response.SendHeaders(closing, memoryStream);
					result = memoryStream;
				}
			}
			return result;
		}

		// Token: 0x06001F78 RID: 8056 RVA: 0x0000232D File Offset: 0x0000052D
		public override void Flush()
		{
		}

		// Token: 0x06001F79 RID: 8057 RVA: 0x00072264 File Offset: 0x00070464
		private static byte[] GetChunkSizeBytes(int size, bool final)
		{
			string s = string.Format("{0:x}\r\n{1}", size, final ? "\r\n" : "");
			return Encoding.ASCII.GetBytes(s);
		}

		// Token: 0x06001F7A RID: 8058 RVA: 0x0007229C File Offset: 0x0007049C
		internal void InternalWrite(byte[] buffer, int offset, int count)
		{
			if (this.ignore_errors)
			{
				try
				{
					this.stream.Write(buffer, offset, count);
					return;
				}
				catch
				{
					return;
				}
			}
			this.stream.Write(buffer, offset, count);
		}

		// Token: 0x06001F7B RID: 8059 RVA: 0x000722E4 File Offset: 0x000704E4
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString());
			}
			if (count == 0)
			{
				return;
			}
			MemoryStream headers = this.GetHeaders(false);
			bool sendChunked = this.response.SendChunked;
			if (headers != null)
			{
				long position = headers.Position;
				headers.Position = headers.Length;
				if (sendChunked)
				{
					byte[] chunkSizeBytes = ResponseStream.GetChunkSizeBytes(count, false);
					headers.Write(chunkSizeBytes, 0, chunkSizeBytes.Length);
				}
				int num = Math.Min(count, 16384 - (int)headers.Position + (int)position);
				headers.Write(buffer, offset, num);
				count -= num;
				offset += num;
				this.InternalWrite(headers.GetBuffer(), (int)position, (int)(headers.Length - position));
				headers.SetLength(0L);
				headers.Capacity = 0;
			}
			else if (sendChunked)
			{
				byte[] chunkSizeBytes = ResponseStream.GetChunkSizeBytes(count, false);
				this.InternalWrite(chunkSizeBytes, 0, chunkSizeBytes.Length);
			}
			if (count > 0)
			{
				this.InternalWrite(buffer, offset, count);
			}
			if (sendChunked)
			{
				this.InternalWrite(ResponseStream.crlf, 0, 2);
			}
		}

		// Token: 0x06001F7C RID: 8060 RVA: 0x000723DC File Offset: 0x000705DC
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback cback, object state)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString());
			}
			MemoryStream headers = this.GetHeaders(false);
			bool sendChunked = this.response.SendChunked;
			if (headers != null)
			{
				long position = headers.Position;
				headers.Position = headers.Length;
				if (sendChunked)
				{
					byte[] chunkSizeBytes = ResponseStream.GetChunkSizeBytes(count, false);
					headers.Write(chunkSizeBytes, 0, chunkSizeBytes.Length);
				}
				headers.Write(buffer, offset, count);
				buffer = headers.GetBuffer();
				offset = (int)position;
				count = (int)(headers.Position - position);
			}
			else if (sendChunked)
			{
				byte[] chunkSizeBytes = ResponseStream.GetChunkSizeBytes(count, false);
				this.InternalWrite(chunkSizeBytes, 0, chunkSizeBytes.Length);
			}
			return this.stream.BeginWrite(buffer, offset, count, cback, state);
		}

		// Token: 0x06001F7D RID: 8061 RVA: 0x00072490 File Offset: 0x00070690
		public override void EndWrite(IAsyncResult ares)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString());
			}
			if (this.ignore_errors)
			{
				try
				{
					this.stream.EndWrite(ares);
					if (this.response.SendChunked)
					{
						this.stream.Write(ResponseStream.crlf, 0, 2);
					}
					return;
				}
				catch
				{
					return;
				}
			}
			this.stream.EndWrite(ares);
			if (this.response.SendChunked)
			{
				this.stream.Write(ResponseStream.crlf, 0, 2);
			}
		}

		// Token: 0x06001F7E RID: 8062 RVA: 0x00006740 File Offset: 0x00004940
		public override int Read([In] [Out] byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001F7F RID: 8063 RVA: 0x00006740 File Offset: 0x00004940
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback cback, object state)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001F80 RID: 8064 RVA: 0x00006740 File Offset: 0x00004940
		public override int EndRead(IAsyncResult ares)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001F81 RID: 8065 RVA: 0x00006740 File Offset: 0x00004940
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001F82 RID: 8066 RVA: 0x00006740 File Offset: 0x00004940
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001F83 RID: 8067 RVA: 0x0007252C File Offset: 0x0007072C
		// Note: this type is marked as 'beforefieldinit'.
		static ResponseStream()
		{
		}

		// Token: 0x04001AE5 RID: 6885
		private HttpListenerResponse response;

		// Token: 0x04001AE6 RID: 6886
		private bool ignore_errors;

		// Token: 0x04001AE7 RID: 6887
		private bool disposed;

		// Token: 0x04001AE8 RID: 6888
		private bool trailer_sent;

		// Token: 0x04001AE9 RID: 6889
		private Stream stream;

		// Token: 0x04001AEA RID: 6890
		private static byte[] crlf = new byte[]
		{
			13,
			10
		};
	}
}
