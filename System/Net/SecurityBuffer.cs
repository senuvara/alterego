﻿using System;
using System.Runtime.InteropServices;
using System.Security.Authentication.ExtendedProtection;

namespace System.Net
{
	// Token: 0x0200030B RID: 779
	internal class SecurityBuffer
	{
		// Token: 0x060016AC RID: 5804 RVA: 0x00051750 File Offset: 0x0004F950
		public SecurityBuffer(byte[] data, int offset, int size, BufferType tokentype)
		{
			this.offset = ((data == null || offset < 0) ? 0 : Math.Min(offset, data.Length));
			this.size = ((data == null || size < 0) ? 0 : Math.Min(size, data.Length - this.offset));
			this.type = tokentype;
			this.token = ((size == 0) ? null : data);
		}

		// Token: 0x060016AD RID: 5805 RVA: 0x000517B1 File Offset: 0x0004F9B1
		public SecurityBuffer(byte[] data, BufferType tokentype)
		{
			this.size = ((data == null) ? 0 : data.Length);
			this.type = tokentype;
			this.token = ((this.size == 0) ? null : data);
		}

		// Token: 0x060016AE RID: 5806 RVA: 0x000517E1 File Offset: 0x0004F9E1
		public SecurityBuffer(int size, BufferType tokentype)
		{
			this.size = size;
			this.type = tokentype;
			this.token = ((size == 0) ? null : new byte[size]);
		}

		// Token: 0x060016AF RID: 5807 RVA: 0x00051809 File Offset: 0x0004FA09
		public SecurityBuffer(ChannelBinding binding)
		{
			this.size = ((binding == null) ? 0 : binding.Size);
			this.type = BufferType.ChannelBindings;
			this.unmanagedToken = binding;
		}

		// Token: 0x040015B6 RID: 5558
		public int size;

		// Token: 0x040015B7 RID: 5559
		public BufferType type;

		// Token: 0x040015B8 RID: 5560
		public byte[] token;

		// Token: 0x040015B9 RID: 5561
		public SafeHandle unmanagedToken;

		// Token: 0x040015BA RID: 5562
		public int offset;
	}
}
