﻿using System;

namespace System.Net
{
	// Token: 0x020003FB RID: 1019
	internal class NetConfig : ICloneable
	{
		// Token: 0x06001F55 RID: 8021 RVA: 0x00071DB2 File Offset: 0x0006FFB2
		internal NetConfig()
		{
		}

		// Token: 0x06001F56 RID: 8022 RVA: 0x00071DC2 File Offset: 0x0006FFC2
		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}

		// Token: 0x04001ADC RID: 6876
		internal bool ipv6Enabled;

		// Token: 0x04001ADD RID: 6877
		internal int MaxResponseHeadersLength = 64;
	}
}
