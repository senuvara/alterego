﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Security.Authentication.ExtendedProtection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace System.Net
{
	/// <summary>Describes an incoming HTTP request to an <see cref="T:System.Net.HttpListener" /> object. This class cannot be inherited.</summary>
	// Token: 0x020003E5 RID: 997
	public sealed class HttpListenerRequest
	{
		// Token: 0x06001DE1 RID: 7649 RVA: 0x0006BFC2 File Offset: 0x0006A1C2
		internal HttpListenerRequest(HttpListenerContext context)
		{
			this.context = context;
			this.headers = new WebHeaderCollection();
			this.version = HttpVersion.Version10;
		}

		// Token: 0x06001DE2 RID: 7650 RVA: 0x0006BFE8 File Offset: 0x0006A1E8
		internal void SetRequestLine(string req)
		{
			string[] array = req.Split(HttpListenerRequest.separators, 3);
			if (array.Length != 3)
			{
				this.context.ErrorMessage = "Invalid request line (parts).";
				return;
			}
			this.method = array[0];
			foreach (char c in this.method)
			{
				int num = (int)c;
				if ((num < 65 || num > 90) && (num <= 32 || c >= '\u007f' || c == '(' || c == ')' || c == '<' || c == '<' || c == '>' || c == '@' || c == ',' || c == ';' || c == ':' || c == '\\' || c == '"' || c == '/' || c == '[' || c == ']' || c == '?' || c == '=' || c == '{' || c == '}'))
				{
					this.context.ErrorMessage = "(Invalid verb)";
					return;
				}
			}
			this.raw_url = array[1];
			if (array[2].Length != 8 || !array[2].StartsWith("HTTP/"))
			{
				this.context.ErrorMessage = "Invalid request line (version).";
				return;
			}
			try
			{
				this.version = new Version(array[2].Substring(5));
				if (this.version.Major < 1)
				{
					throw new Exception();
				}
			}
			catch
			{
				this.context.ErrorMessage = "Invalid request line (version).";
			}
		}

		// Token: 0x06001DE3 RID: 7651 RVA: 0x0006C150 File Offset: 0x0006A350
		private void CreateQueryString(string query)
		{
			if (query == null || query.Length == 0)
			{
				this.query_string = new NameValueCollection(1);
				return;
			}
			this.query_string = new NameValueCollection();
			if (query[0] == '?')
			{
				query = query.Substring(1);
			}
			foreach (string text in query.Split(new char[]
			{
				'&'
			}))
			{
				int num = text.IndexOf('=');
				if (num == -1)
				{
					this.query_string.Add(null, WebUtility.UrlDecode(text));
				}
				else
				{
					string name = WebUtility.UrlDecode(text.Substring(0, num));
					string value = WebUtility.UrlDecode(text.Substring(num + 1));
					this.query_string.Add(name, value);
				}
			}
		}

		// Token: 0x06001DE4 RID: 7652 RVA: 0x0006C208 File Offset: 0x0006A408
		private static bool MaybeUri(string s)
		{
			int num = s.IndexOf(':');
			return num != -1 && num < 10 && HttpListenerRequest.IsPredefinedScheme(s.Substring(0, num));
		}

		// Token: 0x06001DE5 RID: 7653 RVA: 0x0006C238 File Offset: 0x0006A438
		private static bool IsPredefinedScheme(string scheme)
		{
			if (scheme == null || scheme.Length < 3)
			{
				return false;
			}
			char c = scheme[0];
			if (c == 'h')
			{
				return scheme == "http" || scheme == "https";
			}
			if (c == 'f')
			{
				return scheme == "file" || scheme == "ftp";
			}
			if (c != 'n')
			{
				return (c == 'g' && scheme == "gopher") || (c == 'm' && scheme == "mailto");
			}
			c = scheme[1];
			if (c == 'e')
			{
				return scheme == "news" || scheme == "net.pipe" || scheme == "net.tcp";
			}
			return scheme == "nntp";
		}

		// Token: 0x06001DE6 RID: 7654 RVA: 0x0006C310 File Offset: 0x0006A510
		internal void FinishInitialization()
		{
			string text = this.UserHostName;
			if (this.version > HttpVersion.Version10 && (text == null || text.Length == 0))
			{
				this.context.ErrorMessage = "Invalid host name";
				return;
			}
			Uri uri = null;
			string pathAndQuery;
			if (HttpListenerRequest.MaybeUri(this.raw_url.ToLowerInvariant()) && Uri.TryCreate(this.raw_url, UriKind.Absolute, out uri))
			{
				pathAndQuery = uri.PathAndQuery;
			}
			else
			{
				pathAndQuery = this.raw_url;
			}
			if (text == null || text.Length == 0)
			{
				text = this.UserHostAddress;
			}
			if (uri != null)
			{
				text = uri.Host;
			}
			int num = text.IndexOf(':');
			if (num >= 0)
			{
				text = text.Substring(0, num);
			}
			string text2 = string.Format("{0}://{1}:{2}", this.IsSecureConnection ? "https" : "http", text, this.LocalEndPoint.Port);
			if (!Uri.TryCreate(text2 + pathAndQuery, UriKind.Absolute, out this.url))
			{
				this.context.ErrorMessage = WebUtility.HtmlEncode("Invalid url: " + text2 + pathAndQuery);
				return;
			}
			this.CreateQueryString(this.url.Query);
			this.url = HttpListenerRequestUriBuilder.GetRequestUri(this.raw_url, this.url.Scheme, this.url.Authority, this.url.LocalPath, this.url.Query);
			if (this.version >= HttpVersion.Version11)
			{
				string text3 = this.Headers["Transfer-Encoding"];
				this.is_chunked = (text3 != null && string.Compare(text3, "chunked", StringComparison.OrdinalIgnoreCase) == 0);
				if (text3 != null && !this.is_chunked)
				{
					this.context.Connection.SendError(null, 501);
					return;
				}
			}
			if (!this.is_chunked && !this.cl_set && (string.Compare(this.method, "POST", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(this.method, "PUT", StringComparison.OrdinalIgnoreCase) == 0))
			{
				this.context.Connection.SendError(null, 411);
				return;
			}
			if (string.Compare(this.Headers["Expect"], "100-continue", StringComparison.OrdinalIgnoreCase) == 0)
			{
				this.context.Connection.GetResponseStream().InternalWrite(HttpListenerRequest._100continue, 0, HttpListenerRequest._100continue.Length);
			}
		}

		// Token: 0x06001DE7 RID: 7655 RVA: 0x0006C564 File Offset: 0x0006A764
		internal static string Unquote(string str)
		{
			int num = str.IndexOf('"');
			int num2 = str.LastIndexOf('"');
			if (num >= 0 && num2 >= 0)
			{
				str = str.Substring(num + 1, num2 - 1);
			}
			return str.Trim();
		}

		// Token: 0x06001DE8 RID: 7656 RVA: 0x0006C5A0 File Offset: 0x0006A7A0
		internal void AddHeader(string header)
		{
			int num = header.IndexOf(':');
			if (num == -1 || num == 0)
			{
				this.context.ErrorMessage = "Bad Request";
				this.context.ErrorStatus = 400;
				return;
			}
			string text = header.Substring(0, num).Trim();
			string text2 = header.Substring(num + 1).Trim();
			string a = text.ToLower(CultureInfo.InvariantCulture);
			this.headers.SetInternal(text, text2);
			if (a == "accept-language")
			{
				this.user_languages = text2.Split(new char[]
				{
					','
				});
				return;
			}
			if (!(a == "accept"))
			{
				if (!(a == "content-length"))
				{
					if (!(a == "referer"))
					{
						if (!(a == "cookie"))
						{
							return;
						}
						goto IL_155;
					}
				}
				else
				{
					try
					{
						this.content_length = long.Parse(text2.Trim());
						if (this.content_length < 0L)
						{
							this.context.ErrorMessage = "Invalid Content-Length.";
						}
						this.cl_set = true;
						return;
					}
					catch
					{
						this.context.ErrorMessage = "Invalid Content-Length.";
						return;
					}
				}
				try
				{
					this.referrer = new Uri(text2);
					return;
				}
				catch
				{
					this.referrer = new Uri("http://someone.is.screwing.with.the.headers.com/");
					return;
				}
				IL_155:
				if (this.cookies == null)
				{
					this.cookies = new CookieCollection();
				}
				string[] array = text2.Split(new char[]
				{
					',',
					';'
				});
				Cookie cookie = null;
				int num2 = 0;
				string[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					string text3 = array2[i].Trim();
					if (text3.Length != 0)
					{
						if (text3.StartsWith("$Version"))
						{
							num2 = int.Parse(HttpListenerRequest.Unquote(text3.Substring(text3.IndexOf('=') + 1)));
						}
						else if (text3.StartsWith("$Path"))
						{
							if (cookie != null)
							{
								cookie.Path = text3.Substring(text3.IndexOf('=') + 1).Trim();
							}
						}
						else if (text3.StartsWith("$Domain"))
						{
							if (cookie != null)
							{
								cookie.Domain = text3.Substring(text3.IndexOf('=') + 1).Trim();
							}
						}
						else if (text3.StartsWith("$Port"))
						{
							if (cookie != null)
							{
								cookie.Port = text3.Substring(text3.IndexOf('=') + 1).Trim();
							}
						}
						else
						{
							if (cookie != null)
							{
								this.cookies.Add(cookie);
							}
							try
							{
								cookie = new Cookie();
								int num3 = text3.IndexOf('=');
								if (num3 > 0)
								{
									cookie.Name = text3.Substring(0, num3).Trim();
									cookie.Value = text3.Substring(num3 + 1).Trim();
								}
								else
								{
									cookie.Name = text3.Trim();
									cookie.Value = string.Empty;
								}
								cookie.Version = num2;
							}
							catch (CookieException)
							{
								cookie = null;
							}
						}
					}
				}
				if (cookie != null)
				{
					this.cookies.Add(cookie);
				}
				return;
			}
			this.accept_types = text2.Split(new char[]
			{
				','
			});
		}

		// Token: 0x06001DE9 RID: 7657 RVA: 0x0006C8FC File Offset: 0x0006AAFC
		internal bool FlushInput()
		{
			if (!this.HasEntityBody)
			{
				return true;
			}
			int num = 2048;
			if (this.content_length > 0L)
			{
				num = (int)Math.Min(this.content_length, (long)num);
			}
			byte[] buffer = new byte[num];
			bool result;
			for (;;)
			{
				try
				{
					IAsyncResult asyncResult = this.InputStream.BeginRead(buffer, 0, num, null, null);
					if (!asyncResult.IsCompleted && !asyncResult.AsyncWaitHandle.WaitOne(1000))
					{
						result = false;
					}
					else
					{
						if (this.InputStream.EndRead(asyncResult) > 0)
						{
							continue;
						}
						result = true;
					}
				}
				catch (ObjectDisposedException)
				{
					this.input_stream = null;
					result = true;
				}
				catch
				{
					result = false;
				}
				break;
			}
			return result;
		}

		/// <summary>Gets the MIME types accepted by the client. </summary>
		/// <returns>A <see cref="T:System.String" /> array that contains the type names specified in the request's <see langword="Accept" /> header or <see langword="null" /> if the client request did not include an <see langword="Accept" /> header.</returns>
		// Token: 0x17000624 RID: 1572
		// (get) Token: 0x06001DEA RID: 7658 RVA: 0x0006C9AC File Offset: 0x0006ABAC
		public string[] AcceptTypes
		{
			get
			{
				return this.accept_types;
			}
		}

		/// <summary>Gets an error code that identifies a problem with the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> provided by the client.</summary>
		/// <returns>An <see cref="T:System.Int32" /> value that contains a Windows error code.</returns>
		/// <exception cref="T:System.InvalidOperationException">The client certificate has not been initialized yet by a call to the <see cref="M:System.Net.HttpListenerRequest.BeginGetClientCertificate(System.AsyncCallback,System.Object)" /> or <see cref="M:System.Net.HttpListenerRequest.GetClientCertificate" /> methods-or - The operation is still in progress.</exception>
		// Token: 0x17000625 RID: 1573
		// (get) Token: 0x06001DEB RID: 7659 RVA: 0x0006C9B4 File Offset: 0x0006ABB4
		public int ClientCertificateError
		{
			get
			{
				HttpConnection connection = this.context.Connection;
				if (connection.ClientCertificate == null)
				{
					throw new InvalidOperationException("No client certificate");
				}
				int[] clientCertificateErrors = connection.ClientCertificateErrors;
				if (clientCertificateErrors != null && clientCertificateErrors.Length != 0)
				{
					return clientCertificateErrors[0];
				}
				return 0;
			}
		}

		/// <summary>Gets the content encoding that can be used with data sent with the request</summary>
		/// <returns>An <see cref="T:System.Text.Encoding" /> object suitable for use with the data in the <see cref="P:System.Net.HttpListenerRequest.InputStream" /> property.</returns>
		// Token: 0x17000626 RID: 1574
		// (get) Token: 0x06001DEC RID: 7660 RVA: 0x0006C9F1 File Offset: 0x0006ABF1
		public Encoding ContentEncoding
		{
			get
			{
				if (this.content_encoding == null)
				{
					this.content_encoding = Encoding.Default;
				}
				return this.content_encoding;
			}
		}

		/// <summary>Gets the length of the body data included in the request.</summary>
		/// <returns>The value from the request's <see langword="Content-Length" /> header. This value is -1 if the content length is not known.</returns>
		// Token: 0x17000627 RID: 1575
		// (get) Token: 0x06001DED RID: 7661 RVA: 0x0006CA0C File Offset: 0x0006AC0C
		public long ContentLength64
		{
			get
			{
				if (!this.is_chunked)
				{
					return this.content_length;
				}
				return -1L;
			}
		}

		/// <summary>Gets the MIME type of the body data included in the request.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the text of the request's <see langword="Content-Type" /> header.</returns>
		// Token: 0x17000628 RID: 1576
		// (get) Token: 0x06001DEE RID: 7662 RVA: 0x0006CA1F File Offset: 0x0006AC1F
		public string ContentType
		{
			get
			{
				return this.headers["content-type"];
			}
		}

		/// <summary>Gets the cookies sent with the request.</summary>
		/// <returns>A <see cref="T:System.Net.CookieCollection" /> that contains cookies that accompany the request. This property returns an empty collection if the request does not contain cookies.</returns>
		// Token: 0x17000629 RID: 1577
		// (get) Token: 0x06001DEF RID: 7663 RVA: 0x0006CA31 File Offset: 0x0006AC31
		public CookieCollection Cookies
		{
			get
			{
				if (this.cookies == null)
				{
					this.cookies = new CookieCollection();
				}
				return this.cookies;
			}
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the request has associated body data.</summary>
		/// <returns>
		///     <see langword="true" /> if the request has associated body data; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700062A RID: 1578
		// (get) Token: 0x06001DF0 RID: 7664 RVA: 0x0006CA4C File Offset: 0x0006AC4C
		public bool HasEntityBody
		{
			get
			{
				return this.content_length > 0L || this.is_chunked;
			}
		}

		/// <summary>Gets the collection of header name/value pairs sent in the request.</summary>
		/// <returns>A <see cref="T:System.Net.WebHeaderCollection" /> that contains the HTTP headers included in the request.</returns>
		// Token: 0x1700062B RID: 1579
		// (get) Token: 0x06001DF1 RID: 7665 RVA: 0x0006CA60 File Offset: 0x0006AC60
		public NameValueCollection Headers
		{
			get
			{
				return this.headers;
			}
		}

		/// <summary>Gets the HTTP method specified by the client. </summary>
		/// <returns>A <see cref="T:System.String" /> that contains the method used in the request.</returns>
		// Token: 0x1700062C RID: 1580
		// (get) Token: 0x06001DF2 RID: 7666 RVA: 0x0006CA68 File Offset: 0x0006AC68
		public string HttpMethod
		{
			get
			{
				return this.method;
			}
		}

		/// <summary>Gets a stream that contains the body data sent by the client.</summary>
		/// <returns>A readable <see cref="T:System.IO.Stream" /> object that contains the bytes sent by the client in the body of the request. This property returns <see cref="F:System.IO.Stream.Null" /> if no data is sent with the request.</returns>
		// Token: 0x1700062D RID: 1581
		// (get) Token: 0x06001DF3 RID: 7667 RVA: 0x0006CA70 File Offset: 0x0006AC70
		public Stream InputStream
		{
			get
			{
				if (this.input_stream == null)
				{
					if (this.is_chunked || this.content_length > 0L)
					{
						this.input_stream = this.context.Connection.GetRequestStream(this.is_chunked, this.content_length);
					}
					else
					{
						this.input_stream = Stream.Null;
					}
				}
				return this.input_stream;
			}
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the client sending this request is authenticated.</summary>
		/// <returns>
		///     <see langword="true" /> if the client was authenticated; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700062E RID: 1582
		// (get) Token: 0x06001DF4 RID: 7668 RVA: 0x00005AFA File Offset: 0x00003CFA
		[MonoTODO("Always returns false")]
		public bool IsAuthenticated
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the request is sent from the local computer.</summary>
		/// <returns>
		///     <see langword="true" /> if the request originated on the same computer as the <see cref="T:System.Net.HttpListener" /> object that provided the request; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700062F RID: 1583
		// (get) Token: 0x06001DF5 RID: 7669 RVA: 0x0006CACC File Offset: 0x0006ACCC
		public bool IsLocal
		{
			get
			{
				return this.LocalEndPoint.Address.Equals(this.RemoteEndPoint.Address);
			}
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the TCP connection used to send the request is using the Secure Sockets Layer (SSL) protocol.</summary>
		/// <returns>
		///     <see langword="true" /> if the TCP connection is using SSL; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000630 RID: 1584
		// (get) Token: 0x06001DF6 RID: 7670 RVA: 0x0006CAE9 File Offset: 0x0006ACE9
		public bool IsSecureConnection
		{
			get
			{
				return this.context.Connection.IsSecure;
			}
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the client requests a persistent connection.</summary>
		/// <returns>
		///     <see langword="true" /> if the connection should be kept open; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000631 RID: 1585
		// (get) Token: 0x06001DF7 RID: 7671 RVA: 0x0006CAFC File Offset: 0x0006ACFC
		public bool KeepAlive
		{
			get
			{
				if (this.ka_set)
				{
					return this.keep_alive;
				}
				this.ka_set = true;
				string text = this.headers["Connection"];
				if (!string.IsNullOrEmpty(text))
				{
					this.keep_alive = (string.Compare(text, "keep-alive", StringComparison.OrdinalIgnoreCase) == 0);
				}
				else if (this.version == HttpVersion.Version11)
				{
					this.keep_alive = true;
				}
				else
				{
					text = this.headers["keep-alive"];
					if (!string.IsNullOrEmpty(text))
					{
						this.keep_alive = (string.Compare(text, "closed", StringComparison.OrdinalIgnoreCase) != 0);
					}
				}
				return this.keep_alive;
			}
		}

		/// <summary>Get the server IP address and port number to which the request is directed.</summary>
		/// <returns>An <see cref="T:System.Net.IPEndPoint" /> that represents the IP address that the request is sent to.</returns>
		// Token: 0x17000632 RID: 1586
		// (get) Token: 0x06001DF8 RID: 7672 RVA: 0x0006CB9E File Offset: 0x0006AD9E
		public IPEndPoint LocalEndPoint
		{
			get
			{
				return this.context.Connection.LocalEndPoint;
			}
		}

		/// <summary>Gets the HTTP version used by the requesting client.</summary>
		/// <returns>A <see cref="T:System.Version" /> that identifies the client's version of HTTP.</returns>
		// Token: 0x17000633 RID: 1587
		// (get) Token: 0x06001DF9 RID: 7673 RVA: 0x0006CBB0 File Offset: 0x0006ADB0
		public Version ProtocolVersion
		{
			get
			{
				return this.version;
			}
		}

		/// <summary>Gets the query string included in the request.</summary>
		/// <returns>A <see cref="T:System.Collections.Specialized.NameValueCollection" /> object that contains the query data included in the request <see cref="P:System.Net.HttpListenerRequest.Url" />.</returns>
		// Token: 0x17000634 RID: 1588
		// (get) Token: 0x06001DFA RID: 7674 RVA: 0x0006CBB8 File Offset: 0x0006ADB8
		public NameValueCollection QueryString
		{
			get
			{
				return this.query_string;
			}
		}

		/// <summary>Gets the URL information (without the host and port) requested by the client.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the raw URL for this request.</returns>
		// Token: 0x17000635 RID: 1589
		// (get) Token: 0x06001DFB RID: 7675 RVA: 0x0006CBC0 File Offset: 0x0006ADC0
		public string RawUrl
		{
			get
			{
				return this.raw_url;
			}
		}

		/// <summary>Gets the client IP address and port number from which the request originated.</summary>
		/// <returns>An <see cref="T:System.Net.IPEndPoint" /> that represents the IP address and port number from which the request originated.</returns>
		// Token: 0x17000636 RID: 1590
		// (get) Token: 0x06001DFC RID: 7676 RVA: 0x0006CBC8 File Offset: 0x0006ADC8
		public IPEndPoint RemoteEndPoint
		{
			get
			{
				return this.context.Connection.RemoteEndPoint;
			}
		}

		/// <summary>Gets the request identifier of the incoming HTTP request.</summary>
		/// <returns>A <see cref="T:System.Guid" /> object that contains the identifier of the HTTP request.</returns>
		// Token: 0x17000637 RID: 1591
		// (get) Token: 0x06001DFD RID: 7677 RVA: 0x0006CBDA File Offset: 0x0006ADDA
		[MonoTODO("Always returns Guid.Empty")]
		public Guid RequestTraceIdentifier
		{
			get
			{
				return Guid.Empty;
			}
		}

		/// <summary>Gets the <see cref="T:System.Uri" /> object requested by the client.</summary>
		/// <returns>A <see cref="T:System.Uri" /> object that identifies the resource requested by the client.</returns>
		// Token: 0x17000638 RID: 1592
		// (get) Token: 0x06001DFE RID: 7678 RVA: 0x0006CBE1 File Offset: 0x0006ADE1
		public Uri Url
		{
			get
			{
				return this.url;
			}
		}

		/// <summary>Gets the Uniform Resource Identifier (URI) of the resource that referred the client to the server.</summary>
		/// <returns>A <see cref="T:System.Uri" /> object that contains the text of the request's <see cref="F:System.Net.HttpRequestHeader.Referer" /> header, or <see langword="null" /> if the header was not included in the request.</returns>
		// Token: 0x17000639 RID: 1593
		// (get) Token: 0x06001DFF RID: 7679 RVA: 0x0006CBE9 File Offset: 0x0006ADE9
		public Uri UrlReferrer
		{
			get
			{
				return this.referrer;
			}
		}

		/// <summary>Gets the user agent presented by the client.</summary>
		/// <returns>A <see cref="T:System.String" /> object that contains the text of the request's <see langword="User-Agent" /> header.</returns>
		// Token: 0x1700063A RID: 1594
		// (get) Token: 0x06001E00 RID: 7680 RVA: 0x0006CBF1 File Offset: 0x0006ADF1
		public string UserAgent
		{
			get
			{
				return this.headers["user-agent"];
			}
		}

		/// <summary>Gets the server IP address and port number to which the request is directed.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the host address information.</returns>
		// Token: 0x1700063B RID: 1595
		// (get) Token: 0x06001E01 RID: 7681 RVA: 0x0006CC03 File Offset: 0x0006AE03
		public string UserHostAddress
		{
			get
			{
				return this.LocalEndPoint.ToString();
			}
		}

		/// <summary>Gets the DNS name and, if provided, the port number specified by the client.</summary>
		/// <returns>A <see cref="T:System.String" /> value that contains the text of the request's <see langword="Host" /> header.</returns>
		// Token: 0x1700063C RID: 1596
		// (get) Token: 0x06001E02 RID: 7682 RVA: 0x0006CC10 File Offset: 0x0006AE10
		public string UserHostName
		{
			get
			{
				return this.headers["host"];
			}
		}

		/// <summary>Gets the natural languages that are preferred for the response.</summary>
		/// <returns>A <see cref="T:System.String" /> array that contains the languages specified in the request's <see cref="F:System.Net.HttpRequestHeader.AcceptLanguage" /> header or <see langword="null" /> if the client request did not include an <see cref="F:System.Net.HttpRequestHeader.AcceptLanguage" /> header.</returns>
		// Token: 0x1700063D RID: 1597
		// (get) Token: 0x06001E03 RID: 7683 RVA: 0x0006CC22 File Offset: 0x0006AE22
		public string[] UserLanguages
		{
			get
			{
				return this.user_languages;
			}
		}

		/// <summary>Begins an asynchronous request for the client's X.509 v.3 certificate.</summary>
		/// <param name="requestCallback">An <see cref="T:System.AsyncCallback" /> delegate that references the method to invoke when the operation is complete.</param>
		/// <param name="state">A user-defined object that contains information about the operation. This object is passed to the callback delegate when the operation completes.</param>
		/// <returns>An <see cref="T:System.IAsyncResult" /> that indicates the status of the operation.</returns>
		// Token: 0x06001E04 RID: 7684 RVA: 0x0006CC2A File Offset: 0x0006AE2A
		public IAsyncResult BeginGetClientCertificate(AsyncCallback requestCallback, object state)
		{
			if (this.gcc_delegate == null)
			{
				this.gcc_delegate = new HttpListenerRequest.GCCDelegate(this.GetClientCertificate);
			}
			return this.gcc_delegate.BeginInvoke(requestCallback, state);
		}

		/// <summary>Ends an asynchronous request for the client's X.509 v.3 certificate.</summary>
		/// <param name="asyncResult">The pending request for the certificate.</param>
		/// <returns>The <see cref="T:System.IAsyncResult" /> object that is returned when the operation started.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="asyncResult" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="asyncResult" /> was not obtained by calling <see cref="M:System.Net.HttpListenerRequest.BeginGetClientCertificate(System.AsyncCallback,System.Object)" /><paramref name="e." /></exception>
		/// <exception cref="T:System.InvalidOperationException">This method was already called for the operation identified by <paramref name="asyncResult" />. </exception>
		// Token: 0x06001E05 RID: 7685 RVA: 0x0006CC53 File Offset: 0x0006AE53
		public X509Certificate2 EndGetClientCertificate(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			if (this.gcc_delegate == null)
			{
				throw new InvalidOperationException();
			}
			return this.gcc_delegate.EndInvoke(asyncResult);
		}

		/// <summary>Retrieves the client's X.509 v.3 certificate.</summary>
		/// <returns>A <see cref="N:System.Security.Cryptography.X509Certificates" /> object that contains the client's X.509 v.3 certificate.</returns>
		/// <exception cref="T:System.InvalidOperationException">A call to this method to retrieve the client's X.509 v.3 certificate is in progress and therefore another call to this method cannot be made.</exception>
		// Token: 0x06001E06 RID: 7686 RVA: 0x0006CC7D File Offset: 0x0006AE7D
		public X509Certificate2 GetClientCertificate()
		{
			return this.context.Connection.ClientCertificate;
		}

		/// <summary>Gets the Service Provider Name (SPN) that the client sent on the request.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the SPN the client sent on the request. </returns>
		// Token: 0x1700063E RID: 1598
		// (get) Token: 0x06001E07 RID: 7687 RVA: 0x00008B3F File Offset: 0x00006D3F
		[MonoTODO]
		public string ServiceName
		{
			get
			{
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Net.TransportContext" /> for the client request.</summary>
		/// <returns>A <see cref="T:System.Net.TransportContext" /> object for the client request.</returns>
		// Token: 0x1700063F RID: 1599
		// (get) Token: 0x06001E08 RID: 7688 RVA: 0x0006CC8F File Offset: 0x0006AE8F
		public TransportContext TransportContext
		{
			get
			{
				return new HttpListenerRequest.Context();
			}
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the TCP connection was  a WebSocket request.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.
		///     <see langword="true" /> if the TCP connection is a WebSocket request; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000640 RID: 1600
		// (get) Token: 0x06001E09 RID: 7689 RVA: 0x00005AFA File Offset: 0x00003CFA
		[MonoTODO]
		public bool IsWebSocketRequest
		{
			get
			{
				return false;
			}
		}

		/// <summary>Retrieves the client's X.509 v.3 certificate as an asynchronous operation.</summary>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="N:System.Security.Cryptography.X509Certificates" /> object that contains the client's X.509 v.3 certificate.</returns>
		// Token: 0x06001E0A RID: 7690 RVA: 0x0006CC96 File Offset: 0x0006AE96
		public Task<X509Certificate2> GetClientCertificateAsync()
		{
			return Task<X509Certificate2>.Factory.FromAsync(new Func<AsyncCallback, object, IAsyncResult>(this.BeginGetClientCertificate), new Func<IAsyncResult, X509Certificate2>(this.EndGetClientCertificate), null);
		}

		// Token: 0x06001E0B RID: 7691 RVA: 0x0006CCBB File Offset: 0x0006AEBB
		// Note: this type is marked as 'beforefieldinit'.
		static HttpListenerRequest()
		{
		}

		// Token: 0x06001E0C RID: 7692 RVA: 0x000092E2 File Offset: 0x000074E2
		internal HttpListenerRequest()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001A2B RID: 6699
		private string[] accept_types;

		// Token: 0x04001A2C RID: 6700
		private Encoding content_encoding;

		// Token: 0x04001A2D RID: 6701
		private long content_length;

		// Token: 0x04001A2E RID: 6702
		private bool cl_set;

		// Token: 0x04001A2F RID: 6703
		private CookieCollection cookies;

		// Token: 0x04001A30 RID: 6704
		private WebHeaderCollection headers;

		// Token: 0x04001A31 RID: 6705
		private string method;

		// Token: 0x04001A32 RID: 6706
		private Stream input_stream;

		// Token: 0x04001A33 RID: 6707
		private Version version;

		// Token: 0x04001A34 RID: 6708
		private NameValueCollection query_string;

		// Token: 0x04001A35 RID: 6709
		private string raw_url;

		// Token: 0x04001A36 RID: 6710
		private Uri url;

		// Token: 0x04001A37 RID: 6711
		private Uri referrer;

		// Token: 0x04001A38 RID: 6712
		private string[] user_languages;

		// Token: 0x04001A39 RID: 6713
		private HttpListenerContext context;

		// Token: 0x04001A3A RID: 6714
		private bool is_chunked;

		// Token: 0x04001A3B RID: 6715
		private bool ka_set;

		// Token: 0x04001A3C RID: 6716
		private bool keep_alive;

		// Token: 0x04001A3D RID: 6717
		private HttpListenerRequest.GCCDelegate gcc_delegate;

		// Token: 0x04001A3E RID: 6718
		private static byte[] _100continue = Encoding.ASCII.GetBytes("HTTP/1.1 100 Continue\r\n\r\n");

		// Token: 0x04001A3F RID: 6719
		private static char[] separators = new char[]
		{
			' '
		};

		// Token: 0x020003E6 RID: 998
		private class Context : TransportContext
		{
			// Token: 0x06001E0D RID: 7693 RVA: 0x000068D7 File Offset: 0x00004AD7
			public override ChannelBinding GetChannelBinding(ChannelBindingKind kind)
			{
				throw new NotImplementedException();
			}

			// Token: 0x06001E0E RID: 7694 RVA: 0x0006CCE1 File Offset: 0x0006AEE1
			public Context()
			{
			}
		}

		// Token: 0x020003E7 RID: 999
		// (Invoke) Token: 0x06001E10 RID: 7696
		private delegate X509Certificate2 GCCDelegate();
	}
}
