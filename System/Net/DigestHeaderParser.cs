﻿using System;

namespace System.Net
{
	// Token: 0x020003C8 RID: 968
	internal class DigestHeaderParser
	{
		// Token: 0x06001C4F RID: 7247 RVA: 0x00066220 File Offset: 0x00064420
		public DigestHeaderParser(string header)
		{
			this.header = header.Trim();
		}

		// Token: 0x170005C0 RID: 1472
		// (get) Token: 0x06001C50 RID: 7248 RVA: 0x00066246 File Offset: 0x00064446
		public string Realm
		{
			get
			{
				return this.values[0];
			}
		}

		// Token: 0x170005C1 RID: 1473
		// (get) Token: 0x06001C51 RID: 7249 RVA: 0x00066250 File Offset: 0x00064450
		public string Opaque
		{
			get
			{
				return this.values[1];
			}
		}

		// Token: 0x170005C2 RID: 1474
		// (get) Token: 0x06001C52 RID: 7250 RVA: 0x0006625A File Offset: 0x0006445A
		public string Nonce
		{
			get
			{
				return this.values[2];
			}
		}

		// Token: 0x170005C3 RID: 1475
		// (get) Token: 0x06001C53 RID: 7251 RVA: 0x00066264 File Offset: 0x00064464
		public string Algorithm
		{
			get
			{
				return this.values[3];
			}
		}

		// Token: 0x170005C4 RID: 1476
		// (get) Token: 0x06001C54 RID: 7252 RVA: 0x0006626E File Offset: 0x0006446E
		public string QOP
		{
			get
			{
				return this.values[4];
			}
		}

		// Token: 0x06001C55 RID: 7253 RVA: 0x00066278 File Offset: 0x00064478
		public bool Parse()
		{
			if (!this.header.ToLower().StartsWith("digest "))
			{
				return false;
			}
			this.pos = 6;
			this.length = this.header.Length;
			while (this.pos < this.length)
			{
				string value;
				string text;
				if (!this.GetKeywordAndValue(out value, out text))
				{
					return false;
				}
				this.SkipWhitespace();
				if (this.pos < this.length && this.header[this.pos] == ',')
				{
					this.pos++;
				}
				int num = Array.IndexOf<string>(DigestHeaderParser.keywords, value);
				if (num != -1)
				{
					if (this.values[num] != null)
					{
						return false;
					}
					this.values[num] = text;
				}
			}
			return this.Realm != null && this.Nonce != null;
		}

		// Token: 0x06001C56 RID: 7254 RVA: 0x00066344 File Offset: 0x00064544
		private void SkipWhitespace()
		{
			char c = ' ';
			while (this.pos < this.length && (c == ' ' || c == '\t' || c == '\r' || c == '\n'))
			{
				string text = this.header;
				int num = this.pos;
				this.pos = num + 1;
				c = text[num];
			}
			this.pos--;
		}

		// Token: 0x06001C57 RID: 7255 RVA: 0x000663A4 File Offset: 0x000645A4
		private string GetKey()
		{
			this.SkipWhitespace();
			int num = this.pos;
			while (this.pos < this.length && this.header[this.pos] != '=')
			{
				this.pos++;
			}
			return this.header.Substring(num, this.pos - num).Trim().ToLower();
		}

		// Token: 0x06001C58 RID: 7256 RVA: 0x00066410 File Offset: 0x00064610
		private bool GetKeywordAndValue(out string key, out string value)
		{
			key = null;
			value = null;
			key = this.GetKey();
			if (this.pos >= this.length)
			{
				return false;
			}
			this.SkipWhitespace();
			if (this.pos + 1 < this.length)
			{
				string text = this.header;
				int num = this.pos;
				this.pos = num + 1;
				if (text[num] == '=')
				{
					this.SkipWhitespace();
					if (this.pos + 1 >= this.length)
					{
						return false;
					}
					bool flag = false;
					if (this.header[this.pos] == '"')
					{
						this.pos++;
						flag = true;
					}
					int num2 = this.pos;
					if (flag)
					{
						this.pos = this.header.IndexOf('"', this.pos);
						if (this.pos == -1)
						{
							return false;
						}
					}
					else
					{
						do
						{
							char c = this.header[this.pos];
							if (c == ',' || c == ' ' || c == '\t' || c == '\r' || c == '\n')
							{
								break;
							}
							num = this.pos + 1;
							this.pos = num;
						}
						while (num < this.length);
						if (this.pos >= this.length && num2 == this.pos)
						{
							return false;
						}
					}
					value = this.header.Substring(num2, this.pos - num2);
					this.pos += (flag ? 2 : 1);
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001C59 RID: 7257 RVA: 0x00066568 File Offset: 0x00064768
		// Note: this type is marked as 'beforefieldinit'.
		static DigestHeaderParser()
		{
		}

		// Token: 0x0400197D RID: 6525
		private string header;

		// Token: 0x0400197E RID: 6526
		private int length;

		// Token: 0x0400197F RID: 6527
		private int pos;

		// Token: 0x04001980 RID: 6528
		private static string[] keywords = new string[]
		{
			"realm",
			"opaque",
			"nonce",
			"algorithm",
			"qop"
		};

		// Token: 0x04001981 RID: 6529
		private string[] values = new string[DigestHeaderParser.keywords.Length];
	}
}
