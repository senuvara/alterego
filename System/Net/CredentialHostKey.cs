﻿using System;
using System.Globalization;

namespace System.Net
{
	// Token: 0x020002E2 RID: 738
	internal class CredentialHostKey
	{
		// Token: 0x06001609 RID: 5641 RVA: 0x0004F6CC File Offset: 0x0004D8CC
		internal CredentialHostKey(string host, int port, string authenticationType)
		{
			this.Host = host;
			this.Port = port;
			this.AuthenticationType = authenticationType;
		}

		// Token: 0x0600160A RID: 5642 RVA: 0x0004F6E9 File Offset: 0x0004D8E9
		internal bool Match(string host, int port, string authenticationType)
		{
			return host != null && authenticationType != null && string.Compare(authenticationType, this.AuthenticationType, StringComparison.OrdinalIgnoreCase) == 0 && string.Compare(this.Host, host, StringComparison.OrdinalIgnoreCase) == 0 && port == this.Port;
		}

		// Token: 0x0600160B RID: 5643 RVA: 0x0004F724 File Offset: 0x0004D924
		public override int GetHashCode()
		{
			if (!this.m_ComputedHashCode)
			{
				this.m_HashCode = this.AuthenticationType.ToUpperInvariant().GetHashCode() + this.Host.ToUpperInvariant().GetHashCode() + this.Port.GetHashCode();
				this.m_ComputedHashCode = true;
			}
			return this.m_HashCode;
		}

		// Token: 0x0600160C RID: 5644 RVA: 0x0004F77C File Offset: 0x0004D97C
		public override bool Equals(object comparand)
		{
			CredentialHostKey credentialHostKey = comparand as CredentialHostKey;
			return comparand != null && (string.Compare(this.AuthenticationType, credentialHostKey.AuthenticationType, StringComparison.OrdinalIgnoreCase) == 0 && string.Compare(this.Host, credentialHostKey.Host, StringComparison.OrdinalIgnoreCase) == 0) && this.Port == credentialHostKey.Port;
		}

		// Token: 0x0600160D RID: 5645 RVA: 0x0004F7D0 File Offset: 0x0004D9D0
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				"[",
				this.Host.Length.ToString(NumberFormatInfo.InvariantInfo),
				"]:",
				this.Host,
				":",
				this.Port.ToString(NumberFormatInfo.InvariantInfo),
				":",
				ValidationHelper.ToString(this.AuthenticationType)
			});
		}

		// Token: 0x0400144B RID: 5195
		internal string Host;

		// Token: 0x0400144C RID: 5196
		internal string AuthenticationType;

		// Token: 0x0400144D RID: 5197
		internal int Port;

		// Token: 0x0400144E RID: 5198
		private int m_HashCode;

		// Token: 0x0400144F RID: 5199
		private bool m_ComputedHashCode;
	}
}
