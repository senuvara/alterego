﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Net.Cache;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net
{
	/// <summary>Provides common methods for sending data to and receiving data from a resource identified by a URI.</summary>
	// Token: 0x0200038F RID: 911
	[ComVisible(true)]
	public class WebClient : Component
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.WebClient" /> class.</summary>
		// Token: 0x06001A6E RID: 6766 RVA: 0x000603E1 File Offset: 0x0005E5E1
		public WebClient()
		{
			if (base.GetType() == typeof(WebClient))
			{
				GC.SuppressFinalize(this);
			}
		}

		// Token: 0x06001A6F RID: 6767 RVA: 0x0006041C File Offset: 0x0005E61C
		private void InitWebClientAsync()
		{
			if (!this.m_InitWebClientAsync)
			{
				this.openReadOperationCompleted = new SendOrPostCallback(this.OpenReadOperationCompleted);
				this.openWriteOperationCompleted = new SendOrPostCallback(this.OpenWriteOperationCompleted);
				this.downloadStringOperationCompleted = new SendOrPostCallback(this.DownloadStringOperationCompleted);
				this.downloadDataOperationCompleted = new SendOrPostCallback(this.DownloadDataOperationCompleted);
				this.downloadFileOperationCompleted = new SendOrPostCallback(this.DownloadFileOperationCompleted);
				this.uploadStringOperationCompleted = new SendOrPostCallback(this.UploadStringOperationCompleted);
				this.uploadDataOperationCompleted = new SendOrPostCallback(this.UploadDataOperationCompleted);
				this.uploadFileOperationCompleted = new SendOrPostCallback(this.UploadFileOperationCompleted);
				this.uploadValuesOperationCompleted = new SendOrPostCallback(this.UploadValuesOperationCompleted);
				this.reportDownloadProgressChanged = new SendOrPostCallback(this.ReportDownloadProgressChanged);
				this.reportUploadProgressChanged = new SendOrPostCallback(this.ReportUploadProgressChanged);
				this.m_Progress = new WebClient.ProgressData();
				this.m_InitWebClientAsync = true;
			}
		}

		// Token: 0x06001A70 RID: 6768 RVA: 0x0006050C File Offset: 0x0005E70C
		private void ClearWebClientState()
		{
			if (this.AnotherCallInProgress(Interlocked.Increment(ref this.m_CallNesting)))
			{
				this.CompleteWebClientState();
				throw new NotSupportedException(SR.GetString("WebClient does not support concurrent I/O operations."));
			}
			this.m_ContentLength = -1L;
			this.m_WebResponse = null;
			this.m_WebRequest = null;
			this.m_Method = null;
			this.m_Cancelled = false;
			if (this.m_Progress != null)
			{
				this.m_Progress.Reset();
			}
		}

		// Token: 0x06001A71 RID: 6769 RVA: 0x00060579 File Offset: 0x0005E779
		private void CompleteWebClientState()
		{
			Interlocked.Decrement(ref this.m_CallNesting);
		}

		/// <summary>Gets or sets a value that indicates whether to buffer the data read from the Internet resource for a <see cref="T:System.Net.WebClient" /> instance.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.
		///     <see langword="true" /> to enable buffering of the data received from the Internet resource; <see langword="false" /> to disable buffering. The default is <see langword="true" />.</returns>
		// Token: 0x17000585 RID: 1413
		// (get) Token: 0x06001A72 RID: 6770 RVA: 0x00060587 File Offset: 0x0005E787
		// (set) Token: 0x06001A73 RID: 6771 RVA: 0x0006058F File Offset: 0x0005E78F
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("This API supports the .NET Framework infrastructure and is not intended to be used directly from your code.", true)]
		public bool AllowReadStreamBuffering
		{
			[CompilerGenerated]
			get
			{
				return this.<AllowReadStreamBuffering>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<AllowReadStreamBuffering>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether to buffer the data written to the Internet resource for a <see cref="T:System.Net.WebClient" /> instance.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.
		///     <see langword="true" /> to enable buffering of the data written to the Internet resource; <see langword="false" /> to disable buffering. The default is <see langword="true" />.</returns>
		// Token: 0x17000586 RID: 1414
		// (get) Token: 0x06001A74 RID: 6772 RVA: 0x00060598 File Offset: 0x0005E798
		// (set) Token: 0x06001A75 RID: 6773 RVA: 0x000605A0 File Offset: 0x0005E7A0
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("This API supports the .NET Framework infrastructure and is not intended to be used directly from your code.", true)]
		public bool AllowWriteStreamBuffering
		{
			[CompilerGenerated]
			get
			{
				return this.<AllowWriteStreamBuffering>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<AllowWriteStreamBuffering>k__BackingField = value;
			}
		}

		/// <summary>Occurs when an asynchronous operation to write data to a resource using a write stream is closed.</summary>
		// Token: 0x14000030 RID: 48
		// (add) Token: 0x06001A76 RID: 6774 RVA: 0x0000232D File Offset: 0x0000052D
		// (remove) Token: 0x06001A77 RID: 6775 RVA: 0x0000232D File Offset: 0x0000052D
		[Obsolete("This API supports the .NET Framework infrastructure and is not intended to be used directly from your code.", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public event WriteStreamClosedEventHandler WriteStreamClosed
		{
			add
			{
			}
			remove
			{
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.WriteStreamClosed" /> event.</summary>
		/// <param name="e">A <see cref="T:System.Net.WriteStreamClosedEventArgs" />  object containing event data.</param>
		// Token: 0x06001A78 RID: 6776 RVA: 0x0000232D File Offset: 0x0000052D
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("This API supports the .NET Framework infrastructure and is not intended to be used directly from your code.", true)]
		protected virtual void OnWriteStreamClosed(WriteStreamClosedEventArgs e)
		{
		}

		/// <summary>Gets and sets the <see cref="T:System.Text.Encoding" /> used to upload and download strings.</summary>
		/// <returns>A <see cref="T:System.Text.Encoding" /> that is used to encode strings. The default value of this property is the encoding returned by <see cref="P:System.Text.Encoding.Default" />.</returns>
		// Token: 0x17000587 RID: 1415
		// (get) Token: 0x06001A79 RID: 6777 RVA: 0x000605A9 File Offset: 0x0005E7A9
		// (set) Token: 0x06001A7A RID: 6778 RVA: 0x000605B1 File Offset: 0x0005E7B1
		public Encoding Encoding
		{
			get
			{
				return this.m_Encoding;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Encoding");
				}
				this.m_Encoding = value;
			}
		}

		/// <summary>Gets or sets the base URI for requests made by a <see cref="T:System.Net.WebClient" />.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the base URI for requests made by a <see cref="T:System.Net.WebClient" /> or <see cref="F:System.String.Empty" /> if no base address has been specified.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <see cref="P:System.Net.WebClient.BaseAddress" /> is set to an invalid URI. The inner exception may contain information that will help you locate the error.</exception>
		// Token: 0x17000588 RID: 1416
		// (get) Token: 0x06001A7B RID: 6779 RVA: 0x000605C8 File Offset: 0x0005E7C8
		// (set) Token: 0x06001A7C RID: 6780 RVA: 0x000605EC File Offset: 0x0005E7EC
		public string BaseAddress
		{
			get
			{
				if (!(this.m_baseAddress == null))
				{
					return this.m_baseAddress.ToString();
				}
				return string.Empty;
			}
			set
			{
				if (value == null || value.Length == 0)
				{
					this.m_baseAddress = null;
					return;
				}
				try
				{
					this.m_baseAddress = new Uri(value);
				}
				catch (UriFormatException innerException)
				{
					throw new ArgumentException(SR.GetString("The specified value is not a valid base address."), "value", innerException);
				}
			}
		}

		/// <summary>Gets or sets the network credentials that are sent to the host and used to authenticate the request.</summary>
		/// <returns>An <see cref="T:System.Net.ICredentials" /> containing the authentication credentials for the request. The default is <see langword="null" />.</returns>
		// Token: 0x17000589 RID: 1417
		// (get) Token: 0x06001A7D RID: 6781 RVA: 0x00060644 File Offset: 0x0005E844
		// (set) Token: 0x06001A7E RID: 6782 RVA: 0x0006064C File Offset: 0x0005E84C
		public ICredentials Credentials
		{
			get
			{
				return this.m_credentials;
			}
			set
			{
				this.m_credentials = value;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that controls whether the <see cref="P:System.Net.CredentialCache.DefaultCredentials" /> are sent with requests.</summary>
		/// <returns>
		///     <see langword="true" /> if the default credentials are used; otherwise <see langword="false" />. The default value is <see langword="false" />.</returns>
		// Token: 0x1700058A RID: 1418
		// (get) Token: 0x06001A7F RID: 6783 RVA: 0x00060655 File Offset: 0x0005E855
		// (set) Token: 0x06001A80 RID: 6784 RVA: 0x00060667 File Offset: 0x0005E867
		public bool UseDefaultCredentials
		{
			get
			{
				return this.m_credentials is SystemNetworkCredential;
			}
			set
			{
				this.m_credentials = (value ? CredentialCache.DefaultCredentials : null);
			}
		}

		/// <summary>Gets or sets a collection of header name/value pairs associated with the request.</summary>
		/// <returns>A <see cref="T:System.Net.WebHeaderCollection" /> containing header name/value pairs associated with this request.</returns>
		// Token: 0x1700058B RID: 1419
		// (get) Token: 0x06001A81 RID: 6785 RVA: 0x0006067A File Offset: 0x0005E87A
		// (set) Token: 0x06001A82 RID: 6786 RVA: 0x00060696 File Offset: 0x0005E896
		public WebHeaderCollection Headers
		{
			get
			{
				if (this.m_headers == null)
				{
					this.m_headers = new WebHeaderCollection(WebHeaderCollectionType.WebRequest);
				}
				return this.m_headers;
			}
			set
			{
				this.m_headers = value;
			}
		}

		/// <summary>Gets or sets a collection of query name/value pairs associated with the request.</summary>
		/// <returns>A <see cref="T:System.Collections.Specialized.NameValueCollection" /> that contains query name/value pairs associated with the request. If no pairs are associated with the request, the value is an empty <see cref="T:System.Collections.Specialized.NameValueCollection" />.</returns>
		// Token: 0x1700058C RID: 1420
		// (get) Token: 0x06001A83 RID: 6787 RVA: 0x0006069F File Offset: 0x0005E89F
		// (set) Token: 0x06001A84 RID: 6788 RVA: 0x000606BA File Offset: 0x0005E8BA
		public NameValueCollection QueryString
		{
			get
			{
				if (this.m_requestParameters == null)
				{
					this.m_requestParameters = new NameValueCollection();
				}
				return this.m_requestParameters;
			}
			set
			{
				this.m_requestParameters = value;
			}
		}

		/// <summary>Gets a collection of header name/value pairs associated with the response.</summary>
		/// <returns>A <see cref="T:System.Net.WebHeaderCollection" /> containing header name/value pairs associated with the response, or <see langword="null" /> if no response has been received.</returns>
		// Token: 0x1700058D RID: 1421
		// (get) Token: 0x06001A85 RID: 6789 RVA: 0x000606C3 File Offset: 0x0005E8C3
		public WebHeaderCollection ResponseHeaders
		{
			get
			{
				if (this.m_WebResponse != null)
				{
					return this.m_WebResponse.Headers;
				}
				return null;
			}
		}

		/// <summary>Gets or sets the proxy used by this <see cref="T:System.Net.WebClient" /> object.</summary>
		/// <returns>An <see cref="T:System.Net.IWebProxy" /> instance used to send requests.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <see cref="P:System.Net.WebClient.Proxy" /> is set to <see langword="null" />. </exception>
		// Token: 0x1700058E RID: 1422
		// (get) Token: 0x06001A86 RID: 6790 RVA: 0x000606DA File Offset: 0x0005E8DA
		// (set) Token: 0x06001A87 RID: 6791 RVA: 0x000606F0 File Offset: 0x0005E8F0
		public IWebProxy Proxy
		{
			get
			{
				if (!this.m_ProxySet)
				{
					return WebRequest.InternalDefaultWebProxy;
				}
				return this.m_Proxy;
			}
			set
			{
				this.m_Proxy = value;
				this.m_ProxySet = true;
			}
		}

		/// <summary>Gets or sets the application's cache policy for any resources obtained by this WebClient instance using <see cref="T:System.Net.WebRequest" /> objects.</summary>
		/// <returns>A <see cref="T:System.Net.Cache.RequestCachePolicy" /> object that represents the application's caching requirements.</returns>
		// Token: 0x1700058F RID: 1423
		// (get) Token: 0x06001A88 RID: 6792 RVA: 0x00060700 File Offset: 0x0005E900
		// (set) Token: 0x06001A89 RID: 6793 RVA: 0x00060708 File Offset: 0x0005E908
		public RequestCachePolicy CachePolicy
		{
			get
			{
				return this.m_CachePolicy;
			}
			set
			{
				this.m_CachePolicy = value;
			}
		}

		/// <summary>Gets whether a Web request is in progress.</summary>
		/// <returns>
		///     <see langword="true" /> if the Web request is still in progress; otherwise <see langword="false" />.</returns>
		// Token: 0x17000590 RID: 1424
		// (get) Token: 0x06001A8A RID: 6794 RVA: 0x00060711 File Offset: 0x0005E911
		public bool IsBusy
		{
			get
			{
				return this.m_AsyncOp != null;
			}
		}

		/// <summary>Returns a <see cref="T:System.Net.WebRequest" /> object for the specified resource.</summary>
		/// <param name="address">A <see cref="T:System.Uri" /> that identifies the resource to request.</param>
		/// <returns>A new <see cref="T:System.Net.WebRequest" /> object for the specified resource.</returns>
		// Token: 0x06001A8B RID: 6795 RVA: 0x0006071C File Offset: 0x0005E91C
		protected virtual WebRequest GetWebRequest(Uri address)
		{
			WebRequest webRequest = WebRequest.Create(address);
			this.CopyHeadersTo(webRequest);
			if (this.Credentials != null)
			{
				webRequest.Credentials = this.Credentials;
			}
			if (this.m_Method != null)
			{
				webRequest.Method = this.m_Method;
			}
			if (this.m_ContentLength != -1L)
			{
				webRequest.ContentLength = this.m_ContentLength;
			}
			if (this.m_ProxySet)
			{
				webRequest.Proxy = this.m_Proxy;
			}
			if (this.m_CachePolicy != null)
			{
				webRequest.CachePolicy = this.m_CachePolicy;
			}
			return webRequest;
		}

		/// <summary>Returns the <see cref="T:System.Net.WebResponse" /> for the specified <see cref="T:System.Net.WebRequest" />.</summary>
		/// <param name="request">A <see cref="T:System.Net.WebRequest" /> that is used to obtain the response. </param>
		/// <returns>A <see cref="T:System.Net.WebResponse" /> containing the response for the specified <see cref="T:System.Net.WebRequest" />.</returns>
		// Token: 0x06001A8C RID: 6796 RVA: 0x000607A0 File Offset: 0x0005E9A0
		protected virtual WebResponse GetWebResponse(WebRequest request)
		{
			WebResponse response = request.GetResponse();
			this.m_WebResponse = response;
			return response;
		}

		/// <summary>Returns the <see cref="T:System.Net.WebResponse" /> for the specified <see cref="T:System.Net.WebRequest" /> using the specified <see cref="T:System.IAsyncResult" />.</summary>
		/// <param name="request">A <see cref="T:System.Net.WebRequest" /> that is used to obtain the response.</param>
		/// <param name="result">An <see cref="T:System.IAsyncResult" /> object obtained from a previous call to <see cref="M:System.Net.WebRequest.BeginGetResponse(System.AsyncCallback,System.Object)" /> .</param>
		/// <returns>A <see cref="T:System.Net.WebResponse" /> containing the response for the specified <see cref="T:System.Net.WebRequest" />.</returns>
		// Token: 0x06001A8D RID: 6797 RVA: 0x000607BC File Offset: 0x0005E9BC
		protected virtual WebResponse GetWebResponse(WebRequest request, IAsyncResult result)
		{
			WebResponse webResponse = request.EndGetResponse(result);
			this.m_WebResponse = webResponse;
			return webResponse;
		}

		/// <summary>Downloads the resource as a <see cref="T:System.Byte" /> array from the URI specified.</summary>
		/// <param name="address">The URI from which to download data. </param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the downloaded resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading data. </exception>
		/// <exception cref="T:System.NotSupportedException">The method has been called simultaneously on multiple threads.</exception>
		// Token: 0x06001A8E RID: 6798 RVA: 0x000607D9 File Offset: 0x0005E9D9
		public byte[] DownloadData(string address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.DownloadData(this.GetUri(address));
		}

		/// <summary>Downloads the resource as a <see cref="T:System.Byte" /> array from the URI specified.</summary>
		/// <param name="address">The URI represented by the <see cref="T:System.Uri" />  object, from which to download data.</param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the downloaded resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06001A8F RID: 6799 RVA: 0x000607F8 File Offset: 0x0005E9F8
		public byte[] DownloadData(Uri address)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			this.ClearWebClientState();
			byte[] result;
			try
			{
				WebRequest webRequest;
				byte[] array = this.DownloadDataInternal(address, out webRequest);
				bool on2 = Logging.On;
				result = array;
			}
			finally
			{
				this.CompleteWebClientState();
			}
			return result;
		}

		// Token: 0x06001A90 RID: 6800 RVA: 0x00060850 File Offset: 0x0005EA50
		private byte[] DownloadDataInternal(Uri address, out WebRequest request)
		{
			bool on = Logging.On;
			request = null;
			byte[] result;
			try
			{
				request = (this.m_WebRequest = this.GetWebRequest(this.GetUri(address)));
				result = this.DownloadBits(request, null, null, null);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				WebClient.AbortRequest(request);
				throw ex;
			}
			return result;
		}

		/// <summary>Downloads the resource with the specified URI to a local file.</summary>
		/// <param name="address">The URI from which to download data. </param>
		/// <param name="fileName">The name of the local file that is to receive the data. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="filename" /> is <see langword="null" /> or <see cref="F:System.String.Empty" />.-or-The file does not exist.-or- An error occurred while downloading data. </exception>
		/// <exception cref="T:System.NotSupportedException">The method has been called simultaneously on multiple threads.</exception>
		// Token: 0x06001A91 RID: 6801 RVA: 0x000608E4 File Offset: 0x0005EAE4
		public void DownloadFile(string address, string fileName)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			this.DownloadFile(this.GetUri(address), fileName);
		}

		/// <summary>Downloads the resource with the specified URI to a local file.</summary>
		/// <param name="address">The URI specified as a <see cref="T:System.String" />, from which to download data. </param>
		/// <param name="fileName">The name of the local file that is to receive the data. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="filename" /> is <see langword="null" /> or <see cref="F:System.String.Empty" />.-or- The file does not exist. -or- An error occurred while downloading data. </exception>
		/// <exception cref="T:System.NotSupportedException">The method has been called simultaneously on multiple threads.</exception>
		// Token: 0x06001A92 RID: 6802 RVA: 0x00060904 File Offset: 0x0005EB04
		public void DownloadFile(Uri address, string fileName)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			WebRequest request = null;
			FileStream fileStream = null;
			bool flag = false;
			this.ClearWebClientState();
			try
			{
				fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
				request = (this.m_WebRequest = this.GetWebRequest(this.GetUri(address)));
				this.DownloadBits(request, fileStream, null, null);
				flag = true;
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				WebClient.AbortRequest(request);
				throw ex;
			}
			finally
			{
				if (fileStream != null)
				{
					fileStream.Close();
					if (!flag)
					{
						File.Delete(fileName);
					}
					fileStream = null;
				}
				this.CompleteWebClientState();
			}
			bool on2 = Logging.On;
		}

		/// <summary>Opens a readable stream for the data downloaded from a resource with the URI specified as a <see cref="T:System.String" />.</summary>
		/// <param name="address">The URI specified as a <see cref="T:System.String" /> from which to download data. </param>
		/// <returns>A <see cref="T:System.IO.Stream" /> used to read data from a resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, <paramref name="address" /> is invalid.-or- An error occurred while downloading data. </exception>
		// Token: 0x06001A93 RID: 6803 RVA: 0x00060A00 File Offset: 0x0005EC00
		public Stream OpenRead(string address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.OpenRead(this.GetUri(address));
		}

		/// <summary>Opens a readable stream for the data downloaded from a resource with the URI specified as a <see cref="T:System.Uri" /></summary>
		/// <param name="address">The URI specified as a <see cref="T:System.Uri" /> from which to download data. </param>
		/// <returns>A <see cref="T:System.IO.Stream" /> used to read data from a resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, <paramref name="address" /> is invalid.-or- An error occurred while downloading data. </exception>
		// Token: 0x06001A94 RID: 6804 RVA: 0x00060A20 File Offset: 0x0005EC20
		public Stream OpenRead(Uri address)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			WebRequest request = null;
			this.ClearWebClientState();
			Stream result;
			try
			{
				request = (this.m_WebRequest = this.GetWebRequest(this.GetUri(address)));
				Stream responseStream = (this.m_WebResponse = this.GetWebResponse(request)).GetResponseStream();
				bool on2 = Logging.On;
				result = responseStream;
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				WebClient.AbortRequest(request);
				throw ex;
			}
			finally
			{
				this.CompleteWebClientState();
			}
			return result;
		}

		/// <summary>Opens a stream for writing data to the specified resource.</summary>
		/// <param name="address">The URI of the resource to receive the data. </param>
		/// <returns>A <see cref="T:System.IO.Stream" /> used to write data to the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream. </exception>
		// Token: 0x06001A95 RID: 6805 RVA: 0x00060AF8 File Offset: 0x0005ECF8
		public Stream OpenWrite(string address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.OpenWrite(this.GetUri(address), null);
		}

		/// <summary>Opens a stream for writing data to the specified resource.</summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <returns>A <see cref="T:System.IO.Stream" /> used to write data to the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream. </exception>
		// Token: 0x06001A96 RID: 6806 RVA: 0x00060B16 File Offset: 0x0005ED16
		public Stream OpenWrite(Uri address)
		{
			return this.OpenWrite(address, null);
		}

		/// <summary>Opens a stream for writing data to the specified resource, using the specified method.</summary>
		/// <param name="address">The URI of the resource to receive the data. </param>
		/// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <returns>A <see cref="T:System.IO.Stream" /> used to write data to the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream. </exception>
		// Token: 0x06001A97 RID: 6807 RVA: 0x00060B20 File Offset: 0x0005ED20
		public Stream OpenWrite(string address, string method)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.OpenWrite(this.GetUri(address), method);
		}

		/// <summary>Opens a stream for writing data to the specified resource, by using the specified method.</summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <returns>A <see cref="T:System.IO.Stream" /> used to write data to the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream. </exception>
		// Token: 0x06001A98 RID: 6808 RVA: 0x00060B40 File Offset: 0x0005ED40
		public Stream OpenWrite(Uri address, string method)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (method == null)
			{
				method = this.MapToDefaultMethod(address);
			}
			WebRequest webRequest = null;
			this.ClearWebClientState();
			Stream result;
			try
			{
				this.m_Method = method;
				webRequest = (this.m_WebRequest = this.GetWebRequest(this.GetUri(address)));
				Stream stream = new WebClient.WebClientWriteStream(webRequest.GetRequestStream(), webRequest, this);
				bool on2 = Logging.On;
				result = stream;
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				WebClient.AbortRequest(webRequest);
				throw ex;
			}
			finally
			{
				this.CompleteWebClientState();
			}
			return result;
		}

		/// <summary>Uploads a data buffer to a resource identified by a URI.</summary>
		/// <param name="address">The URI of the resource to receive the data. </param>
		/// <param name="data">The data buffer to send to the resource. </param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="data" /> is <see langword="null" />. -or-An error occurred while sending the data.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001A99 RID: 6809 RVA: 0x00060C18 File Offset: 0x0005EE18
		public byte[] UploadData(string address, byte[] data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadData(this.GetUri(address), null, data);
		}

		/// <summary>Uploads a data buffer to a resource identified by a URI.</summary>
		/// <param name="address">The URI of the resource to receive the data. </param>
		/// <param name="data">The data buffer to send to the resource. </param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="data" /> is <see langword="null" />. -or-An error occurred while sending the data.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001A9A RID: 6810 RVA: 0x00060C37 File Offset: 0x0005EE37
		public byte[] UploadData(Uri address, byte[] data)
		{
			return this.UploadData(address, null, data);
		}

		/// <summary>Uploads a data buffer to the specified resource, using the specified method.</summary>
		/// <param name="address">The URI of the resource to receive the data. </param>
		/// <param name="method">The HTTP method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The data buffer to send to the resource. </param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="data" /> is <see langword="null" />.-or- An error occurred while uploading the data.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001A9B RID: 6811 RVA: 0x00060C42 File Offset: 0x0005EE42
		public byte[] UploadData(string address, string method, byte[] data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadData(this.GetUri(address), method, data);
		}

		/// <summary>Uploads a data buffer to the specified resource, using the specified method.</summary>
		/// <param name="address">The URI of the resource to receive the data. </param>
		/// <param name="method">The HTTP method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The data buffer to send to the resource.</param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="data" /> is <see langword="null" />.-or- An error occurred while uploading the data.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001A9C RID: 6812 RVA: 0x00060C64 File Offset: 0x0005EE64
		public byte[] UploadData(Uri address, string method, byte[] data)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (method == null)
			{
				method = this.MapToDefaultMethod(address);
			}
			this.ClearWebClientState();
			byte[] result;
			try
			{
				WebRequest webRequest;
				byte[] array = this.UploadDataInternal(address, method, data, out webRequest);
				bool on2 = Logging.On;
				result = array;
			}
			finally
			{
				this.CompleteWebClientState();
			}
			return result;
		}

		// Token: 0x06001A9D RID: 6813 RVA: 0x00060CD8 File Offset: 0x0005EED8
		private byte[] UploadDataInternal(Uri address, string method, byte[] data, out WebRequest request)
		{
			request = null;
			byte[] result;
			try
			{
				this.m_Method = method;
				this.m_ContentLength = (long)data.Length;
				request = (this.m_WebRequest = this.GetWebRequest(this.GetUri(address)));
				this.UploadBits(request, null, data, 0, null, null, null, null, null);
				result = this.DownloadBits(request, null, null, null);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				WebClient.AbortRequest(request);
				throw ex;
			}
			return result;
		}

		// Token: 0x06001A9E RID: 6814 RVA: 0x00060D8C File Offset: 0x0005EF8C
		private void OpenFileInternal(bool needsHeaderAndBoundary, string fileName, ref FileStream fs, ref byte[] buffer, ref byte[] formHeaderBytes, ref byte[] boundaryBytes)
		{
			fileName = Path.GetFullPath(fileName);
			if (this.m_headers == null)
			{
				this.m_headers = new WebHeaderCollection(WebHeaderCollectionType.WebRequest);
			}
			string text = this.m_headers["Content-Type"];
			if (text != null)
			{
				if (text.ToLower(CultureInfo.InvariantCulture).StartsWith("multipart/"))
				{
					throw new WebException(SR.GetString("The Content-Type header cannot be set to a multipart type for this request."));
				}
			}
			else
			{
				text = "application/octet-stream";
			}
			fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			int num = 8192;
			this.m_ContentLength = -1L;
			if (this.m_Method.ToUpper(CultureInfo.InvariantCulture) == "POST")
			{
				if (needsHeaderAndBoundary)
				{
					string text2 = "---------------------" + DateTime.Now.Ticks.ToString("x", NumberFormatInfo.InvariantInfo);
					this.m_headers["Content-Type"] = "multipart/form-data; boundary=" + text2;
					string s = string.Concat(new string[]
					{
						"--",
						text2,
						"\r\nContent-Disposition: form-data; name=\"file\"; filename=\"",
						Path.GetFileName(fileName),
						"\"\r\nContent-Type: ",
						text,
						"\r\n\r\n"
					});
					formHeaderBytes = Encoding.UTF8.GetBytes(s);
					boundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + text2 + "--\r\n");
				}
				else
				{
					formHeaderBytes = new byte[0];
					boundaryBytes = new byte[0];
				}
				if (fs.CanSeek)
				{
					this.m_ContentLength = fs.Length + (long)formHeaderBytes.Length + (long)boundaryBytes.Length;
					num = (int)Math.Min(8192L, fs.Length);
				}
			}
			else
			{
				this.m_headers["Content-Type"] = text;
				formHeaderBytes = null;
				boundaryBytes = null;
				if (fs.CanSeek)
				{
					this.m_ContentLength = fs.Length;
					num = (int)Math.Min(8192L, fs.Length);
				}
			}
			buffer = new byte[num];
		}

		/// <summary>Uploads the specified local file to a resource with the specified URI.</summary>
		/// <param name="address">The URI of the resource to receive the file. For example, ftp://localhost/samplefile.txt.</param>
		/// <param name="fileName">The file to send to the resource. For example, "samplefile.txt".</param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid characters, or does not exist.-or- An error occurred while uploading the file.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001A9F RID: 6815 RVA: 0x00060F7C File Offset: 0x0005F17C
		public byte[] UploadFile(string address, string fileName)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadFile(this.GetUri(address), fileName);
		}

		/// <summary>Uploads the specified local file to a resource with the specified URI.</summary>
		/// <param name="address">The URI of the resource to receive the file. For example, ftp://localhost/samplefile.txt.</param>
		/// <param name="fileName">The file to send to the resource. For example, "samplefile.txt".</param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid characters, or does not exist.-or- An error occurred while uploading the file.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001AA0 RID: 6816 RVA: 0x00060F9A File Offset: 0x0005F19A
		public byte[] UploadFile(Uri address, string fileName)
		{
			return this.UploadFile(address, null, fileName);
		}

		/// <summary>Uploads the specified local file to the specified resource, using the specified method.</summary>
		/// <param name="address">The URI of the resource to receive the file.</param>
		/// <param name="method">The method used to send the file to the resource. If <see langword="null" />, the default is POST for http and STOR for ftp.</param>
		/// <param name="fileName">The file to send to the resource. </param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid characters, or does not exist.-or- An error occurred while uploading the file.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001AA1 RID: 6817 RVA: 0x00060FA5 File Offset: 0x0005F1A5
		public byte[] UploadFile(string address, string method, string fileName)
		{
			return this.UploadFile(this.GetUri(address), method, fileName);
		}

		/// <summary>Uploads the specified local file to the specified resource, using the specified method.</summary>
		/// <param name="address">The URI of the resource to receive the file.</param>
		/// <param name="method">The method used to send the file to the resource. If <see langword="null" />, the default is POST for http and STOR for ftp.</param>
		/// <param name="fileName">The file to send to the resource. </param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid characters, or does not exist.-or- An error occurred while uploading the file.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001AA2 RID: 6818 RVA: 0x00060FB8 File Offset: 0x0005F1B8
		public byte[] UploadFile(Uri address, string method, string fileName)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			if (method == null)
			{
				method = this.MapToDefaultMethod(address);
			}
			FileStream fileStream = null;
			WebRequest request = null;
			this.ClearWebClientState();
			byte[] result;
			try
			{
				this.m_Method = method;
				byte[] header = null;
				byte[] footer = null;
				byte[] buffer = null;
				Uri uri = this.GetUri(address);
				bool needsHeaderAndBoundary = uri.Scheme != Uri.UriSchemeFile;
				this.OpenFileInternal(needsHeaderAndBoundary, fileName, ref fileStream, ref buffer, ref header, ref footer);
				request = (this.m_WebRequest = this.GetWebRequest(uri));
				this.UploadBits(request, fileStream, buffer, 0, header, footer, null, null, null);
				byte[] array = this.DownloadBits(request, null, null, null);
				bool on2 = Logging.On;
				result = array;
			}
			catch (Exception ex)
			{
				if (fileStream != null)
				{
					fileStream.Close();
					fileStream = null;
				}
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				WebClient.AbortRequest(request);
				throw ex;
			}
			finally
			{
				this.CompleteWebClientState();
			}
			return result;
		}

		// Token: 0x06001AA3 RID: 6819 RVA: 0x000610F4 File Offset: 0x0005F2F4
		private byte[] UploadValuesInternal(NameValueCollection data)
		{
			if (this.m_headers == null)
			{
				this.m_headers = new WebHeaderCollection(WebHeaderCollectionType.WebRequest);
			}
			string text = this.m_headers["Content-Type"];
			if (text != null && string.Compare(text, "application/x-www-form-urlencoded", StringComparison.OrdinalIgnoreCase) != 0)
			{
				throw new WebException(SR.GetString("The Content-Type header cannot be changed from its default value for this request."));
			}
			this.m_headers["Content-Type"] = "application/x-www-form-urlencoded";
			string value = string.Empty;
			StringBuilder stringBuilder = new StringBuilder();
			foreach (string text2 in data.AllKeys)
			{
				stringBuilder.Append(value);
				stringBuilder.Append(WebClient.UrlEncode(text2));
				stringBuilder.Append("=");
				stringBuilder.Append(WebClient.UrlEncode(data[text2]));
				value = "&";
			}
			byte[] bytes = Encoding.ASCII.GetBytes(stringBuilder.ToString());
			this.m_ContentLength = (long)bytes.Length;
			return bytes;
		}

		/// <summary>Uploads the specified name/value collection to the resource identified by the specified URI.</summary>
		/// <param name="address">The URI of the resource to receive the collection. </param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource. </param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="data" /> is <see langword="null" />.-or- There was no response from the server hosting the resource.-or- An error occurred while opening the stream.-or- The <see langword="Content-type" /> header is not <see langword="null" /> or "application/x-www-form-urlencoded". </exception>
		// Token: 0x06001AA4 RID: 6820 RVA: 0x000611E2 File Offset: 0x0005F3E2
		public byte[] UploadValues(string address, NameValueCollection data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadValues(this.GetUri(address), null, data);
		}

		/// <summary>Uploads the specified name/value collection to the resource identified by the specified URI.</summary>
		/// <param name="address">The URI of the resource to receive the collection. </param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource. </param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="data" /> is <see langword="null" />.-or- There was no response from the server hosting the resource.-or- An error occurred while opening the stream.-or- The <see langword="Content-type" /> header is not <see langword="null" /> or "application/x-www-form-urlencoded". </exception>
		// Token: 0x06001AA5 RID: 6821 RVA: 0x00061201 File Offset: 0x0005F401
		public byte[] UploadValues(Uri address, NameValueCollection data)
		{
			return this.UploadValues(address, null, data);
		}

		/// <summary>Uploads the specified name/value collection to the resource identified by the specified URI, using the specified method.</summary>
		/// <param name="address">The URI of the resource to receive the collection. </param>
		/// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource. </param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="data" /> is <see langword="null" />.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header value is not <see langword="null" /> and is not <see langword="application/x-www-form-urlencoded" />. </exception>
		// Token: 0x06001AA6 RID: 6822 RVA: 0x0006120C File Offset: 0x0005F40C
		public byte[] UploadValues(string address, string method, NameValueCollection data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadValues(this.GetUri(address), method, data);
		}

		/// <summary>Uploads the specified name/value collection to the resource identified by the specified URI, using the specified method.</summary>
		/// <param name="address">The URI of the resource to receive the collection. </param>
		/// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource. </param>
		/// <returns>A <see cref="T:System.Byte" /> array containing the body of the response from the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="data" /> is <see langword="null" />.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header value is not <see langword="null" /> and is not <see langword="application/x-www-form-urlencoded" />. </exception>
		// Token: 0x06001AA7 RID: 6823 RVA: 0x0006122C File Offset: 0x0005F42C
		public byte[] UploadValues(Uri address, string method, NameValueCollection data)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (method == null)
			{
				method = this.MapToDefaultMethod(address);
			}
			WebRequest request = null;
			this.ClearWebClientState();
			byte[] result;
			try
			{
				byte[] buffer = this.UploadValuesInternal(data);
				this.m_Method = method;
				request = (this.m_WebRequest = this.GetWebRequest(this.GetUri(address)));
				this.UploadBits(request, null, buffer, 0, null, null, null, null, null);
				byte[] array = this.DownloadBits(request, null, null, null);
				bool on2 = Logging.On;
				result = array;
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				WebClient.AbortRequest(request);
				throw ex;
			}
			finally
			{
				this.CompleteWebClientState();
			}
			return result;
		}

		/// <summary>Uploads the specified string to the specified resource, using the POST method.</summary>
		/// <param name="address">The URI of the resource to receive the string. For Http resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page. </param>
		/// <param name="data">The string to be uploaded.</param>
		/// <returns>A <see cref="T:System.String" /> containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.</exception>
		// Token: 0x06001AA8 RID: 6824 RVA: 0x00061330 File Offset: 0x0005F530
		public string UploadString(string address, string data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadString(this.GetUri(address), null, data);
		}

		/// <summary>Uploads the specified string to the specified resource, using the POST method.</summary>
		/// <param name="address">The URI of the resource to receive the string. For Http resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page. </param>
		/// <param name="data">The string to be uploaded.</param>
		/// <returns>A <see cref="T:System.String" /> containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.</exception>
		// Token: 0x06001AA9 RID: 6825 RVA: 0x0006134F File Offset: 0x0005F54F
		public string UploadString(Uri address, string data)
		{
			return this.UploadString(address, null, data);
		}

		/// <summary>Uploads the specified string to the specified resource, using the specified method.</summary>
		/// <param name="address">The URI of the resource to receive the string. This URI must identify a resource that can accept a request sent with the <paramref name="method" /> method. </param>
		/// <param name="method">The HTTP method used to send the string to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The string to be uploaded.</param>
		/// <returns>A <see cref="T:System.String" /> containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.-or-
		///         <paramref name="method" /> cannot be used to send content.</exception>
		// Token: 0x06001AAA RID: 6826 RVA: 0x0006135A File Offset: 0x0005F55A
		public string UploadString(string address, string method, string data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadString(this.GetUri(address), method, data);
		}

		/// <summary>Uploads the specified string to the specified resource, using the specified method.</summary>
		/// <param name="address">The URI of the resource to receive the string. This URI must identify a resource that can accept a request sent with the <paramref name="method" /> method. </param>
		/// <param name="method">The HTTP method used to send the string to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The string to be uploaded.</param>
		/// <returns>A <see cref="T:System.String" /> containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.-or-
		///         <paramref name="method" /> cannot be used to send content.</exception>
		// Token: 0x06001AAB RID: 6827 RVA: 0x0006137C File Offset: 0x0005F57C
		public string UploadString(Uri address, string method, string data)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (method == null)
			{
				method = this.MapToDefaultMethod(address);
			}
			this.ClearWebClientState();
			string result;
			try
			{
				byte[] bytes = this.Encoding.GetBytes(data);
				WebRequest request;
				byte[] data2 = this.UploadDataInternal(address, method, bytes, out request);
				string stringUsingEncoding = this.GetStringUsingEncoding(request, data2);
				bool on2 = Logging.On;
				result = stringUsingEncoding;
			}
			finally
			{
				this.CompleteWebClientState();
			}
			return result;
		}

		/// <summary>Downloads the requested resource as a <see cref="T:System.String" />. The resource to download is specified as a <see cref="T:System.String" /> containing the URI.</summary>
		/// <param name="address">A <see cref="T:System.String" /> containing the URI to download.</param>
		/// <returns>A <see cref="T:System.String" /> containing the requested resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		/// <exception cref="T:System.NotSupportedException">The method has been called simultaneously on multiple threads.</exception>
		// Token: 0x06001AAC RID: 6828 RVA: 0x00061408 File Offset: 0x0005F608
		public string DownloadString(string address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.DownloadString(this.GetUri(address));
		}

		/// <summary>Downloads the requested resource as a <see cref="T:System.String" />. The resource to download is specified as a <see cref="T:System.Uri" />.</summary>
		/// <param name="address">A <see cref="T:System.Uri" /> object containing the URI to download.</param>
		/// <returns>A <see cref="T:System.String" /> containing the requested resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		/// <exception cref="T:System.NotSupportedException">The method has been called simultaneously on multiple threads.</exception>
		// Token: 0x06001AAD RID: 6829 RVA: 0x00061428 File Offset: 0x0005F628
		public string DownloadString(Uri address)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			this.ClearWebClientState();
			string result;
			try
			{
				WebRequest request;
				byte[] data = this.DownloadDataInternal(address, out request);
				string stringUsingEncoding = this.GetStringUsingEncoding(request, data);
				bool on2 = Logging.On;
				result = stringUsingEncoding;
			}
			finally
			{
				this.CompleteWebClientState();
			}
			return result;
		}

		// Token: 0x06001AAE RID: 6830 RVA: 0x00061488 File Offset: 0x0005F688
		private static void AbortRequest(WebRequest request)
		{
			try
			{
				if (request != null)
				{
					request.Abort();
				}
			}
			catch (Exception ex)
			{
				if (ex is OutOfMemoryException || ex is StackOverflowException || ex is ThreadAbortException)
				{
					throw;
				}
			}
		}

		// Token: 0x06001AAF RID: 6831 RVA: 0x000614D0 File Offset: 0x0005F6D0
		private void CopyHeadersTo(WebRequest request)
		{
			if (this.m_headers != null && request is HttpWebRequest)
			{
				string text = this.m_headers["Accept"];
				string text2 = this.m_headers["Connection"];
				string text3 = this.m_headers["Content-Type"];
				string text4 = this.m_headers["Expect"];
				string text5 = this.m_headers["Referer"];
				string text6 = this.m_headers["User-Agent"];
				string text7 = this.m_headers["Host"];
				this.m_headers.RemoveInternal("Accept");
				this.m_headers.RemoveInternal("Connection");
				this.m_headers.RemoveInternal("Content-Type");
				this.m_headers.RemoveInternal("Expect");
				this.m_headers.RemoveInternal("Referer");
				this.m_headers.RemoveInternal("User-Agent");
				this.m_headers.RemoveInternal("Host");
				request.Headers = this.m_headers;
				if (text != null && text.Length > 0)
				{
					((HttpWebRequest)request).Accept = text;
				}
				if (text2 != null && text2.Length > 0)
				{
					((HttpWebRequest)request).Connection = text2;
				}
				if (text3 != null && text3.Length > 0)
				{
					((HttpWebRequest)request).ContentType = text3;
				}
				if (text4 != null && text4.Length > 0)
				{
					((HttpWebRequest)request).Expect = text4;
				}
				if (text5 != null && text5.Length > 0)
				{
					((HttpWebRequest)request).Referer = text5;
				}
				if (text6 != null && text6.Length > 0)
				{
					((HttpWebRequest)request).UserAgent = text6;
				}
				if (!string.IsNullOrEmpty(text7))
				{
					((HttpWebRequest)request).Host = text7;
				}
			}
		}

		// Token: 0x06001AB0 RID: 6832 RVA: 0x00061698 File Offset: 0x0005F898
		private Uri GetUri(string path)
		{
			Uri address;
			if (this.m_baseAddress != null)
			{
				if (!Uri.TryCreate(this.m_baseAddress, path, out address))
				{
					return new Uri(Path.GetFullPath(path));
				}
			}
			else if (!Uri.TryCreate(path, UriKind.Absolute, out address))
			{
				return new Uri(Path.GetFullPath(path));
			}
			return this.GetUri(address);
		}

		// Token: 0x06001AB1 RID: 6833 RVA: 0x000616F0 File Offset: 0x0005F8F0
		private Uri GetUri(Uri address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			Uri uri = address;
			if (!address.IsAbsoluteUri && this.m_baseAddress != null && !Uri.TryCreate(this.m_baseAddress, address, out uri))
			{
				return address;
			}
			if ((uri.Query == null || uri.Query == string.Empty) && this.m_requestParameters != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				string str = string.Empty;
				for (int i = 0; i < this.m_requestParameters.Count; i++)
				{
					stringBuilder.Append(str + this.m_requestParameters.AllKeys[i] + "=" + this.m_requestParameters[i]);
					str = "&";
				}
				uri = new UriBuilder(uri)
				{
					Query = stringBuilder.ToString()
				}.Uri;
			}
			return uri;
		}

		// Token: 0x06001AB2 RID: 6834 RVA: 0x000617CC File Offset: 0x0005F9CC
		private static void DownloadBitsResponseCallback(IAsyncResult result)
		{
			WebClient.DownloadBitsState downloadBitsState = (WebClient.DownloadBitsState)result.AsyncState;
			WebRequest request = downloadBitsState.Request;
			Exception ex = null;
			try
			{
				WebResponse webResponse = downloadBitsState.WebClient.GetWebResponse(request, result);
				downloadBitsState.WebClient.m_WebResponse = webResponse;
				downloadBitsState.SetResponse(webResponse);
			}
			catch (Exception ex2)
			{
				if (ex2 is ThreadAbortException || ex2 is StackOverflowException || ex2 is OutOfMemoryException)
				{
					throw;
				}
				ex = ex2;
				if (!(ex2 is WebException) && !(ex2 is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex2);
				}
				WebClient.AbortRequest(request);
				if (downloadBitsState != null && downloadBitsState.WriteStream != null)
				{
					downloadBitsState.WriteStream.Close();
				}
			}
			finally
			{
				if (ex != null)
				{
					downloadBitsState.CompletionDelegate(null, ex, downloadBitsState.AsyncOp);
				}
			}
		}

		// Token: 0x06001AB3 RID: 6835 RVA: 0x000618AC File Offset: 0x0005FAAC
		private static void DownloadBitsReadCallback(IAsyncResult result)
		{
			WebClient.DownloadBitsReadCallbackState((WebClient.DownloadBitsState)result.AsyncState, result);
		}

		// Token: 0x06001AB4 RID: 6836 RVA: 0x000618C0 File Offset: 0x0005FAC0
		private static void DownloadBitsReadCallbackState(WebClient.DownloadBitsState state, IAsyncResult result)
		{
			Stream readStream = state.ReadStream;
			Exception ex = null;
			bool flag = false;
			try
			{
				int num = 0;
				if (readStream != null && readStream != Stream.Null)
				{
					num = readStream.EndRead(result);
				}
				flag = state.RetrieveBytes(ref num);
			}
			catch (Exception ex2)
			{
				flag = true;
				if (ex2 is ThreadAbortException || ex2 is StackOverflowException || ex2 is OutOfMemoryException)
				{
					throw;
				}
				ex = ex2;
				state.InnerBuffer = null;
				if (!(ex2 is WebException) && !(ex2 is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex2);
				}
				WebClient.AbortRequest(state.Request);
				if (state != null && state.WriteStream != null)
				{
					state.WriteStream.Close();
				}
			}
			finally
			{
				if (flag)
				{
					if (ex == null)
					{
						state.Close();
					}
					state.CompletionDelegate(state.InnerBuffer, ex, state.AsyncOp);
				}
			}
		}

		// Token: 0x06001AB5 RID: 6837 RVA: 0x000619B0 File Offset: 0x0005FBB0
		private byte[] DownloadBits(WebRequest request, Stream writeStream, CompletionDelegate completionDelegate, AsyncOperation asyncOp)
		{
			WebClient.DownloadBitsState downloadBitsState = new WebClient.DownloadBitsState(request, writeStream, completionDelegate, asyncOp, this.m_Progress, this);
			if (downloadBitsState.Async)
			{
				request.BeginGetResponse(new AsyncCallback(WebClient.DownloadBitsResponseCallback), downloadBitsState);
				return null;
			}
			WebResponse response = this.m_WebResponse = this.GetWebResponse(request);
			int num = downloadBitsState.SetResponse(response);
			bool flag;
			do
			{
				flag = downloadBitsState.RetrieveBytes(ref num);
			}
			while (!flag);
			downloadBitsState.Close();
			return downloadBitsState.InnerBuffer;
		}

		// Token: 0x06001AB6 RID: 6838 RVA: 0x00061A24 File Offset: 0x0005FC24
		private static void UploadBitsRequestCallback(IAsyncResult result)
		{
			WebClient.UploadBitsState uploadBitsState = (WebClient.UploadBitsState)result.AsyncState;
			WebRequest request = uploadBitsState.Request;
			Exception ex = null;
			try
			{
				Stream requestStream = request.EndGetRequestStream(result);
				uploadBitsState.SetRequestStream(requestStream);
			}
			catch (Exception ex2)
			{
				if (ex2 is ThreadAbortException || ex2 is StackOverflowException || ex2 is OutOfMemoryException)
				{
					throw;
				}
				ex = ex2;
				if (!(ex2 is WebException) && !(ex2 is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex2);
				}
				WebClient.AbortRequest(request);
				if (uploadBitsState != null && uploadBitsState.ReadStream != null)
				{
					uploadBitsState.ReadStream.Close();
				}
			}
			finally
			{
				if (ex != null)
				{
					uploadBitsState.UploadCompletionDelegate(null, ex, uploadBitsState);
				}
			}
		}

		// Token: 0x06001AB7 RID: 6839 RVA: 0x00061AEC File Offset: 0x0005FCEC
		private static void UploadBitsWriteCallback(IAsyncResult result)
		{
			WebClient.UploadBitsState uploadBitsState = (WebClient.UploadBitsState)result.AsyncState;
			Stream writeStream = uploadBitsState.WriteStream;
			Exception ex = null;
			bool flag = false;
			try
			{
				writeStream.EndWrite(result);
				flag = uploadBitsState.WriteBytes();
			}
			catch (Exception ex2)
			{
				flag = true;
				if (ex2 is ThreadAbortException || ex2 is StackOverflowException || ex2 is OutOfMemoryException)
				{
					throw;
				}
				ex = ex2;
				if (!(ex2 is WebException) && !(ex2 is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex2);
				}
				WebClient.AbortRequest(uploadBitsState.Request);
				if (uploadBitsState != null && uploadBitsState.ReadStream != null)
				{
					uploadBitsState.ReadStream.Close();
				}
			}
			finally
			{
				if (flag)
				{
					if (ex == null)
					{
						uploadBitsState.Close();
					}
					uploadBitsState.UploadCompletionDelegate(null, ex, uploadBitsState);
				}
			}
		}

		// Token: 0x06001AB8 RID: 6840 RVA: 0x00061BC8 File Offset: 0x0005FDC8
		private void UploadBits(WebRequest request, Stream readStream, byte[] buffer, int chunkSize, byte[] header, byte[] footer, CompletionDelegate uploadCompletionDelegate, CompletionDelegate downloadCompletionDelegate, AsyncOperation asyncOp)
		{
			if (request.RequestUri.Scheme == Uri.UriSchemeFile)
			{
				footer = (header = null);
			}
			WebClient.UploadBitsState uploadBitsState = new WebClient.UploadBitsState(request, readStream, buffer, chunkSize, header, footer, uploadCompletionDelegate, downloadCompletionDelegate, asyncOp, this.m_Progress, this);
			if (uploadBitsState.Async)
			{
				request.BeginGetRequestStream(new AsyncCallback(WebClient.UploadBitsRequestCallback), uploadBitsState);
				return;
			}
			Stream requestStream = request.GetRequestStream();
			uploadBitsState.SetRequestStream(requestStream);
			while (!uploadBitsState.WriteBytes())
			{
			}
			uploadBitsState.Close();
		}

		// Token: 0x06001AB9 RID: 6841 RVA: 0x00061C48 File Offset: 0x0005FE48
		private bool ByteArrayHasPrefix(byte[] prefix, byte[] byteArray)
		{
			if (prefix == null || byteArray == null || prefix.Length > byteArray.Length)
			{
				return false;
			}
			for (int i = 0; i < prefix.Length; i++)
			{
				if (prefix[i] != byteArray[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06001ABA RID: 6842 RVA: 0x00061C80 File Offset: 0x0005FE80
		private string GetStringUsingEncoding(WebRequest request, byte[] data)
		{
			Encoding encoding = null;
			int num = -1;
			string text;
			try
			{
				text = request.ContentType;
			}
			catch (NotImplementedException)
			{
				text = null;
			}
			catch (NotSupportedException)
			{
				text = null;
			}
			if (text != null)
			{
				text = text.ToLower(CultureInfo.InvariantCulture);
				string[] array = text.Split(new char[]
				{
					';',
					'=',
					' '
				});
				bool flag = false;
				foreach (string text2 in array)
				{
					if (text2 == "charset")
					{
						flag = true;
					}
					else if (flag)
					{
						try
						{
							encoding = Encoding.GetEncoding(text2);
						}
						catch (ArgumentException)
						{
							break;
						}
					}
				}
			}
			if (encoding == null)
			{
				Encoding[] array3 = new Encoding[]
				{
					Encoding.UTF8,
					Encoding.UTF32,
					Encoding.Unicode,
					Encoding.BigEndianUnicode
				};
				for (int j = 0; j < array3.Length; j++)
				{
					byte[] preamble = array3[j].GetPreamble();
					if (this.ByteArrayHasPrefix(preamble, data))
					{
						encoding = array3[j];
						num = preamble.Length;
						break;
					}
				}
			}
			if (encoding == null)
			{
				encoding = this.Encoding;
			}
			if (num == -1)
			{
				byte[] preamble2 = encoding.GetPreamble();
				if (this.ByteArrayHasPrefix(preamble2, data))
				{
					num = preamble2.Length;
				}
				else
				{
					num = 0;
				}
			}
			return encoding.GetString(data, num, data.Length - num);
		}

		// Token: 0x06001ABB RID: 6843 RVA: 0x00061DCC File Offset: 0x0005FFCC
		private string MapToDefaultMethod(Uri address)
		{
			Uri uri;
			if (!address.IsAbsoluteUri && this.m_baseAddress != null)
			{
				uri = new Uri(this.m_baseAddress, address);
			}
			else
			{
				uri = address;
			}
			if (uri.Scheme.ToLower(CultureInfo.InvariantCulture) == "ftp")
			{
				return "STOR";
			}
			return "POST";
		}

		// Token: 0x06001ABC RID: 6844 RVA: 0x00061E27 File Offset: 0x00060027
		private static string UrlEncode(string str)
		{
			if (str == null)
			{
				return null;
			}
			return WebClient.UrlEncode(str, Encoding.UTF8);
		}

		// Token: 0x06001ABD RID: 6845 RVA: 0x00061E39 File Offset: 0x00060039
		private static string UrlEncode(string str, Encoding e)
		{
			if (str == null)
			{
				return null;
			}
			return Encoding.ASCII.GetString(WebClient.UrlEncodeToBytes(str, e));
		}

		// Token: 0x06001ABE RID: 6846 RVA: 0x00061E54 File Offset: 0x00060054
		private static byte[] UrlEncodeToBytes(string str, Encoding e)
		{
			if (str == null)
			{
				return null;
			}
			byte[] bytes = e.GetBytes(str);
			return WebClient.UrlEncodeBytesToBytesInternal(bytes, 0, bytes.Length, false);
		}

		// Token: 0x06001ABF RID: 6847 RVA: 0x00061E7C File Offset: 0x0006007C
		private static byte[] UrlEncodeBytesToBytesInternal(byte[] bytes, int offset, int count, bool alwaysCreateReturnValue)
		{
			int num = 0;
			int num2 = 0;
			for (int i = 0; i < count; i++)
			{
				char c = (char)bytes[offset + i];
				if (c == ' ')
				{
					num++;
				}
				else if (!WebClient.IsSafe(c))
				{
					num2++;
				}
			}
			if (!alwaysCreateReturnValue && num == 0 && num2 == 0)
			{
				return bytes;
			}
			byte[] array = new byte[count + num2 * 2];
			int num3 = 0;
			for (int j = 0; j < count; j++)
			{
				byte b = bytes[offset + j];
				char c2 = (char)b;
				if (WebClient.IsSafe(c2))
				{
					array[num3++] = b;
				}
				else if (c2 == ' ')
				{
					array[num3++] = 43;
				}
				else
				{
					array[num3++] = 37;
					array[num3++] = (byte)WebClient.IntToHex(b >> 4 & 15);
					array[num3++] = (byte)WebClient.IntToHex((int)(b & 15));
				}
			}
			return array;
		}

		// Token: 0x06001AC0 RID: 6848 RVA: 0x00061F47 File Offset: 0x00060147
		private static char IntToHex(int n)
		{
			if (n <= 9)
			{
				return (char)(n + 48);
			}
			return (char)(n - 10 + 97);
		}

		// Token: 0x06001AC1 RID: 6849 RVA: 0x00061F5C File Offset: 0x0006015C
		private static bool IsSafe(char ch)
		{
			if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9'))
			{
				return true;
			}
			if (ch != '!')
			{
				switch (ch)
				{
				case '\'':
				case '(':
				case ')':
				case '*':
				case '-':
				case '.':
					return true;
				case '+':
				case ',':
					break;
				default:
					if (ch == '_')
					{
						return true;
					}
					break;
				}
				return false;
			}
			return true;
		}

		// Token: 0x06001AC2 RID: 6850 RVA: 0x00061FBF File Offset: 0x000601BF
		private void InvokeOperationCompleted(AsyncOperation asyncOp, SendOrPostCallback callback, AsyncCompletedEventArgs eventArgs)
		{
			if (Interlocked.CompareExchange<AsyncOperation>(ref this.m_AsyncOp, null, asyncOp) == asyncOp)
			{
				this.CompleteWebClientState();
				asyncOp.PostOperationCompleted(callback, eventArgs);
			}
		}

		// Token: 0x06001AC3 RID: 6851 RVA: 0x00061FDF File Offset: 0x000601DF
		private bool AnotherCallInProgress(int callNesting)
		{
			return callNesting > 1;
		}

		/// <summary>Occurs when an asynchronous operation to open a stream containing a resource completes.</summary>
		// Token: 0x14000031 RID: 49
		// (add) Token: 0x06001AC4 RID: 6852 RVA: 0x00061FE8 File Offset: 0x000601E8
		// (remove) Token: 0x06001AC5 RID: 6853 RVA: 0x00062020 File Offset: 0x00060220
		public event OpenReadCompletedEventHandler OpenReadCompleted
		{
			[CompilerGenerated]
			add
			{
				OpenReadCompletedEventHandler openReadCompletedEventHandler = this.OpenReadCompleted;
				OpenReadCompletedEventHandler openReadCompletedEventHandler2;
				do
				{
					openReadCompletedEventHandler2 = openReadCompletedEventHandler;
					OpenReadCompletedEventHandler value2 = (OpenReadCompletedEventHandler)Delegate.Combine(openReadCompletedEventHandler2, value);
					openReadCompletedEventHandler = Interlocked.CompareExchange<OpenReadCompletedEventHandler>(ref this.OpenReadCompleted, value2, openReadCompletedEventHandler2);
				}
				while (openReadCompletedEventHandler != openReadCompletedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				OpenReadCompletedEventHandler openReadCompletedEventHandler = this.OpenReadCompleted;
				OpenReadCompletedEventHandler openReadCompletedEventHandler2;
				do
				{
					openReadCompletedEventHandler2 = openReadCompletedEventHandler;
					OpenReadCompletedEventHandler value2 = (OpenReadCompletedEventHandler)Delegate.Remove(openReadCompletedEventHandler2, value);
					openReadCompletedEventHandler = Interlocked.CompareExchange<OpenReadCompletedEventHandler>(ref this.OpenReadCompleted, value2, openReadCompletedEventHandler2);
				}
				while (openReadCompletedEventHandler != openReadCompletedEventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.OpenReadCompleted" /> event.</summary>
		/// <param name="e">A <see cref="T:System.Net.OpenReadCompletedEventArgs" />  object containing event data.</param>
		// Token: 0x06001AC6 RID: 6854 RVA: 0x00062055 File Offset: 0x00060255
		protected virtual void OnOpenReadCompleted(OpenReadCompletedEventArgs e)
		{
			if (this.OpenReadCompleted != null)
			{
				this.OpenReadCompleted(this, e);
			}
		}

		// Token: 0x06001AC7 RID: 6855 RVA: 0x0006206C File Offset: 0x0006026C
		private void OpenReadOperationCompleted(object arg)
		{
			this.OnOpenReadCompleted((OpenReadCompletedEventArgs)arg);
		}

		// Token: 0x06001AC8 RID: 6856 RVA: 0x0006207C File Offset: 0x0006027C
		private void OpenReadAsyncCallback(IAsyncResult result)
		{
			AsyncOperation asyncOperation = (AsyncOperation)result.AsyncState;
			WebRequest request;
			if (result is WebAsyncResult)
			{
				request = ((WebAsyncResult)result).AsyncObject;
			}
			else
			{
				request = (WebRequest)((LazyAsyncResult)result).AsyncObject;
			}
			Stream result2 = null;
			Exception exception = null;
			try
			{
				result2 = (this.m_WebResponse = this.GetWebResponse(request, result)).GetResponseStream();
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				exception = ex;
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					exception = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
			}
			OpenReadCompletedEventArgs eventArgs = new OpenReadCompletedEventArgs(result2, exception, this.m_Cancelled, asyncOperation.UserSuppliedState);
			this.InvokeOperationCompleted(asyncOperation, this.openReadOperationCompleted, eventArgs);
		}

		/// <summary>Opens a readable stream containing the specified resource. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to retrieve.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and address is invalid.-or- An error occurred while downloading the resource. -or- An error occurred while opening the stream.</exception>
		// Token: 0x06001AC9 RID: 6857 RVA: 0x00062158 File Offset: 0x00060358
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void OpenReadAsync(Uri address)
		{
			this.OpenReadAsync(address, null);
		}

		/// <summary>Opens a readable stream containing the specified resource. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to retrieve.</param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and address is invalid.-or- An error occurred while downloading the resource. -or- An error occurred while opening the stream.</exception>
		// Token: 0x06001ACA RID: 6858 RVA: 0x00062164 File Offset: 0x00060364
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void OpenReadAsync(Uri address, object userToken)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			this.InitWebClientAsync();
			this.ClearWebClientState();
			AsyncOperation asyncOperation = AsyncOperationManager.CreateOperation(userToken);
			this.m_AsyncOp = asyncOperation;
			try
			{
				(this.m_WebRequest = this.GetWebRequest(this.GetUri(address))).BeginGetResponse(new AsyncCallback(this.OpenReadAsyncCallback), asyncOperation);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				OpenReadCompletedEventArgs eventArgs = new OpenReadCompletedEventArgs(null, ex, this.m_Cancelled, asyncOperation.UserSuppliedState);
				this.InvokeOperationCompleted(asyncOperation, this.openReadOperationCompleted, eventArgs);
			}
			bool on2 = Logging.On;
		}

		/// <summary>Occurs when an asynchronous operation to open a stream to write data to a resource completes.</summary>
		// Token: 0x14000032 RID: 50
		// (add) Token: 0x06001ACB RID: 6859 RVA: 0x00062248 File Offset: 0x00060448
		// (remove) Token: 0x06001ACC RID: 6860 RVA: 0x00062280 File Offset: 0x00060480
		public event OpenWriteCompletedEventHandler OpenWriteCompleted
		{
			[CompilerGenerated]
			add
			{
				OpenWriteCompletedEventHandler openWriteCompletedEventHandler = this.OpenWriteCompleted;
				OpenWriteCompletedEventHandler openWriteCompletedEventHandler2;
				do
				{
					openWriteCompletedEventHandler2 = openWriteCompletedEventHandler;
					OpenWriteCompletedEventHandler value2 = (OpenWriteCompletedEventHandler)Delegate.Combine(openWriteCompletedEventHandler2, value);
					openWriteCompletedEventHandler = Interlocked.CompareExchange<OpenWriteCompletedEventHandler>(ref this.OpenWriteCompleted, value2, openWriteCompletedEventHandler2);
				}
				while (openWriteCompletedEventHandler != openWriteCompletedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				OpenWriteCompletedEventHandler openWriteCompletedEventHandler = this.OpenWriteCompleted;
				OpenWriteCompletedEventHandler openWriteCompletedEventHandler2;
				do
				{
					openWriteCompletedEventHandler2 = openWriteCompletedEventHandler;
					OpenWriteCompletedEventHandler value2 = (OpenWriteCompletedEventHandler)Delegate.Remove(openWriteCompletedEventHandler2, value);
					openWriteCompletedEventHandler = Interlocked.CompareExchange<OpenWriteCompletedEventHandler>(ref this.OpenWriteCompleted, value2, openWriteCompletedEventHandler2);
				}
				while (openWriteCompletedEventHandler != openWriteCompletedEventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.OpenWriteCompleted" /> event.</summary>
		/// <param name="e">A <see cref="T:System.Net.OpenWriteCompletedEventArgs" /> object containing event data.</param>
		// Token: 0x06001ACD RID: 6861 RVA: 0x000622B5 File Offset: 0x000604B5
		protected virtual void OnOpenWriteCompleted(OpenWriteCompletedEventArgs e)
		{
			if (this.OpenWriteCompleted != null)
			{
				this.OpenWriteCompleted(this, e);
			}
		}

		// Token: 0x06001ACE RID: 6862 RVA: 0x000622CC File Offset: 0x000604CC
		private void OpenWriteOperationCompleted(object arg)
		{
			this.OnOpenWriteCompleted((OpenWriteCompletedEventArgs)arg);
		}

		// Token: 0x06001ACF RID: 6863 RVA: 0x000622DC File Offset: 0x000604DC
		private void OpenWriteAsyncCallback(IAsyncResult result)
		{
			WebAsyncResult webAsyncResult = (WebAsyncResult)result;
			AsyncOperation asyncOperation = (AsyncOperation)webAsyncResult.AsyncState;
			WebRequest asyncObject = webAsyncResult.AsyncObject;
			WebClient.WebClientWriteStream result2 = null;
			Exception exception = null;
			try
			{
				result2 = new WebClient.WebClientWriteStream(asyncObject.EndGetRequestStream(result), asyncObject, this);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				exception = ex;
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					exception = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
			}
			OpenWriteCompletedEventArgs eventArgs = new OpenWriteCompletedEventArgs(result2, exception, this.m_Cancelled, asyncOperation.UserSuppliedState);
			this.InvokeOperationCompleted(asyncOperation, this.openWriteOperationCompleted, eventArgs);
		}

		/// <summary>Opens a stream for writing data to the specified resource. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the data. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06001AD0 RID: 6864 RVA: 0x00062394 File Offset: 0x00060594
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void OpenWriteAsync(Uri address)
		{
			this.OpenWriteAsync(address, null, null);
		}

		/// <summary>Opens a stream for writing data to the specified resource. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the data. </param>
		/// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06001AD1 RID: 6865 RVA: 0x0006239F File Offset: 0x0006059F
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void OpenWriteAsync(Uri address, string method)
		{
			this.OpenWriteAsync(address, method, null);
		}

		/// <summary>Opens a stream for writing data to the specified resource, using the specified method. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream. </exception>
		// Token: 0x06001AD2 RID: 6866 RVA: 0x000623AC File Offset: 0x000605AC
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void OpenWriteAsync(Uri address, string method, object userToken)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (method == null)
			{
				method = this.MapToDefaultMethod(address);
			}
			this.InitWebClientAsync();
			this.ClearWebClientState();
			AsyncOperation asyncOperation = AsyncOperationManager.CreateOperation(userToken);
			this.m_AsyncOp = asyncOperation;
			try
			{
				this.m_Method = method;
				(this.m_WebRequest = this.GetWebRequest(this.GetUri(address))).BeginGetRequestStream(new AsyncCallback(this.OpenWriteAsyncCallback), asyncOperation);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				OpenWriteCompletedEventArgs eventArgs = new OpenWriteCompletedEventArgs(null, ex, this.m_Cancelled, asyncOperation.UserSuppliedState);
				this.InvokeOperationCompleted(asyncOperation, this.openWriteOperationCompleted, eventArgs);
			}
			bool on2 = Logging.On;
		}

		/// <summary>Occurs when an asynchronous resource-download operation completes.</summary>
		// Token: 0x14000033 RID: 51
		// (add) Token: 0x06001AD3 RID: 6867 RVA: 0x000624A4 File Offset: 0x000606A4
		// (remove) Token: 0x06001AD4 RID: 6868 RVA: 0x000624DC File Offset: 0x000606DC
		public event DownloadStringCompletedEventHandler DownloadStringCompleted
		{
			[CompilerGenerated]
			add
			{
				DownloadStringCompletedEventHandler downloadStringCompletedEventHandler = this.DownloadStringCompleted;
				DownloadStringCompletedEventHandler downloadStringCompletedEventHandler2;
				do
				{
					downloadStringCompletedEventHandler2 = downloadStringCompletedEventHandler;
					DownloadStringCompletedEventHandler value2 = (DownloadStringCompletedEventHandler)Delegate.Combine(downloadStringCompletedEventHandler2, value);
					downloadStringCompletedEventHandler = Interlocked.CompareExchange<DownloadStringCompletedEventHandler>(ref this.DownloadStringCompleted, value2, downloadStringCompletedEventHandler2);
				}
				while (downloadStringCompletedEventHandler != downloadStringCompletedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				DownloadStringCompletedEventHandler downloadStringCompletedEventHandler = this.DownloadStringCompleted;
				DownloadStringCompletedEventHandler downloadStringCompletedEventHandler2;
				do
				{
					downloadStringCompletedEventHandler2 = downloadStringCompletedEventHandler;
					DownloadStringCompletedEventHandler value2 = (DownloadStringCompletedEventHandler)Delegate.Remove(downloadStringCompletedEventHandler2, value);
					downloadStringCompletedEventHandler = Interlocked.CompareExchange<DownloadStringCompletedEventHandler>(ref this.DownloadStringCompleted, value2, downloadStringCompletedEventHandler2);
				}
				while (downloadStringCompletedEventHandler != downloadStringCompletedEventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.DownloadStringCompleted" /> event.</summary>
		/// <param name="e">A <see cref="T:System.Net.DownloadStringCompletedEventArgs" /> object containing event data.</param>
		// Token: 0x06001AD5 RID: 6869 RVA: 0x00062511 File Offset: 0x00060711
		protected virtual void OnDownloadStringCompleted(DownloadStringCompletedEventArgs e)
		{
			if (this.DownloadStringCompleted != null)
			{
				this.DownloadStringCompleted(this, e);
			}
		}

		// Token: 0x06001AD6 RID: 6870 RVA: 0x00062528 File Offset: 0x00060728
		private void DownloadStringOperationCompleted(object arg)
		{
			this.OnDownloadStringCompleted((DownloadStringCompletedEventArgs)arg);
		}

		// Token: 0x06001AD7 RID: 6871 RVA: 0x00062538 File Offset: 0x00060738
		private void DownloadStringAsyncCallback(byte[] returnBytes, Exception exception, object state)
		{
			AsyncOperation asyncOperation = (AsyncOperation)state;
			string result = null;
			try
			{
				if (returnBytes != null)
				{
					result = this.GetStringUsingEncoding(this.m_WebRequest, returnBytes);
				}
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				exception = ex;
			}
			DownloadStringCompletedEventArgs eventArgs = new DownloadStringCompletedEventArgs(result, exception, this.m_Cancelled, asyncOperation.UserSuppliedState);
			this.InvokeOperationCompleted(asyncOperation, this.downloadStringOperationCompleted, eventArgs);
		}

		/// <summary>Downloads the resource specified as a <see cref="T:System.Uri" />. This method does not block the calling thread.</summary>
		/// <param name="address">A <see cref="T:System.Uri" /> containing the URI to download.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		// Token: 0x06001AD8 RID: 6872 RVA: 0x000625B4 File Offset: 0x000607B4
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void DownloadStringAsync(Uri address)
		{
			this.DownloadStringAsync(address, null);
		}

		/// <summary>Downloads the specified string to the specified resource. This method does not block the calling thread.</summary>
		/// <param name="address">A <see cref="T:System.Uri" /> containing the URI to download.</param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		// Token: 0x06001AD9 RID: 6873 RVA: 0x000625C0 File Offset: 0x000607C0
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void DownloadStringAsync(Uri address, object userToken)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			this.InitWebClientAsync();
			this.ClearWebClientState();
			AsyncOperation asyncOperation = AsyncOperationManager.CreateOperation(userToken);
			this.m_AsyncOp = asyncOperation;
			try
			{
				WebRequest request = this.m_WebRequest = this.GetWebRequest(this.GetUri(address));
				this.DownloadBits(request, null, new CompletionDelegate(this.DownloadStringAsyncCallback), asyncOperation);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				this.DownloadStringAsyncCallback(null, ex, asyncOperation);
			}
			bool on2 = Logging.On;
		}

		/// <summary>Occurs when an asynchronous data download operation completes.</summary>
		// Token: 0x14000034 RID: 52
		// (add) Token: 0x06001ADA RID: 6874 RVA: 0x00062690 File Offset: 0x00060890
		// (remove) Token: 0x06001ADB RID: 6875 RVA: 0x000626C8 File Offset: 0x000608C8
		public event DownloadDataCompletedEventHandler DownloadDataCompleted
		{
			[CompilerGenerated]
			add
			{
				DownloadDataCompletedEventHandler downloadDataCompletedEventHandler = this.DownloadDataCompleted;
				DownloadDataCompletedEventHandler downloadDataCompletedEventHandler2;
				do
				{
					downloadDataCompletedEventHandler2 = downloadDataCompletedEventHandler;
					DownloadDataCompletedEventHandler value2 = (DownloadDataCompletedEventHandler)Delegate.Combine(downloadDataCompletedEventHandler2, value);
					downloadDataCompletedEventHandler = Interlocked.CompareExchange<DownloadDataCompletedEventHandler>(ref this.DownloadDataCompleted, value2, downloadDataCompletedEventHandler2);
				}
				while (downloadDataCompletedEventHandler != downloadDataCompletedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				DownloadDataCompletedEventHandler downloadDataCompletedEventHandler = this.DownloadDataCompleted;
				DownloadDataCompletedEventHandler downloadDataCompletedEventHandler2;
				do
				{
					downloadDataCompletedEventHandler2 = downloadDataCompletedEventHandler;
					DownloadDataCompletedEventHandler value2 = (DownloadDataCompletedEventHandler)Delegate.Remove(downloadDataCompletedEventHandler2, value);
					downloadDataCompletedEventHandler = Interlocked.CompareExchange<DownloadDataCompletedEventHandler>(ref this.DownloadDataCompleted, value2, downloadDataCompletedEventHandler2);
				}
				while (downloadDataCompletedEventHandler != downloadDataCompletedEventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.DownloadDataCompleted" /> event.</summary>
		/// <param name="e">A <see cref="T:System.Net.DownloadDataCompletedEventArgs" /> object that contains event data.</param>
		// Token: 0x06001ADC RID: 6876 RVA: 0x000626FD File Offset: 0x000608FD
		protected virtual void OnDownloadDataCompleted(DownloadDataCompletedEventArgs e)
		{
			if (this.DownloadDataCompleted != null)
			{
				this.DownloadDataCompleted(this, e);
			}
		}

		// Token: 0x06001ADD RID: 6877 RVA: 0x00062714 File Offset: 0x00060914
		private void DownloadDataOperationCompleted(object arg)
		{
			this.OnDownloadDataCompleted((DownloadDataCompletedEventArgs)arg);
		}

		// Token: 0x06001ADE RID: 6878 RVA: 0x00062724 File Offset: 0x00060924
		private void DownloadDataAsyncCallback(byte[] returnBytes, Exception exception, object state)
		{
			AsyncOperation asyncOperation = (AsyncOperation)state;
			DownloadDataCompletedEventArgs eventArgs = new DownloadDataCompletedEventArgs(returnBytes, exception, this.m_Cancelled, asyncOperation.UserSuppliedState);
			this.InvokeOperationCompleted(asyncOperation, this.downloadDataOperationCompleted, eventArgs);
		}

		/// <summary>Downloads the resource as a <see cref="T:System.Byte" /> array from the URI specified as an asynchronous operation. </summary>
		/// <param name="address">A <see cref="T:System.Uri" /> containing the URI to download.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		// Token: 0x06001ADF RID: 6879 RVA: 0x0006275A File Offset: 0x0006095A
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void DownloadDataAsync(Uri address)
		{
			this.DownloadDataAsync(address, null);
		}

		/// <summary>Downloads the resource as a <see cref="T:System.Byte" /> array from the URI specified as an asynchronous operation. </summary>
		/// <param name="address">A <see cref="T:System.Uri" /> containing the URI to download.</param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		// Token: 0x06001AE0 RID: 6880 RVA: 0x00062764 File Offset: 0x00060964
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void DownloadDataAsync(Uri address, object userToken)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			this.InitWebClientAsync();
			this.ClearWebClientState();
			AsyncOperation asyncOperation = AsyncOperationManager.CreateOperation(userToken);
			this.m_AsyncOp = asyncOperation;
			try
			{
				WebRequest request = this.m_WebRequest = this.GetWebRequest(this.GetUri(address));
				this.DownloadBits(request, null, new CompletionDelegate(this.DownloadDataAsyncCallback), asyncOperation);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				this.DownloadDataAsyncCallback(null, ex, asyncOperation);
			}
			bool on2 = Logging.On;
		}

		/// <summary>Occurs when an asynchronous file download operation completes.</summary>
		// Token: 0x14000035 RID: 53
		// (add) Token: 0x06001AE1 RID: 6881 RVA: 0x00062834 File Offset: 0x00060A34
		// (remove) Token: 0x06001AE2 RID: 6882 RVA: 0x0006286C File Offset: 0x00060A6C
		public event AsyncCompletedEventHandler DownloadFileCompleted
		{
			[CompilerGenerated]
			add
			{
				AsyncCompletedEventHandler asyncCompletedEventHandler = this.DownloadFileCompleted;
				AsyncCompletedEventHandler asyncCompletedEventHandler2;
				do
				{
					asyncCompletedEventHandler2 = asyncCompletedEventHandler;
					AsyncCompletedEventHandler value2 = (AsyncCompletedEventHandler)Delegate.Combine(asyncCompletedEventHandler2, value);
					asyncCompletedEventHandler = Interlocked.CompareExchange<AsyncCompletedEventHandler>(ref this.DownloadFileCompleted, value2, asyncCompletedEventHandler2);
				}
				while (asyncCompletedEventHandler != asyncCompletedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				AsyncCompletedEventHandler asyncCompletedEventHandler = this.DownloadFileCompleted;
				AsyncCompletedEventHandler asyncCompletedEventHandler2;
				do
				{
					asyncCompletedEventHandler2 = asyncCompletedEventHandler;
					AsyncCompletedEventHandler value2 = (AsyncCompletedEventHandler)Delegate.Remove(asyncCompletedEventHandler2, value);
					asyncCompletedEventHandler = Interlocked.CompareExchange<AsyncCompletedEventHandler>(ref this.DownloadFileCompleted, value2, asyncCompletedEventHandler2);
				}
				while (asyncCompletedEventHandler != asyncCompletedEventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.DownloadFileCompleted" /> event.</summary>
		/// <param name="e">An <see cref="T:System.ComponentModel.AsyncCompletedEventArgs" /> object containing event data.</param>
		// Token: 0x06001AE3 RID: 6883 RVA: 0x000628A1 File Offset: 0x00060AA1
		protected virtual void OnDownloadFileCompleted(AsyncCompletedEventArgs e)
		{
			if (this.DownloadFileCompleted != null)
			{
				this.DownloadFileCompleted(this, e);
			}
		}

		// Token: 0x06001AE4 RID: 6884 RVA: 0x000628B8 File Offset: 0x00060AB8
		private void DownloadFileOperationCompleted(object arg)
		{
			this.OnDownloadFileCompleted((AsyncCompletedEventArgs)arg);
		}

		// Token: 0x06001AE5 RID: 6885 RVA: 0x000628C8 File Offset: 0x00060AC8
		private void DownloadFileAsyncCallback(byte[] returnBytes, Exception exception, object state)
		{
			AsyncOperation asyncOperation = (AsyncOperation)state;
			AsyncCompletedEventArgs eventArgs = new AsyncCompletedEventArgs(exception, this.m_Cancelled, asyncOperation.UserSuppliedState);
			this.InvokeOperationCompleted(asyncOperation, this.downloadFileOperationCompleted, eventArgs);
		}

		/// <summary>Downloads, to a local file, the resource with the specified URI. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to download. </param>
		/// <param name="fileName">The name of the file to be placed on the local computer. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		/// <exception cref="T:System.InvalidOperationException">The local file specified by <paramref name="fileName" /> is in use by another thread.</exception>
		// Token: 0x06001AE6 RID: 6886 RVA: 0x000628FD File Offset: 0x00060AFD
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void DownloadFileAsync(Uri address, string fileName)
		{
			this.DownloadFileAsync(address, fileName, null);
		}

		/// <summary>Downloads, to a local file, the resource with the specified URI. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to download. </param>
		/// <param name="fileName">The name of the file to be placed on the local computer. </param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		/// <exception cref="T:System.InvalidOperationException">The local file specified by <paramref name="fileName" /> is in use by another thread.</exception>
		// Token: 0x06001AE7 RID: 6887 RVA: 0x00062908 File Offset: 0x00060B08
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void DownloadFileAsync(Uri address, string fileName, object userToken)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			FileStream fileStream = null;
			this.InitWebClientAsync();
			this.ClearWebClientState();
			AsyncOperation asyncOperation = AsyncOperationManager.CreateOperation(userToken);
			this.m_AsyncOp = asyncOperation;
			try
			{
				fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
				WebRequest request = this.m_WebRequest = this.GetWebRequest(this.GetUri(address));
				this.DownloadBits(request, fileStream, new CompletionDelegate(this.DownloadFileAsyncCallback), asyncOperation);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (fileStream != null)
				{
					fileStream.Close();
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				this.DownloadFileAsyncCallback(null, ex, asyncOperation);
			}
			bool on2 = Logging.On;
		}

		/// <summary>Occurs when an asynchronous string-upload operation completes.</summary>
		// Token: 0x14000036 RID: 54
		// (add) Token: 0x06001AE8 RID: 6888 RVA: 0x00062A00 File Offset: 0x00060C00
		// (remove) Token: 0x06001AE9 RID: 6889 RVA: 0x00062A38 File Offset: 0x00060C38
		public event UploadStringCompletedEventHandler UploadStringCompleted
		{
			[CompilerGenerated]
			add
			{
				UploadStringCompletedEventHandler uploadStringCompletedEventHandler = this.UploadStringCompleted;
				UploadStringCompletedEventHandler uploadStringCompletedEventHandler2;
				do
				{
					uploadStringCompletedEventHandler2 = uploadStringCompletedEventHandler;
					UploadStringCompletedEventHandler value2 = (UploadStringCompletedEventHandler)Delegate.Combine(uploadStringCompletedEventHandler2, value);
					uploadStringCompletedEventHandler = Interlocked.CompareExchange<UploadStringCompletedEventHandler>(ref this.UploadStringCompleted, value2, uploadStringCompletedEventHandler2);
				}
				while (uploadStringCompletedEventHandler != uploadStringCompletedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				UploadStringCompletedEventHandler uploadStringCompletedEventHandler = this.UploadStringCompleted;
				UploadStringCompletedEventHandler uploadStringCompletedEventHandler2;
				do
				{
					uploadStringCompletedEventHandler2 = uploadStringCompletedEventHandler;
					UploadStringCompletedEventHandler value2 = (UploadStringCompletedEventHandler)Delegate.Remove(uploadStringCompletedEventHandler2, value);
					uploadStringCompletedEventHandler = Interlocked.CompareExchange<UploadStringCompletedEventHandler>(ref this.UploadStringCompleted, value2, uploadStringCompletedEventHandler2);
				}
				while (uploadStringCompletedEventHandler != uploadStringCompletedEventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.UploadStringCompleted" /> event.</summary>
		/// <param name="e">An <see cref="T:System.Net.UploadStringCompletedEventArgs" />  object containing event data.</param>
		// Token: 0x06001AEA RID: 6890 RVA: 0x00062A6D File Offset: 0x00060C6D
		protected virtual void OnUploadStringCompleted(UploadStringCompletedEventArgs e)
		{
			if (this.UploadStringCompleted != null)
			{
				this.UploadStringCompleted(this, e);
			}
		}

		// Token: 0x06001AEB RID: 6891 RVA: 0x00062A84 File Offset: 0x00060C84
		private void UploadStringOperationCompleted(object arg)
		{
			this.OnUploadStringCompleted((UploadStringCompletedEventArgs)arg);
		}

		// Token: 0x06001AEC RID: 6892 RVA: 0x00062A94 File Offset: 0x00060C94
		private void StartDownloadAsync(WebClient.UploadBitsState state)
		{
			try
			{
				this.DownloadBits(state.Request, null, state.DownloadCompletionDelegate, state.AsyncOp);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				state.DownloadCompletionDelegate(null, ex, state.AsyncOp);
			}
		}

		// Token: 0x06001AED RID: 6893 RVA: 0x00062B20 File Offset: 0x00060D20
		private void UploadStringAsyncWriteCallback(byte[] returnBytes, Exception exception, object state)
		{
			WebClient.UploadBitsState uploadBitsState = (WebClient.UploadBitsState)state;
			if (exception != null)
			{
				UploadStringCompletedEventArgs eventArgs = new UploadStringCompletedEventArgs(null, exception, this.m_Cancelled, uploadBitsState.AsyncOp.UserSuppliedState);
				this.InvokeOperationCompleted(uploadBitsState.AsyncOp, this.uploadStringOperationCompleted, eventArgs);
				return;
			}
			this.StartDownloadAsync(uploadBitsState);
		}

		// Token: 0x06001AEE RID: 6894 RVA: 0x00062B6C File Offset: 0x00060D6C
		private void UploadStringAsyncReadCallback(byte[] returnBytes, Exception exception, object state)
		{
			AsyncOperation asyncOperation = (AsyncOperation)state;
			string result = null;
			try
			{
				if (returnBytes != null)
				{
					result = this.GetStringUsingEncoding(this.m_WebRequest, returnBytes);
				}
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				exception = ex;
			}
			UploadStringCompletedEventArgs eventArgs = new UploadStringCompletedEventArgs(result, exception, this.m_Cancelled, asyncOperation.UserSuppliedState);
			this.InvokeOperationCompleted(asyncOperation, this.uploadStringOperationCompleted, eventArgs);
		}

		/// <summary>Uploads the specified string to the specified resource. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page. </param>
		/// <param name="data">The string to be uploaded.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.</exception>
		// Token: 0x06001AEF RID: 6895 RVA: 0x00062BE8 File Offset: 0x00060DE8
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadStringAsync(Uri address, string data)
		{
			this.UploadStringAsync(address, null, data, null);
		}

		/// <summary>Uploads the specified string to the specified resource. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The string to be uploaded.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or-
		///         <paramref name="method" /> cannot be used to send content.-or- There was no response from the server hosting the resource.</exception>
		// Token: 0x06001AF0 RID: 6896 RVA: 0x00062BF4 File Offset: 0x00060DF4
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadStringAsync(Uri address, string method, string data)
		{
			this.UploadStringAsync(address, method, data, null);
		}

		/// <summary>Uploads the specified string to the specified resource. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The string to be uploaded.</param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or-
		///         <paramref name="method" /> cannot be used to send content.-or- There was no response from the server hosting the resource.</exception>
		// Token: 0x06001AF1 RID: 6897 RVA: 0x00062C00 File Offset: 0x00060E00
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadStringAsync(Uri address, string method, string data, object userToken)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (method == null)
			{
				method = this.MapToDefaultMethod(address);
			}
			this.InitWebClientAsync();
			this.ClearWebClientState();
			AsyncOperation asyncOperation = AsyncOperationManager.CreateOperation(userToken);
			this.m_AsyncOp = asyncOperation;
			try
			{
				byte[] bytes = this.Encoding.GetBytes(data);
				this.m_Method = method;
				this.m_ContentLength = (long)bytes.Length;
				WebRequest request = this.m_WebRequest = this.GetWebRequest(this.GetUri(address));
				this.UploadBits(request, null, bytes, 0, null, null, new CompletionDelegate(this.UploadStringAsyncWriteCallback), new CompletionDelegate(this.UploadStringAsyncReadCallback), asyncOperation);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				UploadStringCompletedEventArgs eventArgs = new UploadStringCompletedEventArgs(null, ex, this.m_Cancelled, asyncOperation.UserSuppliedState);
				this.InvokeOperationCompleted(asyncOperation, this.uploadStringOperationCompleted, eventArgs);
			}
			bool on2 = Logging.On;
		}

		/// <summary>Occurs when an asynchronous data-upload operation completes.</summary>
		// Token: 0x14000037 RID: 55
		// (add) Token: 0x06001AF2 RID: 6898 RVA: 0x00062D3C File Offset: 0x00060F3C
		// (remove) Token: 0x06001AF3 RID: 6899 RVA: 0x00062D74 File Offset: 0x00060F74
		public event UploadDataCompletedEventHandler UploadDataCompleted
		{
			[CompilerGenerated]
			add
			{
				UploadDataCompletedEventHandler uploadDataCompletedEventHandler = this.UploadDataCompleted;
				UploadDataCompletedEventHandler uploadDataCompletedEventHandler2;
				do
				{
					uploadDataCompletedEventHandler2 = uploadDataCompletedEventHandler;
					UploadDataCompletedEventHandler value2 = (UploadDataCompletedEventHandler)Delegate.Combine(uploadDataCompletedEventHandler2, value);
					uploadDataCompletedEventHandler = Interlocked.CompareExchange<UploadDataCompletedEventHandler>(ref this.UploadDataCompleted, value2, uploadDataCompletedEventHandler2);
				}
				while (uploadDataCompletedEventHandler != uploadDataCompletedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				UploadDataCompletedEventHandler uploadDataCompletedEventHandler = this.UploadDataCompleted;
				UploadDataCompletedEventHandler uploadDataCompletedEventHandler2;
				do
				{
					uploadDataCompletedEventHandler2 = uploadDataCompletedEventHandler;
					UploadDataCompletedEventHandler value2 = (UploadDataCompletedEventHandler)Delegate.Remove(uploadDataCompletedEventHandler2, value);
					uploadDataCompletedEventHandler = Interlocked.CompareExchange<UploadDataCompletedEventHandler>(ref this.UploadDataCompleted, value2, uploadDataCompletedEventHandler2);
				}
				while (uploadDataCompletedEventHandler != uploadDataCompletedEventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.UploadDataCompleted" /> event.</summary>
		/// <param name="e">A <see cref="T:System.Net.UploadDataCompletedEventArgs" />  object containing event data.</param>
		// Token: 0x06001AF4 RID: 6900 RVA: 0x00062DA9 File Offset: 0x00060FA9
		protected virtual void OnUploadDataCompleted(UploadDataCompletedEventArgs e)
		{
			if (this.UploadDataCompleted != null)
			{
				this.UploadDataCompleted(this, e);
			}
		}

		// Token: 0x06001AF5 RID: 6901 RVA: 0x00062DC0 File Offset: 0x00060FC0
		private void UploadDataOperationCompleted(object arg)
		{
			this.OnUploadDataCompleted((UploadDataCompletedEventArgs)arg);
		}

		// Token: 0x06001AF6 RID: 6902 RVA: 0x00062DD0 File Offset: 0x00060FD0
		private void UploadDataAsyncWriteCallback(byte[] returnBytes, Exception exception, object state)
		{
			WebClient.UploadBitsState uploadBitsState = (WebClient.UploadBitsState)state;
			if (exception != null)
			{
				UploadDataCompletedEventArgs eventArgs = new UploadDataCompletedEventArgs(returnBytes, exception, this.m_Cancelled, uploadBitsState.AsyncOp.UserSuppliedState);
				this.InvokeOperationCompleted(uploadBitsState.AsyncOp, this.uploadDataOperationCompleted, eventArgs);
				return;
			}
			this.StartDownloadAsync(uploadBitsState);
		}

		// Token: 0x06001AF7 RID: 6903 RVA: 0x00062E1C File Offset: 0x0006101C
		private void UploadDataAsyncReadCallback(byte[] returnBytes, Exception exception, object state)
		{
			AsyncOperation asyncOperation = (AsyncOperation)state;
			UploadDataCompletedEventArgs eventArgs = new UploadDataCompletedEventArgs(returnBytes, exception, this.m_Cancelled, asyncOperation.UserSuppliedState);
			this.InvokeOperationCompleted(asyncOperation, this.uploadDataOperationCompleted, eventArgs);
		}

		/// <summary>Uploads a data buffer to a resource identified by a URI, using the POST method. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the data. </param>
		/// <param name="data">The data buffer to send to the resource. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001AF8 RID: 6904 RVA: 0x00062E52 File Offset: 0x00061052
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadDataAsync(Uri address, byte[] data)
		{
			this.UploadDataAsync(address, null, data, null);
		}

		/// <summary>Uploads a data buffer to a resource identified by a URI, using the specified method. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <param name="method">The method used to send the data to the resource. If <see langword="null" />, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The data buffer to send to the resource.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001AF9 RID: 6905 RVA: 0x00062E5E File Offset: 0x0006105E
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadDataAsync(Uri address, string method, byte[] data)
		{
			this.UploadDataAsync(address, method, data, null);
		}

		/// <summary>Uploads a data buffer to a resource identified by a URI, using the specified method and identifying token.</summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <param name="method">The method used to send the data to the resource. If <see langword="null" />, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The data buffer to send to the resource.</param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001AFA RID: 6906 RVA: 0x00062E6C File Offset: 0x0006106C
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadDataAsync(Uri address, string method, byte[] data, object userToken)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (method == null)
			{
				method = this.MapToDefaultMethod(address);
			}
			this.InitWebClientAsync();
			this.ClearWebClientState();
			AsyncOperation asyncOperation = AsyncOperationManager.CreateOperation(userToken);
			this.m_AsyncOp = asyncOperation;
			int chunkSize = 0;
			try
			{
				this.m_Method = method;
				this.m_ContentLength = (long)data.Length;
				WebRequest request = this.m_WebRequest = this.GetWebRequest(this.GetUri(address));
				if (this.UploadProgressChanged != null)
				{
					chunkSize = (int)Math.Min(8192L, (long)data.Length);
				}
				this.UploadBits(request, null, data, chunkSize, null, null, new CompletionDelegate(this.UploadDataAsyncWriteCallback), new CompletionDelegate(this.UploadDataAsyncReadCallback), asyncOperation);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				UploadDataCompletedEventArgs eventArgs = new UploadDataCompletedEventArgs(null, ex, this.m_Cancelled, asyncOperation.UserSuppliedState);
				this.InvokeOperationCompleted(asyncOperation, this.uploadDataOperationCompleted, eventArgs);
			}
			bool on2 = Logging.On;
		}

		/// <summary>Occurs when an asynchronous file-upload operation completes.</summary>
		// Token: 0x14000038 RID: 56
		// (add) Token: 0x06001AFB RID: 6907 RVA: 0x00062FB4 File Offset: 0x000611B4
		// (remove) Token: 0x06001AFC RID: 6908 RVA: 0x00062FEC File Offset: 0x000611EC
		public event UploadFileCompletedEventHandler UploadFileCompleted
		{
			[CompilerGenerated]
			add
			{
				UploadFileCompletedEventHandler uploadFileCompletedEventHandler = this.UploadFileCompleted;
				UploadFileCompletedEventHandler uploadFileCompletedEventHandler2;
				do
				{
					uploadFileCompletedEventHandler2 = uploadFileCompletedEventHandler;
					UploadFileCompletedEventHandler value2 = (UploadFileCompletedEventHandler)Delegate.Combine(uploadFileCompletedEventHandler2, value);
					uploadFileCompletedEventHandler = Interlocked.CompareExchange<UploadFileCompletedEventHandler>(ref this.UploadFileCompleted, value2, uploadFileCompletedEventHandler2);
				}
				while (uploadFileCompletedEventHandler != uploadFileCompletedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				UploadFileCompletedEventHandler uploadFileCompletedEventHandler = this.UploadFileCompleted;
				UploadFileCompletedEventHandler uploadFileCompletedEventHandler2;
				do
				{
					uploadFileCompletedEventHandler2 = uploadFileCompletedEventHandler;
					UploadFileCompletedEventHandler value2 = (UploadFileCompletedEventHandler)Delegate.Remove(uploadFileCompletedEventHandler2, value);
					uploadFileCompletedEventHandler = Interlocked.CompareExchange<UploadFileCompletedEventHandler>(ref this.UploadFileCompleted, value2, uploadFileCompletedEventHandler2);
				}
				while (uploadFileCompletedEventHandler != uploadFileCompletedEventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.UploadFileCompleted" /> event.</summary>
		/// <param name="e">An <see cref="T:System.Net.UploadFileCompletedEventArgs" /> object containing event data.</param>
		// Token: 0x06001AFD RID: 6909 RVA: 0x00063021 File Offset: 0x00061221
		protected virtual void OnUploadFileCompleted(UploadFileCompletedEventArgs e)
		{
			if (this.UploadFileCompleted != null)
			{
				this.UploadFileCompleted(this, e);
			}
		}

		// Token: 0x06001AFE RID: 6910 RVA: 0x00063038 File Offset: 0x00061238
		private void UploadFileOperationCompleted(object arg)
		{
			this.OnUploadFileCompleted((UploadFileCompletedEventArgs)arg);
		}

		// Token: 0x06001AFF RID: 6911 RVA: 0x00063048 File Offset: 0x00061248
		private void UploadFileAsyncWriteCallback(byte[] returnBytes, Exception exception, object state)
		{
			WebClient.UploadBitsState uploadBitsState = (WebClient.UploadBitsState)state;
			if (exception != null)
			{
				UploadFileCompletedEventArgs eventArgs = new UploadFileCompletedEventArgs(returnBytes, exception, this.m_Cancelled, uploadBitsState.AsyncOp.UserSuppliedState);
				this.InvokeOperationCompleted(uploadBitsState.AsyncOp, this.uploadFileOperationCompleted, eventArgs);
				return;
			}
			this.StartDownloadAsync(uploadBitsState);
		}

		// Token: 0x06001B00 RID: 6912 RVA: 0x00063094 File Offset: 0x00061294
		private void UploadFileAsyncReadCallback(byte[] returnBytes, Exception exception, object state)
		{
			AsyncOperation asyncOperation = (AsyncOperation)state;
			UploadFileCompletedEventArgs eventArgs = new UploadFileCompletedEventArgs(returnBytes, exception, this.m_Cancelled, asyncOperation.UserSuppliedState);
			this.InvokeOperationCompleted(asyncOperation, this.uploadFileOperationCompleted, eventArgs);
		}

		/// <summary>Uploads the specified local file to the specified resource, using the POST method. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page. </param>
		/// <param name="fileName">The file to send to the resource. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid character, or the specified path to the file does not exist.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001B01 RID: 6913 RVA: 0x000630CA File Offset: 0x000612CA
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadFileAsync(Uri address, string fileName)
		{
			this.UploadFileAsync(address, null, fileName, null);
		}

		/// <summary>Uploads the specified local file to the specified resource, using the POST method. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page. </param>
		/// <param name="method">The method used to send the data to the resource. If <see langword="null" />, the default is POST for http and STOR for ftp.</param>
		/// <param name="fileName">The file to send to the resource. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid character, or the specified path to the file does not exist.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001B02 RID: 6914 RVA: 0x000630D6 File Offset: 0x000612D6
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadFileAsync(Uri address, string method, string fileName)
		{
			this.UploadFileAsync(address, method, fileName, null);
		}

		/// <summary>Uploads the specified local file to the specified resource, using the POST method. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="method">The method used to send the data to the resource. If <see langword="null" />, the default is POST for http and STOR for ftp.</param>
		/// <param name="fileName">The file to send to the resource.</param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid character, or the specified path to the file does not exist.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001B03 RID: 6915 RVA: 0x000630E4 File Offset: 0x000612E4
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadFileAsync(Uri address, string method, string fileName, object userToken)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			if (method == null)
			{
				method = this.MapToDefaultMethod(address);
			}
			this.InitWebClientAsync();
			this.ClearWebClientState();
			AsyncOperation asyncOperation = AsyncOperationManager.CreateOperation(userToken);
			this.m_AsyncOp = asyncOperation;
			FileStream fileStream = null;
			try
			{
				this.m_Method = method;
				byte[] header = null;
				byte[] footer = null;
				byte[] buffer = null;
				Uri uri = this.GetUri(address);
				bool needsHeaderAndBoundary = uri.Scheme != Uri.UriSchemeFile;
				this.OpenFileInternal(needsHeaderAndBoundary, fileName, ref fileStream, ref buffer, ref header, ref footer);
				WebRequest request = this.m_WebRequest = this.GetWebRequest(uri);
				this.UploadBits(request, fileStream, buffer, 0, header, footer, new CompletionDelegate(this.UploadFileAsyncWriteCallback), new CompletionDelegate(this.UploadFileAsyncReadCallback), asyncOperation);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (fileStream != null)
				{
					fileStream.Close();
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				UploadFileCompletedEventArgs eventArgs = new UploadFileCompletedEventArgs(null, ex, this.m_Cancelled, asyncOperation.UserSuppliedState);
				this.InvokeOperationCompleted(asyncOperation, this.uploadFileOperationCompleted, eventArgs);
			}
			bool on2 = Logging.On;
		}

		/// <summary>Occurs when an asynchronous upload of a name/value collection completes.</summary>
		// Token: 0x14000039 RID: 57
		// (add) Token: 0x06001B04 RID: 6916 RVA: 0x00063248 File Offset: 0x00061448
		// (remove) Token: 0x06001B05 RID: 6917 RVA: 0x00063280 File Offset: 0x00061480
		public event UploadValuesCompletedEventHandler UploadValuesCompleted
		{
			[CompilerGenerated]
			add
			{
				UploadValuesCompletedEventHandler uploadValuesCompletedEventHandler = this.UploadValuesCompleted;
				UploadValuesCompletedEventHandler uploadValuesCompletedEventHandler2;
				do
				{
					uploadValuesCompletedEventHandler2 = uploadValuesCompletedEventHandler;
					UploadValuesCompletedEventHandler value2 = (UploadValuesCompletedEventHandler)Delegate.Combine(uploadValuesCompletedEventHandler2, value);
					uploadValuesCompletedEventHandler = Interlocked.CompareExchange<UploadValuesCompletedEventHandler>(ref this.UploadValuesCompleted, value2, uploadValuesCompletedEventHandler2);
				}
				while (uploadValuesCompletedEventHandler != uploadValuesCompletedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				UploadValuesCompletedEventHandler uploadValuesCompletedEventHandler = this.UploadValuesCompleted;
				UploadValuesCompletedEventHandler uploadValuesCompletedEventHandler2;
				do
				{
					uploadValuesCompletedEventHandler2 = uploadValuesCompletedEventHandler;
					UploadValuesCompletedEventHandler value2 = (UploadValuesCompletedEventHandler)Delegate.Remove(uploadValuesCompletedEventHandler2, value);
					uploadValuesCompletedEventHandler = Interlocked.CompareExchange<UploadValuesCompletedEventHandler>(ref this.UploadValuesCompleted, value2, uploadValuesCompletedEventHandler2);
				}
				while (uploadValuesCompletedEventHandler != uploadValuesCompletedEventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.UploadValuesCompleted" /> event.</summary>
		/// <param name="e">A <see cref="T:System.Net.UploadValuesCompletedEventArgs" />  object containing event data.</param>
		// Token: 0x06001B06 RID: 6918 RVA: 0x000632B5 File Offset: 0x000614B5
		protected virtual void OnUploadValuesCompleted(UploadValuesCompletedEventArgs e)
		{
			if (this.UploadValuesCompleted != null)
			{
				this.UploadValuesCompleted(this, e);
			}
		}

		// Token: 0x06001B07 RID: 6919 RVA: 0x000632CC File Offset: 0x000614CC
		private void UploadValuesOperationCompleted(object arg)
		{
			this.OnUploadValuesCompleted((UploadValuesCompletedEventArgs)arg);
		}

		// Token: 0x06001B08 RID: 6920 RVA: 0x000632DC File Offset: 0x000614DC
		private void UploadValuesAsyncWriteCallback(byte[] returnBytes, Exception exception, object state)
		{
			WebClient.UploadBitsState uploadBitsState = (WebClient.UploadBitsState)state;
			if (exception != null)
			{
				UploadValuesCompletedEventArgs eventArgs = new UploadValuesCompletedEventArgs(returnBytes, exception, this.m_Cancelled, uploadBitsState.AsyncOp.UserSuppliedState);
				this.InvokeOperationCompleted(uploadBitsState.AsyncOp, this.uploadValuesOperationCompleted, eventArgs);
				return;
			}
			this.StartDownloadAsync(uploadBitsState);
		}

		// Token: 0x06001B09 RID: 6921 RVA: 0x00063328 File Offset: 0x00061528
		private void UploadValuesAsyncReadCallback(byte[] returnBytes, Exception exception, object state)
		{
			AsyncOperation asyncOperation = (AsyncOperation)state;
			UploadValuesCompletedEventArgs eventArgs = new UploadValuesCompletedEventArgs(returnBytes, exception, this.m_Cancelled, asyncOperation.UserSuppliedState);
			this.InvokeOperationCompleted(asyncOperation, this.uploadValuesOperationCompleted, eventArgs);
		}

		/// <summary>Uploads the data in the specified name/value collection to the resource identified by the specified URI. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the collection. This URI must identify a resource that can accept a request sent with the default method. See remarks.</param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.</exception>
		// Token: 0x06001B0A RID: 6922 RVA: 0x0006335E File Offset: 0x0006155E
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadValuesAsync(Uri address, NameValueCollection data)
		{
			this.UploadValuesAsync(address, null, data, null);
		}

		/// <summary>Uploads the data in the specified name/value collection to the resource identified by the specified URI, using the specified method. This method does not block the calling thread.</summary>
		/// <param name="address">The URI of the resource to receive the collection. This URI must identify a resource that can accept a request sent with the <paramref name="method" /> method.</param>
		/// <param name="method">The method used to send the string to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.-or-
		///         <paramref name="method" /> cannot be used to send content.</exception>
		// Token: 0x06001B0B RID: 6923 RVA: 0x0006336A File Offset: 0x0006156A
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadValuesAsync(Uri address, string method, NameValueCollection data)
		{
			this.UploadValuesAsync(address, method, data, null);
		}

		/// <summary>Uploads the data in the specified name/value collection to the resource identified by the specified URI, using the specified method. This method does not block the calling thread, and allows the caller to pass an object to the method that is invoked when the operation completes.</summary>
		/// <param name="address">The URI of the resource to receive the collection. This URI must identify a resource that can accept a request sent with the <paramref name="method" /> method.</param>
		/// <param name="method">The HTTP method used to send the string to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource.</param>
		/// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.-or-
		///         <paramref name="method" /> cannot be used to send content.</exception>
		// Token: 0x06001B0C RID: 6924 RVA: 0x00063378 File Offset: 0x00061578
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public void UploadValuesAsync(Uri address, string method, NameValueCollection data, object userToken)
		{
			bool on = Logging.On;
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (method == null)
			{
				method = this.MapToDefaultMethod(address);
			}
			this.InitWebClientAsync();
			this.ClearWebClientState();
			AsyncOperation asyncOperation = AsyncOperationManager.CreateOperation(userToken);
			this.m_AsyncOp = asyncOperation;
			int chunkSize = 0;
			try
			{
				byte[] array = this.UploadValuesInternal(data);
				this.m_Method = method;
				WebRequest request = this.m_WebRequest = this.GetWebRequest(this.GetUri(address));
				if (this.UploadProgressChanged != null)
				{
					chunkSize = (int)Math.Min(8192L, (long)array.Length);
				}
				this.UploadBits(request, null, array, chunkSize, null, null, new CompletionDelegate(this.UploadValuesAsyncWriteCallback), new CompletionDelegate(this.UploadValuesAsyncReadCallback), asyncOperation);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException || ex is StackOverflowException || ex is OutOfMemoryException)
				{
					throw;
				}
				if (!(ex is WebException) && !(ex is SecurityException))
				{
					ex = new WebException(SR.GetString("An exception occurred during a WebClient request."), ex);
				}
				UploadValuesCompletedEventArgs eventArgs = new UploadValuesCompletedEventArgs(null, ex, this.m_Cancelled, asyncOperation.UserSuppliedState);
				this.InvokeOperationCompleted(asyncOperation, this.uploadValuesOperationCompleted, eventArgs);
			}
			bool on2 = Logging.On;
		}

		/// <summary>Cancels a pending asynchronous operation.</summary>
		// Token: 0x06001B0D RID: 6925 RVA: 0x000634C0 File Offset: 0x000616C0
		public void CancelAsync()
		{
			WebRequest webRequest = this.m_WebRequest;
			this.m_Cancelled = true;
			WebClient.AbortRequest(webRequest);
		}

		/// <summary>Downloads the resource as a <see cref="T:System.String" /> from the URI specified as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to download.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the downloaded resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		// Token: 0x06001B0E RID: 6926 RVA: 0x000634D4 File Offset: 0x000616D4
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<string> DownloadStringTaskAsync(string address)
		{
			return this.DownloadStringTaskAsync(this.GetUri(address));
		}

		/// <summary>Downloads the resource as a <see cref="T:System.String" /> from the URI specified as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to download.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the downloaded resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		// Token: 0x06001B0F RID: 6927 RVA: 0x000634E4 File Offset: 0x000616E4
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<string> DownloadStringTaskAsync(Uri address)
		{
			TaskCompletionSource<string> tcs = new TaskCompletionSource<string>(address);
			DownloadStringCompletedEventHandler handler = null;
			handler = delegate(object sender, DownloadStringCompletedEventArgs e)
			{
				this.HandleCompletion<DownloadStringCompletedEventArgs, DownloadStringCompletedEventHandler, string>(tcs, e, (DownloadStringCompletedEventArgs args) => args.Result, handler, delegate(WebClient webClient, DownloadStringCompletedEventHandler completion)
				{
					webClient.DownloadStringCompleted -= completion;
				});
			};
			this.DownloadStringCompleted += handler;
			try
			{
				this.DownloadStringAsync(address, tcs);
			}
			catch
			{
				this.DownloadStringCompleted -= handler;
				throw;
			}
			return tcs.Task;
		}

		/// <summary>Opens a readable stream containing the specified resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to retrieve.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.IO.Stream" /> used to read data from a resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and address is invalid.-or- An error occurred while downloading the resource. -or- An error occurred while opening the stream.</exception>
		// Token: 0x06001B10 RID: 6928 RVA: 0x00063568 File Offset: 0x00061768
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<Stream> OpenReadTaskAsync(string address)
		{
			return this.OpenReadTaskAsync(this.GetUri(address));
		}

		/// <summary>Opens a readable stream containing the specified resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to retrieve.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.IO.Stream" /> used to read data from a resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and address is invalid.-or- An error occurred while downloading the resource. -or- An error occurred while opening the stream.</exception>
		// Token: 0x06001B11 RID: 6929 RVA: 0x00063578 File Offset: 0x00061778
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<Stream> OpenReadTaskAsync(Uri address)
		{
			TaskCompletionSource<Stream> tcs = new TaskCompletionSource<Stream>(address);
			OpenReadCompletedEventHandler handler = null;
			handler = delegate(object sender, OpenReadCompletedEventArgs e)
			{
				this.HandleCompletion<OpenReadCompletedEventArgs, OpenReadCompletedEventHandler, Stream>(tcs, e, (OpenReadCompletedEventArgs args) => args.Result, handler, delegate(WebClient webClient, OpenReadCompletedEventHandler completion)
				{
					webClient.OpenReadCompleted -= completion;
				});
			};
			this.OpenReadCompleted += handler;
			try
			{
				this.OpenReadAsync(address, tcs);
			}
			catch
			{
				this.OpenReadCompleted -= handler;
				throw;
			}
			return tcs.Task;
		}

		/// <summary>Opens a stream for writing data to the specified resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.IO.Stream" /> used to write data to the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream. </exception>
		// Token: 0x06001B12 RID: 6930 RVA: 0x000635FC File Offset: 0x000617FC
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<Stream> OpenWriteTaskAsync(string address)
		{
			return this.OpenWriteTaskAsync(this.GetUri(address), null);
		}

		/// <summary>Opens a stream for writing data to the specified resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.IO.Stream" /> used to write data to the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream. </exception>
		// Token: 0x06001B13 RID: 6931 RVA: 0x0006360C File Offset: 0x0006180C
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<Stream> OpenWriteTaskAsync(Uri address)
		{
			return this.OpenWriteTaskAsync(address, null);
		}

		/// <summary>Opens a stream for writing data to the specified resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.IO.Stream" /> used to write data to the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream. </exception>
		// Token: 0x06001B14 RID: 6932 RVA: 0x00063616 File Offset: 0x00061816
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<Stream> OpenWriteTaskAsync(string address, string method)
		{
			return this.OpenWriteTaskAsync(this.GetUri(address), method);
		}

		/// <summary>Opens a stream for writing data to the specified resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.IO.Stream" /> used to write data to the resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream. </exception>
		// Token: 0x06001B15 RID: 6933 RVA: 0x00063628 File Offset: 0x00061828
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<Stream> OpenWriteTaskAsync(Uri address, string method)
		{
			TaskCompletionSource<Stream> tcs = new TaskCompletionSource<Stream>(address);
			OpenWriteCompletedEventHandler handler = null;
			handler = delegate(object sender, OpenWriteCompletedEventArgs e)
			{
				this.HandleCompletion<OpenWriteCompletedEventArgs, OpenWriteCompletedEventHandler, Stream>(tcs, e, (OpenWriteCompletedEventArgs args) => args.Result, handler, delegate(WebClient webClient, OpenWriteCompletedEventHandler completion)
				{
					webClient.OpenWriteCompleted -= completion;
				});
			};
			this.OpenWriteCompleted += handler;
			try
			{
				this.OpenWriteAsync(address, method, tcs);
			}
			catch
			{
				this.OpenWriteCompleted -= handler;
				throw;
			}
			return tcs.Task;
		}

		/// <summary>Uploads the specified string to the specified resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="data">The string to be uploaded.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.String" /> containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.</exception>
		// Token: 0x06001B16 RID: 6934 RVA: 0x000636B0 File Offset: 0x000618B0
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<string> UploadStringTaskAsync(string address, string data)
		{
			return this.UploadStringTaskAsync(address, null, data);
		}

		/// <summary>Uploads the specified string to the specified resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="data">The string to be uploaded.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.String" /> containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.</exception>
		// Token: 0x06001B17 RID: 6935 RVA: 0x000636BB File Offset: 0x000618BB
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<string> UploadStringTaskAsync(Uri address, string data)
		{
			return this.UploadStringTaskAsync(address, null, data);
		}

		/// <summary>Uploads the specified string to the specified resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The string to be uploaded.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.String" /> containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or-
		///         <paramref name="method" /> cannot be used to send content.-or- There was no response from the server hosting the resource.</exception>
		// Token: 0x06001B18 RID: 6936 RVA: 0x000636C6 File Offset: 0x000618C6
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<string> UploadStringTaskAsync(string address, string method, string data)
		{
			return this.UploadStringTaskAsync(this.GetUri(address), method, data);
		}

		/// <summary>Uploads the specified string to the specified resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The string to be uploaded.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.String" /> containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or-
		///         <paramref name="method" /> cannot be used to send content.-or- There was no response from the server hosting the resource.</exception>
		// Token: 0x06001B19 RID: 6937 RVA: 0x000636D8 File Offset: 0x000618D8
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<string> UploadStringTaskAsync(Uri address, string method, string data)
		{
			TaskCompletionSource<string> tcs = new TaskCompletionSource<string>(address);
			UploadStringCompletedEventHandler handler = null;
			handler = delegate(object sender, UploadStringCompletedEventArgs e)
			{
				this.HandleCompletion<UploadStringCompletedEventArgs, UploadStringCompletedEventHandler, string>(tcs, e, (UploadStringCompletedEventArgs args) => args.Result, handler, delegate(WebClient webClient, UploadStringCompletedEventHandler completion)
				{
					webClient.UploadStringCompleted -= completion;
				});
			};
			this.UploadStringCompleted += handler;
			try
			{
				this.UploadStringAsync(address, method, data, tcs);
			}
			catch
			{
				this.UploadStringCompleted -= handler;
				throw;
			}
			return tcs.Task;
		}

		/// <summary>Downloads the resource as a <see cref="T:System.Byte" /> array from the URI specified as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to download. </param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the downloaded resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		// Token: 0x06001B1A RID: 6938 RVA: 0x00063760 File Offset: 0x00061960
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> DownloadDataTaskAsync(string address)
		{
			return this.DownloadDataTaskAsync(this.GetUri(address));
		}

		/// <summary>Downloads the resource as a <see cref="T:System.Byte" /> array from the URI specified as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to download.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the downloaded resource.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		// Token: 0x06001B1B RID: 6939 RVA: 0x00063770 File Offset: 0x00061970
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> DownloadDataTaskAsync(Uri address)
		{
			TaskCompletionSource<byte[]> tcs = new TaskCompletionSource<byte[]>(address);
			DownloadDataCompletedEventHandler handler = null;
			handler = delegate(object sender, DownloadDataCompletedEventArgs e)
			{
				this.HandleCompletion<DownloadDataCompletedEventArgs, DownloadDataCompletedEventHandler, byte[]>(tcs, e, (DownloadDataCompletedEventArgs args) => args.Result, handler, delegate(WebClient webClient, DownloadDataCompletedEventHandler completion)
				{
					webClient.DownloadDataCompleted -= completion;
				});
			};
			this.DownloadDataCompleted += handler;
			try
			{
				this.DownloadDataAsync(address, tcs);
			}
			catch
			{
				this.DownloadDataCompleted -= handler;
				throw;
			}
			return tcs.Task;
		}

		/// <summary>Downloads the specified resource to a local file as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to download.</param>
		/// <param name="fileName">The name of the file to be placed on the local computer.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task" />.The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		/// <exception cref="T:System.InvalidOperationException">The local file specified by <paramref name="fileName" /> is in use by another thread.</exception>
		// Token: 0x06001B1C RID: 6940 RVA: 0x000637F4 File Offset: 0x000619F4
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task DownloadFileTaskAsync(string address, string fileName)
		{
			return this.DownloadFileTaskAsync(this.GetUri(address), fileName);
		}

		/// <summary>Downloads the specified resource to a local file as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to download.</param>
		/// <param name="fileName">The name of the file to be placed on the local computer.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task" />.The task object representing the asynchronous operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while downloading the resource. </exception>
		/// <exception cref="T:System.InvalidOperationException">The local file specified by <paramref name="fileName" /> is in use by another thread.</exception>
		// Token: 0x06001B1D RID: 6941 RVA: 0x00063804 File Offset: 0x00061A04
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task DownloadFileTaskAsync(Uri address, string fileName)
		{
			TaskCompletionSource<object> tcs = new TaskCompletionSource<object>(address);
			AsyncCompletedEventHandler handler = null;
			handler = delegate(object sender, AsyncCompletedEventArgs e)
			{
				this.HandleCompletion<AsyncCompletedEventArgs, AsyncCompletedEventHandler, object>(tcs, e, (AsyncCompletedEventArgs args) => null, handler, delegate(WebClient webClient, AsyncCompletedEventHandler completion)
				{
					webClient.DownloadFileCompleted -= completion;
				});
			};
			this.DownloadFileCompleted += handler;
			try
			{
				this.DownloadFileAsync(address, fileName, tcs);
			}
			catch
			{
				this.DownloadFileCompleted -= handler;
				throw;
			}
			return tcs.Task;
		}

		/// <summary>Uploads a data buffer that contains a <see cref="T:System.Byte" /> array to the URI specified as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <param name="data">The data buffer to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the body of the response received from the resource when the data buffer was uploaded.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001B1E RID: 6942 RVA: 0x0006388C File Offset: 0x00061A8C
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadDataTaskAsync(string address, byte[] data)
		{
			return this.UploadDataTaskAsync(this.GetUri(address), null, data);
		}

		/// <summary>Uploads a data buffer that contains a <see cref="T:System.Byte" /> array to the URI specified as an asynchronous operation using a task object.  </summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <param name="data">The data buffer to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the body of the response received from the resource when the data buffer was uploaded.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001B1F RID: 6943 RVA: 0x0006389D File Offset: 0x00061A9D
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadDataTaskAsync(Uri address, byte[] data)
		{
			return this.UploadDataTaskAsync(address, null, data);
		}

		/// <summary>Uploads a data buffer that contains a <see cref="T:System.Byte" /> array to the URI specified as an asynchronous operation using a task object.  </summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <param name="method">The method used to send the data to the resource. If <see langword="null" />, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The data buffer to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the body of the response received from the resource when the data buffer was uploaded.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001B20 RID: 6944 RVA: 0x000638A8 File Offset: 0x00061AA8
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadDataTaskAsync(string address, string method, byte[] data)
		{
			return this.UploadDataTaskAsync(this.GetUri(address), method, data);
		}

		/// <summary>Uploads a data buffer that contains a <see cref="T:System.Byte" /> array to the URI specified as an asynchronous operation using a task object.  </summary>
		/// <param name="address">The URI of the resource to receive the data.</param>
		/// <param name="method">The method used to send the data to the resource. If <see langword="null" />, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The data buffer to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the body of the response received from the resource when the data buffer was uploaded.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource. </exception>
		// Token: 0x06001B21 RID: 6945 RVA: 0x000638BC File Offset: 0x00061ABC
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadDataTaskAsync(Uri address, string method, byte[] data)
		{
			TaskCompletionSource<byte[]> tcs = new TaskCompletionSource<byte[]>(address);
			UploadDataCompletedEventHandler handler = null;
			handler = delegate(object sender, UploadDataCompletedEventArgs e)
			{
				this.HandleCompletion<UploadDataCompletedEventArgs, UploadDataCompletedEventHandler, byte[]>(tcs, e, (UploadDataCompletedEventArgs args) => args.Result, handler, delegate(WebClient webClient, UploadDataCompletedEventHandler completion)
				{
					webClient.UploadDataCompleted -= completion;
				});
			};
			this.UploadDataCompleted += handler;
			try
			{
				this.UploadDataAsync(address, method, data, tcs);
			}
			catch
			{
				this.UploadDataCompleted -= handler;
				throw;
			}
			return tcs.Task;
		}

		/// <summary>Uploads the specified local file to a resource as an asynchronous operation using a task object. </summary>
		/// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="fileName">The local file to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the body of the response received from the resource when the file was uploaded.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid character, or the specified path to the file does not exist.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001B22 RID: 6946 RVA: 0x00063944 File Offset: 0x00061B44
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadFileTaskAsync(string address, string fileName)
		{
			return this.UploadFileTaskAsync(this.GetUri(address), null, fileName);
		}

		/// <summary>Uploads the specified local file to a resource as an asynchronous operation using a task object. </summary>
		/// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="fileName">The local file to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the body of the response received from the resource when the file was uploaded.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid character, or the specified path to the file does not exist.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001B23 RID: 6947 RVA: 0x00063955 File Offset: 0x00061B55
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadFileTaskAsync(Uri address, string fileName)
		{
			return this.UploadFileTaskAsync(address, null, fileName);
		}

		/// <summary>Uploads the specified local file to a resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="method">The method used to send the data to the resource. If <see langword="null" />, the default is POST for http and STOR for ftp.</param>
		/// <param name="fileName">The local file to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the body of the response received from the resource when the file was uploaded.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid character, or the specified path to the file does not exist.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001B24 RID: 6948 RVA: 0x00063960 File Offset: 0x00061B60
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadFileTaskAsync(string address, string method, string fileName)
		{
			return this.UploadFileTaskAsync(this.GetUri(address), method, fileName);
		}

		/// <summary>Uploads the specified local file to a resource as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
		/// <param name="method">The method used to send the data to the resource. If <see langword="null" />, the default is POST for http and STOR for ftp.</param>
		/// <param name="fileName">The local file to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the body of the response received from the resource when the file was uploaded.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />. -or-The <paramref name="fileName" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" /> and <paramref name="address" /> is invalid.-or- 
		///         <paramref name="fileName" /> is <see langword="null" />, is <see cref="F:System.String.Empty" />, contains invalid character, or the specified path to the file does not exist.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header begins with <see langword="multipart" />. </exception>
		// Token: 0x06001B25 RID: 6949 RVA: 0x00063974 File Offset: 0x00061B74
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadFileTaskAsync(Uri address, string method, string fileName)
		{
			TaskCompletionSource<byte[]> tcs = new TaskCompletionSource<byte[]>(address);
			UploadFileCompletedEventHandler handler = null;
			handler = delegate(object sender, UploadFileCompletedEventArgs e)
			{
				this.HandleCompletion<UploadFileCompletedEventArgs, UploadFileCompletedEventHandler, byte[]>(tcs, e, (UploadFileCompletedEventArgs args) => args.Result, handler, delegate(WebClient webClient, UploadFileCompletedEventHandler completion)
				{
					webClient.UploadFileCompleted -= completion;
				});
			};
			this.UploadFileCompleted += handler;
			try
			{
				this.UploadFileAsync(address, method, fileName, tcs);
			}
			catch
			{
				this.UploadFileCompleted -= handler;
				throw;
			}
			return tcs.Task;
		}

		/// <summary>Uploads the specified name/value collection to the resource identified by the specified URI as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the collection.</param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- There was no response from the server hosting the resource.-or- An error occurred while opening the stream.-or- The <see langword="Content-type" /> header is not <see langword="null" /> or "application/x-www-form-urlencoded". </exception>
		// Token: 0x06001B26 RID: 6950 RVA: 0x000639FC File Offset: 0x00061BFC
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadValuesTaskAsync(string address, NameValueCollection data)
		{
			return this.UploadValuesTaskAsync(this.GetUri(address), null, data);
		}

		/// <summary>Uploads the specified name/value collection to the resource identified by the specified URI as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the collection.</param>
		/// <param name="method">The HTTP method used to send the collection to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or-
		///         <paramref name="method" /> cannot be used to send content.-or- There was no response from the server hosting the resource.-or- An error occurred while opening the stream.-or- The <see langword="Content-type" /> header is not <see langword="null" /> or "application/x-www-form-urlencoded". </exception>
		// Token: 0x06001B27 RID: 6951 RVA: 0x00063A0D File Offset: 0x00061C0D
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadValuesTaskAsync(string address, string method, NameValueCollection data)
		{
			return this.UploadValuesTaskAsync(this.GetUri(address), method, data);
		}

		/// <summary>Uploads the specified name/value collection to the resource identified by the specified URI as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the collection.</param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or- An error occurred while opening the stream.-or- There was no response from the server hosting the resource.-or- The <see langword="Content-type" /> header value is not <see langword="null" /> and is not <see langword="application/x-www-form-urlencoded" />. </exception>
		// Token: 0x06001B28 RID: 6952 RVA: 0x00063A1E File Offset: 0x00061C1E
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadValuesTaskAsync(Uri address, NameValueCollection data)
		{
			return this.UploadValuesTaskAsync(address, null, data);
		}

		/// <summary>Uploads the specified name/value collection to the resource identified by the specified URI as an asynchronous operation using a task object.</summary>
		/// <param name="address">The URI of the resource to receive the collection.</param>
		/// <param name="method">The HTTP method used to send the collection to the resource. If null, the default is POST for http and STOR for ftp.</param>
		/// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection" /> to send to the resource.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns a <see cref="T:System.Byte" /> array containing the response sent by the server.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="address" /> parameter is <see langword="null" />.-or-The <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress" />, and <paramref name="address" /> is invalid.-or-
		///         <paramref name="method" /> cannot be used to send content.-or- There was no response from the server hosting the resource.-or- An error occurred while opening the stream.-or- The <see langword="Content-type" /> header is not <see langword="null" /> or "application/x-www-form-urlencoded". </exception>
		// Token: 0x06001B29 RID: 6953 RVA: 0x00063A2C File Offset: 0x00061C2C
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public Task<byte[]> UploadValuesTaskAsync(Uri address, string method, NameValueCollection data)
		{
			TaskCompletionSource<byte[]> tcs = new TaskCompletionSource<byte[]>(address);
			UploadValuesCompletedEventHandler handler = null;
			handler = delegate(object sender, UploadValuesCompletedEventArgs e)
			{
				this.HandleCompletion<UploadValuesCompletedEventArgs, UploadValuesCompletedEventHandler, byte[]>(tcs, e, (UploadValuesCompletedEventArgs args) => args.Result, handler, delegate(WebClient webClient, UploadValuesCompletedEventHandler completion)
				{
					webClient.UploadValuesCompleted -= completion;
				});
			};
			this.UploadValuesCompleted += handler;
			try
			{
				this.UploadValuesAsync(address, method, data, tcs);
			}
			catch
			{
				this.UploadValuesCompleted -= handler;
				throw;
			}
			return tcs.Task;
		}

		// Token: 0x06001B2A RID: 6954 RVA: 0x00063AB4 File Offset: 0x00061CB4
		private void HandleCompletion<TAsyncCompletedEventArgs, TCompletionDelegate, T>(TaskCompletionSource<T> tcs, TAsyncCompletedEventArgs e, Func<TAsyncCompletedEventArgs, T> getResult, TCompletionDelegate handler, Action<WebClient, TCompletionDelegate> unregisterHandler) where TAsyncCompletedEventArgs : AsyncCompletedEventArgs
		{
			if (e.UserState == tcs)
			{
				try
				{
					unregisterHandler(this, handler);
				}
				finally
				{
					if (e.Error != null)
					{
						tcs.TrySetException(e.Error);
					}
					else if (e.Cancelled)
					{
						tcs.TrySetCanceled();
					}
					else
					{
						tcs.TrySetResult(getResult(e));
					}
				}
			}
		}

		/// <summary>Occurs when an asynchronous download operation successfully transfers some or all of the data.</summary>
		// Token: 0x1400003A RID: 58
		// (add) Token: 0x06001B2B RID: 6955 RVA: 0x00063B34 File Offset: 0x00061D34
		// (remove) Token: 0x06001B2C RID: 6956 RVA: 0x00063B6C File Offset: 0x00061D6C
		public event DownloadProgressChangedEventHandler DownloadProgressChanged
		{
			[CompilerGenerated]
			add
			{
				DownloadProgressChangedEventHandler downloadProgressChangedEventHandler = this.DownloadProgressChanged;
				DownloadProgressChangedEventHandler downloadProgressChangedEventHandler2;
				do
				{
					downloadProgressChangedEventHandler2 = downloadProgressChangedEventHandler;
					DownloadProgressChangedEventHandler value2 = (DownloadProgressChangedEventHandler)Delegate.Combine(downloadProgressChangedEventHandler2, value);
					downloadProgressChangedEventHandler = Interlocked.CompareExchange<DownloadProgressChangedEventHandler>(ref this.DownloadProgressChanged, value2, downloadProgressChangedEventHandler2);
				}
				while (downloadProgressChangedEventHandler != downloadProgressChangedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				DownloadProgressChangedEventHandler downloadProgressChangedEventHandler = this.DownloadProgressChanged;
				DownloadProgressChangedEventHandler downloadProgressChangedEventHandler2;
				do
				{
					downloadProgressChangedEventHandler2 = downloadProgressChangedEventHandler;
					DownloadProgressChangedEventHandler value2 = (DownloadProgressChangedEventHandler)Delegate.Remove(downloadProgressChangedEventHandler2, value);
					downloadProgressChangedEventHandler = Interlocked.CompareExchange<DownloadProgressChangedEventHandler>(ref this.DownloadProgressChanged, value2, downloadProgressChangedEventHandler2);
				}
				while (downloadProgressChangedEventHandler != downloadProgressChangedEventHandler2);
			}
		}

		/// <summary>Occurs when an asynchronous upload operation successfully transfers some or all of the data.</summary>
		// Token: 0x1400003B RID: 59
		// (add) Token: 0x06001B2D RID: 6957 RVA: 0x00063BA4 File Offset: 0x00061DA4
		// (remove) Token: 0x06001B2E RID: 6958 RVA: 0x00063BDC File Offset: 0x00061DDC
		public event UploadProgressChangedEventHandler UploadProgressChanged
		{
			[CompilerGenerated]
			add
			{
				UploadProgressChangedEventHandler uploadProgressChangedEventHandler = this.UploadProgressChanged;
				UploadProgressChangedEventHandler uploadProgressChangedEventHandler2;
				do
				{
					uploadProgressChangedEventHandler2 = uploadProgressChangedEventHandler;
					UploadProgressChangedEventHandler value2 = (UploadProgressChangedEventHandler)Delegate.Combine(uploadProgressChangedEventHandler2, value);
					uploadProgressChangedEventHandler = Interlocked.CompareExchange<UploadProgressChangedEventHandler>(ref this.UploadProgressChanged, value2, uploadProgressChangedEventHandler2);
				}
				while (uploadProgressChangedEventHandler != uploadProgressChangedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				UploadProgressChangedEventHandler uploadProgressChangedEventHandler = this.UploadProgressChanged;
				UploadProgressChangedEventHandler uploadProgressChangedEventHandler2;
				do
				{
					uploadProgressChangedEventHandler2 = uploadProgressChangedEventHandler;
					UploadProgressChangedEventHandler value2 = (UploadProgressChangedEventHandler)Delegate.Remove(uploadProgressChangedEventHandler2, value);
					uploadProgressChangedEventHandler = Interlocked.CompareExchange<UploadProgressChangedEventHandler>(ref this.UploadProgressChanged, value2, uploadProgressChangedEventHandler2);
				}
				while (uploadProgressChangedEventHandler != uploadProgressChangedEventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.DownloadProgressChanged" /> event.</summary>
		/// <param name="e">A <see cref="T:System.Net.DownloadProgressChangedEventArgs" /> object containing event data.</param>
		// Token: 0x06001B2F RID: 6959 RVA: 0x00063C11 File Offset: 0x00061E11
		protected virtual void OnDownloadProgressChanged(DownloadProgressChangedEventArgs e)
		{
			if (this.DownloadProgressChanged != null)
			{
				this.DownloadProgressChanged(this, e);
			}
		}

		/// <summary>Raises the <see cref="E:System.Net.WebClient.UploadProgressChanged" /> event.</summary>
		/// <param name="e">An <see cref="T:System.Net.UploadProgressChangedEventArgs" /> object containing event data.</param>
		// Token: 0x06001B30 RID: 6960 RVA: 0x00063C28 File Offset: 0x00061E28
		protected virtual void OnUploadProgressChanged(UploadProgressChangedEventArgs e)
		{
			if (this.UploadProgressChanged != null)
			{
				this.UploadProgressChanged(this, e);
			}
		}

		// Token: 0x06001B31 RID: 6961 RVA: 0x00063C3F File Offset: 0x00061E3F
		private void ReportDownloadProgressChanged(object arg)
		{
			this.OnDownloadProgressChanged((DownloadProgressChangedEventArgs)arg);
		}

		// Token: 0x06001B32 RID: 6962 RVA: 0x00063C4D File Offset: 0x00061E4D
		private void ReportUploadProgressChanged(object arg)
		{
			this.OnUploadProgressChanged((UploadProgressChangedEventArgs)arg);
		}

		// Token: 0x06001B33 RID: 6963 RVA: 0x00063C5C File Offset: 0x00061E5C
		private void PostProgressChanged(AsyncOperation asyncOp, WebClient.ProgressData progress)
		{
			if (asyncOp != null && progress.BytesSent + progress.BytesReceived > 0L)
			{
				int progressPercentage;
				if (progress.HasUploadPhase)
				{
					if (progress.TotalBytesToReceive < 0L && progress.BytesReceived == 0L)
					{
						progressPercentage = ((progress.TotalBytesToSend < 0L) ? 0 : ((progress.TotalBytesToSend == 0L) ? 50 : ((int)(50L * progress.BytesSent / progress.TotalBytesToSend))));
					}
					else
					{
						progressPercentage = ((progress.TotalBytesToSend < 0L) ? 50 : ((progress.TotalBytesToReceive == 0L) ? 100 : ((int)(50L * progress.BytesReceived / progress.TotalBytesToReceive + 50L))));
					}
					asyncOp.Post(this.reportUploadProgressChanged, new UploadProgressChangedEventArgs(progressPercentage, asyncOp.UserSuppliedState, progress.BytesSent, progress.TotalBytesToSend, progress.BytesReceived, progress.TotalBytesToReceive));
					return;
				}
				progressPercentage = ((progress.TotalBytesToReceive < 0L) ? 0 : ((progress.TotalBytesToReceive == 0L) ? 100 : ((int)(100L * progress.BytesReceived / progress.TotalBytesToReceive))));
				asyncOp.Post(this.reportDownloadProgressChanged, new DownloadProgressChangedEventArgs(progressPercentage, asyncOp.UserSuppliedState, progress.BytesReceived, progress.TotalBytesToReceive));
			}
		}

		// Token: 0x040018A1 RID: 6305
		private const int DefaultCopyBufferLength = 8192;

		// Token: 0x040018A2 RID: 6306
		private const int DefaultDownloadBufferLength = 65536;

		// Token: 0x040018A3 RID: 6307
		private const string DefaultUploadFileContentType = "application/octet-stream";

		// Token: 0x040018A4 RID: 6308
		private const string UploadFileContentType = "multipart/form-data";

		// Token: 0x040018A5 RID: 6309
		private const string UploadValuesContentType = "application/x-www-form-urlencoded";

		// Token: 0x040018A6 RID: 6310
		private Uri m_baseAddress;

		// Token: 0x040018A7 RID: 6311
		private ICredentials m_credentials;

		// Token: 0x040018A8 RID: 6312
		private WebHeaderCollection m_headers;

		// Token: 0x040018A9 RID: 6313
		private NameValueCollection m_requestParameters;

		// Token: 0x040018AA RID: 6314
		private WebResponse m_WebResponse;

		// Token: 0x040018AB RID: 6315
		private WebRequest m_WebRequest;

		// Token: 0x040018AC RID: 6316
		private Encoding m_Encoding = Encoding.Default;

		// Token: 0x040018AD RID: 6317
		private string m_Method;

		// Token: 0x040018AE RID: 6318
		private long m_ContentLength = -1L;

		// Token: 0x040018AF RID: 6319
		private bool m_InitWebClientAsync;

		// Token: 0x040018B0 RID: 6320
		private bool m_Cancelled;

		// Token: 0x040018B1 RID: 6321
		private WebClient.ProgressData m_Progress;

		// Token: 0x040018B2 RID: 6322
		private IWebProxy m_Proxy;

		// Token: 0x040018B3 RID: 6323
		private bool m_ProxySet;

		// Token: 0x040018B4 RID: 6324
		private RequestCachePolicy m_CachePolicy;

		// Token: 0x040018B5 RID: 6325
		[CompilerGenerated]
		private bool <AllowReadStreamBuffering>k__BackingField;

		// Token: 0x040018B6 RID: 6326
		[CompilerGenerated]
		private bool <AllowWriteStreamBuffering>k__BackingField;

		// Token: 0x040018B7 RID: 6327
		private int m_CallNesting;

		// Token: 0x040018B8 RID: 6328
		private AsyncOperation m_AsyncOp;

		// Token: 0x040018B9 RID: 6329
		[CompilerGenerated]
		private OpenReadCompletedEventHandler OpenReadCompleted;

		// Token: 0x040018BA RID: 6330
		private SendOrPostCallback openReadOperationCompleted;

		// Token: 0x040018BB RID: 6331
		[CompilerGenerated]
		private OpenWriteCompletedEventHandler OpenWriteCompleted;

		// Token: 0x040018BC RID: 6332
		private SendOrPostCallback openWriteOperationCompleted;

		// Token: 0x040018BD RID: 6333
		[CompilerGenerated]
		private DownloadStringCompletedEventHandler DownloadStringCompleted;

		// Token: 0x040018BE RID: 6334
		private SendOrPostCallback downloadStringOperationCompleted;

		// Token: 0x040018BF RID: 6335
		[CompilerGenerated]
		private DownloadDataCompletedEventHandler DownloadDataCompleted;

		// Token: 0x040018C0 RID: 6336
		private SendOrPostCallback downloadDataOperationCompleted;

		// Token: 0x040018C1 RID: 6337
		[CompilerGenerated]
		private AsyncCompletedEventHandler DownloadFileCompleted;

		// Token: 0x040018C2 RID: 6338
		private SendOrPostCallback downloadFileOperationCompleted;

		// Token: 0x040018C3 RID: 6339
		[CompilerGenerated]
		private UploadStringCompletedEventHandler UploadStringCompleted;

		// Token: 0x040018C4 RID: 6340
		private SendOrPostCallback uploadStringOperationCompleted;

		// Token: 0x040018C5 RID: 6341
		[CompilerGenerated]
		private UploadDataCompletedEventHandler UploadDataCompleted;

		// Token: 0x040018C6 RID: 6342
		private SendOrPostCallback uploadDataOperationCompleted;

		// Token: 0x040018C7 RID: 6343
		[CompilerGenerated]
		private UploadFileCompletedEventHandler UploadFileCompleted;

		// Token: 0x040018C8 RID: 6344
		private SendOrPostCallback uploadFileOperationCompleted;

		// Token: 0x040018C9 RID: 6345
		[CompilerGenerated]
		private UploadValuesCompletedEventHandler UploadValuesCompleted;

		// Token: 0x040018CA RID: 6346
		private SendOrPostCallback uploadValuesOperationCompleted;

		// Token: 0x040018CB RID: 6347
		[CompilerGenerated]
		private DownloadProgressChangedEventHandler DownloadProgressChanged;

		// Token: 0x040018CC RID: 6348
		[CompilerGenerated]
		private UploadProgressChangedEventHandler UploadProgressChanged;

		// Token: 0x040018CD RID: 6349
		private SendOrPostCallback reportDownloadProgressChanged;

		// Token: 0x040018CE RID: 6350
		private SendOrPostCallback reportUploadProgressChanged;

		// Token: 0x02000390 RID: 912
		private class ProgressData
		{
			// Token: 0x06001B34 RID: 6964 RVA: 0x00063D80 File Offset: 0x00061F80
			internal void Reset()
			{
				this.BytesSent = 0L;
				this.TotalBytesToSend = -1L;
				this.BytesReceived = 0L;
				this.TotalBytesToReceive = -1L;
				this.HasUploadPhase = false;
			}

			// Token: 0x06001B35 RID: 6965 RVA: 0x00063DA9 File Offset: 0x00061FA9
			public ProgressData()
			{
			}

			// Token: 0x040018CF RID: 6351
			internal long BytesSent;

			// Token: 0x040018D0 RID: 6352
			internal long TotalBytesToSend = -1L;

			// Token: 0x040018D1 RID: 6353
			internal long BytesReceived;

			// Token: 0x040018D2 RID: 6354
			internal long TotalBytesToReceive = -1L;

			// Token: 0x040018D3 RID: 6355
			internal bool HasUploadPhase;
		}

		// Token: 0x02000391 RID: 913
		private class DownloadBitsState
		{
			// Token: 0x06001B36 RID: 6966 RVA: 0x00063DC1 File Offset: 0x00061FC1
			internal DownloadBitsState(WebRequest request, Stream writeStream, CompletionDelegate completionDelegate, AsyncOperation asyncOp, WebClient.ProgressData progress, WebClient webClient)
			{
				this.WriteStream = writeStream;
				this.Request = request;
				this.AsyncOp = asyncOp;
				this.CompletionDelegate = completionDelegate;
				this.WebClient = webClient;
				this.Progress = progress;
			}

			// Token: 0x17000591 RID: 1425
			// (get) Token: 0x06001B37 RID: 6967 RVA: 0x00063DF6 File Offset: 0x00061FF6
			internal bool Async
			{
				get
				{
					return this.AsyncOp != null;
				}
			}

			// Token: 0x06001B38 RID: 6968 RVA: 0x00063E04 File Offset: 0x00062004
			internal int SetResponse(WebResponse response)
			{
				this.ContentLength = response.ContentLength;
				if (this.ContentLength == -1L || this.ContentLength > 65536L)
				{
					this.Length = 65536L;
				}
				else
				{
					this.Length = this.ContentLength;
				}
				if (this.WriteStream == null)
				{
					if (this.ContentLength > 2147483647L)
					{
						throw new WebException(SR.GetString("The message length limit was exceeded"), WebExceptionStatus.MessageLengthLimitExceeded);
					}
					this.SgBuffers = new ScatterGatherBuffers(this.Length);
				}
				this.InnerBuffer = new byte[(int)this.Length];
				this.ReadStream = response.GetResponseStream();
				if (this.Async && response.ContentLength >= 0L)
				{
					this.Progress.TotalBytesToReceive = response.ContentLength;
				}
				if (this.Async)
				{
					if (this.ReadStream == null || this.ReadStream == Stream.Null)
					{
						WebClient.DownloadBitsReadCallbackState(this, null);
					}
					else
					{
						this.ReadStream.BeginRead(this.InnerBuffer, 0, (int)this.Length, new AsyncCallback(WebClient.DownloadBitsReadCallback), this);
					}
					return -1;
				}
				if (this.ReadStream == null || this.ReadStream == Stream.Null)
				{
					return 0;
				}
				return this.ReadStream.Read(this.InnerBuffer, 0, (int)this.Length);
			}

			// Token: 0x06001B39 RID: 6969 RVA: 0x00063F4C File Offset: 0x0006214C
			internal bool RetrieveBytes(ref int bytesRetrieved)
			{
				if (bytesRetrieved > 0)
				{
					if (this.WriteStream != null)
					{
						this.WriteStream.Write(this.InnerBuffer, 0, bytesRetrieved);
					}
					else
					{
						this.SgBuffers.Write(this.InnerBuffer, 0, bytesRetrieved);
					}
					if (this.Async)
					{
						this.Progress.BytesReceived += (long)bytesRetrieved;
					}
					if (this.ContentLength != 0L)
					{
						if (this.Async)
						{
							this.WebClient.PostProgressChanged(this.AsyncOp, this.Progress);
							this.ReadStream.BeginRead(this.InnerBuffer, 0, (int)this.Length, new AsyncCallback(WebClient.DownloadBitsReadCallback), this);
						}
						else
						{
							bytesRetrieved = this.ReadStream.Read(this.InnerBuffer, 0, (int)this.Length);
						}
						return false;
					}
				}
				if (this.Async)
				{
					if (this.Progress.TotalBytesToReceive < 0L)
					{
						this.Progress.TotalBytesToReceive = this.Progress.BytesReceived;
					}
					this.WebClient.PostProgressChanged(this.AsyncOp, this.Progress);
				}
				if (this.ReadStream != null)
				{
					this.ReadStream.Close();
				}
				if (this.WriteStream != null)
				{
					this.WriteStream.Close();
				}
				else if (this.WriteStream == null)
				{
					byte[] array = new byte[this.SgBuffers.Length];
					if (this.SgBuffers.Length > 0)
					{
						BufferOffsetSize[] buffers = this.SgBuffers.GetBuffers();
						int num = 0;
						foreach (BufferOffsetSize bufferOffsetSize in buffers)
						{
							Buffer.BlockCopy(bufferOffsetSize.Buffer, 0, array, num, bufferOffsetSize.Size);
							num += bufferOffsetSize.Size;
						}
					}
					this.InnerBuffer = array;
				}
				return true;
			}

			// Token: 0x06001B3A RID: 6970 RVA: 0x000640F9 File Offset: 0x000622F9
			internal void Close()
			{
				if (this.WriteStream != null)
				{
					this.WriteStream.Close();
				}
				if (this.ReadStream != null)
				{
					this.ReadStream.Close();
				}
			}

			// Token: 0x040018D4 RID: 6356
			internal WebClient WebClient;

			// Token: 0x040018D5 RID: 6357
			internal Stream WriteStream;

			// Token: 0x040018D6 RID: 6358
			internal byte[] InnerBuffer;

			// Token: 0x040018D7 RID: 6359
			internal AsyncOperation AsyncOp;

			// Token: 0x040018D8 RID: 6360
			internal WebRequest Request;

			// Token: 0x040018D9 RID: 6361
			internal CompletionDelegate CompletionDelegate;

			// Token: 0x040018DA RID: 6362
			internal Stream ReadStream;

			// Token: 0x040018DB RID: 6363
			internal ScatterGatherBuffers SgBuffers;

			// Token: 0x040018DC RID: 6364
			internal long ContentLength;

			// Token: 0x040018DD RID: 6365
			internal long Length;

			// Token: 0x040018DE RID: 6366
			private const int Offset = 0;

			// Token: 0x040018DF RID: 6367
			internal WebClient.ProgressData Progress;
		}

		// Token: 0x02000392 RID: 914
		private class UploadBitsState
		{
			// Token: 0x06001B3B RID: 6971 RVA: 0x00064124 File Offset: 0x00062324
			internal UploadBitsState(WebRequest request, Stream readStream, byte[] buffer, int chunkSize, byte[] header, byte[] footer, CompletionDelegate uploadCompletionDelegate, CompletionDelegate downloadCompletionDelegate, AsyncOperation asyncOp, WebClient.ProgressData progress, WebClient webClient)
			{
				this.InnerBuffer = buffer;
				this.m_ChunkSize = chunkSize;
				this.m_BufferWritePosition = 0;
				this.Header = header;
				this.Footer = footer;
				this.ReadStream = readStream;
				this.Request = request;
				this.AsyncOp = asyncOp;
				this.UploadCompletionDelegate = uploadCompletionDelegate;
				this.DownloadCompletionDelegate = downloadCompletionDelegate;
				if (this.AsyncOp != null)
				{
					this.Progress = progress;
					this.Progress.HasUploadPhase = true;
					this.Progress.TotalBytesToSend = ((request.ContentLength < 0L) ? -1L : request.ContentLength);
				}
				this.WebClient = webClient;
			}

			// Token: 0x17000592 RID: 1426
			// (get) Token: 0x06001B3C RID: 6972 RVA: 0x000641C6 File Offset: 0x000623C6
			internal bool FileUpload
			{
				get
				{
					return this.ReadStream != null;
				}
			}

			// Token: 0x17000593 RID: 1427
			// (get) Token: 0x06001B3D RID: 6973 RVA: 0x000641D1 File Offset: 0x000623D1
			internal bool Async
			{
				get
				{
					return this.AsyncOp != null;
				}
			}

			// Token: 0x06001B3E RID: 6974 RVA: 0x000641DC File Offset: 0x000623DC
			internal void SetRequestStream(Stream writeStream)
			{
				this.WriteStream = writeStream;
				byte[] array;
				if (this.Header != null)
				{
					array = this.Header;
					this.Header = null;
				}
				else
				{
					array = new byte[0];
				}
				if (this.Async)
				{
					this.Progress.BytesSent += (long)array.Length;
					this.WriteStream.BeginWrite(array, 0, array.Length, new AsyncCallback(WebClient.UploadBitsWriteCallback), this);
					return;
				}
				this.WriteStream.Write(array, 0, array.Length);
			}

			// Token: 0x06001B3F RID: 6975 RVA: 0x00064260 File Offset: 0x00062460
			internal bool WriteBytes()
			{
				int num = 0;
				if (this.Async)
				{
					this.WebClient.PostProgressChanged(this.AsyncOp, this.Progress);
				}
				int num3;
				byte[] buffer;
				if (this.FileUpload)
				{
					int num2 = 0;
					if (this.InnerBuffer != null)
					{
						num2 = this.ReadStream.Read(this.InnerBuffer, 0, this.InnerBuffer.Length);
						if (num2 <= 0)
						{
							this.ReadStream.Close();
							this.InnerBuffer = null;
						}
					}
					if (this.InnerBuffer != null)
					{
						num3 = num2;
						buffer = this.InnerBuffer;
					}
					else
					{
						if (this.Footer == null)
						{
							return true;
						}
						num3 = this.Footer.Length;
						buffer = this.Footer;
						this.Footer = null;
					}
				}
				else
				{
					if (this.InnerBuffer == null)
					{
						return true;
					}
					buffer = this.InnerBuffer;
					if (this.m_ChunkSize != 0)
					{
						num = this.m_BufferWritePosition;
						this.m_BufferWritePosition += this.m_ChunkSize;
						num3 = this.m_ChunkSize;
						if (this.m_BufferWritePosition >= this.InnerBuffer.Length)
						{
							num3 = this.InnerBuffer.Length - num;
							this.InnerBuffer = null;
						}
					}
					else
					{
						num3 = this.InnerBuffer.Length;
						this.InnerBuffer = null;
					}
				}
				if (this.Async)
				{
					this.Progress.BytesSent += (long)num3;
					this.WriteStream.BeginWrite(buffer, num, num3, new AsyncCallback(WebClient.UploadBitsWriteCallback), this);
				}
				else
				{
					this.WriteStream.Write(buffer, 0, num3);
				}
				return false;
			}

			// Token: 0x06001B40 RID: 6976 RVA: 0x000643C7 File Offset: 0x000625C7
			internal void Close()
			{
				if (this.WriteStream != null)
				{
					this.WriteStream.Close();
				}
				if (this.ReadStream != null)
				{
					this.ReadStream.Close();
				}
			}

			// Token: 0x040018E0 RID: 6368
			private int m_ChunkSize;

			// Token: 0x040018E1 RID: 6369
			private int m_BufferWritePosition;

			// Token: 0x040018E2 RID: 6370
			internal WebClient WebClient;

			// Token: 0x040018E3 RID: 6371
			internal Stream WriteStream;

			// Token: 0x040018E4 RID: 6372
			internal byte[] InnerBuffer;

			// Token: 0x040018E5 RID: 6373
			internal byte[] Header;

			// Token: 0x040018E6 RID: 6374
			internal byte[] Footer;

			// Token: 0x040018E7 RID: 6375
			internal AsyncOperation AsyncOp;

			// Token: 0x040018E8 RID: 6376
			internal WebRequest Request;

			// Token: 0x040018E9 RID: 6377
			internal CompletionDelegate UploadCompletionDelegate;

			// Token: 0x040018EA RID: 6378
			internal CompletionDelegate DownloadCompletionDelegate;

			// Token: 0x040018EB RID: 6379
			internal Stream ReadStream;

			// Token: 0x040018EC RID: 6380
			internal WebClient.ProgressData Progress;
		}

		// Token: 0x02000393 RID: 915
		private class WebClientWriteStream : Stream
		{
			// Token: 0x06001B41 RID: 6977 RVA: 0x000643EF File Offset: 0x000625EF
			public WebClientWriteStream(Stream stream, WebRequest request, WebClient webClient)
			{
				this.m_request = request;
				this.m_stream = stream;
				this.m_WebClient = webClient;
			}

			// Token: 0x17000594 RID: 1428
			// (get) Token: 0x06001B42 RID: 6978 RVA: 0x0006440C File Offset: 0x0006260C
			public override bool CanRead
			{
				get
				{
					return this.m_stream.CanRead;
				}
			}

			// Token: 0x17000595 RID: 1429
			// (get) Token: 0x06001B43 RID: 6979 RVA: 0x00064419 File Offset: 0x00062619
			public override bool CanSeek
			{
				get
				{
					return this.m_stream.CanSeek;
				}
			}

			// Token: 0x17000596 RID: 1430
			// (get) Token: 0x06001B44 RID: 6980 RVA: 0x00064426 File Offset: 0x00062626
			public override bool CanWrite
			{
				get
				{
					return this.m_stream.CanWrite;
				}
			}

			// Token: 0x17000597 RID: 1431
			// (get) Token: 0x06001B45 RID: 6981 RVA: 0x00064433 File Offset: 0x00062633
			public override bool CanTimeout
			{
				get
				{
					return this.m_stream.CanTimeout;
				}
			}

			// Token: 0x17000598 RID: 1432
			// (get) Token: 0x06001B46 RID: 6982 RVA: 0x00064440 File Offset: 0x00062640
			// (set) Token: 0x06001B47 RID: 6983 RVA: 0x0006444D File Offset: 0x0006264D
			public override int ReadTimeout
			{
				get
				{
					return this.m_stream.ReadTimeout;
				}
				set
				{
					this.m_stream.ReadTimeout = value;
				}
			}

			// Token: 0x17000599 RID: 1433
			// (get) Token: 0x06001B48 RID: 6984 RVA: 0x0006445B File Offset: 0x0006265B
			// (set) Token: 0x06001B49 RID: 6985 RVA: 0x00064468 File Offset: 0x00062668
			public override int WriteTimeout
			{
				get
				{
					return this.m_stream.WriteTimeout;
				}
				set
				{
					this.m_stream.WriteTimeout = value;
				}
			}

			// Token: 0x1700059A RID: 1434
			// (get) Token: 0x06001B4A RID: 6986 RVA: 0x00064476 File Offset: 0x00062676
			public override long Length
			{
				get
				{
					return this.m_stream.Length;
				}
			}

			// Token: 0x1700059B RID: 1435
			// (get) Token: 0x06001B4B RID: 6987 RVA: 0x00064483 File Offset: 0x00062683
			// (set) Token: 0x06001B4C RID: 6988 RVA: 0x00064490 File Offset: 0x00062690
			public override long Position
			{
				get
				{
					return this.m_stream.Position;
				}
				set
				{
					this.m_stream.Position = value;
				}
			}

			// Token: 0x06001B4D RID: 6989 RVA: 0x0006449E File Offset: 0x0006269E
			[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
			public override IAsyncResult BeginRead(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
			{
				return this.m_stream.BeginRead(buffer, offset, size, callback, state);
			}

			// Token: 0x06001B4E RID: 6990 RVA: 0x000644B2 File Offset: 0x000626B2
			[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
			public override IAsyncResult BeginWrite(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
			{
				return this.m_stream.BeginWrite(buffer, offset, size, callback, state);
			}

			// Token: 0x06001B4F RID: 6991 RVA: 0x000644C8 File Offset: 0x000626C8
			protected override void Dispose(bool disposing)
			{
				try
				{
					if (disposing)
					{
						this.m_stream.Close();
						this.m_WebClient.GetWebResponse(this.m_request).Close();
					}
				}
				finally
				{
					base.Dispose(disposing);
				}
			}

			// Token: 0x06001B50 RID: 6992 RVA: 0x00064514 File Offset: 0x00062714
			public override int EndRead(IAsyncResult result)
			{
				return this.m_stream.EndRead(result);
			}

			// Token: 0x06001B51 RID: 6993 RVA: 0x00064522 File Offset: 0x00062722
			public override void EndWrite(IAsyncResult result)
			{
				this.m_stream.EndWrite(result);
			}

			// Token: 0x06001B52 RID: 6994 RVA: 0x00064530 File Offset: 0x00062730
			public override void Flush()
			{
				this.m_stream.Flush();
			}

			// Token: 0x06001B53 RID: 6995 RVA: 0x0006453D File Offset: 0x0006273D
			public override int Read(byte[] buffer, int offset, int count)
			{
				return this.m_stream.Read(buffer, offset, count);
			}

			// Token: 0x06001B54 RID: 6996 RVA: 0x0006454D File Offset: 0x0006274D
			public override long Seek(long offset, SeekOrigin origin)
			{
				return this.m_stream.Seek(offset, origin);
			}

			// Token: 0x06001B55 RID: 6997 RVA: 0x0006455C File Offset: 0x0006275C
			public override void SetLength(long value)
			{
				this.m_stream.SetLength(value);
			}

			// Token: 0x06001B56 RID: 6998 RVA: 0x0006456A File Offset: 0x0006276A
			public override void Write(byte[] buffer, int offset, int count)
			{
				this.m_stream.Write(buffer, offset, count);
			}

			// Token: 0x040018ED RID: 6381
			private WebRequest m_request;

			// Token: 0x040018EE RID: 6382
			private Stream m_stream;

			// Token: 0x040018EF RID: 6383
			private WebClient m_WebClient;
		}

		// Token: 0x02000394 RID: 916
		[CompilerGenerated]
		private sealed class <>c__DisplayClass219_0
		{
			// Token: 0x06001B57 RID: 6999 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass219_0()
			{
			}

			// Token: 0x06001B58 RID: 7000 RVA: 0x0006457C File Offset: 0x0006277C
			internal void <DownloadStringTaskAsync>b__0(object sender, DownloadStringCompletedEventArgs e)
			{
				this.<>4__this.HandleCompletion<DownloadStringCompletedEventArgs, DownloadStringCompletedEventHandler, string>(this.tcs, e, new Func<DownloadStringCompletedEventArgs, string>(WebClient.<>c.<>9.<DownloadStringTaskAsync>b__219_1), this.handler, new Action<WebClient, DownloadStringCompletedEventHandler>(WebClient.<>c.<>9.<DownloadStringTaskAsync>b__219_2));
			}

			// Token: 0x040018F0 RID: 6384
			public WebClient <>4__this;

			// Token: 0x040018F1 RID: 6385
			public TaskCompletionSource<string> tcs;

			// Token: 0x040018F2 RID: 6386
			public DownloadStringCompletedEventHandler handler;
		}

		// Token: 0x02000395 RID: 917
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06001B59 RID: 7001 RVA: 0x000645DF File Offset: 0x000627DF
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06001B5A RID: 7002 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c()
			{
			}

			// Token: 0x06001B5B RID: 7003 RVA: 0x000645EB File Offset: 0x000627EB
			internal string <DownloadStringTaskAsync>b__219_1(DownloadStringCompletedEventArgs args)
			{
				return args.Result;
			}

			// Token: 0x06001B5C RID: 7004 RVA: 0x000645F3 File Offset: 0x000627F3
			internal void <DownloadStringTaskAsync>b__219_2(WebClient webClient, DownloadStringCompletedEventHandler completion)
			{
				webClient.DownloadStringCompleted -= completion;
			}

			// Token: 0x06001B5D RID: 7005 RVA: 0x000645FC File Offset: 0x000627FC
			internal Stream <OpenReadTaskAsync>b__221_1(OpenReadCompletedEventArgs args)
			{
				return args.Result;
			}

			// Token: 0x06001B5E RID: 7006 RVA: 0x00064604 File Offset: 0x00062804
			internal void <OpenReadTaskAsync>b__221_2(WebClient webClient, OpenReadCompletedEventHandler completion)
			{
				webClient.OpenReadCompleted -= completion;
			}

			// Token: 0x06001B5F RID: 7007 RVA: 0x0006460D File Offset: 0x0006280D
			internal Stream <OpenWriteTaskAsync>b__225_1(OpenWriteCompletedEventArgs args)
			{
				return args.Result;
			}

			// Token: 0x06001B60 RID: 7008 RVA: 0x00064615 File Offset: 0x00062815
			internal void <OpenWriteTaskAsync>b__225_2(WebClient webClient, OpenWriteCompletedEventHandler completion)
			{
				webClient.OpenWriteCompleted -= completion;
			}

			// Token: 0x06001B61 RID: 7009 RVA: 0x0006461E File Offset: 0x0006281E
			internal string <UploadStringTaskAsync>b__229_1(UploadStringCompletedEventArgs args)
			{
				return args.Result;
			}

			// Token: 0x06001B62 RID: 7010 RVA: 0x00064626 File Offset: 0x00062826
			internal void <UploadStringTaskAsync>b__229_2(WebClient webClient, UploadStringCompletedEventHandler completion)
			{
				webClient.UploadStringCompleted -= completion;
			}

			// Token: 0x06001B63 RID: 7011 RVA: 0x0006462F File Offset: 0x0006282F
			internal byte[] <DownloadDataTaskAsync>b__231_1(DownloadDataCompletedEventArgs args)
			{
				return args.Result;
			}

			// Token: 0x06001B64 RID: 7012 RVA: 0x00064637 File Offset: 0x00062837
			internal void <DownloadDataTaskAsync>b__231_2(WebClient webClient, DownloadDataCompletedEventHandler completion)
			{
				webClient.DownloadDataCompleted -= completion;
			}

			// Token: 0x06001B65 RID: 7013 RVA: 0x00008B3F File Offset: 0x00006D3F
			internal object <DownloadFileTaskAsync>b__233_1(AsyncCompletedEventArgs args)
			{
				return null;
			}

			// Token: 0x06001B66 RID: 7014 RVA: 0x00064640 File Offset: 0x00062840
			internal void <DownloadFileTaskAsync>b__233_2(WebClient webClient, AsyncCompletedEventHandler completion)
			{
				webClient.DownloadFileCompleted -= completion;
			}

			// Token: 0x06001B67 RID: 7015 RVA: 0x00064649 File Offset: 0x00062849
			internal byte[] <UploadDataTaskAsync>b__237_1(UploadDataCompletedEventArgs args)
			{
				return args.Result;
			}

			// Token: 0x06001B68 RID: 7016 RVA: 0x00064651 File Offset: 0x00062851
			internal void <UploadDataTaskAsync>b__237_2(WebClient webClient, UploadDataCompletedEventHandler completion)
			{
				webClient.UploadDataCompleted -= completion;
			}

			// Token: 0x06001B69 RID: 7017 RVA: 0x0006465A File Offset: 0x0006285A
			internal byte[] <UploadFileTaskAsync>b__241_1(UploadFileCompletedEventArgs args)
			{
				return args.Result;
			}

			// Token: 0x06001B6A RID: 7018 RVA: 0x00064662 File Offset: 0x00062862
			internal void <UploadFileTaskAsync>b__241_2(WebClient webClient, UploadFileCompletedEventHandler completion)
			{
				webClient.UploadFileCompleted -= completion;
			}

			// Token: 0x06001B6B RID: 7019 RVA: 0x0006466B File Offset: 0x0006286B
			internal byte[] <UploadValuesTaskAsync>b__245_1(UploadValuesCompletedEventArgs args)
			{
				return args.Result;
			}

			// Token: 0x06001B6C RID: 7020 RVA: 0x00064673 File Offset: 0x00062873
			internal void <UploadValuesTaskAsync>b__245_2(WebClient webClient, UploadValuesCompletedEventHandler completion)
			{
				webClient.UploadValuesCompleted -= completion;
			}

			// Token: 0x040018F3 RID: 6387
			public static readonly WebClient.<>c <>9 = new WebClient.<>c();

			// Token: 0x040018F4 RID: 6388
			public static Func<DownloadStringCompletedEventArgs, string> <>9__219_1;

			// Token: 0x040018F5 RID: 6389
			public static Action<WebClient, DownloadStringCompletedEventHandler> <>9__219_2;

			// Token: 0x040018F6 RID: 6390
			public static Func<OpenReadCompletedEventArgs, Stream> <>9__221_1;

			// Token: 0x040018F7 RID: 6391
			public static Action<WebClient, OpenReadCompletedEventHandler> <>9__221_2;

			// Token: 0x040018F8 RID: 6392
			public static Func<OpenWriteCompletedEventArgs, Stream> <>9__225_1;

			// Token: 0x040018F9 RID: 6393
			public static Action<WebClient, OpenWriteCompletedEventHandler> <>9__225_2;

			// Token: 0x040018FA RID: 6394
			public static Func<UploadStringCompletedEventArgs, string> <>9__229_1;

			// Token: 0x040018FB RID: 6395
			public static Action<WebClient, UploadStringCompletedEventHandler> <>9__229_2;

			// Token: 0x040018FC RID: 6396
			public static Func<DownloadDataCompletedEventArgs, byte[]> <>9__231_1;

			// Token: 0x040018FD RID: 6397
			public static Action<WebClient, DownloadDataCompletedEventHandler> <>9__231_2;

			// Token: 0x040018FE RID: 6398
			public static Func<AsyncCompletedEventArgs, object> <>9__233_1;

			// Token: 0x040018FF RID: 6399
			public static Action<WebClient, AsyncCompletedEventHandler> <>9__233_2;

			// Token: 0x04001900 RID: 6400
			public static Func<UploadDataCompletedEventArgs, byte[]> <>9__237_1;

			// Token: 0x04001901 RID: 6401
			public static Action<WebClient, UploadDataCompletedEventHandler> <>9__237_2;

			// Token: 0x04001902 RID: 6402
			public static Func<UploadFileCompletedEventArgs, byte[]> <>9__241_1;

			// Token: 0x04001903 RID: 6403
			public static Action<WebClient, UploadFileCompletedEventHandler> <>9__241_2;

			// Token: 0x04001904 RID: 6404
			public static Func<UploadValuesCompletedEventArgs, byte[]> <>9__245_1;

			// Token: 0x04001905 RID: 6405
			public static Action<WebClient, UploadValuesCompletedEventHandler> <>9__245_2;
		}

		// Token: 0x02000396 RID: 918
		[CompilerGenerated]
		private sealed class <>c__DisplayClass221_0
		{
			// Token: 0x06001B6D RID: 7021 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass221_0()
			{
			}

			// Token: 0x06001B6E RID: 7022 RVA: 0x0006467C File Offset: 0x0006287C
			internal void <OpenReadTaskAsync>b__0(object sender, OpenReadCompletedEventArgs e)
			{
				this.<>4__this.HandleCompletion<OpenReadCompletedEventArgs, OpenReadCompletedEventHandler, Stream>(this.tcs, e, new Func<OpenReadCompletedEventArgs, Stream>(WebClient.<>c.<>9.<OpenReadTaskAsync>b__221_1), this.handler, new Action<WebClient, OpenReadCompletedEventHandler>(WebClient.<>c.<>9.<OpenReadTaskAsync>b__221_2));
			}

			// Token: 0x04001906 RID: 6406
			public WebClient <>4__this;

			// Token: 0x04001907 RID: 6407
			public TaskCompletionSource<Stream> tcs;

			// Token: 0x04001908 RID: 6408
			public OpenReadCompletedEventHandler handler;
		}

		// Token: 0x02000397 RID: 919
		[CompilerGenerated]
		private sealed class <>c__DisplayClass225_0
		{
			// Token: 0x06001B6F RID: 7023 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass225_0()
			{
			}

			// Token: 0x06001B70 RID: 7024 RVA: 0x000646E0 File Offset: 0x000628E0
			internal void <OpenWriteTaskAsync>b__0(object sender, OpenWriteCompletedEventArgs e)
			{
				this.<>4__this.HandleCompletion<OpenWriteCompletedEventArgs, OpenWriteCompletedEventHandler, Stream>(this.tcs, e, new Func<OpenWriteCompletedEventArgs, Stream>(WebClient.<>c.<>9.<OpenWriteTaskAsync>b__225_1), this.handler, new Action<WebClient, OpenWriteCompletedEventHandler>(WebClient.<>c.<>9.<OpenWriteTaskAsync>b__225_2));
			}

			// Token: 0x04001909 RID: 6409
			public WebClient <>4__this;

			// Token: 0x0400190A RID: 6410
			public TaskCompletionSource<Stream> tcs;

			// Token: 0x0400190B RID: 6411
			public OpenWriteCompletedEventHandler handler;
		}

		// Token: 0x02000398 RID: 920
		[CompilerGenerated]
		private sealed class <>c__DisplayClass229_0
		{
			// Token: 0x06001B71 RID: 7025 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass229_0()
			{
			}

			// Token: 0x06001B72 RID: 7026 RVA: 0x00064744 File Offset: 0x00062944
			internal void <UploadStringTaskAsync>b__0(object sender, UploadStringCompletedEventArgs e)
			{
				this.<>4__this.HandleCompletion<UploadStringCompletedEventArgs, UploadStringCompletedEventHandler, string>(this.tcs, e, new Func<UploadStringCompletedEventArgs, string>(WebClient.<>c.<>9.<UploadStringTaskAsync>b__229_1), this.handler, new Action<WebClient, UploadStringCompletedEventHandler>(WebClient.<>c.<>9.<UploadStringTaskAsync>b__229_2));
			}

			// Token: 0x0400190C RID: 6412
			public WebClient <>4__this;

			// Token: 0x0400190D RID: 6413
			public TaskCompletionSource<string> tcs;

			// Token: 0x0400190E RID: 6414
			public UploadStringCompletedEventHandler handler;
		}

		// Token: 0x02000399 RID: 921
		[CompilerGenerated]
		private sealed class <>c__DisplayClass231_0
		{
			// Token: 0x06001B73 RID: 7027 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass231_0()
			{
			}

			// Token: 0x06001B74 RID: 7028 RVA: 0x000647A8 File Offset: 0x000629A8
			internal void <DownloadDataTaskAsync>b__0(object sender, DownloadDataCompletedEventArgs e)
			{
				this.<>4__this.HandleCompletion<DownloadDataCompletedEventArgs, DownloadDataCompletedEventHandler, byte[]>(this.tcs, e, new Func<DownloadDataCompletedEventArgs, byte[]>(WebClient.<>c.<>9.<DownloadDataTaskAsync>b__231_1), this.handler, new Action<WebClient, DownloadDataCompletedEventHandler>(WebClient.<>c.<>9.<DownloadDataTaskAsync>b__231_2));
			}

			// Token: 0x0400190F RID: 6415
			public WebClient <>4__this;

			// Token: 0x04001910 RID: 6416
			public TaskCompletionSource<byte[]> tcs;

			// Token: 0x04001911 RID: 6417
			public DownloadDataCompletedEventHandler handler;
		}

		// Token: 0x0200039A RID: 922
		[CompilerGenerated]
		private sealed class <>c__DisplayClass233_0
		{
			// Token: 0x06001B75 RID: 7029 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass233_0()
			{
			}

			// Token: 0x06001B76 RID: 7030 RVA: 0x0006480C File Offset: 0x00062A0C
			internal void <DownloadFileTaskAsync>b__0(object sender, AsyncCompletedEventArgs e)
			{
				this.<>4__this.HandleCompletion<AsyncCompletedEventArgs, AsyncCompletedEventHandler, object>(this.tcs, e, new Func<AsyncCompletedEventArgs, object>(WebClient.<>c.<>9.<DownloadFileTaskAsync>b__233_1), this.handler, new Action<WebClient, AsyncCompletedEventHandler>(WebClient.<>c.<>9.<DownloadFileTaskAsync>b__233_2));
			}

			// Token: 0x04001912 RID: 6418
			public WebClient <>4__this;

			// Token: 0x04001913 RID: 6419
			public TaskCompletionSource<object> tcs;

			// Token: 0x04001914 RID: 6420
			public AsyncCompletedEventHandler handler;
		}

		// Token: 0x0200039B RID: 923
		[CompilerGenerated]
		private sealed class <>c__DisplayClass237_0
		{
			// Token: 0x06001B77 RID: 7031 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass237_0()
			{
			}

			// Token: 0x06001B78 RID: 7032 RVA: 0x00064870 File Offset: 0x00062A70
			internal void <UploadDataTaskAsync>b__0(object sender, UploadDataCompletedEventArgs e)
			{
				this.<>4__this.HandleCompletion<UploadDataCompletedEventArgs, UploadDataCompletedEventHandler, byte[]>(this.tcs, e, new Func<UploadDataCompletedEventArgs, byte[]>(WebClient.<>c.<>9.<UploadDataTaskAsync>b__237_1), this.handler, new Action<WebClient, UploadDataCompletedEventHandler>(WebClient.<>c.<>9.<UploadDataTaskAsync>b__237_2));
			}

			// Token: 0x04001915 RID: 6421
			public WebClient <>4__this;

			// Token: 0x04001916 RID: 6422
			public TaskCompletionSource<byte[]> tcs;

			// Token: 0x04001917 RID: 6423
			public UploadDataCompletedEventHandler handler;
		}

		// Token: 0x0200039C RID: 924
		[CompilerGenerated]
		private sealed class <>c__DisplayClass241_0
		{
			// Token: 0x06001B79 RID: 7033 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass241_0()
			{
			}

			// Token: 0x06001B7A RID: 7034 RVA: 0x000648D4 File Offset: 0x00062AD4
			internal void <UploadFileTaskAsync>b__0(object sender, UploadFileCompletedEventArgs e)
			{
				this.<>4__this.HandleCompletion<UploadFileCompletedEventArgs, UploadFileCompletedEventHandler, byte[]>(this.tcs, e, new Func<UploadFileCompletedEventArgs, byte[]>(WebClient.<>c.<>9.<UploadFileTaskAsync>b__241_1), this.handler, new Action<WebClient, UploadFileCompletedEventHandler>(WebClient.<>c.<>9.<UploadFileTaskAsync>b__241_2));
			}

			// Token: 0x04001918 RID: 6424
			public WebClient <>4__this;

			// Token: 0x04001919 RID: 6425
			public TaskCompletionSource<byte[]> tcs;

			// Token: 0x0400191A RID: 6426
			public UploadFileCompletedEventHandler handler;
		}

		// Token: 0x0200039D RID: 925
		[CompilerGenerated]
		private sealed class <>c__DisplayClass245_0
		{
			// Token: 0x06001B7B RID: 7035 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass245_0()
			{
			}

			// Token: 0x06001B7C RID: 7036 RVA: 0x00064938 File Offset: 0x00062B38
			internal void <UploadValuesTaskAsync>b__0(object sender, UploadValuesCompletedEventArgs e)
			{
				this.<>4__this.HandleCompletion<UploadValuesCompletedEventArgs, UploadValuesCompletedEventHandler, byte[]>(this.tcs, e, new Func<UploadValuesCompletedEventArgs, byte[]>(WebClient.<>c.<>9.<UploadValuesTaskAsync>b__245_1), this.handler, new Action<WebClient, UploadValuesCompletedEventHandler>(WebClient.<>c.<>9.<UploadValuesTaskAsync>b__245_2));
			}

			// Token: 0x0400191B RID: 6427
			public WebClient <>4__this;

			// Token: 0x0400191C RID: 6428
			public TaskCompletionSource<byte[]> tcs;

			// Token: 0x0400191D RID: 6429
			public UploadValuesCompletedEventHandler handler;
		}
	}
}
