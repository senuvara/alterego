﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;

namespace System.Net
{
	// Token: 0x0200038D RID: 909
	internal class TrackingValidationObjectDictionary : StringDictionary
	{
		// Token: 0x06001A5F RID: 6751 RVA: 0x00060238 File Offset: 0x0005E438
		internal TrackingValidationObjectDictionary(IDictionary<string, TrackingValidationObjectDictionary.ValidateAndParseValue> validators)
		{
			this.IsChanged = false;
			this.validators = validators;
		}

		// Token: 0x06001A60 RID: 6752 RVA: 0x00060250 File Offset: 0x0005E450
		private void PersistValue(string key, string value, bool addValue)
		{
			key = key.ToLowerInvariant();
			if (!string.IsNullOrEmpty(value))
			{
				if (this.validators != null && this.validators.ContainsKey(key))
				{
					object obj = this.validators[key](value);
					if (this.internalObjects == null)
					{
						this.internalObjects = new Dictionary<string, object>();
					}
					if (addValue)
					{
						this.internalObjects.Add(key, obj);
						base.Add(key, obj.ToString());
					}
					else
					{
						this.internalObjects[key] = obj;
						base[key] = obj.ToString();
					}
				}
				else if (addValue)
				{
					base.Add(key, value);
				}
				else
				{
					base[key] = value;
				}
				this.IsChanged = true;
			}
		}

		// Token: 0x17000583 RID: 1411
		// (get) Token: 0x06001A61 RID: 6753 RVA: 0x00060303 File Offset: 0x0005E503
		// (set) Token: 0x06001A62 RID: 6754 RVA: 0x0006030B File Offset: 0x0005E50B
		internal bool IsChanged
		{
			[CompilerGenerated]
			get
			{
				return this.<IsChanged>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IsChanged>k__BackingField = value;
			}
		}

		// Token: 0x06001A63 RID: 6755 RVA: 0x00060314 File Offset: 0x0005E514
		internal object InternalGet(string key)
		{
			if (this.internalObjects != null && this.internalObjects.ContainsKey(key))
			{
				return this.internalObjects[key];
			}
			return base[key];
		}

		// Token: 0x06001A64 RID: 6756 RVA: 0x00060340 File Offset: 0x0005E540
		internal void InternalSet(string key, object value)
		{
			if (this.internalObjects == null)
			{
				this.internalObjects = new Dictionary<string, object>();
			}
			this.internalObjects[key] = value;
			base[key] = value.ToString();
			this.IsChanged = true;
		}

		// Token: 0x17000584 RID: 1412
		public override string this[string key]
		{
			get
			{
				return base[key];
			}
			set
			{
				this.PersistValue(key, value, false);
			}
		}

		// Token: 0x06001A67 RID: 6759 RVA: 0x00060381 File Offset: 0x0005E581
		public override void Add(string key, string value)
		{
			this.PersistValue(key, value, true);
		}

		// Token: 0x06001A68 RID: 6760 RVA: 0x0006038C File Offset: 0x0005E58C
		public override void Clear()
		{
			if (this.internalObjects != null)
			{
				this.internalObjects.Clear();
			}
			base.Clear();
			this.IsChanged = true;
		}

		// Token: 0x06001A69 RID: 6761 RVA: 0x000603AE File Offset: 0x0005E5AE
		public override void Remove(string key)
		{
			if (this.internalObjects != null && this.internalObjects.ContainsKey(key))
			{
				this.internalObjects.Remove(key);
			}
			base.Remove(key);
			this.IsChanged = true;
		}

		// Token: 0x0400189E RID: 6302
		private IDictionary<string, object> internalObjects;

		// Token: 0x0400189F RID: 6303
		private readonly IDictionary<string, TrackingValidationObjectDictionary.ValidateAndParseValue> validators;

		// Token: 0x040018A0 RID: 6304
		[CompilerGenerated]
		private bool <IsChanged>k__BackingField;

		// Token: 0x0200038E RID: 910
		// (Invoke) Token: 0x06001A6B RID: 6763
		internal delegate object ValidateAndParseValue(object valueToValidate);
	}
}
