﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Net
{
	/// <summary>Provides data for the <see cref="E:System.Net.WebClient.UploadFileCompleted" /> event.</summary>
	// Token: 0x020003AC RID: 940
	public class UploadFileCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06001BAF RID: 7087 RVA: 0x00064A61 File Offset: 0x00062C61
		internal UploadFileCompletedEventArgs(byte[] result, Exception exception, bool cancelled, object userToken) : base(exception, cancelled, userToken)
		{
			this.m_Result = result;
		}

		/// <summary>Gets the server reply to a data upload operation that is started by calling an <see cref="Overload:System.Net.WebClient.UploadFileAsync" /> method.</summary>
		/// <returns>A <see cref="T:System.Byte" /> array that contains the server reply.</returns>
		// Token: 0x170005A2 RID: 1442
		// (get) Token: 0x06001BB0 RID: 7088 RVA: 0x00064A74 File Offset: 0x00062C74
		public byte[] Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return this.m_Result;
			}
		}

		// Token: 0x06001BB1 RID: 7089 RVA: 0x000092E2 File Offset: 0x000074E2
		internal UploadFileCompletedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001924 RID: 6436
		private byte[] m_Result;
	}
}
