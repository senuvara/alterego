﻿using System;

namespace System.Net
{
	// Token: 0x02000402 RID: 1026
	// (Invoke) Token: 0x06001FE0 RID: 8160
	internal delegate void SimpleAsyncCallback(SimpleAsyncResult result);
}
