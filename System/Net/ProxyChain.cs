﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Net
{
	// Token: 0x02000357 RID: 855
	internal abstract class ProxyChain : IEnumerable<Uri>, IEnumerable, IDisposable
	{
		// Token: 0x060018B0 RID: 6320 RVA: 0x00058C2E File Offset: 0x00056E2E
		protected ProxyChain(Uri destination)
		{
			this.m_Destination = destination;
		}

		// Token: 0x060018B1 RID: 6321 RVA: 0x00058C48 File Offset: 0x00056E48
		public IEnumerator<Uri> GetEnumerator()
		{
			ProxyChain.ProxyEnumerator proxyEnumerator = new ProxyChain.ProxyEnumerator(this);
			if (this.m_MainEnumerator == null)
			{
				this.m_MainEnumerator = proxyEnumerator;
			}
			return proxyEnumerator;
		}

		// Token: 0x060018B2 RID: 6322 RVA: 0x00058C6C File Offset: 0x00056E6C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x060018B3 RID: 6323 RVA: 0x0000232D File Offset: 0x0000052D
		public virtual void Dispose()
		{
		}

		// Token: 0x17000519 RID: 1305
		// (get) Token: 0x060018B4 RID: 6324 RVA: 0x00058C74 File Offset: 0x00056E74
		internal IEnumerator<Uri> Enumerator
		{
			get
			{
				if (this.m_MainEnumerator != null)
				{
					return this.m_MainEnumerator;
				}
				return this.GetEnumerator();
			}
		}

		// Token: 0x1700051A RID: 1306
		// (get) Token: 0x060018B5 RID: 6325 RVA: 0x00058C98 File Offset: 0x00056E98
		internal Uri Destination
		{
			get
			{
				return this.m_Destination;
			}
		}

		// Token: 0x060018B6 RID: 6326 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void Abort()
		{
		}

		// Token: 0x060018B7 RID: 6327 RVA: 0x00058CA0 File Offset: 0x00056EA0
		internal bool HttpAbort(HttpWebRequest request, WebException webException)
		{
			this.Abort();
			return true;
		}

		// Token: 0x1700051B RID: 1307
		// (get) Token: 0x060018B8 RID: 6328 RVA: 0x00058CA9 File Offset: 0x00056EA9
		internal HttpAbortDelegate HttpAbortDelegate
		{
			get
			{
				if (this.m_HttpAbortDelegate == null)
				{
					this.m_HttpAbortDelegate = new HttpAbortDelegate(this.HttpAbort);
				}
				return this.m_HttpAbortDelegate;
			}
		}

		// Token: 0x060018B9 RID: 6329
		protected abstract bool GetNextProxy(out Uri proxy);

		// Token: 0x04001791 RID: 6033
		private List<Uri> m_Cache = new List<Uri>();

		// Token: 0x04001792 RID: 6034
		private bool m_CacheComplete;

		// Token: 0x04001793 RID: 6035
		private ProxyChain.ProxyEnumerator m_MainEnumerator;

		// Token: 0x04001794 RID: 6036
		private Uri m_Destination;

		// Token: 0x04001795 RID: 6037
		private HttpAbortDelegate m_HttpAbortDelegate;

		// Token: 0x02000358 RID: 856
		private class ProxyEnumerator : IEnumerator<Uri>, IDisposable, IEnumerator
		{
			// Token: 0x060018BA RID: 6330 RVA: 0x00058CCB File Offset: 0x00056ECB
			internal ProxyEnumerator(ProxyChain chain)
			{
				this.m_Chain = chain;
			}

			// Token: 0x1700051C RID: 1308
			// (get) Token: 0x060018BB RID: 6331 RVA: 0x00058CE1 File Offset: 0x00056EE1
			public Uri Current
			{
				get
				{
					if (this.m_Finished || this.m_CurrentIndex < 0)
					{
						throw new InvalidOperationException(SR.GetString("Enumeration has either not started or has already finished."));
					}
					return this.m_Chain.m_Cache[this.m_CurrentIndex];
				}
			}

			// Token: 0x1700051D RID: 1309
			// (get) Token: 0x060018BC RID: 6332 RVA: 0x00058D1A File Offset: 0x00056F1A
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x060018BD RID: 6333 RVA: 0x00058D24 File Offset: 0x00056F24
			public bool MoveNext()
			{
				if (this.m_Finished)
				{
					return false;
				}
				checked
				{
					this.m_CurrentIndex++;
					if (this.m_Chain.m_Cache.Count > this.m_CurrentIndex)
					{
						return true;
					}
					if (this.m_Chain.m_CacheComplete)
					{
						this.m_Finished = true;
						return false;
					}
					List<Uri> cache = this.m_Chain.m_Cache;
					bool result;
					lock (cache)
					{
						if (this.m_Chain.m_Cache.Count > this.m_CurrentIndex)
						{
							result = true;
						}
						else if (this.m_Chain.m_CacheComplete)
						{
							this.m_Finished = true;
							result = false;
						}
						else
						{
							Uri uri;
							while (this.m_Chain.GetNextProxy(out uri))
							{
								if (uri == null)
								{
									if (this.m_TriedDirect)
									{
										continue;
									}
									this.m_TriedDirect = true;
								}
								this.m_Chain.m_Cache.Add(uri);
								return true;
							}
							this.m_Finished = true;
							this.m_Chain.m_CacheComplete = true;
							result = false;
						}
					}
					return result;
				}
			}

			// Token: 0x060018BE RID: 6334 RVA: 0x00058E34 File Offset: 0x00057034
			public void Reset()
			{
				this.m_Finished = false;
				this.m_CurrentIndex = -1;
			}

			// Token: 0x060018BF RID: 6335 RVA: 0x0000232D File Offset: 0x0000052D
			public void Dispose()
			{
			}

			// Token: 0x04001796 RID: 6038
			private ProxyChain m_Chain;

			// Token: 0x04001797 RID: 6039
			private bool m_Finished;

			// Token: 0x04001798 RID: 6040
			private int m_CurrentIndex = -1;

			// Token: 0x04001799 RID: 6041
			private bool m_TriedDirect;
		}
	}
}
