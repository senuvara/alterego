﻿using System;

namespace System.Net
{
	// Token: 0x020003F6 RID: 1014
	internal sealed class ListenerPrefix
	{
		// Token: 0x06001F32 RID: 7986 RVA: 0x0007132F File Offset: 0x0006F52F
		public ListenerPrefix(string prefix)
		{
			this.original = prefix;
			this.Parse(prefix);
		}

		// Token: 0x06001F33 RID: 7987 RVA: 0x00071345 File Offset: 0x0006F545
		public override string ToString()
		{
			return this.original;
		}

		// Token: 0x170006AD RID: 1709
		// (get) Token: 0x06001F34 RID: 7988 RVA: 0x0007134D File Offset: 0x0006F54D
		// (set) Token: 0x06001F35 RID: 7989 RVA: 0x00071355 File Offset: 0x0006F555
		public IPAddress[] Addresses
		{
			get
			{
				return this.addresses;
			}
			set
			{
				this.addresses = value;
			}
		}

		// Token: 0x170006AE RID: 1710
		// (get) Token: 0x06001F36 RID: 7990 RVA: 0x0007135E File Offset: 0x0006F55E
		public bool Secure
		{
			get
			{
				return this.secure;
			}
		}

		// Token: 0x170006AF RID: 1711
		// (get) Token: 0x06001F37 RID: 7991 RVA: 0x00071366 File Offset: 0x0006F566
		public string Host
		{
			get
			{
				return this.host;
			}
		}

		// Token: 0x170006B0 RID: 1712
		// (get) Token: 0x06001F38 RID: 7992 RVA: 0x0007136E File Offset: 0x0006F56E
		public int Port
		{
			get
			{
				return (int)this.port;
			}
		}

		// Token: 0x170006B1 RID: 1713
		// (get) Token: 0x06001F39 RID: 7993 RVA: 0x00071376 File Offset: 0x0006F576
		public string Path
		{
			get
			{
				return this.path;
			}
		}

		// Token: 0x06001F3A RID: 7994 RVA: 0x00071380 File Offset: 0x0006F580
		public override bool Equals(object o)
		{
			ListenerPrefix listenerPrefix = o as ListenerPrefix;
			return listenerPrefix != null && this.original == listenerPrefix.original;
		}

		// Token: 0x06001F3B RID: 7995 RVA: 0x000713AA File Offset: 0x0006F5AA
		public override int GetHashCode()
		{
			return this.original.GetHashCode();
		}

		// Token: 0x06001F3C RID: 7996 RVA: 0x000713B8 File Offset: 0x0006F5B8
		private void Parse(string uri)
		{
			ushort num = 80;
			if (uri.StartsWith("https://"))
			{
				num = 443;
				this.secure = true;
			}
			int length = uri.Length;
			int num2 = uri.IndexOf(':') + 3;
			if (num2 >= length)
			{
				throw new ArgumentException("No host specified.");
			}
			int num3 = uri.IndexOf(':', num2, length - num2);
			if (uri[num2] == '[')
			{
				num3 = uri.IndexOf("]:") + 1;
			}
			if (num2 == num3)
			{
				throw new ArgumentException("No host specified.");
			}
			int num4 = uri.IndexOf('/', num2, length - num2);
			if (num4 == -1)
			{
				throw new ArgumentException("No path specified.");
			}
			if (num3 > 0)
			{
				this.host = uri.Substring(num2, num3 - num2).Trim(new char[]
				{
					'[',
					']'
				});
				this.port = ushort.Parse(uri.Substring(num3 + 1, num4 - num3 - 1));
			}
			else
			{
				this.host = uri.Substring(num2, num4 - num2).Trim(new char[]
				{
					'[',
					']'
				});
				this.port = num;
			}
			this.path = uri.Substring(num4);
			if (this.path.Length != 1)
			{
				this.path = this.path.Substring(0, this.path.Length - 1);
			}
		}

		// Token: 0x06001F3D RID: 7997 RVA: 0x00071504 File Offset: 0x0006F704
		public static void CheckUri(string uri)
		{
			if (uri == null)
			{
				throw new ArgumentNullException("uriPrefix");
			}
			if (!uri.StartsWith("http://") && !uri.StartsWith("https://"))
			{
				throw new ArgumentException("Only 'http' and 'https' schemes are supported.");
			}
			int length = uri.Length;
			int num = uri.IndexOf(':') + 3;
			if (num >= length)
			{
				throw new ArgumentException("No host specified.");
			}
			int num2 = uri.IndexOf(':', num, length - num);
			if (uri[num] == '[')
			{
				num2 = uri.IndexOf("]:") + 1;
			}
			if (num == num2)
			{
				throw new ArgumentException("No host specified.");
			}
			int num3 = uri.IndexOf('/', num, length - num);
			if (num3 == -1)
			{
				throw new ArgumentException("No path specified.");
			}
			if (num2 > 0)
			{
				try
				{
					int num4 = int.Parse(uri.Substring(num2 + 1, num3 - num2 - 1));
					if (num4 <= 0 || num4 >= 65536)
					{
						throw new Exception();
					}
				}
				catch
				{
					throw new ArgumentException("Invalid port.");
				}
			}
			if (uri[uri.Length - 1] != '/')
			{
				throw new ArgumentException("The prefix must end with '/'");
			}
		}

		// Token: 0x04001ABF RID: 6847
		private string original;

		// Token: 0x04001AC0 RID: 6848
		private string host;

		// Token: 0x04001AC1 RID: 6849
		private ushort port;

		// Token: 0x04001AC2 RID: 6850
		private string path;

		// Token: 0x04001AC3 RID: 6851
		private bool secure;

		// Token: 0x04001AC4 RID: 6852
		private IPAddress[] addresses;

		// Token: 0x04001AC5 RID: 6853
		public HttpListener Listener;
	}
}
