﻿using System;

namespace System.Net
{
	// Token: 0x0200034D RID: 845
	[Flags]
	internal enum CloseExState
	{
		// Token: 0x04001762 RID: 5986
		Normal = 0,
		// Token: 0x04001763 RID: 5987
		Abort = 1,
		// Token: 0x04001764 RID: 5988
		Silent = 2
	}
}
