﻿using System;

namespace System.Net
{
	/// <summary>Defines transport types for the <see cref="T:System.Net.SocketPermission" /> and <see cref="T:System.Net.Sockets.Socket" /> classes.</summary>
	// Token: 0x02000325 RID: 805
	public enum TransportType
	{
		/// <summary>UDP transport.</summary>
		// Token: 0x04001672 RID: 5746
		Udp = 1,
		/// <summary>The transport type is connectionless, such as UDP. Specifying this value has the same effect as specifying <see cref="F:System.Net.TransportType.Udp" />.</summary>
		// Token: 0x04001673 RID: 5747
		Connectionless = 1,
		/// <summary>TCP transport.</summary>
		// Token: 0x04001674 RID: 5748
		Tcp,
		/// <summary>The transport is connection oriented, such as TCP. Specifying this value has the same effect as specifying <see cref="F:System.Net.TransportType.Tcp" />.</summary>
		// Token: 0x04001675 RID: 5749
		ConnectionOriented = 2,
		/// <summary>All transport types.</summary>
		// Token: 0x04001676 RID: 5750
		All
	}
}
