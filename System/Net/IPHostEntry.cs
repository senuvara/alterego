﻿using System;

namespace System.Net
{
	/// <summary>Provides a container class for Internet host address information.</summary>
	// Token: 0x020002F6 RID: 758
	public class IPHostEntry
	{
		/// <summary>Gets or sets the DNS name of the host.</summary>
		/// <returns>A string that contains the primary host name for the server.</returns>
		// Token: 0x17000498 RID: 1176
		// (get) Token: 0x06001674 RID: 5748 RVA: 0x00051003 File Offset: 0x0004F203
		// (set) Token: 0x06001675 RID: 5749 RVA: 0x0005100B File Offset: 0x0004F20B
		public string HostName
		{
			get
			{
				return this.hostName;
			}
			set
			{
				this.hostName = value;
			}
		}

		/// <summary>Gets or sets a list of aliases that are associated with a host.</summary>
		/// <returns>An array of strings that contain DNS names that resolve to the IP addresses in the <see cref="P:System.Net.IPHostEntry.AddressList" /> property.</returns>
		// Token: 0x17000499 RID: 1177
		// (get) Token: 0x06001676 RID: 5750 RVA: 0x00051014 File Offset: 0x0004F214
		// (set) Token: 0x06001677 RID: 5751 RVA: 0x0005101C File Offset: 0x0004F21C
		public string[] Aliases
		{
			get
			{
				return this.aliases;
			}
			set
			{
				this.aliases = value;
			}
		}

		/// <summary>Gets or sets a list of IP addresses that are associated with a host.</summary>
		/// <returns>An array of type <see cref="T:System.Net.IPAddress" /> that contains IP addresses that resolve to the host names that are contained in the <see cref="P:System.Net.IPHostEntry.Aliases" /> property.</returns>
		// Token: 0x1700049A RID: 1178
		// (get) Token: 0x06001678 RID: 5752 RVA: 0x00051025 File Offset: 0x0004F225
		// (set) Token: 0x06001679 RID: 5753 RVA: 0x0005102D File Offset: 0x0004F22D
		public IPAddress[] AddressList
		{
			get
			{
				return this.addressList;
			}
			set
			{
				this.addressList = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.IPHostEntry" /> class.</summary>
		// Token: 0x0600167A RID: 5754 RVA: 0x00051036 File Offset: 0x0004F236
		public IPHostEntry()
		{
		}

		// Token: 0x04001524 RID: 5412
		private string hostName;

		// Token: 0x04001525 RID: 5413
		private string[] aliases;

		// Token: 0x04001526 RID: 5414
		private IPAddress[] addressList;

		// Token: 0x04001527 RID: 5415
		internal bool isTrustedHost = true;
	}
}
