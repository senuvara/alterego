﻿using System;
using System.Runtime.Serialization;

namespace System.Net
{
	// Token: 0x020002FA RID: 762
	internal class InternalException : SystemException
	{
		// Token: 0x06001682 RID: 5762 RVA: 0x00051064 File Offset: 0x0004F264
		internal InternalException()
		{
		}

		// Token: 0x06001683 RID: 5763 RVA: 0x000447CD File Offset: 0x000429CD
		internal InternalException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
		{
		}
	}
}
