﻿using System;
using System.Security.Principal;

namespace System.Net
{
	/// <summary>Holds the user name and password from a basic authentication request.</summary>
	// Token: 0x020003E2 RID: 994
	public class HttpListenerBasicIdentity : GenericIdentity
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.HttpListenerBasicIdentity" /> class using the specified user name and password.</summary>
		/// <param name="username">The user name.</param>
		/// <param name="password">The password.</param>
		// Token: 0x06001DC1 RID: 7617 RVA: 0x0006BCD5 File Offset: 0x00069ED5
		public HttpListenerBasicIdentity(string username, string password) : base(username, "Basic")
		{
			this.password = password;
		}

		/// <summary>Indicates the password from a basic authentication attempt.</summary>
		/// <returns>A <see cref="T:System.String" /> that holds the password.</returns>
		// Token: 0x17000619 RID: 1561
		// (get) Token: 0x06001DC2 RID: 7618 RVA: 0x0006BCEA File Offset: 0x00069EEA
		public virtual string Password
		{
			get
			{
				return this.password;
			}
		}

		// Token: 0x04001A21 RID: 6689
		private string password;
	}
}
