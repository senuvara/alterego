﻿using System;
using System.Threading;

namespace System.Net
{
	// Token: 0x020003EB RID: 1003
	internal class HttpStreamAsyncResult : IAsyncResult
	{
		// Token: 0x06001E4E RID: 7758 RVA: 0x0006D94A File Offset: 0x0006BB4A
		public void Complete(Exception e)
		{
			this.Error = e;
			this.Complete();
		}

		// Token: 0x06001E4F RID: 7759 RVA: 0x0006D95C File Offset: 0x0006BB5C
		public void Complete()
		{
			object obj = this.locker;
			lock (obj)
			{
				if (!this.completed)
				{
					this.completed = true;
					if (this.handle != null)
					{
						this.handle.Set();
					}
					if (this.Callback != null)
					{
						this.Callback.BeginInvoke(this, null, null);
					}
				}
			}
		}

		// Token: 0x17000654 RID: 1620
		// (get) Token: 0x06001E50 RID: 7760 RVA: 0x0006D9D4 File Offset: 0x0006BBD4
		public object AsyncState
		{
			get
			{
				return this.State;
			}
		}

		// Token: 0x17000655 RID: 1621
		// (get) Token: 0x06001E51 RID: 7761 RVA: 0x0006D9DC File Offset: 0x0006BBDC
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				object obj = this.locker;
				lock (obj)
				{
					if (this.handle == null)
					{
						this.handle = new ManualResetEvent(this.completed);
					}
				}
				return this.handle;
			}
		}

		// Token: 0x17000656 RID: 1622
		// (get) Token: 0x06001E52 RID: 7762 RVA: 0x0006DA38 File Offset: 0x0006BC38
		public bool CompletedSynchronously
		{
			get
			{
				return this.SynchRead == this.Count;
			}
		}

		// Token: 0x17000657 RID: 1623
		// (get) Token: 0x06001E53 RID: 7763 RVA: 0x0006DA48 File Offset: 0x0006BC48
		public bool IsCompleted
		{
			get
			{
				object obj = this.locker;
				bool result;
				lock (obj)
				{
					result = this.completed;
				}
				return result;
			}
		}

		// Token: 0x06001E54 RID: 7764 RVA: 0x0006DA8C File Offset: 0x0006BC8C
		public HttpStreamAsyncResult()
		{
		}

		// Token: 0x04001A53 RID: 6739
		private object locker = new object();

		// Token: 0x04001A54 RID: 6740
		private ManualResetEvent handle;

		// Token: 0x04001A55 RID: 6741
		private bool completed;

		// Token: 0x04001A56 RID: 6742
		internal byte[] Buffer;

		// Token: 0x04001A57 RID: 6743
		internal int Offset;

		// Token: 0x04001A58 RID: 6744
		internal int Count;

		// Token: 0x04001A59 RID: 6745
		internal AsyncCallback Callback;

		// Token: 0x04001A5A RID: 6746
		internal object State;

		// Token: 0x04001A5B RID: 6747
		internal int SynchRead;

		// Token: 0x04001A5C RID: 6748
		internal Exception Error;
	}
}
