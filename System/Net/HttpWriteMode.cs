﻿using System;

namespace System.Net
{
	// Token: 0x02000314 RID: 788
	internal enum HttpWriteMode
	{
		// Token: 0x04001630 RID: 5680
		Unknown,
		// Token: 0x04001631 RID: 5681
		ContentLength,
		// Token: 0x04001632 RID: 5682
		Chunked,
		// Token: 0x04001633 RID: 5683
		Buffer,
		// Token: 0x04001634 RID: 5684
		None
	}
}
