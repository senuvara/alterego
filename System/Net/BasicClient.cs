﻿using System;

namespace System.Net
{
	// Token: 0x020003C2 RID: 962
	internal class BasicClient : IAuthenticationModule
	{
		// Token: 0x06001C39 RID: 7225 RVA: 0x00065CFE File Offset: 0x00063EFE
		public Authorization Authenticate(string challenge, WebRequest webRequest, ICredentials credentials)
		{
			if (credentials == null || challenge == null)
			{
				return null;
			}
			if (challenge.Trim().ToLower().IndexOf("basic", StringComparison.Ordinal) == -1)
			{
				return null;
			}
			return BasicClient.InternalAuthenticate(webRequest, credentials);
		}

		// Token: 0x06001C3A RID: 7226 RVA: 0x00065D2C File Offset: 0x00063F2C
		private static byte[] GetBytes(string str)
		{
			int i = str.Length;
			byte[] array = new byte[i];
			for (i--; i >= 0; i--)
			{
				array[i] = (byte)str[i];
			}
			return array;
		}

		// Token: 0x06001C3B RID: 7227 RVA: 0x00065D64 File Offset: 0x00063F64
		private static Authorization InternalAuthenticate(WebRequest webRequest, ICredentials credentials)
		{
			HttpWebRequest httpWebRequest = webRequest as HttpWebRequest;
			if (httpWebRequest == null || credentials == null)
			{
				return null;
			}
			NetworkCredential credential = credentials.GetCredential(httpWebRequest.AuthUri, "basic");
			if (credential == null)
			{
				return null;
			}
			string userName = credential.UserName;
			if (userName == null || userName == "")
			{
				return null;
			}
			string password = credential.Password;
			string domain = credential.Domain;
			byte[] bytes;
			if (domain == null || domain == "" || domain.Trim() == "")
			{
				bytes = BasicClient.GetBytes(userName + ":" + password);
			}
			else
			{
				bytes = BasicClient.GetBytes(string.Concat(new string[]
				{
					domain,
					"\\",
					userName,
					":",
					password
				}));
			}
			return new Authorization("Basic " + Convert.ToBase64String(bytes));
		}

		// Token: 0x06001C3C RID: 7228 RVA: 0x00065E3E File Offset: 0x0006403E
		public Authorization PreAuthenticate(WebRequest webRequest, ICredentials credentials)
		{
			return BasicClient.InternalAuthenticate(webRequest, credentials);
		}

		// Token: 0x170005BD RID: 1469
		// (get) Token: 0x06001C3D RID: 7229 RVA: 0x00065E47 File Offset: 0x00064047
		public string AuthenticationType
		{
			get
			{
				return "Basic";
			}
		}

		// Token: 0x170005BE RID: 1470
		// (get) Token: 0x06001C3E RID: 7230 RVA: 0x00003298 File Offset: 0x00001498
		public bool CanPreAuthenticate
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06001C3F RID: 7231 RVA: 0x0000232F File Offset: 0x0000052F
		public BasicClient()
		{
		}
	}
}
