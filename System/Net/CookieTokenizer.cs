﻿using System;

namespace System.Net
{
	// Token: 0x02000375 RID: 885
	internal class CookieTokenizer
	{
		// Token: 0x06001974 RID: 6516 RVA: 0x0005BE44 File Offset: 0x0005A044
		internal CookieTokenizer(string tokenStream)
		{
			this.m_length = tokenStream.Length;
			this.m_tokenStream = tokenStream;
		}

		// Token: 0x1700054B RID: 1355
		// (get) Token: 0x06001975 RID: 6517 RVA: 0x0005BE5F File Offset: 0x0005A05F
		// (set) Token: 0x06001976 RID: 6518 RVA: 0x0005BE67 File Offset: 0x0005A067
		internal bool EndOfCookie
		{
			get
			{
				return this.m_eofCookie;
			}
			set
			{
				this.m_eofCookie = value;
			}
		}

		// Token: 0x1700054C RID: 1356
		// (get) Token: 0x06001977 RID: 6519 RVA: 0x0005BE70 File Offset: 0x0005A070
		internal bool Eof
		{
			get
			{
				return this.m_index >= this.m_length;
			}
		}

		// Token: 0x1700054D RID: 1357
		// (get) Token: 0x06001978 RID: 6520 RVA: 0x0005BE83 File Offset: 0x0005A083
		// (set) Token: 0x06001979 RID: 6521 RVA: 0x0005BE8B File Offset: 0x0005A08B
		internal string Name
		{
			get
			{
				return this.m_name;
			}
			set
			{
				this.m_name = value;
			}
		}

		// Token: 0x1700054E RID: 1358
		// (get) Token: 0x0600197A RID: 6522 RVA: 0x0005BE94 File Offset: 0x0005A094
		// (set) Token: 0x0600197B RID: 6523 RVA: 0x0005BE9C File Offset: 0x0005A09C
		internal bool Quoted
		{
			get
			{
				return this.m_quoted;
			}
			set
			{
				this.m_quoted = value;
			}
		}

		// Token: 0x1700054F RID: 1359
		// (get) Token: 0x0600197C RID: 6524 RVA: 0x0005BEA5 File Offset: 0x0005A0A5
		// (set) Token: 0x0600197D RID: 6525 RVA: 0x0005BEAD File Offset: 0x0005A0AD
		internal CookieToken Token
		{
			get
			{
				return this.m_token;
			}
			set
			{
				this.m_token = value;
			}
		}

		// Token: 0x17000550 RID: 1360
		// (get) Token: 0x0600197E RID: 6526 RVA: 0x0005BEB6 File Offset: 0x0005A0B6
		// (set) Token: 0x0600197F RID: 6527 RVA: 0x0005BEBE File Offset: 0x0005A0BE
		internal string Value
		{
			get
			{
				return this.m_value;
			}
			set
			{
				this.m_value = value;
			}
		}

		// Token: 0x06001980 RID: 6528 RVA: 0x0005BEC8 File Offset: 0x0005A0C8
		internal string Extract()
		{
			string text = string.Empty;
			if (this.m_tokenLength != 0)
			{
				text = this.m_tokenStream.Substring(this.m_start, this.m_tokenLength);
				if (!this.Quoted)
				{
					text = text.Trim();
				}
			}
			return text;
		}

		// Token: 0x06001981 RID: 6529 RVA: 0x0005BF0C File Offset: 0x0005A10C
		internal CookieToken FindNext(bool ignoreComma, bool ignoreEquals)
		{
			this.m_tokenLength = 0;
			this.m_start = this.m_index;
			while (this.m_index < this.m_length && char.IsWhiteSpace(this.m_tokenStream[this.m_index]))
			{
				this.m_index++;
				this.m_start++;
			}
			CookieToken result = CookieToken.End;
			int num = 1;
			if (!this.Eof)
			{
				if (this.m_tokenStream[this.m_index] == '"')
				{
					this.Quoted = true;
					this.m_index++;
					bool flag = false;
					while (this.m_index < this.m_length)
					{
						char c = this.m_tokenStream[this.m_index];
						if (!flag && c == '"')
						{
							break;
						}
						if (flag)
						{
							flag = false;
						}
						else if (c == '\\')
						{
							flag = true;
						}
						this.m_index++;
					}
					if (this.m_index < this.m_length)
					{
						this.m_index++;
					}
					this.m_tokenLength = this.m_index - this.m_start;
					num = 0;
					ignoreComma = false;
				}
				while (this.m_index < this.m_length && this.m_tokenStream[this.m_index] != ';' && (ignoreEquals || this.m_tokenStream[this.m_index] != '=') && (ignoreComma || this.m_tokenStream[this.m_index] != ','))
				{
					if (this.m_tokenStream[this.m_index] == ',')
					{
						this.m_start = this.m_index + 1;
						this.m_tokenLength = -1;
						ignoreComma = false;
					}
					this.m_index++;
					this.m_tokenLength += num;
				}
				if (!this.Eof)
				{
					char c2 = this.m_tokenStream[this.m_index];
					if (c2 != ';')
					{
						if (c2 != '=')
						{
							result = CookieToken.EndCookie;
						}
						else
						{
							result = CookieToken.Equals;
						}
					}
					else
					{
						result = CookieToken.EndToken;
					}
					this.m_index++;
				}
			}
			return result;
		}

		// Token: 0x06001982 RID: 6530 RVA: 0x0005C110 File Offset: 0x0005A310
		internal CookieToken Next(bool first, bool parseResponseCookies)
		{
			this.Reset();
			CookieToken cookieToken = this.FindNext(false, false);
			if (cookieToken == CookieToken.EndCookie)
			{
				this.EndOfCookie = true;
			}
			if (cookieToken == CookieToken.End || cookieToken == CookieToken.EndCookie)
			{
				if ((this.Name = this.Extract()).Length != 0)
				{
					this.Token = this.TokenFromName(parseResponseCookies);
					return CookieToken.Attribute;
				}
				return cookieToken;
			}
			else
			{
				this.Name = this.Extract();
				if (first)
				{
					this.Token = CookieToken.CookieName;
				}
				else
				{
					this.Token = this.TokenFromName(parseResponseCookies);
				}
				if (cookieToken == CookieToken.Equals)
				{
					cookieToken = this.FindNext(!first && this.Token == CookieToken.Expires, true);
					if (cookieToken == CookieToken.EndCookie)
					{
						this.EndOfCookie = true;
					}
					this.Value = this.Extract();
					return CookieToken.NameValuePair;
				}
				return CookieToken.Attribute;
			}
		}

		// Token: 0x06001983 RID: 6531 RVA: 0x0005C1C2 File Offset: 0x0005A3C2
		internal void Reset()
		{
			this.m_eofCookie = false;
			this.m_name = string.Empty;
			this.m_quoted = false;
			this.m_start = this.m_index;
			this.m_token = CookieToken.Nothing;
			this.m_tokenLength = 0;
			this.m_value = string.Empty;
		}

		// Token: 0x06001984 RID: 6532 RVA: 0x0005C204 File Offset: 0x0005A404
		internal CookieToken TokenFromName(bool parseResponseCookies)
		{
			if (!parseResponseCookies)
			{
				for (int i = 0; i < CookieTokenizer.RecognizedServerAttributes.Length; i++)
				{
					if (CookieTokenizer.RecognizedServerAttributes[i].IsEqualTo(this.Name))
					{
						return CookieTokenizer.RecognizedServerAttributes[i].Token;
					}
				}
			}
			else
			{
				for (int j = 0; j < CookieTokenizer.RecognizedAttributes.Length; j++)
				{
					if (CookieTokenizer.RecognizedAttributes[j].IsEqualTo(this.Name))
					{
						return CookieTokenizer.RecognizedAttributes[j].Token;
					}
				}
			}
			return CookieToken.Unknown;
		}

		// Token: 0x06001985 RID: 6533 RVA: 0x0005C290 File Offset: 0x0005A490
		// Note: this type is marked as 'beforefieldinit'.
		static CookieTokenizer()
		{
		}

		// Token: 0x04001838 RID: 6200
		private bool m_eofCookie;

		// Token: 0x04001839 RID: 6201
		private int m_index;

		// Token: 0x0400183A RID: 6202
		private int m_length;

		// Token: 0x0400183B RID: 6203
		private string m_name;

		// Token: 0x0400183C RID: 6204
		private bool m_quoted;

		// Token: 0x0400183D RID: 6205
		private int m_start;

		// Token: 0x0400183E RID: 6206
		private CookieToken m_token;

		// Token: 0x0400183F RID: 6207
		private int m_tokenLength;

		// Token: 0x04001840 RID: 6208
		private string m_tokenStream;

		// Token: 0x04001841 RID: 6209
		private string m_value;

		// Token: 0x04001842 RID: 6210
		private static CookieTokenizer.RecognizedAttribute[] RecognizedAttributes = new CookieTokenizer.RecognizedAttribute[]
		{
			new CookieTokenizer.RecognizedAttribute("Path", CookieToken.Path),
			new CookieTokenizer.RecognizedAttribute("Max-Age", CookieToken.MaxAge),
			new CookieTokenizer.RecognizedAttribute("Expires", CookieToken.Expires),
			new CookieTokenizer.RecognizedAttribute("Version", CookieToken.Version),
			new CookieTokenizer.RecognizedAttribute("Domain", CookieToken.Domain),
			new CookieTokenizer.RecognizedAttribute("Secure", CookieToken.Secure),
			new CookieTokenizer.RecognizedAttribute("Discard", CookieToken.Discard),
			new CookieTokenizer.RecognizedAttribute("Port", CookieToken.Port),
			new CookieTokenizer.RecognizedAttribute("Comment", CookieToken.Comment),
			new CookieTokenizer.RecognizedAttribute("CommentURL", CookieToken.CommentUrl),
			new CookieTokenizer.RecognizedAttribute("HttpOnly", CookieToken.HttpOnly)
		};

		// Token: 0x04001843 RID: 6211
		private static CookieTokenizer.RecognizedAttribute[] RecognizedServerAttributes = new CookieTokenizer.RecognizedAttribute[]
		{
			new CookieTokenizer.RecognizedAttribute("$Path", CookieToken.Path),
			new CookieTokenizer.RecognizedAttribute("$Version", CookieToken.Version),
			new CookieTokenizer.RecognizedAttribute("$Domain", CookieToken.Domain),
			new CookieTokenizer.RecognizedAttribute("$Port", CookieToken.Port),
			new CookieTokenizer.RecognizedAttribute("$HttpOnly", CookieToken.HttpOnly)
		};

		// Token: 0x02000376 RID: 886
		private struct RecognizedAttribute
		{
			// Token: 0x06001986 RID: 6534 RVA: 0x0005C3E4 File Offset: 0x0005A5E4
			internal RecognizedAttribute(string name, CookieToken token)
			{
				this.m_name = name;
				this.m_token = token;
			}

			// Token: 0x17000551 RID: 1361
			// (get) Token: 0x06001987 RID: 6535 RVA: 0x0005C3F4 File Offset: 0x0005A5F4
			internal CookieToken Token
			{
				get
				{
					return this.m_token;
				}
			}

			// Token: 0x06001988 RID: 6536 RVA: 0x0005C3FC File Offset: 0x0005A5FC
			internal bool IsEqualTo(string value)
			{
				return string.Compare(this.m_name, value, StringComparison.OrdinalIgnoreCase) == 0;
			}

			// Token: 0x04001844 RID: 6212
			private string m_name;

			// Token: 0x04001845 RID: 6213
			private CookieToken m_token;
		}
	}
}
