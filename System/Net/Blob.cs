﻿using System;

namespace System.Net
{
	// Token: 0x0200031D RID: 797
	internal struct Blob
	{
		// Token: 0x0400165B RID: 5723
		public int cbSize;

		// Token: 0x0400165C RID: 5724
		public int pBlobData;
	}
}
