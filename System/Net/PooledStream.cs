﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Security.Permissions;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net
{
	// Token: 0x02000355 RID: 853
	internal class PooledStream : Stream
	{
		// Token: 0x06001877 RID: 6263 RVA: 0x000584C0 File Offset: 0x000566C0
		internal PooledStream(object owner)
		{
			this.m_Owner = new WeakReference(owner);
			this.m_PooledCount = -1;
			this.m_Initalizing = true;
			this.m_NetworkStream = new NetworkStream();
			this.m_CreateTime = DateTime.UtcNow;
		}

		// Token: 0x06001878 RID: 6264 RVA: 0x000584F8 File Offset: 0x000566F8
		internal PooledStream(ConnectionPool connectionPool, TimeSpan lifetime, bool checkLifetime)
		{
			this.m_ConnectionPool = connectionPool;
			this.m_Lifetime = lifetime;
			this.m_CheckLifetime = checkLifetime;
			this.m_Initalizing = true;
			this.m_NetworkStream = new NetworkStream();
			this.m_CreateTime = DateTime.UtcNow;
		}

		// Token: 0x17000506 RID: 1286
		// (get) Token: 0x06001879 RID: 6265 RVA: 0x00058532 File Offset: 0x00056732
		internal bool JustConnected
		{
			get
			{
				if (this.m_JustConnected)
				{
					this.m_JustConnected = false;
					return true;
				}
				return false;
			}
		}

		// Token: 0x17000507 RID: 1287
		// (get) Token: 0x0600187A RID: 6266 RVA: 0x00058546 File Offset: 0x00056746
		internal IPAddress ServerAddress
		{
			get
			{
				return this.m_ServerAddress;
			}
		}

		// Token: 0x17000508 RID: 1288
		// (get) Token: 0x0600187B RID: 6267 RVA: 0x0005854E File Offset: 0x0005674E
		internal bool IsInitalizing
		{
			get
			{
				return this.m_Initalizing;
			}
		}

		// Token: 0x17000509 RID: 1289
		// (get) Token: 0x0600187C RID: 6268 RVA: 0x00058558 File Offset: 0x00056758
		// (set) Token: 0x0600187D RID: 6269 RVA: 0x0005859D File Offset: 0x0005679D
		internal bool CanBePooled
		{
			get
			{
				if (this.m_Initalizing)
				{
					return true;
				}
				if (!this.m_NetworkStream.Connected)
				{
					return false;
				}
				WeakReference owner = this.m_Owner;
				return !this.m_ConnectionIsDoomed && (owner == null || !owner.IsAlive);
			}
			set
			{
				this.m_ConnectionIsDoomed |= !value;
			}
		}

		// Token: 0x1700050A RID: 1290
		// (get) Token: 0x0600187E RID: 6270 RVA: 0x000585B0 File Offset: 0x000567B0
		internal bool IsEmancipated
		{
			get
			{
				WeakReference owner = this.m_Owner;
				return 0 >= this.m_PooledCount && (owner == null || !owner.IsAlive);
			}
		}

		// Token: 0x1700050B RID: 1291
		// (get) Token: 0x0600187F RID: 6271 RVA: 0x000585E0 File Offset: 0x000567E0
		// (set) Token: 0x06001880 RID: 6272 RVA: 0x00058608 File Offset: 0x00056808
		internal object Owner
		{
			get
			{
				WeakReference owner = this.m_Owner;
				if (owner != null && owner.IsAlive)
				{
					return owner.Target;
				}
				return null;
			}
			set
			{
				lock (this)
				{
					if (this.m_Owner != null)
					{
						this.m_Owner.Target = value;
					}
				}
			}
		}

		// Token: 0x1700050C RID: 1292
		// (get) Token: 0x06001881 RID: 6273 RVA: 0x00058654 File Offset: 0x00056854
		internal ConnectionPool Pool
		{
			get
			{
				return this.m_ConnectionPool;
			}
		}

		// Token: 0x1700050D RID: 1293
		// (get) Token: 0x06001882 RID: 6274 RVA: 0x0005865C File Offset: 0x0005685C
		internal virtual ServicePoint ServicePoint
		{
			get
			{
				return this.Pool.ServicePoint;
			}
		}

		// Token: 0x06001883 RID: 6275 RVA: 0x00058669 File Offset: 0x00056869
		internal bool Activate(object owningObject, GeneralAsyncDelegate asyncCallback)
		{
			return this.Activate(owningObject, asyncCallback != null, asyncCallback);
		}

		// Token: 0x06001884 RID: 6276 RVA: 0x00058678 File Offset: 0x00056878
		protected bool Activate(object owningObject, bool async, GeneralAsyncDelegate asyncCallback)
		{
			bool result;
			try
			{
				if (this.m_Initalizing)
				{
					IPAddress serverAddress = null;
					this.m_AsyncCallback = asyncCallback;
					Socket connection = this.ServicePoint.GetConnection(this, owningObject, async, out serverAddress, ref this.m_AbortSocket, ref this.m_AbortSocket6);
					if (connection != null)
					{
						bool on = Logging.On;
						this.m_NetworkStream.InitNetworkStream(connection, FileAccess.ReadWrite);
						this.m_ServerAddress = serverAddress;
						this.m_Initalizing = false;
						this.m_JustConnected = true;
						this.m_AbortSocket = null;
						this.m_AbortSocket6 = null;
						result = true;
					}
					else
					{
						result = false;
					}
				}
				else
				{
					if (async && asyncCallback != null)
					{
						asyncCallback(owningObject, this);
					}
					result = true;
				}
			}
			catch
			{
				this.m_Initalizing = false;
				throw;
			}
			return result;
		}

		// Token: 0x06001885 RID: 6277 RVA: 0x00058724 File Offset: 0x00056924
		internal void Deactivate()
		{
			this.m_AsyncCallback = null;
			if (!this.m_ConnectionIsDoomed && this.m_CheckLifetime)
			{
				this.CheckLifetime();
			}
		}

		// Token: 0x06001886 RID: 6278 RVA: 0x00058744 File Offset: 0x00056944
		internal virtual void ConnectionCallback(object owningObject, Exception e, Socket socket, IPAddress address)
		{
			object state = null;
			if (e != null)
			{
				this.m_Initalizing = false;
				state = e;
			}
			else
			{
				try
				{
					bool on = Logging.On;
					this.m_NetworkStream.InitNetworkStream(socket, FileAccess.ReadWrite);
					state = this;
				}
				catch (Exception ex)
				{
					if (NclUtilities.IsFatal(ex))
					{
						throw;
					}
					state = ex;
				}
				this.m_ServerAddress = address;
				this.m_Initalizing = false;
				this.m_JustConnected = true;
			}
			if (this.m_AsyncCallback != null)
			{
				this.m_AsyncCallback(owningObject, state);
			}
			this.m_AbortSocket = null;
			this.m_AbortSocket6 = null;
		}

		// Token: 0x06001887 RID: 6279 RVA: 0x000587D0 File Offset: 0x000569D0
		protected void CheckLifetime()
		{
			if (!this.m_ConnectionIsDoomed)
			{
				TimeSpan t = DateTime.UtcNow.Subtract(this.m_CreateTime);
				this.m_ConnectionIsDoomed = (0 < TimeSpan.Compare(this.m_Lifetime, t));
			}
		}

		// Token: 0x06001888 RID: 6280 RVA: 0x00058814 File Offset: 0x00056A14
		internal void UpdateLifetime()
		{
			int connectionLeaseTimeout = this.ServicePoint.ConnectionLeaseTimeout;
			TimeSpan maxValue;
			if (connectionLeaseTimeout == -1)
			{
				maxValue = TimeSpan.MaxValue;
				this.m_CheckLifetime = false;
			}
			else
			{
				maxValue = new TimeSpan(0, 0, 0, 0, connectionLeaseTimeout);
				this.m_CheckLifetime = true;
			}
			if (maxValue != this.m_Lifetime)
			{
				this.m_Lifetime = maxValue;
			}
		}

		// Token: 0x06001889 RID: 6281 RVA: 0x00058868 File Offset: 0x00056A68
		internal void PrePush(object expectedOwner)
		{
			lock (this)
			{
				if (expectedOwner == null)
				{
					if (this.m_Owner != null && this.m_Owner.Target != null)
					{
						throw new InternalException();
					}
				}
				else if (this.m_Owner == null || this.m_Owner.Target != expectedOwner)
				{
					throw new InternalException();
				}
				this.m_PooledCount++;
				if (1 != this.m_PooledCount)
				{
					throw new InternalException();
				}
				if (this.m_Owner != null)
				{
					this.m_Owner.Target = null;
				}
			}
		}

		// Token: 0x0600188A RID: 6282 RVA: 0x00058908 File Offset: 0x00056B08
		internal void PostPop(object newOwner)
		{
			lock (this)
			{
				if (this.m_Owner == null)
				{
					this.m_Owner = new WeakReference(newOwner);
				}
				else
				{
					if (this.m_Owner.Target != null)
					{
						throw new InternalException();
					}
					this.m_Owner.Target = newOwner;
				}
				this.m_PooledCount--;
				if (this.Pool != null)
				{
					if (this.m_PooledCount != 0)
					{
						throw new InternalException();
					}
				}
				else if (-1 != this.m_PooledCount)
				{
					throw new InternalException();
				}
			}
		}

		// Token: 0x1700050E RID: 1294
		// (get) Token: 0x0600188B RID: 6283 RVA: 0x00005AFA File Offset: 0x00003CFA
		protected bool UsingSecureStream
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700050F RID: 1295
		// (get) Token: 0x0600188C RID: 6284 RVA: 0x000589A8 File Offset: 0x00056BA8
		// (set) Token: 0x0600188D RID: 6285 RVA: 0x000589B0 File Offset: 0x00056BB0
		internal NetworkStream NetworkStream
		{
			get
			{
				return this.m_NetworkStream;
			}
			set
			{
				this.m_Initalizing = false;
				this.m_NetworkStream = value;
			}
		}

		// Token: 0x17000510 RID: 1296
		// (get) Token: 0x0600188E RID: 6286 RVA: 0x000589C0 File Offset: 0x00056BC0
		protected Socket Socket
		{
			get
			{
				return this.m_NetworkStream.InternalSocket;
			}
		}

		// Token: 0x17000511 RID: 1297
		// (get) Token: 0x0600188F RID: 6287 RVA: 0x000589CD File Offset: 0x00056BCD
		public override bool CanRead
		{
			get
			{
				return this.m_NetworkStream.CanRead;
			}
		}

		// Token: 0x17000512 RID: 1298
		// (get) Token: 0x06001890 RID: 6288 RVA: 0x000589DA File Offset: 0x00056BDA
		public override bool CanSeek
		{
			get
			{
				return this.m_NetworkStream.CanSeek;
			}
		}

		// Token: 0x17000513 RID: 1299
		// (get) Token: 0x06001891 RID: 6289 RVA: 0x000589E7 File Offset: 0x00056BE7
		public override bool CanWrite
		{
			get
			{
				return this.m_NetworkStream.CanWrite;
			}
		}

		// Token: 0x17000514 RID: 1300
		// (get) Token: 0x06001892 RID: 6290 RVA: 0x000589F4 File Offset: 0x00056BF4
		public override bool CanTimeout
		{
			get
			{
				return this.m_NetworkStream.CanTimeout;
			}
		}

		// Token: 0x17000515 RID: 1301
		// (get) Token: 0x06001893 RID: 6291 RVA: 0x00058A01 File Offset: 0x00056C01
		// (set) Token: 0x06001894 RID: 6292 RVA: 0x00058A0E File Offset: 0x00056C0E
		public override int ReadTimeout
		{
			get
			{
				return this.m_NetworkStream.ReadTimeout;
			}
			set
			{
				this.m_NetworkStream.ReadTimeout = value;
			}
		}

		// Token: 0x17000516 RID: 1302
		// (get) Token: 0x06001895 RID: 6293 RVA: 0x00058A1C File Offset: 0x00056C1C
		// (set) Token: 0x06001896 RID: 6294 RVA: 0x00058A29 File Offset: 0x00056C29
		public override int WriteTimeout
		{
			get
			{
				return this.m_NetworkStream.WriteTimeout;
			}
			set
			{
				this.m_NetworkStream.WriteTimeout = value;
			}
		}

		// Token: 0x17000517 RID: 1303
		// (get) Token: 0x06001897 RID: 6295 RVA: 0x00058A37 File Offset: 0x00056C37
		public override long Length
		{
			get
			{
				return this.m_NetworkStream.Length;
			}
		}

		// Token: 0x17000518 RID: 1304
		// (get) Token: 0x06001898 RID: 6296 RVA: 0x00058A44 File Offset: 0x00056C44
		// (set) Token: 0x06001899 RID: 6297 RVA: 0x00058A51 File Offset: 0x00056C51
		public override long Position
		{
			get
			{
				return this.m_NetworkStream.Position;
			}
			set
			{
				this.m_NetworkStream.Position = value;
			}
		}

		// Token: 0x0600189A RID: 6298 RVA: 0x00058A5F File Offset: 0x00056C5F
		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.m_NetworkStream.Seek(offset, origin);
		}

		// Token: 0x0600189B RID: 6299 RVA: 0x00058A6E File Offset: 0x00056C6E
		public override int Read(byte[] buffer, int offset, int size)
		{
			return this.m_NetworkStream.Read(buffer, offset, size);
		}

		// Token: 0x0600189C RID: 6300 RVA: 0x00058A7E File Offset: 0x00056C7E
		public override void Write(byte[] buffer, int offset, int size)
		{
			this.m_NetworkStream.Write(buffer, offset, size);
		}

		// Token: 0x0600189D RID: 6301 RVA: 0x00058A8E File Offset: 0x00056C8E
		internal void MultipleWrite(BufferOffsetSize[] buffers)
		{
			this.m_NetworkStream.MultipleWrite(buffers);
		}

		// Token: 0x0600189E RID: 6302 RVA: 0x00058A9C File Offset: 0x00056C9C
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
					this.m_Owner = null;
					this.m_ConnectionIsDoomed = true;
					this.CloseSocket();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x0600189F RID: 6303 RVA: 0x00058ADC File Offset: 0x00056CDC
		internal void CloseSocket()
		{
			Socket abortSocket = this.m_AbortSocket;
			Socket abortSocket2 = this.m_AbortSocket6;
			this.m_NetworkStream.Close();
			if (abortSocket != null)
			{
				abortSocket.Close();
			}
			if (abortSocket2 != null)
			{
				abortSocket2.Close();
			}
		}

		// Token: 0x060018A0 RID: 6304 RVA: 0x00058B14 File Offset: 0x00056D14
		public void Close(int timeout)
		{
			Socket abortSocket = this.m_AbortSocket;
			Socket abortSocket2 = this.m_AbortSocket6;
			this.m_NetworkStream.Close(timeout);
			if (abortSocket != null)
			{
				abortSocket.Close(timeout);
			}
			if (abortSocket2 != null)
			{
				abortSocket2.Close(timeout);
			}
		}

		// Token: 0x060018A1 RID: 6305 RVA: 0x00058B4F File Offset: 0x00056D4F
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
		{
			return this.m_NetworkStream.BeginRead(buffer, offset, size, callback, state);
		}

		// Token: 0x060018A2 RID: 6306 RVA: 0x00058B63 File Offset: 0x00056D63
		internal virtual IAsyncResult UnsafeBeginRead(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
		{
			return this.m_NetworkStream.UnsafeBeginRead(buffer, offset, size, callback, state);
		}

		// Token: 0x060018A3 RID: 6307 RVA: 0x00058B77 File Offset: 0x00056D77
		public override int EndRead(IAsyncResult asyncResult)
		{
			return this.m_NetworkStream.EndRead(asyncResult);
		}

		// Token: 0x060018A4 RID: 6308 RVA: 0x00058B85 File Offset: 0x00056D85
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
		{
			return this.m_NetworkStream.BeginWrite(buffer, offset, size, callback, state);
		}

		// Token: 0x060018A5 RID: 6309 RVA: 0x00058B99 File Offset: 0x00056D99
		internal virtual IAsyncResult UnsafeBeginWrite(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
		{
			return this.m_NetworkStream.UnsafeBeginWrite(buffer, offset, size, callback, state);
		}

		// Token: 0x060018A6 RID: 6310 RVA: 0x00058BAD File Offset: 0x00056DAD
		public override void EndWrite(IAsyncResult asyncResult)
		{
			this.m_NetworkStream.EndWrite(asyncResult);
		}

		// Token: 0x060018A7 RID: 6311 RVA: 0x00058BBB File Offset: 0x00056DBB
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		internal IAsyncResult BeginMultipleWrite(BufferOffsetSize[] buffers, AsyncCallback callback, object state)
		{
			return this.m_NetworkStream.BeginMultipleWrite(buffers, callback, state);
		}

		// Token: 0x060018A8 RID: 6312 RVA: 0x00058BCB File Offset: 0x00056DCB
		internal void EndMultipleWrite(IAsyncResult asyncResult)
		{
			this.m_NetworkStream.EndMultipleWrite(asyncResult);
		}

		// Token: 0x060018A9 RID: 6313 RVA: 0x00058BD9 File Offset: 0x00056DD9
		public override void Flush()
		{
			this.m_NetworkStream.Flush();
		}

		// Token: 0x060018AA RID: 6314 RVA: 0x00058BE6 File Offset: 0x00056DE6
		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			return this.m_NetworkStream.FlushAsync(cancellationToken);
		}

		// Token: 0x060018AB RID: 6315 RVA: 0x00058BF4 File Offset: 0x00056DF4
		public override void SetLength(long value)
		{
			this.m_NetworkStream.SetLength(value);
		}

		// Token: 0x060018AC RID: 6316 RVA: 0x00058C02 File Offset: 0x00056E02
		internal void SetSocketTimeoutOption(SocketShutdown mode, int timeout, bool silent)
		{
			this.m_NetworkStream.SetSocketTimeoutOption(mode, timeout, silent);
		}

		// Token: 0x060018AD RID: 6317 RVA: 0x00058C12 File Offset: 0x00056E12
		internal bool Poll(int microSeconds, SelectMode mode)
		{
			return this.m_NetworkStream.Poll(microSeconds, mode);
		}

		// Token: 0x060018AE RID: 6318 RVA: 0x00058C21 File Offset: 0x00056E21
		internal bool PollRead()
		{
			return this.m_NetworkStream.PollRead();
		}

		// Token: 0x04001783 RID: 6019
		private bool m_CheckLifetime;

		// Token: 0x04001784 RID: 6020
		private TimeSpan m_Lifetime;

		// Token: 0x04001785 RID: 6021
		private DateTime m_CreateTime;

		// Token: 0x04001786 RID: 6022
		private bool m_ConnectionIsDoomed;

		// Token: 0x04001787 RID: 6023
		private ConnectionPool m_ConnectionPool;

		// Token: 0x04001788 RID: 6024
		private WeakReference m_Owner;

		// Token: 0x04001789 RID: 6025
		private int m_PooledCount;

		// Token: 0x0400178A RID: 6026
		private bool m_Initalizing;

		// Token: 0x0400178B RID: 6027
		private IPAddress m_ServerAddress;

		// Token: 0x0400178C RID: 6028
		private NetworkStream m_NetworkStream;

		// Token: 0x0400178D RID: 6029
		private Socket m_AbortSocket;

		// Token: 0x0400178E RID: 6030
		private Socket m_AbortSocket6;

		// Token: 0x0400178F RID: 6031
		private bool m_JustConnected;

		// Token: 0x04001790 RID: 6032
		private GeneralAsyncDelegate m_AsyncCallback;
	}
}
