﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Net
{
	/// <summary>Controls rights to access Domain Name System (DNS) servers on the network.</summary>
	// Token: 0x0200062D RID: 1581
	[Serializable]
	public sealed class DnsPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Net.DnsPermission" /> class that either allows unrestricted DNS access or disallows DNS access.</summary>
		/// <param name="state">One of the <see cref="T:System.Security.Permissions.PermissionState" /> values. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="state" /> is not a valid <see cref="T:System.Security.Permissions.PermissionState" /> value. </exception>
		// Token: 0x060032B3 RID: 12979 RVA: 0x000092E2 File Offset: 0x000074E2
		public DnsPermission(PermissionState state)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates an identical copy of the current permission instance.</summary>
		/// <returns>A new instance of the <see cref="T:System.Net.DnsPermission" /> class that is an identical copy of the current instance.</returns>
		// Token: 0x060032B4 RID: 12980 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission Copy()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Reconstructs a <see cref="T:System.Net.DnsPermission" /> instance from an XML encoding.</summary>
		/// <param name="securityElement">The XML encoding to use to reconstruct the <see cref="T:System.Net.DnsPermission" /> instance. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="securityElement" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="securityElement" /> is not a <see cref="T:System.Net.DnsPermission" /> element. </exception>
		// Token: 0x060032B5 RID: 12981 RVA: 0x000092E2 File Offset: 0x000074E2
		public override void FromXml(SecurityElement securityElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a permission instance that is the intersection of the current permission instance and the specified permission instance.</summary>
		/// <param name="target">The <see cref="T:System.Net.DnsPermission" /> instance to intersect with the current instance. </param>
		/// <returns>A <see cref="T:System.Net.DnsPermission" /> instance that represents the intersection of the current <see cref="T:System.Net.DnsPermission" /> instance with the specified <see cref="T:System.Net.DnsPermission" /> instance, or <see langword="null" /> if the intersection is empty. If both the current instance and <paramref name="target" /> are unrestricted, this method returns a new <see cref="T:System.Net.DnsPermission" /> instance that is unrestricted; otherwise, it returns <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> is neither a <see cref="T:System.Net.DnsPermission" /> nor <see langword="null" />. </exception>
		// Token: 0x060032B6 RID: 12982 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission Intersect(IPermission target)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Determines whether the current permission instance is a subset of the specified permission instance.</summary>
		/// <param name="target">The second <see cref="T:System.Net.DnsPermission" /> instance to be tested for the subset relationship. </param>
		/// <returns>
		///     <see langword="false" /> if the current instance is unrestricted and <paramref name="target" /> is either <see langword="null" /> or unrestricted; otherwise, <see langword="true" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> is neither a <see cref="T:System.Net.DnsPermission" /> nor <see langword="null" />. </exception>
		// Token: 0x060032B7 RID: 12983 RVA: 0x000A4AD8 File Offset: 0x000A2CD8
		public override bool IsSubsetOf(IPermission target)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Checks the overall permission state of the object.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Net.DnsPermission" /> instance was created with <see cref="F:System.Security.Permissions.PermissionState.Unrestricted" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060032B8 RID: 12984 RVA: 0x000A4AF4 File Offset: 0x000A2CF4
		public bool IsUnrestricted()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Creates an XML encoding of a <see cref="T:System.Net.DnsPermission" /> instance and its current state.</summary>
		/// <returns>A <see cref="T:System.Security.SecurityElement" /> instance that contains an XML-encoded representation of the security object, including state information.</returns>
		// Token: 0x060032B9 RID: 12985 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override SecurityElement ToXml()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
