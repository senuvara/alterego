﻿using System;
using System.Net.WebSockets;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace System.Net
{
	/// <summary>Provides access to the request and response objects used by the <see cref="T:System.Net.HttpListener" /> class. This class cannot be inherited.</summary>
	// Token: 0x020003E3 RID: 995
	public sealed class HttpListenerContext
	{
		// Token: 0x06001DC3 RID: 7619 RVA: 0x0006BCF2 File Offset: 0x00069EF2
		internal HttpListenerContext(HttpConnection cnc)
		{
			this.err_status = 400;
			base..ctor();
			this.cnc = cnc;
			this.request = new HttpListenerRequest(this);
			this.response = new HttpListenerResponse(this);
		}

		// Token: 0x1700061A RID: 1562
		// (get) Token: 0x06001DC4 RID: 7620 RVA: 0x0006BD24 File Offset: 0x00069F24
		// (set) Token: 0x06001DC5 RID: 7621 RVA: 0x0006BD2C File Offset: 0x00069F2C
		internal int ErrorStatus
		{
			get
			{
				return this.err_status;
			}
			set
			{
				this.err_status = value;
			}
		}

		// Token: 0x1700061B RID: 1563
		// (get) Token: 0x06001DC6 RID: 7622 RVA: 0x0006BD35 File Offset: 0x00069F35
		// (set) Token: 0x06001DC7 RID: 7623 RVA: 0x0006BD3D File Offset: 0x00069F3D
		internal string ErrorMessage
		{
			get
			{
				return this.error;
			}
			set
			{
				this.error = value;
			}
		}

		// Token: 0x1700061C RID: 1564
		// (get) Token: 0x06001DC8 RID: 7624 RVA: 0x0006BD46 File Offset: 0x00069F46
		internal bool HaveError
		{
			get
			{
				return this.error != null;
			}
		}

		// Token: 0x1700061D RID: 1565
		// (get) Token: 0x06001DC9 RID: 7625 RVA: 0x0006BD51 File Offset: 0x00069F51
		internal HttpConnection Connection
		{
			get
			{
				return this.cnc;
			}
		}

		/// <summary>Gets the <see cref="T:System.Net.HttpListenerRequest" /> that represents a client's request for a resource.</summary>
		/// <returns>An <see cref="T:System.Net.HttpListenerRequest" /> object that represents the client request.</returns>
		// Token: 0x1700061E RID: 1566
		// (get) Token: 0x06001DCA RID: 7626 RVA: 0x0006BD59 File Offset: 0x00069F59
		public HttpListenerRequest Request
		{
			get
			{
				return this.request;
			}
		}

		/// <summary>Gets the <see cref="T:System.Net.HttpListenerResponse" /> object that will be sent to the client in response to the client's request. </summary>
		/// <returns>An <see cref="T:System.Net.HttpListenerResponse" /> object used to send a response back to the client.</returns>
		// Token: 0x1700061F RID: 1567
		// (get) Token: 0x06001DCB RID: 7627 RVA: 0x0006BD61 File Offset: 0x00069F61
		public HttpListenerResponse Response
		{
			get
			{
				return this.response;
			}
		}

		/// <summary>Gets an object used to obtain identity, authentication information, and security roles for the client whose request is represented by this <see cref="T:System.Net.HttpListenerContext" /> object. </summary>
		/// <returns>An <see cref="T:System.Security.Principal.IPrincipal" /> object that describes the client, or <see langword="null" /> if the <see cref="T:System.Net.HttpListener" /> that supplied this <see cref="T:System.Net.HttpListenerContext" /> does not require authentication.</returns>
		// Token: 0x17000620 RID: 1568
		// (get) Token: 0x06001DCC RID: 7628 RVA: 0x0006BD69 File Offset: 0x00069F69
		public IPrincipal User
		{
			get
			{
				return this.user;
			}
		}

		// Token: 0x06001DCD RID: 7629 RVA: 0x0006BD74 File Offset: 0x00069F74
		internal void ParseAuthentication(AuthenticationSchemes expectedSchemes)
		{
			if (expectedSchemes == AuthenticationSchemes.Anonymous)
			{
				return;
			}
			string text = this.request.Headers["Authorization"];
			if (text == null || text.Length < 2)
			{
				return;
			}
			string[] array = text.Split(new char[]
			{
				' '
			}, 2);
			if (string.Compare(array[0], "basic", true) == 0)
			{
				this.user = this.ParseBasicAuthentication(array[1]);
			}
		}

		// Token: 0x06001DCE RID: 7630 RVA: 0x0006BDE0 File Offset: 0x00069FE0
		internal IPrincipal ParseBasicAuthentication(string authData)
		{
			IPrincipal result;
			try
			{
				string text = Encoding.Default.GetString(Convert.FromBase64String(authData));
				int num = text.IndexOf(':');
				string password = text.Substring(num + 1);
				text = text.Substring(0, num);
				num = text.IndexOf('\\');
				string username;
				if (num > 0)
				{
					username = text.Substring(num);
				}
				else
				{
					username = text;
				}
				result = new GenericPrincipal(new HttpListenerBasicIdentity(username, password), new string[0]);
			}
			catch (Exception)
			{
				result = null;
			}
			return result;
		}

		/// <summary>Accept a WebSocket connection as an asynchronous operation.</summary>
		/// <param name="subProtocol">The supported WebSocket sub-protocol.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns an <see cref="T:System.Net.WebSockets.HttpListenerWebSocketContext" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="subProtocol" /> is an empty string-or- 
		///         <paramref name="subProtocol" /> contains illegal characters.</exception>
		/// <exception cref="T:System.Net.WebSockets.WebSocketException">An error occurred when sending the response to complete the WebSocket handshake.</exception>
		// Token: 0x06001DCF RID: 7631 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public Task<HttpListenerWebSocketContext> AcceptWebSocketAsync(string subProtocol)
		{
			throw new NotImplementedException();
		}

		/// <summary>Accept a WebSocket connection specifying the supported WebSocket sub-protocol  and WebSocket keep-alive interval as an asynchronous operation.</summary>
		/// <param name="subProtocol">The supported WebSocket sub-protocol.</param>
		/// <param name="keepAliveInterval">The WebSocket protocol keep-alive interval in milliseconds.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns an <see cref="T:System.Net.WebSockets.HttpListenerWebSocketContext" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="subProtocol" /> is an empty string-or- 
		///         <paramref name="subProtocol" /> contains illegal characters.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="keepAliveInterval" /> is too small.</exception>
		/// <exception cref="T:System.Net.WebSockets.WebSocketException">An error occurred when sending the response to complete the WebSocket handshake.</exception>
		// Token: 0x06001DD0 RID: 7632 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public Task<HttpListenerWebSocketContext> AcceptWebSocketAsync(string subProtocol, TimeSpan keepAliveInterval)
		{
			throw new NotImplementedException();
		}

		/// <summary>Accept a WebSocket connection specifying the supported WebSocket sub-protocol, receive buffer size, and WebSocket keep-alive interval as an asynchronous operation.</summary>
		/// <param name="subProtocol">The supported WebSocket sub-protocol.</param>
		/// <param name="receiveBufferSize">The receive buffer size in bytes.</param>
		/// <param name="keepAliveInterval">The WebSocket protocol keep-alive interval in milliseconds.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns an <see cref="T:System.Net.WebSockets.HttpListenerWebSocketContext" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="subProtocol" /> is an empty string-or- 
		///         <paramref name="subProtocol" /> contains illegal characters.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="keepAliveInterval" /> is too small.-or- 
		///         <paramref name="receiveBufferSize" /> is less than 16 bytes-or- 
		///         <paramref name="receiveBufferSize" /> is greater than 64K bytes.</exception>
		/// <exception cref="T:System.Net.WebSockets.WebSocketException">An error occurred when sending the response to complete the WebSocket handshake.</exception>
		// Token: 0x06001DD1 RID: 7633 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public Task<HttpListenerWebSocketContext> AcceptWebSocketAsync(string subProtocol, int receiveBufferSize, TimeSpan keepAliveInterval)
		{
			throw new NotImplementedException();
		}

		/// <summary>Accept a WebSocket connection specifying the supported WebSocket sub-protocol, receive buffer size, WebSocket keep-alive interval, and the internal buffer as an asynchronous operation.</summary>
		/// <param name="subProtocol">The supported WebSocket sub-protocol.</param>
		/// <param name="receiveBufferSize">The receive buffer size in bytes.</param>
		/// <param name="keepAliveInterval">The WebSocket protocol keep-alive interval in milliseconds.</param>
		/// <param name="internalBuffer">An internal buffer to use for this operation.</param>
		/// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1" />.The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result" /> property on the task object returns an <see cref="T:System.Net.WebSockets.HttpListenerWebSocketContext" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="subProtocol" /> is an empty string-or- 
		///         <paramref name="subProtocol" /> contains illegal characters.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="keepAliveInterval" /> is too small.-or- 
		///         <paramref name="receiveBufferSize" /> is less than 16 bytes-or- 
		///         <paramref name="receiveBufferSize" /> is greater than 64K bytes.</exception>
		/// <exception cref="T:System.Net.WebSockets.WebSocketException">An error occurred when sending the response to complete the WebSocket handshake.</exception>
		// Token: 0x06001DD2 RID: 7634 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public Task<HttpListenerWebSocketContext> AcceptWebSocketAsync(string subProtocol, int receiveBufferSize, TimeSpan keepAliveInterval, ArraySegment<byte> internalBuffer)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001DD3 RID: 7635 RVA: 0x000092E2 File Offset: 0x000074E2
		internal HttpListenerContext()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001A22 RID: 6690
		private HttpListenerRequest request;

		// Token: 0x04001A23 RID: 6691
		private HttpListenerResponse response;

		// Token: 0x04001A24 RID: 6692
		private IPrincipal user;

		// Token: 0x04001A25 RID: 6693
		private HttpConnection cnc;

		// Token: 0x04001A26 RID: 6694
		private string error;

		// Token: 0x04001A27 RID: 6695
		private int err_status;

		// Token: 0x04001A28 RID: 6696
		internal HttpListener Listener;
	}
}
