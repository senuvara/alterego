﻿using System;
using System.Net.Sockets;

namespace System.Net
{
	/// <summary>Identifies a network address. This is an <see langword="abstract" /> class.</summary>
	// Token: 0x020002E5 RID: 741
	[Serializable]
	public abstract class EndPoint
	{
		/// <summary>Gets the address family to which the endpoint belongs.</summary>
		/// <returns>One of the <see cref="T:System.Net.Sockets.AddressFamily" /> values.</returns>
		/// <exception cref="T:System.NotImplementedException">Any attempt is made to get or set the property when the property is not overridden in a descendant class. </exception>
		// Token: 0x17000487 RID: 1159
		// (get) Token: 0x0600161C RID: 5660 RVA: 0x0004FB6D File Offset: 0x0004DD6D
		public virtual AddressFamily AddressFamily
		{
			get
			{
				throw ExceptionHelper.PropertyNotImplementedException;
			}
		}

		/// <summary>Serializes endpoint information into a <see cref="T:System.Net.SocketAddress" /> instance.</summary>
		/// <returns>A <see cref="T:System.Net.SocketAddress" /> instance that contains the endpoint information.</returns>
		/// <exception cref="T:System.NotImplementedException">Any attempt is made to access the method when the method is not overridden in a descendant class. </exception>
		// Token: 0x0600161D RID: 5661 RVA: 0x0004FB74 File Offset: 0x0004DD74
		public virtual SocketAddress Serialize()
		{
			throw ExceptionHelper.MethodNotImplementedException;
		}

		/// <summary>Creates an <see cref="T:System.Net.EndPoint" /> instance from a <see cref="T:System.Net.SocketAddress" /> instance.</summary>
		/// <param name="socketAddress">The socket address that serves as the endpoint for a connection. </param>
		/// <returns>A new <see cref="T:System.Net.EndPoint" /> instance that is initialized from the specified <see cref="T:System.Net.SocketAddress" /> instance.</returns>
		/// <exception cref="T:System.NotImplementedException">Any attempt is made to access the method when the method is not overridden in a descendant class. </exception>
		// Token: 0x0600161E RID: 5662 RVA: 0x0004FB74 File Offset: 0x0004DD74
		public virtual EndPoint Create(SocketAddress socketAddress)
		{
			throw ExceptionHelper.MethodNotImplementedException;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.EndPoint" /> class. </summary>
		// Token: 0x0600161F RID: 5663 RVA: 0x0000232F File Offset: 0x0000052F
		protected EndPoint()
		{
		}
	}
}
