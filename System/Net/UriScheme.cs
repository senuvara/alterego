﻿using System;

namespace System.Net
{
	// Token: 0x020002DA RID: 730
	internal static class UriScheme
	{
		// Token: 0x04001421 RID: 5153
		public const string File = "file";

		// Token: 0x04001422 RID: 5154
		public const string Ftp = "ftp";

		// Token: 0x04001423 RID: 5155
		public const string Gopher = "gopher";

		// Token: 0x04001424 RID: 5156
		public const string Http = "http";

		// Token: 0x04001425 RID: 5157
		public const string Https = "https";

		// Token: 0x04001426 RID: 5158
		public const string News = "news";

		// Token: 0x04001427 RID: 5159
		public const string NetPipe = "net.pipe";

		// Token: 0x04001428 RID: 5160
		public const string NetTcp = "net.tcp";

		// Token: 0x04001429 RID: 5161
		public const string Nntp = "nntp";

		// Token: 0x0400142A RID: 5162
		public const string Mailto = "mailto";

		// Token: 0x0400142B RID: 5163
		public const string Ws = "ws";

		// Token: 0x0400142C RID: 5164
		public const string Wss = "wss";

		// Token: 0x0400142D RID: 5165
		public const string SchemeDelimiter = "://";
	}
}
