﻿using System;
using System.Threading;

namespace System.Net
{
	// Token: 0x020003F5 RID: 1013
	internal class ListenerAsyncResult : IAsyncResult
	{
		// Token: 0x06001F27 RID: 7975 RVA: 0x00070EC4 File Offset: 0x0006F0C4
		public ListenerAsyncResult(AsyncCallback cb, object state)
		{
			this.cb = cb;
			this.state = state;
		}

		// Token: 0x06001F28 RID: 7976 RVA: 0x00070EE8 File Offset: 0x0006F0E8
		internal void Complete(Exception exc)
		{
			if (this.forward != null)
			{
				this.forward.Complete(exc);
				return;
			}
			this.exception = exc;
			if (this.InGet && exc is ObjectDisposedException)
			{
				this.exception = new HttpListenerException(500, "Listener closed");
			}
			object obj = this.locker;
			lock (obj)
			{
				this.completed = true;
				if (this.handle != null)
				{
					this.handle.Set();
				}
				if (this.cb != null)
				{
					ThreadPool.UnsafeQueueUserWorkItem(ListenerAsyncResult.InvokeCB, this);
				}
			}
		}

		// Token: 0x06001F29 RID: 7977 RVA: 0x00070F94 File Offset: 0x0006F194
		private static void InvokeCallback(object o)
		{
			ListenerAsyncResult listenerAsyncResult = (ListenerAsyncResult)o;
			if (listenerAsyncResult.forward != null)
			{
				ListenerAsyncResult.InvokeCallback(listenerAsyncResult.forward);
				return;
			}
			try
			{
				listenerAsyncResult.cb(listenerAsyncResult);
			}
			catch
			{
			}
		}

		// Token: 0x06001F2A RID: 7978 RVA: 0x00070FE0 File Offset: 0x0006F1E0
		internal void Complete(HttpListenerContext context)
		{
			this.Complete(context, false);
		}

		// Token: 0x06001F2B RID: 7979 RVA: 0x00070FEC File Offset: 0x0006F1EC
		internal void Complete(HttpListenerContext context, bool synch)
		{
			if (this.forward != null)
			{
				this.forward.Complete(context, synch);
				return;
			}
			this.synch = synch;
			this.context = context;
			object obj = this.locker;
			lock (obj)
			{
				AuthenticationSchemes authenticationSchemes = context.Listener.SelectAuthenticationScheme(context);
				if ((authenticationSchemes == AuthenticationSchemes.Basic || context.Listener.AuthenticationSchemes == AuthenticationSchemes.Negotiate) && context.Request.Headers["Authorization"] == null)
				{
					context.Response.StatusCode = 401;
					context.Response.Headers["WWW-Authenticate"] = string.Concat(new object[]
					{
						authenticationSchemes,
						" realm=\"",
						context.Listener.Realm,
						"\""
					});
					context.Response.OutputStream.Close();
					IAsyncResult asyncResult = context.Listener.BeginGetContext(this.cb, this.state);
					this.forward = (ListenerAsyncResult)asyncResult;
					object obj2 = this.forward.locker;
					lock (obj2)
					{
						if (this.handle != null)
						{
							this.forward.handle = this.handle;
						}
					}
					ListenerAsyncResult listenerAsyncResult = this.forward;
					int num = 0;
					while (listenerAsyncResult.forward != null)
					{
						if (num > 20)
						{
							this.Complete(new HttpListenerException(400, "Too many authentication errors"));
						}
						listenerAsyncResult = listenerAsyncResult.forward;
						num++;
					}
				}
				else
				{
					this.completed = true;
					this.synch = false;
					if (this.handle != null)
					{
						this.handle.Set();
					}
					if (this.cb != null)
					{
						ThreadPool.UnsafeQueueUserWorkItem(ListenerAsyncResult.InvokeCB, this);
					}
				}
			}
		}

		// Token: 0x06001F2C RID: 7980 RVA: 0x000711F0 File Offset: 0x0006F3F0
		internal HttpListenerContext GetContext()
		{
			if (this.forward != null)
			{
				return this.forward.GetContext();
			}
			if (this.exception != null)
			{
				throw this.exception;
			}
			return this.context;
		}

		// Token: 0x170006A9 RID: 1705
		// (get) Token: 0x06001F2D RID: 7981 RVA: 0x0007121B File Offset: 0x0006F41B
		public object AsyncState
		{
			get
			{
				if (this.forward != null)
				{
					return this.forward.AsyncState;
				}
				return this.state;
			}
		}

		// Token: 0x170006AA RID: 1706
		// (get) Token: 0x06001F2E RID: 7982 RVA: 0x00071238 File Offset: 0x0006F438
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				if (this.forward != null)
				{
					return this.forward.AsyncWaitHandle;
				}
				object obj = this.locker;
				lock (obj)
				{
					if (this.handle == null)
					{
						this.handle = new ManualResetEvent(this.completed);
					}
				}
				return this.handle;
			}
		}

		// Token: 0x170006AB RID: 1707
		// (get) Token: 0x06001F2F RID: 7983 RVA: 0x000712A8 File Offset: 0x0006F4A8
		public bool CompletedSynchronously
		{
			get
			{
				if (this.forward != null)
				{
					return this.forward.CompletedSynchronously;
				}
				return this.synch;
			}
		}

		// Token: 0x170006AC RID: 1708
		// (get) Token: 0x06001F30 RID: 7984 RVA: 0x000712C4 File Offset: 0x0006F4C4
		public bool IsCompleted
		{
			get
			{
				if (this.forward != null)
				{
					return this.forward.IsCompleted;
				}
				object obj = this.locker;
				bool result;
				lock (obj)
				{
					result = this.completed;
				}
				return result;
			}
		}

		// Token: 0x06001F31 RID: 7985 RVA: 0x0007131C File Offset: 0x0006F51C
		// Note: this type is marked as 'beforefieldinit'.
		static ListenerAsyncResult()
		{
		}

		// Token: 0x04001AB3 RID: 6835
		private ManualResetEvent handle;

		// Token: 0x04001AB4 RID: 6836
		private bool synch;

		// Token: 0x04001AB5 RID: 6837
		private bool completed;

		// Token: 0x04001AB6 RID: 6838
		private AsyncCallback cb;

		// Token: 0x04001AB7 RID: 6839
		private object state;

		// Token: 0x04001AB8 RID: 6840
		private Exception exception;

		// Token: 0x04001AB9 RID: 6841
		private HttpListenerContext context;

		// Token: 0x04001ABA RID: 6842
		private object locker = new object();

		// Token: 0x04001ABB RID: 6843
		private ListenerAsyncResult forward;

		// Token: 0x04001ABC RID: 6844
		internal bool EndCalled;

		// Token: 0x04001ABD RID: 6845
		internal bool InGet;

		// Token: 0x04001ABE RID: 6846
		private static WaitCallback InvokeCB = new WaitCallback(ListenerAsyncResult.InvokeCallback);
	}
}
