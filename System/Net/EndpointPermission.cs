﻿using System;
using System.Net.Sockets;
using Unity;

namespace System.Net
{
	/// <summary>Defines an endpoint that is authorized by a <see cref="T:System.Net.SocketPermission" /> instance.</summary>
	// Token: 0x020003D3 RID: 979
	[Serializable]
	public class EndpointPermission
	{
		// Token: 0x06001CBA RID: 7354 RVA: 0x00067E09 File Offset: 0x00066009
		internal EndpointPermission(string hostname, int port, TransportType transport)
		{
			if (hostname == null)
			{
				throw new ArgumentNullException("hostname");
			}
			this.hostname = hostname;
			this.port = port;
			this.transport = transport;
			this.resolved = false;
			this.hasWildcard = false;
			this.addresses = null;
		}

		/// <summary>Gets the DNS host name or IP address of the server that is associated with this endpoint.</summary>
		/// <returns>A string that contains the DNS host name or IP address of the server.</returns>
		// Token: 0x170005D0 RID: 1488
		// (get) Token: 0x06001CBB RID: 7355 RVA: 0x00067E49 File Offset: 0x00066049
		public string Hostname
		{
			get
			{
				return this.hostname;
			}
		}

		/// <summary>Gets the network port number that is associated with this endpoint.</summary>
		/// <returns>The network port number that is associated with this request, or <see cref="F:System.Net.SocketPermission.AllPorts" />.</returns>
		// Token: 0x170005D1 RID: 1489
		// (get) Token: 0x06001CBC RID: 7356 RVA: 0x00067E51 File Offset: 0x00066051
		public int Port
		{
			get
			{
				return this.port;
			}
		}

		/// <summary>Gets the transport type that is associated with this endpoint.</summary>
		/// <returns>One of the <see cref="T:System.Net.TransportType" /> values.</returns>
		// Token: 0x170005D2 RID: 1490
		// (get) Token: 0x06001CBD RID: 7357 RVA: 0x00067E59 File Offset: 0x00066059
		public TransportType Transport
		{
			get
			{
				return this.transport;
			}
		}

		/// <summary>Determines whether the specified <see langword="Object" /> is equal to the current <see langword="Object" />.</summary>
		/// <param name="obj">The <see cref="T:System.Object" /> to compare with the current <see langword="Object" />.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see langword="Object" /> is equal to the current <see langword="Object" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001CBE RID: 7358 RVA: 0x00067E64 File Offset: 0x00066064
		public override bool Equals(object obj)
		{
			EndpointPermission endpointPermission = obj as EndpointPermission;
			return endpointPermission != null && this.port == endpointPermission.port && this.transport == endpointPermission.transport && string.Compare(this.hostname, endpointPermission.hostname, true) == 0;
		}

		/// <summary>Serves as a hash function for a particular type.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Object" />.</returns>
		// Token: 0x06001CBF RID: 7359 RVA: 0x00067EAE File Offset: 0x000660AE
		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Net.EndpointPermission" /> instance.</summary>
		/// <returns>A string that represents the current <see cref="T:System.Net.EndpointPermission" /> instance.</returns>
		// Token: 0x06001CC0 RID: 7360 RVA: 0x00067EBC File Offset: 0x000660BC
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				this.hostname,
				"#",
				this.port,
				"#",
				(int)this.transport
			});
		}

		// Token: 0x06001CC1 RID: 7361 RVA: 0x00067F0C File Offset: 0x0006610C
		internal bool IsSubsetOf(EndpointPermission perm)
		{
			if (perm == null)
			{
				return false;
			}
			if (perm.port != -1 && this.port != perm.port)
			{
				return false;
			}
			if (perm.transport != TransportType.All && this.transport != perm.transport)
			{
				return false;
			}
			this.Resolve();
			perm.Resolve();
			if (this.hasWildcard)
			{
				return perm.hasWildcard && this.IsSubsetOf(this.hostname, perm.hostname);
			}
			if (this.addresses == null)
			{
				return false;
			}
			if (perm.hasWildcard)
			{
				foreach (IPAddress ipaddress in this.addresses)
				{
					if (this.IsSubsetOf(ipaddress.ToString(), perm.hostname))
					{
						return true;
					}
				}
			}
			if (perm.addresses == null)
			{
				return false;
			}
			foreach (IPAddress ipaddress2 in perm.addresses)
			{
				if (this.IsSubsetOf(this.hostname, ipaddress2.ToString()))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001CC2 RID: 7362 RVA: 0x00067FFC File Offset: 0x000661FC
		private bool IsSubsetOf(string addr1, string addr2)
		{
			string[] array = addr1.Split(EndpointPermission.dot_char);
			string[] array2 = addr2.Split(EndpointPermission.dot_char);
			for (int i = 0; i < 4; i++)
			{
				int num = this.ToNumber(array[i]);
				if (num == -1)
				{
					return false;
				}
				int num2 = this.ToNumber(array2[i]);
				if (num2 == -1)
				{
					return false;
				}
				if (num != 256 && num != num2 && num2 != 256)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06001CC3 RID: 7363 RVA: 0x00068068 File Offset: 0x00066268
		internal EndpointPermission Intersect(EndpointPermission perm)
		{
			if (perm == null)
			{
				return null;
			}
			int num;
			if (this.port == perm.port)
			{
				num = this.port;
			}
			else if (this.port == -1)
			{
				num = perm.port;
			}
			else
			{
				if (perm.port != -1)
				{
					return null;
				}
				num = this.port;
			}
			TransportType transportType;
			if (this.transport == perm.transport)
			{
				transportType = this.transport;
			}
			else if (this.transport == TransportType.All)
			{
				transportType = perm.transport;
			}
			else
			{
				if (perm.transport != TransportType.All)
				{
					return null;
				}
				transportType = this.transport;
			}
			string text = this.IntersectHostname(perm);
			if (text == null)
			{
				return null;
			}
			if (!this.hasWildcard)
			{
				return this;
			}
			if (!perm.hasWildcard)
			{
				return perm;
			}
			return new EndpointPermission(text, num, transportType)
			{
				hasWildcard = true,
				resolved = true
			};
		}

		// Token: 0x06001CC4 RID: 7364 RVA: 0x0006812C File Offset: 0x0006632C
		private string IntersectHostname(EndpointPermission perm)
		{
			if (this.hostname == perm.hostname)
			{
				return this.hostname;
			}
			this.Resolve();
			perm.Resolve();
			string text = null;
			if (this.hasWildcard)
			{
				if (perm.hasWildcard)
				{
					text = this.Intersect(this.hostname, perm.hostname);
				}
				else if (perm.addresses != null)
				{
					for (int i = 0; i < perm.addresses.Length; i++)
					{
						text = this.Intersect(this.hostname, perm.addresses[i].ToString());
						if (text != null)
						{
							break;
						}
					}
				}
			}
			else if (this.addresses != null)
			{
				for (int j = 0; j < this.addresses.Length; j++)
				{
					string addr = this.addresses[j].ToString();
					if (perm.hasWildcard)
					{
						text = this.Intersect(addr, perm.hostname);
					}
					else if (perm.addresses != null)
					{
						for (int k = 0; k < perm.addresses.Length; k++)
						{
							text = this.Intersect(addr, perm.addresses[k].ToString());
							if (text != null)
							{
								break;
							}
						}
					}
				}
			}
			return text;
		}

		// Token: 0x06001CC5 RID: 7365 RVA: 0x00068244 File Offset: 0x00066444
		private string Intersect(string addr1, string addr2)
		{
			string[] array = addr1.Split(EndpointPermission.dot_char);
			string[] array2 = addr2.Split(EndpointPermission.dot_char);
			string[] array3 = new string[7];
			for (int i = 0; i < 4; i++)
			{
				int num = this.ToNumber(array[i]);
				if (num == -1)
				{
					return null;
				}
				int num2 = this.ToNumber(array2[i]);
				if (num2 == -1)
				{
					return null;
				}
				if (num == 256)
				{
					array3[i << 1] = ((num2 == 256) ? "*" : (string.Empty + num2));
				}
				else if (num2 == 256)
				{
					array3[i << 1] = ((num == 256) ? "*" : (string.Empty + num));
				}
				else
				{
					if (num != num2)
					{
						return null;
					}
					array3[i << 1] = string.Empty + num;
				}
			}
			array3[1] = (array3[3] = (array3[5] = "."));
			return string.Concat(array3);
		}

		// Token: 0x06001CC6 RID: 7366 RVA: 0x00068348 File Offset: 0x00066548
		private int ToNumber(string value)
		{
			if (value == "*")
			{
				return 256;
			}
			int length = value.Length;
			if (length < 1 || length > 3)
			{
				return -1;
			}
			int num = 0;
			for (int i = 0; i < length; i++)
			{
				char c = value[i];
				if ('0' > c || c > '9')
				{
					return -1;
				}
				num = checked(num * 10 + (int)(c - '0'));
			}
			if (num > 255)
			{
				return -1;
			}
			return num;
		}

		// Token: 0x06001CC7 RID: 7367 RVA: 0x000683B4 File Offset: 0x000665B4
		internal void Resolve()
		{
			if (this.resolved)
			{
				return;
			}
			bool flag = false;
			bool flag2 = false;
			this.addresses = null;
			string[] array = this.hostname.Split(EndpointPermission.dot_char);
			if (array.Length != 4)
			{
				flag = true;
			}
			else
			{
				for (int i = 0; i < 4; i++)
				{
					int num = this.ToNumber(array[i]);
					if (num == -1)
					{
						flag = true;
						break;
					}
					if (num == 256)
					{
						flag2 = true;
					}
				}
			}
			if (flag)
			{
				this.hasWildcard = false;
				try
				{
					this.addresses = Dns.GetHostAddresses(this.hostname);
					goto IL_A3;
				}
				catch (SocketException)
				{
					goto IL_A3;
				}
			}
			this.hasWildcard = flag2;
			if (!flag2)
			{
				this.addresses = new IPAddress[1];
				this.addresses[0] = IPAddress.Parse(this.hostname);
			}
			IL_A3:
			this.resolved = true;
		}

		// Token: 0x06001CC8 RID: 7368 RVA: 0x0006847C File Offset: 0x0006667C
		internal void UndoResolve()
		{
			this.resolved = false;
		}

		// Token: 0x06001CC9 RID: 7369 RVA: 0x00068485 File Offset: 0x00066685
		// Note: this type is marked as 'beforefieldinit'.
		static EndpointPermission()
		{
		}

		// Token: 0x06001CCA RID: 7370 RVA: 0x000092E2 File Offset: 0x000074E2
		internal EndpointPermission()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001993 RID: 6547
		private static char[] dot_char = new char[]
		{
			'.'
		};

		// Token: 0x04001994 RID: 6548
		private string hostname;

		// Token: 0x04001995 RID: 6549
		private int port;

		// Token: 0x04001996 RID: 6550
		private TransportType transport;

		// Token: 0x04001997 RID: 6551
		private bool resolved;

		// Token: 0x04001998 RID: 6552
		private bool hasWildcard;

		// Token: 0x04001999 RID: 6553
		private IPAddress[] addresses;
	}
}
