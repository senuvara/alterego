﻿using System;
using System.Globalization;

namespace System.Net
{
	// Token: 0x020003FA RID: 1018
	internal class MonoHttpDate
	{
		// Token: 0x06001F52 RID: 8018 RVA: 0x00071D3C File Offset: 0x0006FF3C
		internal static DateTime Parse(string dateStr)
		{
			return DateTime.ParseExact(dateStr, MonoHttpDate.formats, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces).ToLocalTime();
		}

		// Token: 0x06001F53 RID: 8019 RVA: 0x0000232F File Offset: 0x0000052F
		public MonoHttpDate()
		{
		}

		// Token: 0x06001F54 RID: 8020 RVA: 0x00071D64 File Offset: 0x0006FF64
		// Note: this type is marked as 'beforefieldinit'.
		static MonoHttpDate()
		{
		}

		// Token: 0x04001AD8 RID: 6872
		private static readonly string rfc1123_date = "r";

		// Token: 0x04001AD9 RID: 6873
		private static readonly string rfc850_date = "dddd, dd-MMM-yy HH:mm:ss G\\MT";

		// Token: 0x04001ADA RID: 6874
		private static readonly string asctime_date = "ddd MMM d HH:mm:ss yyyy";

		// Token: 0x04001ADB RID: 6875
		private static readonly string[] formats = new string[]
		{
			MonoHttpDate.rfc1123_date,
			MonoHttpDate.rfc850_date,
			MonoHttpDate.asctime_date
		};
	}
}
