﻿using System;

namespace System.Net
{
	// Token: 0x02000316 RID: 790
	internal enum HttpBehaviour : byte
	{
		// Token: 0x04001636 RID: 5686
		Unknown,
		// Token: 0x04001637 RID: 5687
		HTTP10,
		// Token: 0x04001638 RID: 5688
		HTTP11PartiallyCompliant,
		// Token: 0x04001639 RID: 5689
		HTTP11
	}
}
