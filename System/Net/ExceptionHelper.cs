﻿using System;

namespace System.Net
{
	// Token: 0x020002FF RID: 767
	internal static class ExceptionHelper
	{
		// Token: 0x170004A0 RID: 1184
		// (get) Token: 0x060016A3 RID: 5795 RVA: 0x000516A8 File Offset: 0x0004F8A8
		internal static NotImplementedException MethodNotImplementedException
		{
			get
			{
				return new NotImplementedException(SR.GetString("This method is not implemented by this class."));
			}
		}

		// Token: 0x170004A1 RID: 1185
		// (get) Token: 0x060016A4 RID: 5796 RVA: 0x000516B9 File Offset: 0x0004F8B9
		internal static NotImplementedException PropertyNotImplementedException
		{
			get
			{
				return new NotImplementedException(SR.GetString("This property is not implemented by this class."));
			}
		}

		// Token: 0x170004A2 RID: 1186
		// (get) Token: 0x060016A5 RID: 5797 RVA: 0x000516CA File Offset: 0x0004F8CA
		internal static NotSupportedException MethodNotSupportedException
		{
			get
			{
				return new NotSupportedException(SR.GetString("This method is not supported by this class."));
			}
		}

		// Token: 0x170004A3 RID: 1187
		// (get) Token: 0x060016A6 RID: 5798 RVA: 0x000516DB File Offset: 0x0004F8DB
		internal static NotSupportedException PropertyNotSupportedException
		{
			get
			{
				return new NotSupportedException(SR.GetString("This property is not supported by this class."));
			}
		}

		// Token: 0x170004A4 RID: 1188
		// (get) Token: 0x060016A7 RID: 5799 RVA: 0x000516EC File Offset: 0x0004F8EC
		internal static WebException IsolatedException
		{
			get
			{
				return new WebException(NetRes.GetWebStatusString("net_requestaborted", WebExceptionStatus.KeepAliveFailure), WebExceptionStatus.KeepAliveFailure, WebExceptionInternalStatus.Isolated, null);
			}
		}

		// Token: 0x170004A5 RID: 1189
		// (get) Token: 0x060016A8 RID: 5800 RVA: 0x00051703 File Offset: 0x0004F903
		internal static WebException RequestAbortedException
		{
			get
			{
				return new WebException(NetRes.GetWebStatusString("net_requestaborted", WebExceptionStatus.RequestCanceled), WebExceptionStatus.RequestCanceled);
			}
		}

		// Token: 0x170004A6 RID: 1190
		// (get) Token: 0x060016A9 RID: 5801 RVA: 0x00051716 File Offset: 0x0004F916
		internal static WebException CacheEntryNotFoundException
		{
			get
			{
				return new WebException(NetRes.GetWebStatusString("net_requestaborted", WebExceptionStatus.CacheEntryNotFound), WebExceptionStatus.CacheEntryNotFound);
			}
		}

		// Token: 0x170004A7 RID: 1191
		// (get) Token: 0x060016AA RID: 5802 RVA: 0x0005172B File Offset: 0x0004F92B
		internal static WebException RequestProhibitedByCachePolicyException
		{
			get
			{
				return new WebException(NetRes.GetWebStatusString("net_requestaborted", WebExceptionStatus.RequestProhibitedByCachePolicy), WebExceptionStatus.RequestProhibitedByCachePolicy);
			}
		}
	}
}
