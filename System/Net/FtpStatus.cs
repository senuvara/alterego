﻿using System;

namespace System.Net
{
	// Token: 0x020003D9 RID: 985
	internal class FtpStatus
	{
		// Token: 0x06001CFF RID: 7423 RVA: 0x00068AFC File Offset: 0x00066CFC
		public FtpStatus(FtpStatusCode statusCode, string statusDescription)
		{
			this.statusCode = statusCode;
			this.statusDescription = statusDescription;
		}

		// Token: 0x170005E1 RID: 1505
		// (get) Token: 0x06001D00 RID: 7424 RVA: 0x00068B12 File Offset: 0x00066D12
		public FtpStatusCode StatusCode
		{
			get
			{
				return this.statusCode;
			}
		}

		// Token: 0x170005E2 RID: 1506
		// (get) Token: 0x06001D01 RID: 7425 RVA: 0x00068B1A File Offset: 0x00066D1A
		public string StatusDescription
		{
			get
			{
				return this.statusDescription;
			}
		}

		// Token: 0x040019A8 RID: 6568
		private readonly FtpStatusCode statusCode;

		// Token: 0x040019A9 RID: 6569
		private readonly string statusDescription;
	}
}
