﻿using System;

namespace System.Net.Security
{
	/// <summary>The EncryptionPolicy to use. </summary>
	// Token: 0x0200048C RID: 1164
	public enum EncryptionPolicy
	{
		/// <summary>Require encryption and never allow a NULL cipher.</summary>
		// Token: 0x04001EF0 RID: 7920
		RequireEncryption,
		/// <summary>Prefer that full encryption be used, but allow a NULL cipher (no encryption) if the server agrees. </summary>
		// Token: 0x04001EF1 RID: 7921
		AllowNoEncryption,
		/// <summary>Allow no encryption and request that a NULL cipher be used if the other endpoint can handle a NULL cipher.</summary>
		// Token: 0x04001EF2 RID: 7922
		NoEncryption
	}
}
