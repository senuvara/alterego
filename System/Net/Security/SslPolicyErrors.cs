﻿using System;

namespace System.Net.Security
{
	/// <summary>Enumerates Secure Socket Layer (SSL) policy errors.</summary>
	// Token: 0x02000491 RID: 1169
	[Flags]
	public enum SslPolicyErrors
	{
		/// <summary>No SSL policy errors.</summary>
		// Token: 0x04001EFB RID: 7931
		None = 0,
		/// <summary>Certificate not available.</summary>
		// Token: 0x04001EFC RID: 7932
		RemoteCertificateNotAvailable = 1,
		/// <summary>Certificate name mismatch.</summary>
		// Token: 0x04001EFD RID: 7933
		RemoteCertificateNameMismatch = 2,
		/// <summary>
		///     <see cref="P:System.Security.Cryptography.X509Certificates.X509Chain.ChainStatus" /> has returned a non empty array.</summary>
		// Token: 0x04001EFE RID: 7934
		RemoteCertificateChainErrors = 4
	}
}
