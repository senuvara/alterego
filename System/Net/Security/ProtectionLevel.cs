﻿using System;

namespace System.Net.Security
{
	/// <summary>Indicates the security services requested for an authenticated stream.</summary>
	// Token: 0x0200048B RID: 1163
	public enum ProtectionLevel
	{
		/// <summary>Authentication only.</summary>
		// Token: 0x04001EEC RID: 7916
		None,
		/// <summary>Sign data to help ensure the integrity of transmitted data.</summary>
		// Token: 0x04001EED RID: 7917
		Sign,
		/// <summary>Encrypt and sign data to help ensure the confidentiality and integrity of transmitted data.</summary>
		// Token: 0x04001EEE RID: 7918
		EncryptAndSign
	}
}
