﻿using System;
using System.Collections;

namespace System.Net
{
	// Token: 0x02000371 RID: 881
	internal sealed class InterlockedStack
	{
		// Token: 0x0600193C RID: 6460 RVA: 0x0005AD1E File Offset: 0x00058F1E
		internal InterlockedStack()
		{
		}

		// Token: 0x0600193D RID: 6461 RVA: 0x0005AD34 File Offset: 0x00058F34
		internal void Push(object pooledStream)
		{
			if (pooledStream == null)
			{
				throw new ArgumentNullException("pooledStream");
			}
			object syncRoot = this._stack.SyncRoot;
			lock (syncRoot)
			{
				this._stack.Push(pooledStream);
			}
		}

		// Token: 0x0600193E RID: 6462 RVA: 0x0005AD90 File Offset: 0x00058F90
		internal object Pop()
		{
			object syncRoot = this._stack.SyncRoot;
			object result;
			lock (syncRoot)
			{
				object obj = null;
				if (0 < this._stack.Count)
				{
					obj = this._stack.Pop();
				}
				result = obj;
			}
			return result;
		}

		// Token: 0x040017F3 RID: 6131
		private readonly Stack _stack = new Stack();
	}
}
