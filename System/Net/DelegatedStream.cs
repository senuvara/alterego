﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net
{
	// Token: 0x0200038B RID: 907
	internal class DelegatedStream : Stream
	{
		// Token: 0x06001A40 RID: 6720 RVA: 0x0005FE80 File Offset: 0x0005E080
		protected DelegatedStream()
		{
		}

		// Token: 0x06001A41 RID: 6721 RVA: 0x0005FE88 File Offset: 0x0005E088
		protected DelegatedStream(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			this.stream = stream;
			this.netStream = (stream as NetworkStream);
		}

		// Token: 0x1700057B RID: 1403
		// (get) Token: 0x06001A42 RID: 6722 RVA: 0x0005FEB1 File Offset: 0x0005E0B1
		protected Stream BaseStream
		{
			get
			{
				return this.stream;
			}
		}

		// Token: 0x1700057C RID: 1404
		// (get) Token: 0x06001A43 RID: 6723 RVA: 0x0005FEB9 File Offset: 0x0005E0B9
		public override bool CanRead
		{
			get
			{
				return this.stream.CanRead;
			}
		}

		// Token: 0x1700057D RID: 1405
		// (get) Token: 0x06001A44 RID: 6724 RVA: 0x0005FEC6 File Offset: 0x0005E0C6
		public override bool CanSeek
		{
			get
			{
				return this.stream.CanSeek;
			}
		}

		// Token: 0x1700057E RID: 1406
		// (get) Token: 0x06001A45 RID: 6725 RVA: 0x0005FED3 File Offset: 0x0005E0D3
		public override bool CanWrite
		{
			get
			{
				return this.stream.CanWrite;
			}
		}

		// Token: 0x1700057F RID: 1407
		// (get) Token: 0x06001A46 RID: 6726 RVA: 0x0005FEE0 File Offset: 0x0005E0E0
		public override long Length
		{
			get
			{
				if (!this.CanSeek)
				{
					throw new NotSupportedException(SR.GetString("Seeking is not supported on this stream."));
				}
				return this.stream.Length;
			}
		}

		// Token: 0x17000580 RID: 1408
		// (get) Token: 0x06001A47 RID: 6727 RVA: 0x0005FF05 File Offset: 0x0005E105
		// (set) Token: 0x06001A48 RID: 6728 RVA: 0x0005FF2A File Offset: 0x0005E12A
		public override long Position
		{
			get
			{
				if (!this.CanSeek)
				{
					throw new NotSupportedException(SR.GetString("Seeking is not supported on this stream."));
				}
				return this.stream.Position;
			}
			set
			{
				if (!this.CanSeek)
				{
					throw new NotSupportedException(SR.GetString("Seeking is not supported on this stream."));
				}
				this.stream.Position = value;
			}
		}

		// Token: 0x06001A49 RID: 6729 RVA: 0x0005FF50 File Offset: 0x0005E150
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (!this.CanRead)
			{
				throw new NotSupportedException(SR.GetString("Reading is not supported on this stream."));
			}
			IAsyncResult result;
			if (this.netStream != null)
			{
				result = this.netStream.UnsafeBeginRead(buffer, offset, count, callback, state);
			}
			else
			{
				result = this.stream.BeginRead(buffer, offset, count, callback, state);
			}
			return result;
		}

		// Token: 0x06001A4A RID: 6730 RVA: 0x0005FFA8 File Offset: 0x0005E1A8
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (!this.CanWrite)
			{
				throw new NotSupportedException(SR.GetString("Writing is not supported on this stream."));
			}
			IAsyncResult result;
			if (this.netStream != null)
			{
				result = this.netStream.UnsafeBeginWrite(buffer, offset, count, callback, state);
			}
			else
			{
				result = this.stream.BeginWrite(buffer, offset, count, callback, state);
			}
			return result;
		}

		// Token: 0x06001A4B RID: 6731 RVA: 0x00060000 File Offset: 0x0005E200
		public override void Close()
		{
			this.stream.Close();
		}

		// Token: 0x06001A4C RID: 6732 RVA: 0x0006000D File Offset: 0x0005E20D
		public override int EndRead(IAsyncResult asyncResult)
		{
			if (!this.CanRead)
			{
				throw new NotSupportedException(SR.GetString("Reading is not supported on this stream."));
			}
			return this.stream.EndRead(asyncResult);
		}

		// Token: 0x06001A4D RID: 6733 RVA: 0x00060033 File Offset: 0x0005E233
		public override void EndWrite(IAsyncResult asyncResult)
		{
			if (!this.CanWrite)
			{
				throw new NotSupportedException(SR.GetString("Writing is not supported on this stream."));
			}
			this.stream.EndWrite(asyncResult);
		}

		// Token: 0x06001A4E RID: 6734 RVA: 0x00060059 File Offset: 0x0005E259
		public override void Flush()
		{
			this.stream.Flush();
		}

		// Token: 0x06001A4F RID: 6735 RVA: 0x00060066 File Offset: 0x0005E266
		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			return this.stream.FlushAsync(cancellationToken);
		}

		// Token: 0x06001A50 RID: 6736 RVA: 0x00060074 File Offset: 0x0005E274
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (!this.CanRead)
			{
				throw new NotSupportedException(SR.GetString("Reading is not supported on this stream."));
			}
			return this.stream.Read(buffer, offset, count);
		}

		// Token: 0x06001A51 RID: 6737 RVA: 0x0006009C File Offset: 0x0005E29C
		public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			if (!this.CanRead)
			{
				throw new NotSupportedException(SR.GetString("Reading is not supported on this stream."));
			}
			return this.stream.ReadAsync(buffer, offset, count, cancellationToken);
		}

		// Token: 0x06001A52 RID: 6738 RVA: 0x000600C6 File Offset: 0x0005E2C6
		public override long Seek(long offset, SeekOrigin origin)
		{
			if (!this.CanSeek)
			{
				throw new NotSupportedException(SR.GetString("Seeking is not supported on this stream."));
			}
			return this.stream.Seek(offset, origin);
		}

		// Token: 0x06001A53 RID: 6739 RVA: 0x000600ED File Offset: 0x0005E2ED
		public override void SetLength(long value)
		{
			if (!this.CanSeek)
			{
				throw new NotSupportedException(SR.GetString("Seeking is not supported on this stream."));
			}
			this.stream.SetLength(value);
		}

		// Token: 0x06001A54 RID: 6740 RVA: 0x00060113 File Offset: 0x0005E313
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (!this.CanWrite)
			{
				throw new NotSupportedException(SR.GetString("Writing is not supported on this stream."));
			}
			this.stream.Write(buffer, offset, count);
		}

		// Token: 0x06001A55 RID: 6741 RVA: 0x0006013B File Offset: 0x0005E33B
		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			if (!this.CanWrite)
			{
				throw new NotSupportedException(SR.GetString("Writing is not supported on this stream."));
			}
			return this.stream.WriteAsync(buffer, offset, count, cancellationToken);
		}

		// Token: 0x0400189A RID: 6298
		private Stream stream;

		// Token: 0x0400189B RID: 6299
		private NetworkStream netStream;
	}
}
