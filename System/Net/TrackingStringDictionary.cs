﻿using System;
using System.Collections.Specialized;

namespace System.Net
{
	// Token: 0x0200038C RID: 908
	internal class TrackingStringDictionary : StringDictionary
	{
		// Token: 0x06001A56 RID: 6742 RVA: 0x00060165 File Offset: 0x0005E365
		internal TrackingStringDictionary() : this(false)
		{
		}

		// Token: 0x06001A57 RID: 6743 RVA: 0x0006016E File Offset: 0x0005E36E
		internal TrackingStringDictionary(bool isReadOnly)
		{
			this.isReadOnly = isReadOnly;
		}

		// Token: 0x17000581 RID: 1409
		// (get) Token: 0x06001A58 RID: 6744 RVA: 0x0006017D File Offset: 0x0005E37D
		// (set) Token: 0x06001A59 RID: 6745 RVA: 0x00060185 File Offset: 0x0005E385
		internal bool IsChanged
		{
			get
			{
				return this.isChanged;
			}
			set
			{
				this.isChanged = value;
			}
		}

		// Token: 0x06001A5A RID: 6746 RVA: 0x0006018E File Offset: 0x0005E38E
		public override void Add(string key, string value)
		{
			if (this.isReadOnly)
			{
				throw new InvalidOperationException(SR.GetString("The collection is read-only."));
			}
			base.Add(key, value);
			this.isChanged = true;
		}

		// Token: 0x06001A5B RID: 6747 RVA: 0x000601B7 File Offset: 0x0005E3B7
		public override void Clear()
		{
			if (this.isReadOnly)
			{
				throw new InvalidOperationException(SR.GetString("The collection is read-only."));
			}
			base.Clear();
			this.isChanged = true;
		}

		// Token: 0x06001A5C RID: 6748 RVA: 0x000601DE File Offset: 0x0005E3DE
		public override void Remove(string key)
		{
			if (this.isReadOnly)
			{
				throw new InvalidOperationException(SR.GetString("The collection is read-only."));
			}
			base.Remove(key);
			this.isChanged = true;
		}

		// Token: 0x17000582 RID: 1410
		public override string this[string key]
		{
			get
			{
				return base[key];
			}
			set
			{
				if (this.isReadOnly)
				{
					throw new InvalidOperationException(SR.GetString("The collection is read-only."));
				}
				base[key] = value;
				this.isChanged = true;
			}
		}

		// Token: 0x0400189C RID: 6300
		private bool isChanged;

		// Token: 0x0400189D RID: 6301
		private bool isReadOnly;
	}
}
