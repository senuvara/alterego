﻿using System;
using System.Security.Authentication.ExtendedProtection;

namespace System.Net
{
	// Token: 0x02000324 RID: 804
	internal class CachedTransportContext : TransportContext
	{
		// Token: 0x060016F9 RID: 5881 RVA: 0x00052405 File Offset: 0x00050605
		internal CachedTransportContext(ChannelBinding binding)
		{
			this.binding = binding;
		}

		// Token: 0x060016FA RID: 5882 RVA: 0x00052414 File Offset: 0x00050614
		public override ChannelBinding GetChannelBinding(ChannelBindingKind kind)
		{
			if (kind != ChannelBindingKind.Endpoint)
			{
				return null;
			}
			return this.binding;
		}

		// Token: 0x04001670 RID: 5744
		private ChannelBinding binding;
	}
}
