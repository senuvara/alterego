﻿using System;

namespace System.Net
{
	// Token: 0x02000305 RID: 773
	internal enum CredentialUse
	{
		// Token: 0x04001586 RID: 5510
		Inbound = 1,
		// Token: 0x04001587 RID: 5511
		Outbound,
		// Token: 0x04001588 RID: 5512
		Both
	}
}
