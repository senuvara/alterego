﻿using System;

namespace System.Net
{
	// Token: 0x0200036B RID: 875
	[Serializable]
	internal sealed class EmptyWebProxy : IAutoWebProxy, IWebProxy
	{
		// Token: 0x06001910 RID: 6416 RVA: 0x0000232F File Offset: 0x0000052F
		public EmptyWebProxy()
		{
		}

		// Token: 0x06001911 RID: 6417 RVA: 0x0000206B File Offset: 0x0000026B
		public Uri GetProxy(Uri uri)
		{
			return uri;
		}

		// Token: 0x06001912 RID: 6418 RVA: 0x00003298 File Offset: 0x00001498
		public bool IsBypassed(Uri uri)
		{
			return true;
		}

		// Token: 0x1700052B RID: 1323
		// (get) Token: 0x06001913 RID: 6419 RVA: 0x0005A2A4 File Offset: 0x000584A4
		// (set) Token: 0x06001914 RID: 6420 RVA: 0x0005A2AC File Offset: 0x000584AC
		public ICredentials Credentials
		{
			get
			{
				return this.m_credentials;
			}
			set
			{
				this.m_credentials = value;
			}
		}

		// Token: 0x06001915 RID: 6421 RVA: 0x0005A2B5 File Offset: 0x000584B5
		ProxyChain IAutoWebProxy.GetProxies(Uri destination)
		{
			return new DirectProxy(destination);
		}

		// Token: 0x040017CF RID: 6095
		[NonSerialized]
		private ICredentials m_credentials;
	}
}
