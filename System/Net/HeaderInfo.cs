﻿using System;

namespace System.Net
{
	// Token: 0x0200034B RID: 843
	internal class HeaderInfo
	{
		// Token: 0x06001827 RID: 6183 RVA: 0x00057839 File Offset: 0x00055A39
		internal HeaderInfo(string name, bool requestRestricted, bool responseRestricted, bool multi, HeaderParser p)
		{
			this.HeaderName = name;
			this.IsRequestRestricted = requestRestricted;
			this.IsResponseRestricted = responseRestricted;
			this.Parser = p;
			this.AllowMultiValues = multi;
		}

		// Token: 0x04001758 RID: 5976
		internal readonly bool IsRequestRestricted;

		// Token: 0x04001759 RID: 5977
		internal readonly bool IsResponseRestricted;

		// Token: 0x0400175A RID: 5978
		internal readonly HeaderParser Parser;

		// Token: 0x0400175B RID: 5979
		internal readonly string HeaderName;

		// Token: 0x0400175C RID: 5980
		internal readonly bool AllowMultiValues;
	}
}
