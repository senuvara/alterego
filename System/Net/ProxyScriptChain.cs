﻿using System;

namespace System.Net
{
	// Token: 0x02000359 RID: 857
	internal class ProxyScriptChain : ProxyChain
	{
		// Token: 0x060018C0 RID: 6336 RVA: 0x00058E44 File Offset: 0x00057044
		internal ProxyScriptChain(WebProxy proxy, Uri destination) : base(destination)
		{
			this.m_Proxy = proxy;
		}

		// Token: 0x060018C1 RID: 6337 RVA: 0x00058E54 File Offset: 0x00057054
		protected override bool GetNextProxy(out Uri proxy)
		{
			if (this.m_CurrentIndex < 0)
			{
				proxy = null;
				return false;
			}
			if (this.m_CurrentIndex == 0)
			{
				this.m_ScriptProxies = this.m_Proxy.GetProxiesAuto(base.Destination, ref this.m_SyncStatus);
			}
			if (this.m_ScriptProxies == null || this.m_CurrentIndex >= this.m_ScriptProxies.Length)
			{
				proxy = this.m_Proxy.GetProxyAutoFailover(base.Destination);
				this.m_CurrentIndex = -1;
				return true;
			}
			Uri[] scriptProxies = this.m_ScriptProxies;
			int currentIndex = this.m_CurrentIndex;
			this.m_CurrentIndex = currentIndex + 1;
			proxy = scriptProxies[currentIndex];
			return true;
		}

		// Token: 0x060018C2 RID: 6338 RVA: 0x00058EE3 File Offset: 0x000570E3
		internal override void Abort()
		{
			this.m_Proxy.AbortGetProxiesAuto(ref this.m_SyncStatus);
		}

		// Token: 0x0400179A RID: 6042
		private WebProxy m_Proxy;

		// Token: 0x0400179B RID: 6043
		private Uri[] m_ScriptProxies;

		// Token: 0x0400179C RID: 6044
		private int m_CurrentIndex;

		// Token: 0x0400179D RID: 6045
		private int m_SyncStatus;
	}
}
