﻿using System;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;

namespace System.Net
{
	// Token: 0x0200036A RID: 874
	internal abstract class WebProxyDataBuilder
	{
		// Token: 0x06001904 RID: 6404 RVA: 0x00059E9E File Offset: 0x0005809E
		public WebProxyData Build()
		{
			this.m_Result = new WebProxyData();
			this.BuildInternal();
			return this.m_Result;
		}

		// Token: 0x06001905 RID: 6405
		protected abstract void BuildInternal();

		// Token: 0x06001906 RID: 6406 RVA: 0x00059EB8 File Offset: 0x000580B8
		protected void SetProxyAndBypassList(string addressString, string bypassListString)
		{
			if (addressString != null)
			{
				addressString = addressString.Trim();
				if (addressString != string.Empty)
				{
					if (addressString.IndexOf('=') == -1)
					{
						this.m_Result.proxyAddress = WebProxyDataBuilder.ParseProxyUri(addressString);
					}
					else
					{
						this.m_Result.proxyHostAddresses = WebProxyDataBuilder.ParseProtocolProxies(addressString);
					}
					if (bypassListString != null)
					{
						bypassListString = bypassListString.Trim();
						if (bypassListString != string.Empty)
						{
							bool bypassOnLocal = false;
							this.m_Result.bypassList = WebProxyDataBuilder.ParseBypassList(bypassListString, out bypassOnLocal);
							this.m_Result.bypassOnLocal = bypassOnLocal;
						}
					}
				}
			}
		}

		// Token: 0x06001907 RID: 6407 RVA: 0x00059F48 File Offset: 0x00058148
		protected void SetAutoProxyUrl(string autoConfigUrl)
		{
			if (!string.IsNullOrEmpty(autoConfigUrl))
			{
				Uri scriptLocation = null;
				if (Uri.TryCreate(autoConfigUrl, UriKind.Absolute, out scriptLocation))
				{
					this.m_Result.scriptLocation = scriptLocation;
				}
			}
		}

		// Token: 0x06001908 RID: 6408 RVA: 0x00059F76 File Offset: 0x00058176
		protected void SetAutoDetectSettings(bool value)
		{
			this.m_Result.automaticallyDetectSettings = value;
		}

		// Token: 0x06001909 RID: 6409 RVA: 0x00059F84 File Offset: 0x00058184
		private static Uri ParseProxyUri(string proxyString)
		{
			if (proxyString.IndexOf("://") == -1)
			{
				proxyString = "http://" + proxyString;
			}
			Uri result;
			try
			{
				result = new Uri(proxyString);
			}
			catch (UriFormatException)
			{
				bool on = Logging.On;
				throw WebProxyDataBuilder.CreateInvalidProxyStringException(proxyString);
			}
			return result;
		}

		// Token: 0x0600190A RID: 6410 RVA: 0x00059FD8 File Offset: 0x000581D8
		private static Hashtable ParseProtocolProxies(string proxyListString)
		{
			string[] array = proxyListString.Split(new char[]
			{
				';'
			});
			Hashtable hashtable = new Hashtable(CaseInsensitiveAscii.StaticInstance);
			for (int i = 0; i < array.Length; i++)
			{
				string text = array[i].Trim();
				if (!(text == string.Empty))
				{
					string[] array2 = text.Split(new char[]
					{
						'='
					});
					if (array2.Length != 2)
					{
						throw WebProxyDataBuilder.CreateInvalidProxyStringException(proxyListString);
					}
					array2[0] = array2[0].Trim();
					array2[1] = array2[1].Trim();
					if (array2[0] == string.Empty || array2[1] == string.Empty)
					{
						throw WebProxyDataBuilder.CreateInvalidProxyStringException(proxyListString);
					}
					hashtable[array2[0]] = WebProxyDataBuilder.ParseProxyUri(array2[1]);
				}
			}
			return hashtable;
		}

		// Token: 0x0600190B RID: 6411 RVA: 0x0005A0A2 File Offset: 0x000582A2
		private static FormatException CreateInvalidProxyStringException(string originalProxyString)
		{
			string @string = SR.GetString("The system proxy settings contain an invalid proxy server setting: '{0}'.", new object[]
			{
				originalProxyString
			});
			bool on = Logging.On;
			return new FormatException(@string);
		}

		// Token: 0x0600190C RID: 6412 RVA: 0x0005A0C4 File Offset: 0x000582C4
		private static string BypassStringEscape(string rawString)
		{
			Match match = new Regex("^(?<scheme>.*://)?(?<host>[^:]*)(?<port>:[0-9]{1,5})?$", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant).Match(rawString);
			string text;
			string text2;
			string text3;
			if (match.Success)
			{
				text = match.Groups["scheme"].Value;
				text2 = match.Groups["host"].Value;
				text3 = match.Groups["port"].Value;
			}
			else
			{
				text = string.Empty;
				text2 = rawString;
				text3 = string.Empty;
			}
			text = WebProxyDataBuilder.ConvertRegexReservedChars(text);
			text2 = WebProxyDataBuilder.ConvertRegexReservedChars(text2);
			text3 = WebProxyDataBuilder.ConvertRegexReservedChars(text3);
			if (text == string.Empty)
			{
				text = "(?:.*://)?";
			}
			if (text3 == string.Empty)
			{
				text3 = "(?::[0-9]{1,5})?";
			}
			return string.Concat(new string[]
			{
				"^",
				text,
				text2,
				text3,
				"$"
			});
		}

		// Token: 0x0600190D RID: 6413 RVA: 0x0005A1A4 File Offset: 0x000583A4
		private static string ConvertRegexReservedChars(string rawString)
		{
			if (rawString.Length == 0)
			{
				return rawString;
			}
			StringBuilder stringBuilder = new StringBuilder();
			foreach (char c in rawString)
			{
				if ("#$()+.?[\\^{|".IndexOf(c) != -1)
				{
					stringBuilder.Append('\\');
				}
				else if (c == '*')
				{
					stringBuilder.Append('.');
				}
				stringBuilder.Append(c);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0600190E RID: 6414 RVA: 0x0005A214 File Offset: 0x00058414
		private static ArrayList ParseBypassList(string bypassListString, out bool bypassOnLocal)
		{
			string[] array = bypassListString.Split(new char[]
			{
				';'
			});
			bypassOnLocal = false;
			if (array.Length == 0)
			{
				return null;
			}
			ArrayList arrayList = null;
			foreach (string text in array)
			{
				if (text != null)
				{
					string text2 = text.Trim();
					if (text2.Length > 0)
					{
						if (string.Compare(text2, "<local>", StringComparison.OrdinalIgnoreCase) == 0)
						{
							bypassOnLocal = true;
						}
						else
						{
							text2 = WebProxyDataBuilder.BypassStringEscape(text2);
							if (arrayList == null)
							{
								arrayList = new ArrayList();
							}
							if (!arrayList.Contains(text2))
							{
								arrayList.Add(text2);
							}
						}
					}
				}
			}
			return arrayList;
		}

		// Token: 0x0600190F RID: 6415 RVA: 0x0000232F File Offset: 0x0000052F
		protected WebProxyDataBuilder()
		{
		}

		// Token: 0x040017CA RID: 6090
		private const char addressListDelimiter = ';';

		// Token: 0x040017CB RID: 6091
		private const char addressListSchemeValueDelimiter = '=';

		// Token: 0x040017CC RID: 6092
		private const char bypassListDelimiter = ';';

		// Token: 0x040017CD RID: 6093
		private WebProxyData m_Result;

		// Token: 0x040017CE RID: 6094
		private const string regexReserved = "#$()+.?[\\^{|";
	}
}
