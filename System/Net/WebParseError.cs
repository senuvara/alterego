﻿using System;

namespace System.Net
{
	// Token: 0x02000348 RID: 840
	internal struct WebParseError
	{
		// Token: 0x0400172D RID: 5933
		public WebParseErrorSection Section;

		// Token: 0x0400172E RID: 5934
		public WebParseErrorCode Code;
	}
}
