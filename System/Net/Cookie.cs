﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace System.Net
{
	/// <summary>Provides a set of properties and methods that are used to manage cookies. This class cannot be inherited.</summary>
	// Token: 0x02000373 RID: 883
	[Serializable]
	public sealed class Cookie
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Cookie" /> class.</summary>
		// Token: 0x0600193F RID: 6463 RVA: 0x0005ADF0 File Offset: 0x00058FF0
		public Cookie()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Cookie" /> class with a specified <see cref="P:System.Net.Cookie.Name" /> and <see cref="P:System.Net.Cookie.Value" />.</summary>
		/// <param name="name">The name of a <see cref="T:System.Net.Cookie" />. The following characters must not be used inside <paramref name="name" />: equal sign, semicolon, comma, newline (\n), return (\r), tab (\t), and space character. The dollar sign character ("$") cannot be the first character. </param>
		/// <param name="value">The value of a <see cref="T:System.Net.Cookie" />. The following characters must not be used inside <paramref name="value" />: semicolon, comma. </param>
		/// <exception cref="T:System.Net.CookieException">The <paramref name="name" /> parameter is <see langword="null" />. -or- The <paramref name="name" /> parameter is of zero length. -or- The <paramref name="name" /> parameter contains an invalid character.-or- The <paramref name="value" /> parameter is <see langword="null" /> .-or - The <paramref name="value" /> parameter contains a string not enclosed in quotes that contains an invalid character. </exception>
		// Token: 0x06001940 RID: 6464 RVA: 0x0005AE84 File Offset: 0x00059084
		public Cookie(string name, string value)
		{
			this.Name = name;
			this.m_value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Cookie" /> class with a specified <see cref="P:System.Net.Cookie.Name" />, <see cref="P:System.Net.Cookie.Value" />, and <see cref="P:System.Net.Cookie.Path" />.</summary>
		/// <param name="name">The name of a <see cref="T:System.Net.Cookie" />. The following characters must not be used inside <paramref name="name" />: equal sign, semicolon, comma, newline (\n), return (\r), tab (\t), and space character. The dollar sign character ("$") cannot be the first character. </param>
		/// <param name="value">The value of a <see cref="T:System.Net.Cookie" />. The following characters must not be used inside <paramref name="value" />: semicolon, comma. </param>
		/// <param name="path">The subset of URIs on the origin server to which this <see cref="T:System.Net.Cookie" /> applies. The default value is "/". </param>
		/// <exception cref="T:System.Net.CookieException">The <paramref name="name" /> parameter is <see langword="null" />. -or- The <paramref name="name" /> parameter is of zero length. -or- The <paramref name="name" /> parameter contains an invalid character.-or- The <paramref name="value" /> parameter is <see langword="null" /> .-or - The <paramref name="value" /> parameter contains a string not enclosed in quotes that contains an invalid character.</exception>
		// Token: 0x06001941 RID: 6465 RVA: 0x0005AF24 File Offset: 0x00059124
		public Cookie(string name, string value, string path) : this(name, value)
		{
			this.Path = path;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Cookie" /> class with a specified <see cref="P:System.Net.Cookie.Name" />, <see cref="P:System.Net.Cookie.Value" />, <see cref="P:System.Net.Cookie.Path" />, and <see cref="P:System.Net.Cookie.Domain" />.</summary>
		/// <param name="name">The name of a <see cref="T:System.Net.Cookie" />. The following characters must not be used inside <paramref name="name" />: equal sign, semicolon, comma, newline (\n), return (\r), tab (\t), and space character. The dollar sign character ("$") cannot be the first character. </param>
		/// <param name="value">The value of a <see cref="T:System.Net.Cookie" /> object. The following characters must not be used inside <paramref name="value" />: semicolon, comma. </param>
		/// <param name="path">The subset of URIs on the origin server to which this <see cref="T:System.Net.Cookie" /> applies. The default value is "/". </param>
		/// <param name="domain">The optional internet domain for which this <see cref="T:System.Net.Cookie" /> is valid. The default value is the host this <see cref="T:System.Net.Cookie" /> has been received from. </param>
		/// <exception cref="T:System.Net.CookieException">The <paramref name="name" /> parameter is <see langword="null" />. -or- The <paramref name="name" /> parameter is of zero length. -or- The <paramref name="name" /> parameter contains an invalid character.-or- The <paramref name="value" /> parameter is <see langword="null" /> .-or - The <paramref name="value" /> parameter contains a string not enclosed in quotes that contains an invalid character.</exception>
		// Token: 0x06001942 RID: 6466 RVA: 0x0005AF35 File Offset: 0x00059135
		public Cookie(string name, string value, string path, string domain) : this(name, value, path)
		{
			this.Domain = domain;
		}

		/// <summary>Gets or sets a comment that the server can add to a <see cref="T:System.Net.Cookie" />.</summary>
		/// <returns>An optional comment to document intended usage for this <see cref="T:System.Net.Cookie" />.</returns>
		// Token: 0x17000534 RID: 1332
		// (get) Token: 0x06001943 RID: 6467 RVA: 0x0005AF48 File Offset: 0x00059148
		// (set) Token: 0x06001944 RID: 6468 RVA: 0x0005AF50 File Offset: 0x00059150
		public string Comment
		{
			get
			{
				return this.m_comment;
			}
			set
			{
				if (value == null)
				{
					value = string.Empty;
				}
				this.m_comment = value;
			}
		}

		/// <summary>Gets or sets a URI comment that the server can provide with a <see cref="T:System.Net.Cookie" />.</summary>
		/// <returns>An optional comment that represents the intended usage of the URI reference for this <see cref="T:System.Net.Cookie" />. The value must conform to URI format.</returns>
		// Token: 0x17000535 RID: 1333
		// (get) Token: 0x06001945 RID: 6469 RVA: 0x0005AF63 File Offset: 0x00059163
		// (set) Token: 0x06001946 RID: 6470 RVA: 0x0005AF6B File Offset: 0x0005916B
		public Uri CommentUri
		{
			get
			{
				return this.m_commentUri;
			}
			set
			{
				this.m_commentUri = value;
			}
		}

		/// <summary>Determines whether a page script or other active content can access this cookie.</summary>
		/// <returns>Boolean value that determines whether a page script or other active content can access this cookie.</returns>
		// Token: 0x17000536 RID: 1334
		// (get) Token: 0x06001947 RID: 6471 RVA: 0x0005AF74 File Offset: 0x00059174
		// (set) Token: 0x06001948 RID: 6472 RVA: 0x0005AF7C File Offset: 0x0005917C
		public bool HttpOnly
		{
			get
			{
				return this.m_httpOnly;
			}
			set
			{
				this.m_httpOnly = value;
			}
		}

		/// <summary>Gets or sets the discard flag set by the server.</summary>
		/// <returns>
		///     <see langword="true" /> if the client is to discard the <see cref="T:System.Net.Cookie" /> at the end of the current session; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000537 RID: 1335
		// (get) Token: 0x06001949 RID: 6473 RVA: 0x0005AF85 File Offset: 0x00059185
		// (set) Token: 0x0600194A RID: 6474 RVA: 0x0005AF8D File Offset: 0x0005918D
		public bool Discard
		{
			get
			{
				return this.m_discard;
			}
			set
			{
				this.m_discard = value;
			}
		}

		/// <summary>Gets or sets the URI for which the <see cref="T:System.Net.Cookie" /> is valid.</summary>
		/// <returns>The URI for which the <see cref="T:System.Net.Cookie" /> is valid.</returns>
		// Token: 0x17000538 RID: 1336
		// (get) Token: 0x0600194B RID: 6475 RVA: 0x0005AF96 File Offset: 0x00059196
		// (set) Token: 0x0600194C RID: 6476 RVA: 0x0005AF9E File Offset: 0x0005919E
		public string Domain
		{
			get
			{
				return this.m_domain;
			}
			set
			{
				this.m_domain = ((value == null) ? string.Empty : value);
				this.m_domain_implicit = false;
				this.m_domainKey = string.Empty;
			}
		}

		// Token: 0x17000539 RID: 1337
		// (get) Token: 0x0600194D RID: 6477 RVA: 0x0005AFC4 File Offset: 0x000591C4
		private string _Domain
		{
			get
			{
				if (!this.Plain && !this.m_domain_implicit && this.m_domain.Length != 0)
				{
					return "$Domain=" + (this.IsQuotedDomain ? "\"" : string.Empty) + this.m_domain + (this.IsQuotedDomain ? "\"" : string.Empty);
				}
				return string.Empty;
			}
		}

		// Token: 0x1700053A RID: 1338
		// (get) Token: 0x0600194E RID: 6478 RVA: 0x0005B02C File Offset: 0x0005922C
		// (set) Token: 0x0600194F RID: 6479 RVA: 0x0005B034 File Offset: 0x00059234
		internal bool DomainImplicit
		{
			get
			{
				return this.m_domain_implicit;
			}
			set
			{
				this.m_domain_implicit = value;
			}
		}

		/// <summary>Gets or sets the current state of the <see cref="T:System.Net.Cookie" />.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Net.Cookie" /> has expired; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700053B RID: 1339
		// (get) Token: 0x06001950 RID: 6480 RVA: 0x0005B03D File Offset: 0x0005923D
		// (set) Token: 0x06001951 RID: 6481 RVA: 0x0005B068 File Offset: 0x00059268
		public bool Expired
		{
			get
			{
				return this.m_expires != DateTime.MinValue && this.m_expires.ToLocalTime() <= DateTime.Now;
			}
			set
			{
				if (value)
				{
					this.m_expires = DateTime.Now;
				}
			}
		}

		/// <summary>Gets or sets the expiration date and time for the <see cref="T:System.Net.Cookie" /> as a <see cref="T:System.DateTime" />.</summary>
		/// <returns>The expiration date and time for the <see cref="T:System.Net.Cookie" /> as a <see cref="T:System.DateTime" /> instance.</returns>
		// Token: 0x1700053C RID: 1340
		// (get) Token: 0x06001952 RID: 6482 RVA: 0x0005B078 File Offset: 0x00059278
		// (set) Token: 0x06001953 RID: 6483 RVA: 0x0005B080 File Offset: 0x00059280
		public DateTime Expires
		{
			get
			{
				return this.m_expires;
			}
			set
			{
				this.m_expires = value;
			}
		}

		/// <summary>Gets or sets the name for the <see cref="T:System.Net.Cookie" />.</summary>
		/// <returns>The name for the <see cref="T:System.Net.Cookie" />.</returns>
		/// <exception cref="T:System.Net.CookieException">The value specified for a set operation is <see langword="null" /> or the empty string- or -The value specified for a set operation contained an illegal character. The following characters must not be used inside the <see cref="P:System.Net.Cookie.Name" /> property: equal sign, semicolon, comma, newline (\n), return (\r), tab (\t), and space character. The dollar sign character ("$") cannot be the first character.</exception>
		// Token: 0x1700053D RID: 1341
		// (get) Token: 0x06001954 RID: 6484 RVA: 0x0005B089 File Offset: 0x00059289
		// (set) Token: 0x06001955 RID: 6485 RVA: 0x0005B091 File Offset: 0x00059291
		public string Name
		{
			get
			{
				return this.m_name;
			}
			set
			{
				if (ValidationHelper.IsBlankString(value) || !this.InternalSetName(value))
				{
					throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
					{
						"Name",
						(value == null) ? "<null>" : value
					}));
				}
			}
		}

		// Token: 0x06001956 RID: 6486 RVA: 0x0005B0D0 File Offset: 0x000592D0
		internal bool InternalSetName(string value)
		{
			if (ValidationHelper.IsBlankString(value) || value[0] == '$' || value.IndexOfAny(Cookie.Reserved2Name) != -1)
			{
				this.m_name = string.Empty;
				return false;
			}
			this.m_name = value;
			return true;
		}

		/// <summary>Gets or sets the URIs to which the <see cref="T:System.Net.Cookie" /> applies.</summary>
		/// <returns>The URIs to which the <see cref="T:System.Net.Cookie" /> applies.</returns>
		// Token: 0x1700053E RID: 1342
		// (get) Token: 0x06001957 RID: 6487 RVA: 0x0005B108 File Offset: 0x00059308
		// (set) Token: 0x06001958 RID: 6488 RVA: 0x0005B110 File Offset: 0x00059310
		public string Path
		{
			get
			{
				return this.m_path;
			}
			set
			{
				this.m_path = ((value == null) ? string.Empty : value);
				this.m_path_implicit = false;
			}
		}

		// Token: 0x1700053F RID: 1343
		// (get) Token: 0x06001959 RID: 6489 RVA: 0x0005B12A File Offset: 0x0005932A
		private string _Path
		{
			get
			{
				if (!this.Plain && !this.m_path_implicit && this.m_path.Length != 0)
				{
					return "$Path=" + this.m_path;
				}
				return string.Empty;
			}
		}

		// Token: 0x17000540 RID: 1344
		// (get) Token: 0x0600195A RID: 6490 RVA: 0x0005B15F File Offset: 0x0005935F
		internal bool Plain
		{
			get
			{
				return this.Variant == CookieVariant.Plain;
			}
		}

		// Token: 0x0600195B RID: 6491 RVA: 0x0005B16C File Offset: 0x0005936C
		internal Cookie Clone()
		{
			Cookie cookie = new Cookie(this.m_name, this.m_value);
			if (!this.m_port_implicit)
			{
				cookie.Port = this.m_port;
			}
			if (!this.m_path_implicit)
			{
				cookie.Path = this.m_path;
			}
			cookie.Domain = this.m_domain;
			cookie.DomainImplicit = this.m_domain_implicit;
			cookie.m_timeStamp = this.m_timeStamp;
			cookie.Comment = this.m_comment;
			cookie.CommentUri = this.m_commentUri;
			cookie.HttpOnly = this.m_httpOnly;
			cookie.Discard = this.m_discard;
			cookie.Expires = this.m_expires;
			cookie.Version = this.m_version;
			cookie.Secure = this.m_secure;
			cookie.m_cookieVariant = this.m_cookieVariant;
			return cookie;
		}

		// Token: 0x0600195C RID: 6492 RVA: 0x0005B238 File Offset: 0x00059438
		private static bool IsDomainEqualToHost(string domain, string host)
		{
			return host.Length + 1 == domain.Length && string.Compare(host, 0, domain, 1, host.Length, StringComparison.OrdinalIgnoreCase) == 0;
		}

		// Token: 0x0600195D RID: 6493 RVA: 0x0005B260 File Offset: 0x00059460
		internal bool VerifySetDefaults(CookieVariant variant, Uri uri, bool isLocalDomain, string localDomain, bool set_default, bool isThrow)
		{
			string host = uri.Host;
			int port = uri.Port;
			string absolutePath = uri.AbsolutePath;
			bool flag = true;
			if (set_default)
			{
				if (this.Version == 0)
				{
					variant = CookieVariant.Plain;
				}
				else if (this.Version == 1 && variant == CookieVariant.Unknown)
				{
					variant = CookieVariant.Rfc2109;
				}
				this.m_cookieVariant = variant;
			}
			if (this.m_name == null || this.m_name.Length == 0 || this.m_name[0] == '$' || this.m_name.IndexOfAny(Cookie.Reserved2Name) != -1)
			{
				if (isThrow)
				{
					throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
					{
						"Name",
						(this.m_name == null) ? "<null>" : this.m_name
					}));
				}
				return false;
			}
			else if (this.m_value == null || ((this.m_value.Length <= 2 || this.m_value[0] != '"' || this.m_value[this.m_value.Length - 1] != '"') && this.m_value.IndexOfAny(Cookie.Reserved2Value) != -1))
			{
				if (isThrow)
				{
					throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
					{
						"Value",
						(this.m_value == null) ? "<null>" : this.m_value
					}));
				}
				return false;
			}
			else if (this.Comment != null && (this.Comment.Length <= 2 || this.Comment[0] != '"' || this.Comment[this.Comment.Length - 1] != '"') && this.Comment.IndexOfAny(Cookie.Reserved2Value) != -1)
			{
				if (isThrow)
				{
					throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
					{
						"Comment",
						this.Comment
					}));
				}
				return false;
			}
			else
			{
				if (this.Path == null || (this.Path.Length > 2 && this.Path[0] == '"' && this.Path[this.Path.Length - 1] == '"') || this.Path.IndexOfAny(Cookie.Reserved2Value) == -1)
				{
					if (set_default && this.m_domain_implicit)
					{
						this.m_domain = host;
					}
					else
					{
						if (!this.m_domain_implicit)
						{
							string text = this.m_domain;
							if (!Cookie.DomainCharsTest(text))
							{
								if (isThrow)
								{
									throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
									{
										"Domain",
										(text == null) ? "<null>" : text
									}));
								}
								return false;
							}
							else
							{
								if (text[0] != '.')
								{
									if (variant != CookieVariant.Rfc2965 && variant != CookieVariant.Plain)
									{
										if (isThrow)
										{
											throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
											{
												"Domain",
												this.m_domain
											}));
										}
										return false;
									}
									else
									{
										text = "." + text;
									}
								}
								int num = host.IndexOf('.');
								if (isLocalDomain && string.Compare(localDomain, text, StringComparison.OrdinalIgnoreCase) == 0)
								{
									flag = true;
								}
								else if (text.IndexOf('.', 1, text.Length - 2) == -1)
								{
									if (!Cookie.IsDomainEqualToHost(text, host))
									{
										flag = false;
									}
								}
								else if (variant == CookieVariant.Plain)
								{
									if (!Cookie.IsDomainEqualToHost(text, host) && (host.Length <= text.Length || string.Compare(host, host.Length - text.Length, text, 0, text.Length, StringComparison.OrdinalIgnoreCase) != 0))
									{
										flag = false;
									}
								}
								else if ((num == -1 || text.Length != host.Length - num || string.Compare(host, num, text, 0, text.Length, StringComparison.OrdinalIgnoreCase) != 0) && !Cookie.IsDomainEqualToHost(text, host))
								{
									flag = false;
								}
								if (flag)
								{
									this.m_domainKey = text.ToLower(CultureInfo.InvariantCulture);
								}
							}
						}
						else if (string.Compare(host, this.m_domain, StringComparison.OrdinalIgnoreCase) != 0)
						{
							flag = false;
						}
						if (!flag)
						{
							if (isThrow)
							{
								throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
								{
									"Domain",
									this.m_domain
								}));
							}
							return false;
						}
					}
					if (set_default && this.m_path_implicit)
					{
						switch (this.m_cookieVariant)
						{
						case CookieVariant.Plain:
							this.m_path = absolutePath;
							goto IL_4B8;
						case CookieVariant.Rfc2109:
							this.m_path = absolutePath.Substring(0, absolutePath.LastIndexOf('/'));
							goto IL_4B8;
						}
						this.m_path = absolutePath.Substring(0, absolutePath.LastIndexOf('/') + 1);
					}
					else if (!absolutePath.StartsWith(CookieParser.CheckQuoted(this.m_path)))
					{
						if (isThrow)
						{
							throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
							{
								"Path",
								this.m_path
							}));
						}
						return false;
					}
					IL_4B8:
					if (set_default && !this.m_port_implicit && this.m_port.Length == 0)
					{
						this.m_port_list = new int[]
						{
							port
						};
					}
					if (!this.m_port_implicit)
					{
						flag = false;
						int[] port_list = this.m_port_list;
						for (int i = 0; i < port_list.Length; i++)
						{
							if (port_list[i] == port)
							{
								flag = true;
								break;
							}
						}
						if (!flag)
						{
							if (isThrow)
							{
								throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
								{
									"Port",
									this.m_port
								}));
							}
							return false;
						}
					}
					return true;
				}
				if (isThrow)
				{
					throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
					{
						"Path",
						this.Path
					}));
				}
				return false;
			}
		}

		// Token: 0x0600195E RID: 6494 RVA: 0x0005B7B0 File Offset: 0x000599B0
		private static bool DomainCharsTest(string name)
		{
			if (name == null || name.Length == 0)
			{
				return false;
			}
			foreach (char c in name)
			{
				if ((c < '0' || c > '9') && c != '.' && c != '-' && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && c != '_')
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>Gets or sets a list of TCP ports that the <see cref="T:System.Net.Cookie" /> applies to.</summary>
		/// <returns>The list of TCP ports that the <see cref="T:System.Net.Cookie" /> applies to.</returns>
		/// <exception cref="T:System.Net.CookieException">The value specified for a set operation could not be parsed or is not enclosed in double quotes. </exception>
		// Token: 0x17000541 RID: 1345
		// (get) Token: 0x0600195F RID: 6495 RVA: 0x0005B813 File Offset: 0x00059A13
		// (set) Token: 0x06001960 RID: 6496 RVA: 0x0005B81C File Offset: 0x00059A1C
		public string Port
		{
			get
			{
				return this.m_port;
			}
			set
			{
				this.m_port_implicit = false;
				if (value == null || value.Length == 0)
				{
					this.m_port = string.Empty;
					return;
				}
				if (value[0] != '"' || value[value.Length - 1] != '"')
				{
					throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
					{
						"Port",
						value
					}));
				}
				string[] array = value.Split(Cookie.PortSplitDelimiters);
				List<int> list = new List<int>();
				for (int i = 0; i < array.Length; i++)
				{
					if (array[i] != string.Empty)
					{
						int num;
						if (!int.TryParse(array[i], out num))
						{
							throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
							{
								"Port",
								value
							}));
						}
						if (num < 0 || num > 65535)
						{
							throw new CookieException(SR.GetString("The '{0}'='{1}' part of the cookie is invalid.", new object[]
							{
								"Port",
								value
							}));
						}
						list.Add(num);
					}
				}
				this.m_port_list = list.ToArray();
				this.m_port = value;
				this.m_version = 1;
				this.m_cookieVariant = CookieVariant.Rfc2965;
			}
		}

		// Token: 0x17000542 RID: 1346
		// (get) Token: 0x06001961 RID: 6497 RVA: 0x0005B939 File Offset: 0x00059B39
		internal int[] PortList
		{
			get
			{
				return this.m_port_list;
			}
		}

		// Token: 0x17000543 RID: 1347
		// (get) Token: 0x06001962 RID: 6498 RVA: 0x0005B941 File Offset: 0x00059B41
		private string _Port
		{
			get
			{
				if (!this.m_port_implicit)
				{
					return "$Port" + ((this.m_port.Length == 0) ? string.Empty : ("=" + this.m_port));
				}
				return string.Empty;
			}
		}

		/// <summary>Gets or sets the security level of a <see cref="T:System.Net.Cookie" />.</summary>
		/// <returns>
		///     <see langword="true" /> if the client is only to return the cookie in subsequent requests if those requests use Secure Hypertext Transfer Protocol (HTTPS); otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000544 RID: 1348
		// (get) Token: 0x06001963 RID: 6499 RVA: 0x0005B97F File Offset: 0x00059B7F
		// (set) Token: 0x06001964 RID: 6500 RVA: 0x0005B987 File Offset: 0x00059B87
		public bool Secure
		{
			get
			{
				return this.m_secure;
			}
			set
			{
				this.m_secure = value;
			}
		}

		/// <summary>Gets the time when the cookie was issued as a <see cref="T:System.DateTime" />.</summary>
		/// <returns>The time when the cookie was issued as a <see cref="T:System.DateTime" />.</returns>
		// Token: 0x17000545 RID: 1349
		// (get) Token: 0x06001965 RID: 6501 RVA: 0x0005B990 File Offset: 0x00059B90
		public DateTime TimeStamp
		{
			get
			{
				return this.m_timeStamp;
			}
		}

		/// <summary>Gets or sets the <see cref="P:System.Net.Cookie.Value" /> for the <see cref="T:System.Net.Cookie" />.</summary>
		/// <returns>The <see cref="P:System.Net.Cookie.Value" /> for the <see cref="T:System.Net.Cookie" />.</returns>
		// Token: 0x17000546 RID: 1350
		// (get) Token: 0x06001966 RID: 6502 RVA: 0x0005B998 File Offset: 0x00059B98
		// (set) Token: 0x06001967 RID: 6503 RVA: 0x0005B9A0 File Offset: 0x00059BA0
		public string Value
		{
			get
			{
				return this.m_value;
			}
			set
			{
				this.m_value = ((value == null) ? string.Empty : value);
			}
		}

		// Token: 0x17000547 RID: 1351
		// (get) Token: 0x06001968 RID: 6504 RVA: 0x0005B9B3 File Offset: 0x00059BB3
		// (set) Token: 0x06001969 RID: 6505 RVA: 0x0005B9BB File Offset: 0x00059BBB
		internal CookieVariant Variant
		{
			get
			{
				return this.m_cookieVariant;
			}
			set
			{
				this.m_cookieVariant = value;
			}
		}

		// Token: 0x17000548 RID: 1352
		// (get) Token: 0x0600196A RID: 6506 RVA: 0x0005B9C4 File Offset: 0x00059BC4
		internal string DomainKey
		{
			get
			{
				if (!this.m_domain_implicit)
				{
					return this.m_domainKey;
				}
				return this.Domain;
			}
		}

		/// <summary>Gets or sets the version of HTTP state maintenance to which the cookie conforms.</summary>
		/// <returns>The version of HTTP state maintenance to which the cookie conforms.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified for a version is not allowed. </exception>
		// Token: 0x17000549 RID: 1353
		// (get) Token: 0x0600196B RID: 6507 RVA: 0x0005B9DB File Offset: 0x00059BDB
		// (set) Token: 0x0600196C RID: 6508 RVA: 0x0005B9E3 File Offset: 0x00059BE3
		public int Version
		{
			get
			{
				return this.m_version;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.m_version = value;
				if (value > 0 && this.m_cookieVariant < CookieVariant.Rfc2109)
				{
					this.m_cookieVariant = CookieVariant.Rfc2109;
				}
			}
		}

		// Token: 0x1700054A RID: 1354
		// (get) Token: 0x0600196D RID: 6509 RVA: 0x0005BA10 File Offset: 0x00059C10
		private string _Version
		{
			get
			{
				if (this.Version != 0)
				{
					return "$Version=" + (this.IsQuotedVersion ? "\"" : string.Empty) + this.m_version.ToString(NumberFormatInfo.InvariantInfo) + (this.IsQuotedVersion ? "\"" : string.Empty);
				}
				return string.Empty;
			}
		}

		// Token: 0x0600196E RID: 6510 RVA: 0x0005BA6D File Offset: 0x00059C6D
		internal static IComparer GetComparer()
		{
			return Cookie.staticComparer;
		}

		/// <summary>Overrides the <see cref="M:System.Object.Equals(System.Object)" /> method.</summary>
		/// <param name="comparand">A reference to a <see cref="T:System.Net.Cookie" />. </param>
		/// <returns>Returns <see langword="true" /> if the <see cref="T:System.Net.Cookie" /> is equal to <paramref name="comparand" />. Two <see cref="T:System.Net.Cookie" /> instances are equal if their <see cref="P:System.Net.Cookie.Name" />, <see cref="P:System.Net.Cookie.Value" />, <see cref="P:System.Net.Cookie.Path" />, <see cref="P:System.Net.Cookie.Domain" />, and <see cref="P:System.Net.Cookie.Version" /> properties are equal. <see cref="P:System.Net.Cookie.Name" /> and <see cref="P:System.Net.Cookie.Domain" /> string comparisons are case-insensitive.</returns>
		// Token: 0x0600196F RID: 6511 RVA: 0x0005BA74 File Offset: 0x00059C74
		public override bool Equals(object comparand)
		{
			if (!(comparand is Cookie))
			{
				return false;
			}
			Cookie cookie = (Cookie)comparand;
			return string.Compare(this.Name, cookie.Name, StringComparison.OrdinalIgnoreCase) == 0 && string.Compare(this.Value, cookie.Value, StringComparison.Ordinal) == 0 && string.Compare(this.Path, cookie.Path, StringComparison.Ordinal) == 0 && string.Compare(this.Domain, cookie.Domain, StringComparison.OrdinalIgnoreCase) == 0 && this.Version == cookie.Version;
		}

		/// <summary>Overrides the <see cref="M:System.Object.GetHashCode" /> method.</summary>
		/// <returns>The 32-bit signed integer hash code for this instance.</returns>
		// Token: 0x06001970 RID: 6512 RVA: 0x0005BAF4 File Offset: 0x00059CF4
		public override int GetHashCode()
		{
			return string.Concat(new object[]
			{
				this.Name,
				"=",
				this.Value,
				";",
				this.Path,
				"; ",
				this.Domain,
				"; ",
				this.Version
			}).GetHashCode();
		}

		/// <summary>Overrides the <see cref="M:System.Object.ToString" /> method.</summary>
		/// <returns>Returns a string representation of this <see cref="T:System.Net.Cookie" /> object that is suitable for including in a HTTP Cookie: request header.</returns>
		// Token: 0x06001971 RID: 6513 RVA: 0x0005BB64 File Offset: 0x00059D64
		public override string ToString()
		{
			string domain = this._Domain;
			string path = this._Path;
			string port = this._Port;
			string version = this._Version;
			string text = string.Concat(new string[]
			{
				(version.Length == 0) ? string.Empty : (version + "; "),
				this.Name,
				"=",
				this.Value,
				(path.Length == 0) ? string.Empty : ("; " + path),
				(domain.Length == 0) ? string.Empty : ("; " + domain),
				(port.Length == 0) ? string.Empty : ("; " + port)
			});
			if (text == "=")
			{
				return string.Empty;
			}
			return text;
		}

		// Token: 0x06001972 RID: 6514 RVA: 0x0005BC40 File Offset: 0x00059E40
		internal string ToServerString()
		{
			string text = this.Name + "=" + this.Value;
			if (this.m_comment != null && this.m_comment.Length > 0)
			{
				text = text + "; Comment=" + this.m_comment;
			}
			if (this.m_commentUri != null)
			{
				text = text + "; CommentURL=\"" + this.m_commentUri.ToString() + "\"";
			}
			if (this.m_discard)
			{
				text += "; Discard";
			}
			if (!this.m_domain_implicit && this.m_domain != null && this.m_domain.Length > 0)
			{
				text = text + "; Domain=" + this.m_domain;
			}
			if (this.Expires != DateTime.MinValue)
			{
				int num = (int)(this.Expires.ToLocalTime() - DateTime.Now).TotalSeconds;
				if (num < 0)
				{
					num = 0;
				}
				text = text + "; Max-Age=" + num.ToString(NumberFormatInfo.InvariantInfo);
			}
			if (!this.m_path_implicit && this.m_path != null && this.m_path.Length > 0)
			{
				text = text + "; Path=" + this.m_path;
			}
			if (!this.Plain && !this.m_port_implicit && this.m_port != null && this.m_port.Length > 0)
			{
				text = text + "; Port=" + this.m_port;
			}
			if (this.m_version > 0)
			{
				text = text + "; Version=" + this.m_version.ToString(NumberFormatInfo.InvariantInfo);
			}
			if (!(text == "="))
			{
				return text;
			}
			return null;
		}

		// Token: 0x06001973 RID: 6515 RVA: 0x0005BDEC File Offset: 0x00059FEC
		// Note: this type is marked as 'beforefieldinit'.
		static Cookie()
		{
		}

		// Token: 0x040017FA RID: 6138
		internal const int MaxSupportedVersion = 1;

		// Token: 0x040017FB RID: 6139
		internal const string CommentAttributeName = "Comment";

		// Token: 0x040017FC RID: 6140
		internal const string CommentUrlAttributeName = "CommentURL";

		// Token: 0x040017FD RID: 6141
		internal const string DiscardAttributeName = "Discard";

		// Token: 0x040017FE RID: 6142
		internal const string DomainAttributeName = "Domain";

		// Token: 0x040017FF RID: 6143
		internal const string ExpiresAttributeName = "Expires";

		// Token: 0x04001800 RID: 6144
		internal const string MaxAgeAttributeName = "Max-Age";

		// Token: 0x04001801 RID: 6145
		internal const string PathAttributeName = "Path";

		// Token: 0x04001802 RID: 6146
		internal const string PortAttributeName = "Port";

		// Token: 0x04001803 RID: 6147
		internal const string SecureAttributeName = "Secure";

		// Token: 0x04001804 RID: 6148
		internal const string VersionAttributeName = "Version";

		// Token: 0x04001805 RID: 6149
		internal const string HttpOnlyAttributeName = "HttpOnly";

		// Token: 0x04001806 RID: 6150
		internal const string SeparatorLiteral = "; ";

		// Token: 0x04001807 RID: 6151
		internal const string EqualsLiteral = "=";

		// Token: 0x04001808 RID: 6152
		internal const string QuotesLiteral = "\"";

		// Token: 0x04001809 RID: 6153
		internal const string SpecialAttributeLiteral = "$";

		// Token: 0x0400180A RID: 6154
		internal static readonly char[] PortSplitDelimiters = new char[]
		{
			' ',
			',',
			'"'
		};

		// Token: 0x0400180B RID: 6155
		internal static readonly char[] Reserved2Name = new char[]
		{
			' ',
			'\t',
			'\r',
			'\n',
			'=',
			';',
			','
		};

		// Token: 0x0400180C RID: 6156
		internal static readonly char[] Reserved2Value = new char[]
		{
			';',
			','
		};

		// Token: 0x0400180D RID: 6157
		private static Comparer staticComparer = new Comparer();

		// Token: 0x0400180E RID: 6158
		private string m_comment = string.Empty;

		// Token: 0x0400180F RID: 6159
		private Uri m_commentUri;

		// Token: 0x04001810 RID: 6160
		private CookieVariant m_cookieVariant = CookieVariant.Plain;

		// Token: 0x04001811 RID: 6161
		private bool m_discard;

		// Token: 0x04001812 RID: 6162
		private string m_domain = string.Empty;

		// Token: 0x04001813 RID: 6163
		private bool m_domain_implicit = true;

		// Token: 0x04001814 RID: 6164
		private DateTime m_expires = DateTime.MinValue;

		// Token: 0x04001815 RID: 6165
		private string m_name = string.Empty;

		// Token: 0x04001816 RID: 6166
		private string m_path = string.Empty;

		// Token: 0x04001817 RID: 6167
		private bool m_path_implicit = true;

		// Token: 0x04001818 RID: 6168
		private string m_port = string.Empty;

		// Token: 0x04001819 RID: 6169
		private bool m_port_implicit = true;

		// Token: 0x0400181A RID: 6170
		private int[] m_port_list;

		// Token: 0x0400181B RID: 6171
		private bool m_secure;

		// Token: 0x0400181C RID: 6172
		[OptionalField]
		private bool m_httpOnly;

		// Token: 0x0400181D RID: 6173
		private DateTime m_timeStamp = DateTime.Now;

		// Token: 0x0400181E RID: 6174
		private string m_value = string.Empty;

		// Token: 0x0400181F RID: 6175
		private int m_version;

		// Token: 0x04001820 RID: 6176
		private string m_domainKey = string.Empty;

		// Token: 0x04001821 RID: 6177
		internal bool IsQuotedVersion;

		// Token: 0x04001822 RID: 6178
		internal bool IsQuotedDomain;
	}
}
