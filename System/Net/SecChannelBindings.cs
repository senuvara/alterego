﻿using System;
using System.Runtime.InteropServices;

namespace System.Net
{
	// Token: 0x0200030F RID: 783
	[StructLayout(LayoutKind.Sequential)]
	internal class SecChannelBindings
	{
		// Token: 0x060016B2 RID: 5810 RVA: 0x0000232F File Offset: 0x0000052F
		public SecChannelBindings()
		{
		}

		// Token: 0x040015E5 RID: 5605
		internal int dwInitiatorAddrType;

		// Token: 0x040015E6 RID: 5606
		internal int cbInitiatorLength;

		// Token: 0x040015E7 RID: 5607
		internal int dwInitiatorOffset;

		// Token: 0x040015E8 RID: 5608
		internal int dwAcceptorAddrType;

		// Token: 0x040015E9 RID: 5609
		internal int cbAcceptorLength;

		// Token: 0x040015EA RID: 5610
		internal int dwAcceptorOffset;

		// Token: 0x040015EB RID: 5611
		internal int cbApplicationDataLength;

		// Token: 0x040015EC RID: 5612
		internal int dwApplicationDataOffset;
	}
}
