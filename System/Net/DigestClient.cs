﻿using System;
using System.Collections;

namespace System.Net
{
	// Token: 0x020003CA RID: 970
	internal class DigestClient : IAuthenticationModule
	{
		// Token: 0x170005CC RID: 1484
		// (get) Token: 0x06001C69 RID: 7273 RVA: 0x000669E8 File Offset: 0x00064BE8
		private static Hashtable Cache
		{
			get
			{
				object syncRoot = DigestClient.cache.SyncRoot;
				lock (syncRoot)
				{
					DigestClient.CheckExpired(DigestClient.cache.Count);
				}
				return DigestClient.cache;
			}
		}

		// Token: 0x06001C6A RID: 7274 RVA: 0x00066A3C File Offset: 0x00064C3C
		private static void CheckExpired(int count)
		{
			if (count < 10)
			{
				return;
			}
			DateTime t = DateTime.MaxValue;
			DateTime utcNow = DateTime.UtcNow;
			ArrayList arrayList = null;
			foreach (object obj in DigestClient.cache.Keys)
			{
				int num = (int)obj;
				DigestSession digestSession = (DigestSession)DigestClient.cache[num];
				if (digestSession.LastUse < t && (digestSession.LastUse - utcNow).Ticks > 6000000000L)
				{
					t = digestSession.LastUse;
					if (arrayList == null)
					{
						arrayList = new ArrayList();
					}
					arrayList.Add(num);
				}
			}
			if (arrayList != null)
			{
				foreach (object obj2 in arrayList)
				{
					int num2 = (int)obj2;
					DigestClient.cache.Remove(num2);
				}
			}
		}

		// Token: 0x06001C6B RID: 7275 RVA: 0x00066B68 File Offset: 0x00064D68
		public Authorization Authenticate(string challenge, WebRequest webRequest, ICredentials credentials)
		{
			if (credentials == null || challenge == null)
			{
				return null;
			}
			if (challenge.Trim().ToLower().IndexOf("digest") == -1)
			{
				return null;
			}
			HttpWebRequest httpWebRequest = webRequest as HttpWebRequest;
			if (httpWebRequest == null)
			{
				return null;
			}
			DigestSession digestSession = new DigestSession();
			if (!digestSession.Parse(challenge))
			{
				return null;
			}
			int num = httpWebRequest.Address.GetHashCode() ^ credentials.GetHashCode() ^ digestSession.Nonce.GetHashCode();
			DigestSession digestSession2 = (DigestSession)DigestClient.Cache[num];
			bool flag = digestSession2 == null;
			if (flag)
			{
				digestSession2 = digestSession;
			}
			else if (!digestSession2.Parse(challenge))
			{
				return null;
			}
			if (flag)
			{
				DigestClient.Cache.Add(num, digestSession2);
			}
			return digestSession2.Authenticate(webRequest, credentials);
		}

		// Token: 0x06001C6C RID: 7276 RVA: 0x00066C20 File Offset: 0x00064E20
		public Authorization PreAuthenticate(WebRequest webRequest, ICredentials credentials)
		{
			HttpWebRequest httpWebRequest = webRequest as HttpWebRequest;
			if (httpWebRequest == null)
			{
				return null;
			}
			if (credentials == null)
			{
				return null;
			}
			int num = httpWebRequest.Address.GetHashCode() ^ credentials.GetHashCode();
			DigestSession digestSession = (DigestSession)DigestClient.Cache[num];
			if (digestSession == null)
			{
				return null;
			}
			return digestSession.Authenticate(webRequest, credentials);
		}

		// Token: 0x170005CD RID: 1485
		// (get) Token: 0x06001C6D RID: 7277 RVA: 0x00066C74 File Offset: 0x00064E74
		public string AuthenticationType
		{
			get
			{
				return "Digest";
			}
		}

		// Token: 0x170005CE RID: 1486
		// (get) Token: 0x06001C6E RID: 7278 RVA: 0x00003298 File Offset: 0x00001498
		public bool CanPreAuthenticate
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06001C6F RID: 7279 RVA: 0x0000232F File Offset: 0x0000052F
		public DigestClient()
		{
		}

		// Token: 0x06001C70 RID: 7280 RVA: 0x00066C7B File Offset: 0x00064E7B
		// Note: this type is marked as 'beforefieldinit'.
		static DigestClient()
		{
		}

		// Token: 0x04001988 RID: 6536
		private static readonly Hashtable cache = Hashtable.Synchronized(new Hashtable());
	}
}
