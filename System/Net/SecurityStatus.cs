﻿using System;

namespace System.Net
{
	// Token: 0x02000301 RID: 769
	internal enum SecurityStatus
	{
		// Token: 0x04001543 RID: 5443
		OK,
		// Token: 0x04001544 RID: 5444
		ContinueNeeded = 590610,
		// Token: 0x04001545 RID: 5445
		CompleteNeeded,
		// Token: 0x04001546 RID: 5446
		CompAndContinue,
		// Token: 0x04001547 RID: 5447
		ContextExpired = 590615,
		// Token: 0x04001548 RID: 5448
		CredentialsNeeded = 590624,
		// Token: 0x04001549 RID: 5449
		Renegotiate,
		// Token: 0x0400154A RID: 5450
		OutOfMemory = -2146893056,
		// Token: 0x0400154B RID: 5451
		InvalidHandle,
		// Token: 0x0400154C RID: 5452
		Unsupported,
		// Token: 0x0400154D RID: 5453
		TargetUnknown,
		// Token: 0x0400154E RID: 5454
		InternalError,
		// Token: 0x0400154F RID: 5455
		PackageNotFound,
		// Token: 0x04001550 RID: 5456
		NotOwner,
		// Token: 0x04001551 RID: 5457
		CannotInstall,
		// Token: 0x04001552 RID: 5458
		InvalidToken,
		// Token: 0x04001553 RID: 5459
		CannotPack,
		// Token: 0x04001554 RID: 5460
		QopNotSupported,
		// Token: 0x04001555 RID: 5461
		NoImpersonation,
		// Token: 0x04001556 RID: 5462
		LogonDenied,
		// Token: 0x04001557 RID: 5463
		UnknownCredentials,
		// Token: 0x04001558 RID: 5464
		NoCredentials,
		// Token: 0x04001559 RID: 5465
		MessageAltered,
		// Token: 0x0400155A RID: 5466
		OutOfSequence,
		// Token: 0x0400155B RID: 5467
		NoAuthenticatingAuthority,
		// Token: 0x0400155C RID: 5468
		IncompleteMessage = -2146893032,
		// Token: 0x0400155D RID: 5469
		IncompleteCredentials = -2146893024,
		// Token: 0x0400155E RID: 5470
		BufferNotEnough,
		// Token: 0x0400155F RID: 5471
		WrongPrincipal,
		// Token: 0x04001560 RID: 5472
		TimeSkew = -2146893020,
		// Token: 0x04001561 RID: 5473
		UntrustedRoot,
		// Token: 0x04001562 RID: 5474
		IllegalMessage,
		// Token: 0x04001563 RID: 5475
		CertUnknown,
		// Token: 0x04001564 RID: 5476
		CertExpired,
		// Token: 0x04001565 RID: 5477
		AlgorithmMismatch = -2146893007,
		// Token: 0x04001566 RID: 5478
		SecurityQosFailed,
		// Token: 0x04001567 RID: 5479
		SmartcardLogonRequired = -2146892994,
		// Token: 0x04001568 RID: 5480
		UnsupportedPreauth = -2146892989,
		// Token: 0x04001569 RID: 5481
		BadBinding = -2146892986
	}
}
