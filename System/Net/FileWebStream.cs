﻿using System;
using System.IO;

namespace System.Net
{
	// Token: 0x02000383 RID: 899
	internal sealed class FileWebStream : FileStream, ICloseEx
	{
		// Token: 0x060019FC RID: 6652 RVA: 0x0005EDE8 File Offset: 0x0005CFE8
		public FileWebStream(FileWebRequest request, string path, FileMode mode, FileAccess access, FileShare sharing) : base(path, mode, access, sharing)
		{
			this.m_request = request;
		}

		// Token: 0x060019FD RID: 6653 RVA: 0x0005EDFD File Offset: 0x0005CFFD
		public FileWebStream(FileWebRequest request, string path, FileMode mode, FileAccess access, FileShare sharing, int length, bool async) : base(path, mode, access, sharing, length, async)
		{
			this.m_request = request;
		}

		// Token: 0x060019FE RID: 6654 RVA: 0x0005EE18 File Offset: 0x0005D018
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.m_request != null)
				{
					this.m_request.UnblockReader();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x060019FF RID: 6655 RVA: 0x0005EE58 File Offset: 0x0005D058
		void ICloseEx.CloseEx(CloseExState closeState)
		{
			if ((closeState & CloseExState.Abort) != CloseExState.Normal)
			{
				this.SafeFileHandle.Close();
				return;
			}
			this.Close();
		}

		// Token: 0x06001A00 RID: 6656 RVA: 0x0005EE74 File Offset: 0x0005D074
		public override int Read(byte[] buffer, int offset, int size)
		{
			this.CheckError();
			int result;
			try
			{
				result = base.Read(buffer, offset, size);
			}
			catch
			{
				this.CheckError();
				throw;
			}
			return result;
		}

		// Token: 0x06001A01 RID: 6657 RVA: 0x0005EEB0 File Offset: 0x0005D0B0
		public override void Write(byte[] buffer, int offset, int size)
		{
			this.CheckError();
			try
			{
				base.Write(buffer, offset, size);
			}
			catch
			{
				this.CheckError();
				throw;
			}
		}

		// Token: 0x06001A02 RID: 6658 RVA: 0x0005EEE8 File Offset: 0x0005D0E8
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
		{
			this.CheckError();
			IAsyncResult result;
			try
			{
				result = base.BeginRead(buffer, offset, size, callback, state);
			}
			catch
			{
				this.CheckError();
				throw;
			}
			return result;
		}

		// Token: 0x06001A03 RID: 6659 RVA: 0x0005EF28 File Offset: 0x0005D128
		public override int EndRead(IAsyncResult ar)
		{
			int result;
			try
			{
				result = base.EndRead(ar);
			}
			catch
			{
				this.CheckError();
				throw;
			}
			return result;
		}

		// Token: 0x06001A04 RID: 6660 RVA: 0x0005EF5C File Offset: 0x0005D15C
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
		{
			this.CheckError();
			IAsyncResult result;
			try
			{
				result = base.BeginWrite(buffer, offset, size, callback, state);
			}
			catch
			{
				this.CheckError();
				throw;
			}
			return result;
		}

		// Token: 0x06001A05 RID: 6661 RVA: 0x0005EF9C File Offset: 0x0005D19C
		public override void EndWrite(IAsyncResult ar)
		{
			try
			{
				base.EndWrite(ar);
			}
			catch
			{
				this.CheckError();
				throw;
			}
		}

		// Token: 0x06001A06 RID: 6662 RVA: 0x0005EFCC File Offset: 0x0005D1CC
		private void CheckError()
		{
			if (this.m_request.Aborted)
			{
				throw new WebException(NetRes.GetWebStatusString("net_requestaborted", WebExceptionStatus.RequestCanceled), WebExceptionStatus.RequestCanceled);
			}
		}

		// Token: 0x0400187A RID: 6266
		private FileWebRequest m_request;
	}
}
