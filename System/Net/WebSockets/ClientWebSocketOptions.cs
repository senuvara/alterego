﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace System.Net.WebSockets
{
	/// <summary>Options to use with a  <see cref="T:System.Net.WebSockets.ClientWebSocket" /> object.</summary>
	// Token: 0x0200054F RID: 1359
	public sealed class ClientWebSocketOptions
	{
		// Token: 0x06002ABC RID: 10940 RVA: 0x000956BE File Offset: 0x000938BE
		internal ClientWebSocketOptions()
		{
			this._requestedSubProtocols = new List<string>();
			this._requestHeaders = new WebHeaderCollection();
		}

		/// <summary>Creates a HTTP request header and its value.</summary>
		/// <param name="headerName">The name of the HTTP header.</param>
		/// <param name="headerValue">The value of the HTTP header.</param>
		// Token: 0x06002ABD RID: 10941 RVA: 0x000956FD File Offset: 0x000938FD
		public void SetRequestHeader(string headerName, string headerValue)
		{
			this.ThrowIfReadOnly();
			this._requestHeaders.Set(headerName, headerValue);
		}

		// Token: 0x17000AA3 RID: 2723
		// (get) Token: 0x06002ABE RID: 10942 RVA: 0x00095712 File Offset: 0x00093912
		internal WebHeaderCollection RequestHeaders
		{
			get
			{
				return this._requestHeaders;
			}
		}

		// Token: 0x17000AA4 RID: 2724
		// (get) Token: 0x06002ABF RID: 10943 RVA: 0x0009571A File Offset: 0x0009391A
		internal List<string> RequestedSubProtocols
		{
			get
			{
				return this._requestedSubProtocols;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that indicates if default credentials should be used during WebSocket handshake.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.
		///     <see langword="true" /> if default credentials should be used during WebSocket handshake; otherwise <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000AA5 RID: 2725
		// (get) Token: 0x06002AC0 RID: 10944 RVA: 0x00095722 File Offset: 0x00093922
		// (set) Token: 0x06002AC1 RID: 10945 RVA: 0x0009572A File Offset: 0x0009392A
		public bool UseDefaultCredentials
		{
			get
			{
				return this._useDefaultCredentials;
			}
			set
			{
				this.ThrowIfReadOnly();
				this._useDefaultCredentials = value;
			}
		}

		/// <summary>Gets or sets the credential information for the client.</summary>
		/// <returns>Returns <see cref="T:System.Net.ICredentials" />.The credential information for the client.</returns>
		// Token: 0x17000AA6 RID: 2726
		// (get) Token: 0x06002AC2 RID: 10946 RVA: 0x00095739 File Offset: 0x00093939
		// (set) Token: 0x06002AC3 RID: 10947 RVA: 0x00095741 File Offset: 0x00093941
		public ICredentials Credentials
		{
			get
			{
				return this._credentials;
			}
			set
			{
				this.ThrowIfReadOnly();
				this._credentials = value;
			}
		}

		/// <summary>Gets or sets the proxy for WebSocket requests.</summary>
		/// <returns>Returns <see cref="T:System.Net.IWebProxy" />.The proxy for WebSocket requests.</returns>
		// Token: 0x17000AA7 RID: 2727
		// (get) Token: 0x06002AC4 RID: 10948 RVA: 0x00095750 File Offset: 0x00093950
		// (set) Token: 0x06002AC5 RID: 10949 RVA: 0x00095758 File Offset: 0x00093958
		public IWebProxy Proxy
		{
			get
			{
				return this._proxy;
			}
			set
			{
				this.ThrowIfReadOnly();
				this._proxy = value;
			}
		}

		/// <summary>Gets or sets a collection of client side certificates.</summary>
		/// <returns>Returns <see cref="T:System.Security.Cryptography.X509Certificates.X509CertificateCollection" />.A collection of client side certificates.</returns>
		// Token: 0x17000AA8 RID: 2728
		// (get) Token: 0x06002AC6 RID: 10950 RVA: 0x00095767 File Offset: 0x00093967
		// (set) Token: 0x06002AC7 RID: 10951 RVA: 0x00095782 File Offset: 0x00093982
		public X509CertificateCollection ClientCertificates
		{
			get
			{
				if (this._clientCertificates == null)
				{
					this._clientCertificates = new X509CertificateCollection();
				}
				return this._clientCertificates;
			}
			set
			{
				this.ThrowIfReadOnly();
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this._clientCertificates = value;
			}
		}

		/// <summary>Gets or sets the cookies associated with the request.</summary>
		/// <returns>Returns <see cref="T:System.Net.CookieContainer" />.The cookies associated with the request.</returns>
		// Token: 0x17000AA9 RID: 2729
		// (get) Token: 0x06002AC8 RID: 10952 RVA: 0x0009579F File Offset: 0x0009399F
		// (set) Token: 0x06002AC9 RID: 10953 RVA: 0x000957A7 File Offset: 0x000939A7
		public CookieContainer Cookies
		{
			get
			{
				return this._cookies;
			}
			set
			{
				this.ThrowIfReadOnly();
				this._cookies = value;
			}
		}

		/// <summary>Adds a sub-protocol to be negotiated during the WebSocket connection handshake.</summary>
		/// <param name="subProtocol">The WebSocket sub-protocol to add.</param>
		// Token: 0x06002ACA RID: 10954 RVA: 0x000957B8 File Offset: 0x000939B8
		public void AddSubProtocol(string subProtocol)
		{
			this.ThrowIfReadOnly();
			WebSocketValidate.ValidateSubprotocol(subProtocol);
			using (List<string>.Enumerator enumerator = this._requestedSubProtocols.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if (string.Equals(enumerator.Current, subProtocol, StringComparison.OrdinalIgnoreCase))
					{
						throw new ArgumentException(SR.Format("Duplicate protocols are not allowed: '{0}'.", subProtocol), "subProtocol");
					}
				}
			}
			this._requestedSubProtocols.Add(subProtocol);
		}

		/// <summary>Gets or sets the WebSocket protocol keep-alive interval in milliseconds.</summary>
		/// <returns>Returns <see cref="T:System.TimeSpan" />.The WebSocket protocol keep-alive interval in milliseconds.</returns>
		// Token: 0x17000AAA RID: 2730
		// (get) Token: 0x06002ACB RID: 10955 RVA: 0x0009583C File Offset: 0x00093A3C
		// (set) Token: 0x06002ACC RID: 10956 RVA: 0x00095844 File Offset: 0x00093A44
		public TimeSpan KeepAliveInterval
		{
			get
			{
				return this._keepAliveInterval;
			}
			set
			{
				this.ThrowIfReadOnly();
				if (value != Timeout.InfiniteTimeSpan && value < TimeSpan.Zero)
				{
					throw new ArgumentOutOfRangeException("value", value, SR.Format("The argument must be a value greater than {0}.", Timeout.InfiniteTimeSpan.ToString()));
				}
				this._keepAliveInterval = value;
			}
		}

		// Token: 0x17000AAB RID: 2731
		// (get) Token: 0x06002ACD RID: 10957 RVA: 0x000958A6 File Offset: 0x00093AA6
		internal int ReceiveBufferSize
		{
			get
			{
				return this._receiveBufferSize;
			}
		}

		// Token: 0x17000AAC RID: 2732
		// (get) Token: 0x06002ACE RID: 10958 RVA: 0x000958AE File Offset: 0x00093AAE
		internal int SendBufferSize
		{
			get
			{
				return this._sendBufferSize;
			}
		}

		// Token: 0x17000AAD RID: 2733
		// (get) Token: 0x06002ACF RID: 10959 RVA: 0x000958B6 File Offset: 0x00093AB6
		internal ArraySegment<byte>? Buffer
		{
			get
			{
				return this._buffer;
			}
		}

		/// <summary>Sets the client buffer parameters.</summary>
		/// <param name="receiveBufferSize">The size, in bytes, of the client receive buffer.</param>
		/// <param name="sendBufferSize">The size, in bytes, of the client send buffer.</param>
		// Token: 0x06002AD0 RID: 10960 RVA: 0x000958C0 File Offset: 0x00093AC0
		public void SetBuffer(int receiveBufferSize, int sendBufferSize)
		{
			this.ThrowIfReadOnly();
			if (receiveBufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("receiveBufferSize", receiveBufferSize, SR.Format("The argument must be a value greater than {0}.", 1));
			}
			if (sendBufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("sendBufferSize", sendBufferSize, SR.Format("The argument must be a value greater than {0}.", 1));
			}
			this._receiveBufferSize = receiveBufferSize;
			this._sendBufferSize = sendBufferSize;
			this._buffer = null;
		}

		/// <summary>Sets client buffer parameters.</summary>
		/// <param name="receiveBufferSize">The size, in bytes, of the client receive buffer.</param>
		/// <param name="sendBufferSize">The size, in bytes, of the client send buffer.</param>
		/// <param name="buffer">The receive buffer to use.</param>
		// Token: 0x06002AD1 RID: 10961 RVA: 0x00095938 File Offset: 0x00093B38
		public void SetBuffer(int receiveBufferSize, int sendBufferSize, ArraySegment<byte> buffer)
		{
			this.ThrowIfReadOnly();
			if (receiveBufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("receiveBufferSize", receiveBufferSize, SR.Format("The argument must be a value greater than {0}.", 1));
			}
			if (sendBufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("sendBufferSize", sendBufferSize, SR.Format("The argument must be a value greater than {0}.", 1));
			}
			WebSocketValidate.ValidateArraySegment(buffer, "buffer");
			if (buffer.Count == 0)
			{
				throw new ArgumentOutOfRangeException("buffer");
			}
			this._receiveBufferSize = receiveBufferSize;
			this._sendBufferSize = sendBufferSize;
			this._buffer = new ArraySegment<byte>?(buffer);
		}

		// Token: 0x06002AD2 RID: 10962 RVA: 0x000959CE File Offset: 0x00093BCE
		internal void SetToReadOnly()
		{
			this._isReadOnly = true;
		}

		// Token: 0x06002AD3 RID: 10963 RVA: 0x000959D7 File Offset: 0x00093BD7
		private void ThrowIfReadOnly()
		{
			if (this._isReadOnly)
			{
				throw new InvalidOperationException("The WebSocket has already been started.");
			}
		}

		// Token: 0x04002281 RID: 8833
		private bool _isReadOnly;

		// Token: 0x04002282 RID: 8834
		private readonly List<string> _requestedSubProtocols;

		// Token: 0x04002283 RID: 8835
		private readonly WebHeaderCollection _requestHeaders;

		// Token: 0x04002284 RID: 8836
		private TimeSpan _keepAliveInterval = WebSocket.DefaultKeepAliveInterval;

		// Token: 0x04002285 RID: 8837
		private bool _useDefaultCredentials;

		// Token: 0x04002286 RID: 8838
		private ICredentials _credentials;

		// Token: 0x04002287 RID: 8839
		private IWebProxy _proxy;

		// Token: 0x04002288 RID: 8840
		private X509CertificateCollection _clientCertificates;

		// Token: 0x04002289 RID: 8841
		private CookieContainer _cookies;

		// Token: 0x0400228A RID: 8842
		private int _receiveBufferSize = 4096;

		// Token: 0x0400228B RID: 8843
		private int _sendBufferSize = 4096;

		// Token: 0x0400228C RID: 8844
		private ArraySegment<byte>? _buffer;
	}
}
