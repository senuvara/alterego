﻿using System;
using System.Runtime.CompilerServices;

namespace System.Net.WebSockets
{
	/// <summary>An instance of this class represents the result of performing a single ReceiveAsync operation on a WebSocket.</summary>
	// Token: 0x0200055D RID: 1373
	public class WebSocketReceiveResult
	{
		/// <summary>Creates an instance of the <see cref="T:System.Net.WebSockets.WebSocketReceiveResult" /> class.</summary>
		/// <param name="count">The number of bytes received.</param>
		/// <param name="messageType">The type of message that was received.</param>
		/// <param name="endOfMessage">Indicates whether this is the final message.</param>
		// Token: 0x06002B2D RID: 11053 RVA: 0x000970BC File Offset: 0x000952BC
		public WebSocketReceiveResult(int count, WebSocketMessageType messageType, bool endOfMessage) : this(count, messageType, endOfMessage, null, null)
		{
		}

		/// <summary>Creates an instance of the <see cref="T:System.Net.WebSockets.WebSocketReceiveResult" /> class.</summary>
		/// <param name="count">The number of bytes received.</param>
		/// <param name="messageType">The type of message that was received.</param>
		/// <param name="endOfMessage">Indicates whether this is the final message.</param>
		/// <param name="closeStatus">Indicates the <see cref="T:System.Net.WebSockets.WebSocketCloseStatus" /> of the connection.</param>
		/// <param name="closeStatusDescription">The description of <paramref name="closeStatus" />.</param>
		// Token: 0x06002B2E RID: 11054 RVA: 0x000970DC File Offset: 0x000952DC
		public WebSocketReceiveResult(int count, WebSocketMessageType messageType, bool endOfMessage, WebSocketCloseStatus? closeStatus, string closeStatusDescription)
		{
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			this.Count = count;
			this.EndOfMessage = endOfMessage;
			this.MessageType = messageType;
			this.CloseStatus = closeStatus;
			this.CloseStatusDescription = closeStatusDescription;
		}

		/// <summary>Indicates the number of bytes that the WebSocket received.</summary>
		/// <returns>Returns <see cref="T:System.Int32" />.</returns>
		// Token: 0x17000AC5 RID: 2757
		// (get) Token: 0x06002B2F RID: 11055 RVA: 0x00097118 File Offset: 0x00095318
		public int Count
		{
			[CompilerGenerated]
			get
			{
				return this.<Count>k__BackingField;
			}
		}

		/// <summary>Indicates whether the message has been received completely.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.</returns>
		// Token: 0x17000AC6 RID: 2758
		// (get) Token: 0x06002B30 RID: 11056 RVA: 0x00097120 File Offset: 0x00095320
		public bool EndOfMessage
		{
			[CompilerGenerated]
			get
			{
				return this.<EndOfMessage>k__BackingField;
			}
		}

		/// <summary>Indicates whether the current message is a UTF-8 message or a binary message.</summary>
		/// <returns>Returns <see cref="T:System.Net.WebSockets.WebSocketMessageType" />.</returns>
		// Token: 0x17000AC7 RID: 2759
		// (get) Token: 0x06002B31 RID: 11057 RVA: 0x00097128 File Offset: 0x00095328
		public WebSocketMessageType MessageType
		{
			[CompilerGenerated]
			get
			{
				return this.<MessageType>k__BackingField;
			}
		}

		/// <summary>Indicates the reason why the remote endpoint initiated the close handshake.</summary>
		/// <returns>Returns <see cref="T:System.Net.WebSockets.WebSocketCloseStatus" />.</returns>
		// Token: 0x17000AC8 RID: 2760
		// (get) Token: 0x06002B32 RID: 11058 RVA: 0x00097130 File Offset: 0x00095330
		public WebSocketCloseStatus? CloseStatus
		{
			[CompilerGenerated]
			get
			{
				return this.<CloseStatus>k__BackingField;
			}
		}

		/// <summary>Returns the optional description that describes why the close handshake has been initiated by the remote endpoint.</summary>
		/// <returns>Returns <see cref="T:System.String" />.</returns>
		// Token: 0x17000AC9 RID: 2761
		// (get) Token: 0x06002B33 RID: 11059 RVA: 0x00097138 File Offset: 0x00095338
		public string CloseStatusDescription
		{
			[CompilerGenerated]
			get
			{
				return this.<CloseStatusDescription>k__BackingField;
			}
		}

		// Token: 0x040022E3 RID: 8931
		[CompilerGenerated]
		private readonly int <Count>k__BackingField;

		// Token: 0x040022E4 RID: 8932
		[CompilerGenerated]
		private readonly bool <EndOfMessage>k__BackingField;

		// Token: 0x040022E5 RID: 8933
		[CompilerGenerated]
		private readonly WebSocketMessageType <MessageType>k__BackingField;

		// Token: 0x040022E6 RID: 8934
		[CompilerGenerated]
		private readonly WebSocketCloseStatus? <CloseStatus>k__BackingField;

		// Token: 0x040022E7 RID: 8935
		[CompilerGenerated]
		private readonly string <CloseStatusDescription>k__BackingField;
	}
}
