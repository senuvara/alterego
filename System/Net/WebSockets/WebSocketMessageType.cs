﻿using System;

namespace System.Net.WebSockets
{
	/// <summary>Indicates the message type.</summary>
	// Token: 0x0200055C RID: 1372
	public enum WebSocketMessageType
	{
		/// <summary>The message is clear text.</summary>
		// Token: 0x040022E0 RID: 8928
		Text,
		/// <summary>The message is in binary format.</summary>
		// Token: 0x040022E1 RID: 8929
		Binary,
		/// <summary>A receive has completed because a close message was received.</summary>
		// Token: 0x040022E2 RID: 8930
		Close
	}
}
