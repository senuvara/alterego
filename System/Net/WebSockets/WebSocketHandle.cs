﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net.WebSockets
{
	// Token: 0x02000550 RID: 1360
	internal sealed class WebSocketHandle
	{
		// Token: 0x06002AD4 RID: 10964 RVA: 0x000959EC File Offset: 0x00093BEC
		public static WebSocketHandle Create()
		{
			return new WebSocketHandle();
		}

		// Token: 0x06002AD5 RID: 10965 RVA: 0x000959F3 File Offset: 0x00093BF3
		public static bool IsValid(WebSocketHandle handle)
		{
			return handle != null;
		}

		// Token: 0x17000AAE RID: 2734
		// (get) Token: 0x06002AD6 RID: 10966 RVA: 0x000959FC File Offset: 0x00093BFC
		public WebSocketCloseStatus? CloseStatus
		{
			get
			{
				WebSocket webSocket = this._webSocket;
				if (webSocket == null)
				{
					return null;
				}
				return webSocket.CloseStatus;
			}
		}

		// Token: 0x17000AAF RID: 2735
		// (get) Token: 0x06002AD7 RID: 10967 RVA: 0x00095A22 File Offset: 0x00093C22
		public string CloseStatusDescription
		{
			get
			{
				WebSocket webSocket = this._webSocket;
				if (webSocket == null)
				{
					return null;
				}
				return webSocket.CloseStatusDescription;
			}
		}

		// Token: 0x17000AB0 RID: 2736
		// (get) Token: 0x06002AD8 RID: 10968 RVA: 0x00095A35 File Offset: 0x00093C35
		public WebSocketState State
		{
			get
			{
				WebSocket webSocket = this._webSocket;
				if (webSocket == null)
				{
					return this._state;
				}
				return webSocket.State;
			}
		}

		// Token: 0x17000AB1 RID: 2737
		// (get) Token: 0x06002AD9 RID: 10969 RVA: 0x00095A4D File Offset: 0x00093C4D
		public string SubProtocol
		{
			get
			{
				WebSocket webSocket = this._webSocket;
				if (webSocket == null)
				{
					return null;
				}
				return webSocket.SubProtocol;
			}
		}

		// Token: 0x06002ADA RID: 10970 RVA: 0x0000232D File Offset: 0x0000052D
		public static void CheckPlatformSupport()
		{
		}

		// Token: 0x06002ADB RID: 10971 RVA: 0x00095A60 File Offset: 0x00093C60
		public void Dispose()
		{
			this._state = WebSocketState.Closed;
			WebSocket webSocket = this._webSocket;
			if (webSocket == null)
			{
				return;
			}
			webSocket.Dispose();
		}

		// Token: 0x06002ADC RID: 10972 RVA: 0x00095A79 File Offset: 0x00093C79
		public void Abort()
		{
			this._abortSource.Cancel();
			WebSocket webSocket = this._webSocket;
			if (webSocket == null)
			{
				return;
			}
			webSocket.Abort();
		}

		// Token: 0x06002ADD RID: 10973 RVA: 0x00095A96 File Offset: 0x00093C96
		public Task SendAsync(ArraySegment<byte> buffer, WebSocketMessageType messageType, bool endOfMessage, CancellationToken cancellationToken)
		{
			return this._webSocket.SendAsync(buffer, messageType, endOfMessage, cancellationToken);
		}

		// Token: 0x06002ADE RID: 10974 RVA: 0x00095AA8 File Offset: 0x00093CA8
		public Task<WebSocketReceiveResult> ReceiveAsync(ArraySegment<byte> buffer, CancellationToken cancellationToken)
		{
			return this._webSocket.ReceiveAsync(buffer, cancellationToken);
		}

		// Token: 0x06002ADF RID: 10975 RVA: 0x00095AB7 File Offset: 0x00093CB7
		public Task CloseAsync(WebSocketCloseStatus closeStatus, string statusDescription, CancellationToken cancellationToken)
		{
			return this._webSocket.CloseAsync(closeStatus, statusDescription, cancellationToken);
		}

		// Token: 0x06002AE0 RID: 10976 RVA: 0x00095AC7 File Offset: 0x00093CC7
		public Task CloseOutputAsync(WebSocketCloseStatus closeStatus, string statusDescription, CancellationToken cancellationToken)
		{
			return this._webSocket.CloseOutputAsync(closeStatus, statusDescription, cancellationToken);
		}

		// Token: 0x06002AE1 RID: 10977 RVA: 0x00095AD8 File Offset: 0x00093CD8
		public async Task ConnectAsyncCore(Uri uri, CancellationToken cancellationToken, ClientWebSocketOptions options)
		{
			using (cancellationToken.Register(delegate(object s)
			{
				((WebSocketHandle)s).Abort();
			}, this))
			{
				try
				{
					Socket socket = await this.ConnectSocketAsync(uri.Host, uri.Port, cancellationToken).ConfigureAwait(false);
					Stream stream = new NetworkStream(socket, true);
					if (uri.Scheme == "wss")
					{
						SslStream sslStream = new SslStream(stream);
						await sslStream.AuthenticateAsClientAsync(uri.Host, options.ClientCertificates, SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12, false).ConfigureAwait(false);
						stream = sslStream;
						sslStream = null;
					}
					KeyValuePair<string, string> secKeyAndSecWebSocketAccept = WebSocketHandle.CreateSecKeyAndSecWebSocketAccept();
					byte[] array = WebSocketHandle.BuildRequestHeader(uri, options, secKeyAndSecWebSocketAccept.Key);
					await stream.WriteAsync(array, 0, array.Length, cancellationToken).ConfigureAwait(false);
					string subProtocol = await this.ParseAndValidateConnectResponseAsync(stream, options, secKeyAndSecWebSocketAccept.Value, cancellationToken).ConfigureAwait(false);
					this._webSocket = WebSocket.CreateClientWebSocket(stream, subProtocol, options.ReceiveBufferSize, options.SendBufferSize, options.KeepAliveInterval, false, options.Buffer.GetValueOrDefault());
					if (this._state == WebSocketState.Aborted)
					{
						this._webSocket.Abort();
					}
					else if (this._state == WebSocketState.Closed)
					{
						this._webSocket.Dispose();
					}
					stream = null;
					secKeyAndSecWebSocketAccept = default(KeyValuePair<string, string>);
				}
				catch (Exception ex)
				{
					if (this._state < WebSocketState.Closed)
					{
						this._state = WebSocketState.Closed;
					}
					this.Abort();
					if (ex is WebSocketException)
					{
						throw;
					}
					throw new WebSocketException("Unable to connect to the remote server", ex);
				}
			}
		}

		// Token: 0x06002AE2 RID: 10978 RVA: 0x00095B38 File Offset: 0x00093D38
		private async Task<Socket> ConnectSocketAsync(string host, int port, CancellationToken cancellationToken)
		{
			IPAddress[] array = await Dns.GetHostAddressesAsync(host).ConfigureAwait(false);
			ExceptionDispatchInfo lastException = null;
			foreach (IPAddress ipaddress in array)
			{
				Socket socket = new Socket(ipaddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
				try
				{
					CancellationToken token;
					using (cancellationToken.Register(delegate(object s)
					{
						((Socket)s).Dispose();
					}, socket))
					{
						token = this._abortSource.Token;
						using (token.Register(delegate(object s)
						{
							((Socket)s).Dispose();
						}, socket))
						{
							try
							{
								await socket.ConnectAsync(ipaddress, port).ConfigureAwait(false);
							}
							catch (ObjectDisposedException innerException)
							{
								CancellationToken token2 = cancellationToken.IsCancellationRequested ? cancellationToken : this._abortSource.Token;
								if (token2.IsCancellationRequested)
								{
									throw new OperationCanceledException(new OperationCanceledException().Message, innerException, token2);
								}
							}
						}
						CancellationTokenRegistration cancellationTokenRegistration2 = default(CancellationTokenRegistration);
					}
					CancellationTokenRegistration cancellationTokenRegistration = default(CancellationTokenRegistration);
					cancellationToken.ThrowIfCancellationRequested();
					token = this._abortSource.Token;
					token.ThrowIfCancellationRequested();
					return socket;
				}
				catch (Exception source)
				{
					socket.Dispose();
					lastException = ExceptionDispatchInfo.Capture(source);
				}
				socket = null;
			}
			IPAddress[] array2 = null;
			ExceptionDispatchInfo exceptionDispatchInfo = lastException;
			if (exceptionDispatchInfo != null)
			{
				exceptionDispatchInfo.Throw();
			}
			throw new WebSocketException("Unable to connect to the remote server");
		}

		// Token: 0x06002AE3 RID: 10979 RVA: 0x00095B98 File Offset: 0x00093D98
		private static byte[] BuildRequestHeader(Uri uri, ClientWebSocketOptions options, string secKey)
		{
			StringBuilder stringBuilder;
			if ((stringBuilder = WebSocketHandle.t_cachedStringBuilder) == null)
			{
				stringBuilder = (WebSocketHandle.t_cachedStringBuilder = new StringBuilder());
			}
			StringBuilder stringBuilder2 = stringBuilder;
			byte[] bytes;
			try
			{
				stringBuilder2.Append("GET ").Append(uri.PathAndQuery).Append(" HTTP/1.1\r\n");
				string value = options.RequestHeaders["Host"];
				stringBuilder2.Append("Host: ");
				if (string.IsNullOrEmpty(value))
				{
					stringBuilder2.Append(uri.IdnHost).Append(':').Append(uri.Port).Append("\r\n");
				}
				else
				{
					stringBuilder2.Append(value).Append("\r\n");
				}
				stringBuilder2.Append("Connection: Upgrade\r\n");
				stringBuilder2.Append("Upgrade: websocket\r\n");
				stringBuilder2.Append("Sec-WebSocket-Version: 13\r\n");
				stringBuilder2.Append("Sec-WebSocket-Key: ").Append(secKey).Append("\r\n");
				foreach (string text in options.RequestHeaders.AllKeys)
				{
					if (!string.Equals(text, "Host", StringComparison.OrdinalIgnoreCase))
					{
						stringBuilder2.Append(text).Append(": ").Append(options.RequestHeaders[text]).Append("\r\n");
					}
				}
				if (options.RequestedSubProtocols.Count > 0)
				{
					stringBuilder2.Append("Sec-WebSocket-Protocol").Append(": ");
					stringBuilder2.Append(options.RequestedSubProtocols[0]);
					for (int j = 1; j < options.RequestedSubProtocols.Count; j++)
					{
						stringBuilder2.Append(", ").Append(options.RequestedSubProtocols[j]);
					}
					stringBuilder2.Append("\r\n");
				}
				if (options.Cookies != null)
				{
					string cookieHeader = options.Cookies.GetCookieHeader(uri);
					if (!string.IsNullOrWhiteSpace(cookieHeader))
					{
						stringBuilder2.Append("Cookie").Append(": ").Append(cookieHeader).Append("\r\n");
					}
				}
				stringBuilder2.Append("\r\n");
				bytes = WebSocketHandle.s_defaultHttpEncoding.GetBytes(stringBuilder2.ToString());
			}
			finally
			{
				stringBuilder2.Clear();
			}
			return bytes;
		}

		// Token: 0x06002AE4 RID: 10980 RVA: 0x00095DE4 File Offset: 0x00093FE4
		private static KeyValuePair<string, string> CreateSecKeyAndSecWebSocketAccept()
		{
			string text = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
			KeyValuePair<string, string> result;
			using (SHA1 sha = SHA1.Create())
			{
				result = new KeyValuePair<string, string>(text, Convert.ToBase64String(sha.ComputeHash(Encoding.ASCII.GetBytes(text + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"))));
			}
			return result;
		}

		// Token: 0x06002AE5 RID: 10981 RVA: 0x00095E50 File Offset: 0x00094050
		private async Task<string> ParseAndValidateConnectResponseAsync(Stream stream, ClientWebSocketOptions options, string expectedSecWebSocketAccept, CancellationToken cancellationToken)
		{
			string text = await WebSocketHandle.ReadResponseHeaderLineAsync(stream, cancellationToken).ConfigureAwait(false);
			if (string.IsNullOrEmpty(text))
			{
				throw new WebSocketException(SR.Format("Unable to connect to the remote server", Array.Empty<object>()));
			}
			if (!text.StartsWith("HTTP/1.1 ", StringComparison.Ordinal) || text.Length < "HTTP/1.1 101".Length)
			{
				throw new WebSocketException(WebSocketError.HeaderError);
			}
			if (!text.StartsWith("HTTP/1.1 101", StringComparison.Ordinal) || (text.Length > "HTTP/1.1 101".Length && !char.IsWhiteSpace(text["HTTP/1.1 101".Length])))
			{
				throw new WebSocketException("Unable to connect to the remote server");
			}
			bool foundUpgrade = false;
			bool foundConnection = false;
			bool foundSecWebSocketAccept = false;
			string subprotocol = null;
			string line;
			while (!string.IsNullOrEmpty(line = await WebSocketHandle.ReadResponseHeaderLineAsync(stream, cancellationToken).ConfigureAwait(false)))
			{
				int num = line.IndexOf(':');
				if (num == -1)
				{
					throw new WebSocketException(WebSocketError.HeaderError);
				}
				string text2 = line.SubstringTrim(0, num);
				string headerValue = line.SubstringTrim(num + 1);
				WebSocketHandle.ValidateAndTrackHeader("Connection", "Upgrade", text2, headerValue, ref foundConnection);
				WebSocketHandle.ValidateAndTrackHeader("Upgrade", "websocket", text2, headerValue, ref foundUpgrade);
				WebSocketHandle.ValidateAndTrackHeader("Sec-WebSocket-Accept", expectedSecWebSocketAccept, text2, headerValue, ref foundSecWebSocketAccept);
				if (string.Equals("Sec-WebSocket-Protocol", text2, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(headerValue))
				{
					string text3 = options.RequestedSubProtocols.Find((string requested) => string.Equals(requested, headerValue, StringComparison.OrdinalIgnoreCase));
					if (text3 == null || subprotocol != null)
					{
						throw new WebSocketException(WebSocketError.UnsupportedProtocol, SR.Format("The WebSocket client request requested '{0}' protocol(s), but server is only accepting '{1}' protocol(s).", string.Join(", ", options.RequestedSubProtocols), subprotocol));
					}
					subprotocol = text3;
				}
			}
			if (!foundUpgrade || !foundConnection || !foundSecWebSocketAccept)
			{
				throw new WebSocketException("Unable to connect to the remote server");
			}
			return subprotocol;
		}

		// Token: 0x06002AE6 RID: 10982 RVA: 0x00095EB0 File Offset: 0x000940B0
		private static void ValidateAndTrackHeader(string targetHeaderName, string targetHeaderValue, string foundHeaderName, string foundHeaderValue, ref bool foundHeader)
		{
			bool flag = string.Equals(targetHeaderName, foundHeaderName, StringComparison.OrdinalIgnoreCase);
			if (!foundHeader)
			{
				if (flag)
				{
					if (!string.Equals(targetHeaderValue, foundHeaderValue, StringComparison.OrdinalIgnoreCase))
					{
						throw new WebSocketException(SR.Format("The '{0}' header value '{1}' is invalid.", targetHeaderName, foundHeaderValue));
					}
					foundHeader = true;
					return;
				}
			}
			else if (flag)
			{
				throw new WebSocketException(SR.Format("Unable to connect to the remote server", Array.Empty<object>()));
			}
		}

		// Token: 0x06002AE7 RID: 10983 RVA: 0x00095F08 File Offset: 0x00094108
		private static async Task<string> ReadResponseHeaderLineAsync(Stream stream, CancellationToken cancellationToken)
		{
			StringBuilder sb = WebSocketHandle.t_cachedStringBuilder;
			if (sb != null)
			{
				WebSocketHandle.t_cachedStringBuilder = null;
			}
			else
			{
				sb = new StringBuilder();
			}
			byte[] arr = new byte[1];
			char prevChar = '\0';
			string result;
			try
			{
				for (;;)
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = stream.ReadAsync(arr, 0, 1, cancellationToken).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() != 1)
					{
						break;
					}
					char c = (char)arr[0];
					if (prevChar == '\r' && c == '\n')
					{
						break;
					}
					sb.Append(c);
					prevChar = c;
				}
				if (sb.Length > 0 && sb[sb.Length - 1] == '\r')
				{
					sb.Length--;
				}
				result = sb.ToString();
			}
			finally
			{
				sb.Clear();
				WebSocketHandle.t_cachedStringBuilder = sb;
			}
			return result;
		}

		// Token: 0x06002AE8 RID: 10984 RVA: 0x00095F55 File Offset: 0x00094155
		public WebSocketHandle()
		{
		}

		// Token: 0x06002AE9 RID: 10985 RVA: 0x00095F6F File Offset: 0x0009416F
		// Note: this type is marked as 'beforefieldinit'.
		static WebSocketHandle()
		{
		}

		// Token: 0x0400228D RID: 8845
		[ThreadStatic]
		private static StringBuilder t_cachedStringBuilder;

		// Token: 0x0400228E RID: 8846
		private static readonly Encoding s_defaultHttpEncoding = Encoding.GetEncoding(28591);

		// Token: 0x0400228F RID: 8847
		private const int DefaultReceiveBufferSize = 4096;

		// Token: 0x04002290 RID: 8848
		private const string WSServerGuid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

		// Token: 0x04002291 RID: 8849
		private readonly CancellationTokenSource _abortSource = new CancellationTokenSource();

		// Token: 0x04002292 RID: 8850
		private WebSocketState _state = WebSocketState.Connecting;

		// Token: 0x04002293 RID: 8851
		private WebSocket _webSocket;

		// Token: 0x02000551 RID: 1361
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06002AEA RID: 10986 RVA: 0x00095F80 File Offset: 0x00094180
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06002AEB RID: 10987 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c()
			{
			}

			// Token: 0x06002AEC RID: 10988 RVA: 0x00095F8C File Offset: 0x0009418C
			internal void <ConnectAsyncCore>b__24_0(object s)
			{
				((WebSocketHandle)s).Abort();
			}

			// Token: 0x06002AED RID: 10989 RVA: 0x00095F99 File Offset: 0x00094199
			internal void <ConnectSocketAsync>b__25_0(object s)
			{
				((Socket)s).Dispose();
			}

			// Token: 0x06002AEE RID: 10990 RVA: 0x00095F99 File Offset: 0x00094199
			internal void <ConnectSocketAsync>b__25_1(object s)
			{
				((Socket)s).Dispose();
			}

			// Token: 0x04002294 RID: 8852
			public static readonly WebSocketHandle.<>c <>9 = new WebSocketHandle.<>c();

			// Token: 0x04002295 RID: 8853
			public static Action<object> <>9__24_0;

			// Token: 0x04002296 RID: 8854
			public static Action<object> <>9__25_0;

			// Token: 0x04002297 RID: 8855
			public static Action<object> <>9__25_1;
		}

		// Token: 0x02000552 RID: 1362
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ConnectAsyncCore>d__24 : IAsyncStateMachine
		{
			// Token: 0x06002AEF RID: 10991 RVA: 0x00095FA8 File Offset: 0x000941A8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				WebSocketHandle webSocketHandle = this;
				try
				{
					if (num > 3)
					{
						registration = cancellationToken.Register(new Action<object>(WebSocketHandle.<>c.<>9.<ConnectAsyncCore>b__24_0), webSocketHandle);
					}
					try
					{
						ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter configuredTaskAwaiter;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						switch (num)
						{
						case 0:
						{
							ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							break;
						}
						case 1:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_199;
						}
						case 2:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_24C;
						}
						case 3:
						{
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
							configuredTaskAwaiter5 = configuredTaskAwaiter6;
							configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_2D1;
						}
						default:
							configuredTaskAwaiter = webSocketHandle.ConnectSocketAsync(uri.Host, uri.Port, cancellationToken).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 0);
								ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter, WebSocketHandle.<ConnectAsyncCore>d__24>(ref configuredTaskAwaiter, ref this);
								return;
							}
							break;
						}
						Socket result = configuredTaskAwaiter.GetResult();
						stream = new NetworkStream(result, true);
						if (!(uri.Scheme == "wss"))
						{
							goto IL_1B3;
						}
						sslStream = new SslStream(stream);
						configuredTaskAwaiter3 = sslStream.AuthenticateAsClientAsync(uri.Host, options.ClientCertificates, SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12, false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num = (num2 = 1);
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, WebSocketHandle.<ConnectAsyncCore>d__24>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						IL_199:
						configuredTaskAwaiter3.GetResult();
						stream = sslStream;
						sslStream = null;
						IL_1B3:
						secKeyAndSecWebSocketAccept = WebSocketHandle.CreateSecKeyAndSecWebSocketAccept();
						byte[] array = WebSocketHandle.BuildRequestHeader(uri, options, secKeyAndSecWebSocketAccept.Key);
						configuredTaskAwaiter3 = stream.WriteAsync(array, 0, array.Length, cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num = (num2 = 2);
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, WebSocketHandle.<ConnectAsyncCore>d__24>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						IL_24C:
						configuredTaskAwaiter3.GetResult();
						configuredTaskAwaiter5 = webSocketHandle.ParseAndValidateConnectResponseAsync(stream, options, secKeyAndSecWebSocketAccept.Value, cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num = (num2 = 3);
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, WebSocketHandle.<ConnectAsyncCore>d__24>(ref configuredTaskAwaiter5, ref this);
							return;
						}
						IL_2D1:
						string result2 = configuredTaskAwaiter5.GetResult();
						webSocketHandle._webSocket = WebSocket.CreateClientWebSocket(stream, result2, options.ReceiveBufferSize, options.SendBufferSize, options.KeepAliveInterval, false, options.Buffer.GetValueOrDefault());
						if (webSocketHandle._state == WebSocketState.Aborted)
						{
							webSocketHandle._webSocket.Abort();
						}
						else if (webSocketHandle._state == WebSocketState.Closed)
						{
							webSocketHandle._webSocket.Dispose();
						}
						stream = null;
						secKeyAndSecWebSocketAccept = default(KeyValuePair<string, string>);
					}
					catch (Exception ex)
					{
						if (webSocketHandle._state < WebSocketState.Closed)
						{
							webSocketHandle._state = WebSocketState.Closed;
						}
						webSocketHandle.Abort();
						if (ex is WebSocketException)
						{
							throw;
						}
						throw new WebSocketException("Unable to connect to the remote server", ex);
					}
					finally
					{
						if (num < 0)
						{
							registration.Dispose();
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06002AF0 RID: 10992 RVA: 0x000963D4 File Offset: 0x000945D4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04002298 RID: 8856
			public int <>1__state;

			// Token: 0x04002299 RID: 8857
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400229A RID: 8858
			public CancellationToken cancellationToken;

			// Token: 0x0400229B RID: 8859
			public WebSocketHandle <>4__this;

			// Token: 0x0400229C RID: 8860
			public Uri uri;

			// Token: 0x0400229D RID: 8861
			public ClientWebSocketOptions options;

			// Token: 0x0400229E RID: 8862
			private SslStream <sslStream>5__1;

			// Token: 0x0400229F RID: 8863
			private Stream <stream>5__2;

			// Token: 0x040022A0 RID: 8864
			private KeyValuePair<string, string> <secKeyAndSecWebSocketAccept>5__3;

			// Token: 0x040022A1 RID: 8865
			private CancellationTokenRegistration <registration>5__4;

			// Token: 0x040022A2 RID: 8866
			private ConfiguredTaskAwaitable<Socket>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040022A3 RID: 8867
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x040022A4 RID: 8868
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x02000553 RID: 1363
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ConnectSocketAsync>d__25 : IAsyncStateMachine
		{
			// Token: 0x06002AF1 RID: 10993 RVA: 0x000963E4 File Offset: 0x000945E4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				WebSocketHandle webSocketHandle = this;
				Socket result2;
				try
				{
					ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							goto IL_C3;
						}
						configuredTaskAwaiter = Dns.GetHostAddressesAsync(host).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter, WebSocketHandle.<ConnectSocketAsync>d__25>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					IPAddress[] result = configuredTaskAwaiter.GetResult();
					lastException = null;
					array2 = result;
					i = 0;
					goto IL_2AA;
					IL_C3:
					IPAddress ipaddress;
					try
					{
						if (num != 1)
						{
							cancellationTokenRegistration = cancellationToken.Register(new Action<object>(WebSocketHandle.<>c.<>9.<ConnectSocketAsync>b__25_0), socket);
						}
						CancellationToken token;
						try
						{
							if (num != 1)
							{
								token = webSocketHandle._abortSource.Token;
								cancellationTokenRegistration2 = token.Register(new Action<object>(WebSocketHandle.<>c.<>9.<ConnectSocketAsync>b__25_1), socket);
							}
							try
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
								if (num != 1)
								{
									configuredTaskAwaiter3 = socket.ConnectAsync(ipaddress, port).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter3.IsCompleted)
									{
										num = (num2 = 1);
										ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, WebSocketHandle.<ConnectSocketAsync>d__25>(ref configuredTaskAwaiter3, ref this);
										return;
									}
								}
								else
								{
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
									configuredTaskAwaiter3 = configuredTaskAwaiter4;
									configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
									num = (num2 = -1);
								}
								configuredTaskAwaiter3.GetResult();
							}
							catch (ObjectDisposedException innerException)
							{
								CancellationToken token2 = cancellationToken.IsCancellationRequested ? cancellationToken : webSocketHandle._abortSource.Token;
								if (token2.IsCancellationRequested)
								{
									throw new OperationCanceledException(new OperationCanceledException().Message, innerException, token2);
								}
							}
							finally
							{
								if (num < 0)
								{
									((IDisposable)cancellationTokenRegistration2).Dispose();
								}
							}
							cancellationTokenRegistration2 = default(CancellationTokenRegistration);
						}
						finally
						{
							if (num < 0)
							{
								((IDisposable)cancellationTokenRegistration).Dispose();
							}
						}
						cancellationTokenRegistration = default(CancellationTokenRegistration);
						cancellationToken.ThrowIfCancellationRequested();
						token = webSocketHandle._abortSource.Token;
						token.ThrowIfCancellationRequested();
						result2 = socket;
						goto IL_2F9;
					}
					catch (Exception source)
					{
						socket.Dispose();
						lastException = ExceptionDispatchInfo.Capture(source);
					}
					socket = null;
					i++;
					IL_2AA:
					if (i >= array2.Length)
					{
						array2 = null;
						ExceptionDispatchInfo exceptionDispatchInfo = lastException;
						if (exceptionDispatchInfo != null)
						{
							exceptionDispatchInfo.Throw();
						}
						throw new WebSocketException("Unable to connect to the remote server");
					}
					ipaddress = array2[i];
					socket = new Socket(ipaddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
					goto IL_C3;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_2F9:
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06002AF2 RID: 10994 RVA: 0x0009677C File Offset: 0x0009497C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040022A5 RID: 8869
			public int <>1__state;

			// Token: 0x040022A6 RID: 8870
			public AsyncTaskMethodBuilder<Socket> <>t__builder;

			// Token: 0x040022A7 RID: 8871
			public string host;

			// Token: 0x040022A8 RID: 8872
			public CancellationToken cancellationToken;

			// Token: 0x040022A9 RID: 8873
			public WebSocketHandle <>4__this;

			// Token: 0x040022AA RID: 8874
			public int port;

			// Token: 0x040022AB RID: 8875
			private Socket <socket>5__1;

			// Token: 0x040022AC RID: 8876
			private ExceptionDispatchInfo <lastException>5__2;

			// Token: 0x040022AD RID: 8877
			private ConfiguredTaskAwaitable<IPAddress[]>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040022AE RID: 8878
			private IPAddress[] <>7__wrap1;

			// Token: 0x040022AF RID: 8879
			private int <>7__wrap2;

			// Token: 0x040022B0 RID: 8880
			private CancellationTokenRegistration <>7__wrap3;

			// Token: 0x040022B1 RID: 8881
			private CancellationTokenRegistration <>7__wrap4;

			// Token: 0x040022B2 RID: 8882
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000554 RID: 1364
		[CompilerGenerated]
		private sealed class <>c__DisplayClass28_0
		{
			// Token: 0x06002AF3 RID: 10995 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass28_0()
			{
			}

			// Token: 0x06002AF4 RID: 10996 RVA: 0x0009678A File Offset: 0x0009498A
			internal bool <ParseAndValidateConnectResponseAsync>b__0(string requested)
			{
				return string.Equals(requested, this.headerValue, StringComparison.OrdinalIgnoreCase);
			}

			// Token: 0x040022B3 RID: 8883
			public string headerValue;
		}

		// Token: 0x02000555 RID: 1365
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseAndValidateConnectResponseAsync>d__28 : IAsyncStateMachine
		{
			// Token: 0x06002AF5 RID: 10997 RVA: 0x0009679C File Offset: 0x0009499C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				string result3;
				try
				{
					ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_2B1;
						}
						configuredTaskAwaiter = WebSocketHandle.ReadResponseHeaderLineAsync(stream, cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, WebSocketHandle.<ParseAndValidateConnectResponseAsync>d__28>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					string result = configuredTaskAwaiter.GetResult();
					if (string.IsNullOrEmpty(result))
					{
						throw new WebSocketException(SR.Format("Unable to connect to the remote server", Array.Empty<object>()));
					}
					if (!result.StartsWith("HTTP/1.1 ", StringComparison.Ordinal) || result.Length < "HTTP/1.1 101".Length)
					{
						throw new WebSocketException(WebSocketError.HeaderError);
					}
					if (!result.StartsWith("HTTP/1.1 101", StringComparison.Ordinal) || (result.Length > "HTTP/1.1 101".Length && !char.IsWhiteSpace(result["HTTP/1.1 101".Length])))
					{
						throw new WebSocketException("Unable to connect to the remote server");
					}
					foundUpgrade = false;
					foundConnection = false;
					foundSecWebSocketAccept = false;
					subprotocol = null;
					IL_248:
					configuredTaskAwaiter = WebSocketHandle.ReadResponseHeaderLineAsync(stream, cancellationToken).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, WebSocketHandle.<ParseAndValidateConnectResponseAsync>d__28>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_2B1:
					string result2 = configuredTaskAwaiter.GetResult();
					if (string.IsNullOrEmpty(line = result2))
					{
						if (!foundUpgrade || !foundConnection || !foundSecWebSocketAccept)
						{
							throw new WebSocketException("Unable to connect to the remote server");
						}
						result3 = subprotocol;
					}
					else
					{
						WebSocketHandle.<>c__DisplayClass28_0 CS$<>8__locals1 = new WebSocketHandle.<>c__DisplayClass28_0();
						int num3 = line.IndexOf(':');
						if (num3 == -1)
						{
							throw new WebSocketException(WebSocketError.HeaderError);
						}
						string text = line.SubstringTrim(0, num3);
						CS$<>8__locals1.headerValue = line.SubstringTrim(num3 + 1);
						WebSocketHandle.ValidateAndTrackHeader("Connection", "Upgrade", text, CS$<>8__locals1.headerValue, ref foundConnection);
						WebSocketHandle.ValidateAndTrackHeader("Upgrade", "websocket", text, CS$<>8__locals1.headerValue, ref foundUpgrade);
						WebSocketHandle.ValidateAndTrackHeader("Sec-WebSocket-Accept", expectedSecWebSocketAccept, text, CS$<>8__locals1.headerValue, ref foundSecWebSocketAccept);
						if (!string.Equals("Sec-WebSocket-Protocol", text, StringComparison.OrdinalIgnoreCase) || string.IsNullOrWhiteSpace(CS$<>8__locals1.headerValue))
						{
							goto IL_248;
						}
						string text2 = options.RequestedSubProtocols.Find(new Predicate<string>(CS$<>8__locals1.<ParseAndValidateConnectResponseAsync>b__0));
						if (text2 == null || subprotocol != null)
						{
							throw new WebSocketException(WebSocketError.UnsupportedProtocol, SR.Format("The WebSocket client request requested '{0}' protocol(s), but server is only accepting '{1}' protocol(s).", string.Join(", ", options.RequestedSubProtocols), subprotocol));
						}
						subprotocol = text2;
						goto IL_248;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result3);
			}

			// Token: 0x06002AF6 RID: 10998 RVA: 0x00096AF0 File Offset: 0x00094CF0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040022B4 RID: 8884
			public int <>1__state;

			// Token: 0x040022B5 RID: 8885
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x040022B6 RID: 8886
			public Stream stream;

			// Token: 0x040022B7 RID: 8887
			public CancellationToken cancellationToken;

			// Token: 0x040022B8 RID: 8888
			private string <line>5__1;

			// Token: 0x040022B9 RID: 8889
			private bool <foundConnection>5__2;

			// Token: 0x040022BA RID: 8890
			private bool <foundUpgrade>5__3;

			// Token: 0x040022BB RID: 8891
			public string expectedSecWebSocketAccept;

			// Token: 0x040022BC RID: 8892
			private bool <foundSecWebSocketAccept>5__4;

			// Token: 0x040022BD RID: 8893
			public ClientWebSocketOptions options;

			// Token: 0x040022BE RID: 8894
			private string <subprotocol>5__5;

			// Token: 0x040022BF RID: 8895
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000556 RID: 1366
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadResponseHeaderLineAsync>d__30 : IAsyncStateMachine
		{
			// Token: 0x06002AF7 RID: 10999 RVA: 0x00096B00 File Offset: 0x00094D00
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				string result;
				try
				{
					if (num != 0)
					{
						sb = WebSocketHandle.t_cachedStringBuilder;
						if (sb != null)
						{
							WebSocketHandle.t_cachedStringBuilder = null;
						}
						else
						{
							sb = new StringBuilder();
						}
						arr = new byte[1];
						prevChar = '\0';
					}
					try
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num == 0)
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_EC;
						}
						IL_7B:
						configuredTaskAwaiter3 = stream.ReadAsync(arr, 0, 1, cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num = (num2 = 0);
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, WebSocketHandle.<ReadResponseHeaderLineAsync>d__30>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						IL_EC:
						if (configuredTaskAwaiter3.GetResult() == 1)
						{
							char c = (char)arr[0];
							if (prevChar != '\r' || c != '\n')
							{
								sb.Append(c);
								prevChar = c;
								goto IL_7B;
							}
						}
						if (sb.Length > 0 && sb[sb.Length - 1] == '\r')
						{
							sb.Length--;
						}
						result = sb.ToString();
					}
					finally
					{
						if (num < 0)
						{
							sb.Clear();
							WebSocketHandle.t_cachedStringBuilder = sb;
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06002AF8 RID: 11000 RVA: 0x00096CD4 File Offset: 0x00094ED4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040022C0 RID: 8896
			public int <>1__state;

			// Token: 0x040022C1 RID: 8897
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x040022C2 RID: 8898
			private byte[] <arr>5__1;

			// Token: 0x040022C3 RID: 8899
			private char <prevChar>5__2;

			// Token: 0x040022C4 RID: 8900
			private StringBuilder <sb>5__3;

			// Token: 0x040022C5 RID: 8901
			public Stream stream;

			// Token: 0x040022C6 RID: 8902
			public CancellationToken cancellationToken;

			// Token: 0x040022C7 RID: 8903
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
