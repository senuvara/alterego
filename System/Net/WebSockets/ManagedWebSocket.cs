﻿using System;
using System.Buffers;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace System.Net.WebSockets
{
	// Token: 0x0200053D RID: 1341
	internal sealed class ManagedWebSocket : WebSocket
	{
		// Token: 0x06002A54 RID: 10836 RVA: 0x000926F9 File Offset: 0x000908F9
		public static ManagedWebSocket CreateFromConnectedStream(Stream stream, bool isServer, string subprotocol, TimeSpan keepAliveInterval, int receiveBufferSize, ArraySegment<byte>? receiveBuffer = null)
		{
			return new ManagedWebSocket(stream, isServer, subprotocol, keepAliveInterval, receiveBufferSize, receiveBuffer);
		}

		// Token: 0x17000A8C RID: 2700
		// (get) Token: 0x06002A55 RID: 10837 RVA: 0x00092708 File Offset: 0x00090908
		private object StateUpdateLock
		{
			get
			{
				return this._abortSource;
			}
		}

		// Token: 0x17000A8D RID: 2701
		// (get) Token: 0x06002A56 RID: 10838 RVA: 0x00092710 File Offset: 0x00090910
		private object ReceiveAsyncLock
		{
			get
			{
				return this._utf8TextState;
			}
		}

		// Token: 0x06002A57 RID: 10839 RVA: 0x00092718 File Offset: 0x00090918
		private ManagedWebSocket(Stream stream, bool isServer, string subprotocol, TimeSpan keepAliveInterval, int receiveBufferSize, ArraySegment<byte>? receiveBuffer)
		{
			this._stream = stream;
			this._isServer = isServer;
			this._subprotocol = subprotocol;
			if (receiveBuffer != null && receiveBuffer.GetValueOrDefault().Array != null && receiveBuffer.GetValueOrDefault().Offset == 0 && receiveBuffer.GetValueOrDefault().Count == receiveBuffer.GetValueOrDefault().Array.Length && receiveBuffer.GetValueOrDefault().Count >= 14)
			{
				this._receiveBuffer = receiveBuffer.Value.Array;
			}
			else
			{
				this._receiveBufferFromPool = true;
				this._receiveBuffer = ArrayPool<byte>.Shared.Rent(Math.Max(receiveBufferSize, 14));
			}
			this._abortSource.Token.Register(delegate(object s)
			{
				ManagedWebSocket managedWebSocket = (ManagedWebSocket)s;
				object stateUpdateLock = managedWebSocket.StateUpdateLock;
				lock (stateUpdateLock)
				{
					WebSocketState state = managedWebSocket._state;
					if (state != WebSocketState.Closed && state != WebSocketState.Aborted)
					{
						managedWebSocket._state = ((state != WebSocketState.None && state != WebSocketState.Connecting) ? WebSocketState.Aborted : WebSocketState.Closed);
					}
				}
			}, this);
			if (keepAliveInterval > TimeSpan.Zero)
			{
				this._keepAliveTimer = new Timer(delegate(object s)
				{
					((ManagedWebSocket)s).SendKeepAliveFrameAsync();
				}, this, keepAliveInterval, keepAliveInterval);
			}
		}

		// Token: 0x06002A58 RID: 10840 RVA: 0x00092894 File Offset: 0x00090A94
		public override void Dispose()
		{
			object stateUpdateLock = this.StateUpdateLock;
			lock (stateUpdateLock)
			{
				this.DisposeCore();
			}
		}

		// Token: 0x06002A59 RID: 10841 RVA: 0x000928D4 File Offset: 0x00090AD4
		private void DisposeCore()
		{
			if (!this._disposed)
			{
				this._disposed = true;
				Timer keepAliveTimer = this._keepAliveTimer;
				if (keepAliveTimer != null)
				{
					keepAliveTimer.Dispose();
				}
				Stream stream = this._stream;
				if (stream != null)
				{
					stream.Dispose();
				}
				if (this._receiveBufferFromPool)
				{
					byte[] receiveBuffer = this._receiveBuffer;
					this._receiveBuffer = null;
					ArrayPool<byte>.Shared.Return(receiveBuffer, false);
				}
				if (this._state < WebSocketState.Aborted)
				{
					this._state = WebSocketState.Closed;
				}
			}
		}

		// Token: 0x17000A8E RID: 2702
		// (get) Token: 0x06002A5A RID: 10842 RVA: 0x00092944 File Offset: 0x00090B44
		public override WebSocketCloseStatus? CloseStatus
		{
			get
			{
				return this._closeStatus;
			}
		}

		// Token: 0x17000A8F RID: 2703
		// (get) Token: 0x06002A5B RID: 10843 RVA: 0x0009294C File Offset: 0x00090B4C
		public override string CloseStatusDescription
		{
			get
			{
				return this._closeStatusDescription;
			}
		}

		// Token: 0x17000A90 RID: 2704
		// (get) Token: 0x06002A5C RID: 10844 RVA: 0x00092954 File Offset: 0x00090B54
		public override WebSocketState State
		{
			get
			{
				return this._state;
			}
		}

		// Token: 0x17000A91 RID: 2705
		// (get) Token: 0x06002A5D RID: 10845 RVA: 0x0009295C File Offset: 0x00090B5C
		public override string SubProtocol
		{
			get
			{
				return this._subprotocol;
			}
		}

		// Token: 0x06002A5E RID: 10846 RVA: 0x00092964 File Offset: 0x00090B64
		public override Task SendAsync(ArraySegment<byte> buffer, WebSocketMessageType messageType, bool endOfMessage, CancellationToken cancellationToken)
		{
			if (messageType != WebSocketMessageType.Text && messageType != WebSocketMessageType.Binary)
			{
				throw new ArgumentException(SR.Format("The message type '{0}' is not allowed for the '{1}' operation. Valid message types are: '{2}, {3}'. To close the WebSocket, use the '{4}' operation instead. ", new object[]
				{
					"Close",
					"SendAsync",
					"Binary",
					"Text",
					"CloseOutputAsync"
				}), "messageType");
			}
			WebSocketValidate.ValidateArraySegment(buffer, "buffer");
			try
			{
				WebSocketValidate.ThrowIfInvalidState(this._state, this._disposed, ManagedWebSocket.s_validSendStates);
				this.ThrowIfOperationInProgress(this._lastSendAsync, "SendAsync");
			}
			catch (Exception exception)
			{
				return Task.FromException(exception);
			}
			ManagedWebSocket.MessageOpcode opcode = this._lastSendWasFragment ? ManagedWebSocket.MessageOpcode.Continuation : ((messageType == WebSocketMessageType.Binary) ? ManagedWebSocket.MessageOpcode.Binary : ManagedWebSocket.MessageOpcode.Text);
			Task task = this.SendFrameAsync(opcode, endOfMessage, buffer, cancellationToken);
			this._lastSendWasFragment = !endOfMessage;
			this._lastSendAsync = task;
			return task;
		}

		// Token: 0x06002A5F RID: 10847 RVA: 0x00092A3C File Offset: 0x00090C3C
		public override Task<WebSocketReceiveResult> ReceiveAsync(ArraySegment<byte> buffer, CancellationToken cancellationToken)
		{
			WebSocketValidate.ValidateArraySegment(buffer, "buffer");
			Task<WebSocketReceiveResult> result;
			try
			{
				WebSocketValidate.ThrowIfInvalidState(this._state, this._disposed, ManagedWebSocket.s_validReceiveStates);
				object receiveAsyncLock = this.ReceiveAsyncLock;
				lock (receiveAsyncLock)
				{
					this.ThrowIfOperationInProgress(this._lastReceiveAsync, "ReceiveAsync");
					Task<WebSocketReceiveResult> task = this.ReceiveAsyncPrivate(buffer, cancellationToken);
					this._lastReceiveAsync = task;
					result = task;
				}
			}
			catch (Exception exception)
			{
				result = Task.FromException<WebSocketReceiveResult>(exception);
			}
			return result;
		}

		// Token: 0x06002A60 RID: 10848 RVA: 0x00092AD0 File Offset: 0x00090CD0
		public override Task CloseAsync(WebSocketCloseStatus closeStatus, string statusDescription, CancellationToken cancellationToken)
		{
			WebSocketValidate.ValidateCloseStatus(closeStatus, statusDescription);
			try
			{
				WebSocketValidate.ThrowIfInvalidState(this._state, this._disposed, ManagedWebSocket.s_validCloseStates);
			}
			catch (Exception exception)
			{
				return Task.FromException(exception);
			}
			return this.CloseAsyncPrivate(closeStatus, statusDescription, cancellationToken);
		}

		// Token: 0x06002A61 RID: 10849 RVA: 0x00092B20 File Offset: 0x00090D20
		public override Task CloseOutputAsync(WebSocketCloseStatus closeStatus, string statusDescription, CancellationToken cancellationToken)
		{
			WebSocketValidate.ValidateCloseStatus(closeStatus, statusDescription);
			try
			{
				WebSocketValidate.ThrowIfInvalidState(this._state, this._disposed, ManagedWebSocket.s_validCloseOutputStates);
			}
			catch (Exception exception)
			{
				return Task.FromException(exception);
			}
			return this.SendCloseFrameAsync(closeStatus, statusDescription, cancellationToken);
		}

		// Token: 0x06002A62 RID: 10850 RVA: 0x00092B70 File Offset: 0x00090D70
		public override void Abort()
		{
			this._abortSource.Cancel();
			this.Dispose();
		}

		// Token: 0x06002A63 RID: 10851 RVA: 0x00092B83 File Offset: 0x00090D83
		private Task SendFrameAsync(ManagedWebSocket.MessageOpcode opcode, bool endOfMessage, ArraySegment<byte> payloadBuffer, CancellationToken cancellationToken)
		{
			if (!cancellationToken.CanBeCanceled && this._sendFrameAsyncLock.Wait(0))
			{
				return this.SendFrameLockAcquiredNonCancelableAsync(opcode, endOfMessage, payloadBuffer);
			}
			return this.SendFrameFallbackAsync(opcode, endOfMessage, payloadBuffer, cancellationToken);
		}

		// Token: 0x06002A64 RID: 10852 RVA: 0x00092BB4 File Offset: 0x00090DB4
		private Task SendFrameLockAcquiredNonCancelableAsync(ManagedWebSocket.MessageOpcode opcode, bool endOfMessage, ArraySegment<byte> payloadBuffer)
		{
			Task task = null;
			bool flag = true;
			try
			{
				int count = this.WriteFrameToSendBuffer(opcode, endOfMessage, payloadBuffer);
				task = this._stream.WriteAsync(this._sendBuffer, 0, count, CancellationToken.None);
				if (task.IsCompleted)
				{
					return task;
				}
				flag = false;
			}
			catch (Exception innerException)
			{
				return Task.FromException((this._state == WebSocketState.Aborted) ? ManagedWebSocket.CreateOperationCanceledException(innerException, default(CancellationToken)) : new WebSocketException(WebSocketError.ConnectionClosedPrematurely, innerException));
			}
			finally
			{
				if (flag)
				{
					this._sendFrameAsyncLock.Release();
					this.ReleaseSendBuffer();
				}
			}
			return task.ContinueWith(delegate(Task t, object s)
			{
				ManagedWebSocket managedWebSocket = (ManagedWebSocket)s;
				managedWebSocket._sendFrameAsyncLock.Release();
				managedWebSocket.ReleaseSendBuffer();
				try
				{
					t.GetAwaiter().GetResult();
				}
				catch (Exception innerException2)
				{
					throw (managedWebSocket._state == WebSocketState.Aborted) ? ManagedWebSocket.CreateOperationCanceledException(innerException2, default(CancellationToken)) : new WebSocketException(WebSocketError.ConnectionClosedPrematurely, innerException2);
				}
			}, this, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
		}

		// Token: 0x06002A65 RID: 10853 RVA: 0x00092C90 File Offset: 0x00090E90
		private async Task SendFrameFallbackAsync(ManagedWebSocket.MessageOpcode opcode, bool endOfMessage, ArraySegment<byte> payloadBuffer, CancellationToken cancellationToken)
		{
			await this._sendFrameAsyncLock.WaitAsync().ConfigureAwait(false);
			try
			{
				int count = this.WriteFrameToSendBuffer(opcode, endOfMessage, payloadBuffer);
				using (cancellationToken.Register(delegate(object s)
				{
					((ManagedWebSocket)s).Abort();
				}, this))
				{
					await this._stream.WriteAsync(this._sendBuffer, 0, count, cancellationToken).ConfigureAwait(false);
				}
				CancellationTokenRegistration cancellationTokenRegistration = default(CancellationTokenRegistration);
			}
			catch (Exception innerException)
			{
				throw (this._state == WebSocketState.Aborted) ? ManagedWebSocket.CreateOperationCanceledException(innerException, cancellationToken) : new WebSocketException(WebSocketError.ConnectionClosedPrematurely, innerException);
			}
			finally
			{
				this._sendFrameAsyncLock.Release();
				this.ReleaseSendBuffer();
			}
		}

		// Token: 0x06002A66 RID: 10854 RVA: 0x00092CF8 File Offset: 0x00090EF8
		private int WriteFrameToSendBuffer(ManagedWebSocket.MessageOpcode opcode, bool endOfMessage, ArraySegment<byte> payloadBuffer)
		{
			this.AllocateSendBuffer(payloadBuffer.Count + 14);
			int? num = null;
			int num2;
			if (this._isServer)
			{
				num2 = ManagedWebSocket.WriteHeader(opcode, this._sendBuffer, payloadBuffer, endOfMessage, false);
			}
			else
			{
				num = new int?(ManagedWebSocket.WriteHeader(opcode, this._sendBuffer, payloadBuffer, endOfMessage, true));
				num2 = num.GetValueOrDefault() + 4;
			}
			if (payloadBuffer.Count > 0)
			{
				Buffer.BlockCopy(payloadBuffer.Array, payloadBuffer.Offset, this._sendBuffer, num2, payloadBuffer.Count);
				if (num != null)
				{
					ManagedWebSocket.ApplyMask(this._sendBuffer, num2, this._sendBuffer, num.Value, 0, (long)payloadBuffer.Count);
				}
			}
			return num2 + payloadBuffer.Count;
		}

		// Token: 0x06002A67 RID: 10855 RVA: 0x00092DB8 File Offset: 0x00090FB8
		private void SendKeepAliveFrameAsync()
		{
			if (this._sendFrameAsyncLock.Wait(0))
			{
				Task task = this.SendFrameLockAcquiredNonCancelableAsync(ManagedWebSocket.MessageOpcode.Ping, true, new ArraySegment<byte>(Array.Empty<byte>()));
				if (!task.IsCompletedSuccessfully)
				{
					task.ContinueWith(delegate(Task p)
					{
						AggregateException exception = p.Exception;
					}, CancellationToken.None, TaskContinuationOptions.NotOnRanToCompletion | TaskContinuationOptions.NotOnCanceled | TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
				}
			}
		}

		// Token: 0x06002A68 RID: 10856 RVA: 0x00092E24 File Offset: 0x00091024
		private static int WriteHeader(ManagedWebSocket.MessageOpcode opcode, byte[] sendBuffer, ArraySegment<byte> payload, bool endOfMessage, bool useMask)
		{
			sendBuffer[0] = (byte)opcode;
			if (endOfMessage)
			{
				int num = 0;
				sendBuffer[num] |= 128;
			}
			int num2;
			if (payload.Count <= 125)
			{
				sendBuffer[1] = (byte)payload.Count;
				num2 = 2;
			}
			else if (payload.Count <= 65535)
			{
				sendBuffer[1] = 126;
				sendBuffer[2] = (byte)(payload.Count / 256);
				sendBuffer[3] = (byte)payload.Count;
				num2 = 4;
			}
			else
			{
				sendBuffer[1] = 127;
				int num3 = payload.Count;
				for (int i = 9; i >= 2; i--)
				{
					sendBuffer[i] = (byte)num3;
					num3 /= 256;
				}
				num2 = 10;
			}
			if (useMask)
			{
				int num4 = 1;
				sendBuffer[num4] |= 128;
				ManagedWebSocket.WriteRandomMask(sendBuffer, num2);
			}
			return num2;
		}

		// Token: 0x06002A69 RID: 10857 RVA: 0x00092EDD File Offset: 0x000910DD
		private static void WriteRandomMask(byte[] buffer, int offset)
		{
			ManagedWebSocket.s_random.GetBytes(buffer, offset, 4);
		}

		// Token: 0x06002A6A RID: 10858 RVA: 0x00092EEC File Offset: 0x000910EC
		private async Task<WebSocketReceiveResult> ReceiveAsyncPrivate(ArraySegment<byte> payloadBuffer, CancellationToken cancellationToken)
		{
			WebSocketReceiveResult result;
			using (cancellationToken.Register(delegate(object s)
			{
				((ManagedWebSocket)s).Abort();
			}, this))
			{
				try
				{
					ManagedWebSocket.MessageHeader header;
					for (;;)
					{
						header = this._lastReceiveHeader;
						if (header.PayloadLength == 0L)
						{
							if (this._receiveBufferCount < (this._isServer ? 10 : 14))
							{
								if (this._receiveBufferCount < 2)
								{
									await this.EnsureBufferContainsAsync(2, cancellationToken, true).ConfigureAwait(false);
								}
								long num = (long)(this._receiveBuffer[this._receiveBufferOffset + 1] & 127);
								if (this._isServer || num > 125L)
								{
									await this.EnsureBufferContainsAsync(2 + (this._isServer ? 4 : 0) + ((num <= 125L) ? 0 : ((num == 126L) ? 2 : 8)), cancellationToken, true).ConfigureAwait(false);
								}
							}
							if (!this.TryParseMessageHeaderFromReceiveBuffer(out header))
							{
								await this.CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus.ProtocolError, WebSocketError.Faulted, cancellationToken, null).ConfigureAwait(false);
							}
							this._receivedMaskOffsetOffset = 0;
						}
						if (header.Opcode != ManagedWebSocket.MessageOpcode.Ping && header.Opcode != ManagedWebSocket.MessageOpcode.Pong)
						{
							break;
						}
						await this.HandleReceivedPingPongAsync(header, cancellationToken).ConfigureAwait(false);
					}
					if (header.Opcode == ManagedWebSocket.MessageOpcode.Close)
					{
						result = await this.HandleReceivedCloseAsync(header, cancellationToken).ConfigureAwait(false);
					}
					else
					{
						if (header.Opcode == ManagedWebSocket.MessageOpcode.Continuation)
						{
							header.Opcode = this._lastReceiveHeader.Opcode;
						}
						int bytesToRead = (int)Math.Min((long)payloadBuffer.Count, header.PayloadLength);
						if (bytesToRead == 0)
						{
							this._lastReceiveHeader = header;
							result = new WebSocketReceiveResult(0, (header.Opcode == ManagedWebSocket.MessageOpcode.Text) ? WebSocketMessageType.Text : WebSocketMessageType.Binary, header.PayloadLength == 0L && header.Fin);
						}
						else
						{
							if (this._receiveBufferCount == 0)
							{
								await this.EnsureBufferContainsAsync(1, cancellationToken, false).ConfigureAwait(false);
							}
							int bytesToCopy = Math.Min(bytesToRead, this._receiveBufferCount);
							if (this._isServer)
							{
								this._receivedMaskOffsetOffset = ManagedWebSocket.ApplyMask(this._receiveBuffer, this._receiveBufferOffset, header.Mask, this._receivedMaskOffsetOffset, (long)bytesToCopy);
							}
							Buffer.BlockCopy(this._receiveBuffer, this._receiveBufferOffset, payloadBuffer.Array, payloadBuffer.Offset, bytesToCopy);
							this.ConsumeFromBuffer(bytesToCopy);
							header.PayloadLength -= (long)bytesToCopy;
							if (header.Opcode == ManagedWebSocket.MessageOpcode.Text && !ManagedWebSocket.TryValidateUtf8(new ArraySegment<byte>(payloadBuffer.Array, payloadBuffer.Offset, bytesToCopy), header.Fin, this._utf8TextState))
							{
								await this.CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus.InvalidPayloadData, WebSocketError.Faulted, cancellationToken, null).ConfigureAwait(false);
							}
							this._lastReceiveHeader = header;
							result = new WebSocketReceiveResult(bytesToCopy, (header.Opcode == ManagedWebSocket.MessageOpcode.Text) ? WebSocketMessageType.Text : WebSocketMessageType.Binary, bytesToCopy == 0 || (header.Fin && header.PayloadLength == 0L));
						}
					}
				}
				catch (Exception innerException)
				{
					if (this._state == WebSocketState.Aborted)
					{
						throw new OperationCanceledException("Aborted", innerException);
					}
					throw new WebSocketException(WebSocketError.ConnectionClosedPrematurely, innerException);
				}
			}
			return result;
		}

		// Token: 0x06002A6B RID: 10859 RVA: 0x00092F44 File Offset: 0x00091144
		private async Task<WebSocketReceiveResult> HandleReceivedCloseAsync(ManagedWebSocket.MessageHeader header, CancellationToken cancellationToken)
		{
			object stateUpdateLock = this.StateUpdateLock;
			lock (stateUpdateLock)
			{
				this._receivedCloseFrame = true;
				if (this._state < WebSocketState.CloseReceived)
				{
					this._state = WebSocketState.CloseReceived;
				}
			}
			WebSocketCloseStatus closeStatus = WebSocketCloseStatus.NormalClosure;
			string closeStatusDescription = string.Empty;
			if (header.PayloadLength == 1L)
			{
				await this.CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus.ProtocolError, WebSocketError.Faulted, cancellationToken, null).ConfigureAwait(false);
			}
			else if (header.PayloadLength >= 2L)
			{
				if ((long)this._receiveBufferCount < header.PayloadLength)
				{
					await this.EnsureBufferContainsAsync((int)header.PayloadLength, cancellationToken, true).ConfigureAwait(false);
				}
				if (this._isServer)
				{
					ManagedWebSocket.ApplyMask(this._receiveBuffer, this._receiveBufferOffset, header.Mask, 0, header.PayloadLength);
				}
				closeStatus = (WebSocketCloseStatus)((int)this._receiveBuffer[this._receiveBufferOffset] << 8 | (int)this._receiveBuffer[this._receiveBufferOffset + 1]);
				if (!ManagedWebSocket.IsValidCloseStatus(closeStatus))
				{
					await this.CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus.ProtocolError, WebSocketError.Faulted, cancellationToken, null).ConfigureAwait(false);
				}
				if (header.PayloadLength > 2L)
				{
					int num = 0;
					try
					{
						closeStatusDescription = ManagedWebSocket.s_textEncoding.GetString(this._receiveBuffer, this._receiveBufferOffset + 2, (int)header.PayloadLength - 2);
					}
					catch (DecoderFallbackException stateUpdateLock)
					{
						num = 1;
					}
					if (num == 1)
					{
						await this.CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus.ProtocolError, WebSocketError.Faulted, cancellationToken, (DecoderFallbackException)stateUpdateLock).ConfigureAwait(false);
					}
				}
				this.ConsumeFromBuffer((int)header.PayloadLength);
			}
			this._closeStatus = new WebSocketCloseStatus?(closeStatus);
			this._closeStatusDescription = closeStatusDescription;
			return new WebSocketReceiveResult(0, WebSocketMessageType.Close, true, new WebSocketCloseStatus?(closeStatus), closeStatusDescription);
		}

		// Token: 0x06002A6C RID: 10860 RVA: 0x00092F9C File Offset: 0x0009119C
		private async Task HandleReceivedPingPongAsync(ManagedWebSocket.MessageHeader header, CancellationToken cancellationToken)
		{
			if (header.PayloadLength > 0L && (long)this._receiveBufferCount < header.PayloadLength)
			{
				await this.EnsureBufferContainsAsync((int)header.PayloadLength, cancellationToken, true).ConfigureAwait(false);
			}
			if (header.Opcode == ManagedWebSocket.MessageOpcode.Ping)
			{
				if (this._isServer)
				{
					ManagedWebSocket.ApplyMask(this._receiveBuffer, this._receiveBufferOffset, header.Mask, 0, header.PayloadLength);
				}
				await this.SendFrameAsync(ManagedWebSocket.MessageOpcode.Pong, true, new ArraySegment<byte>(this._receiveBuffer, this._receiveBufferOffset, (int)header.PayloadLength), cancellationToken).ConfigureAwait(false);
			}
			if (header.PayloadLength > 0L)
			{
				this.ConsumeFromBuffer((int)header.PayloadLength);
			}
		}

		// Token: 0x06002A6D RID: 10861 RVA: 0x00092FF1 File Offset: 0x000911F1
		private static bool IsValidCloseStatus(WebSocketCloseStatus closeStatus)
		{
			return closeStatus >= WebSocketCloseStatus.NormalClosure && closeStatus < (WebSocketCloseStatus)5000 && (closeStatus >= (WebSocketCloseStatus)3000 || (closeStatus - WebSocketCloseStatus.NormalClosure <= 3 || closeStatus - WebSocketCloseStatus.InvalidPayloadData <= 4));
		}

		// Token: 0x06002A6E RID: 10862 RVA: 0x00093028 File Offset: 0x00091228
		private async Task CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus closeStatus, WebSocketError error, CancellationToken cancellationToken, Exception innerException = null)
		{
			if (!this._sentCloseFrame)
			{
				await this.CloseOutputAsync(closeStatus, string.Empty, cancellationToken).ConfigureAwait(false);
			}
			this._receiveBufferCount = 0;
			throw new WebSocketException(error, innerException);
		}

		// Token: 0x06002A6F RID: 10863 RVA: 0x00093090 File Offset: 0x00091290
		private bool TryParseMessageHeaderFromReceiveBuffer(out ManagedWebSocket.MessageHeader resultHeader)
		{
			ManagedWebSocket.MessageHeader messageHeader = default(ManagedWebSocket.MessageHeader);
			messageHeader.Fin = ((this._receiveBuffer[this._receiveBufferOffset] & 128) > 0);
			bool flag = (this._receiveBuffer[this._receiveBufferOffset] & 112) > 0;
			messageHeader.Opcode = (ManagedWebSocket.MessageOpcode)(this._receiveBuffer[this._receiveBufferOffset] & 15);
			bool flag2 = (this._receiveBuffer[this._receiveBufferOffset + 1] & 128) > 0;
			messageHeader.PayloadLength = (long)(this._receiveBuffer[this._receiveBufferOffset + 1] & 127);
			this.ConsumeFromBuffer(2);
			if (messageHeader.PayloadLength == 126L)
			{
				messageHeader.PayloadLength = (long)((int)this._receiveBuffer[this._receiveBufferOffset] << 8 | (int)this._receiveBuffer[this._receiveBufferOffset + 1]);
				this.ConsumeFromBuffer(2);
			}
			else if (messageHeader.PayloadLength == 127L)
			{
				messageHeader.PayloadLength = 0L;
				for (int i = 0; i < 8; i++)
				{
					messageHeader.PayloadLength = (messageHeader.PayloadLength << 8 | (long)((ulong)this._receiveBuffer[this._receiveBufferOffset + i]));
				}
				this.ConsumeFromBuffer(8);
			}
			bool flag3 = flag;
			if (flag2)
			{
				if (!this._isServer)
				{
					flag3 = true;
				}
				messageHeader.Mask = ManagedWebSocket.CombineMaskBytes(this._receiveBuffer, this._receiveBufferOffset);
				this.ConsumeFromBuffer(4);
			}
			switch (messageHeader.Opcode)
			{
			case ManagedWebSocket.MessageOpcode.Continuation:
				if (this._lastReceiveHeader.Fin)
				{
					flag3 = true;
					goto IL_1B8;
				}
				goto IL_1B8;
			case ManagedWebSocket.MessageOpcode.Text:
			case ManagedWebSocket.MessageOpcode.Binary:
				if (!this._lastReceiveHeader.Fin)
				{
					flag3 = true;
					goto IL_1B8;
				}
				goto IL_1B8;
			case ManagedWebSocket.MessageOpcode.Close:
			case ManagedWebSocket.MessageOpcode.Ping:
			case ManagedWebSocket.MessageOpcode.Pong:
				if (messageHeader.PayloadLength > 125L || !messageHeader.Fin)
				{
					flag3 = true;
					goto IL_1B8;
				}
				goto IL_1B8;
			}
			flag3 = true;
			IL_1B8:
			resultHeader = messageHeader;
			return !flag3;
		}

		// Token: 0x06002A70 RID: 10864 RVA: 0x00093260 File Offset: 0x00091460
		private async Task CloseAsyncPrivate(WebSocketCloseStatus closeStatus, string statusDescription, CancellationToken cancellationToken)
		{
			if (!this._sentCloseFrame)
			{
				await this.SendCloseFrameAsync(closeStatus, statusDescription, cancellationToken).ConfigureAwait(false);
			}
			byte[] closeBuffer = ArrayPool<byte>.Shared.Rent(139);
			object obj;
			try
			{
				while (!this._receivedCloseFrame)
				{
					obj = this.ReceiveAsyncLock;
					Task<WebSocketReceiveResult> task;
					lock (obj)
					{
						if (this._receivedCloseFrame)
						{
							break;
						}
						task = this._lastReceiveAsync;
						if (task == null || (task.Status == TaskStatus.RanToCompletion && task.Result.MessageType != WebSocketMessageType.Close))
						{
							task = (this._lastReceiveAsync = this.ReceiveAsyncPrivate(new ArraySegment<byte>(closeBuffer), cancellationToken));
						}
					}
					await task.ConfigureAwait(false);
				}
			}
			finally
			{
				ArrayPool<byte>.Shared.Return(closeBuffer, false);
			}
			obj = this.StateUpdateLock;
			lock (obj)
			{
				this.DisposeCore();
				if (this._state < WebSocketState.Closed)
				{
					this._state = WebSocketState.Closed;
				}
			}
		}

		// Token: 0x06002A71 RID: 10865 RVA: 0x000932C0 File Offset: 0x000914C0
		private async Task SendCloseFrameAsync(WebSocketCloseStatus closeStatus, string closeStatusDescription, CancellationToken cancellationToken)
		{
			byte[] buffer = null;
			try
			{
				int num = 2;
				if (string.IsNullOrEmpty(closeStatusDescription))
				{
					buffer = ArrayPool<byte>.Shared.Rent(num);
				}
				else
				{
					num += ManagedWebSocket.s_textEncoding.GetByteCount(closeStatusDescription);
					buffer = ArrayPool<byte>.Shared.Rent(num);
					ManagedWebSocket.s_textEncoding.GetBytes(closeStatusDescription, 0, closeStatusDescription.Length, buffer, 2);
				}
				ushort num2 = (ushort)closeStatus;
				buffer[0] = (byte)(num2 >> 8);
				buffer[1] = (byte)(num2 & 255);
				await this.SendFrameAsync(ManagedWebSocket.MessageOpcode.Close, true, new ArraySegment<byte>(buffer, 0, num), cancellationToken).ConfigureAwait(false);
			}
			finally
			{
				if (buffer != null)
				{
					ArrayPool<byte>.Shared.Return(buffer, false);
				}
			}
			object stateUpdateLock = this.StateUpdateLock;
			lock (stateUpdateLock)
			{
				this._sentCloseFrame = true;
				if (this._state <= WebSocketState.CloseReceived)
				{
					this._state = WebSocketState.CloseSent;
				}
			}
		}

		// Token: 0x06002A72 RID: 10866 RVA: 0x0009331D File Offset: 0x0009151D
		private void ConsumeFromBuffer(int count)
		{
			this._receiveBufferCount -= count;
			this._receiveBufferOffset += count;
		}

		// Token: 0x06002A73 RID: 10867 RVA: 0x0009333C File Offset: 0x0009153C
		private async Task EnsureBufferContainsAsync(int minimumRequiredBytes, CancellationToken cancellationToken, bool throwOnPrematureClosure = true)
		{
			if (this._receiveBufferCount < minimumRequiredBytes)
			{
				if (this._receiveBufferCount > 0)
				{
					Buffer.BlockCopy(this._receiveBuffer, this._receiveBufferOffset, this._receiveBuffer, 0, this._receiveBufferCount);
				}
				this._receiveBufferOffset = 0;
				while (this._receiveBufferCount < minimumRequiredBytes)
				{
					int num = await this._stream.ReadAsync(this._receiveBuffer, this._receiveBufferCount, this._receiveBuffer.Length - this._receiveBufferCount, cancellationToken).ConfigureAwait(false);
					this._receiveBufferCount += num;
					if (num == 0)
					{
						if (this._disposed)
						{
							throw new ObjectDisposedException("WebSocket");
						}
						if (throwOnPrematureClosure)
						{
							throw new WebSocketException(WebSocketError.ConnectionClosedPrematurely);
						}
						break;
					}
				}
			}
		}

		// Token: 0x06002A74 RID: 10868 RVA: 0x00093399 File Offset: 0x00091599
		private void AllocateSendBuffer(int minLength)
		{
			this._sendBuffer = ArrayPool<byte>.Shared.Rent(minLength);
		}

		// Token: 0x06002A75 RID: 10869 RVA: 0x000933AC File Offset: 0x000915AC
		private void ReleaseSendBuffer()
		{
			byte[] sendBuffer = this._sendBuffer;
			if (sendBuffer != null)
			{
				this._sendBuffer = null;
				ArrayPool<byte>.Shared.Return(sendBuffer, false);
			}
		}

		// Token: 0x06002A76 RID: 10870 RVA: 0x000933D6 File Offset: 0x000915D6
		private static int CombineMaskBytes(byte[] buffer, int maskOffset)
		{
			return BitConverter.ToInt32(buffer, maskOffset);
		}

		// Token: 0x06002A77 RID: 10871 RVA: 0x000933DF File Offset: 0x000915DF
		private static int ApplyMask(byte[] toMask, int toMaskOffset, byte[] mask, int maskOffset, int maskOffsetIndex, long count)
		{
			return ManagedWebSocket.ApplyMask(toMask, toMaskOffset, ManagedWebSocket.CombineMaskBytes(mask, maskOffset), maskOffsetIndex, count);
		}

		// Token: 0x06002A78 RID: 10872 RVA: 0x000933F4 File Offset: 0x000915F4
		private unsafe static int ApplyMask(byte[] toMask, int toMaskOffset, int mask, int maskIndex, long count)
		{
			int num = maskIndex * 8;
			int num2 = (int)((uint)mask >> num | (uint)((uint)mask << 32 - num));
			if (count > 0L)
			{
				fixed (byte[] array = toMask)
				{
					byte* ptr;
					if (toMask == null || array.Length == 0)
					{
						ptr = null;
					}
					else
					{
						ptr = &array[0];
					}
					byte* ptr2 = ptr + toMaskOffset;
					if (ptr2 % 4L == null)
					{
						while (count >= 4L)
						{
							count -= 4L;
							*(int*)ptr2 ^= num2;
							ptr2 += 4;
						}
					}
					if (count > 0L)
					{
						byte* ptr3 = (byte*)(&mask);
						byte* ptr4 = ptr2 + count;
						while (ptr2 < ptr4)
						{
							byte* ptr5 = ptr2++;
							*ptr5 ^= ptr3[maskIndex];
							maskIndex = (maskIndex + 1 & 3);
						}
					}
				}
			}
			return maskIndex;
		}

		// Token: 0x06002A79 RID: 10873 RVA: 0x00093493 File Offset: 0x00091693
		private void ThrowIfOperationInProgress(Task operationTask, [CallerMemberName] string methodName = null)
		{
			if (operationTask != null && !operationTask.IsCompleted)
			{
				this.Abort();
				throw new InvalidOperationException(SR.Format("There is already one outstanding '{0}' call for this WebSocket instance. ReceiveAsync and SendAsync can be called simultaneously, but at most one outstanding operation for each of them is allowed at the same time.", methodName));
			}
		}

		// Token: 0x06002A7A RID: 10874 RVA: 0x000934B7 File Offset: 0x000916B7
		private static Exception CreateOperationCanceledException(Exception innerException, CancellationToken cancellationToken = default(CancellationToken))
		{
			return new OperationCanceledException(new OperationCanceledException().Message, innerException, cancellationToken);
		}

		// Token: 0x06002A7B RID: 10875 RVA: 0x000934CC File Offset: 0x000916CC
		private static bool TryValidateUtf8(ArraySegment<byte> arraySegment, bool endOfMessage, ManagedWebSocket.Utf8MessageState state)
		{
			int i = arraySegment.Offset;
			while (i < arraySegment.Offset + arraySegment.Count)
			{
				if (!state.SequenceInProgress)
				{
					state.SequenceInProgress = true;
					byte b = arraySegment.Array[i];
					i++;
					if ((b & 128) == 0)
					{
						state.AdditionalBytesExpected = 0;
						state.CurrentDecodeBits = (int)(b & 127);
						state.ExpectedValueMin = 0;
					}
					else
					{
						if ((b & 192) == 128)
						{
							return false;
						}
						if ((b & 224) == 192)
						{
							state.AdditionalBytesExpected = 1;
							state.CurrentDecodeBits = (int)(b & 31);
							state.ExpectedValueMin = 128;
						}
						else if ((b & 240) == 224)
						{
							state.AdditionalBytesExpected = 2;
							state.CurrentDecodeBits = (int)(b & 15);
							state.ExpectedValueMin = 2048;
						}
						else
						{
							if ((b & 248) != 240)
							{
								return false;
							}
							state.AdditionalBytesExpected = 3;
							state.CurrentDecodeBits = (int)(b & 7);
							state.ExpectedValueMin = 65536;
						}
					}
				}
				while (state.AdditionalBytesExpected > 0 && i < arraySegment.Offset + arraySegment.Count)
				{
					byte b2 = arraySegment.Array[i];
					if ((b2 & 192) != 128)
					{
						return false;
					}
					i++;
					state.AdditionalBytesExpected--;
					state.CurrentDecodeBits = (state.CurrentDecodeBits << 6 | (int)(b2 & 63));
					if (state.AdditionalBytesExpected == 1 && state.CurrentDecodeBits >= 864 && state.CurrentDecodeBits <= 895)
					{
						return false;
					}
					if (state.AdditionalBytesExpected == 2 && state.CurrentDecodeBits >= 272)
					{
						return false;
					}
				}
				if (state.AdditionalBytesExpected == 0)
				{
					state.SequenceInProgress = false;
					if (state.CurrentDecodeBits < state.ExpectedValueMin)
					{
						return false;
					}
				}
			}
			return !endOfMessage || !state.SequenceInProgress;
		}

		// Token: 0x06002A7C RID: 10876 RVA: 0x000936A4 File Offset: 0x000918A4
		// Note: this type is marked as 'beforefieldinit'.
		static ManagedWebSocket()
		{
		}

		// Token: 0x040021E9 RID: 8681
		private static readonly RandomNumberGenerator s_random = RandomNumberGenerator.Create();

		// Token: 0x040021EA RID: 8682
		private static readonly UTF8Encoding s_textEncoding = new UTF8Encoding(false, true);

		// Token: 0x040021EB RID: 8683
		private static readonly WebSocketState[] s_validSendStates = new WebSocketState[]
		{
			WebSocketState.Open,
			WebSocketState.CloseReceived
		};

		// Token: 0x040021EC RID: 8684
		private static readonly WebSocketState[] s_validReceiveStates = new WebSocketState[]
		{
			WebSocketState.Open,
			WebSocketState.CloseSent
		};

		// Token: 0x040021ED RID: 8685
		private static readonly WebSocketState[] s_validCloseOutputStates = new WebSocketState[]
		{
			WebSocketState.Open,
			WebSocketState.CloseReceived
		};

		// Token: 0x040021EE RID: 8686
		private static readonly WebSocketState[] s_validCloseStates = new WebSocketState[]
		{
			WebSocketState.Open,
			WebSocketState.CloseReceived,
			WebSocketState.CloseSent
		};

		// Token: 0x040021EF RID: 8687
		private const int MaxMessageHeaderLength = 14;

		// Token: 0x040021F0 RID: 8688
		private const int MaxControlPayloadLength = 125;

		// Token: 0x040021F1 RID: 8689
		private const int MaskLength = 4;

		// Token: 0x040021F2 RID: 8690
		private readonly Stream _stream;

		// Token: 0x040021F3 RID: 8691
		private readonly bool _isServer;

		// Token: 0x040021F4 RID: 8692
		private readonly string _subprotocol;

		// Token: 0x040021F5 RID: 8693
		private readonly Timer _keepAliveTimer;

		// Token: 0x040021F6 RID: 8694
		private readonly CancellationTokenSource _abortSource = new CancellationTokenSource();

		// Token: 0x040021F7 RID: 8695
		private byte[] _receiveBuffer;

		// Token: 0x040021F8 RID: 8696
		private readonly bool _receiveBufferFromPool;

		// Token: 0x040021F9 RID: 8697
		private readonly ManagedWebSocket.Utf8MessageState _utf8TextState = new ManagedWebSocket.Utf8MessageState();

		// Token: 0x040021FA RID: 8698
		private readonly SemaphoreSlim _sendFrameAsyncLock = new SemaphoreSlim(1, 1);

		// Token: 0x040021FB RID: 8699
		private WebSocketState _state = WebSocketState.Open;

		// Token: 0x040021FC RID: 8700
		private bool _disposed;

		// Token: 0x040021FD RID: 8701
		private bool _sentCloseFrame;

		// Token: 0x040021FE RID: 8702
		private bool _receivedCloseFrame;

		// Token: 0x040021FF RID: 8703
		private WebSocketCloseStatus? _closeStatus;

		// Token: 0x04002200 RID: 8704
		private string _closeStatusDescription;

		// Token: 0x04002201 RID: 8705
		private ManagedWebSocket.MessageHeader _lastReceiveHeader = new ManagedWebSocket.MessageHeader
		{
			Opcode = ManagedWebSocket.MessageOpcode.Text,
			Fin = true
		};

		// Token: 0x04002202 RID: 8706
		private int _receiveBufferOffset;

		// Token: 0x04002203 RID: 8707
		private int _receiveBufferCount;

		// Token: 0x04002204 RID: 8708
		private int _receivedMaskOffsetOffset;

		// Token: 0x04002205 RID: 8709
		private byte[] _sendBuffer;

		// Token: 0x04002206 RID: 8710
		private bool _lastSendWasFragment;

		// Token: 0x04002207 RID: 8711
		private Task _lastSendAsync;

		// Token: 0x04002208 RID: 8712
		private Task<WebSocketReceiveResult> _lastReceiveAsync;

		// Token: 0x0200053E RID: 1342
		private sealed class Utf8MessageState
		{
			// Token: 0x06002A7D RID: 10877 RVA: 0x0000232F File Offset: 0x0000052F
			public Utf8MessageState()
			{
			}

			// Token: 0x04002209 RID: 8713
			internal bool SequenceInProgress;

			// Token: 0x0400220A RID: 8714
			internal int AdditionalBytesExpected;

			// Token: 0x0400220B RID: 8715
			internal int ExpectedValueMin;

			// Token: 0x0400220C RID: 8716
			internal int CurrentDecodeBits;
		}

		// Token: 0x0200053F RID: 1343
		private enum MessageOpcode : byte
		{
			// Token: 0x0400220E RID: 8718
			Continuation,
			// Token: 0x0400220F RID: 8719
			Text,
			// Token: 0x04002210 RID: 8720
			Binary,
			// Token: 0x04002211 RID: 8721
			Close = 8,
			// Token: 0x04002212 RID: 8722
			Ping,
			// Token: 0x04002213 RID: 8723
			Pong
		}

		// Token: 0x02000540 RID: 1344
		[StructLayout(LayoutKind.Auto)]
		private struct MessageHeader
		{
			// Token: 0x04002214 RID: 8724
			internal ManagedWebSocket.MessageOpcode Opcode;

			// Token: 0x04002215 RID: 8725
			internal bool Fin;

			// Token: 0x04002216 RID: 8726
			internal long PayloadLength;

			// Token: 0x04002217 RID: 8727
			internal int Mask;
		}

		// Token: 0x02000541 RID: 1345
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06002A7E RID: 10878 RVA: 0x00093716 File Offset: 0x00091916
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06002A7F RID: 10879 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c()
			{
			}

			// Token: 0x06002A80 RID: 10880 RVA: 0x00093724 File Offset: 0x00091924
			internal void <.ctor>b__37_0(object s)
			{
				ManagedWebSocket managedWebSocket = (ManagedWebSocket)s;
				object stateUpdateLock = managedWebSocket.StateUpdateLock;
				lock (stateUpdateLock)
				{
					WebSocketState state = managedWebSocket._state;
					if (state != WebSocketState.Closed && state != WebSocketState.Aborted)
					{
						managedWebSocket._state = ((state != WebSocketState.None && state != WebSocketState.Connecting) ? WebSocketState.Aborted : WebSocketState.Closed);
					}
				}
			}

			// Token: 0x06002A81 RID: 10881 RVA: 0x00093788 File Offset: 0x00091988
			internal void <.ctor>b__37_1(object s)
			{
				((ManagedWebSocket)s).SendKeepAliveFrameAsync();
			}

			// Token: 0x06002A82 RID: 10882 RVA: 0x00093798 File Offset: 0x00091998
			internal void <SendFrameLockAcquiredNonCancelableAsync>b__54_0(Task t, object s)
			{
				ManagedWebSocket managedWebSocket = (ManagedWebSocket)s;
				managedWebSocket._sendFrameAsyncLock.Release();
				managedWebSocket.ReleaseSendBuffer();
				try
				{
					t.GetAwaiter().GetResult();
				}
				catch (Exception innerException)
				{
					throw (managedWebSocket._state == WebSocketState.Aborted) ? ManagedWebSocket.CreateOperationCanceledException(innerException, default(CancellationToken)) : new WebSocketException(WebSocketError.ConnectionClosedPrematurely, innerException);
				}
			}

			// Token: 0x06002A83 RID: 10883 RVA: 0x00093804 File Offset: 0x00091A04
			internal void <SendFrameFallbackAsync>b__55_0(object s)
			{
				((ManagedWebSocket)s).Abort();
			}

			// Token: 0x06002A84 RID: 10884 RVA: 0x00093811 File Offset: 0x00091A11
			internal void <SendKeepAliveFrameAsync>b__57_0(Task p)
			{
				AggregateException exception = p.Exception;
			}

			// Token: 0x06002A85 RID: 10885 RVA: 0x00093804 File Offset: 0x00091A04
			internal void <ReceiveAsyncPrivate>b__60_0(object s)
			{
				((ManagedWebSocket)s).Abort();
			}

			// Token: 0x04002218 RID: 8728
			public static readonly ManagedWebSocket.<>c <>9 = new ManagedWebSocket.<>c();

			// Token: 0x04002219 RID: 8729
			public static Action<object> <>9__37_0;

			// Token: 0x0400221A RID: 8730
			public static TimerCallback <>9__37_1;

			// Token: 0x0400221B RID: 8731
			public static Action<Task, object> <>9__54_0;

			// Token: 0x0400221C RID: 8732
			public static Action<object> <>9__55_0;

			// Token: 0x0400221D RID: 8733
			public static Action<Task> <>9__57_0;

			// Token: 0x0400221E RID: 8734
			public static Action<object> <>9__60_0;
		}

		// Token: 0x02000542 RID: 1346
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SendFrameFallbackAsync>d__55 : IAsyncStateMachine
		{
			// Token: 0x06002A86 RID: 10886 RVA: 0x0009381C File Offset: 0x00091A1C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ManagedWebSocket managedWebSocket = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num == 1)
						{
							goto IL_7E;
						}
						configuredTaskAwaiter = managedWebSocket._sendFrameAsyncLock.WaitAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<SendFrameFallbackAsync>d__55>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					configuredTaskAwaiter.GetResult();
					IL_7E:
					try
					{
						int count;
						if (num != 1)
						{
							count = managedWebSocket.WriteFrameToSendBuffer(opcode, endOfMessage, payloadBuffer);
							cancellationTokenRegistration = cancellationToken.Register(new Action<object>(ManagedWebSocket.<>c.<>9.<SendFrameFallbackAsync>b__55_0), managedWebSocket);
						}
						try
						{
							if (num != 1)
							{
								configuredTaskAwaiter = managedWebSocket._stream.WriteAsync(managedWebSocket._sendBuffer, 0, count, cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num = (num2 = 1);
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<SendFrameFallbackAsync>d__55>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
							else
							{
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num = (num2 = -1);
							}
							configuredTaskAwaiter.GetResult();
						}
						finally
						{
							if (num < 0)
							{
								((IDisposable)cancellationTokenRegistration).Dispose();
							}
						}
						cancellationTokenRegistration = default(CancellationTokenRegistration);
					}
					catch (Exception innerException)
					{
						throw (managedWebSocket._state == WebSocketState.Aborted) ? ManagedWebSocket.CreateOperationCanceledException(innerException, cancellationToken) : new WebSocketException(WebSocketError.ConnectionClosedPrematurely, innerException);
					}
					finally
					{
						if (num < 0)
						{
							managedWebSocket._sendFrameAsyncLock.Release();
							managedWebSocket.ReleaseSendBuffer();
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06002A87 RID: 10887 RVA: 0x00093A68 File Offset: 0x00091C68
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400221F RID: 8735
			public int <>1__state;

			// Token: 0x04002220 RID: 8736
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04002221 RID: 8737
			public ManagedWebSocket <>4__this;

			// Token: 0x04002222 RID: 8738
			public ManagedWebSocket.MessageOpcode opcode;

			// Token: 0x04002223 RID: 8739
			public bool endOfMessage;

			// Token: 0x04002224 RID: 8740
			public ArraySegment<byte> payloadBuffer;

			// Token: 0x04002225 RID: 8741
			public CancellationToken cancellationToken;

			// Token: 0x04002226 RID: 8742
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04002227 RID: 8743
			private CancellationTokenRegistration <>7__wrap1;
		}

		// Token: 0x02000543 RID: 1347
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReceiveAsyncPrivate>d__60 : IAsyncStateMachine
		{
			// Token: 0x06002A88 RID: 10888 RVA: 0x00093A78 File Offset: 0x00091C78
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ManagedWebSocket managedWebSocket = this;
				WebSocketReceiveResult result;
				try
				{
					if (num > 6)
					{
						registration = cancellationToken.Register(new Action<object>(ManagedWebSocket.<>c.<>9.<ReceiveAsyncPrivate>b__60_0), managedWebSocket);
					}
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						switch (num)
						{
						case 0:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							break;
						}
						case 1:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_1C7;
						}
						case 2:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_24A;
						}
						case 3:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_2E3;
						}
						case 4:
						{
							ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_36A;
						}
						case 5:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_47B;
						}
						case 6:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_5D7;
						}
						default:
							IL_66:
							header = managedWebSocket._lastReceiveHeader;
							if (header.PayloadLength != 0L)
							{
								goto IL_258;
							}
							if (managedWebSocket._receiveBufferCount >= (managedWebSocket._isServer ? 10 : 14))
							{
								goto IL_1CE;
							}
							if (managedWebSocket._receiveBufferCount >= 2)
							{
								goto IL_114;
							}
							configuredTaskAwaiter = managedWebSocket.EnsureBufferContainsAsync(2, cancellationToken, true).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 0);
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<ReceiveAsyncPrivate>d__60>(ref configuredTaskAwaiter, ref this);
								return;
							}
							break;
						}
						configuredTaskAwaiter.GetResult();
						IL_114:
						long num3 = (long)(managedWebSocket._receiveBuffer[managedWebSocket._receiveBufferOffset + 1] & 127);
						if (!managedWebSocket._isServer && num3 <= 125L)
						{
							goto IL_1CE;
						}
						int minimumRequiredBytes = 2 + (managedWebSocket._isServer ? 4 : 0) + ((num3 <= 125L) ? 0 : ((num3 == 126L) ? 2 : 8));
						configuredTaskAwaiter = managedWebSocket.EnsureBufferContainsAsync(minimumRequiredBytes, cancellationToken, true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 1);
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<ReceiveAsyncPrivate>d__60>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_1C7:
						configuredTaskAwaiter.GetResult();
						IL_1CE:
						if (managedWebSocket.TryParseMessageHeaderFromReceiveBuffer(out header))
						{
							goto IL_251;
						}
						configuredTaskAwaiter = managedWebSocket.CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus.ProtocolError, WebSocketError.Faulted, cancellationToken, null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 2);
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<ReceiveAsyncPrivate>d__60>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_24A:
						configuredTaskAwaiter.GetResult();
						IL_251:
						managedWebSocket._receivedMaskOffsetOffset = 0;
						IL_258:
						if (header.Opcode == ManagedWebSocket.MessageOpcode.Ping || header.Opcode == ManagedWebSocket.MessageOpcode.Pong)
						{
							configuredTaskAwaiter = managedWebSocket.HandleReceivedPingPongAsync(header, cancellationToken).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 3);
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<ReceiveAsyncPrivate>d__60>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else if (header.Opcode == ManagedWebSocket.MessageOpcode.Close)
						{
							configuredTaskAwaiter3 = managedWebSocket.HandleReceivedCloseAsync(header, cancellationToken).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num = (num2 = 4);
								ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter, ManagedWebSocket.<ReceiveAsyncPrivate>d__60>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_36A;
						}
						else
						{
							if (header.Opcode == ManagedWebSocket.MessageOpcode.Continuation)
							{
								header.Opcode = managedWebSocket._lastReceiveHeader.Opcode;
							}
							bytesToRead = (int)Math.Min((long)payloadBuffer.Count, header.PayloadLength);
							if (bytesToRead == 0)
							{
								managedWebSocket._lastReceiveHeader = header;
								result = new WebSocketReceiveResult(0, (header.Opcode == ManagedWebSocket.MessageOpcode.Text) ? WebSocketMessageType.Text : WebSocketMessageType.Binary, header.PayloadLength == 0L && header.Fin);
								goto IL_67E;
							}
							if (managedWebSocket._receiveBufferCount != 0)
							{
								goto IL_482;
							}
							configuredTaskAwaiter = managedWebSocket.EnsureBufferContainsAsync(1, cancellationToken, false).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 5);
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<ReceiveAsyncPrivate>d__60>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_47B;
						}
						IL_2E3:
						configuredTaskAwaiter.GetResult();
						goto IL_66;
						IL_36A:
						result = configuredTaskAwaiter3.GetResult();
						goto IL_67E;
						IL_47B:
						configuredTaskAwaiter.GetResult();
						IL_482:
						bytesToCopy = Math.Min(bytesToRead, managedWebSocket._receiveBufferCount);
						if (managedWebSocket._isServer)
						{
							managedWebSocket._receivedMaskOffsetOffset = ManagedWebSocket.ApplyMask(managedWebSocket._receiveBuffer, managedWebSocket._receiveBufferOffset, header.Mask, managedWebSocket._receivedMaskOffsetOffset, (long)bytesToCopy);
						}
						Buffer.BlockCopy(managedWebSocket._receiveBuffer, managedWebSocket._receiveBufferOffset, payloadBuffer.Array, payloadBuffer.Offset, bytesToCopy);
						managedWebSocket.ConsumeFromBuffer(bytesToCopy);
						header.PayloadLength -= (long)bytesToCopy;
						if (header.Opcode != ManagedWebSocket.MessageOpcode.Text || ManagedWebSocket.TryValidateUtf8(new ArraySegment<byte>(payloadBuffer.Array, payloadBuffer.Offset, bytesToCopy), header.Fin, managedWebSocket._utf8TextState))
						{
							goto IL_5DE;
						}
						configuredTaskAwaiter = managedWebSocket.CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus.InvalidPayloadData, WebSocketError.Faulted, cancellationToken, null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 6);
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<ReceiveAsyncPrivate>d__60>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_5D7:
						configuredTaskAwaiter.GetResult();
						IL_5DE:
						managedWebSocket._lastReceiveHeader = header;
						result = new WebSocketReceiveResult(bytesToCopy, (header.Opcode == ManagedWebSocket.MessageOpcode.Text) ? WebSocketMessageType.Text : WebSocketMessageType.Binary, bytesToCopy == 0 || (header.Fin && header.PayloadLength == 0L));
					}
					catch (Exception innerException)
					{
						if (managedWebSocket._state == WebSocketState.Aborted)
						{
							throw new OperationCanceledException("Aborted", innerException);
						}
						throw new WebSocketException(WebSocketError.ConnectionClosedPrematurely, innerException);
					}
					finally
					{
						if (num < 0)
						{
							registration.Dispose();
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_67E:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06002A89 RID: 10889 RVA: 0x00094164 File Offset: 0x00092364
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04002228 RID: 8744
			public int <>1__state;

			// Token: 0x04002229 RID: 8745
			public AsyncTaskMethodBuilder<WebSocketReceiveResult> <>t__builder;

			// Token: 0x0400222A RID: 8746
			public CancellationToken cancellationToken;

			// Token: 0x0400222B RID: 8747
			public ManagedWebSocket <>4__this;

			// Token: 0x0400222C RID: 8748
			private ManagedWebSocket.MessageHeader <header>5__1;

			// Token: 0x0400222D RID: 8749
			public ArraySegment<byte> payloadBuffer;

			// Token: 0x0400222E RID: 8750
			private int <bytesToRead>5__2;

			// Token: 0x0400222F RID: 8751
			private int <bytesToCopy>5__3;

			// Token: 0x04002230 RID: 8752
			private CancellationTokenRegistration <registration>5__4;

			// Token: 0x04002231 RID: 8753
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04002232 RID: 8754
			private ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000544 RID: 1348
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <HandleReceivedCloseAsync>d__61 : IAsyncStateMachine
		{
			// Token: 0x06002A8A RID: 10890 RVA: 0x00094174 File Offset: 0x00092374
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ManagedWebSocket managedWebSocket = this;
				WebSocketReceiveResult result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					object stateUpdateLock;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						goto IL_197;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						goto IL_26F;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						goto IL_33B;
					}
					default:
					{
						stateUpdateLock = managedWebSocket.StateUpdateLock;
						bool flag = false;
						try
						{
							Monitor.Enter(stateUpdateLock, ref flag);
							managedWebSocket._receivedCloseFrame = true;
							if (managedWebSocket._state < WebSocketState.CloseReceived)
							{
								managedWebSocket._state = WebSocketState.CloseReceived;
							}
						}
						finally
						{
							if (num < 0 && flag)
							{
								Monitor.Exit(stateUpdateLock);
							}
						}
						closeStatus = WebSocketCloseStatus.NormalClosure;
						closeStatusDescription = string.Empty;
						if (header.PayloadLength == 1L)
						{
							configuredTaskAwaiter = managedWebSocket.CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus.ProtocolError, WebSocketError.Faulted, cancellationToken, null).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 0);
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<HandleReceivedCloseAsync>d__61>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							if (header.PayloadLength < 2L)
							{
								goto IL_354;
							}
							if ((long)managedWebSocket._receiveBufferCount >= header.PayloadLength)
							{
								goto IL_19E;
							}
							configuredTaskAwaiter = managedWebSocket.EnsureBufferContainsAsync((int)header.PayloadLength, cancellationToken, true).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 1);
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<HandleReceivedCloseAsync>d__61>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_197;
						}
						break;
					}
					}
					configuredTaskAwaiter.GetResult();
					goto IL_354;
					IL_197:
					configuredTaskAwaiter.GetResult();
					IL_19E:
					if (managedWebSocket._isServer)
					{
						ManagedWebSocket.ApplyMask(managedWebSocket._receiveBuffer, managedWebSocket._receiveBufferOffset, header.Mask, 0, header.PayloadLength);
					}
					closeStatus = (WebSocketCloseStatus)((int)managedWebSocket._receiveBuffer[managedWebSocket._receiveBufferOffset] << 8 | (int)managedWebSocket._receiveBuffer[managedWebSocket._receiveBufferOffset + 1]);
					if (ManagedWebSocket.IsValidCloseStatus(closeStatus))
					{
						goto IL_276;
					}
					configuredTaskAwaiter = managedWebSocket.CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus.ProtocolError, WebSocketError.Faulted, cancellationToken, null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num = (num2 = 2);
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<HandleReceivedCloseAsync>d__61>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_26F:
					configuredTaskAwaiter.GetResult();
					IL_276:
					if (header.PayloadLength <= 2L)
					{
						goto IL_342;
					}
					int num3 = 0;
					try
					{
						closeStatusDescription = ManagedWebSocket.s_textEncoding.GetString(managedWebSocket._receiveBuffer, managedWebSocket._receiveBufferOffset + 2, (int)header.PayloadLength - 2);
					}
					catch (DecoderFallbackException stateUpdateLock)
					{
						num3 = 1;
					}
					if (num3 != 1)
					{
						goto IL_342;
					}
					DecoderFallbackException innerException = (DecoderFallbackException)stateUpdateLock;
					configuredTaskAwaiter = managedWebSocket.CloseWithReceiveErrorAndThrowAsync(WebSocketCloseStatus.ProtocolError, WebSocketError.Faulted, cancellationToken, innerException).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num = (num2 = 3);
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<HandleReceivedCloseAsync>d__61>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_33B:
					configuredTaskAwaiter.GetResult();
					IL_342:
					managedWebSocket.ConsumeFromBuffer((int)header.PayloadLength);
					IL_354:
					managedWebSocket._closeStatus = new WebSocketCloseStatus?(closeStatus);
					managedWebSocket._closeStatusDescription = closeStatusDescription;
					result = new WebSocketReceiveResult(0, WebSocketMessageType.Close, true, new WebSocketCloseStatus?(closeStatus), closeStatusDescription);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06002A8B RID: 10891 RVA: 0x00094588 File Offset: 0x00092788
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04002233 RID: 8755
			public int <>1__state;

			// Token: 0x04002234 RID: 8756
			public AsyncTaskMethodBuilder<WebSocketReceiveResult> <>t__builder;

			// Token: 0x04002235 RID: 8757
			public ManagedWebSocket <>4__this;

			// Token: 0x04002236 RID: 8758
			public ManagedWebSocket.MessageHeader header;

			// Token: 0x04002237 RID: 8759
			public CancellationToken cancellationToken;

			// Token: 0x04002238 RID: 8760
			private WebSocketCloseStatus <closeStatus>5__1;

			// Token: 0x04002239 RID: 8761
			private string <closeStatusDescription>5__2;

			// Token: 0x0400223A RID: 8762
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000545 RID: 1349
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <HandleReceivedPingPongAsync>d__62 : IAsyncStateMachine
		{
			// Token: 0x06002A8C RID: 10892 RVA: 0x00094598 File Offset: 0x00092798
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ManagedWebSocket managedWebSocket = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_17B;
						}
						if (header.PayloadLength <= 0L || (long)managedWebSocket._receiveBufferCount >= header.PayloadLength)
						{
							goto IL_B8;
						}
						configuredTaskAwaiter = managedWebSocket.EnsureBufferContainsAsync((int)header.PayloadLength, cancellationToken, true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<HandleReceivedPingPongAsync>d__62>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_B8:
					if (header.Opcode != ManagedWebSocket.MessageOpcode.Ping)
					{
						goto IL_182;
					}
					if (managedWebSocket._isServer)
					{
						ManagedWebSocket.ApplyMask(managedWebSocket._receiveBuffer, managedWebSocket._receiveBufferOffset, header.Mask, 0, header.PayloadLength);
					}
					configuredTaskAwaiter = managedWebSocket.SendFrameAsync(ManagedWebSocket.MessageOpcode.Pong, true, new ArraySegment<byte>(managedWebSocket._receiveBuffer, managedWebSocket._receiveBufferOffset, (int)header.PayloadLength), cancellationToken).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<HandleReceivedPingPongAsync>d__62>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_17B:
					configuredTaskAwaiter.GetResult();
					IL_182:
					if (header.PayloadLength > 0L)
					{
						managedWebSocket.ConsumeFromBuffer((int)header.PayloadLength);
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06002A8D RID: 10893 RVA: 0x00094794 File Offset: 0x00092994
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400223B RID: 8763
			public int <>1__state;

			// Token: 0x0400223C RID: 8764
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400223D RID: 8765
			public ManagedWebSocket.MessageHeader header;

			// Token: 0x0400223E RID: 8766
			public ManagedWebSocket <>4__this;

			// Token: 0x0400223F RID: 8767
			public CancellationToken cancellationToken;

			// Token: 0x04002240 RID: 8768
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000546 RID: 1350
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <CloseWithReceiveErrorAndThrowAsync>d__64 : IAsyncStateMachine
		{
			// Token: 0x06002A8E RID: 10894 RVA: 0x000947A4 File Offset: 0x000929A4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ManagedWebSocket managedWebSocket = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (managedWebSocket._sentCloseFrame)
						{
							goto IL_8B;
						}
						configuredTaskAwaiter = managedWebSocket.CloseOutputAsync(closeStatus, string.Empty, cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<CloseWithReceiveErrorAndThrowAsync>d__64>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_8B:
					managedWebSocket._receiveBufferCount = 0;
					throw new WebSocketException(error, innerException);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
				}
			}

			// Token: 0x06002A8F RID: 10895 RVA: 0x00094880 File Offset: 0x00092A80
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04002241 RID: 8769
			public int <>1__state;

			// Token: 0x04002242 RID: 8770
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04002243 RID: 8771
			public ManagedWebSocket <>4__this;

			// Token: 0x04002244 RID: 8772
			public WebSocketCloseStatus closeStatus;

			// Token: 0x04002245 RID: 8773
			public CancellationToken cancellationToken;

			// Token: 0x04002246 RID: 8774
			public WebSocketError error;

			// Token: 0x04002247 RID: 8775
			public Exception innerException;

			// Token: 0x04002248 RID: 8776
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000547 RID: 1351
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <CloseAsyncPrivate>d__66 : IAsyncStateMachine
		{
			// Token: 0x06002A90 RID: 10896 RVA: 0x00094890 File Offset: 0x00092A90
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ManagedWebSocket managedWebSocket = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							goto IL_AB;
						}
						if (managedWebSocket._sentCloseFrame)
						{
							goto IL_96;
						}
						configuredTaskAwaiter = managedWebSocket.SendCloseFrameAsync(closeStatus, statusDescription, cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<CloseAsyncPrivate>d__66>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					configuredTaskAwaiter.GetResult();
					IL_96:
					closeBuffer = ArrayPool<byte>.Shared.Rent(139);
					IL_AB:
					object obj;
					bool flag;
					try
					{
						if (num != 1)
						{
							goto IL_195;
						}
						ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						IL_18D:
						configuredTaskAwaiter3.GetResult();
						IL_195:
						if (!managedWebSocket._receivedCloseFrame)
						{
							obj = managedWebSocket.ReceiveAsyncLock;
							flag = false;
							Task<WebSocketReceiveResult> task;
							try
							{
								Monitor.Enter(obj, ref flag);
								if (managedWebSocket._receivedCloseFrame)
								{
									goto IL_1A0;
								}
								task = managedWebSocket._lastReceiveAsync;
								if (task == null || (task.Status == TaskStatus.RanToCompletion && task.Result.MessageType != WebSocketMessageType.Close))
								{
									task = (managedWebSocket._lastReceiveAsync = managedWebSocket.ReceiveAsyncPrivate(new ArraySegment<byte>(closeBuffer), cancellationToken));
								}
							}
							finally
							{
								if (num < 0 && flag)
								{
									Monitor.Exit(obj);
								}
							}
							configuredTaskAwaiter3 = task.ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num = (num2 = 1);
								configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter, ManagedWebSocket.<CloseAsyncPrivate>d__66>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_18D;
						}
						IL_1A0:;
					}
					finally
					{
						if (num < 0)
						{
							ArrayPool<byte>.Shared.Return(closeBuffer, false);
						}
					}
					obj = managedWebSocket.StateUpdateLock;
					flag = false;
					try
					{
						Monitor.Enter(obj, ref flag);
						managedWebSocket.DisposeCore();
						if (managedWebSocket._state < WebSocketState.Closed)
						{
							managedWebSocket._state = WebSocketState.Closed;
						}
					}
					finally
					{
						if (num < 0 && flag)
						{
							Monitor.Exit(obj);
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06002A91 RID: 10897 RVA: 0x00094B24 File Offset: 0x00092D24
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04002249 RID: 8777
			public int <>1__state;

			// Token: 0x0400224A RID: 8778
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400224B RID: 8779
			public ManagedWebSocket <>4__this;

			// Token: 0x0400224C RID: 8780
			public WebSocketCloseStatus closeStatus;

			// Token: 0x0400224D RID: 8781
			public string statusDescription;

			// Token: 0x0400224E RID: 8782
			public CancellationToken cancellationToken;

			// Token: 0x0400224F RID: 8783
			private byte[] <closeBuffer>5__1;

			// Token: 0x04002250 RID: 8784
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04002251 RID: 8785
			private ConfiguredTaskAwaitable<WebSocketReceiveResult>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000548 RID: 1352
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SendCloseFrameAsync>d__67 : IAsyncStateMachine
		{
			// Token: 0x06002A92 RID: 10898 RVA: 0x00094B34 File Offset: 0x00092D34
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ManagedWebSocket managedWebSocket = this;
				try
				{
					if (num != 0)
					{
						buffer = null;
					}
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							int num3 = 2;
							if (string.IsNullOrEmpty(closeStatusDescription))
							{
								buffer = ArrayPool<byte>.Shared.Rent(num3);
							}
							else
							{
								num3 += ManagedWebSocket.s_textEncoding.GetByteCount(closeStatusDescription);
								buffer = ArrayPool<byte>.Shared.Rent(num3);
								ManagedWebSocket.s_textEncoding.GetBytes(closeStatusDescription, 0, closeStatusDescription.Length, buffer, 2);
							}
							ushort num4 = (ushort)closeStatus;
							buffer[0] = (byte)(num4 >> 8);
							buffer[1] = (byte)(num4 & 255);
							configuredTaskAwaiter = managedWebSocket.SendFrameAsync(ManagedWebSocket.MessageOpcode.Close, true, new ArraySegment<byte>(buffer, 0, num3), cancellationToken).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 0);
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, ManagedWebSocket.<SendCloseFrameAsync>d__67>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						configuredTaskAwaiter.GetResult();
					}
					finally
					{
						if (num < 0 && buffer != null)
						{
							ArrayPool<byte>.Shared.Return(buffer, false);
						}
					}
					object stateUpdateLock = managedWebSocket.StateUpdateLock;
					bool flag = false;
					try
					{
						Monitor.Enter(stateUpdateLock, ref flag);
						managedWebSocket._sentCloseFrame = true;
						if (managedWebSocket._state <= WebSocketState.CloseReceived)
						{
							managedWebSocket._state = WebSocketState.CloseSent;
						}
					}
					finally
					{
						if (num < 0 && flag)
						{
							Monitor.Exit(stateUpdateLock);
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06002A93 RID: 10899 RVA: 0x00094D44 File Offset: 0x00092F44
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04002252 RID: 8786
			public int <>1__state;

			// Token: 0x04002253 RID: 8787
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04002254 RID: 8788
			public string closeStatusDescription;

			// Token: 0x04002255 RID: 8789
			public WebSocketCloseStatus closeStatus;

			// Token: 0x04002256 RID: 8790
			public ManagedWebSocket <>4__this;

			// Token: 0x04002257 RID: 8791
			public CancellationToken cancellationToken;

			// Token: 0x04002258 RID: 8792
			private byte[] <buffer>5__1;

			// Token: 0x04002259 RID: 8793
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000549 RID: 1353
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <EnsureBufferContainsAsync>d__69 : IAsyncStateMachine
		{
			// Token: 0x06002A94 RID: 10900 RVA: 0x00094D54 File Offset: 0x00092F54
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ManagedWebSocket managedWebSocket = this;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (managedWebSocket._receiveBufferCount < minimumRequiredBytes)
						{
							if (managedWebSocket._receiveBufferCount > 0)
							{
								Buffer.BlockCopy(managedWebSocket._receiveBuffer, managedWebSocket._receiveBufferOffset, managedWebSocket._receiveBuffer, 0, managedWebSocket._receiveBufferCount);
							}
							managedWebSocket._receiveBufferOffset = 0;
							goto IL_117;
						}
						goto IL_128;
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IL_DC:
					int result = configuredTaskAwaiter.GetResult();
					managedWebSocket._receiveBufferCount += result;
					if (result == 0)
					{
						if (managedWebSocket._disposed)
						{
							throw new ObjectDisposedException("WebSocket");
						}
						if (throwOnPrematureClosure)
						{
							throw new WebSocketException(WebSocketError.ConnectionClosedPrematurely);
						}
						goto IL_128;
					}
					IL_117:
					if (managedWebSocket._receiveBufferCount < minimumRequiredBytes)
					{
						configuredTaskAwaiter = managedWebSocket._stream.ReadAsync(managedWebSocket._receiveBuffer, managedWebSocket._receiveBufferCount, managedWebSocket._receiveBuffer.Length - managedWebSocket._receiveBufferCount, cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ManagedWebSocket.<EnsureBufferContainsAsync>d__69>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_DC;
					}
					IL_128:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06002A95 RID: 10901 RVA: 0x00094ED4 File Offset: 0x000930D4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400225A RID: 8794
			public int <>1__state;

			// Token: 0x0400225B RID: 8795
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400225C RID: 8796
			public ManagedWebSocket <>4__this;

			// Token: 0x0400225D RID: 8797
			public int minimumRequiredBytes;

			// Token: 0x0400225E RID: 8798
			public CancellationToken cancellationToken;

			// Token: 0x0400225F RID: 8799
			public bool throwOnPrematureClosure;

			// Token: 0x04002260 RID: 8800
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
