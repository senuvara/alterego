﻿using System;

namespace System.Net.WebSockets
{
	/// <summary> Defines the different states a WebSockets instance can be in.</summary>
	// Token: 0x0200055E RID: 1374
	public enum WebSocketState
	{
		/// <summary>Reserved for future use.</summary>
		// Token: 0x040022E9 RID: 8937
		None,
		/// <summary>The connection is negotiating the handshake with the remote endpoint.</summary>
		// Token: 0x040022EA RID: 8938
		Connecting,
		/// <summary>The initial state after the HTTP handshake has been completed.</summary>
		// Token: 0x040022EB RID: 8939
		Open,
		/// <summary>A close message was sent to the remote endpoint.</summary>
		// Token: 0x040022EC RID: 8940
		CloseSent,
		/// <summary>A close message was received from the remote endpoint.</summary>
		// Token: 0x040022ED RID: 8941
		CloseReceived,
		/// <summary>Indicates the WebSocket close handshake completed gracefully.</summary>
		// Token: 0x040022EE RID: 8942
		Closed,
		/// <summary>Reserved for future use.</summary>
		// Token: 0x040022EF RID: 8943
		Aborted
	}
}
