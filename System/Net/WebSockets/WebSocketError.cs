﻿using System;

namespace System.Net.WebSockets
{
	/// <summary>Contains the list of possible WebSocket errors.</summary>
	// Token: 0x0200055A RID: 1370
	public enum WebSocketError
	{
		/// <summary>Indicates that there was no native error information for the exception.</summary>
		// Token: 0x040022D4 RID: 8916
		Success,
		/// <summary>Indicates that a WebSocket frame with an unknown opcode was received.</summary>
		// Token: 0x040022D5 RID: 8917
		InvalidMessageType,
		/// <summary>Indicates a general error.</summary>
		// Token: 0x040022D6 RID: 8918
		Faulted,
		/// <summary>Indicates that an unknown native error occurred.</summary>
		// Token: 0x040022D7 RID: 8919
		NativeError,
		/// <summary>Indicates that the incoming request was not a valid websocket request.</summary>
		// Token: 0x040022D8 RID: 8920
		NotAWebSocket,
		/// <summary>Indicates that the client requested an unsupported version of the WebSocket protocol.</summary>
		// Token: 0x040022D9 RID: 8921
		UnsupportedVersion,
		/// <summary>Indicates that the client requested an unsupported WebSocket subprotocol.</summary>
		// Token: 0x040022DA RID: 8922
		UnsupportedProtocol,
		/// <summary>Indicates an error occurred when parsing the HTTP headers during the opening handshake.</summary>
		// Token: 0x040022DB RID: 8923
		HeaderError,
		/// <summary>Indicates that the connection was terminated unexpectedly.</summary>
		// Token: 0x040022DC RID: 8924
		ConnectionClosedPrematurely,
		/// <summary>Indicates the WebSocket is an invalid state for the given operation (such as being closed or aborted).</summary>
		// Token: 0x040022DD RID: 8925
		InvalidState
	}
}
