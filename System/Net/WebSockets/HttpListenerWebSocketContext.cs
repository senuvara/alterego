﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Principal;
using Unity;

namespace System.Net.WebSockets
{
	/// <summary>Provides access to information received by the <see cref="T:System.Net.HttpListener" /> class when accepting WebSocket connections.</summary>
	// Token: 0x0200054B RID: 1355
	public class HttpListenerWebSocketContext : WebSocketContext
	{
		// Token: 0x06002A9C RID: 10908 RVA: 0x0009516C File Offset: 0x0009336C
		internal HttpListenerWebSocketContext(Uri requestUri, NameValueCollection headers, CookieCollection cookieCollection, IPrincipal user, bool isAuthenticated, bool isLocal, bool isSecureConnection, string origin, IEnumerable<string> secWebSocketProtocols, string secWebSocketVersion, string secWebSocketKey, WebSocket webSocket)
		{
			this._cookieCollection = new CookieCollection();
			this._cookieCollection.Add(cookieCollection);
			this._headers = new NameValueCollection(headers);
			this._user = HttpListenerWebSocketContext.CopyPrincipal(user);
			this._requestUri = requestUri;
			this._isAuthenticated = isAuthenticated;
			this._isLocal = isLocal;
			this._isSecureConnection = isSecureConnection;
			this._origin = origin;
			this._secWebSocketProtocols = secWebSocketProtocols;
			this._secWebSocketVersion = secWebSocketVersion;
			this._secWebSocketKey = secWebSocketKey;
			this._webSocket = webSocket;
		}

		/// <summary>Gets the URI requested by the WebSocket client.</summary>
		/// <returns>Returns <see cref="T:System.Uri" />.The URI requested by the WebSocket client.</returns>
		// Token: 0x17000A92 RID: 2706
		// (get) Token: 0x06002A9D RID: 10909 RVA: 0x000951F6 File Offset: 0x000933F6
		public override Uri RequestUri
		{
			get
			{
				return this._requestUri;
			}
		}

		/// <summary>Gets the HTTP headers received by the <see cref="T:System.Net.HttpListener" /> object in the WebSocket opening handshake.</summary>
		/// <returns>Returns <see cref="T:System.Collections.Specialized.NameValueCollection" />.The HTTP headers received by the <see cref="T:System.Net.HttpListener" /> object.</returns>
		// Token: 0x17000A93 RID: 2707
		// (get) Token: 0x06002A9E RID: 10910 RVA: 0x000951FE File Offset: 0x000933FE
		public override NameValueCollection Headers
		{
			get
			{
				return this._headers;
			}
		}

		/// <summary>Gets the value of the Origin HTTP header included in the WebSocket opening handshake.</summary>
		/// <returns>Returns <see cref="T:System.String" />.The value of the Origin HTTP header.</returns>
		// Token: 0x17000A94 RID: 2708
		// (get) Token: 0x06002A9F RID: 10911 RVA: 0x00095206 File Offset: 0x00093406
		public override string Origin
		{
			get
			{
				return this._origin;
			}
		}

		/// <summary>Gets the list of the Secure WebSocket protocols included in the WebSocket opening handshake.</summary>
		/// <returns>Returns <see cref="T:System.Collections.Generic.IEnumerable`1" />.The list of the Secure WebSocket protocols.</returns>
		// Token: 0x17000A95 RID: 2709
		// (get) Token: 0x06002AA0 RID: 10912 RVA: 0x0009520E File Offset: 0x0009340E
		public override IEnumerable<string> SecWebSocketProtocols
		{
			get
			{
				return this._secWebSocketProtocols;
			}
		}

		/// <summary>Gets the list of sub-protocols requested by the WebSocket client.</summary>
		/// <returns>Returns <see cref="T:System.String" />.The list of sub-protocols requested by the WebSocket client.</returns>
		// Token: 0x17000A96 RID: 2710
		// (get) Token: 0x06002AA1 RID: 10913 RVA: 0x00095216 File Offset: 0x00093416
		public override string SecWebSocketVersion
		{
			get
			{
				return this._secWebSocketVersion;
			}
		}

		/// <summary>Gets the value of the SecWebSocketKey HTTP header included in the WebSocket opening handshake.</summary>
		/// <returns>Returns <see cref="T:System.String" />.The value of the SecWebSocketKey HTTP header.</returns>
		// Token: 0x17000A97 RID: 2711
		// (get) Token: 0x06002AA2 RID: 10914 RVA: 0x0009521E File Offset: 0x0009341E
		public override string SecWebSocketKey
		{
			get
			{
				return this._secWebSocketKey;
			}
		}

		/// <summary>Gets the cookies received by the <see cref="T:System.Net.HttpListener" /> object in the WebSocket opening handshake.</summary>
		/// <returns>Returns <see cref="T:System.Net.CookieCollection" />.The cookies received by the <see cref="T:System.Net.HttpListener" /> object.</returns>
		// Token: 0x17000A98 RID: 2712
		// (get) Token: 0x06002AA3 RID: 10915 RVA: 0x00095226 File Offset: 0x00093426
		public override CookieCollection CookieCollection
		{
			get
			{
				return this._cookieCollection;
			}
		}

		/// <summary>Gets an object used to obtain identity, authentication information, and security roles for the WebSocket client.</summary>
		/// <returns>Returns <see cref="T:System.Security.Principal.IPrincipal" />.The identity, authentication information, and security roles for the WebSocket client.</returns>
		// Token: 0x17000A99 RID: 2713
		// (get) Token: 0x06002AA4 RID: 10916 RVA: 0x0009522E File Offset: 0x0009342E
		public override IPrincipal User
		{
			get
			{
				return this._user;
			}
		}

		/// <summary>Gets a value that indicates if the WebSocket client is authenticated.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.
		///     <see langword="true" /> if the WebSocket client is authenticated; otherwise <see langword="false" />.</returns>
		// Token: 0x17000A9A RID: 2714
		// (get) Token: 0x06002AA5 RID: 10917 RVA: 0x00095236 File Offset: 0x00093436
		public override bool IsAuthenticated
		{
			get
			{
				return this._isAuthenticated;
			}
		}

		/// <summary>Gets a value that indicates if the WebSocket client connected from the local machine.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.
		///     <see langword="true" /> if the WebSocket client connected from the local machine; otherwise <see langword="false" />.</returns>
		// Token: 0x17000A9B RID: 2715
		// (get) Token: 0x06002AA6 RID: 10918 RVA: 0x0009523E File Offset: 0x0009343E
		public override bool IsLocal
		{
			get
			{
				return this._isLocal;
			}
		}

		/// <summary>Gets a value that indicates if the WebSocket connection is secured using Secure Sockets Layer (SSL).</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.
		///     <see langword="true" /> if the WebSocket connection is secured using SSL; otherwise <see langword="false" />.</returns>
		// Token: 0x17000A9C RID: 2716
		// (get) Token: 0x06002AA7 RID: 10919 RVA: 0x00095246 File Offset: 0x00093446
		public override bool IsSecureConnection
		{
			get
			{
				return this._isSecureConnection;
			}
		}

		/// <summary>Gets the WebSocket instance used to send and receive data over the WebSocket connection.</summary>
		/// <returns>Returns <see cref="T:System.Net.WebSockets.WebSocket" />.The WebSocket instance.</returns>
		// Token: 0x17000A9D RID: 2717
		// (get) Token: 0x06002AA8 RID: 10920 RVA: 0x0009524E File Offset: 0x0009344E
		public override WebSocket WebSocket
		{
			get
			{
				return this._webSocket;
			}
		}

		// Token: 0x06002AA9 RID: 10921 RVA: 0x00095258 File Offset: 0x00093458
		private static IPrincipal CopyPrincipal(IPrincipal user)
		{
			if (user != null)
			{
				if (user is WindowsPrincipal)
				{
					throw new PlatformNotSupportedException();
				}
				HttpListenerBasicIdentity httpListenerBasicIdentity;
				if ((httpListenerBasicIdentity = (user.Identity as HttpListenerBasicIdentity)) != null)
				{
					return new GenericPrincipal(new HttpListenerBasicIdentity(httpListenerBasicIdentity.Name, httpListenerBasicIdentity.Password), null);
				}
			}
			return null;
		}

		// Token: 0x06002AAA RID: 10922 RVA: 0x000092E2 File Offset: 0x000074E2
		internal HttpListenerWebSocketContext()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04002267 RID: 8807
		private readonly Uri _requestUri;

		// Token: 0x04002268 RID: 8808
		private readonly NameValueCollection _headers;

		// Token: 0x04002269 RID: 8809
		private readonly CookieCollection _cookieCollection;

		// Token: 0x0400226A RID: 8810
		private readonly IPrincipal _user;

		// Token: 0x0400226B RID: 8811
		private readonly bool _isAuthenticated;

		// Token: 0x0400226C RID: 8812
		private readonly bool _isLocal;

		// Token: 0x0400226D RID: 8813
		private readonly bool _isSecureConnection;

		// Token: 0x0400226E RID: 8814
		private readonly string _origin;

		// Token: 0x0400226F RID: 8815
		private readonly IEnumerable<string> _secWebSocketProtocols;

		// Token: 0x04002270 RID: 8816
		private readonly string _secWebSocketVersion;

		// Token: 0x04002271 RID: 8817
		private readonly string _secWebSocketKey;

		// Token: 0x04002272 RID: 8818
		private readonly WebSocket _webSocket;
	}
}
