﻿using System;

namespace System.Net
{
	// Token: 0x02000356 RID: 854
	internal interface IAutoWebProxy : IWebProxy
	{
		// Token: 0x060018AF RID: 6319
		ProxyChain GetProxies(Uri destination);
	}
}
