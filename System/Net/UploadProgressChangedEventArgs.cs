﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Net
{
	/// <summary>Provides data for the <see cref="E:System.Net.WebClient.UploadProgressChanged" /> event of a <see cref="T:System.Net.WebClient" />.</summary>
	// Token: 0x020003B2 RID: 946
	public class UploadProgressChangedEventArgs : ProgressChangedEventArgs
	{
		// Token: 0x06001BC5 RID: 7109 RVA: 0x00064ACC File Offset: 0x00062CCC
		internal UploadProgressChangedEventArgs(int progressPercentage, object userToken, long bytesSent, long totalBytesToSend, long bytesReceived, long totalBytesToReceive) : base(progressPercentage, userToken)
		{
			this.m_BytesReceived = bytesReceived;
			this.m_TotalBytesToReceive = totalBytesToReceive;
			this.m_BytesSent = bytesSent;
			this.m_TotalBytesToSend = totalBytesToSend;
		}

		/// <summary>Gets the number of bytes received.</summary>
		/// <returns>An <see cref="T:System.Int64" /> value that indicates the number of bytes received.</returns>
		// Token: 0x170005A6 RID: 1446
		// (get) Token: 0x06001BC6 RID: 7110 RVA: 0x00064AF5 File Offset: 0x00062CF5
		public long BytesReceived
		{
			get
			{
				return this.m_BytesReceived;
			}
		}

		/// <summary>Gets the total number of bytes in a <see cref="T:System.Net.WebClient" /> data upload operation.</summary>
		/// <returns>An <see cref="T:System.Int64" /> value that indicates the number of bytes that will be received.</returns>
		// Token: 0x170005A7 RID: 1447
		// (get) Token: 0x06001BC7 RID: 7111 RVA: 0x00064AFD File Offset: 0x00062CFD
		public long TotalBytesToReceive
		{
			get
			{
				return this.m_TotalBytesToReceive;
			}
		}

		/// <summary>Gets the number of bytes sent.</summary>
		/// <returns>An <see cref="T:System.Int64" /> value that indicates the number of bytes sent.</returns>
		// Token: 0x170005A8 RID: 1448
		// (get) Token: 0x06001BC8 RID: 7112 RVA: 0x00064B05 File Offset: 0x00062D05
		public long BytesSent
		{
			get
			{
				return this.m_BytesSent;
			}
		}

		/// <summary>Gets the total number of bytes to send.</summary>
		/// <returns>An <see cref="T:System.Int64" /> value that indicates the number of bytes that will be sent.</returns>
		// Token: 0x170005A9 RID: 1449
		// (get) Token: 0x06001BC9 RID: 7113 RVA: 0x00064B0D File Offset: 0x00062D0D
		public long TotalBytesToSend
		{
			get
			{
				return this.m_TotalBytesToSend;
			}
		}

		// Token: 0x06001BCA RID: 7114 RVA: 0x000092E2 File Offset: 0x000074E2
		internal UploadProgressChangedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001928 RID: 6440
		private long m_BytesReceived;

		// Token: 0x04001929 RID: 6441
		private long m_TotalBytesToReceive;

		// Token: 0x0400192A RID: 6442
		private long m_BytesSent;

		// Token: 0x0400192B RID: 6443
		private long m_TotalBytesToSend;
	}
}
