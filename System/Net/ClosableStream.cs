﻿using System;
using System.IO;
using System.Threading;

namespace System.Net
{
	// Token: 0x0200038A RID: 906
	internal class ClosableStream : DelegatedStream
	{
		// Token: 0x06001A3E RID: 6718 RVA: 0x0005FE47 File Offset: 0x0005E047
		internal ClosableStream(Stream stream, EventHandler onClose) : base(stream)
		{
			this.onClose = onClose;
		}

		// Token: 0x06001A3F RID: 6719 RVA: 0x0005FE57 File Offset: 0x0005E057
		public override void Close()
		{
			if (Interlocked.Increment(ref this.closed) == 1 && this.onClose != null)
			{
				this.onClose(this, new EventArgs());
			}
		}

		// Token: 0x04001898 RID: 6296
		private EventHandler onClose;

		// Token: 0x04001899 RID: 6297
		private int closed;
	}
}
