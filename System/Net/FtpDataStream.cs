﻿using System;
using System.IO;
using System.Runtime.Remoting.Messaging;

namespace System.Net
{
	// Token: 0x020003D5 RID: 981
	internal class FtpDataStream : Stream, IDisposable
	{
		// Token: 0x06001CDD RID: 7389 RVA: 0x000686FC File Offset: 0x000668FC
		internal FtpDataStream(FtpWebRequest request, Stream stream, bool isRead)
		{
			if (request == null)
			{
				throw new ArgumentNullException("request");
			}
			this.request = request;
			this.networkStream = stream;
			this.isRead = isRead;
		}

		// Token: 0x170005DB RID: 1499
		// (get) Token: 0x06001CDE RID: 7390 RVA: 0x00068727 File Offset: 0x00066927
		public override bool CanRead
		{
			get
			{
				return this.isRead;
			}
		}

		// Token: 0x170005DC RID: 1500
		// (get) Token: 0x06001CDF RID: 7391 RVA: 0x0006872F File Offset: 0x0006692F
		public override bool CanWrite
		{
			get
			{
				return !this.isRead;
			}
		}

		// Token: 0x170005DD RID: 1501
		// (get) Token: 0x06001CE0 RID: 7392 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170005DE RID: 1502
		// (get) Token: 0x06001CE1 RID: 7393 RVA: 0x00006740 File Offset: 0x00004940
		public override long Length
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x170005DF RID: 1503
		// (get) Token: 0x06001CE2 RID: 7394 RVA: 0x00006740 File Offset: 0x00004940
		// (set) Token: 0x06001CE3 RID: 7395 RVA: 0x00006740 File Offset: 0x00004940
		public override long Position
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x170005E0 RID: 1504
		// (get) Token: 0x06001CE4 RID: 7396 RVA: 0x0006873A File Offset: 0x0006693A
		internal Stream NetworkStream
		{
			get
			{
				this.CheckDisposed();
				return this.networkStream;
			}
		}

		// Token: 0x06001CE5 RID: 7397 RVA: 0x00068748 File Offset: 0x00066948
		public override void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x06001CE6 RID: 7398 RVA: 0x0000232D File Offset: 0x0000052D
		public override void Flush()
		{
		}

		// Token: 0x06001CE7 RID: 7399 RVA: 0x00006740 File Offset: 0x00004940
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001CE8 RID: 7400 RVA: 0x00006740 File Offset: 0x00004940
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001CE9 RID: 7401 RVA: 0x00068754 File Offset: 0x00066954
		private int ReadInternal(byte[] buffer, int offset, int size)
		{
			int num = 0;
			this.request.CheckIfAborted();
			try
			{
				num = this.networkStream.Read(buffer, offset, size);
			}
			catch (IOException)
			{
				throw new ProtocolViolationException("Server commited a protocol violation");
			}
			this.totalRead += num;
			if (num == 0)
			{
				this.networkStream = null;
				this.request.CloseDataConnection();
				this.request.SetTransferCompleted();
			}
			return num;
		}

		// Token: 0x06001CEA RID: 7402 RVA: 0x000687CC File Offset: 0x000669CC
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int size, AsyncCallback cb, object state)
		{
			this.CheckDisposed();
			if (!this.isRead)
			{
				throw new NotSupportedException();
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || offset > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (size < 0 || size > buffer.Length - offset)
			{
				throw new ArgumentOutOfRangeException("offset+size");
			}
			return new FtpDataStream.ReadDelegate(this.ReadInternal).BeginInvoke(buffer, offset, size, cb, state);
		}

		// Token: 0x06001CEB RID: 7403 RVA: 0x00068840 File Offset: 0x00066A40
		public override int EndRead(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			AsyncResult asyncResult2 = asyncResult as AsyncResult;
			if (asyncResult2 == null)
			{
				throw new ArgumentException("Invalid asyncResult", "asyncResult");
			}
			FtpDataStream.ReadDelegate readDelegate = asyncResult2.AsyncDelegate as FtpDataStream.ReadDelegate;
			if (readDelegate == null)
			{
				throw new ArgumentException("Invalid asyncResult", "asyncResult");
			}
			return readDelegate.EndInvoke(asyncResult);
		}

		// Token: 0x06001CEC RID: 7404 RVA: 0x00068898 File Offset: 0x00066A98
		public override int Read(byte[] buffer, int offset, int size)
		{
			this.request.CheckIfAborted();
			IAsyncResult asyncResult = this.BeginRead(buffer, offset, size, null, null);
			if (!asyncResult.IsCompleted && !asyncResult.AsyncWaitHandle.WaitOne(this.request.ReadWriteTimeout, false))
			{
				throw new WebException("Read timed out.", WebExceptionStatus.Timeout);
			}
			return this.EndRead(asyncResult);
		}

		// Token: 0x06001CED RID: 7405 RVA: 0x000688F4 File Offset: 0x00066AF4
		private void WriteInternal(byte[] buffer, int offset, int size)
		{
			this.request.CheckIfAborted();
			try
			{
				this.networkStream.Write(buffer, offset, size);
			}
			catch (IOException)
			{
				throw new ProtocolViolationException();
			}
		}

		// Token: 0x06001CEE RID: 7406 RVA: 0x00068934 File Offset: 0x00066B34
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int size, AsyncCallback cb, object state)
		{
			this.CheckDisposed();
			if (this.isRead)
			{
				throw new NotSupportedException();
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || offset > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (size < 0 || size > buffer.Length - offset)
			{
				throw new ArgumentOutOfRangeException("offset+size");
			}
			return new FtpDataStream.WriteDelegate(this.WriteInternal).BeginInvoke(buffer, offset, size, cb, state);
		}

		// Token: 0x06001CEF RID: 7407 RVA: 0x000689A8 File Offset: 0x00066BA8
		public override void EndWrite(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			AsyncResult asyncResult2 = asyncResult as AsyncResult;
			if (asyncResult2 == null)
			{
				throw new ArgumentException("Invalid asyncResult.", "asyncResult");
			}
			FtpDataStream.WriteDelegate writeDelegate = asyncResult2.AsyncDelegate as FtpDataStream.WriteDelegate;
			if (writeDelegate == null)
			{
				throw new ArgumentException("Invalid asyncResult.", "asyncResult");
			}
			writeDelegate.EndInvoke(asyncResult);
		}

		// Token: 0x06001CF0 RID: 7408 RVA: 0x00068A00 File Offset: 0x00066C00
		public override void Write(byte[] buffer, int offset, int size)
		{
			this.request.CheckIfAborted();
			IAsyncResult asyncResult = this.BeginWrite(buffer, offset, size, null, null);
			if (!asyncResult.IsCompleted && !asyncResult.AsyncWaitHandle.WaitOne(this.request.ReadWriteTimeout, false))
			{
				throw new WebException("Read timed out.", WebExceptionStatus.Timeout);
			}
			this.EndWrite(asyncResult);
		}

		// Token: 0x06001CF1 RID: 7409 RVA: 0x00068A5C File Offset: 0x00066C5C
		~FtpDataStream()
		{
			this.Dispose(false);
		}

		// Token: 0x06001CF2 RID: 7410 RVA: 0x00068A8C File Offset: 0x00066C8C
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06001CF3 RID: 7411 RVA: 0x00068A9B File Offset: 0x00066C9B
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			this.disposed = true;
			if (this.networkStream != null)
			{
				this.request.CloseDataConnection();
				this.request.SetTransferCompleted();
				this.request = null;
				this.networkStream = null;
			}
		}

		// Token: 0x06001CF4 RID: 7412 RVA: 0x00068AD9 File Offset: 0x00066CD9
		private void CheckDisposed()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
		}

		// Token: 0x040019A3 RID: 6563
		private FtpWebRequest request;

		// Token: 0x040019A4 RID: 6564
		private Stream networkStream;

		// Token: 0x040019A5 RID: 6565
		private bool disposed;

		// Token: 0x040019A6 RID: 6566
		private bool isRead;

		// Token: 0x040019A7 RID: 6567
		private int totalRead;

		// Token: 0x020003D6 RID: 982
		// (Invoke) Token: 0x06001CF6 RID: 7414
		private delegate void WriteDelegate(byte[] buffer, int offset, int size);

		// Token: 0x020003D7 RID: 983
		// (Invoke) Token: 0x06001CFA RID: 7418
		private delegate int ReadDelegate(byte[] buffer, int offset, int size);
	}
}
