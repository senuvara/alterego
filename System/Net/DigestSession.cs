﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace System.Net
{
	// Token: 0x020003C9 RID: 969
	internal class DigestSession
	{
		// Token: 0x06001C5A RID: 7258 RVA: 0x0006659D File Offset: 0x0006479D
		static DigestSession()
		{
		}

		// Token: 0x06001C5B RID: 7259 RVA: 0x000665A9 File Offset: 0x000647A9
		public DigestSession()
		{
			this._nc = 1;
			this.lastUse = DateTime.UtcNow;
		}

		// Token: 0x170005C5 RID: 1477
		// (get) Token: 0x06001C5C RID: 7260 RVA: 0x000665C3 File Offset: 0x000647C3
		public string Algorithm
		{
			get
			{
				return this.parser.Algorithm;
			}
		}

		// Token: 0x170005C6 RID: 1478
		// (get) Token: 0x06001C5D RID: 7261 RVA: 0x000665D0 File Offset: 0x000647D0
		public string Realm
		{
			get
			{
				return this.parser.Realm;
			}
		}

		// Token: 0x170005C7 RID: 1479
		// (get) Token: 0x06001C5E RID: 7262 RVA: 0x000665DD File Offset: 0x000647DD
		public string Nonce
		{
			get
			{
				return this.parser.Nonce;
			}
		}

		// Token: 0x170005C8 RID: 1480
		// (get) Token: 0x06001C5F RID: 7263 RVA: 0x000665EA File Offset: 0x000647EA
		public string Opaque
		{
			get
			{
				return this.parser.Opaque;
			}
		}

		// Token: 0x170005C9 RID: 1481
		// (get) Token: 0x06001C60 RID: 7264 RVA: 0x000665F7 File Offset: 0x000647F7
		public string QOP
		{
			get
			{
				return this.parser.QOP;
			}
		}

		// Token: 0x170005CA RID: 1482
		// (get) Token: 0x06001C61 RID: 7265 RVA: 0x00066604 File Offset: 0x00064804
		public string CNonce
		{
			get
			{
				if (this._cnonce == null)
				{
					byte[] array = new byte[15];
					DigestSession.rng.GetBytes(array);
					this._cnonce = Convert.ToBase64String(array);
					Array.Clear(array, 0, array.Length);
				}
				return this._cnonce;
			}
		}

		// Token: 0x06001C62 RID: 7266 RVA: 0x00066648 File Offset: 0x00064848
		public bool Parse(string challenge)
		{
			this.parser = new DigestHeaderParser(challenge);
			if (!this.parser.Parse())
			{
				return false;
			}
			if (this.parser.Algorithm == null || this.parser.Algorithm.ToUpper().StartsWith("MD5"))
			{
				this.hash = MD5.Create();
			}
			return true;
		}

		// Token: 0x06001C63 RID: 7267 RVA: 0x000666A8 File Offset: 0x000648A8
		private string HashToHexString(string toBeHashed)
		{
			if (this.hash == null)
			{
				return null;
			}
			this.hash.Initialize();
			byte[] array = this.hash.ComputeHash(Encoding.ASCII.GetBytes(toBeHashed));
			StringBuilder stringBuilder = new StringBuilder();
			foreach (byte b in array)
			{
				stringBuilder.Append(b.ToString("x2"));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06001C64 RID: 7268 RVA: 0x00066714 File Offset: 0x00064914
		private string HA1(string username, string password)
		{
			string toBeHashed = string.Format("{0}:{1}:{2}", username, this.Realm, password);
			if (this.Algorithm != null && this.Algorithm.ToLower() == "md5-sess")
			{
				toBeHashed = string.Format("{0}:{1}:{2}", this.HashToHexString(toBeHashed), this.Nonce, this.CNonce);
			}
			return this.HashToHexString(toBeHashed);
		}

		// Token: 0x06001C65 RID: 7269 RVA: 0x00066778 File Offset: 0x00064978
		private string HA2(HttpWebRequest webRequest)
		{
			string toBeHashed = string.Format("{0}:{1}", webRequest.Method, webRequest.RequestUri.PathAndQuery);
			this.QOP == "auth-int";
			return this.HashToHexString(toBeHashed);
		}

		// Token: 0x06001C66 RID: 7270 RVA: 0x000667BC File Offset: 0x000649BC
		private string Response(string username, string password, HttpWebRequest webRequest)
		{
			string text = string.Format("{0}:{1}:", this.HA1(username, password), this.Nonce);
			if (this.QOP != null)
			{
				text += string.Format("{0}:{1}:{2}:", this._nc.ToString("X8"), this.CNonce, this.QOP);
			}
			text += this.HA2(webRequest);
			return this.HashToHexString(text);
		}

		// Token: 0x06001C67 RID: 7271 RVA: 0x0006682C File Offset: 0x00064A2C
		public Authorization Authenticate(WebRequest webRequest, ICredentials credentials)
		{
			if (this.parser == null)
			{
				throw new InvalidOperationException();
			}
			HttpWebRequest httpWebRequest = webRequest as HttpWebRequest;
			if (httpWebRequest == null)
			{
				return null;
			}
			this.lastUse = DateTime.UtcNow;
			NetworkCredential credential = credentials.GetCredential(httpWebRequest.RequestUri, "digest");
			if (credential == null)
			{
				return null;
			}
			string userName = credential.UserName;
			if (userName == null || userName == "")
			{
				return null;
			}
			string password = credential.Password;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("Digest username=\"{0}\", ", userName);
			stringBuilder.AppendFormat("realm=\"{0}\", ", this.Realm);
			stringBuilder.AppendFormat("nonce=\"{0}\", ", this.Nonce);
			stringBuilder.AppendFormat("uri=\"{0}\", ", httpWebRequest.Address.PathAndQuery);
			if (this.Algorithm != null)
			{
				stringBuilder.AppendFormat("algorithm=\"{0}\", ", this.Algorithm);
			}
			stringBuilder.AppendFormat("response=\"{0}\", ", this.Response(userName, password, httpWebRequest));
			if (this.QOP != null)
			{
				stringBuilder.AppendFormat("qop=\"{0}\", ", this.QOP);
			}
			lock (this)
			{
				if (this.QOP != null)
				{
					stringBuilder.AppendFormat("nc={0:X8}, ", this._nc);
					this._nc++;
				}
			}
			if (this.CNonce != null)
			{
				stringBuilder.AppendFormat("cnonce=\"{0}\", ", this.CNonce);
			}
			if (this.Opaque != null)
			{
				stringBuilder.AppendFormat("opaque=\"{0}\", ", this.Opaque);
			}
			stringBuilder.Length -= 2;
			return new Authorization(stringBuilder.ToString());
		}

		// Token: 0x170005CB RID: 1483
		// (get) Token: 0x06001C68 RID: 7272 RVA: 0x000669E0 File Offset: 0x00064BE0
		public DateTime LastUse
		{
			get
			{
				return this.lastUse;
			}
		}

		// Token: 0x04001982 RID: 6530
		private static RandomNumberGenerator rng = RandomNumberGenerator.Create();

		// Token: 0x04001983 RID: 6531
		private DateTime lastUse;

		// Token: 0x04001984 RID: 6532
		private int _nc;

		// Token: 0x04001985 RID: 6533
		private HashAlgorithm hash;

		// Token: 0x04001986 RID: 6534
		private DigestHeaderParser parser;

		// Token: 0x04001987 RID: 6535
		private string _cnonce;
	}
}
