﻿using System;

namespace System.Net
{
	// Token: 0x02000317 RID: 791
	internal enum HttpProcessingResult
	{
		// Token: 0x0400163B RID: 5691
		Continue,
		// Token: 0x0400163C RID: 5692
		ReadWait,
		// Token: 0x0400163D RID: 5693
		WriteWait
	}
}
