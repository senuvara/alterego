﻿using System;

namespace System.Net
{
	// Token: 0x02000345 RID: 837
	internal enum WriteBufferState
	{
		// Token: 0x0400171C RID: 5916
		Disabled,
		// Token: 0x0400171D RID: 5917
		Headers,
		// Token: 0x0400171E RID: 5918
		Buffer,
		// Token: 0x0400171F RID: 5919
		Playback
	}
}
