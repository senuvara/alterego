﻿using System;
using System.IO;

namespace System.Net
{
	// Token: 0x0200040D RID: 1037
	internal class WebConnectionData
	{
		// Token: 0x06002051 RID: 8273 RVA: 0x000761CF File Offset: 0x000743CF
		public WebConnectionData()
		{
			this._readState = ReadState.None;
		}

		// Token: 0x06002052 RID: 8274 RVA: 0x000761DE File Offset: 0x000743DE
		public WebConnectionData(HttpWebRequest request)
		{
			this._request = request;
		}

		// Token: 0x17000703 RID: 1795
		// (get) Token: 0x06002053 RID: 8275 RVA: 0x000761ED File Offset: 0x000743ED
		// (set) Token: 0x06002054 RID: 8276 RVA: 0x000761F5 File Offset: 0x000743F5
		public HttpWebRequest request
		{
			get
			{
				return this._request;
			}
			set
			{
				this._request = value;
			}
		}

		// Token: 0x17000704 RID: 1796
		// (get) Token: 0x06002055 RID: 8277 RVA: 0x000761FE File Offset: 0x000743FE
		// (set) Token: 0x06002056 RID: 8278 RVA: 0x00076208 File Offset: 0x00074408
		public ReadState ReadState
		{
			get
			{
				return this._readState;
			}
			set
			{
				lock (this)
				{
					if (this._readState == ReadState.Aborted && value != ReadState.Aborted)
					{
						throw new WebException("Aborted", WebExceptionStatus.RequestCanceled);
					}
					this._readState = value;
				}
			}
		}

		// Token: 0x04001B56 RID: 6998
		private HttpWebRequest _request;

		// Token: 0x04001B57 RID: 6999
		public int StatusCode;

		// Token: 0x04001B58 RID: 7000
		public string StatusDescription;

		// Token: 0x04001B59 RID: 7001
		public WebHeaderCollection Headers;

		// Token: 0x04001B5A RID: 7002
		public Version Version;

		// Token: 0x04001B5B RID: 7003
		public Version ProxyVersion;

		// Token: 0x04001B5C RID: 7004
		public Stream stream;

		// Token: 0x04001B5D RID: 7005
		public string[] Challenge;

		// Token: 0x04001B5E RID: 7006
		private ReadState _readState;
	}
}
