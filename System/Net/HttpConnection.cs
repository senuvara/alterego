﻿using System;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;

namespace System.Net
{
	// Token: 0x020003DD RID: 989
	internal sealed class HttpConnection
	{
		// Token: 0x06001D77 RID: 7543 RVA: 0x0006A8EC File Offset: 0x00068AEC
		public HttpConnection(Socket sock, EndPointListener epl, bool secure, X509Certificate cert)
		{
			this.sock = sock;
			this.epl = epl;
			this.secure = secure;
			this.cert = cert;
			if (!secure)
			{
				this.stream = new NetworkStream(sock, false);
			}
			else
			{
				this.ssl_stream = epl.Listener.CreateSslStream(new NetworkStream(sock, false), false, delegate(object t, X509Certificate c, X509Chain ch, SslPolicyErrors e)
				{
					if (c == null)
					{
						return true;
					}
					X509Certificate2 x509Certificate = c as X509Certificate2;
					if (x509Certificate == null)
					{
						x509Certificate = new X509Certificate2(c.GetRawCertData());
					}
					this.client_cert = x509Certificate;
					this.client_cert_errors = new int[]
					{
						(int)e
					};
					return true;
				});
				this.stream = this.ssl_stream;
			}
			this.timer = new Timer(new TimerCallback(this.OnTimeout), null, -1, -1);
			if (this.ssl_stream != null)
			{
				this.ssl_stream.AuthenticateAsServer(cert, true, (SslProtocols)ServicePointManager.SecurityProtocol, false);
			}
			this.Init();
		}

		// Token: 0x17000604 RID: 1540
		// (get) Token: 0x06001D78 RID: 7544 RVA: 0x0006A9A6 File Offset: 0x00068BA6
		internal SslStream SslStream
		{
			get
			{
				return this.ssl_stream;
			}
		}

		// Token: 0x17000605 RID: 1541
		// (get) Token: 0x06001D79 RID: 7545 RVA: 0x0006A9AE File Offset: 0x00068BAE
		internal int[] ClientCertificateErrors
		{
			get
			{
				return this.client_cert_errors;
			}
		}

		// Token: 0x17000606 RID: 1542
		// (get) Token: 0x06001D7A RID: 7546 RVA: 0x0006A9B6 File Offset: 0x00068BB6
		internal X509Certificate2 ClientCertificate
		{
			get
			{
				return this.client_cert;
			}
		}

		// Token: 0x06001D7B RID: 7547 RVA: 0x0006A9C0 File Offset: 0x00068BC0
		private void Init()
		{
			this.context_bound = false;
			this.i_stream = null;
			this.o_stream = null;
			this.prefix = null;
			this.chunked = false;
			this.ms = new MemoryStream();
			this.position = 0;
			this.input_state = HttpConnection.InputState.RequestLine;
			this.line_state = HttpConnection.LineState.None;
			this.context = new HttpListenerContext(this);
		}

		// Token: 0x17000607 RID: 1543
		// (get) Token: 0x06001D7C RID: 7548 RVA: 0x0006AA1C File Offset: 0x00068C1C
		public bool IsClosed
		{
			get
			{
				return this.sock == null;
			}
		}

		// Token: 0x17000608 RID: 1544
		// (get) Token: 0x06001D7D RID: 7549 RVA: 0x0006AA27 File Offset: 0x00068C27
		public int Reuses
		{
			get
			{
				return this.reuses;
			}
		}

		// Token: 0x17000609 RID: 1545
		// (get) Token: 0x06001D7E RID: 7550 RVA: 0x0006AA2F File Offset: 0x00068C2F
		public IPEndPoint LocalEndPoint
		{
			get
			{
				if (this.local_ep != null)
				{
					return this.local_ep;
				}
				this.local_ep = (IPEndPoint)this.sock.LocalEndPoint;
				return this.local_ep;
			}
		}

		// Token: 0x1700060A RID: 1546
		// (get) Token: 0x06001D7F RID: 7551 RVA: 0x0006AA5C File Offset: 0x00068C5C
		public IPEndPoint RemoteEndPoint
		{
			get
			{
				return (IPEndPoint)this.sock.RemoteEndPoint;
			}
		}

		// Token: 0x1700060B RID: 1547
		// (get) Token: 0x06001D80 RID: 7552 RVA: 0x0006AA6E File Offset: 0x00068C6E
		public bool IsSecure
		{
			get
			{
				return this.secure;
			}
		}

		// Token: 0x1700060C RID: 1548
		// (get) Token: 0x06001D81 RID: 7553 RVA: 0x0006AA76 File Offset: 0x00068C76
		// (set) Token: 0x06001D82 RID: 7554 RVA: 0x0006AA7E File Offset: 0x00068C7E
		public ListenerPrefix Prefix
		{
			get
			{
				return this.prefix;
			}
			set
			{
				this.prefix = value;
			}
		}

		// Token: 0x06001D83 RID: 7555 RVA: 0x0006AA87 File Offset: 0x00068C87
		private void OnTimeout(object unused)
		{
			this.CloseSocket();
			this.Unbind();
		}

		// Token: 0x06001D84 RID: 7556 RVA: 0x0006AA98 File Offset: 0x00068C98
		public void BeginReadRequest()
		{
			if (this.buffer == null)
			{
				this.buffer = new byte[8192];
			}
			try
			{
				if (this.reuses == 1)
				{
					this.s_timeout = 15000;
				}
				this.timer.Change(this.s_timeout, -1);
				this.stream.BeginRead(this.buffer, 0, 8192, HttpConnection.onread_cb, this);
			}
			catch
			{
				this.timer.Change(-1, -1);
				this.CloseSocket();
				this.Unbind();
			}
		}

		// Token: 0x06001D85 RID: 7557 RVA: 0x0006AB34 File Offset: 0x00068D34
		public RequestStream GetRequestStream(bool chunked, long contentlength)
		{
			if (this.i_stream == null)
			{
				byte[] array = this.ms.GetBuffer();
				int num = (int)this.ms.Length;
				this.ms = null;
				if (chunked)
				{
					this.chunked = true;
					this.context.Response.SendChunked = true;
					this.i_stream = new ChunkedInputStream(this.context, this.stream, array, this.position, num - this.position);
				}
				else
				{
					this.i_stream = new RequestStream(this.stream, array, this.position, num - this.position, contentlength);
				}
			}
			return this.i_stream;
		}

		// Token: 0x06001D86 RID: 7558 RVA: 0x0006ABD8 File Offset: 0x00068DD8
		public ResponseStream GetResponseStream()
		{
			if (this.o_stream == null)
			{
				HttpListener listener = this.context.Listener;
				if (listener == null)
				{
					return new ResponseStream(this.stream, this.context.Response, true);
				}
				this.o_stream = new ResponseStream(this.stream, this.context.Response, listener.IgnoreWriteExceptions);
			}
			return this.o_stream;
		}

		// Token: 0x06001D87 RID: 7559 RVA: 0x0006AC3C File Offset: 0x00068E3C
		private static void OnRead(IAsyncResult ares)
		{
			((HttpConnection)ares.AsyncState).OnReadInternal(ares);
		}

		// Token: 0x06001D88 RID: 7560 RVA: 0x0006AC50 File Offset: 0x00068E50
		private void OnReadInternal(IAsyncResult ares)
		{
			this.timer.Change(-1, -1);
			int num = -1;
			try
			{
				num = this.stream.EndRead(ares);
				this.ms.Write(this.buffer, 0, num);
				if (this.ms.Length > 32768L)
				{
					this.SendError("Bad request", 400);
					this.Close(true);
					return;
				}
			}
			catch
			{
				if (this.ms != null && this.ms.Length > 0L)
				{
					this.SendError();
				}
				if (this.sock != null)
				{
					this.CloseSocket();
					this.Unbind();
				}
				return;
			}
			if (num == 0)
			{
				this.CloseSocket();
				this.Unbind();
				return;
			}
			if (this.ProcessInput(this.ms))
			{
				if (!this.context.HaveError)
				{
					this.context.Request.FinishInitialization();
				}
				if (this.context.HaveError)
				{
					this.SendError();
					this.Close(true);
					return;
				}
				if (!this.epl.BindContext(this.context))
				{
					this.SendError("Invalid host", 400);
					this.Close(true);
					return;
				}
				HttpListener listener = this.context.Listener;
				if (this.last_listener != listener)
				{
					this.RemoveConnection();
					listener.AddConnection(this);
					this.last_listener = listener;
				}
				this.context_bound = true;
				listener.RegisterContext(this.context);
				return;
			}
			else
			{
				this.stream.BeginRead(this.buffer, 0, 8192, HttpConnection.onread_cb, this);
			}
		}

		// Token: 0x06001D89 RID: 7561 RVA: 0x0006ADE4 File Offset: 0x00068FE4
		private void RemoveConnection()
		{
			if (this.last_listener == null)
			{
				this.epl.RemoveConnection(this);
				return;
			}
			this.last_listener.RemoveConnection(this);
		}

		// Token: 0x06001D8A RID: 7562 RVA: 0x0006AE08 File Offset: 0x00069008
		private bool ProcessInput(MemoryStream ms)
		{
			byte[] array = ms.GetBuffer();
			int num = (int)ms.Length;
			int num2 = 0;
			while (!this.context.HaveError)
			{
				if (this.position < num)
				{
					string text;
					try
					{
						text = this.ReadLine(array, this.position, num - this.position, ref num2);
						this.position += num2;
					}
					catch
					{
						this.context.ErrorMessage = "Bad request";
						this.context.ErrorStatus = 400;
						return true;
					}
					if (text == null)
					{
						goto IL_10D;
					}
					if (text == "")
					{
						if (this.input_state != HttpConnection.InputState.RequestLine)
						{
							this.current_line = null;
							ms = null;
							return true;
						}
						continue;
					}
					else
					{
						if (this.input_state == HttpConnection.InputState.RequestLine)
						{
							this.context.Request.SetRequestLine(text);
							this.input_state = HttpConnection.InputState.Headers;
							continue;
						}
						try
						{
							this.context.Request.AddHeader(text);
							continue;
						}
						catch (Exception ex)
						{
							this.context.ErrorMessage = ex.Message;
							this.context.ErrorStatus = 400;
							return true;
						}
						goto IL_10D;
					}
					bool result;
					return result;
				}
				IL_10D:
				if (num2 == num)
				{
					ms.SetLength(0L);
					this.position = 0;
				}
				return false;
			}
			return true;
		}

		// Token: 0x06001D8B RID: 7563 RVA: 0x0006AF58 File Offset: 0x00069158
		private string ReadLine(byte[] buffer, int offset, int len, ref int used)
		{
			if (this.current_line == null)
			{
				this.current_line = new StringBuilder(128);
			}
			int num = offset + len;
			used = 0;
			int num2 = offset;
			while (num2 < num && this.line_state != HttpConnection.LineState.LF)
			{
				used++;
				byte b = buffer[num2];
				if (b == 13)
				{
					this.line_state = HttpConnection.LineState.CR;
				}
				else if (b == 10)
				{
					this.line_state = HttpConnection.LineState.LF;
				}
				else
				{
					this.current_line.Append((char)b);
				}
				num2++;
			}
			string result = null;
			if (this.line_state == HttpConnection.LineState.LF)
			{
				this.line_state = HttpConnection.LineState.None;
				result = this.current_line.ToString();
				this.current_line.Length = 0;
			}
			return result;
		}

		// Token: 0x06001D8C RID: 7564 RVA: 0x0006AFFC File Offset: 0x000691FC
		public void SendError(string msg, int status)
		{
			try
			{
				HttpListenerResponse response = this.context.Response;
				response.StatusCode = status;
				response.ContentType = "text/html";
				string arg = HttpStatusDescription.Get(status);
				string s;
				if (msg != null)
				{
					s = string.Format("<h1>{0} ({1})</h1>", arg, msg);
				}
				else
				{
					s = string.Format("<h1>{0}</h1>", arg);
				}
				byte[] bytes = this.context.Response.ContentEncoding.GetBytes(s);
				response.Close(bytes, false);
			}
			catch
			{
			}
		}

		// Token: 0x06001D8D RID: 7565 RVA: 0x0006B080 File Offset: 0x00069280
		public void SendError()
		{
			this.SendError(this.context.ErrorMessage, this.context.ErrorStatus);
		}

		// Token: 0x06001D8E RID: 7566 RVA: 0x0006B09E File Offset: 0x0006929E
		private void Unbind()
		{
			if (this.context_bound)
			{
				this.epl.UnbindContext(this.context);
				this.context_bound = false;
			}
		}

		// Token: 0x06001D8F RID: 7567 RVA: 0x0006B0C0 File Offset: 0x000692C0
		public void Close()
		{
			this.Close(false);
		}

		// Token: 0x06001D90 RID: 7568 RVA: 0x0006B0CC File Offset: 0x000692CC
		private void CloseSocket()
		{
			if (this.sock == null)
			{
				return;
			}
			try
			{
				this.sock.Close();
			}
			catch
			{
			}
			finally
			{
				this.sock = null;
			}
			this.RemoveConnection();
		}

		// Token: 0x06001D91 RID: 7569 RVA: 0x0006B120 File Offset: 0x00069320
		internal void Close(bool force_close)
		{
			if (this.sock != null)
			{
				Stream responseStream = this.GetResponseStream();
				if (responseStream != null)
				{
					responseStream.Close();
				}
				this.o_stream = null;
			}
			if (this.sock == null)
			{
				return;
			}
			force_close |= !this.context.Request.KeepAlive;
			if (!force_close)
			{
				force_close = (this.context.Response.Headers["connection"] == "close");
			}
			if (force_close || !this.context.Request.FlushInput())
			{
				Socket socket = this.sock;
				this.sock = null;
				try
				{
					if (socket != null)
					{
						socket.Shutdown(SocketShutdown.Both);
					}
				}
				catch
				{
				}
				finally
				{
					if (socket != null)
					{
						socket.Close();
					}
				}
				this.Unbind();
				this.RemoveConnection();
				return;
			}
			if (this.chunked && !this.context.Response.ForceCloseChunked)
			{
				this.reuses++;
				this.Unbind();
				this.Init();
				this.BeginReadRequest();
				return;
			}
			this.reuses++;
			this.Unbind();
			this.Init();
			this.BeginReadRequest();
		}

		// Token: 0x06001D92 RID: 7570 RVA: 0x0006B258 File Offset: 0x00069458
		// Note: this type is marked as 'beforefieldinit'.
		static HttpConnection()
		{
		}

		// Token: 0x06001D93 RID: 7571 RVA: 0x0006B26C File Offset: 0x0006946C
		[CompilerGenerated]
		private bool <.ctor>b__24_0(object t, X509Certificate c, X509Chain ch, SslPolicyErrors e)
		{
			if (c == null)
			{
				return true;
			}
			X509Certificate2 x509Certificate = c as X509Certificate2;
			if (x509Certificate == null)
			{
				x509Certificate = new X509Certificate2(c.GetRawCertData());
			}
			this.client_cert = x509Certificate;
			this.client_cert_errors = new int[]
			{
				(int)e
			};
			return true;
		}

		// Token: 0x040019EC RID: 6636
		private static AsyncCallback onread_cb = new AsyncCallback(HttpConnection.OnRead);

		// Token: 0x040019ED RID: 6637
		private const int BufferSize = 8192;

		// Token: 0x040019EE RID: 6638
		private Socket sock;

		// Token: 0x040019EF RID: 6639
		private Stream stream;

		// Token: 0x040019F0 RID: 6640
		private EndPointListener epl;

		// Token: 0x040019F1 RID: 6641
		private MemoryStream ms;

		// Token: 0x040019F2 RID: 6642
		private byte[] buffer;

		// Token: 0x040019F3 RID: 6643
		private HttpListenerContext context;

		// Token: 0x040019F4 RID: 6644
		private StringBuilder current_line;

		// Token: 0x040019F5 RID: 6645
		private ListenerPrefix prefix;

		// Token: 0x040019F6 RID: 6646
		private RequestStream i_stream;

		// Token: 0x040019F7 RID: 6647
		private ResponseStream o_stream;

		// Token: 0x040019F8 RID: 6648
		private bool chunked;

		// Token: 0x040019F9 RID: 6649
		private int reuses;

		// Token: 0x040019FA RID: 6650
		private bool context_bound;

		// Token: 0x040019FB RID: 6651
		private bool secure;

		// Token: 0x040019FC RID: 6652
		private X509Certificate cert;

		// Token: 0x040019FD RID: 6653
		private int s_timeout = 90000;

		// Token: 0x040019FE RID: 6654
		private Timer timer;

		// Token: 0x040019FF RID: 6655
		private IPEndPoint local_ep;

		// Token: 0x04001A00 RID: 6656
		private HttpListener last_listener;

		// Token: 0x04001A01 RID: 6657
		private int[] client_cert_errors;

		// Token: 0x04001A02 RID: 6658
		private X509Certificate2 client_cert;

		// Token: 0x04001A03 RID: 6659
		private SslStream ssl_stream;

		// Token: 0x04001A04 RID: 6660
		private HttpConnection.InputState input_state;

		// Token: 0x04001A05 RID: 6661
		private HttpConnection.LineState line_state;

		// Token: 0x04001A06 RID: 6662
		private int position;

		// Token: 0x020003DE RID: 990
		private enum InputState
		{
			// Token: 0x04001A08 RID: 6664
			RequestLine,
			// Token: 0x04001A09 RID: 6665
			Headers
		}

		// Token: 0x020003DF RID: 991
		private enum LineState
		{
			// Token: 0x04001A0B RID: 6667
			None,
			// Token: 0x04001A0C RID: 6668
			CR,
			// Token: 0x04001A0D RID: 6669
			LF
		}
	}
}
