﻿using System;
using System.Security.Authentication;

namespace System.Net
{
	// Token: 0x020002D9 RID: 729
	internal static class SecurityProtocol
	{
		// Token: 0x060015E7 RID: 5607 RVA: 0x0004F10D File Offset: 0x0004D30D
		public static void ThrowOnNotAllowed(SslProtocols protocols, bool allowNone = true)
		{
			if ((!allowNone && protocols == SslProtocols.None) || (protocols & ~(SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12)) != SslProtocols.None)
			{
				throw new NotSupportedException("The requested security protocol is not supported.");
			}
		}

		// Token: 0x0400141E RID: 5150
		public const SslProtocols AllowedSecurityProtocols = SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12;

		// Token: 0x0400141F RID: 5151
		public const SslProtocols DefaultSecurityProtocols = SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12;

		// Token: 0x04001420 RID: 5152
		public const SslProtocols SystemDefaultSecurityProtocols = SslProtocols.None;
	}
}
