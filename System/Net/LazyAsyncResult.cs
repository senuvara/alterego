﻿using System;
using System.Diagnostics;
using System.Threading;

namespace System.Net
{
	// Token: 0x0200034F RID: 847
	internal class LazyAsyncResult : IAsyncResult
	{
		// Token: 0x170004FA RID: 1274
		// (get) Token: 0x0600182E RID: 6190 RVA: 0x00057E48 File Offset: 0x00056048
		private static LazyAsyncResult.ThreadContext CurrentThreadContext
		{
			get
			{
				LazyAsyncResult.ThreadContext threadContext = LazyAsyncResult.t_ThreadContext;
				if (threadContext == null)
				{
					threadContext = new LazyAsyncResult.ThreadContext();
					LazyAsyncResult.t_ThreadContext = threadContext;
				}
				return threadContext;
			}
		}

		// Token: 0x0600182F RID: 6191 RVA: 0x00057E6B File Offset: 0x0005606B
		internal LazyAsyncResult(object myObject, object myState, AsyncCallback myCallBack)
		{
			this.m_AsyncObject = myObject;
			this.m_AsyncState = myState;
			this.m_AsyncCallback = myCallBack;
			this.m_Result = DBNull.Value;
		}

		// Token: 0x06001830 RID: 6192 RVA: 0x00057E93 File Offset: 0x00056093
		internal LazyAsyncResult(object myObject, object myState, AsyncCallback myCallBack, object result)
		{
			this.m_AsyncObject = myObject;
			this.m_AsyncState = myState;
			this.m_AsyncCallback = myCallBack;
			this.m_Result = result;
			this.m_IntCompleted = 1;
			if (this.m_AsyncCallback != null)
			{
				this.m_AsyncCallback(this);
			}
		}

		// Token: 0x170004FB RID: 1275
		// (get) Token: 0x06001831 RID: 6193 RVA: 0x00057ED3 File Offset: 0x000560D3
		internal object AsyncObject
		{
			get
			{
				return this.m_AsyncObject;
			}
		}

		// Token: 0x170004FC RID: 1276
		// (get) Token: 0x06001832 RID: 6194 RVA: 0x00057EDB File Offset: 0x000560DB
		public object AsyncState
		{
			get
			{
				return this.m_AsyncState;
			}
		}

		// Token: 0x170004FD RID: 1277
		// (get) Token: 0x06001833 RID: 6195 RVA: 0x00057EE3 File Offset: 0x000560E3
		// (set) Token: 0x06001834 RID: 6196 RVA: 0x00057EEB File Offset: 0x000560EB
		protected AsyncCallback AsyncCallback
		{
			get
			{
				return this.m_AsyncCallback;
			}
			set
			{
				this.m_AsyncCallback = value;
			}
		}

		// Token: 0x170004FE RID: 1278
		// (get) Token: 0x06001835 RID: 6197 RVA: 0x00057EF4 File Offset: 0x000560F4
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				this.m_UserEvent = true;
				if (this.m_IntCompleted == 0)
				{
					Interlocked.CompareExchange(ref this.m_IntCompleted, int.MinValue, 0);
				}
				ManualResetEvent manualResetEvent = (ManualResetEvent)this.m_Event;
				while (manualResetEvent == null)
				{
					this.LazilyCreateEvent(out manualResetEvent);
				}
				return manualResetEvent;
			}
		}

		// Token: 0x06001836 RID: 6198 RVA: 0x00057F40 File Offset: 0x00056140
		private bool LazilyCreateEvent(out ManualResetEvent waitHandle)
		{
			waitHandle = new ManualResetEvent(false);
			bool result;
			try
			{
				if (Interlocked.CompareExchange(ref this.m_Event, waitHandle, null) == null)
				{
					if (this.InternalPeekCompleted)
					{
						waitHandle.Set();
					}
					result = true;
				}
				else
				{
					waitHandle.Close();
					waitHandle = (ManualResetEvent)this.m_Event;
					result = false;
				}
			}
			catch
			{
				this.m_Event = null;
				if (waitHandle != null)
				{
					waitHandle.Close();
				}
				throw;
			}
			return result;
		}

		// Token: 0x06001837 RID: 6199 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("DEBUG")]
		protected void DebugProtectState(bool protect)
		{
		}

		// Token: 0x170004FF RID: 1279
		// (get) Token: 0x06001838 RID: 6200 RVA: 0x00057FB8 File Offset: 0x000561B8
		public bool CompletedSynchronously
		{
			get
			{
				int num = this.m_IntCompleted;
				if (num == 0)
				{
					num = Interlocked.CompareExchange(ref this.m_IntCompleted, int.MinValue, 0);
				}
				return num > 0;
			}
		}

		// Token: 0x17000500 RID: 1280
		// (get) Token: 0x06001839 RID: 6201 RVA: 0x00057FE8 File Offset: 0x000561E8
		public bool IsCompleted
		{
			get
			{
				int num = this.m_IntCompleted;
				if (num == 0)
				{
					num = Interlocked.CompareExchange(ref this.m_IntCompleted, int.MinValue, 0);
				}
				return (num & int.MaxValue) != 0;
			}
		}

		// Token: 0x17000501 RID: 1281
		// (get) Token: 0x0600183A RID: 6202 RVA: 0x0005801B File Offset: 0x0005621B
		internal bool InternalPeekCompleted
		{
			get
			{
				return (this.m_IntCompleted & int.MaxValue) != 0;
			}
		}

		// Token: 0x17000502 RID: 1282
		// (get) Token: 0x0600183B RID: 6203 RVA: 0x0005802C File Offset: 0x0005622C
		// (set) Token: 0x0600183C RID: 6204 RVA: 0x00058043 File Offset: 0x00056243
		internal object Result
		{
			get
			{
				if (this.m_Result != DBNull.Value)
				{
					return this.m_Result;
				}
				return null;
			}
			set
			{
				this.m_Result = value;
			}
		}

		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x0600183D RID: 6205 RVA: 0x0005804C File Offset: 0x0005624C
		// (set) Token: 0x0600183E RID: 6206 RVA: 0x00058054 File Offset: 0x00056254
		internal bool EndCalled
		{
			get
			{
				return this.m_EndCalled;
			}
			set
			{
				this.m_EndCalled = value;
			}
		}

		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x0600183F RID: 6207 RVA: 0x0005805D File Offset: 0x0005625D
		// (set) Token: 0x06001840 RID: 6208 RVA: 0x00058065 File Offset: 0x00056265
		internal int ErrorCode
		{
			get
			{
				return this.m_ErrorCode;
			}
			set
			{
				this.m_ErrorCode = value;
			}
		}

		// Token: 0x06001841 RID: 6209 RVA: 0x00058070 File Offset: 0x00056270
		protected void ProtectedInvokeCallback(object result, IntPtr userToken)
		{
			if (result == DBNull.Value)
			{
				throw new ArgumentNullException("result");
			}
			if ((this.m_IntCompleted & 2147483647) == 0 && (Interlocked.Increment(ref this.m_IntCompleted) & 2147483647) == 1)
			{
				if (this.m_Result == DBNull.Value)
				{
					this.m_Result = result;
				}
				ManualResetEvent manualResetEvent = (ManualResetEvent)this.m_Event;
				if (manualResetEvent != null)
				{
					try
					{
						manualResetEvent.Set();
					}
					catch (ObjectDisposedException)
					{
					}
				}
				this.Complete(userToken);
			}
		}

		// Token: 0x06001842 RID: 6210 RVA: 0x000580F8 File Offset: 0x000562F8
		internal void InvokeCallback(object result)
		{
			this.ProtectedInvokeCallback(result, IntPtr.Zero);
		}

		// Token: 0x06001843 RID: 6211 RVA: 0x00058106 File Offset: 0x00056306
		internal void InvokeCallback()
		{
			this.ProtectedInvokeCallback(null, IntPtr.Zero);
		}

		// Token: 0x06001844 RID: 6212 RVA: 0x00058114 File Offset: 0x00056314
		protected virtual void Complete(IntPtr userToken)
		{
			bool flag = false;
			LazyAsyncResult.ThreadContext currentThreadContext = LazyAsyncResult.CurrentThreadContext;
			try
			{
				currentThreadContext.m_NestedIOCount++;
				if (this.m_AsyncCallback != null)
				{
					if (currentThreadContext.m_NestedIOCount >= 50)
					{
						ThreadPool.QueueUserWorkItem(new WaitCallback(this.WorkerThreadComplete));
						flag = true;
					}
					else
					{
						this.m_AsyncCallback(this);
					}
				}
			}
			finally
			{
				currentThreadContext.m_NestedIOCount--;
				if (!flag)
				{
					this.Cleanup();
				}
			}
		}

		// Token: 0x06001845 RID: 6213 RVA: 0x00058198 File Offset: 0x00056398
		private void WorkerThreadComplete(object state)
		{
			try
			{
				this.m_AsyncCallback(this);
			}
			finally
			{
				this.Cleanup();
			}
		}

		// Token: 0x06001846 RID: 6214 RVA: 0x0000232D File Offset: 0x0000052D
		protected virtual void Cleanup()
		{
		}

		// Token: 0x06001847 RID: 6215 RVA: 0x000581CC File Offset: 0x000563CC
		internal object InternalWaitForCompletion()
		{
			return this.WaitForCompletion(true);
		}

		// Token: 0x06001848 RID: 6216 RVA: 0x000581D8 File Offset: 0x000563D8
		private object WaitForCompletion(bool snap)
		{
			ManualResetEvent manualResetEvent = null;
			bool flag = false;
			if (!(snap ? this.IsCompleted : this.InternalPeekCompleted))
			{
				manualResetEvent = (ManualResetEvent)this.m_Event;
				if (manualResetEvent == null)
				{
					flag = this.LazilyCreateEvent(out manualResetEvent);
				}
			}
			if (manualResetEvent == null)
			{
				goto IL_73;
			}
			try
			{
				manualResetEvent.WaitOne(-1, false);
				goto IL_73;
			}
			catch (ObjectDisposedException)
			{
				goto IL_73;
			}
			finally
			{
				if (flag && !this.m_UserEvent)
				{
					ManualResetEvent manualResetEvent2 = (ManualResetEvent)this.m_Event;
					this.m_Event = null;
					if (!this.m_UserEvent)
					{
						manualResetEvent2.Close();
					}
				}
			}
			IL_6D:
			Thread.SpinWait(1);
			IL_73:
			if (this.m_Result != DBNull.Value)
			{
				return this.m_Result;
			}
			goto IL_6D;
		}

		// Token: 0x06001849 RID: 6217 RVA: 0x00058288 File Offset: 0x00056488
		internal void InternalCleanup()
		{
			if ((this.m_IntCompleted & 2147483647) == 0 && (Interlocked.Increment(ref this.m_IntCompleted) & 2147483647) == 1)
			{
				this.m_Result = null;
				this.Cleanup();
			}
		}

		// Token: 0x04001765 RID: 5989
		private const int c_HighBit = -2147483648;

		// Token: 0x04001766 RID: 5990
		private const int c_ForceAsyncCount = 50;

		// Token: 0x04001767 RID: 5991
		[ThreadStatic]
		private static LazyAsyncResult.ThreadContext t_ThreadContext;

		// Token: 0x04001768 RID: 5992
		private object m_AsyncObject;

		// Token: 0x04001769 RID: 5993
		private object m_AsyncState;

		// Token: 0x0400176A RID: 5994
		private AsyncCallback m_AsyncCallback;

		// Token: 0x0400176B RID: 5995
		private object m_Result;

		// Token: 0x0400176C RID: 5996
		private int m_ErrorCode;

		// Token: 0x0400176D RID: 5997
		private int m_IntCompleted;

		// Token: 0x0400176E RID: 5998
		private bool m_EndCalled;

		// Token: 0x0400176F RID: 5999
		private bool m_UserEvent;

		// Token: 0x04001770 RID: 6000
		private object m_Event;

		// Token: 0x02000350 RID: 848
		private class ThreadContext
		{
			// Token: 0x0600184A RID: 6218 RVA: 0x0000232F File Offset: 0x0000052F
			public ThreadContext()
			{
			}

			// Token: 0x04001771 RID: 6001
			internal int m_NestedIOCount;
		}
	}
}
