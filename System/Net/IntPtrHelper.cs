﻿using System;

namespace System.Net
{
	// Token: 0x020002F9 RID: 761
	internal static class IntPtrHelper
	{
		// Token: 0x06001680 RID: 5760 RVA: 0x00051045 File Offset: 0x0004F245
		internal static IntPtr Add(IntPtr a, int b)
		{
			return (IntPtr)((long)a + (long)b);
		}

		// Token: 0x06001681 RID: 5761 RVA: 0x00051055 File Offset: 0x0004F255
		internal static long Subtract(IntPtr a, IntPtr b)
		{
			return (long)a - (long)b;
		}
	}
}
