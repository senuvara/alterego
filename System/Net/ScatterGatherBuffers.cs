﻿using System;

namespace System.Net
{
	// Token: 0x0200035C RID: 860
	internal class ScatterGatherBuffers
	{
		// Token: 0x060018C7 RID: 6343 RVA: 0x00058F58 File Offset: 0x00057158
		internal ScatterGatherBuffers()
		{
		}

		// Token: 0x060018C8 RID: 6344 RVA: 0x00058F6B File Offset: 0x0005716B
		internal ScatterGatherBuffers(long totalSize)
		{
			if (totalSize > 0L)
			{
				this.currentChunk = this.AllocateMemoryChunk((totalSize > 2147483647L) ? int.MaxValue : ((int)totalSize));
			}
		}

		// Token: 0x060018C9 RID: 6345 RVA: 0x00058FA4 File Offset: 0x000571A4
		internal BufferOffsetSize[] GetBuffers()
		{
			if (this.Empty)
			{
				return null;
			}
			BufferOffsetSize[] array = new BufferOffsetSize[this.chunkCount];
			int num = 0;
			for (ScatterGatherBuffers.MemoryChunk next = this.headChunk; next != null; next = next.Next)
			{
				array[num] = new BufferOffsetSize(next.Buffer, 0, next.FreeOffset, false);
				num++;
			}
			return array;
		}

		// Token: 0x1700051E RID: 1310
		// (get) Token: 0x060018CA RID: 6346 RVA: 0x00058FF7 File Offset: 0x000571F7
		private bool Empty
		{
			get
			{
				return this.headChunk == null || this.chunkCount == 0;
			}
		}

		// Token: 0x1700051F RID: 1311
		// (get) Token: 0x060018CB RID: 6347 RVA: 0x0005900C File Offset: 0x0005720C
		internal int Length
		{
			get
			{
				return this.totalLength;
			}
		}

		// Token: 0x060018CC RID: 6348 RVA: 0x00059014 File Offset: 0x00057214
		internal void Write(byte[] buffer, int offset, int count)
		{
			while (count > 0)
			{
				int num = this.Empty ? 0 : (this.currentChunk.Buffer.Length - this.currentChunk.FreeOffset);
				if (num == 0)
				{
					ScatterGatherBuffers.MemoryChunk next = this.AllocateMemoryChunk(count);
					if (this.currentChunk != null)
					{
						this.currentChunk.Next = next;
					}
					this.currentChunk = next;
				}
				int num2 = (count < num) ? count : num;
				Buffer.BlockCopy(buffer, offset, this.currentChunk.Buffer, this.currentChunk.FreeOffset, num2);
				offset += num2;
				count -= num2;
				this.totalLength += num2;
				this.currentChunk.FreeOffset += num2;
			}
		}

		// Token: 0x060018CD RID: 6349 RVA: 0x000590CC File Offset: 0x000572CC
		private ScatterGatherBuffers.MemoryChunk AllocateMemoryChunk(int newSize)
		{
			if (newSize > this.nextChunkLength)
			{
				this.nextChunkLength = newSize;
			}
			ScatterGatherBuffers.MemoryChunk result = new ScatterGatherBuffers.MemoryChunk(this.nextChunkLength);
			if (this.Empty)
			{
				this.headChunk = result;
			}
			this.nextChunkLength *= 2;
			this.chunkCount++;
			return result;
		}

		// Token: 0x040017A0 RID: 6048
		private ScatterGatherBuffers.MemoryChunk headChunk;

		// Token: 0x040017A1 RID: 6049
		private ScatterGatherBuffers.MemoryChunk currentChunk;

		// Token: 0x040017A2 RID: 6050
		private int nextChunkLength = 1024;

		// Token: 0x040017A3 RID: 6051
		private int totalLength;

		// Token: 0x040017A4 RID: 6052
		private int chunkCount;

		// Token: 0x0200035D RID: 861
		private class MemoryChunk
		{
			// Token: 0x060018CE RID: 6350 RVA: 0x00059121 File Offset: 0x00057321
			internal MemoryChunk(int bufferSize)
			{
				this.Buffer = new byte[bufferSize];
			}

			// Token: 0x040017A5 RID: 6053
			internal byte[] Buffer;

			// Token: 0x040017A6 RID: 6054
			internal int FreeOffset;

			// Token: 0x040017A7 RID: 6055
			internal ScatterGatherBuffers.MemoryChunk Next;
		}
	}
}
