﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004E2 RID: 1250
	internal class Win32IcmpV4Statistics : IcmpV4Statistics
	{
		// Token: 0x06002878 RID: 10360 RVA: 0x0008E778 File Offset: 0x0008C978
		public Win32IcmpV4Statistics(Win32_MIBICMPINFO info)
		{
			this.iin = info.InStats;
			this.iout = info.OutStats;
		}

		// Token: 0x170009AA RID: 2474
		// (get) Token: 0x06002879 RID: 10361 RVA: 0x0008E798 File Offset: 0x0008C998
		public override long AddressMaskRepliesReceived
		{
			get
			{
				return (long)((ulong)this.iin.AddrMaskReps);
			}
		}

		// Token: 0x170009AB RID: 2475
		// (get) Token: 0x0600287A RID: 10362 RVA: 0x0008E7A6 File Offset: 0x0008C9A6
		public override long AddressMaskRepliesSent
		{
			get
			{
				return (long)((ulong)this.iout.AddrMaskReps);
			}
		}

		// Token: 0x170009AC RID: 2476
		// (get) Token: 0x0600287B RID: 10363 RVA: 0x0008E7B4 File Offset: 0x0008C9B4
		public override long AddressMaskRequestsReceived
		{
			get
			{
				return (long)((ulong)this.iin.AddrMasks);
			}
		}

		// Token: 0x170009AD RID: 2477
		// (get) Token: 0x0600287C RID: 10364 RVA: 0x0008E7C2 File Offset: 0x0008C9C2
		public override long AddressMaskRequestsSent
		{
			get
			{
				return (long)((ulong)this.iout.AddrMasks);
			}
		}

		// Token: 0x170009AE RID: 2478
		// (get) Token: 0x0600287D RID: 10365 RVA: 0x0008E7D0 File Offset: 0x0008C9D0
		public override long DestinationUnreachableMessagesReceived
		{
			get
			{
				return (long)((ulong)this.iin.DestUnreachs);
			}
		}

		// Token: 0x170009AF RID: 2479
		// (get) Token: 0x0600287E RID: 10366 RVA: 0x0008E7DE File Offset: 0x0008C9DE
		public override long DestinationUnreachableMessagesSent
		{
			get
			{
				return (long)((ulong)this.iout.DestUnreachs);
			}
		}

		// Token: 0x170009B0 RID: 2480
		// (get) Token: 0x0600287F RID: 10367 RVA: 0x0008E7EC File Offset: 0x0008C9EC
		public override long EchoRepliesReceived
		{
			get
			{
				return (long)((ulong)this.iin.EchoReps);
			}
		}

		// Token: 0x170009B1 RID: 2481
		// (get) Token: 0x06002880 RID: 10368 RVA: 0x0008E7FA File Offset: 0x0008C9FA
		public override long EchoRepliesSent
		{
			get
			{
				return (long)((ulong)this.iout.EchoReps);
			}
		}

		// Token: 0x170009B2 RID: 2482
		// (get) Token: 0x06002881 RID: 10369 RVA: 0x0008E808 File Offset: 0x0008CA08
		public override long EchoRequestsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Echos);
			}
		}

		// Token: 0x170009B3 RID: 2483
		// (get) Token: 0x06002882 RID: 10370 RVA: 0x0008E816 File Offset: 0x0008CA16
		public override long EchoRequestsSent
		{
			get
			{
				return (long)((ulong)this.iout.Echos);
			}
		}

		// Token: 0x170009B4 RID: 2484
		// (get) Token: 0x06002883 RID: 10371 RVA: 0x0008E824 File Offset: 0x0008CA24
		public override long ErrorsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Errors);
			}
		}

		// Token: 0x170009B5 RID: 2485
		// (get) Token: 0x06002884 RID: 10372 RVA: 0x0008E832 File Offset: 0x0008CA32
		public override long ErrorsSent
		{
			get
			{
				return (long)((ulong)this.iout.Errors);
			}
		}

		// Token: 0x170009B6 RID: 2486
		// (get) Token: 0x06002885 RID: 10373 RVA: 0x0008E840 File Offset: 0x0008CA40
		public override long MessagesReceived
		{
			get
			{
				return (long)((ulong)this.iin.Msgs);
			}
		}

		// Token: 0x170009B7 RID: 2487
		// (get) Token: 0x06002886 RID: 10374 RVA: 0x0008E84E File Offset: 0x0008CA4E
		public override long MessagesSent
		{
			get
			{
				return (long)((ulong)this.iout.Msgs);
			}
		}

		// Token: 0x170009B8 RID: 2488
		// (get) Token: 0x06002887 RID: 10375 RVA: 0x0008E85C File Offset: 0x0008CA5C
		public override long ParameterProblemsReceived
		{
			get
			{
				return (long)((ulong)this.iin.ParmProbs);
			}
		}

		// Token: 0x170009B9 RID: 2489
		// (get) Token: 0x06002888 RID: 10376 RVA: 0x0008E86A File Offset: 0x0008CA6A
		public override long ParameterProblemsSent
		{
			get
			{
				return (long)((ulong)this.iout.ParmProbs);
			}
		}

		// Token: 0x170009BA RID: 2490
		// (get) Token: 0x06002889 RID: 10377 RVA: 0x0008E878 File Offset: 0x0008CA78
		public override long RedirectsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Redirects);
			}
		}

		// Token: 0x170009BB RID: 2491
		// (get) Token: 0x0600288A RID: 10378 RVA: 0x0008E886 File Offset: 0x0008CA86
		public override long RedirectsSent
		{
			get
			{
				return (long)((ulong)this.iout.Redirects);
			}
		}

		// Token: 0x170009BC RID: 2492
		// (get) Token: 0x0600288B RID: 10379 RVA: 0x0008E894 File Offset: 0x0008CA94
		public override long SourceQuenchesReceived
		{
			get
			{
				return (long)((ulong)this.iin.SrcQuenchs);
			}
		}

		// Token: 0x170009BD RID: 2493
		// (get) Token: 0x0600288C RID: 10380 RVA: 0x0008E8A2 File Offset: 0x0008CAA2
		public override long SourceQuenchesSent
		{
			get
			{
				return (long)((ulong)this.iout.SrcQuenchs);
			}
		}

		// Token: 0x170009BE RID: 2494
		// (get) Token: 0x0600288D RID: 10381 RVA: 0x0008E8B0 File Offset: 0x0008CAB0
		public override long TimeExceededMessagesReceived
		{
			get
			{
				return (long)((ulong)this.iin.TimeExcds);
			}
		}

		// Token: 0x170009BF RID: 2495
		// (get) Token: 0x0600288E RID: 10382 RVA: 0x0008E8BE File Offset: 0x0008CABE
		public override long TimeExceededMessagesSent
		{
			get
			{
				return (long)((ulong)this.iout.TimeExcds);
			}
		}

		// Token: 0x170009C0 RID: 2496
		// (get) Token: 0x0600288F RID: 10383 RVA: 0x0008E8CC File Offset: 0x0008CACC
		public override long TimestampRepliesReceived
		{
			get
			{
				return (long)((ulong)this.iin.TimestampReps);
			}
		}

		// Token: 0x170009C1 RID: 2497
		// (get) Token: 0x06002890 RID: 10384 RVA: 0x0008E8DA File Offset: 0x0008CADA
		public override long TimestampRepliesSent
		{
			get
			{
				return (long)((ulong)this.iout.TimestampReps);
			}
		}

		// Token: 0x170009C2 RID: 2498
		// (get) Token: 0x06002891 RID: 10385 RVA: 0x0008E8E8 File Offset: 0x0008CAE8
		public override long TimestampRequestsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Timestamps);
			}
		}

		// Token: 0x170009C3 RID: 2499
		// (get) Token: 0x06002892 RID: 10386 RVA: 0x0008E8F6 File Offset: 0x0008CAF6
		public override long TimestampRequestsSent
		{
			get
			{
				return (long)((ulong)this.iout.Timestamps);
			}
		}

		// Token: 0x04002004 RID: 8196
		private Win32_MIBICMPSTATS iin;

		// Token: 0x04002005 RID: 8197
		private Win32_MIBICMPSTATS iout;
	}
}
