﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004EA RID: 1258
	[StructLayout(LayoutKind.Explicit)]
	internal struct ifa_ifu
	{
		// Token: 0x0400202C RID: 8236
		[FieldOffset(0)]
		public IntPtr ifu_broadaddr;

		// Token: 0x0400202D RID: 8237
		[FieldOffset(0)]
		public IntPtr ifu_dstaddr;
	}
}
