﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000511 RID: 1297
	internal struct Win32_MIB_TCPSTATS
	{
		// Token: 0x040020D3 RID: 8403
		public uint RtoAlgorithm;

		// Token: 0x040020D4 RID: 8404
		public uint RtoMin;

		// Token: 0x040020D5 RID: 8405
		public uint RtoMax;

		// Token: 0x040020D6 RID: 8406
		public uint MaxConn;

		// Token: 0x040020D7 RID: 8407
		public uint ActiveOpens;

		// Token: 0x040020D8 RID: 8408
		public uint PassiveOpens;

		// Token: 0x040020D9 RID: 8409
		public uint AttemptFails;

		// Token: 0x040020DA RID: 8410
		public uint EstabResets;

		// Token: 0x040020DB RID: 8411
		public uint CurrEstab;

		// Token: 0x040020DC RID: 8412
		public uint InSegs;

		// Token: 0x040020DD RID: 8413
		public uint OutSegs;

		// Token: 0x040020DE RID: 8414
		public uint RetransSegs;

		// Token: 0x040020DF RID: 8415
		public uint InErrs;

		// Token: 0x040020E0 RID: 8416
		public uint OutRsts;

		// Token: 0x040020E1 RID: 8417
		public uint NumConns;
	}
}
