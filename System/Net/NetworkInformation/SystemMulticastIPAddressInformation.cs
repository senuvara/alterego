﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004BC RID: 1212
	internal class SystemMulticastIPAddressInformation : MulticastIPAddressInformation
	{
		// Token: 0x0600272C RID: 10028 RVA: 0x0008C748 File Offset: 0x0008A948
		private SystemMulticastIPAddressInformation()
		{
		}

		// Token: 0x0600272D RID: 10029 RVA: 0x0008C750 File Offset: 0x0008A950
		public SystemMulticastIPAddressInformation(SystemIPAddressInformation addressInfo)
		{
			this.innerInfo = addressInfo;
		}

		// Token: 0x170008DA RID: 2266
		// (get) Token: 0x0600272E RID: 10030 RVA: 0x0008C75F File Offset: 0x0008A95F
		public override IPAddress Address
		{
			get
			{
				return this.innerInfo.Address;
			}
		}

		// Token: 0x170008DB RID: 2267
		// (get) Token: 0x0600272F RID: 10031 RVA: 0x0008C76C File Offset: 0x0008A96C
		public override bool IsTransient
		{
			get
			{
				return this.innerInfo.IsTransient;
			}
		}

		// Token: 0x170008DC RID: 2268
		// (get) Token: 0x06002730 RID: 10032 RVA: 0x0008C779 File Offset: 0x0008A979
		public override bool IsDnsEligible
		{
			get
			{
				return this.innerInfo.IsDnsEligible;
			}
		}

		// Token: 0x170008DD RID: 2269
		// (get) Token: 0x06002731 RID: 10033 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override PrefixOrigin PrefixOrigin
		{
			get
			{
				return PrefixOrigin.Other;
			}
		}

		// Token: 0x170008DE RID: 2270
		// (get) Token: 0x06002732 RID: 10034 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override SuffixOrigin SuffixOrigin
		{
			get
			{
				return SuffixOrigin.Other;
			}
		}

		// Token: 0x170008DF RID: 2271
		// (get) Token: 0x06002733 RID: 10035 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override DuplicateAddressDetectionState DuplicateAddressDetectionState
		{
			get
			{
				return DuplicateAddressDetectionState.Invalid;
			}
		}

		// Token: 0x170008E0 RID: 2272
		// (get) Token: 0x06002734 RID: 10036 RVA: 0x0002873C File Offset: 0x0002693C
		public override long AddressValidLifetime
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x170008E1 RID: 2273
		// (get) Token: 0x06002735 RID: 10037 RVA: 0x0002873C File Offset: 0x0002693C
		public override long AddressPreferredLifetime
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x170008E2 RID: 2274
		// (get) Token: 0x06002736 RID: 10038 RVA: 0x0002873C File Offset: 0x0002693C
		public override long DhcpLeaseLifetime
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x06002737 RID: 10039 RVA: 0x0008C788 File Offset: 0x0008A988
		internal static MulticastIPAddressInformationCollection ToMulticastIpAddressInformationCollection(IPAddressInformationCollection addresses)
		{
			MulticastIPAddressInformationCollection multicastIPAddressInformationCollection = new MulticastIPAddressInformationCollection();
			foreach (IPAddressInformation ipaddressInformation in addresses)
			{
				multicastIPAddressInformationCollection.InternalAdd(new SystemMulticastIPAddressInformation((SystemIPAddressInformation)ipaddressInformation));
			}
			return multicastIPAddressInformationCollection;
		}

		// Token: 0x04001F80 RID: 8064
		private SystemIPAddressInformation innerInfo;
	}
}
