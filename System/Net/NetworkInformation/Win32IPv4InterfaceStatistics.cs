﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004DD RID: 1245
	internal class Win32IPv4InterfaceStatistics : IPv4InterfaceStatistics
	{
		// Token: 0x06002831 RID: 10289 RVA: 0x0008E434 File Offset: 0x0008C634
		public Win32IPv4InterfaceStatistics(Win32_MIB_IFROW info)
		{
			this.info = info;
		}

		// Token: 0x1700096A RID: 2410
		// (get) Token: 0x06002832 RID: 10290 RVA: 0x0008E443 File Offset: 0x0008C643
		public override long BytesReceived
		{
			get
			{
				return (long)this.info.InOctets;
			}
		}

		// Token: 0x1700096B RID: 2411
		// (get) Token: 0x06002833 RID: 10291 RVA: 0x0008E451 File Offset: 0x0008C651
		public override long BytesSent
		{
			get
			{
				return (long)this.info.OutOctets;
			}
		}

		// Token: 0x1700096C RID: 2412
		// (get) Token: 0x06002834 RID: 10292 RVA: 0x0008E45F File Offset: 0x0008C65F
		public override long IncomingPacketsDiscarded
		{
			get
			{
				return (long)this.info.InDiscards;
			}
		}

		// Token: 0x1700096D RID: 2413
		// (get) Token: 0x06002835 RID: 10293 RVA: 0x0008E46D File Offset: 0x0008C66D
		public override long IncomingPacketsWithErrors
		{
			get
			{
				return (long)this.info.InErrors;
			}
		}

		// Token: 0x1700096E RID: 2414
		// (get) Token: 0x06002836 RID: 10294 RVA: 0x0008E47B File Offset: 0x0008C67B
		public override long IncomingUnknownProtocolPackets
		{
			get
			{
				return (long)this.info.InUnknownProtos;
			}
		}

		// Token: 0x1700096F RID: 2415
		// (get) Token: 0x06002837 RID: 10295 RVA: 0x0008E489 File Offset: 0x0008C689
		public override long NonUnicastPacketsReceived
		{
			get
			{
				return (long)this.info.InNUcastPkts;
			}
		}

		// Token: 0x17000970 RID: 2416
		// (get) Token: 0x06002838 RID: 10296 RVA: 0x0008E497 File Offset: 0x0008C697
		public override long NonUnicastPacketsSent
		{
			get
			{
				return (long)this.info.OutNUcastPkts;
			}
		}

		// Token: 0x17000971 RID: 2417
		// (get) Token: 0x06002839 RID: 10297 RVA: 0x0008E4A5 File Offset: 0x0008C6A5
		public override long OutgoingPacketsDiscarded
		{
			get
			{
				return (long)this.info.OutDiscards;
			}
		}

		// Token: 0x17000972 RID: 2418
		// (get) Token: 0x0600283A RID: 10298 RVA: 0x0008E4B3 File Offset: 0x0008C6B3
		public override long OutgoingPacketsWithErrors
		{
			get
			{
				return (long)this.info.OutErrors;
			}
		}

		// Token: 0x17000973 RID: 2419
		// (get) Token: 0x0600283B RID: 10299 RVA: 0x0008E4C1 File Offset: 0x0008C6C1
		public override long OutputQueueLength
		{
			get
			{
				return (long)this.info.OutQLen;
			}
		}

		// Token: 0x17000974 RID: 2420
		// (get) Token: 0x0600283C RID: 10300 RVA: 0x0008E4CF File Offset: 0x0008C6CF
		public override long UnicastPacketsReceived
		{
			get
			{
				return (long)this.info.InUcastPkts;
			}
		}

		// Token: 0x17000975 RID: 2421
		// (get) Token: 0x0600283D RID: 10301 RVA: 0x0008E4DD File Offset: 0x0008C6DD
		public override long UnicastPacketsSent
		{
			get
			{
				return (long)this.info.OutUcastPkts;
			}
		}

		// Token: 0x04002000 RID: 8192
		private Win32_MIB_IFROW info;
	}
}
