﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Provides information about a network interface address.</summary>
	// Token: 0x02000499 RID: 1177
	public abstract class IPAddressInformation
	{
		/// <summary>Gets the Internet Protocol (IP) address.</summary>
		/// <returns>An <see cref="T:System.Net.IPAddress" /> instance that contains the IP address of an interface.</returns>
		// Token: 0x17000832 RID: 2098
		// (get) Token: 0x0600261C RID: 9756
		public abstract IPAddress Address { get; }

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the Internet Protocol (IP) address is valid to appear in a Domain Name System (DNS) server database.</summary>
		/// <returns>
		///     <see langword="true" /> if the address can appear in a DNS database; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000833 RID: 2099
		// (get) Token: 0x0600261D RID: 9757
		public abstract bool IsDnsEligible { get; }

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the Internet Protocol (IP) address is transient (a cluster address).</summary>
		/// <returns>
		///     <see langword="true" /> if the address is transient; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000834 RID: 2100
		// (get) Token: 0x0600261E RID: 9758
		public abstract bool IsTransient { get; }

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkInformation.IPAddressInformation" /> class.</summary>
		// Token: 0x0600261F RID: 9759 RVA: 0x0000232F File Offset: 0x0000052F
		protected IPAddressInformation()
		{
		}
	}
}
