﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Provides information about network interfaces that support Internet Protocol version 6 (IPv6).</summary>
	// Token: 0x020004A4 RID: 1188
	public abstract class IPv6InterfaceProperties
	{
		/// <summary>Gets the index of the network interface associated with an Internet Protocol version 6 (IPv6) address.</summary>
		/// <returns>An <see cref="T:System.Int32" /> value that contains the index of the network interface for IPv6 address.</returns>
		// Token: 0x1700087C RID: 2172
		// (get) Token: 0x06002689 RID: 9865
		public abstract int Index { get; }

		/// <summary>Gets the maximum transmission unit (MTU) for this network interface.</summary>
		/// <returns>An <see cref="T:System.Int64" /> value that specifies the MTU.</returns>
		// Token: 0x1700087D RID: 2173
		// (get) Token: 0x0600268A RID: 9866
		public abstract int Mtu { get; }

		/// <summary>Gets the scope ID of the network interface associated with an Internet Protocol version 6 (IPv6) address.</summary>
		/// <param name="scopeLevel">The scope level.</param>
		/// <returns>Returns <see cref="T:System.Int64" />.The scope ID of the network interface associated with an IPv6 address.</returns>
		// Token: 0x0600268B RID: 9867 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual long GetScopeId(ScopeLevel scopeLevel)
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkInformation.IPv6InterfaceProperties" /> class.</summary>
		// Token: 0x0600268C RID: 9868 RVA: 0x0000232F File Offset: 0x0000052F
		protected IPv6InterfaceProperties()
		{
		}
	}
}
