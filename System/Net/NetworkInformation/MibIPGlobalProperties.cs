﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Text;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004CA RID: 1226
	internal class MibIPGlobalProperties : UnixIPGlobalProperties
	{
		// Token: 0x0600278B RID: 10123 RVA: 0x0008CB88 File Offset: 0x0008AD88
		public MibIPGlobalProperties(string procDir)
		{
			this.StatisticsFile = Path.Combine(procDir, "net/snmp");
			this.StatisticsFileIPv6 = Path.Combine(procDir, "net/snmp6");
			this.TcpFile = Path.Combine(procDir, "net/tcp");
			this.Tcp6File = Path.Combine(procDir, "net/tcp6");
			this.UdpFile = Path.Combine(procDir, "net/udp");
			this.Udp6File = Path.Combine(procDir, "net/udp6");
		}

		// Token: 0x0600278C RID: 10124 RVA: 0x0008CC04 File Offset: 0x0008AE04
		private StringDictionary GetProperties4(string item)
		{
			string statisticsFile = this.StatisticsFile;
			string text = item + ": ";
			StringDictionary result;
			using (StreamReader streamReader = new StreamReader(statisticsFile, Encoding.ASCII))
			{
				string[] array = null;
				string[] array2 = null;
				string text2 = string.Empty;
				for (;;)
				{
					text2 = streamReader.ReadLine();
					if (!string.IsNullOrEmpty(text2) && text2.Length > text.Length && string.CompareOrdinal(text2, 0, text, 0, text.Length) == 0)
					{
						if (array != null)
						{
							break;
						}
						array = text2.Substring(text.Length).Split(new char[]
						{
							' '
						});
					}
					if (streamReader.EndOfStream)
					{
						goto IL_C3;
					}
				}
				if (array2 != null)
				{
					throw this.CreateException(statisticsFile, string.Format("Found duplicate line for values for the same item '{0}'", item));
				}
				array2 = text2.Substring(text.Length).Split(new char[]
				{
					' '
				});
				IL_C3:
				if (array2 == null)
				{
					throw this.CreateException(statisticsFile, string.Format("No corresponding line was not found for '{0}'", item));
				}
				if (array.Length != array2.Length)
				{
					throw this.CreateException(statisticsFile, string.Format("The counts in the header line and the value line do not match for '{0}'", item));
				}
				StringDictionary stringDictionary = new StringDictionary();
				for (int i = 0; i < array.Length; i++)
				{
					stringDictionary[array[i]] = array2[i];
				}
				result = stringDictionary;
			}
			return result;
		}

		// Token: 0x0600278D RID: 10125 RVA: 0x0008CD60 File Offset: 0x0008AF60
		private StringDictionary GetProperties6(string item)
		{
			if (!File.Exists(this.StatisticsFileIPv6))
			{
				throw new NetworkInformationException();
			}
			string statisticsFileIPv = this.StatisticsFileIPv6;
			StringDictionary result;
			using (StreamReader streamReader = new StreamReader(statisticsFileIPv, Encoding.ASCII))
			{
				StringDictionary stringDictionary = new StringDictionary();
				string text = string.Empty;
				for (;;)
				{
					text = streamReader.ReadLine();
					if (!string.IsNullOrEmpty(text) && text.Length > item.Length && string.CompareOrdinal(text, 0, item, 0, item.Length) == 0)
					{
						int num = text.IndexOfAny(MibIPGlobalProperties.wsChars, item.Length);
						if (num < 0)
						{
							break;
						}
						stringDictionary[text.Substring(item.Length, num - item.Length)] = text.Substring(num + 1).Trim(MibIPGlobalProperties.wsChars);
					}
					if (streamReader.EndOfStream)
					{
						goto Block_8;
					}
				}
				throw this.CreateException(statisticsFileIPv, null);
				Block_8:
				result = stringDictionary;
			}
			return result;
		}

		// Token: 0x0600278E RID: 10126 RVA: 0x0008CE54 File Offset: 0x0008B054
		private Exception CreateException(string file, string msg)
		{
			return new InvalidOperationException(string.Format("Unsupported (unexpected) '{0}' file format. ", file) + msg);
		}

		// Token: 0x0600278F RID: 10127 RVA: 0x0008CE6C File Offset: 0x0008B06C
		private IPEndPoint[] GetLocalAddresses(List<string[]> list)
		{
			IPEndPoint[] array = new IPEndPoint[list.Count];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = this.ToEndpoint(list[i][1]);
			}
			return array;
		}

		// Token: 0x06002790 RID: 10128 RVA: 0x0008CEA8 File Offset: 0x0008B0A8
		private IPEndPoint ToEndpoint(string s)
		{
			int num = s.IndexOf(':');
			int port = int.Parse(s.Substring(num + 1), NumberStyles.HexNumber);
			if (s.Length == 13)
			{
				return new IPEndPoint(long.Parse(s.Substring(0, num), NumberStyles.HexNumber), port);
			}
			byte[] array = new byte[16];
			int num2 = 0;
			while (num2 << 1 < num)
			{
				array[num2] = byte.Parse(s.Substring(num2 << 1, 2), NumberStyles.HexNumber);
				num2++;
			}
			return new IPEndPoint(new IPAddress(array), port);
		}

		// Token: 0x06002791 RID: 10129 RVA: 0x0008CF30 File Offset: 0x0008B130
		private void GetRows(string file, List<string[]> list)
		{
			if (!File.Exists(file))
			{
				return;
			}
			using (StreamReader streamReader = new StreamReader(file, Encoding.ASCII))
			{
				streamReader.ReadLine();
				while (!streamReader.EndOfStream)
				{
					string[] array = streamReader.ReadLine().Split(MibIPGlobalProperties.wsChars, StringSplitOptions.RemoveEmptyEntries);
					if (array.Length < 4)
					{
						throw this.CreateException(file, null);
					}
					list.Add(array);
				}
			}
		}

		// Token: 0x06002792 RID: 10130 RVA: 0x0008CFA8 File Offset: 0x0008B1A8
		public override TcpConnectionInformation[] GetActiveTcpConnections()
		{
			List<string[]> list = new List<string[]>();
			this.GetRows(this.TcpFile, list);
			this.GetRows(this.Tcp6File, list);
			TcpConnectionInformation[] array = new TcpConnectionInformation[list.Count];
			for (int i = 0; i < array.Length; i++)
			{
				IPEndPoint local = this.ToEndpoint(list[i][1]);
				IPEndPoint remote = this.ToEndpoint(list[i][2]);
				TcpState state = (TcpState)int.Parse(list[i][3], NumberStyles.HexNumber);
				array[i] = new SystemTcpConnectionInformation(local, remote, state);
			}
			return array;
		}

		// Token: 0x06002793 RID: 10131 RVA: 0x0008D034 File Offset: 0x0008B234
		public override IPEndPoint[] GetActiveTcpListeners()
		{
			List<string[]> list = new List<string[]>();
			this.GetRows(this.TcpFile, list);
			this.GetRows(this.Tcp6File, list);
			return this.GetLocalAddresses(list);
		}

		// Token: 0x06002794 RID: 10132 RVA: 0x0008D068 File Offset: 0x0008B268
		public override IPEndPoint[] GetActiveUdpListeners()
		{
			List<string[]> list = new List<string[]>();
			this.GetRows(this.UdpFile, list);
			this.GetRows(this.Udp6File, list);
			return this.GetLocalAddresses(list);
		}

		// Token: 0x06002795 RID: 10133 RVA: 0x0008D09C File Offset: 0x0008B29C
		public override IcmpV4Statistics GetIcmpV4Statistics()
		{
			return new MibIcmpV4Statistics(this.GetProperties4("Icmp"));
		}

		// Token: 0x06002796 RID: 10134 RVA: 0x0008D0AE File Offset: 0x0008B2AE
		public override IcmpV6Statistics GetIcmpV6Statistics()
		{
			return new MibIcmpV6Statistics(this.GetProperties6("Icmp6"));
		}

		// Token: 0x06002797 RID: 10135 RVA: 0x0008D0C0 File Offset: 0x0008B2C0
		public override IPGlobalStatistics GetIPv4GlobalStatistics()
		{
			return new MibIPGlobalStatistics(this.GetProperties4("Ip"));
		}

		// Token: 0x06002798 RID: 10136 RVA: 0x0008D0D2 File Offset: 0x0008B2D2
		public override IPGlobalStatistics GetIPv6GlobalStatistics()
		{
			return new MibIPGlobalStatistics(this.GetProperties6("Ip6"));
		}

		// Token: 0x06002799 RID: 10137 RVA: 0x0008D0E4 File Offset: 0x0008B2E4
		public override TcpStatistics GetTcpIPv4Statistics()
		{
			return new MibTcpStatistics(this.GetProperties4("Tcp"));
		}

		// Token: 0x0600279A RID: 10138 RVA: 0x0008D0E4 File Offset: 0x0008B2E4
		public override TcpStatistics GetTcpIPv6Statistics()
		{
			return new MibTcpStatistics(this.GetProperties4("Tcp"));
		}

		// Token: 0x0600279B RID: 10139 RVA: 0x0008D0F6 File Offset: 0x0008B2F6
		public override UdpStatistics GetUdpIPv4Statistics()
		{
			return new MibUdpStatistics(this.GetProperties4("Udp"));
		}

		// Token: 0x0600279C RID: 10140 RVA: 0x0008D108 File Offset: 0x0008B308
		public override UdpStatistics GetUdpIPv6Statistics()
		{
			return new MibUdpStatistics(this.GetProperties6("Udp6"));
		}

		// Token: 0x0600279D RID: 10141 RVA: 0x0008D11A File Offset: 0x0008B31A
		// Note: this type is marked as 'beforefieldinit'.
		static MibIPGlobalProperties()
		{
		}

		// Token: 0x04001FB7 RID: 8119
		public const string ProcDir = "/proc";

		// Token: 0x04001FB8 RID: 8120
		public const string CompatProcDir = "/usr/compat/linux/proc";

		// Token: 0x04001FB9 RID: 8121
		public readonly string StatisticsFile;

		// Token: 0x04001FBA RID: 8122
		public readonly string StatisticsFileIPv6;

		// Token: 0x04001FBB RID: 8123
		public readonly string TcpFile;

		// Token: 0x04001FBC RID: 8124
		public readonly string Tcp6File;

		// Token: 0x04001FBD RID: 8125
		public readonly string UdpFile;

		// Token: 0x04001FBE RID: 8126
		public readonly string Udp6File;

		// Token: 0x04001FBF RID: 8127
		private static readonly char[] wsChars = new char[]
		{
			' ',
			'\t'
		};
	}
}
