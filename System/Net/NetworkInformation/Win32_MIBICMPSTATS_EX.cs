﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004E9 RID: 1257
	internal struct Win32_MIBICMPSTATS_EX
	{
		// Token: 0x04002029 RID: 8233
		public uint Msgs;

		// Token: 0x0400202A RID: 8234
		public uint Errors;

		// Token: 0x0400202B RID: 8235
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
		public uint[] Counts;
	}
}
