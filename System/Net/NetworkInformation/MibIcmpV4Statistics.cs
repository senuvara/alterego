﻿using System;
using System.Collections.Specialized;
using System.Globalization;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004E1 RID: 1249
	internal class MibIcmpV4Statistics : IcmpV4Statistics
	{
		// Token: 0x0600285C RID: 10332 RVA: 0x0008E5EE File Offset: 0x0008C7EE
		public MibIcmpV4Statistics(StringDictionary dic)
		{
			this.dic = dic;
		}

		// Token: 0x0600285D RID: 10333 RVA: 0x0008E5FD File Offset: 0x0008C7FD
		private long Get(string name)
		{
			if (this.dic[name] == null)
			{
				return 0L;
			}
			return long.Parse(this.dic[name], NumberFormatInfo.InvariantInfo);
		}

		// Token: 0x17000990 RID: 2448
		// (get) Token: 0x0600285E RID: 10334 RVA: 0x0008E626 File Offset: 0x0008C826
		public override long AddressMaskRepliesReceived
		{
			get
			{
				return this.Get("InAddrMaskReps");
			}
		}

		// Token: 0x17000991 RID: 2449
		// (get) Token: 0x0600285F RID: 10335 RVA: 0x0008E633 File Offset: 0x0008C833
		public override long AddressMaskRepliesSent
		{
			get
			{
				return this.Get("OutAddrMaskReps");
			}
		}

		// Token: 0x17000992 RID: 2450
		// (get) Token: 0x06002860 RID: 10336 RVA: 0x0008E640 File Offset: 0x0008C840
		public override long AddressMaskRequestsReceived
		{
			get
			{
				return this.Get("InAddrMasks");
			}
		}

		// Token: 0x17000993 RID: 2451
		// (get) Token: 0x06002861 RID: 10337 RVA: 0x0008E64D File Offset: 0x0008C84D
		public override long AddressMaskRequestsSent
		{
			get
			{
				return this.Get("OutAddrMasks");
			}
		}

		// Token: 0x17000994 RID: 2452
		// (get) Token: 0x06002862 RID: 10338 RVA: 0x0008E65A File Offset: 0x0008C85A
		public override long DestinationUnreachableMessagesReceived
		{
			get
			{
				return this.Get("InDestUnreachs");
			}
		}

		// Token: 0x17000995 RID: 2453
		// (get) Token: 0x06002863 RID: 10339 RVA: 0x0008E667 File Offset: 0x0008C867
		public override long DestinationUnreachableMessagesSent
		{
			get
			{
				return this.Get("OutDestUnreachs");
			}
		}

		// Token: 0x17000996 RID: 2454
		// (get) Token: 0x06002864 RID: 10340 RVA: 0x0008E674 File Offset: 0x0008C874
		public override long EchoRepliesReceived
		{
			get
			{
				return this.Get("InEchoReps");
			}
		}

		// Token: 0x17000997 RID: 2455
		// (get) Token: 0x06002865 RID: 10341 RVA: 0x0008E681 File Offset: 0x0008C881
		public override long EchoRepliesSent
		{
			get
			{
				return this.Get("OutEchoReps");
			}
		}

		// Token: 0x17000998 RID: 2456
		// (get) Token: 0x06002866 RID: 10342 RVA: 0x0008E68E File Offset: 0x0008C88E
		public override long EchoRequestsReceived
		{
			get
			{
				return this.Get("InEchos");
			}
		}

		// Token: 0x17000999 RID: 2457
		// (get) Token: 0x06002867 RID: 10343 RVA: 0x0008E69B File Offset: 0x0008C89B
		public override long EchoRequestsSent
		{
			get
			{
				return this.Get("OutEchos");
			}
		}

		// Token: 0x1700099A RID: 2458
		// (get) Token: 0x06002868 RID: 10344 RVA: 0x0008E6A8 File Offset: 0x0008C8A8
		public override long ErrorsReceived
		{
			get
			{
				return this.Get("InErrors");
			}
		}

		// Token: 0x1700099B RID: 2459
		// (get) Token: 0x06002869 RID: 10345 RVA: 0x0008E6B5 File Offset: 0x0008C8B5
		public override long ErrorsSent
		{
			get
			{
				return this.Get("OutErrors");
			}
		}

		// Token: 0x1700099C RID: 2460
		// (get) Token: 0x0600286A RID: 10346 RVA: 0x0008E6C2 File Offset: 0x0008C8C2
		public override long MessagesReceived
		{
			get
			{
				return this.Get("InMsgs");
			}
		}

		// Token: 0x1700099D RID: 2461
		// (get) Token: 0x0600286B RID: 10347 RVA: 0x0008E6CF File Offset: 0x0008C8CF
		public override long MessagesSent
		{
			get
			{
				return this.Get("OutMsgs");
			}
		}

		// Token: 0x1700099E RID: 2462
		// (get) Token: 0x0600286C RID: 10348 RVA: 0x0008E6DC File Offset: 0x0008C8DC
		public override long ParameterProblemsReceived
		{
			get
			{
				return this.Get("InParmProbs");
			}
		}

		// Token: 0x1700099F RID: 2463
		// (get) Token: 0x0600286D RID: 10349 RVA: 0x0008E6E9 File Offset: 0x0008C8E9
		public override long ParameterProblemsSent
		{
			get
			{
				return this.Get("OutParmProbs");
			}
		}

		// Token: 0x170009A0 RID: 2464
		// (get) Token: 0x0600286E RID: 10350 RVA: 0x0008E6F6 File Offset: 0x0008C8F6
		public override long RedirectsReceived
		{
			get
			{
				return this.Get("InRedirects");
			}
		}

		// Token: 0x170009A1 RID: 2465
		// (get) Token: 0x0600286F RID: 10351 RVA: 0x0008E703 File Offset: 0x0008C903
		public override long RedirectsSent
		{
			get
			{
				return this.Get("OutRedirects");
			}
		}

		// Token: 0x170009A2 RID: 2466
		// (get) Token: 0x06002870 RID: 10352 RVA: 0x0008E710 File Offset: 0x0008C910
		public override long SourceQuenchesReceived
		{
			get
			{
				return this.Get("InSrcQuenchs");
			}
		}

		// Token: 0x170009A3 RID: 2467
		// (get) Token: 0x06002871 RID: 10353 RVA: 0x0008E71D File Offset: 0x0008C91D
		public override long SourceQuenchesSent
		{
			get
			{
				return this.Get("OutSrcQuenchs");
			}
		}

		// Token: 0x170009A4 RID: 2468
		// (get) Token: 0x06002872 RID: 10354 RVA: 0x0008E72A File Offset: 0x0008C92A
		public override long TimeExceededMessagesReceived
		{
			get
			{
				return this.Get("InTimeExcds");
			}
		}

		// Token: 0x170009A5 RID: 2469
		// (get) Token: 0x06002873 RID: 10355 RVA: 0x0008E737 File Offset: 0x0008C937
		public override long TimeExceededMessagesSent
		{
			get
			{
				return this.Get("OutTimeExcds");
			}
		}

		// Token: 0x170009A6 RID: 2470
		// (get) Token: 0x06002874 RID: 10356 RVA: 0x0008E744 File Offset: 0x0008C944
		public override long TimestampRepliesReceived
		{
			get
			{
				return this.Get("InTimestampReps");
			}
		}

		// Token: 0x170009A7 RID: 2471
		// (get) Token: 0x06002875 RID: 10357 RVA: 0x0008E751 File Offset: 0x0008C951
		public override long TimestampRepliesSent
		{
			get
			{
				return this.Get("OutTimestampReps");
			}
		}

		// Token: 0x170009A8 RID: 2472
		// (get) Token: 0x06002876 RID: 10358 RVA: 0x0008E75E File Offset: 0x0008C95E
		public override long TimestampRequestsReceived
		{
			get
			{
				return this.Get("InTimestamps");
			}
		}

		// Token: 0x170009A9 RID: 2473
		// (get) Token: 0x06002877 RID: 10359 RVA: 0x0008E76B File Offset: 0x0008C96B
		public override long TimestampRequestsSent
		{
			get
			{
				return this.Get("OutTimestamps");
			}
		}

		// Token: 0x04002003 RID: 8195
		private StringDictionary dic;
	}
}
