﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004DE RID: 1246
	internal class LinuxIPv4InterfaceStatistics : IPv4InterfaceStatistics
	{
		// Token: 0x0600283E RID: 10302 RVA: 0x0008E4EB File Offset: 0x0008C6EB
		public LinuxIPv4InterfaceStatistics(LinuxNetworkInterface parent)
		{
			this.linux = parent;
		}

		// Token: 0x0600283F RID: 10303 RVA: 0x0008E4FC File Offset: 0x0008C6FC
		private long Read(string file)
		{
			long result;
			try
			{
				result = long.Parse(LinuxNetworkInterface.ReadLine(this.linux.IfacePath + file));
			}
			catch
			{
				result = 0L;
			}
			return result;
		}

		// Token: 0x17000976 RID: 2422
		// (get) Token: 0x06002840 RID: 10304 RVA: 0x0008E540 File Offset: 0x0008C740
		public override long BytesReceived
		{
			get
			{
				return this.Read("statistics/rx_bytes");
			}
		}

		// Token: 0x17000977 RID: 2423
		// (get) Token: 0x06002841 RID: 10305 RVA: 0x0008E54D File Offset: 0x0008C74D
		public override long BytesSent
		{
			get
			{
				return this.Read("statistics/tx_bytes");
			}
		}

		// Token: 0x17000978 RID: 2424
		// (get) Token: 0x06002842 RID: 10306 RVA: 0x0008E55A File Offset: 0x0008C75A
		public override long IncomingPacketsDiscarded
		{
			get
			{
				return this.Read("statistics/rx_dropped");
			}
		}

		// Token: 0x17000979 RID: 2425
		// (get) Token: 0x06002843 RID: 10307 RVA: 0x0008E567 File Offset: 0x0008C767
		public override long IncomingPacketsWithErrors
		{
			get
			{
				return this.Read("statistics/rx_errors");
			}
		}

		// Token: 0x1700097A RID: 2426
		// (get) Token: 0x06002844 RID: 10308 RVA: 0x0002873C File Offset: 0x0002693C
		public override long IncomingUnknownProtocolPackets
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x1700097B RID: 2427
		// (get) Token: 0x06002845 RID: 10309 RVA: 0x0008E574 File Offset: 0x0008C774
		public override long NonUnicastPacketsReceived
		{
			get
			{
				return this.Read("statistics/multicast");
			}
		}

		// Token: 0x1700097C RID: 2428
		// (get) Token: 0x06002846 RID: 10310 RVA: 0x0008E574 File Offset: 0x0008C774
		public override long NonUnicastPacketsSent
		{
			get
			{
				return this.Read("statistics/multicast");
			}
		}

		// Token: 0x1700097D RID: 2429
		// (get) Token: 0x06002847 RID: 10311 RVA: 0x0008E581 File Offset: 0x0008C781
		public override long OutgoingPacketsDiscarded
		{
			get
			{
				return this.Read("statistics/tx_dropped");
			}
		}

		// Token: 0x1700097E RID: 2430
		// (get) Token: 0x06002848 RID: 10312 RVA: 0x0008E58E File Offset: 0x0008C78E
		public override long OutgoingPacketsWithErrors
		{
			get
			{
				return this.Read("statistics/tx_errors");
			}
		}

		// Token: 0x1700097F RID: 2431
		// (get) Token: 0x06002849 RID: 10313 RVA: 0x0008E59B File Offset: 0x0008C79B
		public override long OutputQueueLength
		{
			get
			{
				return 1024L;
			}
		}

		// Token: 0x17000980 RID: 2432
		// (get) Token: 0x0600284A RID: 10314 RVA: 0x0008E5A3 File Offset: 0x0008C7A3
		public override long UnicastPacketsReceived
		{
			get
			{
				return this.Read("statistics/rx_packets");
			}
		}

		// Token: 0x17000981 RID: 2433
		// (get) Token: 0x0600284B RID: 10315 RVA: 0x0008E5B0 File Offset: 0x0008C7B0
		public override long UnicastPacketsSent
		{
			get
			{
				return this.Read("statistics/tx_packets");
			}
		}

		// Token: 0x04002001 RID: 8193
		private LinuxNetworkInterface linux;
	}
}
