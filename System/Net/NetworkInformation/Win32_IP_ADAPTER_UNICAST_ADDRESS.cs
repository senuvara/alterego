﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000524 RID: 1316
	internal struct Win32_IP_ADAPTER_UNICAST_ADDRESS
	{
		// Token: 0x04002171 RID: 8561
		public Win32LengthFlagsUnion LengthFlags;

		// Token: 0x04002172 RID: 8562
		public IntPtr Next;

		// Token: 0x04002173 RID: 8563
		public Win32_SOCKET_ADDRESS Address;

		// Token: 0x04002174 RID: 8564
		public PrefixOrigin PrefixOrigin;

		// Token: 0x04002175 RID: 8565
		public SuffixOrigin SuffixOrigin;

		// Token: 0x04002176 RID: 8566
		public DuplicateAddressDetectionState DadState;

		// Token: 0x04002177 RID: 8567
		public uint ValidLifetime;

		// Token: 0x04002178 RID: 8568
		public uint PreferredLifetime;

		// Token: 0x04002179 RID: 8569
		public uint LeaseLifetime;

		// Token: 0x0400217A RID: 8570
		public byte OnLinkPrefixLength;
	}
}
