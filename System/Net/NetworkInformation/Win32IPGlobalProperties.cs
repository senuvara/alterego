﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004CB RID: 1227
	internal class Win32IPGlobalProperties : IPGlobalProperties
	{
		// Token: 0x0600279E RID: 10142 RVA: 0x0008D134 File Offset: 0x0008B334
		private unsafe void FillTcpTable(out List<Win32IPGlobalProperties.Win32_MIB_TCPROW> tab4, out List<Win32IPGlobalProperties.Win32_MIB_TCP6ROW> tab6)
		{
			tab4 = new List<Win32IPGlobalProperties.Win32_MIB_TCPROW>();
			int num = 0;
			Win32IPGlobalProperties.GetTcpTable(null, ref num, true);
			byte[] array = new byte[num];
			Win32IPGlobalProperties.GetTcpTable(array, ref num, true);
			int num2 = Marshal.SizeOf(typeof(Win32IPGlobalProperties.Win32_MIB_TCPROW));
			fixed (byte[] array2 = array)
			{
				byte* ptr;
				if (array == null || array2.Length == 0)
				{
					ptr = null;
				}
				else
				{
					ptr = &array2[0];
				}
				int num3 = Marshal.ReadInt32((IntPtr)((void*)ptr));
				for (int i = 0; i < num3; i++)
				{
					Win32IPGlobalProperties.Win32_MIB_TCPROW win32_MIB_TCPROW = new Win32IPGlobalProperties.Win32_MIB_TCPROW();
					Marshal.PtrToStructure<Win32IPGlobalProperties.Win32_MIB_TCPROW>((IntPtr)((void*)(ptr + i * num2 + 4)), win32_MIB_TCPROW);
					tab4.Add(win32_MIB_TCPROW);
				}
			}
			tab6 = new List<Win32IPGlobalProperties.Win32_MIB_TCP6ROW>();
			if (Environment.OSVersion.Version.Major >= 6)
			{
				int num4 = 0;
				Win32IPGlobalProperties.GetTcp6Table(null, ref num4, true);
				byte[] array3 = new byte[num4];
				Win32IPGlobalProperties.GetTcp6Table(array3, ref num4, true);
				int num5 = Marshal.SizeOf(typeof(Win32IPGlobalProperties.Win32_MIB_TCP6ROW));
				fixed (byte[] array2 = array3)
				{
					byte* ptr2;
					if (array3 == null || array2.Length == 0)
					{
						ptr2 = null;
					}
					else
					{
						ptr2 = &array2[0];
					}
					int num6 = Marshal.ReadInt32((IntPtr)((void*)ptr2));
					for (int j = 0; j < num6; j++)
					{
						Win32IPGlobalProperties.Win32_MIB_TCP6ROW win32_MIB_TCP6ROW = new Win32IPGlobalProperties.Win32_MIB_TCP6ROW();
						Marshal.PtrToStructure<Win32IPGlobalProperties.Win32_MIB_TCP6ROW>((IntPtr)((void*)(ptr2 + j * num5 + 4)), win32_MIB_TCP6ROW);
						tab6.Add(win32_MIB_TCP6ROW);
					}
				}
			}
		}

		// Token: 0x0600279F RID: 10143 RVA: 0x0008D27A File Offset: 0x0008B47A
		private bool IsListenerState(TcpState state)
		{
			return state - TcpState.Listen <= 1 || state - TcpState.FinWait1 <= 2;
		}

		// Token: 0x060027A0 RID: 10144 RVA: 0x0008D28C File Offset: 0x0008B48C
		public override TcpConnectionInformation[] GetActiveTcpConnections()
		{
			List<Win32IPGlobalProperties.Win32_MIB_TCPROW> list = null;
			List<Win32IPGlobalProperties.Win32_MIB_TCP6ROW> list2 = null;
			this.FillTcpTable(out list, out list2);
			int count = list.Count;
			TcpConnectionInformation[] array = new TcpConnectionInformation[count + list2.Count];
			for (int i = 0; i < count; i++)
			{
				array[i] = list[i].TcpInfo;
			}
			for (int j = 0; j < list2.Count; j++)
			{
				array[count + j] = list2[j].TcpInfo;
			}
			return array;
		}

		// Token: 0x060027A1 RID: 10145 RVA: 0x0008D308 File Offset: 0x0008B508
		public override IPEndPoint[] GetActiveTcpListeners()
		{
			List<Win32IPGlobalProperties.Win32_MIB_TCPROW> list = null;
			List<Win32IPGlobalProperties.Win32_MIB_TCP6ROW> list2 = null;
			this.FillTcpTable(out list, out list2);
			List<IPEndPoint> list3 = new List<IPEndPoint>();
			int i = 0;
			int count = list.Count;
			while (i < count)
			{
				if (this.IsListenerState(list[i].State))
				{
					list3.Add(list[i].LocalEndPoint);
				}
				i++;
			}
			int j = 0;
			int count2 = list2.Count;
			while (j < count2)
			{
				if (this.IsListenerState(list2[j].State))
				{
					list3.Add(list2[j].LocalEndPoint);
				}
				j++;
			}
			return list3.ToArray();
		}

		// Token: 0x060027A2 RID: 10146 RVA: 0x0008D3AC File Offset: 0x0008B5AC
		public unsafe override IPEndPoint[] GetActiveUdpListeners()
		{
			List<IPEndPoint> list = new List<IPEndPoint>();
			int num = 0;
			Win32IPGlobalProperties.GetUdpTable(null, ref num, true);
			byte[] array = new byte[num];
			Win32IPGlobalProperties.GetUdpTable(array, ref num, true);
			int num2 = Marshal.SizeOf(typeof(Win32IPGlobalProperties.Win32_MIB_UDPROW));
			fixed (byte[] array2 = array)
			{
				byte* ptr;
				if (array == null || array2.Length == 0)
				{
					ptr = null;
				}
				else
				{
					ptr = &array2[0];
				}
				int num3 = Marshal.ReadInt32((IntPtr)((void*)ptr));
				for (int i = 0; i < num3; i++)
				{
					Win32IPGlobalProperties.Win32_MIB_UDPROW win32_MIB_UDPROW = new Win32IPGlobalProperties.Win32_MIB_UDPROW();
					Marshal.PtrToStructure<Win32IPGlobalProperties.Win32_MIB_UDPROW>((IntPtr)((void*)(ptr + i * num2 + 4)), win32_MIB_UDPROW);
					list.Add(win32_MIB_UDPROW.LocalEndPoint);
				}
			}
			if (Environment.OSVersion.Version.Major >= 6)
			{
				int num4 = 0;
				Win32IPGlobalProperties.GetUdp6Table(null, ref num4, true);
				byte[] array3 = new byte[num4];
				Win32IPGlobalProperties.GetUdp6Table(array3, ref num4, true);
				int num5 = Marshal.SizeOf(typeof(Win32IPGlobalProperties.Win32_MIB_UDP6ROW));
				fixed (byte[] array2 = array3)
				{
					byte* ptr2;
					if (array3 == null || array2.Length == 0)
					{
						ptr2 = null;
					}
					else
					{
						ptr2 = &array2[0];
					}
					int num6 = Marshal.ReadInt32((IntPtr)((void*)ptr2));
					for (int j = 0; j < num6; j++)
					{
						Win32IPGlobalProperties.Win32_MIB_UDP6ROW win32_MIB_UDP6ROW = new Win32IPGlobalProperties.Win32_MIB_UDP6ROW();
						Marshal.PtrToStructure<Win32IPGlobalProperties.Win32_MIB_UDP6ROW>((IntPtr)((void*)(ptr2 + j * num5 + 4)), win32_MIB_UDP6ROW);
						list.Add(win32_MIB_UDP6ROW.LocalEndPoint);
					}
				}
			}
			return list.ToArray();
		}

		// Token: 0x060027A3 RID: 10147 RVA: 0x0008D500 File Offset: 0x0008B700
		public override IcmpV4Statistics GetIcmpV4Statistics()
		{
			if (!Socket.OSSupportsIPv4)
			{
				throw new NetworkInformationException();
			}
			Win32_MIBICMPINFO info;
			Win32IPGlobalProperties.GetIcmpStatistics(out info, 2);
			return new Win32IcmpV4Statistics(info);
		}

		// Token: 0x060027A4 RID: 10148 RVA: 0x0008D52C File Offset: 0x0008B72C
		public override IcmpV6Statistics GetIcmpV6Statistics()
		{
			if (!Socket.OSSupportsIPv6)
			{
				throw new NetworkInformationException();
			}
			Win32_MIB_ICMP_EX info;
			Win32IPGlobalProperties.GetIcmpStatisticsEx(out info, 23);
			return new Win32IcmpV6Statistics(info);
		}

		// Token: 0x060027A5 RID: 10149 RVA: 0x0008D558 File Offset: 0x0008B758
		public override IPGlobalStatistics GetIPv4GlobalStatistics()
		{
			if (!Socket.OSSupportsIPv4)
			{
				throw new NetworkInformationException();
			}
			Win32_MIB_IPSTATS info;
			Win32IPGlobalProperties.GetIpStatisticsEx(out info, 2);
			return new Win32IPGlobalStatistics(info);
		}

		// Token: 0x060027A6 RID: 10150 RVA: 0x0008D584 File Offset: 0x0008B784
		public override IPGlobalStatistics GetIPv6GlobalStatistics()
		{
			if (!Socket.OSSupportsIPv6)
			{
				throw new NetworkInformationException();
			}
			Win32_MIB_IPSTATS info;
			Win32IPGlobalProperties.GetIpStatisticsEx(out info, 23);
			return new Win32IPGlobalStatistics(info);
		}

		// Token: 0x060027A7 RID: 10151 RVA: 0x0008D5B0 File Offset: 0x0008B7B0
		public override TcpStatistics GetTcpIPv4Statistics()
		{
			if (!Socket.OSSupportsIPv4)
			{
				throw new NetworkInformationException();
			}
			Win32_MIB_TCPSTATS info;
			Win32IPGlobalProperties.GetTcpStatisticsEx(out info, 2);
			return new Win32TcpStatistics(info);
		}

		// Token: 0x060027A8 RID: 10152 RVA: 0x0008D5DC File Offset: 0x0008B7DC
		public override TcpStatistics GetTcpIPv6Statistics()
		{
			if (!Socket.OSSupportsIPv6)
			{
				throw new NetworkInformationException();
			}
			Win32_MIB_TCPSTATS info;
			Win32IPGlobalProperties.GetTcpStatisticsEx(out info, 23);
			return new Win32TcpStatistics(info);
		}

		// Token: 0x060027A9 RID: 10153 RVA: 0x0008D608 File Offset: 0x0008B808
		public override UdpStatistics GetUdpIPv4Statistics()
		{
			if (!Socket.OSSupportsIPv4)
			{
				throw new NetworkInformationException();
			}
			Win32_MIB_UDPSTATS info;
			Win32IPGlobalProperties.GetUdpStatisticsEx(out info, 2);
			return new Win32UdpStatistics(info);
		}

		// Token: 0x060027AA RID: 10154 RVA: 0x0008D634 File Offset: 0x0008B834
		public override UdpStatistics GetUdpIPv6Statistics()
		{
			if (!Socket.OSSupportsIPv6)
			{
				throw new NetworkInformationException();
			}
			Win32_MIB_UDPSTATS info;
			Win32IPGlobalProperties.GetUdpStatisticsEx(out info, 23);
			return new Win32UdpStatistics(info);
		}

		// Token: 0x1700090C RID: 2316
		// (get) Token: 0x060027AB RID: 10155 RVA: 0x0008D65E File Offset: 0x0008B85E
		public override string DhcpScopeName
		{
			get
			{
				return Win32NetworkInterface.FixedInfo.ScopeId;
			}
		}

		// Token: 0x1700090D RID: 2317
		// (get) Token: 0x060027AC RID: 10156 RVA: 0x0008D66A File Offset: 0x0008B86A
		public override string DomainName
		{
			get
			{
				return Win32NetworkInterface.FixedInfo.DomainName;
			}
		}

		// Token: 0x1700090E RID: 2318
		// (get) Token: 0x060027AD RID: 10157 RVA: 0x0008D676 File Offset: 0x0008B876
		public override string HostName
		{
			get
			{
				return Win32NetworkInterface.FixedInfo.HostName;
			}
		}

		// Token: 0x1700090F RID: 2319
		// (get) Token: 0x060027AE RID: 10158 RVA: 0x0008D682 File Offset: 0x0008B882
		public override bool IsWinsProxy
		{
			get
			{
				return Win32NetworkInterface.FixedInfo.EnableProxy > 0U;
			}
		}

		// Token: 0x17000910 RID: 2320
		// (get) Token: 0x060027AF RID: 10159 RVA: 0x0008D691 File Offset: 0x0008B891
		public override NetBiosNodeType NodeType
		{
			get
			{
				return Win32NetworkInterface.FixedInfo.NodeType;
			}
		}

		// Token: 0x060027B0 RID: 10160
		[DllImport("iphlpapi.dll")]
		private static extern int GetTcpTable(byte[] pTcpTable, ref int pdwSize, bool bOrder);

		// Token: 0x060027B1 RID: 10161
		[DllImport("iphlpapi.dll")]
		private static extern int GetTcp6Table(byte[] TcpTable, ref int SizePointer, bool Order);

		// Token: 0x060027B2 RID: 10162
		[DllImport("iphlpapi.dll")]
		private static extern int GetUdpTable(byte[] pUdpTable, ref int pdwSize, bool bOrder);

		// Token: 0x060027B3 RID: 10163
		[DllImport("iphlpapi.dll")]
		private static extern int GetUdp6Table(byte[] Udp6Table, ref int SizePointer, bool Order);

		// Token: 0x060027B4 RID: 10164
		[DllImport("iphlpapi.dll")]
		private static extern int GetTcpStatisticsEx(out Win32_MIB_TCPSTATS pStats, int dwFamily);

		// Token: 0x060027B5 RID: 10165
		[DllImport("iphlpapi.dll")]
		private static extern int GetUdpStatisticsEx(out Win32_MIB_UDPSTATS pStats, int dwFamily);

		// Token: 0x060027B6 RID: 10166
		[DllImport("iphlpapi.dll")]
		private static extern int GetIcmpStatistics(out Win32_MIBICMPINFO pStats, int dwFamily);

		// Token: 0x060027B7 RID: 10167
		[DllImport("iphlpapi.dll")]
		private static extern int GetIcmpStatisticsEx(out Win32_MIB_ICMP_EX pStats, int dwFamily);

		// Token: 0x060027B8 RID: 10168
		[DllImport("iphlpapi.dll")]
		private static extern int GetIpStatisticsEx(out Win32_MIB_IPSTATS pStats, int dwFamily);

		// Token: 0x060027B9 RID: 10169
		[DllImport("Ws2_32.dll")]
		private static extern ushort ntohs(ushort netshort);

		// Token: 0x060027BA RID: 10170 RVA: 0x0008CB77 File Offset: 0x0008AD77
		public Win32IPGlobalProperties()
		{
		}

		// Token: 0x04001FC0 RID: 8128
		public const int AF_INET = 2;

		// Token: 0x04001FC1 RID: 8129
		public const int AF_INET6 = 23;

		// Token: 0x020004CC RID: 1228
		[StructLayout(LayoutKind.Explicit)]
		private struct Win32_IN6_ADDR
		{
			// Token: 0x04001FC2 RID: 8130
			[FieldOffset(0)]
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
			public byte[] Bytes;
		}

		// Token: 0x020004CD RID: 1229
		[StructLayout(LayoutKind.Sequential)]
		private class Win32_MIB_TCPROW
		{
			// Token: 0x17000911 RID: 2321
			// (get) Token: 0x060027BB RID: 10171 RVA: 0x0008D69D File Offset: 0x0008B89D
			public IPEndPoint LocalEndPoint
			{
				get
				{
					return new IPEndPoint((long)((ulong)this.LocalAddr), this.LocalPort);
				}
			}

			// Token: 0x17000912 RID: 2322
			// (get) Token: 0x060027BC RID: 10172 RVA: 0x0008D6B1 File Offset: 0x0008B8B1
			public IPEndPoint RemoteEndPoint
			{
				get
				{
					return new IPEndPoint((long)((ulong)this.RemoteAddr), this.RemotePort);
				}
			}

			// Token: 0x17000913 RID: 2323
			// (get) Token: 0x060027BD RID: 10173 RVA: 0x0008D6C5 File Offset: 0x0008B8C5
			public TcpConnectionInformation TcpInfo
			{
				get
				{
					return new SystemTcpConnectionInformation(this.LocalEndPoint, this.RemoteEndPoint, this.State);
				}
			}

			// Token: 0x060027BE RID: 10174 RVA: 0x0000232F File Offset: 0x0000052F
			public Win32_MIB_TCPROW()
			{
			}

			// Token: 0x04001FC3 RID: 8131
			public TcpState State;

			// Token: 0x04001FC4 RID: 8132
			public uint LocalAddr;

			// Token: 0x04001FC5 RID: 8133
			public int LocalPort;

			// Token: 0x04001FC6 RID: 8134
			public uint RemoteAddr;

			// Token: 0x04001FC7 RID: 8135
			public int RemotePort;
		}

		// Token: 0x020004CE RID: 1230
		[StructLayout(LayoutKind.Sequential)]
		private class Win32_MIB_TCP6ROW
		{
			// Token: 0x17000914 RID: 2324
			// (get) Token: 0x060027BF RID: 10175 RVA: 0x0008D6DE File Offset: 0x0008B8DE
			public IPEndPoint LocalEndPoint
			{
				get
				{
					return new IPEndPoint(new IPAddress(this.LocalAddr.Bytes, (long)((ulong)this.LocalScopeId)), this.LocalPort);
				}
			}

			// Token: 0x17000915 RID: 2325
			// (get) Token: 0x060027C0 RID: 10176 RVA: 0x0008D702 File Offset: 0x0008B902
			public IPEndPoint RemoteEndPoint
			{
				get
				{
					return new IPEndPoint(new IPAddress(this.RemoteAddr.Bytes, (long)((ulong)this.RemoteScopeId)), this.RemotePort);
				}
			}

			// Token: 0x17000916 RID: 2326
			// (get) Token: 0x060027C1 RID: 10177 RVA: 0x0008D726 File Offset: 0x0008B926
			public TcpConnectionInformation TcpInfo
			{
				get
				{
					return new SystemTcpConnectionInformation(this.LocalEndPoint, this.RemoteEndPoint, this.State);
				}
			}

			// Token: 0x060027C2 RID: 10178 RVA: 0x0000232F File Offset: 0x0000052F
			public Win32_MIB_TCP6ROW()
			{
			}

			// Token: 0x04001FC8 RID: 8136
			public TcpState State;

			// Token: 0x04001FC9 RID: 8137
			public Win32IPGlobalProperties.Win32_IN6_ADDR LocalAddr;

			// Token: 0x04001FCA RID: 8138
			public uint LocalScopeId;

			// Token: 0x04001FCB RID: 8139
			public int LocalPort;

			// Token: 0x04001FCC RID: 8140
			public Win32IPGlobalProperties.Win32_IN6_ADDR RemoteAddr;

			// Token: 0x04001FCD RID: 8141
			public uint RemoteScopeId;

			// Token: 0x04001FCE RID: 8142
			public int RemotePort;
		}

		// Token: 0x020004CF RID: 1231
		[StructLayout(LayoutKind.Sequential)]
		private class Win32_MIB_UDPROW
		{
			// Token: 0x17000917 RID: 2327
			// (get) Token: 0x060027C3 RID: 10179 RVA: 0x0008D73F File Offset: 0x0008B93F
			public IPEndPoint LocalEndPoint
			{
				get
				{
					return new IPEndPoint((long)((ulong)this.LocalAddr), (int)Win32IPGlobalProperties.ntohs((ushort)this.LocalPort));
				}
			}

			// Token: 0x060027C4 RID: 10180 RVA: 0x0000232F File Offset: 0x0000052F
			public Win32_MIB_UDPROW()
			{
			}

			// Token: 0x04001FCF RID: 8143
			public uint LocalAddr;

			// Token: 0x04001FD0 RID: 8144
			public uint LocalPort;
		}

		// Token: 0x020004D0 RID: 1232
		[StructLayout(LayoutKind.Sequential)]
		private class Win32_MIB_UDP6ROW
		{
			// Token: 0x17000918 RID: 2328
			// (get) Token: 0x060027C5 RID: 10181 RVA: 0x0008D759 File Offset: 0x0008B959
			public IPEndPoint LocalEndPoint
			{
				get
				{
					return new IPEndPoint(new IPAddress(this.LocalAddr.Bytes, (long)((ulong)this.LocalScopeId)), this.LocalPort);
				}
			}

			// Token: 0x060027C6 RID: 10182 RVA: 0x0000232F File Offset: 0x0000052F
			public Win32_MIB_UDP6ROW()
			{
			}

			// Token: 0x04001FD1 RID: 8145
			public Win32IPGlobalProperties.Win32_IN6_ADDR LocalAddr;

			// Token: 0x04001FD2 RID: 8146
			public uint LocalScopeId;

			// Token: 0x04001FD3 RID: 8147
			public int LocalPort;
		}
	}
}
