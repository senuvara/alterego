﻿using System;
using System.ComponentModel;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Net.NetworkInformation
{
	/// <summary>The exception that is thrown when an error occurs while retrieving network information.</summary>
	// Token: 0x020004AD RID: 1197
	[Serializable]
	public class NetworkInformationException : Win32Exception
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkInformation.NetworkInformationException" /> class.</summary>
		// Token: 0x060026E7 RID: 9959 RVA: 0x0003DB5B File Offset: 0x0003BD5B
		public NetworkInformationException() : base(Marshal.GetLastWin32Error())
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkInformation.NetworkInformationException" /> class with the specified error code.</summary>
		/// <param name="errorCode">A <see langword="Win32" /> error code. </param>
		// Token: 0x060026E8 RID: 9960 RVA: 0x0004FBB9 File Offset: 0x0004DDB9
		public NetworkInformationException(int errorCode) : base(errorCode)
		{
		}

		// Token: 0x060026E9 RID: 9961 RVA: 0x0004FBB9 File Offset: 0x0004DDB9
		internal NetworkInformationException(SocketError socketError) : base((int)socketError)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkInformation.NetworkInformationException" /> class with serialized data.</summary>
		/// <param name="serializationInfo">A SerializationInfo object that contains the serialized exception data. </param>
		/// <param name="streamingContext">A StreamingContext that contains contextual information about the serialized exception. </param>
		// Token: 0x060026EA RID: 9962 RVA: 0x0004FBCC File Offset: 0x0004DDCC
		protected NetworkInformationException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
		{
		}

		/// <summary>Gets the <see langword="Win32" /> error code for this exception.</summary>
		/// <returns>An <see cref="T:System.Int32" /> value that contains the <see langword="Win32" /> error code.</returns>
		// Token: 0x170008C2 RID: 2242
		// (get) Token: 0x060026EB RID: 9963 RVA: 0x0004FBD6 File Offset: 0x0004DDD6
		public override int ErrorCode
		{
			get
			{
				return base.NativeErrorCode;
			}
		}
	}
}
