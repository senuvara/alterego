﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004D7 RID: 1239
	internal class Win32IPInterfaceProperties2 : IPInterfaceProperties
	{
		// Token: 0x0600280B RID: 10251 RVA: 0x0008DF90 File Offset: 0x0008C190
		public Win32IPInterfaceProperties2(Win32_IP_ADAPTER_ADDRESSES addr, Win32_MIB_IFROW mib4, Win32_MIB_IFROW mib6)
		{
			this.addr = addr;
			this.mib4 = mib4;
			this.mib6 = mib6;
		}

		// Token: 0x0600280C RID: 10252 RVA: 0x0008DFAD File Offset: 0x0008C1AD
		public override IPv4InterfaceProperties GetIPv4Properties()
		{
			return new Win32IPv4InterfaceProperties(this.addr, this.mib4);
		}

		// Token: 0x0600280D RID: 10253 RVA: 0x0008DFC0 File Offset: 0x0008C1C0
		public override IPv6InterfaceProperties GetIPv6Properties()
		{
			return new Win32IPv6InterfaceProperties(this.mib6);
		}

		// Token: 0x17000950 RID: 2384
		// (get) Token: 0x0600280E RID: 10254 RVA: 0x0008DFCD File Offset: 0x0008C1CD
		public override IPAddressInformationCollection AnycastAddresses
		{
			get
			{
				return Win32IPInterfaceProperties2.Win32FromAnycast(this.addr.FirstAnycastAddress);
			}
		}

		// Token: 0x0600280F RID: 10255 RVA: 0x0008DFE0 File Offset: 0x0008C1E0
		private static IPAddressInformationCollection Win32FromAnycast(IntPtr ptr)
		{
			IPAddressInformationCollection ipaddressInformationCollection = new IPAddressInformationCollection();
			IntPtr intPtr = ptr;
			while (intPtr != IntPtr.Zero)
			{
				Win32_IP_ADAPTER_ANYCAST_ADDRESS win32_IP_ADAPTER_ANYCAST_ADDRESS = (Win32_IP_ADAPTER_ANYCAST_ADDRESS)Marshal.PtrToStructure(intPtr, typeof(Win32_IP_ADAPTER_ANYCAST_ADDRESS));
				ipaddressInformationCollection.InternalAdd(new SystemIPAddressInformation(win32_IP_ADAPTER_ANYCAST_ADDRESS.Address.GetIPAddress(), win32_IP_ADAPTER_ANYCAST_ADDRESS.LengthFlags.IsDnsEligible, win32_IP_ADAPTER_ANYCAST_ADDRESS.LengthFlags.IsTransient));
				intPtr = win32_IP_ADAPTER_ANYCAST_ADDRESS.Next;
			}
			return ipaddressInformationCollection;
		}

		// Token: 0x17000951 RID: 2385
		// (get) Token: 0x06002810 RID: 10256 RVA: 0x0008E054 File Offset: 0x0008C254
		public override IPAddressCollection DhcpServerAddresses
		{
			get
			{
				IPAddressCollection result;
				try
				{
					result = Win32IPAddressCollection.FromSocketAddress(this.addr.Dhcpv4Server);
				}
				catch (IndexOutOfRangeException)
				{
					result = Win32IPAddressCollection.Empty;
				}
				return result;
			}
		}

		// Token: 0x17000952 RID: 2386
		// (get) Token: 0x06002811 RID: 10257 RVA: 0x0008E090 File Offset: 0x0008C290
		public override IPAddressCollection DnsAddresses
		{
			get
			{
				return Win32IPAddressCollection.FromDnsServer(this.addr.FirstDnsServerAddress);
			}
		}

		// Token: 0x17000953 RID: 2387
		// (get) Token: 0x06002812 RID: 10258 RVA: 0x0008E0A2 File Offset: 0x0008C2A2
		public override string DnsSuffix
		{
			get
			{
				return this.addr.DnsSuffix;
			}
		}

		// Token: 0x17000954 RID: 2388
		// (get) Token: 0x06002813 RID: 10259 RVA: 0x0008E0B0 File Offset: 0x0008C2B0
		public override GatewayIPAddressInformationCollection GatewayAddresses
		{
			get
			{
				GatewayIPAddressInformationCollection gatewayIPAddressInformationCollection = new GatewayIPAddressInformationCollection();
				try
				{
					IntPtr intPtr = this.addr.FirstGatewayAddress;
					while (intPtr != IntPtr.Zero)
					{
						Win32_IP_ADAPTER_GATEWAY_ADDRESS win32_IP_ADAPTER_GATEWAY_ADDRESS = (Win32_IP_ADAPTER_GATEWAY_ADDRESS)Marshal.PtrToStructure(intPtr, typeof(Win32_IP_ADAPTER_GATEWAY_ADDRESS));
						gatewayIPAddressInformationCollection.InternalAdd(new SystemGatewayIPAddressInformation(win32_IP_ADAPTER_GATEWAY_ADDRESS.Address.GetIPAddress()));
						intPtr = win32_IP_ADAPTER_GATEWAY_ADDRESS.Next;
					}
				}
				catch (IndexOutOfRangeException)
				{
				}
				return gatewayIPAddressInformationCollection;
			}
		}

		// Token: 0x17000955 RID: 2389
		// (get) Token: 0x06002814 RID: 10260 RVA: 0x0008E128 File Offset: 0x0008C328
		public override bool IsDnsEnabled
		{
			get
			{
				return Win32NetworkInterface.FixedInfo.EnableDns > 0U;
			}
		}

		// Token: 0x17000956 RID: 2390
		// (get) Token: 0x06002815 RID: 10261 RVA: 0x0008E138 File Offset: 0x0008C338
		public override bool IsDynamicDnsEnabled
		{
			get
			{
				return this.addr.DdnsEnabled;
			}
		}

		// Token: 0x17000957 RID: 2391
		// (get) Token: 0x06002816 RID: 10262 RVA: 0x0008E153 File Offset: 0x0008C353
		public override MulticastIPAddressInformationCollection MulticastAddresses
		{
			get
			{
				return Win32IPInterfaceProperties2.Win32FromMulticast(this.addr.FirstMulticastAddress);
			}
		}

		// Token: 0x06002817 RID: 10263 RVA: 0x0008E168 File Offset: 0x0008C368
		private static MulticastIPAddressInformationCollection Win32FromMulticast(IntPtr ptr)
		{
			MulticastIPAddressInformationCollection multicastIPAddressInformationCollection = new MulticastIPAddressInformationCollection();
			IntPtr intPtr = ptr;
			while (intPtr != IntPtr.Zero)
			{
				Win32_IP_ADAPTER_MULTICAST_ADDRESS win32_IP_ADAPTER_MULTICAST_ADDRESS = (Win32_IP_ADAPTER_MULTICAST_ADDRESS)Marshal.PtrToStructure(intPtr, typeof(Win32_IP_ADAPTER_MULTICAST_ADDRESS));
				multicastIPAddressInformationCollection.InternalAdd(new SystemMulticastIPAddressInformation(new SystemIPAddressInformation(win32_IP_ADAPTER_MULTICAST_ADDRESS.Address.GetIPAddress(), win32_IP_ADAPTER_MULTICAST_ADDRESS.LengthFlags.IsDnsEligible, win32_IP_ADAPTER_MULTICAST_ADDRESS.LengthFlags.IsTransient)));
				intPtr = win32_IP_ADAPTER_MULTICAST_ADDRESS.Next;
			}
			return multicastIPAddressInformationCollection;
		}

		// Token: 0x17000958 RID: 2392
		// (get) Token: 0x06002818 RID: 10264 RVA: 0x0008E1E0 File Offset: 0x0008C3E0
		public override UnicastIPAddressInformationCollection UnicastAddresses
		{
			get
			{
				UnicastIPAddressInformationCollection result;
				try
				{
					result = Win32IPInterfaceProperties2.Win32FromUnicast(this.addr.FirstUnicastAddress);
				}
				catch (IndexOutOfRangeException)
				{
					result = new UnicastIPAddressInformationCollection();
				}
				return result;
			}
		}

		// Token: 0x06002819 RID: 10265 RVA: 0x0008E21C File Offset: 0x0008C41C
		private static UnicastIPAddressInformationCollection Win32FromUnicast(IntPtr ptr)
		{
			UnicastIPAddressInformationCollection unicastIPAddressInformationCollection = new UnicastIPAddressInformationCollection();
			IntPtr intPtr = ptr;
			while (intPtr != IntPtr.Zero)
			{
				Win32_IP_ADAPTER_UNICAST_ADDRESS win32_IP_ADAPTER_UNICAST_ADDRESS = (Win32_IP_ADAPTER_UNICAST_ADDRESS)Marshal.PtrToStructure(intPtr, typeof(Win32_IP_ADAPTER_UNICAST_ADDRESS));
				unicastIPAddressInformationCollection.InternalAdd(new Win32UnicastIPAddressInformation(win32_IP_ADAPTER_UNICAST_ADDRESS));
				intPtr = win32_IP_ADAPTER_UNICAST_ADDRESS.Next;
			}
			return unicastIPAddressInformationCollection;
		}

		// Token: 0x17000959 RID: 2393
		// (get) Token: 0x0600281A RID: 10266 RVA: 0x0008E26C File Offset: 0x0008C46C
		public override IPAddressCollection WinsServersAddresses
		{
			get
			{
				IPAddressCollection result;
				try
				{
					result = Win32IPAddressCollection.FromWinsServer(this.addr.FirstWinsServerAddress);
				}
				catch (IndexOutOfRangeException)
				{
					result = Win32IPAddressCollection.Empty;
				}
				return result;
			}
		}

		// Token: 0x04001FF5 RID: 8181
		private readonly Win32_IP_ADAPTER_ADDRESSES addr;

		// Token: 0x04001FF6 RID: 8182
		private readonly Win32_MIB_IFROW mib4;

		// Token: 0x04001FF7 RID: 8183
		private readonly Win32_MIB_IFROW mib6;
	}
}
