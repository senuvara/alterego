﻿using System;
using System.Net.Sockets;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000515 RID: 1301
	internal class Win32UnicastIPAddressInformation : UnicastIPAddressInformation
	{
		// Token: 0x060029D1 RID: 10705 RVA: 0x0009190C File Offset: 0x0008FB0C
		public Win32UnicastIPAddressInformation(Win32_IP_ADAPTER_UNICAST_ADDRESS info)
		{
			this.info = info;
			IPAddress ipaddress = info.Address.GetIPAddress();
			if (ipaddress.AddressFamily == AddressFamily.InterNetwork)
			{
				this.ipv4Mask = Win32UnicastIPAddressInformation.PrefixLengthToSubnetMask(info.OnLinkPrefixLength, ipaddress.AddressFamily);
			}
		}

		// Token: 0x17000A4D RID: 2637
		// (get) Token: 0x060029D2 RID: 10706 RVA: 0x00091953 File Offset: 0x0008FB53
		public override IPAddress Address
		{
			get
			{
				return this.info.Address.GetIPAddress();
			}
		}

		// Token: 0x17000A4E RID: 2638
		// (get) Token: 0x060029D3 RID: 10707 RVA: 0x00091965 File Offset: 0x0008FB65
		public override bool IsDnsEligible
		{
			get
			{
				return this.info.LengthFlags.IsDnsEligible;
			}
		}

		// Token: 0x17000A4F RID: 2639
		// (get) Token: 0x060029D4 RID: 10708 RVA: 0x00091977 File Offset: 0x0008FB77
		public override bool IsTransient
		{
			get
			{
				return this.info.LengthFlags.IsTransient;
			}
		}

		// Token: 0x17000A50 RID: 2640
		// (get) Token: 0x060029D5 RID: 10709 RVA: 0x00091989 File Offset: 0x0008FB89
		public override long AddressPreferredLifetime
		{
			get
			{
				return (long)((ulong)this.info.PreferredLifetime);
			}
		}

		// Token: 0x17000A51 RID: 2641
		// (get) Token: 0x060029D6 RID: 10710 RVA: 0x00091997 File Offset: 0x0008FB97
		public override long AddressValidLifetime
		{
			get
			{
				return (long)((ulong)this.info.ValidLifetime);
			}
		}

		// Token: 0x17000A52 RID: 2642
		// (get) Token: 0x060029D7 RID: 10711 RVA: 0x000919A5 File Offset: 0x0008FBA5
		public override long DhcpLeaseLifetime
		{
			get
			{
				return (long)((ulong)this.info.LeaseLifetime);
			}
		}

		// Token: 0x17000A53 RID: 2643
		// (get) Token: 0x060029D8 RID: 10712 RVA: 0x000919B3 File Offset: 0x0008FBB3
		public override DuplicateAddressDetectionState DuplicateAddressDetectionState
		{
			get
			{
				return this.info.DadState;
			}
		}

		// Token: 0x17000A54 RID: 2644
		// (get) Token: 0x060029D9 RID: 10713 RVA: 0x000919C0 File Offset: 0x0008FBC0
		public override IPAddress IPv4Mask
		{
			get
			{
				if (this.Address.AddressFamily != AddressFamily.InterNetwork)
				{
					return IPAddress.Any;
				}
				return this.ipv4Mask;
			}
		}

		// Token: 0x17000A55 RID: 2645
		// (get) Token: 0x060029DA RID: 10714 RVA: 0x000919DC File Offset: 0x0008FBDC
		public override PrefixOrigin PrefixOrigin
		{
			get
			{
				return this.info.PrefixOrigin;
			}
		}

		// Token: 0x17000A56 RID: 2646
		// (get) Token: 0x060029DB RID: 10715 RVA: 0x000919E9 File Offset: 0x0008FBE9
		public override SuffixOrigin SuffixOrigin
		{
			get
			{
				return this.info.SuffixOrigin;
			}
		}

		// Token: 0x060029DC RID: 10716 RVA: 0x000919F8 File Offset: 0x0008FBF8
		private static IPAddress PrefixLengthToSubnetMask(byte prefixLength, AddressFamily family)
		{
			byte[] array;
			if (family == AddressFamily.InterNetwork)
			{
				array = new byte[4];
			}
			else
			{
				array = new byte[16];
			}
			for (int i = 0; i < (int)prefixLength; i++)
			{
				byte[] array2 = array;
				int num = i / 8;
				array2[num] |= (byte)(128 >> i % 8);
			}
			return new IPAddress(array);
		}

		// Token: 0x040020E9 RID: 8425
		private Win32_IP_ADAPTER_UNICAST_ADDRESS info;

		// Token: 0x040020EA RID: 8426
		private IPAddress ipv4Mask;
	}
}
