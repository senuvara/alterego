﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Specifies the Internet Protocol versions that are supported by a network interface.</summary>
	// Token: 0x020004B2 RID: 1202
	public enum NetworkInterfaceComponent
	{
		/// <summary>Internet Protocol version 4.</summary>
		// Token: 0x04001F50 RID: 8016
		IPv4,
		/// <summary>Internet Protocol version 6.</summary>
		// Token: 0x04001F51 RID: 8017
		IPv6
	}
}
