﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004DA RID: 1242
	internal sealed class MacOsIPv4InterfaceProperties : UnixIPv4InterfaceProperties
	{
		// Token: 0x06002824 RID: 10276 RVA: 0x0008E2C4 File Offset: 0x0008C4C4
		public MacOsIPv4InterfaceProperties(MacOsNetworkInterface iface) : base(iface)
		{
		}

		// Token: 0x17000961 RID: 2401
		// (get) Token: 0x06002825 RID: 10277 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool IsForwardingEnabled
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000962 RID: 2402
		// (get) Token: 0x06002826 RID: 10278 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override int Mtu
		{
			get
			{
				return 0;
			}
		}
	}
}
