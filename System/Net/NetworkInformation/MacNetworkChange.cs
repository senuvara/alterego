﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using Mono.Util;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004F5 RID: 1269
	internal sealed class MacNetworkChange : INetworkChange, IDisposable
	{
		// Token: 0x060028E4 RID: 10468
		[DllImport("/usr/lib/libSystem.dylib")]
		private static extern IntPtr dlopen(string path, int mode);

		// Token: 0x060028E5 RID: 10469
		[DllImport("/usr/lib/libSystem.dylib")]
		private static extern IntPtr dlsym(IntPtr handle, string symbol);

		// Token: 0x060028E6 RID: 10470
		[DllImport("/usr/lib/libSystem.dylib")]
		private static extern int dlclose(IntPtr handle);

		// Token: 0x060028E7 RID: 10471
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern void CFRelease(IntPtr handle);

		// Token: 0x060028E8 RID: 10472
		[DllImport("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation")]
		private static extern IntPtr CFRunLoopGetMain();

		// Token: 0x060028E9 RID: 10473
		[DllImport("/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration")]
		private static extern IntPtr SCNetworkReachabilityCreateWithAddress(IntPtr allocator, ref MacNetworkChange.sockaddr_in sockaddr);

		// Token: 0x060028EA RID: 10474
		[DllImport("/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration")]
		private static extern bool SCNetworkReachabilityGetFlags(IntPtr reachability, out MacNetworkChange.NetworkReachabilityFlags flags);

		// Token: 0x060028EB RID: 10475
		[DllImport("/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration")]
		private static extern bool SCNetworkReachabilitySetCallback(IntPtr reachability, MacNetworkChange.SCNetworkReachabilityCallback callback, ref MacNetworkChange.SCNetworkReachabilityContext context);

		// Token: 0x060028EC RID: 10476
		[DllImport("/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration")]
		private static extern bool SCNetworkReachabilityScheduleWithRunLoop(IntPtr reachability, IntPtr runLoop, IntPtr runLoopMode);

		// Token: 0x060028ED RID: 10477
		[DllImport("/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration")]
		private static extern bool SCNetworkReachabilityUnscheduleFromRunLoop(IntPtr reachability, IntPtr runLoop, IntPtr runLoopMode);

		// Token: 0x14000043 RID: 67
		// (add) Token: 0x060028EE RID: 10478 RVA: 0x0008EF0C File Offset: 0x0008D10C
		// (remove) Token: 0x060028EF RID: 10479 RVA: 0x0008EF44 File Offset: 0x0008D144
		private event NetworkAddressChangedEventHandler networkAddressChanged
		{
			[CompilerGenerated]
			add
			{
				NetworkAddressChangedEventHandler networkAddressChangedEventHandler = this.networkAddressChanged;
				NetworkAddressChangedEventHandler networkAddressChangedEventHandler2;
				do
				{
					networkAddressChangedEventHandler2 = networkAddressChangedEventHandler;
					NetworkAddressChangedEventHandler value2 = (NetworkAddressChangedEventHandler)Delegate.Combine(networkAddressChangedEventHandler2, value);
					networkAddressChangedEventHandler = Interlocked.CompareExchange<NetworkAddressChangedEventHandler>(ref this.networkAddressChanged, value2, networkAddressChangedEventHandler2);
				}
				while (networkAddressChangedEventHandler != networkAddressChangedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				NetworkAddressChangedEventHandler networkAddressChangedEventHandler = this.networkAddressChanged;
				NetworkAddressChangedEventHandler networkAddressChangedEventHandler2;
				do
				{
					networkAddressChangedEventHandler2 = networkAddressChangedEventHandler;
					NetworkAddressChangedEventHandler value2 = (NetworkAddressChangedEventHandler)Delegate.Remove(networkAddressChangedEventHandler2, value);
					networkAddressChangedEventHandler = Interlocked.CompareExchange<NetworkAddressChangedEventHandler>(ref this.networkAddressChanged, value2, networkAddressChangedEventHandler2);
				}
				while (networkAddressChangedEventHandler != networkAddressChangedEventHandler2);
			}
		}

		// Token: 0x14000044 RID: 68
		// (add) Token: 0x060028F0 RID: 10480 RVA: 0x0008EF7C File Offset: 0x0008D17C
		// (remove) Token: 0x060028F1 RID: 10481 RVA: 0x0008EFB4 File Offset: 0x0008D1B4
		private event NetworkAvailabilityChangedEventHandler networkAvailabilityChanged
		{
			[CompilerGenerated]
			add
			{
				NetworkAvailabilityChangedEventHandler networkAvailabilityChangedEventHandler = this.networkAvailabilityChanged;
				NetworkAvailabilityChangedEventHandler networkAvailabilityChangedEventHandler2;
				do
				{
					networkAvailabilityChangedEventHandler2 = networkAvailabilityChangedEventHandler;
					NetworkAvailabilityChangedEventHandler value2 = (NetworkAvailabilityChangedEventHandler)Delegate.Combine(networkAvailabilityChangedEventHandler2, value);
					networkAvailabilityChangedEventHandler = Interlocked.CompareExchange<NetworkAvailabilityChangedEventHandler>(ref this.networkAvailabilityChanged, value2, networkAvailabilityChangedEventHandler2);
				}
				while (networkAvailabilityChangedEventHandler != networkAvailabilityChangedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				NetworkAvailabilityChangedEventHandler networkAvailabilityChangedEventHandler = this.networkAvailabilityChanged;
				NetworkAvailabilityChangedEventHandler networkAvailabilityChangedEventHandler2;
				do
				{
					networkAvailabilityChangedEventHandler2 = networkAvailabilityChangedEventHandler;
					NetworkAvailabilityChangedEventHandler value2 = (NetworkAvailabilityChangedEventHandler)Delegate.Remove(networkAvailabilityChangedEventHandler2, value);
					networkAvailabilityChangedEventHandler = Interlocked.CompareExchange<NetworkAvailabilityChangedEventHandler>(ref this.networkAvailabilityChanged, value2, networkAvailabilityChangedEventHandler2);
				}
				while (networkAvailabilityChangedEventHandler != networkAvailabilityChangedEventHandler2);
			}
		}

		// Token: 0x14000045 RID: 69
		// (add) Token: 0x060028F2 RID: 10482 RVA: 0x0008EFE9 File Offset: 0x0008D1E9
		// (remove) Token: 0x060028F3 RID: 10483 RVA: 0x0008EFFE File Offset: 0x0008D1FE
		public event NetworkAddressChangedEventHandler NetworkAddressChanged
		{
			add
			{
				value(null, EventArgs.Empty);
				this.networkAddressChanged += value;
			}
			remove
			{
				this.networkAddressChanged -= value;
			}
		}

		// Token: 0x14000046 RID: 70
		// (add) Token: 0x060028F4 RID: 10484 RVA: 0x0008F007 File Offset: 0x0008D207
		// (remove) Token: 0x060028F5 RID: 10485 RVA: 0x0008F022 File Offset: 0x0008D222
		public event NetworkAvailabilityChangedEventHandler NetworkAvailabilityChanged
		{
			add
			{
				value(null, new NetworkAvailabilityEventArgs(this.IsAvailable));
				this.networkAvailabilityChanged += value;
			}
			remove
			{
				this.networkAvailabilityChanged -= value;
			}
		}

		// Token: 0x17000A05 RID: 2565
		// (get) Token: 0x060028F6 RID: 10486 RVA: 0x0008F02B File Offset: 0x0008D22B
		private bool IsAvailable
		{
			get
			{
				return (this.flags & MacNetworkChange.NetworkReachabilityFlags.Reachable) != MacNetworkChange.NetworkReachabilityFlags.None && (this.flags & MacNetworkChange.NetworkReachabilityFlags.ConnectionRequired) == MacNetworkChange.NetworkReachabilityFlags.None;
			}
		}

		// Token: 0x17000A06 RID: 2566
		// (get) Token: 0x060028F7 RID: 10487 RVA: 0x0008F044 File Offset: 0x0008D244
		public bool HasRegisteredEvents
		{
			get
			{
				return this.networkAddressChanged != null || this.networkAvailabilityChanged != null;
			}
		}

		// Token: 0x060028F8 RID: 10488 RVA: 0x0008F05C File Offset: 0x0008D25C
		public MacNetworkChange()
		{
			MacNetworkChange.sockaddr_in sockaddr_in = MacNetworkChange.sockaddr_in.Create();
			this.handle = MacNetworkChange.SCNetworkReachabilityCreateWithAddress(IntPtr.Zero, ref sockaddr_in);
			if (this.handle == IntPtr.Zero)
			{
				throw new Exception("SCNetworkReachabilityCreateWithAddress returned NULL");
			}
			this.callback = new MacNetworkChange.SCNetworkReachabilityCallback(MacNetworkChange.HandleCallback);
			MacNetworkChange.SCNetworkReachabilityContext scnetworkReachabilityContext = new MacNetworkChange.SCNetworkReachabilityContext
			{
				info = GCHandle.ToIntPtr(GCHandle.Alloc(this))
			};
			MacNetworkChange.SCNetworkReachabilitySetCallback(this.handle, this.callback, ref scnetworkReachabilityContext);
			this.scheduledWithRunLoop = (this.LoadRunLoopMode() && MacNetworkChange.SCNetworkReachabilityScheduleWithRunLoop(this.handle, MacNetworkChange.CFRunLoopGetMain(), this.runLoopMode));
			MacNetworkChange.SCNetworkReachabilityGetFlags(this.handle, out this.flags);
		}

		// Token: 0x060028F9 RID: 10489 RVA: 0x0008F120 File Offset: 0x0008D320
		private bool LoadRunLoopMode()
		{
			IntPtr value = MacNetworkChange.dlopen("/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation", 0);
			if (value == IntPtr.Zero)
			{
				return false;
			}
			try
			{
				this.runLoopMode = MacNetworkChange.dlsym(value, "kCFRunLoopDefaultMode");
				if (this.runLoopMode != IntPtr.Zero)
				{
					this.runLoopMode = Marshal.ReadIntPtr(this.runLoopMode);
					return this.runLoopMode != IntPtr.Zero;
				}
			}
			finally
			{
				MacNetworkChange.dlclose(value);
			}
			return false;
		}

		// Token: 0x060028FA RID: 10490 RVA: 0x0008F1B0 File Offset: 0x0008D3B0
		public void Dispose()
		{
			lock (this)
			{
				if (!(this.handle == IntPtr.Zero))
				{
					if (this.scheduledWithRunLoop)
					{
						MacNetworkChange.SCNetworkReachabilityUnscheduleFromRunLoop(this.handle, MacNetworkChange.CFRunLoopGetMain(), this.runLoopMode);
					}
					MacNetworkChange.CFRelease(this.handle);
					this.handle = IntPtr.Zero;
					this.callback = null;
					this.flags = MacNetworkChange.NetworkReachabilityFlags.None;
					this.scheduledWithRunLoop = false;
				}
			}
		}

		// Token: 0x060028FB RID: 10491 RVA: 0x0008F244 File Offset: 0x0008D444
		[MonoPInvokeCallback(typeof(MacNetworkChange.SCNetworkReachabilityCallback))]
		private static void HandleCallback(IntPtr reachability, MacNetworkChange.NetworkReachabilityFlags flags, IntPtr info)
		{
			if (info == IntPtr.Zero)
			{
				return;
			}
			MacNetworkChange macNetworkChange = GCHandle.FromIntPtr(info).Target as MacNetworkChange;
			if (macNetworkChange == null || macNetworkChange.flags == flags)
			{
				return;
			}
			macNetworkChange.flags = flags;
			NetworkAddressChangedEventHandler networkAddressChangedEventHandler = macNetworkChange.networkAddressChanged;
			if (networkAddressChangedEventHandler != null)
			{
				networkAddressChangedEventHandler(null, EventArgs.Empty);
			}
			NetworkAvailabilityChangedEventHandler networkAvailabilityChangedEventHandler = macNetworkChange.networkAvailabilityChanged;
			if (networkAvailabilityChangedEventHandler != null)
			{
				networkAvailabilityChangedEventHandler(null, new NetworkAvailabilityEventArgs(macNetworkChange.IsAvailable));
			}
		}

		// Token: 0x04002070 RID: 8304
		private const string DL_LIB = "/usr/lib/libSystem.dylib";

		// Token: 0x04002071 RID: 8305
		private const string CORE_SERVICES_LIB = "/System/Library/Frameworks/SystemConfiguration.framework/SystemConfiguration";

		// Token: 0x04002072 RID: 8306
		private const string CORE_FOUNDATION_LIB = "/System/Library/Frameworks/CoreFoundation.framework/CoreFoundation";

		// Token: 0x04002073 RID: 8307
		private IntPtr handle;

		// Token: 0x04002074 RID: 8308
		private IntPtr runLoopMode;

		// Token: 0x04002075 RID: 8309
		private MacNetworkChange.SCNetworkReachabilityCallback callback;

		// Token: 0x04002076 RID: 8310
		private bool scheduledWithRunLoop;

		// Token: 0x04002077 RID: 8311
		private MacNetworkChange.NetworkReachabilityFlags flags;

		// Token: 0x04002078 RID: 8312
		[CompilerGenerated]
		private NetworkAddressChangedEventHandler networkAddressChanged;

		// Token: 0x04002079 RID: 8313
		[CompilerGenerated]
		private NetworkAvailabilityChangedEventHandler networkAvailabilityChanged;

		// Token: 0x020004F6 RID: 1270
		// (Invoke) Token: 0x060028FD RID: 10493
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		private delegate void SCNetworkReachabilityCallback(IntPtr target, MacNetworkChange.NetworkReachabilityFlags flags, IntPtr info);

		// Token: 0x020004F7 RID: 1271
		[StructLayout(LayoutKind.Explicit, Size = 28)]
		private struct sockaddr_in
		{
			// Token: 0x06002900 RID: 10496 RVA: 0x0008F2BC File Offset: 0x0008D4BC
			public static MacNetworkChange.sockaddr_in Create()
			{
				return new MacNetworkChange.sockaddr_in
				{
					sin_len = 28,
					sin_family = 2
				};
			}

			// Token: 0x0400207A RID: 8314
			[FieldOffset(0)]
			public byte sin_len;

			// Token: 0x0400207B RID: 8315
			[FieldOffset(1)]
			public byte sin_family;
		}

		// Token: 0x020004F8 RID: 1272
		private struct SCNetworkReachabilityContext
		{
			// Token: 0x0400207C RID: 8316
			public IntPtr version;

			// Token: 0x0400207D RID: 8317
			public IntPtr info;

			// Token: 0x0400207E RID: 8318
			public IntPtr retain;

			// Token: 0x0400207F RID: 8319
			public IntPtr release;

			// Token: 0x04002080 RID: 8320
			public IntPtr copyDescription;
		}

		// Token: 0x020004F9 RID: 1273
		[Flags]
		private enum NetworkReachabilityFlags
		{
			// Token: 0x04002082 RID: 8322
			None = 0,
			// Token: 0x04002083 RID: 8323
			TransientConnection = 1,
			// Token: 0x04002084 RID: 8324
			Reachable = 2,
			// Token: 0x04002085 RID: 8325
			ConnectionRequired = 4,
			// Token: 0x04002086 RID: 8326
			ConnectionOnTraffic = 8,
			// Token: 0x04002087 RID: 8327
			InterventionRequired = 16,
			// Token: 0x04002088 RID: 8328
			ConnectionOnDemand = 32,
			// Token: 0x04002089 RID: 8329
			IsLocalAddress = 65536,
			// Token: 0x0400208A RID: 8330
			IsDirect = 131072,
			// Token: 0x0400208B RID: 8331
			IsWWAN = 262144,
			// Token: 0x0400208C RID: 8332
			ConnectionAutomatic = 8
		}
	}
}
