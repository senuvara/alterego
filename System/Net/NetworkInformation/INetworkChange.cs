﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004F3 RID: 1267
	internal interface INetworkChange : IDisposable
	{
		// Token: 0x1400003F RID: 63
		// (add) Token: 0x060028D7 RID: 10455
		// (remove) Token: 0x060028D8 RID: 10456
		event NetworkAddressChangedEventHandler NetworkAddressChanged;

		// Token: 0x14000040 RID: 64
		// (add) Token: 0x060028D9 RID: 10457
		// (remove) Token: 0x060028DA RID: 10458
		event NetworkAvailabilityChangedEventHandler NetworkAvailabilityChanged;

		// Token: 0x17000A04 RID: 2564
		// (get) Token: 0x060028DB RID: 10459
		bool HasRegisteredEvents { get; }
	}
}
