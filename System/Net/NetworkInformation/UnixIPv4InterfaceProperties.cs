﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004D8 RID: 1240
	internal abstract class UnixIPv4InterfaceProperties : IPv4InterfaceProperties
	{
		// Token: 0x0600281B RID: 10267 RVA: 0x0008E2A8 File Offset: 0x0008C4A8
		public UnixIPv4InterfaceProperties(UnixNetworkInterface iface)
		{
			this.iface = iface;
		}

		// Token: 0x1700095A RID: 2394
		// (get) Token: 0x0600281C RID: 10268 RVA: 0x0008E2B7 File Offset: 0x0008C4B7
		public override int Index
		{
			get
			{
				return this.iface.NameIndex;
			}
		}

		// Token: 0x1700095B RID: 2395
		// (get) Token: 0x0600281D RID: 10269 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool IsAutomaticPrivateAddressingActive
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700095C RID: 2396
		// (get) Token: 0x0600281E RID: 10270 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool IsAutomaticPrivateAddressingEnabled
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700095D RID: 2397
		// (get) Token: 0x0600281F RID: 10271 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool IsDhcpEnabled
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700095E RID: 2398
		// (get) Token: 0x06002820 RID: 10272 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool UsesWins
		{
			get
			{
				return false;
			}
		}

		// Token: 0x04001FF8 RID: 8184
		protected UnixNetworkInterface iface;
	}
}
