﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200051D RID: 1309
	internal struct Win32_IP_ADDR_STRING
	{
		// Token: 0x0400215A RID: 8538
		public IntPtr Next;

		// Token: 0x0400215B RID: 8539
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
		public string IpAddress;

		// Token: 0x0400215C RID: 8540
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)]
		public string IpMask;

		// Token: 0x0400215D RID: 8541
		public uint Context;
	}
}
