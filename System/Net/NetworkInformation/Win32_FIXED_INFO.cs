﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000518 RID: 1304
	internal struct Win32_FIXED_INFO
	{
		// Token: 0x040020EF RID: 8431
		private const int MAX_HOSTNAME_LEN = 128;

		// Token: 0x040020F0 RID: 8432
		private const int MAX_DOMAIN_NAME_LEN = 128;

		// Token: 0x040020F1 RID: 8433
		private const int MAX_SCOPE_ID_LEN = 256;

		// Token: 0x040020F2 RID: 8434
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 132)]
		public string HostName;

		// Token: 0x040020F3 RID: 8435
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 132)]
		public string DomainName;

		// Token: 0x040020F4 RID: 8436
		public IntPtr CurrentDnsServer;

		// Token: 0x040020F5 RID: 8437
		public Win32_IP_ADDR_STRING DnsServerList;

		// Token: 0x040020F6 RID: 8438
		public NetBiosNodeType NodeType;

		// Token: 0x040020F7 RID: 8439
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
		public string ScopeId;

		// Token: 0x040020F8 RID: 8440
		public uint EnableRouting;

		// Token: 0x040020F9 RID: 8441
		public uint EnableProxy;

		// Token: 0x040020FA RID: 8442
		public uint EnableDns;
	}
}
