﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004BA RID: 1210
	internal class SystemGatewayIPAddressInformation : GatewayIPAddressInformation
	{
		// Token: 0x06002725 RID: 10021 RVA: 0x0008C6A0 File Offset: 0x0008A8A0
		internal SystemGatewayIPAddressInformation(IPAddress address)
		{
			this.address = address;
		}

		// Token: 0x170008D6 RID: 2262
		// (get) Token: 0x06002726 RID: 10022 RVA: 0x0008C6AF File Offset: 0x0008A8AF
		public override IPAddress Address
		{
			get
			{
				return this.address;
			}
		}

		// Token: 0x06002727 RID: 10023 RVA: 0x0008C6B8 File Offset: 0x0008A8B8
		internal static GatewayIPAddressInformationCollection ToGatewayIpAddressInformationCollection(IPAddressCollection addresses)
		{
			GatewayIPAddressInformationCollection gatewayIPAddressInformationCollection = new GatewayIPAddressInformationCollection();
			foreach (IPAddress ipaddress in addresses)
			{
				gatewayIPAddressInformationCollection.InternalAdd(new SystemGatewayIPAddressInformation(ipaddress));
			}
			return gatewayIPAddressInformationCollection;
		}

		// Token: 0x04001F7C RID: 8060
		private IPAddress address;
	}
}
