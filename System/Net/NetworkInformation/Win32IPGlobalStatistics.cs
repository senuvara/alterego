﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004D2 RID: 1234
	internal class Win32IPGlobalStatistics : IPGlobalStatistics
	{
		// Token: 0x060027DF RID: 10207 RVA: 0x0008D8DB File Offset: 0x0008BADB
		public Win32IPGlobalStatistics(Win32_MIB_IPSTATS info)
		{
			this.info = info;
		}

		// Token: 0x1700092F RID: 2351
		// (get) Token: 0x060027E0 RID: 10208 RVA: 0x0008D8EA File Offset: 0x0008BAEA
		public override int DefaultTtl
		{
			get
			{
				return this.info.DefaultTTL;
			}
		}

		// Token: 0x17000930 RID: 2352
		// (get) Token: 0x060027E1 RID: 10209 RVA: 0x0008D8F7 File Offset: 0x0008BAF7
		public override bool ForwardingEnabled
		{
			get
			{
				return this.info.Forwarding != 0;
			}
		}

		// Token: 0x17000931 RID: 2353
		// (get) Token: 0x060027E2 RID: 10210 RVA: 0x0008D907 File Offset: 0x0008BB07
		public override int NumberOfInterfaces
		{
			get
			{
				return this.info.NumIf;
			}
		}

		// Token: 0x17000932 RID: 2354
		// (get) Token: 0x060027E3 RID: 10211 RVA: 0x0008D914 File Offset: 0x0008BB14
		public override int NumberOfIPAddresses
		{
			get
			{
				return this.info.NumAddr;
			}
		}

		// Token: 0x17000933 RID: 2355
		// (get) Token: 0x060027E4 RID: 10212 RVA: 0x0008D921 File Offset: 0x0008BB21
		public override int NumberOfRoutes
		{
			get
			{
				return this.info.NumRoutes;
			}
		}

		// Token: 0x17000934 RID: 2356
		// (get) Token: 0x060027E5 RID: 10213 RVA: 0x0008D92E File Offset: 0x0008BB2E
		public override long OutputPacketRequests
		{
			get
			{
				return (long)((ulong)this.info.OutRequests);
			}
		}

		// Token: 0x17000935 RID: 2357
		// (get) Token: 0x060027E6 RID: 10214 RVA: 0x0008D93C File Offset: 0x0008BB3C
		public override long OutputPacketRoutingDiscards
		{
			get
			{
				return (long)((ulong)this.info.RoutingDiscards);
			}
		}

		// Token: 0x17000936 RID: 2358
		// (get) Token: 0x060027E7 RID: 10215 RVA: 0x0008D94A File Offset: 0x0008BB4A
		public override long OutputPacketsDiscarded
		{
			get
			{
				return (long)((ulong)this.info.OutDiscards);
			}
		}

		// Token: 0x17000937 RID: 2359
		// (get) Token: 0x060027E8 RID: 10216 RVA: 0x0008D958 File Offset: 0x0008BB58
		public override long OutputPacketsWithNoRoute
		{
			get
			{
				return (long)((ulong)this.info.OutNoRoutes);
			}
		}

		// Token: 0x17000938 RID: 2360
		// (get) Token: 0x060027E9 RID: 10217 RVA: 0x0008D966 File Offset: 0x0008BB66
		public override long PacketFragmentFailures
		{
			get
			{
				return (long)((ulong)this.info.FragFails);
			}
		}

		// Token: 0x17000939 RID: 2361
		// (get) Token: 0x060027EA RID: 10218 RVA: 0x0008D974 File Offset: 0x0008BB74
		public override long PacketReassembliesRequired
		{
			get
			{
				return (long)((ulong)this.info.ReasmReqds);
			}
		}

		// Token: 0x1700093A RID: 2362
		// (get) Token: 0x060027EB RID: 10219 RVA: 0x0008D982 File Offset: 0x0008BB82
		public override long PacketReassemblyFailures
		{
			get
			{
				return (long)((ulong)this.info.ReasmFails);
			}
		}

		// Token: 0x1700093B RID: 2363
		// (get) Token: 0x060027EC RID: 10220 RVA: 0x0008D990 File Offset: 0x0008BB90
		public override long PacketReassemblyTimeout
		{
			get
			{
				return (long)((ulong)this.info.ReasmTimeout);
			}
		}

		// Token: 0x1700093C RID: 2364
		// (get) Token: 0x060027ED RID: 10221 RVA: 0x0008D99E File Offset: 0x0008BB9E
		public override long PacketsFragmented
		{
			get
			{
				return (long)((ulong)this.info.FragOks);
			}
		}

		// Token: 0x1700093D RID: 2365
		// (get) Token: 0x060027EE RID: 10222 RVA: 0x0008D9AC File Offset: 0x0008BBAC
		public override long PacketsReassembled
		{
			get
			{
				return (long)((ulong)this.info.ReasmOks);
			}
		}

		// Token: 0x1700093E RID: 2366
		// (get) Token: 0x060027EF RID: 10223 RVA: 0x0008D9BA File Offset: 0x0008BBBA
		public override long ReceivedPackets
		{
			get
			{
				return (long)((ulong)this.info.InReceives);
			}
		}

		// Token: 0x1700093F RID: 2367
		// (get) Token: 0x060027F0 RID: 10224 RVA: 0x0008D9C8 File Offset: 0x0008BBC8
		public override long ReceivedPacketsDelivered
		{
			get
			{
				return (long)((ulong)this.info.InDelivers);
			}
		}

		// Token: 0x17000940 RID: 2368
		// (get) Token: 0x060027F1 RID: 10225 RVA: 0x0008D9D6 File Offset: 0x0008BBD6
		public override long ReceivedPacketsDiscarded
		{
			get
			{
				return (long)((ulong)this.info.InDiscards);
			}
		}

		// Token: 0x17000941 RID: 2369
		// (get) Token: 0x060027F2 RID: 10226 RVA: 0x0008D9E4 File Offset: 0x0008BBE4
		public override long ReceivedPacketsForwarded
		{
			get
			{
				return (long)((ulong)this.info.ForwDatagrams);
			}
		}

		// Token: 0x17000942 RID: 2370
		// (get) Token: 0x060027F3 RID: 10227 RVA: 0x0008D9F2 File Offset: 0x0008BBF2
		public override long ReceivedPacketsWithAddressErrors
		{
			get
			{
				return (long)((ulong)this.info.InAddrErrors);
			}
		}

		// Token: 0x17000943 RID: 2371
		// (get) Token: 0x060027F4 RID: 10228 RVA: 0x0008DA00 File Offset: 0x0008BC00
		public override long ReceivedPacketsWithHeadersErrors
		{
			get
			{
				return (long)((ulong)this.info.InHdrErrors);
			}
		}

		// Token: 0x17000944 RID: 2372
		// (get) Token: 0x060027F5 RID: 10229 RVA: 0x0008DA0E File Offset: 0x0008BC0E
		public override long ReceivedPacketsWithUnknownProtocol
		{
			get
			{
				return (long)((ulong)this.info.InUnknownProtos);
			}
		}

		// Token: 0x04001FD5 RID: 8149
		private Win32_MIB_IPSTATS info;
	}
}
