﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000514 RID: 1300
	internal struct Win32_MIB_UDPSTATS
	{
		// Token: 0x040020E4 RID: 8420
		public uint InDatagrams;

		// Token: 0x040020E5 RID: 8421
		public uint NoPorts;

		// Token: 0x040020E6 RID: 8422
		public uint InErrors;

		// Token: 0x040020E7 RID: 8423
		public uint OutDatagrams;

		// Token: 0x040020E8 RID: 8424
		public int NumAddrs;
	}
}
