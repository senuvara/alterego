﻿using System;
using System.Collections.Specialized;
using System.Globalization;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004E5 RID: 1253
	internal class MibIcmpV6Statistics : IcmpV6Statistics
	{
		// Token: 0x06002893 RID: 10387 RVA: 0x0008E904 File Offset: 0x0008CB04
		public MibIcmpV6Statistics(StringDictionary dic)
		{
			this.dic = dic;
		}

		// Token: 0x06002894 RID: 10388 RVA: 0x0008E913 File Offset: 0x0008CB13
		private long Get(string name)
		{
			if (this.dic[name] == null)
			{
				return 0L;
			}
			return long.Parse(this.dic[name], NumberFormatInfo.InvariantInfo);
		}

		// Token: 0x170009C4 RID: 2500
		// (get) Token: 0x06002895 RID: 10389 RVA: 0x0008E93C File Offset: 0x0008CB3C
		public override long DestinationUnreachableMessagesReceived
		{
			get
			{
				return this.Get("InDestUnreachs");
			}
		}

		// Token: 0x170009C5 RID: 2501
		// (get) Token: 0x06002896 RID: 10390 RVA: 0x0008E949 File Offset: 0x0008CB49
		public override long DestinationUnreachableMessagesSent
		{
			get
			{
				return this.Get("OutDestUnreachs");
			}
		}

		// Token: 0x170009C6 RID: 2502
		// (get) Token: 0x06002897 RID: 10391 RVA: 0x0008E956 File Offset: 0x0008CB56
		public override long EchoRepliesReceived
		{
			get
			{
				return this.Get("InEchoReplies");
			}
		}

		// Token: 0x170009C7 RID: 2503
		// (get) Token: 0x06002898 RID: 10392 RVA: 0x0008E963 File Offset: 0x0008CB63
		public override long EchoRepliesSent
		{
			get
			{
				return this.Get("OutEchoReplies");
			}
		}

		// Token: 0x170009C8 RID: 2504
		// (get) Token: 0x06002899 RID: 10393 RVA: 0x0008E970 File Offset: 0x0008CB70
		public override long EchoRequestsReceived
		{
			get
			{
				return this.Get("InEchos");
			}
		}

		// Token: 0x170009C9 RID: 2505
		// (get) Token: 0x0600289A RID: 10394 RVA: 0x0008E97D File Offset: 0x0008CB7D
		public override long EchoRequestsSent
		{
			get
			{
				return this.Get("OutEchos");
			}
		}

		// Token: 0x170009CA RID: 2506
		// (get) Token: 0x0600289B RID: 10395 RVA: 0x0008E98A File Offset: 0x0008CB8A
		public override long ErrorsReceived
		{
			get
			{
				return this.Get("InErrors");
			}
		}

		// Token: 0x170009CB RID: 2507
		// (get) Token: 0x0600289C RID: 10396 RVA: 0x0008E997 File Offset: 0x0008CB97
		public override long ErrorsSent
		{
			get
			{
				return this.Get("OutErrors");
			}
		}

		// Token: 0x170009CC RID: 2508
		// (get) Token: 0x0600289D RID: 10397 RVA: 0x0008E9A4 File Offset: 0x0008CBA4
		public override long MembershipQueriesReceived
		{
			get
			{
				return this.Get("InGroupMembQueries");
			}
		}

		// Token: 0x170009CD RID: 2509
		// (get) Token: 0x0600289E RID: 10398 RVA: 0x0008E9B1 File Offset: 0x0008CBB1
		public override long MembershipQueriesSent
		{
			get
			{
				return this.Get("OutGroupMembQueries");
			}
		}

		// Token: 0x170009CE RID: 2510
		// (get) Token: 0x0600289F RID: 10399 RVA: 0x0008E9BE File Offset: 0x0008CBBE
		public override long MembershipReductionsReceived
		{
			get
			{
				return this.Get("InGroupMembReductiions");
			}
		}

		// Token: 0x170009CF RID: 2511
		// (get) Token: 0x060028A0 RID: 10400 RVA: 0x0008E9CB File Offset: 0x0008CBCB
		public override long MembershipReductionsSent
		{
			get
			{
				return this.Get("OutGroupMembReductiions");
			}
		}

		// Token: 0x170009D0 RID: 2512
		// (get) Token: 0x060028A1 RID: 10401 RVA: 0x0008E9D8 File Offset: 0x0008CBD8
		public override long MembershipReportsReceived
		{
			get
			{
				return this.Get("InGroupMembRespons");
			}
		}

		// Token: 0x170009D1 RID: 2513
		// (get) Token: 0x060028A2 RID: 10402 RVA: 0x0008E9E5 File Offset: 0x0008CBE5
		public override long MembershipReportsSent
		{
			get
			{
				return this.Get("OutGroupMembRespons");
			}
		}

		// Token: 0x170009D2 RID: 2514
		// (get) Token: 0x060028A3 RID: 10403 RVA: 0x0008E9F2 File Offset: 0x0008CBF2
		public override long MessagesReceived
		{
			get
			{
				return this.Get("InMsgs");
			}
		}

		// Token: 0x170009D3 RID: 2515
		// (get) Token: 0x060028A4 RID: 10404 RVA: 0x0008E9FF File Offset: 0x0008CBFF
		public override long MessagesSent
		{
			get
			{
				return this.Get("OutMsgs");
			}
		}

		// Token: 0x170009D4 RID: 2516
		// (get) Token: 0x060028A5 RID: 10405 RVA: 0x0008EA0C File Offset: 0x0008CC0C
		public override long NeighborAdvertisementsReceived
		{
			get
			{
				return this.Get("InNeighborAdvertisements");
			}
		}

		// Token: 0x170009D5 RID: 2517
		// (get) Token: 0x060028A6 RID: 10406 RVA: 0x0008EA19 File Offset: 0x0008CC19
		public override long NeighborAdvertisementsSent
		{
			get
			{
				return this.Get("OutNeighborAdvertisements");
			}
		}

		// Token: 0x170009D6 RID: 2518
		// (get) Token: 0x060028A7 RID: 10407 RVA: 0x0008EA26 File Offset: 0x0008CC26
		public override long NeighborSolicitsReceived
		{
			get
			{
				return this.Get("InNeighborSolicits");
			}
		}

		// Token: 0x170009D7 RID: 2519
		// (get) Token: 0x060028A8 RID: 10408 RVA: 0x0008EA33 File Offset: 0x0008CC33
		public override long NeighborSolicitsSent
		{
			get
			{
				return this.Get("OutNeighborSolicits");
			}
		}

		// Token: 0x170009D8 RID: 2520
		// (get) Token: 0x060028A9 RID: 10409 RVA: 0x0008EA40 File Offset: 0x0008CC40
		public override long PacketTooBigMessagesReceived
		{
			get
			{
				return this.Get("InPktTooBigs");
			}
		}

		// Token: 0x170009D9 RID: 2521
		// (get) Token: 0x060028AA RID: 10410 RVA: 0x0008EA4D File Offset: 0x0008CC4D
		public override long PacketTooBigMessagesSent
		{
			get
			{
				return this.Get("OutPktTooBigs");
			}
		}

		// Token: 0x170009DA RID: 2522
		// (get) Token: 0x060028AB RID: 10411 RVA: 0x0008EA5A File Offset: 0x0008CC5A
		public override long ParameterProblemsReceived
		{
			get
			{
				return this.Get("InParmProblems");
			}
		}

		// Token: 0x170009DB RID: 2523
		// (get) Token: 0x060028AC RID: 10412 RVA: 0x0008EA67 File Offset: 0x0008CC67
		public override long ParameterProblemsSent
		{
			get
			{
				return this.Get("OutParmProblems");
			}
		}

		// Token: 0x170009DC RID: 2524
		// (get) Token: 0x060028AD RID: 10413 RVA: 0x0008EA74 File Offset: 0x0008CC74
		public override long RedirectsReceived
		{
			get
			{
				return this.Get("InRedirects");
			}
		}

		// Token: 0x170009DD RID: 2525
		// (get) Token: 0x060028AE RID: 10414 RVA: 0x0008EA81 File Offset: 0x0008CC81
		public override long RedirectsSent
		{
			get
			{
				return this.Get("OutRedirects");
			}
		}

		// Token: 0x170009DE RID: 2526
		// (get) Token: 0x060028AF RID: 10415 RVA: 0x0008EA8E File Offset: 0x0008CC8E
		public override long RouterAdvertisementsReceived
		{
			get
			{
				return this.Get("InRouterAdvertisements");
			}
		}

		// Token: 0x170009DF RID: 2527
		// (get) Token: 0x060028B0 RID: 10416 RVA: 0x0008EA9B File Offset: 0x0008CC9B
		public override long RouterAdvertisementsSent
		{
			get
			{
				return this.Get("OutRouterAdvertisements");
			}
		}

		// Token: 0x170009E0 RID: 2528
		// (get) Token: 0x060028B1 RID: 10417 RVA: 0x0008EAA8 File Offset: 0x0008CCA8
		public override long RouterSolicitsReceived
		{
			get
			{
				return this.Get("InRouterSolicits");
			}
		}

		// Token: 0x170009E1 RID: 2529
		// (get) Token: 0x060028B2 RID: 10418 RVA: 0x0008EAB5 File Offset: 0x0008CCB5
		public override long RouterSolicitsSent
		{
			get
			{
				return this.Get("OutRouterSolicits");
			}
		}

		// Token: 0x170009E2 RID: 2530
		// (get) Token: 0x060028B3 RID: 10419 RVA: 0x0008EAC2 File Offset: 0x0008CCC2
		public override long TimeExceededMessagesReceived
		{
			get
			{
				return this.Get("InTimeExcds");
			}
		}

		// Token: 0x170009E3 RID: 2531
		// (get) Token: 0x060028B4 RID: 10420 RVA: 0x0008EACF File Offset: 0x0008CCCF
		public override long TimeExceededMessagesSent
		{
			get
			{
				return this.Get("OutTimeExcds");
			}
		}

		// Token: 0x04002015 RID: 8213
		private StringDictionary dic;
	}
}
