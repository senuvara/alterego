﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000520 RID: 1312
	internal struct Win32_IP_ADAPTER_DNS_SERVER_ADDRESS
	{
		// Token: 0x04002165 RID: 8549
		public Win32LengthFlagsUnion LengthFlags;

		// Token: 0x04002166 RID: 8550
		public IntPtr Next;

		// Token: 0x04002167 RID: 8551
		public Win32_SOCKET_ADDRESS Address;
	}
}
