﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004FC RID: 1276
	internal static class SystemNetworkInterface
	{
		// Token: 0x06002917 RID: 10519 RVA: 0x0008F744 File Offset: 0x0008D944
		public static NetworkInterface[] GetNetworkInterfaces()
		{
			NetworkInterface[] result;
			try
			{
				result = SystemNetworkInterface.nif.GetAllNetworkInterfaces();
			}
			catch
			{
				result = new NetworkInterface[0];
			}
			return result;
		}

		// Token: 0x06002918 RID: 10520 RVA: 0x00003298 File Offset: 0x00001498
		public static bool InternalGetIsNetworkAvailable()
		{
			return true;
		}

		// Token: 0x17000A08 RID: 2568
		// (get) Token: 0x06002919 RID: 10521 RVA: 0x0008F77C File Offset: 0x0008D97C
		public static int InternalLoopbackInterfaceIndex
		{
			get
			{
				return SystemNetworkInterface.nif.GetLoopbackInterfaceIndex();
			}
		}

		// Token: 0x17000A09 RID: 2569
		// (get) Token: 0x0600291A RID: 10522 RVA: 0x000068D7 File Offset: 0x00004AD7
		public static int InternalIPv6LoopbackInterfaceIndex
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x0600291B RID: 10523 RVA: 0x0008F788 File Offset: 0x0008D988
		public static IPAddress GetNetMask(IPAddress address)
		{
			return SystemNetworkInterface.nif.GetNetMask(address);
		}

		// Token: 0x0600291C RID: 10524 RVA: 0x0008F795 File Offset: 0x0008D995
		// Note: this type is marked as 'beforefieldinit'.
		static SystemNetworkInterface()
		{
		}

		// Token: 0x04002098 RID: 8344
		private static readonly NetworkInterfaceFactory nif = NetworkInterfaceFactory.Create();
	}
}
