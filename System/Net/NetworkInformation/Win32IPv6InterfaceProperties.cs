﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004E0 RID: 1248
	internal class Win32IPv6InterfaceProperties : IPv6InterfaceProperties
	{
		// Token: 0x06002859 RID: 10329 RVA: 0x0008E5C5 File Offset: 0x0008C7C5
		public Win32IPv6InterfaceProperties(Win32_MIB_IFROW mib)
		{
			this.mib = mib;
		}

		// Token: 0x1700098E RID: 2446
		// (get) Token: 0x0600285A RID: 10330 RVA: 0x0008E5D4 File Offset: 0x0008C7D4
		public override int Index
		{
			get
			{
				return this.mib.Index;
			}
		}

		// Token: 0x1700098F RID: 2447
		// (get) Token: 0x0600285B RID: 10331 RVA: 0x0008E5E1 File Offset: 0x0008C7E1
		public override int Mtu
		{
			get
			{
				return this.mib.Mtu;
			}
		}

		// Token: 0x04002002 RID: 8194
		private Win32_MIB_IFROW mib;
	}
}
