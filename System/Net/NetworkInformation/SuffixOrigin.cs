﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Specifies how an IP address host suffix was located.</summary>
	// Token: 0x020004B9 RID: 1209
	public enum SuffixOrigin
	{
		/// <summary>The suffix was located using an unspecified source.</summary>
		// Token: 0x04001F76 RID: 8054
		Other,
		/// <summary>The suffix was manually configured.</summary>
		// Token: 0x04001F77 RID: 8055
		Manual,
		/// <summary>The suffix is a well-known suffix. Well-known suffixes are specified in standard-track Request for Comments (RFC) documents and assigned by the Internet Assigned Numbers Authority (Iana) or an address registry. Such suffixes are reserved for special purposes.</summary>
		// Token: 0x04001F78 RID: 8056
		WellKnown,
		/// <summary>The suffix was supplied by a Dynamic Host Configuration Protocol (DHCP) server.</summary>
		// Token: 0x04001F79 RID: 8057
		OriginDhcp,
		/// <summary>The suffix is a link-local suffix.</summary>
		// Token: 0x04001F7A RID: 8058
		LinkLayerAddress,
		/// <summary>The suffix was randomly assigned.</summary>
		// Token: 0x04001F7B RID: 8059
		Random
	}
}
