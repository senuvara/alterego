﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004E8 RID: 1256
	internal struct Win32_MIB_ICMP_EX
	{
		// Token: 0x04002027 RID: 8231
		public Win32_MIBICMPSTATS_EX InStats;

		// Token: 0x04002028 RID: 8232
		public Win32_MIBICMPSTATS_EX OutStats;
	}
}
