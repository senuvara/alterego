﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation.MacOsStructs
{
	// Token: 0x0200052A RID: 1322
	internal struct in6_addr
	{
		// Token: 0x0400218D RID: 8589
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
		public byte[] u6_addr8;
	}
}
