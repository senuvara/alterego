﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation.MacOsStructs
{
	// Token: 0x0200052C RID: 1324
	internal struct sockaddr_dl
	{
		// Token: 0x060029F3 RID: 10739 RVA: 0x00091BC8 File Offset: 0x0008FDC8
		internal void Read(IntPtr ptr)
		{
			this.sdl_len = Marshal.ReadByte(ptr, 0);
			this.sdl_family = Marshal.ReadByte(ptr, 1);
			this.sdl_index = (ushort)Marshal.ReadInt16(ptr, 2);
			this.sdl_type = Marshal.ReadByte(ptr, 4);
			this.sdl_nlen = Marshal.ReadByte(ptr, 5);
			this.sdl_alen = Marshal.ReadByte(ptr, 6);
			this.sdl_slen = Marshal.ReadByte(ptr, 7);
			this.sdl_data = new byte[Math.Max(12, (int)(this.sdl_len - 8))];
			Marshal.Copy(new IntPtr(ptr.ToInt64() + 8L), this.sdl_data, 0, this.sdl_data.Length);
		}

		// Token: 0x04002194 RID: 8596
		public byte sdl_len;

		// Token: 0x04002195 RID: 8597
		public byte sdl_family;

		// Token: 0x04002196 RID: 8598
		public ushort sdl_index;

		// Token: 0x04002197 RID: 8599
		public byte sdl_type;

		// Token: 0x04002198 RID: 8600
		public byte sdl_nlen;

		// Token: 0x04002199 RID: 8601
		public byte sdl_alen;

		// Token: 0x0400219A RID: 8602
		public byte sdl_slen;

		// Token: 0x0400219B RID: 8603
		public byte[] sdl_data;
	}
}
