﻿using System;

namespace System.Net.NetworkInformation.MacOsStructs
{
	// Token: 0x02000529 RID: 1321
	internal struct sockaddr_in
	{
		// Token: 0x04002189 RID: 8585
		public byte sin_len;

		// Token: 0x0400218A RID: 8586
		public byte sin_family;

		// Token: 0x0400218B RID: 8587
		public ushort sin_port;

		// Token: 0x0400218C RID: 8588
		public uint sin_addr;
	}
}
