﻿using System;

namespace System.Net.NetworkInformation.MacOsStructs
{
	// Token: 0x02000528 RID: 1320
	internal struct sockaddr
	{
		// Token: 0x04002187 RID: 8583
		public byte sa_len;

		// Token: 0x04002188 RID: 8584
		public byte sa_family;
	}
}
