﻿using System;

namespace System.Net.NetworkInformation.MacOsStructs
{
	// Token: 0x02000527 RID: 1319
	internal struct ifaddrs
	{
		// Token: 0x04002180 RID: 8576
		public IntPtr ifa_next;

		// Token: 0x04002181 RID: 8577
		public string ifa_name;

		// Token: 0x04002182 RID: 8578
		public uint ifa_flags;

		// Token: 0x04002183 RID: 8579
		public IntPtr ifa_addr;

		// Token: 0x04002184 RID: 8580
		public IntPtr ifa_netmask;

		// Token: 0x04002185 RID: 8581
		public IntPtr ifa_dstaddr;

		// Token: 0x04002186 RID: 8582
		public IntPtr ifa_data;
	}
}
