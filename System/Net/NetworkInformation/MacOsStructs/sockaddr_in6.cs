﻿using System;

namespace System.Net.NetworkInformation.MacOsStructs
{
	// Token: 0x0200052B RID: 1323
	internal struct sockaddr_in6
	{
		// Token: 0x0400218E RID: 8590
		public byte sin6_len;

		// Token: 0x0400218F RID: 8591
		public byte sin6_family;

		// Token: 0x04002190 RID: 8592
		public ushort sin6_port;

		// Token: 0x04002191 RID: 8593
		public uint sin6_flowinfo;

		// Token: 0x04002192 RID: 8594
		public in6_addr sin6_addr;

		// Token: 0x04002193 RID: 8595
		public uint sin6_scope_id;
	}
}
