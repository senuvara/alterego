﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004D5 RID: 1237
	internal class LinuxIPInterfaceProperties : UnixIPInterfaceProperties
	{
		// Token: 0x06002803 RID: 10243 RVA: 0x0008DD80 File Offset: 0x0008BF80
		public LinuxIPInterfaceProperties(LinuxNetworkInterface iface, List<IPAddress> addresses) : base(iface, addresses)
		{
		}

		// Token: 0x06002804 RID: 10244 RVA: 0x0008DD8A File Offset: 0x0008BF8A
		public override IPv4InterfaceProperties GetIPv4Properties()
		{
			if (this.ipv4iface_properties == null)
			{
				this.ipv4iface_properties = new LinuxIPv4InterfaceProperties(this.iface as LinuxNetworkInterface);
			}
			return this.ipv4iface_properties;
		}

		// Token: 0x06002805 RID: 10245 RVA: 0x0008DDB0 File Offset: 0x0008BFB0
		private IPAddressCollection ParseRouteInfo(string iface)
		{
			IPAddressCollection ipaddressCollection = new IPAddressCollection();
			try
			{
				using (StreamReader streamReader = new StreamReader("/proc/net/route"))
				{
					streamReader.ReadLine();
					string text;
					while ((text = streamReader.ReadLine()) != null)
					{
						text = text.Trim();
						if (text.Length != 0)
						{
							string[] array = text.Split(new char[]
							{
								'\t'
							});
							if (array.Length >= 3)
							{
								string text2 = array[2].Trim();
								byte[] array2 = new byte[4];
								if (text2.Length == 8 && iface.Equals(array[0], StringComparison.OrdinalIgnoreCase))
								{
									for (int i = 0; i < 4; i++)
									{
										byte.TryParse(text2.Substring(i * 2, 2), NumberStyles.HexNumber, null, out array2[3 - i]);
									}
									IPAddress ipaddress = new IPAddress(array2);
									if (!ipaddress.Equals(IPAddress.Any) && !ipaddressCollection.Contains(ipaddress))
									{
										ipaddressCollection.InternalAdd(ipaddress);
									}
								}
							}
						}
					}
				}
			}
			catch
			{
			}
			return ipaddressCollection;
		}

		// Token: 0x1700094E RID: 2382
		// (get) Token: 0x06002806 RID: 10246 RVA: 0x0008DEC8 File Offset: 0x0008C0C8
		public override GatewayIPAddressInformationCollection GatewayAddresses
		{
			get
			{
				return SystemGatewayIPAddressInformation.ToGatewayIpAddressInformationCollection(this.ParseRouteInfo(this.iface.Name.ToString()));
			}
		}
	}
}
