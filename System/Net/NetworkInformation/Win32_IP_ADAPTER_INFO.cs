﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200051B RID: 1307
	internal struct Win32_IP_ADAPTER_INFO
	{
		// Token: 0x0400212A RID: 8490
		private const int MAX_ADAPTER_NAME_LENGTH = 256;

		// Token: 0x0400212B RID: 8491
		private const int MAX_ADAPTER_DESCRIPTION_LENGTH = 128;

		// Token: 0x0400212C RID: 8492
		private const int MAX_ADAPTER_ADDRESS_LENGTH = 8;

		// Token: 0x0400212D RID: 8493
		public IntPtr Next;

		// Token: 0x0400212E RID: 8494
		public int ComboIndex;

		// Token: 0x0400212F RID: 8495
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
		public string AdapterName;

		// Token: 0x04002130 RID: 8496
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 132)]
		public string Description;

		// Token: 0x04002131 RID: 8497
		public uint AddressLength;

		// Token: 0x04002132 RID: 8498
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public byte[] Address;

		// Token: 0x04002133 RID: 8499
		public uint Index;

		// Token: 0x04002134 RID: 8500
		public uint Type;

		// Token: 0x04002135 RID: 8501
		public uint DhcpEnabled;

		// Token: 0x04002136 RID: 8502
		public IntPtr CurrentIpAddress;

		// Token: 0x04002137 RID: 8503
		public Win32_IP_ADDR_STRING IpAddressList;

		// Token: 0x04002138 RID: 8504
		public Win32_IP_ADDR_STRING GatewayList;

		// Token: 0x04002139 RID: 8505
		public Win32_IP_ADDR_STRING DhcpServer;

		// Token: 0x0400213A RID: 8506
		public bool HaveWins;

		// Token: 0x0400213B RID: 8507
		public Win32_IP_ADDR_STRING PrimaryWinsServer;

		// Token: 0x0400213C RID: 8508
		public Win32_IP_ADDR_STRING SecondaryWinsServer;

		// Token: 0x0400213D RID: 8509
		public long LeaseObtained;

		// Token: 0x0400213E RID: 8510
		public long LeaseExpires;
	}
}
