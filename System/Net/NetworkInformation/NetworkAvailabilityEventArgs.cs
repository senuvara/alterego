﻿using System;
using Unity;

namespace System.Net.NetworkInformation
{
	/// <summary>Provides data for the <see cref="E:System.Net.NetworkInformation.NetworkChange.NetworkAvailabilityChanged" /> event.</summary>
	// Token: 0x020004AA RID: 1194
	public class NetworkAvailabilityEventArgs : EventArgs
	{
		// Token: 0x060026DC RID: 9948 RVA: 0x0008BCC9 File Offset: 0x00089EC9
		internal NetworkAvailabilityEventArgs(bool isAvailable)
		{
			this.isAvailable = isAvailable;
		}

		/// <summary>Gets the current status of the network connection.</summary>
		/// <returns>
		///     <see langword="true" /> if the network is available; otherwise, <see langword="false" />.</returns>
		// Token: 0x170008C1 RID: 2241
		// (get) Token: 0x060026DD RID: 9949 RVA: 0x0008BCD8 File Offset: 0x00089ED8
		public bool IsAvailable
		{
			get
			{
				return this.isAvailable;
			}
		}

		// Token: 0x060026DE RID: 9950 RVA: 0x000092E2 File Offset: 0x000074E2
		internal NetworkAvailabilityEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001F46 RID: 8006
		private bool isAvailable;
	}
}
