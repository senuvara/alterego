﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000523 RID: 1315
	internal struct Win32_IP_ADAPTER_WINS_SERVER_ADDRESS
	{
		// Token: 0x0400216E RID: 8558
		public Win32LengthFlagsUnion LengthFlags;

		// Token: 0x0400216F RID: 8559
		public IntPtr Next;

		// Token: 0x04002170 RID: 8560
		public Win32_SOCKET_ADDRESS Address;
	}
}
