﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004C9 RID: 1225
	internal class UnixIPGlobalProperties : CommonUnixIPGlobalProperties
	{
		// Token: 0x0600277F RID: 10111 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override TcpConnectionInformation[] GetActiveTcpConnections()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002780 RID: 10112 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override IPEndPoint[] GetActiveTcpListeners()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002781 RID: 10113 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override IPEndPoint[] GetActiveUdpListeners()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002782 RID: 10114 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override IcmpV4Statistics GetIcmpV4Statistics()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002783 RID: 10115 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override IcmpV6Statistics GetIcmpV6Statistics()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002784 RID: 10116 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override IPGlobalStatistics GetIPv4GlobalStatistics()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002785 RID: 10117 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override IPGlobalStatistics GetIPv6GlobalStatistics()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002786 RID: 10118 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override TcpStatistics GetTcpIPv4Statistics()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002787 RID: 10119 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override TcpStatistics GetTcpIPv6Statistics()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002788 RID: 10120 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override UdpStatistics GetUdpIPv4Statistics()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002789 RID: 10121 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override UdpStatistics GetUdpIPv6Statistics()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600278A RID: 10122 RVA: 0x0008CB7F File Offset: 0x0008AD7F
		public UnixIPGlobalProperties()
		{
		}
	}
}
