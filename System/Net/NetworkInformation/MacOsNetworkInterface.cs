﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000505 RID: 1285
	internal sealed class MacOsNetworkInterface : UnixNetworkInterface
	{
		// Token: 0x06002955 RID: 10581 RVA: 0x000906E0 File Offset: 0x0008E8E0
		internal MacOsNetworkInterface(string name, uint ifa_flags) : base(name)
		{
			this._ifa_flags = ifa_flags;
		}

		// Token: 0x06002956 RID: 10582 RVA: 0x000906F0 File Offset: 0x0008E8F0
		public override IPInterfaceProperties GetIPProperties()
		{
			if (this.ipproperties == null)
			{
				this.ipproperties = new MacOsIPInterfaceProperties(this, this.addresses);
			}
			return this.ipproperties;
		}

		// Token: 0x06002957 RID: 10583 RVA: 0x00090712 File Offset: 0x0008E912
		public override IPv4InterfaceStatistics GetIPv4Statistics()
		{
			if (this.ipv4stats == null)
			{
				this.ipv4stats = new MacOsIPv4InterfaceStatistics(this);
			}
			return this.ipv4stats;
		}

		// Token: 0x17000A16 RID: 2582
		// (get) Token: 0x06002958 RID: 10584 RVA: 0x0009072E File Offset: 0x0008E92E
		public override OperationalStatus OperationalStatus
		{
			get
			{
				if ((this._ifa_flags & 1U) == 1U)
				{
					return OperationalStatus.Up;
				}
				return OperationalStatus.Unknown;
			}
		}

		// Token: 0x17000A17 RID: 2583
		// (get) Token: 0x06002959 RID: 10585 RVA: 0x0009073E File Offset: 0x0008E93E
		public override bool SupportsMulticast
		{
			get
			{
				return (this._ifa_flags & 32768U) == 32768U;
			}
		}

		// Token: 0x040020AE RID: 8366
		private uint _ifa_flags;
	}
}
