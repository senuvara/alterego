﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>The scope level for an IPv6 address.</summary>
	// Token: 0x020004B8 RID: 1208
	public enum ScopeLevel
	{
		/// <summary>The scope level is not specified.</summary>
		// Token: 0x04001F6D RID: 8045
		None,
		/// <summary>The scope is interface-level.</summary>
		// Token: 0x04001F6E RID: 8046
		Interface,
		/// <summary>The scope is link-level.</summary>
		// Token: 0x04001F6F RID: 8047
		Link,
		/// <summary>The scope is subnet-level.</summary>
		// Token: 0x04001F70 RID: 8048
		Subnet,
		/// <summary>The scope is admin-level.</summary>
		// Token: 0x04001F71 RID: 8049
		Admin,
		/// <summary>The scope is site-level.</summary>
		// Token: 0x04001F72 RID: 8050
		Site,
		/// <summary>The scope is organization-level.</summary>
		// Token: 0x04001F73 RID: 8051
		Organization = 8,
		/// <summary>The scope is global.</summary>
		// Token: 0x04001F74 RID: 8052
		Global = 14
	}
}
