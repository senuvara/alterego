﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004C8 RID: 1224
	internal abstract class CommonUnixIPGlobalProperties : IPGlobalProperties
	{
		// Token: 0x06002777 RID: 10103
		[DllImport("libc")]
		private static extern int gethostname([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] byte[] name, int len);

		// Token: 0x06002778 RID: 10104
		[DllImport("libc")]
		private static extern int getdomainname([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] byte[] name, int len);

		// Token: 0x17000907 RID: 2311
		// (get) Token: 0x06002779 RID: 10105 RVA: 0x0008CABA File Offset: 0x0008ACBA
		public override string DhcpScopeName
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x17000908 RID: 2312
		// (get) Token: 0x0600277A RID: 10106 RVA: 0x0008CAC4 File Offset: 0x0008ACC4
		public override string DomainName
		{
			get
			{
				byte[] array = new byte[256];
				try
				{
					if (CommonUnixIPGlobalProperties.getdomainname(array, 256) != 0)
					{
						throw new NetworkInformationException();
					}
				}
				catch (EntryPointNotFoundException)
				{
					return string.Empty;
				}
				int num = Array.IndexOf<byte>(array, 0);
				return Encoding.ASCII.GetString(array, 0, (num < 0) ? 256 : num);
			}
		}

		// Token: 0x17000909 RID: 2313
		// (get) Token: 0x0600277B RID: 10107 RVA: 0x0008CB2C File Offset: 0x0008AD2C
		public override string HostName
		{
			get
			{
				byte[] array = new byte[256];
				if (CommonUnixIPGlobalProperties.gethostname(array, 256) != 0)
				{
					throw new NetworkInformationException();
				}
				int num = Array.IndexOf<byte>(array, 0);
				return Encoding.ASCII.GetString(array, 0, (num < 0) ? 256 : num);
			}
		}

		// Token: 0x1700090A RID: 2314
		// (get) Token: 0x0600277C RID: 10108 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool IsWinsProxy
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700090B RID: 2315
		// (get) Token: 0x0600277D RID: 10109 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override NetBiosNodeType NodeType
		{
			get
			{
				return NetBiosNodeType.Unknown;
			}
		}

		// Token: 0x0600277E RID: 10110 RVA: 0x0008CB77 File Offset: 0x0008AD77
		protected CommonUnixIPGlobalProperties()
		{
		}
	}
}
