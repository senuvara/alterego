﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Provides configuration and statistical information for a network interface.</summary>
	// Token: 0x020004B1 RID: 1201
	public abstract class NetworkInterface
	{
		/// <summary>Returns objects that describe the network interfaces on the local computer.</summary>
		/// <returns>A <see cref="T:System.Net.NetworkInformation.NetworkInterface" /> array that contains objects that describe the available network interfaces, or an empty array if no interfaces are detected.</returns>
		/// <exception cref="T:System.Net.NetworkInformation.NetworkInformationException">A Windows system function call failed. </exception>
		// Token: 0x060026FC RID: 9980 RVA: 0x0008C17B File Offset: 0x0008A37B
		public static NetworkInterface[] GetAllNetworkInterfaces()
		{
			return SystemNetworkInterface.GetNetworkInterfaces();
		}

		/// <summary>Indicates whether any network connection is available.</summary>
		/// <returns>
		///     <see langword="true" /> if a network connection is available; otherwise, <see langword="false" />.</returns>
		// Token: 0x060026FD RID: 9981 RVA: 0x0008C182 File Offset: 0x0008A382
		public static bool GetIsNetworkAvailable()
		{
			return SystemNetworkInterface.InternalGetIsNetworkAvailable();
		}

		/// <summary>Gets the index of the IPv4 loopback interface.</summary>
		/// <returns>A <see cref="T:System.Int32" /> that contains the index for the IPv4 loopback interface.</returns>
		/// <exception cref="T:System.Net.NetworkInformation.NetworkInformationException">This property is not valid on computers running only Ipv6.</exception>
		// Token: 0x170008C5 RID: 2245
		// (get) Token: 0x060026FE RID: 9982 RVA: 0x0008C189 File Offset: 0x0008A389
		public static int LoopbackInterfaceIndex
		{
			get
			{
				return SystemNetworkInterface.InternalLoopbackInterfaceIndex;
			}
		}

		/// <summary>Gets the index of the IPv6 loopback interface.</summary>
		/// <returns>Returns <see cref="T:System.Int32" />.The index for the IPv6 loopback interface.</returns>
		// Token: 0x170008C6 RID: 2246
		// (get) Token: 0x060026FF RID: 9983 RVA: 0x0008C190 File Offset: 0x0008A390
		public static int IPv6LoopbackInterfaceIndex
		{
			get
			{
				return SystemNetworkInterface.InternalIPv6LoopbackInterfaceIndex;
			}
		}

		/// <summary>Gets the identifier of the network adapter.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the identifier.</returns>
		// Token: 0x170008C7 RID: 2247
		// (get) Token: 0x06002700 RID: 9984 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual string Id
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the name of the network adapter.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the adapter name.</returns>
		// Token: 0x170008C8 RID: 2248
		// (get) Token: 0x06002701 RID: 9985 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual string Name
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the description of the interface.</summary>
		/// <returns>A <see cref="T:System.String" /> that describes this interface.</returns>
		// Token: 0x170008C9 RID: 2249
		// (get) Token: 0x06002702 RID: 9986 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual string Description
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Returns an object that describes the configuration of this network interface.</summary>
		/// <returns>An <see cref="T:System.Net.NetworkInformation.IPInterfaceProperties" /> object that describes this network interface.</returns>
		// Token: 0x06002703 RID: 9987 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual IPInterfaceProperties GetIPProperties()
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the IPv4 statistics for this <see cref="T:System.Net.NetworkInformation.NetworkInterface" /> instance.</summary>
		/// <returns>An <see cref="T:System.Net.NetworkInformation.IPv4InterfaceStatistics" /> object.</returns>
		// Token: 0x06002704 RID: 9988 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual IPv4InterfaceStatistics GetIPv4Statistics()
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the IP statistics for this <see cref="T:System.Net.NetworkInformation.NetworkInterface" /> instance.</summary>
		/// <returns>Returns <see cref="T:System.Net.NetworkInformation.IPInterfaceStatistics" />.The IP statistics.</returns>
		// Token: 0x06002705 RID: 9989 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual IPInterfaceStatistics GetIPStatistics()
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the current operational state of the network connection.</summary>
		/// <returns>One of the <see cref="T:System.Net.NetworkInformation.OperationalStatus" /> values.</returns>
		// Token: 0x170008CA RID: 2250
		// (get) Token: 0x06002706 RID: 9990 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual OperationalStatus OperationalStatus
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the speed of the network interface.</summary>
		/// <returns>A <see cref="T:System.Int64" /> value that specifies the speed in bits per second.</returns>
		// Token: 0x170008CB RID: 2251
		// (get) Token: 0x06002707 RID: 9991 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual long Speed
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the network interface is set to only receive data packets.</summary>
		/// <returns>
		///     <see langword="true" /> if the interface only receives network traffic; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.PlatformNotSupportedException">This property is not valid on computers running operating systems earlier than Windows XP. </exception>
		// Token: 0x170008CC RID: 2252
		// (get) Token: 0x06002708 RID: 9992 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual bool IsReceiveOnly
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the network interface is enabled to receive multicast packets.</summary>
		/// <returns>
		///     <see langword="true" /> if the interface receives multicast packets; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.PlatformNotSupportedException">This property is not valid on computers running operating systems earlier than Windows XP. </exception>
		// Token: 0x170008CD RID: 2253
		// (get) Token: 0x06002709 RID: 9993 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual bool SupportsMulticast
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Returns the Media Access Control (MAC) or physical address for this adapter.</summary>
		/// <returns>A <see cref="T:System.Net.NetworkInformation.PhysicalAddress" /> object that contains the physical address.</returns>
		// Token: 0x0600270A RID: 9994 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual PhysicalAddress GetPhysicalAddress()
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the interface type.</summary>
		/// <returns>An <see cref="T:System.Net.NetworkInformation.NetworkInterfaceType" /> value that specifies the network interface type.</returns>
		// Token: 0x170008CE RID: 2254
		// (get) Token: 0x0600270B RID: 9995 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual NetworkInterfaceType NetworkInterfaceType
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the interface supports the specified protocol.</summary>
		/// <param name="networkInterfaceComponent">A <see cref="T:System.Net.NetworkInformation.NetworkInterfaceComponent" /> value.</param>
		/// <returns>
		///     <see langword="true" /> if the specified protocol is supported; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600270C RID: 9996 RVA: 0x000068D7 File Offset: 0x00004AD7
		public virtual bool Supports(NetworkInterfaceComponent networkInterfaceComponent)
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkInformation.NetworkInterface" /> class.</summary>
		// Token: 0x0600270D RID: 9997 RVA: 0x0000232F File Offset: 0x0000052F
		protected NetworkInterface()
		{
		}
	}
}
