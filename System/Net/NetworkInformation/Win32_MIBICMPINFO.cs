﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004E3 RID: 1251
	internal struct Win32_MIBICMPINFO
	{
		// Token: 0x04002006 RID: 8198
		public Win32_MIBICMPSTATS InStats;

		// Token: 0x04002007 RID: 8199
		public Win32_MIBICMPSTATS OutStats;
	}
}
