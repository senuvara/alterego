﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000522 RID: 1314
	internal struct Win32_IP_ADAPTER_GATEWAY_ADDRESS
	{
		// Token: 0x0400216B RID: 8555
		public Win32LengthFlagsUnion LengthFlags;

		// Token: 0x0400216C RID: 8556
		public IntPtr Next;

		// Token: 0x0400216D RID: 8557
		public Win32_SOCKET_ADDRESS Address;
	}
}
