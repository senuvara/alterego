﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Specifies the Network Basic Input/Output System (NetBIOS) node type.</summary>
	// Token: 0x020004C5 RID: 1221
	public enum NetBiosNodeType
	{
		/// <summary>An unknown node type.</summary>
		// Token: 0x04001FB1 RID: 8113
		Unknown,
		/// <summary>A broadcast node.</summary>
		// Token: 0x04001FB2 RID: 8114
		Broadcast,
		/// <summary>A peer-to-peer node.</summary>
		// Token: 0x04001FB3 RID: 8115
		Peer2Peer,
		/// <summary>A mixed node.</summary>
		// Token: 0x04001FB4 RID: 8116
		Mixed = 4,
		/// <summary>A hybrid node.</summary>
		// Token: 0x04001FB5 RID: 8117
		Hybrid = 8
	}
}
