﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004D4 RID: 1236
	internal abstract class UnixIPInterfaceProperties : IPInterfaceProperties
	{
		// Token: 0x060027F6 RID: 10230 RVA: 0x0008DA1C File Offset: 0x0008BC1C
		public UnixIPInterfaceProperties(UnixNetworkInterface iface, List<IPAddress> addresses)
		{
			this.iface = iface;
			this.addresses = addresses;
		}

		// Token: 0x060027F7 RID: 10231 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override IPv6InterfaceProperties GetIPv6Properties()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060027F8 RID: 10232 RVA: 0x0008DA34 File Offset: 0x0008BC34
		private void ParseResolvConf()
		{
			try
			{
				DateTime lastWriteTime = File.GetLastWriteTime("/etc/resolv.conf");
				if (!(lastWriteTime <= this.last_parse))
				{
					this.last_parse = lastWriteTime;
					this.dns_suffix = "";
					this.dns_servers = new IPAddressCollection();
					using (StreamReader streamReader = new StreamReader("/etc/resolv.conf"))
					{
						string text;
						while ((text = streamReader.ReadLine()) != null)
						{
							text = text.Trim();
							if (text.Length != 0 && text[0] != '#')
							{
								Match match = UnixIPInterfaceProperties.ns.Match(text);
								if (match.Success)
								{
									try
									{
										string text2 = match.Groups["address"].Value;
										text2 = text2.Trim();
										this.dns_servers.InternalAdd(IPAddress.Parse(text2));
										continue;
									}
									catch
									{
										continue;
									}
								}
								match = UnixIPInterfaceProperties.search.Match(text);
								if (match.Success)
								{
									string text2 = match.Groups["domain"].Value;
									string[] array = text2.Split(new char[]
									{
										','
									});
									this.dns_suffix = array[0].Trim();
								}
							}
						}
					}
				}
			}
			catch
			{
			}
		}

		// Token: 0x17000945 RID: 2373
		// (get) Token: 0x060027F9 RID: 10233 RVA: 0x0008DBB0 File Offset: 0x0008BDB0
		public override IPAddressInformationCollection AnycastAddresses
		{
			get
			{
				IPAddressInformationCollection ipaddressInformationCollection = new IPAddressInformationCollection();
				foreach (IPAddress address in this.addresses)
				{
					ipaddressInformationCollection.InternalAdd(new SystemIPAddressInformation(address, false, false));
				}
				return ipaddressInformationCollection;
			}
		}

		// Token: 0x17000946 RID: 2374
		// (get) Token: 0x060027FA RID: 10234 RVA: 0x0008DC14 File Offset: 0x0008BE14
		[MonoTODO("Always returns an empty collection.")]
		public override IPAddressCollection DhcpServerAddresses
		{
			get
			{
				return new IPAddressCollection();
			}
		}

		// Token: 0x17000947 RID: 2375
		// (get) Token: 0x060027FB RID: 10235 RVA: 0x0008DC1B File Offset: 0x0008BE1B
		public override IPAddressCollection DnsAddresses
		{
			get
			{
				this.ParseResolvConf();
				return this.dns_servers;
			}
		}

		// Token: 0x17000948 RID: 2376
		// (get) Token: 0x060027FC RID: 10236 RVA: 0x0008DC29 File Offset: 0x0008BE29
		public override string DnsSuffix
		{
			get
			{
				this.ParseResolvConf();
				return this.dns_suffix;
			}
		}

		// Token: 0x17000949 RID: 2377
		// (get) Token: 0x060027FD RID: 10237 RVA: 0x00003298 File Offset: 0x00001498
		[MonoTODO("Always returns true")]
		public override bool IsDnsEnabled
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700094A RID: 2378
		// (get) Token: 0x060027FE RID: 10238 RVA: 0x00005AFA File Offset: 0x00003CFA
		[MonoTODO("Always returns false")]
		public override bool IsDynamicDnsEnabled
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700094B RID: 2379
		// (get) Token: 0x060027FF RID: 10239 RVA: 0x0008DC38 File Offset: 0x0008BE38
		public override MulticastIPAddressInformationCollection MulticastAddresses
		{
			get
			{
				MulticastIPAddressInformationCollection multicastIPAddressInformationCollection = new MulticastIPAddressInformationCollection();
				foreach (IPAddress ipaddress in this.addresses)
				{
					byte[] addressBytes = ipaddress.GetAddressBytes();
					if (addressBytes[0] >= 224 && addressBytes[0] <= 239)
					{
						multicastIPAddressInformationCollection.InternalAdd(new SystemMulticastIPAddressInformation(new SystemIPAddressInformation(ipaddress, true, false)));
					}
				}
				return multicastIPAddressInformationCollection;
			}
		}

		// Token: 0x1700094C RID: 2380
		// (get) Token: 0x06002800 RID: 10240 RVA: 0x0008DCBC File Offset: 0x0008BEBC
		public override UnicastIPAddressInformationCollection UnicastAddresses
		{
			get
			{
				UnicastIPAddressInformationCollection unicastIPAddressInformationCollection = new UnicastIPAddressInformationCollection();
				foreach (IPAddress ipaddress in this.addresses)
				{
					AddressFamily addressFamily = ipaddress.AddressFamily;
					if (addressFamily != AddressFamily.InterNetwork)
					{
						if (addressFamily == AddressFamily.InterNetworkV6)
						{
							if (!ipaddress.IsIPv6Multicast)
							{
								unicastIPAddressInformationCollection.InternalAdd(new LinuxUnicastIPAddressInformation(ipaddress));
							}
						}
					}
					else
					{
						byte b = ipaddress.GetAddressBytes()[0];
						if (b < 224 || b > 239)
						{
							unicastIPAddressInformationCollection.InternalAdd(new LinuxUnicastIPAddressInformation(ipaddress));
						}
					}
				}
				return unicastIPAddressInformationCollection;
			}
		}

		// Token: 0x1700094D RID: 2381
		// (get) Token: 0x06002801 RID: 10241 RVA: 0x0008DC14 File Offset: 0x0008BE14
		[MonoTODO("Always returns an empty collection.")]
		public override IPAddressCollection WinsServersAddresses
		{
			get
			{
				return new IPAddressCollection();
			}
		}

		// Token: 0x06002802 RID: 10242 RVA: 0x0008DD60 File Offset: 0x0008BF60
		// Note: this type is marked as 'beforefieldinit'.
		static UnixIPInterfaceProperties()
		{
		}

		// Token: 0x04001FED RID: 8173
		protected IPv4InterfaceProperties ipv4iface_properties;

		// Token: 0x04001FEE RID: 8174
		protected UnixNetworkInterface iface;

		// Token: 0x04001FEF RID: 8175
		private List<IPAddress> addresses;

		// Token: 0x04001FF0 RID: 8176
		private IPAddressCollection dns_servers;

		// Token: 0x04001FF1 RID: 8177
		private static Regex ns = new Regex("\\s*nameserver\\s+(?<address>.*)");

		// Token: 0x04001FF2 RID: 8178
		private static Regex search = new Regex("\\s*search\\s+(?<domain>.*)");

		// Token: 0x04001FF3 RID: 8179
		private string dns_suffix;

		// Token: 0x04001FF4 RID: 8180
		private DateTime last_parse;
	}
}
