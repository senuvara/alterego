﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000510 RID: 1296
	internal class Win32TcpStatistics : TcpStatistics
	{
		// Token: 0x060029B5 RID: 10677 RVA: 0x0009176A File Offset: 0x0008F96A
		public Win32TcpStatistics(Win32_MIB_TCPSTATS info)
		{
			this.info = info;
		}

		// Token: 0x17000A35 RID: 2613
		// (get) Token: 0x060029B6 RID: 10678 RVA: 0x00091779 File Offset: 0x0008F979
		public override long ConnectionsAccepted
		{
			get
			{
				return (long)((ulong)this.info.PassiveOpens);
			}
		}

		// Token: 0x17000A36 RID: 2614
		// (get) Token: 0x060029B7 RID: 10679 RVA: 0x00091787 File Offset: 0x0008F987
		public override long ConnectionsInitiated
		{
			get
			{
				return (long)((ulong)this.info.ActiveOpens);
			}
		}

		// Token: 0x17000A37 RID: 2615
		// (get) Token: 0x060029B8 RID: 10680 RVA: 0x00091795 File Offset: 0x0008F995
		public override long CumulativeConnections
		{
			get
			{
				return (long)((ulong)this.info.NumConns);
			}
		}

		// Token: 0x17000A38 RID: 2616
		// (get) Token: 0x060029B9 RID: 10681 RVA: 0x000917A3 File Offset: 0x0008F9A3
		public override long CurrentConnections
		{
			get
			{
				return (long)((ulong)this.info.CurrEstab);
			}
		}

		// Token: 0x17000A39 RID: 2617
		// (get) Token: 0x060029BA RID: 10682 RVA: 0x000917B1 File Offset: 0x0008F9B1
		public override long ErrorsReceived
		{
			get
			{
				return (long)((ulong)this.info.InErrs);
			}
		}

		// Token: 0x17000A3A RID: 2618
		// (get) Token: 0x060029BB RID: 10683 RVA: 0x000917BF File Offset: 0x0008F9BF
		public override long FailedConnectionAttempts
		{
			get
			{
				return (long)((ulong)this.info.AttemptFails);
			}
		}

		// Token: 0x17000A3B RID: 2619
		// (get) Token: 0x060029BC RID: 10684 RVA: 0x000917CD File Offset: 0x0008F9CD
		public override long MaximumConnections
		{
			get
			{
				return (long)((ulong)this.info.MaxConn);
			}
		}

		// Token: 0x17000A3C RID: 2620
		// (get) Token: 0x060029BD RID: 10685 RVA: 0x000917DB File Offset: 0x0008F9DB
		public override long MaximumTransmissionTimeout
		{
			get
			{
				return (long)((ulong)this.info.RtoMax);
			}
		}

		// Token: 0x17000A3D RID: 2621
		// (get) Token: 0x060029BE RID: 10686 RVA: 0x000917E9 File Offset: 0x0008F9E9
		public override long MinimumTransmissionTimeout
		{
			get
			{
				return (long)((ulong)this.info.RtoMin);
			}
		}

		// Token: 0x17000A3E RID: 2622
		// (get) Token: 0x060029BF RID: 10687 RVA: 0x000917F7 File Offset: 0x0008F9F7
		public override long ResetConnections
		{
			get
			{
				return (long)((ulong)this.info.EstabResets);
			}
		}

		// Token: 0x17000A3F RID: 2623
		// (get) Token: 0x060029C0 RID: 10688 RVA: 0x00091805 File Offset: 0x0008FA05
		public override long ResetsSent
		{
			get
			{
				return (long)((ulong)this.info.OutRsts);
			}
		}

		// Token: 0x17000A40 RID: 2624
		// (get) Token: 0x060029C1 RID: 10689 RVA: 0x00091813 File Offset: 0x0008FA13
		public override long SegmentsReceived
		{
			get
			{
				return (long)((ulong)this.info.InSegs);
			}
		}

		// Token: 0x17000A41 RID: 2625
		// (get) Token: 0x060029C2 RID: 10690 RVA: 0x00091821 File Offset: 0x0008FA21
		public override long SegmentsResent
		{
			get
			{
				return (long)((ulong)this.info.RetransSegs);
			}
		}

		// Token: 0x17000A42 RID: 2626
		// (get) Token: 0x060029C3 RID: 10691 RVA: 0x0009182F File Offset: 0x0008FA2F
		public override long SegmentsSent
		{
			get
			{
				return (long)((ulong)this.info.OutSegs);
			}
		}

		// Token: 0x040020D2 RID: 8402
		private Win32_MIB_TCPSTATS info;
	}
}
