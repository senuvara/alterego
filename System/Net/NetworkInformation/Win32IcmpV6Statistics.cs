﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004E7 RID: 1255
	internal class Win32IcmpV6Statistics : IcmpV6Statistics
	{
		// Token: 0x060028B6 RID: 10422 RVA: 0x0008EADC File Offset: 0x0008CCDC
		public Win32IcmpV6Statistics(Win32_MIB_ICMP_EX info)
		{
			this.iin = info.InStats;
			this.iout = info.OutStats;
		}

		// Token: 0x170009E4 RID: 2532
		// (get) Token: 0x060028B7 RID: 10423 RVA: 0x0008EAFC File Offset: 0x0008CCFC
		public override long DestinationUnreachableMessagesReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[1]);
			}
		}

		// Token: 0x170009E5 RID: 2533
		// (get) Token: 0x060028B8 RID: 10424 RVA: 0x0008EB0C File Offset: 0x0008CD0C
		public override long DestinationUnreachableMessagesSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[1]);
			}
		}

		// Token: 0x170009E6 RID: 2534
		// (get) Token: 0x060028B9 RID: 10425 RVA: 0x0008EB1C File Offset: 0x0008CD1C
		public override long EchoRepliesReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[129]);
			}
		}

		// Token: 0x170009E7 RID: 2535
		// (get) Token: 0x060028BA RID: 10426 RVA: 0x0008EB30 File Offset: 0x0008CD30
		public override long EchoRepliesSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[129]);
			}
		}

		// Token: 0x170009E8 RID: 2536
		// (get) Token: 0x060028BB RID: 10427 RVA: 0x0008EB44 File Offset: 0x0008CD44
		public override long EchoRequestsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[128]);
			}
		}

		// Token: 0x170009E9 RID: 2537
		// (get) Token: 0x060028BC RID: 10428 RVA: 0x0008EB58 File Offset: 0x0008CD58
		public override long EchoRequestsSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[128]);
			}
		}

		// Token: 0x170009EA RID: 2538
		// (get) Token: 0x060028BD RID: 10429 RVA: 0x0008EB6C File Offset: 0x0008CD6C
		public override long ErrorsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Errors);
			}
		}

		// Token: 0x170009EB RID: 2539
		// (get) Token: 0x060028BE RID: 10430 RVA: 0x0008EB7A File Offset: 0x0008CD7A
		public override long ErrorsSent
		{
			get
			{
				return (long)((ulong)this.iout.Errors);
			}
		}

		// Token: 0x170009EC RID: 2540
		// (get) Token: 0x060028BF RID: 10431 RVA: 0x0008EB88 File Offset: 0x0008CD88
		public override long MembershipQueriesReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[130]);
			}
		}

		// Token: 0x170009ED RID: 2541
		// (get) Token: 0x060028C0 RID: 10432 RVA: 0x0008EB9C File Offset: 0x0008CD9C
		public override long MembershipQueriesSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[130]);
			}
		}

		// Token: 0x170009EE RID: 2542
		// (get) Token: 0x060028C1 RID: 10433 RVA: 0x0008EBB0 File Offset: 0x0008CDB0
		public override long MembershipReductionsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[132]);
			}
		}

		// Token: 0x170009EF RID: 2543
		// (get) Token: 0x060028C2 RID: 10434 RVA: 0x0008EBC4 File Offset: 0x0008CDC4
		public override long MembershipReductionsSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[132]);
			}
		}

		// Token: 0x170009F0 RID: 2544
		// (get) Token: 0x060028C3 RID: 10435 RVA: 0x0008EBD8 File Offset: 0x0008CDD8
		public override long MembershipReportsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[131]);
			}
		}

		// Token: 0x170009F1 RID: 2545
		// (get) Token: 0x060028C4 RID: 10436 RVA: 0x0008EBEC File Offset: 0x0008CDEC
		public override long MembershipReportsSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[131]);
			}
		}

		// Token: 0x170009F2 RID: 2546
		// (get) Token: 0x060028C5 RID: 10437 RVA: 0x0008EC00 File Offset: 0x0008CE00
		public override long MessagesReceived
		{
			get
			{
				return (long)((ulong)this.iin.Msgs);
			}
		}

		// Token: 0x170009F3 RID: 2547
		// (get) Token: 0x060028C6 RID: 10438 RVA: 0x0008EC0E File Offset: 0x0008CE0E
		public override long MessagesSent
		{
			get
			{
				return (long)((ulong)this.iout.Msgs);
			}
		}

		// Token: 0x170009F4 RID: 2548
		// (get) Token: 0x060028C7 RID: 10439 RVA: 0x0008EC1C File Offset: 0x0008CE1C
		public override long NeighborAdvertisementsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[136]);
			}
		}

		// Token: 0x170009F5 RID: 2549
		// (get) Token: 0x060028C8 RID: 10440 RVA: 0x0008EC30 File Offset: 0x0008CE30
		public override long NeighborAdvertisementsSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[136]);
			}
		}

		// Token: 0x170009F6 RID: 2550
		// (get) Token: 0x060028C9 RID: 10441 RVA: 0x0008EC44 File Offset: 0x0008CE44
		public override long NeighborSolicitsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[135]);
			}
		}

		// Token: 0x170009F7 RID: 2551
		// (get) Token: 0x060028CA RID: 10442 RVA: 0x0008EC58 File Offset: 0x0008CE58
		public override long NeighborSolicitsSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[135]);
			}
		}

		// Token: 0x170009F8 RID: 2552
		// (get) Token: 0x060028CB RID: 10443 RVA: 0x0008EC6C File Offset: 0x0008CE6C
		public override long PacketTooBigMessagesReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[2]);
			}
		}

		// Token: 0x170009F9 RID: 2553
		// (get) Token: 0x060028CC RID: 10444 RVA: 0x0008EC7C File Offset: 0x0008CE7C
		public override long PacketTooBigMessagesSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[2]);
			}
		}

		// Token: 0x170009FA RID: 2554
		// (get) Token: 0x060028CD RID: 10445 RVA: 0x0008EC8C File Offset: 0x0008CE8C
		public override long ParameterProblemsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[4]);
			}
		}

		// Token: 0x170009FB RID: 2555
		// (get) Token: 0x060028CE RID: 10446 RVA: 0x0008EC9C File Offset: 0x0008CE9C
		public override long ParameterProblemsSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[4]);
			}
		}

		// Token: 0x170009FC RID: 2556
		// (get) Token: 0x060028CF RID: 10447 RVA: 0x0008ECAC File Offset: 0x0008CEAC
		public override long RedirectsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[137]);
			}
		}

		// Token: 0x170009FD RID: 2557
		// (get) Token: 0x060028D0 RID: 10448 RVA: 0x0008ECC0 File Offset: 0x0008CEC0
		public override long RedirectsSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[137]);
			}
		}

		// Token: 0x170009FE RID: 2558
		// (get) Token: 0x060028D1 RID: 10449 RVA: 0x0008ECD4 File Offset: 0x0008CED4
		public override long RouterAdvertisementsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[134]);
			}
		}

		// Token: 0x170009FF RID: 2559
		// (get) Token: 0x060028D2 RID: 10450 RVA: 0x0008ECE8 File Offset: 0x0008CEE8
		public override long RouterAdvertisementsSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[134]);
			}
		}

		// Token: 0x17000A00 RID: 2560
		// (get) Token: 0x060028D3 RID: 10451 RVA: 0x0008ECFC File Offset: 0x0008CEFC
		public override long RouterSolicitsReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[133]);
			}
		}

		// Token: 0x17000A01 RID: 2561
		// (get) Token: 0x060028D4 RID: 10452 RVA: 0x0008ED10 File Offset: 0x0008CF10
		public override long RouterSolicitsSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[133]);
			}
		}

		// Token: 0x17000A02 RID: 2562
		// (get) Token: 0x060028D5 RID: 10453 RVA: 0x0008ED24 File Offset: 0x0008CF24
		public override long TimeExceededMessagesReceived
		{
			get
			{
				return (long)((ulong)this.iin.Counts[3]);
			}
		}

		// Token: 0x17000A03 RID: 2563
		// (get) Token: 0x060028D6 RID: 10454 RVA: 0x0008ED34 File Offset: 0x0008CF34
		public override long TimeExceededMessagesSent
		{
			get
			{
				return (long)((ulong)this.iout.Counts[3]);
			}
		}

		// Token: 0x04002025 RID: 8229
		private Win32_MIBICMPSTATS_EX iin;

		// Token: 0x04002026 RID: 8230
		private Win32_MIBICMPSTATS_EX iout;
	}
}
