﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004DB RID: 1243
	internal sealed class Win32IPv4InterfaceProperties : IPv4InterfaceProperties
	{
		// Token: 0x06002827 RID: 10279
		[DllImport("iphlpapi.dll")]
		private static extern int GetPerAdapterInfo(int IfIndex, Win32_IP_PER_ADAPTER_INFO pPerAdapterInfo, ref int pOutBufLen);

		// Token: 0x06002828 RID: 10280 RVA: 0x0008E36C File Offset: 0x0008C56C
		public Win32IPv4InterfaceProperties(Win32_IP_ADAPTER_ADDRESSES addr, Win32_MIB_IFROW mib)
		{
			this.addr = addr;
			this.mib = mib;
			int num = 0;
			Win32IPv4InterfaceProperties.GetPerAdapterInfo(mib.Index, null, ref num);
			this.painfo = new Win32_IP_PER_ADAPTER_INFO();
			int perAdapterInfo = Win32IPv4InterfaceProperties.GetPerAdapterInfo(mib.Index, this.painfo, ref num);
			if (perAdapterInfo != 0)
			{
				throw new NetworkInformationException(perAdapterInfo);
			}
		}

		// Token: 0x17000963 RID: 2403
		// (get) Token: 0x06002829 RID: 10281 RVA: 0x0008E3C7 File Offset: 0x0008C5C7
		public override int Index
		{
			get
			{
				return this.mib.Index;
			}
		}

		// Token: 0x17000964 RID: 2404
		// (get) Token: 0x0600282A RID: 10282 RVA: 0x0008E3D4 File Offset: 0x0008C5D4
		public override bool IsAutomaticPrivateAddressingActive
		{
			get
			{
				return this.painfo.AutoconfigActive > 0U;
			}
		}

		// Token: 0x17000965 RID: 2405
		// (get) Token: 0x0600282B RID: 10283 RVA: 0x0008E3E4 File Offset: 0x0008C5E4
		public override bool IsAutomaticPrivateAddressingEnabled
		{
			get
			{
				return this.painfo.AutoconfigEnabled > 0U;
			}
		}

		// Token: 0x17000966 RID: 2406
		// (get) Token: 0x0600282C RID: 10284 RVA: 0x0008E3F4 File Offset: 0x0008C5F4
		public override bool IsDhcpEnabled
		{
			get
			{
				return this.addr.DhcpEnabled;
			}
		}

		// Token: 0x17000967 RID: 2407
		// (get) Token: 0x0600282D RID: 10285 RVA: 0x0008E401 File Offset: 0x0008C601
		public override bool IsForwardingEnabled
		{
			get
			{
				return Win32NetworkInterface.FixedInfo.EnableRouting > 0U;
			}
		}

		// Token: 0x17000968 RID: 2408
		// (get) Token: 0x0600282E RID: 10286 RVA: 0x0008E410 File Offset: 0x0008C610
		public override int Mtu
		{
			get
			{
				return this.mib.Mtu;
			}
		}

		// Token: 0x17000969 RID: 2409
		// (get) Token: 0x0600282F RID: 10287 RVA: 0x0008E41D File Offset: 0x0008C61D
		public override bool UsesWins
		{
			get
			{
				return this.addr.FirstWinsServerAddress != IntPtr.Zero;
			}
		}

		// Token: 0x04001FF9 RID: 8185
		private Win32_IP_ADAPTER_ADDRESSES addr;

		// Token: 0x04001FFA RID: 8186
		private Win32_IP_PER_ADAPTER_INFO painfo;

		// Token: 0x04001FFB RID: 8187
		private Win32_MIB_IFROW mib;
	}
}
