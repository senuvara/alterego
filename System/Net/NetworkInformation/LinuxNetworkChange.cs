﻿using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004FA RID: 1274
	internal sealed class LinuxNetworkChange : INetworkChange, IDisposable
	{
		// Token: 0x14000047 RID: 71
		// (add) Token: 0x06002901 RID: 10497 RVA: 0x0008F2E3 File Offset: 0x0008D4E3
		// (remove) Token: 0x06002902 RID: 10498 RVA: 0x0008F2EC File Offset: 0x0008D4EC
		public event NetworkAddressChangedEventHandler NetworkAddressChanged
		{
			add
			{
				this.Register(value);
			}
			remove
			{
				this.Unregister(value);
			}
		}

		// Token: 0x14000048 RID: 72
		// (add) Token: 0x06002903 RID: 10499 RVA: 0x0008F2F5 File Offset: 0x0008D4F5
		// (remove) Token: 0x06002904 RID: 10500 RVA: 0x0008F2FE File Offset: 0x0008D4FE
		public event NetworkAvailabilityChangedEventHandler NetworkAvailabilityChanged
		{
			add
			{
				this.Register(value);
			}
			remove
			{
				this.Unregister(value);
			}
		}

		// Token: 0x17000A07 RID: 2567
		// (get) Token: 0x06002905 RID: 10501 RVA: 0x0008F307 File Offset: 0x0008D507
		public bool HasRegisteredEvents
		{
			get
			{
				return this.AddressChanged != null || this.AvailabilityChanged != null;
			}
		}

		// Token: 0x06002906 RID: 10502 RVA: 0x0000232D File Offset: 0x0000052D
		public void Dispose()
		{
		}

		// Token: 0x06002907 RID: 10503 RVA: 0x0008F31C File Offset: 0x0008D51C
		private bool EnsureSocket()
		{
			object @lock = this._lock;
			lock (@lock)
			{
				if (this.nl_sock != null)
				{
					return true;
				}
				IntPtr preexistingHandle = LinuxNetworkChange.CreateNLSocket();
				if (preexistingHandle.ToInt64() == -1L)
				{
					return false;
				}
				SafeSocketHandle safe_handle = new SafeSocketHandle(preexistingHandle, true);
				this.nl_sock = new Socket(AddressFamily.Unspecified, SocketType.Raw, ProtocolType.Udp, safe_handle);
				this.nl_args = new SocketAsyncEventArgs();
				this.nl_args.SetBuffer(new byte[8192], 0, 8192);
				this.nl_args.Completed += this.OnDataAvailable;
				this.nl_sock.ReceiveAsync(this.nl_args);
			}
			return true;
		}

		// Token: 0x06002908 RID: 10504 RVA: 0x0008F3E8 File Offset: 0x0008D5E8
		private void MaybeCloseSocket()
		{
			if (this.nl_sock == null || this.AvailabilityChanged != null || this.AddressChanged != null)
			{
				return;
			}
			LinuxNetworkChange.CloseNLSocket(this.nl_sock.Handle);
			GC.SuppressFinalize(this.nl_sock);
			this.nl_sock = null;
			this.nl_args = null;
		}

		// Token: 0x06002909 RID: 10505 RVA: 0x0008F438 File Offset: 0x0008D638
		private bool GetAvailability()
		{
			foreach (NetworkInterface networkInterface in NetworkInterface.GetAllNetworkInterfaces())
			{
				if (networkInterface.NetworkInterfaceType != NetworkInterfaceType.Loopback && networkInterface.OperationalStatus == OperationalStatus.Up)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600290A RID: 10506 RVA: 0x0008F474 File Offset: 0x0008D674
		private void OnAvailabilityChanged(object unused)
		{
			NetworkAvailabilityChangedEventHandler availabilityChanged = this.AvailabilityChanged;
			if (availabilityChanged != null)
			{
				availabilityChanged(null, new NetworkAvailabilityEventArgs(this.GetAvailability()));
			}
		}

		// Token: 0x0600290B RID: 10507 RVA: 0x0008F4A0 File Offset: 0x0008D6A0
		private void OnAddressChanged(object unused)
		{
			NetworkAddressChangedEventHandler addressChanged = this.AddressChanged;
			if (addressChanged != null)
			{
				addressChanged(null, EventArgs.Empty);
			}
		}

		// Token: 0x0600290C RID: 10508 RVA: 0x0008F4C4 File Offset: 0x0008D6C4
		private void OnEventDue(object unused)
		{
			object @lock = this._lock;
			LinuxNetworkChange.EventType eventType;
			lock (@lock)
			{
				eventType = this.pending_events;
				this.pending_events = (LinuxNetworkChange.EventType)0;
				this.timer.Change(-1, -1);
			}
			if ((eventType & LinuxNetworkChange.EventType.Availability) != (LinuxNetworkChange.EventType)0)
			{
				ThreadPool.QueueUserWorkItem(new WaitCallback(this.OnAvailabilityChanged));
			}
			if ((eventType & LinuxNetworkChange.EventType.Address) != (LinuxNetworkChange.EventType)0)
			{
				ThreadPool.QueueUserWorkItem(new WaitCallback(this.OnAddressChanged));
			}
		}

		// Token: 0x0600290D RID: 10509 RVA: 0x0008F548 File Offset: 0x0008D748
		private void QueueEvent(LinuxNetworkChange.EventType type)
		{
			object @lock = this._lock;
			lock (@lock)
			{
				if (this.timer == null)
				{
					this.timer = new Timer(new TimerCallback(this.OnEventDue));
				}
				if (this.pending_events == (LinuxNetworkChange.EventType)0)
				{
					this.timer.Change(150, -1);
				}
				this.pending_events |= type;
			}
		}

		// Token: 0x0600290E RID: 10510 RVA: 0x0008F5CC File Offset: 0x0008D7CC
		private unsafe void OnDataAvailable(object sender, SocketAsyncEventArgs args)
		{
			if (this.nl_sock == null)
			{
				return;
			}
			byte[] array;
			byte* value;
			if ((array = args.Buffer) == null || array.Length == 0)
			{
				value = null;
			}
			else
			{
				value = &array[0];
			}
			LinuxNetworkChange.EventType eventType = LinuxNetworkChange.ReadEvents(this.nl_sock.Handle, new IntPtr((void*)value), args.BytesTransferred, 8192);
			array = null;
			this.nl_sock.ReceiveAsync(this.nl_args);
			if (eventType != (LinuxNetworkChange.EventType)0)
			{
				this.QueueEvent(eventType);
			}
		}

		// Token: 0x0600290F RID: 10511 RVA: 0x0008F63F File Offset: 0x0008D83F
		private void Register(NetworkAddressChangedEventHandler d)
		{
			this.EnsureSocket();
			this.AddressChanged = (NetworkAddressChangedEventHandler)Delegate.Combine(this.AddressChanged, d);
		}

		// Token: 0x06002910 RID: 10512 RVA: 0x0008F65F File Offset: 0x0008D85F
		private void Register(NetworkAvailabilityChangedEventHandler d)
		{
			this.EnsureSocket();
			this.AvailabilityChanged = (NetworkAvailabilityChangedEventHandler)Delegate.Combine(this.AvailabilityChanged, d);
		}

		// Token: 0x06002911 RID: 10513 RVA: 0x0008F680 File Offset: 0x0008D880
		private void Unregister(NetworkAddressChangedEventHandler d)
		{
			object @lock = this._lock;
			lock (@lock)
			{
				this.AddressChanged = (NetworkAddressChangedEventHandler)Delegate.Remove(this.AddressChanged, d);
				this.MaybeCloseSocket();
			}
		}

		// Token: 0x06002912 RID: 10514 RVA: 0x0008F6D8 File Offset: 0x0008D8D8
		private void Unregister(NetworkAvailabilityChangedEventHandler d)
		{
			object @lock = this._lock;
			lock (@lock)
			{
				this.AvailabilityChanged = (NetworkAvailabilityChangedEventHandler)Delegate.Remove(this.AvailabilityChanged, d);
				this.MaybeCloseSocket();
			}
		}

		// Token: 0x06002913 RID: 10515
		[DllImport("MonoPosixHelper", CallingConvention = CallingConvention.Cdecl)]
		private static extern IntPtr CreateNLSocket();

		// Token: 0x06002914 RID: 10516
		[DllImport("MonoPosixHelper", CallingConvention = CallingConvention.Cdecl)]
		private static extern LinuxNetworkChange.EventType ReadEvents(IntPtr sock, IntPtr buffer, int count, int size);

		// Token: 0x06002915 RID: 10517
		[DllImport("MonoPosixHelper", CallingConvention = CallingConvention.Cdecl)]
		private static extern IntPtr CloseNLSocket(IntPtr sock);

		// Token: 0x06002916 RID: 10518 RVA: 0x0008F730 File Offset: 0x0008D930
		public LinuxNetworkChange()
		{
		}

		// Token: 0x0400208D RID: 8333
		private object _lock = new object();

		// Token: 0x0400208E RID: 8334
		private Socket nl_sock;

		// Token: 0x0400208F RID: 8335
		private SocketAsyncEventArgs nl_args;

		// Token: 0x04002090 RID: 8336
		private LinuxNetworkChange.EventType pending_events;

		// Token: 0x04002091 RID: 8337
		private Timer timer;

		// Token: 0x04002092 RID: 8338
		private NetworkAddressChangedEventHandler AddressChanged;

		// Token: 0x04002093 RID: 8339
		private NetworkAvailabilityChangedEventHandler AvailabilityChanged;

		// Token: 0x04002094 RID: 8340
		private const string LIBNAME = "MonoPosixHelper";

		// Token: 0x020004FB RID: 1275
		[Flags]
		private enum EventType
		{
			// Token: 0x04002096 RID: 8342
			Availability = 1,
			// Token: 0x04002097 RID: 8343
			Address = 2
		}
	}
}
