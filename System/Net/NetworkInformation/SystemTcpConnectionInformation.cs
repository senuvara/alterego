﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004BD RID: 1213
	internal class SystemTcpConnectionInformation : TcpConnectionInformation
	{
		// Token: 0x06002738 RID: 10040 RVA: 0x0008C7E4 File Offset: 0x0008A9E4
		public SystemTcpConnectionInformation(IPEndPoint local, IPEndPoint remote, TcpState state)
		{
			this.localEndPoint = local;
			this.remoteEndPoint = remote;
			this.state = state;
		}

		// Token: 0x170008E3 RID: 2275
		// (get) Token: 0x06002739 RID: 10041 RVA: 0x0008C801 File Offset: 0x0008AA01
		public override TcpState State
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x170008E4 RID: 2276
		// (get) Token: 0x0600273A RID: 10042 RVA: 0x0008C809 File Offset: 0x0008AA09
		public override IPEndPoint LocalEndPoint
		{
			get
			{
				return this.localEndPoint;
			}
		}

		// Token: 0x170008E5 RID: 2277
		// (get) Token: 0x0600273B RID: 10043 RVA: 0x0008C811 File Offset: 0x0008AA11
		public override IPEndPoint RemoteEndPoint
		{
			get
			{
				return this.remoteEndPoint;
			}
		}

		// Token: 0x04001F81 RID: 8065
		private IPEndPoint localEndPoint;

		// Token: 0x04001F82 RID: 8066
		private IPEndPoint remoteEndPoint;

		// Token: 0x04001F83 RID: 8067
		private TcpState state;
	}
}
