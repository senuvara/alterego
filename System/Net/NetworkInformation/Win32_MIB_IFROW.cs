﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200051C RID: 1308
	internal struct Win32_MIB_IFROW
	{
		// Token: 0x0400213F RID: 8511
		private const int MAX_INTERFACE_NAME_LEN = 256;

		// Token: 0x04002140 RID: 8512
		private const int MAXLEN_PHYSADDR = 8;

		// Token: 0x04002141 RID: 8513
		private const int MAXLEN_IFDESCR = 256;

		// Token: 0x04002142 RID: 8514
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] Name;

		// Token: 0x04002143 RID: 8515
		public int Index;

		// Token: 0x04002144 RID: 8516
		public NetworkInterfaceType Type;

		// Token: 0x04002145 RID: 8517
		public int Mtu;

		// Token: 0x04002146 RID: 8518
		public uint Speed;

		// Token: 0x04002147 RID: 8519
		public int PhysAddrLen;

		// Token: 0x04002148 RID: 8520
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public byte[] PhysAddr;

		// Token: 0x04002149 RID: 8521
		public uint AdminStatus;

		// Token: 0x0400214A RID: 8522
		public uint OperStatus;

		// Token: 0x0400214B RID: 8523
		public uint LastChange;

		// Token: 0x0400214C RID: 8524
		public int InOctets;

		// Token: 0x0400214D RID: 8525
		public int InUcastPkts;

		// Token: 0x0400214E RID: 8526
		public int InNUcastPkts;

		// Token: 0x0400214F RID: 8527
		public int InDiscards;

		// Token: 0x04002150 RID: 8528
		public int InErrors;

		// Token: 0x04002151 RID: 8529
		public int InUnknownProtos;

		// Token: 0x04002152 RID: 8530
		public int OutOctets;

		// Token: 0x04002153 RID: 8531
		public int OutUcastPkts;

		// Token: 0x04002154 RID: 8532
		public int OutNUcastPkts;

		// Token: 0x04002155 RID: 8533
		public int OutDiscards;

		// Token: 0x04002156 RID: 8534
		public int OutErrors;

		// Token: 0x04002157 RID: 8535
		public int OutQLen;

		// Token: 0x04002158 RID: 8536
		public int DescrLen;

		// Token: 0x04002159 RID: 8537
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
		public byte[] Descr;
	}
}
