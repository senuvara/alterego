﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200051A RID: 1306
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	internal struct Win32_IP_ADAPTER_ADDRESSES
	{
		// Token: 0x17000A62 RID: 2658
		// (get) Token: 0x060029EC RID: 10732 RVA: 0x00091B0F File Offset: 0x0008FD0F
		public bool DdnsEnabled
		{
			get
			{
				return (this.Flags & 1U) > 0U;
			}
		}

		// Token: 0x17000A63 RID: 2659
		// (get) Token: 0x060029ED RID: 10733 RVA: 0x00091B1C File Offset: 0x0008FD1C
		public bool DhcpEnabled
		{
			get
			{
				return (this.Flags & 4U) > 0U;
			}
		}

		// Token: 0x17000A64 RID: 2660
		// (get) Token: 0x060029EE RID: 10734 RVA: 0x00091B29 File Offset: 0x0008FD29
		public bool IsReceiveOnly
		{
			get
			{
				return (this.Flags & 8U) > 0U;
			}
		}

		// Token: 0x17000A65 RID: 2661
		// (get) Token: 0x060029EF RID: 10735 RVA: 0x00091B36 File Offset: 0x0008FD36
		public bool NoMulticast
		{
			get
			{
				return (this.Flags & 16U) > 0U;
			}
		}

		// Token: 0x040020FE RID: 8446
		public AlignmentUnion Alignment;

		// Token: 0x040020FF RID: 8447
		public IntPtr Next;

		// Token: 0x04002100 RID: 8448
		[MarshalAs(UnmanagedType.LPStr)]
		public string AdapterName;

		// Token: 0x04002101 RID: 8449
		public IntPtr FirstUnicastAddress;

		// Token: 0x04002102 RID: 8450
		public IntPtr FirstAnycastAddress;

		// Token: 0x04002103 RID: 8451
		public IntPtr FirstMulticastAddress;

		// Token: 0x04002104 RID: 8452
		public IntPtr FirstDnsServerAddress;

		// Token: 0x04002105 RID: 8453
		public string DnsSuffix;

		// Token: 0x04002106 RID: 8454
		public string Description;

		// Token: 0x04002107 RID: 8455
		public string FriendlyName;

		// Token: 0x04002108 RID: 8456
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public byte[] PhysicalAddress;

		// Token: 0x04002109 RID: 8457
		public uint PhysicalAddressLength;

		// Token: 0x0400210A RID: 8458
		public uint Flags;

		// Token: 0x0400210B RID: 8459
		public uint Mtu;

		// Token: 0x0400210C RID: 8460
		public NetworkInterfaceType IfType;

		// Token: 0x0400210D RID: 8461
		public OperationalStatus OperStatus;

		// Token: 0x0400210E RID: 8462
		public int Ipv6IfIndex;

		// Token: 0x0400210F RID: 8463
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
		public uint[] ZoneIndices;

		// Token: 0x04002110 RID: 8464
		public IntPtr FirstPrefix;

		// Token: 0x04002111 RID: 8465
		public ulong TransmitLinkSpeed;

		// Token: 0x04002112 RID: 8466
		public ulong ReceiveLinkSpeed;

		// Token: 0x04002113 RID: 8467
		public IntPtr FirstWinsServerAddress;

		// Token: 0x04002114 RID: 8468
		public IntPtr FirstGatewayAddress;

		// Token: 0x04002115 RID: 8469
		public uint Ipv4Metric;

		// Token: 0x04002116 RID: 8470
		public uint Ipv6Metric;

		// Token: 0x04002117 RID: 8471
		public ulong Luid;

		// Token: 0x04002118 RID: 8472
		public Win32_SOCKET_ADDRESS Dhcpv4Server;

		// Token: 0x04002119 RID: 8473
		public uint CompartmentId;

		// Token: 0x0400211A RID: 8474
		public ulong NetworkGuid;

		// Token: 0x0400211B RID: 8475
		public int ConnectionType;

		// Token: 0x0400211C RID: 8476
		public int TunnelType;

		// Token: 0x0400211D RID: 8477
		public Win32_SOCKET_ADDRESS Dhcpv6Server;

		// Token: 0x0400211E RID: 8478
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 130)]
		public byte[] Dhcpv6ClientDuid;

		// Token: 0x0400211F RID: 8479
		public ulong Dhcpv6ClientDuidLength;

		// Token: 0x04002120 RID: 8480
		public ulong Dhcpv6Iaid;

		// Token: 0x04002121 RID: 8481
		public IntPtr FirstDnsSuffix;

		// Token: 0x04002122 RID: 8482
		public const int GAA_FLAG_INCLUDE_WINS_INFO = 64;

		// Token: 0x04002123 RID: 8483
		public const int GAA_FLAG_INCLUDE_GATEWAYS = 128;

		// Token: 0x04002124 RID: 8484
		private const int MAX_ADAPTER_ADDRESS_LENGTH = 8;

		// Token: 0x04002125 RID: 8485
		private const int MAX_DHCPV6_DUID_LENGTH = 130;

		// Token: 0x04002126 RID: 8486
		private const int IP_ADAPTER_DDNS_ENABLED = 1;

		// Token: 0x04002127 RID: 8487
		private const int IP_ADAPTER_DHCP_ENABLED = 4;

		// Token: 0x04002128 RID: 8488
		private const int IP_ADAPTER_RECEIVE_ONLY = 8;

		// Token: 0x04002129 RID: 8489
		private const int IP_ADAPTER_NO_MULTICAST = 16;
	}
}
