﻿using System;
using System.IO;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004D9 RID: 1241
	internal sealed class LinuxIPv4InterfaceProperties : UnixIPv4InterfaceProperties
	{
		// Token: 0x06002821 RID: 10273 RVA: 0x0008E2C4 File Offset: 0x0008C4C4
		public LinuxIPv4InterfaceProperties(LinuxNetworkInterface iface) : base(iface)
		{
		}

		// Token: 0x1700095F RID: 2399
		// (get) Token: 0x06002822 RID: 10274 RVA: 0x0008E2D0 File Offset: 0x0008C4D0
		public override bool IsForwardingEnabled
		{
			get
			{
				string path = "/proc/sys/net/ipv4/conf/" + this.iface.Name + "/forwarding";
				return File.Exists(path) && LinuxNetworkInterface.ReadLine(path) != "0";
			}
		}

		// Token: 0x17000960 RID: 2400
		// (get) Token: 0x06002823 RID: 10275 RVA: 0x0008E314 File Offset: 0x0008C514
		public override int Mtu
		{
			get
			{
				string path = (this.iface as LinuxNetworkInterface).IfacePath + "mtu";
				int result = 0;
				if (File.Exists(path))
				{
					string s = LinuxNetworkInterface.ReadLine(path);
					try
					{
						result = int.Parse(s);
					}
					catch
					{
					}
				}
				return result;
			}
		}
	}
}
