﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000503 RID: 1283
	internal abstract class UnixNetworkInterface : NetworkInterface
	{
		// Token: 0x06002942 RID: 10562 RVA: 0x0009033F File Offset: 0x0008E53F
		internal UnixNetworkInterface(string name)
		{
			this.name = name;
			this.addresses = new List<IPAddress>();
		}

		// Token: 0x06002943 RID: 10563 RVA: 0x00090359 File Offset: 0x0008E559
		internal void AddAddress(IPAddress address)
		{
			this.addresses.Add(address);
		}

		// Token: 0x06002944 RID: 10564 RVA: 0x00090367 File Offset: 0x0008E567
		internal void SetLinkLayerInfo(int index, byte[] macAddress, NetworkInterfaceType type)
		{
			this.macAddress = macAddress;
			this.type = type;
		}

		// Token: 0x06002945 RID: 10565 RVA: 0x00090377 File Offset: 0x0008E577
		public override PhysicalAddress GetPhysicalAddress()
		{
			if (this.macAddress != null)
			{
				return new PhysicalAddress(this.macAddress);
			}
			return PhysicalAddress.None;
		}

		// Token: 0x06002946 RID: 10566 RVA: 0x00090394 File Offset: 0x0008E594
		public override bool Supports(NetworkInterfaceComponent networkInterfaceComponent)
		{
			bool flag = networkInterfaceComponent == NetworkInterfaceComponent.IPv4;
			bool flag2 = !flag && networkInterfaceComponent == NetworkInterfaceComponent.IPv6;
			foreach (IPAddress ipaddress in this.addresses)
			{
				if (flag && ipaddress.AddressFamily == AddressFamily.InterNetwork)
				{
					return true;
				}
				if (flag2 && ipaddress.AddressFamily == AddressFamily.InterNetworkV6)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x17000A0C RID: 2572
		// (get) Token: 0x06002947 RID: 10567 RVA: 0x00090418 File Offset: 0x0008E618
		public override string Description
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000A0D RID: 2573
		// (get) Token: 0x06002948 RID: 10568 RVA: 0x00090418 File Offset: 0x0008E618
		public override string Id
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000A0E RID: 2574
		// (get) Token: 0x06002949 RID: 10569 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool IsReceiveOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A0F RID: 2575
		// (get) Token: 0x0600294A RID: 10570 RVA: 0x00090418 File Offset: 0x0008E618
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000A10 RID: 2576
		// (get) Token: 0x0600294B RID: 10571 RVA: 0x00090420 File Offset: 0x0008E620
		public override NetworkInterfaceType NetworkInterfaceType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x17000A11 RID: 2577
		// (get) Token: 0x0600294C RID: 10572 RVA: 0x00090428 File Offset: 0x0008E628
		[MonoTODO("Parse dmesg?")]
		public override long Speed
		{
			get
			{
				return 1000000L;
			}
		}

		// Token: 0x17000A12 RID: 2578
		// (get) Token: 0x0600294D RID: 10573 RVA: 0x00090430 File Offset: 0x0008E630
		internal int NameIndex
		{
			get
			{
				return NetworkInterfaceFactory.UnixNetworkInterfaceAPI.if_nametoindex(this.Name);
			}
		}

		// Token: 0x040020A5 RID: 8357
		protected IPv4InterfaceStatistics ipv4stats;

		// Token: 0x040020A6 RID: 8358
		protected IPInterfaceProperties ipproperties;

		// Token: 0x040020A7 RID: 8359
		private string name;

		// Token: 0x040020A8 RID: 8360
		protected List<IPAddress> addresses;

		// Token: 0x040020A9 RID: 8361
		private byte[] macAddress;

		// Token: 0x040020AA RID: 8362
		private NetworkInterfaceType type;
	}
}
