﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200051E RID: 1310
	internal struct Win32LengthFlagsUnion
	{
		// Token: 0x17000A66 RID: 2662
		// (get) Token: 0x060029F0 RID: 10736 RVA: 0x00091B44 File Offset: 0x0008FD44
		public bool IsDnsEligible
		{
			get
			{
				return (this.Flags & 1U) > 0U;
			}
		}

		// Token: 0x17000A67 RID: 2663
		// (get) Token: 0x060029F1 RID: 10737 RVA: 0x00091B51 File Offset: 0x0008FD51
		public bool IsTransient
		{
			get
			{
				return (this.Flags & 2U) > 0U;
			}
		}

		// Token: 0x0400215E RID: 8542
		private const int IP_ADAPTER_ADDRESS_DNS_ELIGIBLE = 1;

		// Token: 0x0400215F RID: 8543
		private const int IP_ADAPTER_ADDRESS_TRANSIENT = 2;

		// Token: 0x04002160 RID: 8544
		public uint Length;

		// Token: 0x04002161 RID: 8545
		public uint Flags;
	}
}
