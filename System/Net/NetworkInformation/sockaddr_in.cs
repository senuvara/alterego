﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004EC RID: 1260
	internal struct sockaddr_in
	{
		// Token: 0x04002035 RID: 8245
		public ushort sin_family;

		// Token: 0x04002036 RID: 8246
		public ushort sin_port;

		// Token: 0x04002037 RID: 8247
		public uint sin_addr;
	}
}
