﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Represents the IP address of the network gateway. This class cannot be instantiated.</summary>
	// Token: 0x02000496 RID: 1174
	public abstract class GatewayIPAddressInformation
	{
		/// <summary>Get the IP address of the gateway.</summary>
		/// <returns>An <see cref="T:System.Net.IPAddress" /> object that contains the IP address of the gateway.</returns>
		// Token: 0x1700082B RID: 2091
		// (get) Token: 0x06002602 RID: 9730
		public abstract IPAddress Address { get; }

		/// <summary>Initializes the members of this class.</summary>
		// Token: 0x06002603 RID: 9731 RVA: 0x0000232F File Offset: 0x0000052F
		protected GatewayIPAddressInformation()
		{
		}
	}
}
