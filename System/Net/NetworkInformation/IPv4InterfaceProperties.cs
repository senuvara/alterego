﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Provides information about network interfaces that support Internet Protocol version 4 (IPv4).</summary>
	// Token: 0x020004A3 RID: 1187
	public abstract class IPv4InterfaceProperties
	{
		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether an interface uses Windows Internet Name Service (WINS).</summary>
		/// <returns>
		///     <see langword="true" /> if the interface uses WINS; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000875 RID: 2165
		// (get) Token: 0x06002681 RID: 9857
		public abstract bool UsesWins { get; }

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the interface is configured to use a Dynamic Host Configuration Protocol (DHCP) server to obtain an IP address.</summary>
		/// <returns>
		///     <see langword="true" /> if the interface is configured to obtain an IP address from a DHCP server; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000876 RID: 2166
		// (get) Token: 0x06002682 RID: 9858
		public abstract bool IsDhcpEnabled { get; }

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether this interface has an automatic private IP addressing (APIPA) address.</summary>
		/// <returns>
		///     <see langword="true" /> if the interface uses an APIPA address; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000877 RID: 2167
		// (get) Token: 0x06002683 RID: 9859
		public abstract bool IsAutomaticPrivateAddressingActive { get; }

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether this interface has automatic private IP addressing (APIPA) enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if the interface uses APIPA; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000878 RID: 2168
		// (get) Token: 0x06002684 RID: 9860
		public abstract bool IsAutomaticPrivateAddressingEnabled { get; }

		/// <summary>Gets the index of the network interface associated with the Internet Protocol version 4 (IPv4) address.</summary>
		/// <returns>An <see cref="T:System.Int32" /> that contains the index of the IPv4 interface.</returns>
		// Token: 0x17000879 RID: 2169
		// (get) Token: 0x06002685 RID: 9861
		public abstract int Index { get; }

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether this interface can forward (route) packets.</summary>
		/// <returns>
		///     <see langword="true" /> if this interface routes packets; otherwise <see langword="false" />.</returns>
		// Token: 0x1700087A RID: 2170
		// (get) Token: 0x06002686 RID: 9862
		public abstract bool IsForwardingEnabled { get; }

		/// <summary>Gets the maximum transmission unit (MTU) for this network interface.</summary>
		/// <returns>An <see cref="T:System.Int64" /> value that specifies the MTU.</returns>
		// Token: 0x1700087B RID: 2171
		// (get) Token: 0x06002687 RID: 9863
		public abstract int Mtu { get; }

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkInformation.IPv4InterfaceProperties" /> class.</summary>
		// Token: 0x06002688 RID: 9864 RVA: 0x0000232F File Offset: 0x0000052F
		protected IPv4InterfaceProperties()
		{
		}
	}
}
