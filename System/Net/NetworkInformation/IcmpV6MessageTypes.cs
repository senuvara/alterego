﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004E6 RID: 1254
	internal class IcmpV6MessageTypes
	{
		// Token: 0x060028B5 RID: 10421 RVA: 0x0000232F File Offset: 0x0000052F
		public IcmpV6MessageTypes()
		{
		}

		// Token: 0x04002016 RID: 8214
		public const int DestinationUnreachable = 1;

		// Token: 0x04002017 RID: 8215
		public const int PacketTooBig = 2;

		// Token: 0x04002018 RID: 8216
		public const int TimeExceeded = 3;

		// Token: 0x04002019 RID: 8217
		public const int ParameterProblem = 4;

		// Token: 0x0400201A RID: 8218
		public const int EchoRequest = 128;

		// Token: 0x0400201B RID: 8219
		public const int EchoReply = 129;

		// Token: 0x0400201C RID: 8220
		public const int GroupMembershipQuery = 130;

		// Token: 0x0400201D RID: 8221
		public const int GroupMembershipReport = 131;

		// Token: 0x0400201E RID: 8222
		public const int GroupMembershipReduction = 132;

		// Token: 0x0400201F RID: 8223
		public const int RouterSolicitation = 133;

		// Token: 0x04002020 RID: 8224
		public const int RouterAdvertisement = 134;

		// Token: 0x04002021 RID: 8225
		public const int NeighborSolicitation = 135;

		// Token: 0x04002022 RID: 8226
		public const int NeighborAdvertisement = 136;

		// Token: 0x04002023 RID: 8227
		public const int Redirect = 137;

		// Token: 0x04002024 RID: 8228
		public const int RouterRenumbering = 138;
	}
}
