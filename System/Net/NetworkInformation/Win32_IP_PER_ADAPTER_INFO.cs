﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004DC RID: 1244
	[StructLayout(LayoutKind.Sequential)]
	internal class Win32_IP_PER_ADAPTER_INFO
	{
		// Token: 0x06002830 RID: 10288 RVA: 0x0000232F File Offset: 0x0000052F
		public Win32_IP_PER_ADAPTER_INFO()
		{
		}

		// Token: 0x04001FFC RID: 8188
		public uint AutoconfigEnabled;

		// Token: 0x04001FFD RID: 8189
		public uint AutoconfigActive;

		// Token: 0x04001FFE RID: 8190
		public IntPtr CurrentDnsServer;

		// Token: 0x04001FFF RID: 8191
		public Win32_IP_ADDR_STRING DnsServerList;
	}
}
