﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Specifies the operational state of a network interface.</summary>
	// Token: 0x020004B3 RID: 1203
	public enum OperationalStatus
	{
		/// <summary>The network interface is up; it can transmit data packets.</summary>
		// Token: 0x04001F53 RID: 8019
		Up = 1,
		/// <summary>The network interface is unable to transmit data packets.</summary>
		// Token: 0x04001F54 RID: 8020
		Down,
		/// <summary>The network interface is running tests.</summary>
		// Token: 0x04001F55 RID: 8021
		Testing,
		/// <summary>The network interface status is not known.</summary>
		// Token: 0x04001F56 RID: 8022
		Unknown,
		/// <summary>The network interface is not in a condition to transmit data packets; it is waiting for an external event.</summary>
		// Token: 0x04001F57 RID: 8023
		Dormant,
		/// <summary>The network interface is unable to transmit data packets because of a missing component, typically a hardware component.</summary>
		// Token: 0x04001F58 RID: 8024
		NotPresent,
		/// <summary>The network interface is unable to transmit data packets because it runs on top of one or more other interfaces, and at least one of these "lower layer" interfaces is down.</summary>
		// Token: 0x04001F59 RID: 8025
		LowerLayerDown
	}
}
