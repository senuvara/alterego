﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000525 RID: 1317
	internal struct Win32_SOCKADDR
	{
		// Token: 0x0400217B RID: 8571
		public ushort AddressFamily;

		// Token: 0x0400217C RID: 8572
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 28)]
		public byte[] AddressData;
	}
}
