﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Specifies permission to access information about network interfaces and traffic statistics.</summary>
	// Token: 0x020004AE RID: 1198
	[Flags]
	public enum NetworkInformationAccess
	{
		/// <summary>No access to network information.</summary>
		// Token: 0x04001F48 RID: 8008
		None = 0,
		/// <summary>Read access to network information.</summary>
		// Token: 0x04001F49 RID: 8009
		Read = 1,
		/// <summary>Ping access to network information.</summary>
		// Token: 0x04001F4A RID: 8010
		Ping = 4
	}
}
