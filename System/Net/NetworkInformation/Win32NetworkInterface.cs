﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000517 RID: 1303
	internal class Win32NetworkInterface
	{
		// Token: 0x060029E8 RID: 10728
		[DllImport("iphlpapi.dll", SetLastError = true)]
		private static extern int GetNetworkParams(IntPtr ptr, ref int size);

		// Token: 0x17000A61 RID: 2657
		// (get) Token: 0x060029E9 RID: 10729 RVA: 0x00091AC8 File Offset: 0x0008FCC8
		public static Win32_FIXED_INFO FixedInfo
		{
			get
			{
				if (!Win32NetworkInterface.initialized)
				{
					int cb = 0;
					Win32NetworkInterface.GetNetworkParams(IntPtr.Zero, ref cb);
					IntPtr ptr = Marshal.AllocHGlobal(cb);
					Win32NetworkInterface.GetNetworkParams(ptr, ref cb);
					Win32NetworkInterface.fixedInfo = Marshal.PtrToStructure<Win32_FIXED_INFO>(ptr);
					Win32NetworkInterface.initialized = true;
				}
				return Win32NetworkInterface.fixedInfo;
			}
		}

		// Token: 0x060029EA RID: 10730 RVA: 0x0000232F File Offset: 0x0000052F
		public Win32NetworkInterface()
		{
		}

		// Token: 0x060029EB RID: 10731 RVA: 0x0000232D File Offset: 0x0000052D
		// Note: this type is marked as 'beforefieldinit'.
		static Win32NetworkInterface()
		{
		}

		// Token: 0x040020ED RID: 8429
		private static Win32_FIXED_INFO fixedInfo;

		// Token: 0x040020EE RID: 8430
		private static bool initialized;
	}
}
