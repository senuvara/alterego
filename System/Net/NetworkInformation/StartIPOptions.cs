﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004A9 RID: 1193
	[Flags]
	internal enum StartIPOptions
	{
		// Token: 0x04001F42 RID: 8002
		Both = 3,
		// Token: 0x04001F43 RID: 8003
		None = 0,
		// Token: 0x04001F44 RID: 8004
		StartIPv4 = 1,
		// Token: 0x04001F45 RID: 8005
		StartIPv6 = 2
	}
}
