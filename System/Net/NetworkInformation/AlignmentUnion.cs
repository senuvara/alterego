﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000519 RID: 1305
	[StructLayout(LayoutKind.Explicit)]
	internal struct AlignmentUnion
	{
		// Token: 0x040020FB RID: 8443
		[FieldOffset(0)]
		public ulong Alignment;

		// Token: 0x040020FC RID: 8444
		[FieldOffset(0)]
		public int Length;

		// Token: 0x040020FD RID: 8445
		[FieldOffset(4)]
		public int IfIndex;
	}
}
