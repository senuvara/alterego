﻿using System;
using System.Collections.Specialized;
using System.Globalization;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004D1 RID: 1233
	internal class MibIPGlobalStatistics : IPGlobalStatistics
	{
		// Token: 0x060027C7 RID: 10183 RVA: 0x0008D77D File Offset: 0x0008B97D
		public MibIPGlobalStatistics(StringDictionary dic)
		{
			this.dic = dic;
		}

		// Token: 0x060027C8 RID: 10184 RVA: 0x0008D78C File Offset: 0x0008B98C
		private long Get(string name)
		{
			if (this.dic[name] == null)
			{
				return 0L;
			}
			return long.Parse(this.dic[name], NumberFormatInfo.InvariantInfo);
		}

		// Token: 0x17000919 RID: 2329
		// (get) Token: 0x060027C9 RID: 10185 RVA: 0x0008D7B5 File Offset: 0x0008B9B5
		public override int DefaultTtl
		{
			get
			{
				return (int)this.Get("DefaultTTL");
			}
		}

		// Token: 0x1700091A RID: 2330
		// (get) Token: 0x060027CA RID: 10186 RVA: 0x0008D7C3 File Offset: 0x0008B9C3
		public override bool ForwardingEnabled
		{
			get
			{
				return this.Get("Forwarding") != 0L;
			}
		}

		// Token: 0x1700091B RID: 2331
		// (get) Token: 0x060027CB RID: 10187 RVA: 0x0008D7D4 File Offset: 0x0008B9D4
		public override int NumberOfInterfaces
		{
			get
			{
				return (int)this.Get("NumIf");
			}
		}

		// Token: 0x1700091C RID: 2332
		// (get) Token: 0x060027CC RID: 10188 RVA: 0x0008D7E2 File Offset: 0x0008B9E2
		public override int NumberOfIPAddresses
		{
			get
			{
				return (int)this.Get("NumAddr");
			}
		}

		// Token: 0x1700091D RID: 2333
		// (get) Token: 0x060027CD RID: 10189 RVA: 0x0008D7F0 File Offset: 0x0008B9F0
		public override int NumberOfRoutes
		{
			get
			{
				return (int)this.Get("NumRoutes");
			}
		}

		// Token: 0x1700091E RID: 2334
		// (get) Token: 0x060027CE RID: 10190 RVA: 0x0008D7FE File Offset: 0x0008B9FE
		public override long OutputPacketRequests
		{
			get
			{
				return this.Get("OutRequests");
			}
		}

		// Token: 0x1700091F RID: 2335
		// (get) Token: 0x060027CF RID: 10191 RVA: 0x0008D80B File Offset: 0x0008BA0B
		public override long OutputPacketRoutingDiscards
		{
			get
			{
				return this.Get("RoutingDiscards");
			}
		}

		// Token: 0x17000920 RID: 2336
		// (get) Token: 0x060027D0 RID: 10192 RVA: 0x0008D818 File Offset: 0x0008BA18
		public override long OutputPacketsDiscarded
		{
			get
			{
				return this.Get("OutDiscards");
			}
		}

		// Token: 0x17000921 RID: 2337
		// (get) Token: 0x060027D1 RID: 10193 RVA: 0x0008D825 File Offset: 0x0008BA25
		public override long OutputPacketsWithNoRoute
		{
			get
			{
				return this.Get("OutNoRoutes");
			}
		}

		// Token: 0x17000922 RID: 2338
		// (get) Token: 0x060027D2 RID: 10194 RVA: 0x0008D832 File Offset: 0x0008BA32
		public override long PacketFragmentFailures
		{
			get
			{
				return this.Get("FragFails");
			}
		}

		// Token: 0x17000923 RID: 2339
		// (get) Token: 0x060027D3 RID: 10195 RVA: 0x0008D83F File Offset: 0x0008BA3F
		public override long PacketReassembliesRequired
		{
			get
			{
				return this.Get("ReasmReqds");
			}
		}

		// Token: 0x17000924 RID: 2340
		// (get) Token: 0x060027D4 RID: 10196 RVA: 0x0008D84C File Offset: 0x0008BA4C
		public override long PacketReassemblyFailures
		{
			get
			{
				return this.Get("ReasmFails");
			}
		}

		// Token: 0x17000925 RID: 2341
		// (get) Token: 0x060027D5 RID: 10197 RVA: 0x0008D859 File Offset: 0x0008BA59
		public override long PacketReassemblyTimeout
		{
			get
			{
				return this.Get("ReasmTimeout");
			}
		}

		// Token: 0x17000926 RID: 2342
		// (get) Token: 0x060027D6 RID: 10198 RVA: 0x0008D866 File Offset: 0x0008BA66
		public override long PacketsFragmented
		{
			get
			{
				return this.Get("FragOks");
			}
		}

		// Token: 0x17000927 RID: 2343
		// (get) Token: 0x060027D7 RID: 10199 RVA: 0x0008D873 File Offset: 0x0008BA73
		public override long PacketsReassembled
		{
			get
			{
				return this.Get("ReasmOks");
			}
		}

		// Token: 0x17000928 RID: 2344
		// (get) Token: 0x060027D8 RID: 10200 RVA: 0x0008D880 File Offset: 0x0008BA80
		public override long ReceivedPackets
		{
			get
			{
				return this.Get("InReceives");
			}
		}

		// Token: 0x17000929 RID: 2345
		// (get) Token: 0x060027D9 RID: 10201 RVA: 0x0008D88D File Offset: 0x0008BA8D
		public override long ReceivedPacketsDelivered
		{
			get
			{
				return this.Get("InDelivers");
			}
		}

		// Token: 0x1700092A RID: 2346
		// (get) Token: 0x060027DA RID: 10202 RVA: 0x0008D89A File Offset: 0x0008BA9A
		public override long ReceivedPacketsDiscarded
		{
			get
			{
				return this.Get("InDiscards");
			}
		}

		// Token: 0x1700092B RID: 2347
		// (get) Token: 0x060027DB RID: 10203 RVA: 0x0008D8A7 File Offset: 0x0008BAA7
		public override long ReceivedPacketsForwarded
		{
			get
			{
				return this.Get("ForwDatagrams");
			}
		}

		// Token: 0x1700092C RID: 2348
		// (get) Token: 0x060027DC RID: 10204 RVA: 0x0008D8B4 File Offset: 0x0008BAB4
		public override long ReceivedPacketsWithAddressErrors
		{
			get
			{
				return this.Get("InAddrErrors");
			}
		}

		// Token: 0x1700092D RID: 2349
		// (get) Token: 0x060027DD RID: 10205 RVA: 0x0008D8C1 File Offset: 0x0008BAC1
		public override long ReceivedPacketsWithHeadersErrors
		{
			get
			{
				return this.Get("InHdrErrors");
			}
		}

		// Token: 0x1700092E RID: 2350
		// (get) Token: 0x060027DE RID: 10206 RVA: 0x0008D8CE File Offset: 0x0008BACE
		public override long ReceivedPacketsWithUnknownProtocol
		{
			get
			{
				return this.Get("InUnknownProtos");
			}
		}

		// Token: 0x04001FD4 RID: 8148
		private StringDictionary dic;
	}
}
