﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Used to control how <see cref="T:System.Net.NetworkInformation.Ping" /> data packets are transmitted.</summary>
	// Token: 0x020004B5 RID: 1205
	public class PingOptions
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkInformation.PingOptions" /> class and sets the Time to Live and fragmentation values.</summary>
		/// <param name="ttl">An <see cref="T:System.Int32" /> value greater than zero that specifies the number of times that the <see cref="T:System.Net.NetworkInformation.Ping" /> data packets can be forwarded.</param>
		/// <param name="dontFragment">
		///       <see langword="true" /> to prevent data sent to the remote host from being fragmented; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="ttl " />is less than or equal to zero.</exception>
		// Token: 0x06002715 RID: 10005 RVA: 0x0008C4C4 File Offset: 0x0008A6C4
		public PingOptions(int ttl, bool dontFragment)
		{
			if (ttl <= 0)
			{
				throw new ArgumentOutOfRangeException("ttl");
			}
			this.ttl = ttl;
			this.dontFragment = dontFragment;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkInformation.PingOptions" /> class.</summary>
		// Token: 0x06002716 RID: 10006 RVA: 0x0008C4F4 File Offset: 0x0008A6F4
		public PingOptions()
		{
		}

		/// <summary>Gets or sets the number of routing nodes that can forward the <see cref="T:System.Net.NetworkInformation.Ping" /> data before it is discarded.</summary>
		/// <returns>An <see cref="T:System.Int32" /> value that specifies the number of times the <see cref="T:System.Net.NetworkInformation.Ping" /> data packets can be forwarded. The default is 128.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified for a set operation is less than or equal to zero.</exception>
		// Token: 0x170008CF RID: 2255
		// (get) Token: 0x06002717 RID: 10007 RVA: 0x0008C507 File Offset: 0x0008A707
		// (set) Token: 0x06002718 RID: 10008 RVA: 0x0008C50F File Offset: 0x0008A70F
		public int Ttl
		{
			get
			{
				return this.ttl;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.ttl = value;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that controls fragmentation of the data sent to the remote host.</summary>
		/// <returns>
		///     <see langword="true" /> if the data cannot be sent in multiple packets; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170008D0 RID: 2256
		// (get) Token: 0x06002719 RID: 10009 RVA: 0x0008C527 File Offset: 0x0008A727
		// (set) Token: 0x0600271A RID: 10010 RVA: 0x0008C52F File Offset: 0x0008A72F
		public bool DontFragment
		{
			get
			{
				return this.dontFragment;
			}
			set
			{
				this.dontFragment = value;
			}
		}

		// Token: 0x04001F5E RID: 8030
		private const int DontFragmentFlag = 2;

		// Token: 0x04001F5F RID: 8031
		private int ttl = 128;

		// Token: 0x04001F60 RID: 8032
		private bool dontFragment;
	}
}
