﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004BB RID: 1211
	internal class SystemIPAddressInformation : IPAddressInformation
	{
		// Token: 0x06002728 RID: 10024 RVA: 0x0008C70C File Offset: 0x0008A90C
		public SystemIPAddressInformation(IPAddress address, bool isDnsEligible, bool isTransient)
		{
			this.address = address;
			this.dnsEligible = isDnsEligible;
			this.transient = isTransient;
		}

		// Token: 0x170008D7 RID: 2263
		// (get) Token: 0x06002729 RID: 10025 RVA: 0x0008C730 File Offset: 0x0008A930
		public override IPAddress Address
		{
			get
			{
				return this.address;
			}
		}

		// Token: 0x170008D8 RID: 2264
		// (get) Token: 0x0600272A RID: 10026 RVA: 0x0008C738 File Offset: 0x0008A938
		public override bool IsTransient
		{
			get
			{
				return this.transient;
			}
		}

		// Token: 0x170008D9 RID: 2265
		// (get) Token: 0x0600272B RID: 10027 RVA: 0x0008C740 File Offset: 0x0008A940
		public override bool IsDnsEligible
		{
			get
			{
				return this.dnsEligible;
			}
		}

		// Token: 0x04001F7D RID: 8061
		private IPAddress address;

		// Token: 0x04001F7E RID: 8062
		internal bool transient;

		// Token: 0x04001F7F RID: 8063
		internal bool dnsEligible = true;
	}
}
