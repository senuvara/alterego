﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004EF RID: 1263
	internal struct sockaddr_ll
	{
		// Token: 0x0400203E RID: 8254
		public ushort sll_family;

		// Token: 0x0400203F RID: 8255
		public ushort sll_protocol;

		// Token: 0x04002040 RID: 8256
		public int sll_ifindex;

		// Token: 0x04002041 RID: 8257
		public ushort sll_hatype;

		// Token: 0x04002042 RID: 8258
		public byte sll_pkttype;

		// Token: 0x04002043 RID: 8259
		public byte sll_halen;

		// Token: 0x04002044 RID: 8260
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public byte[] sll_addr;
	}
}
