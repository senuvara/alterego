﻿using System;
using System.Globalization;
using System.IO;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000504 RID: 1284
	internal sealed class LinuxNetworkInterface : UnixNetworkInterface
	{
		// Token: 0x17000A13 RID: 2579
		// (get) Token: 0x0600294E RID: 10574 RVA: 0x0009043D File Offset: 0x0008E63D
		internal string IfacePath
		{
			get
			{
				return this.iface_path;
			}
		}

		// Token: 0x0600294F RID: 10575 RVA: 0x00090448 File Offset: 0x0008E648
		internal LinuxNetworkInterface(string name) : base(name)
		{
			this.iface_path = "/sys/class/net/" + name + "/";
			this.iface_operstate_path = this.iface_path + "operstate";
			this.iface_flags_path = this.iface_path + "flags";
		}

		// Token: 0x06002950 RID: 10576 RVA: 0x0009049E File Offset: 0x0008E69E
		public override IPInterfaceProperties GetIPProperties()
		{
			if (this.ipproperties == null)
			{
				this.ipproperties = new LinuxIPInterfaceProperties(this, this.addresses);
			}
			return this.ipproperties;
		}

		// Token: 0x06002951 RID: 10577 RVA: 0x000904C0 File Offset: 0x0008E6C0
		public override IPv4InterfaceStatistics GetIPv4Statistics()
		{
			if (this.ipv4stats == null)
			{
				this.ipv4stats = new LinuxIPv4InterfaceStatistics(this);
			}
			return this.ipv4stats;
		}

		// Token: 0x17000A14 RID: 2580
		// (get) Token: 0x06002952 RID: 10578 RVA: 0x000904DC File Offset: 0x0008E6DC
		public override OperationalStatus OperationalStatus
		{
			get
			{
				if (!Directory.Exists(this.iface_path))
				{
					return OperationalStatus.Unknown;
				}
				try
				{
					string text = LinuxNetworkInterface.ReadLine(this.iface_operstate_path);
					uint num = <PrivateImplementationDetails>.ComputeStringHash(text);
					if (num <= 2313571237U)
					{
						if (num != 1035581717U)
						{
							if (num != 1128467232U)
							{
								if (num == 2313571237U)
								{
									if (text == "notpresent")
									{
										return OperationalStatus.NotPresent;
									}
								}
							}
							else if (text == "up")
							{
								return OperationalStatus.Up;
							}
						}
						else if (text == "down")
						{
							return OperationalStatus.Down;
						}
					}
					else if (num <= 2966218339U)
					{
						if (num != 2608177081U)
						{
							if (num == 2966218339U)
							{
								if (text == "lowerlayerdown")
								{
									return OperationalStatus.LowerLayerDown;
								}
							}
						}
						else if (text == "unknown")
						{
							return OperationalStatus.Unknown;
						}
					}
					else if (num != 3340047486U)
					{
						if (num == 3948890523U)
						{
							if (text == "testing")
							{
								return OperationalStatus.Testing;
							}
						}
					}
					else if (text == "dormant")
					{
						return OperationalStatus.Dormant;
					}
				}
				catch
				{
				}
				return OperationalStatus.Unknown;
			}
		}

		// Token: 0x17000A15 RID: 2581
		// (get) Token: 0x06002953 RID: 10579 RVA: 0x00090604 File Offset: 0x0008E804
		public override bool SupportsMulticast
		{
			get
			{
				if (!Directory.Exists(this.iface_path))
				{
					return false;
				}
				bool result;
				try
				{
					string text = LinuxNetworkInterface.ReadLine(this.iface_flags_path);
					if (text.Length > 2 && text[0] == '0' && text[1] == 'x')
					{
						text = text.Substring(2);
					}
					result = ((ulong.Parse(text, NumberStyles.HexNumber) & 4096UL) == 4096UL);
				}
				catch
				{
					result = false;
				}
				return result;
			}
		}

		// Token: 0x06002954 RID: 10580 RVA: 0x00090688 File Offset: 0x0008E888
		internal static string ReadLine(string path)
		{
			string result;
			using (FileStream fileStream = File.OpenRead(path))
			{
				using (StreamReader streamReader = new StreamReader(fileStream))
				{
					result = streamReader.ReadLine();
				}
			}
			return result;
		}

		// Token: 0x040020AB RID: 8363
		private string iface_path;

		// Token: 0x040020AC RID: 8364
		private string iface_operstate_path;

		// Token: 0x040020AD RID: 8365
		private string iface_flags_path;
	}
}
