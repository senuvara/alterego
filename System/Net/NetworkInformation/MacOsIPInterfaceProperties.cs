﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004D6 RID: 1238
	internal class MacOsIPInterfaceProperties : UnixIPInterfaceProperties
	{
		// Token: 0x06002807 RID: 10247 RVA: 0x0008DD80 File Offset: 0x0008BF80
		public MacOsIPInterfaceProperties(MacOsNetworkInterface iface, List<IPAddress> addresses) : base(iface, addresses)
		{
		}

		// Token: 0x06002808 RID: 10248 RVA: 0x0008DEE5 File Offset: 0x0008C0E5
		public override IPv4InterfaceProperties GetIPv4Properties()
		{
			if (this.ipv4iface_properties == null)
			{
				this.ipv4iface_properties = new MacOsIPv4InterfaceProperties(this.iface as MacOsNetworkInterface);
			}
			return this.ipv4iface_properties;
		}

		// Token: 0x06002809 RID: 10249
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool ParseRouteInfo_internal(string iface, out string[] gw_addr_list);

		// Token: 0x1700094F RID: 2383
		// (get) Token: 0x0600280A RID: 10250 RVA: 0x0008DF0C File Offset: 0x0008C10C
		public override GatewayIPAddressInformationCollection GatewayAddresses
		{
			get
			{
				IPAddressCollection ipaddressCollection = new IPAddressCollection();
				string[] array;
				if (!MacOsIPInterfaceProperties.ParseRouteInfo_internal(this.iface.Name.ToString(), out array))
				{
					return new GatewayIPAddressInformationCollection();
				}
				for (int i = 0; i < array.Length; i++)
				{
					try
					{
						IPAddress ipaddress = IPAddress.Parse(array[i]);
						if (!ipaddress.Equals(IPAddress.Any) && !ipaddressCollection.Contains(ipaddress))
						{
							ipaddressCollection.InternalAdd(ipaddress);
						}
					}
					catch (ArgumentNullException)
					{
					}
				}
				return SystemGatewayIPAddressInformation.ToGatewayIpAddressInformationCollection(ipaddressCollection);
			}
		}
	}
}
