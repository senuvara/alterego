﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004F0 RID: 1264
	internal enum LinuxArpHardware
	{
		// Token: 0x04002046 RID: 8262
		ETHER = 1,
		// Token: 0x04002047 RID: 8263
		EETHER,
		// Token: 0x04002048 RID: 8264
		PRONET = 4,
		// Token: 0x04002049 RID: 8265
		ATM = 19,
		// Token: 0x0400204A RID: 8266
		SLIP = 256,
		// Token: 0x0400204B RID: 8267
		CSLIP,
		// Token: 0x0400204C RID: 8268
		SLIP6,
		// Token: 0x0400204D RID: 8269
		CSLIP6,
		// Token: 0x0400204E RID: 8270
		PPP = 512,
		// Token: 0x0400204F RID: 8271
		LOOPBACK = 772,
		// Token: 0x04002050 RID: 8272
		FDDI = 774,
		// Token: 0x04002051 RID: 8273
		TUNNEL = 768,
		// Token: 0x04002052 RID: 8274
		TUNNEL6,
		// Token: 0x04002053 RID: 8275
		SIT = 776,
		// Token: 0x04002054 RID: 8276
		IPDDP,
		// Token: 0x04002055 RID: 8277
		IPGRE,
		// Token: 0x04002056 RID: 8278
		IP6GRE = 823
	}
}
