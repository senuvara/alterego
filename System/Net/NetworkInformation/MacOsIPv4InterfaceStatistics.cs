﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004DF RID: 1247
	internal class MacOsIPv4InterfaceStatistics : IPv4InterfaceStatistics
	{
		// Token: 0x0600284C RID: 10316 RVA: 0x0008E5BD File Offset: 0x0008C7BD
		public MacOsIPv4InterfaceStatistics(MacOsNetworkInterface parent)
		{
		}

		// Token: 0x17000982 RID: 2434
		// (get) Token: 0x0600284D RID: 10317 RVA: 0x0002873C File Offset: 0x0002693C
		public override long BytesReceived
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000983 RID: 2435
		// (get) Token: 0x0600284E RID: 10318 RVA: 0x0002873C File Offset: 0x0002693C
		public override long BytesSent
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000984 RID: 2436
		// (get) Token: 0x0600284F RID: 10319 RVA: 0x0002873C File Offset: 0x0002693C
		public override long IncomingPacketsDiscarded
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000985 RID: 2437
		// (get) Token: 0x06002850 RID: 10320 RVA: 0x0002873C File Offset: 0x0002693C
		public override long IncomingPacketsWithErrors
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000986 RID: 2438
		// (get) Token: 0x06002851 RID: 10321 RVA: 0x0002873C File Offset: 0x0002693C
		public override long IncomingUnknownProtocolPackets
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000987 RID: 2439
		// (get) Token: 0x06002852 RID: 10322 RVA: 0x0002873C File Offset: 0x0002693C
		public override long NonUnicastPacketsReceived
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000988 RID: 2440
		// (get) Token: 0x06002853 RID: 10323 RVA: 0x0002873C File Offset: 0x0002693C
		public override long NonUnicastPacketsSent
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000989 RID: 2441
		// (get) Token: 0x06002854 RID: 10324 RVA: 0x0002873C File Offset: 0x0002693C
		public override long OutgoingPacketsDiscarded
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x1700098A RID: 2442
		// (get) Token: 0x06002855 RID: 10325 RVA: 0x0002873C File Offset: 0x0002693C
		public override long OutgoingPacketsWithErrors
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x1700098B RID: 2443
		// (get) Token: 0x06002856 RID: 10326 RVA: 0x0002873C File Offset: 0x0002693C
		public override long OutputQueueLength
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x1700098C RID: 2444
		// (get) Token: 0x06002857 RID: 10327 RVA: 0x0002873C File Offset: 0x0002693C
		public override long UnicastPacketsReceived
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x1700098D RID: 2445
		// (get) Token: 0x06002858 RID: 10328 RVA: 0x0002873C File Offset: 0x0002693C
		public override long UnicastPacketsSent
		{
			get
			{
				return 0L;
			}
		}
	}
}
