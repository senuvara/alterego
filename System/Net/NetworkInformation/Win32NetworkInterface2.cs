﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000506 RID: 1286
	internal class Win32NetworkInterface2 : NetworkInterface
	{
		// Token: 0x0600295A RID: 10586
		[DllImport("iphlpapi.dll", SetLastError = true)]
		private static extern int GetAdaptersInfo(IntPtr info, ref int size);

		// Token: 0x0600295B RID: 10587
		[DllImport("iphlpapi.dll", SetLastError = true)]
		private static extern int GetIfEntry(ref Win32_MIB_IFROW row);

		// Token: 0x0600295C RID: 10588 RVA: 0x00090754 File Offset: 0x0008E954
		private static Win32_IP_ADAPTER_INFO[] GetAdaptersInfo()
		{
			int cb = 0;
			Win32NetworkInterface2.GetAdaptersInfo(IntPtr.Zero, ref cb);
			IntPtr intPtr = Marshal.AllocHGlobal(cb);
			int adaptersInfo = Win32NetworkInterface2.GetAdaptersInfo(intPtr, ref cb);
			if (adaptersInfo != 0)
			{
				throw new NetworkInformationException(adaptersInfo);
			}
			List<Win32_IP_ADAPTER_INFO> list = new List<Win32_IP_ADAPTER_INFO>();
			IntPtr intPtr2 = intPtr;
			while (intPtr2 != IntPtr.Zero)
			{
				Win32_IP_ADAPTER_INFO win32_IP_ADAPTER_INFO = Marshal.PtrToStructure<Win32_IP_ADAPTER_INFO>(intPtr2);
				list.Add(win32_IP_ADAPTER_INFO);
				intPtr2 = win32_IP_ADAPTER_INFO.Next;
			}
			return list.ToArray();
		}

		// Token: 0x0600295D RID: 10589 RVA: 0x000907C0 File Offset: 0x0008E9C0
		internal Win32NetworkInterface2(Win32_IP_ADAPTER_ADDRESSES addr)
		{
			this.addr = addr;
			this.mib4 = default(Win32_MIB_IFROW);
			this.mib4.Index = addr.Alignment.IfIndex;
			if (Win32NetworkInterface2.GetIfEntry(ref this.mib4) != 0)
			{
				this.mib4.Index = -1;
			}
			this.mib6 = default(Win32_MIB_IFROW);
			this.mib6.Index = addr.Ipv6IfIndex;
			if (Win32NetworkInterface2.GetIfEntry(ref this.mib6) != 0)
			{
				this.mib6.Index = -1;
			}
			this.ip4stats = new Win32IPv4InterfaceStatistics(this.mib4);
			this.ip_if_props = new Win32IPInterfaceProperties2(addr, this.mib4, this.mib6);
		}

		// Token: 0x0600295E RID: 10590 RVA: 0x00090874 File Offset: 0x0008EA74
		public override IPInterfaceProperties GetIPProperties()
		{
			return this.ip_if_props;
		}

		// Token: 0x0600295F RID: 10591 RVA: 0x0009087C File Offset: 0x0008EA7C
		public override IPv4InterfaceStatistics GetIPv4Statistics()
		{
			return this.ip4stats;
		}

		// Token: 0x06002960 RID: 10592 RVA: 0x00090884 File Offset: 0x0008EA84
		public override PhysicalAddress GetPhysicalAddress()
		{
			byte[] array = new byte[this.addr.PhysicalAddressLength];
			Array.Copy(this.addr.PhysicalAddress, 0, array, 0, array.Length);
			return new PhysicalAddress(array);
		}

		// Token: 0x06002961 RID: 10593 RVA: 0x000908BE File Offset: 0x0008EABE
		public override bool Supports(NetworkInterfaceComponent networkInterfaceComponent)
		{
			if (networkInterfaceComponent != NetworkInterfaceComponent.IPv4)
			{
				return networkInterfaceComponent == NetworkInterfaceComponent.IPv6 && this.mib6.Index >= 0;
			}
			return this.mib4.Index >= 0;
		}

		// Token: 0x17000A18 RID: 2584
		// (get) Token: 0x06002962 RID: 10594 RVA: 0x000908EE File Offset: 0x0008EAEE
		public override string Description
		{
			get
			{
				return this.addr.Description;
			}
		}

		// Token: 0x17000A19 RID: 2585
		// (get) Token: 0x06002963 RID: 10595 RVA: 0x000908FB File Offset: 0x0008EAFB
		public override string Id
		{
			get
			{
				return this.addr.AdapterName;
			}
		}

		// Token: 0x17000A1A RID: 2586
		// (get) Token: 0x06002964 RID: 10596 RVA: 0x00090908 File Offset: 0x0008EB08
		public override bool IsReceiveOnly
		{
			get
			{
				return this.addr.IsReceiveOnly;
			}
		}

		// Token: 0x17000A1B RID: 2587
		// (get) Token: 0x06002965 RID: 10597 RVA: 0x00090915 File Offset: 0x0008EB15
		public override string Name
		{
			get
			{
				return this.addr.FriendlyName;
			}
		}

		// Token: 0x17000A1C RID: 2588
		// (get) Token: 0x06002966 RID: 10598 RVA: 0x00090922 File Offset: 0x0008EB22
		public override NetworkInterfaceType NetworkInterfaceType
		{
			get
			{
				return this.addr.IfType;
			}
		}

		// Token: 0x17000A1D RID: 2589
		// (get) Token: 0x06002967 RID: 10599 RVA: 0x0009092F File Offset: 0x0008EB2F
		public override OperationalStatus OperationalStatus
		{
			get
			{
				return this.addr.OperStatus;
			}
		}

		// Token: 0x17000A1E RID: 2590
		// (get) Token: 0x06002968 RID: 10600 RVA: 0x0009093C File Offset: 0x0008EB3C
		public override long Speed
		{
			get
			{
				return (long)((ulong)((this.mib6.Index >= 0) ? this.mib6.Speed : this.mib4.Speed));
			}
		}

		// Token: 0x17000A1F RID: 2591
		// (get) Token: 0x06002969 RID: 10601 RVA: 0x00090965 File Offset: 0x0008EB65
		public override bool SupportsMulticast
		{
			get
			{
				return !this.addr.NoMulticast;
			}
		}

		// Token: 0x040020AF RID: 8367
		private Win32_IP_ADAPTER_ADDRESSES addr;

		// Token: 0x040020B0 RID: 8368
		private Win32_MIB_IFROW mib4;

		// Token: 0x040020B1 RID: 8369
		private Win32_MIB_IFROW mib6;

		// Token: 0x040020B2 RID: 8370
		private Win32IPv4InterfaceStatistics ip4stats;

		// Token: 0x040020B3 RID: 8371
		private IPInterfaceProperties ip_if_props;
	}
}
