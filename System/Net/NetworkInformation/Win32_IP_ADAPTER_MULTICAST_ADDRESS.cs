﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000521 RID: 1313
	internal struct Win32_IP_ADAPTER_MULTICAST_ADDRESS
	{
		// Token: 0x04002168 RID: 8552
		public Win32LengthFlagsUnion LengthFlags;

		// Token: 0x04002169 RID: 8553
		public IntPtr Next;

		// Token: 0x0400216A RID: 8554
		public Win32_SOCKET_ADDRESS Address;
	}
}
