﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200051F RID: 1311
	internal struct Win32_IP_ADAPTER_ANYCAST_ADDRESS
	{
		// Token: 0x04002162 RID: 8546
		public Win32LengthFlagsUnion LengthFlags;

		// Token: 0x04002163 RID: 8547
		public IntPtr Next;

		// Token: 0x04002164 RID: 8548
		public Win32_SOCKET_ADDRESS Address;
	}
}
