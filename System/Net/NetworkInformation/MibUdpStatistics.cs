﻿using System;
using System.Collections.Specialized;
using System.Globalization;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000512 RID: 1298
	internal class MibUdpStatistics : UdpStatistics
	{
		// Token: 0x060029C4 RID: 10692 RVA: 0x0009183D File Offset: 0x0008FA3D
		public MibUdpStatistics(StringDictionary dic)
		{
			this.dic = dic;
		}

		// Token: 0x060029C5 RID: 10693 RVA: 0x0009184C File Offset: 0x0008FA4C
		private long Get(string name)
		{
			if (this.dic[name] == null)
			{
				return 0L;
			}
			return long.Parse(this.dic[name], NumberFormatInfo.InvariantInfo);
		}

		// Token: 0x17000A43 RID: 2627
		// (get) Token: 0x060029C6 RID: 10694 RVA: 0x00091875 File Offset: 0x0008FA75
		public override long DatagramsReceived
		{
			get
			{
				return this.Get("InDatagrams");
			}
		}

		// Token: 0x17000A44 RID: 2628
		// (get) Token: 0x060029C7 RID: 10695 RVA: 0x00091882 File Offset: 0x0008FA82
		public override long DatagramsSent
		{
			get
			{
				return this.Get("OutDatagrams");
			}
		}

		// Token: 0x17000A45 RID: 2629
		// (get) Token: 0x060029C8 RID: 10696 RVA: 0x0009188F File Offset: 0x0008FA8F
		public override long IncomingDatagramsDiscarded
		{
			get
			{
				return this.Get("NoPorts");
			}
		}

		// Token: 0x17000A46 RID: 2630
		// (get) Token: 0x060029C9 RID: 10697 RVA: 0x0009189C File Offset: 0x0008FA9C
		public override long IncomingDatagramsWithErrors
		{
			get
			{
				return this.Get("InErrors");
			}
		}

		// Token: 0x17000A47 RID: 2631
		// (get) Token: 0x060029CA RID: 10698 RVA: 0x000918A9 File Offset: 0x0008FAA9
		public override int UdpListeners
		{
			get
			{
				return (int)this.Get("NumAddrs");
			}
		}

		// Token: 0x040020E2 RID: 8418
		private StringDictionary dic;
	}
}
