﻿using System;
using System.Collections.Specialized;
using System.Globalization;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200050F RID: 1295
	internal class MibTcpStatistics : TcpStatistics
	{
		// Token: 0x060029A5 RID: 10661 RVA: 0x0009167C File Offset: 0x0008F87C
		public MibTcpStatistics(StringDictionary dic)
		{
			this.dic = dic;
		}

		// Token: 0x060029A6 RID: 10662 RVA: 0x0009168B File Offset: 0x0008F88B
		private long Get(string name)
		{
			if (this.dic[name] == null)
			{
				return 0L;
			}
			return long.Parse(this.dic[name], NumberFormatInfo.InvariantInfo);
		}

		// Token: 0x17000A27 RID: 2599
		// (get) Token: 0x060029A7 RID: 10663 RVA: 0x000916B4 File Offset: 0x0008F8B4
		public override long ConnectionsAccepted
		{
			get
			{
				return this.Get("PassiveOpens");
			}
		}

		// Token: 0x17000A28 RID: 2600
		// (get) Token: 0x060029A8 RID: 10664 RVA: 0x000916C1 File Offset: 0x0008F8C1
		public override long ConnectionsInitiated
		{
			get
			{
				return this.Get("ActiveOpens");
			}
		}

		// Token: 0x17000A29 RID: 2601
		// (get) Token: 0x060029A9 RID: 10665 RVA: 0x000916CE File Offset: 0x0008F8CE
		public override long CumulativeConnections
		{
			get
			{
				return this.Get("NumConns");
			}
		}

		// Token: 0x17000A2A RID: 2602
		// (get) Token: 0x060029AA RID: 10666 RVA: 0x000916DB File Offset: 0x0008F8DB
		public override long CurrentConnections
		{
			get
			{
				return this.Get("CurrEstab");
			}
		}

		// Token: 0x17000A2B RID: 2603
		// (get) Token: 0x060029AB RID: 10667 RVA: 0x000916E8 File Offset: 0x0008F8E8
		public override long ErrorsReceived
		{
			get
			{
				return this.Get("InErrs");
			}
		}

		// Token: 0x17000A2C RID: 2604
		// (get) Token: 0x060029AC RID: 10668 RVA: 0x000916F5 File Offset: 0x0008F8F5
		public override long FailedConnectionAttempts
		{
			get
			{
				return this.Get("AttemptFails");
			}
		}

		// Token: 0x17000A2D RID: 2605
		// (get) Token: 0x060029AD RID: 10669 RVA: 0x00091702 File Offset: 0x0008F902
		public override long MaximumConnections
		{
			get
			{
				return this.Get("MaxConn");
			}
		}

		// Token: 0x17000A2E RID: 2606
		// (get) Token: 0x060029AE RID: 10670 RVA: 0x0009170F File Offset: 0x0008F90F
		public override long MaximumTransmissionTimeout
		{
			get
			{
				return this.Get("RtoMax");
			}
		}

		// Token: 0x17000A2F RID: 2607
		// (get) Token: 0x060029AF RID: 10671 RVA: 0x0009171C File Offset: 0x0008F91C
		public override long MinimumTransmissionTimeout
		{
			get
			{
				return this.Get("RtoMin");
			}
		}

		// Token: 0x17000A30 RID: 2608
		// (get) Token: 0x060029B0 RID: 10672 RVA: 0x00091729 File Offset: 0x0008F929
		public override long ResetConnections
		{
			get
			{
				return this.Get("EstabResets");
			}
		}

		// Token: 0x17000A31 RID: 2609
		// (get) Token: 0x060029B1 RID: 10673 RVA: 0x00091736 File Offset: 0x0008F936
		public override long ResetsSent
		{
			get
			{
				return this.Get("OutRsts");
			}
		}

		// Token: 0x17000A32 RID: 2610
		// (get) Token: 0x060029B2 RID: 10674 RVA: 0x00091743 File Offset: 0x0008F943
		public override long SegmentsReceived
		{
			get
			{
				return this.Get("InSegs");
			}
		}

		// Token: 0x17000A33 RID: 2611
		// (get) Token: 0x060029B3 RID: 10675 RVA: 0x00091750 File Offset: 0x0008F950
		public override long SegmentsResent
		{
			get
			{
				return this.Get("RetransSegs");
			}
		}

		// Token: 0x17000A34 RID: 2612
		// (get) Token: 0x060029B4 RID: 10676 RVA: 0x0009175D File Offset: 0x0008F95D
		public override long SegmentsSent
		{
			get
			{
				return this.Get("OutSegs");
			}
		}

		// Token: 0x040020D1 RID: 8401
		private StringDictionary dic;
	}
}
