﻿using System;

namespace System.Net.NetworkInformation
{
	/// <summary>Provides Internet Protocol (IP) statistical data for an network interface on the local computer.</summary>
	// Token: 0x0200049E RID: 1182
	public abstract class IPInterfaceStatistics
	{
		/// <summary>Gets the number of bytes that were received on the interface.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of bytes that were received on the interface.</returns>
		// Token: 0x1700085D RID: 2141
		// (get) Token: 0x06002667 RID: 9831
		public abstract long BytesReceived { get; }

		/// <summary>Gets the number of bytes that were sent on the interface.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of bytes that were sent on the interface.</returns>
		// Token: 0x1700085E RID: 2142
		// (get) Token: 0x06002668 RID: 9832
		public abstract long BytesSent { get; }

		/// <summary>Gets the number of incoming packets that were discarded.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of incoming packets that were discarded.</returns>
		// Token: 0x1700085F RID: 2143
		// (get) Token: 0x06002669 RID: 9833
		public abstract long IncomingPacketsDiscarded { get; }

		/// <summary>Gets the number of incoming packets with errors.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of incoming packets with errors.</returns>
		// Token: 0x17000860 RID: 2144
		// (get) Token: 0x0600266A RID: 9834
		public abstract long IncomingPacketsWithErrors { get; }

		/// <summary>Gets the number of incoming packets with an unknown protocol that were received on the interface.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of incoming packets with an unknown protocol that were received on the interface.</returns>
		// Token: 0x17000861 RID: 2145
		// (get) Token: 0x0600266B RID: 9835
		public abstract long IncomingUnknownProtocolPackets { get; }

		/// <summary>Gets the number of non-unicast packets that were received on the interface.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of incoming non-unicast packets received on the interface.</returns>
		// Token: 0x17000862 RID: 2146
		// (get) Token: 0x0600266C RID: 9836
		public abstract long NonUnicastPacketsReceived { get; }

		/// <summary>Gets the number of non-unicast packets that were sent on the interface.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of non-unicast packets that were sent on the interface.</returns>
		// Token: 0x17000863 RID: 2147
		// (get) Token: 0x0600266D RID: 9837
		public abstract long NonUnicastPacketsSent { get; }

		/// <summary>Gets the number of outgoing packets that were discarded.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of outgoing packets that were discarded.</returns>
		// Token: 0x17000864 RID: 2148
		// (get) Token: 0x0600266E RID: 9838
		public abstract long OutgoingPacketsDiscarded { get; }

		/// <summary>Gets the number of outgoing packets with errors.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of outgoing packets with errors.</returns>
		// Token: 0x17000865 RID: 2149
		// (get) Token: 0x0600266F RID: 9839
		public abstract long OutgoingPacketsWithErrors { get; }

		/// <summary>Gets the length of the output queue.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of packets in the output queue.</returns>
		// Token: 0x17000866 RID: 2150
		// (get) Token: 0x06002670 RID: 9840
		public abstract long OutputQueueLength { get; }

		/// <summary>Gets the number of unicast packets that were received on the interface.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of unicast packets that were received on the interface.</returns>
		// Token: 0x17000867 RID: 2151
		// (get) Token: 0x06002671 RID: 9841
		public abstract long UnicastPacketsReceived { get; }

		/// <summary>Gets the number of unicast packets that were sent on the interface.</summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The total number of unicast packets that were sent on the interface.</returns>
		// Token: 0x17000868 RID: 2152
		// (get) Token: 0x06002672 RID: 9842
		public abstract long UnicastPacketsSent { get; }

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkInformation.IPInterfaceStatistics" /> class.</summary>
		// Token: 0x06002673 RID: 9843 RVA: 0x0000232F File Offset: 0x0000052F
		protected IPInterfaceStatistics()
		{
		}
	}
}
