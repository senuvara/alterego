﻿using System;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000526 RID: 1318
	internal struct Win32_SOCKET_ADDRESS
	{
		// Token: 0x060029F2 RID: 10738 RVA: 0x00091B60 File Offset: 0x0008FD60
		public IPAddress GetIPAddress()
		{
			Win32_SOCKADDR win32_SOCKADDR = (Win32_SOCKADDR)Marshal.PtrToStructure(this.Sockaddr, typeof(Win32_SOCKADDR));
			byte[] array;
			if (win32_SOCKADDR.AddressFamily == 23)
			{
				array = new byte[16];
				Array.Copy(win32_SOCKADDR.AddressData, 6, array, 0, 16);
			}
			else
			{
				array = new byte[4];
				Array.Copy(win32_SOCKADDR.AddressData, 2, array, 0, 4);
			}
			return new IPAddress(array);
		}

		// Token: 0x0400217D RID: 8573
		public IntPtr Sockaddr;

		// Token: 0x0400217E RID: 8574
		public int SockaddrLength;

		// Token: 0x0400217F RID: 8575
		private const int AF_INET6 = 23;
	}
}
