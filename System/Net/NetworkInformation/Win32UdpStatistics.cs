﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000513 RID: 1299
	internal class Win32UdpStatistics : UdpStatistics
	{
		// Token: 0x060029CB RID: 10699 RVA: 0x000918B7 File Offset: 0x0008FAB7
		public Win32UdpStatistics(Win32_MIB_UDPSTATS info)
		{
			this.info = info;
		}

		// Token: 0x17000A48 RID: 2632
		// (get) Token: 0x060029CC RID: 10700 RVA: 0x000918C6 File Offset: 0x0008FAC6
		public override long DatagramsReceived
		{
			get
			{
				return (long)((ulong)this.info.InDatagrams);
			}
		}

		// Token: 0x17000A49 RID: 2633
		// (get) Token: 0x060029CD RID: 10701 RVA: 0x000918D4 File Offset: 0x0008FAD4
		public override long DatagramsSent
		{
			get
			{
				return (long)((ulong)this.info.OutDatagrams);
			}
		}

		// Token: 0x17000A4A RID: 2634
		// (get) Token: 0x060029CE RID: 10702 RVA: 0x000918E2 File Offset: 0x0008FAE2
		public override long IncomingDatagramsDiscarded
		{
			get
			{
				return (long)((ulong)this.info.NoPorts);
			}
		}

		// Token: 0x17000A4B RID: 2635
		// (get) Token: 0x060029CF RID: 10703 RVA: 0x000918F0 File Offset: 0x0008FAF0
		public override long IncomingDatagramsWithErrors
		{
			get
			{
				return (long)((ulong)this.info.InErrors);
			}
		}

		// Token: 0x17000A4C RID: 2636
		// (get) Token: 0x060029D0 RID: 10704 RVA: 0x000918FE File Offset: 0x0008FAFE
		public override int UdpListeners
		{
			get
			{
				return this.info.NumAddrs;
			}
		}

		// Token: 0x040020E3 RID: 8419
		private Win32_MIB_UDPSTATS info;
	}
}
