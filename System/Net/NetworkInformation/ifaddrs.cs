﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004EB RID: 1259
	internal struct ifaddrs
	{
		// Token: 0x0400202E RID: 8238
		public IntPtr ifa_next;

		// Token: 0x0400202F RID: 8239
		public string ifa_name;

		// Token: 0x04002030 RID: 8240
		public uint ifa_flags;

		// Token: 0x04002031 RID: 8241
		public IntPtr ifa_addr;

		// Token: 0x04002032 RID: 8242
		public IntPtr ifa_netmask;

		// Token: 0x04002033 RID: 8243
		public ifa_ifu ifa_ifu;

		// Token: 0x04002034 RID: 8244
		public IntPtr ifa_data;
	}
}
