﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004E4 RID: 1252
	internal struct Win32_MIBICMPSTATS
	{
		// Token: 0x04002008 RID: 8200
		public uint Msgs;

		// Token: 0x04002009 RID: 8201
		public uint Errors;

		// Token: 0x0400200A RID: 8202
		public uint DestUnreachs;

		// Token: 0x0400200B RID: 8203
		public uint TimeExcds;

		// Token: 0x0400200C RID: 8204
		public uint ParmProbs;

		// Token: 0x0400200D RID: 8205
		public uint SrcQuenchs;

		// Token: 0x0400200E RID: 8206
		public uint Redirects;

		// Token: 0x0400200F RID: 8207
		public uint Echos;

		// Token: 0x04002010 RID: 8208
		public uint EchoReps;

		// Token: 0x04002011 RID: 8209
		public uint Timestamps;

		// Token: 0x04002012 RID: 8210
		public uint TimestampReps;

		// Token: 0x04002013 RID: 8211
		public uint AddrMasks;

		// Token: 0x04002014 RID: 8212
		public uint AddrMaskReps;
	}
}
