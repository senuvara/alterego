﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004ED RID: 1261
	internal struct sockaddr_in6
	{
		// Token: 0x04002038 RID: 8248
		public ushort sin6_family;

		// Token: 0x04002039 RID: 8249
		public ushort sin6_port;

		// Token: 0x0400203A RID: 8250
		public uint sin6_flowinfo;

		// Token: 0x0400203B RID: 8251
		public in6_addr sin6_addr;

		// Token: 0x0400203C RID: 8252
		public uint sin6_scope_id;
	}
}
