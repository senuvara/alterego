﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Net.NetworkInformation
{
	/// <summary>Provides data for the <see cref="E:System.Net.NetworkInformation.Ping.PingCompleted" /> event.</summary>
	// Token: 0x0200050D RID: 1293
	public class PingCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x0600299E RID: 10654 RVA: 0x00091661 File Offset: 0x0008F861
		internal PingCompletedEventArgs(Exception ex, bool cancelled, object userState, PingReply reply) : base(ex, cancelled, userState)
		{
			this.reply = reply;
		}

		/// <summary>Gets an object that contains data that describes an attempt to send an Internet Control Message Protocol (ICMP) echo request message and receive a corresponding ICMP echo reply message.</summary>
		/// <returns>A <see cref="T:System.Net.NetworkInformation.PingReply" /> object that describes the results of the ICMP echo request.</returns>
		// Token: 0x17000A26 RID: 2598
		// (get) Token: 0x0600299F RID: 10655 RVA: 0x00091674 File Offset: 0x0008F874
		public PingReply Reply
		{
			get
			{
				return this.reply;
			}
		}

		// Token: 0x060029A0 RID: 10656 RVA: 0x000092E2 File Offset: 0x000074E2
		internal PingCompletedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040020D0 RID: 8400
		private PingReply reply;
	}
}
