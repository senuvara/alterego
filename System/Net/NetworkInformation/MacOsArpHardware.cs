﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004F1 RID: 1265
	internal enum MacOsArpHardware
	{
		// Token: 0x04002058 RID: 8280
		ETHER = 6,
		// Token: 0x04002059 RID: 8281
		ATM = 37,
		// Token: 0x0400205A RID: 8282
		SLIP = 28,
		// Token: 0x0400205B RID: 8283
		PPP = 23,
		// Token: 0x0400205C RID: 8284
		LOOPBACK,
		// Token: 0x0400205D RID: 8285
		FDDI = 15
	}
}
