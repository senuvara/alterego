﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation.MacOsStructs;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004FD RID: 1277
	internal abstract class NetworkInterfaceFactory
	{
		// Token: 0x0600291D RID: 10525
		public abstract NetworkInterface[] GetAllNetworkInterfaces();

		// Token: 0x0600291E RID: 10526
		public abstract int GetLoopbackInterfaceIndex();

		// Token: 0x0600291F RID: 10527
		public abstract IPAddress GetNetMask(IPAddress address);

		// Token: 0x06002920 RID: 10528 RVA: 0x0008F7A4 File Offset: 0x0008D9A4
		public static NetworkInterfaceFactory Create()
		{
			if (Environment.OSVersion.Platform == PlatformID.Unix)
			{
				if (Platform.IsMacOS || Platform.IsFreeBSD)
				{
					return new NetworkInterfaceFactory.MacOsNetworkInterfaceAPI();
				}
				return new NetworkInterfaceFactory.LinuxNetworkInterfaceAPI();
			}
			else
			{
				Version v = new Version(5, 1);
				if (Environment.OSVersion.Version >= v)
				{
					return new NetworkInterfaceFactory.Win32NetworkInterfaceAPI();
				}
				throw new NotImplementedException();
			}
		}

		// Token: 0x06002921 RID: 10529 RVA: 0x0000232F File Offset: 0x0000052F
		protected NetworkInterfaceFactory()
		{
		}

		// Token: 0x020004FE RID: 1278
		internal abstract class UnixNetworkInterfaceAPI : NetworkInterfaceFactory
		{
			// Token: 0x06002922 RID: 10530
			[DllImport("libc")]
			public static extern int if_nametoindex(string ifname);

			// Token: 0x06002923 RID: 10531
			[DllImport("libc")]
			protected static extern int getifaddrs(out IntPtr ifap);

			// Token: 0x06002924 RID: 10532
			[DllImport("libc")]
			protected static extern void freeifaddrs(IntPtr ifap);

			// Token: 0x06002925 RID: 10533 RVA: 0x0008F7FF File Offset: 0x0008D9FF
			protected UnixNetworkInterfaceAPI()
			{
			}
		}

		// Token: 0x020004FF RID: 1279
		private class MacOsNetworkInterfaceAPI : NetworkInterfaceFactory.UnixNetworkInterfaceAPI
		{
			// Token: 0x06002926 RID: 10534 RVA: 0x0008F808 File Offset: 0x0008DA08
			public override NetworkInterface[] GetAllNetworkInterfaces()
			{
				Dictionary<string, MacOsNetworkInterface> dictionary = new Dictionary<string, MacOsNetworkInterface>();
				IntPtr intPtr;
				if (NetworkInterfaceFactory.UnixNetworkInterfaceAPI.getifaddrs(out intPtr) != 0)
				{
					throw new SystemException("getifaddrs() failed");
				}
				try
				{
					IntPtr intPtr2 = intPtr;
					while (intPtr2 != IntPtr.Zero)
					{
						ifaddrs ifaddrs = (ifaddrs)Marshal.PtrToStructure(intPtr2, typeof(ifaddrs));
						IPAddress ipaddress = IPAddress.None;
						string ifa_name = ifaddrs.ifa_name;
						int index = -1;
						byte[] array = null;
						NetworkInterfaceType networkInterfaceType = NetworkInterfaceType.Unknown;
						if (ifaddrs.ifa_addr != IntPtr.Zero)
						{
							sockaddr sockaddr = (sockaddr)Marshal.PtrToStructure(ifaddrs.ifa_addr, typeof(sockaddr));
							if (sockaddr.sa_family == 30)
							{
								sockaddr_in6 sockaddr_in = (sockaddr_in6)Marshal.PtrToStructure(ifaddrs.ifa_addr, typeof(sockaddr_in6));
								ipaddress = new IPAddress(sockaddr_in.sin6_addr.u6_addr8, (long)((ulong)sockaddr_in.sin6_scope_id));
							}
							else if (sockaddr.sa_family == 2)
							{
								ipaddress = new IPAddress((long)((ulong)((sockaddr_in)Marshal.PtrToStructure(ifaddrs.ifa_addr, typeof(sockaddr_in))).sin_addr));
							}
							else if (sockaddr.sa_family == 18)
							{
								sockaddr_dl sockaddr_dl = default(sockaddr_dl);
								sockaddr_dl.Read(ifaddrs.ifa_addr);
								array = new byte[(int)sockaddr_dl.sdl_alen];
								Array.Copy(sockaddr_dl.sdl_data, (int)sockaddr_dl.sdl_nlen, array, 0, Math.Min(array.Length, sockaddr_dl.sdl_data.Length - (int)sockaddr_dl.sdl_nlen));
								index = (int)sockaddr_dl.sdl_index;
								int sdl_type = (int)sockaddr_dl.sdl_type;
								if (Enum.IsDefined(typeof(MacOsArpHardware), sdl_type))
								{
									MacOsArpHardware macOsArpHardware = (MacOsArpHardware)sdl_type;
									if (macOsArpHardware <= MacOsArpHardware.PPP)
									{
										if (macOsArpHardware != MacOsArpHardware.ETHER)
										{
											if (macOsArpHardware != MacOsArpHardware.FDDI)
											{
												if (macOsArpHardware == MacOsArpHardware.PPP)
												{
													networkInterfaceType = NetworkInterfaceType.Ppp;
												}
											}
											else
											{
												networkInterfaceType = NetworkInterfaceType.Fddi;
											}
										}
										else
										{
											networkInterfaceType = NetworkInterfaceType.Ethernet;
										}
									}
									else if (macOsArpHardware != MacOsArpHardware.LOOPBACK)
									{
										if (macOsArpHardware != MacOsArpHardware.SLIP)
										{
											if (macOsArpHardware == MacOsArpHardware.ATM)
											{
												networkInterfaceType = NetworkInterfaceType.Atm;
											}
										}
										else
										{
											networkInterfaceType = NetworkInterfaceType.Slip;
										}
									}
									else
									{
										networkInterfaceType = NetworkInterfaceType.Loopback;
										array = null;
									}
								}
							}
						}
						MacOsNetworkInterface macOsNetworkInterface = null;
						if (!dictionary.TryGetValue(ifa_name, out macOsNetworkInterface))
						{
							macOsNetworkInterface = new MacOsNetworkInterface(ifa_name, ifaddrs.ifa_flags);
							dictionary.Add(ifa_name, macOsNetworkInterface);
						}
						if (!ipaddress.Equals(IPAddress.None))
						{
							macOsNetworkInterface.AddAddress(ipaddress);
						}
						if (array != null || networkInterfaceType == NetworkInterfaceType.Loopback)
						{
							macOsNetworkInterface.SetLinkLayerInfo(index, array, networkInterfaceType);
						}
						intPtr2 = ifaddrs.ifa_next;
					}
				}
				finally
				{
					NetworkInterfaceFactory.UnixNetworkInterfaceAPI.freeifaddrs(intPtr);
				}
				NetworkInterface[] array2 = new NetworkInterface[dictionary.Count];
				int num = 0;
				foreach (NetworkInterface networkInterface in dictionary.Values)
				{
					array2[num] = networkInterface;
					num++;
				}
				return array2;
			}

			// Token: 0x06002927 RID: 10535 RVA: 0x0008FAF4 File Offset: 0x0008DCF4
			public override int GetLoopbackInterfaceIndex()
			{
				return NetworkInterfaceFactory.UnixNetworkInterfaceAPI.if_nametoindex("lo0");
			}

			// Token: 0x06002928 RID: 10536 RVA: 0x0008FB00 File Offset: 0x0008DD00
			public override IPAddress GetNetMask(IPAddress address)
			{
				IntPtr intPtr;
				if (NetworkInterfaceFactory.UnixNetworkInterfaceAPI.getifaddrs(out intPtr) != 0)
				{
					throw new SystemException("getifaddrs() failed");
				}
				try
				{
					IntPtr intPtr2 = intPtr;
					while (intPtr2 != IntPtr.Zero)
					{
						ifaddrs ifaddrs = (ifaddrs)Marshal.PtrToStructure(intPtr2, typeof(ifaddrs));
						if (ifaddrs.ifa_addr != IntPtr.Zero && ((sockaddr)Marshal.PtrToStructure(ifaddrs.ifa_addr, typeof(sockaddr))).sa_family == 2)
						{
							IPAddress obj = new IPAddress((long)((ulong)((sockaddr_in)Marshal.PtrToStructure(ifaddrs.ifa_addr, typeof(sockaddr_in))).sin_addr));
							if (address.Equals(obj))
							{
								return new IPAddress((long)((ulong)((sockaddr_in)Marshal.PtrToStructure(ifaddrs.ifa_netmask, typeof(sockaddr_in))).sin_addr));
							}
						}
						intPtr2 = ifaddrs.ifa_next;
					}
				}
				finally
				{
					NetworkInterfaceFactory.UnixNetworkInterfaceAPI.freeifaddrs(intPtr);
				}
				return null;
			}

			// Token: 0x06002929 RID: 10537 RVA: 0x0008FC00 File Offset: 0x0008DE00
			public MacOsNetworkInterfaceAPI()
			{
			}

			// Token: 0x04002099 RID: 8345
			private const int AF_INET = 2;

			// Token: 0x0400209A RID: 8346
			private const int AF_INET6 = 30;

			// Token: 0x0400209B RID: 8347
			private const int AF_LINK = 18;
		}

		// Token: 0x02000500 RID: 1280
		private class LinuxNetworkInterfaceAPI : NetworkInterfaceFactory.UnixNetworkInterfaceAPI
		{
			// Token: 0x0600292A RID: 10538 RVA: 0x0008FC08 File Offset: 0x0008DE08
			private static void FreeInterfaceAddresses(IntPtr ifap)
			{
				NetworkInterfaceFactory.UnixNetworkInterfaceAPI.freeifaddrs(ifap);
			}

			// Token: 0x0600292B RID: 10539 RVA: 0x0008FC10 File Offset: 0x0008DE10
			private static int GetInterfaceAddresses(out IntPtr ifap)
			{
				return NetworkInterfaceFactory.UnixNetworkInterfaceAPI.getifaddrs(out ifap);
			}

			// Token: 0x0600292C RID: 10540 RVA: 0x0008FC18 File Offset: 0x0008DE18
			public override NetworkInterface[] GetAllNetworkInterfaces()
			{
				Dictionary<string, LinuxNetworkInterface> dictionary = new Dictionary<string, LinuxNetworkInterface>();
				IntPtr intPtr;
				if (NetworkInterfaceFactory.LinuxNetworkInterfaceAPI.GetInterfaceAddresses(out intPtr) != 0)
				{
					throw new SystemException("getifaddrs() failed");
				}
				try
				{
					IntPtr intPtr2 = intPtr;
					while (intPtr2 != IntPtr.Zero)
					{
						ifaddrs ifaddrs = (ifaddrs)Marshal.PtrToStructure(intPtr2, typeof(ifaddrs));
						IPAddress ipaddress = IPAddress.None;
						string text = ifaddrs.ifa_name;
						int index = -1;
						byte[] array = null;
						NetworkInterfaceType networkInterfaceType = NetworkInterfaceType.Unknown;
						int num = 0;
						if (ifaddrs.ifa_addr != IntPtr.Zero)
						{
							sockaddr_in sockaddr_in = (sockaddr_in)Marshal.PtrToStructure(ifaddrs.ifa_addr, typeof(sockaddr_in));
							if (sockaddr_in.sin_family == 10)
							{
								sockaddr_in6 sockaddr_in2 = (sockaddr_in6)Marshal.PtrToStructure(ifaddrs.ifa_addr, typeof(sockaddr_in6));
								ipaddress = new IPAddress(sockaddr_in2.sin6_addr.u6_addr8, (long)((ulong)sockaddr_in2.sin6_scope_id));
							}
							else if (sockaddr_in.sin_family == 2)
							{
								ipaddress = new IPAddress((long)((ulong)sockaddr_in.sin_addr));
							}
							else if (sockaddr_in.sin_family == 17)
							{
								sockaddr_ll sockaddr_ll = (sockaddr_ll)Marshal.PtrToStructure(ifaddrs.ifa_addr, typeof(sockaddr_ll));
								if ((int)sockaddr_ll.sll_halen > sockaddr_ll.sll_addr.Length)
								{
									intPtr2 = ifaddrs.ifa_next;
									continue;
								}
								array = new byte[(int)sockaddr_ll.sll_halen];
								Array.Copy(sockaddr_ll.sll_addr, 0, array, 0, array.Length);
								index = sockaddr_ll.sll_ifindex;
								int sll_hatype = (int)sockaddr_ll.sll_hatype;
								if (Enum.IsDefined(typeof(LinuxArpHardware), sll_hatype))
								{
									LinuxArpHardware linuxArpHardware = (LinuxArpHardware)sll_hatype;
									if (linuxArpHardware <= LinuxArpHardware.CSLIP6)
									{
										switch (linuxArpHardware)
										{
										case LinuxArpHardware.ETHER:
										case LinuxArpHardware.EETHER:
											networkInterfaceType = NetworkInterfaceType.Ethernet;
											break;
										case (LinuxArpHardware)3:
											break;
										case LinuxArpHardware.PRONET:
											networkInterfaceType = NetworkInterfaceType.TokenRing;
											break;
										default:
											if (linuxArpHardware != LinuxArpHardware.ATM)
											{
												if (linuxArpHardware - LinuxArpHardware.SLIP <= 3)
												{
													networkInterfaceType = NetworkInterfaceType.Slip;
												}
											}
											else
											{
												networkInterfaceType = NetworkInterfaceType.Atm;
											}
											break;
										}
									}
									else if (linuxArpHardware != LinuxArpHardware.PPP)
									{
										switch (linuxArpHardware)
										{
										case LinuxArpHardware.TUNNEL:
										case LinuxArpHardware.TUNNEL6:
										case LinuxArpHardware.SIT:
										case LinuxArpHardware.IPDDP:
										case LinuxArpHardware.IPGRE:
											break;
										case (LinuxArpHardware)770:
										case (LinuxArpHardware)771:
										case (LinuxArpHardware)773:
										case (LinuxArpHardware)775:
											goto IL_246;
										case LinuxArpHardware.LOOPBACK:
											networkInterfaceType = NetworkInterfaceType.Loopback;
											array = null;
											goto IL_246;
										case LinuxArpHardware.FDDI:
											networkInterfaceType = NetworkInterfaceType.Fddi;
											goto IL_246;
										default:
											if (linuxArpHardware != LinuxArpHardware.IP6GRE)
											{
												goto IL_246;
											}
											break;
										}
										networkInterfaceType = NetworkInterfaceType.Tunnel;
									}
									else
									{
										networkInterfaceType = NetworkInterfaceType.Ppp;
									}
								}
							}
						}
						IL_246:
						LinuxNetworkInterface linuxNetworkInterface = null;
						if (string.IsNullOrEmpty(text))
						{
							text = "\0" + (num + 1).ToString();
						}
						if (!dictionary.TryGetValue(text, out linuxNetworkInterface))
						{
							linuxNetworkInterface = new LinuxNetworkInterface(text);
							dictionary.Add(text, linuxNetworkInterface);
						}
						if (!ipaddress.Equals(IPAddress.None))
						{
							linuxNetworkInterface.AddAddress(ipaddress);
						}
						if (array != null || networkInterfaceType == NetworkInterfaceType.Loopback)
						{
							if (networkInterfaceType == NetworkInterfaceType.Ethernet && Directory.Exists(linuxNetworkInterface.IfacePath + "wireless"))
							{
								networkInterfaceType = NetworkInterfaceType.Wireless80211;
							}
							linuxNetworkInterface.SetLinkLayerInfo(index, array, networkInterfaceType);
						}
						intPtr2 = ifaddrs.ifa_next;
					}
				}
				finally
				{
					NetworkInterfaceFactory.LinuxNetworkInterfaceAPI.FreeInterfaceAddresses(intPtr);
				}
				NetworkInterface[] array2 = new NetworkInterface[dictionary.Count];
				int num2 = 0;
				foreach (NetworkInterface networkInterface in dictionary.Values)
				{
					array2[num2] = networkInterface;
					num2++;
				}
				return array2;
			}

			// Token: 0x0600292D RID: 10541 RVA: 0x0008FFA4 File Offset: 0x0008E1A4
			public override int GetLoopbackInterfaceIndex()
			{
				return NetworkInterfaceFactory.UnixNetworkInterfaceAPI.if_nametoindex("lo");
			}

			// Token: 0x0600292E RID: 10542 RVA: 0x0008FFB0 File Offset: 0x0008E1B0
			public override IPAddress GetNetMask(IPAddress address)
			{
				foreach (ifaddrs ifaddrs in NetworkInterfaceFactory.LinuxNetworkInterfaceAPI.GetNetworkInterfaces())
				{
					if (!(ifaddrs.ifa_addr == IntPtr.Zero))
					{
						sockaddr_in sockaddr_in = (sockaddr_in)Marshal.PtrToStructure(ifaddrs.ifa_addr, typeof(sockaddr_in));
						if (sockaddr_in.sin_family == 2 && address.Equals(new IPAddress((long)((ulong)sockaddr_in.sin_addr))))
						{
							return new IPAddress((long)((ulong)((sockaddr_in)Marshal.PtrToStructure(ifaddrs.ifa_netmask, typeof(sockaddr_in))).sin_addr));
						}
					}
				}
				return null;
			}

			// Token: 0x0600292F RID: 10543 RVA: 0x00090070 File Offset: 0x0008E270
			private static IEnumerable<ifaddrs> GetNetworkInterfaces()
			{
				IntPtr ifap = IntPtr.Zero;
				try
				{
					if (NetworkInterfaceFactory.LinuxNetworkInterfaceAPI.GetInterfaceAddresses(out ifap) != 0)
					{
						yield break;
					}
					IntPtr intPtr = ifap;
					while (intPtr != IntPtr.Zero)
					{
						ifaddrs addr = (ifaddrs)Marshal.PtrToStructure(intPtr, typeof(ifaddrs));
						yield return addr;
						intPtr = addr.ifa_next;
						addr = default(ifaddrs);
					}
				}
				finally
				{
					if (ifap != IntPtr.Zero)
					{
						NetworkInterfaceFactory.LinuxNetworkInterfaceAPI.FreeInterfaceAddresses(ifap);
					}
				}
				yield break;
				yield break;
			}

			// Token: 0x06002930 RID: 10544 RVA: 0x0008FC00 File Offset: 0x0008DE00
			public LinuxNetworkInterfaceAPI()
			{
			}

			// Token: 0x0400209C RID: 8348
			private const int AF_INET = 2;

			// Token: 0x0400209D RID: 8349
			private const int AF_INET6 = 10;

			// Token: 0x0400209E RID: 8350
			private const int AF_PACKET = 17;

			// Token: 0x02000501 RID: 1281
			[CompilerGenerated]
			private sealed class <GetNetworkInterfaces>d__8 : IEnumerable<ifaddrs>, IEnumerable, IEnumerator<ifaddrs>, IDisposable, IEnumerator
			{
				// Token: 0x06002931 RID: 10545 RVA: 0x00090079 File Offset: 0x0008E279
				[DebuggerHidden]
				public <GetNetworkInterfaces>d__8(int <>1__state)
				{
					this.<>1__state = <>1__state;
					this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
				}

				// Token: 0x06002932 RID: 10546 RVA: 0x00090094 File Offset: 0x0008E294
				[DebuggerHidden]
				void IDisposable.Dispose()
				{
					int num = this.<>1__state;
					if (num == -3 || num == 1)
					{
						try
						{
						}
						finally
						{
							this.<>m__Finally1();
						}
					}
				}

				// Token: 0x06002933 RID: 10547 RVA: 0x000900CC File Offset: 0x0008E2CC
				bool IEnumerator.MoveNext()
				{
					bool result;
					try
					{
						int num = this.<>1__state;
						IntPtr intPtr;
						if (num != 0)
						{
							if (num != 1)
							{
								return false;
							}
							this.<>1__state = -3;
							intPtr = addr.ifa_next;
							addr = default(ifaddrs);
						}
						else
						{
							this.<>1__state = -1;
							ifap = IntPtr.Zero;
							this.<>1__state = -3;
							if (NetworkInterfaceFactory.LinuxNetworkInterfaceAPI.GetInterfaceAddresses(out ifap) != 0)
							{
								result = false;
								this.<>m__Finally1();
								return result;
							}
							intPtr = ifap;
						}
						if (!(intPtr != IntPtr.Zero))
						{
							this.<>m__Finally1();
							result = false;
						}
						else
						{
							addr = (ifaddrs)Marshal.PtrToStructure(intPtr, typeof(ifaddrs));
							this.<>2__current = addr;
							this.<>1__state = 1;
							result = true;
						}
					}
					catch
					{
						this.System.IDisposable.Dispose();
						throw;
					}
					return result;
				}

				// Token: 0x06002934 RID: 10548 RVA: 0x000901B0 File Offset: 0x0008E3B0
				private void <>m__Finally1()
				{
					this.<>1__state = -1;
					if (ifap != IntPtr.Zero)
					{
						NetworkInterfaceFactory.LinuxNetworkInterfaceAPI.FreeInterfaceAddresses(ifap);
					}
				}

				// Token: 0x17000A0A RID: 2570
				// (get) Token: 0x06002935 RID: 10549 RVA: 0x000901D6 File Offset: 0x0008E3D6
				ifaddrs IEnumerator<ifaddrs>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06002936 RID: 10550 RVA: 0x00006740 File Offset: 0x00004940
				[DebuggerHidden]
				void IEnumerator.Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x17000A0B RID: 2571
				// (get) Token: 0x06002937 RID: 10551 RVA: 0x000901DE File Offset: 0x0008E3DE
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06002938 RID: 10552 RVA: 0x000901EC File Offset: 0x0008E3EC
				[DebuggerHidden]
				IEnumerator<ifaddrs> IEnumerable<ifaddrs>.GetEnumerator()
				{
					NetworkInterfaceFactory.LinuxNetworkInterfaceAPI.<GetNetworkInterfaces>d__8 result;
					if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
					{
						this.<>1__state = 0;
						result = this;
					}
					else
					{
						result = new NetworkInterfaceFactory.LinuxNetworkInterfaceAPI.<GetNetworkInterfaces>d__8(0);
					}
					return result;
				}

				// Token: 0x06002939 RID: 10553 RVA: 0x00090223 File Offset: 0x0008E423
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<System.Net.NetworkInformation.ifaddrs>.GetEnumerator();
				}

				// Token: 0x0400209F RID: 8351
				private int <>1__state;

				// Token: 0x040020A0 RID: 8352
				private ifaddrs <>2__current;

				// Token: 0x040020A1 RID: 8353
				private int <>l__initialThreadId;

				// Token: 0x040020A2 RID: 8354
				private ifaddrs <addr>5__1;

				// Token: 0x040020A3 RID: 8355
				private IntPtr <ifap>5__2;
			}
		}

		// Token: 0x02000502 RID: 1282
		private class Win32NetworkInterfaceAPI : NetworkInterfaceFactory
		{
			// Token: 0x0600293A RID: 10554
			[DllImport("iphlpapi.dll", SetLastError = true)]
			private static extern int GetAdaptersAddresses(uint family, uint flags, IntPtr reserved, IntPtr info, ref int size);

			// Token: 0x0600293B RID: 10555
			[DllImport("iphlpapi.dll")]
			private static extern uint GetBestInterfaceEx(byte[] ipAddress, out int index);

			// Token: 0x0600293C RID: 10556 RVA: 0x0009022C File Offset: 0x0008E42C
			private static Win32_IP_ADAPTER_ADDRESSES[] GetAdaptersAddresses()
			{
				IntPtr intPtr = IntPtr.Zero;
				int num = 0;
				uint flags = 192U;
				NetworkInterfaceFactory.Win32NetworkInterfaceAPI.GetAdaptersAddresses(0U, flags, IntPtr.Zero, intPtr, ref num);
				if (Marshal.SizeOf(typeof(Win32_IP_ADAPTER_ADDRESSES)) > num)
				{
					throw new NetworkInformationException();
				}
				intPtr = Marshal.AllocHGlobal(num);
				int adaptersAddresses = NetworkInterfaceFactory.Win32NetworkInterfaceAPI.GetAdaptersAddresses(0U, flags, IntPtr.Zero, intPtr, ref num);
				if (adaptersAddresses != 0)
				{
					throw new NetworkInformationException(adaptersAddresses);
				}
				List<Win32_IP_ADAPTER_ADDRESSES> list = new List<Win32_IP_ADAPTER_ADDRESSES>();
				IntPtr intPtr2 = intPtr;
				while (intPtr2 != IntPtr.Zero)
				{
					Win32_IP_ADAPTER_ADDRESSES win32_IP_ADAPTER_ADDRESSES = Marshal.PtrToStructure<Win32_IP_ADAPTER_ADDRESSES>(intPtr2);
					list.Add(win32_IP_ADAPTER_ADDRESSES);
					intPtr2 = win32_IP_ADAPTER_ADDRESSES.Next;
				}
				return list.ToArray();
			}

			// Token: 0x0600293D RID: 10557 RVA: 0x000902CC File Offset: 0x0008E4CC
			public override NetworkInterface[] GetAllNetworkInterfaces()
			{
				Win32_IP_ADAPTER_ADDRESSES[] adaptersAddresses = NetworkInterfaceFactory.Win32NetworkInterfaceAPI.GetAdaptersAddresses();
				NetworkInterface[] array = new NetworkInterface[adaptersAddresses.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = new Win32NetworkInterface2(adaptersAddresses[i]);
				}
				return array;
			}

			// Token: 0x0600293E RID: 10558 RVA: 0x00090308 File Offset: 0x0008E508
			private static int GetBestInterfaceForAddress(IPAddress addr)
			{
				int result;
				int bestInterfaceEx = (int)NetworkInterfaceFactory.Win32NetworkInterfaceAPI.GetBestInterfaceEx(new SocketAddress(addr).m_Buffer, out result);
				if (bestInterfaceEx != 0)
				{
					throw new NetworkInformationException(bestInterfaceEx);
				}
				return result;
			}

			// Token: 0x0600293F RID: 10559 RVA: 0x00090333 File Offset: 0x0008E533
			public override int GetLoopbackInterfaceIndex()
			{
				return NetworkInterfaceFactory.Win32NetworkInterfaceAPI.GetBestInterfaceForAddress(IPAddress.Loopback);
			}

			// Token: 0x06002940 RID: 10560 RVA: 0x000068D7 File Offset: 0x00004AD7
			public override IPAddress GetNetMask(IPAddress address)
			{
				throw new NotImplementedException();
			}

			// Token: 0x06002941 RID: 10561 RVA: 0x0008F7FF File Offset: 0x0008D9FF
			public Win32NetworkInterfaceAPI()
			{
			}

			// Token: 0x040020A4 RID: 8356
			private const string IPHLPAPI = "iphlpapi.dll";
		}
	}
}
