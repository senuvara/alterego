﻿using System;
using System.Net.Sockets;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000516 RID: 1302
	internal class LinuxUnicastIPAddressInformation : UnicastIPAddressInformation
	{
		// Token: 0x060029DD RID: 10717 RVA: 0x00091A47 File Offset: 0x0008FC47
		public LinuxUnicastIPAddressInformation(IPAddress address)
		{
			this.address = address;
		}

		// Token: 0x17000A57 RID: 2647
		// (get) Token: 0x060029DE RID: 10718 RVA: 0x00091A56 File Offset: 0x0008FC56
		public override IPAddress Address
		{
			get
			{
				return this.address;
			}
		}

		// Token: 0x17000A58 RID: 2648
		// (get) Token: 0x060029DF RID: 10719 RVA: 0x00091A60 File Offset: 0x0008FC60
		public override bool IsDnsEligible
		{
			get
			{
				byte[] addressBytes = this.address.GetAddressBytes();
				return addressBytes[0] != 169 || addressBytes[1] != 254;
			}
		}

		// Token: 0x17000A59 RID: 2649
		// (get) Token: 0x060029E0 RID: 10720 RVA: 0x00005AFA File Offset: 0x00003CFA
		[MonoTODO("Always returns false")]
		public override bool IsTransient
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A5A RID: 2650
		// (get) Token: 0x060029E1 RID: 10721 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override long AddressPreferredLifetime
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A5B RID: 2651
		// (get) Token: 0x060029E2 RID: 10722 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override long AddressValidLifetime
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A5C RID: 2652
		// (get) Token: 0x060029E3 RID: 10723 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override long DhcpLeaseLifetime
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A5D RID: 2653
		// (get) Token: 0x060029E4 RID: 10724 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override DuplicateAddressDetectionState DuplicateAddressDetectionState
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A5E RID: 2654
		// (get) Token: 0x060029E5 RID: 10725 RVA: 0x00091A92 File Offset: 0x0008FC92
		public override IPAddress IPv4Mask
		{
			get
			{
				if (this.Address.AddressFamily != AddressFamily.InterNetwork)
				{
					return IPAddress.Any;
				}
				if (this.ipv4Mask == null)
				{
					this.ipv4Mask = SystemNetworkInterface.GetNetMask(this.address);
				}
				return this.ipv4Mask;
			}
		}

		// Token: 0x17000A5F RID: 2655
		// (get) Token: 0x060029E6 RID: 10726 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override PrefixOrigin PrefixOrigin
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A60 RID: 2656
		// (get) Token: 0x060029E7 RID: 10727 RVA: 0x000068D7 File Offset: 0x00004AD7
		public override SuffixOrigin SuffixOrigin
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x040020EB RID: 8427
		private IPAddress address;

		// Token: 0x040020EC RID: 8428
		private IPAddress ipv4Mask;
	}
}
