﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020004D3 RID: 1235
	internal struct Win32_MIB_IPSTATS
	{
		// Token: 0x04001FD6 RID: 8150
		public int Forwarding;

		// Token: 0x04001FD7 RID: 8151
		public int DefaultTTL;

		// Token: 0x04001FD8 RID: 8152
		public uint InReceives;

		// Token: 0x04001FD9 RID: 8153
		public uint InHdrErrors;

		// Token: 0x04001FDA RID: 8154
		public uint InAddrErrors;

		// Token: 0x04001FDB RID: 8155
		public uint ForwDatagrams;

		// Token: 0x04001FDC RID: 8156
		public uint InUnknownProtos;

		// Token: 0x04001FDD RID: 8157
		public uint InDiscards;

		// Token: 0x04001FDE RID: 8158
		public uint InDelivers;

		// Token: 0x04001FDF RID: 8159
		public uint OutRequests;

		// Token: 0x04001FE0 RID: 8160
		public uint RoutingDiscards;

		// Token: 0x04001FE1 RID: 8161
		public uint OutDiscards;

		// Token: 0x04001FE2 RID: 8162
		public uint OutNoRoutes;

		// Token: 0x04001FE3 RID: 8163
		public uint ReasmTimeout;

		// Token: 0x04001FE4 RID: 8164
		public uint ReasmReqds;

		// Token: 0x04001FE5 RID: 8165
		public uint ReasmOks;

		// Token: 0x04001FE6 RID: 8166
		public uint ReasmFails;

		// Token: 0x04001FE7 RID: 8167
		public uint FragOks;

		// Token: 0x04001FE8 RID: 8168
		public uint FragFails;

		// Token: 0x04001FE9 RID: 8169
		public uint FragCreates;

		// Token: 0x04001FEA RID: 8170
		public int NumIf;

		// Token: 0x04001FEB RID: 8171
		public int NumAddr;

		// Token: 0x04001FEC RID: 8172
		public int NumRoutes;
	}
}
