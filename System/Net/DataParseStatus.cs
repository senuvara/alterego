﻿using System;

namespace System.Net
{
	// Token: 0x02000344 RID: 836
	internal enum DataParseStatus
	{
		// Token: 0x04001716 RID: 5910
		NeedMoreData,
		// Token: 0x04001717 RID: 5911
		ContinueParsing,
		// Token: 0x04001718 RID: 5912
		Done,
		// Token: 0x04001719 RID: 5913
		Invalid,
		// Token: 0x0400171A RID: 5914
		DataTooBig
	}
}
