﻿using System;
using System.Collections;

namespace System.Net
{
	// Token: 0x02000378 RID: 888
	internal class Comparer : IComparer
	{
		// Token: 0x0600198D RID: 6541 RVA: 0x0005C9E4 File Offset: 0x0005ABE4
		int IComparer.Compare(object ol, object or)
		{
			Cookie cookie = (Cookie)ol;
			Cookie cookie2 = (Cookie)or;
			int result;
			if ((result = string.Compare(cookie.Name, cookie2.Name, StringComparison.OrdinalIgnoreCase)) != 0)
			{
				return result;
			}
			if ((result = string.Compare(cookie.Domain, cookie2.Domain, StringComparison.OrdinalIgnoreCase)) != 0)
			{
				return result;
			}
			if ((result = string.Compare(cookie.Path, cookie2.Path, StringComparison.Ordinal)) != 0)
			{
				return result;
			}
			return 0;
		}

		// Token: 0x0600198E RID: 6542 RVA: 0x0000232F File Offset: 0x0000052F
		public Comparer()
		{
		}
	}
}
