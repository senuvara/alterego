﻿using System;
using System.Collections.Specialized;
using System.Net.Security;
using System.Threading;

namespace System.Net
{
	/// <summary>Manages the collection of <see cref="T:System.Net.ServicePoint" /> objects.</summary>
	// Token: 0x02000400 RID: 1024
	public class ServicePointManager
	{
		// Token: 0x06001FB4 RID: 8116 RVA: 0x00072E20 File Offset: 0x00071020
		static ServicePointManager()
		{
		}

		// Token: 0x06001FB5 RID: 8117 RVA: 0x0000232F File Offset: 0x0000052F
		private ServicePointManager()
		{
		}

		/// <summary>Gets or sets policy for server certificates.</summary>
		/// <returns>An object that implements the <see cref="T:System.Net.ICertificatePolicy" /> interface.</returns>
		// Token: 0x170006D6 RID: 1750
		// (get) Token: 0x06001FB6 RID: 8118 RVA: 0x00072E6E File Offset: 0x0007106E
		// (set) Token: 0x06001FB7 RID: 8119 RVA: 0x00072E8D File Offset: 0x0007108D
		[Obsolete("Use ServerCertificateValidationCallback instead", false)]
		public static ICertificatePolicy CertificatePolicy
		{
			get
			{
				if (ServicePointManager.policy == null)
				{
					Interlocked.CompareExchange<ICertificatePolicy>(ref ServicePointManager.policy, new DefaultCertificatePolicy(), null);
				}
				return ServicePointManager.policy;
			}
			set
			{
				ServicePointManager.policy = value;
			}
		}

		// Token: 0x06001FB8 RID: 8120 RVA: 0x00072E95 File Offset: 0x00071095
		internal static ICertificatePolicy GetLegacyCertificatePolicy()
		{
			return ServicePointManager.policy;
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that indicates whether the certificate is checked against the certificate authority revocation list.</summary>
		/// <returns>
		///     <see langword="true" /> if the certificate revocation list is checked; otherwise, <see langword="false" />.</returns>
		// Token: 0x170006D7 RID: 1751
		// (get) Token: 0x06001FB9 RID: 8121 RVA: 0x00072E9C File Offset: 0x0007109C
		// (set) Token: 0x06001FBA RID: 8122 RVA: 0x00072EA3 File Offset: 0x000710A3
		[MonoTODO("CRL checks not implemented")]
		public static bool CheckCertificateRevocationList
		{
			get
			{
				return ServicePointManager._checkCRL;
			}
			set
			{
				ServicePointManager._checkCRL = false;
			}
		}

		/// <summary>Gets or sets the maximum number of concurrent connections allowed by a <see cref="T:System.Net.ServicePoint" /> object.</summary>
		/// <returns>The maximum number of concurrent connections allowed by a <see cref="T:System.Net.ServicePoint" /> object. The default value is 2. When an app is running as an ASP.NET host, it is not possible to alter the value of this property through the config file if the autoConfig property is set to <see langword="true" />. However, you can change the value programmatically when the autoConfig property is <see langword="true" />. Set your preferred value once, when the AppDomain loads.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <see cref="P:System.Net.ServicePointManager.DefaultConnectionLimit" /> is less than or equal to 0. </exception>
		// Token: 0x170006D8 RID: 1752
		// (get) Token: 0x06001FBB RID: 8123 RVA: 0x00072EAB File Offset: 0x000710AB
		// (set) Token: 0x06001FBC RID: 8124 RVA: 0x00072EB2 File Offset: 0x000710B2
		public static int DefaultConnectionLimit
		{
			get
			{
				return ServicePointManager.defaultConnectionLimit;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				ServicePointManager.defaultConnectionLimit = value;
			}
		}

		// Token: 0x06001FBD RID: 8125 RVA: 0x000659A3 File Offset: 0x00063BA3
		private static Exception GetMustImplement()
		{
			return new NotImplementedException();
		}

		/// <summary>Gets or sets a value that indicates how long a Domain Name Service (DNS) resolution is considered valid.</summary>
		/// <returns>The time-out value, in milliseconds. A value of -1 indicates an infinite time-out period. The default value is 120,000 milliseconds (two minutes).</returns>
		// Token: 0x170006D9 RID: 1753
		// (get) Token: 0x06001FBE RID: 8126 RVA: 0x00072EC9 File Offset: 0x000710C9
		// (set) Token: 0x06001FBF RID: 8127 RVA: 0x00072ED0 File Offset: 0x000710D0
		public static int DnsRefreshTimeout
		{
			get
			{
				return ServicePointManager.dnsRefreshTimeout;
			}
			set
			{
				ServicePointManager.dnsRefreshTimeout = Math.Max(-1, value);
			}
		}

		/// <summary>Gets or sets a value that indicates whether a Domain Name Service (DNS) resolution rotates among the applicable Internet Protocol (IP) addresses.</summary>
		/// <returns>
		///     <see langword="false" /> if a DNS resolution always returns the first IP address for a particular host; otherwise <see langword="true" />. The default is <see langword="false" />.</returns>
		// Token: 0x170006DA RID: 1754
		// (get) Token: 0x06001FC0 RID: 8128 RVA: 0x00072EDE File Offset: 0x000710DE
		// (set) Token: 0x06001FC1 RID: 8129 RVA: 0x00072EDE File Offset: 0x000710DE
		[MonoTODO]
		public static bool EnableDnsRoundRobin
		{
			get
			{
				throw ServicePointManager.GetMustImplement();
			}
			set
			{
				throw ServicePointManager.GetMustImplement();
			}
		}

		/// <summary>Gets or sets the maximum idle time of a <see cref="T:System.Net.ServicePoint" /> object.</summary>
		/// <returns>The maximum idle time, in milliseconds, of a <see cref="T:System.Net.ServicePoint" /> object. The default value is 100,000 milliseconds (100 seconds).</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <see cref="P:System.Net.ServicePointManager.MaxServicePointIdleTime" /> is less than <see cref="F:System.Threading.Timeout.Infinite" /> or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		// Token: 0x170006DB RID: 1755
		// (get) Token: 0x06001FC2 RID: 8130 RVA: 0x00072EE5 File Offset: 0x000710E5
		// (set) Token: 0x06001FC3 RID: 8131 RVA: 0x00072EEC File Offset: 0x000710EC
		public static int MaxServicePointIdleTime
		{
			get
			{
				return ServicePointManager.maxServicePointIdleTime;
			}
			set
			{
				if (value < -2 || value > 2147483647)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				ServicePointManager.maxServicePointIdleTime = value;
			}
		}

		/// <summary>Gets or sets the maximum number of <see cref="T:System.Net.ServicePoint" /> objects to maintain at any time.</summary>
		/// <returns>The maximum number of <see cref="T:System.Net.ServicePoint" /> objects to maintain. The default value is 0, which means there is no limit to the number of <see cref="T:System.Net.ServicePoint" /> objects.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <see cref="P:System.Net.ServicePointManager.MaxServicePoints" /> is less than 0 or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		// Token: 0x170006DC RID: 1756
		// (get) Token: 0x06001FC4 RID: 8132 RVA: 0x00072F0C File Offset: 0x0007110C
		// (set) Token: 0x06001FC5 RID: 8133 RVA: 0x00072F13 File Offset: 0x00071113
		public static int MaxServicePoints
		{
			get
			{
				return ServicePointManager.maxServicePoints;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentException("value");
				}
				ServicePointManager.maxServicePoints = value;
			}
		}

		/// <summary>Setting this property value to <see langword="true" /> causes all outbound TCP connections from HttpWebRequest to use the native socket option SO_REUSE_UNICASTPORT on the socket. This causes the underlying outgoing ports to be shared. This is useful for scenarios where a large number of outgoing connections are made in a short time, and the app risks running out of ports.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.</returns>
		// Token: 0x170006DD RID: 1757
		// (get) Token: 0x06001FC6 RID: 8134 RVA: 0x00005AFA File Offset: 0x00003CFA
		// (set) Token: 0x06001FC7 RID: 8135 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public static bool ReusePort
		{
			get
			{
				return false;
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the security protocol used by the <see cref="T:System.Net.ServicePoint" /> objects managed by the <see cref="T:System.Net.ServicePointManager" /> object.</summary>
		/// <returns>One of the values defined in the <see cref="T:System.Net.SecurityProtocolType" /> enumeration.</returns>
		/// <exception cref="T:System.NotSupportedException">The value specified to set the property is not a valid <see cref="T:System.Net.SecurityProtocolType" /> enumeration value. </exception>
		// Token: 0x170006DE RID: 1758
		// (get) Token: 0x06001FC8 RID: 8136 RVA: 0x00072F2A File Offset: 0x0007112A
		// (set) Token: 0x06001FC9 RID: 8137 RVA: 0x00072F31 File Offset: 0x00071131
		public static SecurityProtocolType SecurityProtocol
		{
			get
			{
				return ServicePointManager._securityProtocol;
			}
			set
			{
				ServicePointManager._securityProtocol = value;
			}
		}

		// Token: 0x170006DF RID: 1759
		// (get) Token: 0x06001FCA RID: 8138 RVA: 0x00072F39 File Offset: 0x00071139
		internal static ServerCertValidationCallback ServerCertValidationCallback
		{
			get
			{
				return ServicePointManager.server_cert_cb;
			}
		}

		/// <summary>Gets or sets the callback to validate a server certificate.</summary>
		/// <returns>A <see cref="T:System.Net.Security.RemoteCertificateValidationCallback" />. The default value is <see langword="null" />.</returns>
		// Token: 0x170006E0 RID: 1760
		// (get) Token: 0x06001FCB RID: 8139 RVA: 0x00072F40 File Offset: 0x00071140
		// (set) Token: 0x06001FCC RID: 8140 RVA: 0x00072F55 File Offset: 0x00071155
		public static RemoteCertificateValidationCallback ServerCertificateValidationCallback
		{
			get
			{
				if (ServicePointManager.server_cert_cb == null)
				{
					return null;
				}
				return ServicePointManager.server_cert_cb.ValidationCallback;
			}
			set
			{
				if (value == null)
				{
					ServicePointManager.server_cert_cb = null;
					return;
				}
				ServicePointManager.server_cert_cb = new ServerCertValidationCallback(value);
			}
		}

		/// <summary>Gets the <see cref="T:System.Net.Security.EncryptionPolicy" /> for this <see cref="T:System.Net.ServicePointManager" /> instance.</summary>
		/// <returns>The encryption policy to use for this <see cref="T:System.Net.ServicePointManager" /> instance.</returns>
		// Token: 0x170006E1 RID: 1761
		// (get) Token: 0x06001FCD RID: 8141 RVA: 0x00005AFA File Offset: 0x00003CFA
		[MonoTODO("Always returns EncryptionPolicy.RequireEncryption.")]
		public static EncryptionPolicy EncryptionPolicy
		{
			get
			{
				return EncryptionPolicy.RequireEncryption;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that determines whether 100-Continue behavior is used.</summary>
		/// <returns>
		///     <see langword="true" /> to enable 100-Continue behavior. The default value is <see langword="true" />.</returns>
		// Token: 0x170006E2 RID: 1762
		// (get) Token: 0x06001FCE RID: 8142 RVA: 0x00072F6C File Offset: 0x0007116C
		// (set) Token: 0x06001FCF RID: 8143 RVA: 0x00072F73 File Offset: 0x00071173
		public static bool Expect100Continue
		{
			get
			{
				return ServicePointManager.expectContinue;
			}
			set
			{
				ServicePointManager.expectContinue = value;
			}
		}

		/// <summary>Determines whether the Nagle algorithm is used by the service points managed by this <see cref="T:System.Net.ServicePointManager" /> object.</summary>
		/// <returns>
		///     <see langword="true" /> to use the Nagle algorithm; otherwise, <see langword="false" />. The default value is <see langword="true" />.</returns>
		// Token: 0x170006E3 RID: 1763
		// (get) Token: 0x06001FD0 RID: 8144 RVA: 0x00072F7B File Offset: 0x0007117B
		// (set) Token: 0x06001FD1 RID: 8145 RVA: 0x00072F82 File Offset: 0x00071182
		public static bool UseNagleAlgorithm
		{
			get
			{
				return ServicePointManager.useNagle;
			}
			set
			{
				ServicePointManager.useNagle = value;
			}
		}

		// Token: 0x170006E4 RID: 1764
		// (get) Token: 0x06001FD2 RID: 8146 RVA: 0x00005AFA File Offset: 0x00003CFA
		internal static bool DisableStrongCrypto
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170006E5 RID: 1765
		// (get) Token: 0x06001FD3 RID: 8147 RVA: 0x00005AFA File Offset: 0x00003CFA
		internal static bool DisableSendAuxRecord
		{
			get
			{
				return false;
			}
		}

		/// <summary>Enables or disables the keep-alive option on a TCP connection.</summary>
		/// <param name="enabled">If set to true, then the TCP keep-alive option on a TCP connection will be enabled using the specified <paramref name="keepAliveTime " />and <paramref name="keepAliveInterval" /> values. If set to false, then the TCP keep-alive option is disabled and the remaining parameters are ignored.The default value is false.</param>
		/// <param name="keepAliveTime">Specifies the timeout, in milliseconds, with no activity until the first keep-alive packet is sent.The value must be greater than 0.  If a value of less than or equal to zero is passed an <see cref="T:System.ArgumentOutOfRangeException" /> is thrown.</param>
		/// <param name="keepAliveInterval">Specifies the interval, in milliseconds, between when successive keep-alive packets are sent if no acknowledgement is received.The value must be greater than 0.  If a value of less than or equal to zero is passed an <see cref="T:System.ArgumentOutOfRangeException" /> is thrown.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value specified for <paramref name="keepAliveTime" /> or <paramref name="keepAliveInterval" /> parameter is less than or equal to 0.</exception>
		// Token: 0x06001FD4 RID: 8148 RVA: 0x00072F8A File Offset: 0x0007118A
		public static void SetTcpKeepAlive(bool enabled, int keepAliveTime, int keepAliveInterval)
		{
			if (enabled)
			{
				if (keepAliveTime <= 0)
				{
					throw new ArgumentOutOfRangeException("keepAliveTime", "Must be greater than 0");
				}
				if (keepAliveInterval <= 0)
				{
					throw new ArgumentOutOfRangeException("keepAliveInterval", "Must be greater than 0");
				}
			}
			ServicePointManager.tcp_keepalive = enabled;
			ServicePointManager.tcp_keepalive_time = keepAliveTime;
			ServicePointManager.tcp_keepalive_interval = keepAliveInterval;
		}

		/// <summary>Finds an existing <see cref="T:System.Net.ServicePoint" /> object or creates a new <see cref="T:System.Net.ServicePoint" /> object to manage communications with the specified <see cref="T:System.Uri" /> object.</summary>
		/// <param name="address">The <see cref="T:System.Uri" /> object of the Internet resource to contact. </param>
		/// <returns>The <see cref="T:System.Net.ServicePoint" /> object that manages communications for the request.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="address" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The maximum number of <see cref="T:System.Net.ServicePoint" /> objects defined in <see cref="P:System.Net.ServicePointManager.MaxServicePoints" /> has been reached. </exception>
		// Token: 0x06001FD5 RID: 8149 RVA: 0x00072FC9 File Offset: 0x000711C9
		public static ServicePoint FindServicePoint(Uri address)
		{
			return ServicePointManager.FindServicePoint(address, null);
		}

		/// <summary>Finds an existing <see cref="T:System.Net.ServicePoint" /> object or creates a new <see cref="T:System.Net.ServicePoint" /> object to manage communications with the specified Uniform Resource Identifier (URI).</summary>
		/// <param name="uriString">The URI of the Internet resource to be contacted. </param>
		/// <param name="proxy">The proxy data for this request. </param>
		/// <returns>The <see cref="T:System.Net.ServicePoint" /> object that manages communications for the request.</returns>
		/// <exception cref="T:System.UriFormatException">The URI specified in <paramref name="uriString" /> is invalid. </exception>
		/// <exception cref="T:System.InvalidOperationException">The maximum number of <see cref="T:System.Net.ServicePoint" /> objects defined in <see cref="P:System.Net.ServicePointManager.MaxServicePoints" /> has been reached. </exception>
		// Token: 0x06001FD6 RID: 8150 RVA: 0x00072FD2 File Offset: 0x000711D2
		public static ServicePoint FindServicePoint(string uriString, IWebProxy proxy)
		{
			return ServicePointManager.FindServicePoint(new Uri(uriString), proxy);
		}

		/// <summary>Finds an existing <see cref="T:System.Net.ServicePoint" /> object or creates a new <see cref="T:System.Net.ServicePoint" /> object to manage communications with the specified <see cref="T:System.Uri" /> object.</summary>
		/// <param name="address">A <see cref="T:System.Uri" /> object that contains the address of the Internet resource to contact. </param>
		/// <param name="proxy">The proxy data for this request. </param>
		/// <returns>The <see cref="T:System.Net.ServicePoint" /> object that manages communications for the request.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="address" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The maximum number of <see cref="T:System.Net.ServicePoint" /> objects defined in <see cref="P:System.Net.ServicePointManager.MaxServicePoints" /> has been reached. </exception>
		// Token: 0x06001FD7 RID: 8151 RVA: 0x00072FE0 File Offset: 0x000711E0
		public static ServicePoint FindServicePoint(Uri address, IWebProxy proxy)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			Uri uri = new Uri(address.Scheme + "://" + address.Authority);
			bool flag = false;
			bool flag2 = false;
			if (proxy != null && !proxy.IsBypassed(address))
			{
				flag = true;
				bool flag3 = address.Scheme == "https";
				address = proxy.GetProxy(address);
				if (address.Scheme != "http")
				{
					throw new NotSupportedException("Proxy scheme not supported.");
				}
				if (flag3 && address.Scheme == "http")
				{
					flag2 = true;
				}
			}
			address = new Uri(address.Scheme + "://" + address.Authority);
			ServicePoint servicePoint = null;
			ServicePointManager.SPKey key = new ServicePointManager.SPKey(uri, flag ? address : null, flag2);
			HybridDictionary obj = ServicePointManager.servicePoints;
			lock (obj)
			{
				servicePoint = (ServicePointManager.servicePoints[key] as ServicePoint);
				if (servicePoint != null)
				{
					return servicePoint;
				}
				if (ServicePointManager.maxServicePoints > 0 && ServicePointManager.servicePoints.Count >= ServicePointManager.maxServicePoints)
				{
					throw new InvalidOperationException("maximum number of service points reached");
				}
				int connectionLimit = ServicePointManager.defaultConnectionLimit;
				servicePoint = new ServicePoint(address, connectionLimit, ServicePointManager.maxServicePointIdleTime);
				servicePoint.Expect100Continue = ServicePointManager.expectContinue;
				servicePoint.UseNagleAlgorithm = ServicePointManager.useNagle;
				servicePoint.UsesProxy = flag;
				servicePoint.UseConnect = flag2;
				servicePoint.SetTcpKeepAlive(ServicePointManager.tcp_keepalive, ServicePointManager.tcp_keepalive_time, ServicePointManager.tcp_keepalive_interval);
				ServicePointManager.servicePoints.Add(key, servicePoint);
			}
			return servicePoint;
		}

		// Token: 0x06001FD8 RID: 8152 RVA: 0x00073178 File Offset: 0x00071378
		internal static void CloseConnectionGroup(string connectionGroupName)
		{
			HybridDictionary obj = ServicePointManager.servicePoints;
			lock (obj)
			{
				foreach (object obj2 in ServicePointManager.servicePoints.Values)
				{
					((ServicePoint)obj2).CloseConnectionGroup(connectionGroupName);
				}
			}
		}

		// Token: 0x04001B00 RID: 6912
		private static HybridDictionary servicePoints = new HybridDictionary();

		// Token: 0x04001B01 RID: 6913
		private static ICertificatePolicy policy;

		// Token: 0x04001B02 RID: 6914
		private static int defaultConnectionLimit = 10;

		// Token: 0x04001B03 RID: 6915
		private static int maxServicePointIdleTime = 100000;

		// Token: 0x04001B04 RID: 6916
		private static int maxServicePoints = 0;

		// Token: 0x04001B05 RID: 6917
		private static int dnsRefreshTimeout = 120000;

		// Token: 0x04001B06 RID: 6918
		private static bool _checkCRL = false;

		// Token: 0x04001B07 RID: 6919
		private static SecurityProtocolType _securityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

		// Token: 0x04001B08 RID: 6920
		private static bool expectContinue = true;

		// Token: 0x04001B09 RID: 6921
		private static bool useNagle;

		// Token: 0x04001B0A RID: 6922
		private static ServerCertValidationCallback server_cert_cb;

		// Token: 0x04001B0B RID: 6923
		private static bool tcp_keepalive;

		// Token: 0x04001B0C RID: 6924
		private static int tcp_keepalive_time;

		// Token: 0x04001B0D RID: 6925
		private static int tcp_keepalive_interval;

		/// <summary>The default number of non-persistent connections (4) allowed on a <see cref="T:System.Net.ServicePoint" /> object connected to an HTTP/1.0 or later server. This field is constant but is no longer used in the .NET Framework 2.0.</summary>
		// Token: 0x04001B0E RID: 6926
		public const int DefaultNonPersistentConnectionLimit = 4;

		/// <summary>The default number of persistent connections (2) allowed on a <see cref="T:System.Net.ServicePoint" /> object connected to an HTTP/1.1 or later server. This field is constant and is used to initialize the <see cref="P:System.Net.ServicePointManager.DefaultConnectionLimit" /> property if the value of the <see cref="P:System.Net.ServicePointManager.DefaultConnectionLimit" /> property has not been set either directly or through configuration.</summary>
		// Token: 0x04001B0F RID: 6927
		public const int DefaultPersistentConnectionLimit = 10;

		// Token: 0x02000401 RID: 1025
		private class SPKey
		{
			// Token: 0x06001FD9 RID: 8153 RVA: 0x000731FC File Offset: 0x000713FC
			public SPKey(Uri uri, Uri proxy, bool use_connect)
			{
				this.uri = uri;
				this.proxy = proxy;
				this.use_connect = use_connect;
			}

			// Token: 0x170006E6 RID: 1766
			// (get) Token: 0x06001FDA RID: 8154 RVA: 0x00073219 File Offset: 0x00071419
			public Uri Uri
			{
				get
				{
					return this.uri;
				}
			}

			// Token: 0x170006E7 RID: 1767
			// (get) Token: 0x06001FDB RID: 8155 RVA: 0x00073221 File Offset: 0x00071421
			public bool UseConnect
			{
				get
				{
					return this.use_connect;
				}
			}

			// Token: 0x170006E8 RID: 1768
			// (get) Token: 0x06001FDC RID: 8156 RVA: 0x00073229 File Offset: 0x00071429
			public bool UsesProxy
			{
				get
				{
					return this.proxy != null;
				}
			}

			// Token: 0x06001FDD RID: 8157 RVA: 0x00073238 File Offset: 0x00071438
			public override int GetHashCode()
			{
				return ((23 * 31 + (this.use_connect ? 1 : 0)) * 31 + this.uri.GetHashCode()) * 31 + ((this.proxy != null) ? this.proxy.GetHashCode() : 0);
			}

			// Token: 0x06001FDE RID: 8158 RVA: 0x00073288 File Offset: 0x00071488
			public override bool Equals(object obj)
			{
				ServicePointManager.SPKey spkey = obj as ServicePointManager.SPKey;
				return obj != null && this.uri.Equals(spkey.uri) && this.use_connect == spkey.use_connect && this.UsesProxy == spkey.UsesProxy && (!this.UsesProxy || this.proxy.Equals(spkey.proxy));
			}

			// Token: 0x04001B10 RID: 6928
			private Uri uri;

			// Token: 0x04001B11 RID: 6929
			private Uri proxy;

			// Token: 0x04001B12 RID: 6930
			private bool use_connect;
		}
	}
}
