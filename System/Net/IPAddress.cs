﻿using System;
using System.Globalization;
using System.Net.Sockets;
using System.Runtime.CompilerServices;

namespace System.Net
{
	/// <summary>Provides an Internet Protocol (IP) address.</summary>
	// Token: 0x020002F4 RID: 756
	[Serializable]
	public class IPAddress
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.IPAddress" /> class with the address specified as an <see cref="T:System.Int64" />.</summary>
		/// <param name="newAddress">The long value of the IP address. For example, the value 0x2414188f in big-endian format would be the IP address "143.24.20.36". </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="newAddress" /> &lt; 0 or 
		///         <paramref name="newAddress" /> &gt; 0x00000000FFFFFFFF </exception>
		// Token: 0x06001643 RID: 5699 RVA: 0x00050370 File Offset: 0x0004E570
		public IPAddress(long newAddress)
		{
			if (newAddress < 0L || newAddress > (long)((ulong)-1))
			{
				throw new ArgumentOutOfRangeException("newAddress");
			}
			this.m_Address = newAddress;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.IPAddress" /> class with the address specified as a <see cref="T:System.Byte" /> array and the specified scope identifier.</summary>
		/// <param name="address">The byte array value of the IP address. </param>
		/// <param name="scopeid">The long value of the scope identifier. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="address" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="address" /> contains a bad IP address. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="scopeid" /> &lt; 0 or 
		///         <paramref name="scopeid" /> &gt; 0x00000000FFFFFFFF </exception>
		// Token: 0x06001644 RID: 5700 RVA: 0x000503A8 File Offset: 0x0004E5A8
		public IPAddress(byte[] address, long scopeid)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (address.Length != 16)
			{
				throw new ArgumentException(SR.GetString("An invalid IP address was specified."), "address");
			}
			this.m_Family = AddressFamily.InterNetworkV6;
			for (int i = 0; i < 8; i++)
			{
				this.m_Numbers[i] = (ushort)((int)address[i * 2] * 256 + (int)address[i * 2 + 1]);
			}
			if (scopeid < 0L || scopeid > (long)((ulong)-1))
			{
				throw new ArgumentOutOfRangeException("scopeid");
			}
			this.m_ScopeId = scopeid;
		}

		// Token: 0x06001645 RID: 5701 RVA: 0x00050444 File Offset: 0x0004E644
		private IPAddress(ushort[] address, uint scopeid)
		{
			this.m_Family = AddressFamily.InterNetworkV6;
			this.m_Numbers = address;
			this.m_ScopeId = (long)((ulong)scopeid);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.IPAddress" /> class with the address specified as a <see cref="T:System.Byte" /> array.</summary>
		/// <param name="address">The byte array value of the IP address. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="address" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="address" /> contains a bad IP address. </exception>
		// Token: 0x06001646 RID: 5702 RVA: 0x00050478 File Offset: 0x0004E678
		public IPAddress(byte[] address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (address.Length != 4 && address.Length != 16)
			{
				throw new ArgumentException(SR.GetString("An invalid IP address was specified."), "address");
			}
			if (address.Length == 4)
			{
				this.m_Family = AddressFamily.InterNetwork;
				this.m_Address = ((long)((int)address[3] << 24 | (int)address[2] << 16 | (int)address[1] << 8 | (int)address[0]) & (long)((ulong)-1));
				return;
			}
			this.m_Family = AddressFamily.InterNetworkV6;
			for (int i = 0; i < 8; i++)
			{
				this.m_Numbers[i] = (ushort)((int)address[i * 2] * 256 + (int)address[i * 2 + 1]);
			}
		}

		// Token: 0x06001647 RID: 5703 RVA: 0x0005052D File Offset: 0x0004E72D
		internal IPAddress(int newAddress)
		{
			this.m_Address = ((long)newAddress & (long)((ulong)-1));
		}

		/// <summary>Determines whether a string is a valid IP address.</summary>
		/// <param name="ipString">The string to validate.</param>
		/// <param name="address">The <see cref="T:System.Net.IPAddress" /> version of the string.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="ipString" /> was able to be parsed as an IP address; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001648 RID: 5704 RVA: 0x00050553 File Offset: 0x0004E753
		public static bool TryParse(string ipString, out IPAddress address)
		{
			address = IPAddress.InternalParse(ipString, true);
			return address != null;
		}

		/// <summary>Converts an IP address string to an <see cref="T:System.Net.IPAddress" /> instance.</summary>
		/// <param name="ipString">A string that contains an IP address in dotted-quad notation for IPv4 and in colon-hexadecimal notation for IPv6. </param>
		/// <returns>An <see cref="T:System.Net.IPAddress" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="ipString" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="ipString" /> is not a valid IP address. </exception>
		// Token: 0x06001649 RID: 5705 RVA: 0x00050563 File Offset: 0x0004E763
		public static IPAddress Parse(string ipString)
		{
			return IPAddress.InternalParse(ipString, false);
		}

		// Token: 0x0600164A RID: 5706 RVA: 0x0005056C File Offset: 0x0004E76C
		private unsafe static IPAddress InternalParse(string ipString, bool tryParse)
		{
			if (ipString == null)
			{
				if (tryParse)
				{
					return null;
				}
				throw new ArgumentNullException("ipString");
			}
			else if (ipString.IndexOf(':') != -1)
			{
				int start = 0;
				if (ipString[0] != '[')
				{
					ipString += "]";
				}
				else
				{
					start = 1;
				}
				int length = ipString.Length;
				fixed (string text = ipString)
				{
					char* ptr = text;
					if (ptr != null)
					{
						ptr += RuntimeHelpers.OffsetToStringData / 2;
					}
					if (IPv6AddressHelper.IsValidStrict(ptr, start, ref length) || length != ipString.Length)
					{
						ushort[] array = new ushort[8];
						string text2 = null;
						ushort[] array2;
						ushort* numbers;
						if ((array2 = array) == null || array2.Length == 0)
						{
							numbers = null;
						}
						else
						{
							numbers = &array2[0];
						}
						IPv6AddressHelper.Parse(ipString, numbers, 0, ref text2);
						array2 = null;
						if (text2 == null || text2.Length == 0)
						{
							return new IPAddress(array, 0U);
						}
						text2 = text2.Substring(1);
						uint scopeid;
						if (uint.TryParse(text2, NumberStyles.None, null, out scopeid))
						{
							return new IPAddress(array, scopeid);
						}
						return new IPAddress(array, 0U);
					}
					else
					{
						text = null;
						if (tryParse)
						{
							return null;
						}
						SocketException innerException = new SocketException(SocketError.InvalidArgument);
						throw new FormatException(SR.GetString("An invalid IP address was specified."), innerException);
					}
				}
			}
			else
			{
				int length2 = ipString.Length;
				long num;
				fixed (string text = ipString)
				{
					char* ptr2 = text;
					if (ptr2 != null)
					{
						ptr2 += RuntimeHelpers.OffsetToStringData / 2;
					}
					num = IPv4AddressHelper.ParseNonCanonical(ptr2, 0, ref length2, true);
				}
				if (num != -1L && length2 == ipString.Length)
				{
					num = ((num & 255L) << 24 | ((num & 65280L) << 8 | ((num & 16711680L) >> 8 | (num & (long)((ulong)-16777216)) >> 24)));
					return new IPAddress(num);
				}
				if (tryParse)
				{
					return null;
				}
				throw new FormatException(SR.GetString("An invalid IP address was specified."));
			}
		}

		/// <summary>An Internet Protocol (IP) address.</summary>
		/// <returns>The long value of the IP address.</returns>
		/// <exception cref="T:System.Net.Sockets.SocketException">The address family is <see cref="F:System.Net.Sockets.AddressFamily.InterNetworkV6" />. </exception>
		// Token: 0x1700048C RID: 1164
		// (get) Token: 0x0600164B RID: 5707 RVA: 0x00050710 File Offset: 0x0004E910
		// (set) Token: 0x0600164C RID: 5708 RVA: 0x0005072D File Offset: 0x0004E92D
		[Obsolete("This property has been deprecated. It is address family dependent. Please use IPAddress.Equals method to perform comparisons. http://go.microsoft.com/fwlink/?linkid=14202")]
		public long Address
		{
			get
			{
				if (this.m_Family == AddressFamily.InterNetworkV6)
				{
					throw new SocketException(SocketError.OperationNotSupported);
				}
				return this.m_Address;
			}
			set
			{
				if (this.m_Family == AddressFamily.InterNetworkV6)
				{
					throw new SocketException(SocketError.OperationNotSupported);
				}
				if (this.m_Address != value)
				{
					this.m_ToString = null;
					this.m_Address = value;
				}
			}
		}

		/// <summary>Provides a copy of the <see cref="T:System.Net.IPAddress" /> as an array of bytes.</summary>
		/// <returns>A <see cref="T:System.Byte" /> array.</returns>
		// Token: 0x0600164D RID: 5709 RVA: 0x0005075C File Offset: 0x0004E95C
		public byte[] GetAddressBytes()
		{
			byte[] array;
			if (this.m_Family == AddressFamily.InterNetworkV6)
			{
				array = new byte[16];
				int num = 0;
				for (int i = 0; i < 8; i++)
				{
					array[num++] = (byte)(this.m_Numbers[i] >> 8 & 255);
					array[num++] = (byte)(this.m_Numbers[i] & 255);
				}
			}
			else
			{
				array = new byte[]
				{
					(byte)this.m_Address,
					(byte)(this.m_Address >> 8),
					(byte)(this.m_Address >> 16),
					(byte)(this.m_Address >> 24)
				};
			}
			return array;
		}

		/// <summary>Gets the address family of the IP address.</summary>
		/// <returns>Returns <see cref="F:System.Net.Sockets.AddressFamily.InterNetwork" /> for IPv4 or <see cref="F:System.Net.Sockets.AddressFamily.InterNetworkV6" /> for IPv6.</returns>
		// Token: 0x1700048D RID: 1165
		// (get) Token: 0x0600164E RID: 5710 RVA: 0x000507F1 File Offset: 0x0004E9F1
		public AddressFamily AddressFamily
		{
			get
			{
				return this.m_Family;
			}
		}

		/// <summary>Gets or sets the IPv6 address scope identifier.</summary>
		/// <returns>A long integer that specifies the scope of the address.</returns>
		/// <exception cref="T:System.Net.Sockets.SocketException">
		///         <see langword="AddressFamily" /> = <see langword="InterNetwork" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="scopeId" /> &lt; 0- or -
		///             <paramref name="scopeId" /> &gt; 0x00000000FFFFFFFF  </exception>
		// Token: 0x1700048E RID: 1166
		// (get) Token: 0x0600164F RID: 5711 RVA: 0x000507F9 File Offset: 0x0004E9F9
		// (set) Token: 0x06001650 RID: 5712 RVA: 0x00050818 File Offset: 0x0004EA18
		public long ScopeId
		{
			get
			{
				if (this.m_Family == AddressFamily.InterNetwork)
				{
					throw new SocketException(SocketError.OperationNotSupported);
				}
				return this.m_ScopeId;
			}
			set
			{
				if (this.m_Family == AddressFamily.InterNetwork)
				{
					throw new SocketException(SocketError.OperationNotSupported);
				}
				if (value < 0L || value > (long)((ulong)-1))
				{
					throw new ArgumentOutOfRangeException("value");
				}
				if (this.m_ScopeId != value)
				{
					this.m_Address = value;
					this.m_ScopeId = value;
				}
			}
		}

		/// <summary>Converts an Internet address to its standard notation.</summary>
		/// <returns>A string that contains the IP address in either IPv4 dotted-quad or in IPv6 colon-hexadecimal notation.</returns>
		/// <exception cref="T:System.Net.Sockets.SocketException">The address family is <see cref="F:System.Net.Sockets.AddressFamily.InterNetworkV6" /> and the address is bad. </exception>
		// Token: 0x06001651 RID: 5713 RVA: 0x00050868 File Offset: 0x0004EA68
		public unsafe override string ToString()
		{
			if (this.m_ToString == null)
			{
				if (this.m_Family == AddressFamily.InterNetworkV6)
				{
					IPv6AddressFormatter pv6AddressFormatter = new IPv6AddressFormatter(this.m_Numbers, this.ScopeId);
					this.m_ToString = pv6AddressFormatter.ToString();
				}
				else
				{
					int num = 15;
					char* ptr = stackalloc char[(UIntPtr)30];
					int num2 = (int)(this.m_Address >> 24 & 255L);
					do
					{
						ptr[(IntPtr)(--num) * 2] = (char)(48 + num2 % 10);
						num2 /= 10;
					}
					while (num2 > 0);
					ptr[(IntPtr)(--num) * 2] = '.';
					num2 = (int)(this.m_Address >> 16 & 255L);
					do
					{
						ptr[(IntPtr)(--num) * 2] = (char)(48 + num2 % 10);
						num2 /= 10;
					}
					while (num2 > 0);
					ptr[(IntPtr)(--num) * 2] = '.';
					num2 = (int)(this.m_Address >> 8 & 255L);
					do
					{
						ptr[(IntPtr)(--num) * 2] = (char)(48 + num2 % 10);
						num2 /= 10;
					}
					while (num2 > 0);
					ptr[(IntPtr)(--num) * 2] = '.';
					num2 = (int)(this.m_Address & 255L);
					do
					{
						ptr[(IntPtr)(--num) * 2] = (char)(48 + num2 % 10);
						num2 /= 10;
					}
					while (num2 > 0);
					this.m_ToString = new string(ptr, num, 15 - num);
				}
			}
			return this.m_ToString;
		}

		/// <summary>Converts a long value from host byte order to network byte order.</summary>
		/// <param name="host">The number to convert, expressed in host byte order. </param>
		/// <returns>A long value, expressed in network byte order.</returns>
		// Token: 0x06001652 RID: 5714 RVA: 0x000509B0 File Offset: 0x0004EBB0
		public static long HostToNetworkOrder(long host)
		{
			return ((long)IPAddress.HostToNetworkOrder((int)host) & (long)((ulong)-1)) << 32 | ((long)IPAddress.HostToNetworkOrder((int)(host >> 32)) & (long)((ulong)-1));
		}

		/// <summary>Converts an integer value from host byte order to network byte order.</summary>
		/// <param name="host">The number to convert, expressed in host byte order. </param>
		/// <returns>An integer value, expressed in network byte order.</returns>
		// Token: 0x06001653 RID: 5715 RVA: 0x000509CF File Offset: 0x0004EBCF
		public static int HostToNetworkOrder(int host)
		{
			return ((int)IPAddress.HostToNetworkOrder((short)host) & 65535) << 16 | ((int)IPAddress.HostToNetworkOrder((short)(host >> 16)) & 65535);
		}

		/// <summary>Converts a short value from host byte order to network byte order.</summary>
		/// <param name="host">The number to convert, expressed in host byte order. </param>
		/// <returns>A short value, expressed in network byte order.</returns>
		// Token: 0x06001654 RID: 5716 RVA: 0x000509F2 File Offset: 0x0004EBF2
		public static short HostToNetworkOrder(short host)
		{
			return (short)((int)(host & 255) << 8 | (host >> 8 & 255));
		}

		/// <summary>Converts a long value from network byte order to host byte order.</summary>
		/// <param name="network">The number to convert, expressed in network byte order. </param>
		/// <returns>A long value, expressed in host byte order.</returns>
		// Token: 0x06001655 RID: 5717 RVA: 0x00050A08 File Offset: 0x0004EC08
		public static long NetworkToHostOrder(long network)
		{
			return IPAddress.HostToNetworkOrder(network);
		}

		/// <summary>Converts an integer value from network byte order to host byte order.</summary>
		/// <param name="network">The number to convert, expressed in network byte order. </param>
		/// <returns>An integer value, expressed in host byte order.</returns>
		// Token: 0x06001656 RID: 5718 RVA: 0x00050A10 File Offset: 0x0004EC10
		public static int NetworkToHostOrder(int network)
		{
			return IPAddress.HostToNetworkOrder(network);
		}

		/// <summary>Converts a short value from network byte order to host byte order.</summary>
		/// <param name="network">The number to convert, expressed in network byte order. </param>
		/// <returns>A short value, expressed in host byte order.</returns>
		// Token: 0x06001657 RID: 5719 RVA: 0x00050A18 File Offset: 0x0004EC18
		public static short NetworkToHostOrder(short network)
		{
			return IPAddress.HostToNetworkOrder(network);
		}

		/// <summary>Indicates whether the specified IP address is the loopback address.</summary>
		/// <param name="address">An IP address. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="address" /> is the loopback address; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001658 RID: 5720 RVA: 0x00050A20 File Offset: 0x0004EC20
		public static bool IsLoopback(IPAddress address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (address.m_Family == AddressFamily.InterNetworkV6)
			{
				return address.Equals(IPAddress.IPv6Loopback);
			}
			return (address.m_Address & 255L) == (IPAddress.Loopback.m_Address & 255L);
		}

		// Token: 0x1700048F RID: 1167
		// (get) Token: 0x06001659 RID: 5721 RVA: 0x00050A71 File Offset: 0x0004EC71
		internal bool IsBroadcast
		{
			get
			{
				return this.m_Family != AddressFamily.InterNetworkV6 && this.m_Address == IPAddress.Broadcast.m_Address;
			}
		}

		/// <summary>Gets whether the address is an IPv6 multicast global address.</summary>
		/// <returns>
		///     <see langword="true" /> if the IP address is an IPv6 multicast global address; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000490 RID: 1168
		// (get) Token: 0x0600165A RID: 5722 RVA: 0x00050A91 File Offset: 0x0004EC91
		public bool IsIPv6Multicast
		{
			get
			{
				return this.m_Family == AddressFamily.InterNetworkV6 && (this.m_Numbers[0] & 65280) == 65280;
			}
		}

		/// <summary>Gets whether the address is an IPv6 link local address.</summary>
		/// <returns>
		///     <see langword="true" /> if the IP address is an IPv6 link local address; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000491 RID: 1169
		// (get) Token: 0x0600165B RID: 5723 RVA: 0x00050AB4 File Offset: 0x0004ECB4
		public bool IsIPv6LinkLocal
		{
			get
			{
				return this.m_Family == AddressFamily.InterNetworkV6 && (this.m_Numbers[0] & 65472) == 65152;
			}
		}

		/// <summary>Gets whether the address is an IPv6 site local address.</summary>
		/// <returns>
		///     <see langword="true" /> if the IP address is an IPv6 site local address; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000492 RID: 1170
		// (get) Token: 0x0600165C RID: 5724 RVA: 0x00050AD7 File Offset: 0x0004ECD7
		public bool IsIPv6SiteLocal
		{
			get
			{
				return this.m_Family == AddressFamily.InterNetworkV6 && (this.m_Numbers[0] & 65472) == 65216;
			}
		}

		/// <summary>Gets whether the address is an IPv6 Teredo address.</summary>
		/// <returns>
		///     <see langword="true" /> if the IP address is an IPv6 Teredo address; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000493 RID: 1171
		// (get) Token: 0x0600165D RID: 5725 RVA: 0x00050AFA File Offset: 0x0004ECFA
		public bool IsIPv6Teredo
		{
			get
			{
				return this.m_Family == AddressFamily.InterNetworkV6 && this.m_Numbers[0] == 8193 && this.m_Numbers[1] == 0;
			}
		}

		/// <summary>Gets whether the IP address is an IPv4-mapped IPv6 address.</summary>
		/// <returns>Returns <see cref="T:System.Boolean" />.
		///     <see langword="true" /> if the IP address is an IPv4-mapped IPv6 address; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000494 RID: 1172
		// (get) Token: 0x0600165E RID: 5726 RVA: 0x00050B24 File Offset: 0x0004ED24
		public bool IsIPv4MappedToIPv6
		{
			get
			{
				if (this.AddressFamily != AddressFamily.InterNetworkV6)
				{
					return false;
				}
				for (int i = 0; i < 5; i++)
				{
					if (this.m_Numbers[i] != 0)
					{
						return false;
					}
				}
				return this.m_Numbers[5] == ushort.MaxValue;
			}
		}

		// Token: 0x0600165F RID: 5727 RVA: 0x00050B64 File Offset: 0x0004ED64
		internal bool Equals(object comparandObj, bool compareScopeId)
		{
			IPAddress ipaddress = comparandObj as IPAddress;
			if (ipaddress == null)
			{
				return false;
			}
			if (this.m_Family != ipaddress.m_Family)
			{
				return false;
			}
			if (this.m_Family == AddressFamily.InterNetworkV6)
			{
				for (int i = 0; i < 8; i++)
				{
					if (ipaddress.m_Numbers[i] != this.m_Numbers[i])
					{
						return false;
					}
				}
				return ipaddress.m_ScopeId == this.m_ScopeId || !compareScopeId;
			}
			return ipaddress.m_Address == this.m_Address;
		}

		/// <summary>Compares two IP addresses.</summary>
		/// <param name="comparand">An <see cref="T:System.Net.IPAddress" /> instance to compare to the current instance. </param>
		/// <returns>
		///     <see langword="true" /> if the two addresses are equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001660 RID: 5728 RVA: 0x00050BDC File Offset: 0x0004EDDC
		public override bool Equals(object comparand)
		{
			return this.Equals(comparand, true);
		}

		/// <summary>Returns a hash value for an IP address.</summary>
		/// <returns>An integer hash value.</returns>
		// Token: 0x06001661 RID: 5729 RVA: 0x00050BE6 File Offset: 0x0004EDE6
		public override int GetHashCode()
		{
			if (this.m_Family == AddressFamily.InterNetworkV6)
			{
				if (this.m_HashCode == 0)
				{
					this.m_HashCode = StringComparer.InvariantCultureIgnoreCase.GetHashCode(this.ToString());
				}
				return this.m_HashCode;
			}
			return (int)this.m_Address;
		}

		// Token: 0x06001662 RID: 5730 RVA: 0x00050C20 File Offset: 0x0004EE20
		internal IPAddress Snapshot()
		{
			AddressFamily family = this.m_Family;
			if (family == AddressFamily.InterNetwork)
			{
				return new IPAddress(this.m_Address);
			}
			if (family != AddressFamily.InterNetworkV6)
			{
				throw new InternalException();
			}
			return new IPAddress(this.m_Numbers, (uint)this.m_ScopeId);
		}

		/// <summary>Maps the <see cref="T:System.Net.IPAddress" /> object to an IPv6 address.</summary>
		/// <returns>Returns <see cref="T:System.Net.IPAddress" />.An IPv6 address.</returns>
		// Token: 0x06001663 RID: 5731 RVA: 0x00050C64 File Offset: 0x0004EE64
		public IPAddress MapToIPv6()
		{
			if (this.AddressFamily == AddressFamily.InterNetworkV6)
			{
				return this;
			}
			return new IPAddress(new ushort[]
			{
				0,
				0,
				0,
				0,
				0,
				ushort.MaxValue,
				(ushort)((this.m_Address & 65280L) >> 8 | (this.m_Address & 255L) << 8),
				(ushort)((this.m_Address & (long)((ulong)-16777216)) >> 24 | (this.m_Address & 16711680L) >> 8)
			}, 0U);
		}

		/// <summary>Maps the <see cref="T:System.Net.IPAddress" /> object to an IPv4 address.</summary>
		/// <returns>Returns <see cref="T:System.Net.IPAddress" />.An IPv4 address.</returns>
		// Token: 0x06001664 RID: 5732 RVA: 0x00050CD8 File Offset: 0x0004EED8
		public IPAddress MapToIPv4()
		{
			if (this.AddressFamily == AddressFamily.InterNetwork)
			{
				return this;
			}
			return new IPAddress((long)((ulong)((uint)(this.m_Numbers[6] & 65280) >> 8 | (uint)((uint)(this.m_Numbers[6] & 255) << 8) | ((uint)(this.m_Numbers[7] & 65280) >> 8 | (uint)((uint)(this.m_Numbers[7] & 255) << 8)) << 16)));
		}

		// Token: 0x06001665 RID: 5733 RVA: 0x00050D3C File Offset: 0x0004EF3C
		// Note: this type is marked as 'beforefieldinit'.
		static IPAddress()
		{
		}

		/// <summary>Provides an IP address that indicates that the server must listen for client activity on all network interfaces. This field is read-only.</summary>
		// Token: 0x0400150C RID: 5388
		public static readonly IPAddress Any = new IPAddress(0);

		/// <summary>Provides the IP loopback address. This field is read-only.</summary>
		// Token: 0x0400150D RID: 5389
		public static readonly IPAddress Loopback = new IPAddress(16777343);

		/// <summary>Provides the IP broadcast address. This field is read-only.</summary>
		// Token: 0x0400150E RID: 5390
		public static readonly IPAddress Broadcast = new IPAddress((long)((ulong)-1));

		/// <summary>Provides an IP address that indicates that no network interface should be used. This field is read-only.</summary>
		// Token: 0x0400150F RID: 5391
		public static readonly IPAddress None = IPAddress.Broadcast;

		// Token: 0x04001510 RID: 5392
		internal const long LoopbackMask = 255L;

		// Token: 0x04001511 RID: 5393
		internal long m_Address;

		// Token: 0x04001512 RID: 5394
		[NonSerialized]
		internal string m_ToString;

		/// <summary>The <see cref="M:System.Net.Sockets.Socket.Bind(System.Net.EndPoint)" /> method uses the <see cref="F:System.Net.IPAddress.IPv6Any" /> field to indicate that a <see cref="T:System.Net.Sockets.Socket" /> must listen for client activity on all network interfaces.</summary>
		// Token: 0x04001513 RID: 5395
		public static readonly IPAddress IPv6Any = new IPAddress(new byte[16], 0L);

		/// <summary>Provides the IP loopback address. This property is read-only.</summary>
		// Token: 0x04001514 RID: 5396
		public static readonly IPAddress IPv6Loopback = new IPAddress(new byte[]
		{
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1
		}, 0L);

		/// <summary>Provides an IP address that indicates that no network interface should be used. This property is read-only.</summary>
		// Token: 0x04001515 RID: 5397
		public static readonly IPAddress IPv6None = new IPAddress(new byte[16], 0L);

		// Token: 0x04001516 RID: 5398
		private AddressFamily m_Family = AddressFamily.InterNetwork;

		// Token: 0x04001517 RID: 5399
		private ushort[] m_Numbers = new ushort[8];

		// Token: 0x04001518 RID: 5400
		private long m_ScopeId;

		// Token: 0x04001519 RID: 5401
		private int m_HashCode;

		// Token: 0x0400151A RID: 5402
		internal const int IPv4AddressBytes = 4;

		// Token: 0x0400151B RID: 5403
		internal const int IPv6AddressBytes = 16;

		// Token: 0x0400151C RID: 5404
		internal const int NumberOfLabels = 8;
	}
}
