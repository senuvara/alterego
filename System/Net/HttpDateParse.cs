﻿using System;
using System.Globalization;

namespace System.Net
{
	// Token: 0x02000349 RID: 841
	internal static class HttpDateParse
	{
		// Token: 0x06001820 RID: 6176 RVA: 0x00057481 File Offset: 0x00055681
		private static char MAKE_UPPER(char c)
		{
			return char.ToUpper(c, CultureInfo.InvariantCulture);
		}

		// Token: 0x06001821 RID: 6177 RVA: 0x00057490 File Offset: 0x00055690
		private static int MapDayMonthToDword(char[] lpszDay, int index)
		{
			switch (HttpDateParse.MAKE_UPPER(lpszDay[index]))
			{
			case 'A':
			{
				char c = HttpDateParse.MAKE_UPPER(lpszDay[index + 1]);
				if (c == 'P')
				{
					return 4;
				}
				if (c != 'U')
				{
					return -999;
				}
				return 8;
			}
			case 'D':
				return 12;
			case 'F':
			{
				char c = HttpDateParse.MAKE_UPPER(lpszDay[index + 1]);
				if (c == 'E')
				{
					return 2;
				}
				if (c == 'R')
				{
					return 5;
				}
				return -999;
			}
			case 'G':
				return -1000;
			case 'J':
			{
				char c = HttpDateParse.MAKE_UPPER(lpszDay[index + 1]);
				if (c != 'A')
				{
					if (c == 'U')
					{
						c = HttpDateParse.MAKE_UPPER(lpszDay[index + 2]);
						if (c == 'L')
						{
							return 7;
						}
						if (c == 'N')
						{
							return 6;
						}
					}
					return -999;
				}
				return 1;
			}
			case 'M':
			{
				char c = HttpDateParse.MAKE_UPPER(lpszDay[index + 1]);
				if (c != 'A')
				{
					if (c == 'O')
					{
						return 1;
					}
				}
				else
				{
					c = HttpDateParse.MAKE_UPPER(lpszDay[index + 2]);
					if (c == 'R')
					{
						return 3;
					}
					if (c == 'Y')
					{
						return 5;
					}
				}
				return -999;
			}
			case 'N':
				return 11;
			case 'O':
				return 10;
			case 'S':
			{
				char c = HttpDateParse.MAKE_UPPER(lpszDay[index + 1]);
				if (c == 'A')
				{
					return 6;
				}
				if (c == 'E')
				{
					return 9;
				}
				if (c != 'U')
				{
					return -999;
				}
				return 0;
			}
			case 'T':
			{
				char c = HttpDateParse.MAKE_UPPER(lpszDay[index + 1]);
				if (c == 'H')
				{
					return 4;
				}
				if (c == 'U')
				{
					return 2;
				}
				return -999;
			}
			case 'U':
				return -1000;
			case 'W':
				return 3;
			}
			return -999;
		}

		// Token: 0x06001822 RID: 6178 RVA: 0x00057624 File Offset: 0x00055824
		public static bool ParseHttpDate(string DateString, out DateTime dtOut)
		{
			int num = 0;
			int num2 = 0;
			int num3 = -1;
			bool flag = false;
			int[] array = new int[8];
			bool result = true;
			char[] array2 = DateString.ToCharArray();
			dtOut = default(DateTime);
			while (num < DateString.Length && num2 < 8)
			{
				if (array2[num] >= '0' && array2[num] <= '9')
				{
					array[num2] = 0;
					do
					{
						array[num2] *= 10;
						array[num2] += (int)(array2[num] - '0');
						num++;
					}
					while (num < DateString.Length && array2[num] >= '0' && array2[num] <= '9');
					num2++;
				}
				else if ((array2[num] >= 'A' && array2[num] <= 'Z') || (array2[num] >= 'a' && array2[num] <= 'z'))
				{
					array[num2] = HttpDateParse.MapDayMonthToDword(array2, num);
					num3 = num2;
					if (array[num2] == -999 && (!flag || num2 != 6))
					{
						result = false;
						return result;
					}
					if (num2 == 1)
					{
						flag = true;
					}
					do
					{
						num++;
					}
					while (num < DateString.Length && ((array2[num] >= 'A' && array2[num] <= 'Z') || (array2[num] >= 'a' && array2[num] <= 'z')));
					num2++;
				}
				else
				{
					num++;
				}
			}
			int millisecond = 0;
			int num4;
			int month;
			int num5;
			int num6;
			int num7;
			int num8;
			if (flag)
			{
				num4 = array[2];
				month = array[1];
				num5 = array[3];
				num6 = array[4];
				num7 = array[5];
				if (num3 != 6)
				{
					num8 = array[6];
				}
				else
				{
					num8 = array[7];
				}
			}
			else
			{
				num4 = array[1];
				month = array[2];
				num8 = array[3];
				num5 = array[4];
				num6 = array[5];
				num7 = array[6];
			}
			if (num8 < 100)
			{
				num8 += ((num8 < 80) ? 2000 : 1900);
			}
			if (num2 < 4 || num4 > 31 || num5 > 23 || num6 > 59 || num7 > 59)
			{
				return false;
			}
			dtOut = new DateTime(num8, month, num4, num5, num6, num7, millisecond);
			if (num3 == 6)
			{
				dtOut = dtOut.ToUniversalTime();
			}
			if (num2 > 7 && array[7] != -1000)
			{
				double value = (double)array[7];
				dtOut.AddHours(value);
			}
			dtOut = dtOut.ToLocalTime();
			return result;
		}

		// Token: 0x0400172F RID: 5935
		private const int BASE_DEC = 10;

		// Token: 0x04001730 RID: 5936
		private const int DATE_INDEX_DAY_OF_WEEK = 0;

		// Token: 0x04001731 RID: 5937
		private const int DATE_1123_INDEX_DAY = 1;

		// Token: 0x04001732 RID: 5938
		private const int DATE_1123_INDEX_MONTH = 2;

		// Token: 0x04001733 RID: 5939
		private const int DATE_1123_INDEX_YEAR = 3;

		// Token: 0x04001734 RID: 5940
		private const int DATE_1123_INDEX_HRS = 4;

		// Token: 0x04001735 RID: 5941
		private const int DATE_1123_INDEX_MINS = 5;

		// Token: 0x04001736 RID: 5942
		private const int DATE_1123_INDEX_SECS = 6;

		// Token: 0x04001737 RID: 5943
		private const int DATE_ANSI_INDEX_MONTH = 1;

		// Token: 0x04001738 RID: 5944
		private const int DATE_ANSI_INDEX_DAY = 2;

		// Token: 0x04001739 RID: 5945
		private const int DATE_ANSI_INDEX_HRS = 3;

		// Token: 0x0400173A RID: 5946
		private const int DATE_ANSI_INDEX_MINS = 4;

		// Token: 0x0400173B RID: 5947
		private const int DATE_ANSI_INDEX_SECS = 5;

		// Token: 0x0400173C RID: 5948
		private const int DATE_ANSI_INDEX_YEAR = 6;

		// Token: 0x0400173D RID: 5949
		private const int DATE_INDEX_TZ = 7;

		// Token: 0x0400173E RID: 5950
		private const int DATE_INDEX_LAST = 7;

		// Token: 0x0400173F RID: 5951
		private const int MAX_FIELD_DATE_ENTRIES = 8;

		// Token: 0x04001740 RID: 5952
		private const int DATE_TOKEN_JANUARY = 1;

		// Token: 0x04001741 RID: 5953
		private const int DATE_TOKEN_FEBRUARY = 2;

		// Token: 0x04001742 RID: 5954
		private const int DATE_TOKEN_Microsoft = 3;

		// Token: 0x04001743 RID: 5955
		private const int DATE_TOKEN_APRIL = 4;

		// Token: 0x04001744 RID: 5956
		private const int DATE_TOKEN_MAY = 5;

		// Token: 0x04001745 RID: 5957
		private const int DATE_TOKEN_JUNE = 6;

		// Token: 0x04001746 RID: 5958
		private const int DATE_TOKEN_JULY = 7;

		// Token: 0x04001747 RID: 5959
		private const int DATE_TOKEN_AUGUST = 8;

		// Token: 0x04001748 RID: 5960
		private const int DATE_TOKEN_SEPTEMBER = 9;

		// Token: 0x04001749 RID: 5961
		private const int DATE_TOKEN_OCTOBER = 10;

		// Token: 0x0400174A RID: 5962
		private const int DATE_TOKEN_NOVEMBER = 11;

		// Token: 0x0400174B RID: 5963
		private const int DATE_TOKEN_DECEMBER = 12;

		// Token: 0x0400174C RID: 5964
		private const int DATE_TOKEN_LAST_MONTH = 13;

		// Token: 0x0400174D RID: 5965
		private const int DATE_TOKEN_SUNDAY = 0;

		// Token: 0x0400174E RID: 5966
		private const int DATE_TOKEN_MONDAY = 1;

		// Token: 0x0400174F RID: 5967
		private const int DATE_TOKEN_TUESDAY = 2;

		// Token: 0x04001750 RID: 5968
		private const int DATE_TOKEN_WEDNESDAY = 3;

		// Token: 0x04001751 RID: 5969
		private const int DATE_TOKEN_THURSDAY = 4;

		// Token: 0x04001752 RID: 5970
		private const int DATE_TOKEN_FRIDAY = 5;

		// Token: 0x04001753 RID: 5971
		private const int DATE_TOKEN_SATURDAY = 6;

		// Token: 0x04001754 RID: 5972
		private const int DATE_TOKEN_LAST_DAY = 7;

		// Token: 0x04001755 RID: 5973
		private const int DATE_TOKEN_GMT = -1000;

		// Token: 0x04001756 RID: 5974
		private const int DATE_TOKEN_LAST = -1000;

		// Token: 0x04001757 RID: 5975
		private const int DATE_TOKEN_ERROR = -999;
	}
}
