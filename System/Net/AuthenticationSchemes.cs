﻿using System;

namespace System.Net
{
	/// <summary>Specifies protocols for authentication.</summary>
	// Token: 0x020002DC RID: 732
	[Flags]
	public enum AuthenticationSchemes
	{
		/// <summary>No authentication is allowed. A client requesting an <see cref="T:System.Net.HttpListener" /> object with this flag set will always receive a 403 Forbidden status. Use this flag when a resource should never be served to a client.</summary>
		// Token: 0x04001435 RID: 5173
		None = 0,
		/// <summary>Specifies digest authentication.</summary>
		// Token: 0x04001436 RID: 5174
		Digest = 1,
		/// <summary>Negotiates with the client to determine the authentication scheme. If both client and server support Kerberos, it is used; otherwise, NTLM is used.</summary>
		// Token: 0x04001437 RID: 5175
		Negotiate = 2,
		/// <summary>Specifies NTLM authentication.</summary>
		// Token: 0x04001438 RID: 5176
		Ntlm = 4,
		/// <summary>Specifies basic authentication. </summary>
		// Token: 0x04001439 RID: 5177
		Basic = 8,
		/// <summary>Specifies anonymous authentication.</summary>
		// Token: 0x0400143A RID: 5178
		Anonymous = 32768,
		/// <summary>Specifies Windows authentication.</summary>
		// Token: 0x0400143B RID: 5179
		IntegratedWindowsAuthentication = 6
	}
}
