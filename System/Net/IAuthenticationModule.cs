﻿using System;

namespace System.Net
{
	/// <summary>Provides the base authentication interface for Web client authentication modules.</summary>
	// Token: 0x020002F0 RID: 752
	public interface IAuthenticationModule
	{
		/// <summary>Returns an instance of the <see cref="T:System.Net.Authorization" /> class in respose to an authentication challenge from a server.</summary>
		/// <param name="challenge">The authentication challenge sent by the server. </param>
		/// <param name="request">The <see cref="T:System.Net.WebRequest" /> instance associated with the challenge. </param>
		/// <param name="credentials">The credentials associated with the challenge. </param>
		/// <returns>An <see cref="T:System.Net.Authorization" /> instance containing the authorization message for the request, or <see langword="null" /> if the challenge cannot be handled.</returns>
		// Token: 0x0600163C RID: 5692
		Authorization Authenticate(string challenge, WebRequest request, ICredentials credentials);

		/// <summary>Returns an instance of the <see cref="T:System.Net.Authorization" /> class for an authentication request to a server.</summary>
		/// <param name="request">The <see cref="T:System.Net.WebRequest" /> instance associated with the authentication request. </param>
		/// <param name="credentials">The credentials associated with the authentication request. </param>
		/// <returns>An <see cref="T:System.Net.Authorization" /> instance containing the authorization message for the request.</returns>
		// Token: 0x0600163D RID: 5693
		Authorization PreAuthenticate(WebRequest request, ICredentials credentials);

		/// <summary>Gets a value indicating whether the authentication module supports preauthentication.</summary>
		/// <returns>
		///     <see langword="true" /> if the authorization module supports preauthentication; otherwise <see langword="false" />.</returns>
		// Token: 0x1700048A RID: 1162
		// (get) Token: 0x0600163E RID: 5694
		bool CanPreAuthenticate { get; }

		/// <summary>Gets the authentication type provided by this authentication module.</summary>
		/// <returns>A string indicating the authentication type provided by this authentication module.</returns>
		// Token: 0x1700048B RID: 1163
		// (get) Token: 0x0600163F RID: 5695
		string AuthenticationType { get; }
	}
}
