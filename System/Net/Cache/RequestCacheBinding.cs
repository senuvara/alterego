﻿using System;

namespace System.Net.Cache
{
	// Token: 0x02000536 RID: 1334
	internal class RequestCacheBinding
	{
		// Token: 0x06002A3C RID: 10812 RVA: 0x0009235B File Offset: 0x0009055B
		internal RequestCacheBinding(RequestCache requestCache, RequestCacheValidator cacheValidator, RequestCachePolicy policy)
		{
			this.m_RequestCache = requestCache;
			this.m_CacheValidator = cacheValidator;
			this.m_Policy = policy;
		}

		// Token: 0x17000A82 RID: 2690
		// (get) Token: 0x06002A3D RID: 10813 RVA: 0x00092378 File Offset: 0x00090578
		internal RequestCache Cache
		{
			get
			{
				return this.m_RequestCache;
			}
		}

		// Token: 0x17000A83 RID: 2691
		// (get) Token: 0x06002A3E RID: 10814 RVA: 0x00092380 File Offset: 0x00090580
		internal RequestCacheValidator Validator
		{
			get
			{
				return this.m_CacheValidator;
			}
		}

		// Token: 0x17000A84 RID: 2692
		// (get) Token: 0x06002A3F RID: 10815 RVA: 0x00092388 File Offset: 0x00090588
		internal RequestCachePolicy Policy
		{
			get
			{
				return this.m_Policy;
			}
		}

		// Token: 0x040021C6 RID: 8646
		private RequestCache m_RequestCache;

		// Token: 0x040021C7 RID: 8647
		private RequestCacheValidator m_CacheValidator;

		// Token: 0x040021C8 RID: 8648
		private RequestCachePolicy m_Policy;
	}
}
