﻿using System;

namespace System.Net.Cache
{
	// Token: 0x02000535 RID: 1333
	internal class RequestCachingSectionInternal
	{
		// Token: 0x06002A3B RID: 10811 RVA: 0x0009234C File Offset: 0x0009054C
		public RequestCachingSectionInternal()
		{
		}

		// Token: 0x040021C5 RID: 8645
		public readonly bool DisableAllCaching = true;
	}
}
