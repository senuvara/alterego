﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Text;

namespace System.Net.Cache
{
	// Token: 0x02000532 RID: 1330
	internal class RequestCacheEntry
	{
		// Token: 0x06002A19 RID: 10777 RVA: 0x00091DD8 File Offset: 0x0008FFD8
		internal RequestCacheEntry()
		{
			this.m_ExpiresUtc = (this.m_LastAccessedUtc = (this.m_LastModifiedUtc = (this.m_LastSynchronizedUtc = DateTime.MinValue)));
		}

		// Token: 0x17000A75 RID: 2677
		// (get) Token: 0x06002A1A RID: 10778 RVA: 0x00091E11 File Offset: 0x00090011
		// (set) Token: 0x06002A1B RID: 10779 RVA: 0x00091E19 File Offset: 0x00090019
		internal bool IsPrivateEntry
		{
			get
			{
				return this.m_IsPrivateEntry;
			}
			set
			{
				this.m_IsPrivateEntry = value;
			}
		}

		// Token: 0x17000A76 RID: 2678
		// (get) Token: 0x06002A1C RID: 10780 RVA: 0x00091E22 File Offset: 0x00090022
		// (set) Token: 0x06002A1D RID: 10781 RVA: 0x00091E2A File Offset: 0x0009002A
		internal long StreamSize
		{
			get
			{
				return this.m_StreamSize;
			}
			set
			{
				this.m_StreamSize = value;
			}
		}

		// Token: 0x17000A77 RID: 2679
		// (get) Token: 0x06002A1E RID: 10782 RVA: 0x00091E33 File Offset: 0x00090033
		// (set) Token: 0x06002A1F RID: 10783 RVA: 0x00091E3B File Offset: 0x0009003B
		internal DateTime ExpiresUtc
		{
			get
			{
				return this.m_ExpiresUtc;
			}
			set
			{
				this.m_ExpiresUtc = value;
			}
		}

		// Token: 0x17000A78 RID: 2680
		// (get) Token: 0x06002A20 RID: 10784 RVA: 0x00091E44 File Offset: 0x00090044
		// (set) Token: 0x06002A21 RID: 10785 RVA: 0x00091E4C File Offset: 0x0009004C
		internal DateTime LastAccessedUtc
		{
			get
			{
				return this.m_LastAccessedUtc;
			}
			set
			{
				this.m_LastAccessedUtc = value;
			}
		}

		// Token: 0x17000A79 RID: 2681
		// (get) Token: 0x06002A22 RID: 10786 RVA: 0x00091E55 File Offset: 0x00090055
		// (set) Token: 0x06002A23 RID: 10787 RVA: 0x00091E5D File Offset: 0x0009005D
		internal DateTime LastModifiedUtc
		{
			get
			{
				return this.m_LastModifiedUtc;
			}
			set
			{
				this.m_LastModifiedUtc = value;
			}
		}

		// Token: 0x17000A7A RID: 2682
		// (get) Token: 0x06002A24 RID: 10788 RVA: 0x00091E66 File Offset: 0x00090066
		// (set) Token: 0x06002A25 RID: 10789 RVA: 0x00091E6E File Offset: 0x0009006E
		internal DateTime LastSynchronizedUtc
		{
			get
			{
				return this.m_LastSynchronizedUtc;
			}
			set
			{
				this.m_LastSynchronizedUtc = value;
			}
		}

		// Token: 0x17000A7B RID: 2683
		// (get) Token: 0x06002A26 RID: 10790 RVA: 0x00091E77 File Offset: 0x00090077
		// (set) Token: 0x06002A27 RID: 10791 RVA: 0x00091E7F File Offset: 0x0009007F
		internal TimeSpan MaxStale
		{
			get
			{
				return this.m_MaxStale;
			}
			set
			{
				this.m_MaxStale = value;
			}
		}

		// Token: 0x17000A7C RID: 2684
		// (get) Token: 0x06002A28 RID: 10792 RVA: 0x00091E88 File Offset: 0x00090088
		// (set) Token: 0x06002A29 RID: 10793 RVA: 0x00091E90 File Offset: 0x00090090
		internal int HitCount
		{
			get
			{
				return this.m_HitCount;
			}
			set
			{
				this.m_HitCount = value;
			}
		}

		// Token: 0x17000A7D RID: 2685
		// (get) Token: 0x06002A2A RID: 10794 RVA: 0x00091E99 File Offset: 0x00090099
		// (set) Token: 0x06002A2B RID: 10795 RVA: 0x00091EA1 File Offset: 0x000900A1
		internal int UsageCount
		{
			get
			{
				return this.m_UsageCount;
			}
			set
			{
				this.m_UsageCount = value;
			}
		}

		// Token: 0x17000A7E RID: 2686
		// (get) Token: 0x06002A2C RID: 10796 RVA: 0x00091EAA File Offset: 0x000900AA
		// (set) Token: 0x06002A2D RID: 10797 RVA: 0x00091EB2 File Offset: 0x000900B2
		internal bool IsPartialEntry
		{
			get
			{
				return this.m_IsPartialEntry;
			}
			set
			{
				this.m_IsPartialEntry = value;
			}
		}

		// Token: 0x17000A7F RID: 2687
		// (get) Token: 0x06002A2E RID: 10798 RVA: 0x00091EBB File Offset: 0x000900BB
		// (set) Token: 0x06002A2F RID: 10799 RVA: 0x00091EC3 File Offset: 0x000900C3
		internal StringCollection EntryMetadata
		{
			get
			{
				return this.m_EntryMetadata;
			}
			set
			{
				this.m_EntryMetadata = value;
			}
		}

		// Token: 0x17000A80 RID: 2688
		// (get) Token: 0x06002A30 RID: 10800 RVA: 0x00091ECC File Offset: 0x000900CC
		// (set) Token: 0x06002A31 RID: 10801 RVA: 0x00091ED4 File Offset: 0x000900D4
		internal StringCollection SystemMetadata
		{
			get
			{
				return this.m_SystemMetadata;
			}
			set
			{
				this.m_SystemMetadata = value;
			}
		}

		// Token: 0x06002A32 RID: 10802 RVA: 0x00091EE0 File Offset: 0x000900E0
		internal virtual string ToString(bool verbose)
		{
			StringBuilder stringBuilder = new StringBuilder(512);
			stringBuilder.Append("\r\nIsPrivateEntry   = ").Append(this.IsPrivateEntry);
			stringBuilder.Append("\r\nIsPartialEntry   = ").Append(this.IsPartialEntry);
			stringBuilder.Append("\r\nStreamSize       = ").Append(this.StreamSize);
			stringBuilder.Append("\r\nExpires          = ").Append((this.ExpiresUtc == DateTime.MinValue) ? "" : this.ExpiresUtc.ToString("r", CultureInfo.CurrentCulture));
			stringBuilder.Append("\r\nLastAccessed     = ").Append((this.LastAccessedUtc == DateTime.MinValue) ? "" : this.LastAccessedUtc.ToString("r", CultureInfo.CurrentCulture));
			stringBuilder.Append("\r\nLastModified     = ").Append((this.LastModifiedUtc == DateTime.MinValue) ? "" : this.LastModifiedUtc.ToString("r", CultureInfo.CurrentCulture));
			stringBuilder.Append("\r\nLastSynchronized = ").Append((this.LastSynchronizedUtc == DateTime.MinValue) ? "" : this.LastSynchronizedUtc.ToString("r", CultureInfo.CurrentCulture));
			stringBuilder.Append("\r\nMaxStale(sec)    = ").Append((this.MaxStale == TimeSpan.MinValue) ? "" : ((int)this.MaxStale.TotalSeconds).ToString(NumberFormatInfo.CurrentInfo));
			stringBuilder.Append("\r\nHitCount         = ").Append(this.HitCount.ToString(NumberFormatInfo.CurrentInfo));
			stringBuilder.Append("\r\nUsageCount       = ").Append(this.UsageCount.ToString(NumberFormatInfo.CurrentInfo));
			stringBuilder.Append("\r\n");
			if (verbose)
			{
				stringBuilder.Append("EntryMetadata:\r\n");
				if (this.m_EntryMetadata != null)
				{
					foreach (string value in this.m_EntryMetadata)
					{
						stringBuilder.Append(value).Append("\r\n");
					}
				}
				stringBuilder.Append("---\r\nSystemMetadata:\r\n");
				if (this.m_SystemMetadata != null)
				{
					foreach (string value2 in this.m_SystemMetadata)
					{
						stringBuilder.Append(value2).Append("\r\n");
					}
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x040021B4 RID: 8628
		private bool m_IsPrivateEntry;

		// Token: 0x040021B5 RID: 8629
		private long m_StreamSize;

		// Token: 0x040021B6 RID: 8630
		private DateTime m_ExpiresUtc;

		// Token: 0x040021B7 RID: 8631
		private int m_HitCount;

		// Token: 0x040021B8 RID: 8632
		private DateTime m_LastAccessedUtc;

		// Token: 0x040021B9 RID: 8633
		private DateTime m_LastModifiedUtc;

		// Token: 0x040021BA RID: 8634
		private DateTime m_LastSynchronizedUtc;

		// Token: 0x040021BB RID: 8635
		private TimeSpan m_MaxStale;

		// Token: 0x040021BC RID: 8636
		private int m_UsageCount;

		// Token: 0x040021BD RID: 8637
		private bool m_IsPartialEntry;

		// Token: 0x040021BE RID: 8638
		private StringCollection m_EntryMetadata;

		// Token: 0x040021BF RID: 8639
		private StringCollection m_SystemMetadata;
	}
}
