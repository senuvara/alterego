﻿using System;
using System.Collections.Specialized;
using System.IO;

namespace System.Net.Cache
{
	// Token: 0x02000531 RID: 1329
	internal abstract class RequestCache
	{
		// Token: 0x06002A0C RID: 10764 RVA: 0x00091D98 File Offset: 0x0008FF98
		protected RequestCache(bool isPrivateCache, bool canWrite)
		{
			this._IsPrivateCache = isPrivateCache;
			this._CanWrite = canWrite;
		}

		// Token: 0x17000A73 RID: 2675
		// (get) Token: 0x06002A0D RID: 10765 RVA: 0x00091DAE File Offset: 0x0008FFAE
		internal bool IsPrivateCache
		{
			get
			{
				return this._IsPrivateCache;
			}
		}

		// Token: 0x17000A74 RID: 2676
		// (get) Token: 0x06002A0E RID: 10766 RVA: 0x00091DB6 File Offset: 0x0008FFB6
		internal bool CanWrite
		{
			get
			{
				return this._CanWrite;
			}
		}

		// Token: 0x06002A0F RID: 10767
		internal abstract Stream Retrieve(string key, out RequestCacheEntry cacheEntry);

		// Token: 0x06002A10 RID: 10768
		internal abstract Stream Store(string key, long contentLength, DateTime expiresUtc, DateTime lastModifiedUtc, TimeSpan maxStale, StringCollection entryMetadata, StringCollection systemMetadata);

		// Token: 0x06002A11 RID: 10769
		internal abstract void Remove(string key);

		// Token: 0x06002A12 RID: 10770
		internal abstract void Update(string key, DateTime expiresUtc, DateTime lastModifiedUtc, DateTime lastSynchronizedUtc, TimeSpan maxStale, StringCollection entryMetadata, StringCollection systemMetadata);

		// Token: 0x06002A13 RID: 10771
		internal abstract bool TryRetrieve(string key, out RequestCacheEntry cacheEntry, out Stream readStream);

		// Token: 0x06002A14 RID: 10772
		internal abstract bool TryStore(string key, long contentLength, DateTime expiresUtc, DateTime lastModifiedUtc, TimeSpan maxStale, StringCollection entryMetadata, StringCollection systemMetadata, out Stream writeStream);

		// Token: 0x06002A15 RID: 10773
		internal abstract bool TryRemove(string key);

		// Token: 0x06002A16 RID: 10774
		internal abstract bool TryUpdate(string key, DateTime expiresUtc, DateTime lastModifiedUtc, DateTime lastSynchronizedUtc, TimeSpan maxStale, StringCollection entryMetadata, StringCollection systemMetadata);

		// Token: 0x06002A17 RID: 10775
		internal abstract void UnlockEntry(Stream retrieveStream);

		// Token: 0x06002A18 RID: 10776 RVA: 0x00091DBE File Offset: 0x0008FFBE
		// Note: this type is marked as 'beforefieldinit'.
		static RequestCache()
		{
		}

		// Token: 0x040021B1 RID: 8625
		internal static readonly char[] LineSplits = new char[]
		{
			'\r',
			'\n'
		};

		// Token: 0x040021B2 RID: 8626
		private bool _IsPrivateCache;

		// Token: 0x040021B3 RID: 8627
		private bool _CanWrite;
	}
}
