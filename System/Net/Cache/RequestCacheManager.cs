﻿using System;

namespace System.Net.Cache
{
	// Token: 0x02000533 RID: 1331
	internal sealed class RequestCacheManager
	{
		// Token: 0x06002A33 RID: 10803 RVA: 0x0000232F File Offset: 0x0000052F
		private RequestCacheManager()
		{
		}

		// Token: 0x06002A34 RID: 10804 RVA: 0x000921C8 File Offset: 0x000903C8
		internal static RequestCacheBinding GetBinding(string internedScheme)
		{
			if (internedScheme == null)
			{
				throw new ArgumentNullException("uriScheme");
			}
			if (RequestCacheManager.s_CacheConfigSettings == null)
			{
				RequestCacheManager.LoadConfigSettings();
			}
			if (RequestCacheManager.s_CacheConfigSettings.DisableAllCaching)
			{
				return RequestCacheManager.s_BypassCacheBinding;
			}
			if (internedScheme.Length == 0)
			{
				return RequestCacheManager.s_DefaultGlobalBinding;
			}
			if (internedScheme == Uri.UriSchemeHttp || internedScheme == Uri.UriSchemeHttps)
			{
				return RequestCacheManager.s_DefaultHttpBinding;
			}
			if (internedScheme == Uri.UriSchemeFtp)
			{
				return RequestCacheManager.s_DefaultFtpBinding;
			}
			return RequestCacheManager.s_BypassCacheBinding;
		}

		// Token: 0x17000A81 RID: 2689
		// (get) Token: 0x06002A35 RID: 10805 RVA: 0x00092242 File Offset: 0x00090442
		internal static bool IsCachingEnabled
		{
			get
			{
				if (RequestCacheManager.s_CacheConfigSettings == null)
				{
					RequestCacheManager.LoadConfigSettings();
				}
				return !RequestCacheManager.s_CacheConfigSettings.DisableAllCaching;
			}
		}

		// Token: 0x06002A36 RID: 10806 RVA: 0x00092264 File Offset: 0x00090464
		internal static void SetBinding(string uriScheme, RequestCacheBinding binding)
		{
			if (uriScheme == null)
			{
				throw new ArgumentNullException("uriScheme");
			}
			if (RequestCacheManager.s_CacheConfigSettings == null)
			{
				RequestCacheManager.LoadConfigSettings();
			}
			if (RequestCacheManager.s_CacheConfigSettings.DisableAllCaching)
			{
				return;
			}
			if (uriScheme.Length == 0)
			{
				RequestCacheManager.s_DefaultGlobalBinding = binding;
				return;
			}
			if (uriScheme == Uri.UriSchemeHttp || uriScheme == Uri.UriSchemeHttps)
			{
				RequestCacheManager.s_DefaultHttpBinding = binding;
				return;
			}
			if (uriScheme == Uri.UriSchemeFtp)
			{
				RequestCacheManager.s_DefaultFtpBinding = binding;
			}
		}

		// Token: 0x06002A37 RID: 10807 RVA: 0x000922E8 File Offset: 0x000904E8
		private static void LoadConfigSettings()
		{
			RequestCacheBinding obj = RequestCacheManager.s_BypassCacheBinding;
			lock (obj)
			{
				if (RequestCacheManager.s_CacheConfigSettings == null)
				{
					RequestCacheManager.s_CacheConfigSettings = new RequestCachingSectionInternal();
				}
			}
		}

		// Token: 0x06002A38 RID: 10808 RVA: 0x00092338 File Offset: 0x00090538
		// Note: this type is marked as 'beforefieldinit'.
		static RequestCacheManager()
		{
		}

		// Token: 0x040021C0 RID: 8640
		private static volatile RequestCachingSectionInternal s_CacheConfigSettings;

		// Token: 0x040021C1 RID: 8641
		private static readonly RequestCacheBinding s_BypassCacheBinding = new RequestCacheBinding(null, null, new RequestCachePolicy(RequestCacheLevel.BypassCache));

		// Token: 0x040021C2 RID: 8642
		private static volatile RequestCacheBinding s_DefaultGlobalBinding;

		// Token: 0x040021C3 RID: 8643
		private static volatile RequestCacheBinding s_DefaultHttpBinding;

		// Token: 0x040021C4 RID: 8644
		private static volatile RequestCacheBinding s_DefaultFtpBinding;
	}
}
