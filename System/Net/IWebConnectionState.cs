﻿using System;

namespace System.Net
{
	// Token: 0x020003F3 RID: 1011
	internal interface IWebConnectionState
	{
		// Token: 0x170006A5 RID: 1701
		// (get) Token: 0x06001F1E RID: 7966
		WebConnectionGroup Group { get; }

		// Token: 0x170006A6 RID: 1702
		// (get) Token: 0x06001F1F RID: 7967
		ServicePoint ServicePoint { get; }

		// Token: 0x170006A7 RID: 1703
		// (get) Token: 0x06001F20 RID: 7968
		bool Busy { get; }

		// Token: 0x170006A8 RID: 1704
		// (get) Token: 0x06001F21 RID: 7969
		DateTime IdleSince { get; }

		// Token: 0x06001F22 RID: 7970
		bool TrySetBusy();

		// Token: 0x06001F23 RID: 7971
		void SetIdle();
	}
}
