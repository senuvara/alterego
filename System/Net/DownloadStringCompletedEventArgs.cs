﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Net
{
	/// <summary>Provides data for the <see cref="E:System.Net.WebClient.DownloadStringCompleted" /> event.</summary>
	// Token: 0x020003A4 RID: 932
	public class DownloadStringCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06001B93 RID: 7059 RVA: 0x000649DD File Offset: 0x00062BDD
		internal DownloadStringCompletedEventArgs(string result, Exception exception, bool cancelled, object userToken) : base(exception, cancelled, userToken)
		{
			this.m_Result = result;
		}

		/// <summary>Gets the data that is downloaded by a <see cref="Overload:System.Net.WebClient.DownloadStringAsync" /> method.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the downloaded data.</returns>
		// Token: 0x1700059E RID: 1438
		// (get) Token: 0x06001B94 RID: 7060 RVA: 0x000649F0 File Offset: 0x00062BF0
		public string Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return this.m_Result;
			}
		}

		// Token: 0x06001B95 RID: 7061 RVA: 0x000092E2 File Offset: 0x000074E2
		internal DownloadStringCompletedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001920 RID: 6432
		private string m_Result;
	}
}
