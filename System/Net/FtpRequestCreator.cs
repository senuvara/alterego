﻿using System;

namespace System.Net
{
	// Token: 0x020003D8 RID: 984
	internal class FtpRequestCreator : IWebRequestCreate
	{
		// Token: 0x06001CFD RID: 7421 RVA: 0x00068AF4 File Offset: 0x00066CF4
		public WebRequest Create(Uri uri)
		{
			return new FtpWebRequest(uri);
		}

		// Token: 0x06001CFE RID: 7422 RVA: 0x0000232F File Offset: 0x0000052F
		public FtpRequestCreator()
		{
		}
	}
}
