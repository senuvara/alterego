﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Net
{
	// Token: 0x020003FD RID: 1021
	internal class RequestStream : Stream
	{
		// Token: 0x06001F5C RID: 8028 RVA: 0x00071DF7 File Offset: 0x0006FFF7
		internal RequestStream(Stream stream, byte[] buffer, int offset, int length) : this(stream, buffer, offset, length, -1L)
		{
		}

		// Token: 0x06001F5D RID: 8029 RVA: 0x00071E06 File Offset: 0x00070006
		internal RequestStream(Stream stream, byte[] buffer, int offset, int length, long contentlength)
		{
			this.stream = stream;
			this.buffer = buffer;
			this.offset = offset;
			this.length = length;
			this.remaining_body = contentlength;
		}

		// Token: 0x170006B8 RID: 1720
		// (get) Token: 0x06001F5E RID: 8030 RVA: 0x00003298 File Offset: 0x00001498
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170006B9 RID: 1721
		// (get) Token: 0x06001F5F RID: 8031 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170006BA RID: 1722
		// (get) Token: 0x06001F60 RID: 8032 RVA: 0x00005AFA File Offset: 0x00003CFA
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170006BB RID: 1723
		// (get) Token: 0x06001F61 RID: 8033 RVA: 0x00006740 File Offset: 0x00004940
		public override long Length
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x170006BC RID: 1724
		// (get) Token: 0x06001F62 RID: 8034 RVA: 0x00006740 File Offset: 0x00004940
		// (set) Token: 0x06001F63 RID: 8035 RVA: 0x00006740 File Offset: 0x00004940
		public override long Position
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x06001F64 RID: 8036 RVA: 0x00071E33 File Offset: 0x00070033
		public override void Close()
		{
			this.disposed = true;
		}

		// Token: 0x06001F65 RID: 8037 RVA: 0x0000232D File Offset: 0x0000052D
		public override void Flush()
		{
		}

		// Token: 0x06001F66 RID: 8038 RVA: 0x00071E3C File Offset: 0x0007003C
		private int FillFromBuffer(byte[] buffer, int off, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (off < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "< 0");
			}
			int num = buffer.Length;
			if (off > num)
			{
				throw new ArgumentException("destination offset is beyond array size");
			}
			if (off > num - count)
			{
				throw new ArgumentException("Reading would overrun buffer");
			}
			if (this.remaining_body == 0L)
			{
				return -1;
			}
			if (this.length == 0)
			{
				return 0;
			}
			int num2 = Math.Min(this.length, count);
			if (this.remaining_body > 0L)
			{
				num2 = (int)Math.Min((long)num2, this.remaining_body);
			}
			if (this.offset > this.buffer.Length - num2)
			{
				num2 = Math.Min(num2, this.buffer.Length - this.offset);
			}
			if (num2 == 0)
			{
				return 0;
			}
			Buffer.BlockCopy(this.buffer, this.offset, buffer, off, num2);
			this.offset += num2;
			this.length -= num2;
			if (this.remaining_body > 0L)
			{
				this.remaining_body -= (long)num2;
			}
			return num2;
		}

		// Token: 0x06001F67 RID: 8039 RVA: 0x00071F54 File Offset: 0x00070154
		public override int Read([In] [Out] byte[] buffer, int offset, int count)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(typeof(RequestStream).ToString());
			}
			int num = this.FillFromBuffer(buffer, offset, count);
			if (num == -1)
			{
				return 0;
			}
			if (num > 0)
			{
				return num;
			}
			num = this.stream.Read(buffer, offset, count);
			if (num > 0 && this.remaining_body > 0L)
			{
				this.remaining_body -= (long)num;
			}
			return num;
		}

		// Token: 0x06001F68 RID: 8040 RVA: 0x00071FC4 File Offset: 0x000701C4
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback cback, object state)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(typeof(RequestStream).ToString());
			}
			int num = this.FillFromBuffer(buffer, offset, count);
			if (num > 0 || num == -1)
			{
				HttpStreamAsyncResult httpStreamAsyncResult = new HttpStreamAsyncResult();
				httpStreamAsyncResult.Buffer = buffer;
				httpStreamAsyncResult.Offset = offset;
				httpStreamAsyncResult.Count = count;
				httpStreamAsyncResult.Callback = cback;
				httpStreamAsyncResult.State = state;
				httpStreamAsyncResult.SynchRead = Math.Max(0, num);
				httpStreamAsyncResult.Complete();
				return httpStreamAsyncResult;
			}
			if (this.remaining_body >= 0L && (long)count > this.remaining_body)
			{
				count = (int)Math.Min(2147483647L, this.remaining_body);
			}
			return this.stream.BeginRead(buffer, offset, count, cback, state);
		}

		// Token: 0x06001F69 RID: 8041 RVA: 0x00072078 File Offset: 0x00070278
		public override int EndRead(IAsyncResult ares)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(typeof(RequestStream).ToString());
			}
			if (ares == null)
			{
				throw new ArgumentNullException("async_result");
			}
			if (ares is HttpStreamAsyncResult)
			{
				HttpStreamAsyncResult httpStreamAsyncResult = (HttpStreamAsyncResult)ares;
				if (!ares.IsCompleted)
				{
					ares.AsyncWaitHandle.WaitOne();
				}
				return httpStreamAsyncResult.SynchRead;
			}
			int num = this.stream.EndRead(ares);
			if (this.remaining_body > 0L && num > 0)
			{
				this.remaining_body -= (long)num;
			}
			return num;
		}

		// Token: 0x06001F6A RID: 8042 RVA: 0x00006740 File Offset: 0x00004940
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001F6B RID: 8043 RVA: 0x00006740 File Offset: 0x00004940
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001F6C RID: 8044 RVA: 0x00006740 File Offset: 0x00004940
		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001F6D RID: 8045 RVA: 0x00006740 File Offset: 0x00004940
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback cback, object state)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06001F6E RID: 8046 RVA: 0x00006740 File Offset: 0x00004940
		public override void EndWrite(IAsyncResult async_result)
		{
			throw new NotSupportedException();
		}

		// Token: 0x04001ADF RID: 6879
		private byte[] buffer;

		// Token: 0x04001AE0 RID: 6880
		private int offset;

		// Token: 0x04001AE1 RID: 6881
		private int length;

		// Token: 0x04001AE2 RID: 6882
		private long remaining_body;

		// Token: 0x04001AE3 RID: 6883
		private bool disposed;

		// Token: 0x04001AE4 RID: 6884
		private Stream stream;
	}
}
