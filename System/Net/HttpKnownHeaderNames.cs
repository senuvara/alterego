﻿using System;

namespace System.Net
{
	// Token: 0x02000312 RID: 786
	internal static class HttpKnownHeaderNames
	{
		// Token: 0x040015F0 RID: 5616
		public const string CacheControl = "Cache-Control";

		// Token: 0x040015F1 RID: 5617
		public const string Connection = "Connection";

		// Token: 0x040015F2 RID: 5618
		public const string Date = "Date";

		// Token: 0x040015F3 RID: 5619
		public const string KeepAlive = "Keep-Alive";

		// Token: 0x040015F4 RID: 5620
		public const string Pragma = "Pragma";

		// Token: 0x040015F5 RID: 5621
		public const string ProxyConnection = "Proxy-Connection";

		// Token: 0x040015F6 RID: 5622
		public const string Trailer = "Trailer";

		// Token: 0x040015F7 RID: 5623
		public const string TransferEncoding = "Transfer-Encoding";

		// Token: 0x040015F8 RID: 5624
		public const string Upgrade = "Upgrade";

		// Token: 0x040015F9 RID: 5625
		public const string Via = "Via";

		// Token: 0x040015FA RID: 5626
		public const string Warning = "Warning";

		// Token: 0x040015FB RID: 5627
		public const string ContentLength = "Content-Length";

		// Token: 0x040015FC RID: 5628
		public const string ContentType = "Content-Type";

		// Token: 0x040015FD RID: 5629
		public const string ContentDisposition = "Content-Disposition";

		// Token: 0x040015FE RID: 5630
		public const string ContentEncoding = "Content-Encoding";

		// Token: 0x040015FF RID: 5631
		public const string ContentLanguage = "Content-Language";

		// Token: 0x04001600 RID: 5632
		public const string ContentLocation = "Content-Location";

		// Token: 0x04001601 RID: 5633
		public const string ContentRange = "Content-Range";

		// Token: 0x04001602 RID: 5634
		public const string Expires = "Expires";

		// Token: 0x04001603 RID: 5635
		public const string LastModified = "Last-Modified";

		// Token: 0x04001604 RID: 5636
		public const string Age = "Age";

		// Token: 0x04001605 RID: 5637
		public const string Location = "Location";

		// Token: 0x04001606 RID: 5638
		public const string ProxyAuthenticate = "Proxy-Authenticate";

		// Token: 0x04001607 RID: 5639
		public const string RetryAfter = "Retry-After";

		// Token: 0x04001608 RID: 5640
		public const string Server = "Server";

		// Token: 0x04001609 RID: 5641
		public const string SetCookie = "Set-Cookie";

		// Token: 0x0400160A RID: 5642
		public const string SetCookie2 = "Set-Cookie2";

		// Token: 0x0400160B RID: 5643
		public const string Vary = "Vary";

		// Token: 0x0400160C RID: 5644
		public const string WWWAuthenticate = "WWW-Authenticate";

		// Token: 0x0400160D RID: 5645
		public const string Accept = "Accept";

		// Token: 0x0400160E RID: 5646
		public const string AcceptCharset = "Accept-Charset";

		// Token: 0x0400160F RID: 5647
		public const string AcceptEncoding = "Accept-Encoding";

		// Token: 0x04001610 RID: 5648
		public const string AcceptLanguage = "Accept-Language";

		// Token: 0x04001611 RID: 5649
		public const string Authorization = "Authorization";

		// Token: 0x04001612 RID: 5650
		public const string Cookie = "Cookie";

		// Token: 0x04001613 RID: 5651
		public const string Cookie2 = "Cookie2";

		// Token: 0x04001614 RID: 5652
		public const string Expect = "Expect";

		// Token: 0x04001615 RID: 5653
		public const string From = "From";

		// Token: 0x04001616 RID: 5654
		public const string Host = "Host";

		// Token: 0x04001617 RID: 5655
		public const string IfMatch = "If-Match";

		// Token: 0x04001618 RID: 5656
		public const string IfModifiedSince = "If-Modified-Since";

		// Token: 0x04001619 RID: 5657
		public const string IfNoneMatch = "If-None-Match";

		// Token: 0x0400161A RID: 5658
		public const string IfRange = "If-Range";

		// Token: 0x0400161B RID: 5659
		public const string IfUnmodifiedSince = "If-Unmodified-Since";

		// Token: 0x0400161C RID: 5660
		public const string MaxForwards = "Max-Forwards";

		// Token: 0x0400161D RID: 5661
		public const string ProxyAuthorization = "Proxy-Authorization";

		// Token: 0x0400161E RID: 5662
		public const string Referer = "Referer";

		// Token: 0x0400161F RID: 5663
		public const string Range = "Range";

		// Token: 0x04001620 RID: 5664
		public const string UserAgent = "User-Agent";

		// Token: 0x04001621 RID: 5665
		public const string ContentMD5 = "Content-MD5";

		// Token: 0x04001622 RID: 5666
		public const string ETag = "ETag";

		// Token: 0x04001623 RID: 5667
		public const string TE = "TE";

		// Token: 0x04001624 RID: 5668
		public const string Allow = "Allow";

		// Token: 0x04001625 RID: 5669
		public const string AcceptRanges = "Accept-Ranges";

		// Token: 0x04001626 RID: 5670
		public const string P3P = "P3P";

		// Token: 0x04001627 RID: 5671
		public const string XPoweredBy = "X-Powered-By";

		// Token: 0x04001628 RID: 5672
		public const string XAspNetVersion = "X-AspNet-Version";

		// Token: 0x04001629 RID: 5673
		public const string SecWebSocketKey = "Sec-WebSocket-Key";

		// Token: 0x0400162A RID: 5674
		public const string SecWebSocketExtensions = "Sec-WebSocket-Extensions";

		// Token: 0x0400162B RID: 5675
		public const string SecWebSocketAccept = "Sec-WebSocket-Accept";

		// Token: 0x0400162C RID: 5676
		public const string Origin = "Origin";

		// Token: 0x0400162D RID: 5677
		public const string SecWebSocketProtocol = "Sec-WebSocket-Protocol";

		// Token: 0x0400162E RID: 5678
		public const string SecWebSocketVersion = "Sec-WebSocket-Version";
	}
}
