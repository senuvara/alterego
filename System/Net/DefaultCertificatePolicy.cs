﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace System.Net
{
	// Token: 0x020003C7 RID: 967
	internal class DefaultCertificatePolicy : ICertificatePolicy
	{
		// Token: 0x06001C4D RID: 7245 RVA: 0x00066205 File Offset: 0x00064405
		public bool CheckValidationResult(ServicePoint point, X509Certificate certificate, WebRequest request, int certificateProblem)
		{
			return ServicePointManager.ServerCertificateValidationCallback != null || (certificateProblem == -2146762495 || certificateProblem == 0);
		}

		// Token: 0x06001C4E RID: 7246 RVA: 0x0000232F File Offset: 0x0000052F
		public DefaultCertificatePolicy()
		{
		}
	}
}
