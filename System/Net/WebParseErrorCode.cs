﻿using System;

namespace System.Net
{
	// Token: 0x02000347 RID: 839
	internal enum WebParseErrorCode
	{
		// Token: 0x04001726 RID: 5926
		Generic,
		// Token: 0x04001727 RID: 5927
		InvalidHeaderName,
		// Token: 0x04001728 RID: 5928
		InvalidContentLength,
		// Token: 0x04001729 RID: 5929
		IncompleteHeaderLine,
		// Token: 0x0400172A RID: 5930
		CrLfError,
		// Token: 0x0400172B RID: 5931
		InvalidChunkFormat,
		// Token: 0x0400172C RID: 5932
		UnexpectedServerResponse
	}
}
