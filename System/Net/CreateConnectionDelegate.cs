﻿using System;

namespace System.Net
{
	// Token: 0x0200036D RID: 877
	// (Invoke) Token: 0x0600191B RID: 6427
	internal delegate PooledStream CreateConnectionDelegate(ConnectionPool pool);
}
