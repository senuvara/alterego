﻿using System;
using System.IO;
using System.Net.Mail;

namespace System.Net.Mime
{
	// Token: 0x02000445 RID: 1093
	internal class MimePart : MimeBasePart, IDisposable
	{
		// Token: 0x06002256 RID: 8790 RVA: 0x00080578 File Offset: 0x0007E778
		internal MimePart()
		{
		}

		// Token: 0x06002257 RID: 8791 RVA: 0x00080580 File Offset: 0x0007E780
		public void Dispose()
		{
			if (this.stream != null)
			{
				this.stream.Close();
			}
		}

		// Token: 0x1700076C RID: 1900
		// (get) Token: 0x06002258 RID: 8792 RVA: 0x00080595 File Offset: 0x0007E795
		internal Stream Stream
		{
			get
			{
				return this.stream;
			}
		}

		// Token: 0x1700076D RID: 1901
		// (get) Token: 0x06002259 RID: 8793 RVA: 0x0008059D File Offset: 0x0007E79D
		// (set) Token: 0x0600225A RID: 8794 RVA: 0x000805A5 File Offset: 0x0007E7A5
		internal ContentDisposition ContentDisposition
		{
			get
			{
				return this.contentDisposition;
			}
			set
			{
				this.contentDisposition = value;
				if (value == null)
				{
					((HeaderCollection)base.Headers).InternalRemove(MailHeaderInfo.GetString(MailHeaderID.ContentDisposition));
					return;
				}
				this.contentDisposition.PersistIfNeeded((HeaderCollection)base.Headers, true);
			}
		}

		// Token: 0x1700076E RID: 1902
		// (get) Token: 0x0600225B RID: 8795 RVA: 0x000805E0 File Offset: 0x0007E7E0
		// (set) Token: 0x0600225C RID: 8796 RVA: 0x00080640 File Offset: 0x0007E840
		internal TransferEncoding TransferEncoding
		{
			get
			{
				string text = base.Headers[MailHeaderInfo.GetString(MailHeaderID.ContentTransferEncoding)];
				if (text.Equals("base64", StringComparison.OrdinalIgnoreCase))
				{
					return TransferEncoding.Base64;
				}
				if (text.Equals("quoted-printable", StringComparison.OrdinalIgnoreCase))
				{
					return TransferEncoding.QuotedPrintable;
				}
				if (text.Equals("7bit", StringComparison.OrdinalIgnoreCase))
				{
					return TransferEncoding.SevenBit;
				}
				if (text.Equals("8bit", StringComparison.OrdinalIgnoreCase))
				{
					return TransferEncoding.EightBit;
				}
				return TransferEncoding.Unknown;
			}
			set
			{
				if (value == TransferEncoding.Base64)
				{
					base.Headers[MailHeaderInfo.GetString(MailHeaderID.ContentTransferEncoding)] = "base64";
					return;
				}
				if (value == TransferEncoding.QuotedPrintable)
				{
					base.Headers[MailHeaderInfo.GetString(MailHeaderID.ContentTransferEncoding)] = "quoted-printable";
					return;
				}
				if (value == TransferEncoding.SevenBit)
				{
					base.Headers[MailHeaderInfo.GetString(MailHeaderID.ContentTransferEncoding)] = "7bit";
					return;
				}
				if (value == TransferEncoding.EightBit)
				{
					base.Headers[MailHeaderInfo.GetString(MailHeaderID.ContentTransferEncoding)] = "8bit";
					return;
				}
				throw new NotSupportedException(SR.GetString("The MIME transfer encoding '{0}' is not supported.", new object[]
				{
					value
				}));
			}
		}

		// Token: 0x0600225D RID: 8797 RVA: 0x000806D8 File Offset: 0x0007E8D8
		internal void SetContent(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (this.streamSet)
			{
				this.stream.Close();
				this.stream = null;
				this.streamSet = false;
			}
			this.stream = stream;
			this.streamSet = true;
			this.streamUsedOnce = false;
			this.TransferEncoding = TransferEncoding.Base64;
		}

		// Token: 0x0600225E RID: 8798 RVA: 0x00080730 File Offset: 0x0007E930
		internal void SetContent(Stream stream, string name, string mimeType)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (mimeType != null && mimeType != string.Empty)
			{
				this.contentType = new ContentType(mimeType);
			}
			if (name != null && name != string.Empty)
			{
				base.ContentType.Name = name;
			}
			this.SetContent(stream);
		}

		// Token: 0x0600225F RID: 8799 RVA: 0x0008078A File Offset: 0x0007E98A
		internal void SetContent(Stream stream, ContentType contentType)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			this.contentType = contentType;
			this.SetContent(stream);
		}

		// Token: 0x06002260 RID: 8800 RVA: 0x000807A8 File Offset: 0x0007E9A8
		internal void Complete(IAsyncResult result, Exception e)
		{
			MimePart.MimePartContext mimePartContext = (MimePart.MimePartContext)result.AsyncState;
			if (mimePartContext.completed)
			{
				throw e;
			}
			try
			{
				if (mimePartContext.outputStream != null)
				{
					mimePartContext.outputStream.Close();
				}
			}
			catch (Exception ex)
			{
				if (e == null)
				{
					e = ex;
				}
			}
			mimePartContext.completed = true;
			mimePartContext.result.InvokeCallback(e);
		}

		// Token: 0x06002261 RID: 8801 RVA: 0x00080810 File Offset: 0x0007EA10
		internal void ReadCallback(IAsyncResult result)
		{
			if (result.CompletedSynchronously)
			{
				return;
			}
			((MimePart.MimePartContext)result.AsyncState).completedSynchronously = false;
			try
			{
				this.ReadCallbackHandler(result);
			}
			catch (Exception e)
			{
				this.Complete(result, e);
			}
		}

		// Token: 0x06002262 RID: 8802 RVA: 0x0008085C File Offset: 0x0007EA5C
		internal void ReadCallbackHandler(IAsyncResult result)
		{
			MimePart.MimePartContext mimePartContext = (MimePart.MimePartContext)result.AsyncState;
			mimePartContext.bytesLeft = this.Stream.EndRead(result);
			if (mimePartContext.bytesLeft > 0)
			{
				IAsyncResult asyncResult = mimePartContext.outputStream.BeginWrite(mimePartContext.buffer, 0, mimePartContext.bytesLeft, this.writeCallback, mimePartContext);
				if (asyncResult.CompletedSynchronously)
				{
					this.WriteCallbackHandler(asyncResult);
					return;
				}
			}
			else
			{
				this.Complete(result, null);
			}
		}

		// Token: 0x06002263 RID: 8803 RVA: 0x000808C8 File Offset: 0x0007EAC8
		internal void WriteCallback(IAsyncResult result)
		{
			if (result.CompletedSynchronously)
			{
				return;
			}
			((MimePart.MimePartContext)result.AsyncState).completedSynchronously = false;
			try
			{
				this.WriteCallbackHandler(result);
			}
			catch (Exception e)
			{
				this.Complete(result, e);
			}
		}

		// Token: 0x06002264 RID: 8804 RVA: 0x00080914 File Offset: 0x0007EB14
		internal void WriteCallbackHandler(IAsyncResult result)
		{
			MimePart.MimePartContext mimePartContext = (MimePart.MimePartContext)result.AsyncState;
			mimePartContext.outputStream.EndWrite(result);
			IAsyncResult asyncResult = this.Stream.BeginRead(mimePartContext.buffer, 0, mimePartContext.buffer.Length, this.readCallback, mimePartContext);
			if (asyncResult.CompletedSynchronously)
			{
				this.ReadCallbackHandler(asyncResult);
			}
		}

		// Token: 0x06002265 RID: 8805 RVA: 0x0008096C File Offset: 0x0007EB6C
		internal Stream GetEncodedStream(Stream stream)
		{
			Stream result = stream;
			if (this.TransferEncoding == TransferEncoding.Base64)
			{
				result = new Base64Stream(result, new Base64WriteStateInfo());
			}
			else if (this.TransferEncoding == TransferEncoding.QuotedPrintable)
			{
				result = new QuotedPrintableStream(result, true);
			}
			else if (this.TransferEncoding == TransferEncoding.SevenBit || this.TransferEncoding == TransferEncoding.EightBit)
			{
				result = new EightBitStream(result);
			}
			return result;
		}

		// Token: 0x06002266 RID: 8806 RVA: 0x000809C0 File Offset: 0x0007EBC0
		internal void ContentStreamCallbackHandler(IAsyncResult result)
		{
			MimePart.MimePartContext mimePartContext = (MimePart.MimePartContext)result.AsyncState;
			Stream stream = mimePartContext.writer.EndGetContentStream(result);
			mimePartContext.outputStream = this.GetEncodedStream(stream);
			this.readCallback = new AsyncCallback(this.ReadCallback);
			this.writeCallback = new AsyncCallback(this.WriteCallback);
			IAsyncResult asyncResult = this.Stream.BeginRead(mimePartContext.buffer, 0, mimePartContext.buffer.Length, this.readCallback, mimePartContext);
			if (asyncResult.CompletedSynchronously)
			{
				this.ReadCallbackHandler(asyncResult);
			}
		}

		// Token: 0x06002267 RID: 8807 RVA: 0x00080A48 File Offset: 0x0007EC48
		internal void ContentStreamCallback(IAsyncResult result)
		{
			if (result.CompletedSynchronously)
			{
				return;
			}
			((MimePart.MimePartContext)result.AsyncState).completedSynchronously = false;
			try
			{
				this.ContentStreamCallbackHandler(result);
			}
			catch (Exception e)
			{
				this.Complete(result, e);
			}
		}

		// Token: 0x06002268 RID: 8808 RVA: 0x00080A94 File Offset: 0x0007EC94
		internal override IAsyncResult BeginSend(BaseWriter writer, AsyncCallback callback, bool allowUnicode, object state)
		{
			base.PrepareHeaders(allowUnicode);
			writer.WriteHeaders(base.Headers, allowUnicode);
			MimeBasePart.MimePartAsyncResult result = new MimeBasePart.MimePartAsyncResult(this, state, callback);
			MimePart.MimePartContext state2 = new MimePart.MimePartContext(writer, result);
			this.ResetStream();
			this.streamUsedOnce = true;
			IAsyncResult asyncResult = writer.BeginGetContentStream(new AsyncCallback(this.ContentStreamCallback), state2);
			if (asyncResult.CompletedSynchronously)
			{
				this.ContentStreamCallbackHandler(asyncResult);
			}
			return result;
		}

		// Token: 0x06002269 RID: 8809 RVA: 0x00080AF8 File Offset: 0x0007ECF8
		internal override void Send(BaseWriter writer, bool allowUnicode)
		{
			if (this.Stream != null)
			{
				byte[] buffer = new byte[17408];
				base.PrepareHeaders(allowUnicode);
				writer.WriteHeaders(base.Headers, allowUnicode);
				Stream stream = writer.GetContentStream();
				stream = this.GetEncodedStream(stream);
				this.ResetStream();
				this.streamUsedOnce = true;
				int count;
				while ((count = this.Stream.Read(buffer, 0, 17408)) > 0)
				{
					stream.Write(buffer, 0, count);
				}
				stream.Close();
			}
		}

		// Token: 0x0600226A RID: 8810 RVA: 0x00080B70 File Offset: 0x0007ED70
		internal void ResetStream()
		{
			if (!this.streamUsedOnce)
			{
				return;
			}
			if (this.Stream.CanSeek)
			{
				this.Stream.Seek(0L, SeekOrigin.Begin);
				this.streamUsedOnce = false;
				return;
			}
			throw new InvalidOperationException(SR.GetString("One of the streams has already been used and can't be reset to the origin."));
		}

		// Token: 0x04001C96 RID: 7318
		private Stream stream;

		// Token: 0x04001C97 RID: 7319
		private bool streamSet;

		// Token: 0x04001C98 RID: 7320
		private bool streamUsedOnce;

		// Token: 0x04001C99 RID: 7321
		private AsyncCallback readCallback;

		// Token: 0x04001C9A RID: 7322
		private AsyncCallback writeCallback;

		// Token: 0x04001C9B RID: 7323
		private const int maxBufferSize = 17408;

		// Token: 0x02000446 RID: 1094
		internal class MimePartContext
		{
			// Token: 0x0600226B RID: 8811 RVA: 0x00080BAE File Offset: 0x0007EDAE
			internal MimePartContext(BaseWriter writer, LazyAsyncResult result)
			{
				this.writer = writer;
				this.result = result;
				this.buffer = new byte[17408];
			}

			// Token: 0x04001C9C RID: 7324
			internal Stream outputStream;

			// Token: 0x04001C9D RID: 7325
			internal LazyAsyncResult result;

			// Token: 0x04001C9E RID: 7326
			internal int bytesLeft;

			// Token: 0x04001C9F RID: 7327
			internal BaseWriter writer;

			// Token: 0x04001CA0 RID: 7328
			internal byte[] buffer;

			// Token: 0x04001CA1 RID: 7329
			internal bool completed;

			// Token: 0x04001CA2 RID: 7330
			internal bool completedSynchronously = true;
		}
	}
}
