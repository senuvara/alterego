﻿using System;
using System.IO;

namespace System.Net.Mime
{
	// Token: 0x0200043B RID: 1083
	internal class EightBitStream : DelegatedStream, IEncodableStream
	{
		// Token: 0x17000764 RID: 1892
		// (get) Token: 0x06002209 RID: 8713 RVA: 0x0007E968 File Offset: 0x0007CB68
		private WriteStateInfoBase WriteState
		{
			get
			{
				if (this.writeState == null)
				{
					this.writeState = new WriteStateInfoBase();
				}
				return this.writeState;
			}
		}

		// Token: 0x0600220A RID: 8714 RVA: 0x0007E983 File Offset: 0x0007CB83
		internal EightBitStream(Stream stream) : base(stream)
		{
		}

		// Token: 0x0600220B RID: 8715 RVA: 0x0007E98C File Offset: 0x0007CB8C
		internal EightBitStream(Stream stream, bool shouldEncodeLeadingDots) : this(stream)
		{
			this.shouldEncodeLeadingDots = shouldEncodeLeadingDots;
		}

		// Token: 0x0600220C RID: 8716 RVA: 0x0007E99C File Offset: 0x0007CB9C
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || offset >= buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (offset + count > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			IAsyncResult result;
			if (this.shouldEncodeLeadingDots)
			{
				this.EncodeLines(buffer, offset, count);
				result = base.BeginWrite(this.WriteState.Buffer, 0, this.WriteState.Length, callback, state);
			}
			else
			{
				result = base.BeginWrite(buffer, offset, count, callback, state);
			}
			return result;
		}

		// Token: 0x0600220D RID: 8717 RVA: 0x0007EA23 File Offset: 0x0007CC23
		public override void EndWrite(IAsyncResult asyncResult)
		{
			base.EndWrite(asyncResult);
			this.WriteState.BufferFlushed();
		}

		// Token: 0x0600220E RID: 8718 RVA: 0x0007EA38 File Offset: 0x0007CC38
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || offset >= buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (offset + count > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.shouldEncodeLeadingDots)
			{
				this.EncodeLines(buffer, offset, count);
				base.Write(this.WriteState.Buffer, 0, this.WriteState.Length);
				this.WriteState.BufferFlushed();
				return;
			}
			base.Write(buffer, offset, count);
		}

		// Token: 0x0600220F RID: 8719 RVA: 0x0007EAC0 File Offset: 0x0007CCC0
		private void EncodeLines(byte[] buffer, int offset, int count)
		{
			int num = offset;
			while (num < offset + count && num < buffer.Length)
			{
				if (buffer[num] == 13 && num + 1 < offset + count && buffer[num + 1] == 10)
				{
					this.WriteState.AppendCRLF(false);
					num++;
				}
				else if (this.WriteState.CurrentLineLength == 0 && buffer[num] == 46)
				{
					this.WriteState.Append(46);
					this.WriteState.Append(buffer[num]);
				}
				else
				{
					this.WriteState.Append(buffer[num]);
				}
				num++;
			}
		}

		// Token: 0x06002210 RID: 8720 RVA: 0x000068D7 File Offset: 0x00004AD7
		public int DecodeBytes(byte[] buffer, int offset, int count)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002211 RID: 8721 RVA: 0x000068D7 File Offset: 0x00004AD7
		public int EncodeBytes(byte[] buffer, int offset, int count)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002212 RID: 8722 RVA: 0x00002068 File Offset: 0x00000268
		public Stream GetStream()
		{
			return this;
		}

		// Token: 0x06002213 RID: 8723 RVA: 0x000068D7 File Offset: 0x00004AD7
		public string GetEncodedString()
		{
			throw new NotImplementedException();
		}

		// Token: 0x04001C65 RID: 7269
		private WriteStateInfoBase writeState;

		// Token: 0x04001C66 RID: 7270
		private bool shouldEncodeLeadingDots;
	}
}
