﻿using System;
using System.IO;
using System.Text;

namespace System.Net.Mime
{
	// Token: 0x0200043C RID: 1084
	internal class EncodedStreamFactory
	{
		// Token: 0x17000765 RID: 1893
		// (get) Token: 0x06002214 RID: 8724 RVA: 0x0007EB4A File Offset: 0x0007CD4A
		internal static int DefaultMaxLineLength
		{
			get
			{
				return 70;
			}
		}

		// Token: 0x06002215 RID: 8725 RVA: 0x0007EB4E File Offset: 0x0007CD4E
		internal IEncodableStream GetEncoder(TransferEncoding encoding, Stream stream)
		{
			if (encoding == TransferEncoding.Base64)
			{
				return new Base64Stream(stream, new Base64WriteStateInfo());
			}
			if (encoding == TransferEncoding.QuotedPrintable)
			{
				return new QuotedPrintableStream(stream, true);
			}
			if (encoding == TransferEncoding.SevenBit || encoding == TransferEncoding.EightBit)
			{
				return new EightBitStream(stream);
			}
			throw new NotSupportedException("Encoding Stream");
		}

		// Token: 0x06002216 RID: 8726 RVA: 0x0007EB84 File Offset: 0x0007CD84
		internal IEncodableStream GetEncoderForHeader(Encoding encoding, bool useBase64Encoding, int headerTextLength)
		{
			byte[] header = this.CreateHeader(encoding, useBase64Encoding);
			byte[] footer = this.CreateFooter();
			if (useBase64Encoding)
			{
				return new Base64Stream((Base64WriteStateInfo)new Base64WriteStateInfo(1024, header, footer, EncodedStreamFactory.DefaultMaxLineLength, headerTextLength));
			}
			return new QEncodedStream(new WriteStateInfoBase(1024, header, footer, EncodedStreamFactory.DefaultMaxLineLength, headerTextLength));
		}

		// Token: 0x06002217 RID: 8727 RVA: 0x0007EBD8 File Offset: 0x0007CDD8
		protected byte[] CreateHeader(Encoding encoding, bool useBase64Encoding)
		{
			string s = string.Format("=?{0}?{1}?", encoding.HeaderName, useBase64Encoding ? "B" : "Q");
			return Encoding.ASCII.GetBytes(s);
		}

		// Token: 0x06002218 RID: 8728 RVA: 0x0007EC10 File Offset: 0x0007CE10
		protected byte[] CreateFooter()
		{
			return new byte[]
			{
				63,
				61
			};
		}

		// Token: 0x06002219 RID: 8729 RVA: 0x0000232F File Offset: 0x0000052F
		public EncodedStreamFactory()
		{
		}

		// Token: 0x04001C67 RID: 7271
		private const int defaultMaxLineLength = 70;

		// Token: 0x04001C68 RID: 7272
		private const int initialBufferSize = 1024;
	}
}
