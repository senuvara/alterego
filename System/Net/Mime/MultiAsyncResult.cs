﻿using System;
using System.Threading;

namespace System.Net.Mime
{
	// Token: 0x02000448 RID: 1096
	internal class MultiAsyncResult : LazyAsyncResult
	{
		// Token: 0x06002275 RID: 8821 RVA: 0x00080DCC File Offset: 0x0007EFCC
		internal MultiAsyncResult(object context, AsyncCallback callback, object state) : base(context, state, callback)
		{
			this.context = context;
		}

		// Token: 0x1700076F RID: 1903
		// (get) Token: 0x06002276 RID: 8822 RVA: 0x00080DDE File Offset: 0x0007EFDE
		internal object Context
		{
			get
			{
				return this.context;
			}
		}

		// Token: 0x06002277 RID: 8823 RVA: 0x00080DE6 File Offset: 0x0007EFE6
		internal void Enter()
		{
			this.Increment();
		}

		// Token: 0x06002278 RID: 8824 RVA: 0x00080DEE File Offset: 0x0007EFEE
		internal void Leave()
		{
			this.Decrement();
		}

		// Token: 0x06002279 RID: 8825 RVA: 0x00080DF6 File Offset: 0x0007EFF6
		internal void Leave(object result)
		{
			base.Result = result;
			this.Decrement();
		}

		// Token: 0x0600227A RID: 8826 RVA: 0x00080E05 File Offset: 0x0007F005
		private void Decrement()
		{
			if (Interlocked.Decrement(ref this.outstanding) == -1)
			{
				base.InvokeCallback(base.Result);
			}
		}

		// Token: 0x0600227B RID: 8827 RVA: 0x00080E21 File Offset: 0x0007F021
		private void Increment()
		{
			Interlocked.Increment(ref this.outstanding);
		}

		// Token: 0x0600227C RID: 8828 RVA: 0x00080DEE File Offset: 0x0007EFEE
		internal void CompleteSequence()
		{
			this.Decrement();
		}

		// Token: 0x0600227D RID: 8829 RVA: 0x00080E2F File Offset: 0x0007F02F
		internal static object End(IAsyncResult result)
		{
			MultiAsyncResult multiAsyncResult = (MultiAsyncResult)result;
			multiAsyncResult.InternalWaitForCompletion();
			return multiAsyncResult.Result;
		}

		// Token: 0x04001CA6 RID: 7334
		private int outstanding;

		// Token: 0x04001CA7 RID: 7335
		private object context;
	}
}
