﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net.Mail;

namespace System.Net.Mime
{
	// Token: 0x02000436 RID: 1078
	internal abstract class BaseWriter
	{
		// Token: 0x060021C6 RID: 8646 RVA: 0x0007D828 File Offset: 0x0007BA28
		protected BaseWriter(Stream stream, bool shouldEncodeLeadingDots)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			this.stream = stream;
			this.shouldEncodeLeadingDots = shouldEncodeLeadingDots;
			this.onCloseHandler = new EventHandler(this.OnClose);
			this.bufferBuilder = new BufferBuilder();
			this.lineLength = BaseWriter.DefaultLineLength;
		}

		// Token: 0x060021C7 RID: 8647
		internal abstract void WriteHeaders(NameValueCollection headers, bool allowUnicode);

		// Token: 0x060021C8 RID: 8648 RVA: 0x0007D880 File Offset: 0x0007BA80
		internal void WriteHeader(string name, string value, bool allowUnicode)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (this.isInContent)
			{
				throw new InvalidOperationException(SR.GetString("This operation cannot be performed while in content."));
			}
			this.CheckBoundary();
			this.bufferBuilder.Append(name);
			this.bufferBuilder.Append(": ");
			this.WriteAndFold(value, name.Length + 2, allowUnicode);
			this.bufferBuilder.Append(BaseWriter.CRLF);
		}

		// Token: 0x060021C9 RID: 8649 RVA: 0x0007D904 File Offset: 0x0007BB04
		private void WriteAndFold(string value, int charsAlreadyOnLine, bool allowUnicode)
		{
			int num = 0;
			int num2 = 0;
			for (int i = 0; i < value.Length; i++)
			{
				if (MailBnfHelper.IsFWSAt(value, i))
				{
					i += 2;
					this.bufferBuilder.Append(value, num2, i - num2, allowUnicode);
					num2 = i;
					num = i;
					charsAlreadyOnLine = 0;
				}
				else if (i - num2 > this.lineLength - charsAlreadyOnLine && num != num2)
				{
					this.bufferBuilder.Append(value, num2, num - num2, allowUnicode);
					this.bufferBuilder.Append(BaseWriter.CRLF);
					num2 = num;
					charsAlreadyOnLine = 0;
				}
				else if (value[i] == MailBnfHelper.Space || value[i] == MailBnfHelper.Tab)
				{
					num = i;
				}
			}
			if (value.Length - num2 > 0)
			{
				this.bufferBuilder.Append(value, num2, value.Length - num2, allowUnicode);
			}
		}

		// Token: 0x060021CA RID: 8650 RVA: 0x0007D9CB File Offset: 0x0007BBCB
		internal Stream GetContentStream()
		{
			return this.GetContentStream(null);
		}

		// Token: 0x060021CB RID: 8651 RVA: 0x0007D9D4 File Offset: 0x0007BBD4
		private Stream GetContentStream(MultiAsyncResult multiResult)
		{
			if (this.isInContent)
			{
				throw new InvalidOperationException(SR.GetString("This operation cannot be performed while in content."));
			}
			this.isInContent = true;
			this.CheckBoundary();
			this.bufferBuilder.Append(BaseWriter.CRLF);
			this.Flush(multiResult);
			ClosableStream result = new ClosableStream(new EightBitStream(this.stream, this.shouldEncodeLeadingDots), this.onCloseHandler);
			this.contentStream = result;
			return result;
		}

		// Token: 0x060021CC RID: 8652 RVA: 0x0007DA44 File Offset: 0x0007BC44
		internal IAsyncResult BeginGetContentStream(AsyncCallback callback, object state)
		{
			MultiAsyncResult multiAsyncResult = new MultiAsyncResult(this, callback, state);
			Stream result = this.GetContentStream(multiAsyncResult);
			if (!(multiAsyncResult.Result is Exception))
			{
				multiAsyncResult.Result = result;
			}
			multiAsyncResult.CompleteSequence();
			return multiAsyncResult;
		}

		// Token: 0x060021CD RID: 8653 RVA: 0x0007DA80 File Offset: 0x0007BC80
		internal Stream EndGetContentStream(IAsyncResult result)
		{
			object obj = MultiAsyncResult.End(result);
			if (obj is Exception)
			{
				throw (Exception)obj;
			}
			return (Stream)obj;
		}

		// Token: 0x060021CE RID: 8654 RVA: 0x0007DAAC File Offset: 0x0007BCAC
		protected void Flush(MultiAsyncResult multiResult)
		{
			if (this.bufferBuilder.Length > 0)
			{
				if (multiResult != null)
				{
					multiResult.Enter();
					IAsyncResult asyncResult = this.stream.BeginWrite(this.bufferBuilder.GetBuffer(), 0, this.bufferBuilder.Length, BaseWriter.onWrite, multiResult);
					if (asyncResult.CompletedSynchronously)
					{
						this.stream.EndWrite(asyncResult);
						multiResult.Leave();
					}
				}
				else
				{
					this.stream.Write(this.bufferBuilder.GetBuffer(), 0, this.bufferBuilder.Length);
				}
				this.bufferBuilder.Reset();
			}
		}

		// Token: 0x060021CF RID: 8655 RVA: 0x0007DB44 File Offset: 0x0007BD44
		protected static void OnWrite(IAsyncResult result)
		{
			if (!result.CompletedSynchronously)
			{
				MultiAsyncResult multiAsyncResult = (MultiAsyncResult)result.AsyncState;
				BaseWriter baseWriter = (BaseWriter)multiAsyncResult.Context;
				try
				{
					baseWriter.stream.EndWrite(result);
					multiAsyncResult.Leave();
				}
				catch (Exception result2)
				{
					multiAsyncResult.Leave(result2);
				}
			}
		}

		// Token: 0x060021D0 RID: 8656
		internal abstract void Close();

		// Token: 0x060021D1 RID: 8657
		protected abstract void OnClose(object sender, EventArgs args);

		// Token: 0x060021D2 RID: 8658 RVA: 0x0000232D File Offset: 0x0000052D
		protected virtual void CheckBoundary()
		{
		}

		// Token: 0x060021D3 RID: 8659 RVA: 0x0007DBA0 File Offset: 0x0007BDA0
		// Note: this type is marked as 'beforefieldinit'.
		static BaseWriter()
		{
		}

		// Token: 0x04001C44 RID: 7236
		private static int DefaultLineLength = 76;

		// Token: 0x04001C45 RID: 7237
		private static AsyncCallback onWrite = new AsyncCallback(BaseWriter.OnWrite);

		// Token: 0x04001C46 RID: 7238
		protected static byte[] CRLF = new byte[]
		{
			13,
			10
		};

		// Token: 0x04001C47 RID: 7239
		protected BufferBuilder bufferBuilder;

		// Token: 0x04001C48 RID: 7240
		protected Stream contentStream;

		// Token: 0x04001C49 RID: 7241
		protected bool isInContent;

		// Token: 0x04001C4A RID: 7242
		protected Stream stream;

		// Token: 0x04001C4B RID: 7243
		private int lineLength;

		// Token: 0x04001C4C RID: 7244
		private EventHandler onCloseHandler;

		// Token: 0x04001C4D RID: 7245
		private bool shouldEncodeLeadingDots;
	}
}
