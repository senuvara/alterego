﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Threading;

namespace System.Net.Mime
{
	// Token: 0x02000442 RID: 1090
	internal class MimeMultiPart : MimeBasePart
	{
		// Token: 0x06002247 RID: 8775 RVA: 0x000800BF File Offset: 0x0007E2BF
		internal MimeMultiPart(MimeMultiPartType type)
		{
			this.MimeMultiPartType = type;
		}

		// Token: 0x1700076A RID: 1898
		// (set) Token: 0x06002248 RID: 8776 RVA: 0x000800CE File Offset: 0x0007E2CE
		internal MimeMultiPartType MimeMultiPartType
		{
			set
			{
				if (value > MimeMultiPartType.Related || value < MimeMultiPartType.Mixed)
				{
					throw new NotSupportedException(value.ToString());
				}
				this.SetType(value);
			}
		}

		// Token: 0x06002249 RID: 8777 RVA: 0x000800F2 File Offset: 0x0007E2F2
		private void SetType(MimeMultiPartType type)
		{
			base.ContentType.MediaType = "multipart/" + type.ToString().ToLower(CultureInfo.InvariantCulture);
			base.ContentType.Boundary = this.GetNextBoundary();
		}

		// Token: 0x1700076B RID: 1899
		// (get) Token: 0x0600224A RID: 8778 RVA: 0x00080131 File Offset: 0x0007E331
		internal Collection<MimeBasePart> Parts
		{
			get
			{
				if (this.parts == null)
				{
					this.parts = new Collection<MimeBasePart>();
				}
				return this.parts;
			}
		}

		// Token: 0x0600224B RID: 8779 RVA: 0x0008014C File Offset: 0x0007E34C
		internal void Complete(IAsyncResult result, Exception e)
		{
			MimeMultiPart.MimePartContext mimePartContext = (MimeMultiPart.MimePartContext)result.AsyncState;
			if (mimePartContext.completed)
			{
				throw e;
			}
			try
			{
				mimePartContext.outputStream.Close();
			}
			catch (Exception ex)
			{
				if (e == null)
				{
					e = ex;
				}
			}
			mimePartContext.completed = true;
			mimePartContext.result.InvokeCallback(e);
		}

		// Token: 0x0600224C RID: 8780 RVA: 0x000801AC File Offset: 0x0007E3AC
		internal void MimeWriterCloseCallback(IAsyncResult result)
		{
			if (result.CompletedSynchronously)
			{
				return;
			}
			((MimeMultiPart.MimePartContext)result.AsyncState).completedSynchronously = false;
			try
			{
				this.MimeWriterCloseCallbackHandler(result);
			}
			catch (Exception e)
			{
				this.Complete(result, e);
			}
		}

		// Token: 0x0600224D RID: 8781 RVA: 0x000801F8 File Offset: 0x0007E3F8
		private void MimeWriterCloseCallbackHandler(IAsyncResult result)
		{
			((MimeWriter)((MimeMultiPart.MimePartContext)result.AsyncState).writer).EndClose(result);
			this.Complete(result, null);
		}

		// Token: 0x0600224E RID: 8782 RVA: 0x00080220 File Offset: 0x0007E420
		internal void MimePartSentCallback(IAsyncResult result)
		{
			if (result.CompletedSynchronously)
			{
				return;
			}
			((MimeMultiPart.MimePartContext)result.AsyncState).completedSynchronously = false;
			try
			{
				this.MimePartSentCallbackHandler(result);
			}
			catch (Exception e)
			{
				this.Complete(result, e);
			}
		}

		// Token: 0x0600224F RID: 8783 RVA: 0x0008026C File Offset: 0x0007E46C
		private void MimePartSentCallbackHandler(IAsyncResult result)
		{
			MimeMultiPart.MimePartContext mimePartContext = (MimeMultiPart.MimePartContext)result.AsyncState;
			mimePartContext.partsEnumerator.Current.EndSend(result);
			if (mimePartContext.partsEnumerator.MoveNext())
			{
				IAsyncResult asyncResult = mimePartContext.partsEnumerator.Current.BeginSend(mimePartContext.writer, this.mimePartSentCallback, this.allowUnicode, mimePartContext);
				if (asyncResult.CompletedSynchronously)
				{
					this.MimePartSentCallbackHandler(asyncResult);
				}
				return;
			}
			IAsyncResult asyncResult2 = ((MimeWriter)mimePartContext.writer).BeginClose(new AsyncCallback(this.MimeWriterCloseCallback), mimePartContext);
			if (asyncResult2.CompletedSynchronously)
			{
				this.MimeWriterCloseCallbackHandler(asyncResult2);
			}
		}

		// Token: 0x06002250 RID: 8784 RVA: 0x00080304 File Offset: 0x0007E504
		internal void ContentStreamCallback(IAsyncResult result)
		{
			if (result.CompletedSynchronously)
			{
				return;
			}
			((MimeMultiPart.MimePartContext)result.AsyncState).completedSynchronously = false;
			try
			{
				this.ContentStreamCallbackHandler(result);
			}
			catch (Exception e)
			{
				this.Complete(result, e);
			}
		}

		// Token: 0x06002251 RID: 8785 RVA: 0x00080350 File Offset: 0x0007E550
		private void ContentStreamCallbackHandler(IAsyncResult result)
		{
			MimeMultiPart.MimePartContext mimePartContext = (MimeMultiPart.MimePartContext)result.AsyncState;
			mimePartContext.outputStream = mimePartContext.writer.EndGetContentStream(result);
			mimePartContext.writer = new MimeWriter(mimePartContext.outputStream, base.ContentType.Boundary);
			if (mimePartContext.partsEnumerator.MoveNext())
			{
				MimeBasePart mimeBasePart = mimePartContext.partsEnumerator.Current;
				this.mimePartSentCallback = new AsyncCallback(this.MimePartSentCallback);
				IAsyncResult asyncResult = mimeBasePart.BeginSend(mimePartContext.writer, this.mimePartSentCallback, this.allowUnicode, mimePartContext);
				if (asyncResult.CompletedSynchronously)
				{
					this.MimePartSentCallbackHandler(asyncResult);
				}
				return;
			}
			IAsyncResult asyncResult2 = ((MimeWriter)mimePartContext.writer).BeginClose(new AsyncCallback(this.MimeWriterCloseCallback), mimePartContext);
			if (asyncResult2.CompletedSynchronously)
			{
				this.MimeWriterCloseCallbackHandler(asyncResult2);
			}
		}

		// Token: 0x06002252 RID: 8786 RVA: 0x00080418 File Offset: 0x0007E618
		internal override IAsyncResult BeginSend(BaseWriter writer, AsyncCallback callback, bool allowUnicode, object state)
		{
			this.allowUnicode = allowUnicode;
			base.PrepareHeaders(allowUnicode);
			writer.WriteHeaders(base.Headers, allowUnicode);
			MimeBasePart.MimePartAsyncResult result = new MimeBasePart.MimePartAsyncResult(this, state, callback);
			MimeMultiPart.MimePartContext state2 = new MimeMultiPart.MimePartContext(writer, result, this.Parts.GetEnumerator());
			IAsyncResult asyncResult = writer.BeginGetContentStream(new AsyncCallback(this.ContentStreamCallback), state2);
			if (asyncResult.CompletedSynchronously)
			{
				this.ContentStreamCallbackHandler(asyncResult);
			}
			return result;
		}

		// Token: 0x06002253 RID: 8787 RVA: 0x00080484 File Offset: 0x0007E684
		internal override void Send(BaseWriter writer, bool allowUnicode)
		{
			base.PrepareHeaders(allowUnicode);
			writer.WriteHeaders(base.Headers, allowUnicode);
			Stream contentStream = writer.GetContentStream();
			MimeWriter mimeWriter = new MimeWriter(contentStream, base.ContentType.Boundary);
			foreach (MimeBasePart mimeBasePart in this.Parts)
			{
				mimeBasePart.Send(mimeWriter, allowUnicode);
			}
			mimeWriter.Close();
			contentStream.Close();
		}

		// Token: 0x06002254 RID: 8788 RVA: 0x0008050C File Offset: 0x0007E70C
		internal string GetNextBoundary()
		{
			return "--boundary_" + (Interlocked.Increment(ref MimeMultiPart.boundary) - 1).ToString(CultureInfo.InvariantCulture) + "_" + Guid.NewGuid().ToString(null, CultureInfo.InvariantCulture);
		}

		// Token: 0x04001C86 RID: 7302
		private Collection<MimeBasePart> parts;

		// Token: 0x04001C87 RID: 7303
		private static int boundary;

		// Token: 0x04001C88 RID: 7304
		private AsyncCallback mimePartSentCallback;

		// Token: 0x04001C89 RID: 7305
		private bool allowUnicode;

		// Token: 0x02000443 RID: 1091
		internal class MimePartContext
		{
			// Token: 0x06002255 RID: 8789 RVA: 0x00080554 File Offset: 0x0007E754
			internal MimePartContext(BaseWriter writer, LazyAsyncResult result, IEnumerator<MimeBasePart> partsEnumerator)
			{
				this.writer = writer;
				this.result = result;
				this.partsEnumerator = partsEnumerator;
			}

			// Token: 0x04001C8A RID: 7306
			internal IEnumerator<MimeBasePart> partsEnumerator;

			// Token: 0x04001C8B RID: 7307
			internal Stream outputStream;

			// Token: 0x04001C8C RID: 7308
			internal LazyAsyncResult result;

			// Token: 0x04001C8D RID: 7309
			internal BaseWriter writer;

			// Token: 0x04001C8E RID: 7310
			internal bool completed;

			// Token: 0x04001C8F RID: 7311
			internal bool completedSynchronously = true;
		}
	}
}
