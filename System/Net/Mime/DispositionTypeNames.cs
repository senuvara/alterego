﻿using System;

namespace System.Net.Mime
{
	/// <summary>Supplies the strings used to specify the disposition type for an e-mail attachment.</summary>
	// Token: 0x0200043A RID: 1082
	public static class DispositionTypeNames
	{
		/// <summary>Specifies that the attachment is to be displayed as part of the e-mail message body.</summary>
		// Token: 0x04001C63 RID: 7267
		public const string Inline = "inline";

		/// <summary>Specifies that the attachment is to be displayed as a file attached to the e-mail message.</summary>
		// Token: 0x04001C64 RID: 7268
		public const string Attachment = "attachment";
	}
}
