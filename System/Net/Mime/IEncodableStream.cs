﻿using System;
using System.IO;

namespace System.Net.Mime
{
	// Token: 0x0200043E RID: 1086
	internal interface IEncodableStream
	{
		// Token: 0x06002223 RID: 8739
		int DecodeBytes(byte[] buffer, int offset, int count);

		// Token: 0x06002224 RID: 8740
		int EncodeBytes(byte[] buffer, int offset, int count);

		// Token: 0x06002225 RID: 8741
		string GetEncodedString();

		// Token: 0x06002226 RID: 8742
		Stream GetStream();
	}
}
