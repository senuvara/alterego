﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Net.Mime
{
	// Token: 0x0200043F RID: 1087
	internal static class MailBnfHelper
	{
		// Token: 0x06002227 RID: 8743 RVA: 0x0007F084 File Offset: 0x0007D284
		static MailBnfHelper()
		{
			MailBnfHelper.Whitespace = new List<char>();
			MailBnfHelper.Whitespace.Add(MailBnfHelper.Tab);
			MailBnfHelper.Whitespace.Add(MailBnfHelper.Space);
			MailBnfHelper.Whitespace.Add(MailBnfHelper.CR);
			MailBnfHelper.Whitespace.Add(MailBnfHelper.LF);
			for (int i = 48; i <= 57; i++)
			{
				MailBnfHelper.Atext[i] = true;
			}
			for (int j = 65; j <= 90; j++)
			{
				MailBnfHelper.Atext[j] = true;
			}
			for (int k = 97; k <= 122; k++)
			{
				MailBnfHelper.Atext[k] = true;
			}
			MailBnfHelper.Atext[33] = true;
			MailBnfHelper.Atext[35] = true;
			MailBnfHelper.Atext[36] = true;
			MailBnfHelper.Atext[37] = true;
			MailBnfHelper.Atext[38] = true;
			MailBnfHelper.Atext[39] = true;
			MailBnfHelper.Atext[42] = true;
			MailBnfHelper.Atext[43] = true;
			MailBnfHelper.Atext[45] = true;
			MailBnfHelper.Atext[47] = true;
			MailBnfHelper.Atext[61] = true;
			MailBnfHelper.Atext[63] = true;
			MailBnfHelper.Atext[94] = true;
			MailBnfHelper.Atext[95] = true;
			MailBnfHelper.Atext[96] = true;
			MailBnfHelper.Atext[123] = true;
			MailBnfHelper.Atext[124] = true;
			MailBnfHelper.Atext[125] = true;
			MailBnfHelper.Atext[126] = true;
			for (int l = 1; l <= 9; l++)
			{
				MailBnfHelper.Qtext[l] = true;
			}
			MailBnfHelper.Qtext[11] = true;
			MailBnfHelper.Qtext[12] = true;
			for (int m = 14; m <= 33; m++)
			{
				MailBnfHelper.Qtext[m] = true;
			}
			for (int n = 35; n <= 91; n++)
			{
				MailBnfHelper.Qtext[n] = true;
			}
			for (int num = 93; num <= 127; num++)
			{
				MailBnfHelper.Qtext[num] = true;
			}
			for (int num2 = 1; num2 <= 8; num2++)
			{
				MailBnfHelper.Dtext[num2] = true;
			}
			MailBnfHelper.Dtext[11] = true;
			MailBnfHelper.Dtext[12] = true;
			for (int num3 = 14; num3 <= 31; num3++)
			{
				MailBnfHelper.Dtext[num3] = true;
			}
			for (int num4 = 33; num4 <= 90; num4++)
			{
				MailBnfHelper.Dtext[num4] = true;
			}
			for (int num5 = 94; num5 <= 127; num5++)
			{
				MailBnfHelper.Dtext[num5] = true;
			}
			for (int num6 = 33; num6 <= 57; num6++)
			{
				MailBnfHelper.Ftext[num6] = true;
			}
			for (int num7 = 59; num7 <= 126; num7++)
			{
				MailBnfHelper.Ftext[num7] = true;
			}
			for (int num8 = 33; num8 <= 126; num8++)
			{
				MailBnfHelper.Ttext[num8] = true;
			}
			MailBnfHelper.Ttext[40] = false;
			MailBnfHelper.Ttext[41] = false;
			MailBnfHelper.Ttext[60] = false;
			MailBnfHelper.Ttext[62] = false;
			MailBnfHelper.Ttext[64] = false;
			MailBnfHelper.Ttext[44] = false;
			MailBnfHelper.Ttext[59] = false;
			MailBnfHelper.Ttext[58] = false;
			MailBnfHelper.Ttext[92] = false;
			MailBnfHelper.Ttext[34] = false;
			MailBnfHelper.Ttext[47] = false;
			MailBnfHelper.Ttext[91] = false;
			MailBnfHelper.Ttext[93] = false;
			MailBnfHelper.Ttext[63] = false;
			MailBnfHelper.Ttext[61] = false;
			for (int num9 = 1; num9 <= 8; num9++)
			{
				MailBnfHelper.Ctext[num9] = true;
			}
			MailBnfHelper.Ctext[11] = true;
			MailBnfHelper.Ctext[12] = true;
			for (int num10 = 14; num10 <= 31; num10++)
			{
				MailBnfHelper.Ctext[num10] = true;
			}
			for (int num11 = 33; num11 <= 39; num11++)
			{
				MailBnfHelper.Ctext[num11] = true;
			}
			for (int num12 = 42; num12 <= 91; num12++)
			{
				MailBnfHelper.Ctext[num12] = true;
			}
			for (int num13 = 93; num13 <= 127; num13++)
			{
				MailBnfHelper.Ctext[num13] = true;
			}
		}

		// Token: 0x06002228 RID: 8744 RVA: 0x0007F564 File Offset: 0x0007D764
		internal static bool SkipCFWS(string data, ref int offset)
		{
			int num = 0;
			while (offset < data.Length)
			{
				if (data[offset] > '\u007f')
				{
					throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'.", new object[]
					{
						data[offset]
					}));
				}
				if (data[offset] == '\\' && num > 0)
				{
					offset += 2;
				}
				else if (data[offset] == '(')
				{
					num++;
				}
				else if (data[offset] == ')')
				{
					num--;
				}
				else if (data[offset] != ' ' && data[offset] != '\t' && num == 0)
				{
					return true;
				}
				if (num < 0)
				{
					throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'.", new object[]
					{
						data[offset]
					}));
				}
				offset++;
			}
			return false;
		}

		// Token: 0x06002229 RID: 8745 RVA: 0x0007F644 File Offset: 0x0007D844
		internal static void ValidateHeaderName(string data)
		{
			int i;
			for (i = 0; i < data.Length; i++)
			{
				if ((int)data[i] > MailBnfHelper.Ftext.Length || !MailBnfHelper.Ftext[(int)data[i]])
				{
					throw new FormatException(SR.GetString("An invalid character was found in header name."));
				}
			}
			if (i == 0)
			{
				throw new FormatException(SR.GetString("An invalid character was found in header name."));
			}
		}

		// Token: 0x0600222A RID: 8746 RVA: 0x0007F6A4 File Offset: 0x0007D8A4
		internal static string ReadQuotedString(string data, ref int offset, StringBuilder builder)
		{
			return MailBnfHelper.ReadQuotedString(data, ref offset, builder, false, false);
		}

		// Token: 0x0600222B RID: 8747 RVA: 0x0007F6B0 File Offset: 0x0007D8B0
		internal static string ReadQuotedString(string data, ref int offset, StringBuilder builder, bool doesntRequireQuotes, bool permitUnicodeInDisplayName)
		{
			if (!doesntRequireQuotes)
			{
				offset++;
			}
			int num = offset;
			StringBuilder stringBuilder = (builder != null) ? builder : new StringBuilder();
			while (offset < data.Length)
			{
				if (data[offset] == '\\')
				{
					stringBuilder.Append(data, num, offset - num);
					int num2 = offset + 1;
					offset = num2;
					num = num2;
				}
				else if (data[offset] == '"')
				{
					stringBuilder.Append(data, num, offset - num);
					offset++;
					if (builder == null)
					{
						return stringBuilder.ToString();
					}
					return null;
				}
				else if (data[offset] == '=' && data.Length > offset + 3 && data[offset + 1] == '\r' && data[offset + 2] == '\n' && (data[offset + 3] == ' ' || data[offset + 3] == '\t'))
				{
					offset += 3;
				}
				else if (permitUnicodeInDisplayName)
				{
					if ((int)data[offset] <= MailBnfHelper.Ascii7bitMaxValue && !MailBnfHelper.Qtext[(int)data[offset]])
					{
						throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'.", new object[]
						{
							data[offset]
						}));
					}
				}
				else if ((int)data[offset] > MailBnfHelper.Ascii7bitMaxValue || !MailBnfHelper.Qtext[(int)data[offset]])
				{
					throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'.", new object[]
					{
						data[offset]
					}));
				}
				offset++;
			}
			if (!doesntRequireQuotes)
			{
				throw new FormatException(SR.GetString("The mail header is malformed."));
			}
			stringBuilder.Append(data, num, offset - num);
			if (builder == null)
			{
				return stringBuilder.ToString();
			}
			return null;
		}

		// Token: 0x0600222C RID: 8748 RVA: 0x0007F854 File Offset: 0x0007DA54
		internal static string ReadParameterAttribute(string data, ref int offset, StringBuilder builder)
		{
			if (!MailBnfHelper.SkipCFWS(data, ref offset))
			{
				return null;
			}
			return MailBnfHelper.ReadToken(data, ref offset, null);
		}

		// Token: 0x0600222D RID: 8749 RVA: 0x0007F86C File Offset: 0x0007DA6C
		internal static string ReadToken(string data, ref int offset, StringBuilder builder)
		{
			int num = offset;
			while (offset < data.Length)
			{
				if ((int)data[offset] > MailBnfHelper.Ascii7bitMaxValue)
				{
					throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'.", new object[]
					{
						data[offset]
					}));
				}
				if (!MailBnfHelper.Ttext[(int)data[offset]])
				{
					break;
				}
				offset++;
			}
			if (num == offset)
			{
				throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'.", new object[]
				{
					data[offset]
				}));
			}
			return data.Substring(num, offset - num);
		}

		// Token: 0x0600222E RID: 8750 RVA: 0x0007F90C File Offset: 0x0007DB0C
		internal static string GetDateTimeString(DateTime value, StringBuilder builder)
		{
			StringBuilder stringBuilder = (builder != null) ? builder : new StringBuilder();
			stringBuilder.Append(value.Day);
			stringBuilder.Append(' ');
			stringBuilder.Append(MailBnfHelper.s_months[value.Month]);
			stringBuilder.Append(' ');
			stringBuilder.Append(value.Year);
			stringBuilder.Append(' ');
			if (value.Hour <= 9)
			{
				stringBuilder.Append('0');
			}
			stringBuilder.Append(value.Hour);
			stringBuilder.Append(':');
			if (value.Minute <= 9)
			{
				stringBuilder.Append('0');
			}
			stringBuilder.Append(value.Minute);
			stringBuilder.Append(':');
			if (value.Second <= 9)
			{
				stringBuilder.Append('0');
			}
			stringBuilder.Append(value.Second);
			string text = TimeZone.CurrentTimeZone.GetUtcOffset(value).ToString();
			if (text[0] != '-')
			{
				stringBuilder.Append(" +");
			}
			else
			{
				stringBuilder.Append(" ");
			}
			string[] array = text.Split(new char[]
			{
				':'
			});
			stringBuilder.Append(array[0]);
			stringBuilder.Append(array[1]);
			if (builder == null)
			{
				return stringBuilder.ToString();
			}
			return null;
		}

		// Token: 0x0600222F RID: 8751 RVA: 0x0007FA58 File Offset: 0x0007DC58
		internal static void GetTokenOrQuotedString(string data, StringBuilder builder, bool allowUnicode)
		{
			int i = 0;
			int num = 0;
			while (i < data.Length)
			{
				if (!MailBnfHelper.CheckForUnicode(data[i], allowUnicode) && (!MailBnfHelper.Ttext[(int)data[i]] || data[i] == ' '))
				{
					builder.Append('"');
					while (i < data.Length)
					{
						if (!MailBnfHelper.CheckForUnicode(data[i], allowUnicode))
						{
							if (MailBnfHelper.IsFWSAt(data, i))
							{
								i++;
								i++;
							}
							else if (!MailBnfHelper.Qtext[(int)data[i]])
							{
								builder.Append(data, num, i - num);
								builder.Append('\\');
								num = i;
							}
						}
						i++;
					}
					builder.Append(data, num, i - num);
					builder.Append('"');
					return;
				}
				i++;
			}
			if (data.Length == 0)
			{
				builder.Append("\"\"");
			}
			builder.Append(data);
		}

		// Token: 0x06002230 RID: 8752 RVA: 0x0007FB3C File Offset: 0x0007DD3C
		private static bool CheckForUnicode(char ch, bool allowUnicode)
		{
			if ((int)ch < MailBnfHelper.Ascii7bitMaxValue)
			{
				return false;
			}
			if (!allowUnicode)
			{
				throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'.", new object[]
				{
					ch
				}));
			}
			return true;
		}

		// Token: 0x06002231 RID: 8753 RVA: 0x0007FB6C File Offset: 0x0007DD6C
		internal static bool HasCROrLF(string data)
		{
			for (int i = 0; i < data.Length; i++)
			{
				if (data[i] == '\r' || data[i] == '\n')
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002232 RID: 8754 RVA: 0x0007FBA4 File Offset: 0x0007DDA4
		internal static bool IsFWSAt(string data, int index)
		{
			return data[index] == MailBnfHelper.CR && index + 2 < data.Length && data[index + 1] == MailBnfHelper.LF && (data[index + 2] == MailBnfHelper.Space || data[index + 2] == MailBnfHelper.Tab);
		}

		// Token: 0x04001C6A RID: 7274
		internal static bool[] Atext = new bool[128];

		// Token: 0x04001C6B RID: 7275
		internal static bool[] Qtext = new bool[128];

		// Token: 0x04001C6C RID: 7276
		internal static bool[] Dtext = new bool[128];

		// Token: 0x04001C6D RID: 7277
		internal static bool[] Ftext = new bool[128];

		// Token: 0x04001C6E RID: 7278
		internal static bool[] Ttext = new bool[128];

		// Token: 0x04001C6F RID: 7279
		internal static bool[] Ctext = new bool[128];

		// Token: 0x04001C70 RID: 7280
		internal static readonly int Ascii7bitMaxValue = 127;

		// Token: 0x04001C71 RID: 7281
		internal static readonly char Quote = '"';

		// Token: 0x04001C72 RID: 7282
		internal static readonly char Space = ' ';

		// Token: 0x04001C73 RID: 7283
		internal static readonly char Tab = '\t';

		// Token: 0x04001C74 RID: 7284
		internal static readonly char CR = '\r';

		// Token: 0x04001C75 RID: 7285
		internal static readonly char LF = '\n';

		// Token: 0x04001C76 RID: 7286
		internal static readonly char StartComment = '(';

		// Token: 0x04001C77 RID: 7287
		internal static readonly char EndComment = ')';

		// Token: 0x04001C78 RID: 7288
		internal static readonly char Backslash = '\\';

		// Token: 0x04001C79 RID: 7289
		internal static readonly char At = '@';

		// Token: 0x04001C7A RID: 7290
		internal static readonly char EndAngleBracket = '>';

		// Token: 0x04001C7B RID: 7291
		internal static readonly char StartAngleBracket = '<';

		// Token: 0x04001C7C RID: 7292
		internal static readonly char StartSquareBracket = '[';

		// Token: 0x04001C7D RID: 7293
		internal static readonly char EndSquareBracket = ']';

		// Token: 0x04001C7E RID: 7294
		internal static readonly char Comma = ',';

		// Token: 0x04001C7F RID: 7295
		internal static readonly char Dot = '.';

		// Token: 0x04001C80 RID: 7296
		internal static readonly IList<char> Whitespace;

		// Token: 0x04001C81 RID: 7297
		private static string[] s_months = new string[]
		{
			null,
			"Jan",
			"Feb",
			"Mar",
			"Apr",
			"May",
			"Jun",
			"Jul",
			"Aug",
			"Sep",
			"Oct",
			"Nov",
			"Dec"
		};
	}
}
