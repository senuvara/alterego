﻿using System;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Text;

namespace System.Net.Mime
{
	// Token: 0x02000440 RID: 1088
	internal class MimeBasePart
	{
		// Token: 0x06002233 RID: 8755 RVA: 0x0000232F File Offset: 0x0000052F
		internal MimeBasePart()
		{
		}

		// Token: 0x06002234 RID: 8756 RVA: 0x0007FBFE File Offset: 0x0007DDFE
		internal static bool ShouldUseBase64Encoding(Encoding encoding)
		{
			return encoding == Encoding.Unicode || encoding == Encoding.UTF8 || encoding == Encoding.UTF32 || encoding == Encoding.BigEndianUnicode;
		}

		// Token: 0x06002235 RID: 8757 RVA: 0x0007FC23 File Offset: 0x0007DE23
		internal static string EncodeHeaderValue(string value, Encoding encoding, bool base64Encoding)
		{
			return MimeBasePart.EncodeHeaderValue(value, encoding, base64Encoding, 0);
		}

		// Token: 0x06002236 RID: 8758 RVA: 0x0007FC30 File Offset: 0x0007DE30
		internal static string EncodeHeaderValue(string value, Encoding encoding, bool base64Encoding, int headerLength)
		{
			if (MimeBasePart.IsAscii(value, false))
			{
				return value;
			}
			if (encoding == null)
			{
				encoding = Encoding.GetEncoding("utf-8");
			}
			IEncodableStream encoderForHeader = new EncodedStreamFactory().GetEncoderForHeader(encoding, base64Encoding, headerLength);
			byte[] bytes = encoding.GetBytes(value);
			encoderForHeader.EncodeBytes(bytes, 0, bytes.Length);
			return encoderForHeader.GetEncodedString();
		}

		// Token: 0x06002237 RID: 8759 RVA: 0x0007FC80 File Offset: 0x0007DE80
		internal static string DecodeHeaderValue(string value)
		{
			if (value == null || value.Length == 0)
			{
				return string.Empty;
			}
			string text = string.Empty;
			string[] array = value.Split(new char[]
			{
				'\r',
				'\n',
				' '
			}, StringSplitOptions.RemoveEmptyEntries);
			for (int i = 0; i < array.Length; i++)
			{
				string[] array2 = array[i].Split(new char[]
				{
					'?'
				});
				if (array2.Length != 5 || array2[0] != "=" || array2[4] != "=")
				{
					return value;
				}
				string name = array2[1];
				bool useBase64Encoding = array2[2] == "B";
				byte[] bytes = Encoding.ASCII.GetBytes(array2[3]);
				int count = new EncodedStreamFactory().GetEncoderForHeader(Encoding.GetEncoding(name), useBase64Encoding, 0).DecodeBytes(bytes, 0, bytes.Length);
				Encoding encoding = Encoding.GetEncoding(name);
				text += encoding.GetString(bytes, 0, count);
			}
			return text;
		}

		// Token: 0x06002238 RID: 8760 RVA: 0x0007FD70 File Offset: 0x0007DF70
		internal static Encoding DecodeEncoding(string value)
		{
			if (value == null || value.Length == 0)
			{
				return null;
			}
			string[] array = value.Split(new char[]
			{
				'?',
				'\r',
				'\n'
			});
			if (array.Length < 5 || array[0] != "=" || array[4] != "=")
			{
				return null;
			}
			return Encoding.GetEncoding(array[1]);
		}

		// Token: 0x06002239 RID: 8761 RVA: 0x0007FDD0 File Offset: 0x0007DFD0
		internal static bool IsAscii(string value, bool permitCROrLF)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			foreach (char c in value)
			{
				if (c > '\u007f')
				{
					return false;
				}
				if (!permitCROrLF && (c == '\r' || c == '\n'))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600223A RID: 8762 RVA: 0x0007FE20 File Offset: 0x0007E020
		internal static bool IsAnsi(string value, bool permitCROrLF)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			foreach (char c in value)
			{
				if (c > 'ÿ')
				{
					return false;
				}
				if (!permitCROrLF && (c == '\r' || c == '\n'))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x17000766 RID: 1894
		// (get) Token: 0x0600223B RID: 8763 RVA: 0x0007FE70 File Offset: 0x0007E070
		// (set) Token: 0x0600223C RID: 8764 RVA: 0x0007FE83 File Offset: 0x0007E083
		internal string ContentID
		{
			get
			{
				return this.Headers[MailHeaderInfo.GetString(MailHeaderID.ContentID)];
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					this.Headers.Remove(MailHeaderInfo.GetString(MailHeaderID.ContentID));
					return;
				}
				this.Headers[MailHeaderInfo.GetString(MailHeaderID.ContentID)] = value;
			}
		}

		// Token: 0x17000767 RID: 1895
		// (get) Token: 0x0600223D RID: 8765 RVA: 0x0007FEB1 File Offset: 0x0007E0B1
		// (set) Token: 0x0600223E RID: 8766 RVA: 0x0007FEC4 File Offset: 0x0007E0C4
		internal string ContentLocation
		{
			get
			{
				return this.Headers[MailHeaderInfo.GetString(MailHeaderID.ContentLocation)];
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					this.Headers.Remove(MailHeaderInfo.GetString(MailHeaderID.ContentLocation));
					return;
				}
				this.Headers[MailHeaderInfo.GetString(MailHeaderID.ContentLocation)] = value;
			}
		}

		// Token: 0x17000768 RID: 1896
		// (get) Token: 0x0600223F RID: 8767 RVA: 0x0007FEF4 File Offset: 0x0007E0F4
		internal NameValueCollection Headers
		{
			get
			{
				if (this.headers == null)
				{
					this.headers = new HeaderCollection();
				}
				if (this.contentType == null)
				{
					this.contentType = new ContentType();
				}
				this.contentType.PersistIfNeeded(this.headers, false);
				if (this.contentDisposition != null)
				{
					this.contentDisposition.PersistIfNeeded(this.headers, false);
				}
				return this.headers;
			}
		}

		// Token: 0x17000769 RID: 1897
		// (get) Token: 0x06002240 RID: 8768 RVA: 0x0007FF59 File Offset: 0x0007E159
		// (set) Token: 0x06002241 RID: 8769 RVA: 0x0007FF74 File Offset: 0x0007E174
		internal ContentType ContentType
		{
			get
			{
				if (this.contentType == null)
				{
					this.contentType = new ContentType();
				}
				return this.contentType;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.contentType = value;
				this.contentType.PersistIfNeeded((HeaderCollection)this.Headers, true);
			}
		}

		// Token: 0x06002242 RID: 8770 RVA: 0x0007FFA4 File Offset: 0x0007E1A4
		internal void PrepareHeaders(bool allowUnicode)
		{
			this.contentType.PersistIfNeeded((HeaderCollection)this.Headers, false);
			this.headers.InternalSet(MailHeaderInfo.GetString(MailHeaderID.ContentType), this.contentType.Encode(allowUnicode));
			if (this.contentDisposition != null)
			{
				this.contentDisposition.PersistIfNeeded((HeaderCollection)this.Headers, false);
				this.headers.InternalSet(MailHeaderInfo.GetString(MailHeaderID.ContentDisposition), this.contentDisposition.Encode(allowUnicode));
			}
		}

		// Token: 0x06002243 RID: 8771 RVA: 0x000068D7 File Offset: 0x00004AD7
		internal virtual void Send(BaseWriter writer, bool allowUnicode)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002244 RID: 8772 RVA: 0x000068D7 File Offset: 0x00004AD7
		internal virtual IAsyncResult BeginSend(BaseWriter writer, AsyncCallback callback, bool allowUnicode, object state)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002245 RID: 8773 RVA: 0x00080024 File Offset: 0x0007E224
		internal void EndSend(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			LazyAsyncResult lazyAsyncResult = asyncResult as MimeBasePart.MimePartAsyncResult;
			if (lazyAsyncResult == null || lazyAsyncResult.AsyncObject != this)
			{
				throw new ArgumentException(SR.GetString("The IAsyncResult object was not returned from the corresponding asynchronous method on this class."), "asyncResult");
			}
			if (lazyAsyncResult.EndCalled)
			{
				throw new InvalidOperationException(SR.GetString("{0} can only be called once for each asynchronous operation.", new object[]
				{
					"EndSend"
				}));
			}
			lazyAsyncResult.InternalWaitForCompletion();
			lazyAsyncResult.EndCalled = true;
			if (lazyAsyncResult.Result is Exception)
			{
				throw (Exception)lazyAsyncResult.Result;
			}
		}

		// Token: 0x04001C82 RID: 7298
		protected ContentType contentType;

		// Token: 0x04001C83 RID: 7299
		protected ContentDisposition contentDisposition;

		// Token: 0x04001C84 RID: 7300
		private HeaderCollection headers;

		// Token: 0x04001C85 RID: 7301
		internal const string defaultCharSet = "utf-8";

		// Token: 0x02000441 RID: 1089
		internal class MimePartAsyncResult : LazyAsyncResult
		{
			// Token: 0x06002246 RID: 8774 RVA: 0x000800B4 File Offset: 0x0007E2B4
			internal MimePartAsyncResult(MimeBasePart part, object state, AsyncCallback callback) : base(part, state, callback)
			{
			}
		}
	}
}
