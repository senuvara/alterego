﻿using System;

namespace System.Net.Mime
{
	// Token: 0x02000451 RID: 1105
	internal class WriteStateInfoBase
	{
		// Token: 0x060022BE RID: 8894 RVA: 0x00082390 File Offset: 0x00080590
		internal WriteStateInfoBase()
		{
			this.buffer = new byte[1024];
			this._header = new byte[0];
			this._footer = new byte[0];
			this._maxLineLength = EncodedStreamFactory.DefaultMaxLineLength;
			this._currentLineLength = 0;
			this._currentBufferUsed = 0;
		}

		// Token: 0x060022BF RID: 8895 RVA: 0x000823E4 File Offset: 0x000805E4
		internal WriteStateInfoBase(int bufferSize, byte[] header, byte[] footer, int maxLineLength) : this(bufferSize, header, footer, maxLineLength, 0)
		{
		}

		// Token: 0x060022C0 RID: 8896 RVA: 0x000823F2 File Offset: 0x000805F2
		internal WriteStateInfoBase(int bufferSize, byte[] header, byte[] footer, int maxLineLength, int mimeHeaderLength)
		{
			this.buffer = new byte[bufferSize];
			this._header = header;
			this._footer = footer;
			this._maxLineLength = maxLineLength;
			this._currentLineLength = mimeHeaderLength;
			this._currentBufferUsed = 0;
		}

		// Token: 0x17000779 RID: 1913
		// (get) Token: 0x060022C1 RID: 8897 RVA: 0x0008242B File Offset: 0x0008062B
		internal int FooterLength
		{
			get
			{
				return this._footer.Length;
			}
		}

		// Token: 0x1700077A RID: 1914
		// (get) Token: 0x060022C2 RID: 8898 RVA: 0x00082435 File Offset: 0x00080635
		internal byte[] Footer
		{
			get
			{
				return this._footer;
			}
		}

		// Token: 0x1700077B RID: 1915
		// (get) Token: 0x060022C3 RID: 8899 RVA: 0x0008243D File Offset: 0x0008063D
		internal byte[] Header
		{
			get
			{
				return this._header;
			}
		}

		// Token: 0x1700077C RID: 1916
		// (get) Token: 0x060022C4 RID: 8900 RVA: 0x00082445 File Offset: 0x00080645
		internal byte[] Buffer
		{
			get
			{
				return this.buffer;
			}
		}

		// Token: 0x1700077D RID: 1917
		// (get) Token: 0x060022C5 RID: 8901 RVA: 0x0008244D File Offset: 0x0008064D
		internal int Length
		{
			get
			{
				return this._currentBufferUsed;
			}
		}

		// Token: 0x1700077E RID: 1918
		// (get) Token: 0x060022C6 RID: 8902 RVA: 0x00082455 File Offset: 0x00080655
		internal int CurrentLineLength
		{
			get
			{
				return this._currentLineLength;
			}
		}

		// Token: 0x060022C7 RID: 8903 RVA: 0x00082460 File Offset: 0x00080660
		private void EnsureSpaceInBuffer(int moreBytes)
		{
			int num = this.Buffer.Length;
			while (this._currentBufferUsed + moreBytes >= num)
			{
				num *= 2;
			}
			if (num > this.Buffer.Length)
			{
				byte[] array = new byte[num];
				this.buffer.CopyTo(array, 0);
				this.buffer = array;
			}
		}

		// Token: 0x060022C8 RID: 8904 RVA: 0x000824B0 File Offset: 0x000806B0
		internal void Append(byte aByte)
		{
			this.EnsureSpaceInBuffer(1);
			byte[] array = this.Buffer;
			int currentBufferUsed = this._currentBufferUsed;
			this._currentBufferUsed = currentBufferUsed + 1;
			array[currentBufferUsed] = aByte;
			this._currentLineLength++;
		}

		// Token: 0x060022C9 RID: 8905 RVA: 0x000824EB File Offset: 0x000806EB
		internal void Append(params byte[] bytes)
		{
			this.EnsureSpaceInBuffer(bytes.Length);
			bytes.CopyTo(this.buffer, this.Length);
			this._currentLineLength += bytes.Length;
			this._currentBufferUsed += bytes.Length;
		}

		// Token: 0x060022CA RID: 8906 RVA: 0x00082528 File Offset: 0x00080728
		internal void AppendCRLF(bool includeSpace)
		{
			this.AppendFooter();
			this.Append(new byte[]
			{
				13,
				10
			});
			this._currentLineLength = 0;
			if (includeSpace)
			{
				this.Append(32);
			}
			this.AppendHeader();
		}

		// Token: 0x060022CB RID: 8907 RVA: 0x0008255E File Offset: 0x0008075E
		internal void AppendHeader()
		{
			if (this.Header != null && this.Header.Length != 0)
			{
				this.Append(this.Header);
			}
		}

		// Token: 0x060022CC RID: 8908 RVA: 0x0008257D File Offset: 0x0008077D
		internal void AppendFooter()
		{
			if (this.Footer != null && this.Footer.Length != 0)
			{
				this.Append(this.Footer);
			}
		}

		// Token: 0x1700077F RID: 1919
		// (get) Token: 0x060022CD RID: 8909 RVA: 0x0008259C File Offset: 0x0008079C
		internal int MaxLineLength
		{
			get
			{
				return this._maxLineLength;
			}
		}

		// Token: 0x060022CE RID: 8910 RVA: 0x000825A4 File Offset: 0x000807A4
		internal void Reset()
		{
			this._currentBufferUsed = 0;
			this._currentLineLength = 0;
		}

		// Token: 0x060022CF RID: 8911 RVA: 0x000825B4 File Offset: 0x000807B4
		internal void BufferFlushed()
		{
			this._currentBufferUsed = 0;
		}

		// Token: 0x04001CDD RID: 7389
		protected byte[] _header;

		// Token: 0x04001CDE RID: 7390
		protected byte[] _footer;

		// Token: 0x04001CDF RID: 7391
		protected int _maxLineLength;

		// Token: 0x04001CE0 RID: 7392
		protected byte[] buffer;

		// Token: 0x04001CE1 RID: 7393
		protected int _currentLineLength;

		// Token: 0x04001CE2 RID: 7394
		protected int _currentBufferUsed;

		// Token: 0x04001CE3 RID: 7395
		protected const int defaultBufferSize = 1024;
	}
}
