﻿using System;

namespace System.Net.Mime
{
	// Token: 0x02000444 RID: 1092
	internal enum MimeMultiPartType
	{
		// Token: 0x04001C91 RID: 7313
		Mixed,
		// Token: 0x04001C92 RID: 7314
		Alternative,
		// Token: 0x04001C93 RID: 7315
		Parallel,
		// Token: 0x04001C94 RID: 7316
		Related,
		// Token: 0x04001C95 RID: 7317
		Unknown = -1
	}
}
