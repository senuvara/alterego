﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace System.Net.Mime
{
	// Token: 0x0200044F RID: 1103
	internal class SmtpDateTime
	{
		// Token: 0x060022B1 RID: 8881 RVA: 0x00081DE4 File Offset: 0x0007FFE4
		internal static IDictionary<string, TimeSpan> InitializeShortHandLookups()
		{
			return new Dictionary<string, TimeSpan>
			{
				{
					"UT",
					TimeSpan.Zero
				},
				{
					"GMT",
					TimeSpan.Zero
				},
				{
					"EDT",
					new TimeSpan(-4, 0, 0)
				},
				{
					"EST",
					new TimeSpan(-5, 0, 0)
				},
				{
					"CDT",
					new TimeSpan(-5, 0, 0)
				},
				{
					"CST",
					new TimeSpan(-6, 0, 0)
				},
				{
					"MDT",
					new TimeSpan(-6, 0, 0)
				},
				{
					"MST",
					new TimeSpan(-7, 0, 0)
				},
				{
					"PDT",
					new TimeSpan(-7, 0, 0)
				},
				{
					"PST",
					new TimeSpan(-8, 0, 0)
				}
			};
		}

		// Token: 0x060022B2 RID: 8882 RVA: 0x00081EB8 File Offset: 0x000800B8
		internal SmtpDateTime(DateTime value)
		{
			this.date = value;
			switch (value.Kind)
			{
			case DateTimeKind.Unspecified:
				this.unknownTimeZone = true;
				return;
			case DateTimeKind.Utc:
				this.timeZone = TimeSpan.Zero;
				return;
			case DateTimeKind.Local:
			{
				TimeSpan utcOffset = TimeZoneInfo.Local.GetUtcOffset(value);
				this.timeZone = this.ValidateAndGetSanitizedTimeSpan(utcOffset);
				return;
			}
			default:
				return;
			}
		}

		// Token: 0x060022B3 RID: 8883 RVA: 0x00081F1C File Offset: 0x0008011C
		internal SmtpDateTime(string value)
		{
			string timeZoneString;
			this.date = this.ParseValue(value, out timeZoneString);
			if (!this.TryParseTimeZoneString(timeZoneString, out this.timeZone))
			{
				this.unknownTimeZone = true;
			}
		}

		// Token: 0x17000778 RID: 1912
		// (get) Token: 0x060022B4 RID: 8884 RVA: 0x00081F54 File Offset: 0x00080154
		internal DateTime Date
		{
			get
			{
				if (this.unknownTimeZone)
				{
					return DateTime.SpecifyKind(this.date, DateTimeKind.Unspecified);
				}
				DateTimeOffset dateTimeOffset = new DateTimeOffset(this.date, this.timeZone);
				return dateTimeOffset.LocalDateTime;
			}
		}

		// Token: 0x060022B5 RID: 8885 RVA: 0x00081F90 File Offset: 0x00080190
		public override string ToString()
		{
			if (this.unknownTimeZone)
			{
				return string.Format("{0} {1}", this.FormatDate(this.date), "-0000");
			}
			return string.Format("{0} {1}", this.FormatDate(this.date), this.TimeSpanToOffset(this.timeZone));
		}

		// Token: 0x060022B6 RID: 8886 RVA: 0x00081FE4 File Offset: 0x000801E4
		internal void ValidateAndGetTimeZoneOffsetValues(string offset, out bool positive, out int hours, out int minutes)
		{
			if (offset.Length != 5)
			{
				throw new FormatException(SR.GetString("The date is in an invalid format."));
			}
			positive = offset.StartsWith("+");
			if (!int.TryParse(offset.Substring(1, 2), NumberStyles.None, CultureInfo.InvariantCulture, out hours))
			{
				throw new FormatException(SR.GetString("The date is in an invalid format."));
			}
			if (!int.TryParse(offset.Substring(3, 2), NumberStyles.None, CultureInfo.InvariantCulture, out minutes))
			{
				throw new FormatException(SR.GetString("The date is in an invalid format."));
			}
			if (minutes > 59)
			{
				throw new FormatException(SR.GetString("The date is in an invalid format."));
			}
		}

		// Token: 0x060022B7 RID: 8887 RVA: 0x0008207C File Offset: 0x0008027C
		internal void ValidateTimeZoneShortHandValue(string value)
		{
			for (int i = 0; i < value.Length; i++)
			{
				if (!char.IsLetter(value, i))
				{
					throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'."));
				}
			}
		}

		// Token: 0x060022B8 RID: 8888 RVA: 0x000820B3 File Offset: 0x000802B3
		internal string FormatDate(DateTime value)
		{
			return value.ToString("ddd, dd MMM yyyy HH:mm:ss", CultureInfo.InvariantCulture);
		}

		// Token: 0x060022B9 RID: 8889 RVA: 0x000820C8 File Offset: 0x000802C8
		internal DateTime ParseValue(string data, out string timeZone)
		{
			if (string.IsNullOrEmpty(data))
			{
				throw new FormatException(SR.GetString("The date is in an invalid format."));
			}
			int num = data.IndexOf(':');
			if (num == -1)
			{
				throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'."));
			}
			int num2 = data.IndexOfAny(SmtpDateTime.allowedWhiteSpaceChars, num);
			if (num2 == -1)
			{
				throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'."));
			}
			DateTime result;
			if (!DateTime.TryParseExact(data.Substring(0, num2).Trim(), SmtpDateTime.validDateTimeFormats, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out result))
			{
				throw new FormatException(SR.GetString("The date is in an invalid format."));
			}
			string text = data.Substring(num2).Trim();
			int num3 = text.IndexOfAny(SmtpDateTime.allowedWhiteSpaceChars);
			if (num3 != -1)
			{
				text = text.Substring(0, num3);
			}
			if (string.IsNullOrEmpty(text))
			{
				throw new FormatException(SR.GetString("The date is in an invalid format."));
			}
			timeZone = text;
			return result;
		}

		// Token: 0x060022BA RID: 8890 RVA: 0x000821A4 File Offset: 0x000803A4
		internal bool TryParseTimeZoneString(string timeZoneString, out TimeSpan timeZone)
		{
			timeZone = TimeSpan.Zero;
			if (timeZoneString == "-0000")
			{
				return false;
			}
			if (timeZoneString[0] == '+' || timeZoneString[0] == '-')
			{
				bool flag;
				int num;
				int num2;
				this.ValidateAndGetTimeZoneOffsetValues(timeZoneString, out flag, out num, out num2);
				if (!flag)
				{
					if (num != 0)
					{
						num *= -1;
					}
					else if (num2 != 0)
					{
						num2 *= -1;
					}
				}
				timeZone = new TimeSpan(num, num2, 0);
				return true;
			}
			this.ValidateTimeZoneShortHandValue(timeZoneString);
			if (SmtpDateTime.timeZoneOffsetLookup.ContainsKey(timeZoneString))
			{
				timeZone = SmtpDateTime.timeZoneOffsetLookup[timeZoneString];
				return true;
			}
			return false;
		}

		// Token: 0x060022BB RID: 8891 RVA: 0x0008223C File Offset: 0x0008043C
		internal TimeSpan ValidateAndGetSanitizedTimeSpan(TimeSpan span)
		{
			TimeSpan result = new TimeSpan(span.Days, span.Hours, span.Minutes, 0, 0);
			if (Math.Abs(result.Ticks) > SmtpDateTime.timeSpanMaxTicks)
			{
				throw new FormatException(SR.GetString("The date is in an invalid format."));
			}
			return result;
		}

		// Token: 0x060022BC RID: 8892 RVA: 0x0008228C File Offset: 0x0008048C
		internal string TimeSpanToOffset(TimeSpan span)
		{
			if (span.Ticks == 0L)
			{
				return "+0000";
			}
			uint num = (uint)Math.Abs(Math.Floor(span.TotalHours));
			uint num2 = (uint)Math.Abs(span.Minutes);
			string str = (span.Ticks > 0L) ? "+" : "-";
			if (num < 10U)
			{
				str += "0";
			}
			str += num.ToString();
			if (num2 < 10U)
			{
				str += "0";
			}
			return str + num2.ToString();
		}

		// Token: 0x060022BD RID: 8893 RVA: 0x00082320 File Offset: 0x00080520
		// Note: this type is marked as 'beforefieldinit'.
		static SmtpDateTime()
		{
		}

		// Token: 0x04001CC7 RID: 7367
		internal const string unknownTimeZoneDefaultOffset = "-0000";

		// Token: 0x04001CC8 RID: 7368
		internal const string utcDefaultTimeZoneOffset = "+0000";

		// Token: 0x04001CC9 RID: 7369
		internal const int offsetLength = 5;

		// Token: 0x04001CCA RID: 7370
		internal const int maxMinuteValue = 59;

		// Token: 0x04001CCB RID: 7371
		internal const string dateFormatWithDayOfWeek = "ddd, dd MMM yyyy HH:mm:ss";

		// Token: 0x04001CCC RID: 7372
		internal const string dateFormatWithoutDayOfWeek = "dd MMM yyyy HH:mm:ss";

		// Token: 0x04001CCD RID: 7373
		internal const string dateFormatWithDayOfWeekAndNoSeconds = "ddd, dd MMM yyyy HH:mm";

		// Token: 0x04001CCE RID: 7374
		internal const string dateFormatWithoutDayOfWeekAndNoSeconds = "dd MMM yyyy HH:mm";

		// Token: 0x04001CCF RID: 7375
		internal static readonly string[] validDateTimeFormats = new string[]
		{
			"ddd, dd MMM yyyy HH:mm:ss",
			"dd MMM yyyy HH:mm:ss",
			"ddd, dd MMM yyyy HH:mm",
			"dd MMM yyyy HH:mm"
		};

		// Token: 0x04001CD0 RID: 7376
		internal static readonly char[] allowedWhiteSpaceChars = new char[]
		{
			' ',
			'\t'
		};

		// Token: 0x04001CD1 RID: 7377
		internal static readonly IDictionary<string, TimeSpan> timeZoneOffsetLookup = SmtpDateTime.InitializeShortHandLookups();

		// Token: 0x04001CD2 RID: 7378
		internal static readonly long timeSpanMaxTicks = 3599400000000L;

		// Token: 0x04001CD3 RID: 7379
		internal static readonly int offsetMaxValue = 9959;

		// Token: 0x04001CD4 RID: 7380
		private readonly DateTime date;

		// Token: 0x04001CD5 RID: 7381
		private readonly TimeSpan timeZone;

		// Token: 0x04001CD6 RID: 7382
		private readonly bool unknownTimeZone;
	}
}
