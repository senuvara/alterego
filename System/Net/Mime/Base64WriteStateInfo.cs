﻿using System;
using System.Runtime.CompilerServices;

namespace System.Net.Mime
{
	// Token: 0x02000435 RID: 1077
	internal class Base64WriteStateInfo : WriteStateInfoBase
	{
		// Token: 0x060021C0 RID: 8640 RVA: 0x0007D7ED File Offset: 0x0007B9ED
		internal Base64WriteStateInfo()
		{
		}

		// Token: 0x060021C1 RID: 8641 RVA: 0x0007D7F5 File Offset: 0x0007B9F5
		internal Base64WriteStateInfo(int bufferSize, byte[] header, byte[] footer, int maxLineLength, int mimeHeaderLength) : base(bufferSize, header, footer, maxLineLength, mimeHeaderLength)
		{
		}

		// Token: 0x17000753 RID: 1875
		// (get) Token: 0x060021C2 RID: 8642 RVA: 0x0007D804 File Offset: 0x0007BA04
		// (set) Token: 0x060021C3 RID: 8643 RVA: 0x0007D80C File Offset: 0x0007BA0C
		internal int Padding
		{
			[CompilerGenerated]
			get
			{
				return this.<Padding>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Padding>k__BackingField = value;
			}
		}

		// Token: 0x17000754 RID: 1876
		// (get) Token: 0x060021C4 RID: 8644 RVA: 0x0007D815 File Offset: 0x0007BA15
		// (set) Token: 0x060021C5 RID: 8645 RVA: 0x0007D81D File Offset: 0x0007BA1D
		internal byte LastBits
		{
			[CompilerGenerated]
			get
			{
				return this.<LastBits>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<LastBits>k__BackingField = value;
			}
		}

		// Token: 0x04001C42 RID: 7234
		[CompilerGenerated]
		private int <Padding>k__BackingField;

		// Token: 0x04001C43 RID: 7235
		[CompilerGenerated]
		private byte <LastBits>k__BackingField;
	}
}
