﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;

namespace System.Net.Mime
{
	// Token: 0x02000447 RID: 1095
	internal class MimeWriter : BaseWriter
	{
		// Token: 0x0600226C RID: 8812 RVA: 0x00080BDB File Offset: 0x0007EDDB
		internal MimeWriter(Stream stream, string boundary) : base(stream, false)
		{
			if (boundary == null)
			{
				throw new ArgumentNullException("boundary");
			}
			this.boundaryBytes = Encoding.ASCII.GetBytes(boundary);
		}

		// Token: 0x0600226D RID: 8813 RVA: 0x00080C0C File Offset: 0x0007EE0C
		internal override void WriteHeaders(NameValueCollection headers, bool allowUnicode)
		{
			if (headers == null)
			{
				throw new ArgumentNullException("headers");
			}
			foreach (object obj in headers)
			{
				string name = (string)obj;
				base.WriteHeader(name, headers[name], allowUnicode);
			}
		}

		// Token: 0x0600226E RID: 8814 RVA: 0x00080C78 File Offset: 0x0007EE78
		internal IAsyncResult BeginClose(AsyncCallback callback, object state)
		{
			MultiAsyncResult multiAsyncResult = new MultiAsyncResult(this, callback, state);
			this.Close(multiAsyncResult);
			multiAsyncResult.CompleteSequence();
			return multiAsyncResult;
		}

		// Token: 0x0600226F RID: 8815 RVA: 0x00080C9C File Offset: 0x0007EE9C
		internal void EndClose(IAsyncResult result)
		{
			MultiAsyncResult.End(result);
			this.stream.Close();
		}

		// Token: 0x06002270 RID: 8816 RVA: 0x00080CB0 File Offset: 0x0007EEB0
		internal override void Close()
		{
			this.Close(null);
			this.stream.Close();
		}

		// Token: 0x06002271 RID: 8817 RVA: 0x00080CC4 File Offset: 0x0007EEC4
		private void Close(MultiAsyncResult multiResult)
		{
			this.bufferBuilder.Append(BaseWriter.CRLF);
			this.bufferBuilder.Append(MimeWriter.DASHDASH);
			this.bufferBuilder.Append(this.boundaryBytes);
			this.bufferBuilder.Append(MimeWriter.DASHDASH);
			this.bufferBuilder.Append(BaseWriter.CRLF);
			base.Flush(multiResult);
		}

		// Token: 0x06002272 RID: 8818 RVA: 0x00080D29 File Offset: 0x0007EF29
		protected override void OnClose(object sender, EventArgs args)
		{
			if (this.contentStream != sender)
			{
				return;
			}
			this.contentStream.Flush();
			this.contentStream = null;
			this.writeBoundary = true;
			this.isInContent = false;
		}

		// Token: 0x06002273 RID: 8819 RVA: 0x00080D58 File Offset: 0x0007EF58
		protected override void CheckBoundary()
		{
			if (this.writeBoundary)
			{
				this.bufferBuilder.Append(BaseWriter.CRLF);
				this.bufferBuilder.Append(MimeWriter.DASHDASH);
				this.bufferBuilder.Append(this.boundaryBytes);
				this.bufferBuilder.Append(BaseWriter.CRLF);
				this.writeBoundary = false;
			}
		}

		// Token: 0x06002274 RID: 8820 RVA: 0x00080DB5 File Offset: 0x0007EFB5
		// Note: this type is marked as 'beforefieldinit'.
		static MimeWriter()
		{
		}

		// Token: 0x04001CA3 RID: 7331
		private static byte[] DASHDASH = new byte[]
		{
			45,
			45
		};

		// Token: 0x04001CA4 RID: 7332
		private byte[] boundaryBytes;

		// Token: 0x04001CA5 RID: 7333
		private bool writeBoundary = true;
	}
}
