﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Net.Mime
{
	/// <summary>Represents a MIME protocol Content-Disposition header.</summary>
	// Token: 0x02000437 RID: 1079
	public class ContentDisposition
	{
		// Token: 0x060021D4 RID: 8660 RVA: 0x0007DBD0 File Offset: 0x0007BDD0
		static ContentDisposition()
		{
			ContentDisposition.validators.Add("creation-date", ContentDisposition.dateParser);
			ContentDisposition.validators.Add("modification-date", ContentDisposition.dateParser);
			ContentDisposition.validators.Add("read-date", ContentDisposition.dateParser);
			ContentDisposition.validators.Add("size", ContentDisposition.longParser);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Mime.ContentDisposition" /> class with a <see cref="P:System.Net.Mime.ContentDisposition.DispositionType" /> of <see cref="F:System.Net.Mime.DispositionTypeNames.Attachment" />. </summary>
		// Token: 0x060021D5 RID: 8661 RVA: 0x0007DC61 File Offset: 0x0007BE61
		public ContentDisposition()
		{
			this.isChanged = true;
			this.dispositionType = "attachment";
			this.disposition = this.dispositionType;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Mime.ContentDisposition" /> class with the specified disposition information.</summary>
		/// <param name="disposition">A <see cref="T:System.Net.Mime.DispositionTypeNames" /> value that contains the disposition.</param>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="disposition" /> is <see langword="null" /> or equal to <see cref="F:System.String.Empty" /> ("").</exception>
		// Token: 0x060021D6 RID: 8662 RVA: 0x0007DC87 File Offset: 0x0007BE87
		public ContentDisposition(string disposition)
		{
			if (disposition == null)
			{
				throw new ArgumentNullException("disposition");
			}
			this.isChanged = true;
			this.disposition = disposition;
			this.ParseValue();
		}

		// Token: 0x060021D7 RID: 8663 RVA: 0x0007DCB4 File Offset: 0x0007BEB4
		internal DateTime GetDateParameter(string parameterName)
		{
			SmtpDateTime smtpDateTime = ((TrackingValidationObjectDictionary)this.Parameters).InternalGet(parameterName) as SmtpDateTime;
			if (smtpDateTime == null)
			{
				return DateTime.MinValue;
			}
			return smtpDateTime.Date;
		}

		/// <summary>Gets or sets the disposition type for an e-mail attachment.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the disposition type. The value is not restricted but is typically one of the <see cref="P:System.Net.Mime.ContentDisposition.DispositionType" /> values.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value specified for a set operation is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The value specified for a set operation is equal to <see cref="F:System.String.Empty" /> ("").</exception>
		// Token: 0x17000755 RID: 1877
		// (get) Token: 0x060021D8 RID: 8664 RVA: 0x0007DCE7 File Offset: 0x0007BEE7
		// (set) Token: 0x060021D9 RID: 8665 RVA: 0x0007DCEF File Offset: 0x0007BEEF
		public string DispositionType
		{
			get
			{
				return this.dispositionType;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (value == string.Empty)
				{
					throw new ArgumentException(SR.GetString("This property cannot be set to an empty string."), "value");
				}
				this.isChanged = true;
				this.dispositionType = value;
			}
		}

		/// <summary>Gets the parameters included in the Content-Disposition header represented by this instance.</summary>
		/// <returns>A writable <see cref="T:System.Collections.Specialized.StringDictionary" /> that contains parameter name/value pairs.</returns>
		// Token: 0x17000756 RID: 1878
		// (get) Token: 0x060021DA RID: 8666 RVA: 0x0007DD2F File Offset: 0x0007BF2F
		public StringDictionary Parameters
		{
			get
			{
				if (this.parameters == null)
				{
					this.parameters = new TrackingValidationObjectDictionary(ContentDisposition.validators);
				}
				return this.parameters;
			}
		}

		/// <summary>Gets or sets the suggested file name for an e-mail attachment.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the file name. </returns>
		// Token: 0x17000757 RID: 1879
		// (get) Token: 0x060021DB RID: 8667 RVA: 0x0007DD4F File Offset: 0x0007BF4F
		// (set) Token: 0x060021DC RID: 8668 RVA: 0x0007DD61 File Offset: 0x0007BF61
		public string FileName
		{
			get
			{
				return this.Parameters["filename"];
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					this.Parameters.Remove("filename");
					return;
				}
				this.Parameters["filename"] = value;
			}
		}

		/// <summary>Gets or sets the creation date for a file attachment.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> value that indicates the file creation date; otherwise, <see cref="F:System.DateTime.MinValue" /> if no date was specified.</returns>
		// Token: 0x17000758 RID: 1880
		// (get) Token: 0x060021DD RID: 8669 RVA: 0x0007DD8D File Offset: 0x0007BF8D
		// (set) Token: 0x060021DE RID: 8670 RVA: 0x0007DD9C File Offset: 0x0007BF9C
		public DateTime CreationDate
		{
			get
			{
				return this.GetDateParameter("creation-date");
			}
			set
			{
				SmtpDateTime value2 = new SmtpDateTime(value);
				((TrackingValidationObjectDictionary)this.Parameters).InternalSet("creation-date", value2);
			}
		}

		/// <summary>Gets or sets the modification date for a file attachment.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> value that indicates the file modification date; otherwise, <see cref="F:System.DateTime.MinValue" /> if no date was specified.</returns>
		// Token: 0x17000759 RID: 1881
		// (get) Token: 0x060021DF RID: 8671 RVA: 0x0007DDC6 File Offset: 0x0007BFC6
		// (set) Token: 0x060021E0 RID: 8672 RVA: 0x0007DDD4 File Offset: 0x0007BFD4
		public DateTime ModificationDate
		{
			get
			{
				return this.GetDateParameter("modification-date");
			}
			set
			{
				SmtpDateTime value2 = new SmtpDateTime(value);
				((TrackingValidationObjectDictionary)this.Parameters).InternalSet("modification-date", value2);
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that determines the disposition type (Inline or Attachment) for an e-mail attachment.</summary>
		/// <returns>
		///     <see langword="true" /> if content in the attachment is presented inline as part of the e-mail body; otherwise, <see langword="false" />. </returns>
		// Token: 0x1700075A RID: 1882
		// (get) Token: 0x060021E1 RID: 8673 RVA: 0x0007DDFE File Offset: 0x0007BFFE
		// (set) Token: 0x060021E2 RID: 8674 RVA: 0x0007DE10 File Offset: 0x0007C010
		public bool Inline
		{
			get
			{
				return this.dispositionType == "inline";
			}
			set
			{
				this.isChanged = true;
				if (value)
				{
					this.dispositionType = "inline";
					return;
				}
				this.dispositionType = "attachment";
			}
		}

		/// <summary>Gets or sets the read date for a file attachment.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> value that indicates the file read date; otherwise, <see cref="F:System.DateTime.MinValue" /> if no date was specified.</returns>
		// Token: 0x1700075B RID: 1883
		// (get) Token: 0x060021E3 RID: 8675 RVA: 0x0007DE33 File Offset: 0x0007C033
		// (set) Token: 0x060021E4 RID: 8676 RVA: 0x0007DE40 File Offset: 0x0007C040
		public DateTime ReadDate
		{
			get
			{
				return this.GetDateParameter("read-date");
			}
			set
			{
				SmtpDateTime value2 = new SmtpDateTime(value);
				((TrackingValidationObjectDictionary)this.Parameters).InternalSet("read-date", value2);
			}
		}

		/// <summary>Gets or sets the size of a file attachment.</summary>
		/// <returns>A <see cref="T:System.Int32" /> that specifies the number of bytes in the file attachment. The default value is -1, which indicates that the file size is unknown.</returns>
		// Token: 0x1700075C RID: 1884
		// (get) Token: 0x060021E5 RID: 8677 RVA: 0x0007DE6C File Offset: 0x0007C06C
		// (set) Token: 0x060021E6 RID: 8678 RVA: 0x0007DE9B File Offset: 0x0007C09B
		public long Size
		{
			get
			{
				object obj = ((TrackingValidationObjectDictionary)this.Parameters).InternalGet("size");
				if (obj == null)
				{
					return -1L;
				}
				return (long)obj;
			}
			set
			{
				((TrackingValidationObjectDictionary)this.Parameters).InternalSet("size", value);
			}
		}

		// Token: 0x060021E7 RID: 8679 RVA: 0x0007DEB8 File Offset: 0x0007C0B8
		internal void Set(string contentDisposition, HeaderCollection headers)
		{
			this.disposition = contentDisposition;
			this.ParseValue();
			headers.InternalSet(MailHeaderInfo.GetString(MailHeaderID.ContentDisposition), this.ToString());
			this.isPersisted = true;
		}

		// Token: 0x060021E8 RID: 8680 RVA: 0x0007DEE0 File Offset: 0x0007C0E0
		internal void PersistIfNeeded(HeaderCollection headers, bool forcePersist)
		{
			if (this.IsChanged || !this.isPersisted || forcePersist)
			{
				headers.InternalSet(MailHeaderInfo.GetString(MailHeaderID.ContentDisposition), this.ToString());
				this.isPersisted = true;
			}
		}

		// Token: 0x1700075D RID: 1885
		// (get) Token: 0x060021E9 RID: 8681 RVA: 0x0007DF13 File Offset: 0x0007C113
		internal bool IsChanged
		{
			get
			{
				return this.isChanged || (this.parameters != null && this.parameters.IsChanged);
			}
		}

		/// <summary>Returns a <see cref="T:System.String" /> representation of this instance.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the property values for this instance.</returns>
		// Token: 0x060021EA RID: 8682 RVA: 0x0007DF34 File Offset: 0x0007C134
		public override string ToString()
		{
			if (this.disposition == null || this.isChanged || (this.parameters != null && this.parameters.IsChanged))
			{
				this.disposition = this.Encode(false);
				this.isChanged = false;
				this.parameters.IsChanged = false;
				this.isPersisted = false;
			}
			return this.disposition;
		}

		// Token: 0x060021EB RID: 8683 RVA: 0x0007DF94 File Offset: 0x0007C194
		internal string Encode(bool allowUnicode)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(this.dispositionType);
			foreach (object obj in this.Parameters.Keys)
			{
				string text = (string)obj;
				stringBuilder.Append("; ");
				ContentDisposition.EncodeToBuffer(text, stringBuilder, allowUnicode);
				stringBuilder.Append('=');
				ContentDisposition.EncodeToBuffer(this.parameters[text], stringBuilder, allowUnicode);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060021EC RID: 8684 RVA: 0x0007E034 File Offset: 0x0007C234
		private static void EncodeToBuffer(string value, StringBuilder builder, bool allowUnicode)
		{
			Encoding encoding = MimeBasePart.DecodeEncoding(value);
			if (encoding != null)
			{
				builder.Append("\"" + value + "\"");
				return;
			}
			if ((allowUnicode && !MailBnfHelper.HasCROrLF(value)) || MimeBasePart.IsAscii(value, false))
			{
				MailBnfHelper.GetTokenOrQuotedString(value, builder, allowUnicode);
				return;
			}
			encoding = Encoding.GetEncoding("utf-8");
			builder.Append("\"" + MimeBasePart.EncodeHeaderValue(value, encoding, MimeBasePart.ShouldUseBase64Encoding(encoding)) + "\"");
		}

		/// <summary>Determines whether the content-disposition header of the specified <see cref="T:System.Net.Mime.ContentDisposition" /> object is equal to the content-disposition header of this object.</summary>
		/// <param name="rparam">The <see cref="T:System.Net.Mime.ContentDisposition" /> object to compare with this object.</param>
		/// <returns>
		///     <see langword="true" /> if the content-disposition headers are the same; otherwise <see langword="false" />.</returns>
		// Token: 0x060021ED RID: 8685 RVA: 0x0007B0E2 File Offset: 0x000792E2
		public override bool Equals(object rparam)
		{
			return rparam != null && string.Compare(this.ToString(), rparam.ToString(), StringComparison.OrdinalIgnoreCase) == 0;
		}

		/// <summary>Determines the hash code of the specified <see cref="T:System.Net.Mime.ContentDisposition" /> object</summary>
		/// <returns>An integer hash value.</returns>
		// Token: 0x060021EE RID: 8686 RVA: 0x0007E0AE File Offset: 0x0007C2AE
		public override int GetHashCode()
		{
			return this.ToString().ToLowerInvariant().GetHashCode();
		}

		// Token: 0x060021EF RID: 8687 RVA: 0x0007E0C0 File Offset: 0x0007C2C0
		private void ParseValue()
		{
			int num = 0;
			try
			{
				this.dispositionType = MailBnfHelper.ReadToken(this.disposition, ref num, null);
				if (string.IsNullOrEmpty(this.dispositionType))
				{
					throw new FormatException(SR.GetString("The mail header is malformed."));
				}
				if (this.parameters == null)
				{
					this.parameters = new TrackingValidationObjectDictionary(ContentDisposition.validators);
				}
				else
				{
					this.parameters.Clear();
				}
				while (MailBnfHelper.SkipCFWS(this.disposition, ref num))
				{
					if (this.disposition[num++] != ';')
					{
						throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'.", new object[]
						{
							this.disposition[num - 1]
						}));
					}
					if (!MailBnfHelper.SkipCFWS(this.disposition, ref num))
					{
						break;
					}
					string text = MailBnfHelper.ReadParameterAttribute(this.disposition, ref num, null);
					if (this.disposition[num++] != '=')
					{
						throw new FormatException(SR.GetString("The mail header is malformed."));
					}
					if (!MailBnfHelper.SkipCFWS(this.disposition, ref num))
					{
						throw new FormatException(SR.GetString("The specified content disposition is invalid."));
					}
					string value;
					if (this.disposition[num] == '"')
					{
						value = MailBnfHelper.ReadQuotedString(this.disposition, ref num, null);
					}
					else
					{
						value = MailBnfHelper.ReadToken(this.disposition, ref num, null);
					}
					if (string.IsNullOrEmpty(text) || string.IsNullOrEmpty(value))
					{
						throw new FormatException(SR.GetString("The specified content disposition is invalid."));
					}
					this.Parameters.Add(text, value);
				}
			}
			catch (FormatException innerException)
			{
				throw new FormatException(SR.GetString("The specified content disposition is invalid."), innerException);
			}
			this.parameters.IsChanged = false;
		}

		// Token: 0x04001C4E RID: 7246
		private string dispositionType;

		// Token: 0x04001C4F RID: 7247
		private TrackingValidationObjectDictionary parameters;

		// Token: 0x04001C50 RID: 7248
		private bool isChanged;

		// Token: 0x04001C51 RID: 7249
		private bool isPersisted;

		// Token: 0x04001C52 RID: 7250
		private string disposition;

		// Token: 0x04001C53 RID: 7251
		private const string creationDate = "creation-date";

		// Token: 0x04001C54 RID: 7252
		private const string readDate = "read-date";

		// Token: 0x04001C55 RID: 7253
		private const string modificationDate = "modification-date";

		// Token: 0x04001C56 RID: 7254
		private const string size = "size";

		// Token: 0x04001C57 RID: 7255
		private const string fileName = "filename";

		// Token: 0x04001C58 RID: 7256
		private static readonly TrackingValidationObjectDictionary.ValidateAndParseValue dateParser = (object value) => new SmtpDateTime(value.ToString());

		// Token: 0x04001C59 RID: 7257
		private static readonly TrackingValidationObjectDictionary.ValidateAndParseValue longParser = delegate(object value)
		{
			long num;
			if (!long.TryParse(value.ToString(), NumberStyles.None, CultureInfo.InvariantCulture, out num))
			{
				throw new FormatException(SR.GetString("The specified content disposition is invalid."));
			}
			return num;
		};

		// Token: 0x04001C5A RID: 7258
		private static readonly IDictionary<string, TrackingValidationObjectDictionary.ValidateAndParseValue> validators = new Dictionary<string, TrackingValidationObjectDictionary.ValidateAndParseValue>();

		// Token: 0x02000438 RID: 1080
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060021F0 RID: 8688 RVA: 0x0007E27C File Offset: 0x0007C47C
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060021F1 RID: 8689 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c()
			{
			}

			// Token: 0x060021F2 RID: 8690 RVA: 0x0007E288 File Offset: 0x0007C488
			internal object <.cctor>b__13_0(object value)
			{
				return new SmtpDateTime(value.ToString());
			}

			// Token: 0x060021F3 RID: 8691 RVA: 0x0007E298 File Offset: 0x0007C498
			internal object <.cctor>b__13_1(object value)
			{
				long num;
				if (!long.TryParse(value.ToString(), NumberStyles.None, CultureInfo.InvariantCulture, out num))
				{
					throw new FormatException(SR.GetString("The specified content disposition is invalid."));
				}
				return num;
			}

			// Token: 0x04001C5B RID: 7259
			public static readonly ContentDisposition.<>c <>9 = new ContentDisposition.<>c();
		}
	}
}
