﻿using System;

namespace System.Net
{
	// Token: 0x0200037C RID: 892
	internal struct HeaderVariantInfo
	{
		// Token: 0x060019A5 RID: 6565 RVA: 0x0005CEAD File Offset: 0x0005B0AD
		internal HeaderVariantInfo(string name, CookieVariant variant)
		{
			this.m_name = name;
			this.m_variant = variant;
		}

		// Token: 0x1700055A RID: 1370
		// (get) Token: 0x060019A6 RID: 6566 RVA: 0x0005CEBD File Offset: 0x0005B0BD
		internal string Name
		{
			get
			{
				return this.m_name;
			}
		}

		// Token: 0x1700055B RID: 1371
		// (get) Token: 0x060019A7 RID: 6567 RVA: 0x0005CEC5 File Offset: 0x0005B0C5
		internal CookieVariant Variant
		{
			get
			{
				return this.m_variant;
			}
		}

		// Token: 0x04001856 RID: 6230
		private string m_name;

		// Token: 0x04001857 RID: 6231
		private CookieVariant m_variant;
	}
}
