﻿using System;

namespace System.Net
{
	// Token: 0x02000374 RID: 884
	internal enum CookieToken
	{
		// Token: 0x04001824 RID: 6180
		Nothing,
		// Token: 0x04001825 RID: 6181
		NameValuePair,
		// Token: 0x04001826 RID: 6182
		Attribute,
		// Token: 0x04001827 RID: 6183
		EndToken,
		// Token: 0x04001828 RID: 6184
		EndCookie,
		// Token: 0x04001829 RID: 6185
		End,
		// Token: 0x0400182A RID: 6186
		Equals,
		// Token: 0x0400182B RID: 6187
		Comment,
		// Token: 0x0400182C RID: 6188
		CommentUrl,
		// Token: 0x0400182D RID: 6189
		CookieName,
		// Token: 0x0400182E RID: 6190
		Discard,
		// Token: 0x0400182F RID: 6191
		Domain,
		// Token: 0x04001830 RID: 6192
		Expires,
		// Token: 0x04001831 RID: 6193
		MaxAge,
		// Token: 0x04001832 RID: 6194
		Path,
		// Token: 0x04001833 RID: 6195
		Port,
		// Token: 0x04001834 RID: 6196
		Secure,
		// Token: 0x04001835 RID: 6197
		HttpOnly,
		// Token: 0x04001836 RID: 6198
		Unknown,
		// Token: 0x04001837 RID: 6199
		Version
	}
}
