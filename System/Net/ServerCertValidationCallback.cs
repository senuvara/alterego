﻿using System;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace System.Net
{
	// Token: 0x020003BF RID: 959
	internal class ServerCertValidationCallback
	{
		// Token: 0x06001C23 RID: 7203 RVA: 0x00065838 File Offset: 0x00063A38
		internal ServerCertValidationCallback(RemoteCertificateValidationCallback validationCallback)
		{
			this.m_ValidationCallback = validationCallback;
			this.m_Context = ExecutionContext.Capture();
		}

		// Token: 0x170005B8 RID: 1464
		// (get) Token: 0x06001C24 RID: 7204 RVA: 0x00065852 File Offset: 0x00063A52
		internal RemoteCertificateValidationCallback ValidationCallback
		{
			get
			{
				return this.m_ValidationCallback;
			}
		}

		// Token: 0x06001C25 RID: 7205 RVA: 0x0006585C File Offset: 0x00063A5C
		internal void Callback(object state)
		{
			ServerCertValidationCallback.CallbackContext callbackContext = (ServerCertValidationCallback.CallbackContext)state;
			callbackContext.result = this.m_ValidationCallback(callbackContext.request, callbackContext.certificate, callbackContext.chain, callbackContext.sslPolicyErrors);
		}

		// Token: 0x06001C26 RID: 7206 RVA: 0x0006589C File Offset: 0x00063A9C
		internal bool Invoke(object request, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			if (this.m_Context == null)
			{
				return this.m_ValidationCallback(request, certificate, chain, sslPolicyErrors);
			}
			ExecutionContext executionContext = this.m_Context.CreateCopy();
			ServerCertValidationCallback.CallbackContext callbackContext = new ServerCertValidationCallback.CallbackContext(request, certificate, chain, sslPolicyErrors);
			ExecutionContext.Run(executionContext, new ContextCallback(this.Callback), callbackContext);
			return callbackContext.result;
		}

		// Token: 0x04001966 RID: 6502
		private readonly RemoteCertificateValidationCallback m_ValidationCallback;

		// Token: 0x04001967 RID: 6503
		private readonly ExecutionContext m_Context;

		// Token: 0x020003C0 RID: 960
		private class CallbackContext
		{
			// Token: 0x06001C27 RID: 7207 RVA: 0x000658F0 File Offset: 0x00063AF0
			internal CallbackContext(object request, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
			{
				this.request = request;
				this.certificate = certificate;
				this.chain = chain;
				this.sslPolicyErrors = sslPolicyErrors;
			}

			// Token: 0x04001968 RID: 6504
			internal readonly object request;

			// Token: 0x04001969 RID: 6505
			internal readonly X509Certificate certificate;

			// Token: 0x0400196A RID: 6506
			internal readonly X509Chain chain;

			// Token: 0x0400196B RID: 6507
			internal readonly SslPolicyErrors sslPolicyErrors;

			// Token: 0x0400196C RID: 6508
			internal bool result;
		}
	}
}
