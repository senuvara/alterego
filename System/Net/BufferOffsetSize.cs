﻿using System;

namespace System.Net
{
	// Token: 0x02000343 RID: 835
	internal class BufferOffsetSize
	{
		// Token: 0x0600181E RID: 6174 RVA: 0x00057430 File Offset: 0x00055630
		internal BufferOffsetSize(byte[] buffer, int offset, int size, bool copyBuffer)
		{
			if (copyBuffer)
			{
				byte[] array = new byte[size];
				System.Buffer.BlockCopy(buffer, offset, array, 0, size);
				offset = 0;
				buffer = array;
			}
			this.Buffer = buffer;
			this.Offset = offset;
			this.Size = size;
		}

		// Token: 0x0600181F RID: 6175 RVA: 0x00057473 File Offset: 0x00055673
		internal BufferOffsetSize(byte[] buffer, bool copyBuffer) : this(buffer, 0, buffer.Length, copyBuffer)
		{
		}

		// Token: 0x04001712 RID: 5906
		internal byte[] Buffer;

		// Token: 0x04001713 RID: 5907
		internal int Offset;

		// Token: 0x04001714 RID: 5908
		internal int Size;
	}
}
