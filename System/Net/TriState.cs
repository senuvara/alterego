﻿using System;

namespace System.Net
{
	// Token: 0x0200031A RID: 794
	internal enum TriState
	{
		// Token: 0x0400164B RID: 5707
		Unspecified = -1,
		// Token: 0x0400164C RID: 5708
		False,
		// Token: 0x0400164D RID: 5709
		True
	}
}
