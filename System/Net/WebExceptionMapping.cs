﻿using System;
using System.Threading;

namespace System.Net
{
	// Token: 0x02000329 RID: 809
	internal static class WebExceptionMapping
	{
		// Token: 0x0600170A RID: 5898 RVA: 0x0005255C File Offset: 0x0005075C
		internal static string GetWebStatusString(WebExceptionStatus status)
		{
			int num = (int)status;
			if (num >= WebExceptionMapping.s_Mapping.Length || num < 0)
			{
				throw new InternalException();
			}
			string text = Volatile.Read<string>(ref WebExceptionMapping.s_Mapping[num]);
			if (text == null)
			{
				text = "net_webstatus_" + status.ToString();
				Volatile.Write<string>(ref WebExceptionMapping.s_Mapping[num], text);
			}
			return text;
		}

		// Token: 0x0600170B RID: 5899 RVA: 0x000525BD File Offset: 0x000507BD
		// Note: this type is marked as 'beforefieldinit'.
		static WebExceptionMapping()
		{
		}

		// Token: 0x04001695 RID: 5781
		private static readonly string[] s_Mapping = new string[21];
	}
}
