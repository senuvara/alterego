﻿using System;
using System.Text.RegularExpressions;

namespace System.Net
{
	// Token: 0x02000331 RID: 817
	[Serializable]
	internal class DelayedRegex
	{
		// Token: 0x06001773 RID: 6003 RVA: 0x000547B0 File Offset: 0x000529B0
		internal DelayedRegex(string regexString)
		{
			if (regexString == null)
			{
				throw new ArgumentNullException("regexString");
			}
			this._AsString = regexString;
		}

		// Token: 0x06001774 RID: 6004 RVA: 0x000547CD File Offset: 0x000529CD
		internal DelayedRegex(Regex regex)
		{
			if (regex == null)
			{
				throw new ArgumentNullException("regex");
			}
			this._AsRegex = regex;
		}

		// Token: 0x170004D0 RID: 1232
		// (get) Token: 0x06001775 RID: 6005 RVA: 0x000547EA File Offset: 0x000529EA
		internal Regex AsRegex
		{
			get
			{
				if (this._AsRegex == null)
				{
					this._AsRegex = new Regex(this._AsString + "[/]?", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.CultureInvariant);
				}
				return this._AsRegex;
			}
		}

		// Token: 0x06001776 RID: 6006 RVA: 0x0005481C File Offset: 0x00052A1C
		public override string ToString()
		{
			if (this._AsString == null)
			{
				return this._AsString = this._AsRegex.ToString();
			}
			return this._AsString;
		}

		// Token: 0x040016CF RID: 5839
		private Regex _AsRegex;

		// Token: 0x040016D0 RID: 5840
		private string _AsString;
	}
}
