﻿using System;
using System.Collections;

namespace System.Net
{
	// Token: 0x0200037E RID: 894
	[Serializable]
	internal class PathList
	{
		// Token: 0x060019C4 RID: 6596 RVA: 0x0005E26C File Offset: 0x0005C46C
		public PathList()
		{
		}

		// Token: 0x17000560 RID: 1376
		// (get) Token: 0x060019C5 RID: 6597 RVA: 0x0005E289 File Offset: 0x0005C489
		public int Count
		{
			get
			{
				return this.m_list.Count;
			}
		}

		// Token: 0x060019C6 RID: 6598 RVA: 0x0005E298 File Offset: 0x0005C498
		public int GetCookiesCount()
		{
			int num = 0;
			object syncRoot = this.SyncRoot;
			lock (syncRoot)
			{
				foreach (object obj in this.m_list.Values)
				{
					CookieCollection cookieCollection = (CookieCollection)obj;
					num += cookieCollection.Count;
				}
			}
			return num;
		}

		// Token: 0x17000561 RID: 1377
		// (get) Token: 0x060019C7 RID: 6599 RVA: 0x0005E328 File Offset: 0x0005C528
		public ICollection Values
		{
			get
			{
				return this.m_list.Values;
			}
		}

		// Token: 0x17000562 RID: 1378
		public object this[string s]
		{
			get
			{
				return this.m_list[s];
			}
			set
			{
				object syncRoot = this.SyncRoot;
				lock (syncRoot)
				{
					this.m_list[s] = value;
				}
			}
		}

		// Token: 0x060019CA RID: 6602 RVA: 0x0005E38C File Offset: 0x0005C58C
		public IEnumerator GetEnumerator()
		{
			return this.m_list.GetEnumerator();
		}

		// Token: 0x17000563 RID: 1379
		// (get) Token: 0x060019CB RID: 6603 RVA: 0x0005E399 File Offset: 0x0005C599
		public object SyncRoot
		{
			get
			{
				return this.m_list.SyncRoot;
			}
		}

		// Token: 0x04001862 RID: 6242
		private SortedList m_list = SortedList.Synchronized(new SortedList(PathList.PathListComparer.StaticInstance));

		// Token: 0x0200037F RID: 895
		[Serializable]
		private class PathListComparer : IComparer
		{
			// Token: 0x060019CC RID: 6604 RVA: 0x0005E3A8 File Offset: 0x0005C5A8
			int IComparer.Compare(object ol, object or)
			{
				string text = CookieParser.CheckQuoted((string)ol);
				string text2 = CookieParser.CheckQuoted((string)or);
				int length = text.Length;
				int length2 = text2.Length;
				int num = Math.Min(length, length2);
				for (int i = 0; i < num; i++)
				{
					if (text[i] != text2[i])
					{
						return (int)(text[i] - text2[i]);
					}
				}
				return length2 - length;
			}

			// Token: 0x060019CD RID: 6605 RVA: 0x0000232F File Offset: 0x0000052F
			public PathListComparer()
			{
			}

			// Token: 0x060019CE RID: 6606 RVA: 0x0005E41C File Offset: 0x0005C61C
			// Note: this type is marked as 'beforefieldinit'.
			static PathListComparer()
			{
			}

			// Token: 0x04001863 RID: 6243
			internal static readonly PathList.PathListComparer StaticInstance = new PathList.PathListComparer();
		}
	}
}
