﻿using System;

namespace System.Net
{
	// Token: 0x02000352 RID: 850
	[Flags]
	internal enum ThreadKinds
	{
		// Token: 0x04001773 RID: 6003
		Unknown = 0,
		// Token: 0x04001774 RID: 6004
		User = 1,
		// Token: 0x04001775 RID: 6005
		System = 2,
		// Token: 0x04001776 RID: 6006
		Sync = 4,
		// Token: 0x04001777 RID: 6007
		Async = 8,
		// Token: 0x04001778 RID: 6008
		Timer = 16,
		// Token: 0x04001779 RID: 6009
		CompletionPort = 32,
		// Token: 0x0400177A RID: 6010
		Worker = 64,
		// Token: 0x0400177B RID: 6011
		Finalization = 128,
		// Token: 0x0400177C RID: 6012
		Other = 256,
		// Token: 0x0400177D RID: 6013
		OwnerMask = 3,
		// Token: 0x0400177E RID: 6014
		SyncMask = 12,
		// Token: 0x0400177F RID: 6015
		SourceMask = 496,
		// Token: 0x04001780 RID: 6016
		SafeSources = 352,
		// Token: 0x04001781 RID: 6017
		ThreadPool = 96
	}
}
