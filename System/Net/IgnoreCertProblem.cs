﻿using System;

namespace System.Net
{
	// Token: 0x02000308 RID: 776
	internal enum IgnoreCertProblem
	{
		// Token: 0x040015A0 RID: 5536
		not_time_valid = 1,
		// Token: 0x040015A1 RID: 5537
		ctl_not_time_valid,
		// Token: 0x040015A2 RID: 5538
		not_time_nested = 4,
		// Token: 0x040015A3 RID: 5539
		invalid_basic_constraints = 8,
		// Token: 0x040015A4 RID: 5540
		all_not_time_valid = 7,
		// Token: 0x040015A5 RID: 5541
		allow_unknown_ca = 16,
		// Token: 0x040015A6 RID: 5542
		wrong_usage = 32,
		// Token: 0x040015A7 RID: 5543
		invalid_name = 64,
		// Token: 0x040015A8 RID: 5544
		invalid_policy = 128,
		// Token: 0x040015A9 RID: 5545
		end_rev_unknown = 256,
		// Token: 0x040015AA RID: 5546
		ctl_signer_rev_unknown = 512,
		// Token: 0x040015AB RID: 5547
		ca_rev_unknown = 1024,
		// Token: 0x040015AC RID: 5548
		root_rev_unknown = 2048,
		// Token: 0x040015AD RID: 5549
		all_rev_unknown = 3840,
		// Token: 0x040015AE RID: 5550
		none = 4095
	}
}
