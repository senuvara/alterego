﻿using System;

namespace System.Net
{
	// Token: 0x02000351 RID: 849
	internal class BaseLoggingObject
	{
		// Token: 0x0600184B RID: 6219 RVA: 0x0000232F File Offset: 0x0000052F
		internal BaseLoggingObject()
		{
		}

		// Token: 0x0600184C RID: 6220 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void EnterFunc(string funcname)
		{
		}

		// Token: 0x0600184D RID: 6221 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void LeaveFunc(string funcname)
		{
		}

		// Token: 0x0600184E RID: 6222 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void DumpArrayToConsole()
		{
		}

		// Token: 0x0600184F RID: 6223 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void PrintLine(string msg)
		{
		}

		// Token: 0x06001850 RID: 6224 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void DumpArray(bool shouldClose)
		{
		}

		// Token: 0x06001851 RID: 6225 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void DumpArrayToFile(bool shouldClose)
		{
		}

		// Token: 0x06001852 RID: 6226 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void Flush()
		{
		}

		// Token: 0x06001853 RID: 6227 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void Flush(bool close)
		{
		}

		// Token: 0x06001854 RID: 6228 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void LoggingMonitorTick()
		{
		}

		// Token: 0x06001855 RID: 6229 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void Dump(byte[] buffer)
		{
		}

		// Token: 0x06001856 RID: 6230 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void Dump(byte[] buffer, int length)
		{
		}

		// Token: 0x06001857 RID: 6231 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void Dump(byte[] buffer, int offset, int length)
		{
		}

		// Token: 0x06001858 RID: 6232 RVA: 0x0000232D File Offset: 0x0000052D
		internal virtual void Dump(IntPtr pBuffer, int offset, int length)
		{
		}
	}
}
