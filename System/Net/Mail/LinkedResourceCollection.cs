﻿using System;
using System.Collections.ObjectModel;

namespace System.Net.Mail
{
	/// <summary>Stores linked resources to be sent as part of an e-mail message.</summary>
	// Token: 0x02000420 RID: 1056
	public sealed class LinkedResourceCollection : Collection<LinkedResource>, IDisposable
	{
		// Token: 0x06002104 RID: 8452 RVA: 0x0007AECB File Offset: 0x000790CB
		internal LinkedResourceCollection()
		{
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Net.Mail.LinkedResourceCollection" />.</summary>
		// Token: 0x06002105 RID: 8453 RVA: 0x0007AED3 File Offset: 0x000790D3
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06002106 RID: 8454 RVA: 0x0000232D File Offset: 0x0000052D
		private void Dispose(bool disposing)
		{
		}

		// Token: 0x06002107 RID: 8455 RVA: 0x0007AEE2 File Offset: 0x000790E2
		protected override void ClearItems()
		{
			base.ClearItems();
		}

		// Token: 0x06002108 RID: 8456 RVA: 0x0007AEEA File Offset: 0x000790EA
		protected override void InsertItem(int index, LinkedResource item)
		{
			base.InsertItem(index, item);
		}

		// Token: 0x06002109 RID: 8457 RVA: 0x0007AEF4 File Offset: 0x000790F4
		protected override void RemoveItem(int index)
		{
			base.RemoveItem(index);
		}

		// Token: 0x0600210A RID: 8458 RVA: 0x0007AEFD File Offset: 0x000790FD
		protected override void SetItem(int index, LinkedResource item)
		{
			base.SetItem(index, item);
		}
	}
}
