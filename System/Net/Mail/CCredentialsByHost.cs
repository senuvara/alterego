﻿using System;

namespace System.Net.Mail
{
	// Token: 0x0200042E RID: 1070
	internal class CCredentialsByHost : ICredentialsByHost
	{
		// Token: 0x060021A2 RID: 8610 RVA: 0x0007D591 File Offset: 0x0007B791
		public CCredentialsByHost(string userName, string password)
		{
			this.userName = userName;
			this.password = password;
		}

		// Token: 0x060021A3 RID: 8611 RVA: 0x0007D5A7 File Offset: 0x0007B7A7
		public NetworkCredential GetCredential(string host, int port, string authenticationType)
		{
			return new NetworkCredential(this.userName, this.password);
		}

		// Token: 0x04001C1C RID: 7196
		private string userName;

		// Token: 0x04001C1D RID: 7197
		private string password;
	}
}
