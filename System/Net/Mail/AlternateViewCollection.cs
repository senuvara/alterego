﻿using System;
using System.Collections.ObjectModel;

namespace System.Net.Mail
{
	/// <summary>Represents a collection of <see cref="T:System.Net.Mail.AlternateView" /> objects.</summary>
	// Token: 0x02000419 RID: 1049
	public sealed class AlternateViewCollection : Collection<AlternateView>, IDisposable
	{
		// Token: 0x060020CC RID: 8396 RVA: 0x00078774 File Offset: 0x00076974
		internal AlternateViewCollection()
		{
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Net.Mail.AlternateViewCollection" />.</summary>
		// Token: 0x060020CD RID: 8397 RVA: 0x0000232D File Offset: 0x0000052D
		public void Dispose()
		{
		}

		// Token: 0x060020CE RID: 8398 RVA: 0x0007877C File Offset: 0x0007697C
		protected override void ClearItems()
		{
			base.ClearItems();
		}

		// Token: 0x060020CF RID: 8399 RVA: 0x00078784 File Offset: 0x00076984
		protected override void InsertItem(int index, AlternateView item)
		{
			base.InsertItem(index, item);
		}

		// Token: 0x060020D0 RID: 8400 RVA: 0x0007878E File Offset: 0x0007698E
		protected override void RemoveItem(int index)
		{
			base.RemoveItem(index);
		}

		// Token: 0x060020D1 RID: 8401 RVA: 0x00078797 File Offset: 0x00076997
		protected override void SetItem(int index, AlternateView item)
		{
			base.SetItem(index, item);
		}
	}
}
