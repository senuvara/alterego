﻿using System;

namespace System.Net.Mail
{
	/// <summary>Describes the delivery notification options for e-mail.</summary>
	// Token: 0x0200041E RID: 1054
	[Flags]
	public enum DeliveryNotificationOptions
	{
		/// <summary>No notification information will be sent. The mail server will utilize its configured behavior to determine whether it should generate a delivery notification.</summary>
		// Token: 0x04001BC8 RID: 7112
		None = 0,
		/// <summary>Notify if the delivery is successful.</summary>
		// Token: 0x04001BC9 RID: 7113
		OnSuccess = 1,
		/// <summary>Notify if the delivery is unsuccessful.</summary>
		// Token: 0x04001BCA RID: 7114
		OnFailure = 2,
		/// <summary>Notify if the delivery is delayed.</summary>
		// Token: 0x04001BCB RID: 7115
		Delay = 4,
		/// <summary>A notification should not be generated under any circumstances.</summary>
		// Token: 0x04001BCC RID: 7116
		Never = 134217728
	}
}
