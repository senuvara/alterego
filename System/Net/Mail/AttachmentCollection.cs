﻿using System;
using System.Collections.ObjectModel;

namespace System.Net.Mail
{
	/// <summary>Stores attachments to be sent as part of an e-mail message.</summary>
	// Token: 0x0200041D RID: 1053
	public sealed class AttachmentCollection : Collection<Attachment>, IDisposable
	{
		// Token: 0x060020F3 RID: 8435 RVA: 0x0007AD7E File Offset: 0x00078F7E
		internal AttachmentCollection()
		{
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Net.Mail.AttachmentCollection" />. </summary>
		// Token: 0x060020F4 RID: 8436 RVA: 0x0007AD88 File Offset: 0x00078F88
		public void Dispose()
		{
			for (int i = 0; i < base.Count; i++)
			{
				base[i].Dispose();
			}
		}

		// Token: 0x060020F5 RID: 8437 RVA: 0x0007ADB2 File Offset: 0x00078FB2
		protected override void ClearItems()
		{
			base.ClearItems();
		}

		// Token: 0x060020F6 RID: 8438 RVA: 0x0007ADBA File Offset: 0x00078FBA
		protected override void InsertItem(int index, Attachment item)
		{
			base.InsertItem(index, item);
		}

		// Token: 0x060020F7 RID: 8439 RVA: 0x0007ADC4 File Offset: 0x00078FC4
		protected override void RemoveItem(int index)
		{
			base.RemoveItem(index);
		}

		// Token: 0x060020F8 RID: 8440 RVA: 0x0007ADCD File Offset: 0x00078FCD
		protected override void SetItem(int index, Attachment item)
		{
			base.SetItem(index, item);
		}
	}
}
