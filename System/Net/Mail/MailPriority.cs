﻿using System;

namespace System.Net.Mail
{
	/// <summary>Specifies the priority of a <see cref="T:System.Net.Mail.MailMessage" />.</summary>
	// Token: 0x02000424 RID: 1060
	public enum MailPriority
	{
		/// <summary>The email has normal priority.</summary>
		// Token: 0x04001BE7 RID: 7143
		Normal,
		/// <summary>The email has low priority.</summary>
		// Token: 0x04001BE8 RID: 7144
		Low,
		/// <summary>The email has high priority.</summary>
		// Token: 0x04001BE9 RID: 7145
		High
	}
}
