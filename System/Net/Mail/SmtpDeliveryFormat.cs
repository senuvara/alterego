﻿using System;

namespace System.Net.Mail
{
	/// <summary>The delivery format to use for sending outgoing e-mail using the Simple Mail Transport Protocol (SMTP).</summary>
	// Token: 0x0200042F RID: 1071
	public enum SmtpDeliveryFormat
	{
		/// <summary>A delivery format using 7-bit ASCII. The traditional delivery format used in the Simple Mail Transport Protocol (SMTP) for mail messages.</summary>
		// Token: 0x04001C1F RID: 7199
		SevenBit,
		/// <summary>A delivery format where non-ASCII characters in the envelope and header fields used in the Simple Mail Transport Protocol (SMTP) for mail messages are encoded with UTF-8 characters. The extensions to support international e-mail are defined in IETF RFC 6530, 6531, and 6532.</summary>
		// Token: 0x04001C20 RID: 7200
		International
	}
}
