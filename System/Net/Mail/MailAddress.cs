﻿using System;
using System.Text;

namespace System.Net.Mail
{
	/// <summary>Represents the address of an electronic mail sender or recipient.</summary>
	// Token: 0x02000421 RID: 1057
	public class MailAddress
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Mail.MailAddress" /> class using the specified address. </summary>
		/// <param name="address">A <see cref="T:System.String" /> that contains an e-mail address.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="address" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="address" /> is <see cref="F:System.String.Empty" /> ("").</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="address" /> is not in a recognized format.</exception>
		// Token: 0x0600210B RID: 8459 RVA: 0x0007AF07 File Offset: 0x00079107
		public MailAddress(string address) : this(address, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Mail.MailAddress" /> class using the specified address and display name.</summary>
		/// <param name="address">A <see cref="T:System.String" /> that contains an e-mail address.</param>
		/// <param name="displayName">A <see cref="T:System.String" /> that contains the display name associated with <paramref name="address" />. This parameter can be <see langword="null" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="address" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="address" /> is <see cref="F:System.String.Empty" /> ("").</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="address" /> is not in a recognized format.-or-
		///         <paramref name="address" /> contains non-ASCII characters.</exception>
		// Token: 0x0600210C RID: 8460 RVA: 0x0007AF11 File Offset: 0x00079111
		public MailAddress(string address, string displayName) : this(address, displayName, Encoding.UTF8)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Mail.MailAddress" /> class using the specified address, display name, and encoding.</summary>
		/// <param name="address">A <see cref="T:System.String" /> that contains an e-mail address.</param>
		/// <param name="displayName">A <see cref="T:System.String" /> that contains the display name associated with <paramref name="address" />.</param>
		/// <param name="displayNameEncoding">The <see cref="T:System.Text.Encoding" /> that defines the character set used for <paramref name="displayName" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="address" /> is <see langword="null" />.-or-
		///         <paramref name="displayName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="address" /> is <see cref="F:System.String.Empty" /> ("").-or-
		///         <paramref name="displayName" /> is <see cref="F:System.String.Empty" /> ("").</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="address" /> is not in a recognized format.-or-
		///         <paramref name="address" /> contains non-ASCII characters.</exception>
		// Token: 0x0600210D RID: 8461 RVA: 0x0007AF20 File Offset: 0x00079120
		[MonoTODO("We don't do anything with displayNameEncoding")]
		public MailAddress(string address, string displayName, Encoding displayNameEncoding)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (address.Length == 0)
			{
				throw new ArgumentException("address");
			}
			if (displayName != null)
			{
				this.displayName = displayName.Trim();
			}
			this.ParseAddress(address);
		}

		// Token: 0x0600210E RID: 8462 RVA: 0x0007AF60 File Offset: 0x00079160
		private void ParseAddress(string address)
		{
			address = address.Trim();
			int num = address.IndexOf('"');
			if (num != -1)
			{
				if (num != 0 || address.Length == 1)
				{
					throw MailAddress.CreateFormatException();
				}
				int num2 = address.LastIndexOf('"');
				if (num2 == num)
				{
					throw MailAddress.CreateFormatException();
				}
				if (this.displayName == null)
				{
					this.displayName = address.Substring(num + 1, num2 - num - 1).Trim();
				}
				address = address.Substring(num2 + 1).Trim();
			}
			num = address.IndexOf('<');
			if (num >= 0)
			{
				if (this.displayName == null)
				{
					this.displayName = address.Substring(0, num).Trim();
				}
				if (address.Length - 1 == num)
				{
					throw MailAddress.CreateFormatException();
				}
				int num3 = address.IndexOf('>', num + 1);
				if (num3 == -1)
				{
					throw MailAddress.CreateFormatException();
				}
				address = address.Substring(num + 1, num3 - num - 1).Trim();
			}
			this.address = address;
			num = address.IndexOf('@');
			if (num <= 0)
			{
				throw MailAddress.CreateFormatException();
			}
			if (num != address.LastIndexOf('@'))
			{
				throw MailAddress.CreateFormatException();
			}
			this.user = address.Substring(0, num).Trim();
			if (this.user.Length == 0)
			{
				throw MailAddress.CreateFormatException();
			}
			this.host = address.Substring(num + 1).Trim();
			if (this.host.Length == 0)
			{
				throw MailAddress.CreateFormatException();
			}
		}

		/// <summary>Gets the e-mail address specified when this instance was created.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the e-mail address.</returns>
		// Token: 0x1700072A RID: 1834
		// (get) Token: 0x0600210F RID: 8463 RVA: 0x0007B0B4 File Offset: 0x000792B4
		public string Address
		{
			get
			{
				return this.address;
			}
		}

		/// <summary>Gets the display name composed from the display name and address information specified when this instance was created.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the display name; otherwise, <see cref="F:System.String.Empty" /> ("") if no display name information was specified when this instance was created.</returns>
		// Token: 0x1700072B RID: 1835
		// (get) Token: 0x06002110 RID: 8464 RVA: 0x0007B0BC File Offset: 0x000792BC
		public string DisplayName
		{
			get
			{
				if (this.displayName == null)
				{
					return string.Empty;
				}
				return this.displayName;
			}
		}

		/// <summary>Gets the host portion of the address specified when this instance was created.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the name of the host computer that accepts e-mail for the <see cref="P:System.Net.Mail.MailAddress.User" /> property.</returns>
		// Token: 0x1700072C RID: 1836
		// (get) Token: 0x06002111 RID: 8465 RVA: 0x0007B0D2 File Offset: 0x000792D2
		public string Host
		{
			get
			{
				return this.host;
			}
		}

		/// <summary>Gets the user information from the address specified when this instance was created.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the user name portion of the <see cref="P:System.Net.Mail.MailAddress.Address" />.</returns>
		// Token: 0x1700072D RID: 1837
		// (get) Token: 0x06002112 RID: 8466 RVA: 0x0007B0DA File Offset: 0x000792DA
		public string User
		{
			get
			{
				return this.user;
			}
		}

		/// <summary>Compares two mail addresses.</summary>
		/// <param name="value">A <see cref="T:System.Net.Mail.MailAddress" /> instance to compare to the current instance.</param>
		/// <returns>
		///     <see langword="true" /> if the two mail addresses are equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002113 RID: 8467 RVA: 0x0007B0E2 File Offset: 0x000792E2
		public override bool Equals(object value)
		{
			return value != null && string.Compare(this.ToString(), value.ToString(), StringComparison.OrdinalIgnoreCase) == 0;
		}

		/// <summary>Returns a hash value for a mail address.</summary>
		/// <returns>An integer hash value.</returns>
		// Token: 0x06002114 RID: 8468 RVA: 0x00067EAE File Offset: 0x000660AE
		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}

		/// <summary>Returns a string representation of this instance.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the contents of this <see cref="T:System.Net.Mail.MailAddress" />.</returns>
		// Token: 0x06002115 RID: 8469 RVA: 0x0007B100 File Offset: 0x00079300
		public override string ToString()
		{
			if (this.to_string != null)
			{
				return this.to_string;
			}
			if (!string.IsNullOrEmpty(this.displayName))
			{
				this.to_string = string.Format("\"{0}\" <{1}>", this.DisplayName, this.Address);
			}
			else
			{
				this.to_string = this.address;
			}
			return this.to_string;
		}

		// Token: 0x06002116 RID: 8470 RVA: 0x0007B159 File Offset: 0x00079359
		private static FormatException CreateFormatException()
		{
			return new FormatException("The specified string is not in the form required for an e-mail address.");
		}

		// Token: 0x04001BCE RID: 7118
		private string address;

		// Token: 0x04001BCF RID: 7119
		private string displayName;

		// Token: 0x04001BD0 RID: 7120
		private string host;

		// Token: 0x04001BD1 RID: 7121
		private string user;

		// Token: 0x04001BD2 RID: 7122
		private string to_string;
	}
}
