﻿using System;

namespace System.Net.Mail
{
	/// <summary>Specifies the level of access allowed to a Simple Mail Transport Protocol (SMTP) server.</summary>
	// Token: 0x02000426 RID: 1062
	public enum SmtpAccess
	{
		/// <summary>No access to an SMTP host.</summary>
		// Token: 0x04001BEB RID: 7147
		None,
		/// <summary>Connection to an SMTP host on the default port (port 25).</summary>
		// Token: 0x04001BEC RID: 7148
		Connect,
		/// <summary>Connection to an SMTP host on any port.</summary>
		// Token: 0x04001BED RID: 7149
		ConnectToUnrestrictedPort
	}
}
