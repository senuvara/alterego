﻿using System;

namespace System.Net.Mail
{
	// Token: 0x02000415 RID: 1045
	internal enum MailHeaderID
	{
		// Token: 0x04001B94 RID: 7060
		Bcc,
		// Token: 0x04001B95 RID: 7061
		Cc,
		// Token: 0x04001B96 RID: 7062
		Comments,
		// Token: 0x04001B97 RID: 7063
		ContentDescription,
		// Token: 0x04001B98 RID: 7064
		ContentDisposition,
		// Token: 0x04001B99 RID: 7065
		ContentID,
		// Token: 0x04001B9A RID: 7066
		ContentLocation,
		// Token: 0x04001B9B RID: 7067
		ContentTransferEncoding,
		// Token: 0x04001B9C RID: 7068
		ContentType,
		// Token: 0x04001B9D RID: 7069
		Date,
		// Token: 0x04001B9E RID: 7070
		From,
		// Token: 0x04001B9F RID: 7071
		Importance,
		// Token: 0x04001BA0 RID: 7072
		InReplyTo,
		// Token: 0x04001BA1 RID: 7073
		Keywords,
		// Token: 0x04001BA2 RID: 7074
		Max,
		// Token: 0x04001BA3 RID: 7075
		MessageID,
		// Token: 0x04001BA4 RID: 7076
		MimeVersion,
		// Token: 0x04001BA5 RID: 7077
		Priority,
		// Token: 0x04001BA6 RID: 7078
		References,
		// Token: 0x04001BA7 RID: 7079
		ReplyTo,
		// Token: 0x04001BA8 RID: 7080
		ResentBcc,
		// Token: 0x04001BA9 RID: 7081
		ResentCc,
		// Token: 0x04001BAA RID: 7082
		ResentDate,
		// Token: 0x04001BAB RID: 7083
		ResentFrom,
		// Token: 0x04001BAC RID: 7084
		ResentMessageID,
		// Token: 0x04001BAD RID: 7085
		ResentSender,
		// Token: 0x04001BAE RID: 7086
		ResentTo,
		// Token: 0x04001BAF RID: 7087
		Sender,
		// Token: 0x04001BB0 RID: 7088
		Subject,
		// Token: 0x04001BB1 RID: 7089
		To,
		// Token: 0x04001BB2 RID: 7090
		XPriority,
		// Token: 0x04001BB3 RID: 7091
		XReceiver,
		// Token: 0x04001BB4 RID: 7092
		XSender,
		// Token: 0x04001BB5 RID: 7093
		ZMaxEnumValue = 32,
		// Token: 0x04001BB6 RID: 7094
		Unknown = -1
	}
}
