﻿using System;
using System.Text;

namespace System.Net.Mail
{
	// Token: 0x02000414 RID: 1044
	internal class BufferBuilder
	{
		// Token: 0x060020A8 RID: 8360 RVA: 0x00077F20 File Offset: 0x00076120
		internal BufferBuilder() : this(256)
		{
		}

		// Token: 0x060020A9 RID: 8361 RVA: 0x00077F2D File Offset: 0x0007612D
		internal BufferBuilder(int initialSize)
		{
			this.buffer = new byte[initialSize];
		}

		// Token: 0x060020AA RID: 8362 RVA: 0x00077F44 File Offset: 0x00076144
		private void EnsureBuffer(int count)
		{
			if (count > this.buffer.Length - this.offset)
			{
				byte[] dst = new byte[(this.buffer.Length * 2 > this.buffer.Length + count) ? (this.buffer.Length * 2) : (this.buffer.Length + count)];
				Buffer.BlockCopy(this.buffer, 0, dst, 0, this.offset);
				this.buffer = dst;
			}
		}

		// Token: 0x060020AB RID: 8363 RVA: 0x00077FB0 File Offset: 0x000761B0
		internal void Append(byte value)
		{
			this.EnsureBuffer(1);
			byte[] array = this.buffer;
			int num = this.offset;
			this.offset = num + 1;
			array[num] = value;
		}

		// Token: 0x060020AC RID: 8364 RVA: 0x00077FDD File Offset: 0x000761DD
		internal void Append(byte[] value)
		{
			this.Append(value, 0, value.Length);
		}

		// Token: 0x060020AD RID: 8365 RVA: 0x00077FEA File Offset: 0x000761EA
		internal void Append(byte[] value, int offset, int count)
		{
			this.EnsureBuffer(count);
			Buffer.BlockCopy(value, offset, this.buffer, this.offset, count);
			this.offset += count;
		}

		// Token: 0x060020AE RID: 8366 RVA: 0x00078015 File Offset: 0x00076215
		internal void Append(string value)
		{
			this.Append(value, false);
		}

		// Token: 0x060020AF RID: 8367 RVA: 0x0007801F File Offset: 0x0007621F
		internal void Append(string value, bool allowUnicode)
		{
			if (string.IsNullOrEmpty(value))
			{
				return;
			}
			this.Append(value, 0, value.Length, allowUnicode);
		}

		// Token: 0x060020B0 RID: 8368 RVA: 0x0007803C File Offset: 0x0007623C
		internal void Append(string value, int offset, int count, bool allowUnicode)
		{
			if (allowUnicode)
			{
				byte[] bytes = Encoding.UTF8.GetBytes(value.ToCharArray(), offset, count);
				this.Append(bytes);
				return;
			}
			this.Append(value, offset, count);
		}

		// Token: 0x060020B1 RID: 8369 RVA: 0x00078074 File Offset: 0x00076274
		internal void Append(string value, int offset, int count)
		{
			this.EnsureBuffer(count);
			for (int i = 0; i < count; i++)
			{
				char c = value[offset + i];
				if (c > 'ÿ')
				{
					throw new FormatException(SR.GetString("An invalid character was found in the mail header: '{0}'.", new object[]
					{
						c
					}));
				}
				this.buffer[this.offset + i] = (byte)c;
			}
			this.offset += count;
		}

		// Token: 0x1700071F RID: 1823
		// (get) Token: 0x060020B2 RID: 8370 RVA: 0x000780E4 File Offset: 0x000762E4
		internal int Length
		{
			get
			{
				return this.offset;
			}
		}

		// Token: 0x060020B3 RID: 8371 RVA: 0x000780EC File Offset: 0x000762EC
		internal byte[] GetBuffer()
		{
			return this.buffer;
		}

		// Token: 0x060020B4 RID: 8372 RVA: 0x000780F4 File Offset: 0x000762F4
		internal void Reset()
		{
			this.offset = 0;
		}

		// Token: 0x04001B91 RID: 7057
		private byte[] buffer;

		// Token: 0x04001B92 RID: 7058
		private int offset;
	}
}
