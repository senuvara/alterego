﻿using System;
using System.Collections.Specialized;
using System.Net.Mime;
using System.Text;

namespace System.Net.Mail
{
	/// <summary>Represents an e-mail message that can be sent using the <see cref="T:System.Net.Mail.SmtpClient" /> class.</summary>
	// Token: 0x02000423 RID: 1059
	public class MailMessage : IDisposable
	{
		/// <summary>Initializes an empty instance of the <see cref="T:System.Net.Mail.MailMessage" /> class.</summary>
		// Token: 0x0600211C RID: 8476 RVA: 0x0007B224 File Offset: 0x00079424
		public MailMessage()
		{
			this.to = new MailAddressCollection();
			this.alternateViews = new AlternateViewCollection();
			this.attachments = new AttachmentCollection();
			this.bcc = new MailAddressCollection();
			this.cc = new MailAddressCollection();
			this.replyTo = new MailAddressCollection();
			this.headers = new NameValueCollection();
			this.headers.Add("MIME-Version", "1.0");
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Mail.MailMessage" /> class by using the specified <see cref="T:System.Net.Mail.MailAddress" /> class objects. </summary>
		/// <param name="from">A <see cref="T:System.Net.Mail.MailAddress" /> that contains the address of the sender of the e-mail message.</param>
		/// <param name="to">A <see cref="T:System.Net.Mail.MailAddress" /> that contains the address of the recipient of the e-mail message.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="from" /> is <see langword="null" />.-or-
		///         <paramref name="to" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="from" /> or <paramref name="to" /> is malformed.</exception>
		// Token: 0x0600211D RID: 8477 RVA: 0x0007B2A4 File Offset: 0x000794A4
		public MailMessage(MailAddress from, MailAddress to) : this()
		{
			if (from == null || to == null)
			{
				throw new ArgumentNullException();
			}
			this.From = from;
			this.to.Add(to);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Mail.MailMessage" /> class by using the specified <see cref="T:System.String" /> class objects. </summary>
		/// <param name="from">A <see cref="T:System.String" /> that contains the address of the sender of the e-mail message.</param>
		/// <param name="to">A <see cref="T:System.String" /> that contains the addresses of the recipients of the e-mail message.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="from" /> is <see langword="null" />.-or-
		///         <paramref name="to" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="from" /> is <see cref="F:System.String.Empty" /> ("").-or-
		///         <paramref name="to" /> is <see cref="F:System.String.Empty" /> ("").</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="from" /> or <paramref name="to" /> is malformed.</exception>
		// Token: 0x0600211E RID: 8478 RVA: 0x0007B2CC File Offset: 0x000794CC
		public MailMessage(string from, string to) : this()
		{
			if (from == null || from == string.Empty)
			{
				throw new ArgumentNullException("from");
			}
			if (to == null || to == string.Empty)
			{
				throw new ArgumentNullException("to");
			}
			this.from = new MailAddress(from);
			foreach (string text in to.Split(new char[]
			{
				','
			}))
			{
				this.to.Add(new MailAddress(text.Trim()));
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Mail.MailMessage" /> class. </summary>
		/// <param name="from">A <see cref="T:System.String" /> that contains the address of the sender of the e-mail message.</param>
		/// <param name="to">A <see cref="T:System.String" /> that contains the address of the recipient of the e-mail message.</param>
		/// <param name="subject">A <see cref="T:System.String" /> that contains the subject text.</param>
		/// <param name="body">A <see cref="T:System.String" /> that contains the message body.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="from" /> is <see langword="null" />.-or-
		///         <paramref name="to" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="from" /> is <see cref="F:System.String.Empty" /> ("").-or-
		///         <paramref name="to" /> is <see cref="F:System.String.Empty" /> ("").</exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="from" /> or <paramref name="to" /> is malformed.</exception>
		// Token: 0x0600211F RID: 8479 RVA: 0x0007B35C File Offset: 0x0007955C
		public MailMessage(string from, string to, string subject, string body) : this()
		{
			if (from == null || from == string.Empty)
			{
				throw new ArgumentNullException("from");
			}
			if (to == null || to == string.Empty)
			{
				throw new ArgumentNullException("to");
			}
			this.from = new MailAddress(from);
			foreach (string text in to.Split(new char[]
			{
				','
			}))
			{
				this.to.Add(new MailAddress(text.Trim()));
			}
			this.Body = body;
			this.Subject = subject;
		}

		/// <summary>Gets the attachment collection used to store alternate forms of the message body.</summary>
		/// <returns>A writable <see cref="T:System.Net.Mail.AlternateViewCollection" />.</returns>
		// Token: 0x1700072E RID: 1838
		// (get) Token: 0x06002120 RID: 8480 RVA: 0x0007B3FA File Offset: 0x000795FA
		public AlternateViewCollection AlternateViews
		{
			get
			{
				return this.alternateViews;
			}
		}

		/// <summary>Gets the attachment collection used to store data attached to this e-mail message.</summary>
		/// <returns>A writable <see cref="T:System.Net.Mail.AttachmentCollection" />.</returns>
		// Token: 0x1700072F RID: 1839
		// (get) Token: 0x06002121 RID: 8481 RVA: 0x0007B402 File Offset: 0x00079602
		public AttachmentCollection Attachments
		{
			get
			{
				return this.attachments;
			}
		}

		/// <summary>Gets the address collection that contains the blind carbon copy (BCC) recipients for this e-mail message.</summary>
		/// <returns>A writable <see cref="T:System.Net.Mail.MailAddressCollection" /> object.</returns>
		// Token: 0x17000730 RID: 1840
		// (get) Token: 0x06002122 RID: 8482 RVA: 0x0007B40A File Offset: 0x0007960A
		public MailAddressCollection Bcc
		{
			get
			{
				return this.bcc;
			}
		}

		/// <summary>Gets or sets the message body.</summary>
		/// <returns>A <see cref="T:System.String" /> value that contains the body text.</returns>
		// Token: 0x17000731 RID: 1841
		// (get) Token: 0x06002123 RID: 8483 RVA: 0x0007B412 File Offset: 0x00079612
		// (set) Token: 0x06002124 RID: 8484 RVA: 0x0007B41A File Offset: 0x0007961A
		public string Body
		{
			get
			{
				return this.body;
			}
			set
			{
				if (value != null && this.bodyEncoding == null)
				{
					this.bodyEncoding = (this.GuessEncoding(value) ?? Encoding.ASCII);
				}
				this.body = value;
			}
		}

		// Token: 0x17000732 RID: 1842
		// (get) Token: 0x06002125 RID: 8485 RVA: 0x0007B444 File Offset: 0x00079644
		internal ContentType BodyContentType
		{
			get
			{
				return new ContentType(this.isHtml ? "text/html" : "text/plain")
				{
					CharSet = (this.BodyEncoding ?? Encoding.ASCII).HeaderName
				};
			}
		}

		// Token: 0x17000733 RID: 1843
		// (get) Token: 0x06002126 RID: 8486 RVA: 0x0007B479 File Offset: 0x00079679
		internal TransferEncoding ContentTransferEncoding
		{
			get
			{
				return MailMessage.GuessTransferEncoding(this.BodyEncoding);
			}
		}

		/// <summary>Gets or sets the encoding used to encode the message body.</summary>
		/// <returns>An <see cref="T:System.Text.Encoding" /> applied to the contents of the <see cref="P:System.Net.Mail.MailMessage.Body" />.</returns>
		// Token: 0x17000734 RID: 1844
		// (get) Token: 0x06002127 RID: 8487 RVA: 0x0007B486 File Offset: 0x00079686
		// (set) Token: 0x06002128 RID: 8488 RVA: 0x0007B48E File Offset: 0x0007968E
		public Encoding BodyEncoding
		{
			get
			{
				return this.bodyEncoding;
			}
			set
			{
				this.bodyEncoding = value;
			}
		}

		/// <summary>Gets or sets the transfer encoding used to encode the message body.</summary>
		/// <returns>Returns <see cref="T:System.Net.Mime.TransferEncoding" />.A <see cref="T:System.Net.Mime.TransferEncoding" /> applied to the contents of the <see cref="P:System.Net.Mail.MailMessage.Body" />.</returns>
		// Token: 0x17000735 RID: 1845
		// (get) Token: 0x06002129 RID: 8489 RVA: 0x0007B479 File Offset: 0x00079679
		// (set) Token: 0x0600212A RID: 8490 RVA: 0x000068D7 File Offset: 0x00004AD7
		public TransferEncoding BodyTransferEncoding
		{
			get
			{
				return MailMessage.GuessTransferEncoding(this.BodyEncoding);
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the address collection that contains the carbon copy (CC) recipients for this e-mail message.</summary>
		/// <returns>A writable <see cref="T:System.Net.Mail.MailAddressCollection" /> object.</returns>
		// Token: 0x17000736 RID: 1846
		// (get) Token: 0x0600212B RID: 8491 RVA: 0x0007B497 File Offset: 0x00079697
		public MailAddressCollection CC
		{
			get
			{
				return this.cc;
			}
		}

		/// <summary>Gets or sets the delivery notifications for this e-mail message.</summary>
		/// <returns>A <see cref="T:System.Net.Mail.DeliveryNotificationOptions" /> value that contains the delivery notifications for this message.</returns>
		// Token: 0x17000737 RID: 1847
		// (get) Token: 0x0600212C RID: 8492 RVA: 0x0007B49F File Offset: 0x0007969F
		// (set) Token: 0x0600212D RID: 8493 RVA: 0x0007B4A7 File Offset: 0x000796A7
		public DeliveryNotificationOptions DeliveryNotificationOptions
		{
			get
			{
				return this.deliveryNotificationOptions;
			}
			set
			{
				this.deliveryNotificationOptions = value;
			}
		}

		/// <summary>Gets or sets the from address for this e-mail message.</summary>
		/// <returns>A <see cref="T:System.Net.Mail.MailAddress" /> that contains the from address information.</returns>
		// Token: 0x17000738 RID: 1848
		// (get) Token: 0x0600212E RID: 8494 RVA: 0x0007B4B0 File Offset: 0x000796B0
		// (set) Token: 0x0600212F RID: 8495 RVA: 0x0007B4B8 File Offset: 0x000796B8
		public MailAddress From
		{
			get
			{
				return this.from;
			}
			set
			{
				this.from = value;
			}
		}

		/// <summary>Gets the e-mail headers that are transmitted with this e-mail message.</summary>
		/// <returns>A <see cref="T:System.Collections.Specialized.NameValueCollection" /> that contains the e-mail headers.</returns>
		// Token: 0x17000739 RID: 1849
		// (get) Token: 0x06002130 RID: 8496 RVA: 0x0007B4C1 File Offset: 0x000796C1
		public NameValueCollection Headers
		{
			get
			{
				return this.headers;
			}
		}

		/// <summary>Gets or sets a value indicating whether the mail message body is in Html.</summary>
		/// <returns>
		///     <see langword="true" /> if the message body is in Html; else <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700073A RID: 1850
		// (get) Token: 0x06002131 RID: 8497 RVA: 0x0007B4C9 File Offset: 0x000796C9
		// (set) Token: 0x06002132 RID: 8498 RVA: 0x0007B4D1 File Offset: 0x000796D1
		public bool IsBodyHtml
		{
			get
			{
				return this.isHtml;
			}
			set
			{
				this.isHtml = value;
			}
		}

		/// <summary>Gets or sets the priority of this e-mail message.</summary>
		/// <returns>A <see cref="T:System.Net.Mail.MailPriority" /> that contains the priority of this message.</returns>
		// Token: 0x1700073B RID: 1851
		// (get) Token: 0x06002133 RID: 8499 RVA: 0x0007B4DA File Offset: 0x000796DA
		// (set) Token: 0x06002134 RID: 8500 RVA: 0x0007B4E2 File Offset: 0x000796E2
		public MailPriority Priority
		{
			get
			{
				return this.priority;
			}
			set
			{
				this.priority = value;
			}
		}

		/// <summary>Gets or sets the encoding used for the user-defined custom headers for this e-mail message.</summary>
		/// <returns>The encoding used for user-defined custom headers for this e-mail message.</returns>
		// Token: 0x1700073C RID: 1852
		// (get) Token: 0x06002135 RID: 8501 RVA: 0x0007B4EB File Offset: 0x000796EB
		// (set) Token: 0x06002136 RID: 8502 RVA: 0x0007B4F3 File Offset: 0x000796F3
		public Encoding HeadersEncoding
		{
			get
			{
				return this.headersEncoding;
			}
			set
			{
				this.headersEncoding = value;
			}
		}

		/// <summary>Gets or sets the list of addresses to reply to for the mail message.</summary>
		/// <returns>The list of the addresses to reply to for the mail message.</returns>
		// Token: 0x1700073D RID: 1853
		// (get) Token: 0x06002137 RID: 8503 RVA: 0x0007B4FC File Offset: 0x000796FC
		public MailAddressCollection ReplyToList
		{
			get
			{
				return this.replyTo;
			}
		}

		/// <summary>Gets or sets the ReplyTo address for the mail message.</summary>
		/// <returns>A MailAddress that indicates the value of the <see cref="P:System.Net.Mail.MailMessage.ReplyTo" /> field.</returns>
		// Token: 0x1700073E RID: 1854
		// (get) Token: 0x06002138 RID: 8504 RVA: 0x0007B504 File Offset: 0x00079704
		// (set) Token: 0x06002139 RID: 8505 RVA: 0x0007B521 File Offset: 0x00079721
		[Obsolete("Use ReplyToList instead")]
		public MailAddress ReplyTo
		{
			get
			{
				if (this.replyTo.Count == 0)
				{
					return null;
				}
				return this.replyTo[0];
			}
			set
			{
				this.replyTo.Clear();
				this.replyTo.Add(value);
			}
		}

		/// <summary>Gets or sets the sender's address for this e-mail message.</summary>
		/// <returns>A <see cref="T:System.Net.Mail.MailAddress" /> that contains the sender's address information.</returns>
		// Token: 0x1700073F RID: 1855
		// (get) Token: 0x0600213A RID: 8506 RVA: 0x0007B53A File Offset: 0x0007973A
		// (set) Token: 0x0600213B RID: 8507 RVA: 0x0007B542 File Offset: 0x00079742
		public MailAddress Sender
		{
			get
			{
				return this.sender;
			}
			set
			{
				this.sender = value;
			}
		}

		/// <summary>Gets or sets the subject line for this e-mail message.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the subject content.</returns>
		// Token: 0x17000740 RID: 1856
		// (get) Token: 0x0600213C RID: 8508 RVA: 0x0007B54B File Offset: 0x0007974B
		// (set) Token: 0x0600213D RID: 8509 RVA: 0x0007B553 File Offset: 0x00079753
		public string Subject
		{
			get
			{
				return this.subject;
			}
			set
			{
				if (value != null && this.subjectEncoding == null)
				{
					this.subjectEncoding = this.GuessEncoding(value);
				}
				this.subject = value;
			}
		}

		/// <summary>Gets or sets the encoding used for the subject content for this e-mail message.</summary>
		/// <returns>An <see cref="T:System.Text.Encoding" /> that was used to encode the <see cref="P:System.Net.Mail.MailMessage.Subject" /> property.</returns>
		// Token: 0x17000741 RID: 1857
		// (get) Token: 0x0600213E RID: 8510 RVA: 0x0007B574 File Offset: 0x00079774
		// (set) Token: 0x0600213F RID: 8511 RVA: 0x0007B57C File Offset: 0x0007977C
		public Encoding SubjectEncoding
		{
			get
			{
				return this.subjectEncoding;
			}
			set
			{
				this.subjectEncoding = value;
			}
		}

		/// <summary>Gets the address collection that contains the recipients of this e-mail message.</summary>
		/// <returns>A writable <see cref="T:System.Net.Mail.MailAddressCollection" /> object.</returns>
		// Token: 0x17000742 RID: 1858
		// (get) Token: 0x06002140 RID: 8512 RVA: 0x0007B585 File Offset: 0x00079785
		public MailAddressCollection To
		{
			get
			{
				return this.to;
			}
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Net.Mail.MailMessage" />. </summary>
		// Token: 0x06002141 RID: 8513 RVA: 0x0007B58D File Offset: 0x0007978D
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Net.Mail.MailMessage" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06002142 RID: 8514 RVA: 0x0000232D File Offset: 0x0000052D
		protected virtual void Dispose(bool disposing)
		{
		}

		// Token: 0x06002143 RID: 8515 RVA: 0x0007B59C File Offset: 0x0007979C
		private Encoding GuessEncoding(string s)
		{
			for (int i = 0; i < s.Length; i++)
			{
				if (s[i] >= '\u0080')
				{
					return MailMessage.UTF8Unmarked;
				}
			}
			return null;
		}

		// Token: 0x06002144 RID: 8516 RVA: 0x0007B5CF File Offset: 0x000797CF
		internal static TransferEncoding GuessTransferEncoding(Encoding enc)
		{
			if (Encoding.ASCII.Equals(enc))
			{
				return TransferEncoding.SevenBit;
			}
			if (Encoding.UTF8.CodePage == enc.CodePage || Encoding.Unicode.CodePage == enc.CodePage)
			{
				return TransferEncoding.Base64;
			}
			return TransferEncoding.QuotedPrintable;
		}

		// Token: 0x06002145 RID: 8517 RVA: 0x0007B608 File Offset: 0x00079808
		internal static string To2047(byte[] bytes)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (byte b in bytes)
			{
				if (b < 33 || b > 126 || b == 63 || b == 61 || b == 95)
				{
					stringBuilder.Append('=');
					stringBuilder.Append(MailMessage.hex[b >> 4 & 15]);
					stringBuilder.Append(MailMessage.hex[(int)(b & 15)]);
				}
				else
				{
					stringBuilder.Append((char)b);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06002146 RID: 8518 RVA: 0x0007B688 File Offset: 0x00079888
		internal static string EncodeSubjectRFC2047(string s, Encoding enc)
		{
			if (s == null || Encoding.ASCII.Equals(enc))
			{
				return s;
			}
			for (int i = 0; i < s.Length; i++)
			{
				if (s[i] >= '\u0080')
				{
					string text = MailMessage.To2047(enc.GetBytes(s));
					return string.Concat(new string[]
					{
						"=?",
						enc.HeaderName,
						"?Q?",
						text,
						"?="
					});
				}
			}
			return s;
		}

		// Token: 0x17000743 RID: 1859
		// (get) Token: 0x06002147 RID: 8519 RVA: 0x0007B705 File Offset: 0x00079905
		private static Encoding UTF8Unmarked
		{
			get
			{
				if (MailMessage.utf8unmarked == null)
				{
					MailMessage.utf8unmarked = new UTF8Encoding(false);
				}
				return MailMessage.utf8unmarked;
			}
		}

		// Token: 0x06002148 RID: 8520 RVA: 0x0007B71E File Offset: 0x0007991E
		// Note: this type is marked as 'beforefieldinit'.
		static MailMessage()
		{
		}

		// Token: 0x04001BD3 RID: 7123
		private AlternateViewCollection alternateViews;

		// Token: 0x04001BD4 RID: 7124
		private AttachmentCollection attachments;

		// Token: 0x04001BD5 RID: 7125
		private MailAddressCollection bcc;

		// Token: 0x04001BD6 RID: 7126
		private MailAddressCollection replyTo;

		// Token: 0x04001BD7 RID: 7127
		private string body;

		// Token: 0x04001BD8 RID: 7128
		private MailPriority priority;

		// Token: 0x04001BD9 RID: 7129
		private MailAddress sender;

		// Token: 0x04001BDA RID: 7130
		private DeliveryNotificationOptions deliveryNotificationOptions;

		// Token: 0x04001BDB RID: 7131
		private MailAddressCollection cc;

		// Token: 0x04001BDC RID: 7132
		private MailAddress from;

		// Token: 0x04001BDD RID: 7133
		private NameValueCollection headers;

		// Token: 0x04001BDE RID: 7134
		private MailAddressCollection to;

		// Token: 0x04001BDF RID: 7135
		private string subject;

		// Token: 0x04001BE0 RID: 7136
		private Encoding subjectEncoding;

		// Token: 0x04001BE1 RID: 7137
		private Encoding bodyEncoding;

		// Token: 0x04001BE2 RID: 7138
		private Encoding headersEncoding = Encoding.UTF8;

		// Token: 0x04001BE3 RID: 7139
		private bool isHtml;

		// Token: 0x04001BE4 RID: 7140
		private static char[] hex = new char[]
		{
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'A',
			'B',
			'C',
			'D',
			'E',
			'F'
		};

		// Token: 0x04001BE5 RID: 7141
		private static Encoding utf8unmarked;
	}
}
