﻿using System;

namespace System.Net
{
	// Token: 0x0200034A RID: 842
	// (Invoke) Token: 0x06001824 RID: 6180
	internal delegate string[] HeaderParser(string value);
}
