﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Net
{
	// Token: 0x0200040E RID: 1038
	internal class WebConnectionGroup
	{
		// Token: 0x06002057 RID: 8279 RVA: 0x00076260 File Offset: 0x00074460
		public WebConnectionGroup(ServicePoint sPoint, string name)
		{
			this.sPoint = sPoint;
			this.name = name;
			this.connections = new LinkedList<WebConnectionGroup.ConnectionState>();
			this.queue = new Queue();
		}

		// Token: 0x1400003C RID: 60
		// (add) Token: 0x06002058 RID: 8280 RVA: 0x0007628C File Offset: 0x0007448C
		// (remove) Token: 0x06002059 RID: 8281 RVA: 0x000762C4 File Offset: 0x000744C4
		public event EventHandler ConnectionClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler eventHandler = this.ConnectionClosed;
				EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler value2 = (EventHandler)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler>(ref this.ConnectionClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler eventHandler = this.ConnectionClosed;
				EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler value2 = (EventHandler)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler>(ref this.ConnectionClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x0600205A RID: 8282 RVA: 0x000762F9 File Offset: 0x000744F9
		private void OnConnectionClosed()
		{
			if (this.ConnectionClosed != null)
			{
				this.ConnectionClosed(this, null);
			}
		}

		// Token: 0x0600205B RID: 8283 RVA: 0x00076310 File Offset: 0x00074510
		public void Close()
		{
			List<WebConnection> list = null;
			ServicePoint obj = this.sPoint;
			lock (obj)
			{
				this.closing = true;
				LinkedListNode<WebConnectionGroup.ConnectionState> linkedListNode = this.connections.First;
				while (linkedListNode != null)
				{
					WebConnection connection = linkedListNode.Value.Connection;
					LinkedListNode<WebConnectionGroup.ConnectionState> node = linkedListNode;
					linkedListNode = linkedListNode.Next;
					if (list == null)
					{
						list = new List<WebConnection>();
					}
					list.Add(connection);
					this.connections.Remove(node);
				}
			}
			if (list != null)
			{
				foreach (WebConnection webConnection in list)
				{
					webConnection.Close(false);
					this.OnConnectionClosed();
				}
			}
		}

		// Token: 0x0600205C RID: 8284 RVA: 0x000763E0 File Offset: 0x000745E0
		public WebConnection GetConnection(HttpWebRequest request, out bool created)
		{
			ServicePoint obj = this.sPoint;
			WebConnection result;
			lock (obj)
			{
				result = this.CreateOrReuseConnection(request, out created);
			}
			return result;
		}

		// Token: 0x0600205D RID: 8285 RVA: 0x00076424 File Offset: 0x00074624
		private static void PrepareSharingNtlm(WebConnection cnc, HttpWebRequest request)
		{
			if (!cnc.NtlmAuthenticated)
			{
				return;
			}
			bool flag = false;
			NetworkCredential ntlmCredential = cnc.NtlmCredential;
			ICredentials credentials = (request.Proxy == null || request.Proxy.IsBypassed(request.RequestUri)) ? request.Credentials : request.Proxy.Credentials;
			NetworkCredential networkCredential = (credentials != null) ? credentials.GetCredential(request.RequestUri, "NTLM") : null;
			if (ntlmCredential == null || networkCredential == null || ntlmCredential.Domain != networkCredential.Domain || ntlmCredential.UserName != networkCredential.UserName || ntlmCredential.Password != networkCredential.Password)
			{
				flag = true;
			}
			if (!flag)
			{
				bool unsafeAuthenticatedConnectionSharing = request.UnsafeAuthenticatedConnectionSharing;
				bool unsafeAuthenticatedConnectionSharing2 = cnc.UnsafeAuthenticatedConnectionSharing;
				flag = (!unsafeAuthenticatedConnectionSharing || unsafeAuthenticatedConnectionSharing != unsafeAuthenticatedConnectionSharing2);
			}
			if (flag)
			{
				cnc.Close(false);
				cnc.ResetNtlm();
			}
		}

		// Token: 0x0600205E RID: 8286 RVA: 0x00076508 File Offset: 0x00074708
		private WebConnectionGroup.ConnectionState FindIdleConnection()
		{
			foreach (WebConnectionGroup.ConnectionState connectionState in this.connections)
			{
				if (!connectionState.Busy)
				{
					this.connections.Remove(connectionState);
					this.connections.AddFirst(connectionState);
					return connectionState;
				}
			}
			return null;
		}

		// Token: 0x0600205F RID: 8287 RVA: 0x00076580 File Offset: 0x00074780
		private WebConnection CreateOrReuseConnection(HttpWebRequest request, out bool created)
		{
			WebConnectionGroup.ConnectionState connectionState = this.FindIdleConnection();
			if (connectionState != null)
			{
				created = false;
				WebConnectionGroup.PrepareSharingNtlm(connectionState.Connection, request);
				return connectionState.Connection;
			}
			if (this.sPoint.ConnectionLimit > this.connections.Count || this.connections.Count == 0)
			{
				created = true;
				connectionState = new WebConnectionGroup.ConnectionState(this);
				this.connections.AddFirst(connectionState);
				return connectionState.Connection;
			}
			created = false;
			connectionState = this.connections.Last.Value;
			this.connections.Remove(connectionState);
			this.connections.AddFirst(connectionState);
			return connectionState.Connection;
		}

		// Token: 0x17000705 RID: 1797
		// (get) Token: 0x06002060 RID: 8288 RVA: 0x00076624 File Offset: 0x00074824
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000706 RID: 1798
		// (get) Token: 0x06002061 RID: 8289 RVA: 0x0007662C File Offset: 0x0007482C
		internal Queue Queue
		{
			get
			{
				return this.queue;
			}
		}

		// Token: 0x06002062 RID: 8290 RVA: 0x00076634 File Offset: 0x00074834
		internal bool TryRecycle(TimeSpan maxIdleTime, ref DateTime idleSince)
		{
			DateTime utcNow = DateTime.UtcNow;
			bool result;
			for (;;)
			{
				List<WebConnection> list = null;
				ServicePoint obj = this.sPoint;
				lock (obj)
				{
					if (this.closing)
					{
						idleSince = DateTime.MinValue;
						return true;
					}
					int num = 0;
					LinkedListNode<WebConnectionGroup.ConnectionState> linkedListNode = this.connections.First;
					while (linkedListNode != null)
					{
						WebConnectionGroup.ConnectionState value = linkedListNode.Value;
						LinkedListNode<WebConnectionGroup.ConnectionState> node = linkedListNode;
						linkedListNode = linkedListNode.Next;
						num++;
						if (!value.Busy)
						{
							if (num <= this.sPoint.ConnectionLimit && utcNow - value.IdleSince < maxIdleTime)
							{
								if (value.IdleSince > idleSince)
								{
									idleSince = value.IdleSince;
								}
							}
							else
							{
								if (list == null)
								{
									list = new List<WebConnection>();
								}
								list.Add(value.Connection);
								this.connections.Remove(node);
							}
						}
					}
					result = (this.connections.Count == 0);
				}
				if (list == null)
				{
					break;
				}
				using (List<WebConnection>.Enumerator enumerator = list.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						WebConnection webConnection = enumerator.Current;
						webConnection.Close(false);
					}
					continue;
				}
				bool result2;
				return result2;
			}
			return result;
		}

		// Token: 0x04001B5F RID: 7007
		private ServicePoint sPoint;

		// Token: 0x04001B60 RID: 7008
		private string name;

		// Token: 0x04001B61 RID: 7009
		private LinkedList<WebConnectionGroup.ConnectionState> connections;

		// Token: 0x04001B62 RID: 7010
		private Queue queue;

		// Token: 0x04001B63 RID: 7011
		private bool closing;

		// Token: 0x04001B64 RID: 7012
		[CompilerGenerated]
		private EventHandler ConnectionClosed;

		// Token: 0x0200040F RID: 1039
		private class ConnectionState : IWebConnectionState
		{
			// Token: 0x17000707 RID: 1799
			// (get) Token: 0x06002063 RID: 8291 RVA: 0x00076794 File Offset: 0x00074994
			// (set) Token: 0x06002064 RID: 8292 RVA: 0x0007679C File Offset: 0x0007499C
			public WebConnection Connection
			{
				[CompilerGenerated]
				get
				{
					return this.<Connection>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Connection>k__BackingField = value;
				}
			}

			// Token: 0x17000708 RID: 1800
			// (get) Token: 0x06002065 RID: 8293 RVA: 0x000767A5 File Offset: 0x000749A5
			// (set) Token: 0x06002066 RID: 8294 RVA: 0x000767AD File Offset: 0x000749AD
			public WebConnectionGroup Group
			{
				[CompilerGenerated]
				get
				{
					return this.<Group>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Group>k__BackingField = value;
				}
			}

			// Token: 0x17000709 RID: 1801
			// (get) Token: 0x06002067 RID: 8295 RVA: 0x000767B6 File Offset: 0x000749B6
			public ServicePoint ServicePoint
			{
				get
				{
					return this.Group.sPoint;
				}
			}

			// Token: 0x1700070A RID: 1802
			// (get) Token: 0x06002068 RID: 8296 RVA: 0x000767C3 File Offset: 0x000749C3
			public bool Busy
			{
				get
				{
					return this.busy;
				}
			}

			// Token: 0x1700070B RID: 1803
			// (get) Token: 0x06002069 RID: 8297 RVA: 0x000767CB File Offset: 0x000749CB
			public DateTime IdleSince
			{
				get
				{
					return this.idleSince;
				}
			}

			// Token: 0x0600206A RID: 8298 RVA: 0x000767D4 File Offset: 0x000749D4
			public bool TrySetBusy()
			{
				ServicePoint servicePoint = this.ServicePoint;
				bool result;
				lock (servicePoint)
				{
					if (this.busy)
					{
						result = false;
					}
					else
					{
						this.busy = true;
						this.idleSince = DateTime.UtcNow + TimeSpan.FromDays(3650.0);
						result = true;
					}
				}
				return result;
			}

			// Token: 0x0600206B RID: 8299 RVA: 0x00076844 File Offset: 0x00074A44
			public void SetIdle()
			{
				ServicePoint servicePoint = this.ServicePoint;
				lock (servicePoint)
				{
					this.busy = false;
					this.idleSince = DateTime.UtcNow;
				}
			}

			// Token: 0x0600206C RID: 8300 RVA: 0x00076890 File Offset: 0x00074A90
			public ConnectionState(WebConnectionGroup group)
			{
				this.Group = group;
				this.idleSince = DateTime.UtcNow;
				this.Connection = new WebConnection(this, group.sPoint);
			}

			// Token: 0x04001B65 RID: 7013
			[CompilerGenerated]
			private WebConnection <Connection>k__BackingField;

			// Token: 0x04001B66 RID: 7014
			[CompilerGenerated]
			private WebConnectionGroup <Group>k__BackingField;

			// Token: 0x04001B67 RID: 7015
			private bool busy;

			// Token: 0x04001B68 RID: 7016
			private DateTime idleSince;
		}
	}
}
