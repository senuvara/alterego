﻿using System;

namespace System.Net
{
	/// <summary>Defines the HTTP version numbers that are supported by the <see cref="T:System.Net.HttpWebRequest" /> and <see cref="T:System.Net.HttpWebResponse" /> classes.</summary>
	// Token: 0x020002EF RID: 751
	public class HttpVersion
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.HttpVersion" /> class.</summary>
		// Token: 0x0600163A RID: 5690 RVA: 0x0000232F File Offset: 0x0000052F
		public HttpVersion()
		{
		}

		// Token: 0x0600163B RID: 5691 RVA: 0x00050356 File Offset: 0x0004E556
		// Note: this type is marked as 'beforefieldinit'.
		static HttpVersion()
		{
		}

		/// <summary>Defines a <see cref="T:System.Version" /> instance for HTTP 1.0.</summary>
		// Token: 0x0400150A RID: 5386
		public static readonly Version Version10 = new Version(1, 0);

		/// <summary>Defines a <see cref="T:System.Version" /> instance for HTTP 1.1.</summary>
		// Token: 0x0400150B RID: 5387
		public static readonly Version Version11 = new Version(1, 1);
	}
}
