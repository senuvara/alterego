﻿using System;
using System.Threading;

namespace System.Net
{
	// Token: 0x020002FD RID: 765
	internal struct InterlockedGate
	{
		// Token: 0x06001691 RID: 5777 RVA: 0x000513E0 File Offset: 0x0004F5E0
		internal void Reset()
		{
			this.m_State = 0;
		}

		// Token: 0x06001692 RID: 5778 RVA: 0x000513EC File Offset: 0x0004F5EC
		internal bool Trigger(bool exclusive)
		{
			int num = Interlocked.CompareExchange(ref this.m_State, 2, 0);
			if (exclusive && (num == 1 || num == 2))
			{
				throw new InternalException();
			}
			return num == 0;
		}

		// Token: 0x06001693 RID: 5779 RVA: 0x0005141C File Offset: 0x0004F61C
		internal bool StartTriggering(bool exclusive)
		{
			int num = Interlocked.CompareExchange(ref this.m_State, 1, 0);
			if (exclusive && (num == 1 || num == 2))
			{
				throw new InternalException();
			}
			return num == 0;
		}

		// Token: 0x06001694 RID: 5780 RVA: 0x0005144C File Offset: 0x0004F64C
		internal void FinishTriggering()
		{
			if (Interlocked.CompareExchange(ref this.m_State, 2, 1) != 1)
			{
				throw new InternalException();
			}
		}

		// Token: 0x06001695 RID: 5781 RVA: 0x00051464 File Offset: 0x0004F664
		internal bool StartSignaling(bool exclusive)
		{
			int num = Interlocked.CompareExchange(ref this.m_State, 3, 2);
			if (exclusive && (num == 3 || num == 4))
			{
				throw new InternalException();
			}
			return num == 2;
		}

		// Token: 0x06001696 RID: 5782 RVA: 0x00051494 File Offset: 0x0004F694
		internal void FinishSignaling()
		{
			if (Interlocked.CompareExchange(ref this.m_State, 4, 3) != 3)
			{
				throw new InternalException();
			}
		}

		// Token: 0x06001697 RID: 5783 RVA: 0x000514AC File Offset: 0x0004F6AC
		internal bool Complete()
		{
			return Interlocked.CompareExchange(ref this.m_State, 5, 4) == 4;
		}

		// Token: 0x04001532 RID: 5426
		private int m_State;

		// Token: 0x04001533 RID: 5427
		internal const int Open = 0;

		// Token: 0x04001534 RID: 5428
		internal const int Triggering = 1;

		// Token: 0x04001535 RID: 5429
		internal const int Triggered = 2;

		// Token: 0x04001536 RID: 5430
		internal const int Signaling = 3;

		// Token: 0x04001537 RID: 5431
		internal const int Signaled = 4;

		// Token: 0x04001538 RID: 5432
		internal const int Completed = 5;
	}
}
