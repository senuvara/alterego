﻿using System;

namespace System.Net
{
	// Token: 0x020003EA RID: 1002
	internal class HttpRequestCreator : IWebRequestCreate
	{
		// Token: 0x06001E4C RID: 7756 RVA: 0x0000232F File Offset: 0x0000052F
		internal HttpRequestCreator()
		{
		}

		// Token: 0x06001E4D RID: 7757 RVA: 0x0006D942 File Offset: 0x0006BB42
		public WebRequest Create(Uri uri)
		{
			return new HttpWebRequest(uri);
		}
	}
}
