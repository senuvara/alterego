﻿using System;

namespace System.Net
{
	/// <summary>The timeout manager to use for an <see cref="T:System.Net.HttpListener" /> object.</summary>
	// Token: 0x020003E9 RID: 1001
	public class HttpListenerTimeoutManager
	{
		/// <summary>Gets or sets the time, in seconds, allowed for the request entity body to arrive.</summary>
		/// <returns>Returns <see cref="T:System.TimeSpan" />.The time, in seconds, allowed for the request entity body to arrive.</returns>
		// Token: 0x1700064E RID: 1614
		// (get) Token: 0x06001E3F RID: 7743 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001E40 RID: 7744 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public TimeSpan EntityBody
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the time, in seconds, allowed for the <see cref="T:System.Net.HttpListener" />  to drain the entity body on a Keep-Alive connection.</summary>
		/// <returns>Returns <see cref="T:System.TimeSpan" />.The time, in seconds, allowed for the <see cref="T:System.Net.HttpListener" />  to drain the entity body on a Keep-Alive connection.</returns>
		// Token: 0x1700064F RID: 1615
		// (get) Token: 0x06001E41 RID: 7745 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001E42 RID: 7746 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public TimeSpan DrainEntityBody
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the time, in seconds, allowed for the request to remain in the request queue before the <see cref="T:System.Net.HttpListener" /> picks it up.</summary>
		/// <returns>Returns <see cref="T:System.TimeSpan" />.The time, in seconds, allowed for the request to remain in the request queue before the <see cref="T:System.Net.HttpListener" /> picks it up.</returns>
		// Token: 0x17000650 RID: 1616
		// (get) Token: 0x06001E43 RID: 7747 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001E44 RID: 7748 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public TimeSpan RequestQueue
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the time, in seconds, allowed for an idle connection.</summary>
		/// <returns>Returns <see cref="T:System.TimeSpan" />.The time, in seconds, allowed for an idle connection.</returns>
		// Token: 0x17000651 RID: 1617
		// (get) Token: 0x06001E45 RID: 7749 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001E46 RID: 7750 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public TimeSpan IdleConnection
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the time, in seconds, allowed for the <see cref="T:System.Net.HttpListener" /> to parse the request header.</summary>
		/// <returns>Returns <see cref="T:System.TimeSpan" />.The time, in seconds, allowed for the <see cref="T:System.Net.HttpListener" /> to parse the request header.</returns>
		// Token: 0x17000652 RID: 1618
		// (get) Token: 0x06001E47 RID: 7751 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001E48 RID: 7752 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public TimeSpan HeaderWait
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the minimum send rate, in bytes-per-second, for the response. </summary>
		/// <returns>Returns <see cref="T:System.Int64" />.The minimum send rate, in bytes-per-second, for the response.</returns>
		// Token: 0x17000653 RID: 1619
		// (get) Token: 0x06001E49 RID: 7753 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001E4A RID: 7754 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		public long MinSendBytesPerSecond
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x06001E4B RID: 7755 RVA: 0x0000232F File Offset: 0x0000052F
		public HttpListenerTimeoutManager()
		{
		}
	}
}
