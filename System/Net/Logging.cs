﻿using System;
using System.Diagnostics;

namespace System.Net
{
	// Token: 0x020003BD RID: 957
	internal static class Logging
	{
		// Token: 0x170005B5 RID: 1461
		// (get) Token: 0x06001C11 RID: 7185 RVA: 0x00008B3F File Offset: 0x00006D3F
		internal static TraceSource Web
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170005B6 RID: 1462
		// (get) Token: 0x06001C12 RID: 7186 RVA: 0x00008B3F File Offset: 0x00006D3F
		internal static TraceSource HttpListener
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170005B7 RID: 1463
		// (get) Token: 0x06001C13 RID: 7187 RVA: 0x00008B3F File Offset: 0x00006D3F
		internal static TraceSource Sockets
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06001C14 RID: 7188 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void Enter(TraceSource traceSource, object obj, string method, object paramObject)
		{
		}

		// Token: 0x06001C15 RID: 7189 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void Enter(TraceSource traceSource, string msg)
		{
		}

		// Token: 0x06001C16 RID: 7190 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void Enter(TraceSource traceSource, string msg, string parameters)
		{
		}

		// Token: 0x06001C17 RID: 7191 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void Exception(TraceSource traceSource, object obj, string method, Exception e)
		{
		}

		// Token: 0x06001C18 RID: 7192 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void Exit(TraceSource traceSource, object obj, string method, object retObject)
		{
		}

		// Token: 0x06001C19 RID: 7193 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void Exit(TraceSource traceSource, string msg)
		{
		}

		// Token: 0x06001C1A RID: 7194 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void Exit(TraceSource traceSource, string msg, string parameters)
		{
		}

		// Token: 0x06001C1B RID: 7195 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void PrintInfo(TraceSource traceSource, object obj, string method, string msg)
		{
		}

		// Token: 0x06001C1C RID: 7196 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void PrintInfo(TraceSource traceSource, object obj, string msg)
		{
		}

		// Token: 0x06001C1D RID: 7197 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void PrintInfo(TraceSource traceSource, string msg)
		{
		}

		// Token: 0x06001C1E RID: 7198 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void PrintWarning(TraceSource traceSource, object obj, string method, string msg)
		{
		}

		// Token: 0x06001C1F RID: 7199 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void PrintWarning(TraceSource traceSource, string msg)
		{
		}

		// Token: 0x06001C20 RID: 7200 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRACE")]
		internal static void PrintError(TraceSource traceSource, string msg)
		{
		}

		// Token: 0x06001C21 RID: 7201 RVA: 0x0000232D File Offset: 0x0000052D
		// Note: this type is marked as 'beforefieldinit'.
		static Logging()
		{
		}

		// Token: 0x04001965 RID: 6501
		internal static readonly bool On;
	}
}
