﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Net
{
	// Token: 0x02000403 RID: 1027
	internal class SimpleAsyncResult : IAsyncResult
	{
		// Token: 0x06001FE3 RID: 8163 RVA: 0x000732F2 File Offset: 0x000714F2
		private SimpleAsyncResult(SimpleAsyncCallback cb)
		{
			this.cb = cb;
		}

		// Token: 0x06001FE4 RID: 8164 RVA: 0x0007330C File Offset: 0x0007150C
		protected SimpleAsyncResult(AsyncCallback cb, object state)
		{
			SimpleAsyncResult <>4__this = this;
			this.state = state;
			this.cb = delegate(SimpleAsyncResult result)
			{
				if (cb != null)
				{
					cb(<>4__this);
				}
			};
		}

		// Token: 0x06001FE5 RID: 8165 RVA: 0x00073358 File Offset: 0x00071558
		public static void Run(Func<SimpleAsyncResult, bool> func, SimpleAsyncCallback callback)
		{
			SimpleAsyncResult simpleAsyncResult = new SimpleAsyncResult(callback);
			try
			{
				if (!func(simpleAsyncResult))
				{
					simpleAsyncResult.SetCompleted(true);
				}
			}
			catch (Exception e)
			{
				simpleAsyncResult.SetCompleted(true, e);
			}
		}

		// Token: 0x06001FE6 RID: 8166 RVA: 0x0007339C File Offset: 0x0007159C
		public static void RunWithLock(object locker, Func<SimpleAsyncResult, bool> func, SimpleAsyncCallback callback)
		{
			SimpleAsyncResult.Run(delegate(SimpleAsyncResult inner)
			{
				bool flag = func(inner);
				if (flag)
				{
					Monitor.Exit(locker);
				}
				return flag;
			}, delegate(SimpleAsyncResult inner)
			{
				if (inner.GotException)
				{
					if (inner.synch)
					{
						Monitor.Exit(locker);
					}
					callback(inner);
					return;
				}
				try
				{
					if (!inner.synch)
					{
						Monitor.Enter(locker);
					}
					callback(inner);
				}
				finally
				{
					Monitor.Exit(locker);
				}
			});
		}

		// Token: 0x06001FE7 RID: 8167 RVA: 0x000733E4 File Offset: 0x000715E4
		protected void Reset_internal()
		{
			this.callbackDone = false;
			this.exc = null;
			object obj = this.locker;
			lock (obj)
			{
				this.isCompleted = false;
				if (this.handle != null)
				{
					this.handle.Reset();
				}
			}
		}

		// Token: 0x06001FE8 RID: 8168 RVA: 0x00073448 File Offset: 0x00071648
		internal void SetCompleted(bool synch, Exception e)
		{
			this.SetCompleted_internal(synch, e);
			this.DoCallback_private();
		}

		// Token: 0x06001FE9 RID: 8169 RVA: 0x00073458 File Offset: 0x00071658
		internal void SetCompleted(bool synch)
		{
			this.SetCompleted_internal(synch);
			this.DoCallback_private();
		}

		// Token: 0x06001FEA RID: 8170 RVA: 0x00073468 File Offset: 0x00071668
		private void SetCompleted_internal(bool synch, Exception e)
		{
			this.synch = synch;
			this.exc = e;
			object obj = this.locker;
			lock (obj)
			{
				this.isCompleted = true;
				if (this.handle != null)
				{
					this.handle.Set();
				}
			}
		}

		// Token: 0x06001FEB RID: 8171 RVA: 0x000734CC File Offset: 0x000716CC
		protected void SetCompleted_internal(bool synch)
		{
			this.SetCompleted_internal(synch, null);
		}

		// Token: 0x06001FEC RID: 8172 RVA: 0x000734D6 File Offset: 0x000716D6
		private void DoCallback_private()
		{
			if (this.callbackDone)
			{
				return;
			}
			this.callbackDone = true;
			if (this.cb == null)
			{
				return;
			}
			this.cb(this);
		}

		// Token: 0x06001FED RID: 8173 RVA: 0x000734FD File Offset: 0x000716FD
		protected void DoCallback_internal()
		{
			if (!this.callbackDone && this.cb != null)
			{
				this.callbackDone = true;
				this.cb(this);
			}
		}

		// Token: 0x06001FEE RID: 8174 RVA: 0x00073522 File Offset: 0x00071722
		internal void WaitUntilComplete()
		{
			if (this.IsCompleted)
			{
				return;
			}
			this.AsyncWaitHandle.WaitOne();
		}

		// Token: 0x06001FEF RID: 8175 RVA: 0x00073539 File Offset: 0x00071739
		internal bool WaitUntilComplete(int timeout, bool exitContext)
		{
			return this.IsCompleted || this.AsyncWaitHandle.WaitOne(timeout, exitContext);
		}

		// Token: 0x170006E9 RID: 1769
		// (get) Token: 0x06001FF0 RID: 8176 RVA: 0x00073552 File Offset: 0x00071752
		public object AsyncState
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x170006EA RID: 1770
		// (get) Token: 0x06001FF1 RID: 8177 RVA: 0x0007355C File Offset: 0x0007175C
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				object obj = this.locker;
				lock (obj)
				{
					if (this.handle == null)
					{
						this.handle = new ManualResetEvent(this.isCompleted);
					}
				}
				return this.handle;
			}
		}

		// Token: 0x170006EB RID: 1771
		// (get) Token: 0x06001FF2 RID: 8178 RVA: 0x000735B8 File Offset: 0x000717B8
		public bool CompletedSynchronously
		{
			get
			{
				if (this.user_read_synch != null)
				{
					return this.user_read_synch.Value;
				}
				this.user_read_synch = new bool?(this.synch);
				return this.user_read_synch.Value;
			}
		}

		// Token: 0x170006EC RID: 1772
		// (get) Token: 0x06001FF3 RID: 8179 RVA: 0x000735EF File Offset: 0x000717EF
		internal bool CompletedSynchronouslyPeek
		{
			get
			{
				return this.synch;
			}
		}

		// Token: 0x170006ED RID: 1773
		// (get) Token: 0x06001FF4 RID: 8180 RVA: 0x000735F8 File Offset: 0x000717F8
		public bool IsCompleted
		{
			get
			{
				object obj = this.locker;
				bool result;
				lock (obj)
				{
					result = this.isCompleted;
				}
				return result;
			}
		}

		// Token: 0x170006EE RID: 1774
		// (get) Token: 0x06001FF5 RID: 8181 RVA: 0x0007363C File Offset: 0x0007183C
		internal bool GotException
		{
			get
			{
				return this.exc != null;
			}
		}

		// Token: 0x170006EF RID: 1775
		// (get) Token: 0x06001FF6 RID: 8182 RVA: 0x00073647 File Offset: 0x00071847
		internal Exception Exception
		{
			get
			{
				return this.exc;
			}
		}

		// Token: 0x04001B13 RID: 6931
		private ManualResetEvent handle;

		// Token: 0x04001B14 RID: 6932
		private bool synch;

		// Token: 0x04001B15 RID: 6933
		private bool isCompleted;

		// Token: 0x04001B16 RID: 6934
		private readonly SimpleAsyncCallback cb;

		// Token: 0x04001B17 RID: 6935
		private object state;

		// Token: 0x04001B18 RID: 6936
		private bool callbackDone;

		// Token: 0x04001B19 RID: 6937
		private Exception exc;

		// Token: 0x04001B1A RID: 6938
		private object locker = new object();

		// Token: 0x04001B1B RID: 6939
		private bool? user_read_synch;

		// Token: 0x02000404 RID: 1028
		[CompilerGenerated]
		private sealed class <>c__DisplayClass9_0
		{
			// Token: 0x06001FF7 RID: 8183 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass9_0()
			{
			}

			// Token: 0x06001FF8 RID: 8184 RVA: 0x0007364F File Offset: 0x0007184F
			internal void <.ctor>b__0(SimpleAsyncResult result)
			{
				if (this.cb != null)
				{
					this.cb(this.<>4__this);
				}
			}

			// Token: 0x04001B1C RID: 6940
			public AsyncCallback cb;

			// Token: 0x04001B1D RID: 6941
			public SimpleAsyncResult <>4__this;
		}

		// Token: 0x02000405 RID: 1029
		[CompilerGenerated]
		private sealed class <>c__DisplayClass11_0
		{
			// Token: 0x06001FF9 RID: 8185 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c__DisplayClass11_0()
			{
			}

			// Token: 0x06001FFA RID: 8186 RVA: 0x0007366A File Offset: 0x0007186A
			internal bool <RunWithLock>b__0(SimpleAsyncResult inner)
			{
				bool flag = this.func(inner);
				if (flag)
				{
					Monitor.Exit(this.locker);
				}
				return flag;
			}

			// Token: 0x06001FFB RID: 8187 RVA: 0x00073688 File Offset: 0x00071888
			internal void <RunWithLock>b__1(SimpleAsyncResult inner)
			{
				if (inner.GotException)
				{
					if (inner.synch)
					{
						Monitor.Exit(this.locker);
					}
					this.callback(inner);
					return;
				}
				try
				{
					if (!inner.synch)
					{
						Monitor.Enter(this.locker);
					}
					this.callback(inner);
				}
				finally
				{
					Monitor.Exit(this.locker);
				}
			}

			// Token: 0x04001B1E RID: 6942
			public Func<SimpleAsyncResult, bool> func;

			// Token: 0x04001B1F RID: 6943
			public object locker;

			// Token: 0x04001B20 RID: 6944
			public SimpleAsyncCallback callback;
		}
	}
}
