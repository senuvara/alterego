﻿using System;

namespace System.Net
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Net.WebClient.UploadStringCompleted" /> event of a <see cref="T:System.Net.WebClient" />.</summary>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">A <see cref="T:System.Net.UploadStringCompletedEventArgs" /> containing event data.</param>
	// Token: 0x020003A7 RID: 935
	// (Invoke) Token: 0x06001B9E RID: 7070
	public delegate void UploadStringCompletedEventHandler(object sender, UploadStringCompletedEventArgs e);
}
