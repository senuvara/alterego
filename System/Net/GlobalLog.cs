﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.ConstrainedExecution;

namespace System.Net
{
	// Token: 0x02000353 RID: 851
	internal static class GlobalLog
	{
		// Token: 0x06001859 RID: 6233 RVA: 0x000582B9 File Offset: 0x000564B9
		[ReliabilityContract(Consistency.MayCorruptAppDomain, Cer.None)]
		private static BaseLoggingObject LoggingInitialize()
		{
			return new BaseLoggingObject();
		}

		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x0600185A RID: 6234 RVA: 0x00005AFA File Offset: 0x00003CFA
		internal static ThreadKinds CurrentThreadKind
		{
			get
			{
				return ThreadKinds.Unknown;
			}
		}

		// Token: 0x0600185B RID: 6235 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("DEBUG")]
		[ReliabilityContract(Consistency.MayCorruptAppDomain, Cer.None)]
		internal static void SetThreadSource(ThreadKinds source)
		{
		}

		// Token: 0x0600185C RID: 6236 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("DEBUG")]
		[ReliabilityContract(Consistency.MayCorruptAppDomain, Cer.None)]
		internal static void ThreadContract(ThreadKinds kind, string errorMsg)
		{
		}

		// Token: 0x0600185D RID: 6237 RVA: 0x000582C0 File Offset: 0x000564C0
		[ReliabilityContract(Consistency.MayCorruptAppDomain, Cer.None)]
		[Conditional("DEBUG")]
		internal static void ThreadContract(ThreadKinds kind, ThreadKinds allowedSources, string errorMsg)
		{
			if ((kind & ThreadKinds.SourceMask) != ThreadKinds.Unknown || (allowedSources & ThreadKinds.SourceMask) != allowedSources)
			{
				throw new InternalException();
			}
			ThreadKinds currentThreadKind = GlobalLog.CurrentThreadKind;
		}

		// Token: 0x0600185E RID: 6238 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void AddToArray(string msg)
		{
		}

		// Token: 0x0600185F RID: 6239 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Ignore(object msg)
		{
		}

		// Token: 0x06001860 RID: 6240 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		[ReliabilityContract(Consistency.MayCorruptAppDomain, Cer.None)]
		public static void Print(string msg)
		{
		}

		// Token: 0x06001861 RID: 6241 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void PrintHex(string msg, object value)
		{
		}

		// Token: 0x06001862 RID: 6242 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Enter(string func)
		{
		}

		// Token: 0x06001863 RID: 6243 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Enter(string func, string parms)
		{
		}

		// Token: 0x06001864 RID: 6244 RVA: 0x000582E4 File Offset: 0x000564E4
		[Conditional("DEBUG")]
		[Conditional("_FORCE_ASSERTS")]
		[ReliabilityContract(Consistency.MayCorruptAppDomain, Cer.None)]
		public static void Assert(bool condition, string messageFormat, params object[] data)
		{
			if (!condition)
			{
				string text = string.Format(CultureInfo.InvariantCulture, messageFormat, data);
				int num = text.IndexOf('|');
				if (num != -1)
				{
					int length = text.Length;
				}
			}
		}

		// Token: 0x06001865 RID: 6245 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("DEBUG")]
		[Conditional("_FORCE_ASSERTS")]
		[ReliabilityContract(Consistency.MayCorruptAppDomain, Cer.None)]
		public static void Assert(string message)
		{
		}

		// Token: 0x06001866 RID: 6246 RVA: 0x00058318 File Offset: 0x00056518
		[Conditional("DEBUG")]
		[Conditional("_FORCE_ASSERTS")]
		[ReliabilityContract(Consistency.MayCorruptAppDomain, Cer.None)]
		public static void Assert(string message, string detailMessage)
		{
			try
			{
				GlobalLog.Logobject.DumpArray(false);
			}
			finally
			{
				Debugger.Break();
			}
		}

		// Token: 0x06001867 RID: 6247 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void LeaveException(string func, Exception exception)
		{
		}

		// Token: 0x06001868 RID: 6248 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Leave(string func)
		{
		}

		// Token: 0x06001869 RID: 6249 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Leave(string func, string result)
		{
		}

		// Token: 0x0600186A RID: 6250 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Leave(string func, int returnval)
		{
		}

		// Token: 0x0600186B RID: 6251 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Leave(string func, bool returnval)
		{
		}

		// Token: 0x0600186C RID: 6252 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void DumpArray()
		{
		}

		// Token: 0x0600186D RID: 6253 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Dump(byte[] buffer)
		{
		}

		// Token: 0x0600186E RID: 6254 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Dump(byte[] buffer, int length)
		{
		}

		// Token: 0x0600186F RID: 6255 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Dump(byte[] buffer, int offset, int length)
		{
		}

		// Token: 0x06001870 RID: 6256 RVA: 0x0000232D File Offset: 0x0000052D
		[Conditional("TRAVE")]
		public static void Dump(IntPtr buffer, int offset, int length)
		{
		}

		// Token: 0x06001871 RID: 6257 RVA: 0x00058348 File Offset: 0x00056548
		// Note: this type is marked as 'beforefieldinit'.
		static GlobalLog()
		{
		}

		// Token: 0x04001782 RID: 6018
		private static BaseLoggingObject Logobject = GlobalLog.LoggingInitialize();
	}
}
