﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Net
{
	// Token: 0x020003B5 RID: 949
	internal class AutoWebProxyScriptEngine
	{
		// Token: 0x06001BFF RID: 7167 RVA: 0x0000232F File Offset: 0x0000052F
		public AutoWebProxyScriptEngine(WebProxy proxy, bool useRegistry)
		{
		}

		// Token: 0x170005B3 RID: 1459
		// (get) Token: 0x06001C00 RID: 7168 RVA: 0x0006543F File Offset: 0x0006363F
		// (set) Token: 0x06001C01 RID: 7169 RVA: 0x00065447 File Offset: 0x00063647
		public Uri AutomaticConfigurationScript
		{
			[CompilerGenerated]
			get
			{
				return this.<AutomaticConfigurationScript>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<AutomaticConfigurationScript>k__BackingField = value;
			}
		}

		// Token: 0x170005B4 RID: 1460
		// (get) Token: 0x06001C02 RID: 7170 RVA: 0x00065450 File Offset: 0x00063650
		// (set) Token: 0x06001C03 RID: 7171 RVA: 0x00065458 File Offset: 0x00063658
		public bool AutomaticallyDetectSettings
		{
			[CompilerGenerated]
			get
			{
				return this.<AutomaticallyDetectSettings>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<AutomaticallyDetectSettings>k__BackingField = value;
			}
		}

		// Token: 0x06001C04 RID: 7172 RVA: 0x00065464 File Offset: 0x00063664
		public bool GetProxies(Uri destination, out IList<string> proxyList)
		{
			int num = 0;
			return this.GetProxies(destination, out proxyList, ref num);
		}

		// Token: 0x06001C05 RID: 7173 RVA: 0x0006547D File Offset: 0x0006367D
		public bool GetProxies(Uri destination, out IList<string> proxyList, ref int syncStatus)
		{
			proxyList = null;
			return false;
		}

		// Token: 0x06001C06 RID: 7174 RVA: 0x0000232D File Offset: 0x0000052D
		public void Close()
		{
		}

		// Token: 0x06001C07 RID: 7175 RVA: 0x0000232D File Offset: 0x0000052D
		public void Abort(ref int syncStatus)
		{
		}

		// Token: 0x06001C08 RID: 7176 RVA: 0x0000232D File Offset: 0x0000052D
		public void CheckForChanges()
		{
		}

		// Token: 0x0400193B RID: 6459
		[CompilerGenerated]
		private Uri <AutomaticConfigurationScript>k__BackingField;

		// Token: 0x0400193C RID: 6460
		[CompilerGenerated]
		private bool <AutomaticallyDetectSettings>k__BackingField;
	}
}
