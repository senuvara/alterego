﻿using System;

namespace System.Net
{
	// Token: 0x02000307 RID: 775
	internal enum ChainPolicyType
	{
		// Token: 0x04001599 RID: 5529
		Base = 1,
		// Token: 0x0400159A RID: 5530
		Authenticode,
		// Token: 0x0400159B RID: 5531
		Authenticode_TS,
		// Token: 0x0400159C RID: 5532
		SSL,
		// Token: 0x0400159D RID: 5533
		BasicConstraints,
		// Token: 0x0400159E RID: 5534
		NtAuth
	}
}
