﻿using System;

namespace System.Net
{
	// Token: 0x02000300 RID: 768
	internal enum WindowsInstallationType
	{
		// Token: 0x0400153D RID: 5437
		Unknown,
		// Token: 0x0400153E RID: 5438
		Client,
		// Token: 0x0400153F RID: 5439
		Server,
		// Token: 0x04001540 RID: 5440
		ServerCore,
		// Token: 0x04001541 RID: 5441
		Embedded
	}
}
