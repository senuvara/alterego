﻿using System;

namespace System.Net
{
	// Token: 0x02000302 RID: 770
	internal enum ContentTypeValues
	{
		// Token: 0x0400156B RID: 5483
		ChangeCipherSpec = 20,
		// Token: 0x0400156C RID: 5484
		Alert,
		// Token: 0x0400156D RID: 5485
		HandShake,
		// Token: 0x0400156E RID: 5486
		AppData,
		// Token: 0x0400156F RID: 5487
		Unrecognized = 255
	}
}
