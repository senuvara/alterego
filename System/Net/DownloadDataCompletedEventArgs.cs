﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Net
{
	/// <summary>Provides data for the <see cref="E:System.Net.WebClient.DownloadDataCompleted" /> event.</summary>
	// Token: 0x020003A6 RID: 934
	public class DownloadDataCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06001B9A RID: 7066 RVA: 0x000649FE File Offset: 0x00062BFE
		internal DownloadDataCompletedEventArgs(byte[] result, Exception exception, bool cancelled, object userToken) : base(exception, cancelled, userToken)
		{
			this.m_Result = result;
		}

		/// <summary>Gets the data that is downloaded by a <see cref="Overload:System.Net.WebClient.DownloadDataAsync" /> method.</summary>
		/// <returns>A <see cref="T:System.Byte" /> array that contains the downloaded data.</returns>
		// Token: 0x1700059F RID: 1439
		// (get) Token: 0x06001B9B RID: 7067 RVA: 0x00064A11 File Offset: 0x00062C11
		public byte[] Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return this.m_Result;
			}
		}

		// Token: 0x06001B9C RID: 7068 RVA: 0x000092E2 File Offset: 0x000074E2
		internal DownloadDataCompletedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001921 RID: 6433
		private byte[] m_Result;
	}
}
