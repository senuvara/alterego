﻿using System;

namespace System.Net.Sockets
{
	/// <summary>Specifies the addressing scheme that an instance of the <see cref="T:System.Net.Sockets.Socket" /> class can use.</summary>
	// Token: 0x02000457 RID: 1111
	public enum AddressFamily
	{
		/// <summary>Unknown address family.</summary>
		// Token: 0x04001CF2 RID: 7410
		Unknown = -1,
		/// <summary>Unspecified address family.</summary>
		// Token: 0x04001CF3 RID: 7411
		Unspecified,
		/// <summary>Unix local to host address.</summary>
		// Token: 0x04001CF4 RID: 7412
		Unix,
		/// <summary>Address for IP version 4.</summary>
		// Token: 0x04001CF5 RID: 7413
		InterNetwork,
		/// <summary>ARPANET IMP address.</summary>
		// Token: 0x04001CF6 RID: 7414
		ImpLink,
		/// <summary>Address for PUP protocols.</summary>
		// Token: 0x04001CF7 RID: 7415
		Pup,
		/// <summary>Address for MIT CHAOS protocols.</summary>
		// Token: 0x04001CF8 RID: 7416
		Chaos,
		/// <summary>Address for Xerox NS protocols.</summary>
		// Token: 0x04001CF9 RID: 7417
		NS,
		/// <summary>IPX or SPX address.</summary>
		// Token: 0x04001CFA RID: 7418
		Ipx = 6,
		/// <summary>Address for ISO protocols.</summary>
		// Token: 0x04001CFB RID: 7419
		Iso,
		/// <summary>Address for OSI protocols.</summary>
		// Token: 0x04001CFC RID: 7420
		Osi = 7,
		/// <summary>European Computer Manufacturers Association (ECMA) address.</summary>
		// Token: 0x04001CFD RID: 7421
		Ecma,
		/// <summary>Address for Datakit protocols.</summary>
		// Token: 0x04001CFE RID: 7422
		DataKit,
		/// <summary>Addresses for CCITT protocols, such as X.25.</summary>
		// Token: 0x04001CFF RID: 7423
		Ccitt,
		/// <summary>IBM SNA address.</summary>
		// Token: 0x04001D00 RID: 7424
		Sna,
		/// <summary>DECnet address.</summary>
		// Token: 0x04001D01 RID: 7425
		DecNet,
		/// <summary>Direct data-link interface address.</summary>
		// Token: 0x04001D02 RID: 7426
		DataLink,
		/// <summary>LAT address.</summary>
		// Token: 0x04001D03 RID: 7427
		Lat,
		/// <summary>NSC Hyperchannel address.</summary>
		// Token: 0x04001D04 RID: 7428
		HyperChannel,
		/// <summary>AppleTalk address.</summary>
		// Token: 0x04001D05 RID: 7429
		AppleTalk,
		/// <summary>NetBios address.</summary>
		// Token: 0x04001D06 RID: 7430
		NetBios,
		/// <summary>VoiceView address.</summary>
		// Token: 0x04001D07 RID: 7431
		VoiceView,
		/// <summary>FireFox address.</summary>
		// Token: 0x04001D08 RID: 7432
		FireFox,
		/// <summary>Banyan address.</summary>
		// Token: 0x04001D09 RID: 7433
		Banyan = 21,
		/// <summary>Native ATM services address.</summary>
		// Token: 0x04001D0A RID: 7434
		Atm,
		/// <summary>Address for IP version 6.</summary>
		// Token: 0x04001D0B RID: 7435
		InterNetworkV6,
		/// <summary>Address for Microsoft cluster products.</summary>
		// Token: 0x04001D0C RID: 7436
		Cluster,
		/// <summary>IEEE 1284.4 workgroup address.</summary>
		// Token: 0x04001D0D RID: 7437
		Ieee12844,
		/// <summary>IrDA address.</summary>
		// Token: 0x04001D0E RID: 7438
		Irda,
		/// <summary>Address for Network Designers OSI gateway-enabled protocols.</summary>
		// Token: 0x04001D0F RID: 7439
		NetworkDesigners = 28,
		/// <summary>MAX address.</summary>
		// Token: 0x04001D10 RID: 7440
		Max
	}
}
