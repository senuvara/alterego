﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using Unity;

namespace System.Net.Sockets
{
	/// <summary>Represents an asynchronous socket operation.</summary>
	// Token: 0x02000481 RID: 1153
	public class SocketAsyncEventArgs : EventArgs, IDisposable
	{
		/// <summary>Gets the exception in the case of a connection failure when a <see cref="T:System.Net.DnsEndPoint" /> was used.</summary>
		/// <returns>An <see cref="T:System.Exception" /> that indicates the cause of the connection error when a <see cref="T:System.Net.DnsEndPoint" /> was specified for the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.RemoteEndPoint" /> property.</returns>
		// Token: 0x170007E5 RID: 2021
		// (get) Token: 0x060024F2 RID: 9458 RVA: 0x0008A687 File Offset: 0x00088887
		// (set) Token: 0x060024F3 RID: 9459 RVA: 0x0008A68F File Offset: 0x0008888F
		public Exception ConnectByNameError
		{
			[CompilerGenerated]
			get
			{
				return this.<ConnectByNameError>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<ConnectByNameError>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the socket to use or the socket created for accepting a connection with an asynchronous socket method.</summary>
		/// <returns>The <see cref="T:System.Net.Sockets.Socket" /> to use or the socket created for accepting a connection with an asynchronous socket method.</returns>
		// Token: 0x170007E6 RID: 2022
		// (get) Token: 0x060024F4 RID: 9460 RVA: 0x0008A698 File Offset: 0x00088898
		// (set) Token: 0x060024F5 RID: 9461 RVA: 0x0008A6A0 File Offset: 0x000888A0
		public Socket AcceptSocket
		{
			[CompilerGenerated]
			get
			{
				return this.<AcceptSocket>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<AcceptSocket>k__BackingField = value;
			}
		}

		/// <summary>Gets the data buffer to use with an asynchronous socket method.</summary>
		/// <returns>A <see cref="T:System.Byte" /> array that represents the data buffer to use with an asynchronous socket method.</returns>
		// Token: 0x170007E7 RID: 2023
		// (get) Token: 0x060024F6 RID: 9462 RVA: 0x0008A6A9 File Offset: 0x000888A9
		// (set) Token: 0x060024F7 RID: 9463 RVA: 0x0008A6B1 File Offset: 0x000888B1
		public byte[] Buffer
		{
			[CompilerGenerated]
			get
			{
				return this.<Buffer>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Buffer>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets an array of data buffers to use with an asynchronous socket method.</summary>
		/// <returns>An <see cref="T:System.Collections.IList" /> that represents an array of data buffers to use with an asynchronous socket method.</returns>
		/// <exception cref="T:System.ArgumentException">There are ambiguous buffers specified on a set operation. This exception occurs if the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.Buffer" /> property has been set to a non-null value and an attempt was made to set the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.BufferList" /> property to a non-null value.</exception>
		// Token: 0x170007E8 RID: 2024
		// (get) Token: 0x060024F8 RID: 9464 RVA: 0x0008A6BA File Offset: 0x000888BA
		// (set) Token: 0x060024F9 RID: 9465 RVA: 0x0008A6C2 File Offset: 0x000888C2
		public IList<ArraySegment<byte>> BufferList
		{
			get
			{
				return this.m_BufferList;
			}
			set
			{
				if (this.Buffer != null && value != null)
				{
					throw new ArgumentException("Buffer and BufferList properties cannot both be non-null.");
				}
				this.m_BufferList = value;
			}
		}

		/// <summary>Gets the number of bytes transferred in the socket operation.</summary>
		/// <returns>An <see cref="T:System.Int32" /> that contains the number of bytes transferred in the socket operation.</returns>
		// Token: 0x170007E9 RID: 2025
		// (get) Token: 0x060024FA RID: 9466 RVA: 0x0008A6E1 File Offset: 0x000888E1
		// (set) Token: 0x060024FB RID: 9467 RVA: 0x0008A6E9 File Offset: 0x000888E9
		public int BytesTransferred
		{
			[CompilerGenerated]
			get
			{
				return this.<BytesTransferred>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<BytesTransferred>k__BackingField = value;
			}
		}

		/// <summary>Gets the maximum amount of data, in bytes, to send or receive in an asynchronous operation.</summary>
		/// <returns>An <see cref="T:System.Int32" /> that contains the maximum amount of data, in bytes, to send or receive.</returns>
		// Token: 0x170007EA RID: 2026
		// (get) Token: 0x060024FC RID: 9468 RVA: 0x0008A6F2 File Offset: 0x000888F2
		// (set) Token: 0x060024FD RID: 9469 RVA: 0x0008A6FA File Offset: 0x000888FA
		public int Count
		{
			[CompilerGenerated]
			get
			{
				return this.<Count>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Count>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that specifies if socket can be reused after a disconnect operation.</summary>
		/// <returns>A <see cref="T:System.Boolean" /> that specifies if socket can be reused after a disconnect operation.</returns>
		// Token: 0x170007EB RID: 2027
		// (get) Token: 0x060024FE RID: 9470 RVA: 0x0008A703 File Offset: 0x00088903
		// (set) Token: 0x060024FF RID: 9471 RVA: 0x0008A70B File Offset: 0x0008890B
		public bool DisconnectReuseSocket
		{
			[CompilerGenerated]
			get
			{
				return this.<DisconnectReuseSocket>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<DisconnectReuseSocket>k__BackingField = value;
			}
		}

		/// <summary>Gets the type of socket operation most recently performed with this context object.</summary>
		/// <returns>A <see cref="T:System.Net.Sockets.SocketAsyncOperation" /> instance that indicates the type of socket operation most recently performed with this context object.</returns>
		// Token: 0x170007EC RID: 2028
		// (get) Token: 0x06002500 RID: 9472 RVA: 0x0008A714 File Offset: 0x00088914
		// (set) Token: 0x06002501 RID: 9473 RVA: 0x0008A71C File Offset: 0x0008891C
		public SocketAsyncOperation LastOperation
		{
			[CompilerGenerated]
			get
			{
				return this.<LastOperation>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<LastOperation>k__BackingField = value;
			}
		}

		/// <summary>Gets the offset, in bytes, into the data buffer referenced by the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.Buffer" /> property.</summary>
		/// <returns>An <see cref="T:System.Int32" /> that contains the offset, in bytes, into the data buffer referenced by the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.Buffer" /> property.</returns>
		// Token: 0x170007ED RID: 2029
		// (get) Token: 0x06002502 RID: 9474 RVA: 0x0008A725 File Offset: 0x00088925
		// (set) Token: 0x06002503 RID: 9475 RVA: 0x0008A72D File Offset: 0x0008892D
		public int Offset
		{
			[CompilerGenerated]
			get
			{
				return this.<Offset>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Offset>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the remote IP endpoint for an asynchronous operation.</summary>
		/// <returns>An <see cref="T:System.Net.EndPoint" /> that represents the remote IP endpoint for an asynchronous operation.</returns>
		// Token: 0x170007EE RID: 2030
		// (get) Token: 0x06002504 RID: 9476 RVA: 0x0008A736 File Offset: 0x00088936
		// (set) Token: 0x06002505 RID: 9477 RVA: 0x0008A73E File Offset: 0x0008893E
		public EndPoint RemoteEndPoint
		{
			get
			{
				return this.remote_ep;
			}
			set
			{
				this.remote_ep = value;
			}
		}

		/// <summary>Gets the IP address and interface of a received packet.</summary>
		/// <returns>An <see cref="T:System.Net.Sockets.IPPacketInformation" /> instance that contains the destination IP address and interface of a received packet.</returns>
		// Token: 0x170007EF RID: 2031
		// (get) Token: 0x06002506 RID: 9478 RVA: 0x0008A747 File Offset: 0x00088947
		// (set) Token: 0x06002507 RID: 9479 RVA: 0x0008A74F File Offset: 0x0008894F
		public IPPacketInformation ReceiveMessageFromPacketInfo
		{
			[CompilerGenerated]
			get
			{
				return this.<ReceiveMessageFromPacketInfo>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ReceiveMessageFromPacketInfo>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets an array of buffers to be sent for an asynchronous operation used by the <see cref="M:System.Net.Sockets.Socket.SendPacketsAsync(System.Net.Sockets.SocketAsyncEventArgs)" /> method.</summary>
		/// <returns>An array of <see cref="T:System.Net.Sockets.SendPacketsElement" /> objects that represent an array of buffers to be sent.</returns>
		// Token: 0x170007F0 RID: 2032
		// (get) Token: 0x06002508 RID: 9480 RVA: 0x0008A758 File Offset: 0x00088958
		// (set) Token: 0x06002509 RID: 9481 RVA: 0x0008A760 File Offset: 0x00088960
		public SendPacketsElement[] SendPacketsElements
		{
			[CompilerGenerated]
			get
			{
				return this.<SendPacketsElements>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SendPacketsElements>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a bitwise combination of <see cref="T:System.Net.Sockets.TransmitFileOptions" /> values for an asynchronous operation used by the <see cref="M:System.Net.Sockets.Socket.SendPacketsAsync(System.Net.Sockets.SocketAsyncEventArgs)" /> method.</summary>
		/// <returns>A <see cref="T:System.Net.Sockets.TransmitFileOptions" /> that contains a bitwise combination of values that are used with an asynchronous operation.</returns>
		// Token: 0x170007F1 RID: 2033
		// (get) Token: 0x0600250A RID: 9482 RVA: 0x0008A769 File Offset: 0x00088969
		// (set) Token: 0x0600250B RID: 9483 RVA: 0x0008A771 File Offset: 0x00088971
		public TransmitFileOptions SendPacketsFlags
		{
			[CompilerGenerated]
			get
			{
				return this.<SendPacketsFlags>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SendPacketsFlags>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the size, in bytes, of the data block used in the send operation.</summary>
		/// <returns>An <see cref="T:System.Int32" /> that contains the size, in bytes, of the data block used in the send operation.</returns>
		// Token: 0x170007F2 RID: 2034
		// (get) Token: 0x0600250C RID: 9484 RVA: 0x0008A77A File Offset: 0x0008897A
		// (set) Token: 0x0600250D RID: 9485 RVA: 0x0008A782 File Offset: 0x00088982
		[MonoTODO("unused property")]
		public int SendPacketsSendSize
		{
			[CompilerGenerated]
			get
			{
				return this.<SendPacketsSendSize>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SendPacketsSendSize>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the result of the asynchronous socket operation.</summary>
		/// <returns>A <see cref="T:System.Net.Sockets.SocketError" /> that represents the result of the asynchronous socket operation.</returns>
		// Token: 0x170007F3 RID: 2035
		// (get) Token: 0x0600250E RID: 9486 RVA: 0x0008A78B File Offset: 0x0008898B
		// (set) Token: 0x0600250F RID: 9487 RVA: 0x0008A793 File Offset: 0x00088993
		public SocketError SocketError
		{
			[CompilerGenerated]
			get
			{
				return this.<SocketError>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SocketError>k__BackingField = value;
			}
		}

		/// <summary>Gets the results of an asynchronous socket operation or sets the behavior of an asynchronous operation.</summary>
		/// <returns>A <see cref="T:System.Net.Sockets.SocketFlags" /> that represents the results of an asynchronous socket operation.</returns>
		// Token: 0x170007F4 RID: 2036
		// (get) Token: 0x06002510 RID: 9488 RVA: 0x0008A79C File Offset: 0x0008899C
		// (set) Token: 0x06002511 RID: 9489 RVA: 0x0008A7A4 File Offset: 0x000889A4
		public SocketFlags SocketFlags
		{
			[CompilerGenerated]
			get
			{
				return this.<SocketFlags>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SocketFlags>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a user or application object associated with this asynchronous socket operation.</summary>
		/// <returns>An object that represents the user or application object associated with this asynchronous socket operation.</returns>
		// Token: 0x170007F5 RID: 2037
		// (get) Token: 0x06002512 RID: 9490 RVA: 0x0008A7AD File Offset: 0x000889AD
		// (set) Token: 0x06002513 RID: 9491 RVA: 0x0008A7B5 File Offset: 0x000889B5
		public object UserToken
		{
			[CompilerGenerated]
			get
			{
				return this.<UserToken>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<UserToken>k__BackingField = value;
			}
		}

		/// <summary>The created and connected <see cref="T:System.Net.Sockets.Socket" /> object after successful completion of the <see cref="Overload:System.Net.Sockets.Socket.ConnectAsync" /> method.</summary>
		/// <returns>The connected <see cref="T:System.Net.Sockets.Socket" /> object.</returns>
		// Token: 0x170007F6 RID: 2038
		// (get) Token: 0x06002514 RID: 9492 RVA: 0x0008A7C0 File Offset: 0x000889C0
		public Socket ConnectSocket
		{
			get
			{
				SocketError socketError = this.SocketError;
				if (socketError == SocketError.AccessDenied)
				{
					return null;
				}
				return this.current_socket;
			}
		}

		// Token: 0x170007F7 RID: 2039
		// (get) Token: 0x06002515 RID: 9493 RVA: 0x0008A7E4 File Offset: 0x000889E4
		// (set) Token: 0x06002516 RID: 9494 RVA: 0x0008A7EC File Offset: 0x000889EC
		internal bool PolicyRestricted
		{
			[CompilerGenerated]
			get
			{
				return this.<PolicyRestricted>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<PolicyRestricted>k__BackingField = value;
			}
		}

		/// <summary>The event used to complete an asynchronous operation.</summary>
		// Token: 0x1400003E RID: 62
		// (add) Token: 0x06002517 RID: 9495 RVA: 0x0008A7F8 File Offset: 0x000889F8
		// (remove) Token: 0x06002518 RID: 9496 RVA: 0x0008A830 File Offset: 0x00088A30
		public event EventHandler<SocketAsyncEventArgs> Completed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<SocketAsyncEventArgs> eventHandler = this.Completed;
				EventHandler<SocketAsyncEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<SocketAsyncEventArgs> value2 = (EventHandler<SocketAsyncEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<SocketAsyncEventArgs>>(ref this.Completed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<SocketAsyncEventArgs> eventHandler = this.Completed;
				EventHandler<SocketAsyncEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<SocketAsyncEventArgs> value2 = (EventHandler<SocketAsyncEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<SocketAsyncEventArgs>>(ref this.Completed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x06002519 RID: 9497 RVA: 0x0008A865 File Offset: 0x00088A65
		internal SocketAsyncEventArgs(bool policy) : this()
		{
			this.PolicyRestricted = policy;
		}

		/// <summary>Creates an empty <see cref="T:System.Net.Sockets.SocketAsyncEventArgs" /> instance.</summary>
		/// <exception cref="T:System.NotSupportedException">The platform is not supported. </exception>
		// Token: 0x0600251A RID: 9498 RVA: 0x0008A874 File Offset: 0x00088A74
		public SocketAsyncEventArgs()
		{
			this.SendPacketsSendSize = -1;
		}

		/// <summary>Frees resources used by the <see cref="T:System.Net.Sockets.SocketAsyncEventArgs" /> class.</summary>
		// Token: 0x0600251B RID: 9499 RVA: 0x0008A890 File Offset: 0x00088A90
		~SocketAsyncEventArgs()
		{
			this.Dispose(false);
		}

		// Token: 0x0600251C RID: 9500 RVA: 0x0008A8C0 File Offset: 0x00088AC0
		private void Dispose(bool disposing)
		{
			this.disposed = true;
			if (disposing)
			{
				int num = this.in_progress;
				return;
			}
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Net.Sockets.SocketAsyncEventArgs" /> instance and optionally disposes of the managed resources.</summary>
		// Token: 0x0600251D RID: 9501 RVA: 0x0008A8D6 File Offset: 0x00088AD6
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600251E RID: 9502 RVA: 0x0008A8E5 File Offset: 0x00088AE5
		internal void SetLastOperation(SocketAsyncOperation op)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("System.Net.Sockets.SocketAsyncEventArgs");
			}
			if (Interlocked.Exchange(ref this.in_progress, 1) != 0)
			{
				throw new InvalidOperationException("Operation already in progress");
			}
			this.LastOperation = op;
		}

		// Token: 0x0600251F RID: 9503 RVA: 0x0008A91A File Offset: 0x00088B1A
		internal void Complete()
		{
			this.OnCompleted(this);
		}

		/// <summary>Represents a method that is called when an asynchronous operation completes.</summary>
		/// <param name="e">The event that is signaled.</param>
		// Token: 0x06002520 RID: 9504 RVA: 0x0008A924 File Offset: 0x00088B24
		protected virtual void OnCompleted(SocketAsyncEventArgs e)
		{
			if (e == null)
			{
				return;
			}
			EventHandler<SocketAsyncEventArgs> completed = e.Completed;
			if (completed != null)
			{
				completed(e.current_socket, e);
			}
		}

		/// <summary>Sets the data buffer to use with an asynchronous socket method.</summary>
		/// <param name="offset">The offset, in bytes, in the data buffer where the operation starts.</param>
		/// <param name="count">The maximum amount of data, in bytes, to send or receive in the buffer.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">An argument was out of range. This exception occurs if the <paramref name="offset" /> parameter is less than zero or greater than the length of the array in the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.Buffer" /> property. This exception also occurs if the <paramref name="count" /> parameter is less than zero or greater than the length of the array in the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.Buffer" /> property minus the <paramref name="offset" /> parameter.</exception>
		// Token: 0x06002521 RID: 9505 RVA: 0x0008A94C File Offset: 0x00088B4C
		public void SetBuffer(int offset, int count)
		{
			this.SetBuffer(this.Buffer, offset, count);
		}

		/// <summary>Sets the data buffer to use with an asynchronous socket method.</summary>
		/// <param name="buffer">The data buffer to use with an asynchronous socket method.</param>
		/// <param name="offset">The offset, in bytes, in the data buffer where the operation starts.</param>
		/// <param name="count">The maximum amount of data, in bytes, to send or receive in the buffer.</param>
		/// <exception cref="T:System.ArgumentException">There are ambiguous buffers specified. This exception occurs if the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.Buffer" /> property is also not null and the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.BufferList" /> property is also not null.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">An argument was out of range. This exception occurs if the <paramref name="offset" /> parameter is less than zero or greater than the length of the array in the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.Buffer" /> property. This exception also occurs if the <paramref name="count" /> parameter is less than zero or greater than the length of the array in the <see cref="P:System.Net.Sockets.SocketAsyncEventArgs.Buffer" /> property minus the <paramref name="offset" /> parameter.</exception>
		// Token: 0x06002522 RID: 9506 RVA: 0x0008A95C File Offset: 0x00088B5C
		public void SetBuffer(byte[] buffer, int offset, int count)
		{
			if (buffer != null)
			{
				if (this.BufferList != null)
				{
					throw new ArgumentException("Buffer and BufferList properties cannot both be non-null.");
				}
				int num = buffer.Length;
				if (offset < 0 || (offset != 0 && offset >= num))
				{
					throw new ArgumentOutOfRangeException("offset");
				}
				if (count < 0 || count > num - offset)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				this.Count = count;
				this.Offset = offset;
			}
			this.Buffer = buffer;
		}

		// Token: 0x06002523 RID: 9507 RVA: 0x0008A9C3 File Offset: 0x00088BC3
		internal void StartOperationCommon(Socket socket)
		{
			this.current_socket = socket;
		}

		// Token: 0x06002524 RID: 9508 RVA: 0x0008A9CC File Offset: 0x00088BCC
		internal void StartOperationWrapperConnect(MultipleConnectAsync args)
		{
			this.SetLastOperation(SocketAsyncOperation.Connect);
		}

		// Token: 0x06002525 RID: 9509 RVA: 0x000068D7 File Offset: 0x00004AD7
		internal void FinishConnectByNameSyncFailure(Exception exception, int bytesTransferred, SocketFlags flags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002526 RID: 9510 RVA: 0x000068D7 File Offset: 0x00004AD7
		internal void FinishOperationAsyncFailure(Exception exception, int bytesTransferred, SocketFlags flags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002527 RID: 9511 RVA: 0x0008A9D5 File Offset: 0x00088BD5
		internal void FinishWrapperConnectSuccess(Socket connectSocket, int bytesTransferred, SocketFlags flags)
		{
			this.SetResults(SocketError.Success, bytesTransferred, flags);
			this.current_socket = connectSocket;
			this.OnCompleted(this);
		}

		// Token: 0x06002528 RID: 9512 RVA: 0x0008A9EE File Offset: 0x00088BEE
		internal void SetResults(SocketError socketError, int bytesTransferred, SocketFlags flags)
		{
			this.SocketError = socketError;
			this.BytesTransferred = bytesTransferred;
			this.SocketFlags = flags;
		}

		/// <summary>Gets or sets the protocol to use to download the socket client access policy file. </summary>
		/// <returns>Returns <see cref="T:System.Net.Sockets.SocketClientAccessPolicyProtocol" />.The protocol to use to download the socket client access policy file.</returns>
		// Token: 0x170007F8 RID: 2040
		// (get) Token: 0x06002529 RID: 9513 RVA: 0x0008AA08 File Offset: 0x00088C08
		// (set) Token: 0x0600252A RID: 9514 RVA: 0x000092E2 File Offset: 0x000074E2
		public SocketClientAccessPolicyProtocol SocketClientAccessPolicyProtocol
		{
			[CompilerGenerated]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SocketClientAccessPolicyProtocol.Tcp;
			}
			[CompilerGenerated]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x04001E8C RID: 7820
		private bool disposed;

		// Token: 0x04001E8D RID: 7821
		internal volatile int in_progress;

		// Token: 0x04001E8E RID: 7822
		internal EndPoint remote_ep;

		// Token: 0x04001E8F RID: 7823
		internal Socket current_socket;

		// Token: 0x04001E90 RID: 7824
		internal SocketAsyncResult socket_async_result = new SocketAsyncResult();

		// Token: 0x04001E91 RID: 7825
		[CompilerGenerated]
		private Exception <ConnectByNameError>k__BackingField;

		// Token: 0x04001E92 RID: 7826
		[CompilerGenerated]
		private Socket <AcceptSocket>k__BackingField;

		// Token: 0x04001E93 RID: 7827
		[CompilerGenerated]
		private byte[] <Buffer>k__BackingField;

		// Token: 0x04001E94 RID: 7828
		internal IList<ArraySegment<byte>> m_BufferList;

		// Token: 0x04001E95 RID: 7829
		[CompilerGenerated]
		private int <BytesTransferred>k__BackingField;

		// Token: 0x04001E96 RID: 7830
		[CompilerGenerated]
		private int <Count>k__BackingField;

		// Token: 0x04001E97 RID: 7831
		[CompilerGenerated]
		private bool <DisconnectReuseSocket>k__BackingField;

		// Token: 0x04001E98 RID: 7832
		[CompilerGenerated]
		private SocketAsyncOperation <LastOperation>k__BackingField;

		// Token: 0x04001E99 RID: 7833
		[CompilerGenerated]
		private int <Offset>k__BackingField;

		// Token: 0x04001E9A RID: 7834
		[CompilerGenerated]
		private IPPacketInformation <ReceiveMessageFromPacketInfo>k__BackingField;

		// Token: 0x04001E9B RID: 7835
		[CompilerGenerated]
		private SendPacketsElement[] <SendPacketsElements>k__BackingField;

		// Token: 0x04001E9C RID: 7836
		[CompilerGenerated]
		private TransmitFileOptions <SendPacketsFlags>k__BackingField;

		// Token: 0x04001E9D RID: 7837
		[CompilerGenerated]
		private int <SendPacketsSendSize>k__BackingField;

		// Token: 0x04001E9E RID: 7838
		[CompilerGenerated]
		private SocketError <SocketError>k__BackingField;

		// Token: 0x04001E9F RID: 7839
		[CompilerGenerated]
		private SocketFlags <SocketFlags>k__BackingField;

		// Token: 0x04001EA0 RID: 7840
		[CompilerGenerated]
		private object <UserToken>k__BackingField;

		// Token: 0x04001EA1 RID: 7841
		[CompilerGenerated]
		private bool <PolicyRestricted>k__BackingField;

		// Token: 0x04001EA2 RID: 7842
		[CompilerGenerated]
		private EventHandler<SocketAsyncEventArgs> Completed;
	}
}
