﻿using System;

namespace System.Net.Sockets
{
	/// <summary>Specifies the type of protocol that an instance of the <see cref="T:System.Net.Sockets.Socket" /> class can use.</summary>
	// Token: 0x0200045F RID: 1119
	public enum ProtocolFamily
	{
		/// <summary>Unknown protocol.</summary>
		// Token: 0x04001D4B RID: 7499
		Unknown = -1,
		/// <summary>Unspecified protocol.</summary>
		// Token: 0x04001D4C RID: 7500
		Unspecified,
		/// <summary>Unix local to host protocol.</summary>
		// Token: 0x04001D4D RID: 7501
		Unix,
		/// <summary>IP version 4 protocol.</summary>
		// Token: 0x04001D4E RID: 7502
		InterNetwork,
		/// <summary>ARPANET IMP protocol.</summary>
		// Token: 0x04001D4F RID: 7503
		ImpLink,
		/// <summary>PUP protocol.</summary>
		// Token: 0x04001D50 RID: 7504
		Pup,
		/// <summary>MIT CHAOS protocol.</summary>
		// Token: 0x04001D51 RID: 7505
		Chaos,
		/// <summary>Xerox NS protocol.</summary>
		// Token: 0x04001D52 RID: 7506
		NS,
		/// <summary>IPX or SPX protocol.</summary>
		// Token: 0x04001D53 RID: 7507
		Ipx = 6,
		/// <summary>ISO protocol.</summary>
		// Token: 0x04001D54 RID: 7508
		Iso,
		/// <summary>OSI protocol.</summary>
		// Token: 0x04001D55 RID: 7509
		Osi = 7,
		/// <summary>European Computer Manufacturers Association (ECMA) protocol.</summary>
		// Token: 0x04001D56 RID: 7510
		Ecma,
		/// <summary>DataKit protocol.</summary>
		// Token: 0x04001D57 RID: 7511
		DataKit,
		/// <summary>CCITT protocol, such as X.25.</summary>
		// Token: 0x04001D58 RID: 7512
		Ccitt,
		/// <summary>IBM SNA protocol.</summary>
		// Token: 0x04001D59 RID: 7513
		Sna,
		/// <summary>DECNet protocol.</summary>
		// Token: 0x04001D5A RID: 7514
		DecNet,
		/// <summary>Direct data link protocol.</summary>
		// Token: 0x04001D5B RID: 7515
		DataLink,
		/// <summary>LAT protocol.</summary>
		// Token: 0x04001D5C RID: 7516
		Lat,
		/// <summary>NSC HyperChannel protocol.</summary>
		// Token: 0x04001D5D RID: 7517
		HyperChannel,
		/// <summary>AppleTalk protocol.</summary>
		// Token: 0x04001D5E RID: 7518
		AppleTalk,
		/// <summary>NetBIOS protocol.</summary>
		// Token: 0x04001D5F RID: 7519
		NetBios,
		/// <summary>VoiceView protocol.</summary>
		// Token: 0x04001D60 RID: 7520
		VoiceView,
		/// <summary>FireFox protocol.</summary>
		// Token: 0x04001D61 RID: 7521
		FireFox,
		/// <summary>Banyan protocol.</summary>
		// Token: 0x04001D62 RID: 7522
		Banyan = 21,
		/// <summary>Native ATM services protocol.</summary>
		// Token: 0x04001D63 RID: 7523
		Atm,
		/// <summary>IP version 6 protocol.</summary>
		// Token: 0x04001D64 RID: 7524
		InterNetworkV6,
		/// <summary>Microsoft Cluster products protocol.</summary>
		// Token: 0x04001D65 RID: 7525
		Cluster,
		/// <summary>IEEE 1284.4 workgroup protocol.</summary>
		// Token: 0x04001D66 RID: 7526
		Ieee12844,
		/// <summary>IrDA protocol.</summary>
		// Token: 0x04001D67 RID: 7527
		Irda,
		/// <summary>Network Designers OSI gateway enabled protocol.</summary>
		// Token: 0x04001D68 RID: 7528
		NetworkDesigners = 28,
		/// <summary>MAX protocol.</summary>
		// Token: 0x04001D69 RID: 7529
		Max
	}
}
