﻿using System;

namespace System.Net.Sockets
{
	/// <summary>Contains option values for joining an IPv6 multicast group.</summary>
	// Token: 0x0200045D RID: 1117
	public class IPv6MulticastOption
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Sockets.IPv6MulticastOption" /> class with the specified IP multicast group and the local interface address.</summary>
		/// <param name="group">The group <see cref="T:System.Net.IPAddress" />. </param>
		/// <param name="ifindex">The local interface address. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="ifindex" /> is less than 0.-or- 
		///         <paramref name="ifindex" /> is greater than 0x00000000FFFFFFFF. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="group" /> is <see langword="null" />. </exception>
		// Token: 0x060022F0 RID: 8944 RVA: 0x000827ED File Offset: 0x000809ED
		public IPv6MulticastOption(IPAddress group, long ifindex)
		{
			if (group == null)
			{
				throw new ArgumentNullException("group");
			}
			if (ifindex < 0L || ifindex > (long)((ulong)-1))
			{
				throw new ArgumentOutOfRangeException("ifindex");
			}
			this.Group = group;
			this.InterfaceIndex = ifindex;
		}

		/// <summary>Initializes a new version of the <see cref="T:System.Net.Sockets.IPv6MulticastOption" /> class for the specified IP multicast group.</summary>
		/// <param name="group">The <see cref="T:System.Net.IPAddress" /> of the multicast group. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="group" /> is <see langword="null" />. </exception>
		// Token: 0x060022F1 RID: 8945 RVA: 0x00082826 File Offset: 0x00080A26
		public IPv6MulticastOption(IPAddress group)
		{
			if (group == null)
			{
				throw new ArgumentNullException("group");
			}
			this.Group = group;
			this.InterfaceIndex = 0L;
		}

		/// <summary>Gets or sets the IP address of a multicast group.</summary>
		/// <returns>An <see cref="T:System.Net.IPAddress" /> that contains the Internet address of a multicast group.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="group" /> is <see langword="null" />. </exception>
		// Token: 0x1700078A RID: 1930
		// (get) Token: 0x060022F2 RID: 8946 RVA: 0x0008284B File Offset: 0x00080A4B
		// (set) Token: 0x060022F3 RID: 8947 RVA: 0x00082853 File Offset: 0x00080A53
		public IPAddress Group
		{
			get
			{
				return this.m_Group;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.m_Group = value;
			}
		}

		/// <summary>Gets or sets the interface index that is associated with a multicast group.</summary>
		/// <returns>A <see cref="T:System.UInt64" /> value that specifies the address of the interface.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value that is specified for a set operation is less than 0 or greater than 0x00000000FFFFFFFF. </exception>
		// Token: 0x1700078B RID: 1931
		// (get) Token: 0x060022F4 RID: 8948 RVA: 0x0008286A File Offset: 0x00080A6A
		// (set) Token: 0x060022F5 RID: 8949 RVA: 0x00082872 File Offset: 0x00080A72
		public long InterfaceIndex
		{
			get
			{
				return this.m_Interface;
			}
			set
			{
				if (value < 0L || value > (long)((ulong)-1))
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.m_Interface = value;
			}
		}

		// Token: 0x04001D40 RID: 7488
		private IPAddress m_Group;

		// Token: 0x04001D41 RID: 7489
		private long m_Interface;
	}
}
