﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x0200047F RID: 1151
	internal class MultipleSocketMultipleConnectAsync : MultipleConnectAsync
	{
		// Token: 0x060024E8 RID: 9448 RVA: 0x0008A221 File Offset: 0x00088421
		public MultipleSocketMultipleConnectAsync(SocketType socketType, ProtocolType protocolType)
		{
			if (Socket.OSSupportsIPv4)
			{
				this.socket4 = new Socket(AddressFamily.InterNetwork, socketType, protocolType);
			}
			if (Socket.OSSupportsIPv6)
			{
				this.socket6 = new Socket(AddressFamily.InterNetworkV6, socketType, protocolType);
			}
		}

		// Token: 0x060024E9 RID: 9449 RVA: 0x0008A254 File Offset: 0x00088454
		protected override IPAddress GetNextAddress(out Socket attemptSocket)
		{
			IPAddress ipaddress = null;
			attemptSocket = null;
			while (attemptSocket == null)
			{
				if (this.nextAddress >= this.addressList.Length)
				{
					return null;
				}
				ipaddress = this.addressList[this.nextAddress];
				this.nextAddress++;
				if (ipaddress.AddressFamily == AddressFamily.InterNetworkV6)
				{
					attemptSocket = this.socket6;
				}
				else if (ipaddress.AddressFamily == AddressFamily.InterNetwork)
				{
					attemptSocket = this.socket4;
				}
			}
			return ipaddress;
		}

		// Token: 0x060024EA RID: 9450 RVA: 0x0008A2C0 File Offset: 0x000884C0
		protected override void OnSucceed()
		{
			if (this.socket4 != null && !this.socket4.Connected)
			{
				this.socket4.Close();
			}
			if (this.socket6 != null && !this.socket6.Connected)
			{
				this.socket6.Close();
			}
		}

		// Token: 0x060024EB RID: 9451 RVA: 0x0008A30D File Offset: 0x0008850D
		protected override void OnFail(bool abortive)
		{
			if (this.socket4 != null)
			{
				this.socket4.Close();
			}
			if (this.socket6 != null)
			{
				this.socket6.Close();
			}
		}

		// Token: 0x04001E84 RID: 7812
		private Socket socket4;

		// Token: 0x04001E85 RID: 7813
		private Socket socket6;
	}
}
