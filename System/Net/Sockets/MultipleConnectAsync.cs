﻿using System;
using System.Threading;

namespace System.Net.Sockets
{
	// Token: 0x0200047C RID: 1148
	internal abstract class MultipleConnectAsync
	{
		// Token: 0x060024D5 RID: 9429 RVA: 0x00089D18 File Offset: 0x00087F18
		public bool StartConnectAsync(SocketAsyncEventArgs args, DnsEndPoint endPoint)
		{
			object obj = this.lockObject;
			bool result;
			lock (obj)
			{
				this.userArgs = args;
				this.endPoint = endPoint;
				if (this.state == MultipleConnectAsync.State.Canceled)
				{
					this.SyncFail(new SocketException(SocketError.OperationAborted));
					result = false;
				}
				else
				{
					this.state = MultipleConnectAsync.State.DnsQuery;
					IAsyncResult asyncResult = Dns.BeginGetHostAddresses(endPoint.Host, new AsyncCallback(this.DnsCallback), null);
					if (asyncResult.CompletedSynchronously)
					{
						result = this.DoDnsCallback(asyncResult, true);
					}
					else
					{
						result = true;
					}
				}
			}
			return result;
		}

		// Token: 0x060024D6 RID: 9430 RVA: 0x00089DB4 File Offset: 0x00087FB4
		private void DnsCallback(IAsyncResult result)
		{
			if (!result.CompletedSynchronously)
			{
				this.DoDnsCallback(result, false);
			}
		}

		// Token: 0x060024D7 RID: 9431 RVA: 0x00089DC8 File Offset: 0x00087FC8
		private bool DoDnsCallback(IAsyncResult result, bool sync)
		{
			Exception ex = null;
			object obj = this.lockObject;
			lock (obj)
			{
				if (this.state == MultipleConnectAsync.State.Canceled)
				{
					return true;
				}
				try
				{
					this.addressList = Dns.EndGetHostAddresses(result);
				}
				catch (Exception ex2)
				{
					this.state = MultipleConnectAsync.State.Completed;
					ex = ex2;
				}
				if (ex == null)
				{
					this.state = MultipleConnectAsync.State.ConnectAttempt;
					this.internalArgs = new SocketAsyncEventArgs();
					this.internalArgs.Completed += this.InternalConnectCallback;
					this.internalArgs.SetBuffer(this.userArgs.Buffer, this.userArgs.Offset, this.userArgs.Count);
					ex = this.AttemptConnection();
					if (ex != null)
					{
						this.state = MultipleConnectAsync.State.Completed;
					}
				}
			}
			return ex == null || this.Fail(sync, ex);
		}

		// Token: 0x060024D8 RID: 9432 RVA: 0x00089EB4 File Offset: 0x000880B4
		private void InternalConnectCallback(object sender, SocketAsyncEventArgs args)
		{
			Exception ex = null;
			object obj = this.lockObject;
			lock (obj)
			{
				if (this.state == MultipleConnectAsync.State.Canceled)
				{
					ex = new SocketException(SocketError.OperationAborted);
				}
				else if (args.SocketError == SocketError.Success)
				{
					this.state = MultipleConnectAsync.State.Completed;
				}
				else if (args.SocketError == SocketError.OperationAborted)
				{
					ex = new SocketException(SocketError.OperationAborted);
					this.state = MultipleConnectAsync.State.Canceled;
				}
				else
				{
					SocketError socketError = args.SocketError;
					Exception ex2 = this.AttemptConnection();
					if (ex2 == null)
					{
						return;
					}
					SocketException ex3 = ex2 as SocketException;
					if (ex3 != null && ex3.SocketErrorCode == SocketError.NoData)
					{
						ex = new SocketException(socketError);
					}
					else
					{
						ex = ex2;
					}
					this.state = MultipleConnectAsync.State.Completed;
				}
			}
			if (ex == null)
			{
				this.Succeed();
				return;
			}
			this.AsyncFail(ex);
		}

		// Token: 0x060024D9 RID: 9433 RVA: 0x00089F90 File Offset: 0x00088190
		private Exception AttemptConnection()
		{
			try
			{
				Socket socket = null;
				IPAddress ipaddress = this.GetNextAddress(out socket);
				if (ipaddress == null)
				{
					return new SocketException(SocketError.NoData);
				}
				this.internalArgs.RemoteEndPoint = new IPEndPoint(ipaddress, this.endPoint.Port);
				if (!socket.ConnectAsync(this.internalArgs))
				{
					return new SocketException(this.internalArgs.SocketError);
				}
			}
			catch (ObjectDisposedException)
			{
				return new SocketException(SocketError.OperationAborted);
			}
			catch (Exception result)
			{
				return result;
			}
			return null;
		}

		// Token: 0x060024DA RID: 9434
		protected abstract void OnSucceed();

		// Token: 0x060024DB RID: 9435 RVA: 0x0008A028 File Offset: 0x00088228
		protected void Succeed()
		{
			this.OnSucceed();
			this.userArgs.FinishWrapperConnectSuccess(this.internalArgs.ConnectSocket, this.internalArgs.BytesTransferred, this.internalArgs.SocketFlags);
			this.internalArgs.Dispose();
		}

		// Token: 0x060024DC RID: 9436
		protected abstract void OnFail(bool abortive);

		// Token: 0x060024DD RID: 9437 RVA: 0x0008A067 File Offset: 0x00088267
		private bool Fail(bool sync, Exception e)
		{
			if (sync)
			{
				this.SyncFail(e);
				return false;
			}
			this.AsyncFail(e);
			return true;
		}

		// Token: 0x060024DE RID: 9438 RVA: 0x0008A080 File Offset: 0x00088280
		private void SyncFail(Exception e)
		{
			this.OnFail(false);
			if (this.internalArgs != null)
			{
				this.internalArgs.Dispose();
			}
			SocketException ex = e as SocketException;
			if (ex != null)
			{
				this.userArgs.FinishConnectByNameSyncFailure(ex, 0, SocketFlags.None);
				return;
			}
			throw e;
		}

		// Token: 0x060024DF RID: 9439 RVA: 0x0008A0C1 File Offset: 0x000882C1
		private void AsyncFail(Exception e)
		{
			this.OnFail(false);
			if (this.internalArgs != null)
			{
				this.internalArgs.Dispose();
			}
			this.userArgs.FinishOperationAsyncFailure(e, 0, SocketFlags.None);
		}

		// Token: 0x060024E0 RID: 9440 RVA: 0x0008A0EC File Offset: 0x000882EC
		public void Cancel()
		{
			bool flag = false;
			object obj = this.lockObject;
			lock (obj)
			{
				switch (this.state)
				{
				case MultipleConnectAsync.State.NotStarted:
					flag = true;
					break;
				case MultipleConnectAsync.State.DnsQuery:
					ThreadPool.QueueUserWorkItem(new WaitCallback(this.CallAsyncFail));
					flag = true;
					break;
				case MultipleConnectAsync.State.ConnectAttempt:
					flag = true;
					break;
				}
				this.state = MultipleConnectAsync.State.Canceled;
			}
			if (flag)
			{
				this.OnFail(true);
			}
		}

		// Token: 0x060024E1 RID: 9441 RVA: 0x0008A174 File Offset: 0x00088374
		private void CallAsyncFail(object ignored)
		{
			this.AsyncFail(new SocketException(SocketError.OperationAborted));
		}

		// Token: 0x060024E2 RID: 9442
		protected abstract IPAddress GetNextAddress(out Socket attemptSocket);

		// Token: 0x060024E3 RID: 9443 RVA: 0x0008A186 File Offset: 0x00088386
		protected MultipleConnectAsync()
		{
		}

		// Token: 0x04001E75 RID: 7797
		protected SocketAsyncEventArgs userArgs;

		// Token: 0x04001E76 RID: 7798
		protected SocketAsyncEventArgs internalArgs;

		// Token: 0x04001E77 RID: 7799
		protected DnsEndPoint endPoint;

		// Token: 0x04001E78 RID: 7800
		protected IPAddress[] addressList;

		// Token: 0x04001E79 RID: 7801
		protected int nextAddress;

		// Token: 0x04001E7A RID: 7802
		private MultipleConnectAsync.State state;

		// Token: 0x04001E7B RID: 7803
		private object lockObject = new object();

		// Token: 0x0200047D RID: 1149
		private enum State
		{
			// Token: 0x04001E7D RID: 7805
			NotStarted,
			// Token: 0x04001E7E RID: 7806
			DnsQuery,
			// Token: 0x04001E7F RID: 7807
			ConnectAttempt,
			// Token: 0x04001E80 RID: 7808
			Completed,
			// Token: 0x04001E81 RID: 7809
			Canceled
		}
	}
}
