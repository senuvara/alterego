﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.Net.Sockets
{
	// Token: 0x02000482 RID: 1154
	[StructLayout(LayoutKind.Sequential)]
	internal sealed class SocketAsyncResult : IOAsyncResult
	{
		// Token: 0x170007F9 RID: 2041
		// (get) Token: 0x0600252B RID: 9515 RVA: 0x0008AA23 File Offset: 0x00088C23
		public IntPtr Handle
		{
			get
			{
				if (this.socket == null)
				{
					return IntPtr.Zero;
				}
				return this.socket.Handle;
			}
		}

		// Token: 0x0600252C RID: 9516 RVA: 0x0008AA3E File Offset: 0x00088C3E
		public SocketAsyncResult()
		{
		}

		// Token: 0x0600252D RID: 9517 RVA: 0x0008AA48 File Offset: 0x00088C48
		public void Init(Socket socket, AsyncCallback callback, object state, SocketOperation operation)
		{
			base.Init(callback, state);
			this.socket = socket;
			this.operation = operation;
			this.DelayedException = null;
			this.EndPoint = null;
			this.Buffer = null;
			this.Offset = 0;
			this.Size = 0;
			this.SockFlags = SocketFlags.None;
			this.AcceptSocket = null;
			this.Addresses = null;
			this.Port = 0;
			this.Buffers = null;
			this.ReuseSocket = false;
			this.CurrentAddress = 0;
			this.AcceptedSocket = null;
			this.Total = 0;
			this.error = 0;
			this.EndCalled = 0;
		}

		// Token: 0x0600252E RID: 9518 RVA: 0x0008AADC File Offset: 0x00088CDC
		public SocketAsyncResult(Socket socket, AsyncCallback callback, object state, SocketOperation operation) : base(callback, state)
		{
			this.socket = socket;
			this.operation = operation;
		}

		// Token: 0x170007FA RID: 2042
		// (get) Token: 0x0600252F RID: 9519 RVA: 0x0008AAF8 File Offset: 0x00088CF8
		public SocketError ErrorCode
		{
			get
			{
				SocketException ex = this.DelayedException as SocketException;
				if (ex != null)
				{
					return ex.SocketErrorCode;
				}
				if (this.error != 0)
				{
					return (SocketError)this.error;
				}
				return SocketError.Success;
			}
		}

		// Token: 0x06002530 RID: 9520 RVA: 0x0008AB2B File Offset: 0x00088D2B
		public void CheckIfThrowDelayedException()
		{
			if (this.DelayedException != null)
			{
				this.socket.is_connected = false;
				throw this.DelayedException;
			}
			if (this.error != 0)
			{
				this.socket.is_connected = false;
				throw new SocketException(this.error);
			}
		}

		// Token: 0x06002531 RID: 9521 RVA: 0x0008AB68 File Offset: 0x00088D68
		internal override void CompleteDisposed()
		{
			this.Complete();
		}

		// Token: 0x06002532 RID: 9522 RVA: 0x0008AB70 File Offset: 0x00088D70
		public void Complete()
		{
			if (this.operation != SocketOperation.Receive && this.socket.CleanedUp)
			{
				this.DelayedException = new ObjectDisposedException(this.socket.GetType().ToString());
			}
			base.IsCompleted = true;
			Socket socket = this.socket;
			SocketOperation socketOperation = this.operation;
			if (base.AsyncCallback != null)
			{
				ThreadPool.UnsafeQueueUserWorkItem(delegate(object state)
				{
					((SocketAsyncResult)state).AsyncCallback((SocketAsyncResult)state);
				}, this);
			}
			switch (socketOperation)
			{
			case SocketOperation.Accept:
			case SocketOperation.Receive:
			case SocketOperation.ReceiveFrom:
			case SocketOperation.ReceiveGeneric:
				socket.ReadSem.Release();
				return;
			case SocketOperation.Connect:
			case SocketOperation.RecvJustCallback:
			case SocketOperation.SendJustCallback:
			case SocketOperation.Disconnect:
			case SocketOperation.AcceptReceive:
				break;
			case SocketOperation.Send:
			case SocketOperation.SendTo:
			case SocketOperation.SendGeneric:
				socket.WriteSem.Release();
				break;
			default:
				return;
			}
		}

		// Token: 0x06002533 RID: 9523 RVA: 0x0008AC41 File Offset: 0x00088E41
		public void Complete(bool synch)
		{
			base.CompletedSynchronously = synch;
			this.Complete();
		}

		// Token: 0x06002534 RID: 9524 RVA: 0x0008AC50 File Offset: 0x00088E50
		public void Complete(int total)
		{
			this.Total = total;
			this.Complete();
		}

		// Token: 0x06002535 RID: 9525 RVA: 0x0008AC5F File Offset: 0x00088E5F
		public void Complete(Exception e, bool synch)
		{
			this.DelayedException = e;
			base.CompletedSynchronously = synch;
			this.Complete();
		}

		// Token: 0x06002536 RID: 9526 RVA: 0x0008AC75 File Offset: 0x00088E75
		public void Complete(Exception e)
		{
			this.DelayedException = e;
			this.Complete();
		}

		// Token: 0x06002537 RID: 9527 RVA: 0x0008AC84 File Offset: 0x00088E84
		public void Complete(Socket s)
		{
			this.AcceptedSocket = s;
			this.Complete();
		}

		// Token: 0x06002538 RID: 9528 RVA: 0x0008AC93 File Offset: 0x00088E93
		public void Complete(Socket s, int total)
		{
			this.AcceptedSocket = s;
			this.Total = total;
			this.Complete();
		}

		// Token: 0x04001EA3 RID: 7843
		public Socket socket;

		// Token: 0x04001EA4 RID: 7844
		public SocketOperation operation;

		// Token: 0x04001EA5 RID: 7845
		private Exception DelayedException;

		// Token: 0x04001EA6 RID: 7846
		public EndPoint EndPoint;

		// Token: 0x04001EA7 RID: 7847
		public byte[] Buffer;

		// Token: 0x04001EA8 RID: 7848
		public int Offset;

		// Token: 0x04001EA9 RID: 7849
		public int Size;

		// Token: 0x04001EAA RID: 7850
		public SocketFlags SockFlags;

		// Token: 0x04001EAB RID: 7851
		public Socket AcceptSocket;

		// Token: 0x04001EAC RID: 7852
		public IPAddress[] Addresses;

		// Token: 0x04001EAD RID: 7853
		public int Port;

		// Token: 0x04001EAE RID: 7854
		public IList<ArraySegment<byte>> Buffers;

		// Token: 0x04001EAF RID: 7855
		public bool ReuseSocket;

		// Token: 0x04001EB0 RID: 7856
		public int CurrentAddress;

		// Token: 0x04001EB1 RID: 7857
		public Socket AcceptedSocket;

		// Token: 0x04001EB2 RID: 7858
		public int Total;

		// Token: 0x04001EB3 RID: 7859
		internal int error;

		// Token: 0x04001EB4 RID: 7860
		public int EndCalled;

		// Token: 0x02000483 RID: 1155
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06002539 RID: 9529 RVA: 0x0008ACA9 File Offset: 0x00088EA9
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600253A RID: 9530 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c()
			{
			}

			// Token: 0x0600253B RID: 9531 RVA: 0x0008ACB5 File Offset: 0x00088EB5
			internal void <Complete>b__27_0(object state)
			{
				((SocketAsyncResult)state).AsyncCallback((SocketAsyncResult)state);
			}

			// Token: 0x04001EB5 RID: 7861
			public static readonly SocketAsyncResult.<>c <>9 = new SocketAsyncResult.<>c();

			// Token: 0x04001EB6 RID: 7862
			public static WaitCallback <>9__27_0;
		}
	}
}
