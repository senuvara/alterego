﻿using System;

namespace System.Net.Sockets
{
	/// <summary>The result of a <see cref="M:System.Net.Sockets.SocketTaskExtensions.ReceiveFromAsync(System.Net.Sockets.Socket,System.ArraySegment{System.Byte},System.Net.Sockets.SocketFlags,System.Net.EndPoint)" /> operation.</summary>
	// Token: 0x02000485 RID: 1157
	public struct SocketReceiveFromResult
	{
		/// <summary>The number of bytes received. If the <see cref="M:System.Net.Sockets.SocketTaskExtensions.ReceiveFromAsync(System.Net.Sockets.Socket,System.ArraySegment{System.Byte},System.Net.Sockets.SocketFlags,System.Net.EndPoint)" /> operation was unsuccessful, then 0</summary>
		// Token: 0x04001EC4 RID: 7876
		public int ReceivedBytes;

		/// <summary>The source <see cref="T:System.Net.EndPoint" />.</summary>
		// Token: 0x04001EC5 RID: 7877
		public EndPoint RemoteEndPoint;
	}
}
