﻿using System;

namespace System.Net.Sockets
{
	/// <summary>Specifies the protocols that the <see cref="T:System.Net.Sockets.Socket" /> class supports.</summary>
	// Token: 0x02000460 RID: 1120
	public enum ProtocolType
	{
		/// <summary>Internet Protocol.</summary>
		// Token: 0x04001D6B RID: 7531
		IP,
		/// <summary>IPv6 Hop by Hop Options header.</summary>
		// Token: 0x04001D6C RID: 7532
		IPv6HopByHopOptions = 0,
		/// <summary>Internet Control Message Protocol.</summary>
		// Token: 0x04001D6D RID: 7533
		Icmp,
		/// <summary>Internet Group Management Protocol.</summary>
		// Token: 0x04001D6E RID: 7534
		Igmp,
		/// <summary>Gateway To Gateway Protocol.</summary>
		// Token: 0x04001D6F RID: 7535
		Ggp,
		/// <summary>Internet Protocol version 4.</summary>
		// Token: 0x04001D70 RID: 7536
		IPv4,
		/// <summary>Transmission Control Protocol.</summary>
		// Token: 0x04001D71 RID: 7537
		Tcp = 6,
		/// <summary>PARC Universal Packet Protocol.</summary>
		// Token: 0x04001D72 RID: 7538
		Pup = 12,
		/// <summary>User Datagram Protocol.</summary>
		// Token: 0x04001D73 RID: 7539
		Udp = 17,
		/// <summary>Internet Datagram Protocol.</summary>
		// Token: 0x04001D74 RID: 7540
		Idp = 22,
		/// <summary>Internet Protocol version 6 (IPv6). </summary>
		// Token: 0x04001D75 RID: 7541
		IPv6 = 41,
		/// <summary>IPv6 Routing header.</summary>
		// Token: 0x04001D76 RID: 7542
		IPv6RoutingHeader = 43,
		/// <summary>IPv6 Fragment header.</summary>
		// Token: 0x04001D77 RID: 7543
		IPv6FragmentHeader,
		/// <summary>IPv6 Encapsulating Security Payload header.</summary>
		// Token: 0x04001D78 RID: 7544
		IPSecEncapsulatingSecurityPayload = 50,
		/// <summary>IPv6 Authentication header. For details, see RFC 2292 section 2.2.1, available at http://www.ietf.org.</summary>
		// Token: 0x04001D79 RID: 7545
		IPSecAuthenticationHeader,
		/// <summary>Internet Control Message Protocol for IPv6.</summary>
		// Token: 0x04001D7A RID: 7546
		IcmpV6 = 58,
		/// <summary>IPv6 No next header.</summary>
		// Token: 0x04001D7B RID: 7547
		IPv6NoNextHeader,
		/// <summary>IPv6 Destination Options header.</summary>
		// Token: 0x04001D7C RID: 7548
		IPv6DestinationOptions,
		/// <summary>Net Disk Protocol (unofficial).</summary>
		// Token: 0x04001D7D RID: 7549
		ND = 77,
		/// <summary>Raw IP packet protocol.</summary>
		// Token: 0x04001D7E RID: 7550
		Raw = 255,
		/// <summary>Unspecified protocol.</summary>
		// Token: 0x04001D7F RID: 7551
		Unspecified = 0,
		/// <summary>Internet Packet Exchange Protocol.</summary>
		// Token: 0x04001D80 RID: 7552
		Ipx = 1000,
		/// <summary>Sequenced Packet Exchange protocol.</summary>
		// Token: 0x04001D81 RID: 7553
		Spx = 1256,
		/// <summary>Sequenced Packet Exchange version 2 protocol.</summary>
		// Token: 0x04001D82 RID: 7554
		SpxII,
		/// <summary>Unknown protocol.</summary>
		// Token: 0x04001D83 RID: 7555
		Unknown = -1
	}
}
