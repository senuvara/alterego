﻿using System;

namespace System.Net.Sockets
{
	/// <summary>The type of asynchronous socket operation most recently performed with this context object.</summary>
	// Token: 0x0200046B RID: 1131
	public enum SocketAsyncOperation
	{
		/// <summary>None of the socket operations.</summary>
		// Token: 0x04001DC2 RID: 7618
		None,
		/// <summary>A socket Accept operation. </summary>
		// Token: 0x04001DC3 RID: 7619
		Accept,
		/// <summary>A socket Connect operation.</summary>
		// Token: 0x04001DC4 RID: 7620
		Connect,
		/// <summary>A socket Disconnect operation.</summary>
		// Token: 0x04001DC5 RID: 7621
		Disconnect,
		/// <summary>A socket Receive operation.</summary>
		// Token: 0x04001DC6 RID: 7622
		Receive,
		/// <summary>A socket ReceiveFrom operation.</summary>
		// Token: 0x04001DC7 RID: 7623
		ReceiveFrom,
		/// <summary>A socket ReceiveMessageFrom operation.</summary>
		// Token: 0x04001DC8 RID: 7624
		ReceiveMessageFrom,
		/// <summary>A socket Send operation.</summary>
		// Token: 0x04001DC9 RID: 7625
		Send,
		/// <summary>A socket SendPackets operation.</summary>
		// Token: 0x04001DCA RID: 7626
		SendPackets,
		/// <summary>A socket SendTo operation.</summary>
		// Token: 0x04001DCB RID: 7627
		SendTo
	}
}
