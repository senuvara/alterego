﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using Microsoft.Win32.SafeHandles;

namespace System.Net.Sockets
{
	// Token: 0x02000480 RID: 1152
	internal sealed class SafeSocketHandle : SafeHandleZeroOrMinusOneIsInvalid
	{
		// Token: 0x060024EC RID: 9452 RVA: 0x0008A335 File Offset: 0x00088535
		public SafeSocketHandle(IntPtr preexistingHandle, bool ownsHandle) : base(ownsHandle)
		{
			base.SetHandle(preexistingHandle);
			if (SafeSocketHandle.THROW_ON_ABORT_RETRIES)
			{
				this.threads_stacktraces = new Dictionary<Thread, StackTrace>();
			}
		}

		// Token: 0x060024ED RID: 9453 RVA: 0x00009284 File Offset: 0x00007484
		internal SafeSocketHandle() : base(true)
		{
		}

		// Token: 0x060024EE RID: 9454 RVA: 0x0008A358 File Offset: 0x00088558
		protected override bool ReleaseHandle()
		{
			int num = 0;
			Socket.Blocking_internal(this.handle, false, out num);
			Socket.Shutdown_internal(this.handle, SocketShutdown.Both, out num);
			if (this.blocking_threads != null)
			{
				List<Thread> obj = this.blocking_threads;
				lock (obj)
				{
					int num2 = 0;
					while (this.blocking_threads.Count > 0)
					{
						if (num2++ >= 10)
						{
							if (SafeSocketHandle.THROW_ON_ABORT_RETRIES)
							{
								StringBuilder stringBuilder = new StringBuilder();
								stringBuilder.AppendLine("Could not abort registered blocking threads before closing socket.");
								foreach (Thread key in this.blocking_threads)
								{
									stringBuilder.AppendLine("Thread StackTrace:");
									stringBuilder.AppendLine(this.threads_stacktraces[key].ToString());
								}
								stringBuilder.AppendLine();
								throw new Exception(stringBuilder.ToString());
							}
							break;
						}
						else
						{
							if (this.blocking_threads.Count == 1 && this.blocking_threads[0] == Thread.CurrentThread)
							{
								break;
							}
							foreach (Thread thread in this.blocking_threads)
							{
								Socket.cancel_blocking_socket_operation(thread);
							}
							this.in_cleanup = true;
							Monitor.Wait(this.blocking_threads, 100);
						}
					}
				}
			}
			Socket.Close_internal(this.handle, out num);
			return num == 0;
		}

		// Token: 0x060024EF RID: 9455 RVA: 0x0008A524 File Offset: 0x00088724
		public void RegisterForBlockingSyscall()
		{
			if (this.blocking_threads == null)
			{
				Interlocked.CompareExchange<List<Thread>>(ref this.blocking_threads, new List<Thread>(), null);
			}
			bool flag = false;
			try
			{
				base.DangerousAddRef(ref flag);
			}
			finally
			{
				List<Thread> obj = this.blocking_threads;
				lock (obj)
				{
					this.blocking_threads.Add(Thread.CurrentThread);
					if (SafeSocketHandle.THROW_ON_ABORT_RETRIES)
					{
						this.threads_stacktraces.Add(Thread.CurrentThread, new StackTrace(true));
					}
				}
				if (flag)
				{
					base.DangerousRelease();
				}
				if (base.IsClosed)
				{
					throw new SocketException(10004);
				}
			}
		}

		// Token: 0x060024F0 RID: 9456 RVA: 0x0008A5DC File Offset: 0x000887DC
		public void UnRegisterForBlockingSyscall()
		{
			List<Thread> obj = this.blocking_threads;
			lock (obj)
			{
				Thread currentThread = Thread.CurrentThread;
				this.blocking_threads.Remove(currentThread);
				if (SafeSocketHandle.THROW_ON_ABORT_RETRIES && this.blocking_threads.IndexOf(currentThread) == -1)
				{
					this.threads_stacktraces.Remove(currentThread);
				}
				if (this.in_cleanup && this.blocking_threads.Count == 0)
				{
					Monitor.Pulse(this.blocking_threads);
				}
			}
		}

		// Token: 0x060024F1 RID: 9457 RVA: 0x0008A66C File Offset: 0x0008886C
		// Note: this type is marked as 'beforefieldinit'.
		static SafeSocketHandle()
		{
		}

		// Token: 0x04001E86 RID: 7814
		private List<Thread> blocking_threads;

		// Token: 0x04001E87 RID: 7815
		private Dictionary<Thread, StackTrace> threads_stacktraces;

		// Token: 0x04001E88 RID: 7816
		private bool in_cleanup;

		// Token: 0x04001E89 RID: 7817
		private const int SOCKET_CLOSED = 10004;

		// Token: 0x04001E8A RID: 7818
		private const int ABORT_RETRIES = 10;

		// Token: 0x04001E8B RID: 7819
		private static bool THROW_ON_ABORT_RETRIES = Environment.GetEnvironmentVariable("MONO_TESTS_IN_PROGRESS") == "yes";
	}
}
