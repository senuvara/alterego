﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Net.Sockets
{
	/// <summary>The exception that is thrown when a socket error occurs.</summary>
	// Token: 0x02000456 RID: 1110
	[Serializable]
	public class SocketException : Win32Exception
	{
		// Token: 0x060022D0 RID: 8912
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int WSAGetLastError_internal();

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Sockets.SocketException" /> class with the last operating system error code.</summary>
		// Token: 0x060022D1 RID: 8913 RVA: 0x000825BD File Offset: 0x000807BD
		public SocketException() : base(SocketException.WSAGetLastError_internal())
		{
		}

		// Token: 0x060022D2 RID: 8914 RVA: 0x0004FBC2 File Offset: 0x0004DDC2
		internal SocketException(int error, string message) : base(error, message)
		{
		}

		// Token: 0x060022D3 RID: 8915 RVA: 0x000825CA File Offset: 0x000807CA
		internal SocketException(EndPoint endPoint) : base(Marshal.GetLastWin32Error())
		{
			this.m_EndPoint = endPoint;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Sockets.SocketException" /> class with the specified error code.</summary>
		/// <param name="errorCode">The error code that indicates the error that occurred. </param>
		// Token: 0x060022D4 RID: 8916 RVA: 0x0004FBB9 File Offset: 0x0004DDB9
		public SocketException(int errorCode) : base(errorCode)
		{
		}

		// Token: 0x060022D5 RID: 8917 RVA: 0x000825DE File Offset: 0x000807DE
		internal SocketException(int errorCode, EndPoint endPoint) : base(errorCode)
		{
			this.m_EndPoint = endPoint;
		}

		// Token: 0x060022D6 RID: 8918 RVA: 0x0004FBB9 File Offset: 0x0004DDB9
		internal SocketException(SocketError socketError) : base((int)socketError)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.Sockets.SocketException" /> class from the specified instances of the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> and <see cref="T:System.Runtime.Serialization.StreamingContext" /> classes.</summary>
		/// <param name="serializationInfo">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> instance that contains the information that is required to serialize the new <see cref="T:System.Net.Sockets.SocketException" /> instance. </param>
		/// <param name="streamingContext">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains the source of the serialized stream that is associated with the new <see cref="T:System.Net.Sockets.SocketException" /> instance. </param>
		// Token: 0x060022D7 RID: 8919 RVA: 0x0004FBCC File Offset: 0x0004DDCC
		protected SocketException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
		{
		}

		/// <summary>Gets the error code that is associated with this exception.</summary>
		/// <returns>An integer error code that is associated with this exception.</returns>
		// Token: 0x17000780 RID: 1920
		// (get) Token: 0x060022D8 RID: 8920 RVA: 0x0004FBD6 File Offset: 0x0004DDD6
		public override int ErrorCode
		{
			get
			{
				return base.NativeErrorCode;
			}
		}

		/// <summary>Gets the error message that is associated with this exception.</summary>
		/// <returns>A string that contains the error message. </returns>
		// Token: 0x17000781 RID: 1921
		// (get) Token: 0x060022D9 RID: 8921 RVA: 0x000825EE File Offset: 0x000807EE
		public override string Message
		{
			get
			{
				if (this.m_EndPoint == null)
				{
					return base.Message;
				}
				return base.Message + " " + this.m_EndPoint.ToString();
			}
		}

		/// <summary>Gets the error code that is associated with this exception.</summary>
		/// <returns>An integer error code that is associated with this exception.</returns>
		// Token: 0x17000782 RID: 1922
		// (get) Token: 0x060022DA RID: 8922 RVA: 0x0004FBD6 File Offset: 0x0004DDD6
		public SocketError SocketErrorCode
		{
			get
			{
				return (SocketError)base.NativeErrorCode;
			}
		}

		// Token: 0x04001CF0 RID: 7408
		[NonSerialized]
		private EndPoint m_EndPoint;
	}
}
