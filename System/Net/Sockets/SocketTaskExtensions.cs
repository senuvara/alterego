﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace System.Net.Sockets
{
	/// <summary>This class contains extension methods to the <see cref="T:System.Net.Sockets.Socket" /> class.</summary>
	// Token: 0x02000487 RID: 1159
	public static class SocketTaskExtensions
	{
		/// <summary>Performs an asynchronous operation on to accept an incoming connection attempt on the socket.</summary>
		/// <param name="socket">The socket that is listening for connections.</param>
		/// <returns>An asynchronous task that completes with a <see cref="T:System.Net.Sockets.Socket" /> to handle communication with the remote host.</returns>
		// Token: 0x0600253C RID: 9532 RVA: 0x0008ACD0 File Offset: 0x00088ED0
		public static Task<Socket> AcceptAsync(this Socket socket)
		{
			return Task<Socket>.Factory.FromAsync((AsyncCallback callback, object state) => ((Socket)state).BeginAccept(callback, state), (IAsyncResult asyncResult) => ((Socket)asyncResult.AsyncState).EndAccept(asyncResult), socket);
		}

		/// <summary>Performs an asynchronous operation on to accept an incoming connection attempt on the socket.</summary>
		/// <param name="socket">The socket that is listening for incoming connections.</param>
		/// <param name="acceptSocket">The accepted <see cref="T:System.Net.Sockets.Socket" /> object. This value may be <see langword="null" />.</param>
		/// <returns>An asynchronous task that completes with a <see cref="T:System.Net.Sockets.Socket" /> to handle communication with the remote host.</returns>
		// Token: 0x0600253D RID: 9533 RVA: 0x0008AD28 File Offset: 0x00088F28
		public static Task<Socket> AcceptAsync(this Socket socket, Socket acceptSocket)
		{
			return Task<Socket>.Factory.FromAsync<Socket, int>((Socket socketForAccept, int receiveSize, AsyncCallback callback, object state) => ((Socket)state).BeginAccept(socketForAccept, receiveSize, callback, state), (IAsyncResult asyncResult) => ((Socket)asyncResult.AsyncState).EndAccept(asyncResult), acceptSocket, 0, socket);
		}

		/// <summary>Establishes a connection to a remote host.</summary>
		/// <param name="socket">The socket that is used for establishing a connection.</param>
		/// <param name="remoteEP">An EndPoint that represents the remote device.</param>
		/// <returns>An asynchronous Task.</returns>
		// Token: 0x0600253E RID: 9534 RVA: 0x0008AD80 File Offset: 0x00088F80
		public static Task ConnectAsync(this Socket socket, EndPoint remoteEP)
		{
			return Task.Factory.FromAsync<EndPoint>((EndPoint targetEndPoint, AsyncCallback callback, object state) => ((Socket)state).BeginConnect(targetEndPoint, callback, state), delegate(IAsyncResult asyncResult)
			{
				((Socket)asyncResult.AsyncState).EndConnect(asyncResult);
			}, remoteEP, socket);
		}

		/// <summary>Establishes a connection to a remote host. The host is specified by an IP address and a port number.</summary>
		/// <param name="socket">The socket to perform the connect operation on.</param>
		/// <param name="address">The IP address of the remote host.</param>
		/// <param name="port">The port number of the remote host.</param>
		// Token: 0x0600253F RID: 9535 RVA: 0x0008ADD8 File Offset: 0x00088FD8
		public static Task ConnectAsync(this Socket socket, IPAddress address, int port)
		{
			return Task.Factory.FromAsync<IPAddress, int>((IPAddress targetAddress, int targetPort, AsyncCallback callback, object state) => ((Socket)state).BeginConnect(targetAddress, targetPort, callback, state), delegate(IAsyncResult asyncResult)
			{
				((Socket)asyncResult.AsyncState).EndConnect(asyncResult);
			}, address, port, socket);
		}

		/// <summary>Establishes a connection to a remote host. The host is specified by an array of IP addresses and a port number.</summary>
		/// <param name="socket">The socket that the connect operation is performed on.</param>
		/// <param name="addresses">The IP addresses of the remote host.</param>
		/// <param name="port">The port number of the remote host.</param>
		/// <returns>A task that represents the asynchronous connect operation. </returns>
		// Token: 0x06002540 RID: 9536 RVA: 0x0008AE30 File Offset: 0x00089030
		public static Task ConnectAsync(this Socket socket, IPAddress[] addresses, int port)
		{
			return Task.Factory.FromAsync<IPAddress[], int>((IPAddress[] targetAddresses, int targetPort, AsyncCallback callback, object state) => ((Socket)state).BeginConnect(targetAddresses, targetPort, callback, state), delegate(IAsyncResult asyncResult)
			{
				((Socket)asyncResult.AsyncState).EndConnect(asyncResult);
			}, addresses, port, socket);
		}

		/// <summary>Establishes a connection to a remote host. The host is specified by a host name and a port number.</summary>
		/// <param name="socket">The socket to perform the connect operation on.</param>
		/// <param name="host">The name of the remote host.</param>
		/// <param name="port">The port number of the remote host.</param>
		/// <returns>Returns an asynchronous Task.</returns>
		// Token: 0x06002541 RID: 9537 RVA: 0x0008AE88 File Offset: 0x00089088
		public static Task ConnectAsync(this Socket socket, string host, int port)
		{
			return Task.Factory.FromAsync<string, int>((string targetHost, int targetPort, AsyncCallback callback, object state) => ((Socket)state).BeginConnect(targetHost, targetPort, callback, state), delegate(IAsyncResult asyncResult)
			{
				((Socket)asyncResult.AsyncState).EndConnect(asyncResult);
			}, host, port, socket);
		}

		/// <summary>Receive data from a connected socket.</summary>
		/// <param name="socket">The socket to perform the receive operation on.</param>
		/// <param name="buffer">An array that is the storage location for the received data.</param>
		/// <param name="socketFlags">A bitwise combination of the <see cref="T:System.Net.Sockets.SocketFlags" /> values.</param>
		/// <returns>A task that represents the asynchronous receive operation. The value of the <paramref name="TResult" /> parameter contains the number of bytes received.</returns>
		// Token: 0x06002542 RID: 9538 RVA: 0x0008AEE0 File Offset: 0x000890E0
		public static Task<int> ReceiveAsync(this Socket socket, ArraySegment<byte> buffer, SocketFlags socketFlags)
		{
			return Task<int>.Factory.FromAsync<ArraySegment<byte>, SocketFlags>((ArraySegment<byte> targetBuffer, SocketFlags flags, AsyncCallback callback, object state) => ((Socket)state).BeginReceive(targetBuffer.Array, targetBuffer.Offset, targetBuffer.Count, flags, callback, state), (IAsyncResult asyncResult) => ((Socket)asyncResult.AsyncState).EndReceive(asyncResult), buffer, socketFlags, socket);
		}

		/// <summary>Receives data from a connected socket.</summary>
		/// <param name="socket">The socket to perform the receive operation on.</param>
		/// <param name="buffers">An array that is the storage location for the received data.</param>
		/// <param name="socketFlags">A bitwise combination of the <see cref="T:System.Net.Sockets.SocketFlags" /> values.</param>
		/// <returns>A task that represents the asynchronous receive operation. The value of the <paramref name="TResult" /> parameter contains the number of bytes received.</returns>
		// Token: 0x06002543 RID: 9539 RVA: 0x0008AF38 File Offset: 0x00089138
		public static Task<int> ReceiveAsync(this Socket socket, IList<ArraySegment<byte>> buffers, SocketFlags socketFlags)
		{
			return Task<int>.Factory.FromAsync<IList<ArraySegment<byte>>, SocketFlags>((IList<ArraySegment<byte>> targetBuffers, SocketFlags flags, AsyncCallback callback, object state) => ((Socket)state).BeginReceive(targetBuffers, flags, callback, state), (IAsyncResult asyncResult) => ((Socket)asyncResult.AsyncState).EndReceive(asyncResult), buffers, socketFlags, socket);
		}

		/// <summary>Receive data from a specified network device.</summary>
		/// <param name="socket">The socket to perform the ReceiveFrom operation on.</param>
		/// <param name="buffer">An array of type Byte that is the storage location for the received data.</param>
		/// <param name="socketFlags">A bitwise combination of the <see cref="T:System.Net.Sockets.SocketFlags" /> values.</param>
		/// <param name="remoteEndPoint">An EndPoint that represents the source of the data.</param>
		/// <returns>An asynchronous Task that completes with a SocketReceiveFromResult struct.</returns>
		// Token: 0x06002544 RID: 9540 RVA: 0x0008AF90 File Offset: 0x00089190
		public static Task<SocketReceiveFromResult> ReceiveFromAsync(this Socket socket, ArraySegment<byte> buffer, SocketFlags socketFlags, EndPoint remoteEndPoint)
		{
			object[] state2 = new object[]
			{
				socket,
				remoteEndPoint
			};
			return Task<SocketReceiveFromResult>.Factory.FromAsync<ArraySegment<byte>, SocketFlags>(delegate(ArraySegment<byte> targetBuffer, SocketFlags flags, AsyncCallback callback, object state)
			{
				object[] array = (object[])state;
				Socket socket2 = (Socket)array[0];
				EndPoint endPoint = (EndPoint)array[1];
				IAsyncResult result = socket2.BeginReceiveFrom(targetBuffer.Array, targetBuffer.Offset, targetBuffer.Count, flags, ref endPoint, callback, state);
				array[1] = endPoint;
				return result;
			}, delegate(IAsyncResult asyncResult)
			{
				object[] array = (object[])asyncResult.AsyncState;
				Socket socket2 = (Socket)array[0];
				EndPoint remoteEndPoint2 = (EndPoint)array[1];
				int receivedBytes = socket2.EndReceiveFrom(asyncResult, ref remoteEndPoint2);
				return new SocketReceiveFromResult
				{
					ReceivedBytes = receivedBytes,
					RemoteEndPoint = remoteEndPoint2
				};
			}, buffer, socketFlags, state2);
		}

		/// <summary>Receives the specified number of bytes of data into the specified location of the data buffer, using the specified <see cref="T:System.Net.Sockets.SocketFlags" />, and stores the endpoint and packet information.</summary>
		/// <param name="socket">The socket to perform the operation on.</param>
		/// <param name="buffer">An array that is the storage location for received data.</param>
		/// <param name="socketFlags">A bitwise combination of the <see cref="T:System.Net.Sockets.SocketFlags" /> values.</param>
		/// <param name="remoteEndPoint">An <see cref="T:System.Net.EndPoint" />, that represents the remote server.</param>
		/// <returns>An asynchronous Task that completes with a <see cref="T:System.Net.Sockets.SocketReceiveMessageFromResult" /> struct.</returns>
		// Token: 0x06002545 RID: 9541 RVA: 0x0008AFF8 File Offset: 0x000891F8
		public static Task<SocketReceiveMessageFromResult> ReceiveMessageFromAsync(this Socket socket, ArraySegment<byte> buffer, SocketFlags socketFlags, EndPoint remoteEndPoint)
		{
			object[] state2 = new object[]
			{
				socket,
				socketFlags,
				remoteEndPoint
			};
			return Task<SocketReceiveMessageFromResult>.Factory.FromAsync<ArraySegment<byte>>(delegate(ArraySegment<byte> targetBuffer, AsyncCallback callback, object state)
			{
				object[] array = (object[])state;
				Socket socket2 = (Socket)array[0];
				SocketFlags socketFlags2 = (SocketFlags)array[1];
				EndPoint endPoint = (EndPoint)array[2];
				IAsyncResult result = socket2.BeginReceiveMessageFrom(targetBuffer.Array, targetBuffer.Offset, targetBuffer.Count, socketFlags2, ref endPoint, callback, state);
				array[2] = endPoint;
				return result;
			}, delegate(IAsyncResult asyncResult)
			{
				object[] array = (object[])asyncResult.AsyncState;
				Socket socket2 = (Socket)array[0];
				SocketFlags socketFlags2 = (SocketFlags)array[1];
				EndPoint remoteEndPoint2 = (EndPoint)array[2];
				IPPacketInformation packetInformation;
				int receivedBytes = socket2.EndReceiveMessageFrom(asyncResult, ref socketFlags2, ref remoteEndPoint2, out packetInformation);
				return new SocketReceiveMessageFromResult
				{
					PacketInformation = packetInformation,
					ReceivedBytes = receivedBytes,
					RemoteEndPoint = remoteEndPoint2,
					SocketFlags = socketFlags2
				};
			}, buffer, state2);
		}

		/// <summary>Sends data to a connected socket.</summary>
		/// <param name="socket">The socket to perform the operation on.</param>
		/// <param name="buffer">An array of type Byte that contains the data to send.</param>
		/// <param name="socketFlags">A bitwise combination of the <see cref="T:System.Net.Sockets.SocketFlags" /> values.</param>
		/// <returns>An asynchronous task that completes with number of bytes sent to the socket if the operation was successful. Otherwise, the task will complete with an invalid socket error.</returns>
		// Token: 0x06002546 RID: 9542 RVA: 0x0008B068 File Offset: 0x00089268
		public static Task<int> SendAsync(this Socket socket, ArraySegment<byte> buffer, SocketFlags socketFlags)
		{
			return Task<int>.Factory.FromAsync<ArraySegment<byte>, SocketFlags>((ArraySegment<byte> targetBuffer, SocketFlags flags, AsyncCallback callback, object state) => ((Socket)state).BeginSend(targetBuffer.Array, targetBuffer.Offset, targetBuffer.Count, flags, callback, state), (IAsyncResult asyncResult) => ((Socket)asyncResult.AsyncState).EndSend(asyncResult), buffer, socketFlags, socket);
		}

		/// <summary>Sends data to a connected socket.</summary>
		/// <param name="socket">The socket to perform the operation on.</param>
		/// <param name="buffers">An array that contains the data to send.</param>
		/// <param name="socketFlags">A bitwise combination of the <see cref="T:System.Net.Sockets.SocketFlags" /> values.</param>
		/// <returns>An asynchronous task that completes with number of bytes sent to the socket if the operation was successful. Otherwise, the task will complete with an invalid socket error.</returns>
		// Token: 0x06002547 RID: 9543 RVA: 0x0008B0C0 File Offset: 0x000892C0
		public static Task<int> SendAsync(this Socket socket, IList<ArraySegment<byte>> buffers, SocketFlags socketFlags)
		{
			return Task<int>.Factory.FromAsync<IList<ArraySegment<byte>>, SocketFlags>((IList<ArraySegment<byte>> targetBuffers, SocketFlags flags, AsyncCallback callback, object state) => ((Socket)state).BeginSend(targetBuffers, flags, callback, state), (IAsyncResult asyncResult) => ((Socket)asyncResult.AsyncState).EndSend(asyncResult), buffers, socketFlags, socket);
		}

		/// <summary>Sends data asynchronously to a specific remote host.</summary>
		/// <param name="socket">The socket to perform the operation on.</param>
		/// <param name="buffer">An array that contains the data to send.</param>
		/// <param name="socketFlags">A bitwise combination of the <see cref="T:System.Net.Sockets.SocketFlags" /> values.</param>
		/// <param name="remoteEP">An <see cref="T:System.Net.EndPoint" /> that represents the remote device.</param>
		/// <returns>An asynchronous task that completes with number of bytes sent if the operation was successful. Otherwise, the task will complete with an invalid socket error.</returns>
		// Token: 0x06002548 RID: 9544 RVA: 0x0008B118 File Offset: 0x00089318
		public static Task<int> SendToAsync(this Socket socket, ArraySegment<byte> buffer, SocketFlags socketFlags, EndPoint remoteEP)
		{
			return Task<int>.Factory.FromAsync<ArraySegment<byte>, SocketFlags, EndPoint>((ArraySegment<byte> targetBuffer, SocketFlags flags, EndPoint endPoint, AsyncCallback callback, object state) => ((Socket)state).BeginSendTo(targetBuffer.Array, targetBuffer.Offset, targetBuffer.Count, flags, endPoint, callback, state), (IAsyncResult asyncResult) => ((Socket)asyncResult.AsyncState).EndSendTo(asyncResult), buffer, socketFlags, remoteEP, socket);
		}

		// Token: 0x02000488 RID: 1160
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06002549 RID: 9545 RVA: 0x0008B171 File Offset: 0x00089371
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600254A RID: 9546 RVA: 0x0000232F File Offset: 0x0000052F
			public <>c()
			{
			}

			// Token: 0x0600254B RID: 9547 RVA: 0x0008B17D File Offset: 0x0008937D
			internal IAsyncResult <AcceptAsync>b__0_0(AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginAccept(callback, state);
			}

			// Token: 0x0600254C RID: 9548 RVA: 0x0008B18C File Offset: 0x0008938C
			internal Socket <AcceptAsync>b__0_1(IAsyncResult asyncResult)
			{
				return ((Socket)asyncResult.AsyncState).EndAccept(asyncResult);
			}

			// Token: 0x0600254D RID: 9549 RVA: 0x0008B19F File Offset: 0x0008939F
			internal IAsyncResult <AcceptAsync>b__1_0(Socket socketForAccept, int receiveSize, AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginAccept(socketForAccept, receiveSize, callback, state);
			}

			// Token: 0x0600254E RID: 9550 RVA: 0x0008B18C File Offset: 0x0008938C
			internal Socket <AcceptAsync>b__1_1(IAsyncResult asyncResult)
			{
				return ((Socket)asyncResult.AsyncState).EndAccept(asyncResult);
			}

			// Token: 0x0600254F RID: 9551 RVA: 0x0008B1B2 File Offset: 0x000893B2
			internal IAsyncResult <ConnectAsync>b__2_0(EndPoint targetEndPoint, AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginConnect(targetEndPoint, callback, state);
			}

			// Token: 0x06002550 RID: 9552 RVA: 0x0008B1C2 File Offset: 0x000893C2
			internal void <ConnectAsync>b__2_1(IAsyncResult asyncResult)
			{
				((Socket)asyncResult.AsyncState).EndConnect(asyncResult);
			}

			// Token: 0x06002551 RID: 9553 RVA: 0x0008B1D5 File Offset: 0x000893D5
			internal IAsyncResult <ConnectAsync>b__3_0(IPAddress targetAddress, int targetPort, AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginConnect(targetAddress, targetPort, callback, state);
			}

			// Token: 0x06002552 RID: 9554 RVA: 0x0008B1C2 File Offset: 0x000893C2
			internal void <ConnectAsync>b__3_1(IAsyncResult asyncResult)
			{
				((Socket)asyncResult.AsyncState).EndConnect(asyncResult);
			}

			// Token: 0x06002553 RID: 9555 RVA: 0x0008B1E8 File Offset: 0x000893E8
			internal IAsyncResult <ConnectAsync>b__4_0(IPAddress[] targetAddresses, int targetPort, AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginConnect(targetAddresses, targetPort, callback, state);
			}

			// Token: 0x06002554 RID: 9556 RVA: 0x0008B1C2 File Offset: 0x000893C2
			internal void <ConnectAsync>b__4_1(IAsyncResult asyncResult)
			{
				((Socket)asyncResult.AsyncState).EndConnect(asyncResult);
			}

			// Token: 0x06002555 RID: 9557 RVA: 0x0008B1FB File Offset: 0x000893FB
			internal IAsyncResult <ConnectAsync>b__5_0(string targetHost, int targetPort, AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginConnect(targetHost, targetPort, callback, state);
			}

			// Token: 0x06002556 RID: 9558 RVA: 0x0008B1C2 File Offset: 0x000893C2
			internal void <ConnectAsync>b__5_1(IAsyncResult asyncResult)
			{
				((Socket)asyncResult.AsyncState).EndConnect(asyncResult);
			}

			// Token: 0x06002557 RID: 9559 RVA: 0x0008B20E File Offset: 0x0008940E
			internal IAsyncResult <ReceiveAsync>b__6_0(ArraySegment<byte> targetBuffer, SocketFlags flags, AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginReceive(targetBuffer.Array, targetBuffer.Offset, targetBuffer.Count, flags, callback, state);
			}

			// Token: 0x06002558 RID: 9560 RVA: 0x0008B235 File Offset: 0x00089435
			internal int <ReceiveAsync>b__6_1(IAsyncResult asyncResult)
			{
				return ((Socket)asyncResult.AsyncState).EndReceive(asyncResult);
			}

			// Token: 0x06002559 RID: 9561 RVA: 0x0008B248 File Offset: 0x00089448
			internal IAsyncResult <ReceiveAsync>b__7_0(IList<ArraySegment<byte>> targetBuffers, SocketFlags flags, AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginReceive(targetBuffers, flags, callback, state);
			}

			// Token: 0x0600255A RID: 9562 RVA: 0x0008B235 File Offset: 0x00089435
			internal int <ReceiveAsync>b__7_1(IAsyncResult asyncResult)
			{
				return ((Socket)asyncResult.AsyncState).EndReceive(asyncResult);
			}

			// Token: 0x0600255B RID: 9563 RVA: 0x0008B25C File Offset: 0x0008945C
			internal IAsyncResult <ReceiveFromAsync>b__8_0(ArraySegment<byte> targetBuffer, SocketFlags flags, AsyncCallback callback, object state)
			{
				object[] array = (object[])state;
				Socket socket = (Socket)array[0];
				EndPoint endPoint = (EndPoint)array[1];
				IAsyncResult result = socket.BeginReceiveFrom(targetBuffer.Array, targetBuffer.Offset, targetBuffer.Count, flags, ref endPoint, callback, state);
				array[1] = endPoint;
				return result;
			}

			// Token: 0x0600255C RID: 9564 RVA: 0x0008B2A8 File Offset: 0x000894A8
			internal SocketReceiveFromResult <ReceiveFromAsync>b__8_1(IAsyncResult asyncResult)
			{
				object[] array = (object[])asyncResult.AsyncState;
				Socket socket = (Socket)array[0];
				EndPoint remoteEndPoint = (EndPoint)array[1];
				int receivedBytes = socket.EndReceiveFrom(asyncResult, ref remoteEndPoint);
				return new SocketReceiveFromResult
				{
					ReceivedBytes = receivedBytes,
					RemoteEndPoint = remoteEndPoint
				};
			}

			// Token: 0x0600255D RID: 9565 RVA: 0x0008B2F4 File Offset: 0x000894F4
			internal IAsyncResult <ReceiveMessageFromAsync>b__9_0(ArraySegment<byte> targetBuffer, AsyncCallback callback, object state)
			{
				object[] array = (object[])state;
				Socket socket = (Socket)array[0];
				SocketFlags socketFlags = (SocketFlags)array[1];
				EndPoint endPoint = (EndPoint)array[2];
				IAsyncResult result = socket.BeginReceiveMessageFrom(targetBuffer.Array, targetBuffer.Offset, targetBuffer.Count, socketFlags, ref endPoint, callback, state);
				array[2] = endPoint;
				return result;
			}

			// Token: 0x0600255E RID: 9566 RVA: 0x0008B348 File Offset: 0x00089548
			internal SocketReceiveMessageFromResult <ReceiveMessageFromAsync>b__9_1(IAsyncResult asyncResult)
			{
				object[] array = (object[])asyncResult.AsyncState;
				Socket socket = (Socket)array[0];
				SocketFlags socketFlags = (SocketFlags)array[1];
				EndPoint remoteEndPoint = (EndPoint)array[2];
				IPPacketInformation packetInformation;
				int receivedBytes = socket.EndReceiveMessageFrom(asyncResult, ref socketFlags, ref remoteEndPoint, out packetInformation);
				return new SocketReceiveMessageFromResult
				{
					PacketInformation = packetInformation,
					ReceivedBytes = receivedBytes,
					RemoteEndPoint = remoteEndPoint,
					SocketFlags = socketFlags
				};
			}

			// Token: 0x0600255F RID: 9567 RVA: 0x0008B3B4 File Offset: 0x000895B4
			internal IAsyncResult <SendAsync>b__10_0(ArraySegment<byte> targetBuffer, SocketFlags flags, AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginSend(targetBuffer.Array, targetBuffer.Offset, targetBuffer.Count, flags, callback, state);
			}

			// Token: 0x06002560 RID: 9568 RVA: 0x0008B3DB File Offset: 0x000895DB
			internal int <SendAsync>b__10_1(IAsyncResult asyncResult)
			{
				return ((Socket)asyncResult.AsyncState).EndSend(asyncResult);
			}

			// Token: 0x06002561 RID: 9569 RVA: 0x0008B3EE File Offset: 0x000895EE
			internal IAsyncResult <SendAsync>b__11_0(IList<ArraySegment<byte>> targetBuffers, SocketFlags flags, AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginSend(targetBuffers, flags, callback, state);
			}

			// Token: 0x06002562 RID: 9570 RVA: 0x0008B3DB File Offset: 0x000895DB
			internal int <SendAsync>b__11_1(IAsyncResult asyncResult)
			{
				return ((Socket)asyncResult.AsyncState).EndSend(asyncResult);
			}

			// Token: 0x06002563 RID: 9571 RVA: 0x0008B401 File Offset: 0x00089601
			internal IAsyncResult <SendToAsync>b__12_0(ArraySegment<byte> targetBuffer, SocketFlags flags, EndPoint endPoint, AsyncCallback callback, object state)
			{
				return ((Socket)state).BeginSendTo(targetBuffer.Array, targetBuffer.Offset, targetBuffer.Count, flags, endPoint, callback, state);
			}

			// Token: 0x06002564 RID: 9572 RVA: 0x0008B42A File Offset: 0x0008962A
			internal int <SendToAsync>b__12_1(IAsyncResult asyncResult)
			{
				return ((Socket)asyncResult.AsyncState).EndSendTo(asyncResult);
			}

			// Token: 0x04001ECA RID: 7882
			public static readonly SocketTaskExtensions.<>c <>9 = new SocketTaskExtensions.<>c();

			// Token: 0x04001ECB RID: 7883
			public static Func<AsyncCallback, object, IAsyncResult> <>9__0_0;

			// Token: 0x04001ECC RID: 7884
			public static Func<IAsyncResult, Socket> <>9__0_1;

			// Token: 0x04001ECD RID: 7885
			public static Func<Socket, int, AsyncCallback, object, IAsyncResult> <>9__1_0;

			// Token: 0x04001ECE RID: 7886
			public static Func<IAsyncResult, Socket> <>9__1_1;

			// Token: 0x04001ECF RID: 7887
			public static Func<EndPoint, AsyncCallback, object, IAsyncResult> <>9__2_0;

			// Token: 0x04001ED0 RID: 7888
			public static Action<IAsyncResult> <>9__2_1;

			// Token: 0x04001ED1 RID: 7889
			public static Func<IPAddress, int, AsyncCallback, object, IAsyncResult> <>9__3_0;

			// Token: 0x04001ED2 RID: 7890
			public static Action<IAsyncResult> <>9__3_1;

			// Token: 0x04001ED3 RID: 7891
			public static Func<IPAddress[], int, AsyncCallback, object, IAsyncResult> <>9__4_0;

			// Token: 0x04001ED4 RID: 7892
			public static Action<IAsyncResult> <>9__4_1;

			// Token: 0x04001ED5 RID: 7893
			public static Func<string, int, AsyncCallback, object, IAsyncResult> <>9__5_0;

			// Token: 0x04001ED6 RID: 7894
			public static Action<IAsyncResult> <>9__5_1;

			// Token: 0x04001ED7 RID: 7895
			public static Func<ArraySegment<byte>, SocketFlags, AsyncCallback, object, IAsyncResult> <>9__6_0;

			// Token: 0x04001ED8 RID: 7896
			public static Func<IAsyncResult, int> <>9__6_1;

			// Token: 0x04001ED9 RID: 7897
			public static Func<IList<ArraySegment<byte>>, SocketFlags, AsyncCallback, object, IAsyncResult> <>9__7_0;

			// Token: 0x04001EDA RID: 7898
			public static Func<IAsyncResult, int> <>9__7_1;

			// Token: 0x04001EDB RID: 7899
			public static Func<ArraySegment<byte>, SocketFlags, AsyncCallback, object, IAsyncResult> <>9__8_0;

			// Token: 0x04001EDC RID: 7900
			public static Func<IAsyncResult, SocketReceiveFromResult> <>9__8_1;

			// Token: 0x04001EDD RID: 7901
			public static Func<ArraySegment<byte>, AsyncCallback, object, IAsyncResult> <>9__9_0;

			// Token: 0x04001EDE RID: 7902
			public static Func<IAsyncResult, SocketReceiveMessageFromResult> <>9__9_1;

			// Token: 0x04001EDF RID: 7903
			public static Func<ArraySegment<byte>, SocketFlags, AsyncCallback, object, IAsyncResult> <>9__10_0;

			// Token: 0x04001EE0 RID: 7904
			public static Func<IAsyncResult, int> <>9__10_1;

			// Token: 0x04001EE1 RID: 7905
			public static Func<IList<ArraySegment<byte>>, SocketFlags, AsyncCallback, object, IAsyncResult> <>9__11_0;

			// Token: 0x04001EE2 RID: 7906
			public static Func<IAsyncResult, int> <>9__11_1;

			// Token: 0x04001EE3 RID: 7907
			public static Func<ArraySegment<byte>, SocketFlags, EndPoint, AsyncCallback, object, IAsyncResult> <>9__12_0;

			// Token: 0x04001EE4 RID: 7908
			public static Func<IAsyncResult, int> <>9__12_1;
		}
	}
}
