﻿using System;

namespace System.Net.Sockets
{
	/// <summary>Specifies socket send and receive behaviors.</summary>
	// Token: 0x0200046F RID: 1135
	[Flags]
	public enum SocketFlags
	{
		/// <summary>Use no flags for this call.</summary>
		// Token: 0x04001E05 RID: 7685
		None = 0,
		/// <summary>Process out-of-band data.</summary>
		// Token: 0x04001E06 RID: 7686
		OutOfBand = 1,
		/// <summary>Peek at the incoming message.</summary>
		// Token: 0x04001E07 RID: 7687
		Peek = 2,
		/// <summary>Send without using routing tables.</summary>
		// Token: 0x04001E08 RID: 7688
		DontRoute = 4,
		/// <summary>Provides a standard value for the number of WSABUF structures that are used to send and receive data. This value is not used or supported on .NET Framework 4.5.</summary>
		// Token: 0x04001E09 RID: 7689
		MaxIOVectorLength = 16,
		/// <summary>The message was too large to fit into the specified buffer and was truncated.</summary>
		// Token: 0x04001E0A RID: 7690
		Truncated = 256,
		/// <summary>Indicates that the control data did not fit into an internal 64-KB buffer and was truncated.</summary>
		// Token: 0x04001E0B RID: 7691
		ControlDataTruncated = 512,
		/// <summary>Indicates a broadcast packet.</summary>
		// Token: 0x04001E0C RID: 7692
		Broadcast = 1024,
		/// <summary>Indicates a multicast packet.</summary>
		// Token: 0x04001E0D RID: 7693
		Multicast = 2048,
		/// <summary>Partial send or receive for message.</summary>
		// Token: 0x04001E0E RID: 7694
		Partial = 32768
	}
}
