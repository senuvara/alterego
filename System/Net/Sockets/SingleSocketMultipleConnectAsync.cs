﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x0200047E RID: 1150
	internal class SingleSocketMultipleConnectAsync : MultipleConnectAsync
	{
		// Token: 0x060024E4 RID: 9444 RVA: 0x0008A199 File Offset: 0x00088399
		public SingleSocketMultipleConnectAsync(Socket socket, bool userSocket)
		{
			this.socket = socket;
			this.userSocket = userSocket;
		}

		// Token: 0x060024E5 RID: 9445 RVA: 0x0008A1B0 File Offset: 0x000883B0
		protected override IPAddress GetNextAddress(out Socket attemptSocket)
		{
			attemptSocket = this.socket;
			while (this.nextAddress < this.addressList.Length)
			{
				IPAddress ipaddress = this.addressList[this.nextAddress];
				this.nextAddress++;
				if (this.socket.CanTryAddressFamily(ipaddress.AddressFamily))
				{
					return ipaddress;
				}
			}
			return null;
		}

		// Token: 0x060024E6 RID: 9446 RVA: 0x0008A209 File Offset: 0x00088409
		protected override void OnFail(bool abortive)
		{
			if (abortive || !this.userSocket)
			{
				this.socket.Close();
			}
		}

		// Token: 0x060024E7 RID: 9447 RVA: 0x0000232D File Offset: 0x0000052D
		protected override void OnSucceed()
		{
		}

		// Token: 0x04001E82 RID: 7810
		private Socket socket;

		// Token: 0x04001E83 RID: 7811
		private bool userSocket;
	}
}
