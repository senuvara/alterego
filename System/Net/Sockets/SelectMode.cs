﻿using System;

namespace System.Net.Sockets
{
	/// <summary>Defines the polling modes for the <see cref="M:System.Net.Sockets.Socket.Poll(System.Int32,System.Net.Sockets.SelectMode)" /> method.</summary>
	// Token: 0x02000461 RID: 1121
	public enum SelectMode
	{
		/// <summary>Read status mode.</summary>
		// Token: 0x04001D85 RID: 7557
		SelectRead,
		/// <summary>Write status mode.</summary>
		// Token: 0x04001D86 RID: 7558
		SelectWrite,
		/// <summary>Error status mode.</summary>
		// Token: 0x04001D87 RID: 7559
		SelectError
	}
}
