﻿using System;

namespace System.Net.Sockets
{
	/// <summary>Defines socket option levels for the <see cref="M:System.Net.Sockets.Socket.SetSocketOption(System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName,System.Int32)" /> and <see cref="M:System.Net.Sockets.Socket.GetSocketOption(System.Net.Sockets.SocketOptionLevel,System.Net.Sockets.SocketOptionName)" /> methods.</summary>
	// Token: 0x02000472 RID: 1138
	public enum SocketOptionLevel
	{
		/// <summary>
		///     <see cref="T:System.Net.Sockets.Socket" /> options apply to all sockets.</summary>
		// Token: 0x04001E18 RID: 7704
		Socket = 65535,
		/// <summary>
		///     <see cref="T:System.Net.Sockets.Socket" /> options apply only to IP sockets.</summary>
		// Token: 0x04001E19 RID: 7705
		IP = 0,
		/// <summary>
		///     <see cref="T:System.Net.Sockets.Socket" /> options apply only to IPv6 sockets.</summary>
		// Token: 0x04001E1A RID: 7706
		IPv6 = 41,
		/// <summary>
		///     <see cref="T:System.Net.Sockets.Socket" /> options apply only to TCP sockets.</summary>
		// Token: 0x04001E1B RID: 7707
		Tcp = 6,
		/// <summary>
		///     <see cref="T:System.Net.Sockets.Socket" /> options apply only to UDP sockets.</summary>
		// Token: 0x04001E1C RID: 7708
		Udp = 17
	}
}
