﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x02000484 RID: 1156
	internal enum SocketOperation
	{
		// Token: 0x04001EB8 RID: 7864
		Accept,
		// Token: 0x04001EB9 RID: 7865
		Connect,
		// Token: 0x04001EBA RID: 7866
		Receive,
		// Token: 0x04001EBB RID: 7867
		ReceiveFrom,
		// Token: 0x04001EBC RID: 7868
		Send,
		// Token: 0x04001EBD RID: 7869
		SendTo,
		// Token: 0x04001EBE RID: 7870
		RecvJustCallback,
		// Token: 0x04001EBF RID: 7871
		SendJustCallback,
		// Token: 0x04001EC0 RID: 7872
		Disconnect,
		// Token: 0x04001EC1 RID: 7873
		AcceptReceive,
		// Token: 0x04001EC2 RID: 7874
		ReceiveGeneric,
		// Token: 0x04001EC3 RID: 7875
		SendGeneric
	}
}
