﻿using System;
using System.Runtime.Serialization;

namespace System.Net.Sockets
{
	/// <summary>Encapsulates the information that is necessary to duplicate a <see cref="T:System.Net.Sockets.Socket" />.</summary>
	// Token: 0x02000470 RID: 1136
	[Serializable]
	public struct SocketInformation
	{
		/// <summary>Gets or sets the protocol information for a <see cref="T:System.Net.Sockets.Socket" />.</summary>
		/// <returns>An array of type <see cref="T:System.Byte" />.</returns>
		// Token: 0x170007C5 RID: 1989
		// (get) Token: 0x06002447 RID: 9287 RVA: 0x00088084 File Offset: 0x00086284
		// (set) Token: 0x06002448 RID: 9288 RVA: 0x0008808C File Offset: 0x0008628C
		public byte[] ProtocolInformation
		{
			get
			{
				return this.protocolInformation;
			}
			set
			{
				this.protocolInformation = value;
			}
		}

		/// <summary>Gets or sets the options for a <see cref="T:System.Net.Sockets.Socket" />.</summary>
		/// <returns>A <see cref="T:System.Net.Sockets.SocketInformationOptions" /> instance.</returns>
		// Token: 0x170007C6 RID: 1990
		// (get) Token: 0x06002449 RID: 9289 RVA: 0x00088095 File Offset: 0x00086295
		// (set) Token: 0x0600244A RID: 9290 RVA: 0x0008809D File Offset: 0x0008629D
		public SocketInformationOptions Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		// Token: 0x170007C7 RID: 1991
		// (get) Token: 0x0600244B RID: 9291 RVA: 0x000880A6 File Offset: 0x000862A6
		// (set) Token: 0x0600244C RID: 9292 RVA: 0x000880B3 File Offset: 0x000862B3
		internal bool IsNonBlocking
		{
			get
			{
				return (this.options & SocketInformationOptions.NonBlocking) > (SocketInformationOptions)0;
			}
			set
			{
				if (value)
				{
					this.options |= SocketInformationOptions.NonBlocking;
					return;
				}
				this.options &= ~SocketInformationOptions.NonBlocking;
			}
		}

		// Token: 0x170007C8 RID: 1992
		// (get) Token: 0x0600244D RID: 9293 RVA: 0x000880D6 File Offset: 0x000862D6
		// (set) Token: 0x0600244E RID: 9294 RVA: 0x000880E3 File Offset: 0x000862E3
		internal bool IsConnected
		{
			get
			{
				return (this.options & SocketInformationOptions.Connected) > (SocketInformationOptions)0;
			}
			set
			{
				if (value)
				{
					this.options |= SocketInformationOptions.Connected;
					return;
				}
				this.options &= ~SocketInformationOptions.Connected;
			}
		}

		// Token: 0x170007C9 RID: 1993
		// (get) Token: 0x0600244F RID: 9295 RVA: 0x00088106 File Offset: 0x00086306
		// (set) Token: 0x06002450 RID: 9296 RVA: 0x00088113 File Offset: 0x00086313
		internal bool IsListening
		{
			get
			{
				return (this.options & SocketInformationOptions.Listening) > (SocketInformationOptions)0;
			}
			set
			{
				if (value)
				{
					this.options |= SocketInformationOptions.Listening;
					return;
				}
				this.options &= ~SocketInformationOptions.Listening;
			}
		}

		// Token: 0x170007CA RID: 1994
		// (get) Token: 0x06002451 RID: 9297 RVA: 0x00088136 File Offset: 0x00086336
		// (set) Token: 0x06002452 RID: 9298 RVA: 0x00088143 File Offset: 0x00086343
		internal bool UseOnlyOverlappedIO
		{
			get
			{
				return (this.options & SocketInformationOptions.UseOnlyOverlappedIO) > (SocketInformationOptions)0;
			}
			set
			{
				if (value)
				{
					this.options |= SocketInformationOptions.UseOnlyOverlappedIO;
					return;
				}
				this.options &= ~SocketInformationOptions.UseOnlyOverlappedIO;
			}
		}

		// Token: 0x170007CB RID: 1995
		// (get) Token: 0x06002453 RID: 9299 RVA: 0x00088166 File Offset: 0x00086366
		// (set) Token: 0x06002454 RID: 9300 RVA: 0x0008816E File Offset: 0x0008636E
		internal EndPoint RemoteEndPoint
		{
			get
			{
				return this.remoteEndPoint;
			}
			set
			{
				this.remoteEndPoint = value;
			}
		}

		// Token: 0x04001E0F RID: 7695
		private byte[] protocolInformation;

		// Token: 0x04001E10 RID: 7696
		private SocketInformationOptions options;

		// Token: 0x04001E11 RID: 7697
		[OptionalField]
		private EndPoint remoteEndPoint;
	}
}
