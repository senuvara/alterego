﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Net
{
	/// <summary>Specifies permission to access Internet resources. This class cannot be inherited.</summary>
	// Token: 0x02000330 RID: 816
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class WebPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.WebPermissionAttribute" /> class with a value that specifies the security actions that can be performed on this class.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="action" /> is not a valid <see cref="T:System.Security.Permissions.SecurityAction" /> value. </exception>
		// Token: 0x06001769 RID: 5993 RVA: 0x000544DE File Offset: 0x000526DE
		public WebPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Gets or sets the URI connection string controlled by the current <see cref="T:System.Net.WebPermissionAttribute" />.</summary>
		/// <returns>A string containing the URI connection controlled by the current <see cref="T:System.Net.WebPermissionAttribute" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <see cref="P:System.Net.WebPermissionAttribute.Connect" /> is not <see langword="null" /> when you attempt to set the value. If you wish to specify more than one Connect URI, use an additional attribute declaration statement. </exception>
		// Token: 0x170004CC RID: 1228
		// (get) Token: 0x0600176A RID: 5994 RVA: 0x000544E7 File Offset: 0x000526E7
		// (set) Token: 0x0600176B RID: 5995 RVA: 0x000544F4 File Offset: 0x000526F4
		public string Connect
		{
			get
			{
				return this.m_connect as string;
			}
			set
			{
				if (this.m_connect != null)
				{
					throw new ArgumentException(SR.GetString("The permission '{0}={1}' cannot be added. Add a separate Attribute statement.", new object[]
					{
						"Connect",
						value
					}), "value");
				}
				this.m_connect = value;
			}
		}

		/// <summary>Gets or sets the URI string accepted by the current <see cref="T:System.Net.WebPermissionAttribute" />.</summary>
		/// <returns>A string containing the URI accepted by the current <see cref="T:System.Net.WebPermissionAttribute" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <see cref="P:System.Net.WebPermissionAttribute.Accept" /> is not <see langword="null" /> when you attempt to set the value. If you wish to specify more than one Accept URI, use an additional attribute declaration statement. </exception>
		// Token: 0x170004CD RID: 1229
		// (get) Token: 0x0600176C RID: 5996 RVA: 0x0005452C File Offset: 0x0005272C
		// (set) Token: 0x0600176D RID: 5997 RVA: 0x00054539 File Offset: 0x00052739
		public string Accept
		{
			get
			{
				return this.m_accept as string;
			}
			set
			{
				if (this.m_accept != null)
				{
					throw new ArgumentException(SR.GetString("The permission '{0}={1}' cannot be added. Add a separate Attribute statement.", new object[]
					{
						"Accept",
						value
					}), "value");
				}
				this.m_accept = value;
			}
		}

		/// <summary>Gets or sets a regular expression pattern that describes the URI connection controlled by the current <see cref="T:System.Net.WebPermissionAttribute" />.</summary>
		/// <returns>A string containing a regular expression pattern that describes the URI connection controlled by this <see cref="T:System.Net.WebPermissionAttribute" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <see cref="P:System.Net.WebPermissionAttribute.ConnectPattern" /> is not <see langword="null" /> when you attempt to set the value. If you wish to specify more than one connect URI, use an additional attribute declaration statement. </exception>
		// Token: 0x170004CE RID: 1230
		// (get) Token: 0x0600176E RID: 5998 RVA: 0x00054571 File Offset: 0x00052771
		// (set) Token: 0x0600176F RID: 5999 RVA: 0x000545B0 File Offset: 0x000527B0
		public string ConnectPattern
		{
			get
			{
				if (this.m_connect is DelayedRegex)
				{
					return this.m_connect.ToString();
				}
				if (!(this.m_connect is bool) || !(bool)this.m_connect)
				{
					return null;
				}
				return ".*";
			}
			set
			{
				if (this.m_connect != null)
				{
					throw new ArgumentException(SR.GetString("The permission '{0}={1}' cannot be added. Add a separate Attribute statement.", new object[]
					{
						"ConnectPatern",
						value
					}), "value");
				}
				if (value == ".*")
				{
					this.m_connect = true;
					return;
				}
				this.m_connect = new DelayedRegex(value);
			}
		}

		/// <summary>Gets or sets a regular expression pattern that describes the URI accepted by the current <see cref="T:System.Net.WebPermissionAttribute" />.</summary>
		/// <returns>A string containing a regular expression pattern that describes the URI accepted by the current <see cref="T:System.Net.WebPermissionAttribute" />. This string must be escaped according to the rules for encoding a <see cref="T:System.Text.RegularExpressions.Regex" /> constructor string.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <see cref="P:System.Net.WebPermissionAttribute.AcceptPattern" /> is not <see langword="null" /> when you attempt to set the value. If you wish to specify more than one Accept URI, use an additional attribute declaration statement. </exception>
		// Token: 0x170004CF RID: 1231
		// (get) Token: 0x06001770 RID: 6000 RVA: 0x00054612 File Offset: 0x00052812
		// (set) Token: 0x06001771 RID: 6001 RVA: 0x00054650 File Offset: 0x00052850
		public string AcceptPattern
		{
			get
			{
				if (this.m_accept is DelayedRegex)
				{
					return this.m_accept.ToString();
				}
				if (!(this.m_accept is bool) || !(bool)this.m_accept)
				{
					return null;
				}
				return ".*";
			}
			set
			{
				if (this.m_accept != null)
				{
					throw new ArgumentException(SR.GetString("The permission '{0}={1}' cannot be added. Add a separate Attribute statement.", new object[]
					{
						"AcceptPattern",
						value
					}), "value");
				}
				if (value == ".*")
				{
					this.m_accept = true;
					return;
				}
				this.m_accept = new DelayedRegex(value);
			}
		}

		/// <summary>Creates and returns a new instance of the <see cref="T:System.Net.WebPermission" /> class.</summary>
		/// <returns>A <see cref="T:System.Net.WebPermission" /> corresponding to the security declaration.</returns>
		// Token: 0x06001772 RID: 6002 RVA: 0x000546B4 File Offset: 0x000528B4
		public override IPermission CreatePermission()
		{
			WebPermission webPermission;
			if (base.Unrestricted)
			{
				webPermission = new WebPermission(PermissionState.Unrestricted);
			}
			else
			{
				NetworkAccess networkAccess = (NetworkAccess)0;
				if (this.m_connect is bool)
				{
					if ((bool)this.m_connect)
					{
						networkAccess |= NetworkAccess.Connect;
					}
					this.m_connect = null;
				}
				if (this.m_accept is bool)
				{
					if ((bool)this.m_accept)
					{
						networkAccess |= NetworkAccess.Accept;
					}
					this.m_accept = null;
				}
				webPermission = new WebPermission(networkAccess);
				if (this.m_accept != null)
				{
					if (this.m_accept is DelayedRegex)
					{
						webPermission.AddAsPattern(NetworkAccess.Accept, (DelayedRegex)this.m_accept);
					}
					else
					{
						webPermission.AddPermission(NetworkAccess.Accept, (string)this.m_accept);
					}
				}
				if (this.m_connect != null)
				{
					if (this.m_connect is DelayedRegex)
					{
						webPermission.AddAsPattern(NetworkAccess.Connect, (DelayedRegex)this.m_connect);
					}
					else
					{
						webPermission.AddPermission(NetworkAccess.Connect, (string)this.m_connect);
					}
				}
			}
			return webPermission;
		}

		// Token: 0x040016CD RID: 5837
		private object m_accept;

		// Token: 0x040016CE RID: 5838
		private object m_connect;
	}
}
