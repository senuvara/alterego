﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.Net
{
	// Token: 0x02000360 RID: 864
	internal static class TimerThread
	{
		// Token: 0x060018DB RID: 6363 RVA: 0x000594B4 File Offset: 0x000576B4
		static TimerThread()
		{
			TimerThread.s_ThreadEvents = new WaitHandle[]
			{
				TimerThread.s_ThreadShutdownEvent,
				TimerThread.s_ThreadReadyEvent
			};
			AppDomain.CurrentDomain.DomainUnload += TimerThread.OnDomainUnload;
		}

		// Token: 0x060018DC RID: 6364 RVA: 0x0005952C File Offset: 0x0005772C
		internal static TimerThread.Queue CreateQueue(int durationMilliseconds)
		{
			if (durationMilliseconds == -1)
			{
				return new TimerThread.InfiniteTimerQueue();
			}
			if (durationMilliseconds < 0)
			{
				throw new ArgumentOutOfRangeException("durationMilliseconds");
			}
			LinkedList<WeakReference> obj = TimerThread.s_NewQueues;
			TimerThread.TimerQueue timerQueue;
			lock (obj)
			{
				timerQueue = new TimerThread.TimerQueue(durationMilliseconds);
				WeakReference value = new WeakReference(timerQueue);
				TimerThread.s_NewQueues.AddLast(value);
			}
			return timerQueue;
		}

		// Token: 0x060018DD RID: 6365 RVA: 0x0005959C File Offset: 0x0005779C
		internal static TimerThread.Queue GetOrCreateQueue(int durationMilliseconds)
		{
			if (durationMilliseconds == -1)
			{
				return new TimerThread.InfiniteTimerQueue();
			}
			if (durationMilliseconds < 0)
			{
				throw new ArgumentOutOfRangeException("durationMilliseconds");
			}
			WeakReference weakReference = (WeakReference)TimerThread.s_QueuesCache[durationMilliseconds];
			TimerThread.TimerQueue timerQueue;
			if (weakReference == null || (timerQueue = (TimerThread.TimerQueue)weakReference.Target) == null)
			{
				LinkedList<WeakReference> obj = TimerThread.s_NewQueues;
				lock (obj)
				{
					weakReference = (WeakReference)TimerThread.s_QueuesCache[durationMilliseconds];
					if (weakReference == null || (timerQueue = (TimerThread.TimerQueue)weakReference.Target) == null)
					{
						timerQueue = new TimerThread.TimerQueue(durationMilliseconds);
						weakReference = new WeakReference(timerQueue);
						TimerThread.s_NewQueues.AddLast(weakReference);
						TimerThread.s_QueuesCache[durationMilliseconds] = weakReference;
						if (++TimerThread.s_CacheScanIteration % 32 == 0)
						{
							List<int> list = new List<int>();
							foreach (object obj2 in TimerThread.s_QueuesCache)
							{
								DictionaryEntry dictionaryEntry = (DictionaryEntry)obj2;
								if (((WeakReference)dictionaryEntry.Value).Target == null)
								{
									list.Add((int)dictionaryEntry.Key);
								}
							}
							for (int i = 0; i < list.Count; i++)
							{
								TimerThread.s_QueuesCache.Remove(list[i]);
							}
						}
					}
				}
			}
			return timerQueue;
		}

		// Token: 0x060018DE RID: 6366 RVA: 0x00059740 File Offset: 0x00057940
		private static void Prod()
		{
			TimerThread.s_ThreadReadyEvent.Set();
			if (Interlocked.CompareExchange(ref TimerThread.s_ThreadState, 1, 0) == 0)
			{
				new Thread(new ThreadStart(TimerThread.ThreadProc)).Start();
			}
		}

		// Token: 0x060018DF RID: 6367 RVA: 0x00059774 File Offset: 0x00057974
		private static void ThreadProc()
		{
			Thread.CurrentThread.IsBackground = true;
			LinkedList<WeakReference> obj = TimerThread.s_Queues;
			lock (obj)
			{
				if (Interlocked.CompareExchange(ref TimerThread.s_ThreadState, 1, 1) == 1)
				{
					bool flag2 = true;
					while (flag2)
					{
						try
						{
							TimerThread.s_ThreadReadyEvent.Reset();
							for (;;)
							{
								if (TimerThread.s_NewQueues.Count > 0)
								{
									LinkedList<WeakReference> obj2 = TimerThread.s_NewQueues;
									lock (obj2)
									{
										for (LinkedListNode<WeakReference> first = TimerThread.s_NewQueues.First; first != null; first = TimerThread.s_NewQueues.First)
										{
											TimerThread.s_NewQueues.Remove(first);
											TimerThread.s_Queues.AddLast(first);
										}
									}
								}
								int tickCount = Environment.TickCount;
								int num = 0;
								bool flag4 = false;
								LinkedListNode<WeakReference> linkedListNode = TimerThread.s_Queues.First;
								while (linkedListNode != null)
								{
									TimerThread.TimerQueue timerQueue = (TimerThread.TimerQueue)linkedListNode.Value.Target;
									if (timerQueue == null)
									{
										LinkedListNode<WeakReference> next = linkedListNode.Next;
										TimerThread.s_Queues.Remove(linkedListNode);
										linkedListNode = next;
									}
									else
									{
										int num2;
										if (timerQueue.Fire(out num2) && (!flag4 || TimerThread.IsTickBetween(tickCount, num, num2)))
										{
											num = num2;
											flag4 = true;
										}
										linkedListNode = linkedListNode.Next;
									}
								}
								int tickCount2 = Environment.TickCount;
								int millisecondsTimeout = (int)(flag4 ? (TimerThread.IsTickBetween(tickCount, num, tickCount2) ? (Math.Min((uint)(num - tickCount2), 2147483632U) + 15U) : 0U) : 30000U);
								int num3 = WaitHandle.WaitAny(TimerThread.s_ThreadEvents, millisecondsTimeout, false);
								if (num3 == 0)
								{
									break;
								}
								if (num3 == 258 && !flag4)
								{
									Interlocked.CompareExchange(ref TimerThread.s_ThreadState, 0, 1);
									if (!TimerThread.s_ThreadReadyEvent.WaitOne(0, false) || Interlocked.CompareExchange(ref TimerThread.s_ThreadState, 1, 0) != 0)
									{
										goto IL_1A8;
									}
								}
							}
							flag2 = false;
							continue;
							IL_1A8:
							flag2 = false;
						}
						catch (Exception exception)
						{
							if (NclUtilities.IsFatal(exception))
							{
								throw;
							}
							bool on = Logging.On;
							Thread.Sleep(1000);
						}
					}
				}
			}
		}

		// Token: 0x060018E0 RID: 6368 RVA: 0x000599A8 File Offset: 0x00057BA8
		private static void StopTimerThread()
		{
			Interlocked.Exchange(ref TimerThread.s_ThreadState, 2);
			TimerThread.s_ThreadShutdownEvent.Set();
		}

		// Token: 0x060018E1 RID: 6369 RVA: 0x000599C1 File Offset: 0x00057BC1
		private static bool IsTickBetween(int start, int end, int comparand)
		{
			return start <= comparand == end <= comparand != start <= end;
		}

		// Token: 0x060018E2 RID: 6370 RVA: 0x000599E0 File Offset: 0x00057BE0
		private static void OnDomainUnload(object sender, EventArgs e)
		{
			try
			{
				TimerThread.StopTimerThread();
			}
			catch
			{
			}
		}

		// Token: 0x040017AA RID: 6058
		private const int c_ThreadIdleTimeoutMilliseconds = 30000;

		// Token: 0x040017AB RID: 6059
		private const int c_CacheScanPerIterations = 32;

		// Token: 0x040017AC RID: 6060
		private const int c_TickCountResolution = 15;

		// Token: 0x040017AD RID: 6061
		private static LinkedList<WeakReference> s_Queues = new LinkedList<WeakReference>();

		// Token: 0x040017AE RID: 6062
		private static LinkedList<WeakReference> s_NewQueues = new LinkedList<WeakReference>();

		// Token: 0x040017AF RID: 6063
		private static int s_ThreadState = 0;

		// Token: 0x040017B0 RID: 6064
		private static AutoResetEvent s_ThreadReadyEvent = new AutoResetEvent(false);

		// Token: 0x040017B1 RID: 6065
		private static ManualResetEvent s_ThreadShutdownEvent = new ManualResetEvent(false);

		// Token: 0x040017B2 RID: 6066
		private static WaitHandle[] s_ThreadEvents;

		// Token: 0x040017B3 RID: 6067
		private static int s_CacheScanIteration;

		// Token: 0x040017B4 RID: 6068
		private static Hashtable s_QueuesCache = new Hashtable();

		// Token: 0x02000361 RID: 865
		internal abstract class Queue
		{
			// Token: 0x060018E3 RID: 6371 RVA: 0x00059A08 File Offset: 0x00057C08
			internal Queue(int durationMilliseconds)
			{
				this.m_DurationMilliseconds = durationMilliseconds;
			}

			// Token: 0x17000521 RID: 1313
			// (get) Token: 0x060018E4 RID: 6372 RVA: 0x00059A17 File Offset: 0x00057C17
			internal int Duration
			{
				get
				{
					return this.m_DurationMilliseconds;
				}
			}

			// Token: 0x060018E5 RID: 6373 RVA: 0x00059A1F File Offset: 0x00057C1F
			internal TimerThread.Timer CreateTimer()
			{
				return this.CreateTimer(null, null);
			}

			// Token: 0x060018E6 RID: 6374
			internal abstract TimerThread.Timer CreateTimer(TimerThread.Callback callback, object context);

			// Token: 0x040017B5 RID: 6069
			private readonly int m_DurationMilliseconds;
		}

		// Token: 0x02000362 RID: 866
		internal abstract class Timer : IDisposable
		{
			// Token: 0x060018E7 RID: 6375 RVA: 0x00059A29 File Offset: 0x00057C29
			internal Timer(int durationMilliseconds)
			{
				this.m_DurationMilliseconds = durationMilliseconds;
				this.m_StartTimeMilliseconds = Environment.TickCount;
			}

			// Token: 0x17000522 RID: 1314
			// (get) Token: 0x060018E8 RID: 6376 RVA: 0x00059A43 File Offset: 0x00057C43
			internal int Duration
			{
				get
				{
					return this.m_DurationMilliseconds;
				}
			}

			// Token: 0x17000523 RID: 1315
			// (get) Token: 0x060018E9 RID: 6377 RVA: 0x00059A4B File Offset: 0x00057C4B
			internal int StartTime
			{
				get
				{
					return this.m_StartTimeMilliseconds;
				}
			}

			// Token: 0x17000524 RID: 1316
			// (get) Token: 0x060018EA RID: 6378 RVA: 0x00059A53 File Offset: 0x00057C53
			internal int Expiration
			{
				get
				{
					return this.m_StartTimeMilliseconds + this.m_DurationMilliseconds;
				}
			}

			// Token: 0x17000525 RID: 1317
			// (get) Token: 0x060018EB RID: 6379 RVA: 0x00059A64 File Offset: 0x00057C64
			internal int TimeRemaining
			{
				get
				{
					if (this.HasExpired)
					{
						return 0;
					}
					if (this.Duration == -1)
					{
						return -1;
					}
					int tickCount = Environment.TickCount;
					int num = (int)(TimerThread.IsTickBetween(this.StartTime, this.Expiration, tickCount) ? Math.Min((uint)(this.Expiration - tickCount), 2147483647U) : 0U);
					if (num >= 2)
					{
						return num;
					}
					return num + 1;
				}
			}

			// Token: 0x060018EC RID: 6380
			internal abstract bool Cancel();

			// Token: 0x17000526 RID: 1318
			// (get) Token: 0x060018ED RID: 6381
			internal abstract bool HasExpired { get; }

			// Token: 0x060018EE RID: 6382 RVA: 0x00059ABF File Offset: 0x00057CBF
			public void Dispose()
			{
				this.Cancel();
			}

			// Token: 0x040017B6 RID: 6070
			private readonly int m_StartTimeMilliseconds;

			// Token: 0x040017B7 RID: 6071
			private readonly int m_DurationMilliseconds;
		}

		// Token: 0x02000363 RID: 867
		// (Invoke) Token: 0x060018F0 RID: 6384
		internal delegate void Callback(TimerThread.Timer timer, int timeNoticed, object context);

		// Token: 0x02000364 RID: 868
		private enum TimerThreadState
		{
			// Token: 0x040017B9 RID: 6073
			Idle,
			// Token: 0x040017BA RID: 6074
			Running,
			// Token: 0x040017BB RID: 6075
			Stopped
		}

		// Token: 0x02000365 RID: 869
		private class TimerQueue : TimerThread.Queue
		{
			// Token: 0x060018F3 RID: 6387 RVA: 0x00059AC8 File Offset: 0x00057CC8
			internal TimerQueue(int durationMilliseconds) : base(durationMilliseconds)
			{
				this.m_Timers = new TimerThread.TimerNode();
				this.m_Timers.Next = this.m_Timers;
				this.m_Timers.Prev = this.m_Timers;
			}

			// Token: 0x060018F4 RID: 6388 RVA: 0x00059B00 File Offset: 0x00057D00
			internal override TimerThread.Timer CreateTimer(TimerThread.Callback callback, object context)
			{
				TimerThread.TimerNode timerNode = new TimerThread.TimerNode(callback, context, base.Duration, this.m_Timers);
				bool flag = false;
				TimerThread.TimerNode timers = this.m_Timers;
				lock (timers)
				{
					if (this.m_Timers.Next == this.m_Timers)
					{
						if (this.m_ThisHandle == IntPtr.Zero)
						{
							this.m_ThisHandle = (IntPtr)GCHandle.Alloc(this);
						}
						flag = true;
					}
					timerNode.Next = this.m_Timers;
					timerNode.Prev = this.m_Timers.Prev;
					this.m_Timers.Prev.Next = timerNode;
					this.m_Timers.Prev = timerNode;
				}
				if (flag)
				{
					TimerThread.Prod();
				}
				return timerNode;
			}

			// Token: 0x060018F5 RID: 6389 RVA: 0x00059BCC File Offset: 0x00057DCC
			internal bool Fire(out int nextExpiration)
			{
				TimerThread.TimerNode next;
				do
				{
					next = this.m_Timers.Next;
					if (next == this.m_Timers)
					{
						TimerThread.TimerNode timers = this.m_Timers;
						lock (timers)
						{
							next = this.m_Timers.Next;
							if (next == this.m_Timers)
							{
								if (this.m_ThisHandle != IntPtr.Zero)
								{
									((GCHandle)this.m_ThisHandle).Free();
									this.m_ThisHandle = IntPtr.Zero;
								}
								nextExpiration = 0;
								return false;
							}
						}
					}
				}
				while (next.Fire());
				nextExpiration = next.Expiration;
				return true;
			}

			// Token: 0x040017BC RID: 6076
			private IntPtr m_ThisHandle;

			// Token: 0x040017BD RID: 6077
			private readonly TimerThread.TimerNode m_Timers;
		}

		// Token: 0x02000366 RID: 870
		private class InfiniteTimerQueue : TimerThread.Queue
		{
			// Token: 0x060018F6 RID: 6390 RVA: 0x00059C80 File Offset: 0x00057E80
			internal InfiniteTimerQueue() : base(-1)
			{
			}

			// Token: 0x060018F7 RID: 6391 RVA: 0x00059C89 File Offset: 0x00057E89
			internal override TimerThread.Timer CreateTimer(TimerThread.Callback callback, object context)
			{
				return new TimerThread.InfiniteTimer();
			}
		}

		// Token: 0x02000367 RID: 871
		private class TimerNode : TimerThread.Timer
		{
			// Token: 0x060018F8 RID: 6392 RVA: 0x00059C90 File Offset: 0x00057E90
			internal TimerNode(TimerThread.Callback callback, object context, int durationMilliseconds, object queueLock) : base(durationMilliseconds)
			{
				if (callback != null)
				{
					this.m_Callback = callback;
					this.m_Context = context;
				}
				this.m_TimerState = TimerThread.TimerNode.TimerState.Ready;
				this.m_QueueLock = queueLock;
			}

			// Token: 0x060018F9 RID: 6393 RVA: 0x00059CB9 File Offset: 0x00057EB9
			internal TimerNode() : base(0)
			{
				this.m_TimerState = TimerThread.TimerNode.TimerState.Sentinel;
			}

			// Token: 0x17000527 RID: 1319
			// (get) Token: 0x060018FA RID: 6394 RVA: 0x00059CC9 File Offset: 0x00057EC9
			internal override bool HasExpired
			{
				get
				{
					return this.m_TimerState == TimerThread.TimerNode.TimerState.Fired;
				}
			}

			// Token: 0x17000528 RID: 1320
			// (get) Token: 0x060018FB RID: 6395 RVA: 0x00059CD4 File Offset: 0x00057ED4
			// (set) Token: 0x060018FC RID: 6396 RVA: 0x00059CDC File Offset: 0x00057EDC
			internal TimerThread.TimerNode Next
			{
				get
				{
					return this.next;
				}
				set
				{
					this.next = value;
				}
			}

			// Token: 0x17000529 RID: 1321
			// (get) Token: 0x060018FD RID: 6397 RVA: 0x00059CE5 File Offset: 0x00057EE5
			// (set) Token: 0x060018FE RID: 6398 RVA: 0x00059CED File Offset: 0x00057EED
			internal TimerThread.TimerNode Prev
			{
				get
				{
					return this.prev;
				}
				set
				{
					this.prev = value;
				}
			}

			// Token: 0x060018FF RID: 6399 RVA: 0x00059CF8 File Offset: 0x00057EF8
			internal override bool Cancel()
			{
				if (this.m_TimerState == TimerThread.TimerNode.TimerState.Ready)
				{
					object queueLock = this.m_QueueLock;
					lock (queueLock)
					{
						if (this.m_TimerState == TimerThread.TimerNode.TimerState.Ready)
						{
							this.Next.Prev = this.Prev;
							this.Prev.Next = this.Next;
							this.Next = null;
							this.Prev = null;
							this.m_Callback = null;
							this.m_Context = null;
							this.m_TimerState = TimerThread.TimerNode.TimerState.Cancelled;
							return true;
						}
					}
					return false;
				}
				return false;
			}

			// Token: 0x06001900 RID: 6400 RVA: 0x00059D90 File Offset: 0x00057F90
			internal bool Fire()
			{
				if (this.m_TimerState != TimerThread.TimerNode.TimerState.Ready)
				{
					return true;
				}
				int tickCount = Environment.TickCount;
				if (TimerThread.IsTickBetween(base.StartTime, base.Expiration, tickCount))
				{
					return false;
				}
				bool flag = false;
				object queueLock = this.m_QueueLock;
				lock (queueLock)
				{
					if (this.m_TimerState == TimerThread.TimerNode.TimerState.Ready)
					{
						this.m_TimerState = TimerThread.TimerNode.TimerState.Fired;
						this.Next.Prev = this.Prev;
						this.Prev.Next = this.Next;
						this.Next = null;
						this.Prev = null;
						flag = (this.m_Callback != null);
					}
				}
				if (flag)
				{
					try
					{
						TimerThread.Callback callback = this.m_Callback;
						object context = this.m_Context;
						this.m_Callback = null;
						this.m_Context = null;
						callback(this, tickCount, context);
					}
					catch (Exception exception)
					{
						if (NclUtilities.IsFatal(exception))
						{
							throw;
						}
						bool on = Logging.On;
					}
				}
				return true;
			}

			// Token: 0x040017BE RID: 6078
			private TimerThread.TimerNode.TimerState m_TimerState;

			// Token: 0x040017BF RID: 6079
			private TimerThread.Callback m_Callback;

			// Token: 0x040017C0 RID: 6080
			private object m_Context;

			// Token: 0x040017C1 RID: 6081
			private object m_QueueLock;

			// Token: 0x040017C2 RID: 6082
			private TimerThread.TimerNode next;

			// Token: 0x040017C3 RID: 6083
			private TimerThread.TimerNode prev;

			// Token: 0x02000368 RID: 872
			private enum TimerState
			{
				// Token: 0x040017C5 RID: 6085
				Ready,
				// Token: 0x040017C6 RID: 6086
				Fired,
				// Token: 0x040017C7 RID: 6087
				Cancelled,
				// Token: 0x040017C8 RID: 6088
				Sentinel
			}
		}

		// Token: 0x02000369 RID: 873
		private class InfiniteTimer : TimerThread.Timer
		{
			// Token: 0x06001901 RID: 6401 RVA: 0x00059E84 File Offset: 0x00058084
			internal InfiniteTimer() : base(-1)
			{
			}

			// Token: 0x1700052A RID: 1322
			// (get) Token: 0x06001902 RID: 6402 RVA: 0x00005AFA File Offset: 0x00003CFA
			internal override bool HasExpired
			{
				get
				{
					return false;
				}
			}

			// Token: 0x06001903 RID: 6403 RVA: 0x00059E8D File Offset: 0x0005808D
			internal override bool Cancel()
			{
				return Interlocked.Exchange(ref this.cancelled, 1) == 0;
			}

			// Token: 0x040017C9 RID: 6089
			private int cancelled;
		}
	}
}
