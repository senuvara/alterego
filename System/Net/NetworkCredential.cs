﻿using System;
using System.Security;

namespace System.Net
{
	/// <summary>Provides credentials for password-based authentication schemes such as basic, digest, NTLM, and Kerberos authentication.</summary>
	// Token: 0x02000320 RID: 800
	public class NetworkCredential : ICredentials, ICredentialsByHost
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkCredential" /> class.</summary>
		// Token: 0x060016CD RID: 5837 RVA: 0x0004F6A9 File Offset: 0x0004D8A9
		public NetworkCredential() : this(string.Empty, string.Empty, string.Empty)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkCredential" /> class with the specified user name and password.</summary>
		/// <param name="userName">The user name associated with the credentials. </param>
		/// <param name="password">The password for the user name associated with the credentials. </param>
		// Token: 0x060016CE RID: 5838 RVA: 0x00051D4D File Offset: 0x0004FF4D
		public NetworkCredential(string userName, string password) : this(userName, password, string.Empty)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkCredential" /> class with the specified user name and password.</summary>
		/// <param name="userName">The user name associated with the credentials.</param>
		/// <param name="password">The password for the user name associated with the credentials.</param>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Security.SecureString" /> class is not supported on this platform.</exception>
		// Token: 0x060016CF RID: 5839 RVA: 0x00051D5C File Offset: 0x0004FF5C
		public NetworkCredential(string userName, SecureString password) : this(userName, password, string.Empty)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkCredential" /> class with the specified user name, password, and domain.</summary>
		/// <param name="userName">The user name associated with the credentials. </param>
		/// <param name="password">The password for the user name associated with the credentials. </param>
		/// <param name="domain">The domain associated with these credentials. </param>
		// Token: 0x060016D0 RID: 5840 RVA: 0x00051D6B File Offset: 0x0004FF6B
		public NetworkCredential(string userName, string password, string domain)
		{
			this.UserName = userName;
			this.Password = password;
			this.Domain = domain;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Net.NetworkCredential" /> class with the specified user name, password, and domain.</summary>
		/// <param name="userName">The user name associated with the credentials.</param>
		/// <param name="password">The password for the user name associated with the credentials.</param>
		/// <param name="domain">The domain associated with these credentials.</param>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Security.SecureString" /> class is not supported on this platform.</exception>
		// Token: 0x060016D1 RID: 5841 RVA: 0x00051D88 File Offset: 0x0004FF88
		public NetworkCredential(string userName, SecureString password, string domain)
		{
			this.UserName = userName;
			this.SecurePassword = password;
			this.Domain = domain;
		}

		/// <summary>Gets or sets the user name associated with the credentials.</summary>
		/// <returns>The user name associated with the credentials.</returns>
		// Token: 0x170004AA RID: 1194
		// (get) Token: 0x060016D2 RID: 5842 RVA: 0x00051DA5 File Offset: 0x0004FFA5
		// (set) Token: 0x060016D3 RID: 5843 RVA: 0x00051DAD File Offset: 0x0004FFAD
		public string UserName
		{
			get
			{
				return this.InternalGetUserName();
			}
			set
			{
				if (value == null)
				{
					this.m_userName = string.Empty;
					return;
				}
				this.m_userName = value;
			}
		}

		/// <summary>Gets or sets the password for the user name associated with the credentials.</summary>
		/// <returns>The password associated with the credentials. If this <see cref="T:System.Net.NetworkCredential" /> instance was initialized with the <paramref name="password" /> parameter set to <see langword="null" />, then the <see cref="P:System.Net.NetworkCredential.Password" /> property will return an empty string.</returns>
		// Token: 0x170004AB RID: 1195
		// (get) Token: 0x060016D4 RID: 5844 RVA: 0x00051DC5 File Offset: 0x0004FFC5
		// (set) Token: 0x060016D5 RID: 5845 RVA: 0x00051DCD File Offset: 0x0004FFCD
		public string Password
		{
			get
			{
				return this.InternalGetPassword();
			}
			set
			{
				this.m_password = UnsafeNclNativeMethods.SecureStringHelper.CreateSecureString(value);
			}
		}

		/// <summary>Gets or sets the password as a <see cref="T:System.Security.SecureString" /> instance.</summary>
		/// <returns>The password for the user name associated with the credentials.</returns>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Security.SecureString" /> class is not supported on this platform.</exception>
		// Token: 0x170004AC RID: 1196
		// (get) Token: 0x060016D6 RID: 5846 RVA: 0x00051DDB File Offset: 0x0004FFDB
		// (set) Token: 0x060016D7 RID: 5847 RVA: 0x00051DE8 File Offset: 0x0004FFE8
		public SecureString SecurePassword
		{
			get
			{
				return this.InternalGetSecurePassword().Copy();
			}
			set
			{
				if (value == null)
				{
					this.m_password = new SecureString();
					return;
				}
				this.m_password = value.Copy();
			}
		}

		/// <summary>Gets or sets the domain or computer name that verifies the credentials.</summary>
		/// <returns>The name of the domain associated with the credentials.</returns>
		// Token: 0x170004AD RID: 1197
		// (get) Token: 0x060016D8 RID: 5848 RVA: 0x00051E05 File Offset: 0x00050005
		// (set) Token: 0x060016D9 RID: 5849 RVA: 0x00051E0D File Offset: 0x0005000D
		public string Domain
		{
			get
			{
				return this.InternalGetDomain();
			}
			set
			{
				if (value == null)
				{
					this.m_domain = string.Empty;
					return;
				}
				this.m_domain = value;
			}
		}

		// Token: 0x060016DA RID: 5850 RVA: 0x00051E25 File Offset: 0x00050025
		internal string InternalGetUserName()
		{
			return this.m_userName;
		}

		// Token: 0x060016DB RID: 5851 RVA: 0x00051E2D File Offset: 0x0005002D
		internal string InternalGetPassword()
		{
			return UnsafeNclNativeMethods.SecureStringHelper.CreateString(this.m_password);
		}

		// Token: 0x060016DC RID: 5852 RVA: 0x00051E3A File Offset: 0x0005003A
		internal SecureString InternalGetSecurePassword()
		{
			return this.m_password;
		}

		// Token: 0x060016DD RID: 5853 RVA: 0x00051E42 File Offset: 0x00050042
		internal string InternalGetDomain()
		{
			return this.m_domain;
		}

		// Token: 0x060016DE RID: 5854 RVA: 0x00051E4C File Offset: 0x0005004C
		internal string InternalGetDomainUserName()
		{
			string text = this.InternalGetDomain();
			if (text.Length != 0)
			{
				text += "\\";
			}
			return text + this.InternalGetUserName();
		}

		/// <summary>Returns an instance of the <see cref="T:System.Net.NetworkCredential" /> class for the specified Uniform Resource Identifier (URI) and authentication type.</summary>
		/// <param name="uri">The URI that the client provides authentication for. </param>
		/// <param name="authType">The type of authentication requested, as defined in the <see cref="P:System.Net.IAuthenticationModule.AuthenticationType" /> property. </param>
		/// <returns>A <see cref="T:System.Net.NetworkCredential" /> object.</returns>
		// Token: 0x060016DF RID: 5855 RVA: 0x00002068 File Offset: 0x00000268
		public NetworkCredential GetCredential(Uri uri, string authType)
		{
			return this;
		}

		/// <summary>Returns an instance of the <see cref="T:System.Net.NetworkCredential" /> class for the specified host, port, and authentication type.</summary>
		/// <param name="host">The host computer that authenticates the client.</param>
		/// <param name="port">The port on the <paramref name="host" /> that the client communicates with.</param>
		/// <param name="authenticationType">The type of authentication requested, as defined in the <see cref="P:System.Net.IAuthenticationModule.AuthenticationType" /> property. </param>
		/// <returns>A <see cref="T:System.Net.NetworkCredential" /> for the specified host, port, and authentication protocol, or <see langword="null" /> if there are no credentials available for the specified host, port, and authentication protocol.</returns>
		// Token: 0x060016E0 RID: 5856 RVA: 0x00002068 File Offset: 0x00000268
		public NetworkCredential GetCredential(string host, int port, string authenticationType)
		{
			return this;
		}

		// Token: 0x04001665 RID: 5733
		private string m_domain;

		// Token: 0x04001666 RID: 5734
		private string m_userName;

		// Token: 0x04001667 RID: 5735
		private SecureString m_password;
	}
}
