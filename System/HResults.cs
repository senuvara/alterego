﻿using System;

namespace System
{
	// Token: 0x0200008C RID: 140
	internal static class HResults
	{
		// Token: 0x0400080B RID: 2059
		internal const int Configuration = -2146232062;

		// Token: 0x0400080C RID: 2060
		internal const int Xml = -2146232000;

		// Token: 0x0400080D RID: 2061
		internal const int XmlSchema = -2146231999;

		// Token: 0x0400080E RID: 2062
		internal const int XmlXslt = -2146231998;

		// Token: 0x0400080F RID: 2063
		internal const int XmlXPath = -2146231997;

		// Token: 0x04000810 RID: 2064
		internal const int Data = -2146232032;

		// Token: 0x04000811 RID: 2065
		internal const int DataDeletedRowInaccessible = -2146232031;

		// Token: 0x04000812 RID: 2066
		internal const int DataDuplicateName = -2146232030;

		// Token: 0x04000813 RID: 2067
		internal const int DataInRowChangingEvent = -2146232029;

		// Token: 0x04000814 RID: 2068
		internal const int DataInvalidConstraint = -2146232028;

		// Token: 0x04000815 RID: 2069
		internal const int DataMissingPrimaryKey = -2146232027;

		// Token: 0x04000816 RID: 2070
		internal const int DataNoNullAllowed = -2146232026;

		// Token: 0x04000817 RID: 2071
		internal const int DataReadOnly = -2146232025;

		// Token: 0x04000818 RID: 2072
		internal const int DataRowNotInTable = -2146232024;

		// Token: 0x04000819 RID: 2073
		internal const int DataVersionNotFound = -2146232023;

		// Token: 0x0400081A RID: 2074
		internal const int DataConstraint = -2146232022;

		// Token: 0x0400081B RID: 2075
		internal const int StrongTyping = -2146232021;

		// Token: 0x0400081C RID: 2076
		internal const int SqlType = -2146232016;

		// Token: 0x0400081D RID: 2077
		internal const int SqlNullValue = -2146232015;

		// Token: 0x0400081E RID: 2078
		internal const int SqlTruncate = -2146232014;

		// Token: 0x0400081F RID: 2079
		internal const int AdapterMapping = -2146232013;

		// Token: 0x04000820 RID: 2080
		internal const int DataAdapter = -2146232012;

		// Token: 0x04000821 RID: 2081
		internal const int DBConcurrency = -2146232011;

		// Token: 0x04000822 RID: 2082
		internal const int OperationAborted = -2146232010;

		// Token: 0x04000823 RID: 2083
		internal const int InvalidUdt = -2146232009;

		// Token: 0x04000824 RID: 2084
		internal const int Metadata = -2146232007;

		// Token: 0x04000825 RID: 2085
		internal const int InvalidQuery = -2146232006;

		// Token: 0x04000826 RID: 2086
		internal const int CommandCompilation = -2146232005;

		// Token: 0x04000827 RID: 2087
		internal const int CommandExecution = -2146232004;

		// Token: 0x04000828 RID: 2088
		internal const int SqlException = -2146232060;

		// Token: 0x04000829 RID: 2089
		internal const int OdbcException = -2146232009;

		// Token: 0x0400082A RID: 2090
		internal const int OracleException = -2146232008;

		// Token: 0x0400082B RID: 2091
		internal const int ConnectionPlanException = -2146232003;

		// Token: 0x0400082C RID: 2092
		internal const int NteBadKeySet = -2146893802;

		// Token: 0x0400082D RID: 2093
		internal const int Win32AccessDenied = -2147024891;

		// Token: 0x0400082E RID: 2094
		internal const int Win32InvalidHandle = -2147024890;

		// Token: 0x0400082F RID: 2095
		internal const int License = -2146232063;

		// Token: 0x04000830 RID: 2096
		internal const int InternalBufferOverflow = -2146232059;

		// Token: 0x04000831 RID: 2097
		internal const int ServiceControllerTimeout = -2146232058;

		// Token: 0x04000832 RID: 2098
		internal const int Install = -2146232057;

		// Token: 0x04000833 RID: 2099
		internal const int EFail = -2147467259;
	}
}
