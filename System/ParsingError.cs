﻿using System;

namespace System
{
	// Token: 0x0200009D RID: 157
	internal enum ParsingError
	{
		// Token: 0x040008E2 RID: 2274
		None,
		// Token: 0x040008E3 RID: 2275
		BadFormat,
		// Token: 0x040008E4 RID: 2276
		BadScheme,
		// Token: 0x040008E5 RID: 2277
		BadAuthority,
		// Token: 0x040008E6 RID: 2278
		EmptyUriString,
		// Token: 0x040008E7 RID: 2279
		LastRelativeUriOkErrIndex = 4,
		// Token: 0x040008E8 RID: 2280
		SchemeLimit,
		// Token: 0x040008E9 RID: 2281
		SizeLimit,
		// Token: 0x040008EA RID: 2282
		MustRootedPath,
		// Token: 0x040008EB RID: 2283
		BadHostName,
		// Token: 0x040008EC RID: 2284
		NonEmptyHost,
		// Token: 0x040008ED RID: 2285
		BadPort,
		// Token: 0x040008EE RID: 2286
		BadAuthorityTerminator,
		// Token: 0x040008EF RID: 2287
		CannotCreateRelative
	}
}
