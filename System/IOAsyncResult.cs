﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace System
{
	// Token: 0x020000B5 RID: 181
	[StructLayout(LayoutKind.Sequential)]
	internal abstract class IOAsyncResult : IAsyncResult
	{
		// Token: 0x06000435 RID: 1077 RVA: 0x0000232F File Offset: 0x0000052F
		protected IOAsyncResult()
		{
		}

		// Token: 0x06000436 RID: 1078 RVA: 0x000155C7 File Offset: 0x000137C7
		protected void Init(AsyncCallback async_callback, object async_state)
		{
			this.async_callback = async_callback;
			this.async_state = async_state;
			this.completed = false;
			this.completed_synchronously = false;
			if (this.wait_handle != null)
			{
				this.wait_handle.Reset();
			}
		}

		// Token: 0x06000437 RID: 1079 RVA: 0x000155F9 File Offset: 0x000137F9
		protected IOAsyncResult(AsyncCallback async_callback, object async_state)
		{
			this.async_callback = async_callback;
			this.async_state = async_state;
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000438 RID: 1080 RVA: 0x0001560F File Offset: 0x0001380F
		public AsyncCallback AsyncCallback
		{
			get
			{
				return this.async_callback;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x06000439 RID: 1081 RVA: 0x00015617 File Offset: 0x00013817
		public object AsyncState
		{
			get
			{
				return this.async_state;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x0600043A RID: 1082 RVA: 0x00015620 File Offset: 0x00013820
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				WaitHandle result;
				lock (this)
				{
					if (this.wait_handle == null)
					{
						this.wait_handle = new ManualResetEvent(this.completed);
					}
					result = this.wait_handle;
				}
				return result;
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x0600043B RID: 1083 RVA: 0x00015678 File Offset: 0x00013878
		// (set) Token: 0x0600043C RID: 1084 RVA: 0x00015680 File Offset: 0x00013880
		public bool CompletedSynchronously
		{
			get
			{
				return this.completed_synchronously;
			}
			protected set
			{
				this.completed_synchronously = value;
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x0600043D RID: 1085 RVA: 0x00015689 File Offset: 0x00013889
		// (set) Token: 0x0600043E RID: 1086 RVA: 0x00015694 File Offset: 0x00013894
		public bool IsCompleted
		{
			get
			{
				return this.completed;
			}
			protected set
			{
				this.completed = value;
				lock (this)
				{
					if (value && this.wait_handle != null)
					{
						this.wait_handle.Set();
					}
				}
			}
		}

		// Token: 0x0600043F RID: 1087
		internal abstract void CompleteDisposed();

		// Token: 0x04000965 RID: 2405
		private AsyncCallback async_callback;

		// Token: 0x04000966 RID: 2406
		private object async_state;

		// Token: 0x04000967 RID: 2407
		private ManualResetEvent wait_handle;

		// Token: 0x04000968 RID: 2408
		private bool completed_synchronously;

		// Token: 0x04000969 RID: 2409
		private bool completed;
	}
}
