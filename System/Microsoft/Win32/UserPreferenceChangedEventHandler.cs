﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Represents the method that will handle the <see cref="E:Microsoft.Win32.SystemEvents.UserPreferenceChanged" /> event.</summary>
	/// <param name="sender">The source of the event. When this event is raised by the <see cref="T:Microsoft.Win32.SystemEvents" /> class, this object is always <see langword="null" />. </param>
	/// <param name="e">A <see cref="T:Microsoft.Win32.UserPreferenceChangedEventArgs" /> that contains the event data. </param>
	// Token: 0x020006F2 RID: 1778
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class UserPreferenceChangedEventHandler : MulticastDelegate
	{
		// Token: 0x060037CE RID: 14286 RVA: 0x000092E2 File Offset: 0x000074E2
		public UserPreferenceChangedEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060037CF RID: 14287 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, UserPreferenceChangedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060037D0 RID: 14288 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, UserPreferenceChangedEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060037D1 RID: 14289 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
