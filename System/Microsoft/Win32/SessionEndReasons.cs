﻿using System;

namespace Microsoft.Win32
{
	/// <summary>Defines identifiers that represent how the current logon session is ending.</summary>
	// Token: 0x020006E8 RID: 1768
	public enum SessionEndReasons
	{
		/// <summary>The user is logging off and ending the current user session. The operating system continues to run.</summary>
		// Token: 0x04002541 RID: 9537
		Logoff = 1,
		/// <summary>The operating system is shutting down.</summary>
		// Token: 0x04002542 RID: 9538
		SystemShutdown
	}
}
