﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Represents the method that will handle the <see cref="E:Microsoft.Win32.SystemEvents.SessionEnding" /> event from the operating system.</summary>
	/// <param name="sender">The source of the event. When this event is raised by the <see cref="T:Microsoft.Win32.SystemEvents" /> class, this object is always <see langword="null" />. </param>
	/// <param name="e">A <see cref="T:Microsoft.Win32.SessionEndingEventArgs" /> that contains the event data. </param>
	// Token: 0x020006EB RID: 1771
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class SessionEndingEventHandler : MulticastDelegate
	{
		// Token: 0x0600379E RID: 14238 RVA: 0x000092E2 File Offset: 0x000074E2
		public SessionEndingEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600379F RID: 14239 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, SessionEndingEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060037A0 RID: 14240 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, SessionEndingEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060037A1 RID: 14241 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
