﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Provides access to system event notifications. This class cannot be inherited.</summary>
	// Token: 0x020006EF RID: 1775
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public sealed class SystemEvents
	{
		// Token: 0x060037A8 RID: 14248 RVA: 0x000092E2 File Offset: 0x000074E2
		internal SystemEvents()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Occurs when the user changes the display settings.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x14000065 RID: 101
		// (add) Token: 0x060037A9 RID: 14249 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037AA RID: 14250 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event EventHandler DisplaySettingsChanged
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the display settings are changing.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x14000066 RID: 102
		// (add) Token: 0x060037AB RID: 14251 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037AC RID: 14252 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event EventHandler DisplaySettingsChanging
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs before the thread that listens for system events is terminated.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x14000067 RID: 103
		// (add) Token: 0x060037AD RID: 14253 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037AE RID: 14254 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event EventHandler EventsThreadShutdown
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the user adds fonts to or removes fonts from the system.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x14000068 RID: 104
		// (add) Token: 0x060037AF RID: 14255 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037B0 RID: 14256 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event EventHandler InstalledFontsChanged
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the system is running out of available RAM.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x14000069 RID: 105
		// (add) Token: 0x060037B1 RID: 14257 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037B2 RID: 14258 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event EventHandler LowMemory
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the user switches to an application that uses a different palette.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x1400006A RID: 106
		// (add) Token: 0x060037B3 RID: 14259 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037B4 RID: 14260 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event EventHandler PaletteChanged
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the user suspends or resumes the system.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x1400006B RID: 107
		// (add) Token: 0x060037B5 RID: 14261 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037B6 RID: 14262 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event PowerModeChangedEventHandler PowerModeChanged
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the user is logging off or shutting down the system.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x1400006C RID: 108
		// (add) Token: 0x060037B7 RID: 14263 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037B8 RID: 14264 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event SessionEndedEventHandler SessionEnded
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the user is trying to log off or shut down the system.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x1400006D RID: 109
		// (add) Token: 0x060037B9 RID: 14265 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037BA RID: 14266 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event SessionEndingEventHandler SessionEnding
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the currently logged-in user has changed.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x1400006E RID: 110
		// (add) Token: 0x060037BB RID: 14267 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037BC RID: 14268 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event SessionSwitchEventHandler SessionSwitch
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the user changes the time on the system clock.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x1400006F RID: 111
		// (add) Token: 0x060037BD RID: 14269 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037BE RID: 14270 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event EventHandler TimeChanged
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when a windows timer interval has expired.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x14000070 RID: 112
		// (add) Token: 0x060037BF RID: 14271 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037C0 RID: 14272 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event TimerElapsedEventHandler TimerElapsed
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when a user preference has changed.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x14000071 RID: 113
		// (add) Token: 0x060037C1 RID: 14273 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037C2 RID: 14274 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event UserPreferenceChangedEventHandler UserPreferenceChanged
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when a user preference is changing.</summary>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x14000072 RID: 114
		// (add) Token: 0x060037C3 RID: 14275 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060037C4 RID: 14276 RVA: 0x000092E2 File Offset: 0x000074E2
		public static event UserPreferenceChangingEventHandler UserPreferenceChanging
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates a new window timer associated with the system events window.</summary>
		/// <param name="interval">Specifies the interval between timer notifications, in milliseconds.</param>
		/// <returns>The ID of the new timer.</returns>
		/// <exception cref="T:System.ArgumentException">The interval is less than or equal to zero. </exception>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed, or the attempt to create the timer did not succeed.</exception>
		// Token: 0x060037C5 RID: 14277 RVA: 0x000A5F14 File Offset: 0x000A4114
		public static IntPtr CreateTimer(int interval)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Invokes the specified delegate using the thread that listens for system events.</summary>
		/// <param name="method">A delegate to invoke using the thread that listens for system events. </param>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed.</exception>
		// Token: 0x060037C6 RID: 14278 RVA: 0x000092E2 File Offset: 0x000074E2
		public static void InvokeOnEventsThread(Delegate method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Terminates the timer specified by the given id.</summary>
		/// <param name="timerId">The ID of the timer to terminate. </param>
		/// <exception cref="T:System.InvalidOperationException">System event notifications are not supported under the current context. Server processes, for example, might not support global system event notifications.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The attempt to create a system events window thread did not succeed, or the attempt to terminate the timer did not succeed. </exception>
		// Token: 0x060037C7 RID: 14279 RVA: 0x000092E2 File Offset: 0x000074E2
		public static void KillTimer(IntPtr timerId)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
