﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Represents the method that will handle the <see cref="E:Microsoft.Win32.SystemEvents.TimerElapsed" /> event.</summary>
	/// <param name="sender">The source of the event. When this event is raised by the <see cref="T:Microsoft.Win32.SystemEvents" /> class, this object is always <see langword="null" />. </param>
	/// <param name="e">A <see cref="T:Microsoft.Win32.TimerElapsedEventArgs" /> that contains the event data. </param>
	// Token: 0x020006F0 RID: 1776
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class TimerElapsedEventHandler : MulticastDelegate
	{
		// Token: 0x060037C8 RID: 14280 RVA: 0x000092E2 File Offset: 0x000074E2
		public TimerElapsedEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060037C9 RID: 14281 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, TimerElapsedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060037CA RID: 14282 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, TimerElapsedEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060037CB RID: 14283 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
