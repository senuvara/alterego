﻿using System;

namespace Microsoft.Win32
{
	/// <summary>Defines identifiers used to represent the type of a session switch event.</summary>
	// Token: 0x020006ED RID: 1773
	public enum SessionSwitchReason
	{
		/// <summary>A session has been connected from the console.</summary>
		// Token: 0x04002544 RID: 9540
		ConsoleConnect = 1,
		/// <summary>A session has been disconnected from the console.</summary>
		// Token: 0x04002545 RID: 9541
		ConsoleDisconnect,
		/// <summary>A session has been connected from a remote connection.</summary>
		// Token: 0x04002546 RID: 9542
		RemoteConnect,
		/// <summary>A session has been disconnected from a remote connection.</summary>
		// Token: 0x04002547 RID: 9543
		RemoteDisconnect,
		/// <summary>A session has been locked.</summary>
		// Token: 0x04002548 RID: 9544
		SessionLock = 7,
		/// <summary>A user has logged off from a session.</summary>
		// Token: 0x04002549 RID: 9545
		SessionLogoff = 6,
		/// <summary>A user has logged on to a session.</summary>
		// Token: 0x0400254A RID: 9546
		SessionLogon = 5,
		/// <summary>A session has changed its status to or from remote controlled mode.</summary>
		// Token: 0x0400254B RID: 9547
		SessionRemoteControl = 9,
		/// <summary>A session has been unlocked.</summary>
		// Token: 0x0400254C RID: 9548
		SessionUnlock = 8
	}
}
