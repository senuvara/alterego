﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Represents the method that will handle the <see cref="E:Microsoft.Win32.SystemEvents.PowerModeChanged" /> event.</summary>
	/// <param name="sender">The source of the event. When this event is raised by the <see cref="T:Microsoft.Win32.SystemEvents" /> class, this object is always <see langword="null" />. </param>
	/// <param name="e">A <see cref="T:Microsoft.Win32.PowerModeChangedEventArgs" /> that contains the event data. </param>
	// Token: 0x020006E6 RID: 1766
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class PowerModeChangedEventHandler : MulticastDelegate
	{
		// Token: 0x06003790 RID: 14224 RVA: 0x000092E2 File Offset: 0x000074E2
		public PowerModeChangedEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003791 RID: 14225 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, PowerModeChangedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003792 RID: 14226 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, PowerModeChangedEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06003793 RID: 14227 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
