﻿using System;
using Unity;

namespace Microsoft.Win32.SafeHandles
{
	/// <summary>Provides a wrapper class that represents the handle of an X.509 chain object. For more information, see <see cref="T:System.Security.Cryptography.X509Certificates.X509Chain" />.</summary>
	// Token: 0x02000088 RID: 136
	public sealed class SafeX509ChainHandle : SafeHandleZeroOrMinusOneIsInvalid
	{
		// Token: 0x06000302 RID: 770 RVA: 0x000092D4 File Offset: 0x000074D4
		internal SafeX509ChainHandle(IntPtr handle) : base(true)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000303 RID: 771 RVA: 0x000068D7 File Offset: 0x00004AD7
		[MonoTODO]
		protected override bool ReleaseHandle()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000304 RID: 772 RVA: 0x000092E2 File Offset: 0x000074E2
		internal SafeX509ChainHandle()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
