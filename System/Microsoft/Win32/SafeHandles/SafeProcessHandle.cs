﻿using System;
using System.Security;
using System.Security.Permissions;

namespace Microsoft.Win32.SafeHandles
{
	/// <summary>Provides a managed wrapper for a process handle.</summary>
	// Token: 0x02000087 RID: 135
	[SuppressUnmanagedCodeSecurity]
	public sealed class SafeProcessHandle : SafeHandleZeroOrMinusOneIsInvalid
	{
		// Token: 0x060002FC RID: 764 RVA: 0x00009284 File Offset: 0x00007484
		internal SafeProcessHandle() : base(true)
		{
		}

		// Token: 0x060002FD RID: 765 RVA: 0x0000928D File Offset: 0x0000748D
		internal SafeProcessHandle(IntPtr handle) : base(true)
		{
			base.SetHandle(handle);
		}

		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.Win32.SafeHandles.SafeProcessHandle" /> class from the specified handle, indicating whether to release the handle during the finalization phase. </summary>
		/// <param name="existingHandle">The handle to be wrapped.</param>
		/// <param name="ownsHandle">
		///       <see langword="true" /> to reliably let <see cref="T:Microsoft.Win32.SafeHandles.SafeProcessHandle" /> release the handle during the finalization phase; otherwise, <see langword="false" />.</param>
		// Token: 0x060002FE RID: 766 RVA: 0x0000929D File Offset: 0x0000749D
		[SecurityPermission(SecurityAction.LinkDemand, UnmanagedCode = true)]
		public SafeProcessHandle(IntPtr existingHandle, bool ownsHandle) : base(ownsHandle)
		{
			base.SetHandle(existingHandle);
		}

		// Token: 0x060002FF RID: 767 RVA: 0x000092AD File Offset: 0x000074AD
		internal void InitialSetHandle(IntPtr h)
		{
			this.handle = h;
		}

		// Token: 0x06000300 RID: 768 RVA: 0x000092B6 File Offset: 0x000074B6
		protected override bool ReleaseHandle()
		{
			return NativeMethods.CloseProcess(this.handle);
		}

		// Token: 0x06000301 RID: 769 RVA: 0x000092C3 File Offset: 0x000074C3
		// Note: this type is marked as 'beforefieldinit'.
		static SafeProcessHandle()
		{
		}

		// Token: 0x040007FD RID: 2045
		internal static SafeProcessHandle InvalidHandle = new SafeProcessHandle(IntPtr.Zero);
	}
}
