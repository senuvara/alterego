﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Provides data for the <see cref="E:Microsoft.Win32.SystemEvents.UserPreferenceChanging" /> event.</summary>
	// Token: 0x020006F6 RID: 1782
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public class UserPreferenceChangingEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.Win32.UserPreferenceChangingEventArgs" /> class using the specified user preference category identifier.</summary>
		/// <param name="category">One of the <see cref="T:Microsoft.Win32.UserPreferenceCategory" /> values that indicate the user preference category that is changing. </param>
		// Token: 0x060037D8 RID: 14296 RVA: 0x000092E2 File Offset: 0x000074E2
		public UserPreferenceChangingEventArgs(UserPreferenceCategory category)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the category of user preferences that is changing.</summary>
		/// <returns>One of the <see cref="T:Microsoft.Win32.UserPreferenceCategory" /> values that indicates the category of user preferences that is changing.</returns>
		// Token: 0x17000E1A RID: 3610
		// (get) Token: 0x060037D9 RID: 14297 RVA: 0x000A5F68 File Offset: 0x000A4168
		public UserPreferenceCategory Category
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (UserPreferenceCategory)0;
			}
		}
	}
}
