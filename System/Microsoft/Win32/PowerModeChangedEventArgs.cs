﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Provides data for the <see cref="E:Microsoft.Win32.SystemEvents.PowerModeChanged" /> event.</summary>
	// Token: 0x020006E4 RID: 1764
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public class PowerModeChangedEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.Win32.PowerModeChangedEventArgs" /> class using the specified power mode event type.</summary>
		/// <param name="mode">One of the <see cref="T:Microsoft.Win32.PowerModes" /> values that represents the type of power mode event. </param>
		// Token: 0x0600378E RID: 14222 RVA: 0x000092E2 File Offset: 0x000074E2
		public PowerModeChangedEventArgs(PowerModes mode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an identifier that indicates the type of the power mode event that has occurred.</summary>
		/// <returns>One of the <see cref="T:Microsoft.Win32.PowerModes" /> values.</returns>
		// Token: 0x17000E13 RID: 3603
		// (get) Token: 0x0600378F RID: 14223 RVA: 0x000A5E88 File Offset: 0x000A4088
		public PowerModes Mode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (PowerModes)0;
			}
		}
	}
}
