﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Provides data for the <see cref="E:Microsoft.Win32.SystemEvents.SessionSwitch" /> event.</summary>
	// Token: 0x020006EC RID: 1772
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class SessionSwitchEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.Win32.SessionSwitchEventArgs" /> class using the specified session change event type identifer.</summary>
		/// <param name="reason">A <see cref="T:Microsoft.Win32.SessionSwitchReason" /> that indicates the type of session change event. </param>
		// Token: 0x060037A2 RID: 14242 RVA: 0x000092E2 File Offset: 0x000074E2
		public SessionSwitchEventArgs(SessionSwitchReason reason)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an identifier that indicates the type of session change event.</summary>
		/// <returns>A <see cref="T:Microsoft.Win32.SessionSwitchReason" /> indicating the type of the session change event.</returns>
		// Token: 0x17000E17 RID: 3607
		// (get) Token: 0x060037A3 RID: 14243 RVA: 0x000A5EF8 File Offset: 0x000A40F8
		public SessionSwitchReason Reason
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (SessionSwitchReason)0;
			}
		}
	}
}
