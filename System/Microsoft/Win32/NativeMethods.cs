﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace Microsoft.Win32
{
	// Token: 0x02000086 RID: 134
	internal static class NativeMethods
	{
		// Token: 0x060002E7 RID: 743 RVA: 0x00008FA4 File Offset: 0x000071A4
		public static bool DuplicateHandle(HandleRef hSourceProcessHandle, SafeHandle hSourceHandle, HandleRef hTargetProcess, out SafeWaitHandle targetHandle, int dwDesiredAccess, bool bInheritHandle, int dwOptions)
		{
			bool flag = false;
			bool result;
			try
			{
				hSourceHandle.DangerousAddRef(ref flag);
				IntPtr existingHandle;
				MonoIOError monoIOError;
				bool flag2 = MonoIO.DuplicateHandle(hSourceProcessHandle.Handle, hSourceHandle.DangerousGetHandle(), hTargetProcess.Handle, out existingHandle, dwDesiredAccess, bInheritHandle ? 1 : 0, dwOptions, out monoIOError);
				if (monoIOError != MonoIOError.ERROR_SUCCESS)
				{
					throw MonoIO.GetException(monoIOError);
				}
				targetHandle = new SafeWaitHandle(existingHandle, true);
				result = flag2;
			}
			finally
			{
				if (flag)
				{
					hSourceHandle.DangerousRelease();
				}
			}
			return result;
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x00009018 File Offset: 0x00007218
		public static bool DuplicateHandle(HandleRef hSourceProcessHandle, HandleRef hSourceHandle, HandleRef hTargetProcess, out SafeProcessHandle targetHandle, int dwDesiredAccess, bool bInheritHandle, int dwOptions)
		{
			IntPtr existingHandle;
			MonoIOError monoIOError;
			bool result = MonoIO.DuplicateHandle(hSourceProcessHandle.Handle, hSourceHandle.Handle, hTargetProcess.Handle, out existingHandle, dwDesiredAccess, bInheritHandle ? 1 : 0, dwOptions, out monoIOError);
			if (monoIOError != MonoIOError.ERROR_SUCCESS)
			{
				throw MonoIO.GetException(monoIOError);
			}
			targetHandle = new SafeProcessHandle(existingHandle, true);
			return result;
		}

		// Token: 0x060002E9 RID: 745
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr GetCurrentProcess();

		// Token: 0x060002EA RID: 746
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetExitCodeProcess(IntPtr processHandle, out int exitCode);

		// Token: 0x060002EB RID: 747 RVA: 0x00009064 File Offset: 0x00007264
		public static bool GetExitCodeProcess(SafeProcessHandle processHandle, out int exitCode)
		{
			bool flag = false;
			bool exitCodeProcess;
			try
			{
				processHandle.DangerousAddRef(ref flag);
				exitCodeProcess = NativeMethods.GetExitCodeProcess(processHandle.DangerousGetHandle(), out exitCode);
			}
			finally
			{
				if (flag)
				{
					processHandle.DangerousRelease();
				}
			}
			return exitCodeProcess;
		}

		// Token: 0x060002EC RID: 748
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool TerminateProcess(IntPtr processHandle, int exitCode);

		// Token: 0x060002ED RID: 749 RVA: 0x000090A8 File Offset: 0x000072A8
		public static bool TerminateProcess(SafeProcessHandle processHandle, int exitCode)
		{
			bool flag = false;
			bool result;
			try
			{
				processHandle.DangerousAddRef(ref flag);
				result = NativeMethods.TerminateProcess(processHandle.DangerousGetHandle(), exitCode);
			}
			finally
			{
				if (flag)
				{
					processHandle.DangerousRelease();
				}
			}
			return result;
		}

		// Token: 0x060002EE RID: 750
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int WaitForInputIdle(IntPtr handle, int milliseconds);

		// Token: 0x060002EF RID: 751 RVA: 0x000090EC File Offset: 0x000072EC
		public static int WaitForInputIdle(SafeProcessHandle handle, int milliseconds)
		{
			bool flag = false;
			int result;
			try
			{
				handle.DangerousAddRef(ref flag);
				result = NativeMethods.WaitForInputIdle(handle.DangerousGetHandle(), milliseconds);
			}
			finally
			{
				if (flag)
				{
					handle.DangerousRelease();
				}
			}
			return result;
		}

		// Token: 0x060002F0 RID: 752
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetProcessWorkingSetSize(IntPtr handle, out IntPtr min, out IntPtr max);

		// Token: 0x060002F1 RID: 753 RVA: 0x00009130 File Offset: 0x00007330
		public static bool GetProcessWorkingSetSize(SafeProcessHandle handle, out IntPtr min, out IntPtr max)
		{
			bool flag = false;
			bool processWorkingSetSize;
			try
			{
				handle.DangerousAddRef(ref flag);
				processWorkingSetSize = NativeMethods.GetProcessWorkingSetSize(handle.DangerousGetHandle(), out min, out max);
			}
			finally
			{
				if (flag)
				{
					handle.DangerousRelease();
				}
			}
			return processWorkingSetSize;
		}

		// Token: 0x060002F2 RID: 754
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SetProcessWorkingSetSize(IntPtr handle, IntPtr min, IntPtr max);

		// Token: 0x060002F3 RID: 755 RVA: 0x00009174 File Offset: 0x00007374
		public static bool SetProcessWorkingSetSize(SafeProcessHandle handle, IntPtr min, IntPtr max)
		{
			bool flag = false;
			bool result;
			try
			{
				handle.DangerousAddRef(ref flag);
				result = NativeMethods.SetProcessWorkingSetSize(handle.DangerousGetHandle(), min, max);
			}
			finally
			{
				if (flag)
				{
					handle.DangerousRelease();
				}
			}
			return result;
		}

		// Token: 0x060002F4 RID: 756
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetProcessTimes(IntPtr handle, out long creation, out long exit, out long kernel, out long user);

		// Token: 0x060002F5 RID: 757 RVA: 0x000091B8 File Offset: 0x000073B8
		public static bool GetProcessTimes(SafeProcessHandle handle, out long creation, out long exit, out long kernel, out long user)
		{
			bool flag = false;
			bool processTimes;
			try
			{
				handle.DangerousAddRef(ref flag);
				processTimes = NativeMethods.GetProcessTimes(handle.DangerousGetHandle(), out creation, out exit, out kernel, out user);
			}
			finally
			{
				if (flag)
				{
					handle.DangerousRelease();
				}
			}
			return processTimes;
		}

		// Token: 0x060002F6 RID: 758
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetCurrentProcessId();

		// Token: 0x060002F7 RID: 759
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetPriorityClass(IntPtr handle);

		// Token: 0x060002F8 RID: 760 RVA: 0x00009200 File Offset: 0x00007400
		public static int GetPriorityClass(SafeProcessHandle handle)
		{
			bool flag = false;
			int priorityClass;
			try
			{
				handle.DangerousAddRef(ref flag);
				priorityClass = NativeMethods.GetPriorityClass(handle.DangerousGetHandle());
			}
			finally
			{
				if (flag)
				{
					handle.DangerousRelease();
				}
			}
			return priorityClass;
		}

		// Token: 0x060002F9 RID: 761
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SetPriorityClass(IntPtr handle, int priorityClass);

		// Token: 0x060002FA RID: 762 RVA: 0x00009240 File Offset: 0x00007440
		public static bool SetPriorityClass(SafeProcessHandle handle, int priorityClass)
		{
			bool flag = false;
			bool result;
			try
			{
				handle.DangerousAddRef(ref flag);
				result = NativeMethods.SetPriorityClass(handle.DangerousGetHandle(), priorityClass);
			}
			finally
			{
				if (flag)
				{
					handle.DangerousRelease();
				}
			}
			return result;
		}

		// Token: 0x060002FB RID: 763
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool CloseProcess(IntPtr handle);

		// Token: 0x040007DD RID: 2013
		public const int E_ABORT = -2147467260;

		// Token: 0x040007DE RID: 2014
		public const int PROCESS_TERMINATE = 1;

		// Token: 0x040007DF RID: 2015
		public const int PROCESS_CREATE_THREAD = 2;

		// Token: 0x040007E0 RID: 2016
		public const int PROCESS_SET_SESSIONID = 4;

		// Token: 0x040007E1 RID: 2017
		public const int PROCESS_VM_OPERATION = 8;

		// Token: 0x040007E2 RID: 2018
		public const int PROCESS_VM_READ = 16;

		// Token: 0x040007E3 RID: 2019
		public const int PROCESS_VM_WRITE = 32;

		// Token: 0x040007E4 RID: 2020
		public const int PROCESS_DUP_HANDLE = 64;

		// Token: 0x040007E5 RID: 2021
		public const int PROCESS_CREATE_PROCESS = 128;

		// Token: 0x040007E6 RID: 2022
		public const int PROCESS_SET_QUOTA = 256;

		// Token: 0x040007E7 RID: 2023
		public const int PROCESS_SET_INFORMATION = 512;

		// Token: 0x040007E8 RID: 2024
		public const int PROCESS_QUERY_INFORMATION = 1024;

		// Token: 0x040007E9 RID: 2025
		public const int PROCESS_QUERY_LIMITED_INFORMATION = 4096;

		// Token: 0x040007EA RID: 2026
		public const int STANDARD_RIGHTS_REQUIRED = 983040;

		// Token: 0x040007EB RID: 2027
		public const int SYNCHRONIZE = 1048576;

		// Token: 0x040007EC RID: 2028
		public const int PROCESS_ALL_ACCESS = 2035711;

		// Token: 0x040007ED RID: 2029
		public const int DUPLICATE_CLOSE_SOURCE = 1;

		// Token: 0x040007EE RID: 2030
		public const int DUPLICATE_SAME_ACCESS = 2;

		// Token: 0x040007EF RID: 2031
		public const int STILL_ACTIVE = 259;

		// Token: 0x040007F0 RID: 2032
		public const int WAIT_OBJECT_0 = 0;

		// Token: 0x040007F1 RID: 2033
		public const int WAIT_FAILED = -1;

		// Token: 0x040007F2 RID: 2034
		public const int WAIT_TIMEOUT = 258;

		// Token: 0x040007F3 RID: 2035
		public const int WAIT_ABANDONED = 128;

		// Token: 0x040007F4 RID: 2036
		public const int WAIT_ABANDONED_0 = 128;

		// Token: 0x040007F5 RID: 2037
		public const int ERROR_FILE_NOT_FOUND = 2;

		// Token: 0x040007F6 RID: 2038
		public const int ERROR_PATH_NOT_FOUND = 3;

		// Token: 0x040007F7 RID: 2039
		public const int ERROR_ACCESS_DENIED = 5;

		// Token: 0x040007F8 RID: 2040
		public const int ERROR_INVALID_HANDLE = 6;

		// Token: 0x040007F9 RID: 2041
		public const int ERROR_SHARING_VIOLATION = 32;

		// Token: 0x040007FA RID: 2042
		public const int ERROR_INVALID_NAME = 123;

		// Token: 0x040007FB RID: 2043
		public const int ERROR_ALREADY_EXISTS = 183;

		// Token: 0x040007FC RID: 2044
		public const int ERROR_FILENAME_EXCED_RANGE = 206;
	}
}
