﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Provides data for the <see cref="E:Microsoft.Win32.SystemEvents.SessionEnding" /> event.</summary>
	// Token: 0x020006EA RID: 1770
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class SessionEndingEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.Win32.SessionEndingEventArgs" /> class using the specified value indicating the type of session close event that is occurring.</summary>
		/// <param name="reason">One of the <see cref="T:Microsoft.Win32.SessionEndReasons" /> that specifies how the session ends. </param>
		// Token: 0x0600379A RID: 14234 RVA: 0x000092E2 File Offset: 0x000074E2
		public SessionEndingEventArgs(SessionEndReasons reason)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a value indicating whether to cancel the user request to end the session.</summary>
		/// <returns>
		///     <see langword="true" /> to cancel the user request to end the session; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000E15 RID: 3605
		// (get) Token: 0x0600379B RID: 14235 RVA: 0x000A5EC0 File Offset: 0x000A40C0
		// (set) Token: 0x0600379C RID: 14236 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool Cancel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the reason the session is ending.</summary>
		/// <returns>One of the <see cref="T:Microsoft.Win32.SessionEndReasons" /> values that specifies how the session is ending.</returns>
		// Token: 0x17000E16 RID: 3606
		// (get) Token: 0x0600379D RID: 14237 RVA: 0x000A5EDC File Offset: 0x000A40DC
		public SessionEndReasons Reason
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (SessionEndReasons)0;
			}
		}
	}
}
