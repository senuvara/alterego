﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Represents the method that will handle the <see cref="E:Microsoft.Win32.SystemEvents.UserPreferenceChanging" /> event.</summary>
	/// <param name="sender">The source of the event. When this event is raised by the <see cref="T:Microsoft.Win32.SystemEvents" /> class, this object is always <see langword="null" />. </param>
	/// <param name="e">A <see cref="T:Microsoft.Win32.UserPreferenceChangedEventArgs" /> that contains the event data. </param>
	// Token: 0x020006F5 RID: 1781
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class UserPreferenceChangingEventHandler : MulticastDelegate
	{
		// Token: 0x060037D4 RID: 14292 RVA: 0x000092E2 File Offset: 0x000074E2
		public UserPreferenceChangingEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060037D5 RID: 14293 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, UserPreferenceChangingEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060037D6 RID: 14294 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, UserPreferenceChangingEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060037D7 RID: 14295 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
