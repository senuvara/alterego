﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Represents the method that will handle the <see cref="E:Microsoft.Win32.SystemEvents.SessionSwitch" /> event.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">A <see cref="T:Microsoft.Win32.SessionSwitchEventArgs" /> indicating the type of the session change event. </param>
	// Token: 0x020006EE RID: 1774
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class SessionSwitchEventHandler : MulticastDelegate
	{
		// Token: 0x060037A4 RID: 14244 RVA: 0x000092E2 File Offset: 0x000074E2
		public SessionSwitchEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060037A5 RID: 14245 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, SessionSwitchEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060037A6 RID: 14246 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, SessionSwitchEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060037A7 RID: 14247 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
