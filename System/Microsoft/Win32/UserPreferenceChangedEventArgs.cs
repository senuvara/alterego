﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Provides data for the <see cref="E:Microsoft.Win32.SystemEvents.UserPreferenceChanged" /> event.</summary>
	// Token: 0x020006F3 RID: 1779
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public class UserPreferenceChangedEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.Win32.UserPreferenceChangedEventArgs" /> class using the specified user preference category identifier.</summary>
		/// <param name="category">One of the <see cref="T:Microsoft.Win32.UserPreferenceCategory" /> values that indicates the user preference category that has changed. </param>
		// Token: 0x060037D2 RID: 14290 RVA: 0x000092E2 File Offset: 0x000074E2
		public UserPreferenceChangedEventArgs(UserPreferenceCategory category)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the category of user preferences that has changed.</summary>
		/// <returns>One of the <see cref="T:Microsoft.Win32.UserPreferenceCategory" /> values that indicates the category of user preferences that has changed.</returns>
		// Token: 0x17000E19 RID: 3609
		// (get) Token: 0x060037D3 RID: 14291 RVA: 0x000A5F4C File Offset: 0x000A414C
		public UserPreferenceCategory Category
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (UserPreferenceCategory)0;
			}
		}
	}
}
