﻿using System;
using System.Security.Permissions;
using Unity;

namespace Microsoft.Win32
{
	/// <summary>Provides data for the <see cref="E:Microsoft.Win32.SystemEvents.SessionEnded" /> event.</summary>
	// Token: 0x020006E7 RID: 1767
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class SessionEndedEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:Microsoft.Win32.SessionEndedEventArgs" /> class.</summary>
		/// <param name="reason">One of the <see cref="T:Microsoft.Win32.SessionEndReasons" /> values indicating how the session ended. </param>
		// Token: 0x06003794 RID: 14228 RVA: 0x000092E2 File Offset: 0x000074E2
		public SessionEndedEventArgs(SessionEndReasons reason)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an identifier that indicates how the session ended.</summary>
		/// <returns>One of the <see cref="T:Microsoft.Win32.SessionEndReasons" /> values that indicates how the session ended.</returns>
		// Token: 0x17000E14 RID: 3604
		// (get) Token: 0x06003795 RID: 14229 RVA: 0x000A5EA4 File Offset: 0x000A40A4
		public SessionEndReasons Reason
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (SessionEndReasons)0;
			}
		}
	}
}
