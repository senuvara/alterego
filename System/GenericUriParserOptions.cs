﻿using System;

namespace System
{
	/// <summary>Specifies options for a <see cref="T:System.UriParser" />.</summary>
	// Token: 0x0200008E RID: 142
	[Flags]
	public enum GenericUriParserOptions
	{
		/// <summary>The parser:</summary>
		// Token: 0x04000837 RID: 2103
		Default = 0,
		/// <summary>The parser allows a registry-based authority.</summary>
		// Token: 0x04000838 RID: 2104
		GenericAuthority = 1,
		/// <summary>The parser allows a URI with no authority.</summary>
		// Token: 0x04000839 RID: 2105
		AllowEmptyAuthority = 2,
		/// <summary>The scheme does not define a user information part.</summary>
		// Token: 0x0400083A RID: 2106
		NoUserInfo = 4,
		/// <summary>The scheme does not define a port.</summary>
		// Token: 0x0400083B RID: 2107
		NoPort = 8,
		/// <summary>The scheme does not define a query part.</summary>
		// Token: 0x0400083C RID: 2108
		NoQuery = 16,
		/// <summary>The scheme does not define a fragment part.</summary>
		// Token: 0x0400083D RID: 2109
		NoFragment = 32,
		/// <summary>The parser does not convert back slashes into forward slashes.</summary>
		// Token: 0x0400083E RID: 2110
		DontConvertPathBackslashes = 64,
		/// <summary>The parser does not canonicalize the URI.</summary>
		// Token: 0x0400083F RID: 2111
		DontCompressPath = 128,
		/// <summary>The parser does not unescape path dots, forward slashes, or back slashes.</summary>
		// Token: 0x04000840 RID: 2112
		DontUnescapePathDotsAndSlashes = 256,
		/// <summary>The parser supports Internationalized Domain Name (IDN) parsing (IDN) of host names. Whether IDN is used is dictated by configuration values. See the Remarks for more information.</summary>
		// Token: 0x04000841 RID: 2113
		Idn = 512,
		/// <summary>The parser supports the parsing rules specified in RFC 3987 for International Resource Identifiers (IRI). Whether IRI is used is dictated by configuration values. See the Remarks for more information.</summary>
		// Token: 0x04000842 RID: 2114
		IriParsing = 1024
	}
}
