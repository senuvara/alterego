﻿using System;

namespace System.IO
{
	/// <summary>Specifies changes to watch for in a file or folder.</summary>
	// Token: 0x020002C0 RID: 704
	[Flags]
	public enum NotifyFilters
	{
		/// <summary>The attributes of the file or folder.</summary>
		// Token: 0x040013D4 RID: 5076
		Attributes = 4,
		/// <summary>The time the file or folder was created.</summary>
		// Token: 0x040013D5 RID: 5077
		CreationTime = 64,
		/// <summary>The name of the directory.</summary>
		// Token: 0x040013D6 RID: 5078
		DirectoryName = 2,
		/// <summary>The name of the file.</summary>
		// Token: 0x040013D7 RID: 5079
		FileName = 1,
		/// <summary>The date the file or folder was last opened.</summary>
		// Token: 0x040013D8 RID: 5080
		LastAccess = 32,
		/// <summary>The date the file or folder last had anything written to it.</summary>
		// Token: 0x040013D9 RID: 5081
		LastWrite = 16,
		/// <summary>The security settings of the file or folder.</summary>
		// Token: 0x040013DA RID: 5082
		Security = 256,
		/// <summary>The size of the file or folder.</summary>
		// Token: 0x040013DB RID: 5083
		Size = 8
	}
}
