﻿using System;

namespace System.IO.Compression
{
	// Token: 0x020002CF RID: 719
	internal class ZLibNative
	{
		// Token: 0x060015A3 RID: 5539 RVA: 0x0000232F File Offset: 0x0000052F
		public ZLibNative()
		{
		}

		// Token: 0x040013FD RID: 5117
		public const int GZip_DefaultWindowBits = 31;
	}
}
