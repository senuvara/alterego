﻿using System;
using System.Runtime.InteropServices;
using Mono.Util;

namespace System.IO.Compression
{
	// Token: 0x020002CC RID: 716
	internal class DeflateStreamNative
	{
		// Token: 0x0600158B RID: 5515 RVA: 0x0000232F File Offset: 0x0000052F
		private DeflateStreamNative()
		{
		}

		// Token: 0x0600158C RID: 5516 RVA: 0x0004D910 File Offset: 0x0004BB10
		public static DeflateStreamNative Create(Stream compressedStream, CompressionMode mode, bool gzip)
		{
			DeflateStreamNative deflateStreamNative = new DeflateStreamNative();
			deflateStreamNative.data = GCHandle.Alloc(deflateStreamNative);
			deflateStreamNative.feeder = ((mode == CompressionMode.Compress) ? new DeflateStreamNative.UnmanagedReadOrWrite(DeflateStreamNative.UnmanagedWrite) : new DeflateStreamNative.UnmanagedReadOrWrite(DeflateStreamNative.UnmanagedRead));
			deflateStreamNative.z_stream = DeflateStreamNative.CreateZStream(mode, gzip, deflateStreamNative.feeder, GCHandle.ToIntPtr(deflateStreamNative.data));
			if (deflateStreamNative.z_stream.IsInvalid)
			{
				deflateStreamNative.Dispose(true);
				return null;
			}
			deflateStreamNative.base_stream = compressedStream;
			return deflateStreamNative;
		}

		// Token: 0x0600158D RID: 5517 RVA: 0x0004D990 File Offset: 0x0004BB90
		~DeflateStreamNative()
		{
			this.Dispose(false);
		}

		// Token: 0x0600158E RID: 5518 RVA: 0x0004D9C0 File Offset: 0x0004BBC0
		public void Dispose(bool disposing)
		{
			if (disposing && !this.disposed)
			{
				this.disposed = true;
				GC.SuppressFinalize(this);
				this.io_buffer = null;
				this.z_stream.Dispose();
			}
			if (this.data.IsAllocated)
			{
				this.data.Free();
			}
		}

		// Token: 0x0600158F RID: 5519 RVA: 0x0004DA0F File Offset: 0x0004BC0F
		public void Flush()
		{
			DeflateStreamNative.CheckResult(DeflateStreamNative.Flush(this.z_stream), "Flush");
		}

		// Token: 0x06001590 RID: 5520 RVA: 0x0004DA26 File Offset: 0x0004BC26
		public int ReadZStream(IntPtr buffer, int length)
		{
			int result = DeflateStreamNative.ReadZStream(this.z_stream, buffer, length);
			DeflateStreamNative.CheckResult(result, "ReadInternal");
			return result;
		}

		// Token: 0x06001591 RID: 5521 RVA: 0x0004DA40 File Offset: 0x0004BC40
		public void WriteZStream(IntPtr buffer, int length)
		{
			DeflateStreamNative.CheckResult(DeflateStreamNative.WriteZStream(this.z_stream, buffer, length), "WriteInternal");
		}

		// Token: 0x06001592 RID: 5522 RVA: 0x0004DA5C File Offset: 0x0004BC5C
		[MonoPInvokeCallback(typeof(DeflateStreamNative.UnmanagedReadOrWrite))]
		private static int UnmanagedRead(IntPtr buffer, int length, IntPtr data)
		{
			DeflateStreamNative deflateStreamNative = GCHandle.FromIntPtr(data).Target as DeflateStreamNative;
			if (deflateStreamNative == null)
			{
				return -1;
			}
			return deflateStreamNative.UnmanagedRead(buffer, length);
		}

		// Token: 0x06001593 RID: 5523 RVA: 0x0004DA8C File Offset: 0x0004BC8C
		private int UnmanagedRead(IntPtr buffer, int length)
		{
			if (this.io_buffer == null)
			{
				this.io_buffer = new byte[4096];
			}
			int count = Math.Min(length, this.io_buffer.Length);
			int num = this.base_stream.Read(this.io_buffer, 0, count);
			if (num > 0)
			{
				Marshal.Copy(this.io_buffer, 0, buffer, num);
			}
			return num;
		}

		// Token: 0x06001594 RID: 5524 RVA: 0x0004DAE8 File Offset: 0x0004BCE8
		[MonoPInvokeCallback(typeof(DeflateStreamNative.UnmanagedReadOrWrite))]
		private static int UnmanagedWrite(IntPtr buffer, int length, IntPtr data)
		{
			DeflateStreamNative deflateStreamNative = GCHandle.FromIntPtr(data).Target as DeflateStreamNative;
			if (deflateStreamNative == null)
			{
				return -1;
			}
			return deflateStreamNative.UnmanagedWrite(buffer, length);
		}

		// Token: 0x06001595 RID: 5525 RVA: 0x0004DB18 File Offset: 0x0004BD18
		private unsafe int UnmanagedWrite(IntPtr buffer, int length)
		{
			int num = 0;
			while (length > 0)
			{
				if (this.io_buffer == null)
				{
					this.io_buffer = new byte[4096];
				}
				int num2 = Math.Min(length, this.io_buffer.Length);
				Marshal.Copy(buffer, this.io_buffer, 0, num2);
				this.base_stream.Write(this.io_buffer, 0, num2);
				buffer = new IntPtr((void*)((byte*)buffer.ToPointer() + num2));
				length -= num2;
				num += num2;
			}
			return num;
		}

		// Token: 0x06001596 RID: 5526 RVA: 0x0004DB90 File Offset: 0x0004BD90
		private static void CheckResult(int result, string where)
		{
			if (result >= 0)
			{
				return;
			}
			string str;
			switch (result)
			{
			case -11:
				str = "IO error";
				goto IL_82;
			case -10:
				str = "Invalid argument(s)";
				goto IL_82;
			case -6:
				str = "Invalid version";
				goto IL_82;
			case -5:
				str = "Internal error (no progress possible)";
				goto IL_82;
			case -4:
				str = "Not enough memory";
				goto IL_82;
			case -3:
				str = "Corrupted data";
				goto IL_82;
			case -2:
				str = "Internal error";
				goto IL_82;
			case -1:
				str = "Unknown error";
				goto IL_82;
			}
			str = "Unknown error";
			IL_82:
			throw new IOException(str + " " + where);
		}

		// Token: 0x06001597 RID: 5527
		[DllImport("MonoPosixHelper", CallingConvention = CallingConvention.Cdecl)]
		private static extern DeflateStreamNative.SafeDeflateStreamHandle CreateZStream(CompressionMode compress, bool gzip, DeflateStreamNative.UnmanagedReadOrWrite feeder, IntPtr data);

		// Token: 0x06001598 RID: 5528
		[DllImport("MonoPosixHelper", CallingConvention = CallingConvention.Cdecl)]
		private static extern int CloseZStream(IntPtr stream);

		// Token: 0x06001599 RID: 5529
		[DllImport("MonoPosixHelper", CallingConvention = CallingConvention.Cdecl)]
		private static extern int Flush(DeflateStreamNative.SafeDeflateStreamHandle stream);

		// Token: 0x0600159A RID: 5530
		[DllImport("MonoPosixHelper", CallingConvention = CallingConvention.Cdecl)]
		private static extern int ReadZStream(DeflateStreamNative.SafeDeflateStreamHandle stream, IntPtr buffer, int length);

		// Token: 0x0600159B RID: 5531
		[DllImport("MonoPosixHelper", CallingConvention = CallingConvention.Cdecl)]
		private static extern int WriteZStream(DeflateStreamNative.SafeDeflateStreamHandle stream, IntPtr buffer, int length);

		// Token: 0x040013F5 RID: 5109
		private const int BufferSize = 4096;

		// Token: 0x040013F6 RID: 5110
		private DeflateStreamNative.UnmanagedReadOrWrite feeder;

		// Token: 0x040013F7 RID: 5111
		private Stream base_stream;

		// Token: 0x040013F8 RID: 5112
		private DeflateStreamNative.SafeDeflateStreamHandle z_stream;

		// Token: 0x040013F9 RID: 5113
		private GCHandle data;

		// Token: 0x040013FA RID: 5114
		private bool disposed;

		// Token: 0x040013FB RID: 5115
		private byte[] io_buffer;

		// Token: 0x040013FC RID: 5116
		private const string LIBNAME = "MonoPosixHelper";

		// Token: 0x020002CD RID: 717
		// (Invoke) Token: 0x0600159D RID: 5533
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		private delegate int UnmanagedReadOrWrite(IntPtr buffer, int length, IntPtr data);

		// Token: 0x020002CE RID: 718
		private sealed class SafeDeflateStreamHandle : SafeHandle
		{
			// Token: 0x17000476 RID: 1142
			// (get) Token: 0x060015A0 RID: 5536 RVA: 0x0004DC30 File Offset: 0x0004BE30
			public override bool IsInvalid
			{
				get
				{
					return this.handle == IntPtr.Zero;
				}
			}

			// Token: 0x060015A1 RID: 5537 RVA: 0x0004DC42 File Offset: 0x0004BE42
			private SafeDeflateStreamHandle() : base(IntPtr.Zero, true)
			{
			}

			// Token: 0x060015A2 RID: 5538 RVA: 0x0004DC50 File Offset: 0x0004BE50
			protected override bool ReleaseHandle()
			{
				DeflateStreamNative.CloseZStream(this.handle);
				return true;
			}
		}
	}
}
