﻿using System;

namespace System.IO.Compression
{
	/// <summary> Specifies whether to compress or decompress the underlying stream.</summary>
	// Token: 0x020002C7 RID: 711
	public enum CompressionMode
	{
		/// <summary>Decompresses the underlying stream.</summary>
		// Token: 0x040013ED RID: 5101
		Decompress,
		/// <summary>Compresses the underlying stream.</summary>
		// Token: 0x040013EE RID: 5102
		Compress
	}
}
