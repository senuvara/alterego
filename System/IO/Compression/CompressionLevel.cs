﻿using System;

namespace System.IO.Compression
{
	/// <summary>Specifies values that indicate whether a compression operation emphasizes speed or compression size.</summary>
	// Token: 0x020002C6 RID: 710
	public enum CompressionLevel
	{
		/// <summary>The compression operation should be optimally compressed, even if the operation takes a longer time to complete.</summary>
		// Token: 0x040013E9 RID: 5097
		Optimal,
		/// <summary>The compression operation should complete as quickly as possible, even if the resulting file is not optimally compressed.</summary>
		// Token: 0x040013EA RID: 5098
		Fastest,
		/// <summary>No compression should be performed on the file.</summary>
		// Token: 0x040013EB RID: 5099
		NoCompression
	}
}
