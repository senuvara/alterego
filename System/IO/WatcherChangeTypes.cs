﻿using System;

namespace System.IO
{
	/// <summary>Changes that might occur to a file or directory.</summary>
	// Token: 0x020002C4 RID: 708
	[Flags]
	public enum WatcherChangeTypes
	{
		/// <summary>The creation, deletion, change, or renaming of a file or folder.</summary>
		// Token: 0x040013E3 RID: 5091
		All = 15,
		/// <summary>The change of a file or folder. The types of changes include: changes to size, attributes, security settings, last write, and last access time.</summary>
		// Token: 0x040013E4 RID: 5092
		Changed = 4,
		/// <summary>The creation of a file or folder.</summary>
		// Token: 0x040013E5 RID: 5093
		Created = 1,
		/// <summary>The deletion of a file or folder.</summary>
		// Token: 0x040013E6 RID: 5094
		Deleted = 2,
		/// <summary>The renaming of a file or folder.</summary>
		// Token: 0x040013E7 RID: 5095
		Renamed = 8
	}
}
