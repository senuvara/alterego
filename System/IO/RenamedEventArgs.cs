﻿using System;

namespace System.IO
{
	/// <summary>Provides data for the <see cref="E:System.IO.FileSystemWatcher.Renamed" /> event.</summary>
	// Token: 0x020002C1 RID: 705
	public class RenamedEventArgs : FileSystemEventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.RenamedEventArgs" /> class.</summary>
		/// <param name="changeType">One of the <see cref="T:System.IO.WatcherChangeTypes" /> values. </param>
		/// <param name="directory">The name of the affected file or directory. </param>
		/// <param name="name">The name of the affected file or directory. </param>
		/// <param name="oldName">The old name of the affected file or directory. </param>
		// Token: 0x0600152C RID: 5420 RVA: 0x0004CF6C File Offset: 0x0004B16C
		public RenamedEventArgs(WatcherChangeTypes changeType, string directory, string name, string oldName) : base(changeType, directory, name)
		{
			this.oldName = oldName;
			this.oldFullPath = Path.Combine(directory, oldName);
		}

		/// <summary>Gets the previous fully qualified path of the affected file or directory.</summary>
		/// <returns>The previous fully qualified path of the affected file or directory.</returns>
		// Token: 0x17000464 RID: 1124
		// (get) Token: 0x0600152D RID: 5421 RVA: 0x0004CF8D File Offset: 0x0004B18D
		public string OldFullPath
		{
			get
			{
				return this.oldFullPath;
			}
		}

		/// <summary>Gets the old name of the affected file or directory.</summary>
		/// <returns>The previous name of the affected file or directory.</returns>
		// Token: 0x17000465 RID: 1125
		// (get) Token: 0x0600152E RID: 5422 RVA: 0x0004CF95 File Offset: 0x0004B195
		public string OldName
		{
			get
			{
				return this.oldName;
			}
		}

		// Token: 0x040013DC RID: 5084
		private string oldName;

		// Token: 0x040013DD RID: 5085
		private string oldFullPath;
	}
}
