﻿using System;

namespace System.IO
{
	/// <summary>Contains information on the change that occurred.</summary>
	// Token: 0x020002C3 RID: 707
	public struct WaitForChangedResult
	{
		/// <summary>Gets or sets the type of change that occurred.</summary>
		/// <returns>One of the <see cref="T:System.IO.WatcherChangeTypes" /> values.</returns>
		// Token: 0x17000466 RID: 1126
		// (get) Token: 0x06001533 RID: 5427 RVA: 0x0004CF9D File Offset: 0x0004B19D
		// (set) Token: 0x06001534 RID: 5428 RVA: 0x0004CFA5 File Offset: 0x0004B1A5
		public WatcherChangeTypes ChangeType
		{
			get
			{
				return this.changeType;
			}
			set
			{
				this.changeType = value;
			}
		}

		/// <summary>Gets or sets the name of the file or directory that changed.</summary>
		/// <returns>The name of the file or directory that changed.</returns>
		// Token: 0x17000467 RID: 1127
		// (get) Token: 0x06001535 RID: 5429 RVA: 0x0004CFAE File Offset: 0x0004B1AE
		// (set) Token: 0x06001536 RID: 5430 RVA: 0x0004CFB6 File Offset: 0x0004B1B6
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets or sets the original name of the file or directory that was renamed.</summary>
		/// <returns>The original name of the file or directory that was renamed.</returns>
		// Token: 0x17000468 RID: 1128
		// (get) Token: 0x06001537 RID: 5431 RVA: 0x0004CFBF File Offset: 0x0004B1BF
		// (set) Token: 0x06001538 RID: 5432 RVA: 0x0004CFC7 File Offset: 0x0004B1C7
		public string OldName
		{
			get
			{
				return this.oldName;
			}
			set
			{
				this.oldName = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the wait operation timed out.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="M:System.IO.FileSystemWatcher.WaitForChanged(System.IO.WatcherChangeTypes)" /> method timed out; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000469 RID: 1129
		// (get) Token: 0x06001539 RID: 5433 RVA: 0x0004CFD0 File Offset: 0x0004B1D0
		// (set) Token: 0x0600153A RID: 5434 RVA: 0x0004CFD8 File Offset: 0x0004B1D8
		public bool TimedOut
		{
			get
			{
				return this.timedOut;
			}
			set
			{
				this.timedOut = value;
			}
		}

		// Token: 0x040013DE RID: 5086
		private WatcherChangeTypes changeType;

		// Token: 0x040013DF RID: 5087
		private string name;

		// Token: 0x040013E0 RID: 5088
		private string oldName;

		// Token: 0x040013E1 RID: 5089
		private bool timedOut;
	}
}
