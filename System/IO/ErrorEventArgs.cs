﻿using System;

namespace System.IO
{
	/// <summary>Provides data for the <see cref="E:System.IO.FileSystemWatcher.Error" /> event.</summary>
	// Token: 0x020002B8 RID: 696
	public class ErrorEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.ErrorEventArgs" /> class.</summary>
		/// <param name="exception">An <see cref="T:System.Exception" /> that represents the error that occurred. </param>
		// Token: 0x060014EC RID: 5356 RVA: 0x0004CC7E File Offset: 0x0004AE7E
		public ErrorEventArgs(Exception exception)
		{
			this.exception = exception;
		}

		/// <summary>Gets the <see cref="T:System.Exception" /> that represents the error that occurred.</summary>
		/// <returns>An <see cref="T:System.Exception" /> that represents the error that occurred.</returns>
		// Token: 0x060014ED RID: 5357 RVA: 0x0004CC8D File Offset: 0x0004AE8D
		public virtual Exception GetException()
		{
			return this.exception;
		}

		// Token: 0x040013C9 RID: 5065
		private Exception exception;
	}
}
