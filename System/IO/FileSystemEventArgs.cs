﻿using System;

namespace System.IO
{
	/// <summary>Provides data for the directory events: <see cref="E:System.IO.FileSystemWatcher.Changed" />, <see cref="E:System.IO.FileSystemWatcher.Created" />, <see cref="E:System.IO.FileSystemWatcher.Deleted" />.</summary>
	// Token: 0x020002BA RID: 698
	public class FileSystemEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.FileSystemEventArgs" /> class.</summary>
		/// <param name="changeType">One of the <see cref="T:System.IO.WatcherChangeTypes" /> values, which represents the kind of change detected in the file system. </param>
		/// <param name="directory">The root directory of the affected file or directory. </param>
		/// <param name="name">The name of the affected file or directory. </param>
		// Token: 0x060014F2 RID: 5362 RVA: 0x0004CC95 File Offset: 0x0004AE95
		public FileSystemEventArgs(WatcherChangeTypes changeType, string directory, string name)
		{
			this.changeType = changeType;
			this.directory = directory;
			this.name = name;
		}

		// Token: 0x060014F3 RID: 5363 RVA: 0x0004CCB2 File Offset: 0x0004AEB2
		internal void SetName(string name)
		{
			this.name = name;
		}

		/// <summary>Gets the type of directory event that occurred.</summary>
		/// <returns>One of the <see cref="T:System.IO.WatcherChangeTypes" /> values that represents the kind of change detected in the file system.</returns>
		// Token: 0x17000458 RID: 1112
		// (get) Token: 0x060014F4 RID: 5364 RVA: 0x0004CCBB File Offset: 0x0004AEBB
		public WatcherChangeTypes ChangeType
		{
			get
			{
				return this.changeType;
			}
		}

		/// <summary>Gets the fully qualifed path of the affected file or directory.</summary>
		/// <returns>The path of the affected file or directory.</returns>
		// Token: 0x17000459 RID: 1113
		// (get) Token: 0x060014F5 RID: 5365 RVA: 0x0004CCC3 File Offset: 0x0004AEC3
		public string FullPath
		{
			get
			{
				return Path.Combine(this.directory, this.name);
			}
		}

		/// <summary>Gets the name of the affected file or directory.</summary>
		/// <returns>The name of the affected file or directory.</returns>
		// Token: 0x1700045A RID: 1114
		// (get) Token: 0x060014F6 RID: 5366 RVA: 0x0004CCD6 File Offset: 0x0004AED6
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x040013CA RID: 5066
		private WatcherChangeTypes changeType;

		// Token: 0x040013CB RID: 5067
		private string directory;

		// Token: 0x040013CC RID: 5068
		private string name;
	}
}
