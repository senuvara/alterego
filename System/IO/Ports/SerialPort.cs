﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using Unity;

namespace System.IO.Ports
{
	/// <summary>Represents a serial port resource.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x02000662 RID: 1634
	[MonitoringDescription("SerialPortDesc")]
	public class SerialPort : Component
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Ports.SerialPort" /> class.</summary>
		// Token: 0x06003412 RID: 13330 RVA: 0x000092E2 File Offset: 0x000074E2
		public SerialPort()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Ports.SerialPort" /> class using the specified <see cref="T:System.ComponentModel.IContainer" /> object.</summary>
		/// <param name="container">An interface to a container. </param>
		/// <exception cref="T:System.IO.IOException">The specified port could not be found or opened.</exception>
		// Token: 0x06003413 RID: 13331 RVA: 0x000092E2 File Offset: 0x000074E2
		public SerialPort(IContainer container)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Ports.SerialPort" /> class using the specified port name.</summary>
		/// <param name="portName">The port to use (for example, COM1). </param>
		/// <exception cref="T:System.IO.IOException">The specified port could not be found or opened.</exception>
		// Token: 0x06003414 RID: 13332 RVA: 0x000092E2 File Offset: 0x000074E2
		public SerialPort(string portName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Ports.SerialPort" /> class using the specified port name and baud rate.</summary>
		/// <param name="portName">The port to use (for example, COM1). </param>
		/// <param name="baudRate">The baud rate. </param>
		/// <exception cref="T:System.IO.IOException">The specified port could not be found or opened.</exception>
		// Token: 0x06003415 RID: 13333 RVA: 0x000092E2 File Offset: 0x000074E2
		public SerialPort(string portName, int baudRate)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Ports.SerialPort" /> class using the specified port name, baud rate, and parity bit.</summary>
		/// <param name="portName">The port to use (for example, COM1). </param>
		/// <param name="baudRate">The baud rate. </param>
		/// <param name="parity">One of the <see cref="P:System.IO.Ports.SerialPort.Parity" /> values. </param>
		/// <exception cref="T:System.IO.IOException">The specified port could not be found or opened.</exception>
		// Token: 0x06003416 RID: 13334 RVA: 0x000092E2 File Offset: 0x000074E2
		public SerialPort(string portName, int baudRate, Parity parity)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Ports.SerialPort" /> class using the specified port name, baud rate, parity bit, and data bits.</summary>
		/// <param name="portName">The port to use (for example, COM1). </param>
		/// <param name="baudRate">The baud rate. </param>
		/// <param name="parity">One of the <see cref="P:System.IO.Ports.SerialPort.Parity" /> values. </param>
		/// <param name="dataBits">The data bits value. </param>
		/// <exception cref="T:System.IO.IOException">The specified port could not be found or opened.</exception>
		// Token: 0x06003417 RID: 13335 RVA: 0x000092E2 File Offset: 0x000074E2
		public SerialPort(string portName, int baudRate, Parity parity, int dataBits)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Ports.SerialPort" /> class using the specified port name, baud rate, parity bit, data bits, and stop bit.</summary>
		/// <param name="portName">The port to use (for example, COM1). </param>
		/// <param name="baudRate">The baud rate. </param>
		/// <param name="parity">One of the <see cref="P:System.IO.Ports.SerialPort.Parity" /> values. </param>
		/// <param name="dataBits">The data bits value. </param>
		/// <param name="stopBits">One of the <see cref="P:System.IO.Ports.SerialPort.StopBits" /> values. </param>
		/// <exception cref="T:System.IO.IOException">The specified port could not be found or opened.</exception>
		// Token: 0x06003418 RID: 13336 RVA: 0x000092E2 File Offset: 0x000074E2
		public SerialPort(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the underlying <see cref="T:System.IO.Stream" /> object for a <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
		/// <returns>A <see cref="T:System.IO.Stream" /> object.</returns>
		/// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open" /> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close" /> method has been called. </exception>
		/// <exception cref="T:System.NotSupportedException">The stream is in a .NET Compact Framework application and one of the following methods was called:
		///         <see cref="M:System.IO.Stream.BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)" />
		///
		///         <see cref="M:System.IO.Stream.BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)" />
		///
		///         <see cref="M:System.IO.Stream.EndRead(System.IAsyncResult)" />
		///
		///         <see cref="M:System.IO.Stream.EndWrite(System.IAsyncResult)" />
		///       The .NET Compact Framework does not support the asynchronous model with base streams.</exception>
		// Token: 0x17000D2A RID: 3370
		// (get) Token: 0x06003419 RID: 13337 RVA: 0x00043C3C File Offset: 0x00041E3C
		public Stream BaseStream
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the serial baud rate.</summary>
		/// <returns>The baud rate.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The baud rate specified is less than or equal to zero, or is greater than the maximum allowable baud rate for the device. </exception>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state. - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		// Token: 0x17000D2B RID: 3371
		// (get) Token: 0x0600341A RID: 13338 RVA: 0x000A5184 File Offset: 0x000A3384
		// (set) Token: 0x0600341B RID: 13339 RVA: 0x000092E2 File Offset: 0x000074E2
		public int BaudRate
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the break signal state.</summary>
		/// <returns>
		///     <see langword="true" /> if the port is in a break state; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or -An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open" /> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close" /> method has been called.</exception>
		// Token: 0x17000D2C RID: 3372
		// (get) Token: 0x0600341C RID: 13340 RVA: 0x000A51A0 File Offset: 0x000A33A0
		// (set) Token: 0x0600341D RID: 13341 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool BreakState
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the number of bytes of data in the receive buffer.</summary>
		/// <returns>The number of bytes of data in the receive buffer.</returns>
		/// <exception cref="T:System.InvalidOperationException">The port is not open.</exception>
		// Token: 0x17000D2D RID: 3373
		// (get) Token: 0x0600341E RID: 13342 RVA: 0x000A51BC File Offset: 0x000A33BC
		public int BytesToRead
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the number of bytes of data in the send buffer.</summary>
		/// <returns>The number of bytes of data in the send buffer.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open" /> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close" /> method has been called.</exception>
		// Token: 0x17000D2E RID: 3374
		// (get) Token: 0x0600341F RID: 13343 RVA: 0x000A51D8 File Offset: 0x000A33D8
		public int BytesToWrite
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the state of the Carrier Detect line for the port.</summary>
		/// <returns>
		///     <see langword="true" /> if the carrier is detected; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open" /> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close" /> method has been called.</exception>
		// Token: 0x17000D2F RID: 3375
		// (get) Token: 0x06003420 RID: 13344 RVA: 0x000A51F4 File Offset: 0x000A33F4
		public bool CDHolding
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the state of the Clear-to-Send line.</summary>
		/// <returns>
		///     <see langword="true" /> if the Clear-to-Send line is detected; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open" /> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close" /> method has been called.</exception>
		// Token: 0x17000D30 RID: 3376
		// (get) Token: 0x06003421 RID: 13345 RVA: 0x000A5210 File Offset: 0x000A3410
		public bool CtsHolding
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets the standard length of data bits per byte.</summary>
		/// <returns>The data bits length.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or -An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The data bits value is less than 5 or more than 8. </exception>
		// Token: 0x17000D31 RID: 3377
		// (get) Token: 0x06003422 RID: 13346 RVA: 0x000A522C File Offset: 0x000A342C
		// (set) Token: 0x06003423 RID: 13347 RVA: 0x000092E2 File Offset: 0x000074E2
		public int DataBits
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether null bytes are ignored when transmitted between the port and the receive buffer.</summary>
		/// <returns>
		///     <see langword="true" /> if null bytes are ignored; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open" /> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close" /> method has been called.</exception>
		// Token: 0x17000D32 RID: 3378
		// (get) Token: 0x06003424 RID: 13348 RVA: 0x000A5248 File Offset: 0x000A3448
		// (set) Token: 0x06003425 RID: 13349 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool DiscardNull
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the state of the Data Set Ready (DSR) signal.</summary>
		/// <returns>
		///     <see langword="true" /> if a Data Set Ready signal has been sent to the port; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open" /> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close" /> method has been called.</exception>
		// Token: 0x17000D33 RID: 3379
		// (get) Token: 0x06003426 RID: 13350 RVA: 0x000A5264 File Offset: 0x000A3464
		public bool DsrHolding
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets a value that enables the Data Terminal Ready (DTR) signal during serial communication.</summary>
		/// <returns>
		///     <see langword="true" /> to enable Data Terminal Ready (DTR); otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		// Token: 0x17000D34 RID: 3380
		// (get) Token: 0x06003427 RID: 13351 RVA: 0x000A5280 File Offset: 0x000A3480
		// (set) Token: 0x06003428 RID: 13352 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool DtrEnable
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the byte encoding for pre- and post-transmission conversion of text.</summary>
		/// <returns>An <see cref="T:System.Text.Encoding" /> object. The default is <see cref="T:System.Text.ASCIIEncoding" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="P:System.IO.Ports.SerialPort.Encoding" /> property was set to <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.IO.Ports.SerialPort.Encoding" /> property was set to an encoding that is not <see cref="T:System.Text.ASCIIEncoding" />, <see cref="T:System.Text.UTF8Encoding" />, <see cref="T:System.Text.UTF32Encoding" />, <see cref="T:System.Text.UnicodeEncoding" />, one of the Windows single byte encodings, or one of the Windows double byte encodings.</exception>
		// Token: 0x17000D35 RID: 3381
		// (get) Token: 0x06003429 RID: 13353 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600342A RID: 13354 RVA: 0x000092E2 File Offset: 0x000074E2
		public Encoding Encoding
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the handshaking protocol for serial port transmission of data using a value from <see cref="T:System.IO.Ports.Handshake" />.</summary>
		/// <returns>One of the <see cref="T:System.IO.Ports.Handshake" /> values. The default is <see langword="None" />.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value passed is not a valid value in the <see cref="T:System.IO.Ports.Handshake" /> enumeration.</exception>
		// Token: 0x17000D36 RID: 3382
		// (get) Token: 0x0600342B RID: 13355 RVA: 0x000A529C File Offset: 0x000A349C
		// (set) Token: 0x0600342C RID: 13356 RVA: 0x000092E2 File Offset: 0x000074E2
		public Handshake Handshake
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return Handshake.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value indicating the open or closed status of the <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
		/// <returns>
		///     <see langword="true" /> if the serial port is open; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="P:System.IO.Ports.SerialPort.IsOpen" /> value passed is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.IO.Ports.SerialPort.IsOpen" /> value passed is an empty string ("").</exception>
		// Token: 0x17000D37 RID: 3383
		// (get) Token: 0x0600342D RID: 13357 RVA: 0x000A52B8 File Offset: 0x000A34B8
		public bool IsOpen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets the value used to interpret the end of a call to the <see cref="M:System.IO.Ports.SerialPort.ReadLine" /> and <see cref="M:System.IO.Ports.SerialPort.WriteLine(System.String)" /> methods.</summary>
		/// <returns>A value that represents the end of a line. The default is a line feed, <see cref="P:System.Environment.NewLine" />.</returns>
		/// <exception cref="T:System.ArgumentException">The property value is empty.</exception>
		/// <exception cref="T:System.ArgumentNullException">The property value is <see langword="null" />.</exception>
		// Token: 0x17000D38 RID: 3384
		// (get) Token: 0x0600342E RID: 13358 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600342F RID: 13359 RVA: 0x000092E2 File Offset: 0x000074E2
		public string NewLine
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the parity-checking protocol.</summary>
		/// <returns>One of the enumeration values that represents the parity-checking protocol. The default is <see cref="F:System.IO.Ports.Parity.None" />.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.Parity" /> value passed is not a valid value in the <see cref="T:System.IO.Ports.Parity" /> enumeration.</exception>
		// Token: 0x17000D39 RID: 3385
		// (get) Token: 0x06003430 RID: 13360 RVA: 0x000A52D4 File Offset: 0x000A34D4
		// (set) Token: 0x06003431 RID: 13361 RVA: 0x000092E2 File Offset: 0x000074E2
		public Parity Parity
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return Parity.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the byte that replaces invalid bytes in a data stream when a parity error occurs.</summary>
		/// <returns>A byte that replaces invalid bytes.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		// Token: 0x17000D3A RID: 3386
		// (get) Token: 0x06003432 RID: 13362 RVA: 0x000A52F0 File Offset: 0x000A34F0
		// (set) Token: 0x06003433 RID: 13363 RVA: 0x000092E2 File Offset: 0x000074E2
		public byte ParityReplace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the port for communications, including but not limited to all available COM ports.</summary>
		/// <returns>The communications port. The default is COM1.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.IO.Ports.SerialPort.PortName" /> property was set to a value with a length of zero.-or-The <see cref="P:System.IO.Ports.SerialPort.PortName" /> property was set to a value that starts with "\\".-or-The port name was not valid.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="P:System.IO.Ports.SerialPort.PortName" /> property was set to <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The specified port is open. </exception>
		// Token: 0x17000D3B RID: 3387
		// (get) Token: 0x06003434 RID: 13364 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003435 RID: 13365 RVA: 0x000092E2 File Offset: 0x000074E2
		public string PortName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the size of the <see cref="T:System.IO.Ports.SerialPort" /> input buffer.</summary>
		/// <returns>The buffer size, in bytes. The default value is 4096; the maximum value is that of a positive int, or 2147483647.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.ReadBufferSize" /> value set is less than or equal to zero.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.IO.Ports.SerialPort.ReadBufferSize" /> property was set while the stream was open.</exception>
		/// <exception cref="T:System.IO.IOException">The <see cref="P:System.IO.Ports.SerialPort.ReadBufferSize" /> property was set to an odd integer value. </exception>
		// Token: 0x17000D3C RID: 3388
		// (get) Token: 0x06003436 RID: 13366 RVA: 0x000A530C File Offset: 0x000A350C
		// (set) Token: 0x06003437 RID: 13367 RVA: 0x000092E2 File Offset: 0x000074E2
		public int ReadBufferSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the number of milliseconds before a time-out occurs when a read operation does not finish.</summary>
		/// <returns>The number of milliseconds before a time-out occurs when a read operation does not finish.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The read time-out value is less than zero and not equal to <see cref="F:System.IO.Ports.SerialPort.InfiniteTimeout" />. </exception>
		// Token: 0x17000D3D RID: 3389
		// (get) Token: 0x06003438 RID: 13368 RVA: 0x000A5328 File Offset: 0x000A3528
		// (set) Token: 0x06003439 RID: 13369 RVA: 0x000092E2 File Offset: 0x000074E2
		public int ReadTimeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the number of bytes in the internal input buffer before a <see cref="E:System.IO.Ports.SerialPort.DataReceived" /> event occurs.</summary>
		/// <returns>The number of bytes in the internal input buffer before a <see cref="E:System.IO.Ports.SerialPort.DataReceived" /> event is fired. The default is 1.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.ReceivedBytesThreshold" /> value is less than or equal to zero. </exception>
		// Token: 0x17000D3E RID: 3390
		// (get) Token: 0x0600343A RID: 13370 RVA: 0x000A5344 File Offset: 0x000A3544
		// (set) Token: 0x0600343B RID: 13371 RVA: 0x000092E2 File Offset: 0x000074E2
		public int ReceivedBytesThreshold
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the Request to Send (RTS) signal is enabled during serial communication.</summary>
		/// <returns>
		///     <see langword="true" /> to enable Request to Transmit (RTS); otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The value of the <see cref="P:System.IO.Ports.SerialPort.RtsEnable" /> property was set or retrieved while the <see cref="P:System.IO.Ports.SerialPort.Handshake" /> property is set to the <see cref="F:System.IO.Ports.Handshake.RequestToSend" /> value or the <see cref="F:System.IO.Ports.Handshake.RequestToSendXOnXOff" /> value.</exception>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		// Token: 0x17000D3F RID: 3391
		// (get) Token: 0x0600343C RID: 13372 RVA: 0x000A5360 File Offset: 0x000A3560
		// (set) Token: 0x0600343D RID: 13373 RVA: 0x000092E2 File Offset: 0x000074E2
		public bool RtsEnable
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the standard number of stopbits per byte.</summary>
		/// <returns>One of the <see cref="T:System.IO.Ports.StopBits" /> values.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.StopBits" /> value is  <see cref="F:System.IO.Ports.StopBits.None" />.</exception>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		// Token: 0x17000D40 RID: 3392
		// (get) Token: 0x0600343E RID: 13374 RVA: 0x000A537C File Offset: 0x000A357C
		// (set) Token: 0x0600343F RID: 13375 RVA: 0x000092E2 File Offset: 0x000074E2
		public StopBits StopBits
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return StopBits.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the size of the serial port output buffer. </summary>
		/// <returns>The size of the output buffer. The default is 2048.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.WriteBufferSize" /> value is less than or equal to zero.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.IO.Ports.SerialPort.WriteBufferSize" /> property was set while the stream was open.</exception>
		/// <exception cref="T:System.IO.IOException">The <see cref="P:System.IO.Ports.SerialPort.WriteBufferSize" /> property was set to an odd integer value. </exception>
		// Token: 0x17000D41 RID: 3393
		// (get) Token: 0x06003440 RID: 13376 RVA: 0x000A5398 File Offset: 0x000A3598
		// (set) Token: 0x06003441 RID: 13377 RVA: 0x000092E2 File Offset: 0x000074E2
		public int WriteBufferSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the number of milliseconds before a time-out occurs when a write operation does not finish.</summary>
		/// <returns>The number of milliseconds before a time-out occurs. The default is <see cref="F:System.IO.Ports.SerialPort.InfiniteTimeout" />.</returns>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.WriteTimeout" /> value is less than zero and not equal to <see cref="F:System.IO.Ports.SerialPort.InfiniteTimeout" />. </exception>
		// Token: 0x17000D42 RID: 3394
		// (get) Token: 0x06003442 RID: 13378 RVA: 0x000A53B4 File Offset: 0x000A35B4
		// (set) Token: 0x06003443 RID: 13379 RVA: 0x000092E2 File Offset: 0x000074E2
		public int WriteTimeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Indicates that data has been received through a port represented by the <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
		// Token: 0x1400005E RID: 94
		// (add) Token: 0x06003444 RID: 13380 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x06003445 RID: 13381 RVA: 0x000092E2 File Offset: 0x000074E2
		public event SerialDataReceivedEventHandler DataReceived
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Indicates that an error has occurred with a port represented by a <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
		// Token: 0x1400005F RID: 95
		// (add) Token: 0x06003446 RID: 13382 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x06003447 RID: 13383 RVA: 0x000092E2 File Offset: 0x000074E2
		public event SerialErrorReceivedEventHandler ErrorReceived
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Indicates that a non-data signal event has occurred on the port represented by the <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
		// Token: 0x14000060 RID: 96
		// (add) Token: 0x06003448 RID: 13384 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x06003449 RID: 13385 RVA: 0x000092E2 File Offset: 0x000074E2
		public event SerialPinChangedEventHandler PinChanged
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Closes the port connection, sets the <see cref="P:System.IO.Ports.SerialPort.IsOpen" /> property to <see langword="false" />, and disposes of the internal <see cref="T:System.IO.Stream" /> object.</summary>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.- or -An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		// Token: 0x0600344A RID: 13386 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Close()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Discards data from the serial driver's receive buffer.</summary>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or -An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open" /> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close" /> method has been called.</exception>
		// Token: 0x0600344B RID: 13387 RVA: 0x000092E2 File Offset: 0x000074E2
		public void DiscardInBuffer()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Discards data from the serial driver's transmit buffer.</summary>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open" /> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close" /> method has been called.</exception>
		// Token: 0x0600344C RID: 13388 RVA: 0x000092E2 File Offset: 0x000074E2
		public void DiscardOutBuffer()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an array of serial port names for the current computer.</summary>
		/// <returns>An array of serial port names for the current computer.</returns>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The serial port names could not be queried.</exception>
		// Token: 0x0600344D RID: 13389 RVA: 0x00043C3C File Offset: 0x00041E3C
		public static string[] GetPortNames()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens a new serial port connection.</summary>
		/// <exception cref="T:System.UnauthorizedAccessException">Access is denied to the port.- or -The current process, or another process on the system, already has the specified COM port open either by a <see cref="T:System.IO.Ports.SerialPort" /> instance or in unmanaged code.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">One or more of the properties for this instance are invalid. For example, the <see cref="P:System.IO.Ports.SerialPort.Parity" />, <see cref="P:System.IO.Ports.SerialPort.DataBits" />, or <see cref="P:System.IO.Ports.SerialPort.Handshake" /> properties are not valid values; the <see cref="P:System.IO.Ports.SerialPort.BaudRate" /> is less than or equal to zero; the <see cref="P:System.IO.Ports.SerialPort.ReadTimeout" /> or <see cref="P:System.IO.Ports.SerialPort.WriteTimeout" /> property is less than zero and is not <see cref="F:System.IO.Ports.SerialPort.InfiniteTimeout" />. </exception>
		/// <exception cref="T:System.ArgumentException">The port name does not begin with "COM". - or -The file type of the port is not supported.</exception>
		/// <exception cref="T:System.IO.IOException">The port is in an invalid state.  - or - An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort" /> object were invalid.</exception>
		/// <exception cref="T:System.InvalidOperationException">The specified port on the current instance of the <see cref="T:System.IO.Ports.SerialPort" /> is already open.</exception>
		// Token: 0x0600344E RID: 13390 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Open()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Reads a number of bytes from the <see cref="T:System.IO.Ports.SerialPort" /> input buffer and writes those bytes into a byte array at the specified offset.</summary>
		/// <param name="buffer">The byte array to write the input to. </param>
		/// <param name="offset">The offset in <paramref name="buffer" /> at which to write the bytes. </param>
		/// <param name="count">The maximum number of bytes to read. Fewer bytes are read if <paramref name="count" /> is greater than the number of bytes in the input buffer.  </param>
		/// <returns>The number of bytes read.</returns>
		/// <exception cref="T:System.ArgumentNullException">The buffer passed is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="offset" /> or <paramref name="count" /> parameters are outside a valid region of the <paramref name="buffer" /> being passed. Either <paramref name="offset" /> or <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="offset" /> plus <paramref name="count" /> is greater than the length of the <paramref name="buffer" />. </exception>
		/// <exception cref="T:System.TimeoutException">No bytes were available to read.</exception>
		// Token: 0x0600344F RID: 13391 RVA: 0x000A53D0 File Offset: 0x000A35D0
		public int Read(byte[] buffer, int offset, int count)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Reads a number of characters from the <see cref="T:System.IO.Ports.SerialPort" /> input buffer and writes them into an array of characters at a given offset.</summary>
		/// <param name="buffer">The character array to write the input to. </param>
		/// <param name="offset">The offset in <paramref name="buffer" /> at which to write the characters.</param>
		/// <param name="count">The maximum number of characters to read. Fewer characters are read if <paramref name="count" /> is greater than the number of characters in the input buffer.   </param>
		/// <returns>The number of characters read.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="offset" /> plus <paramref name="count" /> is greater than the length of the buffer.- or -
		///         <paramref name="count" /> is 1 and there is a surrogate character in the buffer.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="buffer" /> passed is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="offset" /> or <paramref name="count" /> parameters are outside a valid region of the <paramref name="buffer" /> being passed. Either <paramref name="offset" /> or <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		/// <exception cref="T:System.TimeoutException">No characters were available to read.</exception>
		// Token: 0x06003450 RID: 13392 RVA: 0x000A53EC File Offset: 0x000A35EC
		public int Read(char[] buffer, int offset, int count)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Synchronously reads one byte from the <see cref="T:System.IO.Ports.SerialPort" /> input buffer.</summary>
		/// <returns>The byte, cast to an <see cref="T:System.Int32" />, or -1 if the end of the stream has been read.</returns>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		/// <exception cref="T:System.ServiceProcess.TimeoutException">The operation did not complete before the time-out period ended.- or -No byte was read.</exception>
		// Token: 0x06003451 RID: 13393 RVA: 0x000A5408 File Offset: 0x000A3608
		public int ReadByte()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Synchronously reads one character from the <see cref="T:System.IO.Ports.SerialPort" /> input buffer.</summary>
		/// <returns>The character that was read.</returns>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		/// <exception cref="T:System.ServiceProcess.TimeoutException">The operation did not complete before the time-out period ended.- or -No character was available in the allotted time-out period.</exception>
		// Token: 0x06003452 RID: 13394 RVA: 0x000A5424 File Offset: 0x000A3624
		public int ReadChar()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Reads all immediately available bytes, based on the encoding, in both the stream and the input buffer of the <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
		/// <returns>The contents of the stream and the input buffer of the <see cref="T:System.IO.Ports.SerialPort" /> object.</returns>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		// Token: 0x06003453 RID: 13395 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string ReadExisting()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Reads up to the <see cref="P:System.IO.Ports.SerialPort.NewLine" /> value in the input buffer.</summary>
		/// <returns>The contents of the input buffer up to the first occurrence of a <see cref="P:System.IO.Ports.SerialPort.NewLine" /> value.</returns>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		/// <exception cref="T:System.TimeoutException">The operation did not complete before the time-out period ended.- or -No bytes were read.</exception>
		// Token: 0x06003454 RID: 13396 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string ReadLine()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Reads a string up to the specified <paramref name="value" /> in the input buffer.</summary>
		/// <param name="value">A value that indicates where the read operation stops. </param>
		/// <returns>The contents of the input buffer up to the specified <paramref name="value" />.</returns>
		/// <exception cref="T:System.ArgumentException">The length of the <paramref name="value" /> parameter is 0.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="value" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		/// <exception cref="T:System.TimeoutException">The operation did not complete before the time-out period ended. </exception>
		// Token: 0x06003455 RID: 13397 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string ReadTo(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Writes a specified number of bytes to the serial port using data from a buffer.</summary>
		/// <param name="buffer">The byte array that contains the data to write to the port. </param>
		/// <param name="offset">The zero-based byte offset in the <paramref name="buffer" /> parameter at which to begin copying bytes to the port. </param>
		/// <param name="count">The number of bytes to write. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="buffer" /> passed is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="offset" /> or <paramref name="count" /> parameters are outside a valid region of the <paramref name="buffer" /> being passed. Either <paramref name="offset" /> or <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="offset" /> plus <paramref name="count" /> is greater than the length of the <paramref name="buffer" />. </exception>
		/// <exception cref="T:System.ServiceProcess.TimeoutException">The operation did not complete before the time-out period ended. </exception>
		// Token: 0x06003456 RID: 13398 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Write(byte[] buffer, int offset, int count)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes a specified number of characters to the serial port using data from a buffer.</summary>
		/// <param name="buffer">The character array that contains the data to write to the port. </param>
		/// <param name="offset">The zero-based byte offset in the <paramref name="buffer" /> parameter at which to begin copying bytes to the port. </param>
		/// <param name="count">The number of characters to write. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="buffer" /> passed is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="offset" /> or <paramref name="count" /> parameters are outside a valid region of the <paramref name="buffer" /> being passed. Either <paramref name="offset" /> or <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="offset" /> plus <paramref name="count" /> is greater than the length of the <paramref name="buffer" />. </exception>
		/// <exception cref="T:System.ServiceProcess.TimeoutException">The operation did not complete before the time-out period ended. </exception>
		// Token: 0x06003457 RID: 13399 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Write(char[] buffer, int offset, int count)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the specified string to the serial port.</summary>
		/// <param name="text">The string for output. </param>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="str" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ServiceProcess.TimeoutException">The operation did not complete before the time-out period ended. </exception>
		// Token: 0x06003458 RID: 13400 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Write(string text)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the specified string and the <see cref="P:System.IO.Ports.SerialPort.NewLine" /> value to the output buffer.</summary>
		/// <param name="text">The string to write to the output buffer. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="str" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
		/// <exception cref="T:System.TimeoutException">The <see cref="M:System.IO.Ports.SerialPort.WriteLine(System.String)" /> method could not write to the stream.  </exception>
		// Token: 0x06003459 RID: 13401 RVA: 0x000092E2 File Offset: 0x000074E2
		public void WriteLine(string text)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Indicates that no time-out should occur.</summary>
		// Token: 0x04002508 RID: 9480
		public const int InfiniteTimeout = -1;
	}
}
