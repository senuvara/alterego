﻿using System;
using Unity;

namespace System.IO.Ports
{
	/// <summary>Provides data for the <see cref="E:System.IO.Ports.SerialPort.DataReceived" /> event.</summary>
	// Token: 0x0200065A RID: 1626
	public class SerialDataReceivedEventArgs : EventArgs
	{
		// Token: 0x06003400 RID: 13312 RVA: 0x000092E2 File Offset: 0x000074E2
		internal SerialDataReceivedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the event type.</summary>
		/// <returns>One of the <see cref="T:System.IO.Ports.SerialData" /> values.</returns>
		// Token: 0x17000D27 RID: 3367
		// (get) Token: 0x06003401 RID: 13313 RVA: 0x000A5130 File Offset: 0x000A3330
		public SerialData EventType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (SerialData)0;
			}
		}
	}
}
