﻿using System;
using Unity;

namespace System.IO.Ports
{
	/// <summary>Represents the method that will handle the <see cref="E:System.IO.Ports.SerialPort.ErrorReceived" /> event of a <see cref="T:System.IO.Ports.SerialPort" /> object. </summary>
	/// <param name="sender">The sender of the event, which is the <see cref="T:System.IO.Ports.SerialPort" /> object. </param>
	/// <param name="e">A <see cref="T:System.IO.Ports.SerialErrorReceivedEventArgs" /> object that contains the event data. </param>
	// Token: 0x0200065E RID: 1630
	public sealed class SerialErrorReceivedEventHandler : MulticastDelegate
	{
		// Token: 0x06003408 RID: 13320 RVA: 0x000092E2 File Offset: 0x000074E2
		public SerialErrorReceivedEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003409 RID: 13321 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, SerialErrorReceivedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600340A RID: 13322 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, SerialErrorReceivedEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600340B RID: 13323 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
