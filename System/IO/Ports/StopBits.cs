﻿using System;

namespace System.IO.Ports
{
	/// <summary>Specifies the number of stop bits used on the <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
	// Token: 0x02000663 RID: 1635
	public enum StopBits
	{
		/// <summary>No stop bits are used. This value is not supported by the <see cref="P:System.IO.Ports.SerialPort.StopBits" /> property. </summary>
		// Token: 0x0400250A RID: 9482
		None,
		/// <summary>One stop bit is used.</summary>
		// Token: 0x0400250B RID: 9483
		One,
		/// <summary>1.5 stop bits are used.</summary>
		// Token: 0x0400250C RID: 9484
		OnePointFive = 3,
		/// <summary>Two stop bits are used.</summary>
		// Token: 0x0400250D RID: 9485
		Two = 2
	}
}
