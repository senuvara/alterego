﻿using System;
using Unity;

namespace System.IO.Ports
{
	/// <summary>Provides data for the <see cref="E:System.IO.Ports.SerialPort.PinChanged" /> event.</summary>
	// Token: 0x02000660 RID: 1632
	public class SerialPinChangedEventArgs : EventArgs
	{
		// Token: 0x0600340C RID: 13324 RVA: 0x000092E2 File Offset: 0x000074E2
		internal SerialPinChangedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the event type.</summary>
		/// <returns>One of the <see cref="T:System.IO.Ports.SerialPinChange" /> values.</returns>
		// Token: 0x17000D29 RID: 3369
		// (get) Token: 0x0600340D RID: 13325 RVA: 0x000A5168 File Offset: 0x000A3368
		public SerialPinChange EventType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (SerialPinChange)0;
			}
		}
	}
}
