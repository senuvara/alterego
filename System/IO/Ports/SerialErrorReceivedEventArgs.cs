﻿using System;
using Unity;

namespace System.IO.Ports
{
	/// <summary>Prepares data for the <see cref="E:System.IO.Ports.SerialPort.ErrorReceived" /> event.</summary>
	// Token: 0x0200065D RID: 1629
	public class SerialErrorReceivedEventArgs : EventArgs
	{
		// Token: 0x06003406 RID: 13318 RVA: 0x000092E2 File Offset: 0x000074E2
		internal SerialErrorReceivedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the event type.</summary>
		/// <returns>One of the <see cref="T:System.IO.Ports.SerialError" /> values.</returns>
		// Token: 0x17000D28 RID: 3368
		// (get) Token: 0x06003407 RID: 13319 RVA: 0x000A514C File Offset: 0x000A334C
		public SerialError EventType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (SerialError)0;
			}
		}
	}
}
