﻿using System;
using Unity;

namespace System.IO.Ports
{
	/// <summary>Represents the method that will handle the <see cref="E:System.IO.Ports.SerialPort.PinChanged" /> event of a <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
	/// <param name="sender">The source of the event, which is the <see cref="T:System.IO.Ports.SerialPort" /> object. </param>
	/// <param name="e">A <see cref="T:System.IO.Ports.SerialPinChangedEventArgs" /> object that contains the event data. </param>
	// Token: 0x02000661 RID: 1633
	public sealed class SerialPinChangedEventHandler : MulticastDelegate
	{
		// Token: 0x0600340E RID: 13326 RVA: 0x000092E2 File Offset: 0x000074E2
		public SerialPinChangedEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600340F RID: 13327 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, SerialPinChangedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003410 RID: 13328 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, SerialPinChangedEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06003411 RID: 13329 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
