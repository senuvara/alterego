﻿using System;

namespace System.IO.Ports
{
	/// <summary>Specifies errors that occur on the <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
	// Token: 0x0200065C RID: 1628
	public enum SerialError
	{
		/// <summary>The hardware detected a framing error.</summary>
		// Token: 0x040024FD RID: 9469
		Frame = 8,
		/// <summary>A character-buffer overrun has occurred. The next character is lost.</summary>
		// Token: 0x040024FE RID: 9470
		Overrun = 2,
		/// <summary>An input buffer overflow has occurred. There is either no room in the input buffer, or a character was received after the end-of-file (EOF) character.</summary>
		// Token: 0x040024FF RID: 9471
		RXOver = 1,
		/// <summary>The hardware detected a parity error.</summary>
		// Token: 0x04002500 RID: 9472
		RXParity = 4,
		/// <summary>The application tried to transmit a character, but the output buffer was full.</summary>
		// Token: 0x04002501 RID: 9473
		TXFull = 256
	}
}
