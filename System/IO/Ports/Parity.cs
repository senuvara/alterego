﻿using System;

namespace System.IO.Ports
{
	/// <summary>Specifies the parity bit for a <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
	// Token: 0x02000658 RID: 1624
	public enum Parity
	{
		/// <summary>Sets the parity bit so that the count of bits set is an even number.</summary>
		// Token: 0x040024F4 RID: 9460
		Even = 2,
		/// <summary>Leaves the parity bit set to 1.</summary>
		// Token: 0x040024F5 RID: 9461
		Mark,
		/// <summary>No parity check occurs.</summary>
		// Token: 0x040024F6 RID: 9462
		None = 0,
		/// <summary>Sets the parity bit so that the count of bits set is an odd number.</summary>
		// Token: 0x040024F7 RID: 9463
		Odd,
		/// <summary>Leaves the parity bit set to 0.</summary>
		// Token: 0x040024F8 RID: 9464
		Space = 4
	}
}
