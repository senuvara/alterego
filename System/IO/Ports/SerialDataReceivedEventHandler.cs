﻿using System;
using Unity;

namespace System.IO.Ports
{
	/// <summary>Represents the method that will handle the <see cref="E:System.IO.Ports.SerialPort.DataReceived" /> event of a <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
	/// <param name="sender">The sender of the event, which is the <see cref="T:System.IO.Ports.SerialPort" /> object. </param>
	/// <param name="e">A <see cref="T:System.IO.Ports.SerialDataReceivedEventArgs" /> object that contains the event data. </param>
	// Token: 0x0200065B RID: 1627
	public sealed class SerialDataReceivedEventHandler : MulticastDelegate
	{
		// Token: 0x06003402 RID: 13314 RVA: 0x000092E2 File Offset: 0x000074E2
		public SerialDataReceivedEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003403 RID: 13315 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, SerialDataReceivedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003404 RID: 13316 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, SerialDataReceivedEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06003405 RID: 13317 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
