﻿using System;

namespace System.IO.Ports
{
	/// <summary>Specifies the type of change that occurred on the <see cref="T:System.IO.Ports.SerialPort" /> object.</summary>
	// Token: 0x0200065F RID: 1631
	public enum SerialPinChange
	{
		/// <summary>A break was detected on input.</summary>
		// Token: 0x04002503 RID: 9475
		Break = 64,
		/// <summary>The Carrier Detect (CD) signal changed state. This signal is used to indicate whether a modem is connected to a working phone line and a data carrier signal is detected.</summary>
		// Token: 0x04002504 RID: 9476
		CDChanged = 32,
		/// <summary>The Clear to Send (CTS) signal changed state. This signal is used to indicate whether data can be sent over the serial port.</summary>
		// Token: 0x04002505 RID: 9477
		CtsChanged = 8,
		/// <summary>The Data Set Ready (DSR) signal changed state. This signal is used to indicate whether the device on the serial port is ready to operate.</summary>
		// Token: 0x04002506 RID: 9478
		DsrChanged = 16,
		/// <summary>A ring indicator was detected.</summary>
		// Token: 0x04002507 RID: 9479
		Ring = 256
	}
}
