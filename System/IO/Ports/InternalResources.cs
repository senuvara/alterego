﻿using System;

namespace System.IO.Ports
{
	// Token: 0x020002C5 RID: 709
	internal static class InternalResources
	{
		// Token: 0x0600153B RID: 5435 RVA: 0x0004CFE1 File Offset: 0x0004B1E1
		internal static void EndOfFile()
		{
			throw new EndOfStreamException(SR.GetString("Unable to read beyond the end of the stream."));
		}

		// Token: 0x0600153C RID: 5436 RVA: 0x0004CFF2 File Offset: 0x0004B1F2
		internal static string GetMessage(int errorCode)
		{
			return SR.GetString("Unknown Error '{0}'.", new object[]
			{
				errorCode
			});
		}

		// Token: 0x0600153D RID: 5437 RVA: 0x0004D00D File Offset: 0x0004B20D
		internal static void FileNotOpen()
		{
			throw new ObjectDisposedException(null, SR.GetString("The port is closed."));
		}

		// Token: 0x0600153E RID: 5438 RVA: 0x0004D01F File Offset: 0x0004B21F
		internal static void WrongAsyncResult()
		{
			throw new ArgumentException(SR.GetString("IAsyncResult object did not come from the corresponding async method on this type."));
		}

		// Token: 0x0600153F RID: 5439 RVA: 0x0004D030 File Offset: 0x0004B230
		internal static void EndReadCalledTwice()
		{
			throw new ArgumentException(SR.GetString("EndRead can only be called once for each asynchronous operation."));
		}

		// Token: 0x06001540 RID: 5440 RVA: 0x0004D041 File Offset: 0x0004B241
		internal static void EndWriteCalledTwice()
		{
			throw new ArgumentException(SR.GetString("EndWrite can only be called once for each asynchronous operation."));
		}

		// Token: 0x06001541 RID: 5441 RVA: 0x0004D054 File Offset: 0x0004B254
		internal static void WinIOError(int errorCode, string str)
		{
			if (errorCode <= 5)
			{
				if (errorCode - 2 > 1)
				{
					if (errorCode == 5)
					{
						if (str.Length == 0)
						{
							throw new UnauthorizedAccessException(SR.GetString("Access to the port is denied."));
						}
						throw new UnauthorizedAccessException(SR.GetString("Access to the port '{0}' is denied.", new object[]
						{
							str
						}));
					}
				}
				else
				{
					if (str.Length == 0)
					{
						throw new IOException(SR.GetString("The specified port does not exist."));
					}
					throw new IOException(SR.GetString("The port '{0}' does not exist.", new object[]
					{
						str
					}));
				}
			}
			else if (errorCode != 32)
			{
				if (errorCode == 206)
				{
					throw new PathTooLongException(SR.GetString("The specified port name is too long.  The port name must be less than 260 characters."));
				}
			}
			else
			{
				if (str.Length == 0)
				{
					throw new IOException(SR.GetString("The process cannot access the port because it is being used by another process."));
				}
				throw new IOException(SR.GetString("The process cannot access the port '{0}' because it is being used by another process.", new object[]
				{
					str
				}));
			}
			throw new IOException(InternalResources.GetMessage(errorCode), InternalResources.MakeHRFromErrorCode(errorCode));
		}

		// Token: 0x06001542 RID: 5442 RVA: 0x0004D140 File Offset: 0x0004B340
		internal static int MakeHRFromErrorCode(int errorCode)
		{
			return -2147024896 | errorCode;
		}
	}
}
