﻿using System;
using System.Runtime.Serialization;

namespace System.IO
{
	/// <summary>The exception that is thrown when a data stream is in an invalid format.</summary>
	// Token: 0x020002BF RID: 703
	[Serializable]
	public sealed class InvalidDataException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.InvalidDataException" /> class.</summary>
		// Token: 0x06001528 RID: 5416 RVA: 0x0004CF26 File Offset: 0x0004B126
		public InvalidDataException() : base(Locale.GetText("Invalid data format."))
		{
			base.HResult = -2146233085;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.InvalidDataException" /> class with a specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		// Token: 0x06001529 RID: 5417 RVA: 0x0004CF43 File Offset: 0x0004B143
		public InvalidDataException(string message) : base(message)
		{
			base.HResult = -2146233085;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.InvalidDataException" /> class with a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception.</param>
		// Token: 0x0600152A RID: 5418 RVA: 0x0004CF57 File Offset: 0x0004B157
		public InvalidDataException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233085;
		}

		// Token: 0x0600152B RID: 5419 RVA: 0x000447CD File Offset: 0x000429CD
		private InvalidDataException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040013D2 RID: 5074
		private const int Result = -2146233085;
	}
}
