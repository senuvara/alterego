﻿using System;
using System.ComponentModel;

namespace System.IO
{
	/// <summary>Sets the description visual designers can display when referencing an event, extender, or property.</summary>
	// Token: 0x020002BD RID: 701
	[AttributeUsage(AttributeTargets.All)]
	public class IODescriptionAttribute : DescriptionAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.IODescriptionAttribute" /> class.</summary>
		/// <param name="description">The description to use. </param>
		// Token: 0x06001522 RID: 5410 RVA: 0x000157FB File Offset: 0x000139FB
		public IODescriptionAttribute(string description) : base(description)
		{
		}

		/// <summary>Gets the description.</summary>
		/// <returns>The description for the event, extender, or property.</returns>
		// Token: 0x17000463 RID: 1123
		// (get) Token: 0x06001523 RID: 5411 RVA: 0x0002EC8F File Offset: 0x0002CE8F
		public override string Description
		{
			get
			{
				return base.DescriptionValue;
			}
		}
	}
}
