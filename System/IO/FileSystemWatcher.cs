﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.IO
{
	/// <summary>Listens to the file system change notifications and raises events when a directory, or file in a directory, changes.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x020002BC RID: 700
	public class FileSystemWatcher : Component, IDisposable, ISupportInitialize
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.FileSystemWatcher" /> class.</summary>
		// Token: 0x060014FB RID: 5371 RVA: 0x0004CCDE File Offset: 0x0004AEDE
		public FileSystemWatcher()
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.FileSystemWatcher" /> class, given the specified directory to monitor.</summary>
		/// <param name="path">The directory to monitor, in standard or Universal Naming Convention (UNC) notation. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="path" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="path" /> parameter is an empty string ("").-or- The path specified through the <paramref name="path" /> parameter does not exist. </exception>
		/// <exception cref="T:System.IO.PathTooLongException">
		///         <paramref name="path" /> is too long.</exception>
		// Token: 0x060014FC RID: 5372 RVA: 0x0004CCDE File Offset: 0x0004AEDE
		public FileSystemWatcher(string path)
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.FileSystemWatcher" /> class, given the specified directory and type of files to monitor.</summary>
		/// <param name="path">The directory to monitor, in standard or Universal Naming Convention (UNC) notation. </param>
		/// <param name="filter">The type of files to watch. For example, "*.txt" watches for changes to all text files. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="path" /> parameter is <see langword="null" />.-or- The <paramref name="filter" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="path" /> parameter is an empty string ("").-or- The path specified through the <paramref name="path" /> parameter does not exist. </exception>
		/// <exception cref="T:System.IO.PathTooLongException">
		///         <paramref name="path" /> is too long.</exception>
		// Token: 0x060014FD RID: 5373 RVA: 0x0004CCDE File Offset: 0x0004AEDE
		public FileSystemWatcher(string path, string filter)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets or sets a value indicating whether the component is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if the component is enabled; otherwise, <see langword="false" />. The default is <see langword="false" />. If you are using the component on a designer in Visual Studio 2005, the default is <see langword="true" />.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.IO.FileSystemWatcher" /> object has been disposed.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current operating system is not Microsoft Windows NT or later.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The directory specified in <see cref="P:System.IO.FileSystemWatcher.Path" /> could not be found.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <see cref="P:System.IO.FileSystemWatcher.Path" /> has not been set or is invalid.</exception>
		// Token: 0x1700045B RID: 1115
		// (get) Token: 0x060014FE RID: 5374 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x060014FF RID: 5375 RVA: 0x000068D7 File Offset: 0x00004AD7
		public bool EnableRaisingEvents
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the filter string used to determine what files are monitored in a directory.</summary>
		/// <returns>The filter string. The default is "*.*" (Watches all files.) </returns>
		// Token: 0x1700045C RID: 1116
		// (get) Token: 0x06001500 RID: 5376 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001501 RID: 5377 RVA: 0x0000232D File Offset: 0x0000052D
		public string Filter
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
			}
		}

		/// <summary>Gets or sets a value indicating whether subdirectories within the specified path should be monitored.</summary>
		/// <returns>
		///     <see langword="true" /> if you want to monitor subdirectories; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700045D RID: 1117
		// (get) Token: 0x06001502 RID: 5378 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001503 RID: 5379 RVA: 0x0000232D File Offset: 0x0000052D
		public bool IncludeSubdirectories
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
			}
		}

		/// <summary>Gets or sets the size (in bytes) of the internal buffer.</summary>
		/// <returns>The internal buffer size in bytes. The default is 8192 (8 KB).</returns>
		// Token: 0x1700045E RID: 1118
		// (get) Token: 0x06001504 RID: 5380 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001505 RID: 5381 RVA: 0x0000232D File Offset: 0x0000052D
		public int InternalBufferSize
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
			}
		}

		/// <summary>Gets or sets the type of changes to watch for.</summary>
		/// <returns>One of the <see cref="T:System.IO.NotifyFilters" /> values. The default is the bitwise OR combination of <see langword="LastWrite" />, <see langword="FileName" />, and <see langword="DirectoryName" />.</returns>
		/// <exception cref="T:System.ArgumentException">The value is not a valid bitwise OR combination of the <see cref="T:System.IO.NotifyFilters" /> values. </exception>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">The value that is being set is not valid.</exception>
		// Token: 0x1700045F RID: 1119
		// (get) Token: 0x06001506 RID: 5382 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001507 RID: 5383 RVA: 0x0000232D File Offset: 0x0000052D
		public NotifyFilters NotifyFilter
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
			}
		}

		/// <summary>Gets or sets the path of the directory to watch.</summary>
		/// <returns>The path to monitor. The default is an empty string ("").</returns>
		/// <exception cref="T:System.ArgumentException">The specified path does not exist or could not be found.-or- The specified path contains wildcard characters.-or- The specified path contains invalid path characters.</exception>
		// Token: 0x17000460 RID: 1120
		// (get) Token: 0x06001508 RID: 5384 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x06001509 RID: 5385 RVA: 0x0000232D File Offset: 0x0000052D
		public string Path
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
			}
		}

		/// <summary>Gets or sets an <see cref="T:System.ComponentModel.ISite" /> for the <see cref="T:System.IO.FileSystemWatcher" />.</summary>
		/// <returns>An <see cref="T:System.ComponentModel.ISite" /> for the <see cref="T:System.IO.FileSystemWatcher" />.</returns>
		// Token: 0x17000461 RID: 1121
		// (get) Token: 0x0600150A RID: 5386 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x0600150B RID: 5387 RVA: 0x0000232D File Offset: 0x0000052D
		public override ISite Site
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
			}
		}

		/// <summary>Gets or sets the object used to marshal the event handler calls issued as a result of a directory change.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.ISynchronizeInvoke" /> that represents the object used to marshal the event handler calls issued as a result of a directory change. The default is <see langword="null" />.</returns>
		// Token: 0x17000462 RID: 1122
		// (get) Token: 0x0600150C RID: 5388 RVA: 0x000068D7 File Offset: 0x00004AD7
		// (set) Token: 0x0600150D RID: 5389 RVA: 0x0000232D File Offset: 0x0000052D
		public ISynchronizeInvoke SynchronizingObject
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
			}
		}

		/// <summary>Occurs when a file or directory in the specified <see cref="P:System.IO.FileSystemWatcher.Path" /> is changed.</summary>
		// Token: 0x1400002B RID: 43
		// (add) Token: 0x0600150E RID: 5390 RVA: 0x0004CCEC File Offset: 0x0004AEEC
		// (remove) Token: 0x0600150F RID: 5391 RVA: 0x0004CD24 File Offset: 0x0004AF24
		public event FileSystemEventHandler Changed
		{
			[CompilerGenerated]
			add
			{
				FileSystemEventHandler fileSystemEventHandler = this.Changed;
				FileSystemEventHandler fileSystemEventHandler2;
				do
				{
					fileSystemEventHandler2 = fileSystemEventHandler;
					FileSystemEventHandler value2 = (FileSystemEventHandler)Delegate.Combine(fileSystemEventHandler2, value);
					fileSystemEventHandler = Interlocked.CompareExchange<FileSystemEventHandler>(ref this.Changed, value2, fileSystemEventHandler2);
				}
				while (fileSystemEventHandler != fileSystemEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				FileSystemEventHandler fileSystemEventHandler = this.Changed;
				FileSystemEventHandler fileSystemEventHandler2;
				do
				{
					fileSystemEventHandler2 = fileSystemEventHandler;
					FileSystemEventHandler value2 = (FileSystemEventHandler)Delegate.Remove(fileSystemEventHandler2, value);
					fileSystemEventHandler = Interlocked.CompareExchange<FileSystemEventHandler>(ref this.Changed, value2, fileSystemEventHandler2);
				}
				while (fileSystemEventHandler != fileSystemEventHandler2);
			}
		}

		/// <summary>Occurs when a file or directory in the specified <see cref="P:System.IO.FileSystemWatcher.Path" /> is created.</summary>
		// Token: 0x1400002C RID: 44
		// (add) Token: 0x06001510 RID: 5392 RVA: 0x0004CD5C File Offset: 0x0004AF5C
		// (remove) Token: 0x06001511 RID: 5393 RVA: 0x0004CD94 File Offset: 0x0004AF94
		public event FileSystemEventHandler Created
		{
			[CompilerGenerated]
			add
			{
				FileSystemEventHandler fileSystemEventHandler = this.Created;
				FileSystemEventHandler fileSystemEventHandler2;
				do
				{
					fileSystemEventHandler2 = fileSystemEventHandler;
					FileSystemEventHandler value2 = (FileSystemEventHandler)Delegate.Combine(fileSystemEventHandler2, value);
					fileSystemEventHandler = Interlocked.CompareExchange<FileSystemEventHandler>(ref this.Created, value2, fileSystemEventHandler2);
				}
				while (fileSystemEventHandler != fileSystemEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				FileSystemEventHandler fileSystemEventHandler = this.Created;
				FileSystemEventHandler fileSystemEventHandler2;
				do
				{
					fileSystemEventHandler2 = fileSystemEventHandler;
					FileSystemEventHandler value2 = (FileSystemEventHandler)Delegate.Remove(fileSystemEventHandler2, value);
					fileSystemEventHandler = Interlocked.CompareExchange<FileSystemEventHandler>(ref this.Created, value2, fileSystemEventHandler2);
				}
				while (fileSystemEventHandler != fileSystemEventHandler2);
			}
		}

		/// <summary>Occurs when a file or directory in the specified <see cref="P:System.IO.FileSystemWatcher.Path" /> is deleted.</summary>
		// Token: 0x1400002D RID: 45
		// (add) Token: 0x06001512 RID: 5394 RVA: 0x0004CDCC File Offset: 0x0004AFCC
		// (remove) Token: 0x06001513 RID: 5395 RVA: 0x0004CE04 File Offset: 0x0004B004
		public event FileSystemEventHandler Deleted
		{
			[CompilerGenerated]
			add
			{
				FileSystemEventHandler fileSystemEventHandler = this.Deleted;
				FileSystemEventHandler fileSystemEventHandler2;
				do
				{
					fileSystemEventHandler2 = fileSystemEventHandler;
					FileSystemEventHandler value2 = (FileSystemEventHandler)Delegate.Combine(fileSystemEventHandler2, value);
					fileSystemEventHandler = Interlocked.CompareExchange<FileSystemEventHandler>(ref this.Deleted, value2, fileSystemEventHandler2);
				}
				while (fileSystemEventHandler != fileSystemEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				FileSystemEventHandler fileSystemEventHandler = this.Deleted;
				FileSystemEventHandler fileSystemEventHandler2;
				do
				{
					fileSystemEventHandler2 = fileSystemEventHandler;
					FileSystemEventHandler value2 = (FileSystemEventHandler)Delegate.Remove(fileSystemEventHandler2, value);
					fileSystemEventHandler = Interlocked.CompareExchange<FileSystemEventHandler>(ref this.Deleted, value2, fileSystemEventHandler2);
				}
				while (fileSystemEventHandler != fileSystemEventHandler2);
			}
		}

		/// <summary>Occurs when the instance of <see cref="T:System.IO.FileSystemWatcher" /> is unable to continue monitoring changes or when the internal buffer overflows.</summary>
		// Token: 0x1400002E RID: 46
		// (add) Token: 0x06001514 RID: 5396 RVA: 0x0004CE3C File Offset: 0x0004B03C
		// (remove) Token: 0x06001515 RID: 5397 RVA: 0x0004CE74 File Offset: 0x0004B074
		public event ErrorEventHandler Error
		{
			[CompilerGenerated]
			add
			{
				ErrorEventHandler errorEventHandler = this.Error;
				ErrorEventHandler errorEventHandler2;
				do
				{
					errorEventHandler2 = errorEventHandler;
					ErrorEventHandler value2 = (ErrorEventHandler)Delegate.Combine(errorEventHandler2, value);
					errorEventHandler = Interlocked.CompareExchange<ErrorEventHandler>(ref this.Error, value2, errorEventHandler2);
				}
				while (errorEventHandler != errorEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				ErrorEventHandler errorEventHandler = this.Error;
				ErrorEventHandler errorEventHandler2;
				do
				{
					errorEventHandler2 = errorEventHandler;
					ErrorEventHandler value2 = (ErrorEventHandler)Delegate.Remove(errorEventHandler2, value);
					errorEventHandler = Interlocked.CompareExchange<ErrorEventHandler>(ref this.Error, value2, errorEventHandler2);
				}
				while (errorEventHandler != errorEventHandler2);
			}
		}

		/// <summary>Occurs when a file or directory in the specified <see cref="P:System.IO.FileSystemWatcher.Path" /> is renamed.</summary>
		// Token: 0x1400002F RID: 47
		// (add) Token: 0x06001516 RID: 5398 RVA: 0x0004CEAC File Offset: 0x0004B0AC
		// (remove) Token: 0x06001517 RID: 5399 RVA: 0x0004CEE4 File Offset: 0x0004B0E4
		public event RenamedEventHandler Renamed
		{
			[CompilerGenerated]
			add
			{
				RenamedEventHandler renamedEventHandler = this.Renamed;
				RenamedEventHandler renamedEventHandler2;
				do
				{
					renamedEventHandler2 = renamedEventHandler;
					RenamedEventHandler value2 = (RenamedEventHandler)Delegate.Combine(renamedEventHandler2, value);
					renamedEventHandler = Interlocked.CompareExchange<RenamedEventHandler>(ref this.Renamed, value2, renamedEventHandler2);
				}
				while (renamedEventHandler != renamedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				RenamedEventHandler renamedEventHandler = this.Renamed;
				RenamedEventHandler renamedEventHandler2;
				do
				{
					renamedEventHandler2 = renamedEventHandler;
					RenamedEventHandler value2 = (RenamedEventHandler)Delegate.Remove(renamedEventHandler2, value);
					renamedEventHandler = Interlocked.CompareExchange<RenamedEventHandler>(ref this.Renamed, value2, renamedEventHandler2);
				}
				while (renamedEventHandler != renamedEventHandler2);
			}
		}

		/// <summary>Begins the initialization of a <see cref="T:System.IO.FileSystemWatcher" /> used on a form or used by another component. The initialization occurs at run time.</summary>
		// Token: 0x06001518 RID: 5400 RVA: 0x000068D7 File Offset: 0x00004AD7
		public void BeginInit()
		{
			throw new NotImplementedException();
		}

		/// <summary>Ends the initialization of a <see cref="T:System.IO.FileSystemWatcher" /> used on a form or used by another component. The initialization occurs at run time.</summary>
		// Token: 0x06001519 RID: 5401 RVA: 0x000068D7 File Offset: 0x00004AD7
		public void EndInit()
		{
			throw new NotImplementedException();
		}

		/// <summary>Raises the <see cref="E:System.IO.FileSystemWatcher.Changed" /> event.</summary>
		/// <param name="e">A <see cref="T:System.IO.FileSystemEventArgs" /> that contains the event data. </param>
		// Token: 0x0600151A RID: 5402 RVA: 0x000068D7 File Offset: 0x00004AD7
		protected void OnChanged(FileSystemEventArgs e)
		{
			throw new NotImplementedException();
		}

		/// <summary>Raises the <see cref="E:System.IO.FileSystemWatcher.Created" /> event.</summary>
		/// <param name="e">A <see cref="T:System.IO.FileSystemEventArgs" /> that contains the event data. </param>
		// Token: 0x0600151B RID: 5403 RVA: 0x000068D7 File Offset: 0x00004AD7
		protected void OnCreated(FileSystemEventArgs e)
		{
			throw new NotImplementedException();
		}

		/// <summary>Raises the <see cref="E:System.IO.FileSystemWatcher.Deleted" /> event.</summary>
		/// <param name="e">A <see cref="T:System.IO.FileSystemEventArgs" /> that contains the event data. </param>
		// Token: 0x0600151C RID: 5404 RVA: 0x000068D7 File Offset: 0x00004AD7
		protected void OnDeleted(FileSystemEventArgs e)
		{
			throw new NotImplementedException();
		}

		/// <summary>Raises the <see cref="E:System.IO.FileSystemWatcher.Error" /> event.</summary>
		/// <param name="e">An <see cref="T:System.IO.ErrorEventArgs" /> that contains the event data. </param>
		// Token: 0x0600151D RID: 5405 RVA: 0x000068D7 File Offset: 0x00004AD7
		protected void OnError(ErrorEventArgs e)
		{
			throw new NotImplementedException();
		}

		/// <summary>Raises the <see cref="E:System.IO.FileSystemWatcher.Renamed" /> event.</summary>
		/// <param name="e">A <see cref="T:System.IO.RenamedEventArgs" /> that contains the event data. </param>
		// Token: 0x0600151E RID: 5406 RVA: 0x000068D7 File Offset: 0x00004AD7
		protected void OnRenamed(RenamedEventArgs e)
		{
			throw new NotImplementedException();
		}

		/// <summary>A synchronous method that returns a structure that contains specific information on the change that occurred, given the type of change you want to monitor.</summary>
		/// <param name="changeType">The <see cref="T:System.IO.WatcherChangeTypes" /> to watch for. </param>
		/// <returns>A <see cref="T:System.IO.WaitForChangedResult" /> that contains specific information on the change that occurred.</returns>
		// Token: 0x0600151F RID: 5407 RVA: 0x000068D7 File Offset: 0x00004AD7
		public WaitForChangedResult WaitForChanged(WatcherChangeTypes changeType)
		{
			throw new NotImplementedException();
		}

		/// <summary>A synchronous method that returns a structure that contains specific information on the change that occurred, given the type of change you want to monitor and the time (in milliseconds) to wait before timing out.</summary>
		/// <param name="changeType">The <see cref="T:System.IO.WatcherChangeTypes" /> to watch for. </param>
		/// <param name="timeout">The time (in milliseconds) to wait before timing out. </param>
		/// <returns>A <see cref="T:System.IO.WaitForChangedResult" /> that contains specific information on the change that occurred.</returns>
		// Token: 0x06001520 RID: 5408 RVA: 0x000068D7 File Offset: 0x00004AD7
		public WaitForChangedResult WaitForChanged(WatcherChangeTypes changeType, int timeout)
		{
			throw new NotImplementedException();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.IO.FileSystemWatcher" /> and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x06001521 RID: 5409 RVA: 0x000068D7 File Offset: 0x00004AD7
		protected override void Dispose(bool disposing)
		{
			throw new NotImplementedException();
		}

		// Token: 0x040013CD RID: 5069
		[CompilerGenerated]
		private FileSystemEventHandler Changed;

		// Token: 0x040013CE RID: 5070
		[CompilerGenerated]
		private FileSystemEventHandler Created;

		// Token: 0x040013CF RID: 5071
		[CompilerGenerated]
		private FileSystemEventHandler Deleted;

		// Token: 0x040013D0 RID: 5072
		[CompilerGenerated]
		private ErrorEventHandler Error;

		// Token: 0x040013D1 RID: 5073
		[CompilerGenerated]
		private RenamedEventHandler Renamed;
	}
}
