﻿using System;

namespace System
{
	// Token: 0x020000B2 RID: 178
	internal static class LocalAppContextSwitches
	{
		// Token: 0x06000430 RID: 1072 RVA: 0x0000232D File Offset: 0x0000052D
		// Note: this type is marked as 'beforefieldinit'.
		static LocalAppContextSwitches()
		{
		}

		// Token: 0x04000961 RID: 2401
		public static readonly bool MemberDescriptorEqualsReturnsFalseIfEquivalent;
	}
}
