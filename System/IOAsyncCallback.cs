﻿using System;

namespace System
{
	// Token: 0x020000B4 RID: 180
	// (Invoke) Token: 0x06000432 RID: 1074
	internal delegate void IOAsyncCallback(IOAsyncResult ioares);
}
