﻿using System;

namespace System
{
	/// <summary>A parser based on the NetTcp scheme for the "Indigo" system.</summary>
	// Token: 0x020000A8 RID: 168
	public class NetTcpStyleUriParser : UriParser
	{
		/// <summary>Create a parser based on the NetTcp scheme for the "Indigo" system.</summary>
		// Token: 0x060003EE RID: 1006 RVA: 0x00013763 File Offset: 0x00011963
		public NetTcpStyleUriParser() : base(UriParser.NetTcpUri.Flags)
		{
		}
	}
}
