﻿using System;

namespace System
{
	/// <summary>A customizable parser based on the HTTP scheme.</summary>
	// Token: 0x020000A1 RID: 161
	public class HttpStyleUriParser : UriParser
	{
		/// <summary>Create a customizable parser based on the HTTP scheme.</summary>
		// Token: 0x060003E7 RID: 999 RVA: 0x000136E5 File Offset: 0x000118E5
		public HttpStyleUriParser() : base(UriParser.HttpUri.Flags)
		{
		}
	}
}
