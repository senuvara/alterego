﻿using System;
using System.Diagnostics;
using System.Threading;

namespace System.Buffers
{
	// Token: 0x020002D3 RID: 723
	internal sealed class DefaultArrayPool<T> : ArrayPool<T>
	{
		// Token: 0x060015B1 RID: 5553 RVA: 0x0004DE3D File Offset: 0x0004C03D
		internal DefaultArrayPool() : this(1048576, 50)
		{
		}

		// Token: 0x060015B2 RID: 5554 RVA: 0x0004DE4C File Offset: 0x0004C04C
		internal DefaultArrayPool(int maxArrayLength, int maxArraysPerBucket)
		{
			if (maxArrayLength <= 0)
			{
				throw new ArgumentOutOfRangeException("maxArrayLength");
			}
			if (maxArraysPerBucket <= 0)
			{
				throw new ArgumentOutOfRangeException("maxArraysPerBucket");
			}
			if (maxArrayLength > 1073741824)
			{
				maxArrayLength = 1073741824;
			}
			else if (maxArrayLength < 16)
			{
				maxArrayLength = 16;
			}
			int id = this.Id;
			DefaultArrayPool<T>.Bucket[] array = new DefaultArrayPool<T>.Bucket[Utilities.SelectBucketIndex(maxArrayLength) + 1];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = new DefaultArrayPool<T>.Bucket(Utilities.GetMaxSizeForBucket(i), maxArraysPerBucket, id);
			}
			this._buckets = array;
		}

		// Token: 0x17000478 RID: 1144
		// (get) Token: 0x060015B3 RID: 5555 RVA: 0x0004DED1 File Offset: 0x0004C0D1
		private int Id
		{
			get
			{
				return this.GetHashCode();
			}
		}

		// Token: 0x060015B4 RID: 5556 RVA: 0x0004DEDC File Offset: 0x0004C0DC
		public override T[] Rent(int minimumLength)
		{
			if (minimumLength < 0)
			{
				throw new ArgumentOutOfRangeException("minimumLength");
			}
			if (minimumLength == 0)
			{
				T[] result;
				if ((result = DefaultArrayPool<T>.s_emptyArray) == null)
				{
					result = (DefaultArrayPool<T>.s_emptyArray = new T[0]);
				}
				return result;
			}
			ArrayPoolEventSource log = ArrayPoolEventSource.Log;
			int num = Utilities.SelectBucketIndex(minimumLength);
			T[] array;
			if (num < this._buckets.Length)
			{
				int num2 = num;
				for (;;)
				{
					array = this._buckets[num2].Rent();
					if (array != null)
					{
						break;
					}
					if (++num2 >= this._buckets.Length || num2 == num + 2)
					{
						goto IL_96;
					}
				}
				if (log.IsEnabled())
				{
					log.BufferRented(array.GetHashCode(), array.Length, this.Id, this._buckets[num2].Id);
				}
				return array;
				IL_96:
				array = new T[this._buckets[num]._bufferLength];
			}
			else
			{
				array = new T[minimumLength];
			}
			if (log.IsEnabled())
			{
				int hashCode = array.GetHashCode();
				int bucketId = -1;
				log.BufferRented(hashCode, array.Length, this.Id, bucketId);
				log.BufferAllocated(hashCode, array.Length, this.Id, bucketId, (num >= this._buckets.Length) ? ArrayPoolEventSource.BufferAllocatedReason.OverMaximumSize : ArrayPoolEventSource.BufferAllocatedReason.PoolExhausted);
			}
			return array;
		}

		// Token: 0x060015B5 RID: 5557 RVA: 0x0004DFE4 File Offset: 0x0004C1E4
		public override void Return(T[] array, bool clearArray = false)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Length == 0)
			{
				return;
			}
			int num = Utilities.SelectBucketIndex(array.Length);
			if (num < this._buckets.Length)
			{
				if (clearArray)
				{
					Array.Clear(array, 0, array.Length);
				}
				this._buckets[num].Return(array);
			}
			ArrayPoolEventSource log = ArrayPoolEventSource.Log;
			if (log.IsEnabled())
			{
				log.BufferReturned(array.GetHashCode(), array.Length, this.Id);
			}
		}

		// Token: 0x04001404 RID: 5124
		private const int DefaultMaxArrayLength = 1048576;

		// Token: 0x04001405 RID: 5125
		private const int DefaultMaxNumberOfArraysPerBucket = 50;

		// Token: 0x04001406 RID: 5126
		private static T[] s_emptyArray;

		// Token: 0x04001407 RID: 5127
		private readonly DefaultArrayPool<T>.Bucket[] _buckets;

		// Token: 0x020002D4 RID: 724
		private sealed class Bucket
		{
			// Token: 0x060015B6 RID: 5558 RVA: 0x0004E056 File Offset: 0x0004C256
			internal Bucket(int bufferLength, int numberOfBuffers, int poolId)
			{
				this._lock = new SpinLock(Debugger.IsAttached);
				this._buffers = new T[numberOfBuffers][];
				this._bufferLength = bufferLength;
				this._poolId = poolId;
			}

			// Token: 0x17000479 RID: 1145
			// (get) Token: 0x060015B7 RID: 5559 RVA: 0x0004DED1 File Offset: 0x0004C0D1
			internal int Id
			{
				get
				{
					return this.GetHashCode();
				}
			}

			// Token: 0x060015B8 RID: 5560 RVA: 0x0004E088 File Offset: 0x0004C288
			internal T[] Rent()
			{
				T[][] buffers = this._buffers;
				T[] array = null;
				bool flag = false;
				bool flag2 = false;
				try
				{
					this._lock.Enter(ref flag);
					if (this._index < buffers.Length)
					{
						array = buffers[this._index];
						T[][] array2 = buffers;
						int index = this._index;
						this._index = index + 1;
						array2[index] = null;
						flag2 = (array == null);
					}
				}
				finally
				{
					if (flag)
					{
						this._lock.Exit(false);
					}
				}
				if (flag2)
				{
					array = new T[this._bufferLength];
					ArrayPoolEventSource log = ArrayPoolEventSource.Log;
					if (log.IsEnabled())
					{
						log.BufferAllocated(array.GetHashCode(), this._bufferLength, this._poolId, this.Id, ArrayPoolEventSource.BufferAllocatedReason.Pooled);
					}
				}
				return array;
			}

			// Token: 0x060015B9 RID: 5561 RVA: 0x0004E144 File Offset: 0x0004C344
			internal void Return(T[] array)
			{
				if (array.Length != this._bufferLength)
				{
					throw new ArgumentException("The buffer is not associated with this pool and may not be returned to it.", "array");
				}
				bool flag = false;
				try
				{
					this._lock.Enter(ref flag);
					if (this._index != 0)
					{
						T[][] buffers = this._buffers;
						int num = this._index - 1;
						this._index = num;
						buffers[num] = array;
					}
				}
				finally
				{
					if (flag)
					{
						this._lock.Exit(false);
					}
				}
			}

			// Token: 0x04001408 RID: 5128
			internal readonly int _bufferLength;

			// Token: 0x04001409 RID: 5129
			private readonly T[][] _buffers;

			// Token: 0x0400140A RID: 5130
			private readonly int _poolId;

			// Token: 0x0400140B RID: 5131
			private SpinLock _lock;

			// Token: 0x0400140C RID: 5132
			private int _index;
		}
	}
}
