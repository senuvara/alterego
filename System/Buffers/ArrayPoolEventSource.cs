﻿using System;
using System.Diagnostics.Tracing;

namespace System.Buffers
{
	// Token: 0x020002D1 RID: 721
	[EventSource(Name = "System.Buffers.ArrayPoolEventSource")]
	internal sealed class ArrayPoolEventSource : EventSource
	{
		// Token: 0x060015AC RID: 5548 RVA: 0x0004DC9C File Offset: 0x0004BE9C
		[Event(1, Level = EventLevel.Verbose)]
		internal unsafe void BufferRented(int bufferId, int bufferSize, int poolId, int bucketId)
		{
			EventSource.EventData* ptr = stackalloc EventSource.EventData[checked(unchecked((UIntPtr)4) * (UIntPtr)sizeof(EventSource.EventData))];
			ptr->Size = 4;
			ptr->DataPointer = (IntPtr)((void*)(&bufferId));
			ptr[1].Size = 4;
			ptr[1].DataPointer = (IntPtr)((void*)(&bufferSize));
			ptr[2].Size = 4;
			ptr[2].DataPointer = (IntPtr)((void*)(&poolId));
			ptr[3].Size = 4;
			ptr[3].DataPointer = (IntPtr)((void*)(&bucketId));
			base.WriteEventCore(1, 4, ptr);
		}

		// Token: 0x060015AD RID: 5549 RVA: 0x0004DD48 File Offset: 0x0004BF48
		[Event(2, Level = EventLevel.Informational)]
		internal unsafe void BufferAllocated(int bufferId, int bufferSize, int poolId, int bucketId, ArrayPoolEventSource.BufferAllocatedReason reason)
		{
			EventSource.EventData* ptr = stackalloc EventSource.EventData[checked(unchecked((UIntPtr)5) * (UIntPtr)sizeof(EventSource.EventData))];
			ptr->Size = 4;
			ptr->DataPointer = (IntPtr)((void*)(&bufferId));
			ptr[1].Size = 4;
			ptr[1].DataPointer = (IntPtr)((void*)(&bufferSize));
			ptr[2].Size = 4;
			ptr[2].DataPointer = (IntPtr)((void*)(&poolId));
			ptr[3].Size = 4;
			ptr[3].DataPointer = (IntPtr)((void*)(&bucketId));
			ptr[4].Size = 4;
			ptr[4].DataPointer = (IntPtr)((void*)(&reason));
			base.WriteEventCore(2, 5, ptr);
		}

		// Token: 0x060015AE RID: 5550 RVA: 0x0004DE1D File Offset: 0x0004C01D
		[Event(3, Level = EventLevel.Verbose)]
		internal void BufferReturned(int bufferId, int bufferSize, int poolId)
		{
			base.WriteEvent(3, bufferId, bufferSize, poolId);
		}

		// Token: 0x060015AF RID: 5551 RVA: 0x0004DE29 File Offset: 0x0004C029
		public ArrayPoolEventSource()
		{
		}

		// Token: 0x060015B0 RID: 5552 RVA: 0x0004DE31 File Offset: 0x0004C031
		// Note: this type is marked as 'beforefieldinit'.
		static ArrayPoolEventSource()
		{
		}

		// Token: 0x040013FF RID: 5119
		internal static readonly ArrayPoolEventSource Log = new ArrayPoolEventSource();

		// Token: 0x020002D2 RID: 722
		internal enum BufferAllocatedReason
		{
			// Token: 0x04001401 RID: 5121
			Pooled,
			// Token: 0x04001402 RID: 5122
			OverMaximumSize,
			// Token: 0x04001403 RID: 5123
			PoolExhausted
		}
	}
}
