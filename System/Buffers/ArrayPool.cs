﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Buffers
{
	// Token: 0x020002D0 RID: 720
	internal abstract class ArrayPool<T>
	{
		// Token: 0x17000477 RID: 1143
		// (get) Token: 0x060015A4 RID: 5540 RVA: 0x0004DC5F File Offset: 0x0004BE5F
		public static ArrayPool<T> Shared
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				return Volatile.Read<ArrayPool<T>>(ref ArrayPool<T>.s_sharedInstance) ?? ArrayPool<T>.EnsureSharedCreated();
			}
		}

		// Token: 0x060015A5 RID: 5541 RVA: 0x0004DC74 File Offset: 0x0004BE74
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static ArrayPool<T> EnsureSharedCreated()
		{
			Interlocked.CompareExchange<ArrayPool<T>>(ref ArrayPool<T>.s_sharedInstance, ArrayPool<T>.Create(), null);
			return ArrayPool<T>.s_sharedInstance;
		}

		// Token: 0x060015A6 RID: 5542 RVA: 0x0004DC8C File Offset: 0x0004BE8C
		public static ArrayPool<T> Create()
		{
			return new DefaultArrayPool<T>();
		}

		// Token: 0x060015A7 RID: 5543 RVA: 0x0004DC93 File Offset: 0x0004BE93
		public static ArrayPool<T> Create(int maxArrayLength, int maxArraysPerBucket)
		{
			return new DefaultArrayPool<T>(maxArrayLength, maxArraysPerBucket);
		}

		// Token: 0x060015A8 RID: 5544
		public abstract T[] Rent(int minimumLength);

		// Token: 0x060015A9 RID: 5545
		public abstract void Return(T[] array, bool clearArray = false);

		// Token: 0x060015AA RID: 5546 RVA: 0x0000232F File Offset: 0x0000052F
		protected ArrayPool()
		{
		}

		// Token: 0x060015AB RID: 5547 RVA: 0x0000232D File Offset: 0x0000052D
		// Note: this type is marked as 'beforefieldinit'.
		static ArrayPool()
		{
		}

		// Token: 0x040013FE RID: 5118
		private static ArrayPool<T> s_sharedInstance;
	}
}
