﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("4.0.0.0")]
[assembly: ComVisible(false)]
[assembly: AssemblyTitle("System.Xml.Linq.dll")]
[assembly: AssemblyDescription("System.Xml.Linq.dll")]
[assembly: AssemblyDefaultAlias("System.Xml.Linq.dll")]
[assembly: AssemblyProduct("Mono Common Language Infrastructure")]
[assembly: AssemblyCompany("Mono development team")]
[assembly: SatelliteContractVersion("4.0.0.0")]
[assembly: AssemblyInformationalVersion("4.0.30319.17020")]
[assembly: AssemblyFileVersion("4.0.30319.17020")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: CLSCompliant(true)]
[assembly: AssemblyDelaySign(true)]
[assembly: AssemblyKeyFile("../winfx.pub")]
[assembly: AssemblyCopyright("(c) Various Mono authors")]
