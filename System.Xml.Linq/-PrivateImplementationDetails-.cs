﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x02000055 RID: 85
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x06000392 RID: 914 RVA: 0x0000EBC4 File Offset: 0x0000CDC4
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x04000158 RID: 344 RVA: 0x0000EC0C File Offset: 0x0000CE0C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 1DE4C9D3404961D2404040F52C059D466528856D;

	// Token: 0x04000159 RID: 345 RVA: 0x0000EC34 File Offset: 0x0000CE34
	internal static readonly long EBC658B067B5C785A3F0BB67D73755F6FEE7F70C;

	// Token: 0x02000056 RID: 86
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 40)]
	private struct __StaticArrayInitTypeSize=40
	{
	}
}
