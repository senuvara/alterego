﻿using System;

namespace Unity
{
	// Token: 0x02000058 RID: 88
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x06000394 RID: 916 RVA: 0x0000EC04 File Offset: 0x0000CE04
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
