﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;

namespace System.Xml.Schema
{
	// Token: 0x02000002 RID: 2
	internal class XNodeValidator
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public XNodeValidator(XmlSchemaSet schemas, ValidationEventHandler validationEventHandler)
		{
			this.schemas = schemas;
			this.validationEventHandler = validationEventHandler;
			XNamespace xnamespace = XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance");
			this.xsiTypeName = xnamespace.GetName("type");
			this.xsiNilName = xnamespace.GetName("nil");
		}

		// Token: 0x06000002 RID: 2 RVA: 0x000020A0 File Offset: 0x000002A0
		public void Validate(XObject source, XmlSchemaObject partialValidationType, bool addSchemaInfo)
		{
			this.source = source;
			this.addSchemaInfo = addSchemaInfo;
			XmlSchemaValidationFlags xmlSchemaValidationFlags = XmlSchemaValidationFlags.AllowXmlAttributes;
			XmlNodeType nodeType = source.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType != XmlNodeType.Attribute)
				{
					if (nodeType == XmlNodeType.Document)
					{
						source = ((XDocument)source).Root;
						if (source == null)
						{
							throw new InvalidOperationException(Res.GetString("InvalidOperation_MissingRoot"));
						}
						xmlSchemaValidationFlags |= XmlSchemaValidationFlags.ProcessIdentityConstraints;
						goto IL_8F;
					}
				}
				else if (!((XAttribute)source).IsNamespaceDeclaration)
				{
					if (source.Parent == null)
					{
						throw new InvalidOperationException(Res.GetString("InvalidOperation_MissingParent"));
					}
					goto IL_8F;
				}
				throw new InvalidOperationException(Res.GetString("InvalidOperation_BadNodeType", new object[]
				{
					nodeType
				}));
			}
			IL_8F:
			this.namespaceManager = new XmlNamespaceManager(this.schemas.NameTable);
			this.PushAncestorsAndSelf(source.Parent);
			this.validator = new XmlSchemaValidator(this.schemas.NameTable, this.schemas, this.namespaceManager, xmlSchemaValidationFlags);
			this.validator.ValidationEventHandler += this.ValidationCallback;
			this.validator.XmlResolver = null;
			if (partialValidationType != null)
			{
				this.validator.Initialize(partialValidationType);
			}
			else
			{
				this.validator.Initialize();
			}
			IXmlLineInfo originalLineInfo = this.SaveLineInfo(source);
			if (nodeType == XmlNodeType.Attribute)
			{
				this.ValidateAttribute((XAttribute)source);
			}
			else
			{
				this.ValidateElement((XElement)source);
			}
			this.validator.EndValidation();
			this.RestoreLineInfo(originalLineInfo);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000021F8 File Offset: 0x000003F8
		private XmlSchemaInfo GetDefaultAttributeSchemaInfo(XmlSchemaAttribute sa)
		{
			XmlSchemaInfo xmlSchemaInfo = new XmlSchemaInfo();
			xmlSchemaInfo.IsDefault = true;
			xmlSchemaInfo.IsNil = false;
			xmlSchemaInfo.SchemaAttribute = sa;
			XmlSchemaSimpleType attributeSchemaType = sa.AttributeSchemaType;
			xmlSchemaInfo.SchemaType = attributeSchemaType;
			if (attributeSchemaType.Datatype.Variety == XmlSchemaDatatypeVariety.Union)
			{
				string defaultValue = this.GetDefaultValue(sa);
				foreach (XmlSchemaSimpleType xmlSchemaSimpleType in ((XmlSchemaSimpleTypeUnion)attributeSchemaType.Content).BaseMemberTypes)
				{
					object obj = null;
					try
					{
						obj = xmlSchemaSimpleType.Datatype.ParseValue(defaultValue, this.schemas.NameTable, this.namespaceManager);
					}
					catch (XmlSchemaException)
					{
					}
					if (obj != null)
					{
						xmlSchemaInfo.MemberType = xmlSchemaSimpleType;
						break;
					}
				}
			}
			xmlSchemaInfo.Validity = XmlSchemaValidity.Valid;
			return xmlSchemaInfo;
		}

		// Token: 0x06000004 RID: 4 RVA: 0x000022BC File Offset: 0x000004BC
		private string GetDefaultValue(XmlSchemaAttribute sa)
		{
			XmlQualifiedName refName = sa.RefName;
			if (!refName.IsEmpty)
			{
				sa = (this.schemas.GlobalAttributes[refName] as XmlSchemaAttribute);
				if (sa == null)
				{
					return null;
				}
			}
			string fixedValue = sa.FixedValue;
			if (fixedValue != null)
			{
				return fixedValue;
			}
			return sa.DefaultValue;
		}

		// Token: 0x06000005 RID: 5 RVA: 0x00002308 File Offset: 0x00000508
		private string GetDefaultValue(XmlSchemaElement se)
		{
			XmlQualifiedName refName = se.RefName;
			if (!refName.IsEmpty)
			{
				se = (this.schemas.GlobalElements[refName] as XmlSchemaElement);
				if (se == null)
				{
					return null;
				}
			}
			string fixedValue = se.FixedValue;
			if (fixedValue != null)
			{
				return fixedValue;
			}
			return se.DefaultValue;
		}

		// Token: 0x06000006 RID: 6 RVA: 0x00002354 File Offset: 0x00000554
		private void ReplaceSchemaInfo(XObject o, XmlSchemaInfo schemaInfo)
		{
			if (this.schemaInfos == null)
			{
				this.schemaInfos = new Dictionary<XmlSchemaInfo, XmlSchemaInfo>(new XmlSchemaInfoEqualityComparer());
			}
			XmlSchemaInfo xmlSchemaInfo = o.Annotation<XmlSchemaInfo>();
			if (xmlSchemaInfo != null)
			{
				if (!this.schemaInfos.ContainsKey(xmlSchemaInfo))
				{
					this.schemaInfos.Add(xmlSchemaInfo, xmlSchemaInfo);
				}
				o.RemoveAnnotations<XmlSchemaInfo>();
			}
			if (!this.schemaInfos.TryGetValue(schemaInfo, out xmlSchemaInfo))
			{
				xmlSchemaInfo = schemaInfo;
				this.schemaInfos.Add(xmlSchemaInfo, xmlSchemaInfo);
			}
			o.AddAnnotation(xmlSchemaInfo);
		}

		// Token: 0x06000007 RID: 7 RVA: 0x000023CC File Offset: 0x000005CC
		private void PushAncestorsAndSelf(XElement e)
		{
			while (e != null)
			{
				XAttribute xattribute = e.lastAttr;
				if (xattribute != null)
				{
					do
					{
						xattribute = xattribute.next;
						if (xattribute.IsNamespaceDeclaration)
						{
							string text = xattribute.Name.LocalName;
							if (text == "xmlns")
							{
								text = string.Empty;
							}
							if (!this.namespaceManager.HasNamespace(text))
							{
								this.namespaceManager.AddNamespace(text, xattribute.Value);
							}
						}
					}
					while (xattribute != e.lastAttr);
				}
				e = (e.parent as XElement);
			}
		}

		// Token: 0x06000008 RID: 8 RVA: 0x0000244C File Offset: 0x0000064C
		private void PushElement(XElement e, ref string xsiType, ref string xsiNil)
		{
			this.namespaceManager.PushScope();
			XAttribute xattribute = e.lastAttr;
			if (xattribute != null)
			{
				do
				{
					xattribute = xattribute.next;
					if (xattribute.IsNamespaceDeclaration)
					{
						string text = xattribute.Name.LocalName;
						if (text == "xmlns")
						{
							text = string.Empty;
						}
						this.namespaceManager.AddNamespace(text, xattribute.Value);
					}
					else
					{
						XName name = xattribute.Name;
						if (name == this.xsiTypeName)
						{
							xsiType = xattribute.Value;
						}
						else if (name == this.xsiNilName)
						{
							xsiNil = xattribute.Value;
						}
					}
				}
				while (xattribute != e.lastAttr);
			}
		}

		// Token: 0x06000009 RID: 9 RVA: 0x000024F1 File Offset: 0x000006F1
		private IXmlLineInfo SaveLineInfo(XObject source)
		{
			IXmlLineInfo lineInfoProvider = this.validator.LineInfoProvider;
			this.validator.LineInfoProvider = source;
			return lineInfoProvider;
		}

		// Token: 0x0600000A RID: 10 RVA: 0x0000250A File Offset: 0x0000070A
		private void RestoreLineInfo(IXmlLineInfo originalLineInfo)
		{
			this.validator.LineInfoProvider = originalLineInfo;
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002518 File Offset: 0x00000718
		private void ValidateAttribute(XAttribute a)
		{
			IXmlLineInfo originalLineInfo = this.SaveLineInfo(a);
			XmlSchemaInfo schemaInfo = this.addSchemaInfo ? new XmlSchemaInfo() : null;
			this.source = a;
			this.validator.ValidateAttribute(a.Name.LocalName, a.Name.NamespaceName, a.Value, schemaInfo);
			if (this.addSchemaInfo)
			{
				this.ReplaceSchemaInfo(a, schemaInfo);
			}
			this.RestoreLineInfo(originalLineInfo);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002588 File Offset: 0x00000788
		private void ValidateAttributes(XElement e)
		{
			XAttribute xattribute = e.lastAttr;
			IXmlLineInfo originalLineInfo = this.SaveLineInfo(xattribute);
			if (xattribute != null)
			{
				do
				{
					xattribute = xattribute.next;
					if (!xattribute.IsNamespaceDeclaration)
					{
						this.ValidateAttribute(xattribute);
					}
				}
				while (xattribute != e.lastAttr);
				this.source = e;
			}
			if (this.addSchemaInfo)
			{
				if (this.defaultAttributes == null)
				{
					this.defaultAttributes = new ArrayList();
				}
				else
				{
					this.defaultAttributes.Clear();
				}
				this.validator.GetUnspecifiedDefaultAttributes(this.defaultAttributes);
				foreach (object obj in this.defaultAttributes)
				{
					XmlSchemaAttribute xmlSchemaAttribute = (XmlSchemaAttribute)obj;
					xattribute = new XAttribute(XNamespace.Get(xmlSchemaAttribute.QualifiedName.Namespace).GetName(xmlSchemaAttribute.QualifiedName.Name), this.GetDefaultValue(xmlSchemaAttribute));
					this.ReplaceSchemaInfo(xattribute, this.GetDefaultAttributeSchemaInfo(xmlSchemaAttribute));
					e.Add(xattribute);
				}
			}
			this.RestoreLineInfo(originalLineInfo);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x0000269C File Offset: 0x0000089C
		private void ValidateElement(XElement e)
		{
			XmlSchemaInfo xmlSchemaInfo = this.addSchemaInfo ? new XmlSchemaInfo() : null;
			string xsiType = null;
			string xsiNil = null;
			this.PushElement(e, ref xsiType, ref xsiNil);
			IXmlLineInfo originalLineInfo = this.SaveLineInfo(e);
			this.source = e;
			this.validator.ValidateElement(e.Name.LocalName, e.Name.NamespaceName, xmlSchemaInfo, xsiType, xsiNil, null, null);
			this.ValidateAttributes(e);
			this.validator.ValidateEndOfAttributes(xmlSchemaInfo);
			this.ValidateNodes(e);
			this.validator.ValidateEndElement(xmlSchemaInfo);
			if (this.addSchemaInfo)
			{
				if (xmlSchemaInfo.Validity == XmlSchemaValidity.Valid && xmlSchemaInfo.IsDefault)
				{
					e.Value = this.GetDefaultValue(xmlSchemaInfo.SchemaElement);
				}
				this.ReplaceSchemaInfo(e, xmlSchemaInfo);
			}
			this.RestoreLineInfo(originalLineInfo);
			this.namespaceManager.PopScope();
		}

		// Token: 0x0600000E RID: 14 RVA: 0x0000276C File Offset: 0x0000096C
		private void ValidateNodes(XElement e)
		{
			XNode xnode = e.content as XNode;
			IXmlLineInfo originalLineInfo = this.SaveLineInfo(xnode);
			if (xnode != null)
			{
				do
				{
					xnode = xnode.next;
					XElement xelement = xnode as XElement;
					if (xelement != null)
					{
						this.ValidateElement(xelement);
					}
					else
					{
						XText xtext = xnode as XText;
						if (xtext != null)
						{
							string value = xtext.Value;
							if (value.Length > 0)
							{
								this.validator.LineInfoProvider = xtext;
								this.validator.ValidateText(value);
							}
						}
					}
				}
				while (xnode != e.content);
				this.source = e;
			}
			else
			{
				string text = e.content as string;
				if (text != null && text.Length > 0)
				{
					this.validator.ValidateText(text);
				}
			}
			this.RestoreLineInfo(originalLineInfo);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002820 File Offset: 0x00000A20
		private void ValidationCallback(object sender, ValidationEventArgs e)
		{
			if (this.validationEventHandler != null)
			{
				this.validationEventHandler(this.source, e);
				return;
			}
			if (e.Severity == XmlSeverityType.Error)
			{
				throw e.Exception;
			}
		}

		// Token: 0x04000001 RID: 1
		private XmlSchemaSet schemas;

		// Token: 0x04000002 RID: 2
		private ValidationEventHandler validationEventHandler;

		// Token: 0x04000003 RID: 3
		private XObject source;

		// Token: 0x04000004 RID: 4
		private bool addSchemaInfo;

		// Token: 0x04000005 RID: 5
		private XmlNamespaceManager namespaceManager;

		// Token: 0x04000006 RID: 6
		private XmlSchemaValidator validator;

		// Token: 0x04000007 RID: 7
		private Dictionary<XmlSchemaInfo, XmlSchemaInfo> schemaInfos;

		// Token: 0x04000008 RID: 8
		private ArrayList defaultAttributes;

		// Token: 0x04000009 RID: 9
		private XName xsiTypeName;

		// Token: 0x0400000A RID: 10
		private XName xsiNilName;
	}
}
