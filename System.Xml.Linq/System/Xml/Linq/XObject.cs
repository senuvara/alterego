﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Xml.Linq
{
	/// <summary>Represents a node or an attribute in an XML tree. </summary>
	// Token: 0x02000010 RID: 16
	public abstract class XObject : IXmlLineInfo
	{
		// Token: 0x06000097 RID: 151 RVA: 0x00002966 File Offset: 0x00000B66
		internal XObject()
		{
		}

		/// <summary>Gets the base URI for this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the base URI for this <see cref="T:System.Xml.Linq.XObject" />.</returns>
		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000098 RID: 152 RVA: 0x00004708 File Offset: 0x00002908
		public string BaseUri
		{
			get
			{
				XObject xobject = this;
				BaseUriAnnotation baseUriAnnotation;
				for (;;)
				{
					if (xobject == null || xobject.annotations != null)
					{
						if (xobject == null)
						{
							goto IL_33;
						}
						baseUriAnnotation = xobject.Annotation<BaseUriAnnotation>();
						if (baseUriAnnotation != null)
						{
							break;
						}
						xobject = xobject.parent;
					}
					else
					{
						xobject = xobject.parent;
					}
				}
				return baseUriAnnotation.baseUri;
				IL_33:
				return string.Empty;
			}
		}

		/// <summary>Gets the <see cref="T:System.Xml.Linq.XDocument" /> for this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <returns>The <see cref="T:System.Xml.Linq.XDocument" /> for this <see cref="T:System.Xml.Linq.XObject" />. </returns>
		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000099 RID: 153 RVA: 0x00004750 File Offset: 0x00002950
		public XDocument Document
		{
			get
			{
				XObject xobject = this;
				while (xobject.parent != null)
				{
					xobject = xobject.parent;
				}
				return xobject as XDocument;
			}
		}

		/// <summary>Gets the node type for this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <returns>The node type for this <see cref="T:System.Xml.Linq.XObject" />. </returns>
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600009A RID: 154
		public abstract XmlNodeType NodeType { get; }

		/// <summary>Gets the parent <see cref="T:System.Xml.Linq.XElement" /> of this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <returns>The parent <see cref="T:System.Xml.Linq.XElement" /> of this <see cref="T:System.Xml.Linq.XObject" />.</returns>
		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600009B RID: 155 RVA: 0x00004776 File Offset: 0x00002976
		public XElement Parent
		{
			get
			{
				return this.parent as XElement;
			}
		}

		/// <summary>Adds an object to the annotation list of this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <param name="annotation">An <see cref="T:System.Object" /> that contains the annotation to add.</param>
		// Token: 0x0600009C RID: 156 RVA: 0x00004784 File Offset: 0x00002984
		public void AddAnnotation(object annotation)
		{
			if (annotation == null)
			{
				throw new ArgumentNullException("annotation");
			}
			if (this.annotations == null)
			{
				object obj;
				if (!(annotation is object[]))
				{
					obj = annotation;
				}
				else
				{
					(obj = new object[1])[0] = annotation;
				}
				this.annotations = obj;
				return;
			}
			object[] array = this.annotations as object[];
			if (array == null)
			{
				this.annotations = new object[]
				{
					this.annotations,
					annotation
				};
				return;
			}
			int num = 0;
			while (num < array.Length && array[num] != null)
			{
				num++;
			}
			if (num == array.Length)
			{
				Array.Resize<object>(ref array, num * 2);
				this.annotations = array;
			}
			array[num] = annotation;
		}

		/// <summary>Gets the first annotation object of the specified type from this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> of the annotation to retrieve.</param>
		/// <returns>The <see cref="T:System.Object" /> that contains the first annotation object that matches the specified type, or <see langword="null" /> if no annotation is of the specified type.</returns>
		// Token: 0x0600009D RID: 157 RVA: 0x0000481C File Offset: 0x00002A1C
		public object Annotation(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (this.annotations != null)
			{
				object[] array = this.annotations as object[];
				if (array == null)
				{
					if (type.IsInstanceOfType(this.annotations))
					{
						return this.annotations;
					}
				}
				else
				{
					foreach (object obj in array)
					{
						if (obj == null)
						{
							break;
						}
						if (type.IsInstanceOfType(obj))
						{
							return obj;
						}
					}
				}
			}
			return null;
		}

		/// <summary>Get the first annotation object of the specified type from this <see cref="T:System.Xml.Linq.XObject" />. </summary>
		/// <typeparam name="T">The type of the annotation to retrieve.</typeparam>
		/// <returns>The first annotation object that matches the specified type, or <see langword="null" /> if no annotation is of the specified type.</returns>
		// Token: 0x0600009E RID: 158 RVA: 0x0000488C File Offset: 0x00002A8C
		public T Annotation<T>() where T : class
		{
			if (this.annotations != null)
			{
				object[] array = this.annotations as object[];
				if (array == null)
				{
					return this.annotations as T;
				}
				foreach (object obj in array)
				{
					if (obj == null)
					{
						break;
					}
					T t = obj as T;
					if (t != null)
					{
						return t;
					}
				}
			}
			return default(T);
		}

		/// <summary>Gets a collection of annotations of the specified type for this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> of the annotations to retrieve.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Object" /> that contains the annotations that match the specified type for this <see cref="T:System.Xml.Linq.XObject" />.</returns>
		// Token: 0x0600009F RID: 159 RVA: 0x000048F6 File Offset: 0x00002AF6
		public IEnumerable<object> Annotations(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			return this.AnnotationsIterator(type);
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00004913 File Offset: 0x00002B13
		private IEnumerable<object> AnnotationsIterator(Type type)
		{
			if (this.annotations != null)
			{
				object[] a = this.annotations as object[];
				if (a == null)
				{
					if (type.IsInstanceOfType(this.annotations))
					{
						yield return this.annotations;
					}
				}
				else
				{
					int num;
					for (int i = 0; i < a.Length; i = num + 1)
					{
						object obj = a[i];
						if (obj == null)
						{
							break;
						}
						if (type.IsInstanceOfType(obj))
						{
							yield return obj;
						}
						num = i;
					}
				}
				a = null;
			}
			yield break;
		}

		/// <summary>Gets a collection of annotations of the specified type for this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <typeparam name="T">The type of the annotations to retrieve.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the annotations for this <see cref="T:System.Xml.Linq.XObject" />.</returns>
		// Token: 0x060000A1 RID: 161 RVA: 0x0000492A File Offset: 0x00002B2A
		public IEnumerable<T> Annotations<T>() where T : class
		{
			if (this.annotations != null)
			{
				object[] a = this.annotations as object[];
				if (a == null)
				{
					T t = this.annotations as T;
					if (t != null)
					{
						yield return t;
					}
				}
				else
				{
					int num;
					for (int i = 0; i < a.Length; i = num + 1)
					{
						object obj = a[i];
						if (obj == null)
						{
							break;
						}
						T t2 = obj as T;
						if (t2 != null)
						{
							yield return t2;
						}
						num = i;
					}
				}
				a = null;
			}
			yield break;
		}

		/// <summary>Removes the annotations of the specified type from this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> of annotations to remove.</param>
		// Token: 0x060000A2 RID: 162 RVA: 0x0000493C File Offset: 0x00002B3C
		public void RemoveAnnotations(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (this.annotations != null)
			{
				object[] array = this.annotations as object[];
				if (array == null)
				{
					if (type.IsInstanceOfType(this.annotations))
					{
						this.annotations = null;
						return;
					}
				}
				else
				{
					int i = 0;
					int j = 0;
					while (i < array.Length)
					{
						object obj = array[i];
						if (obj == null)
						{
							break;
						}
						if (!type.IsInstanceOfType(obj))
						{
							array[j++] = obj;
						}
						i++;
					}
					if (j == 0)
					{
						this.annotations = null;
						return;
					}
					while (j < i)
					{
						array[j++] = null;
					}
				}
			}
		}

		/// <summary>Removes the annotations of the specified type from this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <typeparam name="T">The type of annotations to remove.</typeparam>
		// Token: 0x060000A3 RID: 163 RVA: 0x000049CC File Offset: 0x00002BCC
		public void RemoveAnnotations<T>() where T : class
		{
			if (this.annotations != null)
			{
				object[] array = this.annotations as object[];
				if (array == null)
				{
					if (this.annotations is T)
					{
						this.annotations = null;
						return;
					}
				}
				else
				{
					int i = 0;
					int j = 0;
					while (i < array.Length)
					{
						object obj = array[i];
						if (obj == null)
						{
							break;
						}
						if (!(obj is T))
						{
							array[j++] = obj;
						}
						i++;
					}
					if (j == 0)
					{
						this.annotations = null;
						return;
					}
					while (j < i)
					{
						array[j++] = null;
					}
				}
			}
		}

		/// <summary>Raised when this <see cref="T:System.Xml.Linq.XObject" /> or any of its descendants have changed.</summary>
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x060000A4 RID: 164 RVA: 0x00004A44 File Offset: 0x00002C44
		// (remove) Token: 0x060000A5 RID: 165 RVA: 0x00004A84 File Offset: 0x00002C84
		public event EventHandler<XObjectChangeEventArgs> Changed
		{
			add
			{
				if (value == null)
				{
					return;
				}
				XObjectChangeAnnotation xobjectChangeAnnotation = this.Annotation<XObjectChangeAnnotation>();
				if (xobjectChangeAnnotation == null)
				{
					xobjectChangeAnnotation = new XObjectChangeAnnotation();
					this.AddAnnotation(xobjectChangeAnnotation);
				}
				XObjectChangeAnnotation xobjectChangeAnnotation2 = xobjectChangeAnnotation;
				xobjectChangeAnnotation2.changed = (EventHandler<XObjectChangeEventArgs>)Delegate.Combine(xobjectChangeAnnotation2.changed, value);
			}
			remove
			{
				if (value == null)
				{
					return;
				}
				XObjectChangeAnnotation xobjectChangeAnnotation = this.Annotation<XObjectChangeAnnotation>();
				if (xobjectChangeAnnotation == null)
				{
					return;
				}
				XObjectChangeAnnotation xobjectChangeAnnotation2 = xobjectChangeAnnotation;
				xobjectChangeAnnotation2.changed = (EventHandler<XObjectChangeEventArgs>)Delegate.Remove(xobjectChangeAnnotation2.changed, value);
				if (xobjectChangeAnnotation.changing == null && xobjectChangeAnnotation.changed == null)
				{
					this.RemoveAnnotations<XObjectChangeAnnotation>();
				}
			}
		}

		/// <summary>Raised when this <see cref="T:System.Xml.Linq.XObject" /> or any of its descendants are about to change.</summary>
		// Token: 0x14000002 RID: 2
		// (add) Token: 0x060000A6 RID: 166 RVA: 0x00004AD0 File Offset: 0x00002CD0
		// (remove) Token: 0x060000A7 RID: 167 RVA: 0x00004B10 File Offset: 0x00002D10
		public event EventHandler<XObjectChangeEventArgs> Changing
		{
			add
			{
				if (value == null)
				{
					return;
				}
				XObjectChangeAnnotation xobjectChangeAnnotation = this.Annotation<XObjectChangeAnnotation>();
				if (xobjectChangeAnnotation == null)
				{
					xobjectChangeAnnotation = new XObjectChangeAnnotation();
					this.AddAnnotation(xobjectChangeAnnotation);
				}
				XObjectChangeAnnotation xobjectChangeAnnotation2 = xobjectChangeAnnotation;
				xobjectChangeAnnotation2.changing = (EventHandler<XObjectChangeEventArgs>)Delegate.Combine(xobjectChangeAnnotation2.changing, value);
			}
			remove
			{
				if (value == null)
				{
					return;
				}
				XObjectChangeAnnotation xobjectChangeAnnotation = this.Annotation<XObjectChangeAnnotation>();
				if (xobjectChangeAnnotation == null)
				{
					return;
				}
				XObjectChangeAnnotation xobjectChangeAnnotation2 = xobjectChangeAnnotation;
				xobjectChangeAnnotation2.changing = (EventHandler<XObjectChangeEventArgs>)Delegate.Remove(xobjectChangeAnnotation2.changing, value);
				if (xobjectChangeAnnotation.changing == null && xobjectChangeAnnotation.changed == null)
				{
					this.RemoveAnnotations<XObjectChangeAnnotation>();
				}
			}
		}

		/// <summary>Gets a value indicating whether or not this <see cref="T:System.Xml.Linq.XObject" /> has line information.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Xml.Linq.XObject" /> has line information, otherwise <see langword="false" />.</returns>
		// Token: 0x060000A8 RID: 168 RVA: 0x00004B59 File Offset: 0x00002D59
		bool IXmlLineInfo.HasLineInfo()
		{
			return this.Annotation<LineInfoAnnotation>() != null;
		}

		/// <summary>Gets the line number that the underlying <see cref="T:System.Xml.XmlReader" /> reported for this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <returns>An <see cref="T:System.Int32" /> that contains the line number reported by the <see cref="T:System.Xml.XmlReader" /> for this <see cref="T:System.Xml.Linq.XObject" />.</returns>
		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000A9 RID: 169 RVA: 0x00004B64 File Offset: 0x00002D64
		int IXmlLineInfo.LineNumber
		{
			get
			{
				LineInfoAnnotation lineInfoAnnotation = this.Annotation<LineInfoAnnotation>();
				if (lineInfoAnnotation != null)
				{
					return lineInfoAnnotation.lineNumber;
				}
				return 0;
			}
		}

		/// <summary>Gets the line position that the underlying <see cref="T:System.Xml.XmlReader" /> reported for this <see cref="T:System.Xml.Linq.XObject" />.</summary>
		/// <returns>An <see cref="T:System.Int32" /> that contains the line position reported by the <see cref="T:System.Xml.XmlReader" /> for this <see cref="T:System.Xml.Linq.XObject" />.</returns>
		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000AA RID: 170 RVA: 0x00004B84 File Offset: 0x00002D84
		int IXmlLineInfo.LinePosition
		{
			get
			{
				LineInfoAnnotation lineInfoAnnotation = this.Annotation<LineInfoAnnotation>();
				if (lineInfoAnnotation != null)
				{
					return lineInfoAnnotation.linePosition;
				}
				return 0;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000AB RID: 171 RVA: 0x00004BA3 File Offset: 0x00002DA3
		internal bool HasBaseUri
		{
			get
			{
				return this.Annotation<BaseUriAnnotation>() != null;
			}
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00004BB0 File Offset: 0x00002DB0
		internal bool NotifyChanged(object sender, XObjectChangeEventArgs e)
		{
			bool result = false;
			XObject xobject = this;
			for (;;)
			{
				if (xobject == null || xobject.annotations != null)
				{
					if (xobject == null)
					{
						break;
					}
					XObjectChangeAnnotation xobjectChangeAnnotation = xobject.Annotation<XObjectChangeAnnotation>();
					if (xobjectChangeAnnotation != null)
					{
						result = true;
						if (xobjectChangeAnnotation.changed != null)
						{
							xobjectChangeAnnotation.changed(sender, e);
						}
					}
					xobject = xobject.parent;
				}
				else
				{
					xobject = xobject.parent;
				}
			}
			return result;
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00004C04 File Offset: 0x00002E04
		internal bool NotifyChanging(object sender, XObjectChangeEventArgs e)
		{
			bool result = false;
			XObject xobject = this;
			for (;;)
			{
				if (xobject == null || xobject.annotations != null)
				{
					if (xobject == null)
					{
						break;
					}
					XObjectChangeAnnotation xobjectChangeAnnotation = xobject.Annotation<XObjectChangeAnnotation>();
					if (xobjectChangeAnnotation != null)
					{
						result = true;
						if (xobjectChangeAnnotation.changing != null)
						{
							xobjectChangeAnnotation.changing(sender, e);
						}
					}
					xobject = xobject.parent;
				}
				else
				{
					xobject = xobject.parent;
				}
			}
			return result;
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00004C57 File Offset: 0x00002E57
		internal void SetBaseUri(string baseUri)
		{
			this.AddAnnotation(new BaseUriAnnotation(baseUri));
		}

		// Token: 0x060000AF RID: 175 RVA: 0x00004C65 File Offset: 0x00002E65
		internal void SetLineInfo(int lineNumber, int linePosition)
		{
			this.AddAnnotation(new LineInfoAnnotation(lineNumber, linePosition));
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00004C74 File Offset: 0x00002E74
		internal bool SkipNotify()
		{
			XObject xobject = this;
			for (;;)
			{
				if (xobject == null || xobject.annotations != null)
				{
					if (xobject == null)
					{
						break;
					}
					if (xobject.Annotations<XObjectChangeAnnotation>() != null)
					{
						return false;
					}
					xobject = xobject.parent;
				}
				else
				{
					xobject = xobject.parent;
				}
			}
			return true;
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00004CB0 File Offset: 0x00002EB0
		internal SaveOptions GetSaveOptionsFromAnnotations()
		{
			XObject xobject = this;
			object obj;
			for (;;)
			{
				if (xobject == null || xobject.annotations != null)
				{
					if (xobject == null)
					{
						break;
					}
					obj = xobject.Annotation(typeof(SaveOptions));
					if (obj != null)
					{
						goto Block_3;
					}
					xobject = xobject.parent;
				}
				else
				{
					xobject = xobject.parent;
				}
			}
			return SaveOptions.None;
			Block_3:
			return (SaveOptions)obj;
		}

		// Token: 0x04000036 RID: 54
		internal XContainer parent;

		// Token: 0x04000037 RID: 55
		internal object annotations;

		// Token: 0x02000011 RID: 17
		[CompilerGenerated]
		private sealed class <AnnotationsIterator>d__15 : IEnumerable<object>, IEnumerable, IEnumerator<object>, IDisposable, IEnumerator
		{
			// Token: 0x060000B2 RID: 178 RVA: 0x00004CFB File Offset: 0x00002EFB
			[DebuggerHidden]
			public <AnnotationsIterator>d__15(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060000B3 RID: 179 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060000B4 RID: 180 RVA: 0x00004D18 File Offset: 0x00002F18
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XObject xobject = this;
				switch (num)
				{
				case 0:
					this.<>1__state = -1;
					if (xobject.annotations == null)
					{
						return false;
					}
					a = (xobject.annotations as object[]);
					if (a != null)
					{
						i = 0;
						goto IL_CD;
					}
					if (type.IsInstanceOfType(xobject.annotations))
					{
						this.<>2__current = xobject.annotations;
						this.<>1__state = 1;
						return true;
					}
					goto IL_DD;
				case 1:
					this.<>1__state = -1;
					goto IL_DD;
				case 2:
					this.<>1__state = -1;
					break;
				default:
					return false;
				}
				IL_BD:
				int num2 = i;
				i = num2 + 1;
				IL_CD:
				if (i < a.Length)
				{
					object obj = a[i];
					if (obj != null)
					{
						if (type.IsInstanceOfType(obj))
						{
							this.<>2__current = obj;
							this.<>1__state = 2;
							return true;
						}
						goto IL_BD;
					}
				}
				IL_DD:
				a = null;
				return false;
			}

			// Token: 0x1700001F RID: 31
			// (get) Token: 0x060000B5 RID: 181 RVA: 0x00004E0A File Offset: 0x0000300A
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000B6 RID: 182 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000020 RID: 32
			// (get) Token: 0x060000B7 RID: 183 RVA: 0x00004E0A File Offset: 0x0000300A
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000B8 RID: 184 RVA: 0x00004E14 File Offset: 0x00003014
			[DebuggerHidden]
			IEnumerator<object> IEnumerable<object>.GetEnumerator()
			{
				XObject.<AnnotationsIterator>d__15 <AnnotationsIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<AnnotationsIterator>d__ = this;
				}
				else
				{
					<AnnotationsIterator>d__ = new XObject.<AnnotationsIterator>d__15(0);
					<AnnotationsIterator>d__.<>4__this = this;
				}
				<AnnotationsIterator>d__.type = type;
				return <AnnotationsIterator>d__;
			}

			// Token: 0x060000B9 RID: 185 RVA: 0x00004E63 File Offset: 0x00003063
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator();
			}

			// Token: 0x04000038 RID: 56
			private int <>1__state;

			// Token: 0x04000039 RID: 57
			private object <>2__current;

			// Token: 0x0400003A RID: 58
			private int <>l__initialThreadId;

			// Token: 0x0400003B RID: 59
			public XObject <>4__this;

			// Token: 0x0400003C RID: 60
			private Type type;

			// Token: 0x0400003D RID: 61
			public Type <>3__type;

			// Token: 0x0400003E RID: 62
			private object[] <a>5__1;

			// Token: 0x0400003F RID: 63
			private int <i>5__2;
		}

		// Token: 0x02000012 RID: 18
		[CompilerGenerated]
		private sealed class <Annotations>d__16<T> : IEnumerable<T>, IEnumerable, IEnumerator<T>, IDisposable, IEnumerator where T : class
		{
			// Token: 0x060000BA RID: 186 RVA: 0x00004E6B File Offset: 0x0000306B
			[DebuggerHidden]
			public <Annotations>d__16(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060000BB RID: 187 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060000BC RID: 188 RVA: 0x00004E88 File Offset: 0x00003088
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XObject xobject = this;
				switch (num)
				{
				case 0:
				{
					this.<>1__state = -1;
					if (xobject.annotations == null)
					{
						return false;
					}
					a = (xobject.annotations as object[]);
					if (a != null)
					{
						i = 0;
						goto IL_DC;
					}
					T t = xobject.annotations as T;
					if (t != null)
					{
						this.<>2__current = t;
						this.<>1__state = 1;
						return true;
					}
					goto IL_EC;
				}
				case 1:
					this.<>1__state = -1;
					goto IL_EC;
				case 2:
					this.<>1__state = -1;
					break;
				default:
					return false;
				}
				IL_CA:
				int num2 = i;
				i = num2 + 1;
				IL_DC:
				if (i < a.Length)
				{
					object obj = a[i];
					if (obj != null)
					{
						T t2 = obj as T;
						if (t2 != null)
						{
							this.<>2__current = t2;
							this.<>1__state = 2;
							return true;
						}
						goto IL_CA;
					}
				}
				IL_EC:
				a = null;
				return false;
			}

			// Token: 0x17000021 RID: 33
			// (get) Token: 0x060000BD RID: 189 RVA: 0x00004F89 File Offset: 0x00003189
			T IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000BE RID: 190 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000022 RID: 34
			// (get) Token: 0x060000BF RID: 191 RVA: 0x00004F91 File Offset: 0x00003191
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000C0 RID: 192 RVA: 0x00004FA0 File Offset: 0x000031A0
			[DebuggerHidden]
			IEnumerator<T> IEnumerable<!0>.GetEnumerator()
			{
				XObject.<Annotations>d__16<T> <Annotations>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<Annotations>d__ = this;
				}
				else
				{
					<Annotations>d__ = new XObject.<Annotations>d__16<T>(0);
					<Annotations>d__.<>4__this = this;
				}
				return <Annotations>d__;
			}

			// Token: 0x060000C1 RID: 193 RVA: 0x00004FE3 File Offset: 0x000031E3
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<T>.GetEnumerator();
			}

			// Token: 0x04000040 RID: 64
			private int <>1__state;

			// Token: 0x04000041 RID: 65
			private T <>2__current;

			// Token: 0x04000042 RID: 66
			private int <>l__initialThreadId;

			// Token: 0x04000043 RID: 67
			public XObject <>4__this;

			// Token: 0x04000044 RID: 68
			private object[] <a>5__1;

			// Token: 0x04000045 RID: 69
			private int <i>5__2;
		}
	}
}
