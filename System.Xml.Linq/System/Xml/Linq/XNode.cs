﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Xml.Linq
{
	/// <summary>Represents the abstract concept of a node (element, comment, document type, processing instruction, or text node) in the XML tree.  </summary>
	// Token: 0x02000019 RID: 25
	public abstract class XNode : XObject
	{
		// Token: 0x060000C9 RID: 201 RVA: 0x0000505F File Offset: 0x0000325F
		internal XNode()
		{
		}

		/// <summary>Gets the next sibling node of this node.</summary>
		/// <returns>The <see cref="T:System.Xml.Linq.XNode" /> that contains the next sibling node.</returns>
		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000CA RID: 202 RVA: 0x00005067 File Offset: 0x00003267
		public XNode NextNode
		{
			get
			{
				if (this.parent != null && this != this.parent.content)
				{
					return this.next;
				}
				return null;
			}
		}

		/// <summary>Gets the previous sibling node of this node.</summary>
		/// <returns>The <see cref="T:System.Xml.Linq.XNode" /> that contains the previous sibling node.</returns>
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000CB RID: 203 RVA: 0x00005088 File Offset: 0x00003288
		public XNode PreviousNode
		{
			get
			{
				if (this.parent == null)
				{
					return null;
				}
				XNode xnode = ((XNode)this.parent.content).next;
				XNode result = null;
				while (xnode != this)
				{
					result = xnode;
					xnode = xnode.next;
				}
				return result;
			}
		}

		/// <summary>Gets a comparer that can compare the relative position of two nodes.</summary>
		/// <returns>An <see cref="T:System.Xml.Linq.XNodeDocumentOrderComparer" /> that can compare the relative position of two nodes.</returns>
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000CC RID: 204 RVA: 0x000050C7 File Offset: 0x000032C7
		public static XNodeDocumentOrderComparer DocumentOrderComparer
		{
			get
			{
				if (XNode.documentOrderComparer == null)
				{
					XNode.documentOrderComparer = new XNodeDocumentOrderComparer();
				}
				return XNode.documentOrderComparer;
			}
		}

		/// <summary>Gets a comparer that can compare two nodes for value equality.</summary>
		/// <returns>A <see cref="T:System.Xml.Linq.XNodeEqualityComparer" /> that can compare two nodes for value equality.</returns>
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000CD RID: 205 RVA: 0x000050DF File Offset: 0x000032DF
		public static XNodeEqualityComparer EqualityComparer
		{
			get
			{
				if (XNode.equalityComparer == null)
				{
					XNode.equalityComparer = new XNodeEqualityComparer();
				}
				return XNode.equalityComparer;
			}
		}

		/// <summary>Adds the specified content immediately after this node.</summary>
		/// <param name="content">A content object that contains simple content or a collection of content objects to be added after this node.</param>
		/// <exception cref="T:System.InvalidOperationException">The parent is <see langword="null" />.</exception>
		// Token: 0x060000CE RID: 206 RVA: 0x000050F8 File Offset: 0x000032F8
		public void AddAfterSelf(object content)
		{
			if (this.parent == null)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_MissingParent"));
			}
			new Inserter(this.parent, this).Add(content);
		}

		/// <summary>Adds the specified content immediately after this node.</summary>
		/// <param name="content">A parameter list of content objects.</param>
		/// <exception cref="T:System.InvalidOperationException">The parent is <see langword="null" />.</exception>
		// Token: 0x060000CF RID: 207 RVA: 0x00005132 File Offset: 0x00003332
		public void AddAfterSelf(params object[] content)
		{
			this.AddAfterSelf(content);
		}

		/// <summary>Adds the specified content immediately before this node.</summary>
		/// <param name="content">A content object that contains simple content or a collection of content objects to be added before this node.</param>
		/// <exception cref="T:System.InvalidOperationException">The parent is <see langword="null" />.</exception>
		// Token: 0x060000D0 RID: 208 RVA: 0x0000513C File Offset: 0x0000333C
		public void AddBeforeSelf(object content)
		{
			if (this.parent == null)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_MissingParent"));
			}
			XNode xnode = (XNode)this.parent.content;
			while (xnode.next != this)
			{
				xnode = xnode.next;
			}
			if (xnode == this.parent.content)
			{
				xnode = null;
			}
			new Inserter(this.parent, xnode).Add(content);
		}

		/// <summary>Adds the specified content immediately before this node.</summary>
		/// <param name="content">A parameter list of content objects.</param>
		/// <exception cref="T:System.InvalidOperationException">The parent is <see langword="null" />.</exception>
		// Token: 0x060000D1 RID: 209 RVA: 0x000051A9 File Offset: 0x000033A9
		public void AddBeforeSelf(params object[] content)
		{
			this.AddBeforeSelf(content);
		}

		/// <summary>Returns a collection of the ancestor elements of this node.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> of the ancestor elements of this node.</returns>
		// Token: 0x060000D2 RID: 210 RVA: 0x000051B2 File Offset: 0x000033B2
		public IEnumerable<XElement> Ancestors()
		{
			return this.GetAncestors(null, false);
		}

		/// <summary>Returns a filtered collection of the ancestor elements of this node. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> of the ancestor elements of this node. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.The nodes in the returned collection are in reverse document order.This method uses deferred execution.</returns>
		// Token: 0x060000D3 RID: 211 RVA: 0x000051BC File Offset: 0x000033BC
		public IEnumerable<XElement> Ancestors(XName name)
		{
			if (!(name != null))
			{
				return XElement.EmptySequence;
			}
			return this.GetAncestors(name, false);
		}

		/// <summary>Compares two nodes to determine their relative XML document order.</summary>
		/// <param name="n1">First <see cref="T:System.Xml.Linq.XNode" /> to compare.</param>
		/// <param name="n2">Second <see cref="T:System.Xml.Linq.XNode" /> to compare.</param>
		/// <returns>An <see langword="int" /> containing 0 if the nodes are equal; -1 if <paramref name="n1" /> is before <paramref name="n2" />; 1 if <paramref name="n1" /> is after <paramref name="n2" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The two nodes do not share a common ancestor.</exception>
		// Token: 0x060000D4 RID: 212 RVA: 0x000051D8 File Offset: 0x000033D8
		public static int CompareDocumentOrder(XNode n1, XNode n2)
		{
			if (n1 == n2)
			{
				return 0;
			}
			if (n1 == null)
			{
				return -1;
			}
			if (n2 == null)
			{
				return 1;
			}
			if (n1.parent != n2.parent)
			{
				int num = 0;
				XNode xnode = n1;
				while (xnode.parent != null)
				{
					xnode = xnode.parent;
					num++;
				}
				XNode xnode2 = n2;
				while (xnode2.parent != null)
				{
					xnode2 = xnode2.parent;
					num--;
				}
				if (xnode != xnode2)
				{
					throw new InvalidOperationException(Res.GetString("InvalidOperation_MissingAncestor"));
				}
				if (num < 0)
				{
					do
					{
						n2 = n2.parent;
						num++;
					}
					while (num != 0);
					if (n1 == n2)
					{
						return -1;
					}
				}
				else if (num > 0)
				{
					do
					{
						n1 = n1.parent;
						num--;
					}
					while (num != 0);
					if (n1 == n2)
					{
						return 1;
					}
				}
				while (n1.parent != n2.parent)
				{
					n1 = n1.parent;
					n2 = n2.parent;
				}
			}
			else if (n1.parent == null)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_MissingAncestor"));
			}
			XNode xnode3 = (XNode)n1.parent.content;
			for (;;)
			{
				xnode3 = xnode3.next;
				if (xnode3 == n1)
				{
					break;
				}
				if (xnode3 == n2)
				{
					return 1;
				}
			}
			return -1;
		}

		/// <summary>Creates an <see cref="T:System.Xml.XmlReader" /> for this node.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> that can be used to read this node and its descendants.</returns>
		// Token: 0x060000D5 RID: 213 RVA: 0x000052D7 File Offset: 0x000034D7
		public XmlReader CreateReader()
		{
			return new XNodeReader(this, null);
		}

		/// <summary>Creates an <see cref="T:System.Xml.XmlReader" /> with the options specified by the <paramref name="readerOptions" /> parameter.</summary>
		/// <param name="readerOptions">A <see cref="T:System.Xml.Linq.ReaderOptions" /> object that specifies whether to omit duplicate namespaces.</param>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object.</returns>
		// Token: 0x060000D6 RID: 214 RVA: 0x000052E0 File Offset: 0x000034E0
		public XmlReader CreateReader(ReaderOptions readerOptions)
		{
			return new XNodeReader(this, null, readerOptions);
		}

		/// <summary>Returns a collection of the sibling nodes after this node, in document order.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> of the sibling nodes after this node, in document order.</returns>
		// Token: 0x060000D7 RID: 215 RVA: 0x000052EA File Offset: 0x000034EA
		public IEnumerable<XNode> NodesAfterSelf()
		{
			XNode i = this;
			while (i.parent != null && i != i.parent.content)
			{
				i = i.next;
				yield return i;
			}
			yield break;
		}

		/// <summary>Returns a collection of the sibling nodes before this node, in document order.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> of the sibling nodes before this node, in document order.</returns>
		// Token: 0x060000D8 RID: 216 RVA: 0x000052FA File Offset: 0x000034FA
		public IEnumerable<XNode> NodesBeforeSelf()
		{
			if (this.parent != null)
			{
				XNode i = (XNode)this.parent.content;
				do
				{
					i = i.next;
					if (i == this)
					{
						break;
					}
					yield return i;
				}
				while (this.parent != null && this.parent == i.parent);
				i = null;
			}
			yield break;
		}

		/// <summary>Returns a collection of the sibling elements after this node, in document order.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> of the sibling elements after this node, in document order.</returns>
		// Token: 0x060000D9 RID: 217 RVA: 0x0000530A File Offset: 0x0000350A
		public IEnumerable<XElement> ElementsAfterSelf()
		{
			return this.GetElementsAfterSelf(null);
		}

		/// <summary>Returns a filtered collection of the sibling elements after this node, in document order. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> of the sibling elements after this node, in document order. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</returns>
		// Token: 0x060000DA RID: 218 RVA: 0x00005313 File Offset: 0x00003513
		public IEnumerable<XElement> ElementsAfterSelf(XName name)
		{
			if (!(name != null))
			{
				return XElement.EmptySequence;
			}
			return this.GetElementsAfterSelf(name);
		}

		/// <summary>Returns a collection of the sibling elements before this node, in document order.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> of the sibling elements before this node, in document order.</returns>
		// Token: 0x060000DB RID: 219 RVA: 0x0000532B File Offset: 0x0000352B
		public IEnumerable<XElement> ElementsBeforeSelf()
		{
			return this.GetElementsBeforeSelf(null);
		}

		/// <summary>Returns a filtered collection of the sibling elements before this node, in document order. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> of the sibling elements before this node, in document order. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</returns>
		// Token: 0x060000DC RID: 220 RVA: 0x00005334 File Offset: 0x00003534
		public IEnumerable<XElement> ElementsBeforeSelf(XName name)
		{
			if (!(name != null))
			{
				return XElement.EmptySequence;
			}
			return this.GetElementsBeforeSelf(name);
		}

		/// <summary>Determines if the current node appears after a specified node in terms of document order.</summary>
		/// <param name="node">The <see cref="T:System.Xml.Linq.XNode" /> to compare for document order.</param>
		/// <returns>
		///     <see langword="true" /> if this node appears after the specified node; otherwise <see langword="false" />.</returns>
		// Token: 0x060000DD RID: 221 RVA: 0x0000534C File Offset: 0x0000354C
		public bool IsAfter(XNode node)
		{
			return XNode.CompareDocumentOrder(this, node) > 0;
		}

		/// <summary>Determines if the current node appears before a specified node in terms of document order.</summary>
		/// <param name="node">The <see cref="T:System.Xml.Linq.XNode" /> to compare for document order.</param>
		/// <returns>
		///     <see langword="true" /> if this node appears before the specified node; otherwise <see langword="false" />.</returns>
		// Token: 0x060000DE RID: 222 RVA: 0x00005358 File Offset: 0x00003558
		public bool IsBefore(XNode node)
		{
			return XNode.CompareDocumentOrder(this, node) < 0;
		}

		/// <summary>Creates an <see cref="T:System.Xml.Linq.XNode" /> from an <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="reader">An <see cref="T:System.Xml.XmlReader" /> positioned at the node to read into this <see cref="T:System.Xml.Linq.XNode" />.</param>
		/// <returns>An <see cref="T:System.Xml.Linq.XNode" /> that contains the node and its descendant nodes that were read from the reader. The runtime type of the node is determined by the node type (<see cref="P:System.Xml.Linq.XObject.NodeType" />) of the first node encountered in the reader.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on a recognized node type.</exception>
		/// <exception cref="T:System.Xml.XmlException">The underlying <see cref="T:System.Xml.XmlReader" /> throws an exception.</exception>
		// Token: 0x060000DF RID: 223 RVA: 0x00005364 File Offset: 0x00003564
		public static XNode ReadFrom(XmlReader reader)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			if (reader.ReadState != ReadState.Interactive)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_ExpectedInteractive"));
			}
			switch (reader.NodeType)
			{
			case XmlNodeType.Element:
				return new XElement(reader);
			case XmlNodeType.Text:
			case XmlNodeType.Whitespace:
			case XmlNodeType.SignificantWhitespace:
				return new XText(reader);
			case XmlNodeType.CDATA:
				return new XCData(reader);
			case XmlNodeType.ProcessingInstruction:
				return new XProcessingInstruction(reader);
			case XmlNodeType.Comment:
				return new XComment(reader);
			case XmlNodeType.DocumentType:
				return new XDocumentType(reader);
			}
			throw new InvalidOperationException(Res.GetString("InvalidOperation_UnexpectedNodeType", new object[]
			{
				reader.NodeType
			}));
		}

		/// <summary>Removes this node from its parent.</summary>
		/// <exception cref="T:System.InvalidOperationException">The parent is <see langword="null" />.</exception>
		// Token: 0x060000E0 RID: 224 RVA: 0x0000542E File Offset: 0x0000362E
		public void Remove()
		{
			if (this.parent == null)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_MissingParent"));
			}
			this.parent.RemoveNode(this);
		}

		/// <summary>Replaces this node with the specified content.</summary>
		/// <param name="content">Content that replaces this node.</param>
		// Token: 0x060000E1 RID: 225 RVA: 0x00005454 File Offset: 0x00003654
		public void ReplaceWith(object content)
		{
			if (this.parent == null)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_MissingParent"));
			}
			XContainer parent = this.parent;
			XNode xnode = (XNode)this.parent.content;
			while (xnode.next != this)
			{
				xnode = xnode.next;
			}
			if (xnode == this.parent.content)
			{
				xnode = null;
			}
			this.parent.RemoveNode(this);
			if (xnode != null && xnode.parent != parent)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_ExternalCode"));
			}
			new Inserter(parent, xnode).Add(content);
		}

		/// <summary>Replaces this node with the specified content.</summary>
		/// <param name="content">A parameter list of the new content.</param>
		// Token: 0x060000E2 RID: 226 RVA: 0x000054EB File Offset: 0x000036EB
		public void ReplaceWith(params object[] content)
		{
			this.ReplaceWith(content);
		}

		/// <summary>Returns the indented XML for this node.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the indented XML.</returns>
		// Token: 0x060000E3 RID: 227 RVA: 0x000054F4 File Offset: 0x000036F4
		public override string ToString()
		{
			return this.GetXmlString(base.GetSaveOptionsFromAnnotations());
		}

		/// <summary>Returns the XML for this node, optionally disabling formatting.</summary>
		/// <param name="options">A <see cref="T:System.Xml.Linq.SaveOptions" /> that specifies formatting behavior.</param>
		/// <returns>A <see cref="T:System.String" /> containing the XML.</returns>
		// Token: 0x060000E4 RID: 228 RVA: 0x00005502 File Offset: 0x00003702
		public string ToString(SaveOptions options)
		{
			return this.GetXmlString(options);
		}

		/// <summary>Compares the values of two nodes, including the values of all descendant nodes.</summary>
		/// <param name="n1">The first <see cref="T:System.Xml.Linq.XNode" /> to compare.</param>
		/// <param name="n2">The second <see cref="T:System.Xml.Linq.XNode" /> to compare.</param>
		/// <returns>
		///     <see langword="true" /> if the nodes are equal; otherwise <see langword="false" />.</returns>
		// Token: 0x060000E5 RID: 229 RVA: 0x0000550B File Offset: 0x0000370B
		public static bool DeepEquals(XNode n1, XNode n2)
		{
			return n1 == n2 || (n1 != null && n2 != null && n1.DeepEquals(n2));
		}

		/// <summary>Writes this node to an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> into which this method will write.</param>
		// Token: 0x060000E6 RID: 230
		public abstract void WriteTo(XmlWriter writer);

		// Token: 0x060000E7 RID: 231 RVA: 0x00004D15 File Offset: 0x00002F15
		internal virtual void AppendText(StringBuilder sb)
		{
		}

		// Token: 0x060000E8 RID: 232
		internal abstract XNode CloneNode();

		// Token: 0x060000E9 RID: 233
		internal abstract bool DeepEquals(XNode node);

		// Token: 0x060000EA RID: 234 RVA: 0x00005522 File Offset: 0x00003722
		internal IEnumerable<XElement> GetAncestors(XName name, bool self)
		{
			for (XElement e = (self ? this : this.parent) as XElement; e != null; e = (e.parent as XElement))
			{
				if (name == null || e.name == name)
				{
					yield return e;
				}
			}
			yield break;
		}

		// Token: 0x060000EB RID: 235 RVA: 0x00005540 File Offset: 0x00003740
		private IEnumerable<XElement> GetElementsAfterSelf(XName name)
		{
			XNode i = this;
			while (i.parent != null && i != i.parent.content)
			{
				i = i.next;
				XElement xelement = i as XElement;
				if (xelement != null && (name == null || xelement.name == name))
				{
					yield return xelement;
				}
			}
			yield break;
		}

		// Token: 0x060000EC RID: 236 RVA: 0x00005557 File Offset: 0x00003757
		private IEnumerable<XElement> GetElementsBeforeSelf(XName name)
		{
			if (this.parent != null)
			{
				XNode i = (XNode)this.parent.content;
				do
				{
					i = i.next;
					if (i == this)
					{
						break;
					}
					XElement xelement = i as XElement;
					if (xelement != null && (name == null || xelement.name == name))
					{
						yield return xelement;
					}
				}
				while (this.parent != null && this.parent == i.parent);
				i = null;
			}
			yield break;
		}

		// Token: 0x060000ED RID: 237
		internal abstract int GetDeepHashCode();

		// Token: 0x060000EE RID: 238 RVA: 0x00005570 File Offset: 0x00003770
		internal static XmlReaderSettings GetXmlReaderSettings(LoadOptions o)
		{
			XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
			if ((o & LoadOptions.PreserveWhitespace) == LoadOptions.None)
			{
				xmlReaderSettings.IgnoreWhitespace = true;
			}
			xmlReaderSettings.DtdProcessing = DtdProcessing.Parse;
			xmlReaderSettings.MaxCharactersFromEntities = 10000000L;
			xmlReaderSettings.XmlResolver = null;
			return xmlReaderSettings;
		}

		// Token: 0x060000EF RID: 239 RVA: 0x000055AC File Offset: 0x000037AC
		internal static XmlWriterSettings GetXmlWriterSettings(SaveOptions o)
		{
			XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
			if ((o & SaveOptions.DisableFormatting) == SaveOptions.None)
			{
				xmlWriterSettings.Indent = true;
			}
			if ((o & SaveOptions.OmitDuplicateNamespaces) != SaveOptions.None)
			{
				xmlWriterSettings.NamespaceHandling |= NamespaceHandling.OmitDuplicates;
			}
			return xmlWriterSettings;
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x000055E0 File Offset: 0x000037E0
		private string GetXmlString(SaveOptions o)
		{
			string result;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
				xmlWriterSettings.OmitXmlDeclaration = true;
				if ((o & SaveOptions.DisableFormatting) == SaveOptions.None)
				{
					xmlWriterSettings.Indent = true;
				}
				if ((o & SaveOptions.OmitDuplicateNamespaces) != SaveOptions.None)
				{
					xmlWriterSettings.NamespaceHandling |= NamespaceHandling.OmitDuplicates;
				}
				if (this is XText)
				{
					xmlWriterSettings.ConformanceLevel = ConformanceLevel.Fragment;
				}
				using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, xmlWriterSettings))
				{
					XDocument xdocument = this as XDocument;
					if (xdocument != null)
					{
						xdocument.WriteContentTo(xmlWriter);
					}
					else
					{
						this.WriteTo(xmlWriter);
					}
				}
				result = stringWriter.ToString();
			}
			return result;
		}

		// Token: 0x04000055 RID: 85
		private static XNodeDocumentOrderComparer documentOrderComparer;

		// Token: 0x04000056 RID: 86
		private static XNodeEqualityComparer equalityComparer;

		// Token: 0x04000057 RID: 87
		internal XNode next;

		// Token: 0x0200001A RID: 26
		[CompilerGenerated]
		private sealed class <NodesAfterSelf>d__21 : IEnumerable<XNode>, IEnumerable, IEnumerator<XNode>, IDisposable, IEnumerator
		{
			// Token: 0x060000F1 RID: 241 RVA: 0x00005694 File Offset: 0x00003894
			[DebuggerHidden]
			public <NodesAfterSelf>d__21(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060000F2 RID: 242 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060000F3 RID: 243 RVA: 0x000056B0 File Offset: 0x000038B0
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XNode xnode = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
				}
				else
				{
					this.<>1__state = -1;
					i = xnode;
				}
				if (i.parent == null || i == i.parent.content)
				{
					return false;
				}
				i = i.next;
				this.<>2__current = i;
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000028 RID: 40
			// (get) Token: 0x060000F4 RID: 244 RVA: 0x00005737 File Offset: 0x00003937
			XNode IEnumerator<XNode>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000F5 RID: 245 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000029 RID: 41
			// (get) Token: 0x060000F6 RID: 246 RVA: 0x00005737 File Offset: 0x00003937
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000F7 RID: 247 RVA: 0x00005740 File Offset: 0x00003940
			[DebuggerHidden]
			IEnumerator<XNode> IEnumerable<XNode>.GetEnumerator()
			{
				XNode.<NodesAfterSelf>d__21 <NodesAfterSelf>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<NodesAfterSelf>d__ = this;
				}
				else
				{
					<NodesAfterSelf>d__ = new XNode.<NodesAfterSelf>d__21(0);
					<NodesAfterSelf>d__.<>4__this = this;
				}
				return <NodesAfterSelf>d__;
			}

			// Token: 0x060000F8 RID: 248 RVA: 0x00005783 File Offset: 0x00003983
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XNode>.GetEnumerator();
			}

			// Token: 0x04000058 RID: 88
			private int <>1__state;

			// Token: 0x04000059 RID: 89
			private XNode <>2__current;

			// Token: 0x0400005A RID: 90
			private int <>l__initialThreadId;

			// Token: 0x0400005B RID: 91
			public XNode <>4__this;

			// Token: 0x0400005C RID: 92
			private XNode <n>5__1;
		}

		// Token: 0x0200001B RID: 27
		[CompilerGenerated]
		private sealed class <NodesBeforeSelf>d__22 : IEnumerable<XNode>, IEnumerable, IEnumerator<XNode>, IDisposable, IEnumerator
		{
			// Token: 0x060000F9 RID: 249 RVA: 0x0000578B File Offset: 0x0000398B
			[DebuggerHidden]
			public <NodesBeforeSelf>d__22(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060000FA RID: 250 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060000FB RID: 251 RVA: 0x000057A8 File Offset: 0x000039A8
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XNode xnode = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					if (xnode.parent == null || xnode.parent != i.parent)
					{
						goto IL_8D;
					}
				}
				else
				{
					this.<>1__state = -1;
					if (xnode.parent == null)
					{
						return false;
					}
					i = (XNode)xnode.parent.content;
				}
				i = i.next;
				if (i != xnode)
				{
					this.<>2__current = i;
					this.<>1__state = 1;
					return true;
				}
				IL_8D:
				i = null;
				return false;
			}

			// Token: 0x1700002A RID: 42
			// (get) Token: 0x060000FC RID: 252 RVA: 0x0000584A File Offset: 0x00003A4A
			XNode IEnumerator<XNode>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000FD RID: 253 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700002B RID: 43
			// (get) Token: 0x060000FE RID: 254 RVA: 0x0000584A File Offset: 0x00003A4A
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000FF RID: 255 RVA: 0x00005854 File Offset: 0x00003A54
			[DebuggerHidden]
			IEnumerator<XNode> IEnumerable<XNode>.GetEnumerator()
			{
				XNode.<NodesBeforeSelf>d__22 <NodesBeforeSelf>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<NodesBeforeSelf>d__ = this;
				}
				else
				{
					<NodesBeforeSelf>d__ = new XNode.<NodesBeforeSelf>d__22(0);
					<NodesBeforeSelf>d__.<>4__this = this;
				}
				return <NodesBeforeSelf>d__;
			}

			// Token: 0x06000100 RID: 256 RVA: 0x00005897 File Offset: 0x00003A97
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XNode>.GetEnumerator();
			}

			// Token: 0x0400005D RID: 93
			private int <>1__state;

			// Token: 0x0400005E RID: 94
			private XNode <>2__current;

			// Token: 0x0400005F RID: 95
			private int <>l__initialThreadId;

			// Token: 0x04000060 RID: 96
			public XNode <>4__this;

			// Token: 0x04000061 RID: 97
			private XNode <n>5__1;
		}

		// Token: 0x0200001C RID: 28
		[CompilerGenerated]
		private sealed class <GetAncestors>d__40 : IEnumerable<XElement>, IEnumerable, IEnumerator<XElement>, IDisposable, IEnumerator
		{
			// Token: 0x06000101 RID: 257 RVA: 0x0000589F File Offset: 0x00003A9F
			[DebuggerHidden]
			public <GetAncestors>d__40(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000102 RID: 258 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000103 RID: 259 RVA: 0x000058BC File Offset: 0x00003ABC
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XNode xnode = this;
				if (num == 0)
				{
					this.<>1__state = -1;
					e = ((self ? xnode : xnode.parent) as XElement);
					goto IL_94;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				IL_7E:
				e = (e.parent as XElement);
				IL_94:
				if (e == null)
				{
					return false;
				}
				if (name == null || e.name == name)
				{
					this.<>2__current = e;
					this.<>1__state = 1;
					return true;
				}
				goto IL_7E;
			}

			// Token: 0x1700002C RID: 44
			// (get) Token: 0x06000104 RID: 260 RVA: 0x00005966 File Offset: 0x00003B66
			XElement IEnumerator<XElement>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000105 RID: 261 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700002D RID: 45
			// (get) Token: 0x06000106 RID: 262 RVA: 0x00005966 File Offset: 0x00003B66
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000107 RID: 263 RVA: 0x00005970 File Offset: 0x00003B70
			[DebuggerHidden]
			IEnumerator<XElement> IEnumerable<XElement>.GetEnumerator()
			{
				XNode.<GetAncestors>d__40 <GetAncestors>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetAncestors>d__ = this;
				}
				else
				{
					<GetAncestors>d__ = new XNode.<GetAncestors>d__40(0);
					<GetAncestors>d__.<>4__this = this;
				}
				<GetAncestors>d__.name = name;
				<GetAncestors>d__.self = self;
				return <GetAncestors>d__;
			}

			// Token: 0x06000108 RID: 264 RVA: 0x000059CB File Offset: 0x00003BCB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator();
			}

			// Token: 0x04000062 RID: 98
			private int <>1__state;

			// Token: 0x04000063 RID: 99
			private XElement <>2__current;

			// Token: 0x04000064 RID: 100
			private int <>l__initialThreadId;

			// Token: 0x04000065 RID: 101
			private bool self;

			// Token: 0x04000066 RID: 102
			public bool <>3__self;

			// Token: 0x04000067 RID: 103
			public XNode <>4__this;

			// Token: 0x04000068 RID: 104
			private XName name;

			// Token: 0x04000069 RID: 105
			public XName <>3__name;

			// Token: 0x0400006A RID: 106
			private XElement <e>5__1;
		}

		// Token: 0x0200001D RID: 29
		[CompilerGenerated]
		private sealed class <GetElementsAfterSelf>d__41 : IEnumerable<XElement>, IEnumerable, IEnumerator<XElement>, IDisposable, IEnumerator
		{
			// Token: 0x06000109 RID: 265 RVA: 0x000059D3 File Offset: 0x00003BD3
			[DebuggerHidden]
			public <GetElementsAfterSelf>d__41(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600010A RID: 266 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x0600010B RID: 267 RVA: 0x000059F0 File Offset: 0x00003BF0
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XNode xnode = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
				}
				else
				{
					this.<>1__state = -1;
					i = xnode;
				}
				while (i.parent != null && i != i.parent.content)
				{
					i = i.next;
					XElement xelement = i as XElement;
					if (xelement != null && (name == null || xelement.name == name))
					{
						this.<>2__current = xelement;
						this.<>1__state = 1;
						return true;
					}
				}
				return false;
			}

			// Token: 0x1700002E RID: 46
			// (get) Token: 0x0600010C RID: 268 RVA: 0x00005AA2 File Offset: 0x00003CA2
			XElement IEnumerator<XElement>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600010D RID: 269 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700002F RID: 47
			// (get) Token: 0x0600010E RID: 270 RVA: 0x00005AA2 File Offset: 0x00003CA2
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600010F RID: 271 RVA: 0x00005AAC File Offset: 0x00003CAC
			[DebuggerHidden]
			IEnumerator<XElement> IEnumerable<XElement>.GetEnumerator()
			{
				XNode.<GetElementsAfterSelf>d__41 <GetElementsAfterSelf>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetElementsAfterSelf>d__ = this;
				}
				else
				{
					<GetElementsAfterSelf>d__ = new XNode.<GetElementsAfterSelf>d__41(0);
					<GetElementsAfterSelf>d__.<>4__this = this;
				}
				<GetElementsAfterSelf>d__.name = name;
				return <GetElementsAfterSelf>d__;
			}

			// Token: 0x06000110 RID: 272 RVA: 0x00005AFB File Offset: 0x00003CFB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator();
			}

			// Token: 0x0400006B RID: 107
			private int <>1__state;

			// Token: 0x0400006C RID: 108
			private XElement <>2__current;

			// Token: 0x0400006D RID: 109
			private int <>l__initialThreadId;

			// Token: 0x0400006E RID: 110
			public XNode <>4__this;

			// Token: 0x0400006F RID: 111
			private XNode <n>5__1;

			// Token: 0x04000070 RID: 112
			private XName name;

			// Token: 0x04000071 RID: 113
			public XName <>3__name;
		}

		// Token: 0x0200001E RID: 30
		[CompilerGenerated]
		private sealed class <GetElementsBeforeSelf>d__42 : IEnumerable<XElement>, IEnumerable, IEnumerator<XElement>, IDisposable, IEnumerator
		{
			// Token: 0x06000111 RID: 273 RVA: 0x00005B03 File Offset: 0x00003D03
			[DebuggerHidden]
			public <GetElementsBeforeSelf>d__42(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000112 RID: 274 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000113 RID: 275 RVA: 0x00005B20 File Offset: 0x00003D20
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XNode xnode = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					goto IL_A3;
				}
				else
				{
					this.<>1__state = -1;
					if (xnode.parent == null)
					{
						return false;
					}
					i = (XNode)xnode.parent.content;
				}
				IL_42:
				i = i.next;
				if (i == xnode)
				{
					goto IL_BE;
				}
				XElement xelement = i as XElement;
				if (xelement != null && (name == null || xelement.name == name))
				{
					this.<>2__current = xelement;
					this.<>1__state = 1;
					return true;
				}
				IL_A3:
				if (xnode.parent != null && xnode.parent == i.parent)
				{
					goto IL_42;
				}
				IL_BE:
				i = null;
				return false;
			}

			// Token: 0x17000030 RID: 48
			// (get) Token: 0x06000114 RID: 276 RVA: 0x00005BF3 File Offset: 0x00003DF3
			XElement IEnumerator<XElement>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000115 RID: 277 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000031 RID: 49
			// (get) Token: 0x06000116 RID: 278 RVA: 0x00005BF3 File Offset: 0x00003DF3
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000117 RID: 279 RVA: 0x00005BFC File Offset: 0x00003DFC
			[DebuggerHidden]
			IEnumerator<XElement> IEnumerable<XElement>.GetEnumerator()
			{
				XNode.<GetElementsBeforeSelf>d__42 <GetElementsBeforeSelf>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetElementsBeforeSelf>d__ = this;
				}
				else
				{
					<GetElementsBeforeSelf>d__ = new XNode.<GetElementsBeforeSelf>d__42(0);
					<GetElementsBeforeSelf>d__.<>4__this = this;
				}
				<GetElementsBeforeSelf>d__.name = name;
				return <GetElementsBeforeSelf>d__;
			}

			// Token: 0x06000118 RID: 280 RVA: 0x00005C4B File Offset: 0x00003E4B
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator();
			}

			// Token: 0x04000072 RID: 114
			private int <>1__state;

			// Token: 0x04000073 RID: 115
			private XElement <>2__current;

			// Token: 0x04000074 RID: 116
			private int <>l__initialThreadId;

			// Token: 0x04000075 RID: 117
			public XNode <>4__this;

			// Token: 0x04000076 RID: 118
			private XNode <n>5__1;

			// Token: 0x04000077 RID: 119
			private XName name;

			// Token: 0x04000078 RID: 120
			public XName <>3__name;
		}
	}
}
