﻿using System;

namespace System.Xml.Linq
{
	/// <summary>Specifies whether to omit duplicate namespaces when loading an <see cref="T:System.Xml.Linq.XDocument" /> with an <see cref="T:System.Xml.XmlReader" />.</summary>
	// Token: 0x02000031 RID: 49
	[Flags]
	public enum ReaderOptions
	{
		/// <summary>No reader options specified.</summary>
		// Token: 0x040000BA RID: 186
		None = 0,
		/// <summary>Omit duplicate namespaces when loading the <see cref="T:System.Xml.Linq.XDocument" />.</summary>
		// Token: 0x040000BB RID: 187
		OmitDuplicateNamespaces = 1
	}
}
