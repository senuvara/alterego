﻿using System;
using System.Collections.Generic;

namespace System.Xml.Linq
{
	// Token: 0x02000042 RID: 66
	internal class XNodeBuilder : XmlWriter
	{
		// Token: 0x060002F0 RID: 752 RVA: 0x0000C0CF File Offset: 0x0000A2CF
		public XNodeBuilder(XContainer container)
		{
			this.root = container;
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060002F1 RID: 753 RVA: 0x0000C0DE File Offset: 0x0000A2DE
		public override XmlWriterSettings Settings
		{
			get
			{
				return new XmlWriterSettings
				{
					ConformanceLevel = ConformanceLevel.Auto
				};
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060002F2 RID: 754 RVA: 0x00003C5D File Offset: 0x00001E5D
		public override WriteState WriteState
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x060002F3 RID: 755 RVA: 0x0000C0EC File Offset: 0x0000A2EC
		public override void Close()
		{
			this.root.Add(this.content);
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x00004D15 File Offset: 0x00002F15
		public override void Flush()
		{
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x00003C5D File Offset: 0x00001E5D
		public override string LookupPrefix(string namespaceName)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060002F6 RID: 758 RVA: 0x0000C0FF File Offset: 0x0000A2FF
		public override void WriteBase64(byte[] buffer, int index, int count)
		{
			throw new NotSupportedException(Res.GetString("NotSupported_WriteBase64"));
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x0000C110 File Offset: 0x0000A310
		public override void WriteCData(string text)
		{
			this.AddNode(new XCData(text));
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x0000C11E File Offset: 0x0000A31E
		public override void WriteCharEntity(char ch)
		{
			this.AddString(new string(ch, 1));
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x0000C12D File Offset: 0x0000A32D
		public override void WriteChars(char[] buffer, int index, int count)
		{
			this.AddString(new string(buffer, index, count));
		}

		// Token: 0x060002FA RID: 762 RVA: 0x0000C13D File Offset: 0x0000A33D
		public override void WriteComment(string text)
		{
			this.AddNode(new XComment(text));
		}

		// Token: 0x060002FB RID: 763 RVA: 0x0000C14B File Offset: 0x0000A34B
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			this.AddNode(new XDocumentType(name, pubid, sysid, subset));
		}

		// Token: 0x060002FC RID: 764 RVA: 0x0000C160 File Offset: 0x0000A360
		public override void WriteEndAttribute()
		{
			XAttribute o = new XAttribute(this.attrName, this.attrValue);
			this.attrName = null;
			this.attrValue = null;
			if (this.parent != null)
			{
				this.parent.Add(o);
				return;
			}
			this.Add(o);
		}

		// Token: 0x060002FD RID: 765 RVA: 0x00004D15 File Offset: 0x00002F15
		public override void WriteEndDocument()
		{
		}

		// Token: 0x060002FE RID: 766 RVA: 0x0000C1A9 File Offset: 0x0000A3A9
		public override void WriteEndElement()
		{
			this.parent = ((XElement)this.parent).parent;
		}

		// Token: 0x060002FF RID: 767 RVA: 0x0000C1C4 File Offset: 0x0000A3C4
		public override void WriteEntityRef(string name)
		{
			if (name == "amp")
			{
				this.AddString("&");
				return;
			}
			if (name == "apos")
			{
				this.AddString("'");
				return;
			}
			if (name == "gt")
			{
				this.AddString(">");
				return;
			}
			if (name == "lt")
			{
				this.AddString("<");
				return;
			}
			if (!(name == "quot"))
			{
				throw new NotSupportedException(Res.GetString("NotSupported_WriteEntityRef"));
			}
			this.AddString("\"");
		}

		// Token: 0x06000300 RID: 768 RVA: 0x0000C260 File Offset: 0x0000A460
		public override void WriteFullEndElement()
		{
			XElement xelement = (XElement)this.parent;
			if (xelement.IsEmpty)
			{
				xelement.Add(string.Empty);
			}
			this.parent = xelement.parent;
		}

		// Token: 0x06000301 RID: 769 RVA: 0x0000C298 File Offset: 0x0000A498
		public override void WriteProcessingInstruction(string name, string text)
		{
			if (name == "xml")
			{
				return;
			}
			this.AddNode(new XProcessingInstruction(name, text));
		}

		// Token: 0x06000302 RID: 770 RVA: 0x0000C12D File Offset: 0x0000A32D
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			this.AddString(new string(buffer, index, count));
		}

		// Token: 0x06000303 RID: 771 RVA: 0x0000C2B5 File Offset: 0x0000A4B5
		public override void WriteRaw(string data)
		{
			this.AddString(data);
		}

		// Token: 0x06000304 RID: 772 RVA: 0x0000C2BE File Offset: 0x0000A4BE
		public override void WriteStartAttribute(string prefix, string localName, string namespaceName)
		{
			if (prefix == null)
			{
				throw new ArgumentNullException("prefix");
			}
			this.attrName = XNamespace.Get((prefix.Length == 0) ? string.Empty : namespaceName).GetName(localName);
			this.attrValue = string.Empty;
		}

		// Token: 0x06000305 RID: 773 RVA: 0x00004D15 File Offset: 0x00002F15
		public override void WriteStartDocument()
		{
		}

		// Token: 0x06000306 RID: 774 RVA: 0x00004D15 File Offset: 0x00002F15
		public override void WriteStartDocument(bool standalone)
		{
		}

		// Token: 0x06000307 RID: 775 RVA: 0x0000C2FA File Offset: 0x0000A4FA
		public override void WriteStartElement(string prefix, string localName, string namespaceName)
		{
			this.AddNode(new XElement(XNamespace.Get(namespaceName).GetName(localName)));
		}

		// Token: 0x06000308 RID: 776 RVA: 0x0000C2B5 File Offset: 0x0000A4B5
		public override void WriteString(string text)
		{
			this.AddString(text);
		}

		// Token: 0x06000309 RID: 777 RVA: 0x0000C313 File Offset: 0x0000A513
		public override void WriteSurrogateCharEntity(char lowCh, char highCh)
		{
			this.AddString(new string(new char[]
			{
				highCh,
				lowCh
			}));
		}

		// Token: 0x0600030A RID: 778 RVA: 0x0000C32E File Offset: 0x0000A52E
		public override void WriteValue(DateTimeOffset value)
		{
			this.WriteString(XmlConvert.ToString(value));
		}

		// Token: 0x0600030B RID: 779 RVA: 0x0000C2B5 File Offset: 0x0000A4B5
		public override void WriteWhitespace(string ws)
		{
			this.AddString(ws);
		}

		// Token: 0x0600030C RID: 780 RVA: 0x0000C33C File Offset: 0x0000A53C
		private void Add(object o)
		{
			if (this.content == null)
			{
				this.content = new List<object>();
			}
			this.content.Add(o);
		}

		// Token: 0x0600030D RID: 781 RVA: 0x0000C360 File Offset: 0x0000A560
		private void AddNode(XNode n)
		{
			if (this.parent != null)
			{
				this.parent.Add(n);
			}
			else
			{
				this.Add(n);
			}
			XContainer xcontainer = n as XContainer;
			if (xcontainer != null)
			{
				this.parent = xcontainer;
			}
		}

		// Token: 0x0600030E RID: 782 RVA: 0x0000C39B File Offset: 0x0000A59B
		private void AddString(string s)
		{
			if (s == null)
			{
				return;
			}
			if (this.attrValue != null)
			{
				this.attrValue += s;
				return;
			}
			if (this.parent != null)
			{
				this.parent.Add(s);
				return;
			}
			this.Add(s);
		}

		// Token: 0x04000112 RID: 274
		private List<object> content;

		// Token: 0x04000113 RID: 275
		private XContainer parent;

		// Token: 0x04000114 RID: 276
		private XName attrName;

		// Token: 0x04000115 RID: 277
		private string attrValue;

		// Token: 0x04000116 RID: 278
		private XContainer root;
	}
}
