﻿using System;

namespace System.Xml.Linq
{
	// Token: 0x02000029 RID: 41
	internal struct NamespaceCache
	{
		// Token: 0x06000188 RID: 392 RVA: 0x00007BDB File Offset: 0x00005DDB
		public XNamespace Get(string namespaceName)
		{
			if (namespaceName == this.namespaceName)
			{
				return this.ns;
			}
			this.namespaceName = namespaceName;
			this.ns = XNamespace.Get(namespaceName);
			return this.ns;
		}

		// Token: 0x0400009B RID: 155
		private XNamespace ns;

		// Token: 0x0400009C RID: 156
		private string namespaceName;
	}
}
