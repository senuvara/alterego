﻿using System;

namespace System.Xml.Linq
{
	/// <summary>Represents an XML comment. </summary>
	// Token: 0x02000033 RID: 51
	public class XComment : XNode
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XComment" /> class with the specified string content. </summary>
		/// <param name="value">A string that contains the contents of the new <see cref="T:System.Xml.Linq.XComment" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="value" /> parameter is <see langword="null" />.</exception>
		// Token: 0x06000229 RID: 553 RVA: 0x00009AAA File Offset: 0x00007CAA
		public XComment(string value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			this.value = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XComment" /> class from an existing comment node. </summary>
		/// <param name="other">The <see cref="T:System.Xml.Linq.XComment" /> node to copy from.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="other" /> parameter is <see langword="null" />.</exception>
		// Token: 0x0600022A RID: 554 RVA: 0x00009AC7 File Offset: 0x00007CC7
		public XComment(XComment other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			this.value = other.value;
		}

		// Token: 0x0600022B RID: 555 RVA: 0x00009AE9 File Offset: 0x00007CE9
		internal XComment(XmlReader r)
		{
			this.value = r.Value;
			r.Read();
		}

		/// <summary>Gets the node type for this node.</summary>
		/// <returns>The node type. For <see cref="T:System.Xml.Linq.XComment" /> objects, this value is <see cref="F:System.Xml.XmlNodeType.Comment" />.</returns>
		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600022C RID: 556 RVA: 0x00009B04 File Offset: 0x00007D04
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.Comment;
			}
		}

		/// <summary>Gets or sets the string value of this comment.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the string value of this comment.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600022D RID: 557 RVA: 0x00009B07 File Offset: 0x00007D07
		// (set) Token: 0x0600022E RID: 558 RVA: 0x00009B0F File Offset: 0x00007D0F
		public string Value
		{
			get
			{
				return this.value;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				bool flag = base.NotifyChanging(this, XObjectChangeEventArgs.Value);
				this.value = value;
				if (flag)
				{
					base.NotifyChanged(this, XObjectChangeEventArgs.Value);
				}
			}
		}

		/// <summary>Write this comment to an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> into which this method will write.</param>
		// Token: 0x0600022F RID: 559 RVA: 0x00009B41 File Offset: 0x00007D41
		public override void WriteTo(XmlWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			writer.WriteComment(this.value);
		}

		// Token: 0x06000230 RID: 560 RVA: 0x00009B5D File Offset: 0x00007D5D
		internal override XNode CloneNode()
		{
			return new XComment(this);
		}

		// Token: 0x06000231 RID: 561 RVA: 0x00009B68 File Offset: 0x00007D68
		internal override bool DeepEquals(XNode node)
		{
			XComment xcomment = node as XComment;
			return xcomment != null && this.value == xcomment.value;
		}

		// Token: 0x06000232 RID: 562 RVA: 0x00009B92 File Offset: 0x00007D92
		internal override int GetDeepHashCode()
		{
			return this.value.GetHashCode();
		}

		// Token: 0x040000BD RID: 189
		internal string value;
	}
}
