﻿using System;
using System.Runtime.Serialization;

namespace System.Xml.Linq
{
	// Token: 0x0200000A RID: 10
	[Serializable]
	internal sealed class NameSerializer : IObjectReference, ISerializable
	{
		// Token: 0x06000073 RID: 115 RVA: 0x00003FD1 File Offset: 0x000021D1
		private NameSerializer(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.expandedName = info.GetString("name");
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00003FF8 File Offset: 0x000021F8
		object IObjectReference.GetRealObject(StreamingContext context)
		{
			return XName.Get(this.expandedName);
		}

		// Token: 0x06000075 RID: 117 RVA: 0x00003C5D File Offset: 0x00001E5D
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0400001F RID: 31
		private string expandedName;
	}
}
