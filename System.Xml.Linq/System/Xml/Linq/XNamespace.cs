﻿using System;
using System.Threading;
using Unity;

namespace System.Xml.Linq
{
	/// <summary>Represents an XML namespace. This class cannot be inherited. </summary>
	// Token: 0x0200000B RID: 11
	public sealed class XNamespace
	{
		// Token: 0x06000076 RID: 118 RVA: 0x00004005 File Offset: 0x00002205
		internal XNamespace(string namespaceName)
		{
			this.namespaceName = namespaceName;
			this.hashCode = namespaceName.GetHashCode();
			this.names = new XHashtable<XName>(new XHashtable<XName>.ExtractKeyDelegate(XNamespace.ExtractLocalName), 8);
		}

		/// <summary>Gets the Uniform Resource Identifier (URI) of this namespace.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the URI of the namespace.</returns>
		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000077 RID: 119 RVA: 0x00004038 File Offset: 0x00002238
		public string NamespaceName
		{
			get
			{
				return this.namespaceName;
			}
		}

		/// <summary>Returns an <see cref="T:System.Xml.Linq.XName" /> object created from this <see cref="T:System.Xml.Linq.XNamespace" /> and the specified local name.</summary>
		/// <param name="localName">A <see cref="T:System.String" /> that contains a local name.</param>
		/// <returns>An <see cref="T:System.Xml.Linq.XName" /> created from this <see cref="T:System.Xml.Linq.XNamespace" /> and the specified local name.</returns>
		// Token: 0x06000078 RID: 120 RVA: 0x00004040 File Offset: 0x00002240
		public XName GetName(string localName)
		{
			if (localName == null)
			{
				throw new ArgumentNullException("localName");
			}
			return this.GetName(localName, 0, localName.Length);
		}

		/// <summary>Returns the URI of this <see cref="T:System.Xml.Linq.XNamespace" />.</summary>
		/// <returns>The URI of this <see cref="T:System.Xml.Linq.XNamespace" />.</returns>
		// Token: 0x06000079 RID: 121 RVA: 0x00004038 File Offset: 0x00002238
		public override string ToString()
		{
			return this.namespaceName;
		}

		/// <summary>Gets the <see cref="T:System.Xml.Linq.XNamespace" /> object that corresponds to no namespace.</summary>
		/// <returns>The <see cref="T:System.Xml.Linq.XNamespace" /> that corresponds to no namespace.</returns>
		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600007A RID: 122 RVA: 0x0000405E File Offset: 0x0000225E
		public static XNamespace None
		{
			get
			{
				return XNamespace.EnsureNamespace(ref XNamespace.refNone, string.Empty);
			}
		}

		/// <summary>Gets the <see cref="T:System.Xml.Linq.XNamespace" /> object that corresponds to the XML URI (http://www.w3.org/XML/1998/namespace).</summary>
		/// <returns>The <see cref="T:System.Xml.Linq.XNamespace" /> that corresponds to the XML URI (http://www.w3.org/XML/1998/namespace).</returns>
		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600007B RID: 123 RVA: 0x0000406F File Offset: 0x0000226F
		public static XNamespace Xml
		{
			get
			{
				return XNamespace.EnsureNamespace(ref XNamespace.refXml, "http://www.w3.org/XML/1998/namespace");
			}
		}

		/// <summary>Gets the <see cref="T:System.Xml.Linq.XNamespace" /> object that corresponds to the xmlns URI (http://www.w3.org/2000/xmlns/).</summary>
		/// <returns>The <see cref="T:System.Xml.Linq.XNamespace" /> that corresponds to the xmlns URI (http://www.w3.org/2000/xmlns/).</returns>
		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600007C RID: 124 RVA: 0x00004080 File Offset: 0x00002280
		public static XNamespace Xmlns
		{
			get
			{
				return XNamespace.EnsureNamespace(ref XNamespace.refXmlns, "http://www.w3.org/2000/xmlns/");
			}
		}

		/// <summary>Gets an <see cref="T:System.Xml.Linq.XNamespace" /> for the specified Uniform Resource Identifier (URI).</summary>
		/// <param name="namespaceName">A <see cref="T:System.String" /> that contains a namespace URI.</param>
		/// <returns>An <see cref="T:System.Xml.Linq.XNamespace" /> created from the specified URI.</returns>
		// Token: 0x0600007D RID: 125 RVA: 0x00004091 File Offset: 0x00002291
		public static XNamespace Get(string namespaceName)
		{
			if (namespaceName == null)
			{
				throw new ArgumentNullException("namespaceName");
			}
			return XNamespace.Get(namespaceName, 0, namespaceName.Length);
		}

		/// <summary>Converts a string containing a Uniform Resource Identifier (URI) to an <see cref="T:System.Xml.Linq.XNamespace" />.</summary>
		/// <param name="namespaceName">A <see cref="T:System.String" /> that contains the namespace URI.</param>
		/// <returns>An <see cref="T:System.Xml.Linq.XNamespace" /> constructed from the URI string.</returns>
		// Token: 0x0600007E RID: 126 RVA: 0x000040AE File Offset: 0x000022AE
		[CLSCompliant(false)]
		public static implicit operator XNamespace(string namespaceName)
		{
			if (namespaceName == null)
			{
				return null;
			}
			return XNamespace.Get(namespaceName);
		}

		/// <summary>Combines an <see cref="T:System.Xml.Linq.XNamespace" /> object with a local name to create an <see cref="T:System.Xml.Linq.XName" />.</summary>
		/// <param name="ns">An <see cref="T:System.Xml.Linq.XNamespace" /> that contains the namespace.</param>
		/// <param name="localName">A <see cref="T:System.String" /> that contains the local name.</param>
		/// <returns>The new <see cref="T:System.Xml.Linq.XName" /> constructed from the namespace and local name.</returns>
		// Token: 0x0600007F RID: 127 RVA: 0x000040BB File Offset: 0x000022BB
		public static XName operator +(XNamespace ns, string localName)
		{
			if (ns == null)
			{
				throw new ArgumentNullException("ns");
			}
			return ns.GetName(localName);
		}

		/// <summary>Determines whether the specified <see cref="T:System.Xml.Linq.XNamespace" /> is equal to the current <see cref="T:System.Xml.Linq.XNamespace" />.</summary>
		/// <param name="obj">The <see cref="T:System.Xml.Linq.XNamespace" /> to compare to the current <see cref="T:System.Xml.Linq.XNamespace" />.</param>
		/// <returns>A <see cref="T:System.Boolean" /> that indicates whether the specified <see cref="T:System.Xml.Linq.XNamespace" /> is equal to the current <see cref="T:System.Xml.Linq.XNamespace" />.</returns>
		// Token: 0x06000080 RID: 128 RVA: 0x00003F82 File Offset: 0x00002182
		public override bool Equals(object obj)
		{
			return this == obj;
		}

		/// <summary>Gets a hash code for this <see cref="T:System.Xml.Linq.XNamespace" />.</summary>
		/// <returns>An <see cref="T:System.Int32" /> that contains the hash code for the <see cref="T:System.Xml.Linq.XNamespace" />.</returns>
		// Token: 0x06000081 RID: 129 RVA: 0x000040D8 File Offset: 0x000022D8
		public override int GetHashCode()
		{
			return this.hashCode;
		}

		/// <summary>Returns a value indicating whether two instances of <see cref="T:System.Xml.Linq.XNamespace" /> are equal.</summary>
		/// <param name="left">The first <see cref="T:System.Xml.Linq.XNamespace" /> to compare.</param>
		/// <param name="right">The second <see cref="T:System.Xml.Linq.XNamespace" /> to compare.</param>
		/// <returns>A <see cref="T:System.Boolean" /> that indicates whether <paramref name="left" /> and <paramref name="right" /> are equal.</returns>
		// Token: 0x06000082 RID: 130 RVA: 0x00003F82 File Offset: 0x00002182
		public static bool operator ==(XNamespace left, XNamespace right)
		{
			return left == right;
		}

		/// <summary>Returns a value indicating whether two instances of <see cref="T:System.Xml.Linq.XNamespace" /> are not equal.</summary>
		/// <param name="left">The first <see cref="T:System.Xml.Linq.XNamespace" /> to compare.</param>
		/// <param name="right">The second <see cref="T:System.Xml.Linq.XNamespace" /> to compare.</param>
		/// <returns>A <see cref="T:System.Boolean" /> that indicates whether <paramref name="left" /> and <paramref name="right" /> are not equal.</returns>
		// Token: 0x06000083 RID: 131 RVA: 0x00003F90 File Offset: 0x00002190
		public static bool operator !=(XNamespace left, XNamespace right)
		{
			return left != right;
		}

		// Token: 0x06000084 RID: 132 RVA: 0x000040E0 File Offset: 0x000022E0
		internal XName GetName(string localName, int index, int count)
		{
			XName result;
			if (this.names.TryGetValue(localName, index, count, out result))
			{
				return result;
			}
			return this.names.Add(new XName(this, localName.Substring(index, count)));
		}

		// Token: 0x06000085 RID: 133 RVA: 0x0000411C File Offset: 0x0000231C
		internal static XNamespace Get(string namespaceName, int index, int count)
		{
			if (count == 0)
			{
				return XNamespace.None;
			}
			if (XNamespace.namespaces == null)
			{
				Interlocked.CompareExchange<XHashtable<WeakReference>>(ref XNamespace.namespaces, new XHashtable<WeakReference>(new XHashtable<WeakReference>.ExtractKeyDelegate(XNamespace.ExtractNamespace), 32), null);
			}
			for (;;)
			{
				WeakReference weakReference;
				if (!XNamespace.namespaces.TryGetValue(namespaceName, index, count, out weakReference))
				{
					if (count == "http://www.w3.org/XML/1998/namespace".Length && string.CompareOrdinal(namespaceName, index, "http://www.w3.org/XML/1998/namespace", 0, count) == 0)
					{
						break;
					}
					if (count == "http://www.w3.org/2000/xmlns/".Length && string.CompareOrdinal(namespaceName, index, "http://www.w3.org/2000/xmlns/", 0, count) == 0)
					{
						goto Block_7;
					}
					weakReference = XNamespace.namespaces.Add(new WeakReference(new XNamespace(namespaceName.Substring(index, count))));
				}
				XNamespace xnamespace = (weakReference != null) ? ((XNamespace)weakReference.Target) : null;
				if (!(xnamespace == null))
				{
					return xnamespace;
				}
			}
			return XNamespace.Xml;
			Block_7:
			return XNamespace.Xmlns;
		}

		// Token: 0x06000086 RID: 134 RVA: 0x000041EB File Offset: 0x000023EB
		private static string ExtractLocalName(XName n)
		{
			return n.LocalName;
		}

		// Token: 0x06000087 RID: 135 RVA: 0x000041F4 File Offset: 0x000023F4
		private static string ExtractNamespace(WeakReference r)
		{
			XNamespace xnamespace;
			if (r == null || (xnamespace = (XNamespace)r.Target) == null)
			{
				return null;
			}
			return xnamespace.NamespaceName;
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00004224 File Offset: 0x00002424
		private static XNamespace EnsureNamespace(ref WeakReference refNmsp, string namespaceName)
		{
			XNamespace xnamespace;
			for (;;)
			{
				WeakReference weakReference = refNmsp;
				if (weakReference != null)
				{
					xnamespace = (XNamespace)weakReference.Target;
					if (xnamespace != null)
					{
						break;
					}
				}
				Interlocked.CompareExchange<WeakReference>(ref refNmsp, new WeakReference(new XNamespace(namespaceName)), weakReference);
			}
			return xnamespace;
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00003FCA File Offset: 0x000021CA
		internal XNamespace()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000020 RID: 32
		internal const string xmlPrefixNamespace = "http://www.w3.org/XML/1998/namespace";

		// Token: 0x04000021 RID: 33
		internal const string xmlnsPrefixNamespace = "http://www.w3.org/2000/xmlns/";

		// Token: 0x04000022 RID: 34
		private static XHashtable<WeakReference> namespaces;

		// Token: 0x04000023 RID: 35
		private static WeakReference refNone;

		// Token: 0x04000024 RID: 36
		private static WeakReference refXml;

		// Token: 0x04000025 RID: 37
		private static WeakReference refXmlns;

		// Token: 0x04000026 RID: 38
		private string namespaceName;

		// Token: 0x04000027 RID: 39
		private int hashCode;

		// Token: 0x04000028 RID: 40
		private XHashtable<XName> names;

		// Token: 0x04000029 RID: 41
		private const int NamesCapacity = 8;

		// Token: 0x0400002A RID: 42
		private const int NamespacesCapacity = 32;
	}
}
