﻿using System;

namespace System.Xml.Linq
{
	// Token: 0x02000014 RID: 20
	internal class LineInfoAnnotation
	{
		// Token: 0x060000C3 RID: 195 RVA: 0x00004FFA File Offset: 0x000031FA
		public LineInfoAnnotation(int lineNumber, int linePosition)
		{
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
		}

		// Token: 0x04000047 RID: 71
		internal int lineNumber;

		// Token: 0x04000048 RID: 72
		internal int linePosition;
	}
}
