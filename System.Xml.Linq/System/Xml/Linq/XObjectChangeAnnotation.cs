﻿using System;

namespace System.Xml.Linq
{
	// Token: 0x02000016 RID: 22
	internal class XObjectChangeAnnotation
	{
		// Token: 0x060000C5 RID: 197 RVA: 0x00002966 File Offset: 0x00000B66
		public XObjectChangeAnnotation()
		{
		}

		// Token: 0x04000049 RID: 73
		internal EventHandler<XObjectChangeEventArgs> changing;

		// Token: 0x0400004A RID: 74
		internal EventHandler<XObjectChangeEventArgs> changed;
	}
}
