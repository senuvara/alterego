﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace System.Xml.Linq
{
	/// <summary>Represents a node that can contain other nodes.</summary>
	// Token: 0x02000023 RID: 35
	public abstract class XContainer : XNode
	{
		// Token: 0x06000132 RID: 306 RVA: 0x00005F1B File Offset: 0x0000411B
		internal XContainer()
		{
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00005F24 File Offset: 0x00004124
		internal XContainer(XContainer other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			if (other.content is string)
			{
				this.content = other.content;
				return;
			}
			XNode xnode = (XNode)other.content;
			if (xnode != null)
			{
				do
				{
					xnode = xnode.next;
					this.AppendNodeSkipNotify(xnode.CloneNode());
				}
				while (xnode != other.content);
			}
		}

		/// <summary>Get the first child node of this node.</summary>
		/// <returns>An <see cref="T:System.Xml.Linq.XNode" /> containing the first child node of the <see cref="T:System.Xml.Linq.XContainer" />.</returns>
		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000134 RID: 308 RVA: 0x00005F8C File Offset: 0x0000418C
		public XNode FirstNode
		{
			get
			{
				XNode lastNode = this.LastNode;
				if (lastNode == null)
				{
					return null;
				}
				return lastNode.next;
			}
		}

		/// <summary>Get the last child node of this node.</summary>
		/// <returns>An <see cref="T:System.Xml.Linq.XNode" /> containing the last child node of the <see cref="T:System.Xml.Linq.XContainer" />.</returns>
		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000135 RID: 309 RVA: 0x00005FAC File Offset: 0x000041AC
		public XNode LastNode
		{
			get
			{
				if (this.content == null)
				{
					return null;
				}
				XNode xnode = this.content as XNode;
				if (xnode != null)
				{
					return xnode;
				}
				string text = this.content as string;
				if (text != null)
				{
					if (text.Length == 0)
					{
						return null;
					}
					XText xtext = new XText(text);
					xtext.parent = this;
					xtext.next = xtext;
					Interlocked.CompareExchange<object>(ref this.content, xtext, text);
				}
				return (XNode)this.content;
			}
		}

		/// <summary>Adds the specified content as children of this <see cref="T:System.Xml.Linq.XContainer" />.</summary>
		/// <param name="content">A content object containing simple content or a collection of content objects to be added.</param>
		// Token: 0x06000136 RID: 310 RVA: 0x0000601C File Offset: 0x0000421C
		public void Add(object content)
		{
			if (base.SkipNotify())
			{
				this.AddContentSkipNotify(content);
				return;
			}
			if (content == null)
			{
				return;
			}
			XNode xnode = content as XNode;
			if (xnode != null)
			{
				this.AddNode(xnode);
				return;
			}
			string text = content as string;
			if (text != null)
			{
				this.AddString(text);
				return;
			}
			XAttribute xattribute = content as XAttribute;
			if (xattribute != null)
			{
				this.AddAttribute(xattribute);
				return;
			}
			XStreamingElement xstreamingElement = content as XStreamingElement;
			if (xstreamingElement != null)
			{
				this.AddNode(new XElement(xstreamingElement));
				return;
			}
			object[] array = content as object[];
			if (array != null)
			{
				foreach (object obj in array)
				{
					this.Add(obj);
				}
				return;
			}
			IEnumerable enumerable = content as IEnumerable;
			if (enumerable != null)
			{
				foreach (object obj2 in enumerable)
				{
					this.Add(obj2);
				}
				return;
			}
			this.AddString(XContainer.GetStringValue(content));
		}

		/// <summary>Adds the specified content as children of this <see cref="T:System.Xml.Linq.XContainer" />.</summary>
		/// <param name="content">A parameter list of content objects.</param>
		// Token: 0x06000137 RID: 311 RVA: 0x00006124 File Offset: 0x00004324
		public void Add(params object[] content)
		{
			this.Add(content);
		}

		/// <summary>Adds the specified content as the first children of this document or element.</summary>
		/// <param name="content">A content object containing simple content or a collection of content objects to be added.</param>
		// Token: 0x06000138 RID: 312 RVA: 0x00006130 File Offset: 0x00004330
		public void AddFirst(object content)
		{
			new Inserter(this, null).Add(content);
		}

		/// <summary>Adds the specified content as the first children of this document or element.</summary>
		/// <param name="content">A parameter list of content objects.</param>
		/// <exception cref="T:System.InvalidOperationException">The parent is <see langword="null" />.</exception>
		// Token: 0x06000139 RID: 313 RVA: 0x0000614D File Offset: 0x0000434D
		public void AddFirst(params object[] content)
		{
			this.AddFirst(content);
		}

		/// <summary>Creates an <see cref="T:System.Xml.XmlWriter" /> that can be used to add nodes to the <see cref="T:System.Xml.Linq.XContainer" />.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlWriter" /> that is ready to have content written to it.</returns>
		// Token: 0x0600013A RID: 314 RVA: 0x00006158 File Offset: 0x00004358
		public XmlWriter CreateWriter()
		{
			XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
			xmlWriterSettings.ConformanceLevel = ((this is XDocument) ? ConformanceLevel.Document : ConformanceLevel.Fragment);
			return XmlWriter.Create(new XNodeBuilder(this), xmlWriterSettings);
		}

		/// <summary>Returns a collection of the descendant nodes for this document or element, in document order.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> containing the descendant nodes of the <see cref="T:System.Xml.Linq.XContainer" />, in document order.</returns>
		// Token: 0x0600013B RID: 315 RVA: 0x00006189 File Offset: 0x00004389
		public IEnumerable<XNode> DescendantNodes()
		{
			return this.GetDescendantNodes(false);
		}

		/// <summary>Returns a collection of the descendant elements for this document or element, in document order.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> containing the descendant elements of the <see cref="T:System.Xml.Linq.XContainer" />.</returns>
		// Token: 0x0600013C RID: 316 RVA: 0x00006192 File Offset: 0x00004392
		public IEnumerable<XElement> Descendants()
		{
			return this.GetDescendants(null, false);
		}

		/// <summary>Returns a filtered collection of the descendant elements for this document or element, in document order. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> containing the descendant elements of the <see cref="T:System.Xml.Linq.XContainer" /> that match the specified <see cref="T:System.Xml.Linq.XName" />.</returns>
		// Token: 0x0600013D RID: 317 RVA: 0x0000619C File Offset: 0x0000439C
		public IEnumerable<XElement> Descendants(XName name)
		{
			if (!(name != null))
			{
				return XElement.EmptySequence;
			}
			return this.GetDescendants(name, false);
		}

		/// <summary>Gets the first (in document order) child element with the specified <see cref="T:System.Xml.Linq.XName" />.</summary>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <returns>A <see cref="T:System.Xml.Linq.XElement" /> that matches the specified <see cref="T:System.Xml.Linq.XName" />, or <see langword="null" />.</returns>
		// Token: 0x0600013E RID: 318 RVA: 0x000061B8 File Offset: 0x000043B8
		public XElement Element(XName name)
		{
			XNode xnode = this.content as XNode;
			if (xnode != null)
			{
				XElement xelement;
				for (;;)
				{
					xnode = xnode.next;
					xelement = (xnode as XElement);
					if (xelement != null && xelement.name == name)
					{
						break;
					}
					if (xnode == this.content)
					{
						goto IL_39;
					}
				}
				return xelement;
			}
			IL_39:
			return null;
		}

		/// <summary>Returns a collection of the child elements of this element or document, in document order.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> containing the child elements of this <see cref="T:System.Xml.Linq.XContainer" />, in document order.</returns>
		// Token: 0x0600013F RID: 319 RVA: 0x000061FF File Offset: 0x000043FF
		public IEnumerable<XElement> Elements()
		{
			return this.GetElements(null);
		}

		/// <summary>Returns a filtered collection of the child elements of this element or document, in document order. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> containing the children of the <see cref="T:System.Xml.Linq.XContainer" /> that have a matching <see cref="T:System.Xml.Linq.XName" />, in document order.</returns>
		// Token: 0x06000140 RID: 320 RVA: 0x00006208 File Offset: 0x00004408
		public IEnumerable<XElement> Elements(XName name)
		{
			if (!(name != null))
			{
				return XElement.EmptySequence;
			}
			return this.GetElements(name);
		}

		/// <summary>Returns a collection of the child nodes of this element or document, in document order.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> containing the contents of this <see cref="T:System.Xml.Linq.XContainer" />, in document order.</returns>
		// Token: 0x06000141 RID: 321 RVA: 0x00006220 File Offset: 0x00004420
		public IEnumerable<XNode> Nodes()
		{
			XNode i = this.LastNode;
			if (i != null)
			{
				do
				{
					i = i.next;
					yield return i;
				}
				while (i.parent == this && i != this.content);
			}
			yield break;
		}

		/// <summary>Removes the child nodes from this document or element.</summary>
		// Token: 0x06000142 RID: 322 RVA: 0x00006230 File Offset: 0x00004430
		public void RemoveNodes()
		{
			if (base.SkipNotify())
			{
				this.RemoveNodesSkipNotify();
				return;
			}
			while (this.content != null)
			{
				string text = this.content as string;
				if (text != null)
				{
					if (text.Length > 0)
					{
						this.ConvertTextToNode();
					}
					else if (this is XElement)
					{
						base.NotifyChanging(this, XObjectChangeEventArgs.Value);
						if (text != this.content)
						{
							throw new InvalidOperationException(Res.GetString("InvalidOperation_ExternalCode"));
						}
						this.content = null;
						base.NotifyChanged(this, XObjectChangeEventArgs.Value);
					}
					else
					{
						this.content = null;
					}
				}
				XNode xnode = this.content as XNode;
				if (xnode != null)
				{
					XNode next = xnode.next;
					base.NotifyChanging(next, XObjectChangeEventArgs.Remove);
					if (xnode != this.content || next != xnode.next)
					{
						throw new InvalidOperationException(Res.GetString("InvalidOperation_ExternalCode"));
					}
					if (next != xnode)
					{
						xnode.next = next.next;
					}
					else
					{
						this.content = null;
					}
					next.parent = null;
					next.next = null;
					base.NotifyChanged(next, XObjectChangeEventArgs.Remove);
				}
			}
		}

		/// <summary>Replaces the children nodes of this document or element with the specified content.</summary>
		/// <param name="content">A content object containing simple content or a collection of content objects that replace the children nodes.</param>
		// Token: 0x06000143 RID: 323 RVA: 0x0000633E File Offset: 0x0000453E
		public void ReplaceNodes(object content)
		{
			content = XContainer.GetContentSnapshot(content);
			this.RemoveNodes();
			this.Add(content);
		}

		/// <summary>Replaces the children nodes of this document or element with the specified content.</summary>
		/// <param name="content">A parameter list of content objects.</param>
		// Token: 0x06000144 RID: 324 RVA: 0x00006355 File Offset: 0x00004555
		public void ReplaceNodes(params object[] content)
		{
			this.ReplaceNodes(content);
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00004D15 File Offset: 0x00002F15
		internal virtual void AddAttribute(XAttribute a)
		{
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00004D15 File Offset: 0x00002F15
		internal virtual void AddAttributeSkipNotify(XAttribute a)
		{
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00006360 File Offset: 0x00004560
		internal void AddContentSkipNotify(object content)
		{
			if (content == null)
			{
				return;
			}
			XNode xnode = content as XNode;
			if (xnode != null)
			{
				this.AddNodeSkipNotify(xnode);
				return;
			}
			string text = content as string;
			if (text != null)
			{
				this.AddStringSkipNotify(text);
				return;
			}
			XAttribute xattribute = content as XAttribute;
			if (xattribute != null)
			{
				this.AddAttributeSkipNotify(xattribute);
				return;
			}
			XStreamingElement xstreamingElement = content as XStreamingElement;
			if (xstreamingElement != null)
			{
				this.AddNodeSkipNotify(new XElement(xstreamingElement));
				return;
			}
			object[] array = content as object[];
			if (array != null)
			{
				foreach (object obj in array)
				{
					this.AddContentSkipNotify(obj);
				}
				return;
			}
			IEnumerable enumerable = content as IEnumerable;
			if (enumerable != null)
			{
				foreach (object obj2 in enumerable)
				{
					this.AddContentSkipNotify(obj2);
				}
				return;
			}
			this.AddStringSkipNotify(XContainer.GetStringValue(content));
		}

		// Token: 0x06000148 RID: 328 RVA: 0x00006458 File Offset: 0x00004658
		internal void AddNode(XNode n)
		{
			this.ValidateNode(n, this);
			if (n.parent != null)
			{
				n = n.CloneNode();
			}
			else
			{
				XNode xnode = this;
				while (xnode.parent != null)
				{
					xnode = xnode.parent;
				}
				if (n == xnode)
				{
					n = n.CloneNode();
				}
			}
			this.ConvertTextToNode();
			this.AppendNode(n);
		}

		// Token: 0x06000149 RID: 329 RVA: 0x000064AC File Offset: 0x000046AC
		internal void AddNodeSkipNotify(XNode n)
		{
			this.ValidateNode(n, this);
			if (n.parent != null)
			{
				n = n.CloneNode();
			}
			else
			{
				XNode xnode = this;
				while (xnode.parent != null)
				{
					xnode = xnode.parent;
				}
				if (n == xnode)
				{
					n = n.CloneNode();
				}
			}
			this.ConvertTextToNode();
			this.AppendNodeSkipNotify(n);
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00006500 File Offset: 0x00004700
		internal void AddString(string s)
		{
			this.ValidateString(s);
			if (this.content != null)
			{
				if (s.Length > 0)
				{
					this.ConvertTextToNode();
					XText xtext = this.content as XText;
					if (xtext != null && !(xtext is XCData))
					{
						XText xtext2 = xtext;
						xtext2.Value += s;
						return;
					}
					this.AppendNode(new XText(s));
				}
				return;
			}
			if (s.Length > 0)
			{
				this.AppendNode(new XText(s));
				return;
			}
			if (!(this is XElement))
			{
				this.content = s;
				return;
			}
			base.NotifyChanging(this, XObjectChangeEventArgs.Value);
			if (this.content != null)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_ExternalCode"));
			}
			this.content = s;
			base.NotifyChanged(this, XObjectChangeEventArgs.Value);
		}

		// Token: 0x0600014B RID: 331 RVA: 0x000065C4 File Offset: 0x000047C4
		internal void AddStringSkipNotify(string s)
		{
			this.ValidateString(s);
			if (this.content == null)
			{
				this.content = s;
				return;
			}
			if (s.Length > 0)
			{
				if (this.content is string)
				{
					this.content = (string)this.content + s;
					return;
				}
				XText xtext = this.content as XText;
				if (xtext != null && !(xtext is XCData))
				{
					XText xtext2 = xtext;
					xtext2.text += s;
					return;
				}
				this.AppendNodeSkipNotify(new XText(s));
			}
		}

		// Token: 0x0600014C RID: 332 RVA: 0x0000664C File Offset: 0x0000484C
		internal void AppendNode(XNode n)
		{
			bool flag = base.NotifyChanging(n, XObjectChangeEventArgs.Add);
			if (n.parent != null)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_ExternalCode"));
			}
			this.AppendNodeSkipNotify(n);
			if (flag)
			{
				base.NotifyChanged(n, XObjectChangeEventArgs.Add);
			}
		}

		// Token: 0x0600014D RID: 333 RVA: 0x00006688 File Offset: 0x00004888
		internal void AppendNodeSkipNotify(XNode n)
		{
			n.parent = this;
			if (this.content == null || this.content is string)
			{
				n.next = n;
			}
			else
			{
				XNode xnode = (XNode)this.content;
				n.next = xnode.next;
				xnode.next = n;
			}
			this.content = n;
		}

		// Token: 0x0600014E RID: 334 RVA: 0x000066E0 File Offset: 0x000048E0
		internal override void AppendText(StringBuilder sb)
		{
			string text = this.content as string;
			if (text != null)
			{
				sb.Append(text);
				return;
			}
			XNode xnode = (XNode)this.content;
			if (xnode != null)
			{
				do
				{
					xnode = xnode.next;
					xnode.AppendText(sb);
				}
				while (xnode != this.content);
			}
		}

		// Token: 0x0600014F RID: 335 RVA: 0x0000672C File Offset: 0x0000492C
		private string GetTextOnly()
		{
			if (this.content == null)
			{
				return null;
			}
			string text = this.content as string;
			if (text == null)
			{
				XNode xnode = (XNode)this.content;
				for (;;)
				{
					xnode = xnode.next;
					if (xnode.NodeType != XmlNodeType.Text)
					{
						break;
					}
					text += ((XText)xnode).Value;
					if (xnode == this.content)
					{
						return text;
					}
				}
				return null;
			}
			return text;
		}

		// Token: 0x06000150 RID: 336 RVA: 0x0000678C File Offset: 0x0000498C
		private string CollectText(ref XNode n)
		{
			string text = "";
			while (n != null && n.NodeType == XmlNodeType.Text)
			{
				text += ((XText)n).Value;
				n = ((n != this.content) ? n.next : null);
			}
			return text;
		}

		// Token: 0x06000151 RID: 337 RVA: 0x000067DC File Offset: 0x000049DC
		internal bool ContentsEqual(XContainer e)
		{
			if (this.content == e.content)
			{
				return true;
			}
			string textOnly = this.GetTextOnly();
			if (textOnly != null)
			{
				return textOnly == e.GetTextOnly();
			}
			XNode xnode = this.content as XNode;
			XNode xnode2 = e.content as XNode;
			if (xnode != null && xnode2 != null)
			{
				xnode = xnode.next;
				xnode2 = xnode2.next;
				while (!(this.CollectText(ref xnode) != e.CollectText(ref xnode2)))
				{
					if (xnode == null && xnode2 == null)
					{
						return true;
					}
					if (xnode == null || xnode2 == null || !xnode.DeepEquals(xnode2))
					{
						break;
					}
					xnode = ((xnode != this.content) ? xnode.next : null);
					xnode2 = ((xnode2 != e.content) ? xnode2.next : null);
				}
			}
			return false;
		}

		// Token: 0x06000152 RID: 338 RVA: 0x00006894 File Offset: 0x00004A94
		internal int ContentsHashCode()
		{
			string textOnly = this.GetTextOnly();
			if (textOnly != null)
			{
				return textOnly.GetHashCode();
			}
			int num = 0;
			XNode xnode = this.content as XNode;
			if (xnode != null)
			{
				do
				{
					xnode = xnode.next;
					string text = this.CollectText(ref xnode);
					if (text.Length > 0)
					{
						num ^= text.GetHashCode();
					}
					if (xnode == null)
					{
						break;
					}
					num ^= xnode.GetDeepHashCode();
				}
				while (xnode != this.content);
			}
			return num;
		}

		// Token: 0x06000153 RID: 339 RVA: 0x000068FC File Offset: 0x00004AFC
		internal void ConvertTextToNode()
		{
			string text = this.content as string;
			if (text != null && text.Length > 0)
			{
				XText xtext = new XText(text);
				xtext.parent = this;
				xtext.next = xtext;
				this.content = xtext;
			}
		}

		// Token: 0x06000154 RID: 340 RVA: 0x0000693D File Offset: 0x00004B3D
		internal static string GetDateTimeString(DateTime value)
		{
			return XmlConvert.ToString(value, XmlDateTimeSerializationMode.RoundtripKind);
		}

		// Token: 0x06000155 RID: 341 RVA: 0x00006946 File Offset: 0x00004B46
		internal IEnumerable<XNode> GetDescendantNodes(bool self)
		{
			if (self)
			{
				yield return this;
			}
			XNode i = this;
			for (;;)
			{
				XContainer xcontainer = i as XContainer;
				XNode firstNode;
				if (xcontainer != null && (firstNode = xcontainer.FirstNode) != null)
				{
					i = firstNode;
				}
				else
				{
					while (i != null && i != this && i == i.parent.content)
					{
						i = i.parent;
					}
					if (i == null || i == this)
					{
						break;
					}
					i = i.next;
				}
				yield return i;
			}
			yield break;
		}

		// Token: 0x06000156 RID: 342 RVA: 0x0000695D File Offset: 0x00004B5D
		internal IEnumerable<XElement> GetDescendants(XName name, bool self)
		{
			if (self)
			{
				XElement xelement = (XElement)this;
				if (name == null || xelement.name == name)
				{
					yield return xelement;
				}
			}
			XNode i = this;
			XContainer xcontainer = this;
			for (;;)
			{
				if (xcontainer != null && xcontainer.content is XNode)
				{
					i = ((XNode)xcontainer.content).next;
				}
				else
				{
					while (i != this && i == i.parent.content)
					{
						i = i.parent;
					}
					if (i == this)
					{
						break;
					}
					i = i.next;
				}
				XElement e = i as XElement;
				if (e != null && (name == null || e.name == name))
				{
					yield return e;
				}
				xcontainer = e;
				e = null;
			}
			yield break;
		}

		// Token: 0x06000157 RID: 343 RVA: 0x0000697B File Offset: 0x00004B7B
		private IEnumerable<XElement> GetElements(XName name)
		{
			XNode i = this.content as XNode;
			if (i != null)
			{
				do
				{
					i = i.next;
					XElement xelement = i as XElement;
					if (xelement != null && (name == null || xelement.name == name))
					{
						yield return xelement;
					}
				}
				while (i.parent == this && i != this.content);
			}
			yield break;
		}

		// Token: 0x06000158 RID: 344 RVA: 0x00006994 File Offset: 0x00004B94
		internal static string GetStringValue(object value)
		{
			string text;
			if (value is string)
			{
				text = (string)value;
			}
			else if (value is double)
			{
				text = XmlConvert.ToString((double)value);
			}
			else if (value is float)
			{
				text = XmlConvert.ToString((float)value);
			}
			else if (value is decimal)
			{
				text = XmlConvert.ToString((decimal)value);
			}
			else if (value is bool)
			{
				text = XmlConvert.ToString((bool)value);
			}
			else if (value is DateTime)
			{
				text = XContainer.GetDateTimeString((DateTime)value);
			}
			else if (value is DateTimeOffset)
			{
				text = XmlConvert.ToString((DateTimeOffset)value);
			}
			else if (value is TimeSpan)
			{
				text = XmlConvert.ToString((TimeSpan)value);
			}
			else
			{
				if (value is XObject)
				{
					throw new ArgumentException(Res.GetString("Argument_XObjectValue"));
				}
				text = value.ToString();
			}
			if (text == null)
			{
				throw new ArgumentException(Res.GetString("Argument_ConvertToString"));
			}
			return text;
		}

		// Token: 0x06000159 RID: 345 RVA: 0x00006A88 File Offset: 0x00004C88
		internal void ReadContentFrom(XmlReader r)
		{
			if (r.ReadState != ReadState.Interactive)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_ExpectedInteractive"));
			}
			XContainer xcontainer = this;
			NamespaceCache namespaceCache = default(NamespaceCache);
			NamespaceCache namespaceCache2 = default(NamespaceCache);
			for (;;)
			{
				switch (r.NodeType)
				{
				case XmlNodeType.Element:
				{
					XElement xelement = new XElement(namespaceCache.Get(r.NamespaceURI).GetName(r.LocalName));
					if (r.MoveToFirstAttribute())
					{
						do
						{
							xelement.AppendAttributeSkipNotify(new XAttribute(namespaceCache2.Get((r.Prefix.Length == 0) ? string.Empty : r.NamespaceURI).GetName(r.LocalName), r.Value));
						}
						while (r.MoveToNextAttribute());
						r.MoveToElement();
					}
					xcontainer.AddNodeSkipNotify(xelement);
					if (!r.IsEmptyElement)
					{
						xcontainer = xelement;
						goto IL_201;
					}
					goto IL_201;
				}
				case XmlNodeType.Text:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
					xcontainer.AddStringSkipNotify(r.Value);
					goto IL_201;
				case XmlNodeType.CDATA:
					xcontainer.AddNodeSkipNotify(new XCData(r.Value));
					goto IL_201;
				case XmlNodeType.EntityReference:
					if (!r.CanResolveEntity)
					{
						goto Block_8;
					}
					r.ResolveEntity();
					goto IL_201;
				case XmlNodeType.ProcessingInstruction:
					xcontainer.AddNodeSkipNotify(new XProcessingInstruction(r.Name, r.Value));
					goto IL_201;
				case XmlNodeType.Comment:
					xcontainer.AddNodeSkipNotify(new XComment(r.Value));
					goto IL_201;
				case XmlNodeType.DocumentType:
					xcontainer.AddNodeSkipNotify(new XDocumentType(r.LocalName, r.GetAttribute("PUBLIC"), r.GetAttribute("SYSTEM"), r.Value, r.DtdInfo));
					goto IL_201;
				case XmlNodeType.EndElement:
					if (xcontainer.content == null)
					{
						xcontainer.content = string.Empty;
					}
					if (xcontainer == this)
					{
						return;
					}
					xcontainer = xcontainer.parent;
					goto IL_201;
				case XmlNodeType.EndEntity:
					goto IL_201;
				}
				break;
				IL_201:
				if (!r.Read())
				{
					return;
				}
			}
			goto IL_1DD;
			Block_8:
			throw new InvalidOperationException(Res.GetString("InvalidOperation_UnresolvedEntityReference"));
			IL_1DD:
			throw new InvalidOperationException(Res.GetString("InvalidOperation_UnexpectedNodeType", new object[]
			{
				r.NodeType
			}));
		}

		// Token: 0x0600015A RID: 346 RVA: 0x00006CA4 File Offset: 0x00004EA4
		internal void ReadContentFrom(XmlReader r, LoadOptions o)
		{
			if ((o & (LoadOptions.SetBaseUri | LoadOptions.SetLineInfo)) == LoadOptions.None)
			{
				this.ReadContentFrom(r);
				return;
			}
			if (r.ReadState != ReadState.Interactive)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_ExpectedInteractive"));
			}
			XContainer xcontainer = this;
			XNode xnode = null;
			NamespaceCache namespaceCache = default(NamespaceCache);
			NamespaceCache namespaceCache2 = default(NamespaceCache);
			string text = ((o & LoadOptions.SetBaseUri) != LoadOptions.None) ? r.BaseURI : null;
			IXmlLineInfo xmlLineInfo = ((o & LoadOptions.SetLineInfo) != LoadOptions.None) ? (r as IXmlLineInfo) : null;
			for (;;)
			{
				string baseURI = r.BaseURI;
				switch (r.NodeType)
				{
				case XmlNodeType.Element:
				{
					XElement xelement = new XElement(namespaceCache.Get(r.NamespaceURI).GetName(r.LocalName));
					if (text != null && text != baseURI)
					{
						xelement.SetBaseUri(baseURI);
					}
					if (xmlLineInfo != null && xmlLineInfo.HasLineInfo())
					{
						xelement.SetLineInfo(xmlLineInfo.LineNumber, xmlLineInfo.LinePosition);
					}
					if (r.MoveToFirstAttribute())
					{
						do
						{
							XAttribute xattribute = new XAttribute(namespaceCache2.Get((r.Prefix.Length == 0) ? string.Empty : r.NamespaceURI).GetName(r.LocalName), r.Value);
							if (xmlLineInfo != null && xmlLineInfo.HasLineInfo())
							{
								xattribute.SetLineInfo(xmlLineInfo.LineNumber, xmlLineInfo.LinePosition);
							}
							xelement.AppendAttributeSkipNotify(xattribute);
						}
						while (r.MoveToNextAttribute());
						r.MoveToElement();
					}
					xcontainer.AddNodeSkipNotify(xelement);
					if (r.IsEmptyElement)
					{
						goto IL_305;
					}
					xcontainer = xelement;
					if (text != null)
					{
						text = baseURI;
						goto IL_305;
					}
					goto IL_305;
				}
				case XmlNodeType.Text:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
					if ((text != null && text != baseURI) || (xmlLineInfo != null && xmlLineInfo.HasLineInfo()))
					{
						xnode = new XText(r.Value);
						goto IL_305;
					}
					xcontainer.AddStringSkipNotify(r.Value);
					goto IL_305;
				case XmlNodeType.CDATA:
					xnode = new XCData(r.Value);
					goto IL_305;
				case XmlNodeType.EntityReference:
					if (!r.CanResolveEntity)
					{
						goto Block_25;
					}
					r.ResolveEntity();
					goto IL_305;
				case XmlNodeType.ProcessingInstruction:
					xnode = new XProcessingInstruction(r.Name, r.Value);
					goto IL_305;
				case XmlNodeType.Comment:
					xnode = new XComment(r.Value);
					goto IL_305;
				case XmlNodeType.DocumentType:
					xnode = new XDocumentType(r.LocalName, r.GetAttribute("PUBLIC"), r.GetAttribute("SYSTEM"), r.Value, r.DtdInfo);
					goto IL_305;
				case XmlNodeType.EndElement:
				{
					if (xcontainer.content == null)
					{
						xcontainer.content = string.Empty;
					}
					XElement xelement2 = xcontainer as XElement;
					if (xelement2 != null && xmlLineInfo != null && xmlLineInfo.HasLineInfo())
					{
						xelement2.SetEndElementLineInfo(xmlLineInfo.LineNumber, xmlLineInfo.LinePosition);
					}
					if (xcontainer == this)
					{
						return;
					}
					if (text != null && xcontainer.HasBaseUri)
					{
						text = xcontainer.parent.BaseUri;
					}
					xcontainer = xcontainer.parent;
					goto IL_305;
				}
				case XmlNodeType.EndEntity:
					goto IL_305;
				}
				break;
				IL_305:
				if (xnode != null)
				{
					if (text != null && text != baseURI)
					{
						xnode.SetBaseUri(baseURI);
					}
					if (xmlLineInfo != null && xmlLineInfo.HasLineInfo())
					{
						xnode.SetLineInfo(xmlLineInfo.LineNumber, xmlLineInfo.LinePosition);
					}
					xcontainer.AddNodeSkipNotify(xnode);
					xnode = null;
				}
				if (!r.Read())
				{
					return;
				}
			}
			goto IL_2E1;
			Block_25:
			throw new InvalidOperationException(Res.GetString("InvalidOperation_UnresolvedEntityReference"));
			IL_2E1:
			throw new InvalidOperationException(Res.GetString("InvalidOperation_UnexpectedNodeType", new object[]
			{
				r.NodeType
			}));
		}

		// Token: 0x0600015B RID: 347 RVA: 0x00007008 File Offset: 0x00005208
		internal void RemoveNode(XNode n)
		{
			bool flag = base.NotifyChanging(n, XObjectChangeEventArgs.Remove);
			if (n.parent != this)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_ExternalCode"));
			}
			XNode xnode = (XNode)this.content;
			while (xnode.next != n)
			{
				xnode = xnode.next;
			}
			if (xnode == n)
			{
				this.content = null;
			}
			else
			{
				if (this.content == n)
				{
					this.content = xnode;
				}
				xnode.next = n.next;
			}
			n.parent = null;
			n.next = null;
			if (flag)
			{
				base.NotifyChanged(n, XObjectChangeEventArgs.Remove);
			}
		}

		// Token: 0x0600015C RID: 348 RVA: 0x000070A0 File Offset: 0x000052A0
		private void RemoveNodesSkipNotify()
		{
			XNode xnode = this.content as XNode;
			if (xnode != null)
			{
				do
				{
					XNode next = xnode.next;
					xnode.parent = null;
					xnode.next = null;
					xnode = next;
				}
				while (xnode != this.content);
			}
			this.content = null;
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00004D15 File Offset: 0x00002F15
		internal virtual void ValidateNode(XNode node, XNode previous)
		{
		}

		// Token: 0x0600015E RID: 350 RVA: 0x00004D15 File Offset: 0x00002F15
		internal virtual void ValidateString(string s)
		{
		}

		// Token: 0x0600015F RID: 351 RVA: 0x000070E4 File Offset: 0x000052E4
		internal void WriteContentTo(XmlWriter writer)
		{
			if (this.content != null)
			{
				if (this.content is string)
				{
					if (this is XDocument)
					{
						writer.WriteWhitespace((string)this.content);
						return;
					}
					writer.WriteString((string)this.content);
					return;
				}
				else
				{
					XNode xnode = (XNode)this.content;
					do
					{
						xnode = xnode.next;
						xnode.WriteTo(writer);
					}
					while (xnode != this.content);
				}
			}
		}

		// Token: 0x06000160 RID: 352 RVA: 0x00007158 File Offset: 0x00005358
		private static void AddContentToList(List<object> list, object content)
		{
			IEnumerable enumerable = (content is string) ? null : (content as IEnumerable);
			if (enumerable == null)
			{
				list.Add(content);
				return;
			}
			foreach (object obj in enumerable)
			{
				if (obj != null)
				{
					XContainer.AddContentToList(list, obj);
				}
			}
		}

		// Token: 0x06000161 RID: 353 RVA: 0x000071C8 File Offset: 0x000053C8
		internal static object GetContentSnapshot(object content)
		{
			if (content is string || !(content is IEnumerable))
			{
				return content;
			}
			List<object> list = new List<object>();
			XContainer.AddContentToList(list, content);
			return list;
		}

		// Token: 0x0400007A RID: 122
		internal object content;

		// Token: 0x02000024 RID: 36
		[CompilerGenerated]
		private sealed class <Nodes>d__18 : IEnumerable<XNode>, IEnumerable, IEnumerator<XNode>, IDisposable, IEnumerator
		{
			// Token: 0x06000162 RID: 354 RVA: 0x000071E8 File Offset: 0x000053E8
			[DebuggerHidden]
			public <Nodes>d__18(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000163 RID: 355 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000164 RID: 356 RVA: 0x00007204 File Offset: 0x00005404
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XContainer xcontainer = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					if (i.parent != xcontainer || i == xcontainer.content)
					{
						return false;
					}
				}
				else
				{
					this.<>1__state = -1;
					i = xcontainer.LastNode;
					if (i == null)
					{
						return false;
					}
				}
				i = i.next;
				this.<>2__current = i;
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000037 RID: 55
			// (get) Token: 0x06000165 RID: 357 RVA: 0x0000728D File Offset: 0x0000548D
			XNode IEnumerator<XNode>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000166 RID: 358 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000038 RID: 56
			// (get) Token: 0x06000167 RID: 359 RVA: 0x0000728D File Offset: 0x0000548D
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000168 RID: 360 RVA: 0x00007298 File Offset: 0x00005498
			[DebuggerHidden]
			IEnumerator<XNode> IEnumerable<XNode>.GetEnumerator()
			{
				XContainer.<Nodes>d__18 <Nodes>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<Nodes>d__ = this;
				}
				else
				{
					<Nodes>d__ = new XContainer.<Nodes>d__18(0);
					<Nodes>d__.<>4__this = this;
				}
				return <Nodes>d__;
			}

			// Token: 0x06000169 RID: 361 RVA: 0x000072DB File Offset: 0x000054DB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XNode>.GetEnumerator();
			}

			// Token: 0x0400007B RID: 123
			private int <>1__state;

			// Token: 0x0400007C RID: 124
			private XNode <>2__current;

			// Token: 0x0400007D RID: 125
			private int <>l__initialThreadId;

			// Token: 0x0400007E RID: 126
			public XContainer <>4__this;

			// Token: 0x0400007F RID: 127
			private XNode <n>5__1;
		}

		// Token: 0x02000025 RID: 37
		[CompilerGenerated]
		private sealed class <GetDescendantNodes>d__38 : IEnumerable<XNode>, IEnumerable, IEnumerator<XNode>, IDisposable, IEnumerator
		{
			// Token: 0x0600016A RID: 362 RVA: 0x000072E3 File Offset: 0x000054E3
			[DebuggerHidden]
			public <GetDescendantNodes>d__38(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600016B RID: 363 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x0600016C RID: 364 RVA: 0x00007300 File Offset: 0x00005500
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XContainer xcontainer = this;
				switch (num)
				{
				case 0:
					this.<>1__state = -1;
					if (self)
					{
						this.<>2__current = xcontainer;
						this.<>1__state = 1;
						return true;
					}
					break;
				case 1:
					this.<>1__state = -1;
					break;
				case 2:
					this.<>1__state = -1;
					goto IL_4F;
				default:
					return false;
				}
				i = xcontainer;
				IL_4F:
				XContainer xcontainer2 = i as XContainer;
				XNode firstNode;
				if (xcontainer2 != null && (firstNode = xcontainer2.FirstNode) != null)
				{
					i = firstNode;
				}
				else
				{
					while (i != null && i != xcontainer && i == i.parent.content)
					{
						i = i.parent;
					}
					if (i == null || i == xcontainer)
					{
						return false;
					}
					i = i.next;
				}
				this.<>2__current = i;
				this.<>1__state = 2;
				return true;
			}

			// Token: 0x17000039 RID: 57
			// (get) Token: 0x0600016D RID: 365 RVA: 0x000073FC File Offset: 0x000055FC
			XNode IEnumerator<XNode>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600016E RID: 366 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700003A RID: 58
			// (get) Token: 0x0600016F RID: 367 RVA: 0x000073FC File Offset: 0x000055FC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000170 RID: 368 RVA: 0x00007404 File Offset: 0x00005604
			[DebuggerHidden]
			IEnumerator<XNode> IEnumerable<XNode>.GetEnumerator()
			{
				XContainer.<GetDescendantNodes>d__38 <GetDescendantNodes>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetDescendantNodes>d__ = this;
				}
				else
				{
					<GetDescendantNodes>d__ = new XContainer.<GetDescendantNodes>d__38(0);
					<GetDescendantNodes>d__.<>4__this = this;
				}
				<GetDescendantNodes>d__.self = self;
				return <GetDescendantNodes>d__;
			}

			// Token: 0x06000171 RID: 369 RVA: 0x00007453 File Offset: 0x00005653
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XNode>.GetEnumerator();
			}

			// Token: 0x04000080 RID: 128
			private int <>1__state;

			// Token: 0x04000081 RID: 129
			private XNode <>2__current;

			// Token: 0x04000082 RID: 130
			private int <>l__initialThreadId;

			// Token: 0x04000083 RID: 131
			private bool self;

			// Token: 0x04000084 RID: 132
			public bool <>3__self;

			// Token: 0x04000085 RID: 133
			public XContainer <>4__this;

			// Token: 0x04000086 RID: 134
			private XNode <n>5__1;
		}

		// Token: 0x02000026 RID: 38
		[CompilerGenerated]
		private sealed class <GetDescendants>d__39 : IEnumerable<XElement>, IEnumerable, IEnumerator<XElement>, IDisposable, IEnumerator
		{
			// Token: 0x06000172 RID: 370 RVA: 0x0000745B File Offset: 0x0000565B
			[DebuggerHidden]
			public <GetDescendants>d__39(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000173 RID: 371 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000174 RID: 372 RVA: 0x00007478 File Offset: 0x00005678
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XContainer xcontainer = this;
				switch (num)
				{
				case 0:
					this.<>1__state = -1;
					if (self)
					{
						XElement xelement = (XElement)xcontainer;
						if (name == null || xelement.name == name)
						{
							this.<>2__current = xelement;
							this.<>1__state = 1;
							return true;
						}
					}
					break;
				case 1:
					this.<>1__state = -1;
					break;
				case 2:
					this.<>1__state = -1;
					goto IL_148;
				default:
					return false;
				}
				i = xcontainer;
				XContainer xcontainer2 = xcontainer;
				IL_79:
				if (xcontainer2 != null && xcontainer2.content is XNode)
				{
					i = ((XNode)xcontainer2.content).next;
				}
				else
				{
					while (i != xcontainer && i == i.parent.content)
					{
						i = i.parent;
					}
					if (i == xcontainer)
					{
						return false;
					}
					i = i.next;
				}
				e = (i as XElement);
				if (e != null && (name == null || e.name == name))
				{
					this.<>2__current = e;
					this.<>1__state = 2;
					return true;
				}
				IL_148:
				xcontainer2 = e;
				e = null;
				goto IL_79;
			}

			// Token: 0x1700003B RID: 59
			// (get) Token: 0x06000175 RID: 373 RVA: 0x000075E1 File Offset: 0x000057E1
			XElement IEnumerator<XElement>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000176 RID: 374 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700003C RID: 60
			// (get) Token: 0x06000177 RID: 375 RVA: 0x000075E1 File Offset: 0x000057E1
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000178 RID: 376 RVA: 0x000075EC File Offset: 0x000057EC
			[DebuggerHidden]
			IEnumerator<XElement> IEnumerable<XElement>.GetEnumerator()
			{
				XContainer.<GetDescendants>d__39 <GetDescendants>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetDescendants>d__ = this;
				}
				else
				{
					<GetDescendants>d__ = new XContainer.<GetDescendants>d__39(0);
					<GetDescendants>d__.<>4__this = this;
				}
				<GetDescendants>d__.name = name;
				<GetDescendants>d__.self = self;
				return <GetDescendants>d__;
			}

			// Token: 0x06000179 RID: 377 RVA: 0x00007647 File Offset: 0x00005847
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator();
			}

			// Token: 0x04000087 RID: 135
			private int <>1__state;

			// Token: 0x04000088 RID: 136
			private XElement <>2__current;

			// Token: 0x04000089 RID: 137
			private int <>l__initialThreadId;

			// Token: 0x0400008A RID: 138
			private bool self;

			// Token: 0x0400008B RID: 139
			public bool <>3__self;

			// Token: 0x0400008C RID: 140
			public XContainer <>4__this;

			// Token: 0x0400008D RID: 141
			private XName name;

			// Token: 0x0400008E RID: 142
			public XName <>3__name;

			// Token: 0x0400008F RID: 143
			private XNode <n>5__1;

			// Token: 0x04000090 RID: 144
			private XElement <e>5__2;
		}

		// Token: 0x02000027 RID: 39
		[CompilerGenerated]
		private sealed class <GetElements>d__40 : IEnumerable<XElement>, IEnumerable, IEnumerator<XElement>, IDisposable, IEnumerator
		{
			// Token: 0x0600017A RID: 378 RVA: 0x0000764F File Offset: 0x0000584F
			[DebuggerHidden]
			public <GetElements>d__40(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600017B RID: 379 RVA: 0x00004D15 File Offset: 0x00002F15
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x0600017C RID: 380 RVA: 0x0000766C File Offset: 0x0000586C
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				XContainer xcontainer = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					goto IL_8F;
				}
				else
				{
					this.<>1__state = -1;
					i = (xcontainer.content as XNode);
					if (i == null)
					{
						return false;
					}
				}
				IL_37:
				i = i.next;
				XElement xelement = i as XElement;
				if (xelement != null && (name == null || xelement.name == name))
				{
					this.<>2__current = xelement;
					this.<>1__state = 1;
					return true;
				}
				IL_8F:
				if (i.parent == xcontainer && i != xcontainer.content)
				{
					goto IL_37;
				}
				return false;
			}

			// Token: 0x1700003D RID: 61
			// (get) Token: 0x0600017D RID: 381 RVA: 0x00007725 File Offset: 0x00005925
			XElement IEnumerator<XElement>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600017E RID: 382 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700003E RID: 62
			// (get) Token: 0x0600017F RID: 383 RVA: 0x00007725 File Offset: 0x00005925
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000180 RID: 384 RVA: 0x00007730 File Offset: 0x00005930
			[DebuggerHidden]
			IEnumerator<XElement> IEnumerable<XElement>.GetEnumerator()
			{
				XContainer.<GetElements>d__40 <GetElements>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetElements>d__ = this;
				}
				else
				{
					<GetElements>d__ = new XContainer.<GetElements>d__40(0);
					<GetElements>d__.<>4__this = this;
				}
				<GetElements>d__.name = name;
				return <GetElements>d__;
			}

			// Token: 0x06000181 RID: 385 RVA: 0x0000777F File Offset: 0x0000597F
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator();
			}

			// Token: 0x04000091 RID: 145
			private int <>1__state;

			// Token: 0x04000092 RID: 146
			private XElement <>2__current;

			// Token: 0x04000093 RID: 147
			private int <>l__initialThreadId;

			// Token: 0x04000094 RID: 148
			public XContainer <>4__this;

			// Token: 0x04000095 RID: 149
			private XNode <n>5__1;

			// Token: 0x04000096 RID: 150
			private XName name;

			// Token: 0x04000097 RID: 151
			public XName <>3__name;
		}
	}
}
