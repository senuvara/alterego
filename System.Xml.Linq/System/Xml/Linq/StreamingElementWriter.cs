﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Xml.Linq
{
	// Token: 0x02000039 RID: 57
	internal struct StreamingElementWriter
	{
		// Token: 0x06000297 RID: 663 RVA: 0x0000ABE8 File Offset: 0x00008DE8
		public StreamingElementWriter(XmlWriter w)
		{
			this.writer = w;
			this.element = null;
			this.attributes = new List<XAttribute>();
			this.resolver = default(NamespaceResolver);
		}

		// Token: 0x06000298 RID: 664 RVA: 0x0000AC10 File Offset: 0x00008E10
		private void FlushElement()
		{
			if (this.element != null)
			{
				this.PushElement();
				XNamespace @namespace = this.element.Name.Namespace;
				this.writer.WriteStartElement(this.GetPrefixOfNamespace(@namespace, true), this.element.Name.LocalName, @namespace.NamespaceName);
				foreach (XAttribute xattribute in this.attributes)
				{
					@namespace = xattribute.Name.Namespace;
					string localName = xattribute.Name.LocalName;
					string namespaceName = @namespace.NamespaceName;
					this.writer.WriteAttributeString(this.GetPrefixOfNamespace(@namespace, false), localName, (namespaceName.Length == 0 && localName == "xmlns") ? "http://www.w3.org/2000/xmlns/" : namespaceName, xattribute.Value);
				}
				this.element = null;
				this.attributes.Clear();
			}
		}

		// Token: 0x06000299 RID: 665 RVA: 0x0000AD14 File Offset: 0x00008F14
		private string GetPrefixOfNamespace(XNamespace ns, bool allowDefaultNamespace)
		{
			string namespaceName = ns.NamespaceName;
			if (namespaceName.Length == 0)
			{
				return string.Empty;
			}
			string prefixOfNamespace = this.resolver.GetPrefixOfNamespace(ns, allowDefaultNamespace);
			if (prefixOfNamespace != null)
			{
				return prefixOfNamespace;
			}
			if (namespaceName == "http://www.w3.org/XML/1998/namespace")
			{
				return "xml";
			}
			if (namespaceName == "http://www.w3.org/2000/xmlns/")
			{
				return "xmlns";
			}
			return null;
		}

		// Token: 0x0600029A RID: 666 RVA: 0x0000AD68 File Offset: 0x00008F68
		private void PushElement()
		{
			this.resolver.PushScope();
			foreach (XAttribute xattribute in this.attributes)
			{
				if (xattribute.IsNamespaceDeclaration)
				{
					this.resolver.Add((xattribute.Name.NamespaceName.Length == 0) ? string.Empty : xattribute.Name.LocalName, XNamespace.Get(xattribute.Value));
				}
			}
		}

		// Token: 0x0600029B RID: 667 RVA: 0x0000AE04 File Offset: 0x00009004
		private void Write(object content)
		{
			if (content == null)
			{
				return;
			}
			XNode xnode = content as XNode;
			if (xnode != null)
			{
				this.WriteNode(xnode);
				return;
			}
			string text = content as string;
			if (text != null)
			{
				this.WriteString(text);
				return;
			}
			XAttribute xattribute = content as XAttribute;
			if (xattribute != null)
			{
				this.WriteAttribute(xattribute);
				return;
			}
			XStreamingElement xstreamingElement = content as XStreamingElement;
			if (xstreamingElement != null)
			{
				this.WriteStreamingElement(xstreamingElement);
				return;
			}
			object[] array = content as object[];
			if (array != null)
			{
				foreach (object content2 in array)
				{
					this.Write(content2);
				}
				return;
			}
			IEnumerable enumerable = content as IEnumerable;
			if (enumerable != null)
			{
				foreach (object content3 in enumerable)
				{
					this.Write(content3);
				}
				return;
			}
			this.WriteString(XContainer.GetStringValue(content));
		}

		// Token: 0x0600029C RID: 668 RVA: 0x0000AEF8 File Offset: 0x000090F8
		private void WriteAttribute(XAttribute a)
		{
			if (this.element == null)
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_WriteAttribute"));
			}
			this.attributes.Add(a);
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0000AF1E File Offset: 0x0000911E
		private void WriteNode(XNode n)
		{
			this.FlushElement();
			n.WriteTo(this.writer);
		}

		// Token: 0x0600029E RID: 670 RVA: 0x0000AF34 File Offset: 0x00009134
		internal void WriteStreamingElement(XStreamingElement e)
		{
			this.FlushElement();
			this.element = e;
			this.Write(e.content);
			bool flag = this.element == null;
			this.FlushElement();
			if (flag)
			{
				this.writer.WriteFullEndElement();
			}
			else
			{
				this.writer.WriteEndElement();
			}
			this.resolver.PopScope();
		}

		// Token: 0x0600029F RID: 671 RVA: 0x0000AF8E File Offset: 0x0000918E
		private void WriteString(string s)
		{
			this.FlushElement();
			this.writer.WriteString(s);
		}

		// Token: 0x040000CE RID: 206
		private XmlWriter writer;

		// Token: 0x040000CF RID: 207
		private XStreamingElement element;

		// Token: 0x040000D0 RID: 208
		private List<XAttribute> attributes;

		// Token: 0x040000D1 RID: 209
		private NamespaceResolver resolver;
	}
}
