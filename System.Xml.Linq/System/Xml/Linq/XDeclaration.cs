﻿using System;
using System.Text;

namespace System.Xml.Linq
{
	/// <summary>Represents an XML declaration.</summary>
	// Token: 0x02000035 RID: 53
	public class XDeclaration
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XDeclaration" /> class with the specified version, encoding, and standalone status.</summary>
		/// <param name="version">The version of the XML, usually "1.0".</param>
		/// <param name="encoding">The encoding for the XML document.</param>
		/// <param name="standalone">A string containing "yes" or "no" that specifies whether the XML is standalone or requires external entities to be resolved.</param>
		// Token: 0x06000240 RID: 576 RVA: 0x00009D3F File Offset: 0x00007F3F
		public XDeclaration(string version, string encoding, string standalone)
		{
			this.version = version;
			this.encoding = encoding;
			this.standalone = standalone;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XDeclaration" /> class from another <see cref="T:System.Xml.Linq.XDeclaration" /> object. </summary>
		/// <param name="other">The <see cref="T:System.Xml.Linq.XDeclaration" /> used to initialize this <see cref="T:System.Xml.Linq.XDeclaration" /> object.</param>
		// Token: 0x06000241 RID: 577 RVA: 0x00009D5C File Offset: 0x00007F5C
		public XDeclaration(XDeclaration other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			this.version = other.version;
			this.encoding = other.encoding;
			this.standalone = other.standalone;
		}

		// Token: 0x06000242 RID: 578 RVA: 0x00009D98 File Offset: 0x00007F98
		internal XDeclaration(XmlReader r)
		{
			this.version = r.GetAttribute("version");
			this.encoding = r.GetAttribute("encoding");
			this.standalone = r.GetAttribute("standalone");
			r.Read();
		}

		/// <summary>Gets or sets the encoding for this document.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the code page name for this document.</returns>
		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000243 RID: 579 RVA: 0x00009DE5 File Offset: 0x00007FE5
		// (set) Token: 0x06000244 RID: 580 RVA: 0x00009DED File Offset: 0x00007FED
		public string Encoding
		{
			get
			{
				return this.encoding;
			}
			set
			{
				this.encoding = value;
			}
		}

		/// <summary>Gets or sets the standalone property for this document.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the standalone property for this document.</returns>
		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000245 RID: 581 RVA: 0x00009DF6 File Offset: 0x00007FF6
		// (set) Token: 0x06000246 RID: 582 RVA: 0x00009DFE File Offset: 0x00007FFE
		public string Standalone
		{
			get
			{
				return this.standalone;
			}
			set
			{
				this.standalone = value;
			}
		}

		/// <summary>Gets or sets the version property for this document.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the version property for this document.</returns>
		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000247 RID: 583 RVA: 0x00009E07 File Offset: 0x00008007
		// (set) Token: 0x06000248 RID: 584 RVA: 0x00009E0F File Offset: 0x0000800F
		public string Version
		{
			get
			{
				return this.version;
			}
			set
			{
				this.version = value;
			}
		}

		/// <summary>Provides the declaration as a formatted string.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the formatted XML string.</returns>
		// Token: 0x06000249 RID: 585 RVA: 0x00009E18 File Offset: 0x00008018
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder("<?xml");
			if (this.version != null)
			{
				stringBuilder.Append(" version=\"");
				stringBuilder.Append(this.version);
				stringBuilder.Append("\"");
			}
			if (this.encoding != null)
			{
				stringBuilder.Append(" encoding=\"");
				stringBuilder.Append(this.encoding);
				stringBuilder.Append("\"");
			}
			if (this.standalone != null)
			{
				stringBuilder.Append(" standalone=\"");
				stringBuilder.Append(this.standalone);
				stringBuilder.Append("\"");
			}
			stringBuilder.Append("?>");
			return stringBuilder.ToString();
		}

		// Token: 0x040000C0 RID: 192
		private string version;

		// Token: 0x040000C1 RID: 193
		private string encoding;

		// Token: 0x040000C2 RID: 194
		private string standalone;
	}
}
