﻿using System;

namespace System.Xml.Linq
{
	// Token: 0x02000015 RID: 21
	internal class LineInfoEndElementAnnotation : LineInfoAnnotation
	{
		// Token: 0x060000C4 RID: 196 RVA: 0x00005010 File Offset: 0x00003210
		public LineInfoEndElementAnnotation(int lineNumber, int linePosition) : base(lineNumber, linePosition)
		{
		}
	}
}
