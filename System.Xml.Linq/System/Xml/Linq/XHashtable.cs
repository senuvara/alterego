﻿using System;
using System.Threading;

namespace System.Xml.Linq
{
	// Token: 0x0200000C RID: 12
	internal sealed class XHashtable<TValue>
	{
		// Token: 0x0600008A RID: 138 RVA: 0x00004262 File Offset: 0x00002462
		public XHashtable(XHashtable<TValue>.ExtractKeyDelegate extractKey, int capacity)
		{
			this.state = new XHashtable<TValue>.XHashtableState(extractKey, capacity);
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00004277 File Offset: 0x00002477
		public bool TryGetValue(string key, int index, int count, out TValue value)
		{
			return this.state.TryGetValue(key, index, count, out value);
		}

		// Token: 0x0600008C RID: 140 RVA: 0x0000428C File Offset: 0x0000248C
		public TValue Add(TValue value)
		{
			TValue result;
			while (!this.state.TryAdd(value, out result))
			{
				lock (this)
				{
					XHashtable<TValue>.XHashtableState xhashtableState = this.state.Resize();
					Thread.MemoryBarrier();
					this.state = xhashtableState;
				}
			}
			return result;
		}

		// Token: 0x0400002B RID: 43
		private XHashtable<TValue>.XHashtableState state;

		// Token: 0x0400002C RID: 44
		private const int StartingHash = 352654597;

		// Token: 0x0200000D RID: 13
		// (Invoke) Token: 0x0600008E RID: 142
		public delegate string ExtractKeyDelegate(TValue value);

		// Token: 0x0200000E RID: 14
		private sealed class XHashtableState
		{
			// Token: 0x06000091 RID: 145 RVA: 0x000042EC File Offset: 0x000024EC
			public XHashtableState(XHashtable<TValue>.ExtractKeyDelegate extractKey, int capacity)
			{
				this.buckets = new int[capacity];
				this.entries = new XHashtable<TValue>.XHashtableState.Entry[capacity];
				this.extractKey = extractKey;
			}

			// Token: 0x06000092 RID: 146 RVA: 0x00004314 File Offset: 0x00002514
			public XHashtable<TValue>.XHashtableState Resize()
			{
				if (this.numEntries < this.buckets.Length)
				{
					return this;
				}
				int num = 0;
				for (int i = 0; i < this.buckets.Length; i++)
				{
					int j = this.buckets[i];
					if (j == 0)
					{
						j = Interlocked.CompareExchange(ref this.buckets[i], -1, 0);
					}
					while (j > 0)
					{
						if (this.extractKey(this.entries[j].Value) != null)
						{
							num++;
						}
						if (this.entries[j].Next == 0)
						{
							j = Interlocked.CompareExchange(ref this.entries[j].Next, -1, 0);
						}
						else
						{
							j = this.entries[j].Next;
						}
					}
				}
				if (num < this.buckets.Length / 2)
				{
					num = this.buckets.Length;
				}
				else
				{
					num = this.buckets.Length * 2;
					if (num < 0)
					{
						throw new OverflowException();
					}
				}
				XHashtable<TValue>.XHashtableState xhashtableState = new XHashtable<TValue>.XHashtableState(this.extractKey, num);
				for (int k = 0; k < this.buckets.Length; k++)
				{
					for (int l = this.buckets[k]; l > 0; l = this.entries[l].Next)
					{
						TValue tvalue;
						xhashtableState.TryAdd(this.entries[l].Value, out tvalue);
					}
				}
				return xhashtableState;
			}

			// Token: 0x06000093 RID: 147 RVA: 0x0000446C File Offset: 0x0000266C
			public bool TryGetValue(string key, int index, int count, out TValue value)
			{
				int hashCode = XHashtable<TValue>.XHashtableState.ComputeHashCode(key, index, count);
				int num = 0;
				if (this.FindEntry(hashCode, key, index, count, ref num))
				{
					value = this.entries[num].Value;
					return true;
				}
				value = default(TValue);
				return false;
			}

			// Token: 0x06000094 RID: 148 RVA: 0x000044B8 File Offset: 0x000026B8
			public bool TryAdd(TValue value, out TValue newValue)
			{
				newValue = value;
				string text = this.extractKey(value);
				if (text == null)
				{
					return true;
				}
				int num = XHashtable<TValue>.XHashtableState.ComputeHashCode(text, 0, text.Length);
				int num2 = Interlocked.Increment(ref this.numEntries);
				if (num2 < 0 || num2 >= this.buckets.Length)
				{
					return false;
				}
				this.entries[num2].Value = value;
				this.entries[num2].HashCode = num;
				Thread.MemoryBarrier();
				int num3 = 0;
				while (!this.FindEntry(num, text, 0, text.Length, ref num3))
				{
					if (num3 == 0)
					{
						num3 = Interlocked.CompareExchange(ref this.buckets[num & this.buckets.Length - 1], num2, 0);
					}
					else
					{
						num3 = Interlocked.CompareExchange(ref this.entries[num3].Next, num2, 0);
					}
					if (num3 <= 0)
					{
						return num3 == 0;
					}
				}
				newValue = this.entries[num3].Value;
				return true;
			}

			// Token: 0x06000095 RID: 149 RVA: 0x000045A8 File Offset: 0x000027A8
			private bool FindEntry(int hashCode, string key, int index, int count, ref int entryIndex)
			{
				int num = entryIndex;
				int i;
				if (num == 0)
				{
					i = this.buckets[hashCode & this.buckets.Length - 1];
				}
				else
				{
					i = num;
				}
				while (i > 0)
				{
					if (this.entries[i].HashCode == hashCode)
					{
						string text = this.extractKey(this.entries[i].Value);
						if (text == null)
						{
							if (this.entries[i].Next > 0)
							{
								this.entries[i].Value = default(TValue);
								i = this.entries[i].Next;
								if (num == 0)
								{
									this.buckets[hashCode & this.buckets.Length - 1] = i;
									continue;
								}
								this.entries[num].Next = i;
								continue;
							}
						}
						else if (count == text.Length && string.CompareOrdinal(key, index, text, 0, count) == 0)
						{
							entryIndex = i;
							return true;
						}
					}
					num = i;
					i = this.entries[i].Next;
				}
				entryIndex = num;
				return false;
			}

			// Token: 0x06000096 RID: 150 RVA: 0x000046BC File Offset: 0x000028BC
			private static int ComputeHashCode(string key, int index, int count)
			{
				int num = 352654597;
				int num2 = index + count;
				for (int i = index; i < num2; i++)
				{
					num += (num << 7 ^ (int)key[i]);
				}
				num -= num >> 17;
				num -= num >> 11;
				num -= num >> 5;
				return num & int.MaxValue;
			}

			// Token: 0x0400002D RID: 45
			private int[] buckets;

			// Token: 0x0400002E RID: 46
			private XHashtable<TValue>.XHashtableState.Entry[] entries;

			// Token: 0x0400002F RID: 47
			private int numEntries;

			// Token: 0x04000030 RID: 48
			private XHashtable<TValue>.ExtractKeyDelegate extractKey;

			// Token: 0x04000031 RID: 49
			private const int EndOfList = 0;

			// Token: 0x04000032 RID: 50
			private const int FullList = -1;

			// Token: 0x0200000F RID: 15
			private struct Entry
			{
				// Token: 0x04000033 RID: 51
				public TValue Value;

				// Token: 0x04000034 RID: 52
				public int HashCode;

				// Token: 0x04000035 RID: 53
				public int Next;
			}
		}
	}
}
