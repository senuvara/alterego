﻿using System;
using System.Text;

namespace System.Xml.Linq
{
	/// <summary>Represents a text node. </summary>
	// Token: 0x02000021 RID: 33
	public class XText : XNode
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XText" /> class. </summary>
		/// <param name="value">The <see cref="T:System.String" /> that contains the value of the <see cref="T:System.Xml.Linq.XText" /> node.</param>
		// Token: 0x06000121 RID: 289 RVA: 0x00005DBD File Offset: 0x00003FBD
		public XText(string value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			this.text = value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XText" /> class from another <see cref="T:System.Xml.Linq.XText" /> object.</summary>
		/// <param name="other">The <see cref="T:System.Xml.Linq.XText" /> node to copy from.</param>
		// Token: 0x06000122 RID: 290 RVA: 0x00005DDA File Offset: 0x00003FDA
		public XText(XText other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			this.text = other.text;
		}

		// Token: 0x06000123 RID: 291 RVA: 0x00005DFC File Offset: 0x00003FFC
		internal XText(XmlReader r)
		{
			this.text = r.Value;
			r.Read();
		}

		/// <summary>Gets the node type for this node.</summary>
		/// <returns>The node type. For <see cref="T:System.Xml.Linq.XText" /> objects, this value is <see cref="F:System.Xml.XmlNodeType.Text" />.</returns>
		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000124 RID: 292 RVA: 0x00005E17 File Offset: 0x00004017
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.Text;
			}
		}

		/// <summary>Gets or sets the value of this node.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the value of this node.</returns>
		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000125 RID: 293 RVA: 0x00005E1A File Offset: 0x0000401A
		// (set) Token: 0x06000126 RID: 294 RVA: 0x00005E22 File Offset: 0x00004022
		public string Value
		{
			get
			{
				return this.text;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				bool flag = base.NotifyChanging(this, XObjectChangeEventArgs.Value);
				this.text = value;
				if (flag)
				{
					base.NotifyChanged(this, XObjectChangeEventArgs.Value);
				}
			}
		}

		/// <summary>Writes this node to an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> into which this method will write.</param>
		// Token: 0x06000127 RID: 295 RVA: 0x00005E54 File Offset: 0x00004054
		public override void WriteTo(XmlWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			if (this.parent is XDocument)
			{
				writer.WriteWhitespace(this.text);
				return;
			}
			writer.WriteString(this.text);
		}

		// Token: 0x06000128 RID: 296 RVA: 0x00005E8A File Offset: 0x0000408A
		internal override void AppendText(StringBuilder sb)
		{
			sb.Append(this.text);
		}

		// Token: 0x06000129 RID: 297 RVA: 0x00005E99 File Offset: 0x00004099
		internal override XNode CloneNode()
		{
			return new XText(this);
		}

		// Token: 0x0600012A RID: 298 RVA: 0x00005EA1 File Offset: 0x000040A1
		internal override bool DeepEquals(XNode node)
		{
			return node != null && this.NodeType == node.NodeType && this.text == ((XText)node).text;
		}

		// Token: 0x0600012B RID: 299 RVA: 0x00005ECC File Offset: 0x000040CC
		internal override int GetDeepHashCode()
		{
			return this.text.GetHashCode();
		}

		// Token: 0x04000079 RID: 121
		internal string text;
	}
}
