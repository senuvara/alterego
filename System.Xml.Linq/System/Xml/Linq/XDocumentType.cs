﻿using System;

namespace System.Xml.Linq
{
	/// <summary>Represents an XML Document Type Definition (DTD). </summary>
	// Token: 0x02000036 RID: 54
	public class XDocumentType : XNode
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Xml.Linq.XDocumentType" /> class. </summary>
		/// <param name="name">A <see cref="T:System.String" /> that contains the qualified name of the DTD, which is the same as the qualified name of the root element of the XML document.</param>
		/// <param name="publicId">A <see cref="T:System.String" /> that contains the public identifier of an external public DTD.</param>
		/// <param name="systemId">A <see cref="T:System.String" /> that contains the system identifier of an external private DTD.</param>
		/// <param name="internalSubset">A <see cref="T:System.String" /> that contains the internal subset for an internal DTD.</param>
		// Token: 0x0600024A RID: 586 RVA: 0x00009EC9 File Offset: 0x000080C9
		public XDocumentType(string name, string publicId, string systemId, string internalSubset)
		{
			this.name = XmlConvert.VerifyName(name);
			this.publicId = publicId;
			this.systemId = systemId;
			this.internalSubset = internalSubset;
		}

		/// <summary>Initializes an instance of the <see cref="T:System.Xml.Linq.XDocumentType" /> class from another <see cref="T:System.Xml.Linq.XDocumentType" /> object.</summary>
		/// <param name="other">An <see cref="T:System.Xml.Linq.XDocumentType" /> object to copy from.</param>
		// Token: 0x0600024B RID: 587 RVA: 0x00009EF4 File Offset: 0x000080F4
		public XDocumentType(XDocumentType other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			this.name = other.name;
			this.publicId = other.publicId;
			this.systemId = other.systemId;
			this.internalSubset = other.internalSubset;
			this.dtdInfo = other.dtdInfo;
		}

		// Token: 0x0600024C RID: 588 RVA: 0x00009F54 File Offset: 0x00008154
		internal XDocumentType(XmlReader r)
		{
			this.name = r.Name;
			this.publicId = r.GetAttribute("PUBLIC");
			this.systemId = r.GetAttribute("SYSTEM");
			this.internalSubset = r.Value;
			this.dtdInfo = r.DtdInfo;
			r.Read();
		}

		// Token: 0x0600024D RID: 589 RVA: 0x00009FB4 File Offset: 0x000081B4
		internal XDocumentType(string name, string publicId, string systemId, string internalSubset, IDtdInfo dtdInfo) : this(name, publicId, systemId, internalSubset)
		{
			this.dtdInfo = dtdInfo;
		}

		/// <summary>Gets or sets the internal subset for this Document Type Definition (DTD).</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the internal subset for this Document Type Definition (DTD).</returns>
		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600024E RID: 590 RVA: 0x00009FC9 File Offset: 0x000081C9
		// (set) Token: 0x0600024F RID: 591 RVA: 0x00009FD1 File Offset: 0x000081D1
		public string InternalSubset
		{
			get
			{
				return this.internalSubset;
			}
			set
			{
				bool flag = base.NotifyChanging(this, XObjectChangeEventArgs.Value);
				this.internalSubset = value;
				if (flag)
				{
					base.NotifyChanged(this, XObjectChangeEventArgs.Value);
				}
			}
		}

		/// <summary>Gets or sets the name for this Document Type Definition (DTD).</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the name for this Document Type Definition (DTD).</returns>
		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000250 RID: 592 RVA: 0x00009FF5 File Offset: 0x000081F5
		// (set) Token: 0x06000251 RID: 593 RVA: 0x00009FFD File Offset: 0x000081FD
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				value = XmlConvert.VerifyName(value);
				bool flag = base.NotifyChanging(this, XObjectChangeEventArgs.Name);
				this.name = value;
				if (flag)
				{
					base.NotifyChanged(this, XObjectChangeEventArgs.Name);
				}
			}
		}

		/// <summary>Gets the node type for this node.</summary>
		/// <returns>The node type. For <see cref="T:System.Xml.Linq.XDocumentType" /> objects, this value is <see cref="F:System.Xml.XmlNodeType.DocumentType" />.</returns>
		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000252 RID: 594 RVA: 0x0000A029 File Offset: 0x00008229
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.DocumentType;
			}
		}

		/// <summary>Gets or sets the public identifier for this Document Type Definition (DTD).</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the public identifier for this Document Type Definition (DTD).</returns>
		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000253 RID: 595 RVA: 0x0000A02D File Offset: 0x0000822D
		// (set) Token: 0x06000254 RID: 596 RVA: 0x0000A035 File Offset: 0x00008235
		public string PublicId
		{
			get
			{
				return this.publicId;
			}
			set
			{
				bool flag = base.NotifyChanging(this, XObjectChangeEventArgs.Value);
				this.publicId = value;
				if (flag)
				{
					base.NotifyChanged(this, XObjectChangeEventArgs.Value);
				}
			}
		}

		/// <summary>Gets or sets the system identifier for this Document Type Definition (DTD).</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the system identifier for this Document Type Definition (DTD).</returns>
		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000255 RID: 597 RVA: 0x0000A059 File Offset: 0x00008259
		// (set) Token: 0x06000256 RID: 598 RVA: 0x0000A061 File Offset: 0x00008261
		public string SystemId
		{
			get
			{
				return this.systemId;
			}
			set
			{
				bool flag = base.NotifyChanging(this, XObjectChangeEventArgs.Value);
				this.systemId = value;
				if (flag)
				{
					base.NotifyChanged(this, XObjectChangeEventArgs.Value);
				}
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000257 RID: 599 RVA: 0x0000A085 File Offset: 0x00008285
		internal IDtdInfo DtdInfo
		{
			get
			{
				return this.dtdInfo;
			}
		}

		/// <summary>Write this <see cref="T:System.Xml.Linq.XDocumentType" /> to an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> into which this method will write.</param>
		// Token: 0x06000258 RID: 600 RVA: 0x0000A08D File Offset: 0x0000828D
		public override void WriteTo(XmlWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			writer.WriteDocType(this.name, this.publicId, this.systemId, this.internalSubset);
		}

		// Token: 0x06000259 RID: 601 RVA: 0x0000A0BB File Offset: 0x000082BB
		internal override XNode CloneNode()
		{
			return new XDocumentType(this);
		}

		// Token: 0x0600025A RID: 602 RVA: 0x0000A0C4 File Offset: 0x000082C4
		internal override bool DeepEquals(XNode node)
		{
			XDocumentType xdocumentType = node as XDocumentType;
			return xdocumentType != null && this.name == xdocumentType.name && this.publicId == xdocumentType.publicId && this.systemId == xdocumentType.SystemId && this.internalSubset == xdocumentType.internalSubset;
		}

		// Token: 0x0600025B RID: 603 RVA: 0x0000A128 File Offset: 0x00008328
		internal override int GetDeepHashCode()
		{
			return this.name.GetHashCode() ^ ((this.publicId != null) ? this.publicId.GetHashCode() : 0) ^ ((this.systemId != null) ? this.systemId.GetHashCode() : 0) ^ ((this.internalSubset != null) ? this.internalSubset.GetHashCode() : 0);
		}

		// Token: 0x040000C3 RID: 195
		private string name;

		// Token: 0x040000C4 RID: 196
		private string publicId;

		// Token: 0x040000C5 RID: 197
		private string systemId;

		// Token: 0x040000C6 RID: 198
		private string internalSubset;

		// Token: 0x040000C7 RID: 199
		private IDtdInfo dtdInfo;
	}
}
