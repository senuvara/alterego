﻿using System;
using System.Globalization;

namespace System.Xml.Linq
{
	// Token: 0x02000044 RID: 68
	internal static class Res
	{
		// Token: 0x0600034F RID: 847 RVA: 0x0000D920 File Offset: 0x0000BB20
		public static string GetString(string name)
		{
			uint num = <PrivateImplementationDetails>.ComputeStringHash(name);
			if (num <= 2345166790U)
			{
				if (num <= 626770679U)
				{
					if (num <= 272356109U)
					{
						if (num <= 85233838U)
						{
							if (num != 15713876U)
							{
								if (num == 85233838U)
								{
									if (name == "InvalidOperation_ExpectedInteractive")
									{
										return "The XmlReader state should be Interactive.";
									}
								}
							}
							else if (name == "InvalidOperation_DocumentStructure")
							{
								return "This operation would create an incorrectly structured document.";
							}
						}
						else if (num != 103965186U)
						{
							if (num == 272356109U)
							{
								if (name == "InvalidOperation_UnexpectedNodeType")
								{
									return "The XmlReader should not be on a node of type {0}.";
								}
							}
						}
						else if (name == "InvalidOperation_MissingRoot")
						{
							return "The root element is missing.";
						}
					}
					else if (num <= 569780718U)
					{
						if (num != 407913795U)
						{
							if (num == 569780718U)
							{
								if (name == "Argument_InvalidPrefix")
								{
									return "'{0}' is an invalid prefix.";
								}
							}
						}
						else if (name == "Argument_NamespaceDeclarationPrefixed")
						{
							return "The prefix '{0}' cannot be bound to the empty namespace name.";
						}
					}
					else if (num != 625070019U)
					{
						if (num == 626770679U)
						{
							if (name == "InvalidOperation_UnexpectedEvaluation")
							{
								return "The XPath expression evaluated to unexpected type {0}.";
							}
						}
					}
					else if (name == "InvalidOperation_UnresolvedEntityReference")
					{
						return "The XmlReader cannot resolve entity references.";
					}
				}
				else if (num <= 1639487891U)
				{
					if (num <= 1004902937U)
					{
						if (num != 922706940U)
						{
							if (num == 1004902937U)
							{
								if (name == "InvalidOperation_DuplicateAttribute")
								{
									return "Duplicate attribute.";
								}
							}
						}
						else if (name == "Argument_InvalidExpandedName")
						{
							return "'{0}' is an invalid expanded name.";
						}
					}
					else if (num != 1405029661U)
					{
						if (num == 1639487891U)
						{
							if (name == "Argument_NamespaceDeclarationXml")
							{
								return "The prefix 'xml' is bound to the namespace name 'http://www.w3.org/XML/1998/namespace'. Other prefixes must not be bound to this namespace name, and it must not be declared as the default namespace.";
							}
						}
					}
					else if (name == "NotSupported_CheckValidity")
					{
						return "This XPathNavigator does not support XSD validation.";
					}
				}
				else if (num <= 1755217186U)
				{
					if (num != 1718317927U)
					{
						if (num == 1755217186U)
						{
							if (name == "InvalidOperation_ExpectedNodeType")
							{
								return "The XmlReader must be on a node of type {0} instead of a node of type {1}.";
							}
						}
					}
					else if (name == "Argument_XObjectValue")
					{
						return "An XObject cannot be used as a value.";
					}
				}
				else if (num != 1837980878U)
				{
					if (num == 2345166790U)
					{
						if (name == "Argument_InvalidPIName")
						{
							return "'{0}' is an invalid name for a processing instruction.";
						}
					}
				}
				else if (name == "InvalidOperation_DeserializeInstance")
				{
					return "This instance cannot be deserialized.";
				}
			}
			else if (num <= 3510382528U)
			{
				if (num <= 3280571068U)
				{
					if (num <= 2491089664U)
					{
						if (num != 2437381507U)
						{
							if (num == 2491089664U)
							{
								if (name == "InvalidOperation_ExternalCode")
								{
									return "This operation was corrupted by external code.";
								}
							}
						}
						else if (name == "InvalidOperation_MissingAncestor")
						{
							return "A common ancestor is missing.";
						}
					}
					else if (num != 3172179164U)
					{
						if (num == 3280571068U)
						{
							if (name == "Argument_MustBeDerivedFrom")
							{
								return "The argument must be derived from {0}.";
							}
						}
					}
					else if (name == "Argument_NamespaceDeclarationXmlns")
					{
						return "The prefix 'xmlns' is bound to the namespace name 'http://www.w3.org/2000/xmlns/'. It must not be declared. Other prefixes must not be bound to this namespace name, and it must not be declared as the default namespace.";
					}
				}
				else if (num <= 3430209653U)
				{
					if (num != 3388508910U)
					{
						if (num == 3430209653U)
						{
							if (name == "InvalidOperation_BadNodeType")
							{
								return "This operation is not valid on a node of type {0}.";
							}
						}
					}
					else if (name == "Argument_ConvertToString")
					{
						return "The argument cannot be converted to a string.";
					}
				}
				else if (num != 3431575044U)
				{
					if (num == 3510382528U)
					{
						if (name == "InvalidOperation_ExpectedEndOfFile")
						{
							return "The XmlReader state should be EndOfFile after this operation.";
						}
					}
				}
				else if (name == "Argument_AddNonWhitespace")
				{
					return "Non white space characters cannot be added to content.";
				}
			}
			else if (num <= 3723879466U)
			{
				if (num <= 3594069654U)
				{
					if (num != 3539275280U)
					{
						if (num == 3594069654U)
						{
							if (name == "InvalidOperation_MissingParent")
							{
								return "The parent is missing.";
							}
						}
					}
					else if (name == "NotSupported_MoveToId")
					{
						return "This XPathNavigator does not support IDs.";
					}
				}
				else if (num != 3628001091U)
				{
					if (num == 3723879466U)
					{
						if (name == "Argument_AddNode")
						{
							return "A node of type {0} cannot be added to content.";
						}
					}
				}
				else if (name == "NotSupported_WriteBase64")
				{
					return "This XmlWriter does not support base64 encoded data.";
				}
			}
			else if (num <= 3797174551U)
			{
				if (num != 3734520932U)
				{
					if (num == 3797174551U)
					{
						if (name == "InvalidOperation_WriteAttribute")
						{
							return "An attribute cannot be written after content.";
						}
					}
				}
				else if (name == "Argument_CreateNavigator")
				{
					return "This XPathNavigator cannot be created on a node of type {0}.";
				}
			}
			else if (num != 3802163562U)
			{
				if (num == 3833352336U)
				{
					if (name == "NotSupported_WriteEntityRef")
					{
						return "This XmlWriter does not support entity references.";
					}
				}
			}
			else if (name == "Argument_AddAttribute")
			{
				return "An attribute cannot be added to content.";
			}
			return null;
		}

		// Token: 0x06000350 RID: 848 RVA: 0x0000DEC0 File Offset: 0x0000C0C0
		public static string GetString(string name, params object[] args)
		{
			string @string = Res.GetString(name);
			if (args == null || args.Length == 0)
			{
				return @string;
			}
			return string.Format(CultureInfo.CurrentCulture, @string, args);
		}

		// Token: 0x0400011F RID: 287
		internal const string Argument_AddAttribute = "Argument_AddAttribute";

		// Token: 0x04000120 RID: 288
		internal const string Argument_AddNode = "Argument_AddNode";

		// Token: 0x04000121 RID: 289
		internal const string Argument_AddNonWhitespace = "Argument_AddNonWhitespace";

		// Token: 0x04000122 RID: 290
		internal const string Argument_ConvertToString = "Argument_ConvertToString";

		// Token: 0x04000123 RID: 291
		internal const string Argument_CreateNavigator = "Argument_CreateNavigator";

		// Token: 0x04000124 RID: 292
		internal const string Argument_InvalidExpandedName = "Argument_InvalidExpandedName";

		// Token: 0x04000125 RID: 293
		internal const string Argument_InvalidPIName = "Argument_InvalidPIName";

		// Token: 0x04000126 RID: 294
		internal const string Argument_InvalidPrefix = "Argument_InvalidPrefix";

		// Token: 0x04000127 RID: 295
		internal const string Argument_MustBeDerivedFrom = "Argument_MustBeDerivedFrom";

		// Token: 0x04000128 RID: 296
		internal const string Argument_NamespaceDeclarationPrefixed = "Argument_NamespaceDeclarationPrefixed";

		// Token: 0x04000129 RID: 297
		internal const string Argument_NamespaceDeclarationXml = "Argument_NamespaceDeclarationXml";

		// Token: 0x0400012A RID: 298
		internal const string Argument_NamespaceDeclarationXmlns = "Argument_NamespaceDeclarationXmlns";

		// Token: 0x0400012B RID: 299
		internal const string Argument_XObjectValue = "Argument_XObjectValue";

		// Token: 0x0400012C RID: 300
		internal const string InvalidOperation_BadNodeType = "InvalidOperation_BadNodeType";

		// Token: 0x0400012D RID: 301
		internal const string InvalidOperation_DocumentStructure = "InvalidOperation_DocumentStructure";

		// Token: 0x0400012E RID: 302
		internal const string InvalidOperation_DuplicateAttribute = "InvalidOperation_DuplicateAttribute";

		// Token: 0x0400012F RID: 303
		internal const string InvalidOperation_ExpectedEndOfFile = "InvalidOperation_ExpectedEndOfFile";

		// Token: 0x04000130 RID: 304
		internal const string InvalidOperation_ExpectedInteractive = "InvalidOperation_ExpectedInteractive";

		// Token: 0x04000131 RID: 305
		internal const string InvalidOperation_ExpectedNodeType = "InvalidOperation_ExpectedNodeType";

		// Token: 0x04000132 RID: 306
		internal const string InvalidOperation_ExternalCode = "InvalidOperation_ExternalCode";

		// Token: 0x04000133 RID: 307
		internal const string InvalidOperation_DeserializeInstance = "InvalidOperation_DeserializeInstance";

		// Token: 0x04000134 RID: 308
		internal const string InvalidOperation_MissingAncestor = "InvalidOperation_MissingAncestor";

		// Token: 0x04000135 RID: 309
		internal const string InvalidOperation_MissingParent = "InvalidOperation_MissingParent";

		// Token: 0x04000136 RID: 310
		internal const string InvalidOperation_MissingRoot = "InvalidOperation_MissingRoot";

		// Token: 0x04000137 RID: 311
		internal const string InvalidOperation_UnexpectedEvaluation = "InvalidOperation_UnexpectedEvaluation";

		// Token: 0x04000138 RID: 312
		internal const string InvalidOperation_UnexpectedNodeType = "InvalidOperation_UnexpectedNodeType";

		// Token: 0x04000139 RID: 313
		internal const string InvalidOperation_UnresolvedEntityReference = "InvalidOperation_UnresolvedEntityReference";

		// Token: 0x0400013A RID: 314
		internal const string InvalidOperation_WriteAttribute = "InvalidOperation_WriteAttribute";

		// Token: 0x0400013B RID: 315
		internal const string NotSupported_CheckValidity = "NotSupported_CheckValidity";

		// Token: 0x0400013C RID: 316
		internal const string NotSupported_MoveToId = "NotSupported_MoveToId";

		// Token: 0x0400013D RID: 317
		internal const string NotSupported_WriteBase64 = "NotSupported_WriteBase64";

		// Token: 0x0400013E RID: 318
		internal const string NotSupported_WriteEntityRef = "NotSupported_WriteEntityRef";
	}
}
