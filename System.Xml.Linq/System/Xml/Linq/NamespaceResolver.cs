﻿using System;

namespace System.Xml.Linq
{
	// Token: 0x0200002D RID: 45
	internal struct NamespaceResolver
	{
		// Token: 0x060001FE RID: 510 RVA: 0x0000921C File Offset: 0x0000741C
		public void PushScope()
		{
			this.scope++;
		}

		// Token: 0x060001FF RID: 511 RVA: 0x0000922C File Offset: 0x0000742C
		public void PopScope()
		{
			NamespaceResolver.NamespaceDeclaration prev = this.declaration;
			if (prev != null)
			{
				do
				{
					prev = prev.prev;
					if (prev.scope != this.scope)
					{
						break;
					}
					if (prev == this.declaration)
					{
						this.declaration = null;
					}
					else
					{
						this.declaration.prev = prev.prev;
					}
					this.rover = null;
				}
				while (prev != this.declaration && this.declaration != null);
			}
			this.scope--;
		}

		// Token: 0x06000200 RID: 512 RVA: 0x000092A4 File Offset: 0x000074A4
		public void Add(string prefix, XNamespace ns)
		{
			NamespaceResolver.NamespaceDeclaration namespaceDeclaration = new NamespaceResolver.NamespaceDeclaration();
			namespaceDeclaration.prefix = prefix;
			namespaceDeclaration.ns = ns;
			namespaceDeclaration.scope = this.scope;
			if (this.declaration == null)
			{
				this.declaration = namespaceDeclaration;
			}
			else
			{
				namespaceDeclaration.prev = this.declaration.prev;
			}
			this.declaration.prev = namespaceDeclaration;
			this.rover = null;
		}

		// Token: 0x06000201 RID: 513 RVA: 0x00009308 File Offset: 0x00007508
		public void AddFirst(string prefix, XNamespace ns)
		{
			NamespaceResolver.NamespaceDeclaration namespaceDeclaration = new NamespaceResolver.NamespaceDeclaration();
			namespaceDeclaration.prefix = prefix;
			namespaceDeclaration.ns = ns;
			namespaceDeclaration.scope = this.scope;
			if (this.declaration == null)
			{
				namespaceDeclaration.prev = namespaceDeclaration;
			}
			else
			{
				namespaceDeclaration.prev = this.declaration.prev;
				this.declaration.prev = namespaceDeclaration;
			}
			this.declaration = namespaceDeclaration;
			this.rover = null;
		}

		// Token: 0x06000202 RID: 514 RVA: 0x00009374 File Offset: 0x00007574
		public string GetPrefixOfNamespace(XNamespace ns, bool allowDefaultNamespace)
		{
			if (this.rover != null && this.rover.ns == ns && (allowDefaultNamespace || this.rover.prefix.Length > 0))
			{
				return this.rover.prefix;
			}
			NamespaceResolver.NamespaceDeclaration prev = this.declaration;
			if (prev != null)
			{
				for (;;)
				{
					prev = prev.prev;
					if (prev.ns == ns)
					{
						NamespaceResolver.NamespaceDeclaration prev2 = this.declaration.prev;
						while (prev2 != prev && prev2.prefix != prev.prefix)
						{
							prev2 = prev2.prev;
						}
						if (prev2 == prev)
						{
							if (allowDefaultNamespace)
							{
								break;
							}
							if (prev.prefix.Length > 0)
							{
								goto Block_8;
							}
						}
					}
					if (prev == this.declaration)
					{
						goto IL_BB;
					}
				}
				this.rover = prev;
				return prev.prefix;
				Block_8:
				return prev.prefix;
			}
			IL_BB:
			return null;
		}

		// Token: 0x040000A9 RID: 169
		private int scope;

		// Token: 0x040000AA RID: 170
		private NamespaceResolver.NamespaceDeclaration declaration;

		// Token: 0x040000AB RID: 171
		private NamespaceResolver.NamespaceDeclaration rover;

		// Token: 0x0200002E RID: 46
		private class NamespaceDeclaration
		{
			// Token: 0x06000203 RID: 515 RVA: 0x00002966 File Offset: 0x00000B66
			public NamespaceDeclaration()
			{
			}

			// Token: 0x040000AC RID: 172
			public string prefix;

			// Token: 0x040000AD RID: 173
			public XNamespace ns;

			// Token: 0x040000AE RID: 174
			public int scope;

			// Token: 0x040000AF RID: 175
			public NamespaceResolver.NamespaceDeclaration prev;
		}
	}
}
