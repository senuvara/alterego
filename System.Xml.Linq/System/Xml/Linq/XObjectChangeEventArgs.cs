﻿using System;

namespace System.Xml.Linq
{
	/// <summary>Provides data for the <see cref="E:System.Xml.Linq.XObject.Changing" /> and <see cref="E:System.Xml.Linq.XObject.Changed" /> events.</summary>
	// Token: 0x02000018 RID: 24
	public class XObjectChangeEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XObjectChangeEventArgs" /> class. </summary>
		/// <param name="objectChange">An <see cref="T:System.Xml.Linq.XObjectChange" /> that contains the event arguments for LINQ to XML events.</param>
		// Token: 0x060000C6 RID: 198 RVA: 0x0000501A File Offset: 0x0000321A
		public XObjectChangeEventArgs(XObjectChange objectChange)
		{
			this.objectChange = objectChange;
		}

		/// <summary>Gets the type of change.</summary>
		/// <returns>An <see cref="T:System.Xml.Linq.XObjectChange" /> that contains the type of change.</returns>
		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000C7 RID: 199 RVA: 0x00005029 File Offset: 0x00003229
		public XObjectChange ObjectChange
		{
			get
			{
				return this.objectChange;
			}
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00005031 File Offset: 0x00003231
		// Note: this type is marked as 'beforefieldinit'.
		static XObjectChangeEventArgs()
		{
		}

		// Token: 0x04000050 RID: 80
		private XObjectChange objectChange;

		/// <summary>Event argument for an <see cref="F:System.Xml.Linq.XObjectChange.Add" /> change event.</summary>
		// Token: 0x04000051 RID: 81
		public static readonly XObjectChangeEventArgs Add = new XObjectChangeEventArgs(XObjectChange.Add);

		/// <summary>Event argument for a <see cref="F:System.Xml.Linq.XObjectChange.Remove" /> change event.</summary>
		// Token: 0x04000052 RID: 82
		public static readonly XObjectChangeEventArgs Remove = new XObjectChangeEventArgs(XObjectChange.Remove);

		/// <summary>Event argument for a <see cref="F:System.Xml.Linq.XObjectChange.Name" /> change event.</summary>
		// Token: 0x04000053 RID: 83
		public static readonly XObjectChangeEventArgs Name = new XObjectChangeEventArgs(XObjectChange.Name);

		/// <summary>Event argument for a <see cref="F:System.Xml.Linq.XObjectChange.Value" /> change event.</summary>
		// Token: 0x04000054 RID: 84
		public static readonly XObjectChangeEventArgs Value = new XObjectChangeEventArgs(XObjectChange.Value);
	}
}
