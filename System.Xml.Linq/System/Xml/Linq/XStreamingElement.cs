﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace System.Xml.Linq
{
	/// <summary>Represents elements in an XML tree that supports deferred streaming output.</summary>
	// Token: 0x02000038 RID: 56
	public class XStreamingElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XElement" /> class from the specified <see cref="T:System.Xml.Linq.XName" />.</summary>
		/// <param name="name">An <see cref="T:System.Xml.Linq.XName" /> that contains the name of the element.</param>
		// Token: 0x06000285 RID: 645 RVA: 0x0000A942 File Offset: 0x00008B42
		public XStreamingElement(XName name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			this.name = name;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XStreamingElement" /> class with the specified name and content.</summary>
		/// <param name="name">An <see cref="T:System.Xml.Linq.XName" /> that contains the element name.</param>
		/// <param name="content">The contents of the element.</param>
		// Token: 0x06000286 RID: 646 RVA: 0x0000A965 File Offset: 0x00008B65
		public XStreamingElement(XName name, object content) : this(name)
		{
			object obj;
			if (!(content is List<object>))
			{
				obj = content;
			}
			else
			{
				(obj = new object[1])[0] = content;
			}
			this.content = obj;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XStreamingElement" /> class with the specified name and content.</summary>
		/// <param name="name">An <see cref="T:System.Xml.Linq.XName" /> that contains the element name.</param>
		/// <param name="content">The contents of the element.</param>
		// Token: 0x06000287 RID: 647 RVA: 0x0000A989 File Offset: 0x00008B89
		public XStreamingElement(XName name, params object[] content) : this(name)
		{
			this.content = content;
		}

		/// <summary>Gets or sets the name of this streaming element.</summary>
		/// <returns>An <see cref="T:System.Xml.Linq.XName" /> that contains the name of this streaming element.</returns>
		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000288 RID: 648 RVA: 0x0000A999 File Offset: 0x00008B99
		// (set) Token: 0x06000289 RID: 649 RVA: 0x0000A9A1 File Offset: 0x00008BA1
		public XName Name
		{
			get
			{
				return this.name;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.name = value;
			}
		}

		/// <summary>Adds the specified content as children to this <see cref="T:System.Xml.Linq.XStreamingElement" />.</summary>
		/// <param name="content">Content to be added to the streaming element.</param>
		// Token: 0x0600028A RID: 650 RVA: 0x0000A9C0 File Offset: 0x00008BC0
		public void Add(object content)
		{
			if (content != null)
			{
				List<object> list = this.content as List<object>;
				if (list == null)
				{
					list = new List<object>();
					if (this.content != null)
					{
						list.Add(this.content);
					}
					this.content = list;
				}
				list.Add(content);
			}
		}

		/// <summary>Adds the specified content as children to this <see cref="T:System.Xml.Linq.XStreamingElement" />.</summary>
		/// <param name="content">Content to be added to the streaming element.</param>
		// Token: 0x0600028B RID: 651 RVA: 0x0000AA07 File Offset: 0x00008C07
		public void Add(params object[] content)
		{
			this.Add(content);
		}

		/// <summary>Serialize this streaming element to a file.</summary>
		/// <param name="fileName">A <see cref="T:System.String" /> that contains the name of the file.</param>
		// Token: 0x0600028C RID: 652 RVA: 0x0000AA10 File Offset: 0x00008C10
		public void Save(string fileName)
		{
			this.Save(fileName, SaveOptions.None);
		}

		/// <summary>Serialize this streaming element to a file, optionally disabling formatting.</summary>
		/// <param name="fileName">A <see cref="T:System.String" /> that contains the name of the file.</param>
		/// <param name="options">A <see cref="T:System.Xml.Linq.SaveOptions" /> object that specifies formatting behavior.</param>
		// Token: 0x0600028D RID: 653 RVA: 0x0000AA1C File Offset: 0x00008C1C
		public void Save(string fileName, SaveOptions options)
		{
			XmlWriterSettings xmlWriterSettings = XNode.GetXmlWriterSettings(options);
			using (XmlWriter xmlWriter = XmlWriter.Create(fileName, xmlWriterSettings))
			{
				this.Save(xmlWriter);
			}
		}

		/// <summary>Outputs this <see cref="T:System.Xml.Linq.XStreamingElement" /> to the specified <see cref="T:System.IO.Stream" />.</summary>
		/// <param name="stream">The stream to output this <see cref="T:System.Xml.Linq.XDocument" /> to.</param>
		// Token: 0x0600028E RID: 654 RVA: 0x0000AA5C File Offset: 0x00008C5C
		public void Save(Stream stream)
		{
			this.Save(stream, SaveOptions.None);
		}

		/// <summary>Outputs this <see cref="T:System.Xml.Linq.XStreamingElement" /> to the specified <see cref="T:System.IO.Stream" />, optionally specifying formatting behavior.</summary>
		/// <param name="stream">The stream to output this <see cref="T:System.Xml.Linq.XDocument" /> to.</param>
		/// <param name="options">A <see cref="T:System.Xml.Linq.SaveOptions" /> object that specifies formatting behavior.</param>
		// Token: 0x0600028F RID: 655 RVA: 0x0000AA68 File Offset: 0x00008C68
		public void Save(Stream stream, SaveOptions options)
		{
			XmlWriterSettings xmlWriterSettings = XNode.GetXmlWriterSettings(options);
			using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
			{
				this.Save(xmlWriter);
			}
		}

		/// <summary>Serialize this streaming element to a <see cref="T:System.IO.TextWriter" />.</summary>
		/// <param name="textWriter">A <see cref="T:System.IO.TextWriter" /> that the <see cref="T:System.Xml.Linq.XStreamingElement" /> will be written to.</param>
		// Token: 0x06000290 RID: 656 RVA: 0x0000AAA8 File Offset: 0x00008CA8
		public void Save(TextWriter textWriter)
		{
			this.Save(textWriter, SaveOptions.None);
		}

		/// <summary>Serialize this streaming element to a <see cref="T:System.IO.TextWriter" />, optionally disabling formatting.</summary>
		/// <param name="textWriter">The <see cref="T:System.IO.TextWriter" /> to output the XML to.</param>
		/// <param name="options">A <see cref="T:System.Xml.Linq.SaveOptions" /> that specifies formatting behavior.</param>
		// Token: 0x06000291 RID: 657 RVA: 0x0000AAB4 File Offset: 0x00008CB4
		public void Save(TextWriter textWriter, SaveOptions options)
		{
			XmlWriterSettings xmlWriterSettings = XNode.GetXmlWriterSettings(options);
			using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, xmlWriterSettings))
			{
				this.Save(xmlWriter);
			}
		}

		/// <summary>Serialize this streaming element to an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">A <see cref="T:System.Xml.XmlWriter" /> that the <see cref="T:System.Xml.Linq.XElement" /> will be written to.</param>
		// Token: 0x06000292 RID: 658 RVA: 0x0000AAF4 File Offset: 0x00008CF4
		public void Save(XmlWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			writer.WriteStartDocument();
			this.WriteTo(writer);
			writer.WriteEndDocument();
		}

		/// <summary>Returns the formatted (indented) XML for this streaming element.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the indented XML.</returns>
		// Token: 0x06000293 RID: 659 RVA: 0x0000AB17 File Offset: 0x00008D17
		public override string ToString()
		{
			return this.GetXmlString(SaveOptions.None);
		}

		/// <summary>Returns the XML for this streaming element, optionally disabling formatting.</summary>
		/// <param name="options">A <see cref="T:System.Xml.Linq.SaveOptions" /> that specifies formatting behavior.</param>
		/// <returns>A <see cref="T:System.String" /> containing the XML.</returns>
		// Token: 0x06000294 RID: 660 RVA: 0x0000AB20 File Offset: 0x00008D20
		public string ToString(SaveOptions options)
		{
			return this.GetXmlString(options);
		}

		/// <summary>Writes this streaming element to an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> into which this method will write.</param>
		// Token: 0x06000295 RID: 661 RVA: 0x0000AB2C File Offset: 0x00008D2C
		public void WriteTo(XmlWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			new StreamingElementWriter(writer).WriteStreamingElement(this);
		}

		// Token: 0x06000296 RID: 662 RVA: 0x0000AB58 File Offset: 0x00008D58
		private string GetXmlString(SaveOptions o)
		{
			string result;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
				xmlWriterSettings.OmitXmlDeclaration = true;
				if ((o & SaveOptions.DisableFormatting) == SaveOptions.None)
				{
					xmlWriterSettings.Indent = true;
				}
				if ((o & SaveOptions.OmitDuplicateNamespaces) != SaveOptions.None)
				{
					xmlWriterSettings.NamespaceHandling |= NamespaceHandling.OmitDuplicates;
				}
				using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, xmlWriterSettings))
				{
					this.WriteTo(xmlWriter);
				}
				result = stringWriter.ToString();
			}
			return result;
		}

		// Token: 0x040000CC RID: 204
		internal XName name;

		// Token: 0x040000CD RID: 205
		internal object content;
	}
}
