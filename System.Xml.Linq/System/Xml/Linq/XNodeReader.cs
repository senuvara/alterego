﻿using System;

namespace System.Xml.Linq
{
	// Token: 0x02000043 RID: 67
	internal class XNodeReader : XmlReader, IXmlLineInfo
	{
		// Token: 0x0600030F RID: 783 RVA: 0x0000C3D8 File Offset: 0x0000A5D8
		internal XNodeReader(XNode node, XmlNameTable nameTable, ReaderOptions options)
		{
			this.source = node;
			this.root = node;
			this.nameTable = ((nameTable != null) ? nameTable : XNodeReader.CreateNameTable());
			this.omitDuplicateNamespaces = ((options & ReaderOptions.OmitDuplicateNamespaces) != ReaderOptions.None);
		}

		// Token: 0x06000310 RID: 784 RVA: 0x0000C40E File Offset: 0x0000A60E
		internal XNodeReader(XNode node, XmlNameTable nameTable) : this(node, nameTable, ((node.GetSaveOptionsFromAnnotations() & SaveOptions.OmitDuplicateNamespaces) != SaveOptions.None) ? ReaderOptions.OmitDuplicateNamespaces : ReaderOptions.None)
		{
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000311 RID: 785 RVA: 0x0000C428 File Offset: 0x0000A628
		public override int AttributeCount
		{
			get
			{
				if (!this.IsInteractive)
				{
					return 0;
				}
				int num = 0;
				XElement elementInAttributeScope = this.GetElementInAttributeScope();
				if (elementInAttributeScope != null)
				{
					XAttribute xattribute = elementInAttributeScope.lastAttr;
					if (xattribute != null)
					{
						do
						{
							xattribute = xattribute.next;
							if (!this.omitDuplicateNamespaces || !this.IsDuplicateNamespaceAttribute(xattribute))
							{
								num++;
							}
						}
						while (xattribute != elementInAttributeScope.lastAttr);
					}
				}
				return num;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000312 RID: 786 RVA: 0x0000C47C File Offset: 0x0000A67C
		public override string BaseURI
		{
			get
			{
				XObject xobject = this.source as XObject;
				if (xobject != null)
				{
					return xobject.BaseUri;
				}
				xobject = (this.parent as XObject);
				if (xobject != null)
				{
					return xobject.BaseUri;
				}
				return string.Empty;
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000313 RID: 787 RVA: 0x0000C4BC File Offset: 0x0000A6BC
		public override int Depth
		{
			get
			{
				if (!this.IsInteractive)
				{
					return 0;
				}
				XObject xobject = this.source as XObject;
				if (xobject != null)
				{
					return XNodeReader.GetDepth(xobject);
				}
				xobject = (this.parent as XObject);
				if (xobject != null)
				{
					return XNodeReader.GetDepth(xobject) + 1;
				}
				return 0;
			}
		}

		// Token: 0x06000314 RID: 788 RVA: 0x0000C504 File Offset: 0x0000A704
		private static int GetDepth(XObject o)
		{
			int num = 0;
			while (o.parent != null)
			{
				num++;
				o = o.parent;
			}
			if (o is XDocument)
			{
				num--;
			}
			return num;
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000315 RID: 789 RVA: 0x0000C536 File Offset: 0x0000A736
		public override bool EOF
		{
			get
			{
				return this.state == ReadState.EndOfFile;
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x06000316 RID: 790 RVA: 0x0000C544 File Offset: 0x0000A744
		public override bool HasAttributes
		{
			get
			{
				if (!this.IsInteractive)
				{
					return false;
				}
				XElement elementInAttributeScope = this.GetElementInAttributeScope();
				return elementInAttributeScope != null && elementInAttributeScope.lastAttr != null && (!this.omitDuplicateNamespaces || this.GetFirstNonDuplicateNamespaceAttribute(elementInAttributeScope.lastAttr.next) != null);
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000317 RID: 791 RVA: 0x0000C590 File Offset: 0x0000A790
		public override bool HasValue
		{
			get
			{
				if (!this.IsInteractive)
				{
					return false;
				}
				XObject xobject = this.source as XObject;
				if (xobject != null)
				{
					switch (xobject.NodeType)
					{
					case XmlNodeType.Attribute:
					case XmlNodeType.Text:
					case XmlNodeType.CDATA:
					case XmlNodeType.ProcessingInstruction:
					case XmlNodeType.Comment:
					case XmlNodeType.DocumentType:
						return true;
					}
					return false;
				}
				return true;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000318 RID: 792 RVA: 0x0000C5F0 File Offset: 0x0000A7F0
		public override bool IsEmptyElement
		{
			get
			{
				if (!this.IsInteractive)
				{
					return false;
				}
				XElement xelement = this.source as XElement;
				return xelement != null && xelement.IsEmpty;
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000319 RID: 793 RVA: 0x0000C61E File Offset: 0x0000A81E
		public override string LocalName
		{
			get
			{
				return this.nameTable.Add(this.GetLocalName());
			}
		}

		// Token: 0x0600031A RID: 794 RVA: 0x0000C634 File Offset: 0x0000A834
		private string GetLocalName()
		{
			if (!this.IsInteractive)
			{
				return string.Empty;
			}
			XElement xelement = this.source as XElement;
			if (xelement != null)
			{
				return xelement.Name.LocalName;
			}
			XAttribute xattribute = this.source as XAttribute;
			if (xattribute != null)
			{
				return xattribute.Name.LocalName;
			}
			XProcessingInstruction xprocessingInstruction = this.source as XProcessingInstruction;
			if (xprocessingInstruction != null)
			{
				return xprocessingInstruction.Target;
			}
			XDocumentType xdocumentType = this.source as XDocumentType;
			if (xdocumentType != null)
			{
				return xdocumentType.Name;
			}
			return string.Empty;
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x0600031B RID: 795 RVA: 0x0000C6B8 File Offset: 0x0000A8B8
		public override string Name
		{
			get
			{
				string prefix = this.GetPrefix();
				if (prefix.Length == 0)
				{
					return this.nameTable.Add(this.GetLocalName());
				}
				return this.nameTable.Add(prefix + ":" + this.GetLocalName());
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x0600031C RID: 796 RVA: 0x0000C702 File Offset: 0x0000A902
		public override string NamespaceURI
		{
			get
			{
				return this.nameTable.Add(this.GetNamespaceURI());
			}
		}

		// Token: 0x0600031D RID: 797 RVA: 0x0000C718 File Offset: 0x0000A918
		private string GetNamespaceURI()
		{
			if (!this.IsInteractive)
			{
				return string.Empty;
			}
			XElement xelement = this.source as XElement;
			if (xelement != null)
			{
				return xelement.Name.NamespaceName;
			}
			XAttribute xattribute = this.source as XAttribute;
			if (xattribute == null)
			{
				return string.Empty;
			}
			string namespaceName = xattribute.Name.NamespaceName;
			if (namespaceName.Length == 0 && xattribute.Name.LocalName == "xmlns")
			{
				return "http://www.w3.org/2000/xmlns/";
			}
			return namespaceName;
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x0600031E RID: 798 RVA: 0x0000C795 File Offset: 0x0000A995
		public override XmlNameTable NameTable
		{
			get
			{
				return this.nameTable;
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600031F RID: 799 RVA: 0x0000C7A0 File Offset: 0x0000A9A0
		public override XmlNodeType NodeType
		{
			get
			{
				if (!this.IsInteractive)
				{
					return XmlNodeType.None;
				}
				XObject xobject = this.source as XObject;
				if (xobject != null)
				{
					if (this.IsEndElement)
					{
						return XmlNodeType.EndElement;
					}
					XmlNodeType nodeType = xobject.NodeType;
					if (nodeType != XmlNodeType.Text)
					{
						return nodeType;
					}
					if (xobject.parent != null && xobject.parent.parent == null && xobject.parent is XDocument)
					{
						return XmlNodeType.Whitespace;
					}
					return XmlNodeType.Text;
				}
				else
				{
					if (this.parent is XDocument)
					{
						return XmlNodeType.Whitespace;
					}
					return XmlNodeType.Text;
				}
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000320 RID: 800 RVA: 0x0000C816 File Offset: 0x0000AA16
		public override string Prefix
		{
			get
			{
				return this.nameTable.Add(this.GetPrefix());
			}
		}

		// Token: 0x06000321 RID: 801 RVA: 0x0000C82C File Offset: 0x0000AA2C
		private string GetPrefix()
		{
			if (!this.IsInteractive)
			{
				return string.Empty;
			}
			XElement xelement = this.source as XElement;
			if (xelement == null)
			{
				XAttribute xattribute = this.source as XAttribute;
				if (xattribute != null)
				{
					string prefixOfNamespace = xattribute.GetPrefixOfNamespace(xattribute.Name.Namespace);
					if (prefixOfNamespace != null)
					{
						return prefixOfNamespace;
					}
				}
				return string.Empty;
			}
			string prefixOfNamespace2 = xelement.GetPrefixOfNamespace(xelement.Name.Namespace);
			if (prefixOfNamespace2 != null)
			{
				return prefixOfNamespace2;
			}
			return string.Empty;
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000322 RID: 802 RVA: 0x0000C89E File Offset: 0x0000AA9E
		public override ReadState ReadState
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000323 RID: 803 RVA: 0x0000C8A6 File Offset: 0x0000AAA6
		public override XmlReaderSettings Settings
		{
			get
			{
				return new XmlReaderSettings
				{
					CheckCharacters = false
				};
			}
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000324 RID: 804 RVA: 0x0000C8B4 File Offset: 0x0000AAB4
		public override string Value
		{
			get
			{
				if (!this.IsInteractive)
				{
					return string.Empty;
				}
				XObject xobject = this.source as XObject;
				if (xobject != null)
				{
					switch (xobject.NodeType)
					{
					case XmlNodeType.Attribute:
						return ((XAttribute)xobject).Value;
					case XmlNodeType.Text:
					case XmlNodeType.CDATA:
						return ((XText)xobject).Value;
					case XmlNodeType.ProcessingInstruction:
						return ((XProcessingInstruction)xobject).Data;
					case XmlNodeType.Comment:
						return ((XComment)xobject).Value;
					case XmlNodeType.DocumentType:
						return ((XDocumentType)xobject).InternalSubset;
					}
					return string.Empty;
				}
				return (string)this.source;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000325 RID: 805 RVA: 0x0000C960 File Offset: 0x0000AB60
		public override string XmlLang
		{
			get
			{
				if (!this.IsInteractive)
				{
					return string.Empty;
				}
				XElement xelement = this.GetElementInScope();
				if (xelement != null)
				{
					XName name = XNamespace.Xml.GetName("lang");
					XAttribute xattribute;
					for (;;)
					{
						xattribute = xelement.Attribute(name);
						if (xattribute != null)
						{
							break;
						}
						xelement = (xelement.parent as XElement);
						if (xelement == null)
						{
							goto IL_49;
						}
					}
					return xattribute.Value;
				}
				IL_49:
				return string.Empty;
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000326 RID: 806 RVA: 0x0000C9BC File Offset: 0x0000ABBC
		public override XmlSpace XmlSpace
		{
			get
			{
				if (!this.IsInteractive)
				{
					return XmlSpace.None;
				}
				XElement xelement = this.GetElementInScope();
				if (xelement != null)
				{
					XName name = XNamespace.Xml.GetName("space");
					for (;;)
					{
						XAttribute xattribute = xelement.Attribute(name);
						if (xattribute != null)
						{
							string a = xattribute.Value.Trim(new char[]
							{
								' ',
								'\t',
								'\n',
								'\r'
							});
							if (a == "preserve")
							{
								break;
							}
							if (a == "default")
							{
								return XmlSpace.Default;
							}
						}
						xelement = (xelement.parent as XElement);
						if (xelement == null)
						{
							return XmlSpace.None;
						}
					}
					return XmlSpace.Preserve;
				}
				return XmlSpace.None;
			}
		}

		// Token: 0x06000327 RID: 807 RVA: 0x0000CA45 File Offset: 0x0000AC45
		public override void Close()
		{
			this.source = null;
			this.parent = null;
			this.root = null;
			this.state = ReadState.Closed;
		}

		// Token: 0x06000328 RID: 808 RVA: 0x0000CA64 File Offset: 0x0000AC64
		public override string GetAttribute(string name)
		{
			if (!this.IsInteractive)
			{
				return null;
			}
			XElement elementInAttributeScope = this.GetElementInAttributeScope();
			if (elementInAttributeScope != null)
			{
				string b;
				string b2;
				XNodeReader.GetNameInAttributeScope(name, elementInAttributeScope, out b, out b2);
				XAttribute xattribute = elementInAttributeScope.lastAttr;
				if (xattribute != null)
				{
					for (;;)
					{
						xattribute = xattribute.next;
						if (xattribute.Name.LocalName == b && xattribute.Name.NamespaceName == b2)
						{
							break;
						}
						if (xattribute == elementInAttributeScope.lastAttr)
						{
							goto IL_82;
						}
					}
					if (this.omitDuplicateNamespaces && this.IsDuplicateNamespaceAttribute(xattribute))
					{
						return null;
					}
					return xattribute.Value;
				}
				IL_82:
				return null;
			}
			XDocumentType xdocumentType = this.source as XDocumentType;
			if (xdocumentType != null)
			{
				if (name == "PUBLIC")
				{
					return xdocumentType.PublicId;
				}
				if (name == "SYSTEM")
				{
					return xdocumentType.SystemId;
				}
			}
			return null;
		}

		// Token: 0x06000329 RID: 809 RVA: 0x0000CB30 File Offset: 0x0000AD30
		public override string GetAttribute(string localName, string namespaceName)
		{
			if (!this.IsInteractive)
			{
				return null;
			}
			XElement elementInAttributeScope = this.GetElementInAttributeScope();
			if (elementInAttributeScope != null)
			{
				if (localName == "xmlns")
				{
					if (namespaceName != null && namespaceName.Length == 0)
					{
						return null;
					}
					if (namespaceName == "http://www.w3.org/2000/xmlns/")
					{
						namespaceName = string.Empty;
					}
				}
				XAttribute xattribute = elementInAttributeScope.lastAttr;
				if (xattribute != null)
				{
					for (;;)
					{
						xattribute = xattribute.next;
						if (xattribute.Name.LocalName == localName && xattribute.Name.NamespaceName == namespaceName)
						{
							break;
						}
						if (xattribute == elementInAttributeScope.lastAttr)
						{
							goto IL_9F;
						}
					}
					if (this.omitDuplicateNamespaces && this.IsDuplicateNamespaceAttribute(xattribute))
					{
						return null;
					}
					return xattribute.Value;
				}
			}
			IL_9F:
			return null;
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000CBE0 File Offset: 0x0000ADE0
		public override string GetAttribute(int index)
		{
			if (!this.IsInteractive)
			{
				return null;
			}
			if (index < 0)
			{
				return null;
			}
			XElement elementInAttributeScope = this.GetElementInAttributeScope();
			if (elementInAttributeScope != null)
			{
				XAttribute xattribute = elementInAttributeScope.lastAttr;
				if (xattribute != null)
				{
					for (;;)
					{
						xattribute = xattribute.next;
						if ((!this.omitDuplicateNamespaces || !this.IsDuplicateNamespaceAttribute(xattribute)) && index-- == 0)
						{
							break;
						}
						if (xattribute == elementInAttributeScope.lastAttr)
						{
							goto IL_54;
						}
					}
					return xattribute.Value;
				}
			}
			IL_54:
			return null;
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000CC44 File Offset: 0x0000AE44
		public override string LookupNamespace(string prefix)
		{
			if (!this.IsInteractive)
			{
				return null;
			}
			if (prefix == null)
			{
				return null;
			}
			XElement elementInScope = this.GetElementInScope();
			if (elementInScope != null)
			{
				XNamespace xnamespace = (prefix.Length == 0) ? elementInScope.GetDefaultNamespace() : elementInScope.GetNamespaceOfPrefix(prefix);
				if (xnamespace != null)
				{
					return this.nameTable.Add(xnamespace.NamespaceName);
				}
			}
			return null;
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000CCA0 File Offset: 0x0000AEA0
		public override bool MoveToAttribute(string name)
		{
			if (!this.IsInteractive)
			{
				return false;
			}
			XElement elementInAttributeScope = this.GetElementInAttributeScope();
			if (elementInAttributeScope != null)
			{
				string b;
				string b2;
				XNodeReader.GetNameInAttributeScope(name, elementInAttributeScope, out b, out b2);
				XAttribute xattribute = elementInAttributeScope.lastAttr;
				if (xattribute != null)
				{
					for (;;)
					{
						xattribute = xattribute.next;
						if (xattribute.Name.LocalName == b && xattribute.Name.NamespaceName == b2)
						{
							break;
						}
						if (xattribute == elementInAttributeScope.lastAttr)
						{
							return false;
						}
					}
					if (this.omitDuplicateNamespaces && this.IsDuplicateNamespaceAttribute(xattribute))
					{
						return false;
					}
					this.source = xattribute;
					this.parent = null;
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600032D RID: 813 RVA: 0x0000CD30 File Offset: 0x0000AF30
		public override bool MoveToAttribute(string localName, string namespaceName)
		{
			if (!this.IsInteractive)
			{
				return false;
			}
			XElement elementInAttributeScope = this.GetElementInAttributeScope();
			if (elementInAttributeScope != null)
			{
				if (localName == "xmlns")
				{
					if (namespaceName != null && namespaceName.Length == 0)
					{
						return false;
					}
					if (namespaceName == "http://www.w3.org/2000/xmlns/")
					{
						namespaceName = string.Empty;
					}
				}
				XAttribute xattribute = elementInAttributeScope.lastAttr;
				if (xattribute != null)
				{
					for (;;)
					{
						xattribute = xattribute.next;
						if (xattribute.Name.LocalName == localName && xattribute.Name.NamespaceName == namespaceName)
						{
							break;
						}
						if (xattribute == elementInAttributeScope.lastAttr)
						{
							return false;
						}
					}
					if (this.omitDuplicateNamespaces && this.IsDuplicateNamespaceAttribute(xattribute))
					{
						return false;
					}
					this.source = xattribute;
					this.parent = null;
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600032E RID: 814 RVA: 0x0000CDE8 File Offset: 0x0000AFE8
		public override void MoveToAttribute(int index)
		{
			if (!this.IsInteractive)
			{
				return;
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			XElement elementInAttributeScope = this.GetElementInAttributeScope();
			if (elementInAttributeScope != null)
			{
				XAttribute xattribute = elementInAttributeScope.lastAttr;
				if (xattribute != null)
				{
					for (;;)
					{
						xattribute = xattribute.next;
						if ((!this.omitDuplicateNamespaces || !this.IsDuplicateNamespaceAttribute(xattribute)) && index-- == 0)
						{
							break;
						}
						if (xattribute == elementInAttributeScope.lastAttr)
						{
							goto IL_64;
						}
					}
					this.source = xattribute;
					this.parent = null;
					return;
				}
			}
			IL_64:
			throw new ArgumentOutOfRangeException("index");
		}

		// Token: 0x0600032F RID: 815 RVA: 0x0000CE64 File Offset: 0x0000B064
		public override bool MoveToElement()
		{
			if (!this.IsInteractive)
			{
				return false;
			}
			XAttribute xattribute = this.source as XAttribute;
			if (xattribute == null)
			{
				xattribute = (this.parent as XAttribute);
			}
			if (xattribute != null && xattribute.parent != null)
			{
				this.source = xattribute.parent;
				this.parent = null;
				return true;
			}
			return false;
		}

		// Token: 0x06000330 RID: 816 RVA: 0x0000CEB8 File Offset: 0x0000B0B8
		public override bool MoveToFirstAttribute()
		{
			if (!this.IsInteractive)
			{
				return false;
			}
			XElement elementInAttributeScope = this.GetElementInAttributeScope();
			if (elementInAttributeScope != null && elementInAttributeScope.lastAttr != null)
			{
				if (this.omitDuplicateNamespaces)
				{
					object firstNonDuplicateNamespaceAttribute = this.GetFirstNonDuplicateNamespaceAttribute(elementInAttributeScope.lastAttr.next);
					if (firstNonDuplicateNamespaceAttribute == null)
					{
						return false;
					}
					this.source = firstNonDuplicateNamespaceAttribute;
				}
				else
				{
					this.source = elementInAttributeScope.lastAttr.next;
				}
				return true;
			}
			return false;
		}

		// Token: 0x06000331 RID: 817 RVA: 0x0000CF20 File Offset: 0x0000B120
		public override bool MoveToNextAttribute()
		{
			if (!this.IsInteractive)
			{
				return false;
			}
			XElement xelement = this.source as XElement;
			if (xelement != null)
			{
				if (this.IsEndElement)
				{
					return false;
				}
				if (xelement.lastAttr != null)
				{
					if (this.omitDuplicateNamespaces)
					{
						object firstNonDuplicateNamespaceAttribute = this.GetFirstNonDuplicateNamespaceAttribute(xelement.lastAttr.next);
						if (firstNonDuplicateNamespaceAttribute == null)
						{
							return false;
						}
						this.source = firstNonDuplicateNamespaceAttribute;
					}
					else
					{
						this.source = xelement.lastAttr.next;
					}
					return true;
				}
				return false;
			}
			else
			{
				XAttribute xattribute = this.source as XAttribute;
				if (xattribute == null)
				{
					xattribute = (this.parent as XAttribute);
				}
				if (xattribute != null && xattribute.parent != null && ((XElement)xattribute.parent).lastAttr != xattribute)
				{
					if (this.omitDuplicateNamespaces)
					{
						object firstNonDuplicateNamespaceAttribute2 = this.GetFirstNonDuplicateNamespaceAttribute(xattribute.next);
						if (firstNonDuplicateNamespaceAttribute2 == null)
						{
							return false;
						}
						this.source = firstNonDuplicateNamespaceAttribute2;
					}
					else
					{
						this.source = xattribute.next;
					}
					this.parent = null;
					return true;
				}
				return false;
			}
		}

		// Token: 0x06000332 RID: 818 RVA: 0x0000D008 File Offset: 0x0000B208
		public override bool Read()
		{
			ReadState readState = this.state;
			if (readState != ReadState.Initial)
			{
				return readState == ReadState.Interactive && this.Read(false);
			}
			this.state = ReadState.Interactive;
			XDocument xdocument = this.source as XDocument;
			return xdocument == null || this.ReadIntoDocument(xdocument);
		}

		// Token: 0x06000333 RID: 819 RVA: 0x0000D050 File Offset: 0x0000B250
		public override bool ReadAttributeValue()
		{
			if (!this.IsInteractive)
			{
				return false;
			}
			XAttribute xattribute = this.source as XAttribute;
			return xattribute != null && this.ReadIntoAttribute(xattribute);
		}

		// Token: 0x06000334 RID: 820 RVA: 0x0000D080 File Offset: 0x0000B280
		public override bool ReadToDescendant(string localName, string namespaceName)
		{
			if (!this.IsInteractive)
			{
				return false;
			}
			this.MoveToElement();
			XElement xelement = this.source as XElement;
			if (xelement != null && !xelement.IsEmpty)
			{
				if (this.IsEndElement)
				{
					return false;
				}
				foreach (XElement xelement2 in xelement.Descendants())
				{
					if (xelement2.Name.LocalName == localName && xelement2.Name.NamespaceName == namespaceName)
					{
						this.source = xelement2;
						return true;
					}
				}
				this.IsEndElement = true;
				return false;
			}
			return false;
		}

		// Token: 0x06000335 RID: 821 RVA: 0x0000D134 File Offset: 0x0000B334
		public override bool ReadToFollowing(string localName, string namespaceName)
		{
			while (this.Read())
			{
				XElement xelement = this.source as XElement;
				if (xelement != null && !this.IsEndElement && xelement.Name.LocalName == localName && xelement.Name.NamespaceName == namespaceName)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000336 RID: 822 RVA: 0x0000D18C File Offset: 0x0000B38C
		public override bool ReadToNextSibling(string localName, string namespaceName)
		{
			if (!this.IsInteractive)
			{
				return false;
			}
			this.MoveToElement();
			if (this.source != this.root)
			{
				XNode xnode = this.source as XNode;
				if (xnode != null)
				{
					foreach (XElement xelement in xnode.ElementsAfterSelf())
					{
						if (xelement.Name.LocalName == localName && xelement.Name.NamespaceName == namespaceName)
						{
							this.source = xelement;
							this.IsEndElement = false;
							return true;
						}
					}
					if (xnode.parent is XElement)
					{
						this.source = xnode.parent;
						this.IsEndElement = true;
						return false;
					}
					goto IL_E0;
				}
				if (this.parent is XElement)
				{
					this.source = this.parent;
					this.parent = null;
					this.IsEndElement = true;
					return false;
				}
			}
			IL_E0:
			return this.ReadToEnd();
		}

		// Token: 0x06000337 RID: 823 RVA: 0x00004D15 File Offset: 0x00002F15
		public override void ResolveEntity()
		{
		}

		// Token: 0x06000338 RID: 824 RVA: 0x0000D294 File Offset: 0x0000B494
		public override void Skip()
		{
			if (!this.IsInteractive)
			{
				return;
			}
			this.Read(true);
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000339 RID: 825 RVA: 0x0000D2A8 File Offset: 0x0000B4A8
		internal override IDtdInfo DtdInfo
		{
			get
			{
				if (this.dtdInfoInitialized)
				{
					return this.dtdInfo;
				}
				this.dtdInfoInitialized = true;
				XDocumentType xdocumentType = this.source as XDocumentType;
				if (xdocumentType == null)
				{
					for (XNode xnode = this.root; xnode != null; xnode = xnode.parent)
					{
						XDocument xdocument = xnode as XDocument;
						if (xdocument != null)
						{
							xdocumentType = xdocument.DocumentType;
							break;
						}
					}
				}
				if (xdocumentType != null)
				{
					this.dtdInfo = xdocumentType.DtdInfo;
				}
				return this.dtdInfo;
			}
		}

		// Token: 0x0600033A RID: 826 RVA: 0x0000D318 File Offset: 0x0000B518
		bool IXmlLineInfo.HasLineInfo()
		{
			if (this.IsEndElement)
			{
				XElement xelement = this.source as XElement;
				if (xelement != null)
				{
					return xelement.Annotation<LineInfoEndElementAnnotation>() != null;
				}
			}
			else
			{
				IXmlLineInfo xmlLineInfo = this.source as IXmlLineInfo;
				if (xmlLineInfo != null)
				{
					return xmlLineInfo.HasLineInfo();
				}
			}
			return false;
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x0600033B RID: 827 RVA: 0x0000D360 File Offset: 0x0000B560
		int IXmlLineInfo.LineNumber
		{
			get
			{
				if (this.IsEndElement)
				{
					XElement xelement = this.source as XElement;
					if (xelement != null)
					{
						LineInfoEndElementAnnotation lineInfoEndElementAnnotation = xelement.Annotation<LineInfoEndElementAnnotation>();
						if (lineInfoEndElementAnnotation != null)
						{
							return lineInfoEndElementAnnotation.lineNumber;
						}
					}
				}
				else
				{
					IXmlLineInfo xmlLineInfo = this.source as IXmlLineInfo;
					if (xmlLineInfo != null)
					{
						return xmlLineInfo.LineNumber;
					}
				}
				return 0;
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x0600033C RID: 828 RVA: 0x0000D3AC File Offset: 0x0000B5AC
		int IXmlLineInfo.LinePosition
		{
			get
			{
				if (this.IsEndElement)
				{
					XElement xelement = this.source as XElement;
					if (xelement != null)
					{
						LineInfoEndElementAnnotation lineInfoEndElementAnnotation = xelement.Annotation<LineInfoEndElementAnnotation>();
						if (lineInfoEndElementAnnotation != null)
						{
							return lineInfoEndElementAnnotation.linePosition;
						}
					}
				}
				else
				{
					IXmlLineInfo xmlLineInfo = this.source as IXmlLineInfo;
					if (xmlLineInfo != null)
					{
						return xmlLineInfo.LinePosition;
					}
				}
				return 0;
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x0600033D RID: 829 RVA: 0x0000D3F8 File Offset: 0x0000B5F8
		// (set) Token: 0x0600033E RID: 830 RVA: 0x0000D408 File Offset: 0x0000B608
		private bool IsEndElement
		{
			get
			{
				return this.parent == this.source;
			}
			set
			{
				this.parent = (value ? this.source : null);
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x0600033F RID: 831 RVA: 0x0000D41C File Offset: 0x0000B61C
		private bool IsInteractive
		{
			get
			{
				return this.state == ReadState.Interactive;
			}
		}

		// Token: 0x06000340 RID: 832 RVA: 0x0000374E File Offset: 0x0000194E
		private static XmlNameTable CreateNameTable()
		{
			NameTable nameTable = new NameTable();
			nameTable.Add(string.Empty);
			nameTable.Add("http://www.w3.org/2000/xmlns/");
			nameTable.Add("http://www.w3.org/XML/1998/namespace");
			return nameTable;
		}

		// Token: 0x06000341 RID: 833 RVA: 0x0000D428 File Offset: 0x0000B628
		private XElement GetElementInAttributeScope()
		{
			XElement xelement = this.source as XElement;
			if (xelement != null)
			{
				if (this.IsEndElement)
				{
					return null;
				}
				return xelement;
			}
			else
			{
				XAttribute xattribute = this.source as XAttribute;
				if (xattribute != null)
				{
					return (XElement)xattribute.parent;
				}
				xattribute = (this.parent as XAttribute);
				if (xattribute != null)
				{
					return (XElement)xattribute.parent;
				}
				return null;
			}
		}

		// Token: 0x06000342 RID: 834 RVA: 0x0000D488 File Offset: 0x0000B688
		private XElement GetElementInScope()
		{
			XElement xelement = this.source as XElement;
			if (xelement != null)
			{
				return xelement;
			}
			XNode xnode = this.source as XNode;
			if (xnode != null)
			{
				return xnode.parent as XElement;
			}
			XAttribute xattribute = this.source as XAttribute;
			if (xattribute != null)
			{
				return (XElement)xattribute.parent;
			}
			xelement = (this.parent as XElement);
			if (xelement != null)
			{
				return xelement;
			}
			xattribute = (this.parent as XAttribute);
			if (xattribute != null)
			{
				return (XElement)xattribute.parent;
			}
			return null;
		}

		// Token: 0x06000343 RID: 835 RVA: 0x0000D50C File Offset: 0x0000B70C
		private static void GetNameInAttributeScope(string qualifiedName, XElement e, out string localName, out string namespaceName)
		{
			if (qualifiedName != null && qualifiedName.Length != 0)
			{
				int num = qualifiedName.IndexOf(':');
				if (num != 0 && num != qualifiedName.Length - 1)
				{
					if (num == -1)
					{
						localName = qualifiedName;
						namespaceName = string.Empty;
						return;
					}
					XNamespace namespaceOfPrefix = e.GetNamespaceOfPrefix(qualifiedName.Substring(0, num));
					if (namespaceOfPrefix != null)
					{
						localName = qualifiedName.Substring(num + 1, qualifiedName.Length - num - 1);
						namespaceName = namespaceOfPrefix.NamespaceName;
						return;
					}
				}
			}
			localName = null;
			namespaceName = null;
		}

		// Token: 0x06000344 RID: 836 RVA: 0x0000D588 File Offset: 0x0000B788
		private bool Read(bool skipContent)
		{
			XElement xelement = this.source as XElement;
			if (xelement != null)
			{
				if (xelement.IsEmpty || this.IsEndElement || skipContent)
				{
					return this.ReadOverNode(xelement);
				}
				return this.ReadIntoElement(xelement);
			}
			else
			{
				XNode xnode = this.source as XNode;
				if (xnode != null)
				{
					return this.ReadOverNode(xnode);
				}
				XAttribute xattribute = this.source as XAttribute;
				if (xattribute != null)
				{
					return this.ReadOverAttribute(xattribute, skipContent);
				}
				return this.ReadOverText(skipContent);
			}
		}

		// Token: 0x06000345 RID: 837 RVA: 0x0000D600 File Offset: 0x0000B800
		private bool ReadIntoDocument(XDocument d)
		{
			XNode xnode = d.content as XNode;
			if (xnode != null)
			{
				this.source = xnode.next;
				return true;
			}
			string text = d.content as string;
			if (text != null && text.Length > 0)
			{
				this.source = text;
				this.parent = d;
				return true;
			}
			return this.ReadToEnd();
		}

		// Token: 0x06000346 RID: 838 RVA: 0x0000D658 File Offset: 0x0000B858
		private bool ReadIntoElement(XElement e)
		{
			XNode xnode = e.content as XNode;
			if (xnode != null)
			{
				this.source = xnode.next;
				return true;
			}
			string text = e.content as string;
			if (text != null)
			{
				if (text.Length > 0)
				{
					this.source = text;
					this.parent = e;
				}
				else
				{
					this.source = e;
					this.IsEndElement = true;
				}
				return true;
			}
			return this.ReadToEnd();
		}

		// Token: 0x06000347 RID: 839 RVA: 0x0000D6C0 File Offset: 0x0000B8C0
		private bool ReadIntoAttribute(XAttribute a)
		{
			this.source = a.value;
			this.parent = a;
			return true;
		}

		// Token: 0x06000348 RID: 840 RVA: 0x0000D6D8 File Offset: 0x0000B8D8
		private bool ReadOverAttribute(XAttribute a, bool skipContent)
		{
			XElement xelement = (XElement)a.parent;
			if (xelement == null)
			{
				return this.ReadToEnd();
			}
			if (xelement.IsEmpty || skipContent)
			{
				return this.ReadOverNode(xelement);
			}
			return this.ReadIntoElement(xelement);
		}

		// Token: 0x06000349 RID: 841 RVA: 0x0000D714 File Offset: 0x0000B914
		private bool ReadOverNode(XNode n)
		{
			if (n == this.root)
			{
				return this.ReadToEnd();
			}
			XNode next = n.next;
			if (next == null || next == n || n == n.parent.content)
			{
				if (n.parent == null || (n.parent.parent == null && n.parent is XDocument))
				{
					return this.ReadToEnd();
				}
				this.source = n.parent;
				this.IsEndElement = true;
			}
			else
			{
				this.source = next;
				this.IsEndElement = false;
			}
			return true;
		}

		// Token: 0x0600034A RID: 842 RVA: 0x0000D79C File Offset: 0x0000B99C
		private bool ReadOverText(bool skipContent)
		{
			if (this.parent is XElement)
			{
				this.source = this.parent;
				this.parent = null;
				this.IsEndElement = true;
				return true;
			}
			if (this.parent is XAttribute)
			{
				XAttribute a = (XAttribute)this.parent;
				this.parent = null;
				return this.ReadOverAttribute(a, skipContent);
			}
			return this.ReadToEnd();
		}

		// Token: 0x0600034B RID: 843 RVA: 0x0000D801 File Offset: 0x0000BA01
		private bool ReadToEnd()
		{
			this.state = ReadState.EndOfFile;
			return false;
		}

		// Token: 0x0600034C RID: 844 RVA: 0x0000D80B File Offset: 0x0000BA0B
		private bool IsDuplicateNamespaceAttribute(XAttribute candidateAttribute)
		{
			return candidateAttribute.IsNamespaceDeclaration && this.IsDuplicateNamespaceAttributeInner(candidateAttribute);
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000D820 File Offset: 0x0000BA20
		private bool IsDuplicateNamespaceAttributeInner(XAttribute candidateAttribute)
		{
			if (candidateAttribute.Name.LocalName == "xml")
			{
				return true;
			}
			XElement xelement = candidateAttribute.parent as XElement;
			if (xelement == this.root || xelement == null)
			{
				return false;
			}
			for (xelement = (xelement.parent as XElement); xelement != null; xelement = (xelement.parent as XElement))
			{
				XAttribute xattribute = xelement.lastAttr;
				if (xattribute != null)
				{
					while (!(xattribute.name == candidateAttribute.name))
					{
						xattribute = xattribute.next;
						if (xattribute == xelement.lastAttr)
						{
							goto IL_85;
						}
					}
					return xattribute.Value == candidateAttribute.Value;
				}
				IL_85:
				if (xelement == this.root)
				{
					return false;
				}
			}
			return false;
		}

		// Token: 0x0600034E RID: 846 RVA: 0x0000D8D0 File Offset: 0x0000BAD0
		private XAttribute GetFirstNonDuplicateNamespaceAttribute(XAttribute candidate)
		{
			if (!this.IsDuplicateNamespaceAttribute(candidate))
			{
				return candidate;
			}
			XElement xelement = candidate.parent as XElement;
			if (xelement != null && candidate != xelement.lastAttr)
			{
				for (;;)
				{
					candidate = candidate.next;
					if (!this.IsDuplicateNamespaceAttribute(candidate))
					{
						break;
					}
					if (candidate == xelement.lastAttr)
					{
						goto IL_3F;
					}
				}
				return candidate;
			}
			IL_3F:
			return null;
		}

		// Token: 0x04000117 RID: 279
		private object source;

		// Token: 0x04000118 RID: 280
		private object parent;

		// Token: 0x04000119 RID: 281
		private ReadState state;

		// Token: 0x0400011A RID: 282
		private XNode root;

		// Token: 0x0400011B RID: 283
		private XmlNameTable nameTable;

		// Token: 0x0400011C RID: 284
		private bool omitDuplicateNamespaces;

		// Token: 0x0400011D RID: 285
		private IDtdInfo dtdInfo;

		// Token: 0x0400011E RID: 286
		private bool dtdInfoInitialized;
	}
}
