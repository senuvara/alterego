﻿using System;

namespace System.Xml.Linq
{
	// Token: 0x02000013 RID: 19
	internal class BaseUriAnnotation
	{
		// Token: 0x060000C2 RID: 194 RVA: 0x00004FEB File Offset: 0x000031EB
		public BaseUriAnnotation(string baseUri)
		{
			this.baseUri = baseUri;
		}

		// Token: 0x04000046 RID: 70
		internal string baseUri;
	}
}
