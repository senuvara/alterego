﻿using System;

namespace System.Xml.Linq
{
	/// <summary>Represents an XML processing instruction. </summary>
	// Token: 0x02000034 RID: 52
	public class XProcessingInstruction : XNode
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XProcessingInstruction" /> class. </summary>
		/// <param name="target">A <see cref="T:System.String" /> containing the target application for this <see cref="T:System.Xml.Linq.XProcessingInstruction" />.</param>
		/// <param name="data">The string data for this <see cref="T:System.Xml.Linq.XProcessingInstruction" />.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="target" /> or <paramref name="data" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="target" /> does not follow the constraints of an XML name.</exception>
		// Token: 0x06000233 RID: 563 RVA: 0x00009B9F File Offset: 0x00007D9F
		public XProcessingInstruction(string target, string data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			XProcessingInstruction.ValidateName(target);
			this.target = target;
			this.data = data;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XProcessingInstruction" /> class. </summary>
		/// <param name="other">The <see cref="T:System.Xml.Linq.XProcessingInstruction" /> node to copy from.</param>
		// Token: 0x06000234 RID: 564 RVA: 0x00009BC9 File Offset: 0x00007DC9
		public XProcessingInstruction(XProcessingInstruction other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			this.target = other.target;
			this.data = other.data;
		}

		// Token: 0x06000235 RID: 565 RVA: 0x00009BF7 File Offset: 0x00007DF7
		internal XProcessingInstruction(XmlReader r)
		{
			this.target = r.Name;
			this.data = r.Value;
			r.Read();
		}

		/// <summary>Gets or sets the string value of this processing instruction.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the string value of this processing instruction.</returns>
		/// <exception cref="T:System.ArgumentNullException">The string <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000236 RID: 566 RVA: 0x00009C1E File Offset: 0x00007E1E
		// (set) Token: 0x06000237 RID: 567 RVA: 0x00009C26 File Offset: 0x00007E26
		public string Data
		{
			get
			{
				return this.data;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				bool flag = base.NotifyChanging(this, XObjectChangeEventArgs.Value);
				this.data = value;
				if (flag)
				{
					base.NotifyChanged(this, XObjectChangeEventArgs.Value);
				}
			}
		}

		/// <summary>Gets the node type for this node.</summary>
		/// <returns>The node type. For <see cref="T:System.Xml.Linq.XProcessingInstruction" /> objects, this value is <see cref="F:System.Xml.XmlNodeType.ProcessingInstruction" />.</returns>
		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000238 RID: 568 RVA: 0x00009C58 File Offset: 0x00007E58
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.ProcessingInstruction;
			}
		}

		/// <summary>Gets or sets a string containing the target application for this processing instruction.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the target application for this processing instruction.</returns>
		/// <exception cref="T:System.ArgumentNullException">The string <paramref name="value" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="target" /> does not follow the constraints of an XML name.</exception>
		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000239 RID: 569 RVA: 0x00009C5B File Offset: 0x00007E5B
		// (set) Token: 0x0600023A RID: 570 RVA: 0x00009C63 File Offset: 0x00007E63
		public string Target
		{
			get
			{
				return this.target;
			}
			set
			{
				XProcessingInstruction.ValidateName(value);
				bool flag = base.NotifyChanging(this, XObjectChangeEventArgs.Name);
				this.target = value;
				if (flag)
				{
					base.NotifyChanged(this, XObjectChangeEventArgs.Name);
				}
			}
		}

		/// <summary>Writes this processing instruction to an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> to write this processing instruction to.</param>
		// Token: 0x0600023B RID: 571 RVA: 0x00009C8D File Offset: 0x00007E8D
		public override void WriteTo(XmlWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			writer.WriteProcessingInstruction(this.target, this.data);
		}

		// Token: 0x0600023C RID: 572 RVA: 0x00009CAF File Offset: 0x00007EAF
		internal override XNode CloneNode()
		{
			return new XProcessingInstruction(this);
		}

		// Token: 0x0600023D RID: 573 RVA: 0x00009CB8 File Offset: 0x00007EB8
		internal override bool DeepEquals(XNode node)
		{
			XProcessingInstruction xprocessingInstruction = node as XProcessingInstruction;
			return xprocessingInstruction != null && this.target == xprocessingInstruction.target && this.data == xprocessingInstruction.data;
		}

		// Token: 0x0600023E RID: 574 RVA: 0x00009CF5 File Offset: 0x00007EF5
		internal override int GetDeepHashCode()
		{
			return this.target.GetHashCode() ^ this.data.GetHashCode();
		}

		// Token: 0x0600023F RID: 575 RVA: 0x00009D0E File Offset: 0x00007F0E
		private static void ValidateName(string name)
		{
			XmlConvert.VerifyNCName(name);
			if (string.Compare(name, "xml", StringComparison.OrdinalIgnoreCase) == 0)
			{
				throw new ArgumentException(Res.GetString("Argument_InvalidPIName", new object[]
				{
					name
				}));
			}
		}

		// Token: 0x040000BE RID: 190
		internal string target;

		// Token: 0x040000BF RID: 191
		internal string data;
	}
}
