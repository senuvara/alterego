﻿using System;

namespace System.Xml.Linq
{
	/// <summary>Specifies serialization options.</summary>
	// Token: 0x02000030 RID: 48
	[Flags]
	public enum SaveOptions
	{
		/// <summary>Format (indent) the XML while serializing.</summary>
		// Token: 0x040000B6 RID: 182
		None = 0,
		/// <summary>Preserve all insignificant white space while serializing.</summary>
		// Token: 0x040000B7 RID: 183
		DisableFormatting = 1,
		/// <summary>Remove the duplicate namespace declarations while serializing.</summary>
		// Token: 0x040000B8 RID: 184
		OmitDuplicateNamespaces = 2
	}
}
