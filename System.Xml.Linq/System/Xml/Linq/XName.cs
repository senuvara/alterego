﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Unity;

namespace System.Xml.Linq
{
	/// <summary>Represents a name of an XML element or attribute. </summary>
	// Token: 0x02000009 RID: 9
	[KnownType(typeof(NameSerializer))]
	[Serializable]
	public sealed class XName : IEquatable<XName>, ISerializable
	{
		// Token: 0x06000064 RID: 100 RVA: 0x00003E3B File Offset: 0x0000203B
		internal XName(XNamespace ns, string localName)
		{
			this.ns = ns;
			this.localName = XmlConvert.VerifyNCName(localName);
			this.hashCode = (ns.GetHashCode() ^ localName.GetHashCode());
		}

		/// <summary>Gets the local (unqualified) part of the name.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the local (unqualified) part of the name.</returns>
		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000065 RID: 101 RVA: 0x00003E69 File Offset: 0x00002069
		public string LocalName
		{
			get
			{
				return this.localName;
			}
		}

		/// <summary>Gets the namespace part of the fully qualified name.</summary>
		/// <returns>An <see cref="T:System.Xml.Linq.XNamespace" /> that contains the namespace part of the name.</returns>
		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000066 RID: 102 RVA: 0x00003E71 File Offset: 0x00002071
		public XNamespace Namespace
		{
			get
			{
				return this.ns;
			}
		}

		/// <summary>Returns the URI of the <see cref="T:System.Xml.Linq.XNamespace" /> for this <see cref="T:System.Xml.Linq.XName" />.</summary>
		/// <returns>The URI of the <see cref="T:System.Xml.Linq.XNamespace" /> for this <see cref="T:System.Xml.Linq.XName" />.</returns>
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000067 RID: 103 RVA: 0x00003E79 File Offset: 0x00002079
		public string NamespaceName
		{
			get
			{
				return this.ns.NamespaceName;
			}
		}

		/// <summary>Returns the expanded XML name in the format {namespace}localname.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the expanded XML name in the format {namespace}localname.</returns>
		// Token: 0x06000068 RID: 104 RVA: 0x00003E86 File Offset: 0x00002086
		public override string ToString()
		{
			if (this.ns.NamespaceName.Length == 0)
			{
				return this.localName;
			}
			return "{" + this.ns.NamespaceName + "}" + this.localName;
		}

		/// <summary>Gets an <see cref="T:System.Xml.Linq.XName" /> object from an expanded name.</summary>
		/// <param name="expandedName">A <see cref="T:System.String" /> that contains an expanded XML name in the format {namespace}localname.</param>
		/// <returns>An <see cref="T:System.Xml.Linq.XName" /> object constructed from the expanded name.</returns>
		// Token: 0x06000069 RID: 105 RVA: 0x00003EC4 File Offset: 0x000020C4
		public static XName Get(string expandedName)
		{
			if (expandedName == null)
			{
				throw new ArgumentNullException("expandedName");
			}
			if (expandedName.Length == 0)
			{
				throw new ArgumentException(Res.GetString("Argument_InvalidExpandedName", new object[]
				{
					expandedName
				}));
			}
			if (expandedName[0] != '{')
			{
				return XNamespace.None.GetName(expandedName);
			}
			int num = expandedName.LastIndexOf('}');
			if (num <= 1 || num == expandedName.Length - 1)
			{
				throw new ArgumentException(Res.GetString("Argument_InvalidExpandedName", new object[]
				{
					expandedName
				}));
			}
			return XNamespace.Get(expandedName, 1, num - 1).GetName(expandedName, num + 1, expandedName.Length - num - 1);
		}

		/// <summary>Gets an <see cref="T:System.Xml.Linq.XName" /> object from a local name and a namespace.</summary>
		/// <param name="localName">A local (unqualified) name.</param>
		/// <param name="namespaceName">An XML namespace.</param>
		/// <returns>An <see cref="T:System.Xml.Linq.XName" /> object created from the specified local name and namespace.</returns>
		// Token: 0x0600006A RID: 106 RVA: 0x00003F67 File Offset: 0x00002167
		public static XName Get(string localName, string namespaceName)
		{
			return XNamespace.Get(namespaceName).GetName(localName);
		}

		/// <summary>Converts a string formatted as an expanded XML name (that is,{namespace}localname) to an <see cref="T:System.Xml.Linq.XName" /> object.</summary>
		/// <param name="expandedName">A string that contains an expanded XML name in the format {namespace}localname.</param>
		/// <returns>An <see cref="T:System.Xml.Linq.XName" /> object constructed from the expanded name.</returns>
		// Token: 0x0600006B RID: 107 RVA: 0x00003F75 File Offset: 0x00002175
		[CLSCompliant(false)]
		public static implicit operator XName(string expandedName)
		{
			if (expandedName == null)
			{
				return null;
			}
			return XName.Get(expandedName);
		}

		/// <summary>Determines whether the specified <see cref="T:System.Xml.Linq.XName" /> is equal to this <see cref="T:System.Xml.Linq.XName" />.</summary>
		/// <param name="obj">The <see cref="T:System.Xml.Linq.XName" /> to compare to the current <see cref="T:System.Xml.Linq.XName" />.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Xml.Linq.XName" /> is equal to the current <see cref="T:System.Xml.Linq.XName" />; otherwise <see langword="false" />.</returns>
		// Token: 0x0600006C RID: 108 RVA: 0x00003F82 File Offset: 0x00002182
		public override bool Equals(object obj)
		{
			return this == obj;
		}

		/// <summary>Gets a hash code for this <see cref="T:System.Xml.Linq.XName" />.</summary>
		/// <returns>An <see cref="T:System.Int32" /> that contains the hash code for the <see cref="T:System.Xml.Linq.XName" />.</returns>
		// Token: 0x0600006D RID: 109 RVA: 0x00003F88 File Offset: 0x00002188
		public override int GetHashCode()
		{
			return this.hashCode;
		}

		/// <summary>Returns a value indicating whether two instances of <see cref="T:System.Xml.Linq.XName" /> are equal.</summary>
		/// <param name="left">The first <see cref="T:System.Xml.Linq.XName" /> to compare.</param>
		/// <param name="right">The second <see cref="T:System.Xml.Linq.XName" /> to compare.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="left" /> and <paramref name="right" /> are equal; otherwise <see langword="false" />.</returns>
		// Token: 0x0600006E RID: 110 RVA: 0x00003F82 File Offset: 0x00002182
		public static bool operator ==(XName left, XName right)
		{
			return left == right;
		}

		/// <summary>Returns a value indicating whether two instances of <see cref="T:System.Xml.Linq.XName" /> are not equal.</summary>
		/// <param name="left">The first <see cref="T:System.Xml.Linq.XName" /> to compare.</param>
		/// <param name="right">The second <see cref="T:System.Xml.Linq.XName" /> to compare.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="left" /> and <paramref name="right" /> are not equal; otherwise <see langword="false" />.</returns>
		// Token: 0x0600006F RID: 111 RVA: 0x00003F90 File Offset: 0x00002190
		public static bool operator !=(XName left, XName right)
		{
			return left != right;
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00003F82 File Offset: 0x00002182
		bool IEquatable<XName>.Equals(XName other)
		{
			return this == other;
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data required to serialize the target object.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
		/// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
		// Token: 0x06000071 RID: 113 RVA: 0x00003F99 File Offset: 0x00002199
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("name", this.ToString());
			info.SetType(typeof(NameSerializer));
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00003FCA File Offset: 0x000021CA
		internal XName()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400001C RID: 28
		private XNamespace ns;

		// Token: 0x0400001D RID: 29
		private string localName;

		// Token: 0x0400001E RID: 30
		private int hashCode;
	}
}
