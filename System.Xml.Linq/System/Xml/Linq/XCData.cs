﻿using System;

namespace System.Xml.Linq
{
	/// <summary>Represents a text node that contains CDATA. </summary>
	// Token: 0x02000022 RID: 34
	public class XCData : XText
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XCData" /> class. </summary>
		/// <param name="value">A string that contains the value of the <see cref="T:System.Xml.Linq.XCData" /> node.</param>
		// Token: 0x0600012C RID: 300 RVA: 0x00005ED9 File Offset: 0x000040D9
		public XCData(string value) : base(value)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Linq.XCData" /> class. </summary>
		/// <param name="other">The <see cref="T:System.Xml.Linq.XCData" /> node to copy from.</param>
		// Token: 0x0600012D RID: 301 RVA: 0x00005EE2 File Offset: 0x000040E2
		public XCData(XCData other) : base(other)
		{
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00005EEB File Offset: 0x000040EB
		internal XCData(XmlReader r) : base(r)
		{
		}

		/// <summary>Gets the node type for this node.</summary>
		/// <returns>The node type. For <see cref="T:System.Xml.Linq.XCData" /> objects, this value is <see cref="F:System.Xml.XmlNodeType.CDATA" />.</returns>
		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600012F RID: 303 RVA: 0x00005EF4 File Offset: 0x000040F4
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.CDATA;
			}
		}

		/// <summary>Writes this CDATA object to an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> into which this method will write.</param>
		// Token: 0x06000130 RID: 304 RVA: 0x00005EF7 File Offset: 0x000040F7
		public override void WriteTo(XmlWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			writer.WriteCData(this.text);
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00005F13 File Offset: 0x00004113
		internal override XNode CloneNode()
		{
			return new XCData(this);
		}
	}
}
