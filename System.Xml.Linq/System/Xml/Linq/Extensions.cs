﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

namespace System.Xml.Linq
{
	/// <summary>Contains the LINQ to XML extension methods.</summary>
	// Token: 0x0200003A RID: 58
	public static class Extensions
	{
		/// <summary>Returns a collection of the attributes of every element in the source collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the source collection.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XAttribute" /> that contains the attributes of every element in the source collection.</returns>
		// Token: 0x060002A0 RID: 672 RVA: 0x0000AFA2 File Offset: 0x000091A2
		public static IEnumerable<XAttribute> Attributes(this IEnumerable<XElement> source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			return Extensions.GetAttributes(source, null);
		}

		/// <summary>Returns a filtered collection of the attributes of every element in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the source collection.</param>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XAttribute" /> that contains a filtered collection of the attributes of every element in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</returns>
		// Token: 0x060002A1 RID: 673 RVA: 0x0000AFB9 File Offset: 0x000091B9
		public static IEnumerable<XAttribute> Attributes(this IEnumerable<XElement> source, XName name)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (!(name != null))
			{
				return XAttribute.EmptySequence;
			}
			return Extensions.GetAttributes(source, name);
		}

		/// <summary>Returns a collection of elements that contains the ancestors of every node in the source collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> that contains the source collection.</param>
		/// <typeparam name="T">The type of the objects in <paramref name="source" />, constrained to <see cref="T:System.Xml.Linq.XNode" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the ancestors of every node in the source collection.</returns>
		// Token: 0x060002A2 RID: 674 RVA: 0x0000AFDF File Offset: 0x000091DF
		public static IEnumerable<XElement> Ancestors<T>(this IEnumerable<T> source) where T : XNode
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			return Extensions.GetAncestors<T>(source, null, false);
		}

		/// <summary>Returns a filtered collection of elements that contains the ancestors of every node in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> that contains the source collection.</param>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <typeparam name="T">The type of the objects in <paramref name="source" />, constrained to <see cref="T:System.Xml.Linq.XNode" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the ancestors of every node in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</returns>
		// Token: 0x060002A3 RID: 675 RVA: 0x0000AFF7 File Offset: 0x000091F7
		public static IEnumerable<XElement> Ancestors<T>(this IEnumerable<T> source, XName name) where T : XNode
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (!(name != null))
			{
				return XElement.EmptySequence;
			}
			return Extensions.GetAncestors<T>(source, name, false);
		}

		/// <summary>Returns a collection of elements that contains every element in the source collection, and the ancestors of every element in the source collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the source collection.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains every element in the source collection, and the ancestors of every element in the source collection.</returns>
		// Token: 0x060002A4 RID: 676 RVA: 0x0000B01E File Offset: 0x0000921E
		public static IEnumerable<XElement> AncestorsAndSelf(this IEnumerable<XElement> source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			return Extensions.GetAncestors<XElement>(source, null, true);
		}

		/// <summary>Returns a filtered collection of elements that contains every element in the source collection, and the ancestors of every element in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the source collection.</param>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains every element in the source collection, and the ancestors of every element in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</returns>
		// Token: 0x060002A5 RID: 677 RVA: 0x0000B036 File Offset: 0x00009236
		public static IEnumerable<XElement> AncestorsAndSelf(this IEnumerable<XElement> source, XName name)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (!(name != null))
			{
				return XElement.EmptySequence;
			}
			return Extensions.GetAncestors<XElement>(source, name, true);
		}

		/// <summary>Returns a collection of the child nodes of every document and element in the source collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> that contains the source collection.</param>
		/// <typeparam name="T">The type of the objects in <paramref name="source" />, constrained to <see cref="T:System.Xml.Linq.XContainer" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> of the child nodes of every document and element in the source collection.</returns>
		// Token: 0x060002A6 RID: 678 RVA: 0x0000B05D File Offset: 0x0000925D
		public static IEnumerable<XNode> Nodes<T>(this IEnumerable<T> source) where T : XContainer
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			foreach (!0 ! in source)
			{
				XContainer root = !;
				if (root != null)
				{
					XNode i = root.LastNode;
					if (i != null)
					{
						do
						{
							i = i.next;
							yield return i;
						}
						while (i.parent == root && i != root.content);
					}
					i = null;
				}
				root = null;
			}
			IEnumerator<T> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Returns a collection of the descendant nodes of every document and element in the source collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XContainer" /> that contains the source collection.</param>
		/// <typeparam name="T">The type of the objects in <paramref name="source" />, constrained to <see cref="T:System.Xml.Linq.XContainer" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> of the descendant nodes of every document and element in the source collection.</returns>
		// Token: 0x060002A7 RID: 679 RVA: 0x0000B06D File Offset: 0x0000926D
		public static IEnumerable<XNode> DescendantNodes<T>(this IEnumerable<T> source) where T : XContainer
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			return Extensions.GetDescendantNodes<T>(source, false);
		}

		/// <summary>Returns a collection of elements that contains the descendant elements of every element and document in the source collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XContainer" /> that contains the source collection.</param>
		/// <typeparam name="T">The type of the objects in <paramref name="source" />, constrained to <see cref="T:System.Xml.Linq.XContainer" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the descendant elements of every element and document in the source collection.</returns>
		// Token: 0x060002A8 RID: 680 RVA: 0x0000B084 File Offset: 0x00009284
		public static IEnumerable<XElement> Descendants<T>(this IEnumerable<T> source) where T : XContainer
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			return Extensions.GetDescendants<T>(source, null, false);
		}

		/// <summary>Returns a filtered collection of elements that contains the descendant elements of every element and document in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XContainer" /> that contains the source collection.</param>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <typeparam name="T">The type of the objects in <paramref name="source" />, constrained to <see cref="T:System.Xml.Linq.XContainer" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the descendant elements of every element and document in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</returns>
		// Token: 0x060002A9 RID: 681 RVA: 0x0000B09C File Offset: 0x0000929C
		public static IEnumerable<XElement> Descendants<T>(this IEnumerable<T> source, XName name) where T : XContainer
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (!(name != null))
			{
				return XElement.EmptySequence;
			}
			return Extensions.GetDescendants<T>(source, name, false);
		}

		/// <summary>Returns a collection of nodes that contains every element in the source collection, and the descendant nodes of every element in the source collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the source collection.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> that contains every element in the source collection, and the descendant nodes of every element in the source collection.</returns>
		// Token: 0x060002AA RID: 682 RVA: 0x0000B0C3 File Offset: 0x000092C3
		public static IEnumerable<XNode> DescendantNodesAndSelf(this IEnumerable<XElement> source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			return Extensions.GetDescendantNodes<XElement>(source, true);
		}

		/// <summary>Returns a collection of elements that contains every element in the source collection, and the descendent elements of every element in the source collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the source collection.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains every element in the source collection, and the descendent elements of every element in the source collection.</returns>
		// Token: 0x060002AB RID: 683 RVA: 0x0000B0DA File Offset: 0x000092DA
		public static IEnumerable<XElement> DescendantsAndSelf(this IEnumerable<XElement> source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			return Extensions.GetDescendants<XElement>(source, null, true);
		}

		/// <summary>Returns a filtered collection of elements that contains every element in the source collection, and the descendents of every element in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the source collection.</param>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains every element in the source collection, and the descendents of every element in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</returns>
		// Token: 0x060002AC RID: 684 RVA: 0x0000B0F2 File Offset: 0x000092F2
		public static IEnumerable<XElement> DescendantsAndSelf(this IEnumerable<XElement> source, XName name)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (!(name != null))
			{
				return XElement.EmptySequence;
			}
			return Extensions.GetDescendants<XElement>(source, name, true);
		}

		/// <summary>Returns a collection of the child elements of every element and document in the source collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the source collection.</param>
		/// <typeparam name="T">The type of the objects in <paramref name="source" />, constrained to <see cref="T:System.Xml.Linq.XContainer" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> of the child elements of every element or document in the source collection.</returns>
		// Token: 0x060002AD RID: 685 RVA: 0x0000B119 File Offset: 0x00009319
		public static IEnumerable<XElement> Elements<T>(this IEnumerable<T> source) where T : XContainer
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			return Extensions.GetElements<T>(source, null);
		}

		/// <summary>Returns a filtered collection of the child elements of every element and document in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the source collection.</param>
		/// <param name="name">The <see cref="T:System.Xml.Linq.XName" /> to match.</param>
		/// <typeparam name="T">The type of the objects in <paramref name="source" />, constrained to <see cref="T:System.Xml.Linq.XContainer" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> of the child elements of every element and document in the source collection. Only elements that have a matching <see cref="T:System.Xml.Linq.XName" /> are included in the collection.</returns>
		// Token: 0x060002AE RID: 686 RVA: 0x0000B130 File Offset: 0x00009330
		public static IEnumerable<XElement> Elements<T>(this IEnumerable<T> source, XName name) where T : XContainer
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (!(name != null))
			{
				return XElement.EmptySequence;
			}
			return Extensions.GetElements<T>(source, name);
		}

		/// <summary>Returns a collection of nodes that contains all nodes in the source collection, sorted in document order.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> that contains the source collection.</param>
		/// <typeparam name="T">The type of the objects in <paramref name="source" />, constrained to <see cref="T:System.Xml.Linq.XNode" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> that contains all nodes in the source collection, sorted in document order.</returns>
		// Token: 0x060002AF RID: 687 RVA: 0x0000B156 File Offset: 0x00009356
		public static IEnumerable<T> InDocumentOrder<T>(this IEnumerable<T> source) where T : XNode
		{
			return source.OrderBy((T n) => n, XNode.DocumentOrderComparer);
		}

		/// <summary>Removes every attribute in the source collection from its parent element.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XAttribute" /> that contains the source collection.</param>
		// Token: 0x060002B0 RID: 688 RVA: 0x0000B184 File Offset: 0x00009384
		public static void Remove(this IEnumerable<XAttribute> source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			foreach (XAttribute xattribute in new List<XAttribute>(source))
			{
				if (xattribute != null)
				{
					xattribute.Remove();
				}
			}
		}

		/// <summary>Removes every node in the source collection from its parent node.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XNode" /> that contains the source collection.</param>
		/// <typeparam name="T">The type of the objects in <paramref name="source" />, constrained to <see cref="T:System.Xml.Linq.XNode" />.</typeparam>
		// Token: 0x060002B1 RID: 689 RVA: 0x0000B1E8 File Offset: 0x000093E8
		public static void Remove<T>(this IEnumerable<T> source) where T : XNode
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			foreach (T t in new List<T>(source))
			{
				if (t != null)
				{
					t.Remove();
				}
			}
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x0000B258 File Offset: 0x00009458
		private static IEnumerable<XAttribute> GetAttributes(IEnumerable<XElement> source, XName name)
		{
			foreach (XElement e in source)
			{
				if (e != null)
				{
					XAttribute a = e.lastAttr;
					if (a != null)
					{
						do
						{
							a = a.next;
							if (name == null || a.name == name)
							{
								yield return a;
							}
						}
						while (a.parent == e && a != e.lastAttr);
					}
					a = null;
				}
				e = null;
			}
			IEnumerator<XElement> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x0000B26F File Offset: 0x0000946F
		private static IEnumerable<XElement> GetAncestors<T>(IEnumerable<T> source, XName name, bool self) where T : XNode
		{
			foreach (!0 ! in source)
			{
				XNode xnode = !;
				if (xnode != null)
				{
					XElement e;
					for (e = ((self ? xnode : xnode.parent) as XElement); e != null; e = (e.parent as XElement))
					{
						if (name == null || e.name == name)
						{
							yield return e;
						}
					}
					e = null;
				}
			}
			IEnumerator<T> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x0000B28D File Offset: 0x0000948D
		private static IEnumerable<XNode> GetDescendantNodes<T>(IEnumerable<T> source, bool self) where T : XContainer
		{
			foreach (!0 ! in source)
			{
				XContainer root = !;
				if (root != null)
				{
					if (self)
					{
						yield return root;
					}
					XNode i = root;
					for (;;)
					{
						XContainer xcontainer = i as XContainer;
						XNode firstNode;
						if (xcontainer != null && (firstNode = xcontainer.FirstNode) != null)
						{
							i = firstNode;
						}
						else
						{
							while (i != null && i != root && i == i.parent.content)
							{
								i = i.parent;
							}
							if (i == null || i == root)
							{
								break;
							}
							i = i.next;
						}
						yield return i;
					}
					i = null;
				}
				root = null;
			}
			IEnumerator<T> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x0000B2A4 File Offset: 0x000094A4
		private static IEnumerable<XElement> GetDescendants<T>(IEnumerable<T> source, XName name, bool self) where T : XContainer
		{
			foreach (!0 ! in source)
			{
				XContainer root = !;
				if (root != null)
				{
					if (self)
					{
						XElement xelement = (XElement)root;
						if (name == null || xelement.name == name)
						{
							yield return xelement;
						}
					}
					XNode i = root;
					XContainer xcontainer = root;
					for (;;)
					{
						if (xcontainer != null && xcontainer.content is XNode)
						{
							i = ((XNode)xcontainer.content).next;
						}
						else
						{
							while (i != null && i != root && i == i.parent.content)
							{
								i = i.parent;
							}
							if (i == null || i == root)
							{
								break;
							}
							i = i.next;
						}
						XElement e = i as XElement;
						if (e != null && (name == null || e.name == name))
						{
							yield return e;
						}
						xcontainer = e;
						e = null;
					}
					i = null;
				}
				root = null;
			}
			IEnumerator<T> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x0000B2C2 File Offset: 0x000094C2
		private static IEnumerable<XElement> GetElements<T>(IEnumerable<T> source, XName name) where T : XContainer
		{
			foreach (!0 ! in source)
			{
				XContainer root = !;
				if (root != null)
				{
					XNode i = root.content as XNode;
					if (i != null)
					{
						do
						{
							i = i.next;
							XElement xelement = i as XElement;
							if (xelement != null && (name == null || xelement.name == name))
							{
								yield return xelement;
							}
						}
						while (i.parent == root && i != root.content);
					}
					i = null;
				}
				root = null;
			}
			IEnumerator<T> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x0200003B RID: 59
		[CompilerGenerated]
		private sealed class <Nodes>d__6<T> : IEnumerable<XNode>, IEnumerable, IEnumerator<XNode>, IDisposable, IEnumerator where T : XContainer
		{
			// Token: 0x060002B7 RID: 695 RVA: 0x0000B2D9 File Offset: 0x000094D9
			[DebuggerHidden]
			public <Nodes>d__6(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060002B8 RID: 696 RVA: 0x0000B2F4 File Offset: 0x000094F4
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060002B9 RID: 697 RVA: 0x0000B32C File Offset: 0x0000952C
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
						if (i.parent != root || i == root.content)
						{
							goto IL_DD;
						}
					}
					else
					{
						this.<>1__state = -1;
						if (source == null)
						{
							throw new ArgumentNullException("source");
						}
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						goto IL_EB;
					}
					IL_87:
					i = i.next;
					this.<>2__current = i;
					this.<>1__state = 1;
					return true;
					IL_DD:
					i = null;
					IL_E4:
					root = null;
					IL_EB:
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						root = enumerator.Current;
						if (root == null)
						{
							goto IL_E4;
						}
						i = root.LastNode;
						if (i != null)
						{
							goto IL_87;
						}
						goto IL_DD;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060002BA RID: 698 RVA: 0x0000B46C File Offset: 0x0000966C
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x17000064 RID: 100
			// (get) Token: 0x060002BB RID: 699 RVA: 0x0000B488 File Offset: 0x00009688
			XNode IEnumerator<XNode>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002BC RID: 700 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000065 RID: 101
			// (get) Token: 0x060002BD RID: 701 RVA: 0x0000B488 File Offset: 0x00009688
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002BE RID: 702 RVA: 0x0000B490 File Offset: 0x00009690
			[DebuggerHidden]
			IEnumerator<XNode> IEnumerable<XNode>.GetEnumerator()
			{
				Extensions.<Nodes>d__6<T> <Nodes>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<Nodes>d__ = this;
				}
				else
				{
					<Nodes>d__ = new Extensions.<Nodes>d__6<T>(0);
				}
				<Nodes>d__.source = source;
				return <Nodes>d__;
			}

			// Token: 0x060002BF RID: 703 RVA: 0x0000B4D3 File Offset: 0x000096D3
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XNode>.GetEnumerator();
			}

			// Token: 0x040000D2 RID: 210
			private int <>1__state;

			// Token: 0x040000D3 RID: 211
			private XNode <>2__current;

			// Token: 0x040000D4 RID: 212
			private int <>l__initialThreadId;

			// Token: 0x040000D5 RID: 213
			private IEnumerable<T> source;

			// Token: 0x040000D6 RID: 214
			public IEnumerable<T> <>3__source;

			// Token: 0x040000D7 RID: 215
			private XNode <n>5__1;

			// Token: 0x040000D8 RID: 216
			private XContainer <root>5__2;

			// Token: 0x040000D9 RID: 217
			private IEnumerator<T> <>7__wrap1;
		}

		// Token: 0x0200003C RID: 60
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c__15<T> where T : XNode
		{
			// Token: 0x060002C0 RID: 704 RVA: 0x0000B4DB File Offset: 0x000096DB
			// Note: this type is marked as 'beforefieldinit'.
			static <>c__15()
			{
			}

			// Token: 0x060002C1 RID: 705 RVA: 0x00002966 File Offset: 0x00000B66
			public <>c__15()
			{
			}

			// Token: 0x060002C2 RID: 706 RVA: 0x0000B4E7 File Offset: 0x000096E7
			internal XNode <InDocumentOrder>b__15_0(T n)
			{
				return n;
			}

			// Token: 0x040000DA RID: 218
			public static readonly Extensions.<>c__15<T> <>9 = new Extensions.<>c__15<T>();

			// Token: 0x040000DB RID: 219
			public static Func<T, XNode> <>9__15_0;
		}

		// Token: 0x0200003D RID: 61
		[CompilerGenerated]
		private sealed class <GetAttributes>d__18 : IEnumerable<XAttribute>, IEnumerable, IEnumerator<XAttribute>, IDisposable, IEnumerator
		{
			// Token: 0x060002C3 RID: 707 RVA: 0x0000B4EF File Offset: 0x000096EF
			[DebuggerHidden]
			public <GetAttributes>d__18(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060002C4 RID: 708 RVA: 0x0000B50C File Offset: 0x0000970C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060002C5 RID: 709 RVA: 0x0000B544 File Offset: 0x00009744
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num == 0)
					{
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						goto IL_FC;
					}
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -3;
					goto IL_C8;
					IL_72:
					a = a.next;
					if (name == null || a.name == name)
					{
						this.<>2__current = a;
						this.<>1__state = 1;
						return true;
					}
					IL_C8:
					if (a.parent == e && a != e.lastAttr)
					{
						goto IL_72;
					}
					IL_EE:
					a = null;
					IL_F5:
					e = null;
					IL_FC:
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						e = enumerator.Current;
						if (e == null)
						{
							goto IL_F5;
						}
						a = e.lastAttr;
						if (a != null)
						{
							goto IL_72;
						}
						goto IL_EE;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060002C6 RID: 710 RVA: 0x0000B694 File Offset: 0x00009894
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x17000066 RID: 102
			// (get) Token: 0x060002C7 RID: 711 RVA: 0x0000B6B0 File Offset: 0x000098B0
			XAttribute IEnumerator<XAttribute>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002C8 RID: 712 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000067 RID: 103
			// (get) Token: 0x060002C9 RID: 713 RVA: 0x0000B6B0 File Offset: 0x000098B0
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002CA RID: 714 RVA: 0x0000B6B8 File Offset: 0x000098B8
			[DebuggerHidden]
			IEnumerator<XAttribute> IEnumerable<XAttribute>.GetEnumerator()
			{
				Extensions.<GetAttributes>d__18 <GetAttributes>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetAttributes>d__ = this;
				}
				else
				{
					<GetAttributes>d__ = new Extensions.<GetAttributes>d__18(0);
				}
				<GetAttributes>d__.source = source;
				<GetAttributes>d__.name = name;
				return <GetAttributes>d__;
			}

			// Token: 0x060002CB RID: 715 RVA: 0x0000B707 File Offset: 0x00009907
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XAttribute>.GetEnumerator();
			}

			// Token: 0x040000DC RID: 220
			private int <>1__state;

			// Token: 0x040000DD RID: 221
			private XAttribute <>2__current;

			// Token: 0x040000DE RID: 222
			private int <>l__initialThreadId;

			// Token: 0x040000DF RID: 223
			private IEnumerable<XElement> source;

			// Token: 0x040000E0 RID: 224
			public IEnumerable<XElement> <>3__source;

			// Token: 0x040000E1 RID: 225
			private XAttribute <a>5__1;

			// Token: 0x040000E2 RID: 226
			private XName name;

			// Token: 0x040000E3 RID: 227
			public XName <>3__name;

			// Token: 0x040000E4 RID: 228
			private XElement <e>5__2;

			// Token: 0x040000E5 RID: 229
			private IEnumerator<XElement> <>7__wrap1;
		}

		// Token: 0x0200003E RID: 62
		[CompilerGenerated]
		private sealed class <GetAncestors>d__19<T> : IEnumerable<XElement>, IEnumerable, IEnumerator<XElement>, IDisposable, IEnumerator where T : XNode
		{
			// Token: 0x060002CC RID: 716 RVA: 0x0000B70F File Offset: 0x0000990F
			[DebuggerHidden]
			public <GetAncestors>d__19(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060002CD RID: 717 RVA: 0x0000B72C File Offset: 0x0000992C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060002CE RID: 718 RVA: 0x0000B764 File Offset: 0x00009964
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
						goto IL_B7;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					IL_DC:
					while (enumerator.MoveNext())
					{
						XNode xnode = enumerator.Current;
						if (xnode != null)
						{
							e = ((self ? xnode : xnode.parent) as XElement);
							goto IL_CD;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					return false;
					IL_B7:
					e = (e.parent as XElement);
					IL_CD:
					if (e == null)
					{
						e = null;
						goto IL_DC;
					}
					if (!(name == null) && !(e.name == name))
					{
						goto IL_B7;
					}
					this.<>2__current = e;
					this.<>1__state = 1;
					result = true;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060002CF RID: 719 RVA: 0x0000B888 File Offset: 0x00009A88
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x17000068 RID: 104
			// (get) Token: 0x060002D0 RID: 720 RVA: 0x0000B8A4 File Offset: 0x00009AA4
			XElement IEnumerator<XElement>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002D1 RID: 721 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000069 RID: 105
			// (get) Token: 0x060002D2 RID: 722 RVA: 0x0000B8A4 File Offset: 0x00009AA4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002D3 RID: 723 RVA: 0x0000B8AC File Offset: 0x00009AAC
			[DebuggerHidden]
			IEnumerator<XElement> IEnumerable<XElement>.GetEnumerator()
			{
				Extensions.<GetAncestors>d__19<T> <GetAncestors>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetAncestors>d__ = this;
				}
				else
				{
					<GetAncestors>d__ = new Extensions.<GetAncestors>d__19<T>(0);
				}
				<GetAncestors>d__.source = source;
				<GetAncestors>d__.name = name;
				<GetAncestors>d__.self = self;
				return <GetAncestors>d__;
			}

			// Token: 0x060002D4 RID: 724 RVA: 0x0000B907 File Offset: 0x00009B07
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator();
			}

			// Token: 0x040000E6 RID: 230
			private int <>1__state;

			// Token: 0x040000E7 RID: 231
			private XElement <>2__current;

			// Token: 0x040000E8 RID: 232
			private int <>l__initialThreadId;

			// Token: 0x040000E9 RID: 233
			private IEnumerable<T> source;

			// Token: 0x040000EA RID: 234
			public IEnumerable<T> <>3__source;

			// Token: 0x040000EB RID: 235
			private bool self;

			// Token: 0x040000EC RID: 236
			public bool <>3__self;

			// Token: 0x040000ED RID: 237
			private XName name;

			// Token: 0x040000EE RID: 238
			public XName <>3__name;

			// Token: 0x040000EF RID: 239
			private XElement <e>5__1;

			// Token: 0x040000F0 RID: 240
			private IEnumerator<T> <>7__wrap1;
		}

		// Token: 0x0200003F RID: 63
		[CompilerGenerated]
		private sealed class <GetDescendantNodes>d__20<T> : IEnumerable<XNode>, IEnumerable, IEnumerator<XNode>, IDisposable, IEnumerator where T : XContainer
		{
			// Token: 0x060002D5 RID: 725 RVA: 0x0000B90F File Offset: 0x00009B0F
			[DebuggerHidden]
			public <GetDescendantNodes>d__20(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060002D6 RID: 726 RVA: 0x0000B92C File Offset: 0x00009B2C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num - 1 <= 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060002D7 RID: 727 RVA: 0x0000B968 File Offset: 0x00009B68
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						goto IL_156;
					case 1:
						this.<>1__state = -3;
						break;
					case 2:
						this.<>1__state = -3;
						goto IL_9C;
					default:
						return false;
					}
					IL_90:
					i = root;
					IL_9C:
					XContainer xcontainer = i as XContainer;
					XNode firstNode;
					if (xcontainer != null && (firstNode = xcontainer.FirstNode) != null)
					{
						i = firstNode;
					}
					else
					{
						while (i != null && i != root && i == i.parent.content)
						{
							i = i.parent;
						}
						if (i == null || i == root)
						{
							i = null;
							goto IL_14F;
						}
						i = i.next;
					}
					this.<>2__current = i;
					this.<>1__state = 2;
					return true;
					IL_14F:
					root = null;
					IL_156:
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						root = enumerator.Current;
						if (root == null)
						{
							goto IL_14F;
						}
						if (!self)
						{
							goto IL_90;
						}
						this.<>2__current = root;
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060002D8 RID: 728 RVA: 0x0000BB10 File Offset: 0x00009D10
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x1700006A RID: 106
			// (get) Token: 0x060002D9 RID: 729 RVA: 0x0000BB2C File Offset: 0x00009D2C
			XNode IEnumerator<XNode>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002DA RID: 730 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700006B RID: 107
			// (get) Token: 0x060002DB RID: 731 RVA: 0x0000BB2C File Offset: 0x00009D2C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002DC RID: 732 RVA: 0x0000BB34 File Offset: 0x00009D34
			[DebuggerHidden]
			IEnumerator<XNode> IEnumerable<XNode>.GetEnumerator()
			{
				Extensions.<GetDescendantNodes>d__20<T> <GetDescendantNodes>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetDescendantNodes>d__ = this;
				}
				else
				{
					<GetDescendantNodes>d__ = new Extensions.<GetDescendantNodes>d__20<T>(0);
				}
				<GetDescendantNodes>d__.source = source;
				<GetDescendantNodes>d__.self = self;
				return <GetDescendantNodes>d__;
			}

			// Token: 0x060002DD RID: 733 RVA: 0x0000BB83 File Offset: 0x00009D83
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XNode>.GetEnumerator();
			}

			// Token: 0x040000F1 RID: 241
			private int <>1__state;

			// Token: 0x040000F2 RID: 242
			private XNode <>2__current;

			// Token: 0x040000F3 RID: 243
			private int <>l__initialThreadId;

			// Token: 0x040000F4 RID: 244
			private IEnumerable<T> source;

			// Token: 0x040000F5 RID: 245
			public IEnumerable<T> <>3__source;

			// Token: 0x040000F6 RID: 246
			private bool self;

			// Token: 0x040000F7 RID: 247
			public bool <>3__self;

			// Token: 0x040000F8 RID: 248
			private XContainer <root>5__1;

			// Token: 0x040000F9 RID: 249
			private XNode <n>5__2;

			// Token: 0x040000FA RID: 250
			private IEnumerator<T> <>7__wrap1;
		}

		// Token: 0x02000040 RID: 64
		[CompilerGenerated]
		private sealed class <GetDescendants>d__21<T> : IEnumerable<XElement>, IEnumerable, IEnumerator<XElement>, IDisposable, IEnumerator where T : XContainer
		{
			// Token: 0x060002DE RID: 734 RVA: 0x0000BB8B File Offset: 0x00009D8B
			[DebuggerHidden]
			public <GetDescendants>d__21(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060002DF RID: 735 RVA: 0x0000BBA8 File Offset: 0x00009DA8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num - 1 <= 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060002E0 RID: 736 RVA: 0x0000BBE4 File Offset: 0x00009DE4
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						goto IL_1DE;
					case 1:
						this.<>1__state = -3;
						break;
					case 2:
						this.<>1__state = -3;
						goto IL_1BD;
					default:
						return false;
					}
					IL_B8:
					i = root;
					XContainer xcontainer = root;
					IL_CB:
					if (xcontainer != null && xcontainer.content is XNode)
					{
						i = ((XNode)xcontainer.content).next;
					}
					else
					{
						while (i != null && i != root && i == i.parent.content)
						{
							i = i.parent;
						}
						if (i == null || i == root)
						{
							i = null;
							goto IL_1D7;
						}
						i = i.next;
					}
					e = (i as XElement);
					if (e != null && (name == null || e.name == name))
					{
						this.<>2__current = e;
						this.<>1__state = 2;
						return true;
					}
					IL_1BD:
					xcontainer = e;
					e = null;
					goto IL_CB;
					IL_1D7:
					root = null;
					IL_1DE:
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						root = enumerator.Current;
						if (root == null)
						{
							goto IL_1D7;
						}
						if (!self)
						{
							goto IL_B8;
						}
						XElement xelement = (XElement)root;
						if (!(name == null) && !(xelement.name == name))
						{
							goto IL_B8;
						}
						this.<>2__current = xelement;
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060002E1 RID: 737 RVA: 0x0000BE14 File Offset: 0x0000A014
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x1700006C RID: 108
			// (get) Token: 0x060002E2 RID: 738 RVA: 0x0000BE30 File Offset: 0x0000A030
			XElement IEnumerator<XElement>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002E3 RID: 739 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700006D RID: 109
			// (get) Token: 0x060002E4 RID: 740 RVA: 0x0000BE30 File Offset: 0x0000A030
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002E5 RID: 741 RVA: 0x0000BE38 File Offset: 0x0000A038
			[DebuggerHidden]
			IEnumerator<XElement> IEnumerable<XElement>.GetEnumerator()
			{
				Extensions.<GetDescendants>d__21<T> <GetDescendants>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetDescendants>d__ = this;
				}
				else
				{
					<GetDescendants>d__ = new Extensions.<GetDescendants>d__21<T>(0);
				}
				<GetDescendants>d__.source = source;
				<GetDescendants>d__.name = name;
				<GetDescendants>d__.self = self;
				return <GetDescendants>d__;
			}

			// Token: 0x060002E6 RID: 742 RVA: 0x0000BE93 File Offset: 0x0000A093
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator();
			}

			// Token: 0x040000FB RID: 251
			private int <>1__state;

			// Token: 0x040000FC RID: 252
			private XElement <>2__current;

			// Token: 0x040000FD RID: 253
			private int <>l__initialThreadId;

			// Token: 0x040000FE RID: 254
			private IEnumerable<T> source;

			// Token: 0x040000FF RID: 255
			public IEnumerable<T> <>3__source;

			// Token: 0x04000100 RID: 256
			private bool self;

			// Token: 0x04000101 RID: 257
			public bool <>3__self;

			// Token: 0x04000102 RID: 258
			private XName name;

			// Token: 0x04000103 RID: 259
			public XName <>3__name;

			// Token: 0x04000104 RID: 260
			private XContainer <root>5__1;

			// Token: 0x04000105 RID: 261
			private XNode <n>5__2;

			// Token: 0x04000106 RID: 262
			private XElement <e>5__3;

			// Token: 0x04000107 RID: 263
			private IEnumerator<T> <>7__wrap1;
		}

		// Token: 0x02000041 RID: 65
		[CompilerGenerated]
		private sealed class <GetElements>d__22<T> : IEnumerable<XElement>, IEnumerable, IEnumerator<XElement>, IDisposable, IEnumerator where T : XContainer
		{
			// Token: 0x060002E7 RID: 743 RVA: 0x0000BE9B File Offset: 0x0000A09B
			[DebuggerHidden]
			public <GetElements>d__22(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060002E8 RID: 744 RVA: 0x0000BEB8 File Offset: 0x0000A0B8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060002E9 RID: 745 RVA: 0x0000BEF0 File Offset: 0x0000A0F0
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num == 0)
					{
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						goto IL_111;
					}
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -3;
					goto IL_DA;
					IL_7F:
					i = i.next;
					XElement xelement = i as XElement;
					if (xelement != null && (name == null || xelement.name == name))
					{
						this.<>2__current = xelement;
						this.<>1__state = 1;
						return true;
					}
					IL_DA:
					if (i.parent == root && i != root.content)
					{
						goto IL_7F;
					}
					IL_103:
					i = null;
					IL_10A:
					root = null;
					IL_111:
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						root = enumerator.Current;
						if (root == null)
						{
							goto IL_10A;
						}
						i = (root.content as XNode);
						if (i != null)
						{
							goto IL_7F;
						}
						goto IL_103;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060002EA RID: 746 RVA: 0x0000C054 File Offset: 0x0000A254
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x1700006E RID: 110
			// (get) Token: 0x060002EB RID: 747 RVA: 0x0000C070 File Offset: 0x0000A270
			XElement IEnumerator<XElement>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002EC RID: 748 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700006F RID: 111
			// (get) Token: 0x060002ED RID: 749 RVA: 0x0000C070 File Offset: 0x0000A270
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002EE RID: 750 RVA: 0x0000C078 File Offset: 0x0000A278
			[DebuggerHidden]
			IEnumerator<XElement> IEnumerable<XElement>.GetEnumerator()
			{
				Extensions.<GetElements>d__22<T> <GetElements>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetElements>d__ = this;
				}
				else
				{
					<GetElements>d__ = new Extensions.<GetElements>d__22<T>(0);
				}
				<GetElements>d__.source = source;
				<GetElements>d__.name = name;
				return <GetElements>d__;
			}

			// Token: 0x060002EF RID: 751 RVA: 0x0000C0C7 File Offset: 0x0000A2C7
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator();
			}

			// Token: 0x04000108 RID: 264
			private int <>1__state;

			// Token: 0x04000109 RID: 265
			private XElement <>2__current;

			// Token: 0x0400010A RID: 266
			private int <>l__initialThreadId;

			// Token: 0x0400010B RID: 267
			private IEnumerable<T> source;

			// Token: 0x0400010C RID: 268
			public IEnumerable<T> <>3__source;

			// Token: 0x0400010D RID: 269
			private XNode <n>5__1;

			// Token: 0x0400010E RID: 270
			private XName name;

			// Token: 0x0400010F RID: 271
			public XName <>3__name;

			// Token: 0x04000110 RID: 272
			private XContainer <root>5__2;

			// Token: 0x04000111 RID: 273
			private IEnumerator<T> <>7__wrap1;
		}
	}
}
