﻿using System;
using System.ComponentModel;
using System.Xml.Linq;
using Unity;

namespace System.Xml.XPath
{
	/// <summary>Extends the <see cref="T:System.Xml.Linq.XDocument" /> class by providing a method for navigating and editing an XML node.</summary>
	// Token: 0x02000057 RID: 87
	[EditorBrowsable(EditorBrowsableState.Never)]
	public static class XDocumentExtensions
	{
		/// <summary>Returns an accessor that allows you to navigate and edit the specified <see cref="T:System.Xml.Linq.XNode" />.</summary>
		/// <param name="node">The XML node to navigate.</param>
		/// <returns>An interface that provides an accessor to the <see cref="T:System.Xml.XPath.XPathNavigator" /> class.</returns>
		// Token: 0x06000393 RID: 915 RVA: 0x0000EBFC File Offset: 0x0000CDFC
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IXPathNavigable ToXPathNavigable(this XNode node)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
