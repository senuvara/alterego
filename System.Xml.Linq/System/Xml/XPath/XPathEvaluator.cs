﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace System.Xml.XPath
{
	// Token: 0x02000006 RID: 6
	internal struct XPathEvaluator
	{
		// Token: 0x06000050 RID: 80 RVA: 0x000039BC File Offset: 0x00001BBC
		public object Evaluate<T>(XNode node, string expression, IXmlNamespaceResolver resolver) where T : class
		{
			object obj = node.CreateNavigator().Evaluate(expression, resolver);
			if (obj is XPathNodeIterator)
			{
				return this.EvaluateIterator<T>((XPathNodeIterator)obj);
			}
			if (!(obj is T))
			{
				throw new InvalidOperationException(Res.GetString("InvalidOperation_UnexpectedEvaluation", new object[]
				{
					obj.GetType()
				}));
			}
			return (T)((object)obj);
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00003A1E File Offset: 0x00001C1E
		private IEnumerable<T> EvaluateIterator<T>(XPathNodeIterator result)
		{
			foreach (object obj in result)
			{
				XPathNavigator xpathNavigator = (XPathNavigator)obj;
				object r = xpathNavigator.UnderlyingObject;
				if (!(r is T))
				{
					throw new InvalidOperationException(Res.GetString("InvalidOperation_UnexpectedEvaluation", new object[]
					{
						r.GetType()
					}));
				}
				yield return (T)((object)r);
				XText t = r as XText;
				if (t != null && t.parent != null)
				{
					while (t != t.parent.content)
					{
						t = (t.next as XText);
						if (t == null)
						{
							break;
						}
						yield return (T)((object)t);
					}
				}
				r = null;
				t = null;
			}
			IEnumerator enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x02000007 RID: 7
		[CompilerGenerated]
		private sealed class <EvaluateIterator>d__1<T> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000052 RID: 82 RVA: 0x00003A3A File Offset: 0x00001C3A
			[DebuggerHidden]
			public <EvaluateIterator>d__1(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000053 RID: 83 RVA: 0x00003A54 File Offset: 0x00001C54
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num - 1 <= 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000054 RID: 84 RVA: 0x00003A90 File Offset: 0x00001C90
			bool IEnumerator.MoveNext()
			{
				bool flag;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						enumerator = result.GetEnumerator();
						this.<>1__state = -3;
						goto IL_14A;
					case 1:
						this.<>1__state = -3;
						t = (r as XText);
						if (t == null || t.parent == null)
						{
							goto IL_13C;
						}
						break;
					case 2:
						this.<>1__state = -3;
						break;
					default:
						return false;
					}
					if (t != t.parent.content)
					{
						t = (t.next as XText);
						if (t != null)
						{
							this.<>2__current = (T)((object)t);
							this.<>1__state = 2;
							return true;
						}
					}
					IL_13C:
					r = null;
					t = null;
					IL_14A:
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						flag = false;
					}
					else
					{
						XPathNavigator xpathNavigator = (XPathNavigator)enumerator.Current;
						r = xpathNavigator.UnderlyingObject;
						if (!(r is T))
						{
							throw new InvalidOperationException(Res.GetString("InvalidOperation_UnexpectedEvaluation", new object[]
							{
								r.GetType()
							}));
						}
						this.<>2__current = (T)((object)r);
						this.<>1__state = 1;
						flag = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return flag;
			}

			// Token: 0x06000055 RID: 85 RVA: 0x00003C2C File Offset: 0x00001E2C
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}

			// Token: 0x1700000F RID: 15
			// (get) Token: 0x06000056 RID: 86 RVA: 0x00003C55 File Offset: 0x00001E55
			T IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000057 RID: 87 RVA: 0x00003C5D File Offset: 0x00001E5D
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000010 RID: 16
			// (get) Token: 0x06000058 RID: 88 RVA: 0x00003C64 File Offset: 0x00001E64
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000059 RID: 89 RVA: 0x00003C74 File Offset: 0x00001E74
			[DebuggerHidden]
			IEnumerator<T> IEnumerable<!0>.GetEnumerator()
			{
				XPathEvaluator.<EvaluateIterator>d__1<T> <EvaluateIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<EvaluateIterator>d__ = this;
				}
				else
				{
					<EvaluateIterator>d__ = new XPathEvaluator.<EvaluateIterator>d__1<T>(0);
				}
				<EvaluateIterator>d__.<>4__this = ref this;
				<EvaluateIterator>d__.result = result;
				return <EvaluateIterator>d__;
			}

			// Token: 0x0600005A RID: 90 RVA: 0x00003CC3 File Offset: 0x00001EC3
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<T>.GetEnumerator();
			}

			// Token: 0x04000012 RID: 18
			private int <>1__state;

			// Token: 0x04000013 RID: 19
			private T <>2__current;

			// Token: 0x04000014 RID: 20
			private int <>l__initialThreadId;

			// Token: 0x04000015 RID: 21
			private XPathNodeIterator result;

			// Token: 0x04000016 RID: 22
			public XPathNodeIterator <>3__result;

			// Token: 0x04000017 RID: 23
			private object <r>5__1;

			// Token: 0x04000018 RID: 24
			private XText <t>5__2;

			// Token: 0x04000019 RID: 25
			public XPathEvaluator <>4__this;

			// Token: 0x0400001A RID: 26
			public XPathEvaluator <>3__<>4__this;

			// Token: 0x0400001B RID: 27
			private IEnumerator <>7__wrap1;
		}
	}
}
