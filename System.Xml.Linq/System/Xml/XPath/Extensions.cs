﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace System.Xml.XPath
{
	/// <summary>This class contains the LINQ to XML extension methods that enable you to evaluate XPath expressions.</summary>
	// Token: 0x02000008 RID: 8
	public static class Extensions
	{
		/// <summary>Creates an <see cref="T:System.Xml.XPath.XPathNavigator" /> for an <see cref="T:System.Xml.Linq.XNode" />.</summary>
		/// <param name="node">An <see cref="T:System.Xml.Linq.XNode" /> that can process XPath queries.</param>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNavigator" /> that can process XPath queries.</returns>
		// Token: 0x0600005B RID: 91 RVA: 0x00003CCB File Offset: 0x00001ECB
		public static XPathNavigator CreateNavigator(this XNode node)
		{
			return node.CreateNavigator(null);
		}

		/// <summary>Creates an <see cref="T:System.Xml.XPath.XPathNavigator" /> for an <see cref="T:System.Xml.Linq.XNode" />. The <see cref="T:System.Xml.XmlNameTable" /> enables more efficient XPath expression processing.</summary>
		/// <param name="node">An <see cref="T:System.Xml.Linq.XNode" /> that can process an XPath query.</param>
		/// <param name="nameTable">A <see cref="T:System.Xml.XmlNameTable" /> to be used by <see cref="T:System.Xml.XPath.XPathNavigator" />.</param>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNavigator" /> that can process XPath queries.</returns>
		// Token: 0x0600005C RID: 92 RVA: 0x00003CD4 File Offset: 0x00001ED4
		public static XPathNavigator CreateNavigator(this XNode node, XmlNameTable nameTable)
		{
			if (node == null)
			{
				throw new ArgumentNullException("node");
			}
			if (node is XDocumentType)
			{
				throw new ArgumentException(Res.GetString("Argument_CreateNavigator", new object[]
				{
					XmlNodeType.DocumentType
				}));
			}
			XText xtext = node as XText;
			if (xtext != null)
			{
				if (xtext.parent is XDocument)
				{
					throw new ArgumentException(Res.GetString("Argument_CreateNavigator", new object[]
					{
						XmlNodeType.Whitespace
					}));
				}
				node = Extensions.CalibrateText(xtext);
			}
			return new XNodeNavigator(node, nameTable);
		}

		/// <summary>Evaluates an XPath expression.</summary>
		/// <param name="node">The <see cref="T:System.Xml.Linq.XNode" /> on which to evaluate the XPath expression.</param>
		/// <param name="expression">A <see cref="T:System.String" /> that contains an XPath expression.</param>
		/// <returns>An object that can contain a <see langword="bool" />, a <see langword="double" />, a <see langword="string" />, or an <see cref="T:System.Collections.Generic.IEnumerable`1" />. </returns>
		// Token: 0x0600005D RID: 93 RVA: 0x00003D5D File Offset: 0x00001F5D
		public static object XPathEvaluate(this XNode node, string expression)
		{
			return node.XPathEvaluate(expression, null);
		}

		/// <summary>Evaluates an XPath expression, resolving namespace prefixes using the specified <see cref="T:System.Xml.IXmlNamespaceResolver" />.</summary>
		/// <param name="node">The <see cref="T:System.Xml.Linq.XNode" /> on which to evaluate the XPath expression.</param>
		/// <param name="expression">A <see cref="T:System.String" /> that contains an XPath expression.</param>
		/// <param name="resolver">A <see cref="T:System.Xml.IXmlNamespaceResolver" /> for the namespace prefixes in the XPath expression.</param>
		/// <returns>An object that contains the result of evaluating the expression. The object can be a <see langword="bool" />, a <see langword="double" />, a <see langword="string" />, or an <see cref="T:System.Collections.Generic.IEnumerable`1" />.</returns>
		// Token: 0x0600005E RID: 94 RVA: 0x00003D68 File Offset: 0x00001F68
		public static object XPathEvaluate(this XNode node, string expression, IXmlNamespaceResolver resolver)
		{
			if (node == null)
			{
				throw new ArgumentNullException("node");
			}
			return default(XPathEvaluator).Evaluate<object>(node, expression, resolver);
		}

		/// <summary>Selects an <see cref="T:System.Xml.Linq.XElement" /> using a XPath expression.</summary>
		/// <param name="node">The <see cref="T:System.Xml.Linq.XNode" /> on which to evaluate the XPath expression.</param>
		/// <param name="expression">A <see cref="T:System.String" /> that contains an XPath expression.</param>
		/// <returns>An <see cref="T:System.Xml.Linq.XElement" />, or null.</returns>
		// Token: 0x0600005F RID: 95 RVA: 0x00003D94 File Offset: 0x00001F94
		public static XElement XPathSelectElement(this XNode node, string expression)
		{
			return node.XPathSelectElement(expression, null);
		}

		/// <summary>Selects an <see cref="T:System.Xml.Linq.XElement" /> using a XPath expression, resolving namespace prefixes using the specified <see cref="T:System.Xml.IXmlNamespaceResolver" />.</summary>
		/// <param name="node">The <see cref="T:System.Xml.Linq.XNode" /> on which to evaluate the XPath expression.</param>
		/// <param name="expression">A <see cref="T:System.String" /> that contains an XPath expression.</param>
		/// <param name="resolver">An <see cref="T:System.Xml.IXmlNamespaceResolver" /> for the namespace prefixes in the XPath expression.</param>
		/// <returns>An <see cref="T:System.Xml.Linq.XElement" />, or null.</returns>
		// Token: 0x06000060 RID: 96 RVA: 0x00003D9E File Offset: 0x00001F9E
		public static XElement XPathSelectElement(this XNode node, string expression, IXmlNamespaceResolver resolver)
		{
			return node.XPathSelectElements(expression, resolver).FirstOrDefault<XElement>();
		}

		/// <summary>Selects a collection of elements using an XPath expression.</summary>
		/// <param name="node">The <see cref="T:System.Xml.Linq.XNode" /> on which to evaluate the XPath expression.</param>
		/// <param name="expression">A <see cref="T:System.String" /> that contains an XPath expression.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the selected elements.</returns>
		// Token: 0x06000061 RID: 97 RVA: 0x00003DAD File Offset: 0x00001FAD
		public static IEnumerable<XElement> XPathSelectElements(this XNode node, string expression)
		{
			return node.XPathSelectElements(expression, null);
		}

		/// <summary>Selects a collection of elements using an XPath expression, resolving namespace prefixes using the specified <see cref="T:System.Xml.IXmlNamespaceResolver" />.</summary>
		/// <param name="node">The <see cref="T:System.Xml.Linq.XNode" /> on which to evaluate the XPath expression.</param>
		/// <param name="expression">A <see cref="T:System.String" /> that contains an XPath expression.</param>
		/// <param name="resolver">A <see cref="T:System.Xml.IXmlNamespaceResolver" /> for the namespace prefixes in the XPath expression.</param>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> of <see cref="T:System.Xml.Linq.XElement" /> that contains the selected elements.</returns>
		// Token: 0x06000062 RID: 98 RVA: 0x00003DB8 File Offset: 0x00001FB8
		public static IEnumerable<XElement> XPathSelectElements(this XNode node, string expression, IXmlNamespaceResolver resolver)
		{
			if (node == null)
			{
				throw new ArgumentNullException("node");
			}
			return (IEnumerable<XElement>)default(XPathEvaluator).Evaluate<XElement>(node, expression, resolver);
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00003DEC File Offset: 0x00001FEC
		private static XText CalibrateText(XText n)
		{
			if (n.parent == null)
			{
				return n;
			}
			XNode xnode = (XNode)n.parent.content;
			XText xtext;
			for (;;)
			{
				IL_1B:
				xnode = xnode.next;
				xtext = (xnode as XText);
				if (xtext != null)
				{
					while (xnode != n)
					{
						xnode = xnode.next;
						if (!(xnode is XText))
						{
							goto IL_1B;
						}
					}
					break;
				}
			}
			return xtext;
		}
	}
}
