﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x0200004E RID: 78
	internal class XElementElementsPropertyDescriptor : XPropertyDescriptor<XElement, IEnumerable<XElement>>
	{
		// Token: 0x06000378 RID: 888 RVA: 0x0000E679 File Offset: 0x0000C879
		public XElementElementsPropertyDescriptor() : base("Elements")
		{
		}

		// Token: 0x06000379 RID: 889 RVA: 0x0000E688 File Offset: 0x0000C888
		public override object GetValue(object component)
		{
			return this.value = new XDeferredAxis<XElement>(delegate(XElement e, XName n)
			{
				if (!(n != null))
				{
					return e.Elements();
				}
				return e.Elements(n);
			}, component as XElement, null);
		}

		// Token: 0x0600037A RID: 890 RVA: 0x0000E6CC File Offset: 0x0000C8CC
		protected override void OnChanged(object sender, XObjectChangeEventArgs args)
		{
			if (this.value == null)
			{
				return;
			}
			switch (args.ObjectChange)
			{
			case XObjectChange.Add:
			{
				XElement xelement = sender as XElement;
				if (xelement != null && this.value.element == xelement.parent && (this.value.name == xelement.Name || this.value.name == null))
				{
					this.OnValueChanged(this.value.element, EventArgs.Empty);
					return;
				}
				break;
			}
			case XObjectChange.Remove:
			{
				XElement xelement = sender as XElement;
				if (xelement != null && this.value.element == this.changeState as XContainer && (this.value.name == xelement.Name || this.value.name == null))
				{
					this.changeState = null;
					this.OnValueChanged(this.value.element, EventArgs.Empty);
					return;
				}
				break;
			}
			case XObjectChange.Name:
			{
				XElement xelement = sender as XElement;
				if (xelement != null && this.value.element == xelement.parent && this.value.name != null && (this.value.name == xelement.Name || this.value.name == this.changeState as XName))
				{
					this.changeState = null;
					this.OnValueChanged(this.value.element, EventArgs.Empty);
				}
				break;
			}
			default:
				return;
			}
		}

		// Token: 0x0600037B RID: 891 RVA: 0x0000E85C File Offset: 0x0000CA5C
		protected override void OnChanging(object sender, XObjectChangeEventArgs args)
		{
			if (this.value == null)
			{
				return;
			}
			XObjectChange objectChange = args.ObjectChange;
			XElement xelement;
			if (objectChange == XObjectChange.Remove)
			{
				xelement = (sender as XElement);
				this.changeState = ((xelement != null) ? xelement.parent : null);
				return;
			}
			if (objectChange != XObjectChange.Name)
			{
				return;
			}
			xelement = (sender as XElement);
			this.changeState = ((xelement != null) ? xelement.Name : null);
		}

		// Token: 0x0400014B RID: 331
		private XDeferredAxis<XElement> value;

		// Token: 0x0400014C RID: 332
		private object changeState;

		// Token: 0x0200004F RID: 79
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600037C RID: 892 RVA: 0x0000E8B5 File Offset: 0x0000CAB5
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600037D RID: 893 RVA: 0x00002966 File Offset: 0x00000B66
			public <>c()
			{
			}

			// Token: 0x0600037E RID: 894 RVA: 0x0000E8C1 File Offset: 0x0000CAC1
			internal IEnumerable<XElement> <GetValue>b__3_0(XElement e, XName n)
			{
				if (!(n != null))
				{
					return e.Elements();
				}
				return e.Elements(n);
			}

			// Token: 0x0400014D RID: 333
			public static readonly XElementElementsPropertyDescriptor.<>c <>9 = new XElementElementsPropertyDescriptor.<>c();

			// Token: 0x0400014E RID: 334
			public static Func<XElement, XName, IEnumerable<XElement>> <>9__3_0;
		}
	}
}
