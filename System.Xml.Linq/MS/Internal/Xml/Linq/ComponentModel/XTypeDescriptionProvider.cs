﻿using System;
using System.ComponentModel;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x02000045 RID: 69
	internal class XTypeDescriptionProvider<T> : TypeDescriptionProvider
	{
		// Token: 0x06000351 RID: 849 RVA: 0x0000DEE9 File Offset: 0x0000C0E9
		public XTypeDescriptionProvider() : base(TypeDescriptor.GetProvider(typeof(T)))
		{
		}

		// Token: 0x06000352 RID: 850 RVA: 0x0000DF00 File Offset: 0x0000C100
		public override ICustomTypeDescriptor GetTypeDescriptor(Type type, object instance)
		{
			return new XTypeDescriptor<T>(base.GetTypeDescriptor(type, instance));
		}
	}
}
