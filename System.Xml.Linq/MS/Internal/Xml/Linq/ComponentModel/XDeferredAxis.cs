﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x02000053 RID: 83
	internal class XDeferredAxis<T> : IEnumerable<!0>, IEnumerable where T : XObject
	{
		// Token: 0x0600038C RID: 908 RVA: 0x0000EA6B File Offset: 0x0000CC6B
		public XDeferredAxis(Func<XElement, XName, IEnumerable<T>> func, XElement element, XName name)
		{
			if (func == null)
			{
				throw new ArgumentNullException("func");
			}
			if (element == null)
			{
				throw new ArgumentNullException("element");
			}
			this.func = func;
			this.element = element;
			this.name = name;
		}

		// Token: 0x0600038D RID: 909 RVA: 0x0000EAA4 File Offset: 0x0000CCA4
		public IEnumerator<T> GetEnumerator()
		{
			return this.func(this.element, this.name).GetEnumerator();
		}

		// Token: 0x0600038E RID: 910 RVA: 0x0000EAC2 File Offset: 0x0000CCC2
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x1700008F RID: 143
		public IEnumerable<T> this[string expandedName]
		{
			get
			{
				if (expandedName == null)
				{
					throw new ArgumentNullException("expandedName");
				}
				if (this.name == null)
				{
					this.name = expandedName;
				}
				else if (this.name != expandedName)
				{
					return Enumerable.Empty<T>();
				}
				return this;
			}
		}

		// Token: 0x04000152 RID: 338
		private Func<XElement, XName, IEnumerable<T>> func;

		// Token: 0x04000153 RID: 339
		internal XElement element;

		// Token: 0x04000154 RID: 340
		internal XName name;
	}
}
