﻿using System;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x02000052 RID: 82
	internal class XAttributeValuePropertyDescriptor : XPropertyDescriptor<XAttribute, string>
	{
		// Token: 0x06000387 RID: 903 RVA: 0x0000E9EA File Offset: 0x0000CBEA
		public XAttributeValuePropertyDescriptor() : base("Value")
		{
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000388 RID: 904 RVA: 0x0000E0AF File Offset: 0x0000C2AF
		public override bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000389 RID: 905 RVA: 0x0000E9F7 File Offset: 0x0000CBF7
		public override object GetValue(object component)
		{
			this.attribute = (component as XAttribute);
			if (this.attribute == null)
			{
				return string.Empty;
			}
			return this.attribute.Value;
		}

		// Token: 0x0600038A RID: 906 RVA: 0x0000EA1E File Offset: 0x0000CC1E
		public override void SetValue(object component, object value)
		{
			this.attribute = (component as XAttribute);
			if (this.attribute == null)
			{
				return;
			}
			this.attribute.Value = (value as string);
		}

		// Token: 0x0600038B RID: 907 RVA: 0x0000EA46 File Offset: 0x0000CC46
		protected override void OnChanged(object sender, XObjectChangeEventArgs args)
		{
			if (this.attribute == null)
			{
				return;
			}
			if (args.ObjectChange == XObjectChange.Value)
			{
				this.OnValueChanged(this.attribute, EventArgs.Empty);
			}
		}

		// Token: 0x04000151 RID: 337
		private XAttribute attribute;
	}
}
