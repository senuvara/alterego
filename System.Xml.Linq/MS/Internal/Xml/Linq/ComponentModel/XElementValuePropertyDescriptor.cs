﻿using System;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x02000050 RID: 80
	internal class XElementValuePropertyDescriptor : XPropertyDescriptor<XElement, string>
	{
		// Token: 0x0600037F RID: 895 RVA: 0x0000E8DA File Offset: 0x0000CADA
		public XElementValuePropertyDescriptor() : base("Value")
		{
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000380 RID: 896 RVA: 0x0000E0AF File Offset: 0x0000C2AF
		public override bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000381 RID: 897 RVA: 0x0000E8E7 File Offset: 0x0000CAE7
		public override object GetValue(object component)
		{
			this.element = (component as XElement);
			if (this.element == null)
			{
				return string.Empty;
			}
			return this.element.Value;
		}

		// Token: 0x06000382 RID: 898 RVA: 0x0000E90E File Offset: 0x0000CB0E
		public override void SetValue(object component, object value)
		{
			this.element = (component as XElement);
			if (this.element == null)
			{
				return;
			}
			this.element.Value = (value as string);
		}

		// Token: 0x06000383 RID: 899 RVA: 0x0000E938 File Offset: 0x0000CB38
		protected override void OnChanged(object sender, XObjectChangeEventArgs args)
		{
			if (this.element == null)
			{
				return;
			}
			XObjectChange objectChange = args.ObjectChange;
			if (objectChange > XObjectChange.Remove)
			{
				if (objectChange != XObjectChange.Value)
				{
					return;
				}
				if (sender is XText)
				{
					this.OnValueChanged(this.element, EventArgs.Empty);
				}
			}
			else if (sender is XElement || sender is XText)
			{
				this.OnValueChanged(this.element, EventArgs.Empty);
				return;
			}
		}

		// Token: 0x0400014F RID: 335
		private XElement element;
	}
}
