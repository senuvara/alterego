﻿using System;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x0200004C RID: 76
	internal class XElementElementPropertyDescriptor : XPropertyDescriptor<XElement, object>
	{
		// Token: 0x06000371 RID: 881 RVA: 0x0000E423 File Offset: 0x0000C623
		public XElementElementPropertyDescriptor() : base("Element")
		{
		}

		// Token: 0x06000372 RID: 882 RVA: 0x0000E430 File Offset: 0x0000C630
		public override object GetValue(object component)
		{
			return this.value = new XDeferredSingleton<XElement>((XElement e, XName n) => e.Element(n), component as XElement, null);
		}

		// Token: 0x06000373 RID: 883 RVA: 0x0000E474 File Offset: 0x0000C674
		protected override void OnChanged(object sender, XObjectChangeEventArgs args)
		{
			if (this.value == null)
			{
				return;
			}
			switch (args.ObjectChange)
			{
			case XObjectChange.Add:
			{
				XElement xelement = sender as XElement;
				if (xelement != null && this.value.element == xelement.parent && this.value.name == xelement.Name && this.value.element.Element(this.value.name) == xelement)
				{
					this.OnValueChanged(this.value.element, EventArgs.Empty);
					return;
				}
				break;
			}
			case XObjectChange.Remove:
			{
				XElement xelement = sender as XElement;
				if (xelement != null && this.changeState == xelement)
				{
					this.changeState = null;
					this.OnValueChanged(this.value.element, EventArgs.Empty);
					return;
				}
				break;
			}
			case XObjectChange.Name:
			{
				XElement xelement = sender as XElement;
				if (xelement != null)
				{
					if (this.value.element == xelement.parent && this.value.name == xelement.Name && this.value.element.Element(this.value.name) == xelement)
					{
						this.OnValueChanged(this.value.element, EventArgs.Empty);
						return;
					}
					if (this.changeState == xelement)
					{
						this.changeState = null;
						this.OnValueChanged(this.value.element, EventArgs.Empty);
					}
				}
				break;
			}
			default:
				return;
			}
		}

		// Token: 0x06000374 RID: 884 RVA: 0x0000E5E4 File Offset: 0x0000C7E4
		protected override void OnChanging(object sender, XObjectChangeEventArgs args)
		{
			if (this.value == null)
			{
				return;
			}
			XObjectChange objectChange = args.ObjectChange;
			if (objectChange - XObjectChange.Remove <= 1)
			{
				XElement xelement = sender as XElement;
				this.changeState = ((xelement != null && this.value.element == xelement.parent && this.value.name == xelement.Name && this.value.element.Element(this.value.name) == xelement) ? xelement : null);
			}
		}

		// Token: 0x04000147 RID: 327
		private XDeferredSingleton<XElement> value;

		// Token: 0x04000148 RID: 328
		private XElement changeState;

		// Token: 0x0200004D RID: 77
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000375 RID: 885 RVA: 0x0000E664 File Offset: 0x0000C864
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000376 RID: 886 RVA: 0x00002966 File Offset: 0x00000B66
			public <>c()
			{
			}

			// Token: 0x06000377 RID: 887 RVA: 0x0000E670 File Offset: 0x0000C870
			internal XElement <GetValue>b__3_0(XElement e, XName n)
			{
				return e.Element(n);
			}

			// Token: 0x04000149 RID: 329
			public static readonly XElementElementPropertyDescriptor.<>c <>9 = new XElementElementPropertyDescriptor.<>c();

			// Token: 0x0400014A RID: 330
			public static Func<XElement, XName, XElement> <>9__3_0;
		}
	}
}
