﻿using System;
using System.ComponentModel;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x02000046 RID: 70
	internal class XTypeDescriptor<T> : CustomTypeDescriptor
	{
		// Token: 0x06000353 RID: 851 RVA: 0x0000DF0F File Offset: 0x0000C10F
		public XTypeDescriptor(ICustomTypeDescriptor parent) : base(parent)
		{
		}

		// Token: 0x06000354 RID: 852 RVA: 0x0000DF18 File Offset: 0x0000C118
		public override PropertyDescriptorCollection GetProperties()
		{
			return this.GetProperties(null);
		}

		// Token: 0x06000355 RID: 853 RVA: 0x0000DF24 File Offset: 0x0000C124
		public override PropertyDescriptorCollection GetProperties(Attribute[] attributes)
		{
			PropertyDescriptorCollection propertyDescriptorCollection = new PropertyDescriptorCollection(null);
			if (attributes == null)
			{
				if (typeof(T) == typeof(XElement))
				{
					propertyDescriptorCollection.Add(new XElementAttributePropertyDescriptor());
					propertyDescriptorCollection.Add(new XElementDescendantsPropertyDescriptor());
					propertyDescriptorCollection.Add(new XElementElementPropertyDescriptor());
					propertyDescriptorCollection.Add(new XElementElementsPropertyDescriptor());
					propertyDescriptorCollection.Add(new XElementValuePropertyDescriptor());
					propertyDescriptorCollection.Add(new XElementXmlPropertyDescriptor());
				}
				else if (typeof(T) == typeof(XAttribute))
				{
					propertyDescriptorCollection.Add(new XAttributeValuePropertyDescriptor());
				}
			}
			foreach (object obj in base.GetProperties(attributes))
			{
				PropertyDescriptor value = (PropertyDescriptor)obj;
				propertyDescriptorCollection.Add(value);
			}
			return propertyDescriptorCollection;
		}
	}
}
