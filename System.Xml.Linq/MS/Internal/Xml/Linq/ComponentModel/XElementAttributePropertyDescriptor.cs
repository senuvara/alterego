﻿using System;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x02000048 RID: 72
	internal class XElementAttributePropertyDescriptor : XPropertyDescriptor<XElement, object>
	{
		// Token: 0x06000363 RID: 867 RVA: 0x0000E116 File Offset: 0x0000C316
		public XElementAttributePropertyDescriptor() : base("Attribute")
		{
		}

		// Token: 0x06000364 RID: 868 RVA: 0x0000E124 File Offset: 0x0000C324
		public override object GetValue(object component)
		{
			return this.value = new XDeferredSingleton<XAttribute>((XElement e, XName n) => e.Attribute(n), component as XElement, null);
		}

		// Token: 0x06000365 RID: 869 RVA: 0x0000E168 File Offset: 0x0000C368
		protected override void OnChanged(object sender, XObjectChangeEventArgs args)
		{
			if (this.value == null)
			{
				return;
			}
			XObjectChange objectChange = args.ObjectChange;
			if (objectChange != XObjectChange.Add)
			{
				if (objectChange != XObjectChange.Remove)
				{
					return;
				}
				XAttribute xattribute = sender as XAttribute;
				if (xattribute != null && this.changeState == xattribute)
				{
					this.changeState = null;
					this.OnValueChanged(this.value.element, EventArgs.Empty);
				}
			}
			else
			{
				XAttribute xattribute = sender as XAttribute;
				if (xattribute != null && this.value.element == xattribute.parent && this.value.name == xattribute.Name)
				{
					this.OnValueChanged(this.value.element, EventArgs.Empty);
					return;
				}
			}
		}

		// Token: 0x06000366 RID: 870 RVA: 0x0000E20C File Offset: 0x0000C40C
		protected override void OnChanging(object sender, XObjectChangeEventArgs args)
		{
			if (this.value == null)
			{
				return;
			}
			XObjectChange objectChange = args.ObjectChange;
			if (objectChange == XObjectChange.Remove)
			{
				XAttribute xattribute = sender as XAttribute;
				this.changeState = ((xattribute != null && this.value.element == xattribute.parent && this.value.name == xattribute.Name) ? xattribute : null);
			}
		}

		// Token: 0x0400013F RID: 319
		private XDeferredSingleton<XAttribute> value;

		// Token: 0x04000140 RID: 320
		private XAttribute changeState;

		// Token: 0x02000049 RID: 73
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000367 RID: 871 RVA: 0x0000E26C File Offset: 0x0000C46C
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000368 RID: 872 RVA: 0x00002966 File Offset: 0x00000B66
			public <>c()
			{
			}

			// Token: 0x06000369 RID: 873 RVA: 0x0000E278 File Offset: 0x0000C478
			internal XAttribute <GetValue>b__3_0(XElement e, XName n)
			{
				return e.Attribute(n);
			}

			// Token: 0x04000141 RID: 321
			public static readonly XElementAttributePropertyDescriptor.<>c <>9 = new XElementAttributePropertyDescriptor.<>c();

			// Token: 0x04000142 RID: 322
			public static Func<XElement, XName, XAttribute> <>9__3_0;
		}
	}
}
