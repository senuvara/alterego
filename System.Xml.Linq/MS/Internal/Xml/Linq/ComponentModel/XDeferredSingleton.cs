﻿using System;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x02000054 RID: 84
	internal class XDeferredSingleton<T> where T : XObject
	{
		// Token: 0x06000390 RID: 912 RVA: 0x0000EB1D File Offset: 0x0000CD1D
		public XDeferredSingleton(Func<XElement, XName, T> func, XElement element, XName name)
		{
			if (func == null)
			{
				throw new ArgumentNullException("func");
			}
			if (element == null)
			{
				throw new ArgumentNullException("element");
			}
			this.func = func;
			this.element = element;
			this.name = name;
		}

		// Token: 0x17000090 RID: 144
		public T this[string expandedName]
		{
			get
			{
				if (expandedName == null)
				{
					throw new ArgumentNullException("expandedName");
				}
				if (this.name == null)
				{
					this.name = expandedName;
				}
				else if (this.name != expandedName)
				{
					return default(T);
				}
				return this.func(this.element, this.name);
			}
		}

		// Token: 0x04000155 RID: 341
		private Func<XElement, XName, T> func;

		// Token: 0x04000156 RID: 342
		internal XElement element;

		// Token: 0x04000157 RID: 343
		internal XName name;
	}
}
