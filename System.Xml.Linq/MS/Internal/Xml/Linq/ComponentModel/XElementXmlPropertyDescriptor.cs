﻿using System;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x02000051 RID: 81
	internal class XElementXmlPropertyDescriptor : XPropertyDescriptor<XElement, string>
	{
		// Token: 0x06000384 RID: 900 RVA: 0x0000E999 File Offset: 0x0000CB99
		public XElementXmlPropertyDescriptor() : base("Xml")
		{
		}

		// Token: 0x06000385 RID: 901 RVA: 0x0000E9A6 File Offset: 0x0000CBA6
		public override object GetValue(object component)
		{
			this.element = (component as XElement);
			if (this.element == null)
			{
				return string.Empty;
			}
			return this.element.ToString(SaveOptions.DisableFormatting);
		}

		// Token: 0x06000386 RID: 902 RVA: 0x0000E9CE File Offset: 0x0000CBCE
		protected override void OnChanged(object sender, XObjectChangeEventArgs args)
		{
			if (this.element == null)
			{
				return;
			}
			this.OnValueChanged(this.element, EventArgs.Empty);
		}

		// Token: 0x04000150 RID: 336
		private XElement element;
	}
}
