﻿using System;
using System.ComponentModel;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x02000047 RID: 71
	internal abstract class XPropertyDescriptor<T, TProperty> : PropertyDescriptor where T : XObject
	{
		// Token: 0x06000356 RID: 854 RVA: 0x0000E01C File Offset: 0x0000C21C
		public XPropertyDescriptor(string name) : base(name, null)
		{
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000357 RID: 855 RVA: 0x0000E026 File Offset: 0x0000C226
		public override Type ComponentType
		{
			get
			{
				return typeof(T);
			}
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000358 RID: 856 RVA: 0x00007DE2 File Offset: 0x00005FE2
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000359 RID: 857 RVA: 0x0000E032 File Offset: 0x0000C232
		public override Type PropertyType
		{
			get
			{
				return typeof(TProperty);
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600035A RID: 858 RVA: 0x00007DE2 File Offset: 0x00005FE2
		public override bool SupportsChangeEvents
		{
			get
			{
				return true;
			}
		}

		// Token: 0x0600035B RID: 859 RVA: 0x0000E040 File Offset: 0x0000C240
		public override void AddValueChanged(object component, EventHandler handler)
		{
			bool flag = base.GetValueChangedHandler(component) != null;
			base.AddValueChanged(component, handler);
			if (flag)
			{
				return;
			}
			T t = component as T;
			if (t != null && base.GetValueChangedHandler(component) != null)
			{
				t.Changing += this.OnChanging;
				t.Changed += this.OnChanged;
			}
		}

		// Token: 0x0600035C RID: 860 RVA: 0x0000E0AF File Offset: 0x0000C2AF
		public override bool CanResetValue(object component)
		{
			return false;
		}

		// Token: 0x0600035D RID: 861 RVA: 0x0000E0B4 File Offset: 0x0000C2B4
		public override void RemoveValueChanged(object component, EventHandler handler)
		{
			base.RemoveValueChanged(component, handler);
			T t = component as T;
			if (t != null && base.GetValueChangedHandler(component) == null)
			{
				t.Changing -= this.OnChanging;
				t.Changed -= this.OnChanged;
			}
		}

		// Token: 0x0600035E RID: 862 RVA: 0x00004D15 File Offset: 0x00002F15
		public override void ResetValue(object component)
		{
		}

		// Token: 0x0600035F RID: 863 RVA: 0x00004D15 File Offset: 0x00002F15
		public override void SetValue(object component, object value)
		{
		}

		// Token: 0x06000360 RID: 864 RVA: 0x0000E0AF File Offset: 0x0000C2AF
		public override bool ShouldSerializeValue(object component)
		{
			return false;
		}

		// Token: 0x06000361 RID: 865 RVA: 0x00004D15 File Offset: 0x00002F15
		protected virtual void OnChanged(object sender, XObjectChangeEventArgs args)
		{
		}

		// Token: 0x06000362 RID: 866 RVA: 0x00004D15 File Offset: 0x00002F15
		protected virtual void OnChanging(object sender, XObjectChangeEventArgs args)
		{
		}
	}
}
