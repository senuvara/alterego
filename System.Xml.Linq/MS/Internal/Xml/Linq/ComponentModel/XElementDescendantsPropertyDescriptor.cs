﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace MS.Internal.Xml.Linq.ComponentModel
{
	// Token: 0x0200004A RID: 74
	internal class XElementDescendantsPropertyDescriptor : XPropertyDescriptor<XElement, IEnumerable<XElement>>
	{
		// Token: 0x0600036A RID: 874 RVA: 0x0000E281 File Offset: 0x0000C481
		public XElementDescendantsPropertyDescriptor() : base("Descendants")
		{
		}

		// Token: 0x0600036B RID: 875 RVA: 0x0000E290 File Offset: 0x0000C490
		public override object GetValue(object component)
		{
			return this.value = new XDeferredAxis<XElement>(delegate(XElement e, XName n)
			{
				if (!(n != null))
				{
					return e.Descendants();
				}
				return e.Descendants(n);
			}, component as XElement, null);
		}

		// Token: 0x0600036C RID: 876 RVA: 0x0000E2D4 File Offset: 0x0000C4D4
		protected override void OnChanged(object sender, XObjectChangeEventArgs args)
		{
			if (this.value == null)
			{
				return;
			}
			XObjectChange objectChange = args.ObjectChange;
			if (objectChange > XObjectChange.Remove)
			{
				if (objectChange != XObjectChange.Name)
				{
					return;
				}
				XElement xelement = sender as XElement;
				if (xelement != null && this.value.element != xelement && this.value.name != null && (this.value.name == xelement.Name || this.value.name == this.changeState))
				{
					this.changeState = null;
					this.OnValueChanged(this.value.element, EventArgs.Empty);
				}
			}
			else
			{
				XElement xelement = sender as XElement;
				if (xelement != null && (this.value.name == xelement.Name || this.value.name == null))
				{
					this.OnValueChanged(this.value.element, EventArgs.Empty);
					return;
				}
			}
		}

		// Token: 0x0600036D RID: 877 RVA: 0x0000E3C4 File Offset: 0x0000C5C4
		protected override void OnChanging(object sender, XObjectChangeEventArgs args)
		{
			if (this.value == null)
			{
				return;
			}
			XObjectChange objectChange = args.ObjectChange;
			if (objectChange == XObjectChange.Name)
			{
				XElement xelement = sender as XElement;
				this.changeState = ((xelement != null) ? xelement.Name : null);
			}
		}

		// Token: 0x04000143 RID: 323
		private XDeferredAxis<XElement> value;

		// Token: 0x04000144 RID: 324
		private XName changeState;

		// Token: 0x0200004B RID: 75
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600036E RID: 878 RVA: 0x0000E3FE File Offset: 0x0000C5FE
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600036F RID: 879 RVA: 0x00002966 File Offset: 0x00000B66
			public <>c()
			{
			}

			// Token: 0x06000370 RID: 880 RVA: 0x0000E40A File Offset: 0x0000C60A
			internal IEnumerable<XElement> <GetValue>b__3_0(XElement e, XName n)
			{
				if (!(n != null))
				{
					return e.Descendants();
				}
				return e.Descendants(n);
			}

			// Token: 0x04000145 RID: 325
			public static readonly XElementDescendantsPropertyDescriptor.<>c <>9 = new XElementDescendantsPropertyDescriptor.<>c();

			// Token: 0x04000146 RID: 326
			public static Func<XElement, XName, IEnumerable<XElement>> <>9__3_0;
		}
	}
}
