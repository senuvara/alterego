﻿using System;

namespace UnityEngine.XR.Tango
{
	// Token: 0x02000002 RID: 2
	internal enum PoseStatus
	{
		// Token: 0x04000002 RID: 2
		Initializing,
		// Token: 0x04000003 RID: 3
		Valid,
		// Token: 0x04000004 RID: 4
		Invalid,
		// Token: 0x04000005 RID: 5
		Unknown
	}
}
