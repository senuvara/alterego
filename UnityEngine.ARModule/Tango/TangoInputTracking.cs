﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.XR.Tango
{
	// Token: 0x02000004 RID: 4
	[NativeConditional("PLATFORM_ANDROID")]
	[NativeHeader("Runtime/AR/Tango/TangoScriptApi.h")]
	internal static class TangoInputTracking
	{
		// Token: 0x06000003 RID: 3
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_TryGetPoseAtTime(out PoseData pose);

		// Token: 0x06000004 RID: 4 RVA: 0x000020B8 File Offset: 0x000002B8
		internal static bool TryGetPoseAtTime(out PoseData pose)
		{
			return TangoInputTracking.Internal_TryGetPoseAtTime(out pose);
		}
	}
}
