﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Rendering;

namespace UnityEngine.XR
{
	// Token: 0x02000006 RID: 6
	public class ARBackgroundRenderer
	{
		// Token: 0x06000005 RID: 5 RVA: 0x000020D3 File Offset: 0x000002D3
		public ARBackgroundRenderer()
		{
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000006 RID: 6 RVA: 0x0000210C File Offset: 0x0000030C
		// (remove) Token: 0x06000007 RID: 7 RVA: 0x00002144 File Offset: 0x00000344
		public event Action backgroundRendererChanged
		{
			add
			{
				Action action = this.backgroundRendererChanged;
				Action action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action>(ref this.backgroundRendererChanged, (Action)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action action = this.backgroundRendererChanged;
				Action action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action>(ref this.backgroundRendererChanged, (Action)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000008 RID: 8 RVA: 0x0000217C File Offset: 0x0000037C
		// (set) Token: 0x06000009 RID: 9 RVA: 0x00002198 File Offset: 0x00000398
		public Material backgroundMaterial
		{
			get
			{
				return this.m_BackgroundMaterial;
			}
			set
			{
				if (!(this.m_BackgroundMaterial == value))
				{
					this.RemoveCommandBuffersIfNeeded();
					this.m_BackgroundMaterial = value;
					if (this.backgroundRendererChanged != null)
					{
						this.backgroundRendererChanged();
					}
					this.ReapplyCommandBuffersIfNeeded();
				}
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600000A RID: 10 RVA: 0x000021E8 File Offset: 0x000003E8
		// (set) Token: 0x0600000B RID: 11 RVA: 0x00002204 File Offset: 0x00000404
		public Texture backgroundTexture
		{
			get
			{
				return this.m_BackgroundTexture;
			}
			set
			{
				this.m_BackgroundTexture = value;
				if (!value)
				{
					this.RemoveCommandBuffersIfNeeded();
					this.m_BackgroundTexture = value;
					if (this.backgroundRendererChanged != null)
					{
						this.backgroundRendererChanged();
					}
					this.ReapplyCommandBuffersIfNeeded();
				}
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600000C RID: 12 RVA: 0x00002258 File Offset: 0x00000458
		// (set) Token: 0x0600000D RID: 13 RVA: 0x00002290 File Offset: 0x00000490
		public Camera camera
		{
			get
			{
				return (!(this.m_Camera != null)) ? Camera.main : this.m_Camera;
			}
			set
			{
				if (!(this.m_Camera == value))
				{
					this.RemoveCommandBuffersIfNeeded();
					this.m_Camera = value;
					if (this.backgroundRendererChanged != null)
					{
						this.backgroundRendererChanged();
					}
					this.ReapplyCommandBuffersIfNeeded();
				}
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600000E RID: 14 RVA: 0x000022E0 File Offset: 0x000004E0
		// (set) Token: 0x0600000F RID: 15 RVA: 0x000022FC File Offset: 0x000004FC
		public ARRenderMode mode
		{
			get
			{
				return this.m_RenderMode;
			}
			set
			{
				if (value != this.m_RenderMode)
				{
					this.m_RenderMode = value;
					ARRenderMode renderMode = this.m_RenderMode;
					if (renderMode != ARRenderMode.StandardBackground)
					{
						if (renderMode != ARRenderMode.MaterialAsBackground)
						{
							throw new Exception("Unhandled render mode.");
						}
						this.EnableARBackgroundRendering();
					}
					else
					{
						this.DisableARBackgroundRendering();
					}
					if (this.backgroundRendererChanged != null)
					{
						this.backgroundRendererChanged();
					}
				}
			}
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002374 File Offset: 0x00000574
		protected bool EnableARBackgroundRendering()
		{
			bool result;
			if (this.m_BackgroundMaterial == null)
			{
				result = false;
			}
			else
			{
				Camera camera;
				if (this.m_Camera != null)
				{
					camera = this.m_Camera;
				}
				else
				{
					camera = Camera.main;
				}
				if (camera == null)
				{
					result = false;
				}
				else
				{
					this.m_CameraClearFlags = camera.clearFlags;
					camera.clearFlags = CameraClearFlags.Depth;
					this.m_CommandBuffer = new CommandBuffer();
					Texture texture = this.m_BackgroundTexture;
					if (texture == null)
					{
						if (this.m_BackgroundMaterial.HasProperty("_MainTex"))
						{
							texture = this.m_BackgroundMaterial.GetTexture("_MainTex");
						}
					}
					this.m_CommandBuffer.Blit(texture, BuiltinRenderTextureType.CameraTarget, this.m_BackgroundMaterial);
					camera.AddCommandBuffer(CameraEvent.BeforeForwardOpaque, this.m_CommandBuffer);
					camera.AddCommandBuffer(CameraEvent.BeforeGBuffer, this.m_CommandBuffer);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002464 File Offset: 0x00000664
		protected void DisableARBackgroundRendering()
		{
			if (this.m_CommandBuffer != null)
			{
				Camera camera = this.m_Camera ?? Camera.main;
				if (!(camera == null))
				{
					camera.clearFlags = this.m_CameraClearFlags;
					camera.RemoveCommandBuffer(CameraEvent.BeforeForwardOpaque, this.m_CommandBuffer);
					camera.RemoveCommandBuffer(CameraEvent.BeforeGBuffer, this.m_CommandBuffer);
				}
			}
		}

		// Token: 0x06000012 RID: 18 RVA: 0x000024D0 File Offset: 0x000006D0
		private bool ReapplyCommandBuffersIfNeeded()
		{
			bool result;
			if (this.m_RenderMode != ARRenderMode.MaterialAsBackground)
			{
				result = false;
			}
			else
			{
				this.EnableARBackgroundRendering();
				result = true;
			}
			return result;
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00002500 File Offset: 0x00000700
		private bool RemoveCommandBuffersIfNeeded()
		{
			bool result;
			if (this.m_RenderMode != ARRenderMode.MaterialAsBackground)
			{
				result = false;
			}
			else
			{
				this.DisableARBackgroundRendering();
				result = true;
			}
			return result;
		}

		// Token: 0x04000011 RID: 17
		protected Camera m_Camera = null;

		// Token: 0x04000012 RID: 18
		protected Material m_BackgroundMaterial = null;

		// Token: 0x04000013 RID: 19
		protected Texture m_BackgroundTexture = null;

		// Token: 0x04000014 RID: 20
		private ARRenderMode m_RenderMode = ARRenderMode.StandardBackground;

		// Token: 0x04000015 RID: 21
		private CommandBuffer m_CommandBuffer = null;

		// Token: 0x04000016 RID: 22
		private CameraClearFlags m_CameraClearFlags = CameraClearFlags.Skybox;

		// Token: 0x04000017 RID: 23
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Action backgroundRendererChanged = null;
	}
}
