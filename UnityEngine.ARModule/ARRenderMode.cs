﻿using System;

namespace UnityEngine.XR
{
	// Token: 0x02000005 RID: 5
	public enum ARRenderMode
	{
		// Token: 0x0400000F RID: 15
		StandardBackground,
		// Token: 0x04000010 RID: 16
		MaterialAsBackground
	}
}
