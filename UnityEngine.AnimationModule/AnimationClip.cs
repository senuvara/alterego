﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200000D RID: 13
	[NativeType("Runtime/Animation/AnimationClip.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimationClip.bindings.h")]
	public sealed class AnimationClip : Motion
	{
		// Token: 0x06000056 RID: 86 RVA: 0x000024FF File Offset: 0x000006FF
		public AnimationClip()
		{
			AnimationClip.Internal_CreateAnimationClip(this);
		}

		// Token: 0x06000057 RID: 87
		[FreeFunction("AnimationClipBindings::Internal_CreateAnimationClip")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateAnimationClip([Writable] AnimationClip self);

		// Token: 0x06000058 RID: 88 RVA: 0x0000250E File Offset: 0x0000070E
		public void SampleAnimation(GameObject go, float time)
		{
			AnimationClip.SampleAnimation(go, this, time, this.wrapMode);
		}

		// Token: 0x06000059 RID: 89
		[FreeFunction]
		[NativeHeader("Runtime/Animation/AnimationUtility.h")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SampleAnimation([NotNull] GameObject go, [NotNull] AnimationClip clip, float inTime, WrapMode wrapMode);

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600005A RID: 90
		[NativeProperty("Length", false, TargetType.Function)]
		public extern float length { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600005B RID: 91
		[NativeProperty("StartTime", false, TargetType.Function)]
		internal extern float startTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600005C RID: 92
		[NativeProperty("StopTime", false, TargetType.Function)]
		internal extern float stopTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600005D RID: 93
		// (set) Token: 0x0600005E RID: 94
		[NativeProperty("SampleRate", false, TargetType.Function)]
		public extern float frameRate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600005F RID: 95
		[FreeFunction("AnimationClipBindings::Internal_SetCurve", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetCurve([NotNull] string relativePath, Type type, [NotNull] string propertyName, AnimationCurve curve);

		// Token: 0x06000060 RID: 96
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void EnsureQuaternionContinuity();

		// Token: 0x06000061 RID: 97
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ClearCurves();

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000062 RID: 98
		// (set) Token: 0x06000063 RID: 99
		[NativeProperty("WrapMode", false, TargetType.Function)]
		public extern WrapMode wrapMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000064 RID: 100 RVA: 0x00002520 File Offset: 0x00000720
		// (set) Token: 0x06000065 RID: 101 RVA: 0x00002536 File Offset: 0x00000736
		[NativeProperty("Bounds", false, TargetType.Function)]
		public Bounds localBounds
		{
			get
			{
				Bounds result;
				this.get_localBounds_Injected(out result);
				return result;
			}
			set
			{
				this.set_localBounds_Injected(ref value);
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000066 RID: 102
		// (set) Token: 0x06000067 RID: 103
		public new extern bool legacy { [NativeMethod("IsLegacy")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetLegacy")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000068 RID: 104
		public extern bool humanMotion { [NativeMethod("IsHumanMotion")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000069 RID: 105
		public extern bool empty { [NativeMethod("IsEmpty")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600006A RID: 106
		public extern bool hasGenericRootTransform { [NativeMethod("HasGenericRootTransform")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600006B RID: 107
		public extern bool hasMotionFloatCurves { [NativeMethod("HasMotionFloatCurves")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600006C RID: 108
		public extern bool hasMotionCurves { [NativeMethod("HasMotionCurves")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600006D RID: 109
		public extern bool hasRootCurves { [NativeMethod("HasRootCurves")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600006E RID: 110
		internal extern bool hasRootMotion { [FreeFunction(Name = "AnimationClipBindings::Internal_GetHasRootMotion", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600006F RID: 111 RVA: 0x00002540 File Offset: 0x00000740
		public void AddEvent(AnimationEvent evt)
		{
			if (evt == null)
			{
				throw new ArgumentNullException("evt");
			}
			this.AddEventInternal(evt);
		}

		// Token: 0x06000070 RID: 112
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void AddEventInternal(object evt);

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000071 RID: 113 RVA: 0x0000255C File Offset: 0x0000075C
		// (set) Token: 0x06000072 RID: 114 RVA: 0x0000257C File Offset: 0x0000077C
		public AnimationEvent[] events
		{
			get
			{
				return (AnimationEvent[])this.GetEventsInternal();
			}
			set
			{
				this.SetEventsInternal(value);
			}
		}

		// Token: 0x06000073 RID: 115
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetEventsInternal(Array value);

		// Token: 0x06000074 RID: 116
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Array GetEventsInternal();

		// Token: 0x06000075 RID: 117
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_localBounds_Injected(out Bounds ret);

		// Token: 0x06000076 RID: 118
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_localBounds_Injected(ref Bounds value);
	}
}
