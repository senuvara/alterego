﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002E RID: 46
	[NativeHeader("Runtime/Animation/Animator.h")]
	public struct MatchTargetWeightMask
	{
		// Token: 0x06000264 RID: 612 RVA: 0x00004F8B File Offset: 0x0000318B
		public MatchTargetWeightMask(Vector3 positionXYZWeight, float rotationWeight)
		{
			this.m_PositionXYZWeight = positionXYZWeight;
			this.m_RotationWeight = rotationWeight;
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000265 RID: 613 RVA: 0x00004F9C File Offset: 0x0000319C
		// (set) Token: 0x06000266 RID: 614 RVA: 0x00004FB7 File Offset: 0x000031B7
		public Vector3 positionXYZWeight
		{
			get
			{
				return this.m_PositionXYZWeight;
			}
			set
			{
				this.m_PositionXYZWeight = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000267 RID: 615 RVA: 0x00004FC4 File Offset: 0x000031C4
		// (set) Token: 0x06000268 RID: 616 RVA: 0x00004FDF File Offset: 0x000031DF
		public float rotationWeight
		{
			get
			{
				return this.m_RotationWeight;
			}
			set
			{
				this.m_RotationWeight = value;
			}
		}

		// Token: 0x0400007C RID: 124
		private Vector3 m_PositionXYZWeight;

		// Token: 0x0400007D RID: 125
		private float m_RotationWeight;
	}
}
