﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000053 RID: 83
	[NativeHeader("Runtime/Animation/Motion.h")]
	public class Motion : Object
	{
		// Token: 0x06000537 RID: 1335 RVA: 0x000069D4 File Offset: 0x00004BD4
		protected Motion()
		{
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x06000538 RID: 1336
		public extern float averageDuration { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x06000539 RID: 1337
		public extern float averageAngularSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x0600053A RID: 1338 RVA: 0x00007768 File Offset: 0x00005968
		public Vector3 averageSpeed
		{
			get
			{
				Vector3 result;
				this.get_averageSpeed_Injected(out result);
				return result;
			}
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x0600053B RID: 1339
		public extern float apparentSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x0600053C RID: 1340
		public extern bool isLooping { [NativeMethod("IsLooping")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x0600053D RID: 1341
		public extern bool legacy { [NativeMethod("IsLegacy")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x0600053E RID: 1342
		public extern bool isHumanMotion { [NativeMethod("IsHumanMotion")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600053F RID: 1343 RVA: 0x00007780 File Offset: 0x00005980
		[Obsolete("ValidateIfRetargetable is not supported anymore, please use isHumanMotion instead.", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ValidateIfRetargetable(bool val)
		{
			return false;
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000540 RID: 1344 RVA: 0x00007798 File Offset: 0x00005998
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("isAnimatorMotion is not supported anymore, please use !legacy instead.", true)]
		public bool isAnimatorMotion
		{
			[CompilerGenerated]
			get
			{
				return this.<isAnimatorMotion>k__BackingField;
			}
		}

		// Token: 0x06000541 RID: 1345
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_averageSpeed_Injected(out Vector3 ret);

		// Token: 0x04000155 RID: 341
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly bool <isAnimatorMotion>k__BackingField;
	}
}
