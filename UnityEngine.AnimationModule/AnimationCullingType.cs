﻿using System;

namespace UnityEngine
{
	// Token: 0x0200005D RID: 93
	public enum AnimationCullingType
	{
		// Token: 0x04000175 RID: 373
		AlwaysAnimate,
		// Token: 0x04000176 RID: 374
		BasedOnRenderers,
		// Token: 0x04000177 RID: 375
		[Obsolete("Enum member AnimatorCullingMode.BasedOnClipBounds has been deprecated. Use AnimationCullingType.AlwaysAnimate or AnimationCullingType.BasedOnRenderers instead")]
		BasedOnClipBounds,
		// Token: 0x04000178 RID: 376
		[Obsolete("Enum member AnimatorCullingMode.BasedOnUserBounds has been deprecated. Use AnimationCullingType.AlwaysAnimate or AnimationCullingType.BasedOnRenderers instead")]
		BasedOnUserBounds
	}
}
