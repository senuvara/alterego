﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003E RID: 62
	public enum HumanBodyBones
	{
		// Token: 0x040000E1 RID: 225
		Hips,
		// Token: 0x040000E2 RID: 226
		LeftUpperLeg,
		// Token: 0x040000E3 RID: 227
		RightUpperLeg,
		// Token: 0x040000E4 RID: 228
		LeftLowerLeg,
		// Token: 0x040000E5 RID: 229
		RightLowerLeg,
		// Token: 0x040000E6 RID: 230
		LeftFoot,
		// Token: 0x040000E7 RID: 231
		RightFoot,
		// Token: 0x040000E8 RID: 232
		Spine,
		// Token: 0x040000E9 RID: 233
		Chest,
		// Token: 0x040000EA RID: 234
		UpperChest = 54,
		// Token: 0x040000EB RID: 235
		Neck = 9,
		// Token: 0x040000EC RID: 236
		Head,
		// Token: 0x040000ED RID: 237
		LeftShoulder,
		// Token: 0x040000EE RID: 238
		RightShoulder,
		// Token: 0x040000EF RID: 239
		LeftUpperArm,
		// Token: 0x040000F0 RID: 240
		RightUpperArm,
		// Token: 0x040000F1 RID: 241
		LeftLowerArm,
		// Token: 0x040000F2 RID: 242
		RightLowerArm,
		// Token: 0x040000F3 RID: 243
		LeftHand,
		// Token: 0x040000F4 RID: 244
		RightHand,
		// Token: 0x040000F5 RID: 245
		LeftToes,
		// Token: 0x040000F6 RID: 246
		RightToes,
		// Token: 0x040000F7 RID: 247
		LeftEye,
		// Token: 0x040000F8 RID: 248
		RightEye,
		// Token: 0x040000F9 RID: 249
		Jaw,
		// Token: 0x040000FA RID: 250
		LeftThumbProximal,
		// Token: 0x040000FB RID: 251
		LeftThumbIntermediate,
		// Token: 0x040000FC RID: 252
		LeftThumbDistal,
		// Token: 0x040000FD RID: 253
		LeftIndexProximal,
		// Token: 0x040000FE RID: 254
		LeftIndexIntermediate,
		// Token: 0x040000FF RID: 255
		LeftIndexDistal,
		// Token: 0x04000100 RID: 256
		LeftMiddleProximal,
		// Token: 0x04000101 RID: 257
		LeftMiddleIntermediate,
		// Token: 0x04000102 RID: 258
		LeftMiddleDistal,
		// Token: 0x04000103 RID: 259
		LeftRingProximal,
		// Token: 0x04000104 RID: 260
		LeftRingIntermediate,
		// Token: 0x04000105 RID: 261
		LeftRingDistal,
		// Token: 0x04000106 RID: 262
		LeftLittleProximal,
		// Token: 0x04000107 RID: 263
		LeftLittleIntermediate,
		// Token: 0x04000108 RID: 264
		LeftLittleDistal,
		// Token: 0x04000109 RID: 265
		RightThumbProximal,
		// Token: 0x0400010A RID: 266
		RightThumbIntermediate,
		// Token: 0x0400010B RID: 267
		RightThumbDistal,
		// Token: 0x0400010C RID: 268
		RightIndexProximal,
		// Token: 0x0400010D RID: 269
		RightIndexIntermediate,
		// Token: 0x0400010E RID: 270
		RightIndexDistal,
		// Token: 0x0400010F RID: 271
		RightMiddleProximal,
		// Token: 0x04000110 RID: 272
		RightMiddleIntermediate,
		// Token: 0x04000111 RID: 273
		RightMiddleDistal,
		// Token: 0x04000112 RID: 274
		RightRingProximal,
		// Token: 0x04000113 RID: 275
		RightRingIntermediate,
		// Token: 0x04000114 RID: 276
		RightRingDistal,
		// Token: 0x04000115 RID: 277
		RightLittleProximal,
		// Token: 0x04000116 RID: 278
		RightLittleIntermediate,
		// Token: 0x04000117 RID: 279
		RightLittleDistal,
		// Token: 0x04000118 RID: 280
		LastBone = 55
	}
}
