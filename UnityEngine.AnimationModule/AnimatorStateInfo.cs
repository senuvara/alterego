﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200002C RID: 44
	[NativeHeader("Runtime/Animation/AnimatorInfo.h")]
	[RequiredByNativeCode]
	public struct AnimatorStateInfo
	{
		// Token: 0x0600024E RID: 590 RVA: 0x00004CB0 File Offset: 0x00002EB0
		public bool IsName(string name)
		{
			int num = Animator.StringToHash(name);
			return num == this.m_FullPath || num == this.m_Name || num == this.m_Path;
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600024F RID: 591 RVA: 0x00004CF0 File Offset: 0x00002EF0
		public int fullPathHash
		{
			get
			{
				return this.m_FullPath;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000250 RID: 592 RVA: 0x00004D0C File Offset: 0x00002F0C
		[Obsolete("AnimatorStateInfo.nameHash has been deprecated. Use AnimatorStateInfo.fullPathHash instead.")]
		public int nameHash
		{
			get
			{
				return this.m_Path;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000251 RID: 593 RVA: 0x00004D28 File Offset: 0x00002F28
		public int shortNameHash
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000252 RID: 594 RVA: 0x00004D44 File Offset: 0x00002F44
		public float normalizedTime
		{
			get
			{
				return this.m_NormalizedTime;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000253 RID: 595 RVA: 0x00004D60 File Offset: 0x00002F60
		public float length
		{
			get
			{
				return this.m_Length;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000254 RID: 596 RVA: 0x00004D7C File Offset: 0x00002F7C
		public float speed
		{
			get
			{
				return this.m_Speed;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000255 RID: 597 RVA: 0x00004D98 File Offset: 0x00002F98
		public float speedMultiplier
		{
			get
			{
				return this.m_SpeedMultiplier;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000256 RID: 598 RVA: 0x00004DB4 File Offset: 0x00002FB4
		public int tagHash
		{
			get
			{
				return this.m_Tag;
			}
		}

		// Token: 0x06000257 RID: 599 RVA: 0x00004DD0 File Offset: 0x00002FD0
		public bool IsTag(string tag)
		{
			return Animator.StringToHash(tag) == this.m_Tag;
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000258 RID: 600 RVA: 0x00004DF4 File Offset: 0x00002FF4
		public bool loop
		{
			get
			{
				return this.m_Loop != 0;
			}
		}

		// Token: 0x0400006B RID: 107
		private int m_Name;

		// Token: 0x0400006C RID: 108
		private int m_Path;

		// Token: 0x0400006D RID: 109
		private int m_FullPath;

		// Token: 0x0400006E RID: 110
		private float m_NormalizedTime;

		// Token: 0x0400006F RID: 111
		private float m_Length;

		// Token: 0x04000070 RID: 112
		private float m_Speed;

		// Token: 0x04000071 RID: 113
		private float m_SpeedMultiplier;

		// Token: 0x04000072 RID: 114
		private int m_Tag;

		// Token: 0x04000073 RID: 115
		private int m_Loop;
	}
}
