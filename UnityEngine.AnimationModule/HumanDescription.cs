﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000044 RID: 68
	[NativeHeader("Runtime/Animation/HumanDescription.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/AvatarBuilder.bindings.h")]
	public struct HumanDescription
	{
		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x06000455 RID: 1109 RVA: 0x00006C78 File Offset: 0x00004E78
		// (set) Token: 0x06000456 RID: 1110 RVA: 0x00006C93 File Offset: 0x00004E93
		public float upperArmTwist
		{
			get
			{
				return this.m_ArmTwist;
			}
			set
			{
				this.m_ArmTwist = value;
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x06000457 RID: 1111 RVA: 0x00006CA0 File Offset: 0x00004EA0
		// (set) Token: 0x06000458 RID: 1112 RVA: 0x00006CBB File Offset: 0x00004EBB
		public float lowerArmTwist
		{
			get
			{
				return this.m_ForeArmTwist;
			}
			set
			{
				this.m_ForeArmTwist = value;
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x06000459 RID: 1113 RVA: 0x00006CC8 File Offset: 0x00004EC8
		// (set) Token: 0x0600045A RID: 1114 RVA: 0x00006CE3 File Offset: 0x00004EE3
		public float upperLegTwist
		{
			get
			{
				return this.m_UpperLegTwist;
			}
			set
			{
				this.m_UpperLegTwist = value;
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x0600045B RID: 1115 RVA: 0x00006CF0 File Offset: 0x00004EF0
		// (set) Token: 0x0600045C RID: 1116 RVA: 0x00006D0B File Offset: 0x00004F0B
		public float lowerLegTwist
		{
			get
			{
				return this.m_LegTwist;
			}
			set
			{
				this.m_LegTwist = value;
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x0600045D RID: 1117 RVA: 0x00006D18 File Offset: 0x00004F18
		// (set) Token: 0x0600045E RID: 1118 RVA: 0x00006D33 File Offset: 0x00004F33
		public float armStretch
		{
			get
			{
				return this.m_ArmStretch;
			}
			set
			{
				this.m_ArmStretch = value;
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x0600045F RID: 1119 RVA: 0x00006D40 File Offset: 0x00004F40
		// (set) Token: 0x06000460 RID: 1120 RVA: 0x00006D5B File Offset: 0x00004F5B
		public float legStretch
		{
			get
			{
				return this.m_LegStretch;
			}
			set
			{
				this.m_LegStretch = value;
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000461 RID: 1121 RVA: 0x00006D68 File Offset: 0x00004F68
		// (set) Token: 0x06000462 RID: 1122 RVA: 0x00006D83 File Offset: 0x00004F83
		public float feetSpacing
		{
			get
			{
				return this.m_FeetSpacing;
			}
			set
			{
				this.m_FeetSpacing = value;
			}
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000463 RID: 1123 RVA: 0x00006D90 File Offset: 0x00004F90
		// (set) Token: 0x06000464 RID: 1124 RVA: 0x00006DAB File Offset: 0x00004FAB
		public bool hasTranslationDoF
		{
			get
			{
				return this.m_HasTranslationDoF;
			}
			set
			{
				this.m_HasTranslationDoF = value;
			}
		}

		// Token: 0x0400012E RID: 302
		[NativeName("m_Human")]
		public HumanBone[] human;

		// Token: 0x0400012F RID: 303
		[NativeName("m_Skeleton")]
		public SkeletonBone[] skeleton;

		// Token: 0x04000130 RID: 304
		internal float m_ArmTwist;

		// Token: 0x04000131 RID: 305
		internal float m_ForeArmTwist;

		// Token: 0x04000132 RID: 306
		internal float m_UpperLegTwist;

		// Token: 0x04000133 RID: 307
		internal float m_LegTwist;

		// Token: 0x04000134 RID: 308
		internal float m_ArmStretch;

		// Token: 0x04000135 RID: 309
		internal float m_LegStretch;

		// Token: 0x04000136 RID: 310
		internal float m_FeetSpacing;

		// Token: 0x04000137 RID: 311
		internal string m_RootMotionBoneName;

		// Token: 0x04000138 RID: 312
		internal bool m_HasTranslationDoF;

		// Token: 0x04000139 RID: 313
		internal bool m_HasExtraRoot;

		// Token: 0x0400013A RID: 314
		internal bool m_SkeletonHasParents;
	}
}
