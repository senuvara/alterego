﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000042 RID: 66
	[NativeType(CodegenOptions.Custom, "MonoHumanLimit")]
	[NativeHeader("Runtime/Animation/HumanDescription.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/AvatarBuilder.bindings.h")]
	public struct HumanLimit
	{
		// Token: 0x1700009C RID: 156
		// (get) Token: 0x06000447 RID: 1095 RVA: 0x00006B50 File Offset: 0x00004D50
		// (set) Token: 0x06000448 RID: 1096 RVA: 0x00006B71 File Offset: 0x00004D71
		public bool useDefaultValues
		{
			get
			{
				return this.m_UseDefaultValues != 0;
			}
			set
			{
				this.m_UseDefaultValues = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x06000449 RID: 1097 RVA: 0x00006B88 File Offset: 0x00004D88
		// (set) Token: 0x0600044A RID: 1098 RVA: 0x00006BA3 File Offset: 0x00004DA3
		public Vector3 min
		{
			get
			{
				return this.m_Min;
			}
			set
			{
				this.m_Min = value;
			}
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x0600044B RID: 1099 RVA: 0x00006BB0 File Offset: 0x00004DB0
		// (set) Token: 0x0600044C RID: 1100 RVA: 0x00006BCB File Offset: 0x00004DCB
		public Vector3 max
		{
			get
			{
				return this.m_Max;
			}
			set
			{
				this.m_Max = value;
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x0600044D RID: 1101 RVA: 0x00006BD8 File Offset: 0x00004DD8
		// (set) Token: 0x0600044E RID: 1102 RVA: 0x00006BF3 File Offset: 0x00004DF3
		public Vector3 center
		{
			get
			{
				return this.m_Center;
			}
			set
			{
				this.m_Center = value;
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x0600044F RID: 1103 RVA: 0x00006C00 File Offset: 0x00004E00
		// (set) Token: 0x06000450 RID: 1104 RVA: 0x00006C1B File Offset: 0x00004E1B
		public float axisLength
		{
			get
			{
				return this.m_AxisLength;
			}
			set
			{
				this.m_AxisLength = value;
			}
		}

		// Token: 0x04000126 RID: 294
		private Vector3 m_Min;

		// Token: 0x04000127 RID: 295
		private Vector3 m_Max;

		// Token: 0x04000128 RID: 296
		private Vector3 m_Center;

		// Token: 0x04000129 RID: 297
		private float m_AxisLength;

		// Token: 0x0400012A RID: 298
		private int m_UseDefaultValues;
	}
}
