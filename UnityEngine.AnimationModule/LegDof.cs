﻿using System;

namespace UnityEngine
{
	// Token: 0x02000039 RID: 57
	public enum LegDof
	{
		// Token: 0x040000A2 RID: 162
		UpperLegFrontBack,
		// Token: 0x040000A3 RID: 163
		UpperLegInOut,
		// Token: 0x040000A4 RID: 164
		UpperLegRollInOut,
		// Token: 0x040000A5 RID: 165
		LegCloseOpen,
		// Token: 0x040000A6 RID: 166
		LegRollInOut,
		// Token: 0x040000A7 RID: 167
		FootCloseOpen,
		// Token: 0x040000A8 RID: 168
		FootInOut,
		// Token: 0x040000A9 RID: 169
		ToesUpDown,
		// Token: 0x040000AA RID: 170
		LastLegDof
	}
}
