﻿using System;

namespace UnityEngine
{
	// Token: 0x02000022 RID: 34
	public enum AvatarIKGoal
	{
		// Token: 0x04000043 RID: 67
		LeftFoot,
		// Token: 0x04000044 RID: 68
		RightFoot,
		// Token: 0x04000045 RID: 69
		LeftHand,
		// Token: 0x04000046 RID: 70
		RightHand
	}
}
