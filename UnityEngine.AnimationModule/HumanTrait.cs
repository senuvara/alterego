﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000051 RID: 81
	[NativeHeader("Runtime/Animation/HumanTrait.h")]
	public class HumanTrait
	{
		// Token: 0x060004FD RID: 1277 RVA: 0x000067B4 File Offset: 0x000049B4
		public HumanTrait()
		{
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x060004FE RID: 1278
		public static extern int MuscleCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060004FF RID: 1279
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetBoneIndexFromMono(int humanId);

		// Token: 0x06000500 RID: 1280
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetBoneIndexToMono(int boneIndex);

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x06000501 RID: 1281
		public static extern string[] MuscleName { [NativeMethod("GetMuscleNames")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000502 RID: 1282
		public static extern int BoneCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x06000503 RID: 1283
		public static extern string[] BoneName { [NativeMethod("MonoBoneNames")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000504 RID: 1284 RVA: 0x00007554 File Offset: 0x00005754
		public static int MuscleFromBone(int i, int dofIndex)
		{
			return HumanTrait.Internal_MuscleFromBone(HumanTrait.GetBoneIndexFromMono(i), dofIndex);
		}

		// Token: 0x06000505 RID: 1285
		[NativeMethod("MuscleFromBone")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_MuscleFromBone(int i, int dofIndex);

		// Token: 0x06000506 RID: 1286 RVA: 0x00007578 File Offset: 0x00005778
		public static int BoneFromMuscle(int i)
		{
			return HumanTrait.GetBoneIndexToMono(HumanTrait.Internal_BoneFromMuscle(i));
		}

		// Token: 0x06000507 RID: 1287
		[NativeMethod("BoneFromMuscle")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_BoneFromMuscle(int i);

		// Token: 0x06000508 RID: 1288 RVA: 0x00007598 File Offset: 0x00005798
		public static bool RequiredBone(int i)
		{
			return HumanTrait.Internal_RequiredBone(HumanTrait.GetBoneIndexFromMono(i));
		}

		// Token: 0x06000509 RID: 1289
		[NativeMethod("RequiredBone")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_RequiredBone(int i);

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x0600050A RID: 1290
		public static extern int RequiredBoneCount { [NativeMethod("RequiredBoneCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600050B RID: 1291
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetMuscleDefaultMin(int i);

		// Token: 0x0600050C RID: 1292
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetMuscleDefaultMax(int i);

		// Token: 0x0600050D RID: 1293 RVA: 0x000075B8 File Offset: 0x000057B8
		public static float GetBoneDefaultHierarchyMass(int i)
		{
			return HumanTrait.Internal_GetBoneHierarchyMass(HumanTrait.GetBoneIndexFromMono(i));
		}

		// Token: 0x0600050E RID: 1294 RVA: 0x000075D8 File Offset: 0x000057D8
		public static int GetParentBone(int i)
		{
			int num = HumanTrait.Internal_GetParent(HumanTrait.GetBoneIndexFromMono(i));
			return (num == -1) ? -1 : HumanTrait.GetBoneIndexToMono(num);
		}

		// Token: 0x0600050F RID: 1295
		[NativeMethod("GetBoneHierarchyMass")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float Internal_GetBoneHierarchyMass(int i);

		// Token: 0x06000510 RID: 1296
		[NativeMethod("GetParent")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetParent(int i);
	}
}
