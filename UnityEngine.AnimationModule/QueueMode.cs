﻿using System;

namespace UnityEngine
{
	// Token: 0x0200005A RID: 90
	public enum QueueMode
	{
		// Token: 0x0400016B RID: 363
		CompleteOthers,
		// Token: 0x0400016C RID: 364
		PlayNow = 2
	}
}
