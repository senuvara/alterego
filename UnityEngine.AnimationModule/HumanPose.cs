﻿using System;

namespace UnityEngine
{
	// Token: 0x0200004F RID: 79
	public struct HumanPose
	{
		// Token: 0x060004F4 RID: 1268 RVA: 0x00007344 File Offset: 0x00005544
		internal void Init()
		{
			if (this.muscles != null)
			{
				if (this.muscles.Length != HumanTrait.MuscleCount)
				{
					throw new InvalidOperationException("Bad array size for HumanPose.muscles. Size must equal HumanTrait.MuscleCount");
				}
			}
			if (this.muscles == null)
			{
				this.muscles = new float[HumanTrait.MuscleCount];
				if (this.bodyRotation.x == 0f && this.bodyRotation.y == 0f && this.bodyRotation.z == 0f && this.bodyRotation.w == 0f)
				{
					this.bodyRotation.w = 1f;
				}
			}
		}

		// Token: 0x04000151 RID: 337
		public Vector3 bodyPosition;

		// Token: 0x04000152 RID: 338
		public Quaternion bodyRotation;

		// Token: 0x04000153 RID: 339
		public float[] muscles;
	}
}
