﻿using System;
using System.Collections.Generic;

namespace UnityEngine
{
	// Token: 0x02000004 RID: 4
	public interface IAnimationClipSource
	{
		// Token: 0x06000008 RID: 8
		void GetAnimationClips(List<AnimationClip> results);
	}
}
