﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine
{
	// Token: 0x02000047 RID: 71
	[MovedFrom("UnityEditor.Animations", true)]
	[NativeHeader("Runtime/Animation/AvatarMask.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/Animation.bindings.h")]
	[UsedByNativeCode]
	public sealed class AvatarMask : Object
	{
		// Token: 0x0600046A RID: 1130 RVA: 0x00006DF0 File Offset: 0x00004FF0
		public AvatarMask()
		{
			AvatarMask.Internal_Create(this);
		}

		// Token: 0x0600046B RID: 1131
		[FreeFunction("AnimationBindings::CreateAvatarMask")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] AvatarMask self);

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x0600046C RID: 1132 RVA: 0x00006E00 File Offset: 0x00005000
		[Obsolete("AvatarMask.humanoidBodyPartCount is deprecated, use AvatarMaskBodyPart.LastBodyPart instead.")]
		public int humanoidBodyPartCount
		{
			get
			{
				return 13;
			}
		}

		// Token: 0x0600046D RID: 1133
		[NativeMethod("GetBodyPart")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetHumanoidBodyPartActive(AvatarMaskBodyPart index);

		// Token: 0x0600046E RID: 1134
		[NativeMethod("SetBodyPart")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetHumanoidBodyPartActive(AvatarMaskBodyPart index, bool value);

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x0600046F RID: 1135
		// (set) Token: 0x06000470 RID: 1136
		public extern int transformCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000471 RID: 1137 RVA: 0x00006E17 File Offset: 0x00005017
		public void AddTransformPath(Transform transform)
		{
			this.AddTransformPath(transform, true);
		}

		// Token: 0x06000472 RID: 1138
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddTransformPath([NotNull] Transform transform, [DefaultValue("true")] bool recursive);

		// Token: 0x06000473 RID: 1139 RVA: 0x00006E22 File Offset: 0x00005022
		public void RemoveTransformPath(Transform transform)
		{
			this.RemoveTransformPath(transform, true);
		}

		// Token: 0x06000474 RID: 1140
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveTransformPath([NotNull] Transform transform, [DefaultValue("true")] bool recursive);

		// Token: 0x06000475 RID: 1141
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetTransformPath(int index);

		// Token: 0x06000476 RID: 1142
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTransformPath(int index, string path);

		// Token: 0x06000477 RID: 1143
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetTransformWeight(int index);

		// Token: 0x06000478 RID: 1144
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetTransformWeight(int index, float weight);

		// Token: 0x06000479 RID: 1145 RVA: 0x00006E30 File Offset: 0x00005030
		public bool GetTransformActive(int index)
		{
			return this.GetTransformWeight(index) > 0.5f;
		}

		// Token: 0x0600047A RID: 1146 RVA: 0x00006E53 File Offset: 0x00005053
		public void SetTransformActive(int index, bool value)
		{
			this.SetTransformWeight(index, (!value) ? 0f : 1f);
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x0600047B RID: 1147
		internal extern bool hasFeetIK { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600047C RID: 1148 RVA: 0x00006E74 File Offset: 0x00005074
		internal void Copy(AvatarMask other)
		{
			for (AvatarMaskBodyPart avatarMaskBodyPart = AvatarMaskBodyPart.Root; avatarMaskBodyPart < AvatarMaskBodyPart.LastBodyPart; avatarMaskBodyPart++)
			{
				this.SetHumanoidBodyPartActive(avatarMaskBodyPart, other.GetHumanoidBodyPartActive(avatarMaskBodyPart));
			}
			this.transformCount = other.transformCount;
			for (int i = 0; i < other.transformCount; i++)
			{
				this.SetTransformPath(i, other.GetTransformPath(i));
				this.SetTransformActive(i, other.GetTransformActive(i));
			}
		}
	}
}
