﻿using System;

namespace UnityEngine
{
	// Token: 0x0200005B RID: 91
	public enum AnimationBlendMode
	{
		// Token: 0x0400016E RID: 366
		Blend,
		// Token: 0x0400016F RID: 367
		Additive
	}
}
