﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000040 RID: 64
	[UsedByNativeCode]
	[NativeHeader("Runtime/Animation/Avatar.h")]
	public class Avatar : Object
	{
		// Token: 0x0600042F RID: 1071 RVA: 0x000069D4 File Offset: 0x00004BD4
		private Avatar()
		{
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000430 RID: 1072
		public extern bool isValid { [NativeMethod("IsValid")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000431 RID: 1073
		public extern bool isHuman { [NativeMethod("IsHuman")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000432 RID: 1074
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetMuscleMinMax(int muscleId, float min, float max);

		// Token: 0x06000433 RID: 1075
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetParameter(int parameterId, float value);

		// Token: 0x06000434 RID: 1076 RVA: 0x000069E0 File Offset: 0x00004BE0
		internal float GetAxisLength(int humanId)
		{
			return this.Internal_GetAxisLength(HumanTrait.GetBoneIndexFromMono(humanId));
		}

		// Token: 0x06000435 RID: 1077 RVA: 0x00006A04 File Offset: 0x00004C04
		internal Quaternion GetPreRotation(int humanId)
		{
			return this.Internal_GetPreRotation(HumanTrait.GetBoneIndexFromMono(humanId));
		}

		// Token: 0x06000436 RID: 1078 RVA: 0x00006A28 File Offset: 0x00004C28
		internal Quaternion GetPostRotation(int humanId)
		{
			return this.Internal_GetPostRotation(HumanTrait.GetBoneIndexFromMono(humanId));
		}

		// Token: 0x06000437 RID: 1079 RVA: 0x00006A4C File Offset: 0x00004C4C
		internal Quaternion GetZYPostQ(int humanId, Quaternion parentQ, Quaternion q)
		{
			return this.Internal_GetZYPostQ(HumanTrait.GetBoneIndexFromMono(humanId), parentQ, q);
		}

		// Token: 0x06000438 RID: 1080 RVA: 0x00006A70 File Offset: 0x00004C70
		internal Quaternion GetZYRoll(int humanId, Vector3 uvw)
		{
			return this.Internal_GetZYRoll(HumanTrait.GetBoneIndexFromMono(humanId), uvw);
		}

		// Token: 0x06000439 RID: 1081 RVA: 0x00006A94 File Offset: 0x00004C94
		internal Vector3 GetLimitSign(int humanId)
		{
			return this.Internal_GetLimitSign(HumanTrait.GetBoneIndexFromMono(humanId));
		}

		// Token: 0x0600043A RID: 1082
		[NativeMethod("GetAxisLength")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern float Internal_GetAxisLength(int humanId);

		// Token: 0x0600043B RID: 1083 RVA: 0x00006AB8 File Offset: 0x00004CB8
		[NativeMethod("GetPreRotation")]
		internal Quaternion Internal_GetPreRotation(int humanId)
		{
			Quaternion result;
			this.Internal_GetPreRotation_Injected(humanId, out result);
			return result;
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x00006AD0 File Offset: 0x00004CD0
		[NativeMethod("GetPostRotation")]
		internal Quaternion Internal_GetPostRotation(int humanId)
		{
			Quaternion result;
			this.Internal_GetPostRotation_Injected(humanId, out result);
			return result;
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x00006AE8 File Offset: 0x00004CE8
		[NativeMethod("GetZYPostQ")]
		internal Quaternion Internal_GetZYPostQ(int humanId, Quaternion parentQ, Quaternion q)
		{
			Quaternion result;
			this.Internal_GetZYPostQ_Injected(humanId, ref parentQ, ref q, out result);
			return result;
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x00006B04 File Offset: 0x00004D04
		[NativeMethod("GetZYRoll")]
		internal Quaternion Internal_GetZYRoll(int humanId, Vector3 uvw)
		{
			Quaternion result;
			this.Internal_GetZYRoll_Injected(humanId, ref uvw, out result);
			return result;
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x00006B20 File Offset: 0x00004D20
		[NativeMethod("GetLimitSign")]
		internal Vector3 Internal_GetLimitSign(int humanId)
		{
			Vector3 result;
			this.Internal_GetLimitSign_Injected(humanId, out result);
			return result;
		}

		// Token: 0x06000440 RID: 1088
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetPreRotation_Injected(int humanId, out Quaternion ret);

		// Token: 0x06000441 RID: 1089
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetPostRotation_Injected(int humanId, out Quaternion ret);

		// Token: 0x06000442 RID: 1090
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetZYPostQ_Injected(int humanId, ref Quaternion parentQ, ref Quaternion q, out Quaternion ret);

		// Token: 0x06000443 RID: 1091
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetZYRoll_Injected(int humanId, ref Vector3 uvw, out Quaternion ret);

		// Token: 0x06000444 RID: 1092
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetLimitSign_Injected(int humanId, out Vector3 ret);
	}
}
