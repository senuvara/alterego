﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200002B RID: 43
	[NativeHeader("Runtime/Animation/AnimatorInfo.h")]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Animation/ScriptBindings/Animation.bindings.h")]
	public struct AnimatorClipInfo
	{
		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600024B RID: 587 RVA: 0x00004C60 File Offset: 0x00002E60
		public AnimationClip clip
		{
			get
			{
				return (this.m_ClipInstanceID == 0) ? null : AnimatorClipInfo.InstanceIDToAnimationClipPPtr(this.m_ClipInstanceID);
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x0600024C RID: 588 RVA: 0x00004C94 File Offset: 0x00002E94
		public float weight
		{
			get
			{
				return this.m_Weight;
			}
		}

		// Token: 0x0600024D RID: 589
		[FreeFunction("AnimationBindings::InstanceIDToAnimationClipPPtr")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimationClip InstanceIDToAnimationClipPPtr(int instanceID);

		// Token: 0x04000069 RID: 105
		private int m_ClipInstanceID;

		// Token: 0x0400006A RID: 106
		private float m_Weight;
	}
}
