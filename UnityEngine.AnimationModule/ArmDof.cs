﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003A RID: 58
	public enum ArmDof
	{
		// Token: 0x040000AC RID: 172
		ShoulderDownUp,
		// Token: 0x040000AD RID: 173
		ShoulderFrontBack,
		// Token: 0x040000AE RID: 174
		ArmDownUp,
		// Token: 0x040000AF RID: 175
		ArmFrontBack,
		// Token: 0x040000B0 RID: 176
		ArmRollInOut,
		// Token: 0x040000B1 RID: 177
		ForeArmCloseOpen,
		// Token: 0x040000B2 RID: 178
		ForeArmRollInOut,
		// Token: 0x040000B3 RID: 179
		HandDownUp,
		// Token: 0x040000B4 RID: 180
		HandInOut,
		// Token: 0x040000B5 RID: 181
		LastArmDof
	}
}
