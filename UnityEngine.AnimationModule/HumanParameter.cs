﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003F RID: 63
	internal enum HumanParameter
	{
		// Token: 0x0400011A RID: 282
		UpperArmTwist,
		// Token: 0x0400011B RID: 283
		LowerArmTwist,
		// Token: 0x0400011C RID: 284
		UpperLegTwist,
		// Token: 0x0400011D RID: 285
		LowerLegTwist,
		// Token: 0x0400011E RID: 286
		ArmStretch,
		// Token: 0x0400011F RID: 287
		LegStretch,
		// Token: 0x04000120 RID: 288
		FeetSpacing
	}
}
