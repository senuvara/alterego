﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000043 RID: 67
	[NativeType(CodegenOptions.Custom, "MonoHumanBone")]
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Animation/HumanDescription.h")]
	public struct HumanBone
	{
		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000451 RID: 1105 RVA: 0x00006C28 File Offset: 0x00004E28
		// (set) Token: 0x06000452 RID: 1106 RVA: 0x00006C43 File Offset: 0x00004E43
		public string boneName
		{
			get
			{
				return this.m_BoneName;
			}
			set
			{
				this.m_BoneName = value;
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000453 RID: 1107 RVA: 0x00006C50 File Offset: 0x00004E50
		// (set) Token: 0x06000454 RID: 1108 RVA: 0x00006C6B File Offset: 0x00004E6B
		public string humanName
		{
			get
			{
				return this.m_HumanName;
			}
			set
			{
				this.m_HumanName = value;
			}
		}

		// Token: 0x0400012B RID: 299
		private string m_BoneName;

		// Token: 0x0400012C RID: 300
		private string m_HumanName;

		// Token: 0x0400012D RID: 301
		[NativeName("m_Limit")]
		public HumanLimit limit;
	}
}
