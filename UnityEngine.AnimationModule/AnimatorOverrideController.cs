﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000034 RID: 52
	[NativeHeader("Runtime/Animation/AnimatorOverrideController.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/Animation.bindings.h")]
	[UsedByNativeCode]
	public class AnimatorOverrideController : RuntimeAnimatorController
	{
		// Token: 0x06000411 RID: 1041 RVA: 0x000067BC File Offset: 0x000049BC
		public AnimatorOverrideController()
		{
			AnimatorOverrideController.Internal_Create(this, null);
			this.OnOverrideControllerDirty = null;
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x000067D3 File Offset: 0x000049D3
		public AnimatorOverrideController(RuntimeAnimatorController controller)
		{
			AnimatorOverrideController.Internal_Create(this, controller);
			this.OnOverrideControllerDirty = null;
		}

		// Token: 0x06000413 RID: 1043
		[FreeFunction("AnimationBindings::CreateAnimatorOverrideController")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] AnimatorOverrideController self, RuntimeAnimatorController controller);

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x06000414 RID: 1044
		// (set) Token: 0x06000415 RID: 1045
		public extern RuntimeAnimatorController runtimeAnimatorController { [NativeMethod("GetAnimatorController")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetAnimatorController")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000095 RID: 149
		public AnimationClip this[string name]
		{
			get
			{
				return this.Internal_GetClipByName(name, true);
			}
			set
			{
				this.Internal_SetClipByName(name, value);
			}
		}

		// Token: 0x06000418 RID: 1048
		[NativeMethod("GetClip")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimationClip Internal_GetClipByName(string name, bool returnEffectiveClip);

		// Token: 0x06000419 RID: 1049
		[NativeMethod("SetClip")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetClipByName(string name, AnimationClip clip);

		// Token: 0x17000096 RID: 150
		public AnimationClip this[AnimationClip clip]
		{
			get
			{
				return this.GetClip(clip, true);
			}
			set
			{
				this.SetClip(clip, value, true);
			}
		}

		// Token: 0x0600041C RID: 1052
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimationClip GetClip(AnimationClip originalClip, bool returnEffectiveClip);

		// Token: 0x0600041D RID: 1053
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetClip(AnimationClip originalClip, AnimationClip overrideClip, bool notify);

		// Token: 0x0600041E RID: 1054
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SendNotification();

		// Token: 0x0600041F RID: 1055
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimationClip GetOriginalClip(int index);

		// Token: 0x06000420 RID: 1056
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimationClip GetOverrideClip(AnimationClip originalClip);

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000421 RID: 1057
		public extern int overridesCount { [NativeMethod("GetOriginalClipsCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000422 RID: 1058 RVA: 0x00006840 File Offset: 0x00004A40
		public void GetOverrides(List<KeyValuePair<AnimationClip, AnimationClip>> overrides)
		{
			if (overrides == null)
			{
				throw new ArgumentNullException("overrides");
			}
			int overridesCount = this.overridesCount;
			if (overrides.Capacity < overridesCount)
			{
				overrides.Capacity = overridesCount;
			}
			overrides.Clear();
			for (int i = 0; i < overridesCount; i++)
			{
				AnimationClip originalClip = this.GetOriginalClip(i);
				overrides.Add(new KeyValuePair<AnimationClip, AnimationClip>(originalClip, this.GetOverrideClip(originalClip)));
			}
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x000068B0 File Offset: 0x00004AB0
		public void ApplyOverrides(IList<KeyValuePair<AnimationClip, AnimationClip>> overrides)
		{
			if (overrides == null)
			{
				throw new ArgumentNullException("overrides");
			}
			for (int i = 0; i < overrides.Count; i++)
			{
				this.SetClip(overrides[i].Key, overrides[i].Value, false);
			}
			this.SendNotification();
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000424 RID: 1060 RVA: 0x00006914 File Offset: 0x00004B14
		// (set) Token: 0x06000425 RID: 1061 RVA: 0x0000697C File Offset: 0x00004B7C
		[Obsolete("AnimatorOverrideController.clips property is deprecated. Use AnimatorOverrideController.GetOverrides and AnimatorOverrideController.ApplyOverrides instead.")]
		public AnimationClipPair[] clips
		{
			get
			{
				int overridesCount = this.overridesCount;
				AnimationClipPair[] array = new AnimationClipPair[overridesCount];
				for (int i = 0; i < overridesCount; i++)
				{
					array[i] = new AnimationClipPair();
					array[i].originalClip = this.GetOriginalClip(i);
					array[i].overrideClip = this.GetOverrideClip(array[i].originalClip);
				}
				return array;
			}
			set
			{
				for (int i = 0; i < value.Length; i++)
				{
					this.SetClip(value[i].originalClip, value[i].overrideClip, false);
				}
				this.SendNotification();
			}
		}

		// Token: 0x06000426 RID: 1062
		[NativeConditional("UNITY_EDITOR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void PerformOverrideClipListCleanup();

		// Token: 0x06000427 RID: 1063 RVA: 0x000069BB File Offset: 0x00004BBB
		[RequiredByNativeCode]
		[NativeConditional("UNITY_EDITOR")]
		internal static void OnInvalidateOverrideController(AnimatorOverrideController controller)
		{
			if (controller.OnOverrideControllerDirty != null)
			{
				controller.OnOverrideControllerDirty();
			}
		}

		// Token: 0x04000087 RID: 135
		internal AnimatorOverrideController.OnOverrideControllerDirtyCallback OnOverrideControllerDirty;

		// Token: 0x02000035 RID: 53
		// (Invoke) Token: 0x06000429 RID: 1065
		internal delegate void OnOverrideControllerDirtyCallback();
	}
}
