﻿using System;

namespace UnityEngine
{
	// Token: 0x0200005C RID: 92
	public enum AnimationPlayMode
	{
		// Token: 0x04000171 RID: 369
		Stop,
		// Token: 0x04000172 RID: 370
		Queue,
		// Token: 0x04000173 RID: 371
		Mix
	}
}
