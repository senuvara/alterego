﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200005E RID: 94
	public sealed class Animation : Behaviour, IEnumerable
	{
		// Token: 0x0600059E RID: 1438 RVA: 0x00004FE9 File Offset: 0x000031E9
		public Animation()
		{
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x0600059F RID: 1439
		// (set) Token: 0x060005A0 RID: 1440
		public extern AnimationClip clip { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x060005A1 RID: 1441
		// (set) Token: 0x060005A2 RID: 1442
		public extern bool playAutomatically { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x060005A3 RID: 1443
		// (set) Token: 0x060005A4 RID: 1444
		public extern WrapMode wrapMode { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060005A5 RID: 1445 RVA: 0x00007D97 File Offset: 0x00005F97
		public void Stop()
		{
			Animation.INTERNAL_CALL_Stop(this);
		}

		// Token: 0x060005A6 RID: 1446
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Stop(Animation self);

		// Token: 0x060005A7 RID: 1447 RVA: 0x00007DA0 File Offset: 0x00005FA0
		public void Stop(string name)
		{
			this.Internal_StopByName(name);
		}

		// Token: 0x060005A8 RID: 1448
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_StopByName(string name);

		// Token: 0x060005A9 RID: 1449 RVA: 0x00007DAA File Offset: 0x00005FAA
		public void Rewind(string name)
		{
			this.Internal_RewindByName(name);
		}

		// Token: 0x060005AA RID: 1450
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_RewindByName(string name);

		// Token: 0x060005AB RID: 1451 RVA: 0x00007DB4 File Offset: 0x00005FB4
		public void Rewind()
		{
			Animation.INTERNAL_CALL_Rewind(this);
		}

		// Token: 0x060005AC RID: 1452
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Rewind(Animation self);

		// Token: 0x060005AD RID: 1453 RVA: 0x00007DBD File Offset: 0x00005FBD
		public void Sample()
		{
			Animation.INTERNAL_CALL_Sample(this);
		}

		// Token: 0x060005AE RID: 1454
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Sample(Animation self);

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x060005AF RID: 1455
		public extern bool isPlaying { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060005B0 RID: 1456
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsPlaying(string name);

		// Token: 0x170000FF RID: 255
		public AnimationState this[string name]
		{
			get
			{
				return this.GetState(name);
			}
		}

		// Token: 0x060005B2 RID: 1458 RVA: 0x00007DE4 File Offset: 0x00005FE4
		[ExcludeFromDocs]
		public bool Play()
		{
			PlayMode mode = PlayMode.StopSameLayer;
			return this.Play(mode);
		}

		// Token: 0x060005B3 RID: 1459 RVA: 0x00007E04 File Offset: 0x00006004
		public bool Play([DefaultValue("PlayMode.StopSameLayer")] PlayMode mode)
		{
			return this.PlayDefaultAnimation(mode);
		}

		// Token: 0x060005B4 RID: 1460
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool Play(string animation, [DefaultValue("PlayMode.StopSameLayer")] PlayMode mode);

		// Token: 0x060005B5 RID: 1461 RVA: 0x00007E20 File Offset: 0x00006020
		[ExcludeFromDocs]
		public bool Play(string animation)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			return this.Play(animation, mode);
		}

		// Token: 0x060005B6 RID: 1462
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CrossFade(string animation, [DefaultValue("0.3F")] float fadeLength, [DefaultValue("PlayMode.StopSameLayer")] PlayMode mode);

		// Token: 0x060005B7 RID: 1463 RVA: 0x00007E40 File Offset: 0x00006040
		[ExcludeFromDocs]
		public void CrossFade(string animation, float fadeLength)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			this.CrossFade(animation, fadeLength, mode);
		}

		// Token: 0x060005B8 RID: 1464 RVA: 0x00007E5C File Offset: 0x0000605C
		[ExcludeFromDocs]
		public void CrossFade(string animation)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			float fadeLength = 0.3f;
			this.CrossFade(animation, fadeLength, mode);
		}

		// Token: 0x060005B9 RID: 1465
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Blend(string animation, [DefaultValue("1.0F")] float targetWeight, [DefaultValue("0.3F")] float fadeLength);

		// Token: 0x060005BA RID: 1466 RVA: 0x00007E7C File Offset: 0x0000607C
		[ExcludeFromDocs]
		public void Blend(string animation, float targetWeight)
		{
			float fadeLength = 0.3f;
			this.Blend(animation, targetWeight, fadeLength);
		}

		// Token: 0x060005BB RID: 1467 RVA: 0x00007E9C File Offset: 0x0000609C
		[ExcludeFromDocs]
		public void Blend(string animation)
		{
			float fadeLength = 0.3f;
			float targetWeight = 1f;
			this.Blend(animation, targetWeight, fadeLength);
		}

		// Token: 0x060005BC RID: 1468
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimationState CrossFadeQueued(string animation, [DefaultValue("0.3F")] float fadeLength, [DefaultValue("QueueMode.CompleteOthers")] QueueMode queue, [DefaultValue("PlayMode.StopSameLayer")] PlayMode mode);

		// Token: 0x060005BD RID: 1469 RVA: 0x00007EC0 File Offset: 0x000060C0
		[ExcludeFromDocs]
		public AnimationState CrossFadeQueued(string animation, float fadeLength, QueueMode queue)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			return this.CrossFadeQueued(animation, fadeLength, queue, mode);
		}

		// Token: 0x060005BE RID: 1470 RVA: 0x00007EE4 File Offset: 0x000060E4
		[ExcludeFromDocs]
		public AnimationState CrossFadeQueued(string animation, float fadeLength)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			QueueMode queue = QueueMode.CompleteOthers;
			return this.CrossFadeQueued(animation, fadeLength, queue, mode);
		}

		// Token: 0x060005BF RID: 1471 RVA: 0x00007F08 File Offset: 0x00006108
		[ExcludeFromDocs]
		public AnimationState CrossFadeQueued(string animation)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			QueueMode queue = QueueMode.CompleteOthers;
			float fadeLength = 0.3f;
			return this.CrossFadeQueued(animation, fadeLength, queue, mode);
		}

		// Token: 0x060005C0 RID: 1472
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimationState PlayQueued(string animation, [DefaultValue("QueueMode.CompleteOthers")] QueueMode queue, [DefaultValue("PlayMode.StopSameLayer")] PlayMode mode);

		// Token: 0x060005C1 RID: 1473 RVA: 0x00007F34 File Offset: 0x00006134
		[ExcludeFromDocs]
		public AnimationState PlayQueued(string animation, QueueMode queue)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			return this.PlayQueued(animation, queue, mode);
		}

		// Token: 0x060005C2 RID: 1474 RVA: 0x00007F54 File Offset: 0x00006154
		[ExcludeFromDocs]
		public AnimationState PlayQueued(string animation)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			QueueMode queue = QueueMode.CompleteOthers;
			return this.PlayQueued(animation, queue, mode);
		}

		// Token: 0x060005C3 RID: 1475 RVA: 0x00007F76 File Offset: 0x00006176
		public void AddClip(AnimationClip clip, string newName)
		{
			this.AddClip(clip, newName, int.MinValue, int.MaxValue);
		}

		// Token: 0x060005C4 RID: 1476
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddClip(AnimationClip clip, string newName, int firstFrame, int lastFrame, [DefaultValue("false")] bool addLoopFrame);

		// Token: 0x060005C5 RID: 1477 RVA: 0x00007F8C File Offset: 0x0000618C
		[ExcludeFromDocs]
		public void AddClip(AnimationClip clip, string newName, int firstFrame, int lastFrame)
		{
			bool addLoopFrame = false;
			this.AddClip(clip, newName, firstFrame, lastFrame, addLoopFrame);
		}

		// Token: 0x060005C6 RID: 1478
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveClip(AnimationClip clip);

		// Token: 0x060005C7 RID: 1479 RVA: 0x00007FA8 File Offset: 0x000061A8
		public void RemoveClip(string clipName)
		{
			this.RemoveClip2(clipName);
		}

		// Token: 0x060005C8 RID: 1480
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetClipCount();

		// Token: 0x060005C9 RID: 1481
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RemoveClip2(string clipName);

		// Token: 0x060005CA RID: 1482
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool PlayDefaultAnimation(PlayMode mode);

		// Token: 0x060005CB RID: 1483 RVA: 0x00007FB4 File Offset: 0x000061B4
		[Obsolete("use PlayMode instead of AnimationPlayMode.")]
		public bool Play(AnimationPlayMode mode)
		{
			return this.PlayDefaultAnimation((PlayMode)mode);
		}

		// Token: 0x060005CC RID: 1484 RVA: 0x00007FD0 File Offset: 0x000061D0
		[Obsolete("use PlayMode instead of AnimationPlayMode.")]
		public bool Play(string animation, AnimationPlayMode mode)
		{
			return this.Play(animation, (PlayMode)mode);
		}

		// Token: 0x060005CD RID: 1485 RVA: 0x00007FED File Offset: 0x000061ED
		public void SyncLayer(int layer)
		{
			Animation.INTERNAL_CALL_SyncLayer(this, layer);
		}

		// Token: 0x060005CE RID: 1486
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SyncLayer(Animation self, int layer);

		// Token: 0x060005CF RID: 1487 RVA: 0x00007FF8 File Offset: 0x000061F8
		public IEnumerator GetEnumerator()
		{
			return new Animation.Enumerator(this);
		}

		// Token: 0x060005D0 RID: 1488
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern AnimationState GetState(string name);

		// Token: 0x060005D1 RID: 1489
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern AnimationState GetStateAtIndex(int index);

		// Token: 0x060005D2 RID: 1490
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetStateCount();

		// Token: 0x060005D3 RID: 1491 RVA: 0x00008014 File Offset: 0x00006214
		public AnimationClip GetClip(string name)
		{
			AnimationState state = this.GetState(name);
			AnimationClip result;
			if (state)
			{
				result = state.clip;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x060005D4 RID: 1492
		// (set) Token: 0x060005D5 RID: 1493
		public extern bool animatePhysics { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x060005D6 RID: 1494
		// (set) Token: 0x060005D7 RID: 1495
		[Obsolete("Use cullingType instead")]
		public extern bool animateOnlyIfVisible { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x060005D8 RID: 1496
		// (set) Token: 0x060005D9 RID: 1497
		public extern AnimationCullingType cullingType { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x060005DA RID: 1498 RVA: 0x0000804C File Offset: 0x0000624C
		// (set) Token: 0x060005DB RID: 1499 RVA: 0x0000806A File Offset: 0x0000626A
		public Bounds localBounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_localBounds(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_localBounds(ref value);
			}
		}

		// Token: 0x060005DC RID: 1500
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localBounds(out Bounds value);

		// Token: 0x060005DD RID: 1501
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_localBounds(ref Bounds value);

		// Token: 0x0200005F RID: 95
		private sealed class Enumerator : IEnumerator
		{
			// Token: 0x060005DE RID: 1502 RVA: 0x00008075 File Offset: 0x00006275
			internal Enumerator(Animation outer)
			{
				this.m_Outer = outer;
			}

			// Token: 0x17000104 RID: 260
			// (get) Token: 0x060005DF RID: 1503 RVA: 0x0000808C File Offset: 0x0000628C
			public object Current
			{
				get
				{
					return this.m_Outer.GetStateAtIndex(this.m_CurrentIndex);
				}
			}

			// Token: 0x060005E0 RID: 1504 RVA: 0x000080B4 File Offset: 0x000062B4
			public bool MoveNext()
			{
				int stateCount = this.m_Outer.GetStateCount();
				this.m_CurrentIndex++;
				return this.m_CurrentIndex < stateCount;
			}

			// Token: 0x060005E1 RID: 1505 RVA: 0x000080EC File Offset: 0x000062EC
			public void Reset()
			{
				this.m_CurrentIndex = -1;
			}

			// Token: 0x04000179 RID: 377
			private Animation m_Outer;

			// Token: 0x0400017A RID: 378
			private int m_CurrentIndex = -1;
		}
	}
}
