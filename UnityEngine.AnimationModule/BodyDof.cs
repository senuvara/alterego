﻿using System;

namespace UnityEngine
{
	// Token: 0x02000037 RID: 55
	public enum BodyDof
	{
		// Token: 0x04000089 RID: 137
		SpineFrontBack,
		// Token: 0x0400008A RID: 138
		SpineLeftRight,
		// Token: 0x0400008B RID: 139
		SpineRollLeftRight,
		// Token: 0x0400008C RID: 140
		ChestFrontBack,
		// Token: 0x0400008D RID: 141
		ChestLeftRight,
		// Token: 0x0400008E RID: 142
		ChestRollLeftRight,
		// Token: 0x0400008F RID: 143
		UpperChestFrontBack,
		// Token: 0x04000090 RID: 144
		UpperChestLeftRight,
		// Token: 0x04000091 RID: 145
		UpperChestRollLeftRight,
		// Token: 0x04000092 RID: 146
		LastBodyDof
	}
}
