﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000030 RID: 48
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimatorControllerParameter.bindings.h")]
	[NativeHeader("Runtime/Animation/AnimatorControllerParameter.h")]
	[NativeAsStruct]
	[UsedByNativeCode]
	[NativeType(CodegenOptions.Custom, "MonoAnimatorControllerParameter")]
	[StructLayout(LayoutKind.Sequential)]
	public class AnimatorControllerParameter
	{
		// Token: 0x0600037C RID: 892 RVA: 0x00005CAA File Offset: 0x00003EAA
		public AnimatorControllerParameter()
		{
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x0600037D RID: 893 RVA: 0x00005CC0 File Offset: 0x00003EC0
		public string name
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x0600037E RID: 894 RVA: 0x00005CDC File Offset: 0x00003EDC
		public int nameHash
		{
			get
			{
				return Animator.StringToHash(this.m_Name);
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x0600037F RID: 895 RVA: 0x00005CFC File Offset: 0x00003EFC
		// (set) Token: 0x06000380 RID: 896 RVA: 0x00005D17 File Offset: 0x00003F17
		public AnimatorControllerParameterType type
		{
			get
			{
				return this.m_Type;
			}
			set
			{
				this.m_Type = value;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000381 RID: 897 RVA: 0x00005D24 File Offset: 0x00003F24
		// (set) Token: 0x06000382 RID: 898 RVA: 0x00005D3F File Offset: 0x00003F3F
		public float defaultFloat
		{
			get
			{
				return this.m_DefaultFloat;
			}
			set
			{
				this.m_DefaultFloat = value;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000383 RID: 899 RVA: 0x00005D4C File Offset: 0x00003F4C
		// (set) Token: 0x06000384 RID: 900 RVA: 0x00005D67 File Offset: 0x00003F67
		public int defaultInt
		{
			get
			{
				return this.m_DefaultInt;
			}
			set
			{
				this.m_DefaultInt = value;
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000385 RID: 901 RVA: 0x00005D74 File Offset: 0x00003F74
		// (set) Token: 0x06000386 RID: 902 RVA: 0x00005D8F File Offset: 0x00003F8F
		public bool defaultBool
		{
			get
			{
				return this.m_DefaultBool;
			}
			set
			{
				this.m_DefaultBool = value;
			}
		}

		// Token: 0x06000387 RID: 903 RVA: 0x00005D9C File Offset: 0x00003F9C
		public override bool Equals(object o)
		{
			AnimatorControllerParameter animatorControllerParameter = o as AnimatorControllerParameter;
			return animatorControllerParameter != null && this.m_Name == animatorControllerParameter.m_Name && this.m_Type == animatorControllerParameter.m_Type && this.m_DefaultFloat == animatorControllerParameter.m_DefaultFloat && this.m_DefaultInt == animatorControllerParameter.m_DefaultInt && this.m_DefaultBool == animatorControllerParameter.m_DefaultBool;
		}

		// Token: 0x06000388 RID: 904 RVA: 0x00005E18 File Offset: 0x00004018
		public override int GetHashCode()
		{
			return this.name.GetHashCode();
		}

		// Token: 0x0400007E RID: 126
		internal string m_Name = "";

		// Token: 0x0400007F RID: 127
		internal AnimatorControllerParameterType m_Type;

		// Token: 0x04000080 RID: 128
		internal float m_DefaultFloat;

		// Token: 0x04000081 RID: 129
		internal int m_DefaultInt;

		// Token: 0x04000082 RID: 130
		internal bool m_DefaultBool;
	}
}
