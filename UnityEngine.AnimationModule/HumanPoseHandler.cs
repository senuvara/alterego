﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000050 RID: 80
	[NativeHeader("Runtime/Animation/HumanPoseHandler.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/Animation.bindings.h")]
	public class HumanPoseHandler : IDisposable
	{
		// Token: 0x060004F5 RID: 1269 RVA: 0x00007400 File Offset: 0x00005600
		public HumanPoseHandler(Avatar avatar, Transform root)
		{
			this.m_Ptr = IntPtr.Zero;
			if (root == null)
			{
				throw new ArgumentNullException("HumanPoseHandler root Transform is null");
			}
			if (avatar == null)
			{
				throw new ArgumentNullException("HumanPoseHandler avatar is null");
			}
			if (!avatar.isValid)
			{
				throw new ArgumentException("HumanPoseHandler avatar is invalid");
			}
			if (!avatar.isHuman)
			{
				throw new ArgumentException("HumanPoseHandler avatar is not human");
			}
			this.m_Ptr = HumanPoseHandler.Internal_Create(avatar, root);
		}

		// Token: 0x060004F6 RID: 1270
		[FreeFunction("AnimationBindings::CreateHumanPoseHandler")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Internal_Create(Avatar avatar, Transform root);

		// Token: 0x060004F7 RID: 1271
		[FreeFunction("AnimationBindings::DestroyHumanPoseHandler")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Destroy(IntPtr ptr);

		// Token: 0x060004F8 RID: 1272
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetHumanPose(out Vector3 bodyPosition, out Quaternion bodyRotation, [Out] float[] muscles);

		// Token: 0x060004F9 RID: 1273
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetHumanPose(ref Vector3 bodyPosition, ref Quaternion bodyRotation, float[] muscles);

		// Token: 0x060004FA RID: 1274 RVA: 0x00007486 File Offset: 0x00005686
		public void Dispose()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				HumanPoseHandler.Internal_Destroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
			GC.SuppressFinalize(this);
		}

		// Token: 0x060004FB RID: 1275 RVA: 0x000074BC File Offset: 0x000056BC
		public void GetHumanPose(ref HumanPose humanPose)
		{
			if (this.m_Ptr == IntPtr.Zero)
			{
				throw new NullReferenceException("HumanPoseHandler is not initialized properly");
			}
			humanPose.Init();
			this.GetHumanPose(out humanPose.bodyPosition, out humanPose.bodyRotation, humanPose.muscles);
		}

		// Token: 0x060004FC RID: 1276 RVA: 0x00007508 File Offset: 0x00005708
		public void SetHumanPose(ref HumanPose humanPose)
		{
			if (this.m_Ptr == IntPtr.Zero)
			{
				throw new NullReferenceException("HumanPoseHandler is not initialized properly");
			}
			humanPose.Init();
			this.SetHumanPose(ref humanPose.bodyPosition, ref humanPose.bodyRotation, humanPose.muscles);
		}

		// Token: 0x04000154 RID: 340
		internal IntPtr m_Ptr;
	}
}
