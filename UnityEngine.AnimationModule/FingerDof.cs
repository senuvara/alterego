﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003B RID: 59
	public enum FingerDof
	{
		// Token: 0x040000B7 RID: 183
		ProximalDownUp,
		// Token: 0x040000B8 RID: 184
		ProximalInOut,
		// Token: 0x040000B9 RID: 185
		IntermediateCloseOpen,
		// Token: 0x040000BA RID: 186
		DistalCloseOpen,
		// Token: 0x040000BB RID: 187
		LastFingerDof
	}
}
