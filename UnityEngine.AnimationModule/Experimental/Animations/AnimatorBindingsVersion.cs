﻿using System;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x0200001A RID: 26
	internal enum AnimatorBindingsVersion
	{
		// Token: 0x0400001D RID: 29
		kInvalidNotNative,
		// Token: 0x0400001E RID: 30
		kInvalidUnresolved,
		// Token: 0x0400001F RID: 31
		kValidMinVersion
	}
}
