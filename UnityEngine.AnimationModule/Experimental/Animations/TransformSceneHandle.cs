﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x0200001F RID: 31
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimationStreamHandles.bindings.h")]
	[NativeHeader("Runtime/Animation/Director/AnimationSceneHandles.h")]
	public struct TransformSceneHandle
	{
		// Token: 0x06000208 RID: 520 RVA: 0x00004764 File Offset: 0x00002964
		public bool IsValid(AnimationStream stream)
		{
			return stream.isValid && this.createdByNative && this.hasTransformSceneHandleDefinitionIndex && this.HasValidTransform(ref stream);
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000209 RID: 521 RVA: 0x000047A8 File Offset: 0x000029A8
		private bool createdByNative
		{
			get
			{
				return this.valid != 0U;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x0600020A RID: 522 RVA: 0x000047CC File Offset: 0x000029CC
		private bool hasTransformSceneHandleDefinitionIndex
		{
			get
			{
				return this.transformSceneHandleDefinitionIndex != -1;
			}
		}

		// Token: 0x0600020B RID: 523 RVA: 0x000047F0 File Offset: 0x000029F0
		private void CheckIsValid(ref AnimationStream stream)
		{
			stream.CheckIsValid();
			if (!this.createdByNative || !this.hasTransformSceneHandleDefinitionIndex)
			{
				throw new InvalidOperationException("The TransformSceneHandle is invalid. Please use proper function to create the handle.");
			}
			if (!this.HasValidTransform(ref stream))
			{
				throw new NullReferenceException("The transform is invalid.");
			}
		}

		// Token: 0x0600020C RID: 524 RVA: 0x0000483C File Offset: 0x00002A3C
		public Vector3 GetPosition(AnimationStream stream)
		{
			this.CheckIsValid(ref stream);
			return this.GetPositionInternal(ref stream);
		}

		// Token: 0x0600020D RID: 525 RVA: 0x00004861 File Offset: 0x00002A61
		public void SetPosition(AnimationStream stream, Vector3 position)
		{
			this.CheckIsValid(ref stream);
			this.SetPositionInternal(ref stream, position);
		}

		// Token: 0x0600020E RID: 526 RVA: 0x00004878 File Offset: 0x00002A78
		public Vector3 GetLocalPosition(AnimationStream stream)
		{
			this.CheckIsValid(ref stream);
			return this.GetLocalPositionInternal(ref stream);
		}

		// Token: 0x0600020F RID: 527 RVA: 0x0000489D File Offset: 0x00002A9D
		public void SetLocalPosition(AnimationStream stream, Vector3 position)
		{
			this.CheckIsValid(ref stream);
			this.SetLocalPositionInternal(ref stream, position);
		}

		// Token: 0x06000210 RID: 528 RVA: 0x000048B4 File Offset: 0x00002AB4
		public Quaternion GetRotation(AnimationStream stream)
		{
			this.CheckIsValid(ref stream);
			return this.GetRotationInternal(ref stream);
		}

		// Token: 0x06000211 RID: 529 RVA: 0x000048D9 File Offset: 0x00002AD9
		public void SetRotation(AnimationStream stream, Quaternion rotation)
		{
			this.CheckIsValid(ref stream);
			this.SetRotationInternal(ref stream, rotation);
		}

		// Token: 0x06000212 RID: 530 RVA: 0x000048F0 File Offset: 0x00002AF0
		public Quaternion GetLocalRotation(AnimationStream stream)
		{
			this.CheckIsValid(ref stream);
			return this.GetLocalRotationInternal(ref stream);
		}

		// Token: 0x06000213 RID: 531 RVA: 0x00004915 File Offset: 0x00002B15
		public void SetLocalRotation(AnimationStream stream, Quaternion rotation)
		{
			this.CheckIsValid(ref stream);
			this.SetLocalRotationInternal(ref stream, rotation);
		}

		// Token: 0x06000214 RID: 532 RVA: 0x0000492C File Offset: 0x00002B2C
		public Vector3 GetLocalScale(AnimationStream stream)
		{
			this.CheckIsValid(ref stream);
			return this.GetLocalScaleInternal(ref stream);
		}

		// Token: 0x06000215 RID: 533 RVA: 0x00004951 File Offset: 0x00002B51
		public void SetLocalScale(AnimationStream stream, Vector3 scale)
		{
			this.CheckIsValid(ref stream);
			this.SetLocalScaleInternal(ref stream, scale);
		}

		// Token: 0x06000216 RID: 534 RVA: 0x00004965 File Offset: 0x00002B65
		[ThreadSafe]
		private bool HasValidTransform(ref AnimationStream stream)
		{
			return TransformSceneHandle.HasValidTransform_Injected(ref this, ref stream);
		}

		// Token: 0x06000217 RID: 535 RVA: 0x00004970 File Offset: 0x00002B70
		[NativeMethod(Name = "TransformSceneHandleBindings::GetPositionInternal", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private Vector3 GetPositionInternal(ref AnimationStream stream)
		{
			Vector3 result;
			TransformSceneHandle.GetPositionInternal_Injected(ref this, ref stream, out result);
			return result;
		}

		// Token: 0x06000218 RID: 536 RVA: 0x00004987 File Offset: 0x00002B87
		[NativeMethod(Name = "TransformSceneHandleBindings::SetPositionInternal", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private void SetPositionInternal(ref AnimationStream stream, Vector3 position)
		{
			TransformSceneHandle.SetPositionInternal_Injected(ref this, ref stream, ref position);
		}

		// Token: 0x06000219 RID: 537 RVA: 0x00004994 File Offset: 0x00002B94
		[NativeMethod(Name = "TransformSceneHandleBindings::GetLocalPositionInternal", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private Vector3 GetLocalPositionInternal(ref AnimationStream stream)
		{
			Vector3 result;
			TransformSceneHandle.GetLocalPositionInternal_Injected(ref this, ref stream, out result);
			return result;
		}

		// Token: 0x0600021A RID: 538 RVA: 0x000049AB File Offset: 0x00002BAB
		[NativeMethod(Name = "TransformSceneHandleBindings::SetLocalPositionInternal", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private void SetLocalPositionInternal(ref AnimationStream stream, Vector3 position)
		{
			TransformSceneHandle.SetLocalPositionInternal_Injected(ref this, ref stream, ref position);
		}

		// Token: 0x0600021B RID: 539 RVA: 0x000049B8 File Offset: 0x00002BB8
		[NativeMethod(Name = "TransformSceneHandleBindings::GetRotationInternal", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private Quaternion GetRotationInternal(ref AnimationStream stream)
		{
			Quaternion result;
			TransformSceneHandle.GetRotationInternal_Injected(ref this, ref stream, out result);
			return result;
		}

		// Token: 0x0600021C RID: 540 RVA: 0x000049CF File Offset: 0x00002BCF
		[NativeMethod(Name = "TransformSceneHandleBindings::SetRotationInternal", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private void SetRotationInternal(ref AnimationStream stream, Quaternion rotation)
		{
			TransformSceneHandle.SetRotationInternal_Injected(ref this, ref stream, ref rotation);
		}

		// Token: 0x0600021D RID: 541 RVA: 0x000049DC File Offset: 0x00002BDC
		[NativeMethod(Name = "TransformSceneHandleBindings::GetLocalRotationInternal", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private Quaternion GetLocalRotationInternal(ref AnimationStream stream)
		{
			Quaternion result;
			TransformSceneHandle.GetLocalRotationInternal_Injected(ref this, ref stream, out result);
			return result;
		}

		// Token: 0x0600021E RID: 542 RVA: 0x000049F3 File Offset: 0x00002BF3
		[NativeMethod(Name = "TransformSceneHandleBindings::SetLocalRotationInternal", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private void SetLocalRotationInternal(ref AnimationStream stream, Quaternion rotation)
		{
			TransformSceneHandle.SetLocalRotationInternal_Injected(ref this, ref stream, ref rotation);
		}

		// Token: 0x0600021F RID: 543 RVA: 0x00004A00 File Offset: 0x00002C00
		[NativeMethod(Name = "TransformSceneHandleBindings::GetLocalScaleInternal", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private Vector3 GetLocalScaleInternal(ref AnimationStream stream)
		{
			Vector3 result;
			TransformSceneHandle.GetLocalScaleInternal_Injected(ref this, ref stream, out result);
			return result;
		}

		// Token: 0x06000220 RID: 544 RVA: 0x00004A17 File Offset: 0x00002C17
		[NativeMethod(Name = "TransformSceneHandleBindings::SetLocalScaleInternal", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private void SetLocalScaleInternal(ref AnimationStream stream, Vector3 scale)
		{
			TransformSceneHandle.SetLocalScaleInternal_Injected(ref this, ref stream, ref scale);
		}

		// Token: 0x06000221 RID: 545
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool HasValidTransform_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream);

		// Token: 0x06000222 RID: 546
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetPositionInternal_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream, out Vector3 ret);

		// Token: 0x06000223 RID: 547
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetPositionInternal_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream, ref Vector3 position);

		// Token: 0x06000224 RID: 548
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLocalPositionInternal_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream, out Vector3 ret);

		// Token: 0x06000225 RID: 549
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLocalPositionInternal_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream, ref Vector3 position);

		// Token: 0x06000226 RID: 550
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRotationInternal_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream, out Quaternion ret);

		// Token: 0x06000227 RID: 551
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetRotationInternal_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream, ref Quaternion rotation);

		// Token: 0x06000228 RID: 552
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLocalRotationInternal_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream, out Quaternion ret);

		// Token: 0x06000229 RID: 553
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLocalRotationInternal_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream, ref Quaternion rotation);

		// Token: 0x0600022A RID: 554
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLocalScaleInternal_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream, out Vector3 ret);

		// Token: 0x0600022B RID: 555
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLocalScaleInternal_Injected(ref TransformSceneHandle _unity_self, ref AnimationStream stream, ref Vector3 scale);

		// Token: 0x04000037 RID: 55
		private uint valid;

		// Token: 0x04000038 RID: 56
		private int transformSceneHandleDefinitionIndex;
	}
}
