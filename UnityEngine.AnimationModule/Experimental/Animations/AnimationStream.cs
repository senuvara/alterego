﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x0200001B RID: 27
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimationStream.bindings.h")]
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Animation/Director/AnimationStream.h")]
	public struct AnimationStream
	{
		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000191 RID: 401 RVA: 0x00003C4C File Offset: 0x00001E4C
		internal uint animatorBindingsVersion
		{
			get
			{
				return this.m_AnimatorBindingsVersion;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000192 RID: 402 RVA: 0x00003C68 File Offset: 0x00001E68
		public bool isValid
		{
			get
			{
				return this.m_AnimatorBindingsVersion >= 2U && this.constant != IntPtr.Zero && this.input != IntPtr.Zero && this.output != IntPtr.Zero && this.workspace != IntPtr.Zero && this.animationHandleBinder != IntPtr.Zero;
			}
		}

		// Token: 0x06000193 RID: 403 RVA: 0x00003CF0 File Offset: 0x00001EF0
		internal void CheckIsValid()
		{
			if (!this.isValid)
			{
				throw new InvalidOperationException("The AnimationStream is invalid.");
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000194 RID: 404 RVA: 0x00003D0C File Offset: 0x00001F0C
		public float deltaTime
		{
			get
			{
				this.CheckIsValid();
				return this.GetDeltaTime();
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000195 RID: 405 RVA: 0x00003D30 File Offset: 0x00001F30
		// (set) Token: 0x06000196 RID: 406 RVA: 0x00003D51 File Offset: 0x00001F51
		public Vector3 velocity
		{
			get
			{
				this.CheckIsValid();
				return this.GetVelocity();
			}
			set
			{
				this.CheckIsValid();
				this.SetVelocity(value);
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000197 RID: 407 RVA: 0x00003D64 File Offset: 0x00001F64
		// (set) Token: 0x06000198 RID: 408 RVA: 0x00003D85 File Offset: 0x00001F85
		public Vector3 angularVelocity
		{
			get
			{
				this.CheckIsValid();
				return this.GetAngularVelocity();
			}
			set
			{
				this.CheckIsValid();
				this.SetAngularVelocity(value);
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000199 RID: 409 RVA: 0x00003D98 File Offset: 0x00001F98
		public Vector3 rootMotionPosition
		{
			get
			{
				this.CheckIsValid();
				return this.GetRootMotionPosition();
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600019A RID: 410 RVA: 0x00003DBC File Offset: 0x00001FBC
		public Quaternion rootMotionRotation
		{
			get
			{
				this.CheckIsValid();
				return this.GetRootMotionRotation();
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600019B RID: 411 RVA: 0x00003DE0 File Offset: 0x00001FE0
		public bool isHumanStream
		{
			get
			{
				this.CheckIsValid();
				return this.GetIsHumanStream();
			}
		}

		// Token: 0x0600019C RID: 412 RVA: 0x00003E04 File Offset: 0x00002004
		public AnimationHumanStream AsHuman()
		{
			this.CheckIsValid();
			if (!this.GetIsHumanStream())
			{
				throw new InvalidOperationException("Cannot create an AnimationHumanStream for a generic rig.");
			}
			return this.GetHumanStream();
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600019D RID: 413 RVA: 0x00003E3C File Offset: 0x0000203C
		public int inputStreamCount
		{
			get
			{
				this.CheckIsValid();
				return this.GetInputStreamCount();
			}
		}

		// Token: 0x0600019E RID: 414 RVA: 0x00003E60 File Offset: 0x00002060
		public AnimationStream GetInputStream(int index)
		{
			this.CheckIsValid();
			return this.InternalGetInputStream(index);
		}

		// Token: 0x0600019F RID: 415 RVA: 0x00003E82 File Offset: 0x00002082
		private void ReadSceneTransforms()
		{
			this.CheckIsValid();
			this.InternalReadSceneTransforms();
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x00003E91 File Offset: 0x00002091
		private void WriteSceneTransforms()
		{
			this.CheckIsValid();
			this.InternalWriteSceneTransforms();
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x00003EA0 File Offset: 0x000020A0
		[NativeMethod(IsThreadSafe = true)]
		private float GetDeltaTime()
		{
			return AnimationStream.GetDeltaTime_Injected(ref this);
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x00003EA8 File Offset: 0x000020A8
		[NativeMethod(IsThreadSafe = true)]
		private bool GetIsHumanStream()
		{
			return AnimationStream.GetIsHumanStream_Injected(ref this);
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x00003EB0 File Offset: 0x000020B0
		[NativeMethod(Name = "AnimationStreamBindings::GetVelocity", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private Vector3 GetVelocity()
		{
			Vector3 result;
			AnimationStream.GetVelocity_Injected(ref this, out result);
			return result;
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x00003EC6 File Offset: 0x000020C6
		[NativeMethod(Name = "AnimationStreamBindings::SetVelocity", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private void SetVelocity(Vector3 velocity)
		{
			AnimationStream.SetVelocity_Injected(ref this, ref velocity);
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x00003ED0 File Offset: 0x000020D0
		[NativeMethod(Name = "AnimationStreamBindings::GetAngularVelocity", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private Vector3 GetAngularVelocity()
		{
			Vector3 result;
			AnimationStream.GetAngularVelocity_Injected(ref this, out result);
			return result;
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x00003EE6 File Offset: 0x000020E6
		[NativeMethod(Name = "AnimationStreamBindings::SetAngularVelocity", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private void SetAngularVelocity(Vector3 velocity)
		{
			AnimationStream.SetAngularVelocity_Injected(ref this, ref velocity);
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x00003EF0 File Offset: 0x000020F0
		[NativeMethod(Name = "AnimationStreamBindings::GetRootMotionPosition", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private Vector3 GetRootMotionPosition()
		{
			Vector3 result;
			AnimationStream.GetRootMotionPosition_Injected(ref this, out result);
			return result;
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x00003F08 File Offset: 0x00002108
		[NativeMethod(Name = "AnimationStreamBindings::GetRootMotionRotation", IsFreeFunction = true, IsThreadSafe = true, HasExplicitThis = true)]
		private Quaternion GetRootMotionRotation()
		{
			Quaternion result;
			AnimationStream.GetRootMotionRotation_Injected(ref this, out result);
			return result;
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x00003F1E File Offset: 0x0000211E
		[NativeMethod(IsThreadSafe = true)]
		private int GetInputStreamCount()
		{
			return AnimationStream.GetInputStreamCount_Injected(ref this);
		}

		// Token: 0x060001AA RID: 426 RVA: 0x00003F28 File Offset: 0x00002128
		[NativeMethod(Name = "GetInputStream", IsThreadSafe = true)]
		private AnimationStream InternalGetInputStream(int index)
		{
			AnimationStream result;
			AnimationStream.InternalGetInputStream_Injected(ref this, index, out result);
			return result;
		}

		// Token: 0x060001AB RID: 427 RVA: 0x00003F40 File Offset: 0x00002140
		[NativeMethod(IsThreadSafe = true)]
		private AnimationHumanStream GetHumanStream()
		{
			AnimationHumanStream result;
			AnimationStream.GetHumanStream_Injected(ref this, out result);
			return result;
		}

		// Token: 0x060001AC RID: 428 RVA: 0x00003F56 File Offset: 0x00002156
		[NativeMethod(Name = "ReadSceneTransforms", IsThreadSafe = true)]
		private void InternalReadSceneTransforms()
		{
			AnimationStream.InternalReadSceneTransforms_Injected(ref this);
		}

		// Token: 0x060001AD RID: 429 RVA: 0x00003F5E File Offset: 0x0000215E
		[NativeMethod(Name = "WriteSceneTransforms", IsThreadSafe = true)]
		private void InternalWriteSceneTransforms()
		{
			AnimationStream.InternalWriteSceneTransforms_Injected(ref this);
		}

		// Token: 0x060001AE RID: 430
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetDeltaTime_Injected(ref AnimationStream _unity_self);

		// Token: 0x060001AF RID: 431
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetIsHumanStream_Injected(ref AnimationStream _unity_self);

		// Token: 0x060001B0 RID: 432
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetVelocity_Injected(ref AnimationStream _unity_self, out Vector3 ret);

		// Token: 0x060001B1 RID: 433
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetVelocity_Injected(ref AnimationStream _unity_self, ref Vector3 velocity);

		// Token: 0x060001B2 RID: 434
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetAngularVelocity_Injected(ref AnimationStream _unity_self, out Vector3 ret);

		// Token: 0x060001B3 RID: 435
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetAngularVelocity_Injected(ref AnimationStream _unity_self, ref Vector3 velocity);

		// Token: 0x060001B4 RID: 436
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRootMotionPosition_Injected(ref AnimationStream _unity_self, out Vector3 ret);

		// Token: 0x060001B5 RID: 437
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRootMotionRotation_Injected(ref AnimationStream _unity_self, out Quaternion ret);

		// Token: 0x060001B6 RID: 438
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetInputStreamCount_Injected(ref AnimationStream _unity_self);

		// Token: 0x060001B7 RID: 439
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalGetInputStream_Injected(ref AnimationStream _unity_self, int index, out AnimationStream ret);

		// Token: 0x060001B8 RID: 440
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetHumanStream_Injected(ref AnimationStream _unity_self, out AnimationHumanStream ret);

		// Token: 0x060001B9 RID: 441
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalReadSceneTransforms_Injected(ref AnimationStream _unity_self);

		// Token: 0x060001BA RID: 442
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalWriteSceneTransforms_Injected(ref AnimationStream _unity_self);

		// Token: 0x04000020 RID: 32
		private uint m_AnimatorBindingsVersion;

		// Token: 0x04000021 RID: 33
		private IntPtr constant;

		// Token: 0x04000022 RID: 34
		private IntPtr input;

		// Token: 0x04000023 RID: 35
		private IntPtr output;

		// Token: 0x04000024 RID: 36
		private IntPtr workspace;

		// Token: 0x04000025 RID: 37
		private IntPtr inputStreamAccessor;

		// Token: 0x04000026 RID: 38
		private IntPtr animationHandleBinder;

		// Token: 0x04000027 RID: 39
		internal const int InvalidIndex = -1;
	}
}
