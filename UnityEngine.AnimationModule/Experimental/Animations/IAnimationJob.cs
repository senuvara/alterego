﻿using System;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x02000005 RID: 5
	public interface IAnimationJob
	{
		// Token: 0x06000009 RID: 9
		void ProcessAnimation(AnimationStream stream);

		// Token: 0x0600000A RID: 10
		void ProcessRootMotion(AnimationStream stream);
	}
}
