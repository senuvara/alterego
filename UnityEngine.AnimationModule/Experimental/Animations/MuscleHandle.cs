﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x02000054 RID: 84
	[NativeHeader("Runtime/Animation/MuscleHandle.h")]
	[NativeHeader("Runtime/Animation/Animator.h")]
	public struct MuscleHandle
	{
		// Token: 0x06000542 RID: 1346 RVA: 0x000077B2 File Offset: 0x000059B2
		public MuscleHandle(BodyDof bodyDof)
		{
			this.humanPartDof = HumanPartDof.Body;
			this.dof = (int)bodyDof;
		}

		// Token: 0x06000543 RID: 1347 RVA: 0x000077C3 File Offset: 0x000059C3
		public MuscleHandle(HeadDof headDof)
		{
			this.humanPartDof = HumanPartDof.Head;
			this.dof = (int)headDof;
		}

		// Token: 0x06000544 RID: 1348 RVA: 0x000077D4 File Offset: 0x000059D4
		public MuscleHandle(HumanPartDof partDof, LegDof legDof)
		{
			if (partDof != HumanPartDof.LeftLeg && partDof != HumanPartDof.RightLeg)
			{
				throw new InvalidOperationException("Invalid HumanPartDof for a leg, please use either HumanPartDof.LeftLeg or HumanPartDof.RightLeg.");
			}
			this.humanPartDof = partDof;
			this.dof = (int)legDof;
		}

		// Token: 0x06000545 RID: 1349 RVA: 0x000077FE File Offset: 0x000059FE
		public MuscleHandle(HumanPartDof partDof, ArmDof armDof)
		{
			if (partDof != HumanPartDof.LeftArm && partDof != HumanPartDof.RightArm)
			{
				throw new InvalidOperationException("Invalid HumanPartDof for an arm, please use either HumanPartDof.LeftArm or HumanPartDof.RightArm.");
			}
			this.humanPartDof = partDof;
			this.dof = (int)armDof;
		}

		// Token: 0x06000546 RID: 1350 RVA: 0x00007828 File Offset: 0x00005A28
		public MuscleHandle(HumanPartDof partDof, FingerDof fingerDof)
		{
			if (partDof < HumanPartDof.LeftThumb || partDof > HumanPartDof.RightLittle)
			{
				throw new InvalidOperationException("Invalid HumanPartDof for a finger.");
			}
			this.humanPartDof = partDof;
			this.dof = (int)fingerDof;
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x06000547 RID: 1351 RVA: 0x00007854 File Offset: 0x00005A54
		// (set) Token: 0x06000548 RID: 1352 RVA: 0x0000786E File Offset: 0x00005A6E
		public HumanPartDof humanPartDof
		{
			[CompilerGenerated]
			get
			{
				return this.<humanPartDof>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<humanPartDof>k__BackingField = value;
			}
		}

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000549 RID: 1353 RVA: 0x00007878 File Offset: 0x00005A78
		// (set) Token: 0x0600054A RID: 1354 RVA: 0x00007892 File Offset: 0x00005A92
		public int dof
		{
			[CompilerGenerated]
			get
			{
				return this.<dof>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<dof>k__BackingField = value;
			}
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x0600054B RID: 1355 RVA: 0x0000789C File Offset: 0x00005A9C
		public string name
		{
			get
			{
				return this.GetName();
			}
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x0600054C RID: 1356 RVA: 0x000078B8 File Offset: 0x00005AB8
		public static int muscleHandleCount
		{
			get
			{
				return MuscleHandle.GetMuscleHandleCount();
			}
		}

		// Token: 0x0600054D RID: 1357
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void GetMuscleHandles([NotNull] [Out] MuscleHandle[] muscleHandles);

		// Token: 0x0600054E RID: 1358 RVA: 0x000078D2 File Offset: 0x00005AD2
		private string GetName()
		{
			return MuscleHandle.GetName_Injected(ref this);
		}

		// Token: 0x0600054F RID: 1359
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetMuscleHandleCount();

		// Token: 0x06000550 RID: 1360
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetName_Injected(ref MuscleHandle _unity_self);

		// Token: 0x04000156 RID: 342
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private HumanPartDof <humanPartDof>k__BackingField;

		// Token: 0x04000157 RID: 343
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <dof>k__BackingField;
	}
}
