﻿using System;
using UnityEngine.Playables;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x02000006 RID: 6
	public interface IAnimationJobPlayable : IPlayable
	{
		// Token: 0x0600000B RID: 11
		T GetJobData<T>() where T : struct, IAnimationJob;

		// Token: 0x0600000C RID: 12
		void SetJobData<T>(T jobData) where T : struct, IAnimationJob;
	}
}
