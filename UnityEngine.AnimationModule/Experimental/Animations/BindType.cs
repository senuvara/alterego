﻿using System;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x0200001C RID: 28
	internal enum BindType
	{
		// Token: 0x04000029 RID: 41
		Unbound,
		// Token: 0x0400002A RID: 42
		Float = 5,
		// Token: 0x0400002B RID: 43
		Bool,
		// Token: 0x0400002C RID: 44
		GameObjectActive,
		// Token: 0x0400002D RID: 45
		ObjectReference = 9,
		// Token: 0x0400002E RID: 46
		Int,
		// Token: 0x0400002F RID: 47
		DiscreetInt
	}
}
