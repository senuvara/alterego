﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs.LowLevel.Unsafe;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x02000007 RID: 7
	internal struct ProcessAnimationJobStruct<T> where T : struct, IAnimationJob
	{
		// Token: 0x0600000D RID: 13 RVA: 0x00002260 File Offset: 0x00000460
		public static IntPtr GetJobReflectionData()
		{
			if (ProcessAnimationJobStruct<T>.jobReflectionData == IntPtr.Zero)
			{
				Type typeFromHandle = typeof(T);
				JobType jobType = JobType.Single;
				if (ProcessAnimationJobStruct<T>.<>f__mg$cache0 == null)
				{
					ProcessAnimationJobStruct<T>.<>f__mg$cache0 = new ProcessAnimationJobStruct<T>.ExecuteJobFunction(ProcessAnimationJobStruct<T>.ExecuteProcessRootMotion);
				}
				object managedJobFunction = ProcessAnimationJobStruct<T>.<>f__mg$cache0;
				if (ProcessAnimationJobStruct<T>.<>f__mg$cache1 == null)
				{
					ProcessAnimationJobStruct<T>.<>f__mg$cache1 = new ProcessAnimationJobStruct<T>.ExecuteJobFunction(ProcessAnimationJobStruct<T>.ExecuteProcessAnimation);
				}
				ProcessAnimationJobStruct<T>.jobReflectionData = JobsUtility.CreateJobReflectionData(typeFromHandle, jobType, managedJobFunction, ProcessAnimationJobStruct<T>.<>f__mg$cache1, null);
			}
			return ProcessAnimationJobStruct<T>.jobReflectionData;
		}

		// Token: 0x0600000E RID: 14 RVA: 0x000022E0 File Offset: 0x000004E0
		public unsafe static void ExecuteProcessAnimation(ref T data, IntPtr animationStreamPtr, IntPtr unusedPtr, ref JobRanges ranges, int jobIndex)
		{
			AnimationStream stream;
			UnsafeUtility.CopyPtrToStructure<AnimationStream>((void*)animationStreamPtr, out stream);
			data.ProcessAnimation(stream);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002308 File Offset: 0x00000508
		public unsafe static void ExecuteProcessRootMotion(ref T data, IntPtr animationStreamPtr, IntPtr unusedPtr, ref JobRanges ranges, int jobIndex)
		{
			AnimationStream stream;
			UnsafeUtility.CopyPtrToStructure<AnimationStream>((void*)animationStreamPtr, out stream);
			data.ProcessRootMotion(stream);
		}

		// Token: 0x04000002 RID: 2
		private static IntPtr jobReflectionData;

		// Token: 0x04000003 RID: 3
		[CompilerGenerated]
		private static ProcessAnimationJobStruct<T>.ExecuteJobFunction <>f__mg$cache0;

		// Token: 0x04000004 RID: 4
		[CompilerGenerated]
		private static ProcessAnimationJobStruct<T>.ExecuteJobFunction <>f__mg$cache1;

		// Token: 0x02000008 RID: 8
		// (Invoke) Token: 0x06000011 RID: 17
		public delegate void ExecuteJobFunction(ref T data, IntPtr animationStreamPtr, IntPtr unusedPtr, ref JobRanges ranges, int jobIndex);
	}
}
