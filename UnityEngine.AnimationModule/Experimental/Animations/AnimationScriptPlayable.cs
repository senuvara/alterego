﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Bindings;
using UnityEngine.Playables;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x02000019 RID: 25
	[StaticAccessor("AnimationScriptPlayableBindings", StaticAccessorType.DoubleColon)]
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Director/Core/HPlayable.h")]
	[NativeHeader("Runtime/Director/Core/HPlayableGraph.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimationScriptPlayable.bindings.h")]
	public struct AnimationScriptPlayable : IAnimationJobPlayable, IEquatable<AnimationScriptPlayable>, IPlayable
	{
		// Token: 0x0600017D RID: 381 RVA: 0x000039E4 File Offset: 0x00001BE4
		internal AnimationScriptPlayable(PlayableHandle handle)
		{
			if (handle.IsValid())
			{
				if (!handle.IsPlayableOfType<AnimationScriptPlayable>())
				{
					throw new InvalidCastException("Can't set handle: the playable is not an AnimationScriptPlayable.");
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600017E RID: 382 RVA: 0x00003A14 File Offset: 0x00001C14
		public static AnimationScriptPlayable Null
		{
			get
			{
				return AnimationScriptPlayable.m_NullPlayable;
			}
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00003A30 File Offset: 0x00001C30
		public static AnimationScriptPlayable Create<T>(PlayableGraph graph, T jobData, int inputCount = 0) where T : struct, IAnimationJob
		{
			PlayableHandle handle = AnimationScriptPlayable.CreateHandle<T>(graph, inputCount);
			AnimationScriptPlayable result = new AnimationScriptPlayable(handle);
			result.SetJobData<T>(jobData);
			return result;
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00003A60 File Offset: 0x00001C60
		private static PlayableHandle CreateHandle<T>(PlayableGraph graph, int inputCount) where T : struct, IAnimationJob
		{
			IntPtr jobReflectionData = ProcessAnimationJobStruct<T>.GetJobReflectionData();
			PlayableHandle @null = PlayableHandle.Null;
			PlayableHandle result;
			if (!AnimationScriptPlayable.CreateHandleInternal(graph, ref @null, jobReflectionData))
			{
				result = PlayableHandle.Null;
			}
			else
			{
				@null.SetInputCount(inputCount);
				result = @null;
			}
			return result;
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00003AA4 File Offset: 0x00001CA4
		public PlayableHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00003AC0 File Offset: 0x00001CC0
		private void CheckJobTypeValidity<T>()
		{
			Type jobType = this.GetHandle().GetJobType();
			if (jobType != typeof(T))
			{
				throw new ArgumentException(string.Format("Wrong type: the given job type ({0}) is different from the creation job type ({1}).", typeof(T).FullName, jobType.FullName));
			}
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00003B14 File Offset: 0x00001D14
		public unsafe T GetJobData<T>() where T : struct, IAnimationJob
		{
			this.CheckJobTypeValidity<T>();
			T result;
			UnsafeUtility.CopyPtrToStructure<T>((void*)this.GetHandle().GetAdditionalPayload(), out result);
			return result;
		}

		// Token: 0x06000184 RID: 388 RVA: 0x00003B4C File Offset: 0x00001D4C
		public unsafe void SetJobData<T>(T jobData) where T : struct, IAnimationJob
		{
			this.CheckJobTypeValidity<T>();
			UnsafeUtility.CopyStructureToPtr<T>(ref jobData, (void*)this.GetHandle().GetAdditionalPayload());
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00003B7C File Offset: 0x00001D7C
		public static implicit operator Playable(AnimationScriptPlayable playable)
		{
			return new Playable(playable.GetHandle());
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00003BA0 File Offset: 0x00001DA0
		public static explicit operator AnimationScriptPlayable(Playable playable)
		{
			return new AnimationScriptPlayable(playable.GetHandle());
		}

		// Token: 0x06000187 RID: 391 RVA: 0x00003BC4 File Offset: 0x00001DC4
		public bool Equals(AnimationScriptPlayable other)
		{
			return this.GetHandle() == other.GetHandle();
		}

		// Token: 0x06000188 RID: 392 RVA: 0x00003BEB File Offset: 0x00001DEB
		public void SetProcessInputs(bool value)
		{
			AnimationScriptPlayable.SetProcessInputsInternal(this.GetHandle(), value);
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00003BFC File Offset: 0x00001DFC
		public bool GetProcessInputs()
		{
			return AnimationScriptPlayable.GetProcessInputsInternal(this.GetHandle());
		}

		// Token: 0x0600018A RID: 394 RVA: 0x00003C1C File Offset: 0x00001E1C
		[NativeThrows]
		private static bool CreateHandleInternal(PlayableGraph graph, ref PlayableHandle handle, IntPtr jobReflectionData)
		{
			return AnimationScriptPlayable.CreateHandleInternal_Injected(ref graph, ref handle, jobReflectionData);
		}

		// Token: 0x0600018B RID: 395 RVA: 0x00003C27 File Offset: 0x00001E27
		[NativeThrows]
		private static void SetProcessInputsInternal(PlayableHandle handle, bool value)
		{
			AnimationScriptPlayable.SetProcessInputsInternal_Injected(ref handle, value);
		}

		// Token: 0x0600018C RID: 396 RVA: 0x00003C31 File Offset: 0x00001E31
		[NativeThrows]
		private static bool GetProcessInputsInternal(PlayableHandle handle)
		{
			return AnimationScriptPlayable.GetProcessInputsInternal_Injected(ref handle);
		}

		// Token: 0x0600018D RID: 397 RVA: 0x00003C3A File Offset: 0x00001E3A
		// Note: this type is marked as 'beforefieldinit'.
		static AnimationScriptPlayable()
		{
		}

		// Token: 0x0600018E RID: 398
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CreateHandleInternal_Injected(ref PlayableGraph graph, ref PlayableHandle handle, IntPtr jobReflectionData);

		// Token: 0x0600018F RID: 399
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetProcessInputsInternal_Injected(ref PlayableHandle handle, bool value);

		// Token: 0x06000190 RID: 400
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetProcessInputsInternal_Injected(ref PlayableHandle handle);

		// Token: 0x0400001A RID: 26
		private PlayableHandle m_Handle;

		// Token: 0x0400001B RID: 27
		private static readonly AnimationScriptPlayable m_NullPlayable = new AnimationScriptPlayable(PlayableHandle.Null);
	}
}
