﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x0200001E RID: 30
	[NativeHeader("Runtime/Animation/Director/AnimationStreamHandles.h")]
	public struct PropertyStreamHandle
	{
		// Token: 0x060001E7 RID: 487 RVA: 0x00004350 File Offset: 0x00002550
		public bool IsValid(AnimationStream stream)
		{
			return this.IsValidInternal(ref stream);
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x00004370 File Offset: 0x00002570
		private bool IsValidInternal(ref AnimationStream stream)
		{
			return stream.isValid && this.createdByNative && this.hasHandleIndex && this.hasBindType;
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060001E9 RID: 489 RVA: 0x000043B0 File Offset: 0x000025B0
		private bool createdByNative
		{
			get
			{
				return this.animatorBindingsVersion != 0U;
			}
		}

		// Token: 0x060001EA RID: 490 RVA: 0x000043D4 File Offset: 0x000025D4
		private bool IsSameVersionAsStream(ref AnimationStream stream)
		{
			return this.animatorBindingsVersion == stream.animatorBindingsVersion;
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060001EB RID: 491 RVA: 0x000043F8 File Offset: 0x000025F8
		private bool hasHandleIndex
		{
			get
			{
				return this.handleIndex != -1;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060001EC RID: 492 RVA: 0x0000441C File Offset: 0x0000261C
		private bool hasValueArrayIndex
		{
			get
			{
				return this.valueArrayIndex != -1;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060001ED RID: 493 RVA: 0x00004440 File Offset: 0x00002640
		private bool hasBindType
		{
			get
			{
				return this.bindType != 0;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060001EF RID: 495 RVA: 0x0000446C File Offset: 0x0000266C
		// (set) Token: 0x060001EE RID: 494 RVA: 0x00004461 File Offset: 0x00002661
		internal uint animatorBindingsVersion
		{
			get
			{
				return this.m_AnimatorBindingsVersion;
			}
			private set
			{
				this.m_AnimatorBindingsVersion = value;
			}
		}

		// Token: 0x060001F0 RID: 496 RVA: 0x00004487 File Offset: 0x00002687
		public void Resolve(AnimationStream stream)
		{
			this.CheckIsValidAndResolve(ref stream);
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x00004494 File Offset: 0x00002694
		public bool IsResolved(AnimationStream stream)
		{
			return this.IsResolvedInternal(ref stream);
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x000044B4 File Offset: 0x000026B4
		private bool IsResolvedInternal(ref AnimationStream stream)
		{
			return this.IsValidInternal(ref stream) && this.IsSameVersionAsStream(ref stream) && this.hasValueArrayIndex;
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x000044EC File Offset: 0x000026EC
		private void CheckIsValidAndResolve(ref AnimationStream stream)
		{
			stream.CheckIsValid();
			if (!this.IsResolvedInternal(ref stream))
			{
				if (!this.createdByNative || !this.hasHandleIndex || !this.hasBindType)
				{
					throw new InvalidOperationException("The PropertyStreamHandle is invalid. Please use proper function to create the handle.");
				}
				if (!this.IsSameVersionAsStream(ref stream) || (this.hasHandleIndex && !this.hasValueArrayIndex))
				{
					this.ResolveInternal(ref stream);
				}
				if (this.hasHandleIndex && !this.hasValueArrayIndex)
				{
					throw new InvalidOperationException("The PropertyStreamHandle cannot be resolved.");
				}
			}
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x0000458C File Offset: 0x0000278C
		public float GetFloat(AnimationStream stream)
		{
			this.CheckIsValidAndResolve(ref stream);
			if (this.bindType != 5)
			{
				throw new InvalidOperationException("GetValue type doesn't match PropertyStreamHandle bound type.");
			}
			return this.GetFloatInternal(ref stream);
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x000045C8 File Offset: 0x000027C8
		public void SetFloat(AnimationStream stream, float value)
		{
			this.CheckIsValidAndResolve(ref stream);
			if (this.bindType != 5)
			{
				throw new InvalidOperationException("SetValue type doesn't match PropertyStreamHandle bound type.");
			}
			this.SetFloatInternal(ref stream, value);
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x000045F4 File Offset: 0x000027F4
		public int GetInt(AnimationStream stream)
		{
			this.CheckIsValidAndResolve(ref stream);
			if (this.bindType != 10 && this.bindType != 11 && this.bindType != 9)
			{
				throw new InvalidOperationException("GetValue type doesn't match PropertyStreamHandle bound type.");
			}
			return this.GetIntInternal(ref stream);
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x0000464C File Offset: 0x0000284C
		public void SetInt(AnimationStream stream, int value)
		{
			this.CheckIsValidAndResolve(ref stream);
			if (this.bindType != 10 && this.bindType != 11 && this.bindType != 9)
			{
				throw new InvalidOperationException("SetValue type doesn't match PropertyStreamHandle bound type.");
			}
			this.SetIntInternal(ref stream, value);
		}

		// Token: 0x060001F8 RID: 504 RVA: 0x000046A0 File Offset: 0x000028A0
		public bool GetBool(AnimationStream stream)
		{
			this.CheckIsValidAndResolve(ref stream);
			if (this.bindType != 6 && this.bindType != 7)
			{
				throw new InvalidOperationException("GetValue type doesn't match PropertyStreamHandle bound type.");
			}
			return this.GetBoolInternal(ref stream);
		}

		// Token: 0x060001F9 RID: 505 RVA: 0x000046E8 File Offset: 0x000028E8
		public void SetBool(AnimationStream stream, bool value)
		{
			this.CheckIsValidAndResolve(ref stream);
			if (this.bindType != 6 && this.bindType != 7)
			{
				throw new InvalidOperationException("SetValue type doesn't match PropertyStreamHandle bound type.");
			}
			this.SetBoolInternal(ref stream, value);
		}

		// Token: 0x060001FA RID: 506 RVA: 0x0000471F File Offset: 0x0000291F
		[NativeMethod(Name = "Resolve", IsThreadSafe = true)]
		private void ResolveInternal(ref AnimationStream stream)
		{
			PropertyStreamHandle.ResolveInternal_Injected(ref this, ref stream);
		}

		// Token: 0x060001FB RID: 507 RVA: 0x00004728 File Offset: 0x00002928
		[NativeMethod(Name = "GetFloat", IsThreadSafe = true)]
		private float GetFloatInternal(ref AnimationStream stream)
		{
			return PropertyStreamHandle.GetFloatInternal_Injected(ref this, ref stream);
		}

		// Token: 0x060001FC RID: 508 RVA: 0x00004731 File Offset: 0x00002931
		[NativeMethod(Name = "SetFloat", IsThreadSafe = true)]
		private void SetFloatInternal(ref AnimationStream stream, float value)
		{
			PropertyStreamHandle.SetFloatInternal_Injected(ref this, ref stream, value);
		}

		// Token: 0x060001FD RID: 509 RVA: 0x0000473B File Offset: 0x0000293B
		[NativeMethod(Name = "GetInt", IsThreadSafe = true)]
		private int GetIntInternal(ref AnimationStream stream)
		{
			return PropertyStreamHandle.GetIntInternal_Injected(ref this, ref stream);
		}

		// Token: 0x060001FE RID: 510 RVA: 0x00004744 File Offset: 0x00002944
		[NativeMethod(Name = "SetInt", IsThreadSafe = true)]
		private void SetIntInternal(ref AnimationStream stream, int value)
		{
			PropertyStreamHandle.SetIntInternal_Injected(ref this, ref stream, value);
		}

		// Token: 0x060001FF RID: 511 RVA: 0x0000474E File Offset: 0x0000294E
		[NativeMethod(Name = "GetBool", IsThreadSafe = true)]
		private bool GetBoolInternal(ref AnimationStream stream)
		{
			return PropertyStreamHandle.GetBoolInternal_Injected(ref this, ref stream);
		}

		// Token: 0x06000200 RID: 512 RVA: 0x00004757 File Offset: 0x00002957
		[NativeMethod(Name = "SetBool", IsThreadSafe = true)]
		private void SetBoolInternal(ref AnimationStream stream, bool value)
		{
			PropertyStreamHandle.SetBoolInternal_Injected(ref this, ref stream, value);
		}

		// Token: 0x06000201 RID: 513
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ResolveInternal_Injected(ref PropertyStreamHandle _unity_self, ref AnimationStream stream);

		// Token: 0x06000202 RID: 514
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetFloatInternal_Injected(ref PropertyStreamHandle _unity_self, ref AnimationStream stream);

		// Token: 0x06000203 RID: 515
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetFloatInternal_Injected(ref PropertyStreamHandle _unity_self, ref AnimationStream stream, float value);

		// Token: 0x06000204 RID: 516
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetIntInternal_Injected(ref PropertyStreamHandle _unity_self, ref AnimationStream stream);

		// Token: 0x06000205 RID: 517
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetIntInternal_Injected(ref PropertyStreamHandle _unity_self, ref AnimationStream stream, int value);

		// Token: 0x06000206 RID: 518
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetBoolInternal_Injected(ref PropertyStreamHandle _unity_self, ref AnimationStream stream);

		// Token: 0x06000207 RID: 519
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetBoolInternal_Injected(ref PropertyStreamHandle _unity_self, ref AnimationStream stream, bool value);

		// Token: 0x04000033 RID: 51
		private uint m_AnimatorBindingsVersion;

		// Token: 0x04000034 RID: 52
		private int handleIndex;

		// Token: 0x04000035 RID: 53
		private int valueArrayIndex;

		// Token: 0x04000036 RID: 54
		private int bindType;
	}
}
