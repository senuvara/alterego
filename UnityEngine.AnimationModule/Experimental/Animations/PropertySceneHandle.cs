﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.Animations
{
	// Token: 0x02000020 RID: 32
	[NativeHeader("Runtime/Animation/Director/AnimationSceneHandles.h")]
	public struct PropertySceneHandle
	{
		// Token: 0x0600022C RID: 556 RVA: 0x00004A24 File Offset: 0x00002C24
		public bool IsValid(AnimationStream stream)
		{
			return this.IsValidInternal(ref stream);
		}

		// Token: 0x0600022D RID: 557 RVA: 0x00004A44 File Offset: 0x00002C44
		private bool IsValidInternal(ref AnimationStream stream)
		{
			return stream.isValid && this.createdByNative && this.hasHandleIndex && this.HasValidTransform(ref stream);
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x0600022E RID: 558 RVA: 0x00004A84 File Offset: 0x00002C84
		private bool createdByNative
		{
			get
			{
				return this.valid != 0U;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x0600022F RID: 559 RVA: 0x00004AA8 File Offset: 0x00002CA8
		private bool hasHandleIndex
		{
			get
			{
				return this.handleIndex != -1;
			}
		}

		// Token: 0x06000230 RID: 560 RVA: 0x00004AC9 File Offset: 0x00002CC9
		public void Resolve(AnimationStream stream)
		{
			this.CheckIsValid(ref stream);
			this.ResolveInternal(ref stream);
		}

		// Token: 0x06000231 RID: 561 RVA: 0x00004ADC File Offset: 0x00002CDC
		public bool IsResolved(AnimationStream stream)
		{
			return this.IsValidInternal(ref stream) && this.IsBound(ref stream);
		}

		// Token: 0x06000232 RID: 562 RVA: 0x00004B0C File Offset: 0x00002D0C
		private void CheckIsValid(ref AnimationStream stream)
		{
			stream.CheckIsValid();
			if (!this.createdByNative || !this.hasHandleIndex)
			{
				throw new InvalidOperationException("The PropertySceneHandle is invalid. Please use proper function to create the handle.");
			}
			if (!this.HasValidTransform(ref stream))
			{
				throw new NullReferenceException("The transform is invalid.");
			}
		}

		// Token: 0x06000233 RID: 563 RVA: 0x00004B58 File Offset: 0x00002D58
		public float GetFloat(AnimationStream stream)
		{
			this.CheckIsValid(ref stream);
			return this.GetFloatInternal(ref stream);
		}

		// Token: 0x06000234 RID: 564 RVA: 0x00004B7D File Offset: 0x00002D7D
		public void SetFloat(AnimationStream stream, float value)
		{
			this.CheckIsValid(ref stream);
			this.SetFloatInternal(ref stream, value);
		}

		// Token: 0x06000235 RID: 565 RVA: 0x00004B94 File Offset: 0x00002D94
		public int GetInt(AnimationStream stream)
		{
			this.CheckIsValid(ref stream);
			return this.GetIntInternal(ref stream);
		}

		// Token: 0x06000236 RID: 566 RVA: 0x00004BB9 File Offset: 0x00002DB9
		public void SetInt(AnimationStream stream, int value)
		{
			this.CheckIsValid(ref stream);
			this.SetIntInternal(ref stream, value);
		}

		// Token: 0x06000237 RID: 567 RVA: 0x00004BD0 File Offset: 0x00002DD0
		public bool GetBool(AnimationStream stream)
		{
			this.CheckIsValid(ref stream);
			return this.GetBoolInternal(ref stream);
		}

		// Token: 0x06000238 RID: 568 RVA: 0x00004BF5 File Offset: 0x00002DF5
		public void SetBool(AnimationStream stream, bool value)
		{
			this.CheckIsValid(ref stream);
			this.SetBoolInternal(ref stream, value);
		}

		// Token: 0x06000239 RID: 569 RVA: 0x00004C09 File Offset: 0x00002E09
		[ThreadSafe]
		private bool HasValidTransform(ref AnimationStream stream)
		{
			return PropertySceneHandle.HasValidTransform_Injected(ref this, ref stream);
		}

		// Token: 0x0600023A RID: 570 RVA: 0x00004C12 File Offset: 0x00002E12
		[ThreadSafe]
		private bool IsBound(ref AnimationStream stream)
		{
			return PropertySceneHandle.IsBound_Injected(ref this, ref stream);
		}

		// Token: 0x0600023B RID: 571 RVA: 0x00004C1B File Offset: 0x00002E1B
		[NativeMethod(Name = "Resolve", IsThreadSafe = true)]
		private void ResolveInternal(ref AnimationStream stream)
		{
			PropertySceneHandle.ResolveInternal_Injected(ref this, ref stream);
		}

		// Token: 0x0600023C RID: 572 RVA: 0x00004C24 File Offset: 0x00002E24
		[NativeMethod(Name = "GetFloat", IsThreadSafe = true)]
		private float GetFloatInternal(ref AnimationStream stream)
		{
			return PropertySceneHandle.GetFloatInternal_Injected(ref this, ref stream);
		}

		// Token: 0x0600023D RID: 573 RVA: 0x00004C2D File Offset: 0x00002E2D
		[NativeMethod(Name = "SetFloat", IsThreadSafe = true)]
		private void SetFloatInternal(ref AnimationStream stream, float value)
		{
			PropertySceneHandle.SetFloatInternal_Injected(ref this, ref stream, value);
		}

		// Token: 0x0600023E RID: 574 RVA: 0x00004C37 File Offset: 0x00002E37
		[NativeMethod(Name = "GetInt", IsThreadSafe = true)]
		private int GetIntInternal(ref AnimationStream stream)
		{
			return PropertySceneHandle.GetIntInternal_Injected(ref this, ref stream);
		}

		// Token: 0x0600023F RID: 575 RVA: 0x00004C40 File Offset: 0x00002E40
		[NativeMethod(Name = "SetInt", IsThreadSafe = true)]
		private void SetIntInternal(ref AnimationStream stream, int value)
		{
			PropertySceneHandle.SetIntInternal_Injected(ref this, ref stream, value);
		}

		// Token: 0x06000240 RID: 576 RVA: 0x00004C4A File Offset: 0x00002E4A
		[NativeMethod(Name = "GetBool", IsThreadSafe = true)]
		private bool GetBoolInternal(ref AnimationStream stream)
		{
			return PropertySceneHandle.GetBoolInternal_Injected(ref this, ref stream);
		}

		// Token: 0x06000241 RID: 577 RVA: 0x00004C53 File Offset: 0x00002E53
		[NativeMethod(Name = "SetBool", IsThreadSafe = true)]
		private void SetBoolInternal(ref AnimationStream stream, bool value)
		{
			PropertySceneHandle.SetBoolInternal_Injected(ref this, ref stream, value);
		}

		// Token: 0x06000242 RID: 578
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool HasValidTransform_Injected(ref PropertySceneHandle _unity_self, ref AnimationStream stream);

		// Token: 0x06000243 RID: 579
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsBound_Injected(ref PropertySceneHandle _unity_self, ref AnimationStream stream);

		// Token: 0x06000244 RID: 580
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ResolveInternal_Injected(ref PropertySceneHandle _unity_self, ref AnimationStream stream);

		// Token: 0x06000245 RID: 581
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetFloatInternal_Injected(ref PropertySceneHandle _unity_self, ref AnimationStream stream);

		// Token: 0x06000246 RID: 582
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetFloatInternal_Injected(ref PropertySceneHandle _unity_self, ref AnimationStream stream, float value);

		// Token: 0x06000247 RID: 583
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetIntInternal_Injected(ref PropertySceneHandle _unity_self, ref AnimationStream stream);

		// Token: 0x06000248 RID: 584
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetIntInternal_Injected(ref PropertySceneHandle _unity_self, ref AnimationStream stream, int value);

		// Token: 0x06000249 RID: 585
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetBoolInternal_Injected(ref PropertySceneHandle _unity_self, ref AnimationStream stream);

		// Token: 0x0600024A RID: 586
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetBoolInternal_Injected(ref PropertySceneHandle _unity_self, ref AnimationStream stream, bool value);

		// Token: 0x04000039 RID: 57
		private uint valid;

		// Token: 0x0400003A RID: 58
		private int handleIndex;
	}
}
