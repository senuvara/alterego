﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002A RID: 42
	public enum AnimatorUpdateMode
	{
		// Token: 0x04000066 RID: 102
		Normal,
		// Token: 0x04000067 RID: 103
		AnimatePhysics,
		// Token: 0x04000068 RID: 104
		UnscaledTime
	}
}
