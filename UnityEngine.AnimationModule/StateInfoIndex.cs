﻿using System;

namespace UnityEngine
{
	// Token: 0x02000026 RID: 38
	internal enum StateInfoIndex
	{
		// Token: 0x04000056 RID: 86
		CurrentState,
		// Token: 0x04000057 RID: 87
		NextState,
		// Token: 0x04000058 RID: 88
		ExitState,
		// Token: 0x04000059 RID: 89
		InterruptedState
	}
}
