﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200002D RID: 45
	[NativeHeader("Runtime/Animation/AnimatorInfo.h")]
	[RequiredByNativeCode]
	public struct AnimatorTransitionInfo
	{
		// Token: 0x06000259 RID: 601 RVA: 0x00004E18 File Offset: 0x00003018
		public bool IsName(string name)
		{
			return Animator.StringToHash(name) == this.m_Name || Animator.StringToHash(name) == this.m_FullPath;
		}

		// Token: 0x0600025A RID: 602 RVA: 0x00004E50 File Offset: 0x00003050
		public bool IsUserName(string name)
		{
			return Animator.StringToHash(name) == this.m_UserName;
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600025B RID: 603 RVA: 0x00004E74 File Offset: 0x00003074
		public int fullPathHash
		{
			get
			{
				return this.m_FullPath;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600025C RID: 604 RVA: 0x00004E90 File Offset: 0x00003090
		public int nameHash
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x0600025D RID: 605 RVA: 0x00004EAC File Offset: 0x000030AC
		public int userNameHash
		{
			get
			{
				return this.m_UserName;
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x0600025E RID: 606 RVA: 0x00004EC8 File Offset: 0x000030C8
		public DurationUnit durationUnit
		{
			get
			{
				return (!this.m_HasFixedDuration) ? DurationUnit.Normalized : DurationUnit.Fixed;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x0600025F RID: 607 RVA: 0x00004EF0 File Offset: 0x000030F0
		public float duration
		{
			get
			{
				return this.m_Duration;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000260 RID: 608 RVA: 0x00004F0C File Offset: 0x0000310C
		public float normalizedTime
		{
			get
			{
				return this.m_NormalizedTime;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000261 RID: 609 RVA: 0x00004F28 File Offset: 0x00003128
		public bool anyState
		{
			get
			{
				return this.m_AnyState;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000262 RID: 610 RVA: 0x00004F44 File Offset: 0x00003144
		internal bool entry
		{
			get
			{
				return (this.m_TransitionType & 2) != 0;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000263 RID: 611 RVA: 0x00004F68 File Offset: 0x00003168
		internal bool exit
		{
			get
			{
				return (this.m_TransitionType & 4) != 0;
			}
		}

		// Token: 0x04000074 RID: 116
		[NativeName("fullPathHash")]
		private int m_FullPath;

		// Token: 0x04000075 RID: 117
		[NativeName("userNameHash")]
		private int m_UserName;

		// Token: 0x04000076 RID: 118
		[NativeName("nameHash")]
		private int m_Name;

		// Token: 0x04000077 RID: 119
		[NativeName("hasFixedDuration")]
		private bool m_HasFixedDuration;

		// Token: 0x04000078 RID: 120
		[NativeName("duration")]
		private float m_Duration;

		// Token: 0x04000079 RID: 121
		[NativeName("normalizedTime")]
		private float m_NormalizedTime;

		// Token: 0x0400007A RID: 122
		[NativeName("anyState")]
		private bool m_AnyState;

		// Token: 0x0400007B RID: 123
		[NativeName("transitionType")]
		private int m_TransitionType;
	}
}
