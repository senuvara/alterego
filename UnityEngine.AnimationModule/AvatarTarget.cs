﻿using System;

namespace UnityEngine
{
	// Token: 0x02000021 RID: 33
	public enum AvatarTarget
	{
		// Token: 0x0400003C RID: 60
		Root,
		// Token: 0x0400003D RID: 61
		Body,
		// Token: 0x0400003E RID: 62
		LeftFoot,
		// Token: 0x0400003F RID: 63
		RightFoot,
		// Token: 0x04000040 RID: 64
		LeftHand,
		// Token: 0x04000041 RID: 65
		RightHand
	}
}
