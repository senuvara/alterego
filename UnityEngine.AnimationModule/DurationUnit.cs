﻿using System;

namespace UnityEngine
{
	// Token: 0x02000028 RID: 40
	public enum DurationUnit
	{
		// Token: 0x0400005F RID: 95
		Fixed,
		// Token: 0x04000060 RID: 96
		Normalized
	}
}
