﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000009 RID: 9
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	[RequiredByNativeCode]
	public sealed class SharedBetweenAnimatorsAttribute : Attribute
	{
		// Token: 0x06000014 RID: 20 RVA: 0x00002330 File Offset: 0x00000530
		public SharedBetweenAnimatorsAttribute()
		{
		}
	}
}
