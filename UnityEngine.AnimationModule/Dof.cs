﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003D RID: 61
	internal enum Dof
	{
		// Token: 0x040000CF RID: 207
		BodyDofStart,
		// Token: 0x040000D0 RID: 208
		HeadDofStart = 9,
		// Token: 0x040000D1 RID: 209
		LeftLegDofStart = 21,
		// Token: 0x040000D2 RID: 210
		RightLegDofStart = 29,
		// Token: 0x040000D3 RID: 211
		LeftArmDofStart = 37,
		// Token: 0x040000D4 RID: 212
		RightArmDofStart = 46,
		// Token: 0x040000D5 RID: 213
		LeftThumbDofStart = 55,
		// Token: 0x040000D6 RID: 214
		LeftIndexDofStart = 59,
		// Token: 0x040000D7 RID: 215
		LeftMiddleDofStart = 63,
		// Token: 0x040000D8 RID: 216
		LeftRingDofStart = 67,
		// Token: 0x040000D9 RID: 217
		LeftLittleDofStart = 71,
		// Token: 0x040000DA RID: 218
		RightThumbDofStart = 75,
		// Token: 0x040000DB RID: 219
		RightIndexDofStart = 79,
		// Token: 0x040000DC RID: 220
		RightMiddleDofStart = 83,
		// Token: 0x040000DD RID: 221
		RightRingDofStart = 87,
		// Token: 0x040000DE RID: 222
		RightLittleDofStart = 91,
		// Token: 0x040000DF RID: 223
		LastDof = 95
	}
}
