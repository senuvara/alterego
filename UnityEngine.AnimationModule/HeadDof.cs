﻿using System;

namespace UnityEngine
{
	// Token: 0x02000038 RID: 56
	public enum HeadDof
	{
		// Token: 0x04000094 RID: 148
		NeckFrontBack,
		// Token: 0x04000095 RID: 149
		NeckLeftRight,
		// Token: 0x04000096 RID: 150
		NeckRollLeftRight,
		// Token: 0x04000097 RID: 151
		HeadFrontBack,
		// Token: 0x04000098 RID: 152
		HeadLeftRight,
		// Token: 0x04000099 RID: 153
		HeadRollLeftRight,
		// Token: 0x0400009A RID: 154
		LeftEyeDownUp,
		// Token: 0x0400009B RID: 155
		LeftEyeInOut,
		// Token: 0x0400009C RID: 156
		RightEyeDownUp,
		// Token: 0x0400009D RID: 157
		RightEyeInOut,
		// Token: 0x0400009E RID: 158
		JawDownUp,
		// Token: 0x0400009F RID: 159
		JawLeftRight,
		// Token: 0x040000A0 RID: 160
		LastHeadDof
	}
}
