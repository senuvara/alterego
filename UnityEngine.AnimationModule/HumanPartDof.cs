﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003C RID: 60
	public enum HumanPartDof
	{
		// Token: 0x040000BD RID: 189
		Body,
		// Token: 0x040000BE RID: 190
		Head,
		// Token: 0x040000BF RID: 191
		LeftLeg,
		// Token: 0x040000C0 RID: 192
		RightLeg,
		// Token: 0x040000C1 RID: 193
		LeftArm,
		// Token: 0x040000C2 RID: 194
		RightArm,
		// Token: 0x040000C3 RID: 195
		LeftThumb,
		// Token: 0x040000C4 RID: 196
		LeftIndex,
		// Token: 0x040000C5 RID: 197
		LeftMiddle,
		// Token: 0x040000C6 RID: 198
		LeftRing,
		// Token: 0x040000C7 RID: 199
		LeftLittle,
		// Token: 0x040000C8 RID: 200
		RightThumb,
		// Token: 0x040000C9 RID: 201
		RightIndex,
		// Token: 0x040000CA RID: 202
		RightMiddle,
		// Token: 0x040000CB RID: 203
		RightRing,
		// Token: 0x040000CC RID: 204
		RightLittle,
		// Token: 0x040000CD RID: 205
		LastHumanPartDof
	}
}
