﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000058 RID: 88
	[RequiredByNativeCode]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class AnimationEvent
	{
		// Token: 0x06000587 RID: 1415 RVA: 0x00007AF0 File Offset: 0x00005CF0
		public AnimationEvent()
		{
			this.m_Time = 0f;
			this.m_FunctionName = "";
			this.m_StringParameter = "";
			this.m_ObjectReferenceParameter = null;
			this.m_FloatParameter = 0f;
			this.m_IntParameter = 0;
			this.m_MessageOptions = 0;
			this.m_Source = AnimationEventSource.NoSource;
			this.m_StateSender = null;
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000588 RID: 1416 RVA: 0x00007B54 File Offset: 0x00005D54
		// (set) Token: 0x06000589 RID: 1417 RVA: 0x00007B6F File Offset: 0x00005D6F
		[Obsolete("Use stringParameter instead")]
		public string data
		{
			get
			{
				return this.m_StringParameter;
			}
			set
			{
				this.m_StringParameter = value;
			}
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x0600058A RID: 1418 RVA: 0x00007B7C File Offset: 0x00005D7C
		// (set) Token: 0x0600058B RID: 1419 RVA: 0x00007B6F File Offset: 0x00005D6F
		public string stringParameter
		{
			get
			{
				return this.m_StringParameter;
			}
			set
			{
				this.m_StringParameter = value;
			}
		}

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x0600058C RID: 1420 RVA: 0x00007B98 File Offset: 0x00005D98
		// (set) Token: 0x0600058D RID: 1421 RVA: 0x00007BB3 File Offset: 0x00005DB3
		public float floatParameter
		{
			get
			{
				return this.m_FloatParameter;
			}
			set
			{
				this.m_FloatParameter = value;
			}
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x0600058E RID: 1422 RVA: 0x00007BC0 File Offset: 0x00005DC0
		// (set) Token: 0x0600058F RID: 1423 RVA: 0x00007BDB File Offset: 0x00005DDB
		public int intParameter
		{
			get
			{
				return this.m_IntParameter;
			}
			set
			{
				this.m_IntParameter = value;
			}
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x06000590 RID: 1424 RVA: 0x00007BE8 File Offset: 0x00005DE8
		// (set) Token: 0x06000591 RID: 1425 RVA: 0x00007C03 File Offset: 0x00005E03
		public Object objectReferenceParameter
		{
			get
			{
				return this.m_ObjectReferenceParameter;
			}
			set
			{
				this.m_ObjectReferenceParameter = value;
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000592 RID: 1426 RVA: 0x00007C10 File Offset: 0x00005E10
		// (set) Token: 0x06000593 RID: 1427 RVA: 0x00007C2B File Offset: 0x00005E2B
		public string functionName
		{
			get
			{
				return this.m_FunctionName;
			}
			set
			{
				this.m_FunctionName = value;
			}
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x06000594 RID: 1428 RVA: 0x00007C38 File Offset: 0x00005E38
		// (set) Token: 0x06000595 RID: 1429 RVA: 0x00007C53 File Offset: 0x00005E53
		public float time
		{
			get
			{
				return this.m_Time;
			}
			set
			{
				this.m_Time = value;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000596 RID: 1430 RVA: 0x00007C60 File Offset: 0x00005E60
		// (set) Token: 0x06000597 RID: 1431 RVA: 0x00007C7B File Offset: 0x00005E7B
		public SendMessageOptions messageOptions
		{
			get
			{
				return (SendMessageOptions)this.m_MessageOptions;
			}
			set
			{
				this.m_MessageOptions = (int)value;
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000598 RID: 1432 RVA: 0x00007C88 File Offset: 0x00005E88
		public bool isFiredByLegacy
		{
			get
			{
				return this.m_Source == AnimationEventSource.Legacy;
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x06000599 RID: 1433 RVA: 0x00007CA8 File Offset: 0x00005EA8
		public bool isFiredByAnimator
		{
			get
			{
				return this.m_Source == AnimationEventSource.Animator;
			}
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x0600059A RID: 1434 RVA: 0x00007CC8 File Offset: 0x00005EC8
		public AnimationState animationState
		{
			get
			{
				if (!this.isFiredByLegacy)
				{
					Debug.LogError("AnimationEvent was not fired by Animation component, you shouldn't use AnimationEvent.animationState");
				}
				return this.m_StateSender;
			}
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x0600059B RID: 1435 RVA: 0x00007CF8 File Offset: 0x00005EF8
		public AnimatorStateInfo animatorStateInfo
		{
			get
			{
				if (!this.isFiredByAnimator)
				{
					Debug.LogError("AnimationEvent was not fired by Animator component, you shouldn't use AnimationEvent.animatorStateInfo");
				}
				return this.m_AnimatorStateInfo;
			}
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x0600059C RID: 1436 RVA: 0x00007D28 File Offset: 0x00005F28
		public AnimatorClipInfo animatorClipInfo
		{
			get
			{
				if (!this.isFiredByAnimator)
				{
					Debug.LogError("AnimationEvent was not fired by Animator component, you shouldn't use AnimationEvent.animatorClipInfo");
				}
				return this.m_AnimatorClipInfo;
			}
		}

		// Token: 0x0600059D RID: 1437 RVA: 0x00007D58 File Offset: 0x00005F58
		internal int GetHash()
		{
			int hashCode = this.functionName.GetHashCode();
			return 33 * hashCode + this.time.GetHashCode();
		}

		// Token: 0x0400015C RID: 348
		internal float m_Time;

		// Token: 0x0400015D RID: 349
		internal string m_FunctionName;

		// Token: 0x0400015E RID: 350
		internal string m_StringParameter;

		// Token: 0x0400015F RID: 351
		internal Object m_ObjectReferenceParameter;

		// Token: 0x04000160 RID: 352
		internal float m_FloatParameter;

		// Token: 0x04000161 RID: 353
		internal int m_IntParameter;

		// Token: 0x04000162 RID: 354
		internal int m_MessageOptions;

		// Token: 0x04000163 RID: 355
		internal AnimationEventSource m_Source;

		// Token: 0x04000164 RID: 356
		internal AnimationState m_StateSender;

		// Token: 0x04000165 RID: 357
		internal AnimatorStateInfo m_AnimatorStateInfo;

		// Token: 0x04000166 RID: 358
		internal AnimatorClipInfo m_AnimatorClipInfo;
	}
}
