﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Playables;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200002F RID: 47
	[NativeHeader("Runtime/Animation/Animator.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimatorControllerParameter.bindings.h")]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Animation/ScriptBindings/Animator.bindings.h")]
	public class Animator : Behaviour
	{
		// Token: 0x06000269 RID: 617 RVA: 0x00004FE9 File Offset: 0x000031E9
		public Animator()
		{
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x0600026A RID: 618
		public extern bool isOptimizable { [NativeMethod("IsOptimizable")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x0600026B RID: 619
		public extern bool isHuman { [NativeMethod("IsHuman")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x0600026C RID: 620
		public extern bool hasRootMotion { [NativeMethod("HasRootMotion")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x0600026D RID: 621
		internal extern bool isRootPositionOrRotationControlledByCurves { [NativeMethod("IsRootTranslationOrRotationControllerByCurves")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600026E RID: 622
		public extern float humanScale { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600026F RID: 623
		public extern bool isInitialized { [NativeMethod("IsInitialized")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000270 RID: 624 RVA: 0x00004FF4 File Offset: 0x000031F4
		public float GetFloat(string name)
		{
			return this.GetFloatString(name);
		}

		// Token: 0x06000271 RID: 625 RVA: 0x00005010 File Offset: 0x00003210
		public float GetFloat(int id)
		{
			return this.GetFloatID(id);
		}

		// Token: 0x06000272 RID: 626 RVA: 0x0000502C File Offset: 0x0000322C
		public void SetFloat(string name, float value)
		{
			this.SetFloatString(name, value);
		}

		// Token: 0x06000273 RID: 627 RVA: 0x00005037 File Offset: 0x00003237
		public void SetFloat(string name, float value, float dampTime, float deltaTime)
		{
			this.SetFloatStringDamp(name, value, dampTime, deltaTime);
		}

		// Token: 0x06000274 RID: 628 RVA: 0x00005045 File Offset: 0x00003245
		public void SetFloat(int id, float value)
		{
			this.SetFloatID(id, value);
		}

		// Token: 0x06000275 RID: 629 RVA: 0x00005050 File Offset: 0x00003250
		public void SetFloat(int id, float value, float dampTime, float deltaTime)
		{
			this.SetFloatIDDamp(id, value, dampTime, deltaTime);
		}

		// Token: 0x06000276 RID: 630 RVA: 0x00005060 File Offset: 0x00003260
		public bool GetBool(string name)
		{
			return this.GetBoolString(name);
		}

		// Token: 0x06000277 RID: 631 RVA: 0x0000507C File Offset: 0x0000327C
		public bool GetBool(int id)
		{
			return this.GetBoolID(id);
		}

		// Token: 0x06000278 RID: 632 RVA: 0x00005098 File Offset: 0x00003298
		public void SetBool(string name, bool value)
		{
			this.SetBoolString(name, value);
		}

		// Token: 0x06000279 RID: 633 RVA: 0x000050A3 File Offset: 0x000032A3
		public void SetBool(int id, bool value)
		{
			this.SetBoolID(id, value);
		}

		// Token: 0x0600027A RID: 634 RVA: 0x000050B0 File Offset: 0x000032B0
		public int GetInteger(string name)
		{
			return this.GetIntegerString(name);
		}

		// Token: 0x0600027B RID: 635 RVA: 0x000050CC File Offset: 0x000032CC
		public int GetInteger(int id)
		{
			return this.GetIntegerID(id);
		}

		// Token: 0x0600027C RID: 636 RVA: 0x000050E8 File Offset: 0x000032E8
		public void SetInteger(string name, int value)
		{
			this.SetIntegerString(name, value);
		}

		// Token: 0x0600027D RID: 637 RVA: 0x000050F3 File Offset: 0x000032F3
		public void SetInteger(int id, int value)
		{
			this.SetIntegerID(id, value);
		}

		// Token: 0x0600027E RID: 638 RVA: 0x000050FE File Offset: 0x000032FE
		public void SetTrigger(string name)
		{
			this.SetTriggerString(name);
		}

		// Token: 0x0600027F RID: 639 RVA: 0x00005108 File Offset: 0x00003308
		public void SetTrigger(int id)
		{
			this.SetTriggerID(id);
		}

		// Token: 0x06000280 RID: 640 RVA: 0x00005112 File Offset: 0x00003312
		public void ResetTrigger(string name)
		{
			this.ResetTriggerString(name);
		}

		// Token: 0x06000281 RID: 641 RVA: 0x0000511C File Offset: 0x0000331C
		public void ResetTrigger(int id)
		{
			this.ResetTriggerID(id);
		}

		// Token: 0x06000282 RID: 642 RVA: 0x00005128 File Offset: 0x00003328
		public bool IsParameterControlledByCurve(string name)
		{
			return this.IsParameterControlledByCurveString(name);
		}

		// Token: 0x06000283 RID: 643 RVA: 0x00005144 File Offset: 0x00003344
		public bool IsParameterControlledByCurve(int id)
		{
			return this.IsParameterControlledByCurveID(id);
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000284 RID: 644 RVA: 0x00005160 File Offset: 0x00003360
		public Vector3 deltaPosition
		{
			get
			{
				Vector3 result;
				this.get_deltaPosition_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000285 RID: 645 RVA: 0x00005178 File Offset: 0x00003378
		public Quaternion deltaRotation
		{
			get
			{
				Quaternion result;
				this.get_deltaRotation_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000286 RID: 646 RVA: 0x00005190 File Offset: 0x00003390
		public Vector3 velocity
		{
			get
			{
				Vector3 result;
				this.get_velocity_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000287 RID: 647 RVA: 0x000051A8 File Offset: 0x000033A8
		public Vector3 angularVelocity
		{
			get
			{
				Vector3 result;
				this.get_angularVelocity_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000288 RID: 648 RVA: 0x000051C0 File Offset: 0x000033C0
		// (set) Token: 0x06000289 RID: 649 RVA: 0x000051D6 File Offset: 0x000033D6
		public Vector3 rootPosition
		{
			[NativeMethod("GetAvatarPosition")]
			get
			{
				Vector3 result;
				this.get_rootPosition_Injected(out result);
				return result;
			}
			[NativeMethod("SetAvatarPosition")]
			set
			{
				this.set_rootPosition_Injected(ref value);
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600028A RID: 650 RVA: 0x000051E0 File Offset: 0x000033E0
		// (set) Token: 0x0600028B RID: 651 RVA: 0x000051F6 File Offset: 0x000033F6
		public Quaternion rootRotation
		{
			[NativeMethod("GetAvatarRotation")]
			get
			{
				Quaternion result;
				this.get_rootRotation_Injected(out result);
				return result;
			}
			[NativeMethod("SetAvatarRotation")]
			set
			{
				this.set_rootRotation_Injected(ref value);
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x0600028C RID: 652
		// (set) Token: 0x0600028D RID: 653
		public extern bool applyRootMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600028E RID: 654
		// (set) Token: 0x0600028F RID: 655
		[Obsolete("Animator.linearVelocityBlending is no longer used and has been deprecated.")]
		public extern bool linearVelocityBlending { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000290 RID: 656 RVA: 0x00005200 File Offset: 0x00003400
		// (set) Token: 0x06000291 RID: 657 RVA: 0x0000521E File Offset: 0x0000341E
		[Obsolete("Animator.animatePhysics has been deprecated. Use Animator.updateMode instead.")]
		public bool animatePhysics
		{
			get
			{
				return this.updateMode == AnimatorUpdateMode.AnimatePhysics;
			}
			set
			{
				this.updateMode = ((!value) ? AnimatorUpdateMode.Normal : AnimatorUpdateMode.AnimatePhysics);
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000292 RID: 658
		// (set) Token: 0x06000293 RID: 659
		public extern AnimatorUpdateMode updateMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000294 RID: 660
		public extern bool hasTransformHierarchy { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000295 RID: 661
		// (set) Token: 0x06000296 RID: 662
		internal extern bool allowConstantClipSamplingOptimization { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000297 RID: 663
		public extern float gravityWeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000298 RID: 664 RVA: 0x00005234 File Offset: 0x00003434
		// (set) Token: 0x06000299 RID: 665 RVA: 0x00005255 File Offset: 0x00003455
		public Vector3 bodyPosition
		{
			get
			{
				this.CheckIfInIKPass();
				return this.bodyPositionInternal;
			}
			set
			{
				this.CheckIfInIKPass();
				this.bodyPositionInternal = value;
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x0600029A RID: 666 RVA: 0x00005268 File Offset: 0x00003468
		// (set) Token: 0x0600029B RID: 667 RVA: 0x0000527E File Offset: 0x0000347E
		internal Vector3 bodyPositionInternal
		{
			[NativeMethod("GetBodyPosition")]
			get
			{
				Vector3 result;
				this.get_bodyPositionInternal_Injected(out result);
				return result;
			}
			[NativeMethod("SetBodyPosition")]
			set
			{
				this.set_bodyPositionInternal_Injected(ref value);
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x0600029C RID: 668 RVA: 0x00005288 File Offset: 0x00003488
		// (set) Token: 0x0600029D RID: 669 RVA: 0x000052A9 File Offset: 0x000034A9
		public Quaternion bodyRotation
		{
			get
			{
				this.CheckIfInIKPass();
				return this.bodyRotationInternal;
			}
			set
			{
				this.CheckIfInIKPass();
				this.bodyRotationInternal = value;
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x0600029E RID: 670 RVA: 0x000052BC File Offset: 0x000034BC
		// (set) Token: 0x0600029F RID: 671 RVA: 0x000052D2 File Offset: 0x000034D2
		internal Quaternion bodyRotationInternal
		{
			[NativeMethod("GetBodyRotation")]
			get
			{
				Quaternion result;
				this.get_bodyRotationInternal_Injected(out result);
				return result;
			}
			[NativeMethod("SetBodyRotation")]
			set
			{
				this.set_bodyRotationInternal_Injected(ref value);
			}
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x000052DC File Offset: 0x000034DC
		public Vector3 GetIKPosition(AvatarIKGoal goal)
		{
			this.CheckIfInIKPass();
			return this.GetGoalPosition(goal);
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x00005300 File Offset: 0x00003500
		private Vector3 GetGoalPosition(AvatarIKGoal goal)
		{
			Vector3 result;
			this.GetGoalPosition_Injected(goal, out result);
			return result;
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x00005317 File Offset: 0x00003517
		public void SetIKPosition(AvatarIKGoal goal, Vector3 goalPosition)
		{
			this.CheckIfInIKPass();
			this.SetGoalPosition(goal, goalPosition);
		}

		// Token: 0x060002A3 RID: 675 RVA: 0x00005328 File Offset: 0x00003528
		private void SetGoalPosition(AvatarIKGoal goal, Vector3 goalPosition)
		{
			this.SetGoalPosition_Injected(goal, ref goalPosition);
		}

		// Token: 0x060002A4 RID: 676 RVA: 0x00005334 File Offset: 0x00003534
		public Quaternion GetIKRotation(AvatarIKGoal goal)
		{
			this.CheckIfInIKPass();
			return this.GetGoalRotation(goal);
		}

		// Token: 0x060002A5 RID: 677 RVA: 0x00005358 File Offset: 0x00003558
		private Quaternion GetGoalRotation(AvatarIKGoal goal)
		{
			Quaternion result;
			this.GetGoalRotation_Injected(goal, out result);
			return result;
		}

		// Token: 0x060002A6 RID: 678 RVA: 0x0000536F File Offset: 0x0000356F
		public void SetIKRotation(AvatarIKGoal goal, Quaternion goalRotation)
		{
			this.CheckIfInIKPass();
			this.SetGoalRotation(goal, goalRotation);
		}

		// Token: 0x060002A7 RID: 679 RVA: 0x00005380 File Offset: 0x00003580
		private void SetGoalRotation(AvatarIKGoal goal, Quaternion goalRotation)
		{
			this.SetGoalRotation_Injected(goal, ref goalRotation);
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x0000538C File Offset: 0x0000358C
		public float GetIKPositionWeight(AvatarIKGoal goal)
		{
			this.CheckIfInIKPass();
			return this.GetGoalWeightPosition(goal);
		}

		// Token: 0x060002A9 RID: 681
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetGoalWeightPosition(AvatarIKGoal goal);

		// Token: 0x060002AA RID: 682 RVA: 0x000053AE File Offset: 0x000035AE
		public void SetIKPositionWeight(AvatarIKGoal goal, float value)
		{
			this.CheckIfInIKPass();
			this.SetGoalWeightPosition(goal, value);
		}

		// Token: 0x060002AB RID: 683
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetGoalWeightPosition(AvatarIKGoal goal, float value);

		// Token: 0x060002AC RID: 684 RVA: 0x000053C0 File Offset: 0x000035C0
		public float GetIKRotationWeight(AvatarIKGoal goal)
		{
			this.CheckIfInIKPass();
			return this.GetGoalWeightRotation(goal);
		}

		// Token: 0x060002AD RID: 685
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetGoalWeightRotation(AvatarIKGoal goal);

		// Token: 0x060002AE RID: 686 RVA: 0x000053E2 File Offset: 0x000035E2
		public void SetIKRotationWeight(AvatarIKGoal goal, float value)
		{
			this.CheckIfInIKPass();
			this.SetGoalWeightRotation(goal, value);
		}

		// Token: 0x060002AF RID: 687
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetGoalWeightRotation(AvatarIKGoal goal, float value);

		// Token: 0x060002B0 RID: 688 RVA: 0x000053F4 File Offset: 0x000035F4
		public Vector3 GetIKHintPosition(AvatarIKHint hint)
		{
			this.CheckIfInIKPass();
			return this.GetHintPosition(hint);
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x00005418 File Offset: 0x00003618
		private Vector3 GetHintPosition(AvatarIKHint hint)
		{
			Vector3 result;
			this.GetHintPosition_Injected(hint, out result);
			return result;
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x0000542F File Offset: 0x0000362F
		public void SetIKHintPosition(AvatarIKHint hint, Vector3 hintPosition)
		{
			this.CheckIfInIKPass();
			this.SetHintPosition(hint, hintPosition);
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x00005440 File Offset: 0x00003640
		private void SetHintPosition(AvatarIKHint hint, Vector3 hintPosition)
		{
			this.SetHintPosition_Injected(hint, ref hintPosition);
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x0000544C File Offset: 0x0000364C
		public float GetIKHintPositionWeight(AvatarIKHint hint)
		{
			this.CheckIfInIKPass();
			return this.GetHintWeightPosition(hint);
		}

		// Token: 0x060002B5 RID: 693
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetHintWeightPosition(AvatarIKHint hint);

		// Token: 0x060002B6 RID: 694 RVA: 0x0000546E File Offset: 0x0000366E
		public void SetIKHintPositionWeight(AvatarIKHint hint, float value)
		{
			this.CheckIfInIKPass();
			this.SetHintWeightPosition(hint, value);
		}

		// Token: 0x060002B7 RID: 695
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetHintWeightPosition(AvatarIKHint hint, float value);

		// Token: 0x060002B8 RID: 696 RVA: 0x0000547F File Offset: 0x0000367F
		public void SetLookAtPosition(Vector3 lookAtPosition)
		{
			this.CheckIfInIKPass();
			this.SetLookAtPositionInternal(lookAtPosition);
		}

		// Token: 0x060002B9 RID: 697 RVA: 0x0000548F File Offset: 0x0000368F
		[NativeMethod("SetLookAtPosition")]
		private void SetLookAtPositionInternal(Vector3 lookAtPosition)
		{
			this.SetLookAtPositionInternal_Injected(ref lookAtPosition);
		}

		// Token: 0x060002BA RID: 698 RVA: 0x00005499 File Offset: 0x00003699
		public void SetLookAtWeight(float weight)
		{
			this.CheckIfInIKPass();
			this.SetLookAtWeightInternal(weight, 0f, 1f, 0f, 0.5f);
		}

		// Token: 0x060002BB RID: 699 RVA: 0x000054BD File Offset: 0x000036BD
		public void SetLookAtWeight(float weight, float bodyWeight)
		{
			this.CheckIfInIKPass();
			this.SetLookAtWeightInternal(weight, bodyWeight, 1f, 0f, 0.5f);
		}

		// Token: 0x060002BC RID: 700 RVA: 0x000054DD File Offset: 0x000036DD
		public void SetLookAtWeight(float weight, float bodyWeight, float headWeight)
		{
			this.CheckIfInIKPass();
			this.SetLookAtWeightInternal(weight, bodyWeight, headWeight, 0f, 0.5f);
		}

		// Token: 0x060002BD RID: 701 RVA: 0x000054F9 File Offset: 0x000036F9
		public void SetLookAtWeight(float weight, float bodyWeight, float headWeight, float eyesWeight)
		{
			this.CheckIfInIKPass();
			this.SetLookAtWeightInternal(weight, bodyWeight, headWeight, eyesWeight, 0.5f);
		}

		// Token: 0x060002BE RID: 702 RVA: 0x00005512 File Offset: 0x00003712
		public void SetLookAtWeight(float weight, [UnityEngine.Internal.DefaultValue("0.0f")] float bodyWeight, [UnityEngine.Internal.DefaultValue("1.0f")] float headWeight, [UnityEngine.Internal.DefaultValue("0.0f")] float eyesWeight, [UnityEngine.Internal.DefaultValue("0.5f")] float clampWeight)
		{
			this.CheckIfInIKPass();
			this.SetLookAtWeightInternal(weight, bodyWeight, headWeight, eyesWeight, clampWeight);
		}

		// Token: 0x060002BF RID: 703
		[NativeMethod("SetLookAtWeight")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetLookAtWeightInternal(float weight, float bodyWeight, float headWeight, float eyesWeight, float clampWeight);

		// Token: 0x060002C0 RID: 704 RVA: 0x00005528 File Offset: 0x00003728
		public void SetBoneLocalRotation(HumanBodyBones humanBoneId, Quaternion rotation)
		{
			this.CheckIfInIKPass();
			this.SetBoneLocalRotationInternal(HumanTrait.GetBoneIndexFromMono((int)humanBoneId), rotation);
		}

		// Token: 0x060002C1 RID: 705 RVA: 0x0000553E File Offset: 0x0000373E
		[NativeMethod("SetBoneLocalRotation")]
		private void SetBoneLocalRotationInternal(int humanBoneId, Quaternion rotation)
		{
			this.SetBoneLocalRotationInternal_Injected(humanBoneId, ref rotation);
		}

		// Token: 0x060002C2 RID: 706
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern ScriptableObject GetBehaviour([NotNull] Type type);

		// Token: 0x060002C3 RID: 707 RVA: 0x0000554C File Offset: 0x0000374C
		public T GetBehaviour<T>() where T : StateMachineBehaviour
		{
			return this.GetBehaviour(typeof(T)) as T;
		}

		// Token: 0x060002C4 RID: 708 RVA: 0x0000557C File Offset: 0x0000377C
		private static T[] ConvertStateMachineBehaviour<T>(ScriptableObject[] rawObjects) where T : StateMachineBehaviour
		{
			T[] result;
			if (rawObjects == null)
			{
				result = null;
			}
			else
			{
				T[] array = new T[rawObjects.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = (T)((object)rawObjects[i]);
				}
				result = array;
			}
			return result;
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x000055CC File Offset: 0x000037CC
		public T[] GetBehaviours<T>() where T : StateMachineBehaviour
		{
			return Animator.ConvertStateMachineBehaviour<T>(this.InternalGetBehaviours(typeof(T)));
		}

		// Token: 0x060002C6 RID: 710
		[FreeFunction(Name = "AnimatorBindings::InternalGetBehaviours", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern ScriptableObject[] InternalGetBehaviours([NotNull] Type type);

		// Token: 0x060002C7 RID: 711 RVA: 0x000055F8 File Offset: 0x000037F8
		public StateMachineBehaviour[] GetBehaviours(int fullPathHash, int layerIndex)
		{
			return this.InternalGetBehavioursByKey(fullPathHash, layerIndex, typeof(StateMachineBehaviour)) as StateMachineBehaviour[];
		}

		// Token: 0x060002C8 RID: 712
		[FreeFunction(Name = "AnimatorBindings::InternalGetBehavioursByKey", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern ScriptableObject[] InternalGetBehavioursByKey(int fullPathHash, int layerIndex, [NotNull] Type type);

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060002C9 RID: 713
		// (set) Token: 0x060002CA RID: 714
		public extern bool stabilizeFeet { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060002CB RID: 715
		public extern int layerCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060002CC RID: 716
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetLayerName(int layerIndex);

		// Token: 0x060002CD RID: 717
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetLayerIndex(string layerName);

		// Token: 0x060002CE RID: 718
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetLayerWeight(int layerIndex);

		// Token: 0x060002CF RID: 719
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetLayerWeight(int layerIndex, float weight);

		// Token: 0x060002D0 RID: 720
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetAnimatorStateInfo(int layerIndex, StateInfoIndex stateInfoIndex, out AnimatorStateInfo info);

		// Token: 0x060002D1 RID: 721 RVA: 0x00005624 File Offset: 0x00003824
		public AnimatorStateInfo GetCurrentAnimatorStateInfo(int layerIndex)
		{
			AnimatorStateInfo result;
			this.GetAnimatorStateInfo(layerIndex, StateInfoIndex.CurrentState, out result);
			return result;
		}

		// Token: 0x060002D2 RID: 722 RVA: 0x00005644 File Offset: 0x00003844
		public AnimatorStateInfo GetNextAnimatorStateInfo(int layerIndex)
		{
			AnimatorStateInfo result;
			this.GetAnimatorStateInfo(layerIndex, StateInfoIndex.NextState, out result);
			return result;
		}

		// Token: 0x060002D3 RID: 723
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetAnimatorTransitionInfo(int layerIndex, out AnimatorTransitionInfo info);

		// Token: 0x060002D4 RID: 724 RVA: 0x00005664 File Offset: 0x00003864
		public AnimatorTransitionInfo GetAnimatorTransitionInfo(int layerIndex)
		{
			AnimatorTransitionInfo result;
			this.GetAnimatorTransitionInfo(layerIndex, out result);
			return result;
		}

		// Token: 0x060002D5 RID: 725
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetAnimatorClipInfoCount(int layerIndex, bool current);

		// Token: 0x060002D6 RID: 726 RVA: 0x00005684 File Offset: 0x00003884
		public int GetCurrentAnimatorClipInfoCount(int layerIndex)
		{
			return this.GetAnimatorClipInfoCount(layerIndex, true);
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x000056A4 File Offset: 0x000038A4
		public int GetNextAnimatorClipInfoCount(int layerIndex)
		{
			return this.GetAnimatorClipInfoCount(layerIndex, false);
		}

		// Token: 0x060002D8 RID: 728
		[FreeFunction(Name = "AnimatorBindings::GetCurrentAnimatorClipInfo", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimatorClipInfo[] GetCurrentAnimatorClipInfo(int layerIndex);

		// Token: 0x060002D9 RID: 729
		[FreeFunction(Name = "AnimatorBindings::GetNextAnimatorClipInfo", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimatorClipInfo[] GetNextAnimatorClipInfo(int layerIndex);

		// Token: 0x060002DA RID: 730 RVA: 0x000056C1 File Offset: 0x000038C1
		public void GetCurrentAnimatorClipInfo(int layerIndex, List<AnimatorClipInfo> clips)
		{
			if (clips == null)
			{
				throw new ArgumentNullException("clips");
			}
			this.GetAnimatorClipInfoInternal(layerIndex, true, clips);
		}

		// Token: 0x060002DB RID: 731
		[FreeFunction(Name = "AnimatorBindings::GetAnimatorClipInfoInternal", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetAnimatorClipInfoInternal(int layerIndex, bool isCurrent, object clips);

		// Token: 0x060002DC RID: 732
		[NativeConditional("ENABLE_DOTNET")]
		[FreeFunction(Name = "AnimatorBindings::GetAnimatorClipInfoInternalWinRT", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimatorClipInfo[] GetAnimatorClipInfoInternalWinRT(int layerIndex, bool isCurrent);

		// Token: 0x060002DD RID: 733 RVA: 0x000056DE File Offset: 0x000038DE
		public void GetNextAnimatorClipInfo(int layerIndex, List<AnimatorClipInfo> clips)
		{
			if (clips == null)
			{
				throw new ArgumentNullException("clips");
			}
			this.GetAnimatorClipInfoInternal(layerIndex, false, clips);
		}

		// Token: 0x060002DE RID: 734
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsInTransition(int layerIndex);

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x060002DF RID: 735
		public extern AnimatorControllerParameter[] parameters { [FreeFunction(Name = "AnimatorBindings::GetParameters", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060002E0 RID: 736
		public extern int parameterCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060002E1 RID: 737 RVA: 0x000056FC File Offset: 0x000038FC
		public AnimatorControllerParameter GetParameter(int index)
		{
			AnimatorControllerParameter[] parameters = this.parameters;
			if (index < 0 || index >= this.parameters.Length)
			{
				throw new IndexOutOfRangeException("Index must be between 0 and " + this.parameters.Length);
			}
			return parameters[index];
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x060002E2 RID: 738
		// (set) Token: 0x060002E3 RID: 739
		public extern float feetPivotActive { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x060002E4 RID: 740
		public extern float pivotWeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060002E5 RID: 741 RVA: 0x00005750 File Offset: 0x00003950
		public Vector3 pivotPosition
		{
			get
			{
				Vector3 result;
				this.get_pivotPosition_Injected(out result);
				return result;
			}
		}

		// Token: 0x060002E6 RID: 742 RVA: 0x00005766 File Offset: 0x00003966
		private void MatchTarget(Vector3 matchPosition, Quaternion matchRotation, int targetBodyPart, MatchTargetWeightMask weightMask, float startNormalizedTime, float targetNormalizedTime)
		{
			this.MatchTarget_Injected(ref matchPosition, ref matchRotation, targetBodyPart, ref weightMask, startNormalizedTime, targetNormalizedTime);
		}

		// Token: 0x060002E7 RID: 743 RVA: 0x00005779 File Offset: 0x00003979
		public void MatchTarget(Vector3 matchPosition, Quaternion matchRotation, AvatarTarget targetBodyPart, MatchTargetWeightMask weightMask, float startNormalizedTime)
		{
			this.MatchTarget(matchPosition, matchRotation, (int)targetBodyPart, weightMask, startNormalizedTime, 1f);
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x0000578E File Offset: 0x0000398E
		public void MatchTarget(Vector3 matchPosition, Quaternion matchRotation, AvatarTarget targetBodyPart, MatchTargetWeightMask weightMask, float startNormalizedTime, [UnityEngine.Internal.DefaultValue("1")] float targetNormalizedTime)
		{
			this.MatchTarget(matchPosition, matchRotation, (int)targetBodyPart, weightMask, startNormalizedTime, targetNormalizedTime);
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x000057A0 File Offset: 0x000039A0
		public void InterruptMatchTarget()
		{
			this.InterruptMatchTarget(true);
		}

		// Token: 0x060002EA RID: 746
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InterruptMatchTarget([UnityEngine.Internal.DefaultValue("true")] bool completeMatch);

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x060002EB RID: 747
		public extern bool isMatchingTarget { [NativeMethod("IsMatchingTarget")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060002EC RID: 748
		// (set) Token: 0x060002ED RID: 749
		public extern float speed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060002EE RID: 750 RVA: 0x000057AA File Offset: 0x000039AA
		[Obsolete("ForceStateNormalizedTime is deprecated. Please use Play or CrossFade instead.")]
		public void ForceStateNormalizedTime(float normalizedTime)
		{
			this.Play(0, 0, normalizedTime);
		}

		// Token: 0x060002EF RID: 751 RVA: 0x000057B8 File Offset: 0x000039B8
		public void CrossFadeInFixedTime(string stateName, float fixedTransitionDuration)
		{
			float normalizedTransitionTime = 0f;
			float fixedTimeOffset = 0f;
			int layer = -1;
			this.CrossFadeInFixedTime(Animator.StringToHash(stateName), fixedTransitionDuration, layer, fixedTimeOffset, normalizedTransitionTime);
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x000057E4 File Offset: 0x000039E4
		public void CrossFadeInFixedTime(string stateName, float fixedTransitionDuration, int layer)
		{
			float normalizedTransitionTime = 0f;
			float fixedTimeOffset = 0f;
			this.CrossFadeInFixedTime(Animator.StringToHash(stateName), fixedTransitionDuration, layer, fixedTimeOffset, normalizedTransitionTime);
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x00005810 File Offset: 0x00003A10
		public void CrossFadeInFixedTime(string stateName, float fixedTransitionDuration, int layer, float fixedTimeOffset)
		{
			float normalizedTransitionTime = 0f;
			this.CrossFadeInFixedTime(Animator.StringToHash(stateName), fixedTransitionDuration, layer, fixedTimeOffset, normalizedTransitionTime);
		}

		// Token: 0x060002F2 RID: 754 RVA: 0x00005835 File Offset: 0x00003A35
		public void CrossFadeInFixedTime(string stateName, float fixedTransitionDuration, [UnityEngine.Internal.DefaultValue("-1")] int layer, [UnityEngine.Internal.DefaultValue("0.0f")] float fixedTimeOffset, [UnityEngine.Internal.DefaultValue("0.0f")] float normalizedTransitionTime)
		{
			this.CrossFadeInFixedTime(Animator.StringToHash(stateName), fixedTransitionDuration, layer, fixedTimeOffset, normalizedTransitionTime);
		}

		// Token: 0x060002F3 RID: 755 RVA: 0x0000584C File Offset: 0x00003A4C
		public void CrossFadeInFixedTime(int stateHashName, float fixedTransitionDuration, int layer, float fixedTimeOffset)
		{
			float normalizedTransitionTime = 0f;
			this.CrossFadeInFixedTime(stateHashName, fixedTransitionDuration, layer, fixedTimeOffset, normalizedTransitionTime);
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x0000586C File Offset: 0x00003A6C
		public void CrossFadeInFixedTime(int stateHashName, float fixedTransitionDuration, int layer)
		{
			float normalizedTransitionTime = 0f;
			float fixedTimeOffset = 0f;
			this.CrossFadeInFixedTime(stateHashName, fixedTransitionDuration, layer, fixedTimeOffset, normalizedTransitionTime);
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x00005894 File Offset: 0x00003A94
		public void CrossFadeInFixedTime(int stateHashName, float fixedTransitionDuration)
		{
			float normalizedTransitionTime = 0f;
			float fixedTimeOffset = 0f;
			int layer = -1;
			this.CrossFadeInFixedTime(stateHashName, fixedTransitionDuration, layer, fixedTimeOffset, normalizedTransitionTime);
		}

		// Token: 0x060002F6 RID: 758
		[FreeFunction(Name = "AnimatorBindings::CrossFadeInFixedTime", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CrossFadeInFixedTime(int stateHashName, float fixedTransitionDuration, [UnityEngine.Internal.DefaultValue("-1")] int layer, [UnityEngine.Internal.DefaultValue("0.0f")] float fixedTimeOffset, [UnityEngine.Internal.DefaultValue("0.0f")] float normalizedTransitionTime);

		// Token: 0x060002F7 RID: 759
		[FreeFunction(Name = "AnimatorBindings::WriteDefaultValues", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void WriteDefaultValues();

		// Token: 0x060002F8 RID: 760 RVA: 0x000058BC File Offset: 0x00003ABC
		public void CrossFade(string stateName, float normalizedTransitionDuration, int layer, float normalizedTimeOffset)
		{
			float normalizedTransitionTime = 0f;
			this.CrossFade(stateName, normalizedTransitionDuration, layer, normalizedTimeOffset, normalizedTransitionTime);
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x000058DC File Offset: 0x00003ADC
		public void CrossFade(string stateName, float normalizedTransitionDuration, int layer)
		{
			float normalizedTransitionTime = 0f;
			float negativeInfinity = float.NegativeInfinity;
			this.CrossFade(stateName, normalizedTransitionDuration, layer, negativeInfinity, normalizedTransitionTime);
		}

		// Token: 0x060002FA RID: 762 RVA: 0x00005904 File Offset: 0x00003B04
		public void CrossFade(string stateName, float normalizedTransitionDuration)
		{
			float normalizedTransitionTime = 0f;
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.CrossFade(stateName, normalizedTransitionDuration, layer, negativeInfinity, normalizedTransitionTime);
		}

		// Token: 0x060002FB RID: 763 RVA: 0x0000592B File Offset: 0x00003B2B
		public void CrossFade(string stateName, float normalizedTransitionDuration, [UnityEngine.Internal.DefaultValue("-1")] int layer, [UnityEngine.Internal.DefaultValue("float.NegativeInfinity")] float normalizedTimeOffset, [UnityEngine.Internal.DefaultValue("0.0f")] float normalizedTransitionTime)
		{
			this.CrossFade(Animator.StringToHash(stateName), normalizedTransitionDuration, layer, normalizedTimeOffset, normalizedTransitionTime);
		}

		// Token: 0x060002FC RID: 764
		[FreeFunction(Name = "AnimatorBindings::CrossFade", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CrossFade(int stateHashName, float normalizedTransitionDuration, [UnityEngine.Internal.DefaultValue("-1")] int layer, [UnityEngine.Internal.DefaultValue("0.0f")] float normalizedTimeOffset, [UnityEngine.Internal.DefaultValue("0.0f")] float normalizedTransitionTime);

		// Token: 0x060002FD RID: 765 RVA: 0x00005940 File Offset: 0x00003B40
		public void CrossFade(int stateHashName, float normalizedTransitionDuration, int layer, float normalizedTimeOffset)
		{
			float normalizedTransitionTime = 0f;
			this.CrossFade(stateHashName, normalizedTransitionDuration, layer, normalizedTimeOffset, normalizedTransitionTime);
		}

		// Token: 0x060002FE RID: 766 RVA: 0x00005960 File Offset: 0x00003B60
		public void CrossFade(int stateHashName, float normalizedTransitionDuration, int layer)
		{
			float normalizedTransitionTime = 0f;
			float negativeInfinity = float.NegativeInfinity;
			this.CrossFade(stateHashName, normalizedTransitionDuration, layer, negativeInfinity, normalizedTransitionTime);
		}

		// Token: 0x060002FF RID: 767 RVA: 0x00005988 File Offset: 0x00003B88
		public void CrossFade(int stateHashName, float normalizedTransitionDuration)
		{
			float normalizedTransitionTime = 0f;
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.CrossFade(stateHashName, normalizedTransitionDuration, layer, negativeInfinity, normalizedTransitionTime);
		}

		// Token: 0x06000300 RID: 768 RVA: 0x000059B0 File Offset: 0x00003BB0
		public void PlayInFixedTime(string stateName, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.PlayInFixedTime(stateName, layer, negativeInfinity);
		}

		// Token: 0x06000301 RID: 769 RVA: 0x000059D0 File Offset: 0x00003BD0
		public void PlayInFixedTime(string stateName)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.PlayInFixedTime(stateName, layer, negativeInfinity);
		}

		// Token: 0x06000302 RID: 770 RVA: 0x000059EF File Offset: 0x00003BEF
		public void PlayInFixedTime(string stateName, [UnityEngine.Internal.DefaultValue("-1")] int layer, [UnityEngine.Internal.DefaultValue("float.NegativeInfinity")] float fixedTime)
		{
			this.PlayInFixedTime(Animator.StringToHash(stateName), layer, fixedTime);
		}

		// Token: 0x06000303 RID: 771
		[FreeFunction(Name = "AnimatorBindings::PlayInFixedTime", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void PlayInFixedTime(int stateNameHash, [UnityEngine.Internal.DefaultValue("-1")] int layer, [UnityEngine.Internal.DefaultValue("float.NegativeInfinity")] float fixedTime);

		// Token: 0x06000304 RID: 772 RVA: 0x00005A00 File Offset: 0x00003C00
		public void PlayInFixedTime(int stateNameHash, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.PlayInFixedTime(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x06000305 RID: 773 RVA: 0x00005A20 File Offset: 0x00003C20
		public void PlayInFixedTime(int stateNameHash)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.PlayInFixedTime(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x06000306 RID: 774 RVA: 0x00005A40 File Offset: 0x00003C40
		public void Play(string stateName, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.Play(stateName, layer, negativeInfinity);
		}

		// Token: 0x06000307 RID: 775 RVA: 0x00005A60 File Offset: 0x00003C60
		public void Play(string stateName)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.Play(stateName, layer, negativeInfinity);
		}

		// Token: 0x06000308 RID: 776 RVA: 0x00005A7F File Offset: 0x00003C7F
		public void Play(string stateName, [UnityEngine.Internal.DefaultValue("-1")] int layer, [UnityEngine.Internal.DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			this.Play(Animator.StringToHash(stateName), layer, normalizedTime);
		}

		// Token: 0x06000309 RID: 777
		[FreeFunction(Name = "AnimatorBindings::Play", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Play(int stateNameHash, [UnityEngine.Internal.DefaultValue("-1")] int layer, [UnityEngine.Internal.DefaultValue("float.NegativeInfinity")] float normalizedTime);

		// Token: 0x0600030A RID: 778 RVA: 0x00005A90 File Offset: 0x00003C90
		public void Play(int stateNameHash, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.Play(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x0600030B RID: 779 RVA: 0x00005AB0 File Offset: 0x00003CB0
		public void Play(int stateNameHash)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.Play(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x0600030C RID: 780
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTarget(AvatarTarget targetIndex, float targetNormalizedTime);

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x0600030D RID: 781 RVA: 0x00005AD0 File Offset: 0x00003CD0
		public Vector3 targetPosition
		{
			get
			{
				Vector3 result;
				this.get_targetPosition_Injected(out result);
				return result;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x0600030E RID: 782 RVA: 0x00005AE8 File Offset: 0x00003CE8
		public Quaternion targetRotation
		{
			get
			{
				Quaternion result;
				this.get_targetRotation_Injected(out result);
				return result;
			}
		}

		// Token: 0x0600030F RID: 783 RVA: 0x00005B00 File Offset: 0x00003D00
		[Obsolete("Use mask and layers to control subset of transfroms in a skeleton.", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool IsControlled(Transform transform)
		{
			return false;
		}

		// Token: 0x06000310 RID: 784
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool IsBoneTransform(Transform transform);

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000311 RID: 785
		internal extern Transform avatarRoot { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000312 RID: 786 RVA: 0x00005B18 File Offset: 0x00003D18
		public Transform GetBoneTransform(HumanBodyBones humanBoneId)
		{
			if (humanBoneId < HumanBodyBones.Hips || humanBoneId >= HumanBodyBones.LastBone)
			{
				throw new IndexOutOfRangeException("humanBoneId must be between 0 and " + HumanBodyBones.LastBone);
			}
			return this.GetBoneTransformInternal(HumanTrait.GetBoneIndexFromMono((int)humanBoneId));
		}

		// Token: 0x06000313 RID: 787
		[NativeMethod("GetBoneTransform")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Transform GetBoneTransformInternal(int humanBoneId);

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000314 RID: 788
		// (set) Token: 0x06000315 RID: 789
		public extern AnimatorCullingMode cullingMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000316 RID: 790
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StartPlayback();

		// Token: 0x06000317 RID: 791
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StopPlayback();

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000318 RID: 792
		// (set) Token: 0x06000319 RID: 793
		public extern float playbackTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600031A RID: 794
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StartRecording(int frameCount);

		// Token: 0x0600031B RID: 795
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StopRecording();

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x0600031C RID: 796 RVA: 0x00005B60 File Offset: 0x00003D60
		// (set) Token: 0x0600031D RID: 797 RVA: 0x00002340 File Offset: 0x00000540
		public float recorderStartTime
		{
			get
			{
				return this.GetRecorderStartTime();
			}
			set
			{
			}
		}

		// Token: 0x0600031E RID: 798
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetRecorderStartTime();

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x0600031F RID: 799 RVA: 0x00005B7C File Offset: 0x00003D7C
		// (set) Token: 0x06000320 RID: 800 RVA: 0x00002340 File Offset: 0x00000540
		public float recorderStopTime
		{
			get
			{
				return this.GetRecorderStopTime();
			}
			set
			{
			}
		}

		// Token: 0x06000321 RID: 801
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetRecorderStopTime();

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000322 RID: 802
		public extern AnimatorRecorderMode recorderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000323 RID: 803
		// (set) Token: 0x06000324 RID: 804
		public extern RuntimeAnimatorController runtimeAnimatorController { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000325 RID: 805
		public extern bool hasBoundPlayables { [NativeMethod("HasBoundPlayables")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000326 RID: 806
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void ClearInternalControllerPlayable();

		// Token: 0x06000327 RID: 807
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasState(int layerIndex, int stateID);

		// Token: 0x06000328 RID: 808
		[NativeMethod(Name = "ScriptingStringToCRC32", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int StringToHash(string name);

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000329 RID: 809
		// (set) Token: 0x0600032A RID: 810
		public extern Avatar avatar { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600032B RID: 811
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string GetStats();

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x0600032C RID: 812 RVA: 0x00005B98 File Offset: 0x00003D98
		public PlayableGraph playableGraph
		{
			get
			{
				PlayableGraph result = default(PlayableGraph);
				this.GetCurrentGraph(ref result);
				return result;
			}
		}

		// Token: 0x0600032D RID: 813
		[FreeFunction(Name = "AnimatorBindings::GetCurrentGraph", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetCurrentGraph(ref PlayableGraph graph);

		// Token: 0x0600032E RID: 814 RVA: 0x00005BBE File Offset: 0x00003DBE
		private void CheckIfInIKPass()
		{
			if (this.logWarnings && !this.IsInIKPass())
			{
				Debug.LogWarning("Setting and getting Body Position/Rotation, IK Goals, Lookat and BoneLocalRotation should only be done in OnAnimatorIK or OnStateIK");
			}
		}

		// Token: 0x0600032F RID: 815
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsInIKPass();

		// Token: 0x06000330 RID: 816
		[FreeFunction(Name = "AnimatorBindings::SetFloatString", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatString(string name, float value);

		// Token: 0x06000331 RID: 817
		[FreeFunction(Name = "AnimatorBindings::SetFloatID", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatID(int id, float value);

		// Token: 0x06000332 RID: 818
		[FreeFunction(Name = "AnimatorBindings::GetFloatString", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetFloatString(string name);

		// Token: 0x06000333 RID: 819
		[FreeFunction(Name = "AnimatorBindings::GetFloatID", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetFloatID(int id);

		// Token: 0x06000334 RID: 820
		[FreeFunction(Name = "AnimatorBindings::SetBoolString", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetBoolString(string name, bool value);

		// Token: 0x06000335 RID: 821
		[FreeFunction(Name = "AnimatorBindings::SetBoolID", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetBoolID(int id, bool value);

		// Token: 0x06000336 RID: 822
		[FreeFunction(Name = "AnimatorBindings::GetBoolString", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool GetBoolString(string name);

		// Token: 0x06000337 RID: 823
		[FreeFunction(Name = "AnimatorBindings::GetBoolID", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool GetBoolID(int id);

		// Token: 0x06000338 RID: 824
		[FreeFunction(Name = "AnimatorBindings::SetIntegerString", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetIntegerString(string name, int value);

		// Token: 0x06000339 RID: 825
		[FreeFunction(Name = "AnimatorBindings::SetIntegerID", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetIntegerID(int id, int value);

		// Token: 0x0600033A RID: 826
		[FreeFunction(Name = "AnimatorBindings::GetIntegerString", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetIntegerString(string name);

		// Token: 0x0600033B RID: 827
		[FreeFunction(Name = "AnimatorBindings::GetIntegerID", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetIntegerID(int id);

		// Token: 0x0600033C RID: 828
		[FreeFunction(Name = "AnimatorBindings::SetTriggerString", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetTriggerString(string name);

		// Token: 0x0600033D RID: 829
		[FreeFunction(Name = "AnimatorBindings::SetTriggerID", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetTriggerID(int id);

		// Token: 0x0600033E RID: 830
		[FreeFunction(Name = "AnimatorBindings::ResetTriggerString", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ResetTriggerString(string name);

		// Token: 0x0600033F RID: 831
		[FreeFunction(Name = "AnimatorBindings::ResetTriggerID", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ResetTriggerID(int id);

		// Token: 0x06000340 RID: 832
		[FreeFunction(Name = "AnimatorBindings::IsParameterControlledByCurveString", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsParameterControlledByCurveString(string name);

		// Token: 0x06000341 RID: 833
		[FreeFunction(Name = "AnimatorBindings::IsParameterControlledByCurveID", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsParameterControlledByCurveID(int id);

		// Token: 0x06000342 RID: 834
		[FreeFunction(Name = "AnimatorBindings::SetFloatStringDamp", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatStringDamp(string name, float value, float dampTime, float deltaTime);

		// Token: 0x06000343 RID: 835
		[FreeFunction(Name = "AnimatorBindings::SetFloatIDDamp", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatIDDamp(int id, float value, float dampTime, float deltaTime);

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x06000344 RID: 836
		// (set) Token: 0x06000345 RID: 837
		public extern bool layersAffectMassCenter { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000346 RID: 838
		public extern float leftFeetBottomHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000347 RID: 839
		public extern float rightFeetBottomHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000348 RID: 840
		[NativeConditional("UNITY_EDITOR")]
		internal extern bool supportsOnAnimatorMove { [NativeMethod("SupportsOnAnimatorMove")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000349 RID: 841
		[NativeConditional("UNITY_EDITOR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void OnUpdateModeChanged();

		// Token: 0x0600034A RID: 842
		[NativeConditional("UNITY_EDITOR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void OnCullingModeChanged();

		// Token: 0x0600034B RID: 843
		[NativeConditional("UNITY_EDITOR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void WriteDefaultPose();

		// Token: 0x0600034C RID: 844
		[NativeMethod("UpdateWithDelta")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Update(float deltaTime);

		// Token: 0x0600034D RID: 845 RVA: 0x00005BE1 File Offset: 0x00003DE1
		public void Rebind()
		{
			this.Rebind(true);
		}

		// Token: 0x0600034E RID: 846
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Rebind(bool writeDefaultValues);

		// Token: 0x0600034F RID: 847
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ApplyBuiltinRootMotion();

		// Token: 0x06000350 RID: 848 RVA: 0x00005BEB File Offset: 0x00003DEB
		[NativeConditional("UNITY_EDITOR")]
		internal void EvaluateController()
		{
			this.EvaluateController(0f);
		}

		// Token: 0x06000351 RID: 849
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void EvaluateController(float deltaTime);

		// Token: 0x06000352 RID: 850 RVA: 0x00005BFC File Offset: 0x00003DFC
		[NativeConditional("UNITY_EDITOR")]
		internal string GetCurrentStateName(int layerIndex)
		{
			return this.GetAnimatorStateName(layerIndex, true);
		}

		// Token: 0x06000353 RID: 851 RVA: 0x00005C1C File Offset: 0x00003E1C
		[NativeConditional("UNITY_EDITOR")]
		internal string GetNextStateName(int layerIndex)
		{
			return this.GetAnimatorStateName(layerIndex, false);
		}

		// Token: 0x06000354 RID: 852
		[NativeConditional("UNITY_EDITOR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string GetAnimatorStateName(int layerIndex, bool current);

		// Token: 0x06000355 RID: 853
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string ResolveHash(int hash);

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000356 RID: 854
		// (set) Token: 0x06000357 RID: 855
		public extern bool logWarnings { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000358 RID: 856
		// (set) Token: 0x06000359 RID: 857
		public extern bool fireEvents { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600035A RID: 858
		// (set) Token: 0x0600035B RID: 859
		public extern bool keepAnimatorControllerStateOnDisable { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600035C RID: 860 RVA: 0x00005C3C File Offset: 0x00003E3C
		[Obsolete("GetVector is deprecated.")]
		public Vector3 GetVector(string name)
		{
			return Vector3.zero;
		}

		// Token: 0x0600035D RID: 861 RVA: 0x00005C58 File Offset: 0x00003E58
		[Obsolete("GetVector is deprecated.")]
		public Vector3 GetVector(int id)
		{
			return Vector3.zero;
		}

		// Token: 0x0600035E RID: 862 RVA: 0x00002340 File Offset: 0x00000540
		[Obsolete("SetVector is deprecated.")]
		public void SetVector(string name, Vector3 value)
		{
		}

		// Token: 0x0600035F RID: 863 RVA: 0x00002340 File Offset: 0x00000540
		[Obsolete("SetVector is deprecated.")]
		public void SetVector(int id, Vector3 value)
		{
		}

		// Token: 0x06000360 RID: 864 RVA: 0x00005C74 File Offset: 0x00003E74
		[Obsolete("GetQuaternion is deprecated.")]
		public Quaternion GetQuaternion(string name)
		{
			return Quaternion.identity;
		}

		// Token: 0x06000361 RID: 865 RVA: 0x00005C90 File Offset: 0x00003E90
		[Obsolete("GetQuaternion is deprecated.")]
		public Quaternion GetQuaternion(int id)
		{
			return Quaternion.identity;
		}

		// Token: 0x06000362 RID: 866 RVA: 0x00002340 File Offset: 0x00000540
		[Obsolete("SetQuaternion is deprecated.")]
		public void SetQuaternion(string name, Quaternion value)
		{
		}

		// Token: 0x06000363 RID: 867 RVA: 0x00002340 File Offset: 0x00000540
		[Obsolete("SetQuaternion is deprecated.")]
		public void SetQuaternion(int id, Quaternion value)
		{
		}

		// Token: 0x06000364 RID: 868
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_deltaPosition_Injected(out Vector3 ret);

		// Token: 0x06000365 RID: 869
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_deltaRotation_Injected(out Quaternion ret);

		// Token: 0x06000366 RID: 870
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_velocity_Injected(out Vector3 ret);

		// Token: 0x06000367 RID: 871
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_angularVelocity_Injected(out Vector3 ret);

		// Token: 0x06000368 RID: 872
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rootPosition_Injected(out Vector3 ret);

		// Token: 0x06000369 RID: 873
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rootPosition_Injected(ref Vector3 value);

		// Token: 0x0600036A RID: 874
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rootRotation_Injected(out Quaternion ret);

		// Token: 0x0600036B RID: 875
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rootRotation_Injected(ref Quaternion value);

		// Token: 0x0600036C RID: 876
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bodyPositionInternal_Injected(out Vector3 ret);

		// Token: 0x0600036D RID: 877
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_bodyPositionInternal_Injected(ref Vector3 value);

		// Token: 0x0600036E RID: 878
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bodyRotationInternal_Injected(out Quaternion ret);

		// Token: 0x0600036F RID: 879
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_bodyRotationInternal_Injected(ref Quaternion value);

		// Token: 0x06000370 RID: 880
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetGoalPosition_Injected(AvatarIKGoal goal, out Vector3 ret);

		// Token: 0x06000371 RID: 881
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetGoalPosition_Injected(AvatarIKGoal goal, ref Vector3 goalPosition);

		// Token: 0x06000372 RID: 882
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetGoalRotation_Injected(AvatarIKGoal goal, out Quaternion ret);

		// Token: 0x06000373 RID: 883
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetGoalRotation_Injected(AvatarIKGoal goal, ref Quaternion goalRotation);

		// Token: 0x06000374 RID: 884
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetHintPosition_Injected(AvatarIKHint hint, out Vector3 ret);

		// Token: 0x06000375 RID: 885
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetHintPosition_Injected(AvatarIKHint hint, ref Vector3 hintPosition);

		// Token: 0x06000376 RID: 886
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetLookAtPositionInternal_Injected(ref Vector3 lookAtPosition);

		// Token: 0x06000377 RID: 887
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetBoneLocalRotationInternal_Injected(int humanBoneId, ref Quaternion rotation);

		// Token: 0x06000378 RID: 888
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_pivotPosition_Injected(out Vector3 ret);

		// Token: 0x06000379 RID: 889
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void MatchTarget_Injected(ref Vector3 matchPosition, ref Quaternion matchRotation, int targetBodyPart, ref MatchTargetWeightMask weightMask, float startNormalizedTime, float targetNormalizedTime);

		// Token: 0x0600037A RID: 890
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_targetPosition_Injected(out Vector3 ret);

		// Token: 0x0600037B RID: 891
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_targetRotation_Injected(out Quaternion ret);
	}
}
