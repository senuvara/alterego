﻿using System;

namespace UnityEngine
{
	// Token: 0x02000059 RID: 89
	public enum PlayMode
	{
		// Token: 0x04000168 RID: 360
		StopSameLayer,
		// Token: 0x04000169 RID: 361
		StopAll = 4
	}
}
