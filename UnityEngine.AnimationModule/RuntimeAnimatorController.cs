﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000056 RID: 86
	[ExcludeFromObjectFactory]
	[NativeHeader("Runtime/Animation/RuntimeAnimatorController.h")]
	[UsedByNativeCode]
	public class RuntimeAnimatorController : Object
	{
		// Token: 0x06000585 RID: 1413 RVA: 0x000069D4 File Offset: 0x00004BD4
		protected RuntimeAnimatorController()
		{
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x06000586 RID: 1414
		public extern AnimationClip[] animationClips { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
