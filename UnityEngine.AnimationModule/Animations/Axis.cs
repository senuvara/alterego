﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.Animations
{
	// Token: 0x02000048 RID: 72
	[NativeType("Runtime/Animation/Constraints/ConstraintEnums.h")]
	[Flags]
	public enum Axis
	{
		// Token: 0x0400014B RID: 331
		None = 0,
		// Token: 0x0400014C RID: 332
		X = 1,
		// Token: 0x0400014D RID: 333
		Y = 2,
		// Token: 0x0400014E RID: 334
		Z = 4
	}
}
