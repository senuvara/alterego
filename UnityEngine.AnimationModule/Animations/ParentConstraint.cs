﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Animations
{
	// Token: 0x02000055 RID: 85
	[UsedByNativeCode]
	[RequireComponent(typeof(Transform))]
	[NativeHeader("Runtime/Animation/Constraints/ParentConstraint.h")]
	[NativeHeader("Runtime/Animation/Constraints/Constraint.bindings.h")]
	public sealed class ParentConstraint : Behaviour, IConstraint, IConstraintInternal
	{
		// Token: 0x06000551 RID: 1361 RVA: 0x000078DA File Offset: 0x00005ADA
		private ParentConstraint()
		{
			ParentConstraint.Internal_Create(this);
		}

		// Token: 0x06000552 RID: 1362
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] ParentConstraint self);

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000553 RID: 1363
		// (set) Token: 0x06000554 RID: 1364
		public extern float weight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000555 RID: 1365
		// (set) Token: 0x06000556 RID: 1366
		public extern bool constraintActive { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000557 RID: 1367
		// (set) Token: 0x06000558 RID: 1368
		public extern bool locked { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000559 RID: 1369 RVA: 0x000078EC File Offset: 0x00005AEC
		public int sourceCount
		{
			get
			{
				return ParentConstraint.GetSourceCountInternal(this);
			}
		}

		// Token: 0x0600055A RID: 1370
		[FreeFunction("ConstraintBindings::GetSourceCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetSourceCountInternal([NotNull] ParentConstraint self);

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x0600055B RID: 1371 RVA: 0x00007908 File Offset: 0x00005B08
		// (set) Token: 0x0600055C RID: 1372 RVA: 0x0000791E File Offset: 0x00005B1E
		public Vector3 translationAtRest
		{
			get
			{
				Vector3 result;
				this.get_translationAtRest_Injected(out result);
				return result;
			}
			set
			{
				this.set_translationAtRest_Injected(ref value);
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x0600055D RID: 1373 RVA: 0x00007928 File Offset: 0x00005B28
		// (set) Token: 0x0600055E RID: 1374 RVA: 0x0000793E File Offset: 0x00005B3E
		public Vector3 rotationAtRest
		{
			get
			{
				Vector3 result;
				this.get_rotationAtRest_Injected(out result);
				return result;
			}
			set
			{
				this.set_rotationAtRest_Injected(ref value);
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x0600055F RID: 1375
		// (set) Token: 0x06000560 RID: 1376
		public extern Vector3[] translationOffsets { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000561 RID: 1377
		// (set) Token: 0x06000562 RID: 1378
		public extern Vector3[] rotationOffsets { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000563 RID: 1379
		// (set) Token: 0x06000564 RID: 1380
		public extern Axis translationAxis { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000565 RID: 1381
		// (set) Token: 0x06000566 RID: 1382
		public extern Axis rotationAxis { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000567 RID: 1383 RVA: 0x00007948 File Offset: 0x00005B48
		public Vector3 GetTranslationOffset(int index)
		{
			this.ValidateSourceIndex(index);
			return this.GetTranslationOffsetInternal(index);
		}

		// Token: 0x06000568 RID: 1384 RVA: 0x0000796B File Offset: 0x00005B6B
		public void SetTranslationOffset(int index, Vector3 value)
		{
			this.ValidateSourceIndex(index);
			this.SetTranslationOffsetInternal(index, value);
		}

		// Token: 0x06000569 RID: 1385 RVA: 0x00007980 File Offset: 0x00005B80
		[NativeName("GetTranslationOffset")]
		private Vector3 GetTranslationOffsetInternal(int index)
		{
			Vector3 result;
			this.GetTranslationOffsetInternal_Injected(index, out result);
			return result;
		}

		// Token: 0x0600056A RID: 1386 RVA: 0x00007997 File Offset: 0x00005B97
		[NativeName("SetTranslationOffset")]
		private void SetTranslationOffsetInternal(int index, Vector3 value)
		{
			this.SetTranslationOffsetInternal_Injected(index, ref value);
		}

		// Token: 0x0600056B RID: 1387 RVA: 0x000079A4 File Offset: 0x00005BA4
		public Vector3 GetRotationOffset(int index)
		{
			this.ValidateSourceIndex(index);
			return this.GetRotationOffsetInternal(index);
		}

		// Token: 0x0600056C RID: 1388 RVA: 0x000079C7 File Offset: 0x00005BC7
		public void SetRotationOffset(int index, Vector3 value)
		{
			this.ValidateSourceIndex(index);
			this.SetRotationOffsetInternal(index, value);
		}

		// Token: 0x0600056D RID: 1389 RVA: 0x000079DC File Offset: 0x00005BDC
		[NativeName("GetRotationOffset")]
		private Vector3 GetRotationOffsetInternal(int index)
		{
			Vector3 result;
			this.GetRotationOffsetInternal_Injected(index, out result);
			return result;
		}

		// Token: 0x0600056E RID: 1390 RVA: 0x000079F3 File Offset: 0x00005BF3
		[NativeName("SetRotationOffset")]
		private void SetRotationOffsetInternal(int index, Vector3 value)
		{
			this.SetRotationOffsetInternal_Injected(index, ref value);
		}

		// Token: 0x0600056F RID: 1391 RVA: 0x00007A00 File Offset: 0x00005C00
		private void ValidateSourceIndex(int index)
		{
			if (this.sourceCount == 0)
			{
				throw new InvalidOperationException("The ParentConstraint component has no sources.");
			}
			if (index < 0 || index >= this.sourceCount)
			{
				throw new ArgumentOutOfRangeException("index", string.Format("Constraint source index {0} is out of bounds (0-{1}).", index, this.sourceCount));
			}
		}

		// Token: 0x06000570 RID: 1392
		[FreeFunction(Name = "ConstraintBindings::GetSources", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GetSources([NotNull] List<ConstraintSource> sources);

		// Token: 0x06000571 RID: 1393 RVA: 0x00007A5F File Offset: 0x00005C5F
		public void SetSources(List<ConstraintSource> sources)
		{
			if (sources == null)
			{
				throw new ArgumentNullException("sources");
			}
			ParentConstraint.SetSourcesInternal(this, sources);
		}

		// Token: 0x06000572 RID: 1394
		[FreeFunction("ConstraintBindings::SetSources")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetSourcesInternal([NotNull] ParentConstraint self, List<ConstraintSource> sources);

		// Token: 0x06000573 RID: 1395 RVA: 0x00007A7A File Offset: 0x00005C7A
		public int AddSource(ConstraintSource source)
		{
			return this.AddSource_Injected(ref source);
		}

		// Token: 0x06000574 RID: 1396 RVA: 0x00007A84 File Offset: 0x00005C84
		public void RemoveSource(int index)
		{
			this.ValidateSourceIndex(index);
			this.RemoveSourceInternal(index);
		}

		// Token: 0x06000575 RID: 1397
		[NativeName("RemoveSource")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RemoveSourceInternal(int index);

		// Token: 0x06000576 RID: 1398 RVA: 0x00007A98 File Offset: 0x00005C98
		public ConstraintSource GetSource(int index)
		{
			this.ValidateSourceIndex(index);
			return this.GetSourceInternal(index);
		}

		// Token: 0x06000577 RID: 1399 RVA: 0x00007ABC File Offset: 0x00005CBC
		[NativeName("GetSource")]
		private ConstraintSource GetSourceInternal(int index)
		{
			ConstraintSource result;
			this.GetSourceInternal_Injected(index, out result);
			return result;
		}

		// Token: 0x06000578 RID: 1400 RVA: 0x00007AD3 File Offset: 0x00005CD3
		public void SetSource(int index, ConstraintSource source)
		{
			this.ValidateSourceIndex(index);
			this.SetSourceInternal(index, source);
		}

		// Token: 0x06000579 RID: 1401 RVA: 0x00007AE5 File Offset: 0x00005CE5
		[NativeName("SetSource")]
		private void SetSourceInternal(int index, ConstraintSource source)
		{
			this.SetSourceInternal_Injected(index, ref source);
		}

		// Token: 0x0600057A RID: 1402
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_translationAtRest_Injected(out Vector3 ret);

		// Token: 0x0600057B RID: 1403
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_translationAtRest_Injected(ref Vector3 value);

		// Token: 0x0600057C RID: 1404
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rotationAtRest_Injected(out Vector3 ret);

		// Token: 0x0600057D RID: 1405
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rotationAtRest_Injected(ref Vector3 value);

		// Token: 0x0600057E RID: 1406
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetTranslationOffsetInternal_Injected(int index, out Vector3 ret);

		// Token: 0x0600057F RID: 1407
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetTranslationOffsetInternal_Injected(int index, ref Vector3 value);

		// Token: 0x06000580 RID: 1408
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetRotationOffsetInternal_Injected(int index, out Vector3 ret);

		// Token: 0x06000581 RID: 1409
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetRotationOffsetInternal_Injected(int index, ref Vector3 value);

		// Token: 0x06000582 RID: 1410
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int AddSource_Injected(ref ConstraintSource source);

		// Token: 0x06000583 RID: 1411
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetSourceInternal_Injected(int index, out ConstraintSource ret);

		// Token: 0x06000584 RID: 1412
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetSourceInternal_Injected(int index, ref ConstraintSource source);
	}
}
