﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Playables;
using UnityEngine.Scripting;

namespace UnityEngine.Animations
{
	// Token: 0x02000011 RID: 17
	[NativeHeader("Runtime/Director/Core/HPlayable.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimationMixerPlayable.bindings.h")]
	[NativeHeader("Runtime/Animation/Director/AnimationMixerPlayable.h")]
	[StaticAccessor("AnimationMixerPlayableBindings", StaticAccessorType.DoubleColon)]
	[RequiredByNativeCode]
	public struct AnimationMixerPlayable : IPlayable, IEquatable<AnimationMixerPlayable>
	{
		// Token: 0x06000117 RID: 279 RVA: 0x00003061 File Offset: 0x00001261
		internal AnimationMixerPlayable(PlayableHandle handle)
		{
			if (handle.IsValid())
			{
				if (!handle.IsPlayableOfType<AnimationMixerPlayable>())
				{
					throw new InvalidCastException("Can't set handle: the playable is not an AnimationMixerPlayable.");
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000118 RID: 280 RVA: 0x00003090 File Offset: 0x00001290
		public static AnimationMixerPlayable Null
		{
			get
			{
				return AnimationMixerPlayable.m_NullPlayable;
			}
		}

		// Token: 0x06000119 RID: 281 RVA: 0x000030AC File Offset: 0x000012AC
		public static AnimationMixerPlayable Create(PlayableGraph graph, int inputCount = 0, bool normalizeWeights = false)
		{
			PlayableHandle handle = AnimationMixerPlayable.CreateHandle(graph, inputCount, normalizeWeights);
			return new AnimationMixerPlayable(handle);
		}

		// Token: 0x0600011A RID: 282 RVA: 0x000030D0 File Offset: 0x000012D0
		private static PlayableHandle CreateHandle(PlayableGraph graph, int inputCount = 0, bool normalizeWeights = false)
		{
			PlayableHandle @null = PlayableHandle.Null;
			PlayableHandle result;
			if (!AnimationMixerPlayable.CreateHandleInternal(graph, inputCount, normalizeWeights, ref @null))
			{
				result = PlayableHandle.Null;
			}
			else
			{
				@null.SetInputCount(inputCount);
				result = @null;
			}
			return result;
		}

		// Token: 0x0600011B RID: 283 RVA: 0x00003110 File Offset: 0x00001310
		public PlayableHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x0600011C RID: 284 RVA: 0x0000312C File Offset: 0x0000132C
		public static implicit operator Playable(AnimationMixerPlayable playable)
		{
			return new Playable(playable.GetHandle());
		}

		// Token: 0x0600011D RID: 285 RVA: 0x00003150 File Offset: 0x00001350
		public static explicit operator AnimationMixerPlayable(Playable playable)
		{
			return new AnimationMixerPlayable(playable.GetHandle());
		}

		// Token: 0x0600011E RID: 286 RVA: 0x00003174 File Offset: 0x00001374
		public bool Equals(AnimationMixerPlayable other)
		{
			return this.GetHandle() == other.GetHandle();
		}

		// Token: 0x0600011F RID: 287 RVA: 0x0000319B File Offset: 0x0000139B
		[NativeThrows]
		private static bool CreateHandleInternal(PlayableGraph graph, int inputCount, bool normalizeWeights, ref PlayableHandle handle)
		{
			return AnimationMixerPlayable.CreateHandleInternal_Injected(ref graph, inputCount, normalizeWeights, ref handle);
		}

		// Token: 0x06000120 RID: 288 RVA: 0x000031A7 File Offset: 0x000013A7
		// Note: this type is marked as 'beforefieldinit'.
		static AnimationMixerPlayable()
		{
		}

		// Token: 0x06000121 RID: 289
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CreateHandleInternal_Injected(ref PlayableGraph graph, int inputCount, bool normalizeWeights, ref PlayableHandle handle);

		// Token: 0x0400000F RID: 15
		private PlayableHandle m_Handle;

		// Token: 0x04000010 RID: 16
		private static readonly AnimationMixerPlayable m_NullPlayable = new AnimationMixerPlayable(PlayableHandle.Null);
	}
}
