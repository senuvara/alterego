﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Playables;
using UnityEngine.Scripting;

namespace UnityEngine.Animations
{
	// Token: 0x02000018 RID: 24
	[StaticAccessor("AnimationRemoveScalePlayableBindings", StaticAccessorType.DoubleColon)]
	[NativeHeader("Runtime/Director/Core/HPlayable.h")]
	[NativeHeader("Runtime/Animation/Director/AnimationRemoveScalePlayable.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimationRemoveScalePlayable.bindings.h")]
	[RequiredByNativeCode]
	internal struct AnimationRemoveScalePlayable : IPlayable, IEquatable<AnimationRemoveScalePlayable>
	{
		// Token: 0x06000172 RID: 370 RVA: 0x0000388A File Offset: 0x00001A8A
		internal AnimationRemoveScalePlayable(PlayableHandle handle)
		{
			if (handle.IsValid())
			{
				if (!handle.IsPlayableOfType<AnimationRemoveScalePlayable>())
				{
					throw new InvalidCastException("Can't set handle: the playable is not an AnimationRemoveScalePlayable.");
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000173 RID: 371 RVA: 0x000038BC File Offset: 0x00001ABC
		public static AnimationRemoveScalePlayable Null
		{
			get
			{
				return AnimationRemoveScalePlayable.m_NullPlayable;
			}
		}

		// Token: 0x06000174 RID: 372 RVA: 0x000038D8 File Offset: 0x00001AD8
		public static AnimationRemoveScalePlayable Create(PlayableGraph graph, int inputCount)
		{
			PlayableHandle handle = AnimationRemoveScalePlayable.CreateHandle(graph, inputCount);
			return new AnimationRemoveScalePlayable(handle);
		}

		// Token: 0x06000175 RID: 373 RVA: 0x000038FC File Offset: 0x00001AFC
		private static PlayableHandle CreateHandle(PlayableGraph graph, int inputCount)
		{
			PlayableHandle @null = PlayableHandle.Null;
			PlayableHandle result;
			if (!AnimationRemoveScalePlayable.CreateHandleInternal(graph, ref @null))
			{
				result = PlayableHandle.Null;
			}
			else
			{
				@null.SetInputCount(inputCount);
				result = @null;
			}
			return result;
		}

		// Token: 0x06000176 RID: 374 RVA: 0x00003938 File Offset: 0x00001B38
		public PlayableHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x06000177 RID: 375 RVA: 0x00003954 File Offset: 0x00001B54
		public static implicit operator Playable(AnimationRemoveScalePlayable playable)
		{
			return new Playable(playable.GetHandle());
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00003978 File Offset: 0x00001B78
		public static explicit operator AnimationRemoveScalePlayable(Playable playable)
		{
			return new AnimationRemoveScalePlayable(playable.GetHandle());
		}

		// Token: 0x06000179 RID: 377 RVA: 0x0000399C File Offset: 0x00001B9C
		public bool Equals(AnimationRemoveScalePlayable other)
		{
			return this.Equals(other.GetHandle());
		}

		// Token: 0x0600017A RID: 378 RVA: 0x000039C9 File Offset: 0x00001BC9
		[NativeThrows]
		private static bool CreateHandleInternal(PlayableGraph graph, ref PlayableHandle handle)
		{
			return AnimationRemoveScalePlayable.CreateHandleInternal_Injected(ref graph, ref handle);
		}

		// Token: 0x0600017B RID: 379 RVA: 0x000039D3 File Offset: 0x00001BD3
		// Note: this type is marked as 'beforefieldinit'.
		static AnimationRemoveScalePlayable()
		{
		}

		// Token: 0x0600017C RID: 380
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CreateHandleInternal_Injected(ref PlayableGraph graph, ref PlayableHandle handle);

		// Token: 0x04000018 RID: 24
		private PlayableHandle m_Handle;

		// Token: 0x04000019 RID: 25
		private static readonly AnimationRemoveScalePlayable m_NullPlayable = new AnimationRemoveScalePlayable(PlayableHandle.Null);
	}
}
