﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Animations
{
	// Token: 0x0200000B RID: 11
	[UsedByNativeCode]
	[NativeHeader("Runtime/Animation/Constraints/Constraint.bindings.h")]
	[NativeHeader("Runtime/Animation/Constraints/AimConstraint.h")]
	[RequireComponent(typeof(Transform))]
	public sealed class AimConstraint : Behaviour, IConstraint, IConstraintInternal
	{
		// Token: 0x06000024 RID: 36 RVA: 0x00002343 File Offset: 0x00000543
		private AimConstraint()
		{
			AimConstraint.Internal_Create(this);
		}

		// Token: 0x06000025 RID: 37
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] AimConstraint self);

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000026 RID: 38
		// (set) Token: 0x06000027 RID: 39
		public extern float weight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000028 RID: 40
		// (set) Token: 0x06000029 RID: 41
		public extern bool constraintActive { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600002A RID: 42
		// (set) Token: 0x0600002B RID: 43
		public extern bool locked { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600002C RID: 44 RVA: 0x00002354 File Offset: 0x00000554
		// (set) Token: 0x0600002D RID: 45 RVA: 0x0000236A File Offset: 0x0000056A
		public Vector3 rotationAtRest
		{
			get
			{
				Vector3 result;
				this.get_rotationAtRest_Injected(out result);
				return result;
			}
			set
			{
				this.set_rotationAtRest_Injected(ref value);
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600002E RID: 46 RVA: 0x00002374 File Offset: 0x00000574
		// (set) Token: 0x0600002F RID: 47 RVA: 0x0000238A File Offset: 0x0000058A
		public Vector3 rotationOffset
		{
			get
			{
				Vector3 result;
				this.get_rotationOffset_Injected(out result);
				return result;
			}
			set
			{
				this.set_rotationOffset_Injected(ref value);
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000030 RID: 48
		// (set) Token: 0x06000031 RID: 49
		public extern Axis rotationAxis { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000032 RID: 50 RVA: 0x00002394 File Offset: 0x00000594
		// (set) Token: 0x06000033 RID: 51 RVA: 0x000023AA File Offset: 0x000005AA
		public Vector3 aimVector
		{
			get
			{
				Vector3 result;
				this.get_aimVector_Injected(out result);
				return result;
			}
			set
			{
				this.set_aimVector_Injected(ref value);
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000034 RID: 52 RVA: 0x000023B4 File Offset: 0x000005B4
		// (set) Token: 0x06000035 RID: 53 RVA: 0x000023CA File Offset: 0x000005CA
		public Vector3 upVector
		{
			get
			{
				Vector3 result;
				this.get_upVector_Injected(out result);
				return result;
			}
			set
			{
				this.set_upVector_Injected(ref value);
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000036 RID: 54 RVA: 0x000023D4 File Offset: 0x000005D4
		// (set) Token: 0x06000037 RID: 55 RVA: 0x000023EA File Offset: 0x000005EA
		public Vector3 worldUpVector
		{
			get
			{
				Vector3 result;
				this.get_worldUpVector_Injected(out result);
				return result;
			}
			set
			{
				this.set_worldUpVector_Injected(ref value);
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000038 RID: 56
		// (set) Token: 0x06000039 RID: 57
		public extern Transform worldUpObject { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600003A RID: 58
		// (set) Token: 0x0600003B RID: 59
		public extern AimConstraint.WorldUpType worldUpType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600003C RID: 60 RVA: 0x000023F4 File Offset: 0x000005F4
		public int sourceCount
		{
			get
			{
				return AimConstraint.GetSourceCountInternal(this);
			}
		}

		// Token: 0x0600003D RID: 61
		[FreeFunction("ConstraintBindings::GetSourceCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetSourceCountInternal([NotNull] AimConstraint self);

		// Token: 0x0600003E RID: 62
		[FreeFunction(Name = "ConstraintBindings::GetSources", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GetSources([NotNull] List<ConstraintSource> sources);

		// Token: 0x0600003F RID: 63 RVA: 0x0000240F File Offset: 0x0000060F
		public void SetSources(List<ConstraintSource> sources)
		{
			if (sources == null)
			{
				throw new ArgumentNullException("sources");
			}
			AimConstraint.SetSourcesInternal(this, sources);
		}

		// Token: 0x06000040 RID: 64
		[FreeFunction("ConstraintBindings::SetSources")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetSourcesInternal([NotNull] AimConstraint self, List<ConstraintSource> sources);

		// Token: 0x06000041 RID: 65 RVA: 0x0000242A File Offset: 0x0000062A
		public int AddSource(ConstraintSource source)
		{
			return this.AddSource_Injected(ref source);
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00002434 File Offset: 0x00000634
		public void RemoveSource(int index)
		{
			this.ValidateSourceIndex(index);
			this.RemoveSourceInternal(index);
		}

		// Token: 0x06000043 RID: 67
		[NativeName("RemoveSource")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RemoveSourceInternal(int index);

		// Token: 0x06000044 RID: 68 RVA: 0x00002448 File Offset: 0x00000648
		public ConstraintSource GetSource(int index)
		{
			this.ValidateSourceIndex(index);
			return this.GetSourceInternal(index);
		}

		// Token: 0x06000045 RID: 69 RVA: 0x0000246C File Offset: 0x0000066C
		[NativeName("GetSource")]
		private ConstraintSource GetSourceInternal(int index)
		{
			ConstraintSource result;
			this.GetSourceInternal_Injected(index, out result);
			return result;
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00002483 File Offset: 0x00000683
		public void SetSource(int index, ConstraintSource source)
		{
			this.ValidateSourceIndex(index);
			this.SetSourceInternal(index, source);
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00002495 File Offset: 0x00000695
		[NativeName("SetSource")]
		private void SetSourceInternal(int index, ConstraintSource source)
		{
			this.SetSourceInternal_Injected(index, ref source);
		}

		// Token: 0x06000048 RID: 72 RVA: 0x000024A0 File Offset: 0x000006A0
		private void ValidateSourceIndex(int index)
		{
			if (this.sourceCount == 0)
			{
				throw new InvalidOperationException("The AimConstraint component has no sources.");
			}
			if (index < 0 || index >= this.sourceCount)
			{
				throw new ArgumentOutOfRangeException("index", string.Format("Constraint source index {0} is out of bounds (0-{1}).", index, this.sourceCount));
			}
		}

		// Token: 0x06000049 RID: 73
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rotationAtRest_Injected(out Vector3 ret);

		// Token: 0x0600004A RID: 74
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rotationAtRest_Injected(ref Vector3 value);

		// Token: 0x0600004B RID: 75
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rotationOffset_Injected(out Vector3 ret);

		// Token: 0x0600004C RID: 76
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rotationOffset_Injected(ref Vector3 value);

		// Token: 0x0600004D RID: 77
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_aimVector_Injected(out Vector3 ret);

		// Token: 0x0600004E RID: 78
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_aimVector_Injected(ref Vector3 value);

		// Token: 0x0600004F RID: 79
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_upVector_Injected(out Vector3 ret);

		// Token: 0x06000050 RID: 80
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_upVector_Injected(ref Vector3 value);

		// Token: 0x06000051 RID: 81
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_worldUpVector_Injected(out Vector3 ret);

		// Token: 0x06000052 RID: 82
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_worldUpVector_Injected(ref Vector3 value);

		// Token: 0x06000053 RID: 83
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int AddSource_Injected(ref ConstraintSource source);

		// Token: 0x06000054 RID: 84
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetSourceInternal_Injected(int index, out ConstraintSource ret);

		// Token: 0x06000055 RID: 85
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetSourceInternal_Injected(int index, ref ConstraintSource source);

		// Token: 0x0200000C RID: 12
		public enum WorldUpType
		{
			// Token: 0x04000006 RID: 6
			SceneUp,
			// Token: 0x04000007 RID: 7
			ObjectUp,
			// Token: 0x04000008 RID: 8
			ObjectRotationUp,
			// Token: 0x04000009 RID: 9
			Vector,
			// Token: 0x0400000A RID: 10
			None
		}
	}
}
