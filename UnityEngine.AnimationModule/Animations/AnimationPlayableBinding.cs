﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Playables;

namespace UnityEngine.Animations
{
	// Token: 0x02000002 RID: 2
	public static class AnimationPlayableBinding
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public static PlayableBinding Create(string name, Object key)
		{
			Type typeFromHandle = typeof(Animator);
			if (AnimationPlayableBinding.<>f__mg$cache0 == null)
			{
				AnimationPlayableBinding.<>f__mg$cache0 = new PlayableBinding.CreateOutputMethod(AnimationPlayableBinding.CreateAnimationOutput);
			}
			return PlayableBinding.CreateInternal(name, key, typeFromHandle, AnimationPlayableBinding.<>f__mg$cache0);
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002094 File Offset: 0x00000294
		private static PlayableOutput CreateAnimationOutput(PlayableGraph graph, string name)
		{
			return AnimationPlayableOutput.Create(graph, name, null);
		}

		// Token: 0x04000001 RID: 1
		[CompilerGenerated]
		private static PlayableBinding.CreateOutputMethod <>f__mg$cache0;
	}
}
