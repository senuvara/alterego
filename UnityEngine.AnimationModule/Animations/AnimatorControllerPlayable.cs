﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Playables;
using UnityEngine.Scripting;

namespace UnityEngine.Animations
{
	// Token: 0x02000031 RID: 49
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimatorControllerPlayable.bindings.h")]
	[StaticAccessor("AnimatorControllerPlayableBindings", StaticAccessorType.DoubleColon)]
	[NativeHeader("Runtime/Animation/ScriptBindings/Animator.bindings.h")]
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Animation/Director/AnimatorControllerPlayable.h")]
	[NativeHeader("Runtime/Animation/AnimatorInfo.h")]
	[NativeHeader("Runtime/Animation/RuntimeAnimatorController.h")]
	public struct AnimatorControllerPlayable : IPlayable, IEquatable<AnimatorControllerPlayable>
	{
		// Token: 0x06000389 RID: 905 RVA: 0x00005E38 File Offset: 0x00004038
		internal AnimatorControllerPlayable(PlayableHandle handle)
		{
			this.m_Handle = PlayableHandle.Null;
			this.SetHandle(handle);
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x0600038A RID: 906 RVA: 0x00005E50 File Offset: 0x00004050
		public static AnimatorControllerPlayable Null
		{
			get
			{
				return AnimatorControllerPlayable.m_NullPlayable;
			}
		}

		// Token: 0x0600038B RID: 907 RVA: 0x00005E6C File Offset: 0x0000406C
		public static AnimatorControllerPlayable Create(PlayableGraph graph, RuntimeAnimatorController controller)
		{
			PlayableHandle handle = AnimatorControllerPlayable.CreateHandle(graph, controller);
			return new AnimatorControllerPlayable(handle);
		}

		// Token: 0x0600038C RID: 908 RVA: 0x00005E90 File Offset: 0x00004090
		private static PlayableHandle CreateHandle(PlayableGraph graph, RuntimeAnimatorController controller)
		{
			PlayableHandle @null = PlayableHandle.Null;
			PlayableHandle result;
			if (!AnimatorControllerPlayable.CreateHandleInternal(graph, controller, ref @null))
			{
				result = PlayableHandle.Null;
			}
			else
			{
				result = @null;
			}
			return result;
		}

		// Token: 0x0600038D RID: 909 RVA: 0x00005EC8 File Offset: 0x000040C8
		public PlayableHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x0600038E RID: 910 RVA: 0x00005EE4 File Offset: 0x000040E4
		public void SetHandle(PlayableHandle handle)
		{
			if (this.m_Handle.IsValid())
			{
				throw new InvalidOperationException("Cannot call IPlayable.SetHandle on an instance that already contains a valid handle.");
			}
			if (handle.IsValid())
			{
				if (!handle.IsPlayableOfType<AnimatorControllerPlayable>())
				{
					throw new InvalidCastException("Can't set handle: the playable is not an AnimatorControllerPlayable.");
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x0600038F RID: 911 RVA: 0x00005F3C File Offset: 0x0000413C
		public static implicit operator Playable(AnimatorControllerPlayable playable)
		{
			return new Playable(playable.GetHandle());
		}

		// Token: 0x06000390 RID: 912 RVA: 0x00005F60 File Offset: 0x00004160
		public static explicit operator AnimatorControllerPlayable(Playable playable)
		{
			return new AnimatorControllerPlayable(playable.GetHandle());
		}

		// Token: 0x06000391 RID: 913 RVA: 0x00005F84 File Offset: 0x00004184
		public bool Equals(AnimatorControllerPlayable other)
		{
			return this.GetHandle() == other.GetHandle();
		}

		// Token: 0x06000392 RID: 914 RVA: 0x00005FAC File Offset: 0x000041AC
		public float GetFloat(string name)
		{
			return AnimatorControllerPlayable.GetFloatString(ref this.m_Handle, name);
		}

		// Token: 0x06000393 RID: 915 RVA: 0x00005FD0 File Offset: 0x000041D0
		public float GetFloat(int id)
		{
			return AnimatorControllerPlayable.GetFloatID(ref this.m_Handle, id);
		}

		// Token: 0x06000394 RID: 916 RVA: 0x00005FF1 File Offset: 0x000041F1
		public void SetFloat(string name, float value)
		{
			AnimatorControllerPlayable.SetFloatString(ref this.m_Handle, name, value);
		}

		// Token: 0x06000395 RID: 917 RVA: 0x00006001 File Offset: 0x00004201
		public void SetFloat(int id, float value)
		{
			AnimatorControllerPlayable.SetFloatID(ref this.m_Handle, id, value);
		}

		// Token: 0x06000396 RID: 918 RVA: 0x00006014 File Offset: 0x00004214
		public bool GetBool(string name)
		{
			return AnimatorControllerPlayable.GetBoolString(ref this.m_Handle, name);
		}

		// Token: 0x06000397 RID: 919 RVA: 0x00006038 File Offset: 0x00004238
		public bool GetBool(int id)
		{
			return AnimatorControllerPlayable.GetBoolID(ref this.m_Handle, id);
		}

		// Token: 0x06000398 RID: 920 RVA: 0x00006059 File Offset: 0x00004259
		public void SetBool(string name, bool value)
		{
			AnimatorControllerPlayable.SetBoolString(ref this.m_Handle, name, value);
		}

		// Token: 0x06000399 RID: 921 RVA: 0x00006069 File Offset: 0x00004269
		public void SetBool(int id, bool value)
		{
			AnimatorControllerPlayable.SetBoolID(ref this.m_Handle, id, value);
		}

		// Token: 0x0600039A RID: 922 RVA: 0x0000607C File Offset: 0x0000427C
		public int GetInteger(string name)
		{
			return AnimatorControllerPlayable.GetIntegerString(ref this.m_Handle, name);
		}

		// Token: 0x0600039B RID: 923 RVA: 0x000060A0 File Offset: 0x000042A0
		public int GetInteger(int id)
		{
			return AnimatorControllerPlayable.GetIntegerID(ref this.m_Handle, id);
		}

		// Token: 0x0600039C RID: 924 RVA: 0x000060C1 File Offset: 0x000042C1
		public void SetInteger(string name, int value)
		{
			AnimatorControllerPlayable.SetIntegerString(ref this.m_Handle, name, value);
		}

		// Token: 0x0600039D RID: 925 RVA: 0x000060D1 File Offset: 0x000042D1
		public void SetInteger(int id, int value)
		{
			AnimatorControllerPlayable.SetIntegerID(ref this.m_Handle, id, value);
		}

		// Token: 0x0600039E RID: 926 RVA: 0x000060E1 File Offset: 0x000042E1
		public void SetTrigger(string name)
		{
			AnimatorControllerPlayable.SetTriggerString(ref this.m_Handle, name);
		}

		// Token: 0x0600039F RID: 927 RVA: 0x000060F0 File Offset: 0x000042F0
		public void SetTrigger(int id)
		{
			AnimatorControllerPlayable.SetTriggerID(ref this.m_Handle, id);
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x000060FF File Offset: 0x000042FF
		public void ResetTrigger(string name)
		{
			AnimatorControllerPlayable.ResetTriggerString(ref this.m_Handle, name);
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x0000610E File Offset: 0x0000430E
		public void ResetTrigger(int id)
		{
			AnimatorControllerPlayable.ResetTriggerID(ref this.m_Handle, id);
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x00006120 File Offset: 0x00004320
		public bool IsParameterControlledByCurve(string name)
		{
			return AnimatorControllerPlayable.IsParameterControlledByCurveString(ref this.m_Handle, name);
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x00006144 File Offset: 0x00004344
		public bool IsParameterControlledByCurve(int id)
		{
			return AnimatorControllerPlayable.IsParameterControlledByCurveID(ref this.m_Handle, id);
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x00006168 File Offset: 0x00004368
		public int GetLayerCount()
		{
			return AnimatorControllerPlayable.GetLayerCountInternal(ref this.m_Handle);
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x00006188 File Offset: 0x00004388
		public string GetLayerName(int layerIndex)
		{
			return AnimatorControllerPlayable.GetLayerNameInternal(ref this.m_Handle, layerIndex);
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x000061AC File Offset: 0x000043AC
		public int GetLayerIndex(string layerName)
		{
			return AnimatorControllerPlayable.GetLayerIndexInternal(ref this.m_Handle, layerName);
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x000061D0 File Offset: 0x000043D0
		public float GetLayerWeight(int layerIndex)
		{
			return AnimatorControllerPlayable.GetLayerWeightInternal(ref this.m_Handle, layerIndex);
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x000061F1 File Offset: 0x000043F1
		public void SetLayerWeight(int layerIndex, float weight)
		{
			AnimatorControllerPlayable.SetLayerWeightInternal(ref this.m_Handle, layerIndex, weight);
		}

		// Token: 0x060003A9 RID: 937 RVA: 0x00006204 File Offset: 0x00004404
		public AnimatorStateInfo GetCurrentAnimatorStateInfo(int layerIndex)
		{
			return AnimatorControllerPlayable.GetCurrentAnimatorStateInfoInternal(ref this.m_Handle, layerIndex);
		}

		// Token: 0x060003AA RID: 938 RVA: 0x00006228 File Offset: 0x00004428
		public AnimatorStateInfo GetNextAnimatorStateInfo(int layerIndex)
		{
			return AnimatorControllerPlayable.GetNextAnimatorStateInfoInternal(ref this.m_Handle, layerIndex);
		}

		// Token: 0x060003AB RID: 939 RVA: 0x0000624C File Offset: 0x0000444C
		public AnimatorTransitionInfo GetAnimatorTransitionInfo(int layerIndex)
		{
			return AnimatorControllerPlayable.GetAnimatorTransitionInfoInternal(ref this.m_Handle, layerIndex);
		}

		// Token: 0x060003AC RID: 940 RVA: 0x00006270 File Offset: 0x00004470
		public AnimatorClipInfo[] GetCurrentAnimatorClipInfo(int layerIndex)
		{
			return AnimatorControllerPlayable.GetCurrentAnimatorClipInfoInternal(ref this.m_Handle, layerIndex);
		}

		// Token: 0x060003AD RID: 941 RVA: 0x00006291 File Offset: 0x00004491
		public void GetCurrentAnimatorClipInfo(int layerIndex, List<AnimatorClipInfo> clips)
		{
			if (clips == null)
			{
				throw new ArgumentNullException("clips");
			}
			AnimatorControllerPlayable.GetAnimatorClipInfoInternal(ref this.m_Handle, layerIndex, true, clips);
		}

		// Token: 0x060003AE RID: 942 RVA: 0x000062B3 File Offset: 0x000044B3
		public void GetNextAnimatorClipInfo(int layerIndex, List<AnimatorClipInfo> clips)
		{
			if (clips == null)
			{
				throw new ArgumentNullException("clips");
			}
			AnimatorControllerPlayable.GetAnimatorClipInfoInternal(ref this.m_Handle, layerIndex, false, clips);
		}

		// Token: 0x060003AF RID: 943
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetAnimatorClipInfoInternal(ref PlayableHandle handle, int layerIndex, bool isCurrent, object clips);

		// Token: 0x060003B0 RID: 944 RVA: 0x000062D8 File Offset: 0x000044D8
		public int GetCurrentAnimatorClipInfoCount(int layerIndex)
		{
			return AnimatorControllerPlayable.GetAnimatorClipInfoCountInternal(ref this.m_Handle, layerIndex, true);
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x000062FC File Offset: 0x000044FC
		public int GetNextAnimatorClipInfoCount(int layerIndex)
		{
			return AnimatorControllerPlayable.GetAnimatorClipInfoCountInternal(ref this.m_Handle, layerIndex, false);
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x00006320 File Offset: 0x00004520
		public AnimatorClipInfo[] GetNextAnimatorClipInfo(int layerIndex)
		{
			return AnimatorControllerPlayable.GetNextAnimatorClipInfoInternal(ref this.m_Handle, layerIndex);
		}

		// Token: 0x060003B3 RID: 947 RVA: 0x00006344 File Offset: 0x00004544
		public bool IsInTransition(int layerIndex)
		{
			return AnimatorControllerPlayable.IsInTransitionInternal(ref this.m_Handle, layerIndex);
		}

		// Token: 0x060003B4 RID: 948 RVA: 0x00006368 File Offset: 0x00004568
		public int GetParameterCount()
		{
			return AnimatorControllerPlayable.GetParameterCountInternal(ref this.m_Handle);
		}

		// Token: 0x060003B5 RID: 949 RVA: 0x00006388 File Offset: 0x00004588
		public AnimatorControllerParameter GetParameter(int index)
		{
			AnimatorControllerParameter[] parametersArrayInternal = AnimatorControllerPlayable.GetParametersArrayInternal(ref this.m_Handle);
			if (index < 0 || index >= parametersArrayInternal.Length)
			{
				throw new IndexOutOfRangeException("Invalid parameter index.");
			}
			return parametersArrayInternal[index];
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x000063C7 File Offset: 0x000045C7
		public void CrossFadeInFixedTime(string stateName, float transitionDuration)
		{
			AnimatorControllerPlayable.CrossFadeInFixedTimeInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), transitionDuration, -1, 0f);
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x000063E2 File Offset: 0x000045E2
		public void CrossFadeInFixedTime(string stateName, float transitionDuration, int layer)
		{
			AnimatorControllerPlayable.CrossFadeInFixedTimeInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), transitionDuration, layer, 0f);
		}

		// Token: 0x060003B8 RID: 952 RVA: 0x000063FD File Offset: 0x000045FD
		public void CrossFadeInFixedTime(string stateName, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("0.0f")] float fixedTime)
		{
			AnimatorControllerPlayable.CrossFadeInFixedTimeInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), transitionDuration, layer, fixedTime);
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x00006415 File Offset: 0x00004615
		public void CrossFadeInFixedTime(int stateNameHash, float transitionDuration)
		{
			AnimatorControllerPlayable.CrossFadeInFixedTimeInternal(ref this.m_Handle, stateNameHash, transitionDuration, -1, 0f);
		}

		// Token: 0x060003BA RID: 954 RVA: 0x0000642B File Offset: 0x0000462B
		public void CrossFadeInFixedTime(int stateNameHash, float transitionDuration, int layer)
		{
			AnimatorControllerPlayable.CrossFadeInFixedTimeInternal(ref this.m_Handle, stateNameHash, transitionDuration, layer, 0f);
		}

		// Token: 0x060003BB RID: 955 RVA: 0x00006441 File Offset: 0x00004641
		public void CrossFadeInFixedTime(int stateNameHash, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("0.0f")] float fixedTime)
		{
			AnimatorControllerPlayable.CrossFadeInFixedTimeInternal(ref this.m_Handle, stateNameHash, transitionDuration, layer, fixedTime);
		}

		// Token: 0x060003BC RID: 956 RVA: 0x00006454 File Offset: 0x00004654
		public void CrossFade(string stateName, float transitionDuration)
		{
			AnimatorControllerPlayable.CrossFadeInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), transitionDuration, -1, float.NegativeInfinity);
		}

		// Token: 0x060003BD RID: 957 RVA: 0x0000646F File Offset: 0x0000466F
		public void CrossFade(string stateName, float transitionDuration, int layer)
		{
			AnimatorControllerPlayable.CrossFadeInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), transitionDuration, layer, float.NegativeInfinity);
		}

		// Token: 0x060003BE RID: 958 RVA: 0x0000648A File Offset: 0x0000468A
		public void CrossFade(string stateName, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			AnimatorControllerPlayable.CrossFadeInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), transitionDuration, layer, normalizedTime);
		}

		// Token: 0x060003BF RID: 959 RVA: 0x000064A2 File Offset: 0x000046A2
		public void CrossFade(int stateNameHash, float transitionDuration)
		{
			AnimatorControllerPlayable.CrossFadeInternal(ref this.m_Handle, stateNameHash, transitionDuration, -1, float.NegativeInfinity);
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x000064B8 File Offset: 0x000046B8
		public void CrossFade(int stateNameHash, float transitionDuration, int layer)
		{
			AnimatorControllerPlayable.CrossFadeInternal(ref this.m_Handle, stateNameHash, transitionDuration, layer, float.NegativeInfinity);
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x000064CE File Offset: 0x000046CE
		public void CrossFade(int stateNameHash, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			AnimatorControllerPlayable.CrossFadeInternal(ref this.m_Handle, stateNameHash, transitionDuration, layer, normalizedTime);
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x000064E1 File Offset: 0x000046E1
		public void PlayInFixedTime(string stateName)
		{
			AnimatorControllerPlayable.PlayInFixedTimeInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), -1, float.NegativeInfinity);
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x000064FB File Offset: 0x000046FB
		public void PlayInFixedTime(string stateName, int layer)
		{
			AnimatorControllerPlayable.PlayInFixedTimeInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), layer, float.NegativeInfinity);
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x00006515 File Offset: 0x00004715
		public void PlayInFixedTime(string stateName, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float fixedTime)
		{
			AnimatorControllerPlayable.PlayInFixedTimeInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), layer, fixedTime);
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x0000652B File Offset: 0x0000472B
		public void PlayInFixedTime(int stateNameHash)
		{
			AnimatorControllerPlayable.PlayInFixedTimeInternal(ref this.m_Handle, stateNameHash, -1, float.NegativeInfinity);
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x00006540 File Offset: 0x00004740
		public void PlayInFixedTime(int stateNameHash, int layer)
		{
			AnimatorControllerPlayable.PlayInFixedTimeInternal(ref this.m_Handle, stateNameHash, layer, float.NegativeInfinity);
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x00006555 File Offset: 0x00004755
		public void PlayInFixedTime(int stateNameHash, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float fixedTime)
		{
			AnimatorControllerPlayable.PlayInFixedTimeInternal(ref this.m_Handle, stateNameHash, layer, fixedTime);
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x00006566 File Offset: 0x00004766
		public void Play(string stateName)
		{
			AnimatorControllerPlayable.PlayInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), -1, float.NegativeInfinity);
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x00006580 File Offset: 0x00004780
		public void Play(string stateName, int layer)
		{
			AnimatorControllerPlayable.PlayInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), layer, float.NegativeInfinity);
		}

		// Token: 0x060003CA RID: 970 RVA: 0x0000659A File Offset: 0x0000479A
		public void Play(string stateName, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			AnimatorControllerPlayable.PlayInternal(ref this.m_Handle, AnimatorControllerPlayable.StringToHash(stateName), layer, normalizedTime);
		}

		// Token: 0x060003CB RID: 971 RVA: 0x000065B0 File Offset: 0x000047B0
		public void Play(int stateNameHash)
		{
			AnimatorControllerPlayable.PlayInternal(ref this.m_Handle, stateNameHash, -1, float.NegativeInfinity);
		}

		// Token: 0x060003CC RID: 972 RVA: 0x000065C5 File Offset: 0x000047C5
		public void Play(int stateNameHash, int layer)
		{
			AnimatorControllerPlayable.PlayInternal(ref this.m_Handle, stateNameHash, layer, float.NegativeInfinity);
		}

		// Token: 0x060003CD RID: 973 RVA: 0x000065DA File Offset: 0x000047DA
		public void Play(int stateNameHash, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			AnimatorControllerPlayable.PlayInternal(ref this.m_Handle, stateNameHash, layer, normalizedTime);
		}

		// Token: 0x060003CE RID: 974 RVA: 0x000065EC File Offset: 0x000047EC
		public bool HasState(int layerIndex, int stateID)
		{
			return AnimatorControllerPlayable.HasStateInternal(ref this.m_Handle, layerIndex, stateID);
		}

		// Token: 0x060003CF RID: 975 RVA: 0x00006610 File Offset: 0x00004810
		internal string ResolveHash(int hash)
		{
			return AnimatorControllerPlayable.ResolveHashInternal(ref this.m_Handle, hash);
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x00006631 File Offset: 0x00004831
		[NativeThrows]
		private static bool CreateHandleInternal(PlayableGraph graph, RuntimeAnimatorController controller, ref PlayableHandle handle)
		{
			return AnimatorControllerPlayable.CreateHandleInternal_Injected(ref graph, controller, ref handle);
		}

		// Token: 0x060003D1 RID: 977
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RuntimeAnimatorController GetAnimatorControllerInternal(ref PlayableHandle handle);

		// Token: 0x060003D2 RID: 978
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetLayerCountInternal(ref PlayableHandle handle);

		// Token: 0x060003D3 RID: 979
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetLayerNameInternal(ref PlayableHandle handle, int layerIndex);

		// Token: 0x060003D4 RID: 980
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetLayerIndexInternal(ref PlayableHandle handle, string layerName);

		// Token: 0x060003D5 RID: 981
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetLayerWeightInternal(ref PlayableHandle handle, int layerIndex);

		// Token: 0x060003D6 RID: 982
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLayerWeightInternal(ref PlayableHandle handle, int layerIndex, float weight);

		// Token: 0x060003D7 RID: 983 RVA: 0x0000663C File Offset: 0x0000483C
		[NativeThrows]
		private static AnimatorStateInfo GetCurrentAnimatorStateInfoInternal(ref PlayableHandle handle, int layerIndex)
		{
			AnimatorStateInfo result;
			AnimatorControllerPlayable.GetCurrentAnimatorStateInfoInternal_Injected(ref handle, layerIndex, out result);
			return result;
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x00006654 File Offset: 0x00004854
		[NativeThrows]
		private static AnimatorStateInfo GetNextAnimatorStateInfoInternal(ref PlayableHandle handle, int layerIndex)
		{
			AnimatorStateInfo result;
			AnimatorControllerPlayable.GetNextAnimatorStateInfoInternal_Injected(ref handle, layerIndex, out result);
			return result;
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x0000666C File Offset: 0x0000486C
		[NativeThrows]
		private static AnimatorTransitionInfo GetAnimatorTransitionInfoInternal(ref PlayableHandle handle, int layerIndex)
		{
			AnimatorTransitionInfo result;
			AnimatorControllerPlayable.GetAnimatorTransitionInfoInternal_Injected(ref handle, layerIndex, out result);
			return result;
		}

		// Token: 0x060003DA RID: 986
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimatorClipInfo[] GetCurrentAnimatorClipInfoInternal(ref PlayableHandle handle, int layerIndex);

		// Token: 0x060003DB RID: 987
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetAnimatorClipInfoCountInternal(ref PlayableHandle handle, int layerIndex, bool current);

		// Token: 0x060003DC RID: 988
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimatorClipInfo[] GetNextAnimatorClipInfoInternal(ref PlayableHandle handle, int layerIndex);

		// Token: 0x060003DD RID: 989
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string ResolveHashInternal(ref PlayableHandle handle, int hash);

		// Token: 0x060003DE RID: 990
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsInTransitionInternal(ref PlayableHandle handle, int layerIndex);

		// Token: 0x060003DF RID: 991
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimatorControllerParameter[] GetParametersArrayInternal(ref PlayableHandle handle);

		// Token: 0x060003E0 RID: 992
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetParameterCountInternal(ref PlayableHandle handle);

		// Token: 0x060003E1 RID: 993
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int StringToHash(string name);

		// Token: 0x060003E2 RID: 994
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CrossFadeInFixedTimeInternal(ref PlayableHandle handle, int stateNameHash, float transitionDuration, int layer, float fixedTime);

		// Token: 0x060003E3 RID: 995
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CrossFadeInternal(ref PlayableHandle handle, int stateNameHash, float transitionDuration, int layer, float normalizedTime);

		// Token: 0x060003E4 RID: 996
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void PlayInFixedTimeInternal(ref PlayableHandle handle, int stateNameHash, int layer, float fixedTime);

		// Token: 0x060003E5 RID: 997
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void PlayInternal(ref PlayableHandle handle, int stateNameHash, int layer, float normalizedTime);

		// Token: 0x060003E6 RID: 998
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool HasStateInternal(ref PlayableHandle handle, int layerIndex, int stateID);

		// Token: 0x060003E7 RID: 999
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetFloatString(ref PlayableHandle handle, string name, float value);

		// Token: 0x060003E8 RID: 1000
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetFloatID(ref PlayableHandle handle, int id, float value);

		// Token: 0x060003E9 RID: 1001
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetFloatString(ref PlayableHandle handle, string name);

		// Token: 0x060003EA RID: 1002
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetFloatID(ref PlayableHandle handle, int id);

		// Token: 0x060003EB RID: 1003
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetBoolString(ref PlayableHandle handle, string name, bool value);

		// Token: 0x060003EC RID: 1004
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetBoolID(ref PlayableHandle handle, int id, bool value);

		// Token: 0x060003ED RID: 1005
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetBoolString(ref PlayableHandle handle, string name);

		// Token: 0x060003EE RID: 1006
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetBoolID(ref PlayableHandle handle, int id);

		// Token: 0x060003EF RID: 1007
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetIntegerString(ref PlayableHandle handle, string name, int value);

		// Token: 0x060003F0 RID: 1008
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetIntegerID(ref PlayableHandle handle, int id, int value);

		// Token: 0x060003F1 RID: 1009
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetIntegerString(ref PlayableHandle handle, string name);

		// Token: 0x060003F2 RID: 1010
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetIntegerID(ref PlayableHandle handle, int id);

		// Token: 0x060003F3 RID: 1011
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetTriggerString(ref PlayableHandle handle, string name);

		// Token: 0x060003F4 RID: 1012
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetTriggerID(ref PlayableHandle handle, int id);

		// Token: 0x060003F5 RID: 1013
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ResetTriggerString(ref PlayableHandle handle, string name);

		// Token: 0x060003F6 RID: 1014
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ResetTriggerID(ref PlayableHandle handle, int id);

		// Token: 0x060003F7 RID: 1015
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsParameterControlledByCurveString(ref PlayableHandle handle, string name);

		// Token: 0x060003F8 RID: 1016
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsParameterControlledByCurveID(ref PlayableHandle handle, int id);

		// Token: 0x060003F9 RID: 1017 RVA: 0x00006683 File Offset: 0x00004883
		// Note: this type is marked as 'beforefieldinit'.
		static AnimatorControllerPlayable()
		{
		}

		// Token: 0x060003FA RID: 1018
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CreateHandleInternal_Injected(ref PlayableGraph graph, RuntimeAnimatorController controller, ref PlayableHandle handle);

		// Token: 0x060003FB RID: 1019
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetCurrentAnimatorStateInfoInternal_Injected(ref PlayableHandle handle, int layerIndex, out AnimatorStateInfo ret);

		// Token: 0x060003FC RID: 1020
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetNextAnimatorStateInfoInternal_Injected(ref PlayableHandle handle, int layerIndex, out AnimatorStateInfo ret);

		// Token: 0x060003FD RID: 1021
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetAnimatorTransitionInfoInternal_Injected(ref PlayableHandle handle, int layerIndex, out AnimatorTransitionInfo ret);

		// Token: 0x04000083 RID: 131
		private PlayableHandle m_Handle;

		// Token: 0x04000084 RID: 132
		private static readonly AnimatorControllerPlayable m_NullPlayable = new AnimatorControllerPlayable(PlayableHandle.Null);
	}
}
