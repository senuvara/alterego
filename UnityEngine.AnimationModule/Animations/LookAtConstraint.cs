﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Animations
{
	// Token: 0x02000052 RID: 82
	[RequireComponent(typeof(Transform))]
	[NativeHeader("Runtime/Animation/Constraints/Constraint.bindings.h")]
	[NativeHeader("Runtime/Animation/Constraints/LookAtConstraint.h")]
	[UsedByNativeCode]
	public sealed class LookAtConstraint : Behaviour, IConstraint, IConstraintInternal
	{
		// Token: 0x06000511 RID: 1297 RVA: 0x0000760C File Offset: 0x0000580C
		private LookAtConstraint()
		{
			LookAtConstraint.Internal_Create(this);
		}

		// Token: 0x06000512 RID: 1298
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] LookAtConstraint self);

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x06000513 RID: 1299
		// (set) Token: 0x06000514 RID: 1300
		public extern float weight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x06000515 RID: 1301
		// (set) Token: 0x06000516 RID: 1302
		public extern float roll { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x06000517 RID: 1303
		// (set) Token: 0x06000518 RID: 1304
		public extern bool constraintActive { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x06000519 RID: 1305
		// (set) Token: 0x0600051A RID: 1306
		public extern bool locked { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x0600051B RID: 1307 RVA: 0x0000761C File Offset: 0x0000581C
		// (set) Token: 0x0600051C RID: 1308 RVA: 0x00007632 File Offset: 0x00005832
		public Vector3 rotationAtRest
		{
			get
			{
				Vector3 result;
				this.get_rotationAtRest_Injected(out result);
				return result;
			}
			set
			{
				this.set_rotationAtRest_Injected(ref value);
			}
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x0600051D RID: 1309 RVA: 0x0000763C File Offset: 0x0000583C
		// (set) Token: 0x0600051E RID: 1310 RVA: 0x00007652 File Offset: 0x00005852
		public Vector3 rotationOffset
		{
			get
			{
				Vector3 result;
				this.get_rotationOffset_Injected(out result);
				return result;
			}
			set
			{
				this.set_rotationOffset_Injected(ref value);
			}
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x0600051F RID: 1311
		// (set) Token: 0x06000520 RID: 1312
		public extern Transform worldUpObject { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x06000521 RID: 1313
		// (set) Token: 0x06000522 RID: 1314
		public extern bool useUpObject { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x06000523 RID: 1315 RVA: 0x0000765C File Offset: 0x0000585C
		public int sourceCount
		{
			get
			{
				return LookAtConstraint.GetSourceCountInternal(this);
			}
		}

		// Token: 0x06000524 RID: 1316
		[FreeFunction("ConstraintBindings::GetSourceCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetSourceCountInternal([NotNull] LookAtConstraint self);

		// Token: 0x06000525 RID: 1317
		[FreeFunction(Name = "ConstraintBindings::GetSources", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GetSources([NotNull] List<ConstraintSource> sources);

		// Token: 0x06000526 RID: 1318 RVA: 0x00007677 File Offset: 0x00005877
		public void SetSources(List<ConstraintSource> sources)
		{
			if (sources == null)
			{
				throw new ArgumentNullException("sources");
			}
			LookAtConstraint.SetSourcesInternal(this, sources);
		}

		// Token: 0x06000527 RID: 1319
		[FreeFunction("ConstraintBindings::SetSources")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetSourcesInternal([NotNull] LookAtConstraint self, List<ConstraintSource> sources);

		// Token: 0x06000528 RID: 1320 RVA: 0x00007692 File Offset: 0x00005892
		public int AddSource(ConstraintSource source)
		{
			return this.AddSource_Injected(ref source);
		}

		// Token: 0x06000529 RID: 1321 RVA: 0x0000769C File Offset: 0x0000589C
		public void RemoveSource(int index)
		{
			this.ValidateSourceIndex(index);
			this.RemoveSourceInternal(index);
		}

		// Token: 0x0600052A RID: 1322
		[NativeName("RemoveSource")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RemoveSourceInternal(int index);

		// Token: 0x0600052B RID: 1323 RVA: 0x000076B0 File Offset: 0x000058B0
		public ConstraintSource GetSource(int index)
		{
			this.ValidateSourceIndex(index);
			return this.GetSourceInternal(index);
		}

		// Token: 0x0600052C RID: 1324 RVA: 0x000076D4 File Offset: 0x000058D4
		[NativeName("GetSource")]
		private ConstraintSource GetSourceInternal(int index)
		{
			ConstraintSource result;
			this.GetSourceInternal_Injected(index, out result);
			return result;
		}

		// Token: 0x0600052D RID: 1325 RVA: 0x000076EB File Offset: 0x000058EB
		public void SetSource(int index, ConstraintSource source)
		{
			this.ValidateSourceIndex(index);
			this.SetSourceInternal(index, source);
		}

		// Token: 0x0600052E RID: 1326 RVA: 0x000076FD File Offset: 0x000058FD
		[NativeName("SetSource")]
		private void SetSourceInternal(int index, ConstraintSource source)
		{
			this.SetSourceInternal_Injected(index, ref source);
		}

		// Token: 0x0600052F RID: 1327 RVA: 0x00007708 File Offset: 0x00005908
		private void ValidateSourceIndex(int index)
		{
			if (this.sourceCount == 0)
			{
				throw new InvalidOperationException("The LookAtConstraint component has no sources.");
			}
			if (index < 0 || index >= this.sourceCount)
			{
				throw new ArgumentOutOfRangeException("index", string.Format("Constraint source index {0} is out of bounds (0-{1}).", index, this.sourceCount));
			}
		}

		// Token: 0x06000530 RID: 1328
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rotationAtRest_Injected(out Vector3 ret);

		// Token: 0x06000531 RID: 1329
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rotationAtRest_Injected(ref Vector3 value);

		// Token: 0x06000532 RID: 1330
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rotationOffset_Injected(out Vector3 ret);

		// Token: 0x06000533 RID: 1331
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rotationOffset_Injected(ref Vector3 value);

		// Token: 0x06000534 RID: 1332
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int AddSource_Injected(ref ConstraintSource source);

		// Token: 0x06000535 RID: 1333
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetSourceInternal_Injected(int index, out ConstraintSource ret);

		// Token: 0x06000536 RID: 1334
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetSourceInternal_Injected(int index, ref ConstraintSource source);
	}
}
