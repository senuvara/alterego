﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Playables;
using UnityEngine.Scripting;

namespace UnityEngine.Animations
{
	// Token: 0x02000010 RID: 16
	[NativeHeader("Runtime/Director/Core/HPlayable.h")]
	[StaticAccessor("AnimationLayerMixerPlayableBindings", StaticAccessorType.DoubleColon)]
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimationLayerMixerPlayable.bindings.h")]
	[NativeHeader("Runtime/Animation/Director/AnimationLayerMixerPlayable.h")]
	[RequiredByNativeCode]
	public struct AnimationLayerMixerPlayable : IPlayable, IEquatable<AnimationLayerMixerPlayable>
	{
		// Token: 0x06000106 RID: 262 RVA: 0x00002DDB File Offset: 0x00000FDB
		internal AnimationLayerMixerPlayable(PlayableHandle handle)
		{
			if (handle.IsValid())
			{
				if (!handle.IsPlayableOfType<AnimationLayerMixerPlayable>())
				{
					throw new InvalidCastException("Can't set handle: the playable is not an AnimationLayerMixerPlayable.");
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000107 RID: 263 RVA: 0x00002E0C File Offset: 0x0000100C
		public static AnimationLayerMixerPlayable Null
		{
			get
			{
				return AnimationLayerMixerPlayable.m_NullPlayable;
			}
		}

		// Token: 0x06000108 RID: 264 RVA: 0x00002E28 File Offset: 0x00001028
		public static AnimationLayerMixerPlayable Create(PlayableGraph graph, int inputCount = 0)
		{
			PlayableHandle handle = AnimationLayerMixerPlayable.CreateHandle(graph, inputCount);
			return new AnimationLayerMixerPlayable(handle);
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00002E4C File Offset: 0x0000104C
		private static PlayableHandle CreateHandle(PlayableGraph graph, int inputCount = 0)
		{
			PlayableHandle @null = PlayableHandle.Null;
			PlayableHandle result;
			if (!AnimationLayerMixerPlayable.CreateHandleInternal(graph, ref @null))
			{
				result = PlayableHandle.Null;
			}
			else
			{
				@null.SetInputCount(inputCount);
				result = @null;
			}
			return result;
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00002E88 File Offset: 0x00001088
		public PlayableHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x0600010B RID: 267 RVA: 0x00002EA4 File Offset: 0x000010A4
		public static implicit operator Playable(AnimationLayerMixerPlayable playable)
		{
			return new Playable(playable.GetHandle());
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00002EC8 File Offset: 0x000010C8
		public static explicit operator AnimationLayerMixerPlayable(Playable playable)
		{
			return new AnimationLayerMixerPlayable(playable.GetHandle());
		}

		// Token: 0x0600010D RID: 269 RVA: 0x00002EEC File Offset: 0x000010EC
		public bool Equals(AnimationLayerMixerPlayable other)
		{
			return this.GetHandle() == other.GetHandle();
		}

		// Token: 0x0600010E RID: 270 RVA: 0x00002F14 File Offset: 0x00001114
		public bool IsLayerAdditive(uint layerIndex)
		{
			if ((ulong)layerIndex >= (ulong)((long)this.m_Handle.GetInputCount()))
			{
				throw new ArgumentOutOfRangeException("layerIndex", string.Format("layerIndex {0} must be in the range of 0 to {1}.", layerIndex, this.m_Handle.GetInputCount() - 1));
			}
			return AnimationLayerMixerPlayable.IsLayerAdditiveInternal(ref this.m_Handle, layerIndex);
		}

		// Token: 0x0600010F RID: 271 RVA: 0x00002F78 File Offset: 0x00001178
		public void SetLayerAdditive(uint layerIndex, bool value)
		{
			if ((ulong)layerIndex >= (ulong)((long)this.m_Handle.GetInputCount()))
			{
				throw new ArgumentOutOfRangeException("layerIndex", string.Format("layerIndex {0} must be in the range of 0 to {1}.", layerIndex, this.m_Handle.GetInputCount() - 1));
			}
			AnimationLayerMixerPlayable.SetLayerAdditiveInternal(ref this.m_Handle, layerIndex, value);
		}

		// Token: 0x06000110 RID: 272 RVA: 0x00002FD4 File Offset: 0x000011D4
		public void SetLayerMaskFromAvatarMask(uint layerIndex, AvatarMask mask)
		{
			if ((ulong)layerIndex >= (ulong)((long)this.m_Handle.GetInputCount()))
			{
				throw new ArgumentOutOfRangeException("layerIndex", string.Format("layerIndex {0} must be in the range of 0 to {1}.", layerIndex, this.m_Handle.GetInputCount() - 1));
			}
			if (mask == null)
			{
				throw new ArgumentNullException("mask");
			}
			AnimationLayerMixerPlayable.SetLayerMaskFromAvatarMaskInternal(ref this.m_Handle, layerIndex, mask);
		}

		// Token: 0x06000111 RID: 273 RVA: 0x00003046 File Offset: 0x00001246
		[NativeThrows]
		private static bool CreateHandleInternal(PlayableGraph graph, ref PlayableHandle handle)
		{
			return AnimationLayerMixerPlayable.CreateHandleInternal_Injected(ref graph, ref handle);
		}

		// Token: 0x06000112 RID: 274
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsLayerAdditiveInternal(ref PlayableHandle handle, uint layerIndex);

		// Token: 0x06000113 RID: 275
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLayerAdditiveInternal(ref PlayableHandle handle, uint layerIndex, bool value);

		// Token: 0x06000114 RID: 276
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLayerMaskFromAvatarMaskInternal(ref PlayableHandle handle, uint layerIndex, AvatarMask mask);

		// Token: 0x06000115 RID: 277 RVA: 0x00003050 File Offset: 0x00001250
		// Note: this type is marked as 'beforefieldinit'.
		static AnimationLayerMixerPlayable()
		{
		}

		// Token: 0x06000116 RID: 278
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CreateHandleInternal_Injected(ref PlayableGraph graph, ref PlayableHandle handle);

		// Token: 0x0400000D RID: 13
		private PlayableHandle m_Handle;

		// Token: 0x0400000E RID: 14
		private static readonly AnimationLayerMixerPlayable m_NullPlayable = new AnimationLayerMixerPlayable(PlayableHandle.Null);
	}
}
