﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Playables;
using UnityEngine.Scripting;

namespace UnityEngine.Animations
{
	// Token: 0x02000017 RID: 23
	[NativeHeader("Runtime/Animation/Director/AnimationPosePlayable.h")]
	[NativeHeader("Runtime/Animation/ScriptBindings/AnimationPosePlayable.bindings.h")]
	[RequiredByNativeCode]
	[StaticAccessor("AnimationPosePlayableBindings", StaticAccessorType.DoubleColon)]
	[NativeHeader("Runtime/Director/Core/HPlayable.h")]
	internal struct AnimationPosePlayable : IPlayable, IEquatable<AnimationPosePlayable>
	{
		// Token: 0x0600015B RID: 347 RVA: 0x000036A7 File Offset: 0x000018A7
		internal AnimationPosePlayable(PlayableHandle handle)
		{
			if (handle.IsValid())
			{
				if (!handle.IsPlayableOfType<AnimationPosePlayable>())
				{
					throw new InvalidCastException("Can't set handle: the playable is not an AnimationPosePlayable.");
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600015C RID: 348 RVA: 0x000036D8 File Offset: 0x000018D8
		public static AnimationPosePlayable Null
		{
			get
			{
				return AnimationPosePlayable.m_NullPlayable;
			}
		}

		// Token: 0x0600015D RID: 349 RVA: 0x000036F4 File Offset: 0x000018F4
		public static AnimationPosePlayable Create(PlayableGraph graph)
		{
			PlayableHandle handle = AnimationPosePlayable.CreateHandle(graph);
			return new AnimationPosePlayable(handle);
		}

		// Token: 0x0600015E RID: 350 RVA: 0x00003718 File Offset: 0x00001918
		private static PlayableHandle CreateHandle(PlayableGraph graph)
		{
			PlayableHandle @null = PlayableHandle.Null;
			PlayableHandle result;
			if (!AnimationPosePlayable.CreateHandleInternal(graph, ref @null))
			{
				result = PlayableHandle.Null;
			}
			else
			{
				result = @null;
			}
			return result;
		}

		// Token: 0x0600015F RID: 351 RVA: 0x0000374C File Offset: 0x0000194C
		public PlayableHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x06000160 RID: 352 RVA: 0x00003768 File Offset: 0x00001968
		public static implicit operator Playable(AnimationPosePlayable playable)
		{
			return new Playable(playable.GetHandle());
		}

		// Token: 0x06000161 RID: 353 RVA: 0x0000378C File Offset: 0x0000198C
		public static explicit operator AnimationPosePlayable(Playable playable)
		{
			return new AnimationPosePlayable(playable.GetHandle());
		}

		// Token: 0x06000162 RID: 354 RVA: 0x000037B0 File Offset: 0x000019B0
		public bool Equals(AnimationPosePlayable other)
		{
			return this.Equals(other.GetHandle());
		}

		// Token: 0x06000163 RID: 355 RVA: 0x000037E0 File Offset: 0x000019E0
		public bool GetMustReadPreviousPose()
		{
			return AnimationPosePlayable.GetMustReadPreviousPoseInternal(ref this.m_Handle);
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00003800 File Offset: 0x00001A00
		public void SetMustReadPreviousPose(bool value)
		{
			AnimationPosePlayable.SetMustReadPreviousPoseInternal(ref this.m_Handle, value);
		}

		// Token: 0x06000165 RID: 357 RVA: 0x00003810 File Offset: 0x00001A10
		public bool GetReadDefaultPose()
		{
			return AnimationPosePlayable.GetReadDefaultPoseInternal(ref this.m_Handle);
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00003830 File Offset: 0x00001A30
		public void SetReadDefaultPose(bool value)
		{
			AnimationPosePlayable.SetReadDefaultPoseInternal(ref this.m_Handle, value);
		}

		// Token: 0x06000167 RID: 359 RVA: 0x00003840 File Offset: 0x00001A40
		public bool GetApplyFootIK()
		{
			return AnimationPosePlayable.GetApplyFootIKInternal(ref this.m_Handle);
		}

		// Token: 0x06000168 RID: 360 RVA: 0x00003860 File Offset: 0x00001A60
		public void SetApplyFootIK(bool value)
		{
			AnimationPosePlayable.SetApplyFootIKInternal(ref this.m_Handle, value);
		}

		// Token: 0x06000169 RID: 361 RVA: 0x0000386F File Offset: 0x00001A6F
		[NativeThrows]
		private static bool CreateHandleInternal(PlayableGraph graph, ref PlayableHandle handle)
		{
			return AnimationPosePlayable.CreateHandleInternal_Injected(ref graph, ref handle);
		}

		// Token: 0x0600016A RID: 362
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetMustReadPreviousPoseInternal(ref PlayableHandle handle);

		// Token: 0x0600016B RID: 363
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetMustReadPreviousPoseInternal(ref PlayableHandle handle, bool value);

		// Token: 0x0600016C RID: 364
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetReadDefaultPoseInternal(ref PlayableHandle handle);

		// Token: 0x0600016D RID: 365
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetReadDefaultPoseInternal(ref PlayableHandle handle, bool value);

		// Token: 0x0600016E RID: 366
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetApplyFootIKInternal(ref PlayableHandle handle);

		// Token: 0x0600016F RID: 367
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetApplyFootIKInternal(ref PlayableHandle handle, bool value);

		// Token: 0x06000170 RID: 368 RVA: 0x00003879 File Offset: 0x00001A79
		// Note: this type is marked as 'beforefieldinit'.
		static AnimationPosePlayable()
		{
		}

		// Token: 0x06000171 RID: 369
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CreateHandleInternal_Injected(ref PlayableGraph graph, ref PlayableHandle handle);

		// Token: 0x04000016 RID: 22
		private PlayableHandle m_Handle;

		// Token: 0x04000017 RID: 23
		private static readonly AnimationPosePlayable m_NullPlayable = new AnimationPosePlayable(PlayableHandle.Null);
	}
}
