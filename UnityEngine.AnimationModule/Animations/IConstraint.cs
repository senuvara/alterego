﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Animations
{
	// Token: 0x0200004A RID: 74
	public interface IConstraint
	{
		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x06000481 RID: 1153
		// (set) Token: 0x06000482 RID: 1154
		float weight { get; set; }

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x06000483 RID: 1155
		// (set) Token: 0x06000484 RID: 1156
		bool constraintActive { get; set; }

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000485 RID: 1157
		// (set) Token: 0x06000486 RID: 1158
		bool locked { get; set; }

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000487 RID: 1159
		int sourceCount { get; }

		// Token: 0x06000488 RID: 1160
		int AddSource(ConstraintSource source);

		// Token: 0x06000489 RID: 1161
		void RemoveSource(int index);

		// Token: 0x0600048A RID: 1162
		ConstraintSource GetSource(int index);

		// Token: 0x0600048B RID: 1163
		void SetSource(int index, ConstraintSource source);

		// Token: 0x0600048C RID: 1164
		void GetSources(List<ConstraintSource> sources);

		// Token: 0x0600048D RID: 1165
		void SetSources(List<ConstraintSource> sources);
	}
}
