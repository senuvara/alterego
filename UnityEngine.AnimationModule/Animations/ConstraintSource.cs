﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Animations
{
	// Token: 0x02000049 RID: 73
	[NativeType(CodegenOptions = CodegenOptions.Custom, Header = "Runtime/Animation/Constraints/ConstraintSource.h", IntermediateScriptingStructName = "MonoConstraintSource")]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Animation/Constraints/Constraint.bindings.h")]
	[Serializable]
	public struct ConstraintSource
	{
		// Token: 0x170000AE RID: 174
		// (get) Token: 0x0600047D RID: 1149 RVA: 0x00006EE4 File Offset: 0x000050E4
		// (set) Token: 0x0600047E RID: 1150 RVA: 0x00006EFF File Offset: 0x000050FF
		public Transform sourceTransform
		{
			get
			{
				return this.m_SourceTransform;
			}
			set
			{
				this.m_SourceTransform = value;
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x0600047F RID: 1151 RVA: 0x00006F0C File Offset: 0x0000510C
		// (set) Token: 0x06000480 RID: 1152 RVA: 0x00006F27 File Offset: 0x00005127
		public float weight
		{
			get
			{
				return this.m_Weight;
			}
			set
			{
				this.m_Weight = value;
			}
		}

		// Token: 0x0400014F RID: 335
		[NativeName("sourceTransform")]
		private Transform m_SourceTransform;

		// Token: 0x04000150 RID: 336
		[NativeName("weight")]
		private float m_Weight;
	}
}
