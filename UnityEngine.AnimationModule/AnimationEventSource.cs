﻿using System;

namespace UnityEngine
{
	// Token: 0x02000057 RID: 87
	internal enum AnimationEventSource
	{
		// Token: 0x04000159 RID: 345
		NoSource,
		// Token: 0x0400015A RID: 346
		Legacy,
		// Token: 0x0400015B RID: 347
		Animator
	}
}
