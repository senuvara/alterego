﻿using System;
using UnityEngine.Animations;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200000A RID: 10
	[RequiredByNativeCode]
	public abstract class StateMachineBehaviour : ScriptableObject
	{
		// Token: 0x06000015 RID: 21 RVA: 0x00002338 File Offset: 0x00000538
		protected StateMachineBehaviour()
		{
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// Token: 0x06000018 RID: 24 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
		{
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateMachineExit(Animator animator, int stateMachinePathHash)
		{
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateMachineEnter(Animator animator, int stateMachinePathHash, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002340 File Offset: 0x00000540
		public virtual void OnStateMachineExit(Animator animator, int stateMachinePathHash, AnimatorControllerPlayable controller)
		{
		}
	}
}
