﻿using System;
using System.ComponentModel;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000041 RID: 65
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Animation/HumanDescription.h")]
	[NativeType(CodegenOptions.Custom, "MonoSkeletonBone")]
	public struct SkeletonBone
	{
		// Token: 0x1700009B RID: 155
		// (get) Token: 0x06000445 RID: 1093 RVA: 0x00006B38 File Offset: 0x00004D38
		// (set) Token: 0x06000446 RID: 1094 RVA: 0x00002340 File Offset: 0x00000540
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("transformModified is no longer used and has been deprecated.", true)]
		public int transformModified
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x04000121 RID: 289
		[NativeName("m_Name")]
		public string name;

		// Token: 0x04000122 RID: 290
		[NativeName("m_ParentName")]
		internal string parentName;

		// Token: 0x04000123 RID: 291
		[NativeName("m_Position")]
		public Vector3 position;

		// Token: 0x04000124 RID: 292
		[NativeName("m_Rotation")]
		public Quaternion rotation;

		// Token: 0x04000125 RID: 293
		[NativeName("m_Scale")]
		public Vector3 scale;
	}
}
