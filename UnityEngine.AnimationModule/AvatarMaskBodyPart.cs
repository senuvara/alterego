﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine
{
	// Token: 0x02000046 RID: 70
	[MovedFrom("UnityEditor.Animations", true)]
	public enum AvatarMaskBodyPart
	{
		// Token: 0x0400013C RID: 316
		Root,
		// Token: 0x0400013D RID: 317
		Body,
		// Token: 0x0400013E RID: 318
		Head,
		// Token: 0x0400013F RID: 319
		LeftLeg,
		// Token: 0x04000140 RID: 320
		RightLeg,
		// Token: 0x04000141 RID: 321
		LeftArm,
		// Token: 0x04000142 RID: 322
		RightArm,
		// Token: 0x04000143 RID: 323
		LeftFingers,
		// Token: 0x04000144 RID: 324
		RightFingers,
		// Token: 0x04000145 RID: 325
		LeftFootIK,
		// Token: 0x04000146 RID: 326
		RightFootIK,
		// Token: 0x04000147 RID: 327
		LeftHandIK,
		// Token: 0x04000148 RID: 328
		RightHandIK,
		// Token: 0x04000149 RID: 329
		LastBodyPart
	}
}
