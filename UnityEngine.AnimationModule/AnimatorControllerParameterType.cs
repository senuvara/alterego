﻿using System;

namespace UnityEngine
{
	// Token: 0x02000024 RID: 36
	public enum AnimatorControllerParameterType
	{
		// Token: 0x0400004D RID: 77
		Float = 1,
		// Token: 0x0400004E RID: 78
		Int = 3,
		// Token: 0x0400004F RID: 79
		Bool,
		// Token: 0x04000050 RID: 80
		Trigger = 9
	}
}
