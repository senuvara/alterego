﻿using System;
using System.Runtime.InteropServices;

namespace UnityEngine
{
	// Token: 0x02000033 RID: 51
	[Obsolete("This class is not used anymore. See AnimatorOverrideController.GetOverrides() and AnimatorOverrideController.ApplyOverrides()")]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public class AnimationClipPair
	{
		// Token: 0x06000410 RID: 1040 RVA: 0x000067B4 File Offset: 0x000049B4
		public AnimationClipPair()
		{
		}

		// Token: 0x04000085 RID: 133
		public AnimationClip originalClip;

		// Token: 0x04000086 RID: 134
		public AnimationClip overrideClip;
	}
}
