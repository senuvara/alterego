﻿using System;

namespace UnityEngine
{
	// Token: 0x02000029 RID: 41
	public enum AnimatorCullingMode
	{
		// Token: 0x04000062 RID: 98
		AlwaysAnimate,
		// Token: 0x04000063 RID: 99
		CullUpdateTransforms,
		// Token: 0x04000064 RID: 100
		CullCompletely
	}
}
