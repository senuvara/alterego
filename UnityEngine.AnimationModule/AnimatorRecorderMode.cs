﻿using System;

namespace UnityEngine
{
	// Token: 0x02000027 RID: 39
	public enum AnimatorRecorderMode
	{
		// Token: 0x0400005B RID: 91
		Offline,
		// Token: 0x0400005C RID: 92
		Playback,
		// Token: 0x0400005D RID: 93
		Record
	}
}
