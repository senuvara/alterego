﻿using System;

namespace UnityEngine
{
	// Token: 0x02000025 RID: 37
	internal enum TransitionType
	{
		// Token: 0x04000052 RID: 82
		Normal = 1,
		// Token: 0x04000053 RID: 83
		Entry,
		// Token: 0x04000054 RID: 84
		Exit = 4
	}
}
