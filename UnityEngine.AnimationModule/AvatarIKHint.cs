﻿using System;

namespace UnityEngine
{
	// Token: 0x02000023 RID: 35
	public enum AvatarIKHint
	{
		// Token: 0x04000048 RID: 72
		LeftKnee,
		// Token: 0x04000049 RID: 73
		RightKnee,
		// Token: 0x0400004A RID: 74
		LeftElbow,
		// Token: 0x0400004B RID: 75
		RightElbow
	}
}
