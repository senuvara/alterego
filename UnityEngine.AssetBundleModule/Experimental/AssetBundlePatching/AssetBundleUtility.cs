﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.AssetBundlePatching
{
	// Token: 0x02000008 RID: 8
	[NativeHeader("Modules/AssetBundle/Public/AssetBundlePatching.h")]
	public static class AssetBundleUtility
	{
		// Token: 0x06000059 RID: 89
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void PatchAssetBundles(AssetBundle[] bundles, string[] filenames);
	}
}
