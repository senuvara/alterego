﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000A RID: 10
	public enum CompressionLevel
	{
		// Token: 0x04000016 RID: 22
		None,
		// Token: 0x04000017 RID: 23
		Fastest,
		// Token: 0x04000018 RID: 24
		Fast,
		// Token: 0x04000019 RID: 25
		Normal,
		// Token: 0x0400001A RID: 26
		High,
		// Token: 0x0400001B RID: 27
		Maximum
	}
}
