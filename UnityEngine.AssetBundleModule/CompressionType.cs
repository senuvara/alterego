﻿using System;

namespace UnityEngine
{
	// Token: 0x02000009 RID: 9
	public enum CompressionType
	{
		// Token: 0x04000011 RID: 17
		None,
		// Token: 0x04000012 RID: 18
		Lzma,
		// Token: 0x04000013 RID: 19
		Lz4,
		// Token: 0x04000014 RID: 20
		Lz4HC
	}
}
