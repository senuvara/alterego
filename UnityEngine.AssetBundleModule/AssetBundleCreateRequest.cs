﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000004 RID: 4
	[RequiredByNativeCode]
	[NativeHeader("Modules/AssetBundle/Public/AssetBundleLoadFromAsyncOperation.h")]
	[StructLayout(LayoutKind.Sequential)]
	public class AssetBundleCreateRequest : AsyncOperation
	{
		// Token: 0x06000045 RID: 69 RVA: 0x000027EE File Offset: 0x000009EE
		public AssetBundleCreateRequest()
		{
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000046 RID: 70
		public extern AssetBundle assetBundle { [NativeMethod("GetAssetBundleBlocking")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000047 RID: 71
		[NativeMethod("SetEnableCompatibilityChecks")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetEnableCompatibilityChecks(bool set);

		// Token: 0x06000048 RID: 72 RVA: 0x000027F6 File Offset: 0x000009F6
		internal void DisableCompatibilityChecks()
		{
			this.SetEnableCompatibilityChecks(false);
		}
	}
}
