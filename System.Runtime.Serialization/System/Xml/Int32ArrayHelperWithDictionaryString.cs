﻿using System;

namespace System.Xml
{
	// Token: 0x0200000A RID: 10
	internal class Int32ArrayHelperWithDictionaryString : ArrayHelper<XmlDictionaryString, int>
	{
		// Token: 0x06000023 RID: 35 RVA: 0x0000232D File Offset: 0x0000052D
		protected override int ReadArray(XmlDictionaryReader reader, XmlDictionaryString localName, XmlDictionaryString namespaceUri, int[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000024 RID: 36 RVA: 0x0000233D File Offset: 0x0000053D
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, int[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000025 RID: 37 RVA: 0x0000234F File Offset: 0x0000054F
		public Int32ArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00002357 File Offset: 0x00000557
		// Note: this type is marked as 'beforefieldinit'.
		static Int32ArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x04000008 RID: 8
		public static readonly Int32ArrayHelperWithDictionaryString Instance = new Int32ArrayHelperWithDictionaryString();
	}
}
