﻿using System;

namespace System.Xml
{
	// Token: 0x02000026 RID: 38
	internal enum ValueHandleType
	{
		// Token: 0x04000081 RID: 129
		Empty,
		// Token: 0x04000082 RID: 130
		True,
		// Token: 0x04000083 RID: 131
		False,
		// Token: 0x04000084 RID: 132
		Zero,
		// Token: 0x04000085 RID: 133
		One,
		// Token: 0x04000086 RID: 134
		Int8,
		// Token: 0x04000087 RID: 135
		Int16,
		// Token: 0x04000088 RID: 136
		Int32,
		// Token: 0x04000089 RID: 137
		Int64,
		// Token: 0x0400008A RID: 138
		UInt64,
		// Token: 0x0400008B RID: 139
		Single,
		// Token: 0x0400008C RID: 140
		Double,
		// Token: 0x0400008D RID: 141
		Decimal,
		// Token: 0x0400008E RID: 142
		DateTime,
		// Token: 0x0400008F RID: 143
		TimeSpan,
		// Token: 0x04000090 RID: 144
		Guid,
		// Token: 0x04000091 RID: 145
		UniqueId,
		// Token: 0x04000092 RID: 146
		UTF8,
		// Token: 0x04000093 RID: 147
		EscapedUTF8,
		// Token: 0x04000094 RID: 148
		Base64,
		// Token: 0x04000095 RID: 149
		Dictionary,
		// Token: 0x04000096 RID: 150
		List,
		// Token: 0x04000097 RID: 151
		Char,
		// Token: 0x04000098 RID: 152
		Unicode,
		// Token: 0x04000099 RID: 153
		QName,
		// Token: 0x0400009A RID: 154
		ConstString
	}
}
