﻿using System;

namespace System.Xml
{
	// Token: 0x02000024 RID: 36
	internal enum ValueHandleConstStringType
	{
		// Token: 0x0400006E RID: 110
		String,
		// Token: 0x0400006F RID: 111
		Number,
		// Token: 0x04000070 RID: 112
		Array,
		// Token: 0x04000071 RID: 113
		Object,
		// Token: 0x04000072 RID: 114
		Boolean,
		// Token: 0x04000073 RID: 115
		Null
	}
}
