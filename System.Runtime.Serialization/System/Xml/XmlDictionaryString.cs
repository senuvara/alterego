﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace System.Xml
{
	/// <summary>Represents an entry stored in a <see cref="T:System.Xml.XmlDictionary" />.</summary>
	// Token: 0x02000063 RID: 99
	public class XmlDictionaryString
	{
		/// <summary>Creates an instance of this class. </summary>
		/// <param name="dictionary">The <see cref="T:System.Xml.IXmlDictionary" /> containing this instance.</param>
		/// <param name="value">The string that is the value of the dictionary entry.</param>
		/// <param name="key">The integer that is the key of the dictionary entry.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="dictionary" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="key" /> is less than 0 or greater than <see cref="F:System.Int32.MaxValue" /> / 4.</exception>
		// Token: 0x06000546 RID: 1350 RVA: 0x0001899C File Offset: 0x00016B9C
		public XmlDictionaryString(IXmlDictionary dictionary, string value, int key)
		{
			if (dictionary == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentNullException("dictionary"));
			}
			if (value == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentNullException("value"));
			}
			if (key < 0 || key > 536870911)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("key", System.Runtime.Serialization.SR.GetString("The value of this argument must fall within the range {0} to {1}.", new object[]
				{
					0,
					536870911
				})));
			}
			this.dictionary = dictionary;
			this.value = value;
			this.key = key;
		}

		// Token: 0x06000547 RID: 1351 RVA: 0x00018A2C File Offset: 0x00016C2C
		internal static string GetString(XmlDictionaryString s)
		{
			if (s == null)
			{
				return null;
			}
			return s.Value;
		}

		/// <summary>Gets an <see cref="T:System.Xml.XmlDictionaryString" /> representing the empty string.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlDictionaryString" /> representing the empty string.</returns>
		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000548 RID: 1352 RVA: 0x00018A39 File Offset: 0x00016C39
		public static XmlDictionaryString Empty
		{
			get
			{
				return XmlDictionaryString.emptyStringDictionary.EmptyString;
			}
		}

		/// <summary>Represents the <see cref="T:System.Xml.IXmlDictionary" /> passed to the constructor of this instance of <see cref="T:System.Xml.XmlDictionaryString" />.</summary>
		/// <returns>The <see cref="T:System.Xml.IXmlDictionary" /> for this dictionary entry.</returns>
		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000549 RID: 1353 RVA: 0x00018A45 File Offset: 0x00016C45
		public IXmlDictionary Dictionary
		{
			get
			{
				return this.dictionary;
			}
		}

		/// <summary>Gets the integer key for this instance of the class.  </summary>
		/// <returns>The integer key for this instance of the class. </returns>
		// Token: 0x17000099 RID: 153
		// (get) Token: 0x0600054A RID: 1354 RVA: 0x00018A4D File Offset: 0x00016C4D
		public int Key
		{
			get
			{
				return this.key;
			}
		}

		/// <summary>Gets the string value for this instance of the class.  </summary>
		/// <returns>The string value for this instance of the class. </returns>
		// Token: 0x1700009A RID: 154
		// (get) Token: 0x0600054B RID: 1355 RVA: 0x00018A55 File Offset: 0x00016C55
		public string Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x0600054C RID: 1356 RVA: 0x00018A5D File Offset: 0x00016C5D
		internal byte[] ToUTF8()
		{
			if (this.buffer == null)
			{
				this.buffer = Encoding.UTF8.GetBytes(this.value);
			}
			return this.buffer;
		}

		/// <summary>Displays a text representation of this object.</summary>
		/// <returns>The string value for this instance of the class. </returns>
		// Token: 0x0600054D RID: 1357 RVA: 0x00018A55 File Offset: 0x00016C55
		public override string ToString()
		{
			return this.value;
		}

		// Token: 0x0600054E RID: 1358 RVA: 0x00018A83 File Offset: 0x00016C83
		// Note: this type is marked as 'beforefieldinit'.
		static XmlDictionaryString()
		{
		}

		// Token: 0x04000291 RID: 657
		internal const int MinKey = 0;

		// Token: 0x04000292 RID: 658
		internal const int MaxKey = 536870911;

		// Token: 0x04000293 RID: 659
		private IXmlDictionary dictionary;

		// Token: 0x04000294 RID: 660
		private string value;

		// Token: 0x04000295 RID: 661
		private int key;

		// Token: 0x04000296 RID: 662
		private byte[] buffer;

		// Token: 0x04000297 RID: 663
		private static XmlDictionaryString.EmptyStringDictionary emptyStringDictionary = new XmlDictionaryString.EmptyStringDictionary();

		// Token: 0x02000064 RID: 100
		private class EmptyStringDictionary : IXmlDictionary
		{
			// Token: 0x0600054F RID: 1359 RVA: 0x00018A8F File Offset: 0x00016C8F
			public EmptyStringDictionary()
			{
				this.empty = new XmlDictionaryString(this, string.Empty, 0);
			}

			// Token: 0x1700009B RID: 155
			// (get) Token: 0x06000550 RID: 1360 RVA: 0x00018AA9 File Offset: 0x00016CA9
			public XmlDictionaryString EmptyString
			{
				get
				{
					return this.empty;
				}
			}

			// Token: 0x06000551 RID: 1361 RVA: 0x00018AB1 File Offset: 0x00016CB1
			public bool TryLookup(string value, out XmlDictionaryString result)
			{
				if (value == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("value");
				}
				if (value.Length == 0)
				{
					result = this.empty;
					return true;
				}
				result = null;
				return false;
			}

			// Token: 0x06000552 RID: 1362 RVA: 0x00018AD7 File Offset: 0x00016CD7
			public bool TryLookup(int key, out XmlDictionaryString result)
			{
				if (key == 0)
				{
					result = this.empty;
					return true;
				}
				result = null;
				return false;
			}

			// Token: 0x06000553 RID: 1363 RVA: 0x0000FC70 File Offset: 0x0000DE70
			public bool TryLookup(XmlDictionaryString value, out XmlDictionaryString result)
			{
				if (value == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentNullException("value"));
				}
				if (value.Dictionary != this)
				{
					result = null;
					return false;
				}
				result = value;
				return true;
			}

			// Token: 0x04000298 RID: 664
			private XmlDictionaryString empty;
		}
	}
}
