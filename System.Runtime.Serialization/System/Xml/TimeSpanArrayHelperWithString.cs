﻿using System;

namespace System.Xml
{
	// Token: 0x02000017 RID: 23
	internal class TimeSpanArrayHelperWithString : ArrayHelper<string, TimeSpan>
	{
		// Token: 0x06000057 RID: 87 RVA: 0x000025EB File Offset: 0x000007EB
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, TimeSpan[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000058 RID: 88 RVA: 0x000025FB File Offset: 0x000007FB
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, TimeSpan[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000059 RID: 89 RVA: 0x0000260D File Offset: 0x0000080D
		public TimeSpanArrayHelperWithString()
		{
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00002615 File Offset: 0x00000815
		// Note: this type is marked as 'beforefieldinit'.
		static TimeSpanArrayHelperWithString()
		{
		}

		// Token: 0x04000015 RID: 21
		public static readonly TimeSpanArrayHelperWithString Instance = new TimeSpanArrayHelperWithString();
	}
}
