﻿using System;

namespace System.Xml
{
	// Token: 0x0200000F RID: 15
	internal class DoubleArrayHelperWithString : ArrayHelper<string, double>
	{
		// Token: 0x06000037 RID: 55 RVA: 0x0000243B File Offset: 0x0000063B
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, double[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000038 RID: 56 RVA: 0x0000244B File Offset: 0x0000064B
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, double[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000039 RID: 57 RVA: 0x0000245D File Offset: 0x0000065D
		public DoubleArrayHelperWithString()
		{
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00002465 File Offset: 0x00000665
		// Note: this type is marked as 'beforefieldinit'.
		static DoubleArrayHelperWithString()
		{
		}

		// Token: 0x0400000D RID: 13
		public static readonly DoubleArrayHelperWithString Instance = new DoubleArrayHelperWithString();
	}
}
