﻿using System;

namespace System.Xml
{
	// Token: 0x02000008 RID: 8
	internal class Int16ArrayHelperWithDictionaryString : ArrayHelper<XmlDictionaryString, short>
	{
		// Token: 0x0600001B RID: 27 RVA: 0x000022C1 File Offset: 0x000004C1
		protected override int ReadArray(XmlDictionaryReader reader, XmlDictionaryString localName, XmlDictionaryString namespaceUri, short[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000022D1 File Offset: 0x000004D1
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, short[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x0600001D RID: 29 RVA: 0x000022E3 File Offset: 0x000004E3
		public Int16ArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000022EB File Offset: 0x000004EB
		// Note: this type is marked as 'beforefieldinit'.
		static Int16ArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x04000006 RID: 6
		public static readonly Int16ArrayHelperWithDictionaryString Instance = new Int16ArrayHelperWithDictionaryString();
	}
}
