﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace System.Xml
{
	// Token: 0x02000089 RID: 137
	internal class BufferedWrite
	{
		// Token: 0x0600073E RID: 1854 RVA: 0x0001F767 File Offset: 0x0001D967
		internal BufferedWrite() : this(256)
		{
		}

		// Token: 0x0600073F RID: 1855 RVA: 0x0001F774 File Offset: 0x0001D974
		internal BufferedWrite(int initialSize)
		{
			this.buffer = new byte[initialSize];
		}

		// Token: 0x06000740 RID: 1856 RVA: 0x0001F788 File Offset: 0x0001D988
		private void EnsureBuffer(int count)
		{
			int num = this.buffer.Length;
			if (count > num - this.offset)
			{
				int num2 = num;
				while (num2 != 2147483647)
				{
					num2 = ((num2 < 1073741823) ? (num2 * 2) : int.MaxValue);
					if (count <= num2 - this.offset)
					{
						byte[] dst = new byte[num2];
						Buffer.BlockCopy(this.buffer, 0, dst, 0, this.offset);
						this.buffer = dst;
						return;
					}
				}
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(System.Runtime.Serialization.SR.GetString("Write buffer overflow.")));
			}
		}

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000741 RID: 1857 RVA: 0x0001F808 File Offset: 0x0001DA08
		internal int Length
		{
			get
			{
				return this.offset;
			}
		}

		// Token: 0x06000742 RID: 1858 RVA: 0x0001F810 File Offset: 0x0001DA10
		internal byte[] GetBuffer()
		{
			return this.buffer;
		}

		// Token: 0x06000743 RID: 1859 RVA: 0x0001F818 File Offset: 0x0001DA18
		internal void Reset()
		{
			this.offset = 0;
		}

		// Token: 0x06000744 RID: 1860 RVA: 0x0001F821 File Offset: 0x0001DA21
		internal void Write(byte[] value)
		{
			this.Write(value, 0, value.Length);
		}

		// Token: 0x06000745 RID: 1861 RVA: 0x0001F82E File Offset: 0x0001DA2E
		internal void Write(byte[] value, int index, int count)
		{
			this.EnsureBuffer(count);
			Buffer.BlockCopy(value, index, this.buffer, this.offset, count);
			this.offset += count;
		}

		// Token: 0x06000746 RID: 1862 RVA: 0x0001F859 File Offset: 0x0001DA59
		internal void Write(string value)
		{
			this.Write(value, 0, value.Length);
		}

		// Token: 0x06000747 RID: 1863 RVA: 0x0001F86C File Offset: 0x0001DA6C
		internal void Write(string value, int index, int count)
		{
			this.EnsureBuffer(count);
			for (int i = 0; i < count; i++)
			{
				char c = value[index + i];
				if (c > 'ÿ')
				{
					string name = "MIME header has an invalid character ('{0}', {1} in hexadecimal value).";
					object[] array = new object[2];
					array[0] = c;
					int num = 1;
					int num2 = (int)c;
					array[num] = num2.ToString("X", CultureInfo.InvariantCulture);
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new FormatException(System.Runtime.Serialization.SR.GetString(name, array)));
				}
				this.buffer[this.offset + i] = (byte)c;
			}
			this.offset += count;
		}

		// Token: 0x04000366 RID: 870
		private byte[] buffer;

		// Token: 0x04000367 RID: 871
		private int offset;
	}
}
