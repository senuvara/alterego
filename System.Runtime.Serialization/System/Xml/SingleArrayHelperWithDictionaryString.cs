﻿using System;

namespace System.Xml
{
	// Token: 0x0200000E RID: 14
	internal class SingleArrayHelperWithDictionaryString : ArrayHelper<XmlDictionaryString, float>
	{
		// Token: 0x06000033 RID: 51 RVA: 0x00002405 File Offset: 0x00000605
		protected override int ReadArray(XmlDictionaryReader reader, XmlDictionaryString localName, XmlDictionaryString namespaceUri, float[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002415 File Offset: 0x00000615
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, float[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002427 File Offset: 0x00000627
		public SingleArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x06000036 RID: 54 RVA: 0x0000242F File Offset: 0x0000062F
		// Note: this type is marked as 'beforefieldinit'.
		static SingleArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x0400000C RID: 12
		public static readonly SingleArrayHelperWithDictionaryString Instance = new SingleArrayHelperWithDictionaryString();
	}
}
