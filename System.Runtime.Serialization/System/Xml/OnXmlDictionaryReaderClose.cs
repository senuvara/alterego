﻿using System;

namespace System.Xml
{
	/// <summary>
	///     <see langword="delegate" /> for a callback method when closing the reader.</summary>
	/// <param name="reader">The <see cref="T:System.Xml.XmlDictionaryReader" /> that fires the OnClose event.</param>
	// Token: 0x0200005E RID: 94
	// (Invoke) Token: 0x06000470 RID: 1136
	public delegate void OnXmlDictionaryReaderClose(XmlDictionaryReader reader);
}
