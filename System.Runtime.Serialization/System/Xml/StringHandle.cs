﻿using System;

namespace System.Xml
{
	// Token: 0x02000021 RID: 33
	internal class StringHandle
	{
		// Token: 0x060000AD RID: 173 RVA: 0x00003A76 File Offset: 0x00001C76
		public StringHandle(XmlBufferReader bufferReader)
		{
			this.bufferReader = bufferReader;
			this.SetValue(0, 0);
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00003A8D File Offset: 0x00001C8D
		public void SetValue(int offset, int length)
		{
			this.type = StringHandle.StringHandleType.UTF8;
			this.offset = offset;
			this.length = length;
		}

		// Token: 0x060000AF RID: 175 RVA: 0x00003AA4 File Offset: 0x00001CA4
		public void SetConstantValue(StringHandleConstStringType constStringType)
		{
			this.type = StringHandle.StringHandleType.ConstString;
			this.key = (int)constStringType;
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00003AB4 File Offset: 0x00001CB4
		public void SetValue(int offset, int length, bool escaped)
		{
			this.type = (escaped ? StringHandle.StringHandleType.EscapedUTF8 : StringHandle.StringHandleType.UTF8);
			this.offset = offset;
			this.length = length;
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00003AD1 File Offset: 0x00001CD1
		public void SetValue(int key)
		{
			this.type = StringHandle.StringHandleType.Dictionary;
			this.key = key;
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00003AE1 File Offset: 0x00001CE1
		public void SetValue(StringHandle value)
		{
			this.type = value.type;
			this.key = value.key;
			this.offset = value.offset;
			this.length = value.length;
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x060000B3 RID: 179 RVA: 0x00003B13 File Offset: 0x00001D13
		public bool IsEmpty
		{
			get
			{
				if (this.type == StringHandle.StringHandleType.UTF8)
				{
					return this.length == 0;
				}
				return this.Equals2(string.Empty);
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x060000B4 RID: 180 RVA: 0x00003B34 File Offset: 0x00001D34
		public bool IsXmlns
		{
			get
			{
				if (this.type != StringHandle.StringHandleType.UTF8)
				{
					return this.Equals2("xmlns");
				}
				if (this.length != 5)
				{
					return false;
				}
				byte[] buffer = this.bufferReader.Buffer;
				int num = this.offset;
				return buffer[num] == 120 && buffer[num + 1] == 109 && buffer[num + 2] == 108 && buffer[num + 3] == 110 && buffer[num + 4] == 115;
			}
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x00003BA1 File Offset: 0x00001DA1
		public void ToPrefixHandle(PrefixHandle prefix)
		{
			prefix.SetValue(this.offset, this.length);
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00003BB8 File Offset: 0x00001DB8
		public string GetString(XmlNameTable nameTable)
		{
			StringHandle.StringHandleType stringHandleType = this.type;
			if (stringHandleType == StringHandle.StringHandleType.UTF8)
			{
				return this.bufferReader.GetString(this.offset, this.length, nameTable);
			}
			if (stringHandleType == StringHandle.StringHandleType.Dictionary)
			{
				return nameTable.Add(this.bufferReader.GetDictionaryString(this.key).Value);
			}
			if (stringHandleType == StringHandle.StringHandleType.ConstString)
			{
				return nameTable.Add(StringHandle.constStrings[this.key]);
			}
			return this.bufferReader.GetEscapedString(this.offset, this.length, nameTable);
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00003C38 File Offset: 0x00001E38
		public string GetString()
		{
			StringHandle.StringHandleType stringHandleType = this.type;
			if (stringHandleType == StringHandle.StringHandleType.UTF8)
			{
				return this.bufferReader.GetString(this.offset, this.length);
			}
			if (stringHandleType == StringHandle.StringHandleType.Dictionary)
			{
				return this.bufferReader.GetDictionaryString(this.key).Value;
			}
			if (stringHandleType == StringHandle.StringHandleType.ConstString)
			{
				return StringHandle.constStrings[this.key];
			}
			return this.bufferReader.GetEscapedString(this.offset, this.length);
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00003CAC File Offset: 0x00001EAC
		public byte[] GetString(out int offset, out int length)
		{
			StringHandle.StringHandleType stringHandleType = this.type;
			if (stringHandleType == StringHandle.StringHandleType.UTF8)
			{
				offset = this.offset;
				length = this.length;
				return this.bufferReader.Buffer;
			}
			if (stringHandleType == StringHandle.StringHandleType.Dictionary)
			{
				byte[] array = this.bufferReader.GetDictionaryString(this.key).ToUTF8();
				offset = 0;
				length = array.Length;
				return array;
			}
			if (stringHandleType == StringHandle.StringHandleType.ConstString)
			{
				byte[] array2 = XmlConverter.ToBytes(StringHandle.constStrings[this.key]);
				offset = 0;
				length = array2.Length;
				return array2;
			}
			byte[] array3 = XmlConverter.ToBytes(this.bufferReader.GetEscapedString(this.offset, this.length));
			offset = 0;
			length = array3.Length;
			return array3;
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00003D4A File Offset: 0x00001F4A
		public bool TryGetDictionaryString(out XmlDictionaryString value)
		{
			if (this.type == StringHandle.StringHandleType.Dictionary)
			{
				value = this.bufferReader.GetDictionaryString(this.key);
				return true;
			}
			if (this.IsEmpty)
			{
				value = XmlDictionaryString.Empty;
				return true;
			}
			value = null;
			return false;
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00003D7E File Offset: 0x00001F7E
		public override string ToString()
		{
			return this.GetString();
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00003D88 File Offset: 0x00001F88
		private bool Equals2(int key2, XmlBufferReader bufferReader2)
		{
			StringHandle.StringHandleType stringHandleType = this.type;
			if (stringHandleType == StringHandle.StringHandleType.Dictionary)
			{
				return this.bufferReader.Equals2(this.key, key2, bufferReader2);
			}
			if (stringHandleType == StringHandle.StringHandleType.UTF8)
			{
				return this.bufferReader.Equals2(this.offset, this.length, bufferReader2.GetDictionaryString(key2).Value);
			}
			return this.GetString() == this.bufferReader.GetDictionaryString(key2).Value;
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00003DF8 File Offset: 0x00001FF8
		private bool Equals2(XmlDictionaryString xmlString2)
		{
			StringHandle.StringHandleType stringHandleType = this.type;
			if (stringHandleType == StringHandle.StringHandleType.Dictionary)
			{
				return this.bufferReader.Equals2(this.key, xmlString2);
			}
			if (stringHandleType == StringHandle.StringHandleType.UTF8)
			{
				return this.bufferReader.Equals2(this.offset, this.length, xmlString2.ToUTF8());
			}
			return this.GetString() == xmlString2.Value;
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00003E58 File Offset: 0x00002058
		private bool Equals2(string s2)
		{
			StringHandle.StringHandleType stringHandleType = this.type;
			if (stringHandleType == StringHandle.StringHandleType.Dictionary)
			{
				return this.bufferReader.GetDictionaryString(this.key).Value == s2;
			}
			if (stringHandleType == StringHandle.StringHandleType.UTF8)
			{
				return this.bufferReader.Equals2(this.offset, this.length, s2);
			}
			return this.GetString() == s2;
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00003EB8 File Offset: 0x000020B8
		private bool Equals2(int offset2, int length2, XmlBufferReader bufferReader2)
		{
			StringHandle.StringHandleType stringHandleType = this.type;
			if (stringHandleType == StringHandle.StringHandleType.Dictionary)
			{
				return bufferReader2.Equals2(offset2, length2, this.bufferReader.GetDictionaryString(this.key).Value);
			}
			if (stringHandleType == StringHandle.StringHandleType.UTF8)
			{
				return this.bufferReader.Equals2(this.offset, this.length, bufferReader2, offset2, length2);
			}
			return this.GetString() == this.bufferReader.GetString(offset2, length2);
		}

		// Token: 0x060000BF RID: 191 RVA: 0x00003F28 File Offset: 0x00002128
		private bool Equals2(StringHandle s2)
		{
			StringHandle.StringHandleType stringHandleType = s2.type;
			if (stringHandleType == StringHandle.StringHandleType.Dictionary)
			{
				return this.Equals2(s2.key, s2.bufferReader);
			}
			if (stringHandleType == StringHandle.StringHandleType.UTF8)
			{
				return this.Equals2(s2.offset, s2.length, s2.bufferReader);
			}
			return this.Equals2(s2.GetString());
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00003F7B File Offset: 0x0000217B
		public static bool operator ==(StringHandle s1, XmlDictionaryString xmlString2)
		{
			return s1.Equals2(xmlString2);
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00003F84 File Offset: 0x00002184
		public static bool operator !=(StringHandle s1, XmlDictionaryString xmlString2)
		{
			return !s1.Equals2(xmlString2);
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x00003F90 File Offset: 0x00002190
		public static bool operator ==(StringHandle s1, string s2)
		{
			return s1.Equals2(s2);
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00003F99 File Offset: 0x00002199
		public static bool operator !=(StringHandle s1, string s2)
		{
			return !s1.Equals2(s2);
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00003FA5 File Offset: 0x000021A5
		public static bool operator ==(StringHandle s1, StringHandle s2)
		{
			return s1.Equals2(s2);
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x00003FAE File Offset: 0x000021AE
		public static bool operator !=(StringHandle s1, StringHandle s2)
		{
			return !s1.Equals2(s2);
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00003FBC File Offset: 0x000021BC
		public int CompareTo(StringHandle that)
		{
			if (this.type == StringHandle.StringHandleType.UTF8 && that.type == StringHandle.StringHandleType.UTF8)
			{
				return this.bufferReader.Compare(this.offset, this.length, that.offset, that.length);
			}
			return string.Compare(this.GetString(), that.GetString(), StringComparison.Ordinal);
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x00004014 File Offset: 0x00002214
		public override bool Equals(object obj)
		{
			StringHandle stringHandle = obj as StringHandle;
			return stringHandle != null && this == stringHandle;
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00004034 File Offset: 0x00002234
		public override int GetHashCode()
		{
			return this.GetString().GetHashCode();
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00004041 File Offset: 0x00002241
		// Note: this type is marked as 'beforefieldinit'.
		static StringHandle()
		{
		}

		// Token: 0x0400005B RID: 91
		private XmlBufferReader bufferReader;

		// Token: 0x0400005C RID: 92
		private StringHandle.StringHandleType type;

		// Token: 0x0400005D RID: 93
		private int key;

		// Token: 0x0400005E RID: 94
		private int offset;

		// Token: 0x0400005F RID: 95
		private int length;

		// Token: 0x04000060 RID: 96
		private static string[] constStrings = new string[]
		{
			"type",
			"root",
			"item"
		};

		// Token: 0x02000022 RID: 34
		private enum StringHandleType
		{
			// Token: 0x04000062 RID: 98
			Dictionary,
			// Token: 0x04000063 RID: 99
			UTF8,
			// Token: 0x04000064 RID: 100
			EscapedUTF8,
			// Token: 0x04000065 RID: 101
			ConstString
		}
	}
}
