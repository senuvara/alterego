﻿using System;
using System.IO;
using System.Text;

namespace System.Xml
{
	/// <summary>Specifies implementation requirements for XML text writers that derive from this interface.</summary>
	// Token: 0x0200009B RID: 155
	public interface IXmlTextWriterInitializer
	{
		/// <summary>Specifies initialization requirements for XML text writers that implement this method.</summary>
		/// <param name="stream">The stream to write to.</param>
		/// <param name="encoding">The character encoding of the stream.</param>
		/// <param name="ownsStream">If <see langword="true" />, stream is closed by the writer when done; otherwise <see langword="false" />.</param>
		// Token: 0x06000849 RID: 2121
		void SetOutput(Stream stream, Encoding encoding, bool ownsStream);
	}
}
