﻿using System;

namespace System.Xml
{
	// Token: 0x02000018 RID: 24
	internal class TimeSpanArrayHelperWithDictionaryString : ArrayHelper<XmlDictionaryString, TimeSpan>
	{
		// Token: 0x0600005B RID: 91 RVA: 0x00002621 File Offset: 0x00000821
		protected override int ReadArray(XmlDictionaryReader reader, XmlDictionaryString localName, XmlDictionaryString namespaceUri, TimeSpan[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00002631 File Offset: 0x00000831
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, TimeSpan[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00002643 File Offset: 0x00000843
		public TimeSpanArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x0600005E RID: 94 RVA: 0x0000264B File Offset: 0x0000084B
		// Note: this type is marked as 'beforefieldinit'.
		static TimeSpanArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x04000016 RID: 22
		public static readonly TimeSpanArrayHelperWithDictionaryString Instance = new TimeSpanArrayHelperWithDictionaryString();
	}
}
