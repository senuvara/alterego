﻿using System;

namespace System.Xml
{
	// Token: 0x0200000C RID: 12
	internal class Int64ArrayHelperWithDictionaryString : ArrayHelper<XmlDictionaryString, long>
	{
		// Token: 0x0600002B RID: 43 RVA: 0x00002399 File Offset: 0x00000599
		protected override int ReadArray(XmlDictionaryReader reader, XmlDictionaryString localName, XmlDictionaryString namespaceUri, long[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x0600002C RID: 44 RVA: 0x000023A9 File Offset: 0x000005A9
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, long[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x0600002D RID: 45 RVA: 0x000023BB File Offset: 0x000005BB
		public Int64ArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x0600002E RID: 46 RVA: 0x000023C3 File Offset: 0x000005C3
		// Note: this type is marked as 'beforefieldinit'.
		static Int64ArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x0400000A RID: 10
		public static readonly Int64ArrayHelperWithDictionaryString Instance = new Int64ArrayHelperWithDictionaryString();
	}
}
