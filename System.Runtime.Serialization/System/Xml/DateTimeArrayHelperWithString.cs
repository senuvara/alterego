﻿using System;

namespace System.Xml
{
	// Token: 0x02000013 RID: 19
	internal class DateTimeArrayHelperWithString : ArrayHelper<string, DateTime>
	{
		// Token: 0x06000047 RID: 71 RVA: 0x00002513 File Offset: 0x00000713
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, DateTime[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00002523 File Offset: 0x00000723
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, DateTime[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002535 File Offset: 0x00000735
		public DateTimeArrayHelperWithString()
		{
		}

		// Token: 0x0600004A RID: 74 RVA: 0x0000253D File Offset: 0x0000073D
		// Note: this type is marked as 'beforefieldinit'.
		static DateTimeArrayHelperWithString()
		{
		}

		// Token: 0x04000011 RID: 17
		public static readonly DateTimeArrayHelperWithString Instance = new DateTimeArrayHelperWithString();
	}
}
