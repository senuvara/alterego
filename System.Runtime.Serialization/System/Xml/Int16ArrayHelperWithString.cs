﻿using System;

namespace System.Xml
{
	// Token: 0x02000007 RID: 7
	internal class Int16ArrayHelperWithString : ArrayHelper<string, short>
	{
		// Token: 0x06000017 RID: 23 RVA: 0x0000228B File Offset: 0x0000048B
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, short[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000018 RID: 24 RVA: 0x0000229B File Offset: 0x0000049B
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, short[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000019 RID: 25 RVA: 0x000022AD File Offset: 0x000004AD
		public Int16ArrayHelperWithString()
		{
		}

		// Token: 0x0600001A RID: 26 RVA: 0x000022B5 File Offset: 0x000004B5
		// Note: this type is marked as 'beforefieldinit'.
		static Int16ArrayHelperWithString()
		{
		}

		// Token: 0x04000005 RID: 5
		public static readonly Int16ArrayHelperWithString Instance = new Int16ArrayHelperWithString();
	}
}
