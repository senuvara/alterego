﻿using System;

namespace System.Xml
{
	// Token: 0x0200008A RID: 138
	internal enum MtomBinaryDataType
	{
		// Token: 0x04000369 RID: 873
		Provider,
		// Token: 0x0400036A RID: 874
		Segment
	}
}
