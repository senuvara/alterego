﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace System.Xml
{
	/// <summary>An <see langword="abstract" /> class that the Windows Communication Foundation (WCF) derives from <see cref="T:System.Xml.XmlReader" /> to do serialization and deserialization.</summary>
	// Token: 0x0200005F RID: 95
	public abstract class XmlDictionaryReader : XmlReader
	{
		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> from an existing <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="reader">An instance of <see cref="T:System.Xml.XmlReader" />.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="reader" /> is <see langword="null" />.</exception>
		// Token: 0x06000473 RID: 1139 RVA: 0x00016C8C File Offset: 0x00014E8C
		public static XmlDictionaryReader CreateDictionaryReader(XmlReader reader)
		{
			if (reader == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("reader");
			}
			XmlDictionaryReader xmlDictionaryReader = reader as XmlDictionaryReader;
			if (xmlDictionaryReader == null)
			{
				xmlDictionaryReader = new XmlDictionaryReader.XmlWrappedReader(reader, null);
			}
			return xmlDictionaryReader;
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that can read .NET Binary XML Format.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="quotas">The quotas that apply to this operation.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		// Token: 0x06000474 RID: 1140 RVA: 0x00016CBA File Offset: 0x00014EBA
		public static XmlDictionaryReader CreateBinaryReader(byte[] buffer, XmlDictionaryReaderQuotas quotas)
		{
			if (buffer == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("buffer");
			}
			return XmlDictionaryReader.CreateBinaryReader(buffer, 0, buffer.Length, quotas);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that can read .NET Binary XML Format.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <param name="quotas">The quotas that apply to this operation.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is less than zero or greater than the buffer length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is less than zero or greater than the buffer length minus the offset.</exception>
		// Token: 0x06000475 RID: 1141 RVA: 0x00016CD5 File Offset: 0x00014ED5
		public static XmlDictionaryReader CreateBinaryReader(byte[] buffer, int offset, int count, XmlDictionaryReaderQuotas quotas)
		{
			return XmlDictionaryReader.CreateBinaryReader(buffer, offset, count, null, quotas);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that can read .NET Binary XML Format.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <param name="dictionary">
		///       <see cref="T:System.Xml.XmlDictionary" /> to use.</param>
		/// <param name="quotas">The quotas that apply to this operation.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is less than zero or greater than the buffer length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is less than zero or greater than the buffer length minus the offset.</exception>
		// Token: 0x06000476 RID: 1142 RVA: 0x00016CE1 File Offset: 0x00014EE1
		public static XmlDictionaryReader CreateBinaryReader(byte[] buffer, int offset, int count, IXmlDictionary dictionary, XmlDictionaryReaderQuotas quotas)
		{
			return XmlDictionaryReader.CreateBinaryReader(buffer, offset, count, dictionary, quotas, null);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that can read .NET Binary XML Format.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <param name="dictionary">The <see cref="T:System.Xml.XmlDictionary" /> to use.</param>
		/// <param name="quotas">The <see cref="T:System.Xml.XmlDictionaryReaderQuotas" /> to apply.</param>
		/// <param name="session">The <see cref="T:System.Xml.XmlBinaryReaderSession" /> to use.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is less than zero or greater than the buffer length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is less than zero or greater than the buffer length minus the offset.</exception>
		// Token: 0x06000477 RID: 1143 RVA: 0x00016CEF File Offset: 0x00014EEF
		public static XmlDictionaryReader CreateBinaryReader(byte[] buffer, int offset, int count, IXmlDictionary dictionary, XmlDictionaryReaderQuotas quotas, XmlBinaryReaderSession session)
		{
			return XmlDictionaryReader.CreateBinaryReader(buffer, offset, count, dictionary, quotas, session, null);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that can read .NET Binary XML Format.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <param name="dictionary">The <see cref="T:System.Xml.XmlDictionary" /> to use.</param>
		/// <param name="quotas">The <see cref="T:System.Xml.XmlDictionaryReaderQuotas" /> to apply.</param>
		/// <param name="session">The <see cref="T:System.Xml.XmlBinaryReaderSession" /> to use.</param>
		/// <param name="onClose">Delegate to be called when the reader is closed.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is less than zero or greater than the buffer length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is less than zero or greater than the buffer length minus the offset.</exception>
		// Token: 0x06000478 RID: 1144 RVA: 0x00016D00 File Offset: 0x00014F00
		public static XmlDictionaryReader CreateBinaryReader(byte[] buffer, int offset, int count, IXmlDictionary dictionary, XmlDictionaryReaderQuotas quotas, XmlBinaryReaderSession session, OnXmlDictionaryReaderClose onClose)
		{
			XmlBinaryReader xmlBinaryReader = new XmlBinaryReader();
			xmlBinaryReader.SetInput(buffer, offset, count, dictionary, quotas, session, onClose);
			return xmlBinaryReader;
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that can read .NET Binary XML Format.</summary>
		/// <param name="stream">The stream from which to read.</param>
		/// <param name="quotas">The quotas that apply to this operation.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		// Token: 0x06000479 RID: 1145 RVA: 0x00016D22 File Offset: 0x00014F22
		public static XmlDictionaryReader CreateBinaryReader(Stream stream, XmlDictionaryReaderQuotas quotas)
		{
			return XmlDictionaryReader.CreateBinaryReader(stream, null, quotas);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that can read .NET Binary XML Format.</summary>
		/// <param name="stream">The stream from which to read.</param>
		/// <param name="dictionary">
		///       <see cref="T:System.Xml.XmlDictionary" /> to use.</param>
		/// <param name="quotas">The quotas that apply to this operation.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> or <paramref name="quotas" /> is <see langword="null" />.</exception>
		// Token: 0x0600047A RID: 1146 RVA: 0x00016D2C File Offset: 0x00014F2C
		public static XmlDictionaryReader CreateBinaryReader(Stream stream, IXmlDictionary dictionary, XmlDictionaryReaderQuotas quotas)
		{
			return XmlDictionaryReader.CreateBinaryReader(stream, dictionary, quotas, null);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that can read .NET Binary XML Format.</summary>
		/// <param name="stream">The stream from which to read.</param>
		/// <param name="dictionary">
		///       <see cref="T:System.Xml.XmlDictionary" /> to use.</param>
		/// <param name="quotas">The quotas that apply to this operation.</param>
		/// <param name="session">
		///       <see cref="T:System.Xml.XmlBinaryReaderSession" /> to use.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		// Token: 0x0600047B RID: 1147 RVA: 0x00016D37 File Offset: 0x00014F37
		public static XmlDictionaryReader CreateBinaryReader(Stream stream, IXmlDictionary dictionary, XmlDictionaryReaderQuotas quotas, XmlBinaryReaderSession session)
		{
			return XmlDictionaryReader.CreateBinaryReader(stream, dictionary, quotas, session, null);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that can read .NET Binary XML Format.</summary>
		/// <param name="stream">The stream from which to read.</param>
		/// <param name="dictionary">
		///       <see cref="T:System.Xml.XmlDictionary" /> to use.</param>
		/// <param name="quotas">
		///       <see cref="T:System.Xml.XmlDictionaryReaderQuotas" /> to apply.</param>
		/// <param name="session">
		///       <see cref="T:System.Xml.XmlBinaryReaderSession" /> to use.</param>
		/// <param name="onClose">Delegate to be called when the reader is closed.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		// Token: 0x0600047C RID: 1148 RVA: 0x00016D43 File Offset: 0x00014F43
		public static XmlDictionaryReader CreateBinaryReader(Stream stream, IXmlDictionary dictionary, XmlDictionaryReaderQuotas quotas, XmlBinaryReaderSession session, OnXmlDictionaryReaderClose onClose)
		{
			XmlBinaryReader xmlBinaryReader = new XmlBinaryReader();
			xmlBinaryReader.SetInput(stream, dictionary, quotas, session, onClose);
			return xmlBinaryReader;
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="quotas">The quotas applied to the reader.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		// Token: 0x0600047D RID: 1149 RVA: 0x00016D56 File Offset: 0x00014F56
		public static XmlDictionaryReader CreateTextReader(byte[] buffer, XmlDictionaryReaderQuotas quotas)
		{
			if (buffer == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("buffer");
			}
			return XmlDictionaryReader.CreateTextReader(buffer, 0, buffer.Length, quotas);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <param name="quotas">The quotas applied to the reader.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		// Token: 0x0600047E RID: 1150 RVA: 0x00016D71 File Offset: 0x00014F71
		public static XmlDictionaryReader CreateTextReader(byte[] buffer, int offset, int count, XmlDictionaryReaderQuotas quotas)
		{
			return XmlDictionaryReader.CreateTextReader(buffer, offset, count, null, quotas, null);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <param name="encoding">The <see cref="T:System.Text.Encoding" /> object that specifies the encoding properties to apply.</param>
		/// <param name="quotas">The <see cref="T:System.Xml.XmlDictionaryReaderQuotas" /> to apply.</param>
		/// <param name="onClose">The delegate to be called when the reader is closed.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		// Token: 0x0600047F RID: 1151 RVA: 0x00016D7E File Offset: 0x00014F7E
		public static XmlDictionaryReader CreateTextReader(byte[] buffer, int offset, int count, Encoding encoding, XmlDictionaryReaderQuotas quotas, OnXmlDictionaryReaderClose onClose)
		{
			XmlUTF8TextReader xmlUTF8TextReader = new XmlUTF8TextReader();
			xmlUTF8TextReader.SetInput(buffer, offset, count, encoding, quotas, onClose);
			return xmlUTF8TextReader;
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</summary>
		/// <param name="stream">The stream from which to read.</param>
		/// <param name="quotas">The quotas applied to the reader.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		// Token: 0x06000480 RID: 1152 RVA: 0x00016D93 File Offset: 0x00014F93
		public static XmlDictionaryReader CreateTextReader(Stream stream, XmlDictionaryReaderQuotas quotas)
		{
			return XmlDictionaryReader.CreateTextReader(stream, null, quotas, null);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</summary>
		/// <param name="stream">The stream from which to read.</param>
		/// <param name="encoding">The <see cref="T:System.Text.Encoding" /> object that specifies the encoding properties to apply.</param>
		/// <param name="quotas">The <see cref="T:System.Xml.XmlDictionaryReaderQuotas" /> to apply.</param>
		/// <param name="onClose">The delegate to be called when the reader is closed.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		// Token: 0x06000481 RID: 1153 RVA: 0x00016D9E File Offset: 0x00014F9E
		public static XmlDictionaryReader CreateTextReader(Stream stream, Encoding encoding, XmlDictionaryReaderQuotas quotas, OnXmlDictionaryReaderClose onClose)
		{
			XmlUTF8TextReader xmlUTF8TextReader = new XmlUTF8TextReader();
			xmlUTF8TextReader.SetInput(stream, encoding, quotas, onClose);
			return xmlUTF8TextReader;
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that reads XML in the MTOM format.</summary>
		/// <param name="stream">The stream from which to read.</param>
		/// <param name="encoding">The possible character encoding of the stream.</param>
		/// <param name="quotas">The quotas to apply to this reader.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="encoding" /> is <see langword="null" />.</exception>
		// Token: 0x06000482 RID: 1154 RVA: 0x00016DAF File Offset: 0x00014FAF
		public static XmlDictionaryReader CreateMtomReader(Stream stream, Encoding encoding, XmlDictionaryReaderQuotas quotas)
		{
			if (encoding == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("encoding");
			}
			return XmlDictionaryReader.CreateMtomReader(stream, new Encoding[]
			{
				encoding
			}, quotas);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that reads XML in the MTOM format.</summary>
		/// <param name="stream">The stream from which to read.</param>
		/// <param name="encodings">The possible character encodings of the stream.</param>
		/// <param name="quotas">The quotas to apply to this reader.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="encoding" /> is <see langword="null" />.</exception>
		// Token: 0x06000483 RID: 1155 RVA: 0x00016DD0 File Offset: 0x00014FD0
		public static XmlDictionaryReader CreateMtomReader(Stream stream, Encoding[] encodings, XmlDictionaryReaderQuotas quotas)
		{
			return XmlDictionaryReader.CreateMtomReader(stream, encodings, null, quotas);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that reads XML in the MTOM format.</summary>
		/// <param name="stream">The stream from which to read.</param>
		/// <param name="encodings">The possible character encodings of the stream.</param>
		/// <param name="contentType">The Content-Type MIME type of the message.</param>
		/// <param name="quotas">The quotas to apply to this reader.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		// Token: 0x06000484 RID: 1156 RVA: 0x00016DDB File Offset: 0x00014FDB
		public static XmlDictionaryReader CreateMtomReader(Stream stream, Encoding[] encodings, string contentType, XmlDictionaryReaderQuotas quotas)
		{
			return XmlDictionaryReader.CreateMtomReader(stream, encodings, contentType, quotas, int.MaxValue, null);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that reads XML in the MTOM format.</summary>
		/// <param name="stream">The stream from which to read.</param>
		/// <param name="encodings">The possible character encodings of the stream.</param>
		/// <param name="contentType">The Content-Type MIME type of the message.</param>
		/// <param name="quotas">The MIME type of the message.</param>
		/// <param name="maxBufferSize">The <see cref="T:System.Xml.XmlDictionaryReaderQuotas" /> to apply to the reader.</param>
		/// <param name="onClose">The delegate to be called when the reader is closed.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		// Token: 0x06000485 RID: 1157 RVA: 0x00016DEC File Offset: 0x00014FEC
		public static XmlDictionaryReader CreateMtomReader(Stream stream, Encoding[] encodings, string contentType, XmlDictionaryReaderQuotas quotas, int maxBufferSize, OnXmlDictionaryReaderClose onClose)
		{
			XmlMtomReader xmlMtomReader = new XmlMtomReader();
			xmlMtomReader.SetInput(stream, encodings, contentType, quotas, maxBufferSize, onClose);
			return xmlMtomReader;
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that reads XML in the MTOM format.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <param name="encoding">The possible character encoding of the input.</param>
		/// <param name="quotas">The quotas to apply to this reader.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="encoding" /> is <see langword="null" />.</exception>
		// Token: 0x06000486 RID: 1158 RVA: 0x00016E01 File Offset: 0x00015001
		public static XmlDictionaryReader CreateMtomReader(byte[] buffer, int offset, int count, Encoding encoding, XmlDictionaryReaderQuotas quotas)
		{
			if (encoding == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("encoding");
			}
			return XmlDictionaryReader.CreateMtomReader(buffer, offset, count, new Encoding[]
			{
				encoding
			}, quotas);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that reads XML in the MTOM format.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <param name="encodings">The possible character encodings of the input.</param>
		/// <param name="quotas">The quotas to apply to this reader.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		// Token: 0x06000487 RID: 1159 RVA: 0x00016E25 File Offset: 0x00015025
		public static XmlDictionaryReader CreateMtomReader(byte[] buffer, int offset, int count, Encoding[] encodings, XmlDictionaryReaderQuotas quotas)
		{
			return XmlDictionaryReader.CreateMtomReader(buffer, offset, count, encodings, null, quotas);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that reads XML in the MTOM format.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <param name="encodings">The possible character encodings of the input.</param>
		/// <param name="contentType">The Content-Type MIME type of the message.</param>
		/// <param name="quotas">The quotas to apply to this reader.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		// Token: 0x06000488 RID: 1160 RVA: 0x00016E33 File Offset: 0x00015033
		public static XmlDictionaryReader CreateMtomReader(byte[] buffer, int offset, int count, Encoding[] encodings, string contentType, XmlDictionaryReaderQuotas quotas)
		{
			return XmlDictionaryReader.CreateMtomReader(buffer, offset, count, encodings, contentType, quotas, int.MaxValue, null);
		}

		/// <summary>Creates an instance of <see cref="T:System.Xml.XmlDictionaryReader" /> that reads XML in the MTOM format.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <param name="encodings">The possible character encodings of the input.</param>
		/// <param name="contentType">The Content-Type MIME type of the message.</param>
		/// <param name="quotas">The <see cref="T:System.Xml.XmlDictionaryReaderQuotas" /> to apply to the reader.</param>
		/// <param name="maxBufferSize">The maximum allowed size of the buffer.</param>
		/// <param name="onClose">The delegate to be called when the reader is closed.</param>
		/// <returns>An instance of <see cref="T:System.Xml.XmlDictionaryReader" />.</returns>
		// Token: 0x06000489 RID: 1161 RVA: 0x00016E48 File Offset: 0x00015048
		public static XmlDictionaryReader CreateMtomReader(byte[] buffer, int offset, int count, Encoding[] encodings, string contentType, XmlDictionaryReaderQuotas quotas, int maxBufferSize, OnXmlDictionaryReaderClose onClose)
		{
			XmlMtomReader xmlMtomReader = new XmlMtomReader();
			xmlMtomReader.SetInput(buffer, offset, count, encodings, contentType, quotas, maxBufferSize, onClose);
			return xmlMtomReader;
		}

		/// <summary>This property always returns <see langword="false" />. Its derived classes can override to return <see langword="true" /> if they support canonicalization.</summary>
		/// <returns>Returns <see langword="false" />.</returns>
		// Token: 0x17000074 RID: 116
		// (get) Token: 0x0600048A RID: 1162 RVA: 0x0000310F File Offset: 0x0000130F
		public virtual bool CanCanonicalize
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets the quota values that apply to the current instance of this class.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlDictionaryReaderQuotas" /> that applies to the current instance of this class. </returns>
		// Token: 0x17000075 RID: 117
		// (get) Token: 0x0600048B RID: 1163 RVA: 0x00016E6C File Offset: 0x0001506C
		public virtual XmlDictionaryReaderQuotas Quotas
		{
			get
			{
				return XmlDictionaryReaderQuotas.Max;
			}
		}

		/// <summary>This method is not yet implemented.</summary>
		/// <param name="stream">The stream to read from.</param>
		/// <param name="includeComments">Determines whether comments are included.</param>
		/// <param name="inclusivePrefixes">The prefixes to be included.</param>
		/// <exception cref="T:System.NotSupportedException">Always.</exception>
		// Token: 0x0600048C RID: 1164 RVA: 0x00003129 File Offset: 0x00001329
		public virtual void StartCanonicalization(Stream stream, bool includeComments, string[] inclusivePrefixes)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
		}

		/// <summary>This method is not yet implemented.</summary>
		/// <exception cref="T:System.NotSupportedException">Always.</exception>
		// Token: 0x0600048D RID: 1165 RVA: 0x00003129 File Offset: 0x00001329
		public virtual void EndCanonicalization()
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
		}

		/// <summary>Tests whether the current content node is a start element or an empty element.</summary>
		// Token: 0x0600048E RID: 1166 RVA: 0x00016E73 File Offset: 0x00015073
		public virtual void MoveToStartElement()
		{
			if (!this.IsStartElement())
			{
				XmlExceptionHelper.ThrowStartElementExpected(this);
			}
		}

		/// <summary>Tests whether the current content node is a start element or an empty element and if the <see cref="P:System.Xml.XmlReader.Name" /> property of the element matches the given argument.</summary>
		/// <param name="name">The <see cref="P:System.Xml.XmlReader.Name" /> property of the element.</param>
		// Token: 0x0600048F RID: 1167 RVA: 0x00016E83 File Offset: 0x00015083
		public virtual void MoveToStartElement(string name)
		{
			if (!this.IsStartElement(name))
			{
				XmlExceptionHelper.ThrowStartElementExpected(this, name);
			}
		}

		/// <summary>Tests whether the current content node is a start element or an empty element and if the <see cref="P:System.Xml.XmlReader.LocalName" /> and <see cref="P:System.Xml.XmlReader.NamespaceURI" /> properties of the element matches the given arguments.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		// Token: 0x06000490 RID: 1168 RVA: 0x00016E95 File Offset: 0x00015095
		public virtual void MoveToStartElement(string localName, string namespaceUri)
		{
			if (!this.IsStartElement(localName, namespaceUri))
			{
				XmlExceptionHelper.ThrowStartElementExpected(this, localName, namespaceUri);
			}
		}

		/// <summary>Tests whether the current content node is a start element or an empty element and if the <see cref="P:System.Xml.XmlReader.LocalName" /> and <see cref="P:System.Xml.XmlReader.NamespaceURI" /> properties of the element matches the given argument.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		// Token: 0x06000491 RID: 1169 RVA: 0x00016EA9 File Offset: 0x000150A9
		public virtual void MoveToStartElement(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			if (!this.IsStartElement(localName, namespaceUri))
			{
				XmlExceptionHelper.ThrowStartElementExpected(this, localName, namespaceUri);
			}
		}

		/// <summary>Checks whether the parameter, <paramref name="localName" />, is the local name of the current node.</summary>
		/// <param name="localName">The local name of the current node.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="localName" /> matches local name of the current node; otherwise <see langword="false" />.</returns>
		// Token: 0x06000492 RID: 1170 RVA: 0x00016EBD File Offset: 0x000150BD
		public virtual bool IsLocalName(string localName)
		{
			return this.LocalName == localName;
		}

		/// <summary>Checks whether the parameter, <paramref name="localName" />, is the local name of the current node.</summary>
		/// <param name="localName">An <see cref="T:System.Xml.XmlDictionaryString" /> that represents the local name of the current node.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="localName" /> matches local name of the current node; otherwise <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="localName" /> is <see langword="null" />.</exception>
		// Token: 0x06000493 RID: 1171 RVA: 0x00016ECB File Offset: 0x000150CB
		public virtual bool IsLocalName(XmlDictionaryString localName)
		{
			if (localName == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("localName");
			}
			return this.IsLocalName(localName.Value);
		}

		/// <summary>Checks whether the parameter, <paramref name="namespaceUri" />, is the namespace of the current node.</summary>
		/// <param name="namespaceUri">The namespace of current node.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="namespaceUri" /> matches namespace of the current node; otherwise <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="namespaceUri" /> is <see langword="null" />.</exception>
		// Token: 0x06000494 RID: 1172 RVA: 0x00016EE7 File Offset: 0x000150E7
		public virtual bool IsNamespaceUri(string namespaceUri)
		{
			if (namespaceUri == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("namespaceUri");
			}
			return this.NamespaceURI == namespaceUri;
		}

		/// <summary>Checks whether the parameter, <paramref name="namespaceUri" />, is the namespace of the current node.</summary>
		/// <param name="namespaceUri">Namespace of current node.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="namespaceUri" /> matches namespace of the current node; otherwise <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="namespaceUri" /> is <see langword="null" />.</exception>
		// Token: 0x06000495 RID: 1173 RVA: 0x00016F03 File Offset: 0x00015103
		public virtual bool IsNamespaceUri(XmlDictionaryString namespaceUri)
		{
			if (namespaceUri == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("namespaceUri");
			}
			return this.IsNamespaceUri(namespaceUri.Value);
		}

		/// <summary>Checks whether the current node is an element and advances the reader to the next node.</summary>
		/// <exception cref="T:System.Xml.XmlException">
		///         <see cref="M:System.Xml.XmlDictionaryReader.IsStartElement(System.Xml.XmlDictionaryString,System.Xml.XmlDictionaryString)" /> returns <see langword="false" />.</exception>
		// Token: 0x06000496 RID: 1174 RVA: 0x00016F1F File Offset: 0x0001511F
		public virtual void ReadFullStartElement()
		{
			this.MoveToStartElement();
			if (this.IsEmptyElement)
			{
				XmlExceptionHelper.ThrowFullStartElementExpected(this);
			}
			this.Read();
		}

		/// <summary>Checks whether the current node is an element with the given <paramref name="name" /> and advances the reader to the next node.</summary>
		/// <param name="name">The qualified name of the element.</param>
		/// <exception cref="T:System.Xml.XmlException">
		///         <see cref="M:System.Xml.XmlDictionaryReader.IsStartElement(System.Xml.XmlDictionaryString,System.Xml.XmlDictionaryString)" /> returns <see langword="false" />.</exception>
		// Token: 0x06000497 RID: 1175 RVA: 0x00016F3C File Offset: 0x0001513C
		public virtual void ReadFullStartElement(string name)
		{
			this.MoveToStartElement(name);
			if (this.IsEmptyElement)
			{
				XmlExceptionHelper.ThrowFullStartElementExpected(this, name);
			}
			this.Read();
		}

		/// <summary>Checks whether the current node is an element with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> and advances the reader to the next node.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <exception cref="T:System.Xml.XmlException">
		///         <see cref="M:System.Xml.XmlDictionaryReader.IsStartElement(System.Xml.XmlDictionaryString,System.Xml.XmlDictionaryString)" /> returns <see langword="false" />.</exception>
		// Token: 0x06000498 RID: 1176 RVA: 0x00016F5B File Offset: 0x0001515B
		public virtual void ReadFullStartElement(string localName, string namespaceUri)
		{
			this.MoveToStartElement(localName, namespaceUri);
			if (this.IsEmptyElement)
			{
				XmlExceptionHelper.ThrowFullStartElementExpected(this, localName, namespaceUri);
			}
			this.Read();
		}

		/// <summary>Checks whether the current node is an element with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> and advances the reader to the next node.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <exception cref="T:System.Xml.XmlException">
		///         <see cref="M:System.Xml.XmlDictionaryReader.IsStartElement(System.Xml.XmlDictionaryString,System.Xml.XmlDictionaryString)" /> returns <see langword="false" />.</exception>
		// Token: 0x06000499 RID: 1177 RVA: 0x00016F7C File Offset: 0x0001517C
		public virtual void ReadFullStartElement(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			this.MoveToStartElement(localName, namespaceUri);
			if (this.IsEmptyElement)
			{
				XmlExceptionHelper.ThrowFullStartElementExpected(this, localName, namespaceUri);
			}
			this.Read();
		}

		/// <summary>Checks whether the current node is an element with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> and advances the reader to the next node.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		// Token: 0x0600049A RID: 1178 RVA: 0x00016F9D File Offset: 0x0001519D
		public virtual void ReadStartElement(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			this.MoveToStartElement(localName, namespaceUri);
			this.Read();
		}

		/// <summary>Tests whether the first tag is a start tag or empty element tag and if the local name and namespace URI match those of the current node.</summary>
		/// <param name="localName">An <see cref="T:System.Xml.XmlDictionaryString" /> that represents the local name of the attribute.</param>
		/// <param name="namespaceUri">An <see cref="T:System.Xml.XmlDictionaryString" /> that represents the namespace of the attribute.</param>
		/// <returns>
		///     <see langword="true" /> if the first tag in the array is a start tag or empty element tag and matches <paramref name="localName" /> and <paramref name="namespaceUri" />; otherwise <see langword="false" />.</returns>
		// Token: 0x0600049B RID: 1179 RVA: 0x00016FAE File Offset: 0x000151AE
		public virtual bool IsStartElement(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return this.IsStartElement(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri));
		}

		/// <summary>Gets the index of the local name of the current node within an array of names.</summary>
		/// <param name="localNames">The string array of local names to be searched.</param>
		/// <param name="namespaceUri">The namespace of current node.</param>
		/// <returns>The index of the local name of the current node within an array of names.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="localNames" /> or any of the names in the array is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="namespaceUri" /> is <see langword="null" />.</exception>
		// Token: 0x0600049C RID: 1180 RVA: 0x00016FC4 File Offset: 0x000151C4
		public virtual int IndexOfLocalName(string[] localNames, string namespaceUri)
		{
			if (localNames == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("localNames");
			}
			if (namespaceUri == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("namespaceUri");
			}
			if (this.NamespaceURI == namespaceUri)
			{
				string localName = this.LocalName;
				for (int i = 0; i < localNames.Length; i++)
				{
					string text = localNames[i];
					if (text == null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull(string.Format(CultureInfo.InvariantCulture, "localNames[{0}]", i));
					}
					if (localName == text)
					{
						return i;
					}
				}
			}
			return -1;
		}

		/// <summary>Gets the index of the local name of the current node within an array of names.</summary>
		/// <param name="localNames">The <see cref="T:System.Xml.XmlDictionaryString" /> array of local names to be searched.</param>
		/// <param name="namespaceUri">The namespace of current node.</param>
		/// <returns>The index of the local name of the current node within an array of names.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="localNames" /> or any of the names in the array is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="namespaceUri" /> is <see langword="null" />.</exception>
		// Token: 0x0600049D RID: 1181 RVA: 0x00017040 File Offset: 0x00015240
		public virtual int IndexOfLocalName(XmlDictionaryString[] localNames, XmlDictionaryString namespaceUri)
		{
			if (localNames == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("localNames");
			}
			if (namespaceUri == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("namespaceUri");
			}
			if (this.NamespaceURI == namespaceUri.Value)
			{
				string localName = this.LocalName;
				for (int i = 0; i < localNames.Length; i++)
				{
					XmlDictionaryString xmlDictionaryString = localNames[i];
					if (xmlDictionaryString == null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull(string.Format(CultureInfo.InvariantCulture, "localNames[{0}]", i));
					}
					if (localName == xmlDictionaryString.Value)
					{
						return i;
					}
				}
			}
			return -1;
		}

		/// <summary>When overridden in a derived class, gets the value of an attribute.</summary>
		/// <param name="localName">An <see cref="T:System.Xml.XmlDictionaryString" /> that represents the local name of the attribute.</param>
		/// <param name="namespaceUri">An <see cref="T:System.Xml.XmlDictionaryString" /> that represents the namespace of the attribute.</param>
		/// <returns>The value of the attribute.</returns>
		// Token: 0x0600049E RID: 1182 RVA: 0x000170C4 File Offset: 0x000152C4
		public virtual string GetAttribute(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return this.GetAttribute(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri));
		}

		/// <summary>Not implemented in this class (it always returns <see langword="false" />). May be overridden in derived classes.</summary>
		/// <param name="length">Returns 0, unless overridden in a derived class.</param>
		/// <returns>
		///     <see langword="false" />, unless overridden in a derived class.</returns>
		// Token: 0x0600049F RID: 1183 RVA: 0x000170D8 File Offset: 0x000152D8
		public virtual bool TryGetBase64ContentLength(out int length)
		{
			length = 0;
			return false;
		}

		/// <summary>Not implemented.</summary>
		/// <param name="buffer">The buffer from which to read.</param>
		/// <param name="offset">The starting position from which to read in <paramref name="buffer" />.</param>
		/// <param name="count">The number of bytes that can be read from <paramref name="buffer" />.</param>
		/// <returns>Not implemented.</returns>
		/// <exception cref="T:System.NotSupportedException">Always.</exception>
		// Token: 0x060004A0 RID: 1184 RVA: 0x00003129 File Offset: 0x00001329
		public virtual int ReadValueAsBase64(byte[] buffer, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
		}

		/// <summary>Reads the content and returns the Base64 decoded binary bytes.</summary>
		/// <returns>A byte array that contains the Base64 decoded binary bytes.</returns>
		/// <exception cref="T:System.Xml.XmlException">The array size is greater than the MaxArrayLength quota for this reader.</exception>
		// Token: 0x060004A1 RID: 1185 RVA: 0x000170DE File Offset: 0x000152DE
		public virtual byte[] ReadContentAsBase64()
		{
			return this.ReadContentAsBase64(this.Quotas.MaxArrayLength, 65535);
		}

		// Token: 0x060004A2 RID: 1186 RVA: 0x000170F8 File Offset: 0x000152F8
		internal byte[] ReadContentAsBase64(int maxByteArrayContentLength, int maxInitialCount)
		{
			int num;
			if (this.TryGetBase64ContentLength(out num))
			{
				if (num > maxByteArrayContentLength)
				{
					XmlExceptionHelper.ThrowMaxArrayLengthExceeded(this, maxByteArrayContentLength);
				}
				if (num <= maxInitialCount)
				{
					byte[] array = new byte[num];
					int num2;
					for (int i = 0; i < num; i += num2)
					{
						num2 = this.ReadContentAsBase64(array, i, num - i);
						if (num2 == 0)
						{
							XmlExceptionHelper.ThrowBase64DataExpected(this);
						}
					}
					return array;
				}
			}
			return this.ReadContentAsBytes(true, maxByteArrayContentLength);
		}

		/// <summary>Converts a node's content to a string.</summary>
		/// <returns>The node content in a string representation.</returns>
		// Token: 0x060004A3 RID: 1187 RVA: 0x00017150 File Offset: 0x00015350
		public override string ReadContentAsString()
		{
			return this.ReadContentAsString(this.Quotas.MaxStringContentLength);
		}

		/// <summary>Converts a node's content to a string.</summary>
		/// <param name="maxStringContentLength">The maximum string length.</param>
		/// <returns>Node content in string representation.</returns>
		// Token: 0x060004A4 RID: 1188 RVA: 0x00017164 File Offset: 0x00015364
		protected string ReadContentAsString(int maxStringContentLength)
		{
			StringBuilder stringBuilder = null;
			string text = string.Empty;
			bool flag = false;
			for (;;)
			{
				switch (this.NodeType)
				{
				case XmlNodeType.Element:
				case XmlNodeType.Entity:
				case XmlNodeType.Document:
				case XmlNodeType.DocumentType:
				case XmlNodeType.DocumentFragment:
				case XmlNodeType.Notation:
				case XmlNodeType.EndElement:
					goto IL_B6;
				case XmlNodeType.Attribute:
					text = this.Value;
					break;
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
				{
					string value = this.Value;
					if (text.Length == 0)
					{
						text = value;
					}
					else
					{
						if (stringBuilder == null)
						{
							stringBuilder = new StringBuilder(text);
						}
						if (stringBuilder.Length > maxStringContentLength - value.Length)
						{
							XmlExceptionHelper.ThrowMaxStringContentLengthExceeded(this, maxStringContentLength);
						}
						stringBuilder.Append(value);
					}
					break;
				}
				case XmlNodeType.EntityReference:
					if (!this.CanResolveEntity)
					{
						goto IL_B6;
					}
					this.ResolveEntity();
					break;
				case XmlNodeType.ProcessingInstruction:
				case XmlNodeType.Comment:
				case XmlNodeType.EndEntity:
					break;
				default:
					goto IL_B6;
				}
				IL_B8:
				if (flag)
				{
					break;
				}
				if (this.AttributeCount != 0)
				{
					this.ReadAttributeValue();
					continue;
				}
				this.Read();
				continue;
				IL_B6:
				flag = true;
				goto IL_B8;
			}
			if (stringBuilder != null)
			{
				text = stringBuilder.ToString();
			}
			if (text.Length > maxStringContentLength)
			{
				XmlExceptionHelper.ThrowMaxStringContentLengthExceeded(this, maxStringContentLength);
			}
			return text;
		}

		/// <summary>Reads the contents of the current node into a string.</summary>
		/// <returns>A string that contains the contents of the current node.</returns>
		/// <exception cref="T:System.InvalidOperationException">Unable to read the contents of the current node.</exception>
		/// <exception cref="T:System.Xml.XmlException">Maximum allowed string length exceeded.</exception>
		// Token: 0x060004A5 RID: 1189 RVA: 0x00017267 File Offset: 0x00015467
		public override string ReadString()
		{
			return this.ReadString(this.Quotas.MaxStringContentLength);
		}

		/// <summary>Reads the contents of the current node into a string with a given maximum length.</summary>
		/// <param name="maxStringContentLength">Maximum allowed string length.</param>
		/// <returns>A string that contains the contents of the current node.</returns>
		/// <exception cref="T:System.InvalidOperationException">Unable to read the contents of the current node.</exception>
		/// <exception cref="T:System.Xml.XmlException">Maximum allowed string length exceeded.</exception>
		// Token: 0x060004A6 RID: 1190 RVA: 0x0001727C File Offset: 0x0001547C
		protected string ReadString(int maxStringContentLength)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return string.Empty;
			}
			if (this.NodeType != XmlNodeType.Element)
			{
				this.MoveToElement();
			}
			if (this.NodeType == XmlNodeType.Element)
			{
				if (this.IsEmptyElement)
				{
					return string.Empty;
				}
				if (!this.Read())
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(System.Runtime.Serialization.SR.GetString("The reader cannot be advanced.")));
				}
				if (this.NodeType == XmlNodeType.EndElement)
				{
					return string.Empty;
				}
			}
			StringBuilder stringBuilder = null;
			string text = string.Empty;
			while (this.IsTextNode(this.NodeType))
			{
				string value = this.Value;
				if (text.Length == 0)
				{
					text = value;
				}
				else
				{
					if (stringBuilder == null)
					{
						stringBuilder = new StringBuilder(text);
					}
					if (stringBuilder.Length > maxStringContentLength - value.Length)
					{
						XmlExceptionHelper.ThrowMaxStringContentLengthExceeded(this, maxStringContentLength);
					}
					stringBuilder.Append(value);
				}
				if (!this.Read())
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(System.Runtime.Serialization.SR.GetString("The reader cannot be advanced.")));
				}
			}
			if (stringBuilder != null)
			{
				text = stringBuilder.ToString();
			}
			if (text.Length > maxStringContentLength)
			{
				XmlExceptionHelper.ThrowMaxStringContentLengthExceeded(this, maxStringContentLength);
			}
			return text;
		}

		/// <summary>Reads the content and returns the <see langword="BinHex" /> decoded binary bytes.</summary>
		/// <returns>A byte array that contains the <see langword="BinHex" /> decoded binary bytes.</returns>
		/// <exception cref="T:System.Xml.XmlException">The array size is greater than <see cref="F:System.Int32.MaxValue" />.</exception>
		// Token: 0x060004A7 RID: 1191 RVA: 0x00017378 File Offset: 0x00015578
		public virtual byte[] ReadContentAsBinHex()
		{
			return this.ReadContentAsBinHex(this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads the content and returns the <see langword="BinHex" /> decoded binary bytes.</summary>
		/// <param name="maxByteArrayContentLength">The maximum array length.</param>
		/// <returns>A byte array that contains the <see langword="BinHex" /> decoded binary bytes.</returns>
		/// <exception cref="T:System.Xml.XmlException">The array size is greater than <paramref name="maxByteArrayContentLength" />.</exception>
		// Token: 0x060004A8 RID: 1192 RVA: 0x0001738B File Offset: 0x0001558B
		protected byte[] ReadContentAsBinHex(int maxByteArrayContentLength)
		{
			return this.ReadContentAsBytes(false, maxByteArrayContentLength);
		}

		// Token: 0x060004A9 RID: 1193 RVA: 0x00017398 File Offset: 0x00015598
		private byte[] ReadContentAsBytes(bool base64, int maxByteArrayContentLength)
		{
			byte[][] array = new byte[32][];
			int num = 384;
			int num2 = 0;
			int num3 = 0;
			byte[] array2;
			for (;;)
			{
				array2 = new byte[num];
				array[num2++] = array2;
				int i;
				int num4;
				for (i = 0; i < array2.Length; i += num4)
				{
					if (base64)
					{
						num4 = this.ReadContentAsBase64(array2, i, array2.Length - i);
					}
					else
					{
						num4 = this.ReadContentAsBinHex(array2, i, array2.Length - i);
					}
					if (num4 == 0)
					{
						break;
					}
				}
				if (num3 > maxByteArrayContentLength - i)
				{
					XmlExceptionHelper.ThrowMaxArrayLengthExceeded(this, maxByteArrayContentLength);
				}
				num3 += i;
				if (i < array2.Length)
				{
					break;
				}
				num *= 2;
			}
			array2 = new byte[num3];
			int num5 = 0;
			for (int j = 0; j < num2 - 1; j++)
			{
				Buffer.BlockCopy(array[j], 0, array2, num5, array[j].Length);
				num5 += array[j].Length;
			}
			Buffer.BlockCopy(array[num2 - 1], 0, array2, num5, num3 - num5);
			return array2;
		}

		/// <summary>Tests whether the current node is a text node.</summary>
		/// <param name="nodeType">Type of the node being tested.</param>
		/// <returns>
		///     <see langword="true" /> if the node type is <see cref="F:System.Xml.XmlNodeType.Text" />, <see cref="F:System.Xml.XmlNodeType.Whitespace" />, <see cref="F:System.Xml.XmlNodeType.SignificantWhitespace" />, <see cref="F:System.Xml.XmlNodeType.CDATA" />, or <see cref="F:System.Xml.XmlNodeType.Attribute" />; otherwise <see langword="false" />.</returns>
		// Token: 0x060004AA RID: 1194 RVA: 0x00017477 File Offset: 0x00015677
		protected bool IsTextNode(XmlNodeType nodeType)
		{
			return nodeType == XmlNodeType.Text || nodeType == XmlNodeType.Whitespace || nodeType == XmlNodeType.SignificantWhitespace || nodeType == XmlNodeType.CDATA || nodeType == XmlNodeType.Attribute;
		}

		/// <summary>Reads the content into a <see langword="char" /> array.</summary>
		/// <param name="chars">The array into which the characters are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of characters to put in the array.</param>
		/// <returns>Number of characters read.</returns>
		// Token: 0x060004AB RID: 1195 RVA: 0x00017494 File Offset: 0x00015694
		public virtual int ReadContentAsChars(char[] chars, int offset, int count)
		{
			int num = 0;
			for (;;)
			{
				XmlNodeType nodeType = this.NodeType;
				if (nodeType == XmlNodeType.Element || nodeType == XmlNodeType.EndElement)
				{
					break;
				}
				if (this.IsTextNode(nodeType))
				{
					num = this.ReadValueChunk(chars, offset, count);
					if (num > 0 || nodeType == XmlNodeType.Attribute)
					{
						break;
					}
					if (!this.Read())
					{
						break;
					}
				}
				else if (!this.Read())
				{
					break;
				}
			}
			return num;
		}

		/// <summary>Converts a node's content to a specified type.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> of the value to be returned.</param>
		/// <param name="namespaceResolver">An <see cref="T:System.Xml.IXmlNamespaceResolver" /> object that is used to resolve any namespace prefixes related to type conversion. For example, this can be used when converting an <see cref="T:System.Xml.XmlQualifiedName" /> object to an xs:string. This value can be a null reference.</param>
		/// <returns>The concatenated text content or attribute value converted to the requested type.</returns>
		// Token: 0x060004AC RID: 1196 RVA: 0x000174E4 File Offset: 0x000156E4
		public override object ReadContentAs(Type type, IXmlNamespaceResolver namespaceResolver)
		{
			if (type == typeof(Guid[]))
			{
				string[] array = (string[])this.ReadContentAs(typeof(string[]), namespaceResolver);
				Guid[] array2 = new Guid[array.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array2[i] = XmlConverter.ToGuid(array[i]);
				}
				return array2;
			}
			if (type == typeof(UniqueId[]))
			{
				string[] array3 = (string[])this.ReadContentAs(typeof(string[]), namespaceResolver);
				UniqueId[] array4 = new UniqueId[array3.Length];
				for (int j = 0; j < array3.Length; j++)
				{
					array4[j] = XmlConverter.ToUniqueId(array3[j]);
				}
				return array4;
			}
			return base.ReadContentAs(type, namespaceResolver);
		}

		/// <summary>Converts a node's content to a string.</summary>
		/// <param name="strings">The array of strings to match content against.</param>
		/// <param name="index">The index of the entry in <paramref name="strings" /> that matches the content.</param>
		/// <returns>The node content in a string representation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="strings" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">An entry in<paramref name=" strings" /> is <see langword="null" />.</exception>
		// Token: 0x060004AD RID: 1197 RVA: 0x000175A0 File Offset: 0x000157A0
		public virtual string ReadContentAsString(string[] strings, out int index)
		{
			if (strings == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("strings");
			}
			string text = this.ReadContentAsString();
			index = -1;
			for (int i = 0; i < strings.Length; i++)
			{
				string text2 = strings[i];
				if (text2 == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull(string.Format(CultureInfo.InvariantCulture, "strings[{0}]", i));
				}
				if (text2 == text)
				{
					index = i;
					return text2;
				}
			}
			return text;
		}

		/// <summary>Converts a node's content to a string.</summary>
		/// <param name="strings">The array of <see cref="T:System.Xml.XmlDictionaryString" /> objects to match content against.</param>
		/// <param name="index">The index of the entry in <paramref name="strings" /> that matches the content.</param>
		/// <returns>The node content in a string representation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="strings" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">An entry in<paramref name=" strings" /> is <see langword="null" />.</exception>
		// Token: 0x060004AE RID: 1198 RVA: 0x00017604 File Offset: 0x00015804
		public virtual string ReadContentAsString(XmlDictionaryString[] strings, out int index)
		{
			if (strings == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("strings");
			}
			string text = this.ReadContentAsString();
			index = -1;
			for (int i = 0; i < strings.Length; i++)
			{
				XmlDictionaryString xmlDictionaryString = strings[i];
				if (xmlDictionaryString == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull(string.Format(CultureInfo.InvariantCulture, "strings[{0}]", i));
				}
				if (xmlDictionaryString.Value == text)
				{
					index = i;
					return xmlDictionaryString.Value;
				}
			}
			return text;
		}

		/// <summary>Converts a node's content to <see langword="decimal" />.</summary>
		/// <returns>The <see langword="decimal" /> representation of node's content.</returns>
		// Token: 0x060004AF RID: 1199 RVA: 0x00017672 File Offset: 0x00015872
		public override decimal ReadContentAsDecimal()
		{
			return XmlConverter.ToDecimal(this.ReadContentAsString());
		}

		/// <summary>Converts a node's content to <see langword="float" />.</summary>
		/// <returns>The <see langword="float" /> representation of node's content.</returns>
		// Token: 0x060004B0 RID: 1200 RVA: 0x0001767F File Offset: 0x0001587F
		public override float ReadContentAsFloat()
		{
			return XmlConverter.ToSingle(this.ReadContentAsString());
		}

		/// <summary>Converts a node's content to a unique identifier.</summary>
		/// <returns>The node's content represented as a unique identifier.</returns>
		// Token: 0x060004B1 RID: 1201 RVA: 0x0001768C File Offset: 0x0001588C
		public virtual UniqueId ReadContentAsUniqueId()
		{
			return XmlConverter.ToUniqueId(this.ReadContentAsString());
		}

		/// <summary>Converts a node's content to <see langword="guid" />.</summary>
		/// <returns>The <see langword="guid" /> representation of node's content.</returns>
		// Token: 0x060004B2 RID: 1202 RVA: 0x00017699 File Offset: 0x00015899
		public virtual Guid ReadContentAsGuid()
		{
			return XmlConverter.ToGuid(this.ReadContentAsString());
		}

		/// <summary>Converts a node's content to <see cref="T:System.TimeSpan" />.</summary>
		/// <returns>
		///     <see cref="T:System.TimeSpan" /> representation of node's content.</returns>
		// Token: 0x060004B3 RID: 1203 RVA: 0x000176A6 File Offset: 0x000158A6
		public virtual TimeSpan ReadContentAsTimeSpan()
		{
			return XmlConverter.ToTimeSpan(this.ReadContentAsString());
		}

		/// <summary>Converts a node's content to a qualified name representation.</summary>
		/// <param name="localName">The <see cref="P:System.Xml.XmlReader.LocalName" /> part of the qualified name (<see langword="out" /> parameter).</param>
		/// <param name="namespaceUri">The <see cref="P:System.Xml.XmlReader.NamespaceURI" /> part of the qualified name (<see langword="out" /> parameter).</param>
		// Token: 0x060004B4 RID: 1204 RVA: 0x000176B4 File Offset: 0x000158B4
		public virtual void ReadContentAsQualifiedName(out string localName, out string namespaceUri)
		{
			string prefix;
			XmlConverter.ToQualifiedName(this.ReadContentAsString(), out prefix, out localName);
			namespaceUri = this.LookupNamespace(prefix);
			if (namespaceUri == null)
			{
				XmlExceptionHelper.ThrowUndefinedPrefix(this, prefix);
			}
		}

		/// <summary>Converts an element's content to a <see cref="T:System.String" />.</summary>
		/// <returns>The node's content represented as a <see cref="T:System.String" />.</returns>
		// Token: 0x060004B5 RID: 1205 RVA: 0x000176E4 File Offset: 0x000158E4
		public override string ReadElementContentAsString()
		{
			string result;
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				result = string.Empty;
			}
			else
			{
				this.ReadStartElement();
				result = this.ReadContentAsString();
				this.ReadEndElement();
			}
			return result;
		}

		/// <summary>Converts an element's content to a <see cref="T:System.Boolean" />.</summary>
		/// <returns>The node's content represented as a <see cref="T:System.Boolean" />.</returns>
		// Token: 0x060004B6 RID: 1206 RVA: 0x00017728 File Offset: 0x00015928
		public override bool ReadElementContentAsBoolean()
		{
			bool result;
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				result = XmlConverter.ToBoolean(string.Empty);
			}
			else
			{
				this.ReadStartElement();
				result = this.ReadContentAsBoolean();
				this.ReadEndElement();
			}
			return result;
		}

		/// <summary>Converts an element's content to an integer (<see cref="T:System.Int32" />).</summary>
		/// <returns>The node's content represented as an integer (<see cref="T:System.Int32" />).</returns>
		// Token: 0x060004B7 RID: 1207 RVA: 0x00017770 File Offset: 0x00015970
		public override int ReadElementContentAsInt()
		{
			int result;
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				result = XmlConverter.ToInt32(string.Empty);
			}
			else
			{
				this.ReadStartElement();
				result = this.ReadContentAsInt();
				this.ReadEndElement();
			}
			return result;
		}

		/// <summary>Converts an element's content to a long integer (<see cref="T:System.Int64" />).</summary>
		/// <returns>The node's content represented as a long integer (<see cref="T:System.Int64" />).</returns>
		// Token: 0x060004B8 RID: 1208 RVA: 0x000177B8 File Offset: 0x000159B8
		public override long ReadElementContentAsLong()
		{
			long result;
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				result = XmlConverter.ToInt64(string.Empty);
			}
			else
			{
				this.ReadStartElement();
				result = this.ReadContentAsLong();
				this.ReadEndElement();
			}
			return result;
		}

		/// <summary>Converts an element's content to a floating point number (<see cref="T:System.Single" />).</summary>
		/// <returns>The node's content represented as a floating point number (<see cref="T:System.Single" />).</returns>
		// Token: 0x060004B9 RID: 1209 RVA: 0x00017800 File Offset: 0x00015A00
		public override float ReadElementContentAsFloat()
		{
			float result;
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				result = XmlConverter.ToSingle(string.Empty);
			}
			else
			{
				this.ReadStartElement();
				result = this.ReadContentAsFloat();
				this.ReadEndElement();
			}
			return result;
		}

		/// <summary>Converts an element's content to a <see cref="T:System.Double" />.</summary>
		/// <returns>The node's content represented as a <see cref="T:System.Double" />.</returns>
		// Token: 0x060004BA RID: 1210 RVA: 0x00017848 File Offset: 0x00015A48
		public override double ReadElementContentAsDouble()
		{
			double result;
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				result = XmlConverter.ToDouble(string.Empty);
			}
			else
			{
				this.ReadStartElement();
				result = this.ReadContentAsDouble();
				this.ReadEndElement();
			}
			return result;
		}

		/// <summary>Converts an element's content to a <see cref="T:System.Decimal" />.</summary>
		/// <returns>The node's content represented as a <see cref="T:System.Decimal" />.</returns>
		// Token: 0x060004BB RID: 1211 RVA: 0x00017890 File Offset: 0x00015A90
		public override decimal ReadElementContentAsDecimal()
		{
			decimal result;
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				result = XmlConverter.ToDecimal(string.Empty);
			}
			else
			{
				this.ReadStartElement();
				result = this.ReadContentAsDecimal();
				this.ReadEndElement();
			}
			return result;
		}

		/// <summary>Converts an element's content to a <see cref="T:System.DateTime" />.</summary>
		/// <returns>The node's content represented as a <see cref="T:System.DateTime" />.</returns>
		/// <exception cref="T:System.ArgumentException">The element is not in valid format.</exception>
		/// <exception cref="T:System.FormatException">The element is not in valid format.</exception>
		// Token: 0x060004BC RID: 1212 RVA: 0x000178D8 File Offset: 0x00015AD8
		public override DateTime ReadElementContentAsDateTime()
		{
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				try
				{
					return DateTime.Parse(string.Empty, NumberFormatInfo.InvariantInfo);
				}
				catch (ArgumentException exception)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(string.Empty, "DateTime", exception));
				}
				catch (FormatException exception2)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(string.Empty, "DateTime", exception2));
				}
			}
			this.ReadStartElement();
			DateTime result = this.ReadContentAsDateTime();
			this.ReadEndElement();
			return result;
		}

		/// <summary>Converts an element's content to a unique identifier.</summary>
		/// <returns>The node's content represented as a unique identifier.</returns>
		/// <exception cref="T:System.ArgumentException">The element is not in valid format.</exception>
		/// <exception cref="T:System.FormatException">The element is not in valid format.</exception>
		// Token: 0x060004BD RID: 1213 RVA: 0x00017970 File Offset: 0x00015B70
		public virtual UniqueId ReadElementContentAsUniqueId()
		{
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				try
				{
					return new UniqueId(string.Empty);
				}
				catch (ArgumentException exception)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(string.Empty, "UniqueId", exception));
				}
				catch (FormatException exception2)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(string.Empty, "UniqueId", exception2));
				}
			}
			this.ReadStartElement();
			UniqueId result = this.ReadContentAsUniqueId();
			this.ReadEndElement();
			return result;
		}

		/// <summary>Converts an element's content to a <see cref="T:System.Guid" />.</summary>
		/// <returns>The node's content represented as a <see cref="T:System.Guid" />.</returns>
		/// <exception cref="T:System.ArgumentException">The element is not in valid format.</exception>
		/// <exception cref="T:System.FormatException">The element is not in valid format.</exception>
		// Token: 0x060004BE RID: 1214 RVA: 0x00017A04 File Offset: 0x00015C04
		public virtual Guid ReadElementContentAsGuid()
		{
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				try
				{
					return Guid.Empty;
				}
				catch (ArgumentException exception)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(string.Empty, "Guid", exception));
				}
				catch (FormatException exception2)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(string.Empty, "Guid", exception2));
				}
				catch (OverflowException exception3)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(string.Empty, "Guid", exception3));
				}
			}
			this.ReadStartElement();
			Guid result = this.ReadContentAsGuid();
			this.ReadEndElement();
			return result;
		}

		/// <summary>Converts an element's content to a <see cref="T:System.TimeSpan" />.</summary>
		/// <returns>The node's content represented as a <see cref="T:System.TimeSpan" />.</returns>
		// Token: 0x060004BF RID: 1215 RVA: 0x00017AB4 File Offset: 0x00015CB4
		public virtual TimeSpan ReadElementContentAsTimeSpan()
		{
			TimeSpan result;
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				result = XmlConverter.ToTimeSpan(string.Empty);
			}
			else
			{
				this.ReadStartElement();
				result = this.ReadContentAsTimeSpan();
				this.ReadEndElement();
			}
			return result;
		}

		/// <summary>Converts a node's content to a array of Base64 bytes.</summary>
		/// <returns>The node's content represented as an array of Base64 bytes.</returns>
		// Token: 0x060004C0 RID: 1216 RVA: 0x00017AFC File Offset: 0x00015CFC
		public virtual byte[] ReadElementContentAsBase64()
		{
			byte[] result;
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				result = new byte[0];
			}
			else
			{
				this.ReadStartElement();
				result = this.ReadContentAsBase64();
				this.ReadEndElement();
			}
			return result;
		}

		/// <summary>Converts a node's content to an array of <see langword="BinHex" /> bytes.</summary>
		/// <returns>The node's content represented as an array of <see langword="BinHex" /> bytes.</returns>
		// Token: 0x060004C1 RID: 1217 RVA: 0x00017B40 File Offset: 0x00015D40
		public virtual byte[] ReadElementContentAsBinHex()
		{
			byte[] result;
			if (this.IsStartElement() && this.IsEmptyElement)
			{
				this.Read();
				result = new byte[0];
			}
			else
			{
				this.ReadStartElement();
				result = this.ReadContentAsBinHex();
				this.ReadEndElement();
			}
			return result;
		}

		/// <summary>Gets non-atomized names.</summary>
		/// <param name="localName">The local name.</param>
		/// <param name="namespaceUri">The namespace for the local <paramref name="localName" />.</param>
		// Token: 0x060004C2 RID: 1218 RVA: 0x00017B84 File Offset: 0x00015D84
		public virtual void GetNonAtomizedNames(out string localName, out string namespaceUri)
		{
			localName = this.LocalName;
			namespaceUri = this.NamespaceURI;
		}

		/// <summary>Not implemented in this class (it always returns <see langword="false" />). May be overridden in derived classes.</summary>
		/// <param name="localName">Returns <see langword="null" />, unless overridden in a derived class. .</param>
		/// <returns>
		///     <see langword="false" />, unless overridden in a derived class.</returns>
		// Token: 0x060004C3 RID: 1219 RVA: 0x00017B96 File Offset: 0x00015D96
		public virtual bool TryGetLocalNameAsDictionaryString(out XmlDictionaryString localName)
		{
			localName = null;
			return false;
		}

		/// <summary>Not implemented in this class (it always returns <see langword="false" />). May be overridden in derived classes.</summary>
		/// <param name="namespaceUri">Returns <see langword="null" />, unless overridden in a derived class.</param>
		/// <returns>
		///     <see langword="false" />, unless overridden in a derived class.</returns>
		// Token: 0x060004C4 RID: 1220 RVA: 0x00017B96 File Offset: 0x00015D96
		public virtual bool TryGetNamespaceUriAsDictionaryString(out XmlDictionaryString namespaceUri)
		{
			namespaceUri = null;
			return false;
		}

		/// <summary>Not implemented in this class (it always returns <see langword="false" />). May be overridden in derived classes.</summary>
		/// <param name="value">Returns <see langword="null" />, unless overridden in a derived class.</param>
		/// <returns>
		///     <see langword="false" />, unless overridden in a derived class.</returns>
		// Token: 0x060004C5 RID: 1221 RVA: 0x00017B96 File Offset: 0x00015D96
		public virtual bool TryGetValueAsDictionaryString(out XmlDictionaryString value)
		{
			value = null;
			return false;
		}

		// Token: 0x060004C6 RID: 1222 RVA: 0x00017B9C File Offset: 0x00015D9C
		private void CheckArray(Array array, int offset, int count)
		{
			if (array == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentNullException("array"));
			}
			if (offset < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("offset", System.Runtime.Serialization.SR.GetString("The value of this argument must be non-negative.")));
			}
			if (offset > array.Length)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("offset", System.Runtime.Serialization.SR.GetString("The specified offset exceeds the buffer size ({0} bytes).", new object[]
				{
					array.Length
				})));
			}
			if (count < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("count", System.Runtime.Serialization.SR.GetString("The value of this argument must be non-negative.")));
			}
			if (count > array.Length - offset)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("count", System.Runtime.Serialization.SR.GetString("The specified size exceeds the remaining buffer space ({0} bytes).", new object[]
				{
					array.Length - offset
				})));
			}
		}

		/// <summary>Checks whether the reader is positioned at the start of an array. This class returns <see langword="false" />, but derived classes that have the concept of arrays might return <see langword="true" />.</summary>
		/// <param name="type">Type of the node, if a valid node; otherwise <see langword="null" />.</param>
		/// <returns>
		///     <see langword="true" /> if the reader is positioned at the start of an array node; otherwise <see langword="false" />.</returns>
		// Token: 0x060004C7 RID: 1223 RVA: 0x00017B96 File Offset: 0x00015D96
		public virtual bool IsStartArray(out Type type)
		{
			type = null;
			return false;
		}

		/// <summary>Not implemented in this class (it always returns <see langword="false" />). May be overridden in derived classes.</summary>
		/// <param name="count">Returns 0, unless overridden in a derived class.</param>
		/// <returns>
		///     <see langword="false" />, unless overridden in a derived class.</returns>
		// Token: 0x060004C8 RID: 1224 RVA: 0x000170D8 File Offset: 0x000152D8
		public virtual bool TryGetArrayLength(out int count)
		{
			count = 0;
			return false;
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.Boolean" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>A <see cref="T:System.Boolean" /> array of the <see cref="T:System.Boolean" /> nodes.</returns>
		// Token: 0x060004C9 RID: 1225 RVA: 0x00017C6A File Offset: 0x00015E6A
		public virtual bool[] ReadBooleanArray(string localName, string namespaceUri)
		{
			return BooleanArrayHelperWithString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.Boolean" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>A <see cref="T:System.Boolean" /> array of the <see cref="T:System.Boolean" /> nodes.</returns>
		// Token: 0x060004CA RID: 1226 RVA: 0x00017C84 File Offset: 0x00015E84
		public virtual bool[] ReadBooleanArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return BooleanArrayHelperWithDictionaryString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.Boolean" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The local name of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004CB RID: 1227 RVA: 0x00017CA0 File Offset: 0x00015EA0
		public virtual int ReadArray(string localName, string namespaceUri, bool[] array, int offset, int count)
		{
			this.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && this.IsStartElement(localName, namespaceUri))
			{
				array[offset + num] = this.ReadElementContentAsBoolean();
				num++;
			}
			return num;
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.Boolean" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004CC RID: 1228 RVA: 0x00017CDC File Offset: 0x00015EDC
		public virtual int ReadArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri, bool[] array, int offset, int count)
		{
			return this.ReadArray(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri), array, offset, count);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into an array of <see langword="short" /> integers (<see cref="T:System.Int16" />).</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>An array of <see langword="short" /> integers (<see cref="T:System.Int16" />).</returns>
		// Token: 0x060004CD RID: 1229 RVA: 0x00017CF5 File Offset: 0x00015EF5
		public virtual short[] ReadInt16Array(string localName, string namespaceUri)
		{
			return Int16ArrayHelperWithString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into an array of <see langword="short" /> integers (<see cref="T:System.Int16" />).</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>An array of <see langword="short" /> integers (<see cref="T:System.Int16" />).</returns>
		// Token: 0x060004CE RID: 1230 RVA: 0x00017D0F File Offset: 0x00015F0F
		public virtual short[] ReadInt16Array(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return Int16ArrayHelperWithDictionaryString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of <see langword="short" /> integers into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the integers are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of integers to put in the array.</param>
		/// <returns>The number of integers put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004CF RID: 1231 RVA: 0x00017D2C File Offset: 0x00015F2C
		public virtual int ReadArray(string localName, string namespaceUri, short[] array, int offset, int count)
		{
			this.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && this.IsStartElement(localName, namespaceUri))
			{
				int num2 = this.ReadElementContentAsInt();
				if (num2 < -32768 || num2 > 32767)
				{
					XmlExceptionHelper.ThrowConversionOverflow(this, num2.ToString(NumberFormatInfo.CurrentInfo), "Int16");
				}
				array[offset + num] = (short)num2;
				num++;
			}
			return num;
		}

		/// <summary>Reads repeated occurrences of <see langword="short" /> integers into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the integers are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of integers to put in the array.</param>
		/// <returns>The number of integers put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004D0 RID: 1232 RVA: 0x00017D92 File Offset: 0x00015F92
		public virtual int ReadArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri, short[] array, int offset, int count)
		{
			return this.ReadArray(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri), array, offset, count);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into an array of integers (<see cref="T:System.Int32" />).</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>An array of integers (<see cref="T:System.Int32" />).</returns>
		// Token: 0x060004D1 RID: 1233 RVA: 0x00017DAB File Offset: 0x00015FAB
		public virtual int[] ReadInt32Array(string localName, string namespaceUri)
		{
			return Int32ArrayHelperWithString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into an array of integers (<see cref="T:System.Int32" />).</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>An array of integers (<see cref="T:System.Int32" />).</returns>
		// Token: 0x060004D2 RID: 1234 RVA: 0x00017DC5 File Offset: 0x00015FC5
		public virtual int[] ReadInt32Array(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return Int32ArrayHelperWithDictionaryString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of integers into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the integers are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of integers to put in the array.</param>
		/// <returns>The number of integers put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004D3 RID: 1235 RVA: 0x00017DE0 File Offset: 0x00015FE0
		public virtual int ReadArray(string localName, string namespaceUri, int[] array, int offset, int count)
		{
			this.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && this.IsStartElement(localName, namespaceUri))
			{
				array[offset + num] = this.ReadElementContentAsInt();
				num++;
			}
			return num;
		}

		/// <summary>Reads repeated occurrences of integers into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the integers are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of integers to put in the array.</param>
		/// <returns>The number of integers put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004D4 RID: 1236 RVA: 0x00017E1C File Offset: 0x0001601C
		public virtual int ReadArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri, int[] array, int offset, int count)
		{
			return this.ReadArray(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri), array, offset, count);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into an array of <see langword="long" /> integers (<see cref="T:System.Int64" />).</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>An array of <see langword="long" /> integers (<see cref="T:System.Int64" />).</returns>
		// Token: 0x060004D5 RID: 1237 RVA: 0x00017E35 File Offset: 0x00016035
		public virtual long[] ReadInt64Array(string localName, string namespaceUri)
		{
			return Int64ArrayHelperWithString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into an array of <see langword="long" /> integers (<see cref="T:System.Int64" />).</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>An array of <see langword="long" /> integers (<see cref="T:System.Int64" />).</returns>
		// Token: 0x060004D6 RID: 1238 RVA: 0x00017E4F File Offset: 0x0001604F
		public virtual long[] ReadInt64Array(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return Int64ArrayHelperWithDictionaryString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of <see langword="long" /> integers into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the integers are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of integers to put in the array.</param>
		/// <returns>The number of integers put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004D7 RID: 1239 RVA: 0x00017E6C File Offset: 0x0001606C
		public virtual int ReadArray(string localName, string namespaceUri, long[] array, int offset, int count)
		{
			this.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && this.IsStartElement(localName, namespaceUri))
			{
				array[offset + num] = this.ReadElementContentAsLong();
				num++;
			}
			return num;
		}

		/// <summary>Reads repeated occurrences of <see langword="long" /> integers into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the integers are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of integers to put in the array.</param>
		/// <returns>The number of integers put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004D8 RID: 1240 RVA: 0x00017EA8 File Offset: 0x000160A8
		public virtual int ReadArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri, long[] array, int offset, int count)
		{
			return this.ReadArray(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri), array, offset, count);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into an array of <see langword="float" /> numbers (<see cref="T:System.Single" />).</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>An array of <see langword="float" /> numbers (<see cref="T:System.Single" />).</returns>
		// Token: 0x060004D9 RID: 1241 RVA: 0x00017EC1 File Offset: 0x000160C1
		public virtual float[] ReadSingleArray(string localName, string namespaceUri)
		{
			return SingleArrayHelperWithString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into an array of <see langword="float" /> numbers (<see cref="T:System.Single" />).</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>An array of <see langword="float" /> numbers (<see cref="T:System.Single" />).</returns>
		// Token: 0x060004DA RID: 1242 RVA: 0x00017EDB File Offset: 0x000160DB
		public virtual float[] ReadSingleArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return SingleArrayHelperWithDictionaryString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of <see langword="float" /> numbers into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the float numbers are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of float numbers to put in the array.</param>
		/// <returns>The umber of float numbers put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004DB RID: 1243 RVA: 0x00017EF8 File Offset: 0x000160F8
		public virtual int ReadArray(string localName, string namespaceUri, float[] array, int offset, int count)
		{
			this.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && this.IsStartElement(localName, namespaceUri))
			{
				array[offset + num] = this.ReadElementContentAsFloat();
				num++;
			}
			return num;
		}

		/// <summary>Reads repeated occurrences of <see langword="float" /> numbers into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the float numbers are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of float numbers to put in the array.</param>
		/// <returns>The number of float numbers put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004DC RID: 1244 RVA: 0x00017F34 File Offset: 0x00016134
		public virtual int ReadArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri, float[] array, int offset, int count)
		{
			return this.ReadArray(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri), array, offset, count);
		}

		/// <summary>Converts a node's content to a <see cref="T:System.Double" /> array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>The node's content represented as a <see cref="T:System.Double" /> array.</returns>
		// Token: 0x060004DD RID: 1245 RVA: 0x00017F4D File Offset: 0x0001614D
		public virtual double[] ReadDoubleArray(string localName, string namespaceUri)
		{
			return DoubleArrayHelperWithString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Converts a node's content to a <see cref="T:System.Double" /> array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>The node's content represented as a <see cref="T:System.Double" /> array.</returns>
		// Token: 0x060004DE RID: 1246 RVA: 0x00017F67 File Offset: 0x00016167
		public virtual double[] ReadDoubleArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return DoubleArrayHelperWithDictionaryString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.Double" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004DF RID: 1247 RVA: 0x00017F84 File Offset: 0x00016184
		public virtual int ReadArray(string localName, string namespaceUri, double[] array, int offset, int count)
		{
			this.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && this.IsStartElement(localName, namespaceUri))
			{
				array[offset + num] = this.ReadElementContentAsDouble();
				num++;
			}
			return num;
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.Double" /> nodes type into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004E0 RID: 1248 RVA: 0x00017FC0 File Offset: 0x000161C0
		public virtual int ReadArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri, double[] array, int offset, int count)
		{
			return this.ReadArray(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri), array, offset, count);
		}

		/// <summary>Converts a node's content to a <see cref="T:System.Decimal" /> array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>The node's content represented as a <see cref="T:System.Decimal" /> array.</returns>
		// Token: 0x060004E1 RID: 1249 RVA: 0x00017FD9 File Offset: 0x000161D9
		public virtual decimal[] ReadDecimalArray(string localName, string namespaceUri)
		{
			return DecimalArrayHelperWithString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Converts a node's content to a <see cref="T:System.Decimal" /> array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>The node's content represented as a <see cref="T:System.Decimal" /> array.</returns>
		// Token: 0x060004E2 RID: 1250 RVA: 0x00017FF3 File Offset: 0x000161F3
		public virtual decimal[] ReadDecimalArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return DecimalArrayHelperWithDictionaryString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.Decimal" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004E3 RID: 1251 RVA: 0x00018010 File Offset: 0x00016210
		public virtual int ReadArray(string localName, string namespaceUri, decimal[] array, int offset, int count)
		{
			this.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && this.IsStartElement(localName, namespaceUri))
			{
				array[offset + num] = this.ReadElementContentAsDecimal();
				num++;
			}
			return num;
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.Decimal" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004E4 RID: 1252 RVA: 0x00018050 File Offset: 0x00016250
		public virtual int ReadArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri, decimal[] array, int offset, int count)
		{
			return this.ReadArray(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri), array, offset, count);
		}

		/// <summary>Converts a node's content to a <see cref="T:System.DateTime" /> array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>The node's content represented as a <see cref="T:System.DateTime" /> array.</returns>
		// Token: 0x060004E5 RID: 1253 RVA: 0x00018069 File Offset: 0x00016269
		public virtual DateTime[] ReadDateTimeArray(string localName, string namespaceUri)
		{
			return DateTimeArrayHelperWithString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Converts a node's content to a <see cref="T:System.DateTime" /> array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>The node's content represented as a <see cref="T:System.DateTime" /> array.</returns>
		// Token: 0x060004E6 RID: 1254 RVA: 0x00018083 File Offset: 0x00016283
		public virtual DateTime[] ReadDateTimeArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return DateTimeArrayHelperWithDictionaryString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.DateTime" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004E7 RID: 1255 RVA: 0x000180A0 File Offset: 0x000162A0
		public virtual int ReadArray(string localName, string namespaceUri, DateTime[] array, int offset, int count)
		{
			this.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && this.IsStartElement(localName, namespaceUri))
			{
				array[offset + num] = this.ReadElementContentAsDateTime();
				num++;
			}
			return num;
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.DateTime" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004E8 RID: 1256 RVA: 0x000180E0 File Offset: 0x000162E0
		public virtual int ReadArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri, DateTime[] array, int offset, int count)
		{
			return this.ReadArray(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri), array, offset, count);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into an array of <see cref="T:System.Guid" />.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>An array of <see cref="T:System.Guid" />.</returns>
		// Token: 0x060004E9 RID: 1257 RVA: 0x000180F9 File Offset: 0x000162F9
		public virtual Guid[] ReadGuidArray(string localName, string namespaceUri)
		{
			return GuidArrayHelperWithString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into an array of <see cref="T:System.Guid" />.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>An array of <see cref="T:System.Guid" />.</returns>
		// Token: 0x060004EA RID: 1258 RVA: 0x00018113 File Offset: 0x00016313
		public virtual Guid[] ReadGuidArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return GuidArrayHelperWithDictionaryString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.Guid" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004EB RID: 1259 RVA: 0x00018130 File Offset: 0x00016330
		public virtual int ReadArray(string localName, string namespaceUri, Guid[] array, int offset, int count)
		{
			this.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && this.IsStartElement(localName, namespaceUri))
			{
				array[offset + num] = this.ReadElementContentAsGuid();
				num++;
			}
			return num;
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.Guid" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004EC RID: 1260 RVA: 0x00018170 File Offset: 0x00016370
		public virtual int ReadArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri, Guid[] array, int offset, int count)
		{
			return this.ReadArray(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri), array, offset, count);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into a <see cref="T:System.TimeSpan" /> array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>A <see cref="T:System.TimeSpan" /> array.</returns>
		// Token: 0x060004ED RID: 1261 RVA: 0x00018189 File Offset: 0x00016389
		public virtual TimeSpan[] ReadTimeSpanArray(string localName, string namespaceUri)
		{
			return TimeSpanArrayHelperWithString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads the contents of a series of nodes with the given <paramref name="localName" /> and <paramref name="namespaceUri" /> into a <see cref="T:System.TimeSpan" /> array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <returns>A <see cref="T:System.TimeSpan" /> array.</returns>
		// Token: 0x060004EE RID: 1262 RVA: 0x000181A3 File Offset: 0x000163A3
		public virtual TimeSpan[] ReadTimeSpanArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			return TimeSpanArrayHelperWithDictionaryString.Instance.ReadArray(this, localName, namespaceUri, this.Quotas.MaxArrayLength);
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.TimeSpan" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004EF RID: 1263 RVA: 0x000181C0 File Offset: 0x000163C0
		public virtual int ReadArray(string localName, string namespaceUri, TimeSpan[] array, int offset, int count)
		{
			this.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && this.IsStartElement(localName, namespaceUri))
			{
				array[offset + num] = this.ReadElementContentAsTimeSpan();
				num++;
			}
			return num;
		}

		/// <summary>Reads repeated occurrences of <see cref="T:System.TimeSpan" /> nodes into a typed array.</summary>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceUri">The namespace URI of the element.</param>
		/// <param name="array">The array into which the nodes are put.</param>
		/// <param name="offset">The starting index in the array.</param>
		/// <param name="count">The number of nodes to put in the array.</param>
		/// <returns>The number of nodes put in the array.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is &lt; 0 or &gt; <paramref name="array" /> length.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is &lt; 0 or &gt; <paramref name="array" /> length minus <paramref name="offset" />.</exception>
		// Token: 0x060004F0 RID: 1264 RVA: 0x00018200 File Offset: 0x00016400
		public virtual int ReadArray(XmlDictionaryString localName, XmlDictionaryString namespaceUri, TimeSpan[] array, int offset, int count)
		{
			return this.ReadArray(XmlDictionaryString.GetString(localName), XmlDictionaryString.GetString(namespaceUri), array, offset, count);
		}

		/// <summary>Creates an instance of this class.  Invoked only by its derived classes.</summary>
		// Token: 0x060004F1 RID: 1265 RVA: 0x00018219 File Offset: 0x00016419
		protected XmlDictionaryReader()
		{
		}

		// Token: 0x0400027A RID: 634
		internal const int MaxInitialArrayLength = 65535;

		// Token: 0x02000060 RID: 96
		private class XmlWrappedReader : XmlDictionaryReader, IXmlLineInfo
		{
			// Token: 0x060004F2 RID: 1266 RVA: 0x00018221 File Offset: 0x00016421
			public XmlWrappedReader(XmlReader reader, XmlNamespaceManager nsMgr)
			{
				this.reader = reader;
				this.nsMgr = nsMgr;
			}

			// Token: 0x17000076 RID: 118
			// (get) Token: 0x060004F3 RID: 1267 RVA: 0x00018237 File Offset: 0x00016437
			public override int AttributeCount
			{
				get
				{
					return this.reader.AttributeCount;
				}
			}

			// Token: 0x17000077 RID: 119
			// (get) Token: 0x060004F4 RID: 1268 RVA: 0x00018244 File Offset: 0x00016444
			public override string BaseURI
			{
				get
				{
					return this.reader.BaseURI;
				}
			}

			// Token: 0x17000078 RID: 120
			// (get) Token: 0x060004F5 RID: 1269 RVA: 0x00018251 File Offset: 0x00016451
			public override bool CanReadBinaryContent
			{
				get
				{
					return this.reader.CanReadBinaryContent;
				}
			}

			// Token: 0x17000079 RID: 121
			// (get) Token: 0x060004F6 RID: 1270 RVA: 0x0001825E File Offset: 0x0001645E
			public override bool CanReadValueChunk
			{
				get
				{
					return this.reader.CanReadValueChunk;
				}
			}

			// Token: 0x060004F7 RID: 1271 RVA: 0x0001826B File Offset: 0x0001646B
			public override void Close()
			{
				this.reader.Close();
				this.nsMgr = null;
			}

			// Token: 0x1700007A RID: 122
			// (get) Token: 0x060004F8 RID: 1272 RVA: 0x0001827F File Offset: 0x0001647F
			public override int Depth
			{
				get
				{
					return this.reader.Depth;
				}
			}

			// Token: 0x1700007B RID: 123
			// (get) Token: 0x060004F9 RID: 1273 RVA: 0x0001828C File Offset: 0x0001648C
			public override bool EOF
			{
				get
				{
					return this.reader.EOF;
				}
			}

			// Token: 0x060004FA RID: 1274 RVA: 0x00018299 File Offset: 0x00016499
			public override string GetAttribute(int index)
			{
				return this.reader.GetAttribute(index);
			}

			// Token: 0x060004FB RID: 1275 RVA: 0x000182A7 File Offset: 0x000164A7
			public override string GetAttribute(string name)
			{
				return this.reader.GetAttribute(name);
			}

			// Token: 0x060004FC RID: 1276 RVA: 0x000182B5 File Offset: 0x000164B5
			public override string GetAttribute(string name, string namespaceUri)
			{
				return this.reader.GetAttribute(name, namespaceUri);
			}

			// Token: 0x1700007C RID: 124
			// (get) Token: 0x060004FD RID: 1277 RVA: 0x000182C4 File Offset: 0x000164C4
			public override bool HasValue
			{
				get
				{
					return this.reader.HasValue;
				}
			}

			// Token: 0x1700007D RID: 125
			// (get) Token: 0x060004FE RID: 1278 RVA: 0x000182D1 File Offset: 0x000164D1
			public override bool IsDefault
			{
				get
				{
					return this.reader.IsDefault;
				}
			}

			// Token: 0x1700007E RID: 126
			// (get) Token: 0x060004FF RID: 1279 RVA: 0x000182DE File Offset: 0x000164DE
			public override bool IsEmptyElement
			{
				get
				{
					return this.reader.IsEmptyElement;
				}
			}

			// Token: 0x06000500 RID: 1280 RVA: 0x000182EB File Offset: 0x000164EB
			public override bool IsStartElement(string name)
			{
				return this.reader.IsStartElement(name);
			}

			// Token: 0x06000501 RID: 1281 RVA: 0x000182F9 File Offset: 0x000164F9
			public override bool IsStartElement(string localName, string namespaceUri)
			{
				return this.reader.IsStartElement(localName, namespaceUri);
			}

			// Token: 0x1700007F RID: 127
			// (get) Token: 0x06000502 RID: 1282 RVA: 0x00018308 File Offset: 0x00016508
			public override string LocalName
			{
				get
				{
					return this.reader.LocalName;
				}
			}

			// Token: 0x06000503 RID: 1283 RVA: 0x00018315 File Offset: 0x00016515
			public override string LookupNamespace(string namespaceUri)
			{
				return this.reader.LookupNamespace(namespaceUri);
			}

			// Token: 0x06000504 RID: 1284 RVA: 0x00018323 File Offset: 0x00016523
			public override void MoveToAttribute(int index)
			{
				this.reader.MoveToAttribute(index);
			}

			// Token: 0x06000505 RID: 1285 RVA: 0x00018331 File Offset: 0x00016531
			public override bool MoveToAttribute(string name)
			{
				return this.reader.MoveToAttribute(name);
			}

			// Token: 0x06000506 RID: 1286 RVA: 0x0001833F File Offset: 0x0001653F
			public override bool MoveToAttribute(string name, string namespaceUri)
			{
				return this.reader.MoveToAttribute(name, namespaceUri);
			}

			// Token: 0x06000507 RID: 1287 RVA: 0x0001834E File Offset: 0x0001654E
			public override bool MoveToElement()
			{
				return this.reader.MoveToElement();
			}

			// Token: 0x06000508 RID: 1288 RVA: 0x0001835B File Offset: 0x0001655B
			public override bool MoveToFirstAttribute()
			{
				return this.reader.MoveToFirstAttribute();
			}

			// Token: 0x06000509 RID: 1289 RVA: 0x00018368 File Offset: 0x00016568
			public override bool MoveToNextAttribute()
			{
				return this.reader.MoveToNextAttribute();
			}

			// Token: 0x17000080 RID: 128
			// (get) Token: 0x0600050A RID: 1290 RVA: 0x00018375 File Offset: 0x00016575
			public override string Name
			{
				get
				{
					return this.reader.Name;
				}
			}

			// Token: 0x17000081 RID: 129
			// (get) Token: 0x0600050B RID: 1291 RVA: 0x00018382 File Offset: 0x00016582
			public override string NamespaceURI
			{
				get
				{
					return this.reader.NamespaceURI;
				}
			}

			// Token: 0x17000082 RID: 130
			// (get) Token: 0x0600050C RID: 1292 RVA: 0x0001838F File Offset: 0x0001658F
			public override XmlNameTable NameTable
			{
				get
				{
					return this.reader.NameTable;
				}
			}

			// Token: 0x17000083 RID: 131
			// (get) Token: 0x0600050D RID: 1293 RVA: 0x0001839C File Offset: 0x0001659C
			public override XmlNodeType NodeType
			{
				get
				{
					return this.reader.NodeType;
				}
			}

			// Token: 0x17000084 RID: 132
			// (get) Token: 0x0600050E RID: 1294 RVA: 0x000183A9 File Offset: 0x000165A9
			public override string Prefix
			{
				get
				{
					return this.reader.Prefix;
				}
			}

			// Token: 0x17000085 RID: 133
			// (get) Token: 0x0600050F RID: 1295 RVA: 0x000183B6 File Offset: 0x000165B6
			public override char QuoteChar
			{
				get
				{
					return this.reader.QuoteChar;
				}
			}

			// Token: 0x06000510 RID: 1296 RVA: 0x000183C3 File Offset: 0x000165C3
			public override bool Read()
			{
				return this.reader.Read();
			}

			// Token: 0x06000511 RID: 1297 RVA: 0x000183D0 File Offset: 0x000165D0
			public override bool ReadAttributeValue()
			{
				return this.reader.ReadAttributeValue();
			}

			// Token: 0x06000512 RID: 1298 RVA: 0x000183DD File Offset: 0x000165DD
			public override string ReadElementString(string name)
			{
				return this.reader.ReadElementString(name);
			}

			// Token: 0x06000513 RID: 1299 RVA: 0x000183EB File Offset: 0x000165EB
			public override string ReadElementString(string localName, string namespaceUri)
			{
				return this.reader.ReadElementString(localName, namespaceUri);
			}

			// Token: 0x06000514 RID: 1300 RVA: 0x000183FA File Offset: 0x000165FA
			public override string ReadInnerXml()
			{
				return this.reader.ReadInnerXml();
			}

			// Token: 0x06000515 RID: 1301 RVA: 0x00018407 File Offset: 0x00016607
			public override string ReadOuterXml()
			{
				return this.reader.ReadOuterXml();
			}

			// Token: 0x06000516 RID: 1302 RVA: 0x00018414 File Offset: 0x00016614
			public override void ReadStartElement(string name)
			{
				this.reader.ReadStartElement(name);
			}

			// Token: 0x06000517 RID: 1303 RVA: 0x00018422 File Offset: 0x00016622
			public override void ReadStartElement(string localName, string namespaceUri)
			{
				this.reader.ReadStartElement(localName, namespaceUri);
			}

			// Token: 0x06000518 RID: 1304 RVA: 0x00018431 File Offset: 0x00016631
			public override void ReadEndElement()
			{
				this.reader.ReadEndElement();
			}

			// Token: 0x06000519 RID: 1305 RVA: 0x0001843E File Offset: 0x0001663E
			public override string ReadString()
			{
				return this.reader.ReadString();
			}

			// Token: 0x17000086 RID: 134
			// (get) Token: 0x0600051A RID: 1306 RVA: 0x0001844B File Offset: 0x0001664B
			public override ReadState ReadState
			{
				get
				{
					return this.reader.ReadState;
				}
			}

			// Token: 0x0600051B RID: 1307 RVA: 0x00018458 File Offset: 0x00016658
			public override void ResolveEntity()
			{
				this.reader.ResolveEntity();
			}

			// Token: 0x17000087 RID: 135
			public override string this[int index]
			{
				get
				{
					return this.reader[index];
				}
			}

			// Token: 0x17000088 RID: 136
			public override string this[string name]
			{
				get
				{
					return this.reader[name];
				}
			}

			// Token: 0x17000089 RID: 137
			public override string this[string name, string namespaceUri]
			{
				get
				{
					return this.reader[name, namespaceUri];
				}
			}

			// Token: 0x1700008A RID: 138
			// (get) Token: 0x0600051F RID: 1311 RVA: 0x00018490 File Offset: 0x00016690
			public override string Value
			{
				get
				{
					return this.reader.Value;
				}
			}

			// Token: 0x1700008B RID: 139
			// (get) Token: 0x06000520 RID: 1312 RVA: 0x0001849D File Offset: 0x0001669D
			public override string XmlLang
			{
				get
				{
					return this.reader.XmlLang;
				}
			}

			// Token: 0x1700008C RID: 140
			// (get) Token: 0x06000521 RID: 1313 RVA: 0x000184AA File Offset: 0x000166AA
			public override XmlSpace XmlSpace
			{
				get
				{
					return this.reader.XmlSpace;
				}
			}

			// Token: 0x06000522 RID: 1314 RVA: 0x000184B7 File Offset: 0x000166B7
			public override int ReadElementContentAsBase64(byte[] buffer, int offset, int count)
			{
				return this.reader.ReadElementContentAsBase64(buffer, offset, count);
			}

			// Token: 0x06000523 RID: 1315 RVA: 0x000184C7 File Offset: 0x000166C7
			public override int ReadContentAsBase64(byte[] buffer, int offset, int count)
			{
				return this.reader.ReadContentAsBase64(buffer, offset, count);
			}

			// Token: 0x06000524 RID: 1316 RVA: 0x000184D7 File Offset: 0x000166D7
			public override int ReadElementContentAsBinHex(byte[] buffer, int offset, int count)
			{
				return this.reader.ReadElementContentAsBinHex(buffer, offset, count);
			}

			// Token: 0x06000525 RID: 1317 RVA: 0x000184E7 File Offset: 0x000166E7
			public override int ReadContentAsBinHex(byte[] buffer, int offset, int count)
			{
				return this.reader.ReadContentAsBinHex(buffer, offset, count);
			}

			// Token: 0x06000526 RID: 1318 RVA: 0x000184F7 File Offset: 0x000166F7
			public override int ReadValueChunk(char[] chars, int offset, int count)
			{
				return this.reader.ReadValueChunk(chars, offset, count);
			}

			// Token: 0x1700008D RID: 141
			// (get) Token: 0x06000527 RID: 1319 RVA: 0x00018507 File Offset: 0x00016707
			public override Type ValueType
			{
				get
				{
					return this.reader.ValueType;
				}
			}

			// Token: 0x06000528 RID: 1320 RVA: 0x00018514 File Offset: 0x00016714
			public override bool ReadContentAsBoolean()
			{
				return this.reader.ReadContentAsBoolean();
			}

			// Token: 0x06000529 RID: 1321 RVA: 0x00018521 File Offset: 0x00016721
			public override DateTime ReadContentAsDateTime()
			{
				return this.reader.ReadContentAsDateTime();
			}

			// Token: 0x0600052A RID: 1322 RVA: 0x0001852E File Offset: 0x0001672E
			public override decimal ReadContentAsDecimal()
			{
				return (decimal)this.reader.ReadContentAs(typeof(decimal), null);
			}

			// Token: 0x0600052B RID: 1323 RVA: 0x0001854B File Offset: 0x0001674B
			public override double ReadContentAsDouble()
			{
				return this.reader.ReadContentAsDouble();
			}

			// Token: 0x0600052C RID: 1324 RVA: 0x00018558 File Offset: 0x00016758
			public override int ReadContentAsInt()
			{
				return this.reader.ReadContentAsInt();
			}

			// Token: 0x0600052D RID: 1325 RVA: 0x00018565 File Offset: 0x00016765
			public override long ReadContentAsLong()
			{
				return this.reader.ReadContentAsLong();
			}

			// Token: 0x0600052E RID: 1326 RVA: 0x00018572 File Offset: 0x00016772
			public override float ReadContentAsFloat()
			{
				return this.reader.ReadContentAsFloat();
			}

			// Token: 0x0600052F RID: 1327 RVA: 0x0001857F File Offset: 0x0001677F
			public override string ReadContentAsString()
			{
				return this.reader.ReadContentAsString();
			}

			// Token: 0x06000530 RID: 1328 RVA: 0x0001858C File Offset: 0x0001678C
			public override object ReadContentAs(Type type, IXmlNamespaceResolver namespaceResolver)
			{
				return this.reader.ReadContentAs(type, namespaceResolver);
			}

			// Token: 0x06000531 RID: 1329 RVA: 0x0001859C File Offset: 0x0001679C
			public bool HasLineInfo()
			{
				IXmlLineInfo xmlLineInfo = this.reader as IXmlLineInfo;
				return xmlLineInfo != null && xmlLineInfo.HasLineInfo();
			}

			// Token: 0x1700008E RID: 142
			// (get) Token: 0x06000532 RID: 1330 RVA: 0x000185C0 File Offset: 0x000167C0
			public int LineNumber
			{
				get
				{
					IXmlLineInfo xmlLineInfo = this.reader as IXmlLineInfo;
					if (xmlLineInfo == null)
					{
						return 1;
					}
					return xmlLineInfo.LineNumber;
				}
			}

			// Token: 0x1700008F RID: 143
			// (get) Token: 0x06000533 RID: 1331 RVA: 0x000185E4 File Offset: 0x000167E4
			public int LinePosition
			{
				get
				{
					IXmlLineInfo xmlLineInfo = this.reader as IXmlLineInfo;
					if (xmlLineInfo == null)
					{
						return 1;
					}
					return xmlLineInfo.LinePosition;
				}
			}

			// Token: 0x0400027B RID: 635
			private XmlReader reader;

			// Token: 0x0400027C RID: 636
			private XmlNamespaceManager nsMgr;
		}
	}
}
