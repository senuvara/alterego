﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace System.Xml
{
	// Token: 0x02000075 RID: 117
	internal class MimeHeaders
	{
		// Token: 0x060006A3 RID: 1699 RVA: 0x0001C818 File Offset: 0x0001AA18
		public MimeHeaders()
		{
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x060006A4 RID: 1700 RVA: 0x0001C82C File Offset: 0x0001AA2C
		public ContentTypeHeader ContentType
		{
			get
			{
				MimeHeader mimeHeader;
				if (this.headers.TryGetValue("content-type", out mimeHeader))
				{
					return mimeHeader as ContentTypeHeader;
				}
				return null;
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x060006A5 RID: 1701 RVA: 0x0001C858 File Offset: 0x0001AA58
		public ContentIDHeader ContentID
		{
			get
			{
				MimeHeader mimeHeader;
				if (this.headers.TryGetValue("content-id", out mimeHeader))
				{
					return mimeHeader as ContentIDHeader;
				}
				return null;
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x060006A6 RID: 1702 RVA: 0x0001C884 File Offset: 0x0001AA84
		public ContentTransferEncodingHeader ContentTransferEncoding
		{
			get
			{
				MimeHeader mimeHeader;
				if (this.headers.TryGetValue("content-transfer-encoding", out mimeHeader))
				{
					return mimeHeader as ContentTransferEncodingHeader;
				}
				return null;
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x060006A7 RID: 1703 RVA: 0x0001C8B0 File Offset: 0x0001AAB0
		public MimeVersionHeader MimeVersion
		{
			get
			{
				MimeHeader mimeHeader;
				if (this.headers.TryGetValue("mime-version", out mimeHeader))
				{
					return mimeHeader as MimeVersionHeader;
				}
				return null;
			}
		}

		// Token: 0x060006A8 RID: 1704 RVA: 0x0001C8DC File Offset: 0x0001AADC
		public void Add(string name, string value, ref int remaining)
		{
			if (name == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("name");
			}
			if (value == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("value");
			}
			if (!(name == "content-type"))
			{
				if (!(name == "content-id"))
				{
					if (!(name == "content-transfer-encoding"))
					{
						if (!(name == "mime-version"))
						{
							remaining += value.Length * 2;
						}
						else
						{
							this.Add(new MimeVersionHeader(value));
						}
					}
					else
					{
						this.Add(new ContentTransferEncodingHeader(value));
					}
				}
				else
				{
					this.Add(new ContentIDHeader(name, value));
				}
			}
			else
			{
				this.Add(new ContentTypeHeader(value));
			}
			remaining += name.Length * 2;
		}

		// Token: 0x060006A9 RID: 1705 RVA: 0x0001C990 File Offset: 0x0001AB90
		public void Add(MimeHeader header)
		{
			if (header == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("header");
			}
			MimeHeader mimeHeader;
			if (this.headers.TryGetValue(header.Name, out mimeHeader))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new FormatException(System.Runtime.Serialization.SR.GetString("MIME header '{0}' already exists.", new object[]
				{
					header.Name
				})));
			}
			this.headers.Add(header.Name, header);
		}

		// Token: 0x060006AA RID: 1706 RVA: 0x0001C9F8 File Offset: 0x0001ABF8
		public void Release(ref int remaining)
		{
			foreach (MimeHeader mimeHeader in this.headers.Values)
			{
				remaining += mimeHeader.Value.Length * 2;
			}
		}

		// Token: 0x040002EF RID: 751
		private Dictionary<string, MimeHeader> headers = new Dictionary<string, MimeHeader>();

		// Token: 0x02000076 RID: 118
		private static class Constants
		{
			// Token: 0x040002F0 RID: 752
			public const string ContentTransferEncoding = "content-transfer-encoding";

			// Token: 0x040002F1 RID: 753
			public const string ContentID = "content-id";

			// Token: 0x040002F2 RID: 754
			public const string ContentType = "content-type";

			// Token: 0x040002F3 RID: 755
			public const string MimeVersion = "mime-version";
		}
	}
}
