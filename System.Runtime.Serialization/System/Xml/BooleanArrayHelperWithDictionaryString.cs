﻿using System;

namespace System.Xml
{
	// Token: 0x02000006 RID: 6
	internal class BooleanArrayHelperWithDictionaryString : ArrayHelper<XmlDictionaryString, bool>
	{
		// Token: 0x06000013 RID: 19 RVA: 0x00002255 File Offset: 0x00000455
		protected override int ReadArray(XmlDictionaryReader reader, XmlDictionaryString localName, XmlDictionaryString namespaceUri, bool[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002265 File Offset: 0x00000465
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, bool[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002277 File Offset: 0x00000477
		public BooleanArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x06000016 RID: 22 RVA: 0x0000227F File Offset: 0x0000047F
		// Note: this type is marked as 'beforefieldinit'.
		static BooleanArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x04000004 RID: 4
		public static readonly BooleanArrayHelperWithDictionaryString Instance = new BooleanArrayHelperWithDictionaryString();
	}
}
