﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace System.Xml
{
	// Token: 0x02000071 RID: 113
	internal class MimeReader
	{
		// Token: 0x06000683 RID: 1667 RVA: 0x0001C144 File Offset: 0x0001A344
		public MimeReader(Stream stream, string boundary)
		{
			if (stream == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("stream");
			}
			if (boundary == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("boundary");
			}
			this.reader = new DelimittedStreamReader(stream);
			this.boundaryBytes = MimeWriter.GetBoundaryBytes(boundary);
			this.reader.Push(this.boundaryBytes, 0, 2);
		}

		// Token: 0x06000684 RID: 1668 RVA: 0x0001C1AA File Offset: 0x0001A3AA
		public void Close()
		{
			this.reader.Close();
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000685 RID: 1669 RVA: 0x0001C1B8 File Offset: 0x0001A3B8
		public string Preface
		{
			get
			{
				if (this.content == null)
				{
					Stream nextStream = this.reader.GetNextStream(this.boundaryBytes);
					this.content = new StreamReader(nextStream, Encoding.ASCII, false, 256).ReadToEnd();
					nextStream.Close();
					if (this.content == null)
					{
						this.content = string.Empty;
					}
				}
				return this.content;
			}
		}

		// Token: 0x06000686 RID: 1670 RVA: 0x0001C21A File Offset: 0x0001A41A
		public Stream GetContentStream()
		{
			this.mimeHeaderReader.Close();
			return this.reader.GetNextStream(this.boundaryBytes);
		}

		// Token: 0x06000687 RID: 1671 RVA: 0x0001C238 File Offset: 0x0001A438
		public bool ReadNextPart()
		{
			string preface = this.Preface;
			if (this.currentStream != null)
			{
				this.currentStream.Close();
				this.currentStream = null;
			}
			Stream nextStream = this.reader.GetNextStream(MimeReader.CRLFCRLF);
			if (nextStream == null)
			{
				return false;
			}
			if (this.BlockRead(nextStream, this.scratch, 0, 2) == 2)
			{
				if (this.scratch[0] == 13 && this.scratch[1] == 10)
				{
					if (this.mimeHeaderReader == null)
					{
						this.mimeHeaderReader = new MimeHeaderReader(nextStream);
					}
					else
					{
						this.mimeHeaderReader.Reset(nextStream);
					}
					return true;
				}
				if (this.scratch[0] == 45 && this.scratch[1] == 45 && (this.BlockRead(nextStream, this.scratch, 0, 2) < 2 || (this.scratch[0] == 13 && this.scratch[1] == 10)))
				{
					return false;
				}
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new FormatException(System.Runtime.Serialization.SR.GetString("MIME parts are truncated.")));
		}

		// Token: 0x06000688 RID: 1672 RVA: 0x0001C328 File Offset: 0x0001A528
		public MimeHeaders ReadHeaders(int maxBuffer, ref int remaining)
		{
			MimeHeaders mimeHeaders = new MimeHeaders();
			while (this.mimeHeaderReader.Read(maxBuffer, ref remaining))
			{
				mimeHeaders.Add(this.mimeHeaderReader.Name, this.mimeHeaderReader.Value, ref remaining);
			}
			return mimeHeaders;
		}

		// Token: 0x06000689 RID: 1673 RVA: 0x0001C36C File Offset: 0x0001A56C
		private int BlockRead(Stream stream, byte[] buffer, int offset, int count)
		{
			int num = 0;
			do
			{
				int num2 = stream.Read(buffer, offset + num, count - num);
				if (num2 == 0)
				{
					break;
				}
				num += num2;
			}
			while (num < count);
			return num;
		}

		// Token: 0x0600068A RID: 1674 RVA: 0x0001C397 File Offset: 0x0001A597
		// Note: this type is marked as 'beforefieldinit'.
		static MimeReader()
		{
		}

		// Token: 0x040002DD RID: 733
		private static byte[] CRLFCRLF = new byte[]
		{
			13,
			10,
			13,
			10
		};

		// Token: 0x040002DE RID: 734
		private byte[] boundaryBytes;

		// Token: 0x040002DF RID: 735
		private string content;

		// Token: 0x040002E0 RID: 736
		private Stream currentStream;

		// Token: 0x040002E1 RID: 737
		private MimeHeaderReader mimeHeaderReader;

		// Token: 0x040002E2 RID: 738
		private DelimittedStreamReader reader;

		// Token: 0x040002E3 RID: 739
		private byte[] scratch = new byte[2];
	}
}
