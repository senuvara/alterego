﻿using System;
using System.Runtime.Serialization;

namespace System.Xml
{
	// Token: 0x02000077 RID: 119
	internal class MimeHeader
	{
		// Token: 0x060006AB RID: 1707 RVA: 0x0001CA5C File Offset: 0x0001AC5C
		public MimeHeader(string name, string value)
		{
			if (name == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("name");
			}
			this.name = name;
			this.value = value;
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x060006AC RID: 1708 RVA: 0x0001CA80 File Offset: 0x0001AC80
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x060006AD RID: 1709 RVA: 0x0001CA88 File Offset: 0x0001AC88
		public string Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x040002F4 RID: 756
		private string name;

		// Token: 0x040002F5 RID: 757
		private string value;
	}
}
