﻿using System;

namespace System.Xml
{
	// Token: 0x02000020 RID: 32
	internal enum StringHandleConstStringType
	{
		// Token: 0x04000058 RID: 88
		Type,
		// Token: 0x04000059 RID: 89
		Root,
		// Token: 0x0400005A RID: 90
		Item
	}
}
