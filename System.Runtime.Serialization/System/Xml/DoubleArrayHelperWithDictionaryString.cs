﻿using System;

namespace System.Xml
{
	// Token: 0x02000010 RID: 16
	internal class DoubleArrayHelperWithDictionaryString : ArrayHelper<XmlDictionaryString, double>
	{
		// Token: 0x0600003B RID: 59 RVA: 0x00002471 File Offset: 0x00000671
		protected override int ReadArray(XmlDictionaryReader reader, XmlDictionaryString localName, XmlDictionaryString namespaceUri, double[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00002481 File Offset: 0x00000681
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, double[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00002493 File Offset: 0x00000693
		public DoubleArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x0600003E RID: 62 RVA: 0x0000249B File Offset: 0x0000069B
		// Note: this type is marked as 'beforefieldinit'.
		static DoubleArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x0400000E RID: 14
		public static readonly DoubleArrayHelperWithDictionaryString Instance = new DoubleArrayHelperWithDictionaryString();
	}
}
