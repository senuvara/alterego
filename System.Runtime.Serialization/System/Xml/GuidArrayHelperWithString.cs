﻿using System;

namespace System.Xml
{
	// Token: 0x02000015 RID: 21
	internal class GuidArrayHelperWithString : ArrayHelper<string, Guid>
	{
		// Token: 0x0600004F RID: 79 RVA: 0x0000257F File Offset: 0x0000077F
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, Guid[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000050 RID: 80 RVA: 0x0000258F File Offset: 0x0000078F
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, Guid[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000051 RID: 81 RVA: 0x000025A1 File Offset: 0x000007A1
		public GuidArrayHelperWithString()
		{
		}

		// Token: 0x06000052 RID: 82 RVA: 0x000025A9 File Offset: 0x000007A9
		// Note: this type is marked as 'beforefieldinit'.
		static GuidArrayHelperWithString()
		{
		}

		// Token: 0x04000013 RID: 19
		public static readonly GuidArrayHelperWithString Instance = new GuidArrayHelperWithString();
	}
}
