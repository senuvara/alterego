﻿using System;

namespace System.Xml
{
	// Token: 0x02000085 RID: 133
	internal static class MtomGlobals
	{
		// Token: 0x06000731 RID: 1841 RVA: 0x0001F1E8 File Offset: 0x0001D3E8
		// Note: this type is marked as 'beforefieldinit'.
		static MtomGlobals()
		{
		}

		// Token: 0x0400033A RID: 826
		internal static string XopIncludeLocalName = "Include";

		// Token: 0x0400033B RID: 827
		internal static string XopIncludeNamespace = "http://www.w3.org/2004/08/xop/include";

		// Token: 0x0400033C RID: 828
		internal static string XopIncludePrefix = "xop";

		// Token: 0x0400033D RID: 829
		internal static string XopIncludeHrefLocalName = "href";

		// Token: 0x0400033E RID: 830
		internal static string XopIncludeHrefNamespace = string.Empty;

		// Token: 0x0400033F RID: 831
		internal static string MediaType = "multipart";

		// Token: 0x04000340 RID: 832
		internal static string MediaSubtype = "related";

		// Token: 0x04000341 RID: 833
		internal static string BoundaryParam = "boundary";

		// Token: 0x04000342 RID: 834
		internal static string TypeParam = "type";

		// Token: 0x04000343 RID: 835
		internal static string XopMediaType = "application";

		// Token: 0x04000344 RID: 836
		internal static string XopMediaSubtype = "xop+xml";

		// Token: 0x04000345 RID: 837
		internal static string XopType = "application/xop+xml";

		// Token: 0x04000346 RID: 838
		internal static string StartParam = "start";

		// Token: 0x04000347 RID: 839
		internal static string StartInfoParam = "start-info";

		// Token: 0x04000348 RID: 840
		internal static string ActionParam = "action";

		// Token: 0x04000349 RID: 841
		internal static string CharsetParam = "charset";

		// Token: 0x0400034A RID: 842
		internal static string MimeContentTypeLocalName = "contentType";

		// Token: 0x0400034B RID: 843
		internal static string MimeContentTypeNamespace200406 = "http://www.w3.org/2004/06/xmlmime";

		// Token: 0x0400034C RID: 844
		internal static string MimeContentTypeNamespace200505 = "http://www.w3.org/2005/05/xmlmime";

		// Token: 0x0400034D RID: 845
		internal static string DefaultContentTypeForBinary = "application/octet-stream";
	}
}
