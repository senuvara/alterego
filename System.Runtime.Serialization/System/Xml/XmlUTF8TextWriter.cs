﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace System.Xml
{
	// Token: 0x0200009C RID: 156
	internal class XmlUTF8TextWriter : XmlBaseWriter, IXmlTextWriterInitializer
	{
		// Token: 0x17000114 RID: 276
		// (get) Token: 0x0600084A RID: 2122 RVA: 0x000066D0 File Offset: 0x000048D0
		internal override bool FastAsync
		{
			get
			{
				return true;
			}
		}

		// Token: 0x0600084B RID: 2123 RVA: 0x00022434 File Offset: 0x00020634
		public void SetOutput(Stream stream, Encoding encoding, bool ownsStream)
		{
			if (stream == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("stream");
			}
			if (encoding == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("encoding");
			}
			if (encoding.WebName != Encoding.UTF8.WebName)
			{
				stream = new EncodingStreamWrapper(stream, encoding, true);
			}
			if (this.writer == null)
			{
				this.writer = new XmlUTF8NodeWriter();
			}
			this.writer.SetOutput(stream, ownsStream, encoding);
			base.SetOutput(this.writer);
		}

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x0600084C RID: 2124 RVA: 0x000224AB File Offset: 0x000206AB
		public override bool CanFragment
		{
			get
			{
				return this.writer.Encoding == null;
			}
		}

		// Token: 0x0600084D RID: 2125 RVA: 0x00022378 File Offset: 0x00020578
		protected override XmlSigningNodeWriter CreateSigningNodeWriter()
		{
			return new XmlSigningNodeWriter(true);
		}

		// Token: 0x0600084E RID: 2126 RVA: 0x000118B0 File Offset: 0x0000FAB0
		public XmlUTF8TextWriter()
		{
		}

		// Token: 0x040003AC RID: 940
		private XmlUTF8NodeWriter writer;
	}
}
