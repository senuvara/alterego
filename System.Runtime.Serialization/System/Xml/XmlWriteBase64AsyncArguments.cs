﻿using System;
using System.Runtime.CompilerServices;

namespace System.Xml
{
	// Token: 0x020000A1 RID: 161
	internal class XmlWriteBase64AsyncArguments
	{
		// Token: 0x17000117 RID: 279
		// (get) Token: 0x060008AC RID: 2220 RVA: 0x0002394E File Offset: 0x00021B4E
		// (set) Token: 0x060008AD RID: 2221 RVA: 0x00023956 File Offset: 0x00021B56
		internal byte[] Buffer
		{
			[CompilerGenerated]
			get
			{
				return this.<Buffer>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Buffer>k__BackingField = value;
			}
		}

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x060008AE RID: 2222 RVA: 0x0002395F File Offset: 0x00021B5F
		// (set) Token: 0x060008AF RID: 2223 RVA: 0x00023967 File Offset: 0x00021B67
		internal int Index
		{
			[CompilerGenerated]
			get
			{
				return this.<Index>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Index>k__BackingField = value;
			}
		}

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x060008B0 RID: 2224 RVA: 0x00023970 File Offset: 0x00021B70
		// (set) Token: 0x060008B1 RID: 2225 RVA: 0x00023978 File Offset: 0x00021B78
		internal int Count
		{
			[CompilerGenerated]
			get
			{
				return this.<Count>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Count>k__BackingField = value;
			}
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x060008B2 RID: 2226 RVA: 0x00023981 File Offset: 0x00021B81
		// (set) Token: 0x060008B3 RID: 2227 RVA: 0x00023989 File Offset: 0x00021B89
		internal int Offset
		{
			[CompilerGenerated]
			get
			{
				return this.<Offset>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Offset>k__BackingField = value;
			}
		}

		// Token: 0x060008B4 RID: 2228 RVA: 0x00002217 File Offset: 0x00000417
		public XmlWriteBase64AsyncArguments()
		{
		}

		// Token: 0x040003D5 RID: 981
		[CompilerGenerated]
		private byte[] <Buffer>k__BackingField;

		// Token: 0x040003D6 RID: 982
		[CompilerGenerated]
		private int <Index>k__BackingField;

		// Token: 0x040003D7 RID: 983
		[CompilerGenerated]
		private int <Count>k__BackingField;

		// Token: 0x040003D8 RID: 984
		[CompilerGenerated]
		private int <Offset>k__BackingField;
	}
}
