﻿using System;

namespace System.Xml
{
	// Token: 0x02000012 RID: 18
	internal class DecimalArrayHelperWithDictionaryString : ArrayHelper<XmlDictionaryString, decimal>
	{
		// Token: 0x06000043 RID: 67 RVA: 0x000024DD File Offset: 0x000006DD
		protected override int ReadArray(XmlDictionaryReader reader, XmlDictionaryString localName, XmlDictionaryString namespaceUri, decimal[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000044 RID: 68 RVA: 0x000024ED File Offset: 0x000006ED
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, decimal[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000045 RID: 69 RVA: 0x000024FF File Offset: 0x000006FF
		public DecimalArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00002507 File Offset: 0x00000707
		// Note: this type is marked as 'beforefieldinit'.
		static DecimalArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x04000010 RID: 16
		public static readonly DecimalArrayHelperWithDictionaryString Instance = new DecimalArrayHelperWithDictionaryString();
	}
}
