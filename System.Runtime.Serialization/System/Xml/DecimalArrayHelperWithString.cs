﻿using System;

namespace System.Xml
{
	// Token: 0x02000011 RID: 17
	internal class DecimalArrayHelperWithString : ArrayHelper<string, decimal>
	{
		// Token: 0x0600003F RID: 63 RVA: 0x000024A7 File Offset: 0x000006A7
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, decimal[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000040 RID: 64 RVA: 0x000024B7 File Offset: 0x000006B7
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, decimal[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000041 RID: 65 RVA: 0x000024C9 File Offset: 0x000006C9
		public DecimalArrayHelperWithString()
		{
		}

		// Token: 0x06000042 RID: 66 RVA: 0x000024D1 File Offset: 0x000006D1
		// Note: this type is marked as 'beforefieldinit'.
		static DecimalArrayHelperWithString()
		{
		}

		// Token: 0x0400000F RID: 15
		public static readonly DecimalArrayHelperWithString Instance = new DecimalArrayHelperWithString();
	}
}
