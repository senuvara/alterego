﻿using System;

namespace System.Xml
{
	// Token: 0x02000016 RID: 22
	internal class GuidArrayHelperWithDictionaryString : ArrayHelper<XmlDictionaryString, Guid>
	{
		// Token: 0x06000053 RID: 83 RVA: 0x000025B5 File Offset: 0x000007B5
		protected override int ReadArray(XmlDictionaryReader reader, XmlDictionaryString localName, XmlDictionaryString namespaceUri, Guid[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000054 RID: 84 RVA: 0x000025C5 File Offset: 0x000007C5
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, Guid[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000055 RID: 85 RVA: 0x000025D7 File Offset: 0x000007D7
		public GuidArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x06000056 RID: 86 RVA: 0x000025DF File Offset: 0x000007DF
		// Note: this type is marked as 'beforefieldinit'.
		static GuidArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x04000014 RID: 20
		public static readonly GuidArrayHelperWithDictionaryString Instance = new GuidArrayHelperWithDictionaryString();
	}
}
