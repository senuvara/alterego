﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace System.Xml
{
	// Token: 0x02000070 RID: 112
	internal class MimeMessageReader
	{
		// Token: 0x0600067F RID: 1663 RVA: 0x0001C06C File Offset: 0x0001A26C
		public MimeMessageReader(Stream stream)
		{
			if (stream == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("stream");
			}
			this.reader = new DelimittedStreamReader(stream);
			this.mimeHeaderReader = new MimeHeaderReader(this.reader.GetNextStream(MimeMessageReader.CRLFCRLF));
		}

		// Token: 0x06000680 RID: 1664 RVA: 0x0001C0A9 File Offset: 0x0001A2A9
		public Stream GetContentStream()
		{
			if (this.getContentStreamCalled)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(System.Runtime.Serialization.SR.GetString("On MimeMessage, GetContentStream method is already called.")));
			}
			this.mimeHeaderReader.Close();
			Stream nextStream = this.reader.GetNextStream(null);
			this.getContentStreamCalled = true;
			return nextStream;
		}

		// Token: 0x06000681 RID: 1665 RVA: 0x0001C0E8 File Offset: 0x0001A2E8
		public MimeHeaders ReadHeaders(int maxBuffer, ref int remaining)
		{
			MimeHeaders mimeHeaders = new MimeHeaders();
			while (this.mimeHeaderReader.Read(maxBuffer, ref remaining))
			{
				mimeHeaders.Add(this.mimeHeaderReader.Name, this.mimeHeaderReader.Value, ref remaining);
			}
			return mimeHeaders;
		}

		// Token: 0x06000682 RID: 1666 RVA: 0x0001C12A File Offset: 0x0001A32A
		// Note: this type is marked as 'beforefieldinit'.
		static MimeMessageReader()
		{
		}

		// Token: 0x040002D9 RID: 729
		private static byte[] CRLFCRLF = new byte[]
		{
			13,
			10,
			13,
			10
		};

		// Token: 0x040002DA RID: 730
		private bool getContentStreamCalled;

		// Token: 0x040002DB RID: 731
		private MimeHeaderReader mimeHeaderReader;

		// Token: 0x040002DC RID: 732
		private DelimittedStreamReader reader;
	}
}
