﻿using System;

namespace System.Xml
{
	// Token: 0x02000025 RID: 37
	internal static class ValueHandleLength
	{
		// Token: 0x04000074 RID: 116
		public const int Int8 = 1;

		// Token: 0x04000075 RID: 117
		public const int Int16 = 2;

		// Token: 0x04000076 RID: 118
		public const int Int32 = 4;

		// Token: 0x04000077 RID: 119
		public const int Int64 = 8;

		// Token: 0x04000078 RID: 120
		public const int UInt64 = 8;

		// Token: 0x04000079 RID: 121
		public const int Single = 4;

		// Token: 0x0400007A RID: 122
		public const int Double = 8;

		// Token: 0x0400007B RID: 123
		public const int Decimal = 16;

		// Token: 0x0400007C RID: 124
		public const int DateTime = 8;

		// Token: 0x0400007D RID: 125
		public const int TimeSpan = 8;

		// Token: 0x0400007E RID: 126
		public const int Guid = 16;

		// Token: 0x0400007F RID: 127
		public const int UniqueId = 16;
	}
}
