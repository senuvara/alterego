﻿using System;

namespace System.Xml
{
	// Token: 0x02000005 RID: 5
	internal class BooleanArrayHelperWithString : ArrayHelper<string, bool>
	{
		// Token: 0x0600000F RID: 15 RVA: 0x0000221F File Offset: 0x0000041F
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, bool[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000010 RID: 16 RVA: 0x0000222F File Offset: 0x0000042F
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, bool[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002241 File Offset: 0x00000441
		public BooleanArrayHelperWithString()
		{
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002249 File Offset: 0x00000449
		// Note: this type is marked as 'beforefieldinit'.
		static BooleanArrayHelperWithString()
		{
		}

		// Token: 0x04000003 RID: 3
		public static readonly BooleanArrayHelperWithString Instance = new BooleanArrayHelperWithString();
	}
}
