﻿using System;

namespace System.Xml
{
	// Token: 0x0200000B RID: 11
	internal class Int64ArrayHelperWithString : ArrayHelper<string, long>
	{
		// Token: 0x06000027 RID: 39 RVA: 0x00002363 File Offset: 0x00000563
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, long[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00002373 File Offset: 0x00000573
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, long[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002385 File Offset: 0x00000585
		public Int64ArrayHelperWithString()
		{
		}

		// Token: 0x0600002A RID: 42 RVA: 0x0000238D File Offset: 0x0000058D
		// Note: this type is marked as 'beforefieldinit'.
		static Int64ArrayHelperWithString()
		{
		}

		// Token: 0x04000009 RID: 9
		public static readonly Int64ArrayHelperWithString Instance = new Int64ArrayHelperWithString();
	}
}
