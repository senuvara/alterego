﻿using System;

namespace System.Xml
{
	// Token: 0x02000087 RID: 135
	internal enum MimeWriterState
	{
		// Token: 0x0400035B RID: 859
		Start,
		// Token: 0x0400035C RID: 860
		StartPreface,
		// Token: 0x0400035D RID: 861
		StartPart,
		// Token: 0x0400035E RID: 862
		Header,
		// Token: 0x0400035F RID: 863
		Content,
		// Token: 0x04000360 RID: 864
		Closed
	}
}
