﻿using System;

namespace System.Xml
{
	// Token: 0x02000014 RID: 20
	internal class DateTimeArrayHelperWithDictionaryString : ArrayHelper<XmlDictionaryString, DateTime>
	{
		// Token: 0x0600004B RID: 75 RVA: 0x00002549 File Offset: 0x00000749
		protected override int ReadArray(XmlDictionaryReader reader, XmlDictionaryString localName, XmlDictionaryString namespaceUri, DateTime[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00002559 File Offset: 0x00000759
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, DateTime[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x0600004D RID: 77 RVA: 0x0000256B File Offset: 0x0000076B
		public DateTimeArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002573 File Offset: 0x00000773
		// Note: this type is marked as 'beforefieldinit'.
		static DateTimeArrayHelperWithDictionaryString()
		{
		}

		// Token: 0x04000012 RID: 18
		public static readonly DateTimeArrayHelperWithDictionaryString Instance = new DateTimeArrayHelperWithDictionaryString();
	}
}
