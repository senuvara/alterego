﻿using System;

namespace System.Xml
{
	// Token: 0x02000009 RID: 9
	internal class Int32ArrayHelperWithString : ArrayHelper<string, int>
	{
		// Token: 0x0600001F RID: 31 RVA: 0x000022F7 File Offset: 0x000004F7
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, int[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002307 File Offset: 0x00000507
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, int[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002319 File Offset: 0x00000519
		public Int32ArrayHelperWithString()
		{
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002321 File Offset: 0x00000521
		// Note: this type is marked as 'beforefieldinit'.
		static Int32ArrayHelperWithString()
		{
		}

		// Token: 0x04000007 RID: 7
		public static readonly Int32ArrayHelperWithString Instance = new Int32ArrayHelperWithString();
	}
}
