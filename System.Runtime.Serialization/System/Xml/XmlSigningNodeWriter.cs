﻿using System;
using System.IO;
using System.Text;

namespace System.Xml
{
	// Token: 0x02000090 RID: 144
	internal class XmlSigningNodeWriter : XmlNodeWriter
	{
		// Token: 0x060007B4 RID: 1972 RVA: 0x0001FAE0 File Offset: 0x0001DCE0
		public XmlSigningNodeWriter(bool text)
		{
			this.text = text;
		}

		// Token: 0x060007B5 RID: 1973 RVA: 0x0001FAEF File Offset: 0x0001DCEF
		public void SetOutput(XmlNodeWriter writer, Stream stream, bool includeComments, string[] inclusivePrefixes)
		{
			this.writer = writer;
			if (this.signingWriter == null)
			{
				this.signingWriter = new XmlCanonicalWriter();
			}
			this.signingWriter.SetOutput(stream, includeComments, inclusivePrefixes);
			this.chars = new byte[64];
			this.base64Chars = null;
		}

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x060007B6 RID: 1974 RVA: 0x0001FB2E File Offset: 0x0001DD2E
		// (set) Token: 0x060007B7 RID: 1975 RVA: 0x0001FB36 File Offset: 0x0001DD36
		public XmlNodeWriter NodeWriter
		{
			get
			{
				return this.writer;
			}
			set
			{
				this.writer = value;
			}
		}

		// Token: 0x1700010A RID: 266
		// (get) Token: 0x060007B8 RID: 1976 RVA: 0x0001FB3F File Offset: 0x0001DD3F
		public XmlCanonicalWriter CanonicalWriter
		{
			get
			{
				return this.signingWriter;
			}
		}

		// Token: 0x060007B9 RID: 1977 RVA: 0x0001FB47 File Offset: 0x0001DD47
		public override void Flush()
		{
			this.writer.Flush();
			this.signingWriter.Flush();
		}

		// Token: 0x060007BA RID: 1978 RVA: 0x0001FB5F File Offset: 0x0001DD5F
		public override void Close()
		{
			this.writer.Close();
			this.signingWriter.Close();
		}

		// Token: 0x060007BB RID: 1979 RVA: 0x0001FB77 File Offset: 0x0001DD77
		public override void WriteDeclaration()
		{
			this.writer.WriteDeclaration();
			this.signingWriter.WriteDeclaration();
		}

		// Token: 0x060007BC RID: 1980 RVA: 0x0001FB8F File Offset: 0x0001DD8F
		public override void WriteComment(string text)
		{
			this.writer.WriteComment(text);
			this.signingWriter.WriteComment(text);
		}

		// Token: 0x060007BD RID: 1981 RVA: 0x0001FBA9 File Offset: 0x0001DDA9
		public override void WriteCData(string text)
		{
			this.writer.WriteCData(text);
			this.signingWriter.WriteEscapedText(text);
		}

		// Token: 0x060007BE RID: 1982 RVA: 0x0001FBC3 File Offset: 0x0001DDC3
		public override void WriteStartElement(string prefix, string localName)
		{
			this.writer.WriteStartElement(prefix, localName);
			this.signingWriter.WriteStartElement(prefix, localName);
		}

		// Token: 0x060007BF RID: 1983 RVA: 0x0001FBDF File Offset: 0x0001DDDF
		public override void WriteStartElement(byte[] prefixBuffer, int prefixOffset, int prefixLength, byte[] localNameBuffer, int localNameOffset, int localNameLength)
		{
			this.writer.WriteStartElement(prefixBuffer, prefixOffset, prefixLength, localNameBuffer, localNameOffset, localNameLength);
			this.signingWriter.WriteStartElement(prefixBuffer, prefixOffset, prefixLength, localNameBuffer, localNameOffset, localNameLength);
		}

		// Token: 0x060007C0 RID: 1984 RVA: 0x0001FC09 File Offset: 0x0001DE09
		public override void WriteStartElement(string prefix, XmlDictionaryString localName)
		{
			this.writer.WriteStartElement(prefix, localName);
			this.signingWriter.WriteStartElement(prefix, localName.Value);
		}

		// Token: 0x060007C1 RID: 1985 RVA: 0x0001FC2A File Offset: 0x0001DE2A
		public override void WriteEndStartElement(bool isEmpty)
		{
			this.writer.WriteEndStartElement(isEmpty);
			this.signingWriter.WriteEndStartElement(isEmpty);
		}

		// Token: 0x060007C2 RID: 1986 RVA: 0x0001FC44 File Offset: 0x0001DE44
		public override void WriteEndElement(string prefix, string localName)
		{
			this.writer.WriteEndElement(prefix, localName);
			this.signingWriter.WriteEndElement(prefix, localName);
		}

		// Token: 0x060007C3 RID: 1987 RVA: 0x0001FC60 File Offset: 0x0001DE60
		public override void WriteXmlnsAttribute(string prefix, string ns)
		{
			this.writer.WriteXmlnsAttribute(prefix, ns);
			this.signingWriter.WriteXmlnsAttribute(prefix, ns);
		}

		// Token: 0x060007C4 RID: 1988 RVA: 0x0001FC7C File Offset: 0x0001DE7C
		public override void WriteXmlnsAttribute(byte[] prefixBuffer, int prefixOffset, int prefixLength, byte[] nsBuffer, int nsOffset, int nsLength)
		{
			this.writer.WriteXmlnsAttribute(prefixBuffer, prefixOffset, prefixLength, nsBuffer, nsOffset, nsLength);
			this.signingWriter.WriteXmlnsAttribute(prefixBuffer, prefixOffset, prefixLength, nsBuffer, nsOffset, nsLength);
		}

		// Token: 0x060007C5 RID: 1989 RVA: 0x0001FCA6 File Offset: 0x0001DEA6
		public override void WriteXmlnsAttribute(string prefix, XmlDictionaryString ns)
		{
			this.writer.WriteXmlnsAttribute(prefix, ns);
			this.signingWriter.WriteXmlnsAttribute(prefix, ns.Value);
		}

		// Token: 0x060007C6 RID: 1990 RVA: 0x0001FCC7 File Offset: 0x0001DEC7
		public override void WriteStartAttribute(string prefix, string localName)
		{
			this.writer.WriteStartAttribute(prefix, localName);
			this.signingWriter.WriteStartAttribute(prefix, localName);
		}

		// Token: 0x060007C7 RID: 1991 RVA: 0x0001FCE3 File Offset: 0x0001DEE3
		public override void WriteStartAttribute(byte[] prefixBuffer, int prefixOffset, int prefixLength, byte[] localNameBuffer, int localNameOffset, int localNameLength)
		{
			this.writer.WriteStartAttribute(prefixBuffer, prefixOffset, prefixLength, localNameBuffer, localNameOffset, localNameLength);
			this.signingWriter.WriteStartAttribute(prefixBuffer, prefixOffset, prefixLength, localNameBuffer, localNameOffset, localNameLength);
		}

		// Token: 0x060007C8 RID: 1992 RVA: 0x0001FD0D File Offset: 0x0001DF0D
		public override void WriteStartAttribute(string prefix, XmlDictionaryString localName)
		{
			this.writer.WriteStartAttribute(prefix, localName);
			this.signingWriter.WriteStartAttribute(prefix, localName.Value);
		}

		// Token: 0x060007C9 RID: 1993 RVA: 0x0001FD2E File Offset: 0x0001DF2E
		public override void WriteEndAttribute()
		{
			this.writer.WriteEndAttribute();
			this.signingWriter.WriteEndAttribute();
		}

		// Token: 0x060007CA RID: 1994 RVA: 0x0001FD46 File Offset: 0x0001DF46
		public override void WriteCharEntity(int ch)
		{
			this.writer.WriteCharEntity(ch);
			this.signingWriter.WriteCharEntity(ch);
		}

		// Token: 0x060007CB RID: 1995 RVA: 0x0001FD60 File Offset: 0x0001DF60
		public override void WriteEscapedText(string value)
		{
			this.writer.WriteEscapedText(value);
			this.signingWriter.WriteEscapedText(value);
		}

		// Token: 0x060007CC RID: 1996 RVA: 0x0001FD7A File Offset: 0x0001DF7A
		public override void WriteEscapedText(char[] chars, int offset, int count)
		{
			this.writer.WriteEscapedText(chars, offset, count);
			this.signingWriter.WriteEscapedText(chars, offset, count);
		}

		// Token: 0x060007CD RID: 1997 RVA: 0x0001FD98 File Offset: 0x0001DF98
		public override void WriteEscapedText(XmlDictionaryString value)
		{
			this.writer.WriteEscapedText(value);
			this.signingWriter.WriteEscapedText(value.Value);
		}

		// Token: 0x060007CE RID: 1998 RVA: 0x0001FDB7 File Offset: 0x0001DFB7
		public override void WriteEscapedText(byte[] chars, int offset, int count)
		{
			this.writer.WriteEscapedText(chars, offset, count);
			this.signingWriter.WriteEscapedText(chars, offset, count);
		}

		// Token: 0x060007CF RID: 1999 RVA: 0x0001FDD5 File Offset: 0x0001DFD5
		public override void WriteText(string value)
		{
			this.writer.WriteText(value);
			this.signingWriter.WriteText(value);
		}

		// Token: 0x060007D0 RID: 2000 RVA: 0x0001FDEF File Offset: 0x0001DFEF
		public override void WriteText(char[] chars, int offset, int count)
		{
			this.writer.WriteText(chars, offset, count);
			this.signingWriter.WriteText(chars, offset, count);
		}

		// Token: 0x060007D1 RID: 2001 RVA: 0x0001FE0D File Offset: 0x0001E00D
		public override void WriteText(byte[] chars, int offset, int count)
		{
			this.writer.WriteText(chars, offset, count);
			this.signingWriter.WriteText(chars, offset, count);
		}

		// Token: 0x060007D2 RID: 2002 RVA: 0x0001FE2B File Offset: 0x0001E02B
		public override void WriteText(XmlDictionaryString value)
		{
			this.writer.WriteText(value);
			this.signingWriter.WriteText(value.Value);
		}

		// Token: 0x060007D3 RID: 2003 RVA: 0x0001FE4C File Offset: 0x0001E04C
		public override void WriteInt32Text(int value)
		{
			int count = XmlConverter.ToChars(value, this.chars, 0);
			if (this.text)
			{
				this.writer.WriteText(this.chars, 0, count);
			}
			else
			{
				this.writer.WriteInt32Text(value);
			}
			this.signingWriter.WriteText(this.chars, 0, count);
		}

		// Token: 0x060007D4 RID: 2004 RVA: 0x0001FEA4 File Offset: 0x0001E0A4
		public override void WriteInt64Text(long value)
		{
			int count = XmlConverter.ToChars(value, this.chars, 0);
			if (this.text)
			{
				this.writer.WriteText(this.chars, 0, count);
			}
			else
			{
				this.writer.WriteInt64Text(value);
			}
			this.signingWriter.WriteText(this.chars, 0, count);
		}

		// Token: 0x060007D5 RID: 2005 RVA: 0x0001FEFC File Offset: 0x0001E0FC
		public override void WriteBoolText(bool value)
		{
			int count = XmlConverter.ToChars(value, this.chars, 0);
			if (this.text)
			{
				this.writer.WriteText(this.chars, 0, count);
			}
			else
			{
				this.writer.WriteBoolText(value);
			}
			this.signingWriter.WriteText(this.chars, 0, count);
		}

		// Token: 0x060007D6 RID: 2006 RVA: 0x0001FF54 File Offset: 0x0001E154
		public override void WriteUInt64Text(ulong value)
		{
			int count = XmlConverter.ToChars(value, this.chars, 0);
			if (this.text)
			{
				this.writer.WriteText(this.chars, 0, count);
			}
			else
			{
				this.writer.WriteUInt64Text(value);
			}
			this.signingWriter.WriteText(this.chars, 0, count);
		}

		// Token: 0x060007D7 RID: 2007 RVA: 0x0001FFAC File Offset: 0x0001E1AC
		public override void WriteFloatText(float value)
		{
			int count = XmlConverter.ToChars(value, this.chars, 0);
			if (this.text)
			{
				this.writer.WriteText(this.chars, 0, count);
			}
			else
			{
				this.writer.WriteFloatText(value);
			}
			this.signingWriter.WriteText(this.chars, 0, count);
		}

		// Token: 0x060007D8 RID: 2008 RVA: 0x00020004 File Offset: 0x0001E204
		public override void WriteDoubleText(double value)
		{
			int count = XmlConverter.ToChars(value, this.chars, 0);
			if (this.text)
			{
				this.writer.WriteText(this.chars, 0, count);
			}
			else
			{
				this.writer.WriteDoubleText(value);
			}
			this.signingWriter.WriteText(this.chars, 0, count);
		}

		// Token: 0x060007D9 RID: 2009 RVA: 0x0002005C File Offset: 0x0001E25C
		public override void WriteDecimalText(decimal value)
		{
			int count = XmlConverter.ToChars(value, this.chars, 0);
			if (this.text)
			{
				this.writer.WriteText(this.chars, 0, count);
			}
			else
			{
				this.writer.WriteDecimalText(value);
			}
			this.signingWriter.WriteText(this.chars, 0, count);
		}

		// Token: 0x060007DA RID: 2010 RVA: 0x000200B4 File Offset: 0x0001E2B4
		public override void WriteDateTimeText(DateTime value)
		{
			int count = XmlConverter.ToChars(value, this.chars, 0);
			if (this.text)
			{
				this.writer.WriteText(this.chars, 0, count);
			}
			else
			{
				this.writer.WriteDateTimeText(value);
			}
			this.signingWriter.WriteText(this.chars, 0, count);
		}

		// Token: 0x060007DB RID: 2011 RVA: 0x0002010C File Offset: 0x0001E30C
		public override void WriteUniqueIdText(UniqueId value)
		{
			string value2 = XmlConverter.ToString(value);
			if (this.text)
			{
				this.writer.WriteText(value2);
			}
			else
			{
				this.writer.WriteUniqueIdText(value);
			}
			this.signingWriter.WriteText(value2);
		}

		// Token: 0x060007DC RID: 2012 RVA: 0x00020150 File Offset: 0x0001E350
		public override void WriteTimeSpanText(TimeSpan value)
		{
			string value2 = XmlConverter.ToString(value);
			if (this.text)
			{
				this.writer.WriteText(value2);
			}
			else
			{
				this.writer.WriteTimeSpanText(value);
			}
			this.signingWriter.WriteText(value2);
		}

		// Token: 0x060007DD RID: 2013 RVA: 0x00020194 File Offset: 0x0001E394
		public override void WriteGuidText(Guid value)
		{
			string value2 = XmlConverter.ToString(value);
			if (this.text)
			{
				this.writer.WriteText(value2);
			}
			else
			{
				this.writer.WriteGuidText(value);
			}
			this.signingWriter.WriteText(value2);
		}

		// Token: 0x060007DE RID: 2014 RVA: 0x000201D6 File Offset: 0x0001E3D6
		public override void WriteStartListText()
		{
			this.writer.WriteStartListText();
		}

		// Token: 0x060007DF RID: 2015 RVA: 0x000201E3 File Offset: 0x0001E3E3
		public override void WriteListSeparator()
		{
			this.writer.WriteListSeparator();
			this.signingWriter.WriteText(32);
		}

		// Token: 0x060007E0 RID: 2016 RVA: 0x000201FD File Offset: 0x0001E3FD
		public override void WriteEndListText()
		{
			this.writer.WriteEndListText();
		}

		// Token: 0x060007E1 RID: 2017 RVA: 0x0002020A File Offset: 0x0001E40A
		public override void WriteBase64Text(byte[] trailBytes, int trailByteCount, byte[] buffer, int offset, int count)
		{
			if (trailByteCount > 0)
			{
				this.WriteBase64Text(trailBytes, 0, trailByteCount);
			}
			this.WriteBase64Text(buffer, offset, count);
			if (!this.text)
			{
				this.writer.WriteBase64Text(trailBytes, trailByteCount, buffer, offset, count);
			}
		}

		// Token: 0x060007E2 RID: 2018 RVA: 0x00020240 File Offset: 0x0001E440
		private void WriteBase64Text(byte[] buffer, int offset, int count)
		{
			if (this.base64Chars == null)
			{
				this.base64Chars = new byte[512];
			}
			Base64Encoding base64Encoding = XmlConverter.Base64Encoding;
			while (count >= 3)
			{
				int num = Math.Min(this.base64Chars.Length / 4 * 3, count - count % 3);
				int count2 = num / 3 * 4;
				base64Encoding.GetChars(buffer, offset, num, this.base64Chars, 0);
				this.signingWriter.WriteText(this.base64Chars, 0, count2);
				if (this.text)
				{
					this.writer.WriteText(this.base64Chars, 0, count2);
				}
				offset += num;
				count -= num;
			}
			if (count > 0)
			{
				base64Encoding.GetChars(buffer, offset, count, this.base64Chars, 0);
				this.signingWriter.WriteText(this.base64Chars, 0, 4);
				if (this.text)
				{
					this.writer.WriteText(this.base64Chars, 0, 4);
				}
			}
		}

		// Token: 0x060007E3 RID: 2019 RVA: 0x0002031C File Offset: 0x0001E51C
		public override void WriteQualifiedName(string prefix, XmlDictionaryString localName)
		{
			this.writer.WriteQualifiedName(prefix, localName);
			if (prefix.Length != 0)
			{
				this.signingWriter.WriteText(prefix);
				this.signingWriter.WriteText(":");
			}
			this.signingWriter.WriteText(localName.Value);
		}

		// Token: 0x0400037A RID: 890
		private XmlNodeWriter writer;

		// Token: 0x0400037B RID: 891
		private XmlCanonicalWriter signingWriter;

		// Token: 0x0400037C RID: 892
		private byte[] chars;

		// Token: 0x0400037D RID: 893
		private byte[] base64Chars;

		// Token: 0x0400037E RID: 894
		private bool text;
	}
}
