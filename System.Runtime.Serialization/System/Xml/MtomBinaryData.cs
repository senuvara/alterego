﻿using System;

namespace System.Xml
{
	// Token: 0x0200008B RID: 139
	internal class MtomBinaryData
	{
		// Token: 0x06000748 RID: 1864 RVA: 0x0001F8F7 File Offset: 0x0001DAF7
		internal MtomBinaryData(IStreamProvider provider)
		{
			this.type = MtomBinaryDataType.Provider;
			this.provider = provider;
		}

		// Token: 0x06000749 RID: 1865 RVA: 0x0001F90D File Offset: 0x0001DB0D
		internal MtomBinaryData(byte[] buffer, int offset, int count)
		{
			this.type = MtomBinaryDataType.Segment;
			this.chunk = new byte[count];
			Buffer.BlockCopy(buffer, offset, this.chunk, 0, count);
		}

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x0600074A RID: 1866 RVA: 0x0001F937 File Offset: 0x0001DB37
		internal long Length
		{
			get
			{
				if (this.type == MtomBinaryDataType.Segment)
				{
					return (long)this.chunk.Length;
				}
				return -1L;
			}
		}

		// Token: 0x0400036B RID: 875
		internal MtomBinaryDataType type;

		// Token: 0x0400036C RID: 876
		internal IStreamProvider provider;

		// Token: 0x0400036D RID: 877
		internal byte[] chunk;
	}
}
