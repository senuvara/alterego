﻿using System;

namespace System.Xml
{
	// Token: 0x0200007B RID: 123
	internal class ContentIDHeader : MimeHeader
	{
		// Token: 0x060006BA RID: 1722 RVA: 0x0001CE6C File Offset: 0x0001B06C
		public ContentIDHeader(string name, string value) : base(name, value)
		{
		}
	}
}
