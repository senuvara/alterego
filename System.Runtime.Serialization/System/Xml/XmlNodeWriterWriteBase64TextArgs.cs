﻿using System;
using System.Runtime.CompilerServices;

namespace System.Xml
{
	// Token: 0x0200008F RID: 143
	internal class XmlNodeWriterWriteBase64TextArgs
	{
		// Token: 0x17000104 RID: 260
		// (get) Token: 0x060007A9 RID: 1961 RVA: 0x0001FA8B File Offset: 0x0001DC8B
		// (set) Token: 0x060007AA RID: 1962 RVA: 0x0001FA93 File Offset: 0x0001DC93
		internal byte[] TrailBuffer
		{
			[CompilerGenerated]
			get
			{
				return this.<TrailBuffer>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TrailBuffer>k__BackingField = value;
			}
		}

		// Token: 0x17000105 RID: 261
		// (get) Token: 0x060007AB RID: 1963 RVA: 0x0001FA9C File Offset: 0x0001DC9C
		// (set) Token: 0x060007AC RID: 1964 RVA: 0x0001FAA4 File Offset: 0x0001DCA4
		internal int TrailCount
		{
			[CompilerGenerated]
			get
			{
				return this.<TrailCount>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TrailCount>k__BackingField = value;
			}
		}

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x060007AD RID: 1965 RVA: 0x0001FAAD File Offset: 0x0001DCAD
		// (set) Token: 0x060007AE RID: 1966 RVA: 0x0001FAB5 File Offset: 0x0001DCB5
		internal byte[] Buffer
		{
			[CompilerGenerated]
			get
			{
				return this.<Buffer>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Buffer>k__BackingField = value;
			}
		}

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x060007AF RID: 1967 RVA: 0x0001FABE File Offset: 0x0001DCBE
		// (set) Token: 0x060007B0 RID: 1968 RVA: 0x0001FAC6 File Offset: 0x0001DCC6
		internal int Offset
		{
			[CompilerGenerated]
			get
			{
				return this.<Offset>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Offset>k__BackingField = value;
			}
		}

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x060007B1 RID: 1969 RVA: 0x0001FACF File Offset: 0x0001DCCF
		// (set) Token: 0x060007B2 RID: 1970 RVA: 0x0001FAD7 File Offset: 0x0001DCD7
		internal int Count
		{
			[CompilerGenerated]
			get
			{
				return this.<Count>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Count>k__BackingField = value;
			}
		}

		// Token: 0x060007B3 RID: 1971 RVA: 0x00002217 File Offset: 0x00000417
		public XmlNodeWriterWriteBase64TextArgs()
		{
		}

		// Token: 0x04000375 RID: 885
		[CompilerGenerated]
		private byte[] <TrailBuffer>k__BackingField;

		// Token: 0x04000376 RID: 886
		[CompilerGenerated]
		private int <TrailCount>k__BackingField;

		// Token: 0x04000377 RID: 887
		[CompilerGenerated]
		private byte[] <Buffer>k__BackingField;

		// Token: 0x04000378 RID: 888
		[CompilerGenerated]
		private int <Offset>k__BackingField;

		// Token: 0x04000379 RID: 889
		[CompilerGenerated]
		private int <Count>k__BackingField;
	}
}
