﻿using System;

namespace System.Xml
{
	// Token: 0x0200001E RID: 30
	internal enum PrefixHandleType
	{
		// Token: 0x04000034 RID: 52
		Empty,
		// Token: 0x04000035 RID: 53
		A,
		// Token: 0x04000036 RID: 54
		B,
		// Token: 0x04000037 RID: 55
		C,
		// Token: 0x04000038 RID: 56
		D,
		// Token: 0x04000039 RID: 57
		E,
		// Token: 0x0400003A RID: 58
		F,
		// Token: 0x0400003B RID: 59
		G,
		// Token: 0x0400003C RID: 60
		H,
		// Token: 0x0400003D RID: 61
		I,
		// Token: 0x0400003E RID: 62
		J,
		// Token: 0x0400003F RID: 63
		K,
		// Token: 0x04000040 RID: 64
		L,
		// Token: 0x04000041 RID: 65
		M,
		// Token: 0x04000042 RID: 66
		N,
		// Token: 0x04000043 RID: 67
		O,
		// Token: 0x04000044 RID: 68
		P,
		// Token: 0x04000045 RID: 69
		Q,
		// Token: 0x04000046 RID: 70
		R,
		// Token: 0x04000047 RID: 71
		S,
		// Token: 0x04000048 RID: 72
		T,
		// Token: 0x04000049 RID: 73
		U,
		// Token: 0x0400004A RID: 74
		V,
		// Token: 0x0400004B RID: 75
		W,
		// Token: 0x0400004C RID: 76
		X,
		// Token: 0x0400004D RID: 77
		Y,
		// Token: 0x0400004E RID: 78
		Z,
		// Token: 0x0400004F RID: 79
		Buffer,
		// Token: 0x04000050 RID: 80
		Max
	}
}
