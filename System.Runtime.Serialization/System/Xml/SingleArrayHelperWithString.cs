﻿using System;

namespace System.Xml
{
	// Token: 0x0200000D RID: 13
	internal class SingleArrayHelperWithString : ArrayHelper<string, float>
	{
		// Token: 0x0600002F RID: 47 RVA: 0x000023CF File Offset: 0x000005CF
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, float[] array, int offset, int count)
		{
			return reader.ReadArray(localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000030 RID: 48 RVA: 0x000023DF File Offset: 0x000005DF
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, float[] array, int offset, int count)
		{
			writer.WriteArray(prefix, localName, namespaceUri, array, offset, count);
		}

		// Token: 0x06000031 RID: 49 RVA: 0x000023F1 File Offset: 0x000005F1
		public SingleArrayHelperWithString()
		{
		}

		// Token: 0x06000032 RID: 50 RVA: 0x000023F9 File Offset: 0x000005F9
		// Note: this type is marked as 'beforefieldinit'.
		static SingleArrayHelperWithString()
		{
		}

		// Token: 0x0400000B RID: 11
		public static readonly SingleArrayHelperWithString Instance = new SingleArrayHelperWithString();
	}
}
