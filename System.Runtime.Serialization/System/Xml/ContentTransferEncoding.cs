﻿using System;

namespace System.Xml
{
	// Token: 0x02000079 RID: 121
	internal enum ContentTransferEncoding
	{
		// Token: 0x040002FB RID: 763
		SevenBit,
		// Token: 0x040002FC RID: 764
		EightBit,
		// Token: 0x040002FD RID: 765
		Binary,
		// Token: 0x040002FE RID: 766
		Other,
		// Token: 0x040002FF RID: 767
		Unspecified
	}
}
