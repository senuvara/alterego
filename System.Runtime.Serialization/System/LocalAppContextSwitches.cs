﻿using System;

namespace System
{
	// Token: 0x02000003 RID: 3
	internal static class LocalAppContextSwitches
	{
		// Token: 0x06000009 RID: 9 RVA: 0x000020AE File Offset: 0x000002AE
		// Note: this type is marked as 'beforefieldinit'.
		static LocalAppContextSwitches()
		{
		}

		// Token: 0x04000001 RID: 1
		public static readonly bool DoNotUseTimeZoneInfo;

		// Token: 0x04000002 RID: 2
		public static readonly bool DoNotUseEcmaScriptV6EscapeControlCharacter;
	}
}
