﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x020000A6 RID: 166
	internal class FriendAccessAllowedAttribute : Attribute
	{
		// Token: 0x060008FC RID: 2300 RVA: 0x000254EB File Offset: 0x000236EB
		public FriendAccessAllowedAttribute()
		{
		}
	}
}
