﻿using System;
using System.Collections.Specialized;

namespace System.Runtime.Serialization
{
	// Token: 0x020000A8 RID: 168
	internal static class AppSettings
	{
		// Token: 0x1700011E RID: 286
		// (get) Token: 0x06000900 RID: 2304 RVA: 0x000254F3 File Offset: 0x000236F3
		internal static int MaxMimeParts
		{
			get
			{
				AppSettings.EnsureSettingsLoaded();
				return AppSettings.maxMimeParts;
			}
		}

		// Token: 0x06000901 RID: 2305 RVA: 0x00025500 File Offset: 0x00023700
		private static void EnsureSettingsLoaded()
		{
			if (!AppSettings.settingsInitalized)
			{
				object obj = AppSettings.appSettingsLock;
				lock (obj)
				{
					if (!AppSettings.settingsInitalized)
					{
						NameValueCollection nameValueCollection = null;
						if (nameValueCollection == null || !int.TryParse(nameValueCollection["microsoft:xmldictionaryreader:maxmimeparts"], out AppSettings.maxMimeParts))
						{
							AppSettings.maxMimeParts = 1000;
						}
						AppSettings.settingsInitalized = true;
					}
				}
			}
		}

		// Token: 0x06000902 RID: 2306 RVA: 0x0002557C File Offset: 0x0002377C
		// Note: this type is marked as 'beforefieldinit'.
		static AppSettings()
		{
		}

		// Token: 0x040003E6 RID: 998
		internal const string MaxMimePartsAppSettingsString = "microsoft:xmldictionaryreader:maxmimeparts";

		// Token: 0x040003E7 RID: 999
		private const int DefaultMaxMimeParts = 1000;

		// Token: 0x040003E8 RID: 1000
		private static int maxMimeParts;

		// Token: 0x040003E9 RID: 1001
		private static volatile bool settingsInitalized = false;

		// Token: 0x040003EA RID: 1002
		private static object appSettingsLock = new object();
	}
}
