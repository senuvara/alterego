﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;

namespace System.Runtime.Serialization
{
	/// <summary>Allows the transformation of a set of .NET Framework types that are used in data contracts into an XML schema file (.xsd). </summary>
	// Token: 0x0200015A RID: 346
	public class XsdDataContractExporter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.XsdDataContractExporter" /> class.  </summary>
		// Token: 0x0600118E RID: 4494 RVA: 0x000402BF File Offset: 0x0003E4BF
		public XsdDataContractExporter()
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.XsdDataContractExporter" /> class with the specified set of schemas.  </summary>
		/// <param name="schemas">An <see cref="T:System.Xml.Schema.XmlSchemaSet" /> that contains the schemas to be exported. </param>
		// Token: 0x0600118F RID: 4495 RVA: 0x000402BF File Offset: 0x0003E4BF
		public XsdDataContractExporter(XmlSchemaSet schemas)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets or sets an <see cref="T:System.Runtime.Serialization.ExportOptions" /> that contains options that can be set for the export operation. </summary>
		/// <returns>An <see cref="T:System.Runtime.Serialization.ExportOptions" /> that contains options used to customize how types are exported to schemas.</returns>
		// Token: 0x170003C4 RID: 964
		// (get) Token: 0x06001190 RID: 4496 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		// (set) Token: 0x06001191 RID: 4497 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public ExportOptions Options
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the collection of exported XML schemas. </summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaSet" /> that contains the schemas transformed from the set of common language runtime (CLR) types after calling the <see cref="Overload:System.Runtime.Serialization.XsdDataContractExporter.Export" /> method.</returns>
		// Token: 0x170003C5 RID: 965
		// (get) Token: 0x06001192 RID: 4498 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public XmlSchemaSet Schemas
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a value that indicates whether the set of .common language runtime (CLR) types contained in a set of assemblies can be exported. </summary>
		/// <param name="assemblies">A <see cref="T:System.Collections.Generic.ICollection`1" />   of <see cref="T:System.Reflection.Assembly" /> that contains the assemblies with the types to export.</param>
		/// <returns>
		///     <see langword="true" /> if the types can be exported; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001193 RID: 4499 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public bool CanExport(ICollection<Assembly> assemblies)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a value that indicates whether the set of .common language runtime (CLR) types contained in a <see cref="T:System.Collections.Generic.ICollection`1" /> can be exported. </summary>
		/// <param name="types">A <see cref="T:System.Collections.Generic.ICollection`1" />   that contains the specified types to export.</param>
		/// <returns>
		///     <see langword="true" /> if the types can be exported; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001194 RID: 4500 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public bool CanExport(ICollection<Type> types)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a value that indicates whether the specified common language runtime (CLR) type can be exported. </summary>
		/// <param name="type">The <see cref="T:System.Type" /> to export. </param>
		/// <returns>
		///     <see langword="true" /> if the type can be exported; otherwise, <see langword="false" />. </returns>
		// Token: 0x06001195 RID: 4501 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public bool CanExport(Type type)
		{
			throw new NotImplementedException();
		}

		/// <summary>Transforms the types contained in the specified collection of assemblies. </summary>
		/// <param name="assemblies">A <see cref="T:System.Collections.Generic.ICollection`1" />   (of <see cref="T:System.Reflection.Assembly" />) that contains the types to export.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="assemblies" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">An <see cref="T:System.Reflection.Assembly" /> in the collection is <see langword="null" />.</exception>
		// Token: 0x06001196 RID: 4502 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public void Export(ICollection<Assembly> assemblies)
		{
			throw new NotImplementedException();
		}

		/// <summary>Transforms the types contained in the <see cref="T:System.Collections.Generic.ICollection`1" /> passed to this method.</summary>
		/// <param name="types">A  <see cref="T:System.Collections.Generic.ICollection`1" /> (of <see cref="T:System.Type" />) that contains the types to export.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="types" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">A type in the collection is <see langword="null" />.</exception>
		// Token: 0x06001197 RID: 4503 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public void Export(ICollection<Type> types)
		{
			throw new NotImplementedException();
		}

		/// <summary>Transforms the specified .NET Framework type into an XML schema definition language (XSD) schema. </summary>
		/// <param name="type">The <see cref="T:System.Type" /> to transform into an XML schema. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="type" /> argument is <see langword="null" />.</exception>
		// Token: 0x06001198 RID: 4504 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public void Export(Type type)
		{
			throw new NotImplementedException();
		}

		/// <summary>Returns the top-level name and namespace for the <see cref="T:System.Type" />.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> to query.</param>
		/// <returns>The <see cref="T:System.Xml.XmlQualifiedName" /> that represents the top-level name and namespace for this <see cref="T:System.Type" />, which is written to the stream when writing this object. </returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="type" /> argument is <see langword="null" />.</exception>
		// Token: 0x06001199 RID: 4505 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public XmlQualifiedName GetRootElementName(Type type)
		{
			throw new NotImplementedException();
		}

		/// <summary>Returns the XML schema type for the specified type.</summary>
		/// <param name="type">The type to return a schema for.</param>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaType" /> that contains the XML schema. </returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="type" /> argument is <see langword="null" />.</exception>
		// Token: 0x0600119A RID: 4506 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public XmlSchemaType GetSchemaType(Type type)
		{
			throw new NotImplementedException();
		}

		/// <summary>Returns the contract name and contract namespace for the <see cref="T:System.Type" />.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> that was exported. </param>
		/// <returns>An <see cref="T:System.Xml.XmlQualifiedName" /> that represents the contract name of the type and its namespace.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="type" /> argument is <see langword="null" />.</exception>
		// Token: 0x0600119B RID: 4507 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		public XmlQualifiedName GetSchemaTypeName(Type type)
		{
			throw new NotImplementedException();
		}
	}
}
