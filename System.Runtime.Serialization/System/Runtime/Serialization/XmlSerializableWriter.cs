﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x02000148 RID: 328
	internal class XmlSerializableWriter : XmlWriter
	{
		// Token: 0x06001095 RID: 4245 RVA: 0x0003C080 File Offset: 0x0003A280
		internal void BeginWrite(XmlWriter xmlWriter, object obj)
		{
			this.depth = 0;
			this.xmlWriter = xmlWriter;
			this.obj = obj;
		}

		// Token: 0x06001096 RID: 4246 RVA: 0x0003C098 File Offset: 0x0003A298
		internal void EndWrite()
		{
			if (this.depth != 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("IXmlSerializable.WriteXml method of type '{0}' did not close all open tags. Verify that the IXmlSerializable implementation is correct.", new object[]
				{
					(this.obj == null) ? string.Empty : DataContract.GetClrTypeFullName(this.obj.GetType())
				})));
			}
			this.obj = null;
		}

		// Token: 0x06001097 RID: 4247 RVA: 0x0003C0F1 File Offset: 0x0003A2F1
		public override void WriteStartDocument()
		{
			if (this.WriteState == WriteState.Start)
			{
				this.xmlWriter.WriteStartDocument();
			}
		}

		// Token: 0x06001098 RID: 4248 RVA: 0x0003C106 File Offset: 0x0003A306
		public override void WriteEndDocument()
		{
			this.xmlWriter.WriteEndDocument();
		}

		// Token: 0x06001099 RID: 4249 RVA: 0x0003C113 File Offset: 0x0003A313
		public override void WriteStartDocument(bool standalone)
		{
			if (this.WriteState == WriteState.Start)
			{
				this.xmlWriter.WriteStartDocument(standalone);
			}
		}

		// Token: 0x0600109A RID: 4250 RVA: 0x000020AE File Offset: 0x000002AE
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
		}

		// Token: 0x0600109B RID: 4251 RVA: 0x0003C129 File Offset: 0x0003A329
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			this.xmlWriter.WriteStartElement(prefix, localName, ns);
			this.depth++;
		}

		// Token: 0x0600109C RID: 4252 RVA: 0x0003C148 File Offset: 0x0003A348
		public override void WriteEndElement()
		{
			if (this.depth == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("IXmlSerializable.WriteXml method of type '{0}' attempted to close too many tags.  Verify that the IXmlSerializable implementation is correct.", new object[]
				{
					(this.obj == null) ? string.Empty : DataContract.GetClrTypeFullName(this.obj.GetType())
				})));
			}
			this.xmlWriter.WriteEndElement();
			this.depth--;
		}

		// Token: 0x0600109D RID: 4253 RVA: 0x0003C1B4 File Offset: 0x0003A3B4
		public override void WriteFullEndElement()
		{
			if (this.depth == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("IXmlSerializable.WriteXml method of type '{0}' attempted to close too many tags.  Verify that the IXmlSerializable implementation is correct.", new object[]
				{
					(this.obj == null) ? string.Empty : DataContract.GetClrTypeFullName(this.obj.GetType())
				})));
			}
			this.xmlWriter.WriteFullEndElement();
			this.depth--;
		}

		// Token: 0x0600109E RID: 4254 RVA: 0x0003BA75 File Offset: 0x00039C75
		public override void Close()
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("This method cannot be called from IXmlSerializable implementations.")));
		}

		// Token: 0x0600109F RID: 4255 RVA: 0x0003C21F File Offset: 0x0003A41F
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			this.xmlWriter.WriteStartAttribute(prefix, localName, ns);
		}

		// Token: 0x060010A0 RID: 4256 RVA: 0x0003C22F File Offset: 0x0003A42F
		public override void WriteEndAttribute()
		{
			this.xmlWriter.WriteEndAttribute();
		}

		// Token: 0x060010A1 RID: 4257 RVA: 0x0003C23C File Offset: 0x0003A43C
		public override void WriteCData(string text)
		{
			this.xmlWriter.WriteCData(text);
		}

		// Token: 0x060010A2 RID: 4258 RVA: 0x0003C24A File Offset: 0x0003A44A
		public override void WriteComment(string text)
		{
			this.xmlWriter.WriteComment(text);
		}

		// Token: 0x060010A3 RID: 4259 RVA: 0x0003C258 File Offset: 0x0003A458
		public override void WriteProcessingInstruction(string name, string text)
		{
			this.xmlWriter.WriteProcessingInstruction(name, text);
		}

		// Token: 0x060010A4 RID: 4260 RVA: 0x0003C267 File Offset: 0x0003A467
		public override void WriteEntityRef(string name)
		{
			this.xmlWriter.WriteEntityRef(name);
		}

		// Token: 0x060010A5 RID: 4261 RVA: 0x0003C275 File Offset: 0x0003A475
		public override void WriteCharEntity(char ch)
		{
			this.xmlWriter.WriteCharEntity(ch);
		}

		// Token: 0x060010A6 RID: 4262 RVA: 0x0003C283 File Offset: 0x0003A483
		public override void WriteWhitespace(string ws)
		{
			this.xmlWriter.WriteWhitespace(ws);
		}

		// Token: 0x060010A7 RID: 4263 RVA: 0x0003C291 File Offset: 0x0003A491
		public override void WriteString(string text)
		{
			this.xmlWriter.WriteString(text);
		}

		// Token: 0x060010A8 RID: 4264 RVA: 0x0003C29F File Offset: 0x0003A49F
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			this.xmlWriter.WriteSurrogateCharEntity(lowChar, highChar);
		}

		// Token: 0x060010A9 RID: 4265 RVA: 0x0003C2AE File Offset: 0x0003A4AE
		public override void WriteChars(char[] buffer, int index, int count)
		{
			this.xmlWriter.WriteChars(buffer, index, count);
		}

		// Token: 0x060010AA RID: 4266 RVA: 0x0003C2BE File Offset: 0x0003A4BE
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			this.xmlWriter.WriteRaw(buffer, index, count);
		}

		// Token: 0x060010AB RID: 4267 RVA: 0x0003C2CE File Offset: 0x0003A4CE
		public override void WriteRaw(string data)
		{
			this.xmlWriter.WriteRaw(data);
		}

		// Token: 0x060010AC RID: 4268 RVA: 0x0003C2DC File Offset: 0x0003A4DC
		public override void WriteBase64(byte[] buffer, int index, int count)
		{
			this.xmlWriter.WriteBase64(buffer, index, count);
		}

		// Token: 0x060010AD RID: 4269 RVA: 0x0003C2EC File Offset: 0x0003A4EC
		public override void WriteBinHex(byte[] buffer, int index, int count)
		{
			this.xmlWriter.WriteBinHex(buffer, index, count);
		}

		// Token: 0x170003B1 RID: 945
		// (get) Token: 0x060010AE RID: 4270 RVA: 0x0003C2FC File Offset: 0x0003A4FC
		public override WriteState WriteState
		{
			get
			{
				return this.xmlWriter.WriteState;
			}
		}

		// Token: 0x060010AF RID: 4271 RVA: 0x0003C309 File Offset: 0x0003A509
		public override void Flush()
		{
			this.xmlWriter.Flush();
		}

		// Token: 0x060010B0 RID: 4272 RVA: 0x0003C316 File Offset: 0x0003A516
		public override void WriteName(string name)
		{
			this.xmlWriter.WriteName(name);
		}

		// Token: 0x060010B1 RID: 4273 RVA: 0x0003C324 File Offset: 0x0003A524
		public override void WriteQualifiedName(string localName, string ns)
		{
			this.xmlWriter.WriteQualifiedName(localName, ns);
		}

		// Token: 0x060010B2 RID: 4274 RVA: 0x0003C333 File Offset: 0x0003A533
		public override string LookupPrefix(string ns)
		{
			return this.xmlWriter.LookupPrefix(ns);
		}

		// Token: 0x170003B2 RID: 946
		// (get) Token: 0x060010B3 RID: 4275 RVA: 0x0003C341 File Offset: 0x0003A541
		public override XmlSpace XmlSpace
		{
			get
			{
				return this.xmlWriter.XmlSpace;
			}
		}

		// Token: 0x170003B3 RID: 947
		// (get) Token: 0x060010B4 RID: 4276 RVA: 0x0003C34E File Offset: 0x0003A54E
		public override string XmlLang
		{
			get
			{
				return this.xmlWriter.XmlLang;
			}
		}

		// Token: 0x060010B5 RID: 4277 RVA: 0x0003C35B File Offset: 0x0003A55B
		public override void WriteNmToken(string name)
		{
			this.xmlWriter.WriteNmToken(name);
		}

		// Token: 0x060010B6 RID: 4278 RVA: 0x00019949 File Offset: 0x00017B49
		public XmlSerializableWriter()
		{
		}

		// Token: 0x040006F8 RID: 1784
		private XmlWriter xmlWriter;

		// Token: 0x040006F9 RID: 1785
		private int depth;

		// Token: 0x040006FA RID: 1786
		private object obj;
	}
}
