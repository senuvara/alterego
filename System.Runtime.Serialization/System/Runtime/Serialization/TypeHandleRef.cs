﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000BE RID: 190
	internal class TypeHandleRef
	{
		// Token: 0x06000AC8 RID: 2760 RVA: 0x00002217 File Offset: 0x00000417
		public TypeHandleRef()
		{
		}

		// Token: 0x06000AC9 RID: 2761 RVA: 0x0002C6B3 File Offset: 0x0002A8B3
		public TypeHandleRef(RuntimeTypeHandle value)
		{
			this.value = value;
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x06000ACA RID: 2762 RVA: 0x0002C6C2 File Offset: 0x0002A8C2
		// (set) Token: 0x06000ACB RID: 2763 RVA: 0x0002C6CA File Offset: 0x0002A8CA
		public RuntimeTypeHandle Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		// Token: 0x04000479 RID: 1145
		private RuntimeTypeHandle value;
	}
}
