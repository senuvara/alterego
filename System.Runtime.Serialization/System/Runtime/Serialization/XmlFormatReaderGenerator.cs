﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Diagnostics.Application;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x02000131 RID: 305
	internal sealed class XmlFormatReaderGenerator
	{
		// Token: 0x06000EAB RID: 3755 RVA: 0x00035AE1 File Offset: 0x00033CE1
		[SecurityCritical]
		public XmlFormatReaderGenerator()
		{
			this.helper = new XmlFormatReaderGenerator.CriticalHelper();
		}

		// Token: 0x06000EAC RID: 3756 RVA: 0x00035AF4 File Offset: 0x00033CF4
		[SecurityCritical]
		public XmlFormatClassReaderDelegate GenerateClassReader(ClassDataContract classContract)
		{
			XmlFormatClassReaderDelegate result;
			try
			{
				if (TD.DCGenReaderStartIsEnabled())
				{
					TD.DCGenReaderStart("Class", classContract.UnderlyingType.FullName);
				}
				result = this.helper.GenerateClassReader(classContract);
			}
			finally
			{
				if (TD.DCGenReaderStopIsEnabled())
				{
					TD.DCGenReaderStop();
				}
			}
			return result;
		}

		// Token: 0x06000EAD RID: 3757 RVA: 0x00035B4C File Offset: 0x00033D4C
		[SecurityCritical]
		public XmlFormatCollectionReaderDelegate GenerateCollectionReader(CollectionDataContract collectionContract)
		{
			XmlFormatCollectionReaderDelegate result;
			try
			{
				if (TD.DCGenReaderStartIsEnabled())
				{
					TD.DCGenReaderStart("Collection", collectionContract.UnderlyingType.FullName);
				}
				result = this.helper.GenerateCollectionReader(collectionContract);
			}
			finally
			{
				if (TD.DCGenReaderStopIsEnabled())
				{
					TD.DCGenReaderStop();
				}
			}
			return result;
		}

		// Token: 0x06000EAE RID: 3758 RVA: 0x00035BA4 File Offset: 0x00033DA4
		[SecurityCritical]
		public XmlFormatGetOnlyCollectionReaderDelegate GenerateGetOnlyCollectionReader(CollectionDataContract collectionContract)
		{
			XmlFormatGetOnlyCollectionReaderDelegate result;
			try
			{
				if (TD.DCGenReaderStartIsEnabled())
				{
					TD.DCGenReaderStart("GetOnlyCollection", collectionContract.UnderlyingType.FullName);
				}
				result = this.helper.GenerateGetOnlyCollectionReader(collectionContract);
			}
			finally
			{
				if (TD.DCGenReaderStopIsEnabled())
				{
					TD.DCGenReaderStop();
				}
			}
			return result;
		}

		// Token: 0x06000EAF RID: 3759 RVA: 0x00035BFC File Offset: 0x00033DFC
		[SecuritySafeCritical]
		internal static object UnsafeGetUninitializedObject(int id)
		{
			return FormatterServices.GetUninitializedObject(DataContract.GetDataContractForInitialization(id).TypeForInitialization);
		}

		// Token: 0x040006B4 RID: 1716
		[SecurityCritical]
		private XmlFormatReaderGenerator.CriticalHelper helper;

		// Token: 0x02000132 RID: 306
		private class CriticalHelper
		{
			// Token: 0x06000EB0 RID: 3760 RVA: 0x00035C0E File Offset: 0x00033E0E
			internal XmlFormatClassReaderDelegate GenerateClassReader(ClassDataContract classContract)
			{
				return (XmlReaderDelegator xr, XmlObjectSerializerReadContext ctx, XmlDictionaryString[] memberNames, XmlDictionaryString[] memberNamespaces) => new XmlFormatReaderInterpreter(classContract).ReadFromXml(xr, ctx, memberNames, memberNamespaces);
			}

			// Token: 0x06000EB1 RID: 3761 RVA: 0x00035C27 File Offset: 0x00033E27
			internal XmlFormatCollectionReaderDelegate GenerateCollectionReader(CollectionDataContract collectionContract)
			{
				return (XmlReaderDelegator xr, XmlObjectSerializerReadContext ctx, XmlDictionaryString inm, XmlDictionaryString ins, CollectionDataContract cc) => new XmlFormatReaderInterpreter(collectionContract, false).ReadCollectionFromXml(xr, ctx, inm, ins, cc);
			}

			// Token: 0x06000EB2 RID: 3762 RVA: 0x00035C40 File Offset: 0x00033E40
			internal XmlFormatGetOnlyCollectionReaderDelegate GenerateGetOnlyCollectionReader(CollectionDataContract collectionContract)
			{
				return delegate(XmlReaderDelegator xr, XmlObjectSerializerReadContext ctx, XmlDictionaryString inm, XmlDictionaryString ins, CollectionDataContract cc)
				{
					new XmlFormatReaderInterpreter(collectionContract, true).ReadGetOnlyCollectionFromXml(xr, ctx, inm, ins, cc);
				};
			}

			// Token: 0x06000EB3 RID: 3763 RVA: 0x00002217 File Offset: 0x00000417
			public CriticalHelper()
			{
			}

			// Token: 0x02000133 RID: 307
			[CompilerGenerated]
			private sealed class <>c__DisplayClass0_0
			{
				// Token: 0x06000EB4 RID: 3764 RVA: 0x00002217 File Offset: 0x00000417
				public <>c__DisplayClass0_0()
				{
				}

				// Token: 0x06000EB5 RID: 3765 RVA: 0x00035C59 File Offset: 0x00033E59
				internal object <GenerateClassReader>b__0(XmlReaderDelegator xr, XmlObjectSerializerReadContext ctx, XmlDictionaryString[] memberNames, XmlDictionaryString[] memberNamespaces)
				{
					return new XmlFormatReaderInterpreter(this.classContract).ReadFromXml(xr, ctx, memberNames, memberNamespaces);
				}

				// Token: 0x040006B5 RID: 1717
				public ClassDataContract classContract;
			}

			// Token: 0x02000134 RID: 308
			[CompilerGenerated]
			private sealed class <>c__DisplayClass1_0
			{
				// Token: 0x06000EB6 RID: 3766 RVA: 0x00002217 File Offset: 0x00000417
				public <>c__DisplayClass1_0()
				{
				}

				// Token: 0x06000EB7 RID: 3767 RVA: 0x00035C70 File Offset: 0x00033E70
				internal object <GenerateCollectionReader>b__0(XmlReaderDelegator xr, XmlObjectSerializerReadContext ctx, XmlDictionaryString inm, XmlDictionaryString ins, CollectionDataContract cc)
				{
					return new XmlFormatReaderInterpreter(this.collectionContract, false).ReadCollectionFromXml(xr, ctx, inm, ins, cc);
				}

				// Token: 0x040006B6 RID: 1718
				public CollectionDataContract collectionContract;
			}

			// Token: 0x02000135 RID: 309
			[CompilerGenerated]
			private sealed class <>c__DisplayClass2_0
			{
				// Token: 0x06000EB8 RID: 3768 RVA: 0x00002217 File Offset: 0x00000417
				public <>c__DisplayClass2_0()
				{
				}

				// Token: 0x06000EB9 RID: 3769 RVA: 0x00035C8A File Offset: 0x00033E8A
				internal void <GenerateGetOnlyCollectionReader>b__0(XmlReaderDelegator xr, XmlObjectSerializerReadContext ctx, XmlDictionaryString inm, XmlDictionaryString ins, CollectionDataContract cc)
				{
					new XmlFormatReaderInterpreter(this.collectionContract, true).ReadGetOnlyCollectionFromXml(xr, ctx, inm, ins, cc);
				}

				// Token: 0x040006B7 RID: 1719
				public CollectionDataContract collectionContract;
			}
		}
	}
}
