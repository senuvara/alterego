﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000D9 RID: 217
	internal class ExtensionDataReader : XmlReader
	{
		// Token: 0x06000C1F RID: 3103 RVA: 0x0002F454 File Offset: 0x0002D654
		[SecuritySafeCritical]
		static ExtensionDataReader()
		{
			ExtensionDataReader.AddPrefix("i", "http://www.w3.org/2001/XMLSchema-instance");
			ExtensionDataReader.AddPrefix("z", "http://schemas.microsoft.com/2003/10/Serialization/");
			ExtensionDataReader.AddPrefix(string.Empty, string.Empty);
		}

		// Token: 0x06000C20 RID: 3104 RVA: 0x0002F4A2 File Offset: 0x0002D6A2
		internal ExtensionDataReader(XmlObjectSerializerReadContext context)
		{
			this.attributeIndex = -1;
			this.context = context;
		}

		// Token: 0x06000C21 RID: 3105 RVA: 0x0002F4C4 File Offset: 0x0002D6C4
		internal void SetDeserializedValue(object obj)
		{
			IDataNode dataNode = (this.deserializedDataNodes == null || this.deserializedDataNodes.Count == 0) ? null : this.deserializedDataNodes.Dequeue();
			if (dataNode != null && !(obj is IDataNode))
			{
				dataNode.Value = obj;
				dataNode.IsFinalValue = true;
			}
		}

		// Token: 0x06000C22 RID: 3106 RVA: 0x0002F50E File Offset: 0x0002D70E
		internal IDataNode GetCurrentNode()
		{
			IDataNode dataNode = this.element.dataNode;
			this.Skip();
			return dataNode;
		}

		// Token: 0x06000C23 RID: 3107 RVA: 0x0002F521 File Offset: 0x0002D721
		internal void SetDataNode(IDataNode dataNode, string name, string ns)
		{
			this.SetNextElement(dataNode, name, ns, null);
			this.element = this.nextElement;
			this.nextElement = null;
			this.SetElement();
		}

		// Token: 0x06000C24 RID: 3108 RVA: 0x0002F548 File Offset: 0x0002D748
		internal void Reset()
		{
			this.localName = null;
			this.ns = null;
			this.prefix = null;
			this.value = null;
			this.attributeCount = 0;
			this.attributeIndex = -1;
			this.depth = 0;
			this.element = null;
			this.nextElement = null;
			this.elements = null;
			this.deserializedDataNodes = null;
		}

		// Token: 0x17000227 RID: 551
		// (get) Token: 0x06000C25 RID: 3109 RVA: 0x0002F5A2 File Offset: 0x0002D7A2
		private bool IsXmlDataNode
		{
			get
			{
				return this.internalNodeType == ExtensionDataReader.ExtensionDataNodeType.Xml;
			}
		}

		// Token: 0x17000228 RID: 552
		// (get) Token: 0x06000C26 RID: 3110 RVA: 0x0002F5AD File Offset: 0x0002D7AD
		public override XmlNodeType NodeType
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.nodeType;
				}
				return this.xmlNodeReader.NodeType;
			}
		}

		// Token: 0x17000229 RID: 553
		// (get) Token: 0x06000C27 RID: 3111 RVA: 0x0002F5C9 File Offset: 0x0002D7C9
		public override string LocalName
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.localName;
				}
				return this.xmlNodeReader.LocalName;
			}
		}

		// Token: 0x1700022A RID: 554
		// (get) Token: 0x06000C28 RID: 3112 RVA: 0x0002F5E5 File Offset: 0x0002D7E5
		public override string NamespaceURI
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.ns;
				}
				return this.xmlNodeReader.NamespaceURI;
			}
		}

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x06000C29 RID: 3113 RVA: 0x0002F601 File Offset: 0x0002D801
		public override string Prefix
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.prefix;
				}
				return this.xmlNodeReader.Prefix;
			}
		}

		// Token: 0x1700022C RID: 556
		// (get) Token: 0x06000C2A RID: 3114 RVA: 0x0002F61D File Offset: 0x0002D81D
		public override string Value
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.value;
				}
				return this.xmlNodeReader.Value;
			}
		}

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x06000C2B RID: 3115 RVA: 0x0002F639 File Offset: 0x0002D839
		public override int Depth
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.depth;
				}
				return this.xmlNodeReader.Depth;
			}
		}

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x06000C2C RID: 3116 RVA: 0x0002F655 File Offset: 0x0002D855
		public override int AttributeCount
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.attributeCount;
				}
				return this.xmlNodeReader.AttributeCount;
			}
		}

		// Token: 0x1700022F RID: 559
		// (get) Token: 0x06000C2D RID: 3117 RVA: 0x0002F671 File Offset: 0x0002D871
		public override bool EOF
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.readState == ReadState.EndOfFile;
				}
				return this.xmlNodeReader.EOF;
			}
		}

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x06000C2E RID: 3118 RVA: 0x0002F690 File Offset: 0x0002D890
		public override ReadState ReadState
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.readState;
				}
				return this.xmlNodeReader.ReadState;
			}
		}

		// Token: 0x17000231 RID: 561
		// (get) Token: 0x06000C2F RID: 3119 RVA: 0x0002F6AC File Offset: 0x0002D8AC
		public override bool IsEmptyElement
		{
			get
			{
				return this.IsXmlDataNode && this.xmlNodeReader.IsEmptyElement;
			}
		}

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x06000C30 RID: 3120 RVA: 0x0002F6C3 File Offset: 0x0002D8C3
		public override bool IsDefault
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return base.IsDefault;
				}
				return this.xmlNodeReader.IsDefault;
			}
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x06000C31 RID: 3121 RVA: 0x0002F6DF File Offset: 0x0002D8DF
		public override char QuoteChar
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return base.QuoteChar;
				}
				return this.xmlNodeReader.QuoteChar;
			}
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x06000C32 RID: 3122 RVA: 0x0002F6FB File Offset: 0x0002D8FB
		public override XmlSpace XmlSpace
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return base.XmlSpace;
				}
				return this.xmlNodeReader.XmlSpace;
			}
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06000C33 RID: 3123 RVA: 0x0002F717 File Offset: 0x0002D917
		public override string XmlLang
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return base.XmlLang;
				}
				return this.xmlNodeReader.XmlLang;
			}
		}

		// Token: 0x17000236 RID: 566
		public override string this[int i]
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.GetAttribute(i);
				}
				return this.xmlNodeReader[i];
			}
		}

		// Token: 0x17000237 RID: 567
		public override string this[string name]
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.GetAttribute(name);
				}
				return this.xmlNodeReader[name];
			}
		}

		// Token: 0x17000238 RID: 568
		public override string this[string name, string namespaceURI]
		{
			get
			{
				if (!this.IsXmlDataNode)
				{
					return this.GetAttribute(name, namespaceURI);
				}
				return this.xmlNodeReader[name, namespaceURI];
			}
		}

		// Token: 0x06000C37 RID: 3127 RVA: 0x0002F78F File Offset: 0x0002D98F
		public override bool MoveToFirstAttribute()
		{
			if (this.IsXmlDataNode)
			{
				return this.xmlNodeReader.MoveToFirstAttribute();
			}
			if (this.attributeCount == 0)
			{
				return false;
			}
			this.MoveToAttribute(0);
			return true;
		}

		// Token: 0x06000C38 RID: 3128 RVA: 0x0002F7B7 File Offset: 0x0002D9B7
		public override bool MoveToNextAttribute()
		{
			if (this.IsXmlDataNode)
			{
				return this.xmlNodeReader.MoveToNextAttribute();
			}
			if (this.attributeIndex + 1 >= this.attributeCount)
			{
				return false;
			}
			this.MoveToAttribute(this.attributeIndex + 1);
			return true;
		}

		// Token: 0x06000C39 RID: 3129 RVA: 0x0002F7F0 File Offset: 0x0002D9F0
		public override void MoveToAttribute(int index)
		{
			if (this.IsXmlDataNode)
			{
				this.xmlNodeReader.MoveToAttribute(index);
				return;
			}
			if (index < 0 || index >= this.attributeCount)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid XML while deserializing extension data.")));
			}
			this.nodeType = XmlNodeType.Attribute;
			AttributeData attributeData = this.element.attributes[index];
			this.localName = attributeData.localName;
			this.ns = attributeData.ns;
			this.prefix = attributeData.prefix;
			this.value = attributeData.value;
			this.attributeIndex = index;
		}

		// Token: 0x06000C3A RID: 3130 RVA: 0x0002F880 File Offset: 0x0002DA80
		public override string GetAttribute(string name, string namespaceURI)
		{
			if (this.IsXmlDataNode)
			{
				return this.xmlNodeReader.GetAttribute(name, namespaceURI);
			}
			for (int i = 0; i < this.element.attributeCount; i++)
			{
				AttributeData attributeData = this.element.attributes[i];
				if (attributeData.localName == name && attributeData.ns == namespaceURI)
				{
					return attributeData.value;
				}
			}
			return null;
		}

		// Token: 0x06000C3B RID: 3131 RVA: 0x0002F8EC File Offset: 0x0002DAEC
		public override bool MoveToAttribute(string name, string namespaceURI)
		{
			if (this.IsXmlDataNode)
			{
				return this.xmlNodeReader.MoveToAttribute(name, this.ns);
			}
			for (int i = 0; i < this.element.attributeCount; i++)
			{
				AttributeData attributeData = this.element.attributes[i];
				if (attributeData.localName == name && attributeData.ns == namespaceURI)
				{
					this.MoveToAttribute(i);
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000C3C RID: 3132 RVA: 0x0002F95E File Offset: 0x0002DB5E
		public override bool MoveToElement()
		{
			if (this.IsXmlDataNode)
			{
				return this.xmlNodeReader.MoveToElement();
			}
			if (this.nodeType != XmlNodeType.Attribute)
			{
				return false;
			}
			this.SetElement();
			return true;
		}

		// Token: 0x06000C3D RID: 3133 RVA: 0x0002F988 File Offset: 0x0002DB88
		private void SetElement()
		{
			this.nodeType = XmlNodeType.Element;
			this.localName = this.element.localName;
			this.ns = this.element.ns;
			this.prefix = this.element.prefix;
			this.value = string.Empty;
			this.attributeCount = this.element.attributeCount;
			this.attributeIndex = -1;
		}

		// Token: 0x06000C3E RID: 3134 RVA: 0x0002F9F4 File Offset: 0x0002DBF4
		[SecuritySafeCritical]
		public override string LookupNamespace(string prefix)
		{
			if (this.IsXmlDataNode)
			{
				return this.xmlNodeReader.LookupNamespace(prefix);
			}
			string result;
			if (!ExtensionDataReader.prefixToNsTable.TryGetValue(prefix, out result))
			{
				return null;
			}
			return result;
		}

		// Token: 0x06000C3F RID: 3135 RVA: 0x0002FA28 File Offset: 0x0002DC28
		public override void Skip()
		{
			if (this.IsXmlDataNode)
			{
				this.xmlNodeReader.Skip();
				return;
			}
			if (this.ReadState != ReadState.Interactive)
			{
				return;
			}
			this.MoveToElement();
			if (this.IsElementNode(this.internalNodeType))
			{
				int num = 1;
				while (num != 0)
				{
					if (!this.Read())
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid XML while deserializing extension data.")));
					}
					if (this.IsElementNode(this.internalNodeType))
					{
						num++;
					}
					else if (this.internalNodeType == ExtensionDataReader.ExtensionDataNodeType.EndElement)
					{
						this.ReadEndElement();
						num--;
					}
				}
				return;
			}
			this.Read();
		}

		// Token: 0x06000C40 RID: 3136 RVA: 0x0002FABB File Offset: 0x0002DCBB
		private bool IsElementNode(ExtensionDataReader.ExtensionDataNodeType nodeType)
		{
			return nodeType == ExtensionDataReader.ExtensionDataNodeType.Element || nodeType == ExtensionDataReader.ExtensionDataNodeType.ReferencedElement || nodeType == ExtensionDataReader.ExtensionDataNodeType.NullElement;
		}

		// Token: 0x06000C41 RID: 3137 RVA: 0x0002FACB File Offset: 0x0002DCCB
		public override void Close()
		{
			if (this.IsXmlDataNode)
			{
				this.xmlNodeReader.Close();
				return;
			}
			this.Reset();
			this.readState = ReadState.Closed;
		}

		// Token: 0x06000C42 RID: 3138 RVA: 0x0002FAF0 File Offset: 0x0002DCF0
		public override bool Read()
		{
			if (this.nodeType == XmlNodeType.Attribute && this.MoveToNextAttribute())
			{
				return true;
			}
			this.MoveNext(this.element.dataNode);
			switch (this.internalNodeType)
			{
			case ExtensionDataReader.ExtensionDataNodeType.None:
				if (this.depth != 0)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid XML while deserializing extension data.")));
				}
				this.nodeType = XmlNodeType.None;
				this.prefix = string.Empty;
				this.ns = string.Empty;
				this.localName = string.Empty;
				this.value = string.Empty;
				this.attributeCount = 0;
				this.readState = ReadState.EndOfFile;
				return false;
			case ExtensionDataReader.ExtensionDataNodeType.Element:
			case ExtensionDataReader.ExtensionDataNodeType.ReferencedElement:
			case ExtensionDataReader.ExtensionDataNodeType.NullElement:
				this.PushElement();
				this.SetElement();
				break;
			case ExtensionDataReader.ExtensionDataNodeType.EndElement:
				this.nodeType = XmlNodeType.EndElement;
				this.prefix = string.Empty;
				this.ns = string.Empty;
				this.localName = string.Empty;
				this.value = string.Empty;
				this.attributeCount = 0;
				this.attributeIndex = -1;
				this.PopElement();
				break;
			case ExtensionDataReader.ExtensionDataNodeType.Text:
				this.nodeType = XmlNodeType.Text;
				this.prefix = string.Empty;
				this.ns = string.Empty;
				this.localName = string.Empty;
				this.attributeCount = 0;
				this.attributeIndex = -1;
				break;
			case ExtensionDataReader.ExtensionDataNodeType.Xml:
				break;
			default:
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("Invalid state in extension data reader.")));
			}
			this.readState = ReadState.Interactive;
			return true;
		}

		// Token: 0x17000239 RID: 569
		// (get) Token: 0x06000C43 RID: 3139 RVA: 0x0002FC62 File Offset: 0x0002DE62
		public override string Name
		{
			get
			{
				if (this.IsXmlDataNode)
				{
					return this.xmlNodeReader.Name;
				}
				return string.Empty;
			}
		}

		// Token: 0x1700023A RID: 570
		// (get) Token: 0x06000C44 RID: 3140 RVA: 0x0002FC7D File Offset: 0x0002DE7D
		public override bool HasValue
		{
			get
			{
				return this.IsXmlDataNode && this.xmlNodeReader.HasValue;
			}
		}

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x06000C45 RID: 3141 RVA: 0x0002FC94 File Offset: 0x0002DE94
		public override string BaseURI
		{
			get
			{
				if (this.IsXmlDataNode)
				{
					return this.xmlNodeReader.BaseURI;
				}
				return string.Empty;
			}
		}

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x06000C46 RID: 3142 RVA: 0x0002FCAF File Offset: 0x0002DEAF
		public override XmlNameTable NameTable
		{
			get
			{
				if (this.IsXmlDataNode)
				{
					return this.xmlNodeReader.NameTable;
				}
				return null;
			}
		}

		// Token: 0x06000C47 RID: 3143 RVA: 0x0002FCC6 File Offset: 0x0002DEC6
		public override string GetAttribute(string name)
		{
			if (this.IsXmlDataNode)
			{
				return this.xmlNodeReader.GetAttribute(name);
			}
			return null;
		}

		// Token: 0x06000C48 RID: 3144 RVA: 0x0002FCDE File Offset: 0x0002DEDE
		public override string GetAttribute(int i)
		{
			if (this.IsXmlDataNode)
			{
				return this.xmlNodeReader.GetAttribute(i);
			}
			return null;
		}

		// Token: 0x06000C49 RID: 3145 RVA: 0x0002FCF6 File Offset: 0x0002DEF6
		public override bool MoveToAttribute(string name)
		{
			return this.IsXmlDataNode && this.xmlNodeReader.MoveToAttribute(name);
		}

		// Token: 0x06000C4A RID: 3146 RVA: 0x0002FD0E File Offset: 0x0002DF0E
		public override void ResolveEntity()
		{
			if (this.IsXmlDataNode)
			{
				this.xmlNodeReader.ResolveEntity();
			}
		}

		// Token: 0x06000C4B RID: 3147 RVA: 0x0002FD23 File Offset: 0x0002DF23
		public override bool ReadAttributeValue()
		{
			return this.IsXmlDataNode && this.xmlNodeReader.ReadAttributeValue();
		}

		// Token: 0x06000C4C RID: 3148 RVA: 0x0002FD3C File Offset: 0x0002DF3C
		private void MoveNext(IDataNode dataNode)
		{
			ExtensionDataReader.ExtensionDataNodeType extensionDataNodeType = this.internalNodeType;
			if (extensionDataNodeType == ExtensionDataReader.ExtensionDataNodeType.Text || extensionDataNodeType - ExtensionDataReader.ExtensionDataNodeType.ReferencedElement <= 1)
			{
				this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.EndElement;
				return;
			}
			Type dataType = dataNode.DataType;
			if (dataType == Globals.TypeOfClassDataNode)
			{
				this.MoveNextInClass((ClassDataNode)dataNode);
				return;
			}
			if (dataType == Globals.TypeOfCollectionDataNode)
			{
				this.MoveNextInCollection((CollectionDataNode)dataNode);
				return;
			}
			if (dataType == Globals.TypeOfISerializableDataNode)
			{
				this.MoveNextInISerializable((ISerializableDataNode)dataNode);
				return;
			}
			if (dataType == Globals.TypeOfXmlDataNode)
			{
				this.MoveNextInXml((XmlDataNode)dataNode);
				return;
			}
			if (dataNode.Value != null)
			{
				this.MoveToDeserializedObject(dataNode);
				return;
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("Invalid state in extension data reader.")));
		}

		// Token: 0x06000C4D RID: 3149 RVA: 0x0002FDF8 File Offset: 0x0002DFF8
		private void SetNextElement(IDataNode node, string name, string ns, string prefix)
		{
			this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.Element;
			this.nextElement = this.GetNextElement();
			this.nextElement.localName = name;
			this.nextElement.ns = ns;
			this.nextElement.prefix = prefix;
			if (node == null)
			{
				this.nextElement.attributeCount = 0;
				this.nextElement.AddAttribute("i", "http://www.w3.org/2001/XMLSchema-instance", "nil", "true");
				this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.NullElement;
				return;
			}
			if (!this.CheckIfNodeHandled(node))
			{
				this.AddDeserializedDataNode(node);
				node.GetData(this.nextElement);
				if (node is XmlDataNode)
				{
					this.MoveNextInXml((XmlDataNode)node);
				}
			}
		}

		// Token: 0x06000C4E RID: 3150 RVA: 0x0002FEA4 File Offset: 0x0002E0A4
		private void AddDeserializedDataNode(IDataNode node)
		{
			if (node.Id != Globals.NewObjectId && (node.Value == null || !node.IsFinalValue))
			{
				if (this.deserializedDataNodes == null)
				{
					this.deserializedDataNodes = new Queue<IDataNode>();
				}
				this.deserializedDataNodes.Enqueue(node);
			}
		}

		// Token: 0x06000C4F RID: 3151 RVA: 0x0002FEF4 File Offset: 0x0002E0F4
		private bool CheckIfNodeHandled(IDataNode node)
		{
			bool flag = false;
			if (node.Id != Globals.NewObjectId)
			{
				flag = (this.cache[node] != null);
				if (flag)
				{
					if (this.nextElement == null)
					{
						this.nextElement = this.GetNextElement();
					}
					this.nextElement.attributeCount = 0;
					this.nextElement.AddAttribute("z", "http://schemas.microsoft.com/2003/10/Serialization/", "Ref", node.Id.ToString(NumberFormatInfo.InvariantInfo));
					this.nextElement.AddAttribute("i", "http://www.w3.org/2001/XMLSchema-instance", "nil", "true");
					this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.ReferencedElement;
				}
				else
				{
					this.cache.Add(node, node);
				}
			}
			return flag;
		}

		// Token: 0x06000C50 RID: 3152 RVA: 0x0002FFAC File Offset: 0x0002E1AC
		private void MoveNextInClass(ClassDataNode dataNode)
		{
			if (dataNode.Members != null && this.element.childElementIndex < dataNode.Members.Count)
			{
				if (this.element.childElementIndex == 0)
				{
					this.context.IncrementItemCount(-dataNode.Members.Count);
				}
				IList<ExtensionDataMember> members = dataNode.Members;
				ElementData elementData = this.element;
				int childElementIndex = elementData.childElementIndex;
				elementData.childElementIndex = childElementIndex + 1;
				ExtensionDataMember extensionDataMember = members[childElementIndex];
				this.SetNextElement(extensionDataMember.Value, extensionDataMember.Name, extensionDataMember.Namespace, ExtensionDataReader.GetPrefix(extensionDataMember.Namespace));
				return;
			}
			this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.EndElement;
			this.element.childElementIndex = 0;
		}

		// Token: 0x06000C51 RID: 3153 RVA: 0x0003005C File Offset: 0x0002E25C
		private void MoveNextInCollection(CollectionDataNode dataNode)
		{
			if (dataNode.Items != null && this.element.childElementIndex < dataNode.Items.Count)
			{
				if (this.element.childElementIndex == 0)
				{
					this.context.IncrementItemCount(-dataNode.Items.Count);
				}
				IList<IDataNode> items = dataNode.Items;
				ElementData elementData = this.element;
				int childElementIndex = elementData.childElementIndex;
				elementData.childElementIndex = childElementIndex + 1;
				IDataNode node = items[childElementIndex];
				this.SetNextElement(node, dataNode.ItemName, dataNode.ItemNamespace, ExtensionDataReader.GetPrefix(dataNode.ItemNamespace));
				return;
			}
			this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.EndElement;
			this.element.childElementIndex = 0;
		}

		// Token: 0x06000C52 RID: 3154 RVA: 0x00030104 File Offset: 0x0002E304
		private void MoveNextInISerializable(ISerializableDataNode dataNode)
		{
			if (dataNode.Members != null && this.element.childElementIndex < dataNode.Members.Count)
			{
				if (this.element.childElementIndex == 0)
				{
					this.context.IncrementItemCount(-dataNode.Members.Count);
				}
				IList<ISerializableDataMember> members = dataNode.Members;
				ElementData elementData = this.element;
				int childElementIndex = elementData.childElementIndex;
				elementData.childElementIndex = childElementIndex + 1;
				ISerializableDataMember serializableDataMember = members[childElementIndex];
				this.SetNextElement(serializableDataMember.Value, serializableDataMember.Name, string.Empty, string.Empty);
				return;
			}
			this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.EndElement;
			this.element.childElementIndex = 0;
		}

		// Token: 0x06000C53 RID: 3155 RVA: 0x000301A8 File Offset: 0x0002E3A8
		private void MoveNextInXml(XmlDataNode dataNode)
		{
			if (this.IsXmlDataNode)
			{
				this.xmlNodeReader.Read();
				if (this.xmlNodeReader.Depth == 0)
				{
					this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.EndElement;
					this.xmlNodeReader = null;
					return;
				}
			}
			else
			{
				this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.Xml;
				if (this.element == null)
				{
					this.element = this.nextElement;
				}
				else
				{
					this.PushElement();
				}
				XmlNode xmlNode = XmlObjectSerializerReadContext.CreateWrapperXmlElement(dataNode.OwnerDocument, dataNode.XmlAttributes, dataNode.XmlChildNodes, this.element.prefix, this.element.localName, this.element.ns);
				for (int i = 0; i < this.element.attributeCount; i++)
				{
					AttributeData attributeData = this.element.attributes[i];
					XmlAttribute xmlAttribute = dataNode.OwnerDocument.CreateAttribute(attributeData.prefix, attributeData.localName, attributeData.ns);
					xmlAttribute.Value = attributeData.value;
					xmlNode.Attributes.Append(xmlAttribute);
				}
				this.xmlNodeReader = new XmlNodeReader(xmlNode);
				this.xmlNodeReader.Read();
			}
		}

		// Token: 0x06000C54 RID: 3156 RVA: 0x000302B8 File Offset: 0x0002E4B8
		private void MoveToDeserializedObject(IDataNode dataNode)
		{
			Type type = dataNode.DataType;
			bool isTypedNode = true;
			if (type == Globals.TypeOfObject)
			{
				type = dataNode.Value.GetType();
				if (type == Globals.TypeOfObject)
				{
					this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.EndElement;
					return;
				}
				isTypedNode = false;
			}
			if (this.MoveToText(type, dataNode, isTypedNode))
			{
				return;
			}
			if (dataNode.IsFinalValue)
			{
				this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.EndElement;
				return;
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid data node for '{0}' type.", new object[]
			{
				DataContract.GetClrTypeFullName(type)
			})));
		}

		// Token: 0x06000C55 RID: 3157 RVA: 0x00030340 File Offset: 0x0002E540
		private bool MoveToText(Type type, IDataNode dataNode, bool isTypedNode)
		{
			bool flag = true;
			switch (Type.GetTypeCode(type))
			{
			case TypeCode.Boolean:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<bool>)dataNode).GetValue() : ((bool)dataNode.Value));
				goto IL_3E7;
			case TypeCode.Char:
				this.value = XmlConvert.ToString((int)(isTypedNode ? ((DataNode<char>)dataNode).GetValue() : ((char)dataNode.Value)));
				goto IL_3E7;
			case TypeCode.SByte:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<sbyte>)dataNode).GetValue() : ((sbyte)dataNode.Value));
				goto IL_3E7;
			case TypeCode.Byte:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<byte>)dataNode).GetValue() : ((byte)dataNode.Value));
				goto IL_3E7;
			case TypeCode.Int16:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<short>)dataNode).GetValue() : ((short)dataNode.Value));
				goto IL_3E7;
			case TypeCode.UInt16:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<ushort>)dataNode).GetValue() : ((ushort)dataNode.Value));
				goto IL_3E7;
			case TypeCode.Int32:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<int>)dataNode).GetValue() : ((int)dataNode.Value));
				goto IL_3E7;
			case TypeCode.UInt32:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<uint>)dataNode).GetValue() : ((uint)dataNode.Value));
				goto IL_3E7;
			case TypeCode.Int64:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<long>)dataNode).GetValue() : ((long)dataNode.Value));
				goto IL_3E7;
			case TypeCode.UInt64:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<ulong>)dataNode).GetValue() : ((ulong)dataNode.Value));
				goto IL_3E7;
			case TypeCode.Single:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<float>)dataNode).GetValue() : ((float)dataNode.Value));
				goto IL_3E7;
			case TypeCode.Double:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<double>)dataNode).GetValue() : ((double)dataNode.Value));
				goto IL_3E7;
			case TypeCode.Decimal:
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<decimal>)dataNode).GetValue() : ((decimal)dataNode.Value));
				goto IL_3E7;
			case TypeCode.DateTime:
				this.value = (isTypedNode ? ((DataNode<DateTime>)dataNode).GetValue() : ((DateTime)dataNode.Value)).ToString("yyyy-MM-ddTHH:mm:ss.fffffffK", DateTimeFormatInfo.InvariantInfo);
				goto IL_3E7;
			case TypeCode.String:
				this.value = (isTypedNode ? ((DataNode<string>)dataNode).GetValue() : ((string)dataNode.Value));
				goto IL_3E7;
			}
			if (type == Globals.TypeOfByteArray)
			{
				byte[] array = isTypedNode ? ((DataNode<byte[]>)dataNode).GetValue() : ((byte[])dataNode.Value);
				this.value = ((array == null) ? string.Empty : Convert.ToBase64String(array));
			}
			else if (type == Globals.TypeOfTimeSpan)
			{
				this.value = XmlConvert.ToString(isTypedNode ? ((DataNode<TimeSpan>)dataNode).GetValue() : ((TimeSpan)dataNode.Value));
			}
			else if (type == Globals.TypeOfGuid)
			{
				this.value = (isTypedNode ? ((DataNode<Guid>)dataNode).GetValue() : ((Guid)dataNode.Value)).ToString();
			}
			else if (type == Globals.TypeOfUri)
			{
				Uri uri = isTypedNode ? ((DataNode<Uri>)dataNode).GetValue() : ((Uri)dataNode.Value);
				this.value = uri.GetComponents(UriComponents.SerializationInfoString, UriFormat.UriEscaped);
			}
			else
			{
				flag = false;
			}
			IL_3E7:
			if (flag)
			{
				this.internalNodeType = ExtensionDataReader.ExtensionDataNodeType.Text;
			}
			return flag;
		}

		// Token: 0x06000C56 RID: 3158 RVA: 0x00030740 File Offset: 0x0002E940
		private void PushElement()
		{
			this.GrowElementsIfNeeded();
			ElementData[] array = this.elements;
			int num = this.depth;
			this.depth = num + 1;
			array[num] = this.element;
			if (this.nextElement == null)
			{
				this.element = this.GetNextElement();
				return;
			}
			this.element = this.nextElement;
			this.nextElement = null;
		}

		// Token: 0x06000C57 RID: 3159 RVA: 0x0003079C File Offset: 0x0002E99C
		private void PopElement()
		{
			this.prefix = this.element.prefix;
			this.localName = this.element.localName;
			this.ns = this.element.ns;
			if (this.depth == 0)
			{
				return;
			}
			this.depth--;
			if (this.elements != null)
			{
				this.element = this.elements[this.depth];
			}
		}

		// Token: 0x06000C58 RID: 3160 RVA: 0x00030810 File Offset: 0x0002EA10
		private void GrowElementsIfNeeded()
		{
			if (this.elements == null)
			{
				this.elements = new ElementData[8];
				return;
			}
			if (this.elements.Length == this.depth)
			{
				ElementData[] destinationArray = new ElementData[this.elements.Length * 2];
				Array.Copy(this.elements, 0, destinationArray, 0, this.elements.Length);
				this.elements = destinationArray;
			}
		}

		// Token: 0x06000C59 RID: 3161 RVA: 0x00030870 File Offset: 0x0002EA70
		private ElementData GetNextElement()
		{
			int num = this.depth + 1;
			if (this.elements != null && this.elements.Length > num && this.elements[num] != null)
			{
				return this.elements[num];
			}
			return new ElementData();
		}

		// Token: 0x06000C5A RID: 3162 RVA: 0x000308B4 File Offset: 0x0002EAB4
		[SecuritySafeCritical]
		internal static string GetPrefix(string ns)
		{
			ns = (ns ?? string.Empty);
			string result;
			if (!ExtensionDataReader.nsToPrefixTable.TryGetValue(ns, out result))
			{
				Dictionary<string, string> obj = ExtensionDataReader.nsToPrefixTable;
				lock (obj)
				{
					if (!ExtensionDataReader.nsToPrefixTable.TryGetValue(ns, out result))
					{
						result = ((ns == null || ns.Length == 0) ? string.Empty : ("p" + ExtensionDataReader.nsToPrefixTable.Count));
						ExtensionDataReader.AddPrefix(result, ns);
					}
				}
			}
			return result;
		}

		// Token: 0x06000C5B RID: 3163 RVA: 0x0003094C File Offset: 0x0002EB4C
		[SecuritySafeCritical]
		private static void AddPrefix(string prefix, string ns)
		{
			ExtensionDataReader.nsToPrefixTable.Add(ns, prefix);
			ExtensionDataReader.prefixToNsTable.Add(prefix, ns);
		}

		// Token: 0x0400051F RID: 1311
		private Hashtable cache = new Hashtable();

		// Token: 0x04000520 RID: 1312
		private ElementData[] elements;

		// Token: 0x04000521 RID: 1313
		private ElementData element;

		// Token: 0x04000522 RID: 1314
		private ElementData nextElement;

		// Token: 0x04000523 RID: 1315
		private ReadState readState;

		// Token: 0x04000524 RID: 1316
		private ExtensionDataReader.ExtensionDataNodeType internalNodeType;

		// Token: 0x04000525 RID: 1317
		private XmlNodeType nodeType;

		// Token: 0x04000526 RID: 1318
		private int depth;

		// Token: 0x04000527 RID: 1319
		private string localName;

		// Token: 0x04000528 RID: 1320
		private string ns;

		// Token: 0x04000529 RID: 1321
		private string prefix;

		// Token: 0x0400052A RID: 1322
		private string value;

		// Token: 0x0400052B RID: 1323
		private int attributeCount;

		// Token: 0x0400052C RID: 1324
		private int attributeIndex;

		// Token: 0x0400052D RID: 1325
		private XmlNodeReader xmlNodeReader;

		// Token: 0x0400052E RID: 1326
		private Queue<IDataNode> deserializedDataNodes;

		// Token: 0x0400052F RID: 1327
		private XmlObjectSerializerReadContext context;

		// Token: 0x04000530 RID: 1328
		[SecurityCritical]
		private static Dictionary<string, string> nsToPrefixTable = new Dictionary<string, string>();

		// Token: 0x04000531 RID: 1329
		[SecurityCritical]
		private static Dictionary<string, string> prefixToNsTable = new Dictionary<string, string>();

		// Token: 0x020000DA RID: 218
		private enum ExtensionDataNodeType
		{
			// Token: 0x04000533 RID: 1331
			None,
			// Token: 0x04000534 RID: 1332
			Element,
			// Token: 0x04000535 RID: 1333
			EndElement,
			// Token: 0x04000536 RID: 1334
			Text,
			// Token: 0x04000537 RID: 1335
			Xml,
			// Token: 0x04000538 RID: 1336
			ReferencedElement,
			// Token: 0x04000539 RID: 1337
			NullElement
		}
	}
}
