﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000FA RID: 250
	internal class NegativeIntegerDataContract : LongDataContract
	{
		// Token: 0x06000D91 RID: 3473 RVA: 0x00033525 File Offset: 0x00031725
		internal NegativeIntegerDataContract() : base(DictionaryGlobals.negativeIntegerLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
