﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x0200012E RID: 302
	// (Invoke) Token: 0x06000EA0 RID: 3744
	internal delegate object XmlFormatClassReaderDelegate(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContext context, XmlDictionaryString[] memberNames, XmlDictionaryString[] memberNamespaces);
}
