﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000E8 RID: 232
	internal sealed class KnownTypeDataContractResolver : DataContractResolver
	{
		// Token: 0x06000CFE RID: 3326 RVA: 0x00031FC5 File Offset: 0x000301C5
		internal KnownTypeDataContractResolver(XmlObjectSerializerContext context)
		{
			this.context = context;
		}

		// Token: 0x06000CFF RID: 3327 RVA: 0x00031FD4 File Offset: 0x000301D4
		public override bool TryResolveType(Type type, Type declaredType, DataContractResolver knownTypeResolver, out XmlDictionaryString typeName, out XmlDictionaryString typeNamespace)
		{
			if (type == null)
			{
				typeName = null;
				typeNamespace = null;
				return false;
			}
			if (declaredType != null && declaredType.IsInterface && CollectionDataContract.IsCollectionInterface(declaredType))
			{
				typeName = null;
				typeNamespace = null;
				return true;
			}
			DataContract dataContract = DataContract.GetDataContract(type);
			if (this.context.IsKnownType(dataContract, dataContract.KnownDataContracts, declaredType))
			{
				typeName = dataContract.Name;
				typeNamespace = dataContract.Namespace;
				return true;
			}
			typeName = null;
			typeNamespace = null;
			return false;
		}

		// Token: 0x06000D00 RID: 3328 RVA: 0x00032050 File Offset: 0x00030250
		public override Type ResolveName(string typeName, string typeNamespace, Type declaredType, DataContractResolver knownTypeResolver)
		{
			if (typeName == null || typeNamespace == null)
			{
				return null;
			}
			return this.context.ResolveNameFromKnownTypes(new XmlQualifiedName(typeName, typeNamespace));
		}

		// Token: 0x04000627 RID: 1575
		private XmlObjectSerializerContext context;
	}
}
