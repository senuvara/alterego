﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Security;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace System.Runtime.Serialization
{
	// Token: 0x020000DF RID: 223
	internal static class Globals
	{
		// Token: 0x17000240 RID: 576
		// (get) Token: 0x06000C66 RID: 3174 RVA: 0x00030A8A File Offset: 0x0002EC8A
		internal static XmlQualifiedName IdQualifiedName
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.idQualifiedName == null)
				{
					Globals.idQualifiedName = new XmlQualifiedName("Id", "http://schemas.microsoft.com/2003/10/Serialization/");
				}
				return Globals.idQualifiedName;
			}
		}

		// Token: 0x17000241 RID: 577
		// (get) Token: 0x06000C67 RID: 3175 RVA: 0x00030AB2 File Offset: 0x0002ECB2
		internal static XmlQualifiedName RefQualifiedName
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.refQualifiedName == null)
				{
					Globals.refQualifiedName = new XmlQualifiedName("Ref", "http://schemas.microsoft.com/2003/10/Serialization/");
				}
				return Globals.refQualifiedName;
			}
		}

		// Token: 0x17000242 RID: 578
		// (get) Token: 0x06000C68 RID: 3176 RVA: 0x00030ADA File Offset: 0x0002ECDA
		internal static Type TypeOfObject
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfObject == null)
				{
					Globals.typeOfObject = typeof(object);
				}
				return Globals.typeOfObject;
			}
		}

		// Token: 0x17000243 RID: 579
		// (get) Token: 0x06000C69 RID: 3177 RVA: 0x00030AFD File Offset: 0x0002ECFD
		internal static Type TypeOfValueType
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfValueType == null)
				{
					Globals.typeOfValueType = typeof(ValueType);
				}
				return Globals.typeOfValueType;
			}
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x06000C6A RID: 3178 RVA: 0x00030B20 File Offset: 0x0002ED20
		internal static Type TypeOfArray
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfArray == null)
				{
					Globals.typeOfArray = typeof(Array);
				}
				return Globals.typeOfArray;
			}
		}

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x06000C6B RID: 3179 RVA: 0x00030B43 File Offset: 0x0002ED43
		internal static Type TypeOfString
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfString == null)
				{
					Globals.typeOfString = typeof(string);
				}
				return Globals.typeOfString;
			}
		}

		// Token: 0x17000246 RID: 582
		// (get) Token: 0x06000C6C RID: 3180 RVA: 0x00030B66 File Offset: 0x0002ED66
		internal static Type TypeOfInt
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfInt == null)
				{
					Globals.typeOfInt = typeof(int);
				}
				return Globals.typeOfInt;
			}
		}

		// Token: 0x17000247 RID: 583
		// (get) Token: 0x06000C6D RID: 3181 RVA: 0x00030B89 File Offset: 0x0002ED89
		internal static Type TypeOfULong
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfULong == null)
				{
					Globals.typeOfULong = typeof(ulong);
				}
				return Globals.typeOfULong;
			}
		}

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x06000C6E RID: 3182 RVA: 0x00030BAC File Offset: 0x0002EDAC
		internal static Type TypeOfVoid
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfVoid == null)
				{
					Globals.typeOfVoid = typeof(void);
				}
				return Globals.typeOfVoid;
			}
		}

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x06000C6F RID: 3183 RVA: 0x00030BCF File Offset: 0x0002EDCF
		internal static Type TypeOfByteArray
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfByteArray == null)
				{
					Globals.typeOfByteArray = typeof(byte[]);
				}
				return Globals.typeOfByteArray;
			}
		}

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x06000C70 RID: 3184 RVA: 0x00030BF2 File Offset: 0x0002EDF2
		internal static Type TypeOfTimeSpan
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfTimeSpan == null)
				{
					Globals.typeOfTimeSpan = typeof(TimeSpan);
				}
				return Globals.typeOfTimeSpan;
			}
		}

		// Token: 0x1700024B RID: 587
		// (get) Token: 0x06000C71 RID: 3185 RVA: 0x00030C15 File Offset: 0x0002EE15
		internal static Type TypeOfGuid
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfGuid == null)
				{
					Globals.typeOfGuid = typeof(Guid);
				}
				return Globals.typeOfGuid;
			}
		}

		// Token: 0x1700024C RID: 588
		// (get) Token: 0x06000C72 RID: 3186 RVA: 0x00030C38 File Offset: 0x0002EE38
		internal static Type TypeOfDateTimeOffset
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfDateTimeOffset == null)
				{
					Globals.typeOfDateTimeOffset = typeof(DateTimeOffset);
				}
				return Globals.typeOfDateTimeOffset;
			}
		}

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x06000C73 RID: 3187 RVA: 0x00030C5B File Offset: 0x0002EE5B
		internal static Type TypeOfDateTimeOffsetAdapter
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfDateTimeOffsetAdapter == null)
				{
					Globals.typeOfDateTimeOffsetAdapter = typeof(DateTimeOffsetAdapter);
				}
				return Globals.typeOfDateTimeOffsetAdapter;
			}
		}

		// Token: 0x1700024E RID: 590
		// (get) Token: 0x06000C74 RID: 3188 RVA: 0x00030C7E File Offset: 0x0002EE7E
		internal static Type TypeOfUri
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfUri == null)
				{
					Globals.typeOfUri = typeof(Uri);
				}
				return Globals.typeOfUri;
			}
		}

		// Token: 0x1700024F RID: 591
		// (get) Token: 0x06000C75 RID: 3189 RVA: 0x00030CA1 File Offset: 0x0002EEA1
		internal static Type TypeOfTypeEnumerable
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfTypeEnumerable == null)
				{
					Globals.typeOfTypeEnumerable = typeof(IEnumerable<Type>);
				}
				return Globals.typeOfTypeEnumerable;
			}
		}

		// Token: 0x17000250 RID: 592
		// (get) Token: 0x06000C76 RID: 3190 RVA: 0x00030CC4 File Offset: 0x0002EEC4
		internal static Type TypeOfStreamingContext
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfStreamingContext == null)
				{
					Globals.typeOfStreamingContext = typeof(StreamingContext);
				}
				return Globals.typeOfStreamingContext;
			}
		}

		// Token: 0x17000251 RID: 593
		// (get) Token: 0x06000C77 RID: 3191 RVA: 0x00030CE7 File Offset: 0x0002EEE7
		internal static Type TypeOfISerializable
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfISerializable == null)
				{
					Globals.typeOfISerializable = typeof(ISerializable);
				}
				return Globals.typeOfISerializable;
			}
		}

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x06000C78 RID: 3192 RVA: 0x00030D0A File Offset: 0x0002EF0A
		internal static Type TypeOfIDeserializationCallback
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIDeserializationCallback == null)
				{
					Globals.typeOfIDeserializationCallback = typeof(IDeserializationCallback);
				}
				return Globals.typeOfIDeserializationCallback;
			}
		}

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x06000C79 RID: 3193 RVA: 0x00030D2D File Offset: 0x0002EF2D
		internal static Type TypeOfIObjectReference
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIObjectReference == null)
				{
					Globals.typeOfIObjectReference = typeof(IObjectReference);
				}
				return Globals.typeOfIObjectReference;
			}
		}

		// Token: 0x17000254 RID: 596
		// (get) Token: 0x06000C7A RID: 3194 RVA: 0x00030D50 File Offset: 0x0002EF50
		internal static Type TypeOfXmlFormatClassWriterDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlFormatClassWriterDelegate == null)
				{
					Globals.typeOfXmlFormatClassWriterDelegate = typeof(XmlFormatClassWriterDelegate);
				}
				return Globals.typeOfXmlFormatClassWriterDelegate;
			}
		}

		// Token: 0x17000255 RID: 597
		// (get) Token: 0x06000C7B RID: 3195 RVA: 0x00030D73 File Offset: 0x0002EF73
		internal static Type TypeOfXmlFormatCollectionWriterDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlFormatCollectionWriterDelegate == null)
				{
					Globals.typeOfXmlFormatCollectionWriterDelegate = typeof(XmlFormatCollectionWriterDelegate);
				}
				return Globals.typeOfXmlFormatCollectionWriterDelegate;
			}
		}

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x06000C7C RID: 3196 RVA: 0x00030D96 File Offset: 0x0002EF96
		internal static Type TypeOfXmlFormatClassReaderDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlFormatClassReaderDelegate == null)
				{
					Globals.typeOfXmlFormatClassReaderDelegate = typeof(XmlFormatClassReaderDelegate);
				}
				return Globals.typeOfXmlFormatClassReaderDelegate;
			}
		}

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x06000C7D RID: 3197 RVA: 0x00030DB9 File Offset: 0x0002EFB9
		internal static Type TypeOfXmlFormatCollectionReaderDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlFormatCollectionReaderDelegate == null)
				{
					Globals.typeOfXmlFormatCollectionReaderDelegate = typeof(XmlFormatCollectionReaderDelegate);
				}
				return Globals.typeOfXmlFormatCollectionReaderDelegate;
			}
		}

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x06000C7E RID: 3198 RVA: 0x00030DDC File Offset: 0x0002EFDC
		internal static Type TypeOfXmlFormatGetOnlyCollectionReaderDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlFormatGetOnlyCollectionReaderDelegate == null)
				{
					Globals.typeOfXmlFormatGetOnlyCollectionReaderDelegate = typeof(XmlFormatGetOnlyCollectionReaderDelegate);
				}
				return Globals.typeOfXmlFormatGetOnlyCollectionReaderDelegate;
			}
		}

		// Token: 0x17000259 RID: 601
		// (get) Token: 0x06000C7F RID: 3199 RVA: 0x00030DFF File Offset: 0x0002EFFF
		internal static Type TypeOfKnownTypeAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfKnownTypeAttribute == null)
				{
					Globals.typeOfKnownTypeAttribute = typeof(KnownTypeAttribute);
				}
				return Globals.typeOfKnownTypeAttribute;
			}
		}

		// Token: 0x1700025A RID: 602
		// (get) Token: 0x06000C80 RID: 3200 RVA: 0x00030E22 File Offset: 0x0002F022
		internal static Type TypeOfDataContractAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfDataContractAttribute == null)
				{
					Globals.typeOfDataContractAttribute = typeof(DataContractAttribute);
				}
				return Globals.typeOfDataContractAttribute;
			}
		}

		// Token: 0x1700025B RID: 603
		// (get) Token: 0x06000C81 RID: 3201 RVA: 0x00030E45 File Offset: 0x0002F045
		internal static Type TypeOfContractNamespaceAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfContractNamespaceAttribute == null)
				{
					Globals.typeOfContractNamespaceAttribute = typeof(ContractNamespaceAttribute);
				}
				return Globals.typeOfContractNamespaceAttribute;
			}
		}

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x06000C82 RID: 3202 RVA: 0x00030E68 File Offset: 0x0002F068
		internal static Type TypeOfDataMemberAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfDataMemberAttribute == null)
				{
					Globals.typeOfDataMemberAttribute = typeof(DataMemberAttribute);
				}
				return Globals.typeOfDataMemberAttribute;
			}
		}

		// Token: 0x1700025D RID: 605
		// (get) Token: 0x06000C83 RID: 3203 RVA: 0x00030E8B File Offset: 0x0002F08B
		internal static Type TypeOfEnumMemberAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfEnumMemberAttribute == null)
				{
					Globals.typeOfEnumMemberAttribute = typeof(EnumMemberAttribute);
				}
				return Globals.typeOfEnumMemberAttribute;
			}
		}

		// Token: 0x1700025E RID: 606
		// (get) Token: 0x06000C84 RID: 3204 RVA: 0x00030EAE File Offset: 0x0002F0AE
		internal static Type TypeOfCollectionDataContractAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfCollectionDataContractAttribute == null)
				{
					Globals.typeOfCollectionDataContractAttribute = typeof(CollectionDataContractAttribute);
				}
				return Globals.typeOfCollectionDataContractAttribute;
			}
		}

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x06000C85 RID: 3205 RVA: 0x00030ED1 File Offset: 0x0002F0D1
		internal static Type TypeOfOptionalFieldAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfOptionalFieldAttribute == null)
				{
					Globals.typeOfOptionalFieldAttribute = typeof(OptionalFieldAttribute);
				}
				return Globals.typeOfOptionalFieldAttribute;
			}
		}

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x06000C86 RID: 3206 RVA: 0x00030EF4 File Offset: 0x0002F0F4
		internal static Type TypeOfObjectArray
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfObjectArray == null)
				{
					Globals.typeOfObjectArray = typeof(object[]);
				}
				return Globals.typeOfObjectArray;
			}
		}

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x06000C87 RID: 3207 RVA: 0x00030F17 File Offset: 0x0002F117
		internal static Type TypeOfOnSerializingAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfOnSerializingAttribute == null)
				{
					Globals.typeOfOnSerializingAttribute = typeof(OnSerializingAttribute);
				}
				return Globals.typeOfOnSerializingAttribute;
			}
		}

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x06000C88 RID: 3208 RVA: 0x00030F3A File Offset: 0x0002F13A
		internal static Type TypeOfOnSerializedAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfOnSerializedAttribute == null)
				{
					Globals.typeOfOnSerializedAttribute = typeof(OnSerializedAttribute);
				}
				return Globals.typeOfOnSerializedAttribute;
			}
		}

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x06000C89 RID: 3209 RVA: 0x00030F5D File Offset: 0x0002F15D
		internal static Type TypeOfOnDeserializingAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfOnDeserializingAttribute == null)
				{
					Globals.typeOfOnDeserializingAttribute = typeof(OnDeserializingAttribute);
				}
				return Globals.typeOfOnDeserializingAttribute;
			}
		}

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x06000C8A RID: 3210 RVA: 0x00030F80 File Offset: 0x0002F180
		internal static Type TypeOfOnDeserializedAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfOnDeserializedAttribute == null)
				{
					Globals.typeOfOnDeserializedAttribute = typeof(OnDeserializedAttribute);
				}
				return Globals.typeOfOnDeserializedAttribute;
			}
		}

		// Token: 0x17000265 RID: 613
		// (get) Token: 0x06000C8B RID: 3211 RVA: 0x00030FA3 File Offset: 0x0002F1A3
		internal static Type TypeOfFlagsAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfFlagsAttribute == null)
				{
					Globals.typeOfFlagsAttribute = typeof(FlagsAttribute);
				}
				return Globals.typeOfFlagsAttribute;
			}
		}

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x06000C8C RID: 3212 RVA: 0x00030FC6 File Offset: 0x0002F1C6
		internal static Type TypeOfSerializableAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfSerializableAttribute == null)
				{
					Globals.typeOfSerializableAttribute = typeof(SerializableAttribute);
				}
				return Globals.typeOfSerializableAttribute;
			}
		}

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x06000C8D RID: 3213 RVA: 0x00030FE9 File Offset: 0x0002F1E9
		internal static Type TypeOfNonSerializedAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfNonSerializedAttribute == null)
				{
					Globals.typeOfNonSerializedAttribute = typeof(NonSerializedAttribute);
				}
				return Globals.typeOfNonSerializedAttribute;
			}
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x06000C8E RID: 3214 RVA: 0x0003100C File Offset: 0x0002F20C
		internal static Type TypeOfSerializationInfo
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfSerializationInfo == null)
				{
					Globals.typeOfSerializationInfo = typeof(SerializationInfo);
				}
				return Globals.typeOfSerializationInfo;
			}
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x06000C8F RID: 3215 RVA: 0x0003102F File Offset: 0x0002F22F
		internal static Type TypeOfSerializationInfoEnumerator
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfSerializationInfoEnumerator == null)
				{
					Globals.typeOfSerializationInfoEnumerator = typeof(SerializationInfoEnumerator);
				}
				return Globals.typeOfSerializationInfoEnumerator;
			}
		}

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x06000C90 RID: 3216 RVA: 0x00031052 File Offset: 0x0002F252
		internal static Type TypeOfSerializationEntry
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfSerializationEntry == null)
				{
					Globals.typeOfSerializationEntry = typeof(SerializationEntry);
				}
				return Globals.typeOfSerializationEntry;
			}
		}

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x06000C91 RID: 3217 RVA: 0x00031075 File Offset: 0x0002F275
		internal static Type TypeOfIXmlSerializable
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIXmlSerializable == null)
				{
					Globals.typeOfIXmlSerializable = typeof(IXmlSerializable);
				}
				return Globals.typeOfIXmlSerializable;
			}
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x06000C92 RID: 3218 RVA: 0x00031098 File Offset: 0x0002F298
		internal static Type TypeOfXmlSchemaProviderAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlSchemaProviderAttribute == null)
				{
					Globals.typeOfXmlSchemaProviderAttribute = typeof(XmlSchemaProviderAttribute);
				}
				return Globals.typeOfXmlSchemaProviderAttribute;
			}
		}

		// Token: 0x1700026D RID: 621
		// (get) Token: 0x06000C93 RID: 3219 RVA: 0x000310BB File Offset: 0x0002F2BB
		internal static Type TypeOfXmlRootAttribute
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlRootAttribute == null)
				{
					Globals.typeOfXmlRootAttribute = typeof(XmlRootAttribute);
				}
				return Globals.typeOfXmlRootAttribute;
			}
		}

		// Token: 0x1700026E RID: 622
		// (get) Token: 0x06000C94 RID: 3220 RVA: 0x000310DE File Offset: 0x0002F2DE
		internal static Type TypeOfXmlQualifiedName
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlQualifiedName == null)
				{
					Globals.typeOfXmlQualifiedName = typeof(XmlQualifiedName);
				}
				return Globals.typeOfXmlQualifiedName;
			}
		}

		// Token: 0x1700026F RID: 623
		// (get) Token: 0x06000C95 RID: 3221 RVA: 0x00031101 File Offset: 0x0002F301
		internal static Type TypeOfXmlSchemaType
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlSchemaType == null)
				{
					Globals.typeOfXmlSchemaType = typeof(XmlSchemaType);
				}
				return Globals.typeOfXmlSchemaType;
			}
		}

		// Token: 0x17000270 RID: 624
		// (get) Token: 0x06000C96 RID: 3222 RVA: 0x00031124 File Offset: 0x0002F324
		internal static Type TypeOfXmlSerializableServices
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlSerializableServices == null)
				{
					Globals.typeOfXmlSerializableServices = typeof(XmlSerializableServices);
				}
				return Globals.typeOfXmlSerializableServices;
			}
		}

		// Token: 0x17000271 RID: 625
		// (get) Token: 0x06000C97 RID: 3223 RVA: 0x00031147 File Offset: 0x0002F347
		internal static Type TypeOfXmlNodeArray
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlNodeArray == null)
				{
					Globals.typeOfXmlNodeArray = typeof(XmlNode[]);
				}
				return Globals.typeOfXmlNodeArray;
			}
		}

		// Token: 0x17000272 RID: 626
		// (get) Token: 0x06000C98 RID: 3224 RVA: 0x0003116A File Offset: 0x0002F36A
		internal static Type TypeOfXmlSchemaSet
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlSchemaSet == null)
				{
					Globals.typeOfXmlSchemaSet = typeof(XmlSchemaSet);
				}
				return Globals.typeOfXmlSchemaSet;
			}
		}

		// Token: 0x17000273 RID: 627
		// (get) Token: 0x06000C99 RID: 3225 RVA: 0x0003118D File Offset: 0x0002F38D
		internal static object[] EmptyObjectArray
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.emptyObjectArray == null)
				{
					Globals.emptyObjectArray = new object[0];
				}
				return Globals.emptyObjectArray;
			}
		}

		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06000C9A RID: 3226 RVA: 0x000311A6 File Offset: 0x0002F3A6
		internal static Type[] EmptyTypeArray
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.emptyTypeArray == null)
				{
					Globals.emptyTypeArray = new Type[0];
				}
				return Globals.emptyTypeArray;
			}
		}

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06000C9B RID: 3227 RVA: 0x000311BF File Offset: 0x0002F3BF
		internal static Type TypeOfIPropertyChange
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIPropertyChange == null)
				{
					Globals.typeOfIPropertyChange = typeof(INotifyPropertyChanged);
				}
				return Globals.typeOfIPropertyChange;
			}
		}

		// Token: 0x17000276 RID: 630
		// (get) Token: 0x06000C9C RID: 3228 RVA: 0x000311E2 File Offset: 0x0002F3E2
		internal static Type TypeOfIExtensibleDataObject
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIExtensibleDataObject == null)
				{
					Globals.typeOfIExtensibleDataObject = typeof(IExtensibleDataObject);
				}
				return Globals.typeOfIExtensibleDataObject;
			}
		}

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x06000C9D RID: 3229 RVA: 0x00031205 File Offset: 0x0002F405
		internal static Type TypeOfExtensionDataObject
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfExtensionDataObject == null)
				{
					Globals.typeOfExtensionDataObject = typeof(ExtensionDataObject);
				}
				return Globals.typeOfExtensionDataObject;
			}
		}

		// Token: 0x17000278 RID: 632
		// (get) Token: 0x06000C9E RID: 3230 RVA: 0x00031228 File Offset: 0x0002F428
		internal static Type TypeOfISerializableDataNode
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfISerializableDataNode == null)
				{
					Globals.typeOfISerializableDataNode = typeof(ISerializableDataNode);
				}
				return Globals.typeOfISerializableDataNode;
			}
		}

		// Token: 0x17000279 RID: 633
		// (get) Token: 0x06000C9F RID: 3231 RVA: 0x0003124B File Offset: 0x0002F44B
		internal static Type TypeOfClassDataNode
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfClassDataNode == null)
				{
					Globals.typeOfClassDataNode = typeof(ClassDataNode);
				}
				return Globals.typeOfClassDataNode;
			}
		}

		// Token: 0x1700027A RID: 634
		// (get) Token: 0x06000CA0 RID: 3232 RVA: 0x0003126E File Offset: 0x0002F46E
		internal static Type TypeOfCollectionDataNode
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfCollectionDataNode == null)
				{
					Globals.typeOfCollectionDataNode = typeof(CollectionDataNode);
				}
				return Globals.typeOfCollectionDataNode;
			}
		}

		// Token: 0x1700027B RID: 635
		// (get) Token: 0x06000CA1 RID: 3233 RVA: 0x00031291 File Offset: 0x0002F491
		internal static Type TypeOfXmlDataNode
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlDataNode == null)
				{
					Globals.typeOfXmlDataNode = typeof(XmlDataNode);
				}
				return Globals.typeOfXmlDataNode;
			}
		}

		// Token: 0x1700027C RID: 636
		// (get) Token: 0x06000CA2 RID: 3234 RVA: 0x000312B4 File Offset: 0x0002F4B4
		internal static Type TypeOfNullable
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfNullable == null)
				{
					Globals.typeOfNullable = typeof(Nullable<>);
				}
				return Globals.typeOfNullable;
			}
		}

		// Token: 0x1700027D RID: 637
		// (get) Token: 0x06000CA3 RID: 3235 RVA: 0x000312D7 File Offset: 0x0002F4D7
		internal static Type TypeOfReflectionPointer
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfReflectionPointer == null)
				{
					Globals.typeOfReflectionPointer = typeof(Pointer);
				}
				return Globals.typeOfReflectionPointer;
			}
		}

		// Token: 0x1700027E RID: 638
		// (get) Token: 0x06000CA4 RID: 3236 RVA: 0x000312FA File Offset: 0x0002F4FA
		internal static Type TypeOfIDictionaryGeneric
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIDictionaryGeneric == null)
				{
					Globals.typeOfIDictionaryGeneric = typeof(IDictionary<, >);
				}
				return Globals.typeOfIDictionaryGeneric;
			}
		}

		// Token: 0x1700027F RID: 639
		// (get) Token: 0x06000CA5 RID: 3237 RVA: 0x0003131D File Offset: 0x0002F51D
		internal static Type TypeOfIDictionary
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIDictionary == null)
				{
					Globals.typeOfIDictionary = typeof(IDictionary);
				}
				return Globals.typeOfIDictionary;
			}
		}

		// Token: 0x17000280 RID: 640
		// (get) Token: 0x06000CA6 RID: 3238 RVA: 0x00031340 File Offset: 0x0002F540
		internal static Type TypeOfIListGeneric
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIListGeneric == null)
				{
					Globals.typeOfIListGeneric = typeof(IList<>);
				}
				return Globals.typeOfIListGeneric;
			}
		}

		// Token: 0x17000281 RID: 641
		// (get) Token: 0x06000CA7 RID: 3239 RVA: 0x00031363 File Offset: 0x0002F563
		internal static Type TypeOfIList
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIList == null)
				{
					Globals.typeOfIList = typeof(IList);
				}
				return Globals.typeOfIList;
			}
		}

		// Token: 0x17000282 RID: 642
		// (get) Token: 0x06000CA8 RID: 3240 RVA: 0x00031386 File Offset: 0x0002F586
		internal static Type TypeOfICollectionGeneric
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfICollectionGeneric == null)
				{
					Globals.typeOfICollectionGeneric = typeof(ICollection<>);
				}
				return Globals.typeOfICollectionGeneric;
			}
		}

		// Token: 0x17000283 RID: 643
		// (get) Token: 0x06000CA9 RID: 3241 RVA: 0x000313A9 File Offset: 0x0002F5A9
		internal static Type TypeOfICollection
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfICollection == null)
				{
					Globals.typeOfICollection = typeof(ICollection);
				}
				return Globals.typeOfICollection;
			}
		}

		// Token: 0x17000284 RID: 644
		// (get) Token: 0x06000CAA RID: 3242 RVA: 0x000313CC File Offset: 0x0002F5CC
		internal static Type TypeOfIEnumerableGeneric
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIEnumerableGeneric == null)
				{
					Globals.typeOfIEnumerableGeneric = typeof(IEnumerable<>);
				}
				return Globals.typeOfIEnumerableGeneric;
			}
		}

		// Token: 0x17000285 RID: 645
		// (get) Token: 0x06000CAB RID: 3243 RVA: 0x000313EF File Offset: 0x0002F5EF
		internal static Type TypeOfIEnumerable
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIEnumerable == null)
				{
					Globals.typeOfIEnumerable = typeof(IEnumerable);
				}
				return Globals.typeOfIEnumerable;
			}
		}

		// Token: 0x17000286 RID: 646
		// (get) Token: 0x06000CAC RID: 3244 RVA: 0x00031412 File Offset: 0x0002F612
		internal static Type TypeOfIEnumeratorGeneric
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIEnumeratorGeneric == null)
				{
					Globals.typeOfIEnumeratorGeneric = typeof(IEnumerator<>);
				}
				return Globals.typeOfIEnumeratorGeneric;
			}
		}

		// Token: 0x17000287 RID: 647
		// (get) Token: 0x06000CAD RID: 3245 RVA: 0x00031435 File Offset: 0x0002F635
		internal static Type TypeOfIEnumerator
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIEnumerator == null)
				{
					Globals.typeOfIEnumerator = typeof(IEnumerator);
				}
				return Globals.typeOfIEnumerator;
			}
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x06000CAE RID: 3246 RVA: 0x00031458 File Offset: 0x0002F658
		internal static Type TypeOfKeyValuePair
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfKeyValuePair == null)
				{
					Globals.typeOfKeyValuePair = typeof(KeyValuePair<, >);
				}
				return Globals.typeOfKeyValuePair;
			}
		}

		// Token: 0x17000289 RID: 649
		// (get) Token: 0x06000CAF RID: 3247 RVA: 0x0003147B File Offset: 0x0002F67B
		internal static Type TypeOfKeyValue
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfKeyValue == null)
				{
					Globals.typeOfKeyValue = typeof(KeyValue<, >);
				}
				return Globals.typeOfKeyValue;
			}
		}

		// Token: 0x1700028A RID: 650
		// (get) Token: 0x06000CB0 RID: 3248 RVA: 0x0003149E File Offset: 0x0002F69E
		internal static Type TypeOfIDictionaryEnumerator
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfIDictionaryEnumerator == null)
				{
					Globals.typeOfIDictionaryEnumerator = typeof(IDictionaryEnumerator);
				}
				return Globals.typeOfIDictionaryEnumerator;
			}
		}

		// Token: 0x1700028B RID: 651
		// (get) Token: 0x06000CB1 RID: 3249 RVA: 0x000314C1 File Offset: 0x0002F6C1
		internal static Type TypeOfDictionaryEnumerator
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfDictionaryEnumerator == null)
				{
					Globals.typeOfDictionaryEnumerator = typeof(CollectionDataContract.DictionaryEnumerator);
				}
				return Globals.typeOfDictionaryEnumerator;
			}
		}

		// Token: 0x1700028C RID: 652
		// (get) Token: 0x06000CB2 RID: 3250 RVA: 0x000314E4 File Offset: 0x0002F6E4
		internal static Type TypeOfGenericDictionaryEnumerator
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfGenericDictionaryEnumerator == null)
				{
					Globals.typeOfGenericDictionaryEnumerator = typeof(CollectionDataContract.GenericDictionaryEnumerator<, >);
				}
				return Globals.typeOfGenericDictionaryEnumerator;
			}
		}

		// Token: 0x1700028D RID: 653
		// (get) Token: 0x06000CB3 RID: 3251 RVA: 0x00031507 File Offset: 0x0002F707
		internal static Type TypeOfDictionaryGeneric
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfDictionaryGeneric == null)
				{
					Globals.typeOfDictionaryGeneric = typeof(Dictionary<, >);
				}
				return Globals.typeOfDictionaryGeneric;
			}
		}

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x06000CB4 RID: 3252 RVA: 0x0003152A File Offset: 0x0002F72A
		internal static Type TypeOfHashtable
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfHashtable == null)
				{
					Globals.typeOfHashtable = typeof(Hashtable);
				}
				return Globals.typeOfHashtable;
			}
		}

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x06000CB5 RID: 3253 RVA: 0x0003154D File Offset: 0x0002F74D
		internal static Type TypeOfListGeneric
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfListGeneric == null)
				{
					Globals.typeOfListGeneric = typeof(List<>);
				}
				return Globals.typeOfListGeneric;
			}
		}

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x06000CB6 RID: 3254 RVA: 0x00031570 File Offset: 0x0002F770
		internal static Type TypeOfXmlElement
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfXmlElement == null)
				{
					Globals.typeOfXmlElement = typeof(XmlElement);
				}
				return Globals.typeOfXmlElement;
			}
		}

		// Token: 0x17000291 RID: 657
		// (get) Token: 0x06000CB7 RID: 3255 RVA: 0x00031593 File Offset: 0x0002F793
		internal static Type TypeOfDBNull
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.typeOfDBNull == null)
				{
					Globals.typeOfDBNull = typeof(DBNull);
				}
				return Globals.typeOfDBNull;
			}
		}

		// Token: 0x17000292 RID: 658
		// (get) Token: 0x06000CB8 RID: 3256 RVA: 0x000315B6 File Offset: 0x0002F7B6
		internal static Uri DataContractXsdBaseNamespaceUri
		{
			[SecuritySafeCritical]
			get
			{
				if (Globals.dataContractXsdBaseNamespaceUri == null)
				{
					Globals.dataContractXsdBaseNamespaceUri = new Uri("http://schemas.datacontract.org/2004/07/");
				}
				return Globals.dataContractXsdBaseNamespaceUri;
			}
		}

		// Token: 0x06000CB9 RID: 3257 RVA: 0x000315D9 File Offset: 0x0002F7D9
		// Note: this type is marked as 'beforefieldinit'.
		static Globals()
		{
		}

		// Token: 0x04000547 RID: 1351
		internal const BindingFlags ScanAllMembers = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

		// Token: 0x04000548 RID: 1352
		[SecurityCritical]
		private static XmlQualifiedName idQualifiedName;

		// Token: 0x04000549 RID: 1353
		[SecurityCritical]
		private static XmlQualifiedName refQualifiedName;

		// Token: 0x0400054A RID: 1354
		[SecurityCritical]
		private static Type typeOfObject;

		// Token: 0x0400054B RID: 1355
		[SecurityCritical]
		private static Type typeOfValueType;

		// Token: 0x0400054C RID: 1356
		[SecurityCritical]
		private static Type typeOfArray;

		// Token: 0x0400054D RID: 1357
		[SecurityCritical]
		private static Type typeOfString;

		// Token: 0x0400054E RID: 1358
		[SecurityCritical]
		private static Type typeOfInt;

		// Token: 0x0400054F RID: 1359
		[SecurityCritical]
		private static Type typeOfULong;

		// Token: 0x04000550 RID: 1360
		[SecurityCritical]
		private static Type typeOfVoid;

		// Token: 0x04000551 RID: 1361
		[SecurityCritical]
		private static Type typeOfByteArray;

		// Token: 0x04000552 RID: 1362
		[SecurityCritical]
		private static Type typeOfTimeSpan;

		// Token: 0x04000553 RID: 1363
		[SecurityCritical]
		private static Type typeOfGuid;

		// Token: 0x04000554 RID: 1364
		[SecurityCritical]
		private static Type typeOfDateTimeOffset;

		// Token: 0x04000555 RID: 1365
		[SecurityCritical]
		private static Type typeOfDateTimeOffsetAdapter;

		// Token: 0x04000556 RID: 1366
		[SecurityCritical]
		private static Type typeOfUri;

		// Token: 0x04000557 RID: 1367
		[SecurityCritical]
		private static Type typeOfTypeEnumerable;

		// Token: 0x04000558 RID: 1368
		[SecurityCritical]
		private static Type typeOfStreamingContext;

		// Token: 0x04000559 RID: 1369
		[SecurityCritical]
		private static Type typeOfISerializable;

		// Token: 0x0400055A RID: 1370
		[SecurityCritical]
		private static Type typeOfIDeserializationCallback;

		// Token: 0x0400055B RID: 1371
		[SecurityCritical]
		private static Type typeOfIObjectReference;

		// Token: 0x0400055C RID: 1372
		[SecurityCritical]
		private static Type typeOfXmlFormatClassWriterDelegate;

		// Token: 0x0400055D RID: 1373
		[SecurityCritical]
		private static Type typeOfXmlFormatCollectionWriterDelegate;

		// Token: 0x0400055E RID: 1374
		[SecurityCritical]
		private static Type typeOfXmlFormatClassReaderDelegate;

		// Token: 0x0400055F RID: 1375
		[SecurityCritical]
		private static Type typeOfXmlFormatCollectionReaderDelegate;

		// Token: 0x04000560 RID: 1376
		[SecurityCritical]
		private static Type typeOfXmlFormatGetOnlyCollectionReaderDelegate;

		// Token: 0x04000561 RID: 1377
		[SecurityCritical]
		private static Type typeOfKnownTypeAttribute;

		// Token: 0x04000562 RID: 1378
		[SecurityCritical]
		private static Type typeOfDataContractAttribute;

		// Token: 0x04000563 RID: 1379
		[SecurityCritical]
		private static Type typeOfContractNamespaceAttribute;

		// Token: 0x04000564 RID: 1380
		[SecurityCritical]
		private static Type typeOfDataMemberAttribute;

		// Token: 0x04000565 RID: 1381
		[SecurityCritical]
		private static Type typeOfEnumMemberAttribute;

		// Token: 0x04000566 RID: 1382
		[SecurityCritical]
		private static Type typeOfCollectionDataContractAttribute;

		// Token: 0x04000567 RID: 1383
		[SecurityCritical]
		private static Type typeOfOptionalFieldAttribute;

		// Token: 0x04000568 RID: 1384
		[SecurityCritical]
		private static Type typeOfObjectArray;

		// Token: 0x04000569 RID: 1385
		[SecurityCritical]
		private static Type typeOfOnSerializingAttribute;

		// Token: 0x0400056A RID: 1386
		[SecurityCritical]
		private static Type typeOfOnSerializedAttribute;

		// Token: 0x0400056B RID: 1387
		[SecurityCritical]
		private static Type typeOfOnDeserializingAttribute;

		// Token: 0x0400056C RID: 1388
		[SecurityCritical]
		private static Type typeOfOnDeserializedAttribute;

		// Token: 0x0400056D RID: 1389
		[SecurityCritical]
		private static Type typeOfFlagsAttribute;

		// Token: 0x0400056E RID: 1390
		[SecurityCritical]
		private static Type typeOfSerializableAttribute;

		// Token: 0x0400056F RID: 1391
		[SecurityCritical]
		private static Type typeOfNonSerializedAttribute;

		// Token: 0x04000570 RID: 1392
		[SecurityCritical]
		private static Type typeOfSerializationInfo;

		// Token: 0x04000571 RID: 1393
		[SecurityCritical]
		private static Type typeOfSerializationInfoEnumerator;

		// Token: 0x04000572 RID: 1394
		[SecurityCritical]
		private static Type typeOfSerializationEntry;

		// Token: 0x04000573 RID: 1395
		[SecurityCritical]
		private static Type typeOfIXmlSerializable;

		// Token: 0x04000574 RID: 1396
		[SecurityCritical]
		private static Type typeOfXmlSchemaProviderAttribute;

		// Token: 0x04000575 RID: 1397
		[SecurityCritical]
		private static Type typeOfXmlRootAttribute;

		// Token: 0x04000576 RID: 1398
		[SecurityCritical]
		private static Type typeOfXmlQualifiedName;

		// Token: 0x04000577 RID: 1399
		[SecurityCritical]
		private static Type typeOfXmlSchemaType;

		// Token: 0x04000578 RID: 1400
		[SecurityCritical]
		private static Type typeOfXmlSerializableServices;

		// Token: 0x04000579 RID: 1401
		[SecurityCritical]
		private static Type typeOfXmlNodeArray;

		// Token: 0x0400057A RID: 1402
		[SecurityCritical]
		private static Type typeOfXmlSchemaSet;

		// Token: 0x0400057B RID: 1403
		[SecurityCritical]
		private static object[] emptyObjectArray;

		// Token: 0x0400057C RID: 1404
		[SecurityCritical]
		private static Type[] emptyTypeArray;

		// Token: 0x0400057D RID: 1405
		[SecurityCritical]
		private static Type typeOfIPropertyChange;

		// Token: 0x0400057E RID: 1406
		[SecurityCritical]
		private static Type typeOfIExtensibleDataObject;

		// Token: 0x0400057F RID: 1407
		[SecurityCritical]
		private static Type typeOfExtensionDataObject;

		// Token: 0x04000580 RID: 1408
		[SecurityCritical]
		private static Type typeOfISerializableDataNode;

		// Token: 0x04000581 RID: 1409
		[SecurityCritical]
		private static Type typeOfClassDataNode;

		// Token: 0x04000582 RID: 1410
		[SecurityCritical]
		private static Type typeOfCollectionDataNode;

		// Token: 0x04000583 RID: 1411
		[SecurityCritical]
		private static Type typeOfXmlDataNode;

		// Token: 0x04000584 RID: 1412
		[SecurityCritical]
		private static Type typeOfNullable;

		// Token: 0x04000585 RID: 1413
		[SecurityCritical]
		private static Type typeOfReflectionPointer;

		// Token: 0x04000586 RID: 1414
		[SecurityCritical]
		private static Type typeOfIDictionaryGeneric;

		// Token: 0x04000587 RID: 1415
		[SecurityCritical]
		private static Type typeOfIDictionary;

		// Token: 0x04000588 RID: 1416
		[SecurityCritical]
		private static Type typeOfIListGeneric;

		// Token: 0x04000589 RID: 1417
		[SecurityCritical]
		private static Type typeOfIList;

		// Token: 0x0400058A RID: 1418
		[SecurityCritical]
		private static Type typeOfICollectionGeneric;

		// Token: 0x0400058B RID: 1419
		[SecurityCritical]
		private static Type typeOfICollection;

		// Token: 0x0400058C RID: 1420
		[SecurityCritical]
		private static Type typeOfIEnumerableGeneric;

		// Token: 0x0400058D RID: 1421
		[SecurityCritical]
		private static Type typeOfIEnumerable;

		// Token: 0x0400058E RID: 1422
		[SecurityCritical]
		private static Type typeOfIEnumeratorGeneric;

		// Token: 0x0400058F RID: 1423
		[SecurityCritical]
		private static Type typeOfIEnumerator;

		// Token: 0x04000590 RID: 1424
		[SecurityCritical]
		private static Type typeOfKeyValuePair;

		// Token: 0x04000591 RID: 1425
		[SecurityCritical]
		private static Type typeOfKeyValue;

		// Token: 0x04000592 RID: 1426
		[SecurityCritical]
		private static Type typeOfIDictionaryEnumerator;

		// Token: 0x04000593 RID: 1427
		[SecurityCritical]
		private static Type typeOfDictionaryEnumerator;

		// Token: 0x04000594 RID: 1428
		[SecurityCritical]
		private static Type typeOfGenericDictionaryEnumerator;

		// Token: 0x04000595 RID: 1429
		[SecurityCritical]
		private static Type typeOfDictionaryGeneric;

		// Token: 0x04000596 RID: 1430
		[SecurityCritical]
		private static Type typeOfHashtable;

		// Token: 0x04000597 RID: 1431
		[SecurityCritical]
		private static Type typeOfListGeneric;

		// Token: 0x04000598 RID: 1432
		[SecurityCritical]
		private static Type typeOfXmlElement;

		// Token: 0x04000599 RID: 1433
		[SecurityCritical]
		private static Type typeOfDBNull;

		// Token: 0x0400059A RID: 1434
		[SecurityCritical]
		private static Uri dataContractXsdBaseNamespaceUri;

		// Token: 0x0400059B RID: 1435
		public const bool DefaultIsRequired = false;

		// Token: 0x0400059C RID: 1436
		public const bool DefaultEmitDefaultValue = true;

		// Token: 0x0400059D RID: 1437
		public const int DefaultOrder = 0;

		// Token: 0x0400059E RID: 1438
		public const bool DefaultIsReference = false;

		// Token: 0x0400059F RID: 1439
		public static readonly string NewObjectId = string.Empty;

		// Token: 0x040005A0 RID: 1440
		public const string SimpleSRSInternalsVisiblePattern = "^[\\s]*System\\.Runtime\\.Serialization[\\s]*$";

		// Token: 0x040005A1 RID: 1441
		public const string FullSRSInternalsVisiblePattern = "^[\\s]*System\\.Runtime\\.Serialization[\\s]*,[\\s]*PublicKey[\\s]*=[\\s]*(?i:00000000000000000400000000000000)[\\s]*$";

		// Token: 0x040005A2 RID: 1442
		public const string NullObjectId = null;

		// Token: 0x040005A3 RID: 1443
		public const string Space = " ";

		// Token: 0x040005A4 RID: 1444
		public const string OpenBracket = "[";

		// Token: 0x040005A5 RID: 1445
		public const string CloseBracket = "]";

		// Token: 0x040005A6 RID: 1446
		public const string Comma = ",";

		// Token: 0x040005A7 RID: 1447
		public const string XsiPrefix = "i";

		// Token: 0x040005A8 RID: 1448
		public const string XsdPrefix = "x";

		// Token: 0x040005A9 RID: 1449
		public const string SerPrefix = "z";

		// Token: 0x040005AA RID: 1450
		public const string SerPrefixForSchema = "ser";

		// Token: 0x040005AB RID: 1451
		public const string ElementPrefix = "q";

		// Token: 0x040005AC RID: 1452
		public const string DataContractXsdBaseNamespace = "http://schemas.datacontract.org/2004/07/";

		// Token: 0x040005AD RID: 1453
		public const string DataContractXmlNamespace = "http://schemas.datacontract.org/2004/07/System.Xml";

		// Token: 0x040005AE RID: 1454
		public const string SchemaInstanceNamespace = "http://www.w3.org/2001/XMLSchema-instance";

		// Token: 0x040005AF RID: 1455
		public const string SchemaNamespace = "http://www.w3.org/2001/XMLSchema";

		// Token: 0x040005B0 RID: 1456
		public const string XsiNilLocalName = "nil";

		// Token: 0x040005B1 RID: 1457
		public const string XsiTypeLocalName = "type";

		// Token: 0x040005B2 RID: 1458
		public const string TnsPrefix = "tns";

		// Token: 0x040005B3 RID: 1459
		public const string OccursUnbounded = "unbounded";

		// Token: 0x040005B4 RID: 1460
		public const string AnyTypeLocalName = "anyType";

		// Token: 0x040005B5 RID: 1461
		public const string StringLocalName = "string";

		// Token: 0x040005B6 RID: 1462
		public const string IntLocalName = "int";

		// Token: 0x040005B7 RID: 1463
		public const string True = "true";

		// Token: 0x040005B8 RID: 1464
		public const string False = "false";

		// Token: 0x040005B9 RID: 1465
		public const string ArrayPrefix = "ArrayOf";

		// Token: 0x040005BA RID: 1466
		public const string XmlnsNamespace = "http://www.w3.org/2000/xmlns/";

		// Token: 0x040005BB RID: 1467
		public const string XmlnsPrefix = "xmlns";

		// Token: 0x040005BC RID: 1468
		public const string SchemaLocalName = "schema";

		// Token: 0x040005BD RID: 1469
		public const string CollectionsNamespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays";

		// Token: 0x040005BE RID: 1470
		public const string DefaultClrNamespace = "GeneratedNamespace";

		// Token: 0x040005BF RID: 1471
		public const string DefaultTypeName = "GeneratedType";

		// Token: 0x040005C0 RID: 1472
		public const string DefaultGeneratedMember = "GeneratedMember";

		// Token: 0x040005C1 RID: 1473
		public const string DefaultFieldSuffix = "Field";

		// Token: 0x040005C2 RID: 1474
		public const string DefaultPropertySuffix = "Property";

		// Token: 0x040005C3 RID: 1475
		public const string DefaultMemberSuffix = "Member";

		// Token: 0x040005C4 RID: 1476
		public const string NameProperty = "Name";

		// Token: 0x040005C5 RID: 1477
		public const string NamespaceProperty = "Namespace";

		// Token: 0x040005C6 RID: 1478
		public const string OrderProperty = "Order";

		// Token: 0x040005C7 RID: 1479
		public const string IsReferenceProperty = "IsReference";

		// Token: 0x040005C8 RID: 1480
		public const string IsRequiredProperty = "IsRequired";

		// Token: 0x040005C9 RID: 1481
		public const string EmitDefaultValueProperty = "EmitDefaultValue";

		// Token: 0x040005CA RID: 1482
		public const string ClrNamespaceProperty = "ClrNamespace";

		// Token: 0x040005CB RID: 1483
		public const string ItemNameProperty = "ItemName";

		// Token: 0x040005CC RID: 1484
		public const string KeyNameProperty = "KeyName";

		// Token: 0x040005CD RID: 1485
		public const string ValueNameProperty = "ValueName";

		// Token: 0x040005CE RID: 1486
		public const string SerializationInfoPropertyName = "SerializationInfo";

		// Token: 0x040005CF RID: 1487
		public const string SerializationInfoFieldName = "info";

		// Token: 0x040005D0 RID: 1488
		public const string NodeArrayPropertyName = "Nodes";

		// Token: 0x040005D1 RID: 1489
		public const string NodeArrayFieldName = "nodesField";

		// Token: 0x040005D2 RID: 1490
		public const string ExportSchemaMethod = "ExportSchema";

		// Token: 0x040005D3 RID: 1491
		public const string IsAnyProperty = "IsAny";

		// Token: 0x040005D4 RID: 1492
		public const string ContextFieldName = "context";

		// Token: 0x040005D5 RID: 1493
		public const string GetObjectDataMethodName = "GetObjectData";

		// Token: 0x040005D6 RID: 1494
		public const string GetEnumeratorMethodName = "GetEnumerator";

		// Token: 0x040005D7 RID: 1495
		public const string MoveNextMethodName = "MoveNext";

		// Token: 0x040005D8 RID: 1496
		public const string AddValueMethodName = "AddValue";

		// Token: 0x040005D9 RID: 1497
		public const string CurrentPropertyName = "Current";

		// Token: 0x040005DA RID: 1498
		public const string ValueProperty = "Value";

		// Token: 0x040005DB RID: 1499
		public const string EnumeratorFieldName = "enumerator";

		// Token: 0x040005DC RID: 1500
		public const string SerializationEntryFieldName = "entry";

		// Token: 0x040005DD RID: 1501
		public const string ExtensionDataSetMethod = "set_ExtensionData";

		// Token: 0x040005DE RID: 1502
		public const string ExtensionDataSetExplicitMethod = "System.Runtime.Serialization.IExtensibleDataObject.set_ExtensionData";

		// Token: 0x040005DF RID: 1503
		public const string ExtensionDataObjectPropertyName = "ExtensionData";

		// Token: 0x040005E0 RID: 1504
		public const string ExtensionDataObjectFieldName = "extensionDataField";

		// Token: 0x040005E1 RID: 1505
		public const string AddMethodName = "Add";

		// Token: 0x040005E2 RID: 1506
		public const string ParseMethodName = "Parse";

		// Token: 0x040005E3 RID: 1507
		public const string GetCurrentMethodName = "get_Current";

		// Token: 0x040005E4 RID: 1508
		public const string SerializationNamespace = "http://schemas.microsoft.com/2003/10/Serialization/";

		// Token: 0x040005E5 RID: 1509
		public const string ClrTypeLocalName = "Type";

		// Token: 0x040005E6 RID: 1510
		public const string ClrAssemblyLocalName = "Assembly";

		// Token: 0x040005E7 RID: 1511
		public const string IsValueTypeLocalName = "IsValueType";

		// Token: 0x040005E8 RID: 1512
		public const string EnumerationValueLocalName = "EnumerationValue";

		// Token: 0x040005E9 RID: 1513
		public const string SurrogateDataLocalName = "Surrogate";

		// Token: 0x040005EA RID: 1514
		public const string GenericTypeLocalName = "GenericType";

		// Token: 0x040005EB RID: 1515
		public const string GenericParameterLocalName = "GenericParameter";

		// Token: 0x040005EC RID: 1516
		public const string GenericNameAttribute = "Name";

		// Token: 0x040005ED RID: 1517
		public const string GenericNamespaceAttribute = "Namespace";

		// Token: 0x040005EE RID: 1518
		public const string GenericParameterNestedLevelAttribute = "NestedLevel";

		// Token: 0x040005EF RID: 1519
		public const string IsDictionaryLocalName = "IsDictionary";

		// Token: 0x040005F0 RID: 1520
		public const string ActualTypeLocalName = "ActualType";

		// Token: 0x040005F1 RID: 1521
		public const string ActualTypeNameAttribute = "Name";

		// Token: 0x040005F2 RID: 1522
		public const string ActualTypeNamespaceAttribute = "Namespace";

		// Token: 0x040005F3 RID: 1523
		public const string DefaultValueLocalName = "DefaultValue";

		// Token: 0x040005F4 RID: 1524
		public const string EmitDefaultValueAttribute = "EmitDefaultValue";

		// Token: 0x040005F5 RID: 1525
		public const string ISerializableFactoryTypeLocalName = "FactoryType";

		// Token: 0x040005F6 RID: 1526
		public const string IdLocalName = "Id";

		// Token: 0x040005F7 RID: 1527
		public const string RefLocalName = "Ref";

		// Token: 0x040005F8 RID: 1528
		public const string ArraySizeLocalName = "Size";

		// Token: 0x040005F9 RID: 1529
		public const string KeyLocalName = "Key";

		// Token: 0x040005FA RID: 1530
		public const string ValueLocalName = "Value";

		// Token: 0x040005FB RID: 1531
		public const string MscorlibAssemblyName = "0";

		// Token: 0x040005FC RID: 1532
		public const string MscorlibAssemblySimpleName = "mscorlib";

		// Token: 0x040005FD RID: 1533
		public const string MscorlibFileName = "mscorlib.dll";

		// Token: 0x040005FE RID: 1534
		public const string SerializationSchema = "<?xml version='1.0' encoding='utf-8'?>\n<xs:schema elementFormDefault='qualified' attributeFormDefault='qualified' xmlns:tns='http://schemas.microsoft.com/2003/10/Serialization/' targetNamespace='http://schemas.microsoft.com/2003/10/Serialization/' xmlns:xs='http://www.w3.org/2001/XMLSchema'>\n  <xs:element name='anyType' nillable='true' type='xs:anyType' />\n  <xs:element name='anyURI' nillable='true' type='xs:anyURI' />\n  <xs:element name='base64Binary' nillable='true' type='xs:base64Binary' />\n  <xs:element name='boolean' nillable='true' type='xs:boolean' />\n  <xs:element name='byte' nillable='true' type='xs:byte' />\n  <xs:element name='dateTime' nillable='true' type='xs:dateTime' />\n  <xs:element name='decimal' nillable='true' type='xs:decimal' />\n  <xs:element name='double' nillable='true' type='xs:double' />\n  <xs:element name='float' nillable='true' type='xs:float' />\n  <xs:element name='int' nillable='true' type='xs:int' />\n  <xs:element name='long' nillable='true' type='xs:long' />\n  <xs:element name='QName' nillable='true' type='xs:QName' />\n  <xs:element name='short' nillable='true' type='xs:short' />\n  <xs:element name='string' nillable='true' type='xs:string' />\n  <xs:element name='unsignedByte' nillable='true' type='xs:unsignedByte' />\n  <xs:element name='unsignedInt' nillable='true' type='xs:unsignedInt' />\n  <xs:element name='unsignedLong' nillable='true' type='xs:unsignedLong' />\n  <xs:element name='unsignedShort' nillable='true' type='xs:unsignedShort' />\n  <xs:element name='char' nillable='true' type='tns:char' />\n  <xs:simpleType name='char'>\n    <xs:restriction base='xs:int'/>\n  </xs:simpleType>  \n  <xs:element name='duration' nillable='true' type='tns:duration' />\n  <xs:simpleType name='duration'>\n    <xs:restriction base='xs:duration'>\n      <xs:pattern value='\\-?P(\\d*D)?(T(\\d*H)?(\\d*M)?(\\d*(\\.\\d*)?S)?)?' />\n      <xs:minInclusive value='-P10675199DT2H48M5.4775808S' />\n      <xs:maxInclusive value='P10675199DT2H48M5.4775807S' />\n    </xs:restriction>\n  </xs:simpleType>\n  <xs:element name='guid' nillable='true' type='tns:guid' />\n  <xs:simpleType name='guid'>\n    <xs:restriction base='xs:string'>\n      <xs:pattern value='[\\da-fA-F]{8}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{4}-[\\da-fA-F]{12}' />\n    </xs:restriction>\n  </xs:simpleType>\n  <xs:attribute name='FactoryType' type='xs:QName' />\n  <xs:attribute name='Id' type='xs:ID' />\n  <xs:attribute name='Ref' type='xs:IDREF' />\n</xs:schema>\n";
	}
}
