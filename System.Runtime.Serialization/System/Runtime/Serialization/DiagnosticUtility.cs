﻿using System;
using System.Diagnostics;

namespace System.Runtime.Serialization
{
	// Token: 0x0200014C RID: 332
	internal static class DiagnosticUtility
	{
		// Token: 0x0600111C RID: 4380 RVA: 0x0003D34B File Offset: 0x0003B54B
		// Note: this type is marked as 'beforefieldinit'.
		static DiagnosticUtility()
		{
		}

		// Token: 0x04000703 RID: 1795
		internal static bool ShouldTraceError = true;

		// Token: 0x04000704 RID: 1796
		internal static readonly bool ShouldTraceWarning = false;

		// Token: 0x04000705 RID: 1797
		internal static readonly bool ShouldTraceInformation = false;

		// Token: 0x04000706 RID: 1798
		internal static bool ShouldTraceVerbose = true;

		// Token: 0x0200014D RID: 333
		internal static class DiagnosticTrace
		{
			// Token: 0x0600111D RID: 4381 RVA: 0x000020AE File Offset: 0x000002AE
			internal static void TraceEvent(params object[] args)
			{
			}
		}

		// Token: 0x0200014E RID: 334
		internal static class ExceptionUtility
		{
			// Token: 0x0600111E RID: 4382 RVA: 0x0003D365 File Offset: 0x0003B565
			internal static Exception ThrowHelperError(Exception e)
			{
				return DiagnosticUtility.ExceptionUtility.ThrowHelper(e, TraceEventType.Error);
			}

			// Token: 0x0600111F RID: 4383 RVA: 0x0003D36E File Offset: 0x0003B56E
			internal static Exception ThrowHelperCallback(string msg, Exception e)
			{
				return new CallbackException(msg, e);
			}

			// Token: 0x06001120 RID: 4384 RVA: 0x0003D377 File Offset: 0x0003B577
			internal static Exception ThrowHelperCallback(Exception e)
			{
				return new CallbackException("Callback exception", e);
			}

			// Token: 0x06001121 RID: 4385 RVA: 0x00002068 File Offset: 0x00000268
			internal static Exception ThrowHelper(Exception e, TraceEventType type)
			{
				return e;
			}

			// Token: 0x06001122 RID: 4386 RVA: 0x0003D384 File Offset: 0x0003B584
			internal static Exception ThrowHelperArgument(string arg)
			{
				return new ArgumentException(arg);
			}

			// Token: 0x06001123 RID: 4387 RVA: 0x0003D38C File Offset: 0x0003B58C
			internal static Exception ThrowHelperArgument(string arg, string message)
			{
				return new ArgumentException(message, arg);
			}

			// Token: 0x06001124 RID: 4388 RVA: 0x0003D395 File Offset: 0x0003B595
			internal static Exception ThrowHelperArgumentNull(string arg)
			{
				return new ArgumentNullException(arg);
			}

			// Token: 0x06001125 RID: 4389 RVA: 0x0003D39D File Offset: 0x0003B59D
			internal static Exception ThrowHelperFatal(string msg, Exception e)
			{
				return new FatalException(msg, e);
			}
		}
	}
}
