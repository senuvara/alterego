﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000D6 RID: 214
	internal class XmlDataNode : DataNode<object>
	{
		// Token: 0x06000C09 RID: 3081 RVA: 0x0002F32A File Offset: 0x0002D52A
		internal XmlDataNode()
		{
			this.dataType = Globals.TypeOfXmlDataNode;
		}

		// Token: 0x1700021F RID: 543
		// (get) Token: 0x06000C0A RID: 3082 RVA: 0x0002F33D File Offset: 0x0002D53D
		// (set) Token: 0x06000C0B RID: 3083 RVA: 0x0002F345 File Offset: 0x0002D545
		internal IList<XmlAttribute> XmlAttributes
		{
			get
			{
				return this.xmlAttributes;
			}
			set
			{
				this.xmlAttributes = value;
			}
		}

		// Token: 0x17000220 RID: 544
		// (get) Token: 0x06000C0C RID: 3084 RVA: 0x0002F34E File Offset: 0x0002D54E
		// (set) Token: 0x06000C0D RID: 3085 RVA: 0x0002F356 File Offset: 0x0002D556
		internal IList<XmlNode> XmlChildNodes
		{
			get
			{
				return this.xmlChildNodes;
			}
			set
			{
				this.xmlChildNodes = value;
			}
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x06000C0E RID: 3086 RVA: 0x0002F35F File Offset: 0x0002D55F
		// (set) Token: 0x06000C0F RID: 3087 RVA: 0x0002F367 File Offset: 0x0002D567
		internal XmlDocument OwnerDocument
		{
			get
			{
				return this.ownerDocument;
			}
			set
			{
				this.ownerDocument = value;
			}
		}

		// Token: 0x06000C10 RID: 3088 RVA: 0x0002F370 File Offset: 0x0002D570
		public override void Clear()
		{
			base.Clear();
			this.xmlAttributes = null;
			this.xmlChildNodes = null;
			this.ownerDocument = null;
		}

		// Token: 0x04000517 RID: 1303
		private IList<XmlAttribute> xmlAttributes;

		// Token: 0x04000518 RID: 1304
		private IList<XmlNode> xmlChildNodes;

		// Token: 0x04000519 RID: 1305
		private XmlDocument ownerDocument;
	}
}
