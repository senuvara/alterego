﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x0200010C RID: 268
	internal class TokenDataContract : StringDataContract
	{
		// Token: 0x06000DBC RID: 3516 RVA: 0x0003382A File Offset: 0x00031A2A
		internal TokenDataContract() : base(DictionaryGlobals.tokenLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
