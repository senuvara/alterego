﻿using System;

namespace System.Runtime.Serialization
{
	/// <summary>When applied to a collection type, enables custom specification of the collection item elements. This attribute can be applied only to types that are recognized by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" /> as valid, serializable collections. </summary>
	// Token: 0x020000B5 RID: 181
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = false, AllowMultiple = false)]
	public sealed class CollectionDataContractAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.CollectionDataContractAttribute" /> class. </summary>
		// Token: 0x060009E6 RID: 2534 RVA: 0x000254EB File Offset: 0x000236EB
		public CollectionDataContractAttribute()
		{
		}

		/// <summary>Gets or sets the namespace for the data contract.</summary>
		/// <returns>The namespace of the data contract.</returns>
		// Token: 0x1700017A RID: 378
		// (get) Token: 0x060009E7 RID: 2535 RVA: 0x000291EE File Offset: 0x000273EE
		// (set) Token: 0x060009E8 RID: 2536 RVA: 0x000291F6 File Offset: 0x000273F6
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
				this.isNamespaceSetExplicitly = true;
			}
		}

		/// <summary>Gets whether <see cref="P:System.Runtime.Serialization.CollectionDataContractAttribute.Namespace" /> has been explicitly set.</summary>
		/// <returns>Returns <see langword="true" /> if the item namespace has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700017B RID: 379
		// (get) Token: 0x060009E9 RID: 2537 RVA: 0x00029206 File Offset: 0x00027406
		public bool IsNamespaceSetExplicitly
		{
			get
			{
				return this.isNamespaceSetExplicitly;
			}
		}

		/// <summary>Gets or sets the data contract name for the collection type.</summary>
		/// <returns>The data contract name for the collection type.</returns>
		// Token: 0x1700017C RID: 380
		// (get) Token: 0x060009EA RID: 2538 RVA: 0x0002920E File Offset: 0x0002740E
		// (set) Token: 0x060009EB RID: 2539 RVA: 0x00029216 File Offset: 0x00027416
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
				this.isNameSetExplicitly = true;
			}
		}

		/// <summary>Gets whether <see cref="P:System.Runtime.Serialization.CollectionDataContractAttribute.Name" /> has been explicitly set.</summary>
		/// <returns>Returns <see langword="true" /> if the name has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700017D RID: 381
		// (get) Token: 0x060009EC RID: 2540 RVA: 0x00029226 File Offset: 0x00027426
		public bool IsNameSetExplicitly
		{
			get
			{
				return this.isNameSetExplicitly;
			}
		}

		/// <summary>Gets or sets a custom name for a collection element.</summary>
		/// <returns>The name to apply to collection elements.</returns>
		// Token: 0x1700017E RID: 382
		// (get) Token: 0x060009ED RID: 2541 RVA: 0x0002922E File Offset: 0x0002742E
		// (set) Token: 0x060009EE RID: 2542 RVA: 0x00029236 File Offset: 0x00027436
		public string ItemName
		{
			get
			{
				return this.itemName;
			}
			set
			{
				this.itemName = value;
				this.isItemNameSetExplicitly = true;
			}
		}

		/// <summary>Gets whether <see cref="P:System.Runtime.Serialization.CollectionDataContractAttribute.ItemName" /> has been explicitly set.</summary>
		/// <returns>Returns <see langword="true" /> if the item name has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700017F RID: 383
		// (get) Token: 0x060009EF RID: 2543 RVA: 0x00029246 File Offset: 0x00027446
		public bool IsItemNameSetExplicitly
		{
			get
			{
				return this.isItemNameSetExplicitly;
			}
		}

		/// <summary>Gets or sets the custom name for a dictionary key name.</summary>
		/// <returns>The name to use instead of the default dictionary key name.</returns>
		// Token: 0x17000180 RID: 384
		// (get) Token: 0x060009F0 RID: 2544 RVA: 0x0002924E File Offset: 0x0002744E
		// (set) Token: 0x060009F1 RID: 2545 RVA: 0x00029256 File Offset: 0x00027456
		public string KeyName
		{
			get
			{
				return this.keyName;
			}
			set
			{
				this.keyName = value;
				this.isKeyNameSetExplicitly = true;
			}
		}

		/// <summary>Gets or sets a value that indicates whether to preserve object reference data.</summary>
		/// <returns>
		///     <see langword="true" /> to keep object reference data; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000181 RID: 385
		// (get) Token: 0x060009F2 RID: 2546 RVA: 0x00029266 File Offset: 0x00027466
		// (set) Token: 0x060009F3 RID: 2547 RVA: 0x0002926E File Offset: 0x0002746E
		public bool IsReference
		{
			get
			{
				return this.isReference;
			}
			set
			{
				this.isReference = value;
				this.isReferenceSetExplicitly = true;
			}
		}

		/// <summary>Gets whether reference has been explicitly set.</summary>
		/// <returns>Returns <see langword="true" /> if the reference has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000182 RID: 386
		// (get) Token: 0x060009F4 RID: 2548 RVA: 0x0002927E File Offset: 0x0002747E
		public bool IsReferenceSetExplicitly
		{
			get
			{
				return this.isReferenceSetExplicitly;
			}
		}

		/// <summary>Gets whether <see cref="P:System.Runtime.Serialization.CollectionDataContractAttribute.KeyName" /> has been explicitly set.</summary>
		/// <returns>Returns <see langword="true" /> if the key name has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000183 RID: 387
		// (get) Token: 0x060009F5 RID: 2549 RVA: 0x00029286 File Offset: 0x00027486
		public bool IsKeyNameSetExplicitly
		{
			get
			{
				return this.isKeyNameSetExplicitly;
			}
		}

		/// <summary>Gets or sets the custom name for a dictionary value name.</summary>
		/// <returns>The name to use instead of the default dictionary value name.</returns>
		// Token: 0x17000184 RID: 388
		// (get) Token: 0x060009F6 RID: 2550 RVA: 0x0002928E File Offset: 0x0002748E
		// (set) Token: 0x060009F7 RID: 2551 RVA: 0x00029296 File Offset: 0x00027496
		public string ValueName
		{
			get
			{
				return this.valueName;
			}
			set
			{
				this.valueName = value;
				this.isValueNameSetExplicitly = true;
			}
		}

		/// <summary>Gets whether <see cref="P:System.Runtime.Serialization.CollectionDataContractAttribute.ValueName" /> has been explicitly set.</summary>
		/// <returns>Returns <see langword="true" /> if the value name has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000185 RID: 389
		// (get) Token: 0x060009F8 RID: 2552 RVA: 0x000292A6 File Offset: 0x000274A6
		public bool IsValueNameSetExplicitly
		{
			get
			{
				return this.isValueNameSetExplicitly;
			}
		}

		// Token: 0x04000445 RID: 1093
		private string name;

		// Token: 0x04000446 RID: 1094
		private string ns;

		// Token: 0x04000447 RID: 1095
		private string itemName;

		// Token: 0x04000448 RID: 1096
		private string keyName;

		// Token: 0x04000449 RID: 1097
		private string valueName;

		// Token: 0x0400044A RID: 1098
		private bool isReference;

		// Token: 0x0400044B RID: 1099
		private bool isNameSetExplicitly;

		// Token: 0x0400044C RID: 1100
		private bool isNamespaceSetExplicitly;

		// Token: 0x0400044D RID: 1101
		private bool isReferenceSetExplicitly;

		// Token: 0x0400044E RID: 1102
		private bool isItemNameSetExplicitly;

		// Token: 0x0400044F RID: 1103
		private bool isKeyNameSetExplicitly;

		// Token: 0x04000450 RID: 1104
		private bool isValueNameSetExplicitly;
	}
}
