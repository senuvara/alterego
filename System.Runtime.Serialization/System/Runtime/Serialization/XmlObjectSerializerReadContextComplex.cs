﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Diagnostics.Application;
using System.Runtime.Serialization.Formatters;
using System.Security;
using System.Security.Permissions;

namespace System.Runtime.Serialization
{
	// Token: 0x0200013F RID: 319
	internal class XmlObjectSerializerReadContextComplex : XmlObjectSerializerReadContext
	{
		// Token: 0x06000F64 RID: 3940 RVA: 0x000383FF File Offset: 0x000365FF
		internal XmlObjectSerializerReadContextComplex(DataContractSerializer serializer, DataContract rootTypeDataContract, DataContractResolver dataContractResolver) : base(serializer, rootTypeDataContract, dataContractResolver)
		{
			this.mode = SerializationMode.SharedContract;
			this.preserveObjectReferences = serializer.PreserveObjectReferences;
			this.dataContractSurrogate = serializer.DataContractSurrogate;
		}

		// Token: 0x06000F65 RID: 3941 RVA: 0x00038429 File Offset: 0x00036629
		internal XmlObjectSerializerReadContextComplex(NetDataContractSerializer serializer) : base(serializer)
		{
			this.mode = SerializationMode.SharedType;
			this.preserveObjectReferences = true;
			this.binder = serializer.Binder;
			this.surrogateSelector = serializer.SurrogateSelector;
			this.assemblyFormat = serializer.AssemblyFormat;
		}

		// Token: 0x06000F66 RID: 3942 RVA: 0x00038464 File Offset: 0x00036664
		internal XmlObjectSerializerReadContextComplex(XmlObjectSerializer serializer, int maxItemsInObjectGraph, StreamingContext streamingContext, bool ignoreExtensionDataObject) : base(serializer, maxItemsInObjectGraph, streamingContext, ignoreExtensionDataObject)
		{
		}

		// Token: 0x17000378 RID: 888
		// (get) Token: 0x06000F67 RID: 3943 RVA: 0x00038471 File Offset: 0x00036671
		internal override SerializationMode Mode
		{
			get
			{
				return this.mode;
			}
		}

		// Token: 0x06000F68 RID: 3944 RVA: 0x0003847C File Offset: 0x0003667C
		internal override DataContract GetDataContract(int id, RuntimeTypeHandle typeHandle)
		{
			DataContract dataContract = null;
			if (this.mode == SerializationMode.SharedType && this.surrogateSelector != null)
			{
				dataContract = NetDataContractSerializer.GetDataContractFromSurrogateSelector(this.surrogateSelector, base.GetStreamingContext(), typeHandle, null, ref this.surrogateDataContracts);
			}
			if (dataContract == null)
			{
				return base.GetDataContract(id, typeHandle);
			}
			if (this.IsGetOnlyCollection && dataContract is SurrogateDataContract)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Surrogates with get-only collections are not supported. Found on type '{0}'.", new object[]
				{
					DataContract.GetClrTypeFullName(dataContract.UnderlyingType)
				})));
			}
			return dataContract;
		}

		// Token: 0x06000F69 RID: 3945 RVA: 0x000384FC File Offset: 0x000366FC
		internal override DataContract GetDataContract(RuntimeTypeHandle typeHandle, Type type)
		{
			DataContract dataContract = null;
			if (this.mode == SerializationMode.SharedType && this.surrogateSelector != null)
			{
				dataContract = NetDataContractSerializer.GetDataContractFromSurrogateSelector(this.surrogateSelector, base.GetStreamingContext(), typeHandle, type, ref this.surrogateDataContracts);
			}
			if (dataContract == null)
			{
				return base.GetDataContract(typeHandle, type);
			}
			if (this.IsGetOnlyCollection && dataContract is SurrogateDataContract)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Surrogates with get-only collections are not supported. Found on type '{0}'.", new object[]
				{
					DataContract.GetClrTypeFullName(dataContract.UnderlyingType)
				})));
			}
			return dataContract;
		}

		// Token: 0x06000F6A RID: 3946 RVA: 0x0003857C File Offset: 0x0003677C
		public override object InternalDeserialize(XmlReaderDelegator xmlReader, int declaredTypeID, RuntimeTypeHandle declaredTypeHandle, string name, string ns)
		{
			if (this.mode != SerializationMode.SharedContract)
			{
				return this.InternalDeserializeInSharedTypeMode(xmlReader, declaredTypeID, Type.GetTypeFromHandle(declaredTypeHandle), name, ns);
			}
			if (this.dataContractSurrogate == null)
			{
				return base.InternalDeserialize(xmlReader, declaredTypeID, declaredTypeHandle, name, ns);
			}
			return this.InternalDeserializeWithSurrogate(xmlReader, Type.GetTypeFromHandle(declaredTypeHandle), null, name, ns);
		}

		// Token: 0x06000F6B RID: 3947 RVA: 0x000385CC File Offset: 0x000367CC
		internal override object InternalDeserialize(XmlReaderDelegator xmlReader, Type declaredType, string name, string ns)
		{
			if (this.mode != SerializationMode.SharedContract)
			{
				return this.InternalDeserializeInSharedTypeMode(xmlReader, -1, declaredType, name, ns);
			}
			if (this.dataContractSurrogate == null)
			{
				return base.InternalDeserialize(xmlReader, declaredType, name, ns);
			}
			return this.InternalDeserializeWithSurrogate(xmlReader, declaredType, null, name, ns);
		}

		// Token: 0x06000F6C RID: 3948 RVA: 0x00038603 File Offset: 0x00036803
		internal override object InternalDeserialize(XmlReaderDelegator xmlReader, Type declaredType, DataContract dataContract, string name, string ns)
		{
			if (this.mode != SerializationMode.SharedContract)
			{
				return this.InternalDeserializeInSharedTypeMode(xmlReader, -1, declaredType, name, ns);
			}
			if (this.dataContractSurrogate == null)
			{
				return base.InternalDeserialize(xmlReader, declaredType, dataContract, name, ns);
			}
			return this.InternalDeserializeWithSurrogate(xmlReader, declaredType, dataContract, name, ns);
		}

		// Token: 0x06000F6D RID: 3949 RVA: 0x00038640 File Offset: 0x00036840
		private object InternalDeserializeInSharedTypeMode(XmlReaderDelegator xmlReader, int declaredTypeID, Type declaredType, string name, string ns)
		{
			object result = null;
			if (base.TryHandleNullOrRef(xmlReader, declaredType, name, ns, ref result))
			{
				return result;
			}
			string clrAssembly = this.attributes.ClrAssembly;
			string clrType = this.attributes.ClrType;
			DataContract dataContract;
			if (clrAssembly != null && clrType != null)
			{
				Assembly assembly;
				Type left;
				dataContract = this.ResolveDataContractInSharedTypeMode(clrAssembly, clrType, out assembly, out left);
				if (dataContract == null)
				{
					if (assembly == null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Assembly '{0}' was not found.", new object[]
						{
							clrAssembly
						})));
					}
					if (left == null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("CLR type '{1}' in assembly '{0}' is not found.", new object[]
						{
							assembly.FullName,
							clrType
						})));
					}
				}
				if (declaredType != null && declaredType.IsArray)
				{
					dataContract = ((declaredTypeID < 0) ? base.GetDataContract(declaredType) : this.GetDataContract(declaredTypeID, declaredType.TypeHandle));
				}
			}
			else
			{
				if (clrAssembly != null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.TryAddLineInfo(xmlReader, SR.GetString("Attribute was not found for CLR type '{1}' in namespace '{0}'. XML reader node is on {2}, '{4}' node in '{3}' namespace.", new object[]
					{
						"http://schemas.microsoft.com/2003/10/Serialization/",
						"Type",
						xmlReader.NodeType,
						xmlReader.NamespaceURI,
						xmlReader.LocalName
					}))));
				}
				if (clrType != null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.TryAddLineInfo(xmlReader, SR.GetString("Attribute was not found for CLR type '{1}' in namespace '{0}'. XML reader node is on {2}, '{4}' node in '{3}' namespace.", new object[]
					{
						"http://schemas.microsoft.com/2003/10/Serialization/",
						"Assembly",
						xmlReader.NodeType,
						xmlReader.NamespaceURI,
						xmlReader.LocalName
					}))));
				}
				if (declaredType == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.TryAddLineInfo(xmlReader, SR.GetString("Attribute was not found for CLR type '{1}' in namespace '{0}'. XML reader node is on {2}, '{4}' node in '{3}' namespace.", new object[]
					{
						"http://schemas.microsoft.com/2003/10/Serialization/",
						"Type",
						xmlReader.NodeType,
						xmlReader.NamespaceURI,
						xmlReader.LocalName
					}))));
				}
				dataContract = ((declaredTypeID < 0) ? base.GetDataContract(declaredType) : this.GetDataContract(declaredTypeID, declaredType.TypeHandle));
			}
			return this.ReadDataContractValue(dataContract, xmlReader);
		}

		// Token: 0x06000F6E RID: 3950 RVA: 0x00038850 File Offset: 0x00036A50
		private object InternalDeserializeWithSurrogate(XmlReaderDelegator xmlReader, Type declaredType, DataContract surrogateDataContract, string name, string ns)
		{
			if (TD.DCDeserializeWithSurrogateStartIsEnabled())
			{
				TD.DCDeserializeWithSurrogateStart(declaredType.FullName);
			}
			DataContract dataContract = surrogateDataContract ?? base.GetDataContract(DataContractSurrogateCaller.GetDataContractType(this.dataContractSurrogate, declaredType));
			if (this.IsGetOnlyCollection && dataContract.UnderlyingType != declaredType)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Surrogates with get-only collections are not supported. Found on type '{0}'.", new object[]
				{
					DataContract.GetClrTypeFullName(declaredType)
				})));
			}
			this.ReadAttributes(xmlReader);
			string objectId = base.GetObjectId();
			object obj = base.InternalDeserialize(xmlReader, name, ns, declaredType, ref dataContract);
			object deserializedObject = DataContractSurrogateCaller.GetDeserializedObject(this.dataContractSurrogate, obj, dataContract.UnderlyingType, declaredType);
			base.ReplaceDeserializedObject(objectId, obj, deserializedObject);
			if (TD.DCDeserializeWithSurrogateStopIsEnabled())
			{
				TD.DCDeserializeWithSurrogateStop();
			}
			return deserializedObject;
		}

		// Token: 0x06000F6F RID: 3951 RVA: 0x00038908 File Offset: 0x00036B08
		private Type ResolveDataContractTypeInSharedTypeMode(string assemblyName, string typeName, out Assembly assembly)
		{
			assembly = null;
			Type type = null;
			if (this.binder != null)
			{
				type = this.binder.BindToType(assemblyName, typeName);
			}
			if (type == null)
			{
				XmlObjectSerializerReadContextComplex.XmlObjectDataContractTypeKey key = new XmlObjectSerializerReadContextComplex.XmlObjectDataContractTypeKey(assemblyName, typeName);
				XmlObjectSerializerReadContextComplex.XmlObjectDataContractTypeInfo xmlObjectDataContractTypeInfo = (XmlObjectSerializerReadContextComplex.XmlObjectDataContractTypeInfo)XmlObjectSerializerReadContextComplex.dataContractTypeCache[key];
				if (xmlObjectDataContractTypeInfo == null)
				{
					if (this.assemblyFormat == FormatterAssemblyStyle.Full)
					{
						if (assemblyName == "0")
						{
							assembly = Globals.TypeOfInt.Assembly;
						}
						else
						{
							assembly = Assembly.Load(assemblyName);
						}
						if (assembly != null)
						{
							type = assembly.GetType(typeName);
						}
					}
					else
					{
						assembly = XmlObjectSerializerReadContextComplex.ResolveSimpleAssemblyName(assemblyName);
						if (assembly != null)
						{
							try
							{
								type = assembly.GetType(typeName);
							}
							catch (TypeLoadException)
							{
							}
							catch (FileNotFoundException)
							{
							}
							catch (FileLoadException)
							{
							}
							catch (BadImageFormatException)
							{
							}
							if (type == null)
							{
								type = Type.GetType(typeName, new Func<AssemblyName, Assembly>(XmlObjectSerializerReadContextComplex.ResolveSimpleAssemblyName), new Func<Assembly, string, bool, Type>(new XmlObjectSerializerReadContextComplex.TopLevelAssemblyTypeResolver(assembly).ResolveType), false);
							}
						}
					}
					if (!(type != null))
					{
						return type;
					}
					XmlObjectSerializerReadContextComplex.CheckTypeForwardedTo(assembly, type.Assembly, type);
					xmlObjectDataContractTypeInfo = new XmlObjectSerializerReadContextComplex.XmlObjectDataContractTypeInfo(assembly, type);
					Hashtable obj = XmlObjectSerializerReadContextComplex.dataContractTypeCache;
					lock (obj)
					{
						if (!XmlObjectSerializerReadContextComplex.dataContractTypeCache.ContainsKey(key))
						{
							XmlObjectSerializerReadContextComplex.dataContractTypeCache[key] = xmlObjectDataContractTypeInfo;
						}
						return type;
					}
				}
				assembly = xmlObjectDataContractTypeInfo.Assembly;
				type = xmlObjectDataContractTypeInfo.Type;
			}
			return type;
		}

		// Token: 0x06000F70 RID: 3952 RVA: 0x00038A9C File Offset: 0x00036C9C
		private DataContract ResolveDataContractInSharedTypeMode(string assemblyName, string typeName, out Assembly assembly, out Type type)
		{
			type = this.ResolveDataContractTypeInSharedTypeMode(assemblyName, typeName, out assembly);
			if (type != null)
			{
				return base.GetDataContract(type);
			}
			return null;
		}

		// Token: 0x06000F71 RID: 3953 RVA: 0x00038AC0 File Offset: 0x00036CC0
		protected override DataContract ResolveDataContractFromTypeName()
		{
			if (this.mode == SerializationMode.SharedContract)
			{
				return base.ResolveDataContractFromTypeName();
			}
			if (this.attributes.ClrAssembly != null && this.attributes.ClrType != null)
			{
				Assembly assembly;
				Type type;
				return this.ResolveDataContractInSharedTypeMode(this.attributes.ClrAssembly, this.attributes.ClrType, out assembly, out type);
			}
			return null;
		}

		// Token: 0x06000F72 RID: 3954 RVA: 0x00038B18 File Offset: 0x00036D18
		[SecuritySafeCritical]
		[PermissionSet(SecurityAction.Demand, Unrestricted = true)]
		[MethodImpl(MethodImplOptions.NoInlining)]
		private bool CheckIfTypeSerializableForSharedTypeMode(Type memberType)
		{
			ISurrogateSelector surrogateSelector;
			return this.surrogateSelector.GetSurrogate(memberType, base.GetStreamingContext(), out surrogateSelector) != null;
		}

		// Token: 0x06000F73 RID: 3955 RVA: 0x00038B3C File Offset: 0x00036D3C
		internal override void CheckIfTypeSerializable(Type memberType, bool isMemberTypeSerializable)
		{
			if (this.mode == SerializationMode.SharedType && this.surrogateSelector != null && this.CheckIfTypeSerializableForSharedTypeMode(memberType))
			{
				return;
			}
			if (this.dataContractSurrogate == null)
			{
				base.CheckIfTypeSerializable(memberType, isMemberTypeSerializable);
				return;
			}
			while (memberType.IsArray)
			{
				memberType = memberType.GetElementType();
			}
			memberType = DataContractSurrogateCaller.GetDataContractType(this.dataContractSurrogate, memberType);
			if (!DataContract.IsTypeSerializable(memberType))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot be serialized. Consider marking it with the DataContractAttribute attribute, and marking all of its members you want serialized with the DataMemberAttribute attribute. Alternatively, you can ensure that the type is public and has a parameterless constructor - all public members of the type will then be serialized, and no attributes will be required.", new object[]
				{
					memberType
				})));
			}
		}

		// Token: 0x06000F74 RID: 3956 RVA: 0x00038BBC File Offset: 0x00036DBC
		internal override Type GetSurrogatedType(Type type)
		{
			if (this.dataContractSurrogate == null)
			{
				return base.GetSurrogatedType(type);
			}
			type = DataContract.UnwrapNullableType(type);
			Type surrogatedType = DataContractSerializer.GetSurrogatedType(this.dataContractSurrogate, type);
			if (this.IsGetOnlyCollection && surrogatedType != type)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Surrogates with get-only collections are not supported. Found on type '{0}'.", new object[]
				{
					DataContract.GetClrTypeFullName(type)
				})));
			}
			return surrogatedType;
		}

		// Token: 0x06000F75 RID: 3957 RVA: 0x00038C24 File Offset: 0x00036E24
		internal override int GetArraySize()
		{
			if (!this.preserveObjectReferences)
			{
				return -1;
			}
			return this.attributes.ArraySZSize;
		}

		// Token: 0x06000F76 RID: 3958 RVA: 0x00038C3B File Offset: 0x00036E3B
		private static Assembly ResolveSimpleAssemblyName(AssemblyName assemblyName)
		{
			return XmlObjectSerializerReadContextComplex.ResolveSimpleAssemblyName(assemblyName.FullName);
		}

		// Token: 0x06000F77 RID: 3959 RVA: 0x00038C48 File Offset: 0x00036E48
		private static Assembly ResolveSimpleAssemblyName(string assemblyName)
		{
			Assembly assembly;
			if (assemblyName == "0")
			{
				assembly = Globals.TypeOfInt.Assembly;
			}
			else
			{
				assembly = Assembly.LoadWithPartialName(assemblyName);
				if (assembly == null)
				{
					assembly = Assembly.LoadWithPartialName(new AssemblyName(assemblyName)
					{
						Version = null
					}.FullName);
				}
			}
			return assembly;
		}

		// Token: 0x06000F78 RID: 3960 RVA: 0x00038C98 File Offset: 0x00036E98
		[SecuritySafeCritical]
		private static void CheckTypeForwardedTo(Assembly sourceAssembly, Assembly destinationAssembly, Type resolvedType)
		{
			if (sourceAssembly != destinationAssembly && !NetDataContractSerializer.UnsafeTypeForwardingEnabled)
			{
				bool isFullyTrusted = sourceAssembly.IsFullyTrusted;
			}
		}

		// Token: 0x06000F79 RID: 3961 RVA: 0x00038CB1 File Offset: 0x00036EB1
		// Note: this type is marked as 'beforefieldinit'.
		static XmlObjectSerializerReadContextComplex()
		{
		}

		// Token: 0x040006D3 RID: 1747
		private static Hashtable dataContractTypeCache = new Hashtable();

		// Token: 0x040006D4 RID: 1748
		private bool preserveObjectReferences;

		// Token: 0x040006D5 RID: 1749
		protected IDataContractSurrogate dataContractSurrogate;

		// Token: 0x040006D6 RID: 1750
		private SerializationMode mode;

		// Token: 0x040006D7 RID: 1751
		private SerializationBinder binder;

		// Token: 0x040006D8 RID: 1752
		private ISurrogateSelector surrogateSelector;

		// Token: 0x040006D9 RID: 1753
		private FormatterAssemblyStyle assemblyFormat;

		// Token: 0x040006DA RID: 1754
		private Hashtable surrogateDataContracts;

		// Token: 0x02000140 RID: 320
		private sealed class TopLevelAssemblyTypeResolver
		{
			// Token: 0x06000F7A RID: 3962 RVA: 0x00038CBD File Offset: 0x00036EBD
			public TopLevelAssemblyTypeResolver(Assembly topLevelAssembly)
			{
				this.topLevelAssembly = topLevelAssembly;
			}

			// Token: 0x06000F7B RID: 3963 RVA: 0x00038CCC File Offset: 0x00036ECC
			public Type ResolveType(Assembly assembly, string simpleTypeName, bool ignoreCase)
			{
				if (assembly == null)
				{
					assembly = this.topLevelAssembly;
				}
				return assembly.GetType(simpleTypeName, false, ignoreCase);
			}

			// Token: 0x040006DB RID: 1755
			private Assembly topLevelAssembly;
		}

		// Token: 0x02000141 RID: 321
		private class XmlObjectDataContractTypeInfo
		{
			// Token: 0x06000F7C RID: 3964 RVA: 0x00038CE8 File Offset: 0x00036EE8
			public XmlObjectDataContractTypeInfo(Assembly assembly, Type type)
			{
				this.assembly = assembly;
				this.type = type;
			}

			// Token: 0x17000379 RID: 889
			// (get) Token: 0x06000F7D RID: 3965 RVA: 0x00038CFE File Offset: 0x00036EFE
			public Assembly Assembly
			{
				get
				{
					return this.assembly;
				}
			}

			// Token: 0x1700037A RID: 890
			// (get) Token: 0x06000F7E RID: 3966 RVA: 0x00038D06 File Offset: 0x00036F06
			public Type Type
			{
				get
				{
					return this.type;
				}
			}

			// Token: 0x040006DC RID: 1756
			private Assembly assembly;

			// Token: 0x040006DD RID: 1757
			private Type type;
		}

		// Token: 0x02000142 RID: 322
		private class XmlObjectDataContractTypeKey
		{
			// Token: 0x06000F7F RID: 3967 RVA: 0x00038D0E File Offset: 0x00036F0E
			public XmlObjectDataContractTypeKey(string assemblyName, string typeName)
			{
				this.assemblyName = assemblyName;
				this.typeName = typeName;
			}

			// Token: 0x06000F80 RID: 3968 RVA: 0x00038D24 File Offset: 0x00036F24
			public override bool Equals(object obj)
			{
				if (this == obj)
				{
					return true;
				}
				XmlObjectSerializerReadContextComplex.XmlObjectDataContractTypeKey xmlObjectDataContractTypeKey = obj as XmlObjectSerializerReadContextComplex.XmlObjectDataContractTypeKey;
				return xmlObjectDataContractTypeKey != null && !(this.assemblyName != xmlObjectDataContractTypeKey.assemblyName) && !(this.typeName != xmlObjectDataContractTypeKey.typeName);
			}

			// Token: 0x06000F81 RID: 3969 RVA: 0x00038D70 File Offset: 0x00036F70
			public override int GetHashCode()
			{
				int num = 0;
				if (this.assemblyName != null)
				{
					num = this.assemblyName.GetHashCode();
				}
				if (this.typeName != null)
				{
					num ^= this.typeName.GetHashCode();
				}
				return num;
			}

			// Token: 0x040006DE RID: 1758
			private string assemblyName;

			// Token: 0x040006DF RID: 1759
			private string typeName;
		}
	}
}
