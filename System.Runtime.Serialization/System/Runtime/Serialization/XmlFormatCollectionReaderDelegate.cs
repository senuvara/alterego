﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x0200012F RID: 303
	// (Invoke) Token: 0x06000EA4 RID: 3748
	internal delegate object XmlFormatCollectionReaderDelegate(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContext context, XmlDictionaryString itemName, XmlDictionaryString itemNamespace, CollectionDataContract collectionContract);
}
