﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000FF RID: 255
	internal class DoubleDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D9E RID: 3486 RVA: 0x00033611 File Offset: 0x00031811
		internal DoubleDataContract() : base(typeof(double), DictionaryGlobals.DoubleLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x06000D9F RID: 3487 RVA: 0x0003362D File Offset: 0x0003182D
		internal override string WriteMethodName
		{
			get
			{
				return "WriteDouble";
			}
		}

		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x06000DA0 RID: 3488 RVA: 0x00033634 File Offset: 0x00031834
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsDouble";
			}
		}

		// Token: 0x06000DA1 RID: 3489 RVA: 0x0003363B File Offset: 0x0003183B
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteDouble((double)obj);
		}

		// Token: 0x06000DA2 RID: 3490 RVA: 0x00033649 File Offset: 0x00031849
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsDouble(), context);
			}
			return reader.ReadElementContentAsDouble();
		}
	}
}
