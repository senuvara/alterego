﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Security;
using System.Threading;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000B1 RID: 177
	internal sealed class CollectionDataContract : DataContract
	{
		// Token: 0x06000969 RID: 2409 RVA: 0x00027853 File Offset: 0x00025A53
		[SecuritySafeCritical]
		internal CollectionDataContract(CollectionKind kind) : base(new CollectionDataContract.CollectionDataContractCriticalHelper(kind))
		{
			this.InitCollectionDataContract(this);
		}

		// Token: 0x0600096A RID: 2410 RVA: 0x00027868 File Offset: 0x00025A68
		[SecuritySafeCritical]
		internal CollectionDataContract(Type type) : base(new CollectionDataContract.CollectionDataContractCriticalHelper(type))
		{
			this.InitCollectionDataContract(this);
		}

		// Token: 0x0600096B RID: 2411 RVA: 0x0002787D File Offset: 0x00025A7D
		[SecuritySafeCritical]
		internal CollectionDataContract(Type type, DataContract itemContract) : base(new CollectionDataContract.CollectionDataContractCriticalHelper(type, itemContract))
		{
			this.InitCollectionDataContract(this);
		}

		// Token: 0x0600096C RID: 2412 RVA: 0x00027893 File Offset: 0x00025A93
		[SecuritySafeCritical]
		private CollectionDataContract(Type type, CollectionKind kind, Type itemType, MethodInfo getEnumeratorMethod, string serializationExceptionMessage, string deserializationExceptionMessage) : base(new CollectionDataContract.CollectionDataContractCriticalHelper(type, kind, itemType, getEnumeratorMethod, serializationExceptionMessage, deserializationExceptionMessage))
		{
			this.InitCollectionDataContract(this.GetSharedTypeContract(type));
		}

		// Token: 0x0600096D RID: 2413 RVA: 0x000278B6 File Offset: 0x00025AB6
		[SecuritySafeCritical]
		private CollectionDataContract(Type type, CollectionKind kind, Type itemType, MethodInfo getEnumeratorMethod, MethodInfo addMethod, ConstructorInfo constructor) : base(new CollectionDataContract.CollectionDataContractCriticalHelper(type, kind, itemType, getEnumeratorMethod, addMethod, constructor))
		{
			this.InitCollectionDataContract(this.GetSharedTypeContract(type));
		}

		// Token: 0x0600096E RID: 2414 RVA: 0x000278D9 File Offset: 0x00025AD9
		[SecuritySafeCritical]
		private CollectionDataContract(Type type, CollectionKind kind, Type itemType, MethodInfo getEnumeratorMethod, MethodInfo addMethod, ConstructorInfo constructor, bool isConstructorCheckRequired) : base(new CollectionDataContract.CollectionDataContractCriticalHelper(type, kind, itemType, getEnumeratorMethod, addMethod, constructor, isConstructorCheckRequired))
		{
			this.InitCollectionDataContract(this.GetSharedTypeContract(type));
		}

		// Token: 0x0600096F RID: 2415 RVA: 0x000278FE File Offset: 0x00025AFE
		[SecuritySafeCritical]
		private CollectionDataContract(Type type, string invalidCollectionInSharedContractMessage) : base(new CollectionDataContract.CollectionDataContractCriticalHelper(type, invalidCollectionInSharedContractMessage))
		{
			this.InitCollectionDataContract(this.GetSharedTypeContract(type));
		}

		// Token: 0x06000970 RID: 2416 RVA: 0x0002791C File Offset: 0x00025B1C
		[SecurityCritical]
		private void InitCollectionDataContract(DataContract sharedTypeContract)
		{
			this.helper = (base.Helper as CollectionDataContract.CollectionDataContractCriticalHelper);
			this.collectionItemName = this.helper.CollectionItemName;
			if (this.helper.Kind == CollectionKind.Dictionary || this.helper.Kind == CollectionKind.GenericDictionary)
			{
				this.itemContract = this.helper.ItemContract;
			}
			this.helper.SharedTypeContract = sharedTypeContract;
		}

		// Token: 0x06000971 RID: 2417 RVA: 0x000020AE File Offset: 0x000002AE
		private void InitSharedTypeContract()
		{
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x06000972 RID: 2418 RVA: 0x00027984 File Offset: 0x00025B84
		private static Type[] KnownInterfaces
		{
			[SecuritySafeCritical]
			get
			{
				return CollectionDataContract.CollectionDataContractCriticalHelper.KnownInterfaces;
			}
		}

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x06000973 RID: 2419 RVA: 0x0002798B File Offset: 0x00025B8B
		internal CollectionKind Kind
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.Kind;
			}
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x06000974 RID: 2420 RVA: 0x00027998 File Offset: 0x00025B98
		internal Type ItemType
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.ItemType;
			}
		}

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x06000975 RID: 2421 RVA: 0x000279A5 File Offset: 0x00025BA5
		// (set) Token: 0x06000976 RID: 2422 RVA: 0x000279BC File Offset: 0x00025BBC
		public DataContract ItemContract
		{
			[SecuritySafeCritical]
			get
			{
				return this.itemContract ?? this.helper.ItemContract;
			}
			[SecurityCritical]
			set
			{
				this.itemContract = value;
				this.helper.ItemContract = value;
			}
		}

		// Token: 0x17000149 RID: 329
		// (get) Token: 0x06000977 RID: 2423 RVA: 0x000279D1 File Offset: 0x00025BD1
		internal DataContract SharedTypeContract
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.SharedTypeContract;
			}
		}

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x06000978 RID: 2424 RVA: 0x000279DE File Offset: 0x00025BDE
		// (set) Token: 0x06000979 RID: 2425 RVA: 0x000279EB File Offset: 0x00025BEB
		internal string ItemName
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.ItemName;
			}
			[SecurityCritical]
			set
			{
				this.helper.ItemName = value;
			}
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x0600097A RID: 2426 RVA: 0x000279F9 File Offset: 0x00025BF9
		public XmlDictionaryString CollectionItemName
		{
			[SecuritySafeCritical]
			get
			{
				return this.collectionItemName;
			}
		}

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x0600097B RID: 2427 RVA: 0x00027A01 File Offset: 0x00025C01
		// (set) Token: 0x0600097C RID: 2428 RVA: 0x00027A0E File Offset: 0x00025C0E
		internal string KeyName
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.KeyName;
			}
			[SecurityCritical]
			set
			{
				this.helper.KeyName = value;
			}
		}

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x0600097D RID: 2429 RVA: 0x00027A1C File Offset: 0x00025C1C
		// (set) Token: 0x0600097E RID: 2430 RVA: 0x00027A29 File Offset: 0x00025C29
		internal string ValueName
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.ValueName;
			}
			[SecurityCritical]
			set
			{
				this.helper.ValueName = value;
			}
		}

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x0600097F RID: 2431 RVA: 0x00027A37 File Offset: 0x00025C37
		internal bool IsDictionary
		{
			get
			{
				return this.KeyName != null;
			}
		}

		// Token: 0x1700014F RID: 335
		// (get) Token: 0x06000980 RID: 2432 RVA: 0x00027A44 File Offset: 0x00025C44
		public XmlDictionaryString ChildElementNamespace
		{
			[SecuritySafeCritical]
			get
			{
				if (this.childElementNamespace == null)
				{
					lock (this)
					{
						if (this.childElementNamespace == null)
						{
							if (this.helper.ChildElementNamespace == null && !this.IsDictionary)
							{
								XmlDictionaryString childNamespaceToDeclare = ClassDataContract.GetChildNamespaceToDeclare(this, this.ItemType, new XmlDictionary());
								Thread.MemoryBarrier();
								this.helper.ChildElementNamespace = childNamespaceToDeclare;
							}
							this.childElementNamespace = this.helper.ChildElementNamespace;
						}
					}
				}
				return this.childElementNamespace;
			}
		}

		// Token: 0x17000150 RID: 336
		// (get) Token: 0x06000981 RID: 2433 RVA: 0x00027AD8 File Offset: 0x00025CD8
		// (set) Token: 0x06000982 RID: 2434 RVA: 0x00027AE5 File Offset: 0x00025CE5
		internal bool IsItemTypeNullable
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsItemTypeNullable;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsItemTypeNullable = value;
			}
		}

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x06000983 RID: 2435 RVA: 0x00027AF3 File Offset: 0x00025CF3
		// (set) Token: 0x06000984 RID: 2436 RVA: 0x00027B00 File Offset: 0x00025D00
		internal bool IsConstructorCheckRequired
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsConstructorCheckRequired;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsConstructorCheckRequired = value;
			}
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x06000985 RID: 2437 RVA: 0x00027B0E File Offset: 0x00025D0E
		internal MethodInfo GetEnumeratorMethod
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.GetEnumeratorMethod;
			}
		}

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x06000986 RID: 2438 RVA: 0x00027B1B File Offset: 0x00025D1B
		internal MethodInfo AddMethod
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.AddMethod;
			}
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x06000987 RID: 2439 RVA: 0x00027B28 File Offset: 0x00025D28
		internal ConstructorInfo Constructor
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.Constructor;
			}
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x06000988 RID: 2440 RVA: 0x00027B35 File Offset: 0x00025D35
		// (set) Token: 0x06000989 RID: 2441 RVA: 0x00027B42 File Offset: 0x00025D42
		internal override Dictionary<XmlQualifiedName, DataContract> KnownDataContracts
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.KnownDataContracts;
			}
			[SecurityCritical]
			set
			{
				this.helper.KnownDataContracts = value;
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x0600098A RID: 2442 RVA: 0x00027B50 File Offset: 0x00025D50
		internal string InvalidCollectionInSharedContractMessage
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.InvalidCollectionInSharedContractMessage;
			}
		}

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x0600098B RID: 2443 RVA: 0x00027B5D File Offset: 0x00025D5D
		internal string SerializationExceptionMessage
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.SerializationExceptionMessage;
			}
		}

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x0600098C RID: 2444 RVA: 0x00027B6A File Offset: 0x00025D6A
		internal string DeserializationExceptionMessage
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.DeserializationExceptionMessage;
			}
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x0600098D RID: 2445 RVA: 0x00027B77 File Offset: 0x00025D77
		internal bool IsReadOnlyContract
		{
			get
			{
				return this.DeserializationExceptionMessage != null;
			}
		}

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x0600098E RID: 2446 RVA: 0x00027B82 File Offset: 0x00025D82
		private bool ItemNameSetExplicit
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.ItemNameSetExplicit;
			}
		}

		// Token: 0x1700015B RID: 347
		// (get) Token: 0x0600098F RID: 2447 RVA: 0x00027B90 File Offset: 0x00025D90
		internal XmlFormatCollectionWriterDelegate XmlFormatWriterDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.XmlFormatWriterDelegate == null)
				{
					lock (this)
					{
						if (this.helper.XmlFormatWriterDelegate == null)
						{
							XmlFormatCollectionWriterDelegate xmlFormatWriterDelegate = new XmlFormatWriterGenerator().GenerateCollectionWriter(this);
							Thread.MemoryBarrier();
							this.helper.XmlFormatWriterDelegate = xmlFormatWriterDelegate;
						}
					}
				}
				return this.helper.XmlFormatWriterDelegate;
			}
		}

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x06000990 RID: 2448 RVA: 0x00027C08 File Offset: 0x00025E08
		internal XmlFormatCollectionReaderDelegate XmlFormatReaderDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.XmlFormatReaderDelegate == null)
				{
					lock (this)
					{
						if (this.helper.XmlFormatReaderDelegate == null)
						{
							if (this.IsReadOnlyContract)
							{
								DataContract.ThrowInvalidDataContractException(this.helper.DeserializationExceptionMessage, null);
							}
							XmlFormatCollectionReaderDelegate xmlFormatReaderDelegate = new XmlFormatReaderGenerator().GenerateCollectionReader(this);
							Thread.MemoryBarrier();
							this.helper.XmlFormatReaderDelegate = xmlFormatReaderDelegate;
						}
					}
				}
				return this.helper.XmlFormatReaderDelegate;
			}
		}

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x06000991 RID: 2449 RVA: 0x00027C98 File Offset: 0x00025E98
		internal XmlFormatGetOnlyCollectionReaderDelegate XmlFormatGetOnlyCollectionReaderDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.XmlFormatGetOnlyCollectionReaderDelegate == null)
				{
					lock (this)
					{
						if (this.helper.XmlFormatGetOnlyCollectionReaderDelegate == null)
						{
							if (base.UnderlyingType.IsInterface && (this.Kind == CollectionKind.Enumerable || this.Kind == CollectionKind.Collection || this.Kind == CollectionKind.GenericEnumerable))
							{
								throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("On type '{0}', get-only collection must have an Add method.", new object[]
								{
									DataContract.GetClrTypeFullName(base.UnderlyingType)
								})));
							}
							if (this.IsReadOnlyContract)
							{
								DataContract.ThrowInvalidDataContractException(this.helper.DeserializationExceptionMessage, null);
							}
							XmlFormatGetOnlyCollectionReaderDelegate xmlFormatGetOnlyCollectionReaderDelegate = new XmlFormatReaderGenerator().GenerateGetOnlyCollectionReader(this);
							Thread.MemoryBarrier();
							this.helper.XmlFormatGetOnlyCollectionReaderDelegate = xmlFormatGetOnlyCollectionReaderDelegate;
						}
					}
				}
				return this.helper.XmlFormatGetOnlyCollectionReaderDelegate;
			}
		}

		// Token: 0x06000992 RID: 2450 RVA: 0x00027D80 File Offset: 0x00025F80
		private DataContract GetSharedTypeContract(Type type)
		{
			if (type.IsDefined(Globals.TypeOfCollectionDataContractAttribute, false))
			{
				return this;
			}
			if (type.IsSerializable || type.IsDefined(Globals.TypeOfDataContractAttribute, false))
			{
				return new ClassDataContract(type);
			}
			return null;
		}

		// Token: 0x06000993 RID: 2451 RVA: 0x00027DB0 File Offset: 0x00025FB0
		internal static bool IsCollectionInterface(Type type)
		{
			if (type.IsGenericType)
			{
				type = type.GetGenericTypeDefinition();
			}
			return ((ICollection<Type>)CollectionDataContract.KnownInterfaces).Contains(type);
		}

		// Token: 0x06000994 RID: 2452 RVA: 0x00027DD0 File Offset: 0x00025FD0
		internal static bool IsCollection(Type type)
		{
			Type type2;
			return CollectionDataContract.IsCollection(type, out type2);
		}

		// Token: 0x06000995 RID: 2453 RVA: 0x00027DE5 File Offset: 0x00025FE5
		internal static bool IsCollection(Type type, out Type itemType)
		{
			return CollectionDataContract.IsCollectionHelper(type, out itemType, true, false);
		}

		// Token: 0x06000996 RID: 2454 RVA: 0x00027DF0 File Offset: 0x00025FF0
		internal static bool IsCollection(Type type, bool constructorRequired, bool skipIfReadOnlyContract)
		{
			Type type2;
			return CollectionDataContract.IsCollectionHelper(type, out type2, constructorRequired, skipIfReadOnlyContract);
		}

		// Token: 0x06000997 RID: 2455 RVA: 0x00027E08 File Offset: 0x00026008
		private static bool IsCollectionHelper(Type type, out Type itemType, bool constructorRequired, bool skipIfReadOnlyContract = false)
		{
			if (type.IsArray && DataContract.GetBuiltInDataContract(type) == null)
			{
				itemType = type.GetElementType();
				return true;
			}
			DataContract dataContract;
			return CollectionDataContract.IsCollectionOrTryCreate(type, false, out dataContract, out itemType, constructorRequired, skipIfReadOnlyContract);
		}

		// Token: 0x06000998 RID: 2456 RVA: 0x00027E3C File Offset: 0x0002603C
		internal static bool TryCreate(Type type, out DataContract dataContract)
		{
			Type type2;
			return CollectionDataContract.IsCollectionOrTryCreate(type, true, out dataContract, out type2, true, false);
		}

		// Token: 0x06000999 RID: 2457 RVA: 0x00027E58 File Offset: 0x00026058
		internal static bool TryCreateGetOnlyCollectionDataContract(Type type, out DataContract dataContract)
		{
			if (type.IsArray)
			{
				dataContract = new CollectionDataContract(type);
				return true;
			}
			Type type2;
			return CollectionDataContract.IsCollectionOrTryCreate(type, true, out dataContract, out type2, false, false);
		}

		// Token: 0x0600099A RID: 2458 RVA: 0x00027E84 File Offset: 0x00026084
		internal static MethodInfo GetTargetMethodWithName(string name, Type type, Type interfaceType)
		{
			InterfaceMapping interfaceMap = type.GetInterfaceMap(interfaceType);
			for (int i = 0; i < interfaceMap.TargetMethods.Length; i++)
			{
				if (interfaceMap.InterfaceMethods[i].Name == name)
				{
					return interfaceMap.InterfaceMethods[i];
				}
			}
			return null;
		}

		// Token: 0x0600099B RID: 2459 RVA: 0x00027ECB File Offset: 0x000260CB
		private static bool IsArraySegment(Type t)
		{
			return t.IsGenericType && t.GetGenericTypeDefinition() == typeof(ArraySegment<>);
		}

		// Token: 0x0600099C RID: 2460 RVA: 0x00027EEC File Offset: 0x000260EC
		private static bool IsCollectionOrTryCreate(Type type, bool tryCreate, out DataContract dataContract, out Type itemType, bool constructorRequired, bool skipIfReadOnlyContract = false)
		{
			dataContract = null;
			itemType = Globals.TypeOfObject;
			if (DataContract.GetBuiltInDataContract(type) != null)
			{
				return CollectionDataContract.HandleIfInvalidCollection(type, tryCreate, false, false, "{0} is a built-in type and cannot be a collection.", null, ref dataContract);
			}
			bool hasCollectionDataContract = CollectionDataContract.IsCollectionDataContract(type);
			bool flag = false;
			string serializationExceptionMessage = null;
			string deserializationExceptionMessage = null;
			Type baseType = type.BaseType;
			bool flag2 = baseType != null && baseType != Globals.TypeOfObject && baseType != Globals.TypeOfValueType && baseType != Globals.TypeOfUri && CollectionDataContract.IsCollection(baseType) && !type.IsSerializable;
			if (type.IsDefined(Globals.TypeOfDataContractAttribute, false))
			{
				return CollectionDataContract.HandleIfInvalidCollection(type, tryCreate, hasCollectionDataContract, flag2, "{0} has DataContractAttribute attribute.", null, ref dataContract);
			}
			if (Globals.TypeOfIXmlSerializable.IsAssignableFrom(type) || CollectionDataContract.IsArraySegment(type))
			{
				return false;
			}
			if (!Globals.TypeOfIEnumerable.IsAssignableFrom(type))
			{
				return CollectionDataContract.HandleIfInvalidCollection(type, tryCreate, hasCollectionDataContract, flag2, "{0} does not implement IEnumerable interface.", null, ref dataContract);
			}
			if (type.IsInterface)
			{
				Type type2 = type.IsGenericType ? type.GetGenericTypeDefinition() : type;
				Type[] knownInterfaces = CollectionDataContract.KnownInterfaces;
				for (int i = 0; i < knownInterfaces.Length; i++)
				{
					if (knownInterfaces[i] == type2)
					{
						MethodInfo methodInfo = null;
						MethodInfo method;
						if (type.IsGenericType)
						{
							Type[] genericArguments = type.GetGenericArguments();
							if (type2 == Globals.TypeOfIDictionaryGeneric)
							{
								itemType = Globals.TypeOfKeyValue.MakeGenericType(genericArguments);
								methodInfo = type.GetMethod("Add");
								method = Globals.TypeOfIEnumerableGeneric.MakeGenericType(new Type[]
								{
									Globals.TypeOfKeyValuePair.MakeGenericType(genericArguments)
								}).GetMethod("GetEnumerator");
							}
							else
							{
								itemType = genericArguments[0];
								if (type2 == Globals.TypeOfICollectionGeneric || type2 == Globals.TypeOfIListGeneric)
								{
									methodInfo = Globals.TypeOfICollectionGeneric.MakeGenericType(new Type[]
									{
										itemType
									}).GetMethod("Add");
								}
								method = Globals.TypeOfIEnumerableGeneric.MakeGenericType(new Type[]
								{
									itemType
								}).GetMethod("GetEnumerator");
							}
						}
						else
						{
							if (type2 == Globals.TypeOfIDictionary)
							{
								itemType = typeof(KeyValue<object, object>);
								methodInfo = type.GetMethod("Add");
							}
							else
							{
								itemType = Globals.TypeOfObject;
								if (type2 == Globals.TypeOfIList)
								{
									methodInfo = Globals.TypeOfIList.GetMethod("Add");
								}
							}
							method = Globals.TypeOfIEnumerable.GetMethod("GetEnumerator");
						}
						if (tryCreate)
						{
							dataContract = new CollectionDataContract(type, (CollectionKind)(i + 1), itemType, method, methodInfo, null);
						}
						return true;
					}
				}
			}
			ConstructorInfo constructorInfo = null;
			if (!type.IsValueType)
			{
				constructorInfo = type.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Globals.EmptyTypeArray, null);
				if (constructorInfo == null && constructorRequired)
				{
					if (type.IsSerializable)
					{
						return CollectionDataContract.HandleIfInvalidCollection(type, tryCreate, hasCollectionDataContract, flag2, "{0} does not have a default constructor.", null, ref dataContract);
					}
					flag = true;
					CollectionDataContract.GetReadOnlyCollectionExceptionMessages(type, hasCollectionDataContract, "{0} does not have a default constructor.", null, out serializationExceptionMessage, out deserializationExceptionMessage);
				}
			}
			Type type3 = null;
			CollectionKind collectionKind = CollectionKind.None;
			bool flag3 = false;
			foreach (Type type4 in type.GetInterfaces())
			{
				Type right = type4.IsGenericType ? type4.GetGenericTypeDefinition() : type4;
				Type[] knownInterfaces2 = CollectionDataContract.KnownInterfaces;
				int k = 0;
				while (k < knownInterfaces2.Length)
				{
					if (knownInterfaces2[k] == right)
					{
						CollectionKind collectionKind2 = (CollectionKind)(k + 1);
						if (collectionKind == CollectionKind.None || collectionKind2 < collectionKind)
						{
							collectionKind = collectionKind2;
							type3 = type4;
							flag3 = false;
							break;
						}
						if ((collectionKind & collectionKind2) == collectionKind2)
						{
							flag3 = true;
							break;
						}
						break;
					}
					else
					{
						k++;
					}
				}
			}
			if (collectionKind == CollectionKind.None)
			{
				return CollectionDataContract.HandleIfInvalidCollection(type, tryCreate, hasCollectionDataContract, flag2, "{0} does not implement IEnumerable interface.", null, ref dataContract);
			}
			if (collectionKind == CollectionKind.Enumerable || collectionKind == CollectionKind.Collection || collectionKind == CollectionKind.GenericEnumerable)
			{
				if (flag3)
				{
					type3 = Globals.TypeOfIEnumerable;
				}
				itemType = (type3.IsGenericType ? type3.GetGenericArguments()[0] : Globals.TypeOfObject);
				MethodInfo methodInfo;
				MethodInfo method;
				CollectionDataContract.GetCollectionMethods(type, type3, new Type[]
				{
					itemType
				}, false, out method, out methodInfo);
				if (methodInfo == null)
				{
					if (type.IsSerializable || skipIfReadOnlyContract)
					{
						return CollectionDataContract.HandleIfInvalidCollection(type, tryCreate, hasCollectionDataContract, flag2 && !skipIfReadOnlyContract, "{0} does not have a valid Add method with parameter of type '{1}'.", DataContract.GetClrTypeFullName(itemType), ref dataContract);
					}
					flag = true;
					CollectionDataContract.GetReadOnlyCollectionExceptionMessages(type, hasCollectionDataContract, "{0} does not have a valid Add method with parameter of type '{1}'.", DataContract.GetClrTypeFullName(itemType), out serializationExceptionMessage, out deserializationExceptionMessage);
				}
				if (tryCreate)
				{
					dataContract = (flag ? new CollectionDataContract(type, collectionKind, itemType, method, serializationExceptionMessage, deserializationExceptionMessage) : new CollectionDataContract(type, collectionKind, itemType, method, methodInfo, constructorInfo, !constructorRequired));
				}
			}
			else
			{
				if (flag3)
				{
					return CollectionDataContract.HandleIfInvalidCollection(type, tryCreate, hasCollectionDataContract, flag2, "{0} has multiple definitions of interface '{1}'.", CollectionDataContract.KnownInterfaces[(int)(collectionKind - CollectionKind.GenericDictionary)].Name, ref dataContract);
				}
				Type[] array = null;
				switch (collectionKind)
				{
				case CollectionKind.GenericDictionary:
					array = type3.GetGenericArguments();
					itemType = ((type3.IsGenericTypeDefinition || (array[0].IsGenericParameter && array[1].IsGenericParameter)) ? Globals.TypeOfKeyValue : Globals.TypeOfKeyValue.MakeGenericType(array));
					break;
				case CollectionKind.Dictionary:
					array = new Type[]
					{
						Globals.TypeOfObject,
						Globals.TypeOfObject
					};
					itemType = Globals.TypeOfKeyValue.MakeGenericType(array);
					break;
				case CollectionKind.GenericList:
				case CollectionKind.GenericCollection:
					array = type3.GetGenericArguments();
					itemType = array[0];
					break;
				case CollectionKind.List:
					itemType = Globals.TypeOfObject;
					array = new Type[]
					{
						itemType
					};
					break;
				}
				if (tryCreate)
				{
					MethodInfo methodInfo;
					MethodInfo method;
					CollectionDataContract.GetCollectionMethods(type, type3, array, true, out method, out methodInfo);
					dataContract = (flag ? new CollectionDataContract(type, collectionKind, itemType, method, serializationExceptionMessage, deserializationExceptionMessage) : new CollectionDataContract(type, collectionKind, itemType, method, methodInfo, constructorInfo, !constructorRequired));
				}
			}
			return !flag || !skipIfReadOnlyContract;
		}

		// Token: 0x0600099D RID: 2461 RVA: 0x0002845F File Offset: 0x0002665F
		internal static bool IsCollectionDataContract(Type type)
		{
			return type.IsDefined(Globals.TypeOfCollectionDataContractAttribute, false);
		}

		// Token: 0x0600099E RID: 2462 RVA: 0x00028470 File Offset: 0x00026670
		private static bool HandleIfInvalidCollection(Type type, bool tryCreate, bool hasCollectionDataContract, bool createContractWithException, string message, string param, ref DataContract dataContract)
		{
			if (hasCollectionDataContract)
			{
				if (tryCreate)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(CollectionDataContract.GetInvalidCollectionMessage(message, SR.GetString("Type '{0}' with CollectionDataContractAttribute attribute is an invalid collection type since it", new object[]
					{
						DataContract.GetClrTypeFullName(type)
					}), param)));
				}
				return true;
			}
			else
			{
				if (createContractWithException)
				{
					if (tryCreate)
					{
						dataContract = new CollectionDataContract(type, CollectionDataContract.GetInvalidCollectionMessage(message, SR.GetString("Type '{0}' is an invalid collection type since it", new object[]
						{
							DataContract.GetClrTypeFullName(type)
						}), param));
					}
					return true;
				}
				return false;
			}
		}

		// Token: 0x0600099F RID: 2463 RVA: 0x000284E8 File Offset: 0x000266E8
		private static void GetReadOnlyCollectionExceptionMessages(Type type, bool hasCollectionDataContract, string message, string param, out string serializationExceptionMessage, out string deserializationExceptionMessage)
		{
			serializationExceptionMessage = CollectionDataContract.GetInvalidCollectionMessage(message, SR.GetString(hasCollectionDataContract ? "Type '{0}' with CollectionDataContractAttribute attribute is an invalid collection type since it" : "Type '{0}' is an invalid collection type since it", new object[]
			{
				DataContract.GetClrTypeFullName(type)
			}), param);
			deserializationExceptionMessage = CollectionDataContract.GetInvalidCollectionMessage(message, SR.GetString("Error on deserializing read-only collection: {0}", new object[]
			{
				DataContract.GetClrTypeFullName(type)
			}), param);
		}

		// Token: 0x060009A0 RID: 2464 RVA: 0x00028545 File Offset: 0x00026745
		private static string GetInvalidCollectionMessage(string message, string nestedMessage, string param)
		{
			if (param != null)
			{
				return SR.GetString(message, new object[]
				{
					nestedMessage,
					param
				});
			}
			return SR.GetString(message, new object[]
			{
				nestedMessage
			});
		}

		// Token: 0x060009A1 RID: 2465 RVA: 0x00028570 File Offset: 0x00026770
		private static void FindCollectionMethodsOnInterface(Type type, Type interfaceType, ref MethodInfo addMethod, ref MethodInfo getEnumeratorMethod)
		{
			InterfaceMapping interfaceMap = type.GetInterfaceMap(interfaceType);
			for (int i = 0; i < interfaceMap.TargetMethods.Length; i++)
			{
				if (interfaceMap.InterfaceMethods[i].Name == "Add")
				{
					addMethod = interfaceMap.InterfaceMethods[i];
				}
				else if (interfaceMap.InterfaceMethods[i].Name == "GetEnumerator")
				{
					getEnumeratorMethod = interfaceMap.InterfaceMethods[i];
				}
			}
		}

		// Token: 0x060009A2 RID: 2466 RVA: 0x000285E0 File Offset: 0x000267E0
		private static void GetCollectionMethods(Type type, Type interfaceType, Type[] addMethodTypeArray, bool addMethodOnInterface, out MethodInfo getEnumeratorMethod, out MethodInfo addMethod)
		{
			MethodInfo methodInfo;
			getEnumeratorMethod = (methodInfo = null);
			addMethod = methodInfo;
			if (addMethodOnInterface)
			{
				addMethod = type.GetMethod("Add", BindingFlags.Instance | BindingFlags.Public, null, addMethodTypeArray, null);
				if (addMethod == null || addMethod.GetParameters()[0].ParameterType != addMethodTypeArray[0])
				{
					CollectionDataContract.FindCollectionMethodsOnInterface(type, interfaceType, ref addMethod, ref getEnumeratorMethod);
					if (addMethod == null)
					{
						foreach (Type type2 in interfaceType.GetInterfaces())
						{
							if (CollectionDataContract.IsKnownInterface(type2))
							{
								CollectionDataContract.FindCollectionMethodsOnInterface(type, type2, ref addMethod, ref getEnumeratorMethod);
								if (addMethod == null)
								{
									break;
								}
							}
						}
					}
				}
			}
			else
			{
				addMethod = type.GetMethod("Add", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, addMethodTypeArray, null);
			}
			if (getEnumeratorMethod == null)
			{
				getEnumeratorMethod = type.GetMethod("GetEnumerator", BindingFlags.Instance | BindingFlags.Public, null, Globals.EmptyTypeArray, null);
				if (getEnumeratorMethod == null || !Globals.TypeOfIEnumerator.IsAssignableFrom(getEnumeratorMethod.ReturnType))
				{
					Type type3 = interfaceType.GetInterface("System.Collections.Generic.IEnumerable*");
					if (type3 == null)
					{
						type3 = Globals.TypeOfIEnumerable;
					}
					getEnumeratorMethod = CollectionDataContract.GetTargetMethodWithName("GetEnumerator", type, type3);
				}
			}
		}

		// Token: 0x060009A3 RID: 2467 RVA: 0x00028708 File Offset: 0x00026908
		private static bool IsKnownInterface(Type type)
		{
			Type left = type.IsGenericType ? type.GetGenericTypeDefinition() : type;
			foreach (Type right in CollectionDataContract.KnownInterfaces)
			{
				if (left == right)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060009A4 RID: 2468 RVA: 0x0002874C File Offset: 0x0002694C
		[SecuritySafeCritical]
		internal override DataContract BindGenericParameters(DataContract[] paramContracts, Dictionary<DataContract, DataContract> boundContracts)
		{
			DataContract result;
			if (boundContracts.TryGetValue(this, out result))
			{
				return result;
			}
			CollectionDataContract collectionDataContract = new CollectionDataContract(this.Kind);
			boundContracts.Add(this, collectionDataContract);
			collectionDataContract.ItemContract = this.ItemContract.BindGenericParameters(paramContracts, boundContracts);
			collectionDataContract.IsItemTypeNullable = !collectionDataContract.ItemContract.IsValueType;
			collectionDataContract.ItemName = (this.ItemNameSetExplicit ? this.ItemName : collectionDataContract.ItemContract.StableName.Name);
			collectionDataContract.KeyName = this.KeyName;
			collectionDataContract.ValueName = this.ValueName;
			collectionDataContract.StableName = DataContract.CreateQualifiedName(DataContract.ExpandGenericParameters(XmlConvert.DecodeName(base.StableName.Name), new GenericNameProvider(DataContract.GetClrTypeFullName(base.UnderlyingType), paramContracts)), CollectionDataContract.IsCollectionDataContract(base.UnderlyingType) ? base.StableName.Namespace : DataContract.GetCollectionNamespace(collectionDataContract.ItemContract.StableName.Namespace));
			return collectionDataContract;
		}

		// Token: 0x060009A5 RID: 2469 RVA: 0x00028840 File Offset: 0x00026A40
		internal override DataContract GetValidContract(SerializationMode mode)
		{
			if (mode == SerializationMode.SharedType)
			{
				if (this.SharedTypeContract == null)
				{
					DataContract.ThrowTypeNotSerializable(base.UnderlyingType);
				}
				return this.SharedTypeContract;
			}
			this.ThrowIfInvalid();
			return this;
		}

		// Token: 0x060009A6 RID: 2470 RVA: 0x00028867 File Offset: 0x00026A67
		private void ThrowIfInvalid()
		{
			if (this.InvalidCollectionInSharedContractMessage != null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(this.InvalidCollectionInSharedContractMessage));
			}
		}

		// Token: 0x060009A7 RID: 2471 RVA: 0x00028882 File Offset: 0x00026A82
		internal override DataContract GetValidContract()
		{
			if (this.IsConstructorCheckRequired)
			{
				this.CheckConstructor();
			}
			return this;
		}

		// Token: 0x060009A8 RID: 2472 RVA: 0x00028893 File Offset: 0x00026A93
		[SecuritySafeCritical]
		private void CheckConstructor()
		{
			if (this.Constructor == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("{0} does not have a default constructor.", new object[]
				{
					DataContract.GetClrTypeFullName(base.UnderlyingType)
				})));
			}
			this.IsConstructorCheckRequired = false;
		}

		// Token: 0x060009A9 RID: 2473 RVA: 0x000288D3 File Offset: 0x00026AD3
		internal override bool IsValidContract(SerializationMode mode)
		{
			if (mode == SerializationMode.SharedType)
			{
				return this.SharedTypeContract != null;
			}
			return this.InvalidCollectionInSharedContractMessage == null;
		}

		// Token: 0x060009AA RID: 2474 RVA: 0x000288EC File Offset: 0x00026AEC
		internal override bool Equals(object other, Dictionary<DataContractPairKey, object> checkedContracts)
		{
			if (base.IsEqualOrChecked(other, checkedContracts))
			{
				return true;
			}
			if (base.Equals(other, checkedContracts))
			{
				CollectionDataContract collectionDataContract = other as CollectionDataContract;
				if (collectionDataContract != null)
				{
					bool flag = this.ItemContract != null && !this.ItemContract.IsValueType;
					bool flag2 = collectionDataContract.ItemContract != null && !collectionDataContract.ItemContract.IsValueType;
					return this.ItemName == collectionDataContract.ItemName && (this.IsItemTypeNullable || flag) == (collectionDataContract.IsItemTypeNullable || flag2) && this.ItemContract.Equals(collectionDataContract.ItemContract, checkedContracts);
				}
			}
			return false;
		}

		// Token: 0x060009AB RID: 2475 RVA: 0x0002625C File Offset: 0x0002445C
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x060009AC RID: 2476 RVA: 0x00028988 File Offset: 0x00026B88
		public override void WriteXmlValue(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContext context)
		{
			context.IsGetOnlyCollection = false;
			this.XmlFormatWriterDelegate(xmlWriter, obj, context, this);
		}

		// Token: 0x060009AD RID: 2477 RVA: 0x000289A0 File Offset: 0x00026BA0
		public override object ReadXmlValue(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContext context)
		{
			xmlReader.Read();
			object result = null;
			if (context.IsGetOnlyCollection)
			{
				context.IsGetOnlyCollection = false;
				this.XmlFormatGetOnlyCollectionReaderDelegate(xmlReader, context, this.CollectionItemName, this.Namespace, this);
			}
			else
			{
				result = this.XmlFormatReaderDelegate(xmlReader, context, this.CollectionItemName, this.Namespace, this);
			}
			xmlReader.ReadEndElement();
			return result;
		}

		// Token: 0x04000427 RID: 1063
		[SecurityCritical]
		private XmlDictionaryString collectionItemName;

		// Token: 0x04000428 RID: 1064
		[SecurityCritical]
		private XmlDictionaryString childElementNamespace;

		// Token: 0x04000429 RID: 1065
		[SecurityCritical]
		private DataContract itemContract;

		// Token: 0x0400042A RID: 1066
		[SecurityCritical]
		private CollectionDataContract.CollectionDataContractCriticalHelper helper;

		// Token: 0x020000B2 RID: 178
		private class CollectionDataContractCriticalHelper : DataContract.DataContractCriticalHelper
		{
			// Token: 0x1700015E RID: 350
			// (get) Token: 0x060009AE RID: 2478 RVA: 0x00028A04 File Offset: 0x00026C04
			internal static Type[] KnownInterfaces
			{
				get
				{
					if (CollectionDataContract.CollectionDataContractCriticalHelper._knownInterfaces == null)
					{
						CollectionDataContract.CollectionDataContractCriticalHelper._knownInterfaces = new Type[]
						{
							Globals.TypeOfIDictionaryGeneric,
							Globals.TypeOfIDictionary,
							Globals.TypeOfIListGeneric,
							Globals.TypeOfICollectionGeneric,
							Globals.TypeOfIList,
							Globals.TypeOfIEnumerableGeneric,
							Globals.TypeOfICollection,
							Globals.TypeOfIEnumerable
						};
					}
					return CollectionDataContract.CollectionDataContractCriticalHelper._knownInterfaces;
				}
			}

			// Token: 0x060009AF RID: 2479 RVA: 0x00028A68 File Offset: 0x00026C68
			private void Init(CollectionKind kind, Type itemType, CollectionDataContractAttribute collectionContractAttribute)
			{
				this.kind = kind;
				if (itemType != null)
				{
					this.itemType = itemType;
					this.isItemTypeNullable = DataContract.IsTypeNullable(itemType);
					bool flag = kind == CollectionKind.Dictionary || kind == CollectionKind.GenericDictionary;
					string text = null;
					string text2 = null;
					string text3 = null;
					if (collectionContractAttribute != null)
					{
						if (collectionContractAttribute.IsItemNameSetExplicitly)
						{
							if (collectionContractAttribute.ItemName == null || collectionContractAttribute.ItemName.Length == 0)
							{
								throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot have CollectionDataContractAttribute attribute ItemName set to null or empty string.", new object[]
								{
									DataContract.GetClrTypeFullName(base.UnderlyingType)
								})));
							}
							text = DataContract.EncodeLocalName(collectionContractAttribute.ItemName);
							this.itemNameSetExplicit = true;
						}
						if (collectionContractAttribute.IsKeyNameSetExplicitly)
						{
							if (collectionContractAttribute.KeyName == null || collectionContractAttribute.KeyName.Length == 0)
							{
								throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot have CollectionDataContractAttribute attribute KeyName set to null or empty string.", new object[]
								{
									DataContract.GetClrTypeFullName(base.UnderlyingType)
								})));
							}
							if (!flag)
							{
								throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("The collection data contract type '{0}' specifies '{1}' for the KeyName property. This is not allowed since the type is not IDictionary. Remove the setting for the KeyName property.", new object[]
								{
									DataContract.GetClrTypeFullName(base.UnderlyingType),
									collectionContractAttribute.KeyName
								})));
							}
							text2 = DataContract.EncodeLocalName(collectionContractAttribute.KeyName);
						}
						if (collectionContractAttribute.IsValueNameSetExplicitly)
						{
							if (collectionContractAttribute.ValueName == null || collectionContractAttribute.ValueName.Length == 0)
							{
								throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot have CollectionDataContractAttribute attribute ValueName set to null or empty string.", new object[]
								{
									DataContract.GetClrTypeFullName(base.UnderlyingType)
								})));
							}
							if (!flag)
							{
								throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("The collection data contract type '{0}' specifies '{1}' for the ValueName property. This is not allowed since the type is not IDictionary. Remove the setting for the ValueName property.", new object[]
								{
									DataContract.GetClrTypeFullName(base.UnderlyingType),
									collectionContractAttribute.ValueName
								})));
							}
							text3 = DataContract.EncodeLocalName(collectionContractAttribute.ValueName);
						}
					}
					XmlDictionary xmlDictionary = flag ? new XmlDictionary(5) : new XmlDictionary(3);
					base.Name = xmlDictionary.Add(base.StableName.Name);
					base.Namespace = xmlDictionary.Add(base.StableName.Namespace);
					this.itemName = (text ?? DataContract.GetStableName(DataContract.UnwrapNullableType(itemType)).Name);
					this.collectionItemName = xmlDictionary.Add(this.itemName);
					if (flag)
					{
						this.keyName = (text2 ?? "Key");
						this.valueName = (text3 ?? "Value");
					}
				}
				if (collectionContractAttribute != null)
				{
					base.IsReference = collectionContractAttribute.IsReference;
				}
			}

			// Token: 0x060009B0 RID: 2480 RVA: 0x00028CBD File Offset: 0x00026EBD
			internal CollectionDataContractCriticalHelper(CollectionKind kind)
			{
				this.Init(kind, null, null);
			}

			// Token: 0x060009B1 RID: 2481 RVA: 0x00028CD0 File Offset: 0x00026ED0
			internal CollectionDataContractCriticalHelper(Type type) : base(type)
			{
				if (type == Globals.TypeOfArray)
				{
					type = Globals.TypeOfObjectArray;
				}
				if (type.GetArrayRank() > 1)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("Multi-dimensional arrays are not supported.")));
				}
				base.StableName = DataContract.GetStableName(type);
				this.Init(CollectionKind.Array, type.GetElementType(), null);
			}

			// Token: 0x060009B2 RID: 2482 RVA: 0x00028D34 File Offset: 0x00026F34
			internal CollectionDataContractCriticalHelper(Type type, DataContract itemContract) : base(type)
			{
				if (type.GetArrayRank() > 1)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("Multi-dimensional arrays are not supported.")));
				}
				base.StableName = DataContract.CreateQualifiedName("ArrayOf" + itemContract.StableName.Name, itemContract.StableName.Namespace);
				this.itemContract = itemContract;
				this.Init(CollectionKind.Array, type.GetElementType(), null);
			}

			// Token: 0x060009B3 RID: 2483 RVA: 0x00028DA8 File Offset: 0x00026FA8
			internal CollectionDataContractCriticalHelper(Type type, CollectionKind kind, Type itemType, MethodInfo getEnumeratorMethod, string serializationExceptionMessage, string deserializationExceptionMessage) : base(type)
			{
				if (getEnumeratorMethod == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Collection type '{0}' does not have a valid GetEnumerator method.", new object[]
					{
						DataContract.GetClrTypeFullName(type)
					})));
				}
				if (itemType == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Collection type '{0}' must have a non-null item type.", new object[]
					{
						DataContract.GetClrTypeFullName(type)
					})));
				}
				CollectionDataContractAttribute collectionContractAttribute;
				base.StableName = DataContract.GetCollectionStableName(type, itemType, out collectionContractAttribute);
				this.Init(kind, itemType, collectionContractAttribute);
				this.getEnumeratorMethod = getEnumeratorMethod;
				this.serializationExceptionMessage = serializationExceptionMessage;
				this.deserializationExceptionMessage = deserializationExceptionMessage;
			}

			// Token: 0x060009B4 RID: 2484 RVA: 0x00028E48 File Offset: 0x00027048
			internal CollectionDataContractCriticalHelper(Type type, CollectionKind kind, Type itemType, MethodInfo getEnumeratorMethod, MethodInfo addMethod, ConstructorInfo constructor) : this(type, kind, itemType, getEnumeratorMethod, null, null)
			{
				if (addMethod == null && !type.IsInterface)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Collection type '{0}' does not have a valid Add method.", new object[]
					{
						DataContract.GetClrTypeFullName(type)
					})));
				}
				this.addMethod = addMethod;
				this.constructor = constructor;
			}

			// Token: 0x060009B5 RID: 2485 RVA: 0x00028EA8 File Offset: 0x000270A8
			internal CollectionDataContractCriticalHelper(Type type, CollectionKind kind, Type itemType, MethodInfo getEnumeratorMethod, MethodInfo addMethod, ConstructorInfo constructor, bool isConstructorCheckRequired) : this(type, kind, itemType, getEnumeratorMethod, addMethod, constructor)
			{
				this.isConstructorCheckRequired = isConstructorCheckRequired;
			}

			// Token: 0x060009B6 RID: 2486 RVA: 0x00028EC1 File Offset: 0x000270C1
			internal CollectionDataContractCriticalHelper(Type type, string invalidCollectionInSharedContractMessage) : base(type)
			{
				this.Init(CollectionKind.Collection, null, null);
				this.invalidCollectionInSharedContractMessage = invalidCollectionInSharedContractMessage;
			}

			// Token: 0x1700015F RID: 351
			// (get) Token: 0x060009B7 RID: 2487 RVA: 0x00028EDA File Offset: 0x000270DA
			internal CollectionKind Kind
			{
				get
				{
					return this.kind;
				}
			}

			// Token: 0x17000160 RID: 352
			// (get) Token: 0x060009B8 RID: 2488 RVA: 0x00028EE2 File Offset: 0x000270E2
			internal Type ItemType
			{
				get
				{
					return this.itemType;
				}
			}

			// Token: 0x17000161 RID: 353
			// (get) Token: 0x060009B9 RID: 2489 RVA: 0x00028EEC File Offset: 0x000270EC
			// (set) Token: 0x060009BA RID: 2490 RVA: 0x00028FB9 File Offset: 0x000271B9
			internal DataContract ItemContract
			{
				get
				{
					if (this.itemContract == null && base.UnderlyingType != null)
					{
						if (this.IsDictionary)
						{
							if (string.CompareOrdinal(this.KeyName, this.ValueName) == 0)
							{
								DataContract.ThrowInvalidDataContractException(SR.GetString("The collection data contract type '{0}' specifies the same value '{1}' for both the KeyName and the ValueName properties. This is not allowed. Consider changing either the KeyName or the ValueName property.", new object[]
								{
									DataContract.GetClrTypeFullName(base.UnderlyingType),
									this.KeyName
								}), base.UnderlyingType);
							}
							this.itemContract = ClassDataContract.CreateClassDataContractForKeyValue(this.ItemType, base.Namespace, new string[]
							{
								this.KeyName,
								this.ValueName
							});
							DataContract.GetDataContract(this.ItemType);
						}
						else
						{
							this.itemContract = DataContract.GetDataContract(this.ItemType);
						}
					}
					return this.itemContract;
				}
				set
				{
					this.itemContract = value;
				}
			}

			// Token: 0x17000162 RID: 354
			// (get) Token: 0x060009BB RID: 2491 RVA: 0x00028FC2 File Offset: 0x000271C2
			// (set) Token: 0x060009BC RID: 2492 RVA: 0x00028FCA File Offset: 0x000271CA
			internal DataContract SharedTypeContract
			{
				get
				{
					return this.sharedTypeContract;
				}
				set
				{
					this.sharedTypeContract = value;
				}
			}

			// Token: 0x17000163 RID: 355
			// (get) Token: 0x060009BD RID: 2493 RVA: 0x00028FD3 File Offset: 0x000271D3
			// (set) Token: 0x060009BE RID: 2494 RVA: 0x00028FDB File Offset: 0x000271DB
			internal string ItemName
			{
				get
				{
					return this.itemName;
				}
				set
				{
					this.itemName = value;
				}
			}

			// Token: 0x17000164 RID: 356
			// (get) Token: 0x060009BF RID: 2495 RVA: 0x00028FE4 File Offset: 0x000271E4
			// (set) Token: 0x060009C0 RID: 2496 RVA: 0x00028FEC File Offset: 0x000271EC
			internal bool IsConstructorCheckRequired
			{
				get
				{
					return this.isConstructorCheckRequired;
				}
				set
				{
					this.isConstructorCheckRequired = value;
				}
			}

			// Token: 0x17000165 RID: 357
			// (get) Token: 0x060009C1 RID: 2497 RVA: 0x00028FF5 File Offset: 0x000271F5
			public XmlDictionaryString CollectionItemName
			{
				get
				{
					return this.collectionItemName;
				}
			}

			// Token: 0x17000166 RID: 358
			// (get) Token: 0x060009C2 RID: 2498 RVA: 0x00028FFD File Offset: 0x000271FD
			// (set) Token: 0x060009C3 RID: 2499 RVA: 0x00029005 File Offset: 0x00027205
			internal string KeyName
			{
				get
				{
					return this.keyName;
				}
				set
				{
					this.keyName = value;
				}
			}

			// Token: 0x17000167 RID: 359
			// (get) Token: 0x060009C4 RID: 2500 RVA: 0x0002900E File Offset: 0x0002720E
			// (set) Token: 0x060009C5 RID: 2501 RVA: 0x00029016 File Offset: 0x00027216
			internal string ValueName
			{
				get
				{
					return this.valueName;
				}
				set
				{
					this.valueName = value;
				}
			}

			// Token: 0x17000168 RID: 360
			// (get) Token: 0x060009C6 RID: 2502 RVA: 0x0002901F File Offset: 0x0002721F
			internal bool IsDictionary
			{
				get
				{
					return this.KeyName != null;
				}
			}

			// Token: 0x17000169 RID: 361
			// (get) Token: 0x060009C7 RID: 2503 RVA: 0x0002902A File Offset: 0x0002722A
			public string SerializationExceptionMessage
			{
				get
				{
					return this.serializationExceptionMessage;
				}
			}

			// Token: 0x1700016A RID: 362
			// (get) Token: 0x060009C8 RID: 2504 RVA: 0x00029032 File Offset: 0x00027232
			public string DeserializationExceptionMessage
			{
				get
				{
					return this.deserializationExceptionMessage;
				}
			}

			// Token: 0x1700016B RID: 363
			// (get) Token: 0x060009C9 RID: 2505 RVA: 0x0002903A File Offset: 0x0002723A
			// (set) Token: 0x060009CA RID: 2506 RVA: 0x00029042 File Offset: 0x00027242
			public XmlDictionaryString ChildElementNamespace
			{
				get
				{
					return this.childElementNamespace;
				}
				set
				{
					this.childElementNamespace = value;
				}
			}

			// Token: 0x1700016C RID: 364
			// (get) Token: 0x060009CB RID: 2507 RVA: 0x0002904B File Offset: 0x0002724B
			// (set) Token: 0x060009CC RID: 2508 RVA: 0x00029053 File Offset: 0x00027253
			internal bool IsItemTypeNullable
			{
				get
				{
					return this.isItemTypeNullable;
				}
				set
				{
					this.isItemTypeNullable = value;
				}
			}

			// Token: 0x1700016D RID: 365
			// (get) Token: 0x060009CD RID: 2509 RVA: 0x0002905C File Offset: 0x0002725C
			internal MethodInfo GetEnumeratorMethod
			{
				get
				{
					return this.getEnumeratorMethod;
				}
			}

			// Token: 0x1700016E RID: 366
			// (get) Token: 0x060009CE RID: 2510 RVA: 0x00029064 File Offset: 0x00027264
			internal MethodInfo AddMethod
			{
				get
				{
					return this.addMethod;
				}
			}

			// Token: 0x1700016F RID: 367
			// (get) Token: 0x060009CF RID: 2511 RVA: 0x0002906C File Offset: 0x0002726C
			internal ConstructorInfo Constructor
			{
				get
				{
					return this.constructor;
				}
			}

			// Token: 0x17000170 RID: 368
			// (get) Token: 0x060009D0 RID: 2512 RVA: 0x00029074 File Offset: 0x00027274
			// (set) Token: 0x060009D1 RID: 2513 RVA: 0x000290EC File Offset: 0x000272EC
			internal override Dictionary<XmlQualifiedName, DataContract> KnownDataContracts
			{
				get
				{
					if (!this.isKnownTypeAttributeChecked && base.UnderlyingType != null)
					{
						lock (this)
						{
							if (!this.isKnownTypeAttributeChecked)
							{
								this.knownDataContracts = DataContract.ImportKnownTypeAttributes(base.UnderlyingType);
								Thread.MemoryBarrier();
								this.isKnownTypeAttributeChecked = true;
							}
						}
					}
					return this.knownDataContracts;
				}
				set
				{
					this.knownDataContracts = value;
				}
			}

			// Token: 0x17000171 RID: 369
			// (get) Token: 0x060009D2 RID: 2514 RVA: 0x000290F5 File Offset: 0x000272F5
			internal string InvalidCollectionInSharedContractMessage
			{
				get
				{
					return this.invalidCollectionInSharedContractMessage;
				}
			}

			// Token: 0x17000172 RID: 370
			// (get) Token: 0x060009D3 RID: 2515 RVA: 0x000290FD File Offset: 0x000272FD
			internal bool ItemNameSetExplicit
			{
				get
				{
					return this.itemNameSetExplicit;
				}
			}

			// Token: 0x17000173 RID: 371
			// (get) Token: 0x060009D4 RID: 2516 RVA: 0x00029105 File Offset: 0x00027305
			// (set) Token: 0x060009D5 RID: 2517 RVA: 0x0002910D File Offset: 0x0002730D
			internal XmlFormatCollectionWriterDelegate XmlFormatWriterDelegate
			{
				get
				{
					return this.xmlFormatWriterDelegate;
				}
				set
				{
					this.xmlFormatWriterDelegate = value;
				}
			}

			// Token: 0x17000174 RID: 372
			// (get) Token: 0x060009D6 RID: 2518 RVA: 0x00029116 File Offset: 0x00027316
			// (set) Token: 0x060009D7 RID: 2519 RVA: 0x0002911E File Offset: 0x0002731E
			internal XmlFormatCollectionReaderDelegate XmlFormatReaderDelegate
			{
				get
				{
					return this.xmlFormatReaderDelegate;
				}
				set
				{
					this.xmlFormatReaderDelegate = value;
				}
			}

			// Token: 0x17000175 RID: 373
			// (get) Token: 0x060009D8 RID: 2520 RVA: 0x00029127 File Offset: 0x00027327
			// (set) Token: 0x060009D9 RID: 2521 RVA: 0x0002912F File Offset: 0x0002732F
			internal XmlFormatGetOnlyCollectionReaderDelegate XmlFormatGetOnlyCollectionReaderDelegate
			{
				get
				{
					return this.xmlFormatGetOnlyCollectionReaderDelegate;
				}
				set
				{
					this.xmlFormatGetOnlyCollectionReaderDelegate = value;
				}
			}

			// Token: 0x0400042B RID: 1067
			private static Type[] _knownInterfaces;

			// Token: 0x0400042C RID: 1068
			private Type itemType;

			// Token: 0x0400042D RID: 1069
			private bool isItemTypeNullable;

			// Token: 0x0400042E RID: 1070
			private CollectionKind kind;

			// Token: 0x0400042F RID: 1071
			private readonly MethodInfo getEnumeratorMethod;

			// Token: 0x04000430 RID: 1072
			private readonly MethodInfo addMethod;

			// Token: 0x04000431 RID: 1073
			private readonly ConstructorInfo constructor;

			// Token: 0x04000432 RID: 1074
			private readonly string serializationExceptionMessage;

			// Token: 0x04000433 RID: 1075
			private readonly string deserializationExceptionMessage;

			// Token: 0x04000434 RID: 1076
			private DataContract itemContract;

			// Token: 0x04000435 RID: 1077
			private DataContract sharedTypeContract;

			// Token: 0x04000436 RID: 1078
			private Dictionary<XmlQualifiedName, DataContract> knownDataContracts;

			// Token: 0x04000437 RID: 1079
			private bool isKnownTypeAttributeChecked;

			// Token: 0x04000438 RID: 1080
			private string itemName;

			// Token: 0x04000439 RID: 1081
			private bool itemNameSetExplicit;

			// Token: 0x0400043A RID: 1082
			private XmlDictionaryString collectionItemName;

			// Token: 0x0400043B RID: 1083
			private string keyName;

			// Token: 0x0400043C RID: 1084
			private string valueName;

			// Token: 0x0400043D RID: 1085
			private XmlDictionaryString childElementNamespace;

			// Token: 0x0400043E RID: 1086
			private string invalidCollectionInSharedContractMessage;

			// Token: 0x0400043F RID: 1087
			private XmlFormatCollectionReaderDelegate xmlFormatReaderDelegate;

			// Token: 0x04000440 RID: 1088
			private XmlFormatGetOnlyCollectionReaderDelegate xmlFormatGetOnlyCollectionReaderDelegate;

			// Token: 0x04000441 RID: 1089
			private XmlFormatCollectionWriterDelegate xmlFormatWriterDelegate;

			// Token: 0x04000442 RID: 1090
			private bool isConstructorCheckRequired;
		}

		// Token: 0x020000B3 RID: 179
		public class DictionaryEnumerator : IEnumerator<KeyValue<object, object>>, IDisposable, IEnumerator
		{
			// Token: 0x060009DA RID: 2522 RVA: 0x00029138 File Offset: 0x00027338
			public DictionaryEnumerator(IDictionaryEnumerator enumerator)
			{
				this.enumerator = enumerator;
			}

			// Token: 0x060009DB RID: 2523 RVA: 0x000020AE File Offset: 0x000002AE
			public void Dispose()
			{
			}

			// Token: 0x060009DC RID: 2524 RVA: 0x00029147 File Offset: 0x00027347
			public bool MoveNext()
			{
				return this.enumerator.MoveNext();
			}

			// Token: 0x17000176 RID: 374
			// (get) Token: 0x060009DD RID: 2525 RVA: 0x00029154 File Offset: 0x00027354
			public KeyValue<object, object> Current
			{
				get
				{
					return new KeyValue<object, object>(this.enumerator.Key, this.enumerator.Value);
				}
			}

			// Token: 0x17000177 RID: 375
			// (get) Token: 0x060009DE RID: 2526 RVA: 0x00029171 File Offset: 0x00027371
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x060009DF RID: 2527 RVA: 0x0002917E File Offset: 0x0002737E
			public void Reset()
			{
				this.enumerator.Reset();
			}

			// Token: 0x04000443 RID: 1091
			private IDictionaryEnumerator enumerator;
		}

		// Token: 0x020000B4 RID: 180
		public class GenericDictionaryEnumerator<K, V> : IEnumerator<KeyValue<K, V>>, IDisposable, IEnumerator
		{
			// Token: 0x060009E0 RID: 2528 RVA: 0x0002918B File Offset: 0x0002738B
			public GenericDictionaryEnumerator(IEnumerator<KeyValuePair<K, V>> enumerator)
			{
				this.enumerator = enumerator;
			}

			// Token: 0x060009E1 RID: 2529 RVA: 0x000020AE File Offset: 0x000002AE
			public void Dispose()
			{
			}

			// Token: 0x060009E2 RID: 2530 RVA: 0x0002919A File Offset: 0x0002739A
			public bool MoveNext()
			{
				return this.enumerator.MoveNext();
			}

			// Token: 0x17000178 RID: 376
			// (get) Token: 0x060009E3 RID: 2531 RVA: 0x000291A8 File Offset: 0x000273A8
			public KeyValue<K, V> Current
			{
				get
				{
					KeyValuePair<K, V> keyValuePair = this.enumerator.Current;
					return new KeyValue<K, V>(keyValuePair.Key, keyValuePair.Value);
				}
			}

			// Token: 0x17000179 RID: 377
			// (get) Token: 0x060009E4 RID: 2532 RVA: 0x000291D4 File Offset: 0x000273D4
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x060009E5 RID: 2533 RVA: 0x000291E1 File Offset: 0x000273E1
			public void Reset()
			{
				this.enumerator.Reset();
			}

			// Token: 0x04000444 RID: 1092
			private IEnumerator<KeyValuePair<K, V>> enumerator;
		}
	}
}
