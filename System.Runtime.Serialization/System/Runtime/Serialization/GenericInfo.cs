﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000BB RID: 187
	internal class GenericInfo : IGenericNameProvider
	{
		// Token: 0x06000AB5 RID: 2741 RVA: 0x0002C41A File Offset: 0x0002A61A
		internal GenericInfo(XmlQualifiedName stableName, string genericTypeName)
		{
			this.stableName = stableName;
			this.genericTypeName = genericTypeName;
			this.nestedParamCounts = new List<int>();
			this.nestedParamCounts.Add(0);
		}

		// Token: 0x06000AB6 RID: 2742 RVA: 0x0002C447 File Offset: 0x0002A647
		internal void Add(GenericInfo actualParamInfo)
		{
			if (this.paramGenericInfos == null)
			{
				this.paramGenericInfos = new List<GenericInfo>();
			}
			this.paramGenericInfos.Add(actualParamInfo);
		}

		// Token: 0x06000AB7 RID: 2743 RVA: 0x0002C468 File Offset: 0x0002A668
		internal void AddToLevel(int level, int count)
		{
			if (level >= this.nestedParamCounts.Count)
			{
				do
				{
					this.nestedParamCounts.Add((level == this.nestedParamCounts.Count) ? count : 0);
				}
				while (level >= this.nestedParamCounts.Count);
				return;
			}
			this.nestedParamCounts[level] = this.nestedParamCounts[level] + count;
		}

		// Token: 0x06000AB8 RID: 2744 RVA: 0x0002C4C9 File Offset: 0x0002A6C9
		internal XmlQualifiedName GetExpandedStableName()
		{
			if (this.paramGenericInfos == null)
			{
				return this.stableName;
			}
			return new XmlQualifiedName(DataContract.EncodeLocalName(DataContract.ExpandGenericParameters(XmlConvert.DecodeName(this.stableName.Name), this)), this.stableName.Namespace);
		}

		// Token: 0x06000AB9 RID: 2745 RVA: 0x0002C505 File Offset: 0x0002A705
		internal string GetStableNamespace()
		{
			return this.stableName.Namespace;
		}

		// Token: 0x170001AF RID: 431
		// (get) Token: 0x06000ABA RID: 2746 RVA: 0x0002C512 File Offset: 0x0002A712
		internal XmlQualifiedName StableName
		{
			get
			{
				return this.stableName;
			}
		}

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x06000ABB RID: 2747 RVA: 0x0002C51A File Offset: 0x0002A71A
		internal IList<GenericInfo> Parameters
		{
			get
			{
				return this.paramGenericInfos;
			}
		}

		// Token: 0x06000ABC RID: 2748 RVA: 0x0002C522 File Offset: 0x0002A722
		public int GetParameterCount()
		{
			return this.paramGenericInfos.Count;
		}

		// Token: 0x06000ABD RID: 2749 RVA: 0x0002C52F File Offset: 0x0002A72F
		public IList<int> GetNestedParameterCounts()
		{
			return this.nestedParamCounts;
		}

		// Token: 0x06000ABE RID: 2750 RVA: 0x0002C537 File Offset: 0x0002A737
		public string GetParameterName(int paramIndex)
		{
			return this.paramGenericInfos[paramIndex].GetExpandedStableName().Name;
		}

		// Token: 0x06000ABF RID: 2751 RVA: 0x0002C550 File Offset: 0x0002A750
		public string GetNamespaces()
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < this.paramGenericInfos.Count; i++)
			{
				stringBuilder.Append(" ").Append(this.paramGenericInfos[i].GetStableNamespace());
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000AC0 RID: 2752 RVA: 0x0002C5A1 File Offset: 0x0002A7A1
		public string GetGenericTypeName()
		{
			return this.genericTypeName;
		}

		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x06000AC1 RID: 2753 RVA: 0x0002C5AC File Offset: 0x0002A7AC
		public bool ParametersFromBuiltInNamespaces
		{
			get
			{
				bool flag = true;
				int num = 0;
				while (num < this.paramGenericInfos.Count && flag)
				{
					flag = DataContract.IsBuiltInNamespace(this.paramGenericInfos[num].GetStableNamespace());
					num++;
				}
				return flag;
			}
		}

		// Token: 0x04000473 RID: 1139
		private string genericTypeName;

		// Token: 0x04000474 RID: 1140
		private XmlQualifiedName stableName;

		// Token: 0x04000475 RID: 1141
		private List<GenericInfo> paramGenericInfos;

		// Token: 0x04000476 RID: 1142
		private List<int> nestedParamCounts;
	}
}
