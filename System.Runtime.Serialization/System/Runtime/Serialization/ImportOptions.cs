﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using Unity;

namespace System.Runtime.Serialization
{
	/// <summary>Represents the options that can be set on an <see cref="T:System.Runtime.Serialization.XsdDataContractImporter" />. </summary>
	// Token: 0x020001A5 RID: 421
	public class ImportOptions
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.ImportOptions" /> class.</summary>
		// Token: 0x0600140F RID: 5135 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public ImportOptions()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> instance that provides the means to check whether particular options for a target language are supported.</summary>
		/// <returns>A <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> that provides the means to check whether particular options for a target language are supported.</returns>
		// Token: 0x17000418 RID: 1048
		// (get) Token: 0x06001410 RID: 5136 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		// (set) Token: 0x06001411 RID: 5137 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public CodeDomProvider CodeProvider
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a data contract surrogate that can be used to modify the code generated during an import operation. </summary>
		/// <returns>An implementation of the <see cref="T:System.Runtime.Serialization.IDataContractSurrogate" /> interface that handles schema import. </returns>
		// Token: 0x17000419 RID: 1049
		// (get) Token: 0x06001412 RID: 5138 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		// (set) Token: 0x06001413 RID: 5139 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public IDataContractSurrogate DataContractSurrogate
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that specifies whether types in generated code should implement the <see cref="T:System.ComponentModel.INotifyPropertyChanged" /> interface.</summary>
		/// <returns>
		///     <see langword="true" /> if the generated code should implement the <see cref="T:System.ComponentModel.INotifyPropertyChanged" /> interface; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700041A RID: 1050
		// (get) Token: 0x06001414 RID: 5140 RVA: 0x0004ACB8 File Offset: 0x00048EB8
		// (set) Token: 0x06001415 RID: 5141 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public bool EnableDataBinding
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that specifies whether generated code will be marked internal or public.</summary>
		/// <returns>
		///     <see langword="true" /> if the code will be marked <see langword="internal" />; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700041B RID: 1051
		// (get) Token: 0x06001416 RID: 5142 RVA: 0x0004ACD4 File Offset: 0x00048ED4
		// (set) Token: 0x06001417 RID: 5143 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public bool GenerateInternal
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that specifies whether generated data contract classes will be marked with the <see cref="T:System.SerializableAttribute" /> attribute in addition to the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute.</summary>
		/// <returns>
		///     <see langword="true" /> to generate classes with the <see cref="T:System.SerializableAttribute" /> applied; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700041C RID: 1052
		// (get) Token: 0x06001418 RID: 5144 RVA: 0x0004ACF0 File Offset: 0x00048EF0
		// (set) Token: 0x06001419 RID: 5145 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public bool GenerateSerializable
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that determines whether all XML schema types, even those that do not conform to a data contract schema, will be imported.</summary>
		/// <returns>
		///     <see langword="true" /> to import all schema types; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700041D RID: 1053
		// (get) Token: 0x0600141A RID: 5146 RVA: 0x0004AD0C File Offset: 0x00048F0C
		// (set) Token: 0x0600141B RID: 5147 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public bool ImportXmlType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a dictionary that contains the mapping of data contract namespaces to the CLR namespaces that must be used to generate code during an import operation.</summary>
		/// <returns>A <see cref="T:System.Collections.Generic.IDictionary`2" /> that contains the namespace mappings. </returns>
		// Token: 0x1700041E RID: 1054
		// (get) Token: 0x0600141C RID: 5148 RVA: 0x0004AD27 File Offset: 0x00048F27
		public IDictionary<string, string> Namespaces
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a collection of types that represents data contract collections that should be referenced when generating code for collections, such as lists or dictionaries of items.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.ICollection`1" /> that contains the referenced collection types.</returns>
		// Token: 0x1700041F RID: 1055
		// (get) Token: 0x0600141D RID: 5149 RVA: 0x0004AD27 File Offset: 0x00048F27
		public ICollection<Type> ReferencedCollectionTypes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a <see cref="T:System.Collections.Generic.IList`1" /> containing types referenced in generated code. </summary>
		/// <returns>A <see cref="T:System.Collections.Generic.IList`1" /> that contains the referenced types. </returns>
		// Token: 0x17000420 RID: 1056
		// (get) Token: 0x0600141E RID: 5150 RVA: 0x0004AD27 File Offset: 0x00048F27
		public ICollection<Type> ReferencedTypes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}
	}
}
