﻿using System;
using System.Collections.Generic;

namespace System.Runtime.Serialization
{
	// Token: 0x020000E0 RID: 224
	internal class HybridObjectCache
	{
		// Token: 0x06000CBA RID: 3258 RVA: 0x00002217 File Offset: 0x00000417
		internal HybridObjectCache()
		{
		}

		// Token: 0x06000CBB RID: 3259 RVA: 0x000315E8 File Offset: 0x0002F7E8
		internal void Add(string id, object obj)
		{
			if (this.objectDictionary == null)
			{
				this.objectDictionary = new Dictionary<string, object>();
			}
			object obj2;
			if (this.objectDictionary.TryGetValue(id, out obj2))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Invalid XML encountered. The same Id value '{0}' is defined more than once. Multiple objects cannot be deserialized using the same Id.", new object[]
				{
					id
				})));
			}
			this.objectDictionary.Add(id, obj);
		}

		// Token: 0x06000CBC RID: 3260 RVA: 0x00031644 File Offset: 0x0002F844
		internal void Remove(string id)
		{
			if (this.objectDictionary != null)
			{
				this.objectDictionary.Remove(id);
			}
		}

		// Token: 0x06000CBD RID: 3261 RVA: 0x0003165C File Offset: 0x0002F85C
		internal object GetObject(string id)
		{
			if (this.referencedObjectDictionary == null)
			{
				this.referencedObjectDictionary = new Dictionary<string, object>();
				this.referencedObjectDictionary.Add(id, null);
			}
			else if (!this.referencedObjectDictionary.ContainsKey(id))
			{
				this.referencedObjectDictionary.Add(id, null);
			}
			if (this.objectDictionary != null)
			{
				object result;
				this.objectDictionary.TryGetValue(id, out result);
				return result;
			}
			return null;
		}

		// Token: 0x06000CBE RID: 3262 RVA: 0x000316C0 File Offset: 0x0002F8C0
		internal bool IsObjectReferenced(string id)
		{
			return this.referencedObjectDictionary != null && this.referencedObjectDictionary.ContainsKey(id);
		}

		// Token: 0x040005FF RID: 1535
		private Dictionary<string, object> objectDictionary;

		// Token: 0x04000600 RID: 1536
		private Dictionary<string, object> referencedObjectDictionary;
	}
}
