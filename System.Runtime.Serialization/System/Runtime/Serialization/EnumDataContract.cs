﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security;
using System.Threading;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000CC RID: 204
	internal sealed class EnumDataContract : DataContract
	{
		// Token: 0x06000B93 RID: 2963 RVA: 0x0002E4F0 File Offset: 0x0002C6F0
		[SecuritySafeCritical]
		internal EnumDataContract() : base(new EnumDataContract.EnumDataContractCriticalHelper())
		{
			this.helper = (base.Helper as EnumDataContract.EnumDataContractCriticalHelper);
		}

		// Token: 0x06000B94 RID: 2964 RVA: 0x0002E50E File Offset: 0x0002C70E
		[SecuritySafeCritical]
		internal EnumDataContract(Type type) : base(new EnumDataContract.EnumDataContractCriticalHelper(type))
		{
			this.helper = (base.Helper as EnumDataContract.EnumDataContractCriticalHelper);
		}

		// Token: 0x06000B95 RID: 2965 RVA: 0x0002E52D File Offset: 0x0002C72D
		[SecuritySafeCritical]
		internal static XmlQualifiedName GetBaseContractName(Type type)
		{
			return EnumDataContract.EnumDataContractCriticalHelper.GetBaseContractName(type);
		}

		// Token: 0x06000B96 RID: 2966 RVA: 0x0002E535 File Offset: 0x0002C735
		[SecuritySafeCritical]
		internal static Type GetBaseType(XmlQualifiedName baseContractName)
		{
			return EnumDataContract.EnumDataContractCriticalHelper.GetBaseType(baseContractName);
		}

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x06000B97 RID: 2967 RVA: 0x0002E53D File Offset: 0x0002C73D
		// (set) Token: 0x06000B98 RID: 2968 RVA: 0x0002E54A File Offset: 0x0002C74A
		internal XmlQualifiedName BaseContractName
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.BaseContractName;
			}
			[SecurityCritical]
			set
			{
				this.helper.BaseContractName = value;
			}
		}

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x06000B99 RID: 2969 RVA: 0x0002E558 File Offset: 0x0002C758
		// (set) Token: 0x06000B9A RID: 2970 RVA: 0x0002E565 File Offset: 0x0002C765
		internal List<DataMember> Members
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.Members;
			}
			[SecurityCritical]
			set
			{
				this.helper.Members = value;
			}
		}

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x06000B9B RID: 2971 RVA: 0x0002E573 File Offset: 0x0002C773
		// (set) Token: 0x06000B9C RID: 2972 RVA: 0x0002E580 File Offset: 0x0002C780
		internal List<long> Values
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.Values;
			}
			[SecurityCritical]
			set
			{
				this.helper.Values = value;
			}
		}

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x06000B9D RID: 2973 RVA: 0x0002E58E File Offset: 0x0002C78E
		// (set) Token: 0x06000B9E RID: 2974 RVA: 0x0002E59B File Offset: 0x0002C79B
		internal bool IsFlags
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsFlags;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsFlags = value;
			}
		}

		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x06000B9F RID: 2975 RVA: 0x0002E5A9 File Offset: 0x0002C7A9
		internal bool IsULong
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsULong;
			}
		}

		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x06000BA0 RID: 2976 RVA: 0x0002E5B6 File Offset: 0x0002C7B6
		private XmlDictionaryString[] ChildElementNames
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.ChildElementNames;
			}
		}

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x06000BA1 RID: 2977 RVA: 0x0000310F File Offset: 0x0000130F
		internal override bool CanContainReferences
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000BA2 RID: 2978 RVA: 0x0002E5C4 File Offset: 0x0002C7C4
		internal void WriteEnumValue(XmlWriterDelegator writer, object value)
		{
			long num = (long)(this.IsULong ? ((IConvertible)value).ToUInt64(null) : ((ulong)((IConvertible)value).ToInt64(null)));
			for (int i = 0; i < this.Values.Count; i++)
			{
				if (num == this.Values[i])
				{
					writer.WriteString(this.ChildElementNames[i].Value);
					return;
				}
			}
			if (!this.IsFlags)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Enum value '{0}' is invalid for type '{1}' and cannot be serialized. Ensure that the necessary enum values are present and are marked with EnumMemberAttribute attribute if the type has DataContractAttribute attribute.", new object[]
				{
					value,
					DataContract.GetClrTypeFullName(base.UnderlyingType)
				})));
			}
			int num2 = -1;
			bool flag = true;
			for (int j = 0; j < this.Values.Count; j++)
			{
				long num3 = this.Values[j];
				if (num3 == 0L)
				{
					num2 = j;
				}
				else
				{
					if (num == 0L)
					{
						break;
					}
					if ((num3 & num) == num3)
					{
						if (flag)
						{
							flag = false;
						}
						else
						{
							writer.WriteString(DictionaryGlobals.Space.Value);
						}
						writer.WriteString(this.ChildElementNames[j].Value);
						num &= ~num3;
					}
				}
			}
			if (num != 0L)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Enum value '{0}' is invalid for type '{1}' and cannot be serialized. Ensure that the necessary enum values are present and are marked with EnumMemberAttribute attribute if the type has DataContractAttribute attribute.", new object[]
				{
					value,
					DataContract.GetClrTypeFullName(base.UnderlyingType)
				})));
			}
			if (flag && num2 >= 0)
			{
				writer.WriteString(this.ChildElementNames[num2].Value);
				return;
			}
		}

		// Token: 0x06000BA3 RID: 2979 RVA: 0x0002E724 File Offset: 0x0002C924
		internal object ReadEnumValue(XmlReaderDelegator reader)
		{
			string text = reader.ReadElementContentAsString();
			long num = 0L;
			int i = 0;
			if (this.IsFlags)
			{
				while (i < text.Length && text[i] == ' ')
				{
					i++;
				}
				int num2 = i;
				int num3;
				while (i < text.Length)
				{
					if (text[i] == ' ')
					{
						num3 = i - num2;
						if (num3 > 0)
						{
							num |= this.ReadEnumValue(text, num2, num3);
						}
						i++;
						while (i < text.Length && text[i] == ' ')
						{
							i++;
						}
						num2 = i;
						if (i == text.Length)
						{
							break;
						}
					}
					i++;
				}
				num3 = i - num2;
				if (num3 > 0)
				{
					num |= this.ReadEnumValue(text, num2, num3);
				}
			}
			else
			{
				if (text.Length == 0)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Invalid enum value '{0}' cannot be deserialized into type '{1}'. Ensure that the necessary enum values are present and are marked with EnumMemberAttribute attribute if the type has DataContractAttribute attribute.", new object[]
					{
						text,
						DataContract.GetClrTypeFullName(base.UnderlyingType)
					})));
				}
				num = this.ReadEnumValue(text, 0, text.Length);
			}
			if (this.IsULong)
			{
				return Enum.ToObject(base.UnderlyingType, (ulong)num);
			}
			return Enum.ToObject(base.UnderlyingType, num);
		}

		// Token: 0x06000BA4 RID: 2980 RVA: 0x0002E840 File Offset: 0x0002CA40
		private long ReadEnumValue(string value, int index, int count)
		{
			for (int i = 0; i < this.Members.Count; i++)
			{
				string name = this.Members[i].Name;
				if (name.Length == count && string.CompareOrdinal(value, index, name, 0, count) == 0)
				{
					return this.Values[i];
				}
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Invalid enum value '{0}' cannot be deserialized into type '{1}'. Ensure that the necessary enum values are present and are marked with EnumMemberAttribute attribute if the type has DataContractAttribute attribute.", new object[]
			{
				value.Substring(index, count),
				DataContract.GetClrTypeFullName(base.UnderlyingType)
			})));
		}

		// Token: 0x06000BA5 RID: 2981 RVA: 0x0002E8CA File Offset: 0x0002CACA
		internal string GetStringFromEnumValue(long value)
		{
			if (this.IsULong)
			{
				return XmlConvert.ToString((ulong)value);
			}
			return XmlConvert.ToString(value);
		}

		// Token: 0x06000BA6 RID: 2982 RVA: 0x0002E8E1 File Offset: 0x0002CAE1
		internal long GetEnumValueFromString(string value)
		{
			if (this.IsULong)
			{
				return (long)XmlConverter.ToUInt64(value);
			}
			return XmlConverter.ToInt64(value);
		}

		// Token: 0x06000BA7 RID: 2983 RVA: 0x0002E8F8 File Offset: 0x0002CAF8
		internal override bool Equals(object other, Dictionary<DataContractPairKey, object> checkedContracts)
		{
			if (base.IsEqualOrChecked(other, checkedContracts))
			{
				return true;
			}
			if (base.Equals(other, null))
			{
				EnumDataContract enumDataContract = other as EnumDataContract;
				if (enumDataContract != null)
				{
					if (this.Members.Count != enumDataContract.Members.Count || this.Values.Count != enumDataContract.Values.Count)
					{
						return false;
					}
					string[] array = new string[this.Members.Count];
					string[] array2 = new string[this.Members.Count];
					for (int i = 0; i < this.Members.Count; i++)
					{
						array[i] = this.Members[i].Name;
						array2[i] = enumDataContract.Members[i].Name;
					}
					Array.Sort<string>(array);
					Array.Sort<string>(array2);
					for (int j = 0; j < this.Members.Count; j++)
					{
						if (array[j] != array2[j])
						{
							return false;
						}
					}
					return this.IsFlags == enumDataContract.IsFlags;
				}
			}
			return false;
		}

		// Token: 0x06000BA8 RID: 2984 RVA: 0x0002625C File Offset: 0x0002445C
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000BA9 RID: 2985 RVA: 0x0002EA04 File Offset: 0x0002CC04
		public override void WriteXmlValue(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContext context)
		{
			this.WriteEnumValue(xmlWriter, obj);
		}

		// Token: 0x06000BAA RID: 2986 RVA: 0x0002EA10 File Offset: 0x0002CC10
		public override object ReadXmlValue(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContext context)
		{
			object obj = this.ReadEnumValue(xmlReader);
			if (context != null)
			{
				context.AddNewObject(obj);
			}
			return obj;
		}

		// Token: 0x040004F7 RID: 1271
		[SecurityCritical]
		private EnumDataContract.EnumDataContractCriticalHelper helper;

		// Token: 0x020000CD RID: 205
		private class EnumDataContractCriticalHelper : DataContract.DataContractCriticalHelper
		{
			// Token: 0x06000BAB RID: 2987 RVA: 0x0002EA30 File Offset: 0x0002CC30
			static EnumDataContractCriticalHelper()
			{
				EnumDataContract.EnumDataContractCriticalHelper.Add(typeof(sbyte), "byte");
				EnumDataContract.EnumDataContractCriticalHelper.Add(typeof(byte), "unsignedByte");
				EnumDataContract.EnumDataContractCriticalHelper.Add(typeof(short), "short");
				EnumDataContract.EnumDataContractCriticalHelper.Add(typeof(ushort), "unsignedShort");
				EnumDataContract.EnumDataContractCriticalHelper.Add(typeof(int), "int");
				EnumDataContract.EnumDataContractCriticalHelper.Add(typeof(uint), "unsignedInt");
				EnumDataContract.EnumDataContractCriticalHelper.Add(typeof(long), "long");
				EnumDataContract.EnumDataContractCriticalHelper.Add(typeof(ulong), "unsignedLong");
			}

			// Token: 0x06000BAC RID: 2988 RVA: 0x0002EAF4 File Offset: 0x0002CCF4
			internal static void Add(Type type, string localName)
			{
				XmlQualifiedName xmlQualifiedName = DataContract.CreateQualifiedName(localName, "http://www.w3.org/2001/XMLSchema");
				EnumDataContract.EnumDataContractCriticalHelper.typeToName.Add(type, xmlQualifiedName);
				EnumDataContract.EnumDataContractCriticalHelper.nameToType.Add(xmlQualifiedName, type);
			}

			// Token: 0x06000BAD RID: 2989 RVA: 0x0002EB28 File Offset: 0x0002CD28
			internal static XmlQualifiedName GetBaseContractName(Type type)
			{
				XmlQualifiedName result = null;
				EnumDataContract.EnumDataContractCriticalHelper.typeToName.TryGetValue(type, out result);
				return result;
			}

			// Token: 0x06000BAE RID: 2990 RVA: 0x0002EB48 File Offset: 0x0002CD48
			internal static Type GetBaseType(XmlQualifiedName baseContractName)
			{
				Type result = null;
				EnumDataContract.EnumDataContractCriticalHelper.nameToType.TryGetValue(baseContractName, out result);
				return result;
			}

			// Token: 0x06000BAF RID: 2991 RVA: 0x0002EB66 File Offset: 0x0002CD66
			internal EnumDataContractCriticalHelper()
			{
				base.IsValueType = true;
			}

			// Token: 0x06000BB0 RID: 2992 RVA: 0x0002EB78 File Offset: 0x0002CD78
			internal EnumDataContractCriticalHelper(Type type) : base(type)
			{
				base.StableName = DataContract.GetStableName(type, out this.hasDataContract);
				Type underlyingType = Enum.GetUnderlyingType(type);
				this.baseContractName = EnumDataContract.EnumDataContractCriticalHelper.GetBaseContractName(underlyingType);
				this.ImportBaseType(underlyingType);
				this.IsFlags = type.IsDefined(Globals.TypeOfFlagsAttribute, false);
				this.ImportDataMembers();
				XmlDictionary xmlDictionary = new XmlDictionary(2 + this.Members.Count);
				base.Name = xmlDictionary.Add(base.StableName.Name);
				base.Namespace = xmlDictionary.Add(base.StableName.Namespace);
				this.childElementNames = new XmlDictionaryString[this.Members.Count];
				for (int i = 0; i < this.Members.Count; i++)
				{
					this.childElementNames[i] = xmlDictionary.Add(this.Members[i].Name);
				}
				DataContractAttribute dataContractAttribute;
				if (DataContract.TryGetDCAttribute(type, out dataContractAttribute) && dataContractAttribute.IsReference)
				{
					DataContract.ThrowInvalidDataContractException(SR.GetString("Enum type '{0}' cannot have the IsReference setting of '{1}'. Either change the setting to '{2}', or remove it completely.", new object[]
					{
						DataContract.GetClrTypeFullName(type),
						dataContractAttribute.IsReference,
						false
					}), type);
				}
			}

			// Token: 0x170001F9 RID: 505
			// (get) Token: 0x06000BB1 RID: 2993 RVA: 0x0002ECA4 File Offset: 0x0002CEA4
			// (set) Token: 0x06000BB2 RID: 2994 RVA: 0x0002ECAC File Offset: 0x0002CEAC
			internal XmlQualifiedName BaseContractName
			{
				get
				{
					return this.baseContractName;
				}
				set
				{
					this.baseContractName = value;
					Type baseType = EnumDataContract.EnumDataContractCriticalHelper.GetBaseType(this.baseContractName);
					if (baseType == null)
					{
						base.ThrowInvalidDataContractException(SR.GetString("Invalid enum base type is specified for type '{0}' in '{1}' namespace, element name is '{2}' in '{3}' namespace.", new object[]
						{
							value.Name,
							value.Namespace,
							base.StableName.Name,
							base.StableName.Namespace
						}));
					}
					this.ImportBaseType(baseType);
				}
			}

			// Token: 0x170001FA RID: 506
			// (get) Token: 0x06000BB3 RID: 2995 RVA: 0x0002ED20 File Offset: 0x0002CF20
			// (set) Token: 0x06000BB4 RID: 2996 RVA: 0x0002ED28 File Offset: 0x0002CF28
			internal List<DataMember> Members
			{
				get
				{
					return this.members;
				}
				set
				{
					this.members = value;
				}
			}

			// Token: 0x170001FB RID: 507
			// (get) Token: 0x06000BB5 RID: 2997 RVA: 0x0002ED31 File Offset: 0x0002CF31
			// (set) Token: 0x06000BB6 RID: 2998 RVA: 0x0002ED39 File Offset: 0x0002CF39
			internal List<long> Values
			{
				get
				{
					return this.values;
				}
				set
				{
					this.values = value;
				}
			}

			// Token: 0x170001FC RID: 508
			// (get) Token: 0x06000BB7 RID: 2999 RVA: 0x0002ED42 File Offset: 0x0002CF42
			// (set) Token: 0x06000BB8 RID: 3000 RVA: 0x0002ED4A File Offset: 0x0002CF4A
			internal bool IsFlags
			{
				get
				{
					return this.isFlags;
				}
				set
				{
					this.isFlags = value;
				}
			}

			// Token: 0x170001FD RID: 509
			// (get) Token: 0x06000BB9 RID: 3001 RVA: 0x0002ED53 File Offset: 0x0002CF53
			internal bool IsULong
			{
				get
				{
					return this.isULong;
				}
			}

			// Token: 0x170001FE RID: 510
			// (get) Token: 0x06000BBA RID: 3002 RVA: 0x0002ED5B File Offset: 0x0002CF5B
			internal XmlDictionaryString[] ChildElementNames
			{
				get
				{
					return this.childElementNames;
				}
			}

			// Token: 0x06000BBB RID: 3003 RVA: 0x0002ED63 File Offset: 0x0002CF63
			private void ImportBaseType(Type baseType)
			{
				this.isULong = (baseType == Globals.TypeOfULong);
			}

			// Token: 0x06000BBC RID: 3004 RVA: 0x0002ED78 File Offset: 0x0002CF78
			private void ImportDataMembers()
			{
				Type underlyingType = base.UnderlyingType;
				FieldInfo[] fields = underlyingType.GetFields(BindingFlags.Static | BindingFlags.Public);
				Dictionary<string, DataMember> memberNamesTable = new Dictionary<string, DataMember>();
				List<DataMember> list = new List<DataMember>(fields.Length);
				List<long> list2 = new List<long>(fields.Length);
				foreach (FieldInfo fieldInfo in fields)
				{
					bool flag = false;
					if (this.hasDataContract)
					{
						object[] customAttributes = fieldInfo.GetCustomAttributes(Globals.TypeOfEnumMemberAttribute, false);
						if (customAttributes != null && customAttributes.Length != 0)
						{
							if (customAttributes.Length > 1)
							{
								base.ThrowInvalidDataContractException(SR.GetString("Member '{0}.{1}' has more than one EnumMemberAttribute attribute.", new object[]
								{
									DataContract.GetClrTypeFullName(fieldInfo.DeclaringType),
									fieldInfo.Name
								}));
							}
							EnumMemberAttribute enumMemberAttribute = (EnumMemberAttribute)customAttributes[0];
							DataMember dataMember = new DataMember(fieldInfo);
							if (enumMemberAttribute.IsValueSetExplicitly)
							{
								if (enumMemberAttribute.Value == null || enumMemberAttribute.Value.Length == 0)
								{
									base.ThrowInvalidDataContractException(SR.GetString("'{0}' in type '{1}' cannot have EnumMemberAttribute attribute Value set to null or empty string.", new object[]
									{
										fieldInfo.Name,
										DataContract.GetClrTypeFullName(underlyingType)
									}));
								}
								dataMember.Name = enumMemberAttribute.Value;
							}
							else
							{
								dataMember.Name = fieldInfo.Name;
							}
							ClassDataContract.CheckAndAddMember(list, dataMember, memberNamesTable);
							flag = true;
						}
						object[] customAttributes2 = fieldInfo.GetCustomAttributes(Globals.TypeOfDataMemberAttribute, false);
						if (customAttributes2 != null && customAttributes2.Length != 0)
						{
							base.ThrowInvalidDataContractException(SR.GetString("Member '{0}.{1}' has DataMemberAttribute attribute. Use EnumMemberAttribute attribute instead.", new object[]
							{
								DataContract.GetClrTypeFullName(fieldInfo.DeclaringType),
								fieldInfo.Name
							}));
						}
					}
					else if (!fieldInfo.IsNotSerialized)
					{
						ClassDataContract.CheckAndAddMember(list, new DataMember(fieldInfo)
						{
							Name = fieldInfo.Name
						}, memberNamesTable);
						flag = true;
					}
					if (flag)
					{
						object value = fieldInfo.GetValue(null);
						if (this.isULong)
						{
							list2.Add((long)((IConvertible)value).ToUInt64(null));
						}
						else
						{
							list2.Add(((IConvertible)value).ToInt64(null));
						}
					}
				}
				Thread.MemoryBarrier();
				this.members = list;
				this.values = list2;
			}

			// Token: 0x040004F8 RID: 1272
			private static Dictionary<Type, XmlQualifiedName> typeToName = new Dictionary<Type, XmlQualifiedName>();

			// Token: 0x040004F9 RID: 1273
			private static Dictionary<XmlQualifiedName, Type> nameToType = new Dictionary<XmlQualifiedName, Type>();

			// Token: 0x040004FA RID: 1274
			private XmlQualifiedName baseContractName;

			// Token: 0x040004FB RID: 1275
			private List<DataMember> members;

			// Token: 0x040004FC RID: 1276
			private List<long> values;

			// Token: 0x040004FD RID: 1277
			private bool isULong;

			// Token: 0x040004FE RID: 1278
			private bool isFlags;

			// Token: 0x040004FF RID: 1279
			private bool hasDataContract;

			// Token: 0x04000500 RID: 1280
			private XmlDictionaryString[] childElementNames;
		}
	}
}
