﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x0200011B RID: 283
	internal class GuidDataContract : PrimitiveDataContract
	{
		// Token: 0x06000DDA RID: 3546 RVA: 0x00033A77 File Offset: 0x00031C77
		internal GuidDataContract() : this(DictionaryGlobals.GuidLocalName, DictionaryGlobals.SerializationNamespace)
		{
		}

		// Token: 0x06000DDB RID: 3547 RVA: 0x00033A89 File Offset: 0x00031C89
		internal GuidDataContract(XmlDictionaryString name, XmlDictionaryString ns) : base(typeof(Guid), name, ns)
		{
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x06000DDC RID: 3548 RVA: 0x00033A9D File Offset: 0x00031C9D
		internal override string WriteMethodName
		{
			get
			{
				return "WriteGuid";
			}
		}

		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x06000DDD RID: 3549 RVA: 0x00033AA4 File Offset: 0x00031CA4
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsGuid";
			}
		}

		// Token: 0x06000DDE RID: 3550 RVA: 0x00033AAB File Offset: 0x00031CAB
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteGuid((Guid)obj);
		}

		// Token: 0x06000DDF RID: 3551 RVA: 0x00033AB9 File Offset: 0x00031CB9
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsGuid(), context);
			}
			return reader.ReadElementContentAsGuid();
		}
	}
}
