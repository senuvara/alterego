﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000103 RID: 259
	internal class TimeDataContract : StringDataContract
	{
		// Token: 0x06000DB3 RID: 3507 RVA: 0x00033788 File Offset: 0x00031988
		internal TimeDataContract() : base(DictionaryGlobals.timeLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
