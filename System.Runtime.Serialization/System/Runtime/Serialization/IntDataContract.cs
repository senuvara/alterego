﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000F5 RID: 245
	internal class IntDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D7F RID: 3455 RVA: 0x000333E6 File Offset: 0x000315E6
		internal IntDataContract() : base(typeof(int), DictionaryGlobals.IntLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06000D80 RID: 3456 RVA: 0x00033402 File Offset: 0x00031602
		internal override string WriteMethodName
		{
			get
			{
				return "WriteInt";
			}
		}

		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06000D81 RID: 3457 RVA: 0x00033409 File Offset: 0x00031609
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsInt";
			}
		}

		// Token: 0x06000D82 RID: 3458 RVA: 0x00033410 File Offset: 0x00031610
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteInt((int)obj);
		}

		// Token: 0x06000D83 RID: 3459 RVA: 0x0003341E File Offset: 0x0003161E
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsInt(), context);
			}
			return reader.ReadElementContentAsInt();
		}
	}
}
