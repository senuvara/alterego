﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;

namespace System.Runtime.Serialization
{
	// Token: 0x02000145 RID: 325
	internal class XmlReaderDelegator
	{
		// Token: 0x06000FDF RID: 4063 RVA: 0x0003A3FE File Offset: 0x000385FE
		public XmlReaderDelegator(XmlReader reader)
		{
			XmlObjectSerializer.CheckNull(reader, "reader");
			this.reader = reader;
			this.dictionaryReader = (reader as XmlDictionaryReader);
		}

		// Token: 0x17000380 RID: 896
		// (get) Token: 0x06000FE0 RID: 4064 RVA: 0x0003A424 File Offset: 0x00038624
		internal XmlReader UnderlyingReader
		{
			get
			{
				return this.reader;
			}
		}

		// Token: 0x17000381 RID: 897
		// (get) Token: 0x06000FE1 RID: 4065 RVA: 0x0003A42C File Offset: 0x0003862C
		internal ExtensionDataReader UnderlyingExtensionDataReader
		{
			get
			{
				return this.reader as ExtensionDataReader;
			}
		}

		// Token: 0x17000382 RID: 898
		// (get) Token: 0x06000FE2 RID: 4066 RVA: 0x0003A439 File Offset: 0x00038639
		internal int AttributeCount
		{
			get
			{
				if (!this.isEndOfEmptyElement)
				{
					return this.reader.AttributeCount;
				}
				return 0;
			}
		}

		// Token: 0x06000FE3 RID: 4067 RVA: 0x0003A450 File Offset: 0x00038650
		internal string GetAttribute(string name)
		{
			if (!this.isEndOfEmptyElement)
			{
				return this.reader.GetAttribute(name);
			}
			return null;
		}

		// Token: 0x06000FE4 RID: 4068 RVA: 0x0003A468 File Offset: 0x00038668
		internal string GetAttribute(string name, string namespaceUri)
		{
			if (!this.isEndOfEmptyElement)
			{
				return this.reader.GetAttribute(name, namespaceUri);
			}
			return null;
		}

		// Token: 0x06000FE5 RID: 4069 RVA: 0x0003A481 File Offset: 0x00038681
		internal string GetAttribute(int i)
		{
			if (this.isEndOfEmptyElement)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("i", SR.GetString("Only Element nodes have attributes.")));
			}
			return this.reader.GetAttribute(i);
		}

		// Token: 0x17000383 RID: 899
		// (get) Token: 0x06000FE6 RID: 4070 RVA: 0x0000310F File Offset: 0x0000130F
		internal bool IsEmptyElement
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000FE7 RID: 4071 RVA: 0x0003A4B1 File Offset: 0x000386B1
		internal bool IsNamespaceURI(string ns)
		{
			if (this.dictionaryReader == null)
			{
				return ns == this.reader.NamespaceURI;
			}
			return this.dictionaryReader.IsNamespaceUri(ns);
		}

		// Token: 0x06000FE8 RID: 4072 RVA: 0x0003A4D9 File Offset: 0x000386D9
		internal bool IsLocalName(string localName)
		{
			if (this.dictionaryReader == null)
			{
				return localName == this.reader.LocalName;
			}
			return this.dictionaryReader.IsLocalName(localName);
		}

		// Token: 0x06000FE9 RID: 4073 RVA: 0x0003A501 File Offset: 0x00038701
		internal bool IsNamespaceUri(XmlDictionaryString ns)
		{
			if (this.dictionaryReader == null)
			{
				return ns.Value == this.reader.NamespaceURI;
			}
			return this.dictionaryReader.IsNamespaceUri(ns);
		}

		// Token: 0x06000FEA RID: 4074 RVA: 0x0003A52E File Offset: 0x0003872E
		internal bool IsLocalName(XmlDictionaryString localName)
		{
			if (this.dictionaryReader == null)
			{
				return localName.Value == this.reader.LocalName;
			}
			return this.dictionaryReader.IsLocalName(localName);
		}

		// Token: 0x06000FEB RID: 4075 RVA: 0x0003A55C File Offset: 0x0003875C
		internal int IndexOfLocalName(XmlDictionaryString[] localNames, XmlDictionaryString ns)
		{
			if (this.dictionaryReader != null)
			{
				return this.dictionaryReader.IndexOfLocalName(localNames, ns);
			}
			if (this.reader.NamespaceURI == ns.Value)
			{
				string localName = this.LocalName;
				for (int i = 0; i < localNames.Length; i++)
				{
					if (localName == localNames[i].Value)
					{
						return i;
					}
				}
			}
			return -1;
		}

		// Token: 0x06000FEC RID: 4076 RVA: 0x0003A5BF File Offset: 0x000387BF
		public bool IsStartElement()
		{
			return !this.isEndOfEmptyElement && this.reader.IsStartElement();
		}

		// Token: 0x06000FED RID: 4077 RVA: 0x0003A5D6 File Offset: 0x000387D6
		internal bool IsStartElement(string localname, string ns)
		{
			return !this.isEndOfEmptyElement && this.reader.IsStartElement(localname, ns);
		}

		// Token: 0x06000FEE RID: 4078 RVA: 0x0003A5F0 File Offset: 0x000387F0
		public bool IsStartElement(XmlDictionaryString localname, XmlDictionaryString ns)
		{
			if (this.dictionaryReader == null)
			{
				return !this.isEndOfEmptyElement && this.reader.IsStartElement(localname.Value, ns.Value);
			}
			return !this.isEndOfEmptyElement && this.dictionaryReader.IsStartElement(localname, ns);
		}

		// Token: 0x06000FEF RID: 4079 RVA: 0x0003A63E File Offset: 0x0003883E
		internal bool MoveToAttribute(string name)
		{
			return !this.isEndOfEmptyElement && this.reader.MoveToAttribute(name);
		}

		// Token: 0x06000FF0 RID: 4080 RVA: 0x0003A656 File Offset: 0x00038856
		internal bool MoveToAttribute(string name, string ns)
		{
			return !this.isEndOfEmptyElement && this.reader.MoveToAttribute(name, ns);
		}

		// Token: 0x06000FF1 RID: 4081 RVA: 0x0003A66F File Offset: 0x0003886F
		internal void MoveToAttribute(int i)
		{
			if (this.isEndOfEmptyElement)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("i", SR.GetString("Only Element nodes have attributes.")));
			}
			this.reader.MoveToAttribute(i);
		}

		// Token: 0x06000FF2 RID: 4082 RVA: 0x0003A69F File Offset: 0x0003889F
		internal bool MoveToElement()
		{
			return !this.isEndOfEmptyElement && this.reader.MoveToElement();
		}

		// Token: 0x06000FF3 RID: 4083 RVA: 0x0003A6B6 File Offset: 0x000388B6
		internal bool MoveToFirstAttribute()
		{
			return !this.isEndOfEmptyElement && this.reader.MoveToFirstAttribute();
		}

		// Token: 0x06000FF4 RID: 4084 RVA: 0x0003A6CD File Offset: 0x000388CD
		internal bool MoveToNextAttribute()
		{
			return !this.isEndOfEmptyElement && this.reader.MoveToNextAttribute();
		}

		// Token: 0x17000384 RID: 900
		// (get) Token: 0x06000FF5 RID: 4085 RVA: 0x0003A6E4 File Offset: 0x000388E4
		public XmlNodeType NodeType
		{
			get
			{
				if (!this.isEndOfEmptyElement)
				{
					return this.reader.NodeType;
				}
				return XmlNodeType.EndElement;
			}
		}

		// Token: 0x06000FF6 RID: 4086 RVA: 0x0003A6FC File Offset: 0x000388FC
		internal bool Read()
		{
			this.reader.MoveToElement();
			if (!this.reader.IsEmptyElement)
			{
				return this.reader.Read();
			}
			if (this.isEndOfEmptyElement)
			{
				this.isEndOfEmptyElement = false;
				return this.reader.Read();
			}
			this.isEndOfEmptyElement = true;
			return true;
		}

		// Token: 0x06000FF7 RID: 4087 RVA: 0x0003A751 File Offset: 0x00038951
		internal XmlNodeType MoveToContent()
		{
			if (this.isEndOfEmptyElement)
			{
				return XmlNodeType.EndElement;
			}
			return this.reader.MoveToContent();
		}

		// Token: 0x06000FF8 RID: 4088 RVA: 0x0003A769 File Offset: 0x00038969
		internal bool ReadAttributeValue()
		{
			return !this.isEndOfEmptyElement && this.reader.ReadAttributeValue();
		}

		// Token: 0x06000FF9 RID: 4089 RVA: 0x0003A780 File Offset: 0x00038980
		public void ReadEndElement()
		{
			if (this.isEndOfEmptyElement)
			{
				this.Read();
				return;
			}
			this.reader.ReadEndElement();
		}

		// Token: 0x06000FFA RID: 4090 RVA: 0x0003A79D File Offset: 0x0003899D
		private Exception CreateInvalidPrimitiveTypeException(Type type)
		{
			return new InvalidDataContractException(SR.GetString(type.IsInterface ? "Interface type '{0}' cannot be created. Consider replacing with a non-interface serializable type." : "Type '{0}' is not a valid serializable type.", new object[]
			{
				DataContract.GetClrTypeFullName(type)
			}));
		}

		// Token: 0x06000FFB RID: 4091 RVA: 0x0003A7CC File Offset: 0x000389CC
		public object ReadElementContentAsAnyType(Type valueType)
		{
			this.Read();
			object result = this.ReadContentAsAnyType(valueType);
			this.ReadEndElement();
			return result;
		}

		// Token: 0x06000FFC RID: 4092 RVA: 0x0003A7E4 File Offset: 0x000389E4
		internal object ReadContentAsAnyType(Type valueType)
		{
			switch (Type.GetTypeCode(valueType))
			{
			case TypeCode.Boolean:
				return this.ReadContentAsBoolean();
			case TypeCode.Char:
				return this.ReadContentAsChar();
			case TypeCode.SByte:
				return this.ReadContentAsSignedByte();
			case TypeCode.Byte:
				return this.ReadContentAsUnsignedByte();
			case TypeCode.Int16:
				return this.ReadContentAsShort();
			case TypeCode.UInt16:
				return this.ReadContentAsUnsignedShort();
			case TypeCode.Int32:
				return this.ReadContentAsInt();
			case TypeCode.UInt32:
				return this.ReadContentAsUnsignedInt();
			case TypeCode.Int64:
				return this.ReadContentAsLong();
			case TypeCode.UInt64:
				return this.ReadContentAsUnsignedLong();
			case TypeCode.Single:
				return this.ReadContentAsSingle();
			case TypeCode.Double:
				return this.ReadContentAsDouble();
			case TypeCode.Decimal:
				return this.ReadContentAsDecimal();
			case TypeCode.DateTime:
				return this.ReadContentAsDateTime();
			case TypeCode.String:
				return this.ReadContentAsString();
			}
			if (valueType == Globals.TypeOfByteArray)
			{
				return this.ReadContentAsBase64();
			}
			if (valueType == Globals.TypeOfObject)
			{
				return new object();
			}
			if (valueType == Globals.TypeOfTimeSpan)
			{
				return this.ReadContentAsTimeSpan();
			}
			if (valueType == Globals.TypeOfGuid)
			{
				return this.ReadContentAsGuid();
			}
			if (valueType == Globals.TypeOfUri)
			{
				return this.ReadContentAsUri();
			}
			if (valueType == Globals.TypeOfXmlQualifiedName)
			{
				return this.ReadContentAsQName();
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(this.CreateInvalidPrimitiveTypeException(valueType));
		}

		// Token: 0x06000FFD RID: 4093 RVA: 0x0003A98C File Offset: 0x00038B8C
		internal IDataNode ReadExtensionData(Type valueType)
		{
			switch (Type.GetTypeCode(valueType))
			{
			case TypeCode.Boolean:
				return new DataNode<bool>(this.ReadContentAsBoolean());
			case TypeCode.Char:
				return new DataNode<char>(this.ReadContentAsChar());
			case TypeCode.SByte:
				return new DataNode<sbyte>(this.ReadContentAsSignedByte());
			case TypeCode.Byte:
				return new DataNode<byte>(this.ReadContentAsUnsignedByte());
			case TypeCode.Int16:
				return new DataNode<short>(this.ReadContentAsShort());
			case TypeCode.UInt16:
				return new DataNode<ushort>(this.ReadContentAsUnsignedShort());
			case TypeCode.Int32:
				return new DataNode<int>(this.ReadContentAsInt());
			case TypeCode.UInt32:
				return new DataNode<uint>(this.ReadContentAsUnsignedInt());
			case TypeCode.Int64:
				return new DataNode<long>(this.ReadContentAsLong());
			case TypeCode.UInt64:
				return new DataNode<ulong>(this.ReadContentAsUnsignedLong());
			case TypeCode.Single:
				return new DataNode<float>(this.ReadContentAsSingle());
			case TypeCode.Double:
				return new DataNode<double>(this.ReadContentAsDouble());
			case TypeCode.Decimal:
				return new DataNode<decimal>(this.ReadContentAsDecimal());
			case TypeCode.DateTime:
				return new DataNode<DateTime>(this.ReadContentAsDateTime());
			case TypeCode.String:
				return new DataNode<string>(this.ReadContentAsString());
			}
			if (valueType == Globals.TypeOfByteArray)
			{
				return new DataNode<byte[]>(this.ReadContentAsBase64());
			}
			if (valueType == Globals.TypeOfObject)
			{
				return new DataNode<object>(new object());
			}
			if (valueType == Globals.TypeOfTimeSpan)
			{
				return new DataNode<TimeSpan>(this.ReadContentAsTimeSpan());
			}
			if (valueType == Globals.TypeOfGuid)
			{
				return new DataNode<Guid>(this.ReadContentAsGuid());
			}
			if (valueType == Globals.TypeOfUri)
			{
				return new DataNode<Uri>(this.ReadContentAsUri());
			}
			if (valueType == Globals.TypeOfXmlQualifiedName)
			{
				return new DataNode<XmlQualifiedName>(this.ReadContentAsQName());
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(this.CreateInvalidPrimitiveTypeException(valueType));
		}

		// Token: 0x06000FFE RID: 4094 RVA: 0x0003AB4C File Offset: 0x00038D4C
		private void ThrowConversionException(string value, string type)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(XmlObjectSerializer.TryAddLineInfo(this, SR.GetString("The value '{0}' cannot be parsed as the type '{1}'.", new object[]
			{
				value,
				type
			}))));
		}

		// Token: 0x06000FFF RID: 4095 RVA: 0x0003AB76 File Offset: 0x00038D76
		private void ThrowNotAtElement()
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Start element expected. Found {0}.", new object[]
			{
				"EndElement"
			})));
		}

		// Token: 0x06001000 RID: 4096 RVA: 0x0003AB9A File Offset: 0x00038D9A
		internal virtual char ReadElementContentAsChar()
		{
			return this.ToChar(this.ReadElementContentAsInt());
		}

		// Token: 0x06001001 RID: 4097 RVA: 0x0003ABA8 File Offset: 0x00038DA8
		internal virtual char ReadContentAsChar()
		{
			return this.ToChar(this.ReadContentAsInt());
		}

		// Token: 0x06001002 RID: 4098 RVA: 0x0003ABB6 File Offset: 0x00038DB6
		private char ToChar(int value)
		{
			if (value < 0 || value > 65535)
			{
				this.ThrowConversionException(value.ToString(NumberFormatInfo.CurrentInfo), "Char");
			}
			return (char)value;
		}

		// Token: 0x06001003 RID: 4099 RVA: 0x0003ABDD File Offset: 0x00038DDD
		public string ReadElementContentAsString()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			return this.reader.ReadElementContentAsString();
		}

		// Token: 0x06001004 RID: 4100 RVA: 0x0003ABF8 File Offset: 0x00038DF8
		internal string ReadContentAsString()
		{
			if (!this.isEndOfEmptyElement)
			{
				return this.reader.ReadContentAsString();
			}
			return string.Empty;
		}

		// Token: 0x06001005 RID: 4101 RVA: 0x0003AC13 File Offset: 0x00038E13
		public bool ReadElementContentAsBoolean()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			return this.reader.ReadElementContentAsBoolean();
		}

		// Token: 0x06001006 RID: 4102 RVA: 0x0003AC2E File Offset: 0x00038E2E
		internal bool ReadContentAsBoolean()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowConversionException(string.Empty, "Boolean");
			}
			return this.reader.ReadContentAsBoolean();
		}

		// Token: 0x06001007 RID: 4103 RVA: 0x0003AC53 File Offset: 0x00038E53
		public float ReadElementContentAsFloat()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			return this.reader.ReadElementContentAsFloat();
		}

		// Token: 0x06001008 RID: 4104 RVA: 0x0003AC6E File Offset: 0x00038E6E
		internal float ReadContentAsSingle()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowConversionException(string.Empty, "Float");
			}
			return this.reader.ReadContentAsFloat();
		}

		// Token: 0x06001009 RID: 4105 RVA: 0x0003AC93 File Offset: 0x00038E93
		public double ReadElementContentAsDouble()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			return this.reader.ReadElementContentAsDouble();
		}

		// Token: 0x0600100A RID: 4106 RVA: 0x0003ACAE File Offset: 0x00038EAE
		internal double ReadContentAsDouble()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowConversionException(string.Empty, "Double");
			}
			return this.reader.ReadContentAsDouble();
		}

		// Token: 0x0600100B RID: 4107 RVA: 0x0003ACD3 File Offset: 0x00038ED3
		public decimal ReadElementContentAsDecimal()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			return this.reader.ReadElementContentAsDecimal();
		}

		// Token: 0x0600100C RID: 4108 RVA: 0x0003ACEE File Offset: 0x00038EEE
		internal decimal ReadContentAsDecimal()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowConversionException(string.Empty, "Decimal");
			}
			return this.reader.ReadContentAsDecimal();
		}

		// Token: 0x0600100D RID: 4109 RVA: 0x0003AD13 File Offset: 0x00038F13
		internal virtual byte[] ReadElementContentAsBase64()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			if (this.dictionaryReader == null)
			{
				return this.ReadContentAsBase64(this.reader.ReadElementContentAsString());
			}
			return this.dictionaryReader.ReadElementContentAsBase64();
		}

		// Token: 0x0600100E RID: 4110 RVA: 0x0003AD48 File Offset: 0x00038F48
		internal virtual byte[] ReadContentAsBase64()
		{
			if (this.isEndOfEmptyElement)
			{
				return new byte[0];
			}
			if (this.dictionaryReader == null)
			{
				return this.ReadContentAsBase64(this.reader.ReadContentAsString());
			}
			return this.dictionaryReader.ReadContentAsBase64();
		}

		// Token: 0x0600100F RID: 4111 RVA: 0x0003AD80 File Offset: 0x00038F80
		internal byte[] ReadContentAsBase64(string str)
		{
			if (str == null)
			{
				return null;
			}
			str = str.Trim();
			if (str.Length == 0)
			{
				return new byte[0];
			}
			byte[] result;
			try
			{
				result = Convert.FromBase64String(str);
			}
			catch (ArgumentException exception)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(str, "byte[]", exception));
			}
			catch (FormatException exception2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(str, "byte[]", exception2));
			}
			return result;
		}

		// Token: 0x06001010 RID: 4112 RVA: 0x0003ADF8 File Offset: 0x00038FF8
		internal virtual DateTime ReadElementContentAsDateTime()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			return this.reader.ReadElementContentAsDateTime();
		}

		// Token: 0x06001011 RID: 4113 RVA: 0x0003AE13 File Offset: 0x00039013
		internal virtual DateTime ReadContentAsDateTime()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowConversionException(string.Empty, "DateTime");
			}
			return this.reader.ReadContentAsDateTime();
		}

		// Token: 0x06001012 RID: 4114 RVA: 0x0003AE38 File Offset: 0x00039038
		public int ReadElementContentAsInt()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			return this.reader.ReadElementContentAsInt();
		}

		// Token: 0x06001013 RID: 4115 RVA: 0x0003AE53 File Offset: 0x00039053
		internal int ReadContentAsInt()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowConversionException(string.Empty, "Int32");
			}
			return this.reader.ReadContentAsInt();
		}

		// Token: 0x06001014 RID: 4116 RVA: 0x0003AE78 File Offset: 0x00039078
		public long ReadElementContentAsLong()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			return this.reader.ReadElementContentAsLong();
		}

		// Token: 0x06001015 RID: 4117 RVA: 0x0003AE93 File Offset: 0x00039093
		internal long ReadContentAsLong()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowConversionException(string.Empty, "Int64");
			}
			return this.reader.ReadContentAsLong();
		}

		// Token: 0x06001016 RID: 4118 RVA: 0x0003AEB8 File Offset: 0x000390B8
		public short ReadElementContentAsShort()
		{
			return this.ToShort(this.ReadElementContentAsInt());
		}

		// Token: 0x06001017 RID: 4119 RVA: 0x0003AEC6 File Offset: 0x000390C6
		internal short ReadContentAsShort()
		{
			return this.ToShort(this.ReadContentAsInt());
		}

		// Token: 0x06001018 RID: 4120 RVA: 0x0003AED4 File Offset: 0x000390D4
		private short ToShort(int value)
		{
			if (value < -32768 || value > 32767)
			{
				this.ThrowConversionException(value.ToString(NumberFormatInfo.CurrentInfo), "Int16");
			}
			return (short)value;
		}

		// Token: 0x06001019 RID: 4121 RVA: 0x0003AEFF File Offset: 0x000390FF
		public byte ReadElementContentAsUnsignedByte()
		{
			return this.ToByte(this.ReadElementContentAsInt());
		}

		// Token: 0x0600101A RID: 4122 RVA: 0x0003AF0D File Offset: 0x0003910D
		internal byte ReadContentAsUnsignedByte()
		{
			return this.ToByte(this.ReadContentAsInt());
		}

		// Token: 0x0600101B RID: 4123 RVA: 0x0003AF1B File Offset: 0x0003911B
		private byte ToByte(int value)
		{
			if (value < 0 || value > 255)
			{
				this.ThrowConversionException(value.ToString(NumberFormatInfo.CurrentInfo), "Byte");
			}
			return (byte)value;
		}

		// Token: 0x0600101C RID: 4124 RVA: 0x0003AF42 File Offset: 0x00039142
		public sbyte ReadElementContentAsSignedByte()
		{
			return this.ToSByte(this.ReadElementContentAsInt());
		}

		// Token: 0x0600101D RID: 4125 RVA: 0x0003AF50 File Offset: 0x00039150
		internal sbyte ReadContentAsSignedByte()
		{
			return this.ToSByte(this.ReadContentAsInt());
		}

		// Token: 0x0600101E RID: 4126 RVA: 0x0003AF5E File Offset: 0x0003915E
		private sbyte ToSByte(int value)
		{
			if (value < -128 || value > 127)
			{
				this.ThrowConversionException(value.ToString(NumberFormatInfo.CurrentInfo), "SByte");
			}
			return (sbyte)value;
		}

		// Token: 0x0600101F RID: 4127 RVA: 0x0003AF83 File Offset: 0x00039183
		public uint ReadElementContentAsUnsignedInt()
		{
			return this.ToUInt32(this.ReadElementContentAsLong());
		}

		// Token: 0x06001020 RID: 4128 RVA: 0x0003AF91 File Offset: 0x00039191
		internal uint ReadContentAsUnsignedInt()
		{
			return this.ToUInt32(this.ReadContentAsLong());
		}

		// Token: 0x06001021 RID: 4129 RVA: 0x0003AF9F File Offset: 0x0003919F
		private uint ToUInt32(long value)
		{
			if (value < 0L || value > (long)((ulong)-1))
			{
				this.ThrowConversionException(value.ToString(NumberFormatInfo.CurrentInfo), "UInt32");
			}
			return (uint)value;
		}

		// Token: 0x06001022 RID: 4130 RVA: 0x0003AFC4 File Offset: 0x000391C4
		internal virtual ulong ReadElementContentAsUnsignedLong()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			string text = this.reader.ReadElementContentAsString();
			if (text == null || text.Length == 0)
			{
				this.ThrowConversionException(string.Empty, "UInt64");
			}
			return XmlConverter.ToUInt64(text);
		}

		// Token: 0x06001023 RID: 4131 RVA: 0x0003B00C File Offset: 0x0003920C
		internal virtual ulong ReadContentAsUnsignedLong()
		{
			string text = this.reader.ReadContentAsString();
			if (text == null || text.Length == 0)
			{
				this.ThrowConversionException(string.Empty, "UInt64");
			}
			return XmlConverter.ToUInt64(text);
		}

		// Token: 0x06001024 RID: 4132 RVA: 0x0003B046 File Offset: 0x00039246
		public ushort ReadElementContentAsUnsignedShort()
		{
			return this.ToUInt16(this.ReadElementContentAsInt());
		}

		// Token: 0x06001025 RID: 4133 RVA: 0x0003B054 File Offset: 0x00039254
		internal ushort ReadContentAsUnsignedShort()
		{
			return this.ToUInt16(this.ReadContentAsInt());
		}

		// Token: 0x06001026 RID: 4134 RVA: 0x0003B062 File Offset: 0x00039262
		private ushort ToUInt16(int value)
		{
			if (value < 0 || value > 65535)
			{
				this.ThrowConversionException(value.ToString(NumberFormatInfo.CurrentInfo), "UInt16");
			}
			return (ushort)value;
		}

		// Token: 0x06001027 RID: 4135 RVA: 0x0003B089 File Offset: 0x00039289
		public TimeSpan ReadElementContentAsTimeSpan()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			return XmlConverter.ToTimeSpan(this.reader.ReadElementContentAsString());
		}

		// Token: 0x06001028 RID: 4136 RVA: 0x0003B0A9 File Offset: 0x000392A9
		internal TimeSpan ReadContentAsTimeSpan()
		{
			return XmlConverter.ToTimeSpan(this.reader.ReadContentAsString());
		}

		// Token: 0x06001029 RID: 4137 RVA: 0x0003B0BC File Offset: 0x000392BC
		public Guid ReadElementContentAsGuid()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			string text = this.reader.ReadElementContentAsString();
			Guid result;
			try
			{
				result = Guid.Parse(text);
			}
			catch (ArgumentException exception)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "Guid", exception));
			}
			catch (FormatException exception2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "Guid", exception2));
			}
			catch (OverflowException exception3)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "Guid", exception3));
			}
			return result;
		}

		// Token: 0x0600102A RID: 4138 RVA: 0x0003B150 File Offset: 0x00039350
		internal Guid ReadContentAsGuid()
		{
			string text = this.reader.ReadContentAsString();
			Guid result;
			try
			{
				result = Guid.Parse(text);
			}
			catch (ArgumentException exception)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "Guid", exception));
			}
			catch (FormatException exception2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "Guid", exception2));
			}
			catch (OverflowException exception3)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "Guid", exception3));
			}
			return result;
		}

		// Token: 0x0600102B RID: 4139 RVA: 0x0003B1D8 File Offset: 0x000393D8
		public Uri ReadElementContentAsUri()
		{
			if (this.isEndOfEmptyElement)
			{
				this.ThrowNotAtElement();
			}
			string text = this.ReadElementContentAsString();
			Uri result;
			try
			{
				result = new Uri(text, UriKind.RelativeOrAbsolute);
			}
			catch (ArgumentException exception)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "Uri", exception));
			}
			catch (FormatException exception2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "Uri", exception2));
			}
			return result;
		}

		// Token: 0x0600102C RID: 4140 RVA: 0x0003B248 File Offset: 0x00039448
		internal Uri ReadContentAsUri()
		{
			string text = this.ReadContentAsString();
			Uri result;
			try
			{
				result = new Uri(text, UriKind.RelativeOrAbsolute);
			}
			catch (ArgumentException exception)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "Uri", exception));
			}
			catch (FormatException exception2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "Uri", exception2));
			}
			return result;
		}

		// Token: 0x0600102D RID: 4141 RVA: 0x0003B2AC File Offset: 0x000394AC
		public XmlQualifiedName ReadElementContentAsQName()
		{
			this.Read();
			XmlQualifiedName result = this.ReadContentAsQName();
			this.ReadEndElement();
			return result;
		}

		// Token: 0x0600102E RID: 4142 RVA: 0x0003B2C1 File Offset: 0x000394C1
		internal virtual XmlQualifiedName ReadContentAsQName()
		{
			return this.ParseQualifiedName(this.ReadContentAsString());
		}

		// Token: 0x0600102F RID: 4143 RVA: 0x0003B2D0 File Offset: 0x000394D0
		private XmlQualifiedName ParseQualifiedName(string str)
		{
			string empty;
			string ns;
			if (str == null || str.Length == 0)
			{
				ns = (empty = string.Empty);
			}
			else
			{
				string text;
				XmlObjectSerializerReadContext.ParseQualifiedName(str, this, out empty, out ns, out text);
			}
			return new XmlQualifiedName(empty, ns);
		}

		// Token: 0x06001030 RID: 4144 RVA: 0x0003B306 File Offset: 0x00039506
		private void CheckExpectedArrayLength(XmlObjectSerializerReadContext context, int arrayLength)
		{
			context.IncrementItemCount(arrayLength);
		}

		// Token: 0x06001031 RID: 4145 RVA: 0x0003B30F File Offset: 0x0003950F
		protected int GetArrayLengthQuota(XmlObjectSerializerReadContext context)
		{
			if (this.dictionaryReader.Quotas == null)
			{
				return context.RemainingItemCount;
			}
			return Math.Min(context.RemainingItemCount, this.dictionaryReader.Quotas.MaxArrayLength);
		}

		// Token: 0x06001032 RID: 4146 RVA: 0x0003B340 File Offset: 0x00039540
		private void CheckActualArrayLength(int expectedLength, int actualLength, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			if (expectedLength != actualLength)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Array length '{0}' provided by Size attribute is not equal to the number of array elements '{1}' from namespace '{2}' found.", new object[]
				{
					expectedLength,
					itemName.Value,
					itemNamespace.Value
				})));
			}
		}

		// Token: 0x06001033 RID: 4147 RVA: 0x0003B380 File Offset: 0x00039580
		internal bool TryReadBooleanArray(XmlObjectSerializerReadContext context, XmlDictionaryString itemName, XmlDictionaryString itemNamespace, int arrayLength, out bool[] array)
		{
			if (this.dictionaryReader == null)
			{
				array = null;
				return false;
			}
			if (arrayLength != -1)
			{
				this.CheckExpectedArrayLength(context, arrayLength);
				array = new bool[arrayLength];
				int num = 0;
				int num2;
				while ((num2 = this.dictionaryReader.ReadArray(itemName, itemNamespace, array, num, arrayLength - num)) > 0)
				{
					num += num2;
				}
				this.CheckActualArrayLength(arrayLength, num, itemName, itemNamespace);
			}
			else
			{
				array = BooleanArrayHelperWithDictionaryString.Instance.ReadArray(this.dictionaryReader, itemName, itemNamespace, this.GetArrayLengthQuota(context));
				context.IncrementItemCount(array.Length);
			}
			return true;
		}

		// Token: 0x06001034 RID: 4148 RVA: 0x0003B40C File Offset: 0x0003960C
		internal bool TryReadDateTimeArray(XmlObjectSerializerReadContext context, XmlDictionaryString itemName, XmlDictionaryString itemNamespace, int arrayLength, out DateTime[] array)
		{
			if (this.dictionaryReader == null)
			{
				array = null;
				return false;
			}
			if (arrayLength != -1)
			{
				this.CheckExpectedArrayLength(context, arrayLength);
				array = new DateTime[arrayLength];
				int num = 0;
				int num2;
				while ((num2 = this.dictionaryReader.ReadArray(itemName, itemNamespace, array, num, arrayLength - num)) > 0)
				{
					num += num2;
				}
				this.CheckActualArrayLength(arrayLength, num, itemName, itemNamespace);
			}
			else
			{
				array = DateTimeArrayHelperWithDictionaryString.Instance.ReadArray(this.dictionaryReader, itemName, itemNamespace, this.GetArrayLengthQuota(context));
				context.IncrementItemCount(array.Length);
			}
			return true;
		}

		// Token: 0x06001035 RID: 4149 RVA: 0x0003B498 File Offset: 0x00039698
		internal bool TryReadDecimalArray(XmlObjectSerializerReadContext context, XmlDictionaryString itemName, XmlDictionaryString itemNamespace, int arrayLength, out decimal[] array)
		{
			if (this.dictionaryReader == null)
			{
				array = null;
				return false;
			}
			if (arrayLength != -1)
			{
				this.CheckExpectedArrayLength(context, arrayLength);
				array = new decimal[arrayLength];
				int num = 0;
				int num2;
				while ((num2 = this.dictionaryReader.ReadArray(itemName, itemNamespace, array, num, arrayLength - num)) > 0)
				{
					num += num2;
				}
				this.CheckActualArrayLength(arrayLength, num, itemName, itemNamespace);
			}
			else
			{
				array = DecimalArrayHelperWithDictionaryString.Instance.ReadArray(this.dictionaryReader, itemName, itemNamespace, this.GetArrayLengthQuota(context));
				context.IncrementItemCount(array.Length);
			}
			return true;
		}

		// Token: 0x06001036 RID: 4150 RVA: 0x0003B524 File Offset: 0x00039724
		internal bool TryReadInt32Array(XmlObjectSerializerReadContext context, XmlDictionaryString itemName, XmlDictionaryString itemNamespace, int arrayLength, out int[] array)
		{
			if (this.dictionaryReader == null)
			{
				array = null;
				return false;
			}
			if (arrayLength != -1)
			{
				this.CheckExpectedArrayLength(context, arrayLength);
				array = new int[arrayLength];
				int num = 0;
				int num2;
				while ((num2 = this.dictionaryReader.ReadArray(itemName, itemNamespace, array, num, arrayLength - num)) > 0)
				{
					num += num2;
				}
				this.CheckActualArrayLength(arrayLength, num, itemName, itemNamespace);
			}
			else
			{
				array = Int32ArrayHelperWithDictionaryString.Instance.ReadArray(this.dictionaryReader, itemName, itemNamespace, this.GetArrayLengthQuota(context));
				context.IncrementItemCount(array.Length);
			}
			return true;
		}

		// Token: 0x06001037 RID: 4151 RVA: 0x0003B5B0 File Offset: 0x000397B0
		internal bool TryReadInt64Array(XmlObjectSerializerReadContext context, XmlDictionaryString itemName, XmlDictionaryString itemNamespace, int arrayLength, out long[] array)
		{
			if (this.dictionaryReader == null)
			{
				array = null;
				return false;
			}
			if (arrayLength != -1)
			{
				this.CheckExpectedArrayLength(context, arrayLength);
				array = new long[arrayLength];
				int num = 0;
				int num2;
				while ((num2 = this.dictionaryReader.ReadArray(itemName, itemNamespace, array, num, arrayLength - num)) > 0)
				{
					num += num2;
				}
				this.CheckActualArrayLength(arrayLength, num, itemName, itemNamespace);
			}
			else
			{
				array = Int64ArrayHelperWithDictionaryString.Instance.ReadArray(this.dictionaryReader, itemName, itemNamespace, this.GetArrayLengthQuota(context));
				context.IncrementItemCount(array.Length);
			}
			return true;
		}

		// Token: 0x06001038 RID: 4152 RVA: 0x0003B63C File Offset: 0x0003983C
		internal bool TryReadSingleArray(XmlObjectSerializerReadContext context, XmlDictionaryString itemName, XmlDictionaryString itemNamespace, int arrayLength, out float[] array)
		{
			if (this.dictionaryReader == null)
			{
				array = null;
				return false;
			}
			if (arrayLength != -1)
			{
				this.CheckExpectedArrayLength(context, arrayLength);
				array = new float[arrayLength];
				int num = 0;
				int num2;
				while ((num2 = this.dictionaryReader.ReadArray(itemName, itemNamespace, array, num, arrayLength - num)) > 0)
				{
					num += num2;
				}
				this.CheckActualArrayLength(arrayLength, num, itemName, itemNamespace);
			}
			else
			{
				array = SingleArrayHelperWithDictionaryString.Instance.ReadArray(this.dictionaryReader, itemName, itemNamespace, this.GetArrayLengthQuota(context));
				context.IncrementItemCount(array.Length);
			}
			return true;
		}

		// Token: 0x06001039 RID: 4153 RVA: 0x0003B6C8 File Offset: 0x000398C8
		internal bool TryReadDoubleArray(XmlObjectSerializerReadContext context, XmlDictionaryString itemName, XmlDictionaryString itemNamespace, int arrayLength, out double[] array)
		{
			if (this.dictionaryReader == null)
			{
				array = null;
				return false;
			}
			if (arrayLength != -1)
			{
				this.CheckExpectedArrayLength(context, arrayLength);
				array = new double[arrayLength];
				int num = 0;
				int num2;
				while ((num2 = this.dictionaryReader.ReadArray(itemName, itemNamespace, array, num, arrayLength - num)) > 0)
				{
					num += num2;
				}
				this.CheckActualArrayLength(arrayLength, num, itemName, itemNamespace);
			}
			else
			{
				array = DoubleArrayHelperWithDictionaryString.Instance.ReadArray(this.dictionaryReader, itemName, itemNamespace, this.GetArrayLengthQuota(context));
				context.IncrementItemCount(array.Length);
			}
			return true;
		}

		// Token: 0x0600103A RID: 4154 RVA: 0x0003B754 File Offset: 0x00039954
		internal IDictionary<string, string> GetNamespacesInScope(XmlNamespaceScope scope)
		{
			if (!(this.reader is IXmlNamespaceResolver))
			{
				return null;
			}
			return ((IXmlNamespaceResolver)this.reader).GetNamespacesInScope(scope);
		}

		// Token: 0x0600103B RID: 4155 RVA: 0x0003B778 File Offset: 0x00039978
		internal bool HasLineInfo()
		{
			IXmlLineInfo xmlLineInfo = this.reader as IXmlLineInfo;
			return xmlLineInfo != null && xmlLineInfo.HasLineInfo();
		}

		// Token: 0x17000385 RID: 901
		// (get) Token: 0x0600103C RID: 4156 RVA: 0x0003B79C File Offset: 0x0003999C
		internal int LineNumber
		{
			get
			{
				IXmlLineInfo xmlLineInfo = this.reader as IXmlLineInfo;
				if (xmlLineInfo != null)
				{
					return xmlLineInfo.LineNumber;
				}
				return 0;
			}
		}

		// Token: 0x17000386 RID: 902
		// (get) Token: 0x0600103D RID: 4157 RVA: 0x0003B7C0 File Offset: 0x000399C0
		internal int LinePosition
		{
			get
			{
				IXmlLineInfo xmlLineInfo = this.reader as IXmlLineInfo;
				if (xmlLineInfo != null)
				{
					return xmlLineInfo.LinePosition;
				}
				return 0;
			}
		}

		// Token: 0x17000387 RID: 903
		// (get) Token: 0x0600103E RID: 4158 RVA: 0x0003B7E4 File Offset: 0x000399E4
		// (set) Token: 0x0600103F RID: 4159 RVA: 0x0003B820 File Offset: 0x00039A20
		internal bool Normalized
		{
			get
			{
				XmlTextReader xmlTextReader = this.reader as XmlTextReader;
				if (xmlTextReader == null)
				{
					IXmlTextParser xmlTextParser = this.reader as IXmlTextParser;
					return xmlTextParser != null && xmlTextParser.Normalized;
				}
				return xmlTextReader.Normalization;
			}
			set
			{
				XmlTextReader xmlTextReader = this.reader as XmlTextReader;
				if (xmlTextReader == null)
				{
					IXmlTextParser xmlTextParser = this.reader as IXmlTextParser;
					if (xmlTextParser != null)
					{
						xmlTextParser.Normalized = value;
						return;
					}
				}
				else
				{
					xmlTextReader.Normalization = value;
				}
			}
		}

		// Token: 0x17000388 RID: 904
		// (get) Token: 0x06001040 RID: 4160 RVA: 0x0003B85C File Offset: 0x00039A5C
		// (set) Token: 0x06001041 RID: 4161 RVA: 0x0003B898 File Offset: 0x00039A98
		internal WhitespaceHandling WhitespaceHandling
		{
			get
			{
				XmlTextReader xmlTextReader = this.reader as XmlTextReader;
				if (xmlTextReader != null)
				{
					return xmlTextReader.WhitespaceHandling;
				}
				IXmlTextParser xmlTextParser = this.reader as IXmlTextParser;
				if (xmlTextParser != null)
				{
					return xmlTextParser.WhitespaceHandling;
				}
				return WhitespaceHandling.None;
			}
			set
			{
				XmlTextReader xmlTextReader = this.reader as XmlTextReader;
				if (xmlTextReader == null)
				{
					IXmlTextParser xmlTextParser = this.reader as IXmlTextParser;
					if (xmlTextParser != null)
					{
						xmlTextParser.WhitespaceHandling = value;
						return;
					}
				}
				else
				{
					xmlTextReader.WhitespaceHandling = value;
				}
			}
		}

		// Token: 0x17000389 RID: 905
		// (get) Token: 0x06001042 RID: 4162 RVA: 0x0003B8D2 File Offset: 0x00039AD2
		internal string Name
		{
			get
			{
				return this.reader.Name;
			}
		}

		// Token: 0x1700038A RID: 906
		// (get) Token: 0x06001043 RID: 4163 RVA: 0x0003B8DF File Offset: 0x00039ADF
		public string LocalName
		{
			get
			{
				return this.reader.LocalName;
			}
		}

		// Token: 0x1700038B RID: 907
		// (get) Token: 0x06001044 RID: 4164 RVA: 0x0003B8EC File Offset: 0x00039AEC
		internal string NamespaceURI
		{
			get
			{
				return this.reader.NamespaceURI;
			}
		}

		// Token: 0x1700038C RID: 908
		// (get) Token: 0x06001045 RID: 4165 RVA: 0x0003B8F9 File Offset: 0x00039AF9
		internal string Value
		{
			get
			{
				return this.reader.Value;
			}
		}

		// Token: 0x1700038D RID: 909
		// (get) Token: 0x06001046 RID: 4166 RVA: 0x0003B906 File Offset: 0x00039B06
		internal Type ValueType
		{
			get
			{
				return this.reader.ValueType;
			}
		}

		// Token: 0x1700038E RID: 910
		// (get) Token: 0x06001047 RID: 4167 RVA: 0x0003B913 File Offset: 0x00039B13
		internal int Depth
		{
			get
			{
				return this.reader.Depth;
			}
		}

		// Token: 0x06001048 RID: 4168 RVA: 0x0003B920 File Offset: 0x00039B20
		internal string LookupNamespace(string prefix)
		{
			return this.reader.LookupNamespace(prefix);
		}

		// Token: 0x1700038F RID: 911
		// (get) Token: 0x06001049 RID: 4169 RVA: 0x0003B92E File Offset: 0x00039B2E
		internal bool EOF
		{
			get
			{
				return this.reader.EOF;
			}
		}

		// Token: 0x0600104A RID: 4170 RVA: 0x0003B93B File Offset: 0x00039B3B
		internal void Skip()
		{
			this.reader.Skip();
			this.isEndOfEmptyElement = false;
		}

		// Token: 0x040006EE RID: 1774
		protected XmlReader reader;

		// Token: 0x040006EF RID: 1775
		protected XmlDictionaryReader dictionaryReader;

		// Token: 0x040006F0 RID: 1776
		protected bool isEndOfEmptyElement;
	}
}
