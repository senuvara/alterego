﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000117 RID: 279
	internal class ByteArrayDataContract : PrimitiveDataContract
	{
		// Token: 0x06000DC7 RID: 3527 RVA: 0x000338F0 File Offset: 0x00031AF0
		internal ByteArrayDataContract() : base(typeof(byte[]), DictionaryGlobals.ByteArrayLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002EC RID: 748
		// (get) Token: 0x06000DC8 RID: 3528 RVA: 0x0003390C File Offset: 0x00031B0C
		internal override string WriteMethodName
		{
			get
			{
				return "WriteBase64";
			}
		}

		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06000DC9 RID: 3529 RVA: 0x00033913 File Offset: 0x00031B13
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsBase64";
			}
		}

		// Token: 0x06000DCA RID: 3530 RVA: 0x0003391A File Offset: 0x00031B1A
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteBase64((byte[])obj);
		}

		// Token: 0x06000DCB RID: 3531 RVA: 0x00033928 File Offset: 0x00031B28
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsBase64(), context);
			}
			if (!base.TryReadNullAtTopLevel(reader))
			{
				return reader.ReadElementContentAsBase64();
			}
			return null;
		}
	}
}
