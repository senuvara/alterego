﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x02000153 RID: 339
	internal class CodeTypeReference
	{
		// Token: 0x06001139 RID: 4409 RVA: 0x0003D9A5 File Offset: 0x0003BBA5
		public CodeTypeReference()
		{
			this.baseType = string.Empty;
			this.arrayRank = 0;
			this.arrayElementType = null;
		}

		// Token: 0x0600113A RID: 4410 RVA: 0x0003D9C8 File Offset: 0x0003BBC8
		public CodeTypeReference(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (type.IsArray)
			{
				this.arrayRank = type.GetArrayRank();
				this.arrayElementType = new CodeTypeReference(type.GetElementType());
				this.baseType = null;
			}
			else
			{
				this.InitializeFromType(type);
				this.arrayRank = 0;
				this.arrayElementType = null;
			}
			this.isInterface = type.IsInterface;
		}

		// Token: 0x0600113B RID: 4411 RVA: 0x0003DA3E File Offset: 0x0003BC3E
		public CodeTypeReference(Type type, CodeTypeReferenceOptions codeTypeReferenceOption) : this(type)
		{
			this.referenceOptions = codeTypeReferenceOption;
		}

		// Token: 0x0600113C RID: 4412 RVA: 0x0003DA4E File Offset: 0x0003BC4E
		public CodeTypeReference(string typeName, CodeTypeReferenceOptions codeTypeReferenceOption)
		{
			this.Initialize(typeName, codeTypeReferenceOption);
		}

		// Token: 0x0600113D RID: 4413 RVA: 0x0003DA5E File Offset: 0x0003BC5E
		public CodeTypeReference(string typeName)
		{
			this.Initialize(typeName);
		}

		// Token: 0x0600113E RID: 4414 RVA: 0x0003DA70 File Offset: 0x0003BC70
		private void InitializeFromType(Type type)
		{
			this.baseType = type.Name;
			if (!type.IsGenericParameter)
			{
				Type type2 = type;
				while (type2.IsNested)
				{
					type2 = type2.DeclaringType;
					this.baseType = type2.Name + "+" + this.baseType;
				}
				if (!string.IsNullOrEmpty(type.Namespace))
				{
					this.baseType = type.Namespace + "." + this.baseType;
				}
			}
			if (type.IsGenericType && !type.ContainsGenericParameters)
			{
				Type[] genericArguments = type.GetGenericArguments();
				for (int i = 0; i < genericArguments.Length; i++)
				{
					this.TypeArguments.Add(new CodeTypeReference(genericArguments[i]));
				}
				return;
			}
			if (!type.IsGenericTypeDefinition)
			{
				this.needsFixup = true;
			}
		}

		// Token: 0x0600113F RID: 4415 RVA: 0x0003DB31 File Offset: 0x0003BD31
		private void Initialize(string typeName)
		{
			this.Initialize(typeName, this.referenceOptions);
		}

		// Token: 0x06001140 RID: 4416 RVA: 0x0003DB40 File Offset: 0x0003BD40
		private void Initialize(string typeName, CodeTypeReferenceOptions options)
		{
			this.Options = options;
			if (typeName == null || typeName.Length == 0)
			{
				typeName = typeof(void).FullName;
				this.baseType = typeName;
				this.arrayRank = 0;
				this.arrayElementType = null;
				return;
			}
			typeName = this.RipOffAssemblyInformationFromTypeName(typeName);
			int num = typeName.Length - 1;
			int i = num;
			this.needsFixup = true;
			Queue queue = new Queue();
			while (i >= 0)
			{
				int num2 = 1;
				if (typeName[i--] != ']')
				{
					break;
				}
				while (i >= 0 && typeName[i] == ',')
				{
					num2++;
					i--;
				}
				if (i < 0 || typeName[i] != '[')
				{
					break;
				}
				queue.Enqueue(num2);
				i--;
				num = i;
			}
			i = num;
			ArrayList arrayList = new ArrayList();
			Stack stack = new Stack();
			if (i > 0 && typeName[i--] == ']')
			{
				this.needsFixup = false;
				int num3 = 1;
				int num4 = num;
				while (i >= 0)
				{
					if (typeName[i] == '[')
					{
						if (--num3 == 0)
						{
							break;
						}
					}
					else if (typeName[i] == ']')
					{
						num3++;
					}
					else if (typeName[i] == ',' && num3 == 1)
					{
						if (i + 1 < num4)
						{
							stack.Push(typeName.Substring(i + 1, num4 - i - 1));
						}
						num4 = i;
					}
					i--;
				}
				if (i > 0 && num - i - 1 > 0)
				{
					if (i + 1 < num4)
					{
						stack.Push(typeName.Substring(i + 1, num4 - i - 1));
					}
					while (stack.Count > 0)
					{
						string typeName2 = this.RipOffAssemblyInformationFromTypeName((string)stack.Pop());
						arrayList.Add(new CodeTypeReference(typeName2));
					}
					num = i - 1;
				}
			}
			if (num < 0)
			{
				this.baseType = typeName;
				return;
			}
			if (queue.Count > 0)
			{
				CodeTypeReference codeTypeReference = new CodeTypeReference(typeName.Substring(0, num + 1), this.Options);
				for (int j = 0; j < arrayList.Count; j++)
				{
					codeTypeReference.TypeArguments.Add((CodeTypeReference)arrayList[j]);
				}
				while (queue.Count > 1)
				{
					codeTypeReference = new CodeTypeReference(codeTypeReference, (int)queue.Dequeue());
				}
				this.baseType = null;
				this.arrayRank = (int)queue.Dequeue();
				this.arrayElementType = codeTypeReference;
			}
			else if (arrayList.Count > 0)
			{
				for (int k = 0; k < arrayList.Count; k++)
				{
					this.TypeArguments.Add((CodeTypeReference)arrayList[k]);
				}
				this.baseType = typeName.Substring(0, num + 1);
			}
			else
			{
				this.baseType = typeName;
			}
			if (this.baseType != null && this.baseType.IndexOf('`') != -1)
			{
				this.needsFixup = false;
			}
		}

		// Token: 0x06001141 RID: 4417 RVA: 0x0003DDFB File Offset: 0x0003BFFB
		public CodeTypeReference(string typeName, params CodeTypeReference[] typeArguments) : this(typeName)
		{
			if (typeArguments != null && typeArguments.Length != 0)
			{
				this.TypeArguments.AddRange(typeArguments);
			}
		}

		// Token: 0x06001142 RID: 4418 RVA: 0x0003DE17 File Offset: 0x0003C017
		public CodeTypeReference(string baseType, int rank)
		{
			this.baseType = null;
			this.arrayRank = rank;
			this.arrayElementType = new CodeTypeReference(baseType);
		}

		// Token: 0x06001143 RID: 4419 RVA: 0x0003DE39 File Offset: 0x0003C039
		public CodeTypeReference(CodeTypeReference arrayType, int rank)
		{
			this.baseType = null;
			this.arrayRank = rank;
			this.arrayElementType = arrayType;
		}

		// Token: 0x170003BB RID: 955
		// (get) Token: 0x06001144 RID: 4420 RVA: 0x0003DE56 File Offset: 0x0003C056
		// (set) Token: 0x06001145 RID: 4421 RVA: 0x0003DE5E File Offset: 0x0003C05E
		public CodeTypeReference ArrayElementType
		{
			get
			{
				return this.arrayElementType;
			}
			set
			{
				this.arrayElementType = value;
			}
		}

		// Token: 0x170003BC RID: 956
		// (get) Token: 0x06001146 RID: 4422 RVA: 0x0003DE67 File Offset: 0x0003C067
		// (set) Token: 0x06001147 RID: 4423 RVA: 0x0003DE6F File Offset: 0x0003C06F
		public int ArrayRank
		{
			get
			{
				return this.arrayRank;
			}
			set
			{
				this.arrayRank = value;
			}
		}

		// Token: 0x170003BD RID: 957
		// (get) Token: 0x06001148 RID: 4424 RVA: 0x0003DE78 File Offset: 0x0003C078
		internal int NestedArrayDepth
		{
			get
			{
				if (this.arrayElementType == null)
				{
					return 0;
				}
				return 1 + this.arrayElementType.NestedArrayDepth;
			}
		}

		// Token: 0x170003BE RID: 958
		// (get) Token: 0x06001149 RID: 4425 RVA: 0x0003DE94 File Offset: 0x0003C094
		// (set) Token: 0x0600114A RID: 4426 RVA: 0x0003DF13 File Offset: 0x0003C113
		public string BaseType
		{
			get
			{
				if (this.arrayRank > 0 && this.arrayElementType != null)
				{
					return this.arrayElementType.BaseType;
				}
				if (string.IsNullOrEmpty(this.baseType))
				{
					return string.Empty;
				}
				string text = this.baseType;
				if (this.needsFixup && this.TypeArguments.Count > 0)
				{
					text = text + "`" + this.TypeArguments.Count.ToString(CultureInfo.InvariantCulture);
				}
				return text;
			}
			set
			{
				this.baseType = value;
				this.Initialize(this.baseType);
			}
		}

		// Token: 0x170003BF RID: 959
		// (get) Token: 0x0600114B RID: 4427 RVA: 0x0003DF28 File Offset: 0x0003C128
		// (set) Token: 0x0600114C RID: 4428 RVA: 0x0003DF30 File Offset: 0x0003C130
		[ComVisible(false)]
		public CodeTypeReferenceOptions Options
		{
			get
			{
				return this.referenceOptions;
			}
			set
			{
				this.referenceOptions = value;
			}
		}

		// Token: 0x170003C0 RID: 960
		// (get) Token: 0x0600114D RID: 4429 RVA: 0x0003DF39 File Offset: 0x0003C139
		[ComVisible(false)]
		public List<CodeTypeReference> TypeArguments
		{
			get
			{
				if (this.arrayRank > 0 && this.arrayElementType != null)
				{
					return this.arrayElementType.TypeArguments;
				}
				if (this.typeArguments == null)
				{
					this.typeArguments = new List<CodeTypeReference>();
				}
				return this.typeArguments;
			}
		}

		// Token: 0x170003C1 RID: 961
		// (get) Token: 0x0600114E RID: 4430 RVA: 0x0003DF71 File Offset: 0x0003C171
		internal bool IsInterface
		{
			get
			{
				return this.isInterface;
			}
		}

		// Token: 0x0600114F RID: 4431 RVA: 0x0003DF7C File Offset: 0x0003C17C
		private string RipOffAssemblyInformationFromTypeName(string typeName)
		{
			int i = 0;
			int num = typeName.Length - 1;
			string result = typeName;
			while (i < typeName.Length)
			{
				if (!char.IsWhiteSpace(typeName[i]))
				{
					break;
				}
				i++;
			}
			while (num >= 0 && char.IsWhiteSpace(typeName[num]))
			{
				num--;
			}
			if (i < num)
			{
				if (typeName[i] == '[' && typeName[num] == ']')
				{
					i++;
					num--;
				}
				if (typeName[num] != ']')
				{
					int num2 = 0;
					for (int j = num; j >= i; j--)
					{
						if (typeName[j] == ',')
						{
							num2++;
							if (num2 == 4)
							{
								result = typeName.Substring(i, j - i);
								break;
							}
						}
					}
				}
			}
			return result;
		}

		// Token: 0x04000915 RID: 2325
		private string baseType;

		// Token: 0x04000916 RID: 2326
		[OptionalField]
		private bool isInterface;

		// Token: 0x04000917 RID: 2327
		private int arrayRank;

		// Token: 0x04000918 RID: 2328
		private CodeTypeReference arrayElementType;

		// Token: 0x04000919 RID: 2329
		[OptionalField]
		private List<CodeTypeReference> typeArguments;

		// Token: 0x0400091A RID: 2330
		[OptionalField]
		private CodeTypeReferenceOptions referenceOptions;

		// Token: 0x0400091B RID: 2331
		[OptionalField]
		private bool needsFixup;
	}
}
