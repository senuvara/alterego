﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using Unity;

namespace System.Runtime.Serialization
{
	/// <summary>Allows the transformation of a set of XML schema files (.xsd) into common language runtime (CLR) types. </summary>
	// Token: 0x020001A6 RID: 422
	public class XsdDataContractImporter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.XsdDataContractImporter" /> class.  </summary>
		// Token: 0x0600141F RID: 5151 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public XsdDataContractImporter()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.XsdDataContractImporter" /> class with the <see cref="T:System.CodeDom.CodeCompileUnit" /> that will be used to generate CLR code. </summary>
		/// <param name="codeCompileUnit">The <see cref="T:System.CodeDom.CodeCompileUnit" /> that will be used to store the code. </param>
		// Token: 0x06001420 RID: 5152 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public XsdDataContractImporter(CodeCompileUnit codeCompileUnit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.CodeDom.CodeCompileUnit" /> used for storing the CLR types generated.</summary>
		/// <returns>A <see cref="T:System.CodeDom.CodeCompileUnit" /> used to store the CLR types generated.</returns>
		// Token: 0x17000421 RID: 1057
		// (get) Token: 0x06001421 RID: 5153 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public CodeCompileUnit CodeCompileUnit
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets an <see cref="T:System.Runtime.Serialization.ImportOptions" /> that contains settable options for the import operation. </summary>
		/// <returns>A <see cref="T:System.Runtime.Serialization.ImportOptions" /> that contains settable options. </returns>
		// Token: 0x17000422 RID: 1058
		// (get) Token: 0x06001422 RID: 5154 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		// (set) Token: 0x06001423 RID: 5155 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public ImportOptions Options
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value that indicates whether the schemas contained in an <see cref="T:System.Xml.Schema.XmlSchemaSet" /> can be transformed into a <see cref="T:System.CodeDom.CodeCompileUnit" />. </summary>
		/// <param name="schemas">A <see cref="T:System.Xml.Schema.XmlSchemaSet" /> that contains the schemas to transform. </param>
		/// <returns>
		///     <see langword="true" /> if the schemas can be transformed to data contract types; otherwise, <see langword="false" />. </returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="schemas" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">A data contract involved in the import is invalid.</exception>
		// Token: 0x06001424 RID: 5156 RVA: 0x0004AD30 File Offset: 0x00048F30
		public bool CanImport(XmlSchemaSet schemas)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets a value that indicates whether the specified set of types contained in an <see cref="T:System.Xml.Schema.XmlSchemaSet" /> can be transformed into CLR types generated into a <see cref="T:System.CodeDom.CodeCompileUnit" />.</summary>
		/// <param name="schemas">A <see cref="T:System.Xml.Schema.XmlSchemaSet" /> that contains the schemas to transform.</param>
		/// <param name="typeNames">An <see cref="T:System.Collections.Generic.ICollection`1" /> of <see cref="T:System.Xml.XmlQualifiedName" /> that represents the set of schema types to import.</param>
		/// <returns>
		///     <see langword="true" /> if the schemas can be transformed; otherwise, <see langword="false" />. </returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="schemas" /> or <paramref name="typeNames" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">A data contract involved in the import is invalid.</exception>
		// Token: 0x06001425 RID: 5157 RVA: 0x0004AD4C File Offset: 0x00048F4C
		public bool CanImport(XmlSchemaSet schemas, ICollection<XmlQualifiedName> typeNames)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets a value that indicates whether a specific schema element contained in an <see cref="T:System.Xml.Schema.XmlSchemaSet" /> can be imported.</summary>
		/// <param name="schemas">An <see cref="T:System.Xml.Schema.XmlSchemaSet" /> to import.</param>
		/// <param name="element">A specific <see cref="T:System.Xml.Schema.XmlSchemaElement" /> to check in the set of schemas.</param>
		/// <returns>
		///     <see langword="true" /> if the element can be imported; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="schemas" /> or <paramref name="element" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">A data contract involved in the import is invalid.</exception>
		// Token: 0x06001426 RID: 5158 RVA: 0x0004AD68 File Offset: 0x00048F68
		public bool CanImport(XmlSchemaSet schemas, XmlSchemaElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets a value that indicates whether the schemas contained in an <see cref="T:System.Xml.Schema.XmlSchemaSet" /> can be transformed into a <see cref="T:System.CodeDom.CodeCompileUnit" />. </summary>
		/// <param name="schemas">A <see cref="T:System.Xml.Schema.XmlSchemaSet" /> that contains the schema representations. </param>
		/// <param name="typeName">An <see cref="T:System.Collections.IList" /> of <see cref="T:System.Xml.XmlQualifiedName" /> that specifies the names of the schema types that need to be imported from the <see cref="T:System.Xml.Schema.XmlSchemaSet" />.</param>
		/// <returns>
		///     <see langword="true" /> if the schemas can be transformed to data contract types; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="schemas" /> or <paramref name="typeName" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">A data contract involved in the import is invalid.</exception>
		// Token: 0x06001427 RID: 5159 RVA: 0x0004AD84 File Offset: 0x00048F84
		public bool CanImport(XmlSchemaSet schemas, XmlQualifiedName typeName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a <see cref="T:System.CodeDom.CodeTypeReference" /> to the CLR type generated for the schema type with the specified <see cref="T:System.Xml.XmlQualifiedName" />.</summary>
		/// <param name="typeName">The <see cref="T:System.Xml.XmlQualifiedName" /> that specifies the schema type to look up.</param>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> reference to the CLR type generated for the schema type with the <paramref name="typeName" /> specified.</returns>
		// Token: 0x06001428 RID: 5160 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public CodeTypeReference GetCodeTypeReference(XmlQualifiedName typeName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a <see cref="T:System.CodeDom.CodeTypeReference" /> for the specified XML qualified element and schema element.</summary>
		/// <param name="typeName">An <see cref="T:System.Xml.XmlQualifiedName" /> that specifies the XML qualified name of the schema type to look up.</param>
		/// <param name="element">An <see cref="T:System.Xml.Schema.XmlSchemaElement" /> that specifies an element in an XML schema.</param>
		/// <returns>A <see cref="T:System.CodeDom.CodeTypeReference" /> that represents the type that was generated for the specified schema type.</returns>
		// Token: 0x06001429 RID: 5161 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public CodeTypeReference GetCodeTypeReference(XmlQualifiedName typeName, XmlSchemaElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a list of <see cref="T:System.CodeDom.CodeTypeReference" /> objects that represents the known types generated when generating code for the specified schema type.</summary>
		/// <param name="typeName">An <see cref="T:System.Xml.XmlQualifiedName" /> that represents the schema type to look up known types for.</param>
		/// <returns>A <see cref="T:System.Collections.Generic.IList`1" /> of type <see cref="T:System.CodeDom.CodeTypeReference" />.</returns>
		// Token: 0x0600142A RID: 5162 RVA: 0x0004AD27 File Offset: 0x00048F27
		public ICollection<CodeTypeReference> GetKnownTypeReferences(XmlQualifiedName typeName)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Transforms the specified set of XML schemas contained in an <see cref="T:System.Xml.Schema.XmlSchemaSet" /> into a <see cref="T:System.CodeDom.CodeCompileUnit" />. </summary>
		/// <param name="schemas">A <see cref="T:System.Xml.Schema.XmlSchemaSet" /> that contains the schema representations to generate CLR types for.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="schemas" /> parameter is <see langword="null" />.</exception>
		// Token: 0x0600142B RID: 5163 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Import(XmlSchemaSet schemas)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Transforms the specified set of schema types contained in an <see cref="T:System.Xml.Schema.XmlSchemaSet" /> into CLR types generated into a <see cref="T:System.CodeDom.CodeCompileUnit" />.</summary>
		/// <param name="schemas">A <see cref="T:System.Xml.Schema.XmlSchemaSet" /> that contains the schema representations.</param>
		/// <param name="typeNames">A <see cref="T:System.Collections.Generic.ICollection`1" />  (of <see cref="T:System.Xml.XmlQualifiedName" />) that represents the set of schema types to import.</param>
		// Token: 0x0600142C RID: 5164 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Import(XmlSchemaSet schemas, ICollection<XmlQualifiedName> typeNames)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Transforms the specified schema element in the set of specified XML schemas into a <see cref="T:System.CodeDom.CodeCompileUnit" /> and returns an <see cref="T:System.Xml.XmlQualifiedName" /> that represents the data contract name for the specified element.</summary>
		/// <param name="schemas">An <see cref="T:System.Xml.Schema.XmlSchemaSet" /> that contains the schemas to transform.</param>
		/// <param name="element">An <see cref="T:System.Xml.Schema.XmlSchemaElement" /> that represents the specific schema element to transform. </param>
		/// <returns>An <see cref="T:System.Xml.XmlQualifiedName" /> that represents the specified element.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="schemas" /> or <paramref name="element" /> parameter is <see langword="null" />.</exception>
		// Token: 0x0600142D RID: 5165 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public XmlQualifiedName Import(XmlSchemaSet schemas, XmlSchemaElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Transforms the specified XML schema type contained in an <see cref="T:System.Xml.Schema.XmlSchemaSet" /> into a <see cref="T:System.CodeDom.CodeCompileUnit" />.</summary>
		/// <param name="schemas">A <see cref="T:System.Xml.Schema.XmlSchemaSet" /> that contains the schema representations. </param>
		/// <param name="typeName">A <see cref="T:System.Xml.XmlQualifiedName" /> that represents a specific schema type to import.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="schemas" /> or <paramref name="typeName" /> parameter is <see langword="null" />.</exception>
		// Token: 0x0600142E RID: 5166 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Import(XmlSchemaSet schemas, XmlQualifiedName typeName)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
