﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x0200011D RID: 285
	internal class UriDataContract : PrimitiveDataContract
	{
		// Token: 0x06000DE1 RID: 3553 RVA: 0x00033AEE File Offset: 0x00031CEE
		internal UriDataContract() : base(typeof(Uri), DictionaryGlobals.UriLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x06000DE2 RID: 3554 RVA: 0x00033B0A File Offset: 0x00031D0A
		internal override string WriteMethodName
		{
			get
			{
				return "WriteUri";
			}
		}

		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x06000DE3 RID: 3555 RVA: 0x00033B11 File Offset: 0x00031D11
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsUri";
			}
		}

		// Token: 0x06000DE4 RID: 3556 RVA: 0x00033B18 File Offset: 0x00031D18
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteUri((Uri)obj);
		}

		// Token: 0x06000DE5 RID: 3557 RVA: 0x00033B26 File Offset: 0x00031D26
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsUri(), context);
			}
			if (!base.TryReadNullAtTopLevel(reader))
			{
				return reader.ReadElementContentAsUri();
			}
			return null;
		}
	}
}
