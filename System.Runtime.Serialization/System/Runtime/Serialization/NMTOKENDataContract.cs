﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000115 RID: 277
	internal class NMTOKENDataContract : StringDataContract
	{
		// Token: 0x06000DC5 RID: 3525 RVA: 0x000338CC File Offset: 0x00031ACC
		internal NMTOKENDataContract() : base(DictionaryGlobals.NMTOKENLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
