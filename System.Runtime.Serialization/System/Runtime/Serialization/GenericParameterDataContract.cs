﻿using System;
using System.Collections.Generic;
using System.Security;

namespace System.Runtime.Serialization
{
	// Token: 0x020000DD RID: 221
	internal sealed class GenericParameterDataContract : DataContract
	{
		// Token: 0x06000C60 RID: 3168 RVA: 0x00030A2B File Offset: 0x0002EC2B
		[SecuritySafeCritical]
		internal GenericParameterDataContract(Type type) : base(new GenericParameterDataContract.GenericParameterDataContractCriticalHelper(type))
		{
			this.helper = (base.Helper as GenericParameterDataContract.GenericParameterDataContractCriticalHelper);
		}

		// Token: 0x1700023D RID: 573
		// (get) Token: 0x06000C61 RID: 3169 RVA: 0x00030A4A File Offset: 0x0002EC4A
		internal int ParameterPosition
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.ParameterPosition;
			}
		}

		// Token: 0x1700023E RID: 574
		// (get) Token: 0x06000C62 RID: 3170 RVA: 0x000066D0 File Offset: 0x000048D0
		internal override bool IsBuiltInDataContract
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000C63 RID: 3171 RVA: 0x00030A57 File Offset: 0x0002EC57
		internal override DataContract BindGenericParameters(DataContract[] paramContracts, Dictionary<DataContract, DataContract> boundContracts)
		{
			return paramContracts[this.ParameterPosition];
		}

		// Token: 0x04000545 RID: 1349
		[SecurityCritical]
		private GenericParameterDataContract.GenericParameterDataContractCriticalHelper helper;

		// Token: 0x020000DE RID: 222
		private class GenericParameterDataContractCriticalHelper : DataContract.DataContractCriticalHelper
		{
			// Token: 0x06000C64 RID: 3172 RVA: 0x00030A61 File Offset: 0x0002EC61
			internal GenericParameterDataContractCriticalHelper(Type type) : base(type)
			{
				base.SetDataContractName(DataContract.GetStableName(type));
				this.parameterPosition = type.GenericParameterPosition;
			}

			// Token: 0x1700023F RID: 575
			// (get) Token: 0x06000C65 RID: 3173 RVA: 0x00030A82 File Offset: 0x0002EC82
			internal int ParameterPosition
			{
				get
				{
					return this.parameterPosition;
				}
			}

			// Token: 0x04000546 RID: 1350
			private int parameterPosition;
		}
	}
}
