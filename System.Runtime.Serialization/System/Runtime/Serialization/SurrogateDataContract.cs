﻿using System;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Permissions;

namespace System.Runtime.Serialization
{
	// Token: 0x02000124 RID: 292
	internal sealed class SurrogateDataContract : DataContract
	{
		// Token: 0x06000DF9 RID: 3577 RVA: 0x00033D17 File Offset: 0x00031F17
		[SecuritySafeCritical]
		internal SurrogateDataContract(Type type, ISerializationSurrogate serializationSurrogate) : base(new SurrogateDataContract.SurrogateDataContractCriticalHelper(type, serializationSurrogate))
		{
			this.helper = (base.Helper as SurrogateDataContract.SurrogateDataContractCriticalHelper);
		}

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06000DFA RID: 3578 RVA: 0x00033D37 File Offset: 0x00031F37
		internal ISerializationSurrogate SerializationSurrogate
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.SerializationSurrogate;
			}
		}

		// Token: 0x06000DFB RID: 3579 RVA: 0x00033D44 File Offset: 0x00031F44
		public override void WriteXmlValue(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContext context)
		{
			SerializationInfo serInfo = new SerializationInfo(base.UnderlyingType, XmlObjectSerializer.FormatterConverter, !context.UnsafeTypeForwardingEnabled);
			this.SerializationSurrogateGetObjectData(obj, serInfo, context.GetStreamingContext());
			context.WriteSerializationInfo(xmlWriter, base.UnderlyingType, serInfo);
		}

		// Token: 0x06000DFC RID: 3580 RVA: 0x00033D87 File Offset: 0x00031F87
		[SecuritySafeCritical]
		[PermissionSet(SecurityAction.Demand, Unrestricted = true)]
		[MethodImpl(MethodImplOptions.NoInlining)]
		private object SerializationSurrogateSetObjectData(object obj, SerializationInfo serInfo, StreamingContext context)
		{
			return this.SerializationSurrogate.SetObjectData(obj, serInfo, context, null);
		}

		// Token: 0x06000DFD RID: 3581 RVA: 0x00033D98 File Offset: 0x00031F98
		[SecuritySafeCritical]
		[PermissionSet(SecurityAction.Demand, Unrestricted = true)]
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal static object GetRealObject(IObjectReference obj, StreamingContext context)
		{
			return obj.GetRealObject(context);
		}

		// Token: 0x06000DFE RID: 3582 RVA: 0x00033DA1 File Offset: 0x00031FA1
		[SecuritySafeCritical]
		[PermissionSet(SecurityAction.Demand, Unrestricted = true)]
		[MethodImpl(MethodImplOptions.NoInlining)]
		private object GetUninitializedObject(Type objType)
		{
			return FormatterServices.GetUninitializedObject(objType);
		}

		// Token: 0x06000DFF RID: 3583 RVA: 0x00033DA9 File Offset: 0x00031FA9
		[SecuritySafeCritical]
		[PermissionSet(SecurityAction.Demand, Unrestricted = true)]
		[MethodImpl(MethodImplOptions.NoInlining)]
		private void SerializationSurrogateGetObjectData(object obj, SerializationInfo serInfo, StreamingContext context)
		{
			this.SerializationSurrogate.GetObjectData(obj, serInfo, context);
		}

		// Token: 0x06000E00 RID: 3584 RVA: 0x00033DBC File Offset: 0x00031FBC
		public override object ReadXmlValue(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContext context)
		{
			xmlReader.Read();
			Type underlyingType = base.UnderlyingType;
			object obj = underlyingType.IsArray ? Array.CreateInstance(underlyingType.GetElementType(), 0) : this.GetUninitializedObject(underlyingType);
			context.AddNewObject(obj);
			string objectId = context.GetObjectId();
			SerializationInfo serInfo = context.ReadSerializationInfo(xmlReader, underlyingType);
			object obj2 = this.SerializationSurrogateSetObjectData(obj, serInfo, context.GetStreamingContext());
			if (obj2 == null)
			{
				obj2 = obj;
			}
			if (obj2 is IDeserializationCallback)
			{
				((IDeserializationCallback)obj2).OnDeserialization(null);
			}
			if (obj2 is IObjectReference)
			{
				obj2 = SurrogateDataContract.GetRealObject((IObjectReference)obj2, context.GetStreamingContext());
			}
			context.ReplaceDeserializedObject(objectId, obj, obj2);
			xmlReader.ReadEndElement();
			return obj2;
		}

		// Token: 0x04000649 RID: 1609
		[SecurityCritical]
		private SurrogateDataContract.SurrogateDataContractCriticalHelper helper;

		// Token: 0x02000125 RID: 293
		private class SurrogateDataContractCriticalHelper : DataContract.DataContractCriticalHelper
		{
			// Token: 0x06000E01 RID: 3585 RVA: 0x00033E68 File Offset: 0x00032068
			internal SurrogateDataContractCriticalHelper(Type type, ISerializationSurrogate serializationSurrogate) : base(type)
			{
				this.serializationSurrogate = serializationSurrogate;
				string localName;
				string ns;
				DataContract.GetDefaultStableName(DataContract.GetClrTypeFullName(type), out localName, out ns);
				base.SetDataContractName(DataContract.CreateQualifiedName(localName, ns));
			}

			// Token: 0x170002FF RID: 767
			// (get) Token: 0x06000E02 RID: 3586 RVA: 0x00033E9F File Offset: 0x0003209F
			internal ISerializationSurrogate SerializationSurrogate
			{
				get
				{
					return this.serializationSurrogate;
				}
			}

			// Token: 0x0400064A RID: 1610
			private ISerializationSurrogate serializationSurrogate;
		}
	}
}
