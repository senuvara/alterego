﻿using System;

namespace System.Runtime.Serialization
{
	/// <summary>Specifies how often to emit type information.</summary>
	// Token: 0x020000CB RID: 203
	public enum EmitTypeInformation
	{
		/// <summary>As needed emit type information.</summary>
		// Token: 0x040004F4 RID: 1268
		AsNeeded,
		/// <summary>Always to emit type information.</summary>
		// Token: 0x040004F5 RID: 1269
		Always,
		/// <summary>Never to emit type information.</summary>
		// Token: 0x040004F6 RID: 1270
		Never
	}
}
