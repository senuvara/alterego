﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization.Diagnostics.Application;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x0200013D RID: 317
	internal class XmlObjectSerializerContext
	{
		// Token: 0x06000EFF RID: 3839 RVA: 0x000366E1 File Offset: 0x000348E1
		internal XmlObjectSerializerContext(XmlObjectSerializer serializer, int maxItemsInObjectGraph, StreamingContext streamingContext, bool ignoreExtensionDataObject, DataContractResolver dataContractResolver)
		{
			this.serializer = serializer;
			this.itemCount = 1;
			this.maxItemsInObjectGraph = maxItemsInObjectGraph;
			this.streamingContext = streamingContext;
			this.ignoreExtensionDataObject = ignoreExtensionDataObject;
			this.dataContractResolver = dataContractResolver;
		}

		// Token: 0x06000F00 RID: 3840 RVA: 0x00036715 File Offset: 0x00034915
		internal XmlObjectSerializerContext(XmlObjectSerializer serializer, int maxItemsInObjectGraph, StreamingContext streamingContext, bool ignoreExtensionDataObject) : this(serializer, maxItemsInObjectGraph, streamingContext, ignoreExtensionDataObject, null)
		{
		}

		// Token: 0x06000F01 RID: 3841 RVA: 0x00036723 File Offset: 0x00034923
		internal XmlObjectSerializerContext(DataContractSerializer serializer, DataContract rootTypeDataContract, DataContractResolver dataContractResolver) : this(serializer, serializer.MaxItemsInObjectGraph, new StreamingContext(StreamingContextStates.All), serializer.IgnoreExtensionDataObject, dataContractResolver)
		{
			this.rootTypeDataContract = rootTypeDataContract;
			this.serializerKnownTypeList = serializer.knownTypeList;
		}

		// Token: 0x06000F02 RID: 3842 RVA: 0x00036756 File Offset: 0x00034956
		internal XmlObjectSerializerContext(NetDataContractSerializer serializer) : this(serializer, serializer.MaxItemsInObjectGraph, serializer.Context, serializer.IgnoreExtensionDataObject)
		{
		}

		// Token: 0x1700036D RID: 877
		// (get) Token: 0x06000F03 RID: 3843 RVA: 0x0000310F File Offset: 0x0000130F
		internal virtual SerializationMode Mode
		{
			get
			{
				return SerializationMode.SharedContract;
			}
		}

		// Token: 0x1700036E RID: 878
		// (get) Token: 0x06000F04 RID: 3844 RVA: 0x0000310F File Offset: 0x0000130F
		// (set) Token: 0x06000F05 RID: 3845 RVA: 0x000020AE File Offset: 0x000002AE
		internal virtual bool IsGetOnlyCollection
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x06000F06 RID: 3846 RVA: 0x000020AE File Offset: 0x000002AE
		[SecuritySafeCritical]
		public void DemandSerializationFormatterPermission()
		{
		}

		// Token: 0x06000F07 RID: 3847 RVA: 0x000020AE File Offset: 0x000002AE
		[SecuritySafeCritical]
		public void DemandMemberAccessPermission()
		{
		}

		// Token: 0x06000F08 RID: 3848 RVA: 0x00036771 File Offset: 0x00034971
		public StreamingContext GetStreamingContext()
		{
			return this.streamingContext;
		}

		// Token: 0x1700036F RID: 879
		// (get) Token: 0x06000F09 RID: 3849 RVA: 0x00036779 File Offset: 0x00034979
		internal static MethodInfo IncrementItemCountMethod
		{
			get
			{
				if (XmlObjectSerializerContext.incrementItemCountMethod == null)
				{
					XmlObjectSerializerContext.incrementItemCountMethod = typeof(XmlObjectSerializerContext).GetMethod("IncrementItemCount", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlObjectSerializerContext.incrementItemCountMethod;
			}
		}

		// Token: 0x06000F0A RID: 3850 RVA: 0x000367A8 File Offset: 0x000349A8
		public void IncrementItemCount(int count)
		{
			if (count > this.maxItemsInObjectGraph - this.itemCount)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Maximum number of items that can be serialized or deserialized in an object graph is '{0}'.", new object[]
				{
					this.maxItemsInObjectGraph
				})));
			}
			this.itemCount += count;
		}

		// Token: 0x17000370 RID: 880
		// (get) Token: 0x06000F0B RID: 3851 RVA: 0x000367FC File Offset: 0x000349FC
		internal int RemainingItemCount
		{
			get
			{
				return this.maxItemsInObjectGraph - this.itemCount;
			}
		}

		// Token: 0x17000371 RID: 881
		// (get) Token: 0x06000F0C RID: 3852 RVA: 0x0003680B File Offset: 0x00034A0B
		internal bool IgnoreExtensionDataObject
		{
			get
			{
				return this.ignoreExtensionDataObject;
			}
		}

		// Token: 0x17000372 RID: 882
		// (get) Token: 0x06000F0D RID: 3853 RVA: 0x00036813 File Offset: 0x00034A13
		protected DataContractResolver DataContractResolver
		{
			get
			{
				return this.dataContractResolver;
			}
		}

		// Token: 0x17000373 RID: 883
		// (get) Token: 0x06000F0E RID: 3854 RVA: 0x0003681B File Offset: 0x00034A1B
		protected KnownTypeDataContractResolver KnownTypeResolver
		{
			get
			{
				if (this.knownTypeResolver == null)
				{
					this.knownTypeResolver = new KnownTypeDataContractResolver(this);
				}
				return this.knownTypeResolver;
			}
		}

		// Token: 0x06000F0F RID: 3855 RVA: 0x00036837 File Offset: 0x00034A37
		internal DataContract GetDataContract(Type type)
		{
			return this.GetDataContract(type.TypeHandle, type);
		}

		// Token: 0x06000F10 RID: 3856 RVA: 0x00036846 File Offset: 0x00034A46
		internal virtual DataContract GetDataContract(RuntimeTypeHandle typeHandle, Type type)
		{
			if (this.IsGetOnlyCollection)
			{
				return DataContract.GetGetOnlyCollectionDataContract(DataContract.GetId(typeHandle), typeHandle, type, this.Mode);
			}
			return DataContract.GetDataContract(typeHandle, type, this.Mode);
		}

		// Token: 0x06000F11 RID: 3857 RVA: 0x00036871 File Offset: 0x00034A71
		internal virtual DataContract GetDataContractSkipValidation(int typeId, RuntimeTypeHandle typeHandle, Type type)
		{
			if (this.IsGetOnlyCollection)
			{
				return DataContract.GetGetOnlyCollectionDataContractSkipValidation(typeId, typeHandle, type);
			}
			return DataContract.GetDataContractSkipValidation(typeId, typeHandle, type);
		}

		// Token: 0x06000F12 RID: 3858 RVA: 0x0003688C File Offset: 0x00034A8C
		internal virtual DataContract GetDataContract(int id, RuntimeTypeHandle typeHandle)
		{
			if (this.IsGetOnlyCollection)
			{
				return DataContract.GetGetOnlyCollectionDataContract(id, typeHandle, null, this.Mode);
			}
			return DataContract.GetDataContract(id, typeHandle, this.Mode);
		}

		// Token: 0x06000F13 RID: 3859 RVA: 0x000368B2 File Offset: 0x00034AB2
		internal virtual void CheckIfTypeSerializable(Type memberType, bool isMemberTypeSerializable)
		{
			if (!isMemberTypeSerializable)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot be serialized. Consider marking it with the DataContractAttribute attribute, and marking all of its members you want serialized with the DataMemberAttribute attribute. Alternatively, you can ensure that the type is public and has a parameterless constructor - all public members of the type will then be serialized, and no attributes will be required.", new object[]
				{
					memberType
				})));
			}
		}

		// Token: 0x06000F14 RID: 3860 RVA: 0x0000206B File Offset: 0x0000026B
		internal virtual Type GetSurrogatedType(Type type)
		{
			return type;
		}

		// Token: 0x17000374 RID: 884
		// (get) Token: 0x06000F15 RID: 3861 RVA: 0x000368D6 File Offset: 0x00034AD6
		private Dictionary<XmlQualifiedName, DataContract> SerializerKnownDataContracts
		{
			get
			{
				if (!this.isSerializerKnownDataContractsSetExplicit)
				{
					this.serializerKnownDataContracts = this.serializer.KnownDataContracts;
					this.isSerializerKnownDataContractsSetExplicit = true;
				}
				return this.serializerKnownDataContracts;
			}
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x00036900 File Offset: 0x00034B00
		private DataContract GetDataContractFromSerializerKnownTypes(XmlQualifiedName qname)
		{
			Dictionary<XmlQualifiedName, DataContract> dictionary = this.SerializerKnownDataContracts;
			if (dictionary == null)
			{
				return null;
			}
			DataContract result;
			if (!dictionary.TryGetValue(qname, out result))
			{
				return null;
			}
			return result;
		}

		// Token: 0x06000F17 RID: 3863 RVA: 0x00036928 File Offset: 0x00034B28
		internal static Dictionary<XmlQualifiedName, DataContract> GetDataContractsForKnownTypes(IList<Type> knownTypeList)
		{
			if (knownTypeList == null)
			{
				return null;
			}
			Dictionary<XmlQualifiedName, DataContract> result = new Dictionary<XmlQualifiedName, DataContract>();
			Dictionary<Type, Type> typesChecked = new Dictionary<Type, Type>();
			for (int i = 0; i < knownTypeList.Count; i++)
			{
				Type type = knownTypeList[i];
				if (type == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentException(SR.GetString("One of the known types provided to the serializer via '{0}' argument was invalid because it was null. All known types specified must be non-null values.", new object[]
					{
						"knownTypes"
					})));
				}
				DataContract.CheckAndAdd(type, typesChecked, ref result);
			}
			return result;
		}

		// Token: 0x06000F18 RID: 3864 RVA: 0x00036998 File Offset: 0x00034B98
		internal bool IsKnownType(DataContract dataContract, Dictionary<XmlQualifiedName, DataContract> knownDataContracts, Type declaredType)
		{
			bool flag = false;
			if (knownDataContracts != null)
			{
				this.scopedKnownTypes.Push(knownDataContracts);
				flag = true;
			}
			bool result = this.IsKnownType(dataContract, declaredType);
			if (flag)
			{
				this.scopedKnownTypes.Pop();
			}
			return result;
		}

		// Token: 0x06000F19 RID: 3865 RVA: 0x000369D0 File Offset: 0x00034BD0
		internal bool IsKnownType(DataContract dataContract, Type declaredType)
		{
			DataContract dataContract2 = this.ResolveDataContractFromKnownTypes(dataContract.StableName.Name, dataContract.StableName.Namespace, null, declaredType);
			return dataContract2 != null && dataContract2.UnderlyingType == dataContract.UnderlyingType;
		}

		// Token: 0x06000F1A RID: 3866 RVA: 0x00036A14 File Offset: 0x00034C14
		private DataContract ResolveDataContractFromKnownTypes(XmlQualifiedName typeName)
		{
			DataContract dataContract = PrimitiveDataContract.GetPrimitiveDataContract(typeName.Name, typeName.Namespace);
			if (dataContract == null)
			{
				dataContract = this.scopedKnownTypes.GetDataContract(typeName);
				if (dataContract == null)
				{
					dataContract = this.GetDataContractFromSerializerKnownTypes(typeName);
				}
			}
			return dataContract;
		}

		// Token: 0x06000F1B RID: 3867 RVA: 0x00036A50 File Offset: 0x00034C50
		private DataContract ResolveDataContractFromDataContractResolver(XmlQualifiedName typeName, Type declaredType)
		{
			if (TD.DCResolverResolveIsEnabled())
			{
				TD.DCResolverResolve(typeName.Name + ":" + typeName.Namespace);
			}
			Type type = this.DataContractResolver.ResolveName(typeName.Name, typeName.Namespace, declaredType, this.KnownTypeResolver);
			if (type == null)
			{
				return null;
			}
			return this.GetDataContract(type);
		}

		// Token: 0x06000F1C RID: 3868 RVA: 0x00036AB0 File Offset: 0x00034CB0
		internal Type ResolveNameFromKnownTypes(XmlQualifiedName typeName)
		{
			DataContract dataContract = this.ResolveDataContractFromKnownTypes(typeName);
			if (dataContract == null)
			{
				return null;
			}
			return dataContract.OriginalUnderlyingType;
		}

		// Token: 0x06000F1D RID: 3869 RVA: 0x00036AD0 File Offset: 0x00034CD0
		protected DataContract ResolveDataContractFromKnownTypes(string typeName, string typeNs, DataContract memberTypeContract, Type declaredType)
		{
			XmlQualifiedName xmlQualifiedName = new XmlQualifiedName(typeName, typeNs);
			DataContract dataContract;
			if (this.DataContractResolver == null)
			{
				dataContract = this.ResolveDataContractFromKnownTypes(xmlQualifiedName);
			}
			else
			{
				dataContract = this.ResolveDataContractFromDataContractResolver(xmlQualifiedName, declaredType);
			}
			if (dataContract == null)
			{
				if (memberTypeContract != null && !memberTypeContract.UnderlyingType.IsInterface && memberTypeContract.StableName == xmlQualifiedName)
				{
					dataContract = memberTypeContract;
				}
				if (dataContract == null && this.rootTypeDataContract != null)
				{
					dataContract = this.ResolveDataContractFromRootDataContract(xmlQualifiedName);
				}
			}
			return dataContract;
		}

		// Token: 0x06000F1E RID: 3870 RVA: 0x00036B38 File Offset: 0x00034D38
		protected virtual DataContract ResolveDataContractFromRootDataContract(XmlQualifiedName typeQName)
		{
			if (this.rootTypeDataContract.StableName == typeQName)
			{
				return this.rootTypeDataContract;
			}
			DataContract dataContract;
			for (CollectionDataContract collectionDataContract = this.rootTypeDataContract as CollectionDataContract; collectionDataContract != null; collectionDataContract = (dataContract as CollectionDataContract))
			{
				dataContract = this.GetDataContract(this.GetSurrogatedType(collectionDataContract.ItemType));
				if (dataContract.StableName == typeQName)
				{
					return dataContract;
				}
			}
			return null;
		}

		// Token: 0x040006BC RID: 1724
		protected XmlObjectSerializer serializer;

		// Token: 0x040006BD RID: 1725
		protected DataContract rootTypeDataContract;

		// Token: 0x040006BE RID: 1726
		internal ScopedKnownTypes scopedKnownTypes;

		// Token: 0x040006BF RID: 1727
		protected Dictionary<XmlQualifiedName, DataContract> serializerKnownDataContracts;

		// Token: 0x040006C0 RID: 1728
		private bool isSerializerKnownDataContractsSetExplicit;

		// Token: 0x040006C1 RID: 1729
		protected IList<Type> serializerKnownTypeList;

		// Token: 0x040006C2 RID: 1730
		[SecurityCritical]
		private bool demandedSerializationFormatterPermission;

		// Token: 0x040006C3 RID: 1731
		[SecurityCritical]
		private bool demandedMemberAccessPermission;

		// Token: 0x040006C4 RID: 1732
		private int itemCount;

		// Token: 0x040006C5 RID: 1733
		private int maxItemsInObjectGraph;

		// Token: 0x040006C6 RID: 1734
		private StreamingContext streamingContext;

		// Token: 0x040006C7 RID: 1735
		private bool ignoreExtensionDataObject;

		// Token: 0x040006C8 RID: 1736
		private DataContractResolver dataContractResolver;

		// Token: 0x040006C9 RID: 1737
		private KnownTypeDataContractResolver knownTypeResolver;

		// Token: 0x040006CA RID: 1738
		private static MethodInfo incrementItemCountMethod;
	}
}
