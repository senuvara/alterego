﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization
{
	// Token: 0x02000121 RID: 289
	internal static class SerializationTrace
	{
		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06000DF0 RID: 3568 RVA: 0x00033CA5 File Offset: 0x00031EA5
		internal static SourceSwitch CodeGenerationSwitch
		{
			get
			{
				return SerializationTrace.CodeGenerationTraceSource.Switch;
			}
		}

		// Token: 0x06000DF1 RID: 3569 RVA: 0x000020AE File Offset: 0x000002AE
		internal static void WriteInstruction(int lineNumber, string instruction)
		{
		}

		// Token: 0x06000DF2 RID: 3570 RVA: 0x000020AE File Offset: 0x000002AE
		internal static void TraceInstruction(string instruction)
		{
		}

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06000DF3 RID: 3571 RVA: 0x00033CB1 File Offset: 0x00031EB1
		private static TraceSource CodeGenerationTraceSource
		{
			[SecuritySafeCritical]
			get
			{
				if (SerializationTrace.codeGen == null)
				{
					SerializationTrace.codeGen = new TraceSource("System.Runtime.Serialization.CodeGeneration");
				}
				return SerializationTrace.codeGen;
			}
		}

		// Token: 0x04000647 RID: 1607
		[SecurityCritical]
		private static TraceSource codeGen;
	}
}
