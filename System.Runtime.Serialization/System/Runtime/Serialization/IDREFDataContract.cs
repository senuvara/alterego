﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000111 RID: 273
	internal class IDREFDataContract : StringDataContract
	{
		// Token: 0x06000DC1 RID: 3521 RVA: 0x00033884 File Offset: 0x00031A84
		internal IDREFDataContract() : base(DictionaryGlobals.IDREFLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
