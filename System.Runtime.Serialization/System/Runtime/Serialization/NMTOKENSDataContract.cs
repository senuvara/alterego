﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000116 RID: 278
	internal class NMTOKENSDataContract : StringDataContract
	{
		// Token: 0x06000DC6 RID: 3526 RVA: 0x000338DE File Offset: 0x00031ADE
		internal NMTOKENSDataContract() : base(DictionaryGlobals.NMTOKENSLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
