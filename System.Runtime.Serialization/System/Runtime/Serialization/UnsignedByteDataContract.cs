﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000F2 RID: 242
	internal class UnsignedByteDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D70 RID: 3440 RVA: 0x000332D5 File Offset: 0x000314D5
		internal UnsignedByteDataContract() : base(typeof(byte), DictionaryGlobals.UnsignedByteLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x06000D71 RID: 3441 RVA: 0x000332F1 File Offset: 0x000314F1
		internal override string WriteMethodName
		{
			get
			{
				return "WriteUnsignedByte";
			}
		}

		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x06000D72 RID: 3442 RVA: 0x000332F8 File Offset: 0x000314F8
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsUnsignedByte";
			}
		}

		// Token: 0x06000D73 RID: 3443 RVA: 0x000332FF File Offset: 0x000314FF
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteUnsignedByte((byte)obj);
		}

		// Token: 0x06000D74 RID: 3444 RVA: 0x0003330D File Offset: 0x0003150D
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsUnsignedByte(), context);
			}
			return reader.ReadElementContentAsUnsignedByte();
		}
	}
}
