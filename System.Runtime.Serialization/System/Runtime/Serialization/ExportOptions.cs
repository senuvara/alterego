﻿using System;
using System.Collections.ObjectModel;

namespace System.Runtime.Serialization
{
	/// <summary>Represents the options that can be set for an <see cref="T:System.Runtime.Serialization.XsdDataContractExporter" />.</summary>
	// Token: 0x020000CF RID: 207
	public class ExportOptions
	{
		/// <summary>Gets or sets a serialization surrogate. </summary>
		/// <returns>An implementation of the <see cref="T:System.Runtime.Serialization.IDataContractSurrogate" /> interface that can be used to customize how an XML schema representation is exported for a specific type. </returns>
		// Token: 0x17000201 RID: 513
		// (get) Token: 0x06000BC1 RID: 3009 RVA: 0x0002EF9E File Offset: 0x0002D19E
		// (set) Token: 0x06000BC2 RID: 3010 RVA: 0x0002EFA6 File Offset: 0x0002D1A6
		public IDataContractSurrogate DataContractSurrogate
		{
			get
			{
				return this.dataContractSurrogate;
			}
			set
			{
				this.dataContractSurrogate = value;
			}
		}

		// Token: 0x06000BC3 RID: 3011 RVA: 0x0002EF9E File Offset: 0x0002D19E
		internal IDataContractSurrogate GetSurrogate()
		{
			return this.dataContractSurrogate;
		}

		/// <summary>Gets the collection of types that may be encountered during serialization or deserialization. </summary>
		/// <returns>A <see langword="KnownTypes" /> collection that contains types that may be encountered during serialization or deserialization. XML schema representations are exported for all the types specified in this collection by the <see cref="T:System.Runtime.Serialization.XsdDataContractExporter" />.</returns>
		// Token: 0x17000202 RID: 514
		// (get) Token: 0x06000BC4 RID: 3012 RVA: 0x0002EFAF File Offset: 0x0002D1AF
		public Collection<Type> KnownTypes
		{
			get
			{
				if (this.knownTypes == null)
				{
					this.knownTypes = new Collection<Type>();
				}
				return this.knownTypes;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.ExportOptions" /> class.</summary>
		// Token: 0x06000BC5 RID: 3013 RVA: 0x00002217 File Offset: 0x00000417
		public ExportOptions()
		{
		}

		// Token: 0x04000503 RID: 1283
		private Collection<Type> knownTypes;

		// Token: 0x04000504 RID: 1284
		private IDataContractSurrogate dataContractSurrogate;
	}
}
