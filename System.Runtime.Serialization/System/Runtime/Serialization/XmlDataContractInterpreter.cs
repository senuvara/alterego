﻿using System;
using System.Reflection;
using System.Xml.Serialization;

namespace System.Runtime.Serialization
{
	// Token: 0x02000154 RID: 340
	internal class XmlDataContractInterpreter
	{
		// Token: 0x06001150 RID: 4432 RVA: 0x0003E02D File Offset: 0x0003C22D
		public XmlDataContractInterpreter(XmlDataContract contract)
		{
			this.contract = contract;
		}

		// Token: 0x06001151 RID: 4433 RVA: 0x0003E03C File Offset: 0x0003C23C
		public IXmlSerializable CreateXmlSerializable()
		{
			Type underlyingType = this.contract.UnderlyingType;
			object obj;
			if (underlyingType.IsValueType)
			{
				obj = FormatterServices.GetUninitializedObject(underlyingType);
			}
			else
			{
				obj = this.GetConstructor().Invoke(new object[0]);
			}
			return (IXmlSerializable)obj;
		}

		// Token: 0x06001152 RID: 4434 RVA: 0x0003E080 File Offset: 0x0003C280
		private ConstructorInfo GetConstructor()
		{
			Type underlyingType = this.contract.UnderlyingType;
			if (underlyingType.IsValueType)
			{
				return null;
			}
			ConstructorInfo constructor = underlyingType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Globals.EmptyTypeArray, null);
			if (constructor == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("IXmlSerializable Type '{0}' must have default constructor.", new object[]
				{
					DataContract.GetClrTypeFullName(underlyingType)
				})));
			}
			return constructor;
		}

		// Token: 0x0400091C RID: 2332
		private XmlDataContract contract;
	}
}
