﻿using System;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000A9 RID: 169
	internal class Attributes
	{
		// Token: 0x06000903 RID: 2307 RVA: 0x00025590 File Offset: 0x00023790
		[SecuritySafeCritical]
		static Attributes()
		{
		}

		// Token: 0x06000904 RID: 2308 RVA: 0x000255F4 File Offset: 0x000237F4
		[SecuritySafeCritical]
		internal void Read(XmlReaderDelegator reader)
		{
			this.Reset();
			while (reader.MoveToNextAttribute())
			{
				switch (reader.IndexOfLocalName(Attributes.serializationLocalNames, DictionaryGlobals.SerializationNamespace))
				{
				case 0:
					this.ReadId(reader);
					break;
				case 1:
					this.ReadArraySize(reader);
					break;
				case 2:
					this.ReadRef(reader);
					break;
				case 3:
					this.ClrType = reader.Value;
					break;
				case 4:
					this.ClrAssembly = reader.Value;
					break;
				case 5:
					this.ReadFactoryType(reader);
					break;
				default:
				{
					int num = reader.IndexOfLocalName(Attributes.schemaInstanceLocalNames, DictionaryGlobals.SchemaInstanceNamespace);
					if (num != 0)
					{
						if (num != 1)
						{
							if (!reader.IsNamespaceUri(DictionaryGlobals.XmlnsNamespace))
							{
								this.UnrecognizedAttributesFound = true;
							}
						}
						else
						{
							this.ReadXsiType(reader);
						}
					}
					else
					{
						this.ReadXsiNil(reader);
					}
					break;
				}
				}
			}
			reader.MoveToElement();
		}

		// Token: 0x06000905 RID: 2309 RVA: 0x000256D0 File Offset: 0x000238D0
		internal void Reset()
		{
			this.Id = Globals.NewObjectId;
			this.Ref = Globals.NewObjectId;
			this.XsiTypeName = null;
			this.XsiTypeNamespace = null;
			this.XsiTypePrefix = null;
			this.XsiNil = false;
			this.ClrAssembly = null;
			this.ClrType = null;
			this.ArraySZSize = -1;
			this.FactoryTypeName = null;
			this.FactoryTypeNamespace = null;
			this.FactoryTypePrefix = null;
			this.UnrecognizedAttributesFound = false;
		}

		// Token: 0x06000906 RID: 2310 RVA: 0x00025740 File Offset: 0x00023940
		private void ReadId(XmlReaderDelegator reader)
		{
			this.Id = reader.ReadContentAsString();
			if (string.IsNullOrEmpty(this.Id))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Invalid Id '{0}'. Must not be null or empty.", new object[]
				{
					this.Id
				})));
			}
		}

		// Token: 0x06000907 RID: 2311 RVA: 0x0002577F File Offset: 0x0002397F
		private void ReadRef(XmlReaderDelegator reader)
		{
			this.Ref = reader.ReadContentAsString();
			if (string.IsNullOrEmpty(this.Ref))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Invalid Ref '{0}'. Must not be null or empty.", new object[]
				{
					this.Ref
				})));
			}
		}

		// Token: 0x06000908 RID: 2312 RVA: 0x000257BE File Offset: 0x000239BE
		private void ReadXsiNil(XmlReaderDelegator reader)
		{
			this.XsiNil = reader.ReadContentAsBoolean();
		}

		// Token: 0x06000909 RID: 2313 RVA: 0x000257CC File Offset: 0x000239CC
		private void ReadArraySize(XmlReaderDelegator reader)
		{
			this.ArraySZSize = reader.ReadContentAsInt();
			if (this.ArraySZSize < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Invalid Size '{0}'. Must be non-negative integer.", new object[]
				{
					this.ArraySZSize
				})));
			}
		}

		// Token: 0x0600090A RID: 2314 RVA: 0x0002580C File Offset: 0x00023A0C
		private void ReadXsiType(XmlReaderDelegator reader)
		{
			string value = reader.Value;
			if (value != null && value.Length > 0)
			{
				XmlObjectSerializerReadContext.ParseQualifiedName(value, reader, out this.XsiTypeName, out this.XsiTypeNamespace, out this.XsiTypePrefix);
			}
		}

		// Token: 0x0600090B RID: 2315 RVA: 0x00025848 File Offset: 0x00023A48
		private void ReadFactoryType(XmlReaderDelegator reader)
		{
			string value = reader.Value;
			if (value != null && value.Length > 0)
			{
				XmlObjectSerializerReadContext.ParseQualifiedName(value, reader, out this.FactoryTypeName, out this.FactoryTypeNamespace, out this.FactoryTypePrefix);
			}
		}

		// Token: 0x0600090C RID: 2316 RVA: 0x00002217 File Offset: 0x00000417
		public Attributes()
		{
		}

		// Token: 0x040003EB RID: 1003
		[SecurityCritical]
		private static XmlDictionaryString[] serializationLocalNames = new XmlDictionaryString[]
		{
			DictionaryGlobals.IdLocalName,
			DictionaryGlobals.ArraySizeLocalName,
			DictionaryGlobals.RefLocalName,
			DictionaryGlobals.ClrTypeLocalName,
			DictionaryGlobals.ClrAssemblyLocalName,
			DictionaryGlobals.ISerializableFactoryTypeLocalName
		};

		// Token: 0x040003EC RID: 1004
		[SecurityCritical]
		private static XmlDictionaryString[] schemaInstanceLocalNames = new XmlDictionaryString[]
		{
			DictionaryGlobals.XsiNilLocalName,
			DictionaryGlobals.XsiTypeLocalName
		};

		// Token: 0x040003ED RID: 1005
		internal string Id;

		// Token: 0x040003EE RID: 1006
		internal string Ref;

		// Token: 0x040003EF RID: 1007
		internal string XsiTypeName;

		// Token: 0x040003F0 RID: 1008
		internal string XsiTypeNamespace;

		// Token: 0x040003F1 RID: 1009
		internal string XsiTypePrefix;

		// Token: 0x040003F2 RID: 1010
		internal bool XsiNil;

		// Token: 0x040003F3 RID: 1011
		internal string ClrAssembly;

		// Token: 0x040003F4 RID: 1012
		internal string ClrType;

		// Token: 0x040003F5 RID: 1013
		internal int ArraySZSize;

		// Token: 0x040003F6 RID: 1014
		internal string FactoryTypeName;

		// Token: 0x040003F7 RID: 1015
		internal string FactoryTypeNamespace;

		// Token: 0x040003F8 RID: 1016
		internal string FactoryTypePrefix;

		// Token: 0x040003F9 RID: 1017
		internal bool UnrecognizedAttributesFound;
	}
}
