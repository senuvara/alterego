﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000FC RID: 252
	internal class NonNegativeIntegerDataContract : LongDataContract
	{
		// Token: 0x06000D93 RID: 3475 RVA: 0x00033549 File Offset: 0x00031749
		internal NonNegativeIntegerDataContract() : base(DictionaryGlobals.nonNegativeIntegerLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
