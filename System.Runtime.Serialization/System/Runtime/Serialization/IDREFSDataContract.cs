﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000112 RID: 274
	internal class IDREFSDataContract : StringDataContract
	{
		// Token: 0x06000DC2 RID: 3522 RVA: 0x00033896 File Offset: 0x00031A96
		internal IDREFSDataContract() : base(DictionaryGlobals.IDREFSLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
