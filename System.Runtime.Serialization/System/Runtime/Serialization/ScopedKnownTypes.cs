﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x0200011F RID: 287
	internal struct ScopedKnownTypes
	{
		// Token: 0x06000DED RID: 3565 RVA: 0x00033BFC File Offset: 0x00031DFC
		internal void Push(Dictionary<XmlQualifiedName, DataContract> dataContractDictionary)
		{
			if (this.dataContractDictionaries == null)
			{
				this.dataContractDictionaries = new Dictionary<XmlQualifiedName, DataContract>[4];
			}
			else if (this.count == this.dataContractDictionaries.Length)
			{
				Array.Resize<Dictionary<XmlQualifiedName, DataContract>>(ref this.dataContractDictionaries, this.dataContractDictionaries.Length * 2);
			}
			Dictionary<XmlQualifiedName, DataContract>[] array = this.dataContractDictionaries;
			int num = this.count;
			this.count = num + 1;
			array[num] = dataContractDictionary;
		}

		// Token: 0x06000DEE RID: 3566 RVA: 0x00033C5D File Offset: 0x00031E5D
		internal void Pop()
		{
			this.count--;
		}

		// Token: 0x06000DEF RID: 3567 RVA: 0x00033C70 File Offset: 0x00031E70
		internal DataContract GetDataContract(XmlQualifiedName qname)
		{
			for (int i = this.count - 1; i >= 0; i--)
			{
				DataContract result;
				if (this.dataContractDictionaries[i].TryGetValue(qname, out result))
				{
					return result;
				}
			}
			return null;
		}

		// Token: 0x04000642 RID: 1602
		internal Dictionary<XmlQualifiedName, DataContract>[] dataContractDictionaries;

		// Token: 0x04000643 RID: 1603
		private int count;
	}
}
