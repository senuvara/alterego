﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000AF RID: 175
	[DataContract(Namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
	internal struct KeyValue<K, V>
	{
		// Token: 0x06000964 RID: 2404 RVA: 0x00027821 File Offset: 0x00025A21
		internal KeyValue(K key, V value)
		{
			this.key = key;
			this.value = value;
		}

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x06000965 RID: 2405 RVA: 0x00027831 File Offset: 0x00025A31
		// (set) Token: 0x06000966 RID: 2406 RVA: 0x00027839 File Offset: 0x00025A39
		[DataMember(IsRequired = true)]
		public K Key
		{
			get
			{
				return this.key;
			}
			set
			{
				this.key = value;
			}
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x06000967 RID: 2407 RVA: 0x00027842 File Offset: 0x00025A42
		// (set) Token: 0x06000968 RID: 2408 RVA: 0x0002784A File Offset: 0x00025A4A
		[DataMember(IsRequired = true)]
		public V Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		// Token: 0x0400041A RID: 1050
		private K key;

		// Token: 0x0400041B RID: 1051
		private V value;
	}
}
