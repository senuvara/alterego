﻿using System;
using System.Globalization;

namespace System.Runtime.Serialization
{
	/// <summary>Specifies date-time format options.</summary>
	// Token: 0x020000C8 RID: 200
	public class DateTimeFormat
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.DateTimeFormat" /> class using the format string.</summary>
		/// <param name="formatString">The format string.</param>
		// Token: 0x06000B84 RID: 2948 RVA: 0x0002DF3E File Offset: 0x0002C13E
		public DateTimeFormat(string formatString) : this(formatString, DateTimeFormatInfo.CurrentInfo)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.DateTimeFormat" /> class using the format string and format provider.</summary>
		/// <param name="formatString">The format sting.</param>
		/// <param name="formatProvider">The format provider.</param>
		// Token: 0x06000B85 RID: 2949 RVA: 0x0002DF4C File Offset: 0x0002C14C
		public DateTimeFormat(string formatString, IFormatProvider formatProvider)
		{
			if (formatString == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("formatString");
			}
			if (formatProvider == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("formatProvider");
			}
			this.formatString = formatString;
			this.formatProvider = formatProvider;
			this.dateTimeStyles = DateTimeStyles.RoundtripKind;
		}

		/// <summary>Gets the format strings to control the formatting produced when a date or time is represented as a string.</summary>
		/// <returns>The format strings to control the formatting produced when a date or time is represented as a string.</returns>
		// Token: 0x170001ED RID: 493
		// (get) Token: 0x06000B86 RID: 2950 RVA: 0x0002DF89 File Offset: 0x0002C189
		public string FormatString
		{
			get
			{
				return this.formatString;
			}
		}

		/// <summary>Gets an object that controls formatting.</summary>
		// Token: 0x170001EE RID: 494
		// (get) Token: 0x06000B87 RID: 2951 RVA: 0x0002DF91 File Offset: 0x0002C191
		public IFormatProvider FormatProvider
		{
			get
			{
				return this.formatProvider;
			}
		}

		/// <summary>Gets or sets the formatting options that customize string parsing for some date and time parsing methods.</summary>
		/// <returns>The formatting options that customize string parsing for some date and time parsing methods.</returns>
		// Token: 0x170001EF RID: 495
		// (get) Token: 0x06000B88 RID: 2952 RVA: 0x0002DF99 File Offset: 0x0002C199
		// (set) Token: 0x06000B89 RID: 2953 RVA: 0x0002DFA1 File Offset: 0x0002C1A1
		public DateTimeStyles DateTimeStyles
		{
			get
			{
				return this.dateTimeStyles;
			}
			set
			{
				this.dateTimeStyles = value;
			}
		}

		// Token: 0x040004B1 RID: 1201
		private string formatString;

		// Token: 0x040004B2 RID: 1202
		private IFormatProvider formatProvider;

		// Token: 0x040004B3 RID: 1203
		private DateTimeStyles dateTimeStyles;
	}
}
