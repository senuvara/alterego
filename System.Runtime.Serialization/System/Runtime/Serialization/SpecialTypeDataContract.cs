﻿using System;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x02000122 RID: 290
	internal sealed class SpecialTypeDataContract : DataContract
	{
		// Token: 0x06000DF4 RID: 3572 RVA: 0x00033CCE File Offset: 0x00031ECE
		[SecuritySafeCritical]
		public SpecialTypeDataContract(Type type) : base(new SpecialTypeDataContract.SpecialTypeDataContractCriticalHelper(type))
		{
			this.helper = (base.Helper as SpecialTypeDataContract.SpecialTypeDataContractCriticalHelper);
		}

		// Token: 0x06000DF5 RID: 3573 RVA: 0x00033CED File Offset: 0x00031EED
		[SecuritySafeCritical]
		public SpecialTypeDataContract(Type type, XmlDictionaryString name, XmlDictionaryString ns) : base(new SpecialTypeDataContract.SpecialTypeDataContractCriticalHelper(type, name, ns))
		{
			this.helper = (base.Helper as SpecialTypeDataContract.SpecialTypeDataContractCriticalHelper);
		}

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06000DF6 RID: 3574 RVA: 0x000066D0 File Offset: 0x000048D0
		internal override bool IsBuiltInDataContract
		{
			get
			{
				return true;
			}
		}

		// Token: 0x04000648 RID: 1608
		[SecurityCritical]
		private SpecialTypeDataContract.SpecialTypeDataContractCriticalHelper helper;

		// Token: 0x02000123 RID: 291
		private class SpecialTypeDataContractCriticalHelper : DataContract.DataContractCriticalHelper
		{
			// Token: 0x06000DF7 RID: 3575 RVA: 0x00033D0E File Offset: 0x00031F0E
			internal SpecialTypeDataContractCriticalHelper(Type type) : base(type)
			{
			}

			// Token: 0x06000DF8 RID: 3576 RVA: 0x00033164 File Offset: 0x00031364
			internal SpecialTypeDataContractCriticalHelper(Type type, XmlDictionaryString name, XmlDictionaryString ns) : base(type)
			{
				base.SetDataContractName(name, ns);
			}
		}
	}
}
