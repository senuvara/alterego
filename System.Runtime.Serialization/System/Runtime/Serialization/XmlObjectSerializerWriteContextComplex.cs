﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Diagnostics.Application;
using System.Security;
using System.Security.Permissions;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x02000144 RID: 324
	internal class XmlObjectSerializerWriteContextComplex : XmlObjectSerializerWriteContext
	{
		// Token: 0x06000FC4 RID: 4036 RVA: 0x00039D07 File Offset: 0x00037F07
		internal XmlObjectSerializerWriteContextComplex(DataContractSerializer serializer, DataContract rootTypeDataContract, DataContractResolver dataContractResolver) : base(serializer, rootTypeDataContract, dataContractResolver)
		{
			this.mode = SerializationMode.SharedContract;
			this.preserveObjectReferences = serializer.PreserveObjectReferences;
			this.dataContractSurrogate = serializer.DataContractSurrogate;
		}

		// Token: 0x06000FC5 RID: 4037 RVA: 0x00039D34 File Offset: 0x00037F34
		internal XmlObjectSerializerWriteContextComplex(NetDataContractSerializer serializer, Hashtable surrogateDataContracts) : base(serializer)
		{
			this.mode = SerializationMode.SharedType;
			this.preserveObjectReferences = true;
			this.streamingContext = serializer.Context;
			this.binder = serializer.Binder;
			this.surrogateSelector = serializer.SurrogateSelector;
			this.surrogateDataContracts = surrogateDataContracts;
		}

		// Token: 0x06000FC6 RID: 4038 RVA: 0x00039D81 File Offset: 0x00037F81
		internal XmlObjectSerializerWriteContextComplex(XmlObjectSerializer serializer, int maxItemsInObjectGraph, StreamingContext streamingContext, bool ignoreExtensionDataObject) : base(serializer, maxItemsInObjectGraph, streamingContext, ignoreExtensionDataObject)
		{
		}

		// Token: 0x1700037F RID: 895
		// (get) Token: 0x06000FC7 RID: 4039 RVA: 0x00039D8E File Offset: 0x00037F8E
		internal override SerializationMode Mode
		{
			get
			{
				return this.mode;
			}
		}

		// Token: 0x06000FC8 RID: 4040 RVA: 0x00039D98 File Offset: 0x00037F98
		internal override DataContract GetDataContract(RuntimeTypeHandle typeHandle, Type type)
		{
			DataContract dataContract = null;
			if (this.mode == SerializationMode.SharedType && this.surrogateSelector != null)
			{
				dataContract = NetDataContractSerializer.GetDataContractFromSurrogateSelector(this.surrogateSelector, this.streamingContext, typeHandle, type, ref this.surrogateDataContracts);
			}
			if (dataContract == null)
			{
				return base.GetDataContract(typeHandle, type);
			}
			if (this.IsGetOnlyCollection && dataContract is SurrogateDataContract)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Surrogates with get-only collections are not supported. Found on type '{0}'.", new object[]
				{
					DataContract.GetClrTypeFullName(dataContract.UnderlyingType)
				})));
			}
			return dataContract;
		}

		// Token: 0x06000FC9 RID: 4041 RVA: 0x00039E18 File Offset: 0x00038018
		internal override DataContract GetDataContract(int id, RuntimeTypeHandle typeHandle)
		{
			DataContract dataContract = null;
			if (this.mode == SerializationMode.SharedType && this.surrogateSelector != null)
			{
				dataContract = NetDataContractSerializer.GetDataContractFromSurrogateSelector(this.surrogateSelector, this.streamingContext, typeHandle, null, ref this.surrogateDataContracts);
			}
			if (dataContract == null)
			{
				return base.GetDataContract(id, typeHandle);
			}
			if (this.IsGetOnlyCollection && dataContract is SurrogateDataContract)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Surrogates with get-only collections are not supported. Found on type '{0}'.", new object[]
				{
					DataContract.GetClrTypeFullName(dataContract.UnderlyingType)
				})));
			}
			return dataContract;
		}

		// Token: 0x06000FCA RID: 4042 RVA: 0x00039E98 File Offset: 0x00038098
		internal override DataContract GetDataContractSkipValidation(int typeId, RuntimeTypeHandle typeHandle, Type type)
		{
			DataContract dataContract = null;
			if (this.mode == SerializationMode.SharedType && this.surrogateSelector != null)
			{
				dataContract = NetDataContractSerializer.GetDataContractFromSurrogateSelector(this.surrogateSelector, this.streamingContext, typeHandle, null, ref this.surrogateDataContracts);
			}
			if (dataContract == null)
			{
				return base.GetDataContractSkipValidation(typeId, typeHandle, type);
			}
			if (this.IsGetOnlyCollection && dataContract is SurrogateDataContract)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Surrogates with get-only collections are not supported. Found on type '{0}'.", new object[]
				{
					DataContract.GetClrTypeFullName(dataContract.UnderlyingType)
				})));
			}
			return dataContract;
		}

		// Token: 0x06000FCB RID: 4043 RVA: 0x00039F19 File Offset: 0x00038119
		internal override bool WriteClrTypeInfo(XmlWriterDelegator xmlWriter, DataContract dataContract)
		{
			if (this.mode == SerializationMode.SharedType)
			{
				NetDataContractSerializer.WriteClrTypeInfo(xmlWriter, dataContract, this.binder);
				return true;
			}
			return false;
		}

		// Token: 0x06000FCC RID: 4044 RVA: 0x00039F34 File Offset: 0x00038134
		internal override bool WriteClrTypeInfo(XmlWriterDelegator xmlWriter, Type dataContractType, string clrTypeName, string clrAssemblyName)
		{
			if (this.mode == SerializationMode.SharedType)
			{
				NetDataContractSerializer.WriteClrTypeInfo(xmlWriter, dataContractType, this.binder, clrTypeName, clrAssemblyName);
				return true;
			}
			return false;
		}

		// Token: 0x06000FCD RID: 4045 RVA: 0x00039F52 File Offset: 0x00038152
		internal override bool WriteClrTypeInfo(XmlWriterDelegator xmlWriter, Type dataContractType, SerializationInfo serInfo)
		{
			if (this.mode == SerializationMode.SharedType)
			{
				NetDataContractSerializer.WriteClrTypeInfo(xmlWriter, dataContractType, this.binder, serInfo);
				return true;
			}
			return false;
		}

		// Token: 0x06000FCE RID: 4046 RVA: 0x00039F6E File Offset: 0x0003816E
		public override void WriteAnyType(XmlWriterDelegator xmlWriter, object value)
		{
			if (!this.OnHandleReference(xmlWriter, value, false))
			{
				xmlWriter.WriteAnyType(value);
			}
		}

		// Token: 0x06000FCF RID: 4047 RVA: 0x00039F82 File Offset: 0x00038182
		public override void WriteString(XmlWriterDelegator xmlWriter, string value)
		{
			if (!this.OnHandleReference(xmlWriter, value, false))
			{
				xmlWriter.WriteString(value);
			}
		}

		// Token: 0x06000FD0 RID: 4048 RVA: 0x00039F96 File Offset: 0x00038196
		public override void WriteString(XmlWriterDelegator xmlWriter, string value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (value == null)
			{
				base.WriteNull(xmlWriter, typeof(string), true, name, ns);
				return;
			}
			xmlWriter.WriteStartElementPrimitive(name, ns);
			if (!this.OnHandleReference(xmlWriter, value, false))
			{
				xmlWriter.WriteString(value);
			}
			xmlWriter.WriteEndElementPrimitive();
		}

		// Token: 0x06000FD1 RID: 4049 RVA: 0x00039FD2 File Offset: 0x000381D2
		public override void WriteBase64(XmlWriterDelegator xmlWriter, byte[] value)
		{
			if (!this.OnHandleReference(xmlWriter, value, false))
			{
				xmlWriter.WriteBase64(value);
			}
		}

		// Token: 0x06000FD2 RID: 4050 RVA: 0x00039FE6 File Offset: 0x000381E6
		public override void WriteBase64(XmlWriterDelegator xmlWriter, byte[] value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (value == null)
			{
				base.WriteNull(xmlWriter, typeof(byte[]), true, name, ns);
				return;
			}
			xmlWriter.WriteStartElementPrimitive(name, ns);
			if (!this.OnHandleReference(xmlWriter, value, false))
			{
				xmlWriter.WriteBase64(value);
			}
			xmlWriter.WriteEndElementPrimitive();
		}

		// Token: 0x06000FD3 RID: 4051 RVA: 0x0003A022 File Offset: 0x00038222
		public override void WriteUri(XmlWriterDelegator xmlWriter, Uri value)
		{
			if (!this.OnHandleReference(xmlWriter, value, false))
			{
				xmlWriter.WriteUri(value);
			}
		}

		// Token: 0x06000FD4 RID: 4052 RVA: 0x0003A038 File Offset: 0x00038238
		public override void WriteUri(XmlWriterDelegator xmlWriter, Uri value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (value == null)
			{
				base.WriteNull(xmlWriter, typeof(Uri), true, name, ns);
				return;
			}
			xmlWriter.WriteStartElementPrimitive(name, ns);
			if (!this.OnHandleReference(xmlWriter, value, false))
			{
				xmlWriter.WriteUri(value);
			}
			xmlWriter.WriteEndElementPrimitive();
		}

		// Token: 0x06000FD5 RID: 4053 RVA: 0x0003A085 File Offset: 0x00038285
		public override void WriteQName(XmlWriterDelegator xmlWriter, XmlQualifiedName value)
		{
			if (!this.OnHandleReference(xmlWriter, value, false))
			{
				xmlWriter.WriteQName(value);
			}
		}

		// Token: 0x06000FD6 RID: 4054 RVA: 0x0003A09C File Offset: 0x0003829C
		public override void WriteQName(XmlWriterDelegator xmlWriter, XmlQualifiedName value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (value == null)
			{
				base.WriteNull(xmlWriter, typeof(XmlQualifiedName), true, name, ns);
				return;
			}
			if (ns != null && ns.Value != null && ns.Value.Length > 0)
			{
				xmlWriter.WriteStartElement("q", name, ns);
			}
			else
			{
				xmlWriter.WriteStartElement(name, ns);
			}
			if (!this.OnHandleReference(xmlWriter, value, false))
			{
				xmlWriter.WriteQName(value);
			}
			xmlWriter.WriteEndElement();
		}

		// Token: 0x06000FD7 RID: 4055 RVA: 0x0003A115 File Offset: 0x00038315
		public override void InternalSerialize(XmlWriterDelegator xmlWriter, object obj, bool isDeclaredType, bool writeXsiType, int declaredTypeID, RuntimeTypeHandle declaredTypeHandle)
		{
			if (this.dataContractSurrogate == null)
			{
				base.InternalSerialize(xmlWriter, obj, isDeclaredType, writeXsiType, declaredTypeID, declaredTypeHandle);
				return;
			}
			this.InternalSerializeWithSurrogate(xmlWriter, obj, isDeclaredType, writeXsiType, declaredTypeID, declaredTypeHandle);
		}

		// Token: 0x06000FD8 RID: 4056 RVA: 0x0003A140 File Offset: 0x00038340
		internal override bool OnHandleReference(XmlWriterDelegator xmlWriter, object obj, bool canContainCyclicReference)
		{
			if (this.preserveObjectReferences && !this.IsGetOnlyCollection)
			{
				bool flag = true;
				int id = base.SerializedObjects.GetId(obj, ref flag);
				if (flag)
				{
					xmlWriter.WriteAttributeInt("z", DictionaryGlobals.IdLocalName, DictionaryGlobals.SerializationNamespace, id);
				}
				else
				{
					xmlWriter.WriteAttributeInt("z", DictionaryGlobals.RefLocalName, DictionaryGlobals.SerializationNamespace, id);
					xmlWriter.WriteAttributeBool("i", DictionaryGlobals.XsiNilLocalName, DictionaryGlobals.SchemaInstanceNamespace, true);
				}
				return !flag;
			}
			return base.OnHandleReference(xmlWriter, obj, canContainCyclicReference);
		}

		// Token: 0x06000FD9 RID: 4057 RVA: 0x0003A1C3 File Offset: 0x000383C3
		internal override void OnEndHandleReference(XmlWriterDelegator xmlWriter, object obj, bool canContainCyclicReference)
		{
			if (this.preserveObjectReferences && !this.IsGetOnlyCollection)
			{
				return;
			}
			base.OnEndHandleReference(xmlWriter, obj, canContainCyclicReference);
		}

		// Token: 0x06000FDA RID: 4058 RVA: 0x0003A1E0 File Offset: 0x000383E0
		[SecuritySafeCritical]
		[PermissionSet(SecurityAction.Demand, Unrestricted = true)]
		[MethodImpl(MethodImplOptions.NoInlining)]
		private bool CheckIfTypeSerializableForSharedTypeMode(Type memberType)
		{
			ISurrogateSelector surrogateSelector;
			return this.surrogateSelector.GetSurrogate(memberType, this.streamingContext, out surrogateSelector) != null;
		}

		// Token: 0x06000FDB RID: 4059 RVA: 0x0003A204 File Offset: 0x00038404
		internal override void CheckIfTypeSerializable(Type memberType, bool isMemberTypeSerializable)
		{
			if (this.mode == SerializationMode.SharedType && this.surrogateSelector != null && this.CheckIfTypeSerializableForSharedTypeMode(memberType))
			{
				return;
			}
			if (this.dataContractSurrogate == null)
			{
				base.CheckIfTypeSerializable(memberType, isMemberTypeSerializable);
				return;
			}
			while (memberType.IsArray)
			{
				memberType = memberType.GetElementType();
			}
			memberType = DataContractSurrogateCaller.GetDataContractType(this.dataContractSurrogate, memberType);
			if (!DataContract.IsTypeSerializable(memberType))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot be serialized. Consider marking it with the DataContractAttribute attribute, and marking all of its members you want serialized with the DataMemberAttribute attribute. Alternatively, you can ensure that the type is public and has a parameterless constructor - all public members of the type will then be serialized, and no attributes will be required.", new object[]
				{
					memberType
				})));
			}
		}

		// Token: 0x06000FDC RID: 4060 RVA: 0x0003A284 File Offset: 0x00038484
		internal override Type GetSurrogatedType(Type type)
		{
			if (this.dataContractSurrogate == null)
			{
				return base.GetSurrogatedType(type);
			}
			type = DataContract.UnwrapNullableType(type);
			Type surrogatedType = DataContractSerializer.GetSurrogatedType(this.dataContractSurrogate, type);
			if (this.IsGetOnlyCollection && surrogatedType != type)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Surrogates with get-only collections are not supported. Found on type '{0}'.", new object[]
				{
					DataContract.GetClrTypeFullName(type)
				})));
			}
			return surrogatedType;
		}

		// Token: 0x06000FDD RID: 4061 RVA: 0x0003A2EC File Offset: 0x000384EC
		private void InternalSerializeWithSurrogate(XmlWriterDelegator xmlWriter, object obj, bool isDeclaredType, bool writeXsiType, int declaredTypeID, RuntimeTypeHandle declaredTypeHandle)
		{
			RuntimeTypeHandle runtimeTypeHandle = isDeclaredType ? declaredTypeHandle : Type.GetTypeHandle(obj);
			object obj2 = obj;
			int oldObjId = 0;
			Type typeFromHandle = Type.GetTypeFromHandle(runtimeTypeHandle);
			Type type = this.GetSurrogatedType(Type.GetTypeFromHandle(declaredTypeHandle));
			if (TD.DCSerializeWithSurrogateStartIsEnabled())
			{
				TD.DCSerializeWithSurrogateStart(type.FullName);
			}
			declaredTypeHandle = type.TypeHandle;
			obj = DataContractSerializer.SurrogateToDataContractType(this.dataContractSurrogate, obj, type, ref typeFromHandle);
			runtimeTypeHandle = typeFromHandle.TypeHandle;
			if (obj2 != obj)
			{
				oldObjId = base.SerializedObjects.ReassignId(0, obj2, obj);
			}
			if (writeXsiType)
			{
				type = Globals.TypeOfObject;
				this.SerializeWithXsiType(xmlWriter, obj, runtimeTypeHandle, typeFromHandle, -1, type.TypeHandle, type);
			}
			else if (declaredTypeHandle.Equals(runtimeTypeHandle))
			{
				DataContract dataContract = this.GetDataContract(runtimeTypeHandle, typeFromHandle);
				base.SerializeWithoutXsiType(dataContract, xmlWriter, obj, declaredTypeHandle);
			}
			else
			{
				this.SerializeWithXsiType(xmlWriter, obj, runtimeTypeHandle, typeFromHandle, -1, declaredTypeHandle, type);
			}
			if (obj2 != obj)
			{
				base.SerializedObjects.ReassignId(oldObjId, obj, obj2);
			}
			if (TD.DCSerializeWithSurrogateStopIsEnabled())
			{
				TD.DCSerializeWithSurrogateStop();
			}
		}

		// Token: 0x06000FDE RID: 4062 RVA: 0x0003A3DA File Offset: 0x000385DA
		internal override void WriteArraySize(XmlWriterDelegator xmlWriter, int size)
		{
			if (this.preserveObjectReferences && size > -1)
			{
				xmlWriter.WriteAttributeInt("z", DictionaryGlobals.ArraySizeLocalName, DictionaryGlobals.SerializationNamespace, size);
			}
		}

		// Token: 0x040006E8 RID: 1768
		protected IDataContractSurrogate dataContractSurrogate;

		// Token: 0x040006E9 RID: 1769
		private SerializationMode mode;

		// Token: 0x040006EA RID: 1770
		private SerializationBinder binder;

		// Token: 0x040006EB RID: 1771
		private ISurrogateSelector surrogateSelector;

		// Token: 0x040006EC RID: 1772
		private StreamingContext streamingContext;

		// Token: 0x040006ED RID: 1773
		private Hashtable surrogateDataContracts;
	}
}
