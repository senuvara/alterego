﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000F9 RID: 249
	internal class PositiveIntegerDataContract : LongDataContract
	{
		// Token: 0x06000D90 RID: 3472 RVA: 0x00033513 File Offset: 0x00031713
		internal PositiveIntegerDataContract() : base(DictionaryGlobals.positiveIntegerLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
