﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Diagnostics.Application;
using System.Security;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace System.Runtime.Serialization
{
	// Token: 0x020000B7 RID: 183
	internal abstract class DataContract
	{
		// Token: 0x060009FD RID: 2557 RVA: 0x000292D6 File Offset: 0x000274D6
		[SecuritySafeCritical]
		protected DataContract(DataContract.DataContractCriticalHelper helper)
		{
			this.helper = helper;
			this.name = helper.Name;
			this.ns = helper.Namespace;
		}

		// Token: 0x060009FE RID: 2558 RVA: 0x000292FD File Offset: 0x000274FD
		internal static DataContract GetDataContract(Type type)
		{
			return DataContract.GetDataContract(type.TypeHandle, type, SerializationMode.SharedContract);
		}

		// Token: 0x060009FF RID: 2559 RVA: 0x0002930C File Offset: 0x0002750C
		internal static DataContract GetDataContract(RuntimeTypeHandle typeHandle, Type type, SerializationMode mode)
		{
			return DataContract.GetDataContract(DataContract.GetId(typeHandle), typeHandle, mode);
		}

		// Token: 0x06000A00 RID: 2560 RVA: 0x0002931B File Offset: 0x0002751B
		internal static DataContract GetDataContract(int id, RuntimeTypeHandle typeHandle, SerializationMode mode)
		{
			return DataContract.GetDataContractSkipValidation(id, typeHandle, null).GetValidContract(mode);
		}

		// Token: 0x06000A01 RID: 2561 RVA: 0x0002932B File Offset: 0x0002752B
		[SecuritySafeCritical]
		internal static DataContract GetDataContractSkipValidation(int id, RuntimeTypeHandle typeHandle, Type type)
		{
			return DataContract.DataContractCriticalHelper.GetDataContractSkipValidation(id, typeHandle, type);
		}

		// Token: 0x06000A02 RID: 2562 RVA: 0x00029338 File Offset: 0x00027538
		internal static DataContract GetGetOnlyCollectionDataContract(int id, RuntimeTypeHandle typeHandle, Type type, SerializationMode mode)
		{
			DataContract dataContract = DataContract.GetGetOnlyCollectionDataContractSkipValidation(id, typeHandle, type);
			dataContract = dataContract.GetValidContract(mode);
			if (dataContract is ClassDataContract)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("For '{0}' type, class data contract was returned for get-only collection.", new object[]
				{
					DataContract.GetClrTypeFullName(dataContract.UnderlyingType)
				})));
			}
			return dataContract;
		}

		// Token: 0x06000A03 RID: 2563 RVA: 0x00029388 File Offset: 0x00027588
		[SecuritySafeCritical]
		internal static DataContract GetGetOnlyCollectionDataContractSkipValidation(int id, RuntimeTypeHandle typeHandle, Type type)
		{
			return DataContract.DataContractCriticalHelper.GetGetOnlyCollectionDataContractSkipValidation(id, typeHandle, type);
		}

		// Token: 0x06000A04 RID: 2564 RVA: 0x00029392 File Offset: 0x00027592
		[SecuritySafeCritical]
		internal static DataContract GetDataContractForInitialization(int id)
		{
			return DataContract.DataContractCriticalHelper.GetDataContractForInitialization(id);
		}

		// Token: 0x06000A05 RID: 2565 RVA: 0x0002939A File Offset: 0x0002759A
		[SecuritySafeCritical]
		internal static int GetIdForInitialization(ClassDataContract classContract)
		{
			return DataContract.DataContractCriticalHelper.GetIdForInitialization(classContract);
		}

		// Token: 0x06000A06 RID: 2566 RVA: 0x000293A2 File Offset: 0x000275A2
		[SecuritySafeCritical]
		internal static int GetId(RuntimeTypeHandle typeHandle)
		{
			return DataContract.DataContractCriticalHelper.GetId(typeHandle);
		}

		// Token: 0x06000A07 RID: 2567 RVA: 0x000293AA File Offset: 0x000275AA
		[SecuritySafeCritical]
		public static DataContract GetBuiltInDataContract(Type type)
		{
			return DataContract.DataContractCriticalHelper.GetBuiltInDataContract(type);
		}

		// Token: 0x06000A08 RID: 2568 RVA: 0x000293B2 File Offset: 0x000275B2
		[SecuritySafeCritical]
		public static DataContract GetBuiltInDataContract(string name, string ns)
		{
			return DataContract.DataContractCriticalHelper.GetBuiltInDataContract(name, ns);
		}

		// Token: 0x06000A09 RID: 2569 RVA: 0x000293BB File Offset: 0x000275BB
		[SecuritySafeCritical]
		public static DataContract GetBuiltInDataContract(string typeName)
		{
			return DataContract.DataContractCriticalHelper.GetBuiltInDataContract(typeName);
		}

		// Token: 0x06000A0A RID: 2570 RVA: 0x000293C3 File Offset: 0x000275C3
		[SecuritySafeCritical]
		internal static string GetNamespace(string key)
		{
			return DataContract.DataContractCriticalHelper.GetNamespace(key);
		}

		// Token: 0x06000A0B RID: 2571 RVA: 0x000293CB File Offset: 0x000275CB
		[SecuritySafeCritical]
		internal static XmlDictionaryString GetClrTypeString(string key)
		{
			return DataContract.DataContractCriticalHelper.GetClrTypeString(key);
		}

		// Token: 0x06000A0C RID: 2572 RVA: 0x000293D3 File Offset: 0x000275D3
		[SecuritySafeCritical]
		internal static void ThrowInvalidDataContractException(string message, Type type)
		{
			DataContract.DataContractCriticalHelper.ThrowInvalidDataContractException(message, type);
		}

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x06000A0D RID: 2573 RVA: 0x000293DC File Offset: 0x000275DC
		protected DataContract.DataContractCriticalHelper Helper
		{
			[SecurityCritical]
			get
			{
				return this.helper;
			}
		}

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06000A0E RID: 2574 RVA: 0x000293E4 File Offset: 0x000275E4
		internal Type UnderlyingType
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.UnderlyingType;
			}
		}

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x06000A0F RID: 2575 RVA: 0x000293F1 File Offset: 0x000275F1
		internal Type OriginalUnderlyingType
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.OriginalUnderlyingType;
			}
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000A10 RID: 2576 RVA: 0x000293FE File Offset: 0x000275FE
		internal virtual bool IsBuiltInDataContract
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsBuiltInDataContract;
			}
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000A11 RID: 2577 RVA: 0x0002940B File Offset: 0x0002760B
		internal Type TypeForInitialization
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.TypeForInitialization;
			}
		}

		// Token: 0x06000A12 RID: 2578 RVA: 0x00029418 File Offset: 0x00027618
		public virtual void WriteXmlValue(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContext context)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("An internal error has occurred. Unexpected contract type '{0}' for type '{1}' encountered.", new object[]
			{
				DataContract.GetClrTypeFullName(base.GetType()),
				DataContract.GetClrTypeFullName(this.UnderlyingType)
			})));
		}

		// Token: 0x06000A13 RID: 2579 RVA: 0x00029418 File Offset: 0x00027618
		public virtual object ReadXmlValue(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContext context)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("An internal error has occurred. Unexpected contract type '{0}' for type '{1}' encountered.", new object[]
			{
				DataContract.GetClrTypeFullName(base.GetType()),
				DataContract.GetClrTypeFullName(this.UnderlyingType)
			})));
		}

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x06000A14 RID: 2580 RVA: 0x00029450 File Offset: 0x00027650
		// (set) Token: 0x06000A15 RID: 2581 RVA: 0x0002945D File Offset: 0x0002765D
		internal bool IsValueType
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsValueType;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsValueType = value;
			}
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000A16 RID: 2582 RVA: 0x0002946B File Offset: 0x0002766B
		// (set) Token: 0x06000A17 RID: 2583 RVA: 0x00029478 File Offset: 0x00027678
		internal bool IsReference
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsReference;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsReference = value;
			}
		}

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x06000A18 RID: 2584 RVA: 0x00029486 File Offset: 0x00027686
		// (set) Token: 0x06000A19 RID: 2585 RVA: 0x00029493 File Offset: 0x00027693
		internal XmlQualifiedName StableName
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.StableName;
			}
			[SecurityCritical]
			set
			{
				this.helper.StableName = value;
			}
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x06000A1A RID: 2586 RVA: 0x000294A1 File Offset: 0x000276A1
		// (set) Token: 0x06000A1B RID: 2587 RVA: 0x000294AE File Offset: 0x000276AE
		internal GenericInfo GenericInfo
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.GenericInfo;
			}
			[SecurityCritical]
			set
			{
				this.helper.GenericInfo = value;
			}
		}

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x06000A1C RID: 2588 RVA: 0x000294BC File Offset: 0x000276BC
		// (set) Token: 0x06000A1D RID: 2589 RVA: 0x000294C9 File Offset: 0x000276C9
		internal virtual Dictionary<XmlQualifiedName, DataContract> KnownDataContracts
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.KnownDataContracts;
			}
			[SecurityCritical]
			set
			{
				this.helper.KnownDataContracts = value;
			}
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x06000A1E RID: 2590 RVA: 0x000294D7 File Offset: 0x000276D7
		// (set) Token: 0x06000A1F RID: 2591 RVA: 0x000294E4 File Offset: 0x000276E4
		internal virtual bool IsISerializable
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsISerializable;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsISerializable = value;
			}
		}

		// Token: 0x17000193 RID: 403
		// (get) Token: 0x06000A20 RID: 2592 RVA: 0x000294F2 File Offset: 0x000276F2
		internal XmlDictionaryString Name
		{
			[SecuritySafeCritical]
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x06000A21 RID: 2593 RVA: 0x000294FA File Offset: 0x000276FA
		public virtual XmlDictionaryString Namespace
		{
			[SecuritySafeCritical]
			get
			{
				return this.ns;
			}
		}

		// Token: 0x17000195 RID: 405
		// (get) Token: 0x06000A22 RID: 2594 RVA: 0x000066D0 File Offset: 0x000048D0
		// (set) Token: 0x06000A23 RID: 2595 RVA: 0x000020AE File Offset: 0x000002AE
		internal virtual bool HasRoot
		{
			get
			{
				return true;
			}
			set
			{
			}
		}

		// Token: 0x17000196 RID: 406
		// (get) Token: 0x06000A24 RID: 2596 RVA: 0x00029502 File Offset: 0x00027702
		// (set) Token: 0x06000A25 RID: 2597 RVA: 0x0002950F File Offset: 0x0002770F
		internal virtual XmlDictionaryString TopLevelElementName
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.TopLevelElementName;
			}
			[SecurityCritical]
			set
			{
				this.helper.TopLevelElementName = value;
			}
		}

		// Token: 0x17000197 RID: 407
		// (get) Token: 0x06000A26 RID: 2598 RVA: 0x0002951D File Offset: 0x0002771D
		// (set) Token: 0x06000A27 RID: 2599 RVA: 0x0002952A File Offset: 0x0002772A
		internal virtual XmlDictionaryString TopLevelElementNamespace
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.TopLevelElementNamespace;
			}
			[SecurityCritical]
			set
			{
				this.helper.TopLevelElementNamespace = value;
			}
		}

		// Token: 0x17000198 RID: 408
		// (get) Token: 0x06000A28 RID: 2600 RVA: 0x000066D0 File Offset: 0x000048D0
		internal virtual bool CanContainReferences
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000199 RID: 409
		// (get) Token: 0x06000A29 RID: 2601 RVA: 0x0000310F File Offset: 0x0000130F
		internal virtual bool IsPrimitive
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000A2A RID: 2602 RVA: 0x00029538 File Offset: 0x00027738
		internal virtual void WriteRootElement(XmlWriterDelegator writer, XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (ns == DictionaryGlobals.SerializationNamespace && !this.IsPrimitive)
			{
				writer.WriteStartElement("z", name, ns);
				return;
			}
			writer.WriteStartElement(name, ns);
		}

		// Token: 0x06000A2B RID: 2603 RVA: 0x00002068 File Offset: 0x00000268
		internal virtual DataContract BindGenericParameters(DataContract[] paramContracts, Dictionary<DataContract, DataContract> boundContracts)
		{
			return this;
		}

		// Token: 0x06000A2C RID: 2604 RVA: 0x00002068 File Offset: 0x00000268
		internal virtual DataContract GetValidContract(SerializationMode mode)
		{
			return this;
		}

		// Token: 0x06000A2D RID: 2605 RVA: 0x00002068 File Offset: 0x00000268
		internal virtual DataContract GetValidContract()
		{
			return this;
		}

		// Token: 0x06000A2E RID: 2606 RVA: 0x000066D0 File Offset: 0x000048D0
		internal virtual bool IsValidContract(SerializationMode mode)
		{
			return true;
		}

		// Token: 0x1700019A RID: 410
		// (get) Token: 0x06000A2F RID: 2607 RVA: 0x00029560 File Offset: 0x00027760
		internal MethodInfo ParseMethod
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.ParseMethod;
			}
		}

		// Token: 0x06000A30 RID: 2608 RVA: 0x0002956D File Offset: 0x0002776D
		internal static bool IsTypeSerializable(Type type)
		{
			return DataContract.IsTypeSerializable(type, new Dictionary<Type, object>());
		}

		// Token: 0x06000A31 RID: 2609 RVA: 0x0002957C File Offset: 0x0002777C
		private static bool IsTypeSerializable(Type type, Dictionary<Type, object> previousCollectionTypes)
		{
			if (type.IsSerializable || type.IsDefined(Globals.TypeOfDataContractAttribute, false) || type.IsInterface || type.IsPointer || Globals.TypeOfIXmlSerializable.IsAssignableFrom(type))
			{
				return true;
			}
			Type type2;
			if (CollectionDataContract.IsCollection(type, out type2))
			{
				DataContract.ValidatePreviousCollectionTypes(type, type2, previousCollectionTypes);
				if (DataContract.IsTypeSerializable(type2, previousCollectionTypes))
				{
					return true;
				}
			}
			return DataContract.GetBuiltInDataContract(type) != null || ClassDataContract.IsNonAttributedTypeValidForSerialization(type);
		}

		// Token: 0x06000A32 RID: 2610 RVA: 0x000295EC File Offset: 0x000277EC
		private static void ValidatePreviousCollectionTypes(Type collectionType, Type itemType, Dictionary<Type, object> previousCollectionTypes)
		{
			previousCollectionTypes.Add(collectionType, collectionType);
			while (itemType.IsArray)
			{
				itemType = itemType.GetElementType();
			}
			List<Type> list = new List<Type>();
			Queue<Type> queue = new Queue<Type>();
			queue.Enqueue(itemType);
			list.Add(itemType);
			while (queue.Count > 0)
			{
				itemType = queue.Dequeue();
				if (previousCollectionTypes.ContainsKey(itemType))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' involves recursive collection.", new object[]
					{
						DataContract.GetClrTypeFullName(itemType)
					})));
				}
				if (itemType.IsGenericType)
				{
					foreach (Type item in itemType.GetGenericArguments())
					{
						if (!list.Contains(item))
						{
							queue.Enqueue(item);
							list.Add(item);
						}
					}
				}
			}
		}

		// Token: 0x06000A33 RID: 2611 RVA: 0x000296AC File Offset: 0x000278AC
		internal static Type UnwrapRedundantNullableType(Type type)
		{
			Type result = type;
			while (type.IsGenericType && type.GetGenericTypeDefinition() == Globals.TypeOfNullable)
			{
				result = type;
				type = type.GetGenericArguments()[0];
			}
			return result;
		}

		// Token: 0x06000A34 RID: 2612 RVA: 0x000296E4 File Offset: 0x000278E4
		internal static Type UnwrapNullableType(Type type)
		{
			while (type.IsGenericType && type.GetGenericTypeDefinition() == Globals.TypeOfNullable)
			{
				type = type.GetGenericArguments()[0];
			}
			return type;
		}

		// Token: 0x06000A35 RID: 2613 RVA: 0x0002970D File Offset: 0x0002790D
		private static bool IsAlpha(char ch)
		{
			return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z');
		}

		// Token: 0x06000A36 RID: 2614 RVA: 0x0002972A File Offset: 0x0002792A
		private static bool IsDigit(char ch)
		{
			return ch >= '0' && ch <= '9';
		}

		// Token: 0x06000A37 RID: 2615 RVA: 0x0002973C File Offset: 0x0002793C
		private static bool IsAsciiLocalName(string localName)
		{
			if (localName.Length == 0)
			{
				return false;
			}
			if (!DataContract.IsAlpha(localName[0]))
			{
				return false;
			}
			for (int i = 1; i < localName.Length; i++)
			{
				char ch = localName[i];
				if (!DataContract.IsAlpha(ch) && !DataContract.IsDigit(ch))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000A38 RID: 2616 RVA: 0x0002978F File Offset: 0x0002798F
		internal static string EncodeLocalName(string localName)
		{
			if (DataContract.IsAsciiLocalName(localName))
			{
				return localName;
			}
			if (DataContract.IsValidNCName(localName))
			{
				return localName;
			}
			return XmlConvert.EncodeLocalName(localName);
		}

		// Token: 0x06000A39 RID: 2617 RVA: 0x000297AC File Offset: 0x000279AC
		internal static bool IsValidNCName(string name)
		{
			bool result;
			try
			{
				XmlConvert.VerifyNCName(name);
				result = true;
			}
			catch (XmlException)
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000A3A RID: 2618 RVA: 0x000297DC File Offset: 0x000279DC
		internal static XmlQualifiedName GetStableName(Type type)
		{
			bool flag;
			return DataContract.GetStableName(type, out flag);
		}

		// Token: 0x06000A3B RID: 2619 RVA: 0x000297F1 File Offset: 0x000279F1
		internal static XmlQualifiedName GetStableName(Type type, out bool hasDataContract)
		{
			return DataContract.GetStableName(type, new Dictionary<Type, object>(), out hasDataContract);
		}

		// Token: 0x06000A3C RID: 2620 RVA: 0x00029800 File Offset: 0x00027A00
		private static XmlQualifiedName GetStableName(Type type, Dictionary<Type, object> previousCollectionTypes, out bool hasDataContract)
		{
			type = DataContract.UnwrapRedundantNullableType(type);
			XmlQualifiedName result;
			DataContractAttribute dataContractAttribute;
			if (DataContract.TryGetBuiltInXmlAndArrayTypeStableName(type, previousCollectionTypes, out result))
			{
				hasDataContract = false;
			}
			else if (DataContract.TryGetDCAttribute(type, out dataContractAttribute))
			{
				result = DataContract.GetDCTypeStableName(type, dataContractAttribute);
				hasDataContract = true;
			}
			else
			{
				result = DataContract.GetNonDCTypeStableName(type, previousCollectionTypes);
				hasDataContract = false;
			}
			return result;
		}

		// Token: 0x06000A3D RID: 2621 RVA: 0x00029848 File Offset: 0x00027A48
		private static XmlQualifiedName GetDCTypeStableName(Type type, DataContractAttribute dataContractAttribute)
		{
			string text;
			if (dataContractAttribute.IsNameSetExplicitly)
			{
				text = dataContractAttribute.Name;
				if (text == null || text.Length == 0)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot have DataContractAttribute attribute Name set to null or empty string.", new object[]
					{
						DataContract.GetClrTypeFullName(type)
					})));
				}
				if (type.IsGenericType && !type.IsGenericTypeDefinition)
				{
					text = DataContract.ExpandGenericParameters(text, type);
				}
				text = DataContract.EncodeLocalName(text);
			}
			else
			{
				text = DataContract.GetDefaultStableLocalName(type);
			}
			string text2;
			if (dataContractAttribute.IsNamespaceSetExplicitly)
			{
				text2 = dataContractAttribute.Namespace;
				if (text2 == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot have DataContractAttribute attribute Namespace set to null.", new object[]
					{
						DataContract.GetClrTypeFullName(type)
					})));
				}
				DataContract.CheckExplicitDataContractNamespaceUri(text2, type);
			}
			else
			{
				text2 = DataContract.GetDefaultDataContractNamespace(type);
			}
			return DataContract.CreateQualifiedName(text, text2);
		}

		// Token: 0x06000A3E RID: 2622 RVA: 0x0002990C File Offset: 0x00027B0C
		private static XmlQualifiedName GetNonDCTypeStableName(Type type, Dictionary<Type, object> previousCollectionTypes)
		{
			Type itemType;
			if (CollectionDataContract.IsCollection(type, out itemType))
			{
				DataContract.ValidatePreviousCollectionTypes(type, itemType, previousCollectionTypes);
				CollectionDataContractAttribute collectionDataContractAttribute;
				return DataContract.GetCollectionStableName(type, itemType, previousCollectionTypes, out collectionDataContractAttribute);
			}
			string defaultStableLocalName = DataContract.GetDefaultStableLocalName(type);
			string text;
			if (ClassDataContract.IsNonAttributedTypeValidForSerialization(type))
			{
				text = DataContract.GetDefaultDataContractNamespace(type);
			}
			else
			{
				text = DataContract.GetDefaultStableNamespace(type);
			}
			return DataContract.CreateQualifiedName(defaultStableLocalName, text);
		}

		// Token: 0x06000A3F RID: 2623 RVA: 0x0002995C File Offset: 0x00027B5C
		private static bool TryGetBuiltInXmlAndArrayTypeStableName(Type type, Dictionary<Type, object> previousCollectionTypes, out XmlQualifiedName stableName)
		{
			stableName = null;
			DataContract builtInDataContract = DataContract.GetBuiltInDataContract(type);
			if (builtInDataContract != null)
			{
				stableName = builtInDataContract.StableName;
			}
			else if (Globals.TypeOfIXmlSerializable.IsAssignableFrom(type))
			{
				XmlQualifiedName xmlQualifiedName;
				XmlSchemaType xmlSchemaType;
				bool flag;
				SchemaExporter.GetXmlTypeInfo(type, out xmlQualifiedName, out xmlSchemaType, out flag);
				stableName = xmlQualifiedName;
			}
			else if (type.IsArray)
			{
				Type elementType = type.GetElementType();
				DataContract.ValidatePreviousCollectionTypes(type, elementType, previousCollectionTypes);
				CollectionDataContractAttribute collectionDataContractAttribute;
				stableName = DataContract.GetCollectionStableName(type, elementType, previousCollectionTypes, out collectionDataContractAttribute);
			}
			return stableName != null;
		}

		// Token: 0x06000A40 RID: 2624 RVA: 0x000299CC File Offset: 0x00027BCC
		[SecuritySafeCritical]
		internal static bool TryGetDCAttribute(Type type, out DataContractAttribute dataContractAttribute)
		{
			dataContractAttribute = null;
			object[] customAttributes = type.GetCustomAttributes(Globals.TypeOfDataContractAttribute, false);
			if (customAttributes != null && customAttributes.Length != 0)
			{
				dataContractAttribute = (DataContractAttribute)customAttributes[0];
			}
			return dataContractAttribute != null;
		}

		// Token: 0x06000A41 RID: 2625 RVA: 0x000299FF File Offset: 0x00027BFF
		internal static XmlQualifiedName GetCollectionStableName(Type type, Type itemType, out CollectionDataContractAttribute collectionContractAttribute)
		{
			return DataContract.GetCollectionStableName(type, itemType, new Dictionary<Type, object>(), out collectionContractAttribute);
		}

		// Token: 0x06000A42 RID: 2626 RVA: 0x00029A10 File Offset: 0x00027C10
		private static XmlQualifiedName GetCollectionStableName(Type type, Type itemType, Dictionary<Type, object> previousCollectionTypes, out CollectionDataContractAttribute collectionContractAttribute)
		{
			object[] customAttributes = type.GetCustomAttributes(Globals.TypeOfCollectionDataContractAttribute, false);
			string text;
			string text2;
			if (customAttributes != null && customAttributes.Length != 0)
			{
				collectionContractAttribute = (CollectionDataContractAttribute)customAttributes[0];
				if (collectionContractAttribute.IsNameSetExplicitly)
				{
					text = collectionContractAttribute.Name;
					if (text == null || text.Length == 0)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot have CollectionDataContractAttribute attribute Name set to null or empty string.", new object[]
						{
							DataContract.GetClrTypeFullName(type)
						})));
					}
					if (type.IsGenericType && !type.IsGenericTypeDefinition)
					{
						text = DataContract.ExpandGenericParameters(text, type);
					}
					text = DataContract.EncodeLocalName(text);
				}
				else
				{
					text = DataContract.GetDefaultStableLocalName(type);
				}
				if (collectionContractAttribute.IsNamespaceSetExplicitly)
				{
					text2 = collectionContractAttribute.Namespace;
					if (text2 == null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot have CollectionDataContractAttribute attribute Namespace set to null.", new object[]
						{
							DataContract.GetClrTypeFullName(type)
						})));
					}
					DataContract.CheckExplicitDataContractNamespaceUri(text2, type);
				}
				else
				{
					text2 = DataContract.GetDefaultDataContractNamespace(type);
				}
			}
			else
			{
				collectionContractAttribute = null;
				string str = "ArrayOf" + DataContract.GetArrayPrefix(ref itemType);
				bool flag;
				XmlQualifiedName stableName = DataContract.GetStableName(itemType, previousCollectionTypes, out flag);
				text = str + stableName.Name;
				text2 = DataContract.GetCollectionNamespace(stableName.Namespace);
			}
			return DataContract.CreateQualifiedName(text, text2);
		}

		// Token: 0x06000A43 RID: 2627 RVA: 0x00029B34 File Offset: 0x00027D34
		private static string GetArrayPrefix(ref Type itemType)
		{
			string text = string.Empty;
			while (itemType.IsArray && DataContract.GetBuiltInDataContract(itemType) == null)
			{
				text += "ArrayOf";
				itemType = itemType.GetElementType();
			}
			return text;
		}

		// Token: 0x06000A44 RID: 2628 RVA: 0x00029B74 File Offset: 0x00027D74
		internal XmlQualifiedName GetArrayTypeName(bool isNullable)
		{
			XmlQualifiedName xmlQualifiedName;
			if (this.IsValueType && isNullable)
			{
				GenericInfo genericInfo = new GenericInfo(DataContract.GetStableName(Globals.TypeOfNullable), Globals.TypeOfNullable.FullName);
				genericInfo.Add(new GenericInfo(this.StableName, null));
				genericInfo.AddToLevel(0, 1);
				xmlQualifiedName = genericInfo.GetExpandedStableName();
			}
			else
			{
				xmlQualifiedName = this.StableName;
			}
			string collectionNamespace = DataContract.GetCollectionNamespace(xmlQualifiedName.Namespace);
			return new XmlQualifiedName("ArrayOf" + xmlQualifiedName.Name, collectionNamespace);
		}

		// Token: 0x06000A45 RID: 2629 RVA: 0x00029BEF File Offset: 0x00027DEF
		internal static string GetCollectionNamespace(string elementNs)
		{
			if (!DataContract.IsBuiltInNamespace(elementNs))
			{
				return elementNs;
			}
			return "http://schemas.microsoft.com/2003/10/Serialization/Arrays";
		}

		// Token: 0x06000A46 RID: 2630 RVA: 0x00029C00 File Offset: 0x00027E00
		internal static XmlQualifiedName GetDefaultStableName(Type type)
		{
			return DataContract.CreateQualifiedName(DataContract.GetDefaultStableLocalName(type), DataContract.GetDefaultStableNamespace(type));
		}

		// Token: 0x06000A47 RID: 2631 RVA: 0x00029C14 File Offset: 0x00027E14
		private static string GetDefaultStableLocalName(Type type)
		{
			if (type.IsGenericParameter)
			{
				return "{" + type.GenericParameterPosition + "}";
			}
			string text = null;
			if (type.IsArray)
			{
				text = DataContract.GetArrayPrefix(ref type);
			}
			string text2;
			if (type.DeclaringType == null)
			{
				text2 = type.Name;
			}
			else
			{
				int num = (type.Namespace == null) ? 0 : type.Namespace.Length;
				if (num > 0)
				{
					num++;
				}
				text2 = DataContract.GetClrTypeFullName(type).Substring(num).Replace('+', '.');
			}
			if (text != null)
			{
				text2 = text + text2;
			}
			if (type.IsGenericType)
			{
				StringBuilder stringBuilder = new StringBuilder();
				StringBuilder stringBuilder2 = new StringBuilder();
				bool flag = true;
				int num2 = text2.IndexOf('[');
				if (num2 >= 0)
				{
					text2 = text2.Substring(0, num2);
				}
				IList<int> dataContractNameForGenericName = DataContract.GetDataContractNameForGenericName(text2, stringBuilder);
				bool isGenericTypeDefinition = type.IsGenericTypeDefinition;
				Type[] genericArguments = type.GetGenericArguments();
				for (int i = 0; i < genericArguments.Length; i++)
				{
					Type type2 = genericArguments[i];
					if (isGenericTypeDefinition)
					{
						stringBuilder.Append("{").Append(i).Append("}");
					}
					else
					{
						XmlQualifiedName stableName = DataContract.GetStableName(type2);
						stringBuilder.Append(stableName.Name);
						stringBuilder2.Append(" ").Append(stableName.Namespace);
						if (flag)
						{
							flag = DataContract.IsBuiltInNamespace(stableName.Namespace);
						}
					}
				}
				if (isGenericTypeDefinition)
				{
					stringBuilder.Append("{#}");
				}
				else if (dataContractNameForGenericName.Count > 1 || !flag)
				{
					foreach (int value in dataContractNameForGenericName)
					{
						stringBuilder2.Insert(0, value).Insert(0, " ");
					}
					stringBuilder.Append(DataContract.GetNamespacesDigest(stringBuilder2.ToString()));
				}
				text2 = stringBuilder.ToString();
			}
			return DataContract.EncodeLocalName(text2);
		}

		// Token: 0x06000A48 RID: 2632 RVA: 0x00029E0C File Offset: 0x0002800C
		private static string GetDefaultDataContractNamespace(Type type)
		{
			string text = type.Namespace;
			if (text == null)
			{
				text = string.Empty;
			}
			string text2 = DataContract.GetGlobalDataContractNamespace(text, type.Module);
			if (text2 == null)
			{
				text2 = DataContract.GetGlobalDataContractNamespace(text, type.Assembly);
			}
			if (text2 == null)
			{
				text2 = DataContract.GetDefaultStableNamespace(type);
			}
			else
			{
				DataContract.CheckExplicitDataContractNamespaceUri(text2, type);
			}
			return text2;
		}

		// Token: 0x06000A49 RID: 2633 RVA: 0x00029E5C File Offset: 0x0002805C
		internal static IList<int> GetDataContractNameForGenericName(string typeName, StringBuilder localName)
		{
			List<int> list = new List<int>();
			int num = 0;
			int num2;
			for (;;)
			{
				num2 = typeName.IndexOf('`', num);
				if (num2 < 0)
				{
					break;
				}
				if (localName != null)
				{
					localName.Append(typeName.Substring(num, num2 - num));
				}
				while ((num = typeName.IndexOf('.', num + 1, num2 - num - 1)) >= 0)
				{
					list.Add(0);
				}
				num = typeName.IndexOf('.', num2);
				if (num < 0)
				{
					goto Block_5;
				}
				list.Add(int.Parse(typeName.Substring(num2 + 1, num - num2 - 1), CultureInfo.InvariantCulture));
			}
			if (localName != null)
			{
				localName.Append(typeName.Substring(num));
			}
			list.Add(0);
			goto IL_AE;
			Block_5:
			list.Add(int.Parse(typeName.Substring(num2 + 1), CultureInfo.InvariantCulture));
			IL_AE:
			if (localName != null)
			{
				localName.Append("Of");
			}
			return list;
		}

		// Token: 0x06000A4A RID: 2634 RVA: 0x00029F27 File Offset: 0x00028127
		internal static bool IsBuiltInNamespace(string ns)
		{
			return ns == "http://www.w3.org/2001/XMLSchema" || ns == "http://schemas.microsoft.com/2003/10/Serialization/";
		}

		// Token: 0x06000A4B RID: 2635 RVA: 0x00029F43 File Offset: 0x00028143
		internal static string GetDefaultStableNamespace(Type type)
		{
			if (type.IsGenericParameter)
			{
				return "{ns}";
			}
			return DataContract.GetDefaultStableNamespace(type.Namespace);
		}

		// Token: 0x06000A4C RID: 2636 RVA: 0x00029F5E File Offset: 0x0002815E
		internal static XmlQualifiedName CreateQualifiedName(string localName, string ns)
		{
			return new XmlQualifiedName(localName, DataContract.GetNamespace(ns));
		}

		// Token: 0x06000A4D RID: 2637 RVA: 0x00029F6C File Offset: 0x0002816C
		internal static string GetDefaultStableNamespace(string clrNs)
		{
			if (clrNs == null)
			{
				clrNs = string.Empty;
			}
			return new Uri(Globals.DataContractXsdBaseNamespaceUri, clrNs).AbsoluteUri;
		}

		// Token: 0x06000A4E RID: 2638 RVA: 0x00029F88 File Offset: 0x00028188
		private static void CheckExplicitDataContractNamespaceUri(string dataContractNs, Type type)
		{
			if (dataContractNs.Length > 0)
			{
				string text = dataContractNs.Trim();
				if (text.Length == 0 || text.IndexOf("##", StringComparison.Ordinal) != -1)
				{
					DataContract.ThrowInvalidDataContractException(SR.GetString("DataContract namespace '{0}' is not a valid URI.", new object[]
					{
						dataContractNs
					}), type);
				}
				dataContractNs = text;
			}
			Uri uri;
			if (Uri.TryCreate(dataContractNs, UriKind.RelativeOrAbsolute, out uri))
			{
				if (uri.ToString() == "http://schemas.microsoft.com/2003/10/Serialization/")
				{
					DataContract.ThrowInvalidDataContractException(SR.GetString("DataContract namespace '{0}' cannot be specified since it is reserved.", new object[]
					{
						"http://schemas.microsoft.com/2003/10/Serialization/"
					}), type);
					return;
				}
			}
			else
			{
				DataContract.ThrowInvalidDataContractException(SR.GetString("DataContract namespace '{0}' is not a valid URI.", new object[]
				{
					dataContractNs
				}), type);
			}
		}

		// Token: 0x06000A4F RID: 2639 RVA: 0x0002A02F File Offset: 0x0002822F
		internal static string GetClrTypeFullName(Type type)
		{
			if (type.IsGenericTypeDefinition || !type.ContainsGenericParameters)
			{
				return type.FullName;
			}
			return string.Format(CultureInfo.InvariantCulture, "{0}.{1}", type.Namespace, type.Name);
		}

		// Token: 0x06000A50 RID: 2640 RVA: 0x0002A064 File Offset: 0x00028264
		internal static string GetClrAssemblyName(Type type, out bool hasTypeForwardedFrom)
		{
			hasTypeForwardedFrom = false;
			object[] customAttributes = type.GetCustomAttributes(typeof(TypeForwardedFromAttribute), false);
			if (customAttributes != null && customAttributes.Length != 0)
			{
				TypeForwardedFromAttribute typeForwardedFromAttribute = (TypeForwardedFromAttribute)customAttributes[0];
				hasTypeForwardedFrom = true;
				return typeForwardedFromAttribute.AssemblyFullName;
			}
			return type.Assembly.FullName;
		}

		// Token: 0x06000A51 RID: 2641 RVA: 0x0002A0A9 File Offset: 0x000282A9
		internal static string GetClrTypeFullNameUsingTypeForwardedFromAttribute(Type type)
		{
			if (type.IsArray)
			{
				return DataContract.GetClrTypeFullNameForArray(type);
			}
			return DataContract.GetClrTypeFullNameForNonArrayTypes(type);
		}

		// Token: 0x06000A52 RID: 2642 RVA: 0x0002A0C0 File Offset: 0x000282C0
		private static string GetClrTypeFullNameForArray(Type type)
		{
			return string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", DataContract.GetClrTypeFullNameUsingTypeForwardedFromAttribute(type.GetElementType()), "[", "]");
		}

		// Token: 0x06000A53 RID: 2643 RVA: 0x0002A0E8 File Offset: 0x000282E8
		private static string GetClrTypeFullNameForNonArrayTypes(Type type)
		{
			if (!type.IsGenericType)
			{
				return DataContract.GetClrTypeFullName(type);
			}
			Type[] genericArguments = type.GetGenericArguments();
			StringBuilder stringBuilder = new StringBuilder(type.GetGenericTypeDefinition().FullName).Append("[");
			foreach (Type type2 in genericArguments)
			{
				stringBuilder.Append("[").Append(DataContract.GetClrTypeFullNameUsingTypeForwardedFromAttribute(type2)).Append(",");
				bool flag;
				stringBuilder.Append(" ").Append(DataContract.GetClrAssemblyName(type2, out flag));
				stringBuilder.Append("]").Append(",");
			}
			return stringBuilder.Remove(stringBuilder.Length - 1, 1).Append("]").ToString();
		}

		// Token: 0x06000A54 RID: 2644 RVA: 0x0002A1A8 File Offset: 0x000283A8
		internal static void GetClrNameAndNamespace(string fullTypeName, out string localName, out string ns)
		{
			int num = fullTypeName.LastIndexOf('.');
			if (num < 0)
			{
				ns = string.Empty;
				localName = fullTypeName.Replace('+', '.');
			}
			else
			{
				ns = fullTypeName.Substring(0, num);
				localName = fullTypeName.Substring(num + 1).Replace('+', '.');
			}
			int num2 = localName.IndexOf('[');
			if (num2 >= 0)
			{
				localName = localName.Substring(0, num2);
			}
		}

		// Token: 0x06000A55 RID: 2645 RVA: 0x0002A20E File Offset: 0x0002840E
		internal static void GetDefaultStableName(string fullTypeName, out string localName, out string ns)
		{
			DataContract.GetDefaultStableName(new CodeTypeReference(fullTypeName), out localName, out ns);
		}

		// Token: 0x06000A56 RID: 2646 RVA: 0x0002A220 File Offset: 0x00028420
		private static void GetDefaultStableName(CodeTypeReference typeReference, out string localName, out string ns)
		{
			string baseType = typeReference.BaseType;
			DataContract builtInDataContract = DataContract.GetBuiltInDataContract(baseType);
			if (builtInDataContract != null)
			{
				localName = builtInDataContract.StableName.Name;
				ns = builtInDataContract.StableName.Namespace;
				return;
			}
			DataContract.GetClrNameAndNamespace(baseType, out localName, out ns);
			if (typeReference.TypeArguments.Count > 0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				StringBuilder stringBuilder2 = new StringBuilder();
				bool flag = true;
				IList<int> dataContractNameForGenericName = DataContract.GetDataContractNameForGenericName(localName, stringBuilder);
				foreach (CodeTypeReference typeReference2 in typeReference.TypeArguments)
				{
					string value;
					string value2;
					DataContract.GetDefaultStableName(typeReference2, out value, out value2);
					stringBuilder.Append(value);
					stringBuilder2.Append(" ").Append(value2);
					if (flag)
					{
						flag = DataContract.IsBuiltInNamespace(value2);
					}
				}
				if (dataContractNameForGenericName.Count > 1 || !flag)
				{
					foreach (int value3 in dataContractNameForGenericName)
					{
						stringBuilder2.Insert(0, value3).Insert(0, " ");
					}
					stringBuilder.Append(DataContract.GetNamespacesDigest(stringBuilder2.ToString()));
				}
				localName = stringBuilder.ToString();
			}
			localName = DataContract.EncodeLocalName(localName);
			ns = DataContract.GetDefaultStableNamespace(ns);
		}

		// Token: 0x06000A57 RID: 2647 RVA: 0x0002A384 File Offset: 0x00028584
		internal static string GetDataContractNamespaceFromUri(string uriString)
		{
			if (!uriString.StartsWith("http://schemas.datacontract.org/2004/07/", StringComparison.Ordinal))
			{
				return uriString;
			}
			return uriString.Substring("http://schemas.datacontract.org/2004/07/".Length);
		}

		// Token: 0x06000A58 RID: 2648 RVA: 0x0002A3A8 File Offset: 0x000285A8
		private static string GetGlobalDataContractNamespace(string clrNs, ICustomAttributeProvider customAttribuetProvider)
		{
			object[] customAttributes = customAttribuetProvider.GetCustomAttributes(typeof(ContractNamespaceAttribute), false);
			string text = null;
			foreach (ContractNamespaceAttribute contractNamespaceAttribute in customAttributes)
			{
				string text2 = contractNamespaceAttribute.ClrNamespace;
				if (text2 == null)
				{
					text2 = string.Empty;
				}
				if (text2 == clrNs)
				{
					if (contractNamespaceAttribute.ContractNamespace == null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("CLR namespace '{0}' cannot have ContractNamespace set to null.", new object[]
						{
							clrNs
						})));
					}
					if (text != null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("ContractNamespaceAttribute attribute maps CLR namespace '{2}' to multiple data contract namespaces '{0}' and '{1}'. You can map a CLR namespace to only one data contract namespace.", new object[]
						{
							text,
							contractNamespaceAttribute.ContractNamespace,
							clrNs
						})));
					}
					text = contractNamespaceAttribute.ContractNamespace;
				}
			}
			return text;
		}

		// Token: 0x06000A59 RID: 2649 RVA: 0x0002A464 File Offset: 0x00028664
		private static string GetNamespacesDigest(string namespaces)
		{
			byte[] inArray = HashHelper.ComputeHash(Encoding.UTF8.GetBytes(namespaces));
			char[] array = new char[24];
			int num = Convert.ToBase64CharArray(inArray, 0, 6, array, 0);
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < num; i++)
			{
				char c = array[i];
				if (c != '+')
				{
					if (c != '/')
					{
						if (c != '=')
						{
							stringBuilder.Append(c);
						}
					}
					else
					{
						stringBuilder.Append("_S");
					}
				}
				else
				{
					stringBuilder.Append("_P");
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000A5A RID: 2650 RVA: 0x0002A4EC File Offset: 0x000286EC
		private static string ExpandGenericParameters(string format, Type type)
		{
			GenericNameProvider genericNameProvider = new GenericNameProvider(type);
			return DataContract.ExpandGenericParameters(format, genericNameProvider);
		}

		// Token: 0x06000A5B RID: 2651 RVA: 0x0002A508 File Offset: 0x00028708
		internal static string ExpandGenericParameters(string format, IGenericNameProvider genericNameProvider)
		{
			string text = null;
			StringBuilder stringBuilder = new StringBuilder();
			IList<int> nestedParameterCounts = genericNameProvider.GetNestedParameterCounts();
			for (int i = 0; i < format.Length; i++)
			{
				char c = format[i];
				if (c == '{')
				{
					i++;
					int num = i;
					while (i < format.Length && format[i] != '}')
					{
						i++;
					}
					if (i == format.Length)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("The data contract name '{0}' for type '{1}' has a curly brace '{{' that is not matched with a closing curly brace. Curly braces have special meaning in data contract names - they are used to customize the naming of data contracts for generic types.", new object[]
						{
							format,
							genericNameProvider.GetGenericTypeName()
						})));
					}
					if (format[num] == '#' && i == num + 1)
					{
						if (nestedParameterCounts.Count > 1 || !genericNameProvider.ParametersFromBuiltInNamespaces)
						{
							if (text == null)
							{
								StringBuilder stringBuilder2 = new StringBuilder(genericNameProvider.GetNamespaces());
								foreach (int value in nestedParameterCounts)
								{
									stringBuilder2.Insert(0, value).Insert(0, " ");
								}
								text = DataContract.GetNamespacesDigest(stringBuilder2.ToString());
							}
							stringBuilder.Append(text);
						}
					}
					else
					{
						int num2;
						if (!int.TryParse(format.Substring(num, i - num), out num2) || num2 < 0 || num2 >= genericNameProvider.GetParameterCount())
						{
							throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("In the data contract name for type '{1}', there are curly braces with '{0}' inside, which is an invalid value. Curly braces have special meaning in data contract names - they are used to customize the naming of data contracts for generic types. Based on the number of generic parameters this type has, the contents of the curly braces must either be a number between 0 and '{2}' to insert the name of the generic parameter at that index or the '#' symbol to insert a digest of the generic parameter namespaces.", new object[]
							{
								format.Substring(num, i - num),
								genericNameProvider.GetGenericTypeName(),
								genericNameProvider.GetParameterCount() - 1
							})));
						}
						stringBuilder.Append(genericNameProvider.GetParameterName(num2));
					}
				}
				else
				{
					stringBuilder.Append(c);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000A5C RID: 2652 RVA: 0x0002A6CC File Offset: 0x000288CC
		internal static bool IsTypeNullable(Type type)
		{
			return !type.IsValueType || (type.IsGenericType && type.GetGenericTypeDefinition() == Globals.TypeOfNullable);
		}

		// Token: 0x06000A5D RID: 2653 RVA: 0x0002A6F2 File Offset: 0x000288F2
		public static void ThrowTypeNotSerializable(Type type)
		{
			DataContract.ThrowInvalidDataContractException(SR.GetString("Type '{0}' cannot be serialized. Consider marking it with the DataContractAttribute attribute, and marking all of its members you want serialized with the DataMemberAttribute attribute. Alternatively, you can ensure that the type is public and has a parameterless constructor - all public members of the type will then be serialized, and no attributes will be required.", new object[]
			{
				type
			}), type);
		}

		// Token: 0x06000A5E RID: 2654 RVA: 0x0002A710 File Offset: 0x00028910
		internal static Dictionary<XmlQualifiedName, DataContract> ImportKnownTypeAttributes(Type type)
		{
			Dictionary<XmlQualifiedName, DataContract> result = null;
			Dictionary<Type, Type> typesChecked = new Dictionary<Type, Type>();
			DataContract.ImportKnownTypeAttributes(type, typesChecked, ref result);
			return result;
		}

		// Token: 0x06000A5F RID: 2655 RVA: 0x0002A730 File Offset: 0x00028930
		private static void ImportKnownTypeAttributes(Type type, Dictionary<Type, Type> typesChecked, ref Dictionary<XmlQualifiedName, DataContract> knownDataContracts)
		{
			if (TD.ImportKnownTypesStartIsEnabled())
			{
				TD.ImportKnownTypesStart();
			}
			while (type != null && DataContract.IsTypeSerializable(type))
			{
				if (typesChecked.ContainsKey(type))
				{
					return;
				}
				typesChecked.Add(type, type);
				object[] customAttributes = type.GetCustomAttributes(Globals.TypeOfKnownTypeAttribute, false);
				if (customAttributes != null)
				{
					bool flag = false;
					bool flag2 = false;
					foreach (KnownTypeAttribute knownTypeAttribute in customAttributes)
					{
						if (knownTypeAttribute.Type != null)
						{
							if (flag)
							{
								DataContract.ThrowInvalidDataContractException(SR.GetString("Type '{0}': If a KnownTypeAttribute attribute specifies a method it must be the only KnownTypeAttribute attribute on that type.", new object[]
								{
									DataContract.GetClrTypeFullName(type)
								}), type);
							}
							DataContract.CheckAndAdd(knownTypeAttribute.Type, typesChecked, ref knownDataContracts);
							flag2 = true;
						}
						else
						{
							if (flag || flag2)
							{
								DataContract.ThrowInvalidDataContractException(SR.GetString("Type '{0}': If a KnownTypeAttribute attribute specifies a method it must be the only KnownTypeAttribute attribute on that type.", new object[]
								{
									DataContract.GetClrTypeFullName(type)
								}), type);
							}
							string methodName = knownTypeAttribute.MethodName;
							if (methodName == null)
							{
								DataContract.ThrowInvalidDataContractException(SR.GetString("KnownTypeAttribute attribute on type '{0}' contains no data.", new object[]
								{
									DataContract.GetClrTypeFullName(type)
								}), type);
							}
							if (methodName.Length == 0)
							{
								DataContract.ThrowInvalidDataContractException(SR.GetString("Method name specified by KnownTypeAttribute attribute on type '{0}' cannot be the empty string.", new object[]
								{
									DataContract.GetClrTypeFullName(type)
								}), type);
							}
							MethodInfo method = type.GetMethod(methodName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, Globals.EmptyTypeArray, null);
							if (method == null)
							{
								DataContract.ThrowInvalidDataContractException(SR.GetString("KnownTypeAttribute attribute on type '{1}' specifies a method named '{0}' to provide known types. Static method '{0}()' was not found on this type. Ensure that the method exists and is marked as static.", new object[]
								{
									methodName,
									DataContract.GetClrTypeFullName(type)
								}), type);
							}
							if (!Globals.TypeOfTypeEnumerable.IsAssignableFrom(method.ReturnType))
							{
								DataContract.ThrowInvalidDataContractException(SR.GetString("KnownTypeAttribute attribute on type '{0}' specifies a method named '{1}' to provide known types. The return type of this method is invalid because it is not assignable to IEnumerable<Type>. Ensure that the method exists and has a valid signature.", new object[]
								{
									DataContract.GetClrTypeFullName(type),
									methodName
								}), type);
							}
							object obj = method.Invoke(null, Globals.EmptyObjectArray);
							if (obj == null)
							{
								DataContract.ThrowInvalidDataContractException(SR.GetString("Method specified by KnownTypeAttribute attribute on type '{0}' returned null.", new object[]
								{
									DataContract.GetClrTypeFullName(type)
								}), type);
							}
							foreach (Type type2 in ((IEnumerable<Type>)obj))
							{
								if (type2 == null)
								{
									DataContract.ThrowInvalidDataContractException(SR.GetString("Method specified by KnownTypeAttribute attribute on type '{0}' does not expose valid types.", new object[]
									{
										DataContract.GetClrTypeFullName(type)
									}), type);
								}
								DataContract.CheckAndAdd(type2, typesChecked, ref knownDataContracts);
							}
							flag = true;
						}
					}
				}
				DataContract.LoadKnownTypesFromConfig(type, typesChecked, ref knownDataContracts);
				type = type.BaseType;
			}
			if (TD.ImportKnownTypesStopIsEnabled())
			{
				TD.ImportKnownTypesStop();
			}
		}

		// Token: 0x06000A60 RID: 2656 RVA: 0x000020AE File Offset: 0x000002AE
		[SecuritySafeCritical]
		private static void LoadKnownTypesFromConfig(Type type, Dictionary<Type, Type> typesChecked, ref Dictionary<XmlQualifiedName, DataContract> knownDataContracts)
		{
		}

		// Token: 0x06000A61 RID: 2657 RVA: 0x0002A9A8 File Offset: 0x00028BA8
		private static void CheckRootTypeInConfigIsGeneric(Type type, ref Type rootType, ref Type[] genArgs)
		{
			if (rootType.IsGenericType)
			{
				if (!rootType.ContainsGenericParameters)
				{
					genArgs = rootType.GetGenericArguments();
					rootType = rootType.GetGenericTypeDefinition();
					return;
				}
				DataContract.ThrowInvalidDataContractException(SR.GetString("Error while getting known types for Type '{0}'. The type must not be an open or partial generic class.", new object[]
				{
					type
				}), type);
			}
		}

		// Token: 0x06000A62 RID: 2658 RVA: 0x0002A9F4 File Offset: 0x00028BF4
		private static bool IsElemTypeNullOrNotEqualToRootType(string elemTypeName, Type rootType)
		{
			Type type = Type.GetType(elemTypeName, false);
			return type == null || !rootType.Equals(type);
		}

		// Token: 0x06000A63 RID: 2659 RVA: 0x0002AA20 File Offset: 0x00028C20
		private static bool IsCollectionElementTypeEqualToRootType(string collectionElementTypeName, Type rootType)
		{
			if (collectionElementTypeName.StartsWith(DataContract.GetClrTypeFullName(rootType), StringComparison.Ordinal))
			{
				Type type = Type.GetType(collectionElementTypeName, false);
				if (type != null)
				{
					if (type.IsGenericType && !DataContract.IsOpenGenericType(type))
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentException(SR.GetString("Declared type '{0}' in config cannot be a closed or partial generic type.", new object[]
						{
							collectionElementTypeName
						})));
					}
					if (rootType.Equals(type))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06000A64 RID: 2660 RVA: 0x0002AA88 File Offset: 0x00028C88
		[SecurityCritical]
		[SecurityTreatAsSafe]
		internal static void CheckAndAdd(Type type, Dictionary<Type, Type> typesChecked, ref Dictionary<XmlQualifiedName, DataContract> nameToDataContractTable)
		{
			type = DataContract.UnwrapNullableType(type);
			DataContract dataContract = DataContract.GetDataContract(type);
			DataContract dataContract2;
			if (nameToDataContractTable == null)
			{
				nameToDataContractTable = new Dictionary<XmlQualifiedName, DataContract>();
			}
			else if (nameToDataContractTable.TryGetValue(dataContract.StableName, out dataContract2))
			{
				if (dataContract2.UnderlyingType != DataContract.DataContractCriticalHelper.GetDataContractAdapterType(type))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(SR.GetString("Type '{0}' cannot be added to list of known types since another type '{1}' with the same data contract name '{2}:{3}' is already present.", new object[]
					{
						type,
						dataContract2.UnderlyingType,
						dataContract.StableName.Namespace,
						dataContract.StableName.Name
					})));
				}
				return;
			}
			nameToDataContractTable.Add(dataContract.StableName, dataContract);
			DataContract.ImportKnownTypeAttributes(type, typesChecked, ref nameToDataContractTable);
		}

		// Token: 0x06000A65 RID: 2661 RVA: 0x0002AB30 File Offset: 0x00028D30
		private static bool IsOpenGenericType(Type t)
		{
			Type[] genericArguments = t.GetGenericArguments();
			for (int i = 0; i < genericArguments.Length; i++)
			{
				if (!genericArguments[i].IsGenericParameter)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000A66 RID: 2662 RVA: 0x0002AB5F File Offset: 0x00028D5F
		public sealed override bool Equals(object other)
		{
			return this == other || this.Equals(other, new Dictionary<DataContractPairKey, object>());
		}

		// Token: 0x06000A67 RID: 2663 RVA: 0x0002AB74 File Offset: 0x00028D74
		internal virtual bool Equals(object other, Dictionary<DataContractPairKey, object> checkedContracts)
		{
			DataContract dataContract = other as DataContract;
			return dataContract != null && (this.StableName.Name == dataContract.StableName.Name && this.StableName.Namespace == dataContract.StableName.Namespace) && this.IsReference == dataContract.IsReference;
		}

		// Token: 0x06000A68 RID: 2664 RVA: 0x0002ABD8 File Offset: 0x00028DD8
		internal bool IsEqualOrChecked(object other, Dictionary<DataContractPairKey, object> checkedContracts)
		{
			if (this == other)
			{
				return true;
			}
			if (checkedContracts != null)
			{
				DataContractPairKey key = new DataContractPairKey(this, other);
				if (checkedContracts.ContainsKey(key))
				{
					return true;
				}
				checkedContracts.Add(key, null);
			}
			return false;
		}

		// Token: 0x06000A69 RID: 2665 RVA: 0x0002AC0A File Offset: 0x00028E0A
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000A6A RID: 2666 RVA: 0x0002AC12 File Offset: 0x00028E12
		internal void ThrowInvalidDataContractException(string message)
		{
			DataContract.ThrowInvalidDataContractException(message, this.UnderlyingType);
		}

		// Token: 0x06000A6B RID: 2667 RVA: 0x000066D0 File Offset: 0x000048D0
		internal static bool IsTypeVisible(Type t)
		{
			return true;
		}

		// Token: 0x04000453 RID: 1107
		[SecurityCritical]
		private XmlDictionaryString name;

		// Token: 0x04000454 RID: 1108
		[SecurityCritical]
		private XmlDictionaryString ns;

		// Token: 0x04000455 RID: 1109
		[SecurityCritical]
		private DataContract.DataContractCriticalHelper helper;

		// Token: 0x020000B8 RID: 184
		protected class DataContractCriticalHelper
		{
			// Token: 0x06000A6C RID: 2668 RVA: 0x0002AC20 File Offset: 0x00028E20
			static DataContractCriticalHelper()
			{
				DataContract.DataContractCriticalHelper.typeToIDCache = new Dictionary<TypeHandleRef, IntRef>(new TypeHandleRefEqualityComparer());
				DataContract.DataContractCriticalHelper.dataContractCache = new DataContract[32];
				DataContract.DataContractCriticalHelper.dataContractID = 0;
			}

			// Token: 0x06000A6D RID: 2669 RVA: 0x0002AC8C File Offset: 0x00028E8C
			internal static DataContract GetDataContractSkipValidation(int id, RuntimeTypeHandle typeHandle, Type type)
			{
				DataContract dataContract = DataContract.DataContractCriticalHelper.dataContractCache[id];
				if (dataContract == null)
				{
					return DataContract.DataContractCriticalHelper.CreateDataContract(id, typeHandle, type);
				}
				return dataContract.GetValidContract();
			}

			// Token: 0x06000A6E RID: 2670 RVA: 0x0002ACB8 File Offset: 0x00028EB8
			internal static DataContract GetGetOnlyCollectionDataContractSkipValidation(int id, RuntimeTypeHandle typeHandle, Type type)
			{
				DataContract dataContract = DataContract.DataContractCriticalHelper.dataContractCache[id];
				if (dataContract == null)
				{
					dataContract = DataContract.DataContractCriticalHelper.CreateGetOnlyCollectionDataContract(id, typeHandle, type);
					DataContract.DataContractCriticalHelper.AssignDataContractToId(dataContract, id);
				}
				return dataContract;
			}

			// Token: 0x06000A6F RID: 2671 RVA: 0x0002ACE1 File Offset: 0x00028EE1
			internal static DataContract GetDataContractForInitialization(int id)
			{
				DataContract dataContract = DataContract.DataContractCriticalHelper.dataContractCache[id];
				if (dataContract == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("An internal error has occurred. DataContract cache overflow.")));
				}
				return dataContract;
			}

			// Token: 0x06000A70 RID: 2672 RVA: 0x0002AD04 File Offset: 0x00028F04
			internal static int GetIdForInitialization(ClassDataContract classContract)
			{
				int id = DataContract.GetId(classContract.TypeForInitialization.TypeHandle);
				if (id < DataContract.DataContractCriticalHelper.dataContractCache.Length && DataContract.DataContractCriticalHelper.ContractMatches(classContract, DataContract.DataContractCriticalHelper.dataContractCache[id]))
				{
					return id;
				}
				int num = DataContract.DataContractCriticalHelper.dataContractID;
				for (int i = 0; i < num; i++)
				{
					if (DataContract.DataContractCriticalHelper.ContractMatches(classContract, DataContract.DataContractCriticalHelper.dataContractCache[i]))
					{
						return i;
					}
				}
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("An internal error has occurred. DataContract cache overflow.")));
			}

			// Token: 0x06000A71 RID: 2673 RVA: 0x0002AD74 File Offset: 0x00028F74
			private static bool ContractMatches(DataContract contract, DataContract cachedContract)
			{
				return cachedContract != null && cachedContract.UnderlyingType == contract.UnderlyingType;
			}

			// Token: 0x06000A72 RID: 2674 RVA: 0x0002AD8C File Offset: 0x00028F8C
			internal static int GetId(RuntimeTypeHandle typeHandle)
			{
				object obj = DataContract.DataContractCriticalHelper.cacheLock;
				int value;
				lock (obj)
				{
					typeHandle = DataContract.DataContractCriticalHelper.GetDataContractAdapterTypeHandle(typeHandle);
					DataContract.DataContractCriticalHelper.typeHandleRef.Value = typeHandle;
					IntRef nextId;
					if (!DataContract.DataContractCriticalHelper.typeToIDCache.TryGetValue(DataContract.DataContractCriticalHelper.typeHandleRef, out nextId))
					{
						nextId = DataContract.DataContractCriticalHelper.GetNextId();
						try
						{
							DataContract.DataContractCriticalHelper.typeToIDCache.Add(new TypeHandleRef(typeHandle), nextId);
						}
						catch (Exception ex)
						{
							if (Fx.IsFatal(ex))
							{
								throw;
							}
							throw DiagnosticUtility.ExceptionUtility.ThrowHelperFatal(ex.Message, ex);
						}
					}
					value = nextId.Value;
				}
				return value;
			}

			// Token: 0x06000A73 RID: 2675 RVA: 0x0002AE34 File Offset: 0x00029034
			private static IntRef GetNextId()
			{
				int num = DataContract.DataContractCriticalHelper.dataContractID++;
				if (num >= DataContract.DataContractCriticalHelper.dataContractCache.Length)
				{
					int num2 = (num < 1073741823) ? (num * 2) : int.MaxValue;
					if (num2 <= num)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("An internal error has occurred. DataContract cache overflow.")));
					}
					Array.Resize<DataContract>(ref DataContract.DataContractCriticalHelper.dataContractCache, num2);
				}
				return new IntRef(num);
			}

			// Token: 0x06000A74 RID: 2676 RVA: 0x0002AE98 File Offset: 0x00029098
			private static DataContract CreateDataContract(int id, RuntimeTypeHandle typeHandle, Type type)
			{
				DataContract dataContract = DataContract.DataContractCriticalHelper.dataContractCache[id];
				if (dataContract == null)
				{
					object obj = DataContract.DataContractCriticalHelper.createDataContractLock;
					lock (obj)
					{
						dataContract = DataContract.DataContractCriticalHelper.dataContractCache[id];
						if (dataContract == null)
						{
							if (type == null)
							{
								type = Type.GetTypeFromHandle(typeHandle);
							}
							type = DataContract.UnwrapNullableType(type);
							type = DataContract.DataContractCriticalHelper.GetDataContractAdapterType(type);
							dataContract = DataContract.DataContractCriticalHelper.GetBuiltInDataContract(type);
							if (dataContract == null)
							{
								if (type.IsArray)
								{
									dataContract = new CollectionDataContract(type);
								}
								else if (type.IsEnum)
								{
									dataContract = new EnumDataContract(type);
								}
								else if (type.IsGenericParameter)
								{
									dataContract = new GenericParameterDataContract(type);
								}
								else if (Globals.TypeOfIXmlSerializable.IsAssignableFrom(type))
								{
									dataContract = new XmlDataContract(type);
								}
								else
								{
									if (type.IsPointer)
									{
										type = Globals.TypeOfReflectionPointer;
									}
									if (!CollectionDataContract.TryCreate(type, out dataContract))
									{
										if (type.IsSerializable || type.IsDefined(Globals.TypeOfDataContractAttribute, false) || ClassDataContract.IsNonAttributedTypeValidForSerialization(type))
										{
											dataContract = new ClassDataContract(type);
										}
										else
										{
											DataContract.DataContractCriticalHelper.ThrowInvalidDataContractException(SR.GetString("Type '{0}' cannot be serialized. Consider marking it with the DataContractAttribute attribute, and marking all of its members you want serialized with the DataMemberAttribute attribute. Alternatively, you can ensure that the type is public and has a parameterless constructor - all public members of the type will then be serialized, and no attributes will be required.", new object[]
											{
												type
											}), type);
										}
									}
								}
							}
							DataContract.DataContractCriticalHelper.AssignDataContractToId(dataContract, id);
						}
					}
				}
				return dataContract;
			}

			// Token: 0x06000A75 RID: 2677 RVA: 0x0002AFCC File Offset: 0x000291CC
			[MethodImpl(MethodImplOptions.NoInlining)]
			private static void AssignDataContractToId(DataContract dataContract, int id)
			{
				object obj = DataContract.DataContractCriticalHelper.cacheLock;
				lock (obj)
				{
					DataContract.DataContractCriticalHelper.dataContractCache[id] = dataContract;
				}
			}

			// Token: 0x06000A76 RID: 2678 RVA: 0x0002B010 File Offset: 0x00029210
			private static DataContract CreateGetOnlyCollectionDataContract(int id, RuntimeTypeHandle typeHandle, Type type)
			{
				DataContract dataContract = null;
				object obj = DataContract.DataContractCriticalHelper.createDataContractLock;
				lock (obj)
				{
					dataContract = DataContract.DataContractCriticalHelper.dataContractCache[id];
					if (dataContract == null)
					{
						if (type == null)
						{
							type = Type.GetTypeFromHandle(typeHandle);
						}
						type = DataContract.UnwrapNullableType(type);
						type = DataContract.DataContractCriticalHelper.GetDataContractAdapterType(type);
						if (!CollectionDataContract.TryCreateGetOnlyCollectionDataContract(type, out dataContract))
						{
							DataContract.DataContractCriticalHelper.ThrowInvalidDataContractException(SR.GetString("Type '{0}' cannot be serialized. Consider marking it with the DataContractAttribute attribute, and marking all of its members you want serialized with the DataMemberAttribute attribute. Alternatively, you can ensure that the type is public and has a parameterless constructor - all public members of the type will then be serialized, and no attributes will be required.", new object[]
							{
								type
							}), type);
						}
					}
				}
				return dataContract;
			}

			// Token: 0x06000A77 RID: 2679 RVA: 0x0002B09C File Offset: 0x0002929C
			internal static Type GetDataContractAdapterType(Type type)
			{
				if (type == Globals.TypeOfDateTimeOffset)
				{
					return Globals.TypeOfDateTimeOffsetAdapter;
				}
				return type;
			}

			// Token: 0x06000A78 RID: 2680 RVA: 0x0002B0B2 File Offset: 0x000292B2
			internal static Type GetDataContractOriginalType(Type type)
			{
				if (type == Globals.TypeOfDateTimeOffsetAdapter)
				{
					return Globals.TypeOfDateTimeOffset;
				}
				return type;
			}

			// Token: 0x06000A79 RID: 2681 RVA: 0x0002B0C8 File Offset: 0x000292C8
			private static RuntimeTypeHandle GetDataContractAdapterTypeHandle(RuntimeTypeHandle typeHandle)
			{
				if (Globals.TypeOfDateTimeOffset.TypeHandle.Equals(typeHandle))
				{
					return Globals.TypeOfDateTimeOffsetAdapter.TypeHandle;
				}
				return typeHandle;
			}

			// Token: 0x06000A7A RID: 2682 RVA: 0x0002B0F8 File Offset: 0x000292F8
			public static DataContract GetBuiltInDataContract(Type type)
			{
				if (type.IsInterface && !CollectionDataContract.IsCollectionInterface(type))
				{
					type = Globals.TypeOfObject;
				}
				object obj = DataContract.DataContractCriticalHelper.initBuiltInContractsLock;
				DataContract result;
				lock (obj)
				{
					if (DataContract.DataContractCriticalHelper.typeToBuiltInContract == null)
					{
						DataContract.DataContractCriticalHelper.typeToBuiltInContract = new Dictionary<Type, DataContract>();
					}
					DataContract dataContract = null;
					if (!DataContract.DataContractCriticalHelper.typeToBuiltInContract.TryGetValue(type, out dataContract))
					{
						DataContract.DataContractCriticalHelper.TryCreateBuiltInDataContract(type, out dataContract);
						DataContract.DataContractCriticalHelper.typeToBuiltInContract.Add(type, dataContract);
					}
					result = dataContract;
				}
				return result;
			}

			// Token: 0x06000A7B RID: 2683 RVA: 0x0002B184 File Offset: 0x00029384
			public static DataContract GetBuiltInDataContract(string name, string ns)
			{
				object obj = DataContract.DataContractCriticalHelper.initBuiltInContractsLock;
				DataContract result;
				lock (obj)
				{
					if (DataContract.DataContractCriticalHelper.nameToBuiltInContract == null)
					{
						DataContract.DataContractCriticalHelper.nameToBuiltInContract = new Dictionary<XmlQualifiedName, DataContract>();
					}
					DataContract dataContract = null;
					XmlQualifiedName key = new XmlQualifiedName(name, ns);
					if (!DataContract.DataContractCriticalHelper.nameToBuiltInContract.TryGetValue(key, out dataContract) && DataContract.DataContractCriticalHelper.TryCreateBuiltInDataContract(name, ns, out dataContract))
					{
						DataContract.DataContractCriticalHelper.nameToBuiltInContract.Add(key, dataContract);
					}
					result = dataContract;
				}
				return result;
			}

			// Token: 0x06000A7C RID: 2684 RVA: 0x0002B204 File Offset: 0x00029404
			public static DataContract GetBuiltInDataContract(string typeName)
			{
				if (!typeName.StartsWith("System.", StringComparison.Ordinal))
				{
					return null;
				}
				object obj = DataContract.DataContractCriticalHelper.initBuiltInContractsLock;
				DataContract result;
				lock (obj)
				{
					if (DataContract.DataContractCriticalHelper.typeNameToBuiltInContract == null)
					{
						DataContract.DataContractCriticalHelper.typeNameToBuiltInContract = new Dictionary<string, DataContract>();
					}
					DataContract dataContract = null;
					if (!DataContract.DataContractCriticalHelper.typeNameToBuiltInContract.TryGetValue(typeName, out dataContract))
					{
						Type type = null;
						string a = typeName.Substring(7);
						if (a == "Char")
						{
							type = typeof(char);
						}
						else if (a == "Boolean")
						{
							type = typeof(bool);
						}
						else if (a == "SByte")
						{
							type = typeof(sbyte);
						}
						else if (a == "Byte")
						{
							type = typeof(byte);
						}
						else if (a == "Int16")
						{
							type = typeof(short);
						}
						else if (a == "UInt16")
						{
							type = typeof(ushort);
						}
						else if (a == "Int32")
						{
							type = typeof(int);
						}
						else if (a == "UInt32")
						{
							type = typeof(uint);
						}
						else if (a == "Int64")
						{
							type = typeof(long);
						}
						else if (a == "UInt64")
						{
							type = typeof(ulong);
						}
						else if (a == "Single")
						{
							type = typeof(float);
						}
						else if (a == "Double")
						{
							type = typeof(double);
						}
						else if (a == "Decimal")
						{
							type = typeof(decimal);
						}
						else if (a == "DateTime")
						{
							type = typeof(DateTime);
						}
						else if (a == "String")
						{
							type = typeof(string);
						}
						else if (a == "Byte[]")
						{
							type = typeof(byte[]);
						}
						else if (a == "Object")
						{
							type = typeof(object);
						}
						else if (a == "TimeSpan")
						{
							type = typeof(TimeSpan);
						}
						else if (a == "Guid")
						{
							type = typeof(Guid);
						}
						else if (a == "Uri")
						{
							type = typeof(Uri);
						}
						else if (a == "Xml.XmlQualifiedName")
						{
							type = typeof(XmlQualifiedName);
						}
						else if (a == "Enum")
						{
							type = typeof(Enum);
						}
						else if (a == "ValueType")
						{
							type = typeof(ValueType);
						}
						else if (a == "Array")
						{
							type = typeof(Array);
						}
						else if (a == "Xml.XmlElement")
						{
							type = typeof(XmlElement);
						}
						else if (a == "Xml.XmlNode[]")
						{
							type = typeof(XmlNode[]);
						}
						if (type != null)
						{
							DataContract.DataContractCriticalHelper.TryCreateBuiltInDataContract(type, out dataContract);
						}
						DataContract.DataContractCriticalHelper.typeNameToBuiltInContract.Add(typeName, dataContract);
					}
					result = dataContract;
				}
				return result;
			}

			// Token: 0x06000A7D RID: 2685 RVA: 0x0002B5A8 File Offset: 0x000297A8
			public static bool TryCreateBuiltInDataContract(Type type, out DataContract dataContract)
			{
				if (type.IsEnum)
				{
					dataContract = null;
					return false;
				}
				dataContract = null;
				switch (Type.GetTypeCode(type))
				{
				case TypeCode.Boolean:
					dataContract = new BooleanDataContract();
					goto IL_24C;
				case TypeCode.Char:
					dataContract = new CharDataContract();
					goto IL_24C;
				case TypeCode.SByte:
					dataContract = new SignedByteDataContract();
					goto IL_24C;
				case TypeCode.Byte:
					dataContract = new UnsignedByteDataContract();
					goto IL_24C;
				case TypeCode.Int16:
					dataContract = new ShortDataContract();
					goto IL_24C;
				case TypeCode.UInt16:
					dataContract = new UnsignedShortDataContract();
					goto IL_24C;
				case TypeCode.Int32:
					dataContract = new IntDataContract();
					goto IL_24C;
				case TypeCode.UInt32:
					dataContract = new UnsignedIntDataContract();
					goto IL_24C;
				case TypeCode.Int64:
					dataContract = new LongDataContract();
					goto IL_24C;
				case TypeCode.UInt64:
					dataContract = new UnsignedLongDataContract();
					goto IL_24C;
				case TypeCode.Single:
					dataContract = new FloatDataContract();
					goto IL_24C;
				case TypeCode.Double:
					dataContract = new DoubleDataContract();
					goto IL_24C;
				case TypeCode.Decimal:
					dataContract = new DecimalDataContract();
					goto IL_24C;
				case TypeCode.DateTime:
					dataContract = new DateTimeDataContract();
					goto IL_24C;
				case TypeCode.String:
					dataContract = new StringDataContract();
					goto IL_24C;
				}
				if (type == typeof(byte[]))
				{
					dataContract = new ByteArrayDataContract();
				}
				else if (type == typeof(object))
				{
					dataContract = new ObjectDataContract();
				}
				else if (type == typeof(Uri))
				{
					dataContract = new UriDataContract();
				}
				else if (type == typeof(XmlQualifiedName))
				{
					dataContract = new QNameDataContract();
				}
				else if (type == typeof(TimeSpan))
				{
					dataContract = new TimeSpanDataContract();
				}
				else if (type == typeof(Guid))
				{
					dataContract = new GuidDataContract();
				}
				else if (type == typeof(Enum) || type == typeof(ValueType))
				{
					dataContract = new SpecialTypeDataContract(type, DictionaryGlobals.ObjectLocalName, DictionaryGlobals.SchemaNamespace);
				}
				else if (type == typeof(Array))
				{
					dataContract = new CollectionDataContract(type);
				}
				else if (type == typeof(XmlElement) || type == typeof(XmlNode[]))
				{
					dataContract = new XmlDataContract(type);
				}
				IL_24C:
				return dataContract != null;
			}

			// Token: 0x06000A7E RID: 2686 RVA: 0x0002B808 File Offset: 0x00029A08
			public static bool TryCreateBuiltInDataContract(string name, string ns, out DataContract dataContract)
			{
				dataContract = null;
				if (ns == DictionaryGlobals.SchemaNamespace.Value)
				{
					if (DictionaryGlobals.BooleanLocalName.Value == name)
					{
						dataContract = new BooleanDataContract();
					}
					else if (DictionaryGlobals.SignedByteLocalName.Value == name)
					{
						dataContract = new SignedByteDataContract();
					}
					else if (DictionaryGlobals.UnsignedByteLocalName.Value == name)
					{
						dataContract = new UnsignedByteDataContract();
					}
					else if (DictionaryGlobals.ShortLocalName.Value == name)
					{
						dataContract = new ShortDataContract();
					}
					else if (DictionaryGlobals.UnsignedShortLocalName.Value == name)
					{
						dataContract = new UnsignedShortDataContract();
					}
					else if (DictionaryGlobals.IntLocalName.Value == name)
					{
						dataContract = new IntDataContract();
					}
					else if (DictionaryGlobals.UnsignedIntLocalName.Value == name)
					{
						dataContract = new UnsignedIntDataContract();
					}
					else if (DictionaryGlobals.LongLocalName.Value == name)
					{
						dataContract = new LongDataContract();
					}
					else if (DictionaryGlobals.integerLocalName.Value == name)
					{
						dataContract = new IntegerDataContract();
					}
					else if (DictionaryGlobals.positiveIntegerLocalName.Value == name)
					{
						dataContract = new PositiveIntegerDataContract();
					}
					else if (DictionaryGlobals.negativeIntegerLocalName.Value == name)
					{
						dataContract = new NegativeIntegerDataContract();
					}
					else if (DictionaryGlobals.nonPositiveIntegerLocalName.Value == name)
					{
						dataContract = new NonPositiveIntegerDataContract();
					}
					else if (DictionaryGlobals.nonNegativeIntegerLocalName.Value == name)
					{
						dataContract = new NonNegativeIntegerDataContract();
					}
					else if (DictionaryGlobals.UnsignedLongLocalName.Value == name)
					{
						dataContract = new UnsignedLongDataContract();
					}
					else if (DictionaryGlobals.FloatLocalName.Value == name)
					{
						dataContract = new FloatDataContract();
					}
					else if (DictionaryGlobals.DoubleLocalName.Value == name)
					{
						dataContract = new DoubleDataContract();
					}
					else if (DictionaryGlobals.DecimalLocalName.Value == name)
					{
						dataContract = new DecimalDataContract();
					}
					else if (DictionaryGlobals.DateTimeLocalName.Value == name)
					{
						dataContract = new DateTimeDataContract();
					}
					else if (DictionaryGlobals.StringLocalName.Value == name)
					{
						dataContract = new StringDataContract();
					}
					else if (DictionaryGlobals.timeLocalName.Value == name)
					{
						dataContract = new TimeDataContract();
					}
					else if (DictionaryGlobals.dateLocalName.Value == name)
					{
						dataContract = new DateDataContract();
					}
					else if (DictionaryGlobals.hexBinaryLocalName.Value == name)
					{
						dataContract = new HexBinaryDataContract();
					}
					else if (DictionaryGlobals.gYearMonthLocalName.Value == name)
					{
						dataContract = new GYearMonthDataContract();
					}
					else if (DictionaryGlobals.gYearLocalName.Value == name)
					{
						dataContract = new GYearDataContract();
					}
					else if (DictionaryGlobals.gMonthDayLocalName.Value == name)
					{
						dataContract = new GMonthDayDataContract();
					}
					else if (DictionaryGlobals.gDayLocalName.Value == name)
					{
						dataContract = new GDayDataContract();
					}
					else if (DictionaryGlobals.gMonthLocalName.Value == name)
					{
						dataContract = new GMonthDataContract();
					}
					else if (DictionaryGlobals.normalizedStringLocalName.Value == name)
					{
						dataContract = new NormalizedStringDataContract();
					}
					else if (DictionaryGlobals.tokenLocalName.Value == name)
					{
						dataContract = new TokenDataContract();
					}
					else if (DictionaryGlobals.languageLocalName.Value == name)
					{
						dataContract = new LanguageDataContract();
					}
					else if (DictionaryGlobals.NameLocalName.Value == name)
					{
						dataContract = new NameDataContract();
					}
					else if (DictionaryGlobals.NCNameLocalName.Value == name)
					{
						dataContract = new NCNameDataContract();
					}
					else if (DictionaryGlobals.XSDIDLocalName.Value == name)
					{
						dataContract = new IDDataContract();
					}
					else if (DictionaryGlobals.IDREFLocalName.Value == name)
					{
						dataContract = new IDREFDataContract();
					}
					else if (DictionaryGlobals.IDREFSLocalName.Value == name)
					{
						dataContract = new IDREFSDataContract();
					}
					else if (DictionaryGlobals.ENTITYLocalName.Value == name)
					{
						dataContract = new ENTITYDataContract();
					}
					else if (DictionaryGlobals.ENTITIESLocalName.Value == name)
					{
						dataContract = new ENTITIESDataContract();
					}
					else if (DictionaryGlobals.NMTOKENLocalName.Value == name)
					{
						dataContract = new NMTOKENDataContract();
					}
					else if (DictionaryGlobals.NMTOKENSLocalName.Value == name)
					{
						dataContract = new NMTOKENDataContract();
					}
					else if (DictionaryGlobals.ByteArrayLocalName.Value == name)
					{
						dataContract = new ByteArrayDataContract();
					}
					else if (DictionaryGlobals.ObjectLocalName.Value == name)
					{
						dataContract = new ObjectDataContract();
					}
					else if (DictionaryGlobals.TimeSpanLocalName.Value == name)
					{
						dataContract = new XsDurationDataContract();
					}
					else if (DictionaryGlobals.UriLocalName.Value == name)
					{
						dataContract = new UriDataContract();
					}
					else if (DictionaryGlobals.QNameLocalName.Value == name)
					{
						dataContract = new QNameDataContract();
					}
				}
				else if (ns == DictionaryGlobals.SerializationNamespace.Value)
				{
					if (DictionaryGlobals.TimeSpanLocalName.Value == name)
					{
						dataContract = new TimeSpanDataContract();
					}
					else if (DictionaryGlobals.GuidLocalName.Value == name)
					{
						dataContract = new GuidDataContract();
					}
					else if (DictionaryGlobals.CharLocalName.Value == name)
					{
						dataContract = new CharDataContract();
					}
					else if ("ArrayOfanyType" == name)
					{
						dataContract = new CollectionDataContract(typeof(Array));
					}
				}
				else if (ns == DictionaryGlobals.AsmxTypesNamespace.Value)
				{
					if (DictionaryGlobals.CharLocalName.Value == name)
					{
						dataContract = new AsmxCharDataContract();
					}
					else if (DictionaryGlobals.GuidLocalName.Value == name)
					{
						dataContract = new AsmxGuidDataContract();
					}
				}
				else if (ns == "http://schemas.datacontract.org/2004/07/System.Xml")
				{
					if (name == "XmlElement")
					{
						dataContract = new XmlDataContract(typeof(XmlElement));
					}
					else if (name == "ArrayOfXmlNode")
					{
						dataContract = new XmlDataContract(typeof(XmlNode[]));
					}
				}
				return dataContract != null;
			}

			// Token: 0x06000A7F RID: 2687 RVA: 0x0002BE88 File Offset: 0x0002A088
			internal static string GetNamespace(string key)
			{
				object obj = DataContract.DataContractCriticalHelper.namespacesLock;
				string result;
				lock (obj)
				{
					if (DataContract.DataContractCriticalHelper.namespaces == null)
					{
						DataContract.DataContractCriticalHelper.namespaces = new Dictionary<string, string>();
					}
					string text;
					if (DataContract.DataContractCriticalHelper.namespaces.TryGetValue(key, out text))
					{
						result = text;
					}
					else
					{
						try
						{
							DataContract.DataContractCriticalHelper.namespaces.Add(key, key);
						}
						catch (Exception ex)
						{
							if (Fx.IsFatal(ex))
							{
								throw;
							}
							throw DiagnosticUtility.ExceptionUtility.ThrowHelperFatal(ex.Message, ex);
						}
						result = key;
					}
				}
				return result;
			}

			// Token: 0x06000A80 RID: 2688 RVA: 0x0002BF20 File Offset: 0x0002A120
			internal static XmlDictionaryString GetClrTypeString(string key)
			{
				object obj = DataContract.DataContractCriticalHelper.clrTypeStringsLock;
				XmlDictionaryString result;
				lock (obj)
				{
					if (DataContract.DataContractCriticalHelper.clrTypeStrings == null)
					{
						DataContract.DataContractCriticalHelper.clrTypeStringsDictionary = new XmlDictionary();
						DataContract.DataContractCriticalHelper.clrTypeStrings = new Dictionary<string, XmlDictionaryString>();
						try
						{
							DataContract.DataContractCriticalHelper.clrTypeStrings.Add(Globals.TypeOfInt.Assembly.FullName, DataContract.DataContractCriticalHelper.clrTypeStringsDictionary.Add("0"));
						}
						catch (Exception ex)
						{
							if (Fx.IsFatal(ex))
							{
								throw;
							}
							throw DiagnosticUtility.ExceptionUtility.ThrowHelperFatal(ex.Message, ex);
						}
					}
					XmlDictionaryString xmlDictionaryString;
					if (DataContract.DataContractCriticalHelper.clrTypeStrings.TryGetValue(key, out xmlDictionaryString))
					{
						result = xmlDictionaryString;
					}
					else
					{
						xmlDictionaryString = DataContract.DataContractCriticalHelper.clrTypeStringsDictionary.Add(key);
						try
						{
							DataContract.DataContractCriticalHelper.clrTypeStrings.Add(key, xmlDictionaryString);
						}
						catch (Exception ex2)
						{
							if (Fx.IsFatal(ex2))
							{
								throw;
							}
							throw DiagnosticUtility.ExceptionUtility.ThrowHelperFatal(ex2.Message, ex2);
						}
						result = xmlDictionaryString;
					}
				}
				return result;
			}

			// Token: 0x06000A81 RID: 2689 RVA: 0x0002C020 File Offset: 0x0002A220
			internal static void ThrowInvalidDataContractException(string message, Type type)
			{
				if (type != null)
				{
					object obj = DataContract.DataContractCriticalHelper.cacheLock;
					lock (obj)
					{
						DataContract.DataContractCriticalHelper.typeHandleRef.Value = DataContract.DataContractCriticalHelper.GetDataContractAdapterTypeHandle(type.TypeHandle);
						try
						{
							DataContract.DataContractCriticalHelper.typeToIDCache.Remove(DataContract.DataContractCriticalHelper.typeHandleRef);
						}
						catch (Exception ex)
						{
							if (Fx.IsFatal(ex))
							{
								throw;
							}
							throw DiagnosticUtility.ExceptionUtility.ThrowHelperFatal(ex.Message, ex);
						}
					}
				}
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(message));
			}

			// Token: 0x06000A82 RID: 2690 RVA: 0x00002217 File Offset: 0x00000417
			internal DataContractCriticalHelper()
			{
			}

			// Token: 0x06000A83 RID: 2691 RVA: 0x0002C0B8 File Offset: 0x0002A2B8
			internal DataContractCriticalHelper(Type type)
			{
				this.underlyingType = type;
				this.SetTypeForInitialization(type);
				this.isValueType = type.IsValueType;
			}

			// Token: 0x1700019B RID: 411
			// (get) Token: 0x06000A84 RID: 2692 RVA: 0x0002C0DA File Offset: 0x0002A2DA
			internal Type UnderlyingType
			{
				get
				{
					return this.underlyingType;
				}
			}

			// Token: 0x1700019C RID: 412
			// (get) Token: 0x06000A85 RID: 2693 RVA: 0x0002C0E2 File Offset: 0x0002A2E2
			internal Type OriginalUnderlyingType
			{
				get
				{
					if (this.originalUnderlyingType == null)
					{
						this.originalUnderlyingType = DataContract.DataContractCriticalHelper.GetDataContractOriginalType(this.underlyingType);
					}
					return this.originalUnderlyingType;
				}
			}

			// Token: 0x1700019D RID: 413
			// (get) Token: 0x06000A86 RID: 2694 RVA: 0x0000310F File Offset: 0x0000130F
			internal virtual bool IsBuiltInDataContract
			{
				get
				{
					return false;
				}
			}

			// Token: 0x1700019E RID: 414
			// (get) Token: 0x06000A87 RID: 2695 RVA: 0x0002C109 File Offset: 0x0002A309
			internal Type TypeForInitialization
			{
				get
				{
					return this.typeForInitialization;
				}
			}

			// Token: 0x06000A88 RID: 2696 RVA: 0x0002C111 File Offset: 0x0002A311
			[SecuritySafeCritical]
			private void SetTypeForInitialization(Type classType)
			{
				if (classType.IsSerializable || classType.IsDefined(Globals.TypeOfDataContractAttribute, false))
				{
					this.typeForInitialization = classType;
				}
			}

			// Token: 0x1700019F RID: 415
			// (get) Token: 0x06000A89 RID: 2697 RVA: 0x0002C130 File Offset: 0x0002A330
			// (set) Token: 0x06000A8A RID: 2698 RVA: 0x0002C138 File Offset: 0x0002A338
			internal bool IsReference
			{
				get
				{
					return this.isReference;
				}
				set
				{
					this.isReference = value;
				}
			}

			// Token: 0x170001A0 RID: 416
			// (get) Token: 0x06000A8B RID: 2699 RVA: 0x0002C141 File Offset: 0x0002A341
			// (set) Token: 0x06000A8C RID: 2700 RVA: 0x0002C149 File Offset: 0x0002A349
			internal bool IsValueType
			{
				get
				{
					return this.isValueType;
				}
				set
				{
					this.isValueType = value;
				}
			}

			// Token: 0x170001A1 RID: 417
			// (get) Token: 0x06000A8D RID: 2701 RVA: 0x0002C152 File Offset: 0x0002A352
			// (set) Token: 0x06000A8E RID: 2702 RVA: 0x0002C15A File Offset: 0x0002A35A
			internal XmlQualifiedName StableName
			{
				get
				{
					return this.stableName;
				}
				set
				{
					this.stableName = value;
				}
			}

			// Token: 0x170001A2 RID: 418
			// (get) Token: 0x06000A8F RID: 2703 RVA: 0x0002C163 File Offset: 0x0002A363
			// (set) Token: 0x06000A90 RID: 2704 RVA: 0x0002C16B File Offset: 0x0002A36B
			internal GenericInfo GenericInfo
			{
				get
				{
					return this.genericInfo;
				}
				set
				{
					this.genericInfo = value;
				}
			}

			// Token: 0x170001A3 RID: 419
			// (get) Token: 0x06000A91 RID: 2705 RVA: 0x0001BF2F File Offset: 0x0001A12F
			// (set) Token: 0x06000A92 RID: 2706 RVA: 0x000020AE File Offset: 0x000002AE
			internal virtual Dictionary<XmlQualifiedName, DataContract> KnownDataContracts
			{
				get
				{
					return null;
				}
				set
				{
				}
			}

			// Token: 0x170001A4 RID: 420
			// (get) Token: 0x06000A93 RID: 2707 RVA: 0x0000310F File Offset: 0x0000130F
			// (set) Token: 0x06000A94 RID: 2708 RVA: 0x0002C174 File Offset: 0x0002A374
			internal virtual bool IsISerializable
			{
				get
				{
					return false;
				}
				set
				{
					this.ThrowInvalidDataContractException(SR.GetString("To set IsISerializable, class data cotnract is required."));
				}
			}

			// Token: 0x170001A5 RID: 421
			// (get) Token: 0x06000A95 RID: 2709 RVA: 0x0002C186 File Offset: 0x0002A386
			// (set) Token: 0x06000A96 RID: 2710 RVA: 0x0002C18E File Offset: 0x0002A38E
			internal XmlDictionaryString Name
			{
				get
				{
					return this.name;
				}
				set
				{
					this.name = value;
				}
			}

			// Token: 0x170001A6 RID: 422
			// (get) Token: 0x06000A97 RID: 2711 RVA: 0x0002C197 File Offset: 0x0002A397
			// (set) Token: 0x06000A98 RID: 2712 RVA: 0x0002C19F File Offset: 0x0002A39F
			public XmlDictionaryString Namespace
			{
				get
				{
					return this.ns;
				}
				set
				{
					this.ns = value;
				}
			}

			// Token: 0x170001A7 RID: 423
			// (get) Token: 0x06000A99 RID: 2713 RVA: 0x000066D0 File Offset: 0x000048D0
			// (set) Token: 0x06000A9A RID: 2714 RVA: 0x000020AE File Offset: 0x000002AE
			internal virtual bool HasRoot
			{
				get
				{
					return true;
				}
				set
				{
				}
			}

			// Token: 0x170001A8 RID: 424
			// (get) Token: 0x06000A9B RID: 2715 RVA: 0x0002C186 File Offset: 0x0002A386
			// (set) Token: 0x06000A9C RID: 2716 RVA: 0x0002C18E File Offset: 0x0002A38E
			internal virtual XmlDictionaryString TopLevelElementName
			{
				get
				{
					return this.name;
				}
				set
				{
					this.name = value;
				}
			}

			// Token: 0x170001A9 RID: 425
			// (get) Token: 0x06000A9D RID: 2717 RVA: 0x0002C197 File Offset: 0x0002A397
			// (set) Token: 0x06000A9E RID: 2718 RVA: 0x0002C19F File Offset: 0x0002A39F
			internal virtual XmlDictionaryString TopLevelElementNamespace
			{
				get
				{
					return this.ns;
				}
				set
				{
					this.ns = value;
				}
			}

			// Token: 0x170001AA RID: 426
			// (get) Token: 0x06000A9F RID: 2719 RVA: 0x000066D0 File Offset: 0x000048D0
			internal virtual bool CanContainReferences
			{
				get
				{
					return true;
				}
			}

			// Token: 0x170001AB RID: 427
			// (get) Token: 0x06000AA0 RID: 2720 RVA: 0x0000310F File Offset: 0x0000130F
			internal virtual bool IsPrimitive
			{
				get
				{
					return false;
				}
			}

			// Token: 0x170001AC RID: 428
			// (get) Token: 0x06000AA1 RID: 2721 RVA: 0x0002C1A8 File Offset: 0x0002A3A8
			internal MethodInfo ParseMethod
			{
				get
				{
					if (!this.parseMethodSet)
					{
						MethodInfo method = this.UnderlyingType.GetMethod("Parse", BindingFlags.Static | BindingFlags.Public, null, new Type[]
						{
							Globals.TypeOfString
						}, null);
						if (method != null && method.ReturnType == this.UnderlyingType)
						{
							this.parseMethod = method;
						}
						this.parseMethodSet = true;
					}
					return this.parseMethod;
				}
			}

			// Token: 0x06000AA2 RID: 2722 RVA: 0x0002C210 File Offset: 0x0002A410
			internal virtual void WriteRootElement(XmlWriterDelegator writer, XmlDictionaryString name, XmlDictionaryString ns)
			{
				if (ns == DictionaryGlobals.SerializationNamespace && !this.IsPrimitive)
				{
					writer.WriteStartElement("z", name, ns);
					return;
				}
				writer.WriteStartElement(name, ns);
			}

			// Token: 0x06000AA3 RID: 2723 RVA: 0x0002C238 File Offset: 0x0002A438
			internal void SetDataContractName(XmlQualifiedName stableName)
			{
				XmlDictionary xmlDictionary = new XmlDictionary(2);
				this.Name = xmlDictionary.Add(stableName.Name);
				this.Namespace = xmlDictionary.Add(stableName.Namespace);
				this.StableName = stableName;
			}

			// Token: 0x06000AA4 RID: 2724 RVA: 0x0002C277 File Offset: 0x0002A477
			internal void SetDataContractName(XmlDictionaryString name, XmlDictionaryString ns)
			{
				this.Name = name;
				this.Namespace = ns;
				this.StableName = DataContract.CreateQualifiedName(name.Value, ns.Value);
			}

			// Token: 0x06000AA5 RID: 2725 RVA: 0x0002C29E File Offset: 0x0002A49E
			internal void ThrowInvalidDataContractException(string message)
			{
				DataContract.DataContractCriticalHelper.ThrowInvalidDataContractException(message, this.UnderlyingType);
			}

			// Token: 0x04000456 RID: 1110
			private static Dictionary<TypeHandleRef, IntRef> typeToIDCache;

			// Token: 0x04000457 RID: 1111
			private static DataContract[] dataContractCache;

			// Token: 0x04000458 RID: 1112
			private static int dataContractID;

			// Token: 0x04000459 RID: 1113
			private static Dictionary<Type, DataContract> typeToBuiltInContract;

			// Token: 0x0400045A RID: 1114
			private static Dictionary<XmlQualifiedName, DataContract> nameToBuiltInContract;

			// Token: 0x0400045B RID: 1115
			private static Dictionary<string, DataContract> typeNameToBuiltInContract;

			// Token: 0x0400045C RID: 1116
			private static Dictionary<string, string> namespaces;

			// Token: 0x0400045D RID: 1117
			private static Dictionary<string, XmlDictionaryString> clrTypeStrings;

			// Token: 0x0400045E RID: 1118
			private static XmlDictionary clrTypeStringsDictionary;

			// Token: 0x0400045F RID: 1119
			private static TypeHandleRef typeHandleRef = new TypeHandleRef();

			// Token: 0x04000460 RID: 1120
			private static object cacheLock = new object();

			// Token: 0x04000461 RID: 1121
			private static object createDataContractLock = new object();

			// Token: 0x04000462 RID: 1122
			private static object initBuiltInContractsLock = new object();

			// Token: 0x04000463 RID: 1123
			private static object namespacesLock = new object();

			// Token: 0x04000464 RID: 1124
			private static object clrTypeStringsLock = new object();

			// Token: 0x04000465 RID: 1125
			private readonly Type underlyingType;

			// Token: 0x04000466 RID: 1126
			private Type originalUnderlyingType;

			// Token: 0x04000467 RID: 1127
			private bool isReference;

			// Token: 0x04000468 RID: 1128
			private bool isValueType;

			// Token: 0x04000469 RID: 1129
			private XmlQualifiedName stableName;

			// Token: 0x0400046A RID: 1130
			private GenericInfo genericInfo;

			// Token: 0x0400046B RID: 1131
			private XmlDictionaryString name;

			// Token: 0x0400046C RID: 1132
			private XmlDictionaryString ns;

			// Token: 0x0400046D RID: 1133
			private Type typeForInitialization;

			// Token: 0x0400046E RID: 1134
			private MethodInfo parseMethod;

			// Token: 0x0400046F RID: 1135
			private bool parseMethodSet;
		}
	}
}
