﻿using System;
using System.Runtime.Diagnostics;

namespace System.Runtime.Serialization
{
	// Token: 0x0200014F RID: 335
	internal static class FxTrace
	{
		// Token: 0x170003B8 RID: 952
		// (get) Token: 0x06001126 RID: 4390 RVA: 0x0003D3A6 File Offset: 0x0003B5A6
		public static EtwDiagnosticTrace Trace
		{
			get
			{
				return Fx.Trace;
			}
		}

		// Token: 0x170003B9 RID: 953
		// (get) Token: 0x06001127 RID: 4391 RVA: 0x0003D3AD File Offset: 0x0003B5AD
		public static ExceptionTrace Exception
		{
			get
			{
				return new ExceptionTrace("System.Runtime.Serialization", FxTrace.Trace);
			}
		}

		// Token: 0x06001128 RID: 4392 RVA: 0x0000310F File Offset: 0x0000130F
		public static bool IsEventEnabled(int index)
		{
			return false;
		}

		// Token: 0x06001129 RID: 4393 RVA: 0x000020AE File Offset: 0x000002AE
		public static void UpdateEventDefinitions(EventDescriptor[] ed, ushort[] events)
		{
		}

		// Token: 0x0600112A RID: 4394 RVA: 0x0003D3BE File Offset: 0x0003B5BE
		// Note: this type is marked as 'beforefieldinit'.
		static FxTrace()
		{
		}

		// Token: 0x04000707 RID: 1799
		public static bool ShouldTraceError = true;

		// Token: 0x04000708 RID: 1800
		public static bool ShouldTraceVerbose = true;
	}
}
