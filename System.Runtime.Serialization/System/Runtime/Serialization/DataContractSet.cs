﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000C4 RID: 196
	internal class DataContractSet
	{
		// Token: 0x06000B23 RID: 2851 RVA: 0x0002CFEF File Offset: 0x0002B1EF
		internal DataContractSet(IDataContractSurrogate dataContractSurrogate) : this(dataContractSurrogate, null, null)
		{
		}

		// Token: 0x06000B24 RID: 2852 RVA: 0x0002CFFA File Offset: 0x0002B1FA
		internal DataContractSet(IDataContractSurrogate dataContractSurrogate, ICollection<Type> referencedTypes, ICollection<Type> referencedCollectionTypes)
		{
			this.dataContractSurrogate = dataContractSurrogate;
			this.referencedTypes = referencedTypes;
			this.referencedCollectionTypes = referencedCollectionTypes;
		}

		// Token: 0x06000B25 RID: 2853 RVA: 0x0002D018 File Offset: 0x0002B218
		internal DataContractSet(DataContractSet dataContractSet)
		{
			if (dataContractSet == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentNullException("dataContractSet"));
			}
			this.dataContractSurrogate = dataContractSet.dataContractSurrogate;
			this.referencedTypes = dataContractSet.referencedTypes;
			this.referencedCollectionTypes = dataContractSet.referencedCollectionTypes;
			foreach (KeyValuePair<XmlQualifiedName, DataContract> keyValuePair in dataContractSet)
			{
				this.Add(keyValuePair.Key, keyValuePair.Value);
			}
			if (dataContractSet.processedContracts != null)
			{
				foreach (KeyValuePair<DataContract, object> keyValuePair2 in dataContractSet.processedContracts)
				{
					this.ProcessedContracts.Add(keyValuePair2.Key, keyValuePair2.Value);
				}
			}
		}

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x06000B26 RID: 2854 RVA: 0x0002D108 File Offset: 0x0002B308
		private Dictionary<XmlQualifiedName, DataContract> Contracts
		{
			get
			{
				if (this.contracts == null)
				{
					this.contracts = new Dictionary<XmlQualifiedName, DataContract>();
				}
				return this.contracts;
			}
		}

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x06000B27 RID: 2855 RVA: 0x0002D123 File Offset: 0x0002B323
		private Dictionary<DataContract, object> ProcessedContracts
		{
			get
			{
				if (this.processedContracts == null)
				{
					this.processedContracts = new Dictionary<DataContract, object>();
				}
				return this.processedContracts;
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x06000B28 RID: 2856 RVA: 0x0002D13E File Offset: 0x0002B33E
		private Hashtable SurrogateDataTable
		{
			get
			{
				if (this.surrogateDataTable == null)
				{
					this.surrogateDataTable = new Hashtable();
				}
				return this.surrogateDataTable;
			}
		}

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x06000B29 RID: 2857 RVA: 0x0002D159 File Offset: 0x0002B359
		// (set) Token: 0x06000B2A RID: 2858 RVA: 0x0002D161 File Offset: 0x0002B361
		internal Dictionary<XmlQualifiedName, DataContract> KnownTypesForObject
		{
			get
			{
				return this.knownTypesForObject;
			}
			set
			{
				this.knownTypesForObject = value;
			}
		}

		// Token: 0x06000B2B RID: 2859 RVA: 0x0002D16C File Offset: 0x0002B36C
		internal void Add(Type type)
		{
			DataContract dataContract = this.GetDataContract(type);
			DataContractSet.EnsureTypeNotGeneric(dataContract.UnderlyingType);
			this.Add(dataContract);
		}

		// Token: 0x06000B2C RID: 2860 RVA: 0x0002D193 File Offset: 0x0002B393
		internal static void EnsureTypeNotGeneric(Type type)
		{
			if (type.ContainsGenericParameters)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Generic type '{0}' is not exportable.", new object[]
				{
					type
				})));
			}
		}

		// Token: 0x06000B2D RID: 2861 RVA: 0x0002D1BC File Offset: 0x0002B3BC
		private void Add(DataContract dataContract)
		{
			this.Add(dataContract.StableName, dataContract);
		}

		// Token: 0x06000B2E RID: 2862 RVA: 0x0002D1CB File Offset: 0x0002B3CB
		public void Add(XmlQualifiedName name, DataContract dataContract)
		{
			if (dataContract.IsBuiltInDataContract)
			{
				return;
			}
			this.InternalAdd(name, dataContract);
		}

		// Token: 0x06000B2F RID: 2863 RVA: 0x0002D1E0 File Offset: 0x0002B3E0
		internal void InternalAdd(XmlQualifiedName name, DataContract dataContract)
		{
			DataContract dataContract2 = null;
			if (this.Contracts.TryGetValue(name, out dataContract2))
			{
				if (!dataContract2.Equals(dataContract))
				{
					if (dataContract.UnderlyingType == null || dataContract2.UnderlyingType == null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(SR.GetString("Duplicate contract in data contract set was found, for '{0}' in '{1}' namespace.", new object[]
						{
							dataContract.StableName.Name,
							dataContract.StableName.Namespace
						})));
					}
					bool flag = DataContract.GetClrTypeFullName(dataContract.UnderlyingType) == DataContract.GetClrTypeFullName(dataContract2.UnderlyingType);
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(SR.GetString("Duplicate type contract in data contract set. Type name '{0}', for data contract '{1}' in '{2}' namespace.", new object[]
					{
						flag ? dataContract.UnderlyingType.AssemblyQualifiedName : DataContract.GetClrTypeFullName(dataContract.UnderlyingType),
						flag ? dataContract2.UnderlyingType.AssemblyQualifiedName : DataContract.GetClrTypeFullName(dataContract2.UnderlyingType),
						dataContract.StableName.Name,
						dataContract.StableName.Namespace
					})));
				}
			}
			else
			{
				this.Contracts.Add(name, dataContract);
				if (dataContract is ClassDataContract)
				{
					this.AddClassDataContract((ClassDataContract)dataContract);
					return;
				}
				if (dataContract is CollectionDataContract)
				{
					this.AddCollectionDataContract((CollectionDataContract)dataContract);
					return;
				}
				if (dataContract is XmlDataContract)
				{
					this.AddXmlDataContract((XmlDataContract)dataContract);
				}
			}
		}

		// Token: 0x06000B30 RID: 2864 RVA: 0x0002D33C File Offset: 0x0002B53C
		private void AddClassDataContract(ClassDataContract classDataContract)
		{
			if (classDataContract.BaseContract != null)
			{
				this.Add(classDataContract.BaseContract.StableName, classDataContract.BaseContract);
			}
			if (!classDataContract.IsISerializable && classDataContract.Members != null)
			{
				for (int i = 0; i < classDataContract.Members.Count; i++)
				{
					DataMember dataMember = classDataContract.Members[i];
					DataContract memberTypeDataContract = this.GetMemberTypeDataContract(dataMember);
					if (this.dataContractSurrogate != null && dataMember.MemberInfo != null)
					{
						object customDataToExport = DataContractSurrogateCaller.GetCustomDataToExport(this.dataContractSurrogate, dataMember.MemberInfo, memberTypeDataContract.UnderlyingType);
						if (customDataToExport != null)
						{
							this.SurrogateDataTable.Add(dataMember, customDataToExport);
						}
					}
					this.Add(memberTypeDataContract.StableName, memberTypeDataContract);
				}
			}
			this.AddKnownDataContracts(classDataContract.KnownDataContracts);
		}

		// Token: 0x06000B31 RID: 2865 RVA: 0x0002D3FC File Offset: 0x0002B5FC
		private void AddCollectionDataContract(CollectionDataContract collectionDataContract)
		{
			if (collectionDataContract.IsDictionary)
			{
				ClassDataContract classDataContract = collectionDataContract.ItemContract as ClassDataContract;
				this.AddClassDataContract(classDataContract);
			}
			else
			{
				DataContract itemTypeDataContract = this.GetItemTypeDataContract(collectionDataContract);
				if (itemTypeDataContract != null)
				{
					this.Add(itemTypeDataContract.StableName, itemTypeDataContract);
				}
			}
			this.AddKnownDataContracts(collectionDataContract.KnownDataContracts);
		}

		// Token: 0x06000B32 RID: 2866 RVA: 0x0002D44A File Offset: 0x0002B64A
		private void AddXmlDataContract(XmlDataContract xmlDataContract)
		{
			this.AddKnownDataContracts(xmlDataContract.KnownDataContracts);
		}

		// Token: 0x06000B33 RID: 2867 RVA: 0x0002D458 File Offset: 0x0002B658
		private void AddKnownDataContracts(Dictionary<XmlQualifiedName, DataContract> knownDataContracts)
		{
			if (knownDataContracts != null)
			{
				foreach (DataContract dataContract in knownDataContracts.Values)
				{
					this.Add(dataContract);
				}
			}
		}

		// Token: 0x06000B34 RID: 2868 RVA: 0x0002D4B0 File Offset: 0x0002B6B0
		internal XmlQualifiedName GetStableName(Type clrType)
		{
			if (this.dataContractSurrogate != null)
			{
				return DataContract.GetStableName(DataContractSurrogateCaller.GetDataContractType(this.dataContractSurrogate, clrType));
			}
			return DataContract.GetStableName(clrType);
		}

		// Token: 0x06000B35 RID: 2869 RVA: 0x0002D4D4 File Offset: 0x0002B6D4
		internal DataContract GetDataContract(Type clrType)
		{
			if (this.dataContractSurrogate == null)
			{
				return DataContract.GetDataContract(clrType);
			}
			DataContract dataContract = DataContract.GetBuiltInDataContract(clrType);
			if (dataContract != null)
			{
				return dataContract;
			}
			Type dataContractType = DataContractSurrogateCaller.GetDataContractType(this.dataContractSurrogate, clrType);
			dataContract = DataContract.GetDataContract(dataContractType);
			if (!this.SurrogateDataTable.Contains(dataContract))
			{
				object customDataToExport = DataContractSurrogateCaller.GetCustomDataToExport(this.dataContractSurrogate, clrType, dataContractType);
				if (customDataToExport != null)
				{
					this.SurrogateDataTable.Add(dataContract, customDataToExport);
				}
			}
			return dataContract;
		}

		// Token: 0x06000B36 RID: 2870 RVA: 0x0002D540 File Offset: 0x0002B740
		internal DataContract GetMemberTypeDataContract(DataMember dataMember)
		{
			if (!(dataMember.MemberInfo != null))
			{
				return dataMember.MemberTypeContract;
			}
			Type memberType = dataMember.MemberType;
			if (!dataMember.IsGetOnlyCollection)
			{
				return this.GetDataContract(memberType);
			}
			if (this.dataContractSurrogate != null && DataContractSurrogateCaller.GetDataContractType(this.dataContractSurrogate, memberType) != memberType)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Surrogates with get-only collections are not supported. Type '{1}' contains '{2}' which is of '{0}' type.", new object[]
				{
					DataContract.GetClrTypeFullName(memberType),
					DataContract.GetClrTypeFullName(dataMember.MemberInfo.DeclaringType),
					dataMember.MemberInfo.Name
				})));
			}
			return DataContract.GetGetOnlyCollectionDataContract(DataContract.GetId(memberType.TypeHandle), memberType.TypeHandle, memberType, SerializationMode.SharedContract);
		}

		// Token: 0x06000B37 RID: 2871 RVA: 0x0002D5F5 File Offset: 0x0002B7F5
		internal DataContract GetItemTypeDataContract(CollectionDataContract collectionContract)
		{
			if (collectionContract.ItemType != null)
			{
				return this.GetDataContract(collectionContract.ItemType);
			}
			return collectionContract.ItemContract;
		}

		// Token: 0x06000B38 RID: 2872 RVA: 0x0002D618 File Offset: 0x0002B818
		internal object GetSurrogateData(object key)
		{
			return this.SurrogateDataTable[key];
		}

		// Token: 0x06000B39 RID: 2873 RVA: 0x0002D626 File Offset: 0x0002B826
		internal void SetSurrogateData(object key, object surrogateData)
		{
			this.SurrogateDataTable[key] = surrogateData;
		}

		// Token: 0x170001D0 RID: 464
		public DataContract this[XmlQualifiedName key]
		{
			get
			{
				DataContract builtInDataContract = DataContract.GetBuiltInDataContract(key.Name, key.Namespace);
				if (builtInDataContract == null)
				{
					this.Contracts.TryGetValue(key, out builtInDataContract);
				}
				return builtInDataContract;
			}
		}

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000B3B RID: 2875 RVA: 0x0002D66A File Offset: 0x0002B86A
		public IDataContractSurrogate DataContractSurrogate
		{
			get
			{
				return this.dataContractSurrogate;
			}
		}

		// Token: 0x06000B3C RID: 2876 RVA: 0x0002D672 File Offset: 0x0002B872
		public bool Remove(XmlQualifiedName key)
		{
			return DataContract.GetBuiltInDataContract(key.Name, key.Namespace) == null && this.Contracts.Remove(key);
		}

		// Token: 0x06000B3D RID: 2877 RVA: 0x0002D695 File Offset: 0x0002B895
		public IEnumerator<KeyValuePair<XmlQualifiedName, DataContract>> GetEnumerator()
		{
			return this.Contracts.GetEnumerator();
		}

		// Token: 0x06000B3E RID: 2878 RVA: 0x0002D6A7 File Offset: 0x0002B8A7
		internal bool IsContractProcessed(DataContract dataContract)
		{
			return this.ProcessedContracts.ContainsKey(dataContract);
		}

		// Token: 0x06000B3F RID: 2879 RVA: 0x0002D6B5 File Offset: 0x0002B8B5
		internal void SetContractProcessed(DataContract dataContract)
		{
			this.ProcessedContracts.Add(dataContract, dataContract);
		}

		// Token: 0x06000B40 RID: 2880 RVA: 0x0002D6C4 File Offset: 0x0002B8C4
		private Dictionary<XmlQualifiedName, object> GetReferencedTypes()
		{
			if (this.referencedTypesDictionary == null)
			{
				this.referencedTypesDictionary = new Dictionary<XmlQualifiedName, object>();
				this.referencedTypesDictionary.Add(DataContract.GetStableName(Globals.TypeOfNullable), Globals.TypeOfNullable);
				if (this.referencedTypes != null)
				{
					foreach (Type type in this.referencedTypes)
					{
						if (type == null)
						{
							throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(SR.GetString("Referenced types cannot contain null.")));
						}
						this.AddReferencedType(this.referencedTypesDictionary, type);
					}
				}
			}
			return this.referencedTypesDictionary;
		}

		// Token: 0x06000B41 RID: 2881 RVA: 0x0002D774 File Offset: 0x0002B974
		private Dictionary<XmlQualifiedName, object> GetReferencedCollectionTypes()
		{
			if (this.referencedCollectionTypesDictionary == null)
			{
				this.referencedCollectionTypesDictionary = new Dictionary<XmlQualifiedName, object>();
				if (this.referencedCollectionTypes != null)
				{
					foreach (Type type in this.referencedCollectionTypes)
					{
						if (type == null)
						{
							throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(SR.GetString("Referenced collection types cannot contain null.")));
						}
						this.AddReferencedType(this.referencedCollectionTypesDictionary, type);
					}
				}
				XmlQualifiedName stableName = DataContract.GetStableName(Globals.TypeOfDictionaryGeneric);
				if (!this.referencedCollectionTypesDictionary.ContainsKey(stableName) && this.GetReferencedTypes().ContainsKey(stableName))
				{
					this.AddReferencedType(this.referencedCollectionTypesDictionary, Globals.TypeOfDictionaryGeneric);
				}
			}
			return this.referencedCollectionTypesDictionary;
		}

		// Token: 0x06000B42 RID: 2882 RVA: 0x0002D844 File Offset: 0x0002BA44
		private void AddReferencedType(Dictionary<XmlQualifiedName, object> referencedTypes, Type type)
		{
			if (DataContractSet.IsTypeReferenceable(type))
			{
				XmlQualifiedName stableName;
				try
				{
					stableName = this.GetStableName(type);
				}
				catch (InvalidDataContractException)
				{
					return;
				}
				catch (InvalidOperationException)
				{
					return;
				}
				object obj;
				if (referencedTypes.TryGetValue(stableName, out obj))
				{
					Type type2 = obj as Type;
					if (type2 != null)
					{
						if (type2 != type)
						{
							referencedTypes.Remove(stableName);
							referencedTypes.Add(stableName, new List<Type>
							{
								type2,
								type
							});
							return;
						}
					}
					else
					{
						List<Type> list = (List<Type>)obj;
						if (!list.Contains(type))
						{
							list.Add(type);
							return;
						}
					}
				}
				else
				{
					referencedTypes.Add(stableName, type);
				}
			}
		}

		// Token: 0x06000B43 RID: 2883 RVA: 0x0002D8F4 File Offset: 0x0002BAF4
		internal bool TryGetReferencedType(XmlQualifiedName stableName, DataContract dataContract, out Type type)
		{
			return this.TryGetReferencedType(stableName, dataContract, false, out type);
		}

		// Token: 0x06000B44 RID: 2884 RVA: 0x0002D900 File Offset: 0x0002BB00
		internal bool TryGetReferencedCollectionType(XmlQualifiedName stableName, DataContract dataContract, out Type type)
		{
			return this.TryGetReferencedType(stableName, dataContract, true, out type);
		}

		// Token: 0x06000B45 RID: 2885 RVA: 0x0002D90C File Offset: 0x0002BB0C
		private bool TryGetReferencedType(XmlQualifiedName stableName, DataContract dataContract, bool useReferencedCollectionTypes, out Type type)
		{
			object obj;
			if (!(useReferencedCollectionTypes ? this.GetReferencedCollectionTypes() : this.GetReferencedTypes()).TryGetValue(stableName, out obj))
			{
				type = null;
				return false;
			}
			type = (obj as Type);
			if (type != null)
			{
				return true;
			}
			List<Type> list = (List<Type>)obj;
			StringBuilder stringBuilder = new StringBuilder();
			bool flag = false;
			for (int i = 0; i < list.Count; i++)
			{
				Type type2 = list[i];
				if (!flag)
				{
					flag = type2.IsGenericTypeDefinition;
				}
				stringBuilder.AppendFormat("{0}\"{1}\" ", Environment.NewLine, type2.AssemblyQualifiedName);
				if (dataContract != null)
				{
					DataContract dataContract2 = this.GetDataContract(type2);
					stringBuilder.Append(SR.GetString((dataContract2 != null && dataContract2.Equals(dataContract)) ? "Reference type matches." : "Reference type does not match."));
				}
			}
			if (flag)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(SR.GetString(useReferencedCollectionTypes ? "Ambiguous collection types were referenced: {0}" : "Ambiguous types were referenced: {0}", new object[]
				{
					stringBuilder.ToString()
				})));
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(SR.GetString(useReferencedCollectionTypes ? "In '{0}' element in '{1}' namespace, ambiguous collection types were referenced: {2}" : "In '{0}' element in '{1}' namespace, ambiguous types were referenced: {2}", new object[]
			{
				XmlConvert.DecodeName(stableName.Name),
				stableName.Namespace,
				stringBuilder.ToString()
			})));
		}

		// Token: 0x06000B46 RID: 2886 RVA: 0x0002DA4C File Offset: 0x0002BC4C
		private static bool IsTypeReferenceable(Type type)
		{
			try
			{
				Type type2;
				return type.IsSerializable || type.IsDefined(Globals.TypeOfDataContractAttribute, false) || (Globals.TypeOfIXmlSerializable.IsAssignableFrom(type) && !type.IsGenericTypeDefinition) || CollectionDataContract.IsCollection(type, out type2) || ClassDataContract.IsNonAttributedTypeValidForSerialization(type);
			}
			catch (Exception exception)
			{
				if (Fx.IsFatal(exception))
				{
					throw;
				}
			}
			return false;
		}

		// Token: 0x04000498 RID: 1176
		private Dictionary<XmlQualifiedName, DataContract> contracts;

		// Token: 0x04000499 RID: 1177
		private Dictionary<DataContract, object> processedContracts;

		// Token: 0x0400049A RID: 1178
		private IDataContractSurrogate dataContractSurrogate;

		// Token: 0x0400049B RID: 1179
		private Hashtable surrogateDataTable;

		// Token: 0x0400049C RID: 1180
		private Dictionary<XmlQualifiedName, DataContract> knownTypesForObject;

		// Token: 0x0400049D RID: 1181
		private ICollection<Type> referencedTypes;

		// Token: 0x0400049E RID: 1182
		private ICollection<Type> referencedCollectionTypes;

		// Token: 0x0400049F RID: 1183
		private Dictionary<XmlQualifiedName, object> referencedTypesDictionary;

		// Token: 0x040004A0 RID: 1184
		private Dictionary<XmlQualifiedName, object> referencedCollectionTypesDictionary;
	}
}
