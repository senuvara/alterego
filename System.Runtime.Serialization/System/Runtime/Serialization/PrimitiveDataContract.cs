﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000EC RID: 236
	internal abstract class PrimitiveDataContract : DataContract
	{
		// Token: 0x06000D47 RID: 3399 RVA: 0x00032E89 File Offset: 0x00031089
		[SecuritySafeCritical]
		protected PrimitiveDataContract(Type type, XmlDictionaryString name, XmlDictionaryString ns) : base(new PrimitiveDataContract.PrimitiveDataContractCriticalHelper(type, name, ns))
		{
			this.helper = (base.Helper as PrimitiveDataContract.PrimitiveDataContractCriticalHelper);
		}

		// Token: 0x06000D48 RID: 3400 RVA: 0x00032EAA File Offset: 0x000310AA
		internal static PrimitiveDataContract GetPrimitiveDataContract(Type type)
		{
			return DataContract.GetBuiltInDataContract(type) as PrimitiveDataContract;
		}

		// Token: 0x06000D49 RID: 3401 RVA: 0x00032EB7 File Offset: 0x000310B7
		internal static PrimitiveDataContract GetPrimitiveDataContract(string name, string ns)
		{
			return DataContract.GetBuiltInDataContract(name, ns) as PrimitiveDataContract;
		}

		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x06000D4A RID: 3402
		internal abstract string WriteMethodName { get; }

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x06000D4B RID: 3403
		internal abstract string ReadMethodName { get; }

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x06000D4C RID: 3404 RVA: 0x00032EC5 File Offset: 0x000310C5
		// (set) Token: 0x06000D4D RID: 3405 RVA: 0x000020AE File Offset: 0x000002AE
		internal override XmlDictionaryString TopLevelElementNamespace
		{
			get
			{
				return DictionaryGlobals.SerializationNamespace;
			}
			set
			{
			}
		}

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x06000D4E RID: 3406 RVA: 0x0000310F File Offset: 0x0000130F
		internal override bool CanContainReferences
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x06000D4F RID: 3407 RVA: 0x000066D0 File Offset: 0x000048D0
		internal override bool IsPrimitive
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06000D50 RID: 3408 RVA: 0x000066D0 File Offset: 0x000048D0
		internal override bool IsBuiltInDataContract
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06000D51 RID: 3409 RVA: 0x00032ECC File Offset: 0x000310CC
		internal MethodInfo XmlFormatWriterMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.XmlFormatWriterMethod == null)
				{
					if (base.UnderlyingType.IsValueType)
					{
						this.helper.XmlFormatWriterMethod = typeof(XmlWriterDelegator).GetMethod(this.WriteMethodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
						{
							base.UnderlyingType,
							typeof(XmlDictionaryString),
							typeof(XmlDictionaryString)
						}, null);
					}
					else
					{
						this.helper.XmlFormatWriterMethod = typeof(XmlObjectSerializerWriteContext).GetMethod(this.WriteMethodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
						{
							typeof(XmlWriterDelegator),
							base.UnderlyingType,
							typeof(XmlDictionaryString),
							typeof(XmlDictionaryString)
						}, null);
					}
				}
				return this.helper.XmlFormatWriterMethod;
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x06000D52 RID: 3410 RVA: 0x00032FB0 File Offset: 0x000311B0
		internal MethodInfo XmlFormatContentWriterMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.XmlFormatContentWriterMethod == null)
				{
					if (base.UnderlyingType.IsValueType)
					{
						this.helper.XmlFormatContentWriterMethod = typeof(XmlWriterDelegator).GetMethod(this.WriteMethodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
						{
							base.UnderlyingType
						}, null);
					}
					else
					{
						this.helper.XmlFormatContentWriterMethod = typeof(XmlObjectSerializerWriteContext).GetMethod(this.WriteMethodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
						{
							typeof(XmlWriterDelegator),
							base.UnderlyingType
						}, null);
					}
				}
				return this.helper.XmlFormatContentWriterMethod;
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06000D53 RID: 3411 RVA: 0x00033060 File Offset: 0x00031260
		internal MethodInfo XmlFormatReaderMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.XmlFormatReaderMethod == null)
				{
					this.helper.XmlFormatReaderMethod = typeof(XmlReaderDelegator).GetMethod(this.ReadMethodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return this.helper.XmlFormatReaderMethod;
			}
		}

		// Token: 0x06000D54 RID: 3412 RVA: 0x000330AD File Offset: 0x000312AD
		public override void WriteXmlValue(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContext context)
		{
			xmlWriter.WriteAnyType(obj);
		}

		// Token: 0x06000D55 RID: 3413 RVA: 0x000330B6 File Offset: 0x000312B6
		protected object HandleReadValue(object obj, XmlObjectSerializerReadContext context)
		{
			context.AddNewObject(obj);
			return obj;
		}

		// Token: 0x06000D56 RID: 3414 RVA: 0x000330C0 File Offset: 0x000312C0
		protected bool TryReadNullAtTopLevel(XmlReaderDelegator reader)
		{
			Attributes attributes = new Attributes();
			attributes.Read(reader);
			if (attributes.Ref != Globals.NewObjectId)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Cannot deserialize since root element references unrecognized object with id '{0}'.", new object[]
				{
					attributes.Ref
				})));
			}
			if (attributes.XsiNil)
			{
				reader.Skip();
				return true;
			}
			return false;
		}

		// Token: 0x06000D57 RID: 3415 RVA: 0x00033124 File Offset: 0x00031324
		internal override bool Equals(object other, Dictionary<DataContractPairKey, object> checkedContracts)
		{
			if (other is PrimitiveDataContract)
			{
				Type type = base.GetType();
				Type type2 = other.GetType();
				return type.Equals(type2) || type.IsSubclassOf(type2) || type2.IsSubclassOf(type);
			}
			return false;
		}

		// Token: 0x0400063E RID: 1598
		[SecurityCritical]
		private PrimitiveDataContract.PrimitiveDataContractCriticalHelper helper;

		// Token: 0x020000ED RID: 237
		private class PrimitiveDataContractCriticalHelper : DataContract.DataContractCriticalHelper
		{
			// Token: 0x06000D58 RID: 3416 RVA: 0x00033164 File Offset: 0x00031364
			internal PrimitiveDataContractCriticalHelper(Type type, XmlDictionaryString name, XmlDictionaryString ns) : base(type)
			{
				base.SetDataContractName(name, ns);
			}

			// Token: 0x170002CB RID: 715
			// (get) Token: 0x06000D59 RID: 3417 RVA: 0x00033175 File Offset: 0x00031375
			// (set) Token: 0x06000D5A RID: 3418 RVA: 0x0003317D File Offset: 0x0003137D
			internal MethodInfo XmlFormatWriterMethod
			{
				get
				{
					return this.xmlFormatWriterMethod;
				}
				set
				{
					this.xmlFormatWriterMethod = value;
				}
			}

			// Token: 0x170002CC RID: 716
			// (get) Token: 0x06000D5B RID: 3419 RVA: 0x00033186 File Offset: 0x00031386
			// (set) Token: 0x06000D5C RID: 3420 RVA: 0x0003318E File Offset: 0x0003138E
			internal MethodInfo XmlFormatContentWriterMethod
			{
				get
				{
					return this.xmlFormatContentWriterMethod;
				}
				set
				{
					this.xmlFormatContentWriterMethod = value;
				}
			}

			// Token: 0x170002CD RID: 717
			// (get) Token: 0x06000D5D RID: 3421 RVA: 0x00033197 File Offset: 0x00031397
			// (set) Token: 0x06000D5E RID: 3422 RVA: 0x0003319F File Offset: 0x0003139F
			internal MethodInfo XmlFormatReaderMethod
			{
				get
				{
					return this.xmlFormatReaderMethod;
				}
				set
				{
					this.xmlFormatReaderMethod = value;
				}
			}

			// Token: 0x0400063F RID: 1599
			private MethodInfo xmlFormatWriterMethod;

			// Token: 0x04000640 RID: 1600
			private MethodInfo xmlFormatContentWriterMethod;

			// Token: 0x04000641 RID: 1601
			private MethodInfo xmlFormatReaderMethod;
		}
	}
}
