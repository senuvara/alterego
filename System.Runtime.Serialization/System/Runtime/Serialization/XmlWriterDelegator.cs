﻿using System;
using System.Globalization;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x02000149 RID: 329
	internal class XmlWriterDelegator
	{
		// Token: 0x060010B7 RID: 4279 RVA: 0x0003C369 File Offset: 0x0003A569
		public XmlWriterDelegator(XmlWriter writer)
		{
			XmlObjectSerializer.CheckNull(writer, "writer");
			this.writer = writer;
			this.dictionaryWriter = (writer as XmlDictionaryWriter);
		}

		// Token: 0x170003B4 RID: 948
		// (get) Token: 0x060010B8 RID: 4280 RVA: 0x0003C38F File Offset: 0x0003A58F
		internal XmlWriter Writer
		{
			get
			{
				return this.writer;
			}
		}

		// Token: 0x060010B9 RID: 4281 RVA: 0x0003C397 File Offset: 0x0003A597
		internal void Flush()
		{
			this.writer.Flush();
		}

		// Token: 0x060010BA RID: 4282 RVA: 0x0003C3A4 File Offset: 0x0003A5A4
		internal string LookupPrefix(string ns)
		{
			return this.writer.LookupPrefix(ns);
		}

		// Token: 0x060010BB RID: 4283 RVA: 0x0003C3B2 File Offset: 0x0003A5B2
		private void WriteEndAttribute()
		{
			this.writer.WriteEndAttribute();
		}

		// Token: 0x060010BC RID: 4284 RVA: 0x0003C3BF File Offset: 0x0003A5BF
		public void WriteEndElement()
		{
			this.writer.WriteEndElement();
			this.depth--;
		}

		// Token: 0x060010BD RID: 4285 RVA: 0x0003C3DA File Offset: 0x0003A5DA
		internal void WriteRaw(char[] buffer, int index, int count)
		{
			this.writer.WriteRaw(buffer, index, count);
		}

		// Token: 0x060010BE RID: 4286 RVA: 0x0003C3EA File Offset: 0x0003A5EA
		internal void WriteRaw(string data)
		{
			this.writer.WriteRaw(data);
		}

		// Token: 0x060010BF RID: 4287 RVA: 0x0003C3F8 File Offset: 0x0003A5F8
		internal void WriteXmlnsAttribute(XmlDictionaryString ns)
		{
			if (this.dictionaryWriter != null)
			{
				if (ns != null)
				{
					this.dictionaryWriter.WriteXmlnsAttribute(null, ns);
					return;
				}
			}
			else
			{
				this.WriteXmlnsAttribute(ns.Value);
			}
		}

		// Token: 0x060010C0 RID: 4288 RVA: 0x0003C420 File Offset: 0x0003A620
		internal void WriteXmlnsAttribute(string ns)
		{
			if (ns != null)
			{
				if (ns.Length == 0)
				{
					this.writer.WriteAttributeString("xmlns", string.Empty, null, ns);
					return;
				}
				if (this.dictionaryWriter != null)
				{
					this.dictionaryWriter.WriteXmlnsAttribute(null, ns);
					return;
				}
				if (this.writer.LookupPrefix(ns) == null)
				{
					string localName = string.Format(CultureInfo.InvariantCulture, "d{0}p{1}", this.depth, this.prefixes);
					this.prefixes++;
					this.writer.WriteAttributeString("xmlns", localName, null, ns);
				}
			}
		}

		// Token: 0x060010C1 RID: 4289 RVA: 0x0003C4C0 File Offset: 0x0003A6C0
		internal void WriteXmlnsAttribute(string prefix, XmlDictionaryString ns)
		{
			if (this.dictionaryWriter != null)
			{
				this.dictionaryWriter.WriteXmlnsAttribute(prefix, ns);
				return;
			}
			this.writer.WriteAttributeString("xmlns", prefix, null, ns.Value);
		}

		// Token: 0x060010C2 RID: 4290 RVA: 0x0003C4F0 File Offset: 0x0003A6F0
		private void WriteStartAttribute(string prefix, string localName, string ns)
		{
			this.writer.WriteStartAttribute(prefix, localName, ns);
		}

		// Token: 0x060010C3 RID: 4291 RVA: 0x0003C500 File Offset: 0x0003A700
		private void WriteStartAttribute(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			if (this.dictionaryWriter != null)
			{
				this.dictionaryWriter.WriteStartAttribute(prefix, localName, namespaceUri);
				return;
			}
			this.writer.WriteStartAttribute(prefix, (localName == null) ? null : localName.Value, (namespaceUri == null) ? null : namespaceUri.Value);
		}

		// Token: 0x060010C4 RID: 4292 RVA: 0x0003C53D File Offset: 0x0003A73D
		internal void WriteAttributeString(string prefix, string localName, string ns, string value)
		{
			this.WriteStartAttribute(prefix, localName, ns);
			this.WriteAttributeStringValue(value);
			this.WriteEndAttribute();
		}

		// Token: 0x060010C5 RID: 4293 RVA: 0x0003C556 File Offset: 0x0003A756
		internal void WriteAttributeString(string prefix, XmlDictionaryString attrName, XmlDictionaryString attrNs, string value)
		{
			this.WriteStartAttribute(prefix, attrName, attrNs);
			this.WriteAttributeStringValue(value);
			this.WriteEndAttribute();
		}

		// Token: 0x060010C6 RID: 4294 RVA: 0x0003C56F File Offset: 0x0003A76F
		private void WriteAttributeStringValue(string value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010C7 RID: 4295 RVA: 0x0003C57D File Offset: 0x0003A77D
		internal void WriteAttributeString(string prefix, XmlDictionaryString attrName, XmlDictionaryString attrNs, XmlDictionaryString value)
		{
			this.WriteStartAttribute(prefix, attrName, attrNs);
			this.WriteAttributeStringValue(value);
			this.WriteEndAttribute();
		}

		// Token: 0x060010C8 RID: 4296 RVA: 0x0003C596 File Offset: 0x0003A796
		private void WriteAttributeStringValue(XmlDictionaryString value)
		{
			if (this.dictionaryWriter == null)
			{
				this.writer.WriteString(value.Value);
				return;
			}
			this.dictionaryWriter.WriteString(value);
		}

		// Token: 0x060010C9 RID: 4297 RVA: 0x0003C5BE File Offset: 0x0003A7BE
		internal void WriteAttributeInt(string prefix, XmlDictionaryString attrName, XmlDictionaryString attrNs, int value)
		{
			this.WriteStartAttribute(prefix, attrName, attrNs);
			this.WriteAttributeIntValue(value);
			this.WriteEndAttribute();
		}

		// Token: 0x060010CA RID: 4298 RVA: 0x0003C5D7 File Offset: 0x0003A7D7
		private void WriteAttributeIntValue(int value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010CB RID: 4299 RVA: 0x0003C5E5 File Offset: 0x0003A7E5
		internal void WriteAttributeBool(string prefix, XmlDictionaryString attrName, XmlDictionaryString attrNs, bool value)
		{
			this.WriteStartAttribute(prefix, attrName, attrNs);
			this.WriteAttributeBoolValue(value);
			this.WriteEndAttribute();
		}

		// Token: 0x060010CC RID: 4300 RVA: 0x0003C5FE File Offset: 0x0003A7FE
		private void WriteAttributeBoolValue(bool value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010CD RID: 4301 RVA: 0x0003C60C File Offset: 0x0003A80C
		internal void WriteAttributeQualifiedName(string attrPrefix, XmlDictionaryString attrName, XmlDictionaryString attrNs, string name, string ns)
		{
			this.WriteXmlnsAttribute(ns);
			this.WriteStartAttribute(attrPrefix, attrName, attrNs);
			this.WriteAttributeQualifiedNameValue(name, ns);
			this.WriteEndAttribute();
		}

		// Token: 0x060010CE RID: 4302 RVA: 0x0003C62F File Offset: 0x0003A82F
		private void WriteAttributeQualifiedNameValue(string name, string ns)
		{
			this.writer.WriteQualifiedName(name, ns);
		}

		// Token: 0x060010CF RID: 4303 RVA: 0x0003C63E File Offset: 0x0003A83E
		internal void WriteAttributeQualifiedName(string attrPrefix, XmlDictionaryString attrName, XmlDictionaryString attrNs, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteXmlnsAttribute(ns);
			this.WriteStartAttribute(attrPrefix, attrName, attrNs);
			this.WriteAttributeQualifiedNameValue(name, ns);
			this.WriteEndAttribute();
		}

		// Token: 0x060010D0 RID: 4304 RVA: 0x0003C661 File Offset: 0x0003A861
		private void WriteAttributeQualifiedNameValue(XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (this.dictionaryWriter == null)
			{
				this.writer.WriteQualifiedName(name.Value, ns.Value);
				return;
			}
			this.dictionaryWriter.WriteQualifiedName(name, ns);
		}

		// Token: 0x060010D1 RID: 4305 RVA: 0x0003C690 File Offset: 0x0003A890
		internal void WriteStartElement(string localName, string ns)
		{
			this.WriteStartElement(null, localName, ns);
		}

		// Token: 0x060010D2 RID: 4306 RVA: 0x0003C69B File Offset: 0x0003A89B
		internal virtual void WriteStartElement(string prefix, string localName, string ns)
		{
			this.writer.WriteStartElement(prefix, localName, ns);
			this.depth++;
			this.prefixes = 1;
		}

		// Token: 0x060010D3 RID: 4307 RVA: 0x0003C6C0 File Offset: 0x0003A8C0
		public void WriteStartElement(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			this.WriteStartElement(null, localName, namespaceUri);
		}

		// Token: 0x060010D4 RID: 4308 RVA: 0x0003C6CC File Offset: 0x0003A8CC
		internal void WriteStartElement(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			if (this.dictionaryWriter != null)
			{
				this.dictionaryWriter.WriteStartElement(prefix, localName, namespaceUri);
			}
			else
			{
				this.writer.WriteStartElement(prefix, (localName == null) ? null : localName.Value, (namespaceUri == null) ? null : namespaceUri.Value);
			}
			this.depth++;
			this.prefixes = 1;
		}

		// Token: 0x060010D5 RID: 4309 RVA: 0x0003C72A File Offset: 0x0003A92A
		internal void WriteStartElementPrimitive(XmlDictionaryString localName, XmlDictionaryString namespaceUri)
		{
			if (this.dictionaryWriter != null)
			{
				this.dictionaryWriter.WriteStartElement(null, localName, namespaceUri);
				return;
			}
			this.writer.WriteStartElement(null, (localName == null) ? null : localName.Value, (namespaceUri == null) ? null : namespaceUri.Value);
		}

		// Token: 0x060010D6 RID: 4310 RVA: 0x0003C767 File Offset: 0x0003A967
		internal void WriteEndElementPrimitive()
		{
			this.writer.WriteEndElement();
		}

		// Token: 0x170003B5 RID: 949
		// (get) Token: 0x060010D7 RID: 4311 RVA: 0x0003C774 File Offset: 0x0003A974
		internal WriteState WriteState
		{
			get
			{
				return this.writer.WriteState;
			}
		}

		// Token: 0x170003B6 RID: 950
		// (get) Token: 0x060010D8 RID: 4312 RVA: 0x0003C781 File Offset: 0x0003A981
		internal string XmlLang
		{
			get
			{
				return this.writer.XmlLang;
			}
		}

		// Token: 0x170003B7 RID: 951
		// (get) Token: 0x060010D9 RID: 4313 RVA: 0x0003C78E File Offset: 0x0003A98E
		internal XmlSpace XmlSpace
		{
			get
			{
				return this.writer.XmlSpace;
			}
		}

		// Token: 0x060010DA RID: 4314 RVA: 0x0003C79B File Offset: 0x0003A99B
		public void WriteNamespaceDecl(XmlDictionaryString ns)
		{
			this.WriteXmlnsAttribute(ns);
		}

		// Token: 0x060010DB RID: 4315 RVA: 0x0003C7A4 File Offset: 0x0003A9A4
		private Exception CreateInvalidPrimitiveTypeException(Type type)
		{
			return new InvalidDataContractException(SR.GetString("Type '{0}' is not a valid serializable type.", new object[]
			{
				DataContract.GetClrTypeFullName(type)
			}));
		}

		// Token: 0x060010DC RID: 4316 RVA: 0x0003C7C4 File Offset: 0x0003A9C4
		internal void WriteAnyType(object value)
		{
			this.WriteAnyType(value, value.GetType());
		}

		// Token: 0x060010DD RID: 4317 RVA: 0x0003C7D4 File Offset: 0x0003A9D4
		internal void WriteAnyType(object value, Type valueType)
		{
			bool flag = true;
			switch (Type.GetTypeCode(valueType))
			{
			case TypeCode.Boolean:
				this.WriteBoolean((bool)value);
				goto IL_1F5;
			case TypeCode.Char:
				this.WriteChar((char)value);
				goto IL_1F5;
			case TypeCode.SByte:
				this.WriteSignedByte((sbyte)value);
				goto IL_1F5;
			case TypeCode.Byte:
				this.WriteUnsignedByte((byte)value);
				goto IL_1F5;
			case TypeCode.Int16:
				this.WriteShort((short)value);
				goto IL_1F5;
			case TypeCode.UInt16:
				this.WriteUnsignedShort((ushort)value);
				goto IL_1F5;
			case TypeCode.Int32:
				this.WriteInt((int)value);
				goto IL_1F5;
			case TypeCode.UInt32:
				this.WriteUnsignedInt((uint)value);
				goto IL_1F5;
			case TypeCode.Int64:
				this.WriteLong((long)value);
				goto IL_1F5;
			case TypeCode.UInt64:
				this.WriteUnsignedLong((ulong)value);
				goto IL_1F5;
			case TypeCode.Single:
				this.WriteFloat((float)value);
				goto IL_1F5;
			case TypeCode.Double:
				this.WriteDouble((double)value);
				goto IL_1F5;
			case TypeCode.Decimal:
				this.WriteDecimal((decimal)value);
				goto IL_1F5;
			case TypeCode.DateTime:
				this.WriteDateTime((DateTime)value);
				goto IL_1F5;
			case TypeCode.String:
				this.WriteString((string)value);
				goto IL_1F5;
			}
			if (valueType == Globals.TypeOfByteArray)
			{
				this.WriteBase64((byte[])value);
			}
			else if (!(valueType == Globals.TypeOfObject))
			{
				if (valueType == Globals.TypeOfTimeSpan)
				{
					this.WriteTimeSpan((TimeSpan)value);
				}
				else if (valueType == Globals.TypeOfGuid)
				{
					this.WriteGuid((Guid)value);
				}
				else if (valueType == Globals.TypeOfUri)
				{
					this.WriteUri((Uri)value);
				}
				else if (valueType == Globals.TypeOfXmlQualifiedName)
				{
					this.WriteQName((XmlQualifiedName)value);
				}
				else
				{
					flag = false;
				}
			}
			IL_1F5:
			if (!flag)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(this.CreateInvalidPrimitiveTypeException(valueType));
			}
		}

		// Token: 0x060010DE RID: 4318 RVA: 0x0003C9E8 File Offset: 0x0003ABE8
		internal void WriteExtensionData(IDataNode dataNode)
		{
			bool flag = true;
			Type dataType = dataNode.DataType;
			switch (Type.GetTypeCode(dataType))
			{
			case TypeCode.Boolean:
				this.WriteBoolean(((DataNode<bool>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.Char:
				this.WriteChar(((DataNode<char>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.SByte:
				this.WriteSignedByte(((DataNode<sbyte>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.Byte:
				this.WriteUnsignedByte(((DataNode<byte>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.Int16:
				this.WriteShort(((DataNode<short>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.UInt16:
				this.WriteUnsignedShort(((DataNode<ushort>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.Int32:
				this.WriteInt(((DataNode<int>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.UInt32:
				this.WriteUnsignedInt(((DataNode<uint>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.Int64:
				this.WriteLong(((DataNode<long>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.UInt64:
				this.WriteUnsignedLong(((DataNode<ulong>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.Single:
				this.WriteFloat(((DataNode<float>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.Double:
				this.WriteDouble(((DataNode<double>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.Decimal:
				this.WriteDecimal(((DataNode<decimal>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.DateTime:
				this.WriteDateTime(((DataNode<DateTime>)dataNode).GetValue());
				goto IL_27C;
			case TypeCode.String:
				this.WriteString(((DataNode<string>)dataNode).GetValue());
				goto IL_27C;
			}
			if (dataType == Globals.TypeOfByteArray)
			{
				this.WriteBase64(((DataNode<byte[]>)dataNode).GetValue());
			}
			else if (dataType == Globals.TypeOfObject)
			{
				object value = dataNode.Value;
				if (value != null)
				{
					this.WriteAnyType(value);
				}
			}
			else if (dataType == Globals.TypeOfTimeSpan)
			{
				this.WriteTimeSpan(((DataNode<TimeSpan>)dataNode).GetValue());
			}
			else if (dataType == Globals.TypeOfGuid)
			{
				this.WriteGuid(((DataNode<Guid>)dataNode).GetValue());
			}
			else if (dataType == Globals.TypeOfUri)
			{
				this.WriteUri(((DataNode<Uri>)dataNode).GetValue());
			}
			else if (dataType == Globals.TypeOfXmlQualifiedName)
			{
				this.WriteQName(((DataNode<XmlQualifiedName>)dataNode).GetValue());
			}
			else
			{
				flag = false;
			}
			IL_27C:
			if (!flag)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(this.CreateInvalidPrimitiveTypeException(dataType));
			}
		}

		// Token: 0x060010DF RID: 4319 RVA: 0x0003C56F File Offset: 0x0003A76F
		internal void WriteString(string value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010E0 RID: 4320 RVA: 0x0003C5FE File Offset: 0x0003A7FE
		internal virtual void WriteBoolean(bool value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010E1 RID: 4321 RVA: 0x0003CC81 File Offset: 0x0003AE81
		public void WriteBoolean(bool value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteBoolean(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010E2 RID: 4322 RVA: 0x0003CC98 File Offset: 0x0003AE98
		internal virtual void WriteDateTime(DateTime value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010E3 RID: 4323 RVA: 0x0003CCA6 File Offset: 0x0003AEA6
		public void WriteDateTime(DateTime value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteDateTime(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010E4 RID: 4324 RVA: 0x0003CCBD File Offset: 0x0003AEBD
		internal virtual void WriteDecimal(decimal value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010E5 RID: 4325 RVA: 0x0003CCCB File Offset: 0x0003AECB
		public void WriteDecimal(decimal value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteDecimal(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010E6 RID: 4326 RVA: 0x0003CCE2 File Offset: 0x0003AEE2
		internal virtual void WriteDouble(double value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010E7 RID: 4327 RVA: 0x0003CCF0 File Offset: 0x0003AEF0
		public void WriteDouble(double value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteDouble(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010E8 RID: 4328 RVA: 0x0003C5D7 File Offset: 0x0003A7D7
		internal virtual void WriteInt(int value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010E9 RID: 4329 RVA: 0x0003CD07 File Offset: 0x0003AF07
		public void WriteInt(int value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteInt(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010EA RID: 4330 RVA: 0x0003CD1E File Offset: 0x0003AF1E
		internal virtual void WriteLong(long value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010EB RID: 4331 RVA: 0x0003CD2C File Offset: 0x0003AF2C
		public void WriteLong(long value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteLong(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010EC RID: 4332 RVA: 0x0003CD43 File Offset: 0x0003AF43
		internal virtual void WriteFloat(float value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x060010ED RID: 4333 RVA: 0x0003CD51 File Offset: 0x0003AF51
		public void WriteFloat(float value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteFloat(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010EE RID: 4334 RVA: 0x0003CD68 File Offset: 0x0003AF68
		internal virtual void WriteBase64(byte[] bytes)
		{
			if (bytes == null)
			{
				return;
			}
			this.writer.WriteBase64(bytes, 0, bytes.Length);
		}

		// Token: 0x060010EF RID: 4335 RVA: 0x0003C5D7 File Offset: 0x0003A7D7
		internal virtual void WriteShort(short value)
		{
			this.writer.WriteValue((int)value);
		}

		// Token: 0x060010F0 RID: 4336 RVA: 0x0003CD7E File Offset: 0x0003AF7E
		public void WriteShort(short value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteShort(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010F1 RID: 4337 RVA: 0x0003C5D7 File Offset: 0x0003A7D7
		internal virtual void WriteUnsignedByte(byte value)
		{
			this.writer.WriteValue((int)value);
		}

		// Token: 0x060010F2 RID: 4338 RVA: 0x0003CD95 File Offset: 0x0003AF95
		public void WriteUnsignedByte(byte value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteUnsignedByte(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010F3 RID: 4339 RVA: 0x0003C5D7 File Offset: 0x0003A7D7
		internal virtual void WriteSignedByte(sbyte value)
		{
			this.writer.WriteValue((int)value);
		}

		// Token: 0x060010F4 RID: 4340 RVA: 0x0003CDAC File Offset: 0x0003AFAC
		public void WriteSignedByte(sbyte value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteSignedByte(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010F5 RID: 4341 RVA: 0x0003CDC3 File Offset: 0x0003AFC3
		internal virtual void WriteUnsignedInt(uint value)
		{
			this.writer.WriteValue((long)((ulong)value));
		}

		// Token: 0x060010F6 RID: 4342 RVA: 0x0003CDD2 File Offset: 0x0003AFD2
		public void WriteUnsignedInt(uint value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteUnsignedInt(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010F7 RID: 4343 RVA: 0x0003CDE9 File Offset: 0x0003AFE9
		internal virtual void WriteUnsignedLong(ulong value)
		{
			this.writer.WriteRaw(XmlConvert.ToString(value));
		}

		// Token: 0x060010F8 RID: 4344 RVA: 0x0003CDFC File Offset: 0x0003AFFC
		public void WriteUnsignedLong(ulong value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteUnsignedLong(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010F9 RID: 4345 RVA: 0x0003C5D7 File Offset: 0x0003A7D7
		internal virtual void WriteUnsignedShort(ushort value)
		{
			this.writer.WriteValue((int)value);
		}

		// Token: 0x060010FA RID: 4346 RVA: 0x0003CE13 File Offset: 0x0003B013
		public void WriteUnsignedShort(ushort value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteUnsignedShort(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010FB RID: 4347 RVA: 0x0003C5D7 File Offset: 0x0003A7D7
		internal virtual void WriteChar(char value)
		{
			this.writer.WriteValue((int)value);
		}

		// Token: 0x060010FC RID: 4348 RVA: 0x0003CE2A File Offset: 0x0003B02A
		public void WriteChar(char value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteChar(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010FD RID: 4349 RVA: 0x0003CE41 File Offset: 0x0003B041
		internal void WriteTimeSpan(TimeSpan value)
		{
			this.writer.WriteRaw(XmlConvert.ToString(value));
		}

		// Token: 0x060010FE RID: 4350 RVA: 0x0003CE54 File Offset: 0x0003B054
		public void WriteTimeSpan(TimeSpan value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteTimeSpan(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x060010FF RID: 4351 RVA: 0x0003CE6B File Offset: 0x0003B06B
		internal void WriteGuid(Guid value)
		{
			this.writer.WriteRaw(value.ToString());
		}

		// Token: 0x06001100 RID: 4352 RVA: 0x0003CE85 File Offset: 0x0003B085
		public void WriteGuid(Guid value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			this.WriteStartElementPrimitive(name, ns);
			this.WriteGuid(value);
			this.WriteEndElementPrimitive();
		}

		// Token: 0x06001101 RID: 4353 RVA: 0x0003CE9C File Offset: 0x0003B09C
		internal void WriteUri(Uri value)
		{
			this.writer.WriteString(value.GetComponents(UriComponents.SerializationInfoString, UriFormat.UriEscaped));
		}

		// Token: 0x06001102 RID: 4354 RVA: 0x0003CEB5 File Offset: 0x0003B0B5
		internal virtual void WriteQName(XmlQualifiedName value)
		{
			if (value != XmlQualifiedName.Empty)
			{
				this.WriteXmlnsAttribute(value.Namespace);
				this.WriteQualifiedName(value.Name, value.Namespace);
			}
		}

		// Token: 0x06001103 RID: 4355 RVA: 0x0003C62F File Offset: 0x0003A82F
		internal void WriteQualifiedName(string localName, string ns)
		{
			this.writer.WriteQualifiedName(localName, ns);
		}

		// Token: 0x06001104 RID: 4356 RVA: 0x0003C661 File Offset: 0x0003A861
		internal void WriteQualifiedName(XmlDictionaryString localName, XmlDictionaryString ns)
		{
			if (this.dictionaryWriter == null)
			{
				this.writer.WriteQualifiedName(localName.Value, ns.Value);
				return;
			}
			this.dictionaryWriter.WriteQualifiedName(localName, ns);
		}

		// Token: 0x06001105 RID: 4357 RVA: 0x0003CEE4 File Offset: 0x0003B0E4
		public void WriteBooleanArray(bool[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			if (this.dictionaryWriter == null)
			{
				for (int i = 0; i < value.Length; i++)
				{
					this.WriteBoolean(value[i], itemName, itemNamespace);
				}
				return;
			}
			this.dictionaryWriter.WriteArray(null, itemName, itemNamespace, value, 0, value.Length);
		}

		// Token: 0x06001106 RID: 4358 RVA: 0x0003CF28 File Offset: 0x0003B128
		public void WriteDateTimeArray(DateTime[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			if (this.dictionaryWriter == null)
			{
				for (int i = 0; i < value.Length; i++)
				{
					this.WriteDateTime(value[i], itemName, itemNamespace);
				}
				return;
			}
			this.dictionaryWriter.WriteArray(null, itemName, itemNamespace, value, 0, value.Length);
		}

		// Token: 0x06001107 RID: 4359 RVA: 0x0003CF70 File Offset: 0x0003B170
		public void WriteDecimalArray(decimal[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			if (this.dictionaryWriter == null)
			{
				for (int i = 0; i < value.Length; i++)
				{
					this.WriteDecimal(value[i], itemName, itemNamespace);
				}
				return;
			}
			this.dictionaryWriter.WriteArray(null, itemName, itemNamespace, value, 0, value.Length);
		}

		// Token: 0x06001108 RID: 4360 RVA: 0x0003CFB8 File Offset: 0x0003B1B8
		public void WriteInt32Array(int[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			if (this.dictionaryWriter == null)
			{
				for (int i = 0; i < value.Length; i++)
				{
					this.WriteInt(value[i], itemName, itemNamespace);
				}
				return;
			}
			this.dictionaryWriter.WriteArray(null, itemName, itemNamespace, value, 0, value.Length);
		}

		// Token: 0x06001109 RID: 4361 RVA: 0x0003CFFC File Offset: 0x0003B1FC
		public void WriteInt64Array(long[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			if (this.dictionaryWriter == null)
			{
				for (int i = 0; i < value.Length; i++)
				{
					this.WriteLong(value[i], itemName, itemNamespace);
				}
				return;
			}
			this.dictionaryWriter.WriteArray(null, itemName, itemNamespace, value, 0, value.Length);
		}

		// Token: 0x0600110A RID: 4362 RVA: 0x0003D040 File Offset: 0x0003B240
		public void WriteSingleArray(float[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			if (this.dictionaryWriter == null)
			{
				for (int i = 0; i < value.Length; i++)
				{
					this.WriteFloat(value[i], itemName, itemNamespace);
				}
				return;
			}
			this.dictionaryWriter.WriteArray(null, itemName, itemNamespace, value, 0, value.Length);
		}

		// Token: 0x0600110B RID: 4363 RVA: 0x0003D084 File Offset: 0x0003B284
		public void WriteDoubleArray(double[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			if (this.dictionaryWriter == null)
			{
				for (int i = 0; i < value.Length; i++)
				{
					this.WriteDouble(value[i], itemName, itemNamespace);
				}
				return;
			}
			this.dictionaryWriter.WriteArray(null, itemName, itemNamespace, value, 0, value.Length);
		}

		// Token: 0x040006FB RID: 1787
		protected XmlWriter writer;

		// Token: 0x040006FC RID: 1788
		protected XmlDictionaryWriter dictionaryWriter;

		// Token: 0x040006FD RID: 1789
		internal int depth;

		// Token: 0x040006FE RID: 1790
		private int prefixes;

		// Token: 0x040006FF RID: 1791
		private const int CharChunkSize = 76;

		// Token: 0x04000700 RID: 1792
		private const int ByteChunkSize = 57;
	}
}
