﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x0200010A RID: 266
	internal class GMonthDataContract : StringDataContract
	{
		// Token: 0x06000DBA RID: 3514 RVA: 0x00033806 File Offset: 0x00031A06
		internal GMonthDataContract() : base(DictionaryGlobals.gMonthLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
