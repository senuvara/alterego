﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x02000119 RID: 281
	internal class TimeSpanDataContract : PrimitiveDataContract
	{
		// Token: 0x06000DD3 RID: 3539 RVA: 0x00033A00 File Offset: 0x00031C00
		internal TimeSpanDataContract() : this(DictionaryGlobals.TimeSpanLocalName, DictionaryGlobals.SerializationNamespace)
		{
		}

		// Token: 0x06000DD4 RID: 3540 RVA: 0x00033A12 File Offset: 0x00031C12
		internal TimeSpanDataContract(XmlDictionaryString name, XmlDictionaryString ns) : base(typeof(TimeSpan), name, ns)
		{
		}

		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x06000DD5 RID: 3541 RVA: 0x00033A26 File Offset: 0x00031C26
		internal override string WriteMethodName
		{
			get
			{
				return "WriteTimeSpan";
			}
		}

		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x06000DD6 RID: 3542 RVA: 0x00033A2D File Offset: 0x00031C2D
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsTimeSpan";
			}
		}

		// Token: 0x06000DD7 RID: 3543 RVA: 0x00033A34 File Offset: 0x00031C34
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteTimeSpan((TimeSpan)obj);
		}

		// Token: 0x06000DD8 RID: 3544 RVA: 0x00033A42 File Offset: 0x00031C42
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsTimeSpan(), context);
			}
			return reader.ReadElementContentAsTimeSpan();
		}
	}
}
