﻿using System;
using System.Xml.Serialization;

namespace System.Runtime.Serialization
{
	// Token: 0x0200012A RID: 298
	// (Invoke) Token: 0x06000E1D RID: 3613
	internal delegate IXmlSerializable CreateXmlSerializableDelegate();
}
