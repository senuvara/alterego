﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000120 RID: 288
	internal enum SerializationMode
	{
		// Token: 0x04000645 RID: 1605
		SharedContract,
		// Token: 0x04000646 RID: 1606
		SharedType
	}
}
