﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000101 RID: 257
	internal class DateTimeDataContract : PrimitiveDataContract
	{
		// Token: 0x06000DA8 RID: 3496 RVA: 0x000336C7 File Offset: 0x000318C7
		internal DateTimeDataContract() : base(typeof(DateTime), DictionaryGlobals.DateTimeLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x06000DA9 RID: 3497 RVA: 0x000336E3 File Offset: 0x000318E3
		internal override string WriteMethodName
		{
			get
			{
				return "WriteDateTime";
			}
		}

		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x06000DAA RID: 3498 RVA: 0x000336EA File Offset: 0x000318EA
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsDateTime";
			}
		}

		// Token: 0x06000DAB RID: 3499 RVA: 0x000336F1 File Offset: 0x000318F1
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteDateTime((DateTime)obj);
		}

		// Token: 0x06000DAC RID: 3500 RVA: 0x000336FF File Offset: 0x000318FF
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsDateTime(), context);
			}
			return reader.ReadElementContentAsDateTime();
		}
	}
}
