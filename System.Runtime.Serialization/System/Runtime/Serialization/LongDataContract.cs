﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000F7 RID: 247
	internal class LongDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D89 RID: 3465 RVA: 0x0003349C File Offset: 0x0003169C
		internal LongDataContract() : this(DictionaryGlobals.LongLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x06000D8A RID: 3466 RVA: 0x000334AE File Offset: 0x000316AE
		internal LongDataContract(XmlDictionaryString name, XmlDictionaryString ns) : base(typeof(long), name, ns)
		{
		}

		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06000D8B RID: 3467 RVA: 0x000334C2 File Offset: 0x000316C2
		internal override string WriteMethodName
		{
			get
			{
				return "WriteLong";
			}
		}

		// Token: 0x170002DF RID: 735
		// (get) Token: 0x06000D8C RID: 3468 RVA: 0x000334C9 File Offset: 0x000316C9
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsLong";
			}
		}

		// Token: 0x06000D8D RID: 3469 RVA: 0x000334D0 File Offset: 0x000316D0
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteLong((long)obj);
		}

		// Token: 0x06000D8E RID: 3470 RVA: 0x000334DE File Offset: 0x000316DE
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsLong(), context);
			}
			return reader.ReadElementContentAsLong();
		}
	}
}
