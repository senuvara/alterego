﻿using System;

namespace System.Runtime.Serialization
{
	/// <summary>Provides a data structure to store extra data encountered by the <see cref="T:System.Runtime.Serialization.XmlObjectSerializer" /> during deserialization of a type marked with the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute. </summary>
	// Token: 0x020000E3 RID: 227
	public interface IExtensibleDataObject
	{
		/// <summary>Gets or sets the structure that contains extra data.</summary>
		/// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject" /> that contains data that is not recognized as belonging to the data contract.</returns>
		// Token: 0x17000293 RID: 659
		// (get) Token: 0x06000CCE RID: 3278
		// (set) Token: 0x06000CCF RID: 3279
		ExtensionDataObject ExtensionData { get; set; }
	}
}
