﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000EF RID: 239
	internal class AsmxCharDataContract : CharDataContract
	{
		// Token: 0x06000D65 RID: 3429 RVA: 0x0003320D File Offset: 0x0003140D
		internal AsmxCharDataContract() : base(DictionaryGlobals.CharLocalName, DictionaryGlobals.AsmxTypesNamespace)
		{
		}
	}
}
