﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000152 RID: 338
	internal enum CodeTypeReferenceOptions
	{
		// Token: 0x04000913 RID: 2323
		GlobalReference = 1,
		// Token: 0x04000914 RID: 2324
		GenericTypeParameter
	}
}
