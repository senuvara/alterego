﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace System.Runtime.Serialization
{
	// Token: 0x0200012B RID: 299
	internal sealed class XmlDataContract : DataContract
	{
		// Token: 0x06000E20 RID: 3616 RVA: 0x000343BB File Offset: 0x000325BB
		[SecuritySafeCritical]
		internal XmlDataContract() : base(new XmlDataContract.XmlDataContractCriticalHelper())
		{
			this.helper = (base.Helper as XmlDataContract.XmlDataContractCriticalHelper);
		}

		// Token: 0x06000E21 RID: 3617 RVA: 0x000343D9 File Offset: 0x000325D9
		[SecuritySafeCritical]
		internal XmlDataContract(Type type) : base(new XmlDataContract.XmlDataContractCriticalHelper(type))
		{
			this.helper = (base.Helper as XmlDataContract.XmlDataContractCriticalHelper);
		}

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06000E22 RID: 3618 RVA: 0x000343F8 File Offset: 0x000325F8
		// (set) Token: 0x06000E23 RID: 3619 RVA: 0x00034405 File Offset: 0x00032605
		internal override Dictionary<XmlQualifiedName, DataContract> KnownDataContracts
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.KnownDataContracts;
			}
			[SecurityCritical]
			set
			{
				this.helper.KnownDataContracts = value;
			}
		}

		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06000E24 RID: 3620 RVA: 0x00034413 File Offset: 0x00032613
		// (set) Token: 0x06000E25 RID: 3621 RVA: 0x00034420 File Offset: 0x00032620
		internal XmlSchemaType XsdType
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.XsdType;
			}
			[SecurityCritical]
			set
			{
				this.helper.XsdType = value;
			}
		}

		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06000E26 RID: 3622 RVA: 0x0003442E File Offset: 0x0003262E
		internal bool IsAnonymous
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsAnonymous;
			}
		}

		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06000E27 RID: 3623 RVA: 0x0003443B File Offset: 0x0003263B
		// (set) Token: 0x06000E28 RID: 3624 RVA: 0x00034448 File Offset: 0x00032648
		internal override bool HasRoot
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.HasRoot;
			}
			[SecurityCritical]
			set
			{
				this.helper.HasRoot = value;
			}
		}

		// Token: 0x1700030B RID: 779
		// (get) Token: 0x06000E29 RID: 3625 RVA: 0x00034456 File Offset: 0x00032656
		// (set) Token: 0x06000E2A RID: 3626 RVA: 0x00034463 File Offset: 0x00032663
		internal override XmlDictionaryString TopLevelElementName
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.TopLevelElementName;
			}
			[SecurityCritical]
			set
			{
				this.helper.TopLevelElementName = value;
			}
		}

		// Token: 0x1700030C RID: 780
		// (get) Token: 0x06000E2B RID: 3627 RVA: 0x00034471 File Offset: 0x00032671
		// (set) Token: 0x06000E2C RID: 3628 RVA: 0x0003447E File Offset: 0x0003267E
		internal override XmlDictionaryString TopLevelElementNamespace
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.TopLevelElementNamespace;
			}
			[SecurityCritical]
			set
			{
				this.helper.TopLevelElementNamespace = value;
			}
		}

		// Token: 0x1700030D RID: 781
		// (get) Token: 0x06000E2D RID: 3629 RVA: 0x0003448C File Offset: 0x0003268C
		// (set) Token: 0x06000E2E RID: 3630 RVA: 0x00034499 File Offset: 0x00032699
		internal bool IsTopLevelElementNullable
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsTopLevelElementNullable;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsTopLevelElementNullable = value;
			}
		}

		// Token: 0x1700030E RID: 782
		// (get) Token: 0x06000E2F RID: 3631 RVA: 0x000344A7 File Offset: 0x000326A7
		// (set) Token: 0x06000E30 RID: 3632 RVA: 0x000344B4 File Offset: 0x000326B4
		internal bool IsTypeDefinedOnImport
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsTypeDefinedOnImport;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsTypeDefinedOnImport = value;
			}
		}

		// Token: 0x1700030F RID: 783
		// (get) Token: 0x06000E31 RID: 3633 RVA: 0x000344C4 File Offset: 0x000326C4
		internal CreateXmlSerializableDelegate CreateXmlSerializableDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.CreateXmlSerializableDelegate == null)
				{
					lock (this)
					{
						if (this.helper.CreateXmlSerializableDelegate == null)
						{
							CreateXmlSerializableDelegate createXmlSerializableDelegate = this.GenerateCreateXmlSerializableDelegate();
							Thread.MemoryBarrier();
							this.helper.CreateXmlSerializableDelegate = createXmlSerializableDelegate;
						}
					}
				}
				return this.helper.CreateXmlSerializableDelegate;
			}
		}

		// Token: 0x17000310 RID: 784
		// (get) Token: 0x06000E32 RID: 3634 RVA: 0x0000310F File Offset: 0x0000130F
		internal override bool CanContainReferences
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000311 RID: 785
		// (get) Token: 0x06000E33 RID: 3635 RVA: 0x00034538 File Offset: 0x00032738
		internal override bool IsBuiltInDataContract
		{
			get
			{
				return base.UnderlyingType == Globals.TypeOfXmlElement || base.UnderlyingType == Globals.TypeOfXmlNodeArray;
			}
		}

		// Token: 0x06000E34 RID: 3636 RVA: 0x00034560 File Offset: 0x00032760
		private ConstructorInfo GetConstructor()
		{
			Type underlyingType = base.UnderlyingType;
			if (underlyingType.IsValueType)
			{
				return null;
			}
			ConstructorInfo constructor = underlyingType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Globals.EmptyTypeArray, null);
			if (constructor == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("IXmlSerializable Type '{0}' must have default constructor.", new object[]
				{
					DataContract.GetClrTypeFullName(underlyingType)
				})));
			}
			return constructor;
		}

		// Token: 0x06000E35 RID: 3637 RVA: 0x000345BC File Offset: 0x000327BC
		[SecurityCritical]
		internal void SetTopLevelElementName(XmlQualifiedName elementName)
		{
			if (elementName != null)
			{
				XmlDictionary xmlDictionary = new XmlDictionary();
				this.TopLevelElementName = xmlDictionary.Add(elementName.Name);
				this.TopLevelElementNamespace = xmlDictionary.Add(elementName.Namespace);
			}
		}

		// Token: 0x06000E36 RID: 3638 RVA: 0x000345FC File Offset: 0x000327FC
		internal override bool Equals(object other, Dictionary<DataContractPairKey, object> checkedContracts)
		{
			if (base.IsEqualOrChecked(other, checkedContracts))
			{
				return true;
			}
			XmlDataContract xmlDataContract = other as XmlDataContract;
			if (xmlDataContract == null)
			{
				return false;
			}
			if (this.HasRoot != xmlDataContract.HasRoot)
			{
				return false;
			}
			if (this.IsAnonymous)
			{
				return xmlDataContract.IsAnonymous;
			}
			return base.StableName.Name == xmlDataContract.StableName.Name && base.StableName.Namespace == xmlDataContract.StableName.Namespace;
		}

		// Token: 0x06000E37 RID: 3639 RVA: 0x0002625C File Offset: 0x0002445C
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000E38 RID: 3640 RVA: 0x0003467A File Offset: 0x0003287A
		public override void WriteXmlValue(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContext context)
		{
			if (context == null)
			{
				XmlObjectSerializerWriteContext.WriteRootIXmlSerializable(xmlWriter, obj);
				return;
			}
			context.WriteIXmlSerializable(xmlWriter, obj);
		}

		// Token: 0x06000E39 RID: 3641 RVA: 0x00034690 File Offset: 0x00032890
		public override object ReadXmlValue(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContext context)
		{
			object obj;
			if (context == null)
			{
				obj = XmlObjectSerializerReadContext.ReadRootIXmlSerializable(xmlReader, this, true);
			}
			else
			{
				obj = context.ReadIXmlSerializable(xmlReader, this, true);
				context.AddNewObject(obj);
			}
			xmlReader.ReadEndElement();
			return obj;
		}

		// Token: 0x06000E3A RID: 3642 RVA: 0x000346C3 File Offset: 0x000328C3
		internal CreateXmlSerializableDelegate GenerateCreateXmlSerializableDelegate()
		{
			return () => new XmlDataContractInterpreter(this).CreateXmlSerializable();
		}

		// Token: 0x06000E3B RID: 3643 RVA: 0x000346D1 File Offset: 0x000328D1
		[CompilerGenerated]
		private IXmlSerializable <GenerateCreateXmlSerializableDelegate>b__39_0()
		{
			return new XmlDataContractInterpreter(this).CreateXmlSerializable();
		}

		// Token: 0x0400065A RID: 1626
		[SecurityCritical]
		private XmlDataContract.XmlDataContractCriticalHelper helper;

		// Token: 0x0200012C RID: 300
		private class XmlDataContractCriticalHelper : DataContract.DataContractCriticalHelper
		{
			// Token: 0x06000E3C RID: 3644 RVA: 0x00026264 File Offset: 0x00024464
			internal XmlDataContractCriticalHelper()
			{
			}

			// Token: 0x06000E3D RID: 3645 RVA: 0x000346E0 File Offset: 0x000328E0
			internal XmlDataContractCriticalHelper(Type type) : base(type)
			{
				if (type.IsDefined(Globals.TypeOfDataContractAttribute, false))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot be IXmlSerializable and have DataContractAttribute attribute.", new object[]
					{
						DataContract.GetClrTypeFullName(type)
					})));
				}
				if (type.IsDefined(Globals.TypeOfCollectionDataContractAttribute, false))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot be IXmlSerializable and have CollectionDataContractAttribute attribute.", new object[]
					{
						DataContract.GetClrTypeFullName(type)
					})));
				}
				XmlQualifiedName stableName;
				XmlSchemaType xmlSchemaType;
				bool flag;
				SchemaExporter.GetXmlTypeInfo(type, out stableName, out xmlSchemaType, out flag);
				base.StableName = stableName;
				this.XsdType = xmlSchemaType;
				this.HasRoot = flag;
				XmlDictionary xmlDictionary = new XmlDictionary();
				base.Name = xmlDictionary.Add(base.StableName.Name);
				base.Namespace = xmlDictionary.Add(base.StableName.Namespace);
				object[] array = (base.UnderlyingType == null) ? null : base.UnderlyingType.GetCustomAttributes(Globals.TypeOfXmlRootAttribute, false);
				if (array == null || array.Length == 0)
				{
					if (flag)
					{
						this.topLevelElementName = base.Name;
						this.topLevelElementNamespace = ((base.StableName.Namespace == "http://www.w3.org/2001/XMLSchema") ? DictionaryGlobals.EmptyString : base.Namespace);
						this.isTopLevelElementNullable = true;
						return;
					}
					return;
				}
				else
				{
					if (flag)
					{
						XmlRootAttribute xmlRootAttribute = (XmlRootAttribute)array[0];
						this.isTopLevelElementNullable = xmlRootAttribute.IsNullable;
						string elementName = xmlRootAttribute.ElementName;
						this.topLevelElementName = ((elementName == null || elementName.Length == 0) ? base.Name : xmlDictionary.Add(DataContract.EncodeLocalName(elementName)));
						string @namespace = xmlRootAttribute.Namespace;
						this.topLevelElementNamespace = ((@namespace == null || @namespace.Length == 0) ? DictionaryGlobals.EmptyString : xmlDictionary.Add(@namespace));
						return;
					}
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot specify an XmlRootAttribute attribute because its IsAny setting is 'true'. This type must write all its contents including the root element. Verify that the IXmlSerializable implementation is correct.", new object[]
					{
						DataContract.GetClrTypeFullName(base.UnderlyingType)
					})));
				}
			}

			// Token: 0x17000312 RID: 786
			// (get) Token: 0x06000E3E RID: 3646 RVA: 0x000348C0 File Offset: 0x00032AC0
			// (set) Token: 0x06000E3F RID: 3647 RVA: 0x00034938 File Offset: 0x00032B38
			internal override Dictionary<XmlQualifiedName, DataContract> KnownDataContracts
			{
				get
				{
					if (!this.isKnownTypeAttributeChecked && base.UnderlyingType != null)
					{
						lock (this)
						{
							if (!this.isKnownTypeAttributeChecked)
							{
								this.knownDataContracts = DataContract.ImportKnownTypeAttributes(base.UnderlyingType);
								Thread.MemoryBarrier();
								this.isKnownTypeAttributeChecked = true;
							}
						}
					}
					return this.knownDataContracts;
				}
				set
				{
					this.knownDataContracts = value;
				}
			}

			// Token: 0x17000313 RID: 787
			// (get) Token: 0x06000E40 RID: 3648 RVA: 0x00034941 File Offset: 0x00032B41
			// (set) Token: 0x06000E41 RID: 3649 RVA: 0x00034949 File Offset: 0x00032B49
			internal XmlSchemaType XsdType
			{
				get
				{
					return this.xsdType;
				}
				set
				{
					this.xsdType = value;
				}
			}

			// Token: 0x17000314 RID: 788
			// (get) Token: 0x06000E42 RID: 3650 RVA: 0x00034952 File Offset: 0x00032B52
			internal bool IsAnonymous
			{
				get
				{
					return this.xsdType != null;
				}
			}

			// Token: 0x17000315 RID: 789
			// (get) Token: 0x06000E43 RID: 3651 RVA: 0x0003495D File Offset: 0x00032B5D
			// (set) Token: 0x06000E44 RID: 3652 RVA: 0x00034965 File Offset: 0x00032B65
			internal override bool HasRoot
			{
				get
				{
					return this.hasRoot;
				}
				set
				{
					this.hasRoot = value;
				}
			}

			// Token: 0x17000316 RID: 790
			// (get) Token: 0x06000E45 RID: 3653 RVA: 0x0003496E File Offset: 0x00032B6E
			// (set) Token: 0x06000E46 RID: 3654 RVA: 0x00034976 File Offset: 0x00032B76
			internal override XmlDictionaryString TopLevelElementName
			{
				get
				{
					return this.topLevelElementName;
				}
				set
				{
					this.topLevelElementName = value;
				}
			}

			// Token: 0x17000317 RID: 791
			// (get) Token: 0x06000E47 RID: 3655 RVA: 0x0003497F File Offset: 0x00032B7F
			// (set) Token: 0x06000E48 RID: 3656 RVA: 0x00034987 File Offset: 0x00032B87
			internal override XmlDictionaryString TopLevelElementNamespace
			{
				get
				{
					return this.topLevelElementNamespace;
				}
				set
				{
					this.topLevelElementNamespace = value;
				}
			}

			// Token: 0x17000318 RID: 792
			// (get) Token: 0x06000E49 RID: 3657 RVA: 0x00034990 File Offset: 0x00032B90
			// (set) Token: 0x06000E4A RID: 3658 RVA: 0x00034998 File Offset: 0x00032B98
			internal bool IsTopLevelElementNullable
			{
				get
				{
					return this.isTopLevelElementNullable;
				}
				set
				{
					this.isTopLevelElementNullable = value;
				}
			}

			// Token: 0x17000319 RID: 793
			// (get) Token: 0x06000E4B RID: 3659 RVA: 0x000349A1 File Offset: 0x00032BA1
			// (set) Token: 0x06000E4C RID: 3660 RVA: 0x000349A9 File Offset: 0x00032BA9
			internal bool IsTypeDefinedOnImport
			{
				get
				{
					return this.isTypeDefinedOnImport;
				}
				set
				{
					this.isTypeDefinedOnImport = value;
				}
			}

			// Token: 0x1700031A RID: 794
			// (get) Token: 0x06000E4D RID: 3661 RVA: 0x000349B2 File Offset: 0x00032BB2
			// (set) Token: 0x06000E4E RID: 3662 RVA: 0x000349BA File Offset: 0x00032BBA
			internal CreateXmlSerializableDelegate CreateXmlSerializableDelegate
			{
				get
				{
					return this.createXmlSerializable;
				}
				set
				{
					this.createXmlSerializable = value;
				}
			}

			// Token: 0x0400065B RID: 1627
			private Dictionary<XmlQualifiedName, DataContract> knownDataContracts;

			// Token: 0x0400065C RID: 1628
			private bool isKnownTypeAttributeChecked;

			// Token: 0x0400065D RID: 1629
			private XmlDictionaryString topLevelElementName;

			// Token: 0x0400065E RID: 1630
			private XmlDictionaryString topLevelElementNamespace;

			// Token: 0x0400065F RID: 1631
			private bool isTopLevelElementNullable;

			// Token: 0x04000660 RID: 1632
			private bool isTypeDefinedOnImport;

			// Token: 0x04000661 RID: 1633
			private XmlSchemaType xsdType;

			// Token: 0x04000662 RID: 1634
			private bool hasRoot;

			// Token: 0x04000663 RID: 1635
			private CreateXmlSerializableDelegate createXmlSerializable;
		}
	}
}
