﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000D1 RID: 209
	internal class ExtensionDataMember
	{
		// Token: 0x17000204 RID: 516
		// (get) Token: 0x06000BC9 RID: 3017 RVA: 0x0002EFDB File Offset: 0x0002D1DB
		// (set) Token: 0x06000BCA RID: 3018 RVA: 0x0002EFE3 File Offset: 0x0002D1E3
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000BCB RID: 3019 RVA: 0x0002EFEC File Offset: 0x0002D1EC
		// (set) Token: 0x06000BCC RID: 3020 RVA: 0x0002EFF4 File Offset: 0x0002D1F4
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x06000BCD RID: 3021 RVA: 0x0002EFFD File Offset: 0x0002D1FD
		// (set) Token: 0x06000BCE RID: 3022 RVA: 0x0002F005 File Offset: 0x0002D205
		public IDataNode Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x06000BCF RID: 3023 RVA: 0x0002F00E File Offset: 0x0002D20E
		// (set) Token: 0x06000BD0 RID: 3024 RVA: 0x0002F016 File Offset: 0x0002D216
		public int MemberIndex
		{
			get
			{
				return this.memberIndex;
			}
			set
			{
				this.memberIndex = value;
			}
		}

		// Token: 0x06000BD1 RID: 3025 RVA: 0x00002217 File Offset: 0x00000417
		public ExtensionDataMember()
		{
		}

		// Token: 0x04000506 RID: 1286
		private string name;

		// Token: 0x04000507 RID: 1287
		private string ns;

		// Token: 0x04000508 RID: 1288
		private IDataNode value;

		// Token: 0x04000509 RID: 1289
		private int memberIndex;
	}
}
