﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000110 RID: 272
	internal class IDDataContract : StringDataContract
	{
		// Token: 0x06000DC0 RID: 3520 RVA: 0x00033872 File Offset: 0x00031A72
		internal IDDataContract() : base(DictionaryGlobals.XSDIDLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
