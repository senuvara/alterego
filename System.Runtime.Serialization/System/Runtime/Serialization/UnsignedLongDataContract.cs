﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000FD RID: 253
	internal class UnsignedLongDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D94 RID: 3476 RVA: 0x0003355B File Offset: 0x0003175B
		internal UnsignedLongDataContract() : base(typeof(ulong), DictionaryGlobals.UnsignedLongLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x06000D95 RID: 3477 RVA: 0x00033577 File Offset: 0x00031777
		internal override string WriteMethodName
		{
			get
			{
				return "WriteUnsignedLong";
			}
		}

		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x06000D96 RID: 3478 RVA: 0x0003357E File Offset: 0x0003177E
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsUnsignedLong";
			}
		}

		// Token: 0x06000D97 RID: 3479 RVA: 0x00033585 File Offset: 0x00031785
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteUnsignedLong((ulong)obj);
		}

		// Token: 0x06000D98 RID: 3480 RVA: 0x00033593 File Offset: 0x00031793
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsUnsignedLong(), context);
			}
			return reader.ReadElementContentAsUnsignedLong();
		}
	}
}
