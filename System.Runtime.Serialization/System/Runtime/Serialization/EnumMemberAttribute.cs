﻿using System;

namespace System.Runtime.Serialization
{
	/// <summary>Specifies that the field is an enumeration member and should be serialized.</summary>
	// Token: 0x020000CE RID: 206
	[AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
	public sealed class EnumMemberAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.EnumMemberAttribute" /> class. </summary>
		// Token: 0x06000BBD RID: 3005 RVA: 0x000254EB File Offset: 0x000236EB
		public EnumMemberAttribute()
		{
		}

		/// <summary>Gets or sets the value associated with the enumeration member the attribute is applied to. </summary>
		/// <returns>The value associated with the enumeration member.</returns>
		// Token: 0x170001FF RID: 511
		// (get) Token: 0x06000BBE RID: 3006 RVA: 0x0002EF7E File Offset: 0x0002D17E
		// (set) Token: 0x06000BBF RID: 3007 RVA: 0x0002EF86 File Offset: 0x0002D186
		public string Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
				this.isValueSetExplicitly = true;
			}
		}

		/// <summary>Gets whether the <see cref="P:System.Runtime.Serialization.EnumMemberAttribute.Value" /> has been explicitly set.</summary>
		/// <returns>
		///     <see langword="true" /> if the value has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000200 RID: 512
		// (get) Token: 0x06000BC0 RID: 3008 RVA: 0x0002EF96 File Offset: 0x0002D196
		public bool IsValueSetExplicitly
		{
			get
			{
				return this.isValueSetExplicitly;
			}
		}

		// Token: 0x04000501 RID: 1281
		private string value;

		// Token: 0x04000502 RID: 1282
		private bool isValueSetExplicitly;
	}
}
