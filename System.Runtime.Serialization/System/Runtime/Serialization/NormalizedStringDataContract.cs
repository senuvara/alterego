﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x0200010B RID: 267
	internal class NormalizedStringDataContract : StringDataContract
	{
		// Token: 0x06000DBB RID: 3515 RVA: 0x00033818 File Offset: 0x00031A18
		internal NormalizedStringDataContract() : base(DictionaryGlobals.normalizedStringLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
