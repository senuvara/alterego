﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000106 RID: 262
	internal class GYearMonthDataContract : StringDataContract
	{
		// Token: 0x06000DB6 RID: 3510 RVA: 0x000337BE File Offset: 0x000319BE
		internal GYearMonthDataContract() : base(DictionaryGlobals.gYearMonthLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
