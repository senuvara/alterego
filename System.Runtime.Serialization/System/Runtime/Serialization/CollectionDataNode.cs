﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace System.Runtime.Serialization
{
	// Token: 0x020000D5 RID: 213
	internal class CollectionDataNode : DataNode<Array>
	{
		// Token: 0x06000BFE RID: 3070 RVA: 0x0002F277 File Offset: 0x0002D477
		internal CollectionDataNode()
		{
			this.dataType = Globals.TypeOfCollectionDataNode;
		}

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06000BFF RID: 3071 RVA: 0x0002F291 File Offset: 0x0002D491
		// (set) Token: 0x06000C00 RID: 3072 RVA: 0x0002F299 File Offset: 0x0002D499
		internal IList<IDataNode> Items
		{
			get
			{
				return this.items;
			}
			set
			{
				this.items = value;
			}
		}

		// Token: 0x1700021C RID: 540
		// (get) Token: 0x06000C01 RID: 3073 RVA: 0x0002F2A2 File Offset: 0x0002D4A2
		// (set) Token: 0x06000C02 RID: 3074 RVA: 0x0002F2AA File Offset: 0x0002D4AA
		internal string ItemName
		{
			get
			{
				return this.itemName;
			}
			set
			{
				this.itemName = value;
			}
		}

		// Token: 0x1700021D RID: 541
		// (get) Token: 0x06000C03 RID: 3075 RVA: 0x0002F2B3 File Offset: 0x0002D4B3
		// (set) Token: 0x06000C04 RID: 3076 RVA: 0x0002F2BB File Offset: 0x0002D4BB
		internal string ItemNamespace
		{
			get
			{
				return this.itemNamespace;
			}
			set
			{
				this.itemNamespace = value;
			}
		}

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x06000C05 RID: 3077 RVA: 0x0002F2C4 File Offset: 0x0002D4C4
		// (set) Token: 0x06000C06 RID: 3078 RVA: 0x0002F2CC File Offset: 0x0002D4CC
		internal int Size
		{
			get
			{
				return this.size;
			}
			set
			{
				this.size = value;
			}
		}

		// Token: 0x06000C07 RID: 3079 RVA: 0x0002F2D8 File Offset: 0x0002D4D8
		public override void GetData(ElementData element)
		{
			base.GetData(element);
			element.AddAttribute("z", "http://schemas.microsoft.com/2003/10/Serialization/", "Size", this.Size.ToString(NumberFormatInfo.InvariantInfo));
		}

		// Token: 0x06000C08 RID: 3080 RVA: 0x0002F314 File Offset: 0x0002D514
		public override void Clear()
		{
			base.Clear();
			this.items = null;
			this.size = -1;
		}

		// Token: 0x04000513 RID: 1299
		private IList<IDataNode> items;

		// Token: 0x04000514 RID: 1300
		private string itemName;

		// Token: 0x04000515 RID: 1301
		private string itemNamespace;

		// Token: 0x04000516 RID: 1302
		private int size = -1;
	}
}
