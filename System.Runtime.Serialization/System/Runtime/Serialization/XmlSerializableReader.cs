﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace System.Runtime.Serialization
{
	// Token: 0x02000146 RID: 326
	internal class XmlSerializableReader : XmlReader, IXmlLineInfo, IXmlTextParser
	{
		// Token: 0x17000390 RID: 912
		// (get) Token: 0x0600104B RID: 4171 RVA: 0x0003B94F File Offset: 0x00039B4F
		private XmlReader InnerReader
		{
			get
			{
				return this.innerReader;
			}
		}

		// Token: 0x0600104C RID: 4172 RVA: 0x0003B958 File Offset: 0x00039B58
		internal void BeginRead(XmlReaderDelegator xmlReader)
		{
			if (xmlReader.NodeType != XmlNodeType.Element)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.Element, xmlReader));
			}
			this.xmlReader = xmlReader;
			this.startDepth = xmlReader.Depth;
			this.innerReader = xmlReader.UnderlyingReader;
			this.isRootEmptyElement = this.InnerReader.IsEmptyElement;
		}

		// Token: 0x0600104D RID: 4173 RVA: 0x0003B9AC File Offset: 0x00039BAC
		internal void EndRead()
		{
			if (this.isRootEmptyElement)
			{
				this.xmlReader.Read();
				return;
			}
			if (this.xmlReader.IsStartElement() && this.xmlReader.Depth == this.startDepth)
			{
				this.xmlReader.Read();
			}
			while (this.xmlReader.Depth > this.startDepth)
			{
				if (!this.xmlReader.Read())
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.EndElement, this.xmlReader));
				}
			}
		}

		// Token: 0x0600104E RID: 4174 RVA: 0x0003BA30 File Offset: 0x00039C30
		public override bool Read()
		{
			XmlReader xmlReader = this.InnerReader;
			return (xmlReader.Depth != this.startDepth || (xmlReader.NodeType != XmlNodeType.EndElement && (xmlReader.NodeType != XmlNodeType.Element || !xmlReader.IsEmptyElement))) && xmlReader.Read();
		}

		// Token: 0x0600104F RID: 4175 RVA: 0x0003BA75 File Offset: 0x00039C75
		public override void Close()
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("This method cannot be called from IXmlSerializable implementations.")));
		}

		// Token: 0x17000391 RID: 913
		// (get) Token: 0x06001050 RID: 4176 RVA: 0x0003BA8B File Offset: 0x00039C8B
		public override XmlReaderSettings Settings
		{
			get
			{
				return this.InnerReader.Settings;
			}
		}

		// Token: 0x17000392 RID: 914
		// (get) Token: 0x06001051 RID: 4177 RVA: 0x0003BA98 File Offset: 0x00039C98
		public override XmlNodeType NodeType
		{
			get
			{
				return this.InnerReader.NodeType;
			}
		}

		// Token: 0x17000393 RID: 915
		// (get) Token: 0x06001052 RID: 4178 RVA: 0x0003BAA5 File Offset: 0x00039CA5
		public override string Name
		{
			get
			{
				return this.InnerReader.Name;
			}
		}

		// Token: 0x17000394 RID: 916
		// (get) Token: 0x06001053 RID: 4179 RVA: 0x0003BAB2 File Offset: 0x00039CB2
		public override string LocalName
		{
			get
			{
				return this.InnerReader.LocalName;
			}
		}

		// Token: 0x17000395 RID: 917
		// (get) Token: 0x06001054 RID: 4180 RVA: 0x0003BABF File Offset: 0x00039CBF
		public override string NamespaceURI
		{
			get
			{
				return this.InnerReader.NamespaceURI;
			}
		}

		// Token: 0x17000396 RID: 918
		// (get) Token: 0x06001055 RID: 4181 RVA: 0x0003BACC File Offset: 0x00039CCC
		public override string Prefix
		{
			get
			{
				return this.InnerReader.Prefix;
			}
		}

		// Token: 0x17000397 RID: 919
		// (get) Token: 0x06001056 RID: 4182 RVA: 0x0003BAD9 File Offset: 0x00039CD9
		public override bool HasValue
		{
			get
			{
				return this.InnerReader.HasValue;
			}
		}

		// Token: 0x17000398 RID: 920
		// (get) Token: 0x06001057 RID: 4183 RVA: 0x0003BAE6 File Offset: 0x00039CE6
		public override string Value
		{
			get
			{
				return this.InnerReader.Value;
			}
		}

		// Token: 0x17000399 RID: 921
		// (get) Token: 0x06001058 RID: 4184 RVA: 0x0003BAF3 File Offset: 0x00039CF3
		public override int Depth
		{
			get
			{
				return this.InnerReader.Depth;
			}
		}

		// Token: 0x1700039A RID: 922
		// (get) Token: 0x06001059 RID: 4185 RVA: 0x0003BB00 File Offset: 0x00039D00
		public override string BaseURI
		{
			get
			{
				return this.InnerReader.BaseURI;
			}
		}

		// Token: 0x1700039B RID: 923
		// (get) Token: 0x0600105A RID: 4186 RVA: 0x0003BB0D File Offset: 0x00039D0D
		public override bool IsEmptyElement
		{
			get
			{
				return this.InnerReader.IsEmptyElement;
			}
		}

		// Token: 0x1700039C RID: 924
		// (get) Token: 0x0600105B RID: 4187 RVA: 0x0003BB1A File Offset: 0x00039D1A
		public override bool IsDefault
		{
			get
			{
				return this.InnerReader.IsDefault;
			}
		}

		// Token: 0x1700039D RID: 925
		// (get) Token: 0x0600105C RID: 4188 RVA: 0x0003BB27 File Offset: 0x00039D27
		public override char QuoteChar
		{
			get
			{
				return this.InnerReader.QuoteChar;
			}
		}

		// Token: 0x1700039E RID: 926
		// (get) Token: 0x0600105D RID: 4189 RVA: 0x0003BB34 File Offset: 0x00039D34
		public override XmlSpace XmlSpace
		{
			get
			{
				return this.InnerReader.XmlSpace;
			}
		}

		// Token: 0x1700039F RID: 927
		// (get) Token: 0x0600105E RID: 4190 RVA: 0x0003BB41 File Offset: 0x00039D41
		public override string XmlLang
		{
			get
			{
				return this.InnerReader.XmlLang;
			}
		}

		// Token: 0x170003A0 RID: 928
		// (get) Token: 0x0600105F RID: 4191 RVA: 0x0003BB4E File Offset: 0x00039D4E
		public override IXmlSchemaInfo SchemaInfo
		{
			get
			{
				return this.InnerReader.SchemaInfo;
			}
		}

		// Token: 0x170003A1 RID: 929
		// (get) Token: 0x06001060 RID: 4192 RVA: 0x0003BB5B File Offset: 0x00039D5B
		public override Type ValueType
		{
			get
			{
				return this.InnerReader.ValueType;
			}
		}

		// Token: 0x170003A2 RID: 930
		// (get) Token: 0x06001061 RID: 4193 RVA: 0x0003BB68 File Offset: 0x00039D68
		public override int AttributeCount
		{
			get
			{
				return this.InnerReader.AttributeCount;
			}
		}

		// Token: 0x170003A3 RID: 931
		public override string this[int i]
		{
			get
			{
				return this.InnerReader[i];
			}
		}

		// Token: 0x170003A4 RID: 932
		public override string this[string name]
		{
			get
			{
				return this.InnerReader[name];
			}
		}

		// Token: 0x170003A5 RID: 933
		public override string this[string name, string namespaceURI]
		{
			get
			{
				return this.InnerReader[name, namespaceURI];
			}
		}

		// Token: 0x170003A6 RID: 934
		// (get) Token: 0x06001065 RID: 4197 RVA: 0x0003BBA0 File Offset: 0x00039DA0
		public override bool EOF
		{
			get
			{
				return this.InnerReader.EOF;
			}
		}

		// Token: 0x170003A7 RID: 935
		// (get) Token: 0x06001066 RID: 4198 RVA: 0x0003BBAD File Offset: 0x00039DAD
		public override ReadState ReadState
		{
			get
			{
				return this.InnerReader.ReadState;
			}
		}

		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x06001067 RID: 4199 RVA: 0x0003BBBA File Offset: 0x00039DBA
		public override XmlNameTable NameTable
		{
			get
			{
				return this.InnerReader.NameTable;
			}
		}

		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x06001068 RID: 4200 RVA: 0x0003BBC7 File Offset: 0x00039DC7
		public override bool CanResolveEntity
		{
			get
			{
				return this.InnerReader.CanResolveEntity;
			}
		}

		// Token: 0x170003AA RID: 938
		// (get) Token: 0x06001069 RID: 4201 RVA: 0x0003BBD4 File Offset: 0x00039DD4
		public override bool CanReadBinaryContent
		{
			get
			{
				return this.InnerReader.CanReadBinaryContent;
			}
		}

		// Token: 0x170003AB RID: 939
		// (get) Token: 0x0600106A RID: 4202 RVA: 0x0003BBE1 File Offset: 0x00039DE1
		public override bool CanReadValueChunk
		{
			get
			{
				return this.InnerReader.CanReadValueChunk;
			}
		}

		// Token: 0x170003AC RID: 940
		// (get) Token: 0x0600106B RID: 4203 RVA: 0x0003BBEE File Offset: 0x00039DEE
		public override bool HasAttributes
		{
			get
			{
				return this.InnerReader.HasAttributes;
			}
		}

		// Token: 0x0600106C RID: 4204 RVA: 0x0003BBFB File Offset: 0x00039DFB
		public override string GetAttribute(string name)
		{
			return this.InnerReader.GetAttribute(name);
		}

		// Token: 0x0600106D RID: 4205 RVA: 0x0003BC09 File Offset: 0x00039E09
		public override string GetAttribute(string name, string namespaceURI)
		{
			return this.InnerReader.GetAttribute(name, namespaceURI);
		}

		// Token: 0x0600106E RID: 4206 RVA: 0x0003BC18 File Offset: 0x00039E18
		public override string GetAttribute(int i)
		{
			return this.InnerReader.GetAttribute(i);
		}

		// Token: 0x0600106F RID: 4207 RVA: 0x0003BC26 File Offset: 0x00039E26
		public override bool MoveToAttribute(string name)
		{
			return this.InnerReader.MoveToAttribute(name);
		}

		// Token: 0x06001070 RID: 4208 RVA: 0x0003BC34 File Offset: 0x00039E34
		public override bool MoveToAttribute(string name, string ns)
		{
			return this.InnerReader.MoveToAttribute(name, ns);
		}

		// Token: 0x06001071 RID: 4209 RVA: 0x0003BC43 File Offset: 0x00039E43
		public override void MoveToAttribute(int i)
		{
			this.InnerReader.MoveToAttribute(i);
		}

		// Token: 0x06001072 RID: 4210 RVA: 0x0003BC51 File Offset: 0x00039E51
		public override bool MoveToFirstAttribute()
		{
			return this.InnerReader.MoveToFirstAttribute();
		}

		// Token: 0x06001073 RID: 4211 RVA: 0x0003BC5E File Offset: 0x00039E5E
		public override bool MoveToNextAttribute()
		{
			return this.InnerReader.MoveToNextAttribute();
		}

		// Token: 0x06001074 RID: 4212 RVA: 0x0003BC6B File Offset: 0x00039E6B
		public override bool MoveToElement()
		{
			return this.InnerReader.MoveToElement();
		}

		// Token: 0x06001075 RID: 4213 RVA: 0x0003BC78 File Offset: 0x00039E78
		public override string LookupNamespace(string prefix)
		{
			return this.InnerReader.LookupNamespace(prefix);
		}

		// Token: 0x06001076 RID: 4214 RVA: 0x0003BC86 File Offset: 0x00039E86
		public override bool ReadAttributeValue()
		{
			return this.InnerReader.ReadAttributeValue();
		}

		// Token: 0x06001077 RID: 4215 RVA: 0x0003BC93 File Offset: 0x00039E93
		public override void ResolveEntity()
		{
			this.InnerReader.ResolveEntity();
		}

		// Token: 0x06001078 RID: 4216 RVA: 0x0003BCA0 File Offset: 0x00039EA0
		public override bool IsStartElement()
		{
			return this.InnerReader.IsStartElement();
		}

		// Token: 0x06001079 RID: 4217 RVA: 0x0003BCAD File Offset: 0x00039EAD
		public override bool IsStartElement(string name)
		{
			return this.InnerReader.IsStartElement(name);
		}

		// Token: 0x0600107A RID: 4218 RVA: 0x0003BCBB File Offset: 0x00039EBB
		public override bool IsStartElement(string localname, string ns)
		{
			return this.InnerReader.IsStartElement(localname, ns);
		}

		// Token: 0x0600107B RID: 4219 RVA: 0x0003BCCA File Offset: 0x00039ECA
		public override XmlNodeType MoveToContent()
		{
			return this.InnerReader.MoveToContent();
		}

		// Token: 0x0600107C RID: 4220 RVA: 0x0003BCD7 File Offset: 0x00039ED7
		public override object ReadContentAsObject()
		{
			return this.InnerReader.ReadContentAsObject();
		}

		// Token: 0x0600107D RID: 4221 RVA: 0x0003BCE4 File Offset: 0x00039EE4
		public override bool ReadContentAsBoolean()
		{
			return this.InnerReader.ReadContentAsBoolean();
		}

		// Token: 0x0600107E RID: 4222 RVA: 0x0003BCF1 File Offset: 0x00039EF1
		public override DateTime ReadContentAsDateTime()
		{
			return this.InnerReader.ReadContentAsDateTime();
		}

		// Token: 0x0600107F RID: 4223 RVA: 0x0003BCFE File Offset: 0x00039EFE
		public override double ReadContentAsDouble()
		{
			return this.InnerReader.ReadContentAsDouble();
		}

		// Token: 0x06001080 RID: 4224 RVA: 0x0003BD0B File Offset: 0x00039F0B
		public override int ReadContentAsInt()
		{
			return this.InnerReader.ReadContentAsInt();
		}

		// Token: 0x06001081 RID: 4225 RVA: 0x0003BD18 File Offset: 0x00039F18
		public override long ReadContentAsLong()
		{
			return this.InnerReader.ReadContentAsLong();
		}

		// Token: 0x06001082 RID: 4226 RVA: 0x0003BD25 File Offset: 0x00039F25
		public override string ReadContentAsString()
		{
			return this.InnerReader.ReadContentAsString();
		}

		// Token: 0x06001083 RID: 4227 RVA: 0x0003BD32 File Offset: 0x00039F32
		public override object ReadContentAs(Type returnType, IXmlNamespaceResolver namespaceResolver)
		{
			return this.InnerReader.ReadContentAs(returnType, namespaceResolver);
		}

		// Token: 0x06001084 RID: 4228 RVA: 0x0003BD41 File Offset: 0x00039F41
		public override int ReadContentAsBase64(byte[] buffer, int index, int count)
		{
			return this.InnerReader.ReadContentAsBase64(buffer, index, count);
		}

		// Token: 0x06001085 RID: 4229 RVA: 0x0003BD51 File Offset: 0x00039F51
		public override int ReadContentAsBinHex(byte[] buffer, int index, int count)
		{
			return this.InnerReader.ReadContentAsBinHex(buffer, index, count);
		}

		// Token: 0x06001086 RID: 4230 RVA: 0x0003BD61 File Offset: 0x00039F61
		public override int ReadValueChunk(char[] buffer, int index, int count)
		{
			return this.InnerReader.ReadValueChunk(buffer, index, count);
		}

		// Token: 0x06001087 RID: 4231 RVA: 0x0003BD71 File Offset: 0x00039F71
		public override string ReadString()
		{
			return this.InnerReader.ReadString();
		}

		// Token: 0x170003AD RID: 941
		// (get) Token: 0x06001088 RID: 4232 RVA: 0x0003BD80 File Offset: 0x00039F80
		// (set) Token: 0x06001089 RID: 4233 RVA: 0x0003BDB0 File Offset: 0x00039FB0
		bool IXmlTextParser.Normalized
		{
			get
			{
				IXmlTextParser xmlTextParser = this.InnerReader as IXmlTextParser;
				if (xmlTextParser != null)
				{
					return xmlTextParser.Normalized;
				}
				return this.xmlReader.Normalized;
			}
			set
			{
				IXmlTextParser xmlTextParser = this.InnerReader as IXmlTextParser;
				if (xmlTextParser == null)
				{
					this.xmlReader.Normalized = value;
					return;
				}
				xmlTextParser.Normalized = value;
			}
		}

		// Token: 0x170003AE RID: 942
		// (get) Token: 0x0600108A RID: 4234 RVA: 0x0003BDE0 File Offset: 0x00039FE0
		// (set) Token: 0x0600108B RID: 4235 RVA: 0x0003BE10 File Offset: 0x0003A010
		WhitespaceHandling IXmlTextParser.WhitespaceHandling
		{
			get
			{
				IXmlTextParser xmlTextParser = this.InnerReader as IXmlTextParser;
				if (xmlTextParser != null)
				{
					return xmlTextParser.WhitespaceHandling;
				}
				return this.xmlReader.WhitespaceHandling;
			}
			set
			{
				IXmlTextParser xmlTextParser = this.InnerReader as IXmlTextParser;
				if (xmlTextParser == null)
				{
					this.xmlReader.WhitespaceHandling = value;
					return;
				}
				xmlTextParser.WhitespaceHandling = value;
			}
		}

		// Token: 0x0600108C RID: 4236 RVA: 0x0003BE40 File Offset: 0x0003A040
		bool IXmlLineInfo.HasLineInfo()
		{
			IXmlLineInfo xmlLineInfo = this.InnerReader as IXmlLineInfo;
			if (xmlLineInfo != null)
			{
				return xmlLineInfo.HasLineInfo();
			}
			return this.xmlReader.HasLineInfo();
		}

		// Token: 0x170003AF RID: 943
		// (get) Token: 0x0600108D RID: 4237 RVA: 0x0003BE70 File Offset: 0x0003A070
		int IXmlLineInfo.LineNumber
		{
			get
			{
				IXmlLineInfo xmlLineInfo = this.InnerReader as IXmlLineInfo;
				if (xmlLineInfo != null)
				{
					return xmlLineInfo.LineNumber;
				}
				return this.xmlReader.LineNumber;
			}
		}

		// Token: 0x170003B0 RID: 944
		// (get) Token: 0x0600108E RID: 4238 RVA: 0x0003BEA0 File Offset: 0x0003A0A0
		int IXmlLineInfo.LinePosition
		{
			get
			{
				IXmlLineInfo xmlLineInfo = this.InnerReader as IXmlLineInfo;
				if (xmlLineInfo != null)
				{
					return xmlLineInfo.LinePosition;
				}
				return this.xmlReader.LinePosition;
			}
		}

		// Token: 0x0600108F RID: 4239 RVA: 0x00018219 File Offset: 0x00016419
		public XmlSerializableReader()
		{
		}

		// Token: 0x040006F1 RID: 1777
		private XmlReaderDelegator xmlReader;

		// Token: 0x040006F2 RID: 1778
		private int startDepth;

		// Token: 0x040006F3 RID: 1779
		private bool isRootEmptyElement;

		// Token: 0x040006F4 RID: 1780
		private XmlReader innerReader;
	}
}
