﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000F3 RID: 243
	internal class ShortDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D75 RID: 3445 RVA: 0x00033330 File Offset: 0x00031530
		internal ShortDataContract() : base(typeof(short), DictionaryGlobals.ShortLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x06000D76 RID: 3446 RVA: 0x0003334C File Offset: 0x0003154C
		internal override string WriteMethodName
		{
			get
			{
				return "WriteShort";
			}
		}

		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x06000D77 RID: 3447 RVA: 0x00033353 File Offset: 0x00031553
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsShort";
			}
		}

		// Token: 0x06000D78 RID: 3448 RVA: 0x0003335A File Offset: 0x0003155A
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteShort((short)obj);
		}

		// Token: 0x06000D79 RID: 3449 RVA: 0x00033368 File Offset: 0x00031568
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsShort(), context);
			}
			return reader.ReadElementContentAsShort();
		}
	}
}
