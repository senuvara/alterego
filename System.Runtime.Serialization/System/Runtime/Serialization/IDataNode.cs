﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000D2 RID: 210
	internal interface IDataNode
	{
		// Token: 0x17000208 RID: 520
		// (get) Token: 0x06000BD2 RID: 3026
		Type DataType { get; }

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x06000BD3 RID: 3027
		// (set) Token: 0x06000BD4 RID: 3028
		object Value { get; set; }

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x06000BD5 RID: 3029
		// (set) Token: 0x06000BD6 RID: 3030
		string DataContractName { get; set; }

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x06000BD7 RID: 3031
		// (set) Token: 0x06000BD8 RID: 3032
		string DataContractNamespace { get; set; }

		// Token: 0x1700020C RID: 524
		// (get) Token: 0x06000BD9 RID: 3033
		// (set) Token: 0x06000BDA RID: 3034
		string ClrTypeName { get; set; }

		// Token: 0x1700020D RID: 525
		// (get) Token: 0x06000BDB RID: 3035
		// (set) Token: 0x06000BDC RID: 3036
		string ClrAssemblyName { get; set; }

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x06000BDD RID: 3037
		// (set) Token: 0x06000BDE RID: 3038
		string Id { get; set; }

		// Token: 0x1700020F RID: 527
		// (get) Token: 0x06000BDF RID: 3039
		bool PreservesReferences { get; }

		// Token: 0x06000BE0 RID: 3040
		void GetData(ElementData element);

		// Token: 0x17000210 RID: 528
		// (get) Token: 0x06000BE1 RID: 3041
		// (set) Token: 0x06000BE2 RID: 3042
		bool IsFinalValue { get; set; }

		// Token: 0x06000BE3 RID: 3043
		void Clear();
	}
}
