﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Diagnostics.Application;
using System.Security;

namespace System.Runtime.Serialization
{
	// Token: 0x02000138 RID: 312
	internal sealed class XmlFormatWriterGenerator
	{
		// Token: 0x06000EC2 RID: 3778 RVA: 0x00035CA4 File Offset: 0x00033EA4
		[SecurityCritical]
		public XmlFormatWriterGenerator()
		{
			this.helper = new XmlFormatWriterGenerator.CriticalHelper();
		}

		// Token: 0x06000EC3 RID: 3779 RVA: 0x00035CB8 File Offset: 0x00033EB8
		[SecurityCritical]
		internal XmlFormatClassWriterDelegate GenerateClassWriter(ClassDataContract classContract)
		{
			XmlFormatClassWriterDelegate result;
			try
			{
				if (TD.DCGenWriterStartIsEnabled())
				{
					TD.DCGenWriterStart("Class", classContract.UnderlyingType.FullName);
				}
				result = this.helper.GenerateClassWriter(classContract);
			}
			finally
			{
				if (TD.DCGenWriterStopIsEnabled())
				{
					TD.DCGenWriterStop();
				}
			}
			return result;
		}

		// Token: 0x06000EC4 RID: 3780 RVA: 0x00035D10 File Offset: 0x00033F10
		[SecurityCritical]
		internal XmlFormatCollectionWriterDelegate GenerateCollectionWriter(CollectionDataContract collectionContract)
		{
			XmlFormatCollectionWriterDelegate result;
			try
			{
				if (TD.DCGenWriterStartIsEnabled())
				{
					TD.DCGenWriterStart("Collection", collectionContract.UnderlyingType.FullName);
				}
				result = this.helper.GenerateCollectionWriter(collectionContract);
			}
			finally
			{
				if (TD.DCGenWriterStopIsEnabled())
				{
					TD.DCGenWriterStop();
				}
			}
			return result;
		}

		// Token: 0x040006B8 RID: 1720
		[SecurityCritical]
		private XmlFormatWriterGenerator.CriticalHelper helper;

		// Token: 0x02000139 RID: 313
		private class CriticalHelper
		{
			// Token: 0x06000EC5 RID: 3781 RVA: 0x00035D68 File Offset: 0x00033F68
			internal XmlFormatClassWriterDelegate GenerateClassWriter(ClassDataContract classContract)
			{
				return delegate(XmlWriterDelegator xw, object obj, XmlObjectSerializerWriteContext ctx, ClassDataContract ctr)
				{
					new XmlFormatWriterInterpreter(classContract).WriteToXml(xw, obj, ctx, ctr);
				};
			}

			// Token: 0x06000EC6 RID: 3782 RVA: 0x00035D81 File Offset: 0x00033F81
			internal XmlFormatCollectionWriterDelegate GenerateCollectionWriter(CollectionDataContract collectionContract)
			{
				return delegate(XmlWriterDelegator xw, object obj, XmlObjectSerializerWriteContext ctx, CollectionDataContract ctr)
				{
					new XmlFormatWriterInterpreter(collectionContract).WriteCollectionToXml(xw, obj, ctx, ctr);
				};
			}

			// Token: 0x06000EC7 RID: 3783 RVA: 0x00002217 File Offset: 0x00000417
			public CriticalHelper()
			{
			}

			// Token: 0x0200013A RID: 314
			[CompilerGenerated]
			private sealed class <>c__DisplayClass0_0
			{
				// Token: 0x06000EC8 RID: 3784 RVA: 0x00002217 File Offset: 0x00000417
				public <>c__DisplayClass0_0()
				{
				}

				// Token: 0x06000EC9 RID: 3785 RVA: 0x00035D9A File Offset: 0x00033F9A
				internal void <GenerateClassWriter>b__0(XmlWriterDelegator xw, object obj, XmlObjectSerializerWriteContext ctx, ClassDataContract ctr)
				{
					new XmlFormatWriterInterpreter(this.classContract).WriteToXml(xw, obj, ctx, ctr);
				}

				// Token: 0x040006B9 RID: 1721
				public ClassDataContract classContract;
			}

			// Token: 0x0200013B RID: 315
			[CompilerGenerated]
			private sealed class <>c__DisplayClass1_0
			{
				// Token: 0x06000ECA RID: 3786 RVA: 0x00002217 File Offset: 0x00000417
				public <>c__DisplayClass1_0()
				{
				}

				// Token: 0x06000ECB RID: 3787 RVA: 0x00035DB1 File Offset: 0x00033FB1
				internal void <GenerateCollectionWriter>b__0(XmlWriterDelegator xw, object obj, XmlObjectSerializerWriteContext ctx, CollectionDataContract ctr)
				{
					new XmlFormatWriterInterpreter(this.collectionContract).WriteCollectionToXml(xw, obj, ctx, ctr);
				}

				// Token: 0x040006BA RID: 1722
				public CollectionDataContract collectionContract;
			}
		}
	}
}
