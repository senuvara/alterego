﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x0200010E RID: 270
	internal class NameDataContract : StringDataContract
	{
		// Token: 0x06000DBE RID: 3518 RVA: 0x0003384E File Offset: 0x00031A4E
		internal NameDataContract() : base(DictionaryGlobals.NameLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
