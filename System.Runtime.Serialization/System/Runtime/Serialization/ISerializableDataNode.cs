﻿using System;
using System.Collections.Generic;

namespace System.Runtime.Serialization
{
	// Token: 0x020000D7 RID: 215
	internal class ISerializableDataNode : DataNode<object>
	{
		// Token: 0x06000C11 RID: 3089 RVA: 0x0002F38D File Offset: 0x0002D58D
		internal ISerializableDataNode()
		{
			this.dataType = Globals.TypeOfISerializableDataNode;
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x06000C12 RID: 3090 RVA: 0x0002F3A0 File Offset: 0x0002D5A0
		// (set) Token: 0x06000C13 RID: 3091 RVA: 0x0002F3A8 File Offset: 0x0002D5A8
		internal string FactoryTypeName
		{
			get
			{
				return this.factoryTypeName;
			}
			set
			{
				this.factoryTypeName = value;
			}
		}

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x06000C14 RID: 3092 RVA: 0x0002F3B1 File Offset: 0x0002D5B1
		// (set) Token: 0x06000C15 RID: 3093 RVA: 0x0002F3B9 File Offset: 0x0002D5B9
		internal string FactoryTypeNamespace
		{
			get
			{
				return this.factoryTypeNamespace;
			}
			set
			{
				this.factoryTypeNamespace = value;
			}
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x06000C16 RID: 3094 RVA: 0x0002F3C2 File Offset: 0x0002D5C2
		// (set) Token: 0x06000C17 RID: 3095 RVA: 0x0002F3CA File Offset: 0x0002D5CA
		internal IList<ISerializableDataMember> Members
		{
			get
			{
				return this.members;
			}
			set
			{
				this.members = value;
			}
		}

		// Token: 0x06000C18 RID: 3096 RVA: 0x0002F3D3 File Offset: 0x0002D5D3
		public override void GetData(ElementData element)
		{
			base.GetData(element);
			if (this.FactoryTypeName != null)
			{
				base.AddQualifiedNameAttribute(element, "z", "FactoryType", "http://schemas.microsoft.com/2003/10/Serialization/", this.FactoryTypeName, this.FactoryTypeNamespace);
			}
		}

		// Token: 0x06000C19 RID: 3097 RVA: 0x0002F408 File Offset: 0x0002D608
		public override void Clear()
		{
			base.Clear();
			this.members = null;
			this.factoryTypeName = (this.factoryTypeNamespace = null);
		}

		// Token: 0x0400051A RID: 1306
		private string factoryTypeName;

		// Token: 0x0400051B RID: 1307
		private string factoryTypeNamespace;

		// Token: 0x0400051C RID: 1308
		private IList<ISerializableDataMember> members;
	}
}
