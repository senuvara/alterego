﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000BA RID: 186
	internal class GenericNameProvider : IGenericNameProvider
	{
		// Token: 0x06000AAC RID: 2732 RVA: 0x0002C2AC File Offset: 0x0002A4AC
		internal GenericNameProvider(Type type) : this(DataContract.GetClrTypeFullName(type.GetGenericTypeDefinition()), type.GetGenericArguments())
		{
		}

		// Token: 0x06000AAD RID: 2733 RVA: 0x0002C2C8 File Offset: 0x0002A4C8
		internal GenericNameProvider(string genericTypeName, object[] genericParams)
		{
			this.genericTypeName = genericTypeName;
			this.genericParams = new object[genericParams.Length];
			genericParams.CopyTo(this.genericParams, 0);
			string typeName;
			string text;
			DataContract.GetClrNameAndNamespace(genericTypeName, out typeName, out text);
			this.nestedParamCounts = DataContract.GetDataContractNameForGenericName(typeName, null);
		}

		// Token: 0x06000AAE RID: 2734 RVA: 0x0002C314 File Offset: 0x0002A514
		public int GetParameterCount()
		{
			return this.genericParams.Length;
		}

		// Token: 0x06000AAF RID: 2735 RVA: 0x0002C31E File Offset: 0x0002A51E
		public IList<int> GetNestedParameterCounts()
		{
			return this.nestedParamCounts;
		}

		// Token: 0x06000AB0 RID: 2736 RVA: 0x0002C326 File Offset: 0x0002A526
		public string GetParameterName(int paramIndex)
		{
			return this.GetStableName(paramIndex).Name;
		}

		// Token: 0x06000AB1 RID: 2737 RVA: 0x0002C334 File Offset: 0x0002A534
		public string GetNamespaces()
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < this.GetParameterCount(); i++)
			{
				stringBuilder.Append(" ").Append(this.GetStableName(i).Namespace);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000AB2 RID: 2738 RVA: 0x0002C37B File Offset: 0x0002A57B
		public string GetGenericTypeName()
		{
			return this.genericTypeName;
		}

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x06000AB3 RID: 2739 RVA: 0x0002C384 File Offset: 0x0002A584
		public bool ParametersFromBuiltInNamespaces
		{
			get
			{
				bool flag = true;
				int num = 0;
				while (num < this.GetParameterCount() && flag)
				{
					flag = DataContract.IsBuiltInNamespace(this.GetStableName(num).Namespace);
					num++;
				}
				return flag;
			}
		}

		// Token: 0x06000AB4 RID: 2740 RVA: 0x0002C3BC File Offset: 0x0002A5BC
		private XmlQualifiedName GetStableName(int i)
		{
			object obj = this.genericParams[i];
			XmlQualifiedName xmlQualifiedName = obj as XmlQualifiedName;
			if (xmlQualifiedName == null)
			{
				Type type = obj as Type;
				if (type != null)
				{
					xmlQualifiedName = (this.genericParams[i] = DataContract.GetStableName(type));
				}
				else
				{
					xmlQualifiedName = (this.genericParams[i] = ((DataContract)obj).StableName);
				}
			}
			return xmlQualifiedName;
		}

		// Token: 0x04000470 RID: 1136
		private string genericTypeName;

		// Token: 0x04000471 RID: 1137
		private object[] genericParams;

		// Token: 0x04000472 RID: 1138
		private IList<int> nestedParamCounts;
	}
}
