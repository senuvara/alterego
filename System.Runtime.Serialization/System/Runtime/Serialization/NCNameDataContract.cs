﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x0200010F RID: 271
	internal class NCNameDataContract : StringDataContract
	{
		// Token: 0x06000DBF RID: 3519 RVA: 0x00033860 File Offset: 0x00031A60
		internal NCNameDataContract() : base(DictionaryGlobals.NCNameLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
