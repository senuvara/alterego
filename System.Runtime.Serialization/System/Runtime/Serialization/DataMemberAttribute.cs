﻿using System;

namespace System.Runtime.Serialization
{
	/// <summary>When applied to the member of a type, specifies that the member is part of a data contract and is serializable by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />.</summary>
	// Token: 0x020000C7 RID: 199
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
	public sealed class DataMemberAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.DataMemberAttribute" /> class. </summary>
		// Token: 0x06000B7A RID: 2938 RVA: 0x0002DEBC File Offset: 0x0002C0BC
		public DataMemberAttribute()
		{
		}

		/// <summary>Gets or sets a data member name. </summary>
		/// <returns>The name of the data member. The default is the name of the target that the attribute is applied to. </returns>
		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x06000B7B RID: 2939 RVA: 0x0002DED2 File Offset: 0x0002C0D2
		// (set) Token: 0x06000B7C RID: 2940 RVA: 0x0002DEDA File Offset: 0x0002C0DA
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
				this.isNameSetExplicitly = true;
			}
		}

		/// <summary>Gets whether <see cref="P:System.Runtime.Serialization.DataMemberAttribute.Name" /> has been explicitly set.</summary>
		/// <returns>Returns <see langword="true" /> if the name has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x06000B7D RID: 2941 RVA: 0x0002DEEA File Offset: 0x0002C0EA
		public bool IsNameSetExplicitly
		{
			get
			{
				return this.isNameSetExplicitly;
			}
		}

		/// <summary>Gets or sets the order of serialization and deserialization of a member.</summary>
		/// <returns>The numeric order of serialization or deserialization.</returns>
		// Token: 0x170001EA RID: 490
		// (get) Token: 0x06000B7E RID: 2942 RVA: 0x0002DEF2 File Offset: 0x0002C0F2
		// (set) Token: 0x06000B7F RID: 2943 RVA: 0x0002DEFA File Offset: 0x0002C0FA
		public int Order
		{
			get
			{
				return this.order;
			}
			set
			{
				if (value < 0)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Property 'Order' in DataMemberAttribute attribute cannot be a negative number.")));
				}
				this.order = value;
			}
		}

		/// <summary>Gets or sets a value that instructs the serialization engine that the member must be present when reading or deserializing.</summary>
		/// <returns>
		///     <see langword="true" />, if the member is required; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">the member is not present.</exception>
		// Token: 0x170001EB RID: 491
		// (get) Token: 0x06000B80 RID: 2944 RVA: 0x0002DF1C File Offset: 0x0002C11C
		// (set) Token: 0x06000B81 RID: 2945 RVA: 0x0002DF24 File Offset: 0x0002C124
		public bool IsRequired
		{
			get
			{
				return this.isRequired;
			}
			set
			{
				this.isRequired = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether to serialize the default value for a field or property being serialized. </summary>
		/// <returns>
		///     <see langword="true" /> if the default value for a member should be generated in the serialization stream; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x170001EC RID: 492
		// (get) Token: 0x06000B82 RID: 2946 RVA: 0x0002DF2D File Offset: 0x0002C12D
		// (set) Token: 0x06000B83 RID: 2947 RVA: 0x0002DF35 File Offset: 0x0002C135
		public bool EmitDefaultValue
		{
			get
			{
				return this.emitDefaultValue;
			}
			set
			{
				this.emitDefaultValue = value;
			}
		}

		// Token: 0x040004AC RID: 1196
		private string name;

		// Token: 0x040004AD RID: 1197
		private bool isNameSetExplicitly;

		// Token: 0x040004AE RID: 1198
		private int order = -1;

		// Token: 0x040004AF RID: 1199
		private bool isRequired;

		// Token: 0x040004B0 RID: 1200
		private bool emitDefaultValue = true;
	}
}
