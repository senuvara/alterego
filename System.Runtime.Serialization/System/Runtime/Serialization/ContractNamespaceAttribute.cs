﻿using System;

namespace System.Runtime.Serialization
{
	/// <summary>Specifies the CLR namespace and XML namespace of the data contract. </summary>
	// Token: 0x020000B6 RID: 182
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Module, Inherited = false, AllowMultiple = true)]
	public sealed class ContractNamespaceAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.ContractNamespaceAttribute" /> class using the supplied namespace. </summary>
		/// <param name="contractNamespace">The namespace of the contract.</param>
		// Token: 0x060009F9 RID: 2553 RVA: 0x000292AE File Offset: 0x000274AE
		public ContractNamespaceAttribute(string contractNamespace)
		{
			this.contractNamespace = contractNamespace;
		}

		/// <summary>Gets or sets the CLR namespace of the data contract type. </summary>
		/// <returns>The CLR-legal namespace of a type.</returns>
		// Token: 0x17000186 RID: 390
		// (get) Token: 0x060009FA RID: 2554 RVA: 0x000292BD File Offset: 0x000274BD
		// (set) Token: 0x060009FB RID: 2555 RVA: 0x000292C5 File Offset: 0x000274C5
		public string ClrNamespace
		{
			get
			{
				return this.clrNamespace;
			}
			set
			{
				this.clrNamespace = value;
			}
		}

		/// <summary>Gets the namespace of the data contract members.</summary>
		/// <returns>The namespace of the data contract members.</returns>
		// Token: 0x17000187 RID: 391
		// (get) Token: 0x060009FC RID: 2556 RVA: 0x000292CE File Offset: 0x000274CE
		public string ContractNamespace
		{
			get
			{
				return this.contractNamespace;
			}
		}

		// Token: 0x04000451 RID: 1105
		private string clrNamespace;

		// Token: 0x04000452 RID: 1106
		private string contractNamespace;
	}
}
