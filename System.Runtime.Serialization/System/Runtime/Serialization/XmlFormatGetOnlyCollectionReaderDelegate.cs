﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x02000130 RID: 304
	// (Invoke) Token: 0x06000EA8 RID: 3752
	internal delegate void XmlFormatGetOnlyCollectionReaderDelegate(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContext context, XmlDictionaryString itemName, XmlDictionaryString itemNamespace, CollectionDataContract collectionContract);
}
