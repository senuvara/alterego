﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;

namespace System.Runtime.Serialization
{
	/// <summary>When given a class representing a data contract, and metadata representing a member of the contract, produces an XPath query for the member.</summary>
	// Token: 0x02000127 RID: 295
	public static class XPathQueryGenerator
	{
		/// <summary>Creates an XPath from a data contract using the specified data contract type, array of metadata elements, and namespaces..</summary>
		/// <param name="type">The type that represents a data contract. </param>
		/// <param name="pathToMember">The metadata, generated using the <see cref="Overload:System.Type.GetMember" /> method of the <see cref="T:System.Type" /> class, that points to the specific data member used to generate the query.</param>
		/// <param name="namespaces">The XML namespaces and their prefixes found in the data contract.</param>
		/// <returns>
		///     <see cref="T:System.String" />
		///   The XPath generated from the type and member data.</returns>
		// Token: 0x06000E07 RID: 3591 RVA: 0x00033EDC File Offset: 0x000320DC
		public static string CreateFromDataContractSerializer(Type type, MemberInfo[] pathToMember, out XmlNamespaceManager namespaces)
		{
			return XPathQueryGenerator.CreateFromDataContractSerializer(type, pathToMember, null, out namespaces);
		}

		/// <summary>Creates an XPath from a data contract using the specified contract data type, array of metadata elements, the top level element, and namespaces.</summary>
		/// <param name="type">The type that represents a data contract.</param>
		/// <param name="pathToMember">The metadata, generated using the <see cref="Overload:System.Type.GetMember" /> method of the <see cref="T:System.Type" /> class, that points to the specific data member used to generate the query.</param>
		/// <param name="rootElementXpath">The top level element in the xpath.</param>
		/// <param name="namespaces">The XML namespaces and their prefixes found in the data contract.</param>
		/// <returns>
		///     <see cref="T:System.String" />
		///   The XPath generated from the type and member data.</returns>
		// Token: 0x06000E08 RID: 3592 RVA: 0x00033EE8 File Offset: 0x000320E8
		public static string CreateFromDataContractSerializer(Type type, MemberInfo[] pathToMember, StringBuilder rootElementXpath, out XmlNamespaceManager namespaces)
		{
			if (type == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentNullException("type"));
			}
			if (pathToMember == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentNullException("pathToMember"));
			}
			DataContract dataContract = DataContract.GetDataContract(type);
			XPathQueryGenerator.ExportContext exportContext;
			if (rootElementXpath == null)
			{
				exportContext = new XPathQueryGenerator.ExportContext(dataContract);
			}
			else
			{
				exportContext = new XPathQueryGenerator.ExportContext(rootElementXpath);
			}
			for (int i = 0; i < pathToMember.Length; i++)
			{
				dataContract = XPathQueryGenerator.ProcessDataContract(dataContract, exportContext, pathToMember[i]);
			}
			namespaces = exportContext.Namespaces;
			return exportContext.XPath;
		}

		// Token: 0x06000E09 RID: 3593 RVA: 0x00033F62 File Offset: 0x00032162
		private static DataContract ProcessDataContract(DataContract contract, XPathQueryGenerator.ExportContext context, MemberInfo memberNode)
		{
			if (contract is ClassDataContract)
			{
				return XPathQueryGenerator.ProcessClassDataContract((ClassDataContract)contract, context, memberNode);
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("The path to member was not found for XPath query generator.")));
		}

		// Token: 0x06000E0A RID: 3594 RVA: 0x00033F90 File Offset: 0x00032190
		private static DataContract ProcessClassDataContract(ClassDataContract contract, XPathQueryGenerator.ExportContext context, MemberInfo memberNode)
		{
			string prefix = context.SetNamespace(contract.Namespace.Value);
			foreach (DataMember dataMember in XPathQueryGenerator.GetDataMembers(contract))
			{
				if (dataMember.MemberInfo.Name == memberNode.Name && dataMember.MemberInfo.DeclaringType.IsAssignableFrom(memberNode.DeclaringType))
				{
					context.WriteChildToContext(dataMember, prefix);
					return dataMember.MemberTypeContract;
				}
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("The path to member was not found for XPath query generator.")));
		}

		// Token: 0x06000E0B RID: 3595 RVA: 0x00034040 File Offset: 0x00032240
		private static IEnumerable<DataMember> GetDataMembers(ClassDataContract contract)
		{
			if (contract.BaseContract != null)
			{
				foreach (DataMember dataMember in XPathQueryGenerator.GetDataMembers(contract.BaseContract))
				{
					yield return dataMember;
				}
				IEnumerator<DataMember> enumerator = null;
			}
			if (contract.Members != null)
			{
				foreach (DataMember dataMember2 in contract.Members)
				{
					yield return dataMember2;
				}
				List<DataMember>.Enumerator enumerator2 = default(List<DataMember>.Enumerator);
			}
			yield break;
			yield break;
		}

		// Token: 0x0400064E RID: 1614
		private const string XPathSeparator = "/";

		// Token: 0x0400064F RID: 1615
		private const string NsSeparator = ":";

		// Token: 0x02000128 RID: 296
		private class ExportContext
		{
			// Token: 0x06000E0C RID: 3596 RVA: 0x00034050 File Offset: 0x00032250
			public ExportContext(DataContract rootContract)
			{
				this.namespaces = new XmlNamespaceManager(new NameTable());
				string str = this.SetNamespace(rootContract.TopLevelElementNamespace.Value);
				this.xPathBuilder = new StringBuilder("/" + str + ":" + rootContract.TopLevelElementName.Value);
			}

			// Token: 0x06000E0D RID: 3597 RVA: 0x000340AB File Offset: 0x000322AB
			public ExportContext(StringBuilder rootContractXPath)
			{
				this.namespaces = new XmlNamespaceManager(new NameTable());
				this.xPathBuilder = rootContractXPath;
			}

			// Token: 0x06000E0E RID: 3598 RVA: 0x000340CA File Offset: 0x000322CA
			public void WriteChildToContext(DataMember contextMember, string prefix)
			{
				this.xPathBuilder.Append("/" + prefix + ":" + contextMember.Name);
			}

			// Token: 0x17000303 RID: 771
			// (get) Token: 0x06000E0F RID: 3599 RVA: 0x000340EE File Offset: 0x000322EE
			public XmlNamespaceManager Namespaces
			{
				get
				{
					return this.namespaces;
				}
			}

			// Token: 0x17000304 RID: 772
			// (get) Token: 0x06000E10 RID: 3600 RVA: 0x000340F6 File Offset: 0x000322F6
			public string XPath
			{
				get
				{
					return this.xPathBuilder.ToString();
				}
			}

			// Token: 0x06000E11 RID: 3601 RVA: 0x00034104 File Offset: 0x00032304
			public string SetNamespace(string ns)
			{
				string text = this.namespaces.LookupPrefix(ns);
				if (text == null || text.Length == 0)
				{
					string str = "xg";
					int num = this.nextPrefix;
					this.nextPrefix = num + 1;
					text = str + num.ToString(NumberFormatInfo.InvariantInfo);
					this.Namespaces.AddNamespace(text, ns);
				}
				return text;
			}

			// Token: 0x04000650 RID: 1616
			private XmlNamespaceManager namespaces;

			// Token: 0x04000651 RID: 1617
			private int nextPrefix;

			// Token: 0x04000652 RID: 1618
			private StringBuilder xPathBuilder;
		}

		// Token: 0x02000129 RID: 297
		[CompilerGenerated]
		private sealed class <GetDataMembers>d__6 : IEnumerable<DataMember>, IEnumerable, IEnumerator<DataMember>, IDisposable, IEnumerator
		{
			// Token: 0x06000E12 RID: 3602 RVA: 0x0003415E File Offset: 0x0003235E
			[DebuggerHidden]
			public <GetDataMembers>d__6(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000E13 RID: 3603 RVA: 0x00034178 File Offset: 0x00032378
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				switch (this.<>1__state)
				{
				case -4:
				case 2:
					break;
				case -3:
				case 1:
					try
					{
						return;
					}
					finally
					{
						this.<>m__Finally1();
					}
					break;
				case -2:
				case -1:
				case 0:
					return;
				default:
					return;
				}
				try
				{
				}
				finally
				{
					this.<>m__Finally2();
				}
			}

			// Token: 0x06000E14 RID: 3604 RVA: 0x000341E4 File Offset: 0x000323E4
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						if (contract.BaseContract == null)
						{
							goto IL_9C;
						}
						enumerator = XPathQueryGenerator.GetDataMembers(contract.BaseContract).GetEnumerator();
						this.<>1__state = -3;
						break;
					case 1:
						this.<>1__state = -3;
						break;
					case 2:
						this.<>1__state = -4;
						goto IL_EF;
					default:
						return false;
					}
					if (enumerator.MoveNext())
					{
						DataMember dataMember = enumerator.Current;
						this.<>2__current = dataMember;
						this.<>1__state = 1;
						return true;
					}
					this.<>m__Finally1();
					enumerator = null;
					IL_9C:
					if (contract.Members == null)
					{
						goto IL_10E;
					}
					enumerator2 = contract.Members.GetEnumerator();
					this.<>1__state = -4;
					IL_EF:
					if (enumerator2.MoveNext())
					{
						DataMember dataMember2 = enumerator2.Current;
						this.<>2__current = dataMember2;
						this.<>1__state = 2;
						return true;
					}
					this.<>m__Finally2();
					enumerator2 = default(List<DataMember>.Enumerator);
					IL_10E:
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000E15 RID: 3605 RVA: 0x00034328 File Offset: 0x00032528
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x06000E16 RID: 3606 RVA: 0x00034344 File Offset: 0x00032544
			private void <>m__Finally2()
			{
				this.<>1__state = -1;
				((IDisposable)enumerator2).Dispose();
			}

			// Token: 0x17000305 RID: 773
			// (get) Token: 0x06000E17 RID: 3607 RVA: 0x0003435E File Offset: 0x0003255E
			DataMember IEnumerator<DataMember>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000E18 RID: 3608 RVA: 0x00034366 File Offset: 0x00032566
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000306 RID: 774
			// (get) Token: 0x06000E19 RID: 3609 RVA: 0x0003435E File Offset: 0x0003255E
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000E1A RID: 3610 RVA: 0x00034370 File Offset: 0x00032570
			[DebuggerHidden]
			IEnumerator<DataMember> IEnumerable<DataMember>.GetEnumerator()
			{
				XPathQueryGenerator.<GetDataMembers>d__6 <GetDataMembers>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetDataMembers>d__ = this;
				}
				else
				{
					<GetDataMembers>d__ = new XPathQueryGenerator.<GetDataMembers>d__6(0);
				}
				<GetDataMembers>d__.contract = contract;
				return <GetDataMembers>d__;
			}

			// Token: 0x06000E1B RID: 3611 RVA: 0x000343B3 File Offset: 0x000325B3
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Runtime.Serialization.DataMember>.GetEnumerator();
			}

			// Token: 0x04000653 RID: 1619
			private int <>1__state;

			// Token: 0x04000654 RID: 1620
			private DataMember <>2__current;

			// Token: 0x04000655 RID: 1621
			private int <>l__initialThreadId;

			// Token: 0x04000656 RID: 1622
			private ClassDataContract contract;

			// Token: 0x04000657 RID: 1623
			public ClassDataContract <>3__contract;

			// Token: 0x04000658 RID: 1624
			private IEnumerator<DataMember> <>7__wrap1;

			// Token: 0x04000659 RID: 1625
			private List<DataMember>.Enumerator <>7__wrap2;
		}
	}
}
