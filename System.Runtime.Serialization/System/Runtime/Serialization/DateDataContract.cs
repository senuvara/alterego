﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000104 RID: 260
	internal class DateDataContract : StringDataContract
	{
		// Token: 0x06000DB4 RID: 3508 RVA: 0x0003379A File Offset: 0x0003199A
		internal DateDataContract() : base(DictionaryGlobals.dateLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
