﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x0200011C RID: 284
	internal class AsmxGuidDataContract : GuidDataContract
	{
		// Token: 0x06000DE0 RID: 3552 RVA: 0x00033ADC File Offset: 0x00031CDC
		internal AsmxGuidDataContract() : base(DictionaryGlobals.GuidLocalName, DictionaryGlobals.AsmxTypesNamespace)
		{
		}
	}
}
