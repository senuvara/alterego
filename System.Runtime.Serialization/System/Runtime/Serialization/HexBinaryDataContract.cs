﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000105 RID: 261
	internal class HexBinaryDataContract : StringDataContract
	{
		// Token: 0x06000DB5 RID: 3509 RVA: 0x000337AC File Offset: 0x000319AC
		internal HexBinaryDataContract() : base(DictionaryGlobals.hexBinaryLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
