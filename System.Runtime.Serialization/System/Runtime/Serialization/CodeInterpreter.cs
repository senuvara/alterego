﻿using System;
using System.Reflection;

namespace System.Runtime.Serialization
{
	// Token: 0x0200014B RID: 331
	internal static class CodeInterpreter
	{
		// Token: 0x06001117 RID: 4375 RVA: 0x0003D1E4 File Offset: 0x0003B3E4
		internal static object ConvertValue(object arg, Type source, Type target)
		{
			return CodeInterpreter.InternalConvert(arg, source, target, false);
		}

		// Token: 0x06001118 RID: 4376 RVA: 0x0003D1EF File Offset: 0x0003B3EF
		private static bool CanConvert(TypeCode typeCode)
		{
			return typeCode - TypeCode.Boolean <= 11;
		}

		// Token: 0x06001119 RID: 4377 RVA: 0x0003D1FC File Offset: 0x0003B3FC
		private static object InternalConvert(object arg, Type source, Type target, bool isAddress)
		{
			if (target == source)
			{
				return arg;
			}
			if (target.IsValueType)
			{
				if (source.IsValueType)
				{
					if (!CodeInterpreter.CanConvert(Type.GetTypeCode(target)))
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("An internal error has occurred. No conversion is possible to '{0}' - error generating code for serialization.", new object[]
						{
							DataContract.GetClrTypeFullName(target)
						})));
					}
					return target;
				}
				else
				{
					if (source.IsAssignableFrom(target))
					{
						return arg;
					}
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("An internal error has occurred. '{0}' is not assignable from '{1}' - error generating code for serialization.", new object[]
					{
						DataContract.GetClrTypeFullName(target),
						DataContract.GetClrTypeFullName(source)
					})));
				}
			}
			else
			{
				if (target.IsAssignableFrom(source))
				{
					return arg;
				}
				if (source.IsAssignableFrom(target))
				{
					return arg;
				}
				if (target.IsInterface || source.IsInterface)
				{
					return arg;
				}
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("An internal error has occurred. '{0}' is not assignable from '{1}' - error generating code for serialization.", new object[]
				{
					DataContract.GetClrTypeFullName(target),
					DataContract.GetClrTypeFullName(source)
				})));
			}
		}

		// Token: 0x0600111A RID: 4378 RVA: 0x0003D2E4 File Offset: 0x0003B4E4
		public static object GetMember(MemberInfo memberInfo, object instance)
		{
			PropertyInfo propertyInfo = memberInfo as PropertyInfo;
			if (propertyInfo != null)
			{
				return propertyInfo.GetValue(instance);
			}
			return ((FieldInfo)memberInfo).GetValue(instance);
		}

		// Token: 0x0600111B RID: 4379 RVA: 0x0003D318 File Offset: 0x0003B518
		public static void SetMember(MemberInfo memberInfo, object instance, object value)
		{
			PropertyInfo propertyInfo = memberInfo as PropertyInfo;
			if (propertyInfo != null)
			{
				propertyInfo.SetValue(instance, value);
				return;
			}
			((FieldInfo)memberInfo).SetValue(instance, value);
		}
	}
}
