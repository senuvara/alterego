﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000100 RID: 256
	internal class DecimalDataContract : PrimitiveDataContract
	{
		// Token: 0x06000DA3 RID: 3491 RVA: 0x0003366C File Offset: 0x0003186C
		internal DecimalDataContract() : base(typeof(decimal), DictionaryGlobals.DecimalLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x06000DA4 RID: 3492 RVA: 0x00033688 File Offset: 0x00031888
		internal override string WriteMethodName
		{
			get
			{
				return "WriteDecimal";
			}
		}

		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x06000DA5 RID: 3493 RVA: 0x0003368F File Offset: 0x0003188F
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsDecimal";
			}
		}

		// Token: 0x06000DA6 RID: 3494 RVA: 0x00033696 File Offset: 0x00031896
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteDecimal((decimal)obj);
		}

		// Token: 0x06000DA7 RID: 3495 RVA: 0x000336A4 File Offset: 0x000318A4
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsDecimal(), context);
			}
			return reader.ReadElementContentAsDecimal();
		}
	}
}
