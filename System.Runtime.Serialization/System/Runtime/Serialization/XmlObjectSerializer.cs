﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Diagnostics;
using System.Runtime.Serialization.Diagnostics;
using System.Security;
using System.Text;
using System.Xml;

namespace System.Runtime.Serialization
{
	/// <summary>Provides the base class used to serialize objects as XML streams or documents. This class is abstract.</summary>
	/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
	/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized. </exception>
	// Token: 0x0200013C RID: 316
	public abstract class XmlObjectSerializer
	{
		/// <summary>Writes the start of the object's data as an opening XML element using the specified <see cref="T:System.Xml.XmlDictionaryWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlDictionaryWriter" /> used to write the XML document.</param>
		/// <param name="graph">The object to serialize.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of objects to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000ECC RID: 3788
		public abstract void WriteStartObject(XmlDictionaryWriter writer, object graph);

		/// <summary>Writes only the content of the object to the XML document or stream using the specified <see cref="T:System.Xml.XmlDictionaryWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlDictionaryWriter" /> used to write the XML document or stream.</param>
		/// <param name="graph">The object that contains the content to write.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of objects to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000ECD RID: 3789
		public abstract void WriteObjectContent(XmlDictionaryWriter writer, object graph);

		/// <summary>Writes the end of the object data as a closing XML element to the XML document or stream with an <see cref="T:System.Xml.XmlDictionaryWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlDictionaryWriter" /> used to write the XML document or stream.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of objects to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000ECE RID: 3790
		public abstract void WriteEndObject(XmlDictionaryWriter writer);

		/// <summary>Writes the complete content (start, content, and end) of the object to the XML document or stream with the specified <see cref="T:System.IO.Stream" />.</summary>
		/// <param name="stream">A <see cref="T:System.IO.Stream" /> used to write the XML document or stream.</param>
		/// <param name="graph">The object that contains the data to write to the stream.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of objects to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000ECF RID: 3791 RVA: 0x00035DC8 File Offset: 0x00033FC8
		public virtual void WriteObject(Stream stream, object graph)
		{
			XmlObjectSerializer.CheckNull(stream, "stream");
			XmlDictionaryWriter xmlDictionaryWriter = XmlDictionaryWriter.CreateTextWriter(stream, Encoding.UTF8, false);
			this.WriteObject(xmlDictionaryWriter, graph);
			xmlDictionaryWriter.Flush();
		}

		/// <summary>Writes the complete content (start, content, and end) of the object to the XML document or stream with the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> used to write the XML document or stream.</param>
		/// <param name="graph">The object that contains the content to write.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of objects to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000ED0 RID: 3792 RVA: 0x00035DFB File Offset: 0x00033FFB
		public virtual void WriteObject(XmlWriter writer, object graph)
		{
			XmlObjectSerializer.CheckNull(writer, "writer");
			this.WriteObject(XmlDictionaryWriter.CreateDictionaryWriter(writer), graph);
		}

		/// <summary>Writes the start of the object's data as an opening XML element using the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> used to write the XML document.</param>
		/// <param name="graph">The object to serialize.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of objects to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000ED1 RID: 3793 RVA: 0x00035E15 File Offset: 0x00034015
		public virtual void WriteStartObject(XmlWriter writer, object graph)
		{
			XmlObjectSerializer.CheckNull(writer, "writer");
			this.WriteStartObject(XmlDictionaryWriter.CreateDictionaryWriter(writer), graph);
		}

		/// <summary>Writes only the content of the object to the XML document or stream with the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> used to write the XML document or stream.</param>
		/// <param name="graph">The object that contains the content to write.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of objects to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000ED2 RID: 3794 RVA: 0x00035E2F File Offset: 0x0003402F
		public virtual void WriteObjectContent(XmlWriter writer, object graph)
		{
			XmlObjectSerializer.CheckNull(writer, "writer");
			this.WriteObjectContent(XmlDictionaryWriter.CreateDictionaryWriter(writer), graph);
		}

		/// <summary>Writes the end of the object data as a closing XML element to the XML document or stream with an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> used to write the XML document or stream.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of objects to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000ED3 RID: 3795 RVA: 0x00035E49 File Offset: 0x00034049
		public virtual void WriteEndObject(XmlWriter writer)
		{
			XmlObjectSerializer.CheckNull(writer, "writer");
			this.WriteEndObject(XmlDictionaryWriter.CreateDictionaryWriter(writer));
		}

		/// <summary>Writes the complete content (start, content, and end) of the object to the XML document or stream with the specified <see cref="T:System.Xml.XmlDictionaryWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlDictionaryWriter" /> used to write the content to the XML document or stream.</param>
		/// <param name="graph">The object that contains the content to write.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of objects to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000ED4 RID: 3796 RVA: 0x0002CB1A File Offset: 0x0002AD1A
		public virtual void WriteObject(XmlDictionaryWriter writer, object graph)
		{
			this.WriteObjectHandleExceptions(new XmlWriterDelegator(writer), graph);
		}

		// Token: 0x06000ED5 RID: 3797 RVA: 0x00035E62 File Offset: 0x00034062
		internal void WriteObjectHandleExceptions(XmlWriterDelegator writer, object graph)
		{
			this.WriteObjectHandleExceptions(writer, graph, null);
		}

		// Token: 0x06000ED6 RID: 3798 RVA: 0x00035E70 File Offset: 0x00034070
		internal void WriteObjectHandleExceptions(XmlWriterDelegator writer, object graph, DataContractResolver dataContractResolver)
		{
			try
			{
				XmlObjectSerializer.CheckNull(writer, "writer");
				if (DiagnosticUtility.ShouldTraceInformation)
				{
					TraceUtility.Trace(TraceEventType.Information, 196609, SR.GetString("WriteObject begins"), new StringTraceRecord("Type", XmlObjectSerializer.GetTypeInfo(this.GetSerializeType(graph))));
					this.InternalWriteObject(writer, graph, dataContractResolver);
					TraceUtility.Trace(TraceEventType.Information, 196610, SR.GetString("WriteObject ends"), new StringTraceRecord("Type", XmlObjectSerializer.GetTypeInfo(this.GetSerializeType(graph))));
				}
				else
				{
					this.InternalWriteObject(writer, graph, dataContractResolver);
				}
			}
			catch (XmlException innerException)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error serializing the object {0}. {1}", this.GetSerializeType(graph), innerException), innerException));
			}
			catch (FormatException innerException2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error serializing the object {0}. {1}", this.GetSerializeType(graph), innerException2), innerException2));
			}
		}

		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06000ED7 RID: 3799 RVA: 0x0001BF2F File Offset: 0x0001A12F
		internal virtual Dictionary<XmlQualifiedName, DataContract> KnownDataContracts
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06000ED8 RID: 3800 RVA: 0x00035F58 File Offset: 0x00034158
		internal virtual void InternalWriteObject(XmlWriterDelegator writer, object graph)
		{
			this.WriteStartObject(writer.Writer, graph);
			this.WriteObjectContent(writer.Writer, graph);
			this.WriteEndObject(writer.Writer);
		}

		// Token: 0x06000ED9 RID: 3801 RVA: 0x00035F80 File Offset: 0x00034180
		internal virtual void InternalWriteObject(XmlWriterDelegator writer, object graph, DataContractResolver dataContractResolver)
		{
			this.InternalWriteObject(writer, graph);
		}

		// Token: 0x06000EDA RID: 3802 RVA: 0x00003129 File Offset: 0x00001329
		internal virtual void InternalWriteStartObject(XmlWriterDelegator writer, object graph)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
		}

		// Token: 0x06000EDB RID: 3803 RVA: 0x00003129 File Offset: 0x00001329
		internal virtual void InternalWriteObjectContent(XmlWriterDelegator writer, object graph)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
		}

		// Token: 0x06000EDC RID: 3804 RVA: 0x00003129 File Offset: 0x00001329
		internal virtual void InternalWriteEndObject(XmlWriterDelegator writer)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
		}

		// Token: 0x06000EDD RID: 3805 RVA: 0x00035F8C File Offset: 0x0003418C
		internal void WriteStartObjectHandleExceptions(XmlWriterDelegator writer, object graph)
		{
			try
			{
				XmlObjectSerializer.CheckNull(writer, "writer");
				this.InternalWriteStartObject(writer, graph);
			}
			catch (XmlException innerException)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error writing start element of object {0}. {1}", this.GetSerializeType(graph), innerException), innerException));
			}
			catch (FormatException innerException2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error writing start element of object {0}. {1}", this.GetSerializeType(graph), innerException2), innerException2));
			}
		}

		// Token: 0x06000EDE RID: 3806 RVA: 0x00036008 File Offset: 0x00034208
		internal void WriteObjectContentHandleExceptions(XmlWriterDelegator writer, object graph)
		{
			try
			{
				XmlObjectSerializer.CheckNull(writer, "writer");
				if (DiagnosticUtility.ShouldTraceInformation)
				{
					TraceUtility.Trace(TraceEventType.Information, 196611, SR.GetString("WriteObjectContent begins"), new StringTraceRecord("Type", XmlObjectSerializer.GetTypeInfo(this.GetSerializeType(graph))));
					if (writer.WriteState != WriteState.Element)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("WriteState '{0}' not valid. Caller must write start element before serializing in contentOnly mode.", new object[]
						{
							writer.WriteState
						})));
					}
					this.InternalWriteObjectContent(writer, graph);
					TraceUtility.Trace(TraceEventType.Information, 196612, SR.GetString("WriteObjectContent ends"), new StringTraceRecord("Type", XmlObjectSerializer.GetTypeInfo(this.GetSerializeType(graph))));
				}
				else
				{
					if (writer.WriteState != WriteState.Element)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("WriteState '{0}' not valid. Caller must write start element before serializing in contentOnly mode.", new object[]
						{
							writer.WriteState
						})));
					}
					this.InternalWriteObjectContent(writer, graph);
				}
			}
			catch (XmlException innerException)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error serializing the object {0}. {1}", this.GetSerializeType(graph), innerException), innerException));
			}
			catch (FormatException innerException2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error serializing the object {0}. {1}", this.GetSerializeType(graph), innerException2), innerException2));
			}
		}

		// Token: 0x06000EDF RID: 3807 RVA: 0x00036154 File Offset: 0x00034354
		internal void WriteEndObjectHandleExceptions(XmlWriterDelegator writer)
		{
			try
			{
				XmlObjectSerializer.CheckNull(writer, "writer");
				this.InternalWriteEndObject(writer);
			}
			catch (XmlException innerException)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error writing end element of object {0}. {1}", null, innerException), innerException));
			}
			catch (FormatException innerException2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error writing end element of object {0}. {1}", null, innerException2), innerException2));
			}
		}

		// Token: 0x06000EE0 RID: 3808 RVA: 0x000361C4 File Offset: 0x000343C4
		internal void WriteRootElement(XmlWriterDelegator writer, DataContract contract, XmlDictionaryString name, XmlDictionaryString ns, bool needsContractNsAtRoot)
		{
			if (name != null)
			{
				contract.WriteRootElement(writer, name, ns);
				if (needsContractNsAtRoot)
				{
					writer.WriteNamespaceDecl(contract.Namespace);
				}
				return;
			}
			if (!contract.HasRoot)
			{
				return;
			}
			contract.WriteRootElement(writer, contract.TopLevelElementName, contract.TopLevelElementNamespace);
		}

		// Token: 0x06000EE1 RID: 3809 RVA: 0x00036200 File Offset: 0x00034400
		internal bool CheckIfNeedsContractNsAtRoot(XmlDictionaryString name, XmlDictionaryString ns, DataContract contract)
		{
			if (name == null)
			{
				return false;
			}
			if (contract.IsBuiltInDataContract || !contract.CanContainReferences || contract.IsISerializable)
			{
				return false;
			}
			string @string = XmlDictionaryString.GetString(contract.Namespace);
			return !string.IsNullOrEmpty(@string) && !(@string == XmlDictionaryString.GetString(ns));
		}

		// Token: 0x06000EE2 RID: 3810 RVA: 0x00036251 File Offset: 0x00034451
		internal static void WriteNull(XmlWriterDelegator writer)
		{
			writer.WriteAttributeBool("i", DictionaryGlobals.XsiNilLocalName, DictionaryGlobals.SchemaInstanceNamespace, true);
		}

		// Token: 0x06000EE3 RID: 3811 RVA: 0x0003626C File Offset: 0x0003446C
		internal static bool IsContractDeclared(DataContract contract, DataContract declaredContract)
		{
			return (contract.Name == declaredContract.Name && contract.Namespace == declaredContract.Namespace) || (contract.Name.Value == declaredContract.Name.Value && contract.Namespace.Value == declaredContract.Namespace.Value);
		}

		/// <summary>Reads the XML stream or document with a <see cref="T:System.IO.Stream" /> and returns the deserialized object.</summary>
		/// <param name="stream">A <see cref="T:System.IO.Stream" /> used to read the XML stream or document.</param>
		/// <returns>The deserialized object.</returns>
		// Token: 0x06000EE4 RID: 3812 RVA: 0x000362D1 File Offset: 0x000344D1
		public virtual object ReadObject(Stream stream)
		{
			XmlObjectSerializer.CheckNull(stream, "stream");
			return this.ReadObject(XmlDictionaryReader.CreateTextReader(stream, XmlDictionaryReaderQuotas.Max));
		}

		/// <summary>Reads the XML document or stream with an <see cref="T:System.Xml.XmlReader" /> and returns the deserialized object.</summary>
		/// <param name="reader">An <see cref="T:System.Xml.XmlReader" /> used to read the XML stream or document.</param>
		/// <returns>The deserialized object.</returns>
		// Token: 0x06000EE5 RID: 3813 RVA: 0x000362EF File Offset: 0x000344EF
		public virtual object ReadObject(XmlReader reader)
		{
			XmlObjectSerializer.CheckNull(reader, "reader");
			return this.ReadObject(XmlDictionaryReader.CreateDictionaryReader(reader));
		}

		/// <summary>Reads the XML document or stream with an <see cref="T:System.Xml.XmlDictionaryReader" /> and returns the deserialized object.</summary>
		/// <param name="reader">An <see cref="T:System.Xml.XmlDictionaryReader" /> used to read the XML document.</param>
		/// <returns>The deserialized object.</returns>
		// Token: 0x06000EE6 RID: 3814 RVA: 0x0002CB65 File Offset: 0x0002AD65
		public virtual object ReadObject(XmlDictionaryReader reader)
		{
			return this.ReadObjectHandleExceptions(new XmlReaderDelegator(reader), true);
		}

		/// <summary>Reads the XML document or stream with an <see cref="T:System.Xml.XmlReader" /> and returns the deserialized object; it also enables you to specify whether the serializer can read the data before attempting to read it.</summary>
		/// <param name="reader">An <see cref="T:System.Xml.XmlReader" /> used to read the XML document or stream.</param>
		/// <param name="verifyObjectName">
		///       <see langword="true" /> to check whether the enclosing XML element name and namespace correspond to the root name and root namespace; <see langword="false" /> to skip the verification.</param>
		/// <returns>The deserialized object.</returns>
		// Token: 0x06000EE7 RID: 3815 RVA: 0x00036308 File Offset: 0x00034508
		public virtual object ReadObject(XmlReader reader, bool verifyObjectName)
		{
			XmlObjectSerializer.CheckNull(reader, "reader");
			return this.ReadObject(XmlDictionaryReader.CreateDictionaryReader(reader), verifyObjectName);
		}

		/// <summary>Reads the XML stream or document with an <see cref="T:System.Xml.XmlDictionaryReader" /> and returns the deserialized object; it also enables you to specify whether the serializer can read the data before attempting to read it.</summary>
		/// <param name="reader">An <see cref="T:System.Xml.XmlDictionaryReader" /> used to read the XML document.</param>
		/// <param name="verifyObjectName">
		///       <see langword="true" /> to check whether the enclosing XML element name and namespace correspond to the root name and root namespace; otherwise, <see langword="false" /> to skip the verification.</param>
		/// <returns>The deserialized object.</returns>
		// Token: 0x06000EE8 RID: 3816
		public abstract object ReadObject(XmlDictionaryReader reader, bool verifyObjectName);

		/// <summary>Gets a value that specifies whether the <see cref="T:System.Xml.XmlReader" /> is positioned over an XML element that can be read. </summary>
		/// <param name="reader">An <see cref="T:System.Xml.XmlReader" /> used to read the XML stream or document.</param>
		/// <returns>
		///     <see langword="true" /> if the reader is positioned over the starting element; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000EE9 RID: 3817 RVA: 0x00036322 File Offset: 0x00034522
		public virtual bool IsStartObject(XmlReader reader)
		{
			XmlObjectSerializer.CheckNull(reader, "reader");
			return this.IsStartObject(XmlDictionaryReader.CreateDictionaryReader(reader));
		}

		/// <summary>Gets a value that specifies whether the <see cref="T:System.Xml.XmlDictionaryReader" /> is positioned over an XML element that can be read.</summary>
		/// <param name="reader">An <see cref="T:System.Xml.XmlDictionaryReader" /> used to read the XML stream or document.</param>
		/// <returns>
		///     <see langword="true" /> if the reader can read the data; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000EEA RID: 3818
		public abstract bool IsStartObject(XmlDictionaryReader reader);

		// Token: 0x06000EEB RID: 3819 RVA: 0x0003633B File Offset: 0x0003453B
		internal virtual object InternalReadObject(XmlReaderDelegator reader, bool verifyObjectName)
		{
			return this.ReadObject(reader.UnderlyingReader, verifyObjectName);
		}

		// Token: 0x06000EEC RID: 3820 RVA: 0x0003634A File Offset: 0x0003454A
		internal virtual object InternalReadObject(XmlReaderDelegator reader, bool verifyObjectName, DataContractResolver dataContractResolver)
		{
			return this.InternalReadObject(reader, verifyObjectName);
		}

		// Token: 0x06000EED RID: 3821 RVA: 0x00003129 File Offset: 0x00001329
		internal virtual bool InternalIsStartObject(XmlReaderDelegator reader)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
		}

		// Token: 0x06000EEE RID: 3822 RVA: 0x00036354 File Offset: 0x00034554
		internal object ReadObjectHandleExceptions(XmlReaderDelegator reader, bool verifyObjectName)
		{
			return this.ReadObjectHandleExceptions(reader, verifyObjectName, null);
		}

		// Token: 0x06000EEF RID: 3823 RVA: 0x00036360 File Offset: 0x00034560
		internal object ReadObjectHandleExceptions(XmlReaderDelegator reader, bool verifyObjectName, DataContractResolver dataContractResolver)
		{
			object result;
			try
			{
				XmlObjectSerializer.CheckNull(reader, "reader");
				if (DiagnosticUtility.ShouldTraceInformation)
				{
					TraceUtility.Trace(TraceEventType.Information, 196613, SR.GetString("ReadObject begins"), new StringTraceRecord("Type", XmlObjectSerializer.GetTypeInfo(this.GetDeserializeType())));
					object obj = this.InternalReadObject(reader, verifyObjectName, dataContractResolver);
					TraceUtility.Trace(TraceEventType.Information, 196614, SR.GetString("ReadObject ends"), new StringTraceRecord("Type", XmlObjectSerializer.GetTypeInfo(this.GetDeserializeType())));
					result = obj;
				}
				else
				{
					result = this.InternalReadObject(reader, verifyObjectName, dataContractResolver);
				}
			}
			catch (XmlException innerException)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error deserializing the object {0}. {1}", this.GetDeserializeType(), innerException), innerException));
			}
			catch (FormatException innerException2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error deserializing the object {0}. {1}", this.GetDeserializeType(), innerException2), innerException2));
			}
			return result;
		}

		// Token: 0x06000EF0 RID: 3824 RVA: 0x00036444 File Offset: 0x00034644
		internal bool IsStartObjectHandleExceptions(XmlReaderDelegator reader)
		{
			bool result;
			try
			{
				XmlObjectSerializer.CheckNull(reader, "reader");
				result = this.InternalIsStartObject(reader);
			}
			catch (XmlException innerException)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error checking start element of object {0}. {1}", this.GetDeserializeType(), innerException), innerException));
			}
			catch (FormatException innerException2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.GetTypeInfoError("There was an error checking start element of object {0}. {1}", this.GetDeserializeType(), innerException2), innerException2));
			}
			return result;
		}

		// Token: 0x06000EF1 RID: 3825 RVA: 0x000364C0 File Offset: 0x000346C0
		internal bool IsRootXmlAny(XmlDictionaryString rootName, DataContract contract)
		{
			return rootName == null && !contract.HasRoot;
		}

		// Token: 0x06000EF2 RID: 3826 RVA: 0x000364D0 File Offset: 0x000346D0
		internal bool IsStartElement(XmlReaderDelegator reader)
		{
			return reader.MoveToElement() || reader.IsStartElement();
		}

		// Token: 0x06000EF3 RID: 3827 RVA: 0x000364E4 File Offset: 0x000346E4
		internal bool IsRootElement(XmlReaderDelegator reader, DataContract contract, XmlDictionaryString name, XmlDictionaryString ns)
		{
			reader.MoveToElement();
			if (name != null)
			{
				return reader.IsStartElement(name, ns);
			}
			if (!contract.HasRoot)
			{
				return reader.IsStartElement();
			}
			if (reader.IsStartElement(contract.TopLevelElementName, contract.TopLevelElementNamespace))
			{
				return true;
			}
			ClassDataContract classDataContract = contract as ClassDataContract;
			if (classDataContract != null)
			{
				classDataContract = classDataContract.BaseContract;
			}
			while (classDataContract != null)
			{
				if (reader.IsStartElement(classDataContract.TopLevelElementName, classDataContract.TopLevelElementNamespace))
				{
					return true;
				}
				classDataContract = classDataContract.BaseContract;
			}
			if (classDataContract == null)
			{
				DataContract primitiveDataContract = PrimitiveDataContract.GetPrimitiveDataContract(Globals.TypeOfObject);
				if (reader.IsStartElement(primitiveDataContract.TopLevelElementName, primitiveDataContract.TopLevelElementNamespace))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000EF4 RID: 3828 RVA: 0x00036582 File Offset: 0x00034782
		internal static void CheckNull(object obj, string name)
		{
			if (obj == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentNullException(name));
			}
		}

		// Token: 0x06000EF5 RID: 3829 RVA: 0x00036594 File Offset: 0x00034794
		internal static string TryAddLineInfo(XmlReaderDelegator reader, string errorMessage)
		{
			if (reader.HasLineInfo())
			{
				return string.Format(CultureInfo.InvariantCulture, "{0} {1}", SR.GetString("Error in line {0} position {1}.", new object[]
				{
					reader.LineNumber,
					reader.LinePosition
				}), errorMessage);
			}
			return errorMessage;
		}

		// Token: 0x06000EF6 RID: 3830 RVA: 0x000365E8 File Offset: 0x000347E8
		internal static Exception CreateSerializationExceptionWithReaderDetails(string errorMessage, XmlReaderDelegator reader)
		{
			return XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.TryAddLineInfo(reader, SR.GetString("{0}. Encountered '{1}'  with name '{2}', namespace '{3}'.", new object[]
			{
				errorMessage,
				reader.NodeType,
				reader.LocalName,
				reader.NamespaceURI
			})));
		}

		// Token: 0x06000EF7 RID: 3831 RVA: 0x00036634 File Offset: 0x00034834
		internal static SerializationException CreateSerializationException(string errorMessage)
		{
			return XmlObjectSerializer.CreateSerializationException(errorMessage, null);
		}

		// Token: 0x06000EF8 RID: 3832 RVA: 0x0003663D File Offset: 0x0003483D
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal static SerializationException CreateSerializationException(string errorMessage, Exception innerException)
		{
			return new SerializationException(errorMessage, innerException);
		}

		// Token: 0x06000EF9 RID: 3833 RVA: 0x00036646 File Offset: 0x00034846
		private static string GetTypeInfo(Type type)
		{
			if (!(type == null))
			{
				return DataContract.GetClrTypeFullName(type);
			}
			return string.Empty;
		}

		// Token: 0x06000EFA RID: 3834 RVA: 0x00036660 File Offset: 0x00034860
		private static string GetTypeInfoError(string errorMessage, Type type, Exception innerException)
		{
			string text = (type == null) ? string.Empty : SR.GetString("of type {0}", new object[]
			{
				DataContract.GetClrTypeFullName(type)
			});
			string text2 = (innerException == null) ? string.Empty : innerException.Message;
			return SR.GetString(errorMessage, new object[]
			{
				text,
				text2
			});
		}

		// Token: 0x06000EFB RID: 3835 RVA: 0x000366BC File Offset: 0x000348BC
		internal virtual Type GetSerializeType(object graph)
		{
			if (graph != null)
			{
				return graph.GetType();
			}
			return null;
		}

		// Token: 0x06000EFC RID: 3836 RVA: 0x0001BF2F File Offset: 0x0001A12F
		internal virtual Type GetDeserializeType()
		{
			return null;
		}

		// Token: 0x1700036C RID: 876
		// (get) Token: 0x06000EFD RID: 3837 RVA: 0x000366C9 File Offset: 0x000348C9
		internal static IFormatterConverter FormatterConverter
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlObjectSerializer.formatterConverter == null)
				{
					XmlObjectSerializer.formatterConverter = new FormatterConverter();
				}
				return XmlObjectSerializer.formatterConverter;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.XmlObjectSerializer" /> class.  </summary>
		// Token: 0x06000EFE RID: 3838 RVA: 0x00002217 File Offset: 0x00000417
		protected XmlObjectSerializer()
		{
		}

		// Token: 0x040006BB RID: 1723
		[SecurityCritical]
		private static IFormatterConverter formatterConverter;
	}
}
