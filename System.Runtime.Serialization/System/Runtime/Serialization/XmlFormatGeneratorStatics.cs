﻿using System;
using System.Collections;
using System.Reflection;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x0200012D RID: 301
	internal static class XmlFormatGeneratorStatics
	{
		// Token: 0x1700031B RID: 795
		// (get) Token: 0x06000E4F RID: 3663 RVA: 0x000349C4 File Offset: 0x00032BC4
		internal static MethodInfo WriteStartElementMethod2
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.writeStartElementMethod2 == null)
				{
					XmlFormatGeneratorStatics.writeStartElementMethod2 = typeof(XmlWriterDelegator).GetMethod("WriteStartElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlDictionaryString),
						typeof(XmlDictionaryString)
					}, null);
				}
				return XmlFormatGeneratorStatics.writeStartElementMethod2;
			}
		}

		// Token: 0x1700031C RID: 796
		// (get) Token: 0x06000E50 RID: 3664 RVA: 0x00034A20 File Offset: 0x00032C20
		internal static MethodInfo WriteStartElementMethod3
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.writeStartElementMethod3 == null)
				{
					XmlFormatGeneratorStatics.writeStartElementMethod3 = typeof(XmlWriterDelegator).GetMethod("WriteStartElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(string),
						typeof(XmlDictionaryString),
						typeof(XmlDictionaryString)
					}, null);
				}
				return XmlFormatGeneratorStatics.writeStartElementMethod3;
			}
		}

		// Token: 0x1700031D RID: 797
		// (get) Token: 0x06000E51 RID: 3665 RVA: 0x00034A89 File Offset: 0x00032C89
		internal static MethodInfo WriteEndElementMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.writeEndElementMethod == null)
				{
					XmlFormatGeneratorStatics.writeEndElementMethod = typeof(XmlWriterDelegator).GetMethod("WriteEndElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[0], null);
				}
				return XmlFormatGeneratorStatics.writeEndElementMethod;
			}
		}

		// Token: 0x1700031E RID: 798
		// (get) Token: 0x06000E52 RID: 3666 RVA: 0x00034AC0 File Offset: 0x00032CC0
		internal static MethodInfo WriteNamespaceDeclMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.writeNamespaceDeclMethod == null)
				{
					XmlFormatGeneratorStatics.writeNamespaceDeclMethod = typeof(XmlWriterDelegator).GetMethod("WriteNamespaceDecl", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlDictionaryString)
					}, null);
				}
				return XmlFormatGeneratorStatics.writeNamespaceDeclMethod;
			}
		}

		// Token: 0x1700031F RID: 799
		// (get) Token: 0x06000E53 RID: 3667 RVA: 0x00034B0F File Offset: 0x00032D0F
		internal static PropertyInfo ExtensionDataProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.extensionDataProperty == null)
				{
					XmlFormatGeneratorStatics.extensionDataProperty = typeof(IExtensibleDataObject).GetProperty("ExtensionData");
				}
				return XmlFormatGeneratorStatics.extensionDataProperty;
			}
		}

		// Token: 0x17000320 RID: 800
		// (get) Token: 0x06000E54 RID: 3668 RVA: 0x00034B3C File Offset: 0x00032D3C
		internal static MethodInfo BoxPointer
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.boxPointer == null)
				{
					XmlFormatGeneratorStatics.boxPointer = typeof(Pointer).GetMethod("Box");
				}
				return XmlFormatGeneratorStatics.boxPointer;
			}
		}

		// Token: 0x17000321 RID: 801
		// (get) Token: 0x06000E55 RID: 3669 RVA: 0x00034B69 File Offset: 0x00032D69
		internal static ConstructorInfo DictionaryEnumeratorCtor
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.dictionaryEnumeratorCtor == null)
				{
					XmlFormatGeneratorStatics.dictionaryEnumeratorCtor = Globals.TypeOfDictionaryEnumerator.GetConstructor(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						Globals.TypeOfIDictionaryEnumerator
					}, null);
				}
				return XmlFormatGeneratorStatics.dictionaryEnumeratorCtor;
			}
		}

		// Token: 0x17000322 RID: 802
		// (get) Token: 0x06000E56 RID: 3670 RVA: 0x00034B9E File Offset: 0x00032D9E
		internal static MethodInfo MoveNextMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.ienumeratorMoveNextMethod == null)
				{
					XmlFormatGeneratorStatics.ienumeratorMoveNextMethod = typeof(IEnumerator).GetMethod("MoveNext");
				}
				return XmlFormatGeneratorStatics.ienumeratorMoveNextMethod;
			}
		}

		// Token: 0x17000323 RID: 803
		// (get) Token: 0x06000E57 RID: 3671 RVA: 0x00034BCB File Offset: 0x00032DCB
		internal static MethodInfo GetCurrentMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.ienumeratorGetCurrentMethod == null)
				{
					XmlFormatGeneratorStatics.ienumeratorGetCurrentMethod = typeof(IEnumerator).GetProperty("Current").GetGetMethod();
				}
				return XmlFormatGeneratorStatics.ienumeratorGetCurrentMethod;
			}
		}

		// Token: 0x17000324 RID: 804
		// (get) Token: 0x06000E58 RID: 3672 RVA: 0x00034BFD File Offset: 0x00032DFD
		internal static MethodInfo GetItemContractMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getItemContractMethod == null)
				{
					XmlFormatGeneratorStatics.getItemContractMethod = typeof(CollectionDataContract).GetProperty("ItemContract", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic).GetGetMethod(true);
				}
				return XmlFormatGeneratorStatics.getItemContractMethod;
			}
		}

		// Token: 0x17000325 RID: 805
		// (get) Token: 0x06000E59 RID: 3673 RVA: 0x00034C34 File Offset: 0x00032E34
		internal static MethodInfo IsStartElementMethod2
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.isStartElementMethod2 == null)
				{
					XmlFormatGeneratorStatics.isStartElementMethod2 = typeof(XmlReaderDelegator).GetMethod("IsStartElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlDictionaryString),
						typeof(XmlDictionaryString)
					}, null);
				}
				return XmlFormatGeneratorStatics.isStartElementMethod2;
			}
		}

		// Token: 0x17000326 RID: 806
		// (get) Token: 0x06000E5A RID: 3674 RVA: 0x00034C90 File Offset: 0x00032E90
		internal static MethodInfo IsStartElementMethod0
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.isStartElementMethod0 == null)
				{
					XmlFormatGeneratorStatics.isStartElementMethod0 = typeof(XmlReaderDelegator).GetMethod("IsStartElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[0], null);
				}
				return XmlFormatGeneratorStatics.isStartElementMethod0;
			}
		}

		// Token: 0x17000327 RID: 807
		// (get) Token: 0x06000E5B RID: 3675 RVA: 0x00034CC8 File Offset: 0x00032EC8
		internal static MethodInfo GetUninitializedObjectMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getUninitializedObjectMethod == null)
				{
					XmlFormatGeneratorStatics.getUninitializedObjectMethod = typeof(XmlFormatReaderGenerator).GetMethod("UnsafeGetUninitializedObject", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(int)
					}, null);
				}
				return XmlFormatGeneratorStatics.getUninitializedObjectMethod;
			}
		}

		// Token: 0x17000328 RID: 808
		// (get) Token: 0x06000E5C RID: 3676 RVA: 0x00034D17 File Offset: 0x00032F17
		internal static MethodInfo OnDeserializationMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.onDeserializationMethod == null)
				{
					XmlFormatGeneratorStatics.onDeserializationMethod = typeof(IDeserializationCallback).GetMethod("OnDeserialization");
				}
				return XmlFormatGeneratorStatics.onDeserializationMethod;
			}
		}

		// Token: 0x17000329 RID: 809
		// (get) Token: 0x06000E5D RID: 3677 RVA: 0x00034D44 File Offset: 0x00032F44
		internal static MethodInfo UnboxPointer
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.unboxPointer == null)
				{
					XmlFormatGeneratorStatics.unboxPointer = typeof(Pointer).GetMethod("Unbox");
				}
				return XmlFormatGeneratorStatics.unboxPointer;
			}
		}

		// Token: 0x1700032A RID: 810
		// (get) Token: 0x06000E5E RID: 3678 RVA: 0x00034D71 File Offset: 0x00032F71
		internal static PropertyInfo NodeTypeProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.nodeTypeProperty == null)
				{
					XmlFormatGeneratorStatics.nodeTypeProperty = typeof(XmlReaderDelegator).GetProperty("NodeType", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.nodeTypeProperty;
			}
		}

		// Token: 0x1700032B RID: 811
		// (get) Token: 0x06000E5F RID: 3679 RVA: 0x00034DA0 File Offset: 0x00032FA0
		internal static ConstructorInfo SerializationExceptionCtor
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.serializationExceptionCtor == null)
				{
					XmlFormatGeneratorStatics.serializationExceptionCtor = typeof(SerializationException).GetConstructor(new Type[]
					{
						typeof(string)
					});
				}
				return XmlFormatGeneratorStatics.serializationExceptionCtor;
			}
		}

		// Token: 0x1700032C RID: 812
		// (get) Token: 0x06000E60 RID: 3680 RVA: 0x00034DDB File Offset: 0x00032FDB
		internal static ConstructorInfo ExtensionDataObjectCtor
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.extensionDataObjectCtor == null)
				{
					XmlFormatGeneratorStatics.extensionDataObjectCtor = typeof(ExtensionDataObject).GetConstructor(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[0], null);
				}
				return XmlFormatGeneratorStatics.extensionDataObjectCtor;
			}
		}

		// Token: 0x1700032D RID: 813
		// (get) Token: 0x06000E61 RID: 3681 RVA: 0x00034E0D File Offset: 0x0003300D
		internal static ConstructorInfo HashtableCtor
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.hashtableCtor == null)
				{
					XmlFormatGeneratorStatics.hashtableCtor = Globals.TypeOfHashtable.GetConstructor(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, Globals.EmptyTypeArray, null);
				}
				return XmlFormatGeneratorStatics.hashtableCtor;
			}
		}

		// Token: 0x1700032E RID: 814
		// (get) Token: 0x06000E62 RID: 3682 RVA: 0x00034E39 File Offset: 0x00033039
		internal static MethodInfo GetStreamingContextMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getStreamingContextMethod == null)
				{
					XmlFormatGeneratorStatics.getStreamingContextMethod = typeof(XmlObjectSerializerContext).GetMethod("GetStreamingContext", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getStreamingContextMethod;
			}
		}

		// Token: 0x1700032F RID: 815
		// (get) Token: 0x06000E63 RID: 3683 RVA: 0x00034E68 File Offset: 0x00033068
		internal static MethodInfo GetCollectionMemberMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getCollectionMemberMethod == null)
				{
					XmlFormatGeneratorStatics.getCollectionMemberMethod = typeof(XmlObjectSerializerReadContext).GetMethod("GetCollectionMember", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getCollectionMemberMethod;
			}
		}

		// Token: 0x17000330 RID: 816
		// (get) Token: 0x06000E64 RID: 3684 RVA: 0x00034E98 File Offset: 0x00033098
		internal static MethodInfo StoreCollectionMemberInfoMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.storeCollectionMemberInfoMethod == null)
				{
					XmlFormatGeneratorStatics.storeCollectionMemberInfoMethod = typeof(XmlObjectSerializerReadContext).GetMethod("StoreCollectionMemberInfo", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(object)
					}, null);
				}
				return XmlFormatGeneratorStatics.storeCollectionMemberInfoMethod;
			}
		}

		// Token: 0x17000331 RID: 817
		// (get) Token: 0x06000E65 RID: 3685 RVA: 0x00034EE7 File Offset: 0x000330E7
		internal static MethodInfo StoreIsGetOnlyCollectionMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.storeIsGetOnlyCollectionMethod == null)
				{
					XmlFormatGeneratorStatics.storeIsGetOnlyCollectionMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("StoreIsGetOnlyCollection", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.storeIsGetOnlyCollectionMethod;
			}
		}

		// Token: 0x17000332 RID: 818
		// (get) Token: 0x06000E66 RID: 3686 RVA: 0x00034F16 File Offset: 0x00033116
		internal static MethodInfo ThrowNullValueReturnedForGetOnlyCollectionExceptionMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.throwNullValueReturnedForGetOnlyCollectionExceptionMethod == null)
				{
					XmlFormatGeneratorStatics.throwNullValueReturnedForGetOnlyCollectionExceptionMethod = typeof(XmlObjectSerializerReadContext).GetMethod("ThrowNullValueReturnedForGetOnlyCollectionException", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.throwNullValueReturnedForGetOnlyCollectionExceptionMethod;
			}
		}

		// Token: 0x17000333 RID: 819
		// (get) Token: 0x06000E67 RID: 3687 RVA: 0x00034F45 File Offset: 0x00033145
		internal static MethodInfo ThrowArrayExceededSizeExceptionMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.throwArrayExceededSizeExceptionMethod == null)
				{
					XmlFormatGeneratorStatics.throwArrayExceededSizeExceptionMethod = typeof(XmlObjectSerializerReadContext).GetMethod("ThrowArrayExceededSizeException", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.throwArrayExceededSizeExceptionMethod;
			}
		}

		// Token: 0x17000334 RID: 820
		// (get) Token: 0x06000E68 RID: 3688 RVA: 0x00034F74 File Offset: 0x00033174
		internal static MethodInfo IncrementItemCountMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.incrementItemCountMethod == null)
				{
					XmlFormatGeneratorStatics.incrementItemCountMethod = typeof(XmlObjectSerializerContext).GetMethod("IncrementItemCount", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.incrementItemCountMethod;
			}
		}

		// Token: 0x17000335 RID: 821
		// (get) Token: 0x06000E69 RID: 3689 RVA: 0x00034FA3 File Offset: 0x000331A3
		internal static MethodInfo DemandSerializationFormatterPermissionMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.demandSerializationFormatterPermissionMethod == null)
				{
					XmlFormatGeneratorStatics.demandSerializationFormatterPermissionMethod = typeof(XmlObjectSerializerContext).GetMethod("DemandSerializationFormatterPermission", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.demandSerializationFormatterPermissionMethod;
			}
		}

		// Token: 0x17000336 RID: 822
		// (get) Token: 0x06000E6A RID: 3690 RVA: 0x00034FD2 File Offset: 0x000331D2
		internal static MethodInfo DemandMemberAccessPermissionMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.demandMemberAccessPermissionMethod == null)
				{
					XmlFormatGeneratorStatics.demandMemberAccessPermissionMethod = typeof(XmlObjectSerializerContext).GetMethod("DemandMemberAccessPermission", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.demandMemberAccessPermissionMethod;
			}
		}

		// Token: 0x17000337 RID: 823
		// (get) Token: 0x06000E6B RID: 3691 RVA: 0x00035004 File Offset: 0x00033204
		internal static MethodInfo InternalDeserializeMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.internalDeserializeMethod == null)
				{
					XmlFormatGeneratorStatics.internalDeserializeMethod = typeof(XmlObjectSerializerReadContext).GetMethod("InternalDeserialize", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlReaderDelegator),
						typeof(int),
						typeof(RuntimeTypeHandle),
						typeof(string),
						typeof(string)
					}, null);
				}
				return XmlFormatGeneratorStatics.internalDeserializeMethod;
			}
		}

		// Token: 0x17000338 RID: 824
		// (get) Token: 0x06000E6C RID: 3692 RVA: 0x00035087 File Offset: 0x00033287
		internal static MethodInfo MoveToNextElementMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.moveToNextElementMethod == null)
				{
					XmlFormatGeneratorStatics.moveToNextElementMethod = typeof(XmlObjectSerializerReadContext).GetMethod("MoveToNextElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.moveToNextElementMethod;
			}
		}

		// Token: 0x17000339 RID: 825
		// (get) Token: 0x06000E6D RID: 3693 RVA: 0x000350B6 File Offset: 0x000332B6
		internal static MethodInfo GetMemberIndexMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getMemberIndexMethod == null)
				{
					XmlFormatGeneratorStatics.getMemberIndexMethod = typeof(XmlObjectSerializerReadContext).GetMethod("GetMemberIndex", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getMemberIndexMethod;
			}
		}

		// Token: 0x1700033A RID: 826
		// (get) Token: 0x06000E6E RID: 3694 RVA: 0x000350E5 File Offset: 0x000332E5
		internal static MethodInfo GetMemberIndexWithRequiredMembersMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getMemberIndexWithRequiredMembersMethod == null)
				{
					XmlFormatGeneratorStatics.getMemberIndexWithRequiredMembersMethod = typeof(XmlObjectSerializerReadContext).GetMethod("GetMemberIndexWithRequiredMembers", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getMemberIndexWithRequiredMembersMethod;
			}
		}

		// Token: 0x1700033B RID: 827
		// (get) Token: 0x06000E6F RID: 3695 RVA: 0x00035114 File Offset: 0x00033314
		internal static MethodInfo ThrowRequiredMemberMissingExceptionMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.throwRequiredMemberMissingExceptionMethod == null)
				{
					XmlFormatGeneratorStatics.throwRequiredMemberMissingExceptionMethod = typeof(XmlObjectSerializerReadContext).GetMethod("ThrowRequiredMemberMissingException", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.throwRequiredMemberMissingExceptionMethod;
			}
		}

		// Token: 0x1700033C RID: 828
		// (get) Token: 0x06000E70 RID: 3696 RVA: 0x00035143 File Offset: 0x00033343
		internal static MethodInfo SkipUnknownElementMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.skipUnknownElementMethod == null)
				{
					XmlFormatGeneratorStatics.skipUnknownElementMethod = typeof(XmlObjectSerializerReadContext).GetMethod("SkipUnknownElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.skipUnknownElementMethod;
			}
		}

		// Token: 0x1700033D RID: 829
		// (get) Token: 0x06000E71 RID: 3697 RVA: 0x00035174 File Offset: 0x00033374
		internal static MethodInfo ReadIfNullOrRefMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.readIfNullOrRefMethod == null)
				{
					XmlFormatGeneratorStatics.readIfNullOrRefMethod = typeof(XmlObjectSerializerReadContext).GetMethod("ReadIfNullOrRef", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlReaderDelegator),
						typeof(Type),
						typeof(bool)
					}, null);
				}
				return XmlFormatGeneratorStatics.readIfNullOrRefMethod;
			}
		}

		// Token: 0x1700033E RID: 830
		// (get) Token: 0x06000E72 RID: 3698 RVA: 0x000351DD File Offset: 0x000333DD
		internal static MethodInfo ReadAttributesMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.readAttributesMethod == null)
				{
					XmlFormatGeneratorStatics.readAttributesMethod = typeof(XmlObjectSerializerReadContext).GetMethod("ReadAttributes", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.readAttributesMethod;
			}
		}

		// Token: 0x1700033F RID: 831
		// (get) Token: 0x06000E73 RID: 3699 RVA: 0x0003520C File Offset: 0x0003340C
		internal static MethodInfo ResetAttributesMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.resetAttributesMethod == null)
				{
					XmlFormatGeneratorStatics.resetAttributesMethod = typeof(XmlObjectSerializerReadContext).GetMethod("ResetAttributes", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.resetAttributesMethod;
			}
		}

		// Token: 0x17000340 RID: 832
		// (get) Token: 0x06000E74 RID: 3700 RVA: 0x0003523B File Offset: 0x0003343B
		internal static MethodInfo GetObjectIdMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getObjectIdMethod == null)
				{
					XmlFormatGeneratorStatics.getObjectIdMethod = typeof(XmlObjectSerializerReadContext).GetMethod("GetObjectId", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getObjectIdMethod;
			}
		}

		// Token: 0x17000341 RID: 833
		// (get) Token: 0x06000E75 RID: 3701 RVA: 0x0003526A File Offset: 0x0003346A
		internal static MethodInfo GetArraySizeMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getArraySizeMethod == null)
				{
					XmlFormatGeneratorStatics.getArraySizeMethod = typeof(XmlObjectSerializerReadContext).GetMethod("GetArraySize", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getArraySizeMethod;
			}
		}

		// Token: 0x17000342 RID: 834
		// (get) Token: 0x06000E76 RID: 3702 RVA: 0x00035299 File Offset: 0x00033499
		internal static MethodInfo AddNewObjectMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.addNewObjectMethod == null)
				{
					XmlFormatGeneratorStatics.addNewObjectMethod = typeof(XmlObjectSerializerReadContext).GetMethod("AddNewObject", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.addNewObjectMethod;
			}
		}

		// Token: 0x17000343 RID: 835
		// (get) Token: 0x06000E77 RID: 3703 RVA: 0x000352C8 File Offset: 0x000334C8
		internal static MethodInfo AddNewObjectWithIdMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.addNewObjectWithIdMethod == null)
				{
					XmlFormatGeneratorStatics.addNewObjectWithIdMethod = typeof(XmlObjectSerializerReadContext).GetMethod("AddNewObjectWithId", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.addNewObjectWithIdMethod;
			}
		}

		// Token: 0x17000344 RID: 836
		// (get) Token: 0x06000E78 RID: 3704 RVA: 0x000352F7 File Offset: 0x000334F7
		internal static MethodInfo ReplaceDeserializedObjectMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.replaceDeserializedObjectMethod == null)
				{
					XmlFormatGeneratorStatics.replaceDeserializedObjectMethod = typeof(XmlObjectSerializerReadContext).GetMethod("ReplaceDeserializedObject", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.replaceDeserializedObjectMethod;
			}
		}

		// Token: 0x17000345 RID: 837
		// (get) Token: 0x06000E79 RID: 3705 RVA: 0x00035326 File Offset: 0x00033526
		internal static MethodInfo GetExistingObjectMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getExistingObjectMethod == null)
				{
					XmlFormatGeneratorStatics.getExistingObjectMethod = typeof(XmlObjectSerializerReadContext).GetMethod("GetExistingObject", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getExistingObjectMethod;
			}
		}

		// Token: 0x17000346 RID: 838
		// (get) Token: 0x06000E7A RID: 3706 RVA: 0x00035355 File Offset: 0x00033555
		internal static MethodInfo GetRealObjectMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getRealObjectMethod == null)
				{
					XmlFormatGeneratorStatics.getRealObjectMethod = typeof(XmlObjectSerializerReadContext).GetMethod("GetRealObject", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getRealObjectMethod;
			}
		}

		// Token: 0x17000347 RID: 839
		// (get) Token: 0x06000E7B RID: 3707 RVA: 0x00035384 File Offset: 0x00033584
		internal static MethodInfo ReadMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.readMethod == null)
				{
					XmlFormatGeneratorStatics.readMethod = typeof(XmlObjectSerializerReadContext).GetMethod("Read", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.readMethod;
			}
		}

		// Token: 0x17000348 RID: 840
		// (get) Token: 0x06000E7C RID: 3708 RVA: 0x000353B3 File Offset: 0x000335B3
		internal static MethodInfo EnsureArraySizeMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.ensureArraySizeMethod == null)
				{
					XmlFormatGeneratorStatics.ensureArraySizeMethod = typeof(XmlObjectSerializerReadContext).GetMethod("EnsureArraySize", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.ensureArraySizeMethod;
			}
		}

		// Token: 0x17000349 RID: 841
		// (get) Token: 0x06000E7D RID: 3709 RVA: 0x000353E2 File Offset: 0x000335E2
		internal static MethodInfo TrimArraySizeMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.trimArraySizeMethod == null)
				{
					XmlFormatGeneratorStatics.trimArraySizeMethod = typeof(XmlObjectSerializerReadContext).GetMethod("TrimArraySize", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.trimArraySizeMethod;
			}
		}

		// Token: 0x1700034A RID: 842
		// (get) Token: 0x06000E7E RID: 3710 RVA: 0x00035411 File Offset: 0x00033611
		internal static MethodInfo CheckEndOfArrayMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.checkEndOfArrayMethod == null)
				{
					XmlFormatGeneratorStatics.checkEndOfArrayMethod = typeof(XmlObjectSerializerReadContext).GetMethod("CheckEndOfArray", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.checkEndOfArrayMethod;
			}
		}

		// Token: 0x1700034B RID: 843
		// (get) Token: 0x06000E7F RID: 3711 RVA: 0x00035440 File Offset: 0x00033640
		internal static MethodInfo GetArrayLengthMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getArrayLengthMethod == null)
				{
					XmlFormatGeneratorStatics.getArrayLengthMethod = Globals.TypeOfArray.GetProperty("Length").GetGetMethod();
				}
				return XmlFormatGeneratorStatics.getArrayLengthMethod;
			}
		}

		// Token: 0x1700034C RID: 844
		// (get) Token: 0x06000E80 RID: 3712 RVA: 0x0003546D File Offset: 0x0003366D
		internal static MethodInfo ReadSerializationInfoMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.readSerializationInfoMethod == null)
				{
					XmlFormatGeneratorStatics.readSerializationInfoMethod = typeof(XmlObjectSerializerReadContext).GetMethod("ReadSerializationInfo", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.readSerializationInfoMethod;
			}
		}

		// Token: 0x1700034D RID: 845
		// (get) Token: 0x06000E81 RID: 3713 RVA: 0x0003549C File Offset: 0x0003369C
		internal static MethodInfo CreateUnexpectedStateExceptionMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.createUnexpectedStateExceptionMethod == null)
				{
					XmlFormatGeneratorStatics.createUnexpectedStateExceptionMethod = typeof(XmlObjectSerializerReadContext).GetMethod("CreateUnexpectedStateException", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlNodeType),
						typeof(XmlReaderDelegator)
					}, null);
				}
				return XmlFormatGeneratorStatics.createUnexpectedStateExceptionMethod;
			}
		}

		// Token: 0x1700034E RID: 846
		// (get) Token: 0x06000E82 RID: 3714 RVA: 0x000354F8 File Offset: 0x000336F8
		internal static MethodInfo InternalSerializeReferenceMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.internalSerializeReferenceMethod == null)
				{
					XmlFormatGeneratorStatics.internalSerializeReferenceMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("InternalSerializeReference", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.internalSerializeReferenceMethod;
			}
		}

		// Token: 0x1700034F RID: 847
		// (get) Token: 0x06000E83 RID: 3715 RVA: 0x00035527 File Offset: 0x00033727
		internal static MethodInfo InternalSerializeMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.internalSerializeMethod == null)
				{
					XmlFormatGeneratorStatics.internalSerializeMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("InternalSerialize", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.internalSerializeMethod;
			}
		}

		// Token: 0x17000350 RID: 848
		// (get) Token: 0x06000E84 RID: 3716 RVA: 0x00035558 File Offset: 0x00033758
		internal static MethodInfo WriteNullMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.writeNullMethod == null)
				{
					XmlFormatGeneratorStatics.writeNullMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("WriteNull", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlWriterDelegator),
						typeof(Type),
						typeof(bool)
					}, null);
				}
				return XmlFormatGeneratorStatics.writeNullMethod;
			}
		}

		// Token: 0x17000351 RID: 849
		// (get) Token: 0x06000E85 RID: 3717 RVA: 0x000355C1 File Offset: 0x000337C1
		internal static MethodInfo IncrementArrayCountMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.incrementArrayCountMethod == null)
				{
					XmlFormatGeneratorStatics.incrementArrayCountMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("IncrementArrayCount", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.incrementArrayCountMethod;
			}
		}

		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06000E86 RID: 3718 RVA: 0x000355F0 File Offset: 0x000337F0
		internal static MethodInfo IncrementCollectionCountMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.incrementCollectionCountMethod == null)
				{
					XmlFormatGeneratorStatics.incrementCollectionCountMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("IncrementCollectionCount", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlWriterDelegator),
						typeof(ICollection)
					}, null);
				}
				return XmlFormatGeneratorStatics.incrementCollectionCountMethod;
			}
		}

		// Token: 0x17000353 RID: 851
		// (get) Token: 0x06000E87 RID: 3719 RVA: 0x0003564C File Offset: 0x0003384C
		internal static MethodInfo IncrementCollectionCountGenericMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.incrementCollectionCountGenericMethod == null)
				{
					XmlFormatGeneratorStatics.incrementCollectionCountGenericMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("IncrementCollectionCountGeneric", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.incrementCollectionCountGenericMethod;
			}
		}

		// Token: 0x17000354 RID: 852
		// (get) Token: 0x06000E88 RID: 3720 RVA: 0x0003567B File Offset: 0x0003387B
		internal static MethodInfo GetDefaultValueMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getDefaultValueMethod == null)
				{
					XmlFormatGeneratorStatics.getDefaultValueMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("GetDefaultValue", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getDefaultValueMethod;
			}
		}

		// Token: 0x17000355 RID: 853
		// (get) Token: 0x06000E89 RID: 3721 RVA: 0x000356AA File Offset: 0x000338AA
		internal static MethodInfo GetNullableValueMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getNullableValueMethod == null)
				{
					XmlFormatGeneratorStatics.getNullableValueMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("GetNullableValue", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getNullableValueMethod;
			}
		}

		// Token: 0x17000356 RID: 854
		// (get) Token: 0x06000E8A RID: 3722 RVA: 0x000356D9 File Offset: 0x000338D9
		internal static MethodInfo ThrowRequiredMemberMustBeEmittedMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.throwRequiredMemberMustBeEmittedMethod == null)
				{
					XmlFormatGeneratorStatics.throwRequiredMemberMustBeEmittedMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("ThrowRequiredMemberMustBeEmitted", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.throwRequiredMemberMustBeEmittedMethod;
			}
		}

		// Token: 0x17000357 RID: 855
		// (get) Token: 0x06000E8B RID: 3723 RVA: 0x00035708 File Offset: 0x00033908
		internal static MethodInfo GetHasValueMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getHasValueMethod == null)
				{
					XmlFormatGeneratorStatics.getHasValueMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("GetHasValue", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getHasValueMethod;
			}
		}

		// Token: 0x17000358 RID: 856
		// (get) Token: 0x06000E8C RID: 3724 RVA: 0x00035737 File Offset: 0x00033937
		internal static MethodInfo WriteISerializableMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.writeISerializableMethod == null)
				{
					XmlFormatGeneratorStatics.writeISerializableMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("WriteISerializable", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.writeISerializableMethod;
			}
		}

		// Token: 0x17000359 RID: 857
		// (get) Token: 0x06000E8D RID: 3725 RVA: 0x00035766 File Offset: 0x00033966
		internal static MethodInfo WriteExtensionDataMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.writeExtensionDataMethod == null)
				{
					XmlFormatGeneratorStatics.writeExtensionDataMethod = typeof(XmlObjectSerializerWriteContext).GetMethod("WriteExtensionData", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.writeExtensionDataMethod;
			}
		}

		// Token: 0x1700035A RID: 858
		// (get) Token: 0x06000E8E RID: 3726 RVA: 0x00035795 File Offset: 0x00033995
		internal static MethodInfo WriteXmlValueMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.writeXmlValueMethod == null)
				{
					XmlFormatGeneratorStatics.writeXmlValueMethod = typeof(DataContract).GetMethod("WriteXmlValue", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.writeXmlValueMethod;
			}
		}

		// Token: 0x1700035B RID: 859
		// (get) Token: 0x06000E8F RID: 3727 RVA: 0x000357C4 File Offset: 0x000339C4
		internal static MethodInfo ReadXmlValueMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.readXmlValueMethod == null)
				{
					XmlFormatGeneratorStatics.readXmlValueMethod = typeof(DataContract).GetMethod("ReadXmlValue", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.readXmlValueMethod;
			}
		}

		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06000E90 RID: 3728 RVA: 0x000357F3 File Offset: 0x000339F3
		internal static MethodInfo ThrowTypeNotSerializableMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.throwTypeNotSerializableMethod == null)
				{
					XmlFormatGeneratorStatics.throwTypeNotSerializableMethod = typeof(DataContract).GetMethod("ThrowTypeNotSerializable", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.throwTypeNotSerializableMethod;
			}
		}

		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06000E91 RID: 3729 RVA: 0x00035822 File Offset: 0x00033A22
		internal static PropertyInfo NamespaceProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.namespaceProperty == null)
				{
					XmlFormatGeneratorStatics.namespaceProperty = typeof(DataContract).GetProperty("Namespace", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.namespaceProperty;
			}
		}

		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06000E92 RID: 3730 RVA: 0x00035851 File Offset: 0x00033A51
		internal static FieldInfo ContractNamespacesField
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.contractNamespacesField == null)
				{
					XmlFormatGeneratorStatics.contractNamespacesField = typeof(ClassDataContract).GetField("ContractNamespaces", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.contractNamespacesField;
			}
		}

		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06000E93 RID: 3731 RVA: 0x00035880 File Offset: 0x00033A80
		internal static FieldInfo MemberNamesField
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.memberNamesField == null)
				{
					XmlFormatGeneratorStatics.memberNamesField = typeof(ClassDataContract).GetField("MemberNames", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.memberNamesField;
			}
		}

		// Token: 0x17000360 RID: 864
		// (get) Token: 0x06000E94 RID: 3732 RVA: 0x000358AF File Offset: 0x00033AAF
		internal static MethodInfo ExtensionDataSetExplicitMethodInfo
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.extensionDataSetExplicitMethodInfo == null)
				{
					XmlFormatGeneratorStatics.extensionDataSetExplicitMethodInfo = typeof(IExtensibleDataObject).GetMethod("set_ExtensionData");
				}
				return XmlFormatGeneratorStatics.extensionDataSetExplicitMethodInfo;
			}
		}

		// Token: 0x17000361 RID: 865
		// (get) Token: 0x06000E95 RID: 3733 RVA: 0x000358DC File Offset: 0x00033ADC
		internal static PropertyInfo ChildElementNamespacesProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.childElementNamespacesProperty == null)
				{
					XmlFormatGeneratorStatics.childElementNamespacesProperty = typeof(ClassDataContract).GetProperty("ChildElementNamespaces", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.childElementNamespacesProperty;
			}
		}

		// Token: 0x17000362 RID: 866
		// (get) Token: 0x06000E96 RID: 3734 RVA: 0x0003590B File Offset: 0x00033B0B
		internal static PropertyInfo CollectionItemNameProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.collectionItemNameProperty == null)
				{
					XmlFormatGeneratorStatics.collectionItemNameProperty = typeof(CollectionDataContract).GetProperty("CollectionItemName", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.collectionItemNameProperty;
			}
		}

		// Token: 0x17000363 RID: 867
		// (get) Token: 0x06000E97 RID: 3735 RVA: 0x0003593A File Offset: 0x00033B3A
		internal static PropertyInfo ChildElementNamespaceProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.childElementNamespaceProperty == null)
				{
					XmlFormatGeneratorStatics.childElementNamespaceProperty = typeof(CollectionDataContract).GetProperty("ChildElementNamespace", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.childElementNamespaceProperty;
			}
		}

		// Token: 0x17000364 RID: 868
		// (get) Token: 0x06000E98 RID: 3736 RVA: 0x00035969 File Offset: 0x00033B69
		internal static MethodInfo GetDateTimeOffsetMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getDateTimeOffsetMethod == null)
				{
					XmlFormatGeneratorStatics.getDateTimeOffsetMethod = typeof(DateTimeOffsetAdapter).GetMethod("GetDateTimeOffset", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getDateTimeOffsetMethod;
			}
		}

		// Token: 0x17000365 RID: 869
		// (get) Token: 0x06000E99 RID: 3737 RVA: 0x00035998 File Offset: 0x00033B98
		internal static MethodInfo GetDateTimeOffsetAdapterMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.getDateTimeOffsetAdapterMethod == null)
				{
					XmlFormatGeneratorStatics.getDateTimeOffsetAdapterMethod = typeof(DateTimeOffsetAdapter).GetMethod("GetDateTimeOffsetAdapter", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.getDateTimeOffsetAdapterMethod;
			}
		}

		// Token: 0x17000366 RID: 870
		// (get) Token: 0x06000E9A RID: 3738 RVA: 0x000359C7 File Offset: 0x00033BC7
		internal static MethodInfo TraceInstructionMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.traceInstructionMethod == null)
				{
					XmlFormatGeneratorStatics.traceInstructionMethod = typeof(SerializationTrace).GetMethod("TraceInstruction", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.traceInstructionMethod;
			}
		}

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x06000E9B RID: 3739 RVA: 0x000359F8 File Offset: 0x00033BF8
		internal static MethodInfo ThrowInvalidDataContractExceptionMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.throwInvalidDataContractExceptionMethod == null)
				{
					XmlFormatGeneratorStatics.throwInvalidDataContractExceptionMethod = typeof(DataContract).GetMethod("ThrowInvalidDataContractException", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(string),
						typeof(Type)
					}, null);
				}
				return XmlFormatGeneratorStatics.throwInvalidDataContractExceptionMethod;
			}
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x06000E9C RID: 3740 RVA: 0x00035A54 File Offset: 0x00033C54
		internal static PropertyInfo SerializeReadOnlyTypesProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.serializeReadOnlyTypesProperty == null)
				{
					XmlFormatGeneratorStatics.serializeReadOnlyTypesProperty = typeof(XmlObjectSerializerWriteContext).GetProperty("SerializeReadOnlyTypes", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.serializeReadOnlyTypesProperty;
			}
		}

		// Token: 0x17000369 RID: 873
		// (get) Token: 0x06000E9D RID: 3741 RVA: 0x00035A83 File Offset: 0x00033C83
		internal static PropertyInfo ClassSerializationExceptionMessageProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.classSerializationExceptionMessageProperty == null)
				{
					XmlFormatGeneratorStatics.classSerializationExceptionMessageProperty = typeof(ClassDataContract).GetProperty("SerializationExceptionMessage", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.classSerializationExceptionMessageProperty;
			}
		}

		// Token: 0x1700036A RID: 874
		// (get) Token: 0x06000E9E RID: 3742 RVA: 0x00035AB2 File Offset: 0x00033CB2
		internal static PropertyInfo CollectionSerializationExceptionMessageProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlFormatGeneratorStatics.collectionSerializationExceptionMessageProperty == null)
				{
					XmlFormatGeneratorStatics.collectionSerializationExceptionMessageProperty = typeof(CollectionDataContract).GetProperty("SerializationExceptionMessage", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return XmlFormatGeneratorStatics.collectionSerializationExceptionMessageProperty;
			}
		}

		// Token: 0x04000664 RID: 1636
		[SecurityCritical]
		private static MethodInfo writeStartElementMethod2;

		// Token: 0x04000665 RID: 1637
		[SecurityCritical]
		private static MethodInfo writeStartElementMethod3;

		// Token: 0x04000666 RID: 1638
		[SecurityCritical]
		private static MethodInfo writeEndElementMethod;

		// Token: 0x04000667 RID: 1639
		[SecurityCritical]
		private static MethodInfo writeNamespaceDeclMethod;

		// Token: 0x04000668 RID: 1640
		[SecurityCritical]
		private static PropertyInfo extensionDataProperty;

		// Token: 0x04000669 RID: 1641
		[SecurityCritical]
		private static MethodInfo boxPointer;

		// Token: 0x0400066A RID: 1642
		[SecurityCritical]
		private static ConstructorInfo dictionaryEnumeratorCtor;

		// Token: 0x0400066B RID: 1643
		[SecurityCritical]
		private static MethodInfo ienumeratorMoveNextMethod;

		// Token: 0x0400066C RID: 1644
		[SecurityCritical]
		private static MethodInfo ienumeratorGetCurrentMethod;

		// Token: 0x0400066D RID: 1645
		[SecurityCritical]
		private static MethodInfo getItemContractMethod;

		// Token: 0x0400066E RID: 1646
		[SecurityCritical]
		private static MethodInfo isStartElementMethod2;

		// Token: 0x0400066F RID: 1647
		[SecurityCritical]
		private static MethodInfo isStartElementMethod0;

		// Token: 0x04000670 RID: 1648
		[SecurityCritical]
		private static MethodInfo getUninitializedObjectMethod;

		// Token: 0x04000671 RID: 1649
		[SecurityCritical]
		private static MethodInfo onDeserializationMethod;

		// Token: 0x04000672 RID: 1650
		[SecurityCritical]
		private static MethodInfo unboxPointer;

		// Token: 0x04000673 RID: 1651
		[SecurityCritical]
		private static PropertyInfo nodeTypeProperty;

		// Token: 0x04000674 RID: 1652
		[SecurityCritical]
		private static ConstructorInfo serializationExceptionCtor;

		// Token: 0x04000675 RID: 1653
		[SecurityCritical]
		private static ConstructorInfo extensionDataObjectCtor;

		// Token: 0x04000676 RID: 1654
		[SecurityCritical]
		private static ConstructorInfo hashtableCtor;

		// Token: 0x04000677 RID: 1655
		[SecurityCritical]
		private static MethodInfo getStreamingContextMethod;

		// Token: 0x04000678 RID: 1656
		[SecurityCritical]
		private static MethodInfo getCollectionMemberMethod;

		// Token: 0x04000679 RID: 1657
		[SecurityCritical]
		private static MethodInfo storeCollectionMemberInfoMethod;

		// Token: 0x0400067A RID: 1658
		[SecurityCritical]
		private static MethodInfo storeIsGetOnlyCollectionMethod;

		// Token: 0x0400067B RID: 1659
		[SecurityCritical]
		private static MethodInfo throwNullValueReturnedForGetOnlyCollectionExceptionMethod;

		// Token: 0x0400067C RID: 1660
		private static MethodInfo throwArrayExceededSizeExceptionMethod;

		// Token: 0x0400067D RID: 1661
		[SecurityCritical]
		private static MethodInfo incrementItemCountMethod;

		// Token: 0x0400067E RID: 1662
		[SecurityCritical]
		private static MethodInfo demandSerializationFormatterPermissionMethod;

		// Token: 0x0400067F RID: 1663
		[SecurityCritical]
		private static MethodInfo demandMemberAccessPermissionMethod;

		// Token: 0x04000680 RID: 1664
		[SecurityCritical]
		private static MethodInfo internalDeserializeMethod;

		// Token: 0x04000681 RID: 1665
		[SecurityCritical]
		private static MethodInfo moveToNextElementMethod;

		// Token: 0x04000682 RID: 1666
		[SecurityCritical]
		private static MethodInfo getMemberIndexMethod;

		// Token: 0x04000683 RID: 1667
		[SecurityCritical]
		private static MethodInfo getMemberIndexWithRequiredMembersMethod;

		// Token: 0x04000684 RID: 1668
		[SecurityCritical]
		private static MethodInfo throwRequiredMemberMissingExceptionMethod;

		// Token: 0x04000685 RID: 1669
		[SecurityCritical]
		private static MethodInfo skipUnknownElementMethod;

		// Token: 0x04000686 RID: 1670
		[SecurityCritical]
		private static MethodInfo readIfNullOrRefMethod;

		// Token: 0x04000687 RID: 1671
		[SecurityCritical]
		private static MethodInfo readAttributesMethod;

		// Token: 0x04000688 RID: 1672
		[SecurityCritical]
		private static MethodInfo resetAttributesMethod;

		// Token: 0x04000689 RID: 1673
		[SecurityCritical]
		private static MethodInfo getObjectIdMethod;

		// Token: 0x0400068A RID: 1674
		[SecurityCritical]
		private static MethodInfo getArraySizeMethod;

		// Token: 0x0400068B RID: 1675
		[SecurityCritical]
		private static MethodInfo addNewObjectMethod;

		// Token: 0x0400068C RID: 1676
		[SecurityCritical]
		private static MethodInfo addNewObjectWithIdMethod;

		// Token: 0x0400068D RID: 1677
		[SecurityCritical]
		private static MethodInfo replaceDeserializedObjectMethod;

		// Token: 0x0400068E RID: 1678
		[SecurityCritical]
		private static MethodInfo getExistingObjectMethod;

		// Token: 0x0400068F RID: 1679
		[SecurityCritical]
		private static MethodInfo getRealObjectMethod;

		// Token: 0x04000690 RID: 1680
		[SecurityCritical]
		private static MethodInfo readMethod;

		// Token: 0x04000691 RID: 1681
		[SecurityCritical]
		private static MethodInfo ensureArraySizeMethod;

		// Token: 0x04000692 RID: 1682
		[SecurityCritical]
		private static MethodInfo trimArraySizeMethod;

		// Token: 0x04000693 RID: 1683
		[SecurityCritical]
		private static MethodInfo checkEndOfArrayMethod;

		// Token: 0x04000694 RID: 1684
		[SecurityCritical]
		private static MethodInfo getArrayLengthMethod;

		// Token: 0x04000695 RID: 1685
		[SecurityCritical]
		private static MethodInfo readSerializationInfoMethod;

		// Token: 0x04000696 RID: 1686
		[SecurityCritical]
		private static MethodInfo createUnexpectedStateExceptionMethod;

		// Token: 0x04000697 RID: 1687
		[SecurityCritical]
		private static MethodInfo internalSerializeReferenceMethod;

		// Token: 0x04000698 RID: 1688
		[SecurityCritical]
		private static MethodInfo internalSerializeMethod;

		// Token: 0x04000699 RID: 1689
		[SecurityCritical]
		private static MethodInfo writeNullMethod;

		// Token: 0x0400069A RID: 1690
		[SecurityCritical]
		private static MethodInfo incrementArrayCountMethod;

		// Token: 0x0400069B RID: 1691
		[SecurityCritical]
		private static MethodInfo incrementCollectionCountMethod;

		// Token: 0x0400069C RID: 1692
		[SecurityCritical]
		private static MethodInfo incrementCollectionCountGenericMethod;

		// Token: 0x0400069D RID: 1693
		[SecurityCritical]
		private static MethodInfo getDefaultValueMethod;

		// Token: 0x0400069E RID: 1694
		[SecurityCritical]
		private static MethodInfo getNullableValueMethod;

		// Token: 0x0400069F RID: 1695
		[SecurityCritical]
		private static MethodInfo throwRequiredMemberMustBeEmittedMethod;

		// Token: 0x040006A0 RID: 1696
		[SecurityCritical]
		private static MethodInfo getHasValueMethod;

		// Token: 0x040006A1 RID: 1697
		[SecurityCritical]
		private static MethodInfo writeISerializableMethod;

		// Token: 0x040006A2 RID: 1698
		[SecurityCritical]
		private static MethodInfo writeExtensionDataMethod;

		// Token: 0x040006A3 RID: 1699
		[SecurityCritical]
		private static MethodInfo writeXmlValueMethod;

		// Token: 0x040006A4 RID: 1700
		[SecurityCritical]
		private static MethodInfo readXmlValueMethod;

		// Token: 0x040006A5 RID: 1701
		[SecurityCritical]
		private static MethodInfo throwTypeNotSerializableMethod;

		// Token: 0x040006A6 RID: 1702
		[SecurityCritical]
		private static PropertyInfo namespaceProperty;

		// Token: 0x040006A7 RID: 1703
		[SecurityCritical]
		private static FieldInfo contractNamespacesField;

		// Token: 0x040006A8 RID: 1704
		[SecurityCritical]
		private static FieldInfo memberNamesField;

		// Token: 0x040006A9 RID: 1705
		[SecurityCritical]
		private static MethodInfo extensionDataSetExplicitMethodInfo;

		// Token: 0x040006AA RID: 1706
		[SecurityCritical]
		private static PropertyInfo childElementNamespacesProperty;

		// Token: 0x040006AB RID: 1707
		[SecurityCritical]
		private static PropertyInfo collectionItemNameProperty;

		// Token: 0x040006AC RID: 1708
		[SecurityCritical]
		private static PropertyInfo childElementNamespaceProperty;

		// Token: 0x040006AD RID: 1709
		[SecurityCritical]
		private static MethodInfo getDateTimeOffsetMethod;

		// Token: 0x040006AE RID: 1710
		[SecurityCritical]
		private static MethodInfo getDateTimeOffsetAdapterMethod;

		// Token: 0x040006AF RID: 1711
		[SecurityCritical]
		private static MethodInfo traceInstructionMethod;

		// Token: 0x040006B0 RID: 1712
		[SecurityCritical]
		private static MethodInfo throwInvalidDataContractExceptionMethod;

		// Token: 0x040006B1 RID: 1713
		[SecurityCritical]
		private static PropertyInfo serializeReadOnlyTypesProperty;

		// Token: 0x040006B2 RID: 1714
		[SecurityCritical]
		private static PropertyInfo classSerializationExceptionMessageProperty;

		// Token: 0x040006B3 RID: 1715
		[SecurityCritical]
		private static PropertyInfo collectionSerializationExceptionMessageProperty;
	}
}
