﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000109 RID: 265
	internal class GDayDataContract : StringDataContract
	{
		// Token: 0x06000DB9 RID: 3513 RVA: 0x000337F4 File Offset: 0x000319F4
		internal GDayDataContract() : base(DictionaryGlobals.gDayLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
