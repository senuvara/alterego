﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000FB RID: 251
	internal class NonPositiveIntegerDataContract : LongDataContract
	{
		// Token: 0x06000D92 RID: 3474 RVA: 0x00033537 File Offset: 0x00031737
		internal NonPositiveIntegerDataContract() : base(DictionaryGlobals.nonPositiveIntegerLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
