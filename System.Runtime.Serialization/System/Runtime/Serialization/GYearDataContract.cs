﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000107 RID: 263
	internal class GYearDataContract : StringDataContract
	{
		// Token: 0x06000DB7 RID: 3511 RVA: 0x000337D0 File Offset: 0x000319D0
		internal GYearDataContract() : base(DictionaryGlobals.gYearLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
