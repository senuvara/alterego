﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml;

namespace System.Runtime.Serialization
{
	/// <summary>Specifies data contract serializer settings.</summary>
	// Token: 0x020000C3 RID: 195
	public class DataContractSerializerSettings
	{
		/// <summary>Gets or sets the root name of the selected object.</summary>
		/// <returns>The root name of the selected object.</returns>
		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x06000B10 RID: 2832 RVA: 0x0002CF43 File Offset: 0x0002B143
		// (set) Token: 0x06000B11 RID: 2833 RVA: 0x0002CF4B File Offset: 0x0002B14B
		public XmlDictionaryString RootName
		{
			[CompilerGenerated]
			get
			{
				return this.<RootName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RootName>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the root namespace for the specified object.</summary>
		/// <returns>The root namespace for the specified object.</returns>
		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06000B12 RID: 2834 RVA: 0x0002CF54 File Offset: 0x0002B154
		// (set) Token: 0x06000B13 RID: 2835 RVA: 0x0002CF5C File Offset: 0x0002B15C
		public XmlDictionaryString RootNamespace
		{
			[CompilerGenerated]
			get
			{
				return this.<RootNamespace>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RootNamespace>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a collection of types that may be present in the object graph serialized using this instance of the DataContractSerializerSettings.</summary>
		/// <returns>A collection of types that may be present in the object graph serialized using this instance of the DataContractSerializerSettings.</returns>
		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06000B14 RID: 2836 RVA: 0x0002CF65 File Offset: 0x0002B165
		// (set) Token: 0x06000B15 RID: 2837 RVA: 0x0002CF6D File Offset: 0x0002B16D
		public IEnumerable<Type> KnownTypes
		{
			[CompilerGenerated]
			get
			{
				return this.<KnownTypes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<KnownTypes>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the maximum number of items in an object graph to serialize or deserialize.</summary>
		/// <returns>The maximum number of items in an object graph to serialize or deserialize.</returns>
		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06000B16 RID: 2838 RVA: 0x0002CF76 File Offset: 0x0002B176
		// (set) Token: 0x06000B17 RID: 2839 RVA: 0x0002CF7E File Offset: 0x0002B17E
		public int MaxItemsInObjectGraph
		{
			get
			{
				return this.maxItemsInObjectGraph;
			}
			set
			{
				this.maxItemsInObjectGraph = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether to ignore data supplied by an extension of the class when the class is being serialized or deserialized.</summary>
		/// <returns>
		///     <see langword="True" /> to ignore data supplied by an extension of the class when the class is being serialized or deserialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06000B18 RID: 2840 RVA: 0x0002CF87 File Offset: 0x0002B187
		// (set) Token: 0x06000B19 RID: 2841 RVA: 0x0002CF8F File Offset: 0x0002B18F
		public bool IgnoreExtensionDataObject
		{
			[CompilerGenerated]
			get
			{
				return this.<IgnoreExtensionDataObject>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IgnoreExtensionDataObject>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether to use non-standard XML constructs to preserve object reference data.</summary>
		/// <returns>
		///     <see langword="True" /> to use non-standard XML constructs to preserve object reference data; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x06000B1A RID: 2842 RVA: 0x0002CF98 File Offset: 0x0002B198
		// (set) Token: 0x06000B1B RID: 2843 RVA: 0x0002CFA0 File Offset: 0x0002B1A0
		public bool PreserveObjectReferences
		{
			[CompilerGenerated]
			get
			{
				return this.<PreserveObjectReferences>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<PreserveObjectReferences>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a serialization surrogate.</summary>
		/// <returns>The serialization surrogate.</returns>
		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x06000B1C RID: 2844 RVA: 0x0002CFA9 File Offset: 0x0002B1A9
		// (set) Token: 0x06000B1D RID: 2845 RVA: 0x0002CFB1 File Offset: 0x0002B1B1
		public IDataContractSurrogate DataContractSurrogate
		{
			[CompilerGenerated]
			get
			{
				return this.<DataContractSurrogate>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<DataContractSurrogate>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the component used to dynamically map xsi:type declarations to known contract types.</summary>
		/// <returns>The component used to dynamically map xsi:type declarations to known contract types.</returns>
		// Token: 0x170001CA RID: 458
		// (get) Token: 0x06000B1E RID: 2846 RVA: 0x0002CFBA File Offset: 0x0002B1BA
		// (set) Token: 0x06000B1F RID: 2847 RVA: 0x0002CFC2 File Offset: 0x0002B1C2
		public DataContractResolver DataContractResolver
		{
			[CompilerGenerated]
			get
			{
				return this.<DataContractResolver>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<DataContractResolver>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether to serialize read only types.</summary>
		/// <returns>
		///     <see langword="True" /> to serialize read only types; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001CB RID: 459
		// (get) Token: 0x06000B20 RID: 2848 RVA: 0x0002CFCB File Offset: 0x0002B1CB
		// (set) Token: 0x06000B21 RID: 2849 RVA: 0x0002CFD3 File Offset: 0x0002B1D3
		public bool SerializeReadOnlyTypes
		{
			[CompilerGenerated]
			get
			{
				return this.<SerializeReadOnlyTypes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SerializeReadOnlyTypes>k__BackingField = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.DataContractSerializerSettings" /> class.</summary>
		// Token: 0x06000B22 RID: 2850 RVA: 0x0002CFDC File Offset: 0x0002B1DC
		public DataContractSerializerSettings()
		{
		}

		// Token: 0x0400048F RID: 1167
		private int maxItemsInObjectGraph = int.MaxValue;

		// Token: 0x04000490 RID: 1168
		[CompilerGenerated]
		private XmlDictionaryString <RootName>k__BackingField;

		// Token: 0x04000491 RID: 1169
		[CompilerGenerated]
		private XmlDictionaryString <RootNamespace>k__BackingField;

		// Token: 0x04000492 RID: 1170
		[CompilerGenerated]
		private IEnumerable<Type> <KnownTypes>k__BackingField;

		// Token: 0x04000493 RID: 1171
		[CompilerGenerated]
		private bool <IgnoreExtensionDataObject>k__BackingField;

		// Token: 0x04000494 RID: 1172
		[CompilerGenerated]
		private bool <PreserveObjectReferences>k__BackingField;

		// Token: 0x04000495 RID: 1173
		[CompilerGenerated]
		private IDataContractSurrogate <DataContractSurrogate>k__BackingField;

		// Token: 0x04000496 RID: 1174
		[CompilerGenerated]
		private DataContractResolver <DataContractResolver>k__BackingField;

		// Token: 0x04000497 RID: 1175
		[CompilerGenerated]
		private bool <SerializeReadOnlyTypes>k__BackingField;
	}
}
