﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000BC RID: 188
	internal class DataContractPairKey
	{
		// Token: 0x06000AC2 RID: 2754 RVA: 0x0002C5EC File Offset: 0x0002A7EC
		public DataContractPairKey(object object1, object object2)
		{
			this.object1 = object1;
			this.object2 = object2;
		}

		// Token: 0x06000AC3 RID: 2755 RVA: 0x0002C604 File Offset: 0x0002A804
		public override bool Equals(object other)
		{
			DataContractPairKey dataContractPairKey = other as DataContractPairKey;
			return dataContractPairKey != null && ((dataContractPairKey.object1 == this.object1 && dataContractPairKey.object2 == this.object2) || (dataContractPairKey.object1 == this.object2 && dataContractPairKey.object2 == this.object1));
		}

		// Token: 0x06000AC4 RID: 2756 RVA: 0x0002C659 File Offset: 0x0002A859
		public override int GetHashCode()
		{
			return this.object1.GetHashCode() ^ this.object2.GetHashCode();
		}

		// Token: 0x04000477 RID: 1143
		private object object1;

		// Token: 0x04000478 RID: 1144
		private object object2;
	}
}
