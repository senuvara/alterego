﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000114 RID: 276
	internal class ENTITIESDataContract : StringDataContract
	{
		// Token: 0x06000DC4 RID: 3524 RVA: 0x000338BA File Offset: 0x00031ABA
		internal ENTITIESDataContract() : base(DictionaryGlobals.ENTITIESLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
