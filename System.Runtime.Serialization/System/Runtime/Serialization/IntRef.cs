﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000BF RID: 191
	internal class IntRef
	{
		// Token: 0x06000ACC RID: 2764 RVA: 0x0002C6D3 File Offset: 0x0002A8D3
		public IntRef(int value)
		{
			this.value = value;
		}

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x06000ACD RID: 2765 RVA: 0x0002C6E2 File Offset: 0x0002A8E2
		public int Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x0400047A RID: 1146
		private int value;
	}
}
