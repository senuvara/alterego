﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000F8 RID: 248
	internal class IntegerDataContract : LongDataContract
	{
		// Token: 0x06000D8F RID: 3471 RVA: 0x00033501 File Offset: 0x00031701
		internal IntegerDataContract() : base(DictionaryGlobals.integerLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
