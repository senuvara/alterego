﻿using System;

namespace System.Runtime.Serialization
{
	/// <summary>The exception that is thrown when the <see cref="T:System.Runtime.Serialization.DataContractSerializer" /> or <see cref="T:System.Runtime.Serialization.NetDataContractSerializer" /> encounters an invalid data contract during serialization and deserialization. </summary>
	// Token: 0x020000E5 RID: 229
	[Serializable]
	public class InvalidDataContractException : Exception
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.InvalidDataContractException" /> class.  </summary>
		// Token: 0x06000CD1 RID: 3281 RVA: 0x00031772 File Offset: 0x0002F972
		public InvalidDataContractException()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.InvalidDataContractException" /> class with the specified error message. </summary>
		/// <param name="message">A description of the error. </param>
		// Token: 0x06000CD2 RID: 3282 RVA: 0x0003177A File Offset: 0x0002F97A
		public InvalidDataContractException(string message) : base(message)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.InvalidDataContractException" /> class with the specified error message and inner exception. </summary>
		/// <param name="message">A description of the error. </param>
		/// <param name="innerException">The original <see cref="T:System.Exception" />. </param>
		// Token: 0x06000CD3 RID: 3283 RVA: 0x00031783 File Offset: 0x0002F983
		public InvalidDataContractException(string message, Exception innerException) : base(message, innerException)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.InvalidDataContractException" /> class with the specified <see cref="T:System.Runtime.Serialization.SerializationInfo" /> and <see cref="T:System.Runtime.Serialization.StreamingContext" />. </summary>
		/// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that contains data needed to serialize and deserialize an object. </param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that specifies user context during serialization and deserialization.</param>
		// Token: 0x06000CD4 RID: 3284 RVA: 0x0003178D File Offset: 0x0002F98D
		protected InvalidDataContractException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
