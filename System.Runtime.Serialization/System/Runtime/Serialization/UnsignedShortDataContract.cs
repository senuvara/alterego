﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000F4 RID: 244
	internal class UnsignedShortDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D7A RID: 3450 RVA: 0x0003338B File Offset: 0x0003158B
		internal UnsignedShortDataContract() : base(typeof(ushort), DictionaryGlobals.UnsignedShortLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x06000D7B RID: 3451 RVA: 0x000333A7 File Offset: 0x000315A7
		internal override string WriteMethodName
		{
			get
			{
				return "WriteUnsignedShort";
			}
		}

		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x06000D7C RID: 3452 RVA: 0x000333AE File Offset: 0x000315AE
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsUnsignedShort";
			}
		}

		// Token: 0x06000D7D RID: 3453 RVA: 0x000333B5 File Offset: 0x000315B5
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteUnsignedShort((ushort)obj);
		}

		// Token: 0x06000D7E RID: 3454 RVA: 0x000333C3 File Offset: 0x000315C3
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsUnsignedShort(), context);
			}
			return reader.ReadElementContentAsUnsignedShort();
		}
	}
}
