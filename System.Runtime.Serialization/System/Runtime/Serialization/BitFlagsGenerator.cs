﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x0200014A RID: 330
	internal class BitFlagsGenerator
	{
		// Token: 0x0600110C RID: 4364 RVA: 0x0003D0C8 File Offset: 0x0003B2C8
		public BitFlagsGenerator(int bitCount)
		{
			this.bitCount = bitCount;
			int num = (bitCount + 7) / 8;
			this.locals = new byte[num];
		}

		// Token: 0x0600110D RID: 4365 RVA: 0x0003D0F4 File Offset: 0x0003B2F4
		public void Store(int bitIndex, bool value)
		{
			if (value)
			{
				byte[] array = this.locals;
				int byteIndex = BitFlagsGenerator.GetByteIndex(bitIndex);
				array[byteIndex] |= BitFlagsGenerator.GetBitValue(bitIndex);
				return;
			}
			byte[] array2 = this.locals;
			int byteIndex2 = BitFlagsGenerator.GetByteIndex(bitIndex);
			array2[byteIndex2] &= ~BitFlagsGenerator.GetBitValue(bitIndex);
		}

		// Token: 0x0600110E RID: 4366 RVA: 0x0003D134 File Offset: 0x0003B334
		public bool Load(int bitIndex)
		{
			byte b = this.locals[BitFlagsGenerator.GetByteIndex(bitIndex)];
			byte bitValue = BitFlagsGenerator.GetBitValue(bitIndex);
			return (b & bitValue) == bitValue;
		}

		// Token: 0x0600110F RID: 4367 RVA: 0x0003D15A File Offset: 0x0003B35A
		public byte[] LoadArray()
		{
			return (byte[])this.locals.Clone();
		}

		// Token: 0x06001110 RID: 4368 RVA: 0x0003D16C File Offset: 0x0003B36C
		public int GetLocalCount()
		{
			return this.locals.Length;
		}

		// Token: 0x06001111 RID: 4369 RVA: 0x0003D176 File Offset: 0x0003B376
		public int GetBitCount()
		{
			return this.bitCount;
		}

		// Token: 0x06001112 RID: 4370 RVA: 0x0003D17E File Offset: 0x0003B37E
		public byte GetLocal(int i)
		{
			return this.locals[i];
		}

		// Token: 0x06001113 RID: 4371 RVA: 0x0003D188 File Offset: 0x0003B388
		public static bool IsBitSet(byte[] bytes, int bitIndex)
		{
			int byteIndex = BitFlagsGenerator.GetByteIndex(bitIndex);
			byte bitValue = BitFlagsGenerator.GetBitValue(bitIndex);
			return (bytes[byteIndex] & bitValue) == bitValue;
		}

		// Token: 0x06001114 RID: 4372 RVA: 0x0003D1AC File Offset: 0x0003B3AC
		public static void SetBit(byte[] bytes, int bitIndex)
		{
			int byteIndex = BitFlagsGenerator.GetByteIndex(bitIndex);
			byte bitValue = BitFlagsGenerator.GetBitValue(bitIndex);
			int num = byteIndex;
			bytes[num] |= bitValue;
		}

		// Token: 0x06001115 RID: 4373 RVA: 0x0003D1D4 File Offset: 0x0003B3D4
		private static int GetByteIndex(int bitIndex)
		{
			return bitIndex >> 3;
		}

		// Token: 0x06001116 RID: 4374 RVA: 0x0003D1D9 File Offset: 0x0003B3D9
		private static byte GetBitValue(int bitIndex)
		{
			return (byte)(1 << (bitIndex & 7));
		}

		// Token: 0x04000701 RID: 1793
		private int bitCount;

		// Token: 0x04000702 RID: 1794
		private byte[] locals;
	}
}
