﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000EE RID: 238
	internal class CharDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D5F RID: 3423 RVA: 0x000331A8 File Offset: 0x000313A8
		internal CharDataContract() : this(DictionaryGlobals.CharLocalName, DictionaryGlobals.SerializationNamespace)
		{
		}

		// Token: 0x06000D60 RID: 3424 RVA: 0x000331BA File Offset: 0x000313BA
		internal CharDataContract(XmlDictionaryString name, XmlDictionaryString ns) : base(typeof(char), name, ns)
		{
		}

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x06000D61 RID: 3425 RVA: 0x000331CE File Offset: 0x000313CE
		internal override string WriteMethodName
		{
			get
			{
				return "WriteChar";
			}
		}

		// Token: 0x170002CF RID: 719
		// (get) Token: 0x06000D62 RID: 3426 RVA: 0x000331D5 File Offset: 0x000313D5
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsChar";
			}
		}

		// Token: 0x06000D63 RID: 3427 RVA: 0x000331DC File Offset: 0x000313DC
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteChar((char)obj);
		}

		// Token: 0x06000D64 RID: 3428 RVA: 0x000331EA File Offset: 0x000313EA
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsChar(), context);
			}
			return reader.ReadElementContentAsChar();
		}
	}
}
