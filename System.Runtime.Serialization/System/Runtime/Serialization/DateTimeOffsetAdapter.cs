﻿using System;
using System.Globalization;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000C9 RID: 201
	[DataContract(Name = "DateTimeOffset", Namespace = "http://schemas.datacontract.org/2004/07/System")]
	internal struct DateTimeOffsetAdapter
	{
		// Token: 0x06000B8A RID: 2954 RVA: 0x0002DFAA File Offset: 0x0002C1AA
		public DateTimeOffsetAdapter(DateTime dateTime, short offsetMinutes)
		{
			this.utcDateTime = dateTime;
			this.offsetMinutes = offsetMinutes;
		}

		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x06000B8B RID: 2955 RVA: 0x0002DFBA File Offset: 0x0002C1BA
		// (set) Token: 0x06000B8C RID: 2956 RVA: 0x0002DFC2 File Offset: 0x0002C1C2
		[DataMember(Name = "DateTime", IsRequired = true)]
		public DateTime UtcDateTime
		{
			get
			{
				return this.utcDateTime;
			}
			set
			{
				this.utcDateTime = value;
			}
		}

		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x06000B8D RID: 2957 RVA: 0x0002DFCB File Offset: 0x0002C1CB
		// (set) Token: 0x06000B8E RID: 2958 RVA: 0x0002DFD3 File Offset: 0x0002C1D3
		[DataMember(Name = "OffsetMinutes", IsRequired = true)]
		public short OffsetMinutes
		{
			get
			{
				return this.offsetMinutes;
			}
			set
			{
				this.offsetMinutes = value;
			}
		}

		// Token: 0x06000B8F RID: 2959 RVA: 0x0002DFDC File Offset: 0x0002C1DC
		public static DateTimeOffset GetDateTimeOffset(DateTimeOffsetAdapter value)
		{
			DateTimeOffset result;
			try
			{
				if (value.UtcDateTime.Kind == DateTimeKind.Unspecified)
				{
					result = new DateTimeOffset(value.UtcDateTime, new TimeSpan(0, (int)value.OffsetMinutes, 0));
				}
				else
				{
					DateTimeOffset dateTimeOffset = new DateTimeOffset(value.UtcDateTime);
					result = dateTimeOffset.ToOffset(new TimeSpan(0, (int)value.OffsetMinutes, 0));
				}
			}
			catch (ArgumentException exception)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(value.ToString(CultureInfo.InvariantCulture), "DateTimeOffset", exception));
			}
			return result;
		}

		// Token: 0x06000B90 RID: 2960 RVA: 0x0002E070 File Offset: 0x0002C270
		public static DateTimeOffsetAdapter GetDateTimeOffsetAdapter(DateTimeOffset value)
		{
			return new DateTimeOffsetAdapter(value.UtcDateTime, (short)value.Offset.TotalMinutes);
		}

		// Token: 0x06000B91 RID: 2961 RVA: 0x0002E099 File Offset: 0x0002C299
		public string ToString(IFormatProvider provider)
		{
			return string.Concat(new object[]
			{
				"DateTime: ",
				this.UtcDateTime,
				", Offset: ",
				this.OffsetMinutes
			});
		}

		// Token: 0x040004B4 RID: 1204
		private DateTime utcDateTime;

		// Token: 0x040004B5 RID: 1205
		private short offsetMinutes;
	}
}
