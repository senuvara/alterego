﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x0200011A RID: 282
	internal class XsDurationDataContract : TimeSpanDataContract
	{
		// Token: 0x06000DD9 RID: 3545 RVA: 0x00033A65 File Offset: 0x00031C65
		internal XsDurationDataContract() : base(DictionaryGlobals.TimeSpanLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
