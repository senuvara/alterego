﻿using System;
using System.Collections.Generic;

namespace System.Runtime.Serialization
{
	// Token: 0x020000B9 RID: 185
	internal interface IGenericNameProvider
	{
		// Token: 0x06000AA6 RID: 2726
		int GetParameterCount();

		// Token: 0x06000AA7 RID: 2727
		IList<int> GetNestedParameterCounts();

		// Token: 0x06000AA8 RID: 2728
		string GetParameterName(int paramIndex);

		// Token: 0x06000AA9 RID: 2729
		string GetNamespaces();

		// Token: 0x06000AAA RID: 2730
		string GetGenericTypeName();

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x06000AAB RID: 2731
		bool ParametersFromBuiltInNamespaces { get; }
	}
}
