﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000F6 RID: 246
	internal class UnsignedIntDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D84 RID: 3460 RVA: 0x00033441 File Offset: 0x00031641
		internal UnsignedIntDataContract() : base(typeof(uint), DictionaryGlobals.UnsignedIntLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002DC RID: 732
		// (get) Token: 0x06000D85 RID: 3461 RVA: 0x0003345D File Offset: 0x0003165D
		internal override string WriteMethodName
		{
			get
			{
				return "WriteUnsignedInt";
			}
		}

		// Token: 0x170002DD RID: 733
		// (get) Token: 0x06000D86 RID: 3462 RVA: 0x00033464 File Offset: 0x00031664
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsUnsignedInt";
			}
		}

		// Token: 0x06000D87 RID: 3463 RVA: 0x0003346B File Offset: 0x0003166B
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteUnsignedInt((uint)obj);
		}

		// Token: 0x06000D88 RID: 3464 RVA: 0x00033479 File Offset: 0x00031679
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsUnsignedInt(), context);
			}
			return reader.ReadElementContentAsUnsignedInt();
		}
	}
}
