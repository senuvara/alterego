﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Diagnostics;
using System.Runtime.Serialization.Diagnostics;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace System.Runtime.Serialization
{
	// Token: 0x0200013E RID: 318
	internal class XmlObjectSerializerReadContext : XmlObjectSerializerContext
	{
		// Token: 0x17000375 RID: 885
		// (get) Token: 0x06000F1F RID: 3871 RVA: 0x00036B9B File Offset: 0x00034D9B
		private HybridObjectCache DeserializedObjects
		{
			get
			{
				if (this.deserializedObjects == null)
				{
					this.deserializedObjects = new HybridObjectCache();
				}
				return this.deserializedObjects;
			}
		}

		// Token: 0x17000376 RID: 886
		// (get) Token: 0x06000F20 RID: 3872 RVA: 0x00036BB6 File Offset: 0x00034DB6
		private XmlDocument Document
		{
			get
			{
				if (this.xmlDocument == null)
				{
					this.xmlDocument = new XmlDocument();
				}
				return this.xmlDocument;
			}
		}

		// Token: 0x17000377 RID: 887
		// (get) Token: 0x06000F21 RID: 3873 RVA: 0x00036BD1 File Offset: 0x00034DD1
		// (set) Token: 0x06000F22 RID: 3874 RVA: 0x00036BD9 File Offset: 0x00034DD9
		internal override bool IsGetOnlyCollection
		{
			get
			{
				return this.isGetOnlyCollection;
			}
			set
			{
				this.isGetOnlyCollection = value;
			}
		}

		// Token: 0x06000F23 RID: 3875 RVA: 0x00036BE2 File Offset: 0x00034DE2
		internal object GetCollectionMember()
		{
			return this.getOnlyCollectionValue;
		}

		// Token: 0x06000F24 RID: 3876 RVA: 0x00036BEA File Offset: 0x00034DEA
		internal void StoreCollectionMemberInfo(object collectionMember)
		{
			this.getOnlyCollectionValue = collectionMember;
			this.isGetOnlyCollection = true;
		}

		// Token: 0x06000F25 RID: 3877 RVA: 0x00036BFA File Offset: 0x00034DFA
		internal static void ThrowNullValueReturnedForGetOnlyCollectionException(Type type)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("The get-only collection of type '{0}' returned a null value.  The input stream contains collection items which cannot be added if the instance is null.  Consider initializing the collection either in the constructor of the the object or in the getter.", new object[]
			{
				DataContract.GetClrTypeFullName(type)
			})));
		}

		// Token: 0x06000F26 RID: 3878 RVA: 0x00036C1F File Offset: 0x00034E1F
		internal static void ThrowArrayExceededSizeException(int arraySize, Type type)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Array length '{0}' provided by the get-only collection of type '{1}' is less than the number of array elements found in the input stream.  Consider increasing the length of the array.", new object[]
			{
				arraySize,
				DataContract.GetClrTypeFullName(type)
			})));
		}

		// Token: 0x06000F27 RID: 3879 RVA: 0x00036C4D File Offset: 0x00034E4D
		internal static XmlObjectSerializerReadContext CreateContext(DataContractSerializer serializer, DataContract rootTypeDataContract, DataContractResolver dataContractResolver)
		{
			if (!serializer.PreserveObjectReferences && serializer.DataContractSurrogate == null)
			{
				return new XmlObjectSerializerReadContext(serializer, rootTypeDataContract, dataContractResolver);
			}
			return new XmlObjectSerializerReadContextComplex(serializer, rootTypeDataContract, dataContractResolver);
		}

		// Token: 0x06000F28 RID: 3880 RVA: 0x00036C70 File Offset: 0x00034E70
		internal static XmlObjectSerializerReadContext CreateContext(NetDataContractSerializer serializer)
		{
			return new XmlObjectSerializerReadContextComplex(serializer);
		}

		// Token: 0x06000F29 RID: 3881 RVA: 0x00036C78 File Offset: 0x00034E78
		internal XmlObjectSerializerReadContext(XmlObjectSerializer serializer, int maxItemsInObjectGraph, StreamingContext streamingContext, bool ignoreExtensionDataObject) : base(serializer, maxItemsInObjectGraph, streamingContext, ignoreExtensionDataObject)
		{
		}

		// Token: 0x06000F2A RID: 3882 RVA: 0x00036C85 File Offset: 0x00034E85
		internal XmlObjectSerializerReadContext(DataContractSerializer serializer, DataContract rootTypeDataContract, DataContractResolver dataContractResolver) : base(serializer, rootTypeDataContract, dataContractResolver)
		{
			this.attributes = new Attributes();
		}

		// Token: 0x06000F2B RID: 3883 RVA: 0x00036C9B File Offset: 0x00034E9B
		protected XmlObjectSerializerReadContext(NetDataContractSerializer serializer) : base(serializer)
		{
			this.attributes = new Attributes();
		}

		// Token: 0x06000F2C RID: 3884 RVA: 0x00036CB0 File Offset: 0x00034EB0
		public virtual object InternalDeserialize(XmlReaderDelegator xmlReader, int id, RuntimeTypeHandle declaredTypeHandle, string name, string ns)
		{
			DataContract dataContract = this.GetDataContract(id, declaredTypeHandle);
			return this.InternalDeserialize(xmlReader, name, ns, Type.GetTypeFromHandle(declaredTypeHandle), ref dataContract);
		}

		// Token: 0x06000F2D RID: 3885 RVA: 0x00036CDC File Offset: 0x00034EDC
		internal virtual object InternalDeserialize(XmlReaderDelegator xmlReader, Type declaredType, string name, string ns)
		{
			DataContract dataContract = base.GetDataContract(declaredType);
			return this.InternalDeserialize(xmlReader, name, ns, declaredType, ref dataContract);
		}

		// Token: 0x06000F2E RID: 3886 RVA: 0x00036CFE File Offset: 0x00034EFE
		internal virtual object InternalDeserialize(XmlReaderDelegator xmlReader, Type declaredType, DataContract dataContract, string name, string ns)
		{
			if (dataContract == null)
			{
				base.GetDataContract(declaredType);
			}
			return this.InternalDeserialize(xmlReader, name, ns, declaredType, ref dataContract);
		}

		// Token: 0x06000F2F RID: 3887 RVA: 0x00036D1C File Offset: 0x00034F1C
		protected bool TryHandleNullOrRef(XmlReaderDelegator reader, Type declaredType, string name, string ns, ref object retObj)
		{
			this.ReadAttributes(reader);
			if (this.attributes.Ref != Globals.NewObjectId)
			{
				if (this.isGetOnlyCollection)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("On type '{1}', attribute '{0}' points to get-only collection, which is not supported.", new object[]
					{
						this.attributes.Ref,
						DataContract.GetClrTypeFullName(declaredType)
					})));
				}
				retObj = this.GetExistingObject(this.attributes.Ref, declaredType, name, ns);
				reader.Skip();
				return true;
			}
			else
			{
				if (this.attributes.XsiNil)
				{
					reader.Skip();
					return true;
				}
				return false;
			}
		}

		// Token: 0x06000F30 RID: 3888 RVA: 0x00036DB8 File Offset: 0x00034FB8
		protected object InternalDeserialize(XmlReaderDelegator reader, string name, string ns, Type declaredType, ref DataContract dataContract)
		{
			object result = null;
			if (this.TryHandleNullOrRef(reader, dataContract.UnderlyingType, name, ns, ref result))
			{
				return result;
			}
			bool flag = false;
			if (dataContract.KnownDataContracts != null)
			{
				this.scopedKnownTypes.Push(dataContract.KnownDataContracts);
				flag = true;
			}
			if (this.attributes.XsiTypeName != null)
			{
				dataContract = base.ResolveDataContractFromKnownTypes(this.attributes.XsiTypeName, this.attributes.XsiTypeNamespace, dataContract, declaredType);
				if (dataContract == null)
				{
					if (base.DataContractResolver == null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.TryAddLineInfo(reader, SR.GetString("Element '{2}:{3}' contains data of the '{0}:{1}' data contract. The deserializer has no knowledge of any type that maps to this contract. Add the type corresponding to '{1}' to the list of known types - for example, by using the KnownTypeAttribute attribute or by adding it to the list of known types passed to DataContractSerializer.", new object[]
						{
							this.attributes.XsiTypeNamespace,
							this.attributes.XsiTypeName,
							reader.NamespaceURI,
							reader.LocalName
						}))));
					}
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.TryAddLineInfo(reader, SR.GetString("Element '{2}:{3}' contains data from a type that maps to the name '{0}:{1}'. The deserializer has no knowledge of any type that maps to this name. Consider changing the implementation of the ResolveName method on your DataContractResolver to return a non-null value for name '{1}' and namespace '{0}'.", new object[]
					{
						this.attributes.XsiTypeNamespace,
						this.attributes.XsiTypeName,
						reader.NamespaceURI,
						reader.LocalName
					}))));
				}
				else
				{
					flag = this.ReplaceScopedKnownTypesTop(dataContract.KnownDataContracts, flag);
				}
			}
			if (dataContract.IsISerializable && this.attributes.FactoryTypeName != null)
			{
				DataContract dataContract2 = base.ResolveDataContractFromKnownTypes(this.attributes.FactoryTypeName, this.attributes.FactoryTypeNamespace, dataContract, declaredType);
				if (dataContract2 != null)
				{
					if (!dataContract2.IsISerializable)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("For data contract '{1}', factory type '{0}' is not ISerializable.", new object[]
						{
							DataContract.GetClrTypeFullName(dataContract2.UnderlyingType),
							DataContract.GetClrTypeFullName(dataContract.UnderlyingType)
						})));
					}
					dataContract = dataContract2;
					flag = this.ReplaceScopedKnownTypesTop(dataContract.KnownDataContracts, flag);
				}
				else if (DiagnosticUtility.ShouldTraceWarning)
				{
					Dictionary<string, string> dictionary = new Dictionary<string, string>(2);
					dictionary["FactoryType"] = this.attributes.FactoryTypeNamespace + ":" + this.attributes.FactoryTypeName;
					dictionary["ISerializableType"] = dataContract.StableName.Namespace + ":" + dataContract.StableName.Name;
					TraceUtility.Trace(TraceEventType.Warning, 196625, SR.GetString("Factory type not found"), new DictionaryTraceRecord(dictionary));
				}
			}
			if (flag)
			{
				object result2 = this.ReadDataContractValue(dataContract, reader);
				this.scopedKnownTypes.Pop();
				return result2;
			}
			return this.ReadDataContractValue(dataContract, reader);
		}

		// Token: 0x06000F31 RID: 3889 RVA: 0x00037033 File Offset: 0x00035233
		private bool ReplaceScopedKnownTypesTop(Dictionary<XmlQualifiedName, DataContract> knownDataContracts, bool knownTypesAddedInCurrentScope)
		{
			if (knownTypesAddedInCurrentScope)
			{
				this.scopedKnownTypes.Pop();
				knownTypesAddedInCurrentScope = false;
			}
			if (knownDataContracts != null)
			{
				this.scopedKnownTypes.Push(knownDataContracts);
				knownTypesAddedInCurrentScope = true;
			}
			return knownTypesAddedInCurrentScope;
		}

		// Token: 0x06000F32 RID: 3890 RVA: 0x00037059 File Offset: 0x00035259
		public static bool MoveToNextElement(XmlReaderDelegator xmlReader)
		{
			return xmlReader.MoveToContent() != XmlNodeType.EndElement;
		}

		// Token: 0x06000F33 RID: 3891 RVA: 0x00037068 File Offset: 0x00035268
		public int GetMemberIndex(XmlReaderDelegator xmlReader, XmlDictionaryString[] memberNames, XmlDictionaryString[] memberNamespaces, int memberIndex, ExtensionDataObject extensionData)
		{
			for (int i = memberIndex + 1; i < memberNames.Length; i++)
			{
				if (xmlReader.IsStartElement(memberNames[i], memberNamespaces[i]))
				{
					return i;
				}
			}
			this.HandleMemberNotFound(xmlReader, extensionData, memberIndex);
			return memberNames.Length;
		}

		// Token: 0x06000F34 RID: 3892 RVA: 0x000370A4 File Offset: 0x000352A4
		public int GetMemberIndexWithRequiredMembers(XmlReaderDelegator xmlReader, XmlDictionaryString[] memberNames, XmlDictionaryString[] memberNamespaces, int memberIndex, int requiredIndex, ExtensionDataObject extensionData)
		{
			for (int i = memberIndex + 1; i < memberNames.Length; i++)
			{
				if (xmlReader.IsStartElement(memberNames[i], memberNamespaces[i]))
				{
					if (requiredIndex < i)
					{
						XmlObjectSerializerReadContext.ThrowRequiredMemberMissingException(xmlReader, memberIndex, requiredIndex, memberNames);
					}
					return i;
				}
			}
			this.HandleMemberNotFound(xmlReader, extensionData, memberIndex);
			return memberNames.Length;
		}

		// Token: 0x06000F35 RID: 3893 RVA: 0x000370F0 File Offset: 0x000352F0
		public static void ThrowRequiredMemberMissingException(XmlReaderDelegator xmlReader, int memberIndex, int requiredIndex, XmlDictionaryString[] memberNames)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (requiredIndex == memberNames.Length)
			{
				requiredIndex--;
			}
			for (int i = memberIndex + 1; i <= requiredIndex; i++)
			{
				if (stringBuilder.Length != 0)
				{
					stringBuilder.Append(" | ");
				}
				stringBuilder.Append(memberNames[i].Value);
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(XmlObjectSerializer.TryAddLineInfo(xmlReader, SR.GetString("'{0}' '{1}' from namespace '{2}' is not expected. Expecting element '{3}'.", new object[]
			{
				xmlReader.NodeType,
				xmlReader.LocalName,
				xmlReader.NamespaceURI,
				stringBuilder.ToString()
			}))));
		}

		// Token: 0x06000F36 RID: 3894 RVA: 0x00037188 File Offset: 0x00035388
		protected void HandleMemberNotFound(XmlReaderDelegator xmlReader, ExtensionDataObject extensionData, int memberIndex)
		{
			xmlReader.MoveToContent();
			if (xmlReader.NodeType != XmlNodeType.Element)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.Element, xmlReader));
			}
			if (base.IgnoreExtensionDataObject || extensionData == null)
			{
				this.SkipUnknownElement(xmlReader);
				return;
			}
			this.HandleUnknownElement(xmlReader, extensionData, memberIndex);
		}

		// Token: 0x06000F37 RID: 3895 RVA: 0x000371C3 File Offset: 0x000353C3
		internal void HandleUnknownElement(XmlReaderDelegator xmlReader, ExtensionDataObject extensionData, int memberIndex)
		{
			if (extensionData.Members == null)
			{
				extensionData.Members = new List<ExtensionDataMember>();
			}
			extensionData.Members.Add(this.ReadExtensionDataMember(xmlReader, memberIndex));
		}

		// Token: 0x06000F38 RID: 3896 RVA: 0x000371EC File Offset: 0x000353EC
		public void SkipUnknownElement(XmlReaderDelegator xmlReader)
		{
			this.ReadAttributes(xmlReader);
			if (DiagnosticUtility.ShouldTraceVerbose)
			{
				TraceUtility.Trace(TraceEventType.Verbose, 196615, SR.GetString("Element ignored"), new StringTraceRecord("Element", xmlReader.NamespaceURI + ":" + xmlReader.LocalName));
			}
			xmlReader.Skip();
		}

		// Token: 0x06000F39 RID: 3897 RVA: 0x00037244 File Offset: 0x00035444
		public string ReadIfNullOrRef(XmlReaderDelegator xmlReader, Type memberType, bool isMemberTypeSerializable)
		{
			if (this.attributes.Ref != Globals.NewObjectId)
			{
				this.CheckIfTypeSerializable(memberType, isMemberTypeSerializable);
				xmlReader.Skip();
				return this.attributes.Ref;
			}
			if (this.attributes.XsiNil)
			{
				this.CheckIfTypeSerializable(memberType, isMemberTypeSerializable);
				xmlReader.Skip();
				return null;
			}
			return Globals.NewObjectId;
		}

		// Token: 0x06000F3A RID: 3898 RVA: 0x000372A4 File Offset: 0x000354A4
		internal virtual void ReadAttributes(XmlReaderDelegator xmlReader)
		{
			if (this.attributes == null)
			{
				this.attributes = new Attributes();
			}
			this.attributes.Read(xmlReader);
		}

		// Token: 0x06000F3B RID: 3899 RVA: 0x000372C5 File Offset: 0x000354C5
		public void ResetAttributes()
		{
			if (this.attributes != null)
			{
				this.attributes.Reset();
			}
		}

		// Token: 0x06000F3C RID: 3900 RVA: 0x000372DA File Offset: 0x000354DA
		public string GetObjectId()
		{
			return this.attributes.Id;
		}

		// Token: 0x06000F3D RID: 3901 RVA: 0x000372E7 File Offset: 0x000354E7
		internal virtual int GetArraySize()
		{
			return -1;
		}

		// Token: 0x06000F3E RID: 3902 RVA: 0x000372EA File Offset: 0x000354EA
		public void AddNewObject(object obj)
		{
			this.AddNewObjectWithId(this.attributes.Id, obj);
		}

		// Token: 0x06000F3F RID: 3903 RVA: 0x000372FE File Offset: 0x000354FE
		public void AddNewObjectWithId(string id, object obj)
		{
			if (id != Globals.NewObjectId)
			{
				this.DeserializedObjects.Add(id, obj);
			}
			if (this.extensionDataReader != null)
			{
				this.extensionDataReader.UnderlyingExtensionDataReader.SetDeserializedValue(obj);
			}
		}

		// Token: 0x06000F40 RID: 3904 RVA: 0x00037334 File Offset: 0x00035534
		public void ReplaceDeserializedObject(string id, object oldObj, object newObj)
		{
			if (oldObj == newObj)
			{
				return;
			}
			if (id != Globals.NewObjectId)
			{
				if (this.DeserializedObjects.IsObjectReferenced(id))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Factory object contains a reference to self. Old object is '{0}', new object is '{1}'.", new object[]
					{
						DataContract.GetClrTypeFullName(oldObj.GetType()),
						DataContract.GetClrTypeFullName(newObj.GetType()),
						id
					})));
				}
				this.DeserializedObjects.Remove(id);
				this.DeserializedObjects.Add(id, newObj);
			}
			if (this.extensionDataReader != null)
			{
				this.extensionDataReader.UnderlyingExtensionDataReader.SetDeserializedValue(newObj);
			}
		}

		// Token: 0x06000F41 RID: 3905 RVA: 0x000373D0 File Offset: 0x000355D0
		public object GetExistingObject(string id, Type type, string name, string ns)
		{
			object obj = this.DeserializedObjects.GetObject(id);
			if (obj == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Deserialized object with reference id '{0}' not found in stream.", new object[]
				{
					id
				})));
			}
			if (obj is IDataNode)
			{
				IDataNode dataNode = (IDataNode)obj;
				obj = ((dataNode.Value != null && dataNode.IsFinalValue) ? dataNode.Value : this.DeserializeFromExtensionData(dataNode, type, name, ns));
			}
			return obj;
		}

		// Token: 0x06000F42 RID: 3906 RVA: 0x00037440 File Offset: 0x00035640
		private object GetExistingObjectOrExtensionData(string id)
		{
			object @object = this.DeserializedObjects.GetObject(id);
			if (@object == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Deserialized object with reference id '{0}' not found in stream.", new object[]
				{
					id
				})));
			}
			return @object;
		}

		// Token: 0x06000F43 RID: 3907 RVA: 0x00037480 File Offset: 0x00035680
		public object GetRealObject(IObjectReference obj, string id)
		{
			object realObject = SurrogateDataContract.GetRealObject(obj, base.GetStreamingContext());
			if (realObject == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("On the surrogate data contract for '{0}', GetRealObject method returned null.", new object[]
				{
					DataContract.GetClrTypeFullName(obj.GetType())
				})));
			}
			this.ReplaceDeserializedObject(id, obj, realObject);
			return realObject;
		}

		// Token: 0x06000F44 RID: 3908 RVA: 0x000374D0 File Offset: 0x000356D0
		private object DeserializeFromExtensionData(IDataNode dataNode, Type type, string name, string ns)
		{
			ExtensionDataReader extensionDataReader;
			if (this.extensionDataReader == null)
			{
				extensionDataReader = new ExtensionDataReader(this);
				this.extensionDataReader = this.CreateReaderDelegatorForReader(extensionDataReader);
			}
			else
			{
				extensionDataReader = this.extensionDataReader.UnderlyingExtensionDataReader;
			}
			extensionDataReader.SetDataNode(dataNode, name, ns);
			object result = this.InternalDeserialize(this.extensionDataReader, type, name, ns);
			dataNode.Clear();
			extensionDataReader.Reset();
			return result;
		}

		// Token: 0x06000F45 RID: 3909 RVA: 0x0003752D File Offset: 0x0003572D
		public static void Read(XmlReaderDelegator xmlReader)
		{
			if (!xmlReader.Read())
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Unexpected end of file.")));
			}
		}

		// Token: 0x06000F46 RID: 3910 RVA: 0x0003754C File Offset: 0x0003574C
		internal static void ParseQualifiedName(string qname, XmlReaderDelegator xmlReader, out string name, out string ns, out string prefix)
		{
			int num = qname.IndexOf(':');
			prefix = "";
			if (num >= 0)
			{
				prefix = qname.Substring(0, num);
			}
			name = qname.Substring(num + 1);
			ns = xmlReader.LookupNamespace(prefix);
		}

		// Token: 0x06000F47 RID: 3911 RVA: 0x00037590 File Offset: 0x00035790
		public static T[] EnsureArraySize<T>(T[] array, int index)
		{
			if (array.Length <= index)
			{
				if (index == 2147483647)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("The maximum array length ({0}) has been exceeded while reading XML data for array of type '{1}'.", new object[]
					{
						int.MaxValue,
						DataContract.GetClrTypeFullName(typeof(T))
					})));
				}
				T[] array2 = new T[(index < 1073741823) ? (index * 2) : int.MaxValue];
				Array.Copy(array, 0, array2, 0, array.Length);
				array = array2;
			}
			return array;
		}

		// Token: 0x06000F48 RID: 3912 RVA: 0x00037610 File Offset: 0x00035810
		public static T[] TrimArraySize<T>(T[] array, int size)
		{
			if (size != array.Length)
			{
				T[] array2 = new T[size];
				Array.Copy(array, 0, array2, 0, size);
				array = array2;
			}
			return array;
		}

		// Token: 0x06000F49 RID: 3913 RVA: 0x00037638 File Offset: 0x00035838
		public void CheckEndOfArray(XmlReaderDelegator xmlReader, int arraySize, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			if (xmlReader.NodeType == XmlNodeType.EndElement)
			{
				return;
			}
			while (xmlReader.IsStartElement())
			{
				if (xmlReader.IsStartElement(itemName, itemNamespace))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Array length '{0}' provided by Size attribute is not equal to the number of array elements '{1}' from namespace '{2}' found.", new object[]
					{
						arraySize,
						itemName.Value,
						itemNamespace.Value
					})));
				}
				this.SkipUnknownElement(xmlReader);
			}
			if (xmlReader.NodeType != XmlNodeType.EndElement)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.EndElement, xmlReader));
			}
		}

		// Token: 0x06000F4A RID: 3914 RVA: 0x000376B9 File Offset: 0x000358B9
		internal object ReadIXmlSerializable(XmlReaderDelegator xmlReader, XmlDataContract xmlDataContract, bool isMemberType)
		{
			if (this.xmlSerializableReader == null)
			{
				this.xmlSerializableReader = new XmlSerializableReader();
			}
			return XmlObjectSerializerReadContext.ReadIXmlSerializable(this.xmlSerializableReader, xmlReader, xmlDataContract, isMemberType);
		}

		// Token: 0x06000F4B RID: 3915 RVA: 0x000376DC File Offset: 0x000358DC
		internal static object ReadRootIXmlSerializable(XmlReaderDelegator xmlReader, XmlDataContract xmlDataContract, bool isMemberType)
		{
			return XmlObjectSerializerReadContext.ReadIXmlSerializable(new XmlSerializableReader(), xmlReader, xmlDataContract, isMemberType);
		}

		// Token: 0x06000F4C RID: 3916 RVA: 0x000376EC File Offset: 0x000358EC
		internal static object ReadIXmlSerializable(XmlSerializableReader xmlSerializableReader, XmlReaderDelegator xmlReader, XmlDataContract xmlDataContract, bool isMemberType)
		{
			xmlSerializableReader.BeginRead(xmlReader);
			if (isMemberType && !xmlDataContract.HasRoot)
			{
				xmlReader.Read();
				xmlReader.MoveToContent();
			}
			object result;
			if (xmlDataContract.UnderlyingType == Globals.TypeOfXmlElement)
			{
				if (!xmlReader.IsStartElement())
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.Element, xmlReader));
				}
				result = (XmlElement)new XmlDocument().ReadNode(xmlSerializableReader);
			}
			else if (xmlDataContract.UnderlyingType == Globals.TypeOfXmlNodeArray)
			{
				result = XmlSerializableServices.ReadNodes(xmlSerializableReader);
			}
			else
			{
				IXmlSerializable xmlSerializable = xmlDataContract.CreateXmlSerializableDelegate();
				xmlSerializable.ReadXml(xmlSerializableReader);
				result = xmlSerializable;
			}
			xmlSerializableReader.EndRead();
			return result;
		}

		// Token: 0x06000F4D RID: 3917 RVA: 0x0003778C File Offset: 0x0003598C
		public SerializationInfo ReadSerializationInfo(XmlReaderDelegator xmlReader, Type type)
		{
			SerializationInfo serializationInfo = new SerializationInfo(type, XmlObjectSerializer.FormatterConverter);
			XmlNodeType xmlNodeType;
			while ((xmlNodeType = xmlReader.MoveToContent()) != XmlNodeType.EndElement)
			{
				if (xmlNodeType != XmlNodeType.Element)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.Element, xmlReader));
				}
				if (xmlReader.NamespaceURI.Length != 0)
				{
					this.SkipUnknownElement(xmlReader);
				}
				else
				{
					string name = XmlConvert.DecodeName(xmlReader.LocalName);
					base.IncrementItemCount(1);
					this.ReadAttributes(xmlReader);
					object value;
					if (this.attributes.Ref != Globals.NewObjectId)
					{
						xmlReader.Skip();
						value = this.GetExistingObject(this.attributes.Ref, null, name, string.Empty);
					}
					else if (this.attributes.XsiNil)
					{
						xmlReader.Skip();
						value = null;
					}
					else
					{
						value = this.InternalDeserialize(xmlReader, Globals.TypeOfObject, name, string.Empty);
					}
					serializationInfo.AddValue(name, value);
				}
			}
			return serializationInfo;
		}

		// Token: 0x06000F4E RID: 3918 RVA: 0x00037868 File Offset: 0x00035A68
		protected virtual DataContract ResolveDataContractFromTypeName()
		{
			if (this.attributes.XsiTypeName != null)
			{
				return base.ResolveDataContractFromKnownTypes(this.attributes.XsiTypeName, this.attributes.XsiTypeNamespace, null, null);
			}
			return null;
		}

		// Token: 0x06000F4F RID: 3919 RVA: 0x00037898 File Offset: 0x00035A98
		private ExtensionDataMember ReadExtensionDataMember(XmlReaderDelegator xmlReader, int memberIndex)
		{
			ExtensionDataMember extensionDataMember = new ExtensionDataMember();
			extensionDataMember.Name = xmlReader.LocalName;
			extensionDataMember.Namespace = xmlReader.NamespaceURI;
			extensionDataMember.MemberIndex = memberIndex;
			if (xmlReader.UnderlyingExtensionDataReader != null)
			{
				extensionDataMember.Value = xmlReader.UnderlyingExtensionDataReader.GetCurrentNode();
			}
			else
			{
				extensionDataMember.Value = this.ReadExtensionDataValue(xmlReader);
			}
			return extensionDataMember;
		}

		// Token: 0x06000F50 RID: 3920 RVA: 0x000378F4 File Offset: 0x00035AF4
		public IDataNode ReadExtensionDataValue(XmlReaderDelegator xmlReader)
		{
			this.ReadAttributes(xmlReader);
			base.IncrementItemCount(1);
			IDataNode dataNode = null;
			if (this.attributes.Ref != Globals.NewObjectId)
			{
				xmlReader.Skip();
				object existingObjectOrExtensionData = this.GetExistingObjectOrExtensionData(this.attributes.Ref);
				IDataNode dataNode3;
				if (!(existingObjectOrExtensionData is IDataNode))
				{
					IDataNode dataNode2 = new DataNode<object>(existingObjectOrExtensionData);
					dataNode3 = dataNode2;
				}
				else
				{
					dataNode3 = (IDataNode)existingObjectOrExtensionData;
				}
				dataNode = dataNode3;
				dataNode.Id = this.attributes.Ref;
			}
			else if (this.attributes.XsiNil)
			{
				xmlReader.Skip();
				dataNode = null;
			}
			else
			{
				string dataContractName = null;
				string dataContractNamespace = null;
				if (this.attributes.XsiTypeName != null)
				{
					dataContractName = this.attributes.XsiTypeName;
					dataContractNamespace = this.attributes.XsiTypeNamespace;
				}
				if (this.IsReadingCollectionExtensionData(xmlReader))
				{
					XmlObjectSerializerReadContext.Read(xmlReader);
					dataNode = this.ReadUnknownCollectionData(xmlReader, dataContractName, dataContractNamespace);
				}
				else if (this.attributes.FactoryTypeName != null)
				{
					XmlObjectSerializerReadContext.Read(xmlReader);
					dataNode = this.ReadUnknownISerializableData(xmlReader, dataContractName, dataContractNamespace);
				}
				else if (this.IsReadingClassExtensionData(xmlReader))
				{
					XmlObjectSerializerReadContext.Read(xmlReader);
					dataNode = this.ReadUnknownClassData(xmlReader, dataContractName, dataContractNamespace);
				}
				else
				{
					DataContract dataContract = this.ResolveDataContractFromTypeName();
					if (dataContract == null)
					{
						dataNode = this.ReadExtensionDataValue(xmlReader, dataContractName, dataContractNamespace);
					}
					else if (dataContract is XmlDataContract)
					{
						dataNode = this.ReadUnknownXmlData(xmlReader, dataContractName, dataContractNamespace);
					}
					else if (dataContract.IsISerializable)
					{
						XmlObjectSerializerReadContext.Read(xmlReader);
						dataNode = this.ReadUnknownISerializableData(xmlReader, dataContractName, dataContractNamespace);
					}
					else if (dataContract is PrimitiveDataContract)
					{
						if (this.attributes.Id == Globals.NewObjectId)
						{
							XmlObjectSerializerReadContext.Read(xmlReader);
							xmlReader.MoveToContent();
							dataNode = this.ReadUnknownPrimitiveData(xmlReader, dataContract.UnderlyingType, dataContractName, dataContractNamespace);
							xmlReader.ReadEndElement();
						}
						else
						{
							dataNode = new DataNode<object>(xmlReader.ReadElementContentAsAnyType(dataContract.UnderlyingType));
							this.InitializeExtensionDataNode(dataNode, dataContractName, dataContractNamespace);
						}
					}
					else if (dataContract is EnumDataContract)
					{
						dataNode = new DataNode<object>(((EnumDataContract)dataContract).ReadEnumValue(xmlReader));
						this.InitializeExtensionDataNode(dataNode, dataContractName, dataContractNamespace);
					}
					else if (dataContract is ClassDataContract)
					{
						XmlObjectSerializerReadContext.Read(xmlReader);
						dataNode = this.ReadUnknownClassData(xmlReader, dataContractName, dataContractNamespace);
					}
					else if (dataContract is CollectionDataContract)
					{
						XmlObjectSerializerReadContext.Read(xmlReader);
						dataNode = this.ReadUnknownCollectionData(xmlReader, dataContractName, dataContractNamespace);
					}
				}
			}
			return dataNode;
		}

		// Token: 0x06000F51 RID: 3921 RVA: 0x000020AE File Offset: 0x000002AE
		protected virtual void StartReadExtensionDataValue(XmlReaderDelegator xmlReader)
		{
		}

		// Token: 0x06000F52 RID: 3922 RVA: 0x00037B30 File Offset: 0x00035D30
		private IDataNode ReadExtensionDataValue(XmlReaderDelegator xmlReader, string dataContractName, string dataContractNamespace)
		{
			this.StartReadExtensionDataValue(xmlReader);
			if (this.attributes.UnrecognizedAttributesFound)
			{
				return this.ReadUnknownXmlData(xmlReader, dataContractName, dataContractNamespace);
			}
			IDictionary<string, string> namespacesInScope = xmlReader.GetNamespacesInScope(XmlNamespaceScope.ExcludeXml);
			XmlObjectSerializerReadContext.Read(xmlReader);
			xmlReader.MoveToContent();
			XmlNodeType nodeType = xmlReader.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType == XmlNodeType.Text)
				{
					return this.ReadPrimitiveExtensionDataValue(xmlReader, dataContractName, dataContractNamespace);
				}
				if (nodeType != XmlNodeType.EndElement)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.Element, xmlReader));
				}
				IDataNode dataNode = this.ReadUnknownPrimitiveData(xmlReader, Globals.TypeOfObject, dataContractName, dataContractNamespace);
				xmlReader.ReadEndElement();
				dataNode.IsFinalValue = false;
				return dataNode;
			}
			else
			{
				if (xmlReader.NamespaceURI.StartsWith("http://schemas.datacontract.org/2004/07/", StringComparison.Ordinal))
				{
					return this.ReadUnknownClassData(xmlReader, dataContractName, dataContractNamespace);
				}
				return this.ReadAndResolveUnknownXmlData(xmlReader, namespacesInScope, dataContractName, dataContractNamespace);
			}
		}

		// Token: 0x06000F53 RID: 3923 RVA: 0x00037BE0 File Offset: 0x00035DE0
		protected virtual IDataNode ReadPrimitiveExtensionDataValue(XmlReaderDelegator xmlReader, string dataContractName, string dataContractNamespace)
		{
			Type valueType = xmlReader.ValueType;
			if (valueType == Globals.TypeOfString)
			{
				IDataNode dataNode = new DataNode<object>(xmlReader.ReadContentAsString());
				this.InitializeExtensionDataNode(dataNode, dataContractName, dataContractNamespace);
				dataNode.IsFinalValue = false;
				xmlReader.ReadEndElement();
				return dataNode;
			}
			IDataNode result = this.ReadUnknownPrimitiveData(xmlReader, valueType, dataContractName, dataContractNamespace);
			xmlReader.ReadEndElement();
			return result;
		}

		// Token: 0x06000F54 RID: 3924 RVA: 0x00037C38 File Offset: 0x00035E38
		protected void InitializeExtensionDataNode(IDataNode dataNode, string dataContractName, string dataContractNamespace)
		{
			dataNode.DataContractName = dataContractName;
			dataNode.DataContractNamespace = dataContractNamespace;
			dataNode.ClrAssemblyName = this.attributes.ClrAssembly;
			dataNode.ClrTypeName = this.attributes.ClrType;
			this.AddNewObject(dataNode);
			dataNode.Id = this.attributes.Id;
		}

		// Token: 0x06000F55 RID: 3925 RVA: 0x00037C90 File Offset: 0x00035E90
		private IDataNode ReadUnknownPrimitiveData(XmlReaderDelegator xmlReader, Type type, string dataContractName, string dataContractNamespace)
		{
			IDataNode dataNode = xmlReader.ReadExtensionData(type);
			this.InitializeExtensionDataNode(dataNode, dataContractName, dataContractNamespace);
			return dataNode;
		}

		// Token: 0x06000F56 RID: 3926 RVA: 0x00037CB0 File Offset: 0x00035EB0
		private ClassDataNode ReadUnknownClassData(XmlReaderDelegator xmlReader, string dataContractName, string dataContractNamespace)
		{
			ClassDataNode classDataNode = new ClassDataNode();
			this.InitializeExtensionDataNode(classDataNode, dataContractName, dataContractNamespace);
			int num = 0;
			XmlNodeType xmlNodeType;
			while ((xmlNodeType = xmlReader.MoveToContent()) != XmlNodeType.EndElement)
			{
				if (xmlNodeType != XmlNodeType.Element)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.Element, xmlReader));
				}
				if (classDataNode.Members == null)
				{
					classDataNode.Members = new List<ExtensionDataMember>();
				}
				classDataNode.Members.Add(this.ReadExtensionDataMember(xmlReader, num++));
			}
			xmlReader.ReadEndElement();
			return classDataNode;
		}

		// Token: 0x06000F57 RID: 3927 RVA: 0x00037D20 File Offset: 0x00035F20
		private CollectionDataNode ReadUnknownCollectionData(XmlReaderDelegator xmlReader, string dataContractName, string dataContractNamespace)
		{
			CollectionDataNode collectionDataNode = new CollectionDataNode();
			this.InitializeExtensionDataNode(collectionDataNode, dataContractName, dataContractNamespace);
			int arraySZSize = this.attributes.ArraySZSize;
			XmlNodeType xmlNodeType;
			while ((xmlNodeType = xmlReader.MoveToContent()) != XmlNodeType.EndElement)
			{
				if (xmlNodeType != XmlNodeType.Element)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.Element, xmlReader));
				}
				if (collectionDataNode.ItemName == null)
				{
					collectionDataNode.ItemName = xmlReader.LocalName;
					collectionDataNode.ItemNamespace = xmlReader.NamespaceURI;
				}
				if (xmlReader.IsStartElement(collectionDataNode.ItemName, collectionDataNode.ItemNamespace))
				{
					if (collectionDataNode.Items == null)
					{
						collectionDataNode.Items = new List<IDataNode>();
					}
					collectionDataNode.Items.Add(this.ReadExtensionDataValue(xmlReader));
				}
				else
				{
					this.SkipUnknownElement(xmlReader);
				}
			}
			xmlReader.ReadEndElement();
			if (arraySZSize != -1)
			{
				collectionDataNode.Size = arraySZSize;
				if (collectionDataNode.Items == null)
				{
					if (collectionDataNode.Size > 0)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Array size attribute is incorrect; must be between {0} and {1}.", new object[]
						{
							arraySZSize,
							0
						})));
					}
				}
				else if (collectionDataNode.Size != collectionDataNode.Items.Count)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Array size attribute is incorrect; must be between {0} and {1}.", new object[]
					{
						arraySZSize,
						collectionDataNode.Items.Count
					})));
				}
			}
			else if (collectionDataNode.Items != null)
			{
				collectionDataNode.Size = collectionDataNode.Items.Count;
			}
			else
			{
				collectionDataNode.Size = 0;
			}
			return collectionDataNode;
		}

		// Token: 0x06000F58 RID: 3928 RVA: 0x00037E8C File Offset: 0x0003608C
		private ISerializableDataNode ReadUnknownISerializableData(XmlReaderDelegator xmlReader, string dataContractName, string dataContractNamespace)
		{
			ISerializableDataNode serializableDataNode = new ISerializableDataNode();
			this.InitializeExtensionDataNode(serializableDataNode, dataContractName, dataContractNamespace);
			serializableDataNode.FactoryTypeName = this.attributes.FactoryTypeName;
			serializableDataNode.FactoryTypeNamespace = this.attributes.FactoryTypeNamespace;
			XmlNodeType xmlNodeType;
			while ((xmlNodeType = xmlReader.MoveToContent()) != XmlNodeType.EndElement)
			{
				if (xmlNodeType != XmlNodeType.Element)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.Element, xmlReader));
				}
				if (xmlReader.NamespaceURI.Length != 0)
				{
					this.SkipUnknownElement(xmlReader);
				}
				else
				{
					ISerializableDataMember serializableDataMember = new ISerializableDataMember();
					serializableDataMember.Name = xmlReader.LocalName;
					serializableDataMember.Value = this.ReadExtensionDataValue(xmlReader);
					if (serializableDataNode.Members == null)
					{
						serializableDataNode.Members = new List<ISerializableDataMember>();
					}
					serializableDataNode.Members.Add(serializableDataMember);
				}
			}
			xmlReader.ReadEndElement();
			return serializableDataNode;
		}

		// Token: 0x06000F59 RID: 3929 RVA: 0x00037F44 File Offset: 0x00036144
		private IDataNode ReadUnknownXmlData(XmlReaderDelegator xmlReader, string dataContractName, string dataContractNamespace)
		{
			XmlDataNode xmlDataNode = new XmlDataNode();
			this.InitializeExtensionDataNode(xmlDataNode, dataContractName, dataContractNamespace);
			xmlDataNode.OwnerDocument = this.Document;
			if (xmlReader.NodeType == XmlNodeType.EndElement)
			{
				return xmlDataNode;
			}
			IList<XmlAttribute> list = null;
			IList<XmlNode> list2 = null;
			if (xmlReader.MoveToContent() != XmlNodeType.Text)
			{
				while (xmlReader.MoveToNextAttribute())
				{
					string namespaceURI = xmlReader.NamespaceURI;
					if (namespaceURI != "http://schemas.microsoft.com/2003/10/Serialization/" && namespaceURI != "http://www.w3.org/2001/XMLSchema-instance")
					{
						if (list == null)
						{
							list = new List<XmlAttribute>();
						}
						list.Add((XmlAttribute)this.Document.ReadNode(xmlReader.UnderlyingReader));
					}
				}
				XmlObjectSerializerReadContext.Read(xmlReader);
			}
			while (xmlReader.MoveToContent() != XmlNodeType.EndElement)
			{
				if (xmlReader.EOF)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Unexpected end of file.")));
				}
				if (list2 == null)
				{
					list2 = new List<XmlNode>();
				}
				list2.Add(this.Document.ReadNode(xmlReader.UnderlyingReader));
			}
			xmlReader.ReadEndElement();
			xmlDataNode.XmlAttributes = list;
			xmlDataNode.XmlChildNodes = list2;
			return xmlDataNode;
		}

		// Token: 0x06000F5A RID: 3930 RVA: 0x0003803C File Offset: 0x0003623C
		private IDataNode ReadAndResolveUnknownXmlData(XmlReaderDelegator xmlReader, IDictionary<string, string> namespaces, string dataContractName, string dataContractNamespace)
		{
			bool flag = true;
			bool flag2 = true;
			bool flag3 = true;
			string strA = null;
			string text = null;
			IList<XmlNode> list = new List<XmlNode>();
			IList<XmlAttribute> list2 = null;
			if (namespaces == null)
			{
				goto IL_194;
			}
			list2 = new List<XmlAttribute>();
			using (IEnumerator<KeyValuePair<string, string>> enumerator = namespaces.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					KeyValuePair<string, string> keyValuePair = enumerator.Current;
					list2.Add(this.AddNamespaceDeclaration(keyValuePair.Key, keyValuePair.Value));
				}
				goto IL_194;
			}
			IL_6A:
			XmlNodeType nodeType;
			if (nodeType == XmlNodeType.Element)
			{
				string namespaceURI = xmlReader.NamespaceURI;
				string localName = xmlReader.LocalName;
				if (flag)
				{
					flag = (namespaceURI.Length == 0);
				}
				if (flag2)
				{
					if (text == null)
					{
						text = localName;
						strA = namespaceURI;
					}
					else
					{
						flag2 = (string.CompareOrdinal(text, localName) == 0 && string.CompareOrdinal(strA, namespaceURI) == 0);
					}
				}
			}
			else
			{
				if (xmlReader.EOF)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Unexpected end of file.")));
				}
				if (this.IsContentNode(xmlReader.NodeType))
				{
					flag = (flag3 = (flag2 = false));
				}
			}
			if (this.attributesInXmlData == null)
			{
				this.attributesInXmlData = new Attributes();
			}
			this.attributesInXmlData.Read(xmlReader);
			XmlNode xmlNode = this.Document.ReadNode(xmlReader.UnderlyingReader);
			list.Add(xmlNode);
			if (namespaces == null)
			{
				if (this.attributesInXmlData.XsiTypeName != null)
				{
					xmlNode.Attributes.Append(this.AddNamespaceDeclaration(this.attributesInXmlData.XsiTypePrefix, this.attributesInXmlData.XsiTypeNamespace));
				}
				if (this.attributesInXmlData.FactoryTypeName != null)
				{
					xmlNode.Attributes.Append(this.AddNamespaceDeclaration(this.attributesInXmlData.FactoryTypePrefix, this.attributesInXmlData.FactoryTypeNamespace));
				}
			}
			IL_194:
			if ((nodeType = xmlReader.NodeType) != XmlNodeType.EndElement)
			{
				goto IL_6A;
			}
			xmlReader.ReadEndElement();
			if (text != null && flag2)
			{
				return this.ReadUnknownCollectionData(this.CreateReaderOverChildNodes(list2, list), dataContractName, dataContractNamespace);
			}
			if (flag)
			{
				return this.ReadUnknownISerializableData(this.CreateReaderOverChildNodes(list2, list), dataContractName, dataContractNamespace);
			}
			if (flag3)
			{
				return this.ReadUnknownClassData(this.CreateReaderOverChildNodes(list2, list), dataContractName, dataContractNamespace);
			}
			XmlDataNode xmlDataNode = new XmlDataNode();
			this.InitializeExtensionDataNode(xmlDataNode, dataContractName, dataContractNamespace);
			xmlDataNode.OwnerDocument = this.Document;
			xmlDataNode.XmlChildNodes = list;
			xmlDataNode.XmlAttributes = list2;
			return xmlDataNode;
		}

		// Token: 0x06000F5B RID: 3931 RVA: 0x00038284 File Offset: 0x00036484
		private bool IsContentNode(XmlNodeType nodeType)
		{
			switch (nodeType)
			{
			case XmlNodeType.ProcessingInstruction:
			case XmlNodeType.Comment:
			case XmlNodeType.DocumentType:
			case XmlNodeType.Whitespace:
			case XmlNodeType.SignificantWhitespace:
				return false;
			}
			return true;
		}

		// Token: 0x06000F5C RID: 3932 RVA: 0x000382B4 File Offset: 0x000364B4
		internal XmlReaderDelegator CreateReaderOverChildNodes(IList<XmlAttribute> xmlAttributes, IList<XmlNode> xmlChildNodes)
		{
			XmlNode node = XmlObjectSerializerReadContext.CreateWrapperXmlElement(this.Document, xmlAttributes, xmlChildNodes, null, null, null);
			XmlReaderDelegator xmlReaderDelegator = this.CreateReaderDelegatorForReader(new XmlNodeReader(node));
			xmlReaderDelegator.MoveToContent();
			XmlObjectSerializerReadContext.Read(xmlReaderDelegator);
			return xmlReaderDelegator;
		}

		// Token: 0x06000F5D RID: 3933 RVA: 0x000382EC File Offset: 0x000364EC
		internal static XmlNode CreateWrapperXmlElement(XmlDocument document, IList<XmlAttribute> xmlAttributes, IList<XmlNode> xmlChildNodes, string prefix, string localName, string ns)
		{
			localName = (localName ?? "wrapper");
			ns = (ns ?? string.Empty);
			XmlNode xmlNode = document.CreateElement(prefix, localName, ns);
			if (xmlAttributes != null)
			{
				for (int i = 0; i < xmlAttributes.Count; i++)
				{
					xmlNode.Attributes.Append(xmlAttributes[i]);
				}
			}
			if (xmlChildNodes != null)
			{
				for (int j = 0; j < xmlChildNodes.Count; j++)
				{
					xmlNode.AppendChild(xmlChildNodes[j]);
				}
			}
			return xmlNode;
		}

		// Token: 0x06000F5E RID: 3934 RVA: 0x0003836C File Offset: 0x0003656C
		private XmlAttribute AddNamespaceDeclaration(string prefix, string ns)
		{
			XmlAttribute xmlAttribute = (prefix == null || prefix.Length == 0) ? this.Document.CreateAttribute(null, "xmlns", "http://www.w3.org/2000/xmlns/") : this.Document.CreateAttribute("xmlns", prefix, "http://www.w3.org/2000/xmlns/");
			xmlAttribute.Value = ns;
			return xmlAttribute;
		}

		// Token: 0x06000F5F RID: 3935 RVA: 0x000383B9 File Offset: 0x000365B9
		public static Exception CreateUnexpectedStateException(XmlNodeType expectedState, XmlReaderDelegator xmlReader)
		{
			return XmlObjectSerializer.CreateSerializationExceptionWithReaderDetails(SR.GetString("Expecting state '{0}'.", new object[]
			{
				expectedState
			}), xmlReader);
		}

		// Token: 0x06000F60 RID: 3936 RVA: 0x000383DA File Offset: 0x000365DA
		protected virtual object ReadDataContractValue(DataContract dataContract, XmlReaderDelegator reader)
		{
			return dataContract.ReadXmlValue(reader, this);
		}

		// Token: 0x06000F61 RID: 3937 RVA: 0x000383E4 File Offset: 0x000365E4
		protected virtual XmlReaderDelegator CreateReaderDelegatorForReader(XmlReader xmlReader)
		{
			return new XmlReaderDelegator(xmlReader);
		}

		// Token: 0x06000F62 RID: 3938 RVA: 0x000383EC File Offset: 0x000365EC
		protected virtual bool IsReadingCollectionExtensionData(XmlReaderDelegator xmlReader)
		{
			return this.attributes.ArraySZSize != -1;
		}

		// Token: 0x06000F63 RID: 3939 RVA: 0x0000310F File Offset: 0x0000130F
		protected virtual bool IsReadingClassExtensionData(XmlReaderDelegator xmlReader)
		{
			return false;
		}

		// Token: 0x040006CB RID: 1739
		internal Attributes attributes;

		// Token: 0x040006CC RID: 1740
		private HybridObjectCache deserializedObjects;

		// Token: 0x040006CD RID: 1741
		private XmlSerializableReader xmlSerializableReader;

		// Token: 0x040006CE RID: 1742
		private XmlDocument xmlDocument;

		// Token: 0x040006CF RID: 1743
		private Attributes attributesInXmlData;

		// Token: 0x040006D0 RID: 1744
		private XmlReaderDelegator extensionDataReader;

		// Token: 0x040006D1 RID: 1745
		private object getOnlyCollectionValue;

		// Token: 0x040006D2 RID: 1746
		private bool isGetOnlyCollection;
	}
}
