﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000B0 RID: 176
	internal enum CollectionKind : byte
	{
		// Token: 0x0400041D RID: 1053
		None,
		// Token: 0x0400041E RID: 1054
		GenericDictionary,
		// Token: 0x0400041F RID: 1055
		Dictionary,
		// Token: 0x04000420 RID: 1056
		GenericList,
		// Token: 0x04000421 RID: 1057
		GenericCollection,
		// Token: 0x04000422 RID: 1058
		List,
		// Token: 0x04000423 RID: 1059
		GenericEnumerable,
		// Token: 0x04000424 RID: 1060
		Collection,
		// Token: 0x04000425 RID: 1061
		Enumerable,
		// Token: 0x04000426 RID: 1062
		Array
	}
}
