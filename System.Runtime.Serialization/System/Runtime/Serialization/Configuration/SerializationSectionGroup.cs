﻿using System;
using System.Configuration;
using Unity;

namespace System.Runtime.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure serialization by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />.</summary>
	// Token: 0x020001AF RID: 431
	public sealed class SerializationSectionGroup : ConfigurationSectionGroup
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.SerializationSectionGroup" /> class.  </summary>
		// Token: 0x06001477 RID: 5239 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public SerializationSectionGroup()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Runtime.Serialization.Configuration.DataContractSerializerSection" /> used to set up the known types collection. </summary>
		/// <returns>The <see cref="T:System.Runtime.Serialization.Configuration.DataContractSerializerSection" /> used for the serialization configuration section.</returns>
		// Token: 0x17000439 RID: 1081
		// (get) Token: 0x06001478 RID: 5240 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public DataContractSerializerSection DataContractSerializer
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Runtime.Serialization.Configuration.NetDataContractSerializerSection" /> used to set up the known types collection. </summary>
		/// <returns>The <see cref="T:System.Runtime.Serialization.Configuration.NetDataContractSerializerSection" /> object.</returns>
		// Token: 0x1700043A RID: 1082
		// (get) Token: 0x06001479 RID: 5241 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public NetDataContractSerializerSection NetDataContractSerializer
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the serialization configuration section for the specified configuration.</summary>
		/// <param name="config">A <see cref="T:System.Configuration.Configuration" /> that represents the configuration to retrieve.</param>
		/// <returns>A <see cref="T:System.Runtime.Serialization.Configuration.SerializationSectionGroup" /> that represents the configuration section.</returns>
		// Token: 0x0600147A RID: 5242 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public static SerializationSectionGroup GetSectionGroup(Configuration config)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
