﻿using System;
using System.Configuration;
using Unity;

namespace System.Runtime.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure serialization by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />.</summary>
	// Token: 0x020001AC RID: 428
	[ConfigurationCollection(typeof(ParameterElement), AddItemName = "parameter", CollectionType = ConfigurationElementCollectionType.BasicMap)]
	public sealed class ParameterElementCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.ParameterElementCollection" /> class.  </summary>
		// Token: 0x0600145C RID: 5212 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public ParameterElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the type of the parameters collection in configuration.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationElementCollectionType" /> that contains the type of the parameters collection in configuration.</returns>
		// Token: 0x17000430 RID: 1072
		// (get) Token: 0x0600145D RID: 5213 RVA: 0x0004AE2C File Offset: 0x0004902C
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConfigurationElementCollectionType.BasicMap;
			}
		}

		// Token: 0x17000431 RID: 1073
		// (get) Token: 0x0600145E RID: 5214 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override string ElementName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the element in the collection at the specified position. </summary>
		/// <param name="index">The position of the element in the collection to get or set.</param>
		/// <returns>A <see cref="T:System.Runtime.Serialization.Configuration.ParameterElement" /> from the collection.</returns>
		// Token: 0x17000432 RID: 1074
		public ParameterElement this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds an element to the collection of parameter elements.</summary>
		/// <param name="element">The <see cref="T:System.Runtime.Serialization.Configuration.ParameterElement" /> element to add to the collection.</param>
		// Token: 0x06001461 RID: 5217 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Add(ParameterElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all members of the collection.</summary>
		// Token: 0x06001462 RID: 5218 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a value specifying whether the named type is found in the collection.</summary>
		/// <param name="typeName">The name of the type to find.</param>
		/// <returns>
		///     <see langword="true" /> if the element is present; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001463 RID: 5219 RVA: 0x0004AE48 File Offset: 0x00049048
		public bool Contains(string typeName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06001464 RID: 5220 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06001465 RID: 5221 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the position of the specified element in the collection.</summary>
		/// <param name="element">The <see cref="T:System.Runtime.Serialization.Configuration.ParameterElement" /> element to find.</param>
		/// <returns>The position of the specified element.</returns>
		// Token: 0x06001466 RID: 5222 RVA: 0x0004AE64 File Offset: 0x00049064
		public int IndexOf(ParameterElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Removes the specified element from the collection.</summary>
		/// <param name="element">The <see cref="T:System.Runtime.Serialization.Configuration.ParameterElement" /> to remove.</param>
		// Token: 0x06001467 RID: 5223 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Remove(ParameterElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element at the specified position.</summary>
		/// <param name="index">The position of the element to remove.</param>
		// Token: 0x06001468 RID: 5224 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
