﻿using System;
using System.Configuration;
using Unity;

namespace System.Runtime.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure serialization by the <see cref="T:System.Runtime.Serialization.NetDataContractSerializer" />.</summary>
	// Token: 0x020001AE RID: 430
	public sealed class NetDataContractSerializerSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.NetDataContractSerializerSection" /> class.</summary>
		// Token: 0x06001474 RID: 5236 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public NetDataContractSerializerSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates whether unsafe type forwarding is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if unsafe type forwarding is enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000437 RID: 1079
		// (get) Token: 0x06001475 RID: 5237 RVA: 0x0004AE9C File Offset: 0x0004909C
		public bool EnableUnsafeTypeForwarding
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		// Token: 0x17000438 RID: 1080
		// (get) Token: 0x06001476 RID: 5238 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
