﻿using System;
using System.Configuration;
using System.Xml;
using Unity;

namespace System.Runtime.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure XML serialization by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />.</summary>
	// Token: 0x020001AD RID: 429
	public sealed class ParameterElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.ParameterElement" /> class.  </summary>
		// Token: 0x06001469 RID: 5225 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public ParameterElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.ParameterElement" /> class with the specified index. </summary>
		/// <param name="index">Specifies a position in the collection of parameters.</param>
		// Token: 0x0600146A RID: 5226 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public ParameterElement(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.ParameterElement" /> class with the specified type name.</summary>
		/// <param name="typeName">The name of the parameter's type.</param>
		// Token: 0x0600146B RID: 5227 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public ParameterElement(string typeName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the position of the generic known type.</summary>
		/// <returns>The position of the parameter in the containing generic declared type.</returns>
		// Token: 0x17000433 RID: 1075
		// (get) Token: 0x0600146C RID: 5228 RVA: 0x0004AE80 File Offset: 0x00049080
		// (set) Token: 0x0600146D RID: 5229 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public int Index
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of parameters.</summary>
		/// <returns>A <see cref="T:System.Runtime.Serialization.Configuration.ParameterElementCollection" /> that contains all parameters.</returns>
		// Token: 0x17000434 RID: 1076
		// (get) Token: 0x0600146E RID: 5230 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public ParameterElementCollection Parameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000435 RID: 1077
		// (get) Token: 0x0600146F RID: 5231 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the type name of the parameter of the generic known type.</summary>
		/// <returns>The type name of the parameter.</returns>
		// Token: 0x17000436 RID: 1078
		// (get) Token: 0x06001470 RID: 5232 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		// (set) Token: 0x06001471 RID: 5233 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public string Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x06001472 RID: 5234 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001473 RID: 5235 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		protected override void PreSerialize(XmlWriter writer)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
