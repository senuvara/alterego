﻿using System;
using System.Configuration;
using System.Security;
using Unity;

namespace System.Runtime.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to add known types that are used for serialization by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />.</summary>
	// Token: 0x020001A9 RID: 425
	public sealed class DeclaredTypeElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.DeclaredTypeElement" /> class.  </summary>
		// Token: 0x06001440 RID: 5184 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public DeclaredTypeElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.DeclaredTypeElement" /> class with the specified type name.</summary>
		/// <param name="typeName">The name of the type that requires a collection of known types.</param>
		// Token: 0x06001441 RID: 5185 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public DeclaredTypeElement(string typeName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the collection of known types.</summary>
		/// <returns>A <see cref="T:System.Runtime.Serialization.Configuration.TypeElementCollection" /> that contains the known types.</returns>
		// Token: 0x17000426 RID: 1062
		// (get) Token: 0x06001442 RID: 5186 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public TypeElementCollection KnownTypes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000427 RID: 1063
		// (get) Token: 0x06001443 RID: 5187 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the name of the declared type that requires a collection of known types. </summary>
		/// <returns>The name of the declared type.</returns>
		// Token: 0x17000428 RID: 1064
		// (get) Token: 0x06001444 RID: 5188 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		// (set) Token: 0x06001445 RID: 5189 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public string Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x06001446 RID: 5190 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		[SecuritySafeCritical]
		protected override void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
