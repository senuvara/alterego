﻿using System;
using System.Configuration;
using Unity;

namespace System.Runtime.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure the known types used for serialization by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />.</summary>
	// Token: 0x020001AA RID: 426
	[ConfigurationCollection(typeof(TypeElement), CollectionType = ConfigurationElementCollectionType.BasicMap)]
	public sealed class TypeElementCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.TypeElementCollection" /> class.  </summary>
		// Token: 0x06001447 RID: 5191 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public TypeElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the collection of elements that represents the types using known types.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationElementCollectionType" /> that contains the element objects.</returns>
		// Token: 0x17000429 RID: 1065
		// (get) Token: 0x06001448 RID: 5192 RVA: 0x0004ADD8 File Offset: 0x00048FD8
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConfigurationElementCollectionType.BasicMap;
			}
		}

		// Token: 0x1700042A RID: 1066
		// (get) Token: 0x06001449 RID: 5193 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override string ElementName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Returns a specific member of the collection by its position.</summary>
		/// <param name="index">The position of the item to return.</param>
		/// <returns>The element at the specified position.</returns>
		// Token: 0x1700042B RID: 1067
		public TypeElement this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds the specified element to the collection.</summary>
		/// <param name="element">A <see cref="T:System.Runtime.Serialization.Configuration.TypeElement" /> that represents the known type to add. </param>
		// Token: 0x0600144C RID: 5196 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Add(TypeElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all members of the collection.</summary>
		// Token: 0x0600144D RID: 5197 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600144E RID: 5198 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600144F RID: 5199 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the position of the specified element.</summary>
		/// <param name="element">The <see cref="T:System.Runtime.Serialization.Configuration.TypeElement" /> to find in the collection.</param>
		/// <returns>The position of the specified element.</returns>
		// Token: 0x06001450 RID: 5200 RVA: 0x0004ADF4 File Offset: 0x00048FF4
		public int IndexOf(TypeElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Removes the specified element from the collection.</summary>
		/// <param name="element">The <see cref="T:System.Runtime.Serialization.Configuration.TypeElement" /> to remove.</param>
		// Token: 0x06001451 RID: 5201 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Remove(TypeElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element at the specified position.</summary>
		/// <param name="index">The position in the collection from which to remove the element.</param>
		// Token: 0x06001452 RID: 5202 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
