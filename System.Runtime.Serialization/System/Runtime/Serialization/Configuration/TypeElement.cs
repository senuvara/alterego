﻿using System;
using System.Configuration;
using Unity;

namespace System.Runtime.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure serialization by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />.</summary>
	// Token: 0x020001AB RID: 427
	public sealed class TypeElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.TypeElement" /> class.  </summary>
		// Token: 0x06001453 RID: 5203 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public TypeElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.TypeElement" /> class with the specified type name. </summary>
		/// <param name="typeName">The name of the type that uses known types.</param>
		// Token: 0x06001454 RID: 5204 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public TypeElement(string typeName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the position of the element.</summary>
		/// <returns>The position of the element.</returns>
		// Token: 0x1700042C RID: 1068
		// (get) Token: 0x06001455 RID: 5205 RVA: 0x0004AE10 File Offset: 0x00049010
		// (set) Token: 0x06001456 RID: 5206 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public int Index
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a collection of parameters.</summary>
		/// <returns>A <see cref="T:System.Runtime.Serialization.Configuration.ParameterElementCollection" /> that contains the parameters for the type. </returns>
		// Token: 0x1700042D RID: 1069
		// (get) Token: 0x06001457 RID: 5207 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public ParameterElementCollection Parameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x1700042E RID: 1070
		// (get) Token: 0x06001458 RID: 5208 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the name of the type.</summary>
		/// <returns>The name of the type.</returns>
		// Token: 0x1700042F RID: 1071
		// (get) Token: 0x06001459 RID: 5209 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		// (set) Token: 0x0600145A RID: 5210 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public string Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x0600145B RID: 5211 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		protected override void Reset(ConfigurationElement parentElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
