﻿using System;
using System.Configuration;
using Unity;

namespace System.Runtime.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure XML serialization using the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />.</summary>
	// Token: 0x020001A8 RID: 424
	[ConfigurationCollection(typeof(DeclaredTypeElement))]
	public sealed class DeclaredTypeElementCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.DeclaredTypeElementCollection" /> class. </summary>
		// Token: 0x06001432 RID: 5170 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public DeclaredTypeElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001433 RID: 5171 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public DeclaredTypeElement get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06001434 RID: 5172 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void set_Item(int index, DeclaredTypeElement value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the element in the collection of types by its key.</summary>
		/// <param name="typeName">The name (that functions as a key) of the type to get or set.</param>
		/// <returns>The specified element (when used to get the element).</returns>
		// Token: 0x17000425 RID: 1061
		public DeclaredTypeElement this[string typeName]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds a specified configuration element to the collection.</summary>
		/// <param name="element">The configuration element to add.</param>
		// Token: 0x06001437 RID: 5175 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Add(DeclaredTypeElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all members of the collection.</summary>
		// Token: 0x06001438 RID: 5176 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns a value that specifies whether the element is in the collection.</summary>
		/// <param name="typeName">The name of the type to check for.</param>
		/// <returns>
		///     <see langword="true" /> if the element is in the collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001439 RID: 5177 RVA: 0x0004ADA0 File Offset: 0x00048FA0
		public bool Contains(string typeName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x0600143A RID: 5178 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600143B RID: 5179 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the position of the specified configuration element.</summary>
		/// <param name="element">The element to find in the collection.</param>
		/// <returns>The index of the specified configuration element; otherwise, -1.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="element" /> argument is <see langword="null" />.</exception>
		// Token: 0x0600143C RID: 5180 RVA: 0x0004ADBC File Offset: 0x00048FBC
		public int IndexOf(DeclaredTypeElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Removes the specified configuration element from the collection.</summary>
		/// <param name="element">The <see cref="T:System.Runtime.Serialization.Configuration.DeclaredTypeElement" /> to remove.</param>
		// Token: 0x0600143D RID: 5181 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Remove(DeclaredTypeElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the element specified by its key from the collection.</summary>
		/// <param name="typeName">The name of the type (which functions as a key) to remove from the collection.</param>
		// Token: 0x0600143E RID: 5182 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void Remove(string typeName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the configuration element found at the specified position.</summary>
		/// <param name="index">The position of the configuration element to remove.</param>
		// Token: 0x0600143F RID: 5183 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
