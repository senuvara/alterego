﻿using System;
using System.Configuration;
using Unity;

namespace System.Runtime.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure serialization by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />.</summary>
	// Token: 0x020001A7 RID: 423
	public sealed class DataContractSerializerSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Configuration.DataContractSerializerSection" /> class. </summary>
		// Token: 0x0600142F RID: 5167 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		public DataContractSerializerSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a collection of types added to the <see cref="P:System.Runtime.Serialization.DataContractSerializer.KnownTypes" /> property.</summary>
		/// <returns>A <see cref="T:System.Runtime.Serialization.Configuration.DeclaredTypeElementCollection" /> that contains the known types.</returns>
		// Token: 0x17000423 RID: 1059
		// (get) Token: 0x06001430 RID: 5168 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		public DeclaredTypeElementCollection DeclaredTypes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x17000424 RID: 1060
		// (get) Token: 0x06001431 RID: 5169 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
