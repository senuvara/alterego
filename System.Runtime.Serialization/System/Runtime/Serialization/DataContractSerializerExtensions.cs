﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Runtime.Serialization
{
	/// <summary>Extends the <see cref="T:System.Runtime.Serialization.DataContractSerializer" /> class by providing methods for setting and getting an <see cref="T:System.Runtime.Serialization.ISerializationSurrogateProvider" />.</summary>
	// Token: 0x020001A4 RID: 420
	[EditorBrowsable(EditorBrowsableState.Never)]
	public static class DataContractSerializerExtensions
	{
		/// <summary>Returns the surrogate serialization provider for this serializer.</summary>
		/// <param name="serializer">The serializer which is being surrogated.</param>
		/// <returns>The surrogate serializer.</returns>
		// Token: 0x0600140D RID: 5133 RVA: 0x0004ACA8 File Offset: 0x00048EA8
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static ISerializationSurrogateProvider GetSerializationSurrogateProvider(this DataContractSerializer serializer)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Specifies a surrogate serialization provider for this <see cref="T:System.Runtime.Serialization.DataContractSerializer" />.</summary>
		/// <param name="serializer">The serializer which is being surrogated.</param>
		/// <param name="provider">The surrogate serialization provider.</param>
		// Token: 0x0600140E RID: 5134 RVA: 0x0004ACB0 File Offset: 0x00048EB0
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void SetSerializationSurrogateProvider(this DataContractSerializer serializer, ISerializationSurrogateProvider provider)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
