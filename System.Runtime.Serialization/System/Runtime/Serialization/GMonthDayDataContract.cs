﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000108 RID: 264
	internal class GMonthDayDataContract : StringDataContract
	{
		// Token: 0x06000DB8 RID: 3512 RVA: 0x000337E2 File Offset: 0x000319E2
		internal GMonthDayDataContract() : base(DictionaryGlobals.gMonthDayLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
