﻿using System;
using System.Globalization;

namespace System.Runtime.Serialization
{
	// Token: 0x020000D3 RID: 211
	internal class DataNode<T> : IDataNode
	{
		// Token: 0x06000BE4 RID: 3044 RVA: 0x0002F01F File Offset: 0x0002D21F
		internal DataNode()
		{
			this.dataType = typeof(T);
			this.isFinalValue = true;
		}

		// Token: 0x06000BE5 RID: 3045 RVA: 0x0002F049 File Offset: 0x0002D249
		internal DataNode(T value) : this()
		{
			this.value = value;
		}

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x06000BE6 RID: 3046 RVA: 0x0002F058 File Offset: 0x0002D258
		public Type DataType
		{
			get
			{
				return this.dataType;
			}
		}

		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06000BE7 RID: 3047 RVA: 0x0002F060 File Offset: 0x0002D260
		// (set) Token: 0x06000BE8 RID: 3048 RVA: 0x0002F06D File Offset: 0x0002D26D
		public object Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = (T)((object)value);
			}
		}

		// Token: 0x17000213 RID: 531
		// (get) Token: 0x06000BE9 RID: 3049 RVA: 0x0002F07B File Offset: 0x0002D27B
		// (set) Token: 0x06000BEA RID: 3050 RVA: 0x0002F083 File Offset: 0x0002D283
		bool IDataNode.IsFinalValue
		{
			get
			{
				return this.isFinalValue;
			}
			set
			{
				this.isFinalValue = value;
			}
		}

		// Token: 0x06000BEB RID: 3051 RVA: 0x0002F08C File Offset: 0x0002D28C
		public T GetValue()
		{
			return this.value;
		}

		// Token: 0x17000214 RID: 532
		// (get) Token: 0x06000BEC RID: 3052 RVA: 0x0002F094 File Offset: 0x0002D294
		// (set) Token: 0x06000BED RID: 3053 RVA: 0x0002F09C File Offset: 0x0002D29C
		public string DataContractName
		{
			get
			{
				return this.dataContractName;
			}
			set
			{
				this.dataContractName = value;
			}
		}

		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06000BEE RID: 3054 RVA: 0x0002F0A5 File Offset: 0x0002D2A5
		// (set) Token: 0x06000BEF RID: 3055 RVA: 0x0002F0AD File Offset: 0x0002D2AD
		public string DataContractNamespace
		{
			get
			{
				return this.dataContractNamespace;
			}
			set
			{
				this.dataContractNamespace = value;
			}
		}

		// Token: 0x17000216 RID: 534
		// (get) Token: 0x06000BF0 RID: 3056 RVA: 0x0002F0B6 File Offset: 0x0002D2B6
		// (set) Token: 0x06000BF1 RID: 3057 RVA: 0x0002F0BE File Offset: 0x0002D2BE
		public string ClrTypeName
		{
			get
			{
				return this.clrTypeName;
			}
			set
			{
				this.clrTypeName = value;
			}
		}

		// Token: 0x17000217 RID: 535
		// (get) Token: 0x06000BF2 RID: 3058 RVA: 0x0002F0C7 File Offset: 0x0002D2C7
		// (set) Token: 0x06000BF3 RID: 3059 RVA: 0x0002F0CF File Offset: 0x0002D2CF
		public string ClrAssemblyName
		{
			get
			{
				return this.clrAssemblyName;
			}
			set
			{
				this.clrAssemblyName = value;
			}
		}

		// Token: 0x17000218 RID: 536
		// (get) Token: 0x06000BF4 RID: 3060 RVA: 0x0002F0D8 File Offset: 0x0002D2D8
		public bool PreservesReferences
		{
			get
			{
				return this.Id != Globals.NewObjectId;
			}
		}

		// Token: 0x17000219 RID: 537
		// (get) Token: 0x06000BF5 RID: 3061 RVA: 0x0002F0EA File Offset: 0x0002D2EA
		// (set) Token: 0x06000BF6 RID: 3062 RVA: 0x0002F0F2 File Offset: 0x0002D2F2
		public string Id
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;
			}
		}

		// Token: 0x06000BF7 RID: 3063 RVA: 0x0002F0FC File Offset: 0x0002D2FC
		public virtual void GetData(ElementData element)
		{
			element.dataNode = this;
			element.attributeCount = 0;
			element.childElementIndex = 0;
			if (this.DataContractName != null)
			{
				this.AddQualifiedNameAttribute(element, "i", "type", "http://www.w3.org/2001/XMLSchema-instance", this.DataContractName, this.DataContractNamespace);
			}
			if (this.ClrTypeName != null)
			{
				element.AddAttribute("z", "http://schemas.microsoft.com/2003/10/Serialization/", "Type", this.ClrTypeName);
			}
			if (this.ClrAssemblyName != null)
			{
				element.AddAttribute("z", "http://schemas.microsoft.com/2003/10/Serialization/", "Assembly", this.ClrAssemblyName);
			}
		}

		// Token: 0x06000BF8 RID: 3064 RVA: 0x0002F190 File Offset: 0x0002D390
		public virtual void Clear()
		{
			this.clrTypeName = (this.clrAssemblyName = null);
		}

		// Token: 0x06000BF9 RID: 3065 RVA: 0x0002F1B0 File Offset: 0x0002D3B0
		internal void AddQualifiedNameAttribute(ElementData element, string elementPrefix, string elementName, string elementNs, string valueName, string valueNs)
		{
			string prefix = ExtensionDataReader.GetPrefix(valueNs);
			element.AddAttribute(elementPrefix, elementNs, elementName, string.Format(CultureInfo.InvariantCulture, "{0}:{1}", prefix, valueName));
			bool flag = false;
			if (element.attributes != null)
			{
				for (int i = 0; i < element.attributes.Length; i++)
				{
					AttributeData attributeData = element.attributes[i];
					if (attributeData != null && attributeData.prefix == "xmlns" && attributeData.localName == prefix)
					{
						flag = true;
						break;
					}
				}
			}
			if (!flag)
			{
				element.AddAttribute("xmlns", "http://www.w3.org/2000/xmlns/", prefix, valueNs);
			}
		}

		// Token: 0x0400050A RID: 1290
		protected Type dataType;

		// Token: 0x0400050B RID: 1291
		private T value;

		// Token: 0x0400050C RID: 1292
		private string dataContractName;

		// Token: 0x0400050D RID: 1293
		private string dataContractNamespace;

		// Token: 0x0400050E RID: 1294
		private string clrTypeName;

		// Token: 0x0400050F RID: 1295
		private string clrAssemblyName;

		// Token: 0x04000510 RID: 1296
		private string id = Globals.NewObjectId;

		// Token: 0x04000511 RID: 1297
		private bool isFinalValue;
	}
}
