﻿using System;

namespace System.Runtime.Serialization
{
	/// <summary>Specifies that the type defines or implements a data contract and is serializable by a serializer, such as the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />. To make their type serializable, type authors must define a data contract for their type. </summary>
	// Token: 0x020000C0 RID: 192
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum, Inherited = false, AllowMultiple = false)]
	public sealed class DataContractAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> class. </summary>
		// Token: 0x06000ACE RID: 2766 RVA: 0x000254EB File Offset: 0x000236EB
		public DataContractAttribute()
		{
		}

		/// <summary>Gets or sets a value that indicates whether to preserve object reference data.</summary>
		/// <returns>
		///     <see langword="true" /> to keep object reference data using standard XML; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x06000ACF RID: 2767 RVA: 0x0002C6EA File Offset: 0x0002A8EA
		// (set) Token: 0x06000AD0 RID: 2768 RVA: 0x0002C6F2 File Offset: 0x0002A8F2
		public bool IsReference
		{
			get
			{
				return this.isReference;
			}
			set
			{
				this.isReference = value;
				this.isReferenceSetExplicitly = true;
			}
		}

		/// <summary>Gets whether <see cref="P:System.Runtime.Serialization.DataContractAttribute.IsReference" /> has been explicitly set.</summary>
		/// <returns>
		///     <see langword="true" /> if the reference has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06000AD1 RID: 2769 RVA: 0x0002C702 File Offset: 0x0002A902
		public bool IsReferenceSetExplicitly
		{
			get
			{
				return this.isReferenceSetExplicitly;
			}
		}

		/// <summary>Gets or sets the namespace for the data contract for the type.</summary>
		/// <returns>The namespace of the contract. </returns>
		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06000AD2 RID: 2770 RVA: 0x0002C70A File Offset: 0x0002A90A
		// (set) Token: 0x06000AD3 RID: 2771 RVA: 0x0002C712 File Offset: 0x0002A912
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
				this.isNamespaceSetExplicitly = true;
			}
		}

		/// <summary>Gets whether <see cref="P:System.Runtime.Serialization.DataContractAttribute.Namespace" /> has been explicitly set.</summary>
		/// <returns>Returns <see langword="true" /> if the namespace has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06000AD4 RID: 2772 RVA: 0x0002C722 File Offset: 0x0002A922
		public bool IsNamespaceSetExplicitly
		{
			get
			{
				return this.isNamespaceSetExplicitly;
			}
		}

		/// <summary>Gets or sets the name of the data contract for the type.</summary>
		/// <returns>The local name of a data contract. The default is the name of the class that the attribute is applied to. </returns>
		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x06000AD5 RID: 2773 RVA: 0x0002C72A File Offset: 0x0002A92A
		// (set) Token: 0x06000AD6 RID: 2774 RVA: 0x0002C732 File Offset: 0x0002A932
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
				this.isNameSetExplicitly = true;
			}
		}

		/// <summary>Gets whether <see cref="P:System.Runtime.Serialization.DataContractAttribute.Name" /> has been explicitly set.</summary>
		/// <returns>Returns <see langword="true" /> if the name has been explicitly set; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x06000AD7 RID: 2775 RVA: 0x0002C742 File Offset: 0x0002A942
		public bool IsNameSetExplicitly
		{
			get
			{
				return this.isNameSetExplicitly;
			}
		}

		// Token: 0x0400047B RID: 1147
		private string name;

		// Token: 0x0400047C RID: 1148
		private string ns;

		// Token: 0x0400047D RID: 1149
		private bool isNameSetExplicitly;

		// Token: 0x0400047E RID: 1150
		private bool isNamespaceSetExplicitly;

		// Token: 0x0400047F RID: 1151
		private bool isReference;

		// Token: 0x04000480 RID: 1152
		private bool isReferenceSetExplicitly;
	}
}
