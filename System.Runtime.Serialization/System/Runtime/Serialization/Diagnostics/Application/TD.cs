﻿using System;
using System.Globalization;
using System.Resources;
using System.Runtime.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Diagnostics.Application
{
	// Token: 0x02000195 RID: 405
	internal class TD
	{
		// Token: 0x060013DD RID: 5085 RVA: 0x00002217 File Offset: 0x00000417
		private TD()
		{
		}

		// Token: 0x17000416 RID: 1046
		// (get) Token: 0x060013DE RID: 5086 RVA: 0x0004A3A1 File Offset: 0x000485A1
		private static ResourceManager ResourceManager
		{
			get
			{
				if (TD.resourceManager == null)
				{
					TD.resourceManager = new ResourceManager("System.Runtime.Serialization.Diagnostics.Application.TD", typeof(TD).Assembly);
				}
				return TD.resourceManager;
			}
		}

		// Token: 0x17000417 RID: 1047
		// (get) Token: 0x060013DF RID: 5087 RVA: 0x0004A3CD File Offset: 0x000485CD
		// (set) Token: 0x060013E0 RID: 5088 RVA: 0x0004A3D4 File Offset: 0x000485D4
		internal static CultureInfo Culture
		{
			get
			{
				return TD.resourceCulture;
			}
			set
			{
				TD.resourceCulture = value;
			}
		}

		// Token: 0x060013E1 RID: 5089 RVA: 0x0004A3DC File Offset: 0x000485DC
		internal static bool ReaderQuotaExceededIsEnabled()
		{
			return FxTrace.ShouldTraceError && TD.IsEtwEventEnabled(0);
		}

		// Token: 0x060013E2 RID: 5090 RVA: 0x0004A3F0 File Offset: 0x000485F0
		internal static void ReaderQuotaExceeded(string param0)
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(0))
			{
				TD.WriteEtwEvent(0, null, param0, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013E3 RID: 5091 RVA: 0x0004A423 File Offset: 0x00048623
		internal static bool DCSerializeWithSurrogateStartIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(1);
		}

		// Token: 0x060013E4 RID: 5092 RVA: 0x0004A434 File Offset: 0x00048634
		internal static void DCSerializeWithSurrogateStart(string SurrogateType)
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(1))
			{
				TD.WriteEtwEvent(1, null, SurrogateType, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013E5 RID: 5093 RVA: 0x0004A467 File Offset: 0x00048667
		internal static bool DCSerializeWithSurrogateStopIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(2);
		}

		// Token: 0x060013E6 RID: 5094 RVA: 0x0004A478 File Offset: 0x00048678
		internal static void DCSerializeWithSurrogateStop()
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(2))
			{
				TD.WriteEtwEvent(2, null, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013E7 RID: 5095 RVA: 0x0004A4AA File Offset: 0x000486AA
		internal static bool DCDeserializeWithSurrogateStartIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(3);
		}

		// Token: 0x060013E8 RID: 5096 RVA: 0x0004A4BC File Offset: 0x000486BC
		internal static void DCDeserializeWithSurrogateStart(string SurrogateType)
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(3))
			{
				TD.WriteEtwEvent(3, null, SurrogateType, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013E9 RID: 5097 RVA: 0x0004A4EF File Offset: 0x000486EF
		internal static bool DCDeserializeWithSurrogateStopIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(4);
		}

		// Token: 0x060013EA RID: 5098 RVA: 0x0004A500 File Offset: 0x00048700
		internal static void DCDeserializeWithSurrogateStop()
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(4))
			{
				TD.WriteEtwEvent(4, null, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013EB RID: 5099 RVA: 0x0004A532 File Offset: 0x00048732
		internal static bool ImportKnownTypesStartIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(5);
		}

		// Token: 0x060013EC RID: 5100 RVA: 0x0004A544 File Offset: 0x00048744
		internal static void ImportKnownTypesStart()
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(5))
			{
				TD.WriteEtwEvent(5, null, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013ED RID: 5101 RVA: 0x0004A576 File Offset: 0x00048776
		internal static bool ImportKnownTypesStopIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(6);
		}

		// Token: 0x060013EE RID: 5102 RVA: 0x0004A588 File Offset: 0x00048788
		internal static void ImportKnownTypesStop()
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(6))
			{
				TD.WriteEtwEvent(6, null, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013EF RID: 5103 RVA: 0x0004A5BA File Offset: 0x000487BA
		internal static bool DCResolverResolveIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(7);
		}

		// Token: 0x060013F0 RID: 5104 RVA: 0x0004A5CC File Offset: 0x000487CC
		internal static void DCResolverResolve(string TypeName)
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(7))
			{
				TD.WriteEtwEvent(7, null, TypeName, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013F1 RID: 5105 RVA: 0x0004A5FF File Offset: 0x000487FF
		internal static bool DCGenWriterStartIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(8);
		}

		// Token: 0x060013F2 RID: 5106 RVA: 0x0004A610 File Offset: 0x00048810
		internal static void DCGenWriterStart(string Kind, string TypeName)
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(8))
			{
				TD.WriteEtwEvent(8, null, Kind, TypeName, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013F3 RID: 5107 RVA: 0x0004A644 File Offset: 0x00048844
		internal static bool DCGenWriterStopIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(9);
		}

		// Token: 0x060013F4 RID: 5108 RVA: 0x0004A658 File Offset: 0x00048858
		internal static void DCGenWriterStop()
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(9))
			{
				TD.WriteEtwEvent(9, null, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013F5 RID: 5109 RVA: 0x0004A68C File Offset: 0x0004888C
		internal static bool DCGenReaderStartIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(10);
		}

		// Token: 0x060013F6 RID: 5110 RVA: 0x0004A6A0 File Offset: 0x000488A0
		internal static void DCGenReaderStart(string Kind, string TypeName)
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(10))
			{
				TD.WriteEtwEvent(10, null, Kind, TypeName, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013F7 RID: 5111 RVA: 0x0004A6D6 File Offset: 0x000488D6
		internal static bool DCGenReaderStopIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(11);
		}

		// Token: 0x060013F8 RID: 5112 RVA: 0x0004A6E8 File Offset: 0x000488E8
		internal static void DCGenReaderStop()
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(11))
			{
				TD.WriteEtwEvent(11, null, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013F9 RID: 5113 RVA: 0x0004A71C File Offset: 0x0004891C
		internal static bool DCJsonGenReaderStartIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(12);
		}

		// Token: 0x060013FA RID: 5114 RVA: 0x0004A730 File Offset: 0x00048930
		internal static void DCJsonGenReaderStart(string Kind, string TypeName)
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(12))
			{
				TD.WriteEtwEvent(12, null, Kind, TypeName, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013FB RID: 5115 RVA: 0x0004A766 File Offset: 0x00048966
		internal static bool DCJsonGenReaderStopIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(13);
		}

		// Token: 0x060013FC RID: 5116 RVA: 0x0004A778 File Offset: 0x00048978
		internal static void DCJsonGenReaderStop()
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(13))
			{
				TD.WriteEtwEvent(13, null, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013FD RID: 5117 RVA: 0x0004A7AC File Offset: 0x000489AC
		internal static bool DCJsonGenWriterStartIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(14);
		}

		// Token: 0x060013FE RID: 5118 RVA: 0x0004A7C0 File Offset: 0x000489C0
		internal static void DCJsonGenWriterStart(string Kind, string TypeName)
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(14))
			{
				TD.WriteEtwEvent(14, null, Kind, TypeName, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x060013FF RID: 5119 RVA: 0x0004A7F6 File Offset: 0x000489F6
		internal static bool DCJsonGenWriterStopIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(15);
		}

		// Token: 0x06001400 RID: 5120 RVA: 0x0004A808 File Offset: 0x00048A08
		internal static void DCJsonGenWriterStop()
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(15))
			{
				TD.WriteEtwEvent(15, null, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x06001401 RID: 5121 RVA: 0x0004A83C File Offset: 0x00048A3C
		internal static bool GenXmlSerializableStartIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(16);
		}

		// Token: 0x06001402 RID: 5122 RVA: 0x0004A850 File Offset: 0x00048A50
		internal static void GenXmlSerializableStart(string DCType)
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(16))
			{
				TD.WriteEtwEvent(16, null, DCType, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x06001403 RID: 5123 RVA: 0x0004A885 File Offset: 0x00048A85
		internal static bool GenXmlSerializableStopIsEnabled()
		{
			return FxTrace.ShouldTraceVerbose && TD.IsEtwEventEnabled(17);
		}

		// Token: 0x06001404 RID: 5124 RVA: 0x0004A898 File Offset: 0x00048A98
		internal static void GenXmlSerializableStop()
		{
			TracePayload serializedPayload = FxTrace.Trace.GetSerializedPayload(null, null, null);
			if (TD.IsEtwEventEnabled(17))
			{
				TD.WriteEtwEvent(17, null, serializedPayload.AppDomainFriendlyName);
			}
		}

		// Token: 0x06001405 RID: 5125 RVA: 0x0004A8CC File Offset: 0x00048ACC
		[SecuritySafeCritical]
		private static void CreateEventDescriptors()
		{
			EventDescriptor[] ed = new EventDescriptor[]
			{
				new EventDescriptor(1420, 0, 18, 2, 0, 2560, 2305843009217888256L),
				new EventDescriptor(5001, 0, 19, 5, 1, 2592, 1152921504606846978L),
				new EventDescriptor(5002, 0, 19, 5, 2, 2592, 1152921504606846978L),
				new EventDescriptor(5003, 0, 19, 5, 1, 2591, 1152921504606846978L),
				new EventDescriptor(5004, 0, 19, 5, 2, 2591, 1152921504606846978L),
				new EventDescriptor(5005, 0, 19, 5, 1, 2547, 1152921504606846978L),
				new EventDescriptor(5006, 0, 19, 5, 2, 2547, 1152921504606846978L),
				new EventDescriptor(5007, 0, 19, 5, 1, 2528, 1152921504606846978L),
				new EventDescriptor(5008, 0, 19, 5, 1, 2544, 1152921504606846978L),
				new EventDescriptor(5009, 0, 19, 5, 2, 2544, 1152921504606846978L),
				new EventDescriptor(5010, 0, 19, 5, 1, 2543, 1152921504606846978L),
				new EventDescriptor(5011, 0, 19, 5, 2, 2543, 1152921504606846978L),
				new EventDescriptor(5012, 0, 19, 5, 1, 2543, 1152921504606846978L),
				new EventDescriptor(5013, 0, 19, 5, 2, 2543, 1152921504606846978L),
				new EventDescriptor(5014, 0, 19, 5, 1, 2544, 1152921504606846978L),
				new EventDescriptor(5015, 0, 19, 5, 2, 2544, 1152921504606846978L),
				new EventDescriptor(5016, 0, 19, 5, 1, 2545, 1152921504606846978L),
				new EventDescriptor(5017, 0, 19, 5, 2, 2545, 1152921504606846978L)
			};
			ushort[] events = new ushort[0];
			FxTrace.UpdateEventDefinitions(ed, events);
			TD.eventDescriptors = ed;
		}

		// Token: 0x06001406 RID: 5126 RVA: 0x0004AB84 File Offset: 0x00048D84
		private static void EnsureEventDescriptors()
		{
			if (TD.eventDescriptorsCreated)
			{
				return;
			}
			lock (TD.syncLock)
			{
				if (!TD.eventDescriptorsCreated)
				{
					TD.CreateEventDescriptors();
					TD.eventDescriptorsCreated = true;
				}
			}
		}

		// Token: 0x06001407 RID: 5127 RVA: 0x0004ABDC File Offset: 0x00048DDC
		private static bool IsEtwEventEnabled(int eventIndex)
		{
			if (FxTrace.Trace.IsEtwProviderEnabled)
			{
				TD.EnsureEventDescriptors();
				return FxTrace.IsEventEnabled(eventIndex);
			}
			return false;
		}

		// Token: 0x06001408 RID: 5128 RVA: 0x0004ABF7 File Offset: 0x00048DF7
		[SecuritySafeCritical]
		private static bool WriteEtwEvent(int eventIndex, EventTraceActivity eventParam0, string eventParam1, string eventParam2)
		{
			TD.EnsureEventDescriptors();
			return FxTrace.Trace.EtwProvider.WriteEvent(ref TD.eventDescriptors[eventIndex], eventParam0, eventParam1, eventParam2);
		}

		// Token: 0x06001409 RID: 5129 RVA: 0x0004AC1B File Offset: 0x00048E1B
		[SecuritySafeCritical]
		private static bool WriteEtwEvent(int eventIndex, EventTraceActivity eventParam0, string eventParam1)
		{
			TD.EnsureEventDescriptors();
			return FxTrace.Trace.EtwProvider.WriteEvent(ref TD.eventDescriptors[eventIndex], eventParam0, eventParam1);
		}

		// Token: 0x0600140A RID: 5130 RVA: 0x0004AC3E File Offset: 0x00048E3E
		[SecuritySafeCritical]
		private static bool WriteEtwEvent(int eventIndex, EventTraceActivity eventParam0, string eventParam1, string eventParam2, string eventParam3)
		{
			TD.EnsureEventDescriptors();
			return FxTrace.Trace.EtwProvider.WriteEvent(ref TD.eventDescriptors[eventIndex], eventParam0, eventParam1, eventParam2, eventParam3);
		}

		// Token: 0x0600140B RID: 5131 RVA: 0x0004AC64 File Offset: 0x00048E64
		// Note: this type is marked as 'beforefieldinit'.
		static TD()
		{
		}

		// Token: 0x04000A3C RID: 2620
		private static ResourceManager resourceManager;

		// Token: 0x04000A3D RID: 2621
		private static CultureInfo resourceCulture;

		// Token: 0x04000A3E RID: 2622
		[SecurityCritical]
		private static EventDescriptor[] eventDescriptors;

		// Token: 0x04000A3F RID: 2623
		private static object syncLock = new object();

		// Token: 0x04000A40 RID: 2624
		private static volatile bool eventDescriptorsCreated;
	}
}
