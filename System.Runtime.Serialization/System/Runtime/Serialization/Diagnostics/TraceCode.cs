﻿using System;

namespace System.Runtime.Serialization.Diagnostics
{
	// Token: 0x02000193 RID: 403
	internal static class TraceCode
	{
		// Token: 0x04000A28 RID: 2600
		public const int Serialization = 196608;

		// Token: 0x04000A29 RID: 2601
		public const int WriteObjectBegin = 196609;

		// Token: 0x04000A2A RID: 2602
		public const int WriteObjectEnd = 196610;

		// Token: 0x04000A2B RID: 2603
		public const int WriteObjectContentBegin = 196611;

		// Token: 0x04000A2C RID: 2604
		public const int WriteObjectContentEnd = 196612;

		// Token: 0x04000A2D RID: 2605
		public const int ReadObjectBegin = 196613;

		// Token: 0x04000A2E RID: 2606
		public const int ReadObjectEnd = 196614;

		// Token: 0x04000A2F RID: 2607
		public const int ElementIgnored = 196615;

		// Token: 0x04000A30 RID: 2608
		public const int XsdExportBegin = 196616;

		// Token: 0x04000A31 RID: 2609
		public const int XsdExportEnd = 196617;

		// Token: 0x04000A32 RID: 2610
		public const int XsdImportBegin = 196618;

		// Token: 0x04000A33 RID: 2611
		public const int XsdImportEnd = 196619;

		// Token: 0x04000A34 RID: 2612
		public const int XsdExportError = 196620;

		// Token: 0x04000A35 RID: 2613
		public const int XsdImportError = 196621;

		// Token: 0x04000A36 RID: 2614
		public const int XsdExportAnnotationFailed = 196622;

		// Token: 0x04000A37 RID: 2615
		public const int XsdImportAnnotationFailed = 196623;

		// Token: 0x04000A38 RID: 2616
		public const int XsdExportDupItems = 196624;

		// Token: 0x04000A39 RID: 2617
		public const int FactoryTypeNotFound = 196625;

		// Token: 0x04000A3A RID: 2618
		public const int ObjectWithLargeDepth = 196626;
	}
}
