﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x020000E6 RID: 230
	internal static class JsonFormatGeneratorStatics
	{
		// Token: 0x17000294 RID: 660
		// (get) Token: 0x06000CD5 RID: 3285 RVA: 0x00031797 File Offset: 0x0002F997
		public static MethodInfo BoxPointer
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.boxPointer == null)
				{
					JsonFormatGeneratorStatics.boxPointer = typeof(Pointer).GetMethod("Box");
				}
				return JsonFormatGeneratorStatics.boxPointer;
			}
		}

		// Token: 0x17000295 RID: 661
		// (get) Token: 0x06000CD6 RID: 3286 RVA: 0x000317C4 File Offset: 0x0002F9C4
		public static PropertyInfo CollectionItemNameProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.collectionItemNameProperty == null)
				{
					JsonFormatGeneratorStatics.collectionItemNameProperty = typeof(XmlObjectSerializerWriteContextComplexJson).GetProperty("CollectionItemName", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.collectionItemNameProperty;
			}
		}

		// Token: 0x17000296 RID: 662
		// (get) Token: 0x06000CD7 RID: 3287 RVA: 0x000317F3 File Offset: 0x0002F9F3
		public static ConstructorInfo ExtensionDataObjectCtor
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.extensionDataObjectCtor == null)
				{
					JsonFormatGeneratorStatics.extensionDataObjectCtor = typeof(ExtensionDataObject).GetConstructor(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[0], null);
				}
				return JsonFormatGeneratorStatics.extensionDataObjectCtor;
			}
		}

		// Token: 0x17000297 RID: 663
		// (get) Token: 0x06000CD8 RID: 3288 RVA: 0x00031825 File Offset: 0x0002FA25
		public static PropertyInfo ExtensionDataProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.extensionDataProperty == null)
				{
					JsonFormatGeneratorStatics.extensionDataProperty = typeof(IExtensibleDataObject).GetProperty("ExtensionData");
				}
				return JsonFormatGeneratorStatics.extensionDataProperty;
			}
		}

		// Token: 0x17000298 RID: 664
		// (get) Token: 0x06000CD9 RID: 3289 RVA: 0x00031852 File Offset: 0x0002FA52
		public static MethodInfo GetCurrentMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.ienumeratorGetCurrentMethod == null)
				{
					JsonFormatGeneratorStatics.ienumeratorGetCurrentMethod = typeof(IEnumerator).GetProperty("Current").GetGetMethod();
				}
				return JsonFormatGeneratorStatics.ienumeratorGetCurrentMethod;
			}
		}

		// Token: 0x17000299 RID: 665
		// (get) Token: 0x06000CDA RID: 3290 RVA: 0x00031884 File Offset: 0x0002FA84
		public static MethodInfo GetItemContractMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.getItemContractMethod == null)
				{
					JsonFormatGeneratorStatics.getItemContractMethod = typeof(CollectionDataContract).GetProperty("ItemContract", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic).GetGetMethod(true);
				}
				return JsonFormatGeneratorStatics.getItemContractMethod;
			}
		}

		// Token: 0x1700029A RID: 666
		// (get) Token: 0x06000CDB RID: 3291 RVA: 0x000318B9 File Offset: 0x0002FAB9
		public static MethodInfo GetJsonDataContractMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.getJsonDataContractMethod == null)
				{
					JsonFormatGeneratorStatics.getJsonDataContractMethod = typeof(JsonDataContract).GetMethod("GetJsonDataContract", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.getJsonDataContractMethod;
			}
		}

		// Token: 0x1700029B RID: 667
		// (get) Token: 0x06000CDC RID: 3292 RVA: 0x000318E8 File Offset: 0x0002FAE8
		public static MethodInfo GetJsonMemberIndexMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.getJsonMemberIndexMethod == null)
				{
					JsonFormatGeneratorStatics.getJsonMemberIndexMethod = typeof(XmlObjectSerializerReadContextComplexJson).GetMethod("GetJsonMemberIndex", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.getJsonMemberIndexMethod;
			}
		}

		// Token: 0x1700029C RID: 668
		// (get) Token: 0x06000CDD RID: 3293 RVA: 0x00031917 File Offset: 0x0002FB17
		public static MethodInfo GetRevisedItemContractMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.getRevisedItemContractMethod == null)
				{
					JsonFormatGeneratorStatics.getRevisedItemContractMethod = typeof(XmlObjectSerializerWriteContextComplexJson).GetMethod("GetRevisedItemContract", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.getRevisedItemContractMethod;
			}
		}

		// Token: 0x1700029D RID: 669
		// (get) Token: 0x06000CDE RID: 3294 RVA: 0x00031948 File Offset: 0x0002FB48
		public static MethodInfo GetUninitializedObjectMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.getUninitializedObjectMethod == null)
				{
					JsonFormatGeneratorStatics.getUninitializedObjectMethod = typeof(XmlFormatReaderGenerator).GetMethod("UnsafeGetUninitializedObject", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(int)
					}, null);
				}
				return JsonFormatGeneratorStatics.getUninitializedObjectMethod;
			}
		}

		// Token: 0x1700029E RID: 670
		// (get) Token: 0x06000CDF RID: 3295 RVA: 0x00031997 File Offset: 0x0002FB97
		public static MethodInfo IsStartElementMethod0
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.isStartElementMethod0 == null)
				{
					JsonFormatGeneratorStatics.isStartElementMethod0 = typeof(XmlReaderDelegator).GetMethod("IsStartElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[0], null);
				}
				return JsonFormatGeneratorStatics.isStartElementMethod0;
			}
		}

		// Token: 0x1700029F RID: 671
		// (get) Token: 0x06000CE0 RID: 3296 RVA: 0x000319D0 File Offset: 0x0002FBD0
		public static MethodInfo IsStartElementMethod2
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.isStartElementMethod2 == null)
				{
					JsonFormatGeneratorStatics.isStartElementMethod2 = typeof(XmlReaderDelegator).GetMethod("IsStartElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlDictionaryString),
						typeof(XmlDictionaryString)
					}, null);
				}
				return JsonFormatGeneratorStatics.isStartElementMethod2;
			}
		}

		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x06000CE1 RID: 3297 RVA: 0x00031A2C File Offset: 0x0002FC2C
		public static PropertyInfo LocalNameProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.localNameProperty == null)
				{
					JsonFormatGeneratorStatics.localNameProperty = typeof(XmlReaderDelegator).GetProperty("LocalName", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.localNameProperty;
			}
		}

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000CE2 RID: 3298 RVA: 0x00031A5B File Offset: 0x0002FC5B
		public static PropertyInfo NamespaceProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.namespaceProperty == null)
				{
					JsonFormatGeneratorStatics.namespaceProperty = typeof(XmlReaderDelegator).GetProperty("NamespaceProperty", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.namespaceProperty;
			}
		}

		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000CE3 RID: 3299 RVA: 0x00031A8A File Offset: 0x0002FC8A
		public static MethodInfo MoveNextMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.ienumeratorMoveNextMethod == null)
				{
					JsonFormatGeneratorStatics.ienumeratorMoveNextMethod = typeof(IEnumerator).GetMethod("MoveNext");
				}
				return JsonFormatGeneratorStatics.ienumeratorMoveNextMethod;
			}
		}

		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06000CE4 RID: 3300 RVA: 0x00031AB7 File Offset: 0x0002FCB7
		public static MethodInfo MoveToContentMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.moveToContentMethod == null)
				{
					JsonFormatGeneratorStatics.moveToContentMethod = typeof(XmlReaderDelegator).GetMethod("MoveToContent", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.moveToContentMethod;
			}
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06000CE5 RID: 3301 RVA: 0x00031AE6 File Offset: 0x0002FCE6
		public static PropertyInfo NodeTypeProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.nodeTypeProperty == null)
				{
					JsonFormatGeneratorStatics.nodeTypeProperty = typeof(XmlReaderDelegator).GetProperty("NodeType", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.nodeTypeProperty;
			}
		}

		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x06000CE6 RID: 3302 RVA: 0x00031B15 File Offset: 0x0002FD15
		public static MethodInfo OnDeserializationMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.onDeserializationMethod == null)
				{
					JsonFormatGeneratorStatics.onDeserializationMethod = typeof(IDeserializationCallback).GetMethod("OnDeserialization");
				}
				return JsonFormatGeneratorStatics.onDeserializationMethod;
			}
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000CE7 RID: 3303 RVA: 0x00031B42 File Offset: 0x0002FD42
		public static MethodInfo ReadJsonValueMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.readJsonValueMethod == null)
				{
					JsonFormatGeneratorStatics.readJsonValueMethod = typeof(DataContractJsonSerializer).GetMethod("ReadJsonValue", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.readJsonValueMethod;
			}
		}

		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x06000CE8 RID: 3304 RVA: 0x00031B71 File Offset: 0x0002FD71
		public static ConstructorInfo SerializationExceptionCtor
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.serializationExceptionCtor == null)
				{
					JsonFormatGeneratorStatics.serializationExceptionCtor = typeof(SerializationException).GetConstructor(new Type[]
					{
						typeof(string)
					});
				}
				return JsonFormatGeneratorStatics.serializationExceptionCtor;
			}
		}

		// Token: 0x170002A8 RID: 680
		// (get) Token: 0x06000CE9 RID: 3305 RVA: 0x00031BAC File Offset: 0x0002FDAC
		public static Type[] SerInfoCtorArgs
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.serInfoCtorArgs == null)
				{
					JsonFormatGeneratorStatics.serInfoCtorArgs = new Type[]
					{
						typeof(SerializationInfo),
						typeof(StreamingContext)
					};
				}
				return JsonFormatGeneratorStatics.serInfoCtorArgs;
			}
		}

		// Token: 0x170002A9 RID: 681
		// (get) Token: 0x06000CEA RID: 3306 RVA: 0x00031BDF File Offset: 0x0002FDDF
		public static MethodInfo ThrowDuplicateMemberExceptionMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.throwDuplicateMemberExceptionMethod == null)
				{
					JsonFormatGeneratorStatics.throwDuplicateMemberExceptionMethod = typeof(XmlObjectSerializerReadContextComplexJson).GetMethod("ThrowDuplicateMemberException", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.throwDuplicateMemberExceptionMethod;
			}
		}

		// Token: 0x170002AA RID: 682
		// (get) Token: 0x06000CEB RID: 3307 RVA: 0x00031C0E File Offset: 0x0002FE0E
		public static MethodInfo ThrowMissingRequiredMembersMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.throwMissingRequiredMembersMethod == null)
				{
					JsonFormatGeneratorStatics.throwMissingRequiredMembersMethod = typeof(XmlObjectSerializerReadContextComplexJson).GetMethod("ThrowMissingRequiredMembers", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.throwMissingRequiredMembersMethod;
			}
		}

		// Token: 0x170002AB RID: 683
		// (get) Token: 0x06000CEC RID: 3308 RVA: 0x00031C3D File Offset: 0x0002FE3D
		public static PropertyInfo TypeHandleProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.typeHandleProperty == null)
				{
					JsonFormatGeneratorStatics.typeHandleProperty = typeof(Type).GetProperty("TypeHandle");
				}
				return JsonFormatGeneratorStatics.typeHandleProperty;
			}
		}

		// Token: 0x170002AC RID: 684
		// (get) Token: 0x06000CED RID: 3309 RVA: 0x00031C6A File Offset: 0x0002FE6A
		public static MethodInfo UnboxPointer
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.unboxPointer == null)
				{
					JsonFormatGeneratorStatics.unboxPointer = typeof(Pointer).GetMethod("Unbox");
				}
				return JsonFormatGeneratorStatics.unboxPointer;
			}
		}

		// Token: 0x170002AD RID: 685
		// (get) Token: 0x06000CEE RID: 3310 RVA: 0x00031C97 File Offset: 0x0002FE97
		public static PropertyInfo UseSimpleDictionaryFormatReadProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.useSimpleDictionaryFormatReadProperty == null)
				{
					JsonFormatGeneratorStatics.useSimpleDictionaryFormatReadProperty = typeof(XmlObjectSerializerReadContextComplexJson).GetProperty("UseSimpleDictionaryFormat", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.useSimpleDictionaryFormatReadProperty;
			}
		}

		// Token: 0x170002AE RID: 686
		// (get) Token: 0x06000CEF RID: 3311 RVA: 0x00031CC6 File Offset: 0x0002FEC6
		public static PropertyInfo UseSimpleDictionaryFormatWriteProperty
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.useSimpleDictionaryFormatWriteProperty == null)
				{
					JsonFormatGeneratorStatics.useSimpleDictionaryFormatWriteProperty = typeof(XmlObjectSerializerWriteContextComplexJson).GetProperty("UseSimpleDictionaryFormat", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.useSimpleDictionaryFormatWriteProperty;
			}
		}

		// Token: 0x170002AF RID: 687
		// (get) Token: 0x06000CF0 RID: 3312 RVA: 0x00031CF8 File Offset: 0x0002FEF8
		public static MethodInfo WriteAttributeStringMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.writeAttributeStringMethod == null)
				{
					JsonFormatGeneratorStatics.writeAttributeStringMethod = typeof(XmlWriterDelegator).GetMethod("WriteAttributeString", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(string),
						typeof(string),
						typeof(string),
						typeof(string)
					}, null);
				}
				return JsonFormatGeneratorStatics.writeAttributeStringMethod;
			}
		}

		// Token: 0x170002B0 RID: 688
		// (get) Token: 0x06000CF1 RID: 3313 RVA: 0x00031D6E File Offset: 0x0002FF6E
		public static MethodInfo WriteEndElementMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.writeEndElementMethod == null)
				{
					JsonFormatGeneratorStatics.writeEndElementMethod = typeof(XmlWriterDelegator).GetMethod("WriteEndElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[0], null);
				}
				return JsonFormatGeneratorStatics.writeEndElementMethod;
			}
		}

		// Token: 0x170002B1 RID: 689
		// (get) Token: 0x06000CF2 RID: 3314 RVA: 0x00031DA5 File Offset: 0x0002FFA5
		public static MethodInfo WriteJsonISerializableMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.writeJsonISerializableMethod == null)
				{
					JsonFormatGeneratorStatics.writeJsonISerializableMethod = typeof(XmlObjectSerializerWriteContextComplexJson).GetMethod("WriteJsonISerializable", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.writeJsonISerializableMethod;
			}
		}

		// Token: 0x170002B2 RID: 690
		// (get) Token: 0x06000CF3 RID: 3315 RVA: 0x00031DD4 File Offset: 0x0002FFD4
		public static MethodInfo WriteJsonNameWithMappingMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.writeJsonNameWithMappingMethod == null)
				{
					JsonFormatGeneratorStatics.writeJsonNameWithMappingMethod = typeof(XmlObjectSerializerWriteContextComplexJson).GetMethod("WriteJsonNameWithMapping", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.writeJsonNameWithMappingMethod;
			}
		}

		// Token: 0x170002B3 RID: 691
		// (get) Token: 0x06000CF4 RID: 3316 RVA: 0x00031E03 File Offset: 0x00030003
		public static MethodInfo WriteJsonValueMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.writeJsonValueMethod == null)
				{
					JsonFormatGeneratorStatics.writeJsonValueMethod = typeof(DataContractJsonSerializer).GetMethod("WriteJsonValue", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				}
				return JsonFormatGeneratorStatics.writeJsonValueMethod;
			}
		}

		// Token: 0x170002B4 RID: 692
		// (get) Token: 0x06000CF5 RID: 3317 RVA: 0x00031E34 File Offset: 0x00030034
		public static MethodInfo WriteStartElementMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.writeStartElementMethod == null)
				{
					JsonFormatGeneratorStatics.writeStartElementMethod = typeof(XmlWriterDelegator).GetMethod("WriteStartElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlDictionaryString),
						typeof(XmlDictionaryString)
					}, null);
				}
				return JsonFormatGeneratorStatics.writeStartElementMethod;
			}
		}

		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x06000CF6 RID: 3318 RVA: 0x00031E90 File Offset: 0x00030090
		public static MethodInfo WriteStartElementStringMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.writeStartElementStringMethod == null)
				{
					JsonFormatGeneratorStatics.writeStartElementStringMethod = typeof(XmlWriterDelegator).GetMethod("WriteStartElement", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(string),
						typeof(string)
					}, null);
				}
				return JsonFormatGeneratorStatics.writeStartElementStringMethod;
			}
		}

		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x06000CF7 RID: 3319 RVA: 0x00031EEC File Offset: 0x000300EC
		public static MethodInfo ParseEnumMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.parseEnumMethod == null)
				{
					JsonFormatGeneratorStatics.parseEnumMethod = typeof(Enum).GetMethod("Parse", BindingFlags.Static | BindingFlags.Public, null, new Type[]
					{
						typeof(Type),
						typeof(string)
					}, null);
				}
				return JsonFormatGeneratorStatics.parseEnumMethod;
			}
		}

		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x06000CF8 RID: 3320 RVA: 0x00031F48 File Offset: 0x00030148
		public static MethodInfo GetJsonMemberNameMethod
		{
			[SecuritySafeCritical]
			get
			{
				if (JsonFormatGeneratorStatics.getJsonMemberNameMethod == null)
				{
					JsonFormatGeneratorStatics.getJsonMemberNameMethod = typeof(XmlObjectSerializerReadContextComplexJson).GetMethod("GetJsonMemberName", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						typeof(XmlReaderDelegator)
					}, null);
				}
				return JsonFormatGeneratorStatics.getJsonMemberNameMethod;
			}
		}

		// Token: 0x04000601 RID: 1537
		[SecurityCritical]
		private static MethodInfo boxPointer;

		// Token: 0x04000602 RID: 1538
		[SecurityCritical]
		private static PropertyInfo collectionItemNameProperty;

		// Token: 0x04000603 RID: 1539
		[SecurityCritical]
		private static ConstructorInfo extensionDataObjectCtor;

		// Token: 0x04000604 RID: 1540
		[SecurityCritical]
		private static PropertyInfo extensionDataProperty;

		// Token: 0x04000605 RID: 1541
		[SecurityCritical]
		private static MethodInfo getItemContractMethod;

		// Token: 0x04000606 RID: 1542
		[SecurityCritical]
		private static MethodInfo getJsonDataContractMethod;

		// Token: 0x04000607 RID: 1543
		[SecurityCritical]
		private static MethodInfo getJsonMemberIndexMethod;

		// Token: 0x04000608 RID: 1544
		[SecurityCritical]
		private static MethodInfo getRevisedItemContractMethod;

		// Token: 0x04000609 RID: 1545
		[SecurityCritical]
		private static MethodInfo getUninitializedObjectMethod;

		// Token: 0x0400060A RID: 1546
		[SecurityCritical]
		private static MethodInfo ienumeratorGetCurrentMethod;

		// Token: 0x0400060B RID: 1547
		[SecurityCritical]
		private static MethodInfo ienumeratorMoveNextMethod;

		// Token: 0x0400060C RID: 1548
		[SecurityCritical]
		private static MethodInfo isStartElementMethod0;

		// Token: 0x0400060D RID: 1549
		[SecurityCritical]
		private static MethodInfo isStartElementMethod2;

		// Token: 0x0400060E RID: 1550
		[SecurityCritical]
		private static PropertyInfo localNameProperty;

		// Token: 0x0400060F RID: 1551
		[SecurityCritical]
		private static PropertyInfo namespaceProperty;

		// Token: 0x04000610 RID: 1552
		[SecurityCritical]
		private static MethodInfo moveToContentMethod;

		// Token: 0x04000611 RID: 1553
		[SecurityCritical]
		private static PropertyInfo nodeTypeProperty;

		// Token: 0x04000612 RID: 1554
		[SecurityCritical]
		private static MethodInfo onDeserializationMethod;

		// Token: 0x04000613 RID: 1555
		[SecurityCritical]
		private static MethodInfo readJsonValueMethod;

		// Token: 0x04000614 RID: 1556
		[SecurityCritical]
		private static ConstructorInfo serializationExceptionCtor;

		// Token: 0x04000615 RID: 1557
		[SecurityCritical]
		private static Type[] serInfoCtorArgs;

		// Token: 0x04000616 RID: 1558
		[SecurityCritical]
		private static MethodInfo throwDuplicateMemberExceptionMethod;

		// Token: 0x04000617 RID: 1559
		[SecurityCritical]
		private static MethodInfo throwMissingRequiredMembersMethod;

		// Token: 0x04000618 RID: 1560
		[SecurityCritical]
		private static PropertyInfo typeHandleProperty;

		// Token: 0x04000619 RID: 1561
		[SecurityCritical]
		private static MethodInfo unboxPointer;

		// Token: 0x0400061A RID: 1562
		[SecurityCritical]
		private static PropertyInfo useSimpleDictionaryFormatReadProperty;

		// Token: 0x0400061B RID: 1563
		[SecurityCritical]
		private static PropertyInfo useSimpleDictionaryFormatWriteProperty;

		// Token: 0x0400061C RID: 1564
		[SecurityCritical]
		private static MethodInfo writeAttributeStringMethod;

		// Token: 0x0400061D RID: 1565
		[SecurityCritical]
		private static MethodInfo writeEndElementMethod;

		// Token: 0x0400061E RID: 1566
		[SecurityCritical]
		private static MethodInfo writeJsonISerializableMethod;

		// Token: 0x0400061F RID: 1567
		[SecurityCritical]
		private static MethodInfo writeJsonNameWithMappingMethod;

		// Token: 0x04000620 RID: 1568
		[SecurityCritical]
		private static MethodInfo writeJsonValueMethod;

		// Token: 0x04000621 RID: 1569
		[SecurityCritical]
		private static MethodInfo writeStartElementMethod;

		// Token: 0x04000622 RID: 1570
		[SecurityCritical]
		private static MethodInfo writeStartElementStringMethod;

		// Token: 0x04000623 RID: 1571
		[SecurityCritical]
		private static MethodInfo parseEnumMethod;

		// Token: 0x04000624 RID: 1572
		[SecurityCritical]
		private static MethodInfo getJsonMemberNameMethod;
	}
}
