﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;

namespace System.Runtime.Serialization
{
	// Token: 0x020000E2 RID: 226
	internal static class DataContractSurrogateCaller
	{
		// Token: 0x06000CC7 RID: 3271 RVA: 0x000316D8 File Offset: 0x0002F8D8
		internal static Type GetDataContractType(IDataContractSurrogate surrogate, Type type)
		{
			if (DataContract.GetBuiltInDataContract(type) != null)
			{
				return type;
			}
			Type dataContractType = surrogate.GetDataContractType(type);
			if (dataContractType == null)
			{
				return type;
			}
			return dataContractType;
		}

		// Token: 0x06000CC8 RID: 3272 RVA: 0x00031703 File Offset: 0x0002F903
		internal static object GetObjectToSerialize(IDataContractSurrogate surrogate, object obj, Type objType, Type membertype)
		{
			if (obj == null)
			{
				return null;
			}
			if (DataContract.GetBuiltInDataContract(objType) != null)
			{
				return obj;
			}
			return surrogate.GetObjectToSerialize(obj, membertype);
		}

		// Token: 0x06000CC9 RID: 3273 RVA: 0x0003171C File Offset: 0x0002F91C
		internal static object GetDeserializedObject(IDataContractSurrogate surrogate, object obj, Type objType, Type memberType)
		{
			if (obj == null)
			{
				return null;
			}
			if (DataContract.GetBuiltInDataContract(objType) != null)
			{
				return obj;
			}
			return surrogate.GetDeserializedObject(obj, memberType);
		}

		// Token: 0x06000CCA RID: 3274 RVA: 0x00031735 File Offset: 0x0002F935
		internal static object GetCustomDataToExport(IDataContractSurrogate surrogate, MemberInfo memberInfo, Type dataContractType)
		{
			return surrogate.GetCustomDataToExport(memberInfo, dataContractType);
		}

		// Token: 0x06000CCB RID: 3275 RVA: 0x0003173F File Offset: 0x0002F93F
		internal static object GetCustomDataToExport(IDataContractSurrogate surrogate, Type clrType, Type dataContractType)
		{
			if (DataContract.GetBuiltInDataContract(clrType) != null)
			{
				return null;
			}
			return surrogate.GetCustomDataToExport(clrType, dataContractType);
		}

		// Token: 0x06000CCC RID: 3276 RVA: 0x00031753 File Offset: 0x0002F953
		internal static void GetKnownCustomDataTypes(IDataContractSurrogate surrogate, Collection<Type> customDataTypes)
		{
			surrogate.GetKnownCustomDataTypes(customDataTypes);
		}

		// Token: 0x06000CCD RID: 3277 RVA: 0x0003175C File Offset: 0x0002F95C
		internal static Type GetReferencedTypeOnImport(IDataContractSurrogate surrogate, string typeName, string typeNamespace, object customData)
		{
			if (DataContract.GetBuiltInDataContract(typeName, typeNamespace) != null)
			{
				return null;
			}
			return surrogate.GetReferencedTypeOnImport(typeName, typeNamespace, customData);
		}
	}
}
