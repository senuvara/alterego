﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000DC RID: 220
	internal class ElementData
	{
		// Token: 0x06000C5D RID: 3165 RVA: 0x00030968 File Offset: 0x0002EB68
		public void AddAttribute(string prefix, string ns, string name, string value)
		{
			this.GrowAttributesIfNeeded();
			AttributeData attributeData = this.attributes[this.attributeCount];
			if (attributeData == null)
			{
				attributeData = (this.attributes[this.attributeCount] = new AttributeData());
			}
			attributeData.prefix = prefix;
			attributeData.ns = ns;
			attributeData.localName = name;
			attributeData.value = value;
			this.attributeCount++;
		}

		// Token: 0x06000C5E RID: 3166 RVA: 0x000309CC File Offset: 0x0002EBCC
		private void GrowAttributesIfNeeded()
		{
			if (this.attributes == null)
			{
				this.attributes = new AttributeData[4];
				return;
			}
			if (this.attributes.Length == this.attributeCount)
			{
				AttributeData[] destinationArray = new AttributeData[this.attributes.Length * 2];
				Array.Copy(this.attributes, 0, destinationArray, 0, this.attributes.Length);
				this.attributes = destinationArray;
			}
		}

		// Token: 0x06000C5F RID: 3167 RVA: 0x00002217 File Offset: 0x00000417
		public ElementData()
		{
		}

		// Token: 0x0400053E RID: 1342
		public string localName;

		// Token: 0x0400053F RID: 1343
		public string ns;

		// Token: 0x04000540 RID: 1344
		public string prefix;

		// Token: 0x04000541 RID: 1345
		public int attributeCount;

		// Token: 0x04000542 RID: 1346
		public AttributeData[] attributes;

		// Token: 0x04000543 RID: 1347
		public IDataNode dataNode;

		// Token: 0x04000544 RID: 1348
		public int childElementIndex;
	}
}
