﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters;
using System.Security;
using System.Security.Permissions;
using System.Xml;

namespace System.Runtime.Serialization
{
	/// <summary>Serializes and deserializes an instance of a type into XML stream or document using the supplied .NET Framework types. This class cannot be inherited.</summary>
	// Token: 0x020000E9 RID: 233
	public sealed class NetDataContractSerializer : XmlObjectSerializer, IFormatter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.NetDataContractSerializer" /> class.  </summary>
		// Token: 0x06000D01 RID: 3329 RVA: 0x0003206C File Offset: 0x0003026C
		public NetDataContractSerializer() : this(new StreamingContext(StreamingContextStates.All))
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.NetDataContractSerializer" /> class with the supplied streaming context data. </summary>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains context data.</param>
		// Token: 0x06000D02 RID: 3330 RVA: 0x0003207E File Offset: 0x0003027E
		public NetDataContractSerializer(StreamingContext context) : this(context, int.MaxValue, false, FormatterAssemblyStyle.Full, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.NetDataContractSerializer" /> class with the supplied context data; in addition, specifies the maximum number of items in the object to be serialized, and parameters to specify whether extra data is ignored, the assembly loading method, and a surrogate selector.</summary>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains context data.</param>
		/// <param name="maxItemsInObjectGraph">The maximum number of items in the graph to serialize or deserialize. </param>
		/// <param name="ignoreExtensionDataObject">
		///       <see langword="true" /> to ignore the data supplied by an extension of the type; otherwise, <see langword="false" />.</param>
		/// <param name="assemblyFormat">A <see cref="T:System.Runtime.Serialization.Formatters.FormatterAssemblyStyle" /> enumeration value that specifies a method for locating and loading assemblies.</param>
		/// <param name="surrogateSelector">An implementation of the <see cref="T:System.Runtime.Serialization.ISurrogateSelector" />.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="maxItemsInObjectGraph" /> value is less than 0.</exception>
		// Token: 0x06000D03 RID: 3331 RVA: 0x0003208F File Offset: 0x0003028F
		public NetDataContractSerializer(StreamingContext context, int maxItemsInObjectGraph, bool ignoreExtensionDataObject, FormatterAssemblyStyle assemblyFormat, ISurrogateSelector surrogateSelector)
		{
			this.Initialize(context, maxItemsInObjectGraph, ignoreExtensionDataObject, assemblyFormat, surrogateSelector);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.NetDataContractSerializer" /> class with the supplied XML root element and namespace.</summary>
		/// <param name="rootName">The name of the XML element that encloses the content to serialize or deserialize.</param>
		/// <param name="rootNamespace">The namespace of the XML element that encloses the content to serialize or deserialize.</param>
		// Token: 0x06000D04 RID: 3332 RVA: 0x000320A4 File Offset: 0x000302A4
		public NetDataContractSerializer(string rootName, string rootNamespace) : this(rootName, rootNamespace, new StreamingContext(StreamingContextStates.All), int.MaxValue, false, FormatterAssemblyStyle.Full, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.NetDataContractSerializer" /> class with the supplied context data and root name and namespace; in addition, specifies the maximum number of items in the object to be serialized, and parameters to specify whether extra data is ignored, the assembly loading method, and a surrogate selector.</summary>
		/// <param name="rootName">The name of the XML element that encloses the content to serialize or deserialize.</param>
		/// <param name="rootNamespace">The namespace of the XML element that encloses the content to serialize or deserialize.</param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains context data.</param>
		/// <param name="maxItemsInObjectGraph">The maximum number of items in the graph to serialize or deserialize. </param>
		/// <param name="ignoreExtensionDataObject">
		///       <see langword="true" /> to ignore the data supplied by an extension of the type; otherwise, <see langword="false" />.</param>
		/// <param name="assemblyFormat">A <see cref="T:System.Runtime.Serialization.Formatters.FormatterAssemblyStyle" /> enumeration value that specifies a method for locating and loading assemblies.</param>
		/// <param name="surrogateSelector">An implementation of the <see cref="T:System.Runtime.Serialization.IDataContractSurrogate" /> to handle the legacy type.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="maxItemsInObjectGraph" /> value is less than 0.</exception>
		// Token: 0x06000D05 RID: 3333 RVA: 0x000320C0 File Offset: 0x000302C0
		public NetDataContractSerializer(string rootName, string rootNamespace, StreamingContext context, int maxItemsInObjectGraph, bool ignoreExtensionDataObject, FormatterAssemblyStyle assemblyFormat, ISurrogateSelector surrogateSelector)
		{
			XmlDictionary xmlDictionary = new XmlDictionary(2);
			this.Initialize(xmlDictionary.Add(rootName), xmlDictionary.Add(DataContract.GetNamespace(rootNamespace)), context, maxItemsInObjectGraph, ignoreExtensionDataObject, assemblyFormat, surrogateSelector);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.NetDataContractSerializer" /> class with two parameters of type <see cref="T:System.Xml.XmlDictionaryString" /> that contain the root element and namespace used to specify the content.</summary>
		/// <param name="rootName">An <see cref="T:System.Xml.XmlDictionaryString" /> that contains the name of the XML element that encloses the content to serialize or deserialize.</param>
		/// <param name="rootNamespace">An <see cref="T:System.Xml.XmlDictionaryString" /> that contains the namespace of the XML element that encloses the content to serialize or deserialize.</param>
		// Token: 0x06000D06 RID: 3334 RVA: 0x000320FC File Offset: 0x000302FC
		public NetDataContractSerializer(XmlDictionaryString rootName, XmlDictionaryString rootNamespace) : this(rootName, rootNamespace, new StreamingContext(StreamingContextStates.All), int.MaxValue, false, FormatterAssemblyStyle.Full, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.NetDataContractSerializer" /> class with the supplied context data, and root name and namespace (as <see cref="T:System.Xml.XmlDictionaryString" />  parameters); in addition, specifies the maximum number of items in the object to be serialized, and parameters to specify whether extra data found is ignored, assembly loading method, and a surrogate selector.</summary>
		/// <param name="rootName">An <see cref="T:System.Xml.XmlDictionaryString" /> that contains the root element of the content.</param>
		/// <param name="rootNamespace">An <see cref="T:System.Xml.XmlDictionaryString" /> that contains the namespace of the root element.</param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains context data.</param>
		/// <param name="maxItemsInObjectGraph">The maximum number of items in the graph to serialize or deserialize. </param>
		/// <param name="ignoreExtensionDataObject">
		///       <see langword="true" /> to ignore the data supplied by an extension of the type; otherwise, <see langword="false" />.</param>
		/// <param name="assemblyFormat">A <see cref="T:System.Runtime.Serialization.Formatters.FormatterAssemblyStyle" /> enumeration value that specifies a method for locating and loading assemblies.</param>
		/// <param name="surrogateSelector">An implementation of the <see cref="T:System.Runtime.Serialization.IDataContractSurrogate" /> to handle the legacy type.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="maxItemsInObjectGraph" /> value is less than 0.</exception>
		// Token: 0x06000D07 RID: 3335 RVA: 0x00032118 File Offset: 0x00030318
		public NetDataContractSerializer(XmlDictionaryString rootName, XmlDictionaryString rootNamespace, StreamingContext context, int maxItemsInObjectGraph, bool ignoreExtensionDataObject, FormatterAssemblyStyle assemblyFormat, ISurrogateSelector surrogateSelector)
		{
			this.Initialize(rootName, rootNamespace, context, maxItemsInObjectGraph, ignoreExtensionDataObject, assemblyFormat, surrogateSelector);
		}

		// Token: 0x06000D08 RID: 3336 RVA: 0x00032134 File Offset: 0x00030334
		private void Initialize(StreamingContext context, int maxItemsInObjectGraph, bool ignoreExtensionDataObject, FormatterAssemblyStyle assemblyFormat, ISurrogateSelector surrogateSelector)
		{
			this.context = context;
			if (maxItemsInObjectGraph < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("maxItemsInObjectGraph", SR.GetString("The value of this argument must be non-negative.")));
			}
			this.maxItemsInObjectGraph = maxItemsInObjectGraph;
			this.ignoreExtensionDataObject = ignoreExtensionDataObject;
			this.surrogateSelector = surrogateSelector;
			this.AssemblyFormat = assemblyFormat;
		}

		// Token: 0x06000D09 RID: 3337 RVA: 0x00032184 File Offset: 0x00030384
		private void Initialize(XmlDictionaryString rootName, XmlDictionaryString rootNamespace, StreamingContext context, int maxItemsInObjectGraph, bool ignoreExtensionDataObject, FormatterAssemblyStyle assemblyFormat, ISurrogateSelector surrogateSelector)
		{
			this.Initialize(context, maxItemsInObjectGraph, ignoreExtensionDataObject, assemblyFormat, surrogateSelector);
			this.rootName = rootName;
			this.rootNamespace = rootNamespace;
		}

		// Token: 0x170002BA RID: 698
		// (get) Token: 0x06000D0A RID: 3338 RVA: 0x000321A3 File Offset: 0x000303A3
		internal static bool UnsafeTypeForwardingEnabled
		{
			[SecuritySafeCritical]
			get
			{
				if (NetDataContractSerializer.unsafeTypeForwardingEnabled == null)
				{
					NetDataContractSerializer.unsafeTypeForwardingEnabled = new bool?(false);
				}
				return NetDataContractSerializer.unsafeTypeForwardingEnabled.Value;
			}
		}

		/// <summary>Gets or sets the object that enables the passing of context data that is useful while serializing or deserializing.</summary>
		/// <returns>A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains the context data.</returns>
		// Token: 0x170002BB RID: 699
		// (get) Token: 0x06000D0B RID: 3339 RVA: 0x000321C6 File Offset: 0x000303C6
		// (set) Token: 0x06000D0C RID: 3340 RVA: 0x000321CE File Offset: 0x000303CE
		public StreamingContext Context
		{
			get
			{
				return this.context;
			}
			set
			{
				this.context = value;
			}
		}

		/// <summary>Gets or sets an object that controls class loading.</summary>
		/// <returns>The <see cref="T:System.Runtime.Serialization.SerializationBinder" /> used with the current formatter.</returns>
		// Token: 0x170002BC RID: 700
		// (get) Token: 0x06000D0D RID: 3341 RVA: 0x000321D7 File Offset: 0x000303D7
		// (set) Token: 0x06000D0E RID: 3342 RVA: 0x000321DF File Offset: 0x000303DF
		public SerializationBinder Binder
		{
			get
			{
				return this.binder;
			}
			set
			{
				this.binder = value;
			}
		}

		/// <summary>Gets or sets an object that assists the formatter when selecting a surrogate for serialization. </summary>
		/// <returns>An <see cref="T:System.Runtime.Serialization.ISurrogateSelector" /> for selecting a surrogate.</returns>
		// Token: 0x170002BD RID: 701
		// (get) Token: 0x06000D0F RID: 3343 RVA: 0x000321E8 File Offset: 0x000303E8
		// (set) Token: 0x06000D10 RID: 3344 RVA: 0x000321F0 File Offset: 0x000303F0
		public ISurrogateSelector SurrogateSelector
		{
			get
			{
				return this.surrogateSelector;
			}
			set
			{
				this.surrogateSelector = value;
			}
		}

		/// <summary>Gets a value that specifies a method for locating and loading assemblies.</summary>
		/// <returns>A <see cref="T:System.Runtime.Serialization.Formatters.FormatterAssemblyStyle" /> enumeration value that specifies a method for locating and loading assemblies.</returns>
		/// <exception cref="T:System.ArgumentException">The value being set does not correspond to any of the <see cref="T:System.Runtime.Serialization.Formatters.FormatterAssemblyStyle" /> values. </exception>
		// Token: 0x170002BE RID: 702
		// (get) Token: 0x06000D11 RID: 3345 RVA: 0x000321F9 File Offset: 0x000303F9
		// (set) Token: 0x06000D12 RID: 3346 RVA: 0x00032201 File Offset: 0x00030401
		public FormatterAssemblyStyle AssemblyFormat
		{
			get
			{
				return this.assemblyFormat;
			}
			set
			{
				if (value != FormatterAssemblyStyle.Full && value != FormatterAssemblyStyle.Simple)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentException(SR.GetString("'{0}': invalid assembly format.", new object[]
					{
						value
					})));
				}
				this.assemblyFormat = value;
			}
		}

		/// <summary>Gets the maximum number of items allowed in the object to be serialized.</summary>
		/// <returns>The maximum number of items allowed in the object. The default is <see cref="F:System.Int32.MaxValue" />.</returns>
		// Token: 0x170002BF RID: 703
		// (get) Token: 0x06000D13 RID: 3347 RVA: 0x00032235 File Offset: 0x00030435
		public int MaxItemsInObjectGraph
		{
			get
			{
				return this.maxItemsInObjectGraph;
			}
		}

		/// <summary>Gets a value that specifies whether data supplied by an extension of the object is ignored.</summary>
		/// <returns>
		///     <see langword="true" /> to ignore the data supplied by an extension of the type; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x06000D14 RID: 3348 RVA: 0x0003223D File Offset: 0x0003043D
		public bool IgnoreExtensionDataObject
		{
			get
			{
				return this.ignoreExtensionDataObject;
			}
		}

		/// <summary>Serializes the specified object graph using the specified writer.</summary>
		/// <param name="stream">The <see cref="T:System.IO.Stream" /> to serialize with.</param>
		/// <param name="graph">The object to serialize. All child objects of this root object are automatically serialized.</param>
		// Token: 0x06000D15 RID: 3349 RVA: 0x00032245 File Offset: 0x00030445
		public void Serialize(Stream stream, object graph)
		{
			base.WriteObject(stream, graph);
		}

		/// <summary>Deserializes an XML document or stream into an object.</summary>
		/// <param name="stream">A <see cref="T:System.IO.Stream" /> that contains the XML to deserialize.</param>
		/// <returns>The deserialized object.</returns>
		// Token: 0x06000D16 RID: 3350 RVA: 0x0003224F File Offset: 0x0003044F
		public object Deserialize(Stream stream)
		{
			return base.ReadObject(stream);
		}

		// Token: 0x06000D17 RID: 3351 RVA: 0x00032258 File Offset: 0x00030458
		internal override void InternalWriteObject(XmlWriterDelegator writer, object graph)
		{
			Hashtable surrogateDataContracts = null;
			DataContract dataContract = this.GetDataContract(graph, ref surrogateDataContracts);
			this.InternalWriteStartObject(writer, graph, dataContract);
			this.InternalWriteObjectContent(writer, graph, dataContract, surrogateDataContracts);
			this.InternalWriteEndObject(writer);
		}

		/// <summary>Writes the complete content (start, content, and end) of the object to the XML document or stream with the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">An <see cref="T:System.Xml.XmlWriter" /> used to write the XML document or stream.</param>
		/// <param name="graph">The object containing the content to write.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of object to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000D18 RID: 3352 RVA: 0x0002CB1A File Offset: 0x0002AD1A
		public override void WriteObject(XmlWriter writer, object graph)
		{
			base.WriteObjectHandleExceptions(new XmlWriterDelegator(writer), graph);
		}

		/// <summary>Writes the opening XML element using an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlDictionaryWriter" /> used to write the XML element.</param>
		/// <param name="graph">The object to serialize. All child objects of this root object are automatically serialized.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of object to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000D19 RID: 3353 RVA: 0x0002CB29 File Offset: 0x0002AD29
		public override void WriteStartObject(XmlWriter writer, object graph)
		{
			base.WriteStartObjectHandleExceptions(new XmlWriterDelegator(writer), graph);
		}

		/// <summary>Writes the XML content using an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> used to write the XML content.</param>
		/// <param name="graph">The object to serialize. All child objects of this root object are automatically serialized.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of object to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000D1A RID: 3354 RVA: 0x0002CB38 File Offset: 0x0002AD38
		public override void WriteObjectContent(XmlWriter writer, object graph)
		{
			base.WriteObjectContentHandleExceptions(new XmlWriterDelegator(writer), graph);
		}

		/// <summary>Writes the closing XML element using an <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> used to write the XML document or stream.</param>
		/// <exception cref="T:System.ArgumentNullException">the <paramref name="writer" /> is set to <see langword="null" />.</exception>
		// Token: 0x06000D1B RID: 3355 RVA: 0x0002CB47 File Offset: 0x0002AD47
		public override void WriteEndObject(XmlWriter writer)
		{
			base.WriteEndObjectHandleExceptions(new XmlWriterDelegator(writer));
		}

		/// <summary>Writes the opening XML element using an <see cref="T:System.Xml.XmlDictionaryWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlDictionaryWriter" /> used to write the XML element.</param>
		/// <param name="graph">The object to serialize. All child objects of this root object are automatically serialized.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of object to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000D1C RID: 3356 RVA: 0x0002CB29 File Offset: 0x0002AD29
		public override void WriteStartObject(XmlDictionaryWriter writer, object graph)
		{
			base.WriteStartObjectHandleExceptions(new XmlWriterDelegator(writer), graph);
		}

		// Token: 0x06000D1D RID: 3357 RVA: 0x0003228C File Offset: 0x0003048C
		internal override void InternalWriteStartObject(XmlWriterDelegator writer, object graph)
		{
			Hashtable hashtable = null;
			DataContract dataContract = this.GetDataContract(graph, ref hashtable);
			this.InternalWriteStartObject(writer, graph, dataContract);
		}

		// Token: 0x06000D1E RID: 3358 RVA: 0x000322B0 File Offset: 0x000304B0
		private void InternalWriteStartObject(XmlWriterDelegator writer, object graph, DataContract contract)
		{
			base.WriteRootElement(writer, contract, this.rootName, this.rootNamespace, base.CheckIfNeedsContractNsAtRoot(this.rootName, this.rootNamespace, contract));
		}

		/// <summary>Writes the XML content using an <see cref="T:System.Xml.XmlDictionaryWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlDictionaryWriter" /> used to write the XML content.</param>
		/// <param name="graph">The object to serialize. All child objects of this root object are automatically serialized.</param>
		/// <exception cref="T:System.Runtime.Serialization.InvalidDataContractException">the type being serialized does not conform to data contract rules. For example, the <see cref="T:System.Runtime.Serialization.DataContractAttribute" /> attribute has not been applied to the type.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">there is a problem with the instance being serialized.</exception>
		/// <exception cref="T:System.ServiceModel.QuotaExceededException">the maximum number of object to serialize has been exceeded. Check the <see cref="P:System.Runtime.Serialization.DataContractSerializer.MaxItemsInObjectGraph" /> property.</exception>
		// Token: 0x06000D1F RID: 3359 RVA: 0x0002CB38 File Offset: 0x0002AD38
		public override void WriteObjectContent(XmlDictionaryWriter writer, object graph)
		{
			base.WriteObjectContentHandleExceptions(new XmlWriterDelegator(writer), graph);
		}

		// Token: 0x06000D20 RID: 3360 RVA: 0x000322E4 File Offset: 0x000304E4
		internal override void InternalWriteObjectContent(XmlWriterDelegator writer, object graph)
		{
			Hashtable surrogateDataContracts = null;
			DataContract dataContract = this.GetDataContract(graph, ref surrogateDataContracts);
			this.InternalWriteObjectContent(writer, graph, dataContract, surrogateDataContracts);
		}

		// Token: 0x06000D21 RID: 3361 RVA: 0x00032308 File Offset: 0x00030508
		private void InternalWriteObjectContent(XmlWriterDelegator writer, object graph, DataContract contract, Hashtable surrogateDataContracts)
		{
			if (this.MaxItemsInObjectGraph == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Maximum number of items that can be serialized or deserialized in an object graph is '{0}'.", new object[]
				{
					this.MaxItemsInObjectGraph
				})));
			}
			if (base.IsRootXmlAny(this.rootName, contract))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("For type '{0}', IsAny is not supported by NetDataContractSerializer.", new object[]
				{
					contract.UnderlyingType
				})));
			}
			if (graph == null)
			{
				XmlObjectSerializer.WriteNull(writer);
				return;
			}
			Type type = graph.GetType();
			if (contract.UnderlyingType != type)
			{
				contract = this.GetDataContract(graph, ref surrogateDataContracts);
			}
			XmlObjectSerializerWriteContext xmlObjectSerializerWriteContext = null;
			if (contract.CanContainReferences)
			{
				xmlObjectSerializerWriteContext = XmlObjectSerializerWriteContext.CreateContext(this, surrogateDataContracts);
				xmlObjectSerializerWriteContext.HandleGraphAtTopLevel(writer, graph, contract);
			}
			NetDataContractSerializer.WriteClrTypeInfo(writer, contract, this.binder);
			contract.WriteXmlValue(writer, graph, xmlObjectSerializerWriteContext);
		}

		// Token: 0x06000D22 RID: 3362 RVA: 0x000323D8 File Offset: 0x000305D8
		internal static void WriteClrTypeInfo(XmlWriterDelegator writer, DataContract dataContract, SerializationBinder binder)
		{
			if (!dataContract.IsISerializable && !(dataContract is SurrogateDataContract))
			{
				TypeInformation typeInformation = null;
				Type originalUnderlyingType = dataContract.OriginalUnderlyingType;
				string text = null;
				string text2 = null;
				if (binder != null)
				{
					binder.BindToName(originalUnderlyingType, out text2, out text);
				}
				if (text == null)
				{
					typeInformation = NetDataContractSerializer.GetTypeInformation(originalUnderlyingType);
					text = typeInformation.FullTypeName;
				}
				if (text2 == null)
				{
					text2 = ((typeInformation == null) ? NetDataContractSerializer.GetTypeInformation(originalUnderlyingType).AssemblyString : typeInformation.AssemblyString);
					if (!NetDataContractSerializer.UnsafeTypeForwardingEnabled && !originalUnderlyingType.Assembly.IsFullyTrusted && !NetDataContractSerializer.IsAssemblyNameForwardingSafe(originalUnderlyingType.Assembly.FullName, text2))
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Type '{0}' in assembly '{1}' cannot be forwarded from assembly '{2}'.", new object[]
						{
							DataContract.GetClrTypeFullName(originalUnderlyingType),
							originalUnderlyingType.Assembly.FullName,
							text2
						})));
					}
				}
				NetDataContractSerializer.WriteClrTypeInfo(writer, text, text2);
			}
		}

		// Token: 0x06000D23 RID: 3363 RVA: 0x000324A8 File Offset: 0x000306A8
		internal static void WriteClrTypeInfo(XmlWriterDelegator writer, Type dataContractType, SerializationBinder binder, string defaultClrTypeName, string defaultClrAssemblyName)
		{
			string text = null;
			string text2 = null;
			if (binder != null)
			{
				binder.BindToName(dataContractType, out text2, out text);
			}
			if (text == null)
			{
				text = defaultClrTypeName;
			}
			if (text2 == null)
			{
				text2 = defaultClrAssemblyName;
			}
			NetDataContractSerializer.WriteClrTypeInfo(writer, text, text2);
		}

		// Token: 0x06000D24 RID: 3364 RVA: 0x000324DC File Offset: 0x000306DC
		internal static void WriteClrTypeInfo(XmlWriterDelegator writer, Type dataContractType, SerializationBinder binder, SerializationInfo serInfo)
		{
			TypeInformation typeInformation = null;
			string text = null;
			string text2 = null;
			if (binder != null)
			{
				binder.BindToName(dataContractType, out text2, out text);
			}
			if (text == null)
			{
				if (serInfo.IsFullTypeNameSetExplicit)
				{
					text = serInfo.FullTypeName;
				}
				else
				{
					typeInformation = NetDataContractSerializer.GetTypeInformation(serInfo.ObjectType);
					text = typeInformation.FullTypeName;
				}
			}
			if (text2 == null)
			{
				if (serInfo.IsAssemblyNameSetExplicit)
				{
					text2 = serInfo.AssemblyName;
				}
				else
				{
					text2 = ((typeInformation == null) ? NetDataContractSerializer.GetTypeInformation(serInfo.ObjectType).AssemblyString : typeInformation.AssemblyString);
				}
			}
			NetDataContractSerializer.WriteClrTypeInfo(writer, text, text2);
		}

		// Token: 0x06000D25 RID: 3365 RVA: 0x0003255C File Offset: 0x0003075C
		private static void WriteClrTypeInfo(XmlWriterDelegator writer, string clrTypeName, string clrAssemblyName)
		{
			if (clrTypeName != null)
			{
				writer.WriteAttributeString("z", DictionaryGlobals.ClrTypeLocalName, DictionaryGlobals.SerializationNamespace, DataContract.GetClrTypeString(clrTypeName));
			}
			if (clrAssemblyName != null)
			{
				writer.WriteAttributeString("z", DictionaryGlobals.ClrAssemblyLocalName, DictionaryGlobals.SerializationNamespace, DataContract.GetClrTypeString(clrAssemblyName));
			}
		}

		/// <summary>Writes the closing XML element using an <see cref="T:System.Xml.XmlDictionaryWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlDictionaryWriter" /> used to write the XML document or stream.</param>
		/// <exception cref="T:System.ArgumentNullException">the <paramref name="writer" /> is set to <see langword="null" />.</exception>
		// Token: 0x06000D26 RID: 3366 RVA: 0x0002CB47 File Offset: 0x0002AD47
		public override void WriteEndObject(XmlDictionaryWriter writer)
		{
			base.WriteEndObjectHandleExceptions(new XmlWriterDelegator(writer));
		}

		// Token: 0x06000D27 RID: 3367 RVA: 0x0003259A File Offset: 0x0003079A
		internal override void InternalWriteEndObject(XmlWriterDelegator writer)
		{
			writer.WriteEndElement();
		}

		/// <summary>Reads the XML stream or document with an <see cref="T:System.Xml.XmlDictionaryReader" /> and returns the deserialized object.</summary>
		/// <param name="reader">The <see cref="T:System.Xml.XmlReader" /> used to read the XML stream or document.</param>
		/// <returns>The deserialized object.</returns>
		/// <exception cref="T:System.ArgumentNullException">the <paramref name="reader" /> is set to <see langword="null" />.</exception>
		// Token: 0x06000D28 RID: 3368 RVA: 0x0002CB65 File Offset: 0x0002AD65
		public override object ReadObject(XmlReader reader)
		{
			return base.ReadObjectHandleExceptions(new XmlReaderDelegator(reader), true);
		}

		/// <summary>Reads the XML stream or document with an <see cref="T:System.Xml.XmlDictionaryReader" /> and returns the deserialized object; also checks whether the object data conforms to the name and namespace used to create the serializer.</summary>
		/// <param name="reader">The <see cref="T:System.Xml.XmlReader" /> used to read the XML stream or document.</param>
		/// <param name="verifyObjectName">
		///       <see langword="true" /> to check whether the enclosing XML element name and namespace correspond to the root name and root namespace used to construct the serializer; <see langword="false" /> to skip the verification.</param>
		/// <returns>The deserialized object.</returns>
		/// <exception cref="T:System.ArgumentNullException">the <paramref name="reader" /> is set to <see langword="null" />.</exception>
		// Token: 0x06000D29 RID: 3369 RVA: 0x0002CB74 File Offset: 0x0002AD74
		public override object ReadObject(XmlReader reader, bool verifyObjectName)
		{
			return base.ReadObjectHandleExceptions(new XmlReaderDelegator(reader), verifyObjectName);
		}

		/// <summary>Determines whether the <see cref="T:System.Xml.XmlReader" /> is positioned on an object that can be deserialized using the specified reader.</summary>
		/// <param name="reader">An <see cref="T:System.Xml.XmlReader" /> that contains the XML to read.</param>
		/// <returns>
		///     <see langword="true" /> if the reader is at the start element of the stream to read; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">the <paramref name="reader" /> is set to <see langword="null" />.</exception>
		// Token: 0x06000D2A RID: 3370 RVA: 0x0002CB83 File Offset: 0x0002AD83
		public override bool IsStartObject(XmlReader reader)
		{
			return base.IsStartObjectHandleExceptions(new XmlReaderDelegator(reader));
		}

		/// <summary>Reads the XML stream or document with an <see cref="T:System.Xml.XmlDictionaryReader" /> and returns the deserialized object; also checks whether the object data conforms to the name and namespace used to create the serializer.</summary>
		/// <param name="reader">The <see cref="T:System.Xml.XmlDictionaryReader" /> used to read the XML stream or document.</param>
		/// <param name="verifyObjectName">
		///       <see langword="true" /> to check whether the enclosing XML element name and namespace correspond to the root name and root namespace used to construct the serializer; <see langword="false" /> to skip the verification.</param>
		/// <returns>The deserialized object.</returns>
		/// <exception cref="T:System.ArgumentNullException">the <paramref name="reader" /> is set to <see langword="null" />.</exception>
		// Token: 0x06000D2B RID: 3371 RVA: 0x0002CB74 File Offset: 0x0002AD74
		public override object ReadObject(XmlDictionaryReader reader, bool verifyObjectName)
		{
			return base.ReadObjectHandleExceptions(new XmlReaderDelegator(reader), verifyObjectName);
		}

		/// <summary>Determines whether the <see cref="T:System.Xml.XmlDictionaryReader" /> is positioned on an object that can be deserialized using the specified reader.</summary>
		/// <param name="reader">An <see cref="T:System.Xml.XmlDictionaryReader" /> that contains the XML to read.</param>
		/// <returns>
		///     <see langword="true" />, if the reader is at the start element of the stream to read; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">the <paramref name="reader" /> is set to <see langword="null" />.</exception>
		// Token: 0x06000D2C RID: 3372 RVA: 0x0002CB83 File Offset: 0x0002AD83
		public override bool IsStartObject(XmlDictionaryReader reader)
		{
			return base.IsStartObjectHandleExceptions(new XmlReaderDelegator(reader));
		}

		// Token: 0x06000D2D RID: 3373 RVA: 0x000325A4 File Offset: 0x000307A4
		internal override object InternalReadObject(XmlReaderDelegator xmlReader, bool verifyObjectName)
		{
			if (this.MaxItemsInObjectGraph == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Maximum number of items that can be serialized or deserialized in an object graph is '{0}'.", new object[]
				{
					this.MaxItemsInObjectGraph
				})));
			}
			if (!base.IsStartElement(xmlReader))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationExceptionWithReaderDetails(SR.GetString("Expecting state '{0}' when ReadObject is called.", new object[]
				{
					XmlNodeType.Element
				}), xmlReader));
			}
			return XmlObjectSerializerReadContext.CreateContext(this).InternalDeserialize(xmlReader, null, null, null);
		}

		// Token: 0x06000D2E RID: 3374 RVA: 0x0003261F File Offset: 0x0003081F
		internal override bool InternalIsStartObject(XmlReaderDelegator reader)
		{
			return base.IsStartElement(reader);
		}

		// Token: 0x06000D2F RID: 3375 RVA: 0x00032628 File Offset: 0x00030828
		internal DataContract GetDataContract(object obj, ref Hashtable surrogateDataContracts)
		{
			return this.GetDataContract((obj == null) ? Globals.TypeOfObject : obj.GetType(), ref surrogateDataContracts);
		}

		// Token: 0x06000D30 RID: 3376 RVA: 0x00032641 File Offset: 0x00030841
		internal DataContract GetDataContract(Type type, ref Hashtable surrogateDataContracts)
		{
			return this.GetDataContract(type.TypeHandle, type, ref surrogateDataContracts);
		}

		// Token: 0x06000D31 RID: 3377 RVA: 0x00032654 File Offset: 0x00030854
		internal DataContract GetDataContract(RuntimeTypeHandle typeHandle, Type type, ref Hashtable surrogateDataContracts)
		{
			DataContract dataContract = NetDataContractSerializer.GetDataContractFromSurrogateSelector(this.surrogateSelector, this.Context, typeHandle, type, ref surrogateDataContracts);
			if (dataContract != null)
			{
				return dataContract;
			}
			if (this.cachedDataContract == null)
			{
				dataContract = DataContract.GetDataContract(typeHandle, type, SerializationMode.SharedType);
				this.cachedDataContract = dataContract;
				return dataContract;
			}
			DataContract dataContract2 = this.cachedDataContract;
			if (dataContract2.UnderlyingType.TypeHandle.Equals(typeHandle))
			{
				return dataContract2;
			}
			return DataContract.GetDataContract(typeHandle, type, SerializationMode.SharedType);
		}

		// Token: 0x06000D32 RID: 3378 RVA: 0x000326BC File Offset: 0x000308BC
		[SecuritySafeCritical]
		[PermissionSet(SecurityAction.Demand, Unrestricted = true)]
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static ISerializationSurrogate GetSurrogate(Type type, ISurrogateSelector surrogateSelector, StreamingContext context)
		{
			ISurrogateSelector surrogateSelector2;
			return surrogateSelector.GetSurrogate(type, context, out surrogateSelector2);
		}

		// Token: 0x06000D33 RID: 3379 RVA: 0x000326D4 File Offset: 0x000308D4
		internal static DataContract GetDataContractFromSurrogateSelector(ISurrogateSelector surrogateSelector, StreamingContext context, RuntimeTypeHandle typeHandle, Type type, ref Hashtable surrogateDataContracts)
		{
			if (surrogateSelector == null)
			{
				return null;
			}
			if (type == null)
			{
				type = Type.GetTypeFromHandle(typeHandle);
			}
			DataContract builtInDataContract = DataContract.GetBuiltInDataContract(type);
			if (builtInDataContract != null)
			{
				return builtInDataContract;
			}
			if (surrogateDataContracts != null)
			{
				DataContract dataContract = (DataContract)surrogateDataContracts[type];
				if (dataContract != null)
				{
					return dataContract;
				}
			}
			DataContract dataContract2 = null;
			ISerializationSurrogate surrogate = NetDataContractSerializer.GetSurrogate(type, surrogateSelector, context);
			if (surrogate != null)
			{
				dataContract2 = new SurrogateDataContract(type, surrogate);
			}
			else if (type.IsArray)
			{
				Type elementType = type.GetElementType();
				DataContract dataContract3 = NetDataContractSerializer.GetDataContractFromSurrogateSelector(surrogateSelector, context, elementType.TypeHandle, elementType, ref surrogateDataContracts);
				if (dataContract3 == null)
				{
					dataContract3 = DataContract.GetDataContract(elementType.TypeHandle, elementType, SerializationMode.SharedType);
				}
				dataContract2 = new CollectionDataContract(type, dataContract3);
			}
			if (dataContract2 != null)
			{
				if (surrogateDataContracts == null)
				{
					surrogateDataContracts = new Hashtable();
				}
				surrogateDataContracts.Add(type, dataContract2);
				return dataContract2;
			}
			return null;
		}

		// Token: 0x06000D34 RID: 3380 RVA: 0x00032794 File Offset: 0x00030994
		internal static TypeInformation GetTypeInformation(Type type)
		{
			TypeInformation typeInformation = null;
			object obj = NetDataContractSerializer.typeNameCache[type];
			if (obj == null)
			{
				bool hasTypeForwardedFrom;
				string clrAssemblyName = DataContract.GetClrAssemblyName(type, out hasTypeForwardedFrom);
				typeInformation = new TypeInformation(DataContract.GetClrTypeFullNameUsingTypeForwardedFromAttribute(type), clrAssemblyName, hasTypeForwardedFrom);
				Hashtable obj2 = NetDataContractSerializer.typeNameCache;
				lock (obj2)
				{
					NetDataContractSerializer.typeNameCache[type] = typeInformation;
					return typeInformation;
				}
			}
			typeInformation = (TypeInformation)obj;
			return typeInformation;
		}

		// Token: 0x06000D35 RID: 3381 RVA: 0x00032810 File Offset: 0x00030A10
		private static bool IsAssemblyNameForwardingSafe(string originalAssemblyName, string newAssemblyName)
		{
			if (originalAssemblyName == newAssemblyName)
			{
				return true;
			}
			AssemblyName assemblyName = new AssemblyName(originalAssemblyName);
			AssemblyName assemblyName2 = new AssemblyName(newAssemblyName);
			return !string.Equals(assemblyName2.Name, "mscorlib", StringComparison.OrdinalIgnoreCase) && !string.Equals(assemblyName2.Name, "mscorlib.dll", StringComparison.OrdinalIgnoreCase) && NetDataContractSerializer.IsPublicKeyTokenForwardingSafe(assemblyName.GetPublicKeyToken(), assemblyName2.GetPublicKeyToken());
		}

		// Token: 0x06000D36 RID: 3382 RVA: 0x00032870 File Offset: 0x00030A70
		private static bool IsPublicKeyTokenForwardingSafe(byte[] sourceToken, byte[] destinationToken)
		{
			if (sourceToken == null || destinationToken == null || sourceToken.Length == 0 || destinationToken.Length == 0 || sourceToken.Length != destinationToken.Length)
			{
				return false;
			}
			for (int i = 0; i < sourceToken.Length; i++)
			{
				if (sourceToken[i] != destinationToken[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000D37 RID: 3383 RVA: 0x000328AE File Offset: 0x00030AAE
		// Note: this type is marked as 'beforefieldinit'.
		static NetDataContractSerializer()
		{
		}

		// Token: 0x04000628 RID: 1576
		private XmlDictionaryString rootName;

		// Token: 0x04000629 RID: 1577
		private XmlDictionaryString rootNamespace;

		// Token: 0x0400062A RID: 1578
		private StreamingContext context;

		// Token: 0x0400062B RID: 1579
		private SerializationBinder binder;

		// Token: 0x0400062C RID: 1580
		private ISurrogateSelector surrogateSelector;

		// Token: 0x0400062D RID: 1581
		private int maxItemsInObjectGraph;

		// Token: 0x0400062E RID: 1582
		private bool ignoreExtensionDataObject;

		// Token: 0x0400062F RID: 1583
		private FormatterAssemblyStyle assemblyFormat;

		// Token: 0x04000630 RID: 1584
		private DataContract cachedDataContract;

		// Token: 0x04000631 RID: 1585
		private static Hashtable typeNameCache = new Hashtable();

		// Token: 0x04000632 RID: 1586
		private static bool? unsafeTypeForwardingEnabled;
	}
}
