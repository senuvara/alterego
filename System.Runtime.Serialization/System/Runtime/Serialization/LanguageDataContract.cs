﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x0200010D RID: 269
	internal class LanguageDataContract : StringDataContract
	{
		// Token: 0x06000DBD RID: 3517 RVA: 0x0003383C File Offset: 0x00031A3C
		internal LanguageDataContract() : base(DictionaryGlobals.languageLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
