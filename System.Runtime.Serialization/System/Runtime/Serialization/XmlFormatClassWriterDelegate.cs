﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000136 RID: 310
	// (Invoke) Token: 0x06000EBB RID: 3771
	internal delegate void XmlFormatClassWriterDelegate(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContext context, ClassDataContract dataContract);
}
