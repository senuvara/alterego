﻿using System;
using System.Collections.Generic;

namespace System.Runtime.Serialization
{
	// Token: 0x020000D4 RID: 212
	internal class ClassDataNode : DataNode<object>
	{
		// Token: 0x06000BFA RID: 3066 RVA: 0x0002F244 File Offset: 0x0002D444
		internal ClassDataNode()
		{
			this.dataType = Globals.TypeOfClassDataNode;
		}

		// Token: 0x1700021A RID: 538
		// (get) Token: 0x06000BFB RID: 3067 RVA: 0x0002F257 File Offset: 0x0002D457
		// (set) Token: 0x06000BFC RID: 3068 RVA: 0x0002F25F File Offset: 0x0002D45F
		internal IList<ExtensionDataMember> Members
		{
			get
			{
				return this.members;
			}
			set
			{
				this.members = value;
			}
		}

		// Token: 0x06000BFD RID: 3069 RVA: 0x0002F268 File Offset: 0x0002D468
		public override void Clear()
		{
			base.Clear();
			this.members = null;
		}

		// Token: 0x04000512 RID: 1298
		private IList<ExtensionDataMember> members;
	}
}
