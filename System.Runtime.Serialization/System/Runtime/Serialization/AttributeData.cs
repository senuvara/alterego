﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000DB RID: 219
	internal class AttributeData
	{
		// Token: 0x06000C5C RID: 3164 RVA: 0x00002217 File Offset: 0x00000417
		public AttributeData()
		{
		}

		// Token: 0x0400053A RID: 1338
		public string prefix;

		// Token: 0x0400053B RID: 1339
		public string ns;

		// Token: 0x0400053C RID: 1340
		public string localName;

		// Token: 0x0400053D RID: 1341
		public string value;
	}
}
