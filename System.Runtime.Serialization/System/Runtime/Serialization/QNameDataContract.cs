﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x0200011E RID: 286
	internal class QNameDataContract : PrimitiveDataContract
	{
		// Token: 0x06000DE6 RID: 3558 RVA: 0x00033B4A File Offset: 0x00031D4A
		internal QNameDataContract() : base(typeof(XmlQualifiedName), DictionaryGlobals.QNameLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x06000DE7 RID: 3559 RVA: 0x00033B66 File Offset: 0x00031D66
		internal override string WriteMethodName
		{
			get
			{
				return "WriteQName";
			}
		}

		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x06000DE8 RID: 3560 RVA: 0x00033B6D File Offset: 0x00031D6D
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsQName";
			}
		}

		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06000DE9 RID: 3561 RVA: 0x0000310F File Offset: 0x0000130F
		internal override bool IsPrimitive
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000DEA RID: 3562 RVA: 0x00033B74 File Offset: 0x00031D74
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteQName((XmlQualifiedName)obj);
		}

		// Token: 0x06000DEB RID: 3563 RVA: 0x00033B82 File Offset: 0x00031D82
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsQName(), context);
			}
			if (!base.TryReadNullAtTopLevel(reader))
			{
				return reader.ReadElementContentAsQName();
			}
			return null;
		}

		// Token: 0x06000DEC RID: 3564 RVA: 0x00033BA8 File Offset: 0x00031DA8
		internal override void WriteRootElement(XmlWriterDelegator writer, XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (ns == DictionaryGlobals.SerializationNamespace)
			{
				writer.WriteStartElement("z", name, ns);
				return;
			}
			if (ns != null && ns.Value != null && ns.Value.Length > 0)
			{
				writer.WriteStartElement("q", name, ns);
				return;
			}
			writer.WriteStartElement(name, ns);
		}
	}
}
