﻿using System;

namespace System.Runtime.Serialization
{
	/// <summary>Specifies types that should be recognized by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" /> when serializing or deserializing a given type. </summary>
	// Token: 0x020000E7 RID: 231
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = true, AllowMultiple = true)]
	public sealed class KnownTypeAttribute : Attribute
	{
		// Token: 0x06000CF9 RID: 3321 RVA: 0x000254EB File Offset: 0x000236EB
		private KnownTypeAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.KnownTypeAttribute" /> class with the specified type. </summary>
		/// <param name="type">The <see cref="T:System.Type" /> that is included as a known type when serializing or deserializing data.</param>
		// Token: 0x06000CFA RID: 3322 RVA: 0x00031F97 File Offset: 0x00030197
		public KnownTypeAttribute(Type type)
		{
			this.type = type;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.KnownTypeAttribute" /> class with the name of a method that returns an <see cref="T:System.Collections.IEnumerable" /> of known types. </summary>
		/// <param name="methodName">The name of the method that returns an <see cref="T:System.Collections.IEnumerable" /> of types used when serializing or deserializing data.</param>
		// Token: 0x06000CFB RID: 3323 RVA: 0x00031FA6 File Offset: 0x000301A6
		public KnownTypeAttribute(string methodName)
		{
			this.methodName = methodName;
		}

		/// <summary>Gets the name of a method that will return a list of types that should be recognized during serialization or deserialization. </summary>
		/// <returns>A <see cref="T:System.String" /> that contains the name of the method on the type defined by the <see cref="T:System.Runtime.Serialization.KnownTypeAttribute" /> class. </returns>
		// Token: 0x170002B8 RID: 696
		// (get) Token: 0x06000CFC RID: 3324 RVA: 0x00031FB5 File Offset: 0x000301B5
		public string MethodName
		{
			get
			{
				return this.methodName;
			}
		}

		/// <summary>Gets the type that should be recognized during serialization or deserialization by the <see cref="T:System.Runtime.Serialization.DataContractSerializer" />. </summary>
		/// <returns>The <see cref="T:System.Type" /> that is used during serialization or deserialization. </returns>
		// Token: 0x170002B9 RID: 697
		// (get) Token: 0x06000CFD RID: 3325 RVA: 0x00031FBD File Offset: 0x000301BD
		public Type Type
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x04000625 RID: 1573
		private string methodName;

		// Token: 0x04000626 RID: 1574
		private Type type;
	}
}
