﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000137 RID: 311
	// (Invoke) Token: 0x06000EBF RID: 3775
	internal delegate void XmlFormatCollectionWriterDelegate(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContext context, CollectionDataContract dataContract);
}
