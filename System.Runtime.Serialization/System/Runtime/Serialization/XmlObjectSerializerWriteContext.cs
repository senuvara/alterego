﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Diagnostics;
using System.Security;
using System.Xml;
using System.Xml.Serialization;

namespace System.Runtime.Serialization
{
	// Token: 0x02000143 RID: 323
	internal class XmlObjectSerializerWriteContext : XmlObjectSerializerContext
	{
		// Token: 0x06000F82 RID: 3970 RVA: 0x00038DAA File Offset: 0x00036FAA
		internal static XmlObjectSerializerWriteContext CreateContext(DataContractSerializer serializer, DataContract rootTypeDataContract, DataContractResolver dataContractResolver)
		{
			if (!serializer.PreserveObjectReferences && serializer.DataContractSurrogate == null)
			{
				return new XmlObjectSerializerWriteContext(serializer, rootTypeDataContract, dataContractResolver);
			}
			return new XmlObjectSerializerWriteContextComplex(serializer, rootTypeDataContract, dataContractResolver);
		}

		// Token: 0x06000F83 RID: 3971 RVA: 0x00038DCD File Offset: 0x00036FCD
		internal static XmlObjectSerializerWriteContext CreateContext(NetDataContractSerializer serializer, Hashtable surrogateDataContracts)
		{
			return new XmlObjectSerializerWriteContextComplex(serializer, surrogateDataContracts);
		}

		// Token: 0x06000F84 RID: 3972 RVA: 0x00038DD6 File Offset: 0x00036FD6
		protected XmlObjectSerializerWriteContext(DataContractSerializer serializer, DataContract rootTypeDataContract, DataContractResolver resolver) : base(serializer, rootTypeDataContract, resolver)
		{
			this.serializeReadOnlyTypes = serializer.SerializeReadOnlyTypes;
			this.unsafeTypeForwardingEnabled = true;
		}

		// Token: 0x06000F85 RID: 3973 RVA: 0x00038DF4 File Offset: 0x00036FF4
		protected XmlObjectSerializerWriteContext(NetDataContractSerializer serializer) : base(serializer)
		{
			this.unsafeTypeForwardingEnabled = NetDataContractSerializer.UnsafeTypeForwardingEnabled;
		}

		// Token: 0x06000F86 RID: 3974 RVA: 0x00038E08 File Offset: 0x00037008
		internal XmlObjectSerializerWriteContext(XmlObjectSerializer serializer, int maxItemsInObjectGraph, StreamingContext streamingContext, bool ignoreExtensionDataObject) : base(serializer, maxItemsInObjectGraph, streamingContext, ignoreExtensionDataObject)
		{
			this.unsafeTypeForwardingEnabled = true;
		}

		// Token: 0x1700037B RID: 891
		// (get) Token: 0x06000F87 RID: 3975 RVA: 0x00038E1C File Offset: 0x0003701C
		protected ObjectToIdCache SerializedObjects
		{
			get
			{
				if (this.serializedObjects == null)
				{
					this.serializedObjects = new ObjectToIdCache();
				}
				return this.serializedObjects;
			}
		}

		// Token: 0x1700037C RID: 892
		// (get) Token: 0x06000F88 RID: 3976 RVA: 0x00038E37 File Offset: 0x00037037
		// (set) Token: 0x06000F89 RID: 3977 RVA: 0x00038E3F File Offset: 0x0003703F
		internal override bool IsGetOnlyCollection
		{
			get
			{
				return this.isGetOnlyCollection;
			}
			set
			{
				this.isGetOnlyCollection = value;
			}
		}

		// Token: 0x1700037D RID: 893
		// (get) Token: 0x06000F8A RID: 3978 RVA: 0x00038E48 File Offset: 0x00037048
		internal bool SerializeReadOnlyTypes
		{
			get
			{
				return this.serializeReadOnlyTypes;
			}
		}

		// Token: 0x1700037E RID: 894
		// (get) Token: 0x06000F8B RID: 3979 RVA: 0x00038E50 File Offset: 0x00037050
		internal bool UnsafeTypeForwardingEnabled
		{
			get
			{
				return this.unsafeTypeForwardingEnabled;
			}
		}

		// Token: 0x06000F8C RID: 3980 RVA: 0x00038E58 File Offset: 0x00037058
		internal void StoreIsGetOnlyCollection()
		{
			this.isGetOnlyCollection = true;
		}

		// Token: 0x06000F8D RID: 3981 RVA: 0x00038E61 File Offset: 0x00037061
		public void InternalSerializeReference(XmlWriterDelegator xmlWriter, object obj, bool isDeclaredType, bool writeXsiType, int declaredTypeID, RuntimeTypeHandle declaredTypeHandle)
		{
			if (!this.OnHandleReference(xmlWriter, obj, true))
			{
				this.InternalSerialize(xmlWriter, obj, isDeclaredType, writeXsiType, declaredTypeID, declaredTypeHandle);
			}
			this.OnEndHandleReference(xmlWriter, obj, true);
		}

		// Token: 0x06000F8E RID: 3982 RVA: 0x00038E88 File Offset: 0x00037088
		public virtual void InternalSerialize(XmlWriterDelegator xmlWriter, object obj, bool isDeclaredType, bool writeXsiType, int declaredTypeID, RuntimeTypeHandle declaredTypeHandle)
		{
			if (writeXsiType)
			{
				Type typeOfObject = Globals.TypeOfObject;
				this.SerializeWithXsiType(xmlWriter, obj, Type.GetTypeHandle(obj), null, -1, typeOfObject.TypeHandle, typeOfObject);
				return;
			}
			if (isDeclaredType)
			{
				DataContract dataContract = this.GetDataContract(declaredTypeID, declaredTypeHandle);
				this.SerializeWithoutXsiType(dataContract, xmlWriter, obj, declaredTypeHandle);
				return;
			}
			RuntimeTypeHandle typeHandle = Type.GetTypeHandle(obj);
			if (declaredTypeHandle.Equals(typeHandle))
			{
				DataContract dataContract2 = (declaredTypeID >= 0) ? this.GetDataContract(declaredTypeID, declaredTypeHandle) : this.GetDataContract(declaredTypeHandle, null);
				this.SerializeWithoutXsiType(dataContract2, xmlWriter, obj, declaredTypeHandle);
				return;
			}
			this.SerializeWithXsiType(xmlWriter, obj, typeHandle, null, declaredTypeID, declaredTypeHandle, Type.GetTypeFromHandle(declaredTypeHandle));
		}

		// Token: 0x06000F8F RID: 3983 RVA: 0x00038F20 File Offset: 0x00037120
		internal void SerializeWithoutXsiType(DataContract dataContract, XmlWriterDelegator xmlWriter, object obj, RuntimeTypeHandle declaredTypeHandle)
		{
			if (this.OnHandleIsReference(xmlWriter, dataContract, obj))
			{
				return;
			}
			if (dataContract.KnownDataContracts != null)
			{
				this.scopedKnownTypes.Push(dataContract.KnownDataContracts);
				this.WriteDataContractValue(dataContract, xmlWriter, obj, declaredTypeHandle);
				this.scopedKnownTypes.Pop();
				return;
			}
			this.WriteDataContractValue(dataContract, xmlWriter, obj, declaredTypeHandle);
		}

		// Token: 0x06000F90 RID: 3984 RVA: 0x00038F74 File Offset: 0x00037174
		internal virtual void SerializeWithXsiTypeAtTopLevel(DataContract dataContract, XmlWriterDelegator xmlWriter, object obj, RuntimeTypeHandle originalDeclaredTypeHandle, Type graphType)
		{
			bool verifyKnownType = false;
			Type originalUnderlyingType = this.rootTypeDataContract.OriginalUnderlyingType;
			if (originalUnderlyingType.IsInterface && CollectionDataContract.IsCollectionInterface(originalUnderlyingType))
			{
				if (base.DataContractResolver != null)
				{
					this.WriteResolvedTypeInfo(xmlWriter, graphType, originalUnderlyingType);
				}
			}
			else if (!originalUnderlyingType.IsArray)
			{
				verifyKnownType = this.WriteTypeInfo(xmlWriter, dataContract, this.rootTypeDataContract);
			}
			this.SerializeAndVerifyType(dataContract, xmlWriter, obj, verifyKnownType, originalDeclaredTypeHandle, originalUnderlyingType);
		}

		// Token: 0x06000F91 RID: 3985 RVA: 0x00038FD8 File Offset: 0x000371D8
		protected virtual void SerializeWithXsiType(XmlWriterDelegator xmlWriter, object obj, RuntimeTypeHandle objectTypeHandle, Type objectType, int declaredTypeID, RuntimeTypeHandle declaredTypeHandle, Type declaredType)
		{
			bool verifyKnownType = false;
			DataContract dataContract;
			if (declaredType.IsInterface && CollectionDataContract.IsCollectionInterface(declaredType))
			{
				dataContract = this.GetDataContractSkipValidation(DataContract.GetId(objectTypeHandle), objectTypeHandle, objectType);
				if (this.OnHandleIsReference(xmlWriter, dataContract, obj))
				{
					return;
				}
				if (this.Mode == SerializationMode.SharedType && dataContract.IsValidContract(this.Mode))
				{
					dataContract = dataContract.GetValidContract(this.Mode);
				}
				else
				{
					dataContract = this.GetDataContract(declaredTypeHandle, declaredType);
				}
				if (!this.WriteClrTypeInfo(xmlWriter, dataContract) && base.DataContractResolver != null)
				{
					if (objectType == null)
					{
						objectType = Type.GetTypeFromHandle(objectTypeHandle);
					}
					this.WriteResolvedTypeInfo(xmlWriter, objectType, declaredType);
				}
			}
			else if (declaredType.IsArray)
			{
				dataContract = this.GetDataContract(objectTypeHandle, objectType);
				this.WriteClrTypeInfo(xmlWriter, dataContract);
				dataContract = this.GetDataContract(declaredTypeHandle, declaredType);
			}
			else
			{
				dataContract = this.GetDataContract(objectTypeHandle, objectType);
				if (this.OnHandleIsReference(xmlWriter, dataContract, obj))
				{
					return;
				}
				if (!this.WriteClrTypeInfo(xmlWriter, dataContract))
				{
					DataContract declaredContract = (declaredTypeID >= 0) ? this.GetDataContract(declaredTypeID, declaredTypeHandle) : this.GetDataContract(declaredTypeHandle, declaredType);
					verifyKnownType = this.WriteTypeInfo(xmlWriter, dataContract, declaredContract);
				}
			}
			this.SerializeAndVerifyType(dataContract, xmlWriter, obj, verifyKnownType, declaredTypeHandle, declaredType);
		}

		// Token: 0x06000F92 RID: 3986 RVA: 0x00039100 File Offset: 0x00037300
		internal bool OnHandleIsReference(XmlWriterDelegator xmlWriter, DataContract contract, object obj)
		{
			if (this.preserveObjectReferences || !contract.IsReference || this.isGetOnlyCollection)
			{
				return false;
			}
			bool flag = true;
			int id = this.SerializedObjects.GetId(obj, ref flag);
			this.byValObjectsInScope.EnsureSetAsIsReference(obj);
			if (flag)
			{
				xmlWriter.WriteAttributeString("z", DictionaryGlobals.IdLocalName, DictionaryGlobals.SerializationNamespace, string.Format(CultureInfo.InvariantCulture, "{0}{1}", "i", id));
				return false;
			}
			xmlWriter.WriteAttributeString("z", DictionaryGlobals.RefLocalName, DictionaryGlobals.SerializationNamespace, string.Format(CultureInfo.InvariantCulture, "{0}{1}", "i", id));
			return true;
		}

		// Token: 0x06000F93 RID: 3987 RVA: 0x000391A8 File Offset: 0x000373A8
		protected void SerializeAndVerifyType(DataContract dataContract, XmlWriterDelegator xmlWriter, object obj, bool verifyKnownType, RuntimeTypeHandle declaredTypeHandle, Type declaredType)
		{
			bool flag = false;
			if (dataContract.KnownDataContracts != null)
			{
				this.scopedKnownTypes.Push(dataContract.KnownDataContracts);
				flag = true;
			}
			if (verifyKnownType && !base.IsKnownType(dataContract, declaredType))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Type '{0}' with data contract name '{1}:{2}' is not expected. Add any types not known statically to the list of known types - for example, by using the KnownTypeAttribute attribute or by adding them to the list of known types passed to DataContractSerializer.", new object[]
				{
					DataContract.GetClrTypeFullName(dataContract.UnderlyingType),
					dataContract.StableName.Name,
					dataContract.StableName.Namespace
				})));
			}
			this.WriteDataContractValue(dataContract, xmlWriter, obj, declaredTypeHandle);
			if (flag)
			{
				this.scopedKnownTypes.Pop();
			}
		}

		// Token: 0x06000F94 RID: 3988 RVA: 0x0000310F File Offset: 0x0000130F
		internal virtual bool WriteClrTypeInfo(XmlWriterDelegator xmlWriter, DataContract dataContract)
		{
			return false;
		}

		// Token: 0x06000F95 RID: 3989 RVA: 0x0000310F File Offset: 0x0000130F
		internal virtual bool WriteClrTypeInfo(XmlWriterDelegator xmlWriter, Type dataContractType, string clrTypeName, string clrAssemblyName)
		{
			return false;
		}

		// Token: 0x06000F96 RID: 3990 RVA: 0x0000310F File Offset: 0x0000130F
		internal virtual bool WriteClrTypeInfo(XmlWriterDelegator xmlWriter, Type dataContractType, SerializationInfo serInfo)
		{
			return false;
		}

		// Token: 0x06000F97 RID: 3991 RVA: 0x000330AD File Offset: 0x000312AD
		public virtual void WriteAnyType(XmlWriterDelegator xmlWriter, object value)
		{
			xmlWriter.WriteAnyType(value);
		}

		// Token: 0x06000F98 RID: 3992 RVA: 0x0003923F File Offset: 0x0003743F
		public virtual void WriteString(XmlWriterDelegator xmlWriter, string value)
		{
			xmlWriter.WriteString(value);
		}

		// Token: 0x06000F99 RID: 3993 RVA: 0x00039248 File Offset: 0x00037448
		public virtual void WriteString(XmlWriterDelegator xmlWriter, string value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (value == null)
			{
				this.WriteNull(xmlWriter, typeof(string), true, name, ns);
				return;
			}
			xmlWriter.WriteStartElementPrimitive(name, ns);
			xmlWriter.WriteString(value);
			xmlWriter.WriteEndElementPrimitive();
		}

		// Token: 0x06000F9A RID: 3994 RVA: 0x00039279 File Offset: 0x00037479
		public virtual void WriteBase64(XmlWriterDelegator xmlWriter, byte[] value)
		{
			xmlWriter.WriteBase64(value);
		}

		// Token: 0x06000F9B RID: 3995 RVA: 0x00039282 File Offset: 0x00037482
		public virtual void WriteBase64(XmlWriterDelegator xmlWriter, byte[] value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (value == null)
			{
				this.WriteNull(xmlWriter, typeof(byte[]), true, name, ns);
				return;
			}
			xmlWriter.WriteStartElementPrimitive(name, ns);
			xmlWriter.WriteBase64(value);
			xmlWriter.WriteEndElementPrimitive();
		}

		// Token: 0x06000F9C RID: 3996 RVA: 0x000392B3 File Offset: 0x000374B3
		public virtual void WriteUri(XmlWriterDelegator xmlWriter, Uri value)
		{
			xmlWriter.WriteUri(value);
		}

		// Token: 0x06000F9D RID: 3997 RVA: 0x000392BC File Offset: 0x000374BC
		public virtual void WriteUri(XmlWriterDelegator xmlWriter, Uri value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (value == null)
			{
				this.WriteNull(xmlWriter, typeof(Uri), true, name, ns);
				return;
			}
			xmlWriter.WriteStartElementPrimitive(name, ns);
			xmlWriter.WriteUri(value);
			xmlWriter.WriteEndElementPrimitive();
		}

		// Token: 0x06000F9E RID: 3998 RVA: 0x000392F3 File Offset: 0x000374F3
		public virtual void WriteQName(XmlWriterDelegator xmlWriter, XmlQualifiedName value)
		{
			xmlWriter.WriteQName(value);
		}

		// Token: 0x06000F9F RID: 3999 RVA: 0x000392FC File Offset: 0x000374FC
		public virtual void WriteQName(XmlWriterDelegator xmlWriter, XmlQualifiedName value, XmlDictionaryString name, XmlDictionaryString ns)
		{
			if (value == null)
			{
				this.WriteNull(xmlWriter, typeof(XmlQualifiedName), true, name, ns);
				return;
			}
			if (ns != null && ns.Value != null && ns.Value.Length > 0)
			{
				xmlWriter.WriteStartElement("q", name, ns);
			}
			else
			{
				xmlWriter.WriteStartElement(name, ns);
			}
			xmlWriter.WriteQName(value);
			xmlWriter.WriteEndElement();
		}

		// Token: 0x06000FA0 RID: 4000 RVA: 0x0003936A File Offset: 0x0003756A
		internal void HandleGraphAtTopLevel(XmlWriterDelegator writer, object obj, DataContract contract)
		{
			writer.WriteXmlnsAttribute("i", DictionaryGlobals.SchemaInstanceNamespace);
			if (contract.IsISerializable)
			{
				writer.WriteXmlnsAttribute("x", DictionaryGlobals.SchemaNamespace);
			}
			this.OnHandleReference(writer, obj, true);
		}

		// Token: 0x06000FA1 RID: 4001 RVA: 0x000393A0 File Offset: 0x000375A0
		internal virtual bool OnHandleReference(XmlWriterDelegator xmlWriter, object obj, bool canContainCyclicReference)
		{
			if (xmlWriter.depth < 512)
			{
				return false;
			}
			if (canContainCyclicReference)
			{
				if (this.byValObjectsInScope.Count == 0 && DiagnosticUtility.ShouldTraceWarning)
				{
					TraceUtility.Trace(TraceEventType.Warning, 196626, SR.GetString("Object with large depth"));
				}
				if (this.byValObjectsInScope.Contains(obj))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Object graph for type '{0}' contains cycles and cannot be serialized if references are not tracked. Consider using the DataContractAttribute with the IsReference property set to true.", new object[]
					{
						DataContract.GetClrTypeFullName(obj.GetType())
					})));
				}
				this.byValObjectsInScope.Push(obj);
			}
			return false;
		}

		// Token: 0x06000FA2 RID: 4002 RVA: 0x0003942C File Offset: 0x0003762C
		internal virtual void OnEndHandleReference(XmlWriterDelegator xmlWriter, object obj, bool canContainCyclicReference)
		{
			if (xmlWriter.depth < 512)
			{
				return;
			}
			if (canContainCyclicReference)
			{
				this.byValObjectsInScope.Pop(obj);
			}
		}

		// Token: 0x06000FA3 RID: 4003 RVA: 0x0003944B File Offset: 0x0003764B
		public void WriteNull(XmlWriterDelegator xmlWriter, Type memberType, bool isMemberTypeSerializable)
		{
			this.CheckIfTypeSerializable(memberType, isMemberTypeSerializable);
			this.WriteNull(xmlWriter);
		}

		// Token: 0x06000FA4 RID: 4004 RVA: 0x0003945C File Offset: 0x0003765C
		internal void WriteNull(XmlWriterDelegator xmlWriter, Type memberType, bool isMemberTypeSerializable, XmlDictionaryString name, XmlDictionaryString ns)
		{
			xmlWriter.WriteStartElement(name, ns);
			this.WriteNull(xmlWriter, memberType, isMemberTypeSerializable);
			xmlWriter.WriteEndElement();
		}

		// Token: 0x06000FA5 RID: 4005 RVA: 0x00039477 File Offset: 0x00037677
		public void IncrementArrayCount(XmlWriterDelegator xmlWriter, Array array)
		{
			this.IncrementCollectionCount(xmlWriter, array.GetLength(0));
		}

		// Token: 0x06000FA6 RID: 4006 RVA: 0x00039487 File Offset: 0x00037687
		public void IncrementCollectionCount(XmlWriterDelegator xmlWriter, ICollection collection)
		{
			this.IncrementCollectionCount(xmlWriter, collection.Count);
		}

		// Token: 0x06000FA7 RID: 4007 RVA: 0x00039496 File Offset: 0x00037696
		public void IncrementCollectionCountGeneric<T>(XmlWriterDelegator xmlWriter, ICollection<T> collection)
		{
			this.IncrementCollectionCount(xmlWriter, collection.Count);
		}

		// Token: 0x06000FA8 RID: 4008 RVA: 0x000394A5 File Offset: 0x000376A5
		private void IncrementCollectionCount(XmlWriterDelegator xmlWriter, int size)
		{
			base.IncrementItemCount(size);
			this.WriteArraySize(xmlWriter, size);
		}

		// Token: 0x06000FA9 RID: 4009 RVA: 0x000020AE File Offset: 0x000002AE
		internal virtual void WriteArraySize(XmlWriterDelegator xmlWriter, int size)
		{
		}

		// Token: 0x06000FAA RID: 4010 RVA: 0x000394B8 File Offset: 0x000376B8
		public static T GetDefaultValue<T>()
		{
			return default(T);
		}

		// Token: 0x06000FAB RID: 4011 RVA: 0x000394CE File Offset: 0x000376CE
		public static T GetNullableValue<T>(T? value) where T : struct
		{
			return value.Value;
		}

		// Token: 0x06000FAC RID: 4012 RVA: 0x000394D7 File Offset: 0x000376D7
		public static void ThrowRequiredMemberMustBeEmitted(string memberName, Type type)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("Member {0} in type {1} cannot be serialized. This exception is usually caused by trying to use a null value where a null value is not allowed. The '{0}' member is set to its default value (usually null or zero). The member's EmitDefault setting is 'false', indicating that the member should not be serialized. However, the member's IsRequired setting is 'true', indicating that it must be serialized. This conflict cannot be resolved.  Consider setting '{0}' to a non-default value. Alternatively, you can change the EmitDefaultValue property on the DataMemberAttribute attribute to true, or changing the IsRequired property to false.", new object[]
			{
				memberName,
				type.FullName
			})));
		}

		// Token: 0x06000FAD RID: 4013 RVA: 0x00039500 File Offset: 0x00037700
		public static bool GetHasValue<T>(T? value) where T : struct
		{
			return value != null;
		}

		// Token: 0x06000FAE RID: 4014 RVA: 0x00039509 File Offset: 0x00037709
		internal void WriteIXmlSerializable(XmlWriterDelegator xmlWriter, object obj)
		{
			if (this.xmlSerializableWriter == null)
			{
				this.xmlSerializableWriter = new XmlSerializableWriter();
			}
			XmlObjectSerializerWriteContext.WriteIXmlSerializable(xmlWriter, obj, this.xmlSerializableWriter);
		}

		// Token: 0x06000FAF RID: 4015 RVA: 0x0003952B File Offset: 0x0003772B
		internal static void WriteRootIXmlSerializable(XmlWriterDelegator xmlWriter, object obj)
		{
			XmlObjectSerializerWriteContext.WriteIXmlSerializable(xmlWriter, obj, new XmlSerializableWriter());
		}

		// Token: 0x06000FB0 RID: 4016 RVA: 0x0003953C File Offset: 0x0003773C
		private static void WriteIXmlSerializable(XmlWriterDelegator xmlWriter, object obj, XmlSerializableWriter xmlSerializableWriter)
		{
			xmlSerializableWriter.BeginWrite(xmlWriter.Writer, obj);
			IXmlSerializable xmlSerializable = obj as IXmlSerializable;
			if (xmlSerializable != null)
			{
				xmlSerializable.WriteXml(xmlSerializableWriter);
			}
			else
			{
				XmlElement xmlElement = obj as XmlElement;
				if (xmlElement != null)
				{
					xmlElement.WriteTo(xmlSerializableWriter);
				}
				else
				{
					XmlNode[] array = obj as XmlNode[];
					if (array == null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Unknown XML type: '{0}'.", new object[]
						{
							DataContract.GetClrTypeFullName(obj.GetType())
						})));
					}
					XmlNode[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						array2[i].WriteTo(xmlSerializableWriter);
					}
				}
			}
			xmlSerializableWriter.EndWrite();
		}

		// Token: 0x06000FB1 RID: 4017 RVA: 0x000395D5 File Offset: 0x000377D5
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal void GetObjectData(ISerializable obj, SerializationInfo serInfo, StreamingContext context)
		{
			obj.GetObjectData(serInfo, context);
		}

		// Token: 0x06000FB2 RID: 4018 RVA: 0x000395E0 File Offset: 0x000377E0
		public void WriteISerializable(XmlWriterDelegator xmlWriter, ISerializable obj)
		{
			Type type = obj.GetType();
			SerializationInfo serializationInfo = new SerializationInfo(type, XmlObjectSerializer.FormatterConverter, !this.UnsafeTypeForwardingEnabled);
			this.GetObjectData(obj, serializationInfo, base.GetStreamingContext());
			if (!this.UnsafeTypeForwardingEnabled && serializationInfo.AssemblyName == "0")
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("ISerializable AssemblyName is set to \"0\" for type '{0}'.", new object[]
				{
					DataContract.GetClrTypeFullName(obj.GetType())
				})));
			}
			this.WriteSerializationInfo(xmlWriter, type, serializationInfo);
		}

		// Token: 0x06000FB3 RID: 4019 RVA: 0x00039664 File Offset: 0x00037864
		internal void WriteSerializationInfo(XmlWriterDelegator xmlWriter, Type objType, SerializationInfo serInfo)
		{
			if (DataContract.GetClrTypeFullName(objType) != serInfo.FullTypeName)
			{
				if (base.DataContractResolver != null)
				{
					XmlDictionaryString name;
					XmlDictionaryString ns;
					if (this.ResolveType(serInfo.ObjectType, objType, out name, out ns))
					{
						xmlWriter.WriteAttributeQualifiedName("z", DictionaryGlobals.ISerializableFactoryTypeLocalName, DictionaryGlobals.SerializationNamespace, name, ns);
					}
				}
				else
				{
					string key;
					string key2;
					DataContract.GetDefaultStableName(serInfo.FullTypeName, out key, out key2);
					xmlWriter.WriteAttributeQualifiedName("z", DictionaryGlobals.ISerializableFactoryTypeLocalName, DictionaryGlobals.SerializationNamespace, DataContract.GetClrTypeString(key), DataContract.GetClrTypeString(key2));
				}
			}
			this.WriteClrTypeInfo(xmlWriter, objType, serInfo);
			base.IncrementItemCount(serInfo.MemberCount);
			foreach (SerializationEntry serializationEntry in serInfo)
			{
				XmlDictionaryString clrTypeString = DataContract.GetClrTypeString(DataContract.EncodeLocalName(serializationEntry.Name));
				xmlWriter.WriteStartElement(clrTypeString, DictionaryGlobals.EmptyString);
				object value = serializationEntry.Value;
				if (value == null)
				{
					this.WriteNull(xmlWriter);
				}
				else
				{
					this.InternalSerializeReference(xmlWriter, value, false, false, -1, Globals.TypeOfObject.TypeHandle);
				}
				xmlWriter.WriteEndElement();
			}
		}

		// Token: 0x06000FB4 RID: 4020 RVA: 0x0003976C File Offset: 0x0003796C
		public void WriteExtensionData(XmlWriterDelegator xmlWriter, ExtensionDataObject extensionData, int memberIndex)
		{
			if (base.IgnoreExtensionDataObject || extensionData == null)
			{
				return;
			}
			if (extensionData.Members != null)
			{
				for (int i = 0; i < extensionData.Members.Count; i++)
				{
					ExtensionDataMember extensionDataMember = extensionData.Members[i];
					if (extensionDataMember.MemberIndex == memberIndex)
					{
						this.WriteExtensionDataMember(xmlWriter, extensionDataMember);
					}
				}
			}
		}

		// Token: 0x06000FB5 RID: 4021 RVA: 0x000397C4 File Offset: 0x000379C4
		private void WriteExtensionDataMember(XmlWriterDelegator xmlWriter, ExtensionDataMember member)
		{
			xmlWriter.WriteStartElement(member.Name, member.Namespace);
			IDataNode value = member.Value;
			this.WriteExtensionDataValue(xmlWriter, value);
			xmlWriter.WriteEndElement();
		}

		// Token: 0x06000FB6 RID: 4022 RVA: 0x000397F8 File Offset: 0x000379F8
		internal virtual void WriteExtensionDataTypeInfo(XmlWriterDelegator xmlWriter, IDataNode dataNode)
		{
			if (dataNode.DataContractName != null)
			{
				this.WriteTypeInfo(xmlWriter, dataNode.DataContractName, dataNode.DataContractNamespace);
			}
			this.WriteClrTypeInfo(xmlWriter, dataNode.DataType, dataNode.ClrTypeName, dataNode.ClrAssemblyName);
		}

		// Token: 0x06000FB7 RID: 4023 RVA: 0x00039830 File Offset: 0x00037A30
		internal void WriteExtensionDataValue(XmlWriterDelegator xmlWriter, IDataNode dataNode)
		{
			base.IncrementItemCount(1);
			if (dataNode == null)
			{
				this.WriteNull(xmlWriter);
				return;
			}
			if (dataNode.PreservesReferences && this.OnHandleReference(xmlWriter, (dataNode.Value == null) ? dataNode : dataNode.Value, true))
			{
				return;
			}
			Type dataType = dataNode.DataType;
			if (dataType == Globals.TypeOfClassDataNode)
			{
				this.WriteExtensionClassData(xmlWriter, (ClassDataNode)dataNode);
			}
			else if (dataType == Globals.TypeOfCollectionDataNode)
			{
				this.WriteExtensionCollectionData(xmlWriter, (CollectionDataNode)dataNode);
			}
			else if (dataType == Globals.TypeOfXmlDataNode)
			{
				this.WriteExtensionXmlData(xmlWriter, (XmlDataNode)dataNode);
			}
			else if (dataType == Globals.TypeOfISerializableDataNode)
			{
				this.WriteExtensionISerializableData(xmlWriter, (ISerializableDataNode)dataNode);
			}
			else
			{
				this.WriteExtensionDataTypeInfo(xmlWriter, dataNode);
				if (dataType == Globals.TypeOfObject)
				{
					object value = dataNode.Value;
					if (value != null)
					{
						this.InternalSerialize(xmlWriter, value, false, false, -1, value.GetType().TypeHandle);
					}
				}
				else
				{
					xmlWriter.WriteExtensionData(dataNode);
				}
			}
			if (dataNode.PreservesReferences)
			{
				this.OnEndHandleReference(xmlWriter, (dataNode.Value == null) ? dataNode : dataNode.Value, true);
			}
		}

		// Token: 0x06000FB8 RID: 4024 RVA: 0x0003994C File Offset: 0x00037B4C
		internal bool TryWriteDeserializedExtensionData(XmlWriterDelegator xmlWriter, IDataNode dataNode)
		{
			object value = dataNode.Value;
			if (value == null)
			{
				return false;
			}
			Type type = (dataNode.DataContractName == null) ? value.GetType() : Globals.TypeOfObject;
			this.InternalSerialize(xmlWriter, value, false, false, -1, type.TypeHandle);
			return true;
		}

		// Token: 0x06000FB9 RID: 4025 RVA: 0x00039990 File Offset: 0x00037B90
		private void WriteExtensionClassData(XmlWriterDelegator xmlWriter, ClassDataNode dataNode)
		{
			if (!this.TryWriteDeserializedExtensionData(xmlWriter, dataNode))
			{
				this.WriteExtensionDataTypeInfo(xmlWriter, dataNode);
				IList<ExtensionDataMember> members = dataNode.Members;
				if (members != null)
				{
					for (int i = 0; i < members.Count; i++)
					{
						this.WriteExtensionDataMember(xmlWriter, members[i]);
					}
				}
			}
		}

		// Token: 0x06000FBA RID: 4026 RVA: 0x000399D8 File Offset: 0x00037BD8
		private void WriteExtensionCollectionData(XmlWriterDelegator xmlWriter, CollectionDataNode dataNode)
		{
			if (!this.TryWriteDeserializedExtensionData(xmlWriter, dataNode))
			{
				this.WriteExtensionDataTypeInfo(xmlWriter, dataNode);
				this.WriteArraySize(xmlWriter, dataNode.Size);
				IList<IDataNode> items = dataNode.Items;
				if (items != null)
				{
					for (int i = 0; i < items.Count; i++)
					{
						xmlWriter.WriteStartElement(dataNode.ItemName, dataNode.ItemNamespace);
						this.WriteExtensionDataValue(xmlWriter, items[i]);
						xmlWriter.WriteEndElement();
					}
				}
			}
		}

		// Token: 0x06000FBB RID: 4027 RVA: 0x00039A48 File Offset: 0x00037C48
		private void WriteExtensionISerializableData(XmlWriterDelegator xmlWriter, ISerializableDataNode dataNode)
		{
			if (!this.TryWriteDeserializedExtensionData(xmlWriter, dataNode))
			{
				this.WriteExtensionDataTypeInfo(xmlWriter, dataNode);
				if (dataNode.FactoryTypeName != null)
				{
					xmlWriter.WriteAttributeQualifiedName("z", DictionaryGlobals.ISerializableFactoryTypeLocalName, DictionaryGlobals.SerializationNamespace, dataNode.FactoryTypeName, dataNode.FactoryTypeNamespace);
				}
				IList<ISerializableDataMember> members = dataNode.Members;
				if (members != null)
				{
					for (int i = 0; i < members.Count; i++)
					{
						ISerializableDataMember serializableDataMember = members[i];
						xmlWriter.WriteStartElement(serializableDataMember.Name, string.Empty);
						this.WriteExtensionDataValue(xmlWriter, serializableDataMember.Value);
						xmlWriter.WriteEndElement();
					}
				}
			}
		}

		// Token: 0x06000FBC RID: 4028 RVA: 0x00039AD8 File Offset: 0x00037CD8
		private void WriteExtensionXmlData(XmlWriterDelegator xmlWriter, XmlDataNode dataNode)
		{
			if (!this.TryWriteDeserializedExtensionData(xmlWriter, dataNode))
			{
				IList<XmlAttribute> xmlAttributes = dataNode.XmlAttributes;
				if (xmlAttributes != null)
				{
					foreach (XmlAttribute xmlAttribute in xmlAttributes)
					{
						xmlAttribute.WriteTo(xmlWriter.Writer);
					}
				}
				this.WriteExtensionDataTypeInfo(xmlWriter, dataNode);
				IList<XmlNode> xmlChildNodes = dataNode.XmlChildNodes;
				if (xmlChildNodes != null)
				{
					foreach (XmlNode xmlNode in xmlChildNodes)
					{
						xmlNode.WriteTo(xmlWriter.Writer);
					}
				}
			}
		}

		// Token: 0x06000FBD RID: 4029 RVA: 0x00039B84 File Offset: 0x00037D84
		protected virtual void WriteDataContractValue(DataContract dataContract, XmlWriterDelegator xmlWriter, object obj, RuntimeTypeHandle declaredTypeHandle)
		{
			dataContract.WriteXmlValue(xmlWriter, obj, this);
		}

		// Token: 0x06000FBE RID: 4030 RVA: 0x00039B8F File Offset: 0x00037D8F
		protected virtual void WriteNull(XmlWriterDelegator xmlWriter)
		{
			XmlObjectSerializer.WriteNull(xmlWriter);
		}

		// Token: 0x06000FBF RID: 4031 RVA: 0x00039B98 File Offset: 0x00037D98
		private void WriteResolvedTypeInfo(XmlWriterDelegator writer, Type objectType, Type declaredType)
		{
			XmlDictionaryString dataContractName;
			XmlDictionaryString dataContractNamespace;
			if (this.ResolveType(objectType, declaredType, out dataContractName, out dataContractNamespace))
			{
				this.WriteTypeInfo(writer, dataContractName, dataContractNamespace);
			}
		}

		// Token: 0x06000FC0 RID: 4032 RVA: 0x00039BBC File Offset: 0x00037DBC
		private bool ResolveType(Type objectType, Type declaredType, out XmlDictionaryString typeName, out XmlDictionaryString typeNamespace)
		{
			if (!base.DataContractResolver.TryResolveType(objectType, declaredType, base.KnownTypeResolver, out typeName, out typeNamespace))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("An object of type '{0}' which derives from DataContractResolver returned false from its TryResolveType method when attempting to resolve the name for an object of type '{1}', indicating that the resolution failed. Change the TryResolveType implementation to return true.", new object[]
				{
					DataContract.GetClrTypeFullName(base.DataContractResolver.GetType()),
					DataContract.GetClrTypeFullName(objectType)
				})));
			}
			if (typeName == null)
			{
				if (typeNamespace == null)
				{
					return false;
				}
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("An object of type '{0}' which derives from DataContractResolver returned a null typeName or typeNamespace but not both from its TryResolveType method when attempting to resolve the name for an object of type '{1}'. Change the TryResolveType implementation to return non-null values, or to return null values for both typeName and typeNamespace in order to serialize as the declared type.", new object[]
				{
					DataContract.GetClrTypeFullName(base.DataContractResolver.GetType()),
					DataContract.GetClrTypeFullName(objectType)
				})));
			}
			else
			{
				if (typeNamespace == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("An object of type '{0}' which derives from DataContractResolver returned a null typeName or typeNamespace but not both from its TryResolveType method when attempting to resolve the name for an object of type '{1}'. Change the TryResolveType implementation to return non-null values, or to return null values for both typeName and typeNamespace in order to serialize as the declared type.", new object[]
					{
						DataContract.GetClrTypeFullName(base.DataContractResolver.GetType()),
						DataContract.GetClrTypeFullName(objectType)
					})));
				}
				return true;
			}
		}

		// Token: 0x06000FC1 RID: 4033 RVA: 0x00039C97 File Offset: 0x00037E97
		protected virtual bool WriteTypeInfo(XmlWriterDelegator writer, DataContract contract, DataContract declaredContract)
		{
			if (XmlObjectSerializer.IsContractDeclared(contract, declaredContract))
			{
				return false;
			}
			if (base.DataContractResolver == null)
			{
				this.WriteTypeInfo(writer, contract.Name, contract.Namespace);
				return true;
			}
			this.WriteResolvedTypeInfo(writer, contract.OriginalUnderlyingType, declaredContract.OriginalUnderlyingType);
			return false;
		}

		// Token: 0x06000FC2 RID: 4034 RVA: 0x00039CD5 File Offset: 0x00037ED5
		protected virtual void WriteTypeInfo(XmlWriterDelegator writer, string dataContractName, string dataContractNamespace)
		{
			writer.WriteAttributeQualifiedName("i", DictionaryGlobals.XsiTypeLocalName, DictionaryGlobals.SchemaInstanceNamespace, dataContractName, dataContractNamespace);
		}

		// Token: 0x06000FC3 RID: 4035 RVA: 0x00039CEE File Offset: 0x00037EEE
		protected virtual void WriteTypeInfo(XmlWriterDelegator writer, XmlDictionaryString dataContractName, XmlDictionaryString dataContractNamespace)
		{
			writer.WriteAttributeQualifiedName("i", DictionaryGlobals.XsiTypeLocalName, DictionaryGlobals.SchemaInstanceNamespace, dataContractName, dataContractNamespace);
		}

		// Token: 0x040006E0 RID: 1760
		private ObjectReferenceStack byValObjectsInScope;

		// Token: 0x040006E1 RID: 1761
		private XmlSerializableWriter xmlSerializableWriter;

		// Token: 0x040006E2 RID: 1762
		private const int depthToCheckCyclicReference = 512;

		// Token: 0x040006E3 RID: 1763
		protected bool preserveObjectReferences;

		// Token: 0x040006E4 RID: 1764
		private ObjectToIdCache serializedObjects;

		// Token: 0x040006E5 RID: 1765
		private bool isGetOnlyCollection;

		// Token: 0x040006E6 RID: 1766
		private readonly bool unsafeTypeForwardingEnabled;

		// Token: 0x040006E7 RID: 1767
		protected bool serializeReadOnlyTypes;
	}
}
