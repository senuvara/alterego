﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000113 RID: 275
	internal class ENTITYDataContract : StringDataContract
	{
		// Token: 0x06000DC3 RID: 3523 RVA: 0x000338A8 File Offset: 0x00031AA8
		internal ENTITYDataContract() : base(DictionaryGlobals.ENTITYLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}
	}
}
