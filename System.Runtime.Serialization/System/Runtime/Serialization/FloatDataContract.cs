﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000FE RID: 254
	internal class FloatDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D99 RID: 3481 RVA: 0x000335B6 File Offset: 0x000317B6
		internal FloatDataContract() : base(typeof(float), DictionaryGlobals.FloatLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x06000D9A RID: 3482 RVA: 0x000335D2 File Offset: 0x000317D2
		internal override string WriteMethodName
		{
			get
			{
				return "WriteFloat";
			}
		}

		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x06000D9B RID: 3483 RVA: 0x000335D9 File Offset: 0x000317D9
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsFloat";
			}
		}

		// Token: 0x06000D9C RID: 3484 RVA: 0x000335E0 File Offset: 0x000317E0
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteFloat((float)obj);
		}

		// Token: 0x06000D9D RID: 3485 RVA: 0x000335EE File Offset: 0x000317EE
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsFloat(), context);
			}
			return reader.ReadElementContentAsFloat();
		}
	}
}
