﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x02000118 RID: 280
	internal class ObjectDataContract : PrimitiveDataContract
	{
		// Token: 0x06000DCC RID: 3532 RVA: 0x0003394C File Offset: 0x00031B4C
		internal ObjectDataContract() : base(typeof(object), DictionaryGlobals.ObjectLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002EE RID: 750
		// (get) Token: 0x06000DCD RID: 3533 RVA: 0x00033968 File Offset: 0x00031B68
		internal override string WriteMethodName
		{
			get
			{
				return "WriteAnyType";
			}
		}

		// Token: 0x170002EF RID: 751
		// (get) Token: 0x06000DCE RID: 3534 RVA: 0x0003396F File Offset: 0x00031B6F
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsAnyType";
			}
		}

		// Token: 0x06000DCF RID: 3535 RVA: 0x000020AE File Offset: 0x000002AE
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
		}

		// Token: 0x06000DD0 RID: 3536 RVA: 0x00033978 File Offset: 0x00031B78
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			object obj;
			if (reader.IsEmptyElement)
			{
				reader.Skip();
				obj = new object();
			}
			else
			{
				string localName = reader.LocalName;
				string namespaceURI = reader.NamespaceURI;
				reader.Read();
				try
				{
					reader.ReadEndElement();
					obj = new object();
				}
				catch (XmlException innerException)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Element {0} from namespace {1} cannot have child contents to be deserialized as an object. Please use XElement to deserialize this pattern of XML.", new object[]
					{
						localName,
						namespaceURI
					}), innerException));
				}
			}
			if (context != null)
			{
				return base.HandleReadValue(obj, context);
			}
			return obj;
		}

		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x06000DD1 RID: 3537 RVA: 0x000066D0 File Offset: 0x000048D0
		internal override bool CanContainReferences
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06000DD2 RID: 3538 RVA: 0x0000310F File Offset: 0x0000130F
		internal override bool IsPrimitive
		{
			get
			{
				return false;
			}
		}
	}
}
