﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000F1 RID: 241
	internal class SignedByteDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D6B RID: 3435 RVA: 0x0003327A File Offset: 0x0003147A
		internal SignedByteDataContract() : base(typeof(sbyte), DictionaryGlobals.SignedByteLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x06000D6C RID: 3436 RVA: 0x00033296 File Offset: 0x00031496
		internal override string WriteMethodName
		{
			get
			{
				return "WriteSignedByte";
			}
		}

		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x06000D6D RID: 3437 RVA: 0x0003329D File Offset: 0x0003149D
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsSignedByte";
			}
		}

		// Token: 0x06000D6E RID: 3438 RVA: 0x000332A4 File Offset: 0x000314A4
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteSignedByte((sbyte)obj);
		}

		// Token: 0x06000D6F RID: 3439 RVA: 0x000332B2 File Offset: 0x000314B2
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsSignedByte(), context);
			}
			return reader.ReadElementContentAsSignedByte();
		}
	}
}
