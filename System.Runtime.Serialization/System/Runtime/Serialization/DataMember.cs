﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security;

namespace System.Runtime.Serialization
{
	// Token: 0x020000C5 RID: 197
	internal class DataMember
	{
		// Token: 0x06000B47 RID: 2887 RVA: 0x0002DAB8 File Offset: 0x0002BCB8
		[SecuritySafeCritical]
		internal DataMember()
		{
			this.helper = new DataMember.CriticalHelper();
		}

		// Token: 0x06000B48 RID: 2888 RVA: 0x0002DACB File Offset: 0x0002BCCB
		[SecuritySafeCritical]
		internal DataMember(MemberInfo memberInfo)
		{
			this.helper = new DataMember.CriticalHelper(memberInfo);
		}

		// Token: 0x06000B49 RID: 2889 RVA: 0x0002DADF File Offset: 0x0002BCDF
		[SecuritySafeCritical]
		internal DataMember(string name)
		{
			this.helper = new DataMember.CriticalHelper(name);
		}

		// Token: 0x06000B4A RID: 2890 RVA: 0x0002DAF3 File Offset: 0x0002BCF3
		[SecuritySafeCritical]
		internal DataMember(DataContract memberTypeContract, string name, bool isNullable, bool isRequired, bool emitDefaultValue, int order)
		{
			this.helper = new DataMember.CriticalHelper(memberTypeContract, name, isNullable, isRequired, emitDefaultValue, order);
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06000B4B RID: 2891 RVA: 0x0002DB0F File Offset: 0x0002BD0F
		internal MemberInfo MemberInfo
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.MemberInfo;
			}
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x06000B4C RID: 2892 RVA: 0x0002DB1C File Offset: 0x0002BD1C
		// (set) Token: 0x06000B4D RID: 2893 RVA: 0x0002DB29 File Offset: 0x0002BD29
		internal string Name
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.Name;
			}
			[SecurityCritical]
			set
			{
				this.helper.Name = value;
			}
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x06000B4E RID: 2894 RVA: 0x0002DB37 File Offset: 0x0002BD37
		// (set) Token: 0x06000B4F RID: 2895 RVA: 0x0002DB44 File Offset: 0x0002BD44
		internal int Order
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.Order;
			}
			[SecurityCritical]
			set
			{
				this.helper.Order = value;
			}
		}

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x06000B50 RID: 2896 RVA: 0x0002DB52 File Offset: 0x0002BD52
		// (set) Token: 0x06000B51 RID: 2897 RVA: 0x0002DB5F File Offset: 0x0002BD5F
		internal bool IsRequired
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsRequired;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsRequired = value;
			}
		}

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x06000B52 RID: 2898 RVA: 0x0002DB6D File Offset: 0x0002BD6D
		// (set) Token: 0x06000B53 RID: 2899 RVA: 0x0002DB7A File Offset: 0x0002BD7A
		internal bool EmitDefaultValue
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.EmitDefaultValue;
			}
			[SecurityCritical]
			set
			{
				this.helper.EmitDefaultValue = value;
			}
		}

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x06000B54 RID: 2900 RVA: 0x0002DB88 File Offset: 0x0002BD88
		// (set) Token: 0x06000B55 RID: 2901 RVA: 0x0002DB95 File Offset: 0x0002BD95
		internal bool IsNullable
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsNullable;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsNullable = value;
			}
		}

		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x06000B56 RID: 2902 RVA: 0x0002DBA3 File Offset: 0x0002BDA3
		// (set) Token: 0x06000B57 RID: 2903 RVA: 0x0002DBB0 File Offset: 0x0002BDB0
		internal bool IsGetOnlyCollection
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsGetOnlyCollection;
			}
			[SecurityCritical]
			set
			{
				this.helper.IsGetOnlyCollection = value;
			}
		}

		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x06000B58 RID: 2904 RVA: 0x0002DBBE File Offset: 0x0002BDBE
		internal Type MemberType
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.MemberType;
			}
		}

		// Token: 0x170001DA RID: 474
		// (get) Token: 0x06000B59 RID: 2905 RVA: 0x0002DBCB File Offset: 0x0002BDCB
		// (set) Token: 0x06000B5A RID: 2906 RVA: 0x0002DBD8 File Offset: 0x0002BDD8
		internal DataContract MemberTypeContract
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.MemberTypeContract;
			}
			[SecurityCritical]
			set
			{
				this.helper.MemberTypeContract = value;
			}
		}

		// Token: 0x170001DB RID: 475
		// (get) Token: 0x06000B5B RID: 2907 RVA: 0x0002DBE6 File Offset: 0x0002BDE6
		// (set) Token: 0x06000B5C RID: 2908 RVA: 0x0002DBF3 File Offset: 0x0002BDF3
		internal bool HasConflictingNameAndType
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.HasConflictingNameAndType;
			}
			[SecurityCritical]
			set
			{
				this.helper.HasConflictingNameAndType = value;
			}
		}

		// Token: 0x170001DC RID: 476
		// (get) Token: 0x06000B5D RID: 2909 RVA: 0x0002DC01 File Offset: 0x0002BE01
		// (set) Token: 0x06000B5E RID: 2910 RVA: 0x0002DC0E File Offset: 0x0002BE0E
		internal DataMember ConflictingMember
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.ConflictingMember;
			}
			[SecurityCritical]
			set
			{
				this.helper.ConflictingMember = value;
			}
		}

		// Token: 0x06000B5F RID: 2911 RVA: 0x0002DC1C File Offset: 0x0002BE1C
		internal DataMember BindGenericParameters(DataContract[] paramContracts, Dictionary<DataContract, DataContract> boundContracts)
		{
			DataContract dataContract = this.MemberTypeContract.BindGenericParameters(paramContracts, boundContracts);
			return new DataMember(dataContract, this.Name, !dataContract.IsValueType, this.IsRequired, this.EmitDefaultValue, this.Order);
		}

		// Token: 0x06000B60 RID: 2912 RVA: 0x0002DC60 File Offset: 0x0002BE60
		internal bool Equals(object other, Dictionary<DataContractPairKey, object> checkedContracts)
		{
			if (this == other)
			{
				return true;
			}
			DataMember dataMember = other as DataMember;
			if (dataMember != null)
			{
				bool flag = this.MemberTypeContract != null && !this.MemberTypeContract.IsValueType;
				bool flag2 = dataMember.MemberTypeContract != null && !dataMember.MemberTypeContract.IsValueType;
				return this.Name == dataMember.Name && (this.IsNullable || flag) == (dataMember.IsNullable || flag2) && this.IsRequired == dataMember.IsRequired && this.EmitDefaultValue == dataMember.EmitDefaultValue && this.MemberTypeContract.Equals(dataMember.MemberTypeContract, checkedContracts);
			}
			return false;
		}

		// Token: 0x06000B61 RID: 2913 RVA: 0x0002AC0A File Offset: 0x00028E0A
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x040004A1 RID: 1185
		[SecurityCritical]
		private DataMember.CriticalHelper helper;

		// Token: 0x020000C6 RID: 198
		private class CriticalHelper
		{
			// Token: 0x06000B62 RID: 2914 RVA: 0x0002DD0B File Offset: 0x0002BF0B
			internal CriticalHelper()
			{
				this.emitDefaultValue = true;
			}

			// Token: 0x06000B63 RID: 2915 RVA: 0x0002DD1A File Offset: 0x0002BF1A
			internal CriticalHelper(MemberInfo memberInfo)
			{
				this.emitDefaultValue = true;
				this.memberInfo = memberInfo;
			}

			// Token: 0x06000B64 RID: 2916 RVA: 0x0002DD30 File Offset: 0x0002BF30
			internal CriticalHelper(string name)
			{
				this.Name = name;
			}

			// Token: 0x06000B65 RID: 2917 RVA: 0x0002DD3F File Offset: 0x0002BF3F
			internal CriticalHelper(DataContract memberTypeContract, string name, bool isNullable, bool isRequired, bool emitDefaultValue, int order)
			{
				this.MemberTypeContract = memberTypeContract;
				this.Name = name;
				this.IsNullable = isNullable;
				this.IsRequired = isRequired;
				this.EmitDefaultValue = emitDefaultValue;
				this.Order = order;
			}

			// Token: 0x170001DD RID: 477
			// (get) Token: 0x06000B66 RID: 2918 RVA: 0x0002DD74 File Offset: 0x0002BF74
			internal MemberInfo MemberInfo
			{
				get
				{
					return this.memberInfo;
				}
			}

			// Token: 0x170001DE RID: 478
			// (get) Token: 0x06000B67 RID: 2919 RVA: 0x0002DD7C File Offset: 0x0002BF7C
			// (set) Token: 0x06000B68 RID: 2920 RVA: 0x0002DD84 File Offset: 0x0002BF84
			internal string Name
			{
				get
				{
					return this.name;
				}
				set
				{
					this.name = value;
				}
			}

			// Token: 0x170001DF RID: 479
			// (get) Token: 0x06000B69 RID: 2921 RVA: 0x0002DD8D File Offset: 0x0002BF8D
			// (set) Token: 0x06000B6A RID: 2922 RVA: 0x0002DD95 File Offset: 0x0002BF95
			internal int Order
			{
				get
				{
					return this.order;
				}
				set
				{
					this.order = value;
				}
			}

			// Token: 0x170001E0 RID: 480
			// (get) Token: 0x06000B6B RID: 2923 RVA: 0x0002DD9E File Offset: 0x0002BF9E
			// (set) Token: 0x06000B6C RID: 2924 RVA: 0x0002DDA6 File Offset: 0x0002BFA6
			internal bool IsRequired
			{
				get
				{
					return this.isRequired;
				}
				set
				{
					this.isRequired = value;
				}
			}

			// Token: 0x170001E1 RID: 481
			// (get) Token: 0x06000B6D RID: 2925 RVA: 0x0002DDAF File Offset: 0x0002BFAF
			// (set) Token: 0x06000B6E RID: 2926 RVA: 0x0002DDB7 File Offset: 0x0002BFB7
			internal bool EmitDefaultValue
			{
				get
				{
					return this.emitDefaultValue;
				}
				set
				{
					this.emitDefaultValue = value;
				}
			}

			// Token: 0x170001E2 RID: 482
			// (get) Token: 0x06000B6F RID: 2927 RVA: 0x0002DDC0 File Offset: 0x0002BFC0
			// (set) Token: 0x06000B70 RID: 2928 RVA: 0x0002DDC8 File Offset: 0x0002BFC8
			internal bool IsNullable
			{
				get
				{
					return this.isNullable;
				}
				set
				{
					this.isNullable = value;
				}
			}

			// Token: 0x170001E3 RID: 483
			// (get) Token: 0x06000B71 RID: 2929 RVA: 0x0002DDD1 File Offset: 0x0002BFD1
			// (set) Token: 0x06000B72 RID: 2930 RVA: 0x0002DDD9 File Offset: 0x0002BFD9
			internal bool IsGetOnlyCollection
			{
				get
				{
					return this.isGetOnlyCollection;
				}
				set
				{
					this.isGetOnlyCollection = value;
				}
			}

			// Token: 0x170001E4 RID: 484
			// (get) Token: 0x06000B73 RID: 2931 RVA: 0x0002DDE4 File Offset: 0x0002BFE4
			internal Type MemberType
			{
				get
				{
					FieldInfo fieldInfo = this.MemberInfo as FieldInfo;
					if (fieldInfo != null)
					{
						return fieldInfo.FieldType;
					}
					return ((PropertyInfo)this.MemberInfo).PropertyType;
				}
			}

			// Token: 0x170001E5 RID: 485
			// (get) Token: 0x06000B74 RID: 2932 RVA: 0x0002DE20 File Offset: 0x0002C020
			// (set) Token: 0x06000B75 RID: 2933 RVA: 0x0002DE91 File Offset: 0x0002C091
			internal DataContract MemberTypeContract
			{
				get
				{
					if (this.memberTypeContract == null && this.MemberInfo != null)
					{
						if (this.IsGetOnlyCollection)
						{
							this.memberTypeContract = DataContract.GetGetOnlyCollectionDataContract(DataContract.GetId(this.MemberType.TypeHandle), this.MemberType.TypeHandle, this.MemberType, SerializationMode.SharedContract);
						}
						else
						{
							this.memberTypeContract = DataContract.GetDataContract(this.MemberType);
						}
					}
					return this.memberTypeContract;
				}
				set
				{
					this.memberTypeContract = value;
				}
			}

			// Token: 0x170001E6 RID: 486
			// (get) Token: 0x06000B76 RID: 2934 RVA: 0x0002DE9A File Offset: 0x0002C09A
			// (set) Token: 0x06000B77 RID: 2935 RVA: 0x0002DEA2 File Offset: 0x0002C0A2
			internal bool HasConflictingNameAndType
			{
				get
				{
					return this.hasConflictingNameAndType;
				}
				set
				{
					this.hasConflictingNameAndType = value;
				}
			}

			// Token: 0x170001E7 RID: 487
			// (get) Token: 0x06000B78 RID: 2936 RVA: 0x0002DEAB File Offset: 0x0002C0AB
			// (set) Token: 0x06000B79 RID: 2937 RVA: 0x0002DEB3 File Offset: 0x0002C0B3
			internal DataMember ConflictingMember
			{
				get
				{
					return this.conflictingMember;
				}
				set
				{
					this.conflictingMember = value;
				}
			}

			// Token: 0x040004A2 RID: 1186
			private DataContract memberTypeContract;

			// Token: 0x040004A3 RID: 1187
			private string name;

			// Token: 0x040004A4 RID: 1188
			private int order;

			// Token: 0x040004A5 RID: 1189
			private bool isRequired;

			// Token: 0x040004A6 RID: 1190
			private bool emitDefaultValue;

			// Token: 0x040004A7 RID: 1191
			private bool isNullable;

			// Token: 0x040004A8 RID: 1192
			private bool isGetOnlyCollection;

			// Token: 0x040004A9 RID: 1193
			private MemberInfo memberInfo;

			// Token: 0x040004AA RID: 1194
			private bool hasConflictingNameAndType;

			// Token: 0x040004AB RID: 1195
			private DataMember conflictingMember;
		}
	}
}
