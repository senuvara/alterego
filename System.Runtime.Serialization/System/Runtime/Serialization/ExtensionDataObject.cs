﻿using System;
using System.Collections.Generic;

namespace System.Runtime.Serialization
{
	/// <summary>Stores data from a versioned data contract that has been extended by adding new members.</summary>
	// Token: 0x020000D0 RID: 208
	public sealed class ExtensionDataObject
	{
		// Token: 0x06000BC6 RID: 3014 RVA: 0x00002217 File Offset: 0x00000417
		internal ExtensionDataObject()
		{
		}

		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000BC7 RID: 3015 RVA: 0x0002EFCA File Offset: 0x0002D1CA
		// (set) Token: 0x06000BC8 RID: 3016 RVA: 0x0002EFD2 File Offset: 0x0002D1D2
		internal IList<ExtensionDataMember> Members
		{
			get
			{
				return this.members;
			}
			set
			{
				this.members = value;
			}
		}

		// Token: 0x04000505 RID: 1285
		private IList<ExtensionDataMember> members;
	}
}
