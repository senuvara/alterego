﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200018B RID: 395
	internal class XmlObjectSerializerReadContextComplexJson : XmlObjectSerializerReadContextComplex
	{
		// Token: 0x06001363 RID: 4963 RVA: 0x00047260 File Offset: 0x00045460
		public XmlObjectSerializerReadContextComplexJson(DataContractJsonSerializer serializer, DataContract rootTypeDataContract) : base(serializer, serializer.MaxItemsInObjectGraph, new StreamingContext(StreamingContextStates.All), serializer.IgnoreExtensionDataObject)
		{
			this.rootTypeDataContract = rootTypeDataContract;
			this.serializerKnownTypeList = serializer.knownTypeList;
			this.dataContractSurrogate = serializer.DataContractSurrogate;
			this.dateTimeFormat = serializer.DateTimeFormat;
			this.useSimpleDictionaryFormat = serializer.UseSimpleDictionaryFormat;
		}

		// Token: 0x1700040F RID: 1039
		// (get) Token: 0x06001364 RID: 4964 RVA: 0x000472C1 File Offset: 0x000454C1
		internal IList<Type> SerializerKnownTypeList
		{
			get
			{
				return this.serializerKnownTypeList;
			}
		}

		// Token: 0x17000410 RID: 1040
		// (get) Token: 0x06001365 RID: 4965 RVA: 0x000472C9 File Offset: 0x000454C9
		public bool UseSimpleDictionaryFormat
		{
			get
			{
				return this.useSimpleDictionaryFormat;
			}
		}

		// Token: 0x06001366 RID: 4966 RVA: 0x000472D1 File Offset: 0x000454D1
		protected override void StartReadExtensionDataValue(XmlReaderDelegator xmlReader)
		{
			this.extensionDataValueType = xmlReader.GetAttribute("type");
		}

		// Token: 0x06001367 RID: 4967 RVA: 0x000472E4 File Offset: 0x000454E4
		protected override IDataNode ReadPrimitiveExtensionDataValue(XmlReaderDelegator xmlReader, string dataContractName, string dataContractNamespace)
		{
			string text = this.extensionDataValueType;
			IDataNode result;
			if (text != null && !(text == "string"))
			{
				if (!(text == "boolean"))
				{
					if (!(text == "number"))
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Unexpected attribute value '{0}'.", new object[]
						{
							this.extensionDataValueType
						})));
					}
					result = this.ReadNumericalPrimitiveExtensionDataValue(xmlReader);
				}
				else
				{
					result = new DataNode<bool>(xmlReader.ReadContentAsBoolean());
				}
			}
			else
			{
				result = new DataNode<string>(xmlReader.ReadContentAsString());
			}
			xmlReader.ReadEndElement();
			return result;
		}

		// Token: 0x06001368 RID: 4968 RVA: 0x00047378 File Offset: 0x00045578
		private IDataNode ReadNumericalPrimitiveExtensionDataValue(XmlReaderDelegator xmlReader)
		{
			TypeCode typeCode;
			object obj = JsonObjectDataContract.ParseJsonNumber(xmlReader.ReadContentAsString(), out typeCode);
			switch (typeCode)
			{
			case TypeCode.SByte:
				return new DataNode<sbyte>((sbyte)obj);
			case TypeCode.Byte:
				return new DataNode<byte>((byte)obj);
			case TypeCode.Int16:
				return new DataNode<short>((short)obj);
			case TypeCode.UInt16:
				return new DataNode<ushort>((ushort)obj);
			case TypeCode.Int32:
				return new DataNode<int>((int)obj);
			case TypeCode.UInt32:
				return new DataNode<uint>((uint)obj);
			case TypeCode.Int64:
				return new DataNode<long>((long)obj);
			case TypeCode.UInt64:
				return new DataNode<ulong>((ulong)obj);
			case TypeCode.Single:
				return new DataNode<float>((float)obj);
			case TypeCode.Double:
				return new DataNode<double>((double)obj);
			case TypeCode.Decimal:
				return new DataNode<decimal>((decimal)obj);
			default:
				throw Fx.AssertAndThrow("JsonObjectDataContract.ParseJsonNumber shouldn't return a TypeCode that we're not expecting");
			}
		}

		// Token: 0x06001369 RID: 4969 RVA: 0x0004745A File Offset: 0x0004565A
		internal static XmlObjectSerializerReadContextComplexJson CreateContext(DataContractJsonSerializer serializer, DataContract rootTypeDataContract)
		{
			return new XmlObjectSerializerReadContextComplexJson(serializer, rootTypeDataContract);
		}

		// Token: 0x0600136A RID: 4970 RVA: 0x000372E7 File Offset: 0x000354E7
		internal override int GetArraySize()
		{
			return -1;
		}

		// Token: 0x0600136B RID: 4971 RVA: 0x00047463 File Offset: 0x00045663
		protected override object ReadDataContractValue(DataContract dataContract, XmlReaderDelegator reader)
		{
			return DataContractJsonSerializer.ReadJsonValue(dataContract, reader, this);
		}

		// Token: 0x0600136C RID: 4972 RVA: 0x00047470 File Offset: 0x00045670
		internal override void ReadAttributes(XmlReaderDelegator xmlReader)
		{
			if (this.attributes == null)
			{
				this.attributes = new Attributes();
			}
			this.attributes.Reset();
			if (xmlReader.MoveToAttribute("type") && xmlReader.Value == "null")
			{
				this.attributes.XsiNil = true;
			}
			else if (xmlReader.MoveToAttribute("__type"))
			{
				XmlQualifiedName xmlQualifiedName = JsonReaderDelegator.ParseQualifiedName(xmlReader.Value);
				this.attributes.XsiTypeName = xmlQualifiedName.Name;
				string text = xmlQualifiedName.Namespace;
				if (!string.IsNullOrEmpty(text))
				{
					char c = text[0];
					if (c != '#')
					{
						if (c == '\\')
						{
							if (text.Length >= 2)
							{
								c = text[1];
								if (c == '#' || c == '\\')
								{
									text = text.Substring(1);
								}
							}
						}
					}
					else
					{
						text = "http://schemas.datacontract.org/2004/07/" + text.Substring(1);
					}
				}
				this.attributes.XsiTypeNamespace = text;
			}
			xmlReader.MoveToElement();
		}

		// Token: 0x0600136D RID: 4973 RVA: 0x00047568 File Offset: 0x00045768
		public int GetJsonMemberIndex(XmlReaderDelegator xmlReader, XmlDictionaryString[] memberNames, int memberIndex, ExtensionDataObject extensionData)
		{
			int num = memberNames.Length;
			if (num != 0)
			{
				int i = 0;
				int num2 = (memberIndex + 1) % num;
				while (i < num)
				{
					if (xmlReader.IsStartElement(memberNames[num2], XmlDictionaryString.Empty))
					{
						return num2;
					}
					i++;
					num2 = (num2 + 1) % num;
				}
				string b;
				if (XmlObjectSerializerReadContextComplexJson.TryGetJsonLocalName(xmlReader, out b))
				{
					int j = 0;
					int num3 = (memberIndex + 1) % num;
					while (j < num)
					{
						if (memberNames[num3].Value == b)
						{
							return num3;
						}
						j++;
						num3 = (num3 + 1) % num;
					}
				}
			}
			base.HandleMemberNotFound(xmlReader, extensionData, memberIndex);
			return num;
		}

		// Token: 0x0600136E RID: 4974 RVA: 0x000475EE File Offset: 0x000457EE
		internal static bool TryGetJsonLocalName(XmlReaderDelegator xmlReader, out string name)
		{
			if (xmlReader.IsStartElement(JsonGlobals.itemDictionaryString, JsonGlobals.itemDictionaryString) && xmlReader.MoveToAttribute("item"))
			{
				name = xmlReader.Value;
				return true;
			}
			name = null;
			return false;
		}

		// Token: 0x0600136F RID: 4975 RVA: 0x00047620 File Offset: 0x00045820
		public static string GetJsonMemberName(XmlReaderDelegator xmlReader)
		{
			string localName;
			if (!XmlObjectSerializerReadContextComplexJson.TryGetJsonLocalName(xmlReader, out localName))
			{
				localName = xmlReader.LocalName;
			}
			return localName;
		}

		// Token: 0x06001370 RID: 4976 RVA: 0x00047640 File Offset: 0x00045840
		public static void ThrowMissingRequiredMembers(object obj, XmlDictionaryString[] memberNames, byte[] expectedElements, byte[] requiredElements)
		{
			StringBuilder stringBuilder = new StringBuilder();
			int num = 0;
			for (int i = 0; i < memberNames.Length; i++)
			{
				if (XmlObjectSerializerReadContextComplexJson.IsBitSet(expectedElements, i) && XmlObjectSerializerReadContextComplexJson.IsBitSet(requiredElements, i))
				{
					if (stringBuilder.Length != 0)
					{
						stringBuilder.Append(", ");
					}
					stringBuilder.Append(memberNames[i]);
					num++;
				}
			}
			if (num == 1)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("Required member {1} in type '{0}' is not found.", new object[]
				{
					DataContract.GetClrTypeFullName(obj.GetType()),
					stringBuilder.ToString()
				})));
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("Required members {0} in type '{1}' are not found.", new object[]
			{
				DataContract.GetClrTypeFullName(obj.GetType()),
				stringBuilder.ToString()
			})));
		}

		// Token: 0x06001371 RID: 4977 RVA: 0x000476FE File Offset: 0x000458FE
		public static void ThrowDuplicateMemberException(object obj, XmlDictionaryString[] memberNames, int memberIndex)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("Duplicate member '{0}' is found in JSON input.", new object[]
			{
				DataContract.GetClrTypeFullName(obj.GetType()),
				memberNames[memberIndex]
			})));
		}

		// Token: 0x06001372 RID: 4978 RVA: 0x0004772E File Offset: 0x0004592E
		[SecuritySafeCritical]
		private static bool IsBitSet(byte[] bytes, int bitIndex)
		{
			return BitFlagsGenerator.IsBitSet(bytes, bitIndex);
		}

		// Token: 0x06001373 RID: 4979 RVA: 0x00047737 File Offset: 0x00045937
		protected override bool IsReadingCollectionExtensionData(XmlReaderDelegator xmlReader)
		{
			return xmlReader.GetAttribute("type") == "array";
		}

		// Token: 0x06001374 RID: 4980 RVA: 0x0004774E File Offset: 0x0004594E
		protected override bool IsReadingClassExtensionData(XmlReaderDelegator xmlReader)
		{
			return xmlReader.GetAttribute("type") == "object";
		}

		// Token: 0x06001375 RID: 4981 RVA: 0x00047765 File Offset: 0x00045965
		protected override XmlReaderDelegator CreateReaderDelegatorForReader(XmlReader xmlReader)
		{
			return new JsonReaderDelegator(xmlReader, this.dateTimeFormat);
		}

		// Token: 0x06001376 RID: 4982 RVA: 0x00047773 File Offset: 0x00045973
		internal override DataContract GetDataContract(RuntimeTypeHandle typeHandle, Type type)
		{
			DataContract dataContract = base.GetDataContract(typeHandle, type);
			DataContractJsonSerializer.CheckIfTypeIsReference(dataContract);
			return dataContract;
		}

		// Token: 0x06001377 RID: 4983 RVA: 0x00047783 File Offset: 0x00045983
		internal override DataContract GetDataContractSkipValidation(int typeId, RuntimeTypeHandle typeHandle, Type type)
		{
			DataContract dataContractSkipValidation = base.GetDataContractSkipValidation(typeId, typeHandle, type);
			DataContractJsonSerializer.CheckIfTypeIsReference(dataContractSkipValidation);
			return dataContractSkipValidation;
		}

		// Token: 0x06001378 RID: 4984 RVA: 0x00047794 File Offset: 0x00045994
		internal override DataContract GetDataContract(int id, RuntimeTypeHandle typeHandle)
		{
			DataContract dataContract = base.GetDataContract(id, typeHandle);
			DataContractJsonSerializer.CheckIfTypeIsReference(dataContract);
			return dataContract;
		}

		// Token: 0x06001379 RID: 4985 RVA: 0x000477A4 File Offset: 0x000459A4
		protected override DataContract ResolveDataContractFromRootDataContract(XmlQualifiedName typeQName)
		{
			return XmlObjectSerializerWriteContextComplexJson.ResolveJsonDataContractFromRootDataContract(this, typeQName, this.rootTypeDataContract);
		}

		// Token: 0x04000A04 RID: 2564
		private string extensionDataValueType;

		// Token: 0x04000A05 RID: 2565
		private DateTimeFormat dateTimeFormat;

		// Token: 0x04000A06 RID: 2566
		private bool useSimpleDictionaryFormat;
	}
}
