﻿using System;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200016B RID: 363
	// (Invoke) Token: 0x06001258 RID: 4696
	internal delegate object JsonFormatClassReaderDelegate(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContextComplexJson context, XmlDictionaryString emptyDictionaryString, XmlDictionaryString[] memberNames);
}
