﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000183 RID: 387
	internal class JsonXmlDataContract : JsonDataContract
	{
		// Token: 0x060012C8 RID: 4808 RVA: 0x00040D44 File Offset: 0x0003EF44
		public JsonXmlDataContract(XmlDataContract traditionalXmlDataContract) : base(traditionalXmlDataContract)
		{
		}

		// Token: 0x060012C9 RID: 4809 RVA: 0x000435E8 File Offset: 0x000417E8
		public override object ReadJsonValueCore(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			string s = jsonReader.ReadElementContentAsString();
			DataContractSerializer dataContractSerializer = new DataContractSerializer(base.TraditionalDataContract.UnderlyingType, this.GetKnownTypesFromContext(context, (context == null) ? null : context.SerializerKnownTypeList), 1, false, false, null);
			MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(s));
			XmlDictionaryReaderQuotas readerQuotas = ((JsonReaderDelegator)jsonReader).ReaderQuotas;
			object obj;
			if (readerQuotas == null)
			{
				obj = dataContractSerializer.ReadObject(stream);
			}
			else
			{
				obj = dataContractSerializer.ReadObject(XmlDictionaryReader.CreateTextReader(stream, readerQuotas));
			}
			if (context != null)
			{
				context.AddNewObject(obj);
			}
			return obj;
		}

		// Token: 0x060012CA RID: 4810 RVA: 0x0004366C File Offset: 0x0004186C
		public override void WriteJsonValueCore(XmlWriterDelegator jsonWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, RuntimeTypeHandle declaredTypeHandle)
		{
			XmlObjectSerializer xmlObjectSerializer = new DataContractSerializer(Type.GetTypeFromHandle(declaredTypeHandle), this.GetKnownTypesFromContext(context, (context == null) ? null : context.SerializerKnownTypeList), 1, false, false, null);
			MemoryStream memoryStream = new MemoryStream();
			xmlObjectSerializer.WriteObject(memoryStream, obj);
			memoryStream.Position = 0L;
			string value = new StreamReader(memoryStream).ReadToEnd();
			jsonWriter.WriteString(value);
		}

		// Token: 0x060012CB RID: 4811 RVA: 0x000436C4 File Offset: 0x000418C4
		private List<Type> GetKnownTypesFromContext(XmlObjectSerializerContext context, IList<Type> serializerKnownTypeList)
		{
			List<Type> list = new List<Type>();
			if (context != null)
			{
				List<XmlQualifiedName> list2 = new List<XmlQualifiedName>();
				Dictionary<XmlQualifiedName, DataContract>[] dataContractDictionaries = context.scopedKnownTypes.dataContractDictionaries;
				if (dataContractDictionaries != null)
				{
					foreach (Dictionary<XmlQualifiedName, DataContract> dictionary in dataContractDictionaries)
					{
						if (dictionary != null)
						{
							foreach (KeyValuePair<XmlQualifiedName, DataContract> keyValuePair in dictionary)
							{
								if (!list2.Contains(keyValuePair.Key))
								{
									list2.Add(keyValuePair.Key);
									list.Add(keyValuePair.Value.UnderlyingType);
								}
							}
						}
					}
				}
				if (serializerKnownTypeList != null)
				{
					list.AddRange(serializerKnownTypeList);
				}
			}
			return list;
		}
	}
}
