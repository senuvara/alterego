﻿using System;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200016D RID: 365
	// (Invoke) Token: 0x06001260 RID: 4704
	internal delegate void JsonFormatGetOnlyCollectionReaderDelegate(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContextComplexJson context, XmlDictionaryString emptyDictionaryString, XmlDictionaryString itemName, CollectionDataContract collectionContract);
}
