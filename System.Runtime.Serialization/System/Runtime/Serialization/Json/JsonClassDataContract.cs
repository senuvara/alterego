﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Threading;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000161 RID: 353
	internal class JsonClassDataContract : JsonDataContract
	{
		// Token: 0x060011F8 RID: 4600 RVA: 0x00040D6F File Offset: 0x0003EF6F
		[SecuritySafeCritical]
		public JsonClassDataContract(ClassDataContract traditionalDataContract) : base(new JsonClassDataContract.JsonClassDataContractCriticalHelper(traditionalDataContract))
		{
			this.helper = (base.Helper as JsonClassDataContract.JsonClassDataContractCriticalHelper);
		}

		// Token: 0x170003DB RID: 987
		// (get) Token: 0x060011F9 RID: 4601 RVA: 0x00040D90 File Offset: 0x0003EF90
		internal JsonFormatClassReaderDelegate JsonFormatReaderDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.JsonFormatReaderDelegate == null)
				{
					lock (this)
					{
						if (this.helper.JsonFormatReaderDelegate == null)
						{
							if (this.TraditionalClassDataContract.IsReadOnlyContract)
							{
								DataContract.ThrowInvalidDataContractException(this.TraditionalClassDataContract.DeserializationExceptionMessage, null);
							}
							JsonFormatClassReaderDelegate jsonFormatReaderDelegate = new JsonFormatReaderGenerator().GenerateClassReader(this.TraditionalClassDataContract);
							Thread.MemoryBarrier();
							this.helper.JsonFormatReaderDelegate = jsonFormatReaderDelegate;
						}
					}
				}
				return this.helper.JsonFormatReaderDelegate;
			}
		}

		// Token: 0x170003DC RID: 988
		// (get) Token: 0x060011FA RID: 4602 RVA: 0x00040E2C File Offset: 0x0003F02C
		internal JsonFormatClassWriterDelegate JsonFormatWriterDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.JsonFormatWriterDelegate == null)
				{
					lock (this)
					{
						if (this.helper.JsonFormatWriterDelegate == null)
						{
							JsonFormatClassWriterDelegate jsonFormatWriterDelegate = new JsonFormatWriterGenerator().GenerateClassWriter(this.TraditionalClassDataContract);
							Thread.MemoryBarrier();
							this.helper.JsonFormatWriterDelegate = jsonFormatWriterDelegate;
						}
					}
				}
				return this.helper.JsonFormatWriterDelegate;
			}
		}

		// Token: 0x170003DD RID: 989
		// (get) Token: 0x060011FB RID: 4603 RVA: 0x00040EA8 File Offset: 0x0003F0A8
		internal XmlDictionaryString[] MemberNames
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.MemberNames;
			}
		}

		// Token: 0x170003DE RID: 990
		// (get) Token: 0x060011FC RID: 4604 RVA: 0x00040EB5 File Offset: 0x0003F0B5
		internal override string TypeName
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.TypeName;
			}
		}

		// Token: 0x170003DF RID: 991
		// (get) Token: 0x060011FD RID: 4605 RVA: 0x00040EC2 File Offset: 0x0003F0C2
		private ClassDataContract TraditionalClassDataContract
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.TraditionalClassDataContract;
			}
		}

		// Token: 0x060011FE RID: 4606 RVA: 0x00040ECF File Offset: 0x0003F0CF
		public override object ReadJsonValueCore(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			jsonReader.Read();
			object result = this.JsonFormatReaderDelegate(jsonReader, context, XmlDictionaryString.Empty, this.MemberNames);
			jsonReader.ReadEndElement();
			return result;
		}

		// Token: 0x060011FF RID: 4607 RVA: 0x00040EF6 File Offset: 0x0003F0F6
		public override void WriteJsonValueCore(XmlWriterDelegator jsonWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, RuntimeTypeHandle declaredTypeHandle)
		{
			jsonWriter.WriteAttributeString(null, "type", null, "object");
			this.JsonFormatWriterDelegate(jsonWriter, obj, context, this.TraditionalClassDataContract, this.MemberNames);
		}

		// Token: 0x0400094F RID: 2383
		[SecurityCritical]
		private JsonClassDataContract.JsonClassDataContractCriticalHelper helper;

		// Token: 0x02000162 RID: 354
		private class JsonClassDataContractCriticalHelper : JsonDataContract.JsonDataContractCriticalHelper
		{
			// Token: 0x06001200 RID: 4608 RVA: 0x00040F24 File Offset: 0x0003F124
			public JsonClassDataContractCriticalHelper(ClassDataContract traditionalDataContract) : base(traditionalDataContract)
			{
				this.typeName = (string.IsNullOrEmpty(traditionalDataContract.Namespace.Value) ? traditionalDataContract.Name.Value : (traditionalDataContract.Name.Value + ":" + XmlObjectSerializerWriteContextComplexJson.TruncateDefaultDataContractNamespace(traditionalDataContract.Namespace.Value)));
				this.traditionalClassDataContract = traditionalDataContract;
				this.CopyMembersAndCheckDuplicateNames();
			}

			// Token: 0x170003E0 RID: 992
			// (get) Token: 0x06001201 RID: 4609 RVA: 0x00040F8F File Offset: 0x0003F18F
			// (set) Token: 0x06001202 RID: 4610 RVA: 0x00040F97 File Offset: 0x0003F197
			internal JsonFormatClassReaderDelegate JsonFormatReaderDelegate
			{
				get
				{
					return this.jsonFormatReaderDelegate;
				}
				set
				{
					this.jsonFormatReaderDelegate = value;
				}
			}

			// Token: 0x170003E1 RID: 993
			// (get) Token: 0x06001203 RID: 4611 RVA: 0x00040FA0 File Offset: 0x0003F1A0
			// (set) Token: 0x06001204 RID: 4612 RVA: 0x00040FA8 File Offset: 0x0003F1A8
			internal JsonFormatClassWriterDelegate JsonFormatWriterDelegate
			{
				get
				{
					return this.jsonFormatWriterDelegate;
				}
				set
				{
					this.jsonFormatWriterDelegate = value;
				}
			}

			// Token: 0x170003E2 RID: 994
			// (get) Token: 0x06001205 RID: 4613 RVA: 0x00040FB1 File Offset: 0x0003F1B1
			internal XmlDictionaryString[] MemberNames
			{
				get
				{
					return this.memberNames;
				}
			}

			// Token: 0x170003E3 RID: 995
			// (get) Token: 0x06001206 RID: 4614 RVA: 0x00040FB9 File Offset: 0x0003F1B9
			internal ClassDataContract TraditionalClassDataContract
			{
				get
				{
					return this.traditionalClassDataContract;
				}
			}

			// Token: 0x06001207 RID: 4615 RVA: 0x00040FC4 File Offset: 0x0003F1C4
			private void CopyMembersAndCheckDuplicateNames()
			{
				if (this.traditionalClassDataContract.MemberNames != null)
				{
					int num = this.traditionalClassDataContract.MemberNames.Length;
					Dictionary<string, object> dictionary = new Dictionary<string, object>(num);
					XmlDictionaryString[] array = new XmlDictionaryString[num];
					for (int i = 0; i < num; i++)
					{
						if (dictionary.ContainsKey(this.traditionalClassDataContract.MemberNames[i].Value))
						{
							throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("Duplicate member, including '{1}', is found in JSON input, in type '{0}'.", new object[]
							{
								DataContract.GetClrTypeFullName(this.traditionalClassDataContract.UnderlyingType),
								this.traditionalClassDataContract.MemberNames[i].Value
							})));
						}
						dictionary.Add(this.traditionalClassDataContract.MemberNames[i].Value, null);
						array[i] = DataContractJsonSerializer.ConvertXmlNameToJsonName(this.traditionalClassDataContract.MemberNames[i]);
					}
					this.memberNames = array;
				}
			}

			// Token: 0x04000950 RID: 2384
			private JsonFormatClassReaderDelegate jsonFormatReaderDelegate;

			// Token: 0x04000951 RID: 2385
			private JsonFormatClassWriterDelegate jsonFormatWriterDelegate;

			// Token: 0x04000952 RID: 2386
			private XmlDictionaryString[] memberNames;

			// Token: 0x04000953 RID: 2387
			private ClassDataContract traditionalClassDataContract;

			// Token: 0x04000954 RID: 2388
			private string typeName;
		}
	}
}
