﻿using System;
using System.IO;
using System.Text;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000167 RID: 359
	internal class JsonEncodingStreamWrapper : Stream
	{
		// Token: 0x0600122F RID: 4655 RVA: 0x00041A18 File Offset: 0x0003FC18
		public JsonEncodingStreamWrapper(Stream stream, Encoding encoding, bool isReader)
		{
			this.isReading = isReader;
			if (isReader)
			{
				this.InitForReading(stream, encoding);
				return;
			}
			if (encoding == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("encoding");
			}
			this.InitForWriting(stream, encoding);
		}

		// Token: 0x170003F3 RID: 1011
		// (get) Token: 0x06001230 RID: 4656 RVA: 0x00041A55 File Offset: 0x0003FC55
		public override bool CanRead
		{
			get
			{
				return this.isReading && this.stream.CanRead;
			}
		}

		// Token: 0x170003F4 RID: 1012
		// (get) Token: 0x06001231 RID: 4657 RVA: 0x0000310F File Offset: 0x0000130F
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x06001232 RID: 4658 RVA: 0x00041A6C File Offset: 0x0003FC6C
		public override bool CanTimeout
		{
			get
			{
				return this.stream.CanTimeout;
			}
		}

		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x06001233 RID: 4659 RVA: 0x00041A79 File Offset: 0x0003FC79
		public override bool CanWrite
		{
			get
			{
				return !this.isReading && this.stream.CanWrite;
			}
		}

		// Token: 0x170003F7 RID: 1015
		// (get) Token: 0x06001234 RID: 4660 RVA: 0x00041A90 File Offset: 0x0003FC90
		public override long Length
		{
			get
			{
				return this.stream.Length;
			}
		}

		// Token: 0x170003F8 RID: 1016
		// (get) Token: 0x06001235 RID: 4661 RVA: 0x00003129 File Offset: 0x00001329
		// (set) Token: 0x06001236 RID: 4662 RVA: 0x00003129 File Offset: 0x00001329
		public override long Position
		{
			get
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
			}
			set
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
			}
		}

		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x06001237 RID: 4663 RVA: 0x00041A9D File Offset: 0x0003FC9D
		// (set) Token: 0x06001238 RID: 4664 RVA: 0x00041AAA File Offset: 0x0003FCAA
		public override int ReadTimeout
		{
			get
			{
				return this.stream.ReadTimeout;
			}
			set
			{
				this.stream.ReadTimeout = value;
			}
		}

		// Token: 0x170003FA RID: 1018
		// (get) Token: 0x06001239 RID: 4665 RVA: 0x00041AB8 File Offset: 0x0003FCB8
		// (set) Token: 0x0600123A RID: 4666 RVA: 0x00041AC5 File Offset: 0x0003FCC5
		public override int WriteTimeout
		{
			get
			{
				return this.stream.WriteTimeout;
			}
			set
			{
				this.stream.WriteTimeout = value;
			}
		}

		// Token: 0x0600123B RID: 4667 RVA: 0x00041AD4 File Offset: 0x0003FCD4
		public static ArraySegment<byte> ProcessBuffer(byte[] buffer, int offset, int count, Encoding encoding)
		{
			ArraySegment<byte> result;
			try
			{
				JsonEncodingStreamWrapper.SupportedEncoding supportedEncoding = JsonEncodingStreamWrapper.GetSupportedEncoding(encoding);
				JsonEncodingStreamWrapper.SupportedEncoding supportedEncoding2;
				if (count < 2)
				{
					supportedEncoding2 = JsonEncodingStreamWrapper.SupportedEncoding.UTF8;
				}
				else
				{
					supportedEncoding2 = JsonEncodingStreamWrapper.ReadEncoding(buffer[offset], buffer[offset + 1]);
				}
				if (supportedEncoding != JsonEncodingStreamWrapper.SupportedEncoding.None && supportedEncoding != supportedEncoding2)
				{
					JsonEncodingStreamWrapper.ThrowExpectedEncodingMismatch(supportedEncoding, supportedEncoding2);
				}
				if (supportedEncoding2 == JsonEncodingStreamWrapper.SupportedEncoding.UTF8)
				{
					result = new ArraySegment<byte>(buffer, offset, count);
				}
				else
				{
					result = new ArraySegment<byte>(JsonEncodingStreamWrapper.ValidatingUTF8.GetBytes(JsonEncodingStreamWrapper.GetEncoding(supportedEncoding2).GetChars(buffer, offset, count)));
				}
			}
			catch (DecoderFallbackException innerException)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid bytes in JSON."), innerException));
			}
			return result;
		}

		// Token: 0x0600123C RID: 4668 RVA: 0x00041B64 File Offset: 0x0003FD64
		public override void Close()
		{
			this.Flush();
			base.Close();
			this.stream.Close();
		}

		// Token: 0x0600123D RID: 4669 RVA: 0x00041B7D File Offset: 0x0003FD7D
		public override void Flush()
		{
			this.stream.Flush();
		}

		// Token: 0x0600123E RID: 4670 RVA: 0x00041B8C File Offset: 0x0003FD8C
		public override int Read(byte[] buffer, int offset, int count)
		{
			int result;
			try
			{
				if (this.byteCount == 0)
				{
					if (this.encodingCode == JsonEncodingStreamWrapper.SupportedEncoding.UTF8)
					{
						return this.stream.Read(buffer, offset, count);
					}
					this.byteOffset = 0;
					this.byteCount = this.stream.Read(this.bytes, this.byteCount, (this.chars.Length - 1) * 2);
					if (this.byteCount == 0)
					{
						return 0;
					}
					this.CleanupCharBreak();
					int charCount = this.encoding.GetChars(this.bytes, 0, this.byteCount, this.chars, 0);
					this.byteCount = Encoding.UTF8.GetBytes(this.chars, 0, charCount, this.bytes, 0);
				}
				if (this.byteCount < count)
				{
					count = this.byteCount;
				}
				Buffer.BlockCopy(this.bytes, this.byteOffset, buffer, offset, count);
				this.byteOffset += count;
				this.byteCount -= count;
				result = count;
			}
			catch (DecoderFallbackException innerException)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid bytes in JSON."), innerException));
			}
			return result;
		}

		// Token: 0x0600123F RID: 4671 RVA: 0x00041CB4 File Offset: 0x0003FEB4
		public override int ReadByte()
		{
			if (this.byteCount == 0 && this.encodingCode == JsonEncodingStreamWrapper.SupportedEncoding.UTF8)
			{
				return this.stream.ReadByte();
			}
			if (this.Read(this.byteBuffer, 0, 1) == 0)
			{
				return -1;
			}
			return (int)this.byteBuffer[0];
		}

		// Token: 0x06001240 RID: 4672 RVA: 0x00003129 File Offset: 0x00001329
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
		}

		// Token: 0x06001241 RID: 4673 RVA: 0x00003129 File Offset: 0x00001329
		public override void SetLength(long value)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException());
		}

		// Token: 0x06001242 RID: 4674 RVA: 0x00041CEC File Offset: 0x0003FEEC
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (this.encodingCode == JsonEncodingStreamWrapper.SupportedEncoding.UTF8)
			{
				this.stream.Write(buffer, offset, count);
				return;
			}
			while (count > 0)
			{
				int num = (this.chars.Length < count) ? this.chars.Length : count;
				int charCount = this.dec.GetChars(buffer, offset, num, this.chars, 0, false);
				this.byteCount = this.enc.GetBytes(this.chars, 0, charCount, this.bytes, 0, false);
				this.stream.Write(this.bytes, 0, this.byteCount);
				offset += num;
				count -= num;
			}
		}

		// Token: 0x06001243 RID: 4675 RVA: 0x00041D88 File Offset: 0x0003FF88
		public override void WriteByte(byte b)
		{
			if (this.encodingCode == JsonEncodingStreamWrapper.SupportedEncoding.UTF8)
			{
				this.stream.WriteByte(b);
				return;
			}
			this.byteBuffer[0] = b;
			this.Write(this.byteBuffer, 0, 1);
		}

		// Token: 0x06001244 RID: 4676 RVA: 0x00041DB6 File Offset: 0x0003FFB6
		private static Encoding GetEncoding(JsonEncodingStreamWrapper.SupportedEncoding e)
		{
			switch (e)
			{
			case JsonEncodingStreamWrapper.SupportedEncoding.UTF8:
				return JsonEncodingStreamWrapper.ValidatingUTF8;
			case JsonEncodingStreamWrapper.SupportedEncoding.UTF16LE:
				return JsonEncodingStreamWrapper.ValidatingUTF16;
			case JsonEncodingStreamWrapper.SupportedEncoding.UTF16BE:
				return JsonEncodingStreamWrapper.ValidatingBEUTF16;
			default:
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON Encoding is not supported.")));
			}
		}

		// Token: 0x06001245 RID: 4677 RVA: 0x00041DF2 File Offset: 0x0003FFF2
		private static string GetEncodingName(JsonEncodingStreamWrapper.SupportedEncoding enc)
		{
			switch (enc)
			{
			case JsonEncodingStreamWrapper.SupportedEncoding.UTF8:
				return "utf-8";
			case JsonEncodingStreamWrapper.SupportedEncoding.UTF16LE:
				return "utf-16LE";
			case JsonEncodingStreamWrapper.SupportedEncoding.UTF16BE:
				return "utf-16BE";
			default:
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON Encoding is not supported.")));
			}
		}

		// Token: 0x06001246 RID: 4678 RVA: 0x00041E30 File Offset: 0x00040030
		private static JsonEncodingStreamWrapper.SupportedEncoding GetSupportedEncoding(Encoding encoding)
		{
			if (encoding == null)
			{
				return JsonEncodingStreamWrapper.SupportedEncoding.None;
			}
			if (encoding.WebName == JsonEncodingStreamWrapper.ValidatingUTF8.WebName)
			{
				return JsonEncodingStreamWrapper.SupportedEncoding.UTF8;
			}
			if (encoding.WebName == JsonEncodingStreamWrapper.ValidatingUTF16.WebName)
			{
				return JsonEncodingStreamWrapper.SupportedEncoding.UTF16LE;
			}
			if (encoding.WebName == JsonEncodingStreamWrapper.ValidatingBEUTF16.WebName)
			{
				return JsonEncodingStreamWrapper.SupportedEncoding.UTF16BE;
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON Encoding is not supported.")));
		}

		// Token: 0x06001247 RID: 4679 RVA: 0x00041EA1 File Offset: 0x000400A1
		private static JsonEncodingStreamWrapper.SupportedEncoding ReadEncoding(byte b1, byte b2)
		{
			if (b1 == 0 && b2 != 0)
			{
				return JsonEncodingStreamWrapper.SupportedEncoding.UTF16BE;
			}
			if (b1 != 0 && b2 == 0)
			{
				return JsonEncodingStreamWrapper.SupportedEncoding.UTF16LE;
			}
			if (b1 == 0 && b2 == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid bytes in JSON.")));
			}
			return JsonEncodingStreamWrapper.SupportedEncoding.UTF8;
		}

		// Token: 0x06001248 RID: 4680 RVA: 0x00041ECF File Offset: 0x000400CF
		private static void ThrowExpectedEncodingMismatch(JsonEncodingStreamWrapper.SupportedEncoding expEnc, JsonEncodingStreamWrapper.SupportedEncoding actualEnc)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Expected encoding '{0}', got '{1}' instead.", new object[]
			{
				JsonEncodingStreamWrapper.GetEncodingName(expEnc),
				JsonEncodingStreamWrapper.GetEncodingName(actualEnc)
			})));
		}

		// Token: 0x06001249 RID: 4681 RVA: 0x00041F00 File Offset: 0x00040100
		private void CleanupCharBreak()
		{
			int num = this.byteOffset + this.byteCount;
			if (this.byteCount % 2 != 0)
			{
				int num2 = this.stream.ReadByte();
				if (num2 < 0)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Unexpected end of file in JSON.")));
				}
				this.bytes[num++] = (byte)num2;
				this.byteCount++;
			}
			int num3;
			if (this.encodingCode == JsonEncodingStreamWrapper.SupportedEncoding.UTF16LE)
			{
				num3 = (int)this.bytes[num - 2] + ((int)this.bytes[num - 1] << 8);
			}
			else
			{
				num3 = (int)this.bytes[num - 1] + ((int)this.bytes[num - 2] << 8);
			}
			if ((num3 & 56320) != 56320 && num3 >= 55296 && num3 <= 56319)
			{
				int num4 = this.stream.ReadByte();
				int num5 = this.stream.ReadByte();
				if (num5 < 0)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Unexpected end of file in JSON.")));
				}
				this.bytes[num++] = (byte)num4;
				this.bytes[num++] = (byte)num5;
				this.byteCount += 2;
			}
		}

		// Token: 0x0600124A RID: 4682 RVA: 0x0004201D File Offset: 0x0004021D
		private void EnsureBuffers()
		{
			this.EnsureByteBuffer();
			if (this.chars == null)
			{
				this.chars = new char[128];
			}
		}

		// Token: 0x0600124B RID: 4683 RVA: 0x0004203D File Offset: 0x0004023D
		private void EnsureByteBuffer()
		{
			if (this.bytes != null)
			{
				return;
			}
			this.bytes = new byte[512];
			this.byteOffset = 0;
			this.byteCount = 0;
		}

		// Token: 0x0600124C RID: 4684 RVA: 0x00042068 File Offset: 0x00040268
		private void FillBuffer(int count)
		{
			int num;
			for (count -= this.byteCount; count > 0; count -= num)
			{
				num = this.stream.Read(this.bytes, this.byteOffset + this.byteCount, count);
				if (num == 0)
				{
					break;
				}
				this.byteCount += num;
			}
		}

		// Token: 0x0600124D RID: 4685 RVA: 0x000420BC File Offset: 0x000402BC
		private void InitForReading(Stream inputStream, Encoding expectedEncoding)
		{
			try
			{
				this.stream = new BufferedStream(inputStream);
				JsonEncodingStreamWrapper.SupportedEncoding supportedEncoding = JsonEncodingStreamWrapper.GetSupportedEncoding(expectedEncoding);
				JsonEncodingStreamWrapper.SupportedEncoding supportedEncoding2 = this.ReadEncoding();
				if (supportedEncoding != JsonEncodingStreamWrapper.SupportedEncoding.None && supportedEncoding != supportedEncoding2)
				{
					JsonEncodingStreamWrapper.ThrowExpectedEncodingMismatch(supportedEncoding, supportedEncoding2);
				}
				if (supportedEncoding2 != JsonEncodingStreamWrapper.SupportedEncoding.UTF8)
				{
					this.EnsureBuffers();
					this.FillBuffer(254);
					this.encodingCode = supportedEncoding2;
					this.encoding = JsonEncodingStreamWrapper.GetEncoding(supportedEncoding2);
					this.CleanupCharBreak();
					int charCount = this.encoding.GetChars(this.bytes, this.byteOffset, this.byteCount, this.chars, 0);
					this.byteOffset = 0;
					this.byteCount = JsonEncodingStreamWrapper.ValidatingUTF8.GetBytes(this.chars, 0, charCount, this.bytes, 0);
				}
			}
			catch (DecoderFallbackException innerException)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid bytes in JSON."), innerException));
			}
		}

		// Token: 0x0600124E RID: 4686 RVA: 0x00042194 File Offset: 0x00040394
		private void InitForWriting(Stream outputStream, Encoding writeEncoding)
		{
			this.encoding = writeEncoding;
			this.stream = new BufferedStream(outputStream);
			this.encodingCode = JsonEncodingStreamWrapper.GetSupportedEncoding(writeEncoding);
			if (this.encodingCode != JsonEncodingStreamWrapper.SupportedEncoding.UTF8)
			{
				this.EnsureBuffers();
				this.dec = JsonEncodingStreamWrapper.ValidatingUTF8.GetDecoder();
				this.enc = this.encoding.GetEncoder();
			}
		}

		// Token: 0x0600124F RID: 4687 RVA: 0x000421F0 File Offset: 0x000403F0
		private JsonEncodingStreamWrapper.SupportedEncoding ReadEncoding()
		{
			int num = this.stream.ReadByte();
			int num2 = this.stream.ReadByte();
			this.EnsureByteBuffer();
			JsonEncodingStreamWrapper.SupportedEncoding result;
			if (num == -1)
			{
				result = JsonEncodingStreamWrapper.SupportedEncoding.UTF8;
				this.byteCount = 0;
			}
			else if (num2 == -1)
			{
				result = JsonEncodingStreamWrapper.SupportedEncoding.UTF8;
				this.bytes[0] = (byte)num;
				this.byteCount = 1;
			}
			else
			{
				result = JsonEncodingStreamWrapper.ReadEncoding((byte)num, (byte)num2);
				this.bytes[0] = (byte)num;
				this.bytes[1] = (byte)num2;
				this.byteCount = 2;
			}
			return result;
		}

		// Token: 0x06001250 RID: 4688 RVA: 0x0004226C File Offset: 0x0004046C
		// Note: this type is marked as 'beforefieldinit'.
		static JsonEncodingStreamWrapper()
		{
		}

		// Token: 0x04000964 RID: 2404
		private static readonly UnicodeEncoding SafeBEUTF16 = new UnicodeEncoding(true, false, false);

		// Token: 0x04000965 RID: 2405
		private static readonly UnicodeEncoding SafeUTF16 = new UnicodeEncoding(false, false, false);

		// Token: 0x04000966 RID: 2406
		private static readonly UTF8Encoding SafeUTF8 = new UTF8Encoding(false, false);

		// Token: 0x04000967 RID: 2407
		private static readonly UnicodeEncoding ValidatingBEUTF16 = new UnicodeEncoding(true, false, true);

		// Token: 0x04000968 RID: 2408
		private static readonly UnicodeEncoding ValidatingUTF16 = new UnicodeEncoding(false, false, true);

		// Token: 0x04000969 RID: 2409
		private static readonly UTF8Encoding ValidatingUTF8 = new UTF8Encoding(false, true);

		// Token: 0x0400096A RID: 2410
		private const int BufferLength = 128;

		// Token: 0x0400096B RID: 2411
		private byte[] byteBuffer = new byte[1];

		// Token: 0x0400096C RID: 2412
		private int byteCount;

		// Token: 0x0400096D RID: 2413
		private int byteOffset;

		// Token: 0x0400096E RID: 2414
		private byte[] bytes;

		// Token: 0x0400096F RID: 2415
		private char[] chars;

		// Token: 0x04000970 RID: 2416
		private Decoder dec;

		// Token: 0x04000971 RID: 2417
		private Encoder enc;

		// Token: 0x04000972 RID: 2418
		private Encoding encoding;

		// Token: 0x04000973 RID: 2419
		private JsonEncodingStreamWrapper.SupportedEncoding encodingCode;

		// Token: 0x04000974 RID: 2420
		private bool isReading;

		// Token: 0x04000975 RID: 2421
		private Stream stream;

		// Token: 0x02000168 RID: 360
		private enum SupportedEncoding
		{
			// Token: 0x04000977 RID: 2423
			UTF8,
			// Token: 0x04000978 RID: 2424
			UTF16LE,
			// Token: 0x04000979 RID: 2425
			UTF16BE,
			// Token: 0x0400097A RID: 2426
			None
		}
	}
}
