﻿using System;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000160 RID: 352
	internal class JsonByteArrayDataContract : JsonDataContract
	{
		// Token: 0x060011F6 RID: 4598 RVA: 0x00040D44 File Offset: 0x0003EF44
		public JsonByteArrayDataContract(ByteArrayDataContract traditionalByteArrayDataContract) : base(traditionalByteArrayDataContract)
		{
		}

		// Token: 0x060011F7 RID: 4599 RVA: 0x00040D4D File Offset: 0x0003EF4D
		public override object ReadJsonValueCore(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			if (context != null)
			{
				return JsonDataContract.HandleReadValue(jsonReader.ReadElementContentAsBase64(), context);
			}
			if (!JsonDataContract.TryReadNullAtTopLevel(jsonReader))
			{
				return jsonReader.ReadElementContentAsBase64();
			}
			return null;
		}
	}
}
