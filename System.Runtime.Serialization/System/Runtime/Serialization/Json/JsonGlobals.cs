﻿using System;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000179 RID: 377
	internal static class JsonGlobals
	{
		// Token: 0x06001283 RID: 4739 RVA: 0x0004266C File Offset: 0x0004086C
		// Note: this type is marked as 'beforefieldinit'.
		static JsonGlobals()
		{
		}

		// Token: 0x04000984 RID: 2436
		public static readonly int DataContractXsdBaseNamespaceLength = "http://schemas.datacontract.org/2004/07/".Length;

		// Token: 0x04000985 RID: 2437
		public static readonly XmlDictionaryString dDictionaryString = new XmlDictionary().Add("d");

		// Token: 0x04000986 RID: 2438
		public static readonly char[] floatingPointCharacters = new char[]
		{
			'.',
			'e'
		};

		// Token: 0x04000987 RID: 2439
		public static readonly XmlDictionaryString itemDictionaryString = new XmlDictionary().Add("item");

		// Token: 0x04000988 RID: 2440
		public static readonly XmlDictionaryString rootDictionaryString = new XmlDictionary().Add("root");

		// Token: 0x04000989 RID: 2441
		public static readonly long unixEpochTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;

		// Token: 0x0400098A RID: 2442
		public const string applicationJsonMediaType = "application/json";

		// Token: 0x0400098B RID: 2443
		public const string arrayString = "array";

		// Token: 0x0400098C RID: 2444
		public const string booleanString = "boolean";

		// Token: 0x0400098D RID: 2445
		public const string CacheControlString = "Cache-Control";

		// Token: 0x0400098E RID: 2446
		public const byte CollectionByte = 91;

		// Token: 0x0400098F RID: 2447
		public const char CollectionChar = '[';

		// Token: 0x04000990 RID: 2448
		public const string DateTimeEndGuardReader = ")/";

		// Token: 0x04000991 RID: 2449
		public const string DateTimeEndGuardWriter = ")\\/";

		// Token: 0x04000992 RID: 2450
		public const string DateTimeStartGuardReader = "/Date(";

		// Token: 0x04000993 RID: 2451
		public const string DateTimeStartGuardWriter = "\\/Date(";

		// Token: 0x04000994 RID: 2452
		public const string dString = "d";

		// Token: 0x04000995 RID: 2453
		public const byte EndCollectionByte = 93;

		// Token: 0x04000996 RID: 2454
		public const char EndCollectionChar = ']';

		// Token: 0x04000997 RID: 2455
		public const byte EndObjectByte = 125;

		// Token: 0x04000998 RID: 2456
		public const char EndObjectChar = '}';

		// Token: 0x04000999 RID: 2457
		public const string ExpiresString = "Expires";

		// Token: 0x0400099A RID: 2458
		public const string IfModifiedSinceString = "If-Modified-Since";

		// Token: 0x0400099B RID: 2459
		public const string itemString = "item";

		// Token: 0x0400099C RID: 2460
		public const string jsonerrorString = "jsonerror";

		// Token: 0x0400099D RID: 2461
		public const string KeyString = "Key";

		// Token: 0x0400099E RID: 2462
		public const string LastModifiedString = "Last-Modified";

		// Token: 0x0400099F RID: 2463
		public const int maxScopeSize = 25;

		// Token: 0x040009A0 RID: 2464
		public const byte MemberSeparatorByte = 44;

		// Token: 0x040009A1 RID: 2465
		public const char MemberSeparatorChar = ',';

		// Token: 0x040009A2 RID: 2466
		public const byte NameValueSeparatorByte = 58;

		// Token: 0x040009A3 RID: 2467
		public const char NameValueSeparatorChar = ':';

		// Token: 0x040009A4 RID: 2468
		public const string NameValueSeparatorString = ":";

		// Token: 0x040009A5 RID: 2469
		public const string nullString = "null";

		// Token: 0x040009A6 RID: 2470
		public const string numberString = "number";

		// Token: 0x040009A7 RID: 2471
		public const byte ObjectByte = 123;

		// Token: 0x040009A8 RID: 2472
		public const char ObjectChar = '{';

		// Token: 0x040009A9 RID: 2473
		public const string objectString = "object";

		// Token: 0x040009AA RID: 2474
		public const string publicString = "public";

		// Token: 0x040009AB RID: 2475
		public const byte QuoteByte = 34;

		// Token: 0x040009AC RID: 2476
		public const char QuoteChar = '"';

		// Token: 0x040009AD RID: 2477
		public const string rootString = "root";

		// Token: 0x040009AE RID: 2478
		public const string serverTypeString = "__type";

		// Token: 0x040009AF RID: 2479
		public const string stringString = "string";

		// Token: 0x040009B0 RID: 2480
		public const string textJsonMediaType = "text/json";

		// Token: 0x040009B1 RID: 2481
		public const string trueString = "true";

		// Token: 0x040009B2 RID: 2482
		public const string typeString = "type";

		// Token: 0x040009B3 RID: 2483
		public const string ValueString = "Value";

		// Token: 0x040009B4 RID: 2484
		public const char WhitespaceChar = ' ';

		// Token: 0x040009B5 RID: 2485
		public const string xmlnsPrefix = "xmlns";

		// Token: 0x040009B6 RID: 2486
		public const string xmlPrefix = "xml";
	}
}
