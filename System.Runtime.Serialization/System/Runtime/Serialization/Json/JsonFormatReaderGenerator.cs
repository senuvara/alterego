﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Diagnostics.Application;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200016E RID: 366
	internal sealed class JsonFormatReaderGenerator
	{
		// Token: 0x06001263 RID: 4707 RVA: 0x00042390 File Offset: 0x00040590
		[SecurityCritical]
		public JsonFormatReaderGenerator()
		{
			this.helper = new JsonFormatReaderGenerator.CriticalHelper();
		}

		// Token: 0x06001264 RID: 4708 RVA: 0x000423A4 File Offset: 0x000405A4
		[SecurityCritical]
		public JsonFormatClassReaderDelegate GenerateClassReader(ClassDataContract classContract)
		{
			JsonFormatClassReaderDelegate result;
			try
			{
				if (TD.DCJsonGenReaderStartIsEnabled())
				{
					TD.DCJsonGenReaderStart("Class", classContract.UnderlyingType.FullName);
				}
				result = this.helper.GenerateClassReader(classContract);
			}
			finally
			{
				if (TD.DCJsonGenReaderStopIsEnabled())
				{
					TD.DCJsonGenReaderStop();
				}
			}
			return result;
		}

		// Token: 0x06001265 RID: 4709 RVA: 0x000423FC File Offset: 0x000405FC
		[SecurityCritical]
		public JsonFormatCollectionReaderDelegate GenerateCollectionReader(CollectionDataContract collectionContract)
		{
			JsonFormatCollectionReaderDelegate result;
			try
			{
				if (TD.DCJsonGenReaderStartIsEnabled())
				{
					TD.DCJsonGenReaderStart("Collection", collectionContract.StableName.Name);
				}
				result = this.helper.GenerateCollectionReader(collectionContract);
			}
			finally
			{
				if (TD.DCJsonGenReaderStopIsEnabled())
				{
					TD.DCJsonGenReaderStop();
				}
			}
			return result;
		}

		// Token: 0x06001266 RID: 4710 RVA: 0x00042454 File Offset: 0x00040654
		[SecurityCritical]
		public JsonFormatGetOnlyCollectionReaderDelegate GenerateGetOnlyCollectionReader(CollectionDataContract collectionContract)
		{
			JsonFormatGetOnlyCollectionReaderDelegate result;
			try
			{
				if (TD.DCJsonGenReaderStartIsEnabled())
				{
					TD.DCJsonGenReaderStart("GetOnlyCollection", collectionContract.UnderlyingType.FullName);
				}
				result = this.helper.GenerateGetOnlyCollectionReader(collectionContract);
			}
			finally
			{
				if (TD.DCJsonGenReaderStopIsEnabled())
				{
					TD.DCJsonGenReaderStop();
				}
			}
			return result;
		}

		// Token: 0x0400097D RID: 2429
		[SecurityCritical]
		private JsonFormatReaderGenerator.CriticalHelper helper;

		// Token: 0x0200016F RID: 367
		private class CriticalHelper
		{
			// Token: 0x06001267 RID: 4711 RVA: 0x000424AC File Offset: 0x000406AC
			internal JsonFormatClassReaderDelegate GenerateClassReader(ClassDataContract classContract)
			{
				return (XmlReaderDelegator xr, XmlObjectSerializerReadContextComplexJson ctx, XmlDictionaryString emptyDictionaryString, XmlDictionaryString[] memberNames) => new JsonFormatReaderInterpreter(classContract).ReadFromJson(xr, ctx, emptyDictionaryString, memberNames);
			}

			// Token: 0x06001268 RID: 4712 RVA: 0x000424C5 File Offset: 0x000406C5
			internal JsonFormatCollectionReaderDelegate GenerateCollectionReader(CollectionDataContract collectionContract)
			{
				return (XmlReaderDelegator xr, XmlObjectSerializerReadContextComplexJson ctx, XmlDictionaryString emptyDS, XmlDictionaryString inm, CollectionDataContract cc) => new JsonFormatReaderInterpreter(collectionContract, false).ReadCollectionFromJson(xr, ctx, emptyDS, inm, cc);
			}

			// Token: 0x06001269 RID: 4713 RVA: 0x000424DE File Offset: 0x000406DE
			internal JsonFormatGetOnlyCollectionReaderDelegate GenerateGetOnlyCollectionReader(CollectionDataContract collectionContract)
			{
				return delegate(XmlReaderDelegator xr, XmlObjectSerializerReadContextComplexJson ctx, XmlDictionaryString emptyDS, XmlDictionaryString inm, CollectionDataContract cc)
				{
					new JsonFormatReaderInterpreter(collectionContract, true).ReadGetOnlyCollectionFromJson(xr, ctx, emptyDS, inm, cc);
				};
			}

			// Token: 0x0600126A RID: 4714 RVA: 0x00002217 File Offset: 0x00000417
			public CriticalHelper()
			{
			}

			// Token: 0x02000170 RID: 368
			[CompilerGenerated]
			private sealed class <>c__DisplayClass0_0
			{
				// Token: 0x0600126B RID: 4715 RVA: 0x00002217 File Offset: 0x00000417
				public <>c__DisplayClass0_0()
				{
				}

				// Token: 0x0600126C RID: 4716 RVA: 0x000424F7 File Offset: 0x000406F7
				internal object <GenerateClassReader>b__0(XmlReaderDelegator xr, XmlObjectSerializerReadContextComplexJson ctx, XmlDictionaryString emptyDictionaryString, XmlDictionaryString[] memberNames)
				{
					return new JsonFormatReaderInterpreter(this.classContract).ReadFromJson(xr, ctx, emptyDictionaryString, memberNames);
				}

				// Token: 0x0400097E RID: 2430
				public ClassDataContract classContract;
			}

			// Token: 0x02000171 RID: 369
			[CompilerGenerated]
			private sealed class <>c__DisplayClass1_0
			{
				// Token: 0x0600126D RID: 4717 RVA: 0x00002217 File Offset: 0x00000417
				public <>c__DisplayClass1_0()
				{
				}

				// Token: 0x0600126E RID: 4718 RVA: 0x0004250E File Offset: 0x0004070E
				internal object <GenerateCollectionReader>b__0(XmlReaderDelegator xr, XmlObjectSerializerReadContextComplexJson ctx, XmlDictionaryString emptyDS, XmlDictionaryString inm, CollectionDataContract cc)
				{
					return new JsonFormatReaderInterpreter(this.collectionContract, false).ReadCollectionFromJson(xr, ctx, emptyDS, inm, cc);
				}

				// Token: 0x0400097F RID: 2431
				public CollectionDataContract collectionContract;
			}

			// Token: 0x02000172 RID: 370
			[CompilerGenerated]
			private sealed class <>c__DisplayClass2_0
			{
				// Token: 0x0600126F RID: 4719 RVA: 0x00002217 File Offset: 0x00000417
				public <>c__DisplayClass2_0()
				{
				}

				// Token: 0x06001270 RID: 4720 RVA: 0x00042528 File Offset: 0x00040728
				internal void <GenerateGetOnlyCollectionReader>b__0(XmlReaderDelegator xr, XmlObjectSerializerReadContextComplexJson ctx, XmlDictionaryString emptyDS, XmlDictionaryString inm, CollectionDataContract cc)
				{
					new JsonFormatReaderInterpreter(this.collectionContract, true).ReadGetOnlyCollectionFromJson(xr, ctx, emptyDS, inm, cc);
				}

				// Token: 0x04000980 RID: 2432
				public CollectionDataContract collectionContract;
			}
		}
	}
}
