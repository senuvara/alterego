﻿using System;
using System.Security;
using System.Threading;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000163 RID: 355
	internal class JsonCollectionDataContract : JsonDataContract
	{
		// Token: 0x06001208 RID: 4616 RVA: 0x000410A1 File Offset: 0x0003F2A1
		[SecuritySafeCritical]
		public JsonCollectionDataContract(CollectionDataContract traditionalDataContract) : base(new JsonCollectionDataContract.JsonCollectionDataContractCriticalHelper(traditionalDataContract))
		{
			this.helper = (base.Helper as JsonCollectionDataContract.JsonCollectionDataContractCriticalHelper);
		}

		// Token: 0x170003E4 RID: 996
		// (get) Token: 0x06001209 RID: 4617 RVA: 0x000410C0 File Offset: 0x0003F2C0
		internal JsonFormatCollectionReaderDelegate JsonFormatReaderDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.JsonFormatReaderDelegate == null)
				{
					lock (this)
					{
						if (this.helper.JsonFormatReaderDelegate == null)
						{
							if (this.TraditionalCollectionDataContract.IsReadOnlyContract)
							{
								DataContract.ThrowInvalidDataContractException(this.TraditionalCollectionDataContract.DeserializationExceptionMessage, null);
							}
							JsonFormatCollectionReaderDelegate jsonFormatReaderDelegate = new JsonFormatReaderGenerator().GenerateCollectionReader(this.TraditionalCollectionDataContract);
							Thread.MemoryBarrier();
							this.helper.JsonFormatReaderDelegate = jsonFormatReaderDelegate;
						}
					}
				}
				return this.helper.JsonFormatReaderDelegate;
			}
		}

		// Token: 0x170003E5 RID: 997
		// (get) Token: 0x0600120A RID: 4618 RVA: 0x0004115C File Offset: 0x0003F35C
		internal JsonFormatGetOnlyCollectionReaderDelegate JsonFormatGetOnlyReaderDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.JsonFormatGetOnlyReaderDelegate == null)
				{
					lock (this)
					{
						if (this.helper.JsonFormatGetOnlyReaderDelegate == null)
						{
							CollectionKind kind = this.TraditionalCollectionDataContract.Kind;
							if (base.TraditionalDataContract.UnderlyingType.IsInterface && (kind == CollectionKind.Enumerable || kind == CollectionKind.Collection || kind == CollectionKind.GenericEnumerable))
							{
								throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("On type '{0}', get-only collection must have an Add method.", new object[]
								{
									DataContract.GetClrTypeFullName(base.TraditionalDataContract.UnderlyingType)
								})));
							}
							if (this.TraditionalCollectionDataContract.IsReadOnlyContract)
							{
								DataContract.ThrowInvalidDataContractException(this.TraditionalCollectionDataContract.DeserializationExceptionMessage, null);
							}
							JsonFormatGetOnlyCollectionReaderDelegate jsonFormatGetOnlyReaderDelegate = new JsonFormatReaderGenerator().GenerateGetOnlyCollectionReader(this.TraditionalCollectionDataContract);
							Thread.MemoryBarrier();
							this.helper.JsonFormatGetOnlyReaderDelegate = jsonFormatGetOnlyReaderDelegate;
						}
					}
				}
				return this.helper.JsonFormatGetOnlyReaderDelegate;
			}
		}

		// Token: 0x170003E6 RID: 998
		// (get) Token: 0x0600120B RID: 4619 RVA: 0x00041254 File Offset: 0x0003F454
		internal JsonFormatCollectionWriterDelegate JsonFormatWriterDelegate
		{
			[SecuritySafeCritical]
			get
			{
				if (this.helper.JsonFormatWriterDelegate == null)
				{
					lock (this)
					{
						if (this.helper.JsonFormatWriterDelegate == null)
						{
							JsonFormatCollectionWriterDelegate jsonFormatWriterDelegate = new JsonFormatWriterGenerator().GenerateCollectionWriter(this.TraditionalCollectionDataContract);
							Thread.MemoryBarrier();
							this.helper.JsonFormatWriterDelegate = jsonFormatWriterDelegate;
						}
					}
				}
				return this.helper.JsonFormatWriterDelegate;
			}
		}

		// Token: 0x170003E7 RID: 999
		// (get) Token: 0x0600120C RID: 4620 RVA: 0x000412D0 File Offset: 0x0003F4D0
		private CollectionDataContract TraditionalCollectionDataContract
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.TraditionalCollectionDataContract;
			}
		}

		// Token: 0x0600120D RID: 4621 RVA: 0x000412E0 File Offset: 0x0003F4E0
		public override object ReadJsonValueCore(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			jsonReader.Read();
			object result = null;
			if (context.IsGetOnlyCollection)
			{
				context.IsGetOnlyCollection = false;
				this.JsonFormatGetOnlyReaderDelegate(jsonReader, context, XmlDictionaryString.Empty, JsonGlobals.itemDictionaryString, this.TraditionalCollectionDataContract);
			}
			else
			{
				result = this.JsonFormatReaderDelegate(jsonReader, context, XmlDictionaryString.Empty, JsonGlobals.itemDictionaryString, this.TraditionalCollectionDataContract);
			}
			jsonReader.ReadEndElement();
			return result;
		}

		// Token: 0x0600120E RID: 4622 RVA: 0x00041349 File Offset: 0x0003F549
		public override void WriteJsonValueCore(XmlWriterDelegator jsonWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, RuntimeTypeHandle declaredTypeHandle)
		{
			context.IsGetOnlyCollection = false;
			this.JsonFormatWriterDelegate(jsonWriter, obj, context, this.TraditionalCollectionDataContract);
		}

		// Token: 0x04000955 RID: 2389
		[SecurityCritical]
		private JsonCollectionDataContract.JsonCollectionDataContractCriticalHelper helper;

		// Token: 0x02000164 RID: 356
		private class JsonCollectionDataContractCriticalHelper : JsonDataContract.JsonDataContractCriticalHelper
		{
			// Token: 0x0600120F RID: 4623 RVA: 0x00041366 File Offset: 0x0003F566
			public JsonCollectionDataContractCriticalHelper(CollectionDataContract traditionalDataContract) : base(traditionalDataContract)
			{
				this.traditionalCollectionDataContract = traditionalDataContract;
			}

			// Token: 0x170003E8 RID: 1000
			// (get) Token: 0x06001210 RID: 4624 RVA: 0x00041376 File Offset: 0x0003F576
			// (set) Token: 0x06001211 RID: 4625 RVA: 0x0004137E File Offset: 0x0003F57E
			internal JsonFormatCollectionReaderDelegate JsonFormatReaderDelegate
			{
				get
				{
					return this.jsonFormatReaderDelegate;
				}
				set
				{
					this.jsonFormatReaderDelegate = value;
				}
			}

			// Token: 0x170003E9 RID: 1001
			// (get) Token: 0x06001212 RID: 4626 RVA: 0x00041387 File Offset: 0x0003F587
			// (set) Token: 0x06001213 RID: 4627 RVA: 0x0004138F File Offset: 0x0003F58F
			internal JsonFormatGetOnlyCollectionReaderDelegate JsonFormatGetOnlyReaderDelegate
			{
				get
				{
					return this.jsonFormatGetOnlyReaderDelegate;
				}
				set
				{
					this.jsonFormatGetOnlyReaderDelegate = value;
				}
			}

			// Token: 0x170003EA RID: 1002
			// (get) Token: 0x06001214 RID: 4628 RVA: 0x00041398 File Offset: 0x0003F598
			// (set) Token: 0x06001215 RID: 4629 RVA: 0x000413A0 File Offset: 0x0003F5A0
			internal JsonFormatCollectionWriterDelegate JsonFormatWriterDelegate
			{
				get
				{
					return this.jsonFormatWriterDelegate;
				}
				set
				{
					this.jsonFormatWriterDelegate = value;
				}
			}

			// Token: 0x170003EB RID: 1003
			// (get) Token: 0x06001216 RID: 4630 RVA: 0x000413A9 File Offset: 0x0003F5A9
			internal CollectionDataContract TraditionalCollectionDataContract
			{
				get
				{
					return this.traditionalCollectionDataContract;
				}
			}

			// Token: 0x04000956 RID: 2390
			private JsonFormatCollectionReaderDelegate jsonFormatReaderDelegate;

			// Token: 0x04000957 RID: 2391
			private JsonFormatGetOnlyCollectionReaderDelegate jsonFormatGetOnlyReaderDelegate;

			// Token: 0x04000958 RID: 2392
			private JsonFormatCollectionWriterDelegate jsonFormatWriterDelegate;

			// Token: 0x04000959 RID: 2393
			private CollectionDataContract traditionalCollectionDataContract;
		}
	}
}
