﻿using System;
using System.Globalization;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000182 RID: 386
	internal class JsonWriterDelegator : XmlWriterDelegator
	{
		// Token: 0x060012AD RID: 4781 RVA: 0x0004309D File Offset: 0x0004129D
		public JsonWriterDelegator(XmlWriter writer) : base(writer)
		{
		}

		// Token: 0x060012AE RID: 4782 RVA: 0x000430A6 File Offset: 0x000412A6
		public JsonWriterDelegator(XmlWriter writer, DateTimeFormat dateTimeFormat) : this(writer)
		{
			this.dateTimeFormat = dateTimeFormat;
		}

		// Token: 0x060012AF RID: 4783 RVA: 0x000430B6 File Offset: 0x000412B6
		internal override void WriteChar(char value)
		{
			base.WriteString(XmlConvert.ToString(value));
		}

		// Token: 0x060012B0 RID: 4784 RVA: 0x000430C4 File Offset: 0x000412C4
		internal override void WriteBase64(byte[] bytes)
		{
			if (bytes == null)
			{
				return;
			}
			ByteArrayHelperWithString.Instance.WriteArray(base.Writer, bytes, 0, bytes.Length);
		}

		// Token: 0x060012B1 RID: 4785 RVA: 0x000430E0 File Offset: 0x000412E0
		internal override void WriteQName(XmlQualifiedName value)
		{
			if (value != XmlQualifiedName.Empty)
			{
				this.writer.WriteString(value.Name);
				this.writer.WriteString(":");
				this.writer.WriteString(value.Namespace);
			}
		}

		// Token: 0x060012B2 RID: 4786 RVA: 0x0004312C File Offset: 0x0004132C
		internal override void WriteUnsignedLong(ulong value)
		{
			this.WriteDecimal(value);
		}

		// Token: 0x060012B3 RID: 4787 RVA: 0x0004313A File Offset: 0x0004133A
		internal override void WriteDecimal(decimal value)
		{
			this.writer.WriteAttributeString("type", "number");
			base.WriteDecimal(value);
		}

		// Token: 0x060012B4 RID: 4788 RVA: 0x00043158 File Offset: 0x00041358
		internal override void WriteDouble(double value)
		{
			this.writer.WriteAttributeString("type", "number");
			base.WriteDouble(value);
		}

		// Token: 0x060012B5 RID: 4789 RVA: 0x00043176 File Offset: 0x00041376
		internal override void WriteFloat(float value)
		{
			this.writer.WriteAttributeString("type", "number");
			base.WriteFloat(value);
		}

		// Token: 0x060012B6 RID: 4790 RVA: 0x00043194 File Offset: 0x00041394
		internal override void WriteLong(long value)
		{
			this.writer.WriteAttributeString("type", "number");
			base.WriteLong(value);
		}

		// Token: 0x060012B7 RID: 4791 RVA: 0x000431B2 File Offset: 0x000413B2
		internal override void WriteSignedByte(sbyte value)
		{
			this.writer.WriteAttributeString("type", "number");
			base.WriteSignedByte(value);
		}

		// Token: 0x060012B8 RID: 4792 RVA: 0x000431D0 File Offset: 0x000413D0
		internal override void WriteUnsignedInt(uint value)
		{
			this.writer.WriteAttributeString("type", "number");
			base.WriteUnsignedInt(value);
		}

		// Token: 0x060012B9 RID: 4793 RVA: 0x000431EE File Offset: 0x000413EE
		internal override void WriteUnsignedShort(ushort value)
		{
			this.writer.WriteAttributeString("type", "number");
			base.WriteUnsignedShort(value);
		}

		// Token: 0x060012BA RID: 4794 RVA: 0x0004320C File Offset: 0x0004140C
		internal override void WriteUnsignedByte(byte value)
		{
			this.writer.WriteAttributeString("type", "number");
			base.WriteUnsignedByte(value);
		}

		// Token: 0x060012BB RID: 4795 RVA: 0x0004322A File Offset: 0x0004142A
		internal override void WriteShort(short value)
		{
			this.writer.WriteAttributeString("type", "number");
			base.WriteShort(value);
		}

		// Token: 0x060012BC RID: 4796 RVA: 0x00043248 File Offset: 0x00041448
		internal override void WriteBoolean(bool value)
		{
			this.writer.WriteAttributeString("type", "boolean");
			base.WriteBoolean(value);
		}

		// Token: 0x060012BD RID: 4797 RVA: 0x00043266 File Offset: 0x00041466
		internal override void WriteInt(int value)
		{
			this.writer.WriteAttributeString("type", "number");
			base.WriteInt(value);
		}

		// Token: 0x060012BE RID: 4798 RVA: 0x00043284 File Offset: 0x00041484
		internal void WriteJsonBooleanArray(bool[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			for (int i = 0; i < value.Length; i++)
			{
				base.WriteBoolean(value[i], itemName, itemNamespace);
			}
		}

		// Token: 0x060012BF RID: 4799 RVA: 0x000432AC File Offset: 0x000414AC
		internal void WriteJsonDateTimeArray(DateTime[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			for (int i = 0; i < value.Length; i++)
			{
				base.WriteDateTime(value[i], itemName, itemNamespace);
			}
		}

		// Token: 0x060012C0 RID: 4800 RVA: 0x000432D8 File Offset: 0x000414D8
		internal void WriteJsonDecimalArray(decimal[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			for (int i = 0; i < value.Length; i++)
			{
				base.WriteDecimal(value[i], itemName, itemNamespace);
			}
		}

		// Token: 0x060012C1 RID: 4801 RVA: 0x00043304 File Offset: 0x00041504
		internal void WriteJsonInt32Array(int[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			for (int i = 0; i < value.Length; i++)
			{
				base.WriteInt(value[i], itemName, itemNamespace);
			}
		}

		// Token: 0x060012C2 RID: 4802 RVA: 0x0004332C File Offset: 0x0004152C
		internal void WriteJsonInt64Array(long[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			for (int i = 0; i < value.Length; i++)
			{
				base.WriteLong(value[i], itemName, itemNamespace);
			}
		}

		// Token: 0x060012C3 RID: 4803 RVA: 0x00043352 File Offset: 0x00041552
		internal override void WriteDateTime(DateTime value)
		{
			if (this.dateTimeFormat == null)
			{
				this.WriteDateTimeInDefaultFormat(value);
				return;
			}
			this.writer.WriteString(value.ToString(this.dateTimeFormat.FormatString, this.dateTimeFormat.FormatProvider));
		}

		// Token: 0x060012C4 RID: 4804 RVA: 0x0004338C File Offset: 0x0004158C
		private void WriteDateTimeInDefaultFormat(DateTime value)
		{
			if (value.Kind != DateTimeKind.Utc)
			{
				long num;
				if (!LocalAppContextSwitches.DoNotUseTimeZoneInfo)
				{
					num = value.Ticks - TimeZoneInfo.Local.GetUtcOffset(value).Ticks;
				}
				else
				{
					num = value.Ticks - TimeZone.CurrentTimeZone.GetUtcOffset(value).Ticks;
				}
				if (num > DateTime.MaxValue.Ticks || num < DateTime.MinValue.Ticks)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("JSON DateTime is out of range."), new ArgumentOutOfRangeException("value")));
				}
			}
			this.writer.WriteString("/Date(");
			this.writer.WriteValue((value.ToUniversalTime().Ticks - JsonGlobals.unixEpochTicks) / 10000L);
			switch (value.Kind)
			{
			case DateTimeKind.Unspecified:
			case DateTimeKind.Local:
			{
				TimeSpan utcOffset;
				if (!LocalAppContextSwitches.DoNotUseTimeZoneInfo)
				{
					utcOffset = TimeZoneInfo.Local.GetUtcOffset(value.ToLocalTime());
				}
				else
				{
					utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(value.ToLocalTime());
				}
				if (utcOffset.Ticks < 0L)
				{
					this.writer.WriteString("-");
				}
				else
				{
					this.writer.WriteString("+");
				}
				int num2 = Math.Abs(utcOffset.Hours);
				this.writer.WriteString((num2 < 10) ? ("0" + num2) : num2.ToString(CultureInfo.InvariantCulture));
				int num3 = Math.Abs(utcOffset.Minutes);
				this.writer.WriteString((num3 < 10) ? ("0" + num3) : num3.ToString(CultureInfo.InvariantCulture));
				break;
			}
			}
			this.writer.WriteString(")/");
		}

		// Token: 0x060012C5 RID: 4805 RVA: 0x00043564 File Offset: 0x00041764
		internal void WriteJsonSingleArray(float[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			for (int i = 0; i < value.Length; i++)
			{
				base.WriteFloat(value[i], itemName, itemNamespace);
			}
		}

		// Token: 0x060012C6 RID: 4806 RVA: 0x0004358C File Offset: 0x0004178C
		internal void WriteJsonDoubleArray(double[] value, XmlDictionaryString itemName, XmlDictionaryString itemNamespace)
		{
			for (int i = 0; i < value.Length; i++)
			{
				base.WriteDouble(value[i], itemName, itemNamespace);
			}
		}

		// Token: 0x060012C7 RID: 4807 RVA: 0x000435B2 File Offset: 0x000417B2
		internal override void WriteStartElement(string prefix, string localName, string ns)
		{
			if (localName != null && localName.Length == 0)
			{
				base.WriteStartElement("item", "item");
				base.WriteAttributeString(null, "item", null, localName);
				return;
			}
			base.WriteStartElement(prefix, localName, ns);
		}

		// Token: 0x040009C3 RID: 2499
		private DateTimeFormat dateTimeFormat;
	}
}
