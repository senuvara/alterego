﻿using System;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000173 RID: 371
	// (Invoke) Token: 0x06001272 RID: 4722
	internal delegate void JsonFormatClassWriterDelegate(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, ClassDataContract dataContract, XmlDictionaryString[] memberNames);
}
