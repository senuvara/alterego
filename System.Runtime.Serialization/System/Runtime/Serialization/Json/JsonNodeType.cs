﻿using System;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200017A RID: 378
	internal enum JsonNodeType
	{
		// Token: 0x040009B8 RID: 2488
		None,
		// Token: 0x040009B9 RID: 2489
		Object,
		// Token: 0x040009BA RID: 2490
		Element,
		// Token: 0x040009BB RID: 2491
		EndElement,
		// Token: 0x040009BC RID: 2492
		QuotedText,
		// Token: 0x040009BD RID: 2493
		StandaloneText,
		// Token: 0x040009BE RID: 2494
		Collection
	}
}
