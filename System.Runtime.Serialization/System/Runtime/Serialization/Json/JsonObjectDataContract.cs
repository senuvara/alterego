﻿using System;
using System.Globalization;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200017B RID: 379
	internal class JsonObjectDataContract : JsonDataContract
	{
		// Token: 0x06001284 RID: 4740 RVA: 0x00040D44 File Offset: 0x0003EF44
		public JsonObjectDataContract(DataContract traditionalDataContract) : base(traditionalDataContract)
		{
		}

		// Token: 0x06001285 RID: 4741 RVA: 0x000426F8 File Offset: 0x000408F8
		public override object ReadJsonValueCore(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			string attribute = jsonReader.GetAttribute("type");
			uint num = <PrivateImplementationDetails>.ComputeStringHash(attribute);
			object obj;
			if (num <= 467038368U)
			{
				if (num != 0U)
				{
					if (num != 398550328U)
					{
						if (num != 467038368U)
						{
							goto IL_11C;
						}
						if (!(attribute == "number"))
						{
							goto IL_11C;
						}
						obj = JsonObjectDataContract.ParseJsonNumber(jsonReader.ReadElementContentAsString());
						goto IL_13B;
					}
					else if (!(attribute == "string"))
					{
						goto IL_11C;
					}
				}
				else if (attribute != null)
				{
					goto IL_11C;
				}
				obj = jsonReader.ReadElementContentAsString();
				goto IL_13B;
			}
			if (num <= 1996966820U)
			{
				if (num != 1710517951U)
				{
					if (num == 1996966820U)
					{
						if (attribute == "null")
						{
							jsonReader.Skip();
							obj = null;
							goto IL_13B;
						}
					}
				}
				else if (attribute == "boolean")
				{
					obj = jsonReader.ReadElementContentAsBoolean();
					goto IL_13B;
				}
			}
			else if (num != 2321067302U)
			{
				if (num == 3099987130U)
				{
					if (attribute == "object")
					{
						jsonReader.Skip();
						obj = new object();
						goto IL_13B;
					}
				}
			}
			else if (attribute == "array")
			{
				return DataContractJsonSerializer.ReadJsonValue(DataContract.GetDataContract(Globals.TypeOfObjectArray), jsonReader, context);
			}
			IL_11C:
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Unexpected attribute value '{0}'.", new object[]
			{
				attribute
			})));
			IL_13B:
			if (context != null)
			{
				context.AddNewObject(obj);
			}
			return obj;
		}

		// Token: 0x06001286 RID: 4742 RVA: 0x0004284B File Offset: 0x00040A4B
		public override void WriteJsonValueCore(XmlWriterDelegator jsonWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, RuntimeTypeHandle declaredTypeHandle)
		{
			jsonWriter.WriteAttributeString(null, "type", null, "object");
		}

		// Token: 0x06001287 RID: 4743 RVA: 0x00042860 File Offset: 0x00040A60
		internal static object ParseJsonNumber(string value, out TypeCode objectTypeCode)
		{
			if (value == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("The value '{0}' cannot be parsed as the type '{1}'.", new object[]
				{
					value,
					Globals.TypeOfInt
				})));
			}
			if (value.IndexOfAny(JsonGlobals.floatingPointCharacters) == -1)
			{
				int num;
				if (int.TryParse(value, NumberStyles.Float, NumberFormatInfo.InvariantInfo, out num))
				{
					objectTypeCode = TypeCode.Int32;
					return num;
				}
				long num2;
				if (long.TryParse(value, NumberStyles.Float, NumberFormatInfo.InvariantInfo, out num2))
				{
					objectTypeCode = TypeCode.Int64;
					return num2;
				}
			}
			decimal num3;
			if (decimal.TryParse(value, NumberStyles.Float, NumberFormatInfo.InvariantInfo, out num3))
			{
				objectTypeCode = TypeCode.Decimal;
				if (num3 == 0m)
				{
					double num4 = XmlConverter.ToDouble(value);
					if (num4 != 0.0)
					{
						objectTypeCode = TypeCode.Double;
						return num4;
					}
				}
				return num3;
			}
			objectTypeCode = TypeCode.Double;
			return XmlConverter.ToDouble(value);
		}

		// Token: 0x06001288 RID: 4744 RVA: 0x0004293C File Offset: 0x00040B3C
		private static object ParseJsonNumber(string value)
		{
			TypeCode typeCode;
			return JsonObjectDataContract.ParseJsonNumber(value, out typeCode);
		}
	}
}
