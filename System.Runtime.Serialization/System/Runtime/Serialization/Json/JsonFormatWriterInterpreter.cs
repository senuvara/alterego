﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200018F RID: 399
	internal class JsonFormatWriterInterpreter
	{
		// Token: 0x060013B8 RID: 5048 RVA: 0x00049079 File Offset: 0x00047279
		public JsonFormatWriterInterpreter(ClassDataContract classContract)
		{
			this.classContract = classContract;
		}

		// Token: 0x060013B9 RID: 5049 RVA: 0x0004908F File Offset: 0x0004728F
		public JsonFormatWriterInterpreter(CollectionDataContract collectionContract)
		{
			this.collectionContract = collectionContract;
		}

		// Token: 0x17000414 RID: 1044
		// (get) Token: 0x060013BA RID: 5050 RVA: 0x000490A5 File Offset: 0x000472A5
		private ClassDataContract classDataContract
		{
			get
			{
				return (ClassDataContract)this.dataContract;
			}
		}

		// Token: 0x17000415 RID: 1045
		// (get) Token: 0x060013BB RID: 5051 RVA: 0x000490B2 File Offset: 0x000472B2
		private CollectionDataContract collectionDataContract
		{
			get
			{
				return (CollectionDataContract)this.dataContract;
			}
		}

		// Token: 0x060013BC RID: 5052 RVA: 0x000490C0 File Offset: 0x000472C0
		public void WriteToJson(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, ClassDataContract dataContract, XmlDictionaryString[] memberNames)
		{
			this.writer = xmlWriter;
			this.obj = obj;
			this.context = context;
			this.dataContract = dataContract;
			this.memberNames = memberNames;
			this.InitArgs(this.classContract.UnderlyingType);
			if (this.classContract.IsReadOnlyContract)
			{
				DataContract.ThrowInvalidDataContractException(this.classContract.SerializationExceptionMessage, null);
			}
			this.WriteClass(this.classContract);
		}

		// Token: 0x060013BD RID: 5053 RVA: 0x00049130 File Offset: 0x00047330
		public void WriteCollectionToJson(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, CollectionDataContract dataContract)
		{
			this.writer = xmlWriter;
			this.obj = obj;
			this.context = context;
			this.dataContract = dataContract;
			this.InitArgs(this.collectionContract.UnderlyingType);
			if (this.collectionContract.IsReadOnlyContract)
			{
				DataContract.ThrowInvalidDataContractException(this.collectionContract.SerializationExceptionMessage, null);
			}
			this.WriteCollection(this.collectionContract);
		}

		// Token: 0x060013BE RID: 5054 RVA: 0x00049198 File Offset: 0x00047398
		private void InitArgs(Type objType)
		{
			if (objType == Globals.TypeOfDateTimeOffsetAdapter)
			{
				this.objLocal = DateTimeOffsetAdapter.GetDateTimeOffsetAdapter((DateTimeOffset)this.obj);
				return;
			}
			this.objLocal = CodeInterpreter.ConvertValue(this.obj, typeof(object), objType);
		}

		// Token: 0x060013BF RID: 5055 RVA: 0x000491EC File Offset: 0x000473EC
		private void InvokeOnSerializing(ClassDataContract classContract, object objSerialized, XmlObjectSerializerWriteContext context)
		{
			if (classContract.BaseContract != null)
			{
				this.InvokeOnSerializing(classContract.BaseContract, objSerialized, context);
			}
			if (classContract.OnSerializing != null)
			{
				classContract.OnSerializing.Invoke(objSerialized, new object[]
				{
					context.GetStreamingContext()
				});
			}
		}

		// Token: 0x060013C0 RID: 5056 RVA: 0x00049240 File Offset: 0x00047440
		private void InvokeOnSerialized(ClassDataContract classContract, object objSerialized, XmlObjectSerializerWriteContext context)
		{
			if (classContract.BaseContract != null)
			{
				this.InvokeOnSerialized(classContract.BaseContract, objSerialized, context);
			}
			if (classContract.OnSerialized != null)
			{
				classContract.OnSerialized.Invoke(objSerialized, new object[]
				{
					context.GetStreamingContext()
				});
			}
		}

		// Token: 0x060013C1 RID: 5057 RVA: 0x00049294 File Offset: 0x00047494
		private void WriteClass(ClassDataContract classContract)
		{
			this.InvokeOnSerializing(classContract, this.objLocal, this.context);
			if (classContract.IsISerializable)
			{
				this.context.WriteJsonISerializable(this.writer, (ISerializable)this.objLocal);
			}
			else if (classContract.HasExtensionData)
			{
				ExtensionDataObject extensionData = ((IExtensibleDataObject)this.objLocal).ExtensionData;
				this.context.WriteExtensionData(this.writer, extensionData, -1);
				this.WriteMembers(classContract, extensionData, classContract);
			}
			else
			{
				this.WriteMembers(classContract, null, classContract);
			}
			this.InvokeOnSerialized(classContract, this.objLocal, this.context);
		}

		// Token: 0x060013C2 RID: 5058 RVA: 0x00049330 File Offset: 0x00047530
		private void WriteCollection(CollectionDataContract collectionContract)
		{
			XmlDictionaryString collectionItemName = this.context.CollectionItemName;
			if (collectionContract.Kind == CollectionKind.Array)
			{
				Type itemType = collectionContract.ItemType;
				if (this.objLocal.GetType().GetElementType() != itemType)
				{
					throw new InvalidCastException(string.Format("Cannot cast array of {0} to array of {1}", this.objLocal.GetType().GetElementType(), itemType));
				}
				this.context.IncrementArrayCount(this.writer, (Array)this.objLocal);
				if (!this.TryWritePrimitiveArray(collectionContract.UnderlyingType, itemType, () => this.objLocal, collectionItemName))
				{
					this.WriteArrayAttribute();
					Array array = (Array)this.objLocal;
					int[] array2 = new int[1];
					for (int i = 0; i < array.Length; i++)
					{
						if (!this.TryWritePrimitive(itemType, null, null, new int?(i), collectionItemName, 0))
						{
							this.WriteStartElement(collectionItemName, 0);
							array2[0] = i;
							object value = array.GetValue(array2);
							this.WriteValue(itemType, value);
							this.WriteEndElement();
						}
					}
					return;
				}
			}
			else
			{
				if (!collectionContract.UnderlyingType.IsAssignableFrom(this.objLocal.GetType()))
				{
					throw new InvalidCastException(string.Format("Cannot cast {0} to {1}", this.objLocal.GetType(), collectionContract.UnderlyingType));
				}
				MethodInfo methodInfo = null;
				switch (collectionContract.Kind)
				{
				case CollectionKind.GenericDictionary:
					methodInfo = XmlFormatGeneratorStatics.IncrementCollectionCountGenericMethod.MakeGenericMethod(new Type[]
					{
						Globals.TypeOfKeyValuePair.MakeGenericType(collectionContract.ItemType.GetGenericArguments())
					});
					break;
				case CollectionKind.Dictionary:
				case CollectionKind.List:
				case CollectionKind.Collection:
					methodInfo = XmlFormatGeneratorStatics.IncrementCollectionCountMethod;
					break;
				case CollectionKind.GenericList:
				case CollectionKind.GenericCollection:
					methodInfo = XmlFormatGeneratorStatics.IncrementCollectionCountGenericMethod.MakeGenericMethod(new Type[]
					{
						collectionContract.ItemType
					});
					break;
				}
				if (methodInfo != null)
				{
					methodInfo.Invoke(this.context, new object[]
					{
						this.writer,
						this.objLocal
					});
				}
				bool flag = false;
				bool flag2 = false;
				Type[] typeArguments = null;
				Type type;
				if (collectionContract.Kind == CollectionKind.GenericDictionary)
				{
					flag2 = true;
					typeArguments = collectionContract.ItemType.GetGenericArguments();
					type = Globals.TypeOfGenericDictionaryEnumerator.MakeGenericType(typeArguments);
				}
				else if (collectionContract.Kind == CollectionKind.Dictionary)
				{
					flag = true;
					typeArguments = new Type[]
					{
						Globals.TypeOfObject,
						Globals.TypeOfObject
					};
					type = Globals.TypeOfDictionaryEnumerator;
				}
				else
				{
					type = collectionContract.GetEnumeratorMethod.ReturnType;
				}
				MethodInfo methodInfo2 = type.GetMethod("MoveNext", BindingFlags.Instance | BindingFlags.Public, null, Globals.EmptyTypeArray, null);
				MethodInfo methodInfo3 = type.GetMethod("get_Current", BindingFlags.Instance | BindingFlags.Public, null, Globals.EmptyTypeArray, null);
				if (methodInfo2 == null || methodInfo3 == null)
				{
					if (type.IsInterface)
					{
						if (methodInfo2 == null)
						{
							methodInfo2 = JsonFormatGeneratorStatics.MoveNextMethod;
						}
						if (methodInfo3 == null)
						{
							methodInfo3 = JsonFormatGeneratorStatics.GetCurrentMethod;
						}
					}
					else
					{
						Type interfaceType = Globals.TypeOfIEnumerator;
						CollectionKind kind = collectionContract.Kind;
						if (kind == CollectionKind.GenericDictionary || kind == CollectionKind.GenericCollection || kind == CollectionKind.GenericEnumerable)
						{
							foreach (Type type2 in type.GetInterfaces())
							{
								if (type2.IsGenericType && type2.GetGenericTypeDefinition() == Globals.TypeOfIEnumeratorGeneric && type2.GetGenericArguments()[0] == collectionContract.ItemType)
								{
									interfaceType = type2;
									break;
								}
							}
						}
						if (methodInfo2 == null)
						{
							methodInfo2 = CollectionDataContract.GetTargetMethodWithName("MoveNext", type, interfaceType);
						}
						if (methodInfo3 == null)
						{
							methodInfo3 = CollectionDataContract.GetTargetMethodWithName("get_Current", type, interfaceType);
						}
					}
				}
				Type returnType = methodInfo3.ReturnType;
				object currentValue = null;
				IEnumerator enumerator = (IEnumerator)collectionContract.GetEnumeratorMethod.Invoke(this.objLocal, new object[0]);
				if (flag)
				{
					enumerator = (IEnumerator)type.GetConstructor(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						Globals.TypeOfIDictionaryEnumerator
					}, null).Invoke(new object[]
					{
						enumerator
					});
				}
				else if (flag2)
				{
					Type type3 = Globals.TypeOfIEnumeratorGeneric.MakeGenericType(new Type[]
					{
						Globals.TypeOfKeyValuePair.MakeGenericType(typeArguments)
					});
					type.GetConstructor(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
					{
						type3
					}, null);
					enumerator = (IEnumerator)Activator.CreateInstance(type, new object[]
					{
						enumerator
					});
				}
				bool flag3 = flag || flag2;
				bool flag4 = flag3 && this.context.UseSimpleDictionaryFormat;
				PropertyInfo memberInfo = null;
				PropertyInfo propertyInfo = null;
				if (flag3)
				{
					Type type4 = Globals.TypeOfKeyValue.MakeGenericType(typeArguments);
					memberInfo = type4.GetProperty("Key");
					propertyInfo = type4.GetProperty("Value");
				}
				if (flag4)
				{
					this.WriteObjectAttribute();
					object[] parameters = new object[0];
					while ((bool)methodInfo2.Invoke(enumerator, parameters))
					{
						currentValue = methodInfo3.Invoke(enumerator, parameters);
						object member = CodeInterpreter.GetMember(memberInfo, currentValue);
						object member2 = CodeInterpreter.GetMember(propertyInfo, currentValue);
						this.WriteStartElement(member, 0);
						this.WriteValue(propertyInfo.PropertyType, member2);
						this.WriteEndElement();
					}
					return;
				}
				this.WriteArrayAttribute();
				object[] parameters2 = new object[0];
				Func<object> <>9__1;
				while (enumerator != null && enumerator.MoveNext())
				{
					currentValue = methodInfo3.Invoke(enumerator, parameters2);
					if (methodInfo == null)
					{
						XmlFormatGeneratorStatics.IncrementItemCountMethod.Invoke(this.context, new object[]
						{
							1
						});
					}
					Type type5 = returnType;
					Func<object> value2;
					if ((value2 = <>9__1) == null)
					{
						value2 = (<>9__1 = (() => currentValue));
					}
					if (!this.TryWritePrimitive(type5, value2, null, null, collectionItemName, 0))
					{
						this.WriteStartElement(collectionItemName, 0);
						if (flag2 || flag)
						{
							DataContractJsonSerializer.WriteJsonValue(JsonDataContract.GetJsonDataContract(XmlObjectSerializerWriteContextComplexJson.GetRevisedItemContract(this.collectionDataContract.ItemContract)), this.writer, currentValue, this.context, currentValue.GetType().TypeHandle);
						}
						else
						{
							this.WriteValue(returnType, currentValue);
						}
						this.WriteEndElement();
					}
				}
			}
		}

		// Token: 0x060013C3 RID: 5059 RVA: 0x00049930 File Offset: 0x00047B30
		private int WriteMembers(ClassDataContract classContract, ExtensionDataObject extensionData, ClassDataContract derivedMostClassContract)
		{
			int num = (classContract.BaseContract == null) ? 0 : this.WriteMembers(classContract.BaseContract, extensionData, derivedMostClassContract);
			this.context.IncrementItemCount(classContract.Members.Count);
			int i = 0;
			while (i < classContract.Members.Count)
			{
				DataMember dataMember = classContract.Members[i];
				Type memberType = dataMember.MemberType;
				object memberValue = null;
				if (dataMember.IsGetOnlyCollection)
				{
					this.context.StoreIsGetOnlyCollection();
				}
				bool flag = true;
				bool flag2 = false;
				if (!dataMember.EmitDefaultValue)
				{
					flag2 = true;
					memberValue = this.LoadMemberValue(dataMember);
					flag = !this.IsDefaultValue(memberType, memberValue);
				}
				if (flag)
				{
					bool flag3 = DataContractJsonSerializer.CheckIfXmlNameRequiresMapping(classContract.MemberNames[i]);
					if (flag3 || !this.TryWritePrimitive(memberType, flag2 ? (() => memberValue) : null, dataMember.MemberInfo, null, null, i + this.childElementIndex))
					{
						if (flag3)
						{
							XmlObjectSerializerWriteContextComplexJson.WriteJsonNameWithMapping(this.writer, this.memberNames, i + this.childElementIndex);
						}
						else
						{
							this.WriteStartElement(null, i + this.childElementIndex);
						}
						if (memberValue == null)
						{
							memberValue = this.LoadMemberValue(dataMember);
						}
						this.WriteValue(memberType, memberValue);
						this.WriteEndElement();
					}
					if (classContract.HasExtensionData)
					{
						this.context.WriteExtensionData(this.writer, extensionData, num);
					}
				}
				else if (!dataMember.EmitDefaultValue && dataMember.IsRequired)
				{
					XmlObjectSerializerWriteContext.ThrowRequiredMemberMustBeEmitted(dataMember.Name, classContract.UnderlyingType);
				}
				i++;
				num++;
			}
			this.typeIndex++;
			this.childElementIndex += classContract.Members.Count;
			return num;
		}

		// Token: 0x060013C4 RID: 5060 RVA: 0x00049B00 File Offset: 0x00047D00
		internal bool IsDefaultValue(Type type, object value)
		{
			object defaultValue = this.GetDefaultValue(type);
			if (defaultValue != null)
			{
				return defaultValue.Equals(value);
			}
			return value == null;
		}

		// Token: 0x060013C5 RID: 5061 RVA: 0x00049B24 File Offset: 0x00047D24
		internal object GetDefaultValue(Type type)
		{
			if (type.IsValueType)
			{
				switch (Type.GetTypeCode(type))
				{
				case TypeCode.Boolean:
					return false;
				case TypeCode.Char:
				case TypeCode.SByte:
				case TypeCode.Byte:
				case TypeCode.Int16:
				case TypeCode.UInt16:
				case TypeCode.Int32:
				case TypeCode.UInt32:
					return 0;
				case TypeCode.Int64:
				case TypeCode.UInt64:
					return 0L;
				case TypeCode.Single:
					return 0f;
				case TypeCode.Double:
					return 0.0;
				case TypeCode.Decimal:
					return 0m;
				case TypeCode.DateTime:
					return default(DateTime);
				}
			}
			return null;
		}

		// Token: 0x060013C6 RID: 5062 RVA: 0x00049BD0 File Offset: 0x00047DD0
		private void WriteStartElement(object nameLocal, int nameIndex)
		{
			object obj = nameLocal ?? this.memberNames[nameIndex];
			if (nameLocal != null && nameLocal is string)
			{
				this.writer.WriteStartElement((string)obj, null);
				return;
			}
			this.writer.WriteStartElement((XmlDictionaryString)obj, null);
		}

		// Token: 0x060013C7 RID: 5063 RVA: 0x00049C1B File Offset: 0x00047E1B
		private void WriteEndElement()
		{
			this.writer.WriteEndElement();
		}

		// Token: 0x060013C8 RID: 5064 RVA: 0x00049C28 File Offset: 0x00047E28
		private void WriteArrayAttribute()
		{
			this.writer.WriteAttributeString(null, "type", string.Empty, "array");
		}

		// Token: 0x060013C9 RID: 5065 RVA: 0x00049C45 File Offset: 0x00047E45
		private void WriteObjectAttribute()
		{
			this.writer.WriteAttributeString(null, "type", null, "object");
		}

		// Token: 0x060013CA RID: 5066 RVA: 0x00049C60 File Offset: 0x00047E60
		private void WriteValue(Type memberType, object memberValue)
		{
			if (memberType.IsPointer)
			{
				Pointer pointer = (Pointer)JsonFormatGeneratorStatics.BoxPointer.Invoke(null, new object[]
				{
					memberValue,
					memberType
				});
			}
			bool flag = memberType.IsGenericType && memberType.GetGenericTypeDefinition() == Globals.TypeOfNullable;
			if (memberType.IsValueType && !flag)
			{
				PrimitiveDataContract primitiveDataContract = PrimitiveDataContract.GetPrimitiveDataContract(memberType);
				if (primitiveDataContract != null)
				{
					primitiveDataContract.XmlFormatContentWriterMethod.Invoke(this.writer, new object[]
					{
						memberValue
					});
					return;
				}
				this.InternalSerialize(XmlFormatGeneratorStatics.InternalSerializeMethod, () => memberValue, memberType, false);
				return;
			}
			else
			{
				bool flag2;
				if (flag)
				{
					memberValue = this.UnwrapNullableObject(() => memberValue, ref memberType, out flag2);
				}
				else
				{
					flag2 = (memberValue == null);
				}
				if (flag2)
				{
					XmlFormatGeneratorStatics.WriteNullMethod.Invoke(this.context, new object[]
					{
						this.writer,
						memberType,
						DataContract.IsTypeSerializable(memberType)
					});
					return;
				}
				PrimitiveDataContract primitiveDataContract2 = PrimitiveDataContract.GetPrimitiveDataContract(memberType);
				if (primitiveDataContract2 != null && primitiveDataContract2.UnderlyingType != Globals.TypeOfObject)
				{
					if (flag)
					{
						primitiveDataContract2.XmlFormatContentWriterMethod.Invoke(this.writer, new object[]
						{
							memberValue
						});
						return;
					}
					primitiveDataContract2.XmlFormatContentWriterMethod.Invoke(this.context, new object[]
					{
						this.writer,
						memberValue
					});
					return;
				}
				else
				{
					bool flag3 = false;
					if (memberType == Globals.TypeOfObject || memberType == Globals.TypeOfValueType || ((IList)Globals.TypeOfNullable.GetInterfaces()).Contains(memberType))
					{
						object memberValue2 = CodeInterpreter.ConvertValue(memberValue, memberType.GetType(), Globals.TypeOfObject);
						memberValue = memberValue2;
						flag3 = (memberValue == null);
					}
					if (flag3)
					{
						XmlFormatGeneratorStatics.WriteNullMethod.Invoke(this.context, new object[]
						{
							this.writer,
							memberType,
							DataContract.IsTypeSerializable(memberType)
						});
						return;
					}
					this.InternalSerialize(flag ? XmlFormatGeneratorStatics.InternalSerializeMethod : XmlFormatGeneratorStatics.InternalSerializeReferenceMethod, () => memberValue, memberType, false);
					return;
				}
			}
		}

		// Token: 0x060013CB RID: 5067 RVA: 0x00049E9C File Offset: 0x0004809C
		private void InternalSerialize(MethodInfo methodInfo, Func<object> memberValue, Type memberType, bool writeXsiType)
		{
			object obj = memberValue();
			bool flag = Type.GetTypeHandle(obj).Equals(CodeInterpreter.ConvertValue(obj, memberType, Globals.TypeOfObject));
			try
			{
				methodInfo.Invoke(this.context, new object[]
				{
					this.writer,
					(memberValue != null) ? obj : null,
					flag,
					writeXsiType,
					DataContract.GetId(memberType.TypeHandle),
					memberType.TypeHandle
				});
			}
			catch (TargetInvocationException ex)
			{
				if (ex.InnerException != null)
				{
					throw ex.InnerException;
				}
				throw;
			}
		}

		// Token: 0x060013CC RID: 5068 RVA: 0x00049F4C File Offset: 0x0004814C
		private object UnwrapNullableObject(Func<object> memberValue, ref Type memberType, out bool isNull)
		{
			object obj = memberValue();
			isNull = false;
			while (memberType.IsGenericType && memberType.GetGenericTypeDefinition() == Globals.TypeOfNullable)
			{
				Type type = memberType.GetGenericArguments()[0];
				if ((bool)XmlFormatGeneratorStatics.GetHasValueMethod.MakeGenericMethod(new Type[]
				{
					type
				}).Invoke(null, new object[]
				{
					obj
				}))
				{
					obj = XmlFormatGeneratorStatics.GetNullableValueMethod.MakeGenericMethod(new Type[]
					{
						type
					}).Invoke(null, new object[]
					{
						obj
					});
				}
				else
				{
					isNull = true;
					obj = XmlFormatGeneratorStatics.GetDefaultValueMethod.MakeGenericMethod(new Type[]
					{
						memberType
					}).Invoke(null, new object[0]);
				}
				memberType = type;
			}
			return obj;
		}

		// Token: 0x060013CD RID: 5069 RVA: 0x0004A00C File Offset: 0x0004820C
		private bool TryWritePrimitive(Type type, Func<object> value, MemberInfo memberInfo, int? arrayItemIndex, XmlDictionaryString name, int nameIndex)
		{
			PrimitiveDataContract primitiveDataContract = PrimitiveDataContract.GetPrimitiveDataContract(type);
			if (primitiveDataContract == null || primitiveDataContract.UnderlyingType == Globals.TypeOfObject)
			{
				return false;
			}
			List<object> list = new List<object>();
			object obj;
			if (type.IsValueType)
			{
				obj = this.writer;
			}
			else
			{
				obj = this.context;
				list.Add(this.writer);
			}
			if (value != null)
			{
				list.Add(value());
			}
			else if (memberInfo != null)
			{
				list.Add(CodeInterpreter.GetMember(memberInfo, this.objLocal));
			}
			else
			{
				list.Add(((Array)this.objLocal).GetValue(new int[]
				{
					arrayItemIndex.Value
				}));
			}
			if (name != null)
			{
				list.Add(name);
			}
			else
			{
				list.Add(this.memberNames[nameIndex]);
			}
			list.Add(null);
			primitiveDataContract.XmlFormatWriterMethod.Invoke(obj, list.ToArray());
			return true;
		}

		// Token: 0x060013CE RID: 5070 RVA: 0x0004A0F0 File Offset: 0x000482F0
		private bool TryWritePrimitiveArray(Type type, Type itemType, Func<object> value, XmlDictionaryString itemName)
		{
			if (PrimitiveDataContract.GetPrimitiveDataContract(itemType) == null)
			{
				return false;
			}
			string text = null;
			TypeCode typeCode = Type.GetTypeCode(itemType);
			if (typeCode != TypeCode.Boolean)
			{
				switch (typeCode)
				{
				case TypeCode.Int32:
					text = "WriteJsonInt32Array";
					break;
				case TypeCode.Int64:
					text = "WriteJsonInt64Array";
					break;
				case TypeCode.Single:
					text = "WriteJsonSingleArray";
					break;
				case TypeCode.Double:
					text = "WriteJsonDoubleArray";
					break;
				case TypeCode.Decimal:
					text = "WriteJsonDecimalArray";
					break;
				case TypeCode.DateTime:
					text = "WriteJsonDateTimeArray";
					break;
				}
			}
			else
			{
				text = "WriteJsonBooleanArray";
			}
			if (text != null)
			{
				this.WriteArrayAttribute();
				MethodBase method = typeof(JsonWriterDelegator).GetMethod(text, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
				{
					type,
					typeof(XmlDictionaryString),
					typeof(XmlDictionaryString)
				}, null);
				object obj = this.writer;
				object[] array = new object[3];
				array[0] = value();
				array[1] = itemName;
				method.Invoke(obj, array);
				return true;
			}
			return false;
		}

		// Token: 0x060013CF RID: 5071 RVA: 0x0004A1D9 File Offset: 0x000483D9
		private object LoadMemberValue(DataMember member)
		{
			return CodeInterpreter.GetMember(member.MemberInfo, this.objLocal);
		}

		// Token: 0x060013D0 RID: 5072 RVA: 0x0004A1EC File Offset: 0x000483EC
		[CompilerGenerated]
		private object <WriteCollection>b__22_0()
		{
			return this.objLocal;
		}

		// Token: 0x04000A1A RID: 2586
		private ClassDataContract classContract;

		// Token: 0x04000A1B RID: 2587
		private CollectionDataContract collectionContract;

		// Token: 0x04000A1C RID: 2588
		private XmlWriterDelegator writer;

		// Token: 0x04000A1D RID: 2589
		private object obj;

		// Token: 0x04000A1E RID: 2590
		private XmlObjectSerializerWriteContextComplexJson context;

		// Token: 0x04000A1F RID: 2591
		private DataContract dataContract;

		// Token: 0x04000A20 RID: 2592
		private object objLocal;

		// Token: 0x04000A21 RID: 2593
		private XmlDictionaryString[] memberNames;

		// Token: 0x04000A22 RID: 2594
		private int typeIndex = 1;

		// Token: 0x04000A23 RID: 2595
		private int childElementIndex;

		// Token: 0x02000190 RID: 400
		[CompilerGenerated]
		private sealed class <>c__DisplayClass22_0
		{
			// Token: 0x060013D1 RID: 5073 RVA: 0x00002217 File Offset: 0x00000417
			public <>c__DisplayClass22_0()
			{
			}

			// Token: 0x060013D2 RID: 5074 RVA: 0x0004A1F4 File Offset: 0x000483F4
			internal object <WriteCollection>b__1()
			{
				return this.currentValue;
			}

			// Token: 0x04000A24 RID: 2596
			public object currentValue;

			// Token: 0x04000A25 RID: 2597
			public Func<object> <>9__1;
		}

		// Token: 0x02000191 RID: 401
		[CompilerGenerated]
		private sealed class <>c__DisplayClass23_0
		{
			// Token: 0x060013D3 RID: 5075 RVA: 0x00002217 File Offset: 0x00000417
			public <>c__DisplayClass23_0()
			{
			}

			// Token: 0x060013D4 RID: 5076 RVA: 0x0004A1FC File Offset: 0x000483FC
			internal object <WriteMembers>b__0()
			{
				return this.memberValue;
			}

			// Token: 0x04000A26 RID: 2598
			public object memberValue;
		}

		// Token: 0x02000192 RID: 402
		[CompilerGenerated]
		private sealed class <>c__DisplayClass30_0
		{
			// Token: 0x060013D5 RID: 5077 RVA: 0x00002217 File Offset: 0x00000417
			public <>c__DisplayClass30_0()
			{
			}

			// Token: 0x060013D6 RID: 5078 RVA: 0x0004A204 File Offset: 0x00048404
			internal object <WriteValue>b__0()
			{
				return this.memberValue;
			}

			// Token: 0x060013D7 RID: 5079 RVA: 0x0004A204 File Offset: 0x00048404
			internal object <WriteValue>b__1()
			{
				return this.memberValue;
			}

			// Token: 0x060013D8 RID: 5080 RVA: 0x0004A204 File Offset: 0x00048404
			internal object <WriteValue>b__2()
			{
				return this.memberValue;
			}

			// Token: 0x04000A27 RID: 2599
			public object memberValue;
		}
	}
}
