﻿using System;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200016C RID: 364
	// (Invoke) Token: 0x0600125C RID: 4700
	internal delegate object JsonFormatCollectionReaderDelegate(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContextComplexJson context, XmlDictionaryString emptyDictionaryString, XmlDictionaryString itemName, CollectionDataContract collectionContract);
}
