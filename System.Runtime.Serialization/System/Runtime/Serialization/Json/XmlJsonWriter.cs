﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000187 RID: 391
	internal class XmlJsonWriter : XmlDictionaryWriter, IXmlJsonWriterInitializer
	{
		// Token: 0x060012FE RID: 4862 RVA: 0x0004552A File Offset: 0x0004372A
		public XmlJsonWriter() : this(false, null)
		{
		}

		// Token: 0x060012FF RID: 4863 RVA: 0x00045534 File Offset: 0x00043734
		public XmlJsonWriter(bool indent, string indentChars)
		{
			this.indent = indent;
			if (indent)
			{
				if (indentChars == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("indentChars");
				}
				this.indentChars = indentChars;
			}
			this.InitializeWriter();
			if (XmlJsonWriter.CharacterAbbrevs == null)
			{
				XmlJsonWriter.CharacterAbbrevs = XmlJsonWriter.GetCharacterAbbrevs();
			}
		}

		// Token: 0x06001300 RID: 4864 RVA: 0x00045574 File Offset: 0x00043774
		private static char[] GetCharacterAbbrevs()
		{
			char[] array = new char[32];
			for (int i = 0; i < 32; i++)
			{
				char c;
				if (!LocalAppContextSwitches.DoNotUseEcmaScriptV6EscapeControlCharacter && XmlJsonWriter.TryEscapeControlCharacter((char)i, out c))
				{
					array[i] = c;
				}
				else
				{
					array[i] = '\0';
				}
			}
			return array;
		}

		// Token: 0x06001301 RID: 4865 RVA: 0x000455B4 File Offset: 0x000437B4
		private static bool TryEscapeControlCharacter(char ch, out char abbrev)
		{
			switch (ch)
			{
			case '\b':
				abbrev = 'b';
				return true;
			case '\t':
				abbrev = 't';
				return true;
			case '\n':
				abbrev = 'n';
				return true;
			case '\f':
				abbrev = 'f';
				return true;
			case '\r':
				abbrev = 'r';
				return true;
			}
			abbrev = ' ';
			return false;
		}

		// Token: 0x17000404 RID: 1028
		// (get) Token: 0x06001302 RID: 4866 RVA: 0x0001BF2F File Offset: 0x0001A12F
		public override XmlWriterSettings Settings
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000405 RID: 1029
		// (get) Token: 0x06001303 RID: 4867 RVA: 0x00045608 File Offset: 0x00043808
		public override WriteState WriteState
		{
			get
			{
				if (this.writeState == WriteState.Closed)
				{
					return WriteState.Closed;
				}
				if (this.HasOpenAttribute)
				{
					return WriteState.Attribute;
				}
				switch (this.nodeType)
				{
				case JsonNodeType.None:
					return WriteState.Start;
				case JsonNodeType.Element:
					return WriteState.Element;
				case JsonNodeType.EndElement:
				case JsonNodeType.QuotedText:
				case JsonNodeType.StandaloneText:
					return WriteState.Content;
				}
				return WriteState.Error;
			}
		}

		// Token: 0x17000406 RID: 1030
		// (get) Token: 0x06001304 RID: 4868 RVA: 0x0001BF2F File Offset: 0x0001A12F
		public override string XmlLang
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x06001305 RID: 4869 RVA: 0x0000310F File Offset: 0x0000130F
		public override XmlSpace XmlSpace
		{
			get
			{
				return XmlSpace.None;
			}
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x06001306 RID: 4870 RVA: 0x00045658 File Offset: 0x00043858
		private static BinHexEncoding BinHexEncoding
		{
			[SecuritySafeCritical]
			get
			{
				if (XmlJsonWriter.binHexEncoding == null)
				{
					XmlJsonWriter.binHexEncoding = new BinHexEncoding();
				}
				return XmlJsonWriter.binHexEncoding;
			}
		}

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x06001307 RID: 4871 RVA: 0x00045670 File Offset: 0x00043870
		private bool HasOpenAttribute
		{
			get
			{
				return this.isWritingDataTypeAttribute || this.isWritingServerTypeAttribute || this.IsWritingNameAttribute || this.isWritingXmlnsAttribute;
			}
		}

		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x06001308 RID: 4872 RVA: 0x00045692 File Offset: 0x00043892
		private bool IsClosed
		{
			get
			{
				return this.WriteState == WriteState.Closed;
			}
		}

		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x06001309 RID: 4873 RVA: 0x0004569D File Offset: 0x0004389D
		private bool IsWritingCollection
		{
			get
			{
				return this.depth > 0 && this.scopes[this.depth] == JsonNodeType.Collection;
			}
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x0600130A RID: 4874 RVA: 0x000456BA File Offset: 0x000438BA
		private bool IsWritingNameAttribute
		{
			get
			{
				return (this.nameState & XmlJsonWriter.NameState.IsWritingNameAttribute) == XmlJsonWriter.NameState.IsWritingNameAttribute;
			}
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x0600130B RID: 4875 RVA: 0x000456C7 File Offset: 0x000438C7
		private bool IsWritingNameWithMapping
		{
			get
			{
				return (this.nameState & XmlJsonWriter.NameState.IsWritingNameWithMapping) == XmlJsonWriter.NameState.IsWritingNameWithMapping;
			}
		}

		// Token: 0x1700040E RID: 1038
		// (get) Token: 0x0600130C RID: 4876 RVA: 0x000456D4 File Offset: 0x000438D4
		private bool WrittenNameWithMapping
		{
			get
			{
				return (this.nameState & XmlJsonWriter.NameState.WrittenNameWithMapping) == XmlJsonWriter.NameState.WrittenNameWithMapping;
			}
		}

		// Token: 0x0600130D RID: 4877 RVA: 0x000456E4 File Offset: 0x000438E4
		public override void Close()
		{
			if (!this.IsClosed)
			{
				try
				{
					this.WriteEndDocument();
				}
				finally
				{
					try
					{
						this.nodeWriter.Flush();
						this.nodeWriter.Close();
					}
					finally
					{
						this.writeState = WriteState.Closed;
						if (this.depth != 0)
						{
							this.depth = 0;
						}
					}
				}
			}
		}

		// Token: 0x0600130E RID: 4878 RVA: 0x00045750 File Offset: 0x00043950
		public override void Flush()
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			this.nodeWriter.Flush();
		}

		// Token: 0x0600130F RID: 4879 RVA: 0x0004576C File Offset: 0x0004396C
		public override string LookupPrefix(string ns)
		{
			if (ns == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("ns");
			}
			if (ns == "http://www.w3.org/2000/xmlns/")
			{
				return "xmlns";
			}
			if (ns == "http://www.w3.org/XML/1998/namespace")
			{
				return "xml";
			}
			if (ns == string.Empty)
			{
				return string.Empty;
			}
			return null;
		}

		// Token: 0x06001310 RID: 4880 RVA: 0x000457C4 File Offset: 0x000439C4
		public void SetOutput(Stream stream, Encoding encoding, bool ownsStream)
		{
			if (stream == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("stream");
			}
			if (encoding == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("encoding");
			}
			if (encoding.WebName != Encoding.UTF8.WebName)
			{
				stream = new JsonEncodingStreamWrapper(stream, encoding, false);
			}
			else
			{
				encoding = null;
			}
			if (this.nodeWriter == null)
			{
				this.nodeWriter = new XmlJsonWriter.JsonNodeWriter();
			}
			this.nodeWriter.SetOutput(stream, ownsStream, encoding);
			this.InitializeWriter();
		}

		// Token: 0x06001311 RID: 4881 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, string localName, string namespaceUri, bool[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001312 RID: 4882 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, string localName, string namespaceUri, short[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001313 RID: 4883 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, string localName, string namespaceUri, int[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001314 RID: 4884 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, string localName, string namespaceUri, long[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001315 RID: 4885 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, string localName, string namespaceUri, float[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001316 RID: 4886 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, string localName, string namespaceUri, double[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001317 RID: 4887 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, string localName, string namespaceUri, decimal[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001318 RID: 4888 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, string localName, string namespaceUri, DateTime[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001319 RID: 4889 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, string localName, string namespaceUri, Guid[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x0600131A RID: 4890 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, string localName, string namespaceUri, TimeSpan[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x0600131B RID: 4891 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, bool[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x0600131C RID: 4892 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, decimal[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x0600131D RID: 4893 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, double[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x0600131E RID: 4894 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, float[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x0600131F RID: 4895 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, int[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001320 RID: 4896 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, long[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001321 RID: 4897 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, short[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001322 RID: 4898 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, DateTime[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001323 RID: 4899 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, Guid[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001324 RID: 4900 RVA: 0x0004583A File Offset: 0x00043A3A
		public override void WriteArray(string prefix, XmlDictionaryString localName, XmlDictionaryString namespaceUri, TimeSpan[] array, int offset, int count)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("JSON WriteArray is not supported.")));
		}

		// Token: 0x06001325 RID: 4901 RVA: 0x00045850 File Offset: 0x00043A50
		public override void WriteBase64(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("buffer");
			}
			if (index < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("index", SR.GetString("The value of this argument must be non-negative.")));
			}
			if (count < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("count", SR.GetString("The value of this argument must be non-negative.")));
			}
			if (count > buffer.Length - index)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("count", SR.GetString("JSON size exceeded remaining buffer space, by {0} byte(s).", new object[]
				{
					buffer.Length - index
				})));
			}
			this.StartText();
			this.nodeWriter.WriteBase64Text(buffer, 0, buffer, index, count);
		}

		// Token: 0x06001326 RID: 4902 RVA: 0x000458F4 File Offset: 0x00043AF4
		public override void WriteBinHex(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("buffer");
			}
			if (index < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("index", SR.GetString("The value of this argument must be non-negative.")));
			}
			if (count < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("count", SR.GetString("The value of this argument must be non-negative.")));
			}
			if (count > buffer.Length - index)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("count", SR.GetString("JSON size exceeded remaining buffer space, by {0} byte(s).", new object[]
				{
					buffer.Length - index
				})));
			}
			this.StartText();
			this.WriteEscapedJsonString(XmlJsonWriter.BinHexEncoding.GetString(buffer, index, count));
		}

		// Token: 0x06001327 RID: 4903 RVA: 0x00045999 File Offset: 0x00043B99
		public override void WriteCData(string text)
		{
			this.WriteString(text);
		}

		// Token: 0x06001328 RID: 4904 RVA: 0x000459A2 File Offset: 0x00043BA2
		public override void WriteCharEntity(char ch)
		{
			this.WriteString(ch.ToString());
		}

		// Token: 0x06001329 RID: 4905 RVA: 0x000459B4 File Offset: 0x00043BB4
		public override void WriteChars(char[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("buffer");
			}
			if (index < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("index", SR.GetString("The value of this argument must be non-negative.")));
			}
			if (count < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("count", SR.GetString("The value of this argument must be non-negative.")));
			}
			if (count > buffer.Length - index)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("count", SR.GetString("JSON size exceeded remaining buffer space, by {0} byte(s).", new object[]
				{
					buffer.Length - index
				})));
			}
			this.WriteString(new string(buffer, index, count));
		}

		// Token: 0x0600132A RID: 4906 RVA: 0x00045A4E File Offset: 0x00043C4E
		public override void WriteComment(string text)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("Method {0} is not supported in JSON.", new object[]
			{
				"WriteComment"
			})));
		}

		// Token: 0x0600132B RID: 4907 RVA: 0x00045A72 File Offset: 0x00043C72
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("Method {0} is not supported in JSON.", new object[]
			{
				"WriteDocType"
			})));
		}

		// Token: 0x0600132C RID: 4908 RVA: 0x00045A98 File Offset: 0x00043C98
		public override void WriteEndAttribute()
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (!this.HasOpenAttribute)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("WriteEndAttribute was called while there is no open attribute.")));
			}
			if (this.isWritingDataTypeAttribute)
			{
				string a = this.attributeText;
				if (!(a == "number"))
				{
					if (!(a == "string"))
					{
						if (!(a == "array"))
						{
							if (!(a == "object"))
							{
								if (!(a == "null"))
								{
									if (!(a == "boolean"))
									{
										throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Unexpected attribute value '{0}'.", new object[]
										{
											this.attributeText
										})));
									}
									this.ThrowIfServerTypeWritten("boolean");
									this.dataType = XmlJsonWriter.JsonDataType.Boolean;
								}
								else
								{
									this.ThrowIfServerTypeWritten("null");
									this.dataType = XmlJsonWriter.JsonDataType.Null;
								}
							}
							else
							{
								this.dataType = XmlJsonWriter.JsonDataType.Object;
							}
						}
						else
						{
							this.ThrowIfServerTypeWritten("array");
							this.dataType = XmlJsonWriter.JsonDataType.Array;
						}
					}
					else
					{
						this.ThrowIfServerTypeWritten("string");
						this.dataType = XmlJsonWriter.JsonDataType.String;
					}
				}
				else
				{
					this.ThrowIfServerTypeWritten("number");
					this.dataType = XmlJsonWriter.JsonDataType.Number;
				}
				this.attributeText = null;
				this.isWritingDataTypeAttribute = false;
				if (!this.IsWritingNameWithMapping || this.WrittenNameWithMapping)
				{
					this.WriteDataTypeServerType();
					return;
				}
			}
			else if (this.isWritingServerTypeAttribute)
			{
				this.serverTypeValue = this.attributeText;
				this.attributeText = null;
				this.isWritingServerTypeAttribute = false;
				if ((!this.IsWritingNameWithMapping || this.WrittenNameWithMapping) && this.dataType == XmlJsonWriter.JsonDataType.Object)
				{
					this.WriteServerTypeAttribute();
					return;
				}
			}
			else
			{
				if (this.IsWritingNameAttribute)
				{
					this.WriteJsonElementName(this.attributeText);
					this.attributeText = null;
					this.nameState = (XmlJsonWriter.NameState.IsWritingNameWithMapping | XmlJsonWriter.NameState.WrittenNameWithMapping);
					this.WriteDataTypeServerType();
					return;
				}
				if (this.isWritingXmlnsAttribute)
				{
					if (!string.IsNullOrEmpty(this.attributeText) && this.isWritingXmlnsAttributeDefaultNs)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("ns", SR.GetString("JSON namespace is specified as '{0}' but it must be empty.", new object[]
						{
							this.attributeText
						}));
					}
					this.attributeText = null;
					this.isWritingXmlnsAttribute = false;
					this.isWritingXmlnsAttributeDefaultNs = false;
				}
			}
		}

		// Token: 0x0600132D RID: 4909 RVA: 0x00045CB1 File Offset: 0x00043EB1
		public override void WriteEndDocument()
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (this.nodeType != JsonNodeType.None)
			{
				while (this.depth > 0)
				{
					this.WriteEndElement();
				}
			}
		}

		// Token: 0x0600132E RID: 4910 RVA: 0x00045CDC File Offset: 0x00043EDC
		public override void WriteEndElement()
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (this.depth == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Encountered an end element while there was no open element in JSON writer.")));
			}
			if (this.HasOpenAttribute)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON attribute must be closed first before calling {0} method.", new object[]
				{
					"WriteEndElement"
				})));
			}
			this.endElementBuffer = false;
			JsonNodeType jsonNodeType = this.ExitScope();
			if (jsonNodeType == JsonNodeType.Collection)
			{
				this.indentLevel--;
				if (this.indent)
				{
					if (this.nodeType == JsonNodeType.Element)
					{
						this.nodeWriter.WriteText(32);
					}
					else
					{
						this.WriteNewLine();
						this.WriteIndent();
					}
				}
				this.nodeWriter.WriteText(93);
				jsonNodeType = this.ExitScope();
			}
			else if (this.nodeType == JsonNodeType.QuotedText)
			{
				this.WriteJsonQuote();
			}
			else if (this.nodeType == JsonNodeType.Element)
			{
				if (this.dataType == XmlJsonWriter.JsonDataType.None && this.serverTypeValue != null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("On JSON writer data type '{0}' must be specified. Object string is '{1}', server type string is '{2}'.", new object[]
					{
						"type",
						"object",
						"__type"
					})));
				}
				if (this.IsWritingNameWithMapping && !this.WrittenNameWithMapping)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("On JSON writer data type '{0}' must be specified. Object string is '{1}', server type string is '{2}'.", new object[]
					{
						"item",
						string.Empty,
						"item"
					})));
				}
				if (this.dataType == XmlJsonWriter.JsonDataType.None || this.dataType == XmlJsonWriter.JsonDataType.String)
				{
					this.nodeWriter.WriteText(34);
					this.nodeWriter.WriteText(34);
				}
			}
			if (this.depth != 0)
			{
				if (jsonNodeType == JsonNodeType.Element)
				{
					this.endElementBuffer = true;
				}
				else if (jsonNodeType == JsonNodeType.Object)
				{
					this.indentLevel--;
					if (this.indent)
					{
						if (this.nodeType == JsonNodeType.Element)
						{
							this.nodeWriter.WriteText(32);
						}
						else
						{
							this.WriteNewLine();
							this.WriteIndent();
						}
					}
					this.nodeWriter.WriteText(125);
					if (this.depth > 0 && this.scopes[this.depth] == JsonNodeType.Element)
					{
						this.ExitScope();
						this.endElementBuffer = true;
					}
				}
			}
			this.dataType = XmlJsonWriter.JsonDataType.None;
			this.nodeType = JsonNodeType.EndElement;
			this.nameState = XmlJsonWriter.NameState.None;
			this.wroteServerTypeAttribute = false;
		}

		// Token: 0x0600132F RID: 4911 RVA: 0x00045F17 File Offset: 0x00044117
		public override void WriteEntityRef(string name)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("Method {0} is not supported in JSON.", new object[]
			{
				"WriteEntityRef"
			})));
		}

		// Token: 0x06001330 RID: 4912 RVA: 0x00045F3B File Offset: 0x0004413B
		public override void WriteFullEndElement()
		{
			this.WriteEndElement();
		}

		// Token: 0x06001331 RID: 4913 RVA: 0x00045F44 File Offset: 0x00044144
		public override void WriteProcessingInstruction(string name, string text)
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (!name.Equals("xml", StringComparison.OrdinalIgnoreCase))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentException(SR.GetString("processing instruction is not supported in JSON writer."), "name"));
			}
			if (this.WriteState != WriteState.Start)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Attempt to write invalid XML declration.")));
			}
		}

		// Token: 0x06001332 RID: 4914 RVA: 0x00045FA3 File Offset: 0x000441A3
		public override void WriteQualifiedName(string localName, string ns)
		{
			if (localName == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("localName");
			}
			if (localName.Length == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("localName", SR.GetString("Empty string is invalid as a local name."));
			}
			if (ns == null)
			{
				ns = string.Empty;
			}
			base.WriteQualifiedName(localName, ns);
		}

		// Token: 0x06001333 RID: 4915 RVA: 0x00045999 File Offset: 0x00043B99
		public override void WriteRaw(string data)
		{
			this.WriteString(data);
		}

		// Token: 0x06001334 RID: 4916 RVA: 0x00045FE4 File Offset: 0x000441E4
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("buffer");
			}
			if (index < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("index", SR.GetString("The value of this argument must be non-negative.")));
			}
			if (count < 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("count", SR.GetString("The value of this argument must be non-negative.")));
			}
			if (count > buffer.Length - index)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentOutOfRangeException("count", SR.GetString("JSON size exceeded remaining buffer space, by {0} byte(s).", new object[]
				{
					buffer.Length - index
				})));
			}
			this.WriteString(new string(buffer, index, count));
		}

		// Token: 0x06001335 RID: 4917 RVA: 0x00046080 File Offset: 0x00044280
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (!string.IsNullOrEmpty(prefix))
			{
				if (!this.IsWritingNameWithMapping || !(prefix == "xmlns"))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("prefix", SR.GetString("JSON prefix must be null or empty. '{0}' is specified instead.", new object[]
					{
						prefix
					}));
				}
				if (ns != null && ns != "http://www.w3.org/2000/xmlns/")
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentException(SR.GetString("The prefix '{0}' is bound to the namespace '{1}' and cannot be changed to '{2}'.", new object[]
					{
						"xmlns",
						"http://www.w3.org/2000/xmlns/",
						ns
					}), "ns"));
				}
			}
			else if (this.IsWritingNameWithMapping && ns == "http://www.w3.org/2000/xmlns/" && localName != "xmlns")
			{
				prefix = "xmlns";
			}
			if (!string.IsNullOrEmpty(ns))
			{
				if (this.IsWritingNameWithMapping && ns == "http://www.w3.org/2000/xmlns/")
				{
					prefix = "xmlns";
				}
				else
				{
					if (!string.IsNullOrEmpty(prefix) || !(localName == "xmlns") || !(ns == "http://www.w3.org/2000/xmlns/"))
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("ns", SR.GetString("JSON namespace is specified as '{0}' but it must be empty.", new object[]
						{
							ns
						}));
					}
					prefix = "xmlns";
					this.isWritingXmlnsAttributeDefaultNs = true;
				}
			}
			if (localName == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("localName");
			}
			if (localName.Length == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("localName", SR.GetString("Empty string is invalid as a local name."));
			}
			if (this.nodeType != JsonNodeType.Element && !this.wroteServerTypeAttribute)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON attribute must have an owner element.")));
			}
			if (this.HasOpenAttribute)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON attribute must be closed first before calling {0} method.", new object[]
				{
					"WriteStartAttribute"
				})));
			}
			if (prefix == "xmlns")
			{
				this.isWritingXmlnsAttribute = true;
				return;
			}
			if (localName == "type")
			{
				if (this.dataType != XmlJsonWriter.JsonDataType.None)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON attribute '{0}' is already written.", new object[]
					{
						"type"
					})));
				}
				this.isWritingDataTypeAttribute = true;
				return;
			}
			else if (localName == "__type")
			{
				if (this.serverTypeValue != null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON attribute '{0}' is already written.", new object[]
					{
						"__type"
					})));
				}
				if (this.dataType != XmlJsonWriter.JsonDataType.None && this.dataType != XmlJsonWriter.JsonDataType.Object)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Server type is specified for invalid data type in JSON. Server type: '{0}', type: '{1}', dataType: '{2}', object: '{3}'.", new object[]
					{
						"__type",
						"type",
						this.dataType.ToString().ToLowerInvariant(),
						"object"
					})));
				}
				this.isWritingServerTypeAttribute = true;
				return;
			}
			else
			{
				if (!(localName == "item"))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("localName", SR.GetString("Unexpected attribute local name '{0}'.", new object[]
					{
						localName
					}));
				}
				if (this.WrittenNameWithMapping)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON attribute '{0}' is already written.", new object[]
					{
						"item"
					})));
				}
				if (!this.IsWritingNameWithMapping)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Encountered an end element while there was no open element in JSON writer.")));
				}
				this.nameState |= XmlJsonWriter.NameState.IsWritingNameAttribute;
				return;
			}
		}

		// Token: 0x06001336 RID: 4918 RVA: 0x000463B0 File Offset: 0x000445B0
		public override void WriteStartDocument(bool standalone)
		{
			this.WriteStartDocument();
		}

		// Token: 0x06001337 RID: 4919 RVA: 0x000463B8 File Offset: 0x000445B8
		public override void WriteStartDocument()
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (this.WriteState != WriteState.Start)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid write state {1} for '{0}' method.", new object[]
				{
					"WriteStartDocument",
					this.WriteState.ToString()
				})));
			}
		}

		// Token: 0x06001338 RID: 4920 RVA: 0x00046414 File Offset: 0x00044614
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			if (localName == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("localName");
			}
			if (localName.Length == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("localName", SR.GetString("Empty string is invalid as a local name."));
			}
			if (!string.IsNullOrEmpty(prefix) && (string.IsNullOrEmpty(ns) || !this.TrySetWritingNameWithMapping(localName, ns)))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("prefix", SR.GetString("JSON prefix must be null or empty. '{0}' is specified instead.", new object[]
				{
					prefix
				}));
			}
			if (!string.IsNullOrEmpty(ns) && !this.TrySetWritingNameWithMapping(localName, ns))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("ns", SR.GetString("JSON namespace is specified as '{0}' but it must be empty.", new object[]
				{
					ns
				}));
			}
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (this.HasOpenAttribute)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON attribute must be closed first before calling {0} method.", new object[]
				{
					"WriteStartElement"
				})));
			}
			if (this.nodeType != JsonNodeType.None && this.depth == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Multiple root element is not allowed on JSON writer.")));
			}
			switch (this.nodeType)
			{
			case JsonNodeType.None:
				if (!localName.Equals("root"))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid root element name '{0}' (root element is '{1}' in JSON).", new object[]
					{
						localName,
						"root"
					})));
				}
				this.EnterScope(JsonNodeType.Element);
				goto IL_27E;
			case JsonNodeType.Element:
				if (this.dataType != XmlJsonWriter.JsonDataType.Array && this.dataType != XmlJsonWriter.JsonDataType.Object)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Either Object or Array of JSON node type must be specified.")));
				}
				if (this.indent)
				{
					this.WriteNewLine();
					this.WriteIndent();
				}
				if (!this.IsWritingCollection)
				{
					if (this.nameState != XmlJsonWriter.NameState.IsWritingNameWithMapping)
					{
						this.WriteJsonElementName(localName);
					}
				}
				else if (!localName.Equals("item"))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid JSON item name '{0}' for array element (item element is '{1}' in JSON).", new object[]
					{
						localName,
						"item"
					})));
				}
				this.EnterScope(JsonNodeType.Element);
				goto IL_27E;
			case JsonNodeType.EndElement:
				if (this.endElementBuffer)
				{
					this.nodeWriter.WriteText(44);
				}
				if (this.indent)
				{
					this.WriteNewLine();
					this.WriteIndent();
				}
				if (!this.IsWritingCollection)
				{
					if (this.nameState != XmlJsonWriter.NameState.IsWritingNameWithMapping)
					{
						this.WriteJsonElementName(localName);
					}
				}
				else if (!localName.Equals("item"))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid JSON item name '{0}' for array element (item element is '{1}' in JSON).", new object[]
					{
						localName,
						"item"
					})));
				}
				this.EnterScope(JsonNodeType.Element);
				goto IL_27E;
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid call to JSON WriteStartElement method.")));
			IL_27E:
			this.isWritingDataTypeAttribute = false;
			this.isWritingServerTypeAttribute = false;
			this.isWritingXmlnsAttribute = false;
			this.wroteServerTypeAttribute = false;
			this.serverTypeValue = null;
			this.dataType = XmlJsonWriter.JsonDataType.None;
			this.nodeType = JsonNodeType.Element;
		}

		// Token: 0x06001339 RID: 4921 RVA: 0x000466D0 File Offset: 0x000448D0
		public override void WriteString(string text)
		{
			if (this.HasOpenAttribute && text != null)
			{
				this.attributeText += text;
				return;
			}
			if (text == null)
			{
				text = string.Empty;
			}
			if ((this.dataType != XmlJsonWriter.JsonDataType.Array && this.dataType != XmlJsonWriter.JsonDataType.Object && this.nodeType != JsonNodeType.EndElement) || !XmlConverter.IsWhitespace(text))
			{
				this.StartText();
				this.WriteEscapedJsonString(text);
			}
		}

		// Token: 0x0600133A RID: 4922 RVA: 0x00046735 File Offset: 0x00044935
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			this.WriteString(highChar + lowChar);
		}

		// Token: 0x0600133B RID: 4923 RVA: 0x0004674E File Offset: 0x0004494E
		public override void WriteValue(bool value)
		{
			this.StartText();
			this.nodeWriter.WriteBoolText(value);
		}

		// Token: 0x0600133C RID: 4924 RVA: 0x00046762 File Offset: 0x00044962
		public override void WriteValue(decimal value)
		{
			this.StartText();
			this.nodeWriter.WriteDecimalText(value);
		}

		// Token: 0x0600133D RID: 4925 RVA: 0x00046776 File Offset: 0x00044976
		public override void WriteValue(double value)
		{
			this.StartText();
			this.nodeWriter.WriteDoubleText(value);
		}

		// Token: 0x0600133E RID: 4926 RVA: 0x0004678A File Offset: 0x0004498A
		public override void WriteValue(float value)
		{
			this.StartText();
			this.nodeWriter.WriteFloatText(value);
		}

		// Token: 0x0600133F RID: 4927 RVA: 0x0004679E File Offset: 0x0004499E
		public override void WriteValue(int value)
		{
			this.StartText();
			this.nodeWriter.WriteInt32Text(value);
		}

		// Token: 0x06001340 RID: 4928 RVA: 0x000467B2 File Offset: 0x000449B2
		public override void WriteValue(long value)
		{
			this.StartText();
			this.nodeWriter.WriteInt64Text(value);
		}

		// Token: 0x06001341 RID: 4929 RVA: 0x000467C6 File Offset: 0x000449C6
		public override void WriteValue(Guid value)
		{
			this.StartText();
			this.nodeWriter.WriteGuidText(value);
		}

		// Token: 0x06001342 RID: 4930 RVA: 0x000467DA File Offset: 0x000449DA
		public override void WriteValue(DateTime value)
		{
			this.StartText();
			this.nodeWriter.WriteDateTimeText(value);
		}

		// Token: 0x06001343 RID: 4931 RVA: 0x00045999 File Offset: 0x00043B99
		public override void WriteValue(string value)
		{
			this.WriteString(value);
		}

		// Token: 0x06001344 RID: 4932 RVA: 0x000467EE File Offset: 0x000449EE
		public override void WriteValue(TimeSpan value)
		{
			this.StartText();
			this.nodeWriter.WriteTimeSpanText(value);
		}

		// Token: 0x06001345 RID: 4933 RVA: 0x00046802 File Offset: 0x00044A02
		public override void WriteValue(UniqueId value)
		{
			if (value == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("value");
			}
			this.StartText();
			this.nodeWriter.WriteUniqueIdText(value);
		}

		// Token: 0x06001346 RID: 4934 RVA: 0x0004682C File Offset: 0x00044A2C
		public override void WriteValue(object value)
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (value == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("value");
			}
			if (value is Array)
			{
				this.WriteValue((Array)value);
				return;
			}
			if (value is IStreamProvider)
			{
				this.WriteValue((IStreamProvider)value);
				return;
			}
			this.WritePrimitiveValue(value);
		}

		// Token: 0x06001347 RID: 4935 RVA: 0x00046888 File Offset: 0x00044A88
		public override void WriteWhitespace(string ws)
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (ws == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgumentNull("ws");
			}
			foreach (char c in ws)
			{
				if (c != ' ' && c != '\t' && c != '\n' && c != '\r')
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("ws", SR.GetString("Only whitespace characters are allowed for {1} method. The specified value is '{0}'", new object[]
					{
						c.ToString(),
						"WriteWhitespace"
					}));
				}
			}
			this.WriteString(ws);
		}

		// Token: 0x06001348 RID: 4936 RVA: 0x00046911 File Offset: 0x00044B11
		public override void WriteXmlAttribute(string localName, string value)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("Method {0} is not supported in JSON.", new object[]
			{
				"WriteXmlAttribute"
			})));
		}

		// Token: 0x06001349 RID: 4937 RVA: 0x00046911 File Offset: 0x00044B11
		public override void WriteXmlAttribute(XmlDictionaryString localName, XmlDictionaryString value)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("Method {0} is not supported in JSON.", new object[]
			{
				"WriteXmlAttribute"
			})));
		}

		// Token: 0x0600134A RID: 4938 RVA: 0x00046935 File Offset: 0x00044B35
		public override void WriteXmlnsAttribute(string prefix, string namespaceUri)
		{
			if (!this.IsWritingNameWithMapping)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("Method {0} is not supported in JSON.", new object[]
				{
					"WriteXmlnsAttribute"
				})));
			}
		}

		// Token: 0x0600134B RID: 4939 RVA: 0x00046935 File Offset: 0x00044B35
		public override void WriteXmlnsAttribute(string prefix, XmlDictionaryString namespaceUri)
		{
			if (!this.IsWritingNameWithMapping)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotSupportedException(SR.GetString("Method {0} is not supported in JSON.", new object[]
				{
					"WriteXmlnsAttribute"
				})));
			}
		}

		// Token: 0x0600134C RID: 4940 RVA: 0x00046962 File Offset: 0x00044B62
		internal static bool CharacterNeedsEscaping(char ch)
		{
			return ch == '/' || ch == '"' || ch < ' ' || ch == '\\' || (ch >= '\ud800' && (ch <= '\udfff' || ch >= '￾'));
		}

		// Token: 0x0600134D RID: 4941 RVA: 0x00046999 File Offset: 0x00044B99
		private static void ThrowClosed()
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(SR.GetString("JSON writer is already closed.")));
		}

		// Token: 0x0600134E RID: 4942 RVA: 0x000469B0 File Offset: 0x00044BB0
		private void CheckText(JsonNodeType nextNodeType)
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (this.depth == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(SR.GetString("Text cannot be written outside the root element.")));
			}
			if (nextNodeType == JsonNodeType.StandaloneText && this.nodeType == JsonNodeType.QuotedText)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON writer cannot write standalone text after quoted text.")));
			}
		}

		// Token: 0x0600134F RID: 4943 RVA: 0x00046A0C File Offset: 0x00044C0C
		private void EnterScope(JsonNodeType currentNodeType)
		{
			this.depth++;
			if (this.scopes == null)
			{
				this.scopes = new JsonNodeType[4];
			}
			else if (this.scopes.Length == this.depth)
			{
				JsonNodeType[] destinationArray = new JsonNodeType[this.depth * 2];
				Array.Copy(this.scopes, destinationArray, this.depth);
				this.scopes = destinationArray;
			}
			this.scopes[this.depth] = currentNodeType;
		}

		// Token: 0x06001350 RID: 4944 RVA: 0x00046A82 File Offset: 0x00044C82
		private JsonNodeType ExitScope()
		{
			JsonNodeType result = this.scopes[this.depth];
			this.scopes[this.depth] = JsonNodeType.None;
			this.depth--;
			return result;
		}

		// Token: 0x06001351 RID: 4945 RVA: 0x00046AB0 File Offset: 0x00044CB0
		private void InitializeWriter()
		{
			this.nodeType = JsonNodeType.None;
			this.dataType = XmlJsonWriter.JsonDataType.None;
			this.isWritingDataTypeAttribute = false;
			this.wroteServerTypeAttribute = false;
			this.isWritingServerTypeAttribute = false;
			this.serverTypeValue = null;
			this.attributeText = null;
			if (this.depth != 0)
			{
				this.depth = 0;
			}
			if (this.scopes != null && this.scopes.Length > 25)
			{
				this.scopes = null;
			}
			this.writeState = WriteState.Start;
			this.endElementBuffer = false;
			this.indentLevel = 0;
		}

		// Token: 0x06001352 RID: 4946 RVA: 0x00046B2D File Offset: 0x00044D2D
		private static bool IsUnicodeNewlineCharacter(char c)
		{
			return c == '\u0085' || c == '\u2028' || c == '\u2029';
		}

		// Token: 0x06001353 RID: 4947 RVA: 0x00046B4C File Offset: 0x00044D4C
		private void StartText()
		{
			if (this.HasOpenAttribute)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidOperationException(SR.GetString("On JSON writer WriteString must be used for writing attribute values.")));
			}
			if (this.dataType == XmlJsonWriter.JsonDataType.None && this.serverTypeValue != null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("On JSON writer data type '{0}' must be specified. Object string is '{1}', server type string is '{2}'.", new object[]
				{
					"type",
					"object",
					"__type"
				})));
			}
			if (this.IsWritingNameWithMapping && !this.WrittenNameWithMapping)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("On JSON writer data type '{0}' must be specified. Object string is '{1}', server type string is '{2}'.", new object[]
				{
					"item",
					string.Empty,
					"item"
				})));
			}
			if (this.dataType == XmlJsonWriter.JsonDataType.String || this.dataType == XmlJsonWriter.JsonDataType.None)
			{
				this.CheckText(JsonNodeType.QuotedText);
				if (this.nodeType != JsonNodeType.QuotedText)
				{
					this.WriteJsonQuote();
				}
				this.nodeType = JsonNodeType.QuotedText;
				return;
			}
			if (this.dataType == XmlJsonWriter.JsonDataType.Number || this.dataType == XmlJsonWriter.JsonDataType.Boolean)
			{
				this.CheckText(JsonNodeType.StandaloneText);
				this.nodeType = JsonNodeType.StandaloneText;
				return;
			}
			this.ThrowInvalidAttributeContent();
		}

		// Token: 0x06001354 RID: 4948 RVA: 0x00046C54 File Offset: 0x00044E54
		private void ThrowIfServerTypeWritten(string dataTypeSpecified)
		{
			if (this.serverTypeValue != null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("The specified data type is invalid for server type. Type: '{0}', specified data type: '{1}', server type: '{2}', object '{3}'.", new object[]
				{
					"type",
					dataTypeSpecified,
					"__type",
					"object"
				})));
			}
		}

		// Token: 0x06001355 RID: 4949 RVA: 0x00046CA0 File Offset: 0x00044EA0
		private void ThrowInvalidAttributeContent()
		{
			if (this.HasOpenAttribute)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Invalid method call state between start and end attribute.")));
			}
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("JSON writer cannot write text after non-text attribute. Data type is '{0}'.", new object[]
			{
				this.dataType.ToString().ToLowerInvariant()
			})));
		}

		// Token: 0x06001356 RID: 4950 RVA: 0x00046CFD File Offset: 0x00044EFD
		private bool TrySetWritingNameWithMapping(string localName, string ns)
		{
			if (localName.Equals("item") && ns.Equals("item"))
			{
				this.nameState = XmlJsonWriter.NameState.IsWritingNameWithMapping;
				return true;
			}
			return false;
		}

		// Token: 0x06001357 RID: 4951 RVA: 0x00046D24 File Offset: 0x00044F24
		private void WriteDataTypeServerType()
		{
			if (this.dataType != XmlJsonWriter.JsonDataType.None)
			{
				XmlJsonWriter.JsonDataType jsonDataType = this.dataType;
				if (jsonDataType != XmlJsonWriter.JsonDataType.Null)
				{
					if (jsonDataType != XmlJsonWriter.JsonDataType.Object)
					{
						if (jsonDataType == XmlJsonWriter.JsonDataType.Array)
						{
							this.EnterScope(JsonNodeType.Collection);
							this.nodeWriter.WriteText(91);
							this.indentLevel++;
						}
					}
					else
					{
						this.EnterScope(JsonNodeType.Object);
						this.nodeWriter.WriteText(123);
						this.indentLevel++;
					}
				}
				else
				{
					this.nodeWriter.WriteText("null");
				}
				if (this.serverTypeValue != null)
				{
					this.WriteServerTypeAttribute();
				}
			}
		}

		// Token: 0x06001358 RID: 4952 RVA: 0x00046DB4 File Offset: 0x00044FB4
		[SecuritySafeCritical]
		private unsafe void WriteEscapedJsonString(string str)
		{
			fixed (string text = str)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				int num = 0;
				int i;
				for (i = 0; i < str.Length; i++)
				{
					char c = ptr[i];
					if (c <= '/')
					{
						if (c == '/' || c == '"')
						{
							this.nodeWriter.WriteChars(ptr + num, i - num);
							this.nodeWriter.WriteText(92);
							this.nodeWriter.WriteText((int)c);
							num = i + 1;
						}
						else if (c < ' ')
						{
							this.nodeWriter.WriteChars(ptr + num, i - num);
							this.nodeWriter.WriteText(92);
							if (XmlJsonWriter.CharacterAbbrevs[(int)c] == '\0')
							{
								this.nodeWriter.WriteText(117);
								this.nodeWriter.WriteText(string.Format(CultureInfo.InvariantCulture, "{0:x4}", (int)c));
								num = i + 1;
							}
							else
							{
								this.nodeWriter.WriteText((int)XmlJsonWriter.CharacterAbbrevs[(int)c]);
								num = i + 1;
							}
						}
					}
					else if (c == '\\')
					{
						this.nodeWriter.WriteChars(ptr + num, i - num);
						this.nodeWriter.WriteText(92);
						this.nodeWriter.WriteText((int)c);
						num = i + 1;
					}
					else if ((c >= '\ud800' && (c <= '\udfff' || c >= '￾')) || XmlJsonWriter.IsUnicodeNewlineCharacter(c))
					{
						this.nodeWriter.WriteChars(ptr + num, i - num);
						this.nodeWriter.WriteText(92);
						this.nodeWriter.WriteText(117);
						this.nodeWriter.WriteText(string.Format(CultureInfo.InvariantCulture, "{0:x4}", (int)c));
						num = i + 1;
					}
				}
				if (num < i)
				{
					this.nodeWriter.WriteChars(ptr + num, i - num);
				}
			}
		}

		// Token: 0x06001359 RID: 4953 RVA: 0x00046F94 File Offset: 0x00045194
		private void WriteIndent()
		{
			for (int i = 0; i < this.indentLevel; i++)
			{
				this.nodeWriter.WriteText(this.indentChars);
			}
		}

		// Token: 0x0600135A RID: 4954 RVA: 0x00046FC3 File Offset: 0x000451C3
		private void WriteNewLine()
		{
			this.nodeWriter.WriteText(13);
			this.nodeWriter.WriteText(10);
		}

		// Token: 0x0600135B RID: 4955 RVA: 0x00046FDF File Offset: 0x000451DF
		private void WriteJsonElementName(string localName)
		{
			this.WriteJsonQuote();
			this.WriteEscapedJsonString(localName);
			this.WriteJsonQuote();
			this.nodeWriter.WriteText(58);
			if (this.indent)
			{
				this.nodeWriter.WriteText(32);
			}
		}

		// Token: 0x0600135C RID: 4956 RVA: 0x00047016 File Offset: 0x00045216
		private void WriteJsonQuote()
		{
			this.nodeWriter.WriteText(34);
		}

		// Token: 0x0600135D RID: 4957 RVA: 0x00047028 File Offset: 0x00045228
		private void WritePrimitiveValue(object value)
		{
			if (this.IsClosed)
			{
				XmlJsonWriter.ThrowClosed();
			}
			if (value == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentNullException("value"));
			}
			if (value is ulong)
			{
				this.WriteValue((ulong)value);
				return;
			}
			if (value is string)
			{
				this.WriteValue((string)value);
				return;
			}
			if (value is int)
			{
				this.WriteValue((int)value);
				return;
			}
			if (value is long)
			{
				this.WriteValue((long)value);
				return;
			}
			if (value is bool)
			{
				this.WriteValue((bool)value);
				return;
			}
			if (value is double)
			{
				this.WriteValue((double)value);
				return;
			}
			if (value is DateTime)
			{
				this.WriteValue((DateTime)value);
				return;
			}
			if (value is float)
			{
				this.WriteValue((float)value);
				return;
			}
			if (value is decimal)
			{
				this.WriteValue((decimal)value);
				return;
			}
			if (value is XmlDictionaryString)
			{
				this.WriteValue((XmlDictionaryString)value);
				return;
			}
			if (value is UniqueId)
			{
				this.WriteValue((UniqueId)value);
				return;
			}
			if (value is Guid)
			{
				this.WriteValue((Guid)value);
				return;
			}
			if (value is TimeSpan)
			{
				this.WriteValue((TimeSpan)value);
				return;
			}
			if (value.GetType().IsArray)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new ArgumentException(SR.GetString("Nested array is not supported in JSON: '{0}'"), "value"));
			}
			base.WriteValue(value);
		}

		// Token: 0x0600135E RID: 4958 RVA: 0x00047194 File Offset: 0x00045394
		private void WriteServerTypeAttribute()
		{
			string value = this.serverTypeValue;
			XmlJsonWriter.JsonDataType jsonDataType = this.dataType;
			XmlJsonWriter.NameState nameState = this.nameState;
			base.WriteStartElement("__type");
			this.WriteValue(value);
			this.WriteEndElement();
			this.dataType = jsonDataType;
			this.nameState = nameState;
			this.wroteServerTypeAttribute = true;
		}

		// Token: 0x0600135F RID: 4959 RVA: 0x000471E3 File Offset: 0x000453E3
		private void WriteValue(ulong value)
		{
			this.StartText();
			this.nodeWriter.WriteUInt64Text(value);
		}

		// Token: 0x06001360 RID: 4960 RVA: 0x000471F8 File Offset: 0x000453F8
		private void WriteValue(Array array)
		{
			XmlJsonWriter.JsonDataType jsonDataType = this.dataType;
			this.dataType = XmlJsonWriter.JsonDataType.String;
			this.StartText();
			for (int i = 0; i < array.Length; i++)
			{
				if (i != 0)
				{
					this.nodeWriter.WriteText(32);
				}
				this.WritePrimitiveValue(array.GetValue(i));
			}
			this.dataType = jsonDataType;
		}

		// Token: 0x040009D6 RID: 2518
		private const char BACK_SLASH = '\\';

		// Token: 0x040009D7 RID: 2519
		private const char FORWARD_SLASH = '/';

		// Token: 0x040009D8 RID: 2520
		private const char HIGH_SURROGATE_START = '\ud800';

		// Token: 0x040009D9 RID: 2521
		private const char LOW_SURROGATE_END = '\udfff';

		// Token: 0x040009DA RID: 2522
		private const char MAX_CHAR = '￾';

		// Token: 0x040009DB RID: 2523
		private const char WHITESPACE = ' ';

		// Token: 0x040009DC RID: 2524
		private const char CARRIAGE_RETURN = '\r';

		// Token: 0x040009DD RID: 2525
		private const char NEWLINE = '\n';

		// Token: 0x040009DE RID: 2526
		private const char BACKSPACE = '\b';

		// Token: 0x040009DF RID: 2527
		private const char FORM_FEED = '\f';

		// Token: 0x040009E0 RID: 2528
		private const char HORIZONTAL_TABULATION = '\t';

		// Token: 0x040009E1 RID: 2529
		private const string xmlNamespace = "http://www.w3.org/XML/1998/namespace";

		// Token: 0x040009E2 RID: 2530
		private const string xmlnsNamespace = "http://www.w3.org/2000/xmlns/";

		// Token: 0x040009E3 RID: 2531
		[SecurityCritical]
		private static BinHexEncoding binHexEncoding;

		// Token: 0x040009E4 RID: 2532
		private static char[] CharacterAbbrevs;

		// Token: 0x040009E5 RID: 2533
		private string attributeText;

		// Token: 0x040009E6 RID: 2534
		private XmlJsonWriter.JsonDataType dataType;

		// Token: 0x040009E7 RID: 2535
		private int depth;

		// Token: 0x040009E8 RID: 2536
		private bool endElementBuffer;

		// Token: 0x040009E9 RID: 2537
		private bool isWritingDataTypeAttribute;

		// Token: 0x040009EA RID: 2538
		private bool isWritingServerTypeAttribute;

		// Token: 0x040009EB RID: 2539
		private bool isWritingXmlnsAttribute;

		// Token: 0x040009EC RID: 2540
		private bool isWritingXmlnsAttributeDefaultNs;

		// Token: 0x040009ED RID: 2541
		private XmlJsonWriter.NameState nameState;

		// Token: 0x040009EE RID: 2542
		private JsonNodeType nodeType;

		// Token: 0x040009EF RID: 2543
		private XmlJsonWriter.JsonNodeWriter nodeWriter;

		// Token: 0x040009F0 RID: 2544
		private JsonNodeType[] scopes;

		// Token: 0x040009F1 RID: 2545
		private string serverTypeValue;

		// Token: 0x040009F2 RID: 2546
		private WriteState writeState;

		// Token: 0x040009F3 RID: 2547
		private bool wroteServerTypeAttribute;

		// Token: 0x040009F4 RID: 2548
		private bool indent;

		// Token: 0x040009F5 RID: 2549
		private string indentChars;

		// Token: 0x040009F6 RID: 2550
		private int indentLevel;

		// Token: 0x02000188 RID: 392
		private enum JsonDataType
		{
			// Token: 0x040009F8 RID: 2552
			None,
			// Token: 0x040009F9 RID: 2553
			Null,
			// Token: 0x040009FA RID: 2554
			Boolean,
			// Token: 0x040009FB RID: 2555
			Number,
			// Token: 0x040009FC RID: 2556
			String,
			// Token: 0x040009FD RID: 2557
			Object,
			// Token: 0x040009FE RID: 2558
			Array
		}

		// Token: 0x02000189 RID: 393
		[Flags]
		private enum NameState
		{
			// Token: 0x04000A00 RID: 2560
			None = 0,
			// Token: 0x04000A01 RID: 2561
			IsWritingNameWithMapping = 1,
			// Token: 0x04000A02 RID: 2562
			IsWritingNameAttribute = 2,
			// Token: 0x04000A03 RID: 2563
			WrittenNameWithMapping = 4
		}

		// Token: 0x0200018A RID: 394
		private class JsonNodeWriter : XmlUTF8NodeWriter
		{
			// Token: 0x06001361 RID: 4961 RVA: 0x0004724E File Offset: 0x0004544E
			[SecurityCritical]
			internal unsafe void WriteChars(char* chars, int charCount)
			{
				base.UnsafeWriteUTF8Chars(chars, charCount);
			}

			// Token: 0x06001362 RID: 4962 RVA: 0x00047258 File Offset: 0x00045458
			public JsonNodeWriter()
			{
			}
		}
	}
}
