﻿using System;
using System.Security;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000169 RID: 361
	internal class JsonEnumDataContract : JsonDataContract
	{
		// Token: 0x06001251 RID: 4689 RVA: 0x000422C5 File Offset: 0x000404C5
		[SecuritySafeCritical]
		public JsonEnumDataContract(EnumDataContract traditionalDataContract) : base(new JsonEnumDataContract.JsonEnumDataContractCriticalHelper(traditionalDataContract))
		{
			this.helper = (base.Helper as JsonEnumDataContract.JsonEnumDataContractCriticalHelper);
		}

		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x06001252 RID: 4690 RVA: 0x000422E4 File Offset: 0x000404E4
		public bool IsULong
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.IsULong;
			}
		}

		// Token: 0x06001253 RID: 4691 RVA: 0x000422F4 File Offset: 0x000404F4
		public override object ReadJsonValueCore(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			object obj;
			if (this.IsULong)
			{
				obj = Enum.ToObject(base.TraditionalDataContract.UnderlyingType, jsonReader.ReadElementContentAsUnsignedLong());
			}
			else
			{
				obj = Enum.ToObject(base.TraditionalDataContract.UnderlyingType, jsonReader.ReadElementContentAsLong());
			}
			if (context != null)
			{
				context.AddNewObject(obj);
			}
			return obj;
		}

		// Token: 0x06001254 RID: 4692 RVA: 0x00042344 File Offset: 0x00040544
		public override void WriteJsonValueCore(XmlWriterDelegator jsonWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, RuntimeTypeHandle declaredTypeHandle)
		{
			if (this.IsULong)
			{
				jsonWriter.WriteUnsignedLong(((IConvertible)obj).ToUInt64(null));
				return;
			}
			jsonWriter.WriteLong(((IConvertible)obj).ToInt64(null));
		}

		// Token: 0x0400097B RID: 2427
		[SecurityCritical]
		private JsonEnumDataContract.JsonEnumDataContractCriticalHelper helper;

		// Token: 0x0200016A RID: 362
		private class JsonEnumDataContractCriticalHelper : JsonDataContract.JsonDataContractCriticalHelper
		{
			// Token: 0x06001255 RID: 4693 RVA: 0x00042373 File Offset: 0x00040573
			public JsonEnumDataContractCriticalHelper(EnumDataContract traditionalEnumDataContract) : base(traditionalEnumDataContract)
			{
				this.isULong = traditionalEnumDataContract.IsULong;
			}

			// Token: 0x170003FC RID: 1020
			// (get) Token: 0x06001256 RID: 4694 RVA: 0x00042388 File Offset: 0x00040588
			public bool IsULong
			{
				get
				{
					return this.isULong;
				}
			}

			// Token: 0x0400097C RID: 2428
			private bool isULong;
		}
	}
}
