﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000165 RID: 357
	internal class JsonDataContract
	{
		// Token: 0x06001217 RID: 4631 RVA: 0x000413B1 File Offset: 0x0003F5B1
		[SecuritySafeCritical]
		protected JsonDataContract(DataContract traditionalDataContract)
		{
			this.helper = new JsonDataContract.JsonDataContractCriticalHelper(traditionalDataContract);
		}

		// Token: 0x06001218 RID: 4632 RVA: 0x000413C5 File Offset: 0x0003F5C5
		[SecuritySafeCritical]
		protected JsonDataContract(JsonDataContract.JsonDataContractCriticalHelper helper)
		{
			this.helper = helper;
		}

		// Token: 0x170003EC RID: 1004
		// (get) Token: 0x06001219 RID: 4633 RVA: 0x0001BF2F File Offset: 0x0001A12F
		internal virtual string TypeName
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170003ED RID: 1005
		// (get) Token: 0x0600121A RID: 4634 RVA: 0x000413D4 File Offset: 0x0003F5D4
		protected JsonDataContract.JsonDataContractCriticalHelper Helper
		{
			[SecurityCritical]
			get
			{
				return this.helper;
			}
		}

		// Token: 0x170003EE RID: 1006
		// (get) Token: 0x0600121B RID: 4635 RVA: 0x000413DC File Offset: 0x0003F5DC
		protected DataContract TraditionalDataContract
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.TraditionalDataContract;
			}
		}

		// Token: 0x170003EF RID: 1007
		// (get) Token: 0x0600121C RID: 4636 RVA: 0x000413E9 File Offset: 0x0003F5E9
		private Dictionary<XmlQualifiedName, DataContract> KnownDataContracts
		{
			[SecuritySafeCritical]
			get
			{
				return this.helper.KnownDataContracts;
			}
		}

		// Token: 0x0600121D RID: 4637 RVA: 0x000413F6 File Offset: 0x0003F5F6
		[SecuritySafeCritical]
		public static JsonDataContract GetJsonDataContract(DataContract traditionalDataContract)
		{
			return JsonDataContract.JsonDataContractCriticalHelper.GetJsonDataContract(traditionalDataContract);
		}

		// Token: 0x0600121E RID: 4638 RVA: 0x000413FE File Offset: 0x0003F5FE
		public object ReadJsonValue(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			this.PushKnownDataContracts(context);
			object result = this.ReadJsonValueCore(jsonReader, context);
			this.PopKnownDataContracts(context);
			return result;
		}

		// Token: 0x0600121F RID: 4639 RVA: 0x00041416 File Offset: 0x0003F616
		public virtual object ReadJsonValueCore(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			return this.TraditionalDataContract.ReadXmlValue(jsonReader, context);
		}

		// Token: 0x06001220 RID: 4640 RVA: 0x00041425 File Offset: 0x0003F625
		public void WriteJsonValue(XmlWriterDelegator jsonWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, RuntimeTypeHandle declaredTypeHandle)
		{
			this.PushKnownDataContracts(context);
			this.WriteJsonValueCore(jsonWriter, obj, context, declaredTypeHandle);
			this.PopKnownDataContracts(context);
		}

		// Token: 0x06001221 RID: 4641 RVA: 0x00041440 File Offset: 0x0003F640
		public virtual void WriteJsonValueCore(XmlWriterDelegator jsonWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, RuntimeTypeHandle declaredTypeHandle)
		{
			this.TraditionalDataContract.WriteXmlValue(jsonWriter, obj, context);
		}

		// Token: 0x06001222 RID: 4642 RVA: 0x00041450 File Offset: 0x0003F650
		protected static object HandleReadValue(object obj, XmlObjectSerializerReadContext context)
		{
			context.AddNewObject(obj);
			return obj;
		}

		// Token: 0x06001223 RID: 4643 RVA: 0x0004145A File Offset: 0x0003F65A
		protected static bool TryReadNullAtTopLevel(XmlReaderDelegator reader)
		{
			if (!reader.MoveToAttribute("type") || !(reader.Value == "null"))
			{
				reader.MoveToElement();
				return false;
			}
			reader.Skip();
			reader.MoveToElement();
			return true;
		}

		// Token: 0x06001224 RID: 4644 RVA: 0x00041494 File Offset: 0x0003F694
		protected void PopKnownDataContracts(XmlObjectSerializerContext context)
		{
			if (this.KnownDataContracts != null)
			{
				context.scopedKnownTypes.Pop();
			}
		}

		// Token: 0x06001225 RID: 4645 RVA: 0x000414A9 File Offset: 0x0003F6A9
		protected void PushKnownDataContracts(XmlObjectSerializerContext context)
		{
			if (this.KnownDataContracts != null)
			{
				context.scopedKnownTypes.Push(this.KnownDataContracts);
			}
		}

		// Token: 0x0400095A RID: 2394
		[SecurityCritical]
		private JsonDataContract.JsonDataContractCriticalHelper helper;

		// Token: 0x02000166 RID: 358
		internal class JsonDataContractCriticalHelper
		{
			// Token: 0x06001226 RID: 4646 RVA: 0x000414C4 File Offset: 0x0003F6C4
			internal JsonDataContractCriticalHelper(DataContract traditionalDataContract)
			{
				this.traditionalDataContract = traditionalDataContract;
				this.AddCollectionItemContractsToKnownDataContracts();
				this.typeName = (string.IsNullOrEmpty(traditionalDataContract.Namespace.Value) ? traditionalDataContract.Name.Value : (traditionalDataContract.Name.Value + ":" + XmlObjectSerializerWriteContextComplexJson.TruncateDefaultDataContractNamespace(traditionalDataContract.Namespace.Value)));
			}

			// Token: 0x170003F0 RID: 1008
			// (get) Token: 0x06001227 RID: 4647 RVA: 0x0004152E File Offset: 0x0003F72E
			internal Dictionary<XmlQualifiedName, DataContract> KnownDataContracts
			{
				get
				{
					return this.knownDataContracts;
				}
			}

			// Token: 0x170003F1 RID: 1009
			// (get) Token: 0x06001228 RID: 4648 RVA: 0x00041536 File Offset: 0x0003F736
			internal DataContract TraditionalDataContract
			{
				get
				{
					return this.traditionalDataContract;
				}
			}

			// Token: 0x170003F2 RID: 1010
			// (get) Token: 0x06001229 RID: 4649 RVA: 0x0004153E File Offset: 0x0003F73E
			internal virtual string TypeName
			{
				get
				{
					return this.typeName;
				}
			}

			// Token: 0x0600122A RID: 4650 RVA: 0x00041548 File Offset: 0x0003F748
			public static JsonDataContract GetJsonDataContract(DataContract traditionalDataContract)
			{
				int id = JsonDataContract.JsonDataContractCriticalHelper.GetId(traditionalDataContract.UnderlyingType.TypeHandle);
				JsonDataContract jsonDataContract = JsonDataContract.JsonDataContractCriticalHelper.dataContractCache[id];
				if (jsonDataContract == null)
				{
					jsonDataContract = JsonDataContract.JsonDataContractCriticalHelper.CreateJsonDataContract(id, traditionalDataContract);
					JsonDataContract.JsonDataContractCriticalHelper.dataContractCache[id] = jsonDataContract;
				}
				return jsonDataContract;
			}

			// Token: 0x0600122B RID: 4651 RVA: 0x00041584 File Offset: 0x0003F784
			internal static int GetId(RuntimeTypeHandle typeHandle)
			{
				object obj = JsonDataContract.JsonDataContractCriticalHelper.cacheLock;
				int value;
				lock (obj)
				{
					JsonDataContract.JsonDataContractCriticalHelper.typeHandleRef.Value = typeHandle;
					IntRef intRef;
					if (!JsonDataContract.JsonDataContractCriticalHelper.typeToIDCache.TryGetValue(JsonDataContract.JsonDataContractCriticalHelper.typeHandleRef, out intRef))
					{
						int num = JsonDataContract.JsonDataContractCriticalHelper.dataContractID++;
						if (num >= JsonDataContract.JsonDataContractCriticalHelper.dataContractCache.Length)
						{
							int num2 = (num < 1073741823) ? (num * 2) : int.MaxValue;
							if (num2 <= num)
							{
								throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("An internal error has occurred. DataContract cache overflow.")));
							}
							Array.Resize<JsonDataContract>(ref JsonDataContract.JsonDataContractCriticalHelper.dataContractCache, num2);
						}
						intRef = new IntRef(num);
						try
						{
							JsonDataContract.JsonDataContractCriticalHelper.typeToIDCache.Add(new TypeHandleRef(typeHandle), intRef);
						}
						catch (Exception ex)
						{
							if (Fx.IsFatal(ex))
							{
								throw;
							}
							throw DiagnosticUtility.ExceptionUtility.ThrowHelperFatal(ex.Message, ex);
						}
					}
					value = intRef.Value;
				}
				return value;
			}

			// Token: 0x0600122C RID: 4652 RVA: 0x0004167C File Offset: 0x0003F87C
			private static JsonDataContract CreateJsonDataContract(int id, DataContract traditionalDataContract)
			{
				object obj = JsonDataContract.JsonDataContractCriticalHelper.createDataContractLock;
				JsonDataContract result;
				lock (obj)
				{
					JsonDataContract jsonDataContract = JsonDataContract.JsonDataContractCriticalHelper.dataContractCache[id];
					if (jsonDataContract == null)
					{
						Type type = traditionalDataContract.GetType();
						if (type == typeof(ObjectDataContract))
						{
							jsonDataContract = new JsonObjectDataContract(traditionalDataContract);
						}
						else if (type == typeof(StringDataContract))
						{
							jsonDataContract = new JsonStringDataContract((StringDataContract)traditionalDataContract);
						}
						else if (type == typeof(UriDataContract))
						{
							jsonDataContract = new JsonUriDataContract((UriDataContract)traditionalDataContract);
						}
						else if (type == typeof(QNameDataContract))
						{
							jsonDataContract = new JsonQNameDataContract((QNameDataContract)traditionalDataContract);
						}
						else if (type == typeof(ByteArrayDataContract))
						{
							jsonDataContract = new JsonByteArrayDataContract((ByteArrayDataContract)traditionalDataContract);
						}
						else if (traditionalDataContract.IsPrimitive || traditionalDataContract.UnderlyingType == Globals.TypeOfXmlQualifiedName)
						{
							jsonDataContract = new JsonDataContract(traditionalDataContract);
						}
						else if (type == typeof(ClassDataContract))
						{
							jsonDataContract = new JsonClassDataContract((ClassDataContract)traditionalDataContract);
						}
						else if (type == typeof(EnumDataContract))
						{
							jsonDataContract = new JsonEnumDataContract((EnumDataContract)traditionalDataContract);
						}
						else if (type == typeof(GenericParameterDataContract) || type == typeof(SpecialTypeDataContract))
						{
							jsonDataContract = new JsonDataContract(traditionalDataContract);
						}
						else if (type == typeof(CollectionDataContract))
						{
							jsonDataContract = new JsonCollectionDataContract((CollectionDataContract)traditionalDataContract);
						}
						else
						{
							if (!(type == typeof(XmlDataContract)))
							{
								throw DiagnosticUtility.ExceptionUtility.ThrowHelperArgument("traditionalDataContract", SR.GetString("Type '{0}' is not suppotred by DataContractJsonSerializer.", new object[]
								{
									traditionalDataContract.UnderlyingType
								}));
							}
							jsonDataContract = new JsonXmlDataContract((XmlDataContract)traditionalDataContract);
						}
					}
					result = jsonDataContract;
				}
				return result;
			}

			// Token: 0x0600122D RID: 4653 RVA: 0x00041884 File Offset: 0x0003FA84
			private void AddCollectionItemContractsToKnownDataContracts()
			{
				if (this.traditionalDataContract.KnownDataContracts != null)
				{
					foreach (KeyValuePair<XmlQualifiedName, DataContract> keyValuePair in this.traditionalDataContract.KnownDataContracts)
					{
						if (keyValuePair != null)
						{
							DataContract itemContract;
							for (CollectionDataContract collectionDataContract = keyValuePair.Value as CollectionDataContract; collectionDataContract != null; collectionDataContract = (itemContract as CollectionDataContract))
							{
								itemContract = collectionDataContract.ItemContract;
								if (this.knownDataContracts == null)
								{
									this.knownDataContracts = new Dictionary<XmlQualifiedName, DataContract>();
								}
								if (!this.knownDataContracts.ContainsKey(itemContract.StableName))
								{
									this.knownDataContracts.Add(itemContract.StableName, itemContract);
								}
								if (collectionDataContract.ItemType.IsGenericType && collectionDataContract.ItemType.GetGenericTypeDefinition() == typeof(KeyValue<, >))
								{
									DataContract dataContract = DataContract.GetDataContract(Globals.TypeOfKeyValuePair.MakeGenericType(collectionDataContract.ItemType.GetGenericArguments()));
									if (!this.knownDataContracts.ContainsKey(dataContract.StableName))
									{
										this.knownDataContracts.Add(dataContract.StableName, dataContract);
									}
								}
								if (!(itemContract is CollectionDataContract))
								{
									break;
								}
							}
						}
					}
				}
			}

			// Token: 0x0600122E RID: 4654 RVA: 0x000419CC File Offset: 0x0003FBCC
			// Note: this type is marked as 'beforefieldinit'.
			static JsonDataContractCriticalHelper()
			{
			}

			// Token: 0x0400095B RID: 2395
			private static object cacheLock = new object();

			// Token: 0x0400095C RID: 2396
			private static object createDataContractLock = new object();

			// Token: 0x0400095D RID: 2397
			private static JsonDataContract[] dataContractCache = new JsonDataContract[32];

			// Token: 0x0400095E RID: 2398
			private static int dataContractID = 0;

			// Token: 0x0400095F RID: 2399
			private static TypeHandleRef typeHandleRef = new TypeHandleRef();

			// Token: 0x04000960 RID: 2400
			private static Dictionary<TypeHandleRef, IntRef> typeToIDCache = new Dictionary<TypeHandleRef, IntRef>(new TypeHandleRefEqualityComparer());

			// Token: 0x04000961 RID: 2401
			private Dictionary<XmlQualifiedName, DataContract> knownDataContracts;

			// Token: 0x04000962 RID: 2402
			private DataContract traditionalDataContract;

			// Token: 0x04000963 RID: 2403
			private string typeName;
		}
	}
}
