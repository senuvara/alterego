﻿using System;
using System.Globalization;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200017D RID: 381
	internal class JsonReaderDelegator : XmlReaderDelegator
	{
		// Token: 0x0600128B RID: 4747 RVA: 0x00042973 File Offset: 0x00040B73
		public JsonReaderDelegator(XmlReader reader) : base(reader)
		{
		}

		// Token: 0x0600128C RID: 4748 RVA: 0x0004297C File Offset: 0x00040B7C
		public JsonReaderDelegator(XmlReader reader, DateTimeFormat dateTimeFormat) : this(reader)
		{
			this.dateTimeFormat = dateTimeFormat;
		}

		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x0600128D RID: 4749 RVA: 0x0004298C File Offset: 0x00040B8C
		internal XmlDictionaryReaderQuotas ReaderQuotas
		{
			get
			{
				if (this.dictionaryReader == null)
				{
					return null;
				}
				return this.dictionaryReader.Quotas;
			}
		}

		// Token: 0x170003FE RID: 1022
		// (get) Token: 0x0600128E RID: 4750 RVA: 0x000429A3 File Offset: 0x00040BA3
		private JsonReaderDelegator.DateTimeArrayJsonHelperWithString DateTimeArrayHelper
		{
			get
			{
				if (this.dateTimeArrayHelper == null)
				{
					this.dateTimeArrayHelper = new JsonReaderDelegator.DateTimeArrayJsonHelperWithString(this.dateTimeFormat);
				}
				return this.dateTimeArrayHelper;
			}
		}

		// Token: 0x0600128F RID: 4751 RVA: 0x000429C4 File Offset: 0x00040BC4
		internal static XmlQualifiedName ParseQualifiedName(string qname)
		{
			string name;
			string ns;
			if (string.IsNullOrEmpty(qname))
			{
				ns = (name = string.Empty);
			}
			else
			{
				qname = qname.Trim();
				int num = qname.IndexOf(':');
				if (num >= 0)
				{
					name = qname.Substring(0, num);
					ns = qname.Substring(num + 1);
				}
				else
				{
					name = qname;
					ns = string.Empty;
				}
			}
			return new XmlQualifiedName(name, ns);
		}

		// Token: 0x06001290 RID: 4752 RVA: 0x00042A1C File Offset: 0x00040C1C
		internal override char ReadContentAsChar()
		{
			return XmlConvert.ToChar(base.ReadContentAsString());
		}

		// Token: 0x06001291 RID: 4753 RVA: 0x00042A29 File Offset: 0x00040C29
		internal override XmlQualifiedName ReadContentAsQName()
		{
			return JsonReaderDelegator.ParseQualifiedName(base.ReadContentAsString());
		}

		// Token: 0x06001292 RID: 4754 RVA: 0x00042A36 File Offset: 0x00040C36
		internal override char ReadElementContentAsChar()
		{
			return XmlConvert.ToChar(base.ReadElementContentAsString());
		}

		// Token: 0x06001293 RID: 4755 RVA: 0x00042A44 File Offset: 0x00040C44
		internal override byte[] ReadContentAsBase64()
		{
			if (this.isEndOfEmptyElement)
			{
				return new byte[0];
			}
			byte[] result;
			if (this.dictionaryReader == null)
			{
				XmlDictionaryReader xmlDictionaryReader = XmlDictionaryReader.CreateDictionaryReader(this.reader);
				result = ByteArrayHelperWithString.Instance.ReadArray(xmlDictionaryReader, "item", string.Empty, xmlDictionaryReader.Quotas.MaxArrayLength);
			}
			else
			{
				result = ByteArrayHelperWithString.Instance.ReadArray(this.dictionaryReader, "item", string.Empty, this.dictionaryReader.Quotas.MaxArrayLength);
			}
			return result;
		}

		// Token: 0x06001294 RID: 4756 RVA: 0x00042AC4 File Offset: 0x00040CC4
		internal override byte[] ReadElementContentAsBase64()
		{
			if (this.isEndOfEmptyElement)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Start element expected. Found {0}.", new object[]
				{
					"EndElement"
				})));
			}
			byte[] result;
			if (this.reader.IsStartElement() && this.reader.IsEmptyElement)
			{
				this.reader.Read();
				result = new byte[0];
			}
			else
			{
				this.reader.ReadStartElement();
				result = this.ReadContentAsBase64();
				this.reader.ReadEndElement();
			}
			return result;
		}

		// Token: 0x06001295 RID: 4757 RVA: 0x00042B4C File Offset: 0x00040D4C
		internal override DateTime ReadContentAsDateTime()
		{
			return JsonReaderDelegator.ParseJsonDate(base.ReadContentAsString(), this.dateTimeFormat);
		}

		// Token: 0x06001296 RID: 4758 RVA: 0x00042B5F File Offset: 0x00040D5F
		internal static DateTime ParseJsonDate(string originalDateTimeValue, DateTimeFormat dateTimeFormat)
		{
			if (dateTimeFormat == null)
			{
				return JsonReaderDelegator.ParseJsonDateInDefaultFormat(originalDateTimeValue);
			}
			return DateTime.ParseExact(originalDateTimeValue, dateTimeFormat.FormatString, dateTimeFormat.FormatProvider, dateTimeFormat.DateTimeStyles);
		}

		// Token: 0x06001297 RID: 4759 RVA: 0x00042B84 File Offset: 0x00040D84
		internal static DateTime ParseJsonDateInDefaultFormat(string originalDateTimeValue)
		{
			string text;
			if (!string.IsNullOrEmpty(originalDateTimeValue))
			{
				text = originalDateTimeValue.Trim();
			}
			else
			{
				text = originalDateTimeValue;
			}
			if (string.IsNullOrEmpty(text) || !text.StartsWith("/Date(", StringComparison.Ordinal) || !text.EndsWith(")/", StringComparison.Ordinal))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new FormatException(SR.GetString("Invalid JSON dateTime string is specified: original value '{0}', start guide writer: {1}, end guard writer: {2}.", new object[]
				{
					originalDateTimeValue,
					"\\/Date(",
					")\\/"
				})));
			}
			string text2 = text.Substring(6, text.Length - 8);
			DateTimeKind dateTimeKind = DateTimeKind.Utc;
			int num = text2.IndexOf('+', 1);
			if (num == -1)
			{
				num = text2.IndexOf('-', 1);
			}
			if (num != -1)
			{
				dateTimeKind = DateTimeKind.Local;
				text2 = text2.Substring(0, num);
			}
			long num2;
			try
			{
				num2 = long.Parse(text2, CultureInfo.InvariantCulture);
			}
			catch (ArgumentException exception)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text2, "Int64", exception));
			}
			catch (FormatException exception2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text2, "Int64", exception2));
			}
			catch (OverflowException exception3)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text2, "Int64", exception3));
			}
			long ticks = num2 * 10000L + JsonGlobals.unixEpochTicks;
			DateTime result;
			try
			{
				DateTime dateTime = new DateTime(ticks, DateTimeKind.Utc);
				switch (dateTimeKind)
				{
				case DateTimeKind.Unspecified:
					return DateTime.SpecifyKind(dateTime.ToLocalTime(), DateTimeKind.Unspecified);
				case DateTimeKind.Local:
					return dateTime.ToLocalTime();
				}
				result = dateTime;
			}
			catch (ArgumentException exception4)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text2, "DateTime", exception4));
			}
			return result;
		}

		// Token: 0x06001298 RID: 4760 RVA: 0x00042D20 File Offset: 0x00040F20
		internal override DateTime ReadElementContentAsDateTime()
		{
			return JsonReaderDelegator.ParseJsonDate(base.ReadElementContentAsString(), this.dateTimeFormat);
		}

		// Token: 0x06001299 RID: 4761 RVA: 0x00042D34 File Offset: 0x00040F34
		internal bool TryReadJsonDateTimeArray(XmlObjectSerializerReadContext context, XmlDictionaryString itemName, XmlDictionaryString itemNamespace, int arrayLength, out DateTime[] array)
		{
			if (this.dictionaryReader == null || arrayLength != -1)
			{
				array = null;
				return false;
			}
			array = this.DateTimeArrayHelper.ReadArray(this.dictionaryReader, XmlDictionaryString.GetString(itemName), XmlDictionaryString.GetString(itemNamespace), base.GetArrayLengthQuota(context));
			context.IncrementItemCount(array.Length);
			return true;
		}

		// Token: 0x0600129A RID: 4762 RVA: 0x00042D88 File Offset: 0x00040F88
		internal override ulong ReadContentAsUnsignedLong()
		{
			string text = this.reader.ReadContentAsString();
			if (text == null || text.Length == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(XmlObjectSerializer.TryAddLineInfo(this, SR.GetString("The value '{0}' cannot be parsed as the type '{1}'.", new object[]
				{
					text,
					"UInt64"
				}))));
			}
			ulong result;
			try
			{
				result = ulong.Parse(text, NumberStyles.Float, NumberFormatInfo.InvariantInfo);
			}
			catch (ArgumentException exception)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "UInt64", exception));
			}
			catch (FormatException exception2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "UInt64", exception2));
			}
			catch (OverflowException exception3)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "UInt64", exception3));
			}
			return result;
		}

		// Token: 0x0600129B RID: 4763 RVA: 0x00042E54 File Offset: 0x00041054
		internal override ulong ReadElementContentAsUnsignedLong()
		{
			if (this.isEndOfEmptyElement)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("Start element expected. Found {0}.", new object[]
				{
					"EndElement"
				})));
			}
			string text = this.reader.ReadElementContentAsString();
			if (text == null || text.Length == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(XmlObjectSerializer.TryAddLineInfo(this, SR.GetString("The value '{0}' cannot be parsed as the type '{1}'.", new object[]
				{
					text,
					"UInt64"
				}))));
			}
			ulong result;
			try
			{
				result = ulong.Parse(text, NumberStyles.Float, NumberFormatInfo.InvariantInfo);
			}
			catch (ArgumentException exception)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "UInt64", exception));
			}
			catch (FormatException exception2)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "UInt64", exception2));
			}
			catch (OverflowException exception3)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlExceptionHelper.CreateConversionException(text, "UInt64", exception3));
			}
			return result;
		}

		// Token: 0x040009BF RID: 2495
		private DateTimeFormat dateTimeFormat;

		// Token: 0x040009C0 RID: 2496
		private JsonReaderDelegator.DateTimeArrayJsonHelperWithString dateTimeArrayHelper;

		// Token: 0x0200017E RID: 382
		private class DateTimeArrayJsonHelperWithString : ArrayHelper<string, DateTime>
		{
			// Token: 0x0600129C RID: 4764 RVA: 0x00042F48 File Offset: 0x00041148
			public DateTimeArrayJsonHelperWithString(DateTimeFormat dateTimeFormat)
			{
				this.dateTimeFormat = dateTimeFormat;
			}

			// Token: 0x0600129D RID: 4765 RVA: 0x00042F58 File Offset: 0x00041158
			protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, DateTime[] array, int offset, int count)
			{
				XmlJsonReader.CheckArray(array, offset, count);
				int num = 0;
				while (num < count && reader.IsStartElement("item", string.Empty))
				{
					array[offset + num] = JsonReaderDelegator.ParseJsonDate(reader.ReadElementContentAsString(), this.dateTimeFormat);
					num++;
				}
				return num;
			}

			// Token: 0x0600129E RID: 4766 RVA: 0x00042FAC File Offset: 0x000411AC
			protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, DateTime[] array, int offset, int count)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new NotImplementedException());
			}

			// Token: 0x040009C1 RID: 2497
			private DateTimeFormat dateTimeFormat;
		}
	}
}
