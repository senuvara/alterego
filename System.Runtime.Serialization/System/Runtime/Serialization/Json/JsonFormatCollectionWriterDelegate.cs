﻿using System;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000174 RID: 372
	// (Invoke) Token: 0x06001276 RID: 4726
	internal delegate void JsonFormatCollectionWriterDelegate(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, CollectionDataContract dataContract);
}
