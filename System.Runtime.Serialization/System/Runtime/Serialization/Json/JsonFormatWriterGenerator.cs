﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Diagnostics.Application;
using System.Security;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000175 RID: 373
	internal class JsonFormatWriterGenerator
	{
		// Token: 0x06001279 RID: 4729 RVA: 0x00042542 File Offset: 0x00040742
		[SecurityCritical]
		public JsonFormatWriterGenerator()
		{
			this.helper = new JsonFormatWriterGenerator.CriticalHelper();
		}

		// Token: 0x0600127A RID: 4730 RVA: 0x00042558 File Offset: 0x00040758
		[SecurityCritical]
		internal JsonFormatClassWriterDelegate GenerateClassWriter(ClassDataContract classContract)
		{
			JsonFormatClassWriterDelegate result;
			try
			{
				if (TD.DCJsonGenWriterStartIsEnabled())
				{
					TD.DCJsonGenWriterStart("Class", classContract.UnderlyingType.FullName);
				}
				result = this.helper.GenerateClassWriter(classContract);
			}
			finally
			{
				if (TD.DCJsonGenWriterStopIsEnabled())
				{
					TD.DCJsonGenWriterStop();
				}
			}
			return result;
		}

		// Token: 0x0600127B RID: 4731 RVA: 0x000425B0 File Offset: 0x000407B0
		[SecurityCritical]
		internal JsonFormatCollectionWriterDelegate GenerateCollectionWriter(CollectionDataContract collectionContract)
		{
			JsonFormatCollectionWriterDelegate result;
			try
			{
				if (TD.DCJsonGenWriterStartIsEnabled())
				{
					TD.DCJsonGenWriterStart("Collection", collectionContract.UnderlyingType.FullName);
				}
				result = this.helper.GenerateCollectionWriter(collectionContract);
			}
			finally
			{
				if (TD.DCJsonGenWriterStopIsEnabled())
				{
					TD.DCJsonGenWriterStop();
				}
			}
			return result;
		}

		// Token: 0x04000981 RID: 2433
		[SecurityCritical]
		private JsonFormatWriterGenerator.CriticalHelper helper;

		// Token: 0x02000176 RID: 374
		private class CriticalHelper
		{
			// Token: 0x0600127C RID: 4732 RVA: 0x00042608 File Offset: 0x00040808
			internal JsonFormatClassWriterDelegate GenerateClassWriter(ClassDataContract classContract)
			{
				return delegate(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, ClassDataContract dataContract, XmlDictionaryString[] memberNames)
				{
					new JsonFormatWriterInterpreter(classContract).WriteToJson(xmlWriter, obj, context, dataContract, memberNames);
				};
			}

			// Token: 0x0600127D RID: 4733 RVA: 0x00042621 File Offset: 0x00040821
			internal JsonFormatCollectionWriterDelegate GenerateCollectionWriter(CollectionDataContract collectionContract)
			{
				return delegate(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, CollectionDataContract dataContract)
				{
					new JsonFormatWriterInterpreter(collectionContract).WriteCollectionToJson(xmlWriter, obj, context, dataContract);
				};
			}

			// Token: 0x0600127E RID: 4734 RVA: 0x00002217 File Offset: 0x00000417
			public CriticalHelper()
			{
			}

			// Token: 0x02000177 RID: 375
			[CompilerGenerated]
			private sealed class <>c__DisplayClass0_0
			{
				// Token: 0x0600127F RID: 4735 RVA: 0x00002217 File Offset: 0x00000417
				public <>c__DisplayClass0_0()
				{
				}

				// Token: 0x06001280 RID: 4736 RVA: 0x0004263A File Offset: 0x0004083A
				internal void <GenerateClassWriter>b__0(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, ClassDataContract dataContract, XmlDictionaryString[] memberNames)
				{
					new JsonFormatWriterInterpreter(this.classContract).WriteToJson(xmlWriter, obj, context, dataContract, memberNames);
				}

				// Token: 0x04000982 RID: 2434
				public ClassDataContract classContract;
			}

			// Token: 0x02000178 RID: 376
			[CompilerGenerated]
			private sealed class <>c__DisplayClass1_0
			{
				// Token: 0x06001281 RID: 4737 RVA: 0x00002217 File Offset: 0x00000417
				public <>c__DisplayClass1_0()
				{
				}

				// Token: 0x06001282 RID: 4738 RVA: 0x00042653 File Offset: 0x00040853
				internal void <GenerateCollectionWriter>b__0(XmlWriterDelegator xmlWriter, object obj, XmlObjectSerializerWriteContextComplexJson context, CollectionDataContract dataContract)
				{
					new JsonFormatWriterInterpreter(this.collectionContract).WriteCollectionToJson(xmlWriter, obj, context, dataContract);
				}

				// Token: 0x04000983 RID: 2435
				public CollectionDataContract collectionContract;
			}
		}
	}
}
