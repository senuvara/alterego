﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Runtime.Serialization.Json
{
	/// <summary>Specifies <see cref="T:System.Runtime.Serialization.Json.DataContractJsonSerializer" /> settings.</summary>
	// Token: 0x0200015D RID: 349
	public class DataContractJsonSerializerSettings
	{
		/// <summary>Gets or sets the root name of the selected object.</summary>
		/// <returns>The root name of the selected object.</returns>
		// Token: 0x170003D2 RID: 978
		// (get) Token: 0x060011E0 RID: 4576 RVA: 0x00040C98 File Offset: 0x0003EE98
		// (set) Token: 0x060011E1 RID: 4577 RVA: 0x00040CA0 File Offset: 0x0003EEA0
		public string RootName
		{
			[CompilerGenerated]
			get
			{
				return this.<RootName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RootName>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a collection of types that may be present in the object graph serialized using this instance the DataContractJsonSerializerSettings.</summary>
		/// <returns>A collection of types that may be present in the object graph serialized using this instance the DataContractJsonSerializerSettings.</returns>
		// Token: 0x170003D3 RID: 979
		// (get) Token: 0x060011E2 RID: 4578 RVA: 0x00040CA9 File Offset: 0x0003EEA9
		// (set) Token: 0x060011E3 RID: 4579 RVA: 0x00040CB1 File Offset: 0x0003EEB1
		public IEnumerable<Type> KnownTypes
		{
			[CompilerGenerated]
			get
			{
				return this.<KnownTypes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<KnownTypes>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the maximum number of items in an object graph to serialize or deserialize.</summary>
		/// <returns>The maximum number of items in an object graph to serialize or deserialize.</returns>
		// Token: 0x170003D4 RID: 980
		// (get) Token: 0x060011E4 RID: 4580 RVA: 0x00040CBA File Offset: 0x0003EEBA
		// (set) Token: 0x060011E5 RID: 4581 RVA: 0x00040CC2 File Offset: 0x0003EEC2
		public int MaxItemsInObjectGraph
		{
			get
			{
				return this.maxItemsInObjectGraph;
			}
			set
			{
				this.maxItemsInObjectGraph = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether to ignore data supplied by an extension of the class when the class is being serialized or deserialized.</summary>
		/// <returns>
		///     <see langword="True" /> to ignore data supplied by an extension of the class when the class is being serialized or deserialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x170003D5 RID: 981
		// (get) Token: 0x060011E6 RID: 4582 RVA: 0x00040CCB File Offset: 0x0003EECB
		// (set) Token: 0x060011E7 RID: 4583 RVA: 0x00040CD3 File Offset: 0x0003EED3
		public bool IgnoreExtensionDataObject
		{
			[CompilerGenerated]
			get
			{
				return this.<IgnoreExtensionDataObject>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IgnoreExtensionDataObject>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a surrogate type that is currently active for given IDataContractSurrogate instance.</summary>
		/// <returns>The surrogate type that is currently active for given IDataContractSurrogate instance.</returns>
		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x060011E8 RID: 4584 RVA: 0x00040CDC File Offset: 0x0003EEDC
		// (set) Token: 0x060011E9 RID: 4585 RVA: 0x00040CE4 File Offset: 0x0003EEE4
		public IDataContractSurrogate DataContractSurrogate
		{
			[CompilerGenerated]
			get
			{
				return this.<DataContractSurrogate>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<DataContractSurrogate>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the data contract JSON serializer settings to emit type information.</summary>
		/// <returns>The data contract JSON serializer settings to emit type information.</returns>
		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x060011EA RID: 4586 RVA: 0x00040CED File Offset: 0x0003EEED
		// (set) Token: 0x060011EB RID: 4587 RVA: 0x00040CF5 File Offset: 0x0003EEF5
		public EmitTypeInformation EmitTypeInformation
		{
			[CompilerGenerated]
			get
			{
				return this.<EmitTypeInformation>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<EmitTypeInformation>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a DateTimeFormat that defines the culturally appropriate format of displaying dates and times.</summary>
		/// <returns>The DateTimeFormat that defines the culturally appropriate format of displaying dates and times.</returns>
		// Token: 0x170003D8 RID: 984
		// (get) Token: 0x060011EC RID: 4588 RVA: 0x00040CFE File Offset: 0x0003EEFE
		// (set) Token: 0x060011ED RID: 4589 RVA: 0x00040D06 File Offset: 0x0003EF06
		public DateTimeFormat DateTimeFormat
		{
			[CompilerGenerated]
			get
			{
				return this.<DateTimeFormat>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<DateTimeFormat>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether to serialize read only types.</summary>
		/// <returns>
		///     <see langword="True" /> to serialize read only types; otherwise <see langword="false" />.</returns>
		// Token: 0x170003D9 RID: 985
		// (get) Token: 0x060011EE RID: 4590 RVA: 0x00040D0F File Offset: 0x0003EF0F
		// (set) Token: 0x060011EF RID: 4591 RVA: 0x00040D17 File Offset: 0x0003EF17
		public bool SerializeReadOnlyTypes
		{
			[CompilerGenerated]
			get
			{
				return this.<SerializeReadOnlyTypes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SerializeReadOnlyTypes>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether to use a simple dictionary format.</summary>
		/// <returns>
		///     <see langword="True" /> to use a simple dictionary format; otherwise, <see langword="false" />.</returns>
		// Token: 0x170003DA RID: 986
		// (get) Token: 0x060011F0 RID: 4592 RVA: 0x00040D20 File Offset: 0x0003EF20
		// (set) Token: 0x060011F1 RID: 4593 RVA: 0x00040D28 File Offset: 0x0003EF28
		public bool UseSimpleDictionaryFormat
		{
			[CompilerGenerated]
			get
			{
				return this.<UseSimpleDictionaryFormat>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<UseSimpleDictionaryFormat>k__BackingField = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Json.DataContractJsonSerializerSettings" /> class.</summary>
		// Token: 0x060011F2 RID: 4594 RVA: 0x00040D31 File Offset: 0x0003EF31
		public DataContractJsonSerializerSettings()
		{
		}

		// Token: 0x04000946 RID: 2374
		private int maxItemsInObjectGraph = int.MaxValue;

		// Token: 0x04000947 RID: 2375
		[CompilerGenerated]
		private string <RootName>k__BackingField;

		// Token: 0x04000948 RID: 2376
		[CompilerGenerated]
		private IEnumerable<Type> <KnownTypes>k__BackingField;

		// Token: 0x04000949 RID: 2377
		[CompilerGenerated]
		private bool <IgnoreExtensionDataObject>k__BackingField;

		// Token: 0x0400094A RID: 2378
		[CompilerGenerated]
		private IDataContractSurrogate <DataContractSurrogate>k__BackingField;

		// Token: 0x0400094B RID: 2379
		[CompilerGenerated]
		private EmitTypeInformation <EmitTypeInformation>k__BackingField;

		// Token: 0x0400094C RID: 2380
		[CompilerGenerated]
		private DateTimeFormat <DateTimeFormat>k__BackingField;

		// Token: 0x0400094D RID: 2381
		[CompilerGenerated]
		private bool <SerializeReadOnlyTypes>k__BackingField;

		// Token: 0x0400094E RID: 2382
		[CompilerGenerated]
		private bool <UseSimpleDictionaryFormat>k__BackingField;
	}
}
