﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200018D RID: 397
	internal class JsonFormatReaderInterpreter
	{
		// Token: 0x06001397 RID: 5015 RVA: 0x00047F7F File Offset: 0x0004617F
		public JsonFormatReaderInterpreter(ClassDataContract classContract)
		{
			this.classContract = classContract;
		}

		// Token: 0x06001398 RID: 5016 RVA: 0x00047F8E File Offset: 0x0004618E
		public JsonFormatReaderInterpreter(CollectionDataContract collectionContract, bool isGetOnly)
		{
			this.collectionContract = collectionContract;
			this.is_get_only_collection = isGetOnly;
		}

		// Token: 0x06001399 RID: 5017 RVA: 0x00047FA4 File Offset: 0x000461A4
		public object ReadFromJson(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContextComplexJson context, XmlDictionaryString emptyDictionaryString, XmlDictionaryString[] memberNames)
		{
			this.xmlReader = xmlReader;
			this.context = context;
			this.emptyDictionaryString = emptyDictionaryString;
			this.memberNames = memberNames;
			this.CreateObject(this.classContract);
			context.AddNewObject(this.objectLocal);
			this.InvokeOnDeserializing(this.classContract);
			if (this.classContract.IsISerializable)
			{
				this.ReadISerializable(this.classContract);
			}
			else
			{
				this.ReadClass(this.classContract);
			}
			if (Globals.TypeOfIDeserializationCallback.IsAssignableFrom(this.classContract.UnderlyingType))
			{
				((IDeserializationCallback)this.objectLocal).OnDeserialization(null);
			}
			this.InvokeOnDeserialized(this.classContract);
			if (!this.InvokeFactoryMethod(this.classContract) && this.classContract.UnderlyingType == Globals.TypeOfDateTimeOffsetAdapter)
			{
				this.objectLocal = DateTimeOffsetAdapter.GetDateTimeOffset((DateTimeOffsetAdapter)this.objectLocal);
			}
			return this.objectLocal;
		}

		// Token: 0x0600139A RID: 5018 RVA: 0x00048093 File Offset: 0x00046293
		public object ReadCollectionFromJson(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContextComplexJson context, XmlDictionaryString emptyDictionaryString, XmlDictionaryString itemName, CollectionDataContract collectionContract)
		{
			this.xmlReader = xmlReader;
			this.context = context;
			this.emptyDictionaryString = emptyDictionaryString;
			this.itemName = itemName;
			this.collectionContract = collectionContract;
			this.ReadCollection(collectionContract);
			return this.objectLocal;
		}

		// Token: 0x0600139B RID: 5019 RVA: 0x000480C8 File Offset: 0x000462C8
		public void ReadGetOnlyCollectionFromJson(XmlReaderDelegator xmlReader, XmlObjectSerializerReadContextComplexJson context, XmlDictionaryString emptyDictionaryString, XmlDictionaryString itemName, CollectionDataContract collectionContract)
		{
			this.xmlReader = xmlReader;
			this.context = context;
			this.emptyDictionaryString = emptyDictionaryString;
			this.itemName = itemName;
			this.collectionContract = collectionContract;
			this.ReadGetOnlyCollection(collectionContract);
		}

		// Token: 0x0600139C RID: 5020 RVA: 0x000480F8 File Offset: 0x000462F8
		private void CreateObject(ClassDataContract classContract)
		{
			Type type = this.objectType = classContract.UnderlyingType;
			if (type.IsValueType && !classContract.IsNonAttributedType)
			{
				type = Globals.TypeOfValueType;
			}
			if (classContract.UnderlyingType == Globals.TypeOfDBNull)
			{
				this.objectLocal = DBNull.Value;
				return;
			}
			if (!classContract.IsNonAttributedType)
			{
				this.objectLocal = CodeInterpreter.ConvertValue(XmlFormatReaderGenerator.UnsafeGetUninitializedObject(DataContract.GetIdForInitialization(classContract)), Globals.TypeOfObject, type);
				return;
			}
			if (type.IsValueType)
			{
				this.objectLocal = FormatterServices.GetUninitializedObject(type);
				return;
			}
			this.objectLocal = classContract.GetNonAttributedTypeConstructor().Invoke(new object[0]);
		}

		// Token: 0x0600139D RID: 5021 RVA: 0x0004819C File Offset: 0x0004639C
		private void InvokeOnDeserializing(ClassDataContract classContract)
		{
			if (classContract.BaseContract != null)
			{
				this.InvokeOnDeserializing(classContract.BaseContract);
			}
			if (classContract.OnDeserializing != null)
			{
				classContract.OnDeserializing.Invoke(this.objectLocal, new object[]
				{
					this.context.GetStreamingContext()
				});
			}
		}

		// Token: 0x0600139E RID: 5022 RVA: 0x000481F8 File Offset: 0x000463F8
		private void InvokeOnDeserialized(ClassDataContract classContract)
		{
			if (classContract.BaseContract != null)
			{
				this.InvokeOnDeserialized(classContract.BaseContract);
			}
			if (classContract.OnDeserialized != null)
			{
				classContract.OnDeserialized.Invoke(this.objectLocal, new object[]
				{
					this.context.GetStreamingContext()
				});
			}
		}

		// Token: 0x0600139F RID: 5023 RVA: 0x0003E3D2 File Offset: 0x0003C5D2
		private bool HasFactoryMethod(ClassDataContract classContract)
		{
			return Globals.TypeOfIObjectReference.IsAssignableFrom(classContract.UnderlyingType);
		}

		// Token: 0x060013A0 RID: 5024 RVA: 0x00048252 File Offset: 0x00046452
		private bool InvokeFactoryMethod(ClassDataContract classContract)
		{
			if (this.HasFactoryMethod(classContract))
			{
				this.objectLocal = CodeInterpreter.ConvertValue(this.context.GetRealObject((IObjectReference)this.objectLocal, Globals.NewObjectId), Globals.TypeOfObject, classContract.UnderlyingType);
				return true;
			}
			return false;
		}

		// Token: 0x060013A1 RID: 5025 RVA: 0x00048294 File Offset: 0x00046494
		private void ReadISerializable(ClassDataContract classContract)
		{
			ConstructorInfo constructor = classContract.UnderlyingType.GetConstructor(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, JsonFormatGeneratorStatics.SerInfoCtorArgs, null);
			if (constructor == null)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Constructor that takes SerializationInfo and StreamingContext is not found for '{0}'.", new object[]
				{
					DataContract.GetClrTypeFullName(classContract.UnderlyingType)
				})));
			}
			this.context.ReadSerializationInfo(this.xmlReader, classContract.UnderlyingType);
			constructor.Invoke(this.objectLocal, new object[]
			{
				this.context.GetStreamingContext()
			});
		}

		// Token: 0x060013A2 RID: 5026 RVA: 0x00048328 File Offset: 0x00046528
		private void ReadClass(ClassDataContract classContract)
		{
			if (classContract.HasExtensionData)
			{
				ExtensionDataObject extensionDataObject = new ExtensionDataObject();
				this.ReadMembers(classContract, extensionDataObject);
				for (ClassDataContract classDataContract = classContract; classDataContract != null; classDataContract = classDataContract.BaseContract)
				{
					MethodInfo extensionDataSetMethod = classDataContract.ExtensionDataSetMethod;
					if (extensionDataSetMethod != null)
					{
						extensionDataSetMethod.Invoke(this.objectLocal, new object[]
						{
							extensionDataObject
						});
					}
				}
				return;
			}
			this.ReadMembers(classContract, null);
		}

		// Token: 0x060013A3 RID: 5027 RVA: 0x0004838C File Offset: 0x0004658C
		private void ReadMembers(ClassDataContract classContract, ExtensionDataObject extensionData)
		{
			int num = classContract.MemberNames.Length;
			this.context.IncrementItemCount(num);
			int memberIndex = -1;
			BitFlagsGenerator bitFlagsGenerator = new BitFlagsGenerator(num);
			byte[] requiredElements = new byte[bitFlagsGenerator.GetLocalCount()];
			this.SetRequiredElements(classContract, requiredElements);
			this.SetExpectedElements(bitFlagsGenerator, 0);
			while (XmlObjectSerializerReadContext.MoveToNextElement(this.xmlReader))
			{
				int jsonMemberIndex = this.context.GetJsonMemberIndex(this.xmlReader, this.memberNames, memberIndex, extensionData);
				if (num > 0)
				{
					this.ReadMembers(jsonMemberIndex, classContract, bitFlagsGenerator, ref memberIndex);
				}
			}
			if (!this.CheckRequiredElements(bitFlagsGenerator, requiredElements))
			{
				XmlObjectSerializerReadContextComplexJson.ThrowMissingRequiredMembers(this.objectLocal, this.memberNames, bitFlagsGenerator.LoadArray(), requiredElements);
			}
		}

		// Token: 0x060013A4 RID: 5028 RVA: 0x00048434 File Offset: 0x00046634
		private int ReadMembers(int index, ClassDataContract classContract, BitFlagsGenerator expectedElements, ref int memberIndex)
		{
			int num = (classContract.BaseContract == null) ? 0 : this.ReadMembers(index, classContract.BaseContract, expectedElements, ref memberIndex);
			if (num <= index && index < num + classContract.Members.Count)
			{
				DataMember dataMember = classContract.Members[index - num];
				Type memberType = dataMember.MemberType;
				memberIndex = num;
				if (!expectedElements.Load(index))
				{
					XmlObjectSerializerReadContextComplexJson.ThrowDuplicateMemberException(this.objectLocal, this.memberNames, memberIndex);
				}
				if (dataMember.IsGetOnlyCollection)
				{
					object member = CodeInterpreter.GetMember(dataMember.MemberInfo, this.objectLocal);
					this.context.StoreCollectionMemberInfo(member);
					this.ReadValue(memberType, dataMember.Name);
				}
				else
				{
					object value = this.ReadValue(memberType, dataMember.Name);
					CodeInterpreter.SetMember(dataMember.MemberInfo, this.objectLocal, value);
				}
				memberIndex = index;
				this.ResetExpectedElements(expectedElements, index);
			}
			return num + classContract.Members.Count;
		}

		// Token: 0x060013A5 RID: 5029 RVA: 0x00048520 File Offset: 0x00046720
		private bool CheckRequiredElements(BitFlagsGenerator expectedElements, byte[] requiredElements)
		{
			for (int i = 0; i < requiredElements.Length; i++)
			{
				if ((expectedElements.GetLocal(i) & requiredElements[i]) != 0)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060013A6 RID: 5030 RVA: 0x0004854C File Offset: 0x0004674C
		private int SetRequiredElements(ClassDataContract contract, byte[] requiredElements)
		{
			int num = (contract.BaseContract == null) ? 0 : this.SetRequiredElements(contract.BaseContract, requiredElements);
			List<DataMember> members = contract.Members;
			int i = 0;
			while (i < members.Count)
			{
				if (members[i].IsRequired)
				{
					BitFlagsGenerator.SetBit(requiredElements, num);
				}
				i++;
				num++;
			}
			return num;
		}

		// Token: 0x060013A7 RID: 5031 RVA: 0x000485A4 File Offset: 0x000467A4
		private void SetExpectedElements(BitFlagsGenerator expectedElements, int startIndex)
		{
			int bitCount = expectedElements.GetBitCount();
			for (int i = startIndex; i < bitCount; i++)
			{
				expectedElements.Store(i, true);
			}
		}

		// Token: 0x060013A8 RID: 5032 RVA: 0x000485CC File Offset: 0x000467CC
		private void ResetExpectedElements(BitFlagsGenerator expectedElements, int index)
		{
			expectedElements.Store(index, false);
		}

		// Token: 0x060013A9 RID: 5033 RVA: 0x000485D8 File Offset: 0x000467D8
		private object ReadValue(Type type, string name)
		{
			Type type2 = type;
			bool flag = false;
			int num = 0;
			while (type.IsGenericType && type.GetGenericTypeDefinition() == Globals.TypeOfNullable)
			{
				num++;
				type = type.GetGenericArguments()[0];
			}
			PrimitiveDataContract primitiveDataContract = PrimitiveDataContract.GetPrimitiveDataContract(type);
			object obj;
			if ((primitiveDataContract != null && primitiveDataContract.UnderlyingType != Globals.TypeOfObject) || num != 0 || type.IsValueType)
			{
				this.context.ReadAttributes(this.xmlReader);
				string text = this.context.ReadIfNullOrRef(this.xmlReader, type, DataContract.IsTypeSerializable(type));
				if (text == null)
				{
					if (num != 0)
					{
						obj = Activator.CreateInstance(type2);
					}
					else
					{
						if (type.IsValueType)
						{
							throw new SerializationException(SR.GetString("ValueType '{0}' cannot be null.", new object[]
							{
								DataContract.GetClrTypeFullName(type)
							}));
						}
						obj = null;
					}
				}
				else if (text == string.Empty)
				{
					text = this.context.GetObjectId();
					if (type.IsValueType && !string.IsNullOrEmpty(text))
					{
						throw new SerializationException(SR.GetString("ValueType '{0}' cannot have id.", new object[]
						{
							DataContract.GetClrTypeFullName(type)
						}));
					}
					if (num != 0)
					{
						flag = true;
					}
					if (primitiveDataContract != null && primitiveDataContract.UnderlyingType != Globals.TypeOfObject)
					{
						obj = primitiveDataContract.XmlFormatReaderMethod.Invoke(this.xmlReader, new object[0]);
						if (!type.IsValueType)
						{
							this.context.AddNewObject(obj);
						}
					}
					else
					{
						obj = this.InternalDeserialize(type, name);
					}
				}
				else
				{
					if (type.IsValueType)
					{
						throw new SerializationException(SR.GetString("ValueType '{0}' cannot have ref to another object.", new object[]
						{
							DataContract.GetClrTypeFullName(type)
						}));
					}
					obj = CodeInterpreter.ConvertValue(this.context.GetExistingObject(text, type, name, string.Empty), Globals.TypeOfObject, type);
				}
				if (flag && text != null)
				{
					obj = this.WrapNullableObject(type, obj, type2, num);
				}
			}
			else
			{
				obj = this.InternalDeserialize(type, name);
			}
			return obj;
		}

		// Token: 0x060013AA RID: 5034 RVA: 0x000487BC File Offset: 0x000469BC
		private object InternalDeserialize(Type type, string name)
		{
			Type type2 = type.IsPointer ? Globals.TypeOfReflectionPointer : type;
			object obj = this.context.InternalDeserialize(this.xmlReader, DataContract.GetId(type2.TypeHandle), type2.TypeHandle, name, string.Empty);
			if (type.IsPointer)
			{
				return JsonFormatGeneratorStatics.UnboxPointer.Invoke(null, new object[]
				{
					obj
				});
			}
			return CodeInterpreter.ConvertValue(obj, Globals.TypeOfObject, type);
		}

		// Token: 0x060013AB RID: 5035 RVA: 0x00048830 File Offset: 0x00046A30
		private object WrapNullableObject(Type innerType, object innerValue, Type outerType, int nullables)
		{
			object obj = innerValue;
			for (int i = 1; i < nullables; i++)
			{
				Type type = Globals.TypeOfNullable.MakeGenericType(new Type[]
				{
					innerType
				});
				obj = Activator.CreateInstance(type, new object[]
				{
					obj
				});
				innerType = type;
			}
			return Activator.CreateInstance(outerType, new object[]
			{
				obj
			});
		}

		// Token: 0x060013AC RID: 5036 RVA: 0x00048888 File Offset: 0x00046A88
		private void ReadCollection(CollectionDataContract collectionContract)
		{
			Type type = collectionContract.UnderlyingType;
			Type itemType = collectionContract.ItemType;
			bool flag = collectionContract.Kind == CollectionKind.Array;
			ConstructorInfo constructor = collectionContract.Constructor;
			if (type.IsInterface)
			{
				switch (collectionContract.Kind)
				{
				case CollectionKind.GenericDictionary:
					type = Globals.TypeOfDictionaryGeneric.MakeGenericType(itemType.GetGenericArguments());
					constructor = type.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Globals.EmptyTypeArray, null);
					break;
				case CollectionKind.Dictionary:
					type = Globals.TypeOfHashtable;
					constructor = type.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Globals.EmptyTypeArray, null);
					break;
				case CollectionKind.GenericList:
				case CollectionKind.GenericCollection:
				case CollectionKind.List:
				case CollectionKind.GenericEnumerable:
				case CollectionKind.Collection:
				case CollectionKind.Enumerable:
					type = itemType.MakeArrayType();
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				if (type.IsValueType)
				{
					this.objectLocal = FormatterServices.GetUninitializedObject(type);
				}
				else
				{
					this.objectLocal = constructor.Invoke(new object[0]);
					this.context.AddNewObject(this.objectLocal);
				}
			}
			if ((collectionContract.Kind == CollectionKind.Dictionary || collectionContract.Kind == CollectionKind.GenericDictionary) & this.context.UseSimpleDictionaryFormat)
			{
				this.ReadSimpleDictionary(collectionContract, itemType);
				return;
			}
			string objectId = this.context.GetObjectId();
			bool flag2 = false;
			bool flag3 = false;
			if (flag && this.TryReadPrimitiveArray(itemType, out flag3))
			{
				flag2 = true;
			}
			if (!flag2)
			{
				object obj = null;
				if (flag)
				{
					obj = Array.CreateInstance(itemType, 32);
				}
				int i;
				for (i = 0; i < 2147483647; i++)
				{
					if (this.IsStartElement(this.itemName, this.emptyDictionaryString))
					{
						this.context.IncrementItemCount(1);
						object value = this.ReadCollectionItem(collectionContract, itemType);
						if (flag)
						{
							obj = XmlFormatGeneratorStatics.EnsureArraySizeMethod.MakeGenericMethod(new Type[]
							{
								itemType
							}).Invoke(null, new object[]
							{
								obj,
								i
							});
							((Array)obj).SetValue(value, i);
						}
						else
						{
							this.StoreCollectionValue(this.objectLocal, itemType, value, collectionContract);
						}
					}
					else
					{
						if (this.IsEndElement())
						{
							break;
						}
						this.HandleUnexpectedItemInCollection(ref i);
					}
				}
				if (flag)
				{
					MethodInfo methodInfo = XmlFormatGeneratorStatics.TrimArraySizeMethod.MakeGenericMethod(new Type[]
					{
						itemType
					});
					this.objectLocal = methodInfo.Invoke(null, new object[]
					{
						obj,
						i
					});
					this.context.AddNewObjectWithId(objectId, this.objectLocal);
					return;
				}
			}
			else
			{
				this.context.AddNewObjectWithId(objectId, this.objectLocal);
			}
		}

		// Token: 0x060013AD RID: 5037 RVA: 0x00048AE8 File Offset: 0x00046CE8
		private void ReadSimpleDictionary(CollectionDataContract collectionContract, Type keyValueType)
		{
			Type[] genericArguments = keyValueType.GetGenericArguments();
			Type type = genericArguments[0];
			Type type2 = genericArguments[1];
			int num = 0;
			while (type.IsGenericType && type.GetGenericTypeDefinition() == Globals.TypeOfNullable)
			{
				num++;
				type = type.GetGenericArguments()[0];
			}
			DataContract memberTypeContract = ((ClassDataContract)collectionContract.ItemContract).Members[0].MemberTypeContract;
			JsonFormatReaderInterpreter.KeyParseMode keyParseMode = JsonFormatReaderInterpreter.KeyParseMode.Fail;
			if (type == Globals.TypeOfString || type == Globals.TypeOfObject)
			{
				keyParseMode = JsonFormatReaderInterpreter.KeyParseMode.AsString;
			}
			else if (type.IsEnum)
			{
				keyParseMode = JsonFormatReaderInterpreter.KeyParseMode.UsingParseEnum;
			}
			else if (memberTypeContract.ParseMethod != null)
			{
				keyParseMode = JsonFormatReaderInterpreter.KeyParseMode.UsingCustomParse;
			}
			if (keyParseMode == JsonFormatReaderInterpreter.KeyParseMode.Fail)
			{
				this.ThrowSerializationException(SR.GetString("Key type '{1}' for collection type '{0}' cannot be parsed in simple dictionary.", new object[]
				{
					DataContract.GetClrTypeFullName(collectionContract.UnderlyingType),
					DataContract.GetClrTypeFullName(type)
				}), Array.Empty<object>());
				return;
			}
			XmlNodeType xmlNodeType;
			while ((xmlNodeType = this.xmlReader.MoveToContent()) != XmlNodeType.EndElement)
			{
				if (xmlNodeType != XmlNodeType.Element)
				{
					this.ThrowUnexpectedStateException(XmlNodeType.Element);
				}
				this.context.IncrementItemCount(1);
				string jsonMemberName = XmlObjectSerializerReadContextComplexJson.GetJsonMemberName(this.xmlReader);
				object obj = null;
				if (keyParseMode == JsonFormatReaderInterpreter.KeyParseMode.UsingParseEnum)
				{
					obj = Enum.Parse(type, jsonMemberName);
				}
				else if (keyParseMode == JsonFormatReaderInterpreter.KeyParseMode.UsingCustomParse)
				{
					obj = memberTypeContract.ParseMethod.Invoke(null, new object[]
					{
						jsonMemberName
					});
				}
				if (num > 0)
				{
					obj = this.WrapNullableObject(type, obj, type2, num);
				}
				object obj2 = this.ReadValue(type2, string.Empty);
				collectionContract.AddMethod.Invoke(this.objectLocal, new object[]
				{
					obj,
					obj2
				});
			}
		}

		// Token: 0x060013AE RID: 5038 RVA: 0x00048C70 File Offset: 0x00046E70
		private void ReadGetOnlyCollection(CollectionDataContract collectionContract)
		{
			Type underlyingType = collectionContract.UnderlyingType;
			Type itemType = collectionContract.ItemType;
			bool flag = collectionContract.Kind == CollectionKind.Array;
			int num = 0;
			this.objectLocal = this.context.GetCollectionMember();
			if ((collectionContract.Kind != CollectionKind.Dictionary && collectionContract.Kind != CollectionKind.GenericDictionary) || !this.context.UseSimpleDictionaryFormat)
			{
				if (this.IsStartElement(this.itemName, this.emptyDictionaryString))
				{
					if (this.objectLocal == null)
					{
						XmlObjectSerializerReadContext.ThrowNullValueReturnedForGetOnlyCollectionException(underlyingType);
						return;
					}
					num = 0;
					if (flag)
					{
						num = ((Array)this.objectLocal).Length;
					}
					int i = 0;
					while (i < 2147483647)
					{
						if (this.IsStartElement(this.itemName, this.emptyDictionaryString))
						{
							this.context.IncrementItemCount(1);
							object value = this.ReadCollectionItem(collectionContract, itemType);
							if (flag)
							{
								if (num == i)
								{
									XmlObjectSerializerReadContext.ThrowArrayExceededSizeException(num, underlyingType);
								}
								else
								{
									((Array)this.objectLocal).SetValue(value, i);
								}
							}
							else
							{
								this.StoreCollectionValue(this.objectLocal, itemType, value, collectionContract);
							}
						}
						else
						{
							if (this.IsEndElement())
							{
								break;
							}
							this.HandleUnexpectedItemInCollection(ref i);
						}
					}
					this.context.CheckEndOfArray(this.xmlReader, num, this.itemName, this.emptyDictionaryString);
				}
				return;
			}
			if (this.objectLocal == null)
			{
				XmlObjectSerializerReadContext.ThrowNullValueReturnedForGetOnlyCollectionException(underlyingType);
				return;
			}
			this.ReadSimpleDictionary(collectionContract, itemType);
			this.context.CheckEndOfArray(this.xmlReader, num, this.itemName, this.emptyDictionaryString);
		}

		// Token: 0x060013AF RID: 5039 RVA: 0x00048DE4 File Offset: 0x00046FE4
		private bool TryReadPrimitiveArray(Type itemType, out bool readResult)
		{
			readResult = false;
			if (PrimitiveDataContract.GetPrimitiveDataContract(itemType) == null)
			{
				return false;
			}
			string text = null;
			TypeCode typeCode = Type.GetTypeCode(itemType);
			if (typeCode != TypeCode.Boolean)
			{
				switch (typeCode)
				{
				case TypeCode.Int32:
					text = "TryReadInt32Array";
					break;
				case TypeCode.Int64:
					text = "TryReadInt64Array";
					break;
				case TypeCode.Single:
					text = "TryReadSingleArray";
					break;
				case TypeCode.Double:
					text = "TryReadDoubleArray";
					break;
				case TypeCode.Decimal:
					text = "TryReadDecimalArray";
					break;
				case TypeCode.DateTime:
					text = "TryReadJsonDateTimeArray";
					break;
				}
			}
			else
			{
				text = "TryReadBooleanArray";
			}
			if (text != null)
			{
				MethodInfo method = typeof(JsonReaderDelegator).GetMethod(text, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				object[] array = new object[]
				{
					this.context,
					this.itemName,
					this.emptyDictionaryString,
					-1,
					this.objectLocal
				};
				readResult = (bool)method.Invoke((JsonReaderDelegator)this.xmlReader, array);
				this.objectLocal = array.Last<object>();
				return true;
			}
			return false;
		}

		// Token: 0x060013B0 RID: 5040 RVA: 0x00048EE0 File Offset: 0x000470E0
		private object ReadCollectionItem(CollectionDataContract collectionContract, Type itemType)
		{
			if (collectionContract.Kind == CollectionKind.Dictionary || collectionContract.Kind == CollectionKind.GenericDictionary)
			{
				this.context.ResetAttributes();
				return CodeInterpreter.ConvertValue(DataContractJsonSerializer.ReadJsonValue(XmlObjectSerializerWriteContextComplexJson.GetRevisedItemContract(collectionContract.ItemContract), this.xmlReader, this.context), Globals.TypeOfObject, itemType);
			}
			return this.ReadValue(itemType, "item");
		}

		// Token: 0x060013B1 RID: 5041 RVA: 0x00048F40 File Offset: 0x00047140
		private void StoreCollectionValue(object collection, Type valueType, object value, CollectionDataContract collectionContract)
		{
			if (collectionContract.Kind == CollectionKind.GenericDictionary || collectionContract.Kind == CollectionKind.Dictionary)
			{
				ClassDataContract classDataContract = DataContract.GetDataContract(valueType) as ClassDataContract;
				DataMember dataMember = classDataContract.Members[0];
				DataMember dataMember2 = classDataContract.Members[1];
				object member = CodeInterpreter.GetMember(dataMember.MemberInfo, value);
				object member2 = CodeInterpreter.GetMember(dataMember2.MemberInfo, value);
				try
				{
					collectionContract.AddMethod.Invoke(collection, new object[]
					{
						member,
						member2
					});
					return;
				}
				catch (TargetInvocationException ex)
				{
					if (ex.InnerException != null)
					{
						throw ex.InnerException;
					}
					throw;
				}
			}
			collectionContract.AddMethod.Invoke(collection, new object[]
			{
				value
			});
		}

		// Token: 0x060013B2 RID: 5042 RVA: 0x00048FF8 File Offset: 0x000471F8
		private void HandleUnexpectedItemInCollection(ref int iterator)
		{
			if (this.IsStartElement())
			{
				this.context.SkipUnknownElement(this.xmlReader);
				iterator--;
				return;
			}
			throw XmlObjectSerializerReadContext.CreateUnexpectedStateException(XmlNodeType.Element, this.xmlReader);
		}

		// Token: 0x060013B3 RID: 5043 RVA: 0x00049026 File Offset: 0x00047226
		private bool IsStartElement(XmlDictionaryString name, XmlDictionaryString ns)
		{
			return this.xmlReader.IsStartElement(name, ns);
		}

		// Token: 0x060013B4 RID: 5044 RVA: 0x00049035 File Offset: 0x00047235
		private bool IsStartElement()
		{
			return this.xmlReader.IsStartElement();
		}

		// Token: 0x060013B5 RID: 5045 RVA: 0x00049042 File Offset: 0x00047242
		private bool IsEndElement()
		{
			return this.xmlReader.NodeType == XmlNodeType.EndElement;
		}

		// Token: 0x060013B6 RID: 5046 RVA: 0x00049053 File Offset: 0x00047253
		private void ThrowUnexpectedStateException(XmlNodeType expectedState)
		{
			throw XmlObjectSerializerReadContext.CreateUnexpectedStateException(expectedState, this.xmlReader);
		}

		// Token: 0x060013B7 RID: 5047 RVA: 0x00049061 File Offset: 0x00047261
		private void ThrowSerializationException(string msg, params object[] values)
		{
			if (values != null && values.Length != 0)
			{
				msg = string.Format(msg, values);
			}
			throw new SerializationException(msg);
		}

		// Token: 0x04000A0A RID: 2570
		private bool is_get_only_collection;

		// Token: 0x04000A0B RID: 2571
		private ClassDataContract classContract;

		// Token: 0x04000A0C RID: 2572
		private CollectionDataContract collectionContract;

		// Token: 0x04000A0D RID: 2573
		private object objectLocal;

		// Token: 0x04000A0E RID: 2574
		private Type objectType;

		// Token: 0x04000A0F RID: 2575
		private XmlReaderDelegator xmlReader;

		// Token: 0x04000A10 RID: 2576
		private XmlObjectSerializerReadContextComplexJson context;

		// Token: 0x04000A11 RID: 2577
		private XmlDictionaryString[] memberNames;

		// Token: 0x04000A12 RID: 2578
		private XmlDictionaryString emptyDictionaryString;

		// Token: 0x04000A13 RID: 2579
		private XmlDictionaryString itemName;

		// Token: 0x04000A14 RID: 2580
		private XmlDictionaryString itemNamespace;

		// Token: 0x0200018E RID: 398
		private enum KeyParseMode
		{
			// Token: 0x04000A16 RID: 2582
			Fail,
			// Token: 0x04000A17 RID: 2583
			AsString,
			// Token: 0x04000A18 RID: 2584
			UsingParseEnum,
			// Token: 0x04000A19 RID: 2585
			UsingCustomParse
		}
	}
}
