﻿using System;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000180 RID: 384
	internal class JsonStringDataContract : JsonDataContract
	{
		// Token: 0x060012A9 RID: 4777 RVA: 0x00040D44 File Offset: 0x0003EF44
		public JsonStringDataContract(StringDataContract traditionalStringDataContract) : base(traditionalStringDataContract)
		{
		}

		// Token: 0x060012AA RID: 4778 RVA: 0x00043059 File Offset: 0x00041259
		public override object ReadJsonValueCore(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			if (context != null)
			{
				return JsonDataContract.HandleReadValue(jsonReader.ReadElementContentAsString(), context);
			}
			if (!JsonDataContract.TryReadNullAtTopLevel(jsonReader))
			{
				return jsonReader.ReadElementContentAsString();
			}
			return null;
		}
	}
}
