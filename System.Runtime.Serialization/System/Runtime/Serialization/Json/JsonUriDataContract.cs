﻿using System;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x02000181 RID: 385
	internal class JsonUriDataContract : JsonDataContract
	{
		// Token: 0x060012AB RID: 4779 RVA: 0x00040D44 File Offset: 0x0003EF44
		public JsonUriDataContract(UriDataContract traditionalUriDataContract) : base(traditionalUriDataContract)
		{
		}

		// Token: 0x060012AC RID: 4780 RVA: 0x0004307B File Offset: 0x0004127B
		public override object ReadJsonValueCore(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			if (context != null)
			{
				return JsonDataContract.HandleReadValue(jsonReader.ReadElementContentAsUri(), context);
			}
			if (!JsonDataContract.TryReadNullAtTopLevel(jsonReader))
			{
				return jsonReader.ReadElementContentAsUri();
			}
			return null;
		}
	}
}
