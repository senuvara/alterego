﻿using System;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200017C RID: 380
	internal class JsonQNameDataContract : JsonDataContract
	{
		// Token: 0x06001289 RID: 4745 RVA: 0x00040D44 File Offset: 0x0003EF44
		public JsonQNameDataContract(QNameDataContract traditionalQNameDataContract) : base(traditionalQNameDataContract)
		{
		}

		// Token: 0x0600128A RID: 4746 RVA: 0x00042951 File Offset: 0x00040B51
		public override object ReadJsonValueCore(XmlReaderDelegator jsonReader, XmlObjectSerializerReadContextComplexJson context)
		{
			if (context != null)
			{
				return JsonDataContract.HandleReadValue(jsonReader.ReadElementContentAsQName(), context);
			}
			if (!JsonDataContract.TryReadNullAtTopLevel(jsonReader))
			{
				return jsonReader.ReadElementContentAsQName();
			}
			return null;
		}
	}
}
