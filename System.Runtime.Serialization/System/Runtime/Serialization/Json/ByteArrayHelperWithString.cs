﻿using System;
using System.Globalization;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200015B RID: 347
	internal class ByteArrayHelperWithString : ArrayHelper<string, byte>
	{
		// Token: 0x0600119C RID: 4508 RVA: 0x000402CC File Offset: 0x0003E4CC
		internal void WriteArray(XmlWriter writer, byte[] array, int offset, int count)
		{
			XmlJsonReader.CheckArray(array, offset, count);
			writer.WriteAttributeString(string.Empty, "type", string.Empty, "array");
			for (int i = 0; i < count; i++)
			{
				writer.WriteStartElement("item", string.Empty);
				writer.WriteAttributeString(string.Empty, "type", string.Empty, "number");
				writer.WriteValue((int)array[offset + i]);
				writer.WriteEndElement();
			}
		}

		// Token: 0x0600119D RID: 4509 RVA: 0x00040344 File Offset: 0x0003E544
		protected override int ReadArray(XmlDictionaryReader reader, string localName, string namespaceUri, byte[] array, int offset, int count)
		{
			XmlJsonReader.CheckArray(array, offset, count);
			int num = 0;
			while (num < count && reader.IsStartElement("item", string.Empty))
			{
				array[offset + num] = this.ToByte(reader.ReadElementContentAsInt());
				num++;
			}
			return num;
		}

		// Token: 0x0600119E RID: 4510 RVA: 0x0004038F File Offset: 0x0003E58F
		protected override void WriteArray(XmlDictionaryWriter writer, string prefix, string localName, string namespaceUri, byte[] array, int offset, int count)
		{
			this.WriteArray(writer, array, offset, count);
		}

		// Token: 0x0600119F RID: 4511 RVA: 0x0004039E File Offset: 0x0003E59E
		private void ThrowConversionException(string value, string type)
		{
			throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new XmlException(SR.GetString("The value '{0}' cannot be parsed as the type '{1}'.", new object[]
			{
				value,
				type
			})));
		}

		// Token: 0x060011A0 RID: 4512 RVA: 0x000403C2 File Offset: 0x0003E5C2
		private byte ToByte(int value)
		{
			if (value < 0 || value > 255)
			{
				this.ThrowConversionException(value.ToString(NumberFormatInfo.CurrentInfo), "Byte");
			}
			return (byte)value;
		}

		// Token: 0x060011A1 RID: 4513 RVA: 0x000403E9 File Offset: 0x0003E5E9
		public ByteArrayHelperWithString()
		{
		}

		// Token: 0x060011A2 RID: 4514 RVA: 0x000403F1 File Offset: 0x0003E5F1
		// Note: this type is marked as 'beforefieldinit'.
		static ByteArrayHelperWithString()
		{
		}

		// Token: 0x04000937 RID: 2359
		public static readonly ByteArrayHelperWithString Instance = new ByteArrayHelperWithString();
	}
}
