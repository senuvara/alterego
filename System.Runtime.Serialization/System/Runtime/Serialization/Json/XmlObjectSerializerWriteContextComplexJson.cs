﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace System.Runtime.Serialization.Json
{
	// Token: 0x0200018C RID: 396
	internal class XmlObjectSerializerWriteContextComplexJson : XmlObjectSerializerWriteContextComplex
	{
		// Token: 0x0600137A RID: 4986 RVA: 0x000477B4 File Offset: 0x000459B4
		public XmlObjectSerializerWriteContextComplexJson(DataContractJsonSerializer serializer, DataContract rootTypeDataContract) : base(serializer, serializer.MaxItemsInObjectGraph, new StreamingContext(StreamingContextStates.All), serializer.IgnoreExtensionDataObject)
		{
			this.emitXsiType = serializer.EmitTypeInformation;
			this.rootTypeDataContract = rootTypeDataContract;
			this.serializerKnownTypeList = serializer.knownTypeList;
			this.dataContractSurrogate = serializer.DataContractSurrogate;
			this.serializeReadOnlyTypes = serializer.SerializeReadOnlyTypes;
			this.useSimpleDictionaryFormat = serializer.UseSimpleDictionaryFormat;
		}

		// Token: 0x0600137B RID: 4987 RVA: 0x00047821 File Offset: 0x00045A21
		internal static XmlObjectSerializerWriteContextComplexJson CreateContext(DataContractJsonSerializer serializer, DataContract rootTypeDataContract)
		{
			return new XmlObjectSerializerWriteContextComplexJson(serializer, rootTypeDataContract);
		}

		// Token: 0x17000411 RID: 1041
		// (get) Token: 0x0600137C RID: 4988 RVA: 0x000472C1 File Offset: 0x000454C1
		internal IList<Type> SerializerKnownTypeList
		{
			get
			{
				return this.serializerKnownTypeList;
			}
		}

		// Token: 0x17000412 RID: 1042
		// (get) Token: 0x0600137D RID: 4989 RVA: 0x0004782A File Offset: 0x00045A2A
		public bool UseSimpleDictionaryFormat
		{
			get
			{
				return this.useSimpleDictionaryFormat;
			}
		}

		// Token: 0x0600137E RID: 4990 RVA: 0x0000310F File Offset: 0x0000130F
		internal override bool WriteClrTypeInfo(XmlWriterDelegator xmlWriter, Type dataContractType, string clrTypeName, string clrAssemblyName)
		{
			return false;
		}

		// Token: 0x0600137F RID: 4991 RVA: 0x0000310F File Offset: 0x0000130F
		internal override bool WriteClrTypeInfo(XmlWriterDelegator xmlWriter, DataContract dataContract)
		{
			return false;
		}

		// Token: 0x06001380 RID: 4992 RVA: 0x000020AE File Offset: 0x000002AE
		internal override void WriteArraySize(XmlWriterDelegator xmlWriter, int size)
		{
		}

		// Token: 0x06001381 RID: 4993 RVA: 0x00047832 File Offset: 0x00045A32
		protected override void WriteTypeInfo(XmlWriterDelegator writer, string dataContractName, string dataContractNamespace)
		{
			if (this.emitXsiType != EmitTypeInformation.Never)
			{
				if (string.IsNullOrEmpty(dataContractNamespace))
				{
					this.WriteTypeInfo(writer, dataContractName);
					return;
				}
				this.WriteTypeInfo(writer, dataContractName + ":" + XmlObjectSerializerWriteContextComplexJson.TruncateDefaultDataContractNamespace(dataContractNamespace));
			}
		}

		// Token: 0x06001382 RID: 4994 RVA: 0x00047868 File Offset: 0x00045A68
		internal static string TruncateDefaultDataContractNamespace(string dataContractNamespace)
		{
			if (!string.IsNullOrEmpty(dataContractNamespace))
			{
				if (dataContractNamespace[0] == '#')
				{
					return "\\" + dataContractNamespace;
				}
				if (dataContractNamespace[0] == '\\')
				{
					return "\\" + dataContractNamespace;
				}
				if (dataContractNamespace.StartsWith("http://schemas.datacontract.org/2004/07/", StringComparison.Ordinal))
				{
					return "#" + dataContractNamespace.Substring(JsonGlobals.DataContractXsdBaseNamespaceLength);
				}
			}
			return dataContractNamespace;
		}

		// Token: 0x06001383 RID: 4995 RVA: 0x000478D0 File Offset: 0x00045AD0
		private static bool RequiresJsonTypeInfo(DataContract contract)
		{
			return contract is ClassDataContract;
		}

		// Token: 0x06001384 RID: 4996 RVA: 0x000478DB File Offset: 0x00045ADB
		private void WriteTypeInfo(XmlWriterDelegator writer, string typeInformation)
		{
			writer.WriteAttributeString(null, "__type", null, typeInformation);
		}

		// Token: 0x06001385 RID: 4997 RVA: 0x000478EC File Offset: 0x00045AEC
		protected override bool WriteTypeInfo(XmlWriterDelegator writer, DataContract contract, DataContract declaredContract)
		{
			if ((contract.Name != declaredContract.Name || contract.Namespace != declaredContract.Namespace) && (!(contract.Name.Value == declaredContract.Name.Value) || !(contract.Namespace.Value == declaredContract.Namespace.Value)) && contract.UnderlyingType != Globals.TypeOfObjectArray && this.emitXsiType != EmitTypeInformation.Never)
			{
				if (XmlObjectSerializerWriteContextComplexJson.RequiresJsonTypeInfo(contract))
				{
					this.perCallXsiTypeAlreadyEmitted = true;
					this.WriteTypeInfo(writer, contract.Name.Value, contract.Namespace.Value);
				}
				else if (declaredContract.UnderlyingType == typeof(Enum))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new SerializationException(SR.GetString("Enum type is not supported by DataContractJsonSerializer. The underlying type is '{0}'.", new object[]
					{
						declaredContract.UnderlyingType
					})));
				}
				return true;
			}
			return false;
		}

		// Token: 0x06001386 RID: 4998 RVA: 0x000479DC File Offset: 0x00045BDC
		internal void WriteJsonISerializable(XmlWriterDelegator xmlWriter, ISerializable obj)
		{
			Type type = obj.GetType();
			SerializationInfo serializationInfo = new SerializationInfo(type, XmlObjectSerializer.FormatterConverter);
			base.GetObjectData(obj, serializationInfo, base.GetStreamingContext());
			if (DataContract.GetClrTypeFullName(type) != serializationInfo.FullTypeName)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Changing full type name is not supported. Serialization type name: '{0}', data contract type name: '{1}'.", new object[]
				{
					serializationInfo.FullTypeName,
					DataContract.GetClrTypeFullName(type)
				})));
			}
			base.WriteSerializationInfo(xmlWriter, type, serializationInfo);
		}

		// Token: 0x06001387 RID: 4999 RVA: 0x00047A53 File Offset: 0x00045C53
		internal static DataContract GetRevisedItemContract(DataContract oldItemContract)
		{
			if (oldItemContract != null && oldItemContract.UnderlyingType.IsGenericType && oldItemContract.UnderlyingType.GetGenericTypeDefinition() == Globals.TypeOfKeyValue)
			{
				return DataContract.GetDataContract(oldItemContract.UnderlyingType);
			}
			return oldItemContract;
		}

		// Token: 0x06001388 RID: 5000 RVA: 0x00047A8C File Offset: 0x00045C8C
		protected override void WriteDataContractValue(DataContract dataContract, XmlWriterDelegator xmlWriter, object obj, RuntimeTypeHandle declaredTypeHandle)
		{
			JsonDataContract jsonDataContract = JsonDataContract.GetJsonDataContract(dataContract);
			if (this.emitXsiType == EmitTypeInformation.Always && !this.perCallXsiTypeAlreadyEmitted && XmlObjectSerializerWriteContextComplexJson.RequiresJsonTypeInfo(dataContract))
			{
				this.WriteTypeInfo(xmlWriter, jsonDataContract.TypeName);
			}
			this.perCallXsiTypeAlreadyEmitted = false;
			DataContractJsonSerializer.WriteJsonValue(jsonDataContract, xmlWriter, obj, this, declaredTypeHandle);
		}

		// Token: 0x06001389 RID: 5001 RVA: 0x00047AD8 File Offset: 0x00045CD8
		protected override void WriteNull(XmlWriterDelegator xmlWriter)
		{
			DataContractJsonSerializer.WriteJsonNull(xmlWriter);
		}

		// Token: 0x17000413 RID: 1043
		// (get) Token: 0x0600138A RID: 5002 RVA: 0x00047AE0 File Offset: 0x00045CE0
		internal XmlDictionaryString CollectionItemName
		{
			get
			{
				return JsonGlobals.itemDictionaryString;
			}
		}

		// Token: 0x0600138B RID: 5003 RVA: 0x00047AE7 File Offset: 0x00045CE7
		internal static void WriteJsonNameWithMapping(XmlWriterDelegator xmlWriter, XmlDictionaryString[] memberNames, int index)
		{
			xmlWriter.WriteStartElement("a", "item", "item");
			xmlWriter.WriteAttributeString(null, "item", null, memberNames[index].Value);
		}

		// Token: 0x0600138C RID: 5004 RVA: 0x00047B14 File Offset: 0x00045D14
		internal override void WriteExtensionDataTypeInfo(XmlWriterDelegator xmlWriter, IDataNode dataNode)
		{
			Type dataType = dataNode.DataType;
			if (dataType == Globals.TypeOfClassDataNode || dataType == Globals.TypeOfISerializableDataNode)
			{
				xmlWriter.WriteAttributeString(null, "type", null, "object");
				base.WriteExtensionDataTypeInfo(xmlWriter, dataNode);
				return;
			}
			if (dataType == Globals.TypeOfCollectionDataNode)
			{
				xmlWriter.WriteAttributeString(null, "type", null, "array");
				return;
			}
			if (!(dataType == Globals.TypeOfXmlDataNode) && dataType == Globals.TypeOfObject && dataNode.Value != null && XmlObjectSerializerWriteContextComplexJson.RequiresJsonTypeInfo(base.GetDataContract(dataNode.Value.GetType())))
			{
				base.WriteExtensionDataTypeInfo(xmlWriter, dataNode);
			}
		}

		// Token: 0x0600138D RID: 5005 RVA: 0x00047BC0 File Offset: 0x00045DC0
		protected override void SerializeWithXsiType(XmlWriterDelegator xmlWriter, object obj, RuntimeTypeHandle objectTypeHandle, Type objectType, int declaredTypeID, RuntimeTypeHandle declaredTypeHandle, Type declaredType)
		{
			bool verifyKnownType = false;
			bool isInterface = declaredType.IsInterface;
			DataContract dataContract;
			if (isInterface && CollectionDataContract.IsCollectionInterface(declaredType))
			{
				dataContract = this.GetDataContract(declaredTypeHandle, declaredType);
			}
			else if (declaredType.IsArray)
			{
				dataContract = this.GetDataContract(declaredTypeHandle, declaredType);
			}
			else
			{
				dataContract = this.GetDataContract(objectTypeHandle, objectType);
				DataContract declaredContract = (declaredTypeID >= 0) ? this.GetDataContract(declaredTypeID, declaredTypeHandle) : this.GetDataContract(declaredTypeHandle, declaredType);
				verifyKnownType = this.WriteTypeInfo(xmlWriter, dataContract, declaredContract);
				this.HandleCollectionAssignedToObject(declaredType, ref dataContract, ref obj, ref verifyKnownType);
			}
			if (isInterface)
			{
				XmlObjectSerializerWriteContextComplexJson.VerifyObjectCompatibilityWithInterface(dataContract, obj, declaredType);
			}
			base.SerializeAndVerifyType(dataContract, xmlWriter, obj, verifyKnownType, declaredType.TypeHandle, declaredType);
		}

		// Token: 0x0600138E RID: 5006 RVA: 0x00047C64 File Offset: 0x00045E64
		private static void VerifyObjectCompatibilityWithInterface(DataContract contract, object graph, Type declaredType)
		{
			Type type = contract.GetType();
			if (type == typeof(XmlDataContract) && !Globals.TypeOfIXmlSerializable.IsAssignableFrom(declaredType))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Object of type '{0}' is assigned to an incompatible interface '{1}'.", new object[]
				{
					graph.GetType(),
					declaredType
				})));
			}
			if (type == typeof(CollectionDataContract) && !CollectionDataContract.IsCollectionInterface(declaredType))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Collection of type '{0}' is assigned to an incompatible interface '{1}'", new object[]
				{
					graph.GetType(),
					declaredType
				})));
			}
		}

		// Token: 0x0600138F RID: 5007 RVA: 0x00047D04 File Offset: 0x00045F04
		private void HandleCollectionAssignedToObject(Type declaredType, ref DataContract dataContract, ref object obj, ref bool verifyKnownType)
		{
			if (declaredType != dataContract.UnderlyingType && dataContract is CollectionDataContract)
			{
				if (verifyKnownType)
				{
					this.VerifyType(dataContract, declaredType);
					verifyKnownType = false;
				}
				if (((CollectionDataContract)dataContract).Kind == CollectionKind.Dictionary)
				{
					IDictionary dictionary = obj as IDictionary;
					Dictionary<object, object> dictionary2 = new Dictionary<object, object>();
					foreach (object obj2 in dictionary)
					{
						DictionaryEntry dictionaryEntry = (DictionaryEntry)obj2;
						dictionary2.Add(dictionaryEntry.Key, dictionaryEntry.Value);
					}
					obj = dictionary2;
				}
				dataContract = base.GetDataContract(Globals.TypeOfIEnumerable);
			}
		}

		// Token: 0x06001390 RID: 5008 RVA: 0x00047DC0 File Offset: 0x00045FC0
		internal override void SerializeWithXsiTypeAtTopLevel(DataContract dataContract, XmlWriterDelegator xmlWriter, object obj, RuntimeTypeHandle originalDeclaredTypeHandle, Type graphType)
		{
			bool verifyKnownType = false;
			Type underlyingType = this.rootTypeDataContract.UnderlyingType;
			bool isInterface = underlyingType.IsInterface;
			if ((!isInterface || !CollectionDataContract.IsCollectionInterface(underlyingType)) && !underlyingType.IsArray)
			{
				verifyKnownType = this.WriteTypeInfo(xmlWriter, dataContract, this.rootTypeDataContract);
				this.HandleCollectionAssignedToObject(underlyingType, ref dataContract, ref obj, ref verifyKnownType);
			}
			if (isInterface)
			{
				XmlObjectSerializerWriteContextComplexJson.VerifyObjectCompatibilityWithInterface(dataContract, obj, underlyingType);
			}
			base.SerializeAndVerifyType(dataContract, xmlWriter, obj, verifyKnownType, underlyingType.TypeHandle, underlyingType);
		}

		// Token: 0x06001391 RID: 5009 RVA: 0x00047E2C File Offset: 0x0004602C
		private void VerifyType(DataContract dataContract, Type declaredType)
		{
			bool flag = false;
			if (dataContract.KnownDataContracts != null)
			{
				this.scopedKnownTypes.Push(dataContract.KnownDataContracts);
				flag = true;
			}
			if (!base.IsKnownType(dataContract, declaredType))
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(XmlObjectSerializer.CreateSerializationException(SR.GetString("Type '{0}' with data contract name '{1}:{2}' is not expected. Add any types not known statically to the list of known types - for example, by using the KnownTypeAttribute attribute or by adding them to the list of known types passed to DataContractSerializer.", new object[]
				{
					DataContract.GetClrTypeFullName(dataContract.UnderlyingType),
					dataContract.StableName.Name,
					dataContract.StableName.Namespace
				})));
			}
			if (flag)
			{
				this.scopedKnownTypes.Pop();
			}
		}

		// Token: 0x06001392 RID: 5010 RVA: 0x00047EB3 File Offset: 0x000460B3
		internal override DataContract GetDataContract(RuntimeTypeHandle typeHandle, Type type)
		{
			DataContract dataContract = base.GetDataContract(typeHandle, type);
			DataContractJsonSerializer.CheckIfTypeIsReference(dataContract);
			return dataContract;
		}

		// Token: 0x06001393 RID: 5011 RVA: 0x00047EC3 File Offset: 0x000460C3
		internal override DataContract GetDataContractSkipValidation(int typeId, RuntimeTypeHandle typeHandle, Type type)
		{
			DataContract dataContractSkipValidation = base.GetDataContractSkipValidation(typeId, typeHandle, type);
			DataContractJsonSerializer.CheckIfTypeIsReference(dataContractSkipValidation);
			return dataContractSkipValidation;
		}

		// Token: 0x06001394 RID: 5012 RVA: 0x00047ED4 File Offset: 0x000460D4
		internal override DataContract GetDataContract(int id, RuntimeTypeHandle typeHandle)
		{
			DataContract dataContract = base.GetDataContract(id, typeHandle);
			DataContractJsonSerializer.CheckIfTypeIsReference(dataContract);
			return dataContract;
		}

		// Token: 0x06001395 RID: 5013 RVA: 0x00047EE4 File Offset: 0x000460E4
		internal static DataContract ResolveJsonDataContractFromRootDataContract(XmlObjectSerializerContext context, XmlQualifiedName typeQName, DataContract rootTypeDataContract)
		{
			if (rootTypeDataContract.StableName == typeQName)
			{
				return rootTypeDataContract;
			}
			DataContract dataContract;
			for (CollectionDataContract collectionDataContract = rootTypeDataContract as CollectionDataContract; collectionDataContract != null; collectionDataContract = (dataContract as CollectionDataContract))
			{
				if (collectionDataContract.ItemType.IsGenericType && collectionDataContract.ItemType.GetGenericTypeDefinition() == typeof(KeyValue<, >))
				{
					dataContract = context.GetDataContract(Globals.TypeOfKeyValuePair.MakeGenericType(collectionDataContract.ItemType.GetGenericArguments()));
				}
				else
				{
					dataContract = context.GetDataContract(context.GetSurrogatedType(collectionDataContract.ItemType));
				}
				if (dataContract.StableName == typeQName)
				{
					return dataContract;
				}
			}
			return null;
		}

		// Token: 0x06001396 RID: 5014 RVA: 0x000477A4 File Offset: 0x000459A4
		protected override DataContract ResolveDataContractFromRootDataContract(XmlQualifiedName typeQName)
		{
			return XmlObjectSerializerWriteContextComplexJson.ResolveJsonDataContractFromRootDataContract(this, typeQName, this.rootTypeDataContract);
		}

		// Token: 0x04000A07 RID: 2567
		private EmitTypeInformation emitXsiType;

		// Token: 0x04000A08 RID: 2568
		private bool perCallXsiTypeAlreadyEmitted;

		// Token: 0x04000A09 RID: 2569
		private bool useSimpleDictionaryFormat;
	}
}
