﻿using System;
using System.Collections.Generic;

namespace System.Runtime.Serialization
{
	// Token: 0x020000BD RID: 189
	internal class TypeHandleRefEqualityComparer : IEqualityComparer<TypeHandleRef>
	{
		// Token: 0x06000AC5 RID: 2757 RVA: 0x0002C674 File Offset: 0x0002A874
		public bool Equals(TypeHandleRef x, TypeHandleRef y)
		{
			return x.Value.Equals(y.Value);
		}

		// Token: 0x06000AC6 RID: 2758 RVA: 0x0002C698 File Offset: 0x0002A898
		public int GetHashCode(TypeHandleRef obj)
		{
			return obj.Value.GetHashCode();
		}

		// Token: 0x06000AC7 RID: 2759 RVA: 0x00002217 File Offset: 0x00000417
		public TypeHandleRefEqualityComparer()
		{
		}
	}
}
