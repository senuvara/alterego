﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000D8 RID: 216
	internal class ISerializableDataMember
	{
		// Token: 0x17000225 RID: 549
		// (get) Token: 0x06000C1A RID: 3098 RVA: 0x0002F432 File Offset: 0x0002D632
		// (set) Token: 0x06000C1B RID: 3099 RVA: 0x0002F43A File Offset: 0x0002D63A
		internal string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x17000226 RID: 550
		// (get) Token: 0x06000C1C RID: 3100 RVA: 0x0002F443 File Offset: 0x0002D643
		// (set) Token: 0x06000C1D RID: 3101 RVA: 0x0002F44B File Offset: 0x0002D64B
		internal IDataNode Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		// Token: 0x06000C1E RID: 3102 RVA: 0x00002217 File Offset: 0x00000417
		public ISerializableDataMember()
		{
		}

		// Token: 0x0400051D RID: 1309
		private string name;

		// Token: 0x0400051E RID: 1310
		private IDataNode value;
	}
}
