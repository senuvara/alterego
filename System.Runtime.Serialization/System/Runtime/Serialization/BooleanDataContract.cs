﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020000F0 RID: 240
	internal class BooleanDataContract : PrimitiveDataContract
	{
		// Token: 0x06000D66 RID: 3430 RVA: 0x0003321F File Offset: 0x0003141F
		internal BooleanDataContract() : base(typeof(bool), DictionaryGlobals.BooleanLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06000D67 RID: 3431 RVA: 0x0003323B File Offset: 0x0003143B
		internal override string WriteMethodName
		{
			get
			{
				return "WriteBoolean";
			}
		}

		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06000D68 RID: 3432 RVA: 0x00033242 File Offset: 0x00031442
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsBoolean";
			}
		}

		// Token: 0x06000D69 RID: 3433 RVA: 0x00033249 File Offset: 0x00031449
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteBoolean((bool)obj);
		}

		// Token: 0x06000D6A RID: 3434 RVA: 0x00033257 File Offset: 0x00031457
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsBoolean(), context);
			}
			return reader.ReadElementContentAsBoolean();
		}
	}
}
