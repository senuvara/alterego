﻿using System;
using System.Collections.Generic;

namespace System.Runtime.Serialization
{
	// Token: 0x020000EA RID: 234
	internal struct ObjectReferenceStack
	{
		// Token: 0x06000D38 RID: 3384 RVA: 0x000328BC File Offset: 0x00030ABC
		internal void Push(object obj)
		{
			if (this.objectArray == null)
			{
				this.objectArray = new object[4];
				object[] array = this.objectArray;
				int num = this.count;
				this.count = num + 1;
				array[num] = obj;
				return;
			}
			if (this.count < 16)
			{
				if (this.count == this.objectArray.Length)
				{
					Array.Resize<object>(ref this.objectArray, this.objectArray.Length * 2);
				}
				object[] array2 = this.objectArray;
				int num = this.count;
				this.count = num + 1;
				array2[num] = obj;
				return;
			}
			if (this.objectDictionary == null)
			{
				this.objectDictionary = new Dictionary<object, object>();
			}
			this.objectDictionary.Add(obj, null);
			this.count++;
		}

		// Token: 0x06000D39 RID: 3385 RVA: 0x00032970 File Offset: 0x00030B70
		internal void EnsureSetAsIsReference(object obj)
		{
			if (this.count == 0)
			{
				return;
			}
			if (this.count > 16)
			{
				Dictionary<object, object> dictionary = this.objectDictionary;
				this.objectDictionary.Remove(obj);
				return;
			}
			if (this.objectArray != null && this.objectArray[this.count - 1] == obj)
			{
				if (this.isReferenceArray == null)
				{
					this.isReferenceArray = new bool[4];
				}
				else if (this.count == this.isReferenceArray.Length)
				{
					Array.Resize<bool>(ref this.isReferenceArray, this.isReferenceArray.Length * 2);
				}
				this.isReferenceArray[this.count - 1] = true;
			}
		}

		// Token: 0x06000D3A RID: 3386 RVA: 0x00032A0A File Offset: 0x00030C0A
		internal void Pop(object obj)
		{
			if (this.count > 16)
			{
				Dictionary<object, object> dictionary = this.objectDictionary;
				this.objectDictionary.Remove(obj);
			}
			this.count--;
		}

		// Token: 0x06000D3B RID: 3387 RVA: 0x00032A38 File Offset: 0x00030C38
		internal bool Contains(object obj)
		{
			int num = this.count;
			if (num > 16)
			{
				if (this.objectDictionary != null && this.objectDictionary.ContainsKey(obj))
				{
					return true;
				}
				num = 16;
			}
			for (int i = num - 1; i >= 0; i--)
			{
				if (obj == this.objectArray[i] && this.isReferenceArray != null && !this.isReferenceArray[i])
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x06000D3C RID: 3388 RVA: 0x00032A9A File Offset: 0x00030C9A
		internal int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x04000633 RID: 1587
		private const int MaximumArraySize = 16;

		// Token: 0x04000634 RID: 1588
		private const int InitialArraySize = 4;

		// Token: 0x04000635 RID: 1589
		private int count;

		// Token: 0x04000636 RID: 1590
		private object[] objectArray;

		// Token: 0x04000637 RID: 1591
		private bool[] isReferenceArray;

		// Token: 0x04000638 RID: 1592
		private Dictionary<object, object> objectDictionary;
	}
}
