﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000126 RID: 294
	internal sealed class TypeInformation
	{
		// Token: 0x06000E03 RID: 3587 RVA: 0x00033EA7 File Offset: 0x000320A7
		internal TypeInformation(string fullTypeName, string assemblyString, bool hasTypeForwardedFrom)
		{
			this.fullTypeName = fullTypeName;
			this.assemblyString = assemblyString;
			this.hasTypeForwardedFrom = hasTypeForwardedFrom;
		}

		// Token: 0x17000300 RID: 768
		// (get) Token: 0x06000E04 RID: 3588 RVA: 0x00033EC4 File Offset: 0x000320C4
		internal string FullTypeName
		{
			get
			{
				return this.fullTypeName;
			}
		}

		// Token: 0x17000301 RID: 769
		// (get) Token: 0x06000E05 RID: 3589 RVA: 0x00033ECC File Offset: 0x000320CC
		internal string AssemblyString
		{
			get
			{
				return this.assemblyString;
			}
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06000E06 RID: 3590 RVA: 0x00033ED4 File Offset: 0x000320D4
		internal bool HasTypeForwardedFrom
		{
			get
			{
				return this.hasTypeForwardedFrom;
			}
		}

		// Token: 0x0400064B RID: 1611
		private string fullTypeName;

		// Token: 0x0400064C RID: 1612
		private string assemblyString;

		// Token: 0x0400064D RID: 1613
		private bool hasTypeForwardedFrom;
	}
}
