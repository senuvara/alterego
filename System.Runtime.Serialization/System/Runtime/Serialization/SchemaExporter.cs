﻿using System;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace System.Runtime.Serialization
{
	// Token: 0x02000151 RID: 337
	internal class SchemaExporter
	{
		// Token: 0x0600112F RID: 4399 RVA: 0x0003D3DC File Offset: 0x0003B5DC
		internal static void GetXmlTypeInfo(Type type, out XmlQualifiedName stableName, out XmlSchemaType xsdType, out bool hasRoot)
		{
			if (SchemaExporter.IsSpecialXmlType(type, out stableName, out xsdType, out hasRoot))
			{
				return;
			}
			SchemaExporter.InvokeSchemaProviderMethod(type, new XmlSchemaSet
			{
				XmlResolver = null
			}, out stableName, out xsdType, out hasRoot);
			if (stableName.Name == null || stableName.Name.Length == 0)
			{
				throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("XML data contract Name for type '{0}' cannot be set to null or empty string.", new object[]
				{
					DataContract.GetClrTypeFullName(type)
				})));
			}
		}

		// Token: 0x06001130 RID: 4400 RVA: 0x0003D448 File Offset: 0x0003B648
		internal static bool IsSpecialXmlType(Type type, out XmlQualifiedName typeName, out XmlSchemaType xsdType, out bool hasRoot)
		{
			xsdType = null;
			hasRoot = true;
			if (type == Globals.TypeOfXmlElement || type == Globals.TypeOfXmlNodeArray)
			{
				string name;
				if (type == Globals.TypeOfXmlElement)
				{
					xsdType = SchemaExporter.CreateAnyElementType();
					name = "XmlElement";
					hasRoot = false;
				}
				else
				{
					xsdType = SchemaExporter.CreateAnyType();
					name = "ArrayOfXmlNode";
					hasRoot = true;
				}
				typeName = new XmlQualifiedName(name, DataContract.GetDefaultStableNamespace(type));
				return true;
			}
			typeName = null;
			return false;
		}

		// Token: 0x06001131 RID: 4401 RVA: 0x0003D4BA File Offset: 0x0003B6BA
		internal static void AddDefaultXmlType(XmlSchemaSet schemas, string localName, string ns)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001132 RID: 4402 RVA: 0x0003D4C4 File Offset: 0x0003B6C4
		private static bool InvokeSchemaProviderMethod(Type clrType, XmlSchemaSet schemas, out XmlQualifiedName stableName, out XmlSchemaType xsdType, out bool hasRoot)
		{
			xsdType = null;
			hasRoot = true;
			object[] customAttributes = clrType.GetCustomAttributes(Globals.TypeOfXmlSchemaProviderAttribute, false);
			if (customAttributes == null || customAttributes.Length == 0)
			{
				stableName = DataContract.GetDefaultStableName(clrType);
				return false;
			}
			XmlSchemaProviderAttribute xmlSchemaProviderAttribute = (XmlSchemaProviderAttribute)customAttributes[0];
			if (xmlSchemaProviderAttribute.IsAny)
			{
				xsdType = SchemaExporter.CreateAnyElementType();
				hasRoot = false;
			}
			string methodName = xmlSchemaProviderAttribute.MethodName;
			if (methodName == null || methodName.Length == 0)
			{
				if (!xmlSchemaProviderAttribute.IsAny)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' cannot have MethodName on XmlSchemaProviderAttribute attribute set to null or empty string.", new object[]
					{
						DataContract.GetClrTypeFullName(clrType)
					})));
				}
				stableName = DataContract.GetDefaultStableName(clrType);
			}
			else
			{
				MethodInfo method = clrType.GetMethod(methodName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, new Type[]
				{
					typeof(XmlSchemaSet)
				}, null);
				if (method == null)
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Type '{0}' does not have a static method '{1}' that takes a parameter of type 'System.Xml.Schema.XmlSchemaSet' as specified by the XmlSchemaProviderAttribute attribute.", new object[]
					{
						DataContract.GetClrTypeFullName(clrType),
						methodName
					})));
				}
				if (!Globals.TypeOfXmlQualifiedName.IsAssignableFrom(method.ReturnType) && !Globals.TypeOfXmlSchemaType.IsAssignableFrom(method.ReturnType))
				{
					throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Method '{0}.{1}()' returns '{2}'. The return type must be compatible with '{3}'.", new object[]
					{
						DataContract.GetClrTypeFullName(clrType),
						methodName,
						DataContract.GetClrTypeFullName(method.ReturnType),
						DataContract.GetClrTypeFullName(Globals.TypeOfXmlQualifiedName),
						typeof(XmlSchemaType)
					})));
				}
				object obj = method.Invoke(null, new object[]
				{
					schemas
				});
				if (xmlSchemaProviderAttribute.IsAny)
				{
					if (obj != null)
					{
						throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Method '{0}.{1}()' returns a non-null value. The return value must be null since IsAny=true.", new object[]
						{
							DataContract.GetClrTypeFullName(clrType),
							methodName
						})));
					}
					stableName = DataContract.GetDefaultStableName(clrType);
				}
				else if (obj == null)
				{
					xsdType = SchemaExporter.CreateAnyElementType();
					hasRoot = false;
					stableName = DataContract.GetDefaultStableName(clrType);
				}
				else
				{
					XmlSchemaType xmlSchemaType = obj as XmlSchemaType;
					if (xmlSchemaType != null)
					{
						string name = xmlSchemaType.Name;
						string text = null;
						if (name == null || name.Length == 0)
						{
							DataContract.GetDefaultStableName(DataContract.GetClrTypeFullName(clrType), out name, out text);
							stableName = new XmlQualifiedName(name, text);
							xmlSchemaType.Annotation = SchemaExporter.GetSchemaAnnotation(new XmlNode[]
							{
								SchemaExporter.ExportActualType(stableName, new XmlDocument())
							});
							xsdType = xmlSchemaType;
						}
						else
						{
							foreach (object obj2 in schemas.Schemas())
							{
								XmlSchema xmlSchema = (XmlSchema)obj2;
								using (XmlSchemaObjectEnumerator enumerator2 = xmlSchema.Items.GetEnumerator())
								{
									while (enumerator2.MoveNext())
									{
										if (enumerator2.Current == xmlSchemaType)
										{
											text = xmlSchema.TargetNamespace;
											if (text == null)
											{
												text = string.Empty;
												break;
											}
											break;
										}
									}
								}
								if (text != null)
								{
									break;
								}
							}
							if (text == null)
							{
								throw DiagnosticUtility.ExceptionUtility.ThrowHelperError(new InvalidDataContractException(SR.GetString("Schema type '{0}' is missing and required for '{1}' type.", new object[]
								{
									name,
									DataContract.GetClrTypeFullName(clrType)
								})));
							}
							stableName = new XmlQualifiedName(name, text);
						}
					}
					else
					{
						stableName = (XmlQualifiedName)obj;
					}
				}
			}
			return true;
		}

		// Token: 0x06001133 RID: 4403 RVA: 0x0003D7F4 File Offset: 0x0003B9F4
		private static XmlSchemaComplexType CreateAnyElementType()
		{
			XmlSchemaComplexType xmlSchemaComplexType = new XmlSchemaComplexType();
			xmlSchemaComplexType.IsMixed = false;
			xmlSchemaComplexType.Particle = new XmlSchemaSequence();
			XmlSchemaAny xmlSchemaAny = new XmlSchemaAny();
			xmlSchemaAny.MinOccurs = 0m;
			xmlSchemaAny.ProcessContents = XmlSchemaContentProcessing.Lax;
			((XmlSchemaSequence)xmlSchemaComplexType.Particle).Items.Add(xmlSchemaAny);
			return xmlSchemaComplexType;
		}

		// Token: 0x06001134 RID: 4404 RVA: 0x0003D848 File Offset: 0x0003BA48
		private static XmlSchemaAnnotation GetSchemaAnnotation(params XmlNode[] nodes)
		{
			if (nodes == null || nodes.Length == 0)
			{
				return null;
			}
			bool flag = false;
			for (int i = 0; i < nodes.Length; i++)
			{
				if (nodes[i] != null)
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				return null;
			}
			XmlSchemaAnnotation xmlSchemaAnnotation = new XmlSchemaAnnotation();
			XmlSchemaAppInfo xmlSchemaAppInfo = new XmlSchemaAppInfo();
			xmlSchemaAnnotation.Items.Add(xmlSchemaAppInfo);
			xmlSchemaAppInfo.Markup = nodes;
			return xmlSchemaAnnotation;
		}

		// Token: 0x06001135 RID: 4405 RVA: 0x0003D89C File Offset: 0x0003BA9C
		private static XmlSchemaComplexType CreateAnyType()
		{
			XmlSchemaComplexType xmlSchemaComplexType = new XmlSchemaComplexType();
			xmlSchemaComplexType.IsMixed = true;
			xmlSchemaComplexType.Particle = new XmlSchemaSequence();
			XmlSchemaAny xmlSchemaAny = new XmlSchemaAny();
			xmlSchemaAny.MinOccurs = 0m;
			xmlSchemaAny.MaxOccurs = decimal.MaxValue;
			xmlSchemaAny.ProcessContents = XmlSchemaContentProcessing.Lax;
			((XmlSchemaSequence)xmlSchemaComplexType.Particle).Items.Add(xmlSchemaAny);
			xmlSchemaComplexType.AnyAttribute = new XmlSchemaAnyAttribute();
			return xmlSchemaComplexType;
		}

		// Token: 0x06001136 RID: 4406 RVA: 0x0003D90C File Offset: 0x0003BB0C
		private static XmlElement ExportActualType(XmlQualifiedName typeName, XmlDocument xmlDoc)
		{
			XmlElement xmlElement = xmlDoc.CreateElement(SchemaExporter.ActualTypeAnnotationName.Name, SchemaExporter.ActualTypeAnnotationName.Namespace);
			XmlAttribute xmlAttribute = xmlDoc.CreateAttribute("Name");
			xmlAttribute.Value = typeName.Name;
			xmlElement.Attributes.Append(xmlAttribute);
			XmlAttribute xmlAttribute2 = xmlDoc.CreateAttribute("Namespace");
			xmlAttribute2.Value = typeName.Namespace;
			xmlElement.Attributes.Append(xmlAttribute2);
			return xmlElement;
		}

		// Token: 0x170003BA RID: 954
		// (get) Token: 0x06001137 RID: 4407 RVA: 0x0003D97D File Offset: 0x0003BB7D
		internal static XmlQualifiedName ActualTypeAnnotationName
		{
			get
			{
				if (SchemaExporter.actualTypeAnnotationName == null)
				{
					SchemaExporter.actualTypeAnnotationName = new XmlQualifiedName("ActualType", "http://schemas.microsoft.com/2003/10/Serialization/");
				}
				return SchemaExporter.actualTypeAnnotationName;
			}
		}

		// Token: 0x06001138 RID: 4408 RVA: 0x00002217 File Offset: 0x00000417
		public SchemaExporter()
		{
		}

		// Token: 0x04000911 RID: 2321
		private static XmlQualifiedName actualTypeAnnotationName;
	}
}
