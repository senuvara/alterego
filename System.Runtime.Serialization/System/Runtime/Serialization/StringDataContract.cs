﻿using System;
using System.Xml;

namespace System.Runtime.Serialization
{
	// Token: 0x02000102 RID: 258
	internal class StringDataContract : PrimitiveDataContract
	{
		// Token: 0x06000DAD RID: 3501 RVA: 0x00033722 File Offset: 0x00031922
		internal StringDataContract() : this(DictionaryGlobals.StringLocalName, DictionaryGlobals.SchemaNamespace)
		{
		}

		// Token: 0x06000DAE RID: 3502 RVA: 0x00033734 File Offset: 0x00031934
		internal StringDataContract(XmlDictionaryString name, XmlDictionaryString ns) : base(typeof(string), name, ns)
		{
		}

		// Token: 0x170002EA RID: 746
		// (get) Token: 0x06000DAF RID: 3503 RVA: 0x00033748 File Offset: 0x00031948
		internal override string WriteMethodName
		{
			get
			{
				return "WriteString";
			}
		}

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x06000DB0 RID: 3504 RVA: 0x0003374F File Offset: 0x0003194F
		internal override string ReadMethodName
		{
			get
			{
				return "ReadElementContentAsString";
			}
		}

		// Token: 0x06000DB1 RID: 3505 RVA: 0x00033756 File Offset: 0x00031956
		public override void WriteXmlValue(XmlWriterDelegator writer, object obj, XmlObjectSerializerWriteContext context)
		{
			writer.WriteString((string)obj);
		}

		// Token: 0x06000DB2 RID: 3506 RVA: 0x00033764 File Offset: 0x00031964
		public override object ReadXmlValue(XmlReaderDelegator reader, XmlObjectSerializerReadContext context)
		{
			if (context != null)
			{
				return base.HandleReadValue(reader.ReadElementContentAsString(), context);
			}
			if (!base.TryReadNullAtTopLevel(reader))
			{
				return reader.ReadElementContentAsString();
			}
			return null;
		}
	}
}
