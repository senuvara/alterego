﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x02000196 RID: 406
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x0600140C RID: 5132 RVA: 0x0004AC70 File Offset: 0x00048E70
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x04000A41 RID: 2625 RVA: 0x0004AEC0 File Offset: 0x000490C0
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 0953DF544832295E4A5B19928F95C351F25DA86A;

	// Token: 0x04000A42 RID: 2626 RVA: 0x0004AF00 File Offset: 0x00049100
	internal static readonly long 14A9DC09E10179B15BEAF94C0AED53904ACE0336;

	// Token: 0x04000A43 RID: 2627 RVA: 0x0004AF08 File Offset: 0x00049108
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=5 226CF119B78825F1720CF2CA485C2D85113D68C6;

	// Token: 0x04000A44 RID: 2628 RVA: 0x0004AF0D File Offset: 0x0004910D
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=30 2A819C73D5892FC12A45F8232E1E03B764F9EB84;

	// Token: 0x04000A45 RID: 2629 RVA: 0x0004AF2B File Offset: 0x0004912B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=26 32D10C7B8CF96570CA04CE37F2A19D84240D3A89;

	// Token: 0x04000A46 RID: 2630 RVA: 0x0004AF45 File Offset: 0x00049145
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 364A37E6F67B6074B93346D4E369336F4433B1C5;

	// Token: 0x04000A47 RID: 2631 RVA: 0x0004AF48 File Offset: 0x00049148
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 4A9C42A40F7A2C45C3BC0622FC68D70E1194A14A;

	// Token: 0x04000A48 RID: 2632 RVA: 0x0004B048 File Offset: 0x00049248
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=80 52642297909BC88C99EAEE55EE6CC77EB807B54E;

	// Token: 0x04000A49 RID: 2633 RVA: 0x0004B098 File Offset: 0x00049298
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=39 577C1328D31B8277AC320C63F2A94D9988BFC9C1;

	// Token: 0x04000A4A RID: 2634 RVA: 0x0004B0BF File Offset: 0x000492BF
	internal static readonly int 66928E6CBB59C3A3BCE606959EF4A865FE04E642;

	// Token: 0x04000A4B RID: 2635 RVA: 0x0004B0C3 File Offset: 0x000492C3
	internal static readonly int 6AD618784EF9475526FEC47F0466559A788B4A49;

	// Token: 0x04000A4C RID: 2636 RVA: 0x0004B0C7 File Offset: 0x000492C7
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 71599C20F1747A39DEDFB726ADBFA0E04AA71448;

	// Token: 0x04000A4D RID: 2637 RVA: 0x0004B107 File Offset: 0x00049307
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 79103F7D0A100F893D3D18C66F3F8397ED8BFFD9;

	// Token: 0x04000A4E RID: 2638 RVA: 0x0004B10D File Offset: 0x0004930D
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=512 9D35D7AE7F9C8159DCA3A47B8D6D155D5B53897C;

	// Token: 0x04000A4F RID: 2639 RVA: 0x0004B30D File Offset: 0x0004950D
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 9E8419627B46E6737DEF6B4A11ED2CFF7138723F;

	// Token: 0x04000A50 RID: 2640 RVA: 0x0004B40D File Offset: 0x0004960D
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=128 A11F11A5D2F4D7E2E917B8E9B818AE383B384C7D;

	// Token: 0x04000A51 RID: 2641 RVA: 0x0004B48D File Offset: 0x0004968D
	internal static readonly long C7332892AD7D1974562844144E505B853270A0A3;

	// Token: 0x04000A52 RID: 2642 RVA: 0x0004B495 File Offset: 0x00049695
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=38 CD0295D6D69F945B4B3C7D09BEDB2D10B0883CDC;

	// Token: 0x04000A53 RID: 2643 RVA: 0x0004B4BB File Offset: 0x000496BB
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=16 CE27CB141098FEB00714E758646BE3E99C185B71;

	// Token: 0x04000A54 RID: 2644 RVA: 0x0004B4CB File Offset: 0x000496CB
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 DCC1E88AA44895E53932C0DE79C8AF42EB190A95;

	// Token: 0x04000A55 RID: 2645 RVA: 0x0004B50B File Offset: 0x0004970B
	internal static readonly long E767C9DC651168B3700C26F778E2004D2764ADD4;

	// Token: 0x04000A56 RID: 2646 RVA: 0x0004B513 File Offset: 0x00049713
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 E7B33C1F49B2907914D67770D8F998E0EFBF2532;

	// Token: 0x04000A57 RID: 2647 RVA: 0x0004B553 File Offset: 0x00049753
	internal static readonly long EBC658B067B5C785A3F0BB67D73755F6FEE7F70C;

	// Token: 0x04000A58 RID: 2648 RVA: 0x0004B55B File Offset: 0x0004975B
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=128 FA892ED77B87DDCFE5D25380151613AACA162A99;

	// Token: 0x02000197 RID: 407
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 3)]
	private struct __StaticArrayInitTypeSize=3
	{
	}

	// Token: 0x02000198 RID: 408
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 5)]
	private struct __StaticArrayInitTypeSize=5
	{
	}

	// Token: 0x02000199 RID: 409
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 6)]
	private struct __StaticArrayInitTypeSize=6
	{
	}

	// Token: 0x0200019A RID: 410
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 16)]
	private struct __StaticArrayInitTypeSize=16
	{
	}

	// Token: 0x0200019B RID: 411
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 26)]
	private struct __StaticArrayInitTypeSize=26
	{
	}

	// Token: 0x0200019C RID: 412
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 30)]
	private struct __StaticArrayInitTypeSize=30
	{
	}

	// Token: 0x0200019D RID: 413
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 38)]
	private struct __StaticArrayInitTypeSize=38
	{
	}

	// Token: 0x0200019E RID: 414
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 39)]
	private struct __StaticArrayInitTypeSize=39
	{
	}

	// Token: 0x0200019F RID: 415
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 64)]
	private struct __StaticArrayInitTypeSize=64
	{
	}

	// Token: 0x020001A0 RID: 416
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 80)]
	private struct __StaticArrayInitTypeSize=80
	{
	}

	// Token: 0x020001A1 RID: 417
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 128)]
	private struct __StaticArrayInitTypeSize=128
	{
	}

	// Token: 0x020001A2 RID: 418
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 256)]
	private struct __StaticArrayInitTypeSize=256
	{
	}

	// Token: 0x020001A3 RID: 419
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 512)]
	private struct __StaticArrayInitTypeSize=512
	{
	}
}
