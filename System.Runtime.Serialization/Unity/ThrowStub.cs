﻿using System;

namespace Unity
{
	// Token: 0x020001B0 RID: 432
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x0600147B RID: 5243 RVA: 0x0004AEB7 File Offset: 0x000490B7
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
