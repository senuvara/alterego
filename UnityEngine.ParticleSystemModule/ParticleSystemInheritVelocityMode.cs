﻿using System;

namespace UnityEngine
{
	// Token: 0x02000038 RID: 56
	public enum ParticleSystemInheritVelocityMode
	{
		// Token: 0x040000C5 RID: 197
		Initial,
		// Token: 0x040000C6 RID: 198
		Current
	}
}
