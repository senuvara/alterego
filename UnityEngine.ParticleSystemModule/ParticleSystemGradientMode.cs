﻿using System;

namespace UnityEngine
{
	// Token: 0x02000028 RID: 40
	public enum ParticleSystemGradientMode
	{
		// Token: 0x04000074 RID: 116
		Color,
		// Token: 0x04000075 RID: 117
		Gradient,
		// Token: 0x04000076 RID: 118
		TwoColors,
		// Token: 0x04000077 RID: 119
		TwoGradients,
		// Token: 0x04000078 RID: 120
		RandomColor
	}
}
