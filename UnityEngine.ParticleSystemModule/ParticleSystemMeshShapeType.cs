﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002A RID: 42
	public enum ParticleSystemMeshShapeType
	{
		// Token: 0x04000090 RID: 144
		Vertex,
		// Token: 0x04000091 RID: 145
		Edge,
		// Token: 0x04000092 RID: 146
		Triangle
	}
}
