﻿using System;

namespace UnityEngine
{
	// Token: 0x02000043 RID: 67
	public enum ParticleSystemRingBufferMode
	{
		// Token: 0x04000121 RID: 289
		Disabled,
		// Token: 0x04000122 RID: 290
		PauseUntilReplaced,
		// Token: 0x04000123 RID: 291
		LoopUntilReplaced
	}
}
