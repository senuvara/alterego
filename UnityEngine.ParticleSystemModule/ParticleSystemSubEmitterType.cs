﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003E RID: 62
	public enum ParticleSystemSubEmitterType
	{
		// Token: 0x04000106 RID: 262
		Birth,
		// Token: 0x04000107 RID: 263
		Collision,
		// Token: 0x04000108 RID: 264
		Death,
		// Token: 0x04000109 RID: 265
		Trigger,
		// Token: 0x0400010A RID: 266
		Manual
	}
}
