﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002C RID: 44
	public enum ParticleSystemAnimationMode
	{
		// Token: 0x04000099 RID: 153
		Grid,
		// Token: 0x0400009A RID: 154
		Sprites
	}
}
