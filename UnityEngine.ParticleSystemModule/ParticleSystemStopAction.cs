﻿using System;

namespace UnityEngine
{
	// Token: 0x02000035 RID: 53
	public enum ParticleSystemStopAction
	{
		// Token: 0x040000B8 RID: 184
		None,
		// Token: 0x040000B9 RID: 185
		Disable,
		// Token: 0x040000BA RID: 186
		Destroy,
		// Token: 0x040000BB RID: 187
		Callback
	}
}
