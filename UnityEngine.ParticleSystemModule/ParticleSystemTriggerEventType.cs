﻿using System;

namespace UnityEngine
{
	// Token: 0x02000039 RID: 57
	public enum ParticleSystemTriggerEventType
	{
		// Token: 0x040000C8 RID: 200
		Inside,
		// Token: 0x040000C9 RID: 201
		Outside,
		// Token: 0x040000CA RID: 202
		Enter,
		// Token: 0x040000CB RID: 203
		Exit
	}
}
