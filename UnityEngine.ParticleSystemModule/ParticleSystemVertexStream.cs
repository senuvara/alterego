﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200003A RID: 58
	[UsedByNativeCode]
	public enum ParticleSystemVertexStream
	{
		// Token: 0x040000CD RID: 205
		Position,
		// Token: 0x040000CE RID: 206
		Normal,
		// Token: 0x040000CF RID: 207
		Tangent,
		// Token: 0x040000D0 RID: 208
		Color,
		// Token: 0x040000D1 RID: 209
		UV,
		// Token: 0x040000D2 RID: 210
		UV2,
		// Token: 0x040000D3 RID: 211
		UV3,
		// Token: 0x040000D4 RID: 212
		UV4,
		// Token: 0x040000D5 RID: 213
		AnimBlend,
		// Token: 0x040000D6 RID: 214
		AnimFrame,
		// Token: 0x040000D7 RID: 215
		Center,
		// Token: 0x040000D8 RID: 216
		VertexID,
		// Token: 0x040000D9 RID: 217
		SizeX,
		// Token: 0x040000DA RID: 218
		SizeXY,
		// Token: 0x040000DB RID: 219
		SizeXYZ,
		// Token: 0x040000DC RID: 220
		Rotation,
		// Token: 0x040000DD RID: 221
		Rotation3D,
		// Token: 0x040000DE RID: 222
		RotationSpeed,
		// Token: 0x040000DF RID: 223
		RotationSpeed3D,
		// Token: 0x040000E0 RID: 224
		Velocity,
		// Token: 0x040000E1 RID: 225
		Speed,
		// Token: 0x040000E2 RID: 226
		AgePercent,
		// Token: 0x040000E3 RID: 227
		InvStartLifetime,
		// Token: 0x040000E4 RID: 228
		StableRandomX,
		// Token: 0x040000E5 RID: 229
		StableRandomXY,
		// Token: 0x040000E6 RID: 230
		StableRandomXYZ,
		// Token: 0x040000E7 RID: 231
		StableRandomXYZW,
		// Token: 0x040000E8 RID: 232
		VaryingRandomX,
		// Token: 0x040000E9 RID: 233
		VaryingRandomXY,
		// Token: 0x040000EA RID: 234
		VaryingRandomXYZ,
		// Token: 0x040000EB RID: 235
		VaryingRandomXYZW,
		// Token: 0x040000EC RID: 236
		Custom1X,
		// Token: 0x040000ED RID: 237
		Custom1XY,
		// Token: 0x040000EE RID: 238
		Custom1XYZ,
		// Token: 0x040000EF RID: 239
		Custom1XYZW,
		// Token: 0x040000F0 RID: 240
		Custom2X,
		// Token: 0x040000F1 RID: 241
		Custom2XY,
		// Token: 0x040000F2 RID: 242
		Custom2XYZ,
		// Token: 0x040000F3 RID: 243
		Custom2XYZW,
		// Token: 0x040000F4 RID: 244
		NoiseSumX,
		// Token: 0x040000F5 RID: 245
		NoiseSumXY,
		// Token: 0x040000F6 RID: 246
		NoiseSumXYZ,
		// Token: 0x040000F7 RID: 247
		NoiseImpulseX,
		// Token: 0x040000F8 RID: 248
		NoiseImpulseXY,
		// Token: 0x040000F9 RID: 249
		NoiseImpulseXYZ
	}
}
