﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000022 RID: 34
	[NativeHeader("Runtime/ParticleSystem/ParticleSystemRenderer.h")]
	[NativeHeader("ParticleSystemScriptingClasses.h")]
	[RequireComponent(typeof(Transform))]
	[RequireComponent(typeof(Transform))]
	public sealed class ParticleSystemRenderer : Renderer
	{
		// Token: 0x06000648 RID: 1608 RVA: 0x00008074 File Offset: 0x00006274
		public ParticleSystemRenderer()
		{
		}

		// Token: 0x06000649 RID: 1609 RVA: 0x0000807C File Offset: 0x0000627C
		[Obsolete("EnableVertexStreams is deprecated.Use SetActiveVertexStreams instead.", false)]
		public void EnableVertexStreams(ParticleSystemVertexStreams streams)
		{
			this.Internal_SetVertexStreams(streams, true);
		}

		// Token: 0x0600064A RID: 1610 RVA: 0x00008087 File Offset: 0x00006287
		[Obsolete("DisableVertexStreams is deprecated.Use SetActiveVertexStreams instead.", false)]
		public void DisableVertexStreams(ParticleSystemVertexStreams streams)
		{
			this.Internal_SetVertexStreams(streams, false);
		}

		// Token: 0x0600064B RID: 1611 RVA: 0x00008094 File Offset: 0x00006294
		[Obsolete("AreVertexStreamsEnabled is deprecated.Use GetActiveVertexStreams instead.", false)]
		public bool AreVertexStreamsEnabled(ParticleSystemVertexStreams streams)
		{
			return this.Internal_GetEnabledVertexStreams(streams) == streams;
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x000080B4 File Offset: 0x000062B4
		[Obsolete("GetEnabledVertexStreams is deprecated.Use GetActiveVertexStreams instead.", false)]
		public ParticleSystemVertexStreams GetEnabledVertexStreams(ParticleSystemVertexStreams streams)
		{
			return this.Internal_GetEnabledVertexStreams(streams);
		}

		// Token: 0x0600064D RID: 1613 RVA: 0x000080D0 File Offset: 0x000062D0
		[Obsolete("Internal_SetVertexStreams is deprecated.Use SetActiveVertexStreams instead.", false)]
		internal void Internal_SetVertexStreams(ParticleSystemVertexStreams streams, bool enabled)
		{
			List<ParticleSystemVertexStream> list = new List<ParticleSystemVertexStream>(this.activeVertexStreamsCount);
			this.GetActiveVertexStreams(list);
			if (enabled)
			{
				if ((streams & ParticleSystemVertexStreams.Position) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.Position))
					{
						list.Add(ParticleSystemVertexStream.Position);
					}
				}
				if ((streams & ParticleSystemVertexStreams.Normal) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.Normal))
					{
						list.Add(ParticleSystemVertexStream.Normal);
					}
				}
				if ((streams & ParticleSystemVertexStreams.Tangent) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.Tangent))
					{
						list.Add(ParticleSystemVertexStream.Tangent);
					}
				}
				if ((streams & ParticleSystemVertexStreams.Color) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.Color))
					{
						list.Add(ParticleSystemVertexStream.Color);
					}
				}
				if ((streams & ParticleSystemVertexStreams.UV) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.UV))
					{
						list.Add(ParticleSystemVertexStream.UV);
					}
				}
				if ((streams & ParticleSystemVertexStreams.UV2BlendAndFrame) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.UV2))
					{
						list.Add(ParticleSystemVertexStream.UV2);
						list.Add(ParticleSystemVertexStream.AnimBlend);
						list.Add(ParticleSystemVertexStream.AnimFrame);
					}
				}
				if ((streams & ParticleSystemVertexStreams.CenterAndVertexID) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.Center))
					{
						list.Add(ParticleSystemVertexStream.Center);
						list.Add(ParticleSystemVertexStream.VertexID);
					}
				}
				if ((streams & ParticleSystemVertexStreams.Size) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.SizeXYZ))
					{
						list.Add(ParticleSystemVertexStream.SizeXYZ);
					}
				}
				if ((streams & ParticleSystemVertexStreams.Rotation) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.Rotation3D))
					{
						list.Add(ParticleSystemVertexStream.Rotation3D);
					}
				}
				if ((streams & ParticleSystemVertexStreams.Velocity) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.Velocity))
					{
						list.Add(ParticleSystemVertexStream.Velocity);
					}
				}
				if ((streams & ParticleSystemVertexStreams.Lifetime) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.AgePercent))
					{
						list.Add(ParticleSystemVertexStream.AgePercent);
						list.Add(ParticleSystemVertexStream.InvStartLifetime);
					}
				}
				if ((streams & ParticleSystemVertexStreams.Custom1) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.Custom1XYZW))
					{
						list.Add(ParticleSystemVertexStream.Custom1XYZW);
					}
				}
				if ((streams & ParticleSystemVertexStreams.Custom2) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.Custom2XYZW))
					{
						list.Add(ParticleSystemVertexStream.Custom2XYZW);
					}
				}
				if ((streams & ParticleSystemVertexStreams.Random) != ParticleSystemVertexStreams.None)
				{
					if (!list.Contains(ParticleSystemVertexStream.StableRandomXYZ))
					{
						list.Add(ParticleSystemVertexStream.StableRandomXYZ);
						list.Add(ParticleSystemVertexStream.VaryingRandomX);
					}
				}
			}
			else
			{
				if ((streams & ParticleSystemVertexStreams.Position) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.Position);
				}
				if ((streams & ParticleSystemVertexStreams.Normal) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.Normal);
				}
				if ((streams & ParticleSystemVertexStreams.Tangent) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.Tangent);
				}
				if ((streams & ParticleSystemVertexStreams.Color) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.Color);
				}
				if ((streams & ParticleSystemVertexStreams.UV) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.UV);
				}
				if ((streams & ParticleSystemVertexStreams.UV2BlendAndFrame) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.UV2);
					list.Remove(ParticleSystemVertexStream.AnimBlend);
					list.Remove(ParticleSystemVertexStream.AnimFrame);
				}
				if ((streams & ParticleSystemVertexStreams.CenterAndVertexID) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.Center);
					list.Remove(ParticleSystemVertexStream.VertexID);
				}
				if ((streams & ParticleSystemVertexStreams.Size) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.SizeXYZ);
				}
				if ((streams & ParticleSystemVertexStreams.Rotation) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.Rotation3D);
				}
				if ((streams & ParticleSystemVertexStreams.Velocity) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.Velocity);
				}
				if ((streams & ParticleSystemVertexStreams.Lifetime) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.AgePercent);
					list.Remove(ParticleSystemVertexStream.InvStartLifetime);
				}
				if ((streams & ParticleSystemVertexStreams.Custom1) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.Custom1XYZW);
				}
				if ((streams & ParticleSystemVertexStreams.Custom2) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.Custom2XYZW);
				}
				if ((streams & ParticleSystemVertexStreams.Random) != ParticleSystemVertexStreams.None)
				{
					list.Remove(ParticleSystemVertexStream.StableRandomXYZW);
					list.Remove(ParticleSystemVertexStream.VaryingRandomX);
				}
			}
			this.SetActiveVertexStreams(list);
		}

		// Token: 0x0600064E RID: 1614 RVA: 0x00008460 File Offset: 0x00006660
		[Obsolete("Internal_GetVertexStreams is deprecated.Use GetActiveVertexStreams instead.", false)]
		internal ParticleSystemVertexStreams Internal_GetEnabledVertexStreams(ParticleSystemVertexStreams streams)
		{
			List<ParticleSystemVertexStream> list = new List<ParticleSystemVertexStream>(this.activeVertexStreamsCount);
			this.GetActiveVertexStreams(list);
			ParticleSystemVertexStreams particleSystemVertexStreams = ParticleSystemVertexStreams.None;
			if (list.Contains(ParticleSystemVertexStream.Position))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Position;
			}
			if (list.Contains(ParticleSystemVertexStream.Normal))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Normal;
			}
			if (list.Contains(ParticleSystemVertexStream.Tangent))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Tangent;
			}
			if (list.Contains(ParticleSystemVertexStream.Color))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Color;
			}
			if (list.Contains(ParticleSystemVertexStream.UV))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.UV;
			}
			if (list.Contains(ParticleSystemVertexStream.UV2))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.UV2BlendAndFrame;
			}
			if (list.Contains(ParticleSystemVertexStream.Center))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.CenterAndVertexID;
			}
			if (list.Contains(ParticleSystemVertexStream.SizeXYZ))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Size;
			}
			if (list.Contains(ParticleSystemVertexStream.Rotation3D))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Rotation;
			}
			if (list.Contains(ParticleSystemVertexStream.Velocity))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Velocity;
			}
			if (list.Contains(ParticleSystemVertexStream.AgePercent))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Lifetime;
			}
			if (list.Contains(ParticleSystemVertexStream.Custom1XYZW))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Custom1;
			}
			if (list.Contains(ParticleSystemVertexStream.Custom2XYZW))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Custom2;
			}
			if (list.Contains(ParticleSystemVertexStream.StableRandomXYZ))
			{
				particleSystemVertexStreams |= ParticleSystemVertexStreams.Random;
			}
			return particleSystemVertexStreams & streams;
		}

		// Token: 0x170001AB RID: 427
		// (get) Token: 0x0600064F RID: 1615
		// (set) Token: 0x06000650 RID: 1616
		[NativeName("RenderAlignment")]
		public extern ParticleSystemRenderSpace alignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001AC RID: 428
		// (get) Token: 0x06000651 RID: 1617
		// (set) Token: 0x06000652 RID: 1618
		public extern ParticleSystemRenderMode renderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x06000653 RID: 1619
		// (set) Token: 0x06000654 RID: 1620
		public extern ParticleSystemSortMode sortMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x06000655 RID: 1621
		// (set) Token: 0x06000656 RID: 1622
		public extern float lengthScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001AF RID: 431
		// (get) Token: 0x06000657 RID: 1623
		// (set) Token: 0x06000658 RID: 1624
		public extern float velocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x06000659 RID: 1625
		// (set) Token: 0x0600065A RID: 1626
		public extern float cameraVelocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x0600065B RID: 1627
		// (set) Token: 0x0600065C RID: 1628
		public extern float normalDirection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x0600065D RID: 1629
		// (set) Token: 0x0600065E RID: 1630
		public extern float shadowBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x0600065F RID: 1631
		// (set) Token: 0x06000660 RID: 1632
		public extern float sortingFudge { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x06000661 RID: 1633
		// (set) Token: 0x06000662 RID: 1634
		public extern float minParticleSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06000663 RID: 1635
		// (set) Token: 0x06000664 RID: 1636
		public extern float maxParticleSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06000665 RID: 1637 RVA: 0x00008594 File Offset: 0x00006794
		// (set) Token: 0x06000666 RID: 1638 RVA: 0x000085AA File Offset: 0x000067AA
		public Vector3 pivot
		{
			get
			{
				Vector3 result;
				this.get_pivot_Injected(out result);
				return result;
			}
			set
			{
				this.set_pivot_Injected(ref value);
			}
		}

		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06000667 RID: 1639 RVA: 0x000085B4 File Offset: 0x000067B4
		// (set) Token: 0x06000668 RID: 1640 RVA: 0x000085CA File Offset: 0x000067CA
		public Vector3 flip
		{
			get
			{
				Vector3 result;
				this.get_flip_Injected(out result);
				return result;
			}
			set
			{
				this.set_flip_Injected(ref value);
			}
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x06000669 RID: 1641
		// (set) Token: 0x0600066A RID: 1642
		public extern SpriteMaskInteraction maskInteraction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x0600066B RID: 1643
		// (set) Token: 0x0600066C RID: 1644
		public extern Material trailMaterial { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001BA RID: 442
		// (get) Token: 0x0600066D RID: 1645
		// (set) Token: 0x0600066E RID: 1646
		public extern bool enableGPUInstancing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001BB RID: 443
		// (get) Token: 0x0600066F RID: 1647
		// (set) Token: 0x06000670 RID: 1648
		public extern bool allowRoll { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000671 RID: 1649 RVA: 0x000085D4 File Offset: 0x000067D4
		public void BakeMesh(Mesh mesh, bool useTransform = false)
		{
			this.BakeMesh(mesh, Camera.main, useTransform);
		}

		// Token: 0x06000672 RID: 1650
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void BakeMesh([NotNull] Mesh mesh, [NotNull] Camera camera, bool useTransform = false);

		// Token: 0x06000673 RID: 1651 RVA: 0x000085E4 File Offset: 0x000067E4
		public void BakeTrailsMesh(Mesh mesh, bool useTransform = false)
		{
			this.BakeTrailsMesh(mesh, Camera.main, useTransform);
		}

		// Token: 0x06000674 RID: 1652
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void BakeTrailsMesh([NotNull] Mesh mesh, [NotNull] Camera camera, bool useTransform = false);

		// Token: 0x170001BC RID: 444
		// (get) Token: 0x06000675 RID: 1653
		// (set) Token: 0x06000676 RID: 1654
		public extern Mesh mesh { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001BD RID: 445
		// (get) Token: 0x06000677 RID: 1655 RVA: 0x000085F4 File Offset: 0x000067F4
		public int meshCount
		{
			get
			{
				return this.Internal_GetMeshCount();
			}
		}

		// Token: 0x06000678 RID: 1656
		[GeneratedByOldBindingsGenerator]
		[RequiredByNativeCode]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int Internal_GetMeshCount();

		// Token: 0x06000679 RID: 1657
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetMeshes(Mesh[] meshes);

		// Token: 0x0600067A RID: 1658 RVA: 0x0000860F File Offset: 0x0000680F
		public void SetMeshes(Mesh[] meshes)
		{
			this.SetMeshes(meshes, meshes.Length);
		}

		// Token: 0x0600067B RID: 1659
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetMeshes(Mesh[] meshes, int size);

		// Token: 0x170001BE RID: 446
		// (get) Token: 0x0600067C RID: 1660
		public extern int activeVertexStreamsCount { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600067D RID: 1661 RVA: 0x0000861C File Offset: 0x0000681C
		public void SetActiveVertexStreams(List<ParticleSystemVertexStream> streams)
		{
			if (streams == null)
			{
				throw new ArgumentNullException("streams");
			}
			this.SetActiveVertexStreamsInternal(streams);
		}

		// Token: 0x0600067E RID: 1662
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetActiveVertexStreamsInternal(object streams);

		// Token: 0x0600067F RID: 1663 RVA: 0x00008637 File Offset: 0x00006837
		public void GetActiveVertexStreams(List<ParticleSystemVertexStream> streams)
		{
			if (streams == null)
			{
				throw new ArgumentNullException("streams");
			}
			this.GetActiveVertexStreamsInternal(streams);
		}

		// Token: 0x06000680 RID: 1664
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetActiveVertexStreamsInternal(object streams);

		// Token: 0x06000681 RID: 1665
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_pivot_Injected(out Vector3 ret);

		// Token: 0x06000682 RID: 1666
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_pivot_Injected(ref Vector3 value);

		// Token: 0x06000683 RID: 1667
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_flip_Injected(out Vector3 ret);

		// Token: 0x06000684 RID: 1668
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_flip_Injected(ref Vector3 value);
	}
}
