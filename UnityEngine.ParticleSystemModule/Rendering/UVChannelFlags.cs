﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000046 RID: 70
	[Flags]
	public enum UVChannelFlags
	{
		// Token: 0x0400012E RID: 302
		UV0 = 1,
		// Token: 0x0400012F RID: 303
		UV1 = 2,
		// Token: 0x04000130 RID: 304
		UV2 = 4,
		// Token: 0x04000131 RID: 305
		UV3 = 8
	}
}
