﻿using System;

namespace UnityEngine
{
	// Token: 0x02000027 RID: 39
	public enum ParticleSystemCurveMode
	{
		// Token: 0x0400006F RID: 111
		Constant,
		// Token: 0x04000070 RID: 112
		Curve,
		// Token: 0x04000071 RID: 113
		TwoCurves,
		// Token: 0x04000072 RID: 114
		TwoConstants
	}
}
