﻿using System;

namespace UnityEngine
{
	// Token: 0x02000024 RID: 36
	public enum ParticleSystemSortMode
	{
		// Token: 0x04000060 RID: 96
		None,
		// Token: 0x04000061 RID: 97
		Distance,
		// Token: 0x04000062 RID: 98
		OldestInFront,
		// Token: 0x04000063 RID: 99
		YoungestInFront
	}
}
