﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003C RID: 60
	public enum ParticleSystemCustomDataMode
	{
		// Token: 0x040000FE RID: 254
		Disabled,
		// Token: 0x040000FF RID: 255
		Vector,
		// Token: 0x04000100 RID: 256
		Color
	}
}
