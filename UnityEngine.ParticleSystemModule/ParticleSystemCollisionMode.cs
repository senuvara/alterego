﻿using System;

namespace UnityEngine
{
	// Token: 0x02000030 RID: 48
	public enum ParticleSystemCollisionMode
	{
		// Token: 0x040000A6 RID: 166
		Collision3D,
		// Token: 0x040000A7 RID: 167
		Collision2D
	}
}
