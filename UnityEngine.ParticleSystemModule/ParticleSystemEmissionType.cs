﻿using System;

namespace UnityEngine
{
	// Token: 0x02000002 RID: 2
	[Obsolete("ParticleSystemEmissionType no longer does anything. Time and Distance based emission are now both always active.", false)]
	public enum ParticleSystemEmissionType
	{
		// Token: 0x04000002 RID: 2
		Time,
		// Token: 0x04000003 RID: 3
		Distance
	}
}
