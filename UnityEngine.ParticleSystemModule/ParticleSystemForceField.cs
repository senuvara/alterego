﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000047 RID: 71
	[NativeHeader("Runtime/ParticleSystem/ParticleSystem.h")]
	[NativeHeader("Runtime/ParticleSystem/ParticleSystemForceField.h")]
	[NativeHeader("Runtime/ParticleSystem/ParticleSystemForceFieldManager.h")]
	[NativeHeader("Runtime/ParticleSystem/ScriptBindings/ParticleSystemScriptBindings.h")]
	[RequireComponent(typeof(Transform))]
	[NativeHeader("ParticleSystemScriptingClasses.h")]
	public class ParticleSystemForceField : Component
	{
		// Token: 0x06000685 RID: 1669 RVA: 0x00002050 File Offset: 0x00000250
		public ParticleSystemForceField()
		{
		}

		// Token: 0x170001BF RID: 447
		// (get) Token: 0x06000686 RID: 1670
		// (set) Token: 0x06000687 RID: 1671
		[NativeName("ForceShape")]
		public extern ParticleSystemForceFieldShape shape { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x06000688 RID: 1672
		// (set) Token: 0x06000689 RID: 1673
		public extern float startRange { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x0600068A RID: 1674
		// (set) Token: 0x0600068B RID: 1675
		public extern float endRange { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x0600068C RID: 1676
		// (set) Token: 0x0600068D RID: 1677
		public extern float length { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x0600068E RID: 1678
		// (set) Token: 0x0600068F RID: 1679
		public extern float gravityFocus { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06000690 RID: 1680 RVA: 0x00008654 File Offset: 0x00006854
		// (set) Token: 0x06000691 RID: 1681 RVA: 0x0000866A File Offset: 0x0000686A
		public Vector2 rotationRandomness
		{
			get
			{
				Vector2 result;
				this.get_rotationRandomness_Injected(out result);
				return result;
			}
			set
			{
				this.set_rotationRandomness_Injected(ref value);
			}
		}

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06000692 RID: 1682
		// (set) Token: 0x06000693 RID: 1683
		public extern bool multiplyDragByParticleSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06000694 RID: 1684
		// (set) Token: 0x06000695 RID: 1685
		public extern bool multiplyDragByParticleVelocity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06000696 RID: 1686
		// (set) Token: 0x06000697 RID: 1687
		public extern Texture3D vectorField { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x06000698 RID: 1688 RVA: 0x00008674 File Offset: 0x00006874
		// (set) Token: 0x06000699 RID: 1689 RVA: 0x0000868A File Offset: 0x0000688A
		public ParticleSystem.MinMaxCurve directionX
		{
			get
			{
				ParticleSystem.MinMaxCurve result;
				this.get_directionX_Injected(out result);
				return result;
			}
			set
			{
				this.set_directionX_Injected(ref value);
			}
		}

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x0600069A RID: 1690 RVA: 0x00008694 File Offset: 0x00006894
		// (set) Token: 0x0600069B RID: 1691 RVA: 0x000086AA File Offset: 0x000068AA
		public ParticleSystem.MinMaxCurve directionY
		{
			get
			{
				ParticleSystem.MinMaxCurve result;
				this.get_directionY_Injected(out result);
				return result;
			}
			set
			{
				this.set_directionY_Injected(ref value);
			}
		}

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x0600069C RID: 1692 RVA: 0x000086B4 File Offset: 0x000068B4
		// (set) Token: 0x0600069D RID: 1693 RVA: 0x000086CA File Offset: 0x000068CA
		public ParticleSystem.MinMaxCurve directionZ
		{
			get
			{
				ParticleSystem.MinMaxCurve result;
				this.get_directionZ_Injected(out result);
				return result;
			}
			set
			{
				this.set_directionZ_Injected(ref value);
			}
		}

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x0600069E RID: 1694 RVA: 0x000086D4 File Offset: 0x000068D4
		// (set) Token: 0x0600069F RID: 1695 RVA: 0x000086EA File Offset: 0x000068EA
		public ParticleSystem.MinMaxCurve gravity
		{
			get
			{
				ParticleSystem.MinMaxCurve result;
				this.get_gravity_Injected(out result);
				return result;
			}
			set
			{
				this.set_gravity_Injected(ref value);
			}
		}

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x060006A0 RID: 1696 RVA: 0x000086F4 File Offset: 0x000068F4
		// (set) Token: 0x060006A1 RID: 1697 RVA: 0x0000870A File Offset: 0x0000690A
		public ParticleSystem.MinMaxCurve rotationSpeed
		{
			get
			{
				ParticleSystem.MinMaxCurve result;
				this.get_rotationSpeed_Injected(out result);
				return result;
			}
			set
			{
				this.set_rotationSpeed_Injected(ref value);
			}
		}

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x060006A2 RID: 1698 RVA: 0x00008714 File Offset: 0x00006914
		// (set) Token: 0x060006A3 RID: 1699 RVA: 0x0000872A File Offset: 0x0000692A
		public ParticleSystem.MinMaxCurve rotationAttraction
		{
			get
			{
				ParticleSystem.MinMaxCurve result;
				this.get_rotationAttraction_Injected(out result);
				return result;
			}
			set
			{
				this.set_rotationAttraction_Injected(ref value);
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x060006A4 RID: 1700 RVA: 0x00008734 File Offset: 0x00006934
		// (set) Token: 0x060006A5 RID: 1701 RVA: 0x0000874A File Offset: 0x0000694A
		public ParticleSystem.MinMaxCurve drag
		{
			get
			{
				ParticleSystem.MinMaxCurve result;
				this.get_drag_Injected(out result);
				return result;
			}
			set
			{
				this.set_drag_Injected(ref value);
			}
		}

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x060006A6 RID: 1702 RVA: 0x00008754 File Offset: 0x00006954
		// (set) Token: 0x060006A7 RID: 1703 RVA: 0x0000876A File Offset: 0x0000696A
		public ParticleSystem.MinMaxCurve vectorFieldSpeed
		{
			get
			{
				ParticleSystem.MinMaxCurve result;
				this.get_vectorFieldSpeed_Injected(out result);
				return result;
			}
			set
			{
				this.set_vectorFieldSpeed_Injected(ref value);
			}
		}

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x060006A8 RID: 1704 RVA: 0x00008774 File Offset: 0x00006974
		// (set) Token: 0x060006A9 RID: 1705 RVA: 0x0000878A File Offset: 0x0000698A
		public ParticleSystem.MinMaxCurve vectorFieldAttraction
		{
			get
			{
				ParticleSystem.MinMaxCurve result;
				this.get_vectorFieldAttraction_Injected(out result);
				return result;
			}
			set
			{
				this.set_vectorFieldAttraction_Injected(ref value);
			}
		}

		// Token: 0x060006AA RID: 1706
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rotationRandomness_Injected(out Vector2 ret);

		// Token: 0x060006AB RID: 1707
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rotationRandomness_Injected(ref Vector2 value);

		// Token: 0x060006AC RID: 1708
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_directionX_Injected(out ParticleSystem.MinMaxCurve ret);

		// Token: 0x060006AD RID: 1709
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_directionX_Injected(ref ParticleSystem.MinMaxCurve value);

		// Token: 0x060006AE RID: 1710
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_directionY_Injected(out ParticleSystem.MinMaxCurve ret);

		// Token: 0x060006AF RID: 1711
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_directionY_Injected(ref ParticleSystem.MinMaxCurve value);

		// Token: 0x060006B0 RID: 1712
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_directionZ_Injected(out ParticleSystem.MinMaxCurve ret);

		// Token: 0x060006B1 RID: 1713
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_directionZ_Injected(ref ParticleSystem.MinMaxCurve value);

		// Token: 0x060006B2 RID: 1714
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_gravity_Injected(out ParticleSystem.MinMaxCurve ret);

		// Token: 0x060006B3 RID: 1715
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_gravity_Injected(ref ParticleSystem.MinMaxCurve value);

		// Token: 0x060006B4 RID: 1716
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rotationSpeed_Injected(out ParticleSystem.MinMaxCurve ret);

		// Token: 0x060006B5 RID: 1717
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rotationSpeed_Injected(ref ParticleSystem.MinMaxCurve value);

		// Token: 0x060006B6 RID: 1718
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rotationAttraction_Injected(out ParticleSystem.MinMaxCurve ret);

		// Token: 0x060006B7 RID: 1719
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rotationAttraction_Injected(ref ParticleSystem.MinMaxCurve value);

		// Token: 0x060006B8 RID: 1720
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_drag_Injected(out ParticleSystem.MinMaxCurve ret);

		// Token: 0x060006B9 RID: 1721
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_drag_Injected(ref ParticleSystem.MinMaxCurve value);

		// Token: 0x060006BA RID: 1722
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_vectorFieldSpeed_Injected(out ParticleSystem.MinMaxCurve ret);

		// Token: 0x060006BB RID: 1723
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_vectorFieldSpeed_Injected(ref ParticleSystem.MinMaxCurve value);

		// Token: 0x060006BC RID: 1724
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_vectorFieldAttraction_Injected(out ParticleSystem.MinMaxCurve ret);

		// Token: 0x060006BD RID: 1725
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_vectorFieldAttraction_Injected(ref ParticleSystem.MinMaxCurve value);
	}
}
