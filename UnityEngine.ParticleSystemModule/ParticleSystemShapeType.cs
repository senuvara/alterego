﻿using System;

namespace UnityEngine
{
	// Token: 0x02000029 RID: 41
	public enum ParticleSystemShapeType
	{
		// Token: 0x0400007A RID: 122
		Sphere,
		// Token: 0x0400007B RID: 123
		[Obsolete("SphereShell is deprecated and does nothing. Please use ShapeModule.radiusThickness instead, to control edge emission.", false)]
		SphereShell,
		// Token: 0x0400007C RID: 124
		Hemisphere,
		// Token: 0x0400007D RID: 125
		[Obsolete("HemisphereShell is deprecated and does nothing. Please use ShapeModule.radiusThickness instead, to control edge emission.", false)]
		HemisphereShell,
		// Token: 0x0400007E RID: 126
		Cone,
		// Token: 0x0400007F RID: 127
		Box,
		// Token: 0x04000080 RID: 128
		Mesh,
		// Token: 0x04000081 RID: 129
		[Obsolete("ConeShell is deprecated and does nothing. Please use ShapeModule.radiusThickness instead, to control edge emission.", false)]
		ConeShell,
		// Token: 0x04000082 RID: 130
		ConeVolume,
		// Token: 0x04000083 RID: 131
		[Obsolete("ConeVolumeShell is deprecated and does nothing. Please use ShapeModule.radiusThickness instead, to control edge emission.", false)]
		ConeVolumeShell,
		// Token: 0x04000084 RID: 132
		Circle,
		// Token: 0x04000085 RID: 133
		[Obsolete("CircleEdge is deprecated and does nothing. Please use ShapeModule.radiusThickness instead, to control edge emission.", false)]
		CircleEdge,
		// Token: 0x04000086 RID: 134
		SingleSidedEdge,
		// Token: 0x04000087 RID: 135
		MeshRenderer,
		// Token: 0x04000088 RID: 136
		SkinnedMeshRenderer,
		// Token: 0x04000089 RID: 137
		BoxShell,
		// Token: 0x0400008A RID: 138
		BoxEdge,
		// Token: 0x0400008B RID: 139
		Donut,
		// Token: 0x0400008C RID: 140
		Rectangle,
		// Token: 0x0400008D RID: 141
		Sprite,
		// Token: 0x0400008E RID: 142
		SpriteRenderer
	}
}
