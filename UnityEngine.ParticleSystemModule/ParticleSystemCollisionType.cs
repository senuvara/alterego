﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002F RID: 47
	public enum ParticleSystemCollisionType
	{
		// Token: 0x040000A3 RID: 163
		Planes,
		// Token: 0x040000A4 RID: 164
		World
	}
}
