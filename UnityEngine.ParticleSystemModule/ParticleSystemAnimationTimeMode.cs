﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002D RID: 45
	public enum ParticleSystemAnimationTimeMode
	{
		// Token: 0x0400009C RID: 156
		Lifetime,
		// Token: 0x0400009D RID: 157
		Speed,
		// Token: 0x0400009E RID: 158
		FPS
	}
}
