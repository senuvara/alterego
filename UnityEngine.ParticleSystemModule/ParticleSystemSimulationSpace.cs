﻿using System;

namespace UnityEngine
{
	// Token: 0x02000032 RID: 50
	public enum ParticleSystemSimulationSpace
	{
		// Token: 0x040000AD RID: 173
		Local,
		// Token: 0x040000AE RID: 174
		World,
		// Token: 0x040000AF RID: 175
		Custom
	}
}
