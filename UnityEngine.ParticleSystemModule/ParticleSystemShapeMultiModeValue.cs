﻿using System;

namespace UnityEngine
{
	// Token: 0x02000042 RID: 66
	public enum ParticleSystemShapeMultiModeValue
	{
		// Token: 0x0400011C RID: 284
		Random,
		// Token: 0x0400011D RID: 285
		Loop,
		// Token: 0x0400011E RID: 286
		PingPong,
		// Token: 0x0400011F RID: 287
		BurstSpread
	}
}
