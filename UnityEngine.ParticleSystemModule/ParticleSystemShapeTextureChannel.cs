﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002B RID: 43
	public enum ParticleSystemShapeTextureChannel
	{
		// Token: 0x04000094 RID: 148
		Red,
		// Token: 0x04000095 RID: 149
		Green,
		// Token: 0x04000096 RID: 150
		Blue,
		// Token: 0x04000097 RID: 151
		Alpha
	}
}
