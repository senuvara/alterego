﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003D RID: 61
	public enum ParticleSystemNoiseQuality
	{
		// Token: 0x04000102 RID: 258
		Low,
		// Token: 0x04000103 RID: 259
		Medium,
		// Token: 0x04000104 RID: 260
		High
	}
}
