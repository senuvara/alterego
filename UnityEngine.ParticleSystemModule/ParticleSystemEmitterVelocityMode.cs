﻿using System;

namespace UnityEngine
{
	// Token: 0x02000037 RID: 55
	public enum ParticleSystemEmitterVelocityMode
	{
		// Token: 0x040000C2 RID: 194
		Transform,
		// Token: 0x040000C3 RID: 195
		Rigidbody
	}
}
