﻿using System;

namespace UnityEngine
{
	// Token: 0x02000040 RID: 64
	public enum ParticleSystemTrailMode
	{
		// Token: 0x04000114 RID: 276
		PerParticle,
		// Token: 0x04000115 RID: 277
		Ribbon
	}
}
