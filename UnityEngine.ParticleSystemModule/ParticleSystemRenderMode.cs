﻿using System;

namespace UnityEngine
{
	// Token: 0x02000023 RID: 35
	public enum ParticleSystemRenderMode
	{
		// Token: 0x04000059 RID: 89
		Billboard,
		// Token: 0x0400005A RID: 90
		Stretch,
		// Token: 0x0400005B RID: 91
		HorizontalBillboard,
		// Token: 0x0400005C RID: 92
		VerticalBillboard,
		// Token: 0x0400005D RID: 93
		Mesh,
		// Token: 0x0400005E RID: 94
		None
	}
}
