﻿using System;

namespace UnityEngine
{
	// Token: 0x02000025 RID: 37
	public enum ParticleSystemCollisionQuality
	{
		// Token: 0x04000065 RID: 101
		High,
		// Token: 0x04000066 RID: 102
		Medium,
		// Token: 0x04000067 RID: 103
		Low
	}
}
