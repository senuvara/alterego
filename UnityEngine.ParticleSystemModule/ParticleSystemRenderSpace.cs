﻿using System;

namespace UnityEngine
{
	// Token: 0x02000026 RID: 38
	public enum ParticleSystemRenderSpace
	{
		// Token: 0x04000069 RID: 105
		View,
		// Token: 0x0400006A RID: 106
		World,
		// Token: 0x0400006B RID: 107
		Local,
		// Token: 0x0400006C RID: 108
		Facing,
		// Token: 0x0400006D RID: 109
		Velocity
	}
}
