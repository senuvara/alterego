﻿using System;

namespace UnityEngine
{
	// Token: 0x02000031 RID: 49
	public enum ParticleSystemOverlapAction
	{
		// Token: 0x040000A9 RID: 169
		Ignore,
		// Token: 0x040000AA RID: 170
		Kill,
		// Token: 0x040000AB RID: 171
		Callback
	}
}
