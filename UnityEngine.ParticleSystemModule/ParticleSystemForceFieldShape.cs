﻿using System;

namespace UnityEngine
{
	// Token: 0x02000045 RID: 69
	public enum ParticleSystemForceFieldShape
	{
		// Token: 0x04000129 RID: 297
		Sphere,
		// Token: 0x0400012A RID: 298
		Hemisphere,
		// Token: 0x0400012B RID: 299
		Cylinder,
		// Token: 0x0400012C RID: 300
		Box
	}
}
