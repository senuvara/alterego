﻿using System;

namespace UnityEngine
{
	// Token: 0x02000021 RID: 33
	[Obsolete("ParticleSystemVertexStreams is deprecated. Please use ParticleSystemVertexStream instead.", false)]
	[Flags]
	public enum ParticleSystemVertexStreams
	{
		// Token: 0x04000048 RID: 72
		Position = 1,
		// Token: 0x04000049 RID: 73
		Normal = 2,
		// Token: 0x0400004A RID: 74
		Tangent = 4,
		// Token: 0x0400004B RID: 75
		Color = 8,
		// Token: 0x0400004C RID: 76
		UV = 16,
		// Token: 0x0400004D RID: 77
		UV2BlendAndFrame = 32,
		// Token: 0x0400004E RID: 78
		CenterAndVertexID = 64,
		// Token: 0x0400004F RID: 79
		Size = 128,
		// Token: 0x04000050 RID: 80
		Rotation = 256,
		// Token: 0x04000051 RID: 81
		Velocity = 512,
		// Token: 0x04000052 RID: 82
		Lifetime = 1024,
		// Token: 0x04000053 RID: 83
		Custom1 = 2048,
		// Token: 0x04000054 RID: 84
		Custom2 = 4096,
		// Token: 0x04000055 RID: 85
		Random = 8192,
		// Token: 0x04000056 RID: 86
		None = 0,
		// Token: 0x04000057 RID: 87
		All = 2147483647
	}
}
