﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000048 RID: 72
	[RequiredByNativeCode(Optional = true)]
	public struct ParticleCollisionEvent
	{
		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x060006BE RID: 1726 RVA: 0x00008794 File Offset: 0x00006994
		public Vector3 intersection
		{
			get
			{
				return this.m_Intersection;
			}
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x060006BF RID: 1727 RVA: 0x000087B0 File Offset: 0x000069B0
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x060006C0 RID: 1728 RVA: 0x000087CC File Offset: 0x000069CC
		public Vector3 velocity
		{
			get
			{
				return this.m_Velocity;
			}
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x060006C1 RID: 1729 RVA: 0x000087E8 File Offset: 0x000069E8
		public Component colliderComponent
		{
			get
			{
				return ParticleCollisionEvent.InstanceIDToColliderComponent(this.m_ColliderInstanceID);
			}
		}

		// Token: 0x060006C2 RID: 1730
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Component InstanceIDToColliderComponent(int instanceID);

		// Token: 0x04000132 RID: 306
		private Vector3 m_Intersection;

		// Token: 0x04000133 RID: 307
		private Vector3 m_Normal;

		// Token: 0x04000134 RID: 308
		private Vector3 m_Velocity;

		// Token: 0x04000135 RID: 309
		private int m_ColliderInstanceID;
	}
}
