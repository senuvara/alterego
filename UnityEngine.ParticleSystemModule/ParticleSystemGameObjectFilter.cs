﻿using System;

namespace UnityEngine
{
	// Token: 0x02000044 RID: 68
	public enum ParticleSystemGameObjectFilter
	{
		// Token: 0x04000125 RID: 293
		LayerMask,
		// Token: 0x04000126 RID: 294
		List,
		// Token: 0x04000127 RID: 295
		LayerMaskAndList
	}
}
