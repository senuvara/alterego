﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000003 RID: 3
	[NativeHeader("Runtime/ParticleSystem/ParticleSystem.h")]
	[NativeHeader("Runtime/ParticleSystem/ScriptBindings/ParticleSystemScriptBindings.h")]
	[RequireComponent(typeof(Transform))]
	[RequireComponent(typeof(Transform))]
	[UsedByNativeCode]
	[NativeHeader("ParticleSystemScriptingClasses.h")]
	[NativeHeader("Runtime/ParticleSystem/ParticleSystemGeometryJob.h")]
	public sealed class ParticleSystem : Component
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public ParticleSystem()
		{
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
		[Obsolete("Emit with specific parameters is deprecated. Pass a ParticleSystem.EmitParams parameter instead, which allows you to override some/all of the emission properties", false)]
		public void Emit(Vector3 position, Vector3 velocity, float size, float lifetime, Color32 color)
		{
			ParticleSystem.Particle particle = default(ParticleSystem.Particle);
			particle.position = position;
			particle.velocity = velocity;
			particle.lifetime = lifetime;
			particle.startLifetime = lifetime;
			particle.startSize = size;
			particle.rotation3D = Vector3.zero;
			particle.angularVelocity3D = Vector3.zero;
			particle.startColor = color;
			particle.randomSeed = 5U;
			this.Internal_EmitOld(ref particle);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020C9 File Offset: 0x000002C9
		[Obsolete("Emit with a single particle structure is deprecated. Pass a ParticleSystem.EmitParams parameter instead, which allows you to override some/all of the emission properties", false)]
		public void Emit(ParticleSystem.Particle particle)
		{
			this.Internal_EmitOld(ref particle);
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000004 RID: 4 RVA: 0x000020D4 File Offset: 0x000002D4
		// (set) Token: 0x06000005 RID: 5 RVA: 0x000020F8 File Offset: 0x000002F8
		[Obsolete("startDelay property is deprecated. Use main.startDelay or main.startDelayMultiplier instead.", false)]
		public float startDelay
		{
			get
			{
				return this.main.startDelayMultiplier;
			}
			set
			{
				this.main.startDelayMultiplier = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000006 RID: 6 RVA: 0x00002118 File Offset: 0x00000318
		// (set) Token: 0x06000007 RID: 7 RVA: 0x0000213C File Offset: 0x0000033C
		[Obsolete("loop property is deprecated. Use main.loop instead.", false)]
		public bool loop
		{
			get
			{
				return this.main.loop;
			}
			set
			{
				this.main.loop = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000008 RID: 8 RVA: 0x0000215C File Offset: 0x0000035C
		// (set) Token: 0x06000009 RID: 9 RVA: 0x00002180 File Offset: 0x00000380
		[Obsolete("playOnAwake property is deprecated. Use main.playOnAwake instead.", false)]
		public bool playOnAwake
		{
			get
			{
				return this.main.playOnAwake;
			}
			set
			{
				this.main.playOnAwake = value;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600000A RID: 10 RVA: 0x000021A0 File Offset: 0x000003A0
		[Obsolete("duration property is deprecated. Use main.duration instead.", false)]
		public float duration
		{
			get
			{
				return this.main.duration;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600000B RID: 11 RVA: 0x000021C4 File Offset: 0x000003C4
		// (set) Token: 0x0600000C RID: 12 RVA: 0x000021E8 File Offset: 0x000003E8
		[Obsolete("playbackSpeed property is deprecated. Use main.simulationSpeed instead.", false)]
		public float playbackSpeed
		{
			get
			{
				return this.main.simulationSpeed;
			}
			set
			{
				this.main.simulationSpeed = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600000D RID: 13 RVA: 0x00002208 File Offset: 0x00000408
		// (set) Token: 0x0600000E RID: 14 RVA: 0x0000222C File Offset: 0x0000042C
		[Obsolete("enableEmission property is deprecated. Use emission.enabled instead.", false)]
		public bool enableEmission
		{
			get
			{
				return this.emission.enabled;
			}
			set
			{
				this.emission.enabled = value;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600000F RID: 15 RVA: 0x0000224C File Offset: 0x0000044C
		// (set) Token: 0x06000010 RID: 16 RVA: 0x00002270 File Offset: 0x00000470
		[Obsolete("emissionRate property is deprecated. Use emission.rateOverTime, emission.rateOverDistance, emission.rateOverTimeMultiplier or emission.rateOverDistanceMultiplier instead.", false)]
		public float emissionRate
		{
			get
			{
				return this.emission.rateOverTimeMultiplier;
			}
			set
			{
				this.emission.rateOverTime = value;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000011 RID: 17 RVA: 0x00002294 File Offset: 0x00000494
		// (set) Token: 0x06000012 RID: 18 RVA: 0x000022B8 File Offset: 0x000004B8
		[Obsolete("startSpeed property is deprecated. Use main.startSpeed or main.startSpeedMultiplier instead.", false)]
		public float startSpeed
		{
			get
			{
				return this.main.startSpeedMultiplier;
			}
			set
			{
				this.main.startSpeedMultiplier = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000013 RID: 19 RVA: 0x000022D8 File Offset: 0x000004D8
		// (set) Token: 0x06000014 RID: 20 RVA: 0x000022FC File Offset: 0x000004FC
		[Obsolete("startSize property is deprecated. Use main.startSize or main.startSizeMultiplier instead.", false)]
		public float startSize
		{
			get
			{
				return this.main.startSizeMultiplier;
			}
			set
			{
				this.main.startSizeMultiplier = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000015 RID: 21 RVA: 0x0000231C File Offset: 0x0000051C
		// (set) Token: 0x06000016 RID: 22 RVA: 0x00002348 File Offset: 0x00000548
		[Obsolete("startColor property is deprecated. Use main.startColor instead.", false)]
		public Color startColor
		{
			get
			{
				return this.main.startColor.color;
			}
			set
			{
				this.main.startColor = value;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000017 RID: 23 RVA: 0x0000236C File Offset: 0x0000056C
		// (set) Token: 0x06000018 RID: 24 RVA: 0x00002390 File Offset: 0x00000590
		[Obsolete("startRotation property is deprecated. Use main.startRotation or main.startRotationMultiplier instead.", false)]
		public float startRotation
		{
			get
			{
				return this.main.startRotationMultiplier;
			}
			set
			{
				this.main.startRotationMultiplier = value;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000019 RID: 25 RVA: 0x000023B0 File Offset: 0x000005B0
		// (set) Token: 0x0600001A RID: 26 RVA: 0x000023F4 File Offset: 0x000005F4
		[Obsolete("startRotation3D property is deprecated. Use main.startRotationX, main.startRotationY and main.startRotationZ instead. (Or main.startRotationXMultiplier, main.startRotationYMultiplier and main.startRotationZMultiplier).", false)]
		public Vector3 startRotation3D
		{
			get
			{
				return new Vector3(this.main.startRotationXMultiplier, this.main.startRotationYMultiplier, this.main.startRotationZMultiplier);
			}
			set
			{
				ParticleSystem.MainModule main = this.main;
				main.startRotationXMultiplier = value.x;
				main.startRotationYMultiplier = value.y;
				main.startRotationZMultiplier = value.z;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600001B RID: 27 RVA: 0x00002434 File Offset: 0x00000634
		// (set) Token: 0x0600001C RID: 28 RVA: 0x00002458 File Offset: 0x00000658
		[Obsolete("startLifetime property is deprecated. Use main.startLifetime or main.startLifetimeMultiplier instead.", false)]
		public float startLifetime
		{
			get
			{
				return this.main.startLifetimeMultiplier;
			}
			set
			{
				this.main.startLifetimeMultiplier = value;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600001D RID: 29 RVA: 0x00002478 File Offset: 0x00000678
		// (set) Token: 0x0600001E RID: 30 RVA: 0x0000249C File Offset: 0x0000069C
		[Obsolete("gravityModifier property is deprecated. Use main.gravityModifier or main.gravityModifierMultiplier instead.", false)]
		public float gravityModifier
		{
			get
			{
				return this.main.gravityModifierMultiplier;
			}
			set
			{
				this.main.gravityModifierMultiplier = value;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600001F RID: 31 RVA: 0x000024BC File Offset: 0x000006BC
		// (set) Token: 0x06000020 RID: 32 RVA: 0x000024E0 File Offset: 0x000006E0
		[Obsolete("maxParticles property is deprecated. Use main.maxParticles instead.", false)]
		public int maxParticles
		{
			get
			{
				return this.main.maxParticles;
			}
			set
			{
				this.main.maxParticles = value;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000021 RID: 33 RVA: 0x00002500 File Offset: 0x00000700
		// (set) Token: 0x06000022 RID: 34 RVA: 0x00002524 File Offset: 0x00000724
		[Obsolete("simulationSpace property is deprecated. Use main.simulationSpace instead.", false)]
		public ParticleSystemSimulationSpace simulationSpace
		{
			get
			{
				return this.main.simulationSpace;
			}
			set
			{
				this.main.simulationSpace = value;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000023 RID: 35 RVA: 0x00002544 File Offset: 0x00000744
		// (set) Token: 0x06000024 RID: 36 RVA: 0x00002568 File Offset: 0x00000768
		[Obsolete("scalingMode property is deprecated. Use main.scalingMode instead.", false)]
		public ParticleSystemScalingMode scalingMode
		{
			get
			{
				return this.main.scalingMode;
			}
			set
			{
				this.main.scalingMode = value;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000025 RID: 37 RVA: 0x00002588 File Offset: 0x00000788
		[Obsolete("automaticCullingEnabled property is deprecated. Use proceduralSimulationSupported instead (UnityUpgradable) -> proceduralSimulationSupported", true)]
		public bool automaticCullingEnabled
		{
			get
			{
				return this.proceduralSimulationSupported;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000026 RID: 38
		public extern bool isPlaying { [NativeName("SyncJobs(false)->IsPlaying")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000027 RID: 39
		public extern bool isEmitting { [NativeName("SyncJobs(false)->IsEmitting")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000028 RID: 40
		public extern bool isStopped { [NativeName("SyncJobs(false)->IsStopped")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000029 RID: 41
		public extern bool isPaused { [NativeName("SyncJobs(false)->IsPaused")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600002A RID: 42
		public extern int particleCount { [NativeName("SyncJobs(false)->GetParticleCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600002B RID: 43
		// (set) Token: 0x0600002C RID: 44
		public extern float time { [NativeName("SyncJobs(false)->GetSecPosition")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("SyncJobs(false)->SetSecPosition")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600002D RID: 45
		// (set) Token: 0x0600002E RID: 46
		public extern uint randomSeed { [NativeName("GetRandomSeed")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("SyncJobs(false)->SetRandomSeed")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600002F RID: 47
		// (set) Token: 0x06000030 RID: 48
		public extern bool useAutoRandomSeed { [NativeName("GetAutoRandomSeed")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("SyncJobs(false)->SetAutoRandomSeed")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000031 RID: 49
		public extern bool proceduralSimulationSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000032 RID: 50
		[FreeFunction(Name = "ParticleSystemScriptBindings::GetParticleCurrentSize", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern float GetParticleCurrentSize(ref ParticleSystem.Particle particle);

		// Token: 0x06000033 RID: 51 RVA: 0x000025A4 File Offset: 0x000007A4
		[FreeFunction(Name = "ParticleSystemScriptBindings::GetParticleCurrentSize3D", HasExplicitThis = true)]
		internal Vector3 GetParticleCurrentSize3D(ref ParticleSystem.Particle particle)
		{
			Vector3 result;
			this.GetParticleCurrentSize3D_Injected(ref particle, out result);
			return result;
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000025BC File Offset: 0x000007BC
		[FreeFunction(Name = "ParticleSystemScriptBindings::GetParticleCurrentColor", HasExplicitThis = true)]
		internal Color32 GetParticleCurrentColor(ref ParticleSystem.Particle particle)
		{
			Color32 result;
			this.GetParticleCurrentColor_Injected(ref particle, out result);
			return result;
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000035 RID: 53 RVA: 0x000025D4 File Offset: 0x000007D4
		public ParticleSystem.MainModule main
		{
			get
			{
				return new ParticleSystem.MainModule(this);
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000036 RID: 54 RVA: 0x000025F0 File Offset: 0x000007F0
		public ParticleSystem.EmissionModule emission
		{
			get
			{
				return new ParticleSystem.EmissionModule(this);
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000037 RID: 55 RVA: 0x0000260C File Offset: 0x0000080C
		public ParticleSystem.ShapeModule shape
		{
			get
			{
				return new ParticleSystem.ShapeModule(this);
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000038 RID: 56 RVA: 0x00002628 File Offset: 0x00000828
		public ParticleSystem.VelocityOverLifetimeModule velocityOverLifetime
		{
			get
			{
				return new ParticleSystem.VelocityOverLifetimeModule(this);
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000039 RID: 57 RVA: 0x00002644 File Offset: 0x00000844
		public ParticleSystem.LimitVelocityOverLifetimeModule limitVelocityOverLifetime
		{
			get
			{
				return new ParticleSystem.LimitVelocityOverLifetimeModule(this);
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600003A RID: 58 RVA: 0x00002660 File Offset: 0x00000860
		public ParticleSystem.InheritVelocityModule inheritVelocity
		{
			get
			{
				return new ParticleSystem.InheritVelocityModule(this);
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600003B RID: 59 RVA: 0x0000267C File Offset: 0x0000087C
		public ParticleSystem.ForceOverLifetimeModule forceOverLifetime
		{
			get
			{
				return new ParticleSystem.ForceOverLifetimeModule(this);
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600003C RID: 60 RVA: 0x00002698 File Offset: 0x00000898
		public ParticleSystem.ColorOverLifetimeModule colorOverLifetime
		{
			get
			{
				return new ParticleSystem.ColorOverLifetimeModule(this);
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600003D RID: 61 RVA: 0x000026B4 File Offset: 0x000008B4
		public ParticleSystem.ColorBySpeedModule colorBySpeed
		{
			get
			{
				return new ParticleSystem.ColorBySpeedModule(this);
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600003E RID: 62 RVA: 0x000026D0 File Offset: 0x000008D0
		public ParticleSystem.SizeOverLifetimeModule sizeOverLifetime
		{
			get
			{
				return new ParticleSystem.SizeOverLifetimeModule(this);
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x0600003F RID: 63 RVA: 0x000026EC File Offset: 0x000008EC
		public ParticleSystem.SizeBySpeedModule sizeBySpeed
		{
			get
			{
				return new ParticleSystem.SizeBySpeedModule(this);
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000040 RID: 64 RVA: 0x00002708 File Offset: 0x00000908
		public ParticleSystem.RotationOverLifetimeModule rotationOverLifetime
		{
			get
			{
				return new ParticleSystem.RotationOverLifetimeModule(this);
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000041 RID: 65 RVA: 0x00002724 File Offset: 0x00000924
		public ParticleSystem.RotationBySpeedModule rotationBySpeed
		{
			get
			{
				return new ParticleSystem.RotationBySpeedModule(this);
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000042 RID: 66 RVA: 0x00002740 File Offset: 0x00000940
		public ParticleSystem.ExternalForcesModule externalForces
		{
			get
			{
				return new ParticleSystem.ExternalForcesModule(this);
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000043 RID: 67 RVA: 0x0000275C File Offset: 0x0000095C
		public ParticleSystem.NoiseModule noise
		{
			get
			{
				return new ParticleSystem.NoiseModule(this);
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000044 RID: 68 RVA: 0x00002778 File Offset: 0x00000978
		public ParticleSystem.CollisionModule collision
		{
			get
			{
				return new ParticleSystem.CollisionModule(this);
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000045 RID: 69 RVA: 0x00002794 File Offset: 0x00000994
		public ParticleSystem.TriggerModule trigger
		{
			get
			{
				return new ParticleSystem.TriggerModule(this);
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000046 RID: 70 RVA: 0x000027B0 File Offset: 0x000009B0
		public ParticleSystem.SubEmittersModule subEmitters
		{
			get
			{
				return new ParticleSystem.SubEmittersModule(this);
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000047 RID: 71 RVA: 0x000027CC File Offset: 0x000009CC
		public ParticleSystem.TextureSheetAnimationModule textureSheetAnimation
		{
			get
			{
				return new ParticleSystem.TextureSheetAnimationModule(this);
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000048 RID: 72 RVA: 0x000027E8 File Offset: 0x000009E8
		public ParticleSystem.LightsModule lights
		{
			get
			{
				return new ParticleSystem.LightsModule(this);
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000049 RID: 73 RVA: 0x00002804 File Offset: 0x00000A04
		public ParticleSystem.TrailModule trails
		{
			get
			{
				return new ParticleSystem.TrailModule(this);
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600004A RID: 74 RVA: 0x00002820 File Offset: 0x00000A20
		public ParticleSystem.CustomDataModule customData
		{
			get
			{
				return new ParticleSystem.CustomDataModule(this);
			}
		}

		// Token: 0x0600004B RID: 75
		[FreeFunction(Name = "ParticleSystemScriptBindings::SetParticles", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetParticles([Out] ParticleSystem.Particle[] particles, int size, int offset);

		// Token: 0x0600004C RID: 76 RVA: 0x0000283B File Offset: 0x00000A3B
		public void SetParticles([Out] ParticleSystem.Particle[] particles, int size)
		{
			this.SetParticles(particles, size, 0);
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002847 File Offset: 0x00000A47
		public void SetParticles([Out] ParticleSystem.Particle[] particles)
		{
			this.SetParticles(particles, -1);
		}

		// Token: 0x0600004E RID: 78
		[FreeFunction(Name = "ParticleSystemScriptBindings::GetParticles", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetParticles([NotNull] [Out] ParticleSystem.Particle[] particles, int size, int offset);

		// Token: 0x0600004F RID: 79 RVA: 0x00002854 File Offset: 0x00000A54
		public int GetParticles([Out] ParticleSystem.Particle[] particles, int size)
		{
			return this.GetParticles(particles, size, 0);
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00002874 File Offset: 0x00000A74
		public int GetParticles([Out] ParticleSystem.Particle[] particles)
		{
			return this.GetParticles(particles, -1);
		}

		// Token: 0x06000051 RID: 81
		[FreeFunction(Name = "ParticleSystemScriptBindings::Simulate", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Simulate(float t, bool withChildren, bool restart, bool fixedTimeStep);

		// Token: 0x06000052 RID: 82 RVA: 0x00002891 File Offset: 0x00000A91
		public void Simulate(float t, bool withChildren, bool restart)
		{
			this.Simulate(t, withChildren, restart, true);
		}

		// Token: 0x06000053 RID: 83 RVA: 0x0000289E File Offset: 0x00000A9E
		public void Simulate(float t, bool withChildren)
		{
			this.Simulate(t, withChildren, true);
		}

		// Token: 0x06000054 RID: 84 RVA: 0x000028AA File Offset: 0x00000AAA
		public void Simulate(float t)
		{
			this.Simulate(t, true);
		}

		// Token: 0x06000055 RID: 85
		[FreeFunction(Name = "ParticleSystemScriptBindings::Play", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Play(bool withChildren);

		// Token: 0x06000056 RID: 86 RVA: 0x000028B5 File Offset: 0x00000AB5
		public void Play()
		{
			this.Play(true);
		}

		// Token: 0x06000057 RID: 87
		[FreeFunction(Name = "ParticleSystemScriptBindings::Pause", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Pause(bool withChildren);

		// Token: 0x06000058 RID: 88 RVA: 0x000028BF File Offset: 0x00000ABF
		public void Pause()
		{
			this.Pause(true);
		}

		// Token: 0x06000059 RID: 89
		[FreeFunction(Name = "ParticleSystemScriptBindings::Stop", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Stop(bool withChildren, ParticleSystemStopBehavior stopBehavior);

		// Token: 0x0600005A RID: 90 RVA: 0x000028C9 File Offset: 0x00000AC9
		public void Stop(bool withChildren)
		{
			this.Stop(withChildren, ParticleSystemStopBehavior.StopEmitting);
		}

		// Token: 0x0600005B RID: 91 RVA: 0x000028D4 File Offset: 0x00000AD4
		public void Stop()
		{
			this.Stop(true);
		}

		// Token: 0x0600005C RID: 92
		[FreeFunction(Name = "ParticleSystemScriptBindings::Clear", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Clear(bool withChildren);

		// Token: 0x0600005D RID: 93 RVA: 0x000028DE File Offset: 0x00000ADE
		public void Clear()
		{
			this.Clear(true);
		}

		// Token: 0x0600005E RID: 94
		[FreeFunction(Name = "ParticleSystemScriptBindings::IsAlive", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsAlive(bool withChildren);

		// Token: 0x0600005F RID: 95 RVA: 0x000028E8 File Offset: 0x00000AE8
		public bool IsAlive()
		{
			return this.IsAlive(true);
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00002904 File Offset: 0x00000B04
		[RequiredByNativeCode]
		public void Emit(int count)
		{
			this.Emit_Internal(count);
		}

		// Token: 0x06000061 RID: 97
		[NativeName("SyncJobs()->Emit")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Emit_Internal(int count);

		// Token: 0x06000062 RID: 98 RVA: 0x0000290E File Offset: 0x00000B0E
		[NativeName("SyncJobs()->EmitParticlesExternal")]
		public void Emit(ParticleSystem.EmitParams emitParams, int count)
		{
			this.Emit_Injected(ref emitParams, count);
		}

		// Token: 0x06000063 RID: 99
		[FreeFunction(Name = "ParticleSystemGeometryJob::ResetPreMappedBufferMemory")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ResetPreMappedBufferMemory();

		// Token: 0x06000064 RID: 100 RVA: 0x00002919 File Offset: 0x00000B19
		public void SetCustomParticleData(List<Vector4> customData, ParticleSystemCustomData streamIndex)
		{
			this.SetCustomParticleDataInternal(customData, (int)streamIndex);
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00002924 File Offset: 0x00000B24
		public int GetCustomParticleData(List<Vector4> customData, ParticleSystemCustomData streamIndex)
		{
			return this.GetCustomParticleDataInternal(customData, (int)streamIndex);
		}

		// Token: 0x06000066 RID: 102
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetCustomParticleDataInternal(object customData, int streamIndex);

		// Token: 0x06000067 RID: 103
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetCustomParticleDataInternal(object customData, int streamIndex);

		// Token: 0x06000068 RID: 104
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_EmitOld(ref ParticleSystem.Particle particle);

		// Token: 0x06000069 RID: 105 RVA: 0x00002941 File Offset: 0x00000B41
		public void TriggerSubEmitter(int subEmitterIndex)
		{
			this.Internal_TriggerSubEmitter(subEmitterIndex, null);
		}

		// Token: 0x0600006A RID: 106
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void TriggerSubEmitter(int subEmitterIndex, ref ParticleSystem.Particle particle);

		// Token: 0x0600006B RID: 107 RVA: 0x0000294C File Offset: 0x00000B4C
		public void TriggerSubEmitter(int subEmitterIndex, List<ParticleSystem.Particle> particles)
		{
			this.Internal_TriggerSubEmitter(subEmitterIndex, particles);
		}

		// Token: 0x0600006C RID: 108
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void Internal_TriggerSubEmitter(int subEmitterIndex, object particles);

		// Token: 0x0600006D RID: 109
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetParticleCurrentSize3D_Injected(ref ParticleSystem.Particle particle, out Vector3 ret);

		// Token: 0x0600006E RID: 110
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetParticleCurrentColor_Injected(ref ParticleSystem.Particle particle, out Color32 ret);

		// Token: 0x0600006F RID: 111
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Emit_Injected(ref ParticleSystem.EmitParams emitParams, int count);

		// Token: 0x02000004 RID: 4
		public struct MainModule
		{
			// Token: 0x06000070 RID: 112 RVA: 0x00002957 File Offset: 0x00000B57
			internal MainModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000032 RID: 50
			// (get) Token: 0x06000071 RID: 113 RVA: 0x00002964 File Offset: 0x00000B64
			// (set) Token: 0x06000072 RID: 114 RVA: 0x0000297F File Offset: 0x00000B7F
			[Obsolete("Please use flipRotation instead. (UnityUpgradable) -> UnityEngine.ParticleSystem/MainModule.flipRotation", false)]
			public float randomizeRotationDirection
			{
				get
				{
					return this.flipRotation;
				}
				set
				{
					this.flipRotation = value;
				}
			}

			// Token: 0x17000033 RID: 51
			// (get) Token: 0x06000073 RID: 115 RVA: 0x0000298C File Offset: 0x00000B8C
			// (set) Token: 0x06000074 RID: 116 RVA: 0x000029AC File Offset: 0x00000BAC
			public float duration
			{
				get
				{
					return ParticleSystem.MainModule.GetDuration(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetDuration(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000034 RID: 52
			// (get) Token: 0x06000075 RID: 117 RVA: 0x000029BC File Offset: 0x00000BBC
			// (set) Token: 0x06000076 RID: 118 RVA: 0x000029DC File Offset: 0x00000BDC
			public bool loop
			{
				get
				{
					return ParticleSystem.MainModule.GetLoop(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetLoop(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000035 RID: 53
			// (get) Token: 0x06000077 RID: 119 RVA: 0x000029EC File Offset: 0x00000BEC
			// (set) Token: 0x06000078 RID: 120 RVA: 0x00002A0C File Offset: 0x00000C0C
			public bool prewarm
			{
				get
				{
					return ParticleSystem.MainModule.GetPrewarm(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetPrewarm(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000036 RID: 54
			// (get) Token: 0x0600007A RID: 122 RVA: 0x00002A2C File Offset: 0x00000C2C
			// (set) Token: 0x06000079 RID: 121 RVA: 0x00002A1B File Offset: 0x00000C1B
			public ParticleSystem.MinMaxCurve startDelay
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartDelay(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartDelay(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000037 RID: 55
			// (get) Token: 0x0600007B RID: 123 RVA: 0x00002A58 File Offset: 0x00000C58
			// (set) Token: 0x0600007C RID: 124 RVA: 0x00002A78 File Offset: 0x00000C78
			public float startDelayMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartDelayMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartDelayMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000038 RID: 56
			// (get) Token: 0x0600007E RID: 126 RVA: 0x00002A98 File Offset: 0x00000C98
			// (set) Token: 0x0600007D RID: 125 RVA: 0x00002A87 File Offset: 0x00000C87
			public ParticleSystem.MinMaxCurve startLifetime
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartLifetime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartLifetime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000039 RID: 57
			// (get) Token: 0x0600007F RID: 127 RVA: 0x00002AC4 File Offset: 0x00000CC4
			// (set) Token: 0x06000080 RID: 128 RVA: 0x00002AE4 File Offset: 0x00000CE4
			public float startLifetimeMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartLifetimeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartLifetimeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700003A RID: 58
			// (get) Token: 0x06000082 RID: 130 RVA: 0x00002B04 File Offset: 0x00000D04
			// (set) Token: 0x06000081 RID: 129 RVA: 0x00002AF3 File Offset: 0x00000CF3
			public ParticleSystem.MinMaxCurve startSpeed
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartSpeed(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartSpeed(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700003B RID: 59
			// (get) Token: 0x06000083 RID: 131 RVA: 0x00002B30 File Offset: 0x00000D30
			// (set) Token: 0x06000084 RID: 132 RVA: 0x00002B50 File Offset: 0x00000D50
			public float startSpeedMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSpeedMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSpeedMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700003C RID: 60
			// (get) Token: 0x06000085 RID: 133 RVA: 0x00002B60 File Offset: 0x00000D60
			// (set) Token: 0x06000086 RID: 134 RVA: 0x00002B80 File Offset: 0x00000D80
			public bool startSize3D
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSize3D(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSize3D(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700003D RID: 61
			// (get) Token: 0x06000088 RID: 136 RVA: 0x00002BA0 File Offset: 0x00000DA0
			// (set) Token: 0x06000087 RID: 135 RVA: 0x00002B8F File Offset: 0x00000D8F
			public ParticleSystem.MinMaxCurve startSize
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartSizeX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700003E RID: 62
			// (get) Token: 0x06000089 RID: 137 RVA: 0x00002BCC File Offset: 0x00000DCC
			// (set) Token: 0x0600008A RID: 138 RVA: 0x00002BEC File Offset: 0x00000DEC
			public float startSizeMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSizeXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700003F RID: 63
			// (get) Token: 0x0600008C RID: 140 RVA: 0x00002BFC File Offset: 0x00000DFC
			// (set) Token: 0x0600008B RID: 139 RVA: 0x00002B8F File Offset: 0x00000D8F
			public ParticleSystem.MinMaxCurve startSizeX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartSizeX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000040 RID: 64
			// (get) Token: 0x0600008D RID: 141 RVA: 0x00002C28 File Offset: 0x00000E28
			// (set) Token: 0x0600008E RID: 142 RVA: 0x00002BEC File Offset: 0x00000DEC
			public float startSizeXMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSizeXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000041 RID: 65
			// (get) Token: 0x06000090 RID: 144 RVA: 0x00002C58 File Offset: 0x00000E58
			// (set) Token: 0x0600008F RID: 143 RVA: 0x00002C48 File Offset: 0x00000E48
			public ParticleSystem.MinMaxCurve startSizeY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartSizeY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000042 RID: 66
			// (get) Token: 0x06000091 RID: 145 RVA: 0x00002C84 File Offset: 0x00000E84
			// (set) Token: 0x06000092 RID: 146 RVA: 0x00002CA4 File Offset: 0x00000EA4
			public float startSizeYMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSizeYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000043 RID: 67
			// (get) Token: 0x06000094 RID: 148 RVA: 0x00002CC4 File Offset: 0x00000EC4
			// (set) Token: 0x06000093 RID: 147 RVA: 0x00002CB3 File Offset: 0x00000EB3
			public ParticleSystem.MinMaxCurve startSizeZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartSizeZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000044 RID: 68
			// (get) Token: 0x06000095 RID: 149 RVA: 0x00002CF0 File Offset: 0x00000EF0
			// (set) Token: 0x06000096 RID: 150 RVA: 0x00002D10 File Offset: 0x00000F10
			public float startSizeZMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSizeZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000045 RID: 69
			// (get) Token: 0x06000097 RID: 151 RVA: 0x00002D20 File Offset: 0x00000F20
			// (set) Token: 0x06000098 RID: 152 RVA: 0x00002D40 File Offset: 0x00000F40
			public bool startRotation3D
			{
				get
				{
					return ParticleSystem.MainModule.GetStartRotation3D(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotation3D(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000046 RID: 70
			// (get) Token: 0x0600009A RID: 154 RVA: 0x00002D60 File Offset: 0x00000F60
			// (set) Token: 0x06000099 RID: 153 RVA: 0x00002D4F File Offset: 0x00000F4F
			public ParticleSystem.MinMaxCurve startRotation
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartRotationZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000047 RID: 71
			// (get) Token: 0x0600009B RID: 155 RVA: 0x00002D8C File Offset: 0x00000F8C
			// (set) Token: 0x0600009C RID: 156 RVA: 0x00002DAC File Offset: 0x00000FAC
			public float startRotationMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartRotationZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000048 RID: 72
			// (get) Token: 0x0600009E RID: 158 RVA: 0x00002DCC File Offset: 0x00000FCC
			// (set) Token: 0x0600009D RID: 157 RVA: 0x00002DBB File Offset: 0x00000FBB
			public ParticleSystem.MinMaxCurve startRotationX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartRotationX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000049 RID: 73
			// (get) Token: 0x0600009F RID: 159 RVA: 0x00002DF8 File Offset: 0x00000FF8
			// (set) Token: 0x060000A0 RID: 160 RVA: 0x00002E18 File Offset: 0x00001018
			public float startRotationXMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartRotationXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700004A RID: 74
			// (get) Token: 0x060000A2 RID: 162 RVA: 0x00002E38 File Offset: 0x00001038
			// (set) Token: 0x060000A1 RID: 161 RVA: 0x00002E27 File Offset: 0x00001027
			public ParticleSystem.MinMaxCurve startRotationY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartRotationY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700004B RID: 75
			// (get) Token: 0x060000A3 RID: 163 RVA: 0x00002E64 File Offset: 0x00001064
			// (set) Token: 0x060000A4 RID: 164 RVA: 0x00002E84 File Offset: 0x00001084
			public float startRotationYMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartRotationYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700004C RID: 76
			// (get) Token: 0x060000A6 RID: 166 RVA: 0x00002E94 File Offset: 0x00001094
			// (set) Token: 0x060000A5 RID: 165 RVA: 0x00002D4F File Offset: 0x00000F4F
			public ParticleSystem.MinMaxCurve startRotationZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartRotationZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700004D RID: 77
			// (get) Token: 0x060000A7 RID: 167 RVA: 0x00002EC0 File Offset: 0x000010C0
			// (set) Token: 0x060000A8 RID: 168 RVA: 0x00002DAC File Offset: 0x00000FAC
			public float startRotationZMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartRotationZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700004E RID: 78
			// (get) Token: 0x060000A9 RID: 169 RVA: 0x00002EE0 File Offset: 0x000010E0
			// (set) Token: 0x060000AA RID: 170 RVA: 0x00002F00 File Offset: 0x00001100
			public float flipRotation
			{
				get
				{
					return ParticleSystem.MainModule.GetFlipRotation(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetFlipRotation(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700004F RID: 79
			// (get) Token: 0x060000AC RID: 172 RVA: 0x00002F20 File Offset: 0x00001120
			// (set) Token: 0x060000AB RID: 171 RVA: 0x00002F0F File Offset: 0x0000110F
			public ParticleSystem.MinMaxGradient startColor
			{
				get
				{
					ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
					ParticleSystem.MainModule.GetStartColor(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartColor(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000050 RID: 80
			// (get) Token: 0x060000AE RID: 174 RVA: 0x00002F5C File Offset: 0x0000115C
			// (set) Token: 0x060000AD RID: 173 RVA: 0x00002F4B File Offset: 0x0000114B
			public ParticleSystem.MinMaxCurve gravityModifier
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetGravityModifier(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetGravityModifier(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000051 RID: 81
			// (get) Token: 0x060000AF RID: 175 RVA: 0x00002F88 File Offset: 0x00001188
			// (set) Token: 0x060000B0 RID: 176 RVA: 0x00002FA8 File Offset: 0x000011A8
			public float gravityModifierMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetGravityModifierMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetGravityModifierMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000052 RID: 82
			// (get) Token: 0x060000B1 RID: 177 RVA: 0x00002FB8 File Offset: 0x000011B8
			// (set) Token: 0x060000B2 RID: 178 RVA: 0x00002FD8 File Offset: 0x000011D8
			public ParticleSystemSimulationSpace simulationSpace
			{
				get
				{
					return ParticleSystem.MainModule.GetSimulationSpace(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetSimulationSpace(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000053 RID: 83
			// (get) Token: 0x060000B3 RID: 179 RVA: 0x00002FE8 File Offset: 0x000011E8
			// (set) Token: 0x060000B4 RID: 180 RVA: 0x00003008 File Offset: 0x00001208
			public Transform customSimulationSpace
			{
				get
				{
					return ParticleSystem.MainModule.GetCustomSimulationSpace(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetCustomSimulationSpace(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000054 RID: 84
			// (get) Token: 0x060000B5 RID: 181 RVA: 0x00003018 File Offset: 0x00001218
			// (set) Token: 0x060000B6 RID: 182 RVA: 0x00003038 File Offset: 0x00001238
			public float simulationSpeed
			{
				get
				{
					return ParticleSystem.MainModule.GetSimulationSpeed(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetSimulationSpeed(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000055 RID: 85
			// (get) Token: 0x060000B7 RID: 183 RVA: 0x00003048 File Offset: 0x00001248
			// (set) Token: 0x060000B8 RID: 184 RVA: 0x00003068 File Offset: 0x00001268
			public bool useUnscaledTime
			{
				get
				{
					return ParticleSystem.MainModule.GetUseUnscaledTime(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetUseUnscaledTime(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000056 RID: 86
			// (get) Token: 0x060000B9 RID: 185 RVA: 0x00003078 File Offset: 0x00001278
			// (set) Token: 0x060000BA RID: 186 RVA: 0x00003098 File Offset: 0x00001298
			public ParticleSystemScalingMode scalingMode
			{
				get
				{
					return ParticleSystem.MainModule.GetScalingMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetScalingMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000057 RID: 87
			// (get) Token: 0x060000BB RID: 187 RVA: 0x000030A8 File Offset: 0x000012A8
			// (set) Token: 0x060000BC RID: 188 RVA: 0x000030C8 File Offset: 0x000012C8
			public bool playOnAwake
			{
				get
				{
					return ParticleSystem.MainModule.GetPlayOnAwake(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetPlayOnAwake(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000058 RID: 88
			// (get) Token: 0x060000BD RID: 189 RVA: 0x000030D8 File Offset: 0x000012D8
			// (set) Token: 0x060000BE RID: 190 RVA: 0x000030F8 File Offset: 0x000012F8
			public int maxParticles
			{
				get
				{
					return ParticleSystem.MainModule.GetMaxParticles(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetMaxParticles(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000059 RID: 89
			// (get) Token: 0x060000BF RID: 191 RVA: 0x00003108 File Offset: 0x00001308
			// (set) Token: 0x060000C0 RID: 192 RVA: 0x00003134 File Offset: 0x00001334
			public ParticleSystemEmitterVelocityMode emitterVelocityMode
			{
				get
				{
					return (!ParticleSystem.MainModule.GetUseRigidbodyForVelocity(this.m_ParticleSystem)) ? ParticleSystemEmitterVelocityMode.Transform : ParticleSystemEmitterVelocityMode.Rigidbody;
				}
				set
				{
					ParticleSystem.MainModule.SetUseRigidbodyForVelocity(this.m_ParticleSystem, value == ParticleSystemEmitterVelocityMode.Rigidbody);
				}
			}

			// Token: 0x1700005A RID: 90
			// (get) Token: 0x060000C1 RID: 193 RVA: 0x00003148 File Offset: 0x00001348
			// (set) Token: 0x060000C2 RID: 194 RVA: 0x00003168 File Offset: 0x00001368
			public ParticleSystemStopAction stopAction
			{
				get
				{
					return ParticleSystem.MainModule.GetStopAction(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStopAction(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700005B RID: 91
			// (get) Token: 0x060000C3 RID: 195 RVA: 0x00003178 File Offset: 0x00001378
			// (set) Token: 0x060000C4 RID: 196 RVA: 0x00003198 File Offset: 0x00001398
			public ParticleSystemCullingMode cullingMode
			{
				get
				{
					return ParticleSystem.MainModule.GetCullingMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetCullingMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700005C RID: 92
			// (get) Token: 0x060000C5 RID: 197 RVA: 0x000031A8 File Offset: 0x000013A8
			// (set) Token: 0x060000C6 RID: 198 RVA: 0x000031C8 File Offset: 0x000013C8
			public ParticleSystemRingBufferMode ringBufferMode
			{
				get
				{
					return ParticleSystem.MainModule.GetRingBufferMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetRingBufferMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700005D RID: 93
			// (get) Token: 0x060000C7 RID: 199 RVA: 0x000031D8 File Offset: 0x000013D8
			// (set) Token: 0x060000C8 RID: 200 RVA: 0x000031F8 File Offset: 0x000013F8
			public Vector2 ringBufferLoopRange
			{
				get
				{
					return ParticleSystem.MainModule.GetRingBufferLoopRange(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetRingBufferLoopRange(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060000C9 RID: 201
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060000CA RID: 202
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060000CB RID: 203
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDuration(ParticleSystem system, float value);

			// Token: 0x060000CC RID: 204
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetDuration(ParticleSystem system);

			// Token: 0x060000CD RID: 205
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLoop(ParticleSystem system, bool value);

			// Token: 0x060000CE RID: 206
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetLoop(ParticleSystem system);

			// Token: 0x060000CF RID: 207
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetPrewarm(ParticleSystem system, bool value);

			// Token: 0x060000D0 RID: 208
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetPrewarm(ParticleSystem system);

			// Token: 0x060000D1 RID: 209
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartDelay(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000D2 RID: 210
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartDelay(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000D3 RID: 211
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartDelayMultiplier(ParticleSystem system, float value);

			// Token: 0x060000D4 RID: 212
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartDelayMultiplier(ParticleSystem system);

			// Token: 0x060000D5 RID: 213
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000D6 RID: 214
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000D7 RID: 215
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartLifetimeMultiplier(ParticleSystem system, float value);

			// Token: 0x060000D8 RID: 216
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartLifetimeMultiplier(ParticleSystem system);

			// Token: 0x060000D9 RID: 217
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000DA RID: 218
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000DB RID: 219
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSpeedMultiplier(ParticleSystem system, float value);

			// Token: 0x060000DC RID: 220
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartSpeedMultiplier(ParticleSystem system);

			// Token: 0x060000DD RID: 221
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSize3D(ParticleSystem system, bool value);

			// Token: 0x060000DE RID: 222
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetStartSize3D(ParticleSystem system);

			// Token: 0x060000DF RID: 223
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000E0 RID: 224
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartSizeX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000E1 RID: 225
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeXMultiplier(ParticleSystem system, float value);

			// Token: 0x060000E2 RID: 226
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartSizeXMultiplier(ParticleSystem system);

			// Token: 0x060000E3 RID: 227
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000E4 RID: 228
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartSizeY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000E5 RID: 229
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeYMultiplier(ParticleSystem system, float value);

			// Token: 0x060000E6 RID: 230
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartSizeYMultiplier(ParticleSystem system);

			// Token: 0x060000E7 RID: 231
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000E8 RID: 232
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartSizeZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000E9 RID: 233
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeZMultiplier(ParticleSystem system, float value);

			// Token: 0x060000EA RID: 234
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartSizeZMultiplier(ParticleSystem system);

			// Token: 0x060000EB RID: 235
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotation3D(ParticleSystem system, bool value);

			// Token: 0x060000EC RID: 236
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetStartRotation3D(ParticleSystem system);

			// Token: 0x060000ED RID: 237
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000EE RID: 238
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartRotationX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000EF RID: 239
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationXMultiplier(ParticleSystem system, float value);

			// Token: 0x060000F0 RID: 240
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartRotationXMultiplier(ParticleSystem system);

			// Token: 0x060000F1 RID: 241
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000F2 RID: 242
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartRotationY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000F3 RID: 243
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationYMultiplier(ParticleSystem system, float value);

			// Token: 0x060000F4 RID: 244
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartRotationYMultiplier(ParticleSystem system);

			// Token: 0x060000F5 RID: 245
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000F6 RID: 246
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartRotationZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000F7 RID: 247
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationZMultiplier(ParticleSystem system, float value);

			// Token: 0x060000F8 RID: 248
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartRotationZMultiplier(ParticleSystem system);

			// Token: 0x060000F9 RID: 249
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetFlipRotation(ParticleSystem system, float value);

			// Token: 0x060000FA RID: 250
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetFlipRotation(ParticleSystem system);

			// Token: 0x060000FB RID: 251
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x060000FC RID: 252
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x060000FD RID: 253
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetGravityModifier(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000FE RID: 254
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetGravityModifier(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060000FF RID: 255
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetGravityModifierMultiplier(ParticleSystem system, float value);

			// Token: 0x06000100 RID: 256
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetGravityModifierMultiplier(ParticleSystem system);

			// Token: 0x06000101 RID: 257
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSimulationSpace(ParticleSystem system, ParticleSystemSimulationSpace value);

			// Token: 0x06000102 RID: 258
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemSimulationSpace GetSimulationSpace(ParticleSystem system);

			// Token: 0x06000103 RID: 259
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCustomSimulationSpace(ParticleSystem system, Transform value);

			// Token: 0x06000104 RID: 260
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Transform GetCustomSimulationSpace(ParticleSystem system);

			// Token: 0x06000105 RID: 261
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSimulationSpeed(ParticleSystem system, float value);

			// Token: 0x06000106 RID: 262
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetSimulationSpeed(ParticleSystem system);

			// Token: 0x06000107 RID: 263
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseUnscaledTime(ParticleSystem system, bool value);

			// Token: 0x06000108 RID: 264
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseUnscaledTime(ParticleSystem system);

			// Token: 0x06000109 RID: 265
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetScalingMode(ParticleSystem system, ParticleSystemScalingMode value);

			// Token: 0x0600010A RID: 266
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemScalingMode GetScalingMode(ParticleSystem system);

			// Token: 0x0600010B RID: 267
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetPlayOnAwake(ParticleSystem system, bool value);

			// Token: 0x0600010C RID: 268
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetPlayOnAwake(ParticleSystem system);

			// Token: 0x0600010D RID: 269
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMaxParticles(ParticleSystem system, int value);

			// Token: 0x0600010E RID: 270
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMaxParticles(ParticleSystem system);

			// Token: 0x0600010F RID: 271
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseRigidbodyForVelocity(ParticleSystem system, bool value);

			// Token: 0x06000110 RID: 272
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseRigidbodyForVelocity(ParticleSystem system);

			// Token: 0x06000111 RID: 273
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStopAction(ParticleSystem system, ParticleSystemStopAction value);

			// Token: 0x06000112 RID: 274
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemStopAction GetStopAction(ParticleSystem system);

			// Token: 0x06000113 RID: 275
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCullingMode(ParticleSystem system, ParticleSystemCullingMode value);

			// Token: 0x06000114 RID: 276
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemCullingMode GetCullingMode(ParticleSystem system);

			// Token: 0x06000115 RID: 277
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRingBufferMode(ParticleSystem system, ParticleSystemRingBufferMode value);

			// Token: 0x06000116 RID: 278
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemRingBufferMode GetRingBufferMode(ParticleSystem system);

			// Token: 0x06000117 RID: 279 RVA: 0x00003207 File Offset: 0x00001407
			private static void SetRingBufferLoopRange(ParticleSystem system, Vector2 value)
			{
				ParticleSystem.MainModule.INTERNAL_CALL_SetRingBufferLoopRange(system, ref value);
			}

			// Token: 0x06000118 RID: 280
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetRingBufferLoopRange(ParticleSystem system, ref Vector2 value);

			// Token: 0x06000119 RID: 281 RVA: 0x00003214 File Offset: 0x00001414
			private static Vector2 GetRingBufferLoopRange(ParticleSystem system)
			{
				Vector2 result;
				ParticleSystem.MainModule.INTERNAL_CALL_GetRingBufferLoopRange(system, out result);
				return result;
			}

			// Token: 0x0600011A RID: 282
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetRingBufferLoopRange(ParticleSystem system, out Vector2 value);

			// Token: 0x04000004 RID: 4
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000005 RID: 5
		public struct EmissionModule
		{
			// Token: 0x0600011B RID: 283 RVA: 0x00003232 File Offset: 0x00001432
			internal EmissionModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x1700005E RID: 94
			// (get) Token: 0x0600011C RID: 284 RVA: 0x0000323C File Offset: 0x0000143C
			// (set) Token: 0x0600011D RID: 285 RVA: 0x00003252 File Offset: 0x00001452
			[Obsolete("ParticleSystemEmissionType no longer does anything. Time and Distance based emission are now both always active.", false)]
			public ParticleSystemEmissionType type
			{
				get
				{
					return ParticleSystemEmissionType.Time;
				}
				set
				{
				}
			}

			// Token: 0x1700005F RID: 95
			// (get) Token: 0x0600011E RID: 286 RVA: 0x00003258 File Offset: 0x00001458
			// (set) Token: 0x0600011F RID: 287 RVA: 0x00003273 File Offset: 0x00001473
			[Obsolete("rate property is deprecated. Use rateOverTime or rateOverDistance instead.", false)]
			public ParticleSystem.MinMaxCurve rate
			{
				get
				{
					return this.rateOverTime;
				}
				set
				{
					this.rateOverTime = value;
				}
			}

			// Token: 0x17000060 RID: 96
			// (get) Token: 0x06000120 RID: 288 RVA: 0x00003280 File Offset: 0x00001480
			// (set) Token: 0x06000121 RID: 289 RVA: 0x0000329B File Offset: 0x0000149B
			[Obsolete("rateMultiplier property is deprecated. Use rateOverTimeMultiplier or rateOverDistanceMultiplier instead.", false)]
			public float rateMultiplier
			{
				get
				{
					return this.rateOverTimeMultiplier;
				}
				set
				{
					this.rateOverTimeMultiplier = value;
				}
			}

			// Token: 0x17000061 RID: 97
			// (get) Token: 0x06000123 RID: 291 RVA: 0x000032B4 File Offset: 0x000014B4
			// (set) Token: 0x06000122 RID: 290 RVA: 0x000032A5 File Offset: 0x000014A5
			public bool enabled
			{
				get
				{
					return ParticleSystem.EmissionModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.EmissionModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000062 RID: 98
			// (get) Token: 0x06000125 RID: 293 RVA: 0x000032E4 File Offset: 0x000014E4
			// (set) Token: 0x06000124 RID: 292 RVA: 0x000032D4 File Offset: 0x000014D4
			public ParticleSystem.MinMaxCurve rateOverTime
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.EmissionModule.GetRateOverTime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.EmissionModule.SetRateOverTime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000063 RID: 99
			// (get) Token: 0x06000126 RID: 294 RVA: 0x00003310 File Offset: 0x00001510
			// (set) Token: 0x06000127 RID: 295 RVA: 0x00003330 File Offset: 0x00001530
			public float rateOverTimeMultiplier
			{
				get
				{
					return ParticleSystem.EmissionModule.GetRateOverTimeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.EmissionModule.SetRateOverTimeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000064 RID: 100
			// (get) Token: 0x06000129 RID: 297 RVA: 0x00003350 File Offset: 0x00001550
			// (set) Token: 0x06000128 RID: 296 RVA: 0x0000333F File Offset: 0x0000153F
			public ParticleSystem.MinMaxCurve rateOverDistance
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.EmissionModule.GetRateOverDistance(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.EmissionModule.SetRateOverDistance(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000065 RID: 101
			// (get) Token: 0x0600012A RID: 298 RVA: 0x0000337C File Offset: 0x0000157C
			// (set) Token: 0x0600012B RID: 299 RVA: 0x0000339C File Offset: 0x0000159C
			public float rateOverDistanceMultiplier
			{
				get
				{
					return ParticleSystem.EmissionModule.GetRateOverDistanceMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.EmissionModule.SetRateOverDistanceMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x0600012C RID: 300 RVA: 0x000033AB File Offset: 0x000015AB
			public void SetBursts(ParticleSystem.Burst[] bursts)
			{
				this.SetBursts(bursts, bursts.Length);
			}

			// Token: 0x0600012D RID: 301 RVA: 0x000033B8 File Offset: 0x000015B8
			public void SetBursts(ParticleSystem.Burst[] bursts, int size)
			{
				this.burstCount = size;
				for (int i = 0; i < size; i++)
				{
					ParticleSystem.EmissionModule.SetBurst(this.m_ParticleSystem, i, bursts[i]);
				}
			}

			// Token: 0x0600012E RID: 302 RVA: 0x000033FC File Offset: 0x000015FC
			public int GetBursts(ParticleSystem.Burst[] bursts)
			{
				int burstCount = this.burstCount;
				for (int i = 0; i < burstCount; i++)
				{
					bursts[i] = ParticleSystem.EmissionModule.GetBurst(this.m_ParticleSystem, i);
				}
				return burstCount;
			}

			// Token: 0x0600012F RID: 303 RVA: 0x00003445 File Offset: 0x00001645
			public void SetBurst(int index, ParticleSystem.Burst burst)
			{
				ParticleSystem.EmissionModule.SetBurst(this.m_ParticleSystem, index, burst);
			}

			// Token: 0x06000130 RID: 304 RVA: 0x00003458 File Offset: 0x00001658
			public ParticleSystem.Burst GetBurst(int index)
			{
				return ParticleSystem.EmissionModule.GetBurst(this.m_ParticleSystem, index);
			}

			// Token: 0x17000066 RID: 102
			// (get) Token: 0x06000131 RID: 305 RVA: 0x0000347C File Offset: 0x0000167C
			// (set) Token: 0x06000132 RID: 306 RVA: 0x0000349C File Offset: 0x0000169C
			public int burstCount
			{
				get
				{
					return ParticleSystem.EmissionModule.GetBurstCount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.EmissionModule.SetBurstCount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06000133 RID: 307
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06000134 RID: 308
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06000135 RID: 309
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetBurstCount(ParticleSystem system);

			// Token: 0x06000136 RID: 310
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRateOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000137 RID: 311
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRateOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000138 RID: 312
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRateOverTimeMultiplier(ParticleSystem system, float value);

			// Token: 0x06000139 RID: 313
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRateOverTimeMultiplier(ParticleSystem system);

			// Token: 0x0600013A RID: 314
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRateOverDistance(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600013B RID: 315
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRateOverDistance(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600013C RID: 316
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRateOverDistanceMultiplier(ParticleSystem system, float value);

			// Token: 0x0600013D RID: 317
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRateOverDistanceMultiplier(ParticleSystem system);

			// Token: 0x0600013E RID: 318
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetBurstCount(ParticleSystem system, int value);

			// Token: 0x0600013F RID: 319 RVA: 0x000034AB File Offset: 0x000016AB
			private static void SetBurst(ParticleSystem system, int index, ParticleSystem.Burst burst)
			{
				ParticleSystem.EmissionModule.INTERNAL_CALL_SetBurst(system, index, ref burst);
			}

			// Token: 0x06000140 RID: 320
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetBurst(ParticleSystem system, int index, ref ParticleSystem.Burst burst);

			// Token: 0x06000141 RID: 321 RVA: 0x000034B8 File Offset: 0x000016B8
			private static ParticleSystem.Burst GetBurst(ParticleSystem system, int index)
			{
				ParticleSystem.Burst result;
				ParticleSystem.EmissionModule.INTERNAL_CALL_GetBurst(system, index, out result);
				return result;
			}

			// Token: 0x06000142 RID: 322
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetBurst(ParticleSystem system, int index, out ParticleSystem.Burst value);

			// Token: 0x04000005 RID: 5
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000006 RID: 6
		public struct ShapeModule
		{
			// Token: 0x06000143 RID: 323 RVA: 0x000034D7 File Offset: 0x000016D7
			internal ShapeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000067 RID: 103
			// (get) Token: 0x06000144 RID: 324 RVA: 0x000034E4 File Offset: 0x000016E4
			// (set) Token: 0x06000145 RID: 325 RVA: 0x00003504 File Offset: 0x00001704
			[Obsolete("Please use scale instead. (UnityUpgradable) -> UnityEngine.ParticleSystem/ShapeModule.scale", false)]
			public Vector3 box
			{
				get
				{
					return ParticleSystem.ShapeModule.GetScale(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetScale(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000068 RID: 104
			// (get) Token: 0x06000146 RID: 326 RVA: 0x00003514 File Offset: 0x00001714
			// (set) Token: 0x06000147 RID: 327 RVA: 0x00003537 File Offset: 0x00001737
			[Obsolete("meshScale property is deprecated.Please use scale instead.", false)]
			public float meshScale
			{
				get
				{
					return this.scale.x;
				}
				set
				{
					this.scale = new Vector3(value, value, value);
				}
			}

			// Token: 0x17000069 RID: 105
			// (get) Token: 0x06000148 RID: 328 RVA: 0x00003548 File Offset: 0x00001748
			// (set) Token: 0x06000149 RID: 329 RVA: 0x0000356D File Offset: 0x0000176D
			[Obsolete("randomDirection property is deprecated. Use randomDirectionAmount instead.", false)]
			public bool randomDirection
			{
				get
				{
					return this.randomDirectionAmount >= 0.5f;
				}
				set
				{
					this.randomDirectionAmount = ((!value) ? 0f : 1f);
				}
			}

			// Token: 0x1700006A RID: 106
			// (get) Token: 0x0600014B RID: 331 RVA: 0x0000359C File Offset: 0x0000179C
			// (set) Token: 0x0600014A RID: 330 RVA: 0x0000358B File Offset: 0x0000178B
			public bool enabled
			{
				get
				{
					return ParticleSystem.ShapeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700006B RID: 107
			// (get) Token: 0x0600014C RID: 332 RVA: 0x000035BC File Offset: 0x000017BC
			// (set) Token: 0x0600014D RID: 333 RVA: 0x000035DC File Offset: 0x000017DC
			public ParticleSystemShapeType shapeType
			{
				get
				{
					return ParticleSystem.ShapeModule.GetShapeType(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetShapeType(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700006C RID: 108
			// (get) Token: 0x0600014E RID: 334 RVA: 0x000035EC File Offset: 0x000017EC
			// (set) Token: 0x0600014F RID: 335 RVA: 0x0000360C File Offset: 0x0000180C
			public float randomDirectionAmount
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRandomDirectionAmount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetRandomDirectionAmount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700006D RID: 109
			// (get) Token: 0x06000150 RID: 336 RVA: 0x0000361C File Offset: 0x0000181C
			// (set) Token: 0x06000151 RID: 337 RVA: 0x0000363C File Offset: 0x0000183C
			public float sphericalDirectionAmount
			{
				get
				{
					return ParticleSystem.ShapeModule.GetSphericalDirectionAmount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetSphericalDirectionAmount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700006E RID: 110
			// (get) Token: 0x06000152 RID: 338 RVA: 0x0000364C File Offset: 0x0000184C
			// (set) Token: 0x06000153 RID: 339 RVA: 0x0000366C File Offset: 0x0000186C
			public float randomPositionAmount
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRandomPositionAmount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetRandomPositionAmount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700006F RID: 111
			// (get) Token: 0x06000154 RID: 340 RVA: 0x0000367C File Offset: 0x0000187C
			// (set) Token: 0x06000155 RID: 341 RVA: 0x0000369C File Offset: 0x0000189C
			public bool alignToDirection
			{
				get
				{
					return ParticleSystem.ShapeModule.GetAlignToDirection(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetAlignToDirection(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000070 RID: 112
			// (get) Token: 0x06000156 RID: 342 RVA: 0x000036AC File Offset: 0x000018AC
			// (set) Token: 0x06000157 RID: 343 RVA: 0x000036CC File Offset: 0x000018CC
			public float radius
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRadius(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetRadius(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000071 RID: 113
			// (get) Token: 0x06000158 RID: 344 RVA: 0x000036DC File Offset: 0x000018DC
			// (set) Token: 0x06000159 RID: 345 RVA: 0x000036FC File Offset: 0x000018FC
			public ParticleSystemShapeMultiModeValue radiusMode
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRadiusMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetRadiusMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000072 RID: 114
			// (get) Token: 0x0600015A RID: 346 RVA: 0x0000370C File Offset: 0x0000190C
			// (set) Token: 0x0600015B RID: 347 RVA: 0x0000372C File Offset: 0x0000192C
			public float radiusSpread
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRadiusSpread(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetRadiusSpread(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000073 RID: 115
			// (get) Token: 0x0600015D RID: 349 RVA: 0x0000374C File Offset: 0x0000194C
			// (set) Token: 0x0600015C RID: 348 RVA: 0x0000373B File Offset: 0x0000193B
			public ParticleSystem.MinMaxCurve radiusSpeed
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.ShapeModule.GetRadiusSpeed(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ShapeModule.SetRadiusSpeed(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000074 RID: 116
			// (get) Token: 0x0600015E RID: 350 RVA: 0x00003778 File Offset: 0x00001978
			// (set) Token: 0x0600015F RID: 351 RVA: 0x00003798 File Offset: 0x00001998
			public float radiusSpeedMultiplier
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRadiusSpeedMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetRadiusSpeedMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000075 RID: 117
			// (get) Token: 0x06000160 RID: 352 RVA: 0x000037A8 File Offset: 0x000019A8
			// (set) Token: 0x06000161 RID: 353 RVA: 0x000037C8 File Offset: 0x000019C8
			public float radiusThickness
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRadiusThickness(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetRadiusThickness(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000076 RID: 118
			// (get) Token: 0x06000162 RID: 354 RVA: 0x000037D8 File Offset: 0x000019D8
			// (set) Token: 0x06000163 RID: 355 RVA: 0x000037F8 File Offset: 0x000019F8
			public float angle
			{
				get
				{
					return ParticleSystem.ShapeModule.GetAngle(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetAngle(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000077 RID: 119
			// (get) Token: 0x06000164 RID: 356 RVA: 0x00003808 File Offset: 0x00001A08
			// (set) Token: 0x06000165 RID: 357 RVA: 0x00003828 File Offset: 0x00001A28
			public float length
			{
				get
				{
					return ParticleSystem.ShapeModule.GetLength(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetLength(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000078 RID: 120
			// (get) Token: 0x06000166 RID: 358 RVA: 0x00003838 File Offset: 0x00001A38
			// (set) Token: 0x06000167 RID: 359 RVA: 0x00003858 File Offset: 0x00001A58
			public Vector3 boxThickness
			{
				get
				{
					return ParticleSystem.ShapeModule.GetBoxThickness(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetBoxThickness(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000079 RID: 121
			// (get) Token: 0x06000168 RID: 360 RVA: 0x00003868 File Offset: 0x00001A68
			// (set) Token: 0x06000169 RID: 361 RVA: 0x00003888 File Offset: 0x00001A88
			public ParticleSystemMeshShapeType meshShapeType
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMeshShapeType(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshShapeType(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700007A RID: 122
			// (get) Token: 0x0600016A RID: 362 RVA: 0x00003898 File Offset: 0x00001A98
			// (set) Token: 0x0600016B RID: 363 RVA: 0x000038B8 File Offset: 0x00001AB8
			public Mesh mesh
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMesh(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMesh(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700007B RID: 123
			// (get) Token: 0x0600016C RID: 364 RVA: 0x000038C8 File Offset: 0x00001AC8
			// (set) Token: 0x0600016D RID: 365 RVA: 0x000038E8 File Offset: 0x00001AE8
			public MeshRenderer meshRenderer
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMeshRenderer(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshRenderer(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700007C RID: 124
			// (get) Token: 0x0600016E RID: 366 RVA: 0x000038F8 File Offset: 0x00001AF8
			// (set) Token: 0x0600016F RID: 367 RVA: 0x00003918 File Offset: 0x00001B18
			public SkinnedMeshRenderer skinnedMeshRenderer
			{
				get
				{
					return ParticleSystem.ShapeModule.GetSkinnedMeshRenderer(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetSkinnedMeshRenderer(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700007D RID: 125
			// (get) Token: 0x06000170 RID: 368 RVA: 0x00003928 File Offset: 0x00001B28
			// (set) Token: 0x06000171 RID: 369 RVA: 0x00003948 File Offset: 0x00001B48
			public Sprite sprite
			{
				get
				{
					return ParticleSystem.ShapeModule.GetSprite(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetSprite(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700007E RID: 126
			// (get) Token: 0x06000172 RID: 370 RVA: 0x00003958 File Offset: 0x00001B58
			// (set) Token: 0x06000173 RID: 371 RVA: 0x00003978 File Offset: 0x00001B78
			public SpriteRenderer spriteRenderer
			{
				get
				{
					return ParticleSystem.ShapeModule.GetSpriteRenderer(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetSpriteRenderer(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700007F RID: 127
			// (get) Token: 0x06000174 RID: 372 RVA: 0x00003988 File Offset: 0x00001B88
			// (set) Token: 0x06000175 RID: 373 RVA: 0x000039A8 File Offset: 0x00001BA8
			public bool useMeshMaterialIndex
			{
				get
				{
					return ParticleSystem.ShapeModule.GetUseMeshMaterialIndex(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetUseMeshMaterialIndex(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000080 RID: 128
			// (get) Token: 0x06000176 RID: 374 RVA: 0x000039B8 File Offset: 0x00001BB8
			// (set) Token: 0x06000177 RID: 375 RVA: 0x000039D8 File Offset: 0x00001BD8
			public int meshMaterialIndex
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMeshMaterialIndex(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshMaterialIndex(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000081 RID: 129
			// (get) Token: 0x06000178 RID: 376 RVA: 0x000039E8 File Offset: 0x00001BE8
			// (set) Token: 0x06000179 RID: 377 RVA: 0x00003A08 File Offset: 0x00001C08
			public bool useMeshColors
			{
				get
				{
					return ParticleSystem.ShapeModule.GetUseMeshColors(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetUseMeshColors(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000082 RID: 130
			// (get) Token: 0x0600017A RID: 378 RVA: 0x00003A18 File Offset: 0x00001C18
			// (set) Token: 0x0600017B RID: 379 RVA: 0x00003A38 File Offset: 0x00001C38
			public float normalOffset
			{
				get
				{
					return ParticleSystem.ShapeModule.GetNormalOffset(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetNormalOffset(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000083 RID: 131
			// (get) Token: 0x0600017C RID: 380 RVA: 0x00003A48 File Offset: 0x00001C48
			// (set) Token: 0x0600017D RID: 381 RVA: 0x00003A68 File Offset: 0x00001C68
			public ParticleSystemShapeMultiModeValue meshSpawnMode
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMeshSpawnMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshSpawnMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000084 RID: 132
			// (get) Token: 0x0600017E RID: 382 RVA: 0x00003A78 File Offset: 0x00001C78
			// (set) Token: 0x0600017F RID: 383 RVA: 0x00003A98 File Offset: 0x00001C98
			public float meshSpawnSpread
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMeshSpawnSpread(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshSpawnSpread(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000085 RID: 133
			// (get) Token: 0x06000181 RID: 385 RVA: 0x00003AB8 File Offset: 0x00001CB8
			// (set) Token: 0x06000180 RID: 384 RVA: 0x00003AA7 File Offset: 0x00001CA7
			public ParticleSystem.MinMaxCurve meshSpawnSpeed
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.ShapeModule.GetMeshSpawnSpeed(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshSpawnSpeed(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000086 RID: 134
			// (get) Token: 0x06000182 RID: 386 RVA: 0x00003AE4 File Offset: 0x00001CE4
			// (set) Token: 0x06000183 RID: 387 RVA: 0x00003B04 File Offset: 0x00001D04
			public float meshSpawnSpeedMultiplier
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMeshSpawnSpeedMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshSpawnSpeedMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000087 RID: 135
			// (get) Token: 0x06000184 RID: 388 RVA: 0x00003B14 File Offset: 0x00001D14
			// (set) Token: 0x06000185 RID: 389 RVA: 0x00003B34 File Offset: 0x00001D34
			public float arc
			{
				get
				{
					return ParticleSystem.ShapeModule.GetArc(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetArc(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000088 RID: 136
			// (get) Token: 0x06000186 RID: 390 RVA: 0x00003B44 File Offset: 0x00001D44
			// (set) Token: 0x06000187 RID: 391 RVA: 0x00003B64 File Offset: 0x00001D64
			public ParticleSystemShapeMultiModeValue arcMode
			{
				get
				{
					return ParticleSystem.ShapeModule.GetArcMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetArcMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000089 RID: 137
			// (get) Token: 0x06000188 RID: 392 RVA: 0x00003B74 File Offset: 0x00001D74
			// (set) Token: 0x06000189 RID: 393 RVA: 0x00003B94 File Offset: 0x00001D94
			public float arcSpread
			{
				get
				{
					return ParticleSystem.ShapeModule.GetArcSpread(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetArcSpread(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700008A RID: 138
			// (get) Token: 0x0600018B RID: 395 RVA: 0x00003BB4 File Offset: 0x00001DB4
			// (set) Token: 0x0600018A RID: 394 RVA: 0x00003BA3 File Offset: 0x00001DA3
			public ParticleSystem.MinMaxCurve arcSpeed
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.ShapeModule.GetArcSpeed(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ShapeModule.SetArcSpeed(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700008B RID: 139
			// (get) Token: 0x0600018C RID: 396 RVA: 0x00003BE0 File Offset: 0x00001DE0
			// (set) Token: 0x0600018D RID: 397 RVA: 0x00003C00 File Offset: 0x00001E00
			public float arcSpeedMultiplier
			{
				get
				{
					return ParticleSystem.ShapeModule.GetArcSpeedMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetArcSpeedMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700008C RID: 140
			// (get) Token: 0x0600018E RID: 398 RVA: 0x00003C10 File Offset: 0x00001E10
			// (set) Token: 0x0600018F RID: 399 RVA: 0x00003C30 File Offset: 0x00001E30
			public float donutRadius
			{
				get
				{
					return ParticleSystem.ShapeModule.GetDonutRadius(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetDonutRadius(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700008D RID: 141
			// (get) Token: 0x06000190 RID: 400 RVA: 0x00003C40 File Offset: 0x00001E40
			// (set) Token: 0x06000191 RID: 401 RVA: 0x00003C60 File Offset: 0x00001E60
			public Vector3 position
			{
				get
				{
					return ParticleSystem.ShapeModule.GetPosition(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetPosition(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700008E RID: 142
			// (get) Token: 0x06000192 RID: 402 RVA: 0x00003C70 File Offset: 0x00001E70
			// (set) Token: 0x06000193 RID: 403 RVA: 0x00003C90 File Offset: 0x00001E90
			public Vector3 rotation
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRotation(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetRotation(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700008F RID: 143
			// (get) Token: 0x06000194 RID: 404 RVA: 0x00003CA0 File Offset: 0x00001EA0
			// (set) Token: 0x06000195 RID: 405 RVA: 0x00003504 File Offset: 0x00001704
			public Vector3 scale
			{
				get
				{
					return ParticleSystem.ShapeModule.GetScale(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetScale(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000090 RID: 144
			// (get) Token: 0x06000196 RID: 406 RVA: 0x00003CC0 File Offset: 0x00001EC0
			// (set) Token: 0x06000197 RID: 407 RVA: 0x00003CE0 File Offset: 0x00001EE0
			public Texture2D texture
			{
				get
				{
					return ParticleSystem.ShapeModule.GetTexture(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetTexture(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000091 RID: 145
			// (get) Token: 0x06000198 RID: 408 RVA: 0x00003CF0 File Offset: 0x00001EF0
			// (set) Token: 0x06000199 RID: 409 RVA: 0x00003D10 File Offset: 0x00001F10
			public ParticleSystemShapeTextureChannel textureClipChannel
			{
				get
				{
					return (ParticleSystemShapeTextureChannel)ParticleSystem.ShapeModule.GetTextureClipChannel(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetTextureClipChannel(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x17000092 RID: 146
			// (get) Token: 0x0600019A RID: 410 RVA: 0x00003D20 File Offset: 0x00001F20
			// (set) Token: 0x0600019B RID: 411 RVA: 0x00003D40 File Offset: 0x00001F40
			public float textureClipThreshold
			{
				get
				{
					return ParticleSystem.ShapeModule.GetTextureClipThreshold(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetTextureClipThreshold(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000093 RID: 147
			// (get) Token: 0x0600019C RID: 412 RVA: 0x00003D50 File Offset: 0x00001F50
			// (set) Token: 0x0600019D RID: 413 RVA: 0x00003D70 File Offset: 0x00001F70
			public bool textureColorAffectsParticles
			{
				get
				{
					return ParticleSystem.ShapeModule.GetTextureColorAffectsParticles(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetTextureColorAffectsParticles(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000094 RID: 148
			// (get) Token: 0x0600019E RID: 414 RVA: 0x00003D80 File Offset: 0x00001F80
			// (set) Token: 0x0600019F RID: 415 RVA: 0x00003DA0 File Offset: 0x00001FA0
			public bool textureAlphaAffectsParticles
			{
				get
				{
					return ParticleSystem.ShapeModule.GetTextureAlphaAffectsParticles(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetTextureAlphaAffectsParticles(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000095 RID: 149
			// (get) Token: 0x060001A0 RID: 416 RVA: 0x00003DB0 File Offset: 0x00001FB0
			// (set) Token: 0x060001A1 RID: 417 RVA: 0x00003DD0 File Offset: 0x00001FD0
			public bool textureBilinearFiltering
			{
				get
				{
					return ParticleSystem.ShapeModule.GetTextureBilinearFiltering(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetTextureBilinearFiltering(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000096 RID: 150
			// (get) Token: 0x060001A2 RID: 418 RVA: 0x00003DE0 File Offset: 0x00001FE0
			// (set) Token: 0x060001A3 RID: 419 RVA: 0x00003E00 File Offset: 0x00002000
			public int textureUVChannel
			{
				get
				{
					return ParticleSystem.ShapeModule.GetTextureUVChannel(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetTextureUVChannel(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060001A4 RID: 420
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060001A5 RID: 421
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060001A6 RID: 422
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetShapeType(ParticleSystem system, ParticleSystemShapeType value);

			// Token: 0x060001A7 RID: 423
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemShapeType GetShapeType(ParticleSystem system);

			// Token: 0x060001A8 RID: 424
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRandomDirectionAmount(ParticleSystem system, float value);

			// Token: 0x060001A9 RID: 425
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRandomDirectionAmount(ParticleSystem system);

			// Token: 0x060001AA RID: 426
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSphericalDirectionAmount(ParticleSystem system, float value);

			// Token: 0x060001AB RID: 427
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetSphericalDirectionAmount(ParticleSystem system);

			// Token: 0x060001AC RID: 428
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRandomPositionAmount(ParticleSystem system, float value);

			// Token: 0x060001AD RID: 429
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRandomPositionAmount(ParticleSystem system);

			// Token: 0x060001AE RID: 430
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetAlignToDirection(ParticleSystem system, bool value);

			// Token: 0x060001AF RID: 431
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetAlignToDirection(ParticleSystem system);

			// Token: 0x060001B0 RID: 432
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadius(ParticleSystem system, float value);

			// Token: 0x060001B1 RID: 433
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRadius(ParticleSystem system);

			// Token: 0x060001B2 RID: 434
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadiusMode(ParticleSystem system, ParticleSystemShapeMultiModeValue value);

			// Token: 0x060001B3 RID: 435
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemShapeMultiModeValue GetRadiusMode(ParticleSystem system);

			// Token: 0x060001B4 RID: 436
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadiusSpread(ParticleSystem system, float value);

			// Token: 0x060001B5 RID: 437
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRadiusSpread(ParticleSystem system);

			// Token: 0x060001B6 RID: 438
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadiusSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060001B7 RID: 439
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRadiusSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060001B8 RID: 440
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadiusSpeedMultiplier(ParticleSystem system, float value);

			// Token: 0x060001B9 RID: 441
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRadiusSpeedMultiplier(ParticleSystem system);

			// Token: 0x060001BA RID: 442
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadiusThickness(ParticleSystem system, float value);

			// Token: 0x060001BB RID: 443
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRadiusThickness(ParticleSystem system);

			// Token: 0x060001BC RID: 444
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetAngle(ParticleSystem system, float value);

			// Token: 0x060001BD RID: 445
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetAngle(ParticleSystem system);

			// Token: 0x060001BE RID: 446
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLength(ParticleSystem system, float value);

			// Token: 0x060001BF RID: 447
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetLength(ParticleSystem system);

			// Token: 0x060001C0 RID: 448 RVA: 0x00003E0F File Offset: 0x0000200F
			private static void SetBoxThickness(ParticleSystem system, Vector3 value)
			{
				ParticleSystem.ShapeModule.INTERNAL_CALL_SetBoxThickness(system, ref value);
			}

			// Token: 0x060001C1 RID: 449
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetBoxThickness(ParticleSystem system, ref Vector3 value);

			// Token: 0x060001C2 RID: 450 RVA: 0x00003E1C File Offset: 0x0000201C
			private static Vector3 GetBoxThickness(ParticleSystem system)
			{
				Vector3 result;
				ParticleSystem.ShapeModule.INTERNAL_CALL_GetBoxThickness(system, out result);
				return result;
			}

			// Token: 0x060001C3 RID: 451
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetBoxThickness(ParticleSystem system, out Vector3 value);

			// Token: 0x060001C4 RID: 452
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshShapeType(ParticleSystem system, ParticleSystemMeshShapeType value);

			// Token: 0x060001C5 RID: 453
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemMeshShapeType GetMeshShapeType(ParticleSystem system);

			// Token: 0x060001C6 RID: 454
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMesh(ParticleSystem system, Mesh value);

			// Token: 0x060001C7 RID: 455
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Mesh GetMesh(ParticleSystem system);

			// Token: 0x060001C8 RID: 456
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshRenderer(ParticleSystem system, MeshRenderer value);

			// Token: 0x060001C9 RID: 457
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern MeshRenderer GetMeshRenderer(ParticleSystem system);

			// Token: 0x060001CA RID: 458
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSkinnedMeshRenderer(ParticleSystem system, SkinnedMeshRenderer value);

			// Token: 0x060001CB RID: 459
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern SkinnedMeshRenderer GetSkinnedMeshRenderer(ParticleSystem system);

			// Token: 0x060001CC RID: 460
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSprite(ParticleSystem system, Sprite value);

			// Token: 0x060001CD RID: 461
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Sprite GetSprite(ParticleSystem system);

			// Token: 0x060001CE RID: 462
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSpriteRenderer(ParticleSystem system, SpriteRenderer value);

			// Token: 0x060001CF RID: 463
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern SpriteRenderer GetSpriteRenderer(ParticleSystem system);

			// Token: 0x060001D0 RID: 464
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseMeshMaterialIndex(ParticleSystem system, bool value);

			// Token: 0x060001D1 RID: 465
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseMeshMaterialIndex(ParticleSystem system);

			// Token: 0x060001D2 RID: 466
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshMaterialIndex(ParticleSystem system, int value);

			// Token: 0x060001D3 RID: 467
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMeshMaterialIndex(ParticleSystem system);

			// Token: 0x060001D4 RID: 468
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseMeshColors(ParticleSystem system, bool value);

			// Token: 0x060001D5 RID: 469
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseMeshColors(ParticleSystem system);

			// Token: 0x060001D6 RID: 470
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetNormalOffset(ParticleSystem system, float value);

			// Token: 0x060001D7 RID: 471
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetNormalOffset(ParticleSystem system);

			// Token: 0x060001D8 RID: 472
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshSpawnMode(ParticleSystem system, ParticleSystemShapeMultiModeValue value);

			// Token: 0x060001D9 RID: 473
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemShapeMultiModeValue GetMeshSpawnMode(ParticleSystem system);

			// Token: 0x060001DA RID: 474
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshSpawnSpread(ParticleSystem system, float value);

			// Token: 0x060001DB RID: 475
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMeshSpawnSpread(ParticleSystem system);

			// Token: 0x060001DC RID: 476
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshSpawnSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060001DD RID: 477
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetMeshSpawnSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060001DE RID: 478
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshSpawnSpeedMultiplier(ParticleSystem system, float value);

			// Token: 0x060001DF RID: 479
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMeshSpawnSpeedMultiplier(ParticleSystem system);

			// Token: 0x060001E0 RID: 480
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetArc(ParticleSystem system, float value);

			// Token: 0x060001E1 RID: 481
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetArc(ParticleSystem system);

			// Token: 0x060001E2 RID: 482
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetArcMode(ParticleSystem system, ParticleSystemShapeMultiModeValue value);

			// Token: 0x060001E3 RID: 483
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemShapeMultiModeValue GetArcMode(ParticleSystem system);

			// Token: 0x060001E4 RID: 484
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetArcSpread(ParticleSystem system, float value);

			// Token: 0x060001E5 RID: 485
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetArcSpread(ParticleSystem system);

			// Token: 0x060001E6 RID: 486
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetArcSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060001E7 RID: 487
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetArcSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060001E8 RID: 488
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetArcSpeedMultiplier(ParticleSystem system, float value);

			// Token: 0x060001E9 RID: 489
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetArcSpeedMultiplier(ParticleSystem system);

			// Token: 0x060001EA RID: 490
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDonutRadius(ParticleSystem system, float value);

			// Token: 0x060001EB RID: 491
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetDonutRadius(ParticleSystem system);

			// Token: 0x060001EC RID: 492 RVA: 0x00003E3A File Offset: 0x0000203A
			private static void SetPosition(ParticleSystem system, Vector3 value)
			{
				ParticleSystem.ShapeModule.INTERNAL_CALL_SetPosition(system, ref value);
			}

			// Token: 0x060001ED RID: 493
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetPosition(ParticleSystem system, ref Vector3 value);

			// Token: 0x060001EE RID: 494 RVA: 0x00003E48 File Offset: 0x00002048
			private static Vector3 GetPosition(ParticleSystem system)
			{
				Vector3 result;
				ParticleSystem.ShapeModule.INTERNAL_CALL_GetPosition(system, out result);
				return result;
			}

			// Token: 0x060001EF RID: 495
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetPosition(ParticleSystem system, out Vector3 value);

			// Token: 0x060001F0 RID: 496 RVA: 0x00003E66 File Offset: 0x00002066
			private static void SetRotation(ParticleSystem system, Vector3 value)
			{
				ParticleSystem.ShapeModule.INTERNAL_CALL_SetRotation(system, ref value);
			}

			// Token: 0x060001F1 RID: 497
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetRotation(ParticleSystem system, ref Vector3 value);

			// Token: 0x060001F2 RID: 498 RVA: 0x00003E74 File Offset: 0x00002074
			private static Vector3 GetRotation(ParticleSystem system)
			{
				Vector3 result;
				ParticleSystem.ShapeModule.INTERNAL_CALL_GetRotation(system, out result);
				return result;
			}

			// Token: 0x060001F3 RID: 499
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetRotation(ParticleSystem system, out Vector3 value);

			// Token: 0x060001F4 RID: 500 RVA: 0x00003E92 File Offset: 0x00002092
			private static void SetScale(ParticleSystem system, Vector3 value)
			{
				ParticleSystem.ShapeModule.INTERNAL_CALL_SetScale(system, ref value);
			}

			// Token: 0x060001F5 RID: 501
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetScale(ParticleSystem system, ref Vector3 value);

			// Token: 0x060001F6 RID: 502 RVA: 0x00003EA0 File Offset: 0x000020A0
			private static Vector3 GetScale(ParticleSystem system)
			{
				Vector3 result;
				ParticleSystem.ShapeModule.INTERNAL_CALL_GetScale(system, out result);
				return result;
			}

			// Token: 0x060001F7 RID: 503
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetScale(ParticleSystem system, out Vector3 value);

			// Token: 0x060001F8 RID: 504
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetTexture(ParticleSystem system, Texture2D value);

			// Token: 0x060001F9 RID: 505
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Texture2D GetTexture(ParticleSystem system);

			// Token: 0x060001FA RID: 506
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetTextureClipChannel(ParticleSystem system, int value);

			// Token: 0x060001FB RID: 507
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetTextureClipChannel(ParticleSystem system);

			// Token: 0x060001FC RID: 508
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetTextureClipThreshold(ParticleSystem system, float value);

			// Token: 0x060001FD RID: 509
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetTextureClipThreshold(ParticleSystem system);

			// Token: 0x060001FE RID: 510
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetTextureColorAffectsParticles(ParticleSystem system, bool value);

			// Token: 0x060001FF RID: 511
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetTextureColorAffectsParticles(ParticleSystem system);

			// Token: 0x06000200 RID: 512
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetTextureAlphaAffectsParticles(ParticleSystem system, bool value);

			// Token: 0x06000201 RID: 513
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetTextureAlphaAffectsParticles(ParticleSystem system);

			// Token: 0x06000202 RID: 514
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetTextureBilinearFiltering(ParticleSystem system, bool value);

			// Token: 0x06000203 RID: 515
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetTextureBilinearFiltering(ParticleSystem system);

			// Token: 0x06000204 RID: 516
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetTextureUVChannel(ParticleSystem system, int value);

			// Token: 0x06000205 RID: 517
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetTextureUVChannel(ParticleSystem system);

			// Token: 0x04000006 RID: 6
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000007 RID: 7
		public struct CollisionModule
		{
			// Token: 0x06000206 RID: 518 RVA: 0x00003EBE File Offset: 0x000020BE
			internal CollisionModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000097 RID: 151
			// (get) Token: 0x06000207 RID: 519 RVA: 0x00003EC8 File Offset: 0x000020C8
			// (set) Token: 0x06000208 RID: 520 RVA: 0x00003EE8 File Offset: 0x000020E8
			[Obsolete("enableInteriorCollisions property is deprecated and is no longer required and has no effect on the particle system.", false)]
			public bool enableInteriorCollisions
			{
				get
				{
					return ParticleSystem.CollisionModule.GetEnableInteriorCollisions(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetEnableInteriorCollisions(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000098 RID: 152
			// (get) Token: 0x0600020A RID: 522 RVA: 0x00003F08 File Offset: 0x00002108
			// (set) Token: 0x06000209 RID: 521 RVA: 0x00003EF7 File Offset: 0x000020F7
			public bool enabled
			{
				get
				{
					return ParticleSystem.CollisionModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000099 RID: 153
			// (get) Token: 0x0600020C RID: 524 RVA: 0x00003F38 File Offset: 0x00002138
			// (set) Token: 0x0600020B RID: 523 RVA: 0x00003F28 File Offset: 0x00002128
			public ParticleSystemCollisionType type
			{
				get
				{
					return ParticleSystem.CollisionModule.GetType(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetType(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700009A RID: 154
			// (get) Token: 0x0600020E RID: 526 RVA: 0x00003F68 File Offset: 0x00002168
			// (set) Token: 0x0600020D RID: 525 RVA: 0x00003F58 File Offset: 0x00002158
			public ParticleSystemCollisionMode mode
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700009B RID: 155
			// (get) Token: 0x06000210 RID: 528 RVA: 0x00003F98 File Offset: 0x00002198
			// (set) Token: 0x0600020F RID: 527 RVA: 0x00003F88 File Offset: 0x00002188
			public ParticleSystem.MinMaxCurve dampen
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.CollisionModule.GetDampen(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.CollisionModule.SetDampen(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700009C RID: 156
			// (get) Token: 0x06000211 RID: 529 RVA: 0x00003FC4 File Offset: 0x000021C4
			// (set) Token: 0x06000212 RID: 530 RVA: 0x00003FE4 File Offset: 0x000021E4
			public float dampenMultiplier
			{
				get
				{
					return ParticleSystem.CollisionModule.GetDampenMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetDampenMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700009D RID: 157
			// (get) Token: 0x06000214 RID: 532 RVA: 0x00004004 File Offset: 0x00002204
			// (set) Token: 0x06000213 RID: 531 RVA: 0x00003FF3 File Offset: 0x000021F3
			public ParticleSystem.MinMaxCurve bounce
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.CollisionModule.GetBounce(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.CollisionModule.SetBounce(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700009E RID: 158
			// (get) Token: 0x06000215 RID: 533 RVA: 0x00004030 File Offset: 0x00002230
			// (set) Token: 0x06000216 RID: 534 RVA: 0x00004050 File Offset: 0x00002250
			public float bounceMultiplier
			{
				get
				{
					return ParticleSystem.CollisionModule.GetBounceMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetBounceMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700009F RID: 159
			// (get) Token: 0x06000218 RID: 536 RVA: 0x00004070 File Offset: 0x00002270
			// (set) Token: 0x06000217 RID: 535 RVA: 0x0000405F File Offset: 0x0000225F
			public ParticleSystem.MinMaxCurve lifetimeLoss
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.CollisionModule.GetLifetimeLoss(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.CollisionModule.SetLifetimeLoss(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170000A0 RID: 160
			// (get) Token: 0x06000219 RID: 537 RVA: 0x0000409C File Offset: 0x0000229C
			// (set) Token: 0x0600021A RID: 538 RVA: 0x000040BC File Offset: 0x000022BC
			public float lifetimeLossMultiplier
			{
				get
				{
					return ParticleSystem.CollisionModule.GetLifetimeLossMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetLifetimeLossMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000A1 RID: 161
			// (get) Token: 0x0600021B RID: 539 RVA: 0x000040CC File Offset: 0x000022CC
			// (set) Token: 0x0600021C RID: 540 RVA: 0x000040EC File Offset: 0x000022EC
			public float minKillSpeed
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMinKillSpeed(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMinKillSpeed(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000A2 RID: 162
			// (get) Token: 0x0600021D RID: 541 RVA: 0x000040FC File Offset: 0x000022FC
			// (set) Token: 0x0600021E RID: 542 RVA: 0x0000411C File Offset: 0x0000231C
			public float maxKillSpeed
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMaxKillSpeed(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMaxKillSpeed(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000A3 RID: 163
			// (get) Token: 0x0600021F RID: 543 RVA: 0x0000412C File Offset: 0x0000232C
			// (set) Token: 0x06000220 RID: 544 RVA: 0x00004151 File Offset: 0x00002351
			public LayerMask collidesWith
			{
				get
				{
					return ParticleSystem.CollisionModule.GetCollidesWith(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetCollidesWith(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000A4 RID: 164
			// (get) Token: 0x06000221 RID: 545 RVA: 0x00004168 File Offset: 0x00002368
			// (set) Token: 0x06000222 RID: 546 RVA: 0x00004188 File Offset: 0x00002388
			public bool enableDynamicColliders
			{
				get
				{
					return ParticleSystem.CollisionModule.GetEnableDynamicColliders(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetEnableDynamicColliders(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000A5 RID: 165
			// (get) Token: 0x06000223 RID: 547 RVA: 0x00004198 File Offset: 0x00002398
			// (set) Token: 0x06000224 RID: 548 RVA: 0x000041B8 File Offset: 0x000023B8
			public int maxCollisionShapes
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMaxCollisionShapes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMaxCollisionShapes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000A6 RID: 166
			// (get) Token: 0x06000226 RID: 550 RVA: 0x000041D8 File Offset: 0x000023D8
			// (set) Token: 0x06000225 RID: 549 RVA: 0x000041C7 File Offset: 0x000023C7
			public ParticleSystemCollisionQuality quality
			{
				get
				{
					return (ParticleSystemCollisionQuality)ParticleSystem.CollisionModule.GetQuality(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetQuality(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x170000A7 RID: 167
			// (get) Token: 0x06000227 RID: 551 RVA: 0x000041F8 File Offset: 0x000023F8
			// (set) Token: 0x06000228 RID: 552 RVA: 0x00004218 File Offset: 0x00002418
			public float voxelSize
			{
				get
				{
					return ParticleSystem.CollisionModule.GetVoxelSize(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetVoxelSize(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000A8 RID: 168
			// (get) Token: 0x06000229 RID: 553 RVA: 0x00004228 File Offset: 0x00002428
			// (set) Token: 0x0600022A RID: 554 RVA: 0x00004248 File Offset: 0x00002448
			public float radiusScale
			{
				get
				{
					return ParticleSystem.CollisionModule.GetRadiusScale(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetRadiusScale(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000A9 RID: 169
			// (get) Token: 0x0600022B RID: 555 RVA: 0x00004258 File Offset: 0x00002458
			// (set) Token: 0x0600022C RID: 556 RVA: 0x00004278 File Offset: 0x00002478
			public bool sendCollisionMessages
			{
				get
				{
					return ParticleSystem.CollisionModule.GetUsesCollisionMessages(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetUsesCollisionMessages(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000AA RID: 170
			// (get) Token: 0x0600022D RID: 557 RVA: 0x00004288 File Offset: 0x00002488
			// (set) Token: 0x0600022E RID: 558 RVA: 0x000042A8 File Offset: 0x000024A8
			public float colliderForce
			{
				get
				{
					return ParticleSystem.CollisionModule.GetColliderForce(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetColliderForce(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000AB RID: 171
			// (get) Token: 0x0600022F RID: 559 RVA: 0x000042B8 File Offset: 0x000024B8
			// (set) Token: 0x06000230 RID: 560 RVA: 0x000042D8 File Offset: 0x000024D8
			public bool multiplyColliderForceByCollisionAngle
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMultiplyColliderForceByCollisionAngle(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMultiplyColliderForceByCollisionAngle(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000AC RID: 172
			// (get) Token: 0x06000231 RID: 561 RVA: 0x000042E8 File Offset: 0x000024E8
			// (set) Token: 0x06000232 RID: 562 RVA: 0x00004308 File Offset: 0x00002508
			public bool multiplyColliderForceByParticleSpeed
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMultiplyColliderForceByParticleSpeed(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMultiplyColliderForceByParticleSpeed(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000AD RID: 173
			// (get) Token: 0x06000233 RID: 563 RVA: 0x00004318 File Offset: 0x00002518
			// (set) Token: 0x06000234 RID: 564 RVA: 0x00004338 File Offset: 0x00002538
			public bool multiplyColliderForceByParticleSize
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMultiplyColliderForceByParticleSize(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMultiplyColliderForceByParticleSize(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06000235 RID: 565 RVA: 0x00004347 File Offset: 0x00002547
			public void SetPlane(int index, Transform transform)
			{
				ParticleSystem.CollisionModule.SetPlane(this.m_ParticleSystem, index, transform);
			}

			// Token: 0x06000236 RID: 566 RVA: 0x00004358 File Offset: 0x00002558
			public Transform GetPlane(int index)
			{
				return ParticleSystem.CollisionModule.GetPlane(this.m_ParticleSystem, index);
			}

			// Token: 0x170000AE RID: 174
			// (get) Token: 0x06000237 RID: 567 RVA: 0x0000437C File Offset: 0x0000257C
			public int maxPlaneCount
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMaxPlaneCount(this.m_ParticleSystem);
				}
			}

			// Token: 0x06000238 RID: 568
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06000239 RID: 569
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x0600023A RID: 570
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetType(ParticleSystem system, ParticleSystemCollisionType value);

			// Token: 0x0600023B RID: 571
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemCollisionType GetType(ParticleSystem system);

			// Token: 0x0600023C RID: 572
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMode(ParticleSystem system, ParticleSystemCollisionMode value);

			// Token: 0x0600023D RID: 573
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemCollisionMode GetMode(ParticleSystem system);

			// Token: 0x0600023E RID: 574
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDampen(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600023F RID: 575
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetDampen(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000240 RID: 576
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDampenMultiplier(ParticleSystem system, float value);

			// Token: 0x06000241 RID: 577
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetDampenMultiplier(ParticleSystem system);

			// Token: 0x06000242 RID: 578
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetBounce(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000243 RID: 579
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetBounce(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000244 RID: 580
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetBounceMultiplier(ParticleSystem system, float value);

			// Token: 0x06000245 RID: 581
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetBounceMultiplier(ParticleSystem system);

			// Token: 0x06000246 RID: 582
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLifetimeLoss(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000247 RID: 583
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetLifetimeLoss(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000248 RID: 584
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLifetimeLossMultiplier(ParticleSystem system, float value);

			// Token: 0x06000249 RID: 585
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetLifetimeLossMultiplier(ParticleSystem system);

			// Token: 0x0600024A RID: 586
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMinKillSpeed(ParticleSystem system, float value);

			// Token: 0x0600024B RID: 587
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMinKillSpeed(ParticleSystem system);

			// Token: 0x0600024C RID: 588
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMaxKillSpeed(ParticleSystem system, float value);

			// Token: 0x0600024D RID: 589
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMaxKillSpeed(ParticleSystem system);

			// Token: 0x0600024E RID: 590
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCollidesWith(ParticleSystem system, int value);

			// Token: 0x0600024F RID: 591
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetCollidesWith(ParticleSystem system);

			// Token: 0x06000250 RID: 592
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnableDynamicColliders(ParticleSystem system, bool value);

			// Token: 0x06000251 RID: 593
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnableDynamicColliders(ParticleSystem system);

			// Token: 0x06000252 RID: 594
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnableInteriorCollisions(ParticleSystem system, bool value);

			// Token: 0x06000253 RID: 595
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnableInteriorCollisions(ParticleSystem system);

			// Token: 0x06000254 RID: 596
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMaxCollisionShapes(ParticleSystem system, int value);

			// Token: 0x06000255 RID: 597
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMaxCollisionShapes(ParticleSystem system);

			// Token: 0x06000256 RID: 598
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetQuality(ParticleSystem system, int value);

			// Token: 0x06000257 RID: 599
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetQuality(ParticleSystem system);

			// Token: 0x06000258 RID: 600
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetVoxelSize(ParticleSystem system, float value);

			// Token: 0x06000259 RID: 601
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetVoxelSize(ParticleSystem system);

			// Token: 0x0600025A RID: 602
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadiusScale(ParticleSystem system, float value);

			// Token: 0x0600025B RID: 603
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRadiusScale(ParticleSystem system);

			// Token: 0x0600025C RID: 604
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUsesCollisionMessages(ParticleSystem system, bool value);

			// Token: 0x0600025D RID: 605
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUsesCollisionMessages(ParticleSystem system);

			// Token: 0x0600025E RID: 606
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetColliderForce(ParticleSystem system, float value);

			// Token: 0x0600025F RID: 607
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetColliderForce(ParticleSystem system);

			// Token: 0x06000260 RID: 608
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMultiplyColliderForceByCollisionAngle(ParticleSystem system, bool value);

			// Token: 0x06000261 RID: 609
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetMultiplyColliderForceByCollisionAngle(ParticleSystem system);

			// Token: 0x06000262 RID: 610
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMultiplyColliderForceByParticleSpeed(ParticleSystem system, bool value);

			// Token: 0x06000263 RID: 611
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetMultiplyColliderForceByParticleSpeed(ParticleSystem system);

			// Token: 0x06000264 RID: 612
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMultiplyColliderForceByParticleSize(ParticleSystem system, bool value);

			// Token: 0x06000265 RID: 613
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetMultiplyColliderForceByParticleSize(ParticleSystem system);

			// Token: 0x06000266 RID: 614
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetPlane(ParticleSystem system, int index, Transform transform);

			// Token: 0x06000267 RID: 615
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Transform GetPlane(ParticleSystem system, int index);

			// Token: 0x06000268 RID: 616
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMaxPlaneCount(ParticleSystem system);

			// Token: 0x04000007 RID: 7
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000008 RID: 8
		public struct SubEmittersModule
		{
			// Token: 0x06000269 RID: 617 RVA: 0x0000439C File Offset: 0x0000259C
			internal SubEmittersModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x170000AF RID: 175
			// (get) Token: 0x0600026A RID: 618 RVA: 0x000043A8 File Offset: 0x000025A8
			// (set) Token: 0x0600026B RID: 619 RVA: 0x000043C9 File Offset: 0x000025C9
			[Obsolete("birth0 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.", false)]
			public ParticleSystem birth0
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetBirth(this.m_ParticleSystem, 0);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetBirth(this.m_ParticleSystem, 0, value);
				}
			}

			// Token: 0x170000B0 RID: 176
			// (get) Token: 0x0600026C RID: 620 RVA: 0x000043DC File Offset: 0x000025DC
			// (set) Token: 0x0600026D RID: 621 RVA: 0x000043FD File Offset: 0x000025FD
			[Obsolete("birth1 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.", false)]
			public ParticleSystem birth1
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetBirth(this.m_ParticleSystem, 1);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetBirth(this.m_ParticleSystem, 1, value);
				}
			}

			// Token: 0x170000B1 RID: 177
			// (get) Token: 0x0600026E RID: 622 RVA: 0x00004410 File Offset: 0x00002610
			// (set) Token: 0x0600026F RID: 623 RVA: 0x00004431 File Offset: 0x00002631
			[Obsolete("collision0 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.", false)]
			public ParticleSystem collision0
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetCollision(this.m_ParticleSystem, 0);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetCollision(this.m_ParticleSystem, 0, value);
				}
			}

			// Token: 0x170000B2 RID: 178
			// (get) Token: 0x06000270 RID: 624 RVA: 0x00004444 File Offset: 0x00002644
			// (set) Token: 0x06000271 RID: 625 RVA: 0x00004465 File Offset: 0x00002665
			[Obsolete("collision1 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.", false)]
			public ParticleSystem collision1
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetCollision(this.m_ParticleSystem, 1);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetCollision(this.m_ParticleSystem, 1, value);
				}
			}

			// Token: 0x170000B3 RID: 179
			// (get) Token: 0x06000272 RID: 626 RVA: 0x00004478 File Offset: 0x00002678
			// (set) Token: 0x06000273 RID: 627 RVA: 0x00004499 File Offset: 0x00002699
			[Obsolete("death0 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.", false)]
			public ParticleSystem death0
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetDeath(this.m_ParticleSystem, 0);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetDeath(this.m_ParticleSystem, 0, value);
				}
			}

			// Token: 0x170000B4 RID: 180
			// (get) Token: 0x06000274 RID: 628 RVA: 0x000044AC File Offset: 0x000026AC
			// (set) Token: 0x06000275 RID: 629 RVA: 0x000044CD File Offset: 0x000026CD
			[Obsolete("death1 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.", false)]
			public ParticleSystem death1
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetDeath(this.m_ParticleSystem, 1);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetDeath(this.m_ParticleSystem, 1, value);
				}
			}

			// Token: 0x170000B5 RID: 181
			// (get) Token: 0x06000277 RID: 631 RVA: 0x000044EC File Offset: 0x000026EC
			// (set) Token: 0x06000276 RID: 630 RVA: 0x000044DD File Offset: 0x000026DD
			public bool enabled
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000B6 RID: 182
			// (get) Token: 0x06000278 RID: 632 RVA: 0x0000450C File Offset: 0x0000270C
			public int subEmittersCount
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetSubEmittersCount(this.m_ParticleSystem);
				}
			}

			// Token: 0x06000279 RID: 633 RVA: 0x0000452C File Offset: 0x0000272C
			public void AddSubEmitter(ParticleSystem subEmitter, ParticleSystemSubEmitterType type, ParticleSystemSubEmitterProperties properties)
			{
				ParticleSystem.SubEmittersModule.AddSubEmitter(this.m_ParticleSystem, subEmitter, (int)type, (int)properties);
			}

			// Token: 0x0600027A RID: 634 RVA: 0x0000453D File Offset: 0x0000273D
			public void AddSubEmitter(ParticleSystem subEmitter, ParticleSystemSubEmitterType type, ParticleSystemSubEmitterProperties properties, float emitProbability)
			{
				ParticleSystem.SubEmittersModule.AddSubEmitter(this.m_ParticleSystem, subEmitter, (int)type, (int)properties, emitProbability);
			}

			// Token: 0x0600027B RID: 635 RVA: 0x00004550 File Offset: 0x00002750
			public void RemoveSubEmitter(int index)
			{
				ParticleSystem.SubEmittersModule.RemoveSubEmitter(this.m_ParticleSystem, index);
			}

			// Token: 0x0600027C RID: 636 RVA: 0x0000455F File Offset: 0x0000275F
			public void SetSubEmitterSystem(int index, ParticleSystem subEmitter)
			{
				ParticleSystem.SubEmittersModule.SetSubEmitterSystem(this.m_ParticleSystem, index, subEmitter);
			}

			// Token: 0x0600027D RID: 637 RVA: 0x0000456F File Offset: 0x0000276F
			public void SetSubEmitterType(int index, ParticleSystemSubEmitterType type)
			{
				ParticleSystem.SubEmittersModule.SetSubEmitterType(this.m_ParticleSystem, index, (int)type);
			}

			// Token: 0x0600027E RID: 638 RVA: 0x0000457F File Offset: 0x0000277F
			public void SetSubEmitterEmitProbability(int index, float emitProbability)
			{
				ParticleSystem.SubEmittersModule.SetSubEmitterEmitProbability(this.m_ParticleSystem, index, emitProbability);
			}

			// Token: 0x0600027F RID: 639 RVA: 0x0000458F File Offset: 0x0000278F
			public void SetSubEmitterProperties(int index, ParticleSystemSubEmitterProperties properties)
			{
				ParticleSystem.SubEmittersModule.SetSubEmitterProperties(this.m_ParticleSystem, index, (int)properties);
			}

			// Token: 0x06000280 RID: 640 RVA: 0x000045A0 File Offset: 0x000027A0
			public ParticleSystem GetSubEmitterSystem(int index)
			{
				return ParticleSystem.SubEmittersModule.GetSubEmitterSystem(this.m_ParticleSystem, index);
			}

			// Token: 0x06000281 RID: 641 RVA: 0x000045C4 File Offset: 0x000027C4
			public ParticleSystemSubEmitterType GetSubEmitterType(int index)
			{
				return (ParticleSystemSubEmitterType)ParticleSystem.SubEmittersModule.GetSubEmitterType(this.m_ParticleSystem, index);
			}

			// Token: 0x06000282 RID: 642 RVA: 0x000045E8 File Offset: 0x000027E8
			public float GetSubEmitterEmitProbability(int index)
			{
				return ParticleSystem.SubEmittersModule.GetSubEmitterEmitProbability(this.m_ParticleSystem, index);
			}

			// Token: 0x06000283 RID: 643 RVA: 0x0000460C File Offset: 0x0000280C
			public ParticleSystemSubEmitterProperties GetSubEmitterProperties(int index)
			{
				return (ParticleSystemSubEmitterProperties)ParticleSystem.SubEmittersModule.GetSubEmitterProperties(this.m_ParticleSystem, index);
			}

			// Token: 0x06000284 RID: 644
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06000285 RID: 645
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06000286 RID: 646
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetSubEmittersCount(ParticleSystem system);

			// Token: 0x06000287 RID: 647
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetBirth(ParticleSystem system, int index, ParticleSystem value);

			// Token: 0x06000288 RID: 648
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystem GetBirth(ParticleSystem system, int index);

			// Token: 0x06000289 RID: 649
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCollision(ParticleSystem system, int index, ParticleSystem value);

			// Token: 0x0600028A RID: 650
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystem GetCollision(ParticleSystem system, int index);

			// Token: 0x0600028B RID: 651
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDeath(ParticleSystem system, int index, ParticleSystem value);

			// Token: 0x0600028C RID: 652
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystem GetDeath(ParticleSystem system, int index);

			// Token: 0x0600028D RID: 653
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void AddSubEmitter(ParticleSystem system, ParticleSystem subEmitter, int type, int properties, [DefaultValue("1")] float emitProbability);

			// Token: 0x0600028E RID: 654 RVA: 0x00004630 File Offset: 0x00002830
			[ExcludeFromDocs]
			private static void AddSubEmitter(ParticleSystem system, ParticleSystem subEmitter, int type, int properties)
			{
				float emitProbability = 1f;
				ParticleSystem.SubEmittersModule.AddSubEmitter(system, subEmitter, type, properties, emitProbability);
			}

			// Token: 0x0600028F RID: 655
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void RemoveSubEmitter(ParticleSystem system, int index);

			// Token: 0x06000290 RID: 656
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSubEmitterSystem(ParticleSystem system, int index, ParticleSystem subEmitter);

			// Token: 0x06000291 RID: 657
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSubEmitterType(ParticleSystem system, int index, int type);

			// Token: 0x06000292 RID: 658
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSubEmitterEmitProbability(ParticleSystem system, int index, float emitProbability);

			// Token: 0x06000293 RID: 659
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSubEmitterProperties(ParticleSystem system, int index, int properties);

			// Token: 0x06000294 RID: 660
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystem GetSubEmitterSystem(ParticleSystem system, int index);

			// Token: 0x06000295 RID: 661
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetSubEmitterType(ParticleSystem system, int index);

			// Token: 0x06000296 RID: 662
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetSubEmitterEmitProbability(ParticleSystem system, int index);

			// Token: 0x06000297 RID: 663
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetSubEmitterProperties(ParticleSystem system, int index);

			// Token: 0x04000008 RID: 8
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000009 RID: 9
		public struct TextureSheetAnimationModule
		{
			// Token: 0x06000298 RID: 664 RVA: 0x0000464E File Offset: 0x0000284E
			internal TextureSheetAnimationModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x170000B7 RID: 183
			// (get) Token: 0x06000299 RID: 665 RVA: 0x00004658 File Offset: 0x00002858
			// (set) Token: 0x0600029A RID: 666 RVA: 0x00004688 File Offset: 0x00002888
			[Obsolete("flipU property is deprecated. Use ParticleSystemRenderer.flip.x instead.", false)]
			public float flipU
			{
				get
				{
					return this.m_ParticleSystem.GetComponent<ParticleSystemRenderer>().flip.x;
				}
				set
				{
					ParticleSystemRenderer component = this.m_ParticleSystem.GetComponent<ParticleSystemRenderer>();
					Vector3 flip = component.flip;
					flip.x = value;
					component.flip = flip;
				}
			}

			// Token: 0x170000B8 RID: 184
			// (get) Token: 0x0600029B RID: 667 RVA: 0x000046B8 File Offset: 0x000028B8
			// (set) Token: 0x0600029C RID: 668 RVA: 0x000046E8 File Offset: 0x000028E8
			[Obsolete("flipV property is deprecated. Use ParticleSystemRenderer.flip.y instead.", false)]
			public float flipV
			{
				get
				{
					return this.m_ParticleSystem.GetComponent<ParticleSystemRenderer>().flip.y;
				}
				set
				{
					ParticleSystemRenderer component = this.m_ParticleSystem.GetComponent<ParticleSystemRenderer>();
					Vector3 flip = component.flip;
					flip.y = value;
					component.flip = flip;
				}
			}

			// Token: 0x170000B9 RID: 185
			// (get) Token: 0x0600029E RID: 670 RVA: 0x00004728 File Offset: 0x00002928
			// (set) Token: 0x0600029D RID: 669 RVA: 0x00004718 File Offset: 0x00002918
			public bool enabled
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000BA RID: 186
			// (get) Token: 0x060002A0 RID: 672 RVA: 0x00004758 File Offset: 0x00002958
			// (set) Token: 0x0600029F RID: 671 RVA: 0x00004748 File Offset: 0x00002948
			public ParticleSystemAnimationMode mode
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000BB RID: 187
			// (get) Token: 0x060002A2 RID: 674 RVA: 0x00004788 File Offset: 0x00002988
			// (set) Token: 0x060002A1 RID: 673 RVA: 0x00004778 File Offset: 0x00002978
			public ParticleSystemAnimationTimeMode timeMode
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetTimeMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetTimeMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000BC RID: 188
			// (get) Token: 0x060002A3 RID: 675 RVA: 0x000047A8 File Offset: 0x000029A8
			// (set) Token: 0x060002A4 RID: 676 RVA: 0x000047C8 File Offset: 0x000029C8
			public float fps
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetFPS(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetFPS(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000BD RID: 189
			// (get) Token: 0x060002A6 RID: 678 RVA: 0x000047E8 File Offset: 0x000029E8
			// (set) Token: 0x060002A5 RID: 677 RVA: 0x000047D7 File Offset: 0x000029D7
			public int numTilesX
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetNumTilesX(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetNumTilesX(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000BE RID: 190
			// (get) Token: 0x060002A8 RID: 680 RVA: 0x00004818 File Offset: 0x00002A18
			// (set) Token: 0x060002A7 RID: 679 RVA: 0x00004808 File Offset: 0x00002A08
			public int numTilesY
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetNumTilesY(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetNumTilesY(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000BF RID: 191
			// (get) Token: 0x060002AA RID: 682 RVA: 0x00004848 File Offset: 0x00002A48
			// (set) Token: 0x060002A9 RID: 681 RVA: 0x00004838 File Offset: 0x00002A38
			public ParticleSystemAnimationType animation
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetAnimationType(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetAnimationType(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000C0 RID: 192
			// (get) Token: 0x060002AC RID: 684 RVA: 0x00004878 File Offset: 0x00002A78
			// (set) Token: 0x060002AB RID: 683 RVA: 0x00004868 File Offset: 0x00002A68
			public bool useRandomRow
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetUseRandomRow(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetUseRandomRow(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000C1 RID: 193
			// (get) Token: 0x060002AE RID: 686 RVA: 0x000048A8 File Offset: 0x00002AA8
			// (set) Token: 0x060002AD RID: 685 RVA: 0x00004898 File Offset: 0x00002A98
			public ParticleSystem.MinMaxCurve frameOverTime
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.TextureSheetAnimationModule.GetFrameOverTime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetFrameOverTime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170000C2 RID: 194
			// (get) Token: 0x060002AF RID: 687 RVA: 0x000048D4 File Offset: 0x00002AD4
			// (set) Token: 0x060002B0 RID: 688 RVA: 0x000048F4 File Offset: 0x00002AF4
			public float frameOverTimeMultiplier
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetFrameOverTimeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetFrameOverTimeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000C3 RID: 195
			// (get) Token: 0x060002B2 RID: 690 RVA: 0x00004914 File Offset: 0x00002B14
			// (set) Token: 0x060002B1 RID: 689 RVA: 0x00004903 File Offset: 0x00002B03
			public ParticleSystem.MinMaxCurve startFrame
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.TextureSheetAnimationModule.GetStartFrame(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetStartFrame(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170000C4 RID: 196
			// (get) Token: 0x060002B3 RID: 691 RVA: 0x00004940 File Offset: 0x00002B40
			// (set) Token: 0x060002B4 RID: 692 RVA: 0x00004960 File Offset: 0x00002B60
			public float startFrameMultiplier
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetStartFrameMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetStartFrameMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000C5 RID: 197
			// (get) Token: 0x060002B6 RID: 694 RVA: 0x00004980 File Offset: 0x00002B80
			// (set) Token: 0x060002B5 RID: 693 RVA: 0x0000496F File Offset: 0x00002B6F
			public int cycleCount
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetCycleCount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetCycleCount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000C6 RID: 198
			// (get) Token: 0x060002B8 RID: 696 RVA: 0x000049B0 File Offset: 0x00002BB0
			// (set) Token: 0x060002B7 RID: 695 RVA: 0x000049A0 File Offset: 0x00002BA0
			public int rowIndex
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetRowIndex(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetRowIndex(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170000C7 RID: 199
			// (get) Token: 0x060002BA RID: 698 RVA: 0x000049E0 File Offset: 0x00002BE0
			// (set) Token: 0x060002B9 RID: 697 RVA: 0x000049D0 File Offset: 0x00002BD0
			public UVChannelFlags uvChannelMask
			{
				get
				{
					return (UVChannelFlags)ParticleSystem.TextureSheetAnimationModule.GetUVChannelMask(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetUVChannelMask(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x170000C8 RID: 200
			// (get) Token: 0x060002BB RID: 699 RVA: 0x00004A00 File Offset: 0x00002C00
			public int spriteCount
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetSpriteCount(this.m_ParticleSystem);
				}
			}

			// Token: 0x170000C9 RID: 201
			// (get) Token: 0x060002BD RID: 701 RVA: 0x00004A30 File Offset: 0x00002C30
			// (set) Token: 0x060002BC RID: 700 RVA: 0x00004A20 File Offset: 0x00002C20
			public Vector2 speedRange
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetSpeedRange(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetSpeedRange(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060002BE RID: 702 RVA: 0x00004A50 File Offset: 0x00002C50
			public void AddSprite(Sprite sprite)
			{
				ParticleSystem.TextureSheetAnimationModule.AddSprite(this.m_ParticleSystem, sprite);
			}

			// Token: 0x060002BF RID: 703 RVA: 0x00004A5F File Offset: 0x00002C5F
			public void RemoveSprite(int index)
			{
				ParticleSystem.TextureSheetAnimationModule.RemoveSprite(this.m_ParticleSystem, index);
			}

			// Token: 0x060002C0 RID: 704 RVA: 0x00004A6E File Offset: 0x00002C6E
			public void SetSprite(int index, Sprite sprite)
			{
				ParticleSystem.TextureSheetAnimationModule.SetSprite(this.m_ParticleSystem, index, sprite);
			}

			// Token: 0x060002C1 RID: 705 RVA: 0x00004A80 File Offset: 0x00002C80
			public Sprite GetSprite(int index)
			{
				return ParticleSystem.TextureSheetAnimationModule.GetSprite(this.m_ParticleSystem, index);
			}

			// Token: 0x060002C2 RID: 706
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060002C3 RID: 707
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060002C4 RID: 708
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMode(ParticleSystem system, ParticleSystemAnimationMode value);

			// Token: 0x060002C5 RID: 709
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemAnimationMode GetMode(ParticleSystem system);

			// Token: 0x060002C6 RID: 710
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetTimeMode(ParticleSystem system, ParticleSystemAnimationTimeMode value);

			// Token: 0x060002C7 RID: 711
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemAnimationTimeMode GetTimeMode(ParticleSystem system);

			// Token: 0x060002C8 RID: 712
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetFPS(ParticleSystem system, float value);

			// Token: 0x060002C9 RID: 713
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetFPS(ParticleSystem system);

			// Token: 0x060002CA RID: 714
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetNumTilesX(ParticleSystem system, int value);

			// Token: 0x060002CB RID: 715
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetNumTilesX(ParticleSystem system);

			// Token: 0x060002CC RID: 716
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetNumTilesY(ParticleSystem system, int value);

			// Token: 0x060002CD RID: 717
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetNumTilesY(ParticleSystem system);

			// Token: 0x060002CE RID: 718
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetAnimationType(ParticleSystem system, ParticleSystemAnimationType value);

			// Token: 0x060002CF RID: 719
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemAnimationType GetAnimationType(ParticleSystem system);

			// Token: 0x060002D0 RID: 720
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseRandomRow(ParticleSystem system, bool value);

			// Token: 0x060002D1 RID: 721
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseRandomRow(ParticleSystem system);

			// Token: 0x060002D2 RID: 722
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetFrameOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060002D3 RID: 723
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetFrameOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060002D4 RID: 724
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetFrameOverTimeMultiplier(ParticleSystem system, float value);

			// Token: 0x060002D5 RID: 725
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetFrameOverTimeMultiplier(ParticleSystem system);

			// Token: 0x060002D6 RID: 726
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartFrame(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060002D7 RID: 727
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartFrame(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060002D8 RID: 728
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartFrameMultiplier(ParticleSystem system, float value);

			// Token: 0x060002D9 RID: 729
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartFrameMultiplier(ParticleSystem system);

			// Token: 0x060002DA RID: 730
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCycleCount(ParticleSystem system, int value);

			// Token: 0x060002DB RID: 731
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetCycleCount(ParticleSystem system);

			// Token: 0x060002DC RID: 732
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRowIndex(ParticleSystem system, int value);

			// Token: 0x060002DD RID: 733
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetRowIndex(ParticleSystem system);

			// Token: 0x060002DE RID: 734
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUVChannelMask(ParticleSystem system, int value);

			// Token: 0x060002DF RID: 735
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetUVChannelMask(ParticleSystem system);

			// Token: 0x060002E0 RID: 736
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetSpriteCount(ParticleSystem system);

			// Token: 0x060002E1 RID: 737 RVA: 0x00004AA1 File Offset: 0x00002CA1
			private static void SetSpeedRange(ParticleSystem system, Vector2 value)
			{
				ParticleSystem.TextureSheetAnimationModule.INTERNAL_CALL_SetSpeedRange(system, ref value);
			}

			// Token: 0x060002E2 RID: 738
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetSpeedRange(ParticleSystem system, ref Vector2 value);

			// Token: 0x060002E3 RID: 739 RVA: 0x00004AAC File Offset: 0x00002CAC
			private static Vector2 GetSpeedRange(ParticleSystem system)
			{
				Vector2 result;
				ParticleSystem.TextureSheetAnimationModule.INTERNAL_CALL_GetSpeedRange(system, out result);
				return result;
			}

			// Token: 0x060002E4 RID: 740
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetSpeedRange(ParticleSystem system, out Vector2 value);

			// Token: 0x060002E5 RID: 741
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void AddSprite(ParticleSystem system, Object sprite);

			// Token: 0x060002E6 RID: 742
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void RemoveSprite(ParticleSystem system, int index);

			// Token: 0x060002E7 RID: 743
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSprite(ParticleSystem system, int index, Object sprite);

			// Token: 0x060002E8 RID: 744
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Sprite GetSprite(ParticleSystem system, int index);

			// Token: 0x04000009 RID: 9
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200000A RID: 10
		[RequiredByNativeCode("particleSystemParticle", Optional = true)]
		public struct Particle
		{
			// Token: 0x170000CA RID: 202
			// (get) Token: 0x060002E9 RID: 745 RVA: 0x00004ACC File Offset: 0x00002CCC
			// (set) Token: 0x060002EA RID: 746 RVA: 0x00004AE7 File Offset: 0x00002CE7
			[Obsolete("Please use Particle.remainingLifetime instead. (UnityUpgradable) -> UnityEngine.ParticleSystem/Particle.remainingLifetime", false)]
			public float lifetime
			{
				get
				{
					return this.remainingLifetime;
				}
				set
				{
					this.remainingLifetime = value;
				}
			}

			// Token: 0x170000CB RID: 203
			// (get) Token: 0x060002EB RID: 747 RVA: 0x00004AF4 File Offset: 0x00002CF4
			// (set) Token: 0x060002EC RID: 748 RVA: 0x00004B1A File Offset: 0x00002D1A
			[Obsolete("randomValue property is deprecated. Use randomSeed instead to control random behavior of particles.", false)]
			public float randomValue
			{
				get
				{
					return BitConverter.ToSingle(BitConverter.GetBytes(this.m_RandomSeed), 0);
				}
				set
				{
					this.m_RandomSeed = BitConverter.ToUInt32(BitConverter.GetBytes(value), 0);
				}
			}

			// Token: 0x170000CC RID: 204
			// (get) Token: 0x060002ED RID: 749 RVA: 0x00004B30 File Offset: 0x00002D30
			// (set) Token: 0x060002EE RID: 750 RVA: 0x00004B4B File Offset: 0x00002D4B
			[Obsolete("size property is deprecated. Use startSize or GetCurrentSize() instead.", false)]
			public float size
			{
				get
				{
					return this.startSize;
				}
				set
				{
					this.startSize = value;
				}
			}

			// Token: 0x170000CD RID: 205
			// (get) Token: 0x060002EF RID: 751 RVA: 0x00004B58 File Offset: 0x00002D58
			// (set) Token: 0x060002F0 RID: 752 RVA: 0x00004B73 File Offset: 0x00002D73
			[Obsolete("color property is deprecated. Use startColor or GetCurrentColor() instead.", false)]
			public Color32 color
			{
				get
				{
					return this.startColor;
				}
				set
				{
					this.startColor = value;
				}
			}

			// Token: 0x170000CE RID: 206
			// (get) Token: 0x060002F1 RID: 753 RVA: 0x00004B80 File Offset: 0x00002D80
			// (set) Token: 0x060002F2 RID: 754 RVA: 0x00004B9B File Offset: 0x00002D9B
			public Vector3 position
			{
				get
				{
					return this.m_Position;
				}
				set
				{
					this.m_Position = value;
				}
			}

			// Token: 0x170000CF RID: 207
			// (get) Token: 0x060002F3 RID: 755 RVA: 0x00004BA8 File Offset: 0x00002DA8
			// (set) Token: 0x060002F4 RID: 756 RVA: 0x00004BC3 File Offset: 0x00002DC3
			public Vector3 velocity
			{
				get
				{
					return this.m_Velocity;
				}
				set
				{
					this.m_Velocity = value;
				}
			}

			// Token: 0x170000D0 RID: 208
			// (get) Token: 0x060002F5 RID: 757 RVA: 0x00004BD0 File Offset: 0x00002DD0
			public Vector3 animatedVelocity
			{
				get
				{
					return this.m_AnimatedVelocity;
				}
			}

			// Token: 0x170000D1 RID: 209
			// (get) Token: 0x060002F6 RID: 758 RVA: 0x00004BEC File Offset: 0x00002DEC
			public Vector3 totalVelocity
			{
				get
				{
					return this.m_Velocity + this.m_AnimatedVelocity;
				}
			}

			// Token: 0x170000D2 RID: 210
			// (get) Token: 0x060002F7 RID: 759 RVA: 0x00004C14 File Offset: 0x00002E14
			// (set) Token: 0x060002F8 RID: 760 RVA: 0x00004C2F File Offset: 0x00002E2F
			public float remainingLifetime
			{
				get
				{
					return this.m_Lifetime;
				}
				set
				{
					this.m_Lifetime = value;
				}
			}

			// Token: 0x170000D3 RID: 211
			// (get) Token: 0x060002F9 RID: 761 RVA: 0x00004C3C File Offset: 0x00002E3C
			// (set) Token: 0x060002FA RID: 762 RVA: 0x00004C57 File Offset: 0x00002E57
			public float startLifetime
			{
				get
				{
					return this.m_StartLifetime;
				}
				set
				{
					this.m_StartLifetime = value;
				}
			}

			// Token: 0x170000D4 RID: 212
			// (get) Token: 0x060002FB RID: 763 RVA: 0x00004C64 File Offset: 0x00002E64
			// (set) Token: 0x060002FC RID: 764 RVA: 0x00004C7F File Offset: 0x00002E7F
			public Color32 startColor
			{
				get
				{
					return this.m_StartColor;
				}
				set
				{
					this.m_StartColor = value;
				}
			}

			// Token: 0x170000D5 RID: 213
			// (get) Token: 0x060002FD RID: 765 RVA: 0x00004C8C File Offset: 0x00002E8C
			// (set) Token: 0x060002FE RID: 766 RVA: 0x00004CA7 File Offset: 0x00002EA7
			public uint randomSeed
			{
				get
				{
					return this.m_RandomSeed;
				}
				set
				{
					this.m_RandomSeed = value;
				}
			}

			// Token: 0x170000D6 RID: 214
			// (get) Token: 0x060002FF RID: 767 RVA: 0x00004CB4 File Offset: 0x00002EB4
			// (set) Token: 0x06000300 RID: 768 RVA: 0x00004CCF File Offset: 0x00002ECF
			public Vector3 axisOfRotation
			{
				get
				{
					return this.m_AxisOfRotation;
				}
				set
				{
					this.m_AxisOfRotation = value;
				}
			}

			// Token: 0x170000D7 RID: 215
			// (get) Token: 0x06000301 RID: 769 RVA: 0x00004CDC File Offset: 0x00002EDC
			// (set) Token: 0x06000302 RID: 770 RVA: 0x00004CFC File Offset: 0x00002EFC
			public float startSize
			{
				get
				{
					return this.m_StartSize.x;
				}
				set
				{
					this.m_StartSize = new Vector3(value, value, value);
				}
			}

			// Token: 0x170000D8 RID: 216
			// (get) Token: 0x06000303 RID: 771 RVA: 0x00004D10 File Offset: 0x00002F10
			// (set) Token: 0x06000304 RID: 772 RVA: 0x00004D2B File Offset: 0x00002F2B
			public Vector3 startSize3D
			{
				get
				{
					return this.m_StartSize;
				}
				set
				{
					this.m_StartSize = value;
					this.m_Flags |= 1U;
				}
			}

			// Token: 0x170000D9 RID: 217
			// (get) Token: 0x06000305 RID: 773 RVA: 0x00004D44 File Offset: 0x00002F44
			// (set) Token: 0x06000306 RID: 774 RVA: 0x00004D6A File Offset: 0x00002F6A
			public float rotation
			{
				get
				{
					return this.m_Rotation.z * 57.29578f;
				}
				set
				{
					this.m_Rotation = new Vector3(0f, 0f, value * 0.017453292f);
				}
			}

			// Token: 0x170000DA RID: 218
			// (get) Token: 0x06000307 RID: 775 RVA: 0x00004D8C File Offset: 0x00002F8C
			// (set) Token: 0x06000308 RID: 776 RVA: 0x00004DB1 File Offset: 0x00002FB1
			public Vector3 rotation3D
			{
				get
				{
					return this.m_Rotation * 57.29578f;
				}
				set
				{
					this.m_Rotation = value * 0.017453292f;
					this.m_Flags |= 2U;
				}
			}

			// Token: 0x170000DB RID: 219
			// (get) Token: 0x06000309 RID: 777 RVA: 0x00004DD4 File Offset: 0x00002FD4
			// (set) Token: 0x0600030A RID: 778 RVA: 0x00004DFA File Offset: 0x00002FFA
			public float angularVelocity
			{
				get
				{
					return this.m_AngularVelocity.z * 57.29578f;
				}
				set
				{
					this.m_AngularVelocity = new Vector3(0f, 0f, value * 0.017453292f);
				}
			}

			// Token: 0x170000DC RID: 220
			// (get) Token: 0x0600030B RID: 779 RVA: 0x00004E1C File Offset: 0x0000301C
			// (set) Token: 0x0600030C RID: 780 RVA: 0x00004E41 File Offset: 0x00003041
			public Vector3 angularVelocity3D
			{
				get
				{
					return this.m_AngularVelocity * 57.29578f;
				}
				set
				{
					this.m_AngularVelocity = value * 0.017453292f;
					this.m_Flags |= 2U;
				}
			}

			// Token: 0x0600030D RID: 781 RVA: 0x00004E64 File Offset: 0x00003064
			public float GetCurrentSize(ParticleSystem system)
			{
				return system.GetParticleCurrentSize(ref this);
			}

			// Token: 0x0600030E RID: 782 RVA: 0x00004E80 File Offset: 0x00003080
			public Vector3 GetCurrentSize3D(ParticleSystem system)
			{
				return system.GetParticleCurrentSize3D(ref this);
			}

			// Token: 0x0600030F RID: 783 RVA: 0x00004E9C File Offset: 0x0000309C
			public Color32 GetCurrentColor(ParticleSystem system)
			{
				return system.GetParticleCurrentColor(ref this);
			}

			// Token: 0x0400000A RID: 10
			private Vector3 m_Position;

			// Token: 0x0400000B RID: 11
			private Vector3 m_Velocity;

			// Token: 0x0400000C RID: 12
			private Vector3 m_AnimatedVelocity;

			// Token: 0x0400000D RID: 13
			private Vector3 m_InitialVelocity;

			// Token: 0x0400000E RID: 14
			private Vector3 m_AxisOfRotation;

			// Token: 0x0400000F RID: 15
			private Vector3 m_Rotation;

			// Token: 0x04000010 RID: 16
			private Vector3 m_AngularVelocity;

			// Token: 0x04000011 RID: 17
			private Vector3 m_StartSize;

			// Token: 0x04000012 RID: 18
			private Color32 m_StartColor;

			// Token: 0x04000013 RID: 19
			private uint m_RandomSeed;

			// Token: 0x04000014 RID: 20
			private float m_Lifetime;

			// Token: 0x04000015 RID: 21
			private float m_StartLifetime;

			// Token: 0x04000016 RID: 22
			private float m_EmitAccumulator0;

			// Token: 0x04000017 RID: 23
			private float m_EmitAccumulator1;

			// Token: 0x04000018 RID: 24
			private uint m_Flags;

			// Token: 0x0200000B RID: 11
			[Flags]
			private enum Flags
			{
				// Token: 0x0400001A RID: 26
				Size3D = 1,
				// Token: 0x0400001B RID: 27
				Rotation3D = 2
			}
		}

		// Token: 0x0200000C RID: 12
		public struct Burst
		{
			// Token: 0x06000310 RID: 784 RVA: 0x00004EB8 File Offset: 0x000030B8
			public Burst(float _time, short _count)
			{
				this.m_Time = _time;
				this.m_Count = (float)_count;
				this.m_RepeatCount = 0;
				this.m_RepeatInterval = 0f;
				this.m_InvProbability = 0f;
			}

			// Token: 0x06000311 RID: 785 RVA: 0x00004EEC File Offset: 0x000030EC
			public Burst(float _time, short _minCount, short _maxCount)
			{
				this.m_Time = _time;
				this.m_Count = new ParticleSystem.MinMaxCurve((float)_minCount, (float)_maxCount);
				this.m_RepeatCount = 0;
				this.m_RepeatInterval = 0f;
				this.m_InvProbability = 0f;
			}

			// Token: 0x06000312 RID: 786 RVA: 0x00004F22 File Offset: 0x00003122
			public Burst(float _time, short _minCount, short _maxCount, int _cycleCount, float _repeatInterval)
			{
				this.m_Time = _time;
				this.m_Count = new ParticleSystem.MinMaxCurve((float)_minCount, (float)_maxCount);
				this.m_RepeatCount = _cycleCount - 1;
				this.m_RepeatInterval = _repeatInterval;
				this.m_InvProbability = 0f;
			}

			// Token: 0x06000313 RID: 787 RVA: 0x00004F58 File Offset: 0x00003158
			public Burst(float _time, ParticleSystem.MinMaxCurve _count)
			{
				this.m_Time = _time;
				this.m_Count = _count;
				this.m_RepeatCount = 0;
				this.m_RepeatInterval = 0f;
				this.m_InvProbability = 0f;
			}

			// Token: 0x06000314 RID: 788 RVA: 0x00004F86 File Offset: 0x00003186
			public Burst(float _time, ParticleSystem.MinMaxCurve _count, int _cycleCount, float _repeatInterval)
			{
				this.m_Time = _time;
				this.m_Count = _count;
				this.m_RepeatCount = _cycleCount - 1;
				this.m_RepeatInterval = _repeatInterval;
				this.m_InvProbability = 0f;
			}

			// Token: 0x170000DD RID: 221
			// (get) Token: 0x06000315 RID: 789 RVA: 0x00004FB4 File Offset: 0x000031B4
			// (set) Token: 0x06000316 RID: 790 RVA: 0x00004FCF File Offset: 0x000031CF
			public float time
			{
				get
				{
					return this.m_Time;
				}
				set
				{
					this.m_Time = value;
				}
			}

			// Token: 0x170000DE RID: 222
			// (get) Token: 0x06000317 RID: 791 RVA: 0x00004FDC File Offset: 0x000031DC
			// (set) Token: 0x06000318 RID: 792 RVA: 0x00004FF7 File Offset: 0x000031F7
			public ParticleSystem.MinMaxCurve count
			{
				get
				{
					return this.m_Count;
				}
				set
				{
					this.m_Count = value;
				}
			}

			// Token: 0x170000DF RID: 223
			// (get) Token: 0x06000319 RID: 793 RVA: 0x00005004 File Offset: 0x00003204
			// (set) Token: 0x0600031A RID: 794 RVA: 0x00005025 File Offset: 0x00003225
			public short minCount
			{
				get
				{
					return (short)this.m_Count.constantMin;
				}
				set
				{
					this.m_Count.constantMin = (float)value;
				}
			}

			// Token: 0x170000E0 RID: 224
			// (get) Token: 0x0600031B RID: 795 RVA: 0x00005038 File Offset: 0x00003238
			// (set) Token: 0x0600031C RID: 796 RVA: 0x00005059 File Offset: 0x00003259
			public short maxCount
			{
				get
				{
					return (short)this.m_Count.constantMax;
				}
				set
				{
					this.m_Count.constantMax = (float)value;
				}
			}

			// Token: 0x170000E1 RID: 225
			// (get) Token: 0x0600031D RID: 797 RVA: 0x0000506C File Offset: 0x0000326C
			// (set) Token: 0x0600031E RID: 798 RVA: 0x00005089 File Offset: 0x00003289
			public int cycleCount
			{
				get
				{
					return this.m_RepeatCount + 1;
				}
				set
				{
					if (value < 0)
					{
						throw new ArgumentOutOfRangeException("cycleCount", "cycleCount must be at least 0: " + value);
					}
					this.m_RepeatCount = value - 1;
				}
			}

			// Token: 0x170000E2 RID: 226
			// (get) Token: 0x0600031F RID: 799 RVA: 0x000050B8 File Offset: 0x000032B8
			// (set) Token: 0x06000320 RID: 800 RVA: 0x000050D3 File Offset: 0x000032D3
			public float repeatInterval
			{
				get
				{
					return this.m_RepeatInterval;
				}
				set
				{
					if (value <= 0f)
					{
						throw new ArgumentOutOfRangeException("repeatInterval", "repeatInterval must be greater than 0.0f: " + value);
					}
					this.m_RepeatInterval = value;
				}
			}

			// Token: 0x170000E3 RID: 227
			// (get) Token: 0x06000321 RID: 801 RVA: 0x00005104 File Offset: 0x00003304
			// (set) Token: 0x06000322 RID: 802 RVA: 0x00005128 File Offset: 0x00003328
			public float probability
			{
				get
				{
					return 1f - this.m_InvProbability;
				}
				set
				{
					if (value < 0f || value > 1f)
					{
						throw new ArgumentOutOfRangeException("probability", "probability must be between 0.0f and 1.0f: " + value);
					}
					this.m_InvProbability = 1f - value;
				}
			}

			// Token: 0x0400001C RID: 28
			private float m_Time;

			// Token: 0x0400001D RID: 29
			private ParticleSystem.MinMaxCurve m_Count;

			// Token: 0x0400001E RID: 30
			private int m_RepeatCount;

			// Token: 0x0400001F RID: 31
			private float m_RepeatInterval;

			// Token: 0x04000020 RID: 32
			private float m_InvProbability;
		}

		// Token: 0x0200000D RID: 13
		[NativeType(CodegenOptions.Custom, "MonoMinMaxCurve", Header = "Runtime/Scripting/ScriptingCommonStructDefinitions.h")]
		[Serializable]
		public struct MinMaxCurve
		{
			// Token: 0x06000323 RID: 803 RVA: 0x00005174 File Offset: 0x00003374
			public MinMaxCurve(float constant)
			{
				this.m_Mode = ParticleSystemCurveMode.Constant;
				this.m_CurveMultiplier = 0f;
				this.m_CurveMin = null;
				this.m_CurveMax = null;
				this.m_ConstantMin = 0f;
				this.m_ConstantMax = constant;
			}

			// Token: 0x06000324 RID: 804 RVA: 0x000051A9 File Offset: 0x000033A9
			public MinMaxCurve(float multiplier, AnimationCurve curve)
			{
				this.m_Mode = ParticleSystemCurveMode.Curve;
				this.m_CurveMultiplier = multiplier;
				this.m_CurveMin = null;
				this.m_CurveMax = curve;
				this.m_ConstantMin = 0f;
				this.m_ConstantMax = 0f;
			}

			// Token: 0x06000325 RID: 805 RVA: 0x000051DE File Offset: 0x000033DE
			public MinMaxCurve(float multiplier, AnimationCurve min, AnimationCurve max)
			{
				this.m_Mode = ParticleSystemCurveMode.TwoCurves;
				this.m_CurveMultiplier = multiplier;
				this.m_CurveMin = min;
				this.m_CurveMax = max;
				this.m_ConstantMin = 0f;
				this.m_ConstantMax = 0f;
			}

			// Token: 0x06000326 RID: 806 RVA: 0x00005213 File Offset: 0x00003413
			public MinMaxCurve(float min, float max)
			{
				this.m_Mode = ParticleSystemCurveMode.TwoConstants;
				this.m_CurveMultiplier = 0f;
				this.m_CurveMin = null;
				this.m_CurveMax = null;
				this.m_ConstantMin = min;
				this.m_ConstantMax = max;
			}

			// Token: 0x170000E4 RID: 228
			// (get) Token: 0x06000327 RID: 807 RVA: 0x00005244 File Offset: 0x00003444
			// (set) Token: 0x06000328 RID: 808 RVA: 0x0000525F File Offset: 0x0000345F
			public ParticleSystemCurveMode mode
			{
				get
				{
					return this.m_Mode;
				}
				set
				{
					this.m_Mode = value;
				}
			}

			// Token: 0x170000E5 RID: 229
			// (get) Token: 0x06000329 RID: 809 RVA: 0x0000526C File Offset: 0x0000346C
			// (set) Token: 0x0600032A RID: 810 RVA: 0x00005287 File Offset: 0x00003487
			public float curveMultiplier
			{
				get
				{
					return this.m_CurveMultiplier;
				}
				set
				{
					this.m_CurveMultiplier = value;
				}
			}

			// Token: 0x170000E6 RID: 230
			// (get) Token: 0x0600032B RID: 811 RVA: 0x00005294 File Offset: 0x00003494
			// (set) Token: 0x0600032C RID: 812 RVA: 0x000052AF File Offset: 0x000034AF
			public AnimationCurve curveMax
			{
				get
				{
					return this.m_CurveMax;
				}
				set
				{
					this.m_CurveMax = value;
				}
			}

			// Token: 0x170000E7 RID: 231
			// (get) Token: 0x0600032D RID: 813 RVA: 0x000052BC File Offset: 0x000034BC
			// (set) Token: 0x0600032E RID: 814 RVA: 0x000052D7 File Offset: 0x000034D7
			public AnimationCurve curveMin
			{
				get
				{
					return this.m_CurveMin;
				}
				set
				{
					this.m_CurveMin = value;
				}
			}

			// Token: 0x170000E8 RID: 232
			// (get) Token: 0x0600032F RID: 815 RVA: 0x000052E4 File Offset: 0x000034E4
			// (set) Token: 0x06000330 RID: 816 RVA: 0x000052FF File Offset: 0x000034FF
			public float constantMax
			{
				get
				{
					return this.m_ConstantMax;
				}
				set
				{
					this.m_ConstantMax = value;
				}
			}

			// Token: 0x170000E9 RID: 233
			// (get) Token: 0x06000331 RID: 817 RVA: 0x0000530C File Offset: 0x0000350C
			// (set) Token: 0x06000332 RID: 818 RVA: 0x00005327 File Offset: 0x00003527
			public float constantMin
			{
				get
				{
					return this.m_ConstantMin;
				}
				set
				{
					this.m_ConstantMin = value;
				}
			}

			// Token: 0x170000EA RID: 234
			// (get) Token: 0x06000333 RID: 819 RVA: 0x00005334 File Offset: 0x00003534
			// (set) Token: 0x06000334 RID: 820 RVA: 0x000052FF File Offset: 0x000034FF
			public float constant
			{
				get
				{
					return this.m_ConstantMax;
				}
				set
				{
					this.m_ConstantMax = value;
				}
			}

			// Token: 0x170000EB RID: 235
			// (get) Token: 0x06000335 RID: 821 RVA: 0x00005350 File Offset: 0x00003550
			// (set) Token: 0x06000336 RID: 822 RVA: 0x000052AF File Offset: 0x000034AF
			public AnimationCurve curve
			{
				get
				{
					return this.m_CurveMax;
				}
				set
				{
					this.m_CurveMax = value;
				}
			}

			// Token: 0x06000337 RID: 823 RVA: 0x0000536C File Offset: 0x0000356C
			public float Evaluate(float time)
			{
				return this.Evaluate(time, 1f);
			}

			// Token: 0x06000338 RID: 824 RVA: 0x00005390 File Offset: 0x00003590
			public float Evaluate(float time, float lerpFactor)
			{
				switch (this.mode)
				{
				case ParticleSystemCurveMode.Constant:
					return this.m_ConstantMax;
				case ParticleSystemCurveMode.TwoCurves:
					return Mathf.Lerp(this.m_CurveMin.Evaluate(time), this.m_CurveMax.Evaluate(time), lerpFactor) * this.m_CurveMultiplier;
				case ParticleSystemCurveMode.TwoConstants:
					return Mathf.Lerp(this.m_ConstantMin, this.m_ConstantMax, lerpFactor);
				}
				return this.m_CurveMax.Evaluate(time) * this.m_CurveMultiplier;
			}

			// Token: 0x06000339 RID: 825 RVA: 0x0000542C File Offset: 0x0000362C
			public static implicit operator ParticleSystem.MinMaxCurve(float constant)
			{
				return new ParticleSystem.MinMaxCurve(constant);
			}

			// Token: 0x04000021 RID: 33
			[SerializeField]
			private ParticleSystemCurveMode m_Mode;

			// Token: 0x04000022 RID: 34
			[SerializeField]
			private float m_CurveMultiplier;

			// Token: 0x04000023 RID: 35
			[SerializeField]
			private AnimationCurve m_CurveMin;

			// Token: 0x04000024 RID: 36
			[SerializeField]
			private AnimationCurve m_CurveMax;

			// Token: 0x04000025 RID: 37
			[SerializeField]
			private float m_ConstantMin;

			// Token: 0x04000026 RID: 38
			[SerializeField]
			private float m_ConstantMax;
		}

		// Token: 0x0200000E RID: 14
		[Serializable]
		public struct MinMaxGradient
		{
			// Token: 0x0600033A RID: 826 RVA: 0x00005447 File Offset: 0x00003647
			public MinMaxGradient(Color color)
			{
				this.m_Mode = ParticleSystemGradientMode.Color;
				this.m_GradientMin = null;
				this.m_GradientMax = null;
				this.m_ColorMin = Color.black;
				this.m_ColorMax = color;
			}

			// Token: 0x0600033B RID: 827 RVA: 0x00005471 File Offset: 0x00003671
			public MinMaxGradient(Gradient gradient)
			{
				this.m_Mode = ParticleSystemGradientMode.Gradient;
				this.m_GradientMin = null;
				this.m_GradientMax = gradient;
				this.m_ColorMin = Color.black;
				this.m_ColorMax = Color.black;
			}

			// Token: 0x0600033C RID: 828 RVA: 0x0000549F File Offset: 0x0000369F
			public MinMaxGradient(Color min, Color max)
			{
				this.m_Mode = ParticleSystemGradientMode.TwoColors;
				this.m_GradientMin = null;
				this.m_GradientMax = null;
				this.m_ColorMin = min;
				this.m_ColorMax = max;
			}

			// Token: 0x0600033D RID: 829 RVA: 0x000054C5 File Offset: 0x000036C5
			public MinMaxGradient(Gradient min, Gradient max)
			{
				this.m_Mode = ParticleSystemGradientMode.TwoGradients;
				this.m_GradientMin = min;
				this.m_GradientMax = max;
				this.m_ColorMin = Color.black;
				this.m_ColorMax = Color.black;
			}

			// Token: 0x170000EC RID: 236
			// (get) Token: 0x0600033E RID: 830 RVA: 0x000054F4 File Offset: 0x000036F4
			// (set) Token: 0x0600033F RID: 831 RVA: 0x0000550F File Offset: 0x0000370F
			public ParticleSystemGradientMode mode
			{
				get
				{
					return this.m_Mode;
				}
				set
				{
					this.m_Mode = value;
				}
			}

			// Token: 0x170000ED RID: 237
			// (get) Token: 0x06000340 RID: 832 RVA: 0x0000551C File Offset: 0x0000371C
			// (set) Token: 0x06000341 RID: 833 RVA: 0x00005537 File Offset: 0x00003737
			public Gradient gradientMax
			{
				get
				{
					return this.m_GradientMax;
				}
				set
				{
					this.m_GradientMax = value;
				}
			}

			// Token: 0x170000EE RID: 238
			// (get) Token: 0x06000342 RID: 834 RVA: 0x00005544 File Offset: 0x00003744
			// (set) Token: 0x06000343 RID: 835 RVA: 0x0000555F File Offset: 0x0000375F
			public Gradient gradientMin
			{
				get
				{
					return this.m_GradientMin;
				}
				set
				{
					this.m_GradientMin = value;
				}
			}

			// Token: 0x170000EF RID: 239
			// (get) Token: 0x06000344 RID: 836 RVA: 0x0000556C File Offset: 0x0000376C
			// (set) Token: 0x06000345 RID: 837 RVA: 0x00005587 File Offset: 0x00003787
			public Color colorMax
			{
				get
				{
					return this.m_ColorMax;
				}
				set
				{
					this.m_ColorMax = value;
				}
			}

			// Token: 0x170000F0 RID: 240
			// (get) Token: 0x06000346 RID: 838 RVA: 0x00005594 File Offset: 0x00003794
			// (set) Token: 0x06000347 RID: 839 RVA: 0x000055AF File Offset: 0x000037AF
			public Color colorMin
			{
				get
				{
					return this.m_ColorMin;
				}
				set
				{
					this.m_ColorMin = value;
				}
			}

			// Token: 0x170000F1 RID: 241
			// (get) Token: 0x06000348 RID: 840 RVA: 0x000055BC File Offset: 0x000037BC
			// (set) Token: 0x06000349 RID: 841 RVA: 0x00005587 File Offset: 0x00003787
			public Color color
			{
				get
				{
					return this.m_ColorMax;
				}
				set
				{
					this.m_ColorMax = value;
				}
			}

			// Token: 0x170000F2 RID: 242
			// (get) Token: 0x0600034A RID: 842 RVA: 0x000055D8 File Offset: 0x000037D8
			// (set) Token: 0x0600034B RID: 843 RVA: 0x00005537 File Offset: 0x00003737
			public Gradient gradient
			{
				get
				{
					return this.m_GradientMax;
				}
				set
				{
					this.m_GradientMax = value;
				}
			}

			// Token: 0x0600034C RID: 844 RVA: 0x000055F4 File Offset: 0x000037F4
			public Color Evaluate(float time)
			{
				return this.Evaluate(time, 1f);
			}

			// Token: 0x0600034D RID: 845 RVA: 0x00005618 File Offset: 0x00003818
			public Color Evaluate(float time, float lerpFactor)
			{
				switch (this.m_Mode)
				{
				case ParticleSystemGradientMode.Color:
					return this.m_ColorMax;
				case ParticleSystemGradientMode.TwoColors:
					return Color.Lerp(this.m_ColorMin, this.m_ColorMax, lerpFactor);
				case ParticleSystemGradientMode.TwoGradients:
					return Color.Lerp(this.m_GradientMin.Evaluate(time), this.m_GradientMax.Evaluate(time), lerpFactor);
				case ParticleSystemGradientMode.RandomColor:
					return this.m_GradientMax.Evaluate(lerpFactor);
				}
				return this.m_GradientMax.Evaluate(time);
			}

			// Token: 0x0600034E RID: 846 RVA: 0x000056BC File Offset: 0x000038BC
			public static implicit operator ParticleSystem.MinMaxGradient(Color color)
			{
				return new ParticleSystem.MinMaxGradient(color);
			}

			// Token: 0x0600034F RID: 847 RVA: 0x000056D8 File Offset: 0x000038D8
			public static implicit operator ParticleSystem.MinMaxGradient(Gradient gradient)
			{
				return new ParticleSystem.MinMaxGradient(gradient);
			}

			// Token: 0x04000027 RID: 39
			[SerializeField]
			private ParticleSystemGradientMode m_Mode;

			// Token: 0x04000028 RID: 40
			[SerializeField]
			private Gradient m_GradientMin;

			// Token: 0x04000029 RID: 41
			[SerializeField]
			private Gradient m_GradientMax;

			// Token: 0x0400002A RID: 42
			[SerializeField]
			private Color m_ColorMin;

			// Token: 0x0400002B RID: 43
			[SerializeField]
			private Color m_ColorMax;
		}

		// Token: 0x0200000F RID: 15
		public struct EmitParams
		{
			// Token: 0x170000F3 RID: 243
			// (get) Token: 0x06000350 RID: 848 RVA: 0x000056F4 File Offset: 0x000038F4
			// (set) Token: 0x06000351 RID: 849 RVA: 0x00005714 File Offset: 0x00003914
			public Vector3 position
			{
				get
				{
					return this.m_Particle.position;
				}
				set
				{
					this.m_Particle.position = value;
					this.m_PositionSet = true;
				}
			}

			// Token: 0x170000F4 RID: 244
			// (get) Token: 0x06000352 RID: 850 RVA: 0x0000572C File Offset: 0x0000392C
			// (set) Token: 0x06000353 RID: 851 RVA: 0x00005747 File Offset: 0x00003947
			public bool applyShapeToPosition
			{
				get
				{
					return this.m_ApplyShapeToPosition;
				}
				set
				{
					this.m_ApplyShapeToPosition = value;
				}
			}

			// Token: 0x170000F5 RID: 245
			// (get) Token: 0x06000354 RID: 852 RVA: 0x00005754 File Offset: 0x00003954
			// (set) Token: 0x06000355 RID: 853 RVA: 0x00005774 File Offset: 0x00003974
			public Vector3 velocity
			{
				get
				{
					return this.m_Particle.velocity;
				}
				set
				{
					this.m_Particle.velocity = value;
					this.m_VelocitySet = true;
				}
			}

			// Token: 0x170000F6 RID: 246
			// (get) Token: 0x06000356 RID: 854 RVA: 0x0000578C File Offset: 0x0000398C
			// (set) Token: 0x06000357 RID: 855 RVA: 0x000057AC File Offset: 0x000039AC
			public float startLifetime
			{
				get
				{
					return this.m_Particle.startLifetime;
				}
				set
				{
					this.m_Particle.startLifetime = value;
					this.m_StartLifetimeSet = true;
				}
			}

			// Token: 0x170000F7 RID: 247
			// (get) Token: 0x06000358 RID: 856 RVA: 0x000057C4 File Offset: 0x000039C4
			// (set) Token: 0x06000359 RID: 857 RVA: 0x000057E4 File Offset: 0x000039E4
			public float startSize
			{
				get
				{
					return this.m_Particle.startSize;
				}
				set
				{
					this.m_Particle.startSize = value;
					this.m_StartSizeSet = true;
				}
			}

			// Token: 0x170000F8 RID: 248
			// (get) Token: 0x0600035A RID: 858 RVA: 0x000057FC File Offset: 0x000039FC
			// (set) Token: 0x0600035B RID: 859 RVA: 0x0000581C File Offset: 0x00003A1C
			public Vector3 startSize3D
			{
				get
				{
					return this.m_Particle.startSize3D;
				}
				set
				{
					this.m_Particle.startSize3D = value;
					this.m_StartSizeSet = true;
				}
			}

			// Token: 0x170000F9 RID: 249
			// (get) Token: 0x0600035C RID: 860 RVA: 0x00005834 File Offset: 0x00003A34
			// (set) Token: 0x0600035D RID: 861 RVA: 0x00005854 File Offset: 0x00003A54
			public Vector3 axisOfRotation
			{
				get
				{
					return this.m_Particle.axisOfRotation;
				}
				set
				{
					this.m_Particle.axisOfRotation = value;
					this.m_AxisOfRotationSet = true;
				}
			}

			// Token: 0x170000FA RID: 250
			// (get) Token: 0x0600035E RID: 862 RVA: 0x0000586C File Offset: 0x00003A6C
			// (set) Token: 0x0600035F RID: 863 RVA: 0x0000588C File Offset: 0x00003A8C
			public float rotation
			{
				get
				{
					return this.m_Particle.rotation;
				}
				set
				{
					this.m_Particle.rotation = value;
					this.m_RotationSet = true;
				}
			}

			// Token: 0x170000FB RID: 251
			// (get) Token: 0x06000360 RID: 864 RVA: 0x000058A4 File Offset: 0x00003AA4
			// (set) Token: 0x06000361 RID: 865 RVA: 0x000058C4 File Offset: 0x00003AC4
			public Vector3 rotation3D
			{
				get
				{
					return this.m_Particle.rotation3D;
				}
				set
				{
					this.m_Particle.rotation3D = value;
					this.m_RotationSet = true;
				}
			}

			// Token: 0x170000FC RID: 252
			// (get) Token: 0x06000362 RID: 866 RVA: 0x000058DC File Offset: 0x00003ADC
			// (set) Token: 0x06000363 RID: 867 RVA: 0x000058FC File Offset: 0x00003AFC
			public float angularVelocity
			{
				get
				{
					return this.m_Particle.angularVelocity;
				}
				set
				{
					this.m_Particle.angularVelocity = value;
					this.m_AngularVelocitySet = true;
				}
			}

			// Token: 0x170000FD RID: 253
			// (get) Token: 0x06000364 RID: 868 RVA: 0x00005914 File Offset: 0x00003B14
			// (set) Token: 0x06000365 RID: 869 RVA: 0x00005934 File Offset: 0x00003B34
			public Vector3 angularVelocity3D
			{
				get
				{
					return this.m_Particle.angularVelocity3D;
				}
				set
				{
					this.m_Particle.angularVelocity3D = value;
					this.m_AngularVelocitySet = true;
				}
			}

			// Token: 0x170000FE RID: 254
			// (get) Token: 0x06000366 RID: 870 RVA: 0x0000594C File Offset: 0x00003B4C
			// (set) Token: 0x06000367 RID: 871 RVA: 0x0000596C File Offset: 0x00003B6C
			public Color32 startColor
			{
				get
				{
					return this.m_Particle.startColor;
				}
				set
				{
					this.m_Particle.startColor = value;
					this.m_StartColorSet = true;
				}
			}

			// Token: 0x170000FF RID: 255
			// (get) Token: 0x06000368 RID: 872 RVA: 0x00005984 File Offset: 0x00003B84
			// (set) Token: 0x06000369 RID: 873 RVA: 0x000059A4 File Offset: 0x00003BA4
			public uint randomSeed
			{
				get
				{
					return this.m_Particle.randomSeed;
				}
				set
				{
					this.m_Particle.randomSeed = value;
					this.m_RandomSeedSet = true;
				}
			}

			// Token: 0x0600036A RID: 874 RVA: 0x000059BA File Offset: 0x00003BBA
			public void ResetPosition()
			{
				this.m_PositionSet = false;
			}

			// Token: 0x0600036B RID: 875 RVA: 0x000059C4 File Offset: 0x00003BC4
			public void ResetVelocity()
			{
				this.m_VelocitySet = false;
			}

			// Token: 0x0600036C RID: 876 RVA: 0x000059CE File Offset: 0x00003BCE
			public void ResetAxisOfRotation()
			{
				this.m_AxisOfRotationSet = false;
			}

			// Token: 0x0600036D RID: 877 RVA: 0x000059D8 File Offset: 0x00003BD8
			public void ResetRotation()
			{
				this.m_RotationSet = false;
			}

			// Token: 0x0600036E RID: 878 RVA: 0x000059E2 File Offset: 0x00003BE2
			public void ResetAngularVelocity()
			{
				this.m_AngularVelocitySet = false;
			}

			// Token: 0x0600036F RID: 879 RVA: 0x000059EC File Offset: 0x00003BEC
			public void ResetStartSize()
			{
				this.m_StartSizeSet = false;
			}

			// Token: 0x06000370 RID: 880 RVA: 0x000059F6 File Offset: 0x00003BF6
			public void ResetStartColor()
			{
				this.m_StartColorSet = false;
			}

			// Token: 0x06000371 RID: 881 RVA: 0x00005A00 File Offset: 0x00003C00
			public void ResetRandomSeed()
			{
				this.m_RandomSeedSet = false;
			}

			// Token: 0x06000372 RID: 882 RVA: 0x00005A0A File Offset: 0x00003C0A
			public void ResetStartLifetime()
			{
				this.m_StartLifetimeSet = false;
			}

			// Token: 0x0400002C RID: 44
			[NativeName("particle")]
			private ParticleSystem.Particle m_Particle;

			// Token: 0x0400002D RID: 45
			[NativeName("positionSet")]
			private bool m_PositionSet;

			// Token: 0x0400002E RID: 46
			[NativeName("velocitySet")]
			private bool m_VelocitySet;

			// Token: 0x0400002F RID: 47
			[NativeName("axisOfRotationSet")]
			private bool m_AxisOfRotationSet;

			// Token: 0x04000030 RID: 48
			[NativeName("rotationSet")]
			private bool m_RotationSet;

			// Token: 0x04000031 RID: 49
			[NativeName("rotationalSpeedSet")]
			private bool m_AngularVelocitySet;

			// Token: 0x04000032 RID: 50
			[NativeName("startSizeSet")]
			private bool m_StartSizeSet;

			// Token: 0x04000033 RID: 51
			[NativeName("startColorSet")]
			private bool m_StartColorSet;

			// Token: 0x04000034 RID: 52
			[NativeName("randomSeedSet")]
			private bool m_RandomSeedSet;

			// Token: 0x04000035 RID: 53
			[NativeName("startLifetimeSet")]
			private bool m_StartLifetimeSet;

			// Token: 0x04000036 RID: 54
			[NativeName("applyShapeToPosition")]
			private bool m_ApplyShapeToPosition;
		}

		// Token: 0x02000010 RID: 16
		public struct VelocityOverLifetimeModule
		{
			// Token: 0x06000373 RID: 883 RVA: 0x00005A14 File Offset: 0x00003C14
			internal VelocityOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000100 RID: 256
			// (get) Token: 0x06000375 RID: 885 RVA: 0x00005A30 File Offset: 0x00003C30
			// (set) Token: 0x06000374 RID: 884 RVA: 0x00005A1E File Offset: 0x00003C1E
			public bool enabled
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000101 RID: 257
			// (get) Token: 0x06000377 RID: 887 RVA: 0x00005A60 File Offset: 0x00003C60
			// (set) Token: 0x06000376 RID: 886 RVA: 0x00005A50 File Offset: 0x00003C50
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000102 RID: 258
			// (get) Token: 0x06000379 RID: 889 RVA: 0x00005A9C File Offset: 0x00003C9C
			// (set) Token: 0x06000378 RID: 888 RVA: 0x00005A8B File Offset: 0x00003C8B
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000103 RID: 259
			// (get) Token: 0x0600037B RID: 891 RVA: 0x00005AD8 File Offset: 0x00003CD8
			// (set) Token: 0x0600037A RID: 890 RVA: 0x00005AC7 File Offset: 0x00003CC7
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000104 RID: 260
			// (get) Token: 0x0600037C RID: 892 RVA: 0x00005B04 File Offset: 0x00003D04
			// (set) Token: 0x0600037D RID: 893 RVA: 0x00005B24 File Offset: 0x00003D24
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000105 RID: 261
			// (get) Token: 0x0600037E RID: 894 RVA: 0x00005B34 File Offset: 0x00003D34
			// (set) Token: 0x0600037F RID: 895 RVA: 0x00005B54 File Offset: 0x00003D54
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000106 RID: 262
			// (get) Token: 0x06000380 RID: 896 RVA: 0x00005B64 File Offset: 0x00003D64
			// (set) Token: 0x06000381 RID: 897 RVA: 0x00005B84 File Offset: 0x00003D84
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000107 RID: 263
			// (get) Token: 0x06000383 RID: 899 RVA: 0x00005BA4 File Offset: 0x00003DA4
			// (set) Token: 0x06000382 RID: 898 RVA: 0x00005B93 File Offset: 0x00003D93
			public ParticleSystem.MinMaxCurve orbitalX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetOrbitalX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000108 RID: 264
			// (get) Token: 0x06000385 RID: 901 RVA: 0x00005BE0 File Offset: 0x00003DE0
			// (set) Token: 0x06000384 RID: 900 RVA: 0x00005BCF File Offset: 0x00003DCF
			public ParticleSystem.MinMaxCurve orbitalY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetOrbitalY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000109 RID: 265
			// (get) Token: 0x06000387 RID: 903 RVA: 0x00005C1C File Offset: 0x00003E1C
			// (set) Token: 0x06000386 RID: 902 RVA: 0x00005C0B File Offset: 0x00003E0B
			public ParticleSystem.MinMaxCurve orbitalZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetOrbitalZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700010A RID: 266
			// (get) Token: 0x06000388 RID: 904 RVA: 0x00005C48 File Offset: 0x00003E48
			// (set) Token: 0x06000389 RID: 905 RVA: 0x00005C68 File Offset: 0x00003E68
			public float orbitalXMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetOrbitalXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700010B RID: 267
			// (get) Token: 0x0600038A RID: 906 RVA: 0x00005C78 File Offset: 0x00003E78
			// (set) Token: 0x0600038B RID: 907 RVA: 0x00005C98 File Offset: 0x00003E98
			public float orbitalYMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetOrbitalYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700010C RID: 268
			// (get) Token: 0x0600038C RID: 908 RVA: 0x00005CA8 File Offset: 0x00003EA8
			// (set) Token: 0x0600038D RID: 909 RVA: 0x00005CC8 File Offset: 0x00003EC8
			public float orbitalZMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetOrbitalZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700010D RID: 269
			// (get) Token: 0x0600038F RID: 911 RVA: 0x00005CE8 File Offset: 0x00003EE8
			// (set) Token: 0x0600038E RID: 910 RVA: 0x00005CD7 File Offset: 0x00003ED7
			public ParticleSystem.MinMaxCurve orbitalOffsetX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetOrbitalOffsetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalOffsetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700010E RID: 270
			// (get) Token: 0x06000391 RID: 913 RVA: 0x00005D24 File Offset: 0x00003F24
			// (set) Token: 0x06000390 RID: 912 RVA: 0x00005D13 File Offset: 0x00003F13
			public ParticleSystem.MinMaxCurve orbitalOffsetY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetOrbitalOffsetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalOffsetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700010F RID: 271
			// (get) Token: 0x06000393 RID: 915 RVA: 0x00005D60 File Offset: 0x00003F60
			// (set) Token: 0x06000392 RID: 914 RVA: 0x00005D4F File Offset: 0x00003F4F
			public ParticleSystem.MinMaxCurve orbitalOffsetZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetOrbitalOffsetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalOffsetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000110 RID: 272
			// (get) Token: 0x06000394 RID: 916 RVA: 0x00005D8C File Offset: 0x00003F8C
			// (set) Token: 0x06000395 RID: 917 RVA: 0x00005DAC File Offset: 0x00003FAC
			public float orbitalOffsetXMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetOrbitalOffsetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalOffsetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000111 RID: 273
			// (get) Token: 0x06000396 RID: 918 RVA: 0x00005DBC File Offset: 0x00003FBC
			// (set) Token: 0x06000397 RID: 919 RVA: 0x00005DDC File Offset: 0x00003FDC
			public float orbitalOffsetYMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetOrbitalOffsetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalOffsetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000112 RID: 274
			// (get) Token: 0x06000398 RID: 920 RVA: 0x00005DEC File Offset: 0x00003FEC
			// (set) Token: 0x06000399 RID: 921 RVA: 0x00005E0C File Offset: 0x0000400C
			public float orbitalOffsetZMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetOrbitalOffsetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetOrbitalOffsetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000113 RID: 275
			// (get) Token: 0x0600039B RID: 923 RVA: 0x00005E2C File Offset: 0x0000402C
			// (set) Token: 0x0600039A RID: 922 RVA: 0x00005E1B File Offset: 0x0000401B
			public ParticleSystem.MinMaxCurve radial
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetRadial(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetRadial(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000114 RID: 276
			// (get) Token: 0x0600039C RID: 924 RVA: 0x00005E58 File Offset: 0x00004058
			// (set) Token: 0x0600039D RID: 925 RVA: 0x00005E78 File Offset: 0x00004078
			public float radialMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetRadialMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetRadialMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000115 RID: 277
			// (get) Token: 0x0600039F RID: 927 RVA: 0x00005E98 File Offset: 0x00004098
			// (set) Token: 0x0600039E RID: 926 RVA: 0x00005E87 File Offset: 0x00004087
			public ParticleSystem.MinMaxCurve speedModifier
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetSpeedModifier(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetSpeedModifier(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000116 RID: 278
			// (get) Token: 0x060003A0 RID: 928 RVA: 0x00005EC4 File Offset: 0x000040C4
			// (set) Token: 0x060003A1 RID: 929 RVA: 0x00005EE4 File Offset: 0x000040E4
			public float speedModifierMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetSpeedModifierMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetSpeedModifierMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000117 RID: 279
			// (get) Token: 0x060003A2 RID: 930 RVA: 0x00005EF4 File Offset: 0x000040F4
			// (set) Token: 0x060003A3 RID: 931 RVA: 0x00005F20 File Offset: 0x00004120
			public ParticleSystemSimulationSpace space
			{
				get
				{
					return (!ParticleSystem.VelocityOverLifetimeModule.GetWorldSpace(this.m_ParticleSystem)) ? ParticleSystemSimulationSpace.Local : ParticleSystemSimulationSpace.World;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetWorldSpace(this.m_ParticleSystem, value == ParticleSystemSimulationSpace.World);
				}
			}

			// Token: 0x060003A4 RID: 932
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060003A5 RID: 933
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060003A6 RID: 934
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003A7 RID: 935
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003A8 RID: 936
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003A9 RID: 937
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003AA RID: 938
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003AB RID: 939
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003AC RID: 940
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x060003AD RID: 941
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x060003AE RID: 942
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x060003AF RID: 943
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x060003B0 RID: 944
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x060003B1 RID: 945
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x060003B2 RID: 946
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003B3 RID: 947
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetOrbitalX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003B4 RID: 948
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003B5 RID: 949
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetOrbitalY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003B6 RID: 950
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003B7 RID: 951
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetOrbitalZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003B8 RID: 952
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalXMultiplier(ParticleSystem system, float value);

			// Token: 0x060003B9 RID: 953
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetOrbitalXMultiplier(ParticleSystem system);

			// Token: 0x060003BA RID: 954
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalYMultiplier(ParticleSystem system, float value);

			// Token: 0x060003BB RID: 955
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetOrbitalYMultiplier(ParticleSystem system);

			// Token: 0x060003BC RID: 956
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalZMultiplier(ParticleSystem system, float value);

			// Token: 0x060003BD RID: 957
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetOrbitalZMultiplier(ParticleSystem system);

			// Token: 0x060003BE RID: 958
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalOffsetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003BF RID: 959
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetOrbitalOffsetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003C0 RID: 960
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalOffsetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003C1 RID: 961
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetOrbitalOffsetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003C2 RID: 962
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalOffsetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003C3 RID: 963
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetOrbitalOffsetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003C4 RID: 964
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalOffsetXMultiplier(ParticleSystem system, float value);

			// Token: 0x060003C5 RID: 965
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetOrbitalOffsetXMultiplier(ParticleSystem system);

			// Token: 0x060003C6 RID: 966
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalOffsetYMultiplier(ParticleSystem system, float value);

			// Token: 0x060003C7 RID: 967
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetOrbitalOffsetYMultiplier(ParticleSystem system);

			// Token: 0x060003C8 RID: 968
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOrbitalOffsetZMultiplier(ParticleSystem system, float value);

			// Token: 0x060003C9 RID: 969
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetOrbitalOffsetZMultiplier(ParticleSystem system);

			// Token: 0x060003CA RID: 970
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadial(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003CB RID: 971
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRadial(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003CC RID: 972
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadialMultiplier(ParticleSystem system, float value);

			// Token: 0x060003CD RID: 973
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRadialMultiplier(ParticleSystem system);

			// Token: 0x060003CE RID: 974
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSpeedModifier(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003CF RID: 975
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetSpeedModifier(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003D0 RID: 976
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSpeedModifierMultiplier(ParticleSystem system, float value);

			// Token: 0x060003D1 RID: 977
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetSpeedModifierMultiplier(ParticleSystem system);

			// Token: 0x060003D2 RID: 978
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWorldSpace(ParticleSystem system, bool value);

			// Token: 0x060003D3 RID: 979
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetWorldSpace(ParticleSystem system);

			// Token: 0x04000037 RID: 55
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000011 RID: 17
		public struct LimitVelocityOverLifetimeModule
		{
			// Token: 0x060003D4 RID: 980 RVA: 0x00005F32 File Offset: 0x00004132
			internal LimitVelocityOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000118 RID: 280
			// (get) Token: 0x060003D6 RID: 982 RVA: 0x00005F4C File Offset: 0x0000414C
			// (set) Token: 0x060003D5 RID: 981 RVA: 0x00005F3C File Offset: 0x0000413C
			public bool enabled
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000119 RID: 281
			// (get) Token: 0x060003D8 RID: 984 RVA: 0x00005F7C File Offset: 0x0000417C
			// (set) Token: 0x060003D7 RID: 983 RVA: 0x00005F6C File Offset: 0x0000416C
			public ParticleSystem.MinMaxCurve limitX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LimitVelocityOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700011A RID: 282
			// (get) Token: 0x060003D9 RID: 985 RVA: 0x00005FA8 File Offset: 0x000041A8
			// (set) Token: 0x060003DA RID: 986 RVA: 0x00005FC8 File Offset: 0x000041C8
			public float limitXMultiplier
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700011B RID: 283
			// (get) Token: 0x060003DC RID: 988 RVA: 0x00005FE8 File Offset: 0x000041E8
			// (set) Token: 0x060003DB RID: 987 RVA: 0x00005FD7 File Offset: 0x000041D7
			public ParticleSystem.MinMaxCurve limitY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LimitVelocityOverLifetimeModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700011C RID: 284
			// (get) Token: 0x060003DD RID: 989 RVA: 0x00006014 File Offset: 0x00004214
			// (set) Token: 0x060003DE RID: 990 RVA: 0x00006034 File Offset: 0x00004234
			public float limitYMultiplier
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700011D RID: 285
			// (get) Token: 0x060003E0 RID: 992 RVA: 0x00006054 File Offset: 0x00004254
			// (set) Token: 0x060003DF RID: 991 RVA: 0x00006043 File Offset: 0x00004243
			public ParticleSystem.MinMaxCurve limitZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LimitVelocityOverLifetimeModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700011E RID: 286
			// (get) Token: 0x060003E1 RID: 993 RVA: 0x00006080 File Offset: 0x00004280
			// (set) Token: 0x060003E2 RID: 994 RVA: 0x000060A0 File Offset: 0x000042A0
			public float limitZMultiplier
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700011F RID: 287
			// (get) Token: 0x060003E4 RID: 996 RVA: 0x000060C0 File Offset: 0x000042C0
			// (set) Token: 0x060003E3 RID: 995 RVA: 0x000060AF File Offset: 0x000042AF
			public ParticleSystem.MinMaxCurve limit
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LimitVelocityOverLifetimeModule.GetMagnitude(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetMagnitude(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000120 RID: 288
			// (get) Token: 0x060003E5 RID: 997 RVA: 0x000060EC File Offset: 0x000042EC
			// (set) Token: 0x060003E6 RID: 998 RVA: 0x0000610C File Offset: 0x0000430C
			public float limitMultiplier
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetMagnitudeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetMagnitudeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000121 RID: 289
			// (get) Token: 0x060003E7 RID: 999 RVA: 0x0000611C File Offset: 0x0000431C
			// (set) Token: 0x060003E8 RID: 1000 RVA: 0x0000613C File Offset: 0x0000433C
			public float dampen
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetDampen(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetDampen(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000122 RID: 290
			// (get) Token: 0x060003E9 RID: 1001 RVA: 0x0000614C File Offset: 0x0000434C
			// (set) Token: 0x060003EA RID: 1002 RVA: 0x0000616C File Offset: 0x0000436C
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000123 RID: 291
			// (get) Token: 0x060003EB RID: 1003 RVA: 0x0000617C File Offset: 0x0000437C
			// (set) Token: 0x060003EC RID: 1004 RVA: 0x000061A8 File Offset: 0x000043A8
			public ParticleSystemSimulationSpace space
			{
				get
				{
					return (!ParticleSystem.LimitVelocityOverLifetimeModule.GetWorldSpace(this.m_ParticleSystem)) ? ParticleSystemSimulationSpace.Local : ParticleSystemSimulationSpace.World;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetWorldSpace(this.m_ParticleSystem, value == ParticleSystemSimulationSpace.World);
				}
			}

			// Token: 0x17000124 RID: 292
			// (get) Token: 0x060003EE RID: 1006 RVA: 0x000061CC File Offset: 0x000043CC
			// (set) Token: 0x060003ED RID: 1005 RVA: 0x000061BA File Offset: 0x000043BA
			public ParticleSystem.MinMaxCurve drag
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LimitVelocityOverLifetimeModule.GetDrag(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetDrag(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000125 RID: 293
			// (get) Token: 0x060003EF RID: 1007 RVA: 0x000061F8 File Offset: 0x000043F8
			// (set) Token: 0x060003F0 RID: 1008 RVA: 0x00006218 File Offset: 0x00004418
			public float dragMultiplier
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetDragMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetDragMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000126 RID: 294
			// (get) Token: 0x060003F1 RID: 1009 RVA: 0x00006228 File Offset: 0x00004428
			// (set) Token: 0x060003F2 RID: 1010 RVA: 0x00006248 File Offset: 0x00004448
			public bool multiplyDragByParticleSize
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetMultiplyDragByParticleSize(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetMultiplyDragByParticleSize(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000127 RID: 295
			// (get) Token: 0x060003F3 RID: 1011 RVA: 0x00006258 File Offset: 0x00004458
			// (set) Token: 0x060003F4 RID: 1012 RVA: 0x00006278 File Offset: 0x00004478
			public bool multiplyDragByParticleVelocity
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetMultiplyDragByParticleVelocity(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetMultiplyDragByParticleVelocity(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060003F5 RID: 1013
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060003F6 RID: 1014
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060003F7 RID: 1015
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003F8 RID: 1016
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003F9 RID: 1017
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x060003FA RID: 1018
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x060003FB RID: 1019
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003FC RID: 1020
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060003FD RID: 1021
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x060003FE RID: 1022
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x060003FF RID: 1023
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000400 RID: 1024
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000401 RID: 1025
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x06000402 RID: 1026
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x06000403 RID: 1027
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMagnitude(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000404 RID: 1028
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetMagnitude(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000405 RID: 1029
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMagnitudeMultiplier(ParticleSystem system, float value);

			// Token: 0x06000406 RID: 1030
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMagnitudeMultiplier(ParticleSystem system);

			// Token: 0x06000407 RID: 1031
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDampen(ParticleSystem system, float value);

			// Token: 0x06000408 RID: 1032
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetDampen(ParticleSystem system);

			// Token: 0x06000409 RID: 1033
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x0600040A RID: 1034
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x0600040B RID: 1035
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWorldSpace(ParticleSystem system, bool value);

			// Token: 0x0600040C RID: 1036
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetWorldSpace(ParticleSystem system);

			// Token: 0x0600040D RID: 1037
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDrag(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600040E RID: 1038
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetDrag(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600040F RID: 1039
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDragMultiplier(ParticleSystem system, float value);

			// Token: 0x06000410 RID: 1040
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetDragMultiplier(ParticleSystem system);

			// Token: 0x06000411 RID: 1041
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMultiplyDragByParticleSize(ParticleSystem system, bool value);

			// Token: 0x06000412 RID: 1042
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetMultiplyDragByParticleSize(ParticleSystem system);

			// Token: 0x06000413 RID: 1043
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMultiplyDragByParticleVelocity(ParticleSystem system, bool value);

			// Token: 0x06000414 RID: 1044
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetMultiplyDragByParticleVelocity(ParticleSystem system);

			// Token: 0x04000038 RID: 56
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000012 RID: 18
		public struct InheritVelocityModule
		{
			// Token: 0x06000415 RID: 1045 RVA: 0x00006287 File Offset: 0x00004487
			internal InheritVelocityModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000128 RID: 296
			// (get) Token: 0x06000417 RID: 1047 RVA: 0x000062A0 File Offset: 0x000044A0
			// (set) Token: 0x06000416 RID: 1046 RVA: 0x00006291 File Offset: 0x00004491
			public bool enabled
			{
				get
				{
					return ParticleSystem.InheritVelocityModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.InheritVelocityModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000129 RID: 297
			// (get) Token: 0x06000418 RID: 1048 RVA: 0x000062C0 File Offset: 0x000044C0
			// (set) Token: 0x06000419 RID: 1049 RVA: 0x000062E0 File Offset: 0x000044E0
			public ParticleSystemInheritVelocityMode mode
			{
				get
				{
					return ParticleSystem.InheritVelocityModule.GetMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.InheritVelocityModule.SetMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700012A RID: 298
			// (get) Token: 0x0600041B RID: 1051 RVA: 0x00006300 File Offset: 0x00004500
			// (set) Token: 0x0600041A RID: 1050 RVA: 0x000062EF File Offset: 0x000044EF
			public ParticleSystem.MinMaxCurve curve
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.InheritVelocityModule.GetCurve(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.InheritVelocityModule.SetCurve(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700012B RID: 299
			// (get) Token: 0x0600041C RID: 1052 RVA: 0x0000632C File Offset: 0x0000452C
			// (set) Token: 0x0600041D RID: 1053 RVA: 0x0000634C File Offset: 0x0000454C
			public float curveMultiplier
			{
				get
				{
					return ParticleSystem.InheritVelocityModule.GetCurveMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.InheritVelocityModule.SetCurveMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x0600041E RID: 1054
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x0600041F RID: 1055
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06000420 RID: 1056
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMode(ParticleSystem system, ParticleSystemInheritVelocityMode value);

			// Token: 0x06000421 RID: 1057
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemInheritVelocityMode GetMode(ParticleSystem system);

			// Token: 0x06000422 RID: 1058
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCurve(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000423 RID: 1059
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetCurve(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000424 RID: 1060
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCurveMultiplier(ParticleSystem system, float value);

			// Token: 0x06000425 RID: 1061
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetCurveMultiplier(ParticleSystem system);

			// Token: 0x04000039 RID: 57
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000013 RID: 19
		public struct ForceOverLifetimeModule
		{
			// Token: 0x06000426 RID: 1062 RVA: 0x0000635B File Offset: 0x0000455B
			internal ForceOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x1700012C RID: 300
			// (get) Token: 0x06000428 RID: 1064 RVA: 0x00006374 File Offset: 0x00004574
			// (set) Token: 0x06000427 RID: 1063 RVA: 0x00006365 File Offset: 0x00004565
			public bool enabled
			{
				get
				{
					return ParticleSystem.ForceOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700012D RID: 301
			// (get) Token: 0x0600042A RID: 1066 RVA: 0x000063A4 File Offset: 0x000045A4
			// (set) Token: 0x06000429 RID: 1065 RVA: 0x00006394 File Offset: 0x00004594
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.ForceOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700012E RID: 302
			// (get) Token: 0x0600042C RID: 1068 RVA: 0x000063E0 File Offset: 0x000045E0
			// (set) Token: 0x0600042B RID: 1067 RVA: 0x000063CF File Offset: 0x000045CF
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.ForceOverLifetimeModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700012F RID: 303
			// (get) Token: 0x0600042E RID: 1070 RVA: 0x0000641C File Offset: 0x0000461C
			// (set) Token: 0x0600042D RID: 1069 RVA: 0x0000640B File Offset: 0x0000460B
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.ForceOverLifetimeModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000130 RID: 304
			// (get) Token: 0x0600042F RID: 1071 RVA: 0x00006448 File Offset: 0x00004648
			// (set) Token: 0x06000430 RID: 1072 RVA: 0x00006468 File Offset: 0x00004668
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.ForceOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000131 RID: 305
			// (get) Token: 0x06000431 RID: 1073 RVA: 0x00006478 File Offset: 0x00004678
			// (set) Token: 0x06000432 RID: 1074 RVA: 0x00006498 File Offset: 0x00004698
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.ForceOverLifetimeModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000132 RID: 306
			// (get) Token: 0x06000433 RID: 1075 RVA: 0x000064A8 File Offset: 0x000046A8
			// (set) Token: 0x06000434 RID: 1076 RVA: 0x000064C8 File Offset: 0x000046C8
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.ForceOverLifetimeModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000133 RID: 307
			// (get) Token: 0x06000435 RID: 1077 RVA: 0x000064D8 File Offset: 0x000046D8
			// (set) Token: 0x06000436 RID: 1078 RVA: 0x00006504 File Offset: 0x00004704
			public ParticleSystemSimulationSpace space
			{
				get
				{
					return (!ParticleSystem.ForceOverLifetimeModule.GetWorldSpace(this.m_ParticleSystem)) ? ParticleSystemSimulationSpace.Local : ParticleSystemSimulationSpace.World;
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetWorldSpace(this.m_ParticleSystem, value == ParticleSystemSimulationSpace.World);
				}
			}

			// Token: 0x17000134 RID: 308
			// (get) Token: 0x06000438 RID: 1080 RVA: 0x00006528 File Offset: 0x00004728
			// (set) Token: 0x06000437 RID: 1079 RVA: 0x00006516 File Offset: 0x00004716
			public bool randomized
			{
				get
				{
					return ParticleSystem.ForceOverLifetimeModule.GetRandomized(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetRandomized(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06000439 RID: 1081
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x0600043A RID: 1082
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x0600043B RID: 1083
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600043C RID: 1084
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600043D RID: 1085
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600043E RID: 1086
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600043F RID: 1087
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000440 RID: 1088
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000441 RID: 1089
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x06000442 RID: 1090
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x06000443 RID: 1091
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x06000444 RID: 1092
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x06000445 RID: 1093
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x06000446 RID: 1094
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x06000447 RID: 1095
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWorldSpace(ParticleSystem system, bool value);

			// Token: 0x06000448 RID: 1096
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetWorldSpace(ParticleSystem system);

			// Token: 0x06000449 RID: 1097
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRandomized(ParticleSystem system, bool value);

			// Token: 0x0600044A RID: 1098
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetRandomized(ParticleSystem system);

			// Token: 0x0400003A RID: 58
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000014 RID: 20
		public struct ColorOverLifetimeModule
		{
			// Token: 0x0600044B RID: 1099 RVA: 0x00006548 File Offset: 0x00004748
			internal ColorOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000135 RID: 309
			// (get) Token: 0x0600044D RID: 1101 RVA: 0x00006564 File Offset: 0x00004764
			// (set) Token: 0x0600044C RID: 1100 RVA: 0x00006552 File Offset: 0x00004752
			public bool enabled
			{
				get
				{
					return ParticleSystem.ColorOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ColorOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000136 RID: 310
			// (get) Token: 0x0600044F RID: 1103 RVA: 0x00006594 File Offset: 0x00004794
			// (set) Token: 0x0600044E RID: 1102 RVA: 0x00006584 File Offset: 0x00004784
			public ParticleSystem.MinMaxGradient color
			{
				get
				{
					ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
					ParticleSystem.ColorOverLifetimeModule.GetColor(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ColorOverLifetimeModule.SetColor(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x06000450 RID: 1104
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06000451 RID: 1105
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06000452 RID: 1106
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x06000453 RID: 1107
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0400003B RID: 59
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000015 RID: 21
		public struct ColorBySpeedModule
		{
			// Token: 0x06000454 RID: 1108 RVA: 0x000065BF File Offset: 0x000047BF
			internal ColorBySpeedModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000137 RID: 311
			// (get) Token: 0x06000456 RID: 1110 RVA: 0x000065D8 File Offset: 0x000047D8
			// (set) Token: 0x06000455 RID: 1109 RVA: 0x000065C9 File Offset: 0x000047C9
			public bool enabled
			{
				get
				{
					return ParticleSystem.ColorBySpeedModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ColorBySpeedModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000138 RID: 312
			// (get) Token: 0x06000458 RID: 1112 RVA: 0x00006608 File Offset: 0x00004808
			// (set) Token: 0x06000457 RID: 1111 RVA: 0x000065F8 File Offset: 0x000047F8
			public ParticleSystem.MinMaxGradient color
			{
				get
				{
					ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
					ParticleSystem.ColorBySpeedModule.GetColor(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ColorBySpeedModule.SetColor(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000139 RID: 313
			// (get) Token: 0x0600045A RID: 1114 RVA: 0x00006644 File Offset: 0x00004844
			// (set) Token: 0x06000459 RID: 1113 RVA: 0x00006633 File Offset: 0x00004833
			public Vector2 range
			{
				get
				{
					return ParticleSystem.ColorBySpeedModule.GetRange(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ColorBySpeedModule.SetRange(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x0600045B RID: 1115
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x0600045C RID: 1116
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x0600045D RID: 1117
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0600045E RID: 1118
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0600045F RID: 1119 RVA: 0x00006664 File Offset: 0x00004864
			private static void SetRange(ParticleSystem system, Vector2 value)
			{
				ParticleSystem.ColorBySpeedModule.INTERNAL_CALL_SetRange(system, ref value);
			}

			// Token: 0x06000460 RID: 1120
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetRange(ParticleSystem system, ref Vector2 value);

			// Token: 0x06000461 RID: 1121 RVA: 0x00006670 File Offset: 0x00004870
			private static Vector2 GetRange(ParticleSystem system)
			{
				Vector2 result;
				ParticleSystem.ColorBySpeedModule.INTERNAL_CALL_GetRange(system, out result);
				return result;
			}

			// Token: 0x06000462 RID: 1122
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetRange(ParticleSystem system, out Vector2 value);

			// Token: 0x0400003C RID: 60
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000016 RID: 22
		public struct SizeOverLifetimeModule
		{
			// Token: 0x06000463 RID: 1123 RVA: 0x0000668E File Offset: 0x0000488E
			internal SizeOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x1700013A RID: 314
			// (get) Token: 0x06000465 RID: 1125 RVA: 0x000066A8 File Offset: 0x000048A8
			// (set) Token: 0x06000464 RID: 1124 RVA: 0x00006698 File Offset: 0x00004898
			public bool enabled
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700013B RID: 315
			// (get) Token: 0x06000467 RID: 1127 RVA: 0x000066D8 File Offset: 0x000048D8
			// (set) Token: 0x06000466 RID: 1126 RVA: 0x000066C8 File Offset: 0x000048C8
			public ParticleSystem.MinMaxCurve size
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700013C RID: 316
			// (get) Token: 0x06000468 RID: 1128 RVA: 0x00006704 File Offset: 0x00004904
			// (set) Token: 0x06000469 RID: 1129 RVA: 0x00006724 File Offset: 0x00004924
			public float sizeMultiplier
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700013D RID: 317
			// (get) Token: 0x0600046B RID: 1131 RVA: 0x00006734 File Offset: 0x00004934
			// (set) Token: 0x0600046A RID: 1130 RVA: 0x000066C8 File Offset: 0x000048C8
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700013E RID: 318
			// (get) Token: 0x0600046C RID: 1132 RVA: 0x00006760 File Offset: 0x00004960
			// (set) Token: 0x0600046D RID: 1133 RVA: 0x00006724 File Offset: 0x00004924
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700013F RID: 319
			// (get) Token: 0x0600046F RID: 1135 RVA: 0x00006790 File Offset: 0x00004990
			// (set) Token: 0x0600046E RID: 1134 RVA: 0x00006780 File Offset: 0x00004980
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeOverLifetimeModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000140 RID: 320
			// (get) Token: 0x06000470 RID: 1136 RVA: 0x000067BC File Offset: 0x000049BC
			// (set) Token: 0x06000471 RID: 1137 RVA: 0x000067DC File Offset: 0x000049DC
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000141 RID: 321
			// (get) Token: 0x06000473 RID: 1139 RVA: 0x000067FC File Offset: 0x000049FC
			// (set) Token: 0x06000472 RID: 1138 RVA: 0x000067EB File Offset: 0x000049EB
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeOverLifetimeModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000142 RID: 322
			// (get) Token: 0x06000474 RID: 1140 RVA: 0x00006828 File Offset: 0x00004A28
			// (set) Token: 0x06000475 RID: 1141 RVA: 0x00006848 File Offset: 0x00004A48
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000143 RID: 323
			// (get) Token: 0x06000476 RID: 1142 RVA: 0x00006858 File Offset: 0x00004A58
			// (set) Token: 0x06000477 RID: 1143 RVA: 0x00006878 File Offset: 0x00004A78
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06000478 RID: 1144
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06000479 RID: 1145
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x0600047A RID: 1146
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600047B RID: 1147
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600047C RID: 1148
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600047D RID: 1149
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600047E RID: 1150
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600047F RID: 1151
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000480 RID: 1152
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x06000481 RID: 1153
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x06000482 RID: 1154
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x06000483 RID: 1155
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x06000484 RID: 1156
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x06000485 RID: 1157
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x06000486 RID: 1158
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x06000487 RID: 1159
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x0400003D RID: 61
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000017 RID: 23
		public struct SizeBySpeedModule
		{
			// Token: 0x06000488 RID: 1160 RVA: 0x00006887 File Offset: 0x00004A87
			internal SizeBySpeedModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000144 RID: 324
			// (get) Token: 0x0600048A RID: 1162 RVA: 0x000068A0 File Offset: 0x00004AA0
			// (set) Token: 0x06000489 RID: 1161 RVA: 0x00006891 File Offset: 0x00004A91
			public bool enabled
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000145 RID: 325
			// (get) Token: 0x0600048C RID: 1164 RVA: 0x000068D0 File Offset: 0x00004AD0
			// (set) Token: 0x0600048B RID: 1163 RVA: 0x000068C0 File Offset: 0x00004AC0
			public ParticleSystem.MinMaxCurve size
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeBySpeedModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000146 RID: 326
			// (get) Token: 0x0600048D RID: 1165 RVA: 0x000068FC File Offset: 0x00004AFC
			// (set) Token: 0x0600048E RID: 1166 RVA: 0x0000691C File Offset: 0x00004B1C
			public float sizeMultiplier
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000147 RID: 327
			// (get) Token: 0x06000490 RID: 1168 RVA: 0x0000692C File Offset: 0x00004B2C
			// (set) Token: 0x0600048F RID: 1167 RVA: 0x000068C0 File Offset: 0x00004AC0
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeBySpeedModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000148 RID: 328
			// (get) Token: 0x06000491 RID: 1169 RVA: 0x00006958 File Offset: 0x00004B58
			// (set) Token: 0x06000492 RID: 1170 RVA: 0x0000691C File Offset: 0x00004B1C
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000149 RID: 329
			// (get) Token: 0x06000494 RID: 1172 RVA: 0x00006988 File Offset: 0x00004B88
			// (set) Token: 0x06000493 RID: 1171 RVA: 0x00006978 File Offset: 0x00004B78
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeBySpeedModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700014A RID: 330
			// (get) Token: 0x06000495 RID: 1173 RVA: 0x000069B4 File Offset: 0x00004BB4
			// (set) Token: 0x06000496 RID: 1174 RVA: 0x000069D4 File Offset: 0x00004BD4
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700014B RID: 331
			// (get) Token: 0x06000498 RID: 1176 RVA: 0x000069F4 File Offset: 0x00004BF4
			// (set) Token: 0x06000497 RID: 1175 RVA: 0x000069E3 File Offset: 0x00004BE3
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeBySpeedModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700014C RID: 332
			// (get) Token: 0x06000499 RID: 1177 RVA: 0x00006A20 File Offset: 0x00004C20
			// (set) Token: 0x0600049A RID: 1178 RVA: 0x00006A40 File Offset: 0x00004C40
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700014D RID: 333
			// (get) Token: 0x0600049B RID: 1179 RVA: 0x00006A50 File Offset: 0x00004C50
			// (set) Token: 0x0600049C RID: 1180 RVA: 0x00006A70 File Offset: 0x00004C70
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700014E RID: 334
			// (get) Token: 0x0600049E RID: 1182 RVA: 0x00006A90 File Offset: 0x00004C90
			// (set) Token: 0x0600049D RID: 1181 RVA: 0x00006A7F File Offset: 0x00004C7F
			public Vector2 range
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetRange(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetRange(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x0600049F RID: 1183
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060004A0 RID: 1184
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060004A1 RID: 1185
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004A2 RID: 1186
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004A3 RID: 1187
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004A4 RID: 1188
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004A5 RID: 1189
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004A6 RID: 1190
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004A7 RID: 1191
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x060004A8 RID: 1192
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x060004A9 RID: 1193
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x060004AA RID: 1194
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x060004AB RID: 1195
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x060004AC RID: 1196
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x060004AD RID: 1197
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x060004AE RID: 1198
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x060004AF RID: 1199 RVA: 0x00006AB0 File Offset: 0x00004CB0
			private static void SetRange(ParticleSystem system, Vector2 value)
			{
				ParticleSystem.SizeBySpeedModule.INTERNAL_CALL_SetRange(system, ref value);
			}

			// Token: 0x060004B0 RID: 1200
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetRange(ParticleSystem system, ref Vector2 value);

			// Token: 0x060004B1 RID: 1201 RVA: 0x00006ABC File Offset: 0x00004CBC
			private static Vector2 GetRange(ParticleSystem system)
			{
				Vector2 result;
				ParticleSystem.SizeBySpeedModule.INTERNAL_CALL_GetRange(system, out result);
				return result;
			}

			// Token: 0x060004B2 RID: 1202
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetRange(ParticleSystem system, out Vector2 value);

			// Token: 0x0400003E RID: 62
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000018 RID: 24
		public struct RotationOverLifetimeModule
		{
			// Token: 0x060004B3 RID: 1203 RVA: 0x00006ADA File Offset: 0x00004CDA
			internal RotationOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x1700014F RID: 335
			// (get) Token: 0x060004B5 RID: 1205 RVA: 0x00006AF4 File Offset: 0x00004CF4
			// (set) Token: 0x060004B4 RID: 1204 RVA: 0x00006AE4 File Offset: 0x00004CE4
			public bool enabled
			{
				get
				{
					return ParticleSystem.RotationOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000150 RID: 336
			// (get) Token: 0x060004B7 RID: 1207 RVA: 0x00006B24 File Offset: 0x00004D24
			// (set) Token: 0x060004B6 RID: 1206 RVA: 0x00006B14 File Offset: 0x00004D14
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000151 RID: 337
			// (get) Token: 0x060004B8 RID: 1208 RVA: 0x00006B50 File Offset: 0x00004D50
			// (set) Token: 0x060004B9 RID: 1209 RVA: 0x00006B70 File Offset: 0x00004D70
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.RotationOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000152 RID: 338
			// (get) Token: 0x060004BB RID: 1211 RVA: 0x00006B90 File Offset: 0x00004D90
			// (set) Token: 0x060004BA RID: 1210 RVA: 0x00006B7F File Offset: 0x00004D7F
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationOverLifetimeModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000153 RID: 339
			// (get) Token: 0x060004BC RID: 1212 RVA: 0x00006BBC File Offset: 0x00004DBC
			// (set) Token: 0x060004BD RID: 1213 RVA: 0x00006BDC File Offset: 0x00004DDC
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.RotationOverLifetimeModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000154 RID: 340
			// (get) Token: 0x060004BF RID: 1215 RVA: 0x00006BFC File Offset: 0x00004DFC
			// (set) Token: 0x060004BE RID: 1214 RVA: 0x00006BEB File Offset: 0x00004DEB
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationOverLifetimeModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000155 RID: 341
			// (get) Token: 0x060004C0 RID: 1216 RVA: 0x00006C28 File Offset: 0x00004E28
			// (set) Token: 0x060004C1 RID: 1217 RVA: 0x00006C48 File Offset: 0x00004E48
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.RotationOverLifetimeModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000156 RID: 342
			// (get) Token: 0x060004C2 RID: 1218 RVA: 0x00006C58 File Offset: 0x00004E58
			// (set) Token: 0x060004C3 RID: 1219 RVA: 0x00006C78 File Offset: 0x00004E78
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.RotationOverLifetimeModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060004C4 RID: 1220
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060004C5 RID: 1221
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060004C6 RID: 1222
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004C7 RID: 1223
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004C8 RID: 1224
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004C9 RID: 1225
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004CA RID: 1226
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004CB RID: 1227
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004CC RID: 1228
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x060004CD RID: 1229
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x060004CE RID: 1230
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x060004CF RID: 1231
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x060004D0 RID: 1232
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x060004D1 RID: 1233
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x060004D2 RID: 1234
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x060004D3 RID: 1235
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x0400003F RID: 63
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000019 RID: 25
		public struct RotationBySpeedModule
		{
			// Token: 0x060004D4 RID: 1236 RVA: 0x00006C87 File Offset: 0x00004E87
			internal RotationBySpeedModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000157 RID: 343
			// (get) Token: 0x060004D6 RID: 1238 RVA: 0x00006CA0 File Offset: 0x00004EA0
			// (set) Token: 0x060004D5 RID: 1237 RVA: 0x00006C91 File Offset: 0x00004E91
			public bool enabled
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000158 RID: 344
			// (get) Token: 0x060004D8 RID: 1240 RVA: 0x00006CD0 File Offset: 0x00004ED0
			// (set) Token: 0x060004D7 RID: 1239 RVA: 0x00006CC0 File Offset: 0x00004EC0
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationBySpeedModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000159 RID: 345
			// (get) Token: 0x060004D9 RID: 1241 RVA: 0x00006CFC File Offset: 0x00004EFC
			// (set) Token: 0x060004DA RID: 1242 RVA: 0x00006D1C File Offset: 0x00004F1C
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700015A RID: 346
			// (get) Token: 0x060004DC RID: 1244 RVA: 0x00006D3C File Offset: 0x00004F3C
			// (set) Token: 0x060004DB RID: 1243 RVA: 0x00006D2B File Offset: 0x00004F2B
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationBySpeedModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700015B RID: 347
			// (get) Token: 0x060004DD RID: 1245 RVA: 0x00006D68 File Offset: 0x00004F68
			// (set) Token: 0x060004DE RID: 1246 RVA: 0x00006D88 File Offset: 0x00004F88
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700015C RID: 348
			// (get) Token: 0x060004E0 RID: 1248 RVA: 0x00006DA8 File Offset: 0x00004FA8
			// (set) Token: 0x060004DF RID: 1247 RVA: 0x00006D97 File Offset: 0x00004F97
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationBySpeedModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700015D RID: 349
			// (get) Token: 0x060004E1 RID: 1249 RVA: 0x00006DD4 File Offset: 0x00004FD4
			// (set) Token: 0x060004E2 RID: 1250 RVA: 0x00006DF4 File Offset: 0x00004FF4
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700015E RID: 350
			// (get) Token: 0x060004E3 RID: 1251 RVA: 0x00006E04 File Offset: 0x00005004
			// (set) Token: 0x060004E4 RID: 1252 RVA: 0x00006E24 File Offset: 0x00005024
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700015F RID: 351
			// (get) Token: 0x060004E6 RID: 1254 RVA: 0x00006E44 File Offset: 0x00005044
			// (set) Token: 0x060004E5 RID: 1253 RVA: 0x00006E33 File Offset: 0x00005033
			public Vector2 range
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetRange(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetRange(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060004E7 RID: 1255
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060004E8 RID: 1256
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060004E9 RID: 1257
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004EA RID: 1258
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004EB RID: 1259
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004EC RID: 1260
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004ED RID: 1261
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004EE RID: 1262
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060004EF RID: 1263
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x060004F0 RID: 1264
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x060004F1 RID: 1265
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x060004F2 RID: 1266
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x060004F3 RID: 1267
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x060004F4 RID: 1268
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x060004F5 RID: 1269
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x060004F6 RID: 1270
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x060004F7 RID: 1271 RVA: 0x00006E64 File Offset: 0x00005064
			private static void SetRange(ParticleSystem system, Vector2 value)
			{
				ParticleSystem.RotationBySpeedModule.INTERNAL_CALL_SetRange(system, ref value);
			}

			// Token: 0x060004F8 RID: 1272
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetRange(ParticleSystem system, ref Vector2 value);

			// Token: 0x060004F9 RID: 1273 RVA: 0x00006E70 File Offset: 0x00005070
			private static Vector2 GetRange(ParticleSystem system)
			{
				Vector2 result;
				ParticleSystem.RotationBySpeedModule.INTERNAL_CALL_GetRange(system, out result);
				return result;
			}

			// Token: 0x060004FA RID: 1274
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetRange(ParticleSystem system, out Vector2 value);

			// Token: 0x04000040 RID: 64
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200001A RID: 26
		public struct ExternalForcesModule
		{
			// Token: 0x060004FB RID: 1275 RVA: 0x00006E8E File Offset: 0x0000508E
			internal ExternalForcesModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000160 RID: 352
			// (get) Token: 0x060004FD RID: 1277 RVA: 0x00006EA8 File Offset: 0x000050A8
			// (set) Token: 0x060004FC RID: 1276 RVA: 0x00006E98 File Offset: 0x00005098
			public bool enabled
			{
				get
				{
					return ParticleSystem.ExternalForcesModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ExternalForcesModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000161 RID: 353
			// (get) Token: 0x060004FE RID: 1278 RVA: 0x00006EC8 File Offset: 0x000050C8
			// (set) Token: 0x060004FF RID: 1279 RVA: 0x00006EE8 File Offset: 0x000050E8
			public float multiplier
			{
				get
				{
					return ParticleSystem.ExternalForcesModule.GetMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ExternalForcesModule.SetMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000162 RID: 354
			// (get) Token: 0x06000500 RID: 1280 RVA: 0x00006EF8 File Offset: 0x000050F8
			// (set) Token: 0x06000501 RID: 1281 RVA: 0x00006F18 File Offset: 0x00005118
			public ParticleSystemGameObjectFilter influenceFilter
			{
				get
				{
					return ParticleSystem.ExternalForcesModule.GetInfluenceFilter(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ExternalForcesModule.SetInfluenceFilter(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000163 RID: 355
			// (get) Token: 0x06000502 RID: 1282 RVA: 0x00006F28 File Offset: 0x00005128
			public int influenceCount
			{
				get
				{
					return ParticleSystem.ExternalForcesModule.GetInfluenceCount(this.m_ParticleSystem);
				}
			}

			// Token: 0x06000503 RID: 1283 RVA: 0x00006F48 File Offset: 0x00005148
			public void AddInfluence(ParticleSystemForceField field)
			{
				ParticleSystem.ExternalForcesModule.AddInfluence(this.m_ParticleSystem, field);
			}

			// Token: 0x06000504 RID: 1284 RVA: 0x00006F57 File Offset: 0x00005157
			public void RemoveInfluence(int index)
			{
				ParticleSystem.ExternalForcesModule.RemoveInfluenceAtIndex(this.m_ParticleSystem, index);
			}

			// Token: 0x06000505 RID: 1285 RVA: 0x00006F66 File Offset: 0x00005166
			public void RemoveInfluence(ParticleSystemForceField field)
			{
				ParticleSystem.ExternalForcesModule.RemoveInfluence(this.m_ParticleSystem, field);
			}

			// Token: 0x06000506 RID: 1286 RVA: 0x00006F75 File Offset: 0x00005175
			public void SetInfluence(int index, ParticleSystemForceField field)
			{
				ParticleSystem.ExternalForcesModule.SetInfluence(this.m_ParticleSystem, index, field);
			}

			// Token: 0x06000507 RID: 1287 RVA: 0x00006F88 File Offset: 0x00005188
			public ParticleSystemForceField GetInfluence(int index)
			{
				return ParticleSystem.ExternalForcesModule.GetInfluence(this.m_ParticleSystem, index);
			}

			// Token: 0x06000508 RID: 1288 RVA: 0x00006FAC File Offset: 0x000051AC
			public bool IsAffectedBy(ParticleSystemForceField field)
			{
				return ParticleSystem.ExternalForcesModule.IsAffectedBy_Internal(this.m_ParticleSystem, field);
			}

			// Token: 0x06000509 RID: 1289
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool IsAffectedBy_Internal(ParticleSystem system, ParticleSystemForceField field);

			// Token: 0x0600050A RID: 1290
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x0600050B RID: 1291
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x0600050C RID: 1292
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMultiplier(ParticleSystem system, float value);

			// Token: 0x0600050D RID: 1293
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMultiplier(ParticleSystem system);

			// Token: 0x0600050E RID: 1294
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetInfluenceFilter(ParticleSystem system, ParticleSystemGameObjectFilter value);

			// Token: 0x0600050F RID: 1295
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemGameObjectFilter GetInfluenceFilter(ParticleSystem system);

			// Token: 0x06000510 RID: 1296
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetInfluenceCount(ParticleSystem system);

			// Token: 0x06000511 RID: 1297
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void AddInfluence(ParticleSystem system, ParticleSystemForceField field);

			// Token: 0x06000512 RID: 1298
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void RemoveInfluence(ParticleSystem system, ParticleSystemForceField field);

			// Token: 0x06000513 RID: 1299
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void RemoveInfluenceAtIndex(ParticleSystem system, int index);

			// Token: 0x06000514 RID: 1300
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetInfluence(ParticleSystem system, int index, ParticleSystemForceField field);

			// Token: 0x06000515 RID: 1301
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemForceField GetInfluence(ParticleSystem system, int index);

			// Token: 0x04000041 RID: 65
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200001B RID: 27
		public struct NoiseModule
		{
			// Token: 0x06000516 RID: 1302 RVA: 0x00006FCD File Offset: 0x000051CD
			internal NoiseModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000164 RID: 356
			// (get) Token: 0x06000518 RID: 1304 RVA: 0x00006FE8 File Offset: 0x000051E8
			// (set) Token: 0x06000517 RID: 1303 RVA: 0x00006FD7 File Offset: 0x000051D7
			public bool enabled
			{
				get
				{
					return ParticleSystem.NoiseModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000165 RID: 357
			// (get) Token: 0x06000519 RID: 1305 RVA: 0x00007008 File Offset: 0x00005208
			// (set) Token: 0x0600051A RID: 1306 RVA: 0x00007028 File Offset: 0x00005228
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.NoiseModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000166 RID: 358
			// (get) Token: 0x0600051C RID: 1308 RVA: 0x00007048 File Offset: 0x00005248
			// (set) Token: 0x0600051B RID: 1307 RVA: 0x00007037 File Offset: 0x00005237
			public ParticleSystem.MinMaxCurve strength
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetStrengthX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000167 RID: 359
			// (get) Token: 0x0600051D RID: 1309 RVA: 0x00007074 File Offset: 0x00005274
			// (set) Token: 0x0600051E RID: 1310 RVA: 0x00007094 File Offset: 0x00005294
			public float strengthMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetStrengthXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000168 RID: 360
			// (get) Token: 0x06000520 RID: 1312 RVA: 0x000070A4 File Offset: 0x000052A4
			// (set) Token: 0x0600051F RID: 1311 RVA: 0x00007037 File Offset: 0x00005237
			public ParticleSystem.MinMaxCurve strengthX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetStrengthX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000169 RID: 361
			// (get) Token: 0x06000521 RID: 1313 RVA: 0x000070D0 File Offset: 0x000052D0
			// (set) Token: 0x06000522 RID: 1314 RVA: 0x00007094 File Offset: 0x00005294
			public float strengthXMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetStrengthXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700016A RID: 362
			// (get) Token: 0x06000524 RID: 1316 RVA: 0x00007100 File Offset: 0x00005300
			// (set) Token: 0x06000523 RID: 1315 RVA: 0x000070F0 File Offset: 0x000052F0
			public ParticleSystem.MinMaxCurve strengthY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetStrengthY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700016B RID: 363
			// (get) Token: 0x06000525 RID: 1317 RVA: 0x0000712C File Offset: 0x0000532C
			// (set) Token: 0x06000526 RID: 1318 RVA: 0x0000714C File Offset: 0x0000534C
			public float strengthYMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetStrengthYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700016C RID: 364
			// (get) Token: 0x06000528 RID: 1320 RVA: 0x0000716C File Offset: 0x0000536C
			// (set) Token: 0x06000527 RID: 1319 RVA: 0x0000715B File Offset: 0x0000535B
			public ParticleSystem.MinMaxCurve strengthZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetStrengthZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700016D RID: 365
			// (get) Token: 0x06000529 RID: 1321 RVA: 0x00007198 File Offset: 0x00005398
			// (set) Token: 0x0600052A RID: 1322 RVA: 0x000071B8 File Offset: 0x000053B8
			public float strengthZMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetStrengthZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700016E RID: 366
			// (get) Token: 0x0600052B RID: 1323 RVA: 0x000071C8 File Offset: 0x000053C8
			// (set) Token: 0x0600052C RID: 1324 RVA: 0x000071E8 File Offset: 0x000053E8
			public float frequency
			{
				get
				{
					return ParticleSystem.NoiseModule.GetFrequency(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetFrequency(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700016F RID: 367
			// (get) Token: 0x0600052D RID: 1325 RVA: 0x000071F8 File Offset: 0x000053F8
			// (set) Token: 0x0600052E RID: 1326 RVA: 0x00007218 File Offset: 0x00005418
			public bool damping
			{
				get
				{
					return ParticleSystem.NoiseModule.GetDamping(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetDamping(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000170 RID: 368
			// (get) Token: 0x0600052F RID: 1327 RVA: 0x00007228 File Offset: 0x00005428
			// (set) Token: 0x06000530 RID: 1328 RVA: 0x00007248 File Offset: 0x00005448
			public int octaveCount
			{
				get
				{
					return ParticleSystem.NoiseModule.GetOctaveCount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetOctaveCount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000171 RID: 369
			// (get) Token: 0x06000531 RID: 1329 RVA: 0x00007258 File Offset: 0x00005458
			// (set) Token: 0x06000532 RID: 1330 RVA: 0x00007278 File Offset: 0x00005478
			public float octaveMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetOctaveMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetOctaveMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000172 RID: 370
			// (get) Token: 0x06000533 RID: 1331 RVA: 0x00007288 File Offset: 0x00005488
			// (set) Token: 0x06000534 RID: 1332 RVA: 0x000072A8 File Offset: 0x000054A8
			public float octaveScale
			{
				get
				{
					return ParticleSystem.NoiseModule.GetOctaveScale(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetOctaveScale(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000173 RID: 371
			// (get) Token: 0x06000535 RID: 1333 RVA: 0x000072B8 File Offset: 0x000054B8
			// (set) Token: 0x06000536 RID: 1334 RVA: 0x000072D8 File Offset: 0x000054D8
			public ParticleSystemNoiseQuality quality
			{
				get
				{
					return (ParticleSystemNoiseQuality)ParticleSystem.NoiseModule.GetQuality(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetQuality(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x17000174 RID: 372
			// (get) Token: 0x06000538 RID: 1336 RVA: 0x000072F8 File Offset: 0x000054F8
			// (set) Token: 0x06000537 RID: 1335 RVA: 0x000072E7 File Offset: 0x000054E7
			public ParticleSystem.MinMaxCurve scrollSpeed
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetScrollSpeed(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetScrollSpeed(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000175 RID: 373
			// (get) Token: 0x06000539 RID: 1337 RVA: 0x00007324 File Offset: 0x00005524
			// (set) Token: 0x0600053A RID: 1338 RVA: 0x00007344 File Offset: 0x00005544
			public float scrollSpeedMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetScrollSpeedMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetScrollSpeedMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000176 RID: 374
			// (get) Token: 0x0600053B RID: 1339 RVA: 0x00007354 File Offset: 0x00005554
			// (set) Token: 0x0600053C RID: 1340 RVA: 0x00007374 File Offset: 0x00005574
			public bool remapEnabled
			{
				get
				{
					return ParticleSystem.NoiseModule.GetRemapEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000177 RID: 375
			// (get) Token: 0x0600053E RID: 1342 RVA: 0x00007394 File Offset: 0x00005594
			// (set) Token: 0x0600053D RID: 1341 RVA: 0x00007383 File Offset: 0x00005583
			public ParticleSystem.MinMaxCurve remap
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetRemapX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000178 RID: 376
			// (get) Token: 0x0600053F RID: 1343 RVA: 0x000073C0 File Offset: 0x000055C0
			// (set) Token: 0x06000540 RID: 1344 RVA: 0x000073E0 File Offset: 0x000055E0
			public float remapMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetRemapXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000179 RID: 377
			// (get) Token: 0x06000542 RID: 1346 RVA: 0x000073F0 File Offset: 0x000055F0
			// (set) Token: 0x06000541 RID: 1345 RVA: 0x00007383 File Offset: 0x00005583
			public ParticleSystem.MinMaxCurve remapX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetRemapX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700017A RID: 378
			// (get) Token: 0x06000543 RID: 1347 RVA: 0x0000741C File Offset: 0x0000561C
			// (set) Token: 0x06000544 RID: 1348 RVA: 0x000073E0 File Offset: 0x000055E0
			public float remapXMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetRemapXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700017B RID: 379
			// (get) Token: 0x06000546 RID: 1350 RVA: 0x0000744C File Offset: 0x0000564C
			// (set) Token: 0x06000545 RID: 1349 RVA: 0x0000743C File Offset: 0x0000563C
			public ParticleSystem.MinMaxCurve remapY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetRemapY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700017C RID: 380
			// (get) Token: 0x06000547 RID: 1351 RVA: 0x00007478 File Offset: 0x00005678
			// (set) Token: 0x06000548 RID: 1352 RVA: 0x00007498 File Offset: 0x00005698
			public float remapYMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetRemapYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700017D RID: 381
			// (get) Token: 0x0600054A RID: 1354 RVA: 0x000074B8 File Offset: 0x000056B8
			// (set) Token: 0x06000549 RID: 1353 RVA: 0x000074A7 File Offset: 0x000056A7
			public ParticleSystem.MinMaxCurve remapZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetRemapZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700017E RID: 382
			// (get) Token: 0x0600054B RID: 1355 RVA: 0x000074E4 File Offset: 0x000056E4
			// (set) Token: 0x0600054C RID: 1356 RVA: 0x00007504 File Offset: 0x00005704
			public float remapZMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetRemapZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700017F RID: 383
			// (get) Token: 0x0600054E RID: 1358 RVA: 0x00007524 File Offset: 0x00005724
			// (set) Token: 0x0600054D RID: 1357 RVA: 0x00007513 File Offset: 0x00005713
			public ParticleSystem.MinMaxCurve positionAmount
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetPositionAmount(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetPositionAmount(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000180 RID: 384
			// (get) Token: 0x06000550 RID: 1360 RVA: 0x00007560 File Offset: 0x00005760
			// (set) Token: 0x0600054F RID: 1359 RVA: 0x0000754F File Offset: 0x0000574F
			public ParticleSystem.MinMaxCurve rotationAmount
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetRotationAmount(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetRotationAmount(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000181 RID: 385
			// (get) Token: 0x06000552 RID: 1362 RVA: 0x0000759C File Offset: 0x0000579C
			// (set) Token: 0x06000551 RID: 1361 RVA: 0x0000758B File Offset: 0x0000578B
			public ParticleSystem.MinMaxCurve sizeAmount
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetSizeAmount(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetSizeAmount(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x06000553 RID: 1363
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06000554 RID: 1364
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06000555 RID: 1365
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x06000556 RID: 1366
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x06000557 RID: 1367
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000558 RID: 1368
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStrengthX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000559 RID: 1369
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600055A RID: 1370
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStrengthY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600055B RID: 1371
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600055C RID: 1372
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStrengthZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600055D RID: 1373
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthXMultiplier(ParticleSystem system, float value);

			// Token: 0x0600055E RID: 1374
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStrengthXMultiplier(ParticleSystem system);

			// Token: 0x0600055F RID: 1375
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthYMultiplier(ParticleSystem system, float value);

			// Token: 0x06000560 RID: 1376
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStrengthYMultiplier(ParticleSystem system);

			// Token: 0x06000561 RID: 1377
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthZMultiplier(ParticleSystem system, float value);

			// Token: 0x06000562 RID: 1378
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStrengthZMultiplier(ParticleSystem system);

			// Token: 0x06000563 RID: 1379
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetFrequency(ParticleSystem system, float value);

			// Token: 0x06000564 RID: 1380
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetFrequency(ParticleSystem system);

			// Token: 0x06000565 RID: 1381
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDamping(ParticleSystem system, bool value);

			// Token: 0x06000566 RID: 1382
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetDamping(ParticleSystem system);

			// Token: 0x06000567 RID: 1383
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOctaveCount(ParticleSystem system, int value);

			// Token: 0x06000568 RID: 1384
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetOctaveCount(ParticleSystem system);

			// Token: 0x06000569 RID: 1385
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOctaveMultiplier(ParticleSystem system, float value);

			// Token: 0x0600056A RID: 1386
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetOctaveMultiplier(ParticleSystem system);

			// Token: 0x0600056B RID: 1387
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOctaveScale(ParticleSystem system, float value);

			// Token: 0x0600056C RID: 1388
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetOctaveScale(ParticleSystem system);

			// Token: 0x0600056D RID: 1389
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetQuality(ParticleSystem system, int value);

			// Token: 0x0600056E RID: 1390
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetQuality(ParticleSystem system);

			// Token: 0x0600056F RID: 1391
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetScrollSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000570 RID: 1392
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetScrollSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000571 RID: 1393
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetScrollSpeedMultiplier(ParticleSystem system, float value);

			// Token: 0x06000572 RID: 1394
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetScrollSpeedMultiplier(ParticleSystem system);

			// Token: 0x06000573 RID: 1395
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapEnabled(ParticleSystem system, bool value);

			// Token: 0x06000574 RID: 1396
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetRemapEnabled(ParticleSystem system);

			// Token: 0x06000575 RID: 1397
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000576 RID: 1398
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRemapX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000577 RID: 1399
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000578 RID: 1400
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRemapY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000579 RID: 1401
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600057A RID: 1402
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRemapZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600057B RID: 1403
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapXMultiplier(ParticleSystem system, float value);

			// Token: 0x0600057C RID: 1404
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRemapXMultiplier(ParticleSystem system);

			// Token: 0x0600057D RID: 1405
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapYMultiplier(ParticleSystem system, float value);

			// Token: 0x0600057E RID: 1406
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRemapYMultiplier(ParticleSystem system);

			// Token: 0x0600057F RID: 1407
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapZMultiplier(ParticleSystem system, float value);

			// Token: 0x06000580 RID: 1408
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRemapZMultiplier(ParticleSystem system);

			// Token: 0x06000581 RID: 1409
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetPositionAmount(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000582 RID: 1410
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetPositionAmount(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000583 RID: 1411
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRotationAmount(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000584 RID: 1412
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRotationAmount(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000585 RID: 1413
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSizeAmount(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000586 RID: 1414
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetSizeAmount(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x04000042 RID: 66
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200001C RID: 28
		public struct TriggerModule
		{
			// Token: 0x06000587 RID: 1415 RVA: 0x000075C7 File Offset: 0x000057C7
			internal TriggerModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000182 RID: 386
			// (get) Token: 0x06000589 RID: 1417 RVA: 0x000075E0 File Offset: 0x000057E0
			// (set) Token: 0x06000588 RID: 1416 RVA: 0x000075D1 File Offset: 0x000057D1
			public bool enabled
			{
				get
				{
					return ParticleSystem.TriggerModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000183 RID: 387
			// (get) Token: 0x0600058B RID: 1419 RVA: 0x00007610 File Offset: 0x00005810
			// (set) Token: 0x0600058A RID: 1418 RVA: 0x00007600 File Offset: 0x00005800
			public ParticleSystemOverlapAction inside
			{
				get
				{
					return ParticleSystem.TriggerModule.GetInside(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetInside(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000184 RID: 388
			// (get) Token: 0x0600058D RID: 1421 RVA: 0x00007640 File Offset: 0x00005840
			// (set) Token: 0x0600058C RID: 1420 RVA: 0x00007630 File Offset: 0x00005830
			public ParticleSystemOverlapAction outside
			{
				get
				{
					return ParticleSystem.TriggerModule.GetOutside(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetOutside(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000185 RID: 389
			// (get) Token: 0x0600058F RID: 1423 RVA: 0x00007670 File Offset: 0x00005870
			// (set) Token: 0x0600058E RID: 1422 RVA: 0x00007660 File Offset: 0x00005860
			public ParticleSystemOverlapAction enter
			{
				get
				{
					return ParticleSystem.TriggerModule.GetEnter(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetEnter(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000186 RID: 390
			// (get) Token: 0x06000591 RID: 1425 RVA: 0x000076A0 File Offset: 0x000058A0
			// (set) Token: 0x06000590 RID: 1424 RVA: 0x00007690 File Offset: 0x00005890
			public ParticleSystemOverlapAction exit
			{
				get
				{
					return ParticleSystem.TriggerModule.GetExit(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetExit(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000187 RID: 391
			// (get) Token: 0x06000592 RID: 1426 RVA: 0x000076C0 File Offset: 0x000058C0
			// (set) Token: 0x06000593 RID: 1427 RVA: 0x000076E0 File Offset: 0x000058E0
			public float radiusScale
			{
				get
				{
					return ParticleSystem.TriggerModule.GetRadiusScale(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetRadiusScale(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06000594 RID: 1428 RVA: 0x000076EF File Offset: 0x000058EF
			public void SetCollider(int index, Component collider)
			{
				ParticleSystem.TriggerModule.SetCollider(this.m_ParticleSystem, index, collider);
			}

			// Token: 0x06000595 RID: 1429 RVA: 0x00007700 File Offset: 0x00005900
			public Component GetCollider(int index)
			{
				return ParticleSystem.TriggerModule.GetCollider(this.m_ParticleSystem, index);
			}

			// Token: 0x17000188 RID: 392
			// (get) Token: 0x06000596 RID: 1430 RVA: 0x00007724 File Offset: 0x00005924
			public int maxColliderCount
			{
				get
				{
					return ParticleSystem.TriggerModule.GetMaxColliderCount(this.m_ParticleSystem);
				}
			}

			// Token: 0x06000597 RID: 1431
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06000598 RID: 1432
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06000599 RID: 1433
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetInside(ParticleSystem system, ParticleSystemOverlapAction value);

			// Token: 0x0600059A RID: 1434
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemOverlapAction GetInside(ParticleSystem system);

			// Token: 0x0600059B RID: 1435
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOutside(ParticleSystem system, ParticleSystemOverlapAction value);

			// Token: 0x0600059C RID: 1436
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemOverlapAction GetOutside(ParticleSystem system);

			// Token: 0x0600059D RID: 1437
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnter(ParticleSystem system, ParticleSystemOverlapAction value);

			// Token: 0x0600059E RID: 1438
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemOverlapAction GetEnter(ParticleSystem system);

			// Token: 0x0600059F RID: 1439
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetExit(ParticleSystem system, ParticleSystemOverlapAction value);

			// Token: 0x060005A0 RID: 1440
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemOverlapAction GetExit(ParticleSystem system);

			// Token: 0x060005A1 RID: 1441
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadiusScale(ParticleSystem system, float value);

			// Token: 0x060005A2 RID: 1442
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRadiusScale(ParticleSystem system);

			// Token: 0x060005A3 RID: 1443
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCollider(ParticleSystem system, int index, Component collider);

			// Token: 0x060005A4 RID: 1444
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Component GetCollider(ParticleSystem system, int index);

			// Token: 0x060005A5 RID: 1445
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMaxColliderCount(ParticleSystem system);

			// Token: 0x04000043 RID: 67
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200001D RID: 29
		public struct LightsModule
		{
			// Token: 0x060005A6 RID: 1446 RVA: 0x00007744 File Offset: 0x00005944
			internal LightsModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000189 RID: 393
			// (get) Token: 0x060005A8 RID: 1448 RVA: 0x00007760 File Offset: 0x00005960
			// (set) Token: 0x060005A7 RID: 1447 RVA: 0x0000774E File Offset: 0x0000594E
			public bool enabled
			{
				get
				{
					return ParticleSystem.LightsModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700018A RID: 394
			// (get) Token: 0x060005A9 RID: 1449 RVA: 0x00007780 File Offset: 0x00005980
			// (set) Token: 0x060005AA RID: 1450 RVA: 0x000077A0 File Offset: 0x000059A0
			public float ratio
			{
				get
				{
					return ParticleSystem.LightsModule.GetRatio(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetRatio(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700018B RID: 395
			// (get) Token: 0x060005AC RID: 1452 RVA: 0x000077C0 File Offset: 0x000059C0
			// (set) Token: 0x060005AB RID: 1451 RVA: 0x000077AF File Offset: 0x000059AF
			public bool useRandomDistribution
			{
				get
				{
					return ParticleSystem.LightsModule.GetUseRandomDistribution(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetUseRandomDistribution(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700018C RID: 396
			// (get) Token: 0x060005AE RID: 1454 RVA: 0x000077F0 File Offset: 0x000059F0
			// (set) Token: 0x060005AD RID: 1453 RVA: 0x000077E0 File Offset: 0x000059E0
			public Light light
			{
				get
				{
					return ParticleSystem.LightsModule.GetLightPrefab(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetLightPrefab(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700018D RID: 397
			// (get) Token: 0x060005B0 RID: 1456 RVA: 0x00007820 File Offset: 0x00005A20
			// (set) Token: 0x060005AF RID: 1455 RVA: 0x00007810 File Offset: 0x00005A10
			public bool useParticleColor
			{
				get
				{
					return ParticleSystem.LightsModule.GetUseParticleColor(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetUseParticleColor(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700018E RID: 398
			// (get) Token: 0x060005B2 RID: 1458 RVA: 0x00007850 File Offset: 0x00005A50
			// (set) Token: 0x060005B1 RID: 1457 RVA: 0x00007840 File Offset: 0x00005A40
			public bool sizeAffectsRange
			{
				get
				{
					return ParticleSystem.LightsModule.GetSizeAffectsRange(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetSizeAffectsRange(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700018F RID: 399
			// (get) Token: 0x060005B4 RID: 1460 RVA: 0x00007880 File Offset: 0x00005A80
			// (set) Token: 0x060005B3 RID: 1459 RVA: 0x00007870 File Offset: 0x00005A70
			public bool alphaAffectsIntensity
			{
				get
				{
					return ParticleSystem.LightsModule.GetAlphaAffectsIntensity(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetAlphaAffectsIntensity(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000190 RID: 400
			// (get) Token: 0x060005B6 RID: 1462 RVA: 0x000078B0 File Offset: 0x00005AB0
			// (set) Token: 0x060005B5 RID: 1461 RVA: 0x000078A0 File Offset: 0x00005AA0
			public ParticleSystem.MinMaxCurve range
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LightsModule.GetRange(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LightsModule.SetRange(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000191 RID: 401
			// (get) Token: 0x060005B7 RID: 1463 RVA: 0x000078DC File Offset: 0x00005ADC
			// (set) Token: 0x060005B8 RID: 1464 RVA: 0x000078FC File Offset: 0x00005AFC
			public float rangeMultiplier
			{
				get
				{
					return ParticleSystem.LightsModule.GetRangeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetRangeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000192 RID: 402
			// (get) Token: 0x060005BA RID: 1466 RVA: 0x0000791C File Offset: 0x00005B1C
			// (set) Token: 0x060005B9 RID: 1465 RVA: 0x0000790B File Offset: 0x00005B0B
			public ParticleSystem.MinMaxCurve intensity
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LightsModule.GetIntensity(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LightsModule.SetIntensity(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000193 RID: 403
			// (get) Token: 0x060005BB RID: 1467 RVA: 0x00007948 File Offset: 0x00005B48
			// (set) Token: 0x060005BC RID: 1468 RVA: 0x00007968 File Offset: 0x00005B68
			public float intensityMultiplier
			{
				get
				{
					return ParticleSystem.LightsModule.GetIntensityMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetIntensityMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000194 RID: 404
			// (get) Token: 0x060005BE RID: 1470 RVA: 0x00007988 File Offset: 0x00005B88
			// (set) Token: 0x060005BD RID: 1469 RVA: 0x00007977 File Offset: 0x00005B77
			public int maxLights
			{
				get
				{
					return ParticleSystem.LightsModule.GetMaxLights(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetMaxLights(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060005BF RID: 1471
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060005C0 RID: 1472
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060005C1 RID: 1473
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRatio(ParticleSystem system, float value);

			// Token: 0x060005C2 RID: 1474
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRatio(ParticleSystem system);

			// Token: 0x060005C3 RID: 1475
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseRandomDistribution(ParticleSystem system, bool value);

			// Token: 0x060005C4 RID: 1476
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseRandomDistribution(ParticleSystem system);

			// Token: 0x060005C5 RID: 1477
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLightPrefab(ParticleSystem system, Light value);

			// Token: 0x060005C6 RID: 1478
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Light GetLightPrefab(ParticleSystem system);

			// Token: 0x060005C7 RID: 1479
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseParticleColor(ParticleSystem system, bool value);

			// Token: 0x060005C8 RID: 1480
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseParticleColor(ParticleSystem system);

			// Token: 0x060005C9 RID: 1481
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSizeAffectsRange(ParticleSystem system, bool value);

			// Token: 0x060005CA RID: 1482
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSizeAffectsRange(ParticleSystem system);

			// Token: 0x060005CB RID: 1483
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetAlphaAffectsIntensity(ParticleSystem system, bool value);

			// Token: 0x060005CC RID: 1484
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetAlphaAffectsIntensity(ParticleSystem system);

			// Token: 0x060005CD RID: 1485
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRange(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060005CE RID: 1486
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRange(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060005CF RID: 1487
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRangeMultiplier(ParticleSystem system, float value);

			// Token: 0x060005D0 RID: 1488
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRangeMultiplier(ParticleSystem system);

			// Token: 0x060005D1 RID: 1489
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetIntensity(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060005D2 RID: 1490
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetIntensity(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060005D3 RID: 1491
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetIntensityMultiplier(ParticleSystem system, float value);

			// Token: 0x060005D4 RID: 1492
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetIntensityMultiplier(ParticleSystem system);

			// Token: 0x060005D5 RID: 1493
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMaxLights(ParticleSystem system, int value);

			// Token: 0x060005D6 RID: 1494
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMaxLights(ParticleSystem system);

			// Token: 0x04000044 RID: 68
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200001E RID: 30
		public struct TrailModule
		{
			// Token: 0x060005D7 RID: 1495 RVA: 0x000079A8 File Offset: 0x00005BA8
			internal TrailModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000195 RID: 405
			// (get) Token: 0x060005D9 RID: 1497 RVA: 0x000079C4 File Offset: 0x00005BC4
			// (set) Token: 0x060005D8 RID: 1496 RVA: 0x000079B2 File Offset: 0x00005BB2
			public bool enabled
			{
				get
				{
					return ParticleSystem.TrailModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000196 RID: 406
			// (get) Token: 0x060005DA RID: 1498 RVA: 0x000079E4 File Offset: 0x00005BE4
			// (set) Token: 0x060005DB RID: 1499 RVA: 0x00007A04 File Offset: 0x00005C04
			public ParticleSystemTrailMode mode
			{
				get
				{
					return ParticleSystem.TrailModule.GetMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000197 RID: 407
			// (get) Token: 0x060005DC RID: 1500 RVA: 0x00007A14 File Offset: 0x00005C14
			// (set) Token: 0x060005DD RID: 1501 RVA: 0x00007A34 File Offset: 0x00005C34
			public float ratio
			{
				get
				{
					return ParticleSystem.TrailModule.GetRatio(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetRatio(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000198 RID: 408
			// (get) Token: 0x060005DF RID: 1503 RVA: 0x00007A54 File Offset: 0x00005C54
			// (set) Token: 0x060005DE RID: 1502 RVA: 0x00007A43 File Offset: 0x00005C43
			public ParticleSystem.MinMaxCurve lifetime
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.TrailModule.GetLifetime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TrailModule.SetLifetime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000199 RID: 409
			// (get) Token: 0x060005E0 RID: 1504 RVA: 0x00007A80 File Offset: 0x00005C80
			// (set) Token: 0x060005E1 RID: 1505 RVA: 0x00007AA0 File Offset: 0x00005CA0
			public float lifetimeMultiplier
			{
				get
				{
					return ParticleSystem.TrailModule.GetLifetimeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetLifetimeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700019A RID: 410
			// (get) Token: 0x060005E3 RID: 1507 RVA: 0x00007AC0 File Offset: 0x00005CC0
			// (set) Token: 0x060005E2 RID: 1506 RVA: 0x00007AAF File Offset: 0x00005CAF
			public float minVertexDistance
			{
				get
				{
					return ParticleSystem.TrailModule.GetMinVertexDistance(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetMinVertexDistance(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700019B RID: 411
			// (get) Token: 0x060005E4 RID: 1508 RVA: 0x00007AE0 File Offset: 0x00005CE0
			// (set) Token: 0x060005E5 RID: 1509 RVA: 0x00007B00 File Offset: 0x00005D00
			public ParticleSystemTrailTextureMode textureMode
			{
				get
				{
					return ParticleSystem.TrailModule.GetTextureMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetTextureMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700019C RID: 412
			// (get) Token: 0x060005E7 RID: 1511 RVA: 0x00007B20 File Offset: 0x00005D20
			// (set) Token: 0x060005E6 RID: 1510 RVA: 0x00007B0F File Offset: 0x00005D0F
			public bool worldSpace
			{
				get
				{
					return ParticleSystem.TrailModule.GetWorldSpace(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetWorldSpace(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700019D RID: 413
			// (get) Token: 0x060005E9 RID: 1513 RVA: 0x00007B50 File Offset: 0x00005D50
			// (set) Token: 0x060005E8 RID: 1512 RVA: 0x00007B40 File Offset: 0x00005D40
			public bool dieWithParticles
			{
				get
				{
					return ParticleSystem.TrailModule.GetDieWithParticles(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetDieWithParticles(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700019E RID: 414
			// (get) Token: 0x060005EB RID: 1515 RVA: 0x00007B80 File Offset: 0x00005D80
			// (set) Token: 0x060005EA RID: 1514 RVA: 0x00007B70 File Offset: 0x00005D70
			public bool sizeAffectsWidth
			{
				get
				{
					return ParticleSystem.TrailModule.GetSizeAffectsWidth(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetSizeAffectsWidth(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700019F RID: 415
			// (get) Token: 0x060005ED RID: 1517 RVA: 0x00007BB0 File Offset: 0x00005DB0
			// (set) Token: 0x060005EC RID: 1516 RVA: 0x00007BA0 File Offset: 0x00005DA0
			public bool sizeAffectsLifetime
			{
				get
				{
					return ParticleSystem.TrailModule.GetSizeAffectsLifetime(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetSizeAffectsLifetime(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170001A0 RID: 416
			// (get) Token: 0x060005EF RID: 1519 RVA: 0x00007BE0 File Offset: 0x00005DE0
			// (set) Token: 0x060005EE RID: 1518 RVA: 0x00007BD0 File Offset: 0x00005DD0
			public bool inheritParticleColor
			{
				get
				{
					return ParticleSystem.TrailModule.GetInheritParticleColor(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetInheritParticleColor(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170001A1 RID: 417
			// (get) Token: 0x060005F1 RID: 1521 RVA: 0x00007C10 File Offset: 0x00005E10
			// (set) Token: 0x060005F0 RID: 1520 RVA: 0x00007C00 File Offset: 0x00005E00
			public ParticleSystem.MinMaxGradient colorOverLifetime
			{
				get
				{
					ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
					ParticleSystem.TrailModule.GetColorOverLifetime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TrailModule.SetColorOverLifetime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170001A2 RID: 418
			// (get) Token: 0x060005F3 RID: 1523 RVA: 0x00007C4C File Offset: 0x00005E4C
			// (set) Token: 0x060005F2 RID: 1522 RVA: 0x00007C3B File Offset: 0x00005E3B
			public ParticleSystem.MinMaxCurve widthOverTrail
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.TrailModule.GetWidthOverTrail(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TrailModule.SetWidthOverTrail(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170001A3 RID: 419
			// (get) Token: 0x060005F4 RID: 1524 RVA: 0x00007C78 File Offset: 0x00005E78
			// (set) Token: 0x060005F5 RID: 1525 RVA: 0x00007C98 File Offset: 0x00005E98
			public float widthOverTrailMultiplier
			{
				get
				{
					return ParticleSystem.TrailModule.GetWidthOverTrailMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetWidthOverTrailMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170001A4 RID: 420
			// (get) Token: 0x060005F7 RID: 1527 RVA: 0x00007CB8 File Offset: 0x00005EB8
			// (set) Token: 0x060005F6 RID: 1526 RVA: 0x00007CA7 File Offset: 0x00005EA7
			public ParticleSystem.MinMaxGradient colorOverTrail
			{
				get
				{
					ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
					ParticleSystem.TrailModule.GetColorOverTrail(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TrailModule.SetColorOverTrail(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170001A5 RID: 421
			// (get) Token: 0x060005F9 RID: 1529 RVA: 0x00007CF4 File Offset: 0x00005EF4
			// (set) Token: 0x060005F8 RID: 1528 RVA: 0x00007CE3 File Offset: 0x00005EE3
			public bool generateLightingData
			{
				get
				{
					return ParticleSystem.TrailModule.GetGenerateLightingData(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetGenerateLightingData(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170001A6 RID: 422
			// (get) Token: 0x060005FA RID: 1530 RVA: 0x00007D14 File Offset: 0x00005F14
			// (set) Token: 0x060005FB RID: 1531 RVA: 0x00007D34 File Offset: 0x00005F34
			public int ribbonCount
			{
				get
				{
					return ParticleSystem.TrailModule.GetRibbonCount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetRibbonCount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170001A7 RID: 423
			// (get) Token: 0x060005FD RID: 1533 RVA: 0x00007D54 File Offset: 0x00005F54
			// (set) Token: 0x060005FC RID: 1532 RVA: 0x00007D43 File Offset: 0x00005F43
			public float shadowBias
			{
				get
				{
					return ParticleSystem.TrailModule.GetShadowBias(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetShadowBias(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170001A8 RID: 424
			// (get) Token: 0x060005FF RID: 1535 RVA: 0x00007D84 File Offset: 0x00005F84
			// (set) Token: 0x060005FE RID: 1534 RVA: 0x00007D74 File Offset: 0x00005F74
			public bool splitSubEmitterRibbons
			{
				get
				{
					return ParticleSystem.TrailModule.GetSplitSubEmitterRibbons(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetSplitSubEmitterRibbons(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170001A9 RID: 425
			// (get) Token: 0x06000601 RID: 1537 RVA: 0x00007DB4 File Offset: 0x00005FB4
			// (set) Token: 0x06000600 RID: 1536 RVA: 0x00007DA4 File Offset: 0x00005FA4
			public bool attachRibbonsToTransform
			{
				get
				{
					return ParticleSystem.TrailModule.GetAttachRibbonsToTransform(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetAttachRibbonsToTransform(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06000602 RID: 1538
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06000603 RID: 1539
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06000604 RID: 1540
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMode(ParticleSystem system, ParticleSystemTrailMode value);

			// Token: 0x06000605 RID: 1541
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemTrailMode GetMode(ParticleSystem system);

			// Token: 0x06000606 RID: 1542
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRatio(ParticleSystem system, float value);

			// Token: 0x06000607 RID: 1543
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRatio(ParticleSystem system);

			// Token: 0x06000608 RID: 1544
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000609 RID: 1545
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600060A RID: 1546
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLifetimeMultiplier(ParticleSystem system, float value);

			// Token: 0x0600060B RID: 1547
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetLifetimeMultiplier(ParticleSystem system);

			// Token: 0x0600060C RID: 1548
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMinVertexDistance(ParticleSystem system, float value);

			// Token: 0x0600060D RID: 1549
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMinVertexDistance(ParticleSystem system);

			// Token: 0x0600060E RID: 1550
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetTextureMode(ParticleSystem system, ParticleSystemTrailTextureMode value);

			// Token: 0x0600060F RID: 1551
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemTrailTextureMode GetTextureMode(ParticleSystem system);

			// Token: 0x06000610 RID: 1552
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWorldSpace(ParticleSystem system, bool value);

			// Token: 0x06000611 RID: 1553
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetWorldSpace(ParticleSystem system);

			// Token: 0x06000612 RID: 1554
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDieWithParticles(ParticleSystem system, bool value);

			// Token: 0x06000613 RID: 1555
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetDieWithParticles(ParticleSystem system);

			// Token: 0x06000614 RID: 1556
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSizeAffectsWidth(ParticleSystem system, bool value);

			// Token: 0x06000615 RID: 1557
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSizeAffectsWidth(ParticleSystem system);

			// Token: 0x06000616 RID: 1558
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSizeAffectsLifetime(ParticleSystem system, bool value);

			// Token: 0x06000617 RID: 1559
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSizeAffectsLifetime(ParticleSystem system);

			// Token: 0x06000618 RID: 1560
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetInheritParticleColor(ParticleSystem system, bool value);

			// Token: 0x06000619 RID: 1561
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetInheritParticleColor(ParticleSystem system);

			// Token: 0x0600061A RID: 1562
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetColorOverLifetime(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0600061B RID: 1563
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetColorOverLifetime(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0600061C RID: 1564
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWidthOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600061D RID: 1565
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetWidthOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600061E RID: 1566
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWidthOverTrailMultiplier(ParticleSystem system, float value);

			// Token: 0x0600061F RID: 1567
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetWidthOverTrailMultiplier(ParticleSystem system);

			// Token: 0x06000620 RID: 1568
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetColorOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x06000621 RID: 1569
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetColorOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x06000622 RID: 1570
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetGenerateLightingData(ParticleSystem system, bool value);

			// Token: 0x06000623 RID: 1571
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetGenerateLightingData(ParticleSystem system);

			// Token: 0x06000624 RID: 1572
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRibbonCount(ParticleSystem system, int value);

			// Token: 0x06000625 RID: 1573
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetRibbonCount(ParticleSystem system);

			// Token: 0x06000626 RID: 1574
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetShadowBias(ParticleSystem system, float value);

			// Token: 0x06000627 RID: 1575
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetShadowBias(ParticleSystem system);

			// Token: 0x06000628 RID: 1576
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSplitSubEmitterRibbons(ParticleSystem system, bool value);

			// Token: 0x06000629 RID: 1577
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSplitSubEmitterRibbons(ParticleSystem system);

			// Token: 0x0600062A RID: 1578
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetAttachRibbonsToTransform(ParticleSystem system, bool value);

			// Token: 0x0600062B RID: 1579
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetAttachRibbonsToTransform(ParticleSystem system);

			// Token: 0x04000045 RID: 69
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200001F RID: 31
		public struct CustomDataModule
		{
			// Token: 0x0600062C RID: 1580 RVA: 0x00007DD4 File Offset: 0x00005FD4
			internal CustomDataModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x170001AA RID: 426
			// (get) Token: 0x0600062E RID: 1582 RVA: 0x00007DF0 File Offset: 0x00005FF0
			// (set) Token: 0x0600062D RID: 1581 RVA: 0x00007DDE File Offset: 0x00005FDE
			public bool enabled
			{
				get
				{
					return ParticleSystem.CustomDataModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CustomDataModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x0600062F RID: 1583 RVA: 0x00007E10 File Offset: 0x00006010
			public void SetMode(ParticleSystemCustomData stream, ParticleSystemCustomDataMode mode)
			{
				ParticleSystem.CustomDataModule.SetMode(this.m_ParticleSystem, (int)stream, mode);
			}

			// Token: 0x06000630 RID: 1584 RVA: 0x00007E20 File Offset: 0x00006020
			public ParticleSystemCustomDataMode GetMode(ParticleSystemCustomData stream)
			{
				return ParticleSystem.CustomDataModule.GetMode(this.m_ParticleSystem, (int)stream);
			}

			// Token: 0x06000631 RID: 1585 RVA: 0x00007E41 File Offset: 0x00006041
			public void SetVectorComponentCount(ParticleSystemCustomData stream, int count)
			{
				ParticleSystem.CustomDataModule.SetVectorComponentCount(this.m_ParticleSystem, (int)stream, count);
			}

			// Token: 0x06000632 RID: 1586 RVA: 0x00007E54 File Offset: 0x00006054
			public int GetVectorComponentCount(ParticleSystemCustomData stream)
			{
				return ParticleSystem.CustomDataModule.GetVectorComponentCount(this.m_ParticleSystem, (int)stream);
			}

			// Token: 0x06000633 RID: 1587 RVA: 0x00007E75 File Offset: 0x00006075
			public void SetVector(ParticleSystemCustomData stream, int component, ParticleSystem.MinMaxCurve curve)
			{
				ParticleSystem.CustomDataModule.SetVector(this.m_ParticleSystem, (int)stream, component, ref curve);
			}

			// Token: 0x06000634 RID: 1588 RVA: 0x00007E88 File Offset: 0x00006088
			public ParticleSystem.MinMaxCurve GetVector(ParticleSystemCustomData stream, int component)
			{
				ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
				ParticleSystem.CustomDataModule.GetVector(this.m_ParticleSystem, (int)stream, component, ref result);
				return result;
			}

			// Token: 0x06000635 RID: 1589 RVA: 0x00007EB5 File Offset: 0x000060B5
			public void SetColor(ParticleSystemCustomData stream, ParticleSystem.MinMaxGradient gradient)
			{
				ParticleSystem.CustomDataModule.SetColor(this.m_ParticleSystem, (int)stream, ref gradient);
			}

			// Token: 0x06000636 RID: 1590 RVA: 0x00007EC8 File Offset: 0x000060C8
			public ParticleSystem.MinMaxGradient GetColor(ParticleSystemCustomData stream)
			{
				ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
				ParticleSystem.CustomDataModule.GetColor(this.m_ParticleSystem, (int)stream, ref result);
				return result;
			}

			// Token: 0x06000637 RID: 1591
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06000638 RID: 1592
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06000639 RID: 1593
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMode(ParticleSystem system, int stream, ParticleSystemCustomDataMode mode);

			// Token: 0x0600063A RID: 1594
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetVectorComponentCount(ParticleSystem system, int stream, int count);

			// Token: 0x0600063B RID: 1595
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetVector(ParticleSystem system, int stream, int component, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600063C RID: 1596
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetColor(ParticleSystem system, int stream, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0600063D RID: 1597
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemCustomDataMode GetMode(ParticleSystem system, int stream);

			// Token: 0x0600063E RID: 1598
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetVectorComponentCount(ParticleSystem system, int stream);

			// Token: 0x0600063F RID: 1599
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetVector(ParticleSystem system, int stream, int component, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06000640 RID: 1600
			[GeneratedByOldBindingsGenerator]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetColor(ParticleSystem system, int stream, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x04000046 RID: 70
			private ParticleSystem m_ParticleSystem;
		}
	}
}
