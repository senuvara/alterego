﻿using System;

namespace UnityEngine
{
	// Token: 0x02000041 RID: 65
	public enum ParticleSystemTrailTextureMode
	{
		// Token: 0x04000117 RID: 279
		Stretch,
		// Token: 0x04000118 RID: 280
		Tile,
		// Token: 0x04000119 RID: 281
		DistributePerSegment,
		// Token: 0x0400011A RID: 282
		RepeatPerSegment
	}
}
