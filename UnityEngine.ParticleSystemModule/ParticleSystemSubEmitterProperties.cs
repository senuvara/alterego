﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003F RID: 63
	[Flags]
	public enum ParticleSystemSubEmitterProperties
	{
		// Token: 0x0400010C RID: 268
		InheritNothing = 0,
		// Token: 0x0400010D RID: 269
		InheritEverything = 31,
		// Token: 0x0400010E RID: 270
		InheritColor = 1,
		// Token: 0x0400010F RID: 271
		InheritSize = 2,
		// Token: 0x04000110 RID: 272
		InheritRotation = 4,
		// Token: 0x04000111 RID: 273
		InheritLifetime = 8,
		// Token: 0x04000112 RID: 274
		InheritDuration = 16
	}
}
