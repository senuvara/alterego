﻿using System;

namespace UnityEngine
{
	// Token: 0x02000036 RID: 54
	public enum ParticleSystemCullingMode
	{
		// Token: 0x040000BD RID: 189
		Automatic,
		// Token: 0x040000BE RID: 190
		PauseAndCatchup,
		// Token: 0x040000BF RID: 191
		Pause,
		// Token: 0x040000C0 RID: 192
		AlwaysSimulate
	}
}
