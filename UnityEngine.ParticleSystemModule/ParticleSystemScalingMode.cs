﻿using System;

namespace UnityEngine
{
	// Token: 0x02000034 RID: 52
	public enum ParticleSystemScalingMode
	{
		// Token: 0x040000B4 RID: 180
		Hierarchy,
		// Token: 0x040000B5 RID: 181
		Local,
		// Token: 0x040000B6 RID: 182
		Shape
	}
}
