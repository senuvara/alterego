﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002E RID: 46
	public enum ParticleSystemAnimationType
	{
		// Token: 0x040000A0 RID: 160
		WholeSheet,
		// Token: 0x040000A1 RID: 161
		SingleRow
	}
}
