﻿using System;

namespace UnityEngine
{
	// Token: 0x02000033 RID: 51
	public enum ParticleSystemStopBehavior
	{
		// Token: 0x040000B1 RID: 177
		StopEmittingAndClear,
		// Token: 0x040000B2 RID: 178
		StopEmitting
	}
}
