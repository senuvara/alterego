﻿using System;
using UnityEngine.SocialPlatforms;

namespace UnityEngine
{
	// Token: 0x0200000A RID: 10
	public static class Social
	{
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000074 RID: 116 RVA: 0x00003640 File Offset: 0x00001840
		// (set) Token: 0x06000075 RID: 117 RVA: 0x0000365A File Offset: 0x0000185A
		public static ISocialPlatform Active
		{
			get
			{
				return ActivePlatform.Instance;
			}
			set
			{
				ActivePlatform.Instance = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000076 RID: 118 RVA: 0x00003664 File Offset: 0x00001864
		public static ILocalUser localUser
		{
			get
			{
				return Social.Active.localUser;
			}
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00003683 File Offset: 0x00001883
		public static void LoadUsers(string[] userIDs, Action<IUserProfile[]> callback)
		{
			Social.Active.LoadUsers(userIDs, callback);
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00003692 File Offset: 0x00001892
		public static void ReportProgress(string achievementID, double progress, Action<bool> callback)
		{
			Social.Active.ReportProgress(achievementID, progress, callback);
		}

		// Token: 0x06000079 RID: 121 RVA: 0x000036A2 File Offset: 0x000018A2
		public static void LoadAchievementDescriptions(Action<IAchievementDescription[]> callback)
		{
			Social.Active.LoadAchievementDescriptions(callback);
		}

		// Token: 0x0600007A RID: 122 RVA: 0x000036B0 File Offset: 0x000018B0
		public static void LoadAchievements(Action<IAchievement[]> callback)
		{
			Social.Active.LoadAchievements(callback);
		}

		// Token: 0x0600007B RID: 123 RVA: 0x000036BE File Offset: 0x000018BE
		public static void ReportScore(long score, string board, Action<bool> callback)
		{
			Social.Active.ReportScore(score, board, callback);
		}

		// Token: 0x0600007C RID: 124 RVA: 0x000036CE File Offset: 0x000018CE
		public static void LoadScores(string leaderboardID, Action<IScore[]> callback)
		{
			Social.Active.LoadScores(leaderboardID, callback);
		}

		// Token: 0x0600007D RID: 125 RVA: 0x000036E0 File Offset: 0x000018E0
		public static ILeaderboard CreateLeaderboard()
		{
			return Social.Active.CreateLeaderboard();
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00003700 File Offset: 0x00001900
		public static IAchievement CreateAchievement()
		{
			return Social.Active.CreateAchievement();
		}

		// Token: 0x0600007F RID: 127 RVA: 0x0000371F File Offset: 0x0000191F
		public static void ShowAchievementsUI()
		{
			Social.Active.ShowAchievementsUI();
		}

		// Token: 0x06000080 RID: 128 RVA: 0x0000372C File Offset: 0x0000192C
		public static void ShowLeaderboardUI()
		{
			Social.Active.ShowLeaderboardUI();
		}
	}
}
