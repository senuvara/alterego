﻿using System;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x02000002 RID: 2
	public class LocalUser : UserProfile, ILocalUser, IUserProfile
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public LocalUser()
		{
			this.m_Friends = new UserProfile[0];
			this.m_Authenticated = false;
			this.m_Underage = false;
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002073 File Offset: 0x00000273
		public void Authenticate(Action<bool> callback)
		{
			ActivePlatform.Instance.Authenticate(this, callback);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002082 File Offset: 0x00000282
		public void Authenticate(Action<bool, string> callback)
		{
			ActivePlatform.Instance.Authenticate(this, callback);
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002091 File Offset: 0x00000291
		public void LoadFriends(Action<bool> callback)
		{
			ActivePlatform.Instance.LoadFriends(this, callback);
		}

		// Token: 0x06000005 RID: 5 RVA: 0x000020A0 File Offset: 0x000002A0
		public void SetFriends(IUserProfile[] friends)
		{
			this.m_Friends = friends;
		}

		// Token: 0x06000006 RID: 6 RVA: 0x000020AA File Offset: 0x000002AA
		public void SetAuthenticated(bool value)
		{
			this.m_Authenticated = value;
		}

		// Token: 0x06000007 RID: 7 RVA: 0x000020B4 File Offset: 0x000002B4
		public void SetUnderage(bool value)
		{
			this.m_Underage = value;
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000008 RID: 8 RVA: 0x000020C0 File Offset: 0x000002C0
		public IUserProfile[] friends
		{
			get
			{
				return this.m_Friends;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000009 RID: 9 RVA: 0x000020DC File Offset: 0x000002DC
		public bool authenticated
		{
			get
			{
				return this.m_Authenticated;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600000A RID: 10 RVA: 0x000020F8 File Offset: 0x000002F8
		public bool underage
		{
			get
			{
				return this.m_Underage;
			}
		}

		// Token: 0x04000001 RID: 1
		private IUserProfile[] m_Friends;

		// Token: 0x04000002 RID: 2
		private bool m_Authenticated;

		// Token: 0x04000003 RID: 3
		private bool m_Underage;
	}
}
