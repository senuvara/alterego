﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x02000004 RID: 4
	public class Achievement : IAchievement
	{
		// Token: 0x06000019 RID: 25 RVA: 0x000022B7 File Offset: 0x000004B7
		public Achievement(string id, double percentCompleted, bool completed, bool hidden, DateTime lastReportedDate)
		{
			this.id = id;
			this.percentCompleted = percentCompleted;
			this.m_Completed = completed;
			this.m_Hidden = hidden;
			this.m_LastReportedDate = lastReportedDate;
		}

		// Token: 0x0600001A RID: 26 RVA: 0x000022E5 File Offset: 0x000004E5
		public Achievement(string id, double percent)
		{
			this.id = id;
			this.percentCompleted = percent;
			this.m_Hidden = false;
			this.m_Completed = false;
			this.m_LastReportedDate = DateTime.MinValue;
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002315 File Offset: 0x00000515
		public Achievement() : this("unknown", 0.0)
		{
		}

		// Token: 0x0600001C RID: 28 RVA: 0x0000232C File Offset: 0x0000052C
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				this.id,
				" - ",
				this.percentCompleted,
				" - ",
				this.completed,
				" - ",
				this.hidden,
				" - ",
				this.lastReportedDate
			});
		}

		// Token: 0x0600001D RID: 29 RVA: 0x000023AE File Offset: 0x000005AE
		public void ReportProgress(Action<bool> callback)
		{
			ActivePlatform.Instance.ReportProgress(this.id, this.percentCompleted, callback);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000023C8 File Offset: 0x000005C8
		public void SetCompleted(bool value)
		{
			this.m_Completed = value;
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000023D2 File Offset: 0x000005D2
		public void SetHidden(bool value)
		{
			this.m_Hidden = value;
		}

		// Token: 0x06000020 RID: 32 RVA: 0x000023DC File Offset: 0x000005DC
		public void SetLastReportedDate(DateTime date)
		{
			this.m_LastReportedDate = date;
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000021 RID: 33 RVA: 0x000023E8 File Offset: 0x000005E8
		// (set) Token: 0x06000022 RID: 34 RVA: 0x00002402 File Offset: 0x00000602
		public string id
		{
			[CompilerGenerated]
			get
			{
				return this.<id>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<id>k__BackingField = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000023 RID: 35 RVA: 0x0000240C File Offset: 0x0000060C
		// (set) Token: 0x06000024 RID: 36 RVA: 0x00002426 File Offset: 0x00000626
		public double percentCompleted
		{
			[CompilerGenerated]
			get
			{
				return this.<percentCompleted>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<percentCompleted>k__BackingField = value;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000025 RID: 37 RVA: 0x00002430 File Offset: 0x00000630
		public bool completed
		{
			get
			{
				return this.m_Completed;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000026 RID: 38 RVA: 0x0000244C File Offset: 0x0000064C
		public bool hidden
		{
			get
			{
				return this.m_Hidden;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000027 RID: 39 RVA: 0x00002468 File Offset: 0x00000668
		public DateTime lastReportedDate
		{
			get
			{
				return this.m_LastReportedDate;
			}
		}

		// Token: 0x04000009 RID: 9
		private bool m_Completed;

		// Token: 0x0400000A RID: 10
		private bool m_Hidden;

		// Token: 0x0400000B RID: 11
		private DateTime m_LastReportedDate;

		// Token: 0x0400000C RID: 12
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <id>k__BackingField;

		// Token: 0x0400000D RID: 13
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private double <percentCompleted>k__BackingField;
	}
}
