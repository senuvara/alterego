﻿using System;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x02000003 RID: 3
	public class UserProfile : IUserProfile
	{
		// Token: 0x0600000B RID: 11 RVA: 0x00002113 File Offset: 0x00000313
		public UserProfile()
		{
			this.m_UserName = "Uninitialized";
			this.m_ID = "0";
			this.m_IsFriend = false;
			this.m_State = UserState.Offline;
			this.m_Image = new Texture2D(32, 32);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x0000214F File Offset: 0x0000034F
		public UserProfile(string name, string id, bool friend) : this(name, id, friend, UserState.Offline, new Texture2D(0, 0))
		{
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00002163 File Offset: 0x00000363
		public UserProfile(string name, string id, bool friend, UserState state, Texture2D image)
		{
			this.m_UserName = name;
			this.m_ID = id;
			this.m_IsFriend = friend;
			this.m_State = state;
			this.m_Image = image;
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00002194 File Offset: 0x00000394
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				this.id,
				" - ",
				this.userName,
				" - ",
				this.isFriend,
				" - ",
				this.state
			});
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000021FA File Offset: 0x000003FA
		public void SetUserName(string name)
		{
			this.m_UserName = name;
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002204 File Offset: 0x00000404
		public void SetUserID(string id)
		{
			this.m_ID = id;
		}

		// Token: 0x06000011 RID: 17 RVA: 0x0000220E File Offset: 0x0000040E
		public void SetImage(Texture2D image)
		{
			this.m_Image = image;
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002218 File Offset: 0x00000418
		public void SetIsFriend(bool value)
		{
			this.m_IsFriend = value;
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00002222 File Offset: 0x00000422
		public void SetState(UserState state)
		{
			this.m_State = state;
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000014 RID: 20 RVA: 0x0000222C File Offset: 0x0000042C
		public string userName
		{
			get
			{
				return this.m_UserName;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000015 RID: 21 RVA: 0x00002248 File Offset: 0x00000448
		public string id
		{
			get
			{
				return this.m_ID;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000016 RID: 22 RVA: 0x00002264 File Offset: 0x00000464
		public bool isFriend
		{
			get
			{
				return this.m_IsFriend;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000017 RID: 23 RVA: 0x00002280 File Offset: 0x00000480
		public UserState state
		{
			get
			{
				return this.m_State;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000018 RID: 24 RVA: 0x0000229C File Offset: 0x0000049C
		public Texture2D image
		{
			get
			{
				return this.m_Image;
			}
		}

		// Token: 0x04000004 RID: 4
		protected string m_UserName;

		// Token: 0x04000005 RID: 5
		protected string m_ID;

		// Token: 0x04000006 RID: 6
		protected bool m_IsFriend;

		// Token: 0x04000007 RID: 7
		protected UserState m_State;

		// Token: 0x04000008 RID: 8
		protected Texture2D m_Image;
	}
}
