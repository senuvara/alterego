﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x02000006 RID: 6
	public class Score : IScore
	{
		// Token: 0x06000033 RID: 51 RVA: 0x00002627 File Offset: 0x00000827
		public Score() : this("unkown", -1L)
		{
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002637 File Offset: 0x00000837
		public Score(string leaderboardID, long value) : this(leaderboardID, value, "0", DateTime.Now, "", -1)
		{
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002652 File Offset: 0x00000852
		public Score(string leaderboardID, long value, string userID, DateTime date, string formattedValue, int rank)
		{
			this.leaderboardID = leaderboardID;
			this.value = value;
			this.m_UserID = userID;
			this.m_Date = date;
			this.m_FormattedValue = formattedValue;
			this.m_Rank = rank;
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002688 File Offset: 0x00000888
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"Rank: '",
				this.m_Rank,
				"' Value: '",
				this.value,
				"' Category: '",
				this.leaderboardID,
				"' PlayerID: '",
				this.m_UserID,
				"' Date: '",
				this.m_Date
			});
		}

		// Token: 0x06000037 RID: 55 RVA: 0x0000270E File Offset: 0x0000090E
		public void ReportScore(Action<bool> callback)
		{
			ActivePlatform.Instance.ReportScore(this.value, this.leaderboardID, callback);
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00002728 File Offset: 0x00000928
		public void SetDate(DateTime date)
		{
			this.m_Date = date;
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00002732 File Offset: 0x00000932
		public void SetFormattedValue(string value)
		{
			this.m_FormattedValue = value;
		}

		// Token: 0x0600003A RID: 58 RVA: 0x0000273C File Offset: 0x0000093C
		public void SetUserID(string userID)
		{
			this.m_UserID = userID;
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00002746 File Offset: 0x00000946
		public void SetRank(int rank)
		{
			this.m_Rank = rank;
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600003C RID: 60 RVA: 0x00002750 File Offset: 0x00000950
		// (set) Token: 0x0600003D RID: 61 RVA: 0x0000276A File Offset: 0x0000096A
		public string leaderboardID
		{
			[CompilerGenerated]
			get
			{
				return this.<leaderboardID>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<leaderboardID>k__BackingField = value;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600003E RID: 62 RVA: 0x00002774 File Offset: 0x00000974
		// (set) Token: 0x0600003F RID: 63 RVA: 0x0000278E File Offset: 0x0000098E
		public long value
		{
			[CompilerGenerated]
			get
			{
				return this.<value>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<value>k__BackingField = value;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000040 RID: 64 RVA: 0x00002798 File Offset: 0x00000998
		public DateTime date
		{
			get
			{
				return this.m_Date;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000041 RID: 65 RVA: 0x000027B4 File Offset: 0x000009B4
		public string formattedValue
		{
			get
			{
				return this.m_FormattedValue;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000042 RID: 66 RVA: 0x000027D0 File Offset: 0x000009D0
		public string userID
		{
			get
			{
				return this.m_UserID;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000043 RID: 67 RVA: 0x000027EC File Offset: 0x000009EC
		public int rank
		{
			get
			{
				return this.m_Rank;
			}
		}

		// Token: 0x04000015 RID: 21
		private DateTime m_Date;

		// Token: 0x04000016 RID: 22
		private string m_FormattedValue;

		// Token: 0x04000017 RID: 23
		private string m_UserID;

		// Token: 0x04000018 RID: 24
		private int m_Rank;

		// Token: 0x04000019 RID: 25
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <leaderboardID>k__BackingField;

		// Token: 0x0400001A RID: 26
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private long <value>k__BackingField;
	}
}
