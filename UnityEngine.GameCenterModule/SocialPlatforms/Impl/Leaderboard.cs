﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x02000007 RID: 7
	public class Leaderboard : ILeaderboard
	{
		// Token: 0x06000044 RID: 68 RVA: 0x00002808 File Offset: 0x00000A08
		public Leaderboard()
		{
			this.id = "Invalid";
			this.range = new Range(1, 10);
			this.userScope = UserScope.Global;
			this.timeScope = TimeScope.AllTime;
			this.m_Loading = false;
			this.m_LocalUserScore = new Score("Invalid", 0L);
			this.m_MaxRange = 0U;
			this.m_Scores = new Score[0];
			this.m_Title = "Invalid";
			this.m_UserIDs = new string[0];
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00002886 File Offset: 0x00000A86
		public void SetUserFilter(string[] userIDs)
		{
			this.m_UserIDs = userIDs;
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00002890 File Offset: 0x00000A90
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"ID: '",
				this.id,
				"' Title: '",
				this.m_Title,
				"' Loading: '",
				this.m_Loading,
				"' Range: [",
				this.range.from,
				",",
				this.range.count,
				"] MaxRange: '",
				this.m_MaxRange,
				"' Scores: '",
				this.m_Scores.Length,
				"' UserScope: '",
				this.userScope,
				"' TimeScope: '",
				this.timeScope,
				"' UserFilter: '",
				this.m_UserIDs.Length
			});
		}

		// Token: 0x06000047 RID: 71 RVA: 0x000029A2 File Offset: 0x00000BA2
		public void LoadScores(Action<bool> callback)
		{
			ActivePlatform.Instance.LoadScores(this, callback);
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000048 RID: 72 RVA: 0x000029B4 File Offset: 0x00000BB4
		public bool loading
		{
			get
			{
				return ActivePlatform.Instance.GetLoading(this);
			}
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000029D4 File Offset: 0x00000BD4
		public void SetLocalUserScore(IScore score)
		{
			this.m_LocalUserScore = score;
		}

		// Token: 0x0600004A RID: 74 RVA: 0x000029DE File Offset: 0x00000BDE
		public void SetMaxRange(uint maxRange)
		{
			this.m_MaxRange = maxRange;
		}

		// Token: 0x0600004B RID: 75 RVA: 0x000029E8 File Offset: 0x00000BE8
		public void SetScores(IScore[] scores)
		{
			this.m_Scores = scores;
		}

		// Token: 0x0600004C RID: 76 RVA: 0x000029F2 File Offset: 0x00000BF2
		public void SetTitle(string title)
		{
			this.m_Title = title;
		}

		// Token: 0x0600004D RID: 77 RVA: 0x000029FC File Offset: 0x00000BFC
		public string[] GetUserFilter()
		{
			return this.m_UserIDs;
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600004E RID: 78 RVA: 0x00002A18 File Offset: 0x00000C18
		// (set) Token: 0x0600004F RID: 79 RVA: 0x00002A32 File Offset: 0x00000C32
		public string id
		{
			[CompilerGenerated]
			get
			{
				return this.<id>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<id>k__BackingField = value;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000050 RID: 80 RVA: 0x00002A3C File Offset: 0x00000C3C
		// (set) Token: 0x06000051 RID: 81 RVA: 0x00002A56 File Offset: 0x00000C56
		public UserScope userScope
		{
			[CompilerGenerated]
			get
			{
				return this.<userScope>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<userScope>k__BackingField = value;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000052 RID: 82 RVA: 0x00002A60 File Offset: 0x00000C60
		// (set) Token: 0x06000053 RID: 83 RVA: 0x00002A7A File Offset: 0x00000C7A
		public Range range
		{
			[CompilerGenerated]
			get
			{
				return this.<range>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<range>k__BackingField = value;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000054 RID: 84 RVA: 0x00002A84 File Offset: 0x00000C84
		// (set) Token: 0x06000055 RID: 85 RVA: 0x00002A9E File Offset: 0x00000C9E
		public TimeScope timeScope
		{
			[CompilerGenerated]
			get
			{
				return this.<timeScope>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<timeScope>k__BackingField = value;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000056 RID: 86 RVA: 0x00002AA8 File Offset: 0x00000CA8
		public IScore localUserScore
		{
			get
			{
				return this.m_LocalUserScore;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000057 RID: 87 RVA: 0x00002AC4 File Offset: 0x00000CC4
		public uint maxRange
		{
			get
			{
				return this.m_MaxRange;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000058 RID: 88 RVA: 0x00002AE0 File Offset: 0x00000CE0
		public IScore[] scores
		{
			get
			{
				return this.m_Scores;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000059 RID: 89 RVA: 0x00002AFC File Offset: 0x00000CFC
		public string title
		{
			get
			{
				return this.m_Title;
			}
		}

		// Token: 0x0400001B RID: 27
		private bool m_Loading;

		// Token: 0x0400001C RID: 28
		private IScore m_LocalUserScore;

		// Token: 0x0400001D RID: 29
		private uint m_MaxRange;

		// Token: 0x0400001E RID: 30
		private IScore[] m_Scores;

		// Token: 0x0400001F RID: 31
		private string m_Title;

		// Token: 0x04000020 RID: 32
		private string[] m_UserIDs;

		// Token: 0x04000021 RID: 33
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <id>k__BackingField;

		// Token: 0x04000022 RID: 34
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private UserScope <userScope>k__BackingField;

		// Token: 0x04000023 RID: 35
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Range <range>k__BackingField;

		// Token: 0x04000024 RID: 36
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TimeScope <timeScope>k__BackingField;
	}
}
