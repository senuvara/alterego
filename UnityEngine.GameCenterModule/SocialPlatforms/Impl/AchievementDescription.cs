﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x02000005 RID: 5
	public class AchievementDescription : IAchievementDescription
	{
		// Token: 0x06000028 RID: 40 RVA: 0x00002483 File Offset: 0x00000683
		public AchievementDescription(string id, string title, Texture2D image, string achievedDescription, string unachievedDescription, bool hidden, int points)
		{
			this.id = id;
			this.m_Title = title;
			this.m_Image = image;
			this.m_AchievedDescription = achievedDescription;
			this.m_UnachievedDescription = unachievedDescription;
			this.m_Hidden = hidden;
			this.m_Points = points;
		}

		// Token: 0x06000029 RID: 41 RVA: 0x000024C4 File Offset: 0x000006C4
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				this.id,
				" - ",
				this.title,
				" - ",
				this.achievedDescription,
				" - ",
				this.unachievedDescription,
				" - ",
				this.points,
				" - ",
				this.hidden
			});
		}

		// Token: 0x0600002A RID: 42 RVA: 0x0000254F File Offset: 0x0000074F
		public void SetImage(Texture2D image)
		{
			this.m_Image = image;
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600002B RID: 43 RVA: 0x0000255C File Offset: 0x0000075C
		// (set) Token: 0x0600002C RID: 44 RVA: 0x00002576 File Offset: 0x00000776
		public string id
		{
			[CompilerGenerated]
			get
			{
				return this.<id>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<id>k__BackingField = value;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600002D RID: 45 RVA: 0x00002580 File Offset: 0x00000780
		public string title
		{
			get
			{
				return this.m_Title;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600002E RID: 46 RVA: 0x0000259C File Offset: 0x0000079C
		public Texture2D image
		{
			get
			{
				return this.m_Image;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600002F RID: 47 RVA: 0x000025B8 File Offset: 0x000007B8
		public string achievedDescription
		{
			get
			{
				return this.m_AchievedDescription;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000030 RID: 48 RVA: 0x000025D4 File Offset: 0x000007D4
		public string unachievedDescription
		{
			get
			{
				return this.m_UnachievedDescription;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000031 RID: 49 RVA: 0x000025F0 File Offset: 0x000007F0
		public bool hidden
		{
			get
			{
				return this.m_Hidden;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000032 RID: 50 RVA: 0x0000260C File Offset: 0x0000080C
		public int points
		{
			get
			{
				return this.m_Points;
			}
		}

		// Token: 0x0400000E RID: 14
		private string m_Title;

		// Token: 0x0400000F RID: 15
		private Texture2D m_Image;

		// Token: 0x04000010 RID: 16
		private string m_AchievedDescription;

		// Token: 0x04000011 RID: 17
		private string m_UnachievedDescription;

		// Token: 0x04000012 RID: 18
		private bool m_Hidden;

		// Token: 0x04000013 RID: 19
		private int m_Points;

		// Token: 0x04000014 RID: 20
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <id>k__BackingField;
	}
}
