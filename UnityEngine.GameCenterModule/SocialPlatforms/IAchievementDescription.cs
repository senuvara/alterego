﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000011 RID: 17
	public interface IAchievementDescription
	{
		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000A7 RID: 167
		// (set) Token: 0x060000A8 RID: 168
		string id { get; set; }

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060000A9 RID: 169
		string title { get; }

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060000AA RID: 170
		Texture2D image { get; }

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060000AB RID: 171
		string achievedDescription { get; }

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060000AC RID: 172
		string unachievedDescription { get; }

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060000AD RID: 173
		bool hidden { get; }

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060000AE RID: 174
		int points { get; }
	}
}
