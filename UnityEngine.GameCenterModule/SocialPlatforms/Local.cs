﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.SocialPlatforms.Impl;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000008 RID: 8
	public class Local : ISocialPlatform
	{
		// Token: 0x0600005A RID: 90 RVA: 0x00002B17 File Offset: 0x00000D17
		public Local()
		{
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600005B RID: 91 RVA: 0x00002B58 File Offset: 0x00000D58
		public ILocalUser localUser
		{
			get
			{
				if (Local.m_LocalUser == null)
				{
					Local.m_LocalUser = new LocalUser();
				}
				return Local.m_LocalUser;
			}
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00002B88 File Offset: 0x00000D88
		void ISocialPlatform.Authenticate(ILocalUser user, Action<bool> callback)
		{
			LocalUser localUser = (LocalUser)user;
			this.m_DefaultTexture = this.CreateDummyTexture(32, 32);
			this.PopulateStaticData();
			localUser.SetAuthenticated(true);
			localUser.SetUnderage(false);
			localUser.SetUserID("1000");
			localUser.SetUserName("Lerpz");
			localUser.SetImage(this.m_DefaultTexture);
			if (callback != null)
			{
				callback(true);
			}
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00002BF0 File Offset: 0x00000DF0
		void ISocialPlatform.Authenticate(ILocalUser user, Action<bool, string> callback)
		{
			((ISocialPlatform)this).Authenticate(user, delegate(bool success)
			{
				callback(success, null);
			});
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00002C1E File Offset: 0x00000E1E
		void ISocialPlatform.LoadFriends(ILocalUser user, Action<bool> callback)
		{
			if (this.VerifyUser())
			{
				((LocalUser)user).SetFriends(this.m_Friends.ToArray());
				if (callback != null)
				{
					callback(true);
				}
			}
		}

		// Token: 0x0600005F RID: 95 RVA: 0x00002C54 File Offset: 0x00000E54
		public void LoadUsers(string[] userIDs, Action<IUserProfile[]> callback)
		{
			List<UserProfile> list = new List<UserProfile>();
			if (this.VerifyUser())
			{
				foreach (string b in userIDs)
				{
					foreach (UserProfile userProfile in this.m_Users)
					{
						if (userProfile.id == b)
						{
							list.Add(userProfile);
						}
					}
					foreach (UserProfile userProfile2 in this.m_Friends)
					{
						if (userProfile2.id == b)
						{
							list.Add(userProfile2);
						}
					}
				}
				callback(list.ToArray());
			}
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00002D68 File Offset: 0x00000F68
		public void ReportProgress(string id, double progress, Action<bool> callback)
		{
			if (this.VerifyUser())
			{
				foreach (Achievement achievement in this.m_Achievements)
				{
					if (achievement.id == id && achievement.percentCompleted <= progress)
					{
						if (progress >= 100.0)
						{
							achievement.SetCompleted(true);
						}
						achievement.SetHidden(false);
						achievement.SetLastReportedDate(DateTime.Now);
						achievement.percentCompleted = progress;
						if (callback != null)
						{
							callback(true);
						}
						return;
					}
				}
				foreach (AchievementDescription achievementDescription in this.m_AchievementDescriptions)
				{
					if (achievementDescription.id == id)
					{
						bool completed = progress >= 100.0;
						Achievement item = new Achievement(id, progress, completed, false, DateTime.Now);
						this.m_Achievements.Add(item);
						if (callback != null)
						{
							callback(true);
						}
						return;
					}
				}
				Debug.LogError("Achievement ID not found");
				if (callback != null)
				{
					callback(false);
				}
			}
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00002EE8 File Offset: 0x000010E8
		public void LoadAchievementDescriptions(Action<IAchievementDescription[]> callback)
		{
			if (this.VerifyUser())
			{
				if (callback != null)
				{
					callback(this.m_AchievementDescriptions.ToArray());
				}
			}
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00002F12 File Offset: 0x00001112
		public void LoadAchievements(Action<IAchievement[]> callback)
		{
			if (this.VerifyUser())
			{
				if (callback != null)
				{
					callback(this.m_Achievements.ToArray());
				}
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00002F3C File Offset: 0x0000113C
		public void ReportScore(long score, string board, Action<bool> callback)
		{
			if (this.VerifyUser())
			{
				foreach (Leaderboard leaderboard in this.m_Leaderboards)
				{
					if (leaderboard.id == board)
					{
						leaderboard.SetScores(new List<Score>((Score[])leaderboard.scores)
						{
							new Score(board, score, this.localUser.id, DateTime.Now, score + " points", 0)
						}.ToArray());
						if (callback != null)
						{
							callback(true);
						}
						return;
					}
				}
				Debug.LogError("Leaderboard not found");
				if (callback != null)
				{
					callback(false);
				}
			}
		}

		// Token: 0x06000064 RID: 100 RVA: 0x0000302C File Offset: 0x0000122C
		public void LoadScores(string leaderboardID, Action<IScore[]> callback)
		{
			if (this.VerifyUser())
			{
				foreach (Leaderboard leaderboard in this.m_Leaderboards)
				{
					if (leaderboard.id == leaderboardID)
					{
						this.SortScores(leaderboard);
						if (callback != null)
						{
							callback(leaderboard.scores);
						}
						return;
					}
				}
				Debug.LogError("Leaderboard not found");
				if (callback != null)
				{
					callback(new Score[0]);
				}
			}
		}

		// Token: 0x06000065 RID: 101 RVA: 0x000030E4 File Offset: 0x000012E4
		void ISocialPlatform.LoadScores(ILeaderboard board, Action<bool> callback)
		{
			if (this.VerifyUser())
			{
				Leaderboard leaderboard = (Leaderboard)board;
				foreach (Leaderboard leaderboard2 in this.m_Leaderboards)
				{
					if (leaderboard2.id == leaderboard.id)
					{
						leaderboard.SetTitle(leaderboard2.title);
						leaderboard.SetScores(leaderboard2.scores);
						leaderboard.SetMaxRange((uint)leaderboard2.scores.Length);
					}
				}
				this.SortScores(leaderboard);
				this.SetLocalPlayerScore(leaderboard);
				if (callback != null)
				{
					callback(true);
				}
			}
		}

		// Token: 0x06000066 RID: 102 RVA: 0x000031B0 File Offset: 0x000013B0
		bool ISocialPlatform.GetLoading(ILeaderboard board)
		{
			return this.VerifyUser() && ((Leaderboard)board).loading;
		}

		// Token: 0x06000067 RID: 103 RVA: 0x000031E4 File Offset: 0x000013E4
		private void SortScores(Leaderboard board)
		{
			List<Score> list = new List<Score>((Score[])board.scores);
			list.Sort((Score s1, Score s2) => s2.value.CompareTo(s1.value));
			for (int i = 0; i < list.Count; i++)
			{
				list[i].SetRank(i + 1);
			}
		}

		// Token: 0x06000068 RID: 104 RVA: 0x0000324C File Offset: 0x0000144C
		private void SetLocalPlayerScore(Leaderboard board)
		{
			foreach (Score score in board.scores)
			{
				if (score.userID == this.localUser.id)
				{
					board.SetLocalUserScore(score);
					break;
				}
			}
		}

		// Token: 0x06000069 RID: 105 RVA: 0x000032A9 File Offset: 0x000014A9
		public void ShowAchievementsUI()
		{
			Debug.Log("ShowAchievementsUI not implemented");
		}

		// Token: 0x0600006A RID: 106 RVA: 0x000032B6 File Offset: 0x000014B6
		public void ShowLeaderboardUI()
		{
			Debug.Log("ShowLeaderboardUI not implemented");
		}

		// Token: 0x0600006B RID: 107 RVA: 0x000032C4 File Offset: 0x000014C4
		public ILeaderboard CreateLeaderboard()
		{
			return new Leaderboard();
		}

		// Token: 0x0600006C RID: 108 RVA: 0x000032E0 File Offset: 0x000014E0
		public IAchievement CreateAchievement()
		{
			return new Achievement();
		}

		// Token: 0x0600006D RID: 109 RVA: 0x000032FC File Offset: 0x000014FC
		private bool VerifyUser()
		{
			bool result;
			if (!this.localUser.authenticated)
			{
				Debug.LogError("Must authenticate first");
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x0600006E RID: 110 RVA: 0x00003334 File Offset: 0x00001534
		private void PopulateStaticData()
		{
			this.m_Friends.Add(new UserProfile("Fred", "1001", true, UserState.Online, this.m_DefaultTexture));
			this.m_Friends.Add(new UserProfile("Julia", "1002", true, UserState.Online, this.m_DefaultTexture));
			this.m_Friends.Add(new UserProfile("Jeff", "1003", true, UserState.Online, this.m_DefaultTexture));
			this.m_Users.Add(new UserProfile("Sam", "1004", false, UserState.Offline, this.m_DefaultTexture));
			this.m_Users.Add(new UserProfile("Max", "1005", false, UserState.Offline, this.m_DefaultTexture));
			this.m_AchievementDescriptions.Add(new AchievementDescription("Achievement01", "First achievement", this.m_DefaultTexture, "Get first achievement", "Received first achievement", false, 10));
			this.m_AchievementDescriptions.Add(new AchievementDescription("Achievement02", "Second achievement", this.m_DefaultTexture, "Get second achievement", "Received second achievement", false, 20));
			this.m_AchievementDescriptions.Add(new AchievementDescription("Achievement03", "Third achievement", this.m_DefaultTexture, "Get third achievement", "Received third achievement", false, 15));
			Leaderboard leaderboard = new Leaderboard();
			leaderboard.SetTitle("High Scores");
			leaderboard.id = "Leaderboard01";
			leaderboard.SetScores(new List<Score>
			{
				new Score("Leaderboard01", 300L, "1001", DateTime.Now.AddDays(-1.0), "300 points", 1),
				new Score("Leaderboard01", 255L, "1002", DateTime.Now.AddDays(-1.0), "255 points", 2),
				new Score("Leaderboard01", 55L, "1003", DateTime.Now.AddDays(-1.0), "55 points", 3),
				new Score("Leaderboard01", 10L, "1004", DateTime.Now.AddDays(-1.0), "10 points", 4)
			}.ToArray());
			this.m_Leaderboards.Add(leaderboard);
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00003588 File Offset: 0x00001788
		private Texture2D CreateDummyTexture(int width, int height)
		{
			Texture2D texture2D = new Texture2D(width, height);
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					Color color = ((j & i) <= 0) ? Color.gray : Color.white;
					texture2D.SetPixel(j, i, color);
				}
			}
			texture2D.Apply();
			return texture2D;
		}

		// Token: 0x06000070 RID: 112 RVA: 0x000035F8 File Offset: 0x000017F8
		// Note: this type is marked as 'beforefieldinit'.
		static Local()
		{
		}

		// Token: 0x06000071 RID: 113 RVA: 0x00003600 File Offset: 0x00001800
		[CompilerGenerated]
		private static int <SortScores>m__0(Score s1, Score s2)
		{
			return s2.value.CompareTo(s1.value);
		}

		// Token: 0x04000025 RID: 37
		private static LocalUser m_LocalUser = null;

		// Token: 0x04000026 RID: 38
		private List<UserProfile> m_Friends = new List<UserProfile>();

		// Token: 0x04000027 RID: 39
		private List<UserProfile> m_Users = new List<UserProfile>();

		// Token: 0x04000028 RID: 40
		private List<AchievementDescription> m_AchievementDescriptions = new List<AchievementDescription>();

		// Token: 0x04000029 RID: 41
		private List<Achievement> m_Achievements = new List<Achievement>();

		// Token: 0x0400002A RID: 42
		private List<Leaderboard> m_Leaderboards = new List<Leaderboard>();

		// Token: 0x0400002B RID: 43
		private Texture2D m_DefaultTexture;

		// Token: 0x0400002C RID: 44
		[CompilerGenerated]
		private static Comparison<Score> <>f__am$cache0;

		// Token: 0x02000009 RID: 9
		[CompilerGenerated]
		private sealed class <UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate>c__AnonStorey0
		{
			// Token: 0x06000072 RID: 114 RVA: 0x00003629 File Offset: 0x00001829
			public <UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate>c__AnonStorey0()
			{
			}

			// Token: 0x06000073 RID: 115 RVA: 0x00003631 File Offset: 0x00001831
			internal void <>m__0(bool success)
			{
				this.callback(success, null);
			}

			// Token: 0x0400002D RID: 45
			internal Action<bool, string> callback;
		}
	}
}
