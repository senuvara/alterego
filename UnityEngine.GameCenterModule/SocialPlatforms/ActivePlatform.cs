﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200000B RID: 11
	internal static class ActivePlatform
	{
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000081 RID: 129 RVA: 0x0000373C File Offset: 0x0000193C
		// (set) Token: 0x06000082 RID: 130 RVA: 0x0000376A File Offset: 0x0000196A
		internal static ISocialPlatform Instance
		{
			get
			{
				if (ActivePlatform._active == null)
				{
					ActivePlatform._active = ActivePlatform.SelectSocialPlatform();
				}
				return ActivePlatform._active;
			}
			set
			{
				ActivePlatform._active = value;
			}
		}

		// Token: 0x06000083 RID: 131 RVA: 0x00003774 File Offset: 0x00001974
		private static ISocialPlatform SelectSocialPlatform()
		{
			return new Local();
		}

		// Token: 0x0400002E RID: 46
		private static ISocialPlatform _active;
	}
}
