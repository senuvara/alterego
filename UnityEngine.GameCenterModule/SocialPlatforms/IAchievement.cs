﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000010 RID: 16
	public interface IAchievement
	{
		// Token: 0x0600009F RID: 159
		void ReportProgress(Action<bool> callback);

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060000A0 RID: 160
		// (set) Token: 0x060000A1 RID: 161
		string id { get; set; }

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000A2 RID: 162
		// (set) Token: 0x060000A3 RID: 163
		double percentCompleted { get; set; }

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000A4 RID: 164
		bool completed { get; }

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060000A5 RID: 165
		bool hidden { get; }

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000A6 RID: 166
		DateTime lastReportedDate { get; }
	}
}
