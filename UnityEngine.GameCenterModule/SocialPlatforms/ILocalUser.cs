﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200000D RID: 13
	public interface ILocalUser : IUserProfile
	{
		// Token: 0x06000094 RID: 148
		void Authenticate(Action<bool> callback);

		// Token: 0x06000095 RID: 149
		void Authenticate(Action<bool, string> callback);

		// Token: 0x06000096 RID: 150
		void LoadFriends(Action<bool> callback);

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000097 RID: 151
		IUserProfile[] friends { get; }

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000098 RID: 152
		bool authenticated { get; }

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000099 RID: 153
		bool underage { get; }
	}
}
