﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000015 RID: 21
	public struct Range
	{
		// Token: 0x060000B8 RID: 184 RVA: 0x0000378E File Offset: 0x0000198E
		public Range(int fromValue, int valueCount)
		{
			this.from = fromValue;
			this.count = valueCount;
		}

		// Token: 0x0400003C RID: 60
		public int from;

		// Token: 0x0400003D RID: 61
		public int count;
	}
}
