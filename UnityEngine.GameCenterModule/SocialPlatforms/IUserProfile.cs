﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200000F RID: 15
	public interface IUserProfile
	{
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600009A RID: 154
		string userName { get; }

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600009B RID: 155
		string id { get; }

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600009C RID: 156
		bool isFriend { get; }

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600009D RID: 157
		UserState state { get; }

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600009E RID: 158
		Texture2D image { get; }
	}
}
