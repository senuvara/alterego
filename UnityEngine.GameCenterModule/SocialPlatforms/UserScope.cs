﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000013 RID: 19
	public enum UserScope
	{
		// Token: 0x04000036 RID: 54
		Global,
		// Token: 0x04000037 RID: 55
		FriendsOnly
	}
}
