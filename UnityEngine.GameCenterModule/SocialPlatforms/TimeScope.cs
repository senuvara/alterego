﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000014 RID: 20
	public enum TimeScope
	{
		// Token: 0x04000039 RID: 57
		Today,
		// Token: 0x0400003A RID: 58
		Week,
		// Token: 0x0400003B RID: 59
		AllTime
	}
}
