﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200000E RID: 14
	public enum UserState
	{
		// Token: 0x04000030 RID: 48
		Online,
		// Token: 0x04000031 RID: 49
		OnlineAndAway,
		// Token: 0x04000032 RID: 50
		OnlineAndBusy,
		// Token: 0x04000033 RID: 51
		Offline,
		// Token: 0x04000034 RID: 52
		Playing
	}
}
