﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000016 RID: 22
	public interface ILeaderboard
	{
		// Token: 0x060000B9 RID: 185
		void SetUserFilter(string[] userIDs);

		// Token: 0x060000BA RID: 186
		void LoadScores(Action<bool> callback);

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000BB RID: 187
		bool loading { get; }

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000BC RID: 188
		// (set) Token: 0x060000BD RID: 189
		string id { get; set; }

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060000BE RID: 190
		// (set) Token: 0x060000BF RID: 191
		UserScope userScope { get; set; }

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060000C0 RID: 192
		// (set) Token: 0x060000C1 RID: 193
		Range range { get; set; }

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060000C2 RID: 194
		// (set) Token: 0x060000C3 RID: 195
		TimeScope timeScope { get; set; }

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060000C4 RID: 196
		IScore localUserScore { get; }

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060000C5 RID: 197
		uint maxRange { get; }

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060000C6 RID: 198
		IScore[] scores { get; }

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060000C7 RID: 199
		string title { get; }
	}
}
