﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200000C RID: 12
	public interface ISocialPlatform
	{
		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000084 RID: 132
		ILocalUser localUser { get; }

		// Token: 0x06000085 RID: 133
		void LoadUsers(string[] userIDs, Action<IUserProfile[]> callback);

		// Token: 0x06000086 RID: 134
		void ReportProgress(string achievementID, double progress, Action<bool> callback);

		// Token: 0x06000087 RID: 135
		void LoadAchievementDescriptions(Action<IAchievementDescription[]> callback);

		// Token: 0x06000088 RID: 136
		void LoadAchievements(Action<IAchievement[]> callback);

		// Token: 0x06000089 RID: 137
		IAchievement CreateAchievement();

		// Token: 0x0600008A RID: 138
		void ReportScore(long score, string board, Action<bool> callback);

		// Token: 0x0600008B RID: 139
		void LoadScores(string leaderboardID, Action<IScore[]> callback);

		// Token: 0x0600008C RID: 140
		ILeaderboard CreateLeaderboard();

		// Token: 0x0600008D RID: 141
		void ShowAchievementsUI();

		// Token: 0x0600008E RID: 142
		void ShowLeaderboardUI();

		// Token: 0x0600008F RID: 143
		void Authenticate(ILocalUser user, Action<bool> callback);

		// Token: 0x06000090 RID: 144
		void Authenticate(ILocalUser user, Action<bool, string> callback);

		// Token: 0x06000091 RID: 145
		void LoadFriends(ILocalUser user, Action<bool> callback);

		// Token: 0x06000092 RID: 146
		void LoadScores(ILeaderboard board, Action<bool> callback);

		// Token: 0x06000093 RID: 147
		bool GetLoading(ILeaderboard board);
	}
}
