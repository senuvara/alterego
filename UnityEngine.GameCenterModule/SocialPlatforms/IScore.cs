﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000012 RID: 18
	public interface IScore
	{
		// Token: 0x060000AF RID: 175
		void ReportScore(Action<bool> callback);

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000B0 RID: 176
		// (set) Token: 0x060000B1 RID: 177
		string leaderboardID { get; set; }

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000B2 RID: 178
		// (set) Token: 0x060000B3 RID: 179
		long value { get; set; }

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000B4 RID: 180
		DateTime date { get; }

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000B5 RID: 181
		string formattedValue { get; }

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000B6 RID: 182
		string userID { get; }

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000B7 RID: 183
		int rank { get; }
	}
}
