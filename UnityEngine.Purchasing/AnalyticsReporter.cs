﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000002 RID: 2
	internal class AnalyticsReporter
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public AnalyticsReporter(IUnityAnalytics analytics)
		{
			this.m_Analytics = analytics;
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002060 File Offset: 0x00000260
		public void OnPurchaseSucceeded(Product product)
		{
			if (product.metadata.isoCurrencyCode == null)
			{
				return;
			}
			this.m_Analytics.Transaction(product.definition.storeSpecificId, product.metadata.localizedPrice, product.metadata.isoCurrencyCode, product.receipt, null);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020B4 File Offset: 0x000002B4
		public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
		{
			Dictionary<string, object> data = new Dictionary<string, object>
			{
				{
					"productID",
					product.definition.storeSpecificId
				},
				{
					"reason",
					reason
				},
				{
					"price",
					product.metadata.localizedPrice
				},
				{
					"currency",
					product.metadata.isoCurrencyCode
				}
			};
			this.m_Analytics.CustomEvent("unity.PurchaseFailed", data);
		}

		// Token: 0x04000001 RID: 1
		private IUnityAnalytics m_Analytics;
	}
}
