﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000008 RID: 8
	public interface IStoreListener
	{
		// Token: 0x06000024 RID: 36
		void OnInitializeFailed(InitializationFailureReason error);

		// Token: 0x06000025 RID: 37
		PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e);

		// Token: 0x06000026 RID: 38
		void OnPurchaseFailed(Product i, PurchaseFailureReason p);

		// Token: 0x06000027 RID: 39
		void OnInitialized(IStoreController controller, IExtensionProvider extensions);
	}
}
