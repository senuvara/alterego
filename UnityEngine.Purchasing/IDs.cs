﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000003 RID: 3
	public class IDs : IEnumerable<KeyValuePair<string, string>>, IEnumerable
	{
		// Token: 0x06000004 RID: 4 RVA: 0x00002132 File Offset: 0x00000332
		public IDs()
		{
		}

		// Token: 0x06000005 RID: 5 RVA: 0x00002145 File Offset: 0x00000345
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.m_Dic.GetEnumerator();
		}

		// Token: 0x06000006 RID: 6 RVA: 0x00002158 File Offset: 0x00000358
		public void Add(string id, params string[] stores)
		{
			foreach (string key in stores)
			{
				this.m_Dic[key] = id;
			}
		}

		// Token: 0x06000007 RID: 7 RVA: 0x0000218C File Offset: 0x0000038C
		public void Add(string id, params object[] stores)
		{
			foreach (object obj in stores)
			{
				this.m_Dic[obj.ToString()] = id;
			}
		}

		// Token: 0x06000008 RID: 8 RVA: 0x000021C5 File Offset: 0x000003C5
		internal string SpecificIDForStore(string store, string defaultValue)
		{
			if (this.m_Dic.ContainsKey(store))
			{
				return this.m_Dic[store];
			}
			return defaultValue;
		}

		// Token: 0x06000009 RID: 9 RVA: 0x000021E6 File Offset: 0x000003E6
		public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
		{
			return this.m_Dic.GetEnumerator();
		}

		// Token: 0x04000002 RID: 2
		private Dictionary<string, string> m_Dic = new Dictionary<string, string>();
	}
}
