﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001A RID: 26
	internal class SimpleCatalogProvider : ICatalogProvider
	{
		// Token: 0x060000A4 RID: 164 RVA: 0x000031F7 File Offset: 0x000013F7
		internal SimpleCatalogProvider(Action<Action<HashSet<ProductDefinition>>> func)
		{
			this.m_Func = func;
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x00003206 File Offset: 0x00001406
		public void FetchProducts(Action<HashSet<ProductDefinition>> callback)
		{
			if (this.m_Func != null)
			{
				this.m_Func(callback);
			}
		}

		// Token: 0x04000056 RID: 86
		private Action<Action<HashSet<ProductDefinition>>> m_Func;
	}
}
