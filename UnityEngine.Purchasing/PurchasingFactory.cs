﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000017 RID: 23
	internal class PurchasingFactory : IPurchasingBinder, IExtensionProvider
	{
		// Token: 0x06000081 RID: 129 RVA: 0x00002998 File Offset: 0x00000B98
		public PurchasingFactory(IPurchasingModule first, params IPurchasingModule[] remainingModules)
		{
			first.Configure(this);
			foreach (IPurchasingModule purchasingModule in remainingModules)
			{
				purchasingModule.Configure(this);
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000082 RID: 130 RVA: 0x000029E9 File Offset: 0x00000BE9
		// (set) Token: 0x06000083 RID: 131 RVA: 0x000029F1 File Offset: 0x00000BF1
		public string storeName
		{
			[CompilerGenerated]
			get
			{
				return this.<storeName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<storeName>k__BackingField = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000084 RID: 132 RVA: 0x000029FA File Offset: 0x00000BFA
		// (set) Token: 0x06000085 RID: 133 RVA: 0x00002A18 File Offset: 0x00000C18
		public IStore service
		{
			get
			{
				if (this.m_Store != null)
				{
					return this.m_Store;
				}
				throw new InvalidOperationException("No impl available!");
			}
			set
			{
				this.m_Store = value;
			}
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00002A21 File Offset: 0x00000C21
		public void RegisterStore(string name, IStore s)
		{
			if (this.m_Store == null && s != null)
			{
				this.storeName = name;
				this.service = s;
			}
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00002A42 File Offset: 0x00000C42
		public void RegisterExtension<T>(T instance) where T : IStoreExtension
		{
			this.m_ExtensionMap[typeof(T)] = instance;
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00002A5F File Offset: 0x00000C5F
		public void RegisterConfiguration<T>(T instance) where T : IStoreConfiguration
		{
			this.m_ConfigMap[typeof(T)] = instance;
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00002A7C File Offset: 0x00000C7C
		public T GetConfig<T>() where T : IStoreConfiguration
		{
			if (this.service is T)
			{
				return (T)((object)this.service);
			}
			Type typeFromHandle = typeof(T);
			if (this.m_ConfigMap.ContainsKey(typeFromHandle))
			{
				return (T)((object)this.m_ConfigMap[typeFromHandle]);
			}
			throw new ArgumentException("No binding for config type " + typeFromHandle);
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00002AE4 File Offset: 0x00000CE4
		public T GetExtension<T>() where T : IStoreExtension
		{
			if (this.service is T)
			{
				return (T)((object)this.service);
			}
			Type typeFromHandle = typeof(T);
			if (this.m_ExtensionMap.ContainsKey(typeFromHandle))
			{
				return (T)((object)this.m_ExtensionMap[typeFromHandle]);
			}
			throw new ArgumentException("No binding for type " + typeFromHandle);
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00002B4B File Offset: 0x00000D4B
		public void SetCatalogProvider(ICatalogProvider provider)
		{
			this.m_CatalogProvider = provider;
		}

		// Token: 0x0600008C RID: 140 RVA: 0x00002B54 File Offset: 0x00000D54
		public void SetCatalogProviderFunction(Action<Action<HashSet<ProductDefinition>>> func)
		{
			this.m_CatalogProvider = new SimpleCatalogProvider(func);
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00002B62 File Offset: 0x00000D62
		internal ICatalogProvider GetCatalogProvider()
		{
			return this.m_CatalogProvider;
		}

		// Token: 0x04000042 RID: 66
		private Dictionary<Type, IStoreConfiguration> m_ConfigMap = new Dictionary<Type, IStoreConfiguration>();

		// Token: 0x04000043 RID: 67
		private Dictionary<Type, IStoreExtension> m_ExtensionMap = new Dictionary<Type, IStoreExtension>();

		// Token: 0x04000044 RID: 68
		private IStore m_Store;

		// Token: 0x04000045 RID: 69
		private ICatalogProvider m_CatalogProvider;

		// Token: 0x04000046 RID: 70
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <storeName>k__BackingField;
	}
}
