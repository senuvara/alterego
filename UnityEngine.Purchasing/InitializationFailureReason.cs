﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000A RID: 10
	public enum InitializationFailureReason
	{
		// Token: 0x04000008 RID: 8
		PurchasingUnavailable,
		// Token: 0x04000009 RID: 9
		NoProductsAvailable,
		// Token: 0x0400000A RID: 10
		AppNotKnown
	}
}
