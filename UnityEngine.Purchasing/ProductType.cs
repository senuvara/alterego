﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000011 RID: 17
	public enum ProductType
	{
		// Token: 0x0400002C RID: 44
		Consumable,
		// Token: 0x0400002D RID: 45
		NonConsumable,
		// Token: 0x0400002E RID: 46
		Subscription
	}
}
