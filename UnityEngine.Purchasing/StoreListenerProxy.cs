﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001B RID: 27
	internal class StoreListenerProxy : IInternalStoreListener
	{
		// Token: 0x060000A6 RID: 166 RVA: 0x0000321F File Offset: 0x0000141F
		public StoreListenerProxy(IStoreListener forwardTo, AnalyticsReporter analytics, IExtensionProvider extensions)
		{
			this.m_ForwardTo = forwardTo;
			this.m_Analytics = analytics;
			this.m_Extensions = extensions;
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x0000323C File Offset: 0x0000143C
		public void OnInitialized(IStoreController controller)
		{
			this.m_ForwardTo.OnInitialized(controller, this.m_Extensions);
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x00003250 File Offset: 0x00001450
		public void OnInitializeFailed(InitializationFailureReason error)
		{
			this.m_ForwardTo.OnInitializeFailed(error);
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x0000325E File Offset: 0x0000145E
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
		{
			this.m_Analytics.OnPurchaseSucceeded(e.purchasedProduct);
			return this.m_ForwardTo.ProcessPurchase(e);
		}

		// Token: 0x060000AA RID: 170 RVA: 0x0000327D File Offset: 0x0000147D
		public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
		{
			this.m_Analytics.OnPurchaseFailed(i, p);
			this.m_ForwardTo.OnPurchaseFailed(i, p);
		}

		// Token: 0x04000057 RID: 87
		private AnalyticsReporter m_Analytics;

		// Token: 0x04000058 RID: 88
		private IStoreListener m_ForwardTo;

		// Token: 0x04000059 RID: 89
		private IExtensionProvider m_Extensions;
	}
}
