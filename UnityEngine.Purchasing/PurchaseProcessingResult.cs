﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000016 RID: 22
	public enum PurchaseProcessingResult
	{
		// Token: 0x04000040 RID: 64
		Complete,
		// Token: 0x04000041 RID: 65
		Pending
	}
}
