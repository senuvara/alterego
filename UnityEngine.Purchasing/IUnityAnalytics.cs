﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000009 RID: 9
	internal interface IUnityAnalytics
	{
		// Token: 0x06000028 RID: 40
		void Transaction(string productId, decimal price, string currency, string receipt, string signature);

		// Token: 0x06000029 RID: 41
		void CustomEvent(string name, Dictionary<string, object> data);
	}
}
