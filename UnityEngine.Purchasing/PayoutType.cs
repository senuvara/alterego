﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000C RID: 12
	public enum PayoutType
	{
		// Token: 0x04000012 RID: 18
		Other,
		// Token: 0x04000013 RID: 19
		Currency,
		// Token: 0x04000014 RID: 20
		Item,
		// Token: 0x04000015 RID: 21
		Resource
	}
}
