﻿using System;
using System.Collections.Generic;
using UnityEngine.Analytics;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001D RID: 29
	internal class UnityAnalytics : IUnityAnalytics
	{
		// Token: 0x060000B1 RID: 177 RVA: 0x0000340C File Offset: 0x0000160C
		public UnityAnalytics()
		{
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00003414 File Offset: 0x00001614
		public void Transaction(string productId, decimal price, string currency, string receipt, string signature)
		{
			Analytics.Transaction(productId, price, currency, receipt, signature, true);
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x00003424 File Offset: 0x00001624
		public void CustomEvent(string name, Dictionary<string, object> data)
		{
			Analytics.CustomEvent(name, data);
		}
	}
}
