﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000006 RID: 6
	internal interface IInternalStoreListener
	{
		// Token: 0x06000019 RID: 25
		void OnInitializeFailed(InitializationFailureReason error);

		// Token: 0x0600001A RID: 26
		PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e);

		// Token: 0x0600001B RID: 27
		void OnPurchaseFailed(Product i, PurchaseFailureReason p);

		// Token: 0x0600001C RID: 28
		void OnInitialized(IStoreController controller);
	}
}
