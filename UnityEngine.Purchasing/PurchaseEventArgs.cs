﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000012 RID: 18
	public class PurchaseEventArgs
	{
		// Token: 0x06000070 RID: 112 RVA: 0x000028D5 File Offset: 0x00000AD5
		internal PurchaseEventArgs(Product purchasedProduct)
		{
			this.purchasedProduct = purchasedProduct;
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000071 RID: 113 RVA: 0x000028E4 File Offset: 0x00000AE4
		// (set) Token: 0x06000072 RID: 114 RVA: 0x000028EC File Offset: 0x00000AEC
		public Product purchasedProduct
		{
			[CompilerGenerated]
			get
			{
				return this.<purchasedProduct>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<purchasedProduct>k__BackingField = value;
			}
		}

		// Token: 0x0400002F RID: 47
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Product <purchasedProduct>k__BackingField;
	}
}
