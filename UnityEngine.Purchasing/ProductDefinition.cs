﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000F RID: 15
	public class ProductDefinition
	{
		// Token: 0x06000051 RID: 81 RVA: 0x000026DD File Offset: 0x000008DD
		private ProductDefinition()
		{
		}

		// Token: 0x06000052 RID: 82 RVA: 0x000026F0 File Offset: 0x000008F0
		public ProductDefinition(string id, string storeSpecificId, ProductType type) : this(id, storeSpecificId, type, true)
		{
		}

		// Token: 0x06000053 RID: 83 RVA: 0x000026FC File Offset: 0x000008FC
		public ProductDefinition(string id, string storeSpecificId, ProductType type, bool enabled) : this(id, storeSpecificId, type, enabled, null)
		{
		}

		// Token: 0x06000054 RID: 84 RVA: 0x0000270C File Offset: 0x0000090C
		public ProductDefinition(string id, string storeSpecificId, ProductType type, bool enabled, PayoutDefinition payout) : this(id, storeSpecificId, type, enabled, new List<PayoutDefinition>
		{
			payout
		})
		{
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00002733 File Offset: 0x00000933
		public ProductDefinition(string id, string storeSpecificId, ProductType type, bool enabled, IEnumerable<PayoutDefinition> payouts)
		{
			this.id = id;
			this.storeSpecificId = storeSpecificId;
			this.type = type;
			this.enabled = enabled;
			this.SetPayouts(payouts);
		}

		// Token: 0x06000056 RID: 86 RVA: 0x0000276B File Offset: 0x0000096B
		public ProductDefinition(string id, ProductType type) : this(id, id, type)
		{
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000057 RID: 87 RVA: 0x00002776 File Offset: 0x00000976
		// (set) Token: 0x06000058 RID: 88 RVA: 0x0000277E File Offset: 0x0000097E
		public string id
		{
			[CompilerGenerated]
			get
			{
				return this.<id>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<id>k__BackingField = value;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000059 RID: 89 RVA: 0x00002787 File Offset: 0x00000987
		// (set) Token: 0x0600005A RID: 90 RVA: 0x0000278F File Offset: 0x0000098F
		public string storeSpecificId
		{
			[CompilerGenerated]
			get
			{
				return this.<storeSpecificId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<storeSpecificId>k__BackingField = value;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600005B RID: 91 RVA: 0x00002798 File Offset: 0x00000998
		// (set) Token: 0x0600005C RID: 92 RVA: 0x000027A0 File Offset: 0x000009A0
		public ProductType type
		{
			[CompilerGenerated]
			get
			{
				return this.<type>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<type>k__BackingField = value;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600005D RID: 93 RVA: 0x000027A9 File Offset: 0x000009A9
		// (set) Token: 0x0600005E RID: 94 RVA: 0x000027B1 File Offset: 0x000009B1
		public bool enabled
		{
			[CompilerGenerated]
			get
			{
				return this.<enabled>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<enabled>k__BackingField = value;
			}
		}

		// Token: 0x0600005F RID: 95 RVA: 0x000027BC File Offset: 0x000009BC
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			ProductDefinition productDefinition = obj as ProductDefinition;
			return productDefinition != null && this.id == productDefinition.id;
		}

		// Token: 0x06000060 RID: 96 RVA: 0x000027F1 File Offset: 0x000009F1
		public override int GetHashCode()
		{
			return this.id.GetHashCode();
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000061 RID: 97 RVA: 0x000027FE File Offset: 0x000009FE
		public IEnumerable<PayoutDefinition> payouts
		{
			get
			{
				return this.m_Payouts;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000062 RID: 98 RVA: 0x00002806 File Offset: 0x00000A06
		public PayoutDefinition payout
		{
			get
			{
				return (this.m_Payouts.Count <= 0) ? null : this.m_Payouts[0];
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x0000282B File Offset: 0x00000A2B
		internal void SetPayouts(IEnumerable<PayoutDefinition> newPayouts)
		{
			if (newPayouts == null)
			{
				return;
			}
			this.m_Payouts.Clear();
			this.m_Payouts.AddRange(newPayouts);
		}

		// Token: 0x04000021 RID: 33
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <id>k__BackingField;

		// Token: 0x04000022 RID: 34
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <storeSpecificId>k__BackingField;

		// Token: 0x04000023 RID: 35
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ProductType <type>k__BackingField;

		// Token: 0x04000024 RID: 36
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <enabled>k__BackingField;

		// Token: 0x04000025 RID: 37
		private List<PayoutDefinition> m_Payouts = new List<PayoutDefinition>();
	}
}
