﻿using System;
using System.IO;
using System.Text;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001C RID: 28
	internal class TransactionLog
	{
		// Token: 0x060000AB RID: 171 RVA: 0x00003299 File Offset: 0x00001499
		public TransactionLog(ILogger logger, string persistentDataPath)
		{
			this.logger = logger;
			if (!string.IsNullOrEmpty(persistentDataPath))
			{
				this.persistentDataPath = Path.Combine(Path.Combine(persistentDataPath, "Unity"), "UnityPurchasing");
			}
		}

		// Token: 0x060000AC RID: 172 RVA: 0x000032CE File Offset: 0x000014CE
		public void Clear()
		{
			Directory.Delete(this.persistentDataPath, true);
		}

		// Token: 0x060000AD RID: 173 RVA: 0x000032DC File Offset: 0x000014DC
		public bool HasRecordOf(string transactionID)
		{
			return !string.IsNullOrEmpty(transactionID) && !string.IsNullOrEmpty(this.persistentDataPath) && Directory.Exists(this.GetRecordPath(transactionID));
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00003308 File Offset: 0x00001508
		public void Record(string transactionID)
		{
			if (!string.IsNullOrEmpty(transactionID) && !string.IsNullOrEmpty(this.persistentDataPath))
			{
				string recordPath = this.GetRecordPath(transactionID);
				try
				{
					Directory.CreateDirectory(recordPath);
				}
				catch (Exception ex)
				{
					this.logger.Log(ex.Message);
				}
			}
		}

		// Token: 0x060000AF RID: 175 RVA: 0x0000336C File Offset: 0x0000156C
		private string GetRecordPath(string transactionID)
		{
			return Path.Combine(this.persistentDataPath, TransactionLog.ComputeHash(transactionID));
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00003380 File Offset: 0x00001580
		internal static string ComputeHash(string transactionID)
		{
			ulong num = 3074457345618258791UL;
			for (int i = 0; i < transactionID.Length; i++)
			{
				num += (ulong)transactionID[i];
				num *= 3074457345618258799UL;
			}
			StringBuilder stringBuilder = new StringBuilder(16);
			foreach (byte b in BitConverter.GetBytes(num))
			{
				stringBuilder.AppendFormat("{0:X2}", b);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0400005A RID: 90
		private readonly ILogger logger;

		// Token: 0x0400005B RID: 91
		private readonly string persistentDataPath;
	}
}
