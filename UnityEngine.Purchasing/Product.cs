﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000D RID: 13
	public class Product
	{
		// Token: 0x0600003A RID: 58 RVA: 0x00002503 File Offset: 0x00000703
		internal Product(ProductDefinition definition, ProductMetadata metadata, string receipt)
		{
			this.definition = definition;
			this.metadata = metadata;
			this.receipt = receipt;
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00002520 File Offset: 0x00000720
		internal Product(ProductDefinition definition, ProductMetadata metadata) : this(definition, metadata, null)
		{
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600003C RID: 60 RVA: 0x0000252B File Offset: 0x0000072B
		// (set) Token: 0x0600003D RID: 61 RVA: 0x00002533 File Offset: 0x00000733
		public ProductDefinition definition
		{
			[CompilerGenerated]
			get
			{
				return this.<definition>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<definition>k__BackingField = value;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600003E RID: 62 RVA: 0x0000253C File Offset: 0x0000073C
		// (set) Token: 0x0600003F RID: 63 RVA: 0x00002544 File Offset: 0x00000744
		public ProductMetadata metadata
		{
			[CompilerGenerated]
			get
			{
				return this.<metadata>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<metadata>k__BackingField = value;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000040 RID: 64 RVA: 0x0000254D File Offset: 0x0000074D
		// (set) Token: 0x06000041 RID: 65 RVA: 0x00002555 File Offset: 0x00000755
		public bool availableToPurchase
		{
			[CompilerGenerated]
			get
			{
				return this.<availableToPurchase>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<availableToPurchase>k__BackingField = value;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000042 RID: 66 RVA: 0x0000255E File Offset: 0x0000075E
		// (set) Token: 0x06000043 RID: 67 RVA: 0x00002566 File Offset: 0x00000766
		public string transactionID
		{
			[CompilerGenerated]
			get
			{
				return this.<transactionID>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<transactionID>k__BackingField = value;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000044 RID: 68 RVA: 0x0000256F File Offset: 0x0000076F
		public bool hasReceipt
		{
			get
			{
				return !string.IsNullOrEmpty(this.receipt);
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000045 RID: 69 RVA: 0x0000257F File Offset: 0x0000077F
		// (set) Token: 0x06000046 RID: 70 RVA: 0x00002587 File Offset: 0x00000787
		public string receipt
		{
			[CompilerGenerated]
			get
			{
				return this.<receipt>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<receipt>k__BackingField = value;
			}
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00002590 File Offset: 0x00000790
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			Product product = obj as Product;
			return product != null && this.definition.Equals(product.definition);
		}

		// Token: 0x06000048 RID: 72 RVA: 0x000025C5 File Offset: 0x000007C5
		public override int GetHashCode()
		{
			return this.definition.GetHashCode();
		}

		// Token: 0x04000016 RID: 22
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ProductDefinition <definition>k__BackingField;

		// Token: 0x04000017 RID: 23
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ProductMetadata <metadata>k__BackingField;

		// Token: 0x04000018 RID: 24
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <availableToPurchase>k__BackingField;

		// Token: 0x04000019 RID: 25
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <transactionID>k__BackingField;

		// Token: 0x0400001A RID: 26
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <receipt>k__BackingField;
	}
}
