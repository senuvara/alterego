﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000007 RID: 7
	public interface IStoreController
	{
		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600001D RID: 29
		ProductCollection products { get; }

		// Token: 0x0600001E RID: 30
		void InitiatePurchase(Product product, string payload);

		// Token: 0x0600001F RID: 31
		void InitiatePurchase(string productId, string payload);

		// Token: 0x06000020 RID: 32
		void InitiatePurchase(Product product);

		// Token: 0x06000021 RID: 33
		void InitiatePurchase(string productId);

		// Token: 0x06000022 RID: 34
		void FetchAdditionalProducts(HashSet<ProductDefinition> products, Action successCallback, Action<InitializationFailureReason> failCallback);

		// Token: 0x06000023 RID: 35
		void ConfirmPendingPurchase(Product product);
	}
}
