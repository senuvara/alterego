﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000005 RID: 5
	public interface IExtensionProvider
	{
		// Token: 0x06000018 RID: 24
		T GetExtension<T>() where T : IStoreExtension;
	}
}
