﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001E RID: 30
	public abstract class UnityPurchasing
	{
		// Token: 0x060000B4 RID: 180 RVA: 0x0000342E File Offset: 0x0000162E
		protected UnityPurchasing()
		{
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x00003436 File Offset: 0x00001636
		public static void Initialize(IStoreListener listener, ConfigurationBuilder builder)
		{
			UnityPurchasing.Initialize(listener, builder, Debug.unityLogger, Application.persistentDataPath, new UnityAnalytics(), builder.factory.GetCatalogProvider());
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x0000345C File Offset: 0x0000165C
		public static void ClearTransactionLog()
		{
			TransactionLog transactionLog = new TransactionLog(Debug.unityLogger, Application.persistentDataPath);
			transactionLog.Clear();
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00003480 File Offset: 0x00001680
		internal static void Initialize(IStoreListener listener, ConfigurationBuilder builder, ILogger logger, string persistentDatapath, IUnityAnalytics analytics, ICatalogProvider catalog)
		{
			TransactionLog tDb = new TransactionLog(logger, persistentDatapath);
			PurchasingManager manager = new PurchasingManager(tDb, logger, builder.factory.service, builder.factory.storeName);
			AnalyticsReporter analytics2 = new AnalyticsReporter(analytics);
			StoreListenerProxy proxy = new StoreListenerProxy(listener, analytics2, builder.factory);
			UnityPurchasing.FetchAndMergeProducts(builder.useCatalogProvider, builder.products, catalog, delegate(HashSet<ProductDefinition> response)
			{
				manager.Initialize(proxy, response);
			});
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x000034F8 File Offset: 0x000016F8
		internal static void FetchAndMergeProducts(bool useCatalog, HashSet<ProductDefinition> localProductSet, ICatalogProvider catalog, Action<HashSet<ProductDefinition>> callback)
		{
			if (useCatalog && catalog != null)
			{
				catalog.FetchProducts(delegate(HashSet<ProductDefinition> cloudProducts)
				{
					HashSet<ProductDefinition> hashSet = new HashSet<ProductDefinition>(localProductSet);
					foreach (ProductDefinition item in cloudProducts)
					{
						hashSet.Remove(item);
						hashSet.Add(item);
					}
					callback(hashSet);
				});
			}
			else
			{
				callback(localProductSet);
			}
		}

		// Token: 0x02000029 RID: 41
		[CompilerGenerated]
		private sealed class <Initialize>c__AnonStorey0
		{
			// Token: 0x060000E1 RID: 225 RVA: 0x0000354D File Offset: 0x0000174D
			public <Initialize>c__AnonStorey0()
			{
			}

			// Token: 0x060000E2 RID: 226 RVA: 0x00003555 File Offset: 0x00001755
			internal void <>m__0(HashSet<ProductDefinition> response)
			{
				this.manager.Initialize(this.proxy, response);
			}

			// Token: 0x04000062 RID: 98
			internal PurchasingManager manager;

			// Token: 0x04000063 RID: 99
			internal StoreListenerProxy proxy;
		}

		// Token: 0x0200002A RID: 42
		[CompilerGenerated]
		private sealed class <FetchAndMergeProducts>c__AnonStorey1
		{
			// Token: 0x060000E3 RID: 227 RVA: 0x00003569 File Offset: 0x00001769
			public <FetchAndMergeProducts>c__AnonStorey1()
			{
			}

			// Token: 0x060000E4 RID: 228 RVA: 0x00003574 File Offset: 0x00001774
			internal void <>m__0(HashSet<ProductDefinition> cloudProducts)
			{
				HashSet<ProductDefinition> hashSet = new HashSet<ProductDefinition>(this.localProductSet);
				foreach (ProductDefinition item in cloudProducts)
				{
					hashSet.Remove(item);
					hashSet.Add(item);
				}
				this.callback(hashSet);
			}

			// Token: 0x04000064 RID: 100
			internal HashSet<ProductDefinition> localProductSet;

			// Token: 0x04000065 RID: 101
			internal Action<HashSet<ProductDefinition>> callback;
		}
	}
}
