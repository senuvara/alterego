﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000010 RID: 16
	public class ProductMetadata
	{
		// Token: 0x06000064 RID: 100 RVA: 0x0000284B File Offset: 0x00000A4B
		public ProductMetadata(string priceString, string title, string description, string currencyCode, decimal localizedPrice)
		{
			this.localizedPriceString = priceString;
			this.localizedTitle = title;
			this.localizedDescription = description;
			this.isoCurrencyCode = currencyCode;
			this.localizedPrice = localizedPrice;
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00002878 File Offset: 0x00000A78
		public ProductMetadata()
		{
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000066 RID: 102 RVA: 0x00002880 File Offset: 0x00000A80
		// (set) Token: 0x06000067 RID: 103 RVA: 0x00002888 File Offset: 0x00000A88
		public string localizedPriceString
		{
			[CompilerGenerated]
			get
			{
				return this.<localizedPriceString>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<localizedPriceString>k__BackingField = value;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000068 RID: 104 RVA: 0x00002891 File Offset: 0x00000A91
		// (set) Token: 0x06000069 RID: 105 RVA: 0x00002899 File Offset: 0x00000A99
		public string localizedTitle
		{
			[CompilerGenerated]
			get
			{
				return this.<localizedTitle>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<localizedTitle>k__BackingField = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600006A RID: 106 RVA: 0x000028A2 File Offset: 0x00000AA2
		// (set) Token: 0x0600006B RID: 107 RVA: 0x000028AA File Offset: 0x00000AAA
		public string localizedDescription
		{
			[CompilerGenerated]
			get
			{
				return this.<localizedDescription>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<localizedDescription>k__BackingField = value;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600006C RID: 108 RVA: 0x000028B3 File Offset: 0x00000AB3
		// (set) Token: 0x0600006D RID: 109 RVA: 0x000028BB File Offset: 0x00000ABB
		public string isoCurrencyCode
		{
			[CompilerGenerated]
			get
			{
				return this.<isoCurrencyCode>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<isoCurrencyCode>k__BackingField = value;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600006E RID: 110 RVA: 0x000028C4 File Offset: 0x00000AC4
		// (set) Token: 0x0600006F RID: 111 RVA: 0x000028CC File Offset: 0x00000ACC
		public decimal localizedPrice
		{
			[CompilerGenerated]
			get
			{
				return this.<localizedPrice>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<localizedPrice>k__BackingField = value;
			}
		}

		// Token: 0x04000026 RID: 38
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <localizedPriceString>k__BackingField;

		// Token: 0x04000027 RID: 39
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <localizedTitle>k__BackingField;

		// Token: 0x04000028 RID: 40
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <localizedDescription>k__BackingField;

		// Token: 0x04000029 RID: 41
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <isoCurrencyCode>k__BackingField;

		// Token: 0x0400002A RID: 42
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private decimal <localizedPrice>k__BackingField;
	}
}
