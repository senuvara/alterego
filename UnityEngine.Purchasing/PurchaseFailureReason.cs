﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000015 RID: 21
	public enum PurchaseFailureReason
	{
		// Token: 0x04000037 RID: 55
		PurchasingUnavailable,
		// Token: 0x04000038 RID: 56
		ExistingPurchasePending,
		// Token: 0x04000039 RID: 57
		ProductUnavailable,
		// Token: 0x0400003A RID: 58
		SignatureInvalid,
		// Token: 0x0400003B RID: 59
		UserCancelled,
		// Token: 0x0400003C RID: 60
		PaymentDeclined,
		// Token: 0x0400003D RID: 61
		DuplicateTransaction,
		// Token: 0x0400003E RID: 62
		Unknown
	}
}
