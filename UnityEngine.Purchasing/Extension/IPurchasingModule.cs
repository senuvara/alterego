﻿using System;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000023 RID: 35
	public interface IPurchasingModule
	{
		// Token: 0x060000CA RID: 202
		void Configure(IPurchasingBinder binder);
	}
}
