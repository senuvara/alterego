﻿using System;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x0200001F RID: 31
	public abstract class AbstractPurchasingModule : IPurchasingModule
	{
		// Token: 0x060000B9 RID: 185 RVA: 0x000035EC File Offset: 0x000017EC
		protected AbstractPurchasingModule()
		{
		}

		// Token: 0x060000BA RID: 186 RVA: 0x000035F4 File Offset: 0x000017F4
		public void Configure(IPurchasingBinder binder)
		{
			this.m_Binder = binder;
			this.Configure();
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00003603 File Offset: 0x00001803
		protected void RegisterStore(string name, IStore a)
		{
			this.m_Binder.RegisterStore(name, a);
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00003612 File Offset: 0x00001812
		protected void BindExtension<T>(T instance) where T : IStoreExtension
		{
			this.m_Binder.RegisterExtension<T>(instance);
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00003620 File Offset: 0x00001820
		protected void BindConfiguration<T>(T instance) where T : IStoreConfiguration
		{
			this.m_Binder.RegisterConfiguration<T>(instance);
		}

		// Token: 0x060000BE RID: 190
		public abstract void Configure();

		// Token: 0x0400005C RID: 92
		protected IPurchasingBinder m_Binder;
	}
}
