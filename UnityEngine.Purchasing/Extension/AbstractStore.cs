﻿using System;
using System.Collections.ObjectModel;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000020 RID: 32
	public abstract class AbstractStore : IStore
	{
		// Token: 0x060000BF RID: 191 RVA: 0x0000362E File Offset: 0x0000182E
		protected AbstractStore()
		{
		}

		// Token: 0x060000C0 RID: 192
		public abstract void Initialize(IStoreCallback callback);

		// Token: 0x060000C1 RID: 193
		public abstract void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products);

		// Token: 0x060000C2 RID: 194
		public abstract void Purchase(ProductDefinition product, string developerPayload);

		// Token: 0x060000C3 RID: 195
		public abstract void FinishTransaction(ProductDefinition product, string transactionId);
	}
}
