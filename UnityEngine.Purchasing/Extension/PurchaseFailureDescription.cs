﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000014 RID: 20
	public class PurchaseFailureDescription
	{
		// Token: 0x0600007A RID: 122 RVA: 0x00002945 File Offset: 0x00000B45
		public PurchaseFailureDescription(string productId, PurchaseFailureReason reason, string message)
		{
			this.productId = productId;
			this.reason = reason;
			this.message = message;
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600007B RID: 123 RVA: 0x00002962 File Offset: 0x00000B62
		// (set) Token: 0x0600007C RID: 124 RVA: 0x0000296A File Offset: 0x00000B6A
		public string productId
		{
			[CompilerGenerated]
			get
			{
				return this.<productId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<productId>k__BackingField = value;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600007D RID: 125 RVA: 0x00002973 File Offset: 0x00000B73
		// (set) Token: 0x0600007E RID: 126 RVA: 0x0000297B File Offset: 0x00000B7B
		public PurchaseFailureReason reason
		{
			[CompilerGenerated]
			get
			{
				return this.<reason>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<reason>k__BackingField = value;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600007F RID: 127 RVA: 0x00002984 File Offset: 0x00000B84
		// (set) Token: 0x06000080 RID: 128 RVA: 0x0000298C File Offset: 0x00000B8C
		public string message
		{
			[CompilerGenerated]
			get
			{
				return this.<message>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<message>k__BackingField = value;
			}
		}

		// Token: 0x04000033 RID: 51
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <productId>k__BackingField;

		// Token: 0x04000034 RID: 52
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private PurchaseFailureReason <reason>k__BackingField;

		// Token: 0x04000035 RID: 53
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <message>k__BackingField;
	}
}
