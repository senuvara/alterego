﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000022 RID: 34
	public interface IPurchasingBinder
	{
		// Token: 0x060000C5 RID: 197
		void RegisterStore(string name, IStore a);

		// Token: 0x060000C6 RID: 198
		void RegisterExtension<T>(T instance) where T : IStoreExtension;

		// Token: 0x060000C7 RID: 199
		void RegisterConfiguration<T>(T instance) where T : IStoreConfiguration;

		// Token: 0x060000C8 RID: 200
		void SetCatalogProvider(ICatalogProvider provider);

		// Token: 0x060000C9 RID: 201
		void SetCatalogProviderFunction(Action<Action<HashSet<ProductDefinition>>> func);
	}
}
