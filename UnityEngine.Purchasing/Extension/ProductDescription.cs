﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000028 RID: 40
	public class ProductDescription
	{
		// Token: 0x060000D6 RID: 214 RVA: 0x00003636 File Offset: 0x00001836
		public ProductDescription(string id, ProductMetadata metadata, string receipt, string transactionId)
		{
			this.storeSpecificId = id;
			this.metadata = metadata;
			this.receipt = receipt;
			this.transactionId = transactionId;
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x0000365B File Offset: 0x0000185B
		public ProductDescription(string id, ProductMetadata metadata, string receipt, string transactionId, ProductType type) : this(id, metadata, receipt, transactionId)
		{
			this.type = type;
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x00003670 File Offset: 0x00001870
		public ProductDescription(string id, ProductMetadata metadata) : this(id, metadata, null, null)
		{
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000D9 RID: 217 RVA: 0x0000367C File Offset: 0x0000187C
		// (set) Token: 0x060000DA RID: 218 RVA: 0x00003684 File Offset: 0x00001884
		public string storeSpecificId
		{
			[CompilerGenerated]
			get
			{
				return this.<storeSpecificId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<storeSpecificId>k__BackingField = value;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000DB RID: 219 RVA: 0x0000368D File Offset: 0x0000188D
		// (set) Token: 0x060000DC RID: 220 RVA: 0x00003695 File Offset: 0x00001895
		public ProductMetadata metadata
		{
			[CompilerGenerated]
			get
			{
				return this.<metadata>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<metadata>k__BackingField = value;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000DD RID: 221 RVA: 0x0000369E File Offset: 0x0000189E
		// (set) Token: 0x060000DE RID: 222 RVA: 0x000036A6 File Offset: 0x000018A6
		public string receipt
		{
			[CompilerGenerated]
			get
			{
				return this.<receipt>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<receipt>k__BackingField = value;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000DF RID: 223 RVA: 0x000036AF File Offset: 0x000018AF
		// (set) Token: 0x060000E0 RID: 224 RVA: 0x000036B7 File Offset: 0x000018B7
		public string transactionId
		{
			[CompilerGenerated]
			get
			{
				return this.<transactionId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<transactionId>k__BackingField = value;
			}
		}

		// Token: 0x0400005D RID: 93
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <storeSpecificId>k__BackingField;

		// Token: 0x0400005E RID: 94
		public ProductType type;

		// Token: 0x0400005F RID: 95
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ProductMetadata <metadata>k__BackingField;

		// Token: 0x04000060 RID: 96
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <receipt>k__BackingField;

		// Token: 0x04000061 RID: 97
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <transactionId>k__BackingField;
	}
}
