﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000021 RID: 33
	public interface ICatalogProvider
	{
		// Token: 0x060000C4 RID: 196
		void FetchProducts(Action<HashSet<ProductDefinition>> callback);
	}
}
