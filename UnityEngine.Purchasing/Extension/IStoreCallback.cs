﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000025 RID: 37
	public interface IStoreCallback
	{
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000CF RID: 207
		ProductCollection products { get; }

		// Token: 0x060000D0 RID: 208
		void OnSetupFailed(InitializationFailureReason reason);

		// Token: 0x060000D1 RID: 209
		void OnProductsRetrieved(List<ProductDescription> products);

		// Token: 0x060000D2 RID: 210
		void OnPurchaseSucceeded(string storeSpecificId, string receipt, string transactionIdentifier);

		// Token: 0x060000D3 RID: 211
		void OnPurchaseFailed(PurchaseFailureDescription desc);

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000D4 RID: 212
		// (set) Token: 0x060000D5 RID: 213
		bool useTransactionLog { get; set; }
	}
}
