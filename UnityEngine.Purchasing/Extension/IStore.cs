﻿using System;
using System.Collections.ObjectModel;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000024 RID: 36
	public interface IStore
	{
		// Token: 0x060000CB RID: 203
		void Initialize(IStoreCallback callback);

		// Token: 0x060000CC RID: 204
		void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products);

		// Token: 0x060000CD RID: 205
		void Purchase(ProductDefinition product, string developerPayload);

		// Token: 0x060000CE RID: 206
		void FinishTransaction(ProductDefinition product, string transactionId);
	}
}
