﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000018 RID: 24
	internal class PurchasingManager : IStoreCallback, IStoreController
	{
		// Token: 0x0600008E RID: 142 RVA: 0x00002B6A File Offset: 0x00000D6A
		internal PurchasingManager(TransactionLog tDb, ILogger logger, IStore store, string storeName)
		{
			this.m_TransactionLog = tDb;
			this.m_Store = store;
			this.m_Logger = logger;
			this.m_StoreName = storeName;
			this.useTransactionLog = true;
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x0600008F RID: 143 RVA: 0x00002B96 File Offset: 0x00000D96
		// (set) Token: 0x06000090 RID: 144 RVA: 0x00002B9E File Offset: 0x00000D9E
		public bool useTransactionLog
		{
			[CompilerGenerated]
			get
			{
				return this.<useTransactionLog>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<useTransactionLog>k__BackingField = value;
			}
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00002BA7 File Offset: 0x00000DA7
		public void InitiatePurchase(Product product)
		{
			this.InitiatePurchase(product, string.Empty);
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00002BB5 File Offset: 0x00000DB5
		public void InitiatePurchase(string productId)
		{
			this.InitiatePurchase(productId, string.Empty);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00002BC4 File Offset: 0x00000DC4
		public void InitiatePurchase(Product product, string developerPayload)
		{
			if (product == null)
			{
				this.m_Logger.Log("Trying to purchase null Product");
				return;
			}
			if (!product.availableToPurchase)
			{
				this.m_Listener.OnPurchaseFailed(product, PurchaseFailureReason.ProductUnavailable);
				return;
			}
			this.m_Store.Purchase(product.definition, developerPayload);
			this.m_Logger.Log("purchase({0})", product.definition.id);
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00002C30 File Offset: 0x00000E30
		public void InitiatePurchase(string purchasableId, string developerPayload)
		{
			Product product = this.products.WithID(purchasableId);
			if (product == null)
			{
				this.m_Logger.LogWarning("Unable to purchase unknown product with id: {0}", purchasableId);
			}
			this.InitiatePurchase(product, developerPayload);
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00002C6C File Offset: 0x00000E6C
		public void ConfirmPendingPurchase(Product product)
		{
			if (product == null)
			{
				this.m_Logger.Log("Unable to confirm purchase with null Product");
				return;
			}
			if (string.IsNullOrEmpty(product.transactionID))
			{
				this.m_Logger.Log("Unable to confirm purchase; Product has missing or empty transactionID");
				return;
			}
			if (this.useTransactionLog)
			{
				this.m_TransactionLog.Record(product.transactionID);
			}
			this.m_Store.FinishTransaction(product.definition, product.transactionID);
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000096 RID: 150 RVA: 0x00002CE4 File Offset: 0x00000EE4
		// (set) Token: 0x06000097 RID: 151 RVA: 0x00002CEC File Offset: 0x00000EEC
		public ProductCollection products
		{
			[CompilerGenerated]
			get
			{
				return this.<products>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<products>k__BackingField = value;
			}
		}

		// Token: 0x06000098 RID: 152 RVA: 0x00002CF8 File Offset: 0x00000EF8
		public void OnPurchaseSucceeded(string id, string receipt, string transactionId)
		{
			Product product = this.products.WithStoreSpecificID(id);
			if (product == null)
			{
				ProductDefinition definition = new ProductDefinition(id, ProductType.NonConsumable);
				product = new Product(definition, new ProductMetadata());
			}
			string receipt2 = this.FormatUnifiedReceipt(receipt, transactionId);
			product.receipt = receipt2;
			product.transactionID = transactionId;
			this.ProcessPurchaseIfNew(product);
		}

		// Token: 0x06000099 RID: 153 RVA: 0x00002D4A File Offset: 0x00000F4A
		public void OnSetupFailed(InitializationFailureReason reason)
		{
			if (this.initialized)
			{
				if (this.m_AdditionalProductsFailCallback != null)
				{
					this.m_AdditionalProductsFailCallback(reason);
				}
			}
			else
			{
				this.m_Listener.OnInitializeFailed(reason);
			}
		}

		// Token: 0x0600009A RID: 154 RVA: 0x00002D80 File Offset: 0x00000F80
		public void OnPurchaseFailed(PurchaseFailureDescription description)
		{
			Product product = this.products.WithStoreSpecificID(description.productId);
			if (product == null)
			{
				this.m_Logger.LogError("Failed to purchase unknown product {0}", description.productId);
				return;
			}
			this.m_Logger.Log("onPurchaseFailedEvent({0})", product.definition.id);
			this.m_Listener.OnPurchaseFailed(product, description.reason);
		}

		// Token: 0x0600009B RID: 155 RVA: 0x00002DEC File Offset: 0x00000FEC
		public void OnProductsRetrieved(List<ProductDescription> products)
		{
			HashSet<Product> hashSet = new HashSet<Product>();
			foreach (ProductDescription productDescription in products)
			{
				Product product = this.products.WithStoreSpecificID(productDescription.storeSpecificId);
				if (product == null)
				{
					ProductDefinition definition = new ProductDefinition(productDescription.storeSpecificId, productDescription.storeSpecificId, productDescription.type);
					product = new Product(definition, productDescription.metadata);
					hashSet.Add(product);
				}
				product.availableToPurchase = true;
				product.metadata = productDescription.metadata;
				product.transactionID = productDescription.transactionId;
				if (!string.IsNullOrEmpty(productDescription.receipt))
				{
					product.receipt = this.FormatUnifiedReceipt(productDescription.receipt, productDescription.transactionId);
				}
			}
			if (hashSet.Count > 0)
			{
				this.products.AddProducts(hashSet);
			}
			this.CheckForInitialization();
			foreach (Product product2 in this.products.set)
			{
				if (!string.IsNullOrEmpty(product2.receipt) && !string.IsNullOrEmpty(product2.transactionID))
				{
					this.ProcessPurchaseIfNew(product2);
				}
			}
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00002F64 File Offset: 0x00001164
		public void FetchAdditionalProducts(HashSet<ProductDefinition> products, Action successCallback, Action<InitializationFailureReason> failCallback)
		{
			this.m_AdditionalProductsCallback = successCallback;
			this.m_AdditionalProductsFailCallback = failCallback;
			this.products.AddProducts(from x in products
			select new Product(x, new ProductMetadata()));
			this.m_Store.RetrieveProducts(new ReadOnlyCollection<ProductDefinition>(products.ToList<ProductDefinition>()));
		}

		// Token: 0x0600009D RID: 157 RVA: 0x00002FC4 File Offset: 0x000011C4
		private void ProcessPurchaseIfNew(Product product)
		{
			if (this.useTransactionLog && this.m_TransactionLog.HasRecordOf(product.transactionID))
			{
				this.m_Logger.Log("Already recorded transaction " + product.transactionID);
				this.m_Store.FinishTransaction(product.definition, product.transactionID);
				return;
			}
			PurchaseEventArgs e = new PurchaseEventArgs(product);
			if (this.m_Listener.ProcessPurchase(e) == PurchaseProcessingResult.Complete)
			{
				this.ConfirmPendingPurchase(product);
			}
		}

		// Token: 0x0600009E RID: 158 RVA: 0x00003044 File Offset: 0x00001244
		private void CheckForInitialization()
		{
			if (!this.initialized)
			{
				bool flag = false;
				foreach (Product product in this.products.set)
				{
					if (!product.availableToPurchase)
					{
						this.m_Logger.LogFormat(LogType.Warning, "Unavailable product {0} -{1}", new object[]
						{
							product.definition.id,
							product.definition.storeSpecificId
						});
					}
					else
					{
						flag = true;
					}
				}
				if (flag)
				{
					this.m_Listener.OnInitialized(this);
				}
				else
				{
					this.OnSetupFailed(InitializationFailureReason.NoProductsAvailable);
				}
				this.initialized = true;
			}
			else if (this.m_AdditionalProductsCallback != null)
			{
				this.m_AdditionalProductsCallback();
			}
		}

		// Token: 0x0600009F RID: 159 RVA: 0x00003130 File Offset: 0x00001330
		public void Initialize(IInternalStoreListener listener, HashSet<ProductDefinition> products)
		{
			this.m_Listener = listener;
			this.m_Store.Initialize(this);
			Product[] products2 = (from x in products
			select new Product(x, new ProductMetadata())).ToArray<Product>();
			this.products = new ProductCollection(products2);
			ReadOnlyCollection<ProductDefinition> products3 = new ReadOnlyCollection<ProductDefinition>(products.ToList<ProductDefinition>());
			this.m_Store.RetrieveProducts(products3);
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x000031A0 File Offset: 0x000013A0
		private string FormatUnifiedReceipt(string platformReceipt, string transactionId)
		{
			PurchasingManager.UnifiedReceipt obj = new PurchasingManager.UnifiedReceipt
			{
				Store = this.m_StoreName,
				TransactionID = transactionId,
				Payload = platformReceipt
			};
			return JsonUtility.ToJson(obj);
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x000031D5 File Offset: 0x000013D5
		[CompilerGenerated]
		private static Product <FetchAdditionalProducts>m__0(ProductDefinition x)
		{
			return new Product(x, new ProductMetadata());
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x000031E2 File Offset: 0x000013E2
		[CompilerGenerated]
		private static Product <Initialize>m__1(ProductDefinition x)
		{
			return new Product(x, new ProductMetadata());
		}

		// Token: 0x04000047 RID: 71
		private IStore m_Store;

		// Token: 0x04000048 RID: 72
		private IInternalStoreListener m_Listener;

		// Token: 0x04000049 RID: 73
		private ILogger m_Logger;

		// Token: 0x0400004A RID: 74
		private TransactionLog m_TransactionLog;

		// Token: 0x0400004B RID: 75
		private string m_StoreName;

		// Token: 0x0400004C RID: 76
		private Action m_AdditionalProductsCallback;

		// Token: 0x0400004D RID: 77
		private Action<InitializationFailureReason> m_AdditionalProductsFailCallback;

		// Token: 0x0400004E RID: 78
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <useTransactionLog>k__BackingField;

		// Token: 0x0400004F RID: 79
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ProductCollection <products>k__BackingField;

		// Token: 0x04000050 RID: 80
		private bool initialized;

		// Token: 0x04000051 RID: 81
		[CompilerGenerated]
		private static Func<ProductDefinition, Product> <>f__am$cache0;

		// Token: 0x04000052 RID: 82
		[CompilerGenerated]
		private static Func<ProductDefinition, Product> <>f__am$cache1;

		// Token: 0x02000019 RID: 25
		[Serializable]
		private class UnifiedReceipt
		{
			// Token: 0x060000A3 RID: 163 RVA: 0x000031EF File Offset: 0x000013EF
			public UnifiedReceipt()
			{
			}

			// Token: 0x04000053 RID: 83
			public string Store;

			// Token: 0x04000054 RID: 84
			public string TransactionID;

			// Token: 0x04000055 RID: 85
			public string Payload;
		}
	}
}
