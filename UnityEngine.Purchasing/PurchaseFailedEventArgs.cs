﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000013 RID: 19
	public class PurchaseFailedEventArgs
	{
		// Token: 0x06000073 RID: 115 RVA: 0x000028F5 File Offset: 0x00000AF5
		internal PurchaseFailedEventArgs(Product purchasedProduct, PurchaseFailureReason reason, string message)
		{
			this.purchasedProduct = purchasedProduct;
			this.reason = reason;
			this.message = message;
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000074 RID: 116 RVA: 0x00002912 File Offset: 0x00000B12
		// (set) Token: 0x06000075 RID: 117 RVA: 0x0000291A File Offset: 0x00000B1A
		public Product purchasedProduct
		{
			[CompilerGenerated]
			get
			{
				return this.<purchasedProduct>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<purchasedProduct>k__BackingField = value;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000076 RID: 118 RVA: 0x00002923 File Offset: 0x00000B23
		// (set) Token: 0x06000077 RID: 119 RVA: 0x0000292B File Offset: 0x00000B2B
		public PurchaseFailureReason reason
		{
			[CompilerGenerated]
			get
			{
				return this.<reason>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<reason>k__BackingField = value;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000078 RID: 120 RVA: 0x00002934 File Offset: 0x00000B34
		// (set) Token: 0x06000079 RID: 121 RVA: 0x0000293C File Offset: 0x00000B3C
		public string message
		{
			[CompilerGenerated]
			get
			{
				return this.<message>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<message>k__BackingField = value;
			}
		}

		// Token: 0x04000030 RID: 48
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Product <purchasedProduct>k__BackingField;

		// Token: 0x04000031 RID: 49
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private PurchaseFailureReason <reason>k__BackingField;

		// Token: 0x04000032 RID: 50
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <message>k__BackingField;
	}
}
