﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000E RID: 14
	public class ProductCollection
	{
		// Token: 0x06000049 RID: 73 RVA: 0x000025D2 File Offset: 0x000007D2
		internal ProductCollection(Product[] products)
		{
			this.AddProducts(products);
		}

		// Token: 0x0600004A RID: 74 RVA: 0x000025EC File Offset: 0x000007EC
		internal void AddProducts(IEnumerable<Product> products)
		{
			this.m_ProductSet.UnionWith(products);
			this.m_Products = this.m_ProductSet.ToArray<Product>();
			this.m_IdToProduct = this.m_Products.ToDictionary((Product x) => x.definition.id);
			this.m_StoreSpecificIdToProduct = this.m_Products.ToDictionary((Product x) => x.definition.storeSpecificId);
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600004B RID: 75 RVA: 0x00002672 File Offset: 0x00000872
		public HashSet<Product> set
		{
			get
			{
				return this.m_ProductSet;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600004C RID: 76 RVA: 0x0000267A File Offset: 0x0000087A
		public Product[] all
		{
			get
			{
				return this.m_Products;
			}
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002684 File Offset: 0x00000884
		public Product WithID(string id)
		{
			Product result = null;
			this.m_IdToProduct.TryGetValue(id, out result);
			return result;
		}

		// Token: 0x0600004E RID: 78 RVA: 0x000026A4 File Offset: 0x000008A4
		public Product WithStoreSpecificID(string id)
		{
			Product result = null;
			this.m_StoreSpecificIdToProduct.TryGetValue(id, out result);
			return result;
		}

		// Token: 0x0600004F RID: 79 RVA: 0x000026C3 File Offset: 0x000008C3
		[CompilerGenerated]
		private static string <AddProducts>m__0(Product x)
		{
			return x.definition.id;
		}

		// Token: 0x06000050 RID: 80 RVA: 0x000026D0 File Offset: 0x000008D0
		[CompilerGenerated]
		private static string <AddProducts>m__1(Product x)
		{
			return x.definition.storeSpecificId;
		}

		// Token: 0x0400001B RID: 27
		private Dictionary<string, Product> m_IdToProduct;

		// Token: 0x0400001C RID: 28
		private Dictionary<string, Product> m_StoreSpecificIdToProduct;

		// Token: 0x0400001D RID: 29
		private Product[] m_Products;

		// Token: 0x0400001E RID: 30
		private HashSet<Product> m_ProductSet = new HashSet<Product>();

		// Token: 0x0400001F RID: 31
		[CompilerGenerated]
		private static Func<Product, string> <>f__am$cache0;

		// Token: 0x04000020 RID: 32
		[CompilerGenerated]
		private static Func<Product, string> <>f__am$cache1;
	}
}
