﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000B RID: 11
	[Serializable]
	public class PayoutDefinition
	{
		// Token: 0x0600002A RID: 42 RVA: 0x00002354 File Offset: 0x00000554
		public PayoutDefinition()
		{
		}

		// Token: 0x0600002B RID: 43 RVA: 0x00002372 File Offset: 0x00000572
		public PayoutDefinition(string typeString, string subtype, double quantity) : this(typeString, subtype, quantity, string.Empty)
		{
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00002384 File Offset: 0x00000584
		public PayoutDefinition(string typeString, string subtype, double quantity, string data)
		{
			PayoutType type = PayoutType.Other;
			if (Enum.IsDefined(typeof(PayoutType), typeString))
			{
				type = (PayoutType)Enum.Parse(typeof(PayoutType), typeString);
			}
			this.type = type;
			this.subtype = subtype;
			this.quantity = quantity;
			this.data = data;
		}

		// Token: 0x0600002D RID: 45 RVA: 0x000023F7 File Offset: 0x000005F7
		public PayoutDefinition(string subtype, double quantity) : this(PayoutType.Other, subtype, quantity, string.Empty)
		{
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002407 File Offset: 0x00000607
		public PayoutDefinition(string subtype, double quantity, string data) : this(PayoutType.Other, subtype, quantity, data)
		{
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002413 File Offset: 0x00000613
		public PayoutDefinition(PayoutType type, string subtype, double quantity) : this(type, subtype, quantity, string.Empty)
		{
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002423 File Offset: 0x00000623
		public PayoutDefinition(PayoutType type, string subtype, double quantity, string data)
		{
			this.type = type;
			this.subtype = subtype;
			this.quantity = quantity;
			this.data = data;
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000031 RID: 49 RVA: 0x0000245E File Offset: 0x0000065E
		// (set) Token: 0x06000032 RID: 50 RVA: 0x00002466 File Offset: 0x00000666
		public PayoutType type
		{
			get
			{
				return this.m_Type;
			}
			private set
			{
				this.m_Type = value;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000033 RID: 51 RVA: 0x0000246F File Offset: 0x0000066F
		public string typeString
		{
			get
			{
				return this.m_Type.ToString();
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000034 RID: 52 RVA: 0x00002482 File Offset: 0x00000682
		// (set) Token: 0x06000035 RID: 53 RVA: 0x0000248A File Offset: 0x0000068A
		public string subtype
		{
			get
			{
				return this.m_Subtype;
			}
			private set
			{
				if (value.Length > 64)
				{
					throw new ArgumentException(string.Format("subtype connot be longer than {0}", 64));
				}
				this.m_Subtype = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000036 RID: 54 RVA: 0x000024B7 File Offset: 0x000006B7
		// (set) Token: 0x06000037 RID: 55 RVA: 0x000024BF File Offset: 0x000006BF
		public double quantity
		{
			get
			{
				return this.m_Quantity;
			}
			private set
			{
				this.m_Quantity = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000038 RID: 56 RVA: 0x000024C8 File Offset: 0x000006C8
		// (set) Token: 0x06000039 RID: 57 RVA: 0x000024D0 File Offset: 0x000006D0
		public string data
		{
			get
			{
				return this.m_Data;
			}
			private set
			{
				if (value.Length > 1024)
				{
					throw new ArgumentException(string.Format("data cannot be longer than {0}", 1024));
				}
				this.m_Data = value;
			}
		}

		// Token: 0x0400000B RID: 11
		[SerializeField]
		private PayoutType m_Type;

		// Token: 0x0400000C RID: 12
		[SerializeField]
		private string m_Subtype = string.Empty;

		// Token: 0x0400000D RID: 13
		[SerializeField]
		private double m_Quantity;

		// Token: 0x0400000E RID: 14
		[SerializeField]
		private string m_Data = string.Empty;

		// Token: 0x0400000F RID: 15
		public const int MaxSubtypeLength = 64;

		// Token: 0x04000010 RID: 16
		public const int MaxDataLength = 1024;
	}
}
