﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000004 RID: 4
	public class ConfigurationBuilder
	{
		// Token: 0x0600000A RID: 10 RVA: 0x000021F8 File Offset: 0x000003F8
		internal ConfigurationBuilder(PurchasingFactory factory)
		{
			this.m_Factory = factory;
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600000B RID: 11 RVA: 0x00002212 File Offset: 0x00000412
		// (set) Token: 0x0600000C RID: 12 RVA: 0x0000221A File Offset: 0x0000041A
		[Obsolete("This property has been renamed 'useCatalogProvider'", false)]
		public bool useCloudCatalog
		{
			[CompilerGenerated]
			get
			{
				return this.<useCloudCatalog>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<useCloudCatalog>k__BackingField = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000D RID: 13 RVA: 0x00002223 File Offset: 0x00000423
		// (set) Token: 0x0600000E RID: 14 RVA: 0x0000222B File Offset: 0x0000042B
		public bool useCatalogProvider
		{
			[CompilerGenerated]
			get
			{
				return this.<useCatalogProvider>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<useCatalogProvider>k__BackingField = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600000F RID: 15 RVA: 0x00002234 File Offset: 0x00000434
		public HashSet<ProductDefinition> products
		{
			get
			{
				return this.m_Products;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000010 RID: 16 RVA: 0x0000223C File Offset: 0x0000043C
		internal PurchasingFactory factory
		{
			get
			{
				return this.m_Factory;
			}
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002244 File Offset: 0x00000444
		public T Configure<T>() where T : IStoreConfiguration
		{
			return this.m_Factory.GetConfig<T>();
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002254 File Offset: 0x00000454
		public static ConfigurationBuilder Instance(IPurchasingModule first, params IPurchasingModule[] rest)
		{
			PurchasingFactory factory = new PurchasingFactory(first, rest);
			return new ConfigurationBuilder(factory);
		}

		// Token: 0x06000013 RID: 19 RVA: 0x0000226F File Offset: 0x0000046F
		public ConfigurationBuilder AddProduct(string id, ProductType type)
		{
			return this.AddProduct(id, type, null);
		}

		// Token: 0x06000014 RID: 20 RVA: 0x0000227A File Offset: 0x0000047A
		public ConfigurationBuilder AddProduct(string id, ProductType type, IDs storeIDs)
		{
			return this.AddProduct(id, type, storeIDs, null);
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002288 File Offset: 0x00000488
		public ConfigurationBuilder AddProduct(string id, ProductType type, IDs storeIDs, PayoutDefinition payout)
		{
			return this.AddProduct(id, type, storeIDs, new List<PayoutDefinition>
			{
				payout
			});
		}

		// Token: 0x06000016 RID: 22 RVA: 0x000022B0 File Offset: 0x000004B0
		public ConfigurationBuilder AddProduct(string id, ProductType type, IDs storeIDs, IEnumerable<PayoutDefinition> payouts)
		{
			string storeSpecificId = id;
			if (storeIDs != null)
			{
				storeSpecificId = storeIDs.SpecificIDForStore(this.factory.storeName, id);
			}
			ProductDefinition productDefinition = new ProductDefinition(id, storeSpecificId, type);
			productDefinition.SetPayouts(payouts);
			this.m_Products.Add(productDefinition);
			return this;
		}

		// Token: 0x06000017 RID: 23 RVA: 0x000022F8 File Offset: 0x000004F8
		public ConfigurationBuilder AddProducts(IEnumerable<ProductDefinition> products)
		{
			foreach (ProductDefinition item in products)
			{
				this.m_Products.Add(item);
			}
			return this;
		}

		// Token: 0x04000003 RID: 3
		private PurchasingFactory m_Factory;

		// Token: 0x04000004 RID: 4
		private HashSet<ProductDefinition> m_Products = new HashSet<ProductDefinition>();

		// Token: 0x04000005 RID: 5
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <useCloudCatalog>k__BackingField;

		// Token: 0x04000006 RID: 6
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <useCatalogProvider>k__BackingField;
	}
}
