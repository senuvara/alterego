﻿using System;

namespace UnityEngine
{
	// Token: 0x02000027 RID: 39
	public enum AudioReverbPreset
	{
		// Token: 0x0400006B RID: 107
		Off,
		// Token: 0x0400006C RID: 108
		Generic,
		// Token: 0x0400006D RID: 109
		PaddedCell,
		// Token: 0x0400006E RID: 110
		Room,
		// Token: 0x0400006F RID: 111
		Bathroom,
		// Token: 0x04000070 RID: 112
		Livingroom,
		// Token: 0x04000071 RID: 113
		Stoneroom,
		// Token: 0x04000072 RID: 114
		Auditorium,
		// Token: 0x04000073 RID: 115
		Concerthall,
		// Token: 0x04000074 RID: 116
		Cave,
		// Token: 0x04000075 RID: 117
		Arena,
		// Token: 0x04000076 RID: 118
		Hangar,
		// Token: 0x04000077 RID: 119
		CarpetedHallway,
		// Token: 0x04000078 RID: 120
		Hallway,
		// Token: 0x04000079 RID: 121
		StoneCorridor,
		// Token: 0x0400007A RID: 122
		Alley,
		// Token: 0x0400007B RID: 123
		Forest,
		// Token: 0x0400007C RID: 124
		City,
		// Token: 0x0400007D RID: 125
		Mountains,
		// Token: 0x0400007E RID: 126
		Quarry,
		// Token: 0x0400007F RID: 127
		Plain,
		// Token: 0x04000080 RID: 128
		ParkingLot,
		// Token: 0x04000081 RID: 129
		SewerPipe,
		// Token: 0x04000082 RID: 130
		Underwater,
		// Token: 0x04000083 RID: 131
		Drugged,
		// Token: 0x04000084 RID: 132
		Dizzy,
		// Token: 0x04000085 RID: 133
		Psychotic,
		// Token: 0x04000086 RID: 134
		User
	}
}
