﻿using System;

namespace UnityEngine
{
	// Token: 0x02000024 RID: 36
	public enum AudioRolloffMode
	{
		// Token: 0x04000060 RID: 96
		Logarithmic,
		// Token: 0x04000061 RID: 97
		Linear,
		// Token: 0x04000062 RID: 98
		Custom
	}
}
