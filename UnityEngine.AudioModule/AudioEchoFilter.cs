﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200002C RID: 44
	[RequireComponent(typeof(AudioBehaviour))]
	public sealed class AudioEchoFilter : Behaviour
	{
		// Token: 0x060001DA RID: 474 RVA: 0x000041EE File Offset: 0x000023EE
		public AudioEchoFilter()
		{
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060001DB RID: 475
		// (set) Token: 0x060001DC RID: 476
		public extern float delay { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060001DD RID: 477
		// (set) Token: 0x060001DE RID: 478
		public extern float decayRatio { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060001DF RID: 479
		// (set) Token: 0x060001E0 RID: 480
		public extern float dryMix { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060001E1 RID: 481
		// (set) Token: 0x060001E2 RID: 482
		public extern float wetMix { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
