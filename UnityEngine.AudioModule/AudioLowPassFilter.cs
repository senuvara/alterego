﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000029 RID: 41
	[RequireComponent(typeof(AudioBehaviour))]
	public sealed class AudioLowPassFilter : Behaviour
	{
		// Token: 0x060001CB RID: 459 RVA: 0x000041EE File Offset: 0x000023EE
		public AudioLowPassFilter()
		{
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060001CC RID: 460
		// (set) Token: 0x060001CD RID: 461
		public extern float cutoffFrequency { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060001CE RID: 462
		// (set) Token: 0x060001CF RID: 463
		public extern AnimationCurve customCutoffCurve { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060001D0 RID: 464
		// (set) Token: 0x060001D1 RID: 465
		public extern float lowpassResonanceQ { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
