﻿using System;

namespace UnityEngine
{
	// Token: 0x02000002 RID: 2
	internal class AudioExtensionDefinition
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public AudioExtensionDefinition(AudioExtensionDefinition definition)
		{
			this.assemblyName = definition.assemblyName;
			this.extensionNamespace = definition.extensionNamespace;
			this.extensionTypeName = definition.extensionTypeName;
			this.extensionType = this.GetExtensionType();
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002089 File Offset: 0x00000289
		public AudioExtensionDefinition(string assemblyNameIn, string extensionNamespaceIn, string extensionTypeNameIn)
		{
			this.assemblyName = assemblyNameIn;
			this.extensionNamespace = extensionNamespaceIn;
			this.extensionTypeName = extensionTypeNameIn;
			this.extensionType = this.GetExtensionType();
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020B4 File Offset: 0x000002B4
		public Type GetExtensionType()
		{
			if (this.extensionType == null)
			{
				this.extensionType = Type.GetType(string.Concat(new string[]
				{
					this.extensionNamespace,
					".",
					this.extensionTypeName,
					", ",
					this.assemblyName
				}));
			}
			return this.extensionType;
		}

		// Token: 0x04000001 RID: 1
		private string assemblyName;

		// Token: 0x04000002 RID: 2
		private string extensionNamespace;

		// Token: 0x04000003 RID: 3
		private string extensionTypeName;

		// Token: 0x04000004 RID: 4
		private Type extensionType;
	}
}
