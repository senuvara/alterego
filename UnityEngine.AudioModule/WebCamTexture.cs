﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000015 RID: 21
	[NativeHeader("Runtime/Video/BaseWebCamTexture.h")]
	public sealed class WebCamTexture : Texture
	{
		// Token: 0x060000BD RID: 189 RVA: 0x00003CBB File Offset: 0x00001EBB
		public WebCamTexture(string deviceName, int requestedWidth, int requestedHeight, int requestedFPS)
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, deviceName, requestedWidth, requestedHeight, requestedFPS);
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00003CCF File Offset: 0x00001ECF
		public WebCamTexture(string deviceName, int requestedWidth, int requestedHeight)
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, deviceName, requestedWidth, requestedHeight, 0);
		}

		// Token: 0x060000BF RID: 191 RVA: 0x00003CE2 File Offset: 0x00001EE2
		public WebCamTexture(string deviceName)
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, deviceName, 0, 0, 0);
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00003CF5 File Offset: 0x00001EF5
		public WebCamTexture(int requestedWidth, int requestedHeight, int requestedFPS)
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, "", requestedWidth, requestedHeight, requestedFPS);
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00003D0C File Offset: 0x00001F0C
		public WebCamTexture(int requestedWidth, int requestedHeight)
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, "", requestedWidth, requestedHeight, 0);
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x00003D23 File Offset: 0x00001F23
		public WebCamTexture()
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, "", 0, 0, 0);
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x060000C3 RID: 195 RVA: 0x00003D3C File Offset: 0x00001F3C
		// (set) Token: 0x060000C4 RID: 196 RVA: 0x00003D82 File Offset: 0x00001F82
		public Vector2? autoFocusPoint
		{
			get
			{
				return (this.internalAutoFocusPoint.x >= 0f) ? new Vector2?(this.internalAutoFocusPoint) : null;
			}
			set
			{
				this.internalAutoFocusPoint = ((value != null) ? value.Value : new Vector2(-1f, -1f));
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x060000C5 RID: 197 RVA: 0x00003DB8 File Offset: 0x00001FB8
		// (set) Token: 0x060000C6 RID: 198 RVA: 0x00003DCE File Offset: 0x00001FCE
		internal Vector2 internalAutoFocusPoint
		{
			get
			{
				Vector2 result;
				this.get_internalAutoFocusPoint_Injected(out result);
				return result;
			}
			set
			{
				this.set_internalAutoFocusPoint_Injected(ref value);
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x060000C7 RID: 199
		public extern bool isDepth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060000C8 RID: 200
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateWebCamTexture([Writable] WebCamTexture self, string scriptingDevice, int requestedWidth, int requestedHeight, int maxFramerate);

		// Token: 0x060000C9 RID: 201 RVA: 0x00003DD8 File Offset: 0x00001FD8
		public void Play()
		{
			WebCamTexture.INTERNAL_CALL_Play(this);
		}

		// Token: 0x060000CA RID: 202
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Play(WebCamTexture self);

		// Token: 0x060000CB RID: 203 RVA: 0x00003DE1 File Offset: 0x00001FE1
		public void Pause()
		{
			WebCamTexture.INTERNAL_CALL_Pause(this);
		}

		// Token: 0x060000CC RID: 204
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Pause(WebCamTexture self);

		// Token: 0x060000CD RID: 205 RVA: 0x00003DEA File Offset: 0x00001FEA
		public void Stop()
		{
			WebCamTexture.INTERNAL_CALL_Stop(this);
		}

		// Token: 0x060000CE RID: 206
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Stop(WebCamTexture self);

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000CF RID: 207
		public extern bool isPlaying { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000D0 RID: 208
		// (set) Token: 0x060000D1 RID: 209
		public extern string deviceName { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000D2 RID: 210
		// (set) Token: 0x060000D3 RID: 211
		public extern float requestedFPS { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000D4 RID: 212
		// (set) Token: 0x060000D5 RID: 213
		public extern int requestedWidth { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000D6 RID: 214
		// (set) Token: 0x060000D7 RID: 215
		public extern int requestedHeight { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000D8 RID: 216
		public static extern WebCamDevice[] devices { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060000D9 RID: 217 RVA: 0x00003DF4 File Offset: 0x00001FF4
		public Color GetPixel(int x, int y)
		{
			Color result;
			WebCamTexture.INTERNAL_CALL_GetPixel(this, x, y, out result);
			return result;
		}

		// Token: 0x060000DA RID: 218
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPixel(WebCamTexture self, int x, int y, out Color value);

		// Token: 0x060000DB RID: 219 RVA: 0x00003E14 File Offset: 0x00002014
		public Color[] GetPixels()
		{
			return this.GetPixels(0, 0, this.width, this.height);
		}

		// Token: 0x060000DC RID: 220
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color[] GetPixels(int x, int y, int blockWidth, int blockHeight);

		// Token: 0x060000DD RID: 221
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color32[] GetPixels32([DefaultValue("null")] Color32[] colors);

		// Token: 0x060000DE RID: 222 RVA: 0x00003E40 File Offset: 0x00002040
		[ExcludeFromDocs]
		public Color32[] GetPixels32()
		{
			Color32[] colors = null;
			return this.GetPixels32(colors);
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000DF RID: 223
		public extern int videoRotationAngle { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000E0 RID: 224
		public extern bool videoVerticallyMirrored { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000E1 RID: 225
		public extern bool didUpdateThisFrame { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060000E2 RID: 226
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_internalAutoFocusPoint_Injected(out Vector2 ret);

		// Token: 0x060000E3 RID: 227
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_internalAutoFocusPoint_Injected(ref Vector2 value);
	}
}
