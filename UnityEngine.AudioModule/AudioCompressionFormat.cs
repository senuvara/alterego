﻿using System;

namespace UnityEngine
{
	// Token: 0x0200001B RID: 27
	public enum AudioCompressionFormat
	{
		// Token: 0x04000043 RID: 67
		PCM,
		// Token: 0x04000044 RID: 68
		Vorbis,
		// Token: 0x04000045 RID: 69
		ADPCM,
		// Token: 0x04000046 RID: 70
		MP3,
		// Token: 0x04000047 RID: 71
		VAG,
		// Token: 0x04000048 RID: 72
		HEVAG,
		// Token: 0x04000049 RID: 73
		XMA,
		// Token: 0x0400004A RID: 74
		AAC,
		// Token: 0x0400004B RID: 75
		GCADPCM,
		// Token: 0x0400004C RID: 76
		ATRAC9
	}
}
