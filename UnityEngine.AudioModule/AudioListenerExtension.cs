﻿using System;

namespace UnityEngine
{
	// Token: 0x02000005 RID: 5
	internal class AudioListenerExtension : ScriptableObject
	{
		// Token: 0x06000006 RID: 6 RVA: 0x0000215A File Offset: 0x0000035A
		public AudioListenerExtension()
		{
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000007 RID: 7 RVA: 0x00002164 File Offset: 0x00000364
		// (set) Token: 0x06000008 RID: 8 RVA: 0x0000217F File Offset: 0x0000037F
		public AudioListener audioListener
		{
			get
			{
				return this.m_audioListener;
			}
			set
			{
				this.m_audioListener = value;
			}
		}

		// Token: 0x06000009 RID: 9 RVA: 0x0000218C File Offset: 0x0000038C
		public virtual float ReadExtensionProperty(PropertyName propertyName)
		{
			return 0f;
		}

		// Token: 0x0600000A RID: 10 RVA: 0x000021A6 File Offset: 0x000003A6
		public virtual void WriteExtensionProperty(PropertyName propertyName, float propertyValue)
		{
		}

		// Token: 0x0600000B RID: 11 RVA: 0x000021A6 File Offset: 0x000003A6
		public virtual void ExtensionUpdate()
		{
		}

		// Token: 0x0400000A RID: 10
		[SerializeField]
		private AudioListener m_audioListener;
	}
}
