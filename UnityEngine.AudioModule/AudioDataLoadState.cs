﻿using System;

namespace UnityEngine
{
	// Token: 0x02000017 RID: 23
	public enum AudioDataLoadState
	{
		// Token: 0x04000038 RID: 56
		Unloaded,
		// Token: 0x04000039 RID: 57
		Loading,
		// Token: 0x0400003A RID: 58
		Loaded,
		// Token: 0x0400003B RID: 59
		Failed
	}
}
