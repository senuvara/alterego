﻿using System;

namespace UnityEngine
{
	// Token: 0x02000025 RID: 37
	public enum AudioSourceCurveType
	{
		// Token: 0x04000064 RID: 100
		CustomRolloff,
		// Token: 0x04000065 RID: 101
		SpatialBlend,
		// Token: 0x04000066 RID: 102
		ReverbZoneMix,
		// Token: 0x04000067 RID: 103
		Spread
	}
}
