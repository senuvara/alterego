﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000014 RID: 20
	[UsedByNativeCode]
	public struct WebCamDevice
	{
		// Token: 0x17000011 RID: 17
		// (get) Token: 0x060000B7 RID: 183 RVA: 0x00003BE8 File Offset: 0x00001DE8
		public string name
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x060000B8 RID: 184 RVA: 0x00003C04 File Offset: 0x00001E04
		public bool isFrontFacing
		{
			get
			{
				return (this.m_Flags & 1) != 0;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x060000B9 RID: 185 RVA: 0x00003C28 File Offset: 0x00001E28
		public WebCamKind kind
		{
			get
			{
				return this.m_Kind;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x060000BA RID: 186 RVA: 0x00003C44 File Offset: 0x00001E44
		public string depthCameraName
		{
			get
			{
				return (!(this.m_DepthCameraName == "")) ? this.m_DepthCameraName : null;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x060000BB RID: 187 RVA: 0x00003C7C File Offset: 0x00001E7C
		public bool isAutoFocusPointSupported
		{
			get
			{
				return (this.m_Flags & 2) != 0;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x060000BC RID: 188 RVA: 0x00003CA0 File Offset: 0x00001EA0
		public Resolution[] availableResolutions
		{
			get
			{
				return this.m_Resolutions;
			}
		}

		// Token: 0x04000029 RID: 41
		internal string m_Name;

		// Token: 0x0400002A RID: 42
		internal string m_DepthCameraName;

		// Token: 0x0400002B RID: 43
		internal int m_Flags;

		// Token: 0x0400002C RID: 44
		internal WebCamKind m_Kind;

		// Token: 0x0400002D RID: 45
		internal Resolution[] m_Resolutions;
	}
}
