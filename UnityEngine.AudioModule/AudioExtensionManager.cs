﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000007 RID: 7
	internal sealed class AudioExtensionManager
	{
		// Token: 0x06000015 RID: 21 RVA: 0x0000226D File Offset: 0x0000046D
		public AudioExtensionManager()
		{
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002278 File Offset: 0x00000478
		internal static bool IsListenerSpatializerExtensionRegistered()
		{
			foreach (AudioSpatializerExtensionDefinition audioSpatializerExtensionDefinition in AudioExtensionManager.m_ListenerSpatializerExtensionDefinitions)
			{
				if (AudioSettings.GetSpatializerPluginName() == audioSpatializerExtensionDefinition.spatializerName)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000017 RID: 23 RVA: 0x000022FC File Offset: 0x000004FC
		internal static bool IsSourceSpatializerExtensionRegistered()
		{
			foreach (AudioSpatializerExtensionDefinition audioSpatializerExtensionDefinition in AudioExtensionManager.m_SourceSpatializerExtensionDefinitions)
			{
				if (AudioSettings.GetSpatializerPluginName() == audioSpatializerExtensionDefinition.spatializerName)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000018 RID: 24 RVA: 0x00002380 File Offset: 0x00000580
		internal static bool IsSourceAmbisonicDecoderExtensionRegistered()
		{
			foreach (AudioAmbisonicExtensionDefinition audioAmbisonicExtensionDefinition in AudioExtensionManager.m_SourceAmbisonicDecoderExtensionDefinitions)
			{
				if (AudioSettings.GetAmbisonicDecoderPluginName() == audioAmbisonicExtensionDefinition.ambisonicPluginName)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002404 File Offset: 0x00000604
		internal static AudioSourceExtension AddSpatializerExtension(AudioSource source)
		{
			AudioSourceExtension result;
			if (!source.spatialize)
			{
				result = null;
			}
			else if (source.spatializerExtension != null)
			{
				result = source.spatializerExtension;
			}
			else
			{
				AudioExtensionManager.RegisterBuiltinDefinitions();
				foreach (AudioSpatializerExtensionDefinition audioSpatializerExtensionDefinition in AudioExtensionManager.m_SourceSpatializerExtensionDefinitions)
				{
					if (AudioSettings.GetSpatializerPluginName() == audioSpatializerExtensionDefinition.spatializerName)
					{
						AudioSourceExtension audioSourceExtension = source.AddSpatializerExtension(audioSpatializerExtensionDefinition.definition.GetExtensionType());
						if (audioSourceExtension != null)
						{
							audioSourceExtension.audioSource = source;
							source.spatializerExtension = audioSourceExtension;
							AudioExtensionManager.WriteExtensionProperties(audioSourceExtension, audioSpatializerExtensionDefinition.definition.GetExtensionType().Name);
							return audioSourceExtension;
						}
					}
				}
				result = null;
			}
			return result;
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00002500 File Offset: 0x00000700
		internal static AudioSourceExtension AddAmbisonicDecoderExtension(AudioSource source)
		{
			AudioSourceExtension result;
			if (source.ambisonicExtension != null)
			{
				result = source.ambisonicExtension;
			}
			else
			{
				AudioExtensionManager.RegisterBuiltinDefinitions();
				foreach (AudioAmbisonicExtensionDefinition audioAmbisonicExtensionDefinition in AudioExtensionManager.m_SourceAmbisonicDecoderExtensionDefinitions)
				{
					if (AudioSettings.GetAmbisonicDecoderPluginName() == audioAmbisonicExtensionDefinition.ambisonicPluginName)
					{
						AudioSourceExtension audioSourceExtension = source.AddAmbisonicExtension(audioAmbisonicExtensionDefinition.definition.GetExtensionType());
						if (audioSourceExtension != null)
						{
							audioSourceExtension.audioSource = source;
							source.ambisonicExtension = audioSourceExtension;
							return audioSourceExtension;
						}
					}
				}
				result = null;
			}
			return result;
		}

		// Token: 0x0600001B RID: 27 RVA: 0x000025D4 File Offset: 0x000007D4
		internal static void WriteExtensionProperties(AudioSourceExtension extension, string extensionName)
		{
			if (AudioExtensionManager.m_SpatializerExtensionName == 0)
			{
				AudioExtensionManager.m_SpatializerExtensionName = extensionName;
			}
			for (int i = 0; i < extension.audioSource.GetNumExtensionProperties(); i++)
			{
				if (extension.audioSource.ReadExtensionName(i) == AudioExtensionManager.m_SpatializerExtensionName)
				{
					PropertyName propertyName = extension.audioSource.ReadExtensionPropertyName(i);
					float propertyValue = extension.audioSource.ReadExtensionPropertyValue(i);
					extension.WriteExtensionProperty(propertyName, propertyValue);
				}
			}
			extension.audioSource.ClearExtensionProperties(AudioExtensionManager.m_SpatializerExtensionName);
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002670 File Offset: 0x00000870
		internal static AudioListenerExtension AddSpatializerExtension(AudioListener listener)
		{
			AudioListenerExtension result;
			if (listener.spatializerExtension != null)
			{
				result = listener.spatializerExtension;
			}
			else
			{
				AudioExtensionManager.RegisterBuiltinDefinitions();
				foreach (AudioSpatializerExtensionDefinition audioSpatializerExtensionDefinition in AudioExtensionManager.m_ListenerSpatializerExtensionDefinitions)
				{
					if (AudioSettings.GetSpatializerPluginName() == audioSpatializerExtensionDefinition.spatializerName || AudioSettings.GetAmbisonicDecoderPluginName() == audioSpatializerExtensionDefinition.spatializerName)
					{
						AudioListenerExtension audioListenerExtension = listener.AddExtension(audioSpatializerExtensionDefinition.definition.GetExtensionType());
						if (audioListenerExtension != null)
						{
							audioListenerExtension.audioListener = listener;
							listener.spatializerExtension = audioListenerExtension;
							AudioExtensionManager.WriteExtensionProperties(audioListenerExtension, audioSpatializerExtensionDefinition.definition.GetExtensionType().Name);
							return audioListenerExtension;
						}
					}
				}
				result = null;
			}
			return result;
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002774 File Offset: 0x00000974
		internal static void WriteExtensionProperties(AudioListenerExtension extension, string extensionName)
		{
			if (AudioExtensionManager.m_ListenerSpatializerExtensionName == 0)
			{
				AudioExtensionManager.m_ListenerSpatializerExtensionName = extensionName;
			}
			for (int i = 0; i < extension.audioListener.GetNumExtensionProperties(); i++)
			{
				if (extension.audioListener.ReadExtensionName(i) == AudioExtensionManager.m_ListenerSpatializerExtensionName)
				{
					PropertyName propertyName = extension.audioListener.ReadExtensionPropertyName(i);
					float propertyValue = extension.audioListener.ReadExtensionPropertyValue(i);
					extension.WriteExtensionProperty(propertyName, propertyValue);
				}
			}
			extension.audioListener.ClearExtensionProperties(AudioExtensionManager.m_ListenerSpatializerExtensionName);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002810 File Offset: 0x00000A10
		internal static AudioListenerExtension GetSpatializerExtension(AudioListener listener)
		{
			AudioListenerExtension result;
			if (listener.spatializerExtension != null)
			{
				result = listener.spatializerExtension;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002844 File Offset: 0x00000A44
		internal static AudioSourceExtension GetSpatializerExtension(AudioSource source)
		{
			return (!source.spatialize) ? null : source.spatializerExtension;
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002870 File Offset: 0x00000A70
		internal static AudioSourceExtension GetAmbisonicExtension(AudioSource source)
		{
			return source.ambisonicExtension;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x0000288C File Offset: 0x00000A8C
		internal static Type GetListenerSpatializerExtensionType()
		{
			foreach (AudioSpatializerExtensionDefinition audioSpatializerExtensionDefinition in AudioExtensionManager.m_ListenerSpatializerExtensionDefinitions)
			{
				if (AudioSettings.GetSpatializerPluginName() == audioSpatializerExtensionDefinition.spatializerName)
				{
					return audioSpatializerExtensionDefinition.definition.GetExtensionType();
				}
			}
			return null;
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002918 File Offset: 0x00000B18
		internal static Type GetListenerSpatializerExtensionEditorType()
		{
			foreach (AudioSpatializerExtensionDefinition audioSpatializerExtensionDefinition in AudioExtensionManager.m_ListenerSpatializerExtensionDefinitions)
			{
				if (AudioSettings.GetSpatializerPluginName() == audioSpatializerExtensionDefinition.spatializerName)
				{
					return audioSpatializerExtensionDefinition.editorDefinition.GetExtensionType();
				}
			}
			return null;
		}

		// Token: 0x06000023 RID: 35 RVA: 0x000029A4 File Offset: 0x00000BA4
		internal static Type GetSourceSpatializerExtensionType()
		{
			foreach (AudioSpatializerExtensionDefinition audioSpatializerExtensionDefinition in AudioExtensionManager.m_SourceSpatializerExtensionDefinitions)
			{
				if (AudioSettings.GetSpatializerPluginName() == audioSpatializerExtensionDefinition.spatializerName)
				{
					return audioSpatializerExtensionDefinition.definition.GetExtensionType();
				}
			}
			return null;
		}

		// Token: 0x06000024 RID: 36 RVA: 0x00002A30 File Offset: 0x00000C30
		internal static Type GetSourceSpatializerExtensionEditorType()
		{
			foreach (AudioSpatializerExtensionDefinition audioSpatializerExtensionDefinition in AudioExtensionManager.m_SourceSpatializerExtensionDefinitions)
			{
				if (AudioSettings.GetSpatializerPluginName() == audioSpatializerExtensionDefinition.spatializerName)
				{
					return audioSpatializerExtensionDefinition.editorDefinition.GetExtensionType();
				}
			}
			return null;
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002ABC File Offset: 0x00000CBC
		internal static Type GetSourceAmbisonicExtensionType()
		{
			foreach (AudioAmbisonicExtensionDefinition audioAmbisonicExtensionDefinition in AudioExtensionManager.m_SourceAmbisonicDecoderExtensionDefinitions)
			{
				if (AudioSettings.GetAmbisonicDecoderPluginName() == audioAmbisonicExtensionDefinition.ambisonicPluginName)
				{
					return audioAmbisonicExtensionDefinition.definition.GetExtensionType();
				}
			}
			return null;
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00002B48 File Offset: 0x00000D48
		internal static PropertyName GetSpatializerName()
		{
			return AudioExtensionManager.m_SpatializerName;
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002B64 File Offset: 0x00000D64
		internal static PropertyName GetSourceSpatializerExtensionName()
		{
			return AudioExtensionManager.m_SpatializerExtensionName;
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00002B80 File Offset: 0x00000D80
		internal static PropertyName GetListenerSpatializerExtensionName()
		{
			return AudioExtensionManager.m_ListenerSpatializerExtensionName;
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002B9A File Offset: 0x00000D9A
		internal static void AddExtensionToManager(AudioSourceExtension extension)
		{
			AudioExtensionManager.RegisterBuiltinDefinitions();
			if (extension.m_ExtensionManagerUpdateIndex == -1)
			{
				AudioExtensionManager.m_SourceExtensionsToUpdate.Add(extension);
				extension.m_ExtensionManagerUpdateIndex = AudioExtensionManager.m_SourceExtensionsToUpdate.Count - 1;
			}
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00002BD0 File Offset: 0x00000DD0
		internal static void RemoveExtensionFromManager(AudioSourceExtension extension)
		{
			int extensionManagerUpdateIndex = extension.m_ExtensionManagerUpdateIndex;
			if (extensionManagerUpdateIndex >= 0 && extensionManagerUpdateIndex < AudioExtensionManager.m_SourceExtensionsToUpdate.Count)
			{
				int index = AudioExtensionManager.m_SourceExtensionsToUpdate.Count - 1;
				AudioExtensionManager.m_SourceExtensionsToUpdate[extensionManagerUpdateIndex] = AudioExtensionManager.m_SourceExtensionsToUpdate[index];
				AudioExtensionManager.m_SourceExtensionsToUpdate[extensionManagerUpdateIndex].m_ExtensionManagerUpdateIndex = extensionManagerUpdateIndex;
				AudioExtensionManager.m_SourceExtensionsToUpdate.RemoveAt(index);
			}
			extension.m_ExtensionManagerUpdateIndex = -1;
		}

		// Token: 0x0600002B RID: 43 RVA: 0x00002C44 File Offset: 0x00000E44
		internal static void Update()
		{
			AudioExtensionManager.RegisterBuiltinDefinitions();
			AudioListener audioListener = AudioExtensionManager.GetAudioListener() as AudioListener;
			if (audioListener != null)
			{
				AudioListenerExtension audioListenerExtension = AudioExtensionManager.AddSpatializerExtension(audioListener);
				if (audioListenerExtension != null)
				{
					audioListenerExtension.ExtensionUpdate();
				}
			}
			for (int i = 0; i < AudioExtensionManager.m_SourceExtensionsToUpdate.Count; i++)
			{
				AudioExtensionManager.m_SourceExtensionsToUpdate[i].ExtensionUpdate();
			}
			AudioExtensionManager.m_NextStopIndex = ((AudioExtensionManager.m_NextStopIndex < AudioExtensionManager.m_SourceExtensionsToUpdate.Count) ? AudioExtensionManager.m_NextStopIndex : 0);
			int num = (AudioExtensionManager.m_SourceExtensionsToUpdate.Count <= 0) ? 0 : (1 + AudioExtensionManager.m_SourceExtensionsToUpdate.Count / 8);
			for (int j = 0; j < num; j++)
			{
				AudioSourceExtension audioSourceExtension = AudioExtensionManager.m_SourceExtensionsToUpdate[AudioExtensionManager.m_NextStopIndex];
				if (audioSourceExtension.audioSource == null || !audioSourceExtension.audioSource.enabled || !audioSourceExtension.audioSource.isPlaying)
				{
					audioSourceExtension.Stop();
					AudioExtensionManager.RemoveExtensionFromManager(audioSourceExtension);
				}
				else
				{
					AudioExtensionManager.m_NextStopIndex++;
					AudioExtensionManager.m_NextStopIndex = ((AudioExtensionManager.m_NextStopIndex < AudioExtensionManager.m_SourceExtensionsToUpdate.Count) ? AudioExtensionManager.m_NextStopIndex : 0);
				}
			}
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00002DA1 File Offset: 0x00000FA1
		internal static void GetReadyToPlay(AudioSourceExtension extension)
		{
			if (extension != null)
			{
				extension.Play();
				AudioExtensionManager.AddExtensionToManager(extension);
			}
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00002DC0 File Offset: 0x00000FC0
		private static void RegisterBuiltinDefinitions()
		{
			bool flag = false;
			if (!AudioExtensionManager.m_BuiltinDefinitionsRegistered)
			{
				if (flag || AudioSettings.GetSpatializerPluginName() == "GVR Audio Spatializer")
				{
				}
				if (flag || AudioSettings.GetAmbisonicDecoderPluginName() == "GVR Audio Spatializer")
				{
				}
				AudioExtensionManager.m_BuiltinDefinitionsRegistered = true;
			}
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002E1C File Offset: 0x0000101C
		private static bool RegisterListenerSpatializerDefinition(string spatializerName, AudioExtensionDefinition extensionDefinition, AudioExtensionDefinition editorDefinition)
		{
			foreach (AudioSpatializerExtensionDefinition audioSpatializerExtensionDefinition in AudioExtensionManager.m_ListenerSpatializerExtensionDefinitions)
			{
				if (spatializerName == audioSpatializerExtensionDefinition.spatializerName)
				{
					Debug.Log("RegisterListenerSpatializerDefinition failed for " + extensionDefinition.GetExtensionType() + ". We only allow one audio listener extension to be registered for each spatializer.");
					return false;
				}
			}
			AudioSpatializerExtensionDefinition item = new AudioSpatializerExtensionDefinition(spatializerName, extensionDefinition, editorDefinition);
			AudioExtensionManager.m_ListenerSpatializerExtensionDefinitions.Add(item);
			return true;
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002EC8 File Offset: 0x000010C8
		private static bool RegisterSourceSpatializerDefinition(string spatializerName, AudioExtensionDefinition extensionDefinition, AudioExtensionDefinition editorDefinition)
		{
			foreach (AudioSpatializerExtensionDefinition audioSpatializerExtensionDefinition in AudioExtensionManager.m_SourceSpatializerExtensionDefinitions)
			{
				if (spatializerName == audioSpatializerExtensionDefinition.spatializerName)
				{
					Debug.Log("RegisterSourceSpatializerDefinition failed for " + extensionDefinition.GetExtensionType() + ". We only allow one audio source extension to be registered for each spatializer.");
					return false;
				}
			}
			AudioSpatializerExtensionDefinition item = new AudioSpatializerExtensionDefinition(spatializerName, extensionDefinition, editorDefinition);
			AudioExtensionManager.m_SourceSpatializerExtensionDefinitions.Add(item);
			return true;
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002F74 File Offset: 0x00001174
		private static bool RegisterSourceAmbisonicDefinition(string ambisonicDecoderName, AudioExtensionDefinition extensionDefinition)
		{
			foreach (AudioAmbisonicExtensionDefinition audioAmbisonicExtensionDefinition in AudioExtensionManager.m_SourceAmbisonicDecoderExtensionDefinitions)
			{
				if (ambisonicDecoderName == audioAmbisonicExtensionDefinition.ambisonicPluginName)
				{
					Debug.Log("RegisterSourceAmbisonicDefinition failed for " + extensionDefinition.GetExtensionType() + ". We only allow one audio source extension to be registered for each ambisonic decoder.");
					return false;
				}
			}
			AudioAmbisonicExtensionDefinition item = new AudioAmbisonicExtensionDefinition(ambisonicDecoderName, extensionDefinition);
			AudioExtensionManager.m_SourceAmbisonicDecoderExtensionDefinitions.Add(item);
			return true;
		}

		// Token: 0x06000031 RID: 49
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Object GetAudioListener();

		// Token: 0x06000032 RID: 50 RVA: 0x00003020 File Offset: 0x00001220
		// Note: this type is marked as 'beforefieldinit'.
		static AudioExtensionManager()
		{
		}

		// Token: 0x0400000D RID: 13
		private static List<AudioSpatializerExtensionDefinition> m_ListenerSpatializerExtensionDefinitions = new List<AudioSpatializerExtensionDefinition>();

		// Token: 0x0400000E RID: 14
		private static List<AudioSpatializerExtensionDefinition> m_SourceSpatializerExtensionDefinitions = new List<AudioSpatializerExtensionDefinition>();

		// Token: 0x0400000F RID: 15
		private static List<AudioAmbisonicExtensionDefinition> m_SourceAmbisonicDecoderExtensionDefinitions = new List<AudioAmbisonicExtensionDefinition>();

		// Token: 0x04000010 RID: 16
		private static List<AudioSourceExtension> m_SourceExtensionsToUpdate = new List<AudioSourceExtension>();

		// Token: 0x04000011 RID: 17
		private static int m_NextStopIndex = 0;

		// Token: 0x04000012 RID: 18
		private static bool m_BuiltinDefinitionsRegistered = false;

		// Token: 0x04000013 RID: 19
		private static PropertyName m_SpatializerName = 0;

		// Token: 0x04000014 RID: 20
		private static PropertyName m_SpatializerExtensionName = 0;

		// Token: 0x04000015 RID: 21
		private static PropertyName m_ListenerSpatializerExtensionName = 0;
	}
}
