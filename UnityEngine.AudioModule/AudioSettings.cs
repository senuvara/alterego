﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000019 RID: 25
	public sealed class AudioSettings
	{
		// Token: 0x060000E4 RID: 228 RVA: 0x0000226D File Offset: 0x0000046D
		public AudioSettings()
		{
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000E5 RID: 229
		public static extern AudioSpeakerMode driverCapabilities { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000E6 RID: 230
		// (set) Token: 0x060000E7 RID: 231
		public static extern AudioSpeakerMode speakerMode { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000E8 RID: 232
		internal static extern int profilerCaptureFlags { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000E9 RID: 233
		[ThreadAndSerializationSafe]
		public static extern double dspTime { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000EA RID: 234
		// (set) Token: 0x060000EB RID: 235
		public static extern int outputSampleRate { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060000EC RID: 236
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void GetDSPBufferSize(out int bufferLength, out int numBuffers);

		// Token: 0x060000ED RID: 237
		[Obsolete("AudioSettings.SetDSPBufferSize is deprecated and has been replaced by audio project settings and the AudioSettings.GetConfiguration/AudioSettings.Reset API.")]
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetDSPBufferSize(int bufferLength, int numBuffers);

		// Token: 0x060000EE RID: 238
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetSpatializerPluginName();

		// Token: 0x060000EF RID: 239 RVA: 0x00003E60 File Offset: 0x00002060
		public static AudioConfiguration GetConfiguration()
		{
			AudioConfiguration result;
			AudioSettings.INTERNAL_CALL_GetConfiguration(out result);
			return result;
		}

		// Token: 0x060000F0 RID: 240
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetConfiguration(out AudioConfiguration value);

		// Token: 0x060000F1 RID: 241 RVA: 0x00003E80 File Offset: 0x00002080
		public static bool Reset(AudioConfiguration config)
		{
			return AudioSettings.INTERNAL_CALL_Reset(ref config);
		}

		// Token: 0x060000F2 RID: 242
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Reset(ref AudioConfiguration config);

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x060000F3 RID: 243 RVA: 0x00003E9C File Offset: 0x0000209C
		// (remove) Token: 0x060000F4 RID: 244 RVA: 0x00003ED0 File Offset: 0x000020D0
		public static event AudioSettings.AudioConfigurationChangeHandler OnAudioConfigurationChanged
		{
			add
			{
				AudioSettings.AudioConfigurationChangeHandler audioConfigurationChangeHandler = AudioSettings.OnAudioConfigurationChanged;
				AudioSettings.AudioConfigurationChangeHandler audioConfigurationChangeHandler2;
				do
				{
					audioConfigurationChangeHandler2 = audioConfigurationChangeHandler;
					audioConfigurationChangeHandler = Interlocked.CompareExchange<AudioSettings.AudioConfigurationChangeHandler>(ref AudioSettings.OnAudioConfigurationChanged, (AudioSettings.AudioConfigurationChangeHandler)Delegate.Combine(audioConfigurationChangeHandler2, value), audioConfigurationChangeHandler);
				}
				while (audioConfigurationChangeHandler != audioConfigurationChangeHandler2);
			}
			remove
			{
				AudioSettings.AudioConfigurationChangeHandler audioConfigurationChangeHandler = AudioSettings.OnAudioConfigurationChanged;
				AudioSettings.AudioConfigurationChangeHandler audioConfigurationChangeHandler2;
				do
				{
					audioConfigurationChangeHandler2 = audioConfigurationChangeHandler;
					audioConfigurationChangeHandler = Interlocked.CompareExchange<AudioSettings.AudioConfigurationChangeHandler>(ref AudioSettings.OnAudioConfigurationChanged, (AudioSettings.AudioConfigurationChangeHandler)Delegate.Remove(audioConfigurationChangeHandler2, value), audioConfigurationChangeHandler);
				}
				while (audioConfigurationChangeHandler != audioConfigurationChangeHandler2);
			}
		}

		// Token: 0x060000F5 RID: 245 RVA: 0x00003F04 File Offset: 0x00002104
		[RequiredByNativeCode]
		internal static void InvokeOnAudioConfigurationChanged(bool deviceWasChanged)
		{
			if (AudioSettings.OnAudioConfigurationChanged != null)
			{
				AudioSettings.OnAudioConfigurationChanged(deviceWasChanged);
			}
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x00003F1C File Offset: 0x0000211C
		[RequiredByNativeCode]
		internal static void InvokeOnAudioManagerUpdate()
		{
			AudioExtensionManager.Update();
		}

		// Token: 0x060000F7 RID: 247 RVA: 0x00003F24 File Offset: 0x00002124
		[RequiredByNativeCode]
		internal static void InvokeOnAudioSourcePlay(AudioSource source)
		{
			AudioSourceExtension audioSourceExtension = AudioExtensionManager.AddSpatializerExtension(source);
			if (audioSourceExtension != null)
			{
				AudioExtensionManager.GetReadyToPlay(audioSourceExtension);
			}
			if (source.clip != null && source.clip.ambisonic)
			{
				AudioSourceExtension audioSourceExtension2 = AudioExtensionManager.AddAmbisonicDecoderExtension(source);
				if (audioSourceExtension2 != null)
				{
					AudioExtensionManager.GetReadyToPlay(audioSourceExtension2);
				}
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000F8 RID: 248
		internal static extern bool unityAudioDisabled { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060000F9 RID: 249
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string GetAmbisonicDecoderPluginName();

		// Token: 0x04000041 RID: 65
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static AudioSettings.AudioConfigurationChangeHandler OnAudioConfigurationChanged;

		// Token: 0x0200001A RID: 26
		// (Invoke) Token: 0x060000FB RID: 251
		public delegate void AudioConfigurationChangeHandler(bool deviceWasChanged);
	}
}
