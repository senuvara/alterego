﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Audio
{
	// Token: 0x0200000D RID: 13
	[NativeType(Header = "Modules/Audio/Public/ScriptBindings/AudioSampleProvider.bindings.h")]
	[StaticAccessor("AudioSampleProviderBindings", StaticAccessorType.DoubleColon)]
	public class AudioSampleProvider : IDisposable
	{
		// Token: 0x06000073 RID: 115 RVA: 0x000036D8 File Offset: 0x000018D8
		private AudioSampleProvider(uint providerId, Object ownerObj, ushort trackIdx)
		{
			this.owner = ownerObj;
			this.id = providerId;
			this.trackIndex = trackIdx;
			this.m_ConsumeSampleFramesNativeFunction = (AudioSampleProvider.ConsumeSampleFramesNativeFunction)Marshal.GetDelegateForFunctionPointer(AudioSampleProvider.InternalGetConsumeSampleFramesNativeFunctionPtr(), typeof(AudioSampleProvider.ConsumeSampleFramesNativeFunction));
			ushort channelCount = 0;
			uint sampleRate = 0U;
			AudioSampleProvider.InternalGetFormatInfo(providerId, out channelCount, out sampleRate);
			this.channelCount = channelCount;
			this.sampleRate = sampleRate;
			AudioSampleProvider.InternalSetScriptingPtr(providerId, this);
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00003744 File Offset: 0x00001944
		[VisibleToOtherModules]
		internal static AudioSampleProvider Lookup(uint providerId, Object ownerObj, ushort trackIndex)
		{
			AudioSampleProvider audioSampleProvider = AudioSampleProvider.InternalGetScriptingPtr(providerId);
			AudioSampleProvider result;
			if (audioSampleProvider != null || !AudioSampleProvider.InternalIsValid(providerId))
			{
				result = audioSampleProvider;
			}
			else
			{
				result = new AudioSampleProvider(providerId, ownerObj, trackIndex);
			}
			return result;
		}

		// Token: 0x06000075 RID: 117 RVA: 0x00003780 File Offset: 0x00001980
		~AudioSampleProvider()
		{
			this.Dispose();
		}

		// Token: 0x06000076 RID: 118 RVA: 0x000037B0 File Offset: 0x000019B0
		public void Dispose()
		{
			if (this.id != 0U)
			{
				AudioSampleProvider.InternalSetScriptingPtr(this.id, null);
				this.id = 0U;
			}
			GC.SuppressFinalize(this);
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000077 RID: 119 RVA: 0x000037DC File Offset: 0x000019DC
		// (set) Token: 0x06000078 RID: 120 RVA: 0x000037F6 File Offset: 0x000019F6
		public uint id
		{
			[CompilerGenerated]
			get
			{
				return this.<id>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<id>k__BackingField = value;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000079 RID: 121 RVA: 0x00003800 File Offset: 0x00001A00
		// (set) Token: 0x0600007A RID: 122 RVA: 0x0000381A File Offset: 0x00001A1A
		public ushort trackIndex
		{
			[CompilerGenerated]
			get
			{
				return this.<trackIndex>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<trackIndex>k__BackingField = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600007B RID: 123 RVA: 0x00003824 File Offset: 0x00001A24
		// (set) Token: 0x0600007C RID: 124 RVA: 0x0000383E File Offset: 0x00001A3E
		public Object owner
		{
			[CompilerGenerated]
			get
			{
				return this.<owner>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<owner>k__BackingField = value;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600007D RID: 125 RVA: 0x00003848 File Offset: 0x00001A48
		public bool valid
		{
			get
			{
				return AudioSampleProvider.InternalIsValid(this.id);
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600007E RID: 126 RVA: 0x00003868 File Offset: 0x00001A68
		// (set) Token: 0x0600007F RID: 127 RVA: 0x00003882 File Offset: 0x00001A82
		public ushort channelCount
		{
			[CompilerGenerated]
			get
			{
				return this.<channelCount>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<channelCount>k__BackingField = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000080 RID: 128 RVA: 0x0000388C File Offset: 0x00001A8C
		// (set) Token: 0x06000081 RID: 129 RVA: 0x000038A6 File Offset: 0x00001AA6
		public uint sampleRate
		{
			[CompilerGenerated]
			get
			{
				return this.<sampleRate>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<sampleRate>k__BackingField = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000082 RID: 130 RVA: 0x000038B0 File Offset: 0x00001AB0
		public uint maxSampleFrameCount
		{
			get
			{
				return AudioSampleProvider.InternalGetMaxSampleFrameCount(this.id);
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000083 RID: 131 RVA: 0x000038D0 File Offset: 0x00001AD0
		public uint availableSampleFrameCount
		{
			get
			{
				return AudioSampleProvider.InternalGetAvailableSampleFrameCount(this.id);
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000084 RID: 132 RVA: 0x000038F0 File Offset: 0x00001AF0
		public uint freeSampleFrameCount
		{
			get
			{
				return AudioSampleProvider.InternalGetFreeSampleFrameCount(this.id);
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000085 RID: 133 RVA: 0x00003910 File Offset: 0x00001B10
		// (set) Token: 0x06000086 RID: 134 RVA: 0x00003930 File Offset: 0x00001B30
		public uint freeSampleFrameCountLowThreshold
		{
			get
			{
				return AudioSampleProvider.InternalGetFreeSampleFrameCountLowThreshold(this.id);
			}
			set
			{
				AudioSampleProvider.InternalSetFreeSampleFrameCountLowThreshold(this.id, value);
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000087 RID: 135 RVA: 0x00003940 File Offset: 0x00001B40
		// (set) Token: 0x06000088 RID: 136 RVA: 0x00003960 File Offset: 0x00001B60
		public bool enableSampleFramesAvailableEvents
		{
			get
			{
				return AudioSampleProvider.InternalGetEnableSampleFramesAvailableEvents(this.id);
			}
			set
			{
				AudioSampleProvider.InternalSetEnableSampleFramesAvailableEvents(this.id, value);
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000089 RID: 137 RVA: 0x00003970 File Offset: 0x00001B70
		// (set) Token: 0x0600008A RID: 138 RVA: 0x00003990 File Offset: 0x00001B90
		public bool enableSilencePadding
		{
			get
			{
				return AudioSampleProvider.InternalGetEnableSilencePadding(this.id);
			}
			set
			{
				AudioSampleProvider.InternalSetEnableSilencePadding(this.id, value);
			}
		}

		// Token: 0x0600008B RID: 139 RVA: 0x000039A0 File Offset: 0x00001BA0
		public uint ConsumeSampleFrames(NativeArray<float> sampleFrames)
		{
			uint result;
			if (this.channelCount == 0)
			{
				result = 0U;
			}
			else
			{
				result = this.m_ConsumeSampleFramesNativeFunction(this.id, (IntPtr)sampleFrames.GetUnsafePtr<float>(), (uint)(sampleFrames.Length / (int)this.channelCount));
			}
			return result;
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600008C RID: 140 RVA: 0x000039F4 File Offset: 0x00001BF4
		public static AudioSampleProvider.ConsumeSampleFramesNativeFunction consumeSampleFramesNativeFunction
		{
			get
			{
				return (AudioSampleProvider.ConsumeSampleFramesNativeFunction)Marshal.GetDelegateForFunctionPointer(AudioSampleProvider.InternalGetConsumeSampleFramesNativeFunctionPtr(), typeof(AudioSampleProvider.ConsumeSampleFramesNativeFunction));
			}
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x0600008D RID: 141 RVA: 0x00003A24 File Offset: 0x00001C24
		// (remove) Token: 0x0600008E RID: 142 RVA: 0x00003A5C File Offset: 0x00001C5C
		public event AudioSampleProvider.SampleFramesHandler sampleFramesAvailable
		{
			add
			{
				AudioSampleProvider.SampleFramesHandler sampleFramesHandler = this.sampleFramesAvailable;
				AudioSampleProvider.SampleFramesHandler sampleFramesHandler2;
				do
				{
					sampleFramesHandler2 = sampleFramesHandler;
					sampleFramesHandler = Interlocked.CompareExchange<AudioSampleProvider.SampleFramesHandler>(ref this.sampleFramesAvailable, (AudioSampleProvider.SampleFramesHandler)Delegate.Combine(sampleFramesHandler2, value), sampleFramesHandler);
				}
				while (sampleFramesHandler != sampleFramesHandler2);
			}
			remove
			{
				AudioSampleProvider.SampleFramesHandler sampleFramesHandler = this.sampleFramesAvailable;
				AudioSampleProvider.SampleFramesHandler sampleFramesHandler2;
				do
				{
					sampleFramesHandler2 = sampleFramesHandler;
					sampleFramesHandler = Interlocked.CompareExchange<AudioSampleProvider.SampleFramesHandler>(ref this.sampleFramesAvailable, (AudioSampleProvider.SampleFramesHandler)Delegate.Remove(sampleFramesHandler2, value), sampleFramesHandler);
				}
				while (sampleFramesHandler != sampleFramesHandler2);
			}
		}

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x0600008F RID: 143 RVA: 0x00003A94 File Offset: 0x00001C94
		// (remove) Token: 0x06000090 RID: 144 RVA: 0x00003ACC File Offset: 0x00001CCC
		public event AudioSampleProvider.SampleFramesHandler sampleFramesOverflow
		{
			add
			{
				AudioSampleProvider.SampleFramesHandler sampleFramesHandler = this.sampleFramesOverflow;
				AudioSampleProvider.SampleFramesHandler sampleFramesHandler2;
				do
				{
					sampleFramesHandler2 = sampleFramesHandler;
					sampleFramesHandler = Interlocked.CompareExchange<AudioSampleProvider.SampleFramesHandler>(ref this.sampleFramesOverflow, (AudioSampleProvider.SampleFramesHandler)Delegate.Combine(sampleFramesHandler2, value), sampleFramesHandler);
				}
				while (sampleFramesHandler != sampleFramesHandler2);
			}
			remove
			{
				AudioSampleProvider.SampleFramesHandler sampleFramesHandler = this.sampleFramesOverflow;
				AudioSampleProvider.SampleFramesHandler sampleFramesHandler2;
				do
				{
					sampleFramesHandler2 = sampleFramesHandler;
					sampleFramesHandler = Interlocked.CompareExchange<AudioSampleProvider.SampleFramesHandler>(ref this.sampleFramesOverflow, (AudioSampleProvider.SampleFramesHandler)Delegate.Remove(sampleFramesHandler2, value), sampleFramesHandler);
				}
				while (sampleFramesHandler != sampleFramesHandler2);
			}
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00003B02 File Offset: 0x00001D02
		public void SetSampleFramesAvailableNativeHandler(AudioSampleProvider.SampleFramesEventNativeFunction handler, IntPtr userData)
		{
			AudioSampleProvider.InternalSetSampleFramesAvailableNativeHandler(this.id, Marshal.GetFunctionPointerForDelegate(handler), userData);
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00003B17 File Offset: 0x00001D17
		public void ClearSampleFramesAvailableNativeHandler()
		{
			AudioSampleProvider.InternalClearSampleFramesAvailableNativeHandler(this.id);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00003B25 File Offset: 0x00001D25
		public void SetSampleFramesOverflowNativeHandler(AudioSampleProvider.SampleFramesEventNativeFunction handler, IntPtr userData)
		{
			AudioSampleProvider.InternalSetSampleFramesOverflowNativeHandler(this.id, Marshal.GetFunctionPointerForDelegate(handler), userData);
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00003B3A File Offset: 0x00001D3A
		public void ClearSampleFramesOverflowNativeHandler()
		{
			AudioSampleProvider.InternalClearSampleFramesOverflowNativeHandler(this.id);
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00003B48 File Offset: 0x00001D48
		[RequiredByNativeCode]
		private void InvokeSampleFramesAvailable(int sampleFrameCount)
		{
			if (this.sampleFramesAvailable != null)
			{
				this.sampleFramesAvailable(this, (uint)sampleFrameCount);
			}
		}

		// Token: 0x06000096 RID: 150 RVA: 0x00003B63 File Offset: 0x00001D63
		[RequiredByNativeCode]
		private void InvokeSampleFramesOverflow(int droppedSampleFrameCount)
		{
			if (this.sampleFramesOverflow != null)
			{
				this.sampleFramesOverflow(this, (uint)droppedSampleFrameCount);
			}
		}

		// Token: 0x06000097 RID: 151
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalGetFormatInfo(uint providerId, out ushort chCount, out uint sRate);

		// Token: 0x06000098 RID: 152
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AudioSampleProvider InternalGetScriptingPtr(uint providerId);

		// Token: 0x06000099 RID: 153
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalSetScriptingPtr(uint providerId, AudioSampleProvider provider);

		// Token: 0x0600009A RID: 154
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool InternalIsValid(uint providerId);

		// Token: 0x0600009B RID: 155
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern uint InternalGetMaxSampleFrameCount(uint providerId);

		// Token: 0x0600009C RID: 156
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern uint InternalGetAvailableSampleFrameCount(uint providerId);

		// Token: 0x0600009D RID: 157
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern uint InternalGetFreeSampleFrameCount(uint providerId);

		// Token: 0x0600009E RID: 158
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern uint InternalGetFreeSampleFrameCountLowThreshold(uint providerId);

		// Token: 0x0600009F RID: 159
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalSetFreeSampleFrameCountLowThreshold(uint providerId, uint sampleFrameCount);

		// Token: 0x060000A0 RID: 160
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool InternalGetEnableSampleFramesAvailableEvents(uint providerId);

		// Token: 0x060000A1 RID: 161
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalSetEnableSampleFramesAvailableEvents(uint providerId, bool enable);

		// Token: 0x060000A2 RID: 162
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalSetSampleFramesAvailableNativeHandler(uint providerId, IntPtr handler, IntPtr userData);

		// Token: 0x060000A3 RID: 163
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalClearSampleFramesAvailableNativeHandler(uint providerId);

		// Token: 0x060000A4 RID: 164
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalSetSampleFramesOverflowNativeHandler(uint providerId, IntPtr handler, IntPtr userData);

		// Token: 0x060000A5 RID: 165
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalClearSampleFramesOverflowNativeHandler(uint providerId);

		// Token: 0x060000A6 RID: 166
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool InternalGetEnableSilencePadding(uint id);

		// Token: 0x060000A7 RID: 167
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalSetEnableSilencePadding(uint id, bool enabled);

		// Token: 0x060000A8 RID: 168
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr InternalGetConsumeSampleFramesNativeFunctionPtr();

		// Token: 0x04000019 RID: 25
		private AudioSampleProvider.ConsumeSampleFramesNativeFunction m_ConsumeSampleFramesNativeFunction;

		// Token: 0x0400001A RID: 26
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private uint <id>k__BackingField;

		// Token: 0x0400001B RID: 27
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ushort <trackIndex>k__BackingField;

		// Token: 0x0400001C RID: 28
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Object <owner>k__BackingField;

		// Token: 0x0400001D RID: 29
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ushort <channelCount>k__BackingField;

		// Token: 0x0400001E RID: 30
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private uint <sampleRate>k__BackingField;

		// Token: 0x0400001F RID: 31
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private AudioSampleProvider.SampleFramesHandler sampleFramesAvailable;

		// Token: 0x04000020 RID: 32
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private AudioSampleProvider.SampleFramesHandler sampleFramesOverflow;

		// Token: 0x0200000E RID: 14
		// (Invoke) Token: 0x060000AA RID: 170
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public delegate uint ConsumeSampleFramesNativeFunction(uint providerId, IntPtr interleavedSampleFrames, uint sampleFrameCount);

		// Token: 0x0200000F RID: 15
		// (Invoke) Token: 0x060000AE RID: 174
		public delegate void SampleFramesHandler(AudioSampleProvider provider, uint sampleFrameCount);

		// Token: 0x02000010 RID: 16
		// (Invoke) Token: 0x060000B2 RID: 178
		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public delegate void SampleFramesEventNativeFunction(IntPtr userData, uint providerId, uint sampleFrameCount);
	}
}
