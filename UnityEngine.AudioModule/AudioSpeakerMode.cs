﻿using System;

namespace UnityEngine
{
	// Token: 0x02000016 RID: 22
	public enum AudioSpeakerMode
	{
		// Token: 0x0400002F RID: 47
		Raw,
		// Token: 0x04000030 RID: 48
		Mono,
		// Token: 0x04000031 RID: 49
		Stereo,
		// Token: 0x04000032 RID: 50
		Quad,
		// Token: 0x04000033 RID: 51
		Surround,
		// Token: 0x04000034 RID: 52
		Mode5point1,
		// Token: 0x04000035 RID: 53
		Mode7point1,
		// Token: 0x04000036 RID: 54
		Prologic
	}
}
