﻿using System;

namespace UnityEngine
{
	// Token: 0x02000004 RID: 4
	internal class AudioAmbisonicExtensionDefinition
	{
		// Token: 0x06000005 RID: 5 RVA: 0x0000213E File Offset: 0x0000033E
		public AudioAmbisonicExtensionDefinition(string ambisonicNameIn, AudioExtensionDefinition definitionIn)
		{
			this.ambisonicPluginName = ambisonicNameIn;
			this.definition = definitionIn;
		}

		// Token: 0x04000008 RID: 8
		public PropertyName ambisonicPluginName;

		// Token: 0x04000009 RID: 9
		public AudioExtensionDefinition definition;
	}
}
