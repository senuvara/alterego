﻿using System;

namespace UnityEngine
{
	// Token: 0x0200001C RID: 28
	public enum AudioClipLoadType
	{
		// Token: 0x0400004E RID: 78
		DecompressOnLoad,
		// Token: 0x0400004F RID: 79
		CompressedInMemory,
		// Token: 0x04000050 RID: 80
		Streaming
	}
}
