﻿using System;

namespace UnityEngine
{
	// Token: 0x02000006 RID: 6
	internal class AudioSourceExtension : ScriptableObject
	{
		// Token: 0x0600000C RID: 12 RVA: 0x000021A9 File Offset: 0x000003A9
		public AudioSourceExtension()
		{
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000D RID: 13 RVA: 0x000021B8 File Offset: 0x000003B8
		// (set) Token: 0x0600000E RID: 14 RVA: 0x000021D3 File Offset: 0x000003D3
		public AudioSource audioSource
		{
			get
			{
				return this.m_audioSource;
			}
			set
			{
				this.m_audioSource = value;
			}
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000021E0 File Offset: 0x000003E0
		public virtual float ReadExtensionProperty(PropertyName propertyName)
		{
			return 0f;
		}

		// Token: 0x06000010 RID: 16 RVA: 0x000021A6 File Offset: 0x000003A6
		public virtual void WriteExtensionProperty(PropertyName propertyName, float propertyValue)
		{
		}

		// Token: 0x06000011 RID: 17 RVA: 0x000021A6 File Offset: 0x000003A6
		public virtual void Play()
		{
		}

		// Token: 0x06000012 RID: 18 RVA: 0x000021A6 File Offset: 0x000003A6
		public virtual void Stop()
		{
		}

		// Token: 0x06000013 RID: 19 RVA: 0x000021A6 File Offset: 0x000003A6
		public virtual void ExtensionUpdate()
		{
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000021FC File Offset: 0x000003FC
		public void OnDestroy()
		{
			this.Stop();
			AudioExtensionManager.RemoveExtensionFromManager(this);
			if (this.audioSource != null)
			{
				if (this.audioSource.spatializerExtension == this)
				{
					this.audioSource.spatializerExtension = null;
				}
				if (this.audioSource.ambisonicExtension == this)
				{
					this.audioSource.ambisonicExtension = null;
				}
			}
		}

		// Token: 0x0400000B RID: 11
		[SerializeField]
		private AudioSource m_audioSource;

		// Token: 0x0400000C RID: 12
		internal int m_ExtensionManagerUpdateIndex = -1;
	}
}
