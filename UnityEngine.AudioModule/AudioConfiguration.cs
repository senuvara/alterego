﻿using System;

namespace UnityEngine
{
	// Token: 0x02000018 RID: 24
	public struct AudioConfiguration
	{
		// Token: 0x0400003C RID: 60
		public AudioSpeakerMode speakerMode;

		// Token: 0x0400003D RID: 61
		public int dspBufferSize;

		// Token: 0x0400003E RID: 62
		public int sampleRate;

		// Token: 0x0400003F RID: 63
		public int numRealVoices;

		// Token: 0x04000040 RID: 64
		public int numVirtualVoices;
	}
}
