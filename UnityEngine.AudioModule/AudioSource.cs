﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Audio;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000026 RID: 38
	[RequireComponent(typeof(Transform))]
	public sealed class AudioSource : AudioBehaviour
	{
		// Token: 0x0600013E RID: 318 RVA: 0x00004333 File Offset: 0x00002533
		public AudioSource()
		{
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600013F RID: 319
		// (set) Token: 0x06000140 RID: 320
		public extern float volume { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000141 RID: 321
		// (set) Token: 0x06000142 RID: 322
		public extern float pitch { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000143 RID: 323
		// (set) Token: 0x06000144 RID: 324
		public extern float time { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000145 RID: 325
		// (set) Token: 0x06000146 RID: 326
		[ThreadAndSerializationSafe]
		public extern int timeSamples { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000147 RID: 327
		// (set) Token: 0x06000148 RID: 328
		[ThreadAndSerializationSafe]
		public extern AudioClip clip { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000149 RID: 329
		// (set) Token: 0x0600014A RID: 330
		public extern AudioMixerGroup outputAudioMixerGroup { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600014B RID: 331
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Play([DefaultValue("0")] ulong delay);

		// Token: 0x0600014C RID: 332 RVA: 0x0000434C File Offset: 0x0000254C
		[ExcludeFromDocs]
		public void Play()
		{
			ulong delay = 0UL;
			this.Play(delay);
		}

		// Token: 0x0600014D RID: 333
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void PlayDelayed(float delay);

		// Token: 0x0600014E RID: 334
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void PlayScheduled(double time);

		// Token: 0x0600014F RID: 335
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetScheduledStartTime(double time);

		// Token: 0x06000150 RID: 336
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetScheduledEndTime(double time);

		// Token: 0x06000151 RID: 337
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Stop();

		// Token: 0x06000152 RID: 338 RVA: 0x00004364 File Offset: 0x00002564
		public void Pause()
		{
			AudioSource.INTERNAL_CALL_Pause(this);
		}

		// Token: 0x06000153 RID: 339
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Pause(AudioSource self);

		// Token: 0x06000154 RID: 340 RVA: 0x0000436D File Offset: 0x0000256D
		public void UnPause()
		{
			AudioSource.INTERNAL_CALL_UnPause(this);
		}

		// Token: 0x06000155 RID: 341
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_UnPause(AudioSource self);

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000156 RID: 342
		public extern bool isPlaying { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000157 RID: 343
		public extern bool isVirtual { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000158 RID: 344 RVA: 0x00004378 File Offset: 0x00002578
		[ExcludeFromDocs]
		public void PlayOneShot(AudioClip clip)
		{
			float volumeScale = 1f;
			this.PlayOneShot(clip, volumeScale);
		}

		// Token: 0x06000159 RID: 345 RVA: 0x00004394 File Offset: 0x00002594
		public void PlayOneShot(AudioClip clip, [DefaultValue("1.0F")] float volumeScale)
		{
			if (clip != null && clip.ambisonic)
			{
				AudioSourceExtension audioSourceExtension = AudioExtensionManager.AddAmbisonicDecoderExtension(this);
				if (audioSourceExtension != null)
				{
					AudioExtensionManager.GetReadyToPlay(audioSourceExtension);
				}
			}
			this.PlayOneShotHelper(clip, volumeScale);
		}

		// Token: 0x0600015A RID: 346
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void PlayOneShotHelper(AudioClip clip, [DefaultValue("1.0F")] float volumeScale);

		// Token: 0x0600015B RID: 347 RVA: 0x000043DC File Offset: 0x000025DC
		[ExcludeFromDocs]
		private void PlayOneShotHelper(AudioClip clip)
		{
			float volumeScale = 1f;
			this.PlayOneShotHelper(clip, volumeScale);
		}

		// Token: 0x0600015C RID: 348 RVA: 0x000043F8 File Offset: 0x000025F8
		[ExcludeFromDocs]
		public static void PlayClipAtPoint(AudioClip clip, Vector3 position)
		{
			float volume = 1f;
			AudioSource.PlayClipAtPoint(clip, position, volume);
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00004414 File Offset: 0x00002614
		public static void PlayClipAtPoint(AudioClip clip, Vector3 position, [DefaultValue("1.0F")] float volume)
		{
			GameObject gameObject = new GameObject("One shot audio");
			gameObject.transform.position = position;
			AudioSource audioSource = (AudioSource)gameObject.AddComponent(typeof(AudioSource));
			audioSource.clip = clip;
			audioSource.spatialBlend = 1f;
			audioSource.volume = volume;
			audioSource.Play();
			Object.Destroy(gameObject, clip.length * ((Time.timeScale >= 0.01f) ? Time.timeScale : 0.01f));
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x0600015E RID: 350
		// (set) Token: 0x0600015F RID: 351
		public extern bool loop { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000160 RID: 352
		// (set) Token: 0x06000161 RID: 353
		public extern bool ignoreListenerVolume { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000162 RID: 354
		// (set) Token: 0x06000163 RID: 355
		public extern bool playOnAwake { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000164 RID: 356
		// (set) Token: 0x06000165 RID: 357
		public extern bool ignoreListenerPause { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000166 RID: 358
		// (set) Token: 0x06000167 RID: 359
		public extern AudioVelocityUpdateMode velocityUpdateMode { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000168 RID: 360
		// (set) Token: 0x06000169 RID: 361
		public extern float panStereo { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600016A RID: 362
		// (set) Token: 0x0600016B RID: 363
		public extern float spatialBlend { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x0600016C RID: 364
		// (set) Token: 0x0600016D RID: 365
		internal extern bool spatializeInternal { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600016E RID: 366 RVA: 0x0000449C File Offset: 0x0000269C
		// (set) Token: 0x0600016F RID: 367 RVA: 0x000044B8 File Offset: 0x000026B8
		public bool spatialize
		{
			get
			{
				return this.spatializeInternal;
			}
			set
			{
				if (this.spatializeInternal != value)
				{
					this.spatializeInternal = value;
					if (value)
					{
						AudioSourceExtension audioSourceExtension = AudioExtensionManager.AddSpatializerExtension(this);
						if (audioSourceExtension != null && this.isPlaying)
						{
							AudioExtensionManager.GetReadyToPlay(audioSourceExtension);
						}
					}
				}
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000170 RID: 368
		// (set) Token: 0x06000171 RID: 369
		public extern bool spatializePostEffects { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000172 RID: 370
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetCustomCurve(AudioSourceCurveType type, AnimationCurve curve);

		// Token: 0x06000173 RID: 371
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimationCurve GetCustomCurve(AudioSourceCurveType type);

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000174 RID: 372
		// (set) Token: 0x06000175 RID: 373
		public extern float reverbZoneMix { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000176 RID: 374
		// (set) Token: 0x06000177 RID: 375
		public extern bool bypassEffects { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000178 RID: 376
		// (set) Token: 0x06000179 RID: 377
		public extern bool bypassListenerEffects { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x0600017A RID: 378
		// (set) Token: 0x0600017B RID: 379
		public extern bool bypassReverbZones { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x0600017C RID: 380
		// (set) Token: 0x0600017D RID: 381
		public extern float dopplerLevel { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x0600017E RID: 382
		// (set) Token: 0x0600017F RID: 383
		public extern float spread { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000180 RID: 384
		// (set) Token: 0x06000181 RID: 385
		public extern int priority { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000182 RID: 386
		// (set) Token: 0x06000183 RID: 387
		public extern bool mute { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000184 RID: 388
		// (set) Token: 0x06000185 RID: 389
		public extern float minDistance { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000186 RID: 390
		// (set) Token: 0x06000187 RID: 391
		public extern float maxDistance { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000188 RID: 392
		// (set) Token: 0x06000189 RID: 393
		public extern AudioRolloffMode rolloffMode { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600018A RID: 394
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetOutputDataHelper(float[] samples, int channel);

		// Token: 0x0600018B RID: 395 RVA: 0x00004508 File Offset: 0x00002708
		[Obsolete("GetOutputData return a float[] is deprecated, use GetOutputData passing a pre allocated array instead.")]
		public float[] GetOutputData(int numSamples, int channel)
		{
			float[] array = new float[numSamples];
			this.GetOutputDataHelper(array, channel);
			return array;
		}

		// Token: 0x0600018C RID: 396 RVA: 0x0000452D File Offset: 0x0000272D
		public void GetOutputData(float[] samples, int channel)
		{
			this.GetOutputDataHelper(samples, channel);
		}

		// Token: 0x0600018D RID: 397
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetSpectrumDataHelper(float[] samples, int channel, FFTWindow window);

		// Token: 0x0600018E RID: 398 RVA: 0x00004538 File Offset: 0x00002738
		[Obsolete("GetSpectrumData returning a float[] is deprecated, use GetSpectrumData passing a pre allocated array instead.")]
		public float[] GetSpectrumData(int numSamples, int channel, FFTWindow window)
		{
			float[] array = new float[numSamples];
			this.GetSpectrumDataHelper(array, channel, window);
			return array;
		}

		// Token: 0x0600018F RID: 399 RVA: 0x0000455E File Offset: 0x0000275E
		public void GetSpectrumData(float[] samples, int channel, FFTWindow window)
		{
			this.GetSpectrumDataHelper(samples, channel, window);
		}

		// Token: 0x06000190 RID: 400
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetNumExtensionProperties();

		// Token: 0x06000191 RID: 401 RVA: 0x0000456C File Offset: 0x0000276C
		internal int GetNumExtensionPropertiesForThisExtension(PropertyName extensionName)
		{
			return AudioSource.INTERNAL_CALL_GetNumExtensionPropertiesForThisExtension(this, ref extensionName);
		}

		// Token: 0x06000192 RID: 402
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_GetNumExtensionPropertiesForThisExtension(AudioSource self, ref PropertyName extensionName);

		// Token: 0x06000193 RID: 403 RVA: 0x0000458C File Offset: 0x0000278C
		internal PropertyName ReadExtensionName(int sourceIndex)
		{
			PropertyName result;
			AudioSource.INTERNAL_CALL_ReadExtensionName(this, sourceIndex, out result);
			return result;
		}

		// Token: 0x06000194 RID: 404
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ReadExtensionName(AudioSource self, int sourceIndex, out PropertyName value);

		// Token: 0x06000195 RID: 405 RVA: 0x000045AC File Offset: 0x000027AC
		internal PropertyName ReadExtensionPropertyName(int sourceIndex)
		{
			PropertyName result;
			AudioSource.INTERNAL_CALL_ReadExtensionPropertyName(this, sourceIndex, out result);
			return result;
		}

		// Token: 0x06000196 RID: 406
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ReadExtensionPropertyName(AudioSource self, int sourceIndex, out PropertyName value);

		// Token: 0x06000197 RID: 407
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern float ReadExtensionPropertyValue(int sourceIndex);

		// Token: 0x06000198 RID: 408 RVA: 0x000045CC File Offset: 0x000027CC
		internal bool ReadExtensionProperty(PropertyName extensionName, PropertyName propertyName, ref float propertyValue)
		{
			return AudioSource.INTERNAL_CALL_ReadExtensionProperty(this, ref extensionName, ref propertyName, ref propertyValue);
		}

		// Token: 0x06000199 RID: 409
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_ReadExtensionProperty(AudioSource self, ref PropertyName extensionName, ref PropertyName propertyName, ref float propertyValue);

		// Token: 0x0600019A RID: 410 RVA: 0x000045EC File Offset: 0x000027EC
		internal void ClearExtensionProperties(PropertyName extensionName)
		{
			AudioSource.INTERNAL_CALL_ClearExtensionProperties(this, ref extensionName);
		}

		// Token: 0x0600019B RID: 411
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ClearExtensionProperties(AudioSource self, ref PropertyName extensionName);

		// Token: 0x0600019C RID: 412 RVA: 0x000045F8 File Offset: 0x000027F8
		internal AudioSourceExtension AddSpatializerExtension(Type extensionType)
		{
			if (this.spatializerExtension == null)
			{
				this.spatializerExtension = (ScriptableObject.CreateInstance(extensionType) as AudioSourceExtension);
			}
			return this.spatializerExtension;
		}

		// Token: 0x0600019D RID: 413 RVA: 0x00004638 File Offset: 0x00002838
		internal AudioSourceExtension AddAmbisonicExtension(Type extensionType)
		{
			if (this.ambisonicExtension == null)
			{
				this.ambisonicExtension = (ScriptableObject.CreateInstance(extensionType) as AudioSourceExtension);
			}
			return this.ambisonicExtension;
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x0600019E RID: 414
		// (set) Token: 0x0600019F RID: 415
		[Obsolete("minVolume is not supported anymore. Use min-, maxDistance and rolloffMode instead.", true)]
		public extern float minVolume { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060001A0 RID: 416
		// (set) Token: 0x060001A1 RID: 417
		[Obsolete("maxVolume is not supported anymore. Use min-, maxDistance and rolloffMode instead.", true)]
		public extern float maxVolume { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060001A2 RID: 418
		// (set) Token: 0x060001A3 RID: 419
		[Obsolete("rolloffFactor is not supported anymore. Use min-, maxDistance and rolloffMode instead.", true)]
		public extern float rolloffFactor { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060001A4 RID: 420
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool SetSpatializerFloat(int index, float value);

		// Token: 0x060001A5 RID: 421
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetSpatializerFloat(int index, out float value);

		// Token: 0x060001A6 RID: 422
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool SetAmbisonicDecoderFloat(int index, float value);

		// Token: 0x060001A7 RID: 423
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetAmbisonicDecoderFloat(int index, out float value);

		// Token: 0x04000068 RID: 104
		internal AudioSourceExtension spatializerExtension = null;

		// Token: 0x04000069 RID: 105
		internal AudioSourceExtension ambisonicExtension = null;
	}
}
