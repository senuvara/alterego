﻿using System;

namespace UnityEngine
{
	// Token: 0x02000020 RID: 32
	public enum AudioVelocityUpdateMode
	{
		// Token: 0x04000054 RID: 84
		Auto,
		// Token: 0x04000055 RID: 85
		Fixed,
		// Token: 0x04000056 RID: 86
		Dynamic
	}
}
