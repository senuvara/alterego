﻿using System;

namespace UnityEngine
{
	// Token: 0x02000013 RID: 19
	public enum WebCamKind
	{
		// Token: 0x04000026 RID: 38
		WideAngle = 1,
		// Token: 0x04000027 RID: 39
		Telephoto,
		// Token: 0x04000028 RID: 40
		ColorAndDepth
	}
}
