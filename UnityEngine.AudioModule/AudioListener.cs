﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000022 RID: 34
	[RequireComponent(typeof(Transform))]
	public sealed class AudioListener : AudioBehaviour
	{
		// Token: 0x06000124 RID: 292 RVA: 0x000041F6 File Offset: 0x000023F6
		public AudioListener()
		{
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000125 RID: 293
		// (set) Token: 0x06000126 RID: 294
		public static extern float volume { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000127 RID: 295
		// (set) Token: 0x06000128 RID: 296
		public static extern bool pause { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000129 RID: 297
		// (set) Token: 0x0600012A RID: 298
		public extern AudioVelocityUpdateMode velocityUpdateMode { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600012B RID: 299
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetOutputDataHelper(float[] samples, int channel);

		// Token: 0x0600012C RID: 300
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetSpectrumDataHelper(float[] samples, int channel, FFTWindow window);

		// Token: 0x0600012D RID: 301 RVA: 0x00004208 File Offset: 0x00002408
		[Obsolete("GetOutputData returning a float[] is deprecated, use GetOutputData and pass a pre allocated array instead.")]
		public static float[] GetOutputData(int numSamples, int channel)
		{
			float[] array = new float[numSamples];
			AudioListener.GetOutputDataHelper(array, channel);
			return array;
		}

		// Token: 0x0600012E RID: 302 RVA: 0x0000422C File Offset: 0x0000242C
		public static void GetOutputData(float[] samples, int channel)
		{
			AudioListener.GetOutputDataHelper(samples, channel);
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00004238 File Offset: 0x00002438
		[Obsolete("GetSpectrumData returning a float[] is deprecated, use GetOutputData and pass a pre allocated array instead.")]
		public static float[] GetSpectrumData(int numSamples, int channel, FFTWindow window)
		{
			float[] array = new float[numSamples];
			AudioListener.GetSpectrumDataHelper(array, channel, window);
			return array;
		}

		// Token: 0x06000130 RID: 304 RVA: 0x0000425D File Offset: 0x0000245D
		public static void GetSpectrumData(float[] samples, int channel, FFTWindow window)
		{
			AudioListener.GetSpectrumDataHelper(samples, channel, window);
		}

		// Token: 0x06000131 RID: 305
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetNumExtensionProperties();

		// Token: 0x06000132 RID: 306 RVA: 0x00004268 File Offset: 0x00002468
		internal int GetNumExtensionPropertiesForThisExtension(PropertyName extensionName)
		{
			return AudioListener.INTERNAL_CALL_GetNumExtensionPropertiesForThisExtension(this, ref extensionName);
		}

		// Token: 0x06000133 RID: 307
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_GetNumExtensionPropertiesForThisExtension(AudioListener self, ref PropertyName extensionName);

		// Token: 0x06000134 RID: 308 RVA: 0x00004288 File Offset: 0x00002488
		internal PropertyName ReadExtensionName(int listenerIndex)
		{
			PropertyName result;
			AudioListener.INTERNAL_CALL_ReadExtensionName(this, listenerIndex, out result);
			return result;
		}

		// Token: 0x06000135 RID: 309
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ReadExtensionName(AudioListener self, int listenerIndex, out PropertyName value);

		// Token: 0x06000136 RID: 310 RVA: 0x000042A8 File Offset: 0x000024A8
		internal PropertyName ReadExtensionPropertyName(int listenerIndex)
		{
			PropertyName result;
			AudioListener.INTERNAL_CALL_ReadExtensionPropertyName(this, listenerIndex, out result);
			return result;
		}

		// Token: 0x06000137 RID: 311
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ReadExtensionPropertyName(AudioListener self, int listenerIndex, out PropertyName value);

		// Token: 0x06000138 RID: 312
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern float ReadExtensionPropertyValue(int listenerIndex);

		// Token: 0x06000139 RID: 313 RVA: 0x000042C8 File Offset: 0x000024C8
		internal bool ReadExtensionProperty(PropertyName extensionName, PropertyName propertyName, ref float propertyValue)
		{
			return AudioListener.INTERNAL_CALL_ReadExtensionProperty(this, ref extensionName, ref propertyName, ref propertyValue);
		}

		// Token: 0x0600013A RID: 314
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_ReadExtensionProperty(AudioListener self, ref PropertyName extensionName, ref PropertyName propertyName, ref float propertyValue);

		// Token: 0x0600013B RID: 315 RVA: 0x000042E8 File Offset: 0x000024E8
		internal void ClearExtensionProperties(PropertyName extensionName)
		{
			AudioListener.INTERNAL_CALL_ClearExtensionProperties(this, ref extensionName);
		}

		// Token: 0x0600013C RID: 316
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ClearExtensionProperties(AudioListener self, ref PropertyName extensionName);

		// Token: 0x0600013D RID: 317 RVA: 0x000042F4 File Offset: 0x000024F4
		internal AudioListenerExtension AddExtension(Type extensionType)
		{
			if (this.spatializerExtension == null)
			{
				this.spatializerExtension = (ScriptableObject.CreateInstance(extensionType) as AudioListenerExtension);
			}
			return this.spatializerExtension;
		}

		// Token: 0x04000057 RID: 87
		internal AudioListenerExtension spatializerExtension = null;
	}
}
