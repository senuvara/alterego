﻿using System;

namespace UnityEngine
{
	// Token: 0x02000023 RID: 35
	public enum FFTWindow
	{
		// Token: 0x04000059 RID: 89
		Rectangular,
		// Token: 0x0400005A RID: 90
		Triangle,
		// Token: 0x0400005B RID: 91
		Hamming,
		// Token: 0x0400005C RID: 92
		Hanning,
		// Token: 0x0400005D RID: 93
		Blackman,
		// Token: 0x0400005E RID: 94
		BlackmanHarris
	}
}
