﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine.Audio
{
	// Token: 0x02000032 RID: 50
	public class AudioMixerSnapshot : Object, ISubAssetNotDuplicatable
	{
		// Token: 0x06000228 RID: 552 RVA: 0x000046AE File Offset: 0x000028AE
		internal AudioMixerSnapshot()
		{
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000229 RID: 553
		public extern AudioMixer audioMixer { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600022A RID: 554
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void TransitionTo(float timeToReach);
	}
}
