﻿using System;

namespace UnityEngine.Audio
{
	// Token: 0x02000030 RID: 48
	public enum AudioMixerUpdateMode
	{
		// Token: 0x04000088 RID: 136
		Normal,
		// Token: 0x04000089 RID: 137
		UnscaledTime
	}
}
