﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Playables;

namespace UnityEngine.Audio
{
	// Token: 0x02000011 RID: 17
	public static class AudioPlayableBinding
	{
		// Token: 0x060000B5 RID: 181 RVA: 0x00003B80 File Offset: 0x00001D80
		public static PlayableBinding Create(string name, Object key)
		{
			Type typeFromHandle = typeof(AudioSource);
			if (AudioPlayableBinding.<>f__mg$cache0 == null)
			{
				AudioPlayableBinding.<>f__mg$cache0 = new PlayableBinding.CreateOutputMethod(AudioPlayableBinding.CreateAudioOutput);
			}
			return PlayableBinding.CreateInternal(name, key, typeFromHandle, AudioPlayableBinding.<>f__mg$cache0);
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00003BC4 File Offset: 0x00001DC4
		private static PlayableOutput CreateAudioOutput(PlayableGraph graph, string name)
		{
			return AudioPlayableOutput.Create(graph, name, null);
		}

		// Token: 0x04000021 RID: 33
		[CompilerGenerated]
		private static PlayableBinding.CreateOutputMethod <>f__mg$cache0;
	}
}
