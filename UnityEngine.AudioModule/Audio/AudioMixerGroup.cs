﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine.Audio
{
	// Token: 0x02000033 RID: 51
	public class AudioMixerGroup : Object, ISubAssetNotDuplicatable
	{
		// Token: 0x0600022B RID: 555 RVA: 0x000046AE File Offset: 0x000028AE
		internal AudioMixerGroup()
		{
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x0600022C RID: 556
		public extern AudioMixer audioMixer { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
