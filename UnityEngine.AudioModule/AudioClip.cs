﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200001D RID: 29
	public sealed class AudioClip : Object
	{
		// Token: 0x060000FE RID: 254 RVA: 0x00003F87 File Offset: 0x00002187
		private AudioClip()
		{
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000FF RID: 255
		public extern float length { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000100 RID: 256
		public extern int samples { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000101 RID: 257
		public extern int channels { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000102 RID: 258
		public extern int frequency { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000103 RID: 259
		[Obsolete("Use AudioClip.loadState instead to get more detailed information about the loading process.")]
		public extern bool isReadyToPlay { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000104 RID: 260
		public extern AudioClipLoadType loadType { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000105 RID: 261
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool LoadAudioData();

		// Token: 0x06000106 RID: 262
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool UnloadAudioData();

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000107 RID: 263
		public extern bool preloadAudioData { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000108 RID: 264
		public extern bool ambisonic { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000109 RID: 265
		public extern AudioDataLoadState loadState { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x0600010A RID: 266
		public extern bool loadInBackground { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600010B RID: 267
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetData(float[] data, int offsetSamples);

		// Token: 0x0600010C RID: 268
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool SetData(float[] data, int offsetSamples);

		// Token: 0x0600010D RID: 269 RVA: 0x00003FA0 File Offset: 0x000021A0
		[Obsolete("The _3D argument of AudioClip is deprecated. Use the spatialBlend property of AudioSource instead to morph between 2D and 3D playback.")]
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool _3D, bool stream)
		{
			return AudioClip.Create(name, lengthSamples, channels, frequency, stream);
		}

		// Token: 0x0600010E RID: 270 RVA: 0x00003FC0 File Offset: 0x000021C0
		[Obsolete("The _3D argument of AudioClip is deprecated. Use the spatialBlend property of AudioSource instead to morph between 2D and 3D playback.")]
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool _3D, bool stream, AudioClip.PCMReaderCallback pcmreadercallback)
		{
			return AudioClip.Create(name, lengthSamples, channels, frequency, stream, pcmreadercallback, null);
		}

		// Token: 0x0600010F RID: 271 RVA: 0x00003FE4 File Offset: 0x000021E4
		[Obsolete("The _3D argument of AudioClip is deprecated. Use the spatialBlend property of AudioSource instead to morph between 2D and 3D playback.")]
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool _3D, bool stream, AudioClip.PCMReaderCallback pcmreadercallback, AudioClip.PCMSetPositionCallback pcmsetpositioncallback)
		{
			return AudioClip.Create(name, lengthSamples, channels, frequency, stream, pcmreadercallback, pcmsetpositioncallback);
		}

		// Token: 0x06000110 RID: 272 RVA: 0x00004008 File Offset: 0x00002208
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool stream)
		{
			return AudioClip.Create(name, lengthSamples, channels, frequency, stream, null, null);
		}

		// Token: 0x06000111 RID: 273 RVA: 0x0000402C File Offset: 0x0000222C
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool stream, AudioClip.PCMReaderCallback pcmreadercallback)
		{
			return AudioClip.Create(name, lengthSamples, channels, frequency, stream, pcmreadercallback, null);
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00004054 File Offset: 0x00002254
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool stream, AudioClip.PCMReaderCallback pcmreadercallback, AudioClip.PCMSetPositionCallback pcmsetpositioncallback)
		{
			if (name == null)
			{
				throw new NullReferenceException();
			}
			if (lengthSamples <= 0)
			{
				throw new ArgumentException("Length of created clip must be larger than 0");
			}
			if (channels <= 0)
			{
				throw new ArgumentException("Number of channels in created clip must be greater than 0");
			}
			if (frequency <= 0)
			{
				throw new ArgumentException("Frequency in created clip must be greater than 0");
			}
			AudioClip audioClip = AudioClip.Construct_Internal();
			if (pcmreadercallback != null)
			{
				audioClip.m_PCMReaderCallback += pcmreadercallback;
			}
			if (pcmsetpositioncallback != null)
			{
				audioClip.m_PCMSetPositionCallback += pcmsetpositioncallback;
			}
			audioClip.Init_Internal(name, lengthSamples, channels, frequency, stream);
			return audioClip;
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x06000113 RID: 275 RVA: 0x000040DC File Offset: 0x000022DC
		// (remove) Token: 0x06000114 RID: 276 RVA: 0x00004114 File Offset: 0x00002314
		private event AudioClip.PCMReaderCallback m_PCMReaderCallback
		{
			add
			{
				AudioClip.PCMReaderCallback pcmreaderCallback = this.m_PCMReaderCallback;
				AudioClip.PCMReaderCallback pcmreaderCallback2;
				do
				{
					pcmreaderCallback2 = pcmreaderCallback;
					pcmreaderCallback = Interlocked.CompareExchange<AudioClip.PCMReaderCallback>(ref this.m_PCMReaderCallback, (AudioClip.PCMReaderCallback)Delegate.Combine(pcmreaderCallback2, value), pcmreaderCallback);
				}
				while (pcmreaderCallback != pcmreaderCallback2);
			}
			remove
			{
				AudioClip.PCMReaderCallback pcmreaderCallback = this.m_PCMReaderCallback;
				AudioClip.PCMReaderCallback pcmreaderCallback2;
				do
				{
					pcmreaderCallback2 = pcmreaderCallback;
					pcmreaderCallback = Interlocked.CompareExchange<AudioClip.PCMReaderCallback>(ref this.m_PCMReaderCallback, (AudioClip.PCMReaderCallback)Delegate.Remove(pcmreaderCallback2, value), pcmreaderCallback);
				}
				while (pcmreaderCallback != pcmreaderCallback2);
			}
		}

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x06000115 RID: 277 RVA: 0x0000414C File Offset: 0x0000234C
		// (remove) Token: 0x06000116 RID: 278 RVA: 0x00004184 File Offset: 0x00002384
		private event AudioClip.PCMSetPositionCallback m_PCMSetPositionCallback
		{
			add
			{
				AudioClip.PCMSetPositionCallback pcmsetPositionCallback = this.m_PCMSetPositionCallback;
				AudioClip.PCMSetPositionCallback pcmsetPositionCallback2;
				do
				{
					pcmsetPositionCallback2 = pcmsetPositionCallback;
					pcmsetPositionCallback = Interlocked.CompareExchange<AudioClip.PCMSetPositionCallback>(ref this.m_PCMSetPositionCallback, (AudioClip.PCMSetPositionCallback)Delegate.Combine(pcmsetPositionCallback2, value), pcmsetPositionCallback);
				}
				while (pcmsetPositionCallback != pcmsetPositionCallback2);
			}
			remove
			{
				AudioClip.PCMSetPositionCallback pcmsetPositionCallback = this.m_PCMSetPositionCallback;
				AudioClip.PCMSetPositionCallback pcmsetPositionCallback2;
				do
				{
					pcmsetPositionCallback2 = pcmsetPositionCallback;
					pcmsetPositionCallback = Interlocked.CompareExchange<AudioClip.PCMSetPositionCallback>(ref this.m_PCMSetPositionCallback, (AudioClip.PCMSetPositionCallback)Delegate.Remove(pcmsetPositionCallback2, value), pcmsetPositionCallback);
				}
				while (pcmsetPositionCallback != pcmsetPositionCallback2);
			}
		}

		// Token: 0x06000117 RID: 279 RVA: 0x000041BA File Offset: 0x000023BA
		[RequiredByNativeCode]
		private void InvokePCMReaderCallback_Internal(float[] data)
		{
			if (this.m_PCMReaderCallback != null)
			{
				this.m_PCMReaderCallback(data);
			}
		}

		// Token: 0x06000118 RID: 280 RVA: 0x000041D4 File Offset: 0x000023D4
		[RequiredByNativeCode]
		private void InvokePCMSetPositionCallback_Internal(int position)
		{
			if (this.m_PCMSetPositionCallback != null)
			{
				this.m_PCMSetPositionCallback(position);
			}
		}

		// Token: 0x06000119 RID: 281
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AudioClip Construct_Internal();

		// Token: 0x0600011A RID: 282
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Init_Internal(string name, int lengthSamples, int channels, int frequency, bool stream);

		// Token: 0x04000051 RID: 81
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private AudioClip.PCMReaderCallback m_PCMReaderCallback = null;

		// Token: 0x04000052 RID: 82
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private AudioClip.PCMSetPositionCallback m_PCMSetPositionCallback = null;

		// Token: 0x0200001E RID: 30
		// (Invoke) Token: 0x0600011C RID: 284
		public delegate void PCMReaderCallback(float[] data);

		// Token: 0x0200001F RID: 31
		// (Invoke) Token: 0x06000120 RID: 288
		public delegate void PCMSetPositionCallback(int position);
	}
}
