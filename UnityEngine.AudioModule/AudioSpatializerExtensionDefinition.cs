﻿using System;

namespace UnityEngine
{
	// Token: 0x02000003 RID: 3
	internal class AudioSpatializerExtensionDefinition
	{
		// Token: 0x06000004 RID: 4 RVA: 0x0000211B File Offset: 0x0000031B
		public AudioSpatializerExtensionDefinition(string spatializerNameIn, AudioExtensionDefinition definitionIn, AudioExtensionDefinition editorDefinitionIn)
		{
			this.spatializerName = spatializerNameIn;
			this.definition = definitionIn;
			this.editorDefinition = editorDefinitionIn;
		}

		// Token: 0x04000005 RID: 5
		public PropertyName spatializerName;

		// Token: 0x04000006 RID: 6
		public AudioExtensionDefinition definition;

		// Token: 0x04000007 RID: 7
		public AudioExtensionDefinition editorDefinition;
	}
}
