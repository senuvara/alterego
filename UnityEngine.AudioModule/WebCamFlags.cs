﻿using System;

namespace UnityEngine
{
	// Token: 0x02000012 RID: 18
	public enum WebCamFlags
	{
		// Token: 0x04000023 RID: 35
		FrontFacing = 1,
		// Token: 0x04000024 RID: 36
		AutoFocusPointSupported
	}
}
