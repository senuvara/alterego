﻿using System;

namespace UnityEngine.Experimental.U2D
{
	// Token: 0x02000003 RID: 3
	public struct SpriteShapeMetaData
	{
		// Token: 0x0400000D RID: 13
		public float height;

		// Token: 0x0400000E RID: 14
		public float bevelCutoff;

		// Token: 0x0400000F RID: 15
		public float bevelSize;

		// Token: 0x04000010 RID: 16
		public uint spriteIndex;

		// Token: 0x04000011 RID: 17
		public bool corner;
	}
}
