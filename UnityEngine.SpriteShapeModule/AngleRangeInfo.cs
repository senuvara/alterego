﻿using System;

namespace UnityEngine.Experimental.U2D
{
	// Token: 0x02000005 RID: 5
	public struct AngleRangeInfo
	{
		// Token: 0x04000016 RID: 22
		public float start;

		// Token: 0x04000017 RID: 23
		public float end;

		// Token: 0x04000018 RID: 24
		public uint order;

		// Token: 0x04000019 RID: 25
		public int[] sprites;
	}
}
