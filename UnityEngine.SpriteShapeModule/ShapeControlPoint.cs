﻿using System;

namespace UnityEngine.Experimental.U2D
{
	// Token: 0x02000004 RID: 4
	public struct ShapeControlPoint
	{
		// Token: 0x04000012 RID: 18
		public Vector3 position;

		// Token: 0x04000013 RID: 19
		public Vector3 leftTangent;

		// Token: 0x04000014 RID: 20
		public Vector3 rightTangent;

		// Token: 0x04000015 RID: 21
		public int mode;
	}
}
