﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.U2D
{
	// Token: 0x02000006 RID: 6
	[NativeType(Header = "Modules/SpriteShape/Public/SpriteShapeRenderer.h")]
	public sealed class SpriteShapeRenderer : Renderer
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public SpriteShapeRenderer()
		{
		}
	}
}
