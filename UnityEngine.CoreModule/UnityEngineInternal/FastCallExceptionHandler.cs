﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x02000155 RID: 341
	// (Invoke) Token: 0x06000EDE RID: 3806
	public delegate void FastCallExceptionHandler(Exception ex);
}
