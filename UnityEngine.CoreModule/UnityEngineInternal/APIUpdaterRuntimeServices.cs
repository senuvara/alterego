﻿using System;
using UnityEngine;

namespace UnityEngineInternal
{
	// Token: 0x02000321 RID: 801
	public sealed class APIUpdaterRuntimeServices
	{
		// Token: 0x06001B4F RID: 6991 RVA: 0x00002370 File Offset: 0x00000570
		public APIUpdaterRuntimeServices()
		{
		}

		// Token: 0x06001B50 RID: 6992 RVA: 0x00030647 File Offset: 0x0002E847
		[Obsolete("Method is not meant to be used at runtime. Please, replace this call with GameObject.AddComponent<T>()/GameObject.AddComponent(Type).", true)]
		public static Component AddComponent(GameObject go, string sourceInfo, string name)
		{
			throw new Exception();
		}
	}
}
