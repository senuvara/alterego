﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x02000085 RID: 133
	public enum MemorylessMode
	{
		// Token: 0x04000168 RID: 360
		Unused,
		// Token: 0x04000169 RID: 361
		Forced,
		// Token: 0x0400016A RID: 362
		Automatic
	}
}
