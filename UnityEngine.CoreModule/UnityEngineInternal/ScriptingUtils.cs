﻿using System;
using System.Reflection;

namespace UnityEngineInternal
{
	// Token: 0x02000157 RID: 343
	public class ScriptingUtils
	{
		// Token: 0x06000EE5 RID: 3813 RVA: 0x00002370 File Offset: 0x00000570
		public ScriptingUtils()
		{
		}

		// Token: 0x06000EE6 RID: 3814 RVA: 0x00019758 File Offset: 0x00017958
		public static Delegate CreateDelegate(Type type, MethodInfo methodInfo)
		{
			return Delegate.CreateDelegate(type, methodInfo);
		}
	}
}
