﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x02000110 RID: 272
	internal enum LightmapType
	{
		// Token: 0x040004F1 RID: 1265
		NoLightmap = -1,
		// Token: 0x040004F2 RID: 1266
		StaticLightmap,
		// Token: 0x040004F3 RID: 1267
		DynamicLightmap
	}
}
