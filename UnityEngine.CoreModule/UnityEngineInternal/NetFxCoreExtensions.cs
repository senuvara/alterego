﻿using System;
using System.Reflection;

namespace UnityEngineInternal
{
	// Token: 0x02000326 RID: 806
	internal static class NetFxCoreExtensions
	{
		// Token: 0x06001B7E RID: 7038 RVA: 0x000309CC File Offset: 0x0002EBCC
		public static Delegate CreateDelegate(this MethodInfo self, Type delegateType, object target)
		{
			return Delegate.CreateDelegate(delegateType, target, self);
		}

		// Token: 0x06001B7F RID: 7039 RVA: 0x000309EC File Offset: 0x0002EBEC
		public static MethodInfo GetMethodInfo(this Delegate self)
		{
			return self.Method;
		}
	}
}
