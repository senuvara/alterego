﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x02000322 RID: 802
	public enum TypeInferenceRules
	{
		// Token: 0x04000A5C RID: 2652
		TypeReferencedByFirstArgument,
		// Token: 0x04000A5D RID: 2653
		TypeReferencedBySecondArgument,
		// Token: 0x04000A5E RID: 2654
		ArrayOfTypeReferencedByFirstArgument,
		// Token: 0x04000A5F RID: 2655
		TypeOfFirstArgument
	}
}
