﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x02000323 RID: 803
	[AttributeUsage(AttributeTargets.Method)]
	[Serializable]
	public class TypeInferenceRuleAttribute : Attribute
	{
		// Token: 0x06001B51 RID: 6993 RVA: 0x0003064F File Offset: 0x0002E84F
		public TypeInferenceRuleAttribute(TypeInferenceRules rule) : this(rule.ToString())
		{
		}

		// Token: 0x06001B52 RID: 6994 RVA: 0x00030665 File Offset: 0x0002E865
		public TypeInferenceRuleAttribute(string rule)
		{
			this._rule = rule;
		}

		// Token: 0x06001B53 RID: 6995 RVA: 0x00030678 File Offset: 0x0002E878
		public override string ToString()
		{
			return this._rule;
		}

		// Token: 0x04000A60 RID: 2656
		private readonly string _rule;
	}
}
