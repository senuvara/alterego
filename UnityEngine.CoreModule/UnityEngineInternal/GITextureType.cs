﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x020002A6 RID: 678
	public enum GITextureType
	{
		// Token: 0x04000897 RID: 2199
		Charting,
		// Token: 0x04000898 RID: 2200
		Albedo,
		// Token: 0x04000899 RID: 2201
		Emissive,
		// Token: 0x0400089A RID: 2202
		Irradiance,
		// Token: 0x0400089B RID: 2203
		Directionality,
		// Token: 0x0400089C RID: 2204
		Baked,
		// Token: 0x0400089D RID: 2205
		BakedDirectional,
		// Token: 0x0400089E RID: 2206
		InputWorkspace,
		// Token: 0x0400089F RID: 2207
		BakedShadowMask,
		// Token: 0x040008A0 RID: 2208
		BakedAlbedo,
		// Token: 0x040008A1 RID: 2209
		BakedEmissive,
		// Token: 0x040008A2 RID: 2210
		BakedCharting,
		// Token: 0x040008A3 RID: 2211
		BakedTexelValidity,
		// Token: 0x040008A4 RID: 2212
		BakedUVOverlap,
		// Token: 0x040008A5 RID: 2213
		BakedLightmapCulling
	}
}
