﻿using System;
using System.Reflection;

namespace UnityEngineInternal
{
	// Token: 0x02000156 RID: 342
	// (Invoke) Token: 0x06000EE2 RID: 3810
	public delegate MethodInfo GetMethodDelegate(Type classType, string methodName, bool searchBaseTypes, bool instanceMethod, Type[] methodParamTypes);
}
