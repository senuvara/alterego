﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngineInternal
{
	// Token: 0x020002A7 RID: 679
	[NativeHeader("Runtime/Export/GI/GIDebugVisualisation.bindings.h")]
	public static class GIDebugVisualisation
	{
		// Token: 0x0600188C RID: 6284
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ResetRuntimeInputTextures();

		// Token: 0x0600188D RID: 6285
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void PlayCycleMode();

		// Token: 0x0600188E RID: 6286
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void PauseCycleMode();

		// Token: 0x0600188F RID: 6287
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void StopCycleMode();

		// Token: 0x06001890 RID: 6288
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void CycleSkipSystems(int skip);

		// Token: 0x06001891 RID: 6289
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void CycleSkipInstances(int skip);

		// Token: 0x1700046F RID: 1135
		// (get) Token: 0x06001892 RID: 6290
		public static extern bool cycleMode { [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000470 RID: 1136
		// (get) Token: 0x06001893 RID: 6291
		public static extern bool pauseCycleMode { [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000471 RID: 1137
		// (get) Token: 0x06001894 RID: 6292
		// (set) Token: 0x06001895 RID: 6293
		public static extern GITextureType texType { [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
