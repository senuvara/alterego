﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngineInternal
{
	// Token: 0x02000086 RID: 134
	[NativeHeader("Runtime/Misc/PlayerSettings.h")]
	public class MemorylessManager
	{
		// Token: 0x0600069F RID: 1695 RVA: 0x00002370 File Offset: 0x00000570
		public MemorylessManager()
		{
		}

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x060006A0 RID: 1696 RVA: 0x0000E8A4 File Offset: 0x0000CAA4
		// (set) Token: 0x060006A1 RID: 1697 RVA: 0x0000E8BE File Offset: 0x0000CABE
		public static MemorylessMode depthMemorylessMode
		{
			get
			{
				return MemorylessManager.GetFramebufferDepthMemorylessMode();
			}
			set
			{
				MemorylessManager.SetFramebufferDepthMemorylessMode(value);
			}
		}

		// Token: 0x060006A2 RID: 1698
		[StaticAccessor("GetPlayerSettings()", StaticAccessorType.Dot)]
		[NativeMethod(Name = "GetFramebufferDepthMemorylessMode")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern MemorylessMode GetFramebufferDepthMemorylessMode();

		// Token: 0x060006A3 RID: 1699
		[StaticAccessor("GetPlayerSettings()", StaticAccessorType.Dot)]
		[NativeMethod(Name = "SetFramebufferDepthMemorylessMode")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetFramebufferDepthMemorylessMode(MemorylessMode mode);
	}
}
