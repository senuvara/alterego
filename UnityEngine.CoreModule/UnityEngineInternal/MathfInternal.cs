﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x0200014F RID: 335
	public struct MathfInternal
	{
		// Token: 0x06000E23 RID: 3619 RVA: 0x00018668 File Offset: 0x00016868
		// Note: this type is marked as 'beforefieldinit'.
		static MathfInternal()
		{
		}

		// Token: 0x040006EC RID: 1772
		public static volatile float FloatMinNormal = 1.1754944E-38f;

		// Token: 0x040006ED RID: 1773
		public static volatile float FloatMinDenormal = float.Epsilon;

		// Token: 0x040006EE RID: 1774
		public static bool IsFlushToZeroEnabled = MathfInternal.FloatMinDenormal == 0f;
	}
}
