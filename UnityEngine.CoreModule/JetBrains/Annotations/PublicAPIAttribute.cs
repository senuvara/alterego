﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace JetBrains.Annotations
{
	// Token: 0x0200013B RID: 315
	[MeansImplicitUse]
	public sealed class PublicAPIAttribute : Attribute
	{
		// Token: 0x06000CE1 RID: 3297 RVA: 0x000086CD File Offset: 0x000068CD
		public PublicAPIAttribute()
		{
		}

		// Token: 0x06000CE2 RID: 3298 RVA: 0x000142F3 File Offset: 0x000124F3
		public PublicAPIAttribute([NotNull] string comment)
		{
			this.Comment = comment;
		}

		// Token: 0x17000292 RID: 658
		// (get) Token: 0x06000CE3 RID: 3299 RVA: 0x00014304 File Offset: 0x00012504
		// (set) Token: 0x06000CE4 RID: 3300 RVA: 0x0001431E File Offset: 0x0001251E
		[NotNull]
		public string Comment
		{
			[CompilerGenerated]
			get
			{
				return this.<Comment>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Comment>k__BackingField = value;
			}
		}

		// Token: 0x04000568 RID: 1384
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <Comment>k__BackingField;
	}
}
