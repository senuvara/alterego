﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace JetBrains.Annotations
{
	// Token: 0x02000132 RID: 306
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public sealed class NotifyPropertyChangedInvocatorAttribute : Attribute
	{
		// Token: 0x06000CBF RID: 3263 RVA: 0x000086CD File Offset: 0x000068CD
		public NotifyPropertyChangedInvocatorAttribute()
		{
		}

		// Token: 0x06000CC0 RID: 3264 RVA: 0x000140DF File Offset: 0x000122DF
		public NotifyPropertyChangedInvocatorAttribute(string parameterName)
		{
			this.ParameterName = parameterName;
		}

		// Token: 0x17000289 RID: 649
		// (get) Token: 0x06000CC1 RID: 3265 RVA: 0x000140F0 File Offset: 0x000122F0
		// (set) Token: 0x06000CC2 RID: 3266 RVA: 0x0001410A File Offset: 0x0001230A
		public string ParameterName
		{
			[CompilerGenerated]
			get
			{
				return this.<ParameterName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ParameterName>k__BackingField = value;
			}
		}

		// Token: 0x04000554 RID: 1364
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <ParameterName>k__BackingField;
	}
}
