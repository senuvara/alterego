﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace JetBrains.Annotations
{
	// Token: 0x02000136 RID: 310
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	[BaseTypeRequired(typeof(Attribute))]
	public sealed class BaseTypeRequiredAttribute : Attribute
	{
		// Token: 0x06000CCE RID: 3278 RVA: 0x000141BF File Offset: 0x000123BF
		public BaseTypeRequiredAttribute([NotNull] Type baseType)
		{
			this.BaseType = baseType;
		}

		// Token: 0x1700028D RID: 653
		// (get) Token: 0x06000CCF RID: 3279 RVA: 0x000141D0 File Offset: 0x000123D0
		// (set) Token: 0x06000CD0 RID: 3280 RVA: 0x000141EA File Offset: 0x000123EA
		[NotNull]
		public Type BaseType
		{
			[CompilerGenerated]
			get
			{
				return this.<BaseType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<BaseType>k__BackingField = value;
			}
		}

		// Token: 0x04000558 RID: 1368
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Type <BaseType>k__BackingField;
	}
}
