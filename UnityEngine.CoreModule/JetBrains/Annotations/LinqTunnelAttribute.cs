﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000141 RID: 321
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class LinqTunnelAttribute : Attribute
	{
		// Token: 0x06000CED RID: 3309 RVA: 0x0000898B File Offset: 0x00006B8B
		public LinqTunnelAttribute()
		{
		}
	}
}
