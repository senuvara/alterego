﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200013A RID: 314
	[Flags]
	public enum ImplicitUseTargetFlags
	{
		// Token: 0x04000564 RID: 1380
		Default = 1,
		// Token: 0x04000565 RID: 1381
		Itself = 1,
		// Token: 0x04000566 RID: 1382
		Members = 2,
		// Token: 0x04000567 RID: 1383
		WithMembers = 3
	}
}
