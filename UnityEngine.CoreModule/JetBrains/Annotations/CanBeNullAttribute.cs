﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200012E RID: 302
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.Delegate, AllowMultiple = false, Inherited = true)]
	public sealed class CanBeNullAttribute : Attribute
	{
		// Token: 0x06000CB9 RID: 3257 RVA: 0x0000898B File Offset: 0x00006B8B
		public CanBeNullAttribute()
		{
		}
	}
}
