﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace JetBrains.Annotations
{
	// Token: 0x02000133 RID: 307
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
	public sealed class ContractAnnotationAttribute : Attribute
	{
		// Token: 0x06000CC3 RID: 3267 RVA: 0x00014113 File Offset: 0x00012313
		public ContractAnnotationAttribute([NotNull] string contract) : this(contract, false)
		{
		}

		// Token: 0x06000CC4 RID: 3268 RVA: 0x0001411E File Offset: 0x0001231E
		public ContractAnnotationAttribute([NotNull] string contract, bool forceFullStates)
		{
			this.Contract = contract;
			this.ForceFullStates = forceFullStates;
		}

		// Token: 0x1700028A RID: 650
		// (get) Token: 0x06000CC5 RID: 3269 RVA: 0x00014138 File Offset: 0x00012338
		// (set) Token: 0x06000CC6 RID: 3270 RVA: 0x00014152 File Offset: 0x00012352
		public string Contract
		{
			[CompilerGenerated]
			get
			{
				return this.<Contract>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Contract>k__BackingField = value;
			}
		}

		// Token: 0x1700028B RID: 651
		// (get) Token: 0x06000CC7 RID: 3271 RVA: 0x0001415C File Offset: 0x0001235C
		// (set) Token: 0x06000CC8 RID: 3272 RVA: 0x00014176 File Offset: 0x00012376
		public bool ForceFullStates
		{
			[CompilerGenerated]
			get
			{
				return this.<ForceFullStates>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ForceFullStates>k__BackingField = value;
			}
		}

		// Token: 0x04000555 RID: 1365
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <Contract>k__BackingField;

		// Token: 0x04000556 RID: 1366
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <ForceFullStates>k__BackingField;
	}
}
