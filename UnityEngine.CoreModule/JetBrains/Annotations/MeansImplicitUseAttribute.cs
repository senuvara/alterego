﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace JetBrains.Annotations
{
	// Token: 0x02000138 RID: 312
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class MeansImplicitUseAttribute : Attribute
	{
		// Token: 0x06000CD9 RID: 3289 RVA: 0x00014273 File Offset: 0x00012473
		public MeansImplicitUseAttribute() : this(ImplicitUseKindFlags.Default, ImplicitUseTargetFlags.Default)
		{
		}

		// Token: 0x06000CDA RID: 3290 RVA: 0x0001427E File Offset: 0x0001247E
		public MeansImplicitUseAttribute(ImplicitUseKindFlags useKindFlags) : this(useKindFlags, ImplicitUseTargetFlags.Default)
		{
		}

		// Token: 0x06000CDB RID: 3291 RVA: 0x00014289 File Offset: 0x00012489
		public MeansImplicitUseAttribute(ImplicitUseTargetFlags targetFlags) : this(ImplicitUseKindFlags.Default, targetFlags)
		{
		}

		// Token: 0x06000CDC RID: 3292 RVA: 0x00014294 File Offset: 0x00012494
		public MeansImplicitUseAttribute(ImplicitUseKindFlags useKindFlags, ImplicitUseTargetFlags targetFlags)
		{
			this.UseKindFlags = useKindFlags;
			this.TargetFlags = targetFlags;
		}

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x06000CDD RID: 3293 RVA: 0x000142AC File Offset: 0x000124AC
		// (set) Token: 0x06000CDE RID: 3294 RVA: 0x000142C6 File Offset: 0x000124C6
		[UsedImplicitly]
		public ImplicitUseKindFlags UseKindFlags
		{
			[CompilerGenerated]
			get
			{
				return this.<UseKindFlags>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<UseKindFlags>k__BackingField = value;
			}
		}

		// Token: 0x17000291 RID: 657
		// (get) Token: 0x06000CDF RID: 3295 RVA: 0x000142D0 File Offset: 0x000124D0
		// (set) Token: 0x06000CE0 RID: 3296 RVA: 0x000142EA File Offset: 0x000124EA
		[UsedImplicitly]
		public ImplicitUseTargetFlags TargetFlags
		{
			[CompilerGenerated]
			get
			{
				return this.<TargetFlags>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<TargetFlags>k__BackingField = value;
			}
		}

		// Token: 0x0400055B RID: 1371
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ImplicitUseKindFlags <UseKindFlags>k__BackingField;

		// Token: 0x0400055C RID: 1372
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ImplicitUseTargetFlags <TargetFlags>k__BackingField;
	}
}
