﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200013F RID: 319
	[AttributeUsage(AttributeTargets.Parameter)]
	public sealed class NoEnumerationAttribute : Attribute
	{
		// Token: 0x06000CEB RID: 3307 RVA: 0x0000898B File Offset: 0x00006B8B
		public NoEnumerationAttribute()
		{
		}
	}
}
