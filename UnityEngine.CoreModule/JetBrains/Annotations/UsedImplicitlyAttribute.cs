﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace JetBrains.Annotations
{
	// Token: 0x02000137 RID: 311
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
	public sealed class UsedImplicitlyAttribute : Attribute
	{
		// Token: 0x06000CD1 RID: 3281 RVA: 0x000141F3 File Offset: 0x000123F3
		public UsedImplicitlyAttribute() : this(ImplicitUseKindFlags.Default, ImplicitUseTargetFlags.Default)
		{
		}

		// Token: 0x06000CD2 RID: 3282 RVA: 0x000141FE File Offset: 0x000123FE
		public UsedImplicitlyAttribute(ImplicitUseKindFlags useKindFlags) : this(useKindFlags, ImplicitUseTargetFlags.Default)
		{
		}

		// Token: 0x06000CD3 RID: 3283 RVA: 0x00014209 File Offset: 0x00012409
		public UsedImplicitlyAttribute(ImplicitUseTargetFlags targetFlags) : this(ImplicitUseKindFlags.Default, targetFlags)
		{
		}

		// Token: 0x06000CD4 RID: 3284 RVA: 0x00014214 File Offset: 0x00012414
		public UsedImplicitlyAttribute(ImplicitUseKindFlags useKindFlags, ImplicitUseTargetFlags targetFlags)
		{
			this.UseKindFlags = useKindFlags;
			this.TargetFlags = targetFlags;
		}

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x06000CD5 RID: 3285 RVA: 0x0001422C File Offset: 0x0001242C
		// (set) Token: 0x06000CD6 RID: 3286 RVA: 0x00014246 File Offset: 0x00012446
		public ImplicitUseKindFlags UseKindFlags
		{
			[CompilerGenerated]
			get
			{
				return this.<UseKindFlags>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<UseKindFlags>k__BackingField = value;
			}
		}

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x06000CD7 RID: 3287 RVA: 0x00014250 File Offset: 0x00012450
		// (set) Token: 0x06000CD8 RID: 3288 RVA: 0x0001426A File Offset: 0x0001246A
		public ImplicitUseTargetFlags TargetFlags
		{
			[CompilerGenerated]
			get
			{
				return this.<TargetFlags>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<TargetFlags>k__BackingField = value;
			}
		}

		// Token: 0x04000559 RID: 1369
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ImplicitUseKindFlags <UseKindFlags>k__BackingField;

		// Token: 0x0400055A RID: 1370
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private ImplicitUseTargetFlags <TargetFlags>k__BackingField;
	}
}
