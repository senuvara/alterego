﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000135 RID: 309
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface, AllowMultiple = false, Inherited = true)]
	public sealed class CannotApplyEqualityOperatorAttribute : Attribute
	{
		// Token: 0x06000CCD RID: 3277 RVA: 0x0000898B File Offset: 0x00006B8B
		public CannotApplyEqualityOperatorAttribute()
		{
		}
	}
}
