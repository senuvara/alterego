﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200012F RID: 303
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.Delegate, AllowMultiple = false, Inherited = true)]
	public sealed class NotNullAttribute : Attribute
	{
		// Token: 0x06000CBA RID: 3258 RVA: 0x0000898B File Offset: 0x00006B8B
		public NotNullAttribute()
		{
		}
	}
}
