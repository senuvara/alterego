﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200013D RID: 317
	[AttributeUsage(AttributeTargets.Method, Inherited = true)]
	public sealed class PureAttribute : Attribute
	{
		// Token: 0x06000CE6 RID: 3302 RVA: 0x0000898B File Offset: 0x00006B8B
		public PureAttribute()
		{
		}
	}
}
