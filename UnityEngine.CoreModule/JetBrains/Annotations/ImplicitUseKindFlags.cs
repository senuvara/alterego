﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000139 RID: 313
	[Flags]
	public enum ImplicitUseKindFlags
	{
		// Token: 0x0400055E RID: 1374
		Default = 7,
		// Token: 0x0400055F RID: 1375
		Access = 1,
		// Token: 0x04000560 RID: 1376
		Assign = 2,
		// Token: 0x04000561 RID: 1377
		InstantiatedWithFixedConstructorSignature = 4,
		// Token: 0x04000562 RID: 1378
		InstantiatedNoFixedConstructorSignature = 8
	}
}
