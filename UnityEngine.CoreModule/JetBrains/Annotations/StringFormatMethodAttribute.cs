﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace JetBrains.Annotations
{
	// Token: 0x02000130 RID: 304
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public sealed class StringFormatMethodAttribute : Attribute
	{
		// Token: 0x06000CBB RID: 3259 RVA: 0x000140AB File Offset: 0x000122AB
		public StringFormatMethodAttribute(string formatParameterName)
		{
			this.FormatParameterName = formatParameterName;
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x06000CBC RID: 3260 RVA: 0x000140BC File Offset: 0x000122BC
		// (set) Token: 0x06000CBD RID: 3261 RVA: 0x000140D6 File Offset: 0x000122D6
		public string FormatParameterName
		{
			[CompilerGenerated]
			get
			{
				return this.<FormatParameterName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<FormatParameterName>k__BackingField = value;
			}
		}

		// Token: 0x04000553 RID: 1363
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <FormatParameterName>k__BackingField;
	}
}
