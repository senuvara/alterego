﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200013C RID: 316
	[AttributeUsage(AttributeTargets.Parameter, Inherited = true)]
	public sealed class InstantHandleAttribute : Attribute
	{
		// Token: 0x06000CE5 RID: 3301 RVA: 0x0000898B File Offset: 0x00006B8B
		public InstantHandleAttribute()
		{
		}
	}
}
