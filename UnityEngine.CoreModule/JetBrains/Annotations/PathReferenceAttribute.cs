﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace JetBrains.Annotations
{
	// Token: 0x0200013E RID: 318
	[AttributeUsage(AttributeTargets.Parameter)]
	public class PathReferenceAttribute : Attribute
	{
		// Token: 0x06000CE7 RID: 3303 RVA: 0x000086CD File Offset: 0x000068CD
		public PathReferenceAttribute()
		{
		}

		// Token: 0x06000CE8 RID: 3304 RVA: 0x00014327 File Offset: 0x00012527
		public PathReferenceAttribute([PathReference] string basePath)
		{
			this.BasePath = basePath;
		}

		// Token: 0x17000293 RID: 659
		// (get) Token: 0x06000CE9 RID: 3305 RVA: 0x00014338 File Offset: 0x00012538
		// (set) Token: 0x06000CEA RID: 3306 RVA: 0x00014352 File Offset: 0x00012552
		[NotNull]
		public string BasePath
		{
			[CompilerGenerated]
			get
			{
				return this.<BasePath>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<BasePath>k__BackingField = value;
			}
		}

		// Token: 0x04000569 RID: 1385
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <BasePath>k__BackingField;
	}
}
