﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace JetBrains.Annotations
{
	// Token: 0x02000134 RID: 308
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
	public sealed class LocalizationRequiredAttribute : Attribute
	{
		// Token: 0x06000CC9 RID: 3273 RVA: 0x0001417F File Offset: 0x0001237F
		public LocalizationRequiredAttribute() : this(true)
		{
		}

		// Token: 0x06000CCA RID: 3274 RVA: 0x00014189 File Offset: 0x00012389
		public LocalizationRequiredAttribute(bool required)
		{
			this.Required = required;
		}

		// Token: 0x1700028C RID: 652
		// (get) Token: 0x06000CCB RID: 3275 RVA: 0x0001419C File Offset: 0x0001239C
		// (set) Token: 0x06000CCC RID: 3276 RVA: 0x000141B6 File Offset: 0x000123B6
		public bool Required
		{
			[CompilerGenerated]
			get
			{
				return this.<Required>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Required>k__BackingField = value;
			}
		}

		// Token: 0x04000557 RID: 1367
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <Required>k__BackingField;
	}
}
