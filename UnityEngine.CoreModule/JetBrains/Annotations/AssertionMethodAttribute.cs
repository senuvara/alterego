﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000140 RID: 320
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public sealed class AssertionMethodAttribute : Attribute
	{
		// Token: 0x06000CEC RID: 3308 RVA: 0x0000898B File Offset: 0x00006B8B
		public AssertionMethodAttribute()
		{
		}
	}
}
