﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000131 RID: 305
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
	public sealed class InvokerParameterNameAttribute : Attribute
	{
		// Token: 0x06000CBE RID: 3262 RVA: 0x0000898B File Offset: 0x00006B8B
		public InvokerParameterNameAttribute()
		{
		}
	}
}
