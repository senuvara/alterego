﻿using System;

namespace UnityEngine
{
	// Token: 0x020000E2 RID: 226
	public enum LineTextureMode
	{
		// Token: 0x04000377 RID: 887
		Stretch,
		// Token: 0x04000378 RID: 888
		Tile,
		// Token: 0x04000379 RID: 889
		DistributePerSegment,
		// Token: 0x0400037A RID: 890
		RepeatPerSegment
	}
}
