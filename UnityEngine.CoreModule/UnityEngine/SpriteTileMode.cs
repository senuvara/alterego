﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035B RID: 859
	public enum SpriteTileMode
	{
		// Token: 0x04000AE5 RID: 2789
		Continuous,
		// Token: 0x04000AE6 RID: 2790
		Adaptive
	}
}
