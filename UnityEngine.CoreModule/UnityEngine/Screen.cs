﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000083 RID: 131
	[StaticAccessor("GetScreenManager()", StaticAccessorType.Dot)]
	[NativeHeader("Runtime/Graphics/ScreenManager.h")]
	[NativeHeader("Runtime/Graphics/GraphicsScriptBindings.h")]
	public sealed class Screen
	{
		// Token: 0x0600066E RID: 1646 RVA: 0x00002370 File Offset: 0x00000570
		public Screen()
		{
		}

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x0600066F RID: 1647
		public static extern int width { [NativeMethod(Name = "GetWidth", IsThreadSafe = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x06000670 RID: 1648
		public static extern int height { [NativeMethod(Name = "GetHeight", IsThreadSafe = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x06000671 RID: 1649
		public static extern float dpi { [NativeName("GetDPI")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000672 RID: 1650
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void RequestOrientation(ScreenOrientation orient);

		// Token: 0x06000673 RID: 1651
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern ScreenOrientation GetScreenOrientation();

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x06000674 RID: 1652 RVA: 0x0000E6A8 File Offset: 0x0000C8A8
		// (set) Token: 0x06000675 RID: 1653 RVA: 0x0000E6C2 File Offset: 0x0000C8C2
		public static ScreenOrientation orientation
		{
			get
			{
				return Screen.GetScreenOrientation();
			}
			set
			{
				if (value == ScreenOrientation.Unknown)
				{
					Debug.Log("ScreenOrientation.Unknown is deprecated. Please use ScreenOrientation.AutoRotation");
					value = ScreenOrientation.AutoRotation;
				}
				Screen.RequestOrientation(value);
			}
		}

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x06000676 RID: 1654
		// (set) Token: 0x06000677 RID: 1655
		[NativeProperty("ScreenTimeout")]
		public static extern int sleepTimeout { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000678 RID: 1656
		[NativeName("GetIsOrientationEnabled")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsOrientationEnabled(EnabledOrientation orient);

		// Token: 0x06000679 RID: 1657
		[NativeName("SetIsOrientationEnabled")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetOrientationEnabled(EnabledOrientation orient, bool enabled);

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x0600067A RID: 1658 RVA: 0x0000E6E0 File Offset: 0x0000C8E0
		// (set) Token: 0x0600067B RID: 1659 RVA: 0x0000E6FB File Offset: 0x0000C8FB
		public static bool autorotateToPortrait
		{
			get
			{
				return Screen.IsOrientationEnabled(EnabledOrientation.kAutorotateToPortrait);
			}
			set
			{
				Screen.SetOrientationEnabled(EnabledOrientation.kAutorotateToPortrait, value);
			}
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x0600067C RID: 1660 RVA: 0x0000E708 File Offset: 0x0000C908
		// (set) Token: 0x0600067D RID: 1661 RVA: 0x0000E723 File Offset: 0x0000C923
		public static bool autorotateToPortraitUpsideDown
		{
			get
			{
				return Screen.IsOrientationEnabled(EnabledOrientation.kAutorotateToPortraitUpsideDown);
			}
			set
			{
				Screen.SetOrientationEnabled(EnabledOrientation.kAutorotateToPortraitUpsideDown, value);
			}
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x0600067E RID: 1662 RVA: 0x0000E730 File Offset: 0x0000C930
		// (set) Token: 0x0600067F RID: 1663 RVA: 0x0000E74B File Offset: 0x0000C94B
		public static bool autorotateToLandscapeLeft
		{
			get
			{
				return Screen.IsOrientationEnabled(EnabledOrientation.kAutorotateToLandscapeLeft);
			}
			set
			{
				Screen.SetOrientationEnabled(EnabledOrientation.kAutorotateToLandscapeLeft, value);
			}
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x06000680 RID: 1664 RVA: 0x0000E758 File Offset: 0x0000C958
		// (set) Token: 0x06000681 RID: 1665 RVA: 0x0000E773 File Offset: 0x0000C973
		public static bool autorotateToLandscapeRight
		{
			get
			{
				return Screen.IsOrientationEnabled(EnabledOrientation.kAutorotateToLandscapeRight);
			}
			set
			{
				Screen.SetOrientationEnabled(EnabledOrientation.kAutorotateToLandscapeRight, value);
			}
		}

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x06000682 RID: 1666 RVA: 0x0000E780 File Offset: 0x0000C980
		public static Resolution currentResolution
		{
			get
			{
				Resolution result;
				Screen.get_currentResolution_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x06000683 RID: 1667
		// (set) Token: 0x06000684 RID: 1668
		public static extern bool fullScreen { [NativeName("IsFullscreen")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("RequestSetFullscreenFromScript")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x06000685 RID: 1669
		// (set) Token: 0x06000686 RID: 1670
		public static extern FullScreenMode fullScreenMode { [NativeName("GetFullscreenMode")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("RequestSetFullscreenModeFromScript")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x06000687 RID: 1671 RVA: 0x0000E798 File Offset: 0x0000C998
		public static Rect safeArea
		{
			get
			{
				Rect result;
				Screen.get_safeArea_Injected(out result);
				return result;
			}
		}

		// Token: 0x06000688 RID: 1672
		[NativeName("RequestResolution")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetResolution(int width, int height, FullScreenMode fullscreenMode, [UnityEngine.Internal.DefaultValue("0")] int preferredRefreshRate);

		// Token: 0x06000689 RID: 1673 RVA: 0x0000E7AD File Offset: 0x0000C9AD
		public static void SetResolution(int width, int height, FullScreenMode fullscreenMode)
		{
			Screen.SetResolution(width, height, fullscreenMode, 0);
		}

		// Token: 0x0600068A RID: 1674 RVA: 0x0000E7B9 File Offset: 0x0000C9B9
		public static void SetResolution(int width, int height, bool fullscreen, [UnityEngine.Internal.DefaultValue("0")] int preferredRefreshRate)
		{
			Screen.SetResolution(width, height, (!fullscreen) ? FullScreenMode.Windowed : FullScreenMode.FullScreenWindow, preferredRefreshRate);
		}

		// Token: 0x0600068B RID: 1675 RVA: 0x0000E7D1 File Offset: 0x0000C9D1
		public static void SetResolution(int width, int height, bool fullscreen)
		{
			Screen.SetResolution(width, height, fullscreen, 0);
		}

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x0600068C RID: 1676
		public static extern Resolution[] resolutions { [FreeFunction("ScreenScripting::GetResolutions")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x0600068D RID: 1677 RVA: 0x0000E7E0 File Offset: 0x0000C9E0
		// (set) Token: 0x0600068E RID: 1678 RVA: 0x0000E7FD File Offset: 0x0000C9FD
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use Cursor.lockState and Cursor.visible instead.", false)]
		public static bool lockCursor
		{
			get
			{
				return CursorLockMode.Locked == Cursor.lockState;
			}
			set
			{
				if (value)
				{
					Cursor.visible = false;
					Cursor.lockState = CursorLockMode.Locked;
				}
				else
				{
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
				}
			}
		}

		// Token: 0x0600068F RID: 1679
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_currentResolution_Injected(out Resolution ret);

		// Token: 0x06000690 RID: 1680
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_safeArea_Injected(out Rect ret);
	}
}
