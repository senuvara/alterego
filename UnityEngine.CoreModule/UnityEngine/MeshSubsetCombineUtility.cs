﻿using System;
using System.Collections.Generic;

namespace UnityEngine
{
	// Token: 0x0200031D RID: 797
	internal class MeshSubsetCombineUtility
	{
		// Token: 0x06001B4E RID: 6990 RVA: 0x00002370 File Offset: 0x00000570
		public MeshSubsetCombineUtility()
		{
		}

		// Token: 0x0200031E RID: 798
		public struct MeshInstance
		{
			// Token: 0x04000A4D RID: 2637
			public int meshInstanceID;

			// Token: 0x04000A4E RID: 2638
			public int rendererInstanceID;

			// Token: 0x04000A4F RID: 2639
			public int additionalVertexStreamsMeshInstanceID;

			// Token: 0x04000A50 RID: 2640
			public Matrix4x4 transform;

			// Token: 0x04000A51 RID: 2641
			public Vector4 lightmapScaleOffset;

			// Token: 0x04000A52 RID: 2642
			public Vector4 realtimeLightmapScaleOffset;
		}

		// Token: 0x0200031F RID: 799
		public struct SubMeshInstance
		{
			// Token: 0x04000A53 RID: 2643
			public int meshInstanceID;

			// Token: 0x04000A54 RID: 2644
			public int vertexOffset;

			// Token: 0x04000A55 RID: 2645
			public int gameObjectInstanceID;

			// Token: 0x04000A56 RID: 2646
			public int subMeshIndex;

			// Token: 0x04000A57 RID: 2647
			public Matrix4x4 transform;
		}

		// Token: 0x02000320 RID: 800
		public struct MeshContainer
		{
			// Token: 0x04000A58 RID: 2648
			public GameObject gameObject;

			// Token: 0x04000A59 RID: 2649
			public MeshSubsetCombineUtility.MeshInstance instance;

			// Token: 0x04000A5A RID: 2650
			public List<MeshSubsetCombineUtility.SubMeshInstance> subMeshInstances;
		}
	}
}
