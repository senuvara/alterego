﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B1 RID: 177
	public enum StereoTargetEyeMask
	{
		// Token: 0x040001BA RID: 442
		None,
		// Token: 0x040001BB RID: 443
		Left,
		// Token: 0x040001BC RID: 444
		Right,
		// Token: 0x040001BD RID: 445
		Both
	}
}
