﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;
using UnityEngineInternal;

namespace UnityEngine
{
	// Token: 0x02000079 RID: 121
	[NativeHeader("Runtime/Export/GameObject.bindings.h")]
	[ExcludeFromPreset]
	[UsedByNativeCode]
	public sealed class GameObject : Object
	{
		// Token: 0x060005D6 RID: 1494 RVA: 0x0000DA88 File Offset: 0x0000BC88
		public GameObject(string name)
		{
			GameObject.Internal_CreateGameObject(this, name);
		}

		// Token: 0x060005D7 RID: 1495 RVA: 0x0000DA98 File Offset: 0x0000BC98
		public GameObject()
		{
			GameObject.Internal_CreateGameObject(this, null);
		}

		// Token: 0x060005D8 RID: 1496 RVA: 0x0000DAA8 File Offset: 0x0000BCA8
		public GameObject(string name, params Type[] components)
		{
			GameObject.Internal_CreateGameObject(this, name);
			foreach (Type componentType in components)
			{
				this.AddComponent(componentType);
			}
		}

		// Token: 0x060005D9 RID: 1497
		[FreeFunction("GameObjectBindings::CreatePrimitive")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern GameObject CreatePrimitive(PrimitiveType type);

		// Token: 0x060005DA RID: 1498 RVA: 0x0000DAE8 File Offset: 0x0000BCE8
		[SecuritySafeCritical]
		public unsafe T GetComponent<T>()
		{
			CastHelper<T> castHelper = default(CastHelper<T>);
			this.GetComponentFastPath(typeof(T), new IntPtr((void*)(&castHelper.onePointerFurtherThanT)));
			return castHelper.t;
		}

		// Token: 0x060005DB RID: 1499
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
		[FreeFunction(Name = "GameObjectBindings::GetComponentFromType", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Component GetComponent(Type type);

		// Token: 0x060005DC RID: 1500
		[FreeFunction(Name = "GameObjectBindings::GetComponentFastPath", HasExplicitThis = true, ThrowsException = true)]
		[NativeWritableSelf]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetComponentFastPath(Type type, IntPtr oneFurtherThanResultValue);

		// Token: 0x060005DD RID: 1501
		[FreeFunction(Name = "Scripting::GetScriptingWrapperOfComponentOfGameObject", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Component GetComponentByName(string type);

		// Token: 0x060005DE RID: 1502 RVA: 0x0000DB28 File Offset: 0x0000BD28
		public Component GetComponent(string type)
		{
			return this.GetComponentByName(type);
		}

		// Token: 0x060005DF RID: 1503
		[FreeFunction(Name = "GameObjectBindings::GetComponentInChildren", HasExplicitThis = true, ThrowsException = true)]
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Component GetComponentInChildren(Type type, bool includeInactive);

		// Token: 0x060005E0 RID: 1504 RVA: 0x0000DB44 File Offset: 0x0000BD44
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
		public Component GetComponentInChildren(Type type)
		{
			return this.GetComponentInChildren(type, false);
		}

		// Token: 0x060005E1 RID: 1505 RVA: 0x0000DB64 File Offset: 0x0000BD64
		[ExcludeFromDocs]
		public T GetComponentInChildren<T>()
		{
			bool includeInactive = false;
			return this.GetComponentInChildren<T>(includeInactive);
		}

		// Token: 0x060005E2 RID: 1506 RVA: 0x0000DB84 File Offset: 0x0000BD84
		public T GetComponentInChildren<T>([DefaultValue("false")] bool includeInactive)
		{
			return (T)((object)this.GetComponentInChildren(typeof(T), includeInactive));
		}

		// Token: 0x060005E3 RID: 1507
		[FreeFunction(Name = "GameObjectBindings::GetComponentInParent", HasExplicitThis = true, ThrowsException = true)]
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Component GetComponentInParent(Type type);

		// Token: 0x060005E4 RID: 1508 RVA: 0x0000DBB0 File Offset: 0x0000BDB0
		public T GetComponentInParent<T>()
		{
			return (T)((object)this.GetComponentInParent(typeof(T)));
		}

		// Token: 0x060005E5 RID: 1509
		[FreeFunction(Name = "GameObjectBindings::GetComponentsInternal", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Array GetComponentsInternal(Type type, bool useSearchTypeAsArrayReturnType, bool recursive, bool includeInactive, bool reverse, object resultList);

		// Token: 0x060005E6 RID: 1510 RVA: 0x0000DBDC File Offset: 0x0000BDDC
		public Component[] GetComponents(Type type)
		{
			return (Component[])this.GetComponentsInternal(type, false, false, true, false, null);
		}

		// Token: 0x060005E7 RID: 1511 RVA: 0x0000DC04 File Offset: 0x0000BE04
		public T[] GetComponents<T>()
		{
			return (T[])this.GetComponentsInternal(typeof(T), true, false, true, false, null);
		}

		// Token: 0x060005E8 RID: 1512 RVA: 0x0000DC33 File Offset: 0x0000BE33
		public void GetComponents(Type type, List<Component> results)
		{
			this.GetComponentsInternal(type, false, false, true, false, results);
		}

		// Token: 0x060005E9 RID: 1513 RVA: 0x0000DC43 File Offset: 0x0000BE43
		public void GetComponents<T>(List<T> results)
		{
			this.GetComponentsInternal(typeof(T), false, false, true, false, results);
		}

		// Token: 0x060005EA RID: 1514 RVA: 0x0000DC5C File Offset: 0x0000BE5C
		[ExcludeFromDocs]
		public Component[] GetComponentsInChildren(Type type)
		{
			bool includeInactive = false;
			return this.GetComponentsInChildren(type, includeInactive);
		}

		// Token: 0x060005EB RID: 1515 RVA: 0x0000DC7C File Offset: 0x0000BE7C
		public Component[] GetComponentsInChildren(Type type, [DefaultValue("false")] bool includeInactive)
		{
			return (Component[])this.GetComponentsInternal(type, false, true, includeInactive, false, null);
		}

		// Token: 0x060005EC RID: 1516 RVA: 0x0000DCA4 File Offset: 0x0000BEA4
		public T[] GetComponentsInChildren<T>(bool includeInactive)
		{
			return (T[])this.GetComponentsInternal(typeof(T), true, true, includeInactive, false, null);
		}

		// Token: 0x060005ED RID: 1517 RVA: 0x0000DCD3 File Offset: 0x0000BED3
		public void GetComponentsInChildren<T>(bool includeInactive, List<T> results)
		{
			this.GetComponentsInternal(typeof(T), true, true, includeInactive, false, results);
		}

		// Token: 0x060005EE RID: 1518 RVA: 0x0000DCEC File Offset: 0x0000BEEC
		public T[] GetComponentsInChildren<T>()
		{
			return this.GetComponentsInChildren<T>(false);
		}

		// Token: 0x060005EF RID: 1519 RVA: 0x0000DD08 File Offset: 0x0000BF08
		public void GetComponentsInChildren<T>(List<T> results)
		{
			this.GetComponentsInChildren<T>(false, results);
		}

		// Token: 0x060005F0 RID: 1520 RVA: 0x0000DD14 File Offset: 0x0000BF14
		[ExcludeFromDocs]
		public Component[] GetComponentsInParent(Type type)
		{
			bool includeInactive = false;
			return this.GetComponentsInParent(type, includeInactive);
		}

		// Token: 0x060005F1 RID: 1521 RVA: 0x0000DD34 File Offset: 0x0000BF34
		public Component[] GetComponentsInParent(Type type, [DefaultValue("false")] bool includeInactive)
		{
			return (Component[])this.GetComponentsInternal(type, false, true, includeInactive, true, null);
		}

		// Token: 0x060005F2 RID: 1522 RVA: 0x0000DD5A File Offset: 0x0000BF5A
		public void GetComponentsInParent<T>(bool includeInactive, List<T> results)
		{
			this.GetComponentsInternal(typeof(T), true, true, includeInactive, true, results);
		}

		// Token: 0x060005F3 RID: 1523 RVA: 0x0000DD74 File Offset: 0x0000BF74
		public T[] GetComponentsInParent<T>(bool includeInactive)
		{
			return (T[])this.GetComponentsInternal(typeof(T), true, true, includeInactive, true, null);
		}

		// Token: 0x060005F4 RID: 1524 RVA: 0x0000DDA4 File Offset: 0x0000BFA4
		public T[] GetComponentsInParent<T>()
		{
			return this.GetComponentsInParent<T>(false);
		}

		// Token: 0x060005F5 RID: 1525 RVA: 0x0000DDC0 File Offset: 0x0000BFC0
		public static GameObject FindWithTag(string tag)
		{
			return GameObject.FindGameObjectWithTag(tag);
		}

		// Token: 0x060005F6 RID: 1526 RVA: 0x0000DDDB File Offset: 0x0000BFDB
		public void SendMessageUpwards(string methodName, SendMessageOptions options)
		{
			this.SendMessageUpwards(methodName, null, options);
		}

		// Token: 0x060005F7 RID: 1527 RVA: 0x0000DDE7 File Offset: 0x0000BFE7
		public void SendMessage(string methodName, SendMessageOptions options)
		{
			this.SendMessage(methodName, null, options);
		}

		// Token: 0x060005F8 RID: 1528 RVA: 0x0000DDF3 File Offset: 0x0000BFF3
		public void BroadcastMessage(string methodName, SendMessageOptions options)
		{
			this.BroadcastMessage(methodName, null, options);
		}

		// Token: 0x060005F9 RID: 1529
		[FreeFunction(Name = "MonoAddComponent", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Component AddComponentInternal(string className);

		// Token: 0x060005FA RID: 1530
		[FreeFunction(Name = "MonoAddComponentWithType", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Component Internal_AddComponentWithType(Type componentType);

		// Token: 0x060005FB RID: 1531 RVA: 0x0000DE00 File Offset: 0x0000C000
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
		public Component AddComponent(Type componentType)
		{
			return this.Internal_AddComponentWithType(componentType);
		}

		// Token: 0x060005FC RID: 1532 RVA: 0x0000DE1C File Offset: 0x0000C01C
		public T AddComponent<T>() where T : Component
		{
			return this.AddComponent(typeof(T)) as T;
		}

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x060005FD RID: 1533
		public extern Transform transform { [FreeFunction("GameObjectBindings::GetTransform", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x060005FE RID: 1534
		// (set) Token: 0x060005FF RID: 1535
		public extern int layer { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700010A RID: 266
		// (get) Token: 0x06000600 RID: 1536
		// (set) Token: 0x06000601 RID: 1537
		[Obsolete("GameObject.active is obsolete. Use GameObject.SetActive(), GameObject.activeSelf or GameObject.activeInHierarchy.")]
		public extern bool active { [NativeMethod(Name = "IsActive")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod(Name = "SetSelfActive")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000602 RID: 1538
		[NativeMethod(Name = "SetSelfActive")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetActive(bool value);

		// Token: 0x1700010B RID: 267
		// (get) Token: 0x06000603 RID: 1539
		public extern bool activeSelf { [NativeMethod(Name = "IsSelfActive")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700010C RID: 268
		// (get) Token: 0x06000604 RID: 1540
		public extern bool activeInHierarchy { [NativeMethod(Name = "IsActive")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000605 RID: 1541
		[NativeMethod(Name = "SetActiveRecursivelyDeprecated")]
		[Obsolete("gameObject.SetActiveRecursively() is obsolete. Use GameObject.SetActive(), which is now inherited by children.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetActiveRecursively(bool state);

		// Token: 0x1700010D RID: 269
		// (get) Token: 0x06000606 RID: 1542
		// (set) Token: 0x06000607 RID: 1543
		public extern bool isStatic { [NativeMethod(Name = "GetIsStaticDeprecated")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod(Name = "SetIsStaticDeprecated")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x06000608 RID: 1544
		internal extern bool isStaticBatchable { [NativeMethod(Name = "IsStaticBatchable")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x06000609 RID: 1545
		// (set) Token: 0x0600060A RID: 1546
		public extern string tag { [FreeFunction("GameObjectBindings::GetTag", HasExplicitThis = true, ThrowsException = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("GameObjectBindings::SetTag", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600060B RID: 1547
		[FreeFunction(Name = "GameObjectBindings::CompareTag", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool CompareTag(string tag);

		// Token: 0x0600060C RID: 1548
		[FreeFunction(Name = "GameObjectBindings::FindGameObjectWithTag", ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern GameObject FindGameObjectWithTag(string tag);

		// Token: 0x0600060D RID: 1549
		[FreeFunction(Name = "GameObjectBindings::FindGameObjectsWithTag", ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern GameObject[] FindGameObjectsWithTag(string tag);

		// Token: 0x0600060E RID: 1550
		[FreeFunction(Name = "Scripting::SendScriptingMessageUpwards", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SendMessageUpwards(string methodName, [DefaultValue("null")] object value, [DefaultValue("SendMessageOptions.RequireReceiver")] SendMessageOptions options);

		// Token: 0x0600060F RID: 1551 RVA: 0x0000DE4C File Offset: 0x0000C04C
		[ExcludeFromDocs]
		public void SendMessageUpwards(string methodName, object value)
		{
			SendMessageOptions options = SendMessageOptions.RequireReceiver;
			this.SendMessageUpwards(methodName, value, options);
		}

		// Token: 0x06000610 RID: 1552 RVA: 0x0000DE68 File Offset: 0x0000C068
		[ExcludeFromDocs]
		public void SendMessageUpwards(string methodName)
		{
			SendMessageOptions options = SendMessageOptions.RequireReceiver;
			object value = null;
			this.SendMessageUpwards(methodName, value, options);
		}

		// Token: 0x06000611 RID: 1553
		[FreeFunction(Name = "Scripting::SendScriptingMessage", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SendMessage(string methodName, [DefaultValue("null")] object value, [DefaultValue("SendMessageOptions.RequireReceiver")] SendMessageOptions options);

		// Token: 0x06000612 RID: 1554 RVA: 0x0000DE84 File Offset: 0x0000C084
		[ExcludeFromDocs]
		public void SendMessage(string methodName, object value)
		{
			SendMessageOptions options = SendMessageOptions.RequireReceiver;
			this.SendMessage(methodName, value, options);
		}

		// Token: 0x06000613 RID: 1555 RVA: 0x0000DEA0 File Offset: 0x0000C0A0
		[ExcludeFromDocs]
		public void SendMessage(string methodName)
		{
			SendMessageOptions options = SendMessageOptions.RequireReceiver;
			object value = null;
			this.SendMessage(methodName, value, options);
		}

		// Token: 0x06000614 RID: 1556
		[FreeFunction(Name = "Scripting::BroadcastScriptingMessage", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void BroadcastMessage(string methodName, [DefaultValue("null")] object parameter, [DefaultValue("SendMessageOptions.RequireReceiver")] SendMessageOptions options);

		// Token: 0x06000615 RID: 1557 RVA: 0x0000DEBC File Offset: 0x0000C0BC
		[ExcludeFromDocs]
		public void BroadcastMessage(string methodName, object parameter)
		{
			SendMessageOptions options = SendMessageOptions.RequireReceiver;
			this.BroadcastMessage(methodName, parameter, options);
		}

		// Token: 0x06000616 RID: 1558 RVA: 0x0000DED8 File Offset: 0x0000C0D8
		[ExcludeFromDocs]
		public void BroadcastMessage(string methodName)
		{
			SendMessageOptions options = SendMessageOptions.RequireReceiver;
			object parameter = null;
			this.BroadcastMessage(methodName, parameter, options);
		}

		// Token: 0x06000617 RID: 1559
		[FreeFunction(Name = "GameObjectBindings::Internal_CreateGameObject")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateGameObject([Writable] GameObject self, string name);

		// Token: 0x06000618 RID: 1560
		[FreeFunction(Name = "GameObjectBindings::Find")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern GameObject Find(string name);

		// Token: 0x17000110 RID: 272
		// (get) Token: 0x06000619 RID: 1561 RVA: 0x0000DEF4 File Offset: 0x0000C0F4
		public Scene scene
		{
			[FreeFunction("GameObjectBindings::GetScene", HasExplicitThis = true)]
			get
			{
				Scene result;
				this.get_scene_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000111 RID: 273
		// (get) Token: 0x0600061A RID: 1562 RVA: 0x0000DF0C File Offset: 0x0000C10C
		public GameObject gameObject
		{
			get
			{
				return this;
			}
		}

		// Token: 0x0600061B RID: 1563
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_scene_Injected(out Scene ret);
	}
}
