﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Android
{
	// Token: 0x02000010 RID: 16
	[NativeHeader("Runtime/Export/AndroidPermissions.bindings.h")]
	[UsedByNativeCode]
	public struct Permission
	{
		// Token: 0x06000172 RID: 370
		[StaticAccessor("PermissionsBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool HasUserAuthorizedPermission(string permission);

		// Token: 0x06000173 RID: 371
		[StaticAccessor("PermissionsBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void RequestUserPermission(string permission);

		// Token: 0x0400001C RID: 28
		public const string Camera = "android.permission.CAMERA";

		// Token: 0x0400001D RID: 29
		public const string Microphone = "android.permission.RECORD_AUDIO";

		// Token: 0x0400001E RID: 30
		public const string FineLocation = "android.permission.ACCESS_FINE_LOCATION";

		// Token: 0x0400001F RID: 31
		public const string CoarseLocation = "android.permission.ACCESS_COARSE_LOCATION";

		// Token: 0x04000020 RID: 32
		public const string ExternalStorageRead = "android.permission.READ_EXTERNAL_STORAGE";

		// Token: 0x04000021 RID: 33
		public const string ExternalStorageWrite = "android.permission.WRITE_EXTERNAL_STORAGE";
	}
}
