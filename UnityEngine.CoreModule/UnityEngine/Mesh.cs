﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000150 RID: 336
	[NativeHeader("Runtime/Graphics/Mesh/MeshScriptBindings.h")]
	[RequiredByNativeCode]
	public sealed class Mesh : Object
	{
		// Token: 0x06000E24 RID: 3620 RVA: 0x00018695 File Offset: 0x00016895
		[RequiredByNativeCode]
		public Mesh()
		{
			Mesh.Internal_Create(this);
		}

		// Token: 0x06000E25 RID: 3621
		[FreeFunction("MeshScripting::CreateMesh")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] Mesh mono);

		// Token: 0x06000E26 RID: 3622
		[FreeFunction("MeshScripting::MeshFromInstanceId")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Mesh FromInstanceID(int id);

		// Token: 0x170002BB RID: 699
		// (get) Token: 0x06000E27 RID: 3623
		// (set) Token: 0x06000E28 RID: 3624
		public extern IndexFormat indexFormat { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000E29 RID: 3625
		[FreeFunction(Name = "MeshScripting::GetIndexStart", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern uint GetIndexStartImpl(int submesh);

		// Token: 0x06000E2A RID: 3626
		[FreeFunction(Name = "MeshScripting::GetIndexCount", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern uint GetIndexCountImpl(int submesh);

		// Token: 0x06000E2B RID: 3627
		[FreeFunction(Name = "MeshScripting::GetBaseVertex", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern uint GetBaseVertexImpl(int submesh);

		// Token: 0x06000E2C RID: 3628
		[FreeFunction(Name = "MeshScripting::GetTriangles", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int[] GetTrianglesImpl(int submesh, bool applyBaseVertex);

		// Token: 0x06000E2D RID: 3629
		[FreeFunction(Name = "MeshScripting::GetIndices", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int[] GetIndicesImpl(int submesh, bool applyBaseVertex);

		// Token: 0x06000E2E RID: 3630
		[FreeFunction(Name = "SetMeshIndicesFromScript", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetIndicesImpl(int submesh, MeshTopology topology, Array indices, int arraySize, bool calculateBounds, int baseVertex);

		// Token: 0x06000E2F RID: 3631
		[FreeFunction(Name = "MeshScripting::ExtractTrianglesToArray", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetTrianglesNonAllocImpl([Out] int[] values, int submesh, bool applyBaseVertex);

		// Token: 0x06000E30 RID: 3632
		[FreeFunction(Name = "MeshScripting::ExtractIndicesToArray", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetIndicesNonAllocImpl([Out] int[] values, int submesh, bool applyBaseVertex);

		// Token: 0x06000E31 RID: 3633
		[FreeFunction(Name = "MeshScripting::PrintErrorCantAccessChannel", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void PrintErrorCantAccessChannel(VertexAttribute ch);

		// Token: 0x06000E32 RID: 3634
		[FreeFunction(Name = "MeshScripting::HasChannel", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool HasChannel(VertexAttribute ch);

		// Token: 0x06000E33 RID: 3635
		[FreeFunction(Name = "SetMeshComponentFromArrayFromScript", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetArrayForChannelImpl(VertexAttribute channel, Mesh.InternalVertexChannelType format, int dim, Array values, int arraySize);

		// Token: 0x06000E34 RID: 3636
		[FreeFunction(Name = "AllocExtractMeshComponentFromScript", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Array GetAllocArrayFromChannelImpl(VertexAttribute channel, Mesh.InternalVertexChannelType format, int dim);

		// Token: 0x06000E35 RID: 3637
		[FreeFunction(Name = "ExtractMeshComponentFromScript", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetArrayFromChannelImpl(VertexAttribute channel, Mesh.InternalVertexChannelType format, int dim, Array values);

		// Token: 0x170002BC RID: 700
		// (get) Token: 0x06000E36 RID: 3638
		public extern int vertexBufferCount { [FreeFunction(Name = "MeshScripting::GetVertexBufferCount", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000E37 RID: 3639
		[FreeFunction(Name = "MeshScripting::GetNativeVertexBufferPtr", HasExplicitThis = true)]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern IntPtr GetNativeVertexBufferPtr(int index);

		// Token: 0x06000E38 RID: 3640
		[FreeFunction(Name = "MeshScripting::GetNativeIndexBufferPtr", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern IntPtr GetNativeIndexBufferPtr();

		// Token: 0x170002BD RID: 701
		// (get) Token: 0x06000E39 RID: 3641
		public extern int blendShapeCount { [NativeMethod(Name = "GetBlendShapeChannelCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000E3A RID: 3642
		[FreeFunction(Name = "MeshScripting::ClearBlendShapes", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ClearBlendShapes();

		// Token: 0x06000E3B RID: 3643
		[FreeFunction(Name = "MeshScripting::GetBlendShapeName", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetBlendShapeName(int shapeIndex);

		// Token: 0x06000E3C RID: 3644
		[FreeFunction(Name = "MeshScripting::GetBlendShapeIndex", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetBlendShapeIndex(string blendShapeName);

		// Token: 0x06000E3D RID: 3645
		[FreeFunction(Name = "MeshScripting::GetBlendShapeFrameCount", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetBlendShapeFrameCount(int shapeIndex);

		// Token: 0x06000E3E RID: 3646
		[FreeFunction(Name = "MeshScripting::GetBlendShapeFrameWeight", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetBlendShapeFrameWeight(int shapeIndex, int frameIndex);

		// Token: 0x06000E3F RID: 3647
		[FreeFunction(Name = "GetBlendShapeFrameVerticesFromScript", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GetBlendShapeFrameVertices(int shapeIndex, int frameIndex, Vector3[] deltaVertices, Vector3[] deltaNormals, Vector3[] deltaTangents);

		// Token: 0x06000E40 RID: 3648
		[FreeFunction(Name = "AddBlendShapeFrameFromScript", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddBlendShapeFrame(string shapeName, float frameWeight, Vector3[] deltaVertices, Vector3[] deltaNormals, Vector3[] deltaTangents);

		// Token: 0x06000E41 RID: 3649
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetBoneWeightCount();

		// Token: 0x170002BE RID: 702
		// (get) Token: 0x06000E42 RID: 3650
		// (set) Token: 0x06000E43 RID: 3651
		[NativeName("BoneWeightsFromScript")]
		public extern BoneWeight[] boneWeights { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000E44 RID: 3652
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetBindposeCount();

		// Token: 0x170002BF RID: 703
		// (get) Token: 0x06000E45 RID: 3653
		// (set) Token: 0x06000E46 RID: 3654
		[NativeName("BindPosesFromScript")]
		public extern Matrix4x4[] bindposes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000E47 RID: 3655
		[FreeFunction(Name = "MeshScripting::ExtractBoneWeightsIntoArray", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetBoneWeightsNonAllocImpl([Out] BoneWeight[] values);

		// Token: 0x06000E48 RID: 3656
		[FreeFunction(Name = "MeshScripting::ExtractBindPosesIntoArray", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetBindposesNonAllocImpl([Out] Matrix4x4[] values);

		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x06000E49 RID: 3657
		public extern bool isReadable { [NativeMethod("GetIsReadable")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x06000E4A RID: 3658
		internal extern bool canAccess { [NativeMethod("CanAccessFromScript")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x06000E4B RID: 3659
		public extern int vertexCount { [NativeMethod("GetVertexCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x06000E4C RID: 3660
		// (set) Token: 0x06000E4D RID: 3661
		public extern int subMeshCount { [NativeMethod(Name = "GetSubMeshCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction(Name = "MeshScripting::SetSubMeshCount", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x06000E4E RID: 3662 RVA: 0x000186A4 File Offset: 0x000168A4
		// (set) Token: 0x06000E4F RID: 3663 RVA: 0x000186BA File Offset: 0x000168BA
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.get_bounds_Injected(out result);
				return result;
			}
			set
			{
				this.set_bounds_Injected(ref value);
			}
		}

		// Token: 0x06000E50 RID: 3664
		[NativeMethod("Clear")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ClearImpl(bool keepVertexLayout);

		// Token: 0x06000E51 RID: 3665
		[NativeMethod("RecalculateBounds")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RecalculateBoundsImpl();

		// Token: 0x06000E52 RID: 3666
		[NativeMethod("RecalculateNormals")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RecalculateNormalsImpl();

		// Token: 0x06000E53 RID: 3667
		[NativeMethod("RecalculateTangents")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RecalculateTangentsImpl();

		// Token: 0x06000E54 RID: 3668
		[NativeMethod("MarkDynamic")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void MarkDynamicImpl();

		// Token: 0x06000E55 RID: 3669
		[NativeMethod("UploadMeshData")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void UploadMeshDataImpl(bool markNoLongerReadable);

		// Token: 0x06000E56 RID: 3670
		[FreeFunction(Name = "MeshScripting::GetPrimitiveType", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern MeshTopology GetTopologyImpl(int submesh);

		// Token: 0x06000E57 RID: 3671
		[NativeMethod("GetMeshMetric")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetUVDistributionMetric(int uvSetIndex);

		// Token: 0x06000E58 RID: 3672
		[FreeFunction(Name = "MeshScripting::CombineMeshes", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void CombineMeshesImpl(CombineInstance[] combine, bool mergeSubMeshes, bool useMatrices, bool hasLightmapData);

		// Token: 0x06000E59 RID: 3673 RVA: 0x000186C4 File Offset: 0x000168C4
		internal VertexAttribute GetUVChannel(int uvIndex)
		{
			if (uvIndex < 0 || uvIndex > 7)
			{
				throw new ArgumentException("GetUVChannel called for bad uvIndex", "uvIndex");
			}
			return VertexAttribute.TexCoord0 + uvIndex;
		}

		// Token: 0x06000E5A RID: 3674 RVA: 0x000186FC File Offset: 0x000168FC
		internal static int DefaultDimensionForChannel(VertexAttribute channel)
		{
			int result;
			if (channel == VertexAttribute.Position || channel == VertexAttribute.Normal)
			{
				result = 3;
			}
			else if (channel >= VertexAttribute.TexCoord0 && channel <= VertexAttribute.TexCoord7)
			{
				result = 2;
			}
			else
			{
				if (channel != VertexAttribute.Tangent && channel != VertexAttribute.Color)
				{
					throw new ArgumentException("DefaultDimensionForChannel called for bad channel", "channel");
				}
				result = 4;
			}
			return result;
		}

		// Token: 0x06000E5B RID: 3675 RVA: 0x0001875C File Offset: 0x0001695C
		private T[] GetAllocArrayFromChannel<T>(VertexAttribute channel, Mesh.InternalVertexChannelType format, int dim)
		{
			if (this.canAccess)
			{
				if (this.HasChannel(channel))
				{
					return (T[])this.GetAllocArrayFromChannelImpl(channel, format, dim);
				}
			}
			else
			{
				this.PrintErrorCantAccessChannel(channel);
			}
			return new T[0];
		}

		// Token: 0x06000E5C RID: 3676 RVA: 0x000187B4 File Offset: 0x000169B4
		private T[] GetAllocArrayFromChannel<T>(VertexAttribute channel)
		{
			return this.GetAllocArrayFromChannel<T>(channel, Mesh.InternalVertexChannelType.Float, Mesh.DefaultDimensionForChannel(channel));
		}

		// Token: 0x06000E5D RID: 3677 RVA: 0x000187D7 File Offset: 0x000169D7
		private void SetSizedArrayForChannel(VertexAttribute channel, Mesh.InternalVertexChannelType format, int dim, Array values, int valuesCount)
		{
			if (this.canAccess)
			{
				this.SetArrayForChannelImpl(channel, format, dim, values, valuesCount);
			}
			else
			{
				this.PrintErrorCantAccessChannel(channel);
			}
		}

		// Token: 0x06000E5E RID: 3678 RVA: 0x000187FE File Offset: 0x000169FE
		private void SetArrayForChannel<T>(VertexAttribute channel, Mesh.InternalVertexChannelType format, int dim, T[] values)
		{
			this.SetSizedArrayForChannel(channel, format, dim, values, NoAllocHelpers.SafeLength(values));
		}

		// Token: 0x06000E5F RID: 3679 RVA: 0x00018813 File Offset: 0x00016A13
		private void SetArrayForChannel<T>(VertexAttribute channel, T[] values)
		{
			this.SetSizedArrayForChannel(channel, Mesh.InternalVertexChannelType.Float, Mesh.DefaultDimensionForChannel(channel), values, NoAllocHelpers.SafeLength(values));
		}

		// Token: 0x06000E60 RID: 3680 RVA: 0x0001882B File Offset: 0x00016A2B
		private void SetListForChannel<T>(VertexAttribute channel, Mesh.InternalVertexChannelType format, int dim, List<T> values)
		{
			this.SetSizedArrayForChannel(channel, format, dim, NoAllocHelpers.ExtractArrayFromList(values), NoAllocHelpers.SafeLength<T>(values));
		}

		// Token: 0x06000E61 RID: 3681 RVA: 0x00018845 File Offset: 0x00016A45
		private void SetListForChannel<T>(VertexAttribute channel, List<T> values)
		{
			this.SetSizedArrayForChannel(channel, Mesh.InternalVertexChannelType.Float, Mesh.DefaultDimensionForChannel(channel), NoAllocHelpers.ExtractArrayFromList(values), NoAllocHelpers.SafeLength<T>(values));
		}

		// Token: 0x06000E62 RID: 3682 RVA: 0x00018862 File Offset: 0x00016A62
		private void GetListForChannel<T>(List<T> buffer, int capacity, VertexAttribute channel, int dim)
		{
			this.GetListForChannel<T>(buffer, capacity, channel, dim, Mesh.InternalVertexChannelType.Float);
		}

		// Token: 0x06000E63 RID: 3683 RVA: 0x00018874 File Offset: 0x00016A74
		private void GetListForChannel<T>(List<T> buffer, int capacity, VertexAttribute channel, int dim, Mesh.InternalVertexChannelType channelType)
		{
			buffer.Clear();
			if (!this.canAccess)
			{
				this.PrintErrorCantAccessChannel(channel);
			}
			else if (this.HasChannel(channel))
			{
				NoAllocHelpers.EnsureListElemCount<T>(buffer, capacity);
				this.GetArrayFromChannelImpl(channel, channelType, dim, NoAllocHelpers.ExtractArrayFromList(buffer));
			}
		}

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x06000E64 RID: 3684 RVA: 0x000188CC File Offset: 0x00016ACC
		// (set) Token: 0x06000E65 RID: 3685 RVA: 0x000188E8 File Offset: 0x00016AE8
		public Vector3[] vertices
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector3>(VertexAttribute.Position);
			}
			set
			{
				this.SetArrayForChannel<Vector3>(VertexAttribute.Position, value);
			}
		}

		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x06000E66 RID: 3686 RVA: 0x000188F4 File Offset: 0x00016AF4
		// (set) Token: 0x06000E67 RID: 3687 RVA: 0x00018910 File Offset: 0x00016B10
		public Vector3[] normals
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector3>(VertexAttribute.Normal);
			}
			set
			{
				this.SetArrayForChannel<Vector3>(VertexAttribute.Normal, value);
			}
		}

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06000E68 RID: 3688 RVA: 0x0001891C File Offset: 0x00016B1C
		// (set) Token: 0x06000E69 RID: 3689 RVA: 0x00018938 File Offset: 0x00016B38
		public Vector4[] tangents
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector4>(VertexAttribute.Tangent);
			}
			set
			{
				this.SetArrayForChannel<Vector4>(VertexAttribute.Tangent, value);
			}
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06000E6A RID: 3690 RVA: 0x00018944 File Offset: 0x00016B44
		// (set) Token: 0x06000E6B RID: 3691 RVA: 0x00018960 File Offset: 0x00016B60
		public Vector2[] uv
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(VertexAttribute.TexCoord0);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(VertexAttribute.TexCoord0, value);
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x06000E6C RID: 3692 RVA: 0x0001896C File Offset: 0x00016B6C
		// (set) Token: 0x06000E6D RID: 3693 RVA: 0x00018988 File Offset: 0x00016B88
		public Vector2[] uv2
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(VertexAttribute.TexCoord1);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(VertexAttribute.TexCoord1, value);
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06000E6E RID: 3694 RVA: 0x00018994 File Offset: 0x00016B94
		// (set) Token: 0x06000E6F RID: 3695 RVA: 0x000189B0 File Offset: 0x00016BB0
		public Vector2[] uv3
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(VertexAttribute.TexCoord2);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(VertexAttribute.TexCoord2, value);
			}
		}

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x06000E70 RID: 3696 RVA: 0x000189BC File Offset: 0x00016BBC
		// (set) Token: 0x06000E71 RID: 3697 RVA: 0x000189D8 File Offset: 0x00016BD8
		public Vector2[] uv4
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(VertexAttribute.TexCoord3);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(VertexAttribute.TexCoord3, value);
			}
		}

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x06000E72 RID: 3698 RVA: 0x000189E4 File Offset: 0x00016BE4
		// (set) Token: 0x06000E73 RID: 3699 RVA: 0x00018A00 File Offset: 0x00016C00
		public Vector2[] uv5
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(VertexAttribute.TexCoord4);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(VertexAttribute.TexCoord4, value);
			}
		}

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x06000E74 RID: 3700 RVA: 0x00018A0C File Offset: 0x00016C0C
		// (set) Token: 0x06000E75 RID: 3701 RVA: 0x00018A29 File Offset: 0x00016C29
		public Vector2[] uv6
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(VertexAttribute.TexCoord5);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(VertexAttribute.TexCoord5, value);
			}
		}

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x06000E76 RID: 3702 RVA: 0x00018A38 File Offset: 0x00016C38
		// (set) Token: 0x06000E77 RID: 3703 RVA: 0x00018A55 File Offset: 0x00016C55
		public Vector2[] uv7
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(VertexAttribute.TexCoord6);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(VertexAttribute.TexCoord6, value);
			}
		}

		// Token: 0x170002CF RID: 719
		// (get) Token: 0x06000E78 RID: 3704 RVA: 0x00018A64 File Offset: 0x00016C64
		// (set) Token: 0x06000E79 RID: 3705 RVA: 0x00018A81 File Offset: 0x00016C81
		public Vector2[] uv8
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(VertexAttribute.TexCoord7);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(VertexAttribute.TexCoord7, value);
			}
		}

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06000E7A RID: 3706 RVA: 0x00018A90 File Offset: 0x00016C90
		// (set) Token: 0x06000E7B RID: 3707 RVA: 0x00018AAC File Offset: 0x00016CAC
		public Color[] colors
		{
			get
			{
				return this.GetAllocArrayFromChannel<Color>(VertexAttribute.Color);
			}
			set
			{
				this.SetArrayForChannel<Color>(VertexAttribute.Color, value);
			}
		}

		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06000E7C RID: 3708 RVA: 0x00018AB8 File Offset: 0x00016CB8
		// (set) Token: 0x06000E7D RID: 3709 RVA: 0x00018AD6 File Offset: 0x00016CD6
		public Color32[] colors32
		{
			get
			{
				return this.GetAllocArrayFromChannel<Color32>(VertexAttribute.Color, Mesh.InternalVertexChannelType.Color, 1);
			}
			set
			{
				this.SetArrayForChannel<Color32>(VertexAttribute.Color, Mesh.InternalVertexChannelType.Color, 1, value);
			}
		}

		// Token: 0x06000E7E RID: 3710 RVA: 0x00018AE3 File Offset: 0x00016CE3
		public void GetVertices(List<Vector3> vertices)
		{
			if (vertices == null)
			{
				throw new ArgumentNullException("The result vertices list cannot be null.", "vertices");
			}
			this.GetListForChannel<Vector3>(vertices, this.vertexCount, VertexAttribute.Position, Mesh.DefaultDimensionForChannel(VertexAttribute.Position));
		}

		// Token: 0x06000E7F RID: 3711 RVA: 0x00018B10 File Offset: 0x00016D10
		public void SetVertices(List<Vector3> inVertices)
		{
			this.SetListForChannel<Vector3>(VertexAttribute.Position, inVertices);
		}

		// Token: 0x06000E80 RID: 3712 RVA: 0x00018B1B File Offset: 0x00016D1B
		public void GetNormals(List<Vector3> normals)
		{
			if (normals == null)
			{
				throw new ArgumentNullException("The result normals list cannot be null.", "normals");
			}
			this.GetListForChannel<Vector3>(normals, this.vertexCount, VertexAttribute.Normal, Mesh.DefaultDimensionForChannel(VertexAttribute.Normal));
		}

		// Token: 0x06000E81 RID: 3713 RVA: 0x00018B48 File Offset: 0x00016D48
		public void SetNormals(List<Vector3> inNormals)
		{
			this.SetListForChannel<Vector3>(VertexAttribute.Normal, inNormals);
		}

		// Token: 0x06000E82 RID: 3714 RVA: 0x00018B53 File Offset: 0x00016D53
		public void GetTangents(List<Vector4> tangents)
		{
			if (tangents == null)
			{
				throw new ArgumentNullException("The result tangents list cannot be null.", "tangents");
			}
			this.GetListForChannel<Vector4>(tangents, this.vertexCount, VertexAttribute.Tangent, Mesh.DefaultDimensionForChannel(VertexAttribute.Tangent));
		}

		// Token: 0x06000E83 RID: 3715 RVA: 0x00018B80 File Offset: 0x00016D80
		public void SetTangents(List<Vector4> inTangents)
		{
			this.SetListForChannel<Vector4>(VertexAttribute.Tangent, inTangents);
		}

		// Token: 0x06000E84 RID: 3716 RVA: 0x00018B8B File Offset: 0x00016D8B
		public void GetColors(List<Color> colors)
		{
			if (colors == null)
			{
				throw new ArgumentNullException("The result colors list cannot be null.", "colors");
			}
			this.GetListForChannel<Color>(colors, this.vertexCount, VertexAttribute.Color, Mesh.DefaultDimensionForChannel(VertexAttribute.Color));
		}

		// Token: 0x06000E85 RID: 3717 RVA: 0x00018BB8 File Offset: 0x00016DB8
		public void SetColors(List<Color> inColors)
		{
			this.SetListForChannel<Color>(VertexAttribute.Color, inColors);
		}

		// Token: 0x06000E86 RID: 3718 RVA: 0x00018BC3 File Offset: 0x00016DC3
		public void GetColors(List<Color32> colors)
		{
			if (colors == null)
			{
				throw new ArgumentNullException("The result colors list cannot be null.", "colors");
			}
			this.GetListForChannel<Color32>(colors, this.vertexCount, VertexAttribute.Color, 1, Mesh.InternalVertexChannelType.Color);
		}

		// Token: 0x06000E87 RID: 3719 RVA: 0x00018BEC File Offset: 0x00016DEC
		public void SetColors(List<Color32> inColors)
		{
			this.SetListForChannel<Color32>(VertexAttribute.Color, Mesh.InternalVertexChannelType.Color, 1, inColors);
		}

		// Token: 0x06000E88 RID: 3720 RVA: 0x00018BF9 File Offset: 0x00016DF9
		private void SetUvsImpl<T>(int uvIndex, int dim, List<T> uvs)
		{
			if (uvIndex < 0 || uvIndex > 7)
			{
				Debug.LogError("The uv index is invalid. Must be in the range 0 to 7.");
			}
			else
			{
				this.SetListForChannel<T>(this.GetUVChannel(uvIndex), Mesh.InternalVertexChannelType.Float, dim, uvs);
			}
		}

		// Token: 0x06000E89 RID: 3721 RVA: 0x00018C2A File Offset: 0x00016E2A
		public void SetUVs(int channel, List<Vector2> uvs)
		{
			this.SetUvsImpl<Vector2>(channel, 2, uvs);
		}

		// Token: 0x06000E8A RID: 3722 RVA: 0x00018C36 File Offset: 0x00016E36
		public void SetUVs(int channel, List<Vector3> uvs)
		{
			this.SetUvsImpl<Vector3>(channel, 3, uvs);
		}

		// Token: 0x06000E8B RID: 3723 RVA: 0x00018C42 File Offset: 0x00016E42
		public void SetUVs(int channel, List<Vector4> uvs)
		{
			this.SetUvsImpl<Vector4>(channel, 4, uvs);
		}

		// Token: 0x06000E8C RID: 3724 RVA: 0x00018C50 File Offset: 0x00016E50
		private void GetUVsImpl<T>(int uvIndex, List<T> uvs, int dim)
		{
			if (uvs == null)
			{
				throw new ArgumentNullException("The result uvs list cannot be null.", "uvs");
			}
			if (uvIndex < 0 || uvIndex > 7)
			{
				throw new IndexOutOfRangeException("The uv index is invalid. Must be in the range 0 to 7.");
			}
			this.GetListForChannel<T>(uvs, this.vertexCount, this.GetUVChannel(uvIndex), dim);
		}

		// Token: 0x06000E8D RID: 3725 RVA: 0x00018CA2 File Offset: 0x00016EA2
		public void GetUVs(int channel, List<Vector2> uvs)
		{
			this.GetUVsImpl<Vector2>(channel, uvs, 2);
		}

		// Token: 0x06000E8E RID: 3726 RVA: 0x00018CAE File Offset: 0x00016EAE
		public void GetUVs(int channel, List<Vector3> uvs)
		{
			this.GetUVsImpl<Vector3>(channel, uvs, 3);
		}

		// Token: 0x06000E8F RID: 3727 RVA: 0x00018CBA File Offset: 0x00016EBA
		public void GetUVs(int channel, List<Vector4> uvs)
		{
			this.GetUVsImpl<Vector4>(channel, uvs, 4);
		}

		// Token: 0x06000E90 RID: 3728 RVA: 0x00018CC6 File Offset: 0x00016EC6
		private void PrintErrorCantAccessIndices()
		{
			Debug.LogError(string.Format("Not allowed to access triangles/indices on mesh '{0}' (isReadable is false; Read/Write must be enabled in import settings)", base.name));
		}

		// Token: 0x06000E91 RID: 3729 RVA: 0x00018CE0 File Offset: 0x00016EE0
		private bool CheckCanAccessSubmesh(int submesh, bool errorAboutTriangles)
		{
			bool result;
			if (!this.canAccess)
			{
				this.PrintErrorCantAccessIndices();
				result = false;
			}
			else if (submesh < 0 || submesh >= this.subMeshCount)
			{
				Debug.LogError(string.Format("Failed getting {0}. Submesh index is out of bounds.", (!errorAboutTriangles) ? "indices" : "triangles"), this);
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06000E92 RID: 3730 RVA: 0x00018D50 File Offset: 0x00016F50
		private bool CheckCanAccessSubmeshTriangles(int submesh)
		{
			return this.CheckCanAccessSubmesh(submesh, true);
		}

		// Token: 0x06000E93 RID: 3731 RVA: 0x00018D70 File Offset: 0x00016F70
		private bool CheckCanAccessSubmeshIndices(int submesh)
		{
			return this.CheckCanAccessSubmesh(submesh, false);
		}

		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x06000E94 RID: 3732 RVA: 0x00018D90 File Offset: 0x00016F90
		// (set) Token: 0x06000E95 RID: 3733 RVA: 0x00018DCA File Offset: 0x00016FCA
		public int[] triangles
		{
			get
			{
				int[] result;
				if (this.canAccess)
				{
					result = this.GetTrianglesImpl(-1, true);
				}
				else
				{
					this.PrintErrorCantAccessIndices();
					result = new int[0];
				}
				return result;
			}
			set
			{
				if (this.canAccess)
				{
					this.SetTrianglesImpl(-1, value, NoAllocHelpers.SafeLength(value), true, 0);
				}
				else
				{
					this.PrintErrorCantAccessIndices();
				}
			}
		}

		// Token: 0x06000E96 RID: 3734 RVA: 0x00018DF4 File Offset: 0x00016FF4
		public int[] GetTriangles(int submesh)
		{
			return this.GetTriangles(submesh, true);
		}

		// Token: 0x06000E97 RID: 3735 RVA: 0x00018E14 File Offset: 0x00017014
		public int[] GetTriangles(int submesh, [UnityEngine.Internal.DefaultValue("true")] bool applyBaseVertex)
		{
			return (!this.CheckCanAccessSubmeshTriangles(submesh)) ? new int[0] : this.GetTrianglesImpl(submesh, applyBaseVertex);
		}

		// Token: 0x06000E98 RID: 3736 RVA: 0x00018E48 File Offset: 0x00017048
		public void GetTriangles(List<int> triangles, int submesh)
		{
			this.GetTriangles(triangles, submesh, true);
		}

		// Token: 0x06000E99 RID: 3737 RVA: 0x00018E54 File Offset: 0x00017054
		public void GetTriangles(List<int> triangles, int submesh, [UnityEngine.Internal.DefaultValue("true")] bool applyBaseVertex)
		{
			if (triangles == null)
			{
				throw new ArgumentNullException("The result triangles list cannot be null.", "triangles");
			}
			if (submesh < 0 || submesh >= this.subMeshCount)
			{
				throw new IndexOutOfRangeException("Specified sub mesh is out of range. Must be greater or equal to 0 and less than subMeshCount.");
			}
			NoAllocHelpers.EnsureListElemCount<int>(triangles, (int)this.GetIndexCount(submesh));
			this.GetTrianglesNonAllocImpl(NoAllocHelpers.ExtractArrayFromListT<int>(triangles), submesh, applyBaseVertex);
		}

		// Token: 0x06000E9A RID: 3738 RVA: 0x00018EB4 File Offset: 0x000170B4
		public int[] GetIndices(int submesh)
		{
			return this.GetIndices(submesh, true);
		}

		// Token: 0x06000E9B RID: 3739 RVA: 0x00018ED4 File Offset: 0x000170D4
		public int[] GetIndices(int submesh, [UnityEngine.Internal.DefaultValue("true")] bool applyBaseVertex)
		{
			return (!this.CheckCanAccessSubmeshIndices(submesh)) ? new int[0] : this.GetIndicesImpl(submesh, applyBaseVertex);
		}

		// Token: 0x06000E9C RID: 3740 RVA: 0x00018F08 File Offset: 0x00017108
		public void GetIndices(List<int> indices, int submesh)
		{
			this.GetIndices(indices, submesh, true);
		}

		// Token: 0x06000E9D RID: 3741 RVA: 0x00018F14 File Offset: 0x00017114
		public void GetIndices(List<int> indices, int submesh, [UnityEngine.Internal.DefaultValue("true")] bool applyBaseVertex)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("The result indices list cannot be null.", "indices");
			}
			if (submesh < 0 || submesh >= this.subMeshCount)
			{
				throw new IndexOutOfRangeException("Specified sub mesh is out of range. Must be greater or equal to 0 and less than subMeshCount.");
			}
			NoAllocHelpers.EnsureListElemCount<int>(indices, (int)this.GetIndexCount(submesh));
			this.GetIndicesNonAllocImpl(NoAllocHelpers.ExtractArrayFromListT<int>(indices), submesh, applyBaseVertex);
		}

		// Token: 0x06000E9E RID: 3742 RVA: 0x00018F74 File Offset: 0x00017174
		public uint GetIndexStart(int submesh)
		{
			if (submesh < 0 || submesh >= this.subMeshCount)
			{
				throw new IndexOutOfRangeException("Specified sub mesh is out of range. Must be greater or equal to 0 and less than subMeshCount.");
			}
			return this.GetIndexStartImpl(submesh);
		}

		// Token: 0x06000E9F RID: 3743 RVA: 0x00018FB0 File Offset: 0x000171B0
		public uint GetIndexCount(int submesh)
		{
			if (submesh < 0 || submesh >= this.subMeshCount)
			{
				throw new IndexOutOfRangeException("Specified sub mesh is out of range. Must be greater or equal to 0 and less than subMeshCount.");
			}
			return this.GetIndexCountImpl(submesh);
		}

		// Token: 0x06000EA0 RID: 3744 RVA: 0x00018FEC File Offset: 0x000171EC
		public uint GetBaseVertex(int submesh)
		{
			if (submesh < 0 || submesh >= this.subMeshCount)
			{
				throw new IndexOutOfRangeException("Specified sub mesh is out of range. Must be greater or equal to 0 and less than subMeshCount.");
			}
			return this.GetBaseVertexImpl(submesh);
		}

		// Token: 0x06000EA1 RID: 3745 RVA: 0x00019026 File Offset: 0x00017226
		private void SetTrianglesImpl(int submesh, Array triangles, int arraySize, bool calculateBounds, int baseVertex)
		{
			this.SetIndicesImpl(submesh, MeshTopology.Triangles, triangles, arraySize, calculateBounds, baseVertex);
		}

		// Token: 0x06000EA2 RID: 3746 RVA: 0x00019037 File Offset: 0x00017237
		public void SetTriangles(int[] triangles, int submesh)
		{
			this.SetTriangles(triangles, submesh, true, 0);
		}

		// Token: 0x06000EA3 RID: 3747 RVA: 0x00019044 File Offset: 0x00017244
		public void SetTriangles(int[] triangles, int submesh, bool calculateBounds)
		{
			this.SetTriangles(triangles, submesh, calculateBounds, 0);
		}

		// Token: 0x06000EA4 RID: 3748 RVA: 0x00019051 File Offset: 0x00017251
		public void SetTriangles(int[] triangles, int submesh, [UnityEngine.Internal.DefaultValue("true")] bool calculateBounds, [UnityEngine.Internal.DefaultValue("0")] int baseVertex)
		{
			if (this.CheckCanAccessSubmeshTriangles(submesh))
			{
				this.SetTrianglesImpl(submesh, triangles, NoAllocHelpers.SafeLength(triangles), calculateBounds, baseVertex);
			}
		}

		// Token: 0x06000EA5 RID: 3749 RVA: 0x00019071 File Offset: 0x00017271
		public void SetTriangles(List<int> triangles, int submesh)
		{
			this.SetTriangles(triangles, submesh, true, 0);
		}

		// Token: 0x06000EA6 RID: 3750 RVA: 0x0001907E File Offset: 0x0001727E
		public void SetTriangles(List<int> triangles, int submesh, bool calculateBounds)
		{
			this.SetTriangles(triangles, submesh, calculateBounds, 0);
		}

		// Token: 0x06000EA7 RID: 3751 RVA: 0x0001908B File Offset: 0x0001728B
		public void SetTriangles(List<int> triangles, int submesh, [UnityEngine.Internal.DefaultValue("true")] bool calculateBounds, [UnityEngine.Internal.DefaultValue("0")] int baseVertex)
		{
			if (this.CheckCanAccessSubmeshTriangles(submesh))
			{
				this.SetTrianglesImpl(submesh, NoAllocHelpers.ExtractArrayFromList(triangles), NoAllocHelpers.SafeLength<int>(triangles), calculateBounds, baseVertex);
			}
		}

		// Token: 0x06000EA8 RID: 3752 RVA: 0x000190B0 File Offset: 0x000172B0
		public void SetIndices(int[] indices, MeshTopology topology, int submesh)
		{
			this.SetIndices(indices, topology, submesh, true, 0);
		}

		// Token: 0x06000EA9 RID: 3753 RVA: 0x000190BE File Offset: 0x000172BE
		public void SetIndices(int[] indices, MeshTopology topology, int submesh, bool calculateBounds)
		{
			this.SetIndices(indices, topology, submesh, calculateBounds, 0);
		}

		// Token: 0x06000EAA RID: 3754 RVA: 0x000190CD File Offset: 0x000172CD
		public void SetIndices(int[] indices, MeshTopology topology, int submesh, [UnityEngine.Internal.DefaultValue("true")] bool calculateBounds, [UnityEngine.Internal.DefaultValue("0")] int baseVertex)
		{
			if (this.CheckCanAccessSubmeshIndices(submesh))
			{
				this.SetIndicesImpl(submesh, topology, indices, NoAllocHelpers.SafeLength(indices), calculateBounds, baseVertex);
			}
		}

		// Token: 0x06000EAB RID: 3755 RVA: 0x000190EF File Offset: 0x000172EF
		public void GetBindposes(List<Matrix4x4> bindposes)
		{
			if (bindposes == null)
			{
				throw new ArgumentNullException("The result bindposes list cannot be null.", "bindposes");
			}
			NoAllocHelpers.EnsureListElemCount<Matrix4x4>(bindposes, this.GetBindposeCount());
			this.GetBindposesNonAllocImpl(NoAllocHelpers.ExtractArrayFromListT<Matrix4x4>(bindposes));
		}

		// Token: 0x06000EAC RID: 3756 RVA: 0x00019120 File Offset: 0x00017320
		public void GetBoneWeights(List<BoneWeight> boneWeights)
		{
			if (boneWeights == null)
			{
				throw new ArgumentNullException("The result boneWeights list cannot be null.", "boneWeights");
			}
			NoAllocHelpers.EnsureListElemCount<BoneWeight>(boneWeights, this.GetBoneWeightCount());
			this.GetBoneWeightsNonAllocImpl(NoAllocHelpers.ExtractArrayFromListT<BoneWeight>(boneWeights));
		}

		// Token: 0x06000EAD RID: 3757 RVA: 0x00019151 File Offset: 0x00017351
		public void Clear(bool keepVertexLayout)
		{
			this.ClearImpl(keepVertexLayout);
		}

		// Token: 0x06000EAE RID: 3758 RVA: 0x0001915B File Offset: 0x0001735B
		public void Clear()
		{
			this.ClearImpl(true);
		}

		// Token: 0x06000EAF RID: 3759 RVA: 0x00019165 File Offset: 0x00017365
		public void RecalculateBounds()
		{
			if (this.canAccess)
			{
				this.RecalculateBoundsImpl();
			}
			else
			{
				Debug.LogError(string.Format("Not allowed to call RecalculateBounds() on mesh '{0}'", base.name));
			}
		}

		// Token: 0x06000EB0 RID: 3760 RVA: 0x00019193 File Offset: 0x00017393
		public void RecalculateNormals()
		{
			if (this.canAccess)
			{
				this.RecalculateNormalsImpl();
			}
			else
			{
				Debug.LogError(string.Format("Not allowed to call RecalculateNormals() on mesh '{0}'", base.name));
			}
		}

		// Token: 0x06000EB1 RID: 3761 RVA: 0x000191C1 File Offset: 0x000173C1
		public void RecalculateTangents()
		{
			if (this.canAccess)
			{
				this.RecalculateTangentsImpl();
			}
			else
			{
				Debug.LogError(string.Format("Not allowed to call RecalculateTangents() on mesh '{0}'", base.name));
			}
		}

		// Token: 0x06000EB2 RID: 3762 RVA: 0x000191EF File Offset: 0x000173EF
		public void MarkDynamic()
		{
			if (this.canAccess)
			{
				this.MarkDynamicImpl();
			}
		}

		// Token: 0x06000EB3 RID: 3763 RVA: 0x00019203 File Offset: 0x00017403
		public void UploadMeshData(bool markNoLongerReadable)
		{
			if (this.canAccess)
			{
				this.UploadMeshDataImpl(markNoLongerReadable);
			}
		}

		// Token: 0x06000EB4 RID: 3764 RVA: 0x00019218 File Offset: 0x00017418
		public MeshTopology GetTopology(int submesh)
		{
			MeshTopology result;
			if (submesh < 0 || submesh >= this.subMeshCount)
			{
				Debug.LogError(string.Format("Failed getting topology. Submesh index is out of bounds.", new object[0]), this);
				result = MeshTopology.Triangles;
			}
			else
			{
				result = this.GetTopologyImpl(submesh);
			}
			return result;
		}

		// Token: 0x06000EB5 RID: 3765 RVA: 0x00019265 File Offset: 0x00017465
		public void CombineMeshes(CombineInstance[] combine, [UnityEngine.Internal.DefaultValue("true")] bool mergeSubMeshes, [UnityEngine.Internal.DefaultValue("true")] bool useMatrices, [UnityEngine.Internal.DefaultValue("false")] bool hasLightmapData)
		{
			this.CombineMeshesImpl(combine, mergeSubMeshes, useMatrices, hasLightmapData);
		}

		// Token: 0x06000EB6 RID: 3766 RVA: 0x00019273 File Offset: 0x00017473
		public void CombineMeshes(CombineInstance[] combine, bool mergeSubMeshes, bool useMatrices)
		{
			this.CombineMeshesImpl(combine, mergeSubMeshes, useMatrices, false);
		}

		// Token: 0x06000EB7 RID: 3767 RVA: 0x00019280 File Offset: 0x00017480
		public void CombineMeshes(CombineInstance[] combine, bool mergeSubMeshes)
		{
			this.CombineMeshesImpl(combine, mergeSubMeshes, true, false);
		}

		// Token: 0x06000EB8 RID: 3768 RVA: 0x0001928D File Offset: 0x0001748D
		public void CombineMeshes(CombineInstance[] combine)
		{
			this.CombineMeshesImpl(combine, true, true, false);
		}

		// Token: 0x06000EB9 RID: 3769 RVA: 0x00007476 File Offset: 0x00005676
		[Obsolete("This method is no longer supported (UnityUpgradable)", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void Optimize()
		{
		}

		// Token: 0x06000EBA RID: 3770
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bounds_Injected(out Bounds ret);

		// Token: 0x06000EBB RID: 3771
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_bounds_Injected(ref Bounds value);

		// Token: 0x02000151 RID: 337
		internal enum InternalVertexChannelType
		{
			// Token: 0x040006F0 RID: 1776
			Float,
			// Token: 0x040006F1 RID: 1777
			Color = 2
		}
	}
}
