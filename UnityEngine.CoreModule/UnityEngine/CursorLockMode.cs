﻿using System;

namespace UnityEngine
{
	// Token: 0x02000063 RID: 99
	public enum CursorLockMode
	{
		// Token: 0x04000130 RID: 304
		None,
		// Token: 0x04000131 RID: 305
		Locked,
		// Token: 0x04000132 RID: 306
		Confined
	}
}
