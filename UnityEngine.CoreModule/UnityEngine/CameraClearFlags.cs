﻿using System;

namespace UnityEngine
{
	// Token: 0x020000BF RID: 191
	public enum CameraClearFlags
	{
		// Token: 0x040001FE RID: 510
		Skybox = 1,
		// Token: 0x040001FF RID: 511
		Color,
		// Token: 0x04000200 RID: 512
		SolidColor = 2,
		// Token: 0x04000201 RID: 513
		Depth,
		// Token: 0x04000202 RID: 514
		Nothing
	}
}
