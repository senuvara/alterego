﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C6 RID: 198
	public enum ColorSpace
	{
		// Token: 0x04000223 RID: 547
		Uninitialized = -1,
		// Token: 0x04000224 RID: 548
		Gamma,
		// Token: 0x04000225 RID: 549
		Linear
	}
}
