﻿using System;

namespace UnityEngine
{
	// Token: 0x0200005D RID: 93
	public struct BoundingSphere
	{
		// Token: 0x060004FF RID: 1279 RVA: 0x0000CBB8 File Offset: 0x0000ADB8
		public BoundingSphere(Vector3 pos, float rad)
		{
			this.position = pos;
			this.radius = rad;
		}

		// Token: 0x06000500 RID: 1280 RVA: 0x0000CBC9 File Offset: 0x0000ADC9
		public BoundingSphere(Vector4 packedSphere)
		{
			this.position = new Vector3(packedSphere.x, packedSphere.y, packedSphere.z);
			this.radius = packedSphere.w;
		}

		// Token: 0x0400011F RID: 287
		public Vector3 position;

		// Token: 0x04000120 RID: 288
		public float radius;
	}
}
