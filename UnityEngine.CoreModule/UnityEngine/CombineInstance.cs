﻿using System;

namespace UnityEngine
{
	// Token: 0x02000154 RID: 340
	public struct CombineInstance
	{
		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06000ED3 RID: 3795 RVA: 0x00019674 File Offset: 0x00017874
		// (set) Token: 0x06000ED4 RID: 3796 RVA: 0x00019694 File Offset: 0x00017894
		public Mesh mesh
		{
			get
			{
				return Mesh.FromInstanceID(this.m_MeshInstanceID);
			}
			set
			{
				this.m_MeshInstanceID = ((!(value != null)) ? 0 : value.GetInstanceID());
			}
		}

		// Token: 0x170002DC RID: 732
		// (get) Token: 0x06000ED5 RID: 3797 RVA: 0x000196B8 File Offset: 0x000178B8
		// (set) Token: 0x06000ED6 RID: 3798 RVA: 0x000196D3 File Offset: 0x000178D3
		public int subMeshIndex
		{
			get
			{
				return this.m_SubMeshIndex;
			}
			set
			{
				this.m_SubMeshIndex = value;
			}
		}

		// Token: 0x170002DD RID: 733
		// (get) Token: 0x06000ED7 RID: 3799 RVA: 0x000196E0 File Offset: 0x000178E0
		// (set) Token: 0x06000ED8 RID: 3800 RVA: 0x000196FB File Offset: 0x000178FB
		public Matrix4x4 transform
		{
			get
			{
				return this.m_Transform;
			}
			set
			{
				this.m_Transform = value;
			}
		}

		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06000ED9 RID: 3801 RVA: 0x00019708 File Offset: 0x00017908
		// (set) Token: 0x06000EDA RID: 3802 RVA: 0x00019723 File Offset: 0x00017923
		public Vector4 lightmapScaleOffset
		{
			get
			{
				return this.m_LightmapScaleOffset;
			}
			set
			{
				this.m_LightmapScaleOffset = value;
			}
		}

		// Token: 0x170002DF RID: 735
		// (get) Token: 0x06000EDB RID: 3803 RVA: 0x00019730 File Offset: 0x00017930
		// (set) Token: 0x06000EDC RID: 3804 RVA: 0x0001974B File Offset: 0x0001794B
		public Vector4 realtimeLightmapScaleOffset
		{
			get
			{
				return this.m_RealtimeLightmapScaleOffset;
			}
			set
			{
				this.m_RealtimeLightmapScaleOffset = value;
			}
		}

		// Token: 0x040006FA RID: 1786
		private int m_MeshInstanceID;

		// Token: 0x040006FB RID: 1787
		private int m_SubMeshIndex;

		// Token: 0x040006FC RID: 1788
		private Matrix4x4 m_Transform;

		// Token: 0x040006FD RID: 1789
		private Vector4 m_LightmapScaleOffset;

		// Token: 0x040006FE RID: 1790
		private Vector4 m_RealtimeLightmapScaleOffset;
	}
}
