﻿using System;

namespace UnityEngine
{
	// Token: 0x020001E2 RID: 482
	public class PlayerPrefsException : Exception
	{
		// Token: 0x06000F59 RID: 3929 RVA: 0x0001A909 File Offset: 0x00018B09
		public PlayerPrefsException(string error) : base(error)
		{
		}
	}
}
