﻿using System;

namespace UnityEngine
{
	// Token: 0x02000121 RID: 289
	public enum TouchPhase
	{
		// Token: 0x04000518 RID: 1304
		Began,
		// Token: 0x04000519 RID: 1305
		Moved,
		// Token: 0x0400051A RID: 1306
		Stationary,
		// Token: 0x0400051B RID: 1307
		Ended,
		// Token: 0x0400051C RID: 1308
		Canceled
	}
}
