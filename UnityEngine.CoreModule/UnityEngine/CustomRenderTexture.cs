﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000230 RID: 560
	[NativeHeader("Runtime/Graphics/CustomRenderTexture.h")]
	[UsedByNativeCode]
	public sealed class CustomRenderTexture : RenderTexture
	{
		// Token: 0x06001404 RID: 5124 RVA: 0x000225B2 File Offset: 0x000207B2
		public CustomRenderTexture(int width, int height, RenderTextureFormat format, RenderTextureReadWrite readWrite)
		{
			if (base.ValidateFormat(format))
			{
				CustomRenderTexture.Internal_CreateCustomRenderTexture(this, readWrite);
				this.width = width;
				this.height = height;
				base.format = format;
			}
		}

		// Token: 0x06001405 RID: 5125 RVA: 0x000225E9 File Offset: 0x000207E9
		public CustomRenderTexture(int width, int height, RenderTextureFormat format)
		{
			if (base.ValidateFormat(format))
			{
				CustomRenderTexture.Internal_CreateCustomRenderTexture(this, RenderTextureReadWrite.Default);
				this.width = width;
				this.height = height;
				base.format = format;
			}
		}

		// Token: 0x06001406 RID: 5126 RVA: 0x0002261F File Offset: 0x0002081F
		public CustomRenderTexture(int width, int height)
		{
			CustomRenderTexture.Internal_CreateCustomRenderTexture(this, RenderTextureReadWrite.Default);
			this.width = width;
			this.height = height;
			base.format = RenderTextureFormat.Default;
		}

		// Token: 0x06001407 RID: 5127 RVA: 0x00022644 File Offset: 0x00020844
		public CustomRenderTexture(int width, int height, GraphicsFormat format)
		{
			CustomRenderTexture.Internal_CreateCustomRenderTexture(this, (!GraphicsFormatUtility.IsSRGBFormat(format)) ? RenderTextureReadWrite.Linear : RenderTextureReadWrite.sRGB);
			this.width = width;
			this.height = height;
			base.format = GraphicsFormatUtility.GetRenderTextureFormat(format);
		}

		// Token: 0x06001408 RID: 5128
		[FreeFunction(Name = "CustomRenderTextureScripting::Create")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateCustomRenderTexture([Writable] CustomRenderTexture rt, RenderTextureReadWrite readWrite);

		// Token: 0x06001409 RID: 5129
		[NativeName("TriggerUpdate")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Update(int count);

		// Token: 0x0600140A RID: 5130 RVA: 0x0002267F File Offset: 0x0002087F
		public void Update()
		{
			this.Update(1);
		}

		// Token: 0x0600140B RID: 5131
		[NativeName("TriggerInitialization")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Initialize();

		// Token: 0x0600140C RID: 5132
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ClearUpdateZones();

		// Token: 0x170003D4 RID: 980
		// (get) Token: 0x0600140D RID: 5133
		// (set) Token: 0x0600140E RID: 5134
		public extern Material material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003D5 RID: 981
		// (get) Token: 0x0600140F RID: 5135
		// (set) Token: 0x06001410 RID: 5136
		public extern Material initializationMaterial { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x06001411 RID: 5137
		// (set) Token: 0x06001412 RID: 5138
		public extern Texture initializationTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001413 RID: 5139
		[FreeFunction(Name = "CustomRenderTextureScripting::GetUpdateZonesInternal", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetUpdateZonesInternal([NotNull] object updateZones);

		// Token: 0x06001414 RID: 5140 RVA: 0x00022689 File Offset: 0x00020889
		public void GetUpdateZones(List<CustomRenderTextureUpdateZone> updateZones)
		{
			this.GetUpdateZonesInternal(updateZones);
		}

		// Token: 0x06001415 RID: 5141
		[FreeFunction(Name = "CustomRenderTextureScripting::SetUpdateZonesInternal", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetUpdateZonesInternal(CustomRenderTextureUpdateZone[] updateZones);

		// Token: 0x06001416 RID: 5142 RVA: 0x00022693 File Offset: 0x00020893
		public void SetUpdateZones(CustomRenderTextureUpdateZone[] updateZones)
		{
			if (updateZones == null)
			{
				throw new ArgumentNullException("updateZones");
			}
			this.SetUpdateZonesInternal(updateZones);
		}

		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x06001417 RID: 5143
		// (set) Token: 0x06001418 RID: 5144
		public extern CustomRenderTextureInitializationSource initializationSource { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003D8 RID: 984
		// (get) Token: 0x06001419 RID: 5145 RVA: 0x000226B0 File Offset: 0x000208B0
		// (set) Token: 0x0600141A RID: 5146 RVA: 0x000226C6 File Offset: 0x000208C6
		public Color initializationColor
		{
			get
			{
				Color result;
				this.get_initializationColor_Injected(out result);
				return result;
			}
			set
			{
				this.set_initializationColor_Injected(ref value);
			}
		}

		// Token: 0x170003D9 RID: 985
		// (get) Token: 0x0600141B RID: 5147
		// (set) Token: 0x0600141C RID: 5148
		public extern CustomRenderTextureUpdateMode updateMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003DA RID: 986
		// (get) Token: 0x0600141D RID: 5149
		// (set) Token: 0x0600141E RID: 5150
		public extern CustomRenderTextureUpdateMode initializationMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003DB RID: 987
		// (get) Token: 0x0600141F RID: 5151
		// (set) Token: 0x06001420 RID: 5152
		public extern CustomRenderTextureUpdateZoneSpace updateZoneSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003DC RID: 988
		// (get) Token: 0x06001421 RID: 5153
		// (set) Token: 0x06001422 RID: 5154
		public extern int shaderPass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003DD RID: 989
		// (get) Token: 0x06001423 RID: 5155
		// (set) Token: 0x06001424 RID: 5156
		public extern uint cubemapFaceMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003DE RID: 990
		// (get) Token: 0x06001425 RID: 5157
		// (set) Token: 0x06001426 RID: 5158
		public extern bool doubleBuffered { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003DF RID: 991
		// (get) Token: 0x06001427 RID: 5159
		// (set) Token: 0x06001428 RID: 5160
		public extern bool wrapUpdateZones { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001429 RID: 5161 RVA: 0x000226D0 File Offset: 0x000208D0
		private bool IsCubemapFaceEnabled(CubemapFace face)
		{
			return ((ulong)this.cubemapFaceMask & (ulong)(1L << (int)(face & (CubemapFace)31))) != 0UL;
		}

		// Token: 0x0600142A RID: 5162 RVA: 0x000226FC File Offset: 0x000208FC
		private void EnableCubemapFace(CubemapFace face, bool value)
		{
			uint num = this.cubemapFaceMask;
			uint num2 = 1U << (int)face;
			if (value)
			{
				num |= num2;
			}
			else
			{
				num &= ~num2;
			}
			this.cubemapFaceMask = num;
		}

		// Token: 0x0600142B RID: 5163
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_initializationColor_Injected(out Color ret);

		// Token: 0x0600142C RID: 5164
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_initializationColor_Injected(ref Color value);
	}
}
