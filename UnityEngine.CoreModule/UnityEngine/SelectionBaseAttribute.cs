﻿using System;

namespace UnityEngine
{
	// Token: 0x02000208 RID: 520
	[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
	public class SelectionBaseAttribute : Attribute
	{
		// Token: 0x060011A5 RID: 4517 RVA: 0x0000898B File Offset: 0x00006B8B
		public SelectionBaseAttribute()
		{
		}
	}
}
