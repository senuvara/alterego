﻿using System;

namespace UnityEngine
{
	// Token: 0x020000DE RID: 222
	public enum CustomRenderTextureInitializationSource
	{
		// Token: 0x04000369 RID: 873
		TextureAndColor,
		// Token: 0x0400036A RID: 874
		Material
	}
}
