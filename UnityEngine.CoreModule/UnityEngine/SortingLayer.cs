﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000215 RID: 533
	[NativeHeader("Runtime/BaseClasses/TagManager.h")]
	public struct SortingLayer
	{
		// Token: 0x1700033F RID: 831
		// (get) Token: 0x060011EF RID: 4591 RVA: 0x0001E40C File Offset: 0x0001C60C
		public int id
		{
			get
			{
				return this.m_Id;
			}
		}

		// Token: 0x17000340 RID: 832
		// (get) Token: 0x060011F0 RID: 4592 RVA: 0x0001E428 File Offset: 0x0001C628
		public string name
		{
			get
			{
				return SortingLayer.IDToName(this.m_Id);
			}
		}

		// Token: 0x17000341 RID: 833
		// (get) Token: 0x060011F1 RID: 4593 RVA: 0x0001E448 File Offset: 0x0001C648
		public int value
		{
			get
			{
				return SortingLayer.GetLayerValueFromID(this.m_Id);
			}
		}

		// Token: 0x17000342 RID: 834
		// (get) Token: 0x060011F2 RID: 4594 RVA: 0x0001E468 File Offset: 0x0001C668
		public static SortingLayer[] layers
		{
			get
			{
				int[] sortingLayerIDsInternal = SortingLayer.GetSortingLayerIDsInternal();
				SortingLayer[] array = new SortingLayer[sortingLayerIDsInternal.Length];
				for (int i = 0; i < sortingLayerIDsInternal.Length; i++)
				{
					array[i].m_Id = sortingLayerIDsInternal[i];
				}
				return array;
			}
		}

		// Token: 0x060011F3 RID: 4595
		[FreeFunction("GetTagManager().GetSortingLayerIDs")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int[] GetSortingLayerIDsInternal();

		// Token: 0x060011F4 RID: 4596
		[FreeFunction("GetTagManager().GetSortingLayerValueFromUniqueID")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetLayerValueFromID(int id);

		// Token: 0x060011F5 RID: 4597
		[FreeFunction("GetTagManager().GetSortingLayerValueFromName")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetLayerValueFromName(string name);

		// Token: 0x060011F6 RID: 4598
		[FreeFunction("GetTagManager().GetSortingLayerUniqueIDFromName")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int NameToID(string name);

		// Token: 0x060011F7 RID: 4599
		[FreeFunction("GetTagManager().GetSortingLayerNameFromUniqueID")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string IDToName(int id);

		// Token: 0x060011F8 RID: 4600
		[FreeFunction("GetTagManager().IsSortingLayerUniqueIDValid")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsValid(int id);

		// Token: 0x04000767 RID: 1895
		private int m_Id;
	}
}
