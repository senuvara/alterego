﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020000A4 RID: 164
	[NativeHeader("Runtime/Camera/OcclusionPortal.h")]
	public sealed class OcclusionPortal : Component
	{
		// Token: 0x06000A9C RID: 2716 RVA: 0x00008E34 File Offset: 0x00007034
		public OcclusionPortal()
		{
		}

		// Token: 0x170001DE RID: 478
		// (get) Token: 0x06000A9D RID: 2717
		// (set) Token: 0x06000A9E RID: 2718
		[NativeProperty("IsOpen")]
		public extern bool open { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
