﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000067 RID: 103
	[NativeHeader("Runtime/Export/Debug.bindings.h")]
	public class Debug
	{
		// Token: 0x0600053B RID: 1339 RVA: 0x00002370 File Offset: 0x00000570
		public Debug()
		{
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x0600053C RID: 1340 RVA: 0x0000CF08 File Offset: 0x0000B108
		public static ILogger unityLogger
		{
			get
			{
				return Debug.s_Logger;
			}
		}

		// Token: 0x0600053D RID: 1341 RVA: 0x0000CF24 File Offset: 0x0000B124
		[ExcludeFromDocs]
		public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration)
		{
			bool depthTest = true;
			Debug.DrawLine(start, end, color, duration, depthTest);
		}

		// Token: 0x0600053E RID: 1342 RVA: 0x0000CF40 File Offset: 0x0000B140
		[ExcludeFromDocs]
		public static void DrawLine(Vector3 start, Vector3 end, Color color)
		{
			bool depthTest = true;
			float duration = 0f;
			Debug.DrawLine(start, end, color, duration, depthTest);
		}

		// Token: 0x0600053F RID: 1343 RVA: 0x0000CF60 File Offset: 0x0000B160
		[ExcludeFromDocs]
		public static void DrawLine(Vector3 start, Vector3 end)
		{
			bool depthTest = true;
			float duration = 0f;
			Color white = Color.white;
			Debug.DrawLine(start, end, white, duration, depthTest);
		}

		// Token: 0x06000540 RID: 1344 RVA: 0x0000CF86 File Offset: 0x0000B186
		[FreeFunction("DebugDrawLine")]
		public static void DrawLine(Vector3 start, Vector3 end, [UnityEngine.Internal.DefaultValue("Color.white")] Color color, [UnityEngine.Internal.DefaultValue("0.0f")] float duration, [UnityEngine.Internal.DefaultValue("true")] bool depthTest)
		{
			Debug.DrawLine_Injected(ref start, ref end, ref color, duration, depthTest);
		}

		// Token: 0x06000541 RID: 1345 RVA: 0x0000CF98 File Offset: 0x0000B198
		[ExcludeFromDocs]
		public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration)
		{
			bool depthTest = true;
			Debug.DrawRay(start, dir, color, duration, depthTest);
		}

		// Token: 0x06000542 RID: 1346 RVA: 0x0000CFB4 File Offset: 0x0000B1B4
		[ExcludeFromDocs]
		public static void DrawRay(Vector3 start, Vector3 dir, Color color)
		{
			bool depthTest = true;
			float duration = 0f;
			Debug.DrawRay(start, dir, color, duration, depthTest);
		}

		// Token: 0x06000543 RID: 1347 RVA: 0x0000CFD4 File Offset: 0x0000B1D4
		[ExcludeFromDocs]
		public static void DrawRay(Vector3 start, Vector3 dir)
		{
			bool depthTest = true;
			float duration = 0f;
			Color white = Color.white;
			Debug.DrawRay(start, dir, white, duration, depthTest);
		}

		// Token: 0x06000544 RID: 1348 RVA: 0x0000CFFA File Offset: 0x0000B1FA
		public static void DrawRay(Vector3 start, Vector3 dir, [UnityEngine.Internal.DefaultValue("Color.white")] Color color, [UnityEngine.Internal.DefaultValue("0.0f")] float duration, [UnityEngine.Internal.DefaultValue("true")] bool depthTest)
		{
			Debug.DrawLine(start, start + dir, color, duration, depthTest);
		}

		// Token: 0x06000545 RID: 1349
		[FreeFunction("PauseEditor")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Break();

		// Token: 0x06000546 RID: 1350
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void DebugBreak();

		// Token: 0x06000547 RID: 1351 RVA: 0x0000D00E File Offset: 0x0000B20E
		public static void Log(object message)
		{
			Debug.unityLogger.Log(LogType.Log, message);
		}

		// Token: 0x06000548 RID: 1352 RVA: 0x0000D01D File Offset: 0x0000B21D
		public static void Log(object message, Object context)
		{
			Debug.unityLogger.Log(LogType.Log, message, context);
		}

		// Token: 0x06000549 RID: 1353 RVA: 0x0000D02D File Offset: 0x0000B22D
		public static void LogFormat(string format, params object[] args)
		{
			Debug.unityLogger.LogFormat(LogType.Log, format, args);
		}

		// Token: 0x0600054A RID: 1354 RVA: 0x0000D03D File Offset: 0x0000B23D
		public static void LogFormat(Object context, string format, params object[] args)
		{
			Debug.unityLogger.LogFormat(LogType.Log, context, format, args);
		}

		// Token: 0x0600054B RID: 1355 RVA: 0x0000D04E File Offset: 0x0000B24E
		public static void LogError(object message)
		{
			Debug.unityLogger.Log(LogType.Error, message);
		}

		// Token: 0x0600054C RID: 1356 RVA: 0x0000D05D File Offset: 0x0000B25D
		public static void LogError(object message, Object context)
		{
			Debug.unityLogger.Log(LogType.Error, message, context);
		}

		// Token: 0x0600054D RID: 1357 RVA: 0x0000D06D File Offset: 0x0000B26D
		public static void LogErrorFormat(string format, params object[] args)
		{
			Debug.unityLogger.LogFormat(LogType.Error, format, args);
		}

		// Token: 0x0600054E RID: 1358 RVA: 0x0000D07D File Offset: 0x0000B27D
		public static void LogErrorFormat(Object context, string format, params object[] args)
		{
			Debug.unityLogger.LogFormat(LogType.Error, context, format, args);
		}

		// Token: 0x0600054F RID: 1359
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ClearDeveloperConsole();

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x06000550 RID: 1360
		// (set) Token: 0x06000551 RID: 1361
		public static extern bool developerConsoleVisible { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000552 RID: 1362 RVA: 0x0000D08E File Offset: 0x0000B28E
		public static void LogException(Exception exception)
		{
			Debug.unityLogger.LogException(exception, null);
		}

		// Token: 0x06000553 RID: 1363 RVA: 0x0000D09D File Offset: 0x0000B29D
		public static void LogException(Exception exception, Object context)
		{
			Debug.unityLogger.LogException(exception, context);
		}

		// Token: 0x06000554 RID: 1364 RVA: 0x0000D0AC File Offset: 0x0000B2AC
		public static void LogWarning(object message)
		{
			Debug.unityLogger.Log(LogType.Warning, message);
		}

		// Token: 0x06000555 RID: 1365 RVA: 0x0000D0BB File Offset: 0x0000B2BB
		public static void LogWarning(object message, Object context)
		{
			Debug.unityLogger.Log(LogType.Warning, message, context);
		}

		// Token: 0x06000556 RID: 1366 RVA: 0x0000D0CB File Offset: 0x0000B2CB
		public static void LogWarningFormat(string format, params object[] args)
		{
			Debug.unityLogger.LogFormat(LogType.Warning, format, args);
		}

		// Token: 0x06000557 RID: 1367 RVA: 0x0000D0DB File Offset: 0x0000B2DB
		public static void LogWarningFormat(Object context, string format, params object[] args)
		{
			Debug.unityLogger.LogFormat(LogType.Warning, context, format, args);
		}

		// Token: 0x06000558 RID: 1368 RVA: 0x0000D0EC File Offset: 0x0000B2EC
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition)
		{
			if (!condition)
			{
				Debug.unityLogger.Log(LogType.Assert, "Assertion failed");
			}
		}

		// Token: 0x06000559 RID: 1369 RVA: 0x0000D105 File Offset: 0x0000B305
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, Object context)
		{
			if (!condition)
			{
				Debug.unityLogger.Log(LogType.Assert, "Assertion failed", context);
			}
		}

		// Token: 0x0600055A RID: 1370 RVA: 0x0000D11F File Offset: 0x0000B31F
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, object message)
		{
			if (!condition)
			{
				Debug.unityLogger.Log(LogType.Assert, message);
			}
		}

		// Token: 0x0600055B RID: 1371 RVA: 0x0000D11F File Offset: 0x0000B31F
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, string message)
		{
			if (!condition)
			{
				Debug.unityLogger.Log(LogType.Assert, message);
			}
		}

		// Token: 0x0600055C RID: 1372 RVA: 0x0000D134 File Offset: 0x0000B334
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, object message, Object context)
		{
			if (!condition)
			{
				Debug.unityLogger.Log(LogType.Assert, message, context);
			}
		}

		// Token: 0x0600055D RID: 1373 RVA: 0x0000D134 File Offset: 0x0000B334
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, string message, Object context)
		{
			if (!condition)
			{
				Debug.unityLogger.Log(LogType.Assert, message, context);
			}
		}

		// Token: 0x0600055E RID: 1374 RVA: 0x0000D14A File Offset: 0x0000B34A
		[Conditional("UNITY_ASSERTIONS")]
		public static void AssertFormat(bool condition, string format, params object[] args)
		{
			if (!condition)
			{
				Debug.unityLogger.LogFormat(LogType.Assert, format, args);
			}
		}

		// Token: 0x0600055F RID: 1375 RVA: 0x0000D160 File Offset: 0x0000B360
		[Conditional("UNITY_ASSERTIONS")]
		public static void AssertFormat(bool condition, Object context, string format, params object[] args)
		{
			if (!condition)
			{
				Debug.unityLogger.LogFormat(LogType.Assert, context, format, args);
			}
		}

		// Token: 0x06000560 RID: 1376 RVA: 0x0000D177 File Offset: 0x0000B377
		[Conditional("UNITY_ASSERTIONS")]
		public static void LogAssertion(object message)
		{
			Debug.unityLogger.Log(LogType.Assert, message);
		}

		// Token: 0x06000561 RID: 1377 RVA: 0x0000D186 File Offset: 0x0000B386
		[Conditional("UNITY_ASSERTIONS")]
		public static void LogAssertion(object message, Object context)
		{
			Debug.unityLogger.Log(LogType.Assert, message, context);
		}

		// Token: 0x06000562 RID: 1378 RVA: 0x0000D196 File Offset: 0x0000B396
		[Conditional("UNITY_ASSERTIONS")]
		public static void LogAssertionFormat(string format, params object[] args)
		{
			Debug.unityLogger.LogFormat(LogType.Assert, format, args);
		}

		// Token: 0x06000563 RID: 1379 RVA: 0x0000D1A6 File Offset: 0x0000B3A6
		[Conditional("UNITY_ASSERTIONS")]
		public static void LogAssertionFormat(Object context, string format, params object[] args)
		{
			Debug.unityLogger.LogFormat(LogType.Assert, context, format, args);
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000564 RID: 1380
		[NativeProperty(TargetType = TargetType.Field)]
		[StaticAccessor("GetBuildSettings()", StaticAccessorType.Dot)]
		public static extern bool isDebugBuild { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000565 RID: 1381
		[FreeFunction("DeveloperConsole_OpenConsoleFile")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void OpenConsoleFile();

		// Token: 0x06000566 RID: 1382
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void GetDiagnosticSwitches(List<DiagnosticSwitch> results);

		// Token: 0x06000567 RID: 1383
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern object GetDiagnosticSwitch(string name);

		// Token: 0x06000568 RID: 1384
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetDiagnosticSwitch(string name, object value, bool setPersistent);

		// Token: 0x06000569 RID: 1385 RVA: 0x0000D14A File Offset: 0x0000B34A
		[Obsolete("Assert(bool, string, params object[]) is obsolete. Use AssertFormat(bool, string, params object[]) (UnityUpgradable) -> AssertFormat(*)", true)]
		[Conditional("UNITY_ASSERTIONS")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void Assert(bool condition, string format, params object[] args)
		{
			if (!condition)
			{
				Debug.unityLogger.LogFormat(LogType.Assert, format, args);
			}
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x0600056A RID: 1386 RVA: 0x0000D1B8 File Offset: 0x0000B3B8
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Debug.logger is obsolete. Please use Debug.unityLogger instead (UnityUpgradable) -> unityLogger")]
		public static ILogger logger
		{
			get
			{
				return Debug.s_Logger;
			}
		}

		// Token: 0x0600056B RID: 1387 RVA: 0x0000D1D2 File Offset: 0x0000B3D2
		// Note: this type is marked as 'beforefieldinit'.
		static Debug()
		{
		}

		// Token: 0x0600056C RID: 1388
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DrawLine_Injected(ref Vector3 start, ref Vector3 end, [UnityEngine.Internal.DefaultValue("Color.white")] ref Color color, [UnityEngine.Internal.DefaultValue("0.0f")] float duration, [UnityEngine.Internal.DefaultValue("true")] bool depthTest);

		// Token: 0x04000133 RID: 307
		internal static ILogger s_Logger = new Logger(new DebugLogHandler());
	}
}
