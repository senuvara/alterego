﻿using System;

namespace UnityEngine
{
	// Token: 0x02000143 RID: 323
	public enum LODFadeMode
	{
		// Token: 0x040006B2 RID: 1714
		None,
		// Token: 0x040006B3 RID: 1715
		CrossFade,
		// Token: 0x040006B4 RID: 1716
		SpeedTree
	}
}
