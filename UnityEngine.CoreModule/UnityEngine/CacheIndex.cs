﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000048 RID: 72
	[Obsolete("This struct is not for public use.")]
	[UsedByNativeCode]
	public struct CacheIndex
	{
		// Token: 0x040000F3 RID: 243
		public string name;

		// Token: 0x040000F4 RID: 244
		public int bytesUsed;

		// Token: 0x040000F5 RID: 245
		public int expires;
	}
}
