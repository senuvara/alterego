﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000099 RID: 153
	[UsedByNativeCode]
	public sealed class ImageEffectOpaque : Attribute
	{
		// Token: 0x0600080A RID: 2058 RVA: 0x0000898B File Offset: 0x00006B8B
		public ImageEffectOpaque()
		{
		}
	}
}
