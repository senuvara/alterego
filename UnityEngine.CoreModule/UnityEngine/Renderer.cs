﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Rendering;
using UnityEngine.Scripting;
using UnityEngineInternal;

namespace UnityEngine
{
	// Token: 0x0200009F RID: 159
	[RequireComponent(typeof(Transform))]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Graphics/GraphicsScriptBindings.h")]
	[NativeHeader("Runtime/Graphics/Renderer.h")]
	public class Renderer : Component
	{
		// Token: 0x060008ED RID: 2285 RVA: 0x00008E34 File Offset: 0x00007034
		public Renderer()
		{
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x060008EE RID: 2286 RVA: 0x00010DB0 File Offset: 0x0000EFB0
		// (set) Token: 0x060008EF RID: 2287 RVA: 0x00010DD1 File Offset: 0x0000EFD1
		[Obsolete("Use shadowCastingMode instead.", false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool castShadows
		{
			get
			{
				return this.shadowCastingMode != ShadowCastingMode.Off;
			}
			set
			{
				this.shadowCastingMode = ((!value) ? ShadowCastingMode.Off : ShadowCastingMode.On);
			}
		}

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x060008F0 RID: 2288 RVA: 0x00010DE8 File Offset: 0x0000EFE8
		// (set) Token: 0x060008F1 RID: 2289 RVA: 0x00010E06 File Offset: 0x0000F006
		[Obsolete("Use motionVectorGenerationMode instead.", false)]
		public bool motionVectors
		{
			get
			{
				return this.motionVectorGenerationMode == MotionVectorGenerationMode.Object;
			}
			set
			{
				this.motionVectorGenerationMode = ((!value) ? MotionVectorGenerationMode.Camera : MotionVectorGenerationMode.Object);
			}
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x060008F2 RID: 2290 RVA: 0x00010E1C File Offset: 0x0000F01C
		// (set) Token: 0x060008F3 RID: 2291 RVA: 0x00010E3D File Offset: 0x0000F03D
		[Obsolete("Use lightProbeUsage instead.", false)]
		public bool useLightProbes
		{
			get
			{
				return this.lightProbeUsage != LightProbeUsage.Off;
			}
			set
			{
				this.lightProbeUsage = ((!value) ? LightProbeUsage.Off : LightProbeUsage.BlendProbes);
			}
		}

		// Token: 0x17000193 RID: 403
		// (get) Token: 0x060008F4 RID: 2292 RVA: 0x00010E54 File Offset: 0x0000F054
		public Bounds bounds
		{
			[FreeFunction(Name = "RendererScripting::GetBounds", HasExplicitThis = true)]
			get
			{
				Bounds result;
				this.get_bounds_Injected(out result);
				return result;
			}
		}

		// Token: 0x060008F5 RID: 2293 RVA: 0x00010E6A File Offset: 0x0000F06A
		[FreeFunction(Name = "RendererScripting::SetStaticLightmapST", HasExplicitThis = true)]
		private void SetStaticLightmapST(Vector4 st)
		{
			this.SetStaticLightmapST_Injected(ref st);
		}

		// Token: 0x060008F6 RID: 2294
		[FreeFunction(Name = "RendererScripting::GetMaterial", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Material GetMaterial();

		// Token: 0x060008F7 RID: 2295
		[FreeFunction(Name = "RendererScripting::GetSharedMaterial", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Material GetSharedMaterial();

		// Token: 0x060008F8 RID: 2296
		[FreeFunction(Name = "RendererScripting::SetMaterial", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetMaterial(Material m);

		// Token: 0x060008F9 RID: 2297
		[FreeFunction(Name = "RendererScripting::GetMaterialArray", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Material[] GetMaterialArray();

		// Token: 0x060008FA RID: 2298
		[FreeFunction(Name = "RendererScripting::GetMaterialArray", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void CopyMaterialArray([Out] Material[] m);

		// Token: 0x060008FB RID: 2299
		[FreeFunction(Name = "RendererScripting::GetSharedMaterialArray", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void CopySharedMaterialArray([Out] Material[] m);

		// Token: 0x060008FC RID: 2300
		[FreeFunction(Name = "RendererScripting::SetMaterialArray", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetMaterialArray([NotNull] Material[] m);

		// Token: 0x060008FD RID: 2301
		[FreeFunction(Name = "RendererScripting::SetPropertyBlock", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void Internal_SetPropertyBlock(MaterialPropertyBlock properties);

		// Token: 0x060008FE RID: 2302
		[FreeFunction(Name = "RendererScripting::GetPropertyBlock", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void Internal_GetPropertyBlock([NotNull] MaterialPropertyBlock dest);

		// Token: 0x060008FF RID: 2303
		[FreeFunction(Name = "RendererScripting::SetPropertyBlockMaterialIndex", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void Internal_SetPropertyBlockMaterialIndex(MaterialPropertyBlock properties, int materialIndex);

		// Token: 0x06000900 RID: 2304
		[FreeFunction(Name = "RendererScripting::GetPropertyBlockMaterialIndex", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void Internal_GetPropertyBlockMaterialIndex([NotNull] MaterialPropertyBlock dest, int materialIndex);

		// Token: 0x06000901 RID: 2305
		[FreeFunction(Name = "RendererScripting::HasPropertyBlock", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasPropertyBlock();

		// Token: 0x06000902 RID: 2306 RVA: 0x00010E74 File Offset: 0x0000F074
		public void SetPropertyBlock(MaterialPropertyBlock properties)
		{
			this.Internal_SetPropertyBlock(properties);
		}

		// Token: 0x06000903 RID: 2307 RVA: 0x00010E7E File Offset: 0x0000F07E
		public void SetPropertyBlock(MaterialPropertyBlock properties, int materialIndex)
		{
			this.Internal_SetPropertyBlockMaterialIndex(properties, materialIndex);
		}

		// Token: 0x06000904 RID: 2308 RVA: 0x00010E89 File Offset: 0x0000F089
		public void GetPropertyBlock(MaterialPropertyBlock properties)
		{
			this.Internal_GetPropertyBlock(properties);
		}

		// Token: 0x06000905 RID: 2309 RVA: 0x00010E93 File Offset: 0x0000F093
		public void GetPropertyBlock(MaterialPropertyBlock properties, int materialIndex)
		{
			this.Internal_GetPropertyBlockMaterialIndex(properties, materialIndex);
		}

		// Token: 0x06000906 RID: 2310
		[FreeFunction(Name = "RendererScripting::GetClosestReflectionProbes", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetClosestReflectionProbesInternal(object result);

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x06000907 RID: 2311
		// (set) Token: 0x06000908 RID: 2312
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000195 RID: 405
		// (get) Token: 0x06000909 RID: 2313
		public extern bool isVisible { [NativeName("IsVisibleInScene")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000196 RID: 406
		// (get) Token: 0x0600090A RID: 2314
		// (set) Token: 0x0600090B RID: 2315
		public extern ShadowCastingMode shadowCastingMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000197 RID: 407
		// (get) Token: 0x0600090C RID: 2316
		// (set) Token: 0x0600090D RID: 2317
		public extern bool receiveShadows { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000198 RID: 408
		// (get) Token: 0x0600090E RID: 2318
		// (set) Token: 0x0600090F RID: 2319
		public extern MotionVectorGenerationMode motionVectorGenerationMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000199 RID: 409
		// (get) Token: 0x06000910 RID: 2320
		// (set) Token: 0x06000911 RID: 2321
		public extern LightProbeUsage lightProbeUsage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700019A RID: 410
		// (get) Token: 0x06000912 RID: 2322
		// (set) Token: 0x06000913 RID: 2323
		public extern ReflectionProbeUsage reflectionProbeUsage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700019B RID: 411
		// (get) Token: 0x06000914 RID: 2324
		// (set) Token: 0x06000915 RID: 2325
		public extern uint renderingLayerMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700019C RID: 412
		// (get) Token: 0x06000916 RID: 2326
		// (set) Token: 0x06000917 RID: 2327
		public extern int rendererPriority { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700019D RID: 413
		// (get) Token: 0x06000918 RID: 2328
		// (set) Token: 0x06000919 RID: 2329
		public extern string sortingLayerName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700019E RID: 414
		// (get) Token: 0x0600091A RID: 2330
		// (set) Token: 0x0600091B RID: 2331
		public extern int sortingLayerID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700019F RID: 415
		// (get) Token: 0x0600091C RID: 2332
		// (set) Token: 0x0600091D RID: 2333
		public extern int sortingOrder { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x0600091E RID: 2334
		// (set) Token: 0x0600091F RID: 2335
		internal extern int sortingGroupID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x06000920 RID: 2336
		// (set) Token: 0x06000921 RID: 2337
		internal extern int sortingGroupOrder { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x06000922 RID: 2338
		// (set) Token: 0x06000923 RID: 2339
		[NativeProperty("IsDynamicOccludee")]
		public extern bool allowOcclusionWhenDynamic { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x06000924 RID: 2340
		// (set) Token: 0x06000925 RID: 2341
		[NativeProperty("StaticBatchRoot")]
		internal extern Transform staticBatchRootTransform { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x06000926 RID: 2342
		internal extern int staticBatchIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000927 RID: 2343
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetStaticBatchInfo(int firstSubMesh, int subMeshCount);

		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x06000928 RID: 2344
		public extern bool isPartOfStaticBatch { [NativeName("IsPartOfStaticBatch")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x06000929 RID: 2345 RVA: 0x00010EA0 File Offset: 0x0000F0A0
		public Matrix4x4 worldToLocalMatrix
		{
			get
			{
				Matrix4x4 result;
				this.get_worldToLocalMatrix_Injected(out result);
				return result;
			}
		}

		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x0600092A RID: 2346 RVA: 0x00010EB8 File Offset: 0x0000F0B8
		public Matrix4x4 localToWorldMatrix
		{
			get
			{
				Matrix4x4 result;
				this.get_localToWorldMatrix_Injected(out result);
				return result;
			}
		}

		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x0600092B RID: 2347
		// (set) Token: 0x0600092C RID: 2348
		public extern GameObject lightProbeProxyVolumeOverride { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x0600092D RID: 2349
		// (set) Token: 0x0600092E RID: 2350
		public extern Transform probeAnchor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600092F RID: 2351
		[NativeName("GetLightmapIndexInt")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetLightmapIndex(LightmapType lt);

		// Token: 0x06000930 RID: 2352
		[NativeName("SetLightmapIndexInt")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetLightmapIndex(int index, LightmapType lt);

		// Token: 0x06000931 RID: 2353 RVA: 0x00010ED0 File Offset: 0x0000F0D0
		[NativeName("GetLightmapST")]
		private Vector4 GetLightmapST(LightmapType lt)
		{
			Vector4 result;
			this.GetLightmapST_Injected(lt, out result);
			return result;
		}

		// Token: 0x06000932 RID: 2354 RVA: 0x00010EE7 File Offset: 0x0000F0E7
		[NativeName("SetLightmapST")]
		private void SetLightmapST(Vector4 st, LightmapType lt)
		{
			this.SetLightmapST_Injected(ref st, lt);
		}

		// Token: 0x170001AA RID: 426
		// (get) Token: 0x06000933 RID: 2355 RVA: 0x00010EF4 File Offset: 0x0000F0F4
		// (set) Token: 0x06000934 RID: 2356 RVA: 0x00010F10 File Offset: 0x0000F110
		public int lightmapIndex
		{
			get
			{
				return this.GetLightmapIndex(LightmapType.StaticLightmap);
			}
			set
			{
				this.SetLightmapIndex(value, LightmapType.StaticLightmap);
			}
		}

		// Token: 0x170001AB RID: 427
		// (get) Token: 0x06000935 RID: 2357 RVA: 0x00010F1C File Offset: 0x0000F11C
		// (set) Token: 0x06000936 RID: 2358 RVA: 0x00010F38 File Offset: 0x0000F138
		public int realtimeLightmapIndex
		{
			get
			{
				return this.GetLightmapIndex(LightmapType.DynamicLightmap);
			}
			set
			{
				this.SetLightmapIndex(value, LightmapType.DynamicLightmap);
			}
		}

		// Token: 0x170001AC RID: 428
		// (get) Token: 0x06000937 RID: 2359 RVA: 0x00010F44 File Offset: 0x0000F144
		// (set) Token: 0x06000938 RID: 2360 RVA: 0x00010F60 File Offset: 0x0000F160
		public Vector4 lightmapScaleOffset
		{
			get
			{
				return this.GetLightmapST(LightmapType.StaticLightmap);
			}
			set
			{
				this.SetStaticLightmapST(value);
			}
		}

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x06000939 RID: 2361 RVA: 0x00010F6C File Offset: 0x0000F16C
		// (set) Token: 0x0600093A RID: 2362 RVA: 0x00010F88 File Offset: 0x0000F188
		public Vector4 realtimeLightmapScaleOffset
		{
			get
			{
				return this.GetLightmapST(LightmapType.DynamicLightmap);
			}
			set
			{
				this.SetLightmapST(value, LightmapType.DynamicLightmap);
			}
		}

		// Token: 0x0600093B RID: 2363
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetMaterialCount();

		// Token: 0x0600093C RID: 2364
		[NativeName("GetMaterialArray")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Material[] GetSharedMaterialArray();

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x0600093D RID: 2365 RVA: 0x00010F94 File Offset: 0x0000F194
		// (set) Token: 0x0600093E RID: 2366 RVA: 0x00010FAF File Offset: 0x0000F1AF
		public Material[] materials
		{
			get
			{
				return this.GetMaterialArray();
			}
			set
			{
				this.SetMaterialArray(value);
			}
		}

		// Token: 0x170001AF RID: 431
		// (get) Token: 0x0600093F RID: 2367 RVA: 0x00010FBC File Offset: 0x0000F1BC
		// (set) Token: 0x06000940 RID: 2368 RVA: 0x00010FD7 File Offset: 0x0000F1D7
		public Material material
		{
			get
			{
				return this.GetMaterial();
			}
			set
			{
				this.SetMaterial(value);
			}
		}

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x06000941 RID: 2369 RVA: 0x00010FE4 File Offset: 0x0000F1E4
		// (set) Token: 0x06000942 RID: 2370 RVA: 0x00010FD7 File Offset: 0x0000F1D7
		public Material sharedMaterial
		{
			get
			{
				return this.GetSharedMaterial();
			}
			set
			{
				this.SetMaterial(value);
			}
		}

		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x06000943 RID: 2371 RVA: 0x00011000 File Offset: 0x0000F200
		// (set) Token: 0x06000944 RID: 2372 RVA: 0x00010FAF File Offset: 0x0000F1AF
		public Material[] sharedMaterials
		{
			get
			{
				return this.GetSharedMaterialArray();
			}
			set
			{
				this.SetMaterialArray(value);
			}
		}

		// Token: 0x06000945 RID: 2373 RVA: 0x0001101B File Offset: 0x0000F21B
		public void GetMaterials(List<Material> m)
		{
			if (m == null)
			{
				throw new ArgumentNullException("The result material list cannot be null.", "m");
			}
			NoAllocHelpers.EnsureListElemCount<Material>(m, this.GetMaterialCount());
			this.CopyMaterialArray(NoAllocHelpers.ExtractArrayFromListT<Material>(m));
		}

		// Token: 0x06000946 RID: 2374 RVA: 0x0001104C File Offset: 0x0000F24C
		public void GetSharedMaterials(List<Material> m)
		{
			if (m == null)
			{
				throw new ArgumentNullException("The result material list cannot be null.", "m");
			}
			NoAllocHelpers.EnsureListElemCount<Material>(m, this.GetMaterialCount());
			this.CopySharedMaterialArray(NoAllocHelpers.ExtractArrayFromListT<Material>(m));
		}

		// Token: 0x06000947 RID: 2375 RVA: 0x0001107D File Offset: 0x0000F27D
		public void GetClosestReflectionProbes(List<ReflectionProbeBlendInfo> result)
		{
			this.GetClosestReflectionProbesInternal(result);
		}

		// Token: 0x06000948 RID: 2376
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bounds_Injected(out Bounds ret);

		// Token: 0x06000949 RID: 2377
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetStaticLightmapST_Injected(ref Vector4 st);

		// Token: 0x0600094A RID: 2378
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_worldToLocalMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x0600094B RID: 2379
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_localToWorldMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x0600094C RID: 2380
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetLightmapST_Injected(LightmapType lt, out Vector4 ret);

		// Token: 0x0600094D RID: 2381
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetLightmapST_Injected(ref Vector4 st, LightmapType lt);
	}
}
