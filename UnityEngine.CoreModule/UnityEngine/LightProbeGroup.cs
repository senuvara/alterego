﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000147 RID: 327
	[NativeHeader("Runtime/Graphics/LightProbeGroup.h")]
	public sealed class LightProbeGroup : Behaviour
	{
		// Token: 0x06000D0B RID: 3339 RVA: 0x0000A52A File Offset: 0x0000872A
		public LightProbeGroup()
		{
		}

		// Token: 0x1700029C RID: 668
		// (get) Token: 0x06000D0C RID: 3340 RVA: 0x0001446C File Offset: 0x0001266C
		public Vector3[] probePositions
		{
			get
			{
				return null;
			}
		}
	}
}
