﻿using System;

namespace UnityEngine
{
	// Token: 0x0200034F RID: 847
	internal enum RotationOrder
	{
		// Token: 0x04000ACC RID: 2764
		OrderXYZ,
		// Token: 0x04000ACD RID: 2765
		OrderXZY,
		// Token: 0x04000ACE RID: 2766
		OrderYZX,
		// Token: 0x04000ACF RID: 2767
		OrderYXZ,
		// Token: 0x04000AD0 RID: 2768
		OrderZXY,
		// Token: 0x04000AD1 RID: 2769
		OrderZYX
	}
}
