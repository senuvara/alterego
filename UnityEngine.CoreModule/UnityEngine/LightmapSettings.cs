﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200008D RID: 141
	[NativeHeader("Runtime/Graphics/LightmapSettings.h")]
	[StaticAccessor("GetLightmapSettings()")]
	public sealed class LightmapSettings : Object
	{
		// Token: 0x06000786 RID: 1926 RVA: 0x0000D647 File Offset: 0x0000B847
		private LightmapSettings()
		{
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06000787 RID: 1927
		// (set) Token: 0x06000788 RID: 1928
		public static extern LightmapData[] lightmaps { [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x06000789 RID: 1929
		// (set) Token: 0x0600078A RID: 1930
		public static extern LightmapsMode lightmapsMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction(ThrowsException = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x0600078B RID: 1931
		// (set) Token: 0x0600078C RID: 1932
		public static extern LightProbes lightProbes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600078D RID: 1933
		[NativeName("ResetAndAwakeFromLoad")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Reset();

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x0600078E RID: 1934 RVA: 0x0000FD50 File Offset: 0x0000DF50
		// (set) Token: 0x0600078F RID: 1935 RVA: 0x00007476 File Offset: 0x00005676
		[Obsolete("Use lightmapsMode instead.", false)]
		public static LightmapsModeLegacy lightmapsModeLegacy
		{
			get
			{
				return LightmapsModeLegacy.Single;
			}
			set
			{
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000790 RID: 1936 RVA: 0x0000FD68 File Offset: 0x0000DF68
		// (set) Token: 0x06000791 RID: 1937 RVA: 0x00007476 File Offset: 0x00005676
		[Obsolete("Use QualitySettings.desiredColorSpace instead.", false)]
		public static ColorSpace bakedColorSpace
		{
			get
			{
				return QualitySettings.desiredColorSpace;
			}
			set
			{
			}
		}
	}
}
