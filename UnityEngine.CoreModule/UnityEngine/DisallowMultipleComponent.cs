﻿using System;

namespace UnityEngine
{
	// Token: 0x02000028 RID: 40
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public sealed class DisallowMultipleComponent : Attribute
	{
		// Token: 0x06000279 RID: 633 RVA: 0x0000898B File Offset: 0x00006B8B
		public DisallowMultipleComponent()
		{
		}
	}
}
