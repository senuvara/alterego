﻿using System;

namespace UnityEngine
{
	// Token: 0x02000029 RID: 41
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
	public sealed class RequireComponent : Attribute
	{
		// Token: 0x0600027A RID: 634 RVA: 0x00008993 File Offset: 0x00006B93
		public RequireComponent(Type requiredComponent)
		{
			this.m_Type0 = requiredComponent;
		}

		// Token: 0x0600027B RID: 635 RVA: 0x000089A3 File Offset: 0x00006BA3
		public RequireComponent(Type requiredComponent, Type requiredComponent2)
		{
			this.m_Type0 = requiredComponent;
			this.m_Type1 = requiredComponent2;
		}

		// Token: 0x0600027C RID: 636 RVA: 0x000089BA File Offset: 0x00006BBA
		public RequireComponent(Type requiredComponent, Type requiredComponent2, Type requiredComponent3)
		{
			this.m_Type0 = requiredComponent;
			this.m_Type1 = requiredComponent2;
			this.m_Type2 = requiredComponent3;
		}

		// Token: 0x04000062 RID: 98
		public Type m_Type0;

		// Token: 0x04000063 RID: 99
		public Type m_Type1;

		// Token: 0x04000064 RID: 100
		public Type m_Type2;
	}
}
