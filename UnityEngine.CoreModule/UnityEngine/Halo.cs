﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020000AE RID: 174
	[NativeHeader("Runtime/Camera/HaloManager.h")]
	[RequireComponent(typeof(Transform))]
	internal sealed class Halo : Behaviour
	{
		// Token: 0x06000B1C RID: 2844 RVA: 0x0000A52A File Offset: 0x0000872A
		public Halo()
		{
		}
	}
}
