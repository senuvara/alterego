﻿using System;

namespace UnityEngine
{
	// Token: 0x02000349 RID: 841
	[Flags]
	public enum DrivenTransformProperties
	{
		// Token: 0x04000AA9 RID: 2729
		None = 0,
		// Token: 0x04000AAA RID: 2730
		All = -1,
		// Token: 0x04000AAB RID: 2731
		AnchoredPositionX = 2,
		// Token: 0x04000AAC RID: 2732
		AnchoredPositionY = 4,
		// Token: 0x04000AAD RID: 2733
		AnchoredPositionZ = 8,
		// Token: 0x04000AAE RID: 2734
		Rotation = 16,
		// Token: 0x04000AAF RID: 2735
		ScaleX = 32,
		// Token: 0x04000AB0 RID: 2736
		ScaleY = 64,
		// Token: 0x04000AB1 RID: 2737
		ScaleZ = 128,
		// Token: 0x04000AB2 RID: 2738
		AnchorMinX = 256,
		// Token: 0x04000AB3 RID: 2739
		AnchorMinY = 512,
		// Token: 0x04000AB4 RID: 2740
		AnchorMaxX = 1024,
		// Token: 0x04000AB5 RID: 2741
		AnchorMaxY = 2048,
		// Token: 0x04000AB6 RID: 2742
		SizeDeltaX = 4096,
		// Token: 0x04000AB7 RID: 2743
		SizeDeltaY = 8192,
		// Token: 0x04000AB8 RID: 2744
		PivotX = 16384,
		// Token: 0x04000AB9 RID: 2745
		PivotY = 32768,
		// Token: 0x04000ABA RID: 2746
		AnchoredPosition = 6,
		// Token: 0x04000ABB RID: 2747
		AnchoredPosition3D = 14,
		// Token: 0x04000ABC RID: 2748
		Scale = 224,
		// Token: 0x04000ABD RID: 2749
		AnchorMin = 768,
		// Token: 0x04000ABE RID: 2750
		AnchorMax = 3072,
		// Token: 0x04000ABF RID: 2751
		Anchors = 3840,
		// Token: 0x04000AC0 RID: 2752
		SizeDelta = 12288,
		// Token: 0x04000AC1 RID: 2753
		Pivot = 49152
	}
}
