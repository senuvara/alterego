﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Internal;
using UnityEngine.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200022E RID: 558
	[NativeHeader("Runtime/Graphics/RenderTexture.h")]
	[NativeHeader("Runtime/Camera/Camera.h")]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Graphics/GraphicsScriptBindings.h")]
	[NativeHeader("Runtime/Graphics/RenderBufferManager.h")]
	public class RenderTexture : Texture
	{
		// Token: 0x060013A7 RID: 5031 RVA: 0x00022018 File Offset: 0x00020218
		[RequiredByNativeCode]
		protected internal RenderTexture()
		{
		}

		// Token: 0x060013A8 RID: 5032 RVA: 0x00022021 File Offset: 0x00020221
		public RenderTexture(RenderTextureDescriptor desc)
		{
			RenderTexture.ValidateRenderTextureDesc(desc);
			RenderTexture.Internal_Create(this);
			this.SetRenderTextureDescriptor(desc);
		}

		// Token: 0x060013A9 RID: 5033 RVA: 0x0002203D File Offset: 0x0002023D
		public RenderTexture(RenderTexture textureToCopy)
		{
			if (textureToCopy == null)
			{
				throw new ArgumentNullException("textureToCopy");
			}
			RenderTexture.ValidateRenderTextureDesc(textureToCopy.descriptor);
			RenderTexture.Internal_Create(this);
			this.SetRenderTextureDescriptor(textureToCopy.descriptor);
		}

		// Token: 0x060013AA RID: 5034 RVA: 0x0002207C File Offset: 0x0002027C
		public RenderTexture(int width, int height, int depth, GraphicsFormat format)
		{
			if (base.ValidateFormat(format, FormatUsage.Render))
			{
				RenderTexture.Internal_Create(this);
				this.width = width;
				this.height = height;
				this.depth = depth;
				this.format = GraphicsFormatUtility.GetRenderTextureFormat(format);
				this.SetSRGBReadWrite(GraphicsFormatUtility.IsSRGBFormat(format));
			}
		}

		// Token: 0x060013AB RID: 5035 RVA: 0x000220D8 File Offset: 0x000202D8
		public RenderTexture(int width, int height, int depth, [UnityEngine.Internal.DefaultValue("RenderTextureFormat.Default")] RenderTextureFormat format, [UnityEngine.Internal.DefaultValue("RenderTextureReadWrite.Default")] RenderTextureReadWrite readWrite)
		{
			if (base.ValidateFormat(format))
			{
				RenderTexture.Internal_Create(this);
				this.width = width;
				this.height = height;
				this.depth = depth;
				this.format = format;
				bool flag = QualitySettings.activeColorSpace == ColorSpace.Linear;
				this.SetSRGBReadWrite((readWrite != RenderTextureReadWrite.Default) ? (readWrite == RenderTextureReadWrite.sRGB) : flag);
			}
		}

		// Token: 0x060013AC RID: 5036 RVA: 0x00022142 File Offset: 0x00020342
		[ExcludeFromDocs]
		public RenderTexture(int width, int height, int depth, RenderTextureFormat format) : this(width, height, depth, format, RenderTextureReadWrite.Default)
		{
		}

		// Token: 0x060013AD RID: 5037 RVA: 0x00022151 File Offset: 0x00020351
		[ExcludeFromDocs]
		public RenderTexture(int width, int height, int depth) : this(width, height, depth, RenderTextureFormat.Default, RenderTextureReadWrite.Default)
		{
		}

		// Token: 0x170003BD RID: 957
		// (get) Token: 0x060013AE RID: 5038
		// (set) Token: 0x060013AF RID: 5039
		public override extern int width { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003BE RID: 958
		// (get) Token: 0x060013B0 RID: 5040
		// (set) Token: 0x060013B1 RID: 5041
		public override extern int height { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003BF RID: 959
		// (get) Token: 0x060013B2 RID: 5042
		// (set) Token: 0x060013B3 RID: 5043
		public override extern TextureDimension dimension { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003C0 RID: 960
		// (get) Token: 0x060013B4 RID: 5044
		// (set) Token: 0x060013B5 RID: 5045
		[NativeProperty("MipMap")]
		public extern bool useMipMap { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003C1 RID: 961
		// (get) Token: 0x060013B6 RID: 5046
		[NativeProperty("SRGBReadWrite")]
		public extern bool sRGB { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003C2 RID: 962
		// (get) Token: 0x060013B7 RID: 5047
		// (set) Token: 0x060013B8 RID: 5048
		[NativeProperty("ColorFormat")]
		public extern RenderTextureFormat format { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003C3 RID: 963
		// (get) Token: 0x060013B9 RID: 5049
		// (set) Token: 0x060013BA RID: 5050
		[NativeProperty("VRUsage")]
		public extern VRTextureUsage vrUsage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003C4 RID: 964
		// (get) Token: 0x060013BB RID: 5051
		// (set) Token: 0x060013BC RID: 5052
		[NativeProperty("Memoryless")]
		public extern RenderTextureMemoryless memorylessMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003C5 RID: 965
		// (get) Token: 0x060013BD RID: 5053
		// (set) Token: 0x060013BE RID: 5054
		public extern bool autoGenerateMips { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003C6 RID: 966
		// (get) Token: 0x060013BF RID: 5055
		// (set) Token: 0x060013C0 RID: 5056
		public extern int volumeDepth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003C7 RID: 967
		// (get) Token: 0x060013C1 RID: 5057
		// (set) Token: 0x060013C2 RID: 5058
		public extern int antiAliasing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003C8 RID: 968
		// (get) Token: 0x060013C3 RID: 5059
		// (set) Token: 0x060013C4 RID: 5060
		public extern bool bindTextureMS { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003C9 RID: 969
		// (get) Token: 0x060013C5 RID: 5061
		// (set) Token: 0x060013C6 RID: 5062
		public extern bool enableRandomWrite { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003CA RID: 970
		// (get) Token: 0x060013C7 RID: 5063
		// (set) Token: 0x060013C8 RID: 5064
		public extern bool useDynamicScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060013C9 RID: 5065
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool GetIsPowerOfTwo();

		// Token: 0x170003CB RID: 971
		// (get) Token: 0x060013CA RID: 5066 RVA: 0x00022160 File Offset: 0x00020360
		// (set) Token: 0x060013CB RID: 5067 RVA: 0x00007476 File Offset: 0x00005676
		public bool isPowerOfTwo
		{
			get
			{
				return this.GetIsPowerOfTwo();
			}
			set
			{
			}
		}

		// Token: 0x060013CC RID: 5068
		[FreeFunction("RenderTexture::GetActive")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RenderTexture GetActive();

		// Token: 0x060013CD RID: 5069
		[FreeFunction("RenderTextureScripting::SetActive")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetActive(RenderTexture rt);

		// Token: 0x170003CC RID: 972
		// (get) Token: 0x060013CE RID: 5070 RVA: 0x0002217C File Offset: 0x0002037C
		// (set) Token: 0x060013CF RID: 5071 RVA: 0x00022196 File Offset: 0x00020396
		public static RenderTexture active
		{
			get
			{
				return RenderTexture.GetActive();
			}
			set
			{
				RenderTexture.SetActive(value);
			}
		}

		// Token: 0x060013D0 RID: 5072 RVA: 0x000221A0 File Offset: 0x000203A0
		[FreeFunction(Name = "RenderTextureScripting::GetColorBuffer", HasExplicitThis = true)]
		private RenderBuffer GetColorBuffer()
		{
			RenderBuffer result;
			this.GetColorBuffer_Injected(out result);
			return result;
		}

		// Token: 0x060013D1 RID: 5073 RVA: 0x000221B8 File Offset: 0x000203B8
		[FreeFunction(Name = "RenderTextureScripting::GetDepthBuffer", HasExplicitThis = true)]
		private RenderBuffer GetDepthBuffer()
		{
			RenderBuffer result;
			this.GetDepthBuffer_Injected(out result);
			return result;
		}

		// Token: 0x170003CD RID: 973
		// (get) Token: 0x060013D2 RID: 5074 RVA: 0x000221D0 File Offset: 0x000203D0
		public RenderBuffer colorBuffer
		{
			get
			{
				return this.GetColorBuffer();
			}
		}

		// Token: 0x170003CE RID: 974
		// (get) Token: 0x060013D3 RID: 5075 RVA: 0x000221EC File Offset: 0x000203EC
		public RenderBuffer depthBuffer
		{
			get
			{
				return this.GetDepthBuffer();
			}
		}

		// Token: 0x060013D4 RID: 5076
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern IntPtr GetNativeDepthBufferPtr();

		// Token: 0x060013D5 RID: 5077
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void DiscardContents(bool discardColor, bool discardDepth);

		// Token: 0x060013D6 RID: 5078
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void MarkRestoreExpected();

		// Token: 0x060013D7 RID: 5079 RVA: 0x00022207 File Offset: 0x00020407
		public void DiscardContents()
		{
			this.DiscardContents(true, true);
		}

		// Token: 0x060013D8 RID: 5080
		[NativeName("ResolveAntiAliasedSurface")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ResolveAA();

		// Token: 0x060013D9 RID: 5081
		[NativeName("ResolveAntiAliasedSurface")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ResolveAATo(RenderTexture rt);

		// Token: 0x060013DA RID: 5082 RVA: 0x00022212 File Offset: 0x00020412
		public void ResolveAntiAliasedSurface()
		{
			this.ResolveAA();
		}

		// Token: 0x060013DB RID: 5083 RVA: 0x0002221B File Offset: 0x0002041B
		public void ResolveAntiAliasedSurface(RenderTexture target)
		{
			this.ResolveAATo(target);
		}

		// Token: 0x060013DC RID: 5084
		[FreeFunction(Name = "RenderTextureScripting::SetGlobalShaderProperty", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetGlobalShaderProperty(string propertyName);

		// Token: 0x060013DD RID: 5085
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool Create();

		// Token: 0x060013DE RID: 5086
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Release();

		// Token: 0x060013DF RID: 5087
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsCreated();

		// Token: 0x060013E0 RID: 5088
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GenerateMips();

		// Token: 0x060013E1 RID: 5089
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ConvertToEquirect(RenderTexture equirect, Camera.MonoOrStereoscopicEye eye = Camera.MonoOrStereoscopicEye.Mono);

		// Token: 0x060013E2 RID: 5090
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetSRGBReadWrite(bool srgb);

		// Token: 0x060013E3 RID: 5091
		[FreeFunction("RenderTextureScripting::Create")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] RenderTexture rt);

		// Token: 0x060013E4 RID: 5092
		[FreeFunction("RenderTextureSupportsStencil")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SupportsStencil(RenderTexture rt);

		// Token: 0x060013E5 RID: 5093 RVA: 0x00022225 File Offset: 0x00020425
		[NativeName("SetRenderTextureDescFromScript")]
		private void SetRenderTextureDescriptor(RenderTextureDescriptor desc)
		{
			this.SetRenderTextureDescriptor_Injected(ref desc);
		}

		// Token: 0x060013E6 RID: 5094 RVA: 0x00022230 File Offset: 0x00020430
		[NativeName("GetRenderTextureDesc")]
		private RenderTextureDescriptor GetDescriptor()
		{
			RenderTextureDescriptor result;
			this.GetDescriptor_Injected(out result);
			return result;
		}

		// Token: 0x060013E7 RID: 5095 RVA: 0x00022246 File Offset: 0x00020446
		[FreeFunction("GetRenderBufferManager().GetTextures().GetTempBuffer")]
		private static RenderTexture GetTemporary_Internal(RenderTextureDescriptor desc)
		{
			return RenderTexture.GetTemporary_Internal_Injected(ref desc);
		}

		// Token: 0x060013E8 RID: 5096
		[FreeFunction("GetRenderBufferManager().GetTextures().ReleaseTempBuffer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ReleaseTemporary(RenderTexture temp);

		// Token: 0x170003CF RID: 975
		// (get) Token: 0x060013E9 RID: 5097
		// (set) Token: 0x060013EA RID: 5098
		public extern int depth { [FreeFunction("RenderTextureScripting::GetDepth", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("RenderTextureScripting::SetDepth", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003D0 RID: 976
		// (get) Token: 0x060013EB RID: 5099 RVA: 0x00022250 File Offset: 0x00020450
		// (set) Token: 0x060013EC RID: 5100 RVA: 0x0002226B File Offset: 0x0002046B
		public RenderTextureDescriptor descriptor
		{
			get
			{
				return this.GetDescriptor();
			}
			set
			{
				RenderTexture.ValidateRenderTextureDesc(value);
				this.SetRenderTextureDescriptor(value);
			}
		}

		// Token: 0x060013ED RID: 5101 RVA: 0x0002227C File Offset: 0x0002047C
		private static void ValidateRenderTextureDesc(RenderTextureDescriptor desc)
		{
			if (desc.width <= 0)
			{
				throw new ArgumentException("RenderTextureDesc width must be greater than zero.", "desc.width");
			}
			if (desc.height <= 0)
			{
				throw new ArgumentException("RenderTextureDesc height must be greater than zero.", "desc.height");
			}
			if (desc.volumeDepth <= 0)
			{
				throw new ArgumentException("RenderTextureDesc volumeDepth must be greater than zero.", "desc.volumeDepth");
			}
			if (desc.msaaSamples != 1 && desc.msaaSamples != 2 && desc.msaaSamples != 4 && desc.msaaSamples != 8)
			{
				throw new ArgumentException("RenderTextureDesc msaaSamples must be 1, 2, 4, or 8.", "desc.msaaSamples");
			}
			if (desc.depthBufferBits != 0 && desc.depthBufferBits != 16 && desc.depthBufferBits != 24)
			{
				throw new ArgumentException("RenderTextureDesc depthBufferBits must be 0, 16, or 24.", "desc.depthBufferBits");
			}
		}

		// Token: 0x060013EE RID: 5102 RVA: 0x00022360 File Offset: 0x00020560
		public static RenderTexture GetTemporary(RenderTextureDescriptor desc)
		{
			RenderTexture.ValidateRenderTextureDesc(desc);
			desc.createdFromScript = true;
			return RenderTexture.GetTemporary_Internal(desc);
		}

		// Token: 0x060013EF RID: 5103 RVA: 0x0002238C File Offset: 0x0002058C
		private static RenderTexture GetTemporaryImpl(int width, int height, int depthBuffer = 0, RenderTextureFormat format = RenderTextureFormat.Default, RenderTextureReadWrite readWrite = RenderTextureReadWrite.Default, int antiAliasing = 1, RenderTextureMemoryless memorylessMode = RenderTextureMemoryless.None, VRTextureUsage vrUsage = VRTextureUsage.None, bool useDynamicScale = false)
		{
			return RenderTexture.GetTemporary(new RenderTextureDescriptor(width, height, format, depthBuffer)
			{
				sRGB = (readWrite != RenderTextureReadWrite.Linear),
				msaaSamples = antiAliasing,
				memoryless = memorylessMode,
				vrUsage = vrUsage,
				useDynamicScale = useDynamicScale
			});
		}

		// Token: 0x060013F0 RID: 5104 RVA: 0x000223E8 File Offset: 0x000205E8
		public static RenderTexture GetTemporary(int width, int height, [UnityEngine.Internal.DefaultValue("0")] int depthBuffer, [UnityEngine.Internal.DefaultValue("RenderTextureFormat.Default")] RenderTextureFormat format, [UnityEngine.Internal.DefaultValue("RenderTextureReadWrite.Default")] RenderTextureReadWrite readWrite, [UnityEngine.Internal.DefaultValue("1")] int antiAliasing, [UnityEngine.Internal.DefaultValue("RenderTextureMemoryless.None")] RenderTextureMemoryless memorylessMode, [UnityEngine.Internal.DefaultValue("VRTextureUsage.None")] VRTextureUsage vrUsage, [UnityEngine.Internal.DefaultValue("false")] bool useDynamicScale)
		{
			return RenderTexture.GetTemporaryImpl(width, height, depthBuffer, format, readWrite, antiAliasing, memorylessMode, vrUsage, useDynamicScale);
		}

		// Token: 0x060013F1 RID: 5105 RVA: 0x00022410 File Offset: 0x00020610
		[ExcludeFromDocs]
		public static RenderTexture GetTemporary(int width, int height, int depthBuffer, RenderTextureFormat format, RenderTextureReadWrite readWrite, int antiAliasing, RenderTextureMemoryless memorylessMode, VRTextureUsage vrUsage)
		{
			return RenderTexture.GetTemporaryImpl(width, height, depthBuffer, format, readWrite, antiAliasing, memorylessMode, vrUsage, false);
		}

		// Token: 0x060013F2 RID: 5106 RVA: 0x00022438 File Offset: 0x00020638
		[ExcludeFromDocs]
		public static RenderTexture GetTemporary(int width, int height, int depthBuffer, RenderTextureFormat format, RenderTextureReadWrite readWrite, int antiAliasing, RenderTextureMemoryless memorylessMode)
		{
			return RenderTexture.GetTemporaryImpl(width, height, depthBuffer, format, readWrite, antiAliasing, memorylessMode, VRTextureUsage.None, false);
		}

		// Token: 0x060013F3 RID: 5107 RVA: 0x00022460 File Offset: 0x00020660
		[ExcludeFromDocs]
		public static RenderTexture GetTemporary(int width, int height, int depthBuffer, RenderTextureFormat format, RenderTextureReadWrite readWrite, int antiAliasing)
		{
			return RenderTexture.GetTemporaryImpl(width, height, depthBuffer, format, readWrite, antiAliasing, RenderTextureMemoryless.None, VRTextureUsage.None, false);
		}

		// Token: 0x060013F4 RID: 5108 RVA: 0x00022488 File Offset: 0x00020688
		[ExcludeFromDocs]
		public static RenderTexture GetTemporary(int width, int height, int depthBuffer, RenderTextureFormat format, RenderTextureReadWrite readWrite)
		{
			return RenderTexture.GetTemporaryImpl(width, height, depthBuffer, format, readWrite, 1, RenderTextureMemoryless.None, VRTextureUsage.None, false);
		}

		// Token: 0x060013F5 RID: 5109 RVA: 0x000224AC File Offset: 0x000206AC
		[ExcludeFromDocs]
		public static RenderTexture GetTemporary(int width, int height, int depthBuffer, RenderTextureFormat format)
		{
			return RenderTexture.GetTemporaryImpl(width, height, depthBuffer, format, RenderTextureReadWrite.Default, 1, RenderTextureMemoryless.None, VRTextureUsage.None, false);
		}

		// Token: 0x060013F6 RID: 5110 RVA: 0x000224D0 File Offset: 0x000206D0
		[ExcludeFromDocs]
		public static RenderTexture GetTemporary(int width, int height, int depthBuffer)
		{
			return RenderTexture.GetTemporaryImpl(width, height, depthBuffer, RenderTextureFormat.Default, RenderTextureReadWrite.Default, 1, RenderTextureMemoryless.None, VRTextureUsage.None, false);
		}

		// Token: 0x060013F7 RID: 5111 RVA: 0x000224F4 File Offset: 0x000206F4
		[ExcludeFromDocs]
		public static RenderTexture GetTemporary(int width, int height)
		{
			return RenderTexture.GetTemporaryImpl(width, height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Default, 1, RenderTextureMemoryless.None, VRTextureUsage.None, false);
		}

		// Token: 0x170003D1 RID: 977
		// (get) Token: 0x060013F8 RID: 5112 RVA: 0x00022518 File Offset: 0x00020718
		// (set) Token: 0x060013F9 RID: 5113 RVA: 0x00022536 File Offset: 0x00020736
		[Obsolete("Use RenderTexture.dimension instead.", false)]
		public bool isCubemap
		{
			get
			{
				return this.dimension == TextureDimension.Cube;
			}
			set
			{
				this.dimension = ((!value) ? TextureDimension.Tex2D : TextureDimension.Cube);
			}
		}

		// Token: 0x170003D2 RID: 978
		// (get) Token: 0x060013FA RID: 5114 RVA: 0x0002254C File Offset: 0x0002074C
		// (set) Token: 0x060013FB RID: 5115 RVA: 0x0002256A File Offset: 0x0002076A
		[Obsolete("Use RenderTexture.dimension instead.", false)]
		public bool isVolume
		{
			get
			{
				return this.dimension == TextureDimension.Tex3D;
			}
			set
			{
				this.dimension = ((!value) ? TextureDimension.Tex2D : TextureDimension.Tex3D);
			}
		}

		// Token: 0x170003D3 RID: 979
		// (get) Token: 0x060013FC RID: 5116 RVA: 0x00022580 File Offset: 0x00020780
		// (set) Token: 0x060013FD RID: 5117 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("RenderTexture.enabled is always now, no need to use it.", false)]
		public static bool enabled
		{
			get
			{
				return true;
			}
			set
			{
			}
		}

		// Token: 0x060013FE RID: 5118 RVA: 0x00022598 File Offset: 0x00020798
		[Obsolete("GetTexelOffset always returns zero now, no point in using it.", false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public Vector2 GetTexelOffset()
		{
			return Vector2.zero;
		}

		// Token: 0x060013FF RID: 5119
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetColorBuffer_Injected(out RenderBuffer ret);

		// Token: 0x06001400 RID: 5120
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetDepthBuffer_Injected(out RenderBuffer ret);

		// Token: 0x06001401 RID: 5121
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetRenderTextureDescriptor_Injected(ref RenderTextureDescriptor desc);

		// Token: 0x06001402 RID: 5122
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetDescriptor_Injected(out RenderTextureDescriptor ret);

		// Token: 0x06001403 RID: 5123
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RenderTexture GetTemporary_Internal_Injected(ref RenderTextureDescriptor desc);
	}
}
