﻿using System;

namespace UnityEngine
{
	// Token: 0x02000036 RID: 54
	public enum PrimitiveType
	{
		// Token: 0x04000081 RID: 129
		Sphere,
		// Token: 0x04000082 RID: 130
		Capsule,
		// Token: 0x04000083 RID: 131
		Cylinder,
		// Token: 0x04000084 RID: 132
		Cube,
		// Token: 0x04000085 RID: 133
		Plane,
		// Token: 0x04000086 RID: 134
		Quad
	}
}
