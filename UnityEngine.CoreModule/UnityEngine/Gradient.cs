﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200007F RID: 127
	[NativeHeader("Runtime/Export/Gradient.bindings.h")]
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class Gradient : IEquatable<Gradient>
	{
		// Token: 0x0600065C RID: 1628 RVA: 0x0000E54C File Offset: 0x0000C74C
		[RequiredByNativeCode]
		public Gradient()
		{
			this.m_Ptr = Gradient.Init();
		}

		// Token: 0x0600065D RID: 1629
		[FreeFunction(Name = "Gradient_Bindings::Init", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Init();

		// Token: 0x0600065E RID: 1630
		[FreeFunction(Name = "Gradient_Bindings::Cleanup", IsThreadSafe = true, HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Cleanup();

		// Token: 0x0600065F RID: 1631
		[FreeFunction("Gradient_Bindings::Internal_Equals", IsThreadSafe = true, HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool Internal_Equals(IntPtr other);

		// Token: 0x06000660 RID: 1632 RVA: 0x0000E560 File Offset: 0x0000C760
		~Gradient()
		{
			this.Cleanup();
		}

		// Token: 0x06000661 RID: 1633 RVA: 0x0000E590 File Offset: 0x0000C790
		[FreeFunction(Name = "Gradient_Bindings::Evaluate", IsThreadSafe = true, HasExplicitThis = true)]
		public Color Evaluate(float time)
		{
			Color result;
			this.Evaluate_Injected(time, out result);
			return result;
		}

		// Token: 0x17000114 RID: 276
		// (get) Token: 0x06000662 RID: 1634
		// (set) Token: 0x06000663 RID: 1635
		public extern GradientColorKey[] colorKeys { [FreeFunction("Gradient_Bindings::GetColorKeys", IsThreadSafe = true, HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("Gradient_Bindings::SetColorKeys", IsThreadSafe = true, HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x06000664 RID: 1636
		// (set) Token: 0x06000665 RID: 1637
		public extern GradientAlphaKey[] alphaKeys { [FreeFunction("Gradient_Bindings::GetAlphaKeys", IsThreadSafe = true, HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("Gradient_Bindings::SetAlphaKeys", IsThreadSafe = true, HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000116 RID: 278
		// (get) Token: 0x06000666 RID: 1638
		// (set) Token: 0x06000667 RID: 1639
		public extern GradientMode mode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000668 RID: 1640
		[FreeFunction(Name = "Gradient_Bindings::SetKeys", IsThreadSafe = true, HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetKeys(GradientColorKey[] colorKeys, GradientAlphaKey[] alphaKeys);

		// Token: 0x06000669 RID: 1641 RVA: 0x0000E5A8 File Offset: 0x0000C7A8
		public override bool Equals(object o)
		{
			return !object.ReferenceEquals(null, o) && (object.ReferenceEquals(this, o) || (o.GetType() == base.GetType() && this.Equals((Gradient)o)));
		}

		// Token: 0x0600066A RID: 1642 RVA: 0x0000E60C File Offset: 0x0000C80C
		public bool Equals(Gradient other)
		{
			return !object.ReferenceEquals(null, other) && (object.ReferenceEquals(this, other) || this.m_Ptr.Equals(other.m_Ptr) || this.Internal_Equals(other.m_Ptr));
		}

		// Token: 0x0600066B RID: 1643 RVA: 0x0000E680 File Offset: 0x0000C880
		public override int GetHashCode()
		{
			return this.m_Ptr.GetHashCode();
		}

		// Token: 0x0600066C RID: 1644
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Evaluate_Injected(float time, out Color ret);

		// Token: 0x04000158 RID: 344
		internal IntPtr m_Ptr;
	}
}
