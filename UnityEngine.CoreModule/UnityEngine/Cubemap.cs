﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000229 RID: 553
	[ExcludeFromPreset]
	[NativeHeader("Runtime/Graphics/CubemapTexture.h")]
	public sealed class Cubemap : Texture
	{
		// Token: 0x06001349 RID: 4937 RVA: 0x00021A40 File Offset: 0x0001FC40
		[RequiredByNativeCode]
		public Cubemap(int width, GraphicsFormat format, TextureCreationFlags flags)
		{
			if (base.ValidateFormat(format, FormatUsage.Sample))
			{
				Cubemap.Internal_Create(this, width, format, flags, IntPtr.Zero);
			}
		}

		// Token: 0x0600134A RID: 4938 RVA: 0x00021A64 File Offset: 0x0001FC64
		internal Cubemap(int width, TextureFormat textureFormat, bool mipChain, IntPtr nativeTex)
		{
			if (base.ValidateFormat(textureFormat))
			{
				GraphicsFormat graphicsFormat = GraphicsFormatUtility.GetGraphicsFormat(textureFormat, false);
				TextureCreationFlags textureCreationFlags = TextureCreationFlags.None;
				if (mipChain)
				{
					textureCreationFlags |= TextureCreationFlags.MipChain;
				}
				if (GraphicsFormatUtility.IsCrunchFormat(textureFormat))
				{
					textureCreationFlags |= TextureCreationFlags.Crunch;
				}
				Cubemap.Internal_Create(this, width, graphicsFormat, textureCreationFlags, nativeTex);
			}
		}

		// Token: 0x0600134B RID: 4939 RVA: 0x00021AB8 File Offset: 0x0001FCB8
		public Cubemap(int width, TextureFormat textureFormat, bool mipChain) : this(width, textureFormat, mipChain, IntPtr.Zero)
		{
		}

		// Token: 0x170003AE RID: 942
		// (get) Token: 0x0600134C RID: 4940
		public extern int mipmapCount { [NativeName("CountDataMipmaps")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003AF RID: 943
		// (get) Token: 0x0600134D RID: 4941
		public extern TextureFormat format { [NativeName("GetTextureFormat")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600134E RID: 4942
		[FreeFunction("CubemapScripting::Create")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_CreateImpl([Writable] Cubemap mono, int ext, GraphicsFormat format, TextureCreationFlags flags, IntPtr nativeTex);

		// Token: 0x0600134F RID: 4943 RVA: 0x00021AC9 File Offset: 0x0001FCC9
		private static void Internal_Create([Writable] Cubemap mono, int ext, GraphicsFormat format, TextureCreationFlags flags, IntPtr nativeTex)
		{
			if (!Cubemap.Internal_CreateImpl(mono, ext, format, flags, nativeTex))
			{
				throw new UnityException("Failed to create texture because of invalid parameters.");
			}
		}

		// Token: 0x06001350 RID: 4944
		[FreeFunction(Name = "CubemapScripting::Apply", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ApplyImpl(bool updateMipmaps, bool makeNoLongerReadable);

		// Token: 0x170003B0 RID: 944
		// (get) Token: 0x06001351 RID: 4945
		public override extern bool isReadable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001352 RID: 4946 RVA: 0x00021AE7 File Offset: 0x0001FCE7
		[NativeName("SetPixel")]
		private void SetPixelImpl(int image, int x, int y, Color color)
		{
			this.SetPixelImpl_Injected(image, x, y, ref color);
		}

		// Token: 0x06001353 RID: 4947 RVA: 0x00021AF4 File Offset: 0x0001FCF4
		[NativeName("GetPixel")]
		private Color GetPixelImpl(int image, int x, int y)
		{
			Color result;
			this.GetPixelImpl_Injected(image, x, y, out result);
			return result;
		}

		// Token: 0x06001354 RID: 4948
		[NativeName("FixupEdges")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SmoothEdges([DefaultValue("1")] int smoothRegionWidthInPixels);

		// Token: 0x06001355 RID: 4949 RVA: 0x00021B0D File Offset: 0x0001FD0D
		public void SmoothEdges()
		{
			this.SmoothEdges(1);
		}

		// Token: 0x06001356 RID: 4950
		[FreeFunction(Name = "CubemapScripting::GetPixels", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color[] GetPixels(CubemapFace face, int miplevel);

		// Token: 0x06001357 RID: 4951 RVA: 0x00021B18 File Offset: 0x0001FD18
		public Color[] GetPixels(CubemapFace face)
		{
			return this.GetPixels(face, 0);
		}

		// Token: 0x06001358 RID: 4952
		[FreeFunction(Name = "CubemapScripting::SetPixels", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels(Color[] colors, CubemapFace face, int miplevel);

		// Token: 0x06001359 RID: 4953 RVA: 0x00021B35 File Offset: 0x0001FD35
		public void SetPixels(Color[] colors, CubemapFace face)
		{
			this.SetPixels(colors, face, 0);
		}

		// Token: 0x0600135A RID: 4954 RVA: 0x00021B44 File Offset: 0x0001FD44
		public static Cubemap CreateExternalTexture(int width, TextureFormat format, bool mipmap, IntPtr nativeTex)
		{
			if (nativeTex == IntPtr.Zero)
			{
				throw new ArgumentException("nativeTex can not be null");
			}
			return new Cubemap(width, format, mipmap, nativeTex);
		}

		// Token: 0x0600135B RID: 4955 RVA: 0x00021B7D File Offset: 0x0001FD7D
		public void SetPixel(CubemapFace face, int x, int y, Color color)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			this.SetPixelImpl((int)face, x, y, color);
		}

		// Token: 0x0600135C RID: 4956 RVA: 0x00021BA0 File Offset: 0x0001FDA0
		public Color GetPixel(CubemapFace face, int x, int y)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			return this.GetPixelImpl((int)face, x, y);
		}

		// Token: 0x0600135D RID: 4957 RVA: 0x00021BD1 File Offset: 0x0001FDD1
		public void Apply([DefaultValue("true")] bool updateMipmaps, [DefaultValue("false")] bool makeNoLongerReadable)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			this.ApplyImpl(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x0600135E RID: 4958 RVA: 0x00021BEF File Offset: 0x0001FDEF
		public void Apply(bool updateMipmaps)
		{
			this.Apply(updateMipmaps, false);
		}

		// Token: 0x0600135F RID: 4959 RVA: 0x00021BFA File Offset: 0x0001FDFA
		public void Apply()
		{
			this.Apply(true, false);
		}

		// Token: 0x06001360 RID: 4960
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetPixelImpl_Injected(int image, int x, int y, ref Color color);

		// Token: 0x06001361 RID: 4961
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetPixelImpl_Injected(int image, int x, int y, out Color ret);
	}
}
