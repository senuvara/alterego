﻿using System;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000027 RID: 39
	internal class AttributeHelperEngine
	{
		// Token: 0x06000272 RID: 626 RVA: 0x00002370 File Offset: 0x00000570
		public AttributeHelperEngine()
		{
		}

		// Token: 0x06000273 RID: 627 RVA: 0x000086D8 File Offset: 0x000068D8
		[RequiredByNativeCode]
		private static Type GetParentTypeDisallowingMultipleInclusion(Type type)
		{
			Type result = null;
			while (type != null && type != typeof(MonoBehaviour))
			{
				if (Attribute.IsDefined(type, typeof(DisallowMultipleComponent)))
				{
					result = type;
				}
				type = type.BaseType;
			}
			return result;
		}

		// Token: 0x06000274 RID: 628 RVA: 0x0000872C File Offset: 0x0000692C
		[RequiredByNativeCode]
		private static Type[] GetRequiredComponents(Type klass)
		{
			List<Type> list = null;
			while (klass != null && klass != typeof(MonoBehaviour))
			{
				RequireComponent[] array = (RequireComponent[])klass.GetCustomAttributes(typeof(RequireComponent), false);
				Type baseType = klass.BaseType;
				foreach (RequireComponent requireComponent in array)
				{
					if (list == null && array.Length == 1 && baseType == typeof(MonoBehaviour))
					{
						return new Type[]
						{
							requireComponent.m_Type0,
							requireComponent.m_Type1,
							requireComponent.m_Type2
						};
					}
					if (list == null)
					{
						list = new List<Type>();
					}
					if (requireComponent.m_Type0 != null)
					{
						list.Add(requireComponent.m_Type0);
					}
					if (requireComponent.m_Type1 != null)
					{
						list.Add(requireComponent.m_Type1);
					}
					if (requireComponent.m_Type2 != null)
					{
						list.Add(requireComponent.m_Type2);
					}
				}
				klass = baseType;
			}
			if (list == null)
			{
				return null;
			}
			return list.ToArray();
		}

		// Token: 0x06000275 RID: 629 RVA: 0x0000885C File Offset: 0x00006A5C
		private static int GetExecuteMode(Type klass)
		{
			object[] customAttributes = klass.GetCustomAttributes(false);
			object[] array = customAttributes;
			int i = 0;
			while (i < array.Length)
			{
				object obj = array[i];
				int result;
				if (obj is ExecuteAlways)
				{
					result = 2;
				}
				else
				{
					if (!(obj is ExecuteInEditMode))
					{
						i++;
						continue;
					}
					result = 1;
				}
				return result;
			}
			return 0;
		}

		// Token: 0x06000276 RID: 630 RVA: 0x000088C0 File Offset: 0x00006AC0
		[RequiredByNativeCode]
		private static int CheckIsEditorScript(Type klass)
		{
			while (klass != null && klass != typeof(MonoBehaviour))
			{
				int executeMode = AttributeHelperEngine.GetExecuteMode(klass);
				if (executeMode > 0)
				{
					return executeMode;
				}
				klass = klass.BaseType;
			}
			return 0;
		}

		// Token: 0x06000277 RID: 631 RVA: 0x00008910 File Offset: 0x00006B10
		[RequiredByNativeCode]
		private static int GetDefaultExecutionOrderFor(Type klass)
		{
			DefaultExecutionOrder customAttributeOfType = AttributeHelperEngine.GetCustomAttributeOfType<DefaultExecutionOrder>(klass);
			int result;
			if (customAttributeOfType == null)
			{
				result = 0;
			}
			else
			{
				result = customAttributeOfType.order;
			}
			return result;
		}

		// Token: 0x06000278 RID: 632 RVA: 0x00008940 File Offset: 0x00006B40
		private static T GetCustomAttributeOfType<T>(Type klass) where T : Attribute
		{
			Type typeFromHandle = typeof(T);
			object[] customAttributes = klass.GetCustomAttributes(typeFromHandle, true);
			T result;
			if (customAttributes != null && customAttributes.Length != 0)
			{
				result = (T)((object)customAttributes[0]);
			}
			else
			{
				result = (T)((object)null);
			}
			return result;
		}
	}
}
