﻿using System;

namespace UnityEngine
{
	// Token: 0x02000122 RID: 290
	public enum IMECompositionMode
	{
		// Token: 0x0400051E RID: 1310
		Auto,
		// Token: 0x0400051F RID: 1311
		On,
		// Token: 0x04000520 RID: 1312
		Off
	}
}
