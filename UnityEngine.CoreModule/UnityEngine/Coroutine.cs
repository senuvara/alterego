﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200005A RID: 90
	[NativeHeader("Runtime/Mono/Coroutine.h")]
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class Coroutine : YieldInstruction
	{
		// Token: 0x060004ED RID: 1261 RVA: 0x0000C820 File Offset: 0x0000AA20
		private Coroutine()
		{
		}

		// Token: 0x060004EE RID: 1262 RVA: 0x0000C82C File Offset: 0x0000AA2C
		~Coroutine()
		{
			Coroutine.ReleaseCoroutine(this.m_Ptr);
		}

		// Token: 0x060004EF RID: 1263
		[FreeFunction("Coroutine::CleanupCoroutineGC", true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ReleaseCoroutine(IntPtr ptr);

		// Token: 0x04000118 RID: 280
		internal IntPtr m_Ptr;
	}
}
