﻿using System;

namespace UnityEngine
{
	// Token: 0x02000081 RID: 129
	public enum FullScreenMode
	{
		// Token: 0x0400015F RID: 351
		ExclusiveFullScreen,
		// Token: 0x04000160 RID: 352
		FullScreenWindow,
		// Token: 0x04000161 RID: 353
		MaximizedWindow,
		// Token: 0x04000162 RID: 354
		Windowed
	}
}
