﻿using System;

namespace UnityEngine
{
	// Token: 0x02000080 RID: 128
	internal enum EnabledOrientation
	{
		// Token: 0x0400015A RID: 346
		kAutorotateToPortrait = 1,
		// Token: 0x0400015B RID: 347
		kAutorotateToPortraitUpsideDown,
		// Token: 0x0400015C RID: 348
		kAutorotateToLandscapeLeft = 4,
		// Token: 0x0400015D RID: 349
		kAutorotateToLandscapeRight = 8
	}
}
