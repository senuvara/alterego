﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000070 RID: 112
	[UsedByNativeCode(Name = "ExposedReference")]
	[Serializable]
	public struct ExposedReference<T> where T : Object
	{
		// Token: 0x0600059B RID: 1435 RVA: 0x0000D5F4 File Offset: 0x0000B7F4
		public T Resolve(IExposedPropertyTable resolver)
		{
			if (resolver != null)
			{
				bool flag;
				Object referenceValue = resolver.GetReferenceValue(this.exposedName, out flag);
				if (flag)
				{
					return referenceValue as T;
				}
			}
			return this.defaultValue as T;
		}

		// Token: 0x04000148 RID: 328
		[SerializeField]
		public PropertyName exposedName;

		// Token: 0x04000149 RID: 329
		[SerializeField]
		public Object defaultValue;
	}
}
