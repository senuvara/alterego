﻿using System;

namespace UnityEngine
{
	// Token: 0x020001ED RID: 493
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class MultilineAttribute : PropertyAttribute
	{
		// Token: 0x06000F85 RID: 3973 RVA: 0x0001AD51 File Offset: 0x00018F51
		public MultilineAttribute()
		{
			this.lines = 3;
		}

		// Token: 0x06000F86 RID: 3974 RVA: 0x0001AD61 File Offset: 0x00018F61
		public MultilineAttribute(int lines)
		{
			this.lines = lines;
		}

		// Token: 0x04000725 RID: 1829
		public readonly int lines;
	}
}
