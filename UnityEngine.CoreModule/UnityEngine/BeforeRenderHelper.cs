﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace UnityEngine
{
	// Token: 0x0200003D RID: 61
	internal static class BeforeRenderHelper
	{
		// Token: 0x06000298 RID: 664 RVA: 0x00008B78 File Offset: 0x00006D78
		private static int GetUpdateOrder(UnityAction callback)
		{
			object[] customAttributes = callback.Method.GetCustomAttributes(typeof(BeforeRenderOrderAttribute), true);
			BeforeRenderOrderAttribute beforeRenderOrderAttribute = (customAttributes == null || customAttributes.Length <= 0) ? null : (customAttributes[0] as BeforeRenderOrderAttribute);
			return (beforeRenderOrderAttribute == null) ? 0 : beforeRenderOrderAttribute.order;
		}

		// Token: 0x06000299 RID: 665 RVA: 0x00008BD4 File Offset: 0x00006DD4
		public static void RegisterCallback(UnityAction callback)
		{
			int updateOrder = BeforeRenderHelper.GetUpdateOrder(callback);
			object obj = BeforeRenderHelper.s_OrderBlocks;
			lock (obj)
			{
				int num = 0;
				while (num < BeforeRenderHelper.s_OrderBlocks.Count && BeforeRenderHelper.s_OrderBlocks[num].order <= updateOrder)
				{
					if (BeforeRenderHelper.s_OrderBlocks[num].order == updateOrder)
					{
						BeforeRenderHelper.OrderBlock value = BeforeRenderHelper.s_OrderBlocks[num];
						value.callback = (UnityAction)Delegate.Combine(value.callback, callback);
						BeforeRenderHelper.s_OrderBlocks[num] = value;
						return;
					}
					num++;
				}
				BeforeRenderHelper.OrderBlock item = default(BeforeRenderHelper.OrderBlock);
				item.order = updateOrder;
				item.callback = (UnityAction)Delegate.Combine(item.callback, callback);
				BeforeRenderHelper.s_OrderBlocks.Insert(num, item);
			}
		}

		// Token: 0x0600029A RID: 666 RVA: 0x00008CD0 File Offset: 0x00006ED0
		public static void UnregisterCallback(UnityAction callback)
		{
			int updateOrder = BeforeRenderHelper.GetUpdateOrder(callback);
			object obj = BeforeRenderHelper.s_OrderBlocks;
			lock (obj)
			{
				int num = 0;
				while (num < BeforeRenderHelper.s_OrderBlocks.Count && BeforeRenderHelper.s_OrderBlocks[num].order <= updateOrder)
				{
					if (BeforeRenderHelper.s_OrderBlocks[num].order == updateOrder)
					{
						BeforeRenderHelper.OrderBlock value = BeforeRenderHelper.s_OrderBlocks[num];
						value.callback = (UnityAction)Delegate.Remove(value.callback, callback);
						BeforeRenderHelper.s_OrderBlocks[num] = value;
						if (value.callback == null)
						{
							BeforeRenderHelper.s_OrderBlocks.RemoveAt(num);
						}
						break;
					}
					num++;
				}
			}
		}

		// Token: 0x0600029B RID: 667 RVA: 0x00008DB0 File Offset: 0x00006FB0
		public static void Invoke()
		{
			object obj = BeforeRenderHelper.s_OrderBlocks;
			lock (obj)
			{
				for (int i = 0; i < BeforeRenderHelper.s_OrderBlocks.Count; i++)
				{
					UnityAction callback = BeforeRenderHelper.s_OrderBlocks[i].callback;
					if (callback != null)
					{
						callback();
					}
				}
			}
		}

		// Token: 0x0600029C RID: 668 RVA: 0x00008E28 File Offset: 0x00007028
		// Note: this type is marked as 'beforefieldinit'.
		static BeforeRenderHelper()
		{
		}

		// Token: 0x040000E5 RID: 229
		private static List<BeforeRenderHelper.OrderBlock> s_OrderBlocks = new List<BeforeRenderHelper.OrderBlock>();

		// Token: 0x0200003E RID: 62
		private struct OrderBlock
		{
			// Token: 0x040000E6 RID: 230
			internal int order;

			// Token: 0x040000E7 RID: 231
			internal UnityAction callback;
		}
	}
}
