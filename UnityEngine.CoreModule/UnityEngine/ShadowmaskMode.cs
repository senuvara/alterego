﻿using System;

namespace UnityEngine
{
	// Token: 0x020000BE RID: 190
	public enum ShadowmaskMode
	{
		// Token: 0x040001FB RID: 507
		Shadowmask,
		// Token: 0x040001FC RID: 508
		DistanceShadowmask
	}
}
