﻿using System;

namespace UnityEngine
{
	// Token: 0x02000062 RID: 98
	public enum CursorMode
	{
		// Token: 0x0400012D RID: 301
		Auto,
		// Token: 0x0400012E RID: 302
		ForceSoftware
	}
}
