﻿using System;

namespace UnityEngine
{
	// Token: 0x020000CB RID: 203
	public enum NPOTSupport
	{
		// Token: 0x0400023F RID: 575
		None,
		// Token: 0x04000240 RID: 576
		Restricted,
		// Token: 0x04000241 RID: 577
		Full
	}
}
