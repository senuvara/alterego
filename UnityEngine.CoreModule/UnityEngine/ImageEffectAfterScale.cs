﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200009A RID: 154
	[UsedByNativeCode]
	public sealed class ImageEffectAfterScale : Attribute
	{
		// Token: 0x0600080B RID: 2059 RVA: 0x0000898B File Offset: 0x00006B8B
		public ImageEffectAfterScale()
		{
		}
	}
}
