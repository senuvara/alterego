﻿using System;

namespace UnityEngine
{
	// Token: 0x020000CE RID: 206
	public enum RenderTextureFormat
	{
		// Token: 0x04000283 RID: 643
		ARGB32,
		// Token: 0x04000284 RID: 644
		Depth,
		// Token: 0x04000285 RID: 645
		ARGBHalf,
		// Token: 0x04000286 RID: 646
		Shadowmap,
		// Token: 0x04000287 RID: 647
		RGB565,
		// Token: 0x04000288 RID: 648
		ARGB4444,
		// Token: 0x04000289 RID: 649
		ARGB1555,
		// Token: 0x0400028A RID: 650
		Default,
		// Token: 0x0400028B RID: 651
		ARGB2101010,
		// Token: 0x0400028C RID: 652
		DefaultHDR,
		// Token: 0x0400028D RID: 653
		ARGB64,
		// Token: 0x0400028E RID: 654
		ARGBFloat,
		// Token: 0x0400028F RID: 655
		RGFloat,
		// Token: 0x04000290 RID: 656
		RGHalf,
		// Token: 0x04000291 RID: 657
		RFloat,
		// Token: 0x04000292 RID: 658
		RHalf,
		// Token: 0x04000293 RID: 659
		R8,
		// Token: 0x04000294 RID: 660
		ARGBInt,
		// Token: 0x04000295 RID: 661
		RGInt,
		// Token: 0x04000296 RID: 662
		RInt,
		// Token: 0x04000297 RID: 663
		BGRA32,
		// Token: 0x04000298 RID: 664
		RGB111110Float = 22,
		// Token: 0x04000299 RID: 665
		RG32,
		// Token: 0x0400029A RID: 666
		RGBAUShort,
		// Token: 0x0400029B RID: 667
		RG16,
		// Token: 0x0400029C RID: 668
		BGRA10101010_XR,
		// Token: 0x0400029D RID: 669
		BGR101010_XR,
		// Token: 0x0400029E RID: 670
		R16
	}
}
