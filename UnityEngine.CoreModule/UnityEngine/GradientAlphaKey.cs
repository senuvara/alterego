﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200007D RID: 125
	[UsedByNativeCode]
	public struct GradientAlphaKey
	{
		// Token: 0x0600065B RID: 1627 RVA: 0x0000E53B File Offset: 0x0000C73B
		public GradientAlphaKey(float alpha, float time)
		{
			this.alpha = alpha;
			this.time = time;
		}

		// Token: 0x04000153 RID: 339
		public float alpha;

		// Token: 0x04000154 RID: 340
		public float time;
	}
}
