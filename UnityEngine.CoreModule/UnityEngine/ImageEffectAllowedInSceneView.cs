﻿using System;

namespace UnityEngine
{
	// Token: 0x02000098 RID: 152
	public sealed class ImageEffectAllowedInSceneView : Attribute
	{
		// Token: 0x06000809 RID: 2057 RVA: 0x0000898B File Offset: 0x00006B8B
		public ImageEffectAllowedInSceneView()
		{
		}
	}
}
