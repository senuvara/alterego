﻿using System;

namespace UnityEngine
{
	// Token: 0x020000E1 RID: 225
	public enum MotionVectorGenerationMode
	{
		// Token: 0x04000373 RID: 883
		Camera,
		// Token: 0x04000374 RID: 884
		Object,
		// Token: 0x04000375 RID: 885
		ForceNoMotion
	}
}
