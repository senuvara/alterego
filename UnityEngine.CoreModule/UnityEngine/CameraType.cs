﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B2 RID: 178
	[Flags]
	public enum CameraType
	{
		// Token: 0x040001BF RID: 447
		Game = 1,
		// Token: 0x040001C0 RID: 448
		SceneView = 2,
		// Token: 0x040001C1 RID: 449
		Preview = 4,
		// Token: 0x040001C2 RID: 450
		VR = 8,
		// Token: 0x040001C3 RID: 451
		Reflection = 16
	}
}
