﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x0200008E RID: 142
	[NativeHeader("Runtime/Export/Graphics.bindings.h")]
	[NativeAsStruct]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class LightProbes : Object
	{
		// Token: 0x06000792 RID: 1938 RVA: 0x0000D647 File Offset: 0x0000B847
		private LightProbes()
		{
		}

		// Token: 0x06000793 RID: 1939 RVA: 0x0000FD82 File Offset: 0x0000DF82
		[FreeFunction]
		public static void GetInterpolatedProbe(Vector3 position, Renderer renderer, out SphericalHarmonicsL2 probe)
		{
			LightProbes.GetInterpolatedProbe_Injected(ref position, renderer, out probe);
		}

		// Token: 0x06000794 RID: 1940
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool AreLightProbesAllowed(Renderer renderer);

		// Token: 0x06000795 RID: 1941 RVA: 0x0000FD90 File Offset: 0x0000DF90
		public static void CalculateInterpolatedLightAndOcclusionProbes(Vector3[] positions, SphericalHarmonicsL2[] lightProbes, Vector4[] occlusionProbes)
		{
			if (positions == null)
			{
				throw new ArgumentNullException("positions");
			}
			if (lightProbes == null && occlusionProbes == null)
			{
				throw new ArgumentException("Argument lightProbes and occlusionProbes cannot both be null.");
			}
			if (lightProbes != null && lightProbes.Length < positions.Length)
			{
				throw new ArgumentException("lightProbes", "Argument lightProbes has less elements than positions");
			}
			if (occlusionProbes != null && occlusionProbes.Length < positions.Length)
			{
				throw new ArgumentException("occlusionProbes", "Argument occlusionProbes has less elements than positions");
			}
			LightProbes.CalculateInterpolatedLightAndOcclusionProbes_Internal(positions, positions.Length, lightProbes, occlusionProbes);
		}

		// Token: 0x06000796 RID: 1942 RVA: 0x0000FE14 File Offset: 0x0000E014
		public static void CalculateInterpolatedLightAndOcclusionProbes(List<Vector3> positions, List<SphericalHarmonicsL2> lightProbes, List<Vector4> occlusionProbes)
		{
			if (positions == null)
			{
				throw new ArgumentNullException("positions");
			}
			if (lightProbes == null && occlusionProbes == null)
			{
				throw new ArgumentException("Argument lightProbes and occlusionProbes cannot both be null.");
			}
			if (lightProbes != null)
			{
				if (lightProbes.Capacity < positions.Count)
				{
					lightProbes.Capacity = positions.Count;
				}
				if (lightProbes.Count < positions.Count)
				{
					NoAllocHelpers.ResizeList<SphericalHarmonicsL2>(lightProbes, positions.Count);
				}
			}
			if (occlusionProbes != null)
			{
				if (occlusionProbes.Capacity < positions.Count)
				{
					occlusionProbes.Capacity = positions.Count;
				}
				if (occlusionProbes.Count < positions.Count)
				{
					NoAllocHelpers.ResizeList<Vector4>(occlusionProbes, positions.Count);
				}
			}
			LightProbes.CalculateInterpolatedLightAndOcclusionProbes_Internal(NoAllocHelpers.ExtractArrayFromListT<Vector3>(positions), positions.Count, NoAllocHelpers.ExtractArrayFromListT<SphericalHarmonicsL2>(lightProbes), NoAllocHelpers.ExtractArrayFromListT<Vector4>(occlusionProbes));
		}

		// Token: 0x06000797 RID: 1943
		[FreeFunction]
		[NativeName("CalculateInterpolatedLightAndOcclusionProbes")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void CalculateInterpolatedLightAndOcclusionProbes_Internal(Vector3[] positions, int positionsCount, SphericalHarmonicsL2[] lightProbes, Vector4[] occlusionProbes);

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x06000798 RID: 1944
		public extern Vector3[] positions { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x06000799 RID: 1945
		// (set) Token: 0x0600079A RID: 1946
		public extern SphericalHarmonicsL2[] bakedProbes { [NativeName("GetBakedCoefficients")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("SetBakedCoefficients")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x0600079B RID: 1947
		public extern int count { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x0600079C RID: 1948
		public extern int cellCount { [NativeName("GetTetrahedraSize")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600079D RID: 1949 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use GetInterpolatedProbe instead.", true)]
		public void GetInterpolatedLightProbe(Vector3 position, Renderer renderer, float[] coefficients)
		{
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x0600079E RID: 1950 RVA: 0x0000FEEC File Offset: 0x0000E0EC
		// (set) Token: 0x0600079F RID: 1951 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use bakedProbes instead.", true)]
		public float[] coefficients
		{
			get
			{
				return new float[0];
			}
			set
			{
			}
		}

		// Token: 0x060007A0 RID: 1952
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetInterpolatedProbe_Injected(ref Vector3 position, Renderer renderer, out SphericalHarmonicsL2 probe);
	}
}
