﻿using System;

namespace UnityEngine
{
	// Token: 0x02000082 RID: 130
	public sealed class SleepTimeout
	{
		// Token: 0x0600066D RID: 1645 RVA: 0x00002370 File Offset: 0x00000570
		public SleepTimeout()
		{
		}

		// Token: 0x04000163 RID: 355
		public const int NeverSleep = -1;

		// Token: 0x04000164 RID: 356
		public const int SystemSetting = -2;
	}
}
