﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000097 RID: 151
	[UsedByNativeCode]
	public sealed class ImageEffectTransformsToLDR : Attribute
	{
		// Token: 0x06000808 RID: 2056 RVA: 0x0000898B File Offset: 0x00006B8B
		public ImageEffectTransformsToLDR()
		{
		}
	}
}
