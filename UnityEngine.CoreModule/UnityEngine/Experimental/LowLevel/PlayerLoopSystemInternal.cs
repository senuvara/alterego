﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.LowLevel
{
	// Token: 0x020001DE RID: 478
	[RequiredByNativeCode]
	[NativeType(Header = "Runtime/Misc/PlayerLoop.h")]
	internal struct PlayerLoopSystemInternal
	{
		// Token: 0x0400070E RID: 1806
		public Type type;

		// Token: 0x0400070F RID: 1807
		public PlayerLoopSystem.UpdateFunction updateDelegate;

		// Token: 0x04000710 RID: 1808
		public IntPtr updateFunction;

		// Token: 0x04000711 RID: 1809
		public IntPtr loopConditionFunction;

		// Token: 0x04000712 RID: 1810
		public int numSubSystems;
	}
}
