﻿using System;

namespace UnityEngine.Experimental.LowLevel
{
	// Token: 0x020001DF RID: 479
	public struct PlayerLoopSystem
	{
		// Token: 0x04000713 RID: 1811
		public Type type;

		// Token: 0x04000714 RID: 1812
		public PlayerLoopSystem[] subSystemList;

		// Token: 0x04000715 RID: 1813
		public PlayerLoopSystem.UpdateFunction updateDelegate;

		// Token: 0x04000716 RID: 1814
		public IntPtr updateFunction;

		// Token: 0x04000717 RID: 1815
		public IntPtr loopConditionFunction;

		// Token: 0x020001E0 RID: 480
		// (Invoke) Token: 0x06000F4F RID: 3919
		public delegate void UpdateFunction();
	}
}
