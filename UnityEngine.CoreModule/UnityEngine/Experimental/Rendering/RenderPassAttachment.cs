﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x02000308 RID: 776
	[NativeType("Runtime/Graphics/ScriptableRenderLoop/ScriptableRenderContext.h")]
	public class RenderPassAttachment : Object
	{
		// Token: 0x06001ADE RID: 6878 RVA: 0x0002F4F8 File Offset: 0x0002D6F8
		public RenderPassAttachment(RenderTextureFormat fmt)
		{
			RenderPassAttachment.Internal_CreateAttachment(this);
			this.loadAction = RenderBufferLoadAction.DontCare;
			this.storeAction = RenderBufferStoreAction.DontCare;
			this.format = fmt;
			this.loadStoreTarget = new RenderTargetIdentifier(BuiltinRenderTextureType.None);
			this.resolveTarget = new RenderTargetIdentifier(BuiltinRenderTextureType.None);
			this.clearColor = new Color(0f, 0f, 0f, 0f);
			this.clearDepth = 1f;
		}

		// Token: 0x17000502 RID: 1282
		// (get) Token: 0x06001ADF RID: 6879
		// (set) Token: 0x06001AE0 RID: 6880
		public extern RenderBufferLoadAction loadAction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] private set; }

		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x06001AE1 RID: 6881
		// (set) Token: 0x06001AE2 RID: 6882
		public extern RenderBufferStoreAction storeAction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] private set; }

		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x06001AE3 RID: 6883
		// (set) Token: 0x06001AE4 RID: 6884
		public extern RenderTextureFormat format { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] private set; }

		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x06001AE5 RID: 6885 RVA: 0x0002F56C File Offset: 0x0002D76C
		// (set) Token: 0x06001AE6 RID: 6886 RVA: 0x0002F582 File Offset: 0x0002D782
		private RenderTargetIdentifier loadStoreTarget
		{
			get
			{
				RenderTargetIdentifier result;
				this.get_loadStoreTarget_Injected(out result);
				return result;
			}
			set
			{
				this.set_loadStoreTarget_Injected(ref value);
			}
		}

		// Token: 0x17000506 RID: 1286
		// (get) Token: 0x06001AE7 RID: 6887 RVA: 0x0002F58C File Offset: 0x0002D78C
		// (set) Token: 0x06001AE8 RID: 6888 RVA: 0x0002F5A2 File Offset: 0x0002D7A2
		private RenderTargetIdentifier resolveTarget
		{
			get
			{
				RenderTargetIdentifier result;
				this.get_resolveTarget_Injected(out result);
				return result;
			}
			set
			{
				this.set_resolveTarget_Injected(ref value);
			}
		}

		// Token: 0x17000507 RID: 1287
		// (get) Token: 0x06001AE9 RID: 6889 RVA: 0x0002F5AC File Offset: 0x0002D7AC
		// (set) Token: 0x06001AEA RID: 6890 RVA: 0x0002F5C2 File Offset: 0x0002D7C2
		public Color clearColor
		{
			get
			{
				Color result;
				this.get_clearColor_Injected(out result);
				return result;
			}
			private set
			{
				this.set_clearColor_Injected(ref value);
			}
		}

		// Token: 0x17000508 RID: 1288
		// (get) Token: 0x06001AEB RID: 6891
		// (set) Token: 0x06001AEC RID: 6892
		public extern float clearDepth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] private set; }

		// Token: 0x17000509 RID: 1289
		// (get) Token: 0x06001AED RID: 6893
		// (set) Token: 0x06001AEE RID: 6894
		public extern uint clearStencil { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] private set; }

		// Token: 0x06001AEF RID: 6895 RVA: 0x0002F5CC File Offset: 0x0002D7CC
		public void BindSurface(RenderTargetIdentifier tgt, bool loadExistingContents, bool storeResults)
		{
			this.loadStoreTarget = tgt;
			if (loadExistingContents && this.loadAction != RenderBufferLoadAction.Clear)
			{
				this.loadAction = RenderBufferLoadAction.Load;
			}
			if (storeResults)
			{
				if (this.storeAction == RenderBufferStoreAction.StoreAndResolve || this.storeAction == RenderBufferStoreAction.Resolve)
				{
					this.storeAction = RenderBufferStoreAction.StoreAndResolve;
				}
				else
				{
					this.storeAction = RenderBufferStoreAction.Store;
				}
			}
		}

		// Token: 0x06001AF0 RID: 6896 RVA: 0x0002F62D File Offset: 0x0002D82D
		public void BindResolveSurface(RenderTargetIdentifier tgt)
		{
			this.resolveTarget = tgt;
			if (this.storeAction == RenderBufferStoreAction.StoreAndResolve || this.storeAction == RenderBufferStoreAction.Store)
			{
				this.storeAction = RenderBufferStoreAction.StoreAndResolve;
			}
			else
			{
				this.storeAction = RenderBufferStoreAction.Resolve;
			}
		}

		// Token: 0x06001AF1 RID: 6897 RVA: 0x0002F661 File Offset: 0x0002D861
		public void Clear(Color clearCol, float clearDep = 1f, uint clearStenc = 0U)
		{
			this.clearColor = clearCol;
			this.clearDepth = clearDep;
			this.clearStencil = clearStenc;
			this.loadAction = RenderBufferLoadAction.Clear;
		}

		// Token: 0x06001AF2 RID: 6898
		[NativeMethod(Name = "RenderPassAttachment::Internal_CreateAttachment", IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Internal_CreateAttachment([Writable] RenderPassAttachment self);

		// Token: 0x06001AF3 RID: 6899
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_loadStoreTarget_Injected(out RenderTargetIdentifier ret);

		// Token: 0x06001AF4 RID: 6900
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_loadStoreTarget_Injected(ref RenderTargetIdentifier value);

		// Token: 0x06001AF5 RID: 6901
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_resolveTarget_Injected(out RenderTargetIdentifier ret);

		// Token: 0x06001AF6 RID: 6902
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_resolveTarget_Injected(ref RenderTargetIdentifier value);

		// Token: 0x06001AF7 RID: 6903
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_clearColor_Injected(out Color ret);

		// Token: 0x06001AF8 RID: 6904
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_clearColor_Injected(ref Color value);
	}
}
