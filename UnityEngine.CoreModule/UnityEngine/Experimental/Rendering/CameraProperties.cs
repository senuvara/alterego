﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002D8 RID: 728
	[UsedByNativeCode]
	public struct CameraProperties
	{
		// Token: 0x0600198D RID: 6541 RVA: 0x0002D000 File Offset: 0x0002B200
		public unsafe Plane GetShadowCullingPlane(int index)
		{
			if (index < 0 || index >= 6)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &this._shadowCullPlanes.FixedElementField)
			{
				return new Plane(new Vector3(ptr[(IntPtr)(index * 4) * 4], ptr[(IntPtr)(index * 4 + 1) * 4], ptr[(IntPtr)(index * 4 + 2) * 4]), ptr[(IntPtr)(index * 4 + 3) * 4]);
			}
		}

		// Token: 0x0600198E RID: 6542 RVA: 0x0002D070 File Offset: 0x0002B270
		public unsafe void SetShadowCullingPlane(int index, Plane plane)
		{
			if (index < 0 || index >= 6)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &this._shadowCullPlanes.FixedElementField)
			{
				ptr[(IntPtr)(index * 4) * 4] = plane.normal.x;
				ptr[(IntPtr)(index * 4 + 1) * 4] = plane.normal.y;
				ptr[(IntPtr)(index * 4 + 2) * 4] = plane.normal.z;
				ptr[(IntPtr)(index * 4 + 3) * 4] = plane.distance;
			}
		}

		// Token: 0x0600198F RID: 6543 RVA: 0x0002D108 File Offset: 0x0002B308
		public unsafe Plane GetCameraCullingPlane(int index)
		{
			if (index < 0 || index >= 6)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &this._cameraCullPlanes.FixedElementField)
			{
				return new Plane(new Vector3(ptr[(IntPtr)(index * 4) * 4], ptr[(IntPtr)(index * 4 + 1) * 4], ptr[(IntPtr)(index * 4 + 2) * 4]), ptr[(IntPtr)(index * 4 + 3) * 4]);
			}
		}

		// Token: 0x06001990 RID: 6544 RVA: 0x0002D178 File Offset: 0x0002B378
		public unsafe void SetCameraCullingPlane(int index, Plane plane)
		{
			if (index < 0 || index >= 6)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &this._cameraCullPlanes.FixedElementField)
			{
				ptr[(IntPtr)(index * 4) * 4] = plane.normal.x;
				ptr[(IntPtr)(index * 4 + 1) * 4] = plane.normal.y;
				ptr[(IntPtr)(index * 4 + 2) * 4] = plane.normal.z;
				ptr[(IntPtr)(index * 4 + 3) * 4] = plane.distance;
			}
		}

		// Token: 0x04000939 RID: 2361
		private const int kNumLayers = 32;

		// Token: 0x0400093A RID: 2362
		private Rect screenRect;

		// Token: 0x0400093B RID: 2363
		private Vector3 viewDir;

		// Token: 0x0400093C RID: 2364
		private float projectionNear;

		// Token: 0x0400093D RID: 2365
		private float projectionFar;

		// Token: 0x0400093E RID: 2366
		private float cameraNear;

		// Token: 0x0400093F RID: 2367
		private float cameraFar;

		// Token: 0x04000940 RID: 2368
		private float cameraAspect;

		// Token: 0x04000941 RID: 2369
		private Matrix4x4 cameraToWorld;

		// Token: 0x04000942 RID: 2370
		private Matrix4x4 actualWorldToClip;

		// Token: 0x04000943 RID: 2371
		private Matrix4x4 cameraClipToWorld;

		// Token: 0x04000944 RID: 2372
		private Matrix4x4 cameraWorldToClip;

		// Token: 0x04000945 RID: 2373
		private Matrix4x4 implicitProjection;

		// Token: 0x04000946 RID: 2374
		private Matrix4x4 stereoWorldToClipLeft;

		// Token: 0x04000947 RID: 2375
		private Matrix4x4 stereoWorldToClipRight;

		// Token: 0x04000948 RID: 2376
		private Matrix4x4 worldToCamera;

		// Token: 0x04000949 RID: 2377
		private Vector3 up;

		// Token: 0x0400094A RID: 2378
		private Vector3 right;

		// Token: 0x0400094B RID: 2379
		private Vector3 transformDirection;

		// Token: 0x0400094C RID: 2380
		private Vector3 cameraEuler;

		// Token: 0x0400094D RID: 2381
		private Vector3 velocity;

		// Token: 0x0400094E RID: 2382
		private float farPlaneWorldSpaceLength;

		// Token: 0x0400094F RID: 2383
		private uint rendererCount;

		// Token: 0x04000950 RID: 2384
		[FixedBuffer(typeof(float), 24)]
		internal CameraProperties.<_shadowCullPlanes>__FixedBuffer1 _shadowCullPlanes;

		// Token: 0x04000951 RID: 2385
		[FixedBuffer(typeof(float), 24)]
		internal CameraProperties.<_cameraCullPlanes>__FixedBuffer2 _cameraCullPlanes;

		// Token: 0x04000952 RID: 2386
		private float baseFarDistance;

		// Token: 0x04000953 RID: 2387
		private Vector3 shadowCullCenter;

		// Token: 0x04000954 RID: 2388
		[FixedBuffer(typeof(float), 32)]
		internal CameraProperties.<layerCullDistances>__FixedBuffer3 layerCullDistances;

		// Token: 0x04000955 RID: 2389
		private int layerCullSpherical;

		// Token: 0x04000956 RID: 2390
		private CoreCameraValues coreCameraValues;

		// Token: 0x04000957 RID: 2391
		private uint cameraType;

		// Token: 0x04000958 RID: 2392
		private int projectionIsOblique;

		// Token: 0x020002D9 RID: 729
		[UnsafeValueType]
		[CompilerGenerated]
		[StructLayout(LayoutKind.Sequential, Size = 96)]
		public struct <_shadowCullPlanes>__FixedBuffer1
		{
			// Token: 0x04000959 RID: 2393
			public float FixedElementField;
		}

		// Token: 0x020002DA RID: 730
		[UnsafeValueType]
		[CompilerGenerated]
		[StructLayout(LayoutKind.Sequential, Size = 96)]
		public struct <_cameraCullPlanes>__FixedBuffer2
		{
			// Token: 0x0400095A RID: 2394
			public float FixedElementField;
		}

		// Token: 0x020002DB RID: 731
		[UnsafeValueType]
		[CompilerGenerated]
		[StructLayout(LayoutKind.Sequential, Size = 128)]
		public struct <layerCullDistances>__FixedBuffer3
		{
			// Token: 0x0400095B RID: 2395
			public float FixedElementField;
		}
	}
}
