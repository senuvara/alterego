﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002F4 RID: 756
	[Flags]
	public enum RendererConfiguration
	{
		// Token: 0x040009C4 RID: 2500
		None = 0,
		// Token: 0x040009C5 RID: 2501
		PerObjectLightProbe = 1,
		// Token: 0x040009C6 RID: 2502
		PerObjectReflectionProbes = 2,
		// Token: 0x040009C7 RID: 2503
		PerObjectLightProbeProxyVolume = 4,
		// Token: 0x040009C8 RID: 2504
		PerObjectLightmaps = 8,
		// Token: 0x040009C9 RID: 2505
		ProvideLightIndices = 16,
		// Token: 0x040009CA RID: 2506
		PerObjectMotionVectors = 32,
		// Token: 0x040009CB RID: 2507
		PerObjectLightIndices8 = 64,
		// Token: 0x040009CC RID: 2508
		ProvideReflectionProbeIndices = 128,
		// Token: 0x040009CD RID: 2509
		PerObjectOcclusionProbe = 256,
		// Token: 0x040009CE RID: 2510
		PerObjectOcclusionProbeProxyVolume = 512,
		// Token: 0x040009CF RID: 2511
		PerObjectShadowMask = 1024
	}
}
