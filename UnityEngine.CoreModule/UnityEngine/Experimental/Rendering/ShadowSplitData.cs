﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002F5 RID: 757
	[UsedByNativeCode]
	public struct ShadowSplitData
	{
		// Token: 0x06001A24 RID: 6692 RVA: 0x0002E1AC File Offset: 0x0002C3AC
		public unsafe Plane GetCullingPlane(int index)
		{
			if (index < 0 || index >= this.cullingPlaneCount || index >= 10)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &this._cullingPlanes.FixedElementField)
			{
				return new Plane(new Vector3(ptr[(IntPtr)(index * 4) * 4], ptr[(IntPtr)(index * 4 + 1) * 4], ptr[(IntPtr)(index * 4 + 2) * 4]), ptr[(IntPtr)(index * 4 + 3) * 4]);
			}
		}

		// Token: 0x06001A25 RID: 6693 RVA: 0x0002E228 File Offset: 0x0002C428
		public unsafe void SetCullingPlane(int index, Plane plane)
		{
			if (index < 0 || index >= this.cullingPlaneCount || index >= 10)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &this._cullingPlanes.FixedElementField)
			{
				ptr[(IntPtr)(index * 4) * 4] = plane.normal.x;
				ptr[(IntPtr)(index * 4 + 1) * 4] = plane.normal.y;
				ptr[(IntPtr)(index * 4 + 2) * 4] = plane.normal.z;
				ptr[(IntPtr)(index * 4 + 3) * 4] = plane.distance;
			}
		}

		// Token: 0x040009D0 RID: 2512
		public int cullingPlaneCount;

		// Token: 0x040009D1 RID: 2513
		[FixedBuffer(typeof(float), 40)]
		internal ShadowSplitData.<_cullingPlanes>__FixedBuffer6 _cullingPlanes;

		// Token: 0x040009D2 RID: 2514
		public Vector4 cullingSphere;

		// Token: 0x020002F6 RID: 758
		[UnsafeValueType]
		[CompilerGenerated]
		[StructLayout(LayoutKind.Sequential, Size = 160)]
		public struct <_cullingPlanes>__FixedBuffer6
		{
			// Token: 0x040009D3 RID: 2515
			public float FixedElementField;
		}
	}
}
