﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002ED RID: 749
	public abstract class RenderPipelineAsset : ScriptableObject, IRenderPipelineAsset
	{
		// Token: 0x060019E5 RID: 6629 RVA: 0x0002DAE5 File Offset: 0x0002BCE5
		protected RenderPipelineAsset()
		{
		}

		// Token: 0x060019E6 RID: 6630 RVA: 0x0002DAF8 File Offset: 0x0002BCF8
		public void DestroyCreatedInstances()
		{
			foreach (IRenderPipeline renderPipeline in this.m_CreatedPipelines)
			{
				renderPipeline.Dispose();
			}
			this.m_CreatedPipelines.Clear();
		}

		// Token: 0x060019E7 RID: 6631 RVA: 0x0002DB60 File Offset: 0x0002BD60
		public IRenderPipeline CreatePipeline()
		{
			IRenderPipeline renderPipeline = this.InternalCreatePipeline();
			if (renderPipeline != null)
			{
				this.m_CreatedPipelines.Add(renderPipeline);
			}
			return renderPipeline;
		}

		// Token: 0x060019E8 RID: 6632 RVA: 0x0002DB90 File Offset: 0x0002BD90
		public virtual string[] GetRenderingLayerMaskNames()
		{
			return null;
		}

		// Token: 0x060019E9 RID: 6633 RVA: 0x0002DBA8 File Offset: 0x0002BDA8
		public virtual Material GetDefaultMaterial()
		{
			return null;
		}

		// Token: 0x060019EA RID: 6634 RVA: 0x0002DBC0 File Offset: 0x0002BDC0
		public virtual Shader GetAutodeskInteractiveShader()
		{
			return null;
		}

		// Token: 0x060019EB RID: 6635 RVA: 0x0002DBD8 File Offset: 0x0002BDD8
		public virtual Shader GetAutodeskInteractiveTransparentShader()
		{
			return null;
		}

		// Token: 0x060019EC RID: 6636 RVA: 0x0002DBF0 File Offset: 0x0002BDF0
		public virtual Shader GetAutodeskInteractiveMaskedShader()
		{
			return null;
		}

		// Token: 0x060019ED RID: 6637 RVA: 0x0002DC08 File Offset: 0x0002BE08
		public virtual Material GetDefaultParticleMaterial()
		{
			return null;
		}

		// Token: 0x060019EE RID: 6638 RVA: 0x0002DC20 File Offset: 0x0002BE20
		public virtual Material GetDefaultLineMaterial()
		{
			return null;
		}

		// Token: 0x060019EF RID: 6639 RVA: 0x0002DC38 File Offset: 0x0002BE38
		public virtual Material GetDefaultTerrainMaterial()
		{
			return null;
		}

		// Token: 0x060019F0 RID: 6640 RVA: 0x0002DC50 File Offset: 0x0002BE50
		public virtual Material GetDefaultUIMaterial()
		{
			return null;
		}

		// Token: 0x060019F1 RID: 6641 RVA: 0x0002DC68 File Offset: 0x0002BE68
		public virtual Material GetDefaultUIOverdrawMaterial()
		{
			return null;
		}

		// Token: 0x060019F2 RID: 6642 RVA: 0x0002DC80 File Offset: 0x0002BE80
		public virtual Material GetDefaultUIETC1SupportedMaterial()
		{
			return null;
		}

		// Token: 0x060019F3 RID: 6643 RVA: 0x0002DC98 File Offset: 0x0002BE98
		public virtual Material GetDefault2DMaterial()
		{
			return null;
		}

		// Token: 0x060019F4 RID: 6644 RVA: 0x0002DCB0 File Offset: 0x0002BEB0
		public virtual Shader GetDefaultShader()
		{
			return null;
		}

		// Token: 0x060019F5 RID: 6645
		protected abstract IRenderPipeline InternalCreatePipeline();

		// Token: 0x060019F6 RID: 6646 RVA: 0x0002DCC8 File Offset: 0x0002BEC8
		protected IEnumerable<IRenderPipeline> CreatedInstances()
		{
			return this.m_CreatedPipelines;
		}

		// Token: 0x060019F7 RID: 6647 RVA: 0x0002DCE3 File Offset: 0x0002BEE3
		protected virtual void OnValidate()
		{
			this.DestroyCreatedInstances();
		}

		// Token: 0x060019F8 RID: 6648 RVA: 0x0002DCE3 File Offset: 0x0002BEE3
		protected virtual void OnDisable()
		{
			this.DestroyCreatedInstances();
		}

		// Token: 0x040009A7 RID: 2471
		private readonly List<IRenderPipeline> m_CreatedPipelines = new List<IRenderPipeline>();
	}
}
