﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002F0 RID: 752
	public struct RenderStateBlock
	{
		// Token: 0x06001A01 RID: 6657 RVA: 0x0002DE76 File Offset: 0x0002C076
		public RenderStateBlock(RenderStateMask mask)
		{
			this.m_BlendState = BlendState.Default;
			this.m_RasterState = RasterState.Default;
			this.m_DepthState = DepthState.Default;
			this.m_StencilState = StencilState.Default;
			this.m_StencilReference = 0;
			this.m_Mask = mask;
		}

		// Token: 0x170004BF RID: 1215
		// (get) Token: 0x06001A02 RID: 6658 RVA: 0x0002DEB4 File Offset: 0x0002C0B4
		// (set) Token: 0x06001A03 RID: 6659 RVA: 0x0002DECF File Offset: 0x0002C0CF
		public BlendState blendState
		{
			get
			{
				return this.m_BlendState;
			}
			set
			{
				this.m_BlendState = value;
			}
		}

		// Token: 0x170004C0 RID: 1216
		// (get) Token: 0x06001A04 RID: 6660 RVA: 0x0002DEDC File Offset: 0x0002C0DC
		// (set) Token: 0x06001A05 RID: 6661 RVA: 0x0002DEF7 File Offset: 0x0002C0F7
		public RasterState rasterState
		{
			get
			{
				return this.m_RasterState;
			}
			set
			{
				this.m_RasterState = value;
			}
		}

		// Token: 0x170004C1 RID: 1217
		// (get) Token: 0x06001A06 RID: 6662 RVA: 0x0002DF04 File Offset: 0x0002C104
		// (set) Token: 0x06001A07 RID: 6663 RVA: 0x0002DF1F File Offset: 0x0002C11F
		public DepthState depthState
		{
			get
			{
				return this.m_DepthState;
			}
			set
			{
				this.m_DepthState = value;
			}
		}

		// Token: 0x170004C2 RID: 1218
		// (get) Token: 0x06001A08 RID: 6664 RVA: 0x0002DF2C File Offset: 0x0002C12C
		// (set) Token: 0x06001A09 RID: 6665 RVA: 0x0002DF47 File Offset: 0x0002C147
		public StencilState stencilState
		{
			get
			{
				return this.m_StencilState;
			}
			set
			{
				this.m_StencilState = value;
			}
		}

		// Token: 0x170004C3 RID: 1219
		// (get) Token: 0x06001A0A RID: 6666 RVA: 0x0002DF54 File Offset: 0x0002C154
		// (set) Token: 0x06001A0B RID: 6667 RVA: 0x0002DF6F File Offset: 0x0002C16F
		public int stencilReference
		{
			get
			{
				return this.m_StencilReference;
			}
			set
			{
				this.m_StencilReference = value;
			}
		}

		// Token: 0x170004C4 RID: 1220
		// (get) Token: 0x06001A0C RID: 6668 RVA: 0x0002DF7C File Offset: 0x0002C17C
		// (set) Token: 0x06001A0D RID: 6669 RVA: 0x0002DF97 File Offset: 0x0002C197
		public RenderStateMask mask
		{
			get
			{
				return this.m_Mask;
			}
			set
			{
				this.m_Mask = value;
			}
		}

		// Token: 0x040009AC RID: 2476
		private BlendState m_BlendState;

		// Token: 0x040009AD RID: 2477
		private RasterState m_RasterState;

		// Token: 0x040009AE RID: 2478
		private DepthState m_DepthState;

		// Token: 0x040009AF RID: 2479
		private StencilState m_StencilState;

		// Token: 0x040009B0 RID: 2480
		private int m_StencilReference;

		// Token: 0x040009B1 RID: 2481
		private RenderStateMask m_Mask;
	}
}
