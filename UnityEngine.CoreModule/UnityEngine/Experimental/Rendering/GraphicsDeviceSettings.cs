﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x02000091 RID: 145
	public static class GraphicsDeviceSettings
	{
		// Token: 0x17000141 RID: 321
		// (get) Token: 0x060007A1 RID: 1953
		// (set) Token: 0x060007A2 RID: 1954
		[StaticAccessor("GetGfxDevice()", StaticAccessorType.Dot)]
		public static extern WaitForPresentSyncPoint waitForPresentSyncPoint { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x060007A3 RID: 1955
		// (set) Token: 0x060007A4 RID: 1956
		[StaticAccessor("GetGfxDevice()", StaticAccessorType.Dot)]
		public static extern GraphicsJobsSyncPoint graphicsJobsSyncPoint { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
