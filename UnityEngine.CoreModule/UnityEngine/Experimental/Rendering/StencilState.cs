﻿using System;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002F8 RID: 760
	public struct StencilState
	{
		// Token: 0x06001A26 RID: 6694 RVA: 0x0002E2CC File Offset: 0x0002C4CC
		public StencilState(bool enabled = false, byte readMask = 255, byte writeMask = 255, CompareFunction compareFunction = CompareFunction.Always, StencilOp passOperation = StencilOp.Keep, StencilOp failOperation = StencilOp.Keep, StencilOp zFailOperation = StencilOp.Keep)
		{
			this = new StencilState(enabled, readMask, writeMask, compareFunction, passOperation, failOperation, zFailOperation, compareFunction, passOperation, failOperation, zFailOperation);
		}

		// Token: 0x06001A27 RID: 6695 RVA: 0x0002E2F4 File Offset: 0x0002C4F4
		public StencilState(bool enabled, byte readMask, byte writeMask, CompareFunction compareFunctionFront, StencilOp passOperationFront, StencilOp failOperationFront, StencilOp zFailOperationFront, CompareFunction compareFunctionBack, StencilOp passOperationBack, StencilOp failOperationBack, StencilOp zFailOperationBack)
		{
			this.m_Enabled = Convert.ToByte(enabled);
			this.m_ReadMask = readMask;
			this.m_WriteMask = writeMask;
			this.m_Padding = 0;
			this.m_CompareFunctionFront = (byte)compareFunctionFront;
			this.m_PassOperationFront = (byte)passOperationFront;
			this.m_FailOperationFront = (byte)failOperationFront;
			this.m_ZFailOperationFront = (byte)zFailOperationFront;
			this.m_CompareFunctionBack = (byte)compareFunctionBack;
			this.m_PassOperationBack = (byte)passOperationBack;
			this.m_FailOperationBack = (byte)failOperationBack;
			this.m_ZFailOperationBack = (byte)zFailOperationBack;
		}

		// Token: 0x170004CF RID: 1231
		// (get) Token: 0x06001A28 RID: 6696 RVA: 0x0002E36C File Offset: 0x0002C56C
		public static StencilState Default
		{
			get
			{
				return new StencilState(false, byte.MaxValue, byte.MaxValue, CompareFunction.Always, StencilOp.Keep, StencilOp.Keep, StencilOp.Keep);
			}
		}

		// Token: 0x170004D0 RID: 1232
		// (get) Token: 0x06001A29 RID: 6697 RVA: 0x0002E398 File Offset: 0x0002C598
		// (set) Token: 0x06001A2A RID: 6698 RVA: 0x0002E3B8 File Offset: 0x0002C5B8
		public bool enabled
		{
			get
			{
				return Convert.ToBoolean(this.m_Enabled);
			}
			set
			{
				this.m_Enabled = Convert.ToByte(value);
			}
		}

		// Token: 0x170004D1 RID: 1233
		// (get) Token: 0x06001A2B RID: 6699 RVA: 0x0002E3C8 File Offset: 0x0002C5C8
		// (set) Token: 0x06001A2C RID: 6700 RVA: 0x0002E3E3 File Offset: 0x0002C5E3
		public byte readMask
		{
			get
			{
				return this.m_ReadMask;
			}
			set
			{
				this.m_ReadMask = value;
			}
		}

		// Token: 0x170004D2 RID: 1234
		// (get) Token: 0x06001A2D RID: 6701 RVA: 0x0002E3F0 File Offset: 0x0002C5F0
		// (set) Token: 0x06001A2E RID: 6702 RVA: 0x0002E40B File Offset: 0x0002C60B
		public byte writeMask
		{
			get
			{
				return this.m_WriteMask;
			}
			set
			{
				this.m_WriteMask = value;
			}
		}

		// Token: 0x170004D3 RID: 1235
		// (set) Token: 0x06001A2F RID: 6703 RVA: 0x0002E415 File Offset: 0x0002C615
		public CompareFunction compareFunction
		{
			set
			{
				this.compareFunctionFront = value;
				this.compareFunctionBack = value;
			}
		}

		// Token: 0x170004D4 RID: 1236
		// (set) Token: 0x06001A30 RID: 6704 RVA: 0x0002E426 File Offset: 0x0002C626
		public StencilOp passOperation
		{
			set
			{
				this.passOperationFront = value;
				this.passOperationBack = value;
			}
		}

		// Token: 0x170004D5 RID: 1237
		// (set) Token: 0x06001A31 RID: 6705 RVA: 0x0002E437 File Offset: 0x0002C637
		public StencilOp failOperation
		{
			set
			{
				this.failOperationFront = value;
				this.failOperationBack = value;
			}
		}

		// Token: 0x170004D6 RID: 1238
		// (set) Token: 0x06001A32 RID: 6706 RVA: 0x0002E448 File Offset: 0x0002C648
		public StencilOp zFailOperation
		{
			set
			{
				this.zFailOperationFront = value;
				this.zFailOperationBack = value;
			}
		}

		// Token: 0x170004D7 RID: 1239
		// (get) Token: 0x06001A33 RID: 6707 RVA: 0x0002E45C File Offset: 0x0002C65C
		// (set) Token: 0x06001A34 RID: 6708 RVA: 0x0002E477 File Offset: 0x0002C677
		public CompareFunction compareFunctionFront
		{
			get
			{
				return (CompareFunction)this.m_CompareFunctionFront;
			}
			set
			{
				this.m_CompareFunctionFront = (byte)value;
			}
		}

		// Token: 0x170004D8 RID: 1240
		// (get) Token: 0x06001A35 RID: 6709 RVA: 0x0002E484 File Offset: 0x0002C684
		// (set) Token: 0x06001A36 RID: 6710 RVA: 0x0002E49F File Offset: 0x0002C69F
		public StencilOp passOperationFront
		{
			get
			{
				return (StencilOp)this.m_PassOperationFront;
			}
			set
			{
				this.m_PassOperationFront = (byte)value;
			}
		}

		// Token: 0x170004D9 RID: 1241
		// (get) Token: 0x06001A37 RID: 6711 RVA: 0x0002E4AC File Offset: 0x0002C6AC
		// (set) Token: 0x06001A38 RID: 6712 RVA: 0x0002E4C7 File Offset: 0x0002C6C7
		public StencilOp failOperationFront
		{
			get
			{
				return (StencilOp)this.m_FailOperationFront;
			}
			set
			{
				this.m_FailOperationFront = (byte)value;
			}
		}

		// Token: 0x170004DA RID: 1242
		// (get) Token: 0x06001A39 RID: 6713 RVA: 0x0002E4D4 File Offset: 0x0002C6D4
		// (set) Token: 0x06001A3A RID: 6714 RVA: 0x0002E4EF File Offset: 0x0002C6EF
		public StencilOp zFailOperationFront
		{
			get
			{
				return (StencilOp)this.m_ZFailOperationFront;
			}
			set
			{
				this.m_ZFailOperationFront = (byte)value;
			}
		}

		// Token: 0x170004DB RID: 1243
		// (get) Token: 0x06001A3B RID: 6715 RVA: 0x0002E4FC File Offset: 0x0002C6FC
		// (set) Token: 0x06001A3C RID: 6716 RVA: 0x0002E517 File Offset: 0x0002C717
		public CompareFunction compareFunctionBack
		{
			get
			{
				return (CompareFunction)this.m_CompareFunctionBack;
			}
			set
			{
				this.m_CompareFunctionBack = (byte)value;
			}
		}

		// Token: 0x170004DC RID: 1244
		// (get) Token: 0x06001A3D RID: 6717 RVA: 0x0002E524 File Offset: 0x0002C724
		// (set) Token: 0x06001A3E RID: 6718 RVA: 0x0002E53F File Offset: 0x0002C73F
		public StencilOp passOperationBack
		{
			get
			{
				return (StencilOp)this.m_PassOperationBack;
			}
			set
			{
				this.m_PassOperationBack = (byte)value;
			}
		}

		// Token: 0x170004DD RID: 1245
		// (get) Token: 0x06001A3F RID: 6719 RVA: 0x0002E54C File Offset: 0x0002C74C
		// (set) Token: 0x06001A40 RID: 6720 RVA: 0x0002E567 File Offset: 0x0002C767
		public StencilOp failOperationBack
		{
			get
			{
				return (StencilOp)this.m_FailOperationBack;
			}
			set
			{
				this.m_FailOperationBack = (byte)value;
			}
		}

		// Token: 0x170004DE RID: 1246
		// (get) Token: 0x06001A41 RID: 6721 RVA: 0x0002E574 File Offset: 0x0002C774
		// (set) Token: 0x06001A42 RID: 6722 RVA: 0x0002E58F File Offset: 0x0002C78F
		public StencilOp zFailOperationBack
		{
			get
			{
				return (StencilOp)this.m_ZFailOperationBack;
			}
			set
			{
				this.m_ZFailOperationBack = (byte)value;
			}
		}

		// Token: 0x040009DF RID: 2527
		private byte m_Enabled;

		// Token: 0x040009E0 RID: 2528
		private byte m_ReadMask;

		// Token: 0x040009E1 RID: 2529
		private byte m_WriteMask;

		// Token: 0x040009E2 RID: 2530
		private byte m_Padding;

		// Token: 0x040009E3 RID: 2531
		private byte m_CompareFunctionFront;

		// Token: 0x040009E4 RID: 2532
		private byte m_PassOperationFront;

		// Token: 0x040009E5 RID: 2533
		private byte m_FailOperationFront;

		// Token: 0x040009E6 RID: 2534
		private byte m_ZFailOperationFront;

		// Token: 0x040009E7 RID: 2535
		private byte m_CompareFunctionBack;

		// Token: 0x040009E8 RID: 2536
		private byte m_PassOperationBack;

		// Token: 0x040009E9 RID: 2537
		private byte m_FailOperationBack;

		// Token: 0x040009EA RID: 2538
		private byte m_ZFailOperationBack;
	}
}
