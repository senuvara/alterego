﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x02000269 RID: 617
	public interface IScriptableRuntimeReflectionSystem : IDisposable
	{
		// Token: 0x060016A9 RID: 5801
		bool TickRealtimeProbes();
	}
}
