﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020000D3 RID: 211
	[Flags]
	public enum TextureCreationFlags
	{
		// Token: 0x040002B8 RID: 696
		None = 0,
		// Token: 0x040002B9 RID: 697
		MipChain = 1,
		// Token: 0x040002BA RID: 698
		Crunch = 64
	}
}
