﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x0200030E RID: 782
	[UsedByNativeCode]
	public struct CullResults
	{
		// Token: 0x06001B07 RID: 6919 RVA: 0x0002F840 File Offset: 0x0002DA40
		private void Init()
		{
			this.visibleLights = new List<VisibleLight>();
			this.visibleOffscreenVertexLights = new List<VisibleLight>();
			this.visibleReflectionProbes = new List<VisibleReflectionProbe>();
			this.visibleRenderers = default(FilterResults);
			this.cullResults = IntPtr.Zero;
		}

		// Token: 0x06001B08 RID: 6920 RVA: 0x0002F88C File Offset: 0x0002DA8C
		public static bool GetCullingParameters(Camera camera, out ScriptableCullingParameters cullingParameters)
		{
			return CullResults.GetCullingParameters_Internal(camera, false, out cullingParameters, sizeof(ScriptableCullingParameters));
		}

		// Token: 0x06001B09 RID: 6921 RVA: 0x0002F8B0 File Offset: 0x0002DAB0
		public static bool GetCullingParameters(Camera camera, bool stereoAware, out ScriptableCullingParameters cullingParameters)
		{
			return CullResults.GetCullingParameters_Internal(camera, stereoAware, out cullingParameters, sizeof(ScriptableCullingParameters));
		}

		// Token: 0x06001B0A RID: 6922
		[FreeFunction("ScriptableRenderLoop_Bindings::GetCullingParameters_Internal")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetCullingParameters_Internal(Camera camera, bool stereoAware, out ScriptableCullingParameters cullingParameters, int managedCullingParametersSize);

		// Token: 0x06001B0B RID: 6923 RVA: 0x0002F8D3 File Offset: 0x0002DAD3
		[FreeFunction("ScriptableRenderLoop_Bindings::Internal_Cull")]
		internal static void Internal_Cull(ref ScriptableCullingParameters parameters, ScriptableRenderContext renderLoop, ref CullResults results)
		{
			CullResults.Internal_Cull_Injected(ref parameters, ref renderLoop, ref results);
		}

		// Token: 0x06001B0C RID: 6924 RVA: 0x0002F8E0 File Offset: 0x0002DAE0
		public static CullResults Cull(ref ScriptableCullingParameters parameters, ScriptableRenderContext renderLoop)
		{
			CullResults result = default(CullResults);
			CullResults.Cull(ref parameters, renderLoop, ref result);
			return result;
		}

		// Token: 0x06001B0D RID: 6925 RVA: 0x0002F907 File Offset: 0x0002DB07
		public static void Cull(ref ScriptableCullingParameters parameters, ScriptableRenderContext renderLoop, ref CullResults results)
		{
			if (results.visibleLights == null || results.visibleOffscreenVertexLights == null || results.visibleReflectionProbes == null)
			{
				results.Init();
			}
			CullResults.Internal_Cull(ref parameters, renderLoop, ref results);
		}

		// Token: 0x06001B0E RID: 6926 RVA: 0x0002F93C File Offset: 0x0002DB3C
		public static bool Cull(Camera camera, ScriptableRenderContext renderLoop, out CullResults results)
		{
			results.cullResults = IntPtr.Zero;
			results.visibleLights = null;
			results.visibleOffscreenVertexLights = null;
			results.visibleReflectionProbes = null;
			results.visibleRenderers = default(FilterResults);
			ScriptableCullingParameters scriptableCullingParameters;
			bool result;
			if (!CullResults.GetCullingParameters(camera, out scriptableCullingParameters))
			{
				result = false;
			}
			else
			{
				results = CullResults.Cull(ref scriptableCullingParameters, renderLoop);
				result = true;
			}
			return result;
		}

		// Token: 0x06001B0F RID: 6927 RVA: 0x0002F9A4 File Offset: 0x0002DBA4
		public bool GetShadowCasterBounds(int lightIndex, out Bounds outBounds)
		{
			return CullResults.GetShadowCasterBounds(this.cullResults, lightIndex, out outBounds);
		}

		// Token: 0x06001B10 RID: 6928
		[FreeFunction("ScriptableRenderLoop_Bindings::GetShadowCasterBounds")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetShadowCasterBounds(IntPtr cullResults, int lightIndex, out Bounds bounds);

		// Token: 0x06001B11 RID: 6929 RVA: 0x0002F9C8 File Offset: 0x0002DBC8
		public int GetLightIndicesCount()
		{
			return CullResults.GetLightIndicesCount(this.cullResults);
		}

		// Token: 0x06001B12 RID: 6930
		[FreeFunction("ScriptableRenderLoop_Bindings::GetLightIndicesCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetLightIndicesCount(IntPtr cullResults);

		// Token: 0x06001B13 RID: 6931 RVA: 0x0002F9E8 File Offset: 0x0002DBE8
		public void FillLightIndices(ComputeBuffer computeBuffer)
		{
			CullResults.FillLightIndices(this.cullResults, computeBuffer);
		}

		// Token: 0x06001B14 RID: 6932
		[FreeFunction("ScriptableRenderLoop_Bindings::FillLightIndices")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void FillLightIndices(IntPtr cullResults, ComputeBuffer computeBuffer);

		// Token: 0x06001B15 RID: 6933 RVA: 0x0002F9F8 File Offset: 0x0002DBF8
		public int[] GetLightIndexMap()
		{
			return CullResults.GetLightIndexMap(this.cullResults);
		}

		// Token: 0x06001B16 RID: 6934
		[FreeFunction("ScriptableRenderLoop_Bindings::GetLightIndexMap")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int[] GetLightIndexMap(IntPtr cullResults);

		// Token: 0x06001B17 RID: 6935 RVA: 0x0002FA18 File Offset: 0x0002DC18
		public void SetLightIndexMap(int[] mapping)
		{
			CullResults.SetLightIndexMap(this.cullResults, mapping);
		}

		// Token: 0x06001B18 RID: 6936
		[FreeFunction("ScriptableRenderLoop_Bindings::SetLightIndexMap")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLightIndexMap(IntPtr cullResults, int[] mapping);

		// Token: 0x06001B19 RID: 6937 RVA: 0x0002FA28 File Offset: 0x0002DC28
		public bool ComputeSpotShadowMatricesAndCullingPrimitives(int activeLightIndex, out Matrix4x4 viewMatrix, out Matrix4x4 projMatrix, out ShadowSplitData shadowSplitData)
		{
			return CullResults.ComputeSpotShadowMatricesAndCullingPrimitives(this.cullResults, activeLightIndex, out viewMatrix, out projMatrix, out shadowSplitData);
		}

		// Token: 0x06001B1A RID: 6938
		[FreeFunction("ScriptableRenderLoop_Bindings::ComputeSpotShadowMatricesAndCullingPrimitives")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool ComputeSpotShadowMatricesAndCullingPrimitives(IntPtr cullResults, int activeLightIndex, out Matrix4x4 viewMatrix, out Matrix4x4 projMatrix, out ShadowSplitData shadowSplitData);

		// Token: 0x06001B1B RID: 6939 RVA: 0x0002FA50 File Offset: 0x0002DC50
		public bool ComputePointShadowMatricesAndCullingPrimitives(int activeLightIndex, CubemapFace cubemapFace, float fovBias, out Matrix4x4 viewMatrix, out Matrix4x4 projMatrix, out ShadowSplitData shadowSplitData)
		{
			return CullResults.ComputePointShadowMatricesAndCullingPrimitives(this.cullResults, activeLightIndex, cubemapFace, fovBias, out viewMatrix, out projMatrix, out shadowSplitData);
		}

		// Token: 0x06001B1C RID: 6940
		[FreeFunction("ScriptableRenderLoop_Bindings::ComputePointShadowMatricesAndCullingPrimitives")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool ComputePointShadowMatricesAndCullingPrimitives(IntPtr cullResults, int activeLightIndex, CubemapFace cubemapFace, float fovBias, out Matrix4x4 viewMatrix, out Matrix4x4 projMatrix, out ShadowSplitData shadowSplitData);

		// Token: 0x06001B1D RID: 6941 RVA: 0x0002FA7C File Offset: 0x0002DC7C
		public bool ComputeDirectionalShadowMatricesAndCullingPrimitives(int activeLightIndex, int splitIndex, int splitCount, Vector3 splitRatio, int shadowResolution, float shadowNearPlaneOffset, out Matrix4x4 viewMatrix, out Matrix4x4 projMatrix, out ShadowSplitData shadowSplitData)
		{
			return CullResults.ComputeDirectionalShadowMatricesAndCullingPrimitives(this.cullResults, activeLightIndex, splitIndex, splitCount, splitRatio, shadowResolution, shadowNearPlaneOffset, out viewMatrix, out projMatrix, out shadowSplitData);
		}

		// Token: 0x06001B1E RID: 6942 RVA: 0x0002FAAC File Offset: 0x0002DCAC
		[FreeFunction("ScriptableRenderLoop_Bindings::ComputeDirectionalShadowMatricesAndCullingPrimitives")]
		private static bool ComputeDirectionalShadowMatricesAndCullingPrimitives(IntPtr cullResults, int activeLightIndex, int splitIndex, int splitCount, Vector3 splitRatio, int shadowResolution, float shadowNearPlaneOffset, out Matrix4x4 viewMatrix, out Matrix4x4 projMatrix, out ShadowSplitData shadowSplitData)
		{
			return CullResults.ComputeDirectionalShadowMatricesAndCullingPrimitives_Injected(cullResults, activeLightIndex, splitIndex, splitCount, ref splitRatio, shadowResolution, shadowNearPlaneOffset, out viewMatrix, out projMatrix, out shadowSplitData);
		}

		// Token: 0x06001B1F RID: 6943
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Cull_Injected(ref ScriptableCullingParameters parameters, ref ScriptableRenderContext renderLoop, ref CullResults results);

		// Token: 0x06001B20 RID: 6944
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool ComputeDirectionalShadowMatricesAndCullingPrimitives_Injected(IntPtr cullResults, int activeLightIndex, int splitIndex, int splitCount, ref Vector3 splitRatio, int shadowResolution, float shadowNearPlaneOffset, out Matrix4x4 viewMatrix, out Matrix4x4 projMatrix, out ShadowSplitData shadowSplitData);

		// Token: 0x04000A3D RID: 2621
		public List<VisibleLight> visibleLights;

		// Token: 0x04000A3E RID: 2622
		public List<VisibleLight> visibleOffscreenVertexLights;

		// Token: 0x04000A3F RID: 2623
		public List<VisibleReflectionProbe> visibleReflectionProbes;

		// Token: 0x04000A40 RID: 2624
		public FilterResults visibleRenderers;

		// Token: 0x04000A41 RID: 2625
		internal IntPtr cullResults;
	}
}
