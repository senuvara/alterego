﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x0200026A RID: 618
	public abstract class ScriptableRuntimeReflectionSystem : IScriptableRuntimeReflectionSystem, IDisposable
	{
		// Token: 0x060016AA RID: 5802 RVA: 0x00002370 File Offset: 0x00000570
		protected ScriptableRuntimeReflectionSystem()
		{
		}

		// Token: 0x060016AB RID: 5803 RVA: 0x000285C0 File Offset: 0x000267C0
		public virtual bool TickRealtimeProbes()
		{
			return false;
		}

		// Token: 0x060016AC RID: 5804 RVA: 0x00007476 File Offset: 0x00005676
		protected virtual void Dispose(bool disposing)
		{
		}

		// Token: 0x060016AD RID: 5805 RVA: 0x000285D6 File Offset: 0x000267D6
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
