﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002D6 RID: 726
	public struct BlendState
	{
		// Token: 0x06001977 RID: 6519 RVA: 0x0002CDBC File Offset: 0x0002AFBC
		public BlendState(bool separateMRTBlend = false, bool alphaToMask = false)
		{
			this.m_BlendState0 = RenderTargetBlendState.Default;
			this.m_BlendState1 = RenderTargetBlendState.Default;
			this.m_BlendState2 = RenderTargetBlendState.Default;
			this.m_BlendState3 = RenderTargetBlendState.Default;
			this.m_BlendState4 = RenderTargetBlendState.Default;
			this.m_BlendState5 = RenderTargetBlendState.Default;
			this.m_BlendState6 = RenderTargetBlendState.Default;
			this.m_BlendState7 = RenderTargetBlendState.Default;
			this.m_SeparateMRTBlendStates = Convert.ToByte(separateMRTBlend);
			this.m_AlphaToMask = Convert.ToByte(alphaToMask);
			this.m_Padding = 0;
		}

		// Token: 0x1700048F RID: 1167
		// (get) Token: 0x06001978 RID: 6520 RVA: 0x0002CE44 File Offset: 0x0002B044
		public static BlendState Default
		{
			get
			{
				return new BlendState(false, false);
			}
		}

		// Token: 0x17000490 RID: 1168
		// (get) Token: 0x06001979 RID: 6521 RVA: 0x0002CE60 File Offset: 0x0002B060
		// (set) Token: 0x0600197A RID: 6522 RVA: 0x0002CE80 File Offset: 0x0002B080
		public bool separateMRTBlendStates
		{
			get
			{
				return Convert.ToBoolean(this.m_SeparateMRTBlendStates);
			}
			set
			{
				this.m_SeparateMRTBlendStates = Convert.ToByte(value);
			}
		}

		// Token: 0x17000491 RID: 1169
		// (get) Token: 0x0600197B RID: 6523 RVA: 0x0002CE90 File Offset: 0x0002B090
		// (set) Token: 0x0600197C RID: 6524 RVA: 0x0002CEB0 File Offset: 0x0002B0B0
		public bool alphaToMask
		{
			get
			{
				return Convert.ToBoolean(this.m_AlphaToMask);
			}
			set
			{
				this.m_AlphaToMask = Convert.ToByte(value);
			}
		}

		// Token: 0x17000492 RID: 1170
		// (get) Token: 0x0600197D RID: 6525 RVA: 0x0002CEC0 File Offset: 0x0002B0C0
		// (set) Token: 0x0600197E RID: 6526 RVA: 0x0002CEDB File Offset: 0x0002B0DB
		public RenderTargetBlendState blendState0
		{
			get
			{
				return this.m_BlendState0;
			}
			set
			{
				this.m_BlendState0 = value;
			}
		}

		// Token: 0x17000493 RID: 1171
		// (get) Token: 0x0600197F RID: 6527 RVA: 0x0002CEE8 File Offset: 0x0002B0E8
		// (set) Token: 0x06001980 RID: 6528 RVA: 0x0002CF03 File Offset: 0x0002B103
		public RenderTargetBlendState blendState1
		{
			get
			{
				return this.m_BlendState1;
			}
			set
			{
				this.m_BlendState1 = value;
			}
		}

		// Token: 0x17000494 RID: 1172
		// (get) Token: 0x06001981 RID: 6529 RVA: 0x0002CF10 File Offset: 0x0002B110
		// (set) Token: 0x06001982 RID: 6530 RVA: 0x0002CF2B File Offset: 0x0002B12B
		public RenderTargetBlendState blendState2
		{
			get
			{
				return this.m_BlendState2;
			}
			set
			{
				this.m_BlendState2 = value;
			}
		}

		// Token: 0x17000495 RID: 1173
		// (get) Token: 0x06001983 RID: 6531 RVA: 0x0002CF38 File Offset: 0x0002B138
		// (set) Token: 0x06001984 RID: 6532 RVA: 0x0002CF53 File Offset: 0x0002B153
		public RenderTargetBlendState blendState3
		{
			get
			{
				return this.m_BlendState3;
			}
			set
			{
				this.m_BlendState3 = value;
			}
		}

		// Token: 0x17000496 RID: 1174
		// (get) Token: 0x06001985 RID: 6533 RVA: 0x0002CF60 File Offset: 0x0002B160
		// (set) Token: 0x06001986 RID: 6534 RVA: 0x0002CF7B File Offset: 0x0002B17B
		public RenderTargetBlendState blendState4
		{
			get
			{
				return this.m_BlendState4;
			}
			set
			{
				this.m_BlendState4 = value;
			}
		}

		// Token: 0x17000497 RID: 1175
		// (get) Token: 0x06001987 RID: 6535 RVA: 0x0002CF88 File Offset: 0x0002B188
		// (set) Token: 0x06001988 RID: 6536 RVA: 0x0002CFA3 File Offset: 0x0002B1A3
		public RenderTargetBlendState blendState5
		{
			get
			{
				return this.m_BlendState5;
			}
			set
			{
				this.m_BlendState5 = value;
			}
		}

		// Token: 0x17000498 RID: 1176
		// (get) Token: 0x06001989 RID: 6537 RVA: 0x0002CFB0 File Offset: 0x0002B1B0
		// (set) Token: 0x0600198A RID: 6538 RVA: 0x0002CFCB File Offset: 0x0002B1CB
		public RenderTargetBlendState blendState6
		{
			get
			{
				return this.m_BlendState6;
			}
			set
			{
				this.m_BlendState6 = value;
			}
		}

		// Token: 0x17000499 RID: 1177
		// (get) Token: 0x0600198B RID: 6539 RVA: 0x0002CFD8 File Offset: 0x0002B1D8
		// (set) Token: 0x0600198C RID: 6540 RVA: 0x0002CFF3 File Offset: 0x0002B1F3
		public RenderTargetBlendState blendState7
		{
			get
			{
				return this.m_BlendState7;
			}
			set
			{
				this.m_BlendState7 = value;
			}
		}

		// Token: 0x0400092A RID: 2346
		private RenderTargetBlendState m_BlendState0;

		// Token: 0x0400092B RID: 2347
		private RenderTargetBlendState m_BlendState1;

		// Token: 0x0400092C RID: 2348
		private RenderTargetBlendState m_BlendState2;

		// Token: 0x0400092D RID: 2349
		private RenderTargetBlendState m_BlendState3;

		// Token: 0x0400092E RID: 2350
		private RenderTargetBlendState m_BlendState4;

		// Token: 0x0400092F RID: 2351
		private RenderTargetBlendState m_BlendState5;

		// Token: 0x04000930 RID: 2352
		private RenderTargetBlendState m_BlendState6;

		// Token: 0x04000931 RID: 2353
		private RenderTargetBlendState m_BlendState7;

		// Token: 0x04000932 RID: 2354
		private byte m_SeparateMRTBlendStates;

		// Token: 0x04000933 RID: 2355
		private byte m_AlphaToMask;

		// Token: 0x04000934 RID: 2356
		private short m_Padding;
	}
}
