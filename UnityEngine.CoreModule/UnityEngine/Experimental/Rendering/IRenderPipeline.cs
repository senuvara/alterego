﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002E7 RID: 743
	public interface IRenderPipeline : IDisposable
	{
		// Token: 0x170004B0 RID: 1200
		// (get) Token: 0x060019C2 RID: 6594
		bool disposed { get; }

		// Token: 0x060019C3 RID: 6595
		void Render(ScriptableRenderContext renderContext, Camera[] cameras);
	}
}
