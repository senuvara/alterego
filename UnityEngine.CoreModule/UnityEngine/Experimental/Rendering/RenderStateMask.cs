﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002F2 RID: 754
	[Flags]
	public enum RenderStateMask
	{
		// Token: 0x040009B5 RID: 2485
		Nothing = 0,
		// Token: 0x040009B6 RID: 2486
		Blend = 1,
		// Token: 0x040009B7 RID: 2487
		Raster = 2,
		// Token: 0x040009B8 RID: 2488
		Depth = 4,
		// Token: 0x040009B9 RID: 2489
		Stencil = 8,
		// Token: 0x040009BA RID: 2490
		Everything = 15
	}
}
