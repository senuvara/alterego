﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002DC RID: 732
	[Flags]
	public enum CullFlag
	{
		// Token: 0x0400095D RID: 2397
		None = 0,
		// Token: 0x0400095E RID: 2398
		ForceEvenIfCameraIsNotActive = 1,
		// Token: 0x0400095F RID: 2399
		OcclusionCull = 2,
		// Token: 0x04000960 RID: 2400
		NeedsLighting = 4,
		// Token: 0x04000961 RID: 2401
		NeedsReflectionProbes = 8,
		// Token: 0x04000962 RID: 2402
		Stereo = 16,
		// Token: 0x04000963 RID: 2403
		DisablePerObjectCulling = 32
	}
}
