﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x0200008F RID: 143
	public enum WaitForPresentSyncPoint
	{
		// Token: 0x0400017C RID: 380
		BeginFrame,
		// Token: 0x0400017D RID: 381
		EndFrame
	}
}
