﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002E6 RID: 742
	public struct FilterResults
	{
		// Token: 0x04000991 RID: 2449
		internal IntPtr m_CullResults;
	}
}
