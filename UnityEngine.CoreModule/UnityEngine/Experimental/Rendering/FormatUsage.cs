﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020000D4 RID: 212
	public enum FormatUsage
	{
		// Token: 0x040002BC RID: 700
		Sample,
		// Token: 0x040002BD RID: 701
		Linear,
		// Token: 0x040002BE RID: 702
		Render = 3,
		// Token: 0x040002BF RID: 703
		Blend,
		// Token: 0x040002C0 RID: 704
		LoadStore = 8,
		// Token: 0x040002C1 RID: 705
		MSAA2x,
		// Token: 0x040002C2 RID: 706
		MSAA4x,
		// Token: 0x040002C3 RID: 707
		MSAA8x
	}
}
