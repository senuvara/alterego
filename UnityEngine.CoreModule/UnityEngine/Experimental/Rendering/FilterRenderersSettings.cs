﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002E5 RID: 741
	public struct FilterRenderersSettings
	{
		// Token: 0x060019B9 RID: 6585 RVA: 0x0002D6EB File Offset: 0x0002B8EB
		public FilterRenderersSettings(bool initializeValues = false)
		{
			this = default(FilterRenderersSettings);
			if (initializeValues)
			{
				this.m_RenderQueueRange = RenderQueueRange.all;
				this.m_LayerMask = -1;
				this.m_RenderingLayerMask = uint.MaxValue;
				this.m_ExcludeMotionVectorObjects = 0;
			}
		}

		// Token: 0x170004AC RID: 1196
		// (get) Token: 0x060019BA RID: 6586 RVA: 0x0002D720 File Offset: 0x0002B920
		// (set) Token: 0x060019BB RID: 6587 RVA: 0x0002D73B File Offset: 0x0002B93B
		public RenderQueueRange renderQueueRange
		{
			get
			{
				return this.m_RenderQueueRange;
			}
			set
			{
				this.m_RenderQueueRange = value;
			}
		}

		// Token: 0x170004AD RID: 1197
		// (get) Token: 0x060019BC RID: 6588 RVA: 0x0002D748 File Offset: 0x0002B948
		// (set) Token: 0x060019BD RID: 6589 RVA: 0x0002D763 File Offset: 0x0002B963
		public int layerMask
		{
			get
			{
				return this.m_LayerMask;
			}
			set
			{
				this.m_LayerMask = value;
			}
		}

		// Token: 0x170004AE RID: 1198
		// (get) Token: 0x060019BE RID: 6590 RVA: 0x0002D770 File Offset: 0x0002B970
		// (set) Token: 0x060019BF RID: 6591 RVA: 0x0002D78B File Offset: 0x0002B98B
		public uint renderingLayerMask
		{
			get
			{
				return this.m_RenderingLayerMask;
			}
			set
			{
				this.m_RenderingLayerMask = value;
			}
		}

		// Token: 0x170004AF RID: 1199
		// (get) Token: 0x060019C0 RID: 6592 RVA: 0x0002D798 File Offset: 0x0002B998
		// (set) Token: 0x060019C1 RID: 6593 RVA: 0x0002D7B9 File Offset: 0x0002B9B9
		public bool excludeMotionVectorObjects
		{
			get
			{
				return this.m_ExcludeMotionVectorObjects != 0;
			}
			set
			{
				this.m_ExcludeMotionVectorObjects = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x0400098D RID: 2445
		private RenderQueueRange m_RenderQueueRange;

		// Token: 0x0400098E RID: 2446
		private int m_LayerMask;

		// Token: 0x0400098F RID: 2447
		private uint m_RenderingLayerMask;

		// Token: 0x04000990 RID: 2448
		private int m_ExcludeMotionVectorObjects;
	}
}
