﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x0200026C RID: 620
	[RequiredByNativeCode]
	internal class ScriptableRuntimeReflectionSystemWrapper
	{
		// Token: 0x060016B5 RID: 5813 RVA: 0x00002370 File Offset: 0x00000570
		public ScriptableRuntimeReflectionSystemWrapper()
		{
		}

		// Token: 0x17000449 RID: 1097
		// (get) Token: 0x060016B6 RID: 5814 RVA: 0x00028704 File Offset: 0x00026904
		// (set) Token: 0x060016B7 RID: 5815 RVA: 0x0002871E File Offset: 0x0002691E
		internal IScriptableRuntimeReflectionSystem implementation
		{
			[CompilerGenerated]
			get
			{
				return this.<implementation>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<implementation>k__BackingField = value;
			}
		}

		// Token: 0x060016B8 RID: 5816 RVA: 0x00028728 File Offset: 0x00026928
		[RequiredByNativeCode]
		private bool Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes_ReturnResult()
		{
			return this.implementation != null && this.implementation.TickRealtimeProbes();
		}

		// Token: 0x060016B9 RID: 5817 RVA: 0x00028756 File Offset: 0x00026956
		[RequiredByNativeCode]
		private void Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes(out bool result)
		{
			result = (this.implementation != null && this.implementation.TickRealtimeProbes());
		}

		// Token: 0x04000845 RID: 2117
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private IScriptableRuntimeReflectionSystem <implementation>k__BackingField;
	}
}
