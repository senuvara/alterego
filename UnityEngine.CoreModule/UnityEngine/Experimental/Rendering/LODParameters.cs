﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002E9 RID: 745
	public struct LODParameters
	{
		// Token: 0x170004B1 RID: 1201
		// (get) Token: 0x060019C6 RID: 6598 RVA: 0x0002D7D0 File Offset: 0x0002B9D0
		// (set) Token: 0x060019C7 RID: 6599 RVA: 0x0002D7F0 File Offset: 0x0002B9F0
		public bool isOrthographic
		{
			get
			{
				return Convert.ToBoolean(this.m_IsOrthographic);
			}
			set
			{
				this.m_IsOrthographic = Convert.ToInt32(value);
			}
		}

		// Token: 0x170004B2 RID: 1202
		// (get) Token: 0x060019C8 RID: 6600 RVA: 0x0002D800 File Offset: 0x0002BA00
		// (set) Token: 0x060019C9 RID: 6601 RVA: 0x0002D81B File Offset: 0x0002BA1B
		public Vector3 cameraPosition
		{
			get
			{
				return this.m_CameraPosition;
			}
			set
			{
				this.m_CameraPosition = value;
			}
		}

		// Token: 0x170004B3 RID: 1203
		// (get) Token: 0x060019CA RID: 6602 RVA: 0x0002D828 File Offset: 0x0002BA28
		// (set) Token: 0x060019CB RID: 6603 RVA: 0x0002D843 File Offset: 0x0002BA43
		public float fieldOfView
		{
			get
			{
				return this.m_FieldOfView;
			}
			set
			{
				this.m_FieldOfView = value;
			}
		}

		// Token: 0x170004B4 RID: 1204
		// (get) Token: 0x060019CC RID: 6604 RVA: 0x0002D850 File Offset: 0x0002BA50
		// (set) Token: 0x060019CD RID: 6605 RVA: 0x0002D86B File Offset: 0x0002BA6B
		public float orthoSize
		{
			get
			{
				return this.m_OrthoSize;
			}
			set
			{
				this.m_OrthoSize = value;
			}
		}

		// Token: 0x170004B5 RID: 1205
		// (get) Token: 0x060019CE RID: 6606 RVA: 0x0002D878 File Offset: 0x0002BA78
		// (set) Token: 0x060019CF RID: 6607 RVA: 0x0002D893 File Offset: 0x0002BA93
		public int cameraPixelHeight
		{
			get
			{
				return this.m_CameraPixelHeight;
			}
			set
			{
				this.m_CameraPixelHeight = value;
			}
		}

		// Token: 0x04000992 RID: 2450
		private int m_IsOrthographic;

		// Token: 0x04000993 RID: 2451
		private Vector3 m_CameraPosition;

		// Token: 0x04000994 RID: 2452
		private float m_FieldOfView;

		// Token: 0x04000995 RID: 2453
		private float m_OrthoSize;

		// Token: 0x04000996 RID: 2454
		private int m_CameraPixelHeight;
	}
}
