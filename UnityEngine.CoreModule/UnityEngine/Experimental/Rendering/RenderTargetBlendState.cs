﻿using System;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002F3 RID: 755
	public struct RenderTargetBlendState
	{
		// Token: 0x06001A14 RID: 6676 RVA: 0x0002E01C File Offset: 0x0002C21C
		public RenderTargetBlendState(ColorWriteMask writeMask = ColorWriteMask.All, BlendMode sourceColorBlendMode = BlendMode.One, BlendMode destinationColorBlendMode = BlendMode.Zero, BlendMode sourceAlphaBlendMode = BlendMode.One, BlendMode destinationAlphaBlendMode = BlendMode.Zero, BlendOp colorBlendOperation = BlendOp.Add, BlendOp alphaBlendOperation = BlendOp.Add)
		{
			this.m_WriteMask = (byte)writeMask;
			this.m_SourceColorBlendMode = (byte)sourceColorBlendMode;
			this.m_DestinationColorBlendMode = (byte)destinationColorBlendMode;
			this.m_SourceAlphaBlendMode = (byte)sourceAlphaBlendMode;
			this.m_DestinationAlphaBlendMode = (byte)destinationAlphaBlendMode;
			this.m_ColorBlendOperation = (byte)colorBlendOperation;
			this.m_AlphaBlendOperation = (byte)alphaBlendOperation;
			this.m_Padding = 0;
		}

		// Token: 0x170004C7 RID: 1223
		// (get) Token: 0x06001A15 RID: 6677 RVA: 0x0002E070 File Offset: 0x0002C270
		public static RenderTargetBlendState Default
		{
			get
			{
				return new RenderTargetBlendState(ColorWriteMask.All, BlendMode.One, BlendMode.Zero, BlendMode.One, BlendMode.Zero, BlendOp.Add, BlendOp.Add);
			}
		}

		// Token: 0x170004C8 RID: 1224
		// (get) Token: 0x06001A16 RID: 6678 RVA: 0x0002E094 File Offset: 0x0002C294
		// (set) Token: 0x06001A17 RID: 6679 RVA: 0x0002E0AF File Offset: 0x0002C2AF
		public ColorWriteMask writeMask
		{
			get
			{
				return (ColorWriteMask)this.m_WriteMask;
			}
			set
			{
				this.m_WriteMask = (byte)value;
			}
		}

		// Token: 0x170004C9 RID: 1225
		// (get) Token: 0x06001A18 RID: 6680 RVA: 0x0002E0BC File Offset: 0x0002C2BC
		// (set) Token: 0x06001A19 RID: 6681 RVA: 0x0002E0D7 File Offset: 0x0002C2D7
		public BlendMode sourceColorBlendMode
		{
			get
			{
				return (BlendMode)this.m_SourceColorBlendMode;
			}
			set
			{
				this.m_SourceColorBlendMode = (byte)value;
			}
		}

		// Token: 0x170004CA RID: 1226
		// (get) Token: 0x06001A1A RID: 6682 RVA: 0x0002E0E4 File Offset: 0x0002C2E4
		// (set) Token: 0x06001A1B RID: 6683 RVA: 0x0002E0FF File Offset: 0x0002C2FF
		public BlendMode destinationColorBlendMode
		{
			get
			{
				return (BlendMode)this.m_DestinationColorBlendMode;
			}
			set
			{
				this.m_DestinationColorBlendMode = (byte)value;
			}
		}

		// Token: 0x170004CB RID: 1227
		// (get) Token: 0x06001A1C RID: 6684 RVA: 0x0002E10C File Offset: 0x0002C30C
		// (set) Token: 0x06001A1D RID: 6685 RVA: 0x0002E127 File Offset: 0x0002C327
		public BlendMode sourceAlphaBlendMode
		{
			get
			{
				return (BlendMode)this.m_SourceAlphaBlendMode;
			}
			set
			{
				this.m_SourceAlphaBlendMode = (byte)value;
			}
		}

		// Token: 0x170004CC RID: 1228
		// (get) Token: 0x06001A1E RID: 6686 RVA: 0x0002E134 File Offset: 0x0002C334
		// (set) Token: 0x06001A1F RID: 6687 RVA: 0x0002E14F File Offset: 0x0002C34F
		public BlendMode destinationAlphaBlendMode
		{
			get
			{
				return (BlendMode)this.m_DestinationAlphaBlendMode;
			}
			set
			{
				this.m_DestinationAlphaBlendMode = (byte)value;
			}
		}

		// Token: 0x170004CD RID: 1229
		// (get) Token: 0x06001A20 RID: 6688 RVA: 0x0002E15C File Offset: 0x0002C35C
		// (set) Token: 0x06001A21 RID: 6689 RVA: 0x0002E177 File Offset: 0x0002C377
		public BlendOp colorBlendOperation
		{
			get
			{
				return (BlendOp)this.m_ColorBlendOperation;
			}
			set
			{
				this.m_ColorBlendOperation = (byte)value;
			}
		}

		// Token: 0x170004CE RID: 1230
		// (get) Token: 0x06001A22 RID: 6690 RVA: 0x0002E184 File Offset: 0x0002C384
		// (set) Token: 0x06001A23 RID: 6691 RVA: 0x0002E19F File Offset: 0x0002C39F
		public BlendOp alphaBlendOperation
		{
			get
			{
				return (BlendOp)this.m_AlphaBlendOperation;
			}
			set
			{
				this.m_AlphaBlendOperation = (byte)value;
			}
		}

		// Token: 0x040009BB RID: 2491
		private byte m_WriteMask;

		// Token: 0x040009BC RID: 2492
		private byte m_SourceColorBlendMode;

		// Token: 0x040009BD RID: 2493
		private byte m_DestinationColorBlendMode;

		// Token: 0x040009BE RID: 2494
		private byte m_SourceAlphaBlendMode;

		// Token: 0x040009BF RID: 2495
		private byte m_DestinationAlphaBlendMode;

		// Token: 0x040009C0 RID: 2496
		private byte m_ColorBlendOperation;

		// Token: 0x040009C1 RID: 2497
		private byte m_AlphaBlendOperation;

		// Token: 0x040009C2 RID: 2498
		private byte m_Padding;
	}
}
