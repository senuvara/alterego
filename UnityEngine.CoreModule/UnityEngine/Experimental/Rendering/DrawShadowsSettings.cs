﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002E4 RID: 740
	[UsedByNativeCode]
	public struct DrawShadowsSettings
	{
		// Token: 0x060019B7 RID: 6583 RVA: 0x0002D6A8 File Offset: 0x0002B8A8
		public DrawShadowsSettings(CullResults cullResults, int lightIndex)
		{
			this._cullResults = cullResults.cullResults;
			this.lightIndex = lightIndex;
			this.splitData.cullingPlaneCount = 0;
			this.splitData.cullingSphere = Vector4.zero;
		}

		// Token: 0x170004AB RID: 1195
		// (set) Token: 0x060019B8 RID: 6584 RVA: 0x0002D6DB File Offset: 0x0002B8DB
		public CullResults cullResults
		{
			set
			{
				this._cullResults = value.cullResults;
			}
		}

		// Token: 0x0400098A RID: 2442
		private IntPtr _cullResults;

		// Token: 0x0400098B RID: 2443
		public int lightIndex;

		// Token: 0x0400098C RID: 2444
		public ShadowSplitData splitData;
	}
}
