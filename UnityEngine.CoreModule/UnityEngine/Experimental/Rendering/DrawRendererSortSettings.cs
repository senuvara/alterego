﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002E3 RID: 739
	public struct DrawRendererSortSettings
	{
		// Token: 0x170004AA RID: 1194
		// (get) Token: 0x060019B5 RID: 6581 RVA: 0x0002D674 File Offset: 0x0002B874
		// (set) Token: 0x060019B6 RID: 6582 RVA: 0x0002D692 File Offset: 0x0002B892
		[Obsolete("Use sortMode instead")]
		public bool sortOrthographic
		{
			get
			{
				return this.sortMode == DrawRendererSortMode.Orthographic;
			}
			set
			{
				this.sortMode = ((!value) ? DrawRendererSortMode.Perspective : DrawRendererSortMode.Orthographic);
			}
		}

		// Token: 0x04000983 RID: 2435
		public Matrix4x4 worldToCameraMatrix;

		// Token: 0x04000984 RID: 2436
		public Vector3 cameraPosition;

		// Token: 0x04000985 RID: 2437
		public Vector3 cameraCustomSortAxis;

		// Token: 0x04000986 RID: 2438
		public SortFlags flags;

		// Token: 0x04000987 RID: 2439
		public DrawRendererSortMode sortMode;

		// Token: 0x04000988 RID: 2440
		private Matrix4x4 _previousVPMatrix;

		// Token: 0x04000989 RID: 2441
		private Matrix4x4 _nonJitteredVPMatrix;
	}
}
