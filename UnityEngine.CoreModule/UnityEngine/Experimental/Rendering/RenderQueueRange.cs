﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002EF RID: 751
	public struct RenderQueueRange
	{
		// Token: 0x170004BC RID: 1212
		// (get) Token: 0x060019FE RID: 6654 RVA: 0x0002DDD8 File Offset: 0x0002BFD8
		public static RenderQueueRange all
		{
			get
			{
				return new RenderQueueRange
				{
					min = 0,
					max = 5000
				};
			}
		}

		// Token: 0x170004BD RID: 1213
		// (get) Token: 0x060019FF RID: 6655 RVA: 0x0002DE0C File Offset: 0x0002C00C
		public static RenderQueueRange opaque
		{
			get
			{
				return new RenderQueueRange
				{
					min = 0,
					max = 2500
				};
			}
		}

		// Token: 0x170004BE RID: 1214
		// (get) Token: 0x06001A00 RID: 6656 RVA: 0x0002DE40 File Offset: 0x0002C040
		public static RenderQueueRange transparent
		{
			get
			{
				return new RenderQueueRange
				{
					min = 2501,
					max = 5000
				};
			}
		}

		// Token: 0x040009AA RID: 2474
		public int min;

		// Token: 0x040009AB RID: 2475
		public int max;
	}
}
