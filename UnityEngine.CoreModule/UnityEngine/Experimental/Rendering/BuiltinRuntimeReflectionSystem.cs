﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x02000268 RID: 616
	[NativeHeader("Runtime/Camera/ReflectionProbes.h")]
	internal class BuiltinRuntimeReflectionSystem : IScriptableRuntimeReflectionSystem, IDisposable
	{
		// Token: 0x060016A3 RID: 5795 RVA: 0x00002370 File Offset: 0x00000570
		public BuiltinRuntimeReflectionSystem()
		{
		}

		// Token: 0x060016A4 RID: 5796 RVA: 0x00028580 File Offset: 0x00026780
		public bool TickRealtimeProbes()
		{
			return BuiltinRuntimeReflectionSystem.BuiltinUpdate();
		}

		// Token: 0x060016A5 RID: 5797 RVA: 0x0002859A File Offset: 0x0002679A
		public void Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x060016A6 RID: 5798 RVA: 0x00007476 File Offset: 0x00005676
		private void Dispose(bool disposing)
		{
		}

		// Token: 0x060016A7 RID: 5799
		[StaticAccessor("GetReflectionProbes()", Type = StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool BuiltinUpdate();

		// Token: 0x060016A8 RID: 5800 RVA: 0x000285A4 File Offset: 0x000267A4
		[RequiredByNativeCode]
		private static BuiltinRuntimeReflectionSystem Internal_BuiltinRuntimeReflectionSystem_New()
		{
			return new BuiltinRuntimeReflectionSystem();
		}
	}
}
