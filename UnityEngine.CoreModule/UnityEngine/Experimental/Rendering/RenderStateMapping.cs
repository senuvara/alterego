﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002F1 RID: 753
	public struct RenderStateMapping
	{
		// Token: 0x06001A0E RID: 6670 RVA: 0x0002DFA1 File Offset: 0x0002C1A1
		public RenderStateMapping(string renderType, RenderStateBlock stateBlock)
		{
			this.m_RenderTypeID = Shader.TagToID(renderType);
			this.m_StateBlock = stateBlock;
		}

		// Token: 0x06001A0F RID: 6671 RVA: 0x0002DFB7 File Offset: 0x0002C1B7
		public RenderStateMapping(RenderStateBlock stateBlock)
		{
			this = new RenderStateMapping(null, stateBlock);
		}

		// Token: 0x170004C5 RID: 1221
		// (get) Token: 0x06001A10 RID: 6672 RVA: 0x0002DFC4 File Offset: 0x0002C1C4
		// (set) Token: 0x06001A11 RID: 6673 RVA: 0x0002DFE4 File Offset: 0x0002C1E4
		public string renderType
		{
			get
			{
				return Shader.IDToTag(this.m_RenderTypeID);
			}
			set
			{
				this.m_RenderTypeID = Shader.TagToID(value);
			}
		}

		// Token: 0x170004C6 RID: 1222
		// (get) Token: 0x06001A12 RID: 6674 RVA: 0x0002DFF4 File Offset: 0x0002C1F4
		// (set) Token: 0x06001A13 RID: 6675 RVA: 0x0002E00F File Offset: 0x0002C20F
		public RenderStateBlock stateBlock
		{
			get
			{
				return this.m_StateBlock;
			}
			set
			{
				this.m_StateBlock = value;
			}
		}

		// Token: 0x040009B2 RID: 2482
		private int m_RenderTypeID;

		// Token: 0x040009B3 RID: 2483
		private RenderStateBlock m_StateBlock;
	}
}
