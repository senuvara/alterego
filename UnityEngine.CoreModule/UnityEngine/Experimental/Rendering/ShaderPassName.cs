﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x02000309 RID: 777
	[NativeHeader("Runtime/Export/ScriptableRenderLoop/ScriptableRenderLoop.bindings.h")]
	public struct ShaderPassName
	{
		// Token: 0x06001AF9 RID: 6905 RVA: 0x0002F680 File Offset: 0x0002D880
		public ShaderPassName(string name)
		{
			this.m_NameIndex = ShaderPassName.Init(name);
		}

		// Token: 0x06001AFA RID: 6906
		[FreeFunction("ScriptableRenderLoop_Bindings::InitShaderPassName")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Init(string name);

		// Token: 0x1700050A RID: 1290
		// (get) Token: 0x06001AFB RID: 6907 RVA: 0x0002F690 File Offset: 0x0002D890
		internal int nameIndex
		{
			get
			{
				return this.m_NameIndex;
			}
		}

		// Token: 0x04000A21 RID: 2593
		private int m_NameIndex;
	}
}
