﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002FC RID: 764
	[Flags]
	public enum VisibleLightFlags
	{
		// Token: 0x04000A02 RID: 2562
		None = 0,
		// Token: 0x04000A03 RID: 2563
		IntersectsNearPlane = 1,
		// Token: 0x04000A04 RID: 2564
		IntersectsFarPlane = 2
	}
}
