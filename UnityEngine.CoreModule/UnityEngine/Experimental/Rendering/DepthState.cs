﻿using System;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002E0 RID: 736
	public struct DepthState
	{
		// Token: 0x060019AF RID: 6575 RVA: 0x0002D5E7 File Offset: 0x0002B7E7
		public DepthState(bool writeEnabled = true, CompareFunction compareFunction = CompareFunction.Less)
		{
			this.m_WriteEnabled = Convert.ToByte(writeEnabled);
			this.m_CompareFunction = (sbyte)compareFunction;
		}

		// Token: 0x170004A7 RID: 1191
		// (get) Token: 0x060019B0 RID: 6576 RVA: 0x0002D600 File Offset: 0x0002B800
		public static DepthState Default
		{
			get
			{
				return new DepthState(true, CompareFunction.Less);
			}
		}

		// Token: 0x170004A8 RID: 1192
		// (get) Token: 0x060019B1 RID: 6577 RVA: 0x0002D61C File Offset: 0x0002B81C
		// (set) Token: 0x060019B2 RID: 6578 RVA: 0x0002D63C File Offset: 0x0002B83C
		public bool writeEnabled
		{
			get
			{
				return Convert.ToBoolean(this.m_WriteEnabled);
			}
			set
			{
				this.m_WriteEnabled = Convert.ToByte(value);
			}
		}

		// Token: 0x170004A9 RID: 1193
		// (get) Token: 0x060019B3 RID: 6579 RVA: 0x0002D64C File Offset: 0x0002B84C
		// (set) Token: 0x060019B4 RID: 6580 RVA: 0x0002D668 File Offset: 0x0002B868
		public CompareFunction compareFunction
		{
			get
			{
				return (CompareFunction)this.m_CompareFunction;
			}
			set
			{
				this.m_CompareFunction = (sbyte)value;
			}
		}

		// Token: 0x04000979 RID: 2425
		private byte m_WriteEnabled;

		// Token: 0x0400097A RID: 2426
		private sbyte m_CompareFunction;
	}
}
