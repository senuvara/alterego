﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002DD RID: 733
	[UsedByNativeCode]
	public struct ScriptableCullingParameters
	{
		// Token: 0x1700049A RID: 1178
		// (get) Token: 0x06001991 RID: 6545 RVA: 0x0002D210 File Offset: 0x0002B410
		// (set) Token: 0x06001992 RID: 6546 RVA: 0x0002D22B File Offset: 0x0002B42B
		public int cullingPlaneCount
		{
			get
			{
				return this.m_CullingPlaneCount;
			}
			set
			{
				if (value < 0 || value > 10)
				{
					throw new IndexOutOfRangeException("Invalid plane count (0 <= count <= 10)");
				}
				this.m_CullingPlaneCount = value;
			}
		}

		// Token: 0x1700049B RID: 1179
		// (get) Token: 0x06001993 RID: 6547 RVA: 0x0002D250 File Offset: 0x0002B450
		// (set) Token: 0x06001994 RID: 6548 RVA: 0x0002D270 File Offset: 0x0002B470
		public bool isOrthographic
		{
			get
			{
				return Convert.ToBoolean(this.m_IsOrthographic);
			}
			set
			{
				this.m_IsOrthographic = Convert.ToInt32(value);
			}
		}

		// Token: 0x1700049C RID: 1180
		// (get) Token: 0x06001995 RID: 6549 RVA: 0x0002D280 File Offset: 0x0002B480
		// (set) Token: 0x06001996 RID: 6550 RVA: 0x0002D29B File Offset: 0x0002B49B
		public LODParameters lodParameters
		{
			get
			{
				return this.m_LodParameters;
			}
			set
			{
				this.m_LodParameters = value;
			}
		}

		// Token: 0x1700049D RID: 1181
		// (get) Token: 0x06001997 RID: 6551 RVA: 0x0002D2A8 File Offset: 0x0002B4A8
		// (set) Token: 0x06001998 RID: 6552 RVA: 0x0002D2C3 File Offset: 0x0002B4C3
		public int cullingMask
		{
			get
			{
				return this.m_CullingMask;
			}
			set
			{
				this.m_CullingMask = value;
			}
		}

		// Token: 0x1700049E RID: 1182
		// (get) Token: 0x06001999 RID: 6553 RVA: 0x0002D2D0 File Offset: 0x0002B4D0
		// (set) Token: 0x0600199A RID: 6554 RVA: 0x0002D2EB File Offset: 0x0002B4EB
		public long sceneMask
		{
			get
			{
				return this.m_SceneMask;
			}
			set
			{
				this.m_SceneMask = value;
			}
		}

		// Token: 0x1700049F RID: 1183
		// (get) Token: 0x0600199B RID: 6555 RVA: 0x0002D2F8 File Offset: 0x0002B4F8
		// (set) Token: 0x0600199C RID: 6556 RVA: 0x0002D313 File Offset: 0x0002B513
		public int layerCull
		{
			get
			{
				return this.m_LayerCull;
			}
			set
			{
				this.m_LayerCull = value;
			}
		}

		// Token: 0x170004A0 RID: 1184
		// (get) Token: 0x0600199D RID: 6557 RVA: 0x0002D320 File Offset: 0x0002B520
		// (set) Token: 0x0600199E RID: 6558 RVA: 0x0002D33B File Offset: 0x0002B53B
		public Matrix4x4 cullingMatrix
		{
			get
			{
				return this.m_CullingMatrix;
			}
			set
			{
				this.m_CullingMatrix = value;
			}
		}

		// Token: 0x170004A1 RID: 1185
		// (get) Token: 0x0600199F RID: 6559 RVA: 0x0002D348 File Offset: 0x0002B548
		// (set) Token: 0x060019A0 RID: 6560 RVA: 0x0002D363 File Offset: 0x0002B563
		public Vector3 position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				this.m_Position = value;
			}
		}

		// Token: 0x170004A2 RID: 1186
		// (get) Token: 0x060019A1 RID: 6561 RVA: 0x0002D370 File Offset: 0x0002B570
		// (set) Token: 0x060019A2 RID: 6562 RVA: 0x0002D38B File Offset: 0x0002B58B
		public float shadowDistance
		{
			get
			{
				return this.m_shadowDistance;
			}
			set
			{
				this.m_shadowDistance = value;
			}
		}

		// Token: 0x170004A3 RID: 1187
		// (get) Token: 0x060019A3 RID: 6563 RVA: 0x0002D398 File Offset: 0x0002B598
		// (set) Token: 0x060019A4 RID: 6564 RVA: 0x0002D3B3 File Offset: 0x0002B5B3
		public CullFlag cullingFlags
		{
			get
			{
				return this.m_CullingFlags;
			}
			set
			{
				this.m_CullingFlags = value;
			}
		}

		// Token: 0x170004A4 RID: 1188
		// (get) Token: 0x060019A5 RID: 6565 RVA: 0x0002D3C0 File Offset: 0x0002B5C0
		// (set) Token: 0x060019A6 RID: 6566 RVA: 0x0002D3DB File Offset: 0x0002B5DB
		public ReflectionProbeSortOptions reflectionProbeSortOptions
		{
			get
			{
				return this.m_ReflectionProbeSortOptions;
			}
			set
			{
				this.m_ReflectionProbeSortOptions = value;
			}
		}

		// Token: 0x170004A5 RID: 1189
		// (get) Token: 0x060019A7 RID: 6567 RVA: 0x0002D3E8 File Offset: 0x0002B5E8
		// (set) Token: 0x060019A8 RID: 6568 RVA: 0x0002D403 File Offset: 0x0002B603
		public CameraProperties cameraProperties
		{
			get
			{
				return this.m_CameraProperties;
			}
			set
			{
				this.m_CameraProperties = value;
			}
		}

		// Token: 0x170004A6 RID: 1190
		// (get) Token: 0x060019A9 RID: 6569 RVA: 0x0002D410 File Offset: 0x0002B610
		// (set) Token: 0x060019AA RID: 6570 RVA: 0x0002D42B File Offset: 0x0002B62B
		public float accurateOcclusionThreshold
		{
			get
			{
				return this.m_AccurateoOcclusionThreshold;
			}
			set
			{
				this.m_AccurateoOcclusionThreshold = Mathf.Max(-1f, value);
			}
		}

		// Token: 0x060019AB RID: 6571 RVA: 0x0002D440 File Offset: 0x0002B640
		public unsafe float GetLayerCullDistance(int layerIndex)
		{
			if (layerIndex < 0 || layerIndex >= 32)
			{
				throw new IndexOutOfRangeException("Invalid layer index");
			}
			fixed (float* ptr = &this.m_LayerFarCullDistances.FixedElementField)
			{
				return ptr[(IntPtr)layerIndex * 4];
			}
		}

		// Token: 0x060019AC RID: 6572 RVA: 0x0002D484 File Offset: 0x0002B684
		public unsafe void SetLayerCullDistance(int layerIndex, float distance)
		{
			if (layerIndex < 0 || layerIndex >= 32)
			{
				throw new IndexOutOfRangeException("Invalid layer index");
			}
			fixed (float* ptr = &this.m_LayerFarCullDistances.FixedElementField)
			{
				ptr[(IntPtr)layerIndex * 4] = distance;
			}
		}

		// Token: 0x060019AD RID: 6573 RVA: 0x0002D4C8 File Offset: 0x0002B6C8
		public unsafe Plane GetCullingPlane(int index)
		{
			if (index < 0 || index >= this.cullingPlaneCount || index >= 10)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &this.m_CullingPlanes.FixedElementField)
			{
				return new Plane(new Vector3(ptr[(IntPtr)(index * 4) * 4], ptr[(IntPtr)(index * 4 + 1) * 4], ptr[(IntPtr)(index * 4 + 2) * 4]), ptr[(IntPtr)(index * 4 + 3) * 4]);
			}
		}

		// Token: 0x060019AE RID: 6574 RVA: 0x0002D544 File Offset: 0x0002B744
		public unsafe void SetCullingPlane(int index, Plane plane)
		{
			if (index < 0 || index >= this.cullingPlaneCount || index >= 10)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &this.m_CullingPlanes.FixedElementField)
			{
				ptr[(IntPtr)(index * 4) * 4] = plane.normal.x;
				ptr[(IntPtr)(index * 4 + 1) * 4] = plane.normal.y;
				ptr[(IntPtr)(index * 4 + 2) * 4] = plane.normal.z;
				ptr[(IntPtr)(index * 4 + 3) * 4] = plane.distance;
			}
		}

		// Token: 0x04000964 RID: 2404
		private int m_IsOrthographic;

		// Token: 0x04000965 RID: 2405
		private LODParameters m_LodParameters;

		// Token: 0x04000966 RID: 2406
		[FixedBuffer(typeof(float), 40)]
		internal ScriptableCullingParameters.<m_CullingPlanes>__FixedBuffer4 m_CullingPlanes;

		// Token: 0x04000967 RID: 2407
		private int m_CullingPlaneCount;

		// Token: 0x04000968 RID: 2408
		private int m_CullingMask;

		// Token: 0x04000969 RID: 2409
		private long m_SceneMask;

		// Token: 0x0400096A RID: 2410
		[FixedBuffer(typeof(float), 32)]
		internal ScriptableCullingParameters.<m_LayerFarCullDistances>__FixedBuffer5 m_LayerFarCullDistances;

		// Token: 0x0400096B RID: 2411
		private int m_LayerCull;

		// Token: 0x0400096C RID: 2412
		private Matrix4x4 m_CullingMatrix;

		// Token: 0x0400096D RID: 2413
		private Vector3 m_Position;

		// Token: 0x0400096E RID: 2414
		private float m_shadowDistance;

		// Token: 0x0400096F RID: 2415
		private CullFlag m_CullingFlags;

		// Token: 0x04000970 RID: 2416
		private ReflectionProbeSortOptions m_ReflectionProbeSortOptions;

		// Token: 0x04000971 RID: 2417
		private CameraProperties m_CameraProperties;

		// Token: 0x04000972 RID: 2418
		private float m_AccurateoOcclusionThreshold;

		// Token: 0x04000973 RID: 2419
		public Matrix4x4 cullStereoView;

		// Token: 0x04000974 RID: 2420
		public Matrix4x4 cullStereoProj;

		// Token: 0x04000975 RID: 2421
		public float cullStereoSeparation;

		// Token: 0x04000976 RID: 2422
		private int padding2;

		// Token: 0x020002DE RID: 734
		[CompilerGenerated]
		[UnsafeValueType]
		[StructLayout(LayoutKind.Sequential, Size = 160)]
		public struct <m_CullingPlanes>__FixedBuffer4
		{
			// Token: 0x04000977 RID: 2423
			public float FixedElementField;
		}

		// Token: 0x020002DF RID: 735
		[CompilerGenerated]
		[UnsafeValueType]
		[StructLayout(LayoutKind.Sequential, Size = 128)]
		public struct <m_LayerFarCullDistances>__FixedBuffer5
		{
			// Token: 0x04000978 RID: 2424
			public float FixedElementField;
		}
	}
}
