﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002F7 RID: 759
	[Flags]
	public enum SortFlags
	{
		// Token: 0x040009D5 RID: 2517
		None = 0,
		// Token: 0x040009D6 RID: 2518
		SortingLayer = 1,
		// Token: 0x040009D7 RID: 2519
		RenderQueue = 2,
		// Token: 0x040009D8 RID: 2520
		BackToFront = 4,
		// Token: 0x040009D9 RID: 2521
		QuantizedFrontToBack = 8,
		// Token: 0x040009DA RID: 2522
		OptimizeStateChanges = 16,
		// Token: 0x040009DB RID: 2523
		CanvasOrder = 32,
		// Token: 0x040009DC RID: 2524
		RendererPriority = 64,
		// Token: 0x040009DD RID: 2525
		CommonOpaque = 59,
		// Token: 0x040009DE RID: 2526
		CommonTransparent = 23
	}
}
