﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x02000090 RID: 144
	public enum GraphicsJobsSyncPoint
	{
		// Token: 0x0400017F RID: 383
		EndOfFrame,
		// Token: 0x04000180 RID: 384
		AfterScriptUpdate,
		// Token: 0x04000181 RID: 385
		AfterScriptLateUpdate,
		// Token: 0x04000182 RID: 386
		WaitForPresent
	}
}
