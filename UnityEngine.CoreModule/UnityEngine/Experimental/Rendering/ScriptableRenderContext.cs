﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x02000204 RID: 516
	[NativeType("Runtime/Graphics/ScriptableRenderLoop/ScriptableRenderContext.h")]
	[NativeHeader("Runtime/Export/ScriptableRenderContext.bindings.h")]
	[NativeHeader("Runtime/UI/CanvasManager.h")]
	[NativeHeader("Runtime/UI/Canvas.h")]
	public struct ScriptableRenderContext
	{
		// Token: 0x06001174 RID: 4468 RVA: 0x0001DD31 File Offset: 0x0001BF31
		internal ScriptableRenderContext(IntPtr ptr)
		{
			this.m_Ptr = ptr;
		}

		// Token: 0x06001175 RID: 4469
		[FreeFunction("ScriptableRenderContext::BeginRenderPass")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void BeginRenderPassInternal(IntPtr _self, int w, int h, int samples, RenderPassAttachment[] colors, RenderPassAttachment depth);

		// Token: 0x06001176 RID: 4470
		[FreeFunction("ScriptableRenderContext::BeginSubPass")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void BeginSubPassInternal(IntPtr _self, RenderPassAttachment[] colors, RenderPassAttachment[] inputs, bool readOnlyDepth);

		// Token: 0x06001177 RID: 4471
		[FreeFunction("ScriptableRenderContext::EndRenderPass")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void EndRenderPassInternal(IntPtr _self);

		// Token: 0x06001178 RID: 4472 RVA: 0x0001DD3B File Offset: 0x0001BF3B
		private void Submit_Internal()
		{
			ScriptableRenderContext.Submit_Internal_Injected(ref this);
		}

		// Token: 0x06001179 RID: 4473 RVA: 0x0001DD43 File Offset: 0x0001BF43
		private void DrawRenderers_Internal(FilterResults renderers, ref DrawRendererSettings drawSettings, FilterRenderersSettings filterSettings)
		{
			ScriptableRenderContext.DrawRenderers_Internal_Injected(ref this, ref renderers, ref drawSettings, ref filterSettings);
		}

		// Token: 0x0600117A RID: 4474 RVA: 0x0001DD50 File Offset: 0x0001BF50
		private void DrawRenderers_StateBlock_Internal(FilterResults renderers, ref DrawRendererSettings drawSettings, FilterRenderersSettings filterSettings, RenderStateBlock stateBlock)
		{
			ScriptableRenderContext.DrawRenderers_StateBlock_Internal_Injected(ref this, ref renderers, ref drawSettings, ref filterSettings, ref stateBlock);
		}

		// Token: 0x0600117B RID: 4475 RVA: 0x0001DD5F File Offset: 0x0001BF5F
		private void DrawRenderers_StateMap_Internal(FilterResults renderers, ref DrawRendererSettings drawSettings, FilterRenderersSettings filterSettings, Array stateMap, int stateMapLength)
		{
			ScriptableRenderContext.DrawRenderers_StateMap_Internal_Injected(ref this, ref renderers, ref drawSettings, ref filterSettings, stateMap, stateMapLength);
		}

		// Token: 0x0600117C RID: 4476 RVA: 0x0001DD70 File Offset: 0x0001BF70
		private void DrawShadows_Internal(ref DrawShadowsSettings settings)
		{
			ScriptableRenderContext.DrawShadows_Internal_Injected(ref this, ref settings);
		}

		// Token: 0x0600117D RID: 4477 RVA: 0x0001DD79 File Offset: 0x0001BF79
		private void ExecuteCommandBuffer_Internal(CommandBuffer commandBuffer)
		{
			ScriptableRenderContext.ExecuteCommandBuffer_Internal_Injected(ref this, commandBuffer);
		}

		// Token: 0x0600117E RID: 4478 RVA: 0x0001DD82 File Offset: 0x0001BF82
		private void ExecuteCommandBufferAsync_Internal(CommandBuffer commandBuffer, ComputeQueueType queueType)
		{
			ScriptableRenderContext.ExecuteCommandBufferAsync_Internal_Injected(ref this, commandBuffer, queueType);
		}

		// Token: 0x0600117F RID: 4479 RVA: 0x0001DD8C File Offset: 0x0001BF8C
		private void SetupCameraProperties_Internal(Camera camera, bool stereoSetup)
		{
			ScriptableRenderContext.SetupCameraProperties_Internal_Injected(ref this, camera, stereoSetup);
		}

		// Token: 0x06001180 RID: 4480 RVA: 0x0001DD96 File Offset: 0x0001BF96
		private void StereoEndRender_Internal(Camera camera)
		{
			ScriptableRenderContext.StereoEndRender_Internal_Injected(ref this, camera);
		}

		// Token: 0x06001181 RID: 4481 RVA: 0x0001DD9F File Offset: 0x0001BF9F
		private void StartMultiEye_Internal(Camera camera)
		{
			ScriptableRenderContext.StartMultiEye_Internal_Injected(ref this, camera);
		}

		// Token: 0x06001182 RID: 4482 RVA: 0x0001DDA8 File Offset: 0x0001BFA8
		private void StopMultiEye_Internal(Camera camera)
		{
			ScriptableRenderContext.StopMultiEye_Internal_Injected(ref this, camera);
		}

		// Token: 0x06001183 RID: 4483 RVA: 0x0001DDB1 File Offset: 0x0001BFB1
		private void DrawSkybox_Internal(Camera camera)
		{
			ScriptableRenderContext.DrawSkybox_Internal_Injected(ref this, camera);
		}

		// Token: 0x06001184 RID: 4484 RVA: 0x0001DDBC File Offset: 0x0001BFBC
		internal IntPtr Internal_GetPtr()
		{
			return this.m_Ptr;
		}

		// Token: 0x06001185 RID: 4485 RVA: 0x0001DDD7 File Offset: 0x0001BFD7
		public void Submit()
		{
			this.CheckValid();
			this.Submit_Internal();
		}

		// Token: 0x06001186 RID: 4486 RVA: 0x0001DDE6 File Offset: 0x0001BFE6
		public void DrawRenderers(FilterResults renderers, ref DrawRendererSettings drawSettings, FilterRenderersSettings filterSettings)
		{
			this.CheckValid();
			this.DrawRenderers_Internal(renderers, ref drawSettings, filterSettings);
		}

		// Token: 0x06001187 RID: 4487 RVA: 0x0001DDF8 File Offset: 0x0001BFF8
		public void DrawRenderers(FilterResults renderers, ref DrawRendererSettings drawSettings, FilterRenderersSettings filterSettings, RenderStateBlock stateBlock)
		{
			this.CheckValid();
			this.DrawRenderers_StateBlock_Internal(renderers, ref drawSettings, filterSettings, stateBlock);
		}

		// Token: 0x06001188 RID: 4488 RVA: 0x0001DE0C File Offset: 0x0001C00C
		public void DrawRenderers(FilterResults renderers, ref DrawRendererSettings drawSettings, FilterRenderersSettings filterSettings, List<RenderStateMapping> stateMap)
		{
			this.CheckValid();
			this.DrawRenderers_StateMap_Internal(renderers, ref drawSettings, filterSettings, NoAllocHelpers.ExtractArrayFromList(stateMap), stateMap.Count);
		}

		// Token: 0x06001189 RID: 4489 RVA: 0x0001DE2C File Offset: 0x0001C02C
		public void DrawShadows(ref DrawShadowsSettings settings)
		{
			this.CheckValid();
			this.DrawShadows_Internal(ref settings);
		}

		// Token: 0x0600118A RID: 4490 RVA: 0x0001DE3C File Offset: 0x0001C03C
		public void ExecuteCommandBuffer(CommandBuffer commandBuffer)
		{
			if (commandBuffer == null)
			{
				throw new ArgumentNullException("commandBuffer");
			}
			this.CheckValid();
			this.ExecuteCommandBuffer_Internal(commandBuffer);
		}

		// Token: 0x0600118B RID: 4491 RVA: 0x0001DE5D File Offset: 0x0001C05D
		public void ExecuteCommandBufferAsync(CommandBuffer commandBuffer, ComputeQueueType queueType)
		{
			if (commandBuffer == null)
			{
				throw new ArgumentNullException("commandBuffer");
			}
			this.CheckValid();
			this.ExecuteCommandBufferAsync_Internal(commandBuffer, queueType);
		}

		// Token: 0x0600118C RID: 4492 RVA: 0x0001DE7F File Offset: 0x0001C07F
		public void SetupCameraProperties(Camera camera)
		{
			this.CheckValid();
			this.SetupCameraProperties_Internal(camera, false);
		}

		// Token: 0x0600118D RID: 4493 RVA: 0x0001DE90 File Offset: 0x0001C090
		public void SetupCameraProperties(Camera camera, bool stereoSetup)
		{
			this.CheckValid();
			this.SetupCameraProperties_Internal(camera, stereoSetup);
		}

		// Token: 0x0600118E RID: 4494 RVA: 0x0001DEA1 File Offset: 0x0001C0A1
		public void StereoEndRender(Camera camera)
		{
			this.CheckValid();
			this.StereoEndRender_Internal(camera);
		}

		// Token: 0x0600118F RID: 4495 RVA: 0x0001DEB1 File Offset: 0x0001C0B1
		public void StartMultiEye(Camera camera)
		{
			this.CheckValid();
			this.StartMultiEye_Internal(camera);
		}

		// Token: 0x06001190 RID: 4496 RVA: 0x0001DEC1 File Offset: 0x0001C0C1
		public void StopMultiEye(Camera camera)
		{
			this.CheckValid();
			this.StopMultiEye_Internal(camera);
		}

		// Token: 0x06001191 RID: 4497 RVA: 0x0001DED1 File Offset: 0x0001C0D1
		public void DrawSkybox(Camera camera)
		{
			this.CheckValid();
			this.DrawSkybox_Internal(camera);
		}

		// Token: 0x06001192 RID: 4498 RVA: 0x0001DEE1 File Offset: 0x0001C0E1
		internal void CheckValid()
		{
			if (this.m_Ptr.ToInt64() == 0L)
			{
				throw new ArgumentException("Invalid ScriptableRenderContext.  This can be caused by allocating a context in user code.");
			}
		}

		// Token: 0x06001193 RID: 4499
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Submit_Internal_Injected(ref ScriptableRenderContext _unity_self);

		// Token: 0x06001194 RID: 4500
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DrawRenderers_Internal_Injected(ref ScriptableRenderContext _unity_self, ref FilterResults renderers, ref DrawRendererSettings drawSettings, ref FilterRenderersSettings filterSettings);

		// Token: 0x06001195 RID: 4501
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DrawRenderers_StateBlock_Internal_Injected(ref ScriptableRenderContext _unity_self, ref FilterResults renderers, ref DrawRendererSettings drawSettings, ref FilterRenderersSettings filterSettings, ref RenderStateBlock stateBlock);

		// Token: 0x06001196 RID: 4502
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DrawRenderers_StateMap_Internal_Injected(ref ScriptableRenderContext _unity_self, ref FilterResults renderers, ref DrawRendererSettings drawSettings, ref FilterRenderersSettings filterSettings, Array stateMap, int stateMapLength);

		// Token: 0x06001197 RID: 4503
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DrawShadows_Internal_Injected(ref ScriptableRenderContext _unity_self, ref DrawShadowsSettings settings);

		// Token: 0x06001198 RID: 4504
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ExecuteCommandBuffer_Internal_Injected(ref ScriptableRenderContext _unity_self, CommandBuffer commandBuffer);

		// Token: 0x06001199 RID: 4505
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ExecuteCommandBufferAsync_Internal_Injected(ref ScriptableRenderContext _unity_self, CommandBuffer commandBuffer, ComputeQueueType queueType);

		// Token: 0x0600119A RID: 4506
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetupCameraProperties_Internal_Injected(ref ScriptableRenderContext _unity_self, Camera camera, bool stereoSetup);

		// Token: 0x0600119B RID: 4507
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void StereoEndRender_Internal_Injected(ref ScriptableRenderContext _unity_self, Camera camera);

		// Token: 0x0600119C RID: 4508
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void StartMultiEye_Internal_Injected(ref ScriptableRenderContext _unity_self, Camera camera);

		// Token: 0x0600119D RID: 4509
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void StopMultiEye_Internal_Injected(ref ScriptableRenderContext _unity_self, Camera camera);

		// Token: 0x0600119E RID: 4510
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DrawSkybox_Internal_Injected(ref ScriptableRenderContext _unity_self, Camera camera);

		// Token: 0x04000751 RID: 1873
		private IntPtr m_Ptr;
	}
}
