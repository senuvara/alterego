﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x02000306 RID: 774
	public class RenderPass : IDisposable
	{
		// Token: 0x06001ACE RID: 6862 RVA: 0x0002F354 File Offset: 0x0002D554
		public RenderPass(ScriptableRenderContext ctx, int w, int h, int samples, RenderPassAttachment[] colors, RenderPassAttachment depth = null)
		{
			this.width = w;
			this.height = h;
			this.sampleCount = samples;
			this.colorAttachments = colors;
			this.depthAttachment = depth;
			this.context = ctx;
			ScriptableRenderContext.BeginRenderPassInternal(ctx.Internal_GetPtr(), w, h, samples, colors, depth);
		}

		// Token: 0x170004FC RID: 1276
		// (get) Token: 0x06001ACF RID: 6863 RVA: 0x0002F3AC File Offset: 0x0002D5AC
		// (set) Token: 0x06001AD0 RID: 6864 RVA: 0x0002F3C6 File Offset: 0x0002D5C6
		public RenderPassAttachment[] colorAttachments
		{
			[CompilerGenerated]
			get
			{
				return this.<colorAttachments>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<colorAttachments>k__BackingField = value;
			}
		}

		// Token: 0x170004FD RID: 1277
		// (get) Token: 0x06001AD1 RID: 6865 RVA: 0x0002F3D0 File Offset: 0x0002D5D0
		// (set) Token: 0x06001AD2 RID: 6866 RVA: 0x0002F3EA File Offset: 0x0002D5EA
		public RenderPassAttachment depthAttachment
		{
			[CompilerGenerated]
			get
			{
				return this.<depthAttachment>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<depthAttachment>k__BackingField = value;
			}
		}

		// Token: 0x170004FE RID: 1278
		// (get) Token: 0x06001AD3 RID: 6867 RVA: 0x0002F3F4 File Offset: 0x0002D5F4
		// (set) Token: 0x06001AD4 RID: 6868 RVA: 0x0002F40E File Offset: 0x0002D60E
		public int width
		{
			[CompilerGenerated]
			get
			{
				return this.<width>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<width>k__BackingField = value;
			}
		}

		// Token: 0x170004FF RID: 1279
		// (get) Token: 0x06001AD5 RID: 6869 RVA: 0x0002F418 File Offset: 0x0002D618
		// (set) Token: 0x06001AD6 RID: 6870 RVA: 0x0002F432 File Offset: 0x0002D632
		public int height
		{
			[CompilerGenerated]
			get
			{
				return this.<height>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<height>k__BackingField = value;
			}
		}

		// Token: 0x17000500 RID: 1280
		// (get) Token: 0x06001AD7 RID: 6871 RVA: 0x0002F43C File Offset: 0x0002D63C
		// (set) Token: 0x06001AD8 RID: 6872 RVA: 0x0002F456 File Offset: 0x0002D656
		public int sampleCount
		{
			[CompilerGenerated]
			get
			{
				return this.<sampleCount>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<sampleCount>k__BackingField = value;
			}
		}

		// Token: 0x17000501 RID: 1281
		// (get) Token: 0x06001AD9 RID: 6873 RVA: 0x0002F460 File Offset: 0x0002D660
		// (set) Token: 0x06001ADA RID: 6874 RVA: 0x0002F47A File Offset: 0x0002D67A
		public ScriptableRenderContext context
		{
			[CompilerGenerated]
			get
			{
				return this.<context>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<context>k__BackingField = value;
			}
		}

		// Token: 0x06001ADB RID: 6875 RVA: 0x0002F484 File Offset: 0x0002D684
		public void Dispose()
		{
			ScriptableRenderContext.EndRenderPassInternal(this.context.Internal_GetPtr());
		}

		// Token: 0x04000A1B RID: 2587
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private RenderPassAttachment[] <colorAttachments>k__BackingField;

		// Token: 0x04000A1C RID: 2588
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private RenderPassAttachment <depthAttachment>k__BackingField;

		// Token: 0x04000A1D RID: 2589
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <width>k__BackingField;

		// Token: 0x04000A1E RID: 2590
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <height>k__BackingField;

		// Token: 0x04000A1F RID: 2591
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <sampleCount>k__BackingField;

		// Token: 0x04000A20 RID: 2592
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ScriptableRenderContext <context>k__BackingField;

		// Token: 0x02000307 RID: 775
		public class SubPass : IDisposable
		{
			// Token: 0x06001ADC RID: 6876 RVA: 0x0002F4A8 File Offset: 0x0002D6A8
			public SubPass(RenderPass renderPass, RenderPassAttachment[] colors, RenderPassAttachment[] inputs, bool readOnlyDepth = false)
			{
				ScriptableRenderContext.BeginSubPassInternal(renderPass.context.Internal_GetPtr(), (colors == null) ? new RenderPassAttachment[0] : colors, (inputs == null) ? new RenderPassAttachment[0] : inputs, readOnlyDepth);
			}

			// Token: 0x06001ADD RID: 6877 RVA: 0x00007476 File Offset: 0x00005676
			public void Dispose()
			{
			}
		}
	}
}
