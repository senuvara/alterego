﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002E2 RID: 738
	public enum DrawRendererSortMode
	{
		// Token: 0x04000980 RID: 2432
		Perspective,
		// Token: 0x04000981 RID: 2433
		Orthographic,
		// Token: 0x04000982 RID: 2434
		CustomAxis
	}
}
