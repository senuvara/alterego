﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002F9 RID: 761
	public class SupportedRenderingFeatures
	{
		// Token: 0x06001A43 RID: 6723 RVA: 0x0002E59C File Offset: 0x0002C79C
		public SupportedRenderingFeatures()
		{
		}

		// Token: 0x170004DF RID: 1247
		// (get) Token: 0x06001A44 RID: 6724 RVA: 0x0002E60C File Offset: 0x0002C80C
		// (set) Token: 0x06001A45 RID: 6725 RVA: 0x0002E63A File Offset: 0x0002C83A
		public static SupportedRenderingFeatures active
		{
			get
			{
				if (SupportedRenderingFeatures.s_Active == null)
				{
					SupportedRenderingFeatures.s_Active = new SupportedRenderingFeatures();
				}
				return SupportedRenderingFeatures.s_Active;
			}
			set
			{
				SupportedRenderingFeatures.s_Active = value;
			}
		}

		// Token: 0x170004E0 RID: 1248
		// (get) Token: 0x06001A46 RID: 6726 RVA: 0x0002E644 File Offset: 0x0002C844
		// (set) Token: 0x06001A47 RID: 6727 RVA: 0x0002E65E File Offset: 0x0002C85E
		public SupportedRenderingFeatures.ReflectionProbeSupportFlags reflectionProbeSupportFlags
		{
			[CompilerGenerated]
			get
			{
				return this.<reflectionProbeSupportFlags>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<reflectionProbeSupportFlags>k__BackingField = value;
			}
		} = SupportedRenderingFeatures.ReflectionProbeSupportFlags.None;

		// Token: 0x170004E1 RID: 1249
		// (get) Token: 0x06001A48 RID: 6728 RVA: 0x0002E668 File Offset: 0x0002C868
		// (set) Token: 0x06001A49 RID: 6729 RVA: 0x0002E682 File Offset: 0x0002C882
		public SupportedRenderingFeatures.LightmapMixedBakeMode defaultMixedLightingMode
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultMixedLightingMode>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultMixedLightingMode>k__BackingField = value;
			}
		} = SupportedRenderingFeatures.LightmapMixedBakeMode.None;

		// Token: 0x170004E2 RID: 1250
		// (get) Token: 0x06001A4A RID: 6730 RVA: 0x0002E68C File Offset: 0x0002C88C
		// (set) Token: 0x06001A4B RID: 6731 RVA: 0x0002E6A6 File Offset: 0x0002C8A6
		public SupportedRenderingFeatures.LightmapMixedBakeMode supportedMixedLightingModes
		{
			[CompilerGenerated]
			get
			{
				return this.<supportedMixedLightingModes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<supportedMixedLightingModes>k__BackingField = value;
			}
		} = SupportedRenderingFeatures.LightmapMixedBakeMode.IndirectOnly | SupportedRenderingFeatures.LightmapMixedBakeMode.Subtractive | SupportedRenderingFeatures.LightmapMixedBakeMode.Shadowmask;

		// Token: 0x170004E3 RID: 1251
		// (get) Token: 0x06001A4C RID: 6732 RVA: 0x0002E6B0 File Offset: 0x0002C8B0
		// (set) Token: 0x06001A4D RID: 6733 RVA: 0x0002E6CA File Offset: 0x0002C8CA
		public LightmapBakeType supportedLightmapBakeTypes
		{
			[CompilerGenerated]
			get
			{
				return this.<supportedLightmapBakeTypes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<supportedLightmapBakeTypes>k__BackingField = value;
			}
		} = LightmapBakeType.Realtime | LightmapBakeType.Baked | LightmapBakeType.Mixed;

		// Token: 0x170004E4 RID: 1252
		// (get) Token: 0x06001A4E RID: 6734 RVA: 0x0002E6D4 File Offset: 0x0002C8D4
		// (set) Token: 0x06001A4F RID: 6735 RVA: 0x0002E6EE File Offset: 0x0002C8EE
		public LightmapsMode supportedLightmapsModes
		{
			[CompilerGenerated]
			get
			{
				return this.<supportedLightmapsModes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<supportedLightmapsModes>k__BackingField = value;
			}
		} = LightmapsMode.CombinedDirectional;

		// Token: 0x170004E5 RID: 1253
		// (get) Token: 0x06001A50 RID: 6736 RVA: 0x0002E6F8 File Offset: 0x0002C8F8
		// (set) Token: 0x06001A51 RID: 6737 RVA: 0x0002E712 File Offset: 0x0002C912
		public bool rendererSupportsLightProbeProxyVolumes
		{
			[CompilerGenerated]
			get
			{
				return this.<rendererSupportsLightProbeProxyVolumes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<rendererSupportsLightProbeProxyVolumes>k__BackingField = value;
			}
		} = true;

		// Token: 0x170004E6 RID: 1254
		// (get) Token: 0x06001A52 RID: 6738 RVA: 0x0002E71C File Offset: 0x0002C91C
		// (set) Token: 0x06001A53 RID: 6739 RVA: 0x0002E736 File Offset: 0x0002C936
		public bool rendererSupportsMotionVectors
		{
			[CompilerGenerated]
			get
			{
				return this.<rendererSupportsMotionVectors>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<rendererSupportsMotionVectors>k__BackingField = value;
			}
		} = true;

		// Token: 0x170004E7 RID: 1255
		// (get) Token: 0x06001A54 RID: 6740 RVA: 0x0002E740 File Offset: 0x0002C940
		// (set) Token: 0x06001A55 RID: 6741 RVA: 0x0002E75A File Offset: 0x0002C95A
		public bool rendererSupportsReceiveShadows
		{
			[CompilerGenerated]
			get
			{
				return this.<rendererSupportsReceiveShadows>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<rendererSupportsReceiveShadows>k__BackingField = value;
			}
		} = true;

		// Token: 0x170004E8 RID: 1256
		// (get) Token: 0x06001A56 RID: 6742 RVA: 0x0002E764 File Offset: 0x0002C964
		// (set) Token: 0x06001A57 RID: 6743 RVA: 0x0002E77E File Offset: 0x0002C97E
		public bool rendererSupportsReflectionProbes
		{
			[CompilerGenerated]
			get
			{
				return this.<rendererSupportsReflectionProbes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<rendererSupportsReflectionProbes>k__BackingField = value;
			}
		} = true;

		// Token: 0x170004E9 RID: 1257
		// (get) Token: 0x06001A58 RID: 6744 RVA: 0x0002E788 File Offset: 0x0002C988
		// (set) Token: 0x06001A59 RID: 6745 RVA: 0x0002E7A2 File Offset: 0x0002C9A2
		public bool rendererSupportsRendererPriority
		{
			[CompilerGenerated]
			get
			{
				return this.<rendererSupportsRendererPriority>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<rendererSupportsRendererPriority>k__BackingField = value;
			}
		} = false;

		// Token: 0x170004EA RID: 1258
		// (get) Token: 0x06001A5A RID: 6746 RVA: 0x0002E7AC File Offset: 0x0002C9AC
		// (set) Token: 0x06001A5B RID: 6747 RVA: 0x0002E7C6 File Offset: 0x0002C9C6
		public bool rendererOverridesEnvironmentLighting
		{
			[CompilerGenerated]
			get
			{
				return this.<rendererOverridesEnvironmentLighting>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<rendererOverridesEnvironmentLighting>k__BackingField = value;
			}
		} = false;

		// Token: 0x170004EB RID: 1259
		// (get) Token: 0x06001A5C RID: 6748 RVA: 0x0002E7D0 File Offset: 0x0002C9D0
		// (set) Token: 0x06001A5D RID: 6749 RVA: 0x0002E7EA File Offset: 0x0002C9EA
		public bool rendererOverridesFog
		{
			[CompilerGenerated]
			get
			{
				return this.<rendererOverridesFog>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<rendererOverridesFog>k__BackingField = value;
			}
		} = false;

		// Token: 0x170004EC RID: 1260
		// (get) Token: 0x06001A5E RID: 6750 RVA: 0x0002E7F4 File Offset: 0x0002C9F4
		// (set) Token: 0x06001A5F RID: 6751 RVA: 0x0002E80E File Offset: 0x0002CA0E
		public bool rendererOverridesOtherLightingSettings
		{
			[CompilerGenerated]
			get
			{
				return this.<rendererOverridesOtherLightingSettings>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<rendererOverridesOtherLightingSettings>k__BackingField = value;
			}
		} = false;

		// Token: 0x06001A60 RID: 6752 RVA: 0x0002E818 File Offset: 0x0002CA18
		internal unsafe static MixedLightingMode FallbackMixedLightingMode()
		{
			MixedLightingMode result;
			SupportedRenderingFeatures.FallbackMixedLightingModeByRef(new IntPtr((void*)(&result)));
			return result;
		}

		// Token: 0x06001A61 RID: 6753 RVA: 0x0002E83C File Offset: 0x0002CA3C
		[RequiredByNativeCode]
		internal unsafe static void FallbackMixedLightingModeByRef(IntPtr fallbackModePtr)
		{
			MixedLightingMode* ptr = (MixedLightingMode*)((void*)fallbackModePtr);
			if (SupportedRenderingFeatures.active.defaultMixedLightingMode != SupportedRenderingFeatures.LightmapMixedBakeMode.None && (SupportedRenderingFeatures.active.supportedMixedLightingModes & SupportedRenderingFeatures.active.defaultMixedLightingMode) == SupportedRenderingFeatures.active.defaultMixedLightingMode)
			{
				SupportedRenderingFeatures.LightmapMixedBakeMode defaultMixedLightingMode = SupportedRenderingFeatures.active.defaultMixedLightingMode;
				if (defaultMixedLightingMode != SupportedRenderingFeatures.LightmapMixedBakeMode.Shadowmask)
				{
					if (defaultMixedLightingMode != SupportedRenderingFeatures.LightmapMixedBakeMode.Subtractive)
					{
						*ptr = MixedLightingMode.IndirectOnly;
					}
					else
					{
						*ptr = MixedLightingMode.Subtractive;
					}
				}
				else
				{
					*ptr = MixedLightingMode.Shadowmask;
				}
			}
			else if (SupportedRenderingFeatures.IsMixedLightingModeSupported(MixedLightingMode.Shadowmask))
			{
				*ptr = MixedLightingMode.Shadowmask;
			}
			else if (SupportedRenderingFeatures.IsMixedLightingModeSupported(MixedLightingMode.Subtractive))
			{
				*ptr = MixedLightingMode.Subtractive;
			}
			else
			{
				*ptr = MixedLightingMode.IndirectOnly;
			}
		}

		// Token: 0x06001A62 RID: 6754 RVA: 0x0002E8EC File Offset: 0x0002CAEC
		internal unsafe static bool IsMixedLightingModeSupported(MixedLightingMode mixedMode)
		{
			bool result;
			SupportedRenderingFeatures.IsMixedLightingModeSupportedByRef(mixedMode, new IntPtr((void*)(&result)));
			return result;
		}

		// Token: 0x06001A63 RID: 6755 RVA: 0x0002E910 File Offset: 0x0002CB10
		[RequiredByNativeCode]
		internal unsafe static void IsMixedLightingModeSupportedByRef(MixedLightingMode mixedMode, IntPtr isSupportedPtr)
		{
			bool* ptr = (bool*)((void*)isSupportedPtr);
			if (!SupportedRenderingFeatures.IsLightmapBakeTypeSupported(LightmapBakeType.Mixed))
			{
				*ptr = false;
			}
			else
			{
				*ptr = ((mixedMode == MixedLightingMode.IndirectOnly && (SupportedRenderingFeatures.active.supportedMixedLightingModes & SupportedRenderingFeatures.LightmapMixedBakeMode.IndirectOnly) == SupportedRenderingFeatures.LightmapMixedBakeMode.IndirectOnly) || (mixedMode == MixedLightingMode.Subtractive && (SupportedRenderingFeatures.active.supportedMixedLightingModes & SupportedRenderingFeatures.LightmapMixedBakeMode.Subtractive) == SupportedRenderingFeatures.LightmapMixedBakeMode.Subtractive) || (mixedMode == MixedLightingMode.Shadowmask && (SupportedRenderingFeatures.active.supportedMixedLightingModes & SupportedRenderingFeatures.LightmapMixedBakeMode.Shadowmask) == SupportedRenderingFeatures.LightmapMixedBakeMode.Shadowmask));
			}
		}

		// Token: 0x06001A64 RID: 6756 RVA: 0x0002E988 File Offset: 0x0002CB88
		internal unsafe static bool IsLightmapBakeTypeSupported(LightmapBakeType bakeType)
		{
			bool result;
			SupportedRenderingFeatures.IsLightmapBakeTypeSupportedByRef(bakeType, new IntPtr((void*)(&result)));
			return result;
		}

		// Token: 0x06001A65 RID: 6757 RVA: 0x0002E9AC File Offset: 0x0002CBAC
		[RequiredByNativeCode]
		internal unsafe static void IsLightmapBakeTypeSupportedByRef(LightmapBakeType bakeType, IntPtr isSupportedPtr)
		{
			bool* ptr = (bool*)((void*)isSupportedPtr);
			if (bakeType == LightmapBakeType.Mixed)
			{
				bool flag = SupportedRenderingFeatures.IsLightmapBakeTypeSupported(LightmapBakeType.Baked);
				if (!flag || SupportedRenderingFeatures.active.supportedMixedLightingModes == SupportedRenderingFeatures.LightmapMixedBakeMode.None)
				{
					*ptr = false;
					return;
				}
			}
			*ptr = ((SupportedRenderingFeatures.active.supportedLightmapBakeTypes & bakeType) == bakeType);
		}

		// Token: 0x06001A66 RID: 6758 RVA: 0x0002EA00 File Offset: 0x0002CC00
		internal unsafe static bool IsLightmapsModeSupported(LightmapsMode mode)
		{
			bool result;
			SupportedRenderingFeatures.IsLightmapsModeSupportedByRef(mode, new IntPtr((void*)(&result)));
			return result;
		}

		// Token: 0x06001A67 RID: 6759 RVA: 0x0002EA24 File Offset: 0x0002CC24
		[RequiredByNativeCode]
		internal unsafe static void IsLightmapsModeSupportedByRef(LightmapsMode mode, IntPtr isSupportedPtr)
		{
			bool* ptr = (bool*)((void*)isSupportedPtr);
			*ptr = ((SupportedRenderingFeatures.active.supportedLightmapsModes & mode) == mode);
		}

		// Token: 0x06001A68 RID: 6760 RVA: 0x0002EA4A File Offset: 0x0002CC4A
		// Note: this type is marked as 'beforefieldinit'.
		static SupportedRenderingFeatures()
		{
		}

		// Token: 0x040009EB RID: 2539
		private static SupportedRenderingFeatures s_Active = new SupportedRenderingFeatures();

		// Token: 0x040009EC RID: 2540
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private SupportedRenderingFeatures.ReflectionProbeSupportFlags <reflectionProbeSupportFlags>k__BackingField;

		// Token: 0x040009ED RID: 2541
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private SupportedRenderingFeatures.LightmapMixedBakeMode <defaultMixedLightingMode>k__BackingField;

		// Token: 0x040009EE RID: 2542
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private SupportedRenderingFeatures.LightmapMixedBakeMode <supportedMixedLightingModes>k__BackingField;

		// Token: 0x040009EF RID: 2543
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private LightmapBakeType <supportedLightmapBakeTypes>k__BackingField;

		// Token: 0x040009F0 RID: 2544
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private LightmapsMode <supportedLightmapsModes>k__BackingField;

		// Token: 0x040009F1 RID: 2545
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <rendererSupportsLightProbeProxyVolumes>k__BackingField;

		// Token: 0x040009F2 RID: 2546
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <rendererSupportsMotionVectors>k__BackingField;

		// Token: 0x040009F3 RID: 2547
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <rendererSupportsReceiveShadows>k__BackingField;

		// Token: 0x040009F4 RID: 2548
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <rendererSupportsReflectionProbes>k__BackingField;

		// Token: 0x040009F5 RID: 2549
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <rendererSupportsRendererPriority>k__BackingField;

		// Token: 0x040009F6 RID: 2550
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <rendererOverridesEnvironmentLighting>k__BackingField;

		// Token: 0x040009F7 RID: 2551
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <rendererOverridesFog>k__BackingField;

		// Token: 0x040009F8 RID: 2552
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <rendererOverridesOtherLightingSettings>k__BackingField;

		// Token: 0x020002FA RID: 762
		[Flags]
		public enum ReflectionProbeSupportFlags
		{
			// Token: 0x040009FA RID: 2554
			None = 0,
			// Token: 0x040009FB RID: 2555
			Rotation = 1
		}

		// Token: 0x020002FB RID: 763
		[Flags]
		public enum LightmapMixedBakeMode
		{
			// Token: 0x040009FD RID: 2557
			None = 0,
			// Token: 0x040009FE RID: 2558
			IndirectOnly = 1,
			// Token: 0x040009FF RID: 2559
			Subtractive = 2,
			// Token: 0x04000A00 RID: 2560
			Shadowmask = 4
		}
	}
}
