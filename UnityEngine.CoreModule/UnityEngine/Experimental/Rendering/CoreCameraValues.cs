﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002D7 RID: 727
	[UsedByNativeCode]
	public struct CoreCameraValues
	{
		// Token: 0x04000935 RID: 2357
		private int filterMode;

		// Token: 0x04000936 RID: 2358
		private uint cullingMask;

		// Token: 0x04000937 RID: 2359
		private int guid;

		// Token: 0x04000938 RID: 2360
		private int renderImmediateObjects;
	}
}
