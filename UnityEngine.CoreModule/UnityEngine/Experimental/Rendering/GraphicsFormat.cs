﻿using System;
using System.ComponentModel;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020000D5 RID: 213
	public enum GraphicsFormat
	{
		// Token: 0x040002C5 RID: 709
		None,
		// Token: 0x040002C6 RID: 710
		R8_SRGB,
		// Token: 0x040002C7 RID: 711
		R8G8_SRGB,
		// Token: 0x040002C8 RID: 712
		R8G8B8_SRGB,
		// Token: 0x040002C9 RID: 713
		R8G8B8A8_SRGB,
		// Token: 0x040002CA RID: 714
		R8_UNorm,
		// Token: 0x040002CB RID: 715
		R8G8_UNorm,
		// Token: 0x040002CC RID: 716
		R8G8B8_UNorm,
		// Token: 0x040002CD RID: 717
		R8G8B8A8_UNorm,
		// Token: 0x040002CE RID: 718
		R8_SNorm,
		// Token: 0x040002CF RID: 719
		R8G8_SNorm,
		// Token: 0x040002D0 RID: 720
		R8G8B8_SNorm,
		// Token: 0x040002D1 RID: 721
		R8G8B8A8_SNorm,
		// Token: 0x040002D2 RID: 722
		R8_UInt,
		// Token: 0x040002D3 RID: 723
		R8G8_UInt,
		// Token: 0x040002D4 RID: 724
		R8G8B8_UInt,
		// Token: 0x040002D5 RID: 725
		R8G8B8A8_UInt,
		// Token: 0x040002D6 RID: 726
		R8_SInt,
		// Token: 0x040002D7 RID: 727
		R8G8_SInt,
		// Token: 0x040002D8 RID: 728
		R8G8B8_SInt,
		// Token: 0x040002D9 RID: 729
		R8G8B8A8_SInt,
		// Token: 0x040002DA RID: 730
		R16_UNorm,
		// Token: 0x040002DB RID: 731
		R16G16_UNorm,
		// Token: 0x040002DC RID: 732
		R16G16B16_UNorm,
		// Token: 0x040002DD RID: 733
		R16G16B16A16_UNorm,
		// Token: 0x040002DE RID: 734
		R16_SNorm,
		// Token: 0x040002DF RID: 735
		R16G16_SNorm,
		// Token: 0x040002E0 RID: 736
		R16G16B16_SNorm,
		// Token: 0x040002E1 RID: 737
		R16G16B16A16_SNorm,
		// Token: 0x040002E2 RID: 738
		R16_UInt,
		// Token: 0x040002E3 RID: 739
		R16G16_UInt,
		// Token: 0x040002E4 RID: 740
		R16G16B16_UInt,
		// Token: 0x040002E5 RID: 741
		R16G16B16A16_UInt,
		// Token: 0x040002E6 RID: 742
		R16_SInt,
		// Token: 0x040002E7 RID: 743
		R16G16_SInt,
		// Token: 0x040002E8 RID: 744
		R16G16B16_SInt,
		// Token: 0x040002E9 RID: 745
		R16G16B16A16_SInt,
		// Token: 0x040002EA RID: 746
		R32_UInt,
		// Token: 0x040002EB RID: 747
		R32G32_UInt,
		// Token: 0x040002EC RID: 748
		R32G32B32_UInt,
		// Token: 0x040002ED RID: 749
		R32G32B32A32_UInt,
		// Token: 0x040002EE RID: 750
		R32_SInt,
		// Token: 0x040002EF RID: 751
		R32G32_SInt,
		// Token: 0x040002F0 RID: 752
		R32G32B32_SInt,
		// Token: 0x040002F1 RID: 753
		R32G32B32A32_SInt,
		// Token: 0x040002F2 RID: 754
		R16_SFloat,
		// Token: 0x040002F3 RID: 755
		R16G16_SFloat,
		// Token: 0x040002F4 RID: 756
		R16G16B16_SFloat,
		// Token: 0x040002F5 RID: 757
		R16G16B16A16_SFloat,
		// Token: 0x040002F6 RID: 758
		R32_SFloat,
		// Token: 0x040002F7 RID: 759
		R32G32_SFloat,
		// Token: 0x040002F8 RID: 760
		R32G32B32_SFloat,
		// Token: 0x040002F9 RID: 761
		R32G32B32A32_SFloat,
		// Token: 0x040002FA RID: 762
		B8G8R8_SRGB = 56,
		// Token: 0x040002FB RID: 763
		B8G8R8A8_SRGB,
		// Token: 0x040002FC RID: 764
		B8G8R8_UNorm,
		// Token: 0x040002FD RID: 765
		B8G8R8A8_UNorm,
		// Token: 0x040002FE RID: 766
		B8G8R8_SNorm,
		// Token: 0x040002FF RID: 767
		B8G8R8A8_SNorm,
		// Token: 0x04000300 RID: 768
		B8G8R8_UInt,
		// Token: 0x04000301 RID: 769
		B8G8R8A8_UInt,
		// Token: 0x04000302 RID: 770
		B8G8R8_SInt,
		// Token: 0x04000303 RID: 771
		B8G8R8A8_SInt,
		// Token: 0x04000304 RID: 772
		R4G4B4A4_UNormPack16,
		// Token: 0x04000305 RID: 773
		B4G4R4A4_UNormPack16,
		// Token: 0x04000306 RID: 774
		R5G6B5_UNormPack16,
		// Token: 0x04000307 RID: 775
		B5G6R5_UNormPack16,
		// Token: 0x04000308 RID: 776
		R5G5B5A1_UNormPack16,
		// Token: 0x04000309 RID: 777
		B5G5R5A1_UNormPack16,
		// Token: 0x0400030A RID: 778
		A1R5G5B5_UNormPack16,
		// Token: 0x0400030B RID: 779
		E5B9G9R9_UFloatPack32,
		// Token: 0x0400030C RID: 780
		B10G11R11_UFloatPack32,
		// Token: 0x0400030D RID: 781
		A2B10G10R10_UNormPack32,
		// Token: 0x0400030E RID: 782
		A2B10G10R10_UIntPack32,
		// Token: 0x0400030F RID: 783
		A2B10G10R10_SIntPack32,
		// Token: 0x04000310 RID: 784
		A2R10G10B10_UNormPack32,
		// Token: 0x04000311 RID: 785
		A2R10G10B10_UIntPack32,
		// Token: 0x04000312 RID: 786
		A2R10G10B10_SIntPack32,
		// Token: 0x04000313 RID: 787
		A2R10G10B10_XRSRGBPack32,
		// Token: 0x04000314 RID: 788
		A2R10G10B10_XRUNormPack32,
		// Token: 0x04000315 RID: 789
		R10G10B10_XRSRGBPack32,
		// Token: 0x04000316 RID: 790
		R10G10B10_XRUNormPack32,
		// Token: 0x04000317 RID: 791
		A10R10G10B10_XRSRGBPack32,
		// Token: 0x04000318 RID: 792
		A10R10G10B10_XRUNormPack32,
		// Token: 0x04000319 RID: 793
		D16_UNorm = 90,
		// Token: 0x0400031A RID: 794
		D24_UNorm,
		// Token: 0x0400031B RID: 795
		D24_UNorm_S8_UInt,
		// Token: 0x0400031C RID: 796
		D32_SFloat,
		// Token: 0x0400031D RID: 797
		D32_SFloat_S8_Uint,
		// Token: 0x0400031E RID: 798
		S8_Uint,
		// Token: 0x0400031F RID: 799
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Enum member GraphicsFormat.RGB_DXT1_SRGB has been deprecated. Use GraphicsFormat.RGBA_DXT1_SRGB instead (UnityUpgradable) -> RGBA_DXT1_SRGB", true)]
		RGB_DXT1_SRGB,
		// Token: 0x04000320 RID: 800
		RGBA_DXT1_SRGB = 96,
		// Token: 0x04000321 RID: 801
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Enum member GraphicsFormat.RGB_DXT1_UNorm has been deprecated. Use GraphicsFormat.RGBA_DXT1_UNorm instead (UnityUpgradable) -> RGBA_DXT1_UNorm", true)]
		RGB_DXT1_UNorm,
		// Token: 0x04000322 RID: 802
		RGBA_DXT1_UNorm = 97,
		// Token: 0x04000323 RID: 803
		RGBA_DXT3_SRGB,
		// Token: 0x04000324 RID: 804
		RGBA_DXT3_UNorm,
		// Token: 0x04000325 RID: 805
		RGBA_DXT5_SRGB,
		// Token: 0x04000326 RID: 806
		RGBA_DXT5_UNorm,
		// Token: 0x04000327 RID: 807
		R_BC4_UNorm,
		// Token: 0x04000328 RID: 808
		R_BC4_SNorm,
		// Token: 0x04000329 RID: 809
		RG_BC5_UNorm,
		// Token: 0x0400032A RID: 810
		RG_BC5_SNorm,
		// Token: 0x0400032B RID: 811
		RGB_BC6H_UFloat,
		// Token: 0x0400032C RID: 812
		RGB_BC6H_SFloat,
		// Token: 0x0400032D RID: 813
		RGBA_BC7_SRGB,
		// Token: 0x0400032E RID: 814
		RGBA_BC7_UNorm,
		// Token: 0x0400032F RID: 815
		RGB_PVRTC_2Bpp_SRGB,
		// Token: 0x04000330 RID: 816
		RGB_PVRTC_2Bpp_UNorm,
		// Token: 0x04000331 RID: 817
		RGB_PVRTC_4Bpp_SRGB,
		// Token: 0x04000332 RID: 818
		RGB_PVRTC_4Bpp_UNorm,
		// Token: 0x04000333 RID: 819
		RGBA_PVRTC_2Bpp_SRGB,
		// Token: 0x04000334 RID: 820
		RGBA_PVRTC_2Bpp_UNorm,
		// Token: 0x04000335 RID: 821
		RGBA_PVRTC_4Bpp_SRGB,
		// Token: 0x04000336 RID: 822
		RGBA_PVRTC_4Bpp_UNorm,
		// Token: 0x04000337 RID: 823
		RGB_ETC_UNorm,
		// Token: 0x04000338 RID: 824
		RGB_ETC2_SRGB,
		// Token: 0x04000339 RID: 825
		RGB_ETC2_UNorm,
		// Token: 0x0400033A RID: 826
		RGB_A1_ETC2_SRGB,
		// Token: 0x0400033B RID: 827
		RGB_A1_ETC2_UNorm,
		// Token: 0x0400033C RID: 828
		RGBA_ETC2_SRGB,
		// Token: 0x0400033D RID: 829
		RGBA_ETC2_UNorm,
		// Token: 0x0400033E RID: 830
		R_EAC_UNorm,
		// Token: 0x0400033F RID: 831
		R_EAC_SNorm,
		// Token: 0x04000340 RID: 832
		RG_EAC_UNorm,
		// Token: 0x04000341 RID: 833
		RG_EAC_SNorm,
		// Token: 0x04000342 RID: 834
		RGBA_ASTC4X4_SRGB,
		// Token: 0x04000343 RID: 835
		RGBA_ASTC4X4_UNorm,
		// Token: 0x04000344 RID: 836
		RGBA_ASTC5X5_SRGB,
		// Token: 0x04000345 RID: 837
		RGBA_ASTC5X5_UNorm,
		// Token: 0x04000346 RID: 838
		RGBA_ASTC6X6_SRGB,
		// Token: 0x04000347 RID: 839
		RGBA_ASTC6X6_UNorm,
		// Token: 0x04000348 RID: 840
		RGBA_ASTC8X8_SRGB,
		// Token: 0x04000349 RID: 841
		RGBA_ASTC8X8_UNorm,
		// Token: 0x0400034A RID: 842
		RGBA_ASTC10X10_SRGB,
		// Token: 0x0400034B RID: 843
		RGBA_ASTC10X10_UNorm,
		// Token: 0x0400034C RID: 844
		RGBA_ASTC12X12_SRGB,
		// Token: 0x0400034D RID: 845
		RGBA_ASTC12X12_UNorm
	}
}
