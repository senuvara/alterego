﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x0200030D RID: 781
	[UsedByNativeCode]
	public struct VisibleReflectionProbe
	{
		// Token: 0x1700050C RID: 1292
		// (get) Token: 0x06001B03 RID: 6915 RVA: 0x0002F800 File Offset: 0x0002DA00
		public Texture texture
		{
			get
			{
				return VisibleReflectionProbe.GetTextureObject(this.textureId);
			}
		}

		// Token: 0x06001B04 RID: 6916
		[FreeFunction("(Texture*)Object::IDToPointer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Texture GetTextureObject(int textureId);

		// Token: 0x1700050D RID: 1293
		// (get) Token: 0x06001B05 RID: 6917 RVA: 0x0002F820 File Offset: 0x0002DA20
		public ReflectionProbe probe
		{
			get
			{
				return VisibleReflectionProbe.GetReflectionProbeObject(this.instanceId);
			}
		}

		// Token: 0x06001B06 RID: 6918
		[FreeFunction("(ReflectionProbe*)Object::IDToPointer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern ReflectionProbe GetReflectionProbeObject(int instanceId);

		// Token: 0x04000A34 RID: 2612
		public Bounds bounds;

		// Token: 0x04000A35 RID: 2613
		public Matrix4x4 localToWorld;

		// Token: 0x04000A36 RID: 2614
		public Vector4 hdr;

		// Token: 0x04000A37 RID: 2615
		public Vector3 center;

		// Token: 0x04000A38 RID: 2616
		public float blendDistance;

		// Token: 0x04000A39 RID: 2617
		public int importance;

		// Token: 0x04000A3A RID: 2618
		public int boxProjection;

		// Token: 0x04000A3B RID: 2619
		private int instanceId;

		// Token: 0x04000A3C RID: 2620
		private int textureId;
	}
}
