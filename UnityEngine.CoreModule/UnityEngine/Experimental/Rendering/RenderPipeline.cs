﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002EC RID: 748
	public abstract class RenderPipeline : IRenderPipeline, IDisposable
	{
		// Token: 0x060019DA RID: 6618 RVA: 0x00002370 File Offset: 0x00000570
		protected RenderPipeline()
		{
		}

		// Token: 0x14000011 RID: 17
		// (add) Token: 0x060019DB RID: 6619 RVA: 0x0002D994 File Offset: 0x0002BB94
		// (remove) Token: 0x060019DC RID: 6620 RVA: 0x0002D9C8 File Offset: 0x0002BBC8
		public static event Action<Camera[]> beginFrameRendering
		{
			add
			{
				Action<Camera[]> action = RenderPipeline.beginFrameRendering;
				Action<Camera[]> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<Camera[]>>(ref RenderPipeline.beginFrameRendering, (Action<Camera[]>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<Camera[]> action = RenderPipeline.beginFrameRendering;
				Action<Camera[]> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<Camera[]>>(ref RenderPipeline.beginFrameRendering, (Action<Camera[]>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000012 RID: 18
		// (add) Token: 0x060019DD RID: 6621 RVA: 0x0002D9FC File Offset: 0x0002BBFC
		// (remove) Token: 0x060019DE RID: 6622 RVA: 0x0002DA30 File Offset: 0x0002BC30
		public static event Action<Camera> beginCameraRendering
		{
			add
			{
				Action<Camera> action = RenderPipeline.beginCameraRendering;
				Action<Camera> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<Camera>>(ref RenderPipeline.beginCameraRendering, (Action<Camera>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<Camera> action = RenderPipeline.beginCameraRendering;
				Action<Camera> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<Camera>>(ref RenderPipeline.beginCameraRendering, (Action<Camera>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x060019DF RID: 6623 RVA: 0x0002DA64 File Offset: 0x0002BC64
		public virtual void Render(ScriptableRenderContext renderContext, Camera[] cameras)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(string.Format("{0} has been disposed. Do not call Render on disposed RenderLoops.", this));
			}
		}

		// Token: 0x170004BA RID: 1210
		// (get) Token: 0x060019E0 RID: 6624 RVA: 0x0002DA84 File Offset: 0x0002BC84
		// (set) Token: 0x060019E1 RID: 6625 RVA: 0x0002DA9E File Offset: 0x0002BC9E
		public bool disposed
		{
			[CompilerGenerated]
			get
			{
				return this.<disposed>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<disposed>k__BackingField = value;
			}
		}

		// Token: 0x060019E2 RID: 6626 RVA: 0x0002DAA7 File Offset: 0x0002BCA7
		public virtual void Dispose()
		{
			this.disposed = true;
		}

		// Token: 0x060019E3 RID: 6627 RVA: 0x0002DAB1 File Offset: 0x0002BCB1
		public static void BeginFrameRendering(Camera[] cameras)
		{
			if (RenderPipeline.beginFrameRendering != null)
			{
				RenderPipeline.beginFrameRendering(cameras);
			}
		}

		// Token: 0x060019E4 RID: 6628 RVA: 0x0002DACB File Offset: 0x0002BCCB
		public static void BeginCameraRendering(Camera camera)
		{
			if (RenderPipeline.beginCameraRendering != null)
			{
				RenderPipeline.beginCameraRendering(camera);
			}
		}

		// Token: 0x040009A4 RID: 2468
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action<Camera[]> beginFrameRendering;

		// Token: 0x040009A5 RID: 2469
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action<Camera> beginCameraRendering;

		// Token: 0x040009A6 RID: 2470
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <disposed>k__BackingField;
	}
}
