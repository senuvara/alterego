﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002EB RID: 747
	public enum ReflectionProbeSortOptions
	{
		// Token: 0x040009A0 RID: 2464
		None,
		// Token: 0x040009A1 RID: 2465
		Importance,
		// Token: 0x040009A2 RID: 2466
		Size,
		// Token: 0x040009A3 RID: 2467
		ImportanceThenSize
	}
}
