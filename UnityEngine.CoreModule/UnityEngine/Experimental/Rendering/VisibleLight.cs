﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x0200030C RID: 780
	[UsedByNativeCode]
	public struct VisibleLight
	{
		// Token: 0x1700050B RID: 1291
		// (get) Token: 0x06001B01 RID: 6913 RVA: 0x0002F7E0 File Offset: 0x0002D9E0
		public Light light
		{
			get
			{
				return VisibleLight.GetLightObject(this.instanceId);
			}
		}

		// Token: 0x06001B02 RID: 6914
		[FreeFunction("(Light*)Object::IDToPointer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Light GetLightObject(int instanceId);

		// Token: 0x04000A2C RID: 2604
		public LightType lightType;

		// Token: 0x04000A2D RID: 2605
		public Color finalColor;

		// Token: 0x04000A2E RID: 2606
		public Rect screenRect;

		// Token: 0x04000A2F RID: 2607
		public Matrix4x4 localToWorld;

		// Token: 0x04000A30 RID: 2608
		public float range;

		// Token: 0x04000A31 RID: 2609
		public float spotAngle;

		// Token: 0x04000A32 RID: 2610
		private int instanceId;

		// Token: 0x04000A33 RID: 2611
		public VisibleLightFlags flags;
	}
}
