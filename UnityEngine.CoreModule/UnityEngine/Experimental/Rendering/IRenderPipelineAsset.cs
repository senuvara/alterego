﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002E8 RID: 744
	public interface IRenderPipelineAsset
	{
		// Token: 0x060019C4 RID: 6596
		void DestroyCreatedInstances();

		// Token: 0x060019C5 RID: 6597
		IRenderPipeline CreatePipeline();
	}
}
