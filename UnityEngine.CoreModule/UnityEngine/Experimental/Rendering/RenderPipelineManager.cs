﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002EE RID: 750
	public static class RenderPipelineManager
	{
		// Token: 0x170004BB RID: 1211
		// (get) Token: 0x060019F9 RID: 6649 RVA: 0x0002DCEC File Offset: 0x0002BEEC
		// (set) Token: 0x060019FA RID: 6650 RVA: 0x0002DD05 File Offset: 0x0002BF05
		public static IRenderPipeline currentPipeline
		{
			[CompilerGenerated]
			get
			{
				return RenderPipelineManager.<currentPipeline>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				RenderPipelineManager.<currentPipeline>k__BackingField = value;
			}
		}

		// Token: 0x060019FB RID: 6651 RVA: 0x0002DD0D File Offset: 0x0002BF0D
		[RequiredByNativeCode]
		internal static void CleanupRenderPipeline()
		{
			if (RenderPipelineManager.s_CurrentPipelineAsset != null)
			{
				RenderPipelineManager.s_CurrentPipelineAsset.DestroyCreatedInstances();
				RenderPipelineManager.s_CurrentPipelineAsset = null;
				RenderPipelineManager.currentPipeline = null;
				SupportedRenderingFeatures.active = new SupportedRenderingFeatures();
			}
		}

		// Token: 0x060019FC RID: 6652 RVA: 0x0002DD3C File Offset: 0x0002BF3C
		[RequiredByNativeCode]
		private static void DoRenderLoop_Internal(IRenderPipelineAsset pipe, Camera[] cameras, IntPtr loopPtr)
		{
			RenderPipelineManager.PrepareRenderPipeline(pipe);
			if (RenderPipelineManager.currentPipeline != null)
			{
				ScriptableRenderContext renderContext = new ScriptableRenderContext(loopPtr);
				RenderPipelineManager.currentPipeline.Render(renderContext, cameras);
			}
		}

		// Token: 0x060019FD RID: 6653 RVA: 0x0002DD74 File Offset: 0x0002BF74
		private static void PrepareRenderPipeline(IRenderPipelineAsset pipe)
		{
			if (RenderPipelineManager.s_CurrentPipelineAsset != pipe)
			{
				if (RenderPipelineManager.s_CurrentPipelineAsset != null)
				{
					RenderPipelineManager.CleanupRenderPipeline();
				}
				RenderPipelineManager.s_CurrentPipelineAsset = pipe;
			}
			if (RenderPipelineManager.s_CurrentPipelineAsset != null && (RenderPipelineManager.currentPipeline == null || RenderPipelineManager.currentPipeline.disposed))
			{
				RenderPipelineManager.currentPipeline = RenderPipelineManager.s_CurrentPipelineAsset.CreatePipeline();
			}
		}

		// Token: 0x040009A8 RID: 2472
		private static IRenderPipelineAsset s_CurrentPipelineAsset;

		// Token: 0x040009A9 RID: 2473
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static IRenderPipeline <currentPipeline>k__BackingField;
	}
}
