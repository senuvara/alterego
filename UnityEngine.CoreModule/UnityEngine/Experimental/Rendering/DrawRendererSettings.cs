﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x0200030A RID: 778
	[NativeHeader("Runtime/Graphics/ScriptableRenderLoop/ScriptableDrawRenderersUtility.h")]
	public struct DrawRendererSettings
	{
		// Token: 0x06001AFC RID: 6908 RVA: 0x0002F6AC File Offset: 0x0002D8AC
		public unsafe DrawRendererSettings(Camera camera, ShaderPassName shaderPassName)
		{
			this.rendererConfiguration = RendererConfiguration.None;
			this.flags = DrawRendererFlags.EnableInstancing;
			this.m_OverrideMaterialInstanceId = 0;
			this.m_OverrideMaterialPassIdx = 0;
			fixed (int* ptr = &this.shaderPassNames.FixedElementField)
			{
				for (int i = 0; i < DrawRendererSettings.maxShaderPasses; i++)
				{
					ptr[(IntPtr)i * 4] = -1;
				}
			}
			fixed (int* ptr2 = &this.shaderPassNames.FixedElementField)
			{
				*ptr2 = shaderPassName.nameIndex;
			}
			this.rendererConfiguration = RendererConfiguration.None;
			this.flags = DrawRendererFlags.EnableInstancing;
			DrawRendererSettings.InitializeSortSettings(camera, out this.sorting);
			this.useSRPBatcher = 0;
		}

		// Token: 0x06001AFD RID: 6909 RVA: 0x0002F742 File Offset: 0x0002D942
		public void SetOverrideMaterial(Material mat, int passIndex)
		{
			if (mat == null)
			{
				this.m_OverrideMaterialInstanceId = 0;
			}
			else
			{
				this.m_OverrideMaterialInstanceId = mat.GetInstanceID();
			}
			this.m_OverrideMaterialPassIdx = passIndex;
		}

		// Token: 0x06001AFE RID: 6910 RVA: 0x0002F770 File Offset: 0x0002D970
		public unsafe void SetShaderPassName(int index, ShaderPassName shaderPassName)
		{
			if (index >= DrawRendererSettings.maxShaderPasses || index < 0)
			{
				throw new ArgumentOutOfRangeException("index", string.Format("Index should range from 0 - DrawRendererSettings.maxShaderPasses ({0}), was {1}", DrawRendererSettings.maxShaderPasses, index));
			}
			fixed (int* ptr = &this.shaderPassNames.FixedElementField)
			{
				ptr[(IntPtr)index * 4] = shaderPassName.nameIndex;
			}
		}

		// Token: 0x06001AFF RID: 6911
		[FreeFunction("InitializeSortSettings")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InitializeSortSettings(Camera camera, out DrawRendererSortSettings sortSettings);

		// Token: 0x06001B00 RID: 6912 RVA: 0x0002F7D4 File Offset: 0x0002D9D4
		// Note: this type is marked as 'beforefieldinit'.
		static DrawRendererSettings()
		{
		}

		// Token: 0x04000A22 RID: 2594
		private const int kMaxShaderPasses = 16;

		// Token: 0x04000A23 RID: 2595
		public static readonly int maxShaderPasses = 16;

		// Token: 0x04000A24 RID: 2596
		public DrawRendererSortSettings sorting;

		// Token: 0x04000A25 RID: 2597
		[FixedBuffer(typeof(int), 16)]
		internal DrawRendererSettings.<shaderPassNames>__FixedBuffer7 shaderPassNames;

		// Token: 0x04000A26 RID: 2598
		public RendererConfiguration rendererConfiguration;

		// Token: 0x04000A27 RID: 2599
		public DrawRendererFlags flags;

		// Token: 0x04000A28 RID: 2600
		private int m_OverrideMaterialInstanceId;

		// Token: 0x04000A29 RID: 2601
		private int m_OverrideMaterialPassIdx;

		// Token: 0x04000A2A RID: 2602
		private int useSRPBatcher;

		// Token: 0x0200030B RID: 779
		[CompilerGenerated]
		[UnsafeValueType]
		[StructLayout(LayoutKind.Sequential, Size = 64)]
		public struct <shaderPassNames>__FixedBuffer7
		{
			// Token: 0x04000A2B RID: 2603
			public int FixedElementField;
		}
	}
}
