﻿using System;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002EA RID: 746
	public struct RasterState
	{
		// Token: 0x060019D0 RID: 6608 RVA: 0x0002D89D File Offset: 0x0002BA9D
		public RasterState(CullMode cullingMode = CullMode.Back, int offsetUnits = 0, float offsetFactor = 0f, bool depthClip = true)
		{
			this.m_CullingMode = cullingMode;
			this.m_OffsetUnits = offsetUnits;
			this.m_OffsetFactor = offsetFactor;
			this.m_DepthClip = Convert.ToByte(depthClip);
			this.m_Padding1 = 0;
			this.m_Padding2 = 0;
			this.m_Padding3 = 0;
		}

		// Token: 0x170004B6 RID: 1206
		// (get) Token: 0x060019D1 RID: 6609 RVA: 0x0002D8D8 File Offset: 0x0002BAD8
		// (set) Token: 0x060019D2 RID: 6610 RVA: 0x0002D8F3 File Offset: 0x0002BAF3
		public CullMode cullingMode
		{
			get
			{
				return this.m_CullingMode;
			}
			set
			{
				this.m_CullingMode = value;
			}
		}

		// Token: 0x170004B7 RID: 1207
		// (get) Token: 0x060019D3 RID: 6611 RVA: 0x0002D900 File Offset: 0x0002BB00
		// (set) Token: 0x060019D4 RID: 6612 RVA: 0x0002D920 File Offset: 0x0002BB20
		public bool depthClip
		{
			get
			{
				return Convert.ToBoolean(this.m_DepthClip);
			}
			set
			{
				this.m_DepthClip = Convert.ToByte(value);
			}
		}

		// Token: 0x170004B8 RID: 1208
		// (get) Token: 0x060019D5 RID: 6613 RVA: 0x0002D930 File Offset: 0x0002BB30
		// (set) Token: 0x060019D6 RID: 6614 RVA: 0x0002D94B File Offset: 0x0002BB4B
		public int offsetUnits
		{
			get
			{
				return this.m_OffsetUnits;
			}
			set
			{
				this.m_OffsetUnits = value;
			}
		}

		// Token: 0x170004B9 RID: 1209
		// (get) Token: 0x060019D7 RID: 6615 RVA: 0x0002D958 File Offset: 0x0002BB58
		// (set) Token: 0x060019D8 RID: 6616 RVA: 0x0002D973 File Offset: 0x0002BB73
		public float offsetFactor
		{
			get
			{
				return this.m_OffsetFactor;
			}
			set
			{
				this.m_OffsetFactor = value;
			}
		}

		// Token: 0x060019D9 RID: 6617 RVA: 0x0002D97D File Offset: 0x0002BB7D
		// Note: this type is marked as 'beforefieldinit'.
		static RasterState()
		{
		}

		// Token: 0x04000997 RID: 2455
		public static readonly RasterState Default = new RasterState(CullMode.Back, 0, 0f, true);

		// Token: 0x04000998 RID: 2456
		private CullMode m_CullingMode;

		// Token: 0x04000999 RID: 2457
		private int m_OffsetUnits;

		// Token: 0x0400099A RID: 2458
		private float m_OffsetFactor;

		// Token: 0x0400099B RID: 2459
		private byte m_DepthClip;

		// Token: 0x0400099C RID: 2460
		private byte m_Padding1;

		// Token: 0x0400099D RID: 2461
		private byte m_Padding2;

		// Token: 0x0400099E RID: 2462
		private byte m_Padding3;
	}
}
