﻿using System;

namespace UnityEngine.Experimental.Rendering
{
	// Token: 0x020002E1 RID: 737
	[Flags]
	public enum DrawRendererFlags
	{
		// Token: 0x0400097C RID: 2428
		None = 0,
		// Token: 0x0400097D RID: 2429
		EnableDynamicBatching = 1,
		// Token: 0x0400097E RID: 2430
		EnableInstancing = 2
	}
}
