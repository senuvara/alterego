﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Playables;

namespace UnityEngine.Experimental.Playables
{
	// Token: 0x020002A2 RID: 674
	public static class TexturePlayableBinding
	{
		// Token: 0x0600186D RID: 6253 RVA: 0x0002A944 File Offset: 0x00028B44
		public static PlayableBinding Create(string name, Object key)
		{
			Type typeFromHandle = typeof(RenderTexture);
			if (TexturePlayableBinding.<>f__mg$cache0 == null)
			{
				TexturePlayableBinding.<>f__mg$cache0 = new PlayableBinding.CreateOutputMethod(TexturePlayableBinding.CreateTextureOutput);
			}
			return PlayableBinding.CreateInternal(name, key, typeFromHandle, TexturePlayableBinding.<>f__mg$cache0);
		}

		// Token: 0x0600186E RID: 6254 RVA: 0x0002A988 File Offset: 0x00028B88
		private static PlayableOutput CreateTextureOutput(PlayableGraph graph, string name)
		{
			return TexturePlayableOutput.Create(graph, name, null);
		}

		// Token: 0x04000894 RID: 2196
		[CompilerGenerated]
		private static PlayableBinding.CreateOutputMethod <>f__mg$cache0;
	}
}
