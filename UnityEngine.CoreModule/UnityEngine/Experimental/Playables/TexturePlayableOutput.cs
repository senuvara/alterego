﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Playables;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Playables
{
	// Token: 0x020002A4 RID: 676
	[StaticAccessor("TexturePlayableOutputBindings", StaticAccessorType.DoubleColon)]
	[NativeHeader("Runtime/Graphics/RenderTexture.h")]
	[NativeHeader("Runtime/Graphics/Director/TexturePlayableOutput.h")]
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Export/Director/TexturePlayableOutput.bindings.h")]
	public struct TexturePlayableOutput : IPlayableOutput
	{
		// Token: 0x06001870 RID: 6256 RVA: 0x0002A9AA File Offset: 0x00028BAA
		internal TexturePlayableOutput(PlayableOutputHandle handle)
		{
			if (handle.IsValid())
			{
				if (!handle.IsPlayableOutputOfType<TexturePlayableOutput>())
				{
					throw new InvalidCastException("Can't set handle: the playable is not an TexturePlayableOutput.");
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x06001871 RID: 6257 RVA: 0x0002A9DC File Offset: 0x00028BDC
		public static TexturePlayableOutput Create(PlayableGraph graph, string name, RenderTexture target)
		{
			PlayableOutputHandle handle;
			TexturePlayableOutput result;
			if (!TexturePlayableGraphExtensions.InternalCreateTextureOutput(ref graph, name, out handle))
			{
				result = TexturePlayableOutput.Null;
			}
			else
			{
				TexturePlayableOutput texturePlayableOutput = new TexturePlayableOutput(handle);
				texturePlayableOutput.SetTarget(target);
				result = texturePlayableOutput;
			}
			return result;
		}

		// Token: 0x17000468 RID: 1128
		// (get) Token: 0x06001872 RID: 6258 RVA: 0x0002AA1C File Offset: 0x00028C1C
		public static TexturePlayableOutput Null
		{
			get
			{
				return new TexturePlayableOutput(PlayableOutputHandle.Null);
			}
		}

		// Token: 0x06001873 RID: 6259 RVA: 0x0002AA3C File Offset: 0x00028C3C
		public PlayableOutputHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x06001874 RID: 6260 RVA: 0x0002AA58 File Offset: 0x00028C58
		public static implicit operator PlayableOutput(TexturePlayableOutput output)
		{
			return new PlayableOutput(output.GetHandle());
		}

		// Token: 0x06001875 RID: 6261 RVA: 0x0002AA7C File Offset: 0x00028C7C
		public static explicit operator TexturePlayableOutput(PlayableOutput output)
		{
			return new TexturePlayableOutput(output.GetHandle());
		}

		// Token: 0x06001876 RID: 6262 RVA: 0x0002AAA0 File Offset: 0x00028CA0
		public RenderTexture GetTarget()
		{
			return TexturePlayableOutput.InternalGetTarget(ref this.m_Handle);
		}

		// Token: 0x06001877 RID: 6263 RVA: 0x0002AAC0 File Offset: 0x00028CC0
		public void SetTarget(RenderTexture value)
		{
			TexturePlayableOutput.InternalSetTarget(ref this.m_Handle, value);
		}

		// Token: 0x06001878 RID: 6264
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RenderTexture InternalGetTarget(ref PlayableOutputHandle output);

		// Token: 0x06001879 RID: 6265
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalSetTarget(ref PlayableOutputHandle output, RenderTexture target);

		// Token: 0x04000895 RID: 2197
		private PlayableOutputHandle m_Handle;
	}
}
