﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Playables;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Playables
{
	// Token: 0x020002A1 RID: 673
	[NativeHeader("Runtime/Director/Core/HPlayable.h")]
	[NativeHeader("Runtime/Graphics/Director/TextureMixerPlayable.h")]
	[NativeHeader("Runtime/Export/Director/TextureMixerPlayable.bindings.h")]
	[RequiredByNativeCode]
	[StaticAccessor("TextureMixerPlayableBindings", StaticAccessorType.DoubleColon)]
	public struct TextureMixerPlayable : IPlayable, IEquatable<TextureMixerPlayable>
	{
		// Token: 0x06001865 RID: 6245 RVA: 0x0002A82D File Offset: 0x00028A2D
		internal TextureMixerPlayable(PlayableHandle handle)
		{
			if (handle.IsValid())
			{
				if (!handle.IsPlayableOfType<TextureMixerPlayable>())
				{
					throw new InvalidCastException("Can't set handle: the playable is not an TextureMixerPlayable.");
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x06001866 RID: 6246 RVA: 0x0002A85C File Offset: 0x00028A5C
		public static TextureMixerPlayable Create(PlayableGraph graph)
		{
			PlayableHandle handle = TextureMixerPlayable.CreateHandle(graph);
			return new TextureMixerPlayable(handle);
		}

		// Token: 0x06001867 RID: 6247 RVA: 0x0002A880 File Offset: 0x00028A80
		private static PlayableHandle CreateHandle(PlayableGraph graph)
		{
			PlayableHandle @null = PlayableHandle.Null;
			PlayableHandle result;
			if (!TextureMixerPlayable.CreateTextureMixerPlayableInternal(ref graph, ref @null))
			{
				result = PlayableHandle.Null;
			}
			else
			{
				result = @null;
			}
			return result;
		}

		// Token: 0x06001868 RID: 6248 RVA: 0x0002A8B8 File Offset: 0x00028AB8
		public PlayableHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x06001869 RID: 6249 RVA: 0x0002A8D4 File Offset: 0x00028AD4
		public static implicit operator Playable(TextureMixerPlayable playable)
		{
			return new Playable(playable.GetHandle());
		}

		// Token: 0x0600186A RID: 6250 RVA: 0x0002A8F8 File Offset: 0x00028AF8
		public static explicit operator TextureMixerPlayable(Playable playable)
		{
			return new TextureMixerPlayable(playable.GetHandle());
		}

		// Token: 0x0600186B RID: 6251 RVA: 0x0002A91C File Offset: 0x00028B1C
		public bool Equals(TextureMixerPlayable other)
		{
			return this.GetHandle() == other.GetHandle();
		}

		// Token: 0x0600186C RID: 6252
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CreateTextureMixerPlayableInternal(ref PlayableGraph graph, ref PlayableHandle handle);

		// Token: 0x04000893 RID: 2195
		private PlayableHandle m_Handle;
	}
}
