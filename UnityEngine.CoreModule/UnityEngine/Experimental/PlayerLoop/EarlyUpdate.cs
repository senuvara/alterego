﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.PlayerLoop
{
	// Token: 0x02000168 RID: 360
	[RequiredByNativeCode]
	public struct EarlyUpdate
	{
		// Token: 0x02000169 RID: 361
		[RequiredByNativeCode]
		public struct PollPlayerConnection
		{
		}

		// Token: 0x0200016A RID: 362
		[RequiredByNativeCode]
		public struct ProfilerStartFrame
		{
		}

		// Token: 0x0200016B RID: 363
		[RequiredByNativeCode]
		public struct PollHtcsPlayerConnection
		{
		}

		// Token: 0x0200016C RID: 364
		[RequiredByNativeCode]
		public struct GpuTimestamp
		{
		}

		// Token: 0x0200016D RID: 365
		[RequiredByNativeCode]
		public struct AnalyticsCoreStatsUpdate
		{
		}

		// Token: 0x0200016E RID: 366
		[RequiredByNativeCode]
		public struct UnityWebRequestUpdate
		{
		}

		// Token: 0x0200016F RID: 367
		[RequiredByNativeCode]
		public struct UpdateStreamingManager
		{
		}

		// Token: 0x02000170 RID: 368
		[RequiredByNativeCode]
		public struct ExecuteMainThreadJobs
		{
		}

		// Token: 0x02000171 RID: 369
		[RequiredByNativeCode]
		public struct ProcessMouseInWindow
		{
		}

		// Token: 0x02000172 RID: 370
		[RequiredByNativeCode]
		public struct ClearIntermediateRenderers
		{
		}

		// Token: 0x02000173 RID: 371
		[RequiredByNativeCode]
		public struct ClearLines
		{
		}

		// Token: 0x02000174 RID: 372
		[RequiredByNativeCode]
		public struct PresentBeforeUpdate
		{
		}

		// Token: 0x02000175 RID: 373
		[RequiredByNativeCode]
		public struct ResetFrameStatsAfterPresent
		{
		}

		// Token: 0x02000176 RID: 374
		[RequiredByNativeCode]
		public struct UpdateAllUnityWebStreams
		{
		}

		// Token: 0x02000177 RID: 375
		[RequiredByNativeCode]
		public struct UpdateAsyncReadbackManager
		{
		}

		// Token: 0x02000178 RID: 376
		[RequiredByNativeCode]
		public struct UpdateTextureStreamingManager
		{
		}

		// Token: 0x02000179 RID: 377
		[RequiredByNativeCode]
		public struct UpdatePreloading
		{
		}

		// Token: 0x0200017A RID: 378
		[RequiredByNativeCode]
		public struct RendererNotifyInvisible
		{
		}

		// Token: 0x0200017B RID: 379
		[RequiredByNativeCode]
		public struct PlayerCleanupCachedData
		{
		}

		// Token: 0x0200017C RID: 380
		[RequiredByNativeCode]
		public struct UpdateMainGameViewRect
		{
		}

		// Token: 0x0200017D RID: 381
		[RequiredByNativeCode]
		public struct UpdateCanvasRectTransform
		{
		}

		// Token: 0x0200017E RID: 382
		[RequiredByNativeCode]
		public struct UpdateInputManager
		{
		}

		// Token: 0x0200017F RID: 383
		[RequiredByNativeCode]
		public struct ProcessRemoteInput
		{
		}

		// Token: 0x02000180 RID: 384
		[RequiredByNativeCode]
		public struct XRUpdate
		{
		}

		// Token: 0x02000181 RID: 385
		[RequiredByNativeCode]
		public struct ScriptRunDelayedStartupFrame
		{
		}

		// Token: 0x02000182 RID: 386
		[RequiredByNativeCode]
		public struct UpdateKinect
		{
		}

		// Token: 0x02000183 RID: 387
		[RequiredByNativeCode]
		public struct DeliverIosPlatformEvents
		{
		}

		// Token: 0x02000184 RID: 388
		[RequiredByNativeCode]
		public struct DispatchEventQueueEvents
		{
		}

		// Token: 0x02000185 RID: 389
		[RequiredByNativeCode]
		public struct DirectorSampleTime
		{
		}

		// Token: 0x02000186 RID: 390
		[RequiredByNativeCode]
		public struct PhysicsResetInterpolatedTransformPosition
		{
		}

		// Token: 0x02000187 RID: 391
		[RequiredByNativeCode]
		public struct SpriteAtlasManagerUpdate
		{
		}

		// Token: 0x02000188 RID: 392
		[RequiredByNativeCode]
		public struct TangoUpdate
		{
		}

		// Token: 0x02000189 RID: 393
		[RequiredByNativeCode]
		public struct PerformanceAnalyticsUpdate
		{
		}
	}
}
