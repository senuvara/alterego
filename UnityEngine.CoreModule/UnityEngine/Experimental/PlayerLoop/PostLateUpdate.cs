﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.PlayerLoop
{
	// Token: 0x020001B3 RID: 435
	[RequiredByNativeCode]
	public struct PostLateUpdate
	{
		// Token: 0x020001B4 RID: 436
		[RequiredByNativeCode]
		public struct PlayerSendFrameStarted
		{
		}

		// Token: 0x020001B5 RID: 437
		[RequiredByNativeCode]
		public struct UpdateRectTransform
		{
		}

		// Token: 0x020001B6 RID: 438
		[RequiredByNativeCode]
		public struct UpdateCanvasRectTransform
		{
		}

		// Token: 0x020001B7 RID: 439
		[RequiredByNativeCode]
		public struct PlayerUpdateCanvases
		{
		}

		// Token: 0x020001B8 RID: 440
		[RequiredByNativeCode]
		public struct UpdateAudio
		{
		}

		// Token: 0x020001B9 RID: 441
		[RequiredByNativeCode]
		public struct UpdateVideo
		{
		}

		// Token: 0x020001BA RID: 442
		[RequiredByNativeCode]
		public struct DirectorLateUpdate
		{
		}

		// Token: 0x020001BB RID: 443
		[RequiredByNativeCode]
		public struct ScriptRunDelayedDynamicFrameRate
		{
		}

		// Token: 0x020001BC RID: 444
		[RequiredByNativeCode]
		public struct VFXUpdate
		{
		}

		// Token: 0x020001BD RID: 445
		[RequiredByNativeCode]
		public struct ParticleSystemEndUpdateAll
		{
		}

		// Token: 0x020001BE RID: 446
		[RequiredByNativeCode]
		public struct EndGraphicsJobsAfterScriptLateUpdate
		{
		}

		// Token: 0x020001BF RID: 447
		[RequiredByNativeCode]
		public struct UpdateSubstance
		{
		}

		// Token: 0x020001C0 RID: 448
		[RequiredByNativeCode]
		public struct UpdateCustomRenderTextures
		{
		}

		// Token: 0x020001C1 RID: 449
		[RequiredByNativeCode]
		public struct UpdateAllRenderers
		{
		}

		// Token: 0x020001C2 RID: 450
		[RequiredByNativeCode]
		public struct EnlightenRuntimeUpdate
		{
		}

		// Token: 0x020001C3 RID: 451
		[RequiredByNativeCode]
		public struct UpdateAllSkinnedMeshes
		{
		}

		// Token: 0x020001C4 RID: 452
		[RequiredByNativeCode]
		public struct ProcessWebSendMessages
		{
		}

		// Token: 0x020001C5 RID: 453
		[RequiredByNativeCode]
		public struct SortingGroupsUpdate
		{
		}

		// Token: 0x020001C6 RID: 454
		[RequiredByNativeCode]
		public struct UpdateVideoTextures
		{
		}

		// Token: 0x020001C7 RID: 455
		[RequiredByNativeCode]
		public struct DirectorRenderImage
		{
		}

		// Token: 0x020001C8 RID: 456
		[RequiredByNativeCode]
		public struct PlayerEmitCanvasGeometry
		{
		}

		// Token: 0x020001C9 RID: 457
		[RequiredByNativeCode]
		public struct FinishFrameRendering
		{
		}

		// Token: 0x020001CA RID: 458
		[RequiredByNativeCode]
		public struct BatchModeUpdate
		{
		}

		// Token: 0x020001CB RID: 459
		[RequiredByNativeCode]
		public struct PlayerSendFrameComplete
		{
		}

		// Token: 0x020001CC RID: 460
		[RequiredByNativeCode]
		public struct UpdateCaptureScreenshot
		{
		}

		// Token: 0x020001CD RID: 461
		[RequiredByNativeCode]
		public struct PresentAfterDraw
		{
		}

		// Token: 0x020001CE RID: 462
		[RequiredByNativeCode]
		public struct ClearImmediateRenderers
		{
		}

		// Token: 0x020001CF RID: 463
		[RequiredByNativeCode]
		public struct XRPostPresent
		{
		}

		// Token: 0x020001D0 RID: 464
		[RequiredByNativeCode]
		public struct UpdateResolution
		{
		}

		// Token: 0x020001D1 RID: 465
		[RequiredByNativeCode]
		public struct InputEndFrame
		{
		}

		// Token: 0x020001D2 RID: 466
		[RequiredByNativeCode]
		public struct GUIClearEvents
		{
		}

		// Token: 0x020001D3 RID: 467
		[RequiredByNativeCode]
		public struct ShaderHandleErrors
		{
		}

		// Token: 0x020001D4 RID: 468
		[RequiredByNativeCode]
		public struct ResetInputAxis
		{
		}

		// Token: 0x020001D5 RID: 469
		[RequiredByNativeCode]
		public struct ThreadedLoadingDebug
		{
		}

		// Token: 0x020001D6 RID: 470
		[RequiredByNativeCode]
		public struct ProfilerSynchronizeStats
		{
		}

		// Token: 0x020001D7 RID: 471
		[RequiredByNativeCode]
		public struct MemoryFrameMaintenance
		{
		}

		// Token: 0x020001D8 RID: 472
		[RequiredByNativeCode]
		public struct ExecuteGameCenterCallbacks
		{
		}

		// Token: 0x020001D9 RID: 473
		[RequiredByNativeCode]
		public struct ProfilerEndFrame
		{
		}

		// Token: 0x020001DA RID: 474
		[RequiredByNativeCode]
		public struct PlayerSendFramePostPresent
		{
		}

		// Token: 0x020001DB RID: 475
		[RequiredByNativeCode]
		public struct PhysicsSkinnedClothBeginUpdate
		{
		}

		// Token: 0x020001DC RID: 476
		[RequiredByNativeCode]
		public struct PhysicsSkinnedClothFinishUpdate
		{
		}

		// Token: 0x020001DD RID: 477
		[RequiredByNativeCode]
		public struct TriggerEndOfFrameCallbacks
		{
		}
	}
}
