﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.PlayerLoop
{
	// Token: 0x02000162 RID: 354
	[RequiredByNativeCode]
	public struct Initialization
	{
		// Token: 0x02000163 RID: 355
		[RequiredByNativeCode]
		public struct PlayerUpdateTime
		{
		}

		// Token: 0x02000164 RID: 356
		[RequiredByNativeCode]
		public struct AsyncUploadTimeSlicedUpdate
		{
		}

		// Token: 0x02000165 RID: 357
		[RequiredByNativeCode]
		public struct SynchronizeState
		{
		}

		// Token: 0x02000166 RID: 358
		[RequiredByNativeCode]
		public struct SynchronizeInputs
		{
		}

		// Token: 0x02000167 RID: 359
		[RequiredByNativeCode]
		public struct XREarlyUpdate
		{
		}
	}
}
