﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.PlayerLoop
{
	// Token: 0x0200018A RID: 394
	[RequiredByNativeCode]
	public struct FixedUpdate
	{
		// Token: 0x0200018B RID: 395
		[RequiredByNativeCode]
		public struct ClearLines
		{
		}

		// Token: 0x0200018C RID: 396
		[RequiredByNativeCode]
		public struct DirectorFixedSampleTime
		{
		}

		// Token: 0x0200018D RID: 397
		[RequiredByNativeCode]
		public struct AudioFixedUpdate
		{
		}

		// Token: 0x0200018E RID: 398
		[RequiredByNativeCode]
		public struct ScriptRunBehaviourFixedUpdate
		{
		}

		// Token: 0x0200018F RID: 399
		[RequiredByNativeCode]
		public struct DirectorFixedUpdate
		{
		}

		// Token: 0x02000190 RID: 400
		[RequiredByNativeCode]
		public struct LegacyFixedAnimationUpdate
		{
		}

		// Token: 0x02000191 RID: 401
		[RequiredByNativeCode]
		public struct XRFixedUpdate
		{
		}

		// Token: 0x02000192 RID: 402
		[RequiredByNativeCode]
		public struct PhysicsFixedUpdate
		{
		}

		// Token: 0x02000193 RID: 403
		[RequiredByNativeCode]
		public struct Physics2DFixedUpdate
		{
		}

		// Token: 0x02000194 RID: 404
		[RequiredByNativeCode]
		public struct DirectorFixedUpdatePostPhysics
		{
		}

		// Token: 0x02000195 RID: 405
		[RequiredByNativeCode]
		public struct ScriptRunDelayedFixedFrameRate
		{
		}

		// Token: 0x02000196 RID: 406
		[RequiredByNativeCode]
		public struct NewInputFixedUpdate
		{
		}
	}
}
