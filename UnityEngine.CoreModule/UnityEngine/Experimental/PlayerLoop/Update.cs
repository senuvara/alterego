﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.PlayerLoop
{
	// Token: 0x020001A1 RID: 417
	[RequiredByNativeCode]
	public struct Update
	{
		// Token: 0x020001A2 RID: 418
		[RequiredByNativeCode]
		public struct ScriptRunBehaviourUpdate
		{
		}

		// Token: 0x020001A3 RID: 419
		[RequiredByNativeCode]
		public struct DirectorUpdate
		{
		}

		// Token: 0x020001A4 RID: 420
		[RequiredByNativeCode]
		public struct ScriptRunDelayedDynamicFrameRate
		{
		}

		// Token: 0x020001A5 RID: 421
		[RequiredByNativeCode]
		public struct ScriptRunDelayedTasks
		{
		}
	}
}
