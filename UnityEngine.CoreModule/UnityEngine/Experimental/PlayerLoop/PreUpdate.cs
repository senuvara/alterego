﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.PlayerLoop
{
	// Token: 0x02000197 RID: 407
	[RequiredByNativeCode]
	public struct PreUpdate
	{
		// Token: 0x02000198 RID: 408
		[RequiredByNativeCode]
		public struct PhysicsUpdate
		{
		}

		// Token: 0x02000199 RID: 409
		[RequiredByNativeCode]
		public struct Physics2DUpdate
		{
		}

		// Token: 0x0200019A RID: 410
		[RequiredByNativeCode]
		public struct CheckTexFieldInput
		{
		}

		// Token: 0x0200019B RID: 411
		[RequiredByNativeCode]
		public struct IMGUISendQueuedEvents
		{
		}

		// Token: 0x0200019C RID: 412
		[RequiredByNativeCode]
		public struct SendMouseEvents
		{
		}

		// Token: 0x0200019D RID: 413
		[RequiredByNativeCode]
		public struct AIUpdate
		{
		}

		// Token: 0x0200019E RID: 414
		[RequiredByNativeCode]
		public struct WindUpdate
		{
		}

		// Token: 0x0200019F RID: 415
		[RequiredByNativeCode]
		public struct UpdateVideo
		{
		}

		// Token: 0x020001A0 RID: 416
		[RequiredByNativeCode]
		public struct NewInputUpdate
		{
		}
	}
}
