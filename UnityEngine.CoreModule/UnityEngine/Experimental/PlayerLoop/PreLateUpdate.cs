﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.PlayerLoop
{
	// Token: 0x020001A6 RID: 422
	[RequiredByNativeCode]
	public struct PreLateUpdate
	{
		// Token: 0x020001A7 RID: 423
		[RequiredByNativeCode]
		public struct AIUpdatePostScript
		{
		}

		// Token: 0x020001A8 RID: 424
		[RequiredByNativeCode]
		public struct DirectorUpdateAnimationBegin
		{
		}

		// Token: 0x020001A9 RID: 425
		[RequiredByNativeCode]
		public struct LegacyAnimationUpdate
		{
		}

		// Token: 0x020001AA RID: 426
		[RequiredByNativeCode]
		public struct DirectorUpdateAnimationEnd
		{
		}

		// Token: 0x020001AB RID: 427
		[RequiredByNativeCode]
		public struct DirectorDeferredEvaluate
		{
		}

		// Token: 0x020001AC RID: 428
		[RequiredByNativeCode]
		public struct UpdateNetworkManager
		{
		}

		// Token: 0x020001AD RID: 429
		[RequiredByNativeCode]
		public struct UpdateMasterServerInterface
		{
		}

		// Token: 0x020001AE RID: 430
		[RequiredByNativeCode]
		public struct UNetUpdate
		{
		}

		// Token: 0x020001AF RID: 431
		[RequiredByNativeCode]
		public struct EndGraphicsJobsAfterScriptUpdate
		{
		}

		// Token: 0x020001B0 RID: 432
		[RequiredByNativeCode]
		public struct ParticleSystemBeginUpdateAll
		{
		}

		// Token: 0x020001B1 RID: 433
		[RequiredByNativeCode]
		public struct ScriptRunBehaviourLateUpdate
		{
		}

		// Token: 0x020001B2 RID: 434
		[RequiredByNativeCode]
		public struct ConstraintManagerUpdate
		{
		}
	}
}
