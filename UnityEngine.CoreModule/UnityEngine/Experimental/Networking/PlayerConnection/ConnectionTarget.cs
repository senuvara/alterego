﻿using System;

namespace UnityEngine.Experimental.Networking.PlayerConnection
{
	// Token: 0x020002C7 RID: 711
	public enum ConnectionTarget
	{
		// Token: 0x04000914 RID: 2324
		None,
		// Token: 0x04000915 RID: 2325
		Player,
		// Token: 0x04000916 RID: 2326
		Editor
	}
}
