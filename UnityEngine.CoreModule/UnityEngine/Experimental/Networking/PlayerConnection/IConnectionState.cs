﻿using System;

namespace UnityEngine.Experimental.Networking.PlayerConnection
{
	// Token: 0x020002C8 RID: 712
	public interface IConnectionState : IDisposable
	{
		// Token: 0x1700048A RID: 1162
		// (get) Token: 0x06001949 RID: 6473
		ConnectionTarget connectedToTarget { get; }

		// Token: 0x1700048B RID: 1163
		// (get) Token: 0x0600194A RID: 6474
		string connectionName { get; }
	}
}
