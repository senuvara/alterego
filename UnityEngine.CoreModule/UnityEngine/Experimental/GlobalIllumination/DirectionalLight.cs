﻿using System;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002AC RID: 684
	public struct DirectionalLight
	{
		// Token: 0x040008BB RID: 2235
		public int instanceID;

		// Token: 0x040008BC RID: 2236
		public bool shadow;

		// Token: 0x040008BD RID: 2237
		public LightMode mode;

		// Token: 0x040008BE RID: 2238
		public Vector3 direction;

		// Token: 0x040008BF RID: 2239
		public LinearColor color;

		// Token: 0x040008C0 RID: 2240
		public LinearColor indirectColor;

		// Token: 0x040008C1 RID: 2241
		public float penumbraWidthRadian;
	}
}
