﻿using System;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002AA RID: 682
	public enum FalloffType : byte
	{
		// Token: 0x040008B2 RID: 2226
		InverseSquared,
		// Token: 0x040008B3 RID: 2227
		InverseSquaredNoRangeAttenuation,
		// Token: 0x040008B4 RID: 2228
		Linear,
		// Token: 0x040008B5 RID: 2229
		Legacy,
		// Token: 0x040008B6 RID: 2230
		Undefined
	}
}
