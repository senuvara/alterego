﻿using System;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002A9 RID: 681
	public enum LightMode : byte
	{
		// Token: 0x040008AD RID: 2221
		Realtime,
		// Token: 0x040008AE RID: 2222
		Mixed,
		// Token: 0x040008AF RID: 2223
		Baked,
		// Token: 0x040008B0 RID: 2224
		Unknown
	}
}
