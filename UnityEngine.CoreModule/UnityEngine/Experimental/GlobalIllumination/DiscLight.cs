﻿using System;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002B0 RID: 688
	public struct DiscLight
	{
		// Token: 0x040008E1 RID: 2273
		public int instanceID;

		// Token: 0x040008E2 RID: 2274
		public bool shadow;

		// Token: 0x040008E3 RID: 2275
		public LightMode mode;

		// Token: 0x040008E4 RID: 2276
		public Vector3 position;

		// Token: 0x040008E5 RID: 2277
		public Quaternion orientation;

		// Token: 0x040008E6 RID: 2278
		public LinearColor color;

		// Token: 0x040008E7 RID: 2279
		public LinearColor indirectColor;

		// Token: 0x040008E8 RID: 2280
		public float range;

		// Token: 0x040008E9 RID: 2281
		public float radius;
	}
}
