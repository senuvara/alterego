﻿using System;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002AE RID: 686
	public struct SpotLight
	{
		// Token: 0x040008CB RID: 2251
		public int instanceID;

		// Token: 0x040008CC RID: 2252
		public bool shadow;

		// Token: 0x040008CD RID: 2253
		public LightMode mode;

		// Token: 0x040008CE RID: 2254
		public Vector3 position;

		// Token: 0x040008CF RID: 2255
		public Quaternion orientation;

		// Token: 0x040008D0 RID: 2256
		public LinearColor color;

		// Token: 0x040008D1 RID: 2257
		public LinearColor indirectColor;

		// Token: 0x040008D2 RID: 2258
		public float range;

		// Token: 0x040008D3 RID: 2259
		public float sphereRadius;

		// Token: 0x040008D4 RID: 2260
		public float coneAngle;

		// Token: 0x040008D5 RID: 2261
		public float innerConeAngle;

		// Token: 0x040008D6 RID: 2262
		public FalloffType falloff;
	}
}
