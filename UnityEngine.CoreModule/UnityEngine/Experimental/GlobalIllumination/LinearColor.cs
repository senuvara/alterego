﻿using System;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002AB RID: 683
	public struct LinearColor
	{
		// Token: 0x17000472 RID: 1138
		// (get) Token: 0x06001896 RID: 6294 RVA: 0x0002AADC File Offset: 0x00028CDC
		// (set) Token: 0x06001897 RID: 6295 RVA: 0x0002AAF7 File Offset: 0x00028CF7
		public float red
		{
			get
			{
				return this.m_red;
			}
			set
			{
				if (value < 0f || value > 1f)
				{
					throw new ArgumentOutOfRangeException("Red color (" + value + ") must be in range [0;1].");
				}
				this.m_red = value;
			}
		}

		// Token: 0x17000473 RID: 1139
		// (get) Token: 0x06001898 RID: 6296 RVA: 0x0002AB34 File Offset: 0x00028D34
		// (set) Token: 0x06001899 RID: 6297 RVA: 0x0002AB4F File Offset: 0x00028D4F
		public float green
		{
			get
			{
				return this.m_green;
			}
			set
			{
				if (value < 0f || value > 1f)
				{
					throw new ArgumentOutOfRangeException("Green color (" + value + ") must be in range [0;1].");
				}
				this.m_green = value;
			}
		}

		// Token: 0x17000474 RID: 1140
		// (get) Token: 0x0600189A RID: 6298 RVA: 0x0002AB8C File Offset: 0x00028D8C
		// (set) Token: 0x0600189B RID: 6299 RVA: 0x0002ABA7 File Offset: 0x00028DA7
		public float blue
		{
			get
			{
				return this.m_blue;
			}
			set
			{
				if (value < 0f || value > 1f)
				{
					throw new ArgumentOutOfRangeException("Blue color (" + value + ") must be in range [0;1].");
				}
				this.m_blue = value;
			}
		}

		// Token: 0x17000475 RID: 1141
		// (get) Token: 0x0600189C RID: 6300 RVA: 0x0002ABE4 File Offset: 0x00028DE4
		// (set) Token: 0x0600189D RID: 6301 RVA: 0x0002ABFF File Offset: 0x00028DFF
		public float intensity
		{
			get
			{
				return this.m_intensity;
			}
			set
			{
				if (value < 0f)
				{
					throw new ArgumentOutOfRangeException("Intensity (" + value + ") must be positive.");
				}
				this.m_intensity = value;
			}
		}

		// Token: 0x0600189E RID: 6302 RVA: 0x0002AC30 File Offset: 0x00028E30
		public static LinearColor Convert(Color color, float intensity)
		{
			Color color2 = (!GraphicsSettings.lightsUseLinearIntensity) ? color.RGBMultiplied(intensity).linear : color.linear.RGBMultiplied(intensity);
			float maxColorComponent = color2.maxColorComponent;
			LinearColor result;
			if (maxColorComponent <= 0f)
			{
				result = LinearColor.Black();
			}
			else
			{
				float num = 1f / color2.maxColorComponent;
				LinearColor linearColor;
				linearColor.m_red = color2.r * num;
				linearColor.m_green = color2.g * num;
				linearColor.m_blue = color2.b * num;
				linearColor.m_intensity = maxColorComponent;
				result = linearColor;
			}
			return result;
		}

		// Token: 0x0600189F RID: 6303 RVA: 0x0002ACE4 File Offset: 0x00028EE4
		public static LinearColor Black()
		{
			LinearColor result;
			result.m_red = (result.m_green = (result.m_blue = (result.m_intensity = 0f)));
			return result;
		}

		// Token: 0x040008B7 RID: 2231
		private float m_red;

		// Token: 0x040008B8 RID: 2232
		private float m_green;

		// Token: 0x040008B9 RID: 2233
		private float m_blue;

		// Token: 0x040008BA RID: 2234
		private float m_intensity;
	}
}
