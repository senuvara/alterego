﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002B1 RID: 689
	[UsedByNativeCode]
	public struct LightDataGI
	{
		// Token: 0x060018A0 RID: 6304 RVA: 0x0002AD24 File Offset: 0x00028F24
		public void Init(ref DirectionalLight light)
		{
			this.instanceID = light.instanceID;
			this.color = light.color;
			this.indirectColor = light.indirectColor;
			this.orientation.SetLookRotation(light.direction, Vector3.up);
			this.position = Vector3.zero;
			this.range = 0f;
			this.coneAngle = 0f;
			this.innerConeAngle = 0f;
			this.shape0 = light.penumbraWidthRadian;
			this.shape1 = 0f;
			this.type = LightType.Directional;
			this.mode = light.mode;
			this.shadow = ((!light.shadow) ? 0 : 1);
			this.falloff = FalloffType.Undefined;
		}

		// Token: 0x060018A1 RID: 6305 RVA: 0x0002ADE4 File Offset: 0x00028FE4
		public void Init(ref PointLight light)
		{
			this.instanceID = light.instanceID;
			this.color = light.color;
			this.indirectColor = light.indirectColor;
			this.orientation = Quaternion.identity;
			this.position = light.position;
			this.range = light.range;
			this.coneAngle = 0f;
			this.innerConeAngle = 0f;
			this.shape0 = light.sphereRadius;
			this.shape1 = 0f;
			this.type = LightType.Point;
			this.mode = light.mode;
			this.shadow = ((!light.shadow) ? 0 : 1);
			this.falloff = light.falloff;
		}

		// Token: 0x060018A2 RID: 6306 RVA: 0x0002AEA0 File Offset: 0x000290A0
		public void Init(ref SpotLight light)
		{
			this.instanceID = light.instanceID;
			this.color = light.color;
			this.indirectColor = light.indirectColor;
			this.orientation = light.orientation;
			this.position = light.position;
			this.range = light.range;
			this.coneAngle = light.coneAngle;
			this.innerConeAngle = light.innerConeAngle;
			this.shape0 = light.sphereRadius;
			this.shape1 = 0f;
			this.type = LightType.Spot;
			this.mode = light.mode;
			this.shadow = ((!light.shadow) ? 0 : 1);
			this.falloff = light.falloff;
		}

		// Token: 0x060018A3 RID: 6307 RVA: 0x0002AF60 File Offset: 0x00029160
		public void Init(ref RectangleLight light)
		{
			this.instanceID = light.instanceID;
			this.color = light.color;
			this.indirectColor = light.indirectColor;
			this.orientation = light.orientation;
			this.position = light.position;
			this.range = light.range;
			this.coneAngle = 0f;
			this.innerConeAngle = 0f;
			this.shape0 = light.width;
			this.shape1 = light.height;
			this.type = LightType.Rectangle;
			this.mode = light.mode;
			this.shadow = ((!light.shadow) ? 0 : 1);
			this.falloff = FalloffType.Undefined;
		}

		// Token: 0x060018A4 RID: 6308 RVA: 0x0002B018 File Offset: 0x00029218
		public void Init(ref DiscLight light)
		{
			this.instanceID = light.instanceID;
			this.color = light.color;
			this.indirectColor = light.indirectColor;
			this.orientation = light.orientation;
			this.position = light.position;
			this.range = light.range;
			this.coneAngle = 0f;
			this.innerConeAngle = 0f;
			this.shape0 = light.radius;
			this.shape1 = 0f;
			this.type = LightType.Disc;
			this.mode = light.mode;
			this.shadow = ((!light.shadow) ? 0 : 1);
			this.falloff = FalloffType.Undefined;
		}

		// Token: 0x060018A5 RID: 6309 RVA: 0x0002B0CE File Offset: 0x000292CE
		public void InitNoBake(int lightInstanceID)
		{
			this.instanceID = lightInstanceID;
			this.mode = LightMode.Unknown;
		}

		// Token: 0x040008EA RID: 2282
		public int instanceID;

		// Token: 0x040008EB RID: 2283
		public LinearColor color;

		// Token: 0x040008EC RID: 2284
		public LinearColor indirectColor;

		// Token: 0x040008ED RID: 2285
		public Quaternion orientation;

		// Token: 0x040008EE RID: 2286
		public Vector3 position;

		// Token: 0x040008EF RID: 2287
		public float range;

		// Token: 0x040008F0 RID: 2288
		public float coneAngle;

		// Token: 0x040008F1 RID: 2289
		public float innerConeAngle;

		// Token: 0x040008F2 RID: 2290
		public float shape0;

		// Token: 0x040008F3 RID: 2291
		public float shape1;

		// Token: 0x040008F4 RID: 2292
		public LightType type;

		// Token: 0x040008F5 RID: 2293
		public LightMode mode;

		// Token: 0x040008F6 RID: 2294
		public byte shadow;

		// Token: 0x040008F7 RID: 2295
		public FalloffType falloff;
	}
}
