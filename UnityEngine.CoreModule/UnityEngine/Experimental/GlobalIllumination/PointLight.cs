﻿using System;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002AD RID: 685
	public struct PointLight
	{
		// Token: 0x040008C2 RID: 2242
		public int instanceID;

		// Token: 0x040008C3 RID: 2243
		public bool shadow;

		// Token: 0x040008C4 RID: 2244
		public LightMode mode;

		// Token: 0x040008C5 RID: 2245
		public Vector3 position;

		// Token: 0x040008C6 RID: 2246
		public LinearColor color;

		// Token: 0x040008C7 RID: 2247
		public LinearColor indirectColor;

		// Token: 0x040008C8 RID: 2248
		public float range;

		// Token: 0x040008C9 RID: 2249
		public float sphereRadius;

		// Token: 0x040008CA RID: 2250
		public FalloffType falloff;
	}
}
