﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002B3 RID: 691
	public static class Lightmapping
	{
		// Token: 0x060018AE RID: 6318 RVA: 0x0002B45F File Offset: 0x0002965F
		public static void SetDelegate(Lightmapping.RequestLightsDelegate del)
		{
			Lightmapping.s_RequestLightsDelegate = ((del == null) ? Lightmapping.s_DefaultDelegate : del);
		}

		// Token: 0x060018AF RID: 6319 RVA: 0x0002B478 File Offset: 0x00029678
		public static Lightmapping.RequestLightsDelegate GetDelegate()
		{
			return Lightmapping.s_RequestLightsDelegate;
		}

		// Token: 0x060018B0 RID: 6320 RVA: 0x0002B492 File Offset: 0x00029692
		public static void ResetDelegate()
		{
			Lightmapping.s_RequestLightsDelegate = Lightmapping.s_DefaultDelegate;
		}

		// Token: 0x060018B1 RID: 6321 RVA: 0x0002B4A0 File Offset: 0x000296A0
		[UsedByNativeCode]
		internal unsafe static void RequestLights(Light[] lights, IntPtr outLightsPtr, int outLightsCount)
		{
			NativeArray<LightDataGI> lightsOutput = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<LightDataGI>((void*)outLightsPtr, outLightsCount, Allocator.None);
			Lightmapping.s_RequestLightsDelegate(lights, lightsOutput);
		}

		// Token: 0x060018B2 RID: 6322 RVA: 0x0002B4C8 File Offset: 0x000296C8
		// Note: this type is marked as 'beforefieldinit'.
		static Lightmapping()
		{
		}

		// Token: 0x060018B3 RID: 6323 RVA: 0x0002B4E8 File Offset: 0x000296E8
		[CompilerGenerated]
		private static void <s_DefaultDelegate>m__0(Light[] requests, NativeArray<LightDataGI> lightsOutput)
		{
			DirectionalLight directionalLight = default(DirectionalLight);
			PointLight pointLight = default(PointLight);
			SpotLight spotLight = default(SpotLight);
			RectangleLight rectangleLight = default(RectangleLight);
			DiscLight discLight = default(DiscLight);
			LightDataGI value = default(LightDataGI);
			for (int i = 0; i < requests.Length; i++)
			{
				Light light = requests[i];
				switch (light.type)
				{
				case LightType.Spot:
					LightmapperUtils.Extract(light, ref spotLight);
					value.Init(ref spotLight);
					break;
				case LightType.Directional:
					LightmapperUtils.Extract(light, ref directionalLight);
					value.Init(ref directionalLight);
					break;
				case LightType.Point:
					LightmapperUtils.Extract(light, ref pointLight);
					value.Init(ref pointLight);
					break;
				case LightType.Area:
					LightmapperUtils.Extract(light, ref rectangleLight);
					value.Init(ref rectangleLight);
					break;
				case LightType.Disc:
					LightmapperUtils.Extract(light, ref discLight);
					value.Init(ref discLight);
					break;
				default:
					value.InitNoBake(light.GetInstanceID());
					break;
				}
				lightsOutput[i] = value;
			}
		}

		// Token: 0x040008F8 RID: 2296
		private static readonly Lightmapping.RequestLightsDelegate s_DefaultDelegate = delegate(Light[] requests, NativeArray<LightDataGI> lightsOutput)
		{
			DirectionalLight directionalLight = default(DirectionalLight);
			PointLight pointLight = default(PointLight);
			SpotLight spotLight = default(SpotLight);
			RectangleLight rectangleLight = default(RectangleLight);
			DiscLight discLight = default(DiscLight);
			LightDataGI value = default(LightDataGI);
			for (int i = 0; i < requests.Length; i++)
			{
				Light light = requests[i];
				switch (light.type)
				{
				case LightType.Spot:
					LightmapperUtils.Extract(light, ref spotLight);
					value.Init(ref spotLight);
					break;
				case LightType.Directional:
					LightmapperUtils.Extract(light, ref directionalLight);
					value.Init(ref directionalLight);
					break;
				case LightType.Point:
					LightmapperUtils.Extract(light, ref pointLight);
					value.Init(ref pointLight);
					break;
				case LightType.Area:
					LightmapperUtils.Extract(light, ref rectangleLight);
					value.Init(ref rectangleLight);
					break;
				case LightType.Disc:
					LightmapperUtils.Extract(light, ref discLight);
					value.Init(ref discLight);
					break;
				default:
					value.InitNoBake(light.GetInstanceID());
					break;
				}
				lightsOutput[i] = value;
			}
		};

		// Token: 0x040008F9 RID: 2297
		private static Lightmapping.RequestLightsDelegate s_RequestLightsDelegate = Lightmapping.s_DefaultDelegate;

		// Token: 0x020002B4 RID: 692
		// (Invoke) Token: 0x060018B5 RID: 6325
		public delegate void RequestLightsDelegate(Light[] requests, NativeArray<LightDataGI> lightsOutput);
	}
}
