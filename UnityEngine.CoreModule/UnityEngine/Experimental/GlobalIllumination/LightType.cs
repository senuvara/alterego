﻿using System;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002A8 RID: 680
	public enum LightType : byte
	{
		// Token: 0x040008A7 RID: 2215
		Directional,
		// Token: 0x040008A8 RID: 2216
		Point,
		// Token: 0x040008A9 RID: 2217
		Spot,
		// Token: 0x040008AA RID: 2218
		Rectangle,
		// Token: 0x040008AB RID: 2219
		Disc
	}
}
