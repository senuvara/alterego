﻿using System;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002AF RID: 687
	public struct RectangleLight
	{
		// Token: 0x040008D7 RID: 2263
		public int instanceID;

		// Token: 0x040008D8 RID: 2264
		public bool shadow;

		// Token: 0x040008D9 RID: 2265
		public LightMode mode;

		// Token: 0x040008DA RID: 2266
		public Vector3 position;

		// Token: 0x040008DB RID: 2267
		public Quaternion orientation;

		// Token: 0x040008DC RID: 2268
		public LinearColor color;

		// Token: 0x040008DD RID: 2269
		public LinearColor indirectColor;

		// Token: 0x040008DE RID: 2270
		public float range;

		// Token: 0x040008DF RID: 2271
		public float width;

		// Token: 0x040008E0 RID: 2272
		public float height;
	}
}
