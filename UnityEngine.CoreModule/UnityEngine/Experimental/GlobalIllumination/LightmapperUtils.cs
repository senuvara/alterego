﻿using System;

namespace UnityEngine.Experimental.GlobalIllumination
{
	// Token: 0x020002B2 RID: 690
	public static class LightmapperUtils
	{
		// Token: 0x060018A6 RID: 6310 RVA: 0x0002B0E0 File Offset: 0x000292E0
		public static LightMode Extract(LightmapBakeType baketype)
		{
			return (baketype != LightmapBakeType.Realtime) ? ((baketype != LightmapBakeType.Mixed) ? LightMode.Baked : LightMode.Mixed) : LightMode.Realtime;
		}

		// Token: 0x060018A7 RID: 6311 RVA: 0x0002B110 File Offset: 0x00029310
		public static LinearColor ExtractIndirect(Light l)
		{
			return LinearColor.Convert(l.color, l.intensity * l.bounceIntensity);
		}

		// Token: 0x060018A8 RID: 6312 RVA: 0x0002B140 File Offset: 0x00029340
		public static float ExtractInnerCone(Light l)
		{
			return 2f * Mathf.Atan(Mathf.Tan(l.spotAngle * 0.5f * 0.017453292f) * 46f / 64f);
		}

		// Token: 0x060018A9 RID: 6313 RVA: 0x0002B184 File Offset: 0x00029384
		public static void Extract(Light l, ref DirectionalLight dir)
		{
			dir.instanceID = l.GetInstanceID();
			dir.mode = LightMode.Realtime;
			dir.shadow = (l.shadows != LightShadows.None);
			dir.direction = l.transform.forward;
			dir.color = LinearColor.Convert(l.color, l.intensity);
			dir.indirectColor = LightmapperUtils.ExtractIndirect(l);
			dir.penumbraWidthRadian = 0f;
		}

		// Token: 0x060018AA RID: 6314 RVA: 0x0002B1F8 File Offset: 0x000293F8
		public static void Extract(Light l, ref PointLight point)
		{
			point.instanceID = l.GetInstanceID();
			point.mode = LightMode.Realtime;
			point.shadow = (l.shadows != LightShadows.None);
			point.position = l.transform.position;
			point.color = LinearColor.Convert(l.color, l.intensity);
			point.indirectColor = LightmapperUtils.ExtractIndirect(l);
			point.range = l.range;
			point.sphereRadius = 0f;
			point.falloff = FalloffType.Legacy;
		}

		// Token: 0x060018AB RID: 6315 RVA: 0x0002B280 File Offset: 0x00029480
		public static void Extract(Light l, ref SpotLight spot)
		{
			spot.instanceID = l.GetInstanceID();
			spot.mode = LightMode.Realtime;
			spot.shadow = (l.shadows != LightShadows.None);
			spot.position = l.transform.position;
			spot.orientation = l.transform.rotation;
			spot.color = LinearColor.Convert(l.color, l.intensity);
			spot.indirectColor = LightmapperUtils.ExtractIndirect(l);
			spot.range = l.range;
			spot.sphereRadius = 0f;
			spot.coneAngle = l.spotAngle * 0.017453292f;
			spot.innerConeAngle = LightmapperUtils.ExtractInnerCone(l);
			spot.falloff = FalloffType.Legacy;
		}

		// Token: 0x060018AC RID: 6316 RVA: 0x0002B334 File Offset: 0x00029534
		public static void Extract(Light l, ref RectangleLight rect)
		{
			rect.instanceID = l.GetInstanceID();
			rect.mode = LightMode.Realtime;
			rect.shadow = (l.shadows != LightShadows.None);
			rect.position = l.transform.position;
			rect.orientation = l.transform.rotation;
			rect.color = LinearColor.Convert(l.color, l.intensity);
			rect.indirectColor = LightmapperUtils.ExtractIndirect(l);
			rect.range = l.range;
			rect.width = 0f;
			rect.height = 0f;
		}

		// Token: 0x060018AD RID: 6317 RVA: 0x0002B3D0 File Offset: 0x000295D0
		public static void Extract(Light l, ref DiscLight disc)
		{
			disc.instanceID = l.GetInstanceID();
			disc.mode = LightMode.Realtime;
			disc.shadow = (l.shadows != LightShadows.None);
			disc.position = l.transform.position;
			disc.orientation = l.transform.rotation;
			disc.color = LinearColor.Convert(l.color, l.intensity);
			disc.indirectColor = LightmapperUtils.ExtractIndirect(l);
			disc.range = l.range;
			disc.radius = 0f;
		}
	}
}
