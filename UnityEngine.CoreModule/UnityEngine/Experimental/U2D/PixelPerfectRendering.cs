﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.U2D
{
	// Token: 0x02000354 RID: 852
	[NativeHeader("Runtime/2D/Common/PixelSnapping.h")]
	public static class PixelPerfectRendering
	{
		// Token: 0x17000552 RID: 1362
		// (get) Token: 0x06001D17 RID: 7447
		// (set) Token: 0x06001D18 RID: 7448
		public static extern float pixelSnapSpacing { [FreeFunction("GetPixelSnapSpacing")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("SetPixelSnapSpacing")] [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
