﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.U2D
{
	// Token: 0x02000355 RID: 853
	[NativeHeader("Runtime/2D/Common/SpriteDataAccess.h")]
	[RequiredByNativeCode]
	[NativeType(CodegenOptions.Custom, "ScriptingSpriteBone")]
	[Serializable]
	public struct SpriteBone
	{
		// Token: 0x17000553 RID: 1363
		// (get) Token: 0x06001D19 RID: 7449 RVA: 0x00032A9C File Offset: 0x00030C9C
		// (set) Token: 0x06001D1A RID: 7450 RVA: 0x00032AB7 File Offset: 0x00030CB7
		public string name
		{
			get
			{
				return this.m_Name;
			}
			set
			{
				this.m_Name = value;
			}
		}

		// Token: 0x17000554 RID: 1364
		// (get) Token: 0x06001D1B RID: 7451 RVA: 0x00032AC4 File Offset: 0x00030CC4
		// (set) Token: 0x06001D1C RID: 7452 RVA: 0x00032ADF File Offset: 0x00030CDF
		public Vector3 position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				this.m_Position = value;
			}
		}

		// Token: 0x17000555 RID: 1365
		// (get) Token: 0x06001D1D RID: 7453 RVA: 0x00032AEC File Offset: 0x00030CEC
		// (set) Token: 0x06001D1E RID: 7454 RVA: 0x00032B07 File Offset: 0x00030D07
		public Quaternion rotation
		{
			get
			{
				return this.m_Rotation;
			}
			set
			{
				this.m_Rotation = value;
			}
		}

		// Token: 0x17000556 RID: 1366
		// (get) Token: 0x06001D1F RID: 7455 RVA: 0x00032B14 File Offset: 0x00030D14
		// (set) Token: 0x06001D20 RID: 7456 RVA: 0x00032B2F File Offset: 0x00030D2F
		public float length
		{
			get
			{
				return this.m_Length;
			}
			set
			{
				this.m_Length = value;
			}
		}

		// Token: 0x17000557 RID: 1367
		// (get) Token: 0x06001D21 RID: 7457 RVA: 0x00032B3C File Offset: 0x00030D3C
		// (set) Token: 0x06001D22 RID: 7458 RVA: 0x00032B57 File Offset: 0x00030D57
		public int parentId
		{
			get
			{
				return this.m_ParentId;
			}
			set
			{
				this.m_ParentId = value;
			}
		}

		// Token: 0x04000AD7 RID: 2775
		[SerializeField]
		[NativeName("name")]
		private string m_Name;

		// Token: 0x04000AD8 RID: 2776
		[NativeName("position")]
		[SerializeField]
		private Vector3 m_Position;

		// Token: 0x04000AD9 RID: 2777
		[NativeName("rotation")]
		[SerializeField]
		private Quaternion m_Rotation;

		// Token: 0x04000ADA RID: 2778
		[SerializeField]
		[NativeName("length")]
		private float m_Length;

		// Token: 0x04000ADB RID: 2779
		[SerializeField]
		[NativeName("parentId")]
		private int m_ParentId;
	}
}
