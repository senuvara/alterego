﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.U2D
{
	// Token: 0x02000356 RID: 854
	internal struct SpriteChannelInfo
	{
		// Token: 0x17000558 RID: 1368
		// (get) Token: 0x06001D23 RID: 7459 RVA: 0x00032B64 File Offset: 0x00030D64
		// (set) Token: 0x06001D24 RID: 7460 RVA: 0x00032B84 File Offset: 0x00030D84
		public unsafe void* buffer
		{
			get
			{
				return (void*)this.m_Buffer;
			}
			set
			{
				this.m_Buffer = (IntPtr)value;
			}
		}

		// Token: 0x17000559 RID: 1369
		// (get) Token: 0x06001D25 RID: 7461 RVA: 0x00032B94 File Offset: 0x00030D94
		// (set) Token: 0x06001D26 RID: 7462 RVA: 0x00032BAF File Offset: 0x00030DAF
		public int count
		{
			get
			{
				return this.m_Count;
			}
			set
			{
				this.m_Count = value;
			}
		}

		// Token: 0x1700055A RID: 1370
		// (get) Token: 0x06001D27 RID: 7463 RVA: 0x00032BBC File Offset: 0x00030DBC
		// (set) Token: 0x06001D28 RID: 7464 RVA: 0x00032BD7 File Offset: 0x00030DD7
		public int offset
		{
			get
			{
				return this.m_Offset;
			}
			set
			{
				this.m_Offset = value;
			}
		}

		// Token: 0x1700055B RID: 1371
		// (get) Token: 0x06001D29 RID: 7465 RVA: 0x00032BE4 File Offset: 0x00030DE4
		// (set) Token: 0x06001D2A RID: 7466 RVA: 0x00032BFF File Offset: 0x00030DFF
		public int stride
		{
			get
			{
				return this.m_Stride;
			}
			set
			{
				this.m_Stride = value;
			}
		}

		// Token: 0x04000ADC RID: 2780
		[NativeName("buffer")]
		private IntPtr m_Buffer;

		// Token: 0x04000ADD RID: 2781
		[NativeName("count")]
		private int m_Count;

		// Token: 0x04000ADE RID: 2782
		[NativeName("offset")]
		private int m_Offset;

		// Token: 0x04000ADF RID: 2783
		[NativeName("stride")]
		private int m_Stride;
	}
}
