﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Bindings;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.U2D
{
	// Token: 0x02000357 RID: 855
	[NativeHeader("Runtime/2D/Common/SpriteDataAccess.h")]
	[NativeHeader("Runtime/Graphics/SpriteFrame.h")]
	public static class SpriteDataAccessExtensions
	{
		// Token: 0x06001D2B RID: 7467 RVA: 0x00032C0C File Offset: 0x00030E0C
		private static void CheckAttributeTypeMatchesAndThrow<T>(VertexAttribute channel)
		{
			bool flag;
			switch (channel)
			{
			case VertexAttribute.Position:
			case VertexAttribute.Normal:
				flag = (typeof(T) == typeof(Vector3));
				break;
			case VertexAttribute.Tangent:
				flag = (typeof(T) == typeof(Vector4));
				break;
			case VertexAttribute.Color:
				flag = (typeof(T) == typeof(Color32));
				break;
			case VertexAttribute.TexCoord0:
			case VertexAttribute.TexCoord1:
			case VertexAttribute.TexCoord2:
			case VertexAttribute.TexCoord3:
			case VertexAttribute.TexCoord4:
			case VertexAttribute.TexCoord5:
			case VertexAttribute.TexCoord6:
				flag = (typeof(T) == typeof(Vector2));
				break;
			default:
				throw new InvalidOperationException(string.Format("The requested channel '{0}' is unknown.", channel));
			}
			if (!flag)
			{
				throw new InvalidOperationException(string.Format("The requested channel '{0}' does not match the return type {1}.", channel, typeof(T).Name));
			}
		}

		// Token: 0x06001D2C RID: 7468 RVA: 0x00032D04 File Offset: 0x00030F04
		public unsafe static NativeSlice<T> GetVertexAttribute<T>(this Sprite sprite, VertexAttribute channel) where T : struct
		{
			SpriteDataAccessExtensions.CheckAttributeTypeMatchesAndThrow<T>(channel);
			SpriteChannelInfo channelInfo = SpriteDataAccessExtensions.GetChannelInfo(sprite, channel);
			byte* dataPointer = (byte*)channelInfo.buffer + channelInfo.offset;
			return NativeSliceUnsafeUtility.ConvertExistingDataToNativeSlice<T>((void*)dataPointer, channelInfo.stride, channelInfo.count);
		}

		// Token: 0x06001D2D RID: 7469 RVA: 0x00032D4E File Offset: 0x00030F4E
		public static void SetVertexAttribute<T>(this Sprite sprite, VertexAttribute channel, NativeArray<T> src) where T : struct
		{
			SpriteDataAccessExtensions.CheckAttributeTypeMatchesAndThrow<T>(channel);
			SpriteDataAccessExtensions.SetChannelData(sprite, channel, src.GetUnsafeReadOnlyPtr<T>());
		}

		// Token: 0x06001D2E RID: 7470 RVA: 0x00032D64 File Offset: 0x00030F64
		public static NativeArray<Matrix4x4> GetBindPoses(this Sprite sprite)
		{
			SpriteChannelInfo bindPoseInfo = SpriteDataAccessExtensions.GetBindPoseInfo(sprite);
			return NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<Matrix4x4>(bindPoseInfo.buffer, bindPoseInfo.count, Allocator.Invalid);
		}

		// Token: 0x06001D2F RID: 7471 RVA: 0x00032D96 File Offset: 0x00030F96
		public static void SetBindPoses(this Sprite sprite, NativeArray<Matrix4x4> src)
		{
			SpriteDataAccessExtensions.SetBindPoseData(sprite, src.GetUnsafeReadOnlyPtr<Matrix4x4>(), src.Length);
		}

		// Token: 0x06001D30 RID: 7472 RVA: 0x00032DAC File Offset: 0x00030FAC
		public static NativeArray<ushort> GetIndices(this Sprite sprite)
		{
			SpriteChannelInfo indicesInfo = SpriteDataAccessExtensions.GetIndicesInfo(sprite);
			return NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<ushort>(indicesInfo.buffer, indicesInfo.count, Allocator.Invalid);
		}

		// Token: 0x06001D31 RID: 7473 RVA: 0x00032DDE File Offset: 0x00030FDE
		public static void SetIndices(this Sprite sprite, NativeArray<ushort> src)
		{
			SpriteDataAccessExtensions.SetIndicesData(sprite, src.GetUnsafeReadOnlyPtr<ushort>(), src.Length);
		}

		// Token: 0x06001D32 RID: 7474 RVA: 0x00032DF4 File Offset: 0x00030FF4
		public static NativeArray<BoneWeight> GetBoneWeights(this Sprite sprite)
		{
			SpriteChannelInfo boneWeightsInfo = SpriteDataAccessExtensions.GetBoneWeightsInfo(sprite);
			return NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<BoneWeight>(boneWeightsInfo.buffer, boneWeightsInfo.count, Allocator.Invalid);
		}

		// Token: 0x06001D33 RID: 7475 RVA: 0x00032E26 File Offset: 0x00031026
		public static void SetBoneWeights(this Sprite sprite, NativeArray<BoneWeight> src)
		{
			SpriteDataAccessExtensions.SetBoneWeightsData(sprite, src.GetUnsafeReadOnlyPtr<BoneWeight>(), src.Length);
		}

		// Token: 0x06001D34 RID: 7476 RVA: 0x00032E3C File Offset: 0x0003103C
		public static SpriteBone[] GetBones(this Sprite sprite)
		{
			return SpriteDataAccessExtensions.GetBoneInfo(sprite);
		}

		// Token: 0x06001D35 RID: 7477 RVA: 0x00032E57 File Offset: 0x00031057
		public static void SetBones(this Sprite sprite, SpriteBone[] src)
		{
			SpriteDataAccessExtensions.SetBoneData(sprite, src);
		}

		// Token: 0x06001D36 RID: 7478
		[NativeName("HasChannel")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool HasVertexAttribute(this Sprite sprite, VertexAttribute channel);

		// Token: 0x06001D37 RID: 7479
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetVertexCount(this Sprite sprite, int count);

		// Token: 0x06001D38 RID: 7480
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetVertexCount(this Sprite sprite);

		// Token: 0x06001D39 RID: 7481 RVA: 0x00032E64 File Offset: 0x00031064
		private static SpriteChannelInfo GetBindPoseInfo(Sprite sprite)
		{
			SpriteChannelInfo result;
			SpriteDataAccessExtensions.GetBindPoseInfo_Injected(sprite, out result);
			return result;
		}

		// Token: 0x06001D3A RID: 7482
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void SetBindPoseData(Sprite sprite, void* src, int count);

		// Token: 0x06001D3B RID: 7483 RVA: 0x00032E7C File Offset: 0x0003107C
		private static SpriteChannelInfo GetIndicesInfo(Sprite sprite)
		{
			SpriteChannelInfo result;
			SpriteDataAccessExtensions.GetIndicesInfo_Injected(sprite, out result);
			return result;
		}

		// Token: 0x06001D3C RID: 7484
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void SetIndicesData(Sprite sprite, void* src, int count);

		// Token: 0x06001D3D RID: 7485 RVA: 0x00032E94 File Offset: 0x00031094
		private static SpriteChannelInfo GetChannelInfo(Sprite sprite, VertexAttribute channel)
		{
			SpriteChannelInfo result;
			SpriteDataAccessExtensions.GetChannelInfo_Injected(sprite, channel, out result);
			return result;
		}

		// Token: 0x06001D3E RID: 7486
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void SetChannelData(Sprite sprite, VertexAttribute channel, void* src);

		// Token: 0x06001D3F RID: 7487
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern SpriteBone[] GetBoneInfo(Sprite sprite);

		// Token: 0x06001D40 RID: 7488
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetBoneData(Sprite sprite, SpriteBone[] src);

		// Token: 0x06001D41 RID: 7489 RVA: 0x00032EAC File Offset: 0x000310AC
		private static SpriteChannelInfo GetBoneWeightsInfo(Sprite sprite)
		{
			SpriteChannelInfo result;
			SpriteDataAccessExtensions.GetBoneWeightsInfo_Injected(sprite, out result);
			return result;
		}

		// Token: 0x06001D42 RID: 7490
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void SetBoneWeightsData(Sprite sprite, void* src, int count);

		// Token: 0x06001D43 RID: 7491
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetBindPoseInfo_Injected(Sprite sprite, out SpriteChannelInfo ret);

		// Token: 0x06001D44 RID: 7492
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetIndicesInfo_Injected(Sprite sprite, out SpriteChannelInfo ret);

		// Token: 0x06001D45 RID: 7493
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetChannelInfo_Injected(Sprite sprite, VertexAttribute channel, out SpriteChannelInfo ret);

		// Token: 0x06001D46 RID: 7494
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetBoneWeightsInfo_Injected(Sprite sprite, out SpriteChannelInfo ret);
	}
}
