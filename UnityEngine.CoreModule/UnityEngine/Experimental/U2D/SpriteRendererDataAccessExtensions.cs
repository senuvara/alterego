﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using UnityEngine.Bindings;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.U2D
{
	// Token: 0x02000358 RID: 856
	[NativeHeader("Runtime/2D/Common/SpriteDataAccess.h")]
	[NativeHeader("Runtime/Graphics/Mesh/SpriteRenderer.h")]
	public static class SpriteRendererDataAccessExtensions
	{
		// Token: 0x06001D47 RID: 7495 RVA: 0x00032EC4 File Offset: 0x000310C4
		public static NativeArray<Vector3> GetDeformableVertices(this SpriteRenderer spriteRenderer)
		{
			SpriteChannelInfo deformableChannelInfo = spriteRenderer.GetDeformableChannelInfo(VertexAttribute.Position);
			return NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<Vector3>(deformableChannelInfo.buffer, deformableChannelInfo.count, Allocator.Invalid);
		}

		// Token: 0x06001D48 RID: 7496
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void DeactivateDeformableBuffer(this SpriteRenderer renderer);

		// Token: 0x06001D49 RID: 7497 RVA: 0x00032EF7 File Offset: 0x000310F7
		public static void UpdateDeformableBuffer(this SpriteRenderer spriteRenderer, JobHandle fence)
		{
			SpriteRendererDataAccessExtensions.UpdateDeformableBuffer_Injected(spriteRenderer, ref fence);
		}

		// Token: 0x06001D4A RID: 7498 RVA: 0x00032F04 File Offset: 0x00031104
		private static SpriteChannelInfo GetDeformableChannelInfo(this SpriteRenderer sprite, VertexAttribute channel)
		{
			SpriteChannelInfo result;
			SpriteRendererDataAccessExtensions.GetDeformableChannelInfo_Injected(sprite, channel, out result);
			return result;
		}

		// Token: 0x06001D4B RID: 7499 RVA: 0x00032F1B File Offset: 0x0003111B
		internal static void SetLocalAABB(this SpriteRenderer renderer, Bounds aabb)
		{
			SpriteRendererDataAccessExtensions.SetLocalAABB_Injected(renderer, ref aabb);
		}

		// Token: 0x06001D4C RID: 7500
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void UpdateDeformableBuffer_Injected(SpriteRenderer spriteRenderer, ref JobHandle fence);

		// Token: 0x06001D4D RID: 7501
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetDeformableChannelInfo_Injected(SpriteRenderer sprite, VertexAttribute channel, out SpriteChannelInfo ret);

		// Token: 0x06001D4E RID: 7502
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLocalAABB_Injected(SpriteRenderer renderer, ref Bounds aabb);
	}
}
