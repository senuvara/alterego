﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200014C RID: 332
	[NativeHeader("Runtime/Math/Vector3.h")]
	[ThreadAndSerializationSafe]
	[NativeType(Header = "Runtime/Math/Vector3.h")]
	[NativeHeader("Runtime/Math/MathScripting.h")]
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	[NativeClass("Vector3f")]
	public struct Vector3 : IEquatable<Vector3>
	{
		// Token: 0x06000D5A RID: 3418 RVA: 0x00015FAD File Offset: 0x000141AD
		public Vector3(float x, float y, float z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		// Token: 0x06000D5B RID: 3419 RVA: 0x00015FC5 File Offset: 0x000141C5
		public Vector3(float x, float y)
		{
			this.x = x;
			this.y = y;
			this.z = 0f;
		}

		// Token: 0x06000D5C RID: 3420 RVA: 0x00015FE4 File Offset: 0x000141E4
		[FreeFunction("VectorScripting::Slerp", IsThreadSafe = true)]
		public static Vector3 Slerp(Vector3 a, Vector3 b, float t)
		{
			Vector3 result;
			Vector3.Slerp_Injected(ref a, ref b, t, out result);
			return result;
		}

		// Token: 0x06000D5D RID: 3421 RVA: 0x00016000 File Offset: 0x00014200
		[FreeFunction("VectorScripting::SlerpUnclamped", IsThreadSafe = true)]
		public static Vector3 SlerpUnclamped(Vector3 a, Vector3 b, float t)
		{
			Vector3 result;
			Vector3.SlerpUnclamped_Injected(ref a, ref b, t, out result);
			return result;
		}

		// Token: 0x06000D5E RID: 3422
		[FreeFunction("VectorScripting::OrthoNormalize", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void OrthoNormalize2(ref Vector3 a, ref Vector3 b);

		// Token: 0x06000D5F RID: 3423 RVA: 0x0001601A File Offset: 0x0001421A
		public static void OrthoNormalize(ref Vector3 normal, ref Vector3 tangent)
		{
			Vector3.OrthoNormalize2(ref normal, ref tangent);
		}

		// Token: 0x06000D60 RID: 3424
		[FreeFunction("VectorScripting::OrthoNormalize", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void OrthoNormalize3(ref Vector3 a, ref Vector3 b, ref Vector3 c);

		// Token: 0x06000D61 RID: 3425 RVA: 0x00016024 File Offset: 0x00014224
		public static void OrthoNormalize(ref Vector3 normal, ref Vector3 tangent, ref Vector3 binormal)
		{
			Vector3.OrthoNormalize3(ref normal, ref tangent, ref binormal);
		}

		// Token: 0x06000D62 RID: 3426 RVA: 0x00016030 File Offset: 0x00014230
		[FreeFunction(IsThreadSafe = true)]
		public static Vector3 RotateTowards(Vector3 current, Vector3 target, float maxRadiansDelta, float maxMagnitudeDelta)
		{
			Vector3 result;
			Vector3.RotateTowards_Injected(ref current, ref target, maxRadiansDelta, maxMagnitudeDelta, out result);
			return result;
		}

		// Token: 0x06000D63 RID: 3427 RVA: 0x0001604C File Offset: 0x0001424C
		public static Vector3 Lerp(Vector3 a, Vector3 b, float t)
		{
			t = Mathf.Clamp01(t);
			return new Vector3(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t, a.z + (b.z - a.z) * t);
		}

		// Token: 0x06000D64 RID: 3428 RVA: 0x000160BC File Offset: 0x000142BC
		public static Vector3 LerpUnclamped(Vector3 a, Vector3 b, float t)
		{
			return new Vector3(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t, a.z + (b.z - a.z) * t);
		}

		// Token: 0x06000D65 RID: 3429 RVA: 0x00016124 File Offset: 0x00014324
		public static Vector3 MoveTowards(Vector3 current, Vector3 target, float maxDistanceDelta)
		{
			Vector3 a = target - current;
			float magnitude = a.magnitude;
			Vector3 result;
			if (magnitude <= maxDistanceDelta || magnitude < 1E-45f)
			{
				result = target;
			}
			else
			{
				result = current + a / magnitude * maxDistanceDelta;
			}
			return result;
		}

		// Token: 0x06000D66 RID: 3430 RVA: 0x00016178 File Offset: 0x00014378
		[ExcludeFromDocs]
		public static Vector3 SmoothDamp(Vector3 current, Vector3 target, ref Vector3 currentVelocity, float smoothTime, float maxSpeed)
		{
			float deltaTime = Time.deltaTime;
			return Vector3.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
		}

		// Token: 0x06000D67 RID: 3431 RVA: 0x000161A0 File Offset: 0x000143A0
		[ExcludeFromDocs]
		public static Vector3 SmoothDamp(Vector3 current, Vector3 target, ref Vector3 currentVelocity, float smoothTime)
		{
			float deltaTime = Time.deltaTime;
			float positiveInfinity = float.PositiveInfinity;
			return Vector3.SmoothDamp(current, target, ref currentVelocity, smoothTime, positiveInfinity, deltaTime);
		}

		// Token: 0x06000D68 RID: 3432 RVA: 0x000161CC File Offset: 0x000143CC
		public static Vector3 SmoothDamp(Vector3 current, Vector3 target, ref Vector3 currentVelocity, float smoothTime, [DefaultValue("Mathf.Infinity")] float maxSpeed, [DefaultValue("Time.deltaTime")] float deltaTime)
		{
			smoothTime = Mathf.Max(0.0001f, smoothTime);
			float num = 2f / smoothTime;
			float num2 = num * deltaTime;
			float d = 1f / (1f + num2 + 0.48f * num2 * num2 + 0.235f * num2 * num2 * num2);
			Vector3 vector = current - target;
			Vector3 vector2 = target;
			float maxLength = maxSpeed * smoothTime;
			vector = Vector3.ClampMagnitude(vector, maxLength);
			target = current - vector;
			Vector3 vector3 = (currentVelocity + num * vector) * deltaTime;
			currentVelocity = (currentVelocity - num * vector3) * d;
			Vector3 vector4 = target + (vector + vector3) * d;
			if (Vector3.Dot(vector2 - current, vector4 - vector2) > 0f)
			{
				vector4 = vector2;
				currentVelocity = (vector4 - vector2) / deltaTime;
			}
			return vector4;
		}

		// Token: 0x170002A8 RID: 680
		public float this[int index]
		{
			get
			{
				float result;
				switch (index)
				{
				case 0:
					result = this.x;
					break;
				case 1:
					result = this.y;
					break;
				case 2:
					result = this.z;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector3 index!");
				}
				return result;
			}
			set
			{
				switch (index)
				{
				case 0:
					this.x = value;
					break;
				case 1:
					this.y = value;
					break;
				case 2:
					this.z = value;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector3 index!");
				}
			}
		}

		// Token: 0x06000D6B RID: 3435 RVA: 0x00015FAD File Offset: 0x000141AD
		public void Set(float newX, float newY, float newZ)
		{
			this.x = newX;
			this.y = newY;
			this.z = newZ;
		}

		// Token: 0x06000D6C RID: 3436 RVA: 0x00016380 File Offset: 0x00014580
		public static Vector3 Scale(Vector3 a, Vector3 b)
		{
			return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
		}

		// Token: 0x06000D6D RID: 3437 RVA: 0x000163C7 File Offset: 0x000145C7
		public void Scale(Vector3 scale)
		{
			this.x *= scale.x;
			this.y *= scale.y;
			this.z *= scale.z;
		}

		// Token: 0x06000D6E RID: 3438 RVA: 0x00016408 File Offset: 0x00014608
		public static Vector3 Cross(Vector3 lhs, Vector3 rhs)
		{
			return new Vector3(lhs.y * rhs.z - lhs.z * rhs.y, lhs.z * rhs.x - lhs.x * rhs.z, lhs.x * rhs.y - lhs.y * rhs.x);
		}

		// Token: 0x06000D6F RID: 3439 RVA: 0x00016480 File Offset: 0x00014680
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.y.GetHashCode() << 2 ^ this.z.GetHashCode() >> 2;
		}

		// Token: 0x06000D70 RID: 3440 RVA: 0x000164D0 File Offset: 0x000146D0
		public override bool Equals(object other)
		{
			return other is Vector3 && this.Equals((Vector3)other);
		}

		// Token: 0x06000D71 RID: 3441 RVA: 0x00016504 File Offset: 0x00014704
		public bool Equals(Vector3 other)
		{
			return this.x.Equals(other.x) && this.y.Equals(other.y) && this.z.Equals(other.z);
		}

		// Token: 0x06000D72 RID: 3442 RVA: 0x0001655C File Offset: 0x0001475C
		public static Vector3 Reflect(Vector3 inDirection, Vector3 inNormal)
		{
			return -2f * Vector3.Dot(inNormal, inDirection) * inNormal + inDirection;
		}

		// Token: 0x06000D73 RID: 3443 RVA: 0x0001658C File Offset: 0x0001478C
		public static Vector3 Normalize(Vector3 value)
		{
			float num = Vector3.Magnitude(value);
			Vector3 result;
			if (num > 1E-05f)
			{
				result = value / num;
			}
			else
			{
				result = Vector3.zero;
			}
			return result;
		}

		// Token: 0x06000D74 RID: 3444 RVA: 0x000165C8 File Offset: 0x000147C8
		public void Normalize()
		{
			float num = Vector3.Magnitude(this);
			if (num > 1E-05f)
			{
				this /= num;
			}
			else
			{
				this = Vector3.zero;
			}
		}

		// Token: 0x170002A9 RID: 681
		// (get) Token: 0x06000D75 RID: 3445 RVA: 0x00016610 File Offset: 0x00014810
		public Vector3 normalized
		{
			get
			{
				return Vector3.Normalize(this);
			}
		}

		// Token: 0x06000D76 RID: 3446 RVA: 0x00016630 File Offset: 0x00014830
		public static float Dot(Vector3 lhs, Vector3 rhs)
		{
			return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
		}

		// Token: 0x06000D77 RID: 3447 RVA: 0x00016674 File Offset: 0x00014874
		public static Vector3 Project(Vector3 vector, Vector3 onNormal)
		{
			float num = Vector3.Dot(onNormal, onNormal);
			Vector3 result;
			if (num < Mathf.Epsilon)
			{
				result = Vector3.zero;
			}
			else
			{
				result = onNormal * Vector3.Dot(vector, onNormal) / num;
			}
			return result;
		}

		// Token: 0x06000D78 RID: 3448 RVA: 0x000166BC File Offset: 0x000148BC
		public static Vector3 ProjectOnPlane(Vector3 vector, Vector3 planeNormal)
		{
			return vector - Vector3.Project(vector, planeNormal);
		}

		// Token: 0x06000D79 RID: 3449 RVA: 0x000166E0 File Offset: 0x000148E0
		public static float Angle(Vector3 from, Vector3 to)
		{
			float num = Mathf.Sqrt(from.sqrMagnitude * to.sqrMagnitude);
			float result;
			if (num < 1E-15f)
			{
				result = 0f;
			}
			else
			{
				float f = Mathf.Clamp(Vector3.Dot(from, to) / num, -1f, 1f);
				result = Mathf.Acos(f) * 57.29578f;
			}
			return result;
		}

		// Token: 0x06000D7A RID: 3450 RVA: 0x00016748 File Offset: 0x00014948
		public static float SignedAngle(Vector3 from, Vector3 to, Vector3 axis)
		{
			float num = Vector3.Angle(from, to);
			float num2 = Mathf.Sign(Vector3.Dot(axis, Vector3.Cross(from, to)));
			return num * num2;
		}

		// Token: 0x06000D7B RID: 3451 RVA: 0x0001677C File Offset: 0x0001497C
		public static float Distance(Vector3 a, Vector3 b)
		{
			Vector3 vector = new Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
			return Mathf.Sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
		}

		// Token: 0x06000D7C RID: 3452 RVA: 0x000167FC File Offset: 0x000149FC
		public static Vector3 ClampMagnitude(Vector3 vector, float maxLength)
		{
			Vector3 result;
			if (vector.sqrMagnitude > maxLength * maxLength)
			{
				result = vector.normalized * maxLength;
			}
			else
			{
				result = vector;
			}
			return result;
		}

		// Token: 0x06000D7D RID: 3453 RVA: 0x00016834 File Offset: 0x00014A34
		public static float Magnitude(Vector3 vector)
		{
			return Mathf.Sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
		}

		// Token: 0x170002AA RID: 682
		// (get) Token: 0x06000D7E RID: 3454 RVA: 0x00016880 File Offset: 0x00014A80
		public float magnitude
		{
			get
			{
				return Mathf.Sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
			}
		}

		// Token: 0x06000D7F RID: 3455 RVA: 0x000168C4 File Offset: 0x00014AC4
		public static float SqrMagnitude(Vector3 vector)
		{
			return vector.x * vector.x + vector.y * vector.y + vector.z * vector.z;
		}

		// Token: 0x170002AB RID: 683
		// (get) Token: 0x06000D80 RID: 3456 RVA: 0x00016908 File Offset: 0x00014B08
		public float sqrMagnitude
		{
			get
			{
				return this.x * this.x + this.y * this.y + this.z * this.z;
			}
		}

		// Token: 0x06000D81 RID: 3457 RVA: 0x00016948 File Offset: 0x00014B48
		public static Vector3 Min(Vector3 lhs, Vector3 rhs)
		{
			return new Vector3(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y), Mathf.Min(lhs.z, rhs.z));
		}

		// Token: 0x06000D82 RID: 3458 RVA: 0x0001699C File Offset: 0x00014B9C
		public static Vector3 Max(Vector3 lhs, Vector3 rhs)
		{
			return new Vector3(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y), Mathf.Max(lhs.z, rhs.z));
		}

		// Token: 0x170002AC RID: 684
		// (get) Token: 0x06000D83 RID: 3459 RVA: 0x000169F0 File Offset: 0x00014BF0
		public static Vector3 zero
		{
			get
			{
				return Vector3.zeroVector;
			}
		}

		// Token: 0x170002AD RID: 685
		// (get) Token: 0x06000D84 RID: 3460 RVA: 0x00016A0C File Offset: 0x00014C0C
		public static Vector3 one
		{
			get
			{
				return Vector3.oneVector;
			}
		}

		// Token: 0x170002AE RID: 686
		// (get) Token: 0x06000D85 RID: 3461 RVA: 0x00016A28 File Offset: 0x00014C28
		public static Vector3 forward
		{
			get
			{
				return Vector3.forwardVector;
			}
		}

		// Token: 0x170002AF RID: 687
		// (get) Token: 0x06000D86 RID: 3462 RVA: 0x00016A44 File Offset: 0x00014C44
		public static Vector3 back
		{
			get
			{
				return Vector3.backVector;
			}
		}

		// Token: 0x170002B0 RID: 688
		// (get) Token: 0x06000D87 RID: 3463 RVA: 0x00016A60 File Offset: 0x00014C60
		public static Vector3 up
		{
			get
			{
				return Vector3.upVector;
			}
		}

		// Token: 0x170002B1 RID: 689
		// (get) Token: 0x06000D88 RID: 3464 RVA: 0x00016A7C File Offset: 0x00014C7C
		public static Vector3 down
		{
			get
			{
				return Vector3.downVector;
			}
		}

		// Token: 0x170002B2 RID: 690
		// (get) Token: 0x06000D89 RID: 3465 RVA: 0x00016A98 File Offset: 0x00014C98
		public static Vector3 left
		{
			get
			{
				return Vector3.leftVector;
			}
		}

		// Token: 0x170002B3 RID: 691
		// (get) Token: 0x06000D8A RID: 3466 RVA: 0x00016AB4 File Offset: 0x00014CB4
		public static Vector3 right
		{
			get
			{
				return Vector3.rightVector;
			}
		}

		// Token: 0x170002B4 RID: 692
		// (get) Token: 0x06000D8B RID: 3467 RVA: 0x00016AD0 File Offset: 0x00014CD0
		public static Vector3 positiveInfinity
		{
			get
			{
				return Vector3.positiveInfinityVector;
			}
		}

		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x06000D8C RID: 3468 RVA: 0x00016AEC File Offset: 0x00014CEC
		public static Vector3 negativeInfinity
		{
			get
			{
				return Vector3.negativeInfinityVector;
			}
		}

		// Token: 0x06000D8D RID: 3469 RVA: 0x00016B08 File Offset: 0x00014D08
		public static Vector3 operator +(Vector3 a, Vector3 b)
		{
			return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
		}

		// Token: 0x06000D8E RID: 3470 RVA: 0x00016B50 File Offset: 0x00014D50
		public static Vector3 operator -(Vector3 a, Vector3 b)
		{
			return new Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
		}

		// Token: 0x06000D8F RID: 3471 RVA: 0x00016B98 File Offset: 0x00014D98
		public static Vector3 operator -(Vector3 a)
		{
			return new Vector3(-a.x, -a.y, -a.z);
		}

		// Token: 0x06000D90 RID: 3472 RVA: 0x00016BCC File Offset: 0x00014DCC
		public static Vector3 operator *(Vector3 a, float d)
		{
			return new Vector3(a.x * d, a.y * d, a.z * d);
		}

		// Token: 0x06000D91 RID: 3473 RVA: 0x00016C04 File Offset: 0x00014E04
		public static Vector3 operator *(float d, Vector3 a)
		{
			return new Vector3(a.x * d, a.y * d, a.z * d);
		}

		// Token: 0x06000D92 RID: 3474 RVA: 0x00016C3C File Offset: 0x00014E3C
		public static Vector3 operator /(Vector3 a, float d)
		{
			return new Vector3(a.x / d, a.y / d, a.z / d);
		}

		// Token: 0x06000D93 RID: 3475 RVA: 0x00016C74 File Offset: 0x00014E74
		public static bool operator ==(Vector3 lhs, Vector3 rhs)
		{
			return Vector3.SqrMagnitude(lhs - rhs) < 9.9999994E-11f;
		}

		// Token: 0x06000D94 RID: 3476 RVA: 0x00016C9C File Offset: 0x00014E9C
		public static bool operator !=(Vector3 lhs, Vector3 rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x06000D95 RID: 3477 RVA: 0x00016CBC File Offset: 0x00014EBC
		public override string ToString()
		{
			return UnityString.Format("({0:F1}, {1:F1}, {2:F1})", new object[]
			{
				this.x,
				this.y,
				this.z
			});
		}

		// Token: 0x06000D96 RID: 3478 RVA: 0x00016D0C File Offset: 0x00014F0C
		public string ToString(string format)
		{
			return UnityString.Format("({0}, {1}, {2})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format),
				this.z.ToString(format)
			});
		}

		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x06000D97 RID: 3479 RVA: 0x00016D60 File Offset: 0x00014F60
		[Obsolete("Use Vector3.forward instead.")]
		public static Vector3 fwd
		{
			get
			{
				return new Vector3(0f, 0f, 1f);
			}
		}

		// Token: 0x06000D98 RID: 3480 RVA: 0x00016D8C File Offset: 0x00014F8C
		[Obsolete("Use Vector3.Angle instead. AngleBetween uses radians instead of degrees and was deprecated for this reason")]
		public static float AngleBetween(Vector3 from, Vector3 to)
		{
			return Mathf.Acos(Mathf.Clamp(Vector3.Dot(from.normalized, to.normalized), -1f, 1f));
		}

		// Token: 0x06000D99 RID: 3481 RVA: 0x00016DC8 File Offset: 0x00014FC8
		[Obsolete("Use Vector3.ProjectOnPlane instead.")]
		public static Vector3 Exclude(Vector3 excludeThis, Vector3 fromThat)
		{
			return Vector3.ProjectOnPlane(fromThat, excludeThis);
		}

		// Token: 0x06000D9A RID: 3482 RVA: 0x00016DE4 File Offset: 0x00014FE4
		// Note: this type is marked as 'beforefieldinit'.
		static Vector3()
		{
		}

		// Token: 0x06000D9B RID: 3483
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Slerp_Injected(ref Vector3 a, ref Vector3 b, float t, out Vector3 ret);

		// Token: 0x06000D9C RID: 3484
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SlerpUnclamped_Injected(ref Vector3 a, ref Vector3 b, float t, out Vector3 ret);

		// Token: 0x06000D9D RID: 3485
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void RotateTowards_Injected(ref Vector3 current, ref Vector3 target, float maxRadiansDelta, float maxMagnitudeDelta, out Vector3 ret);

		// Token: 0x040006D1 RID: 1745
		public const float kEpsilon = 1E-05f;

		// Token: 0x040006D2 RID: 1746
		public const float kEpsilonNormalSqrt = 1E-15f;

		// Token: 0x040006D3 RID: 1747
		public float x;

		// Token: 0x040006D4 RID: 1748
		public float y;

		// Token: 0x040006D5 RID: 1749
		public float z;

		// Token: 0x040006D6 RID: 1750
		private static readonly Vector3 zeroVector = new Vector3(0f, 0f, 0f);

		// Token: 0x040006D7 RID: 1751
		private static readonly Vector3 oneVector = new Vector3(1f, 1f, 1f);

		// Token: 0x040006D8 RID: 1752
		private static readonly Vector3 upVector = new Vector3(0f, 1f, 0f);

		// Token: 0x040006D9 RID: 1753
		private static readonly Vector3 downVector = new Vector3(0f, -1f, 0f);

		// Token: 0x040006DA RID: 1754
		private static readonly Vector3 leftVector = new Vector3(-1f, 0f, 0f);

		// Token: 0x040006DB RID: 1755
		private static readonly Vector3 rightVector = new Vector3(1f, 0f, 0f);

		// Token: 0x040006DC RID: 1756
		private static readonly Vector3 forwardVector = new Vector3(0f, 0f, 1f);

		// Token: 0x040006DD RID: 1757
		private static readonly Vector3 backVector = new Vector3(0f, 0f, -1f);

		// Token: 0x040006DE RID: 1758
		private static readonly Vector3 positiveInfinityVector = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);

		// Token: 0x040006DF RID: 1759
		private static readonly Vector3 negativeInfinityVector = new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);
	}
}
