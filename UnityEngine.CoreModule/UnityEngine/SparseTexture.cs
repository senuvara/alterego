﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;

namespace UnityEngine
{
	// Token: 0x0200022D RID: 557
	[NativeHeader("Runtime/Graphics/SparseTexture.h")]
	public sealed class SparseTexture : Texture
	{
		// Token: 0x0600139D RID: 5021 RVA: 0x00021F9C File Offset: 0x0002019C
		public SparseTexture(int width, int height, GraphicsFormat format, int mipCount)
		{
			if (base.ValidateFormat(format, FormatUsage.Sample))
			{
				SparseTexture.Internal_Create(this, width, height, GraphicsFormatUtility.GetTextureFormat(format), GraphicsFormatUtility.IsSRGBFormat(format), mipCount);
			}
		}

		// Token: 0x0600139E RID: 5022 RVA: 0x00021FC8 File Offset: 0x000201C8
		public SparseTexture(int width, int height, TextureFormat format, int mipCount)
		{
			if (base.ValidateFormat(format))
			{
				SparseTexture.Internal_Create(this, width, height, format, false, mipCount);
			}
		}

		// Token: 0x0600139F RID: 5023 RVA: 0x00021FE9 File Offset: 0x000201E9
		public SparseTexture(int width, int height, TextureFormat format, int mipCount, bool linear)
		{
			if (base.ValidateFormat(format))
			{
				SparseTexture.Internal_Create(this, width, height, format, linear, mipCount);
			}
		}

		// Token: 0x170003BA RID: 954
		// (get) Token: 0x060013A0 RID: 5024
		public extern int tileWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003BB RID: 955
		// (get) Token: 0x060013A1 RID: 5025
		public extern int tileHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003BC RID: 956
		// (get) Token: 0x060013A2 RID: 5026
		public extern bool isCreated { [NativeName("IsInitialized")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060013A3 RID: 5027
		[FreeFunction(Name = "SparseTextureScripting::Create", ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] SparseTexture mono, int width, int height, TextureFormat format, bool linear, int mipCount);

		// Token: 0x060013A4 RID: 5028
		[FreeFunction(Name = "SparseTextureScripting::UpdateTile", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UpdateTile(int tileX, int tileY, int miplevel, Color32[] data);

		// Token: 0x060013A5 RID: 5029
		[FreeFunction(Name = "SparseTextureScripting::UpdateTileRaw", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UpdateTileRaw(int tileX, int tileY, int miplevel, byte[] data);

		// Token: 0x060013A6 RID: 5030 RVA: 0x0002200B File Offset: 0x0002020B
		public void UnloadTile(int tileX, int tileY, int miplevel)
		{
			this.UpdateTileRaw(tileX, tileY, miplevel, null);
		}
	}
}
