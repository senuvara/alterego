﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000032 RID: 50
	[AttributeUsage(AttributeTargets.Assembly)]
	[RequiredByNativeCode]
	public class AssemblyIsEditorAssembly : Attribute
	{
		// Token: 0x06000293 RID: 659 RVA: 0x0000898B File Offset: 0x00006B8B
		public AssemblyIsEditorAssembly()
		{
		}
	}
}
