﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200014B RID: 331
	[NativeType(Header = "Runtime/Math/Matrix4x4.h")]
	[ThreadAndSerializationSafe]
	[NativeHeader("Runtime/Math/MathScripting.h")]
	[NativeClass("Matrix4x4f")]
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	public struct Matrix4x4 : IEquatable<Matrix4x4>
	{
		// Token: 0x06000D1A RID: 3354 RVA: 0x0001463C File Offset: 0x0001283C
		public Matrix4x4(Vector4 column0, Vector4 column1, Vector4 column2, Vector4 column3)
		{
			this.m00 = column0.x;
			this.m01 = column1.x;
			this.m02 = column2.x;
			this.m03 = column3.x;
			this.m10 = column0.y;
			this.m11 = column1.y;
			this.m12 = column2.y;
			this.m13 = column3.y;
			this.m20 = column0.z;
			this.m21 = column1.z;
			this.m22 = column2.z;
			this.m23 = column3.z;
			this.m30 = column0.w;
			this.m31 = column1.w;
			this.m32 = column2.w;
			this.m33 = column3.w;
		}

		// Token: 0x06000D1B RID: 3355 RVA: 0x0001471C File Offset: 0x0001291C
		[ThreadSafe]
		private Quaternion GetRotation()
		{
			Quaternion result;
			Matrix4x4.GetRotation_Injected(ref this, out result);
			return result;
		}

		// Token: 0x06000D1C RID: 3356 RVA: 0x00014734 File Offset: 0x00012934
		[ThreadSafe]
		private Vector3 GetLossyScale()
		{
			Vector3 result;
			Matrix4x4.GetLossyScale_Injected(ref this, out result);
			return result;
		}

		// Token: 0x06000D1D RID: 3357 RVA: 0x0001474A File Offset: 0x0001294A
		[ThreadSafe]
		private bool IsIdentity()
		{
			return Matrix4x4.IsIdentity_Injected(ref this);
		}

		// Token: 0x06000D1E RID: 3358 RVA: 0x00014752 File Offset: 0x00012952
		[ThreadSafe]
		private float GetDeterminant()
		{
			return Matrix4x4.GetDeterminant_Injected(ref this);
		}

		// Token: 0x06000D1F RID: 3359 RVA: 0x0001475C File Offset: 0x0001295C
		[ThreadSafe]
		private FrustumPlanes DecomposeProjection()
		{
			FrustumPlanes result;
			Matrix4x4.DecomposeProjection_Injected(ref this, out result);
			return result;
		}

		// Token: 0x1700029D RID: 669
		// (get) Token: 0x06000D20 RID: 3360 RVA: 0x00014774 File Offset: 0x00012974
		public Quaternion rotation
		{
			get
			{
				return this.GetRotation();
			}
		}

		// Token: 0x1700029E RID: 670
		// (get) Token: 0x06000D21 RID: 3361 RVA: 0x00014790 File Offset: 0x00012990
		public Vector3 lossyScale
		{
			get
			{
				return this.GetLossyScale();
			}
		}

		// Token: 0x1700029F RID: 671
		// (get) Token: 0x06000D22 RID: 3362 RVA: 0x000147AC File Offset: 0x000129AC
		public bool isIdentity
		{
			get
			{
				return this.IsIdentity();
			}
		}

		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x06000D23 RID: 3363 RVA: 0x000147C8 File Offset: 0x000129C8
		public float determinant
		{
			get
			{
				return this.GetDeterminant();
			}
		}

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000D24 RID: 3364 RVA: 0x000147E4 File Offset: 0x000129E4
		public FrustumPlanes decomposeProjection
		{
			get
			{
				return this.DecomposeProjection();
			}
		}

		// Token: 0x06000D25 RID: 3365 RVA: 0x000147FF File Offset: 0x000129FF
		[ThreadSafe]
		public bool ValidTRS()
		{
			return Matrix4x4.ValidTRS_Injected(ref this);
		}

		// Token: 0x06000D26 RID: 3366 RVA: 0x00014808 File Offset: 0x00012A08
		public static float Determinant(Matrix4x4 m)
		{
			return m.determinant;
		}

		// Token: 0x06000D27 RID: 3367 RVA: 0x00014824 File Offset: 0x00012A24
		[FreeFunction("MatrixScripting::TRS", IsThreadSafe = true)]
		public static Matrix4x4 TRS(Vector3 pos, Quaternion q, Vector3 s)
		{
			Matrix4x4 result;
			Matrix4x4.TRS_Injected(ref pos, ref q, ref s, out result);
			return result;
		}

		// Token: 0x06000D28 RID: 3368 RVA: 0x0001483F File Offset: 0x00012A3F
		public void SetTRS(Vector3 pos, Quaternion q, Vector3 s)
		{
			this = Matrix4x4.TRS(pos, q, s);
		}

		// Token: 0x06000D29 RID: 3369 RVA: 0x00014850 File Offset: 0x00012A50
		[FreeFunction("MatrixScripting::Inverse", IsThreadSafe = true)]
		public static Matrix4x4 Inverse(Matrix4x4 m)
		{
			Matrix4x4 result;
			Matrix4x4.Inverse_Injected(ref m, out result);
			return result;
		}

		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000D2A RID: 3370 RVA: 0x00014868 File Offset: 0x00012A68
		public Matrix4x4 inverse
		{
			get
			{
				return Matrix4x4.Inverse(this);
			}
		}

		// Token: 0x06000D2B RID: 3371 RVA: 0x00014888 File Offset: 0x00012A88
		[FreeFunction("MatrixScripting::Transpose", IsThreadSafe = true)]
		public static Matrix4x4 Transpose(Matrix4x4 m)
		{
			Matrix4x4 result;
			Matrix4x4.Transpose_Injected(ref m, out result);
			return result;
		}

		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06000D2C RID: 3372 RVA: 0x000148A0 File Offset: 0x00012AA0
		public Matrix4x4 transpose
		{
			get
			{
				return Matrix4x4.Transpose(this);
			}
		}

		// Token: 0x06000D2D RID: 3373 RVA: 0x000148C0 File Offset: 0x00012AC0
		[FreeFunction("MatrixScripting::Ortho", IsThreadSafe = true)]
		public static Matrix4x4 Ortho(float left, float right, float bottom, float top, float zNear, float zFar)
		{
			Matrix4x4 result;
			Matrix4x4.Ortho_Injected(left, right, bottom, top, zNear, zFar, out result);
			return result;
		}

		// Token: 0x06000D2E RID: 3374 RVA: 0x000148E0 File Offset: 0x00012AE0
		[FreeFunction("MatrixScripting::Perspective", IsThreadSafe = true)]
		public static Matrix4x4 Perspective(float fov, float aspect, float zNear, float zFar)
		{
			Matrix4x4 result;
			Matrix4x4.Perspective_Injected(fov, aspect, zNear, zFar, out result);
			return result;
		}

		// Token: 0x06000D2F RID: 3375 RVA: 0x000148FC File Offset: 0x00012AFC
		[FreeFunction("MatrixScripting::LookAt", IsThreadSafe = true)]
		public static Matrix4x4 LookAt(Vector3 from, Vector3 to, Vector3 up)
		{
			Matrix4x4 result;
			Matrix4x4.LookAt_Injected(ref from, ref to, ref up, out result);
			return result;
		}

		// Token: 0x06000D30 RID: 3376 RVA: 0x00014918 File Offset: 0x00012B18
		[FreeFunction("MatrixScripting::Frustum", IsThreadSafe = true)]
		public static Matrix4x4 Frustum(float left, float right, float bottom, float top, float zNear, float zFar)
		{
			Matrix4x4 result;
			Matrix4x4.Frustum_Injected(left, right, bottom, top, zNear, zFar, out result);
			return result;
		}

		// Token: 0x06000D31 RID: 3377 RVA: 0x00014938 File Offset: 0x00012B38
		public static Matrix4x4 Frustum(FrustumPlanes fp)
		{
			return Matrix4x4.Frustum(fp.left, fp.right, fp.bottom, fp.top, fp.zNear, fp.zFar);
		}

		// Token: 0x170002A4 RID: 676
		public float this[int row, int column]
		{
			get
			{
				return this[row + column * 4];
			}
			set
			{
				this[row + column * 4] = value;
			}
		}

		// Token: 0x170002A5 RID: 677
		public float this[int index]
		{
			get
			{
				float result;
				switch (index)
				{
				case 0:
					result = this.m00;
					break;
				case 1:
					result = this.m10;
					break;
				case 2:
					result = this.m20;
					break;
				case 3:
					result = this.m30;
					break;
				case 4:
					result = this.m01;
					break;
				case 5:
					result = this.m11;
					break;
				case 6:
					result = this.m21;
					break;
				case 7:
					result = this.m31;
					break;
				case 8:
					result = this.m02;
					break;
				case 9:
					result = this.m12;
					break;
				case 10:
					result = this.m22;
					break;
				case 11:
					result = this.m32;
					break;
				case 12:
					result = this.m03;
					break;
				case 13:
					result = this.m13;
					break;
				case 14:
					result = this.m23;
					break;
				case 15:
					result = this.m33;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid matrix index!");
				}
				return result;
			}
			set
			{
				switch (index)
				{
				case 0:
					this.m00 = value;
					break;
				case 1:
					this.m10 = value;
					break;
				case 2:
					this.m20 = value;
					break;
				case 3:
					this.m30 = value;
					break;
				case 4:
					this.m01 = value;
					break;
				case 5:
					this.m11 = value;
					break;
				case 6:
					this.m21 = value;
					break;
				case 7:
					this.m31 = value;
					break;
				case 8:
					this.m02 = value;
					break;
				case 9:
					this.m12 = value;
					break;
				case 10:
					this.m22 = value;
					break;
				case 11:
					this.m32 = value;
					break;
				case 12:
					this.m03 = value;
					break;
				case 13:
					this.m13 = value;
					break;
				case 14:
					this.m23 = value;
					break;
				case 15:
					this.m33 = value;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid matrix index!");
				}
			}
		}

		// Token: 0x06000D36 RID: 3382 RVA: 0x00014BF8 File Offset: 0x00012DF8
		public override int GetHashCode()
		{
			return this.GetColumn(0).GetHashCode() ^ this.GetColumn(1).GetHashCode() << 2 ^ this.GetColumn(2).GetHashCode() >> 2 ^ this.GetColumn(3).GetHashCode() >> 1;
		}

		// Token: 0x06000D37 RID: 3383 RVA: 0x00014C6C File Offset: 0x00012E6C
		public override bool Equals(object other)
		{
			return other is Matrix4x4 && this.Equals((Matrix4x4)other);
		}

		// Token: 0x06000D38 RID: 3384 RVA: 0x00014CA0 File Offset: 0x00012EA0
		public bool Equals(Matrix4x4 other)
		{
			return this.GetColumn(0).Equals(other.GetColumn(0)) && this.GetColumn(1).Equals(other.GetColumn(1)) && this.GetColumn(2).Equals(other.GetColumn(2)) && this.GetColumn(3).Equals(other.GetColumn(3));
		}

		// Token: 0x06000D39 RID: 3385 RVA: 0x00014D28 File Offset: 0x00012F28
		public static Matrix4x4 operator *(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			Matrix4x4 result;
			result.m00 = lhs.m00 * rhs.m00 + lhs.m01 * rhs.m10 + lhs.m02 * rhs.m20 + lhs.m03 * rhs.m30;
			result.m01 = lhs.m00 * rhs.m01 + lhs.m01 * rhs.m11 + lhs.m02 * rhs.m21 + lhs.m03 * rhs.m31;
			result.m02 = lhs.m00 * rhs.m02 + lhs.m01 * rhs.m12 + lhs.m02 * rhs.m22 + lhs.m03 * rhs.m32;
			result.m03 = lhs.m00 * rhs.m03 + lhs.m01 * rhs.m13 + lhs.m02 * rhs.m23 + lhs.m03 * rhs.m33;
			result.m10 = lhs.m10 * rhs.m00 + lhs.m11 * rhs.m10 + lhs.m12 * rhs.m20 + lhs.m13 * rhs.m30;
			result.m11 = lhs.m10 * rhs.m01 + lhs.m11 * rhs.m11 + lhs.m12 * rhs.m21 + lhs.m13 * rhs.m31;
			result.m12 = lhs.m10 * rhs.m02 + lhs.m11 * rhs.m12 + lhs.m12 * rhs.m22 + lhs.m13 * rhs.m32;
			result.m13 = lhs.m10 * rhs.m03 + lhs.m11 * rhs.m13 + lhs.m12 * rhs.m23 + lhs.m13 * rhs.m33;
			result.m20 = lhs.m20 * rhs.m00 + lhs.m21 * rhs.m10 + lhs.m22 * rhs.m20 + lhs.m23 * rhs.m30;
			result.m21 = lhs.m20 * rhs.m01 + lhs.m21 * rhs.m11 + lhs.m22 * rhs.m21 + lhs.m23 * rhs.m31;
			result.m22 = lhs.m20 * rhs.m02 + lhs.m21 * rhs.m12 + lhs.m22 * rhs.m22 + lhs.m23 * rhs.m32;
			result.m23 = lhs.m20 * rhs.m03 + lhs.m21 * rhs.m13 + lhs.m22 * rhs.m23 + lhs.m23 * rhs.m33;
			result.m30 = lhs.m30 * rhs.m00 + lhs.m31 * rhs.m10 + lhs.m32 * rhs.m20 + lhs.m33 * rhs.m30;
			result.m31 = lhs.m30 * rhs.m01 + lhs.m31 * rhs.m11 + lhs.m32 * rhs.m21 + lhs.m33 * rhs.m31;
			result.m32 = lhs.m30 * rhs.m02 + lhs.m31 * rhs.m12 + lhs.m32 * rhs.m22 + lhs.m33 * rhs.m32;
			result.m33 = lhs.m30 * rhs.m03 + lhs.m31 * rhs.m13 + lhs.m32 * rhs.m23 + lhs.m33 * rhs.m33;
			return result;
		}

		// Token: 0x06000D3A RID: 3386 RVA: 0x000151A0 File Offset: 0x000133A0
		public static Vector4 operator *(Matrix4x4 lhs, Vector4 vector)
		{
			Vector4 result;
			result.x = lhs.m00 * vector.x + lhs.m01 * vector.y + lhs.m02 * vector.z + lhs.m03 * vector.w;
			result.y = lhs.m10 * vector.x + lhs.m11 * vector.y + lhs.m12 * vector.z + lhs.m13 * vector.w;
			result.z = lhs.m20 * vector.x + lhs.m21 * vector.y + lhs.m22 * vector.z + lhs.m23 * vector.w;
			result.w = lhs.m30 * vector.x + lhs.m31 * vector.y + lhs.m32 * vector.z + lhs.m33 * vector.w;
			return result;
		}

		// Token: 0x06000D3B RID: 3387 RVA: 0x000152D0 File Offset: 0x000134D0
		public static bool operator ==(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			return lhs.GetColumn(0) == rhs.GetColumn(0) && lhs.GetColumn(1) == rhs.GetColumn(1) && lhs.GetColumn(2) == rhs.GetColumn(2) && lhs.GetColumn(3) == rhs.GetColumn(3);
		}

		// Token: 0x06000D3C RID: 3388 RVA: 0x0001534C File Offset: 0x0001354C
		public static bool operator !=(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x06000D3D RID: 3389 RVA: 0x0001536C File Offset: 0x0001356C
		public Vector4 GetColumn(int index)
		{
			Vector4 result;
			switch (index)
			{
			case 0:
				result = new Vector4(this.m00, this.m10, this.m20, this.m30);
				break;
			case 1:
				result = new Vector4(this.m01, this.m11, this.m21, this.m31);
				break;
			case 2:
				result = new Vector4(this.m02, this.m12, this.m22, this.m32);
				break;
			case 3:
				result = new Vector4(this.m03, this.m13, this.m23, this.m33);
				break;
			default:
				throw new IndexOutOfRangeException("Invalid column index!");
			}
			return result;
		}

		// Token: 0x06000D3E RID: 3390 RVA: 0x00015430 File Offset: 0x00013630
		public Vector4 GetRow(int index)
		{
			Vector4 result;
			switch (index)
			{
			case 0:
				result = new Vector4(this.m00, this.m01, this.m02, this.m03);
				break;
			case 1:
				result = new Vector4(this.m10, this.m11, this.m12, this.m13);
				break;
			case 2:
				result = new Vector4(this.m20, this.m21, this.m22, this.m23);
				break;
			case 3:
				result = new Vector4(this.m30, this.m31, this.m32, this.m33);
				break;
			default:
				throw new IndexOutOfRangeException("Invalid row index!");
			}
			return result;
		}

		// Token: 0x06000D3F RID: 3391 RVA: 0x000154F1 File Offset: 0x000136F1
		public void SetColumn(int index, Vector4 column)
		{
			this[0, index] = column.x;
			this[1, index] = column.y;
			this[2, index] = column.z;
			this[3, index] = column.w;
		}

		// Token: 0x06000D40 RID: 3392 RVA: 0x00015530 File Offset: 0x00013730
		public void SetRow(int index, Vector4 row)
		{
			this[index, 0] = row.x;
			this[index, 1] = row.y;
			this[index, 2] = row.z;
			this[index, 3] = row.w;
		}

		// Token: 0x06000D41 RID: 3393 RVA: 0x00015570 File Offset: 0x00013770
		public Vector3 MultiplyPoint(Vector3 point)
		{
			Vector3 result;
			result.x = this.m00 * point.x + this.m01 * point.y + this.m02 * point.z + this.m03;
			result.y = this.m10 * point.x + this.m11 * point.y + this.m12 * point.z + this.m13;
			result.z = this.m20 * point.x + this.m21 * point.y + this.m22 * point.z + this.m23;
			float num = this.m30 * point.x + this.m31 * point.y + this.m32 * point.z + this.m33;
			num = 1f / num;
			result.x *= num;
			result.y *= num;
			result.z *= num;
			return result;
		}

		// Token: 0x06000D42 RID: 3394 RVA: 0x000156A0 File Offset: 0x000138A0
		public Vector3 MultiplyPoint3x4(Vector3 point)
		{
			Vector3 result;
			result.x = this.m00 * point.x + this.m01 * point.y + this.m02 * point.z + this.m03;
			result.y = this.m10 * point.x + this.m11 * point.y + this.m12 * point.z + this.m13;
			result.z = this.m20 * point.x + this.m21 * point.y + this.m22 * point.z + this.m23;
			return result;
		}

		// Token: 0x06000D43 RID: 3395 RVA: 0x00015764 File Offset: 0x00013964
		public Vector3 MultiplyVector(Vector3 vector)
		{
			Vector3 result;
			result.x = this.m00 * vector.x + this.m01 * vector.y + this.m02 * vector.z;
			result.y = this.m10 * vector.x + this.m11 * vector.y + this.m12 * vector.z;
			result.z = this.m20 * vector.x + this.m21 * vector.y + this.m22 * vector.z;
			return result;
		}

		// Token: 0x06000D44 RID: 3396 RVA: 0x00015814 File Offset: 0x00013A14
		public Plane TransformPlane(Plane plane)
		{
			Matrix4x4 inverse = this.inverse;
			float x = plane.normal.x;
			float y = plane.normal.y;
			float z = plane.normal.z;
			float distance = plane.distance;
			float x2 = inverse.m00 * x + inverse.m10 * y + inverse.m20 * z + inverse.m30 * distance;
			float y2 = inverse.m01 * x + inverse.m11 * y + inverse.m21 * z + inverse.m31 * distance;
			float z2 = inverse.m02 * x + inverse.m12 * y + inverse.m22 * z + inverse.m32 * distance;
			float d = inverse.m03 * x + inverse.m13 * y + inverse.m23 * z + inverse.m33 * distance;
			return new Plane(new Vector3(x2, y2, z2), d);
		}

		// Token: 0x06000D45 RID: 3397 RVA: 0x0001592C File Offset: 0x00013B2C
		public static Matrix4x4 Scale(Vector3 vector)
		{
			Matrix4x4 result;
			result.m00 = vector.x;
			result.m01 = 0f;
			result.m02 = 0f;
			result.m03 = 0f;
			result.m10 = 0f;
			result.m11 = vector.y;
			result.m12 = 0f;
			result.m13 = 0f;
			result.m20 = 0f;
			result.m21 = 0f;
			result.m22 = vector.z;
			result.m23 = 0f;
			result.m30 = 0f;
			result.m31 = 0f;
			result.m32 = 0f;
			result.m33 = 1f;
			return result;
		}

		// Token: 0x06000D46 RID: 3398 RVA: 0x00015A08 File Offset: 0x00013C08
		public static Matrix4x4 Translate(Vector3 vector)
		{
			Matrix4x4 result;
			result.m00 = 1f;
			result.m01 = 0f;
			result.m02 = 0f;
			result.m03 = vector.x;
			result.m10 = 0f;
			result.m11 = 1f;
			result.m12 = 0f;
			result.m13 = vector.y;
			result.m20 = 0f;
			result.m21 = 0f;
			result.m22 = 1f;
			result.m23 = vector.z;
			result.m30 = 0f;
			result.m31 = 0f;
			result.m32 = 0f;
			result.m33 = 1f;
			return result;
		}

		// Token: 0x06000D47 RID: 3399 RVA: 0x00015AE4 File Offset: 0x00013CE4
		public static Matrix4x4 Rotate(Quaternion q)
		{
			float num = q.x * 2f;
			float num2 = q.y * 2f;
			float num3 = q.z * 2f;
			float num4 = q.x * num;
			float num5 = q.y * num2;
			float num6 = q.z * num3;
			float num7 = q.x * num2;
			float num8 = q.x * num3;
			float num9 = q.y * num3;
			float num10 = q.w * num;
			float num11 = q.w * num2;
			float num12 = q.w * num3;
			Matrix4x4 result;
			result.m00 = 1f - (num5 + num6);
			result.m10 = num7 + num12;
			result.m20 = num8 - num11;
			result.m30 = 0f;
			result.m01 = num7 - num12;
			result.m11 = 1f - (num4 + num6);
			result.m21 = num9 + num10;
			result.m31 = 0f;
			result.m02 = num8 + num11;
			result.m12 = num9 - num10;
			result.m22 = 1f - (num4 + num5);
			result.m32 = 0f;
			result.m03 = 0f;
			result.m13 = 0f;
			result.m23 = 0f;
			result.m33 = 1f;
			return result;
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000D48 RID: 3400 RVA: 0x00015C5C File Offset: 0x00013E5C
		public static Matrix4x4 zero
		{
			get
			{
				return Matrix4x4.zeroMatrix;
			}
		}

		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x06000D49 RID: 3401 RVA: 0x00015C78 File Offset: 0x00013E78
		public static Matrix4x4 identity
		{
			get
			{
				return Matrix4x4.identityMatrix;
			}
		}

		// Token: 0x06000D4A RID: 3402 RVA: 0x00015C94 File Offset: 0x00013E94
		public override string ToString()
		{
			return UnityString.Format("{0:F5}\t{1:F5}\t{2:F5}\t{3:F5}\n{4:F5}\t{5:F5}\t{6:F5}\t{7:F5}\n{8:F5}\t{9:F5}\t{10:F5}\t{11:F5}\n{12:F5}\t{13:F5}\t{14:F5}\t{15:F5}\n", new object[]
			{
				this.m00,
				this.m01,
				this.m02,
				this.m03,
				this.m10,
				this.m11,
				this.m12,
				this.m13,
				this.m20,
				this.m21,
				this.m22,
				this.m23,
				this.m30,
				this.m31,
				this.m32,
				this.m33
			});
		}

		// Token: 0x06000D4B RID: 3403 RVA: 0x00015DA4 File Offset: 0x00013FA4
		public string ToString(string format)
		{
			return UnityString.Format("{0}\t{1}\t{2}\t{3}\n{4}\t{5}\t{6}\t{7}\n{8}\t{9}\t{10}\t{11}\n{12}\t{13}\t{14}\t{15}\n", new object[]
			{
				this.m00.ToString(format),
				this.m01.ToString(format),
				this.m02.ToString(format),
				this.m03.ToString(format),
				this.m10.ToString(format),
				this.m11.ToString(format),
				this.m12.ToString(format),
				this.m13.ToString(format),
				this.m20.ToString(format),
				this.m21.ToString(format),
				this.m22.ToString(format),
				this.m23.ToString(format),
				this.m30.ToString(format),
				this.m31.ToString(format),
				this.m32.ToString(format),
				this.m33.ToString(format)
			});
		}

		// Token: 0x06000D4C RID: 3404 RVA: 0x00015EC4 File Offset: 0x000140C4
		// Note: this type is marked as 'beforefieldinit'.
		static Matrix4x4()
		{
		}

		// Token: 0x06000D4D RID: 3405
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRotation_Injected(ref Matrix4x4 _unity_self, out Quaternion ret);

		// Token: 0x06000D4E RID: 3406
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLossyScale_Injected(ref Matrix4x4 _unity_self, out Vector3 ret);

		// Token: 0x06000D4F RID: 3407
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsIdentity_Injected(ref Matrix4x4 _unity_self);

		// Token: 0x06000D50 RID: 3408
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetDeterminant_Injected(ref Matrix4x4 _unity_self);

		// Token: 0x06000D51 RID: 3409
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DecomposeProjection_Injected(ref Matrix4x4 _unity_self, out FrustumPlanes ret);

		// Token: 0x06000D52 RID: 3410
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool ValidTRS_Injected(ref Matrix4x4 _unity_self);

		// Token: 0x06000D53 RID: 3411
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void TRS_Injected(ref Vector3 pos, ref Quaternion q, ref Vector3 s, out Matrix4x4 ret);

		// Token: 0x06000D54 RID: 3412
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Inverse_Injected(ref Matrix4x4 m, out Matrix4x4 ret);

		// Token: 0x06000D55 RID: 3413
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Transpose_Injected(ref Matrix4x4 m, out Matrix4x4 ret);

		// Token: 0x06000D56 RID: 3414
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Ortho_Injected(float left, float right, float bottom, float top, float zNear, float zFar, out Matrix4x4 ret);

		// Token: 0x06000D57 RID: 3415
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Perspective_Injected(float fov, float aspect, float zNear, float zFar, out Matrix4x4 ret);

		// Token: 0x06000D58 RID: 3416
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void LookAt_Injected(ref Vector3 from, ref Vector3 to, ref Vector3 up, out Matrix4x4 ret);

		// Token: 0x06000D59 RID: 3417
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Frustum_Injected(float left, float right, float bottom, float top, float zNear, float zFar, out Matrix4x4 ret);

		// Token: 0x040006BF RID: 1727
		[NativeName("m_Data[0]")]
		public float m00;

		// Token: 0x040006C0 RID: 1728
		[NativeName("m_Data[1]")]
		public float m10;

		// Token: 0x040006C1 RID: 1729
		[NativeName("m_Data[2]")]
		public float m20;

		// Token: 0x040006C2 RID: 1730
		[NativeName("m_Data[3]")]
		public float m30;

		// Token: 0x040006C3 RID: 1731
		[NativeName("m_Data[4]")]
		public float m01;

		// Token: 0x040006C4 RID: 1732
		[NativeName("m_Data[5]")]
		public float m11;

		// Token: 0x040006C5 RID: 1733
		[NativeName("m_Data[6]")]
		public float m21;

		// Token: 0x040006C6 RID: 1734
		[NativeName("m_Data[7]")]
		public float m31;

		// Token: 0x040006C7 RID: 1735
		[NativeName("m_Data[8]")]
		public float m02;

		// Token: 0x040006C8 RID: 1736
		[NativeName("m_Data[9]")]
		public float m12;

		// Token: 0x040006C9 RID: 1737
		[NativeName("m_Data[10]")]
		public float m22;

		// Token: 0x040006CA RID: 1738
		[NativeName("m_Data[11]")]
		public float m32;

		// Token: 0x040006CB RID: 1739
		[NativeName("m_Data[12]")]
		public float m03;

		// Token: 0x040006CC RID: 1740
		[NativeName("m_Data[13]")]
		public float m13;

		// Token: 0x040006CD RID: 1741
		[NativeName("m_Data[14]")]
		public float m23;

		// Token: 0x040006CE RID: 1742
		[NativeName("m_Data[15]")]
		public float m33;

		// Token: 0x040006CF RID: 1743
		private static readonly Matrix4x4 zeroMatrix = new Matrix4x4(new Vector4(0f, 0f, 0f, 0f), new Vector4(0f, 0f, 0f, 0f), new Vector4(0f, 0f, 0f, 0f), new Vector4(0f, 0f, 0f, 0f));

		// Token: 0x040006D0 RID: 1744
		private static readonly Matrix4x4 identityMatrix = new Matrix4x4(new Vector4(1f, 0f, 0f, 0f), new Vector4(0f, 1f, 0f, 0f), new Vector4(0f, 0f, 1f, 0f), new Vector4(0f, 0f, 0f, 1f));
	}
}
