﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020001F2 RID: 498
	[NativeHeader("Runtime/Utilities/PropertyName.h")]
	internal class PropertyNameUtils
	{
		// Token: 0x06000F8E RID: 3982 RVA: 0x00002370 File Offset: 0x00000570
		public PropertyNameUtils()
		{
		}

		// Token: 0x06000F8F RID: 3983 RVA: 0x0001AEF0 File Offset: 0x000190F0
		[FreeFunction]
		public static PropertyName PropertyNameFromString([Unmarshalled] string name)
		{
			PropertyName result;
			PropertyNameUtils.PropertyNameFromString_Injected(name, out result);
			return result;
		}

		// Token: 0x06000F90 RID: 3984
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void PropertyNameFromString_Injected(string name, out PropertyName ret);
	}
}
