﻿using System;

namespace UnityEngine
{
	// Token: 0x020000E0 RID: 224
	public enum CustomRenderTextureUpdateZoneSpace
	{
		// Token: 0x04000370 RID: 880
		Normalized,
		// Token: 0x04000371 RID: 881
		Pixel
	}
}
