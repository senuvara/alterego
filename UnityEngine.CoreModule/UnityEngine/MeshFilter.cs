﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020000AD RID: 173
	[NativeHeader("Runtime/Graphics/Mesh/MeshFilter.h")]
	[RequireComponent(typeof(Transform))]
	public sealed class MeshFilter : Component
	{
		// Token: 0x06000B16 RID: 2838 RVA: 0x00008E34 File Offset: 0x00007034
		public MeshFilter()
		{
		}

		// Token: 0x06000B17 RID: 2839 RVA: 0x00007476 File Offset: 0x00005676
		[RequiredByNativeCode]
		private void DontStripMeshFilter()
		{
		}

		// Token: 0x1700020C RID: 524
		// (get) Token: 0x06000B18 RID: 2840
		// (set) Token: 0x06000B19 RID: 2841
		public extern Mesh sharedMesh { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700020D RID: 525
		// (get) Token: 0x06000B1A RID: 2842
		// (set) Token: 0x06000B1B RID: 2843
		public extern Mesh mesh { [NativeName("GetInstantiatedMeshFromScript")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("SetInstantiatedMesh")] [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
