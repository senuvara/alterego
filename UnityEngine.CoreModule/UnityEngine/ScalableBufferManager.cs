﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000089 RID: 137
	[StaticAccessor("ScalableBufferManager::GetInstance()", StaticAccessorType.Dot)]
	[NativeHeader("Runtime/GfxDevice/ScalableBufferManager.h")]
	public static class ScalableBufferManager
	{
		// Token: 0x17000131 RID: 305
		// (get) Token: 0x06000775 RID: 1909
		public static extern float widthScaleFactor { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x06000776 RID: 1910
		public static extern float heightScaleFactor { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000777 RID: 1911
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ResizeBuffers(float widthScale, float heightScale);
	}
}
