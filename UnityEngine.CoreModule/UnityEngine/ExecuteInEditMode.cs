﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200002D RID: 45
	[UsedByNativeCode]
	public sealed class ExecuteInEditMode : Attribute
	{
		// Token: 0x0600028B RID: 651 RVA: 0x0000898B File Offset: 0x00006B8B
		public ExecuteInEditMode()
		{
		}
	}
}
