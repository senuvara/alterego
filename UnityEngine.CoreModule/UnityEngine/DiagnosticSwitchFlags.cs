﻿using System;

namespace UnityEngine
{
	// Token: 0x02000068 RID: 104
	[Flags]
	internal enum DiagnosticSwitchFlags
	{
		// Token: 0x04000135 RID: 309
		None = 0,
		// Token: 0x04000136 RID: 310
		CanChangeAfterEngineStart = 1
	}
}
