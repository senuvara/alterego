﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000047 RID: 71
	[StaticAccessor("CacheWrapper", StaticAccessorType.DoubleColon)]
	[NativeHeader("Runtime/Misc/Cache.h")]
	public struct Cache : IEquatable<Cache>
	{
		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000321 RID: 801 RVA: 0x00009E64 File Offset: 0x00008064
		internal int handle
		{
			get
			{
				return this.m_Handle;
			}
		}

		// Token: 0x06000322 RID: 802 RVA: 0x00009E80 File Offset: 0x00008080
		public static bool operator ==(Cache lhs, Cache rhs)
		{
			return lhs.handle == rhs.handle;
		}

		// Token: 0x06000323 RID: 803 RVA: 0x00009EA8 File Offset: 0x000080A8
		public static bool operator !=(Cache lhs, Cache rhs)
		{
			return lhs.handle != rhs.handle;
		}

		// Token: 0x06000324 RID: 804 RVA: 0x00009ED0 File Offset: 0x000080D0
		public override int GetHashCode()
		{
			return this.m_Handle;
		}

		// Token: 0x06000325 RID: 805 RVA: 0x00009EEC File Offset: 0x000080EC
		public override bool Equals(object other)
		{
			return other is Cache && this.Equals((Cache)other);
		}

		// Token: 0x06000326 RID: 806 RVA: 0x00009F1C File Offset: 0x0000811C
		public bool Equals(Cache other)
		{
			return this.handle == other.handle;
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000327 RID: 807 RVA: 0x00009F40 File Offset: 0x00008140
		public bool valid
		{
			get
			{
				return Cache.Cache_IsValid(this.m_Handle);
			}
		}

		// Token: 0x06000328 RID: 808
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool Cache_IsValid(int handle);

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000329 RID: 809 RVA: 0x00009F60 File Offset: 0x00008160
		public bool ready
		{
			get
			{
				return Cache.Cache_IsReady(this.m_Handle);
			}
		}

		// Token: 0x0600032A RID: 810
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool Cache_IsReady(int handle);

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x0600032B RID: 811 RVA: 0x00009F80 File Offset: 0x00008180
		public bool readOnly
		{
			get
			{
				return Cache.Cache_IsReadonly(this.m_Handle);
			}
		}

		// Token: 0x0600032C RID: 812
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool Cache_IsReadonly(int handle);

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x0600032D RID: 813 RVA: 0x00009FA0 File Offset: 0x000081A0
		public string path
		{
			get
			{
				return Cache.Cache_GetPath(this.m_Handle);
			}
		}

		// Token: 0x0600032E RID: 814
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string Cache_GetPath(int handle);

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x0600032F RID: 815 RVA: 0x00009FC0 File Offset: 0x000081C0
		public int index
		{
			get
			{
				return Cache.Cache_GetIndex(this.m_Handle);
			}
		}

		// Token: 0x06000330 RID: 816
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int Cache_GetIndex(int handle);

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000331 RID: 817 RVA: 0x00009FE0 File Offset: 0x000081E0
		public long spaceFree
		{
			get
			{
				return Cache.Cache_GetSpaceFree(this.m_Handle);
			}
		}

		// Token: 0x06000332 RID: 818
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern long Cache_GetSpaceFree(int handle);

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000333 RID: 819 RVA: 0x0000A000 File Offset: 0x00008200
		// (set) Token: 0x06000334 RID: 820 RVA: 0x0000A020 File Offset: 0x00008220
		public long maximumAvailableStorageSpace
		{
			get
			{
				return Cache.Cache_GetMaximumDiskSpaceAvailable(this.m_Handle);
			}
			set
			{
				Cache.Cache_SetMaximumDiskSpaceAvailable(this.m_Handle, value);
			}
		}

		// Token: 0x06000335 RID: 821
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern long Cache_GetMaximumDiskSpaceAvailable(int handle);

		// Token: 0x06000336 RID: 822
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Cache_SetMaximumDiskSpaceAvailable(int handle, long value);

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000337 RID: 823 RVA: 0x0000A030 File Offset: 0x00008230
		public long spaceOccupied
		{
			get
			{
				return Cache.Cache_GetCachingDiskSpaceUsed(this.m_Handle);
			}
		}

		// Token: 0x06000338 RID: 824
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern long Cache_GetCachingDiskSpaceUsed(int handle);

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000339 RID: 825 RVA: 0x0000A050 File Offset: 0x00008250
		// (set) Token: 0x0600033A RID: 826 RVA: 0x0000A070 File Offset: 0x00008270
		public int expirationDelay
		{
			get
			{
				return Cache.Cache_GetExpirationDelay(this.m_Handle);
			}
			set
			{
				Cache.Cache_SetExpirationDelay(this.m_Handle, value);
			}
		}

		// Token: 0x0600033B RID: 827
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int Cache_GetExpirationDelay(int handle);

		// Token: 0x0600033C RID: 828
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Cache_SetExpirationDelay(int handle, int value);

		// Token: 0x0600033D RID: 829 RVA: 0x0000A080 File Offset: 0x00008280
		public bool ClearCache()
		{
			return Cache.Cache_ClearCache(this.m_Handle);
		}

		// Token: 0x0600033E RID: 830
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool Cache_ClearCache(int handle);

		// Token: 0x0600033F RID: 831 RVA: 0x0000A0A0 File Offset: 0x000082A0
		public bool ClearCache(int expiration)
		{
			return Cache.Cache_ClearCache_Expiration(this.m_Handle, expiration);
		}

		// Token: 0x06000340 RID: 832
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool Cache_ClearCache_Expiration(int handle, int expiration);

		// Token: 0x040000F2 RID: 242
		private int m_Handle;
	}
}
