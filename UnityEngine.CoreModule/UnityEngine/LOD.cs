﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000144 RID: 324
	[UsedByNativeCode]
	public struct LOD
	{
		// Token: 0x06000CEE RID: 3310 RVA: 0x0001435B File Offset: 0x0001255B
		public LOD(float screenRelativeTransitionHeight, Renderer[] renderers)
		{
			this.screenRelativeTransitionHeight = screenRelativeTransitionHeight;
			this.fadeTransitionWidth = 0f;
			this.renderers = renderers;
		}

		// Token: 0x040006B5 RID: 1717
		public float screenRelativeTransitionHeight;

		// Token: 0x040006B6 RID: 1718
		public float fadeTransitionWidth;

		// Token: 0x040006B7 RID: 1719
		public Renderer[] renderers;
	}
}
