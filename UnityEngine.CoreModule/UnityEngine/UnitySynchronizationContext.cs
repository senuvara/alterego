﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000253 RID: 595
	internal sealed class UnitySynchronizationContext : SynchronizationContext
	{
		// Token: 0x06001568 RID: 5480 RVA: 0x00024FF8 File Offset: 0x000231F8
		private UnitySynchronizationContext(int mainThreadID)
		{
			this.m_AsyncWorkQueue = new List<UnitySynchronizationContext.WorkRequest>(20);
			this.m_MainThreadID = mainThreadID;
		}

		// Token: 0x06001569 RID: 5481 RVA: 0x00025022 File Offset: 0x00023222
		private UnitySynchronizationContext(List<UnitySynchronizationContext.WorkRequest> queue, int mainThreadID)
		{
			this.m_AsyncWorkQueue = queue;
			this.m_MainThreadID = mainThreadID;
		}

		// Token: 0x0600156A RID: 5482 RVA: 0x00025048 File Offset: 0x00023248
		public override void Send(SendOrPostCallback callback, object state)
		{
			if (this.m_MainThreadID == Thread.CurrentThread.ManagedThreadId)
			{
				callback(state);
			}
			else
			{
				using (ManualResetEvent manualResetEvent = new ManualResetEvent(false))
				{
					object asyncWorkQueue = this.m_AsyncWorkQueue;
					lock (asyncWorkQueue)
					{
						this.m_AsyncWorkQueue.Add(new UnitySynchronizationContext.WorkRequest(callback, state, manualResetEvent));
					}
					manualResetEvent.WaitOne();
				}
			}
		}

		// Token: 0x0600156B RID: 5483 RVA: 0x000250E8 File Offset: 0x000232E8
		public override void Post(SendOrPostCallback callback, object state)
		{
			object asyncWorkQueue = this.m_AsyncWorkQueue;
			lock (asyncWorkQueue)
			{
				this.m_AsyncWorkQueue.Add(new UnitySynchronizationContext.WorkRequest(callback, state, null));
			}
		}

		// Token: 0x0600156C RID: 5484 RVA: 0x00025134 File Offset: 0x00023334
		public override SynchronizationContext CreateCopy()
		{
			return new UnitySynchronizationContext(this.m_AsyncWorkQueue, this.m_MainThreadID);
		}

		// Token: 0x0600156D RID: 5485 RVA: 0x0002515C File Offset: 0x0002335C
		private void Exec()
		{
			object asyncWorkQueue = this.m_AsyncWorkQueue;
			lock (asyncWorkQueue)
			{
				this.m_CurrentFrameWork.AddRange(this.m_AsyncWorkQueue);
				this.m_AsyncWorkQueue.Clear();
			}
			foreach (UnitySynchronizationContext.WorkRequest workRequest in this.m_CurrentFrameWork)
			{
				workRequest.Invoke();
			}
			this.m_CurrentFrameWork.Clear();
		}

		// Token: 0x0600156E RID: 5486 RVA: 0x00025208 File Offset: 0x00023408
		[RequiredByNativeCode]
		private static void InitializeSynchronizationContext()
		{
			if (SynchronizationContext.Current == null)
			{
				SynchronizationContext.SetSynchronizationContext(new UnitySynchronizationContext(Thread.CurrentThread.ManagedThreadId));
			}
		}

		// Token: 0x0600156F RID: 5487 RVA: 0x0002522C File Offset: 0x0002342C
		[RequiredByNativeCode]
		private static void ExecuteTasks()
		{
			UnitySynchronizationContext unitySynchronizationContext = SynchronizationContext.Current as UnitySynchronizationContext;
			if (unitySynchronizationContext != null)
			{
				unitySynchronizationContext.Exec();
			}
		}

		// Token: 0x04000809 RID: 2057
		private const int kAwqInitialCapacity = 20;

		// Token: 0x0400080A RID: 2058
		private readonly List<UnitySynchronizationContext.WorkRequest> m_AsyncWorkQueue;

		// Token: 0x0400080B RID: 2059
		private readonly List<UnitySynchronizationContext.WorkRequest> m_CurrentFrameWork = new List<UnitySynchronizationContext.WorkRequest>(20);

		// Token: 0x0400080C RID: 2060
		private readonly int m_MainThreadID;

		// Token: 0x02000254 RID: 596
		private struct WorkRequest
		{
			// Token: 0x06001570 RID: 5488 RVA: 0x00025251 File Offset: 0x00023451
			public WorkRequest(SendOrPostCallback callback, object state, ManualResetEvent waitHandle = null)
			{
				this.m_DelagateCallback = callback;
				this.m_DelagateState = state;
				this.m_WaitHandle = waitHandle;
			}

			// Token: 0x06001571 RID: 5489 RVA: 0x0002526C File Offset: 0x0002346C
			public void Invoke()
			{
				try
				{
					this.m_DelagateCallback(this.m_DelagateState);
				}
				catch (Exception exception)
				{
					Debug.LogException(exception);
				}
				if (this.m_WaitHandle != null)
				{
					this.m_WaitHandle.Set();
				}
			}

			// Token: 0x0400080D RID: 2061
			private readonly SendOrPostCallback m_DelagateCallback;

			// Token: 0x0400080E RID: 2062
			private readonly object m_DelagateState;

			// Token: 0x0400080F RID: 2063
			private readonly ManualResetEvent m_WaitHandle;
		}
	}
}
