﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000224 RID: 548
	[NativeHeader("Runtime/Scripting/TextAsset.h")]
	public class TextAsset : Object
	{
		// Token: 0x060012BD RID: 4797 RVA: 0x00021186 File Offset: 0x0001F386
		public TextAsset() : this(TextAsset.CreateOptions.CreateNativeObject, null)
		{
		}

		// Token: 0x060012BE RID: 4798 RVA: 0x00021191 File Offset: 0x0001F391
		public TextAsset(string text) : this(TextAsset.CreateOptions.CreateNativeObject, text)
		{
		}

		// Token: 0x060012BF RID: 4799 RVA: 0x0002119C File Offset: 0x0001F39C
		internal TextAsset(TextAsset.CreateOptions options, string text)
		{
			if (options == TextAsset.CreateOptions.CreateNativeObject)
			{
				TextAsset.Internal_CreateInstance(this, text);
			}
		}

		// Token: 0x17000385 RID: 901
		// (get) Token: 0x060012C0 RID: 4800
		public extern string text { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000386 RID: 902
		// (get) Token: 0x060012C1 RID: 4801
		public extern byte[] bytes { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060012C2 RID: 4802 RVA: 0x000211B8 File Offset: 0x0001F3B8
		public override string ToString()
		{
			return this.text;
		}

		// Token: 0x060012C3 RID: 4803
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateInstance([Writable] TextAsset self, string text);

		// Token: 0x02000225 RID: 549
		internal enum CreateOptions
		{
			// Token: 0x040007A8 RID: 1960
			None,
			// Token: 0x040007A9 RID: 1961
			CreateNativeObject
		}
	}
}
