﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020000A6 RID: 166
	[NativeHeader("Runtime/Camera/Flare.h")]
	public sealed class Flare : Object
	{
		// Token: 0x06000AA8 RID: 2728 RVA: 0x00012344 File Offset: 0x00010544
		public Flare()
		{
			Flare.Internal_Create(this);
		}

		// Token: 0x06000AA9 RID: 2729
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] Flare self);
	}
}
