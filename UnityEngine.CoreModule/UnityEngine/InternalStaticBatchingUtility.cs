﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200031B RID: 795
	internal class InternalStaticBatchingUtility
	{
		// Token: 0x06001B43 RID: 6979 RVA: 0x00002370 File Offset: 0x00000570
		public InternalStaticBatchingUtility()
		{
		}

		// Token: 0x06001B44 RID: 6980 RVA: 0x0002FE9D File Offset: 0x0002E09D
		public static void CombineRoot(GameObject staticBatchRoot)
		{
			InternalStaticBatchingUtility.Combine(staticBatchRoot, false, false);
		}

		// Token: 0x06001B45 RID: 6981 RVA: 0x0002FEA8 File Offset: 0x0002E0A8
		public static void Combine(GameObject staticBatchRoot, bool combineOnlyStatic, bool isEditorPostprocessScene)
		{
			GameObject[] array = (GameObject[])Object.FindObjectsOfType(typeof(GameObject));
			List<GameObject> list = new List<GameObject>();
			foreach (GameObject gameObject in array)
			{
				if (!(staticBatchRoot != null) || gameObject.transform.IsChildOf(staticBatchRoot.transform))
				{
					if (!combineOnlyStatic || gameObject.isStaticBatchable)
					{
						list.Add(gameObject);
					}
				}
			}
			array = list.ToArray();
			InternalStaticBatchingUtility.CombineGameObjects(array, staticBatchRoot, isEditorPostprocessScene);
		}

		// Token: 0x06001B46 RID: 6982 RVA: 0x0002FF48 File Offset: 0x0002E148
		public static void CombineGameObjects(GameObject[] gos, GameObject staticBatchRoot, bool isEditorPostprocessScene)
		{
			Matrix4x4 lhs = Matrix4x4.identity;
			Transform staticBatchRootTransform = null;
			if (staticBatchRoot)
			{
				lhs = staticBatchRoot.transform.worldToLocalMatrix;
				staticBatchRootTransform = staticBatchRoot.transform;
			}
			int batchIndex = 0;
			int num = 0;
			List<MeshSubsetCombineUtility.MeshContainer> list = new List<MeshSubsetCombineUtility.MeshContainer>();
			Array.Sort(gos, new InternalStaticBatchingUtility.SortGO());
			foreach (GameObject gameObject in gos)
			{
				MeshFilter meshFilter = gameObject.GetComponent(typeof(MeshFilter)) as MeshFilter;
				if (!(meshFilter == null))
				{
					Mesh sharedMesh = meshFilter.sharedMesh;
					if (!(sharedMesh == null) && (isEditorPostprocessScene || sharedMesh.canAccess))
					{
						Renderer component = meshFilter.GetComponent<Renderer>();
						if (!(component == null) && component.enabled)
						{
							if (component.staticBatchIndex == 0)
							{
								Material[] array = component.sharedMaterials;
								if (!array.Any((Material m) => m != null && m.shader != null && m.shader.disableBatching != DisableBatchingType.False))
								{
									int vertexCount = sharedMesh.vertexCount;
									if (vertexCount != 0)
									{
										MeshRenderer meshRenderer = component as MeshRenderer;
										if (meshRenderer != null && meshRenderer.additionalVertexStreams != null)
										{
											if (vertexCount != meshRenderer.additionalVertexStreams.vertexCount)
											{
												goto IL_387;
											}
										}
										if (num + vertexCount > 64000)
										{
											InternalStaticBatchingUtility.MakeBatch(list, staticBatchRootTransform, batchIndex++);
											list.Clear();
											num = 0;
										}
										MeshSubsetCombineUtility.MeshInstance instance = default(MeshSubsetCombineUtility.MeshInstance);
										instance.meshInstanceID = sharedMesh.GetInstanceID();
										instance.rendererInstanceID = component.GetInstanceID();
										if (meshRenderer != null && meshRenderer.additionalVertexStreams != null)
										{
											instance.additionalVertexStreamsMeshInstanceID = meshRenderer.additionalVertexStreams.GetInstanceID();
										}
										instance.transform = lhs * meshFilter.transform.localToWorldMatrix;
										instance.lightmapScaleOffset = component.lightmapScaleOffset;
										instance.realtimeLightmapScaleOffset = component.realtimeLightmapScaleOffset;
										MeshSubsetCombineUtility.MeshContainer item = new MeshSubsetCombineUtility.MeshContainer
										{
											gameObject = gameObject,
											instance = instance,
											subMeshInstances = new List<MeshSubsetCombineUtility.SubMeshInstance>()
										};
										list.Add(item);
										if (array.Length > sharedMesh.subMeshCount)
										{
											Debug.LogWarning(string.Concat(new object[]
											{
												"Mesh '",
												sharedMesh.name,
												"' has more materials (",
												array.Length,
												") than subsets (",
												sharedMesh.subMeshCount,
												")"
											}), component);
											Material[] array2 = new Material[sharedMesh.subMeshCount];
											for (int j = 0; j < sharedMesh.subMeshCount; j++)
											{
												array2[j] = component.sharedMaterials[j];
											}
											component.sharedMaterials = array2;
											array = array2;
										}
										for (int k = 0; k < Math.Min(array.Length, sharedMesh.subMeshCount); k++)
										{
											MeshSubsetCombineUtility.SubMeshInstance item2 = default(MeshSubsetCombineUtility.SubMeshInstance);
											item2.meshInstanceID = meshFilter.sharedMesh.GetInstanceID();
											item2.vertexOffset = num;
											item2.subMeshIndex = k;
											item2.gameObjectInstanceID = gameObject.GetInstanceID();
											item2.transform = instance.transform;
											item.subMeshInstances.Add(item2);
										}
										num += sharedMesh.vertexCount;
									}
								}
							}
						}
					}
				}
				IL_387:;
			}
			InternalStaticBatchingUtility.MakeBatch(list, staticBatchRootTransform, batchIndex);
		}

		// Token: 0x06001B47 RID: 6983 RVA: 0x000302F8 File Offset: 0x0002E4F8
		private static void MakeBatch(List<MeshSubsetCombineUtility.MeshContainer> meshes, Transform staticBatchRootTransform, int batchIndex)
		{
			if (meshes.Count >= 2)
			{
				List<MeshSubsetCombineUtility.MeshInstance> list = new List<MeshSubsetCombineUtility.MeshInstance>();
				List<MeshSubsetCombineUtility.SubMeshInstance> list2 = new List<MeshSubsetCombineUtility.SubMeshInstance>();
				foreach (MeshSubsetCombineUtility.MeshContainer meshContainer in meshes)
				{
					list.Add(meshContainer.instance);
					list2.AddRange(meshContainer.subMeshInstances);
				}
				string text = "Combined Mesh";
				text = text + " (root: " + ((!(staticBatchRootTransform != null)) ? "scene" : staticBatchRootTransform.name) + ")";
				if (batchIndex > 0)
				{
					text = text + " " + (batchIndex + 1);
				}
				Mesh mesh = StaticBatchingHelper.InternalCombineVertices(list.ToArray(), text);
				StaticBatchingHelper.InternalCombineIndices(list2.ToArray(), mesh);
				int num = 0;
				foreach (MeshSubsetCombineUtility.MeshContainer meshContainer2 in meshes)
				{
					MeshFilter meshFilter = (MeshFilter)meshContainer2.gameObject.GetComponent(typeof(MeshFilter));
					meshFilter.sharedMesh = mesh;
					int num2 = meshContainer2.subMeshInstances.Count<MeshSubsetCombineUtility.SubMeshInstance>();
					Renderer component = meshContainer2.gameObject.GetComponent<Renderer>();
					component.SetStaticBatchInfo(num, num2);
					component.staticBatchRootTransform = staticBatchRootTransform;
					component.enabled = false;
					component.enabled = true;
					MeshRenderer meshRenderer = component as MeshRenderer;
					if (meshRenderer != null)
					{
						meshRenderer.additionalVertexStreams = null;
					}
					num += num2;
				}
			}
		}

		// Token: 0x06001B48 RID: 6984 RVA: 0x000304C4 File Offset: 0x0002E6C4
		[CompilerGenerated]
		private static bool <CombineGameObjects>m__0(Material m)
		{
			return m != null && m.shader != null && m.shader.disableBatching != DisableBatchingType.False;
		}

		// Token: 0x04000A4A RID: 2634
		private const int MaxVerticesInBatch = 64000;

		// Token: 0x04000A4B RID: 2635
		private const string CombinedMeshPrefix = "Combined Mesh";

		// Token: 0x04000A4C RID: 2636
		[CompilerGenerated]
		private static Func<Material, bool> <>f__am$cache0;

		// Token: 0x0200031C RID: 796
		internal class SortGO : IComparer
		{
			// Token: 0x06001B49 RID: 6985 RVA: 0x00002370 File Offset: 0x00000570
			public SortGO()
			{
			}

			// Token: 0x06001B4A RID: 6986 RVA: 0x0003050C File Offset: 0x0002E70C
			int IComparer.Compare(object a, object b)
			{
				int result;
				if (a == b)
				{
					result = 0;
				}
				else
				{
					Renderer renderer = InternalStaticBatchingUtility.SortGO.GetRenderer(a as GameObject);
					Renderer renderer2 = InternalStaticBatchingUtility.SortGO.GetRenderer(b as GameObject);
					int num = InternalStaticBatchingUtility.SortGO.GetMaterialId(renderer).CompareTo(InternalStaticBatchingUtility.SortGO.GetMaterialId(renderer2));
					if (num == 0)
					{
						num = InternalStaticBatchingUtility.SortGO.GetLightmapIndex(renderer).CompareTo(InternalStaticBatchingUtility.SortGO.GetLightmapIndex(renderer2));
					}
					result = num;
				}
				return result;
			}

			// Token: 0x06001B4B RID: 6987 RVA: 0x0003057C File Offset: 0x0002E77C
			private static int GetMaterialId(Renderer renderer)
			{
				int result;
				if (renderer == null || renderer.sharedMaterial == null)
				{
					result = 0;
				}
				else
				{
					result = renderer.sharedMaterial.GetInstanceID();
				}
				return result;
			}

			// Token: 0x06001B4C RID: 6988 RVA: 0x000305C0 File Offset: 0x0002E7C0
			private static int GetLightmapIndex(Renderer renderer)
			{
				int result;
				if (renderer == null)
				{
					result = -1;
				}
				else
				{
					result = renderer.lightmapIndex;
				}
				return result;
			}

			// Token: 0x06001B4D RID: 6989 RVA: 0x000305F0 File Offset: 0x0002E7F0
			private static Renderer GetRenderer(GameObject go)
			{
				Renderer result;
				if (go == null)
				{
					result = null;
				}
				else
				{
					MeshFilter meshFilter = go.GetComponent(typeof(MeshFilter)) as MeshFilter;
					if (meshFilter == null)
					{
						result = null;
					}
					else
					{
						result = meshFilter.GetComponent<Renderer>();
					}
				}
				return result;
			}
		}
	}
}
