﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200002E RID: 46
	[UsedByNativeCode]
	public sealed class ExecuteAlways : Attribute
	{
		// Token: 0x0600028C RID: 652 RVA: 0x0000898B File Offset: 0x00006B8B
		public ExecuteAlways()
		{
		}
	}
}
