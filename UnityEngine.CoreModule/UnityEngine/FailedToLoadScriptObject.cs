﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000071 RID: 113
	[NativeClass(null)]
	[RequiredByNativeCode]
	[ExcludeFromObjectFactory]
	[StructLayout(LayoutKind.Sequential)]
	internal class FailedToLoadScriptObject : Object
	{
		// Token: 0x0600059C RID: 1436 RVA: 0x0000D647 File Offset: 0x0000B847
		private FailedToLoadScriptObject()
		{
		}
	}
}
