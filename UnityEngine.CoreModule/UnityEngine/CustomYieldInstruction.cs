﻿using System;
using System.Collections;

namespace UnityEngine
{
	// Token: 0x02000065 RID: 101
	public abstract class CustomYieldInstruction : IEnumerator
	{
		// Token: 0x06000531 RID: 1329 RVA: 0x00002370 File Offset: 0x00000570
		protected CustomYieldInstruction()
		{
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x06000532 RID: 1330
		public abstract bool keepWaiting { get; }

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000533 RID: 1331 RVA: 0x0000CEA8 File Offset: 0x0000B0A8
		public object Current
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06000534 RID: 1332 RVA: 0x0000CEC0 File Offset: 0x0000B0C0
		public bool MoveNext()
		{
			return this.keepWaiting;
		}

		// Token: 0x06000535 RID: 1333 RVA: 0x00007476 File Offset: 0x00005676
		public void Reset()
		{
		}
	}
}
