﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000145 RID: 325
	[NativeHeader("Runtime/Graphics/LOD/LODGroup.h")]
	[StaticAccessor("GetLODGroupManager()", StaticAccessorType.Dot)]
	[NativeHeader("Runtime/Graphics/LOD/LODUtility.h")]
	[NativeHeader("Runtime/Graphics/LOD/LODGroupManager.h")]
	public class LODGroup : Component
	{
		// Token: 0x06000CEF RID: 3311 RVA: 0x00008E34 File Offset: 0x00007034
		public LODGroup()
		{
		}

		// Token: 0x17000294 RID: 660
		// (get) Token: 0x06000CF0 RID: 3312 RVA: 0x00014378 File Offset: 0x00012578
		// (set) Token: 0x06000CF1 RID: 3313 RVA: 0x0001438E File Offset: 0x0001258E
		public Vector3 localReferencePoint
		{
			get
			{
				Vector3 result;
				this.get_localReferencePoint_Injected(out result);
				return result;
			}
			set
			{
				this.set_localReferencePoint_Injected(ref value);
			}
		}

		// Token: 0x17000295 RID: 661
		// (get) Token: 0x06000CF2 RID: 3314
		// (set) Token: 0x06000CF3 RID: 3315
		public extern float size { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000296 RID: 662
		// (get) Token: 0x06000CF4 RID: 3316
		public extern int lodCount { [NativeMethod("GetLODCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000297 RID: 663
		// (get) Token: 0x06000CF5 RID: 3317
		// (set) Token: 0x06000CF6 RID: 3318
		public extern LODFadeMode fadeMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000298 RID: 664
		// (get) Token: 0x06000CF7 RID: 3319
		// (set) Token: 0x06000CF8 RID: 3320
		public extern bool animateCrossFading { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000299 RID: 665
		// (get) Token: 0x06000CF9 RID: 3321
		// (set) Token: 0x06000CFA RID: 3322
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000CFB RID: 3323
		[FreeFunction("UpdateLODGroupBoundingBox", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RecalculateBounds();

		// Token: 0x06000CFC RID: 3324
		[FreeFunction("GetLODs_Binding", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern LOD[] GetLODs();

		// Token: 0x06000CFD RID: 3325 RVA: 0x00014398 File Offset: 0x00012598
		[Obsolete("Use SetLODs instead.")]
		public void SetLODS(LOD[] lods)
		{
			this.SetLODs(lods);
		}

		// Token: 0x06000CFE RID: 3326
		[FreeFunction("SetLODs_Binding", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetLODs(LOD[] lods);

		// Token: 0x06000CFF RID: 3327
		[FreeFunction("ForceLODLevel", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ForceLOD(int index);

		// Token: 0x1700029A RID: 666
		// (get) Token: 0x06000D00 RID: 3328
		// (set) Token: 0x06000D01 RID: 3329
		[StaticAccessor("GetLODGroupManager()")]
		public static extern float crossFadeAnimationDuration { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000D02 RID: 3330
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_localReferencePoint_Injected(out Vector3 ret);

		// Token: 0x06000D03 RID: 3331
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_localReferencePoint_Injected(ref Vector3 value);
	}
}
