﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B8 RID: 184
	[Flags]
	public enum LightmapBakeType
	{
		// Token: 0x040001E0 RID: 480
		Realtime = 4,
		// Token: 0x040001E1 RID: 481
		Baked = 2,
		// Token: 0x040001E2 RID: 482
		Mixed = 1
	}
}
