﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000258 RID: 600
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	[NativeHeader("Runtime/Math/Vector4.h")]
	[NativeClass("Vector4f")]
	public struct Vector4 : IEquatable<Vector4>
	{
		// Token: 0x060015F6 RID: 5622 RVA: 0x00026E53 File Offset: 0x00025053
		public Vector4(float x, float y, float z, float w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		// Token: 0x060015F7 RID: 5623 RVA: 0x00026E73 File Offset: 0x00025073
		public Vector4(float x, float y, float z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = 0f;
		}

		// Token: 0x060015F8 RID: 5624 RVA: 0x00026E96 File Offset: 0x00025096
		public Vector4(float x, float y)
		{
			this.x = x;
			this.y = y;
			this.z = 0f;
			this.w = 0f;
		}

		// Token: 0x17000438 RID: 1080
		public float this[int index]
		{
			get
			{
				float result;
				switch (index)
				{
				case 0:
					result = this.x;
					break;
				case 1:
					result = this.y;
					break;
				case 2:
					result = this.z;
					break;
				case 3:
					result = this.w;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector4 index!");
				}
				return result;
			}
			set
			{
				switch (index)
				{
				case 0:
					this.x = value;
					break;
				case 1:
					this.y = value;
					break;
				case 2:
					this.z = value;
					break;
				case 3:
					this.w = value;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector4 index!");
				}
			}
		}

		// Token: 0x060015FB RID: 5627 RVA: 0x00026E53 File Offset: 0x00025053
		public void Set(float newX, float newY, float newZ, float newW)
		{
			this.x = newX;
			this.y = newY;
			this.z = newZ;
			this.w = newW;
		}

		// Token: 0x060015FC RID: 5628 RVA: 0x00026F8C File Offset: 0x0002518C
		public static Vector4 Lerp(Vector4 a, Vector4 b, float t)
		{
			t = Mathf.Clamp01(t);
			return new Vector4(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t, a.z + (b.z - a.z) * t, a.w + (b.w - a.w) * t);
		}

		// Token: 0x060015FD RID: 5629 RVA: 0x00027014 File Offset: 0x00025214
		public static Vector4 LerpUnclamped(Vector4 a, Vector4 b, float t)
		{
			return new Vector4(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t, a.z + (b.z - a.z) * t, a.w + (b.w - a.w) * t);
		}

		// Token: 0x060015FE RID: 5630 RVA: 0x00027094 File Offset: 0x00025294
		public static Vector4 MoveTowards(Vector4 current, Vector4 target, float maxDistanceDelta)
		{
			Vector4 a = target - current;
			float magnitude = a.magnitude;
			Vector4 result;
			if (magnitude <= maxDistanceDelta || magnitude == 0f)
			{
				result = target;
			}
			else
			{
				result = current + a / magnitude * maxDistanceDelta;
			}
			return result;
		}

		// Token: 0x060015FF RID: 5631 RVA: 0x000270E8 File Offset: 0x000252E8
		public static Vector4 Scale(Vector4 a, Vector4 b)
		{
			return new Vector4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
		}

		// Token: 0x06001600 RID: 5632 RVA: 0x00027140 File Offset: 0x00025340
		public void Scale(Vector4 scale)
		{
			this.x *= scale.x;
			this.y *= scale.y;
			this.z *= scale.z;
			this.w *= scale.w;
		}

		// Token: 0x06001601 RID: 5633 RVA: 0x000271A0 File Offset: 0x000253A0
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.y.GetHashCode() << 2 ^ this.z.GetHashCode() >> 2 ^ this.w.GetHashCode() >> 1;
		}

		// Token: 0x06001602 RID: 5634 RVA: 0x00027204 File Offset: 0x00025404
		public override bool Equals(object other)
		{
			return other is Vector4 && this.Equals((Vector4)other);
		}

		// Token: 0x06001603 RID: 5635 RVA: 0x00027238 File Offset: 0x00025438
		public bool Equals(Vector4 other)
		{
			return this.x.Equals(other.x) && this.y.Equals(other.y) && this.z.Equals(other.z) && this.w.Equals(other.w);
		}

		// Token: 0x06001604 RID: 5636 RVA: 0x000272A8 File Offset: 0x000254A8
		public static Vector4 Normalize(Vector4 a)
		{
			float num = Vector4.Magnitude(a);
			Vector4 result;
			if (num > 1E-05f)
			{
				result = a / num;
			}
			else
			{
				result = Vector4.zero;
			}
			return result;
		}

		// Token: 0x06001605 RID: 5637 RVA: 0x000272E4 File Offset: 0x000254E4
		public void Normalize()
		{
			float num = Vector4.Magnitude(this);
			if (num > 1E-05f)
			{
				this /= num;
			}
			else
			{
				this = Vector4.zero;
			}
		}

		// Token: 0x17000439 RID: 1081
		// (get) Token: 0x06001606 RID: 5638 RVA: 0x0002732C File Offset: 0x0002552C
		public Vector4 normalized
		{
			get
			{
				return Vector4.Normalize(this);
			}
		}

		// Token: 0x06001607 RID: 5639 RVA: 0x0002734C File Offset: 0x0002554C
		public static float Dot(Vector4 a, Vector4 b)
		{
			return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
		}

		// Token: 0x06001608 RID: 5640 RVA: 0x000273A0 File Offset: 0x000255A0
		public static Vector4 Project(Vector4 a, Vector4 b)
		{
			return b * Vector4.Dot(a, b) / Vector4.Dot(b, b);
		}

		// Token: 0x06001609 RID: 5641 RVA: 0x000273D0 File Offset: 0x000255D0
		public static float Distance(Vector4 a, Vector4 b)
		{
			return Vector4.Magnitude(a - b);
		}

		// Token: 0x0600160A RID: 5642 RVA: 0x000273F4 File Offset: 0x000255F4
		public static float Magnitude(Vector4 a)
		{
			return Mathf.Sqrt(Vector4.Dot(a, a));
		}

		// Token: 0x1700043A RID: 1082
		// (get) Token: 0x0600160B RID: 5643 RVA: 0x00027418 File Offset: 0x00025618
		public float magnitude
		{
			get
			{
				return Mathf.Sqrt(Vector4.Dot(this, this));
			}
		}

		// Token: 0x1700043B RID: 1083
		// (get) Token: 0x0600160C RID: 5644 RVA: 0x00027444 File Offset: 0x00025644
		public float sqrMagnitude
		{
			get
			{
				return Vector4.Dot(this, this);
			}
		}

		// Token: 0x0600160D RID: 5645 RVA: 0x0002746C File Offset: 0x0002566C
		public static Vector4 Min(Vector4 lhs, Vector4 rhs)
		{
			return new Vector4(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y), Mathf.Min(lhs.z, rhs.z), Mathf.Min(lhs.w, rhs.w));
		}

		// Token: 0x0600160E RID: 5646 RVA: 0x000274D4 File Offset: 0x000256D4
		public static Vector4 Max(Vector4 lhs, Vector4 rhs)
		{
			return new Vector4(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y), Mathf.Max(lhs.z, rhs.z), Mathf.Max(lhs.w, rhs.w));
		}

		// Token: 0x1700043C RID: 1084
		// (get) Token: 0x0600160F RID: 5647 RVA: 0x0002753C File Offset: 0x0002573C
		public static Vector4 zero
		{
			get
			{
				return Vector4.zeroVector;
			}
		}

		// Token: 0x1700043D RID: 1085
		// (get) Token: 0x06001610 RID: 5648 RVA: 0x00027558 File Offset: 0x00025758
		public static Vector4 one
		{
			get
			{
				return Vector4.oneVector;
			}
		}

		// Token: 0x1700043E RID: 1086
		// (get) Token: 0x06001611 RID: 5649 RVA: 0x00027574 File Offset: 0x00025774
		public static Vector4 positiveInfinity
		{
			get
			{
				return Vector4.positiveInfinityVector;
			}
		}

		// Token: 0x1700043F RID: 1087
		// (get) Token: 0x06001612 RID: 5650 RVA: 0x00027590 File Offset: 0x00025790
		public static Vector4 negativeInfinity
		{
			get
			{
				return Vector4.negativeInfinityVector;
			}
		}

		// Token: 0x06001613 RID: 5651 RVA: 0x000275AC File Offset: 0x000257AC
		public static Vector4 operator +(Vector4 a, Vector4 b)
		{
			return new Vector4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
		}

		// Token: 0x06001614 RID: 5652 RVA: 0x00027604 File Offset: 0x00025804
		public static Vector4 operator -(Vector4 a, Vector4 b)
		{
			return new Vector4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
		}

		// Token: 0x06001615 RID: 5653 RVA: 0x0002765C File Offset: 0x0002585C
		public static Vector4 operator -(Vector4 a)
		{
			return new Vector4(-a.x, -a.y, -a.z, -a.w);
		}

		// Token: 0x06001616 RID: 5654 RVA: 0x00027698 File Offset: 0x00025898
		public static Vector4 operator *(Vector4 a, float d)
		{
			return new Vector4(a.x * d, a.y * d, a.z * d, a.w * d);
		}

		// Token: 0x06001617 RID: 5655 RVA: 0x000276D8 File Offset: 0x000258D8
		public static Vector4 operator *(float d, Vector4 a)
		{
			return new Vector4(a.x * d, a.y * d, a.z * d, a.w * d);
		}

		// Token: 0x06001618 RID: 5656 RVA: 0x00027718 File Offset: 0x00025918
		public static Vector4 operator /(Vector4 a, float d)
		{
			return new Vector4(a.x / d, a.y / d, a.z / d, a.w / d);
		}

		// Token: 0x06001619 RID: 5657 RVA: 0x00027758 File Offset: 0x00025958
		public static bool operator ==(Vector4 lhs, Vector4 rhs)
		{
			return Vector4.SqrMagnitude(lhs - rhs) < 9.9999994E-11f;
		}

		// Token: 0x0600161A RID: 5658 RVA: 0x00027780 File Offset: 0x00025980
		public static bool operator !=(Vector4 lhs, Vector4 rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x0600161B RID: 5659 RVA: 0x000277A0 File Offset: 0x000259A0
		public static implicit operator Vector4(Vector3 v)
		{
			return new Vector4(v.x, v.y, v.z, 0f);
		}

		// Token: 0x0600161C RID: 5660 RVA: 0x000277D4 File Offset: 0x000259D4
		public static implicit operator Vector3(Vector4 v)
		{
			return new Vector3(v.x, v.y, v.z);
		}

		// Token: 0x0600161D RID: 5661 RVA: 0x00027804 File Offset: 0x00025A04
		public static implicit operator Vector4(Vector2 v)
		{
			return new Vector4(v.x, v.y, 0f, 0f);
		}

		// Token: 0x0600161E RID: 5662 RVA: 0x00027838 File Offset: 0x00025A38
		public static implicit operator Vector2(Vector4 v)
		{
			return new Vector2(v.x, v.y);
		}

		// Token: 0x0600161F RID: 5663 RVA: 0x00027860 File Offset: 0x00025A60
		public override string ToString()
		{
			return UnityString.Format("({0:F1}, {1:F1}, {2:F1}, {3:F1})", new object[]
			{
				this.x,
				this.y,
				this.z,
				this.w
			});
		}

		// Token: 0x06001620 RID: 5664 RVA: 0x000278C0 File Offset: 0x00025AC0
		public string ToString(string format)
		{
			return UnityString.Format("({0}, {1}, {2}, {3})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format),
				this.z.ToString(format),
				this.w.ToString(format)
			});
		}

		// Token: 0x06001621 RID: 5665 RVA: 0x00027924 File Offset: 0x00025B24
		public static float SqrMagnitude(Vector4 a)
		{
			return Vector4.Dot(a, a);
		}

		// Token: 0x06001622 RID: 5666 RVA: 0x00027940 File Offset: 0x00025B40
		public float SqrMagnitude()
		{
			return Vector4.Dot(this, this);
		}

		// Token: 0x06001623 RID: 5667 RVA: 0x00027968 File Offset: 0x00025B68
		// Note: this type is marked as 'beforefieldinit'.
		static Vector4()
		{
		}

		// Token: 0x0400082D RID: 2093
		public const float kEpsilon = 1E-05f;

		// Token: 0x0400082E RID: 2094
		public float x;

		// Token: 0x0400082F RID: 2095
		public float y;

		// Token: 0x04000830 RID: 2096
		public float z;

		// Token: 0x04000831 RID: 2097
		public float w;

		// Token: 0x04000832 RID: 2098
		private static readonly Vector4 zeroVector = new Vector4(0f, 0f, 0f, 0f);

		// Token: 0x04000833 RID: 2099
		private static readonly Vector4 oneVector = new Vector4(1f, 1f, 1f, 1f);

		// Token: 0x04000834 RID: 2100
		private static readonly Vector4 positiveInfinityVector = new Vector4(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);

		// Token: 0x04000835 RID: 2101
		private static readonly Vector4 negativeInfinityVector = new Vector4(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);
	}
}
