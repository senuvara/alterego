﻿using System;

namespace UnityEngine
{
	// Token: 0x020001F7 RID: 503
	public struct Ray
	{
		// Token: 0x06000FBC RID: 4028 RVA: 0x0001B2DE File Offset: 0x000194DE
		public Ray(Vector3 origin, Vector3 direction)
		{
			this.m_Origin = origin;
			this.m_Direction = direction.normalized;
		}

		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x06000FBD RID: 4029 RVA: 0x0001B2F8 File Offset: 0x000194F8
		// (set) Token: 0x06000FBE RID: 4030 RVA: 0x0001B313 File Offset: 0x00019513
		public Vector3 origin
		{
			get
			{
				return this.m_Origin;
			}
			set
			{
				this.m_Origin = value;
			}
		}

		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x06000FBF RID: 4031 RVA: 0x0001B320 File Offset: 0x00019520
		// (set) Token: 0x06000FC0 RID: 4032 RVA: 0x0001B33B File Offset: 0x0001953B
		public Vector3 direction
		{
			get
			{
				return this.m_Direction;
			}
			set
			{
				this.m_Direction = value.normalized;
			}
		}

		// Token: 0x06000FC1 RID: 4033 RVA: 0x0001B34C File Offset: 0x0001954C
		public Vector3 GetPoint(float distance)
		{
			return this.m_Origin + this.m_Direction * distance;
		}

		// Token: 0x06000FC2 RID: 4034 RVA: 0x0001B378 File Offset: 0x00019578
		public override string ToString()
		{
			return UnityString.Format("Origin: {0}, Dir: {1}", new object[]
			{
				this.m_Origin,
				this.m_Direction
			});
		}

		// Token: 0x06000FC3 RID: 4035 RVA: 0x0001B3BC File Offset: 0x000195BC
		public string ToString(string format)
		{
			return UnityString.Format("Origin: {0}, Dir: {1}", new object[]
			{
				this.m_Origin.ToString(format),
				this.m_Direction.ToString(format)
			});
		}

		// Token: 0x04000736 RID: 1846
		private Vector3 m_Origin;

		// Token: 0x04000737 RID: 1847
		private Vector3 m_Direction;
	}
}
