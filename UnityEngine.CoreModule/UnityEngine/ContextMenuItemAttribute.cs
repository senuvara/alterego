﻿using System;

namespace UnityEngine
{
	// Token: 0x020001E6 RID: 486
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = true)]
	public class ContextMenuItemAttribute : PropertyAttribute
	{
		// Token: 0x06000F7D RID: 3965 RVA: 0x0001ACBF File Offset: 0x00018EBF
		public ContextMenuItemAttribute(string name, string function)
		{
			this.name = name;
			this.function = function;
		}

		// Token: 0x0400071C RID: 1820
		public readonly string name;

		// Token: 0x0400071D RID: 1821
		public readonly string function;
	}
}
