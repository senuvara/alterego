﻿using System;

namespace UnityEngine
{
	// Token: 0x020000D7 RID: 215
	[Flags]
	public enum MaterialGlobalIlluminationFlags
	{
		// Token: 0x04000352 RID: 850
		None = 0,
		// Token: 0x04000353 RID: 851
		RealtimeEmissive = 1,
		// Token: 0x04000354 RID: 852
		BakedEmissive = 2,
		// Token: 0x04000355 RID: 853
		EmissiveIsBlack = 4,
		// Token: 0x04000356 RID: 854
		AnyEmissive = 3
	}
}
