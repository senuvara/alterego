﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B7 RID: 183
	public enum FogMode
	{
		// Token: 0x040001DC RID: 476
		Linear = 1,
		// Token: 0x040001DD RID: 477
		Exponential,
		// Token: 0x040001DE RID: 478
		ExponentialSquared
	}
}
