﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020002A5 RID: 677
	[NativeHeader("Runtime/GI/DynamicGI.h")]
	public sealed class DynamicGI
	{
		// Token: 0x0600187A RID: 6266 RVA: 0x00002370 File Offset: 0x00000570
		public DynamicGI()
		{
		}

		// Token: 0x17000469 RID: 1129
		// (get) Token: 0x0600187B RID: 6267
		// (set) Token: 0x0600187C RID: 6268
		public static extern float indirectScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700046A RID: 1130
		// (get) Token: 0x0600187D RID: 6269
		// (set) Token: 0x0600187E RID: 6270
		public static extern float updateThreshold { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700046B RID: 1131
		// (get) Token: 0x0600187F RID: 6271
		// (set) Token: 0x06001880 RID: 6272
		public static extern int materialUpdateTimeSlice { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001881 RID: 6273 RVA: 0x0002AACF File Offset: 0x00028CCF
		public static void SetEmissive(Renderer renderer, Color color)
		{
			DynamicGI.SetEmissive_Injected(renderer, ref color);
		}

		// Token: 0x06001882 RID: 6274
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetEnvironmentData([NotNull] float[] input);

		// Token: 0x1700046C RID: 1132
		// (get) Token: 0x06001883 RID: 6275
		// (set) Token: 0x06001884 RID: 6276
		public static extern bool synchronousMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700046D RID: 1133
		// (get) Token: 0x06001885 RID: 6277
		public static extern bool isConverged { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700046E RID: 1134
		// (get) Token: 0x06001886 RID: 6278
		internal static extern int scheduledMaterialUpdatesCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001887 RID: 6279
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void UpdateEnvironment();

		// Token: 0x06001888 RID: 6280 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("DynamicGI.UpdateMaterials(Renderer) is deprecated; instead, use extension method from RendererExtensions: 'renderer.UpdateGIMaterials()' (UnityUpgradable).", true)]
		public static void UpdateMaterials(Renderer renderer)
		{
		}

		// Token: 0x06001889 RID: 6281 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("DynamicGI.UpdateMaterials(Terrain) is deprecated; instead, use extension method from TerrainExtensions: 'terrain.UpdateGIMaterials()' (UnityUpgradable).", true)]
		public static void UpdateMaterials(Object renderer)
		{
		}

		// Token: 0x0600188A RID: 6282 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("DynamicGI.UpdateMaterials(Terrain, int, int, int, int) is deprecated; instead, use extension method from TerrainExtensions: 'terrain.UpdateGIMaterials(x, y, width, height)' (UnityUpgradable).", true)]
		public static void UpdateMaterials(Object renderer, int x, int y, int width, int height)
		{
		}

		// Token: 0x0600188B RID: 6283
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetEmissive_Injected(Renderer renderer, ref Color color);
	}
}
