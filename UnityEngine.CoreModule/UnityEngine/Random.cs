﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020001F4 RID: 500
	[NativeHeader("Runtime/Export/Random.bindings.h")]
	public sealed class Random
	{
		// Token: 0x06000F9D RID: 3997 RVA: 0x00002370 File Offset: 0x00000570
		public Random()
		{
		}

		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06000F9E RID: 3998
		// (set) Token: 0x06000F9F RID: 3999
		[StaticAccessor("GetScriptingRand()", StaticAccessorType.Dot)]
		[Obsolete("Deprecated. Use InitState() function or Random.state property instead.")]
		public static extern int seed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000FA0 RID: 4000
		[NativeMethod("SetSeed")]
		[StaticAccessor("GetScriptingRand()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void InitState(int seed);

		// Token: 0x170002EE RID: 750
		// (get) Token: 0x06000FA1 RID: 4001 RVA: 0x0001B074 File Offset: 0x00019274
		// (set) Token: 0x06000FA2 RID: 4002 RVA: 0x0001B089 File Offset: 0x00019289
		[StaticAccessor("GetScriptingRand()", StaticAccessorType.Dot)]
		public static Random.State state
		{
			get
			{
				Random.State result;
				Random.get_state_Injected(out result);
				return result;
			}
			set
			{
				Random.set_state_Injected(ref value);
			}
		}

		// Token: 0x06000FA3 RID: 4003
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float Range(float min, float max);

		// Token: 0x06000FA4 RID: 4004 RVA: 0x0001B094 File Offset: 0x00019294
		public static int Range(int min, int max)
		{
			return Random.RandomRangeInt(min, max);
		}

		// Token: 0x06000FA5 RID: 4005
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int RandomRangeInt(int min, int max);

		// Token: 0x170002EF RID: 751
		// (get) Token: 0x06000FA6 RID: 4006
		public static extern float value { [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x06000FA7 RID: 4007 RVA: 0x0001B0B0 File Offset: 0x000192B0
		public static Vector3 insideUnitSphere
		{
			[FreeFunction]
			get
			{
				Vector3 result;
				Random.get_insideUnitSphere_Injected(out result);
				return result;
			}
		}

		// Token: 0x06000FA8 RID: 4008
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRandomUnitCircle(out Vector2 output);

		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06000FA9 RID: 4009 RVA: 0x0001B0C8 File Offset: 0x000192C8
		public static Vector2 insideUnitCircle
		{
			get
			{
				Vector2 result;
				Random.GetRandomUnitCircle(out result);
				return result;
			}
		}

		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x06000FAA RID: 4010 RVA: 0x0001B0E8 File Offset: 0x000192E8
		public static Vector3 onUnitSphere
		{
			[FreeFunction]
			get
			{
				Vector3 result;
				Random.get_onUnitSphere_Injected(out result);
				return result;
			}
		}

		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x06000FAB RID: 4011 RVA: 0x0001B100 File Offset: 0x00019300
		public static Quaternion rotation
		{
			[FreeFunction]
			get
			{
				Quaternion result;
				Random.get_rotation_Injected(out result);
				return result;
			}
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x06000FAC RID: 4012 RVA: 0x0001B118 File Offset: 0x00019318
		public static Quaternion rotationUniform
		{
			[FreeFunction]
			get
			{
				Quaternion result;
				Random.get_rotationUniform_Injected(out result);
				return result;
			}
		}

		// Token: 0x06000FAD RID: 4013 RVA: 0x0001B130 File Offset: 0x00019330
		[Obsolete("Use Random.Range instead")]
		public static float RandomRange(float min, float max)
		{
			return Random.Range(min, max);
		}

		// Token: 0x06000FAE RID: 4014 RVA: 0x0001B14C File Offset: 0x0001934C
		[Obsolete("Use Random.Range instead")]
		public static int RandomRange(int min, int max)
		{
			return Random.Range(min, max);
		}

		// Token: 0x06000FAF RID: 4015 RVA: 0x0001B168 File Offset: 0x00019368
		public static Color ColorHSV()
		{
			return Random.ColorHSV(0f, 1f, 0f, 1f, 0f, 1f, 1f, 1f);
		}

		// Token: 0x06000FB0 RID: 4016 RVA: 0x0001B1AC File Offset: 0x000193AC
		public static Color ColorHSV(float hueMin, float hueMax)
		{
			return Random.ColorHSV(hueMin, hueMax, 0f, 1f, 0f, 1f, 1f, 1f);
		}

		// Token: 0x06000FB1 RID: 4017 RVA: 0x0001B1E8 File Offset: 0x000193E8
		public static Color ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax)
		{
			return Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMax, 0f, 1f, 1f, 1f);
		}

		// Token: 0x06000FB2 RID: 4018 RVA: 0x0001B21C File Offset: 0x0001941C
		public static Color ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax, float valueMin, float valueMax)
		{
			return Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMax, valueMin, valueMax, 1f, 1f);
		}

		// Token: 0x06000FB3 RID: 4019 RVA: 0x0001B248 File Offset: 0x00019448
		public static Color ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax, float valueMin, float valueMax, float alphaMin, float alphaMax)
		{
			float h = Mathf.Lerp(hueMin, hueMax, Random.value);
			float s = Mathf.Lerp(saturationMin, saturationMax, Random.value);
			float v = Mathf.Lerp(valueMin, valueMax, Random.value);
			Color result = Color.HSVToRGB(h, s, v, true);
			result.a = Mathf.Lerp(alphaMin, alphaMax, Random.value);
			return result;
		}

		// Token: 0x06000FB4 RID: 4020
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_state_Injected(out Random.State ret);

		// Token: 0x06000FB5 RID: 4021
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_state_Injected(ref Random.State value);

		// Token: 0x06000FB6 RID: 4022
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_insideUnitSphere_Injected(out Vector3 ret);

		// Token: 0x06000FB7 RID: 4023
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_onUnitSphere_Injected(out Vector3 ret);

		// Token: 0x06000FB8 RID: 4024
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_rotation_Injected(out Quaternion ret);

		// Token: 0x06000FB9 RID: 4025
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_rotationUniform_Injected(out Quaternion ret);

		// Token: 0x020001F5 RID: 501
		[Serializable]
		public struct State
		{
			// Token: 0x04000730 RID: 1840
			[SerializeField]
			private int s0;

			// Token: 0x04000731 RID: 1841
			[SerializeField]
			private int s1;

			// Token: 0x04000732 RID: 1842
			[SerializeField]
			private int s2;

			// Token: 0x04000733 RID: 1843
			[SerializeField]
			private int s3;
		}
	}
}
