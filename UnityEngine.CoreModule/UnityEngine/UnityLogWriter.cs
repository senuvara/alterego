﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000252 RID: 594
	[NativeHeader("Runtime/Export/UnityLogWriter.bindings.h")]
	internal class UnityLogWriter : TextWriter
	{
		// Token: 0x06001560 RID: 5472 RVA: 0x00024F85 File Offset: 0x00023185
		public UnityLogWriter()
		{
		}

		// Token: 0x06001561 RID: 5473 RVA: 0x00024F8D File Offset: 0x0002318D
		[ThreadAndSerializationSafe]
		public static void WriteStringToUnityLog(string s)
		{
			if (s != null)
			{
				UnityLogWriter.WriteStringToUnityLogImpl(s);
			}
		}

		// Token: 0x06001562 RID: 5474
		[FreeFunction(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void WriteStringToUnityLogImpl(string s);

		// Token: 0x06001563 RID: 5475 RVA: 0x00024FA1 File Offset: 0x000231A1
		public static void Init()
		{
			Console.SetOut(new UnityLogWriter());
		}

		// Token: 0x17000414 RID: 1044
		// (get) Token: 0x06001564 RID: 5476 RVA: 0x00024FB0 File Offset: 0x000231B0
		public override Encoding Encoding
		{
			get
			{
				return Encoding.UTF8;
			}
		}

		// Token: 0x06001565 RID: 5477 RVA: 0x00024FCA File Offset: 0x000231CA
		public override void Write(char value)
		{
			UnityLogWriter.WriteStringToUnityLog(value.ToString());
		}

		// Token: 0x06001566 RID: 5478 RVA: 0x00024FDF File Offset: 0x000231DF
		public override void Write(string s)
		{
			UnityLogWriter.WriteStringToUnityLog(s);
		}

		// Token: 0x06001567 RID: 5479 RVA: 0x00024FE8 File Offset: 0x000231E8
		public override void Write(char[] buffer, int index, int count)
		{
			UnityLogWriter.WriteStringToUnityLogImpl(new string(buffer, index, count));
		}
	}
}
