﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B3 RID: 179
	[Flags]
	public enum ComputeBufferType
	{
		// Token: 0x040001C5 RID: 453
		Default = 0,
		// Token: 0x040001C6 RID: 454
		Raw = 1,
		// Token: 0x040001C7 RID: 455
		Append = 2,
		// Token: 0x040001C8 RID: 456
		Counter = 4,
		// Token: 0x040001C9 RID: 457
		[Obsolete("Enum member DrawIndirect has been deprecated. Use IndirectArguments instead (UnityUpgradable) -> IndirectArguments", false)]
		DrawIndirect = 256,
		// Token: 0x040001CA RID: 458
		IndirectArguments = 256,
		// Token: 0x040001CB RID: 459
		[Obsolete("Enum member GPUMemory has been deprecated. All compute buffers now follow the behavior previously defined by this member.", false)]
		GPUMemory = 512
	}
}
