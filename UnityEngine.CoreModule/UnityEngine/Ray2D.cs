﻿using System;

namespace UnityEngine
{
	// Token: 0x020001F8 RID: 504
	public struct Ray2D
	{
		// Token: 0x06000FC4 RID: 4036 RVA: 0x0001B3FF File Offset: 0x000195FF
		public Ray2D(Vector2 origin, Vector2 direction)
		{
			this.m_Origin = origin;
			this.m_Direction = direction.normalized;
		}

		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x06000FC5 RID: 4037 RVA: 0x0001B418 File Offset: 0x00019618
		// (set) Token: 0x06000FC6 RID: 4038 RVA: 0x0001B433 File Offset: 0x00019633
		public Vector2 origin
		{
			get
			{
				return this.m_Origin;
			}
			set
			{
				this.m_Origin = value;
			}
		}

		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x06000FC7 RID: 4039 RVA: 0x0001B440 File Offset: 0x00019640
		// (set) Token: 0x06000FC8 RID: 4040 RVA: 0x0001B45B File Offset: 0x0001965B
		public Vector2 direction
		{
			get
			{
				return this.m_Direction;
			}
			set
			{
				this.m_Direction = value.normalized;
			}
		}

		// Token: 0x06000FC9 RID: 4041 RVA: 0x0001B46C File Offset: 0x0001966C
		public Vector2 GetPoint(float distance)
		{
			return this.m_Origin + this.m_Direction * distance;
		}

		// Token: 0x06000FCA RID: 4042 RVA: 0x0001B498 File Offset: 0x00019698
		public override string ToString()
		{
			return UnityString.Format("Origin: {0}, Dir: {1}", new object[]
			{
				this.m_Origin,
				this.m_Direction
			});
		}

		// Token: 0x06000FCB RID: 4043 RVA: 0x0001B4DC File Offset: 0x000196DC
		public string ToString(string format)
		{
			return UnityString.Format("Origin: {0}, Dir: {1}", new object[]
			{
				this.m_Origin.ToString(format),
				this.m_Direction.ToString(format)
			});
		}

		// Token: 0x04000738 RID: 1848
		private Vector2 m_Origin;

		// Token: 0x04000739 RID: 1849
		private Vector2 m_Direction;
	}
}
