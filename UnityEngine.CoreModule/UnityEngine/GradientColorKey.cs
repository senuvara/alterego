﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200007C RID: 124
	[UsedByNativeCode]
	public struct GradientColorKey
	{
		// Token: 0x0600065A RID: 1626 RVA: 0x0000E52A File Offset: 0x0000C72A
		public GradientColorKey(Color col, float time)
		{
			this.color = col;
			this.time = time;
		}

		// Token: 0x04000151 RID: 337
		public Color color;

		// Token: 0x04000152 RID: 338
		public float time;
	}
}
