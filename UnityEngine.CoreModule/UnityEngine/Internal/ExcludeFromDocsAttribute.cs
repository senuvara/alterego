﻿using System;

namespace UnityEngine.Internal
{
	// Token: 0x020002B6 RID: 694
	[Serializable]
	public class ExcludeFromDocsAttribute : Attribute
	{
		// Token: 0x060018BC RID: 6332 RVA: 0x000086CD File Offset: 0x000068CD
		public ExcludeFromDocsAttribute()
		{
		}
	}
}
