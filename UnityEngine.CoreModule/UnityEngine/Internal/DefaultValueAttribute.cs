﻿using System;

namespace UnityEngine.Internal
{
	// Token: 0x020002B5 RID: 693
	[AttributeUsage(AttributeTargets.Parameter | AttributeTargets.GenericParameter)]
	[Serializable]
	public class DefaultValueAttribute : Attribute
	{
		// Token: 0x060018B8 RID: 6328 RVA: 0x0002B600 File Offset: 0x00029800
		public DefaultValueAttribute(string value)
		{
			this.DefaultValue = value;
		}

		// Token: 0x17000476 RID: 1142
		// (get) Token: 0x060018B9 RID: 6329 RVA: 0x0002B610 File Offset: 0x00029810
		public object Value
		{
			get
			{
				return this.DefaultValue;
			}
		}

		// Token: 0x060018BA RID: 6330 RVA: 0x0002B62C File Offset: 0x0002982C
		public override bool Equals(object obj)
		{
			DefaultValueAttribute defaultValueAttribute = obj as DefaultValueAttribute;
			bool result;
			if (defaultValueAttribute == null)
			{
				result = false;
			}
			else if (this.DefaultValue == null)
			{
				result = (defaultValueAttribute.Value == null);
			}
			else
			{
				result = this.DefaultValue.Equals(defaultValueAttribute.Value);
			}
			return result;
		}

		// Token: 0x060018BB RID: 6331 RVA: 0x0002B680 File Offset: 0x00029880
		public override int GetHashCode()
		{
			int hashCode;
			if (this.DefaultValue == null)
			{
				hashCode = base.GetHashCode();
			}
			else
			{
				hashCode = this.DefaultValue.GetHashCode();
			}
			return hashCode;
		}

		// Token: 0x040008FA RID: 2298
		private object DefaultValue;
	}
}
