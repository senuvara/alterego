﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.Internal
{
	// Token: 0x020002B7 RID: 695
	[VisibleToOtherModules]
	internal interface ISubAssetNotDuplicatable
	{
	}
}
