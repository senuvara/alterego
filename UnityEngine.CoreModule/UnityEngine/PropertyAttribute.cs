﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001E5 RID: 485
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public abstract class PropertyAttribute : Attribute
	{
		// Token: 0x06000F7A RID: 3962 RVA: 0x0000898B File Offset: 0x00006B8B
		protected PropertyAttribute()
		{
		}

		// Token: 0x170002EC RID: 748
		// (get) Token: 0x06000F7B RID: 3963 RVA: 0x0001AC9C File Offset: 0x00018E9C
		// (set) Token: 0x06000F7C RID: 3964 RVA: 0x0001ACB6 File Offset: 0x00018EB6
		public int order
		{
			[CompilerGenerated]
			get
			{
				return this.<order>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<order>k__BackingField = value;
			}
		}

		// Token: 0x0400071B RID: 1819
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <order>k__BackingField;
	}
}
