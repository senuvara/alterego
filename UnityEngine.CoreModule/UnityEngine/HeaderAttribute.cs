﻿using System;

namespace UnityEngine
{
	// Token: 0x020001EA RID: 490
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = true)]
	public class HeaderAttribute : PropertyAttribute
	{
		// Token: 0x06000F82 RID: 3970 RVA: 0x0001AD1A File Offset: 0x00018F1A
		public HeaderAttribute(string header)
		{
			this.header = header;
		}

		// Token: 0x04000721 RID: 1825
		public readonly string header;
	}
}
