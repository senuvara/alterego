﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000202 RID: 514
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class RuntimeInitializeOnLoadMethodAttribute : PreserveAttribute
	{
		// Token: 0x06001168 RID: 4456 RVA: 0x0001DC7A File Offset: 0x0001BE7A
		public RuntimeInitializeOnLoadMethodAttribute()
		{
			this.loadType = RuntimeInitializeLoadType.AfterSceneLoad;
		}

		// Token: 0x06001169 RID: 4457 RVA: 0x0001DC8A File Offset: 0x0001BE8A
		public RuntimeInitializeOnLoadMethodAttribute(RuntimeInitializeLoadType loadType)
		{
			this.loadType = loadType;
		}

		// Token: 0x1700033B RID: 827
		// (get) Token: 0x0600116A RID: 4458 RVA: 0x0001DC9C File Offset: 0x0001BE9C
		// (set) Token: 0x0600116B RID: 4459 RVA: 0x0001DCB6 File Offset: 0x0001BEB6
		public RuntimeInitializeLoadType loadType
		{
			[CompilerGenerated]
			get
			{
				return this.<loadType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<loadType>k__BackingField = value;
			}
		}

		// Token: 0x04000750 RID: 1872
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private RuntimeInitializeLoadType <loadType>k__BackingField;
	}
}
