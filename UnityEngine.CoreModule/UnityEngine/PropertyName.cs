﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001F3 RID: 499
	[UsedByNativeCode]
	public struct PropertyName : IEquatable<PropertyName>
	{
		// Token: 0x06000F91 RID: 3985 RVA: 0x0001AF06 File Offset: 0x00019106
		public PropertyName(string name)
		{
			this = new PropertyName(PropertyNameUtils.PropertyNameFromString(name));
		}

		// Token: 0x06000F92 RID: 3986 RVA: 0x0001AF15 File Offset: 0x00019115
		public PropertyName(PropertyName other)
		{
			this.id = other.id;
		}

		// Token: 0x06000F93 RID: 3987 RVA: 0x0001AF25 File Offset: 0x00019125
		public PropertyName(int id)
		{
			this.id = id;
		}

		// Token: 0x06000F94 RID: 3988 RVA: 0x0001AF30 File Offset: 0x00019130
		public static bool IsNullOrEmpty(PropertyName prop)
		{
			return prop.id == 0;
		}

		// Token: 0x06000F95 RID: 3989 RVA: 0x0001AF50 File Offset: 0x00019150
		public static bool operator ==(PropertyName lhs, PropertyName rhs)
		{
			return lhs.id == rhs.id;
		}

		// Token: 0x06000F96 RID: 3990 RVA: 0x0001AF78 File Offset: 0x00019178
		public static bool operator !=(PropertyName lhs, PropertyName rhs)
		{
			return lhs.id != rhs.id;
		}

		// Token: 0x06000F97 RID: 3991 RVA: 0x0001AFA0 File Offset: 0x000191A0
		public override int GetHashCode()
		{
			return this.id;
		}

		// Token: 0x06000F98 RID: 3992 RVA: 0x0001AFBC File Offset: 0x000191BC
		public override bool Equals(object other)
		{
			return other is PropertyName && this.Equals((PropertyName)other);
		}

		// Token: 0x06000F99 RID: 3993 RVA: 0x0001AFEC File Offset: 0x000191EC
		public bool Equals(PropertyName other)
		{
			return this == other;
		}

		// Token: 0x06000F9A RID: 3994 RVA: 0x0001B010 File Offset: 0x00019210
		public static implicit operator PropertyName(string name)
		{
			return new PropertyName(name);
		}

		// Token: 0x06000F9B RID: 3995 RVA: 0x0001B02C File Offset: 0x0001922C
		public static implicit operator PropertyName(int id)
		{
			return new PropertyName(id);
		}

		// Token: 0x06000F9C RID: 3996 RVA: 0x0001B048 File Offset: 0x00019248
		public override string ToString()
		{
			return string.Format("Unknown:{0}", this.id);
		}

		// Token: 0x0400072F RID: 1839
		internal int id;
	}
}
