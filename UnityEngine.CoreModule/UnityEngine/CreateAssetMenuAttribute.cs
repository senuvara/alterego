﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200002B RID: 43
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public sealed class CreateAssetMenuAttribute : Attribute
	{
		// Token: 0x06000281 RID: 641 RVA: 0x0000898B File Offset: 0x00006B8B
		public CreateAssetMenuAttribute()
		{
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000282 RID: 642 RVA: 0x00008A40 File Offset: 0x00006C40
		// (set) Token: 0x06000283 RID: 643 RVA: 0x00008A5A File Offset: 0x00006C5A
		public string menuName
		{
			[CompilerGenerated]
			get
			{
				return this.<menuName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<menuName>k__BackingField = value;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000284 RID: 644 RVA: 0x00008A64 File Offset: 0x00006C64
		// (set) Token: 0x06000285 RID: 645 RVA: 0x00008A7E File Offset: 0x00006C7E
		public string fileName
		{
			[CompilerGenerated]
			get
			{
				return this.<fileName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<fileName>k__BackingField = value;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000286 RID: 646 RVA: 0x00008A88 File Offset: 0x00006C88
		// (set) Token: 0x06000287 RID: 647 RVA: 0x00008AA2 File Offset: 0x00006CA2
		public int order
		{
			[CompilerGenerated]
			get
			{
				return this.<order>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<order>k__BackingField = value;
			}
		}

		// Token: 0x04000067 RID: 103
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <menuName>k__BackingField;

		// Token: 0x04000068 RID: 104
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <fileName>k__BackingField;

		// Token: 0x04000069 RID: 105
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <order>k__BackingField;
	}
}
