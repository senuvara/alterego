﻿using System;

namespace UnityEngine
{
	// Token: 0x020000D2 RID: 210
	[Flags]
	public enum RenderTextureMemoryless
	{
		// Token: 0x040002B3 RID: 691
		None = 0,
		// Token: 0x040002B4 RID: 692
		Color = 1,
		// Token: 0x040002B5 RID: 693
		Depth = 2,
		// Token: 0x040002B6 RID: 694
		MSAA = 4
	}
}
