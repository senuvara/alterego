﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000227 RID: 551
	[UsedByNativeCode]
	[NativeHeader("Runtime/Graphics/GeneratedTextures.h")]
	[NativeHeader("Runtime/Graphics/Texture2D.h")]
	public sealed class Texture2D : Texture
	{
		// Token: 0x060012FC RID: 4860 RVA: 0x000213AA File Offset: 0x0001F5AA
		internal Texture2D(int width, int height, GraphicsFormat format, TextureCreationFlags flags, IntPtr nativeTex)
		{
			if (base.ValidateFormat(format, FormatUsage.Sample))
			{
				Texture2D.Internal_Create(this, width, height, format, flags, nativeTex);
			}
		}

		// Token: 0x060012FD RID: 4861 RVA: 0x000213CD File Offset: 0x0001F5CD
		public Texture2D(int width, int height, GraphicsFormat format, TextureCreationFlags flags) : this(width, height, format, flags, IntPtr.Zero)
		{
		}

		// Token: 0x060012FE RID: 4862 RVA: 0x000213E0 File Offset: 0x0001F5E0
		internal Texture2D(int width, int height, TextureFormat textureFormat, bool mipChain, bool linear, IntPtr nativeTex)
		{
			if (base.ValidateFormat(textureFormat))
			{
				GraphicsFormat graphicsFormat = GraphicsFormatUtility.GetGraphicsFormat(textureFormat, !linear);
				TextureCreationFlags textureCreationFlags = TextureCreationFlags.None;
				if (mipChain)
				{
					textureCreationFlags |= TextureCreationFlags.MipChain;
				}
				if (GraphicsFormatUtility.IsCrunchFormat(textureFormat))
				{
					textureCreationFlags |= TextureCreationFlags.Crunch;
				}
				Texture2D.Internal_Create(this, width, height, graphicsFormat, textureCreationFlags, nativeTex);
			}
		}

		// Token: 0x060012FF RID: 4863 RVA: 0x0002143A File Offset: 0x0001F63A
		public Texture2D(int width, int height, [DefaultValue("TextureFormat.RGBA32")] TextureFormat textureFormat, [DefaultValue("true")] bool mipChain, [DefaultValue("false")] bool linear) : this(width, height, textureFormat, mipChain, linear, IntPtr.Zero)
		{
		}

		// Token: 0x06001300 RID: 4864 RVA: 0x0002144F File Offset: 0x0001F64F
		public Texture2D(int width, int height, TextureFormat textureFormat, bool mipChain) : this(width, height, textureFormat, mipChain, false, IntPtr.Zero)
		{
		}

		// Token: 0x06001301 RID: 4865 RVA: 0x00021463 File Offset: 0x0001F663
		public Texture2D(int width, int height) : this(width, height, TextureFormat.RGBA32, true, false, IntPtr.Zero)
		{
		}

		// Token: 0x170003A3 RID: 931
		// (get) Token: 0x06001302 RID: 4866
		public extern int mipmapCount { [NativeName("CountDataMipmaps")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003A4 RID: 932
		// (get) Token: 0x06001303 RID: 4867
		public extern TextureFormat format { [NativeName("GetTextureFormat")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003A5 RID: 933
		// (get) Token: 0x06001304 RID: 4868
		[StaticAccessor("builtintex", StaticAccessorType.DoubleColon)]
		public static extern Texture2D whiteTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003A6 RID: 934
		// (get) Token: 0x06001305 RID: 4869
		[StaticAccessor("builtintex", StaticAccessorType.DoubleColon)]
		public static extern Texture2D blackTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001306 RID: 4870
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Compress(bool highQuality);

		// Token: 0x06001307 RID: 4871
		[FreeFunction("Texture2DScripting::Create")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_CreateImpl([Writable] Texture2D mono, int w, int h, GraphicsFormat format, TextureCreationFlags flags, IntPtr nativeTex);

		// Token: 0x06001308 RID: 4872 RVA: 0x00021476 File Offset: 0x0001F676
		private static void Internal_Create([Writable] Texture2D mono, int w, int h, GraphicsFormat format, TextureCreationFlags flags, IntPtr nativeTex)
		{
			if (!Texture2D.Internal_CreateImpl(mono, w, h, format, flags, nativeTex))
			{
				throw new UnityException("Failed to create texture because of invalid parameters.");
			}
		}

		// Token: 0x170003A7 RID: 935
		// (get) Token: 0x06001309 RID: 4873
		public override extern bool isReadable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600130A RID: 4874
		[NativeName("Apply")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ApplyImpl(bool updateMipmaps, bool makeNoLongerReadable);

		// Token: 0x0600130B RID: 4875
		[NativeName("Resize")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool ResizeImpl(int width, int height);

		// Token: 0x0600130C RID: 4876 RVA: 0x00021496 File Offset: 0x0001F696
		[NativeName("SetPixel")]
		private void SetPixelImpl(int image, int x, int y, Color color)
		{
			this.SetPixelImpl_Injected(image, x, y, ref color);
		}

		// Token: 0x0600130D RID: 4877 RVA: 0x000214A4 File Offset: 0x0001F6A4
		[NativeName("GetPixel")]
		private Color GetPixelImpl(int image, int x, int y)
		{
			Color result;
			this.GetPixelImpl_Injected(image, x, y, out result);
			return result;
		}

		// Token: 0x0600130E RID: 4878 RVA: 0x000214C0 File Offset: 0x0001F6C0
		[NativeName("GetPixelBilinear")]
		private Color GetPixelBilinearImpl(int image, float x, float y)
		{
			Color result;
			this.GetPixelBilinearImpl_Injected(image, x, y, out result);
			return result;
		}

		// Token: 0x0600130F RID: 4879
		[FreeFunction(Name = "Texture2DScripting::ResizeWithFormat", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool ResizeWithFormatImpl(int width, int height, TextureFormat format, bool hasMipMap);

		// Token: 0x06001310 RID: 4880 RVA: 0x000214D9 File Offset: 0x0001F6D9
		[FreeFunction(Name = "Texture2DScripting::ReadPixels", HasExplicitThis = true)]
		private void ReadPixelsImpl(Rect source, int destX, int destY, bool recalculateMipMaps)
		{
			this.ReadPixelsImpl_Injected(ref source, destX, destY, recalculateMipMaps);
		}

		// Token: 0x06001311 RID: 4881
		[FreeFunction(Name = "Texture2DScripting::SetPixels", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetPixelsImpl(int x, int y, int w, int h, Color[] pixel, int miplevel, int frame);

		// Token: 0x06001312 RID: 4882
		[FreeFunction(Name = "Texture2DScripting::LoadRawData", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool LoadRawTextureDataImpl(IntPtr data, int size);

		// Token: 0x06001313 RID: 4883
		[FreeFunction(Name = "Texture2DScripting::LoadRawData", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool LoadRawTextureDataImplArray(byte[] data);

		// Token: 0x06001314 RID: 4884
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern IntPtr GetWritableImageData(int frame);

		// Token: 0x06001315 RID: 4885
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern long GetRawImageDataSize();

		// Token: 0x06001316 RID: 4886
		[FreeFunction("Texture2DScripting::GenerateAtlas")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GenerateAtlasImpl(Vector2[] sizes, int padding, int atlasSize, [Out] Rect[] rect);

		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x06001317 RID: 4887
		public extern bool streamingMipmaps { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x06001318 RID: 4888
		public extern int streamingMipmapsPriority { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003AA RID: 938
		// (get) Token: 0x06001319 RID: 4889
		// (set) Token: 0x0600131A RID: 4890
		public extern int requestedMipmapLevel { [FreeFunction(Name = "GetTextureStreamingManager().GetRequestedMipmapLevel", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction(Name = "GetTextureStreamingManager().SetRequestedMipmapLevel", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003AB RID: 939
		// (get) Token: 0x0600131B RID: 4891
		public extern int desiredMipmapLevel { [FreeFunction(Name = "GetTextureStreamingManager().GetDesiredMipmapLevel", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003AC RID: 940
		// (get) Token: 0x0600131C RID: 4892
		public extern int loadingMipmapLevel { [FreeFunction(Name = "GetTextureStreamingManager().GetLoadingMipmapLevel", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003AD RID: 941
		// (get) Token: 0x0600131D RID: 4893
		public extern int loadedMipmapLevel { [FreeFunction(Name = "GetTextureStreamingManager().GetLoadedMipmapLevel", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600131E RID: 4894
		[FreeFunction(Name = "GetTextureStreamingManager().ClearRequestedMipmapLevel", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ClearRequestedMipmapLevel();

		// Token: 0x0600131F RID: 4895
		[FreeFunction(Name = "GetTextureStreamingManager().IsRequestedMipmapLevelLoaded", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsRequestedMipmapLevelLoaded();

		// Token: 0x06001320 RID: 4896
		[FreeFunction("Texture2DScripting::UpdateExternalTexture", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UpdateExternalTexture(IntPtr nativeTex);

		// Token: 0x06001321 RID: 4897
		[FreeFunction("Texture2DScripting::SetAllPixels32", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetAllPixels32(Color32[] colors, int miplevel);

		// Token: 0x06001322 RID: 4898
		[FreeFunction("Texture2DScripting::SetBlockOfPixels32", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetBlockOfPixels32(int x, int y, int blockWidth, int blockHeight, Color32[] colors, int miplevel);

		// Token: 0x06001323 RID: 4899
		[FreeFunction("Texture2DScripting::GetRawTextureData", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern byte[] GetRawTextureData();

		// Token: 0x06001324 RID: 4900
		[FreeFunction("Texture2DScripting::GetPixels", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color[] GetPixels(int x, int y, int blockWidth, int blockHeight, int miplevel);

		// Token: 0x06001325 RID: 4901 RVA: 0x000214E8 File Offset: 0x0001F6E8
		public Color[] GetPixels(int x, int y, int blockWidth, int blockHeight)
		{
			return this.GetPixels(x, y, blockWidth, blockHeight, 0);
		}

		// Token: 0x06001326 RID: 4902
		[FreeFunction("Texture2DScripting::GetPixels32", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color32[] GetPixels32(int miplevel);

		// Token: 0x06001327 RID: 4903 RVA: 0x0002150C File Offset: 0x0001F70C
		public Color32[] GetPixels32()
		{
			return this.GetPixels32(0);
		}

		// Token: 0x06001328 RID: 4904
		[FreeFunction("Texture2DScripting::PackTextures", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Rect[] PackTextures(Texture2D[] textures, int padding, int maximumAtlasSize, bool makeNoLongerReadable);

		// Token: 0x06001329 RID: 4905 RVA: 0x00021528 File Offset: 0x0001F728
		public Rect[] PackTextures(Texture2D[] textures, int padding, int maximumAtlasSize)
		{
			return this.PackTextures(textures, padding, maximumAtlasSize, false);
		}

		// Token: 0x0600132A RID: 4906 RVA: 0x00021548 File Offset: 0x0001F748
		public Rect[] PackTextures(Texture2D[] textures, int padding)
		{
			return this.PackTextures(textures, padding, 2048);
		}

		// Token: 0x0600132B RID: 4907 RVA: 0x0002156C File Offset: 0x0001F76C
		public static Texture2D CreateExternalTexture(int width, int height, TextureFormat format, bool mipChain, bool linear, IntPtr nativeTex)
		{
			if (nativeTex == IntPtr.Zero)
			{
				throw new ArgumentException("nativeTex can not be null");
			}
			return new Texture2D(width, height, format, mipChain, linear, nativeTex);
		}

		// Token: 0x0600132C RID: 4908 RVA: 0x000215AA File Offset: 0x0001F7AA
		public void SetPixel(int x, int y, Color color)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			this.SetPixelImpl(0, x, y, color);
		}

		// Token: 0x0600132D RID: 4909 RVA: 0x000215CA File Offset: 0x0001F7CA
		public void SetPixels(int x, int y, int blockWidth, int blockHeight, Color[] colors, [DefaultValue("0")] int miplevel)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			this.SetPixelsImpl(x, y, blockWidth, blockHeight, colors, miplevel, 0);
		}

		// Token: 0x0600132E RID: 4910 RVA: 0x000215F0 File Offset: 0x0001F7F0
		public void SetPixels(int x, int y, int blockWidth, int blockHeight, Color[] colors)
		{
			this.SetPixels(x, y, blockWidth, blockHeight, colors, 0);
		}

		// Token: 0x0600132F RID: 4911 RVA: 0x00021604 File Offset: 0x0001F804
		public void SetPixels(Color[] colors, [DefaultValue("0")] int miplevel)
		{
			int num = this.width >> miplevel;
			if (num < 1)
			{
				num = 1;
			}
			int num2 = this.height >> miplevel;
			if (num2 < 1)
			{
				num2 = 1;
			}
			this.SetPixels(0, 0, num, num2, colors, miplevel);
		}

		// Token: 0x06001330 RID: 4912 RVA: 0x00021648 File Offset: 0x0001F848
		public void SetPixels(Color[] colors)
		{
			this.SetPixels(0, 0, this.width, this.height, colors, 0);
		}

		// Token: 0x06001331 RID: 4913 RVA: 0x00021664 File Offset: 0x0001F864
		public Color GetPixel(int x, int y)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			return this.GetPixelImpl(0, x, y);
		}

		// Token: 0x06001332 RID: 4914 RVA: 0x00021698 File Offset: 0x0001F898
		public Color GetPixelBilinear(float x, float y)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			return this.GetPixelBilinearImpl(0, x, y);
		}

		// Token: 0x06001333 RID: 4915 RVA: 0x000216CC File Offset: 0x0001F8CC
		public void LoadRawTextureData(IntPtr data, int size)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			if (data == IntPtr.Zero || size == 0)
			{
				Debug.LogError("No texture data provided to LoadRawTextureData", this);
			}
			else if (!this.LoadRawTextureDataImpl(data, size))
			{
				throw new UnityException("LoadRawTextureData: not enough data provided (will result in overread).");
			}
		}

		// Token: 0x06001334 RID: 4916 RVA: 0x0002172C File Offset: 0x0001F92C
		public void LoadRawTextureData(byte[] data)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			if (data == null || data.Length == 0)
			{
				Debug.LogError("No texture data provided to LoadRawTextureData", this);
			}
			else if (!this.LoadRawTextureDataImplArray(data))
			{
				throw new UnityException("LoadRawTextureData: not enough data provided (will result in overread).");
			}
		}

		// Token: 0x06001335 RID: 4917 RVA: 0x00021784 File Offset: 0x0001F984
		public void LoadRawTextureData<T>(NativeArray<T> data) where T : struct
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			if (!data.IsCreated || data.Length == 0)
			{
				throw new UnityException("No texture data provided to LoadRawTextureData");
			}
			if (!this.LoadRawTextureDataImpl((IntPtr)data.GetUnsafeReadOnlyPtr<T>(), data.Length * UnsafeUtility.SizeOf<T>()))
			{
				throw new UnityException("LoadRawTextureData: not enough data provided (will result in overread).");
			}
		}

		// Token: 0x06001336 RID: 4918 RVA: 0x000217F8 File Offset: 0x0001F9F8
		public unsafe NativeArray<T> GetRawTextureData<T>() where T : struct
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			int num = UnsafeUtility.SizeOf<T>();
			return NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<T>((void*)this.GetWritableImageData(0), (int)(this.GetRawImageDataSize() / (long)num), Allocator.None);
		}

		// Token: 0x06001337 RID: 4919 RVA: 0x00021844 File Offset: 0x0001FA44
		public void Apply([DefaultValue("true")] bool updateMipmaps, [DefaultValue("false")] bool makeNoLongerReadable)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			this.ApplyImpl(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x06001338 RID: 4920 RVA: 0x00021862 File Offset: 0x0001FA62
		public void Apply(bool updateMipmaps)
		{
			this.Apply(updateMipmaps, false);
		}

		// Token: 0x06001339 RID: 4921 RVA: 0x0002186D File Offset: 0x0001FA6D
		public void Apply()
		{
			this.Apply(true, false);
		}

		// Token: 0x0600133A RID: 4922 RVA: 0x00021878 File Offset: 0x0001FA78
		public bool Resize(int width, int height)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			return this.ResizeImpl(width, height);
		}

		// Token: 0x0600133B RID: 4923 RVA: 0x000218A8 File Offset: 0x0001FAA8
		public bool Resize(int width, int height, TextureFormat format, bool hasMipMap)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			return this.ResizeWithFormatImpl(width, height, format, hasMipMap);
		}

		// Token: 0x0600133C RID: 4924 RVA: 0x000218DB File Offset: 0x0001FADB
		public void ReadPixels(Rect source, int destX, int destY, [DefaultValue("true")] bool recalculateMipMaps)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			this.ReadPixelsImpl(source, destX, destY, recalculateMipMaps);
		}

		// Token: 0x0600133D RID: 4925 RVA: 0x000218FC File Offset: 0x0001FAFC
		[ExcludeFromDocs]
		public void ReadPixels(Rect source, int destX, int destY)
		{
			this.ReadPixels(source, destX, destY, true);
		}

		// Token: 0x0600133E RID: 4926 RVA: 0x0002190C File Offset: 0x0001FB0C
		public static bool GenerateAtlas(Vector2[] sizes, int padding, int atlasSize, List<Rect> results)
		{
			if (sizes == null)
			{
				throw new ArgumentException("sizes array can not be null");
			}
			if (results == null)
			{
				throw new ArgumentException("results list cannot be null");
			}
			if (padding < 0)
			{
				throw new ArgumentException("padding can not be negative");
			}
			if (atlasSize <= 0)
			{
				throw new ArgumentException("atlas size must be positive");
			}
			results.Clear();
			bool result;
			if (sizes.Length == 0)
			{
				result = true;
			}
			else
			{
				NoAllocHelpers.EnsureListElemCount<Rect>(results, sizes.Length);
				Texture2D.GenerateAtlasImpl(sizes, padding, atlasSize, NoAllocHelpers.ExtractArrayFromListT<Rect>(results));
				result = (results.Count != 0);
			}
			return result;
		}

		// Token: 0x0600133F RID: 4927 RVA: 0x0002199F File Offset: 0x0001FB9F
		public void SetPixels32(Color32[] colors, int miplevel)
		{
			this.SetAllPixels32(colors, miplevel);
		}

		// Token: 0x06001340 RID: 4928 RVA: 0x000219AA File Offset: 0x0001FBAA
		public void SetPixels32(Color32[] colors)
		{
			this.SetPixels32(colors, 0);
		}

		// Token: 0x06001341 RID: 4929 RVA: 0x000219B5 File Offset: 0x0001FBB5
		public void SetPixels32(int x, int y, int blockWidth, int blockHeight, Color32[] colors, int miplevel)
		{
			this.SetBlockOfPixels32(x, y, blockWidth, blockHeight, colors, miplevel);
		}

		// Token: 0x06001342 RID: 4930 RVA: 0x000219C7 File Offset: 0x0001FBC7
		public void SetPixels32(int x, int y, int blockWidth, int blockHeight, Color32[] colors)
		{
			this.SetPixels32(x, y, blockWidth, blockHeight, colors, 0);
		}

		// Token: 0x06001343 RID: 4931 RVA: 0x000219D8 File Offset: 0x0001FBD8
		public Color[] GetPixels(int miplevel)
		{
			int num = this.width >> miplevel;
			if (num < 1)
			{
				num = 1;
			}
			int num2 = this.height >> miplevel;
			if (num2 < 1)
			{
				num2 = 1;
			}
			return this.GetPixels(0, 0, num, num2, miplevel);
		}

		// Token: 0x06001344 RID: 4932 RVA: 0x00021A24 File Offset: 0x0001FC24
		public Color[] GetPixels()
		{
			return this.GetPixels(0);
		}

		// Token: 0x06001345 RID: 4933
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetPixelImpl_Injected(int image, int x, int y, ref Color color);

		// Token: 0x06001346 RID: 4934
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetPixelImpl_Injected(int image, int x, int y, out Color ret);

		// Token: 0x06001347 RID: 4935
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetPixelBilinearImpl_Injected(int image, float x, float y, out Color ret);

		// Token: 0x06001348 RID: 4936
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ReadPixelsImpl_Injected(ref Rect source, int destX, int destY, bool recalculateMipMaps);

		// Token: 0x02000228 RID: 552
		[Flags]
		public enum EXRFlags
		{
			// Token: 0x040007AB RID: 1963
			None = 0,
			// Token: 0x040007AC RID: 1964
			OutputAsFloat = 1,
			// Token: 0x040007AD RID: 1965
			CompressZIP = 2,
			// Token: 0x040007AE RID: 1966
			CompressRLE = 4,
			// Token: 0x040007AF RID: 1967
			CompressPIZ = 8
		}
	}
}
