﻿using System;

namespace UnityEngine
{
	// Token: 0x0200012C RID: 300
	public class Compass
	{
		// Token: 0x06000C6D RID: 3181 RVA: 0x00002370 File Offset: 0x00000570
		public Compass()
		{
		}

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x06000C6E RID: 3182 RVA: 0x00013D70 File Offset: 0x00011F70
		public float magneticHeading
		{
			get
			{
				return LocationService.GetLastHeading().magneticHeading;
			}
		}

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x06000C6F RID: 3183 RVA: 0x00013D94 File Offset: 0x00011F94
		public float trueHeading
		{
			get
			{
				return LocationService.GetLastHeading().trueHeading;
			}
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x06000C70 RID: 3184 RVA: 0x00013DB8 File Offset: 0x00011FB8
		public float headingAccuracy
		{
			get
			{
				return LocationService.GetLastHeading().headingAccuracy;
			}
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x06000C71 RID: 3185 RVA: 0x00013DDC File Offset: 0x00011FDC
		public Vector3 rawVector
		{
			get
			{
				return LocationService.GetLastHeading().raw;
			}
		}

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x06000C72 RID: 3186 RVA: 0x00013E00 File Offset: 0x00012000
		public double timestamp
		{
			get
			{
				return LocationService.GetLastHeading().timestamp;
			}
		}

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x06000C73 RID: 3187 RVA: 0x00013E24 File Offset: 0x00012024
		// (set) Token: 0x06000C74 RID: 3188 RVA: 0x00013E3E File Offset: 0x0001203E
		public bool enabled
		{
			get
			{
				return LocationService.IsHeadingUpdatesEnabled();
			}
			set
			{
				LocationService.SetHeadingUpdatesEnabled(value);
			}
		}
	}
}
