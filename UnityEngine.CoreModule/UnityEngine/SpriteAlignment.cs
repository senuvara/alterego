﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035F RID: 863
	public enum SpriteAlignment
	{
		// Token: 0x04000AEF RID: 2799
		Center,
		// Token: 0x04000AF0 RID: 2800
		TopLeft,
		// Token: 0x04000AF1 RID: 2801
		TopCenter,
		// Token: 0x04000AF2 RID: 2802
		TopRight,
		// Token: 0x04000AF3 RID: 2803
		LeftCenter,
		// Token: 0x04000AF4 RID: 2804
		RightCenter,
		// Token: 0x04000AF5 RID: 2805
		BottomLeft,
		// Token: 0x04000AF6 RID: 2806
		BottomCenter,
		// Token: 0x04000AF7 RID: 2807
		BottomRight,
		// Token: 0x04000AF8 RID: 2808
		Custom
	}
}
