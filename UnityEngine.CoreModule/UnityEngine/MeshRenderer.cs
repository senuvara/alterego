﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000114 RID: 276
	[NativeHeader("Runtime/Graphics/Mesh/MeshRenderer.h")]
	public class MeshRenderer : Renderer
	{
		// Token: 0x06000BAA RID: 2986 RVA: 0x00008EED File Offset: 0x000070ED
		public MeshRenderer()
		{
		}

		// Token: 0x06000BAB RID: 2987 RVA: 0x00007476 File Offset: 0x00005676
		[RequiredByNativeCode]
		private void DontStripMeshRenderer()
		{
		}

		// Token: 0x1700022A RID: 554
		// (get) Token: 0x06000BAC RID: 2988
		// (set) Token: 0x06000BAD RID: 2989
		public extern Mesh additionalVertexStreams { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x06000BAE RID: 2990
		public extern int subMeshStartIndex { [NativeName("GetSubMeshStartIndex")] [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
