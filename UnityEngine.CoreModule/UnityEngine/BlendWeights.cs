﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C3 RID: 195
	public enum BlendWeights
	{
		// Token: 0x04000214 RID: 532
		OneBone = 1,
		// Token: 0x04000215 RID: 533
		TwoBones,
		// Token: 0x04000216 RID: 534
		FourBones = 4
	}
}
