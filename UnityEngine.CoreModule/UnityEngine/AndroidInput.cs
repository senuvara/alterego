﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000002 RID: 2
	[NativeHeader("PlatformDependent/AndroidPlayer/Source/AndroidInput.h")]
	[NativeHeader("PlatformDependent/AndroidPlayer/Source/TouchInput.h")]
	[NativeHeader("Runtime/Input/GetInput.h")]
	public class AndroidInput
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		private AndroidInput()
		{
		}

		// Token: 0x06000002 RID: 2 RVA: 0x0000205C File Offset: 0x0000025C
		public static Touch GetSecondaryTouch(int index)
		{
			return AndroidInput.GetTouch_Bindings(index);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002078 File Offset: 0x00000278
		[NativeThrows]
		[FreeFunction]
		internal static Touch GetTouch_Bindings(int index)
		{
			Touch result;
			AndroidInput.GetTouch_Bindings_Injected(index, out result);
			return result;
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000004 RID: 4 RVA: 0x00002090 File Offset: 0x00000290
		public static int touchCountSecondary
		{
			get
			{
				return AndroidInput.GetTouchCount_Bindings();
			}
		}

		// Token: 0x06000005 RID: 5
		[NativeConditional("PLATFORM_ANDROID")]
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetTouchCount_Bindings();

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000006 RID: 6 RVA: 0x000020AC File Offset: 0x000002AC
		public static bool secondaryTouchEnabled
		{
			get
			{
				return AndroidInput.IsInputDeviceEnabled_Bindings();
			}
		}

		// Token: 0x06000007 RID: 7
		[NativeConditional("PLATFORM_ANDROID")]
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool IsInputDeviceEnabled_Bindings();

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000008 RID: 8 RVA: 0x000020C8 File Offset: 0x000002C8
		public static int secondaryTouchWidth
		{
			get
			{
				return AndroidInput.GetTouchpadWidth();
			}
		}

		// Token: 0x06000009 RID: 9
		[FreeFunction]
		[NativeConditional("PLATFORM_ANDROID")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetTouchpadWidth();

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600000A RID: 10 RVA: 0x000020E4 File Offset: 0x000002E4
		public static int secondaryTouchHeight
		{
			get
			{
				return AndroidInput.GetTouchpadHeight();
			}
		}

		// Token: 0x0600000B RID: 11
		[NativeConditional("PLATFORM_ANDROID")]
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetTouchpadHeight();

		// Token: 0x0600000C RID: 12
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetTouch_Bindings_Injected(int index, out Touch ret);
	}
}
