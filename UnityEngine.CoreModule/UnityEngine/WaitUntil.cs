﻿using System;

namespace UnityEngine
{
	// Token: 0x0200025D RID: 605
	public sealed class WaitUntil : CustomYieldInstruction
	{
		// Token: 0x0600162B RID: 5675 RVA: 0x00027A97 File Offset: 0x00025C97
		public WaitUntil(Func<bool> predicate)
		{
			this.m_Predicate = predicate;
		}

		// Token: 0x17000442 RID: 1090
		// (get) Token: 0x0600162C RID: 5676 RVA: 0x00027AA8 File Offset: 0x00025CA8
		public override bool keepWaiting
		{
			get
			{
				return !this.m_Predicate();
			}
		}

		// Token: 0x04000839 RID: 2105
		private Func<bool> m_Predicate;
	}
}
