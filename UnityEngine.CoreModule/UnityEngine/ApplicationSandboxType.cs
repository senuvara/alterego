﻿using System;

namespace UnityEngine
{
	// Token: 0x0200001D RID: 29
	public enum ApplicationSandboxType
	{
		// Token: 0x04000051 RID: 81
		Unknown,
		// Token: 0x04000052 RID: 82
		NotSandboxed,
		// Token: 0x04000053 RID: 83
		Sandboxed,
		// Token: 0x04000054 RID: 84
		SandboxBroken
	}
}
