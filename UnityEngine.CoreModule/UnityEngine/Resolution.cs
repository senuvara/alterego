﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000092 RID: 146
	[RequiredByNativeCode]
	public struct Resolution
	{
		// Token: 0x17000143 RID: 323
		// (get) Token: 0x060007A5 RID: 1957 RVA: 0x0000FF08 File Offset: 0x0000E108
		// (set) Token: 0x060007A6 RID: 1958 RVA: 0x0000FF23 File Offset: 0x0000E123
		public int width
		{
			get
			{
				return this.m_Width;
			}
			set
			{
				this.m_Width = value;
			}
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x060007A7 RID: 1959 RVA: 0x0000FF30 File Offset: 0x0000E130
		// (set) Token: 0x060007A8 RID: 1960 RVA: 0x0000FF4B File Offset: 0x0000E14B
		public int height
		{
			get
			{
				return this.m_Height;
			}
			set
			{
				this.m_Height = value;
			}
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x060007A9 RID: 1961 RVA: 0x0000FF58 File Offset: 0x0000E158
		// (set) Token: 0x060007AA RID: 1962 RVA: 0x0000FF73 File Offset: 0x0000E173
		public int refreshRate
		{
			get
			{
				return this.m_RefreshRate;
			}
			set
			{
				this.m_RefreshRate = value;
			}
		}

		// Token: 0x060007AB RID: 1963 RVA: 0x0000FF80 File Offset: 0x0000E180
		public override string ToString()
		{
			return UnityString.Format("{0} x {1} @ {2}Hz", new object[]
			{
				this.m_Width,
				this.m_Height,
				this.m_RefreshRate
			});
		}

		// Token: 0x04000183 RID: 387
		private int m_Width;

		// Token: 0x04000184 RID: 388
		private int m_Height;

		// Token: 0x04000185 RID: 389
		private int m_RefreshRate;
	}
}
