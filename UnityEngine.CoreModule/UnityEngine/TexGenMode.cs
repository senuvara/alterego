﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C1 RID: 193
	public enum TexGenMode
	{
		// Token: 0x04000209 RID: 521
		None,
		// Token: 0x0400020A RID: 522
		SphereMap,
		// Token: 0x0400020B RID: 523
		Object,
		// Token: 0x0400020C RID: 524
		EyeLinear,
		// Token: 0x0400020D RID: 525
		CubeReflect,
		// Token: 0x0400020E RID: 526
		CubeNormal
	}
}
