﻿using System;

namespace UnityEngine
{
	// Token: 0x02000120 RID: 288
	public interface IExposedPropertyTable
	{
		// Token: 0x06000C1A RID: 3098
		void SetReferenceValue(PropertyName id, Object value);

		// Token: 0x06000C1B RID: 3099
		Object GetReferenceValue(PropertyName id, out bool idValid);

		// Token: 0x06000C1C RID: 3100
		void ClearReferenceValue(PropertyName id);
	}
}
