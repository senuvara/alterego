﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000146 RID: 326
	[NativeHeader("Runtime/BaseClasses/TagManager.h")]
	[NativeHeader("Runtime/BaseClasses/BitField.h")]
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	[NativeClass("BitField", "struct BitField;")]
	public struct LayerMask
	{
		// Token: 0x06000D04 RID: 3332 RVA: 0x000143A4 File Offset: 0x000125A4
		public static implicit operator int(LayerMask mask)
		{
			return mask.m_Mask;
		}

		// Token: 0x06000D05 RID: 3333 RVA: 0x000143C0 File Offset: 0x000125C0
		public static implicit operator LayerMask(int intVal)
		{
			LayerMask result;
			result.m_Mask = intVal;
			return result;
		}

		// Token: 0x1700029B RID: 667
		// (get) Token: 0x06000D06 RID: 3334 RVA: 0x000143E0 File Offset: 0x000125E0
		// (set) Token: 0x06000D07 RID: 3335 RVA: 0x000143FB File Offset: 0x000125FB
		public int value
		{
			get
			{
				return this.m_Mask;
			}
			set
			{
				this.m_Mask = value;
			}
		}

		// Token: 0x06000D08 RID: 3336
		[StaticAccessor("GetTagManager()", StaticAccessorType.Dot)]
		[NativeMethod("LayerToString")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string LayerToName(int layer);

		// Token: 0x06000D09 RID: 3337
		[StaticAccessor("GetTagManager()", StaticAccessorType.Dot)]
		[NativeMethod("StringToLayer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int NameToLayer(string layerName);

		// Token: 0x06000D0A RID: 3338 RVA: 0x00014408 File Offset: 0x00012608
		public static int GetMask(params string[] layerNames)
		{
			if (layerNames == null)
			{
				throw new ArgumentNullException("layerNames");
			}
			int num = 0;
			foreach (string layerName in layerNames)
			{
				int num2 = LayerMask.NameToLayer(layerName);
				if (num2 != -1)
				{
					num |= 1 << num2;
				}
			}
			return num;
		}

		// Token: 0x040006B8 RID: 1720
		[NativeName("m_Bits")]
		private int m_Mask;
	}
}
