﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000056 RID: 86
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Explicit)]
	public struct Color32
	{
		// Token: 0x0600049B RID: 1179 RVA: 0x0000BB0E File Offset: 0x00009D0E
		public Color32(byte r, byte g, byte b, byte a)
		{
			this.rgba = 0;
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}

		// Token: 0x0600049C RID: 1180 RVA: 0x0000BB38 File Offset: 0x00009D38
		public static implicit operator Color32(Color c)
		{
			return new Color32((byte)(Mathf.Clamp01(c.r) * 255f), (byte)(Mathf.Clamp01(c.g) * 255f), (byte)(Mathf.Clamp01(c.b) * 255f), (byte)(Mathf.Clamp01(c.a) * 255f));
		}

		// Token: 0x0600049D RID: 1181 RVA: 0x0000BBA0 File Offset: 0x00009DA0
		public static implicit operator Color(Color32 c)
		{
			return new Color((float)c.r / 255f, (float)c.g / 255f, (float)c.b / 255f, (float)c.a / 255f);
		}

		// Token: 0x0600049E RID: 1182 RVA: 0x0000BBF4 File Offset: 0x00009DF4
		public static Color32 Lerp(Color32 a, Color32 b, float t)
		{
			t = Mathf.Clamp01(t);
			return new Color32((byte)((float)a.r + (float)(b.r - a.r) * t), (byte)((float)a.g + (float)(b.g - a.g) * t), (byte)((float)a.b + (float)(b.b - a.b) * t), (byte)((float)a.a + (float)(b.a - a.a) * t));
		}

		// Token: 0x0600049F RID: 1183 RVA: 0x0000BC88 File Offset: 0x00009E88
		public static Color32 LerpUnclamped(Color32 a, Color32 b, float t)
		{
			return new Color32((byte)((float)a.r + (float)(b.r - a.r) * t), (byte)((float)a.g + (float)(b.g - a.g) * t), (byte)((float)a.b + (float)(b.b - a.b) * t), (byte)((float)a.a + (float)(b.a - a.a) * t));
		}

		// Token: 0x060004A0 RID: 1184 RVA: 0x0000BD14 File Offset: 0x00009F14
		public override string ToString()
		{
			return UnityString.Format("RGBA({0}, {1}, {2}, {3})", new object[]
			{
				this.r,
				this.g,
				this.b,
				this.a
			});
		}

		// Token: 0x060004A1 RID: 1185 RVA: 0x0000BD74 File Offset: 0x00009F74
		public string ToString(string format)
		{
			return UnityString.Format("RGBA({0}, {1}, {2}, {3})", new object[]
			{
				this.r.ToString(format),
				this.g.ToString(format),
				this.b.ToString(format),
				this.a.ToString(format)
			});
		}

		// Token: 0x04000112 RID: 274
		[Ignore(DoesNotContributeToSize = true)]
		[FieldOffset(0)]
		private int rgba;

		// Token: 0x04000113 RID: 275
		[FieldOffset(0)]
		public byte r;

		// Token: 0x04000114 RID: 276
		[FieldOffset(1)]
		public byte g;

		// Token: 0x04000115 RID: 277
		[FieldOffset(2)]
		public byte b;

		// Token: 0x04000116 RID: 278
		[FieldOffset(3)]
		public byte a;
	}
}
