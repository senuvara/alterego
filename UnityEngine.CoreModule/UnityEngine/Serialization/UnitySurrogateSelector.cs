﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UnityEngine.Serialization
{
	// Token: 0x02000317 RID: 791
	public class UnitySurrogateSelector : ISurrogateSelector
	{
		// Token: 0x06001B34 RID: 6964 RVA: 0x00002370 File Offset: 0x00000570
		public UnitySurrogateSelector()
		{
		}

		// Token: 0x06001B35 RID: 6965 RVA: 0x0002FC18 File Offset: 0x0002DE18
		public ISerializationSurrogate GetSurrogate(Type type, StreamingContext context, out ISurrogateSelector selector)
		{
			if (type.IsGenericType)
			{
				Type genericTypeDefinition = type.GetGenericTypeDefinition();
				if (genericTypeDefinition == typeof(List<>))
				{
					selector = this;
					return ListSerializationSurrogate.Default;
				}
				if (genericTypeDefinition == typeof(Dictionary<, >))
				{
					selector = this;
					Type type2 = typeof(DictionarySerializationSurrogate<, >).MakeGenericType(type.GetGenericArguments());
					return (ISerializationSurrogate)Activator.CreateInstance(type2);
				}
			}
			selector = null;
			return null;
		}

		// Token: 0x06001B36 RID: 6966 RVA: 0x000211EF File Offset: 0x0001F3EF
		public void ChainSelector(ISurrogateSelector selector)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001B37 RID: 6967 RVA: 0x000211EF File Offset: 0x0001F3EF
		public ISurrogateSelector GetNextSelector()
		{
			throw new NotImplementedException();
		}
	}
}
