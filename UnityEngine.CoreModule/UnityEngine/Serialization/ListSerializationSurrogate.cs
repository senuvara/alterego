﻿using System;
using System.Collections;
using System.Runtime.Serialization;

namespace UnityEngine.Serialization
{
	// Token: 0x02000318 RID: 792
	internal class ListSerializationSurrogate : ISerializationSurrogate
	{
		// Token: 0x06001B38 RID: 6968 RVA: 0x00002370 File Offset: 0x00000570
		public ListSerializationSurrogate()
		{
		}

		// Token: 0x06001B39 RID: 6969 RVA: 0x0002FCA0 File Offset: 0x0002DEA0
		public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
		{
			IList list = (IList)obj;
			info.AddValue("_size", list.Count);
			info.AddValue("_items", ListSerializationSurrogate.ArrayFromGenericList(list));
			info.AddValue("_version", 0);
		}

		// Token: 0x06001B3A RID: 6970 RVA: 0x0002FCE4 File Offset: 0x0002DEE4
		public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
		{
			IList list = (IList)Activator.CreateInstance(obj.GetType());
			int @int = info.GetInt32("_size");
			object result;
			if (@int == 0)
			{
				result = list;
			}
			else
			{
				IEnumerator enumerator = ((IEnumerable)info.GetValue("_items", typeof(IEnumerable))).GetEnumerator();
				for (int i = 0; i < @int; i++)
				{
					if (!enumerator.MoveNext())
					{
						throw new InvalidOperationException();
					}
					list.Add(enumerator.Current);
				}
				result = list;
			}
			return result;
		}

		// Token: 0x06001B3B RID: 6971 RVA: 0x0002FD7C File Offset: 0x0002DF7C
		private static Array ArrayFromGenericList(IList list)
		{
			Array array = Array.CreateInstance(list.GetType().GetGenericArguments()[0], list.Count);
			list.CopyTo(array, 0);
			return array;
		}

		// Token: 0x06001B3C RID: 6972 RVA: 0x0002FDB3 File Offset: 0x0002DFB3
		// Note: this type is marked as 'beforefieldinit'.
		static ListSerializationSurrogate()
		{
		}

		// Token: 0x04000A49 RID: 2633
		public static readonly ISerializationSurrogate Default = new ListSerializationSurrogate();
	}
}
