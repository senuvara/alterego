﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Serialization
{
	// Token: 0x02000316 RID: 790
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = false)]
	public class FormerlySerializedAsAttribute : Attribute
	{
		// Token: 0x06001B32 RID: 6962 RVA: 0x0002FBEB File Offset: 0x0002DDEB
		public FormerlySerializedAsAttribute(string oldName)
		{
			this.m_oldName = oldName;
		}

		// Token: 0x17000511 RID: 1297
		// (get) Token: 0x06001B33 RID: 6963 RVA: 0x0002FBFC File Offset: 0x0002DDFC
		public string oldName
		{
			get
			{
				return this.m_oldName;
			}
		}

		// Token: 0x04000A48 RID: 2632
		private string m_oldName;
	}
}
