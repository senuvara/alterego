﻿using System;

namespace UnityEngine
{
	// Token: 0x020001F6 RID: 502
	public struct RangeInt
	{
		// Token: 0x06000FBA RID: 4026 RVA: 0x0001B2A8 File Offset: 0x000194A8
		public RangeInt(int start, int length)
		{
			this.start = start;
			this.length = length;
		}

		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x06000FBB RID: 4027 RVA: 0x0001B2BC File Offset: 0x000194BC
		public int end
		{
			get
			{
				return this.start + this.length;
			}
		}

		// Token: 0x04000734 RID: 1844
		public int start;

		// Token: 0x04000735 RID: 1845
		public int length;
	}
}
