﻿using System;

namespace UnityEngine
{
	// Token: 0x0200001A RID: 26
	public enum NetworkReachability
	{
		// Token: 0x04000043 RID: 67
		NotReachable,
		// Token: 0x04000044 RID: 68
		ReachableViaCarrierDataNetwork,
		// Token: 0x04000045 RID: 69
		ReachableViaLocalAreaNetwork
	}
}
