﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000064 RID: 100
	[NativeHeader("Runtime/Export/Cursor.bindings.h")]
	public class Cursor
	{
		// Token: 0x06000529 RID: 1321 RVA: 0x00002370 File Offset: 0x00000570
		public Cursor()
		{
		}

		// Token: 0x0600052A RID: 1322 RVA: 0x0000CE8B File Offset: 0x0000B08B
		private static void SetCursor(Texture2D texture, CursorMode cursorMode)
		{
			Cursor.SetCursor(texture, Vector2.zero, cursorMode);
		}

		// Token: 0x0600052B RID: 1323 RVA: 0x0000CE9A File Offset: 0x0000B09A
		public static void SetCursor(Texture2D texture, Vector2 hotspot, CursorMode cursorMode)
		{
			Cursor.SetCursor_Injected(texture, ref hotspot, cursorMode);
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x0600052C RID: 1324
		// (set) Token: 0x0600052D RID: 1325
		public static extern bool visible { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x0600052E RID: 1326
		// (set) Token: 0x0600052F RID: 1327
		public static extern CursorLockMode lockState { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000530 RID: 1328
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetCursor_Injected(Texture2D texture, ref Vector2 hotspot, CursorMode cursorMode);
	}
}
