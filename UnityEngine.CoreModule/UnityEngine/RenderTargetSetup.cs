﻿using System;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x02000093 RID: 147
	public struct RenderTargetSetup
	{
		// Token: 0x060007AC RID: 1964 RVA: 0x0000FFD0 File Offset: 0x0000E1D0
		public RenderTargetSetup(RenderBuffer[] color, RenderBuffer depth, int mip, CubemapFace face, RenderBufferLoadAction[] colorLoad, RenderBufferStoreAction[] colorStore, RenderBufferLoadAction depthLoad, RenderBufferStoreAction depthStore)
		{
			this.color = color;
			this.depth = depth;
			this.mipLevel = mip;
			this.cubemapFace = face;
			this.depthSlice = 0;
			this.colorLoad = colorLoad;
			this.colorStore = colorStore;
			this.depthLoad = depthLoad;
			this.depthStore = depthStore;
		}

		// Token: 0x060007AD RID: 1965 RVA: 0x00010022 File Offset: 0x0000E222
		public RenderTargetSetup(RenderBuffer color, RenderBuffer depth)
		{
			this = new RenderTargetSetup(new RenderBuffer[]
			{
				color
			}, depth);
		}

		// Token: 0x060007AE RID: 1966 RVA: 0x0001003F File Offset: 0x0000E23F
		public RenderTargetSetup(RenderBuffer color, RenderBuffer depth, int mipLevel)
		{
			this = new RenderTargetSetup(new RenderBuffer[]
			{
				color
			}, depth, mipLevel);
		}

		// Token: 0x060007AF RID: 1967 RVA: 0x0001005D File Offset: 0x0000E25D
		public RenderTargetSetup(RenderBuffer color, RenderBuffer depth, int mipLevel, CubemapFace face)
		{
			this = new RenderTargetSetup(new RenderBuffer[]
			{
				color
			}, depth, mipLevel, face);
		}

		// Token: 0x060007B0 RID: 1968 RVA: 0x0001007D File Offset: 0x0000E27D
		public RenderTargetSetup(RenderBuffer color, RenderBuffer depth, int mipLevel, CubemapFace face, int depthSlice)
		{
			this = new RenderTargetSetup(new RenderBuffer[]
			{
				color
			}, depth, mipLevel, face);
			this.depthSlice = depthSlice;
		}

		// Token: 0x060007B1 RID: 1969 RVA: 0x000100A5 File Offset: 0x0000E2A5
		public RenderTargetSetup(RenderBuffer[] color, RenderBuffer depth)
		{
			this = new RenderTargetSetup(color, depth, 0, CubemapFace.Unknown);
		}

		// Token: 0x060007B2 RID: 1970 RVA: 0x000100B2 File Offset: 0x0000E2B2
		public RenderTargetSetup(RenderBuffer[] color, RenderBuffer depth, int mipLevel)
		{
			this = new RenderTargetSetup(color, depth, mipLevel, CubemapFace.Unknown);
		}

		// Token: 0x060007B3 RID: 1971 RVA: 0x000100C0 File Offset: 0x0000E2C0
		public RenderTargetSetup(RenderBuffer[] color, RenderBuffer depth, int mip, CubemapFace face)
		{
			this = new RenderTargetSetup(color, depth, mip, face, RenderTargetSetup.LoadActions(color), RenderTargetSetup.StoreActions(color), depth.loadAction, depth.storeAction);
		}

		// Token: 0x060007B4 RID: 1972 RVA: 0x000100F4 File Offset: 0x0000E2F4
		internal static RenderBufferLoadAction[] LoadActions(RenderBuffer[] buf)
		{
			RenderBufferLoadAction[] array = new RenderBufferLoadAction[buf.Length];
			for (int i = 0; i < buf.Length; i++)
			{
				array[i] = buf[i].loadAction;
				buf[i].loadAction = RenderBufferLoadAction.Load;
			}
			return array;
		}

		// Token: 0x060007B5 RID: 1973 RVA: 0x00010148 File Offset: 0x0000E348
		internal static RenderBufferStoreAction[] StoreActions(RenderBuffer[] buf)
		{
			RenderBufferStoreAction[] array = new RenderBufferStoreAction[buf.Length];
			for (int i = 0; i < buf.Length; i++)
			{
				array[i] = buf[i].storeAction;
				buf[i].storeAction = RenderBufferStoreAction.Store;
			}
			return array;
		}

		// Token: 0x04000186 RID: 390
		public RenderBuffer[] color;

		// Token: 0x04000187 RID: 391
		public RenderBuffer depth;

		// Token: 0x04000188 RID: 392
		public int mipLevel;

		// Token: 0x04000189 RID: 393
		public CubemapFace cubemapFace;

		// Token: 0x0400018A RID: 394
		public int depthSlice;

		// Token: 0x0400018B RID: 395
		public RenderBufferLoadAction[] colorLoad;

		// Token: 0x0400018C RID: 396
		public RenderBufferStoreAction[] colorStore;

		// Token: 0x0400018D RID: 397
		public RenderBufferLoadAction depthLoad;

		// Token: 0x0400018E RID: 398
		public RenderBufferStoreAction depthStore;
	}
}
