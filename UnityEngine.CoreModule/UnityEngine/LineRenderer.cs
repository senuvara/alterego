﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200009D RID: 157
	[NativeHeader("Runtime/Graphics/LineRenderer.h")]
	[NativeHeader("Runtime/Graphics/GraphicsScriptBindings.h")]
	public sealed class LineRenderer : Renderer
	{
		// Token: 0x06000845 RID: 2117 RVA: 0x00008EED File Offset: 0x000070ED
		public LineRenderer()
		{
		}

		// Token: 0x06000846 RID: 2118 RVA: 0x00010303 File Offset: 0x0000E503
		[Obsolete("Use startWidth, endWidth or widthCurve instead.", false)]
		public void SetWidth(float start, float end)
		{
			this.startWidth = start;
			this.endWidth = end;
		}

		// Token: 0x06000847 RID: 2119 RVA: 0x00010314 File Offset: 0x0000E514
		[Obsolete("Use startColor, endColor or colorGradient instead.", false)]
		public void SetColors(Color start, Color end)
		{
			this.startColor = start;
			this.endColor = end;
		}

		// Token: 0x06000848 RID: 2120 RVA: 0x00010325 File Offset: 0x0000E525
		[Obsolete("Use positionCount instead.", false)]
		public void SetVertexCount(int count)
		{
			this.positionCount = count;
		}

		// Token: 0x1700017E RID: 382
		// (get) Token: 0x06000849 RID: 2121 RVA: 0x00010330 File Offset: 0x0000E530
		// (set) Token: 0x0600084A RID: 2122 RVA: 0x00010325 File Offset: 0x0000E525
		[Obsolete("Use positionCount instead (UnityUpgradable) -> positionCount", false)]
		public int numPositions
		{
			get
			{
				return this.positionCount;
			}
			set
			{
				this.positionCount = value;
			}
		}

		// Token: 0x1700017F RID: 383
		// (get) Token: 0x0600084B RID: 2123
		// (set) Token: 0x0600084C RID: 2124
		public extern float startWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000180 RID: 384
		// (get) Token: 0x0600084D RID: 2125
		// (set) Token: 0x0600084E RID: 2126
		public extern float endWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000181 RID: 385
		// (get) Token: 0x0600084F RID: 2127
		// (set) Token: 0x06000850 RID: 2128
		public extern float widthMultiplier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000182 RID: 386
		// (get) Token: 0x06000851 RID: 2129
		// (set) Token: 0x06000852 RID: 2130
		public extern int numCornerVertices { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000183 RID: 387
		// (get) Token: 0x06000853 RID: 2131
		// (set) Token: 0x06000854 RID: 2132
		public extern int numCapVertices { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000184 RID: 388
		// (get) Token: 0x06000855 RID: 2133
		// (set) Token: 0x06000856 RID: 2134
		public extern bool useWorldSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000185 RID: 389
		// (get) Token: 0x06000857 RID: 2135
		// (set) Token: 0x06000858 RID: 2136
		public extern bool loop { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000186 RID: 390
		// (get) Token: 0x06000859 RID: 2137 RVA: 0x0001034C File Offset: 0x0000E54C
		// (set) Token: 0x0600085A RID: 2138 RVA: 0x00010362 File Offset: 0x0000E562
		public Color startColor
		{
			get
			{
				Color result;
				this.get_startColor_Injected(out result);
				return result;
			}
			set
			{
				this.set_startColor_Injected(ref value);
			}
		}

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x0600085B RID: 2139 RVA: 0x0001036C File Offset: 0x0000E56C
		// (set) Token: 0x0600085C RID: 2140 RVA: 0x00010382 File Offset: 0x0000E582
		public Color endColor
		{
			get
			{
				Color result;
				this.get_endColor_Injected(out result);
				return result;
			}
			set
			{
				this.set_endColor_Injected(ref value);
			}
		}

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x0600085D RID: 2141
		// (set) Token: 0x0600085E RID: 2142
		[NativeProperty("PositionsCount")]
		public extern int positionCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600085F RID: 2143 RVA: 0x0001038C File Offset: 0x0000E58C
		public void SetPosition(int index, Vector3 position)
		{
			this.SetPosition_Injected(index, ref position);
		}

		// Token: 0x06000860 RID: 2144 RVA: 0x00010398 File Offset: 0x0000E598
		public Vector3 GetPosition(int index)
		{
			Vector3 result;
			this.GetPosition_Injected(index, out result);
			return result;
		}

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06000861 RID: 2145
		// (set) Token: 0x06000862 RID: 2146
		public extern float shadowBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x06000863 RID: 2147
		// (set) Token: 0x06000864 RID: 2148
		public extern bool generateLightingData { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000865 RID: 2149
		// (set) Token: 0x06000866 RID: 2150
		public extern LineTextureMode textureMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000867 RID: 2151
		// (set) Token: 0x06000868 RID: 2152
		public extern LineAlignment alignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000869 RID: 2153
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Simplify(float tolerance);

		// Token: 0x0600086A RID: 2154 RVA: 0x000103AF File Offset: 0x0000E5AF
		public void BakeMesh(Mesh mesh, bool useTransform = false)
		{
			this.BakeMesh(mesh, Camera.main, useTransform);
		}

		// Token: 0x0600086B RID: 2155
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void BakeMesh([NotNull] Mesh mesh, [NotNull] Camera camera, bool useTransform = false);

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x0600086C RID: 2156 RVA: 0x000103C0 File Offset: 0x0000E5C0
		// (set) Token: 0x0600086D RID: 2157 RVA: 0x000103DB File Offset: 0x0000E5DB
		public AnimationCurve widthCurve
		{
			get
			{
				return this.GetWidthCurveCopy();
			}
			set
			{
				this.SetWidthCurve(value);
			}
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x0600086E RID: 2158 RVA: 0x000103E8 File Offset: 0x0000E5E8
		// (set) Token: 0x0600086F RID: 2159 RVA: 0x00010403 File Offset: 0x0000E603
		public Gradient colorGradient
		{
			get
			{
				return this.GetColorGradientCopy();
			}
			set
			{
				this.SetColorGradient(value);
			}
		}

		// Token: 0x06000870 RID: 2160
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimationCurve GetWidthCurveCopy();

		// Token: 0x06000871 RID: 2161
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetWidthCurve([NotNull] AnimationCurve curve);

		// Token: 0x06000872 RID: 2162
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Gradient GetColorGradientCopy();

		// Token: 0x06000873 RID: 2163
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetColorGradient([NotNull] Gradient curve);

		// Token: 0x06000874 RID: 2164
		[FreeFunction(Name = "LineRendererScripting::GetPositions", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetPositions([NotNull] [Out] Vector3[] positions);

		// Token: 0x06000875 RID: 2165
		[FreeFunction(Name = "LineRendererScripting::SetPositions", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPositions([NotNull] Vector3[] positions);

		// Token: 0x06000876 RID: 2166
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_startColor_Injected(out Color ret);

		// Token: 0x06000877 RID: 2167
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_startColor_Injected(ref Color value);

		// Token: 0x06000878 RID: 2168
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_endColor_Injected(out Color ret);

		// Token: 0x06000879 RID: 2169
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_endColor_Injected(ref Color value);

		// Token: 0x0600087A RID: 2170
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetPosition_Injected(int index, ref Vector3 position);

		// Token: 0x0600087B RID: 2171
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetPosition_Injected(int index, out Vector3 ret);
	}
}
