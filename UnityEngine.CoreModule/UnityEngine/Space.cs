﻿using System;

namespace UnityEngine
{
	// Token: 0x02000037 RID: 55
	public enum Space
	{
		// Token: 0x04000088 RID: 136
		World,
		// Token: 0x04000089 RID: 137
		Self
	}
}
