﻿using System;

namespace UnityEngine
{
	// Token: 0x02000222 RID: 546
	public enum DeviceType
	{
		// Token: 0x040007A2 RID: 1954
		Unknown,
		// Token: 0x040007A3 RID: 1955
		Handheld,
		// Token: 0x040007A4 RID: 1956
		Console,
		// Token: 0x040007A5 RID: 1957
		Desktop
	}
}
