﻿using System;

namespace UnityEngine
{
	// Token: 0x02000360 RID: 864
	public enum SpritePackingMode
	{
		// Token: 0x04000AFA RID: 2810
		Tight,
		// Token: 0x04000AFB RID: 2811
		Rectangle
	}
}
