﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000C RID: 12
	internal class AndroidJavaRunnableProxy : AndroidJavaProxy
	{
		// Token: 0x06000148 RID: 328 RVA: 0x0000556B File Offset: 0x0000376B
		public AndroidJavaRunnableProxy(AndroidJavaRunnable runnable) : base("java/lang/Runnable")
		{
			this.mRunnable = runnable;
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00005580 File Offset: 0x00003780
		public void run()
		{
			this.mRunnable();
		}

		// Token: 0x04000011 RID: 17
		private AndroidJavaRunnable mRunnable;
	}
}
