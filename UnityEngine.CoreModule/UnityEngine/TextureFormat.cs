﻿using System;

namespace UnityEngine
{
	// Token: 0x020000CC RID: 204
	public enum TextureFormat
	{
		// Token: 0x04000243 RID: 579
		Alpha8 = 1,
		// Token: 0x04000244 RID: 580
		ARGB4444,
		// Token: 0x04000245 RID: 581
		RGB24,
		// Token: 0x04000246 RID: 582
		RGBA32,
		// Token: 0x04000247 RID: 583
		ARGB32,
		// Token: 0x04000248 RID: 584
		RGB565 = 7,
		// Token: 0x04000249 RID: 585
		R16 = 9,
		// Token: 0x0400024A RID: 586
		DXT1,
		// Token: 0x0400024B RID: 587
		DXT5 = 12,
		// Token: 0x0400024C RID: 588
		RGBA4444,
		// Token: 0x0400024D RID: 589
		BGRA32,
		// Token: 0x0400024E RID: 590
		RHalf,
		// Token: 0x0400024F RID: 591
		RGHalf,
		// Token: 0x04000250 RID: 592
		RGBAHalf,
		// Token: 0x04000251 RID: 593
		RFloat,
		// Token: 0x04000252 RID: 594
		RGFloat,
		// Token: 0x04000253 RID: 595
		RGBAFloat,
		// Token: 0x04000254 RID: 596
		YUY2,
		// Token: 0x04000255 RID: 597
		RGB9e5Float,
		// Token: 0x04000256 RID: 598
		BC4 = 26,
		// Token: 0x04000257 RID: 599
		BC5,
		// Token: 0x04000258 RID: 600
		BC6H = 24,
		// Token: 0x04000259 RID: 601
		BC7,
		// Token: 0x0400025A RID: 602
		DXT1Crunched = 28,
		// Token: 0x0400025B RID: 603
		DXT5Crunched,
		// Token: 0x0400025C RID: 604
		PVRTC_RGB2,
		// Token: 0x0400025D RID: 605
		PVRTC_RGBA2,
		// Token: 0x0400025E RID: 606
		PVRTC_RGB4,
		// Token: 0x0400025F RID: 607
		PVRTC_RGBA4,
		// Token: 0x04000260 RID: 608
		ETC_RGB4,
		// Token: 0x04000261 RID: 609
		EAC_R = 41,
		// Token: 0x04000262 RID: 610
		EAC_R_SIGNED,
		// Token: 0x04000263 RID: 611
		EAC_RG,
		// Token: 0x04000264 RID: 612
		EAC_RG_SIGNED,
		// Token: 0x04000265 RID: 613
		ETC2_RGB,
		// Token: 0x04000266 RID: 614
		ETC2_RGBA1,
		// Token: 0x04000267 RID: 615
		ETC2_RGBA8,
		// Token: 0x04000268 RID: 616
		ASTC_RGB_4x4,
		// Token: 0x04000269 RID: 617
		ASTC_RGB_5x5,
		// Token: 0x0400026A RID: 618
		ASTC_RGB_6x6,
		// Token: 0x0400026B RID: 619
		ASTC_RGB_8x8,
		// Token: 0x0400026C RID: 620
		ASTC_RGB_10x10,
		// Token: 0x0400026D RID: 621
		ASTC_RGB_12x12,
		// Token: 0x0400026E RID: 622
		ASTC_RGBA_4x4,
		// Token: 0x0400026F RID: 623
		ASTC_RGBA_5x5,
		// Token: 0x04000270 RID: 624
		ASTC_RGBA_6x6,
		// Token: 0x04000271 RID: 625
		ASTC_RGBA_8x8,
		// Token: 0x04000272 RID: 626
		ASTC_RGBA_10x10,
		// Token: 0x04000273 RID: 627
		ASTC_RGBA_12x12,
		// Token: 0x04000274 RID: 628
		[Obsolete("Nintendo 3DS is no longer supported.")]
		ETC_RGB4_3DS,
		// Token: 0x04000275 RID: 629
		[Obsolete("Nintendo 3DS is no longer supported.")]
		ETC_RGBA8_3DS,
		// Token: 0x04000276 RID: 630
		RG16,
		// Token: 0x04000277 RID: 631
		R8,
		// Token: 0x04000278 RID: 632
		ETC_RGB4Crunched,
		// Token: 0x04000279 RID: 633
		ETC2_RGBA8Crunched
	}
}
