﻿using System;

namespace UnityEngine
{
	// Token: 0x0200001C RID: 28
	public enum ApplicationInstallMode
	{
		// Token: 0x0400004A RID: 74
		Unknown,
		// Token: 0x0400004B RID: 75
		Store,
		// Token: 0x0400004C RID: 76
		DeveloperBuild,
		// Token: 0x0400004D RID: 77
		Adhoc,
		// Token: 0x0400004E RID: 78
		Enterprise,
		// Token: 0x0400004F RID: 79
		Editor
	}
}
