﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200022F RID: 559
	[UsedByNativeCode]
	[Serializable]
	public struct CustomRenderTextureUpdateZone
	{
		// Token: 0x040007B0 RID: 1968
		public Vector3 updateZoneCenter;

		// Token: 0x040007B1 RID: 1969
		public Vector3 updateZoneSize;

		// Token: 0x040007B2 RID: 1970
		public float rotation;

		// Token: 0x040007B3 RID: 1971
		public int passIndex;

		// Token: 0x040007B4 RID: 1972
		public bool needSwap;
	}
}
