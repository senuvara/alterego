﻿using System;

namespace UnityEngine
{
	// Token: 0x02000125 RID: 293
	public enum DeviceOrientation
	{
		// Token: 0x04000534 RID: 1332
		Unknown,
		// Token: 0x04000535 RID: 1333
		Portrait,
		// Token: 0x04000536 RID: 1334
		PortraitUpsideDown,
		// Token: 0x04000537 RID: 1335
		LandscapeLeft,
		// Token: 0x04000538 RID: 1336
		LandscapeRight,
		// Token: 0x04000539 RID: 1337
		FaceUp,
		// Token: 0x0400053A RID: 1338
		FaceDown
	}
}
