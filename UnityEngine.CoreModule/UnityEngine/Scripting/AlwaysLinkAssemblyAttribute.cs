﻿using System;

namespace UnityEngine.Scripting
{
	// Token: 0x0200030F RID: 783
	[AttributeUsage(AttributeTargets.Assembly)]
	public class AlwaysLinkAssemblyAttribute : Attribute
	{
		// Token: 0x06001B21 RID: 6945 RVA: 0x0000898B File Offset: 0x00006B8B
		public AlwaysLinkAssemblyAttribute()
		{
		}
	}
}
