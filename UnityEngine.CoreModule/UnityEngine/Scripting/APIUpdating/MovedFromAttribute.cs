﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Scripting.APIUpdating
{
	// Token: 0x02000315 RID: 789
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate)]
	public class MovedFromAttribute : Attribute
	{
		// Token: 0x06001B2C RID: 6956 RVA: 0x0002FB80 File Offset: 0x0002DD80
		public MovedFromAttribute(string sourceNamespace) : this(sourceNamespace, false)
		{
		}

		// Token: 0x06001B2D RID: 6957 RVA: 0x0002FB8B File Offset: 0x0002DD8B
		public MovedFromAttribute(string sourceNamespace, bool isInDifferentAssembly)
		{
			this.Namespace = sourceNamespace;
			this.IsInDifferentAssembly = isInDifferentAssembly;
		}

		// Token: 0x1700050F RID: 1295
		// (get) Token: 0x06001B2E RID: 6958 RVA: 0x0002FBA4 File Offset: 0x0002DDA4
		// (set) Token: 0x06001B2F RID: 6959 RVA: 0x0002FBBE File Offset: 0x0002DDBE
		public string Namespace
		{
			[CompilerGenerated]
			get
			{
				return this.<Namespace>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Namespace>k__BackingField = value;
			}
		}

		// Token: 0x17000510 RID: 1296
		// (get) Token: 0x06001B30 RID: 6960 RVA: 0x0002FBC8 File Offset: 0x0002DDC8
		// (set) Token: 0x06001B31 RID: 6961 RVA: 0x0002FBE2 File Offset: 0x0002DDE2
		public bool IsInDifferentAssembly
		{
			[CompilerGenerated]
			get
			{
				return this.<IsInDifferentAssembly>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsInDifferentAssembly>k__BackingField = value;
			}
		}

		// Token: 0x04000A46 RID: 2630
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <Namespace>k__BackingField;

		// Token: 0x04000A47 RID: 2631
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <IsInDifferentAssembly>k__BackingField;
	}
}
