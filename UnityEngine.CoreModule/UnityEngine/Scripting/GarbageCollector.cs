﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;

namespace UnityEngine.Scripting
{
	// Token: 0x02000311 RID: 785
	[NativeHeader("Runtime/Scripting/GarbageCollector.h")]
	[VisibleToOtherModules]
	public static class GarbageCollector
	{
		// Token: 0x14000016 RID: 22
		// (add) Token: 0x06001B23 RID: 6947 RVA: 0x0002FAD0 File Offset: 0x0002DCD0
		// (remove) Token: 0x06001B24 RID: 6948 RVA: 0x0002FB04 File Offset: 0x0002DD04
		public static event Action<GarbageCollector.Mode> GCModeChanged
		{
			add
			{
				Action<GarbageCollector.Mode> action = GarbageCollector.GCModeChanged;
				Action<GarbageCollector.Mode> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<GarbageCollector.Mode>>(ref GarbageCollector.GCModeChanged, (Action<GarbageCollector.Mode>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<GarbageCollector.Mode> action = GarbageCollector.GCModeChanged;
				Action<GarbageCollector.Mode> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<GarbageCollector.Mode>>(ref GarbageCollector.GCModeChanged, (Action<GarbageCollector.Mode>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x1700050E RID: 1294
		// (get) Token: 0x06001B25 RID: 6949 RVA: 0x0002FB38 File Offset: 0x0002DD38
		// (set) Token: 0x06001B26 RID: 6950 RVA: 0x0002FB52 File Offset: 0x0002DD52
		public static GarbageCollector.Mode GCMode
		{
			get
			{
				return GarbageCollector.GetMode();
			}
			set
			{
				if (value != GarbageCollector.GetMode())
				{
					GarbageCollector.SetMode(value);
					if (GarbageCollector.GCModeChanged != null)
					{
						GarbageCollector.GCModeChanged(value);
					}
				}
			}
		}

		// Token: 0x06001B27 RID: 6951
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetMode(GarbageCollector.Mode mode);

		// Token: 0x06001B28 RID: 6952
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern GarbageCollector.Mode GetMode();

		// Token: 0x04000A42 RID: 2626
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action<GarbageCollector.Mode> GCModeChanged;

		// Token: 0x02000312 RID: 786
		public enum Mode
		{
			// Token: 0x04000A44 RID: 2628
			Disabled,
			// Token: 0x04000A45 RID: 2629
			Enabled
		}
	}
}
