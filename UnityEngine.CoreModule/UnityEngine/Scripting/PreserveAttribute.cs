﻿using System;

namespace UnityEngine.Scripting
{
	// Token: 0x02000313 RID: 787
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false)]
	public class PreserveAttribute : Attribute
	{
		// Token: 0x06001B29 RID: 6953 RVA: 0x0000898B File Offset: 0x00006B8B
		public PreserveAttribute()
		{
		}
	}
}
