﻿using System;

namespace UnityEngine
{
	// Token: 0x020001F0 RID: 496
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class GradientUsageAttribute : PropertyAttribute
	{
		// Token: 0x06000F8C RID: 3980 RVA: 0x0001AECF File Offset: 0x000190CF
		public GradientUsageAttribute(bool hdr)
		{
			this.hdr = hdr;
		}

		// Token: 0x0400072E RID: 1838
		public readonly bool hdr = false;
	}
}
