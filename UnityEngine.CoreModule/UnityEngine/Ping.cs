﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200015C RID: 348
	[NativeHeader("Runtime/Export/Ping.bindings.h")]
	public sealed class Ping
	{
		// Token: 0x06000F15 RID: 3861 RVA: 0x0001A17C File Offset: 0x0001837C
		public Ping(string address)
		{
			this.m_Ptr = Ping.Internal_Create(address);
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x0001A194 File Offset: 0x00018394
		~Ping()
		{
			this.DestroyPing();
		}

		// Token: 0x06000F17 RID: 3863 RVA: 0x0001A1C4 File Offset: 0x000183C4
		[ThreadAndSerializationSafe]
		public void DestroyPing()
		{
			if (!(this.m_Ptr == IntPtr.Zero))
			{
				Ping.Internal_Destroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x06000F18 RID: 3864
		[FreeFunction("DestroyPing", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Destroy(IntPtr ptr);

		// Token: 0x06000F19 RID: 3865
		[FreeFunction("CreatePing")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Internal_Create(string address);

		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x06000F1A RID: 3866 RVA: 0x0001A1F8 File Offset: 0x000183F8
		public bool isDone
		{
			get
			{
				return !(this.m_Ptr == IntPtr.Zero) && this.Internal_IsDone();
			}
		}

		// Token: 0x06000F1B RID: 3867
		[NativeName("GetIsDone")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool Internal_IsDone();

		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x06000F1C RID: 3868
		public extern int time { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x06000F1D RID: 3869
		public extern string ip { [NativeName("GetIP")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x04000709 RID: 1801
		internal IntPtr m_Ptr;
	}
}
