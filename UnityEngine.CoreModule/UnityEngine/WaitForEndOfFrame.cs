﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000259 RID: 601
	[RequiredByNativeCode]
	public sealed class WaitForEndOfFrame : YieldInstruction
	{
		// Token: 0x06001624 RID: 5668 RVA: 0x000084D7 File Offset: 0x000066D7
		public WaitForEndOfFrame()
		{
		}
	}
}
