﻿using System;

namespace UnityEngine
{
	// Token: 0x02000035 RID: 53
	public enum SendMessageOptions
	{
		// Token: 0x0400007E RID: 126
		RequireReceiver,
		// Token: 0x0400007F RID: 127
		DontRequireReceiver
	}
}
