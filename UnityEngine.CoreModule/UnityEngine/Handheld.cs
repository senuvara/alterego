﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x0200011A RID: 282
	[NativeHeader("Runtime/Input/GetInput.h")]
	[NativeHeader("Runtime/Video/MoviePlayback.h")]
	[NativeHeader("PlatformDependent/AndroidPlayer/Source/EntryPoint.h")]
	[NativeHeader("Runtime/Export/Handheld.bindings.h")]
	public class Handheld
	{
		// Token: 0x06000BC7 RID: 3015 RVA: 0x00002370 File Offset: 0x00000570
		public Handheld()
		{
		}

		// Token: 0x06000BC8 RID: 3016 RVA: 0x00012DF0 File Offset: 0x00010FF0
		public static bool PlayFullScreenMovie(string path, [DefaultValue("Color.black")] Color bgColor, [DefaultValue("FullScreenMovieControlMode.Full")] FullScreenMovieControlMode controlMode, [DefaultValue("FullScreenMovieScalingMode.AspectFit")] FullScreenMovieScalingMode scalingMode)
		{
			return Handheld.PlayFullScreenMovie_Bindings(path, bgColor, controlMode, scalingMode);
		}

		// Token: 0x06000BC9 RID: 3017 RVA: 0x00012E10 File Offset: 0x00011010
		[ExcludeFromDocs]
		public static bool PlayFullScreenMovie(string path, Color bgColor, FullScreenMovieControlMode controlMode)
		{
			FullScreenMovieScalingMode scalingMode = FullScreenMovieScalingMode.AspectFit;
			return Handheld.PlayFullScreenMovie_Bindings(path, bgColor, controlMode, scalingMode);
		}

		// Token: 0x06000BCA RID: 3018 RVA: 0x00012E30 File Offset: 0x00011030
		[ExcludeFromDocs]
		public static bool PlayFullScreenMovie(string path, Color bgColor)
		{
			FullScreenMovieScalingMode scalingMode = FullScreenMovieScalingMode.AspectFit;
			FullScreenMovieControlMode controlMode = FullScreenMovieControlMode.Full;
			return Handheld.PlayFullScreenMovie_Bindings(path, bgColor, controlMode, scalingMode);
		}

		// Token: 0x06000BCB RID: 3019 RVA: 0x00012E54 File Offset: 0x00011054
		[ExcludeFromDocs]
		public static bool PlayFullScreenMovie(string path)
		{
			FullScreenMovieScalingMode scalingMode = FullScreenMovieScalingMode.AspectFit;
			FullScreenMovieControlMode controlMode = FullScreenMovieControlMode.Full;
			Color black = Color.black;
			return Handheld.PlayFullScreenMovie_Bindings(path, black, controlMode, scalingMode);
		}

		// Token: 0x06000BCC RID: 3020 RVA: 0x00012E7C File Offset: 0x0001107C
		[FreeFunction("PlayFullScreenMovie")]
		private static bool PlayFullScreenMovie_Bindings(string path, Color bgColor, FullScreenMovieControlMode controlMode, FullScreenMovieScalingMode scalingMode)
		{
			return Handheld.PlayFullScreenMovie_Bindings_Injected(path, ref bgColor, controlMode, scalingMode);
		}

		// Token: 0x06000BCD RID: 3021
		[FreeFunction("Vibrate")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Vibrate();

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x06000BCE RID: 3022 RVA: 0x00012E88 File Offset: 0x00011088
		// (set) Token: 0x06000BCF RID: 3023 RVA: 0x00007476 File Offset: 0x00005676
		[Obsolete("Property Handheld.use32BitDisplayBuffer has been deprecated. Modifying it has no effect, use PlayerSettings instead.")]
		public static bool use32BitDisplayBuffer
		{
			get
			{
				return Handheld.GetUse32BitDisplayBuffer_Bindings();
			}
			set
			{
			}
		}

		// Token: 0x06000BD0 RID: 3024
		[FreeFunction("GetUse32BitDisplayBuffer_Bindings")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetUse32BitDisplayBuffer_Bindings();

		// Token: 0x06000BD1 RID: 3025
		[FreeFunction("SetActivityIndicatorStyle_Bindings")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetActivityIndicatorStyleImpl_Bindings(int style);

		// Token: 0x06000BD2 RID: 3026 RVA: 0x00012EA2 File Offset: 0x000110A2
		public static void SetActivityIndicatorStyle(AndroidActivityIndicatorStyle style)
		{
			Handheld.SetActivityIndicatorStyleImpl_Bindings((int)style);
		}

		// Token: 0x06000BD3 RID: 3027
		[FreeFunction("GetActivityIndicatorStyle_Bindings")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetActivityIndicatorStyle();

		// Token: 0x06000BD4 RID: 3028
		[FreeFunction("StartActivityIndicator_Bindings")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void StartActivityIndicator();

		// Token: 0x06000BD5 RID: 3029
		[FreeFunction("StopActivityIndicator_Bindings")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void StopActivityIndicator();

		// Token: 0x06000BD6 RID: 3030
		[FreeFunction("ClearShaderCache_Bindings")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ClearShaderCache();

		// Token: 0x06000BD7 RID: 3031
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool PlayFullScreenMovie_Bindings_Injected(string path, ref Color bgColor, FullScreenMovieControlMode controlMode, FullScreenMovieScalingMode scalingMode);
	}
}
