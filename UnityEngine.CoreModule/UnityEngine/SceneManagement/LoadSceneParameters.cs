﻿using System;

namespace UnityEngine.SceneManagement
{
	// Token: 0x02000302 RID: 770
	[Serializable]
	public struct LoadSceneParameters
	{
		// Token: 0x06001AC3 RID: 6851 RVA: 0x0002F2BC File Offset: 0x0002D4BC
		public LoadSceneParameters(LoadSceneMode mode)
		{
			this.m_LoadSceneMode = mode;
			this.m_LocalPhysicsMode = LocalPhysicsMode.None;
		}

		// Token: 0x06001AC4 RID: 6852 RVA: 0x0002F2CD File Offset: 0x0002D4CD
		public LoadSceneParameters(LoadSceneMode mode, LocalPhysicsMode physicsMode)
		{
			this.m_LoadSceneMode = mode;
			this.m_LocalPhysicsMode = physicsMode;
		}

		// Token: 0x170004F9 RID: 1273
		// (get) Token: 0x06001AC5 RID: 6853 RVA: 0x0002F2E0 File Offset: 0x0002D4E0
		// (set) Token: 0x06001AC6 RID: 6854 RVA: 0x0002F2FB File Offset: 0x0002D4FB
		public LoadSceneMode loadSceneMode
		{
			get
			{
				return this.m_LoadSceneMode;
			}
			set
			{
				this.m_LoadSceneMode = value;
			}
		}

		// Token: 0x170004FA RID: 1274
		// (get) Token: 0x06001AC7 RID: 6855 RVA: 0x0002F308 File Offset: 0x0002D508
		// (set) Token: 0x06001AC8 RID: 6856 RVA: 0x0002F323 File Offset: 0x0002D523
		public LocalPhysicsMode localPhysicsMode
		{
			get
			{
				return this.m_LocalPhysicsMode;
			}
			set
			{
				this.m_LocalPhysicsMode = value;
			}
		}

		// Token: 0x04000A15 RID: 2581
		[SerializeField]
		private LoadSceneMode m_LoadSceneMode;

		// Token: 0x04000A16 RID: 2582
		[SerializeField]
		private LocalPhysicsMode m_LocalPhysicsMode;
	}
}
