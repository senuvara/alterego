﻿using System;

namespace UnityEngine.SceneManagement
{
	// Token: 0x02000304 RID: 772
	[Flags]
	public enum UnloadSceneOptions
	{
		// Token: 0x04000A19 RID: 2585
		None = 0,
		// Token: 0x04000A1A RID: 2586
		UnloadAllEmbeddedSceneObjects = 1
	}
}
