﻿using System;

namespace UnityEngine.SceneManagement
{
	// Token: 0x02000303 RID: 771
	[Serializable]
	public struct CreateSceneParameters
	{
		// Token: 0x06001AC9 RID: 6857 RVA: 0x0002F32D File Offset: 0x0002D52D
		public CreateSceneParameters(LocalPhysicsMode physicsMode)
		{
			this.m_LocalPhysicsMode = physicsMode;
		}

		// Token: 0x170004FB RID: 1275
		// (get) Token: 0x06001ACA RID: 6858 RVA: 0x0002F338 File Offset: 0x0002D538
		// (set) Token: 0x06001ACB RID: 6859 RVA: 0x0002F32D File Offset: 0x0002D52D
		public LocalPhysicsMode localPhysicsMode
		{
			get
			{
				return this.m_LocalPhysicsMode;
			}
			set
			{
				this.m_LocalPhysicsMode = value;
			}
		}

		// Token: 0x04000A17 RID: 2583
		[SerializeField]
		private LocalPhysicsMode m_LocalPhysicsMode;
	}
}
