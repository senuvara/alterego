﻿using System;

namespace UnityEngine.SceneManagement
{
	// Token: 0x02000300 RID: 768
	public enum LoadSceneMode
	{
		// Token: 0x04000A0F RID: 2575
		Single,
		// Token: 0x04000A10 RID: 2576
		Additive
	}
}
