﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.SceneManagement
{
	// Token: 0x020002FD RID: 765
	[NativeHeader("Runtime/Export/SceneManager/Scene.bindings.h")]
	[Serializable]
	public struct Scene
	{
		// Token: 0x06001A69 RID: 6761
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsValidInternal(int sceneHandle);

		// Token: 0x06001A6A RID: 6762
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetPathInternal(int sceneHandle);

		// Token: 0x06001A6B RID: 6763
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetNameInternal(int sceneHandle);

		// Token: 0x06001A6C RID: 6764
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetNameInternal(int sceneHandle, string name);

		// Token: 0x06001A6D RID: 6765
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetGUIDInternal(int sceneHandle);

		// Token: 0x06001A6E RID: 6766
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetIsLoadedInternal(int sceneHandle);

		// Token: 0x06001A6F RID: 6767
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Scene.LoadingState GetLoadingStateInternal(int sceneHandle);

		// Token: 0x06001A70 RID: 6768
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetIsDirtyInternal(int sceneHandle);

		// Token: 0x06001A71 RID: 6769
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetDirtyID(int sceneHandle);

		// Token: 0x06001A72 RID: 6770
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetBuildIndexInternal(int sceneHandle);

		// Token: 0x06001A73 RID: 6771
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetRootCountInternal(int sceneHandle);

		// Token: 0x06001A74 RID: 6772
		[StaticAccessor("SceneBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRootGameObjectsInternal(int sceneHandle, object resultRootList);

		// Token: 0x170004ED RID: 1261
		// (get) Token: 0x06001A75 RID: 6773 RVA: 0x0002EA58 File Offset: 0x0002CC58
		public int handle
		{
			get
			{
				return this.m_Handle;
			}
		}

		// Token: 0x170004EE RID: 1262
		// (get) Token: 0x06001A76 RID: 6774 RVA: 0x0002EA74 File Offset: 0x0002CC74
		internal Scene.LoadingState loadingState
		{
			get
			{
				return Scene.GetLoadingStateInternal(this.handle);
			}
		}

		// Token: 0x170004EF RID: 1263
		// (get) Token: 0x06001A77 RID: 6775 RVA: 0x0002EA94 File Offset: 0x0002CC94
		internal string guid
		{
			get
			{
				return Scene.GetGUIDInternal(this.handle);
			}
		}

		// Token: 0x06001A78 RID: 6776 RVA: 0x0002EAB4 File Offset: 0x0002CCB4
		public bool IsValid()
		{
			return Scene.IsValidInternal(this.handle);
		}

		// Token: 0x170004F0 RID: 1264
		// (get) Token: 0x06001A79 RID: 6777 RVA: 0x0002EAD4 File Offset: 0x0002CCD4
		public string path
		{
			get
			{
				return Scene.GetPathInternal(this.handle);
			}
		}

		// Token: 0x170004F1 RID: 1265
		// (get) Token: 0x06001A7A RID: 6778 RVA: 0x0002EAF4 File Offset: 0x0002CCF4
		// (set) Token: 0x06001A7B RID: 6779 RVA: 0x0002EB14 File Offset: 0x0002CD14
		public string name
		{
			get
			{
				return Scene.GetNameInternal(this.handle);
			}
			set
			{
				Scene.SetNameInternal(this.handle, value);
			}
		}

		// Token: 0x170004F2 RID: 1266
		// (get) Token: 0x06001A7C RID: 6780 RVA: 0x0002EB24 File Offset: 0x0002CD24
		public bool isLoaded
		{
			get
			{
				return Scene.GetIsLoadedInternal(this.handle);
			}
		}

		// Token: 0x170004F3 RID: 1267
		// (get) Token: 0x06001A7D RID: 6781 RVA: 0x0002EB44 File Offset: 0x0002CD44
		public int buildIndex
		{
			get
			{
				return Scene.GetBuildIndexInternal(this.handle);
			}
		}

		// Token: 0x170004F4 RID: 1268
		// (get) Token: 0x06001A7E RID: 6782 RVA: 0x0002EB64 File Offset: 0x0002CD64
		public bool isDirty
		{
			get
			{
				return Scene.GetIsDirtyInternal(this.handle);
			}
		}

		// Token: 0x170004F5 RID: 1269
		// (get) Token: 0x06001A7F RID: 6783 RVA: 0x0002EB84 File Offset: 0x0002CD84
		internal int dirtyID
		{
			get
			{
				return Scene.GetDirtyID(this.handle);
			}
		}

		// Token: 0x170004F6 RID: 1270
		// (get) Token: 0x06001A80 RID: 6784 RVA: 0x0002EBA4 File Offset: 0x0002CDA4
		public int rootCount
		{
			get
			{
				return Scene.GetRootCountInternal(this.handle);
			}
		}

		// Token: 0x06001A81 RID: 6785 RVA: 0x0002EBC4 File Offset: 0x0002CDC4
		public GameObject[] GetRootGameObjects()
		{
			List<GameObject> list = new List<GameObject>(this.rootCount);
			this.GetRootGameObjects(list);
			return list.ToArray();
		}

		// Token: 0x06001A82 RID: 6786 RVA: 0x0002EBF4 File Offset: 0x0002CDF4
		public void GetRootGameObjects(List<GameObject> rootGameObjects)
		{
			if (rootGameObjects.Capacity < this.rootCount)
			{
				rootGameObjects.Capacity = this.rootCount;
			}
			rootGameObjects.Clear();
			if (!this.IsValid())
			{
				throw new ArgumentException("The scene is invalid.");
			}
			if (!Application.isPlaying && !this.isLoaded)
			{
				throw new ArgumentException("The scene is not loaded.");
			}
			if (this.rootCount != 0)
			{
				Scene.GetRootGameObjectsInternal(this.handle, rootGameObjects);
			}
		}

		// Token: 0x06001A83 RID: 6787 RVA: 0x0002EC78 File Offset: 0x0002CE78
		public static bool operator ==(Scene lhs, Scene rhs)
		{
			return lhs.handle == rhs.handle;
		}

		// Token: 0x06001A84 RID: 6788 RVA: 0x0002ECA0 File Offset: 0x0002CEA0
		public static bool operator !=(Scene lhs, Scene rhs)
		{
			return lhs.handle != rhs.handle;
		}

		// Token: 0x06001A85 RID: 6789 RVA: 0x0002ECC8 File Offset: 0x0002CEC8
		public override int GetHashCode()
		{
			return this.m_Handle;
		}

		// Token: 0x06001A86 RID: 6790 RVA: 0x0002ECE4 File Offset: 0x0002CEE4
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is Scene))
			{
				result = false;
			}
			else
			{
				Scene scene = (Scene)other;
				result = (this.handle == scene.handle);
			}
			return result;
		}

		// Token: 0x04000A05 RID: 2565
		[SerializeField]
		private int m_Handle;

		// Token: 0x020002FE RID: 766
		internal enum LoadingState
		{
			// Token: 0x04000A07 RID: 2567
			NotLoaded,
			// Token: 0x04000A08 RID: 2568
			Loading,
			// Token: 0x04000A09 RID: 2569
			Loaded,
			// Token: 0x04000A0A RID: 2570
			Unloading
		}
	}
}
