﻿using System;

namespace UnityEngine.SceneManagement
{
	// Token: 0x02000301 RID: 769
	[Flags]
	public enum LocalPhysicsMode
	{
		// Token: 0x04000A12 RID: 2578
		None = 0,
		// Token: 0x04000A13 RID: 2579
		Physics2D = 1,
		// Token: 0x04000A14 RID: 2580
		Physics3D = 2
	}
}
