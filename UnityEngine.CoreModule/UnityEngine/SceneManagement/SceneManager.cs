﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Events;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine.SceneManagement
{
	// Token: 0x020002FF RID: 767
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Export/SceneManager/SceneManager.bindings.h")]
	public class SceneManager
	{
		// Token: 0x06001A87 RID: 6791 RVA: 0x00002370 File Offset: 0x00000570
		public SceneManager()
		{
		}

		// Token: 0x170004F7 RID: 1271
		// (get) Token: 0x06001A88 RID: 6792
		public static extern int sceneCount { [StaticAccessor("GetSceneManager()", StaticAccessorType.Dot)] [NativeHeader("Runtime/SceneManager/SceneManager.h")] [NativeMethod("GetSceneCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170004F8 RID: 1272
		// (get) Token: 0x06001A89 RID: 6793
		public static extern int sceneCountInBuildSettings { [NativeMethod("GetNumScenesInBuildSettings")] [StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001A8A RID: 6794 RVA: 0x0002ED24 File Offset: 0x0002CF24
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		public static Scene GetActiveScene()
		{
			Scene result;
			SceneManager.GetActiveScene_Injected(out result);
			return result;
		}

		// Token: 0x06001A8B RID: 6795 RVA: 0x0002ED39 File Offset: 0x0002CF39
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		[NativeThrows]
		public static bool SetActiveScene(Scene scene)
		{
			return SceneManager.SetActiveScene_Injected(ref scene);
		}

		// Token: 0x06001A8C RID: 6796 RVA: 0x0002ED44 File Offset: 0x0002CF44
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		public static Scene GetSceneByPath(string scenePath)
		{
			Scene result;
			SceneManager.GetSceneByPath_Injected(scenePath, out result);
			return result;
		}

		// Token: 0x06001A8D RID: 6797 RVA: 0x0002ED5C File Offset: 0x0002CF5C
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		public static Scene GetSceneByName(string name)
		{
			Scene result;
			SceneManager.GetSceneByName_Injected(name, out result);
			return result;
		}

		// Token: 0x06001A8E RID: 6798 RVA: 0x0002ED74 File Offset: 0x0002CF74
		[NativeThrows]
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		public static Scene GetSceneByBuildIndex(int buildIndex)
		{
			Scene result;
			SceneManager.GetSceneByBuildIndex_Injected(buildIndex, out result);
			return result;
		}

		// Token: 0x06001A8F RID: 6799 RVA: 0x0002ED8C File Offset: 0x0002CF8C
		[NativeThrows]
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		public static Scene GetSceneAt(int index)
		{
			Scene result;
			SceneManager.GetSceneAt_Injected(index, out result);
			return result;
		}

		// Token: 0x06001A90 RID: 6800 RVA: 0x0002EDA4 File Offset: 0x0002CFA4
		[NativeThrows]
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		public static Scene CreateScene([NotNull] string sceneName, CreateSceneParameters parameters)
		{
			Scene result;
			SceneManager.CreateScene_Injected(sceneName, ref parameters, out result);
			return result;
		}

		// Token: 0x06001A91 RID: 6801 RVA: 0x0002EDBC File Offset: 0x0002CFBC
		[NativeThrows]
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		private static bool UnloadSceneInternal(Scene scene, UnloadSceneOptions options)
		{
			return SceneManager.UnloadSceneInternal_Injected(ref scene, options);
		}

		// Token: 0x06001A92 RID: 6802 RVA: 0x0002EDC6 File Offset: 0x0002CFC6
		[NativeThrows]
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		private static AsyncOperation UnloadSceneAsyncInternal(Scene scene, UnloadSceneOptions options)
		{
			return SceneManager.UnloadSceneAsyncInternal_Injected(ref scene, options);
		}

		// Token: 0x06001A93 RID: 6803 RVA: 0x0002EDD0 File Offset: 0x0002CFD0
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		[NativeThrows]
		private static AsyncOperation LoadSceneAsyncNameIndexInternal(string sceneName, int sceneBuildIndex, LoadSceneParameters parameters, bool mustCompleteNextFrame)
		{
			return SceneManager.LoadSceneAsyncNameIndexInternal_Injected(sceneName, sceneBuildIndex, ref parameters, mustCompleteNextFrame);
		}

		// Token: 0x06001A94 RID: 6804
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AsyncOperation UnloadSceneNameIndexInternal(string sceneName, int sceneBuildIndex, bool immediately, UnloadSceneOptions options, out bool outSuccess);

		// Token: 0x06001A95 RID: 6805 RVA: 0x0002EDDC File Offset: 0x0002CFDC
		[NativeThrows]
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		public static void MergeScenes(Scene sourceScene, Scene destinationScene)
		{
			SceneManager.MergeScenes_Injected(ref sourceScene, ref destinationScene);
		}

		// Token: 0x06001A96 RID: 6806 RVA: 0x0002EDE7 File Offset: 0x0002CFE7
		[StaticAccessor("SceneManagerBindings", StaticAccessorType.DoubleColon)]
		[NativeThrows]
		public static void MoveGameObjectToScene([NotNull] GameObject go, Scene scene)
		{
			SceneManager.MoveGameObjectToScene_Injected(go, ref scene);
		}

		// Token: 0x14000013 RID: 19
		// (add) Token: 0x06001A97 RID: 6807 RVA: 0x0002EDF4 File Offset: 0x0002CFF4
		// (remove) Token: 0x06001A98 RID: 6808 RVA: 0x0002EE28 File Offset: 0x0002D028
		public static event UnityAction<Scene, LoadSceneMode> sceneLoaded
		{
			add
			{
				UnityAction<Scene, LoadSceneMode> unityAction = SceneManager.sceneLoaded;
				UnityAction<Scene, LoadSceneMode> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<Scene, LoadSceneMode>>(ref SceneManager.sceneLoaded, (UnityAction<Scene, LoadSceneMode>)Delegate.Combine(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
			remove
			{
				UnityAction<Scene, LoadSceneMode> unityAction = SceneManager.sceneLoaded;
				UnityAction<Scene, LoadSceneMode> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<Scene, LoadSceneMode>>(ref SceneManager.sceneLoaded, (UnityAction<Scene, LoadSceneMode>)Delegate.Remove(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
		}

		// Token: 0x14000014 RID: 20
		// (add) Token: 0x06001A99 RID: 6809 RVA: 0x0002EE5C File Offset: 0x0002D05C
		// (remove) Token: 0x06001A9A RID: 6810 RVA: 0x0002EE90 File Offset: 0x0002D090
		public static event UnityAction<Scene> sceneUnloaded
		{
			add
			{
				UnityAction<Scene> unityAction = SceneManager.sceneUnloaded;
				UnityAction<Scene> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<Scene>>(ref SceneManager.sceneUnloaded, (UnityAction<Scene>)Delegate.Combine(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
			remove
			{
				UnityAction<Scene> unityAction = SceneManager.sceneUnloaded;
				UnityAction<Scene> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<Scene>>(ref SceneManager.sceneUnloaded, (UnityAction<Scene>)Delegate.Remove(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
		}

		// Token: 0x14000015 RID: 21
		// (add) Token: 0x06001A9B RID: 6811 RVA: 0x0002EEC4 File Offset: 0x0002D0C4
		// (remove) Token: 0x06001A9C RID: 6812 RVA: 0x0002EEF8 File Offset: 0x0002D0F8
		public static event UnityAction<Scene, Scene> activeSceneChanged
		{
			add
			{
				UnityAction<Scene, Scene> unityAction = SceneManager.activeSceneChanged;
				UnityAction<Scene, Scene> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<Scene, Scene>>(ref SceneManager.activeSceneChanged, (UnityAction<Scene, Scene>)Delegate.Combine(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
			remove
			{
				UnityAction<Scene, Scene> unityAction = SceneManager.activeSceneChanged;
				UnityAction<Scene, Scene> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<Scene, Scene>>(ref SceneManager.activeSceneChanged, (UnityAction<Scene, Scene>)Delegate.Remove(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
		}

		// Token: 0x06001A9D RID: 6813 RVA: 0x0002EF2C File Offset: 0x0002D12C
		[Obsolete("Use SceneManager.sceneCount and SceneManager.GetSceneAt(int index) to loop the all scenes instead.")]
		public static Scene[] GetAllScenes()
		{
			Scene[] array = new Scene[SceneManager.sceneCount];
			for (int i = 0; i < SceneManager.sceneCount; i++)
			{
				array[i] = SceneManager.GetSceneAt(i);
			}
			return array;
		}

		// Token: 0x06001A9E RID: 6814 RVA: 0x0002EF78 File Offset: 0x0002D178
		public static Scene CreateScene(string sceneName)
		{
			CreateSceneParameters parameters = new CreateSceneParameters(LocalPhysicsMode.None);
			return SceneManager.CreateScene(sceneName, parameters);
		}

		// Token: 0x06001A9F RID: 6815 RVA: 0x0002EF9C File Offset: 0x0002D19C
		public static void LoadScene(string sceneName, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
		{
			LoadSceneParameters parameters = new LoadSceneParameters(mode);
			SceneManager.LoadScene(sceneName, parameters);
		}

		// Token: 0x06001AA0 RID: 6816 RVA: 0x0002EFBC File Offset: 0x0002D1BC
		[ExcludeFromDocs]
		public static void LoadScene(string sceneName)
		{
			LoadSceneParameters parameters = new LoadSceneParameters(LoadSceneMode.Single);
			SceneManager.LoadScene(sceneName, parameters);
		}

		// Token: 0x06001AA1 RID: 6817 RVA: 0x0002EFDC File Offset: 0x0002D1DC
		public static Scene LoadScene(string sceneName, LoadSceneParameters parameters)
		{
			SceneManager.LoadSceneAsyncNameIndexInternal(sceneName, -1, parameters, true);
			return SceneManager.GetSceneAt(SceneManager.sceneCount - 1);
		}

		// Token: 0x06001AA2 RID: 6818 RVA: 0x0002F008 File Offset: 0x0002D208
		public static void LoadScene(int sceneBuildIndex, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
		{
			LoadSceneParameters parameters = new LoadSceneParameters(mode);
			SceneManager.LoadScene(sceneBuildIndex, parameters);
		}

		// Token: 0x06001AA3 RID: 6819 RVA: 0x0002F028 File Offset: 0x0002D228
		[ExcludeFromDocs]
		public static void LoadScene(int sceneBuildIndex)
		{
			LoadSceneParameters parameters = new LoadSceneParameters(LoadSceneMode.Single);
			SceneManager.LoadScene(sceneBuildIndex, parameters);
		}

		// Token: 0x06001AA4 RID: 6820 RVA: 0x0002F048 File Offset: 0x0002D248
		public static Scene LoadScene(int sceneBuildIndex, LoadSceneParameters parameters)
		{
			SceneManager.LoadSceneAsyncNameIndexInternal(null, sceneBuildIndex, parameters, true);
			return SceneManager.GetSceneAt(SceneManager.sceneCount - 1);
		}

		// Token: 0x06001AA5 RID: 6821 RVA: 0x0002F074 File Offset: 0x0002D274
		public static AsyncOperation LoadSceneAsync(int sceneBuildIndex, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
		{
			LoadSceneParameters parameters = new LoadSceneParameters(mode);
			return SceneManager.LoadSceneAsync(sceneBuildIndex, parameters);
		}

		// Token: 0x06001AA6 RID: 6822 RVA: 0x0002F098 File Offset: 0x0002D298
		[ExcludeFromDocs]
		public static AsyncOperation LoadSceneAsync(int sceneBuildIndex)
		{
			LoadSceneParameters parameters = new LoadSceneParameters(LoadSceneMode.Single);
			return SceneManager.LoadSceneAsync(sceneBuildIndex, parameters);
		}

		// Token: 0x06001AA7 RID: 6823 RVA: 0x0002F0BC File Offset: 0x0002D2BC
		public static AsyncOperation LoadSceneAsync(int sceneBuildIndex, LoadSceneParameters parameters)
		{
			return SceneManager.LoadSceneAsyncNameIndexInternal(null, sceneBuildIndex, parameters, false);
		}

		// Token: 0x06001AA8 RID: 6824 RVA: 0x0002F0DC File Offset: 0x0002D2DC
		public static AsyncOperation LoadSceneAsync(string sceneName, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
		{
			LoadSceneParameters parameters = new LoadSceneParameters(mode);
			return SceneManager.LoadSceneAsync(sceneName, parameters);
		}

		// Token: 0x06001AA9 RID: 6825 RVA: 0x0002F100 File Offset: 0x0002D300
		[ExcludeFromDocs]
		public static AsyncOperation LoadSceneAsync(string sceneName)
		{
			LoadSceneParameters parameters = new LoadSceneParameters(LoadSceneMode.Single);
			return SceneManager.LoadSceneAsync(sceneName, parameters);
		}

		// Token: 0x06001AAA RID: 6826 RVA: 0x0002F124 File Offset: 0x0002D324
		public static AsyncOperation LoadSceneAsync(string sceneName, LoadSceneParameters parameters)
		{
			return SceneManager.LoadSceneAsyncNameIndexInternal(sceneName, -1, parameters, false);
		}

		// Token: 0x06001AAB RID: 6827 RVA: 0x0002F144 File Offset: 0x0002D344
		[Obsolete("Use SceneManager.UnloadSceneAsync. This function is not safe to use during triggers and under other circumstances. See Scripting reference for more details.")]
		public static bool UnloadScene(Scene scene)
		{
			return SceneManager.UnloadSceneInternal(scene, UnloadSceneOptions.None);
		}

		// Token: 0x06001AAC RID: 6828 RVA: 0x0002F160 File Offset: 0x0002D360
		[Obsolete("Use SceneManager.UnloadSceneAsync. This function is not safe to use during triggers and under other circumstances. See Scripting reference for more details.")]
		public static bool UnloadScene(int sceneBuildIndex)
		{
			bool result;
			SceneManager.UnloadSceneNameIndexInternal("", sceneBuildIndex, true, UnloadSceneOptions.None, out result);
			return result;
		}

		// Token: 0x06001AAD RID: 6829 RVA: 0x0002F188 File Offset: 0x0002D388
		[Obsolete("Use SceneManager.UnloadSceneAsync. This function is not safe to use during triggers and under other circumstances. See Scripting reference for more details.")]
		public static bool UnloadScene(string sceneName)
		{
			bool result;
			SceneManager.UnloadSceneNameIndexInternal(sceneName, -1, true, UnloadSceneOptions.None, out result);
			return result;
		}

		// Token: 0x06001AAE RID: 6830 RVA: 0x0002F1AC File Offset: 0x0002D3AC
		public static AsyncOperation UnloadSceneAsync(int sceneBuildIndex)
		{
			bool flag;
			return SceneManager.UnloadSceneNameIndexInternal("", sceneBuildIndex, false, UnloadSceneOptions.None, out flag);
		}

		// Token: 0x06001AAF RID: 6831 RVA: 0x0002F1D0 File Offset: 0x0002D3D0
		public static AsyncOperation UnloadSceneAsync(string sceneName)
		{
			bool flag;
			return SceneManager.UnloadSceneNameIndexInternal(sceneName, -1, false, UnloadSceneOptions.None, out flag);
		}

		// Token: 0x06001AB0 RID: 6832 RVA: 0x0002F1F0 File Offset: 0x0002D3F0
		public static AsyncOperation UnloadSceneAsync(Scene scene)
		{
			return SceneManager.UnloadSceneAsyncInternal(scene, UnloadSceneOptions.None);
		}

		// Token: 0x06001AB1 RID: 6833 RVA: 0x0002F20C File Offset: 0x0002D40C
		public static AsyncOperation UnloadSceneAsync(int sceneBuildIndex, UnloadSceneOptions options)
		{
			bool flag;
			return SceneManager.UnloadSceneNameIndexInternal("", sceneBuildIndex, false, options, out flag);
		}

		// Token: 0x06001AB2 RID: 6834 RVA: 0x0002F230 File Offset: 0x0002D430
		public static AsyncOperation UnloadSceneAsync(string sceneName, UnloadSceneOptions options)
		{
			bool flag;
			return SceneManager.UnloadSceneNameIndexInternal(sceneName, -1, false, options, out flag);
		}

		// Token: 0x06001AB3 RID: 6835 RVA: 0x0002F250 File Offset: 0x0002D450
		public static AsyncOperation UnloadSceneAsync(Scene scene, UnloadSceneOptions options)
		{
			return SceneManager.UnloadSceneAsyncInternal(scene, options);
		}

		// Token: 0x06001AB4 RID: 6836 RVA: 0x0002F26C File Offset: 0x0002D46C
		[RequiredByNativeCode]
		private static void Internal_SceneLoaded(Scene scene, LoadSceneMode mode)
		{
			if (SceneManager.sceneLoaded != null)
			{
				SceneManager.sceneLoaded(scene, mode);
			}
		}

		// Token: 0x06001AB5 RID: 6837 RVA: 0x0002F287 File Offset: 0x0002D487
		[RequiredByNativeCode]
		private static void Internal_SceneUnloaded(Scene scene)
		{
			if (SceneManager.sceneUnloaded != null)
			{
				SceneManager.sceneUnloaded(scene);
			}
		}

		// Token: 0x06001AB6 RID: 6838 RVA: 0x0002F2A1 File Offset: 0x0002D4A1
		[RequiredByNativeCode]
		private static void Internal_ActiveSceneChanged(Scene previousActiveScene, Scene newActiveScene)
		{
			if (SceneManager.activeSceneChanged != null)
			{
				SceneManager.activeSceneChanged(previousActiveScene, newActiveScene);
			}
		}

		// Token: 0x06001AB7 RID: 6839
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetActiveScene_Injected(out Scene ret);

		// Token: 0x06001AB8 RID: 6840
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SetActiveScene_Injected(ref Scene scene);

		// Token: 0x06001AB9 RID: 6841
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetSceneByPath_Injected(string scenePath, out Scene ret);

		// Token: 0x06001ABA RID: 6842
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetSceneByName_Injected(string name, out Scene ret);

		// Token: 0x06001ABB RID: 6843
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetSceneByBuildIndex_Injected(int buildIndex, out Scene ret);

		// Token: 0x06001ABC RID: 6844
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetSceneAt_Injected(int index, out Scene ret);

		// Token: 0x06001ABD RID: 6845
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CreateScene_Injected(string sceneName, ref CreateSceneParameters parameters, out Scene ret);

		// Token: 0x06001ABE RID: 6846
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool UnloadSceneInternal_Injected(ref Scene scene, UnloadSceneOptions options);

		// Token: 0x06001ABF RID: 6847
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AsyncOperation UnloadSceneAsyncInternal_Injected(ref Scene scene, UnloadSceneOptions options);

		// Token: 0x06001AC0 RID: 6848
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AsyncOperation LoadSceneAsyncNameIndexInternal_Injected(string sceneName, int sceneBuildIndex, ref LoadSceneParameters parameters, bool mustCompleteNextFrame);

		// Token: 0x06001AC1 RID: 6849
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void MergeScenes_Injected(ref Scene sourceScene, ref Scene destinationScene);

		// Token: 0x06001AC2 RID: 6850
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void MoveGameObjectToScene_Injected(GameObject go, ref Scene scene);

		// Token: 0x04000A0B RID: 2571
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static UnityAction<Scene, LoadSceneMode> sceneLoaded;

		// Token: 0x04000A0C RID: 2572
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static UnityAction<Scene> sceneUnloaded;

		// Token: 0x04000A0D RID: 2573
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static UnityAction<Scene, Scene> activeSceneChanged;
	}
}
