﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200005C RID: 92
	[NativeHeader("Runtime/Export/CrashReport.bindings.h")]
	public sealed class CrashReport
	{
		// Token: 0x060004F4 RID: 1268 RVA: 0x0000C914 File Offset: 0x0000AB14
		private CrashReport(string id, DateTime time, string text)
		{
			this.id = id;
			this.time = time;
			this.text = text;
		}

		// Token: 0x060004F5 RID: 1269 RVA: 0x0000C934 File Offset: 0x0000AB34
		private static int Compare(CrashReport c1, CrashReport c2)
		{
			long ticks = c1.time.Ticks;
			long ticks2 = c2.time.Ticks;
			int result;
			if (ticks > ticks2)
			{
				result = 1;
			}
			else if (ticks < ticks2)
			{
				result = -1;
			}
			else
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x060004F6 RID: 1270 RVA: 0x0000C988 File Offset: 0x0000AB88
		private static void PopulateReports()
		{
			object obj = CrashReport.reportsLock;
			lock (obj)
			{
				if (CrashReport.internalReports == null)
				{
					string[] reports = CrashReport.GetReports();
					CrashReport.internalReports = new List<CrashReport>(reports.Length);
					foreach (string text in reports)
					{
						double value;
						string reportData = CrashReport.GetReportData(text, out value);
						DateTime dateTime = new DateTime(1970, 1, 1);
						DateTime dateTime2 = dateTime.AddSeconds(value);
						CrashReport.internalReports.Add(new CrashReport(text, dateTime2, reportData));
					}
					List<CrashReport> list = CrashReport.internalReports;
					if (CrashReport.<>f__mg$cache0 == null)
					{
						CrashReport.<>f__mg$cache0 = new Comparison<CrashReport>(CrashReport.Compare);
					}
					list.Sort(CrashReport.<>f__mg$cache0);
				}
			}
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x060004F7 RID: 1271 RVA: 0x0000CA64 File Offset: 0x0000AC64
		public static CrashReport[] reports
		{
			get
			{
				CrashReport.PopulateReports();
				object obj = CrashReport.reportsLock;
				CrashReport[] result;
				lock (obj)
				{
					result = CrashReport.internalReports.ToArray();
				}
				return result;
			}
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x060004F8 RID: 1272 RVA: 0x0000CAAC File Offset: 0x0000ACAC
		public static CrashReport lastReport
		{
			get
			{
				CrashReport.PopulateReports();
				object obj = CrashReport.reportsLock;
				lock (obj)
				{
					if (CrashReport.internalReports.Count > 0)
					{
						return CrashReport.internalReports[CrashReport.internalReports.Count - 1];
					}
				}
				return null;
			}
		}

		// Token: 0x060004F9 RID: 1273 RVA: 0x0000CB20 File Offset: 0x0000AD20
		public static void RemoveAll()
		{
			foreach (CrashReport crashReport in CrashReport.reports)
			{
				crashReport.Remove();
			}
		}

		// Token: 0x060004FA RID: 1274 RVA: 0x0000CB54 File Offset: 0x0000AD54
		public void Remove()
		{
			if (CrashReport.RemoveReport(this.id))
			{
				object obj = CrashReport.reportsLock;
				lock (obj)
				{
					CrashReport.internalReports.Remove(this);
				}
			}
		}

		// Token: 0x060004FB RID: 1275
		[FreeFunction(Name = "CrashReport_Bindings::GetReports", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetReports();

		// Token: 0x060004FC RID: 1276
		[FreeFunction(Name = "CrashReport_Bindings::GetReportData", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetReportData(string id, out double secondsSinceUnixEpoch);

		// Token: 0x060004FD RID: 1277
		[FreeFunction(Name = "CrashReport_Bindings::RemoveReport", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool RemoveReport(string id);

		// Token: 0x060004FE RID: 1278 RVA: 0x0000CBAC File Offset: 0x0000ADAC
		// Note: this type is marked as 'beforefieldinit'.
		static CrashReport()
		{
		}

		// Token: 0x04000119 RID: 281
		private static List<CrashReport> internalReports;

		// Token: 0x0400011A RID: 282
		private static object reportsLock = new object();

		// Token: 0x0400011B RID: 283
		private readonly string id;

		// Token: 0x0400011C RID: 284
		public readonly DateTime time;

		// Token: 0x0400011D RID: 285
		public readonly string text;

		// Token: 0x0400011E RID: 286
		[CompilerGenerated]
		private static Comparison<CrashReport> <>f__mg$cache0;
	}
}
