﻿using System;

namespace UnityEngine
{
	// Token: 0x0200014A RID: 330
	[Serializable]
	public struct FrustumPlanes
	{
		// Token: 0x040006B9 RID: 1721
		public float left;

		// Token: 0x040006BA RID: 1722
		public float right;

		// Token: 0x040006BB RID: 1723
		public float bottom;

		// Token: 0x040006BC RID: 1724
		public float top;

		// Token: 0x040006BD RID: 1725
		public float zNear;

		// Token: 0x040006BE RID: 1726
		public float zFar;
	}
}
