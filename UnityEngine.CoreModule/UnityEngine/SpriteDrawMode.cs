﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035A RID: 858
	public enum SpriteDrawMode
	{
		// Token: 0x04000AE1 RID: 2785
		Simple,
		// Token: 0x04000AE2 RID: 2786
		Sliced,
		// Token: 0x04000AE3 RID: 2787
		Tiled
	}
}
