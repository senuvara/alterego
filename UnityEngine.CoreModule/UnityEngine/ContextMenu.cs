﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200002C RID: 44
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	[RequiredByNativeCode]
	public sealed class ContextMenu : Attribute
	{
		// Token: 0x06000288 RID: 648 RVA: 0x00008AAB File Offset: 0x00006CAB
		public ContextMenu(string itemName) : this(itemName, false)
		{
		}

		// Token: 0x06000289 RID: 649 RVA: 0x00008AB6 File Offset: 0x00006CB6
		public ContextMenu(string itemName, bool isValidateFunction) : this(itemName, isValidateFunction, 1000000)
		{
		}

		// Token: 0x0600028A RID: 650 RVA: 0x00008AC6 File Offset: 0x00006CC6
		public ContextMenu(string itemName, bool isValidateFunction, int priority)
		{
			this.menuItem = itemName;
			this.validate = isValidateFunction;
			this.priority = priority;
		}

		// Token: 0x0400006A RID: 106
		public readonly string menuItem;

		// Token: 0x0400006B RID: 107
		public readonly bool validate;

		// Token: 0x0400006C RID: 108
		public readonly int priority;
	}
}
