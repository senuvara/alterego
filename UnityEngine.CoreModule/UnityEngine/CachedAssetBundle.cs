﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000046 RID: 70
	[UsedByNativeCode]
	public struct CachedAssetBundle
	{
		// Token: 0x0600031C RID: 796 RVA: 0x00009E00 File Offset: 0x00008000
		public CachedAssetBundle(string name, Hash128 hash)
		{
			this.m_Name = name;
			this.m_Hash = hash;
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x0600031D RID: 797 RVA: 0x00009E14 File Offset: 0x00008014
		// (set) Token: 0x0600031E RID: 798 RVA: 0x00009E2F File Offset: 0x0000802F
		public string name
		{
			get
			{
				return this.m_Name;
			}
			set
			{
				this.m_Name = value;
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600031F RID: 799 RVA: 0x00009E3C File Offset: 0x0000803C
		// (set) Token: 0x06000320 RID: 800 RVA: 0x00009E57 File Offset: 0x00008057
		public Hash128 hash
		{
			get
			{
				return this.m_Hash;
			}
			set
			{
				this.m_Hash = value;
			}
		}

		// Token: 0x040000F0 RID: 240
		private string m_Name;

		// Token: 0x040000F1 RID: 241
		private Hash128 m_Hash;
	}
}
