﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003B RID: 59
	public enum ThreadPriority
	{
		// Token: 0x040000E0 RID: 224
		Low,
		// Token: 0x040000E1 RID: 225
		BelowNormal,
		// Token: 0x040000E2 RID: 226
		Normal,
		// Token: 0x040000E3 RID: 227
		High = 4
	}
}
