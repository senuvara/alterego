﻿using System;
using Unity.Collections.LowLevel.Unsafe;

namespace UnityEngine
{
	// Token: 0x0200011E RID: 286
	public static class HashUtilities
	{
		// Token: 0x06000C14 RID: 3092 RVA: 0x000136AC File Offset: 0x000118AC
		public unsafe static void AppendHash(ref Hash128 inHash, ref Hash128 outHash)
		{
			fixed (Hash128* hash = &outHash)
			{
				fixed (Hash128* data = &inHash)
				{
					HashUnsafeUtilities.ComputeHash128((void*)data, (ulong)((long)sizeof(Hash128)), hash);
				}
			}
		}

		// Token: 0x06000C15 RID: 3093 RVA: 0x000136D8 File Offset: 0x000118D8
		public unsafe static void QuantisedMatrixHash(ref Matrix4x4 value, ref Hash128 hash)
		{
			fixed (Hash128* hash2 = &hash)
			{
				int* ptr = stackalloc int[checked(16 * 4)];
				for (int i = 0; i < 16; i++)
				{
					ptr[i] = (int)(value[i] * 1000f + 0.5f);
				}
				HashUnsafeUtilities.ComputeHash128((void*)ptr, 64UL, hash2);
			}
		}

		// Token: 0x06000C16 RID: 3094 RVA: 0x00013730 File Offset: 0x00011930
		public unsafe static void QuantisedVectorHash(ref Vector3 value, ref Hash128 hash)
		{
			fixed (Hash128* hash2 = &hash)
			{
				int* ptr = stackalloc int[checked(3 * 4)];
				for (int i = 0; i < 3; i++)
				{
					ptr[i] = (int)(value[i] * 1000f + 0.5f);
				}
				HashUnsafeUtilities.ComputeHash128((void*)ptr, 12UL, hash2);
			}
		}

		// Token: 0x06000C17 RID: 3095 RVA: 0x00013784 File Offset: 0x00011984
		public unsafe static void ComputeHash128<T>(ref T value, ref Hash128 hash) where T : struct
		{
			void* data = UnsafeUtility.AddressOf<T>(ref value);
			ulong dataSize = (ulong)((long)UnsafeUtility.SizeOf<T>());
			Hash128* hash2 = (Hash128*)UnsafeUtility.AddressOf<Hash128>(ref hash);
			HashUnsafeUtilities.ComputeHash128(data, dataSize, hash2);
		}
	}
}
