﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x02000231 RID: 561
	public struct RenderTextureDescriptor
	{
		// Token: 0x0600142D RID: 5165 RVA: 0x00022733 File Offset: 0x00020933
		public RenderTextureDescriptor(int width, int height)
		{
			this = new RenderTextureDescriptor(width, height, RenderTextureFormat.Default, 0);
		}

		// Token: 0x0600142E RID: 5166 RVA: 0x00022740 File Offset: 0x00020940
		public RenderTextureDescriptor(int width, int height, RenderTextureFormat colorFormat)
		{
			this = new RenderTextureDescriptor(width, height, colorFormat, 0);
		}

		// Token: 0x0600142F RID: 5167 RVA: 0x00022750 File Offset: 0x00020950
		public RenderTextureDescriptor(int width, int height, RenderTextureFormat colorFormat, int depthBufferBits)
		{
			this = default(RenderTextureDescriptor);
			this.width = width;
			this.height = height;
			this.volumeDepth = 1;
			this.msaaSamples = 1;
			this.colorFormat = colorFormat;
			this.depthBufferBits = depthBufferBits;
			this.dimension = TextureDimension.Tex2D;
			this.shadowSamplingMode = ShadowSamplingMode.None;
			this.vrUsage = VRTextureUsage.None;
			this._flags = (RenderTextureCreationFlags.AutoGenerateMips | RenderTextureCreationFlags.AllowVerticalFlip);
			this.memoryless = RenderTextureMemoryless.None;
		}

		// Token: 0x170003E0 RID: 992
		// (get) Token: 0x06001430 RID: 5168 RVA: 0x000227B8 File Offset: 0x000209B8
		// (set) Token: 0x06001431 RID: 5169 RVA: 0x000227D2 File Offset: 0x000209D2
		public int width
		{
			[CompilerGenerated]
			get
			{
				return this.<width>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<width>k__BackingField = value;
			}
		}

		// Token: 0x170003E1 RID: 993
		// (get) Token: 0x06001432 RID: 5170 RVA: 0x000227DC File Offset: 0x000209DC
		// (set) Token: 0x06001433 RID: 5171 RVA: 0x000227F6 File Offset: 0x000209F6
		public int height
		{
			[CompilerGenerated]
			get
			{
				return this.<height>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<height>k__BackingField = value;
			}
		}

		// Token: 0x170003E2 RID: 994
		// (get) Token: 0x06001434 RID: 5172 RVA: 0x00022800 File Offset: 0x00020A00
		// (set) Token: 0x06001435 RID: 5173 RVA: 0x0002281A File Offset: 0x00020A1A
		public int msaaSamples
		{
			[CompilerGenerated]
			get
			{
				return this.<msaaSamples>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<msaaSamples>k__BackingField = value;
			}
		}

		// Token: 0x170003E3 RID: 995
		// (get) Token: 0x06001436 RID: 5174 RVA: 0x00022824 File Offset: 0x00020A24
		// (set) Token: 0x06001437 RID: 5175 RVA: 0x0002283E File Offset: 0x00020A3E
		public int volumeDepth
		{
			[CompilerGenerated]
			get
			{
				return this.<volumeDepth>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<volumeDepth>k__BackingField = value;
			}
		}

		// Token: 0x170003E4 RID: 996
		// (get) Token: 0x06001438 RID: 5176 RVA: 0x00022848 File Offset: 0x00020A48
		// (set) Token: 0x06001439 RID: 5177 RVA: 0x00022862 File Offset: 0x00020A62
		public RenderTextureFormat colorFormat
		{
			[CompilerGenerated]
			get
			{
				return this.<colorFormat>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<colorFormat>k__BackingField = value;
			}
		}

		// Token: 0x170003E5 RID: 997
		// (get) Token: 0x0600143A RID: 5178 RVA: 0x0002286C File Offset: 0x00020A6C
		// (set) Token: 0x0600143B RID: 5179 RVA: 0x0002288D File Offset: 0x00020A8D
		public int depthBufferBits
		{
			get
			{
				return RenderTextureDescriptor.depthFormatBits[this._depthBufferBits];
			}
			set
			{
				if (value <= 0)
				{
					this._depthBufferBits = 0;
				}
				else if (value <= 16)
				{
					this._depthBufferBits = 1;
				}
				else
				{
					this._depthBufferBits = 2;
				}
			}
		}

		// Token: 0x170003E6 RID: 998
		// (get) Token: 0x0600143C RID: 5180 RVA: 0x000228C0 File Offset: 0x00020AC0
		// (set) Token: 0x0600143D RID: 5181 RVA: 0x000228DA File Offset: 0x00020ADA
		public TextureDimension dimension
		{
			[CompilerGenerated]
			get
			{
				return this.<dimension>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<dimension>k__BackingField = value;
			}
		}

		// Token: 0x170003E7 RID: 999
		// (get) Token: 0x0600143E RID: 5182 RVA: 0x000228E4 File Offset: 0x00020AE4
		// (set) Token: 0x0600143F RID: 5183 RVA: 0x000228FE File Offset: 0x00020AFE
		public ShadowSamplingMode shadowSamplingMode
		{
			[CompilerGenerated]
			get
			{
				return this.<shadowSamplingMode>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<shadowSamplingMode>k__BackingField = value;
			}
		}

		// Token: 0x170003E8 RID: 1000
		// (get) Token: 0x06001440 RID: 5184 RVA: 0x00022908 File Offset: 0x00020B08
		// (set) Token: 0x06001441 RID: 5185 RVA: 0x00022922 File Offset: 0x00020B22
		public VRTextureUsage vrUsage
		{
			[CompilerGenerated]
			get
			{
				return this.<vrUsage>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<vrUsage>k__BackingField = value;
			}
		}

		// Token: 0x170003E9 RID: 1001
		// (get) Token: 0x06001442 RID: 5186 RVA: 0x0002292C File Offset: 0x00020B2C
		public RenderTextureCreationFlags flags
		{
			get
			{
				return this._flags;
			}
		}

		// Token: 0x170003EA RID: 1002
		// (get) Token: 0x06001443 RID: 5187 RVA: 0x00022948 File Offset: 0x00020B48
		// (set) Token: 0x06001444 RID: 5188 RVA: 0x00022962 File Offset: 0x00020B62
		public RenderTextureMemoryless memoryless
		{
			[CompilerGenerated]
			get
			{
				return this.<memoryless>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<memoryless>k__BackingField = value;
			}
		}

		// Token: 0x06001445 RID: 5189 RVA: 0x0002296B File Offset: 0x00020B6B
		private void SetOrClearRenderTextureCreationFlag(bool value, RenderTextureCreationFlags flag)
		{
			if (value)
			{
				this._flags |= flag;
			}
			else
			{
				this._flags &= ~flag;
			}
		}

		// Token: 0x170003EB RID: 1003
		// (get) Token: 0x06001446 RID: 5190 RVA: 0x0002299C File Offset: 0x00020B9C
		// (set) Token: 0x06001447 RID: 5191 RVA: 0x000229BF File Offset: 0x00020BBF
		public bool sRGB
		{
			get
			{
				return (this._flags & RenderTextureCreationFlags.SRGB) != (RenderTextureCreationFlags)0;
			}
			set
			{
				this.SetOrClearRenderTextureCreationFlag(value, RenderTextureCreationFlags.SRGB);
			}
		}

		// Token: 0x170003EC RID: 1004
		// (get) Token: 0x06001448 RID: 5192 RVA: 0x000229CC File Offset: 0x00020BCC
		// (set) Token: 0x06001449 RID: 5193 RVA: 0x000229EF File Offset: 0x00020BEF
		public bool useMipMap
		{
			get
			{
				return (this._flags & RenderTextureCreationFlags.MipMap) != (RenderTextureCreationFlags)0;
			}
			set
			{
				this.SetOrClearRenderTextureCreationFlag(value, RenderTextureCreationFlags.MipMap);
			}
		}

		// Token: 0x170003ED RID: 1005
		// (get) Token: 0x0600144A RID: 5194 RVA: 0x000229FC File Offset: 0x00020BFC
		// (set) Token: 0x0600144B RID: 5195 RVA: 0x00022A1F File Offset: 0x00020C1F
		public bool autoGenerateMips
		{
			get
			{
				return (this._flags & RenderTextureCreationFlags.AutoGenerateMips) != (RenderTextureCreationFlags)0;
			}
			set
			{
				this.SetOrClearRenderTextureCreationFlag(value, RenderTextureCreationFlags.AutoGenerateMips);
			}
		}

		// Token: 0x170003EE RID: 1006
		// (get) Token: 0x0600144C RID: 5196 RVA: 0x00022A2C File Offset: 0x00020C2C
		// (set) Token: 0x0600144D RID: 5197 RVA: 0x00022A50 File Offset: 0x00020C50
		public bool enableRandomWrite
		{
			get
			{
				return (this._flags & RenderTextureCreationFlags.EnableRandomWrite) != (RenderTextureCreationFlags)0;
			}
			set
			{
				this.SetOrClearRenderTextureCreationFlag(value, RenderTextureCreationFlags.EnableRandomWrite);
			}
		}

		// Token: 0x170003EF RID: 1007
		// (get) Token: 0x0600144E RID: 5198 RVA: 0x00022A5C File Offset: 0x00020C5C
		// (set) Token: 0x0600144F RID: 5199 RVA: 0x00022A83 File Offset: 0x00020C83
		public bool bindMS
		{
			get
			{
				return (this._flags & RenderTextureCreationFlags.BindMS) != (RenderTextureCreationFlags)0;
			}
			set
			{
				this.SetOrClearRenderTextureCreationFlag(value, RenderTextureCreationFlags.BindMS);
			}
		}

		// Token: 0x170003F0 RID: 1008
		// (get) Token: 0x06001450 RID: 5200 RVA: 0x00022A94 File Offset: 0x00020C94
		// (set) Token: 0x06001451 RID: 5201 RVA: 0x00022AB8 File Offset: 0x00020CB8
		internal bool createdFromScript
		{
			get
			{
				return (this._flags & RenderTextureCreationFlags.CreatedFromScript) != (RenderTextureCreationFlags)0;
			}
			set
			{
				this.SetOrClearRenderTextureCreationFlag(value, RenderTextureCreationFlags.CreatedFromScript);
			}
		}

		// Token: 0x170003F1 RID: 1009
		// (get) Token: 0x06001452 RID: 5202 RVA: 0x00022AC4 File Offset: 0x00020CC4
		// (set) Token: 0x06001453 RID: 5203 RVA: 0x00022AEB File Offset: 0x00020CEB
		internal bool useDynamicScale
		{
			get
			{
				return (this._flags & RenderTextureCreationFlags.DynamicallyScalable) != (RenderTextureCreationFlags)0;
			}
			set
			{
				this.SetOrClearRenderTextureCreationFlag(value, RenderTextureCreationFlags.DynamicallyScalable);
			}
		}

		// Token: 0x06001454 RID: 5204 RVA: 0x00022AFA File Offset: 0x00020CFA
		// Note: this type is marked as 'beforefieldinit'.
		static RenderTextureDescriptor()
		{
		}

		// Token: 0x040007B5 RID: 1973
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <width>k__BackingField;

		// Token: 0x040007B6 RID: 1974
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <height>k__BackingField;

		// Token: 0x040007B7 RID: 1975
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <msaaSamples>k__BackingField;

		// Token: 0x040007B8 RID: 1976
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <volumeDepth>k__BackingField;

		// Token: 0x040007B9 RID: 1977
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private RenderTextureFormat <colorFormat>k__BackingField;

		// Token: 0x040007BA RID: 1978
		private int _depthBufferBits;

		// Token: 0x040007BB RID: 1979
		private static int[] depthFormatBits = new int[]
		{
			0,
			16,
			24
		};

		// Token: 0x040007BC RID: 1980
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TextureDimension <dimension>k__BackingField;

		// Token: 0x040007BD RID: 1981
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ShadowSamplingMode <shadowSamplingMode>k__BackingField;

		// Token: 0x040007BE RID: 1982
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private VRTextureUsage <vrUsage>k__BackingField;

		// Token: 0x040007BF RID: 1983
		private RenderTextureCreationFlags _flags;

		// Token: 0x040007C0 RID: 1984
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private RenderTextureMemoryless <memoryless>k__BackingField;
	}
}
