﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200025A RID: 602
	[RequiredByNativeCode]
	public sealed class WaitForFixedUpdate : YieldInstruction
	{
		// Token: 0x06001625 RID: 5669 RVA: 0x000084D7 File Offset: 0x000066D7
		public WaitForFixedUpdate()
		{
		}
	}
}
