﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020000C7 RID: 199
	[UsedByNativeCode]
	[NativeHeader("Runtime/Graphics/ColorGamut.h")]
	public enum ColorGamut
	{
		// Token: 0x04000227 RID: 551
		sRGB,
		// Token: 0x04000228 RID: 552
		Rec709,
		// Token: 0x04000229 RID: 553
		Rec2020,
		// Token: 0x0400022A RID: 554
		DisplayP3,
		// Token: 0x0400022B RID: 555
		HDR10,
		// Token: 0x0400022C RID: 556
		DolbyHDR
	}
}
