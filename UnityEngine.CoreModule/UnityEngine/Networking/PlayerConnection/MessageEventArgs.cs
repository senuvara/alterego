﻿using System;

namespace UnityEngine.Networking.PlayerConnection
{
	// Token: 0x020002C9 RID: 713
	[Serializable]
	public class MessageEventArgs
	{
		// Token: 0x0600194B RID: 6475 RVA: 0x00002370 File Offset: 0x00000570
		public MessageEventArgs()
		{
		}

		// Token: 0x04000917 RID: 2327
		public int playerId;

		// Token: 0x04000918 RID: 2328
		public byte[] data;
	}
}
