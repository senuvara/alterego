﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Events;
using UnityEngine.Scripting;

namespace UnityEngine.Networking.PlayerConnection
{
	// Token: 0x020002CB RID: 715
	[Serializable]
	public class PlayerConnection : ScriptableObject, IEditorPlayerConnection
	{
		// Token: 0x06001952 RID: 6482 RVA: 0x0002C6D0 File Offset: 0x0002A8D0
		public PlayerConnection()
		{
		}

		// Token: 0x1700048C RID: 1164
		// (get) Token: 0x06001953 RID: 6483 RVA: 0x0002C6F0 File Offset: 0x0002A8F0
		public static PlayerConnection instance
		{
			get
			{
				PlayerConnection result;
				if (PlayerConnection.s_Instance == null)
				{
					result = PlayerConnection.CreateInstance();
				}
				else
				{
					result = PlayerConnection.s_Instance;
				}
				return result;
			}
		}

		// Token: 0x1700048D RID: 1165
		// (get) Token: 0x06001954 RID: 6484 RVA: 0x0002C728 File Offset: 0x0002A928
		public bool isConnected
		{
			get
			{
				return this.GetConnectionNativeApi().IsConnected();
			}
		}

		// Token: 0x06001955 RID: 6485 RVA: 0x0002C748 File Offset: 0x0002A948
		private static PlayerConnection CreateInstance()
		{
			PlayerConnection.s_Instance = ScriptableObject.CreateInstance<PlayerConnection>();
			PlayerConnection.s_Instance.hideFlags = HideFlags.HideAndDontSave;
			return PlayerConnection.s_Instance;
		}

		// Token: 0x06001956 RID: 6486 RVA: 0x0002C778 File Offset: 0x0002A978
		public void OnEnable()
		{
			if (!this.m_IsInitilized)
			{
				this.m_IsInitilized = true;
				this.GetConnectionNativeApi().Initialize();
			}
		}

		// Token: 0x06001957 RID: 6487 RVA: 0x0002C7A0 File Offset: 0x0002A9A0
		private IPlayerEditorConnectionNative GetConnectionNativeApi()
		{
			return PlayerConnection.connectionNative ?? new PlayerConnectionInternal();
		}

		// Token: 0x06001958 RID: 6488 RVA: 0x0002C7C8 File Offset: 0x0002A9C8
		public void Register(Guid messageId, UnityAction<MessageEventArgs> callback)
		{
			if (messageId == Guid.Empty)
			{
				throw new ArgumentException("Cant be Guid.Empty", "messageId");
			}
			if (!this.m_PlayerEditorConnectionEvents.messageTypeSubscribers.Any((PlayerEditorConnectionEvents.MessageTypeSubscribers x) => x.MessageTypeId == messageId))
			{
				this.GetConnectionNativeApi().RegisterInternal(messageId);
			}
			this.m_PlayerEditorConnectionEvents.AddAndCreate(messageId).AddListener(callback);
		}

		// Token: 0x06001959 RID: 6489 RVA: 0x0002C854 File Offset: 0x0002AA54
		public void Unregister(Guid messageId, UnityAction<MessageEventArgs> callback)
		{
			this.m_PlayerEditorConnectionEvents.UnregisterManagedCallback(messageId, callback);
			if (!this.m_PlayerEditorConnectionEvents.messageTypeSubscribers.Any((PlayerEditorConnectionEvents.MessageTypeSubscribers x) => x.MessageTypeId == messageId))
			{
				this.GetConnectionNativeApi().UnregisterInternal(messageId);
			}
		}

		// Token: 0x0600195A RID: 6490 RVA: 0x0002C8B8 File Offset: 0x0002AAB8
		public void RegisterConnection(UnityAction<int> callback)
		{
			foreach (int arg in this.m_connectedPlayers)
			{
				callback(arg);
			}
			this.m_PlayerEditorConnectionEvents.connectionEvent.AddListener(callback);
		}

		// Token: 0x0600195B RID: 6491 RVA: 0x0002C92C File Offset: 0x0002AB2C
		public void RegisterDisconnection(UnityAction<int> callback)
		{
			this.m_PlayerEditorConnectionEvents.disconnectionEvent.AddListener(callback);
		}

		// Token: 0x0600195C RID: 6492 RVA: 0x0002C940 File Offset: 0x0002AB40
		public void Send(Guid messageId, byte[] data)
		{
			if (messageId == Guid.Empty)
			{
				throw new ArgumentException("Cant be Guid.Empty", "messageId");
			}
			this.GetConnectionNativeApi().SendMessage(messageId, data, 0);
		}

		// Token: 0x0600195D RID: 6493 RVA: 0x0002C974 File Offset: 0x0002AB74
		public bool BlockUntilRecvMsg(Guid messageId, int timeout)
		{
			bool msgReceived = false;
			UnityAction<MessageEventArgs> callback = delegate(MessageEventArgs args)
			{
				msgReceived = true;
			};
			DateTime now = DateTime.Now;
			this.Register(messageId, callback);
			while ((DateTime.Now - now).TotalMilliseconds < (double)timeout && !msgReceived)
			{
				this.GetConnectionNativeApi().Poll();
			}
			this.Unregister(messageId, callback);
			return msgReceived;
		}

		// Token: 0x0600195E RID: 6494 RVA: 0x0002C9F6 File Offset: 0x0002ABF6
		public void DisconnectAll()
		{
			this.GetConnectionNativeApi().DisconnectAll();
		}

		// Token: 0x0600195F RID: 6495 RVA: 0x0002CA04 File Offset: 0x0002AC04
		[RequiredByNativeCode]
		private static void MessageCallbackInternal(IntPtr data, ulong size, ulong guid, string messageId)
		{
			byte[] array = null;
			if (size > 0UL)
			{
				array = new byte[size];
				Marshal.Copy(data, array, 0, (int)size);
			}
			PlayerConnection.instance.m_PlayerEditorConnectionEvents.InvokeMessageIdSubscribers(new Guid(messageId), array, (int)guid);
		}

		// Token: 0x06001960 RID: 6496 RVA: 0x0002CA48 File Offset: 0x0002AC48
		[RequiredByNativeCode]
		private static void ConnectedCallbackInternal(int playerId)
		{
			PlayerConnection.instance.m_connectedPlayers.Add(playerId);
			PlayerConnection.instance.m_PlayerEditorConnectionEvents.connectionEvent.Invoke(playerId);
		}

		// Token: 0x06001961 RID: 6497 RVA: 0x0002CA70 File Offset: 0x0002AC70
		[RequiredByNativeCode]
		private static void DisconnectedCallback(int playerId)
		{
			PlayerConnection.instance.m_connectedPlayers.Remove(playerId);
			PlayerConnection.instance.m_PlayerEditorConnectionEvents.disconnectionEvent.Invoke(playerId);
		}

		// Token: 0x04000919 RID: 2329
		internal static IPlayerEditorConnectionNative connectionNative;

		// Token: 0x0400091A RID: 2330
		[SerializeField]
		private PlayerEditorConnectionEvents m_PlayerEditorConnectionEvents = new PlayerEditorConnectionEvents();

		// Token: 0x0400091B RID: 2331
		[SerializeField]
		private List<int> m_connectedPlayers = new List<int>();

		// Token: 0x0400091C RID: 2332
		private bool m_IsInitilized;

		// Token: 0x0400091D RID: 2333
		private static PlayerConnection s_Instance;

		// Token: 0x020002CC RID: 716
		[CompilerGenerated]
		private sealed class <Register>c__AnonStorey0
		{
			// Token: 0x06001962 RID: 6498 RVA: 0x00002370 File Offset: 0x00000570
			public <Register>c__AnonStorey0()
			{
			}

			// Token: 0x06001963 RID: 6499 RVA: 0x0002CA9C File Offset: 0x0002AC9C
			internal bool <>m__0(PlayerEditorConnectionEvents.MessageTypeSubscribers x)
			{
				return x.MessageTypeId == this.messageId;
			}

			// Token: 0x0400091E RID: 2334
			internal Guid messageId;
		}

		// Token: 0x020002CD RID: 717
		[CompilerGenerated]
		private sealed class <Unregister>c__AnonStorey1
		{
			// Token: 0x06001964 RID: 6500 RVA: 0x00002370 File Offset: 0x00000570
			public <Unregister>c__AnonStorey1()
			{
			}

			// Token: 0x06001965 RID: 6501 RVA: 0x0002CAC4 File Offset: 0x0002ACC4
			internal bool <>m__0(PlayerEditorConnectionEvents.MessageTypeSubscribers x)
			{
				return x.MessageTypeId == this.messageId;
			}

			// Token: 0x0400091F RID: 2335
			internal Guid messageId;
		}

		// Token: 0x020002CE RID: 718
		[CompilerGenerated]
		private sealed class <BlockUntilRecvMsg>c__AnonStorey2
		{
			// Token: 0x06001966 RID: 6502 RVA: 0x00002370 File Offset: 0x00000570
			public <BlockUntilRecvMsg>c__AnonStorey2()
			{
			}

			// Token: 0x06001967 RID: 6503 RVA: 0x0002CAE9 File Offset: 0x0002ACE9
			internal void <>m__0(MessageEventArgs args)
			{
				this.msgReceived = true;
			}

			// Token: 0x04000920 RID: 2336
			internal bool msgReceived;
		}
	}
}
