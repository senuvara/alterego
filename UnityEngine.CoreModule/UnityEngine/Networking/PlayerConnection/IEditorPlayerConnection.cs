﻿using System;
using UnityEngine.Events;

namespace UnityEngine.Networking.PlayerConnection
{
	// Token: 0x020002CA RID: 714
	public interface IEditorPlayerConnection
	{
		// Token: 0x0600194C RID: 6476
		void Register(Guid messageId, UnityAction<MessageEventArgs> callback);

		// Token: 0x0600194D RID: 6477
		void Unregister(Guid messageId, UnityAction<MessageEventArgs> callback);

		// Token: 0x0600194E RID: 6478
		void DisconnectAll();

		// Token: 0x0600194F RID: 6479
		void RegisterConnection(UnityAction<int> callback);

		// Token: 0x06001950 RID: 6480
		void RegisterDisconnection(UnityAction<int> callback);

		// Token: 0x06001951 RID: 6481
		void Send(Guid messageId, byte[] data);
	}
}
