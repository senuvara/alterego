﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine.Events;

namespace UnityEngine.Networking.PlayerConnection
{
	// Token: 0x020002CF RID: 719
	[Serializable]
	internal class PlayerEditorConnectionEvents
	{
		// Token: 0x06001968 RID: 6504 RVA: 0x0002CAF2 File Offset: 0x0002ACF2
		public PlayerEditorConnectionEvents()
		{
		}

		// Token: 0x06001969 RID: 6505 RVA: 0x0002CB1C File Offset: 0x0002AD1C
		public void InvokeMessageIdSubscribers(Guid messageId, byte[] data, int playerId)
		{
			IEnumerable<PlayerEditorConnectionEvents.MessageTypeSubscribers> enumerable = from x in this.messageTypeSubscribers
			where x.MessageTypeId == messageId
			select x;
			if (!enumerable.Any<PlayerEditorConnectionEvents.MessageTypeSubscribers>())
			{
				Debug.LogError("No actions found for messageId: " + messageId);
			}
			else
			{
				MessageEventArgs arg = new MessageEventArgs
				{
					playerId = playerId,
					data = data
				};
				foreach (PlayerEditorConnectionEvents.MessageTypeSubscribers messageTypeSubscribers in enumerable)
				{
					messageTypeSubscribers.messageCallback.Invoke(arg);
				}
			}
		}

		// Token: 0x0600196A RID: 6506 RVA: 0x0002CBE8 File Offset: 0x0002ADE8
		public UnityEvent<MessageEventArgs> AddAndCreate(Guid messageId)
		{
			PlayerEditorConnectionEvents.MessageTypeSubscribers messageTypeSubscribers = this.messageTypeSubscribers.SingleOrDefault((PlayerEditorConnectionEvents.MessageTypeSubscribers x) => x.MessageTypeId == messageId);
			if (messageTypeSubscribers == null)
			{
				messageTypeSubscribers = new PlayerEditorConnectionEvents.MessageTypeSubscribers
				{
					MessageTypeId = messageId,
					messageCallback = new PlayerEditorConnectionEvents.MessageEvent()
				};
				this.messageTypeSubscribers.Add(messageTypeSubscribers);
			}
			messageTypeSubscribers.subscriberCount++;
			return messageTypeSubscribers.messageCallback;
		}

		// Token: 0x0600196B RID: 6507 RVA: 0x0002CC6C File Offset: 0x0002AE6C
		public void UnregisterManagedCallback(Guid messageId, UnityAction<MessageEventArgs> callback)
		{
			PlayerEditorConnectionEvents.MessageTypeSubscribers messageTypeSubscribers = this.messageTypeSubscribers.SingleOrDefault((PlayerEditorConnectionEvents.MessageTypeSubscribers x) => x.MessageTypeId == messageId);
			if (messageTypeSubscribers != null)
			{
				messageTypeSubscribers.subscriberCount--;
				messageTypeSubscribers.messageCallback.RemoveListener(callback);
				if (messageTypeSubscribers.subscriberCount <= 0)
				{
					this.messageTypeSubscribers.Remove(messageTypeSubscribers);
				}
			}
		}

		// Token: 0x04000921 RID: 2337
		[SerializeField]
		public List<PlayerEditorConnectionEvents.MessageTypeSubscribers> messageTypeSubscribers = new List<PlayerEditorConnectionEvents.MessageTypeSubscribers>();

		// Token: 0x04000922 RID: 2338
		[SerializeField]
		public PlayerEditorConnectionEvents.ConnectionChangeEvent connectionEvent = new PlayerEditorConnectionEvents.ConnectionChangeEvent();

		// Token: 0x04000923 RID: 2339
		[SerializeField]
		public PlayerEditorConnectionEvents.ConnectionChangeEvent disconnectionEvent = new PlayerEditorConnectionEvents.ConnectionChangeEvent();

		// Token: 0x020002D0 RID: 720
		[Serializable]
		public class MessageEvent : UnityEvent<MessageEventArgs>
		{
			// Token: 0x0600196C RID: 6508 RVA: 0x0002CCE0 File Offset: 0x0002AEE0
			public MessageEvent()
			{
			}
		}

		// Token: 0x020002D1 RID: 721
		[Serializable]
		public class ConnectionChangeEvent : UnityEvent<int>
		{
			// Token: 0x0600196D RID: 6509 RVA: 0x0002CCE8 File Offset: 0x0002AEE8
			public ConnectionChangeEvent()
			{
			}
		}

		// Token: 0x020002D2 RID: 722
		[Serializable]
		public class MessageTypeSubscribers
		{
			// Token: 0x0600196E RID: 6510 RVA: 0x0002CCF0 File Offset: 0x0002AEF0
			public MessageTypeSubscribers()
			{
			}

			// Token: 0x1700048E RID: 1166
			// (get) Token: 0x0600196F RID: 6511 RVA: 0x0002CD0C File Offset: 0x0002AF0C
			// (set) Token: 0x06001970 RID: 6512 RVA: 0x0002CD2C File Offset: 0x0002AF2C
			public Guid MessageTypeId
			{
				get
				{
					return new Guid(this.m_messageTypeId);
				}
				set
				{
					this.m_messageTypeId = value.ToString();
				}
			}

			// Token: 0x04000924 RID: 2340
			[SerializeField]
			private string m_messageTypeId;

			// Token: 0x04000925 RID: 2341
			public int subscriberCount = 0;

			// Token: 0x04000926 RID: 2342
			public PlayerEditorConnectionEvents.MessageEvent messageCallback = new PlayerEditorConnectionEvents.MessageEvent();
		}

		// Token: 0x020002D3 RID: 723
		[CompilerGenerated]
		private sealed class <InvokeMessageIdSubscribers>c__AnonStorey0
		{
			// Token: 0x06001971 RID: 6513 RVA: 0x00002370 File Offset: 0x00000570
			public <InvokeMessageIdSubscribers>c__AnonStorey0()
			{
			}

			// Token: 0x06001972 RID: 6514 RVA: 0x0002CD44 File Offset: 0x0002AF44
			internal bool <>m__0(PlayerEditorConnectionEvents.MessageTypeSubscribers x)
			{
				return x.MessageTypeId == this.messageId;
			}

			// Token: 0x04000927 RID: 2343
			internal Guid messageId;
		}

		// Token: 0x020002D4 RID: 724
		[CompilerGenerated]
		private sealed class <AddAndCreate>c__AnonStorey1
		{
			// Token: 0x06001973 RID: 6515 RVA: 0x00002370 File Offset: 0x00000570
			public <AddAndCreate>c__AnonStorey1()
			{
			}

			// Token: 0x06001974 RID: 6516 RVA: 0x0002CD6C File Offset: 0x0002AF6C
			internal bool <>m__0(PlayerEditorConnectionEvents.MessageTypeSubscribers x)
			{
				return x.MessageTypeId == this.messageId;
			}

			// Token: 0x04000928 RID: 2344
			internal Guid messageId;
		}

		// Token: 0x020002D5 RID: 725
		[CompilerGenerated]
		private sealed class <UnregisterManagedCallback>c__AnonStorey2
		{
			// Token: 0x06001975 RID: 6517 RVA: 0x00002370 File Offset: 0x00000570
			public <UnregisterManagedCallback>c__AnonStorey2()
			{
			}

			// Token: 0x06001976 RID: 6518 RVA: 0x0002CD94 File Offset: 0x0002AF94
			internal bool <>m__0(PlayerEditorConnectionEvents.MessageTypeSubscribers x)
			{
				return x.MessageTypeId == this.messageId;
			}

			// Token: 0x04000929 RID: 2345
			internal Guid messageId;
		}
	}
}
