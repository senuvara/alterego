﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000043 RID: 67
	[NativeHeader("Runtime/Geometry/AABB.h")]
	[NativeHeader("Runtime/Math/MathScripting.h")]
	[NativeClass("AABB")]
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	[ThreadAndSerializationSafe]
	[NativeHeader("Runtime/Geometry/Ray.h")]
	[NativeType(Header = "Runtime/Geometry/AABB.h")]
	[NativeHeader("Runtime/Geometry/Intersection.h")]
	public struct Bounds : IEquatable<Bounds>
	{
		// Token: 0x060002CC RID: 716 RVA: 0x00008F87 File Offset: 0x00007187
		public Bounds(Vector3 center, Vector3 size)
		{
			this.m_Center = center;
			this.m_Extents = size * 0.5f;
		}

		// Token: 0x060002CD RID: 717 RVA: 0x00008FA4 File Offset: 0x000071A4
		public override int GetHashCode()
		{
			return this.center.GetHashCode() ^ this.extents.GetHashCode() << 2;
		}

		// Token: 0x060002CE RID: 718 RVA: 0x00008FE4 File Offset: 0x000071E4
		public override bool Equals(object other)
		{
			return other is Bounds && this.Equals((Bounds)other);
		}

		// Token: 0x060002CF RID: 719 RVA: 0x00009018 File Offset: 0x00007218
		public bool Equals(Bounds other)
		{
			return this.center.Equals(other.center) && this.extents.Equals(other.extents);
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060002D0 RID: 720 RVA: 0x00009060 File Offset: 0x00007260
		// (set) Token: 0x060002D1 RID: 721 RVA: 0x0000907B File Offset: 0x0000727B
		public Vector3 center
		{
			get
			{
				return this.m_Center;
			}
			set
			{
				this.m_Center = value;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060002D2 RID: 722 RVA: 0x00009088 File Offset: 0x00007288
		// (set) Token: 0x060002D3 RID: 723 RVA: 0x000090AD File Offset: 0x000072AD
		public Vector3 size
		{
			get
			{
				return this.m_Extents * 2f;
			}
			set
			{
				this.m_Extents = value * 0.5f;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060002D4 RID: 724 RVA: 0x000090C4 File Offset: 0x000072C4
		// (set) Token: 0x060002D5 RID: 725 RVA: 0x000090DF File Offset: 0x000072DF
		public Vector3 extents
		{
			get
			{
				return this.m_Extents;
			}
			set
			{
				this.m_Extents = value;
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060002D6 RID: 726 RVA: 0x000090EC File Offset: 0x000072EC
		// (set) Token: 0x060002D7 RID: 727 RVA: 0x00009112 File Offset: 0x00007312
		public Vector3 min
		{
			get
			{
				return this.center - this.extents;
			}
			set
			{
				this.SetMinMax(value, this.max);
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060002D8 RID: 728 RVA: 0x00009124 File Offset: 0x00007324
		// (set) Token: 0x060002D9 RID: 729 RVA: 0x0000914A File Offset: 0x0000734A
		public Vector3 max
		{
			get
			{
				return this.center + this.extents;
			}
			set
			{
				this.SetMinMax(this.min, value);
			}
		}

		// Token: 0x060002DA RID: 730 RVA: 0x0000915C File Offset: 0x0000735C
		public static bool operator ==(Bounds lhs, Bounds rhs)
		{
			return lhs.center == rhs.center && lhs.extents == rhs.extents;
		}

		// Token: 0x060002DB RID: 731 RVA: 0x000091A0 File Offset: 0x000073A0
		public static bool operator !=(Bounds lhs, Bounds rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x060002DC RID: 732 RVA: 0x000091BF File Offset: 0x000073BF
		public void SetMinMax(Vector3 min, Vector3 max)
		{
			this.extents = (max - min) * 0.5f;
			this.center = min + this.extents;
		}

		// Token: 0x060002DD RID: 733 RVA: 0x000091EB File Offset: 0x000073EB
		public void Encapsulate(Vector3 point)
		{
			this.SetMinMax(Vector3.Min(this.min, point), Vector3.Max(this.max, point));
		}

		// Token: 0x060002DE RID: 734 RVA: 0x0000920C File Offset: 0x0000740C
		public void Encapsulate(Bounds bounds)
		{
			this.Encapsulate(bounds.center - bounds.extents);
			this.Encapsulate(bounds.center + bounds.extents);
		}

		// Token: 0x060002DF RID: 735 RVA: 0x00009241 File Offset: 0x00007441
		public void Expand(float amount)
		{
			amount *= 0.5f;
			this.extents += new Vector3(amount, amount, amount);
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x00009266 File Offset: 0x00007466
		public void Expand(Vector3 amount)
		{
			this.extents += amount * 0.5f;
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x00009288 File Offset: 0x00007488
		public bool Intersects(Bounds bounds)
		{
			return this.min.x <= bounds.max.x && this.max.x >= bounds.min.x && this.min.y <= bounds.max.y && this.max.y >= bounds.min.y && this.min.z <= bounds.max.z && this.max.z >= bounds.min.z;
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x00009378 File Offset: 0x00007578
		public bool IntersectRay(Ray ray)
		{
			float num;
			return Bounds.IntersectRayAABB(ray, this, out num);
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x0000939C File Offset: 0x0000759C
		public bool IntersectRay(Ray ray, out float distance)
		{
			return Bounds.IntersectRayAABB(ray, this, out distance);
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x000093C0 File Offset: 0x000075C0
		public override string ToString()
		{
			return UnityString.Format("Center: {0}, Extents: {1}", new object[]
			{
				this.m_Center,
				this.m_Extents
			});
		}

		// Token: 0x060002E5 RID: 741 RVA: 0x00009404 File Offset: 0x00007604
		public string ToString(string format)
		{
			return UnityString.Format("Center: {0}, Extents: {1}", new object[]
			{
				this.m_Center.ToString(format),
				this.m_Extents.ToString(format)
			});
		}

		// Token: 0x060002E6 RID: 742 RVA: 0x00009447 File Offset: 0x00007647
		[NativeMethod("IsInside", IsThreadSafe = true)]
		public bool Contains(Vector3 point)
		{
			return Bounds.Contains_Injected(ref this, ref point);
		}

		// Token: 0x060002E7 RID: 743 RVA: 0x00009451 File Offset: 0x00007651
		[FreeFunction("BoundsScripting::SqrDistance", HasExplicitThis = true, IsThreadSafe = true)]
		public float SqrDistance(Vector3 point)
		{
			return Bounds.SqrDistance_Injected(ref this, ref point);
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x0000945B File Offset: 0x0000765B
		[FreeFunction("IntersectRayAABB", IsThreadSafe = true)]
		private static bool IntersectRayAABB(Ray ray, Bounds bounds, out float dist)
		{
			return Bounds.IntersectRayAABB_Injected(ref ray, ref bounds, out dist);
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x00009468 File Offset: 0x00007668
		[FreeFunction("BoundsScripting::ClosestPoint", HasExplicitThis = true, IsThreadSafe = true)]
		public Vector3 ClosestPoint(Vector3 point)
		{
			Vector3 result;
			Bounds.ClosestPoint_Injected(ref this, ref point, out result);
			return result;
		}

		// Token: 0x060002EA RID: 746
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Contains_Injected(ref Bounds _unity_self, ref Vector3 point);

		// Token: 0x060002EB RID: 747
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float SqrDistance_Injected(ref Bounds _unity_self, ref Vector3 point);

		// Token: 0x060002EC RID: 748
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IntersectRayAABB_Injected(ref Ray ray, ref Bounds bounds, out float dist);

		// Token: 0x060002ED RID: 749
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ClosestPoint_Injected(ref Bounds _unity_self, ref Vector3 point, out Vector3 ret);

		// Token: 0x040000E9 RID: 233
		private Vector3 m_Center;

		// Token: 0x040000EA RID: 234
		[NativeName("m_Extent")]
		private Vector3 m_Extents;
	}
}
