﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200003F RID: 63
	[UsedByNativeCode]
	[NativeHeader("Runtime/Mono/MonoBehaviour.h")]
	public class Behaviour : Component
	{
		// Token: 0x0600029D RID: 669 RVA: 0x00008E34 File Offset: 0x00007034
		public Behaviour()
		{
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600029E RID: 670
		// (set) Token: 0x0600029F RID: 671
		[RequiredByNativeCode]
		[NativeProperty]
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060002A0 RID: 672
		[NativeProperty]
		public extern bool isActiveAndEnabled { [NativeMethod("IsAddedToManager")] [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
