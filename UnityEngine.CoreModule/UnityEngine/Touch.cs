﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000124 RID: 292
	[NativeHeader("Runtime/Input/InputBindings.h")]
	public struct Touch
	{
		// Token: 0x17000246 RID: 582
		// (get) Token: 0x06000C1D RID: 3101 RVA: 0x000137F0 File Offset: 0x000119F0
		// (set) Token: 0x06000C1E RID: 3102 RVA: 0x0001380B File Offset: 0x00011A0B
		public int fingerId
		{
			get
			{
				return this.m_FingerId;
			}
			set
			{
				this.m_FingerId = value;
			}
		}

		// Token: 0x17000247 RID: 583
		// (get) Token: 0x06000C1F RID: 3103 RVA: 0x00013818 File Offset: 0x00011A18
		// (set) Token: 0x06000C20 RID: 3104 RVA: 0x00013833 File Offset: 0x00011A33
		public Vector2 position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				this.m_Position = value;
			}
		}

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x06000C21 RID: 3105 RVA: 0x00013840 File Offset: 0x00011A40
		// (set) Token: 0x06000C22 RID: 3106 RVA: 0x0001385B File Offset: 0x00011A5B
		public Vector2 rawPosition
		{
			get
			{
				return this.m_RawPosition;
			}
			set
			{
				this.m_RawPosition = value;
			}
		}

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x06000C23 RID: 3107 RVA: 0x00013868 File Offset: 0x00011A68
		// (set) Token: 0x06000C24 RID: 3108 RVA: 0x00013883 File Offset: 0x00011A83
		public Vector2 deltaPosition
		{
			get
			{
				return this.m_PositionDelta;
			}
			set
			{
				this.m_PositionDelta = value;
			}
		}

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x06000C25 RID: 3109 RVA: 0x00013890 File Offset: 0x00011A90
		// (set) Token: 0x06000C26 RID: 3110 RVA: 0x000138AB File Offset: 0x00011AAB
		public float deltaTime
		{
			get
			{
				return this.m_TimeDelta;
			}
			set
			{
				this.m_TimeDelta = value;
			}
		}

		// Token: 0x1700024B RID: 587
		// (get) Token: 0x06000C27 RID: 3111 RVA: 0x000138B8 File Offset: 0x00011AB8
		// (set) Token: 0x06000C28 RID: 3112 RVA: 0x000138D3 File Offset: 0x00011AD3
		public int tapCount
		{
			get
			{
				return this.m_TapCount;
			}
			set
			{
				this.m_TapCount = value;
			}
		}

		// Token: 0x1700024C RID: 588
		// (get) Token: 0x06000C29 RID: 3113 RVA: 0x000138E0 File Offset: 0x00011AE0
		// (set) Token: 0x06000C2A RID: 3114 RVA: 0x000138FB File Offset: 0x00011AFB
		public TouchPhase phase
		{
			get
			{
				return this.m_Phase;
			}
			set
			{
				this.m_Phase = value;
			}
		}

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x06000C2B RID: 3115 RVA: 0x00013908 File Offset: 0x00011B08
		// (set) Token: 0x06000C2C RID: 3116 RVA: 0x00013923 File Offset: 0x00011B23
		public float pressure
		{
			get
			{
				return this.m_Pressure;
			}
			set
			{
				this.m_Pressure = value;
			}
		}

		// Token: 0x1700024E RID: 590
		// (get) Token: 0x06000C2D RID: 3117 RVA: 0x00013930 File Offset: 0x00011B30
		// (set) Token: 0x06000C2E RID: 3118 RVA: 0x0001394B File Offset: 0x00011B4B
		public float maximumPossiblePressure
		{
			get
			{
				return this.m_maximumPossiblePressure;
			}
			set
			{
				this.m_maximumPossiblePressure = value;
			}
		}

		// Token: 0x1700024F RID: 591
		// (get) Token: 0x06000C2F RID: 3119 RVA: 0x00013958 File Offset: 0x00011B58
		// (set) Token: 0x06000C30 RID: 3120 RVA: 0x00013973 File Offset: 0x00011B73
		public TouchType type
		{
			get
			{
				return this.m_Type;
			}
			set
			{
				this.m_Type = value;
			}
		}

		// Token: 0x17000250 RID: 592
		// (get) Token: 0x06000C31 RID: 3121 RVA: 0x00013980 File Offset: 0x00011B80
		// (set) Token: 0x06000C32 RID: 3122 RVA: 0x0001399B File Offset: 0x00011B9B
		public float altitudeAngle
		{
			get
			{
				return this.m_AltitudeAngle;
			}
			set
			{
				this.m_AltitudeAngle = value;
			}
		}

		// Token: 0x17000251 RID: 593
		// (get) Token: 0x06000C33 RID: 3123 RVA: 0x000139A8 File Offset: 0x00011BA8
		// (set) Token: 0x06000C34 RID: 3124 RVA: 0x000139C3 File Offset: 0x00011BC3
		public float azimuthAngle
		{
			get
			{
				return this.m_AzimuthAngle;
			}
			set
			{
				this.m_AzimuthAngle = value;
			}
		}

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x06000C35 RID: 3125 RVA: 0x000139D0 File Offset: 0x00011BD0
		// (set) Token: 0x06000C36 RID: 3126 RVA: 0x000139EB File Offset: 0x00011BEB
		public float radius
		{
			get
			{
				return this.m_Radius;
			}
			set
			{
				this.m_Radius = value;
			}
		}

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x06000C37 RID: 3127 RVA: 0x000139F8 File Offset: 0x00011BF8
		// (set) Token: 0x06000C38 RID: 3128 RVA: 0x00013A13 File Offset: 0x00011C13
		public float radiusVariance
		{
			get
			{
				return this.m_RadiusVariance;
			}
			set
			{
				this.m_RadiusVariance = value;
			}
		}

		// Token: 0x04000525 RID: 1317
		private int m_FingerId;

		// Token: 0x04000526 RID: 1318
		private Vector2 m_Position;

		// Token: 0x04000527 RID: 1319
		private Vector2 m_RawPosition;

		// Token: 0x04000528 RID: 1320
		private Vector2 m_PositionDelta;

		// Token: 0x04000529 RID: 1321
		private float m_TimeDelta;

		// Token: 0x0400052A RID: 1322
		private int m_TapCount;

		// Token: 0x0400052B RID: 1323
		private TouchPhase m_Phase;

		// Token: 0x0400052C RID: 1324
		private TouchType m_Type;

		// Token: 0x0400052D RID: 1325
		private float m_Pressure;

		// Token: 0x0400052E RID: 1326
		private float m_maximumPossiblePressure;

		// Token: 0x0400052F RID: 1327
		private float m_Radius;

		// Token: 0x04000530 RID: 1328
		private float m_RadiusVariance;

		// Token: 0x04000531 RID: 1329
		private float m_AltitudeAngle;

		// Token: 0x04000532 RID: 1330
		private float m_AzimuthAngle;
	}
}
