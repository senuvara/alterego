﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x020000A0 RID: 160
	[NativeHeader("Runtime/Graphics/QualitySettingsTypes.h")]
	[NativeHeader("Runtime/Graphics/GraphicsScriptBindings.h")]
	[NativeHeader("Runtime/Camera/RenderSettings.h")]
	[StaticAccessor("GetRenderSettings()", StaticAccessorType.Dot)]
	public sealed class RenderSettings : Object
	{
		// Token: 0x0600094E RID: 2382 RVA: 0x0000D647 File Offset: 0x0000B847
		private RenderSettings()
		{
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x0600094F RID: 2383 RVA: 0x00011088 File Offset: 0x0000F288
		// (set) Token: 0x06000950 RID: 2384 RVA: 0x000110A2 File Offset: 0x0000F2A2
		[Obsolete("Use RenderSettings.ambientIntensity instead (UnityUpgradable) -> ambientIntensity", false)]
		public static float ambientSkyboxAmount
		{
			get
			{
				return RenderSettings.ambientIntensity;
			}
			set
			{
				RenderSettings.ambientIntensity = value;
			}
		}

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x06000951 RID: 2385
		// (set) Token: 0x06000952 RID: 2386
		[NativeProperty("UseFog")]
		public static extern bool fog { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x06000953 RID: 2387
		// (set) Token: 0x06000954 RID: 2388
		[NativeProperty("LinearFogStart")]
		public static extern float fogStartDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06000955 RID: 2389
		// (set) Token: 0x06000956 RID: 2390
		[NativeProperty("LinearFogEnd")]
		public static extern float fogEndDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06000957 RID: 2391
		// (set) Token: 0x06000958 RID: 2392
		public static extern FogMode fogMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06000959 RID: 2393 RVA: 0x000110AC File Offset: 0x0000F2AC
		// (set) Token: 0x0600095A RID: 2394 RVA: 0x000110C1 File Offset: 0x0000F2C1
		public static Color fogColor
		{
			get
			{
				Color result;
				RenderSettings.get_fogColor_Injected(out result);
				return result;
			}
			set
			{
				RenderSettings.set_fogColor_Injected(ref value);
			}
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x0600095B RID: 2395
		// (set) Token: 0x0600095C RID: 2396
		public static extern float fogDensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x0600095D RID: 2397
		// (set) Token: 0x0600095E RID: 2398
		public static extern AmbientMode ambientMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001BA RID: 442
		// (get) Token: 0x0600095F RID: 2399 RVA: 0x000110CC File Offset: 0x0000F2CC
		// (set) Token: 0x06000960 RID: 2400 RVA: 0x000110E1 File Offset: 0x0000F2E1
		public static Color ambientSkyColor
		{
			get
			{
				Color result;
				RenderSettings.get_ambientSkyColor_Injected(out result);
				return result;
			}
			set
			{
				RenderSettings.set_ambientSkyColor_Injected(ref value);
			}
		}

		// Token: 0x170001BB RID: 443
		// (get) Token: 0x06000961 RID: 2401 RVA: 0x000110EC File Offset: 0x0000F2EC
		// (set) Token: 0x06000962 RID: 2402 RVA: 0x00011101 File Offset: 0x0000F301
		public static Color ambientEquatorColor
		{
			get
			{
				Color result;
				RenderSettings.get_ambientEquatorColor_Injected(out result);
				return result;
			}
			set
			{
				RenderSettings.set_ambientEquatorColor_Injected(ref value);
			}
		}

		// Token: 0x170001BC RID: 444
		// (get) Token: 0x06000963 RID: 2403 RVA: 0x0001110C File Offset: 0x0000F30C
		// (set) Token: 0x06000964 RID: 2404 RVA: 0x00011121 File Offset: 0x0000F321
		public static Color ambientGroundColor
		{
			get
			{
				Color result;
				RenderSettings.get_ambientGroundColor_Injected(out result);
				return result;
			}
			set
			{
				RenderSettings.set_ambientGroundColor_Injected(ref value);
			}
		}

		// Token: 0x170001BD RID: 445
		// (get) Token: 0x06000965 RID: 2405
		// (set) Token: 0x06000966 RID: 2406
		public static extern float ambientIntensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001BE RID: 446
		// (get) Token: 0x06000967 RID: 2407 RVA: 0x0001112C File Offset: 0x0000F32C
		// (set) Token: 0x06000968 RID: 2408 RVA: 0x00011141 File Offset: 0x0000F341
		[NativeProperty("AmbientSkyColor")]
		public static Color ambientLight
		{
			get
			{
				Color result;
				RenderSettings.get_ambientLight_Injected(out result);
				return result;
			}
			set
			{
				RenderSettings.set_ambientLight_Injected(ref value);
			}
		}

		// Token: 0x170001BF RID: 447
		// (get) Token: 0x06000969 RID: 2409 RVA: 0x0001114C File Offset: 0x0000F34C
		// (set) Token: 0x0600096A RID: 2410 RVA: 0x00011161 File Offset: 0x0000F361
		public static Color subtractiveShadowColor
		{
			get
			{
				Color result;
				RenderSettings.get_subtractiveShadowColor_Injected(out result);
				return result;
			}
			set
			{
				RenderSettings.set_subtractiveShadowColor_Injected(ref value);
			}
		}

		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x0600096B RID: 2411
		// (set) Token: 0x0600096C RID: 2412
		[NativeProperty("SkyboxMaterial")]
		public static extern Material skybox { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x0600096D RID: 2413
		// (set) Token: 0x0600096E RID: 2414
		public static extern Light sun { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x0600096F RID: 2415 RVA: 0x0001116C File Offset: 0x0000F36C
		// (set) Token: 0x06000970 RID: 2416 RVA: 0x00011181 File Offset: 0x0000F381
		public static SphericalHarmonicsL2 ambientProbe
		{
			get
			{
				SphericalHarmonicsL2 result;
				RenderSettings.get_ambientProbe_Injected(out result);
				return result;
			}
			set
			{
				RenderSettings.set_ambientProbe_Injected(ref value);
			}
		}

		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x06000971 RID: 2417
		// (set) Token: 0x06000972 RID: 2418
		public static extern Cubemap customReflection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06000973 RID: 2419
		// (set) Token: 0x06000974 RID: 2420
		public static extern float reflectionIntensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06000975 RID: 2421
		// (set) Token: 0x06000976 RID: 2422
		public static extern int reflectionBounces { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06000977 RID: 2423
		// (set) Token: 0x06000978 RID: 2424
		public static extern DefaultReflectionMode defaultReflectionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06000979 RID: 2425
		// (set) Token: 0x0600097A RID: 2426
		public static extern int defaultReflectionResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x0600097B RID: 2427
		// (set) Token: 0x0600097C RID: 2428
		public static extern float haloStrength { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x0600097D RID: 2429
		// (set) Token: 0x0600097E RID: 2430
		public static extern float flareStrength { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x0600097F RID: 2431
		// (set) Token: 0x06000980 RID: 2432
		public static extern float flareFadeSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000981 RID: 2433
		[FreeFunction("GetRenderSettings")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Object GetRenderSettings();

		// Token: 0x06000982 RID: 2434
		[StaticAccessor("RenderSettingsScripting", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Reset();

		// Token: 0x06000983 RID: 2435
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_fogColor_Injected(out Color ret);

		// Token: 0x06000984 RID: 2436
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_fogColor_Injected(ref Color value);

		// Token: 0x06000985 RID: 2437
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_ambientSkyColor_Injected(out Color ret);

		// Token: 0x06000986 RID: 2438
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_ambientSkyColor_Injected(ref Color value);

		// Token: 0x06000987 RID: 2439
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_ambientEquatorColor_Injected(out Color ret);

		// Token: 0x06000988 RID: 2440
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_ambientEquatorColor_Injected(ref Color value);

		// Token: 0x06000989 RID: 2441
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_ambientGroundColor_Injected(out Color ret);

		// Token: 0x0600098A RID: 2442
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_ambientGroundColor_Injected(ref Color value);

		// Token: 0x0600098B RID: 2443
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_ambientLight_Injected(out Color ret);

		// Token: 0x0600098C RID: 2444
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_ambientLight_Injected(ref Color value);

		// Token: 0x0600098D RID: 2445
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_subtractiveShadowColor_Injected(out Color ret);

		// Token: 0x0600098E RID: 2446
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_subtractiveShadowColor_Injected(ref Color value);

		// Token: 0x0600098F RID: 2447
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_ambientProbe_Injected(out SphericalHarmonicsL2 ret);

		// Token: 0x06000990 RID: 2448
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_ambientProbe_Injected(ref SphericalHarmonicsL2 value);
	}
}
