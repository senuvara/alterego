﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000363 RID: 867
	[NativeHeader("Runtime/2D/Common/SpriteDataAccess.h")]
	[NativeHeader("Runtime/2D/Common/ScriptBindings/SpritesMarshalling.h")]
	[NativeType("Runtime/Graphics/SpriteFrame.h")]
	[NativeHeader("Runtime/Graphics/SpriteUtility.h")]
	[ExcludeFromPreset]
	public sealed class Sprite : Object
	{
		// Token: 0x06001D71 RID: 7537 RVA: 0x0000D647 File Offset: 0x0000B847
		[RequiredByNativeCode]
		private Sprite()
		{
		}

		// Token: 0x06001D72 RID: 7538
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetPackingMode();

		// Token: 0x06001D73 RID: 7539
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetPackingRotation();

		// Token: 0x06001D74 RID: 7540
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetPacked();

		// Token: 0x06001D75 RID: 7541 RVA: 0x0003305C File Offset: 0x0003125C
		internal Rect GetTextureRect()
		{
			Rect result;
			this.GetTextureRect_Injected(out result);
			return result;
		}

		// Token: 0x06001D76 RID: 7542 RVA: 0x00033074 File Offset: 0x00031274
		internal Vector2 GetTextureRectOffset()
		{
			Vector2 result;
			this.GetTextureRectOffset_Injected(out result);
			return result;
		}

		// Token: 0x06001D77 RID: 7543 RVA: 0x0003308C File Offset: 0x0003128C
		internal Vector4 GetInnerUVs()
		{
			Vector4 result;
			this.GetInnerUVs_Injected(out result);
			return result;
		}

		// Token: 0x06001D78 RID: 7544 RVA: 0x000330A4 File Offset: 0x000312A4
		internal Vector4 GetOuterUVs()
		{
			Vector4 result;
			this.GetOuterUVs_Injected(out result);
			return result;
		}

		// Token: 0x06001D79 RID: 7545 RVA: 0x000330BC File Offset: 0x000312BC
		internal Vector4 GetPadding()
		{
			Vector4 result;
			this.GetPadding_Injected(out result);
			return result;
		}

		// Token: 0x06001D7A RID: 7546 RVA: 0x000330D2 File Offset: 0x000312D2
		[FreeFunction("SpritesBindings::CreateSpriteWithoutTextureScripting")]
		internal static Sprite CreateSpriteWithoutTextureScripting(Rect rect, Vector2 pivot, float pixelsToUnits, Texture2D texture)
		{
			return Sprite.CreateSpriteWithoutTextureScripting_Injected(ref rect, ref pivot, pixelsToUnits, texture);
		}

		// Token: 0x06001D7B RID: 7547 RVA: 0x000330DF File Offset: 0x000312DF
		[FreeFunction("SpritesBindings::CreateSprite")]
		internal static Sprite CreateSprite(Texture2D texture, Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, Vector4 border, bool generateFallbackPhysicsShape)
		{
			return Sprite.CreateSprite_Injected(texture, ref rect, ref pivot, pixelsPerUnit, extrude, meshType, ref border, generateFallbackPhysicsShape);
		}

		// Token: 0x17000567 RID: 1383
		// (get) Token: 0x06001D7C RID: 7548 RVA: 0x000330F4 File Offset: 0x000312F4
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.get_bounds_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000568 RID: 1384
		// (get) Token: 0x06001D7D RID: 7549 RVA: 0x0003310C File Offset: 0x0003130C
		public Rect rect
		{
			get
			{
				Rect result;
				this.get_rect_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000569 RID: 1385
		// (get) Token: 0x06001D7E RID: 7550 RVA: 0x00033124 File Offset: 0x00031324
		public Vector4 border
		{
			get
			{
				Vector4 result;
				this.get_border_Injected(out result);
				return result;
			}
		}

		// Token: 0x1700056A RID: 1386
		// (get) Token: 0x06001D7F RID: 7551
		public extern Texture2D texture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700056B RID: 1387
		// (get) Token: 0x06001D80 RID: 7552
		public extern float pixelsPerUnit { [NativeMethod("GetPixelsToUnits")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700056C RID: 1388
		// (get) Token: 0x06001D81 RID: 7553
		public extern Texture2D associatedAlphaSplitTexture { [NativeMethod("GetAlphaTexture")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700056D RID: 1389
		// (get) Token: 0x06001D82 RID: 7554 RVA: 0x0003313C File Offset: 0x0003133C
		public Vector2 pivot
		{
			[NativeMethod("GetPivotInPixels")]
			get
			{
				Vector2 result;
				this.get_pivot_Injected(out result);
				return result;
			}
		}

		// Token: 0x1700056E RID: 1390
		// (get) Token: 0x06001D83 RID: 7555 RVA: 0x00033154 File Offset: 0x00031354
		public bool packed
		{
			get
			{
				return this.GetPacked() == 1;
			}
		}

		// Token: 0x1700056F RID: 1391
		// (get) Token: 0x06001D84 RID: 7556 RVA: 0x00033174 File Offset: 0x00031374
		public SpritePackingMode packingMode
		{
			get
			{
				return (SpritePackingMode)this.GetPackingMode();
			}
		}

		// Token: 0x17000570 RID: 1392
		// (get) Token: 0x06001D85 RID: 7557 RVA: 0x00033190 File Offset: 0x00031390
		public SpritePackingRotation packingRotation
		{
			get
			{
				return (SpritePackingRotation)this.GetPackingRotation();
			}
		}

		// Token: 0x17000571 RID: 1393
		// (get) Token: 0x06001D86 RID: 7558 RVA: 0x000331AC File Offset: 0x000313AC
		public Rect textureRect
		{
			get
			{
				Rect result;
				if (this.packed && this.packingMode != SpritePackingMode.Rectangle)
				{
					result = Rect.zero;
				}
				else
				{
					result = this.GetTextureRect();
				}
				return result;
			}
		}

		// Token: 0x17000572 RID: 1394
		// (get) Token: 0x06001D87 RID: 7559 RVA: 0x000331EC File Offset: 0x000313EC
		public Vector2 textureRectOffset
		{
			get
			{
				Vector2 result;
				if (this.packed && this.packingMode != SpritePackingMode.Rectangle)
				{
					result = Vector2.zero;
				}
				else
				{
					result = this.GetTextureRectOffset();
				}
				return result;
			}
		}

		// Token: 0x17000573 RID: 1395
		// (get) Token: 0x06001D88 RID: 7560
		public extern Vector2[] vertices { [FreeFunction("SpriteAccessLegacy::GetSpriteVertices", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000574 RID: 1396
		// (get) Token: 0x06001D89 RID: 7561
		public extern ushort[] triangles { [FreeFunction("SpriteAccessLegacy::GetSpriteIndices", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000575 RID: 1397
		// (get) Token: 0x06001D8A RID: 7562
		public extern Vector2[] uv { [FreeFunction("SpriteAccessLegacy::GetSpriteUVs", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001D8B RID: 7563
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetPhysicsShapeCount();

		// Token: 0x06001D8C RID: 7564 RVA: 0x0003322C File Offset: 0x0003142C
		public int GetPhysicsShapePointCount(int shapeIdx)
		{
			int physicsShapeCount = this.GetPhysicsShapeCount();
			if (shapeIdx < 0 || shapeIdx >= physicsShapeCount)
			{
				throw new IndexOutOfRangeException(string.Format("Index({0}) is out of bounds(0 - {1})", shapeIdx, physicsShapeCount - 1));
			}
			return this.Internal_GetPhysicsShapePointCount(shapeIdx);
		}

		// Token: 0x06001D8D RID: 7565
		[NativeMethod("GetPhysicsShapePointCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int Internal_GetPhysicsShapePointCount(int shapeIdx);

		// Token: 0x06001D8E RID: 7566 RVA: 0x0003327C File Offset: 0x0003147C
		public int GetPhysicsShape(int shapeIdx, List<Vector2> physicsShape)
		{
			int physicsShapeCount = this.GetPhysicsShapeCount();
			if (shapeIdx < 0 || shapeIdx >= physicsShapeCount)
			{
				throw new IndexOutOfRangeException(string.Format("Index({0}) is out of bounds(0 - {1})", shapeIdx, physicsShapeCount - 1));
			}
			Sprite.GetPhysicsShapeImpl(this, shapeIdx, physicsShape);
			return physicsShape.Count;
		}

		// Token: 0x06001D8F RID: 7567
		[FreeFunction("SpritesBindings::GetPhysicsShape", ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetPhysicsShapeImpl(Sprite sprite, int shapeIdx, List<Vector2> physicsShape);

		// Token: 0x06001D90 RID: 7568 RVA: 0x000332D4 File Offset: 0x000314D4
		public void OverridePhysicsShape(IList<Vector2[]> physicsShapes)
		{
			for (int i = 0; i < physicsShapes.Count; i++)
			{
				Vector2[] array = physicsShapes[i];
				if (array == null)
				{
					throw new ArgumentNullException(string.Format("Physics Shape at {0} is null.", i));
				}
				if (array.Length < 3)
				{
					throw new ArgumentException(string.Format("Physics Shape at {0} has less than 3 vertices ({1}).", i, array.Length));
				}
			}
			Sprite.OverridePhysicsShapeCount(this, physicsShapes.Count);
			for (int j = 0; j < physicsShapes.Count; j++)
			{
				Sprite.OverridePhysicsShape(this, physicsShapes[j], j);
			}
		}

		// Token: 0x06001D91 RID: 7569
		[FreeFunction("SpritesBindings::OverridePhysicsShapeCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void OverridePhysicsShapeCount(Sprite sprite, int physicsShapeCount);

		// Token: 0x06001D92 RID: 7570
		[FreeFunction("SpritesBindings::OverridePhysicsShape", ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void OverridePhysicsShape(Sprite sprite, Vector2[] physicsShape, int idx);

		// Token: 0x06001D93 RID: 7571
		[FreeFunction("SpritesBindings::OverrideGeometry", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void OverrideGeometry(Vector2[] vertices, ushort[] triangles);

		// Token: 0x06001D94 RID: 7572 RVA: 0x0003337C File Offset: 0x0003157C
		internal static Sprite Create(Rect rect, Vector2 pivot, float pixelsToUnits, Texture2D texture)
		{
			return Sprite.CreateSpriteWithoutTextureScripting(rect, pivot, pixelsToUnits, texture);
		}

		// Token: 0x06001D95 RID: 7573 RVA: 0x0003339C File Offset: 0x0003159C
		internal static Sprite Create(Rect rect, Vector2 pivot, float pixelsToUnits)
		{
			return Sprite.CreateSpriteWithoutTextureScripting(rect, pivot, pixelsToUnits, null);
		}

		// Token: 0x06001D96 RID: 7574 RVA: 0x000333BC File Offset: 0x000315BC
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, Vector4 border, bool generateFallbackPhysicsShape)
		{
			Sprite result;
			if (texture == null)
			{
				result = null;
			}
			else
			{
				if (rect.xMax > (float)texture.width || rect.yMax > (float)texture.height)
				{
					throw new ArgumentException(string.Format("Could not create sprite ({0}, {1}, {2}, {3}) from a {4}x{5} texture.", new object[]
					{
						rect.x,
						rect.y,
						rect.width,
						rect.height,
						texture.width,
						texture.height
					}));
				}
				if (pixelsPerUnit <= 0f)
				{
					throw new ArgumentException("pixelsPerUnit must be set to a positive non-zero value.");
				}
				result = Sprite.CreateSprite(texture, rect, pivot, pixelsPerUnit, extrude, meshType, border, generateFallbackPhysicsShape);
			}
			return result;
		}

		// Token: 0x06001D97 RID: 7575 RVA: 0x000334A0 File Offset: 0x000316A0
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, Vector4 border)
		{
			return Sprite.Create(texture, rect, pivot, pixelsPerUnit, extrude, meshType, border, false);
		}

		// Token: 0x06001D98 RID: 7576 RVA: 0x000334C8 File Offset: 0x000316C8
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType)
		{
			return Sprite.Create(texture, rect, pivot, pixelsPerUnit, extrude, meshType, Vector4.zero);
		}

		// Token: 0x06001D99 RID: 7577 RVA: 0x000334F0 File Offset: 0x000316F0
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude)
		{
			return Sprite.Create(texture, rect, pivot, pixelsPerUnit, extrude, SpriteMeshType.Tight);
		}

		// Token: 0x06001D9A RID: 7578 RVA: 0x00033514 File Offset: 0x00031714
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot, float pixelsPerUnit)
		{
			return Sprite.Create(texture, rect, pivot, pixelsPerUnit, 0U);
		}

		// Token: 0x06001D9B RID: 7579 RVA: 0x00033534 File Offset: 0x00031734
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot)
		{
			return Sprite.Create(texture, rect, pivot, 100f);
		}

		// Token: 0x06001D9C RID: 7580
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetTextureRect_Injected(out Rect ret);

		// Token: 0x06001D9D RID: 7581
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetTextureRectOffset_Injected(out Vector2 ret);

		// Token: 0x06001D9E RID: 7582
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetInnerUVs_Injected(out Vector4 ret);

		// Token: 0x06001D9F RID: 7583
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetOuterUVs_Injected(out Vector4 ret);

		// Token: 0x06001DA0 RID: 7584
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetPadding_Injected(out Vector4 ret);

		// Token: 0x06001DA1 RID: 7585
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Sprite CreateSpriteWithoutTextureScripting_Injected(ref Rect rect, ref Vector2 pivot, float pixelsToUnits, Texture2D texture);

		// Token: 0x06001DA2 RID: 7586
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Sprite CreateSprite_Injected(Texture2D texture, ref Rect rect, ref Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, ref Vector4 border, bool generateFallbackPhysicsShape);

		// Token: 0x06001DA3 RID: 7587
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bounds_Injected(out Bounds ret);

		// Token: 0x06001DA4 RID: 7588
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rect_Injected(out Rect ret);

		// Token: 0x06001DA5 RID: 7589
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_border_Injected(out Vector4 ret);

		// Token: 0x06001DA6 RID: 7590
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_pivot_Injected(out Vector2 ret);
	}
}
