﻿using System;

namespace UnityEngine
{
	// Token: 0x020001F1 RID: 497
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class DelayedAttribute : PropertyAttribute
	{
		// Token: 0x06000F8D RID: 3981 RVA: 0x0001AEE6 File Offset: 0x000190E6
		public DelayedAttribute()
		{
		}
	}
}
