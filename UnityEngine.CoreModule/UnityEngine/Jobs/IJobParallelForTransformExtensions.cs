﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Jobs.LowLevel.Unsafe;

namespace UnityEngine.Jobs
{
	// Token: 0x02000333 RID: 819
	public static class IJobParallelForTransformExtensions
	{
		// Token: 0x06001B9D RID: 7069 RVA: 0x00030CE0 File Offset: 0x0002EEE0
		public static JobHandle Schedule<T>(this T jobData, TransformAccessArray transforms, JobHandle dependsOn = default(JobHandle)) where T : struct, IJobParallelForTransform
		{
			JobsUtility.JobScheduleParameters jobScheduleParameters = new JobsUtility.JobScheduleParameters(UnsafeUtility.AddressOf<T>(ref jobData), IJobParallelForTransformExtensions.TransformParallelForLoopStruct<T>.Initialize(), dependsOn, ScheduleMode.Batched);
			return JobsUtility.ScheduleParallelForTransform(ref jobScheduleParameters, transforms.GetTransformAccessArrayForSchedule());
		}

		// Token: 0x02000334 RID: 820
		internal struct TransformParallelForLoopStruct<T> where T : struct, IJobParallelForTransform
		{
			// Token: 0x06001B9E RID: 7070 RVA: 0x00030D18 File Offset: 0x0002EF18
			public static IntPtr Initialize()
			{
				if (IJobParallelForTransformExtensions.TransformParallelForLoopStruct<T>.jobReflectionData == IntPtr.Zero)
				{
					Type typeFromHandle = typeof(T);
					JobType jobType = JobType.ParallelFor;
					if (IJobParallelForTransformExtensions.TransformParallelForLoopStruct<T>.<>f__mg$cache0 == null)
					{
						IJobParallelForTransformExtensions.TransformParallelForLoopStruct<T>.<>f__mg$cache0 = new IJobParallelForTransformExtensions.TransformParallelForLoopStruct<T>.ExecuteJobFunction(IJobParallelForTransformExtensions.TransformParallelForLoopStruct<T>.Execute);
					}
					IJobParallelForTransformExtensions.TransformParallelForLoopStruct<T>.jobReflectionData = JobsUtility.CreateJobReflectionData(typeFromHandle, jobType, IJobParallelForTransformExtensions.TransformParallelForLoopStruct<T>.<>f__mg$cache0, null, null);
				}
				return IJobParallelForTransformExtensions.TransformParallelForLoopStruct<T>.jobReflectionData;
			}

			// Token: 0x06001B9F RID: 7071 RVA: 0x00030D7C File Offset: 0x0002EF7C
			public unsafe static void Execute(ref T jobData, IntPtr jobData2, IntPtr bufferRangePatchData, ref JobRanges ranges, int jobIndex)
			{
				IntPtr transformArrayIntPtr;
				UnsafeUtility.CopyPtrToStructure<IntPtr>((void*)jobData2, out transformArrayIntPtr);
				int* ptr = (int*)((void*)TransformAccessArray.GetSortedToUserIndex(transformArrayIntPtr));
				TransformAccess* ptr2 = (TransformAccess*)((void*)TransformAccessArray.GetSortedTransformAccess(transformArrayIntPtr));
				int num;
				int num2;
				JobsUtility.GetJobRange(ref ranges, jobIndex, out num, out num2);
				for (int i = num; i < num2; i++)
				{
					int num3 = i;
					int index = ptr[num3];
					jobData.Execute(index, ptr2[num3]);
				}
			}

			// Token: 0x04000A68 RID: 2664
			public static IntPtr jobReflectionData;

			// Token: 0x04000A69 RID: 2665
			[CompilerGenerated]
			private static IJobParallelForTransformExtensions.TransformParallelForLoopStruct<T>.ExecuteJobFunction <>f__mg$cache0;

			// Token: 0x02000335 RID: 821
			// (Invoke) Token: 0x06001BA1 RID: 7073
			public delegate void ExecuteJobFunction(ref T jobData, IntPtr additionalPtr, IntPtr bufferRangePatchData, ref JobRanges ranges, int jobIndex);
		}
	}
}
