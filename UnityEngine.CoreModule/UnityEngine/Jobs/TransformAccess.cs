﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Jobs
{
	// Token: 0x02000352 RID: 850
	[NativeHeader("Runtime/Transform/ScriptBindings/TransformAccess.bindings.h")]
	public struct TransformAccess
	{
		// Token: 0x17000549 RID: 1353
		// (get) Token: 0x06001CE9 RID: 7401 RVA: 0x0003287C File Offset: 0x00030A7C
		// (set) Token: 0x06001CEA RID: 7402 RVA: 0x0003289A File Offset: 0x00030A9A
		public Vector3 position
		{
			get
			{
				Vector3 result;
				TransformAccess.GetPosition(ref this, out result);
				return result;
			}
			set
			{
				TransformAccess.SetPosition(ref this, ref value);
			}
		}

		// Token: 0x1700054A RID: 1354
		// (get) Token: 0x06001CEB RID: 7403 RVA: 0x000328A8 File Offset: 0x00030AA8
		// (set) Token: 0x06001CEC RID: 7404 RVA: 0x000328C6 File Offset: 0x00030AC6
		public Quaternion rotation
		{
			get
			{
				Quaternion result;
				TransformAccess.GetRotation(ref this, out result);
				return result;
			}
			set
			{
				TransformAccess.SetRotation(ref this, ref value);
			}
		}

		// Token: 0x1700054B RID: 1355
		// (get) Token: 0x06001CED RID: 7405 RVA: 0x000328D4 File Offset: 0x00030AD4
		// (set) Token: 0x06001CEE RID: 7406 RVA: 0x000328F2 File Offset: 0x00030AF2
		public Vector3 localPosition
		{
			get
			{
				Vector3 result;
				TransformAccess.GetLocalPosition(ref this, out result);
				return result;
			}
			set
			{
				TransformAccess.SetLocalPosition(ref this, ref value);
			}
		}

		// Token: 0x1700054C RID: 1356
		// (get) Token: 0x06001CEF RID: 7407 RVA: 0x00032900 File Offset: 0x00030B00
		// (set) Token: 0x06001CF0 RID: 7408 RVA: 0x0003291E File Offset: 0x00030B1E
		public Quaternion localRotation
		{
			get
			{
				Quaternion result;
				TransformAccess.GetLocalRotation(ref this, out result);
				return result;
			}
			set
			{
				TransformAccess.SetLocalRotation(ref this, ref value);
			}
		}

		// Token: 0x1700054D RID: 1357
		// (get) Token: 0x06001CF1 RID: 7409 RVA: 0x0003292C File Offset: 0x00030B2C
		// (set) Token: 0x06001CF2 RID: 7410 RVA: 0x0003294A File Offset: 0x00030B4A
		public Vector3 localScale
		{
			get
			{
				Vector3 result;
				TransformAccess.GetLocalScale(ref this, out result);
				return result;
			}
			set
			{
				TransformAccess.SetLocalScale(ref this, ref value);
			}
		}

		// Token: 0x06001CF3 RID: 7411
		[NativeMethod(Name = "TransformAccessBindings::GetPosition", IsThreadSafe = true, IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetPosition(ref TransformAccess access, out Vector3 p);

		// Token: 0x06001CF4 RID: 7412
		[NativeMethod(Name = "TransformAccessBindings::SetPosition", IsThreadSafe = true, IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetPosition(ref TransformAccess access, ref Vector3 p);

		// Token: 0x06001CF5 RID: 7413
		[NativeMethod(Name = "TransformAccessBindings::GetRotation", IsThreadSafe = true, IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRotation(ref TransformAccess access, out Quaternion r);

		// Token: 0x06001CF6 RID: 7414
		[NativeMethod(Name = "TransformAccessBindings::SetRotation", IsThreadSafe = true, IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetRotation(ref TransformAccess access, ref Quaternion r);

		// Token: 0x06001CF7 RID: 7415
		[NativeMethod(Name = "TransformAccessBindings::GetLocalPosition", IsThreadSafe = true, IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLocalPosition(ref TransformAccess access, out Vector3 p);

		// Token: 0x06001CF8 RID: 7416
		[NativeMethod(Name = "TransformAccessBindings::SetLocalPosition", IsThreadSafe = true, IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLocalPosition(ref TransformAccess access, ref Vector3 p);

		// Token: 0x06001CF9 RID: 7417
		[NativeMethod(Name = "TransformAccessBindings::GetLocalRotation", IsThreadSafe = true, IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLocalRotation(ref TransformAccess access, out Quaternion r);

		// Token: 0x06001CFA RID: 7418
		[NativeMethod(Name = "TransformAccessBindings::SetLocalRotation", IsThreadSafe = true, IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLocalRotation(ref TransformAccess access, ref Quaternion r);

		// Token: 0x06001CFB RID: 7419
		[NativeMethod(Name = "TransformAccessBindings::GetLocalScale", IsThreadSafe = true, IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLocalScale(ref TransformAccess access, out Vector3 r);

		// Token: 0x06001CFC RID: 7420
		[NativeMethod(Name = "TransformAccessBindings::SetLocalScale", IsThreadSafe = true, IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLocalScale(ref TransformAccess access, ref Vector3 r);

		// Token: 0x04000AD4 RID: 2772
		private IntPtr hierarchy;

		// Token: 0x04000AD5 RID: 2773
		private int index;
	}
}
