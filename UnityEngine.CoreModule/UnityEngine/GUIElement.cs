﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000076 RID: 118
	[RequireComponent(typeof(Transform))]
	public class GUIElement : Behaviour
	{
		// Token: 0x060005BB RID: 1467 RVA: 0x0000A52A File Offset: 0x0000872A
		public GUIElement()
		{
		}

		// Token: 0x060005BC RID: 1468 RVA: 0x0000D8F0 File Offset: 0x0000BAF0
		[ExcludeFromDocs]
		public bool HitTest(Vector3 screenPosition)
		{
			return this.HitTest(new Vector2(screenPosition.x, screenPosition.y), null);
		}

		// Token: 0x060005BD RID: 1469 RVA: 0x0000D924 File Offset: 0x0000BB24
		public bool HitTest(Vector3 screenPosition, [DefaultValue("null")] Camera camera)
		{
			return this.HitTest(new Vector2(screenPosition.x, screenPosition.y), GUIElement.GetCameraOrWindowRect(camera));
		}

		// Token: 0x060005BE RID: 1470 RVA: 0x0000D958 File Offset: 0x0000BB58
		public Rect GetScreenRect([DefaultValue("null")] Camera camera)
		{
			return this.GetScreenRect(GUIElement.GetCameraOrWindowRect(camera));
		}

		// Token: 0x060005BF RID: 1471 RVA: 0x0000D97C File Offset: 0x0000BB7C
		[ExcludeFromDocs]
		public Rect GetScreenRect()
		{
			return this.GetScreenRect(null);
		}

		// Token: 0x060005C0 RID: 1472 RVA: 0x0000D998 File Offset: 0x0000BB98
		private Rect GetScreenRect(Rect rect)
		{
			Rect result;
			this.GetScreenRect_Injected(ref rect, out result);
			return result;
		}

		// Token: 0x060005C1 RID: 1473 RVA: 0x0000D9B0 File Offset: 0x0000BBB0
		private bool HitTest(Vector2 screenPosition, Rect cameraRect)
		{
			return this.HitTest_Injected(ref screenPosition, ref cameraRect);
		}

		// Token: 0x060005C2 RID: 1474 RVA: 0x0000D9BC File Offset: 0x0000BBBC
		private static Rect GetCameraOrWindowRect(Camera camera)
		{
			Rect result;
			if (camera != null)
			{
				result = camera.pixelRect;
			}
			else
			{
				result = new Rect(0f, 0f, (float)Screen.width, (float)Screen.height);
			}
			return result;
		}

		// Token: 0x060005C3 RID: 1475
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetScreenRect_Injected(ref Rect rect, out Rect ret);

		// Token: 0x060005C4 RID: 1476
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool HitTest_Injected(ref Vector2 screenPosition, ref Rect cameraRect);
	}
}
