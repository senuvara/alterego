﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000A RID: 10
	public sealed class AndroidJavaException : Exception
	{
		// Token: 0x06000142 RID: 322 RVA: 0x00005474 File Offset: 0x00003674
		internal AndroidJavaException(string message, string javaStackTrace) : base(message)
		{
			this.mJavaStackTrace = javaStackTrace;
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000143 RID: 323 RVA: 0x00005488 File Offset: 0x00003688
		public override string StackTrace
		{
			get
			{
				return this.mJavaStackTrace + base.StackTrace;
			}
		}

		// Token: 0x0400000E RID: 14
		private string mJavaStackTrace;
	}
}
