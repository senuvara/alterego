﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200015D RID: 349
	[UsedByNativeCode]
	public struct Plane
	{
		// Token: 0x06000F1E RID: 3870 RVA: 0x0001A22F File Offset: 0x0001842F
		public Plane(Vector3 inNormal, Vector3 inPoint)
		{
			this.m_Normal = Vector3.Normalize(inNormal);
			this.m_Distance = -Vector3.Dot(this.m_Normal, inPoint);
		}

		// Token: 0x06000F1F RID: 3871 RVA: 0x0001A251 File Offset: 0x00018451
		public Plane(Vector3 inNormal, float d)
		{
			this.m_Normal = Vector3.Normalize(inNormal);
			this.m_Distance = d;
		}

		// Token: 0x06000F20 RID: 3872 RVA: 0x0001A267 File Offset: 0x00018467
		public Plane(Vector3 a, Vector3 b, Vector3 c)
		{
			this.m_Normal = Vector3.Normalize(Vector3.Cross(b - a, c - a));
			this.m_Distance = -Vector3.Dot(this.m_Normal, a);
		}

		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x06000F21 RID: 3873 RVA: 0x0001A29C File Offset: 0x0001849C
		// (set) Token: 0x06000F22 RID: 3874 RVA: 0x0001A2B7 File Offset: 0x000184B7
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
			set
			{
				this.m_Normal = value;
			}
		}

		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x06000F23 RID: 3875 RVA: 0x0001A2C4 File Offset: 0x000184C4
		// (set) Token: 0x06000F24 RID: 3876 RVA: 0x0001A2DF File Offset: 0x000184DF
		public float distance
		{
			get
			{
				return this.m_Distance;
			}
			set
			{
				this.m_Distance = value;
			}
		}

		// Token: 0x06000F25 RID: 3877 RVA: 0x0001A2E9 File Offset: 0x000184E9
		public void SetNormalAndPosition(Vector3 inNormal, Vector3 inPoint)
		{
			this.m_Normal = Vector3.Normalize(inNormal);
			this.m_Distance = -Vector3.Dot(inNormal, inPoint);
		}

		// Token: 0x06000F26 RID: 3878 RVA: 0x0001A267 File Offset: 0x00018467
		public void Set3Points(Vector3 a, Vector3 b, Vector3 c)
		{
			this.m_Normal = Vector3.Normalize(Vector3.Cross(b - a, c - a));
			this.m_Distance = -Vector3.Dot(this.m_Normal, a);
		}

		// Token: 0x06000F27 RID: 3879 RVA: 0x0001A306 File Offset: 0x00018506
		public void Flip()
		{
			this.m_Normal = -this.m_Normal;
			this.m_Distance = -this.m_Distance;
		}

		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x06000F28 RID: 3880 RVA: 0x0001A328 File Offset: 0x00018528
		public Plane flipped
		{
			get
			{
				return new Plane(-this.m_Normal, -this.m_Distance);
			}
		}

		// Token: 0x06000F29 RID: 3881 RVA: 0x0001A354 File Offset: 0x00018554
		public void Translate(Vector3 translation)
		{
			this.m_Distance += Vector3.Dot(this.m_Normal, translation);
		}

		// Token: 0x06000F2A RID: 3882 RVA: 0x0001A370 File Offset: 0x00018570
		public static Plane Translate(Plane plane, Vector3 translation)
		{
			return new Plane(plane.m_Normal, plane.m_Distance += Vector3.Dot(plane.m_Normal, translation));
		}

		// Token: 0x06000F2B RID: 3883 RVA: 0x0001A3B0 File Offset: 0x000185B0
		public Vector3 ClosestPointOnPlane(Vector3 point)
		{
			float d = Vector3.Dot(this.m_Normal, point) + this.m_Distance;
			return point - this.m_Normal * d;
		}

		// Token: 0x06000F2C RID: 3884 RVA: 0x0001A3EC File Offset: 0x000185EC
		public float GetDistanceToPoint(Vector3 point)
		{
			return Vector3.Dot(this.m_Normal, point) + this.m_Distance;
		}

		// Token: 0x06000F2D RID: 3885 RVA: 0x0001A414 File Offset: 0x00018614
		public bool GetSide(Vector3 point)
		{
			return Vector3.Dot(this.m_Normal, point) + this.m_Distance > 0f;
		}

		// Token: 0x06000F2E RID: 3886 RVA: 0x0001A444 File Offset: 0x00018644
		public bool SameSide(Vector3 inPt0, Vector3 inPt1)
		{
			float distanceToPoint = this.GetDistanceToPoint(inPt0);
			float distanceToPoint2 = this.GetDistanceToPoint(inPt1);
			return (distanceToPoint > 0f && distanceToPoint2 > 0f) || (distanceToPoint <= 0f && distanceToPoint2 <= 0f);
		}

		// Token: 0x06000F2F RID: 3887 RVA: 0x0001A49C File Offset: 0x0001869C
		public bool Raycast(Ray ray, out float enter)
		{
			float num = Vector3.Dot(ray.direction, this.m_Normal);
			float num2 = -Vector3.Dot(ray.origin, this.m_Normal) - this.m_Distance;
			bool result;
			if (Mathf.Approximately(num, 0f))
			{
				enter = 0f;
				result = false;
			}
			else
			{
				enter = num2 / num;
				result = (enter > 0f);
			}
			return result;
		}

		// Token: 0x06000F30 RID: 3888 RVA: 0x0001A50C File Offset: 0x0001870C
		public override string ToString()
		{
			return UnityString.Format("(normal:({0:F1}, {1:F1}, {2:F1}), distance:{3:F1})", new object[]
			{
				this.m_Normal.x,
				this.m_Normal.y,
				this.m_Normal.z,
				this.m_Distance
			});
		}

		// Token: 0x06000F31 RID: 3889 RVA: 0x0001A578 File Offset: 0x00018778
		public string ToString(string format)
		{
			return UnityString.Format("(normal:({0}, {1}, {2}), distance:{3})", new object[]
			{
				this.m_Normal.x.ToString(format),
				this.m_Normal.y.ToString(format),
				this.m_Normal.z.ToString(format),
				this.m_Distance.ToString(format)
			});
		}

		// Token: 0x0400070A RID: 1802
		private Vector3 m_Normal;

		// Token: 0x0400070B RID: 1803
		private float m_Distance;
	}
}
