﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200004A RID: 74
	[UsedByNativeCode]
	[NativeHeader("Runtime/Camera/RenderManager.h")]
	[NativeHeader("Runtime/Misc/GameObjectUtility.h")]
	[NativeHeader("Runtime/Camera/Camera.h")]
	[NativeHeader("Runtime/GfxDevice/GfxDeviceTypes.h")]
	[RequireComponent(typeof(Transform))]
	[NativeHeader("Runtime/Graphics/RenderTexture.h")]
	[NativeHeader("Runtime/Shaders/Shader.h")]
	[NativeHeader("Runtime/Graphics/CommandBuffer/RenderingCommandBuffer.h")]
	public sealed class Camera : Behaviour
	{
		// Token: 0x0600037A RID: 890 RVA: 0x0000A52A File Offset: 0x0000872A
		public Camera()
		{
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600037B RID: 891
		// (set) Token: 0x0600037C RID: 892
		[NativeProperty("Near")]
		public extern float nearClipPlane { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x0600037D RID: 893
		// (set) Token: 0x0600037E RID: 894
		[NativeProperty("Far")]
		public extern float farClipPlane { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x0600037F RID: 895
		// (set) Token: 0x06000380 RID: 896
		[NativeProperty("Fov")]
		public extern float fieldOfView { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000381 RID: 897
		// (set) Token: 0x06000382 RID: 898
		public extern RenderingPath renderingPath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000383 RID: 899
		public extern RenderingPath actualRenderingPath { [NativeName("CalculateRenderingPath")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000384 RID: 900
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Reset();

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000385 RID: 901
		// (set) Token: 0x06000386 RID: 902
		public extern bool allowHDR { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000387 RID: 903
		// (set) Token: 0x06000388 RID: 904
		public extern bool allowMSAA { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000389 RID: 905
		// (set) Token: 0x0600038A RID: 906
		public extern bool allowDynamicResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x0600038B RID: 907
		// (set) Token: 0x0600038C RID: 908
		[NativeProperty("ForceIntoRT")]
		public extern bool forceIntoRenderTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x0600038D RID: 909
		// (set) Token: 0x0600038E RID: 910
		public extern float orthographicSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x0600038F RID: 911
		// (set) Token: 0x06000390 RID: 912
		public extern bool orthographic { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000391 RID: 913
		// (set) Token: 0x06000392 RID: 914
		public extern OpaqueSortMode opaqueSortMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000393 RID: 915
		// (set) Token: 0x06000394 RID: 916
		public extern TransparencySortMode transparencySortMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000395 RID: 917 RVA: 0x0000A534 File Offset: 0x00008734
		// (set) Token: 0x06000396 RID: 918 RVA: 0x0000A54A File Offset: 0x0000874A
		public Vector3 transparencySortAxis
		{
			get
			{
				Vector3 result;
				this.get_transparencySortAxis_Injected(out result);
				return result;
			}
			set
			{
				this.set_transparencySortAxis_Injected(ref value);
			}
		}

		// Token: 0x06000397 RID: 919
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ResetTransparencySortSettings();

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000398 RID: 920
		// (set) Token: 0x06000399 RID: 921
		public extern float depth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x0600039A RID: 922
		// (set) Token: 0x0600039B RID: 923
		public extern float aspect { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600039C RID: 924
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ResetAspect();

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x0600039D RID: 925 RVA: 0x0000A554 File Offset: 0x00008754
		public Vector3 velocity
		{
			get
			{
				Vector3 result;
				this.get_velocity_Injected(out result);
				return result;
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x0600039E RID: 926
		// (set) Token: 0x0600039F RID: 927
		public extern int cullingMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x060003A0 RID: 928
		// (set) Token: 0x060003A1 RID: 929
		public extern int eventMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x060003A2 RID: 930
		// (set) Token: 0x060003A3 RID: 931
		public extern bool layerCullSpherical { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x060003A4 RID: 932
		// (set) Token: 0x060003A5 RID: 933
		public extern CameraType cameraType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060003A6 RID: 934
		[FreeFunction("CameraScripting::GetLayerCullDistances", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float[] GetLayerCullDistances();

		// Token: 0x060003A7 RID: 935
		[FreeFunction("CameraScripting::SetLayerCullDistances", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetLayerCullDistances([NotNull] float[] d);

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x060003A8 RID: 936 RVA: 0x0000A56C File Offset: 0x0000876C
		// (set) Token: 0x060003A9 RID: 937 RVA: 0x0000A587 File Offset: 0x00008787
		public float[] layerCullDistances
		{
			get
			{
				return this.GetLayerCullDistances();
			}
			set
			{
				if (value.Length != 32)
				{
					throw new UnityException("Array needs to contain exactly 32 floats for layerCullDistances.");
				}
				this.SetLayerCullDistances(value);
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x060003AA RID: 938
		internal static extern int PreviewCullingLayer { [FreeFunction("CameraScripting::GetPreviewCullingLayer")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x060003AB RID: 939
		// (set) Token: 0x060003AC RID: 940
		public extern bool useOcclusionCulling { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x060003AD RID: 941 RVA: 0x0000A5A8 File Offset: 0x000087A8
		// (set) Token: 0x060003AE RID: 942 RVA: 0x0000A5BE File Offset: 0x000087BE
		public Matrix4x4 cullingMatrix
		{
			get
			{
				Matrix4x4 result;
				this.get_cullingMatrix_Injected(out result);
				return result;
			}
			set
			{
				this.set_cullingMatrix_Injected(ref value);
			}
		}

		// Token: 0x060003AF RID: 943
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ResetCullingMatrix();

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x060003B0 RID: 944 RVA: 0x0000A5C8 File Offset: 0x000087C8
		// (set) Token: 0x060003B1 RID: 945 RVA: 0x0000A5DE File Offset: 0x000087DE
		public Color backgroundColor
		{
			get
			{
				Color result;
				this.get_backgroundColor_Injected(out result);
				return result;
			}
			set
			{
				this.set_backgroundColor_Injected(ref value);
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x060003B2 RID: 946
		// (set) Token: 0x060003B3 RID: 947
		public extern CameraClearFlags clearFlags { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x060003B4 RID: 948
		// (set) Token: 0x060003B5 RID: 949
		public extern DepthTextureMode depthTextureMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x060003B6 RID: 950
		// (set) Token: 0x060003B7 RID: 951
		public extern bool clearStencilAfterLightingPass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060003B8 RID: 952
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetReplacementShader(Shader shader, string replacementTag);

		// Token: 0x060003B9 RID: 953
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ResetReplacementShader();

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x060003BA RID: 954
		internal extern Camera.ProjectionMatrixMode projectionMatrixMode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x060003BB RID: 955
		// (set) Token: 0x060003BC RID: 956
		public extern bool usePhysicalProperties { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x060003BD RID: 957 RVA: 0x0000A5E8 File Offset: 0x000087E8
		// (set) Token: 0x060003BE RID: 958 RVA: 0x0000A5FE File Offset: 0x000087FE
		public Vector2 sensorSize
		{
			get
			{
				Vector2 result;
				this.get_sensorSize_Injected(out result);
				return result;
			}
			set
			{
				this.set_sensorSize_Injected(ref value);
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x060003BF RID: 959 RVA: 0x0000A608 File Offset: 0x00008808
		// (set) Token: 0x060003C0 RID: 960 RVA: 0x0000A61E File Offset: 0x0000881E
		public Vector2 lensShift
		{
			get
			{
				Vector2 result;
				this.get_lensShift_Injected(out result);
				return result;
			}
			set
			{
				this.set_lensShift_Injected(ref value);
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x060003C1 RID: 961
		// (set) Token: 0x060003C2 RID: 962
		public extern float focalLength { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x060003C3 RID: 963
		// (set) Token: 0x060003C4 RID: 964
		public extern Camera.GateFitMode gateFit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060003C5 RID: 965 RVA: 0x0000A628 File Offset: 0x00008828
		internal Vector3 GetLocalSpaceAim()
		{
			Vector3 result;
			this.GetLocalSpaceAim_Injected(out result);
			return result;
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x060003C6 RID: 966 RVA: 0x0000A640 File Offset: 0x00008840
		// (set) Token: 0x060003C7 RID: 967 RVA: 0x0000A656 File Offset: 0x00008856
		[NativeProperty("NormalizedViewportRect")]
		public Rect rect
		{
			get
			{
				Rect result;
				this.get_rect_Injected(out result);
				return result;
			}
			set
			{
				this.set_rect_Injected(ref value);
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x060003C8 RID: 968 RVA: 0x0000A660 File Offset: 0x00008860
		// (set) Token: 0x060003C9 RID: 969 RVA: 0x0000A676 File Offset: 0x00008876
		[NativeProperty("ScreenViewportRect")]
		public Rect pixelRect
		{
			get
			{
				Rect result;
				this.get_pixelRect_Injected(out result);
				return result;
			}
			set
			{
				this.set_pixelRect_Injected(ref value);
			}
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x060003CA RID: 970
		public extern int pixelWidth { [FreeFunction("CameraScripting::GetPixelWidth", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x060003CB RID: 971
		public extern int pixelHeight { [FreeFunction("CameraScripting::GetPixelHeight", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x060003CC RID: 972
		public extern int scaledPixelWidth { [FreeFunction("CameraScripting::GetScaledPixelWidth", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x060003CD RID: 973
		public extern int scaledPixelHeight { [FreeFunction("CameraScripting::GetScaledPixelHeight", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x060003CE RID: 974
		// (set) Token: 0x060003CF RID: 975
		public extern RenderTexture targetTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x060003D0 RID: 976
		public extern RenderTexture activeTexture { [NativeName("GetCurrentTargetTexture")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x060003D1 RID: 977
		// (set) Token: 0x060003D2 RID: 978
		public extern int targetDisplay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060003D3 RID: 979 RVA: 0x0000A680 File Offset: 0x00008880
		[FreeFunction("CameraScripting::SetTargetBuffers", HasExplicitThis = true)]
		private void SetTargetBuffersImpl(RenderBuffer color, RenderBuffer depth)
		{
			this.SetTargetBuffersImpl_Injected(ref color, ref depth);
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x0000A68C File Offset: 0x0000888C
		public void SetTargetBuffers(RenderBuffer colorBuffer, RenderBuffer depthBuffer)
		{
			this.SetTargetBuffersImpl(colorBuffer, depthBuffer);
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x0000A697 File Offset: 0x00008897
		[FreeFunction("CameraScripting::SetTargetBuffers", HasExplicitThis = true)]
		private void SetTargetBuffersMRTImpl(RenderBuffer[] color, RenderBuffer depth)
		{
			this.SetTargetBuffersMRTImpl_Injected(color, ref depth);
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x0000A6A2 File Offset: 0x000088A2
		public void SetTargetBuffers(RenderBuffer[] colorBuffer, RenderBuffer depthBuffer)
		{
			this.SetTargetBuffersMRTImpl(colorBuffer, depthBuffer);
		}

		// Token: 0x060003D7 RID: 983
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string[] GetCameraBufferWarnings();

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x060003D8 RID: 984 RVA: 0x0000A6B0 File Offset: 0x000088B0
		public Matrix4x4 cameraToWorldMatrix
		{
			get
			{
				Matrix4x4 result;
				this.get_cameraToWorldMatrix_Injected(out result);
				return result;
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x060003D9 RID: 985 RVA: 0x0000A6C8 File Offset: 0x000088C8
		// (set) Token: 0x060003DA RID: 986 RVA: 0x0000A6DE File Offset: 0x000088DE
		public Matrix4x4 worldToCameraMatrix
		{
			get
			{
				Matrix4x4 result;
				this.get_worldToCameraMatrix_Injected(out result);
				return result;
			}
			set
			{
				this.set_worldToCameraMatrix_Injected(ref value);
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x060003DB RID: 987 RVA: 0x0000A6E8 File Offset: 0x000088E8
		// (set) Token: 0x060003DC RID: 988 RVA: 0x0000A6FE File Offset: 0x000088FE
		public Matrix4x4 projectionMatrix
		{
			get
			{
				Matrix4x4 result;
				this.get_projectionMatrix_Injected(out result);
				return result;
			}
			set
			{
				this.set_projectionMatrix_Injected(ref value);
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x060003DD RID: 989 RVA: 0x0000A708 File Offset: 0x00008908
		// (set) Token: 0x060003DE RID: 990 RVA: 0x0000A71E File Offset: 0x0000891E
		public Matrix4x4 nonJitteredProjectionMatrix
		{
			get
			{
				Matrix4x4 result;
				this.get_nonJitteredProjectionMatrix_Injected(out result);
				return result;
			}
			set
			{
				this.set_nonJitteredProjectionMatrix_Injected(ref value);
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x060003DF RID: 991
		// (set) Token: 0x060003E0 RID: 992
		[NativeProperty("UseJitteredProjectionMatrixForTransparent")]
		public extern bool useJitteredProjectionMatrixForTransparentRendering { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x060003E1 RID: 993 RVA: 0x0000A728 File Offset: 0x00008928
		public Matrix4x4 previousViewProjectionMatrix
		{
			get
			{
				Matrix4x4 result;
				this.get_previousViewProjectionMatrix_Injected(out result);
				return result;
			}
		}

		// Token: 0x060003E2 RID: 994
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ResetWorldToCameraMatrix();

		// Token: 0x060003E3 RID: 995
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ResetProjectionMatrix();

		// Token: 0x060003E4 RID: 996 RVA: 0x0000A740 File Offset: 0x00008940
		[FreeFunction("CameraScripting::CalculateObliqueMatrix", HasExplicitThis = true)]
		public Matrix4x4 CalculateObliqueMatrix(Vector4 clipPlane)
		{
			Matrix4x4 result;
			this.CalculateObliqueMatrix_Injected(ref clipPlane, out result);
			return result;
		}

		// Token: 0x060003E5 RID: 997 RVA: 0x0000A758 File Offset: 0x00008958
		public Vector3 WorldToScreenPoint(Vector3 position, Camera.MonoOrStereoscopicEye eye)
		{
			Vector3 result;
			this.WorldToScreenPoint_Injected(ref position, eye, out result);
			return result;
		}

		// Token: 0x060003E6 RID: 998 RVA: 0x0000A774 File Offset: 0x00008974
		public Vector3 WorldToViewportPoint(Vector3 position, Camera.MonoOrStereoscopicEye eye)
		{
			Vector3 result;
			this.WorldToViewportPoint_Injected(ref position, eye, out result);
			return result;
		}

		// Token: 0x060003E7 RID: 999 RVA: 0x0000A790 File Offset: 0x00008990
		public Vector3 ViewportToWorldPoint(Vector3 position, Camera.MonoOrStereoscopicEye eye)
		{
			Vector3 result;
			this.ViewportToWorldPoint_Injected(ref position, eye, out result);
			return result;
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x0000A7AC File Offset: 0x000089AC
		public Vector3 ScreenToWorldPoint(Vector3 position, Camera.MonoOrStereoscopicEye eye)
		{
			Vector3 result;
			this.ScreenToWorldPoint_Injected(ref position, eye, out result);
			return result;
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x0000A7C8 File Offset: 0x000089C8
		public Vector3 WorldToScreenPoint(Vector3 position)
		{
			return this.WorldToScreenPoint(position, Camera.MonoOrStereoscopicEye.Mono);
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x0000A7E8 File Offset: 0x000089E8
		public Vector3 WorldToViewportPoint(Vector3 position)
		{
			return this.WorldToViewportPoint(position, Camera.MonoOrStereoscopicEye.Mono);
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x0000A808 File Offset: 0x00008A08
		public Vector3 ViewportToWorldPoint(Vector3 position)
		{
			return this.ViewportToWorldPoint(position, Camera.MonoOrStereoscopicEye.Mono);
		}

		// Token: 0x060003EC RID: 1004 RVA: 0x0000A828 File Offset: 0x00008A28
		public Vector3 ScreenToWorldPoint(Vector3 position)
		{
			return this.ScreenToWorldPoint(position, Camera.MonoOrStereoscopicEye.Mono);
		}

		// Token: 0x060003ED RID: 1005 RVA: 0x0000A848 File Offset: 0x00008A48
		public Vector3 ScreenToViewportPoint(Vector3 position)
		{
			Vector3 result;
			this.ScreenToViewportPoint_Injected(ref position, out result);
			return result;
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x0000A860 File Offset: 0x00008A60
		public Vector3 ViewportToScreenPoint(Vector3 position)
		{
			Vector3 result;
			this.ViewportToScreenPoint_Injected(ref position, out result);
			return result;
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x0000A878 File Offset: 0x00008A78
		internal Vector2 GetFrustumPlaneSizeAt(float distance)
		{
			Vector2 result;
			this.GetFrustumPlaneSizeAt_Injected(distance, out result);
			return result;
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x0000A890 File Offset: 0x00008A90
		private Ray ViewportPointToRay(Vector2 pos, Camera.MonoOrStereoscopicEye eye)
		{
			Ray result;
			this.ViewportPointToRay_Injected(ref pos, eye, out result);
			return result;
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x0000A8AC File Offset: 0x00008AAC
		public Ray ViewportPointToRay(Vector3 pos, Camera.MonoOrStereoscopicEye eye)
		{
			return this.ViewportPointToRay(pos, eye);
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x0000A8D0 File Offset: 0x00008AD0
		public Ray ViewportPointToRay(Vector3 pos)
		{
			return this.ViewportPointToRay(pos, Camera.MonoOrStereoscopicEye.Mono);
		}

		// Token: 0x060003F3 RID: 1011 RVA: 0x0000A8F0 File Offset: 0x00008AF0
		private Ray ScreenPointToRay(Vector2 pos, Camera.MonoOrStereoscopicEye eye)
		{
			Ray result;
			this.ScreenPointToRay_Injected(ref pos, eye, out result);
			return result;
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x0000A90C File Offset: 0x00008B0C
		public Ray ScreenPointToRay(Vector3 pos, Camera.MonoOrStereoscopicEye eye)
		{
			return this.ScreenPointToRay(pos, eye);
		}

		// Token: 0x060003F5 RID: 1013 RVA: 0x0000A930 File Offset: 0x00008B30
		public Ray ScreenPointToRay(Vector3 pos)
		{
			return this.ScreenPointToRay(pos, Camera.MonoOrStereoscopicEye.Mono);
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x0000A94D File Offset: 0x00008B4D
		[FreeFunction("CameraScripting::RaycastTry", HasExplicitThis = true)]
		internal GameObject RaycastTry(Ray ray, float distance, int layerMask)
		{
			return this.RaycastTry_Injected(ref ray, distance, layerMask);
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x0000A959 File Offset: 0x00008B59
		[FreeFunction("CameraScripting::RaycastTry2D", HasExplicitThis = true)]
		internal GameObject RaycastTry2D(Ray ray, float distance, int layerMask)
		{
			return this.RaycastTry2D_Injected(ref ray, distance, layerMask);
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x0000A965 File Offset: 0x00008B65
		[FreeFunction("CameraScripting::CalculateViewportRayVectors", HasExplicitThis = true)]
		private void CalculateFrustumCornersInternal(Rect viewport, float z, Camera.MonoOrStereoscopicEye eye, [Out] Vector3[] outCorners)
		{
			this.CalculateFrustumCornersInternal_Injected(ref viewport, z, eye, outCorners);
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x0000A973 File Offset: 0x00008B73
		public void CalculateFrustumCorners(Rect viewport, float z, Camera.MonoOrStereoscopicEye eye, Vector3[] outCorners)
		{
			if (outCorners == null)
			{
				throw new ArgumentNullException("outCorners");
			}
			if (outCorners.Length < 4)
			{
				throw new ArgumentException("outCorners minimum size is 4", "outCorners");
			}
			this.CalculateFrustumCornersInternal(viewport, z, eye, outCorners);
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x0000A9AD File Offset: 0x00008BAD
		[NativeName("CalculateProjectionMatrixFromPhysicalProperties")]
		private static void CalculateProjectionMatrixFromPhysicalPropertiesInternal(out Matrix4x4 output, float focalLength, Vector2 sensorSize, Vector2 lensShift, float nearClip, float farClip, float gateAspect, Camera.GateFitMode gateFitMode)
		{
			Camera.CalculateProjectionMatrixFromPhysicalPropertiesInternal_Injected(out output, focalLength, ref sensorSize, ref lensShift, nearClip, farClip, gateAspect, gateFitMode);
		}

		// Token: 0x060003FB RID: 1019 RVA: 0x0000A9C2 File Offset: 0x00008BC2
		public static void CalculateProjectionMatrixFromPhysicalProperties(out Matrix4x4 output, float focalLength, Vector2 sensorSize, Vector2 lensShift, float nearClip, float farClip, Camera.GateFitParameters gateFitParameters = default(Camera.GateFitParameters))
		{
			Camera.CalculateProjectionMatrixFromPhysicalPropertiesInternal(out output, focalLength, sensorSize, lensShift, nearClip, farClip, gateFitParameters.aspect, gateFitParameters.mode);
		}

		// Token: 0x060003FC RID: 1020
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float FocalLengthToFOV(float focalLength, float sensorSize);

		// Token: 0x060003FD RID: 1021
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float FOVToFocalLength(float fov, float sensorSize);

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x060003FE RID: 1022
		public static extern Camera main { [FreeFunction("FindMainCamera")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x060003FF RID: 1023
		public static extern Camera current { [FreeFunction("GetCurrentCameraPtr")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000400 RID: 1024 RVA: 0x0000A9E0 File Offset: 0x00008BE0
		// (set) Token: 0x06000401 RID: 1025 RVA: 0x0000A9F6 File Offset: 0x00008BF6
		public Scene scene
		{
			[FreeFunction("CameraScripting::GetScene", HasExplicitThis = true)]
			get
			{
				Scene result;
				this.get_scene_Injected(out result);
				return result;
			}
			[FreeFunction("CameraScripting::SetScene", HasExplicitThis = true)]
			set
			{
				this.set_scene_Injected(ref value);
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x06000402 RID: 1026
		public extern bool stereoEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000403 RID: 1027
		// (set) Token: 0x06000404 RID: 1028
		public extern float stereoSeparation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000405 RID: 1029
		// (set) Token: 0x06000406 RID: 1030
		public extern float stereoConvergence { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000407 RID: 1031
		public extern bool areVRStereoViewMatricesWithinSingleCullTolerance { [NativeName("AreVRStereoViewMatricesWithinSingleCullTolerance")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x06000408 RID: 1032
		// (set) Token: 0x06000409 RID: 1033
		public extern StereoTargetEyeMask stereoTargetEye { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x0600040A RID: 1034
		public extern Camera.MonoOrStereoscopicEye stereoActiveEye { [FreeFunction("CameraScripting::GetStereoActiveEye", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600040B RID: 1035 RVA: 0x0000AA00 File Offset: 0x00008C00
		public Matrix4x4 GetStereoNonJitteredProjectionMatrix(Camera.StereoscopicEye eye)
		{
			Matrix4x4 result;
			this.GetStereoNonJitteredProjectionMatrix_Injected(eye, out result);
			return result;
		}

		// Token: 0x0600040C RID: 1036 RVA: 0x0000AA18 File Offset: 0x00008C18
		public Matrix4x4 GetStereoViewMatrix(Camera.StereoscopicEye eye)
		{
			Matrix4x4 result;
			this.GetStereoViewMatrix_Injected(eye, out result);
			return result;
		}

		// Token: 0x0600040D RID: 1037
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CopyStereoDeviceProjectionMatrixToNonJittered(Camera.StereoscopicEye eye);

		// Token: 0x0600040E RID: 1038 RVA: 0x0000AA30 File Offset: 0x00008C30
		public Matrix4x4 GetStereoProjectionMatrix(Camera.StereoscopicEye eye)
		{
			Matrix4x4 result;
			this.GetStereoProjectionMatrix_Injected(eye, out result);
			return result;
		}

		// Token: 0x0600040F RID: 1039 RVA: 0x0000AA47 File Offset: 0x00008C47
		public void SetStereoProjectionMatrix(Camera.StereoscopicEye eye, Matrix4x4 matrix)
		{
			this.SetStereoProjectionMatrix_Injected(eye, ref matrix);
		}

		// Token: 0x06000410 RID: 1040
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ResetStereoProjectionMatrices();

		// Token: 0x06000411 RID: 1041 RVA: 0x0000AA52 File Offset: 0x00008C52
		public void SetStereoViewMatrix(Camera.StereoscopicEye eye, Matrix4x4 matrix)
		{
			this.SetStereoViewMatrix_Injected(eye, ref matrix);
		}

		// Token: 0x06000412 RID: 1042
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ResetStereoViewMatrices();

		// Token: 0x06000413 RID: 1043
		[FreeFunction("CameraScripting::GetAllCamerasCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetAllCamerasCount();

		// Token: 0x06000414 RID: 1044
		[FreeFunction("CameraScripting::GetAllCameras")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetAllCamerasImpl([NotNull] [Out] Camera[] cam);

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000415 RID: 1045 RVA: 0x0000AA60 File Offset: 0x00008C60
		public static int allCamerasCount
		{
			get
			{
				return Camera.GetAllCamerasCount();
			}
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x06000416 RID: 1046 RVA: 0x0000AA7C File Offset: 0x00008C7C
		public static Camera[] allCameras
		{
			get
			{
				Camera[] array = new Camera[Camera.allCamerasCount];
				Camera.GetAllCamerasImpl(array);
				return array;
			}
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x0000AAA4 File Offset: 0x00008CA4
		public static int GetAllCameras(Camera[] cameras)
		{
			if (cameras == null)
			{
				throw new NullReferenceException();
			}
			if (cameras.Length < Camera.allCamerasCount)
			{
				throw new ArgumentException("Passed in array to fill with cameras is to small to hold the number of cameras. Use Camera.allCamerasCount to get the needed size.");
			}
			return Camera.GetAllCamerasImpl(cameras);
		}

		// Token: 0x06000418 RID: 1048
		[FreeFunction("CameraScripting::RenderToCubemap", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool RenderToCubemapImpl(Texture tex, [DefaultValue("63")] int faceMask);

		// Token: 0x06000419 RID: 1049 RVA: 0x0000AAE4 File Offset: 0x00008CE4
		public bool RenderToCubemap(Cubemap cubemap, int faceMask)
		{
			return this.RenderToCubemapImpl(cubemap, faceMask);
		}

		// Token: 0x0600041A RID: 1050 RVA: 0x0000AB04 File Offset: 0x00008D04
		public bool RenderToCubemap(Cubemap cubemap)
		{
			return this.RenderToCubemapImpl(cubemap, 63);
		}

		// Token: 0x0600041B RID: 1051 RVA: 0x0000AB24 File Offset: 0x00008D24
		public bool RenderToCubemap(RenderTexture cubemap, int faceMask)
		{
			return this.RenderToCubemapImpl(cubemap, faceMask);
		}

		// Token: 0x0600041C RID: 1052 RVA: 0x0000AB44 File Offset: 0x00008D44
		public bool RenderToCubemap(RenderTexture cubemap)
		{
			return this.RenderToCubemapImpl(cubemap, 63);
		}

		// Token: 0x0600041D RID: 1053
		[NativeName("RenderToCubemap")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool RenderToCubemapEyeImpl(RenderTexture cubemap, int faceMask, Camera.MonoOrStereoscopicEye stereoEye);

		// Token: 0x0600041E RID: 1054 RVA: 0x0000AB64 File Offset: 0x00008D64
		public bool RenderToCubemap(RenderTexture cubemap, int faceMask, Camera.MonoOrStereoscopicEye stereoEye)
		{
			return this.RenderToCubemapEyeImpl(cubemap, faceMask, stereoEye);
		}

		// Token: 0x0600041F RID: 1055
		[FreeFunction("CameraScripting::Render", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Render();

		// Token: 0x06000420 RID: 1056
		[FreeFunction("CameraScripting::RenderWithShader", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RenderWithShader(Shader shader, string replacementTag);

		// Token: 0x06000421 RID: 1057
		[FreeFunction("CameraScripting::RenderDontRestore", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RenderDontRestore();

		// Token: 0x06000422 RID: 1058
		[FreeFunction("CameraScripting::SetupCurrent")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetupCurrent(Camera cur);

		// Token: 0x06000423 RID: 1059
		[FreeFunction("CameraScripting::CopyFrom", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CopyFrom(Camera other);

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x06000424 RID: 1060
		public extern int commandBufferCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000425 RID: 1061
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveCommandBuffers(CameraEvent evt);

		// Token: 0x06000426 RID: 1062
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveAllCommandBuffers();

		// Token: 0x06000427 RID: 1063
		[NativeName("AddCommandBuffer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AddCommandBufferImpl(CameraEvent evt, [NotNull] CommandBuffer buffer);

		// Token: 0x06000428 RID: 1064
		[NativeName("AddCommandBufferAsync")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AddCommandBufferAsyncImpl(CameraEvent evt, [NotNull] CommandBuffer buffer, ComputeQueueType queueType);

		// Token: 0x06000429 RID: 1065
		[NativeName("RemoveCommandBuffer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RemoveCommandBufferImpl(CameraEvent evt, [NotNull] CommandBuffer buffer);

		// Token: 0x0600042A RID: 1066 RVA: 0x0000AB84 File Offset: 0x00008D84
		public void AddCommandBuffer(CameraEvent evt, CommandBuffer buffer)
		{
			if (!CameraEventUtils.IsValid(evt))
			{
				throw new ArgumentException(string.Format("Invalid CameraEvent value \"{0}\".", (int)evt), "evt");
			}
			if (buffer == null)
			{
				throw new NullReferenceException("buffer is null");
			}
			this.AddCommandBufferImpl(evt, buffer);
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x0000ABD4 File Offset: 0x00008DD4
		public void AddCommandBufferAsync(CameraEvent evt, CommandBuffer buffer, ComputeQueueType queueType)
		{
			if (!CameraEventUtils.IsValid(evt))
			{
				throw new ArgumentException(string.Format("Invalid CameraEvent value \"{0}\".", (int)evt), "evt");
			}
			if (buffer == null)
			{
				throw new NullReferenceException("buffer is null");
			}
			this.AddCommandBufferAsyncImpl(evt, buffer, queueType);
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x0000AC24 File Offset: 0x00008E24
		public void RemoveCommandBuffer(CameraEvent evt, CommandBuffer buffer)
		{
			if (!CameraEventUtils.IsValid(evt))
			{
				throw new ArgumentException(string.Format("Invalid CameraEvent value \"{0}\".", (int)evt), "evt");
			}
			if (buffer == null)
			{
				throw new NullReferenceException("buffer is null");
			}
			this.RemoveCommandBufferImpl(evt, buffer);
		}

		// Token: 0x0600042D RID: 1069
		[FreeFunction("CameraScripting::GetCommandBuffers", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern CommandBuffer[] GetCommandBuffers(CameraEvent evt);

		// Token: 0x0600042E RID: 1070 RVA: 0x0000AC71 File Offset: 0x00008E71
		[RequiredByNativeCode]
		private static void FireOnPreCull(Camera cam)
		{
			if (Camera.onPreCull != null)
			{
				Camera.onPreCull(cam);
			}
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x0000AC89 File Offset: 0x00008E89
		[RequiredByNativeCode]
		private static void FireOnPreRender(Camera cam)
		{
			if (Camera.onPreRender != null)
			{
				Camera.onPreRender(cam);
			}
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x0000ACA1 File Offset: 0x00008EA1
		[RequiredByNativeCode]
		private static void FireOnPostRender(Camera cam)
		{
			if (Camera.onPostRender != null)
			{
				Camera.onPostRender(cam);
			}
		}

		// Token: 0x06000431 RID: 1073 RVA: 0x00007476 File Offset: 0x00005676
		internal void OnlyUsedForTesting1()
		{
		}

		// Token: 0x06000432 RID: 1074 RVA: 0x00007476 File Offset: 0x00005676
		internal void OnlyUsedForTesting2()
		{
		}

		// Token: 0x06000433 RID: 1075
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_transparencySortAxis_Injected(out Vector3 ret);

		// Token: 0x06000434 RID: 1076
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_transparencySortAxis_Injected(ref Vector3 value);

		// Token: 0x06000435 RID: 1077
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_velocity_Injected(out Vector3 ret);

		// Token: 0x06000436 RID: 1078
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_cullingMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x06000437 RID: 1079
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_cullingMatrix_Injected(ref Matrix4x4 value);

		// Token: 0x06000438 RID: 1080
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_backgroundColor_Injected(out Color ret);

		// Token: 0x06000439 RID: 1081
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_backgroundColor_Injected(ref Color value);

		// Token: 0x0600043A RID: 1082
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_sensorSize_Injected(out Vector2 ret);

		// Token: 0x0600043B RID: 1083
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_sensorSize_Injected(ref Vector2 value);

		// Token: 0x0600043C RID: 1084
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_lensShift_Injected(out Vector2 ret);

		// Token: 0x0600043D RID: 1085
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_lensShift_Injected(ref Vector2 value);

		// Token: 0x0600043E RID: 1086
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetLocalSpaceAim_Injected(out Vector3 ret);

		// Token: 0x0600043F RID: 1087
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rect_Injected(out Rect ret);

		// Token: 0x06000440 RID: 1088
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rect_Injected(ref Rect value);

		// Token: 0x06000441 RID: 1089
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_pixelRect_Injected(out Rect ret);

		// Token: 0x06000442 RID: 1090
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_pixelRect_Injected(ref Rect value);

		// Token: 0x06000443 RID: 1091
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetTargetBuffersImpl_Injected(ref RenderBuffer color, ref RenderBuffer depth);

		// Token: 0x06000444 RID: 1092
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetTargetBuffersMRTImpl_Injected(RenderBuffer[] color, ref RenderBuffer depth);

		// Token: 0x06000445 RID: 1093
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_cameraToWorldMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x06000446 RID: 1094
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_worldToCameraMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x06000447 RID: 1095
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_worldToCameraMatrix_Injected(ref Matrix4x4 value);

		// Token: 0x06000448 RID: 1096
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_projectionMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x06000449 RID: 1097
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_projectionMatrix_Injected(ref Matrix4x4 value);

		// Token: 0x0600044A RID: 1098
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_nonJitteredProjectionMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x0600044B RID: 1099
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_nonJitteredProjectionMatrix_Injected(ref Matrix4x4 value);

		// Token: 0x0600044C RID: 1100
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_previousViewProjectionMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x0600044D RID: 1101
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void CalculateObliqueMatrix_Injected(ref Vector4 clipPlane, out Matrix4x4 ret);

		// Token: 0x0600044E RID: 1102
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void WorldToScreenPoint_Injected(ref Vector3 position, Camera.MonoOrStereoscopicEye eye, out Vector3 ret);

		// Token: 0x0600044F RID: 1103
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void WorldToViewportPoint_Injected(ref Vector3 position, Camera.MonoOrStereoscopicEye eye, out Vector3 ret);

		// Token: 0x06000450 RID: 1104
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ViewportToWorldPoint_Injected(ref Vector3 position, Camera.MonoOrStereoscopicEye eye, out Vector3 ret);

		// Token: 0x06000451 RID: 1105
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ScreenToWorldPoint_Injected(ref Vector3 position, Camera.MonoOrStereoscopicEye eye, out Vector3 ret);

		// Token: 0x06000452 RID: 1106
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ScreenToViewportPoint_Injected(ref Vector3 position, out Vector3 ret);

		// Token: 0x06000453 RID: 1107
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ViewportToScreenPoint_Injected(ref Vector3 position, out Vector3 ret);

		// Token: 0x06000454 RID: 1108
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetFrustumPlaneSizeAt_Injected(float distance, out Vector2 ret);

		// Token: 0x06000455 RID: 1109
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ViewportPointToRay_Injected(ref Vector2 pos, Camera.MonoOrStereoscopicEye eye, out Ray ret);

		// Token: 0x06000456 RID: 1110
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ScreenPointToRay_Injected(ref Vector2 pos, Camera.MonoOrStereoscopicEye eye, out Ray ret);

		// Token: 0x06000457 RID: 1111
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern GameObject RaycastTry_Injected(ref Ray ray, float distance, int layerMask);

		// Token: 0x06000458 RID: 1112
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern GameObject RaycastTry2D_Injected(ref Ray ray, float distance, int layerMask);

		// Token: 0x06000459 RID: 1113
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void CalculateFrustumCornersInternal_Injected(ref Rect viewport, float z, Camera.MonoOrStereoscopicEye eye, [Out] Vector3[] outCorners);

		// Token: 0x0600045A RID: 1114
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CalculateProjectionMatrixFromPhysicalPropertiesInternal_Injected(out Matrix4x4 output, float focalLength, ref Vector2 sensorSize, ref Vector2 lensShift, float nearClip, float farClip, float gateAspect, Camera.GateFitMode gateFitMode);

		// Token: 0x0600045B RID: 1115
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_scene_Injected(out Scene ret);

		// Token: 0x0600045C RID: 1116
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_scene_Injected(ref Scene value);

		// Token: 0x0600045D RID: 1117
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetStereoNonJitteredProjectionMatrix_Injected(Camera.StereoscopicEye eye, out Matrix4x4 ret);

		// Token: 0x0600045E RID: 1118
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetStereoViewMatrix_Injected(Camera.StereoscopicEye eye, out Matrix4x4 ret);

		// Token: 0x0600045F RID: 1119
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetStereoProjectionMatrix_Injected(Camera.StereoscopicEye eye, out Matrix4x4 ret);

		// Token: 0x06000460 RID: 1120
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetStereoProjectionMatrix_Injected(Camera.StereoscopicEye eye, ref Matrix4x4 matrix);

		// Token: 0x06000461 RID: 1121
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetStereoViewMatrix_Injected(Camera.StereoscopicEye eye, ref Matrix4x4 matrix);

		// Token: 0x040000F6 RID: 246
		public static Camera.CameraCallback onPreCull;

		// Token: 0x040000F7 RID: 247
		public static Camera.CameraCallback onPreRender;

		// Token: 0x040000F8 RID: 248
		public static Camera.CameraCallback onPostRender;

		// Token: 0x0200004B RID: 75
		internal enum ProjectionMatrixMode
		{
			// Token: 0x040000FA RID: 250
			Explicit,
			// Token: 0x040000FB RID: 251
			Implicit,
			// Token: 0x040000FC RID: 252
			PhysicalPropertiesBased
		}

		// Token: 0x0200004C RID: 76
		public enum GateFitMode
		{
			// Token: 0x040000FE RID: 254
			Vertical = 1,
			// Token: 0x040000FF RID: 255
			Horizontal,
			// Token: 0x04000100 RID: 256
			Fill,
			// Token: 0x04000101 RID: 257
			Overscan,
			// Token: 0x04000102 RID: 258
			None = 0
		}

		// Token: 0x0200004D RID: 77
		public struct GateFitParameters
		{
			// Token: 0x06000462 RID: 1122 RVA: 0x0000ACB9 File Offset: 0x00008EB9
			public GateFitParameters(Camera.GateFitMode mode, float aspect)
			{
				this.mode = mode;
				this.aspect = aspect;
			}

			// Token: 0x170000CA RID: 202
			// (get) Token: 0x06000463 RID: 1123 RVA: 0x0000ACCC File Offset: 0x00008ECC
			// (set) Token: 0x06000464 RID: 1124 RVA: 0x0000ACE6 File Offset: 0x00008EE6
			public Camera.GateFitMode mode
			{
				[CompilerGenerated]
				get
				{
					return this.<mode>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<mode>k__BackingField = value;
				}
			}

			// Token: 0x170000CB RID: 203
			// (get) Token: 0x06000465 RID: 1125 RVA: 0x0000ACF0 File Offset: 0x00008EF0
			// (set) Token: 0x06000466 RID: 1126 RVA: 0x0000AD0A File Offset: 0x00008F0A
			public float aspect
			{
				[CompilerGenerated]
				get
				{
					return this.<aspect>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<aspect>k__BackingField = value;
				}
			}

			// Token: 0x04000103 RID: 259
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private Camera.GateFitMode <mode>k__BackingField;

			// Token: 0x04000104 RID: 260
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private float <aspect>k__BackingField;
		}

		// Token: 0x0200004E RID: 78
		public enum StereoscopicEye
		{
			// Token: 0x04000106 RID: 262
			Left,
			// Token: 0x04000107 RID: 263
			Right
		}

		// Token: 0x0200004F RID: 79
		public enum MonoOrStereoscopicEye
		{
			// Token: 0x04000109 RID: 265
			Left,
			// Token: 0x0400010A RID: 266
			Right,
			// Token: 0x0400010B RID: 267
			Mono
		}

		// Token: 0x02000050 RID: 80
		// (Invoke) Token: 0x06000468 RID: 1128
		public delegate void CameraCallback(Camera cam);
	}
}
