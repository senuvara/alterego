﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000060 RID: 96
	[NativeHeader("Runtime/Export/CullingGroup.bindings.h")]
	[StructLayout(LayoutKind.Sequential)]
	public class CullingGroup : IDisposable
	{
		// Token: 0x06000508 RID: 1288 RVA: 0x0000CCFE File Offset: 0x0000AEFE
		public CullingGroup()
		{
			this.m_Ptr = CullingGroup.Init(this);
		}

		// Token: 0x06000509 RID: 1289 RVA: 0x0000CD1C File Offset: 0x0000AF1C
		~CullingGroup()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				this.FinalizerFailure();
			}
		}

		// Token: 0x0600050A RID: 1290
		[FreeFunction("CullingGroup_Bindings::Dispose", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void DisposeInternal();

		// Token: 0x0600050B RID: 1291 RVA: 0x0000CD64 File Offset: 0x0000AF64
		public void Dispose()
		{
			this.DisposeInternal();
			this.m_Ptr = IntPtr.Zero;
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x0600050C RID: 1292 RVA: 0x0000CD78 File Offset: 0x0000AF78
		// (set) Token: 0x0600050D RID: 1293 RVA: 0x0000CD93 File Offset: 0x0000AF93
		public CullingGroup.StateChanged onStateChanged
		{
			get
			{
				return this.m_OnStateChanged;
			}
			set
			{
				this.m_OnStateChanged = value;
			}
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x0600050E RID: 1294
		// (set) Token: 0x0600050F RID: 1295
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000510 RID: 1296
		// (set) Token: 0x06000511 RID: 1297
		public extern Camera targetCamera { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000512 RID: 1298
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetBoundingSpheres(BoundingSphere[] array);

		// Token: 0x06000513 RID: 1299
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetBoundingSphereCount(int count);

		// Token: 0x06000514 RID: 1300
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void EraseSwapBack(int index);

		// Token: 0x06000515 RID: 1301 RVA: 0x0000CD9D File Offset: 0x0000AF9D
		public static void EraseSwapBack<T>(int index, T[] myArray, ref int size)
		{
			size--;
			myArray[index] = myArray[size];
		}

		// Token: 0x06000516 RID: 1302 RVA: 0x0000CDB8 File Offset: 0x0000AFB8
		public int QueryIndices(bool visible, int[] result, int firstIndex)
		{
			return this.QueryIndices(visible, -1, CullingQueryOptions.IgnoreDistance, result, firstIndex);
		}

		// Token: 0x06000517 RID: 1303 RVA: 0x0000CDD8 File Offset: 0x0000AFD8
		public int QueryIndices(int distanceIndex, int[] result, int firstIndex)
		{
			return this.QueryIndices(false, distanceIndex, CullingQueryOptions.IgnoreVisibility, result, firstIndex);
		}

		// Token: 0x06000518 RID: 1304 RVA: 0x0000CDF8 File Offset: 0x0000AFF8
		public int QueryIndices(bool visible, int distanceIndex, int[] result, int firstIndex)
		{
			return this.QueryIndices(visible, distanceIndex, CullingQueryOptions.Normal, result, firstIndex);
		}

		// Token: 0x06000519 RID: 1305
		[FreeFunction("CullingGroup_Bindings::QueryIndices", HasExplicitThis = true)]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int QueryIndices(bool visible, int distanceIndex, CullingQueryOptions options, int[] result, int firstIndex);

		// Token: 0x0600051A RID: 1306
		[NativeThrows]
		[FreeFunction("CullingGroup_Bindings::IsVisible", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsVisible(int index);

		// Token: 0x0600051B RID: 1307
		[FreeFunction("CullingGroup_Bindings::GetDistance", HasExplicitThis = true)]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetDistance(int index);

		// Token: 0x0600051C RID: 1308
		[FreeFunction("CullingGroup_Bindings::SetBoundingDistances", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetBoundingDistances(float[] distances);

		// Token: 0x0600051D RID: 1309 RVA: 0x0000CE19 File Offset: 0x0000B019
		[FreeFunction("CullingGroup_Bindings::SetDistanceReferencePoint", HasExplicitThis = true)]
		private void SetDistanceReferencePoint_InternalVector3(Vector3 point)
		{
			this.SetDistanceReferencePoint_InternalVector3_Injected(ref point);
		}

		// Token: 0x0600051E RID: 1310
		[NativeMethod("SetDistanceReferenceTransform")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetDistanceReferencePoint_InternalTransform(Transform transform);

		// Token: 0x0600051F RID: 1311 RVA: 0x0000CE23 File Offset: 0x0000B023
		public void SetDistanceReferencePoint(Vector3 point)
		{
			this.SetDistanceReferencePoint_InternalVector3(point);
		}

		// Token: 0x06000520 RID: 1312 RVA: 0x0000CE2D File Offset: 0x0000B02D
		public void SetDistanceReferencePoint(Transform transform)
		{
			this.SetDistanceReferencePoint_InternalTransform(transform);
		}

		// Token: 0x06000521 RID: 1313 RVA: 0x0000CE38 File Offset: 0x0000B038
		[SecuritySafeCritical]
		[RequiredByNativeCode]
		private unsafe static void SendEvents(CullingGroup cullingGroup, IntPtr eventsPtr, int count)
		{
			CullingGroupEvent* ptr = (CullingGroupEvent*)eventsPtr.ToPointer();
			if (cullingGroup.m_OnStateChanged != null)
			{
				for (int i = 0; i < count; i++)
				{
					cullingGroup.m_OnStateChanged(ptr[i]);
				}
			}
		}

		// Token: 0x06000522 RID: 1314
		[FreeFunction("CullingGroup_Bindings::Init")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Init(object scripting);

		// Token: 0x06000523 RID: 1315
		[FreeFunction("CullingGroup_Bindings::FinalizerFailure", HasExplicitThis = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void FinalizerFailure();

		// Token: 0x06000524 RID: 1316
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetDistanceReferencePoint_InternalVector3_Injected(ref Vector3 point);

		// Token: 0x0400012A RID: 298
		internal IntPtr m_Ptr;

		// Token: 0x0400012B RID: 299
		private CullingGroup.StateChanged m_OnStateChanged = null;

		// Token: 0x02000061 RID: 97
		// (Invoke) Token: 0x06000526 RID: 1318
		public delegate void StateChanged(CullingGroupEvent sphere);
	}
}
