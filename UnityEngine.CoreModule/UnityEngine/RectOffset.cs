﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000075 RID: 117
	[UsedByNativeCode]
	[NativeHeader("Runtime/Camera/RenderLayers/GUILayer.h")]
	[NativeHeader("Modules/IMGUI/GUIStyle.h")]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public class RectOffset
	{
		// Token: 0x060005A5 RID: 1445 RVA: 0x0000D796 File Offset: 0x0000B996
		public RectOffset()
		{
			this.m_Ptr = RectOffset.InternalCreate();
		}

		// Token: 0x060005A6 RID: 1446 RVA: 0x0000D7AA File Offset: 0x0000B9AA
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.IMGUIModule"
		})]
		internal RectOffset(object sourceStyle, IntPtr source)
		{
			this.m_SourceStyle = sourceStyle;
			this.m_Ptr = source;
		}

		// Token: 0x060005A7 RID: 1447 RVA: 0x0000D7C1 File Offset: 0x0000B9C1
		public RectOffset(int left, int right, int top, int bottom)
		{
			this.m_Ptr = RectOffset.InternalCreate();
			this.left = left;
			this.right = right;
			this.top = top;
			this.bottom = bottom;
		}

		// Token: 0x060005A8 RID: 1448
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr InternalCreate();

		// Token: 0x060005A9 RID: 1449
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalDestroy(IntPtr ptr);

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x060005AA RID: 1450
		// (set) Token: 0x060005AB RID: 1451
		[NativeProperty("left", false, TargetType.Field)]
		public extern int left { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x060005AC RID: 1452
		// (set) Token: 0x060005AD RID: 1453
		[NativeProperty("right", false, TargetType.Field)]
		public extern int right { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x060005AE RID: 1454
		// (set) Token: 0x060005AF RID: 1455
		[NativeProperty("top", false, TargetType.Field)]
		public extern int top { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x060005B0 RID: 1456
		// (set) Token: 0x060005B1 RID: 1457
		[NativeProperty("bottom", false, TargetType.Field)]
		public extern int bottom { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x060005B2 RID: 1458
		public extern int horizontal { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x060005B3 RID: 1459
		public extern int vertical { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060005B4 RID: 1460 RVA: 0x0000D7F4 File Offset: 0x0000B9F4
		public Rect Add(Rect rect)
		{
			Rect result;
			this.Add_Injected(ref rect, out result);
			return result;
		}

		// Token: 0x060005B5 RID: 1461 RVA: 0x0000D80C File Offset: 0x0000BA0C
		public Rect Remove(Rect rect)
		{
			Rect result;
			this.Remove_Injected(ref rect, out result);
			return result;
		}

		// Token: 0x060005B6 RID: 1462 RVA: 0x0000D824 File Offset: 0x0000BA24
		~RectOffset()
		{
			if (this.m_SourceStyle == null)
			{
				this.Destroy();
			}
		}

		// Token: 0x060005B7 RID: 1463 RVA: 0x0000D860 File Offset: 0x0000BA60
		public override string ToString()
		{
			return UnityString.Format("RectOffset (l:{0} r:{1} t:{2} b:{3})", new object[]
			{
				this.left,
				this.right,
				this.top,
				this.bottom
			});
		}

		// Token: 0x060005B8 RID: 1464 RVA: 0x0000D8BD File Offset: 0x0000BABD
		private void Destroy()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				RectOffset.InternalDestroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x060005B9 RID: 1465
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Add_Injected(ref Rect rect, out Rect ret);

		// Token: 0x060005BA RID: 1466
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Remove_Injected(ref Rect rect, out Rect ret);

		// Token: 0x0400014F RID: 335
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.IMGUIModule"
		})]
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x04000150 RID: 336
		private readonly object m_SourceStyle;
	}
}
