﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000232 RID: 562
	[NativeHeader("Runtime/Input/TimeManager.h")]
	[StaticAccessor("GetTimeManager()", StaticAccessorType.Dot)]
	public class Time
	{
		// Token: 0x06001455 RID: 5205 RVA: 0x00002370 File Offset: 0x00000570
		public Time()
		{
		}

		// Token: 0x170003F2 RID: 1010
		// (get) Token: 0x06001456 RID: 5206
		[NativeProperty("CurTime")]
		public static extern float time { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003F3 RID: 1011
		// (get) Token: 0x06001457 RID: 5207
		[NativeProperty("TimeSinceSceneLoad")]
		public static extern float timeSinceLevelLoad { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003F4 RID: 1012
		// (get) Token: 0x06001458 RID: 5208
		public static extern float deltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x06001459 RID: 5209
		public static extern float fixedTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x0600145A RID: 5210
		public static extern float unscaledTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003F7 RID: 1015
		// (get) Token: 0x0600145B RID: 5211
		public static extern float fixedUnscaledTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003F8 RID: 1016
		// (get) Token: 0x0600145C RID: 5212
		public static extern float unscaledDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x0600145D RID: 5213
		public static extern float fixedUnscaledDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003FA RID: 1018
		// (get) Token: 0x0600145E RID: 5214
		// (set) Token: 0x0600145F RID: 5215
		public static extern float fixedDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x06001460 RID: 5216
		// (set) Token: 0x06001461 RID: 5217
		public static extern float maximumDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003FC RID: 1020
		// (get) Token: 0x06001462 RID: 5218
		public static extern float smoothDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x06001463 RID: 5219
		// (set) Token: 0x06001464 RID: 5220
		public static extern float maximumParticleDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003FE RID: 1022
		// (get) Token: 0x06001465 RID: 5221
		// (set) Token: 0x06001466 RID: 5222
		public static extern float timeScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003FF RID: 1023
		// (get) Token: 0x06001467 RID: 5223
		public static extern int frameCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000400 RID: 1024
		// (get) Token: 0x06001468 RID: 5224
		[NativeProperty("RenderFrameCount")]
		public static extern int renderedFrameCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000401 RID: 1025
		// (get) Token: 0x06001469 RID: 5225
		[NativeProperty("Realtime")]
		public static extern float realtimeSinceStartup { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000402 RID: 1026
		// (get) Token: 0x0600146A RID: 5226
		// (set) Token: 0x0600146B RID: 5227
		public static extern int captureFramerate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000403 RID: 1027
		// (get) Token: 0x0600146C RID: 5228
		public static extern bool inFixedTimeStep { [NativeName("IsUsingFixedTimeStep")] [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
