﻿using System;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000069 RID: 105
	[UsedByNativeCode]
	internal struct DiagnosticSwitch
	{
		// Token: 0x0600056D RID: 1389 RVA: 0x0000D1E4 File Offset: 0x0000B3E4
		[UsedByNativeCode]
		private static void AppendDiagnosticSwitchToList(List<DiagnosticSwitch> list, string name, string description, DiagnosticSwitchFlags flags, object value, object minValue, object maxValue, object persistentValue, EnumInfo enumInfo)
		{
			list.Add(new DiagnosticSwitch
			{
				name = name,
				description = description,
				flags = flags,
				value = value,
				minValue = minValue,
				maxValue = maxValue,
				persistentValue = persistentValue,
				enumInfo = enumInfo
			});
		}

		// Token: 0x04000137 RID: 311
		public string name;

		// Token: 0x04000138 RID: 312
		public string description;

		// Token: 0x04000139 RID: 313
		public DiagnosticSwitchFlags flags;

		// Token: 0x0400013A RID: 314
		public object value;

		// Token: 0x0400013B RID: 315
		public object minValue;

		// Token: 0x0400013C RID: 316
		public object maxValue;

		// Token: 0x0400013D RID: 317
		public object persistentValue;

		// Token: 0x0400013E RID: 318
		public EnumInfo enumInfo;
	}
}
