﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Diagnostics
{
	// Token: 0x02000281 RID: 641
	[NativeHeader("Runtime/Export/Diagnostics/DiagnosticsUtils.bindings.h")]
	public static class Utils
	{
		// Token: 0x060016CC RID: 5836
		[FreeFunction("DiagnosticsUtils_Bindings::ForceCrash", ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ForceCrash(ForcedCrashCategory crashCategory);

		// Token: 0x060016CD RID: 5837
		[FreeFunction("DiagnosticsUtils_Bindings::NativeAssert")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void NativeAssert(string message);

		// Token: 0x060016CE RID: 5838
		[FreeFunction("DiagnosticsUtils_Bindings::NativeError")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void NativeError(string message);

		// Token: 0x060016CF RID: 5839
		[FreeFunction("DiagnosticsUtils_Bindings::NativeWarning")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void NativeWarning(string message);
	}
}
