﻿using System;
using System.ComponentModel;
using UnityEngine.Networking.PlayerConnection;

namespace UnityEngine.Diagnostics
{
	// Token: 0x0200015F RID: 351
	public static class PlayerConnection
	{
		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x06000F36 RID: 3894 RVA: 0x0001A660 File Offset: 0x00018860
		[Obsolete("Use UnityEngine.Networking.PlayerConnection.PlayerConnection.instance.isConnected instead.")]
		public static bool connected
		{
			get
			{
				return PlayerConnection.instance.isConnected;
			}
		}

		// Token: 0x06000F37 RID: 3895 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("PlayerConnection.SendFile is no longer supported.", true)]
		public static void SendFile(string remoteFilePath, byte[] data)
		{
		}
	}
}
