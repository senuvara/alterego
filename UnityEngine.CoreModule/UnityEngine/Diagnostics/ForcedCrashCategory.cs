﻿using System;

namespace UnityEngine.Diagnostics
{
	// Token: 0x02000280 RID: 640
	public enum ForcedCrashCategory
	{
		// Token: 0x0400084E RID: 2126
		AccessViolation,
		// Token: 0x0400084F RID: 2127
		FatalError,
		// Token: 0x04000850 RID: 2128
		Abort,
		// Token: 0x04000851 RID: 2129
		PureVirtualFunction
	}
}
