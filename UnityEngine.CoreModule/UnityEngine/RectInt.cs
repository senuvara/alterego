﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001FA RID: 506
	[UsedByNativeCode]
	public struct RectInt : IEquatable<RectInt>
	{
		// Token: 0x06000FFF RID: 4095 RVA: 0x0001BF9E File Offset: 0x0001A19E
		public RectInt(int xMin, int yMin, int width, int height)
		{
			this.m_XMin = xMin;
			this.m_YMin = yMin;
			this.m_Width = width;
			this.m_Height = height;
		}

		// Token: 0x06001000 RID: 4096 RVA: 0x0001BFBE File Offset: 0x0001A1BE
		public RectInt(Vector2Int position, Vector2Int size)
		{
			this.m_XMin = position.x;
			this.m_YMin = position.y;
			this.m_Width = size.x;
			this.m_Height = size.y;
		}

		// Token: 0x1700030C RID: 780
		// (get) Token: 0x06001001 RID: 4097 RVA: 0x0001BFF8 File Offset: 0x0001A1F8
		// (set) Token: 0x06001002 RID: 4098 RVA: 0x0001C013 File Offset: 0x0001A213
		public int x
		{
			get
			{
				return this.m_XMin;
			}
			set
			{
				this.m_XMin = value;
			}
		}

		// Token: 0x1700030D RID: 781
		// (get) Token: 0x06001003 RID: 4099 RVA: 0x0001C020 File Offset: 0x0001A220
		// (set) Token: 0x06001004 RID: 4100 RVA: 0x0001C03B File Offset: 0x0001A23B
		public int y
		{
			get
			{
				return this.m_YMin;
			}
			set
			{
				this.m_YMin = value;
			}
		}

		// Token: 0x1700030E RID: 782
		// (get) Token: 0x06001005 RID: 4101 RVA: 0x0001C048 File Offset: 0x0001A248
		public Vector2 center
		{
			get
			{
				return new Vector2((float)this.x + (float)this.m_Width / 2f, (float)this.y + (float)this.m_Height / 2f);
			}
		}

		// Token: 0x1700030F RID: 783
		// (get) Token: 0x06001006 RID: 4102 RVA: 0x0001C08C File Offset: 0x0001A28C
		// (set) Token: 0x06001007 RID: 4103 RVA: 0x0001C0B2 File Offset: 0x0001A2B2
		public Vector2Int min
		{
			get
			{
				return new Vector2Int(this.xMin, this.yMin);
			}
			set
			{
				this.xMin = value.x;
				this.yMin = value.y;
			}
		}

		// Token: 0x17000310 RID: 784
		// (get) Token: 0x06001008 RID: 4104 RVA: 0x0001C0D0 File Offset: 0x0001A2D0
		// (set) Token: 0x06001009 RID: 4105 RVA: 0x0001C0F6 File Offset: 0x0001A2F6
		public Vector2Int max
		{
			get
			{
				return new Vector2Int(this.xMax, this.yMax);
			}
			set
			{
				this.xMax = value.x;
				this.yMax = value.y;
			}
		}

		// Token: 0x17000311 RID: 785
		// (get) Token: 0x0600100A RID: 4106 RVA: 0x0001C114 File Offset: 0x0001A314
		// (set) Token: 0x0600100B RID: 4107 RVA: 0x0001C12F File Offset: 0x0001A32F
		public int width
		{
			get
			{
				return this.m_Width;
			}
			set
			{
				this.m_Width = value;
			}
		}

		// Token: 0x17000312 RID: 786
		// (get) Token: 0x0600100C RID: 4108 RVA: 0x0001C13C File Offset: 0x0001A33C
		// (set) Token: 0x0600100D RID: 4109 RVA: 0x0001C157 File Offset: 0x0001A357
		public int height
		{
			get
			{
				return this.m_Height;
			}
			set
			{
				this.m_Height = value;
			}
		}

		// Token: 0x17000313 RID: 787
		// (get) Token: 0x0600100E RID: 4110 RVA: 0x0001C164 File Offset: 0x0001A364
		// (set) Token: 0x0600100F RID: 4111 RVA: 0x0001C194 File Offset: 0x0001A394
		public int xMin
		{
			get
			{
				return Math.Min(this.m_XMin, this.m_XMin + this.m_Width);
			}
			set
			{
				int xMax = this.xMax;
				this.m_XMin = value;
				this.m_Width = xMax - this.m_XMin;
			}
		}

		// Token: 0x17000314 RID: 788
		// (get) Token: 0x06001010 RID: 4112 RVA: 0x0001C1C0 File Offset: 0x0001A3C0
		// (set) Token: 0x06001011 RID: 4113 RVA: 0x0001C1F0 File Offset: 0x0001A3F0
		public int yMin
		{
			get
			{
				return Math.Min(this.m_YMin, this.m_YMin + this.m_Height);
			}
			set
			{
				int yMax = this.yMax;
				this.m_YMin = value;
				this.m_Height = yMax - this.m_YMin;
			}
		}

		// Token: 0x17000315 RID: 789
		// (get) Token: 0x06001012 RID: 4114 RVA: 0x0001C21C File Offset: 0x0001A41C
		// (set) Token: 0x06001013 RID: 4115 RVA: 0x0001C249 File Offset: 0x0001A449
		public int xMax
		{
			get
			{
				return Math.Max(this.m_XMin, this.m_XMin + this.m_Width);
			}
			set
			{
				this.m_Width = value - this.m_XMin;
			}
		}

		// Token: 0x17000316 RID: 790
		// (get) Token: 0x06001014 RID: 4116 RVA: 0x0001C25C File Offset: 0x0001A45C
		// (set) Token: 0x06001015 RID: 4117 RVA: 0x0001C289 File Offset: 0x0001A489
		public int yMax
		{
			get
			{
				return Math.Max(this.m_YMin, this.m_YMin + this.m_Height);
			}
			set
			{
				this.m_Height = value - this.m_YMin;
			}
		}

		// Token: 0x17000317 RID: 791
		// (get) Token: 0x06001016 RID: 4118 RVA: 0x0001C29C File Offset: 0x0001A49C
		// (set) Token: 0x06001017 RID: 4119 RVA: 0x0001C2C2 File Offset: 0x0001A4C2
		public Vector2Int position
		{
			get
			{
				return new Vector2Int(this.m_XMin, this.m_YMin);
			}
			set
			{
				this.m_XMin = value.x;
				this.m_YMin = value.y;
			}
		}

		// Token: 0x17000318 RID: 792
		// (get) Token: 0x06001018 RID: 4120 RVA: 0x0001C2E0 File Offset: 0x0001A4E0
		// (set) Token: 0x06001019 RID: 4121 RVA: 0x0001C306 File Offset: 0x0001A506
		public Vector2Int size
		{
			get
			{
				return new Vector2Int(this.m_Width, this.m_Height);
			}
			set
			{
				this.m_Width = value.x;
				this.m_Height = value.y;
			}
		}

		// Token: 0x0600101A RID: 4122 RVA: 0x0001C323 File Offset: 0x0001A523
		public void SetMinMax(Vector2Int minPosition, Vector2Int maxPosition)
		{
			this.min = minPosition;
			this.max = maxPosition;
		}

		// Token: 0x0600101B RID: 4123 RVA: 0x0001C334 File Offset: 0x0001A534
		public void ClampToBounds(RectInt bounds)
		{
			this.position = new Vector2Int(Math.Max(Math.Min(bounds.xMax, this.position.x), bounds.xMin), Math.Max(Math.Min(bounds.yMax, this.position.y), bounds.yMin));
			this.size = new Vector2Int(Math.Min(bounds.xMax - this.position.x, this.size.x), Math.Min(bounds.yMax - this.position.y, this.size.y));
		}

		// Token: 0x0600101C RID: 4124 RVA: 0x0001C3F8 File Offset: 0x0001A5F8
		public bool Contains(Vector2Int position)
		{
			return position.x >= this.xMin && position.y >= this.yMin && position.x < this.xMax && position.y < this.yMax;
		}

		// Token: 0x0600101D RID: 4125 RVA: 0x0001C458 File Offset: 0x0001A658
		public override string ToString()
		{
			return UnityString.Format("(x:{0}, y:{1}, width:{2}, height:{3})", new object[]
			{
				this.x,
				this.y,
				this.width,
				this.height
			});
		}

		// Token: 0x0600101E RID: 4126 RVA: 0x0001C4B8 File Offset: 0x0001A6B8
		public bool Equals(RectInt other)
		{
			return this.m_XMin == other.m_XMin && this.m_YMin == other.m_YMin && this.m_Width == other.m_Width && this.m_Height == other.m_Height;
		}

		// Token: 0x17000319 RID: 793
		// (get) Token: 0x0600101F RID: 4127 RVA: 0x0001C518 File Offset: 0x0001A718
		public RectInt.PositionEnumerator allPositionsWithin
		{
			get
			{
				return new RectInt.PositionEnumerator(this.min, this.max);
			}
		}

		// Token: 0x0400073E RID: 1854
		private int m_XMin;

		// Token: 0x0400073F RID: 1855
		private int m_YMin;

		// Token: 0x04000740 RID: 1856
		private int m_Width;

		// Token: 0x04000741 RID: 1857
		private int m_Height;

		// Token: 0x020001FB RID: 507
		public struct PositionEnumerator : IEnumerator<Vector2Int>, IEnumerator, IDisposable
		{
			// Token: 0x06001020 RID: 4128 RVA: 0x0001C540 File Offset: 0x0001A740
			public PositionEnumerator(Vector2Int min, Vector2Int max)
			{
				this._current = min;
				this._min = min;
				this._max = max;
				this.Reset();
			}

			// Token: 0x06001021 RID: 4129 RVA: 0x0001C56C File Offset: 0x0001A76C
			public RectInt.PositionEnumerator GetEnumerator()
			{
				return this;
			}

			// Token: 0x06001022 RID: 4130 RVA: 0x0001C588 File Offset: 0x0001A788
			public bool MoveNext()
			{
				bool result;
				if (this._current.y >= this._max.y)
				{
					result = false;
				}
				else
				{
					this._current.x = this._current.x + 1;
					if (this._current.x >= this._max.x)
					{
						this._current.x = this._min.x;
						this._current.y = this._current.y + 1;
						if (this._current.y >= this._max.y)
						{
							return false;
						}
					}
					result = true;
				}
				return result;
			}

			// Token: 0x06001023 RID: 4131 RVA: 0x0001C649 File Offset: 0x0001A849
			public void Reset()
			{
				this._current = this._min;
				this._current.x = this._current.x - 1;
			}

			// Token: 0x1700031B RID: 795
			// (get) Token: 0x06001024 RID: 4132 RVA: 0x0001C66C File Offset: 0x0001A86C
			public Vector2Int Current
			{
				get
				{
					return this._current;
				}
			}

			// Token: 0x1700031A RID: 794
			// (get) Token: 0x06001025 RID: 4133 RVA: 0x0001C688 File Offset: 0x0001A888
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x06001026 RID: 4134 RVA: 0x00007476 File Offset: 0x00005676
			void IDisposable.Dispose()
			{
			}

			// Token: 0x04000742 RID: 1858
			private readonly Vector2Int _min;

			// Token: 0x04000743 RID: 1859
			private readonly Vector2Int _max;

			// Token: 0x04000744 RID: 1860
			private Vector2Int _current;
		}
	}
}
