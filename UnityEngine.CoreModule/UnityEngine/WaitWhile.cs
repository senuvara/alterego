﻿using System;

namespace UnityEngine
{
	// Token: 0x0200025E RID: 606
	public sealed class WaitWhile : CustomYieldInstruction
	{
		// Token: 0x0600162D RID: 5677 RVA: 0x00027ACB File Offset: 0x00025CCB
		public WaitWhile(Func<bool> predicate)
		{
			this.m_Predicate = predicate;
		}

		// Token: 0x17000443 RID: 1091
		// (get) Token: 0x0600162E RID: 5678 RVA: 0x00027ADC File Offset: 0x00025CDC
		public override bool keepWaiting
		{
			get
			{
				return this.m_Predicate();
			}
		}

		// Token: 0x0400083A RID: 2106
		private Func<bool> m_Predicate;
	}
}
