﻿using System;

namespace UnityEngine
{
	// Token: 0x02000118 RID: 280
	public enum FullScreenMovieScalingMode
	{
		// Token: 0x04000500 RID: 1280
		None,
		// Token: 0x04000501 RID: 1281
		AspectFit,
		// Token: 0x04000502 RID: 1282
		AspectFill,
		// Token: 0x04000503 RID: 1283
		Fill
	}
}
