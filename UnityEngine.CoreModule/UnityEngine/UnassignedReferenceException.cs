﻿using System;
using System.Runtime.Serialization;

namespace UnityEngine
{
	// Token: 0x0200021D RID: 541
	[Serializable]
	public class UnassignedReferenceException : SystemException
	{
		// Token: 0x0600122D RID: 4653 RVA: 0x000208EF File Offset: 0x0001EAEF
		public UnassignedReferenceException() : base("A Unity Runtime error occurred!")
		{
			base.HResult = -2147467261;
		}

		// Token: 0x0600122E RID: 4654 RVA: 0x00020908 File Offset: 0x0001EB08
		public UnassignedReferenceException(string message) : base(message)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x0600122F RID: 4655 RVA: 0x0002091D File Offset: 0x0001EB1D
		public UnassignedReferenceException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06001230 RID: 4656 RVA: 0x00020933 File Offset: 0x0001EB33
		protected UnassignedReferenceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000791 RID: 1937
		private const int Result = -2147467261;

		// Token: 0x04000792 RID: 1938
		private string unityStackTrace;
	}
}
