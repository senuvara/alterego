﻿using System;

namespace UnityEngine
{
	// Token: 0x02000038 RID: 56
	public enum RuntimePlatform
	{
		// Token: 0x0400008B RID: 139
		OSXEditor,
		// Token: 0x0400008C RID: 140
		OSXPlayer,
		// Token: 0x0400008D RID: 141
		WindowsPlayer,
		// Token: 0x0400008E RID: 142
		[Obsolete("WebPlayer export is no longer supported in Unity 5.4+.", true)]
		OSXWebPlayer,
		// Token: 0x0400008F RID: 143
		[Obsolete("Dashboard widget on Mac OS X export is no longer supported in Unity 5.4+.", true)]
		OSXDashboardPlayer,
		// Token: 0x04000090 RID: 144
		[Obsolete("WebPlayer export is no longer supported in Unity 5.4+.", true)]
		WindowsWebPlayer,
		// Token: 0x04000091 RID: 145
		WindowsEditor = 7,
		// Token: 0x04000092 RID: 146
		IPhonePlayer,
		// Token: 0x04000093 RID: 147
		[Obsolete("Xbox360 export is no longer supported in Unity 5.5+.")]
		XBOX360 = 10,
		// Token: 0x04000094 RID: 148
		[Obsolete("PS3 export is no longer supported in Unity >=5.5.")]
		PS3 = 9,
		// Token: 0x04000095 RID: 149
		Android = 11,
		// Token: 0x04000096 RID: 150
		[Obsolete("NaCl export is no longer supported in Unity 5.0+.")]
		NaCl,
		// Token: 0x04000097 RID: 151
		[Obsolete("FlashPlayer export is no longer supported in Unity 5.0+.")]
		FlashPlayer = 15,
		// Token: 0x04000098 RID: 152
		LinuxPlayer = 13,
		// Token: 0x04000099 RID: 153
		LinuxEditor = 16,
		// Token: 0x0400009A RID: 154
		WebGLPlayer,
		// Token: 0x0400009B RID: 155
		[Obsolete("Use WSAPlayerX86 instead")]
		MetroPlayerX86,
		// Token: 0x0400009C RID: 156
		WSAPlayerX86 = 18,
		// Token: 0x0400009D RID: 157
		[Obsolete("Use WSAPlayerX64 instead")]
		MetroPlayerX64,
		// Token: 0x0400009E RID: 158
		WSAPlayerX64 = 19,
		// Token: 0x0400009F RID: 159
		[Obsolete("Use WSAPlayerARM instead")]
		MetroPlayerARM,
		// Token: 0x040000A0 RID: 160
		WSAPlayerARM = 20,
		// Token: 0x040000A1 RID: 161
		[Obsolete("Windows Phone 8 was removed in 5.3")]
		WP8Player,
		// Token: 0x040000A2 RID: 162
		[Obsolete("BlackBerryPlayer export is no longer supported in Unity 5.4+.")]
		BlackBerryPlayer,
		// Token: 0x040000A3 RID: 163
		[Obsolete("TizenPlayer export is no longer supported in Unity 2017.3+.")]
		TizenPlayer,
		// Token: 0x040000A4 RID: 164
		[Obsolete("PSP2 is no longer supported as of Unity 2018.3")]
		PSP2,
		// Token: 0x040000A5 RID: 165
		PS4,
		// Token: 0x040000A6 RID: 166
		[Obsolete("PSM export is no longer supported in Unity >= 5.3")]
		PSM,
		// Token: 0x040000A7 RID: 167
		XboxOne,
		// Token: 0x040000A8 RID: 168
		[Obsolete("SamsungTVPlayer export is no longer supported in Unity 2017.3+.")]
		SamsungTVPlayer,
		// Token: 0x040000A9 RID: 169
		[Obsolete("Wii U is no longer supported in Unity 2018.1+.")]
		WiiU = 30,
		// Token: 0x040000AA RID: 170
		tvOS,
		// Token: 0x040000AB RID: 171
		Switch,
		// Token: 0x040000AC RID: 172
		Lumin
	}
}
