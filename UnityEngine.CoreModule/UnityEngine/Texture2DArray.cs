﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200022B RID: 555
	[NativeHeader("Runtime/Graphics/Texture2DArray.h")]
	public sealed class Texture2DArray : Texture
	{
		// Token: 0x06001375 RID: 4981 RVA: 0x00021D26 File Offset: 0x0001FF26
		[RequiredByNativeCode]
		public Texture2DArray(int width, int height, int depth, GraphicsFormat format, TextureCreationFlags flags)
		{
			if (base.ValidateFormat(format, FormatUsage.Sample))
			{
				Texture2DArray.Internal_Create(this, width, height, depth, format, flags);
			}
		}

		// Token: 0x06001376 RID: 4982 RVA: 0x00021D4C File Offset: 0x0001FF4C
		public Texture2DArray(int width, int height, int depth, TextureFormat textureFormat, bool mipChain, [DefaultValue("false")] bool linear)
		{
			if (base.ValidateFormat(textureFormat))
			{
				GraphicsFormat graphicsFormat = GraphicsFormatUtility.GetGraphicsFormat(textureFormat, !linear);
				TextureCreationFlags textureCreationFlags = TextureCreationFlags.None;
				if (mipChain)
				{
					textureCreationFlags |= TextureCreationFlags.MipChain;
				}
				if (GraphicsFormatUtility.IsCrunchFormat(textureFormat))
				{
					textureCreationFlags |= TextureCreationFlags.Crunch;
				}
				Texture2DArray.Internal_Create(this, width, height, depth, graphicsFormat, textureCreationFlags);
			}
		}

		// Token: 0x06001377 RID: 4983 RVA: 0x00021DA8 File Offset: 0x0001FFA8
		public Texture2DArray(int width, int height, int depth, TextureFormat textureFormat, bool mipChain) : this(width, height, depth, textureFormat, mipChain, false)
		{
		}

		// Token: 0x170003B4 RID: 948
		// (get) Token: 0x06001378 RID: 4984
		public extern int depth { [NativeName("GetTextureLayerCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003B5 RID: 949
		// (get) Token: 0x06001379 RID: 4985
		public extern TextureFormat format { [NativeName("GetTextureFormat")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003B6 RID: 950
		// (get) Token: 0x0600137A RID: 4986
		public override extern bool isReadable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600137B RID: 4987
		[FreeFunction("Texture2DArrayScripting::Create")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_CreateImpl([Writable] Texture2DArray mono, int w, int h, int d, GraphicsFormat format, TextureCreationFlags flags);

		// Token: 0x0600137C RID: 4988 RVA: 0x00021DB9 File Offset: 0x0001FFB9
		private static void Internal_Create([Writable] Texture2DArray mono, int w, int h, int d, GraphicsFormat format, TextureCreationFlags flags)
		{
			if (!Texture2DArray.Internal_CreateImpl(mono, w, h, d, format, flags))
			{
				throw new UnityException("Failed to create 2D array texture because of invalid parameters.");
			}
		}

		// Token: 0x0600137D RID: 4989
		[FreeFunction(Name = "Texture2DArrayScripting::Apply", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ApplyImpl(bool updateMipmaps, bool makeNoLongerReadable);

		// Token: 0x0600137E RID: 4990
		[FreeFunction(Name = "Texture2DArrayScripting::GetPixels", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color[] GetPixels(int arrayElement, int miplevel);

		// Token: 0x0600137F RID: 4991 RVA: 0x00021DDC File Offset: 0x0001FFDC
		public Color[] GetPixels(int arrayElement)
		{
			return this.GetPixels(arrayElement, 0);
		}

		// Token: 0x06001380 RID: 4992
		[FreeFunction(Name = "Texture2DArrayScripting::GetPixels32", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color32[] GetPixels32(int arrayElement, int miplevel);

		// Token: 0x06001381 RID: 4993 RVA: 0x00021DFC File Offset: 0x0001FFFC
		public Color32[] GetPixels32(int arrayElement)
		{
			return this.GetPixels32(arrayElement, 0);
		}

		// Token: 0x06001382 RID: 4994
		[FreeFunction(Name = "Texture2DArrayScripting::SetPixels", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels(Color[] colors, int arrayElement, int miplevel);

		// Token: 0x06001383 RID: 4995 RVA: 0x00021E19 File Offset: 0x00020019
		public void SetPixels(Color[] colors, int arrayElement)
		{
			this.SetPixels(colors, arrayElement, 0);
		}

		// Token: 0x06001384 RID: 4996
		[FreeFunction(Name = "Texture2DArrayScripting::SetPixels32", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels32(Color32[] colors, int arrayElement, int miplevel);

		// Token: 0x06001385 RID: 4997 RVA: 0x00021E25 File Offset: 0x00020025
		public void SetPixels32(Color32[] colors, int arrayElement)
		{
			this.SetPixels32(colors, arrayElement, 0);
		}

		// Token: 0x06001386 RID: 4998 RVA: 0x00021E31 File Offset: 0x00020031
		public void Apply([DefaultValue("true")] bool updateMipmaps, [DefaultValue("false")] bool makeNoLongerReadable)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			this.ApplyImpl(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x06001387 RID: 4999 RVA: 0x00021E4F File Offset: 0x0002004F
		public void Apply(bool updateMipmaps)
		{
			this.Apply(updateMipmaps, false);
		}

		// Token: 0x06001388 RID: 5000 RVA: 0x00021E5A File Offset: 0x0002005A
		public void Apply()
		{
			this.Apply(true, false);
		}
	}
}
