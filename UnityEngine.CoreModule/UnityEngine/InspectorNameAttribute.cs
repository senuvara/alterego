﻿using System;

namespace UnityEngine
{
	// Token: 0x020001E7 RID: 487
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	internal class InspectorNameAttribute : PropertyAttribute
	{
		// Token: 0x06000F7E RID: 3966 RVA: 0x0001ACD6 File Offset: 0x00018ED6
		public InspectorNameAttribute(string displayName)
		{
			this.displayName = displayName;
		}

		// Token: 0x0400071E RID: 1822
		public readonly string displayName;
	}
}
