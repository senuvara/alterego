﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x0200020E RID: 526
	public sealed class ShaderVariantCollection : Object
	{
		// Token: 0x060011AB RID: 4523 RVA: 0x0001DF86 File Offset: 0x0001C186
		public ShaderVariantCollection()
		{
			ShaderVariantCollection.Internal_Create(this);
		}

		// Token: 0x1700033C RID: 828
		// (get) Token: 0x060011AC RID: 4524
		public extern int shaderCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700033D RID: 829
		// (get) Token: 0x060011AD RID: 4525
		public extern int variantCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700033E RID: 830
		// (get) Token: 0x060011AE RID: 4526
		public extern bool isWarmedUp { [NativeName("IsWarmedUp")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060011AF RID: 4527
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddVariant(Shader shader, PassType passType, string[] keywords);

		// Token: 0x060011B0 RID: 4528
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool RemoveVariant(Shader shader, PassType passType, string[] keywords);

		// Token: 0x060011B1 RID: 4529
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool ContainsVariant(Shader shader, PassType passType, string[] keywords);

		// Token: 0x060011B2 RID: 4530
		[NativeName("ClearVariants")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Clear();

		// Token: 0x060011B3 RID: 4531
		[NativeName("WarmupShaders")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void WarmUp();

		// Token: 0x060011B4 RID: 4532
		[NativeName("CreateFromScript")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] ShaderVariantCollection svc);

		// Token: 0x060011B5 RID: 4533 RVA: 0x0001DF98 File Offset: 0x0001C198
		public bool Add(ShaderVariantCollection.ShaderVariant variant)
		{
			return this.AddVariant(variant.shader, variant.passType, variant.keywords);
		}

		// Token: 0x060011B6 RID: 4534 RVA: 0x0001DFC8 File Offset: 0x0001C1C8
		public bool Remove(ShaderVariantCollection.ShaderVariant variant)
		{
			return this.RemoveVariant(variant.shader, variant.passType, variant.keywords);
		}

		// Token: 0x060011B7 RID: 4535 RVA: 0x0001DFF8 File Offset: 0x0001C1F8
		public bool Contains(ShaderVariantCollection.ShaderVariant variant)
		{
			return this.ContainsVariant(variant.shader, variant.passType, variant.keywords);
		}

		// Token: 0x0200020F RID: 527
		public struct ShaderVariant
		{
			// Token: 0x060011B8 RID: 4536 RVA: 0x0001E028 File Offset: 0x0001C228
			public ShaderVariant(Shader shader, PassType passType, params string[] keywords)
			{
				this.shader = shader;
				this.passType = passType;
				this.keywords = keywords;
			}

			// Token: 0x060011B9 RID: 4537
			[FreeFunction]
			[NativeConditional("UNITY_EDITOR")]
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern string CheckShaderVariant(Shader shader, PassType passType, string[] keywords);

			// Token: 0x04000758 RID: 1880
			public Shader shader;

			// Token: 0x04000759 RID: 1881
			public PassType passType;

			// Token: 0x0400075A RID: 1882
			public string[] keywords;
		}
	}
}
