﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020000A8 RID: 168
	[NativeHeader("Runtime/Camera/Projector.h")]
	public sealed class Projector : Behaviour
	{
		// Token: 0x06000AB5 RID: 2741 RVA: 0x0000A52A File Offset: 0x0000872A
		public Projector()
		{
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x06000AB6 RID: 2742
		// (set) Token: 0x06000AB7 RID: 2743
		public extern float nearClipPlane { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x06000AB8 RID: 2744
		// (set) Token: 0x06000AB9 RID: 2745
		public extern float farClipPlane { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06000ABA RID: 2746
		// (set) Token: 0x06000ABB RID: 2747
		public extern float fieldOfView { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x06000ABC RID: 2748
		// (set) Token: 0x06000ABD RID: 2749
		public extern float aspectRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x06000ABE RID: 2750
		// (set) Token: 0x06000ABF RID: 2751
		public extern bool orthographic { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x06000AC0 RID: 2752
		// (set) Token: 0x06000AC1 RID: 2753
		public extern float orthographicSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001EB RID: 491
		// (get) Token: 0x06000AC2 RID: 2754
		// (set) Token: 0x06000AC3 RID: 2755
		public extern int ignoreLayers { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001EC RID: 492
		// (get) Token: 0x06000AC4 RID: 2756
		// (set) Token: 0x06000AC5 RID: 2757
		public extern Material material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
