﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;
using UnityEngineInternal;

namespace UnityEngine
{
	// Token: 0x02000058 RID: 88
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Export/Component.bindings.h")]
	[NativeClass("Unity::Component")]
	public class Component : Object
	{
		// Token: 0x060004A7 RID: 1191 RVA: 0x0000AD1B File Offset: 0x00008F1B
		public Component()
		{
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x060004A8 RID: 1192
		public extern Transform transform { [FreeFunction("GetTransform", HasExplicitThis = true, ThrowsException = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x060004A9 RID: 1193
		public extern GameObject gameObject { [FreeFunction("GetGameObject", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060004AA RID: 1194 RVA: 0x0000BF8C File Offset: 0x0000A18C
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
		public Component GetComponent(Type type)
		{
			return this.gameObject.GetComponent(type);
		}

		// Token: 0x060004AB RID: 1195
		[FreeFunction(HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetComponentFastPath(Type type, IntPtr oneFurtherThanResultValue);

		// Token: 0x060004AC RID: 1196 RVA: 0x0000BFB0 File Offset: 0x0000A1B0
		[SecuritySafeCritical]
		public unsafe T GetComponent<T>()
		{
			CastHelper<T> castHelper = default(CastHelper<T>);
			this.GetComponentFastPath(typeof(T), new IntPtr((void*)(&castHelper.onePointerFurtherThanT)));
			return castHelper.t;
		}

		// Token: 0x060004AD RID: 1197
		[FreeFunction(HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Component GetComponent(string type);

		// Token: 0x060004AE RID: 1198 RVA: 0x0000BFF0 File Offset: 0x0000A1F0
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
		public Component GetComponentInChildren(Type t, bool includeInactive)
		{
			return this.gameObject.GetComponentInChildren(t, includeInactive);
		}

		// Token: 0x060004AF RID: 1199 RVA: 0x0000C014 File Offset: 0x0000A214
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
		public Component GetComponentInChildren(Type t)
		{
			return this.GetComponentInChildren(t, false);
		}

		// Token: 0x060004B0 RID: 1200 RVA: 0x0000C034 File Offset: 0x0000A234
		public T GetComponentInChildren<T>([DefaultValue("false")] bool includeInactive)
		{
			return (T)((object)this.GetComponentInChildren(typeof(T), includeInactive));
		}

		// Token: 0x060004B1 RID: 1201 RVA: 0x0000C060 File Offset: 0x0000A260
		[ExcludeFromDocs]
		public T GetComponentInChildren<T>()
		{
			return (T)((object)this.GetComponentInChildren(typeof(T), false));
		}

		// Token: 0x060004B2 RID: 1202 RVA: 0x0000C08C File Offset: 0x0000A28C
		public Component[] GetComponentsInChildren(Type t, bool includeInactive)
		{
			return this.gameObject.GetComponentsInChildren(t, includeInactive);
		}

		// Token: 0x060004B3 RID: 1203 RVA: 0x0000C0B0 File Offset: 0x0000A2B0
		[ExcludeFromDocs]
		public Component[] GetComponentsInChildren(Type t)
		{
			return this.gameObject.GetComponentsInChildren(t, false);
		}

		// Token: 0x060004B4 RID: 1204 RVA: 0x0000C0D4 File Offset: 0x0000A2D4
		public T[] GetComponentsInChildren<T>(bool includeInactive)
		{
			return this.gameObject.GetComponentsInChildren<T>(includeInactive);
		}

		// Token: 0x060004B5 RID: 1205 RVA: 0x0000C0F5 File Offset: 0x0000A2F5
		public void GetComponentsInChildren<T>(bool includeInactive, List<T> result)
		{
			this.gameObject.GetComponentsInChildren<T>(includeInactive, result);
		}

		// Token: 0x060004B6 RID: 1206 RVA: 0x0000C108 File Offset: 0x0000A308
		public T[] GetComponentsInChildren<T>()
		{
			return this.GetComponentsInChildren<T>(false);
		}

		// Token: 0x060004B7 RID: 1207 RVA: 0x0000C124 File Offset: 0x0000A324
		public void GetComponentsInChildren<T>(List<T> results)
		{
			this.GetComponentsInChildren<T>(false, results);
		}

		// Token: 0x060004B8 RID: 1208 RVA: 0x0000C130 File Offset: 0x0000A330
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
		public Component GetComponentInParent(Type t)
		{
			return this.gameObject.GetComponentInParent(t);
		}

		// Token: 0x060004B9 RID: 1209 RVA: 0x0000C154 File Offset: 0x0000A354
		public T GetComponentInParent<T>()
		{
			return (T)((object)this.GetComponentInParent(typeof(T)));
		}

		// Token: 0x060004BA RID: 1210 RVA: 0x0000C180 File Offset: 0x0000A380
		public Component[] GetComponentsInParent(Type t, [DefaultValue("false")] bool includeInactive)
		{
			return this.gameObject.GetComponentsInParent(t, includeInactive);
		}

		// Token: 0x060004BB RID: 1211 RVA: 0x0000C1A4 File Offset: 0x0000A3A4
		[ExcludeFromDocs]
		public Component[] GetComponentsInParent(Type t)
		{
			return this.GetComponentsInParent(t, false);
		}

		// Token: 0x060004BC RID: 1212 RVA: 0x0000C1C4 File Offset: 0x0000A3C4
		public T[] GetComponentsInParent<T>(bool includeInactive)
		{
			return this.gameObject.GetComponentsInParent<T>(includeInactive);
		}

		// Token: 0x060004BD RID: 1213 RVA: 0x0000C1E5 File Offset: 0x0000A3E5
		public void GetComponentsInParent<T>(bool includeInactive, List<T> results)
		{
			this.gameObject.GetComponentsInParent<T>(includeInactive, results);
		}

		// Token: 0x060004BE RID: 1214 RVA: 0x0000C1F8 File Offset: 0x0000A3F8
		public T[] GetComponentsInParent<T>()
		{
			return this.GetComponentsInParent<T>(false);
		}

		// Token: 0x060004BF RID: 1215 RVA: 0x0000C214 File Offset: 0x0000A414
		public Component[] GetComponents(Type type)
		{
			return this.gameObject.GetComponents(type);
		}

		// Token: 0x060004C0 RID: 1216
		[FreeFunction(HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetComponentsForListInternal(Type searchType, object resultList);

		// Token: 0x060004C1 RID: 1217 RVA: 0x0000C235 File Offset: 0x0000A435
		public void GetComponents(Type type, List<Component> results)
		{
			this.GetComponentsForListInternal(type, results);
		}

		// Token: 0x060004C2 RID: 1218 RVA: 0x0000C240 File Offset: 0x0000A440
		public void GetComponents<T>(List<T> results)
		{
			this.GetComponentsForListInternal(typeof(T), results);
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x060004C3 RID: 1219 RVA: 0x0000C254 File Offset: 0x0000A454
		// (set) Token: 0x060004C4 RID: 1220 RVA: 0x0000C274 File Offset: 0x0000A474
		public string tag
		{
			get
			{
				return this.gameObject.tag;
			}
			set
			{
				this.gameObject.tag = value;
			}
		}

		// Token: 0x060004C5 RID: 1221 RVA: 0x0000C284 File Offset: 0x0000A484
		public T[] GetComponents<T>()
		{
			return this.gameObject.GetComponents<T>();
		}

		// Token: 0x060004C6 RID: 1222 RVA: 0x0000C2A4 File Offset: 0x0000A4A4
		public bool CompareTag(string tag)
		{
			return this.gameObject.CompareTag(tag);
		}

		// Token: 0x060004C7 RID: 1223
		[FreeFunction(HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SendMessageUpwards(string methodName, [DefaultValue("null")] object value, [DefaultValue("SendMessageOptions.RequireReceiver")] SendMessageOptions options);

		// Token: 0x060004C8 RID: 1224 RVA: 0x0000C2C5 File Offset: 0x0000A4C5
		[ExcludeFromDocs]
		public void SendMessageUpwards(string methodName, object value)
		{
			this.SendMessageUpwards(methodName, value, SendMessageOptions.RequireReceiver);
		}

		// Token: 0x060004C9 RID: 1225 RVA: 0x0000C2D1 File Offset: 0x0000A4D1
		[ExcludeFromDocs]
		public void SendMessageUpwards(string methodName)
		{
			this.SendMessageUpwards(methodName, null, SendMessageOptions.RequireReceiver);
		}

		// Token: 0x060004CA RID: 1226 RVA: 0x0000C2DD File Offset: 0x0000A4DD
		public void SendMessageUpwards(string methodName, SendMessageOptions options)
		{
			this.SendMessageUpwards(methodName, null, options);
		}

		// Token: 0x060004CB RID: 1227 RVA: 0x0000C2E9 File Offset: 0x0000A4E9
		public void SendMessage(string methodName, object value)
		{
			this.SendMessage(methodName, value, SendMessageOptions.RequireReceiver);
		}

		// Token: 0x060004CC RID: 1228 RVA: 0x0000C2F5 File Offset: 0x0000A4F5
		public void SendMessage(string methodName)
		{
			this.SendMessage(methodName, null, SendMessageOptions.RequireReceiver);
		}

		// Token: 0x060004CD RID: 1229
		[FreeFunction("SendMessage", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SendMessage(string methodName, object value, SendMessageOptions options);

		// Token: 0x060004CE RID: 1230 RVA: 0x0000C301 File Offset: 0x0000A501
		public void SendMessage(string methodName, SendMessageOptions options)
		{
			this.SendMessage(methodName, null, options);
		}

		// Token: 0x060004CF RID: 1231
		[FreeFunction("BroadcastMessage", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void BroadcastMessage(string methodName, [DefaultValue("null")] object parameter, [DefaultValue("SendMessageOptions.RequireReceiver")] SendMessageOptions options);

		// Token: 0x060004D0 RID: 1232 RVA: 0x0000C30D File Offset: 0x0000A50D
		[ExcludeFromDocs]
		public void BroadcastMessage(string methodName, object parameter)
		{
			this.BroadcastMessage(methodName, parameter, SendMessageOptions.RequireReceiver);
		}

		// Token: 0x060004D1 RID: 1233 RVA: 0x0000C319 File Offset: 0x0000A519
		[ExcludeFromDocs]
		public void BroadcastMessage(string methodName)
		{
			this.BroadcastMessage(methodName, null, SendMessageOptions.RequireReceiver);
		}

		// Token: 0x060004D2 RID: 1234 RVA: 0x0000C325 File Offset: 0x0000A525
		public void BroadcastMessage(string methodName, SendMessageOptions options)
		{
			this.BroadcastMessage(methodName, null, options);
		}
	}
}
