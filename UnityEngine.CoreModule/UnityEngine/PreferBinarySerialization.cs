﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200020B RID: 523
	[AttributeUsage(AttributeTargets.Class)]
	[RequiredByNativeCode]
	public sealed class PreferBinarySerialization : Attribute
	{
		// Token: 0x060011A8 RID: 4520 RVA: 0x0000898B File Offset: 0x00006B8B
		public PreferBinarySerialization()
		{
		}
	}
}
