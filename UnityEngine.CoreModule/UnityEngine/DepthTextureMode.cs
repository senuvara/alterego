﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C0 RID: 192
	[Flags]
	public enum DepthTextureMode
	{
		// Token: 0x04000204 RID: 516
		None = 0,
		// Token: 0x04000205 RID: 517
		Depth = 1,
		// Token: 0x04000206 RID: 518
		DepthNormals = 2,
		// Token: 0x04000207 RID: 519
		MotionVectors = 4
	}
}
