﻿using System;

namespace UnityEngine
{
	// Token: 0x02000123 RID: 291
	public enum TouchType
	{
		// Token: 0x04000522 RID: 1314
		Direct,
		// Token: 0x04000523 RID: 1315
		Indirect,
		// Token: 0x04000524 RID: 1316
		Stylus
	}
}
