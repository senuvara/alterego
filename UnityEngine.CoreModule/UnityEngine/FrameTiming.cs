﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200008A RID: 138
	[NativeHeader("Runtime/GfxDevice/FrameTiming.h")]
	public struct FrameTiming
	{
		// Token: 0x04000171 RID: 369
		[NativeName("m_CPUTimePresentCalled")]
		public ulong cpuTimePresentCalled;

		// Token: 0x04000172 RID: 370
		[NativeName("m_CPUFrameTime")]
		public double cpuFrameTime;

		// Token: 0x04000173 RID: 371
		[NativeName("m_CPUTimeFrameComplete")]
		public ulong cpuTimeFrameComplete;

		// Token: 0x04000174 RID: 372
		[NativeName("m_GPUFrameTime")]
		public double gpuFrameTime;

		// Token: 0x04000175 RID: 373
		[NativeName("m_HeightScale")]
		public float heightScale;

		// Token: 0x04000176 RID: 374
		[NativeName("m_WidthScale")]
		public float widthScale;

		// Token: 0x04000177 RID: 375
		[NativeName("m_SyncInterval")]
		public uint syncInterval;
	}
}
