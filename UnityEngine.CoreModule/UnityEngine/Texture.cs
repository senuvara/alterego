﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000226 RID: 550
	[NativeHeader("Runtime/Graphics/Texture.h")]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Streaming/TextureStreamingManager.h")]
	public class Texture : Object
	{
		// Token: 0x060012C4 RID: 4804 RVA: 0x0000D647 File Offset: 0x0000B847
		protected Texture()
		{
		}

		// Token: 0x17000387 RID: 903
		// (get) Token: 0x060012C5 RID: 4805
		// (set) Token: 0x060012C6 RID: 4806
		public static extern int masterTextureLimit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000388 RID: 904
		// (get) Token: 0x060012C7 RID: 4807
		// (set) Token: 0x060012C8 RID: 4808
		[NativeProperty("AnisoLimit")]
		public static extern AnisotropicFiltering anisotropicFiltering { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060012C9 RID: 4809
		[NativeName("SetGlobalAnisoLimits")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetGlobalAnisotropicFilteringLimits(int forcedMin, int globalMax);

		// Token: 0x060012CA RID: 4810
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetDataWidth();

		// Token: 0x060012CB RID: 4811
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetDataHeight();

		// Token: 0x060012CC RID: 4812
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern TextureDimension GetDimension();

		// Token: 0x17000389 RID: 905
		// (get) Token: 0x060012CD RID: 4813 RVA: 0x000211D4 File Offset: 0x0001F3D4
		// (set) Token: 0x060012CE RID: 4814 RVA: 0x000211EF File Offset: 0x0001F3EF
		public virtual int width
		{
			get
			{
				return this.GetDataWidth();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x1700038A RID: 906
		// (get) Token: 0x060012CF RID: 4815 RVA: 0x000211F8 File Offset: 0x0001F3F8
		// (set) Token: 0x060012D0 RID: 4816 RVA: 0x000211EF File Offset: 0x0001F3EF
		public virtual int height
		{
			get
			{
				return this.GetDataHeight();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x1700038B RID: 907
		// (get) Token: 0x060012D1 RID: 4817 RVA: 0x00021214 File Offset: 0x0001F414
		// (set) Token: 0x060012D2 RID: 4818 RVA: 0x000211EF File Offset: 0x0001F3EF
		public virtual TextureDimension dimension
		{
			get
			{
				return this.GetDimension();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x1700038C RID: 908
		// (get) Token: 0x060012D3 RID: 4819
		public virtual extern bool isReadable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700038D RID: 909
		// (get) Token: 0x060012D4 RID: 4820
		// (set) Token: 0x060012D5 RID: 4821
		public extern TextureWrapMode wrapMode { [NativeName("GetWrapModeU")] [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700038E RID: 910
		// (get) Token: 0x060012D6 RID: 4822
		// (set) Token: 0x060012D7 RID: 4823
		public extern TextureWrapMode wrapModeU { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700038F RID: 911
		// (get) Token: 0x060012D8 RID: 4824
		// (set) Token: 0x060012D9 RID: 4825
		public extern TextureWrapMode wrapModeV { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000390 RID: 912
		// (get) Token: 0x060012DA RID: 4826
		// (set) Token: 0x060012DB RID: 4827
		public extern TextureWrapMode wrapModeW { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000391 RID: 913
		// (get) Token: 0x060012DC RID: 4828
		// (set) Token: 0x060012DD RID: 4829
		public extern FilterMode filterMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000392 RID: 914
		// (get) Token: 0x060012DE RID: 4830
		// (set) Token: 0x060012DF RID: 4831
		public extern int anisoLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000393 RID: 915
		// (get) Token: 0x060012E0 RID: 4832
		// (set) Token: 0x060012E1 RID: 4833
		public extern float mipMapBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000394 RID: 916
		// (get) Token: 0x060012E2 RID: 4834 RVA: 0x00021230 File Offset: 0x0001F430
		public Vector2 texelSize
		{
			[NativeName("GetNpotTexelSize")]
			get
			{
				Vector2 result;
				this.get_texelSize_Injected(out result);
				return result;
			}
		}

		// Token: 0x060012E3 RID: 4835
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern IntPtr GetNativeTexturePtr();

		// Token: 0x060012E4 RID: 4836 RVA: 0x00021248 File Offset: 0x0001F448
		[Obsolete("Use GetNativeTexturePtr instead.", false)]
		public int GetNativeTextureID()
		{
			return (int)this.GetNativeTexturePtr();
		}

		// Token: 0x17000395 RID: 917
		// (get) Token: 0x060012E5 RID: 4837
		public extern uint updateCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060012E6 RID: 4838
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void IncrementUpdateCount();

		// Token: 0x17000396 RID: 918
		// (get) Token: 0x060012E7 RID: 4839
		public static extern ulong totalTextureMemory { [FreeFunction("GetTextureStreamingManager().GetTotalTextureMemory")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000397 RID: 919
		// (get) Token: 0x060012E8 RID: 4840
		public static extern ulong desiredTextureMemory { [FreeFunction("GetTextureStreamingManager().GetDesiredTextureMemory")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000398 RID: 920
		// (get) Token: 0x060012E9 RID: 4841
		public static extern ulong targetTextureMemory { [FreeFunction("GetTextureStreamingManager().GetTargetTextureMemory")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000399 RID: 921
		// (get) Token: 0x060012EA RID: 4842
		public static extern ulong currentTextureMemory { [FreeFunction("GetTextureStreamingManager().GetCurrentTextureMemory")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700039A RID: 922
		// (get) Token: 0x060012EB RID: 4843
		public static extern ulong nonStreamingTextureMemory { [FreeFunction("GetTextureStreamingManager().GetNonStreamingTextureMemory")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700039B RID: 923
		// (get) Token: 0x060012EC RID: 4844
		public static extern ulong streamingMipmapUploadCount { [FreeFunction("GetTextureStreamingManager().GetStreamingMipmapUploadCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700039C RID: 924
		// (get) Token: 0x060012ED RID: 4845
		public static extern ulong streamingRendererCount { [FreeFunction("GetTextureStreamingManager().GetStreamingRendererCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700039D RID: 925
		// (get) Token: 0x060012EE RID: 4846
		public static extern ulong streamingTextureCount { [FreeFunction("GetTextureStreamingManager().GetStreamingTextureCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700039E RID: 926
		// (get) Token: 0x060012EF RID: 4847
		public static extern ulong nonStreamingTextureCount { [FreeFunction("GetTextureStreamingManager().GetNonStreamingTextureCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700039F RID: 927
		// (get) Token: 0x060012F0 RID: 4848
		public static extern ulong streamingTexturePendingLoadCount { [FreeFunction("GetTextureStreamingManager().GetStreamingTexturePendingLoadCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003A0 RID: 928
		// (get) Token: 0x060012F1 RID: 4849
		public static extern ulong streamingTextureLoadingCount { [FreeFunction("GetTextureStreamingManager().GetStreamingTextureLoadingCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060012F2 RID: 4850
		[FreeFunction("GetTextureStreamingManager().SetStreamingTextureMaterialDebugProperties")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetStreamingTextureMaterialDebugProperties();

		// Token: 0x170003A1 RID: 929
		// (get) Token: 0x060012F3 RID: 4851
		// (set) Token: 0x060012F4 RID: 4852
		public static extern bool streamingTextureForceLoadAll { [FreeFunction(Name = "GetTextureStreamingManager().GetForceLoadAll")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction(Name = "GetTextureStreamingManager().SetForceLoadAll")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003A2 RID: 930
		// (get) Token: 0x060012F5 RID: 4853
		// (set) Token: 0x060012F6 RID: 4854
		public static extern bool streamingTextureDiscardUnusedMips { [FreeFunction(Name = "GetTextureStreamingManager().GetDiscardUnusedMips")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction(Name = "GetTextureStreamingManager().SetDiscardUnusedMips")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060012F7 RID: 4855 RVA: 0x00021268 File Offset: 0x0001F468
		internal bool ValidateFormat(RenderTextureFormat format)
		{
			bool result;
			if (SystemInfo.SupportsRenderTextureFormat(format))
			{
				result = true;
			}
			else
			{
				Debug.LogError(string.Format("RenderTexture creation failed. '{0}' is not supported on this platform. Use 'SystemInfo.SupportsRenderTextureFormat' C# API to check format support.", format.ToString()), this);
				result = false;
			}
			return result;
		}

		// Token: 0x060012F8 RID: 4856 RVA: 0x000212B0 File Offset: 0x0001F4B0
		internal bool ValidateFormat(TextureFormat format)
		{
			bool result;
			if (SystemInfo.SupportsTextureFormat(format))
			{
				result = true;
			}
			else if (GraphicsFormatUtility.IsCompressedTextureFormat(format))
			{
				Debug.LogWarning(string.Format("'{0}' is not supported on this platform. Decompressing texture. Use 'SystemInfo.SupportsTextureFormat' C# API to check format support.", format.ToString()), this);
				result = true;
			}
			else
			{
				Debug.LogError(string.Format("Texture creation failed. '{0}' is not supported on this platform. Use 'SystemInfo.SupportsTextureFormat' C# API to check format support.", format.ToString()), this);
				result = false;
			}
			return result;
		}

		// Token: 0x060012F9 RID: 4857 RVA: 0x00021328 File Offset: 0x0001F528
		internal bool ValidateFormat(GraphicsFormat format, FormatUsage usage)
		{
			bool result;
			if (SystemInfo.IsFormatSupported(format, usage))
			{
				result = true;
			}
			else
			{
				Debug.LogError(string.Format("Texture creation failed. '{0}' is not supported for {1} usage on this platform. Use 'SystemInfo.IsFormatSupported' C# API to check format support.", format.ToString(), usage.ToString()), this);
				result = false;
			}
			return result;
		}

		// Token: 0x060012FA RID: 4858 RVA: 0x00021380 File Offset: 0x0001F580
		internal UnityException CreateNonReadableException(Texture t)
		{
			return new UnityException(string.Format("Texture '{0}' is not readable, the texture memory can not be accessed from scripts. You can make the texture readable in the Texture Import Settings.", t.name));
		}

		// Token: 0x060012FB RID: 4859
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_texelSize_Injected(out Vector2 ret);
	}
}
