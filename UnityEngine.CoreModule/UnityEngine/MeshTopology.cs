﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C4 RID: 196
	public enum MeshTopology
	{
		// Token: 0x04000218 RID: 536
		Triangles,
		// Token: 0x04000219 RID: 537
		Quads = 2,
		// Token: 0x0400021A RID: 538
		Lines,
		// Token: 0x0400021B RID: 539
		LineStrip,
		// Token: 0x0400021C RID: 540
		Points
	}
}
