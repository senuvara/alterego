﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200025F RID: 607
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class YieldInstruction
	{
		// Token: 0x0600162F RID: 5679 RVA: 0x00002370 File Offset: 0x00000570
		public YieldInstruction()
		{
		}
	}
}
