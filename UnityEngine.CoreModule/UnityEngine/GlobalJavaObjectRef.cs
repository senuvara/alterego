﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000B RID: 11
	internal class GlobalJavaObjectRef
	{
		// Token: 0x06000144 RID: 324 RVA: 0x000054AE File Offset: 0x000036AE
		public GlobalJavaObjectRef(IntPtr jobject)
		{
			this.m_jobject = ((!(jobject == IntPtr.Zero)) ? AndroidJNI.NewGlobalRef(jobject) : IntPtr.Zero);
		}

		// Token: 0x06000145 RID: 325 RVA: 0x000054E4 File Offset: 0x000036E4
		~GlobalJavaObjectRef()
		{
			this.Dispose();
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00005514 File Offset: 0x00003714
		public static implicit operator IntPtr(GlobalJavaObjectRef obj)
		{
			return obj.m_jobject;
		}

		// Token: 0x06000147 RID: 327 RVA: 0x0000552F File Offset: 0x0000372F
		public void Dispose()
		{
			if (!this.m_disposed)
			{
				this.m_disposed = true;
				if (this.m_jobject != IntPtr.Zero)
				{
					AndroidJNISafe.DeleteGlobalRef(this.m_jobject);
				}
			}
		}

		// Token: 0x0400000F RID: 15
		private bool m_disposed = false;

		// Token: 0x04000010 RID: 16
		protected IntPtr m_jobject;
	}
}
