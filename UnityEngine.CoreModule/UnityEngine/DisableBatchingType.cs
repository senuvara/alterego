﻿using System;

namespace UnityEngine
{
	// Token: 0x0200020D RID: 525
	internal enum DisableBatchingType
	{
		// Token: 0x04000755 RID: 1877
		False,
		// Token: 0x04000756 RID: 1878
		True,
		// Token: 0x04000757 RID: 1879
		WhenLODFading
	}
}
