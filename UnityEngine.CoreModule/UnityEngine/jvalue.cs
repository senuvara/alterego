﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000003 RID: 3
	[NativeType(CodegenOptions.Custom, "ScriptingJvalue")]
	[StructLayout(LayoutKind.Explicit)]
	public struct jvalue
	{
		// Token: 0x04000001 RID: 1
		[FieldOffset(0)]
		public bool z;

		// Token: 0x04000002 RID: 2
		[FieldOffset(0)]
		public byte b;

		// Token: 0x04000003 RID: 3
		[FieldOffset(0)]
		public char c;

		// Token: 0x04000004 RID: 4
		[FieldOffset(0)]
		public short s;

		// Token: 0x04000005 RID: 5
		[FieldOffset(0)]
		public int i;

		// Token: 0x04000006 RID: 6
		[FieldOffset(0)]
		public long j;

		// Token: 0x04000007 RID: 7
		[FieldOffset(0)]
		public float f;

		// Token: 0x04000008 RID: 8
		[FieldOffset(0)]
		public double d;

		// Token: 0x04000009 RID: 9
		[FieldOffset(0)]
		public IntPtr l;
	}
}
