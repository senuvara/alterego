﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C5 RID: 197
	public enum SkinQuality
	{
		// Token: 0x0400021E RID: 542
		Auto,
		// Token: 0x0400021F RID: 543
		Bone1,
		// Token: 0x04000220 RID: 544
		Bone2,
		// Token: 0x04000221 RID: 545
		Bone4 = 4
	}
}
