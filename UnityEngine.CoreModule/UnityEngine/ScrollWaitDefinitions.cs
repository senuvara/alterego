﻿using System;

namespace UnityEngine
{
	// Token: 0x02000205 RID: 517
	internal static class ScrollWaitDefinitions
	{
		// Token: 0x04000752 RID: 1874
		public const int firstWait = 250;

		// Token: 0x04000753 RID: 1875
		public const int regularWait = 30;
	}
}
