﻿using System;

namespace UnityEngine
{
	// Token: 0x02000220 RID: 544
	public enum BatteryStatus
	{
		// Token: 0x04000797 RID: 1943
		Unknown,
		// Token: 0x04000798 RID: 1944
		Charging,
		// Token: 0x04000799 RID: 1945
		Discharging,
		// Token: 0x0400079A RID: 1946
		NotCharging,
		// Token: 0x0400079B RID: 1947
		Full
	}
}
