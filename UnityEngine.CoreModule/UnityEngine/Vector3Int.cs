﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000257 RID: 599
	[UsedByNativeCode]
	public struct Vector3Int : IEquatable<Vector3Int>
	{
		// Token: 0x060015CD RID: 5581 RVA: 0x00026569 File Offset: 0x00024769
		public Vector3Int(int x, int y, int z)
		{
			this.m_X = x;
			this.m_Y = y;
			this.m_Z = z;
		}

		// Token: 0x1700042C RID: 1068
		// (get) Token: 0x060015CE RID: 5582 RVA: 0x00026584 File Offset: 0x00024784
		// (set) Token: 0x060015CF RID: 5583 RVA: 0x0002659F File Offset: 0x0002479F
		public int x
		{
			get
			{
				return this.m_X;
			}
			set
			{
				this.m_X = value;
			}
		}

		// Token: 0x1700042D RID: 1069
		// (get) Token: 0x060015D0 RID: 5584 RVA: 0x000265AC File Offset: 0x000247AC
		// (set) Token: 0x060015D1 RID: 5585 RVA: 0x000265C7 File Offset: 0x000247C7
		public int y
		{
			get
			{
				return this.m_Y;
			}
			set
			{
				this.m_Y = value;
			}
		}

		// Token: 0x1700042E RID: 1070
		// (get) Token: 0x060015D2 RID: 5586 RVA: 0x000265D4 File Offset: 0x000247D4
		// (set) Token: 0x060015D3 RID: 5587 RVA: 0x000265EF File Offset: 0x000247EF
		public int z
		{
			get
			{
				return this.m_Z;
			}
			set
			{
				this.m_Z = value;
			}
		}

		// Token: 0x060015D4 RID: 5588 RVA: 0x00026569 File Offset: 0x00024769
		public void Set(int x, int y, int z)
		{
			this.m_X = x;
			this.m_Y = y;
			this.m_Z = z;
		}

		// Token: 0x1700042F RID: 1071
		public int this[int index]
		{
			get
			{
				int result;
				switch (index)
				{
				case 0:
					result = this.x;
					break;
				case 1:
					result = this.y;
					break;
				case 2:
					result = this.z;
					break;
				default:
					throw new IndexOutOfRangeException(UnityString.Format("Invalid Vector3Int index addressed: {0}!", new object[]
					{
						index
					}));
				}
				return result;
			}
			set
			{
				switch (index)
				{
				case 0:
					this.x = value;
					break;
				case 1:
					this.y = value;
					break;
				case 2:
					this.z = value;
					break;
				default:
					throw new IndexOutOfRangeException(UnityString.Format("Invalid Vector3Int index addressed: {0}!", new object[]
					{
						index
					}));
				}
			}
		}

		// Token: 0x17000430 RID: 1072
		// (get) Token: 0x060015D7 RID: 5591 RVA: 0x000266D0 File Offset: 0x000248D0
		public float magnitude
		{
			get
			{
				return Mathf.Sqrt((float)(this.x * this.x + this.y * this.y + this.z * this.z));
			}
		}

		// Token: 0x17000431 RID: 1073
		// (get) Token: 0x060015D8 RID: 5592 RVA: 0x00026714 File Offset: 0x00024914
		public int sqrMagnitude
		{
			get
			{
				return this.x * this.x + this.y * this.y + this.z * this.z;
			}
		}

		// Token: 0x060015D9 RID: 5593 RVA: 0x00026754 File Offset: 0x00024954
		public static float Distance(Vector3Int a, Vector3Int b)
		{
			return (a - b).magnitude;
		}

		// Token: 0x060015DA RID: 5594 RVA: 0x00026778 File Offset: 0x00024978
		public static Vector3Int Min(Vector3Int lhs, Vector3Int rhs)
		{
			return new Vector3Int(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y), Mathf.Min(lhs.z, rhs.z));
		}

		// Token: 0x060015DB RID: 5595 RVA: 0x000267CC File Offset: 0x000249CC
		public static Vector3Int Max(Vector3Int lhs, Vector3Int rhs)
		{
			return new Vector3Int(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y), Mathf.Max(lhs.z, rhs.z));
		}

		// Token: 0x060015DC RID: 5596 RVA: 0x00026820 File Offset: 0x00024A20
		public static Vector3Int Scale(Vector3Int a, Vector3Int b)
		{
			return new Vector3Int(a.x * b.x, a.y * b.y, a.z * b.z);
		}

		// Token: 0x060015DD RID: 5597 RVA: 0x00026867 File Offset: 0x00024A67
		public void Scale(Vector3Int scale)
		{
			this.x *= scale.x;
			this.y *= scale.y;
			this.z *= scale.z;
		}

		// Token: 0x060015DE RID: 5598 RVA: 0x000268A8 File Offset: 0x00024AA8
		public void Clamp(Vector3Int min, Vector3Int max)
		{
			this.x = Math.Max(min.x, this.x);
			this.x = Math.Min(max.x, this.x);
			this.y = Math.Max(min.y, this.y);
			this.y = Math.Min(max.y, this.y);
			this.z = Math.Max(min.z, this.z);
			this.z = Math.Min(max.z, this.z);
		}

		// Token: 0x060015DF RID: 5599 RVA: 0x00026948 File Offset: 0x00024B48
		public static implicit operator Vector3(Vector3Int v)
		{
			return new Vector3((float)v.x, (float)v.y, (float)v.z);
		}

		// Token: 0x060015E0 RID: 5600 RVA: 0x0002697C File Offset: 0x00024B7C
		public static explicit operator Vector2Int(Vector3Int v)
		{
			return new Vector2Int(v.x, v.y);
		}

		// Token: 0x060015E1 RID: 5601 RVA: 0x000269A4 File Offset: 0x00024BA4
		public static Vector3Int FloorToInt(Vector3 v)
		{
			return new Vector3Int(Mathf.FloorToInt(v.x), Mathf.FloorToInt(v.y), Mathf.FloorToInt(v.z));
		}

		// Token: 0x060015E2 RID: 5602 RVA: 0x000269E4 File Offset: 0x00024BE4
		public static Vector3Int CeilToInt(Vector3 v)
		{
			return new Vector3Int(Mathf.CeilToInt(v.x), Mathf.CeilToInt(v.y), Mathf.CeilToInt(v.z));
		}

		// Token: 0x060015E3 RID: 5603 RVA: 0x00026A24 File Offset: 0x00024C24
		public static Vector3Int RoundToInt(Vector3 v)
		{
			return new Vector3Int(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y), Mathf.RoundToInt(v.z));
		}

		// Token: 0x060015E4 RID: 5604 RVA: 0x00026A64 File Offset: 0x00024C64
		public static Vector3Int operator +(Vector3Int a, Vector3Int b)
		{
			return new Vector3Int(a.x + b.x, a.y + b.y, a.z + b.z);
		}

		// Token: 0x060015E5 RID: 5605 RVA: 0x00026AAC File Offset: 0x00024CAC
		public static Vector3Int operator -(Vector3Int a, Vector3Int b)
		{
			return new Vector3Int(a.x - b.x, a.y - b.y, a.z - b.z);
		}

		// Token: 0x060015E6 RID: 5606 RVA: 0x00026AF4 File Offset: 0x00024CF4
		public static Vector3Int operator *(Vector3Int a, Vector3Int b)
		{
			return new Vector3Int(a.x * b.x, a.y * b.y, a.z * b.z);
		}

		// Token: 0x060015E7 RID: 5607 RVA: 0x00026B3C File Offset: 0x00024D3C
		public static Vector3Int operator *(Vector3Int a, int b)
		{
			return new Vector3Int(a.x * b, a.y * b, a.z * b);
		}

		// Token: 0x060015E8 RID: 5608 RVA: 0x00026B74 File Offset: 0x00024D74
		public static bool operator ==(Vector3Int lhs, Vector3Int rhs)
		{
			return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
		}

		// Token: 0x060015E9 RID: 5609 RVA: 0x00026BC4 File Offset: 0x00024DC4
		public static bool operator !=(Vector3Int lhs, Vector3Int rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x060015EA RID: 5610 RVA: 0x00026BE4 File Offset: 0x00024DE4
		public override bool Equals(object other)
		{
			return other is Vector3Int && this.Equals((Vector3Int)other);
		}

		// Token: 0x060015EB RID: 5611 RVA: 0x00026C18 File Offset: 0x00024E18
		public bool Equals(Vector3Int other)
		{
			return this == other;
		}

		// Token: 0x060015EC RID: 5612 RVA: 0x00026C3C File Offset: 0x00024E3C
		public override int GetHashCode()
		{
			int hashCode = this.y.GetHashCode();
			int hashCode2 = this.z.GetHashCode();
			return this.x.GetHashCode() ^ hashCode << 4 ^ hashCode >> 28 ^ hashCode2 >> 4 ^ hashCode2 << 28;
		}

		// Token: 0x060015ED RID: 5613 RVA: 0x00026CA4 File Offset: 0x00024EA4
		public override string ToString()
		{
			return UnityString.Format("({0}, {1}, {2})", new object[]
			{
				this.x,
				this.y,
				this.z
			});
		}

		// Token: 0x060015EE RID: 5614 RVA: 0x00026CF4 File Offset: 0x00024EF4
		public string ToString(string format)
		{
			return UnityString.Format("({0}, {1}, {2})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format),
				this.z.ToString(format)
			});
		}

		// Token: 0x17000432 RID: 1074
		// (get) Token: 0x060015EF RID: 5615 RVA: 0x00026D50 File Offset: 0x00024F50
		public static Vector3Int zero
		{
			get
			{
				return Vector3Int.s_Zero;
			}
		}

		// Token: 0x17000433 RID: 1075
		// (get) Token: 0x060015F0 RID: 5616 RVA: 0x00026D6C File Offset: 0x00024F6C
		public static Vector3Int one
		{
			get
			{
				return Vector3Int.s_One;
			}
		}

		// Token: 0x17000434 RID: 1076
		// (get) Token: 0x060015F1 RID: 5617 RVA: 0x00026D88 File Offset: 0x00024F88
		public static Vector3Int up
		{
			get
			{
				return Vector3Int.s_Up;
			}
		}

		// Token: 0x17000435 RID: 1077
		// (get) Token: 0x060015F2 RID: 5618 RVA: 0x00026DA4 File Offset: 0x00024FA4
		public static Vector3Int down
		{
			get
			{
				return Vector3Int.s_Down;
			}
		}

		// Token: 0x17000436 RID: 1078
		// (get) Token: 0x060015F3 RID: 5619 RVA: 0x00026DC0 File Offset: 0x00024FC0
		public static Vector3Int left
		{
			get
			{
				return Vector3Int.s_Left;
			}
		}

		// Token: 0x17000437 RID: 1079
		// (get) Token: 0x060015F4 RID: 5620 RVA: 0x00026DDC File Offset: 0x00024FDC
		public static Vector3Int right
		{
			get
			{
				return Vector3Int.s_Right;
			}
		}

		// Token: 0x060015F5 RID: 5621 RVA: 0x00026DF8 File Offset: 0x00024FF8
		// Note: this type is marked as 'beforefieldinit'.
		static Vector3Int()
		{
		}

		// Token: 0x04000824 RID: 2084
		private int m_X;

		// Token: 0x04000825 RID: 2085
		private int m_Y;

		// Token: 0x04000826 RID: 2086
		private int m_Z;

		// Token: 0x04000827 RID: 2087
		private static readonly Vector3Int s_Zero = new Vector3Int(0, 0, 0);

		// Token: 0x04000828 RID: 2088
		private static readonly Vector3Int s_One = new Vector3Int(1, 1, 1);

		// Token: 0x04000829 RID: 2089
		private static readonly Vector3Int s_Up = new Vector3Int(0, 1, 0);

		// Token: 0x0400082A RID: 2090
		private static readonly Vector3Int s_Down = new Vector3Int(0, -1, 0);

		// Token: 0x0400082B RID: 2091
		private static readonly Vector3Int s_Left = new Vector3Int(-1, 0, 0);

		// Token: 0x0400082C RID: 2092
		private static readonly Vector3Int s_Right = new Vector3Int(1, 0, 0);
	}
}
