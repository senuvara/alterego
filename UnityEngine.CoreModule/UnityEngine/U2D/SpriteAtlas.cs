﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.U2D
{
	// Token: 0x02000366 RID: 870
	[NativeHeader("Runtime/Graphics/SpriteFrame.h")]
	[NativeType(Header = "Runtime/2D/SpriteAtlas/SpriteAtlas.h")]
	public class SpriteAtlas : Object
	{
		// Token: 0x06001DBA RID: 7610 RVA: 0x0000AD1B File Offset: 0x00008F1B
		public SpriteAtlas()
		{
		}

		// Token: 0x1700057C RID: 1404
		// (get) Token: 0x06001DBB RID: 7611
		public extern bool isVariant { [NativeMethod("IsVariant")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700057D RID: 1405
		// (get) Token: 0x06001DBC RID: 7612
		public extern string tag { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700057E RID: 1406
		// (get) Token: 0x06001DBD RID: 7613
		public extern int spriteCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001DBE RID: 7614
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool CanBindTo(Sprite sprite);

		// Token: 0x06001DBF RID: 7615
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Sprite GetSprite(string name);

		// Token: 0x06001DC0 RID: 7616 RVA: 0x000336A0 File Offset: 0x000318A0
		public int GetSprites(Sprite[] sprites)
		{
			return this.GetSpritesScripting(sprites);
		}

		// Token: 0x06001DC1 RID: 7617 RVA: 0x000336BC File Offset: 0x000318BC
		public int GetSprites(Sprite[] sprites, string name)
		{
			return this.GetSpritesWithNameScripting(sprites, name);
		}

		// Token: 0x06001DC2 RID: 7618
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetSpritesScripting(Sprite[] sprites);

		// Token: 0x06001DC3 RID: 7619
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetSpritesWithNameScripting(Sprite[] sprites, string name);
	}
}
