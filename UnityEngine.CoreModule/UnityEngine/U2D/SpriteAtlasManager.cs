﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.U2D
{
	// Token: 0x02000365 RID: 869
	[NativeHeader("Runtime/2D/SpriteAtlas/SpriteAtlasManager.h")]
	[NativeHeader("Runtime/2D/SpriteAtlas/SpriteAtlas.h")]
	[StaticAccessor("GetSpriteAtlasManager()", StaticAccessorType.Dot)]
	public class SpriteAtlasManager
	{
		// Token: 0x06001DB1 RID: 7601 RVA: 0x00002370 File Offset: 0x00000570
		public SpriteAtlasManager()
		{
		}

		// Token: 0x1400001A RID: 26
		// (add) Token: 0x06001DB2 RID: 7602 RVA: 0x00033558 File Offset: 0x00031758
		// (remove) Token: 0x06001DB3 RID: 7603 RVA: 0x0003358C File Offset: 0x0003178C
		public static event Action<string, Action<SpriteAtlas>> atlasRequested
		{
			add
			{
				Action<string, Action<SpriteAtlas>> action = SpriteAtlasManager.atlasRequested;
				Action<string, Action<SpriteAtlas>> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<string, Action<SpriteAtlas>>>(ref SpriteAtlasManager.atlasRequested, (Action<string, Action<SpriteAtlas>>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<string, Action<SpriteAtlas>> action = SpriteAtlasManager.atlasRequested;
				Action<string, Action<SpriteAtlas>> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<string, Action<SpriteAtlas>>>(ref SpriteAtlasManager.atlasRequested, (Action<string, Action<SpriteAtlas>>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x06001DB4 RID: 7604 RVA: 0x000335C0 File Offset: 0x000317C0
		[RequiredByNativeCode]
		private static bool RequestAtlas(string tag)
		{
			bool result;
			if (SpriteAtlasManager.atlasRequested != null)
			{
				Action<string, Action<SpriteAtlas>> action = SpriteAtlasManager.atlasRequested;
				if (SpriteAtlasManager.<>f__mg$cache0 == null)
				{
					SpriteAtlasManager.<>f__mg$cache0 = new Action<SpriteAtlas>(SpriteAtlasManager.Register);
				}
				action(tag, SpriteAtlasManager.<>f__mg$cache0);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x1400001B RID: 27
		// (add) Token: 0x06001DB5 RID: 7605 RVA: 0x00033610 File Offset: 0x00031810
		// (remove) Token: 0x06001DB6 RID: 7606 RVA: 0x00033644 File Offset: 0x00031844
		public static event Action<SpriteAtlas> atlasRegistered
		{
			add
			{
				Action<SpriteAtlas> action = SpriteAtlasManager.atlasRegistered;
				Action<SpriteAtlas> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<SpriteAtlas>>(ref SpriteAtlasManager.atlasRegistered, (Action<SpriteAtlas>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<SpriteAtlas> action = SpriteAtlasManager.atlasRegistered;
				Action<SpriteAtlas> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<SpriteAtlas>>(ref SpriteAtlasManager.atlasRegistered, (Action<SpriteAtlas>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x06001DB7 RID: 7607 RVA: 0x00033678 File Offset: 0x00031878
		[RequiredByNativeCode]
		private static void PostRegisteredAtlas(SpriteAtlas spriteAtlas)
		{
			if (SpriteAtlasManager.atlasRegistered != null)
			{
				SpriteAtlasManager.atlasRegistered(spriteAtlas);
			}
		}

		// Token: 0x06001DB8 RID: 7608
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Register(SpriteAtlas spriteAtlas);

		// Token: 0x06001DB9 RID: 7609 RVA: 0x00033692 File Offset: 0x00031892
		// Note: this type is marked as 'beforefieldinit'.
		static SpriteAtlasManager()
		{
		}

		// Token: 0x04000B05 RID: 2821
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action<string, Action<SpriteAtlas>> atlasRequested = null;

		// Token: 0x04000B06 RID: 2822
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action<SpriteAtlas> atlasRegistered = null;

		// Token: 0x04000B07 RID: 2823
		[CompilerGenerated]
		private static Action<SpriteAtlas> <>f__mg$cache0;
	}
}
