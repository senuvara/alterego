﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200008C RID: 140
	[UsedByNativeCode]
	[NativeHeader("Runtime/Graphics/LightmapData.h")]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class LightmapData
	{
		// Token: 0x0600077D RID: 1917 RVA: 0x00002370 File Offset: 0x00000570
		public LightmapData()
		{
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x0600077E RID: 1918 RVA: 0x0000FCBC File Offset: 0x0000DEBC
		// (set) Token: 0x0600077F RID: 1919 RVA: 0x0000FCD7 File Offset: 0x0000DED7
		[Obsolete("Use lightmapColor property (UnityUpgradable) -> lightmapColor", false)]
		public Texture2D lightmapLight
		{
			get
			{
				return this.m_Light;
			}
			set
			{
				this.m_Light = value;
			}
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x06000780 RID: 1920 RVA: 0x0000FCE4 File Offset: 0x0000DEE4
		// (set) Token: 0x06000781 RID: 1921 RVA: 0x0000FCD7 File Offset: 0x0000DED7
		public Texture2D lightmapColor
		{
			get
			{
				return this.m_Light;
			}
			set
			{
				this.m_Light = value;
			}
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x06000782 RID: 1922 RVA: 0x0000FD00 File Offset: 0x0000DF00
		// (set) Token: 0x06000783 RID: 1923 RVA: 0x0000FD1B File Offset: 0x0000DF1B
		public Texture2D lightmapDir
		{
			get
			{
				return this.m_Dir;
			}
			set
			{
				this.m_Dir = value;
			}
		}

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x06000784 RID: 1924 RVA: 0x0000FD28 File Offset: 0x0000DF28
		// (set) Token: 0x06000785 RID: 1925 RVA: 0x0000FD43 File Offset: 0x0000DF43
		public Texture2D shadowMask
		{
			get
			{
				return this.m_ShadowMask;
			}
			set
			{
				this.m_ShadowMask = value;
			}
		}

		// Token: 0x04000178 RID: 376
		internal Texture2D m_Light;

		// Token: 0x04000179 RID: 377
		internal Texture2D m_Dir;

		// Token: 0x0400017A RID: 378
		internal Texture2D m_ShadowMask;
	}
}
