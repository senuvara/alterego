﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200002F RID: 47
	[UsedByNativeCode]
	public sealed class HideInInspector : Attribute
	{
		// Token: 0x0600028D RID: 653 RVA: 0x0000898B File Offset: 0x00006B8B
		public HideInInspector()
		{
		}
	}
}
