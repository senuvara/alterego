﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x02000223 RID: 547
	[NativeHeader("Runtime/Graphics/GraphicsFormatUtility.bindings.h")]
	[NativeHeader("Runtime/Misc/SystemInfo.h")]
	[NativeHeader("Runtime/Shaders/GraphicsCapsScriptBindings.h")]
	[NativeHeader("Runtime/Input/GetInput.h")]
	[NativeHeader("Runtime/Camera/RenderLoops/MotionVectorRenderLoop.h")]
	public sealed class SystemInfo
	{
		// Token: 0x0600123A RID: 4666 RVA: 0x00002370 File Offset: 0x00000570
		public SystemInfo()
		{
		}

		// Token: 0x17000346 RID: 838
		// (get) Token: 0x0600123B RID: 4667 RVA: 0x000209DC File Offset: 0x0001EBDC
		[NativeProperty]
		public static float batteryLevel
		{
			get
			{
				return SystemInfo.GetBatteryLevel();
			}
		}

		// Token: 0x17000347 RID: 839
		// (get) Token: 0x0600123C RID: 4668 RVA: 0x000209F8 File Offset: 0x0001EBF8
		public static BatteryStatus batteryStatus
		{
			get
			{
				return SystemInfo.GetBatteryStatus();
			}
		}

		// Token: 0x17000348 RID: 840
		// (get) Token: 0x0600123D RID: 4669 RVA: 0x00020A14 File Offset: 0x0001EC14
		public static string operatingSystem
		{
			get
			{
				return SystemInfo.GetOperatingSystem();
			}
		}

		// Token: 0x17000349 RID: 841
		// (get) Token: 0x0600123E RID: 4670 RVA: 0x00020A30 File Offset: 0x0001EC30
		public static OperatingSystemFamily operatingSystemFamily
		{
			get
			{
				return SystemInfo.GetOperatingSystemFamily();
			}
		}

		// Token: 0x1700034A RID: 842
		// (get) Token: 0x0600123F RID: 4671 RVA: 0x00020A4C File Offset: 0x0001EC4C
		public static string processorType
		{
			get
			{
				return SystemInfo.GetProcessorType();
			}
		}

		// Token: 0x1700034B RID: 843
		// (get) Token: 0x06001240 RID: 4672 RVA: 0x00020A68 File Offset: 0x0001EC68
		public static int processorFrequency
		{
			get
			{
				return SystemInfo.GetProcessorFrequencyMHz();
			}
		}

		// Token: 0x1700034C RID: 844
		// (get) Token: 0x06001241 RID: 4673 RVA: 0x00020A84 File Offset: 0x0001EC84
		public static int processorCount
		{
			get
			{
				return SystemInfo.GetProcessorCount();
			}
		}

		// Token: 0x1700034D RID: 845
		// (get) Token: 0x06001242 RID: 4674 RVA: 0x00020AA0 File Offset: 0x0001ECA0
		public static int systemMemorySize
		{
			get
			{
				return SystemInfo.GetPhysicalMemoryMB();
			}
		}

		// Token: 0x1700034E RID: 846
		// (get) Token: 0x06001243 RID: 4675 RVA: 0x00020ABC File Offset: 0x0001ECBC
		public static string deviceUniqueIdentifier
		{
			get
			{
				return SystemInfo.GetDeviceUniqueIdentifier();
			}
		}

		// Token: 0x1700034F RID: 847
		// (get) Token: 0x06001244 RID: 4676 RVA: 0x00020AD8 File Offset: 0x0001ECD8
		public static string deviceName
		{
			get
			{
				return SystemInfo.GetDeviceName();
			}
		}

		// Token: 0x17000350 RID: 848
		// (get) Token: 0x06001245 RID: 4677 RVA: 0x00020AF4 File Offset: 0x0001ECF4
		public static string deviceModel
		{
			get
			{
				return SystemInfo.GetDeviceModel();
			}
		}

		// Token: 0x17000351 RID: 849
		// (get) Token: 0x06001246 RID: 4678 RVA: 0x00020B10 File Offset: 0x0001ED10
		public static bool supportsAccelerometer
		{
			get
			{
				return SystemInfo.SupportsAccelerometer();
			}
		}

		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06001247 RID: 4679 RVA: 0x00020B2C File Offset: 0x0001ED2C
		public static bool supportsGyroscope
		{
			get
			{
				return SystemInfo.IsGyroAvailable();
			}
		}

		// Token: 0x17000353 RID: 851
		// (get) Token: 0x06001248 RID: 4680 RVA: 0x00020B48 File Offset: 0x0001ED48
		public static bool supportsLocationService
		{
			get
			{
				return SystemInfo.SupportsLocationService();
			}
		}

		// Token: 0x17000354 RID: 852
		// (get) Token: 0x06001249 RID: 4681 RVA: 0x00020B64 File Offset: 0x0001ED64
		public static bool supportsVibration
		{
			get
			{
				return SystemInfo.SupportsVibration();
			}
		}

		// Token: 0x17000355 RID: 853
		// (get) Token: 0x0600124A RID: 4682 RVA: 0x00020B80 File Offset: 0x0001ED80
		public static bool supportsAudio
		{
			get
			{
				return SystemInfo.SupportsAudio();
			}
		}

		// Token: 0x17000356 RID: 854
		// (get) Token: 0x0600124B RID: 4683 RVA: 0x00020B9C File Offset: 0x0001ED9C
		public static DeviceType deviceType
		{
			get
			{
				return SystemInfo.GetDeviceType();
			}
		}

		// Token: 0x17000357 RID: 855
		// (get) Token: 0x0600124C RID: 4684 RVA: 0x00020BB8 File Offset: 0x0001EDB8
		public static int graphicsMemorySize
		{
			get
			{
				return SystemInfo.GetGraphicsMemorySize();
			}
		}

		// Token: 0x17000358 RID: 856
		// (get) Token: 0x0600124D RID: 4685 RVA: 0x00020BD4 File Offset: 0x0001EDD4
		public static string graphicsDeviceName
		{
			get
			{
				return SystemInfo.GetGraphicsDeviceName();
			}
		}

		// Token: 0x17000359 RID: 857
		// (get) Token: 0x0600124E RID: 4686 RVA: 0x00020BF0 File Offset: 0x0001EDF0
		public static string graphicsDeviceVendor
		{
			get
			{
				return SystemInfo.GetGraphicsDeviceVendor();
			}
		}

		// Token: 0x1700035A RID: 858
		// (get) Token: 0x0600124F RID: 4687 RVA: 0x00020C0C File Offset: 0x0001EE0C
		public static int graphicsDeviceID
		{
			get
			{
				return SystemInfo.GetGraphicsDeviceID();
			}
		}

		// Token: 0x1700035B RID: 859
		// (get) Token: 0x06001250 RID: 4688 RVA: 0x00020C28 File Offset: 0x0001EE28
		public static int graphicsDeviceVendorID
		{
			get
			{
				return SystemInfo.GetGraphicsDeviceVendorID();
			}
		}

		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06001251 RID: 4689 RVA: 0x00020C44 File Offset: 0x0001EE44
		public static GraphicsDeviceType graphicsDeviceType
		{
			get
			{
				return SystemInfo.GetGraphicsDeviceType();
			}
		}

		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06001252 RID: 4690 RVA: 0x00020C60 File Offset: 0x0001EE60
		public static bool graphicsUVStartsAtTop
		{
			get
			{
				return SystemInfo.GetGraphicsUVStartsAtTop();
			}
		}

		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06001253 RID: 4691 RVA: 0x00020C7C File Offset: 0x0001EE7C
		public static string graphicsDeviceVersion
		{
			get
			{
				return SystemInfo.GetGraphicsDeviceVersion();
			}
		}

		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06001254 RID: 4692 RVA: 0x00020C98 File Offset: 0x0001EE98
		public static int graphicsShaderLevel
		{
			get
			{
				return SystemInfo.GetGraphicsShaderLevel();
			}
		}

		// Token: 0x17000360 RID: 864
		// (get) Token: 0x06001255 RID: 4693 RVA: 0x00020CB4 File Offset: 0x0001EEB4
		public static bool graphicsMultiThreaded
		{
			get
			{
				return SystemInfo.GetGraphicsMultiThreaded();
			}
		}

		// Token: 0x17000361 RID: 865
		// (get) Token: 0x06001256 RID: 4694 RVA: 0x00020CD0 File Offset: 0x0001EED0
		public static bool hasHiddenSurfaceRemovalOnGPU
		{
			get
			{
				return SystemInfo.HasHiddenSurfaceRemovalOnGPU();
			}
		}

		// Token: 0x17000362 RID: 866
		// (get) Token: 0x06001257 RID: 4695 RVA: 0x00020CEC File Offset: 0x0001EEEC
		public static bool hasDynamicUniformArrayIndexingInFragmentShaders
		{
			get
			{
				return SystemInfo.HasDynamicUniformArrayIndexingInFragmentShaders();
			}
		}

		// Token: 0x17000363 RID: 867
		// (get) Token: 0x06001258 RID: 4696 RVA: 0x00020D08 File Offset: 0x0001EF08
		public static bool supportsShadows
		{
			get
			{
				return SystemInfo.SupportsShadows();
			}
		}

		// Token: 0x17000364 RID: 868
		// (get) Token: 0x06001259 RID: 4697 RVA: 0x00020D24 File Offset: 0x0001EF24
		public static bool supportsRawShadowDepthSampling
		{
			get
			{
				return SystemInfo.SupportsRawShadowDepthSampling();
			}
		}

		// Token: 0x17000365 RID: 869
		// (get) Token: 0x0600125A RID: 4698 RVA: 0x00020D40 File Offset: 0x0001EF40
		[Obsolete("supportsRenderTextures always returns true, no need to call it")]
		public static bool supportsRenderTextures
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000366 RID: 870
		// (get) Token: 0x0600125B RID: 4699 RVA: 0x00020D58 File Offset: 0x0001EF58
		public static bool supportsMotionVectors
		{
			get
			{
				return SystemInfo.SupportsMotionVectors();
			}
		}

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x0600125C RID: 4700 RVA: 0x00020D74 File Offset: 0x0001EF74
		public static bool supportsRenderToCubemap
		{
			get
			{
				return SystemInfo.SupportsRenderToCubemap();
			}
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x0600125D RID: 4701 RVA: 0x00020D90 File Offset: 0x0001EF90
		public static bool supportsImageEffects
		{
			get
			{
				return SystemInfo.SupportsImageEffects();
			}
		}

		// Token: 0x17000369 RID: 873
		// (get) Token: 0x0600125E RID: 4702 RVA: 0x00020DAC File Offset: 0x0001EFAC
		public static bool supports3DTextures
		{
			get
			{
				return SystemInfo.Supports3DTextures();
			}
		}

		// Token: 0x1700036A RID: 874
		// (get) Token: 0x0600125F RID: 4703 RVA: 0x00020DC8 File Offset: 0x0001EFC8
		public static bool supports2DArrayTextures
		{
			get
			{
				return SystemInfo.Supports2DArrayTextures();
			}
		}

		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06001260 RID: 4704 RVA: 0x00020DE4 File Offset: 0x0001EFE4
		public static bool supports3DRenderTextures
		{
			get
			{
				return SystemInfo.Supports3DRenderTextures();
			}
		}

		// Token: 0x1700036C RID: 876
		// (get) Token: 0x06001261 RID: 4705 RVA: 0x00020E00 File Offset: 0x0001F000
		public static bool supportsCubemapArrayTextures
		{
			get
			{
				return SystemInfo.SupportsCubemapArrayTextures();
			}
		}

		// Token: 0x1700036D RID: 877
		// (get) Token: 0x06001262 RID: 4706 RVA: 0x00020E1C File Offset: 0x0001F01C
		public static CopyTextureSupport copyTextureSupport
		{
			get
			{
				return SystemInfo.GetCopyTextureSupport();
			}
		}

		// Token: 0x1700036E RID: 878
		// (get) Token: 0x06001263 RID: 4707 RVA: 0x00020E38 File Offset: 0x0001F038
		public static bool supportsComputeShaders
		{
			get
			{
				return SystemInfo.SupportsComputeShaders();
			}
		}

		// Token: 0x1700036F RID: 879
		// (get) Token: 0x06001264 RID: 4708 RVA: 0x00020E54 File Offset: 0x0001F054
		public static bool supportsInstancing
		{
			get
			{
				return SystemInfo.SupportsInstancing();
			}
		}

		// Token: 0x17000370 RID: 880
		// (get) Token: 0x06001265 RID: 4709 RVA: 0x00020E70 File Offset: 0x0001F070
		public static bool supportsHardwareQuadTopology
		{
			get
			{
				return SystemInfo.SupportsHardwareQuadTopology();
			}
		}

		// Token: 0x17000371 RID: 881
		// (get) Token: 0x06001266 RID: 4710 RVA: 0x00020E8C File Offset: 0x0001F08C
		public static bool supports32bitsIndexBuffer
		{
			get
			{
				return SystemInfo.Supports32bitsIndexBuffer();
			}
		}

		// Token: 0x17000372 RID: 882
		// (get) Token: 0x06001267 RID: 4711 RVA: 0x00020EA8 File Offset: 0x0001F0A8
		public static bool supportsSparseTextures
		{
			get
			{
				return SystemInfo.SupportsSparseTextures();
			}
		}

		// Token: 0x17000373 RID: 883
		// (get) Token: 0x06001268 RID: 4712 RVA: 0x00020EC4 File Offset: 0x0001F0C4
		public static int supportedRenderTargetCount
		{
			get
			{
				return SystemInfo.SupportedRenderTargetCount();
			}
		}

		// Token: 0x17000374 RID: 884
		// (get) Token: 0x06001269 RID: 4713 RVA: 0x00020EE0 File Offset: 0x0001F0E0
		public static bool supportsSeparatedRenderTargetsBlend
		{
			get
			{
				return SystemInfo.SupportsSeparatedRenderTargetsBlend();
			}
		}

		// Token: 0x17000375 RID: 885
		// (get) Token: 0x0600126A RID: 4714 RVA: 0x00020EFC File Offset: 0x0001F0FC
		internal static int supportedRandomWriteTargetCount
		{
			get
			{
				return SystemInfo.SupportedRandomWriteTargetCount();
			}
		}

		// Token: 0x17000376 RID: 886
		// (get) Token: 0x0600126B RID: 4715 RVA: 0x00020F18 File Offset: 0x0001F118
		public static int supportsMultisampledTextures
		{
			get
			{
				return SystemInfo.SupportsMultisampledTextures();
			}
		}

		// Token: 0x17000377 RID: 887
		// (get) Token: 0x0600126C RID: 4716 RVA: 0x00020F34 File Offset: 0x0001F134
		public static bool supportsMultisampleAutoResolve
		{
			get
			{
				return SystemInfo.SupportsMultisampleAutoResolve();
			}
		}

		// Token: 0x17000378 RID: 888
		// (get) Token: 0x0600126D RID: 4717 RVA: 0x00020F50 File Offset: 0x0001F150
		public static int supportsTextureWrapMirrorOnce
		{
			get
			{
				return SystemInfo.SupportsTextureWrapMirrorOnce();
			}
		}

		// Token: 0x17000379 RID: 889
		// (get) Token: 0x0600126E RID: 4718 RVA: 0x00020F6C File Offset: 0x0001F16C
		public static bool usesReversedZBuffer
		{
			get
			{
				return SystemInfo.UsesReversedZBuffer();
			}
		}

		// Token: 0x1700037A RID: 890
		// (get) Token: 0x0600126F RID: 4719 RVA: 0x00020F88 File Offset: 0x0001F188
		[Obsolete("supportsStencil always returns true, no need to call it")]
		public static int supportsStencil
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06001270 RID: 4720 RVA: 0x00020FA0 File Offset: 0x0001F1A0
		private static bool IsValidEnumValue(Enum value)
		{
			return Enum.IsDefined(value.GetType(), value);
		}

		// Token: 0x06001271 RID: 4721 RVA: 0x00020FD0 File Offset: 0x0001F1D0
		public static bool SupportsRenderTextureFormat(RenderTextureFormat format)
		{
			if (!SystemInfo.IsValidEnumValue(format))
			{
				throw new ArgumentException("Failed SupportsRenderTextureFormat; format is not a valid RenderTextureFormat");
			}
			return SystemInfo.HasRenderTextureNative(format);
		}

		// Token: 0x06001272 RID: 4722 RVA: 0x00021008 File Offset: 0x0001F208
		public static bool SupportsBlendingOnRenderTextureFormat(RenderTextureFormat format)
		{
			if (!SystemInfo.IsValidEnumValue(format))
			{
				throw new ArgumentException("Failed SupportsBlendingOnRenderTextureFormat; format is not a valid RenderTextureFormat");
			}
			return SystemInfo.SupportsBlendingOnRenderTextureFormatNative(format);
		}

		// Token: 0x06001273 RID: 4723 RVA: 0x00021040 File Offset: 0x0001F240
		public static bool SupportsTextureFormat(TextureFormat format)
		{
			if (!SystemInfo.IsValidEnumValue(format))
			{
				throw new ArgumentException("Failed SupportsTextureFormat; format is not a valid TextureFormat");
			}
			return SystemInfo.SupportsTextureFormatNative(format);
		}

		// Token: 0x1700037B RID: 891
		// (get) Token: 0x06001274 RID: 4724 RVA: 0x00021078 File Offset: 0x0001F278
		public static NPOTSupport npotSupport
		{
			get
			{
				return SystemInfo.GetNPOTSupport();
			}
		}

		// Token: 0x1700037C RID: 892
		// (get) Token: 0x06001275 RID: 4725 RVA: 0x00021094 File Offset: 0x0001F294
		public static int maxTextureSize
		{
			get
			{
				return SystemInfo.GetMaxTextureSize();
			}
		}

		// Token: 0x1700037D RID: 893
		// (get) Token: 0x06001276 RID: 4726 RVA: 0x000210B0 File Offset: 0x0001F2B0
		public static int maxCubemapSize
		{
			get
			{
				return SystemInfo.GetMaxCubemapSize();
			}
		}

		// Token: 0x1700037E RID: 894
		// (get) Token: 0x06001277 RID: 4727 RVA: 0x000210CC File Offset: 0x0001F2CC
		internal static int maxRenderTextureSize
		{
			get
			{
				return SystemInfo.GetMaxRenderTextureSize();
			}
		}

		// Token: 0x1700037F RID: 895
		// (get) Token: 0x06001278 RID: 4728 RVA: 0x000210E8 File Offset: 0x0001F2E8
		public static bool supportsAsyncCompute
		{
			get
			{
				return SystemInfo.SupportsAsyncCompute();
			}
		}

		// Token: 0x17000380 RID: 896
		// (get) Token: 0x06001279 RID: 4729 RVA: 0x00021104 File Offset: 0x0001F304
		public static bool supportsGPUFence
		{
			get
			{
				return SystemInfo.SupportsGPUFence();
			}
		}

		// Token: 0x17000381 RID: 897
		// (get) Token: 0x0600127A RID: 4730 RVA: 0x00021120 File Offset: 0x0001F320
		public static bool supportsAsyncGPUReadback
		{
			get
			{
				return SystemInfo.SupportsAsyncGPUReadback();
			}
		}

		// Token: 0x17000382 RID: 898
		// (get) Token: 0x0600127B RID: 4731 RVA: 0x0002113C File Offset: 0x0001F33C
		public static bool supportsMipStreaming
		{
			get
			{
				return SystemInfo.SupportsMipStreaming();
			}
		}

		// Token: 0x17000383 RID: 899
		// (get) Token: 0x0600127C RID: 4732 RVA: 0x00021158 File Offset: 0x0001F358
		[Obsolete("graphicsPixelFillrate is no longer supported in Unity 5.0+.")]
		public static int graphicsPixelFillrate
		{
			get
			{
				return -1;
			}
		}

		// Token: 0x17000384 RID: 900
		// (get) Token: 0x0600127D RID: 4733 RVA: 0x00021170 File Offset: 0x0001F370
		[Obsolete("Vertex program support is required in Unity 5.0+")]
		public static bool supportsVertexPrograms
		{
			get
			{
				return true;
			}
		}

		// Token: 0x0600127E RID: 4734
		[FreeFunction("systeminfo::GetBatteryLevel")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetBatteryLevel();

		// Token: 0x0600127F RID: 4735
		[FreeFunction("systeminfo::GetBatteryStatus")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern BatteryStatus GetBatteryStatus();

		// Token: 0x06001280 RID: 4736
		[FreeFunction("systeminfo::GetOperatingSystem")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetOperatingSystem();

		// Token: 0x06001281 RID: 4737
		[FreeFunction("systeminfo::GetOperatingSystemFamily")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern OperatingSystemFamily GetOperatingSystemFamily();

		// Token: 0x06001282 RID: 4738
		[FreeFunction("systeminfo::GetProcessorType")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetProcessorType();

		// Token: 0x06001283 RID: 4739
		[FreeFunction("systeminfo::GetProcessorFrequencyMHz")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetProcessorFrequencyMHz();

		// Token: 0x06001284 RID: 4740
		[FreeFunction("systeminfo::GetProcessorCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetProcessorCount();

		// Token: 0x06001285 RID: 4741
		[FreeFunction("systeminfo::GetPhysicalMemoryMB")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetPhysicalMemoryMB();

		// Token: 0x06001286 RID: 4742
		[FreeFunction("systeminfo::GetDeviceUniqueIdentifier")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetDeviceUniqueIdentifier();

		// Token: 0x06001287 RID: 4743
		[FreeFunction("systeminfo::GetDeviceName")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetDeviceName();

		// Token: 0x06001288 RID: 4744
		[FreeFunction("systeminfo::GetDeviceModel")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetDeviceModel();

		// Token: 0x06001289 RID: 4745
		[FreeFunction("systeminfo::SupportsAccelerometer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsAccelerometer();

		// Token: 0x0600128A RID: 4746
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsGyroAvailable();

		// Token: 0x0600128B RID: 4747
		[FreeFunction("systeminfo::SupportsLocationService")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsLocationService();

		// Token: 0x0600128C RID: 4748
		[FreeFunction("systeminfo::SupportsVibration")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsVibration();

		// Token: 0x0600128D RID: 4749
		[FreeFunction("systeminfo::SupportsAudio")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsAudio();

		// Token: 0x0600128E RID: 4750
		[FreeFunction("systeminfo::GetDeviceType")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern DeviceType GetDeviceType();

		// Token: 0x0600128F RID: 4751
		[FreeFunction("ScriptingGraphicsCaps::GetGraphicsMemorySize")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetGraphicsMemorySize();

		// Token: 0x06001290 RID: 4752
		[FreeFunction("ScriptingGraphicsCaps::GetGraphicsDeviceName")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetGraphicsDeviceName();

		// Token: 0x06001291 RID: 4753
		[FreeFunction("ScriptingGraphicsCaps::GetGraphicsDeviceVendor")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetGraphicsDeviceVendor();

		// Token: 0x06001292 RID: 4754
		[FreeFunction("ScriptingGraphicsCaps::GetGraphicsDeviceID")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetGraphicsDeviceID();

		// Token: 0x06001293 RID: 4755
		[FreeFunction("ScriptingGraphicsCaps::GetGraphicsDeviceVendorID")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetGraphicsDeviceVendorID();

		// Token: 0x06001294 RID: 4756
		[FreeFunction("ScriptingGraphicsCaps::GetGraphicsDeviceType")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern GraphicsDeviceType GetGraphicsDeviceType();

		// Token: 0x06001295 RID: 4757
		[FreeFunction("ScriptingGraphicsCaps::GetGraphicsUVStartsAtTop")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetGraphicsUVStartsAtTop();

		// Token: 0x06001296 RID: 4758
		[FreeFunction("ScriptingGraphicsCaps::GetGraphicsDeviceVersion")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetGraphicsDeviceVersion();

		// Token: 0x06001297 RID: 4759
		[FreeFunction("ScriptingGraphicsCaps::GetGraphicsShaderLevel")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetGraphicsShaderLevel();

		// Token: 0x06001298 RID: 4760
		[FreeFunction("ScriptingGraphicsCaps::GetGraphicsMultiThreaded")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetGraphicsMultiThreaded();

		// Token: 0x06001299 RID: 4761
		[FreeFunction("ScriptingGraphicsCaps::HasHiddenSurfaceRemovalOnGPU")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool HasHiddenSurfaceRemovalOnGPU();

		// Token: 0x0600129A RID: 4762
		[FreeFunction("ScriptingGraphicsCaps::HasDynamicUniformArrayIndexingInFragmentShaders")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool HasDynamicUniformArrayIndexingInFragmentShaders();

		// Token: 0x0600129B RID: 4763
		[FreeFunction("ScriptingGraphicsCaps::SupportsShadows")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsShadows();

		// Token: 0x0600129C RID: 4764
		[FreeFunction("ScriptingGraphicsCaps::SupportsRawShadowDepthSampling")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsRawShadowDepthSampling();

		// Token: 0x0600129D RID: 4765
		[FreeFunction("SupportsMotionVectors")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsMotionVectors();

		// Token: 0x0600129E RID: 4766
		[FreeFunction("ScriptingGraphicsCaps::SupportsRenderToCubemap")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsRenderToCubemap();

		// Token: 0x0600129F RID: 4767
		[FreeFunction("ScriptingGraphicsCaps::SupportsImageEffects")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsImageEffects();

		// Token: 0x060012A0 RID: 4768
		[FreeFunction("ScriptingGraphicsCaps::Supports3DTextures")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Supports3DTextures();

		// Token: 0x060012A1 RID: 4769
		[FreeFunction("ScriptingGraphicsCaps::Supports2DArrayTextures")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Supports2DArrayTextures();

		// Token: 0x060012A2 RID: 4770
		[FreeFunction("ScriptingGraphicsCaps::Supports3DRenderTextures")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Supports3DRenderTextures();

		// Token: 0x060012A3 RID: 4771
		[FreeFunction("ScriptingGraphicsCaps::SupportsCubemapArrayTextures")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsCubemapArrayTextures();

		// Token: 0x060012A4 RID: 4772
		[FreeFunction("ScriptingGraphicsCaps::GetCopyTextureSupport")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern CopyTextureSupport GetCopyTextureSupport();

		// Token: 0x060012A5 RID: 4773
		[FreeFunction("ScriptingGraphicsCaps::SupportsComputeShaders")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsComputeShaders();

		// Token: 0x060012A6 RID: 4774
		[FreeFunction("ScriptingGraphicsCaps::SupportsInstancing")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsInstancing();

		// Token: 0x060012A7 RID: 4775
		[FreeFunction("ScriptingGraphicsCaps::SupportsHardwareQuadTopology")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsHardwareQuadTopology();

		// Token: 0x060012A8 RID: 4776
		[FreeFunction("ScriptingGraphicsCaps::Supports32bitsIndexBuffer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Supports32bitsIndexBuffer();

		// Token: 0x060012A9 RID: 4777
		[FreeFunction("ScriptingGraphicsCaps::SupportsSparseTextures")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsSparseTextures();

		// Token: 0x060012AA RID: 4778
		[FreeFunction("ScriptingGraphicsCaps::SupportedRenderTargetCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int SupportedRenderTargetCount();

		// Token: 0x060012AB RID: 4779
		[FreeFunction("ScriptingGraphicsCaps::SupportsSeparatedRenderTargetsBlend")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsSeparatedRenderTargetsBlend();

		// Token: 0x060012AC RID: 4780
		[FreeFunction("ScriptingGraphicsCaps::SupportedRandomWriteTargetCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int SupportedRandomWriteTargetCount();

		// Token: 0x060012AD RID: 4781
		[FreeFunction("ScriptingGraphicsCaps::SupportsMultisampledTextures")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int SupportsMultisampledTextures();

		// Token: 0x060012AE RID: 4782
		[FreeFunction("ScriptingGraphicsCaps::SupportsMultisampleAutoResolve")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsMultisampleAutoResolve();

		// Token: 0x060012AF RID: 4783
		[FreeFunction("ScriptingGraphicsCaps::SupportsTextureWrapMirrorOnce")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int SupportsTextureWrapMirrorOnce();

		// Token: 0x060012B0 RID: 4784
		[FreeFunction("ScriptingGraphicsCaps::UsesReversedZBuffer")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool UsesReversedZBuffer();

		// Token: 0x060012B1 RID: 4785
		[FreeFunction("ScriptingGraphicsCaps::HasRenderTexture")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool HasRenderTextureNative(RenderTextureFormat format);

		// Token: 0x060012B2 RID: 4786
		[FreeFunction("ScriptingGraphicsCaps::SupportsBlendingOnRenderTextureFormat")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsBlendingOnRenderTextureFormatNative(RenderTextureFormat format);

		// Token: 0x060012B3 RID: 4787
		[FreeFunction("ScriptingGraphicsCaps::SupportsTextureFormat")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsTextureFormatNative(TextureFormat format);

		// Token: 0x060012B4 RID: 4788
		[FreeFunction("ScriptingGraphicsCaps::GetNPOTSupport")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern NPOTSupport GetNPOTSupport();

		// Token: 0x060012B5 RID: 4789
		[FreeFunction("ScriptingGraphicsCaps::GetMaxTextureSize")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetMaxTextureSize();

		// Token: 0x060012B6 RID: 4790
		[FreeFunction("ScriptingGraphicsCaps::GetMaxCubemapSize")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetMaxCubemapSize();

		// Token: 0x060012B7 RID: 4791
		[FreeFunction("ScriptingGraphicsCaps::GetMaxRenderTextureSize")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetMaxRenderTextureSize();

		// Token: 0x060012B8 RID: 4792
		[FreeFunction("ScriptingGraphicsCaps::SupportsAsyncCompute")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsAsyncCompute();

		// Token: 0x060012B9 RID: 4793
		[FreeFunction("ScriptingGraphicsCaps::SupportsGPUFence")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsGPUFence();

		// Token: 0x060012BA RID: 4794
		[FreeFunction("ScriptingGraphicsCaps::SupportsAsyncGPUReadback")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsAsyncGPUReadback();

		// Token: 0x060012BB RID: 4795
		[FreeFunction("ScriptingGraphicsCaps::SupportsMipStreaming")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SupportsMipStreaming();

		// Token: 0x060012BC RID: 4796
		[FreeFunction("ScriptingGraphicsCaps::IsFormatSupported")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsFormatSupported(GraphicsFormat format, FormatUsage usage);

		// Token: 0x040007A6 RID: 1958
		public const string unsupportedIdentifier = "n/a";
	}
}
