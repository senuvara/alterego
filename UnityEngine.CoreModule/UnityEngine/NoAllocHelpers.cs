﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200015B RID: 347
	[NativeHeader("Runtime/Export/NoAllocHelpers.bindings.h")]
	internal sealed class NoAllocHelpers
	{
		// Token: 0x06000F0D RID: 3853 RVA: 0x00002370 File Offset: 0x00000570
		public NoAllocHelpers()
		{
		}

		// Token: 0x06000F0E RID: 3854 RVA: 0x0001A094 File Offset: 0x00018294
		public static void ResizeList<T>(List<T> list, int size)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (size < 0 || size > list.Capacity)
			{
				throw new ArgumentException("invalid size to resize.", "list");
			}
			if (size != list.Count)
			{
				NoAllocHelpers.Internal_ResizeList(list, size);
			}
		}

		// Token: 0x06000F0F RID: 3855 RVA: 0x0001A0E9 File Offset: 0x000182E9
		public static void EnsureListElemCount<T>(List<T> list, int count)
		{
			list.Clear();
			if (list.Capacity < count)
			{
				list.Capacity = count;
			}
			NoAllocHelpers.ResizeList<T>(list, count);
		}

		// Token: 0x06000F10 RID: 3856 RVA: 0x0001A10C File Offset: 0x0001830C
		public static int SafeLength(Array values)
		{
			return (values == null) ? 0 : values.Length;
		}

		// Token: 0x06000F11 RID: 3857 RVA: 0x0001A134 File Offset: 0x00018334
		public static int SafeLength<T>(List<T> values)
		{
			return (values == null) ? 0 : values.Count;
		}

		// Token: 0x06000F12 RID: 3858 RVA: 0x0001A15C File Offset: 0x0001835C
		public static T[] ExtractArrayFromListT<T>(List<T> list)
		{
			return (T[])NoAllocHelpers.ExtractArrayFromList(list);
		}

		// Token: 0x06000F13 RID: 3859
		[FreeFunction("NoAllocHelpers_Bindings::Internal_ResizeList")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_ResizeList(object list, int size);

		// Token: 0x06000F14 RID: 3860
		[FreeFunction("NoAllocHelpers_Bindings::ExtractArrayFromList")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Array ExtractArrayFromList(object list);
	}
}
