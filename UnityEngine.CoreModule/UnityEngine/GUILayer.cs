﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000078 RID: 120
	[RequireComponent(typeof(Camera))]
	[Obsolete("This component is part of the legacy UI system and will be removed in a future release.")]
	public class GUILayer : Behaviour
	{
		// Token: 0x060005D2 RID: 1490 RVA: 0x0000A52A File Offset: 0x0000872A
		public GUILayer()
		{
		}

		// Token: 0x060005D3 RID: 1491 RVA: 0x0000DA50 File Offset: 0x0000BC50
		public GUIElement HitTest(Vector3 screenPosition)
		{
			return this.HitTest(new Vector2(screenPosition.x, screenPosition.y));
		}

		// Token: 0x060005D4 RID: 1492 RVA: 0x0000DA7E File Offset: 0x0000BC7E
		private GUIElement HitTest(Vector2 screenPosition)
		{
			return this.HitTest_Injected(ref screenPosition);
		}

		// Token: 0x060005D5 RID: 1493
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern GUIElement HitTest_Injected(ref Vector2 screenPosition);
	}
}
