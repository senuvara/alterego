﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C2 RID: 194
	public enum AnisotropicFiltering
	{
		// Token: 0x04000210 RID: 528
		Disable,
		// Token: 0x04000211 RID: 529
		Enable,
		// Token: 0x04000212 RID: 530
		ForceEnable
	}
}
