﻿using System;

namespace UnityEngine
{
	// Token: 0x02000119 RID: 281
	public enum AndroidActivityIndicatorStyle
	{
		// Token: 0x04000505 RID: 1285
		DontShow = -1,
		// Token: 0x04000506 RID: 1286
		Large,
		// Token: 0x04000507 RID: 1287
		InversedLarge,
		// Token: 0x04000508 RID: 1288
		Small,
		// Token: 0x04000509 RID: 1289
		InversedSmall
	}
}
