﻿using System;
using System.Collections;
using System.Reflection;
using System.Security;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200005B RID: 91
	[RequiredByNativeCode]
	internal class SetupCoroutine
	{
		// Token: 0x060004F0 RID: 1264 RVA: 0x00002370 File Offset: 0x00000570
		public SetupCoroutine()
		{
		}

		// Token: 0x060004F1 RID: 1265 RVA: 0x0000C864 File Offset: 0x0000AA64
		[SecuritySafeCritical]
		[RequiredByNativeCode]
		public unsafe static void InvokeMoveNext(IEnumerator enumerator, IntPtr returnValueAddress)
		{
			if (returnValueAddress == IntPtr.Zero)
			{
				throw new ArgumentException("Return value address cannot be 0.", "returnValueAddress");
			}
			*(byte*)((void*)returnValueAddress) = (enumerator.MoveNext() ? 1 : 0);
		}

		// Token: 0x060004F2 RID: 1266 RVA: 0x0000C894 File Offset: 0x0000AA94
		[RequiredByNativeCode]
		public static object InvokeMember(object behaviour, string name, object variable)
		{
			object[] args = null;
			if (variable != null)
			{
				args = new object[]
				{
					variable
				};
			}
			return behaviour.GetType().InvokeMember(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.InvokeMethod, null, behaviour, args, null, null, null);
		}

		// Token: 0x060004F3 RID: 1267 RVA: 0x0000C8D8 File Offset: 0x0000AAD8
		public static object InvokeStatic(Type klass, string name, object variable)
		{
			object[] args = null;
			if (variable != null)
			{
				args = new object[]
				{
					variable
				};
			}
			return klass.InvokeMember(name, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.InvokeMethod, null, null, args, null, null, null);
		}
	}
}
