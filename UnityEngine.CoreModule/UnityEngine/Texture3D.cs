﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200022A RID: 554
	[NativeHeader("Runtime/Graphics/Texture3D.h")]
	[ExcludeFromPreset]
	public sealed class Texture3D : Texture
	{
		// Token: 0x06001362 RID: 4962 RVA: 0x00021C05 File Offset: 0x0001FE05
		[RequiredByNativeCode]
		public Texture3D(int width, int height, int depth, GraphicsFormat format, TextureCreationFlags flags)
		{
			if (base.ValidateFormat(format, FormatUsage.Sample))
			{
				Texture3D.Internal_Create(this, width, height, depth, format, flags);
			}
		}

		// Token: 0x06001363 RID: 4963 RVA: 0x00021C2C File Offset: 0x0001FE2C
		public Texture3D(int width, int height, int depth, TextureFormat textureFormat, bool mipChain)
		{
			if (base.ValidateFormat(textureFormat))
			{
				GraphicsFormat graphicsFormat = GraphicsFormatUtility.GetGraphicsFormat(textureFormat, false);
				TextureCreationFlags textureCreationFlags = TextureCreationFlags.None;
				if (mipChain)
				{
					textureCreationFlags |= TextureCreationFlags.MipChain;
				}
				if (GraphicsFormatUtility.IsCrunchFormat(textureFormat))
				{
					textureCreationFlags |= TextureCreationFlags.Crunch;
				}
				Texture3D.Internal_Create(this, width, height, depth, graphicsFormat, textureCreationFlags);
			}
		}

		// Token: 0x170003B1 RID: 945
		// (get) Token: 0x06001364 RID: 4964
		public extern int depth { [NativeName("GetTextureLayerCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003B2 RID: 946
		// (get) Token: 0x06001365 RID: 4965
		public extern TextureFormat format { [NativeName("GetTextureFormat")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170003B3 RID: 947
		// (get) Token: 0x06001366 RID: 4966
		public override extern bool isReadable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001367 RID: 4967
		[FreeFunction("Texture3DScripting::Create")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_CreateImpl([Writable] Texture3D mono, int w, int h, int d, GraphicsFormat format, TextureCreationFlags flags);

		// Token: 0x06001368 RID: 4968 RVA: 0x00021C84 File Offset: 0x0001FE84
		private static void Internal_Create([Writable] Texture3D mono, int w, int h, int d, GraphicsFormat format, TextureCreationFlags flags)
		{
			if (!Texture3D.Internal_CreateImpl(mono, w, h, d, format, flags))
			{
				throw new UnityException("Failed to create texture because of invalid parameters.");
			}
		}

		// Token: 0x06001369 RID: 4969
		[FreeFunction(Name = "Texture3DScripting::Apply", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ApplyImpl(bool updateMipmaps, bool makeNoLongerReadable);

		// Token: 0x0600136A RID: 4970
		[FreeFunction(Name = "Texture3DScripting::GetPixels", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color[] GetPixels(int miplevel);

		// Token: 0x0600136B RID: 4971 RVA: 0x00021CA4 File Offset: 0x0001FEA4
		public Color[] GetPixels()
		{
			return this.GetPixels(0);
		}

		// Token: 0x0600136C RID: 4972
		[FreeFunction(Name = "Texture3DScripting::GetPixels32", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color32[] GetPixels32(int miplevel);

		// Token: 0x0600136D RID: 4973 RVA: 0x00021CC0 File Offset: 0x0001FEC0
		public Color32[] GetPixels32()
		{
			return this.GetPixels32(0);
		}

		// Token: 0x0600136E RID: 4974
		[FreeFunction(Name = "Texture3DScripting::SetPixels", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels(Color[] colors, int miplevel);

		// Token: 0x0600136F RID: 4975 RVA: 0x00021CDC File Offset: 0x0001FEDC
		public void SetPixels(Color[] colors)
		{
			this.SetPixels(colors, 0);
		}

		// Token: 0x06001370 RID: 4976
		[FreeFunction(Name = "Texture3DScripting::SetPixels32", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels32(Color32[] colors, int miplevel);

		// Token: 0x06001371 RID: 4977 RVA: 0x00021CE7 File Offset: 0x0001FEE7
		public void SetPixels32(Color32[] colors)
		{
			this.SetPixels32(colors, 0);
		}

		// Token: 0x06001372 RID: 4978 RVA: 0x00021CF2 File Offset: 0x0001FEF2
		public void Apply([DefaultValue("true")] bool updateMipmaps, [DefaultValue("false")] bool makeNoLongerReadable)
		{
			if (!this.isReadable)
			{
				throw base.CreateNonReadableException(this);
			}
			this.ApplyImpl(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x06001373 RID: 4979 RVA: 0x00021D10 File Offset: 0x0001FF10
		public void Apply(bool updateMipmaps)
		{
			this.Apply(updateMipmaps, false);
		}

		// Token: 0x06001374 RID: 4980 RVA: 0x00021D1B File Offset: 0x0001FF1B
		public void Apply()
		{
			this.Apply(true, false);
		}
	}
}
