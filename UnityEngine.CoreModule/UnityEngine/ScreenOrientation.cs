﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C8 RID: 200
	public enum ScreenOrientation
	{
		// Token: 0x0400022E RID: 558
		[Obsolete("Enum member Unknown has been deprecated.", false)]
		Unknown,
		// Token: 0x0400022F RID: 559
		Portrait,
		// Token: 0x04000230 RID: 560
		PortraitUpsideDown,
		// Token: 0x04000231 RID: 561
		LandscapeLeft,
		// Token: 0x04000232 RID: 562
		LandscapeRight,
		// Token: 0x04000233 RID: 563
		AutoRotation,
		// Token: 0x04000234 RID: 564
		Landscape = 3
	}
}
