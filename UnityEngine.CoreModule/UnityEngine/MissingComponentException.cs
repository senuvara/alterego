﻿using System;
using System.Runtime.Serialization;

namespace UnityEngine
{
	// Token: 0x0200021C RID: 540
	[Serializable]
	public class MissingComponentException : SystemException
	{
		// Token: 0x06001229 RID: 4649 RVA: 0x000208EF File Offset: 0x0001EAEF
		public MissingComponentException() : base("A Unity Runtime error occurred!")
		{
			base.HResult = -2147467261;
		}

		// Token: 0x0600122A RID: 4650 RVA: 0x00020908 File Offset: 0x0001EB08
		public MissingComponentException(string message) : base(message)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x0600122B RID: 4651 RVA: 0x0002091D File Offset: 0x0001EB1D
		public MissingComponentException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x0600122C RID: 4652 RVA: 0x00020933 File Offset: 0x0001EB33
		protected MissingComponentException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0400078F RID: 1935
		private const int Result = -2147467261;

		// Token: 0x04000790 RID: 1936
		private string unityStackTrace;
	}
}
