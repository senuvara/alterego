﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002A RID: 42
	public sealed class AddComponentMenu : Attribute
	{
		// Token: 0x0600027D RID: 637 RVA: 0x000089D8 File Offset: 0x00006BD8
		public AddComponentMenu(string menuName)
		{
			this.m_AddComponentMenu = menuName;
			this.m_Ordering = 0;
		}

		// Token: 0x0600027E RID: 638 RVA: 0x000089EF File Offset: 0x00006BEF
		public AddComponentMenu(string menuName, int order)
		{
			this.m_AddComponentMenu = menuName;
			this.m_Ordering = order;
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x0600027F RID: 639 RVA: 0x00008A08 File Offset: 0x00006C08
		public string componentMenu
		{
			get
			{
				return this.m_AddComponentMenu;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000280 RID: 640 RVA: 0x00008A24 File Offset: 0x00006C24
		public int componentOrder
		{
			get
			{
				return this.m_Ordering;
			}
		}

		// Token: 0x04000065 RID: 101
		private string m_AddComponentMenu;

		// Token: 0x04000066 RID: 102
		private int m_Ordering;
	}
}
