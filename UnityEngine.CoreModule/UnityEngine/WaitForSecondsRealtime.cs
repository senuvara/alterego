﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200025C RID: 604
	public class WaitForSecondsRealtime : CustomYieldInstruction
	{
		// Token: 0x06001627 RID: 5671 RVA: 0x000279FD File Offset: 0x00025BFD
		public WaitForSecondsRealtime(float time)
		{
			this.waitTime = time;
		}

		// Token: 0x17000440 RID: 1088
		// (get) Token: 0x06001628 RID: 5672 RVA: 0x00027A18 File Offset: 0x00025C18
		// (set) Token: 0x06001629 RID: 5673 RVA: 0x00027A32 File Offset: 0x00025C32
		public float waitTime
		{
			[CompilerGenerated]
			get
			{
				return this.<waitTime>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<waitTime>k__BackingField = value;
			}
		}

		// Token: 0x17000441 RID: 1089
		// (get) Token: 0x0600162A RID: 5674 RVA: 0x00027A3C File Offset: 0x00025C3C
		public override bool keepWaiting
		{
			get
			{
				if (this.m_WaitUntilTime < 0f)
				{
					this.m_WaitUntilTime = Time.realtimeSinceStartup + this.waitTime;
				}
				bool flag = Time.realtimeSinceStartup < this.m_WaitUntilTime;
				if (!flag)
				{
					this.m_WaitUntilTime = -1f;
				}
				return flag;
			}
		}

		// Token: 0x04000837 RID: 2103
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private float <waitTime>k__BackingField;

		// Token: 0x04000838 RID: 2104
		private float m_WaitUntilTime = -1f;
	}
}
