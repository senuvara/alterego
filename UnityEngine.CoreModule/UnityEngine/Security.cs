﻿using System;
using System.ComponentModel;
using System.Reflection;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000206 RID: 518
	public sealed class Security
	{
		// Token: 0x0600119F RID: 4511 RVA: 0x00002370 File Offset: 0x00000570
		public Security()
		{
		}

		// Token: 0x060011A0 RID: 4512 RVA: 0x0001DF04 File Offset: 0x0001C104
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("This was an internal method which is no longer used", true)]
		public static Assembly LoadAndVerifyAssembly(byte[] assemblyData, string authorizationKey)
		{
			return null;
		}

		// Token: 0x060011A1 RID: 4513 RVA: 0x0001DF1C File Offset: 0x0001C11C
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("This was an internal method which is no longer used", true)]
		public static Assembly LoadAndVerifyAssembly(byte[] assemblyData)
		{
			return null;
		}

		// Token: 0x060011A2 RID: 4514 RVA: 0x0001DF34 File Offset: 0x0001C134
		[ExcludeFromDocs]
		[Obsolete("Security.PrefetchSocketPolicy is no longer supported, since the Unity Web Player is no longer supported by Unity.", true)]
		public static bool PrefetchSocketPolicy(string ip, int atPort)
		{
			int timeout = 3000;
			return Security.PrefetchSocketPolicy(ip, atPort, timeout);
		}

		// Token: 0x060011A3 RID: 4515 RVA: 0x0001DF58 File Offset: 0x0001C158
		[Obsolete("Security.PrefetchSocketPolicy is no longer supported, since the Unity Web Player is no longer supported by Unity.", true)]
		public static bool PrefetchSocketPolicy(string ip, int atPort, [UnityEngine.Internal.DefaultValue("3000")] int timeout)
		{
			return false;
		}
	}
}
