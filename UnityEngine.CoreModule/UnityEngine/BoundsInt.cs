﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000044 RID: 68
	[UsedByNativeCode]
	public struct BoundsInt : IEquatable<BoundsInt>
	{
		// Token: 0x060002EE RID: 750 RVA: 0x00009480 File Offset: 0x00007680
		public BoundsInt(int xMin, int yMin, int zMin, int sizeX, int sizeY, int sizeZ)
		{
			this.m_Position = new Vector3Int(xMin, yMin, zMin);
			this.m_Size = new Vector3Int(sizeX, sizeY, sizeZ);
		}

		// Token: 0x060002EF RID: 751 RVA: 0x000094A2 File Offset: 0x000076A2
		public BoundsInt(Vector3Int position, Vector3Int size)
		{
			this.m_Position = position;
			this.m_Size = size;
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060002F0 RID: 752 RVA: 0x000094B4 File Offset: 0x000076B4
		// (set) Token: 0x060002F1 RID: 753 RVA: 0x000094D4 File Offset: 0x000076D4
		public int x
		{
			get
			{
				return this.m_Position.x;
			}
			set
			{
				this.m_Position.x = value;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060002F2 RID: 754 RVA: 0x000094E4 File Offset: 0x000076E4
		// (set) Token: 0x060002F3 RID: 755 RVA: 0x00009504 File Offset: 0x00007704
		public int y
		{
			get
			{
				return this.m_Position.y;
			}
			set
			{
				this.m_Position.y = value;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060002F4 RID: 756 RVA: 0x00009514 File Offset: 0x00007714
		// (set) Token: 0x060002F5 RID: 757 RVA: 0x00009534 File Offset: 0x00007734
		public int z
		{
			get
			{
				return this.m_Position.z;
			}
			set
			{
				this.m_Position.z = value;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060002F6 RID: 758 RVA: 0x00009544 File Offset: 0x00007744
		public Vector3 center
		{
			get
			{
				return new Vector3((float)this.x + (float)this.m_Size.x / 2f, (float)this.y + (float)this.m_Size.y / 2f, (float)this.z + (float)this.m_Size.z / 2f);
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060002F7 RID: 759 RVA: 0x000095AC File Offset: 0x000077AC
		// (set) Token: 0x060002F8 RID: 760 RVA: 0x000095D8 File Offset: 0x000077D8
		public Vector3Int min
		{
			get
			{
				return new Vector3Int(this.xMin, this.yMin, this.zMin);
			}
			set
			{
				this.xMin = value.x;
				this.yMin = value.y;
				this.zMin = value.z;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060002F9 RID: 761 RVA: 0x00009604 File Offset: 0x00007804
		// (set) Token: 0x060002FA RID: 762 RVA: 0x00009630 File Offset: 0x00007830
		public Vector3Int max
		{
			get
			{
				return new Vector3Int(this.xMax, this.yMax, this.zMax);
			}
			set
			{
				this.xMax = value.x;
				this.yMax = value.y;
				this.zMax = value.z;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060002FB RID: 763 RVA: 0x0000965C File Offset: 0x0000785C
		// (set) Token: 0x060002FC RID: 764 RVA: 0x00009698 File Offset: 0x00007898
		public int xMin
		{
			get
			{
				return Math.Min(this.m_Position.x, this.m_Position.x + this.m_Size.x);
			}
			set
			{
				int xMax = this.xMax;
				this.m_Position.x = value;
				this.m_Size.x = xMax - this.m_Position.x;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x060002FD RID: 765 RVA: 0x000096D4 File Offset: 0x000078D4
		// (set) Token: 0x060002FE RID: 766 RVA: 0x00009710 File Offset: 0x00007910
		public int yMin
		{
			get
			{
				return Math.Min(this.m_Position.y, this.m_Position.y + this.m_Size.y);
			}
			set
			{
				int yMax = this.yMax;
				this.m_Position.y = value;
				this.m_Size.y = yMax - this.m_Position.y;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060002FF RID: 767 RVA: 0x0000974C File Offset: 0x0000794C
		// (set) Token: 0x06000300 RID: 768 RVA: 0x00009788 File Offset: 0x00007988
		public int zMin
		{
			get
			{
				return Math.Min(this.m_Position.z, this.m_Position.z + this.m_Size.z);
			}
			set
			{
				int zMax = this.zMax;
				this.m_Position.z = value;
				this.m_Size.z = zMax - this.m_Position.z;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000301 RID: 769 RVA: 0x000097C4 File Offset: 0x000079C4
		// (set) Token: 0x06000302 RID: 770 RVA: 0x00009800 File Offset: 0x00007A00
		public int xMax
		{
			get
			{
				return Math.Max(this.m_Position.x, this.m_Position.x + this.m_Size.x);
			}
			set
			{
				this.m_Size.x = value - this.m_Position.x;
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000303 RID: 771 RVA: 0x0000981C File Offset: 0x00007A1C
		// (set) Token: 0x06000304 RID: 772 RVA: 0x00009858 File Offset: 0x00007A58
		public int yMax
		{
			get
			{
				return Math.Max(this.m_Position.y, this.m_Position.y + this.m_Size.y);
			}
			set
			{
				this.m_Size.y = value - this.m_Position.y;
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000305 RID: 773 RVA: 0x00009874 File Offset: 0x00007A74
		// (set) Token: 0x06000306 RID: 774 RVA: 0x000098B0 File Offset: 0x00007AB0
		public int zMax
		{
			get
			{
				return Math.Max(this.m_Position.z, this.m_Position.z + this.m_Size.z);
			}
			set
			{
				this.m_Size.z = value - this.m_Position.z;
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000307 RID: 775 RVA: 0x000098CC File Offset: 0x00007ACC
		// (set) Token: 0x06000308 RID: 776 RVA: 0x000098E7 File Offset: 0x00007AE7
		public Vector3Int position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				this.m_Position = value;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000309 RID: 777 RVA: 0x000098F4 File Offset: 0x00007AF4
		// (set) Token: 0x0600030A RID: 778 RVA: 0x0000990F File Offset: 0x00007B0F
		public Vector3Int size
		{
			get
			{
				return this.m_Size;
			}
			set
			{
				this.m_Size = value;
			}
		}

		// Token: 0x0600030B RID: 779 RVA: 0x00009919 File Offset: 0x00007B19
		public void SetMinMax(Vector3Int minPosition, Vector3Int maxPosition)
		{
			this.min = minPosition;
			this.max = maxPosition;
		}

		// Token: 0x0600030C RID: 780 RVA: 0x0000992C File Offset: 0x00007B2C
		public void ClampToBounds(BoundsInt bounds)
		{
			this.position = new Vector3Int(Math.Max(Math.Min(bounds.xMax, this.position.x), bounds.xMin), Math.Max(Math.Min(bounds.yMax, this.position.y), bounds.yMin), Math.Max(Math.Min(bounds.zMax, this.position.z), bounds.zMin));
			this.size = new Vector3Int(Math.Min(bounds.xMax - this.position.x, this.size.x), Math.Min(bounds.yMax - this.position.y, this.size.y), Math.Min(bounds.zMax - this.position.z, this.size.z));
		}

		// Token: 0x0600030D RID: 781 RVA: 0x00009A44 File Offset: 0x00007C44
		public bool Contains(Vector3Int position)
		{
			return position.x >= this.xMin && position.y >= this.yMin && position.z >= this.zMin && position.x < this.xMax && position.y < this.yMax && position.z < this.zMax;
		}

		// Token: 0x0600030E RID: 782 RVA: 0x00009AC8 File Offset: 0x00007CC8
		public override string ToString()
		{
			return UnityString.Format("Position: {0}, Size: {1}", new object[]
			{
				this.m_Position,
				this.m_Size
			});
		}

		// Token: 0x0600030F RID: 783 RVA: 0x00009B0C File Offset: 0x00007D0C
		public static bool operator ==(BoundsInt lhs, BoundsInt rhs)
		{
			return lhs.m_Position == rhs.m_Position && lhs.m_Size == rhs.m_Size;
		}

		// Token: 0x06000310 RID: 784 RVA: 0x00009B50 File Offset: 0x00007D50
		public static bool operator !=(BoundsInt lhs, BoundsInt rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x06000311 RID: 785 RVA: 0x00009B70 File Offset: 0x00007D70
		public override bool Equals(object other)
		{
			return other is BoundsInt && this.Equals((BoundsInt)other);
		}

		// Token: 0x06000312 RID: 786 RVA: 0x00009BA4 File Offset: 0x00007DA4
		public bool Equals(BoundsInt other)
		{
			return this.m_Position.Equals(other.m_Position) && this.m_Size.Equals(other.m_Size);
		}

		// Token: 0x06000313 RID: 787 RVA: 0x00009BE8 File Offset: 0x00007DE8
		public override int GetHashCode()
		{
			return this.m_Position.GetHashCode() ^ this.m_Size.GetHashCode() << 2;
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000314 RID: 788 RVA: 0x00009C24 File Offset: 0x00007E24
		public BoundsInt.PositionEnumerator allPositionsWithin
		{
			get
			{
				return new BoundsInt.PositionEnumerator(this.min, this.max);
			}
		}

		// Token: 0x040000EB RID: 235
		private Vector3Int m_Position;

		// Token: 0x040000EC RID: 236
		private Vector3Int m_Size;

		// Token: 0x02000045 RID: 69
		public struct PositionEnumerator : IEnumerator<Vector3Int>, IEnumerator, IDisposable
		{
			// Token: 0x06000315 RID: 789 RVA: 0x00009C4C File Offset: 0x00007E4C
			public PositionEnumerator(Vector3Int min, Vector3Int max)
			{
				this._current = min;
				this._min = min;
				this._max = max;
				this.Reset();
			}

			// Token: 0x06000316 RID: 790 RVA: 0x00009C78 File Offset: 0x00007E78
			public BoundsInt.PositionEnumerator GetEnumerator()
			{
				return this;
			}

			// Token: 0x06000317 RID: 791 RVA: 0x00009C94 File Offset: 0x00007E94
			public bool MoveNext()
			{
				bool result;
				if (this._current.z >= this._max.z)
				{
					result = false;
				}
				else
				{
					this._current.x = this._current.x + 1;
					if (this._current.x >= this._max.x)
					{
						this._current.x = this._min.x;
						this._current.y = this._current.y + 1;
						if (this._current.y >= this._max.y)
						{
							this._current.y = this._min.y;
							this._current.z = this._current.z + 1;
							if (this._current.z >= this._max.z)
							{
								return false;
							}
						}
					}
					result = true;
				}
				return result;
			}

			// Token: 0x06000318 RID: 792 RVA: 0x00009DA2 File Offset: 0x00007FA2
			public void Reset()
			{
				this._current = this._min;
				this._current.x = this._current.x - 1;
			}

			// Token: 0x17000074 RID: 116
			// (get) Token: 0x06000319 RID: 793 RVA: 0x00009DC4 File Offset: 0x00007FC4
			public Vector3Int Current
			{
				get
				{
					return this._current;
				}
			}

			// Token: 0x17000073 RID: 115
			// (get) Token: 0x0600031A RID: 794 RVA: 0x00009DE0 File Offset: 0x00007FE0
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x0600031B RID: 795 RVA: 0x00007476 File Offset: 0x00005676
			void IDisposable.Dispose()
			{
			}

			// Token: 0x040000ED RID: 237
			private readonly Vector3Int _min;

			// Token: 0x040000EE RID: 238
			private readonly Vector3Int _max;

			// Token: 0x040000EF RID: 239
			private Vector3Int _current;
		}
	}
}
