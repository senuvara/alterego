﻿using System;

namespace UnityEngine
{
	// Token: 0x02000221 RID: 545
	public enum OperatingSystemFamily
	{
		// Token: 0x0400079D RID: 1949
		Other,
		// Token: 0x0400079E RID: 1950
		MacOSX,
		// Token: 0x0400079F RID: 1951
		Windows,
		// Token: 0x040007A0 RID: 1952
		Linux
	}
}
