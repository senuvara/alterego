﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200012D RID: 301
	[NativeHeader("Runtime/Input/InputBindings.h")]
	public class Input
	{
		// Token: 0x06000C75 RID: 3189 RVA: 0x00002370 File Offset: 0x00000570
		public Input()
		{
		}

		// Token: 0x06000C76 RID: 3190
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyInt(KeyCode key);

		// Token: 0x06000C77 RID: 3191
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyString(string name);

		// Token: 0x06000C78 RID: 3192
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyUpInt(KeyCode key);

		// Token: 0x06000C79 RID: 3193
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyUpString(string name);

		// Token: 0x06000C7A RID: 3194
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyDownInt(KeyCode key);

		// Token: 0x06000C7B RID: 3195
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyDownString(string name);

		// Token: 0x06000C7C RID: 3196
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetAxis(string axisName);

		// Token: 0x06000C7D RID: 3197
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetAxisRaw(string axisName);

		// Token: 0x06000C7E RID: 3198
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetButton(string buttonName);

		// Token: 0x06000C7F RID: 3199
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetButtonDown(string buttonName);

		// Token: 0x06000C80 RID: 3200
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetButtonUp(string buttonName);

		// Token: 0x06000C81 RID: 3201
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetMouseButton(int button);

		// Token: 0x06000C82 RID: 3202
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetMouseButtonDown(int button);

		// Token: 0x06000C83 RID: 3203
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetMouseButtonUp(int button);

		// Token: 0x06000C84 RID: 3204
		[FreeFunction("ResetInput")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ResetInputAxes();

		// Token: 0x06000C85 RID: 3205
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string[] GetJoystickNames();

		// Token: 0x06000C86 RID: 3206 RVA: 0x00013E48 File Offset: 0x00012048
		[NativeThrows]
		public static Touch GetTouch(int index)
		{
			Touch result;
			Input.GetTouch_Injected(index, out result);
			return result;
		}

		// Token: 0x06000C87 RID: 3207 RVA: 0x00013E60 File Offset: 0x00012060
		[NativeThrows]
		public static AccelerationEvent GetAccelerationEvent(int index)
		{
			AccelerationEvent result;
			Input.GetAccelerationEvent_Injected(index, out result);
			return result;
		}

		// Token: 0x06000C88 RID: 3208 RVA: 0x00013E78 File Offset: 0x00012078
		public static bool GetKey(KeyCode key)
		{
			return Input.GetKeyInt(key);
		}

		// Token: 0x06000C89 RID: 3209 RVA: 0x00013E94 File Offset: 0x00012094
		public static bool GetKey(string name)
		{
			return Input.GetKeyString(name);
		}

		// Token: 0x06000C8A RID: 3210 RVA: 0x00013EB0 File Offset: 0x000120B0
		public static bool GetKeyUp(KeyCode key)
		{
			return Input.GetKeyUpInt(key);
		}

		// Token: 0x06000C8B RID: 3211 RVA: 0x00013ECC File Offset: 0x000120CC
		public static bool GetKeyUp(string name)
		{
			return Input.GetKeyUpString(name);
		}

		// Token: 0x06000C8C RID: 3212 RVA: 0x00013EE8 File Offset: 0x000120E8
		public static bool GetKeyDown(KeyCode key)
		{
			return Input.GetKeyDownInt(key);
		}

		// Token: 0x06000C8D RID: 3213 RVA: 0x00013F04 File Offset: 0x00012104
		public static bool GetKeyDown(string name)
		{
			return Input.GetKeyDownString(name);
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x06000C8E RID: 3214
		// (set) Token: 0x06000C8F RID: 3215
		public static extern bool simulateMouseWithTouches { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700026D RID: 621
		// (get) Token: 0x06000C90 RID: 3216
		public static extern bool anyKey { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700026E RID: 622
		// (get) Token: 0x06000C91 RID: 3217
		public static extern bool anyKeyDown { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700026F RID: 623
		// (get) Token: 0x06000C92 RID: 3218
		public static extern string inputString { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000270 RID: 624
		// (get) Token: 0x06000C93 RID: 3219 RVA: 0x00013F20 File Offset: 0x00012120
		public static Vector3 mousePosition
		{
			get
			{
				Vector3 result;
				Input.get_mousePosition_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000271 RID: 625
		// (get) Token: 0x06000C94 RID: 3220 RVA: 0x00013F38 File Offset: 0x00012138
		public static Vector2 mouseScrollDelta
		{
			get
			{
				Vector2 result;
				Input.get_mouseScrollDelta_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000272 RID: 626
		// (get) Token: 0x06000C95 RID: 3221
		// (set) Token: 0x06000C96 RID: 3222
		public static extern IMECompositionMode imeCompositionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000273 RID: 627
		// (get) Token: 0x06000C97 RID: 3223
		public static extern string compositionString { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06000C98 RID: 3224
		public static extern bool imeIsSelected { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06000C99 RID: 3225 RVA: 0x00013F50 File Offset: 0x00012150
		// (set) Token: 0x06000C9A RID: 3226 RVA: 0x00013F65 File Offset: 0x00012165
		public static Vector2 compositionCursorPos
		{
			get
			{
				Vector2 result;
				Input.get_compositionCursorPos_Injected(out result);
				return result;
			}
			set
			{
				Input.set_compositionCursorPos_Injected(ref value);
			}
		}

		// Token: 0x17000276 RID: 630
		// (get) Token: 0x06000C9B RID: 3227
		// (set) Token: 0x06000C9C RID: 3228
		[Obsolete("eatKeyPressOnTextFieldFocus property is deprecated, and only provided to support legacy behavior.")]
		public static extern bool eatKeyPressOnTextFieldFocus { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x06000C9D RID: 3229
		public static extern bool mousePresent { [FreeFunction("GetMousePresent")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000278 RID: 632
		// (get) Token: 0x06000C9E RID: 3230
		public static extern int touchCount { [FreeFunction("GetTouchCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000279 RID: 633
		// (get) Token: 0x06000C9F RID: 3231
		public static extern bool touchPressureSupported { [FreeFunction("IsTouchPressureSupported")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700027A RID: 634
		// (get) Token: 0x06000CA0 RID: 3232
		public static extern bool stylusTouchSupported { [FreeFunction("IsStylusTouchSupported")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700027B RID: 635
		// (get) Token: 0x06000CA1 RID: 3233
		public static extern bool touchSupported { [FreeFunction("IsTouchSupported")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700027C RID: 636
		// (get) Token: 0x06000CA2 RID: 3234
		// (set) Token: 0x06000CA3 RID: 3235
		public static extern bool multiTouchEnabled { [FreeFunction("IsMultiTouchEnabled")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("SetMultiTouchEnabled")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700027D RID: 637
		// (get) Token: 0x06000CA4 RID: 3236
		[Obsolete("isGyroAvailable property is deprecated. Please use SystemInfo.supportsGyroscope instead.")]
		public static extern bool isGyroAvailable { [FreeFunction("IsGyroAvailable")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700027E RID: 638
		// (get) Token: 0x06000CA5 RID: 3237
		public static extern DeviceOrientation deviceOrientation { [FreeFunction("GetOrientation")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700027F RID: 639
		// (get) Token: 0x06000CA6 RID: 3238 RVA: 0x00013F70 File Offset: 0x00012170
		public static Vector3 acceleration
		{
			[FreeFunction("GetAcceleration")]
			get
			{
				Vector3 result;
				Input.get_acceleration_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000280 RID: 640
		// (get) Token: 0x06000CA7 RID: 3239
		// (set) Token: 0x06000CA8 RID: 3240
		public static extern bool compensateSensors { [FreeFunction("IsCompensatingSensors")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("SetCompensatingSensors")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000281 RID: 641
		// (get) Token: 0x06000CA9 RID: 3241
		public static extern int accelerationEventCount { [FreeFunction("GetAccelerationCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000282 RID: 642
		// (get) Token: 0x06000CAA RID: 3242
		// (set) Token: 0x06000CAB RID: 3243
		public static extern bool backButtonLeavesApp { [FreeFunction("GetBackButtonLeavesApp")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("SetBackButtonLeavesApp")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000283 RID: 643
		// (get) Token: 0x06000CAC RID: 3244 RVA: 0x00013F88 File Offset: 0x00012188
		public static LocationService location
		{
			get
			{
				if (Input.locationServiceInstance == null)
				{
					Input.locationServiceInstance = new LocationService();
				}
				return Input.locationServiceInstance;
			}
		}

		// Token: 0x17000284 RID: 644
		// (get) Token: 0x06000CAD RID: 3245 RVA: 0x00013FB8 File Offset: 0x000121B8
		public static Compass compass
		{
			get
			{
				if (Input.compassInstance == null)
				{
					Input.compassInstance = new Compass();
				}
				return Input.compassInstance;
			}
		}

		// Token: 0x06000CAE RID: 3246
		[FreeFunction("GetGyro")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetGyroInternal();

		// Token: 0x17000285 RID: 645
		// (get) Token: 0x06000CAF RID: 3247 RVA: 0x00013FE8 File Offset: 0x000121E8
		public static Gyroscope gyro
		{
			get
			{
				if (Input.s_MainGyro == null)
				{
					Input.s_MainGyro = new Gyroscope(Input.GetGyroInternal());
				}
				return Input.s_MainGyro;
			}
		}

		// Token: 0x17000286 RID: 646
		// (get) Token: 0x06000CB0 RID: 3248 RVA: 0x0001401C File Offset: 0x0001221C
		public static Touch[] touches
		{
			get
			{
				int touchCount = Input.touchCount;
				Touch[] array = new Touch[touchCount];
				for (int i = 0; i < touchCount; i++)
				{
					array[i] = Input.GetTouch(i);
				}
				return array;
			}
		}

		// Token: 0x17000287 RID: 647
		// (get) Token: 0x06000CB1 RID: 3249 RVA: 0x00014064 File Offset: 0x00012264
		public static AccelerationEvent[] accelerationEvents
		{
			get
			{
				int accelerationEventCount = Input.accelerationEventCount;
				AccelerationEvent[] array = new AccelerationEvent[accelerationEventCount];
				for (int i = 0; i < accelerationEventCount; i++)
				{
					array[i] = Input.GetAccelerationEvent(i);
				}
				return array;
			}
		}

		// Token: 0x06000CB2 RID: 3250
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetTouch_Injected(int index, out Touch ret);

		// Token: 0x06000CB3 RID: 3251
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetAccelerationEvent_Injected(int index, out AccelerationEvent ret);

		// Token: 0x06000CB4 RID: 3252
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_mousePosition_Injected(out Vector3 ret);

		// Token: 0x06000CB5 RID: 3253
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_mouseScrollDelta_Injected(out Vector2 ret);

		// Token: 0x06000CB6 RID: 3254
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_compositionCursorPos_Injected(out Vector2 ret);

		// Token: 0x06000CB7 RID: 3255
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_compositionCursorPos_Injected(ref Vector2 value);

		// Token: 0x06000CB8 RID: 3256
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_acceleration_Injected(out Vector3 ret);

		// Token: 0x04000550 RID: 1360
		private static LocationService locationServiceInstance;

		// Token: 0x04000551 RID: 1361
		private static Compass compassInstance;

		// Token: 0x04000552 RID: 1362
		private static Gyroscope s_MainGyro;
	}
}
