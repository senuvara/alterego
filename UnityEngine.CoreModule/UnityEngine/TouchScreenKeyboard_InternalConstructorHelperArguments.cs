﻿using System;

namespace UnityEngine
{
	// Token: 0x02000116 RID: 278
	internal struct TouchScreenKeyboard_InternalConstructorHelperArguments
	{
		// Token: 0x040004F4 RID: 1268
		public uint keyboardType;

		// Token: 0x040004F5 RID: 1269
		public uint autocorrection;

		// Token: 0x040004F6 RID: 1270
		public uint multiline;

		// Token: 0x040004F7 RID: 1271
		public uint secure;

		// Token: 0x040004F8 RID: 1272
		public uint alert;

		// Token: 0x040004F9 RID: 1273
		public int characterLimit;
	}
}
