﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000256 RID: 598
	[UsedByNativeCode]
	public struct Vector2Int : IEquatable<Vector2Int>
	{
		// Token: 0x060015A7 RID: 5543 RVA: 0x00025E65 File Offset: 0x00024065
		public Vector2Int(int x, int y)
		{
			this.m_X = x;
			this.m_Y = y;
		}

		// Token: 0x17000421 RID: 1057
		// (get) Token: 0x060015A8 RID: 5544 RVA: 0x00025E78 File Offset: 0x00024078
		// (set) Token: 0x060015A9 RID: 5545 RVA: 0x00025E93 File Offset: 0x00024093
		public int x
		{
			get
			{
				return this.m_X;
			}
			set
			{
				this.m_X = value;
			}
		}

		// Token: 0x17000422 RID: 1058
		// (get) Token: 0x060015AA RID: 5546 RVA: 0x00025EA0 File Offset: 0x000240A0
		// (set) Token: 0x060015AB RID: 5547 RVA: 0x00025EBB File Offset: 0x000240BB
		public int y
		{
			get
			{
				return this.m_Y;
			}
			set
			{
				this.m_Y = value;
			}
		}

		// Token: 0x060015AC RID: 5548 RVA: 0x00025E65 File Offset: 0x00024065
		public void Set(int x, int y)
		{
			this.m_X = x;
			this.m_Y = y;
		}

		// Token: 0x17000423 RID: 1059
		public int this[int index]
		{
			get
			{
				int result;
				if (index != 0)
				{
					if (index != 1)
					{
						throw new IndexOutOfRangeException(string.Format("Invalid Vector2Int index addressed: {0}!", index));
					}
					result = this.y;
				}
				else
				{
					result = this.x;
				}
				return result;
			}
			set
			{
				if (index != 0)
				{
					if (index != 1)
					{
						throw new IndexOutOfRangeException(string.Format("Invalid Vector2Int index addressed: {0}!", index));
					}
					this.y = value;
				}
				else
				{
					this.x = value;
				}
			}
		}

		// Token: 0x17000424 RID: 1060
		// (get) Token: 0x060015AF RID: 5551 RVA: 0x00025F68 File Offset: 0x00024168
		public float magnitude
		{
			get
			{
				return Mathf.Sqrt((float)(this.x * this.x + this.y * this.y));
			}
		}

		// Token: 0x17000425 RID: 1061
		// (get) Token: 0x060015B0 RID: 5552 RVA: 0x00025FA0 File Offset: 0x000241A0
		public int sqrMagnitude
		{
			get
			{
				return this.x * this.x + this.y * this.y;
			}
		}

		// Token: 0x060015B1 RID: 5553 RVA: 0x00025FD0 File Offset: 0x000241D0
		public static float Distance(Vector2Int a, Vector2Int b)
		{
			return (a - b).magnitude;
		}

		// Token: 0x060015B2 RID: 5554 RVA: 0x00025FF4 File Offset: 0x000241F4
		public static Vector2Int Min(Vector2Int lhs, Vector2Int rhs)
		{
			return new Vector2Int(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y));
		}

		// Token: 0x060015B3 RID: 5555 RVA: 0x00026034 File Offset: 0x00024234
		public static Vector2Int Max(Vector2Int lhs, Vector2Int rhs)
		{
			return new Vector2Int(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y));
		}

		// Token: 0x060015B4 RID: 5556 RVA: 0x00026074 File Offset: 0x00024274
		public static Vector2Int Scale(Vector2Int a, Vector2Int b)
		{
			return new Vector2Int(a.x * b.x, a.y * b.y);
		}

		// Token: 0x060015B5 RID: 5557 RVA: 0x000260AC File Offset: 0x000242AC
		public void Scale(Vector2Int scale)
		{
			this.x *= scale.x;
			this.y *= scale.y;
		}

		// Token: 0x060015B6 RID: 5558 RVA: 0x000260D8 File Offset: 0x000242D8
		public void Clamp(Vector2Int min, Vector2Int max)
		{
			this.x = Math.Max(min.x, this.x);
			this.x = Math.Min(max.x, this.x);
			this.y = Math.Max(min.y, this.y);
			this.y = Math.Min(max.y, this.y);
		}

		// Token: 0x060015B7 RID: 5559 RVA: 0x00026148 File Offset: 0x00024348
		public static implicit operator Vector2(Vector2Int v)
		{
			return new Vector2((float)v.x, (float)v.y);
		}

		// Token: 0x060015B8 RID: 5560 RVA: 0x00026174 File Offset: 0x00024374
		public static explicit operator Vector3Int(Vector2Int v)
		{
			return new Vector3Int(v.x, v.y, 0);
		}

		// Token: 0x060015B9 RID: 5561 RVA: 0x000261A0 File Offset: 0x000243A0
		public static Vector2Int FloorToInt(Vector2 v)
		{
			return new Vector2Int(Mathf.FloorToInt(v.x), Mathf.FloorToInt(v.y));
		}

		// Token: 0x060015BA RID: 5562 RVA: 0x000261D4 File Offset: 0x000243D4
		public static Vector2Int CeilToInt(Vector2 v)
		{
			return new Vector2Int(Mathf.CeilToInt(v.x), Mathf.CeilToInt(v.y));
		}

		// Token: 0x060015BB RID: 5563 RVA: 0x00026208 File Offset: 0x00024408
		public static Vector2Int RoundToInt(Vector2 v)
		{
			return new Vector2Int(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y));
		}

		// Token: 0x060015BC RID: 5564 RVA: 0x0002623C File Offset: 0x0002443C
		public static Vector2Int operator +(Vector2Int a, Vector2Int b)
		{
			return new Vector2Int(a.x + b.x, a.y + b.y);
		}

		// Token: 0x060015BD RID: 5565 RVA: 0x00026274 File Offset: 0x00024474
		public static Vector2Int operator -(Vector2Int a, Vector2Int b)
		{
			return new Vector2Int(a.x - b.x, a.y - b.y);
		}

		// Token: 0x060015BE RID: 5566 RVA: 0x000262AC File Offset: 0x000244AC
		public static Vector2Int operator *(Vector2Int a, Vector2Int b)
		{
			return new Vector2Int(a.x * b.x, a.y * b.y);
		}

		// Token: 0x060015BF RID: 5567 RVA: 0x000262E4 File Offset: 0x000244E4
		public static Vector2Int operator *(Vector2Int a, int b)
		{
			return new Vector2Int(a.x * b, a.y * b);
		}

		// Token: 0x060015C0 RID: 5568 RVA: 0x00026310 File Offset: 0x00024510
		public static bool operator ==(Vector2Int lhs, Vector2Int rhs)
		{
			return lhs.x == rhs.x && lhs.y == rhs.y;
		}

		// Token: 0x060015C1 RID: 5569 RVA: 0x0002634C File Offset: 0x0002454C
		public static bool operator !=(Vector2Int lhs, Vector2Int rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x060015C2 RID: 5570 RVA: 0x0002636C File Offset: 0x0002456C
		public override bool Equals(object other)
		{
			return other is Vector2Int && this.Equals((Vector2Int)other);
		}

		// Token: 0x060015C3 RID: 5571 RVA: 0x000263A0 File Offset: 0x000245A0
		public bool Equals(Vector2Int other)
		{
			return this.x.Equals(other.x) && this.y.Equals(other.y);
		}

		// Token: 0x060015C4 RID: 5572 RVA: 0x000263E8 File Offset: 0x000245E8
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.y.GetHashCode() << 2;
		}

		// Token: 0x060015C5 RID: 5573 RVA: 0x00026428 File Offset: 0x00024628
		public override string ToString()
		{
			return UnityString.Format("({0}, {1})", new object[]
			{
				this.x,
				this.y
			});
		}

		// Token: 0x17000426 RID: 1062
		// (get) Token: 0x060015C6 RID: 5574 RVA: 0x0002646C File Offset: 0x0002466C
		public static Vector2Int zero
		{
			get
			{
				return Vector2Int.s_Zero;
			}
		}

		// Token: 0x17000427 RID: 1063
		// (get) Token: 0x060015C7 RID: 5575 RVA: 0x00026488 File Offset: 0x00024688
		public static Vector2Int one
		{
			get
			{
				return Vector2Int.s_One;
			}
		}

		// Token: 0x17000428 RID: 1064
		// (get) Token: 0x060015C8 RID: 5576 RVA: 0x000264A4 File Offset: 0x000246A4
		public static Vector2Int up
		{
			get
			{
				return Vector2Int.s_Up;
			}
		}

		// Token: 0x17000429 RID: 1065
		// (get) Token: 0x060015C9 RID: 5577 RVA: 0x000264C0 File Offset: 0x000246C0
		public static Vector2Int down
		{
			get
			{
				return Vector2Int.s_Down;
			}
		}

		// Token: 0x1700042A RID: 1066
		// (get) Token: 0x060015CA RID: 5578 RVA: 0x000264DC File Offset: 0x000246DC
		public static Vector2Int left
		{
			get
			{
				return Vector2Int.s_Left;
			}
		}

		// Token: 0x1700042B RID: 1067
		// (get) Token: 0x060015CB RID: 5579 RVA: 0x000264F8 File Offset: 0x000246F8
		public static Vector2Int right
		{
			get
			{
				return Vector2Int.s_Right;
			}
		}

		// Token: 0x060015CC RID: 5580 RVA: 0x00026514 File Offset: 0x00024714
		// Note: this type is marked as 'beforefieldinit'.
		static Vector2Int()
		{
		}

		// Token: 0x0400081C RID: 2076
		private int m_X;

		// Token: 0x0400081D RID: 2077
		private int m_Y;

		// Token: 0x0400081E RID: 2078
		private static readonly Vector2Int s_Zero = new Vector2Int(0, 0);

		// Token: 0x0400081F RID: 2079
		private static readonly Vector2Int s_One = new Vector2Int(1, 1);

		// Token: 0x04000820 RID: 2080
		private static readonly Vector2Int s_Up = new Vector2Int(0, 1);

		// Token: 0x04000821 RID: 2081
		private static readonly Vector2Int s_Down = new Vector2Int(0, -1);

		// Token: 0x04000822 RID: 2082
		private static readonly Vector2Int s_Left = new Vector2Int(-1, 0);

		// Token: 0x04000823 RID: 2083
		private static readonly Vector2Int s_Right = new Vector2Int(1, 0);
	}
}
