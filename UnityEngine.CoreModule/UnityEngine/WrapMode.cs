﻿using System;

namespace UnityEngine
{
	// Token: 0x02000013 RID: 19
	public enum WrapMode
	{
		// Token: 0x0400002F RID: 47
		Once = 1,
		// Token: 0x04000030 RID: 48
		Loop,
		// Token: 0x04000031 RID: 49
		PingPong = 4,
		// Token: 0x04000032 RID: 50
		Default = 0,
		// Token: 0x04000033 RID: 51
		ClampForever = 8,
		// Token: 0x04000034 RID: 52
		Clamp = 1
	}
}
