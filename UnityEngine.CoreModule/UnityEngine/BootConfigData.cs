﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000042 RID: 66
	[NativeHeader("Runtime/Export/BootConfig.bindings.h")]
	internal class BootConfigData
	{
		// Token: 0x060002C4 RID: 708 RVA: 0x00008EF5 File Offset: 0x000070F5
		private BootConfigData(IntPtr nativeHandle)
		{
			if (nativeHandle == IntPtr.Zero)
			{
				throw new ArgumentException("native handle can not be null");
			}
			this.m_Ptr = nativeHandle;
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x00008F20 File Offset: 0x00007120
		public void AddKey(string key)
		{
			this.Append(key, null);
		}

		// Token: 0x060002C6 RID: 710 RVA: 0x00008F2C File Offset: 0x0000712C
		public string Get(string key)
		{
			return this.GetValue(key, 0);
		}

		// Token: 0x060002C7 RID: 711 RVA: 0x00008F4C File Offset: 0x0000714C
		public string Get(string key, int index)
		{
			return this.GetValue(key, index);
		}

		// Token: 0x060002C8 RID: 712
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Append(string key, string value);

		// Token: 0x060002C9 RID: 713
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Set(string key, string value);

		// Token: 0x060002CA RID: 714
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string GetValue(string key, int index);

		// Token: 0x060002CB RID: 715 RVA: 0x00008F6C File Offset: 0x0000716C
		[RequiredByNativeCode]
		private static BootConfigData WrapBootConfigData(IntPtr nativeHandle)
		{
			return new BootConfigData(nativeHandle);
		}

		// Token: 0x040000E8 RID: 232
		private IntPtr m_Ptr;
	}
}
