﻿using System;

namespace UnityEngine
{
	// Token: 0x02000051 RID: 81
	internal struct CastHelper<T>
	{
		// Token: 0x0400010C RID: 268
		public T t;

		// Token: 0x0400010D RID: 269
		public IntPtr onePointerFurtherThanT;
	}
}
