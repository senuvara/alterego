﻿using System;
using System.Runtime.Serialization;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200021B RID: 539
	[RequiredByNativeCode]
	[Serializable]
	public class UnityException : SystemException
	{
		// Token: 0x06001225 RID: 4645 RVA: 0x000208EF File Offset: 0x0001EAEF
		public UnityException() : base("A Unity Runtime error occurred!")
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06001226 RID: 4646 RVA: 0x00020908 File Offset: 0x0001EB08
		public UnityException(string message) : base(message)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06001227 RID: 4647 RVA: 0x0002091D File Offset: 0x0001EB1D
		public UnityException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06001228 RID: 4648 RVA: 0x00020933 File Offset: 0x0001EB33
		protected UnityException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0400078D RID: 1933
		private const int Result = -2147467261;

		// Token: 0x0400078E RID: 1934
		private string unityStackTrace;
	}
}
