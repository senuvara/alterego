﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000158 RID: 344
	[NativeHeader("Runtime/Mono/MonoBehaviour.h")]
	[NativeHeader("Runtime/Scripting/DelayedCallUtility.h")]
	[ExtensionOfNativeClass]
	[RequiredByNativeCode]
	public class MonoBehaviour : Behaviour
	{
		// Token: 0x06000EE7 RID: 3815 RVA: 0x0000A52A File Offset: 0x0000872A
		public MonoBehaviour()
		{
		}

		// Token: 0x06000EE8 RID: 3816 RVA: 0x00019774 File Offset: 0x00017974
		public bool IsInvoking()
		{
			return MonoBehaviour.Internal_IsInvokingAll(this);
		}

		// Token: 0x06000EE9 RID: 3817 RVA: 0x0001978F File Offset: 0x0001798F
		public void CancelInvoke()
		{
			MonoBehaviour.Internal_CancelInvokeAll(this);
		}

		// Token: 0x06000EEA RID: 3818 RVA: 0x00019798 File Offset: 0x00017998
		public void Invoke(string methodName, float time)
		{
			MonoBehaviour.InvokeDelayed(this, methodName, time, 0f);
		}

		// Token: 0x06000EEB RID: 3819 RVA: 0x000197A8 File Offset: 0x000179A8
		public void InvokeRepeating(string methodName, float time, float repeatRate)
		{
			if (repeatRate <= 1E-05f && repeatRate != 0f)
			{
				throw new UnityException("Invoke repeat rate has to be larger than 0.00001F)");
			}
			MonoBehaviour.InvokeDelayed(this, methodName, time, repeatRate);
		}

		// Token: 0x06000EEC RID: 3820 RVA: 0x000197D5 File Offset: 0x000179D5
		public void CancelInvoke(string methodName)
		{
			MonoBehaviour.CancelInvoke(this, methodName);
		}

		// Token: 0x06000EED RID: 3821 RVA: 0x000197E0 File Offset: 0x000179E0
		public bool IsInvoking(string methodName)
		{
			return MonoBehaviour.IsInvoking(this, methodName);
		}

		// Token: 0x06000EEE RID: 3822 RVA: 0x000197FC File Offset: 0x000179FC
		[ExcludeFromDocs]
		public Coroutine StartCoroutine(string methodName)
		{
			object value = null;
			return this.StartCoroutine(methodName, value);
		}

		// Token: 0x06000EEF RID: 3823 RVA: 0x0001981C File Offset: 0x00017A1C
		public Coroutine StartCoroutine(string methodName, [DefaultValue("null")] object value)
		{
			if (string.IsNullOrEmpty(methodName))
			{
				throw new NullReferenceException("methodName is null or empty");
			}
			if (!MonoBehaviour.IsObjectMonoBehaviour(this))
			{
				throw new ArgumentException("Coroutines can only be stopped on a MonoBehaviour");
			}
			return this.StartCoroutineManaged(methodName, value);
		}

		// Token: 0x06000EF0 RID: 3824 RVA: 0x00019868 File Offset: 0x00017A68
		public Coroutine StartCoroutine(IEnumerator routine)
		{
			if (routine == null)
			{
				throw new NullReferenceException("routine is null");
			}
			if (!MonoBehaviour.IsObjectMonoBehaviour(this))
			{
				throw new ArgumentException("Coroutines can only be stopped on a MonoBehaviour");
			}
			return this.StartCoroutineManaged2(routine);
		}

		// Token: 0x06000EF1 RID: 3825 RVA: 0x000198AC File Offset: 0x00017AAC
		[Obsolete("StartCoroutine_Auto has been deprecated. Use StartCoroutine instead (UnityUpgradable) -> StartCoroutine([mscorlib] System.Collections.IEnumerator)", false)]
		public Coroutine StartCoroutine_Auto(IEnumerator routine)
		{
			return this.StartCoroutine(routine);
		}

		// Token: 0x06000EF2 RID: 3826 RVA: 0x000198C8 File Offset: 0x00017AC8
		public void StopCoroutine(IEnumerator routine)
		{
			if (routine == null)
			{
				throw new NullReferenceException("routine is null");
			}
			if (!MonoBehaviour.IsObjectMonoBehaviour(this))
			{
				throw new ArgumentException("Coroutines can only be stopped on a MonoBehaviour");
			}
			this.StopCoroutineFromEnumeratorManaged(routine);
		}

		// Token: 0x06000EF3 RID: 3827 RVA: 0x000198F9 File Offset: 0x00017AF9
		public void StopCoroutine(Coroutine routine)
		{
			if (routine == null)
			{
				throw new NullReferenceException("routine is null");
			}
			if (!MonoBehaviour.IsObjectMonoBehaviour(this))
			{
				throw new ArgumentException("Coroutines can only be stopped on a MonoBehaviour");
			}
			this.StopCoroutineManaged(routine);
		}

		// Token: 0x06000EF4 RID: 3828
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StopCoroutine(string methodName);

		// Token: 0x06000EF5 RID: 3829
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StopAllCoroutines();

		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x06000EF6 RID: 3830
		// (set) Token: 0x06000EF7 RID: 3831
		public extern bool useGUILayout { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000EF8 RID: 3832 RVA: 0x0001992A File Offset: 0x00017B2A
		public static void print(object message)
		{
			Debug.Log(message);
		}

		// Token: 0x06000EF9 RID: 3833
		[FreeFunction("CancelInvoke")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CancelInvokeAll(MonoBehaviour self);

		// Token: 0x06000EFA RID: 3834
		[FreeFunction("IsInvoking")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_IsInvokingAll(MonoBehaviour self);

		// Token: 0x06000EFB RID: 3835
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InvokeDelayed(MonoBehaviour self, string methodName, float time, float repeatRate);

		// Token: 0x06000EFC RID: 3836
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CancelInvoke(MonoBehaviour self, string methodName);

		// Token: 0x06000EFD RID: 3837
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsInvoking(MonoBehaviour self, string methodName);

		// Token: 0x06000EFE RID: 3838
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsObjectMonoBehaviour(Object obj);

		// Token: 0x06000EFF RID: 3839
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Coroutine StartCoroutineManaged(string methodName, object value);

		// Token: 0x06000F00 RID: 3840
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Coroutine StartCoroutineManaged2(IEnumerator enumerator);

		// Token: 0x06000F01 RID: 3841
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void StopCoroutineManaged(Coroutine routine);

		// Token: 0x06000F02 RID: 3842
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void StopCoroutineFromEnumeratorManaged(IEnumerator routine);

		// Token: 0x06000F03 RID: 3843
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string GetScriptClassName();
	}
}
