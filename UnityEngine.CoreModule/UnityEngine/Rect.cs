﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001F9 RID: 505
	[NativeHeader("Runtime/Math/Rect.h")]
	[NativeClass("Rectf", "template<typename T> class RectT; typedef RectT<float> Rectf;")]
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	public struct Rect : IEquatable<Rect>
	{
		// Token: 0x06000FCC RID: 4044 RVA: 0x0001B51F File Offset: 0x0001971F
		public Rect(float x, float y, float width, float height)
		{
			this.m_XMin = x;
			this.m_YMin = y;
			this.m_Width = width;
			this.m_Height = height;
		}

		// Token: 0x06000FCD RID: 4045 RVA: 0x0001B53F File Offset: 0x0001973F
		public Rect(Vector2 position, Vector2 size)
		{
			this.m_XMin = position.x;
			this.m_YMin = position.y;
			this.m_Width = size.x;
			this.m_Height = size.y;
		}

		// Token: 0x06000FCE RID: 4046 RVA: 0x0001B576 File Offset: 0x00019776
		public Rect(Rect source)
		{
			this.m_XMin = source.m_XMin;
			this.m_YMin = source.m_YMin;
			this.m_Width = source.m_Width;
			this.m_Height = source.m_Height;
		}

		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06000FCF RID: 4047 RVA: 0x0001B5B0 File Offset: 0x000197B0
		public static Rect zero
		{
			[CompilerGenerated]
			get
			{
				return new Rect(0f, 0f, 0f, 0f);
			}
		}

		// Token: 0x06000FD0 RID: 4048 RVA: 0x0001B5E0 File Offset: 0x000197E0
		public static Rect MinMaxRect(float xmin, float ymin, float xmax, float ymax)
		{
			return new Rect(xmin, ymin, xmax - xmin, ymax - ymin);
		}

		// Token: 0x06000FD1 RID: 4049 RVA: 0x0001B51F File Offset: 0x0001971F
		public void Set(float x, float y, float width, float height)
		{
			this.m_XMin = x;
			this.m_YMin = y;
			this.m_Width = width;
			this.m_Height = height;
		}

		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06000FD2 RID: 4050 RVA: 0x0001B604 File Offset: 0x00019804
		// (set) Token: 0x06000FD3 RID: 4051 RVA: 0x0001B61F File Offset: 0x0001981F
		public float x
		{
			get
			{
				return this.m_XMin;
			}
			set
			{
				this.m_XMin = value;
			}
		}

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06000FD4 RID: 4052 RVA: 0x0001B62C File Offset: 0x0001982C
		// (set) Token: 0x06000FD5 RID: 4053 RVA: 0x0001B647 File Offset: 0x00019847
		public float y
		{
			get
			{
				return this.m_YMin;
			}
			set
			{
				this.m_YMin = value;
			}
		}

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06000FD6 RID: 4054 RVA: 0x0001B654 File Offset: 0x00019854
		// (set) Token: 0x06000FD7 RID: 4055 RVA: 0x0001B67A File Offset: 0x0001987A
		public Vector2 position
		{
			get
			{
				return new Vector2(this.m_XMin, this.m_YMin);
			}
			set
			{
				this.m_XMin = value.x;
				this.m_YMin = value.y;
			}
		}

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06000FD8 RID: 4056 RVA: 0x0001B698 File Offset: 0x00019898
		// (set) Token: 0x06000FD9 RID: 4057 RVA: 0x0001B6D8 File Offset: 0x000198D8
		public Vector2 center
		{
			get
			{
				return new Vector2(this.x + this.m_Width / 2f, this.y + this.m_Height / 2f);
			}
			set
			{
				this.m_XMin = value.x - this.m_Width / 2f;
				this.m_YMin = value.y - this.m_Height / 2f;
			}
		}

		// Token: 0x170002FF RID: 767
		// (get) Token: 0x06000FDA RID: 4058 RVA: 0x0001B710 File Offset: 0x00019910
		// (set) Token: 0x06000FDB RID: 4059 RVA: 0x0001B736 File Offset: 0x00019936
		public Vector2 min
		{
			get
			{
				return new Vector2(this.xMin, this.yMin);
			}
			set
			{
				this.xMin = value.x;
				this.yMin = value.y;
			}
		}

		// Token: 0x17000300 RID: 768
		// (get) Token: 0x06000FDC RID: 4060 RVA: 0x0001B754 File Offset: 0x00019954
		// (set) Token: 0x06000FDD RID: 4061 RVA: 0x0001B77A File Offset: 0x0001997A
		public Vector2 max
		{
			get
			{
				return new Vector2(this.xMax, this.yMax);
			}
			set
			{
				this.xMax = value.x;
				this.yMax = value.y;
			}
		}

		// Token: 0x17000301 RID: 769
		// (get) Token: 0x06000FDE RID: 4062 RVA: 0x0001B798 File Offset: 0x00019998
		// (set) Token: 0x06000FDF RID: 4063 RVA: 0x0001B7B3 File Offset: 0x000199B3
		public float width
		{
			get
			{
				return this.m_Width;
			}
			set
			{
				this.m_Width = value;
			}
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06000FE0 RID: 4064 RVA: 0x0001B7C0 File Offset: 0x000199C0
		// (set) Token: 0x06000FE1 RID: 4065 RVA: 0x0001B7DB File Offset: 0x000199DB
		public float height
		{
			get
			{
				return this.m_Height;
			}
			set
			{
				this.m_Height = value;
			}
		}

		// Token: 0x17000303 RID: 771
		// (get) Token: 0x06000FE2 RID: 4066 RVA: 0x0001B7E8 File Offset: 0x000199E8
		// (set) Token: 0x06000FE3 RID: 4067 RVA: 0x0001B80E File Offset: 0x00019A0E
		public Vector2 size
		{
			get
			{
				return new Vector2(this.m_Width, this.m_Height);
			}
			set
			{
				this.m_Width = value.x;
				this.m_Height = value.y;
			}
		}

		// Token: 0x17000304 RID: 772
		// (get) Token: 0x06000FE4 RID: 4068 RVA: 0x0001B82C File Offset: 0x00019A2C
		// (set) Token: 0x06000FE5 RID: 4069 RVA: 0x0001B848 File Offset: 0x00019A48
		public float xMin
		{
			get
			{
				return this.m_XMin;
			}
			set
			{
				float xMax = this.xMax;
				this.m_XMin = value;
				this.m_Width = xMax - this.m_XMin;
			}
		}

		// Token: 0x17000305 RID: 773
		// (get) Token: 0x06000FE6 RID: 4070 RVA: 0x0001B874 File Offset: 0x00019A74
		// (set) Token: 0x06000FE7 RID: 4071 RVA: 0x0001B890 File Offset: 0x00019A90
		public float yMin
		{
			get
			{
				return this.m_YMin;
			}
			set
			{
				float yMax = this.yMax;
				this.m_YMin = value;
				this.m_Height = yMax - this.m_YMin;
			}
		}

		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06000FE8 RID: 4072 RVA: 0x0001B8BC File Offset: 0x00019ABC
		// (set) Token: 0x06000FE9 RID: 4073 RVA: 0x0001B8DE File Offset: 0x00019ADE
		public float xMax
		{
			get
			{
				return this.m_Width + this.m_XMin;
			}
			set
			{
				this.m_Width = value - this.m_XMin;
			}
		}

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06000FEA RID: 4074 RVA: 0x0001B8F0 File Offset: 0x00019AF0
		// (set) Token: 0x06000FEB RID: 4075 RVA: 0x0001B912 File Offset: 0x00019B12
		public float yMax
		{
			get
			{
				return this.m_Height + this.m_YMin;
			}
			set
			{
				this.m_Height = value - this.m_YMin;
			}
		}

		// Token: 0x06000FEC RID: 4076 RVA: 0x0001B924 File Offset: 0x00019B24
		public bool Contains(Vector2 point)
		{
			return point.x >= this.xMin && point.x < this.xMax && point.y >= this.yMin && point.y < this.yMax;
		}

		// Token: 0x06000FED RID: 4077 RVA: 0x0001B984 File Offset: 0x00019B84
		public bool Contains(Vector3 point)
		{
			return point.x >= this.xMin && point.x < this.xMax && point.y >= this.yMin && point.y < this.yMax;
		}

		// Token: 0x06000FEE RID: 4078 RVA: 0x0001B9E4 File Offset: 0x00019BE4
		public bool Contains(Vector3 point, bool allowInverse)
		{
			bool result;
			if (!allowInverse)
			{
				result = this.Contains(point);
			}
			else
			{
				bool flag = false;
				if ((this.width < 0f && point.x <= this.xMin && point.x > this.xMax) || (this.width >= 0f && point.x >= this.xMin && point.x < this.xMax))
				{
					flag = true;
				}
				result = (flag && ((this.height < 0f && point.y <= this.yMin && point.y > this.yMax) || (this.height >= 0f && point.y >= this.yMin && point.y < this.yMax)));
			}
			return result;
		}

		// Token: 0x06000FEF RID: 4079 RVA: 0x0001BAF0 File Offset: 0x00019CF0
		private static Rect OrderMinMax(Rect rect)
		{
			if (rect.xMin > rect.xMax)
			{
				float xMin = rect.xMin;
				rect.xMin = rect.xMax;
				rect.xMax = xMin;
			}
			if (rect.yMin > rect.yMax)
			{
				float yMin = rect.yMin;
				rect.yMin = rect.yMax;
				rect.yMax = yMin;
			}
			return rect;
		}

		// Token: 0x06000FF0 RID: 4080 RVA: 0x0001BB6C File Offset: 0x00019D6C
		public bool Overlaps(Rect other)
		{
			return other.xMax > this.xMin && other.xMin < this.xMax && other.yMax > this.yMin && other.yMin < this.yMax;
		}

		// Token: 0x06000FF1 RID: 4081 RVA: 0x0001BBCC File Offset: 0x00019DCC
		public bool Overlaps(Rect other, bool allowInverse)
		{
			Rect rect = this;
			if (allowInverse)
			{
				rect = Rect.OrderMinMax(rect);
				other = Rect.OrderMinMax(other);
			}
			return rect.Overlaps(other);
		}

		// Token: 0x06000FF2 RID: 4082 RVA: 0x0001BC08 File Offset: 0x00019E08
		public static Vector2 NormalizedToPoint(Rect rectangle, Vector2 normalizedRectCoordinates)
		{
			return new Vector2(Mathf.Lerp(rectangle.x, rectangle.xMax, normalizedRectCoordinates.x), Mathf.Lerp(rectangle.y, rectangle.yMax, normalizedRectCoordinates.y));
		}

		// Token: 0x06000FF3 RID: 4083 RVA: 0x0001BC58 File Offset: 0x00019E58
		public static Vector2 PointToNormalized(Rect rectangle, Vector2 point)
		{
			return new Vector2(Mathf.InverseLerp(rectangle.x, rectangle.xMax, point.x), Mathf.InverseLerp(rectangle.y, rectangle.yMax, point.y));
		}

		// Token: 0x06000FF4 RID: 4084 RVA: 0x0001BCA8 File Offset: 0x00019EA8
		public static bool operator !=(Rect lhs, Rect rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x06000FF5 RID: 4085 RVA: 0x0001BCC8 File Offset: 0x00019EC8
		public static bool operator ==(Rect lhs, Rect rhs)
		{
			return lhs.x == rhs.x && lhs.y == rhs.y && lhs.width == rhs.width && lhs.height == rhs.height;
		}

		// Token: 0x06000FF6 RID: 4086 RVA: 0x0001BD2C File Offset: 0x00019F2C
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.width.GetHashCode() << 2 ^ this.y.GetHashCode() >> 2 ^ this.height.GetHashCode() >> 1;
		}

		// Token: 0x06000FF7 RID: 4087 RVA: 0x0001BD9C File Offset: 0x00019F9C
		public override bool Equals(object other)
		{
			return other is Rect && this.Equals((Rect)other);
		}

		// Token: 0x06000FF8 RID: 4088 RVA: 0x0001BDD0 File Offset: 0x00019FD0
		public bool Equals(Rect other)
		{
			return this.x.Equals(other.x) && this.y.Equals(other.y) && this.width.Equals(other.width) && this.height.Equals(other.height);
		}

		// Token: 0x06000FF9 RID: 4089 RVA: 0x0001BE50 File Offset: 0x0001A050
		public override string ToString()
		{
			return UnityString.Format("(x:{0:F2}, y:{1:F2}, width:{2:F2}, height:{3:F2})", new object[]
			{
				this.x,
				this.y,
				this.width,
				this.height
			});
		}

		// Token: 0x06000FFA RID: 4090 RVA: 0x0001BEB0 File Offset: 0x0001A0B0
		public string ToString(string format)
		{
			return UnityString.Format("(x:{0}, y:{1}, width:{2}, height:{3})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format),
				this.width.ToString(format),
				this.height.ToString(format)
			});
		}

		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06000FFB RID: 4091 RVA: 0x0001BF20 File Offset: 0x0001A120
		[Obsolete("use xMin")]
		public float left
		{
			get
			{
				return this.m_XMin;
			}
		}

		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06000FFC RID: 4092 RVA: 0x0001BF3C File Offset: 0x0001A13C
		[Obsolete("use xMax")]
		public float right
		{
			get
			{
				return this.m_XMin + this.m_Width;
			}
		}

		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06000FFD RID: 4093 RVA: 0x0001BF60 File Offset: 0x0001A160
		[Obsolete("use yMin")]
		public float top
		{
			get
			{
				return this.m_YMin;
			}
		}

		// Token: 0x1700030B RID: 779
		// (get) Token: 0x06000FFE RID: 4094 RVA: 0x0001BF7C File Offset: 0x0001A17C
		[Obsolete("use yMax")]
		public float bottom
		{
			get
			{
				return this.m_YMin + this.m_Height;
			}
		}

		// Token: 0x0400073A RID: 1850
		[NativeName("x")]
		private float m_XMin;

		// Token: 0x0400073B RID: 1851
		[NativeName("y")]
		private float m_YMin;

		// Token: 0x0400073C RID: 1852
		[NativeName("width")]
		private float m_Width;

		// Token: 0x0400073D RID: 1853
		[NativeName("height")]
		private float m_Height;
	}
}
