﻿using System;

namespace UnityEngine
{
	// Token: 0x020000DF RID: 223
	public enum CustomRenderTextureUpdateMode
	{
		// Token: 0x0400036C RID: 876
		OnLoad,
		// Token: 0x0400036D RID: 877
		Realtime,
		// Token: 0x0400036E RID: 878
		OnDemand
	}
}
