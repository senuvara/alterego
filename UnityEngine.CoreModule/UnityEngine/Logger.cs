﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020002BA RID: 698
	public class Logger : ILogger, ILogHandler
	{
		// Token: 0x060018D3 RID: 6355 RVA: 0x00002050 File Offset: 0x00000250
		private Logger()
		{
		}

		// Token: 0x060018D4 RID: 6356 RVA: 0x0002B6B7 File Offset: 0x000298B7
		public Logger(ILogHandler logHandler)
		{
			this.logHandler = logHandler;
			this.logEnabled = true;
			this.filterLogType = LogType.Log;
		}

		// Token: 0x1700047A RID: 1146
		// (get) Token: 0x060018D5 RID: 6357 RVA: 0x0002B6D8 File Offset: 0x000298D8
		// (set) Token: 0x060018D6 RID: 6358 RVA: 0x0002B6F2 File Offset: 0x000298F2
		public ILogHandler logHandler
		{
			[CompilerGenerated]
			get
			{
				return this.<logHandler>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<logHandler>k__BackingField = value;
			}
		}

		// Token: 0x1700047B RID: 1147
		// (get) Token: 0x060018D7 RID: 6359 RVA: 0x0002B6FC File Offset: 0x000298FC
		// (set) Token: 0x060018D8 RID: 6360 RVA: 0x0002B716 File Offset: 0x00029916
		public bool logEnabled
		{
			[CompilerGenerated]
			get
			{
				return this.<logEnabled>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<logEnabled>k__BackingField = value;
			}
		}

		// Token: 0x1700047C RID: 1148
		// (get) Token: 0x060018D9 RID: 6361 RVA: 0x0002B720 File Offset: 0x00029920
		// (set) Token: 0x060018DA RID: 6362 RVA: 0x0002B73A File Offset: 0x0002993A
		public LogType filterLogType
		{
			[CompilerGenerated]
			get
			{
				return this.<filterLogType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<filterLogType>k__BackingField = value;
			}
		}

		// Token: 0x060018DB RID: 6363 RVA: 0x0002B744 File Offset: 0x00029944
		public bool IsLogTypeAllowed(LogType logType)
		{
			if (this.logEnabled)
			{
				if (logType == LogType.Exception)
				{
					return true;
				}
				if (this.filterLogType != LogType.Exception)
				{
					return logType <= this.filterLogType;
				}
			}
			return false;
		}

		// Token: 0x060018DC RID: 6364 RVA: 0x0002B794 File Offset: 0x00029994
		private static string GetString(object message)
		{
			string result;
			if (message == null)
			{
				result = "Null";
			}
			else
			{
				IFormattable formattable = message as IFormattable;
				if (formattable != null)
				{
					result = formattable.ToString(null, CultureInfo.InvariantCulture);
				}
				else
				{
					result = message.ToString();
				}
			}
			return result;
		}

		// Token: 0x060018DD RID: 6365 RVA: 0x0002B7E2 File Offset: 0x000299E2
		public void Log(LogType logType, object message)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, null, "{0}", new object[]
				{
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018DE RID: 6366 RVA: 0x0002B812 File Offset: 0x00029A12
		public void Log(LogType logType, object message, Object context)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, context, "{0}", new object[]
				{
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018DF RID: 6367 RVA: 0x0002B842 File Offset: 0x00029A42
		public void Log(LogType logType, string tag, object message)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, null, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018E0 RID: 6368 RVA: 0x0002B876 File Offset: 0x00029A76
		public void Log(LogType logType, string tag, object message, Object context)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, context, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018E1 RID: 6369 RVA: 0x0002B8AB File Offset: 0x00029AAB
		public void Log(object message)
		{
			if (this.IsLogTypeAllowed(LogType.Log))
			{
				this.logHandler.LogFormat(LogType.Log, null, "{0}", new object[]
				{
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018E2 RID: 6370 RVA: 0x0002B8DB File Offset: 0x00029ADB
		public void Log(string tag, object message)
		{
			if (this.IsLogTypeAllowed(LogType.Log))
			{
				this.logHandler.LogFormat(LogType.Log, null, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018E3 RID: 6371 RVA: 0x0002B90F File Offset: 0x00029B0F
		public void Log(string tag, object message, Object context)
		{
			if (this.IsLogTypeAllowed(LogType.Log))
			{
				this.logHandler.LogFormat(LogType.Log, context, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018E4 RID: 6372 RVA: 0x0002B943 File Offset: 0x00029B43
		public void LogWarning(string tag, object message)
		{
			if (this.IsLogTypeAllowed(LogType.Warning))
			{
				this.logHandler.LogFormat(LogType.Warning, null, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018E5 RID: 6373 RVA: 0x0002B977 File Offset: 0x00029B77
		public void LogWarning(string tag, object message, Object context)
		{
			if (this.IsLogTypeAllowed(LogType.Warning))
			{
				this.logHandler.LogFormat(LogType.Warning, context, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018E6 RID: 6374 RVA: 0x0002B9AB File Offset: 0x00029BAB
		public void LogError(string tag, object message)
		{
			if (this.IsLogTypeAllowed(LogType.Error))
			{
				this.logHandler.LogFormat(LogType.Error, null, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018E7 RID: 6375 RVA: 0x0002B9DF File Offset: 0x00029BDF
		public void LogError(string tag, object message, Object context)
		{
			if (this.IsLogTypeAllowed(LogType.Error))
			{
				this.logHandler.LogFormat(LogType.Error, context, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x060018E8 RID: 6376 RVA: 0x0002BA13 File Offset: 0x00029C13
		public void LogFormat(LogType logType, string format, params object[] args)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, null, format, args);
			}
		}

		// Token: 0x060018E9 RID: 6377 RVA: 0x0002BA31 File Offset: 0x00029C31
		public void LogException(Exception exception)
		{
			if (this.logEnabled)
			{
				this.logHandler.LogException(exception, null);
			}
		}

		// Token: 0x060018EA RID: 6378 RVA: 0x0002BA4C File Offset: 0x00029C4C
		public void LogFormat(LogType logType, Object context, string format, params object[] args)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, context, format, args);
			}
		}

		// Token: 0x060018EB RID: 6379 RVA: 0x0002BA6B File Offset: 0x00029C6B
		public void LogException(Exception exception, Object context)
		{
			if (this.logEnabled)
			{
				this.logHandler.LogException(exception, context);
			}
		}

		// Token: 0x040008FB RID: 2299
		private const string kNoTagFormat = "{0}";

		// Token: 0x040008FC RID: 2300
		private const string kTagFormat = "{0}: {1}";

		// Token: 0x040008FD RID: 2301
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ILogHandler <logHandler>k__BackingField;

		// Token: 0x040008FE RID: 2302
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <logEnabled>k__BackingField;

		// Token: 0x040008FF RID: 2303
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private LogType <filterLogType>k__BackingField;
	}
}
