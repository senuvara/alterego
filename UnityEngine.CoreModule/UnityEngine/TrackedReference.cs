﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000234 RID: 564
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class TrackedReference
	{
		// Token: 0x0600146D RID: 5229 RVA: 0x00002050 File Offset: 0x00000250
		protected TrackedReference()
		{
		}

		// Token: 0x0600146E RID: 5230 RVA: 0x00022B14 File Offset: 0x00020D14
		public static bool operator ==(TrackedReference x, TrackedReference y)
		{
			bool result;
			if (y == null && x == null)
			{
				result = true;
			}
			else if (y == null)
			{
				result = (x.m_Ptr == IntPtr.Zero);
			}
			else if (x == null)
			{
				result = (y.m_Ptr == IntPtr.Zero);
			}
			else
			{
				result = (x.m_Ptr == y.m_Ptr);
			}
			return result;
		}

		// Token: 0x0600146F RID: 5231 RVA: 0x00022B8C File Offset: 0x00020D8C
		public static bool operator !=(TrackedReference x, TrackedReference y)
		{
			return !(x == y);
		}

		// Token: 0x06001470 RID: 5232 RVA: 0x00022BAC File Offset: 0x00020DAC
		public override bool Equals(object o)
		{
			return o as TrackedReference == this;
		}

		// Token: 0x06001471 RID: 5233 RVA: 0x00022BD0 File Offset: 0x00020DD0
		public override int GetHashCode()
		{
			return (int)this.m_Ptr;
		}

		// Token: 0x06001472 RID: 5234 RVA: 0x00022BF0 File Offset: 0x00020DF0
		public static implicit operator bool(TrackedReference exists)
		{
			return exists != null;
		}

		// Token: 0x040007CD RID: 1997
		internal IntPtr m_Ptr;
	}
}
