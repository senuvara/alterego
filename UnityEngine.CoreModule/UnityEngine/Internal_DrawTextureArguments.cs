﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000094 RID: 148
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.IMGUIModule"
	})]
	internal struct Internal_DrawTextureArguments
	{
		// Token: 0x0400018F RID: 399
		public Rect screenRect;

		// Token: 0x04000190 RID: 400
		public Rect sourceRect;

		// Token: 0x04000191 RID: 401
		public int leftBorder;

		// Token: 0x04000192 RID: 402
		public int rightBorder;

		// Token: 0x04000193 RID: 403
		public int topBorder;

		// Token: 0x04000194 RID: 404
		public int bottomBorder;

		// Token: 0x04000195 RID: 405
		public Color color;

		// Token: 0x04000196 RID: 406
		public Vector4 borderWidths;

		// Token: 0x04000197 RID: 407
		public Vector4 cornerRadiuses;

		// Token: 0x04000198 RID: 408
		public int pass;

		// Token: 0x04000199 RID: 409
		public Texture texture;

		// Token: 0x0400019A RID: 410
		public Material mat;
	}
}
