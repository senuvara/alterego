﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000095 RID: 149
	[StaticAccessor("GetQualitySettings()", StaticAccessorType.Dot)]
	[NativeHeader("Runtime/Graphics/QualitySettings.h")]
	[NativeHeader("Runtime/Misc/PlayerSettings.h")]
	public sealed class QualitySettings : Object
	{
		// Token: 0x060007B6 RID: 1974 RVA: 0x0000D647 File Offset: 0x0000B847
		private QualitySettings()
		{
		}

		// Token: 0x060007B7 RID: 1975 RVA: 0x00010199 File Offset: 0x0000E399
		public static void IncreaseLevel([DefaultValue("false")] bool applyExpensiveChanges)
		{
			QualitySettings.SetQualityLevel(QualitySettings.GetQualityLevel() + 1, applyExpensiveChanges);
		}

		// Token: 0x060007B8 RID: 1976 RVA: 0x000101A9 File Offset: 0x0000E3A9
		public static void DecreaseLevel([DefaultValue("false")] bool applyExpensiveChanges)
		{
			QualitySettings.SetQualityLevel(QualitySettings.GetQualityLevel() - 1, applyExpensiveChanges);
		}

		// Token: 0x060007B9 RID: 1977 RVA: 0x000101B9 File Offset: 0x0000E3B9
		public static void SetQualityLevel(int index)
		{
			QualitySettings.SetQualityLevel(index, true);
		}

		// Token: 0x060007BA RID: 1978 RVA: 0x000101C3 File Offset: 0x0000E3C3
		public static void IncreaseLevel()
		{
			QualitySettings.IncreaseLevel(false);
		}

		// Token: 0x060007BB RID: 1979 RVA: 0x000101CC File Offset: 0x0000E3CC
		public static void DecreaseLevel()
		{
			QualitySettings.DecreaseLevel(false);
		}

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x060007BC RID: 1980 RVA: 0x000101D8 File Offset: 0x0000E3D8
		// (set) Token: 0x060007BD RID: 1981 RVA: 0x000101B9 File Offset: 0x0000E3B9
		[Obsolete("Use GetQualityLevel and SetQualityLevel", false)]
		public static QualityLevel currentLevel
		{
			get
			{
				return (QualityLevel)QualitySettings.GetQualityLevel();
			}
			set
			{
				QualitySettings.SetQualityLevel((int)value, true);
			}
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x060007BE RID: 1982
		// (set) Token: 0x060007BF RID: 1983
		public static extern int pixelLightCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x060007C0 RID: 1984
		// (set) Token: 0x060007C1 RID: 1985
		[NativeProperty("ShadowQuality")]
		public static extern ShadowQuality shadows { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000149 RID: 329
		// (get) Token: 0x060007C2 RID: 1986
		// (set) Token: 0x060007C3 RID: 1987
		public static extern ShadowProjection shadowProjection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x060007C4 RID: 1988
		// (set) Token: 0x060007C5 RID: 1989
		public static extern int shadowCascades { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x060007C6 RID: 1990
		// (set) Token: 0x060007C7 RID: 1991
		public static extern float shadowDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x060007C8 RID: 1992
		// (set) Token: 0x060007C9 RID: 1993
		[NativeProperty("ShadowResolution")]
		public static extern ShadowResolution shadowResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x060007CA RID: 1994
		// (set) Token: 0x060007CB RID: 1995
		[NativeProperty("ShadowmaskMode")]
		public static extern ShadowmaskMode shadowmaskMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x060007CC RID: 1996
		// (set) Token: 0x060007CD RID: 1997
		public static extern float shadowNearPlaneOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014F RID: 335
		// (get) Token: 0x060007CE RID: 1998
		// (set) Token: 0x060007CF RID: 1999
		public static extern float shadowCascade2Split { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000150 RID: 336
		// (get) Token: 0x060007D0 RID: 2000 RVA: 0x000101F4 File Offset: 0x0000E3F4
		// (set) Token: 0x060007D1 RID: 2001 RVA: 0x00010209 File Offset: 0x0000E409
		public static Vector3 shadowCascade4Split
		{
			get
			{
				Vector3 result;
				QualitySettings.get_shadowCascade4Split_Injected(out result);
				return result;
			}
			set
			{
				QualitySettings.set_shadowCascade4Split_Injected(ref value);
			}
		}

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x060007D2 RID: 2002
		// (set) Token: 0x060007D3 RID: 2003
		[NativeProperty("LODBias")]
		public static extern float lodBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x060007D4 RID: 2004
		// (set) Token: 0x060007D5 RID: 2005
		[NativeProperty("AnisotropicTextures")]
		public static extern AnisotropicFiltering anisotropicFiltering { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x060007D6 RID: 2006
		// (set) Token: 0x060007D7 RID: 2007
		public static extern int masterTextureLimit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x060007D8 RID: 2008
		// (set) Token: 0x060007D9 RID: 2009
		public static extern int maximumLODLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060007DA RID: 2010
		// (set) Token: 0x060007DB RID: 2011
		public static extern int particleRaycastBudget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060007DC RID: 2012
		// (set) Token: 0x060007DD RID: 2013
		public static extern bool softParticles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x060007DE RID: 2014
		// (set) Token: 0x060007DF RID: 2015
		public static extern bool softVegetation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060007E0 RID: 2016
		// (set) Token: 0x060007E1 RID: 2017
		public static extern int vSyncCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060007E2 RID: 2018
		// (set) Token: 0x060007E3 RID: 2019
		public static extern int antiAliasing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x060007E4 RID: 2020
		// (set) Token: 0x060007E5 RID: 2021
		public static extern int asyncUploadTimeSlice { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015B RID: 347
		// (get) Token: 0x060007E6 RID: 2022
		// (set) Token: 0x060007E7 RID: 2023
		public static extern int asyncUploadBufferSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x060007E8 RID: 2024
		// (set) Token: 0x060007E9 RID: 2025
		public static extern bool asyncUploadPersistentBuffer { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060007EA RID: 2026
		// (set) Token: 0x060007EB RID: 2027
		public static extern bool realtimeReflectionProbes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015E RID: 350
		// (get) Token: 0x060007EC RID: 2028
		// (set) Token: 0x060007ED RID: 2029
		public static extern bool billboardsFaceCameraPosition { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x060007EE RID: 2030
		// (set) Token: 0x060007EF RID: 2031
		public static extern float resolutionScalingFixedDPIFactor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x060007F0 RID: 2032
		// (set) Token: 0x060007F1 RID: 2033
		public static extern BlendWeights blendWeights { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x060007F2 RID: 2034
		// (set) Token: 0x060007F3 RID: 2035
		public static extern bool streamingMipmapsActive { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000162 RID: 354
		// (get) Token: 0x060007F4 RID: 2036
		// (set) Token: 0x060007F5 RID: 2037
		public static extern float streamingMipmapsMemoryBudget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000163 RID: 355
		// (get) Token: 0x060007F6 RID: 2038
		public static extern int streamingMipmapsRenderersPerFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000164 RID: 356
		// (get) Token: 0x060007F7 RID: 2039
		// (set) Token: 0x060007F8 RID: 2040
		public static extern int streamingMipmapsMaxLevelReduction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000165 RID: 357
		// (get) Token: 0x060007F9 RID: 2041
		// (set) Token: 0x060007FA RID: 2042
		public static extern bool streamingMipmapsAddAllCameras { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x060007FB RID: 2043
		// (set) Token: 0x060007FC RID: 2044
		public static extern int streamingMipmapsMaxFileIORequests { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x060007FD RID: 2045
		// (set) Token: 0x060007FE RID: 2046
		[StaticAccessor("QualitySettingsScripting", StaticAccessorType.DoubleColon)]
		public static extern int maxQueuedFrames { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060007FF RID: 2047
		[NativeName("GetCurrentIndex")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetQualityLevel();

		// Token: 0x06000800 RID: 2048
		[NativeName("SetCurrentIndex")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetQualityLevel(int index, [DefaultValue("true")] bool applyExpensiveChanges);

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x06000801 RID: 2049
		[NativeProperty("QualitySettingsNames")]
		public static extern string[] names { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000169 RID: 361
		// (get) Token: 0x06000802 RID: 2050
		public static extern ColorSpace desiredColorSpace { [NativeName("GetColorSpace")] [StaticAccessor("GetPlayerSettings()", StaticAccessorType.Dot)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700016A RID: 362
		// (get) Token: 0x06000803 RID: 2051
		public static extern ColorSpace activeColorSpace { [StaticAccessor("GetPlayerSettings()", StaticAccessorType.Dot)] [NativeName("GetColorSpace")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000804 RID: 2052
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_shadowCascade4Split_Injected(out Vector3 ret);

		// Token: 0x06000805 RID: 2053
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_shadowCascade4Split_Injected(ref Vector3 value);
	}
}
