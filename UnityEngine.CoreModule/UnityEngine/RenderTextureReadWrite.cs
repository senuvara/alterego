﻿using System;

namespace UnityEngine
{
	// Token: 0x020000D1 RID: 209
	public enum RenderTextureReadWrite
	{
		// Token: 0x040002AF RID: 687
		Default,
		// Token: 0x040002B0 RID: 688
		Linear,
		// Token: 0x040002B1 RID: 689
		sRGB
	}
}
