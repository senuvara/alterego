﻿using System;

namespace UnityEngine
{
	// Token: 0x0200031A RID: 794
	public sealed class StaticBatchingUtility
	{
		// Token: 0x06001B40 RID: 6976 RVA: 0x00002370 File Offset: 0x00000570
		public StaticBatchingUtility()
		{
		}

		// Token: 0x06001B41 RID: 6977 RVA: 0x0002FE89 File Offset: 0x0002E089
		public static void Combine(GameObject staticBatchRoot)
		{
			InternalStaticBatchingUtility.CombineRoot(staticBatchRoot);
		}

		// Token: 0x06001B42 RID: 6978 RVA: 0x0002FE92 File Offset: 0x0002E092
		public static void Combine(GameObject[] gos, GameObject staticBatchRoot)
		{
			InternalStaticBatchingUtility.CombineGameObjects(gos, staticBatchRoot, false);
		}
	}
}
