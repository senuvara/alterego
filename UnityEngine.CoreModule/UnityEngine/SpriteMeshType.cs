﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035E RID: 862
	public enum SpriteMeshType
	{
		// Token: 0x04000AEC RID: 2796
		FullRect,
		// Token: 0x04000AED RID: 2797
		Tight
	}
}
