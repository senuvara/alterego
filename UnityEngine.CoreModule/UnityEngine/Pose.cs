﻿using System;

namespace UnityEngine
{
	// Token: 0x020001E4 RID: 484
	[Serializable]
	public struct Pose : IEquatable<Pose>
	{
		// Token: 0x06000F6B RID: 3947 RVA: 0x0001A9C4 File Offset: 0x00018BC4
		public Pose(Vector3 position, Quaternion rotation)
		{
			this.position = position;
			this.rotation = rotation;
		}

		// Token: 0x06000F6C RID: 3948 RVA: 0x0001A9D8 File Offset: 0x00018BD8
		public override string ToString()
		{
			return string.Format("({0}, {1})", this.position.ToString(), this.rotation.ToString());
		}

		// Token: 0x06000F6D RID: 3949 RVA: 0x0001AA1C File Offset: 0x00018C1C
		public string ToString(string format)
		{
			return string.Format("({0}, {1})", this.position.ToString(format), this.rotation.ToString(format));
		}

		// Token: 0x06000F6E RID: 3950 RVA: 0x0001AA54 File Offset: 0x00018C54
		public Pose GetTransformedBy(Pose lhs)
		{
			return new Pose
			{
				position = lhs.position + lhs.rotation * this.position,
				rotation = lhs.rotation * this.rotation
			};
		}

		// Token: 0x06000F6F RID: 3951 RVA: 0x0001AAB0 File Offset: 0x00018CB0
		public Pose GetTransformedBy(Transform lhs)
		{
			return new Pose
			{
				position = lhs.TransformPoint(this.position),
				rotation = lhs.rotation * this.rotation
			};
		}

		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x06000F70 RID: 3952 RVA: 0x0001AAFC File Offset: 0x00018CFC
		public Vector3 forward
		{
			get
			{
				return this.rotation * Vector3.forward;
			}
		}

		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x06000F71 RID: 3953 RVA: 0x0001AB24 File Offset: 0x00018D24
		public Vector3 right
		{
			get
			{
				return this.rotation * Vector3.right;
			}
		}

		// Token: 0x170002EA RID: 746
		// (get) Token: 0x06000F72 RID: 3954 RVA: 0x0001AB4C File Offset: 0x00018D4C
		public Vector3 up
		{
			get
			{
				return this.rotation * Vector3.up;
			}
		}

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x06000F73 RID: 3955 RVA: 0x0001AB74 File Offset: 0x00018D74
		public static Pose identity
		{
			get
			{
				return Pose.k_Identity;
			}
		}

		// Token: 0x06000F74 RID: 3956 RVA: 0x0001AB90 File Offset: 0x00018D90
		public override bool Equals(object obj)
		{
			return obj is Pose && this.Equals((Pose)obj);
		}

		// Token: 0x06000F75 RID: 3957 RVA: 0x0001ABC4 File Offset: 0x00018DC4
		public bool Equals(Pose other)
		{
			return this.position == other.position && this.rotation == other.rotation;
		}

		// Token: 0x06000F76 RID: 3958 RVA: 0x0001AC08 File Offset: 0x00018E08
		public override int GetHashCode()
		{
			return this.position.GetHashCode() ^ this.rotation.GetHashCode() << 1;
		}

		// Token: 0x06000F77 RID: 3959 RVA: 0x0001AC44 File Offset: 0x00018E44
		public static bool operator ==(Pose a, Pose b)
		{
			return a.Equals(b);
		}

		// Token: 0x06000F78 RID: 3960 RVA: 0x0001AC64 File Offset: 0x00018E64
		public static bool operator !=(Pose a, Pose b)
		{
			return !(a == b);
		}

		// Token: 0x06000F79 RID: 3961 RVA: 0x0001AC83 File Offset: 0x00018E83
		// Note: this type is marked as 'beforefieldinit'.
		static Pose()
		{
		}

		// Token: 0x04000718 RID: 1816
		public Vector3 position;

		// Token: 0x04000719 RID: 1817
		public Quaternion rotation;

		// Token: 0x0400071A RID: 1818
		private static readonly Pose k_Identity = new Pose(Vector3.zero, Quaternion.identity);
	}
}
