﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000127 RID: 295
	[NativeHeader("Runtime/Input/GetInput.h")]
	public class Gyroscope
	{
		// Token: 0x06000C3B RID: 3131 RVA: 0x00013A67 File Offset: 0x00011C67
		internal Gyroscope(int index)
		{
			this.m_GyroIndex = index;
		}

		// Token: 0x06000C3C RID: 3132 RVA: 0x00013A78 File Offset: 0x00011C78
		[FreeFunction("GetGyroRotationRate")]
		private static Vector3 rotationRate_Internal(int idx)
		{
			Vector3 result;
			Gyroscope.rotationRate_Internal_Injected(idx, out result);
			return result;
		}

		// Token: 0x06000C3D RID: 3133 RVA: 0x00013A90 File Offset: 0x00011C90
		[FreeFunction("GetGyroRotationRateUnbiased")]
		private static Vector3 rotationRateUnbiased_Internal(int idx)
		{
			Vector3 result;
			Gyroscope.rotationRateUnbiased_Internal_Injected(idx, out result);
			return result;
		}

		// Token: 0x06000C3E RID: 3134 RVA: 0x00013AA8 File Offset: 0x00011CA8
		[FreeFunction("GetGravity")]
		private static Vector3 gravity_Internal(int idx)
		{
			Vector3 result;
			Gyroscope.gravity_Internal_Injected(idx, out result);
			return result;
		}

		// Token: 0x06000C3F RID: 3135 RVA: 0x00013AC0 File Offset: 0x00011CC0
		[FreeFunction("GetUserAcceleration")]
		private static Vector3 userAcceleration_Internal(int idx)
		{
			Vector3 result;
			Gyroscope.userAcceleration_Internal_Injected(idx, out result);
			return result;
		}

		// Token: 0x06000C40 RID: 3136 RVA: 0x00013AD8 File Offset: 0x00011CD8
		[FreeFunction("GetAttitude")]
		private static Quaternion attitude_Internal(int idx)
		{
			Quaternion result;
			Gyroscope.attitude_Internal_Injected(idx, out result);
			return result;
		}

		// Token: 0x06000C41 RID: 3137
		[FreeFunction("IsGyroEnabled")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool getEnabled_Internal(int idx);

		// Token: 0x06000C42 RID: 3138
		[FreeFunction("SetGyroEnabled")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void setEnabled_Internal(int idx, bool enabled);

		// Token: 0x06000C43 RID: 3139
		[FreeFunction("GetGyroUpdateInterval")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float getUpdateInterval_Internal(int idx);

		// Token: 0x06000C44 RID: 3140
		[FreeFunction("SetGyroUpdateInterval")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void setUpdateInterval_Internal(int idx, float interval);

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x06000C45 RID: 3141 RVA: 0x00013AF0 File Offset: 0x00011CF0
		public Vector3 rotationRate
		{
			get
			{
				return Gyroscope.rotationRate_Internal(this.m_GyroIndex);
			}
		}

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x06000C46 RID: 3142 RVA: 0x00013B10 File Offset: 0x00011D10
		public Vector3 rotationRateUnbiased
		{
			get
			{
				return Gyroscope.rotationRateUnbiased_Internal(this.m_GyroIndex);
			}
		}

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x06000C47 RID: 3143 RVA: 0x00013B30 File Offset: 0x00011D30
		public Vector3 gravity
		{
			get
			{
				return Gyroscope.gravity_Internal(this.m_GyroIndex);
			}
		}

		// Token: 0x17000259 RID: 601
		// (get) Token: 0x06000C48 RID: 3144 RVA: 0x00013B50 File Offset: 0x00011D50
		public Vector3 userAcceleration
		{
			get
			{
				return Gyroscope.userAcceleration_Internal(this.m_GyroIndex);
			}
		}

		// Token: 0x1700025A RID: 602
		// (get) Token: 0x06000C49 RID: 3145 RVA: 0x00013B70 File Offset: 0x00011D70
		public Quaternion attitude
		{
			get
			{
				return Gyroscope.attitude_Internal(this.m_GyroIndex);
			}
		}

		// Token: 0x1700025B RID: 603
		// (get) Token: 0x06000C4A RID: 3146 RVA: 0x00013B90 File Offset: 0x00011D90
		// (set) Token: 0x06000C4B RID: 3147 RVA: 0x00013BB0 File Offset: 0x00011DB0
		public bool enabled
		{
			get
			{
				return Gyroscope.getEnabled_Internal(this.m_GyroIndex);
			}
			set
			{
				Gyroscope.setEnabled_Internal(this.m_GyroIndex, value);
			}
		}

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x06000C4C RID: 3148 RVA: 0x00013BC0 File Offset: 0x00011DC0
		// (set) Token: 0x06000C4D RID: 3149 RVA: 0x00013BE0 File Offset: 0x00011DE0
		public float updateInterval
		{
			get
			{
				return Gyroscope.getUpdateInterval_Internal(this.m_GyroIndex);
			}
			set
			{
				Gyroscope.setUpdateInterval_Internal(this.m_GyroIndex, value);
			}
		}

		// Token: 0x06000C4E RID: 3150
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void rotationRate_Internal_Injected(int idx, out Vector3 ret);

		// Token: 0x06000C4F RID: 3151
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void rotationRateUnbiased_Internal_Injected(int idx, out Vector3 ret);

		// Token: 0x06000C50 RID: 3152
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void gravity_Internal_Injected(int idx, out Vector3 ret);

		// Token: 0x06000C51 RID: 3153
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void userAcceleration_Internal_Injected(int idx, out Vector3 ret);

		// Token: 0x06000C52 RID: 3154
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void attitude_Internal_Injected(int idx, out Quaternion ret);

		// Token: 0x0400053F RID: 1343
		private int m_GyroIndex;
	}
}
