﻿using System;
using System.Reflection;

namespace UnityEngine
{
	// Token: 0x0200000D RID: 13
	public class AndroidJavaProxy
	{
		// Token: 0x0600014A RID: 330 RVA: 0x0000558E File Offset: 0x0000378E
		public AndroidJavaProxy(string javaInterface) : this(new AndroidJavaClass(javaInterface))
		{
		}

		// Token: 0x0600014B RID: 331 RVA: 0x0000559D File Offset: 0x0000379D
		public AndroidJavaProxy(AndroidJavaClass javaInterface)
		{
			this.javaInterface = javaInterface;
		}

		// Token: 0x0600014C RID: 332 RVA: 0x000055B0 File Offset: 0x000037B0
		public virtual AndroidJavaObject Invoke(string methodName, object[] args)
		{
			Exception ex = null;
			BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
			Type[] array = new Type[args.Length];
			for (int i = 0; i < args.Length; i++)
			{
				array[i] = ((args[i] != null) ? args[i].GetType() : typeof(AndroidJavaObject));
			}
			try
			{
				MethodInfo method = base.GetType().GetMethod(methodName, bindingAttr, null, array, null);
				if (method != null)
				{
					return _AndroidJNIHelper.Box(method.Invoke(this, args));
				}
			}
			catch (TargetInvocationException ex2)
			{
				ex = ex2.InnerException;
			}
			catch (Exception ex3)
			{
				ex = ex3;
			}
			string[] array2 = new string[args.Length];
			for (int j = 0; j < array.Length; j++)
			{
				array2[j] = array[j].ToString();
			}
			if (ex != null)
			{
				throw new TargetInvocationException(string.Concat(new object[]
				{
					base.GetType(),
					".",
					methodName,
					"(",
					string.Join(",", array2),
					")"
				}), ex);
			}
			throw new Exception(string.Concat(new object[]
			{
				"No such proxy method: ",
				base.GetType(),
				".",
				methodName,
				"(",
				string.Join(",", array2),
				")"
			}));
		}

		// Token: 0x0600014D RID: 333 RVA: 0x0000573C File Offset: 0x0000393C
		public virtual AndroidJavaObject Invoke(string methodName, AndroidJavaObject[] javaArgs)
		{
			object[] array = new object[javaArgs.Length];
			for (int i = 0; i < javaArgs.Length; i++)
			{
				array[i] = _AndroidJNIHelper.Unbox(javaArgs[i]);
				if (!(array[i] is AndroidJavaObject))
				{
					if (javaArgs[i] != null)
					{
						javaArgs[i].Dispose();
					}
				}
			}
			return this.Invoke(methodName, array);
		}

		// Token: 0x0600014E RID: 334 RVA: 0x000057A4 File Offset: 0x000039A4
		public virtual bool equals(AndroidJavaObject obj)
		{
			IntPtr obj2 = (obj != null) ? obj.GetRawObject() : IntPtr.Zero;
			return AndroidJNI.IsSameObject(this.GetProxy().GetRawObject(), obj2);
		}

		// Token: 0x0600014F RID: 335 RVA: 0x000057E4 File Offset: 0x000039E4
		public virtual int hashCode()
		{
			jvalue[] array = new jvalue[1];
			array[0].l = this.GetProxy().GetRawObject();
			return AndroidJNISafe.CallStaticIntMethod(AndroidJavaProxy.s_JavaLangSystemClass, AndroidJavaProxy.s_HashCodeMethodID, array);
		}

		// Token: 0x06000150 RID: 336 RVA: 0x0000582C File Offset: 0x00003A2C
		public virtual string toString()
		{
			return this.ToString() + " <c# proxy java object>";
		}

		// Token: 0x06000151 RID: 337 RVA: 0x00005854 File Offset: 0x00003A54
		internal AndroidJavaObject GetProxy()
		{
			if (this.proxyObject == null)
			{
				this.proxyObject = AndroidJavaObject.AndroidJavaObjectDeleteLocalRef(AndroidJNIHelper.CreateJavaProxy(this));
			}
			return this.proxyObject;
		}

		// Token: 0x06000152 RID: 338 RVA: 0x0000588D File Offset: 0x00003A8D
		// Note: this type is marked as 'beforefieldinit'.
		static AndroidJavaProxy()
		{
		}

		// Token: 0x04000012 RID: 18
		public readonly AndroidJavaClass javaInterface;

		// Token: 0x04000013 RID: 19
		internal AndroidJavaObject proxyObject;

		// Token: 0x04000014 RID: 20
		private static readonly GlobalJavaObjectRef s_JavaLangSystemClass = new GlobalJavaObjectRef(AndroidJNISafe.FindClass("java/lang/System"));

		// Token: 0x04000015 RID: 21
		private static readonly IntPtr s_HashCodeMethodID = AndroidJNIHelper.GetMethodID(AndroidJavaProxy.s_JavaLangSystemClass, "identityHashCode", "(Ljava/lang/Object;)I", true);
	}
}
