﻿using System;

namespace UnityEngine
{
	// Token: 0x02000362 RID: 866
	public enum SpriteSortPoint
	{
		// Token: 0x04000B03 RID: 2819
		Center,
		// Token: 0x04000B04 RID: 2820
		Pivot
	}
}
