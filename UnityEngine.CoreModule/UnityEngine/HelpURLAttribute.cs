﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000030 RID: 48
	[UsedByNativeCode]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	public sealed class HelpURLAttribute : Attribute
	{
		// Token: 0x0600028E RID: 654 RVA: 0x00008AE4 File Offset: 0x00006CE4
		public HelpURLAttribute(string url)
		{
			this.m_Url = url;
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x0600028F RID: 655 RVA: 0x00008AF4 File Offset: 0x00006CF4
		public string URL
		{
			get
			{
				return this.m_Url;
			}
		}

		// Token: 0x0400006D RID: 109
		internal readonly string m_Url;
	}
}
