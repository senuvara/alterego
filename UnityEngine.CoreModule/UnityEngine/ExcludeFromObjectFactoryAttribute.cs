﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200006E RID: 110
	[UsedByNativeCode]
	[AttributeUsage(AttributeTargets.Class)]
	public class ExcludeFromObjectFactoryAttribute : Attribute
	{
		// Token: 0x06000597 RID: 1431 RVA: 0x0000898B File Offset: 0x00006B8B
		public ExcludeFromObjectFactoryAttribute()
		{
		}
	}
}
