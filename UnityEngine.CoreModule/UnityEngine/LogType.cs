﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003A RID: 58
	public enum LogType
	{
		// Token: 0x040000DA RID: 218
		Error,
		// Token: 0x040000DB RID: 219
		Assert,
		// Token: 0x040000DC RID: 220
		Warning,
		// Token: 0x040000DD RID: 221
		Log,
		// Token: 0x040000DE RID: 222
		Exception
	}
}
