﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngineInternal;

namespace UnityEngine
{
	// Token: 0x02000200 RID: 512
	[NativeHeader("Runtime/Export/Resources.bindings.h")]
	[NativeHeader("Runtime/Misc/ResourceManagerUtility.h")]
	public sealed class Resources
	{
		// Token: 0x06001156 RID: 4438 RVA: 0x00002370 File Offset: 0x00000570
		public Resources()
		{
		}

		// Token: 0x06001157 RID: 4439 RVA: 0x0001DAB0 File Offset: 0x0001BCB0
		internal static T[] ConvertObjects<T>(Object[] rawObjects) where T : Object
		{
			T[] result;
			if (rawObjects == null)
			{
				result = null;
			}
			else
			{
				T[] array = new T[rawObjects.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = (T)((object)rawObjects[i]);
				}
				result = array;
			}
			return result;
		}

		// Token: 0x06001158 RID: 4440
		[FreeFunction("Resources_Bindings::FindObjectsOfTypeAll")]
		[TypeInferenceRule(TypeInferenceRules.ArrayOfTypeReferencedByFirstArgument)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Object[] FindObjectsOfTypeAll(Type type);

		// Token: 0x06001159 RID: 4441 RVA: 0x0001DB00 File Offset: 0x0001BD00
		public static T[] FindObjectsOfTypeAll<T>() where T : Object
		{
			return Resources.ConvertObjects<T>(Resources.FindObjectsOfTypeAll(typeof(T)));
		}

		// Token: 0x0600115A RID: 4442 RVA: 0x0001DB2C File Offset: 0x0001BD2C
		public static Object Load(string path)
		{
			return Resources.Load(path, typeof(Object));
		}

		// Token: 0x0600115B RID: 4443 RVA: 0x0001DB54 File Offset: 0x0001BD54
		public static T Load<T>(string path) where T : Object
		{
			return (T)((object)Resources.Load(path, typeof(T)));
		}

		// Token: 0x0600115C RID: 4444
		[NativeThrows]
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedBySecondArgument)]
		[FreeFunction("Resources_Bindings::Load")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Object Load(string path, [NotNull] Type systemTypeInstance);

		// Token: 0x0600115D RID: 4445 RVA: 0x0001DB80 File Offset: 0x0001BD80
		public static ResourceRequest LoadAsync(string path)
		{
			return Resources.LoadAsync(path, typeof(Object));
		}

		// Token: 0x0600115E RID: 4446 RVA: 0x0001DBA8 File Offset: 0x0001BDA8
		public static ResourceRequest LoadAsync<T>(string path) where T : Object
		{
			return Resources.LoadAsync(path, typeof(T));
		}

		// Token: 0x0600115F RID: 4447 RVA: 0x0001DBD0 File Offset: 0x0001BDD0
		public static ResourceRequest LoadAsync(string path, Type type)
		{
			ResourceRequest resourceRequest = Resources.LoadAsyncInternal(path, type);
			resourceRequest.m_Path = path;
			resourceRequest.m_Type = type;
			return resourceRequest;
		}

		// Token: 0x06001160 RID: 4448
		[FreeFunction("Resources_Bindings::LoadAsyncInternal")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern ResourceRequest LoadAsyncInternal(string path, Type type);

		// Token: 0x06001161 RID: 4449
		[FreeFunction("Resources_Bindings::LoadAll")]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Object[] LoadAll([NotNull] string path, [NotNull] Type systemTypeInstance);

		// Token: 0x06001162 RID: 4450 RVA: 0x0001DBFC File Offset: 0x0001BDFC
		public static Object[] LoadAll(string path)
		{
			return Resources.LoadAll(path, typeof(Object));
		}

		// Token: 0x06001163 RID: 4451 RVA: 0x0001DC24 File Offset: 0x0001BE24
		public static T[] LoadAll<T>(string path) where T : Object
		{
			return Resources.ConvertObjects<T>(Resources.LoadAll(path, typeof(T)));
		}

		// Token: 0x06001164 RID: 4452
		[FreeFunction("GetScriptingBuiltinResource")]
		[TypeInferenceRule(TypeInferenceRules.TypeReferencedByFirstArgument)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Object GetBuiltinResource([NotNull] Type type, string path);

		// Token: 0x06001165 RID: 4453 RVA: 0x0001DC50 File Offset: 0x0001BE50
		public static T GetBuiltinResource<T>(string path) where T : Object
		{
			return (T)((object)Resources.GetBuiltinResource(typeof(T), path));
		}

		// Token: 0x06001166 RID: 4454
		[FreeFunction("Scripting::UnloadAssetFromScripting")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void UnloadAsset(Object assetToUnload);

		// Token: 0x06001167 RID: 4455
		[FreeFunction("Resources_Bindings::UnloadUnusedAssets")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern AsyncOperation UnloadUnusedAssets();
	}
}
