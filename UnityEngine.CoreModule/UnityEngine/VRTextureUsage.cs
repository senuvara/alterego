﻿using System;

namespace UnityEngine
{
	// Token: 0x020000CF RID: 207
	public enum VRTextureUsage
	{
		// Token: 0x040002A0 RID: 672
		None,
		// Token: 0x040002A1 RID: 673
		OneEye,
		// Token: 0x040002A2 RID: 674
		TwoEyes
	}
}
