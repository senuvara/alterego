﻿using System;
using System.ComponentModel;

namespace UnityEngine
{
	// Token: 0x02000207 RID: 519
	public static class Types
	{
		// Token: 0x060011A4 RID: 4516 RVA: 0x0001DF70 File Offset: 0x0001C170
		[Obsolete("This was an internal method which is no longer used", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static Type GetType(string typeName, string assemblyName)
		{
			return null;
		}
	}
}
