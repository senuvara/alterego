﻿using System;

namespace UnityEngine
{
	// Token: 0x0200011F RID: 287
	public static class HashUnsafeUtilities
	{
		// Token: 0x06000C18 RID: 3096 RVA: 0x000137AF File Offset: 0x000119AF
		public unsafe static void ComputeHash128(void* data, ulong dataSize, ulong* hash1, ulong* hash2)
		{
			SpookyHash.Hash(data, dataSize, hash1, hash2);
		}

		// Token: 0x06000C19 RID: 3097 RVA: 0x000137BC File Offset: 0x000119BC
		public unsafe static void ComputeHash128(void* data, ulong dataSize, Hash128* hash)
		{
			ulong u64_ = hash->u64_0;
			ulong u64_2 = hash->u64_1;
			HashUnsafeUtilities.ComputeHash128(data, dataSize, &u64_, &u64_2);
			*hash = new Hash128(u64_, u64_2);
		}
	}
}
