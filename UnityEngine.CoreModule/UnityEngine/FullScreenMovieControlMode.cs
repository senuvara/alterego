﻿using System;

namespace UnityEngine
{
	// Token: 0x02000117 RID: 279
	public enum FullScreenMovieControlMode
	{
		// Token: 0x040004FB RID: 1275
		Full,
		// Token: 0x040004FC RID: 1276
		Minimal,
		// Token: 0x040004FD RID: 1277
		CancelOnInput,
		// Token: 0x040004FE RID: 1278
		Hidden
	}
}
