﻿using System;

namespace UnityEngine
{
	// Token: 0x020001EC RID: 492
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class MinAttribute : PropertyAttribute
	{
		// Token: 0x06000F84 RID: 3972 RVA: 0x0001AD41 File Offset: 0x00018F41
		public MinAttribute(float min)
		{
			this.min = min;
		}

		// Token: 0x04000724 RID: 1828
		public readonly float min;
	}
}
