﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000053 RID: 83
	internal class LowerResBlitTexture : Object
	{
		// Token: 0x0600046C RID: 1132 RVA: 0x0000AD1B File Offset: 0x00008F1B
		public LowerResBlitTexture()
		{
		}

		// Token: 0x0600046D RID: 1133 RVA: 0x00007476 File Offset: 0x00005676
		[RequiredByNativeCode]
		internal void LowerResBlitTextureDontStripMe()
		{
		}
	}
}
