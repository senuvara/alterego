﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C9 RID: 201
	public enum FilterMode
	{
		// Token: 0x04000236 RID: 566
		Point,
		// Token: 0x04000237 RID: 567
		Bilinear,
		// Token: 0x04000238 RID: 568
		Trilinear
	}
}
