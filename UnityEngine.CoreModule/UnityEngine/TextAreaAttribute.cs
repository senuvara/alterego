﻿using System;

namespace UnityEngine
{
	// Token: 0x020001EE RID: 494
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class TextAreaAttribute : PropertyAttribute
	{
		// Token: 0x06000F87 RID: 3975 RVA: 0x0001AD71 File Offset: 0x00018F71
		public TextAreaAttribute()
		{
			this.minLines = 3;
			this.maxLines = 3;
		}

		// Token: 0x06000F88 RID: 3976 RVA: 0x0001AD88 File Offset: 0x00018F88
		public TextAreaAttribute(int minLines, int maxLines)
		{
			this.minLines = minLines;
			this.maxLines = maxLines;
		}

		// Token: 0x04000726 RID: 1830
		public readonly int minLines;

		// Token: 0x04000727 RID: 1831
		public readonly int maxLines;
	}
}
