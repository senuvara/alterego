﻿using System;

namespace UnityEngine
{
	// Token: 0x020000D6 RID: 214
	[Flags]
	public enum LightmapsMode
	{
		// Token: 0x0400034F RID: 847
		NonDirectional = 0,
		// Token: 0x04000350 RID: 848
		CombinedDirectional = 1
	}
}
