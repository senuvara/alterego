﻿using System;

namespace UnityEngine
{
	// Token: 0x0200005E RID: 94
	internal enum CullingQueryOptions
	{
		// Token: 0x04000122 RID: 290
		Normal,
		// Token: 0x04000123 RID: 291
		IgnoreVisibility,
		// Token: 0x04000124 RID: 292
		IgnoreDistance
	}
}
