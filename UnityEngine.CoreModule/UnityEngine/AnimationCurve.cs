﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000014 RID: 20
	[ThreadAndSerializationSafe]
	[NativeHeader("Runtime/Math/AnimationCurve.bindings.h")]
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class AnimationCurve : IEquatable<AnimationCurve>
	{
		// Token: 0x06000189 RID: 393 RVA: 0x00007479 File Offset: 0x00005679
		public AnimationCurve(params Keyframe[] keys)
		{
			this.m_Ptr = AnimationCurve.Internal_Create(keys);
		}

		// Token: 0x0600018A RID: 394 RVA: 0x0000748E File Offset: 0x0000568E
		[RequiredByNativeCode]
		public AnimationCurve()
		{
			this.m_Ptr = AnimationCurve.Internal_Create(null);
		}

		// Token: 0x0600018B RID: 395
		[FreeFunction("AnimationCurveBindings::Internal_Destroy", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Destroy(IntPtr ptr);

		// Token: 0x0600018C RID: 396
		[FreeFunction("AnimationCurveBindings::Internal_Create", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Internal_Create(Keyframe[] keys);

		// Token: 0x0600018D RID: 397
		[FreeFunction("AnimationCurveBindings::Internal_Equals", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool Internal_Equals(IntPtr other);

		// Token: 0x0600018E RID: 398 RVA: 0x000074A4 File Offset: 0x000056A4
		~AnimationCurve()
		{
			AnimationCurve.Internal_Destroy(this.m_Ptr);
		}

		// Token: 0x0600018F RID: 399
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float Evaluate(float time);

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000190 RID: 400 RVA: 0x000074DC File Offset: 0x000056DC
		// (set) Token: 0x06000191 RID: 401 RVA: 0x000074F7 File Offset: 0x000056F7
		public Keyframe[] keys
		{
			get
			{
				return this.GetKeys();
			}
			set
			{
				this.SetKeys(value);
			}
		}

		// Token: 0x06000192 RID: 402
		[FreeFunction("AnimationCurveBindings::AddKeySmoothTangents", HasExplicitThis = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int AddKey(float time, float value);

		// Token: 0x06000193 RID: 403 RVA: 0x00007504 File Offset: 0x00005704
		public int AddKey(Keyframe key)
		{
			return this.AddKey_Internal(key);
		}

		// Token: 0x06000194 RID: 404 RVA: 0x00007520 File Offset: 0x00005720
		[NativeMethod("AddKey", IsThreadSafe = true)]
		private int AddKey_Internal(Keyframe key)
		{
			return this.AddKey_Internal_Injected(ref key);
		}

		// Token: 0x06000195 RID: 405 RVA: 0x0000752A File Offset: 0x0000572A
		[NativeThrows]
		[FreeFunction("AnimationCurveBindings::MoveKey", HasExplicitThis = true, IsThreadSafe = true)]
		public int MoveKey(int index, Keyframe key)
		{
			return this.MoveKey_Injected(index, ref key);
		}

		// Token: 0x06000196 RID: 406
		[NativeThrows]
		[FreeFunction("AnimationCurveBindings::RemoveKey", HasExplicitThis = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveKey(int index);

		// Token: 0x17000012 RID: 18
		public Keyframe this[int index]
		{
			get
			{
				return this.GetKey(index);
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000198 RID: 408
		public extern int length { [NativeMethod("GetKeyCount", IsThreadSafe = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000199 RID: 409
		[FreeFunction("AnimationCurveBindings::SetKeys", HasExplicitThis = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetKeys(Keyframe[] keys);

		// Token: 0x0600019A RID: 410 RVA: 0x00007554 File Offset: 0x00005754
		[FreeFunction("AnimationCurveBindings::GetKey", HasExplicitThis = true, IsThreadSafe = true)]
		[NativeThrows]
		private Keyframe GetKey(int index)
		{
			Keyframe result;
			this.GetKey_Injected(index, out result);
			return result;
		}

		// Token: 0x0600019B RID: 411
		[FreeFunction("AnimationCurveBindings::GetKeys", HasExplicitThis = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Keyframe[] GetKeys();

		// Token: 0x0600019C RID: 412
		[NativeThrows]
		[FreeFunction("AnimationCurveBindings::SmoothTangents", HasExplicitThis = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SmoothTangents(int index, float weight);

		// Token: 0x0600019D RID: 413 RVA: 0x0000756C File Offset: 0x0000576C
		public static AnimationCurve Constant(float timeStart, float timeEnd, float value)
		{
			return AnimationCurve.Linear(timeStart, value, timeEnd, value);
		}

		// Token: 0x0600019E RID: 414 RVA: 0x0000758C File Offset: 0x0000578C
		public static AnimationCurve Linear(float timeStart, float valueStart, float timeEnd, float valueEnd)
		{
			AnimationCurve result;
			if (timeStart == timeEnd)
			{
				Keyframe keyframe = new Keyframe(timeStart, valueStart);
				result = new AnimationCurve(new Keyframe[]
				{
					keyframe
				});
			}
			else
			{
				float num = (valueEnd - valueStart) / (timeEnd - timeStart);
				Keyframe[] keys = new Keyframe[]
				{
					new Keyframe(timeStart, valueStart, 0f, num),
					new Keyframe(timeEnd, valueEnd, num, 0f)
				};
				result = new AnimationCurve(keys);
			}
			return result;
		}

		// Token: 0x0600019F RID: 415 RVA: 0x00007618 File Offset: 0x00005818
		public static AnimationCurve EaseInOut(float timeStart, float valueStart, float timeEnd, float valueEnd)
		{
			AnimationCurve result;
			if (timeStart == timeEnd)
			{
				Keyframe keyframe = new Keyframe(timeStart, valueStart);
				result = new AnimationCurve(new Keyframe[]
				{
					keyframe
				});
			}
			else
			{
				Keyframe[] keys = new Keyframe[]
				{
					new Keyframe(timeStart, valueStart, 0f, 0f),
					new Keyframe(timeEnd, valueEnd, 0f, 0f)
				};
				result = new AnimationCurve(keys);
			}
			return result;
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x060001A0 RID: 416
		// (set) Token: 0x060001A1 RID: 417
		public extern WrapMode preWrapMode { [NativeMethod("GetPreInfinity", IsThreadSafe = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetPreInfinity", IsThreadSafe = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x060001A2 RID: 418
		// (set) Token: 0x060001A3 RID: 419
		public extern WrapMode postWrapMode { [NativeMethod("GetPostInfinity", IsThreadSafe = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetPostInfinity", IsThreadSafe = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060001A4 RID: 420 RVA: 0x000076A4 File Offset: 0x000058A4
		public override bool Equals(object o)
		{
			return !object.ReferenceEquals(null, o) && (object.ReferenceEquals(this, o) || (o.GetType() == base.GetType() && this.Equals((AnimationCurve)o)));
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x00007708 File Offset: 0x00005908
		public bool Equals(AnimationCurve other)
		{
			return !object.ReferenceEquals(null, other) && (object.ReferenceEquals(this, other) || this.m_Ptr.Equals(other.m_Ptr) || this.Internal_Equals(other.m_Ptr));
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x0000777C File Offset: 0x0000597C
		public override int GetHashCode()
		{
			return this.m_Ptr.GetHashCode();
		}

		// Token: 0x060001A7 RID: 423
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int AddKey_Internal_Injected(ref Keyframe key);

		// Token: 0x060001A8 RID: 424
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int MoveKey_Injected(int index, ref Keyframe key);

		// Token: 0x060001A9 RID: 425
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetKey_Injected(int index, out Keyframe ret);

		// Token: 0x04000035 RID: 53
		internal IntPtr m_Ptr;
	}
}
