﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000209 RID: 521
	[RequiredByNativeCode]
	[Obsolete("Use SerializeField on the private variables that you want to be serialized instead")]
	public sealed class SerializePrivateVariables : Attribute
	{
		// Token: 0x060011A6 RID: 4518 RVA: 0x0000898B File Offset: 0x00006B8B
		public SerializePrivateVariables()
		{
		}
	}
}
