﻿using System;

namespace UnityEngine
{
	// Token: 0x02000019 RID: 25
	public enum StackTraceLogType
	{
		// Token: 0x0400003F RID: 63
		None,
		// Token: 0x04000040 RID: 64
		ScriptOnly,
		// Token: 0x04000041 RID: 65
		Full
	}
}
