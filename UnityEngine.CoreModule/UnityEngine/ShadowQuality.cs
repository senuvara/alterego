﻿using System;

namespace UnityEngine
{
	// Token: 0x020000BC RID: 188
	public enum ShadowQuality
	{
		// Token: 0x040001F2 RID: 498
		Disable,
		// Token: 0x040001F3 RID: 499
		HardOnly,
		// Token: 0x040001F4 RID: 500
		All
	}
}
