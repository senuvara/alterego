﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200020C RID: 524
	[RequiredByNativeCode]
	public interface ISerializationCallbackReceiver
	{
		// Token: 0x060011A9 RID: 4521
		[RequiredByNativeCode]
		void OnBeforeSerialize();

		// Token: 0x060011AA RID: 4522
		[RequiredByNativeCode]
		void OnAfterDeserialize();
	}
}
