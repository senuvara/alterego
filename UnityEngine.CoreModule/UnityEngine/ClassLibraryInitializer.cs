﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000052 RID: 82
	internal static class ClassLibraryInitializer
	{
		// Token: 0x0600046B RID: 1131 RVA: 0x0000AD13 File Offset: 0x00008F13
		[RequiredByNativeCode]
		private static void Init()
		{
			UnityLogWriter.Init();
		}
	}
}
