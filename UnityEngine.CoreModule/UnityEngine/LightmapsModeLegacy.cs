﻿using System;

namespace UnityEngine
{
	// Token: 0x0200009B RID: 155
	public enum LightmapsModeLegacy
	{
		// Token: 0x0400019C RID: 412
		Single,
		// Token: 0x0400019D RID: 413
		Dual,
		// Token: 0x0400019E RID: 414
		Directional
	}
}
