﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200009C RID: 156
	[NativeHeader("Runtime/Graphics/GraphicsScriptBindings.h")]
	[NativeHeader("Runtime/Graphics/TrailRenderer.h")]
	public sealed class TrailRenderer : Renderer
	{
		// Token: 0x0600080C RID: 2060 RVA: 0x00008EED File Offset: 0x000070ED
		public TrailRenderer()
		{
		}

		// Token: 0x1700016B RID: 363
		// (get) Token: 0x0600080D RID: 2061 RVA: 0x0001021C File Offset: 0x0000E41C
		[Obsolete("Use positionCount instead (UnityUpgradable) -> positionCount", false)]
		public int numPositions
		{
			get
			{
				return this.positionCount;
			}
		}

		// Token: 0x1700016C RID: 364
		// (get) Token: 0x0600080E RID: 2062
		// (set) Token: 0x0600080F RID: 2063
		public extern float time { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700016D RID: 365
		// (get) Token: 0x06000810 RID: 2064
		// (set) Token: 0x06000811 RID: 2065
		public extern float startWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700016E RID: 366
		// (get) Token: 0x06000812 RID: 2066
		// (set) Token: 0x06000813 RID: 2067
		public extern float endWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700016F RID: 367
		// (get) Token: 0x06000814 RID: 2068
		// (set) Token: 0x06000815 RID: 2069
		public extern float widthMultiplier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000170 RID: 368
		// (get) Token: 0x06000816 RID: 2070
		// (set) Token: 0x06000817 RID: 2071
		public extern bool autodestruct { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000171 RID: 369
		// (get) Token: 0x06000818 RID: 2072
		// (set) Token: 0x06000819 RID: 2073
		public extern bool emitting { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000172 RID: 370
		// (get) Token: 0x0600081A RID: 2074
		// (set) Token: 0x0600081B RID: 2075
		public extern int numCornerVertices { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000173 RID: 371
		// (get) Token: 0x0600081C RID: 2076
		// (set) Token: 0x0600081D RID: 2077
		public extern int numCapVertices { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000174 RID: 372
		// (get) Token: 0x0600081E RID: 2078
		// (set) Token: 0x0600081F RID: 2079
		public extern float minVertexDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000175 RID: 373
		// (get) Token: 0x06000820 RID: 2080 RVA: 0x00010238 File Offset: 0x0000E438
		// (set) Token: 0x06000821 RID: 2081 RVA: 0x0001024E File Offset: 0x0000E44E
		public Color startColor
		{
			get
			{
				Color result;
				this.get_startColor_Injected(out result);
				return result;
			}
			set
			{
				this.set_startColor_Injected(ref value);
			}
		}

		// Token: 0x17000176 RID: 374
		// (get) Token: 0x06000822 RID: 2082 RVA: 0x00010258 File Offset: 0x0000E458
		// (set) Token: 0x06000823 RID: 2083 RVA: 0x0001026E File Offset: 0x0000E46E
		public Color endColor
		{
			get
			{
				Color result;
				this.get_endColor_Injected(out result);
				return result;
			}
			set
			{
				this.set_endColor_Injected(ref value);
			}
		}

		// Token: 0x17000177 RID: 375
		// (get) Token: 0x06000824 RID: 2084
		[NativeProperty("PositionsCount")]
		public extern int positionCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000825 RID: 2085 RVA: 0x00010278 File Offset: 0x0000E478
		public void SetPosition(int index, Vector3 position)
		{
			this.SetPosition_Injected(index, ref position);
		}

		// Token: 0x06000826 RID: 2086 RVA: 0x00010284 File Offset: 0x0000E484
		public Vector3 GetPosition(int index)
		{
			Vector3 result;
			this.GetPosition_Injected(index, out result);
			return result;
		}

		// Token: 0x17000178 RID: 376
		// (get) Token: 0x06000827 RID: 2087
		// (set) Token: 0x06000828 RID: 2088
		public extern float shadowBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000179 RID: 377
		// (get) Token: 0x06000829 RID: 2089
		// (set) Token: 0x0600082A RID: 2090
		public extern bool generateLightingData { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700017A RID: 378
		// (get) Token: 0x0600082B RID: 2091
		// (set) Token: 0x0600082C RID: 2092
		public extern LineTextureMode textureMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700017B RID: 379
		// (get) Token: 0x0600082D RID: 2093
		// (set) Token: 0x0600082E RID: 2094
		public extern LineAlignment alignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600082F RID: 2095
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Clear();

		// Token: 0x06000830 RID: 2096 RVA: 0x0001029B File Offset: 0x0000E49B
		public void BakeMesh(Mesh mesh, bool useTransform = false)
		{
			this.BakeMesh(mesh, Camera.main, useTransform);
		}

		// Token: 0x06000831 RID: 2097
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void BakeMesh([NotNull] Mesh mesh, [NotNull] Camera camera, bool useTransform = false);

		// Token: 0x1700017C RID: 380
		// (get) Token: 0x06000832 RID: 2098 RVA: 0x000102AC File Offset: 0x0000E4AC
		// (set) Token: 0x06000833 RID: 2099 RVA: 0x000102C7 File Offset: 0x0000E4C7
		public AnimationCurve widthCurve
		{
			get
			{
				return this.GetWidthCurveCopy();
			}
			set
			{
				this.SetWidthCurve(value);
			}
		}

		// Token: 0x1700017D RID: 381
		// (get) Token: 0x06000834 RID: 2100 RVA: 0x000102D4 File Offset: 0x0000E4D4
		// (set) Token: 0x06000835 RID: 2101 RVA: 0x000102EF File Offset: 0x0000E4EF
		public Gradient colorGradient
		{
			get
			{
				return this.GetColorGradientCopy();
			}
			set
			{
				this.SetColorGradient(value);
			}
		}

		// Token: 0x06000836 RID: 2102
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimationCurve GetWidthCurveCopy();

		// Token: 0x06000837 RID: 2103
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetWidthCurve([NotNull] AnimationCurve curve);

		// Token: 0x06000838 RID: 2104
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Gradient GetColorGradientCopy();

		// Token: 0x06000839 RID: 2105
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetColorGradient([NotNull] Gradient curve);

		// Token: 0x0600083A RID: 2106
		[FreeFunction(Name = "TrailRendererScripting::GetPositions", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetPositions([NotNull] [Out] Vector3[] positions);

		// Token: 0x0600083B RID: 2107
		[FreeFunction(Name = "TrailRendererScripting::SetPositions", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPositions([NotNull] Vector3[] positions);

		// Token: 0x0600083C RID: 2108 RVA: 0x000102F9 File Offset: 0x0000E4F9
		[FreeFunction(Name = "TrailRendererScripting::AddPosition", HasExplicitThis = true)]
		public void AddPosition(Vector3 position)
		{
			this.AddPosition_Injected(ref position);
		}

		// Token: 0x0600083D RID: 2109
		[FreeFunction(Name = "TrailRendererScripting::AddPositions", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddPositions([NotNull] Vector3[] positions);

		// Token: 0x0600083E RID: 2110
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_startColor_Injected(out Color ret);

		// Token: 0x0600083F RID: 2111
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_startColor_Injected(ref Color value);

		// Token: 0x06000840 RID: 2112
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_endColor_Injected(out Color ret);

		// Token: 0x06000841 RID: 2113
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_endColor_Injected(ref Color value);

		// Token: 0x06000842 RID: 2114
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetPosition_Injected(int index, ref Vector3 position);

		// Token: 0x06000843 RID: 2115
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetPosition_Injected(int index, out Vector3 ret);

		// Token: 0x06000844 RID: 2116
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AddPosition_Injected(ref Vector3 position);
	}
}
