﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200035D RID: 861
	[RequireComponent(typeof(Transform))]
	[NativeType("Runtime/Graphics/Mesh/SpriteRenderer.h")]
	public sealed class SpriteRenderer : Renderer
	{
		// Token: 0x06001D54 RID: 7508 RVA: 0x00008EED File Offset: 0x000070ED
		public SpriteRenderer()
		{
		}

		// Token: 0x1700055C RID: 1372
		// (get) Token: 0x06001D55 RID: 7509
		internal extern bool shouldSupportTiling { [NativeMethod("ShouldSupportTiling")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700055D RID: 1373
		// (get) Token: 0x06001D56 RID: 7510
		// (set) Token: 0x06001D57 RID: 7511
		public extern Sprite sprite { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700055E RID: 1374
		// (get) Token: 0x06001D58 RID: 7512
		// (set) Token: 0x06001D59 RID: 7513
		public extern SpriteDrawMode drawMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700055F RID: 1375
		// (get) Token: 0x06001D5A RID: 7514 RVA: 0x00032FE0 File Offset: 0x000311E0
		// (set) Token: 0x06001D5B RID: 7515 RVA: 0x00032FF6 File Offset: 0x000311F6
		public Vector2 size
		{
			get
			{
				Vector2 result;
				this.get_size_Injected(out result);
				return result;
			}
			set
			{
				this.set_size_Injected(ref value);
			}
		}

		// Token: 0x17000560 RID: 1376
		// (get) Token: 0x06001D5C RID: 7516
		// (set) Token: 0x06001D5D RID: 7517
		public extern float adaptiveModeThreshold { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000561 RID: 1377
		// (get) Token: 0x06001D5E RID: 7518
		// (set) Token: 0x06001D5F RID: 7519
		public extern SpriteTileMode tileMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000562 RID: 1378
		// (get) Token: 0x06001D60 RID: 7520 RVA: 0x00033000 File Offset: 0x00031200
		// (set) Token: 0x06001D61 RID: 7521 RVA: 0x00033016 File Offset: 0x00031216
		public Color color
		{
			get
			{
				Color result;
				this.get_color_Injected(out result);
				return result;
			}
			set
			{
				this.set_color_Injected(ref value);
			}
		}

		// Token: 0x17000563 RID: 1379
		// (get) Token: 0x06001D62 RID: 7522
		// (set) Token: 0x06001D63 RID: 7523
		public extern SpriteMaskInteraction maskInteraction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000564 RID: 1380
		// (get) Token: 0x06001D64 RID: 7524
		// (set) Token: 0x06001D65 RID: 7525
		public extern bool flipX { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000565 RID: 1381
		// (get) Token: 0x06001D66 RID: 7526
		// (set) Token: 0x06001D67 RID: 7527
		public extern bool flipY { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000566 RID: 1382
		// (get) Token: 0x06001D68 RID: 7528
		// (set) Token: 0x06001D69 RID: 7529
		public extern SpriteSortPoint spriteSortPoint { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001D6A RID: 7530 RVA: 0x00033020 File Offset: 0x00031220
		[NativeMethod(Name = "GetSpriteBounds")]
		internal Bounds Internal_GetSpriteBounds(SpriteDrawMode mode)
		{
			Bounds result;
			this.Internal_GetSpriteBounds_Injected(mode, out result);
			return result;
		}

		// Token: 0x06001D6B RID: 7531 RVA: 0x00033038 File Offset: 0x00031238
		internal Bounds GetSpriteBounds()
		{
			return this.Internal_GetSpriteBounds(this.drawMode);
		}

		// Token: 0x06001D6C RID: 7532
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_size_Injected(out Vector2 ret);

		// Token: 0x06001D6D RID: 7533
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_size_Injected(ref Vector2 value);

		// Token: 0x06001D6E RID: 7534
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_color_Injected(out Color ret);

		// Token: 0x06001D6F RID: 7535
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_color_Injected(ref Color value);

		// Token: 0x06001D70 RID: 7536
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetSpriteBounds_Injected(SpriteDrawMode mode, out Bounds ret);
	}
}
