﻿using System;

namespace UnityEngine
{
	// Token: 0x020001E9 RID: 489
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = true)]
	public class SpaceAttribute : PropertyAttribute
	{
		// Token: 0x06000F80 RID: 3968 RVA: 0x0001ACF6 File Offset: 0x00018EF6
		public SpaceAttribute()
		{
			this.height = 8f;
		}

		// Token: 0x06000F81 RID: 3969 RVA: 0x0001AD0A File Offset: 0x00018F0A
		public SpaceAttribute(float height)
		{
			this.height = height;
		}

		// Token: 0x04000720 RID: 1824
		public readonly float height;
	}
}
