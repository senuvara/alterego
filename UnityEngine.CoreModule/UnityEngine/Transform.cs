﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000350 RID: 848
	[NativeHeader("Runtime/Transform/Transform.h")]
	[NativeHeader("Runtime/Transform/ScriptBindings/TransformScriptBindings.h")]
	[RequiredByNativeCode]
	[NativeHeader("Configuration/UnityConfigure.h")]
	public class Transform : Component, IEnumerable
	{
		// Token: 0x06001C69 RID: 7273 RVA: 0x00032089 File Offset: 0x00030289
		protected Transform()
		{
		}

		// Token: 0x17000533 RID: 1331
		// (get) Token: 0x06001C6A RID: 7274 RVA: 0x00032094 File Offset: 0x00030294
		// (set) Token: 0x06001C6B RID: 7275 RVA: 0x000320AA File Offset: 0x000302AA
		public Vector3 position
		{
			get
			{
				Vector3 result;
				this.get_position_Injected(out result);
				return result;
			}
			set
			{
				this.set_position_Injected(ref value);
			}
		}

		// Token: 0x17000534 RID: 1332
		// (get) Token: 0x06001C6C RID: 7276 RVA: 0x000320B4 File Offset: 0x000302B4
		// (set) Token: 0x06001C6D RID: 7277 RVA: 0x000320CA File Offset: 0x000302CA
		public Vector3 localPosition
		{
			get
			{
				Vector3 result;
				this.get_localPosition_Injected(out result);
				return result;
			}
			set
			{
				this.set_localPosition_Injected(ref value);
			}
		}

		// Token: 0x06001C6E RID: 7278 RVA: 0x000320D4 File Offset: 0x000302D4
		internal Vector3 GetLocalEulerAngles(RotationOrder order)
		{
			Vector3 result;
			this.GetLocalEulerAngles_Injected(order, out result);
			return result;
		}

		// Token: 0x06001C6F RID: 7279 RVA: 0x000320EB File Offset: 0x000302EB
		internal void SetLocalEulerAngles(Vector3 euler, RotationOrder order)
		{
			this.SetLocalEulerAngles_Injected(ref euler, order);
		}

		// Token: 0x06001C70 RID: 7280 RVA: 0x000320F6 File Offset: 0x000302F6
		[NativeConditional("UNITY_EDITOR")]
		internal void SetLocalEulerHint(Vector3 euler)
		{
			this.SetLocalEulerHint_Injected(ref euler);
		}

		// Token: 0x17000535 RID: 1333
		// (get) Token: 0x06001C71 RID: 7281 RVA: 0x00032100 File Offset: 0x00030300
		// (set) Token: 0x06001C72 RID: 7282 RVA: 0x00032123 File Offset: 0x00030323
		public Vector3 eulerAngles
		{
			get
			{
				return this.rotation.eulerAngles;
			}
			set
			{
				this.rotation = Quaternion.Euler(value);
			}
		}

		// Token: 0x17000536 RID: 1334
		// (get) Token: 0x06001C73 RID: 7283 RVA: 0x00032134 File Offset: 0x00030334
		// (set) Token: 0x06001C74 RID: 7284 RVA: 0x00032157 File Offset: 0x00030357
		public Vector3 localEulerAngles
		{
			get
			{
				return this.localRotation.eulerAngles;
			}
			set
			{
				this.localRotation = Quaternion.Euler(value);
			}
		}

		// Token: 0x17000537 RID: 1335
		// (get) Token: 0x06001C75 RID: 7285 RVA: 0x00032168 File Offset: 0x00030368
		// (set) Token: 0x06001C76 RID: 7286 RVA: 0x0003218D File Offset: 0x0003038D
		public Vector3 right
		{
			get
			{
				return this.rotation * Vector3.right;
			}
			set
			{
				this.rotation = Quaternion.FromToRotation(Vector3.right, value);
			}
		}

		// Token: 0x17000538 RID: 1336
		// (get) Token: 0x06001C77 RID: 7287 RVA: 0x000321A4 File Offset: 0x000303A4
		// (set) Token: 0x06001C78 RID: 7288 RVA: 0x000321C9 File Offset: 0x000303C9
		public Vector3 up
		{
			get
			{
				return this.rotation * Vector3.up;
			}
			set
			{
				this.rotation = Quaternion.FromToRotation(Vector3.up, value);
			}
		}

		// Token: 0x17000539 RID: 1337
		// (get) Token: 0x06001C79 RID: 7289 RVA: 0x000321E0 File Offset: 0x000303E0
		// (set) Token: 0x06001C7A RID: 7290 RVA: 0x00032205 File Offset: 0x00030405
		public Vector3 forward
		{
			get
			{
				return this.rotation * Vector3.forward;
			}
			set
			{
				this.rotation = Quaternion.LookRotation(value);
			}
		}

		// Token: 0x1700053A RID: 1338
		// (get) Token: 0x06001C7B RID: 7291 RVA: 0x00032214 File Offset: 0x00030414
		// (set) Token: 0x06001C7C RID: 7292 RVA: 0x0003222A File Offset: 0x0003042A
		public Quaternion rotation
		{
			get
			{
				Quaternion result;
				this.get_rotation_Injected(out result);
				return result;
			}
			set
			{
				this.set_rotation_Injected(ref value);
			}
		}

		// Token: 0x1700053B RID: 1339
		// (get) Token: 0x06001C7D RID: 7293 RVA: 0x00032234 File Offset: 0x00030434
		// (set) Token: 0x06001C7E RID: 7294 RVA: 0x0003224A File Offset: 0x0003044A
		public Quaternion localRotation
		{
			get
			{
				Quaternion result;
				this.get_localRotation_Injected(out result);
				return result;
			}
			set
			{
				this.set_localRotation_Injected(ref value);
			}
		}

		// Token: 0x1700053C RID: 1340
		// (get) Token: 0x06001C7F RID: 7295 RVA: 0x00032254 File Offset: 0x00030454
		// (set) Token: 0x06001C80 RID: 7296 RVA: 0x0003226F File Offset: 0x0003046F
		[NativeConditional("UNITY_EDITOR")]
		internal RotationOrder rotationOrder
		{
			get
			{
				return (RotationOrder)this.GetRotationOrderInternal();
			}
			set
			{
				this.SetRotationOrderInternal(value);
			}
		}

		// Token: 0x06001C81 RID: 7297
		[NativeConditional("UNITY_EDITOR")]
		[NativeMethod("GetRotationOrder")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetRotationOrderInternal();

		// Token: 0x06001C82 RID: 7298
		[NativeConditional("UNITY_EDITOR")]
		[NativeMethod("SetRotationOrder")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetRotationOrderInternal(RotationOrder rotationOrder);

		// Token: 0x1700053D RID: 1341
		// (get) Token: 0x06001C83 RID: 7299 RVA: 0x0003227C File Offset: 0x0003047C
		// (set) Token: 0x06001C84 RID: 7300 RVA: 0x00032292 File Offset: 0x00030492
		public Vector3 localScale
		{
			get
			{
				Vector3 result;
				this.get_localScale_Injected(out result);
				return result;
			}
			set
			{
				this.set_localScale_Injected(ref value);
			}
		}

		// Token: 0x1700053E RID: 1342
		// (get) Token: 0x06001C85 RID: 7301 RVA: 0x0003229C File Offset: 0x0003049C
		// (set) Token: 0x06001C86 RID: 7302 RVA: 0x000322B7 File Offset: 0x000304B7
		public Transform parent
		{
			get
			{
				return this.parentInternal;
			}
			set
			{
				if (this is RectTransform)
				{
					Debug.LogWarning("Parent of RectTransform is being set with parent property. Consider using the SetParent method instead, with the worldPositionStays argument set to false. This will retain local orientation and scale rather than world orientation and scale, which can prevent common UI scaling issues.", this);
				}
				this.parentInternal = value;
			}
		}

		// Token: 0x1700053F RID: 1343
		// (get) Token: 0x06001C87 RID: 7303 RVA: 0x000322D8 File Offset: 0x000304D8
		// (set) Token: 0x06001C88 RID: 7304 RVA: 0x000322F3 File Offset: 0x000304F3
		internal Transform parentInternal
		{
			get
			{
				return this.GetParent();
			}
			set
			{
				this.SetParent(value);
			}
		}

		// Token: 0x06001C89 RID: 7305
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Transform GetParent();

		// Token: 0x06001C8A RID: 7306 RVA: 0x000322FD File Offset: 0x000304FD
		public void SetParent(Transform p)
		{
			this.SetParent(p, true);
		}

		// Token: 0x06001C8B RID: 7307
		[FreeFunction("SetParent", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetParent(Transform parent, bool worldPositionStays);

		// Token: 0x17000540 RID: 1344
		// (get) Token: 0x06001C8C RID: 7308 RVA: 0x00032308 File Offset: 0x00030508
		public Matrix4x4 worldToLocalMatrix
		{
			get
			{
				Matrix4x4 result;
				this.get_worldToLocalMatrix_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000541 RID: 1345
		// (get) Token: 0x06001C8D RID: 7309 RVA: 0x00032320 File Offset: 0x00030520
		public Matrix4x4 localToWorldMatrix
		{
			get
			{
				Matrix4x4 result;
				this.get_localToWorldMatrix_Injected(out result);
				return result;
			}
		}

		// Token: 0x06001C8E RID: 7310 RVA: 0x00032336 File Offset: 0x00030536
		public void SetPositionAndRotation(Vector3 position, Quaternion rotation)
		{
			this.SetPositionAndRotation_Injected(ref position, ref rotation);
		}

		// Token: 0x06001C8F RID: 7311 RVA: 0x00032342 File Offset: 0x00030542
		public void Translate(Vector3 translation, [DefaultValue("Space.Self")] Space relativeTo)
		{
			if (relativeTo == Space.World)
			{
				this.position += translation;
			}
			else
			{
				this.position += this.TransformDirection(translation);
			}
		}

		// Token: 0x06001C90 RID: 7312 RVA: 0x0003237A File Offset: 0x0003057A
		public void Translate(Vector3 translation)
		{
			this.Translate(translation, Space.Self);
		}

		// Token: 0x06001C91 RID: 7313 RVA: 0x00032385 File Offset: 0x00030585
		public void Translate(float x, float y, float z, [DefaultValue("Space.Self")] Space relativeTo)
		{
			this.Translate(new Vector3(x, y, z), relativeTo);
		}

		// Token: 0x06001C92 RID: 7314 RVA: 0x00032398 File Offset: 0x00030598
		public void Translate(float x, float y, float z)
		{
			this.Translate(new Vector3(x, y, z), Space.Self);
		}

		// Token: 0x06001C93 RID: 7315 RVA: 0x000323AA File Offset: 0x000305AA
		public void Translate(Vector3 translation, Transform relativeTo)
		{
			if (relativeTo)
			{
				this.position += relativeTo.TransformDirection(translation);
			}
			else
			{
				this.position += translation;
			}
		}

		// Token: 0x06001C94 RID: 7316 RVA: 0x000323E7 File Offset: 0x000305E7
		public void Translate(float x, float y, float z, Transform relativeTo)
		{
			this.Translate(new Vector3(x, y, z), relativeTo);
		}

		// Token: 0x06001C95 RID: 7317 RVA: 0x000323FC File Offset: 0x000305FC
		public void Rotate(Vector3 eulers, [DefaultValue("Space.Self")] Space relativeTo)
		{
			Quaternion rhs = Quaternion.Euler(eulers.x, eulers.y, eulers.z);
			if (relativeTo == Space.Self)
			{
				this.localRotation *= rhs;
			}
			else
			{
				this.rotation *= Quaternion.Inverse(this.rotation) * rhs * this.rotation;
			}
		}

		// Token: 0x06001C96 RID: 7318 RVA: 0x00032472 File Offset: 0x00030672
		public void Rotate(Vector3 eulers)
		{
			this.Rotate(eulers, Space.Self);
		}

		// Token: 0x06001C97 RID: 7319 RVA: 0x0003247D File Offset: 0x0003067D
		public void Rotate(float xAngle, float yAngle, float zAngle, [DefaultValue("Space.Self")] Space relativeTo)
		{
			this.Rotate(new Vector3(xAngle, yAngle, zAngle), relativeTo);
		}

		// Token: 0x06001C98 RID: 7320 RVA: 0x00032490 File Offset: 0x00030690
		public void Rotate(float xAngle, float yAngle, float zAngle)
		{
			this.Rotate(new Vector3(xAngle, yAngle, zAngle), Space.Self);
		}

		// Token: 0x06001C99 RID: 7321 RVA: 0x000324A2 File Offset: 0x000306A2
		[NativeMethod("RotateAround")]
		internal void RotateAroundInternal(Vector3 axis, float angle)
		{
			this.RotateAroundInternal_Injected(ref axis, angle);
		}

		// Token: 0x06001C9A RID: 7322 RVA: 0x000324AD File Offset: 0x000306AD
		public void Rotate(Vector3 axis, float angle, [DefaultValue("Space.Self")] Space relativeTo)
		{
			if (relativeTo == Space.Self)
			{
				this.RotateAroundInternal(base.transform.TransformDirection(axis), angle * 0.017453292f);
			}
			else
			{
				this.RotateAroundInternal(axis, angle * 0.017453292f);
			}
		}

		// Token: 0x06001C9B RID: 7323 RVA: 0x000324E3 File Offset: 0x000306E3
		public void Rotate(Vector3 axis, float angle)
		{
			this.Rotate(axis, angle, Space.Self);
		}

		// Token: 0x06001C9C RID: 7324 RVA: 0x000324F0 File Offset: 0x000306F0
		public void RotateAround(Vector3 point, Vector3 axis, float angle)
		{
			Vector3 vector = this.position;
			Quaternion rotation = Quaternion.AngleAxis(angle, axis);
			Vector3 vector2 = vector - point;
			vector2 = rotation * vector2;
			vector = point + vector2;
			this.position = vector;
			this.RotateAroundInternal(axis, angle * 0.017453292f);
		}

		// Token: 0x06001C9D RID: 7325 RVA: 0x0003253A File Offset: 0x0003073A
		public void LookAt(Transform target, [DefaultValue("Vector3.up")] Vector3 worldUp)
		{
			if (target)
			{
				this.LookAt(target.position, worldUp);
			}
		}

		// Token: 0x06001C9E RID: 7326 RVA: 0x00032555 File Offset: 0x00030755
		public void LookAt(Transform target)
		{
			if (target)
			{
				this.LookAt(target.position, Vector3.up);
			}
		}

		// Token: 0x06001C9F RID: 7327 RVA: 0x00032574 File Offset: 0x00030774
		public void LookAt(Vector3 worldPosition, [DefaultValue("Vector3.up")] Vector3 worldUp)
		{
			this.Internal_LookAt(worldPosition, worldUp);
		}

		// Token: 0x06001CA0 RID: 7328 RVA: 0x0003257F File Offset: 0x0003077F
		public void LookAt(Vector3 worldPosition)
		{
			this.Internal_LookAt(worldPosition, Vector3.up);
		}

		// Token: 0x06001CA1 RID: 7329 RVA: 0x0003258E File Offset: 0x0003078E
		[FreeFunction("Internal_LookAt", HasExplicitThis = true)]
		private void Internal_LookAt(Vector3 worldPosition, Vector3 worldUp)
		{
			this.Internal_LookAt_Injected(ref worldPosition, ref worldUp);
		}

		// Token: 0x06001CA2 RID: 7330 RVA: 0x0003259C File Offset: 0x0003079C
		public Vector3 TransformDirection(Vector3 direction)
		{
			Vector3 result;
			this.TransformDirection_Injected(ref direction, out result);
			return result;
		}

		// Token: 0x06001CA3 RID: 7331 RVA: 0x000325B4 File Offset: 0x000307B4
		public Vector3 TransformDirection(float x, float y, float z)
		{
			return this.TransformDirection(new Vector3(x, y, z));
		}

		// Token: 0x06001CA4 RID: 7332 RVA: 0x000325D8 File Offset: 0x000307D8
		public Vector3 InverseTransformDirection(Vector3 direction)
		{
			Vector3 result;
			this.InverseTransformDirection_Injected(ref direction, out result);
			return result;
		}

		// Token: 0x06001CA5 RID: 7333 RVA: 0x000325F0 File Offset: 0x000307F0
		public Vector3 InverseTransformDirection(float x, float y, float z)
		{
			return this.InverseTransformDirection(new Vector3(x, y, z));
		}

		// Token: 0x06001CA6 RID: 7334 RVA: 0x00032614 File Offset: 0x00030814
		public Vector3 TransformVector(Vector3 vector)
		{
			Vector3 result;
			this.TransformVector_Injected(ref vector, out result);
			return result;
		}

		// Token: 0x06001CA7 RID: 7335 RVA: 0x0003262C File Offset: 0x0003082C
		public Vector3 TransformVector(float x, float y, float z)
		{
			return this.TransformVector(new Vector3(x, y, z));
		}

		// Token: 0x06001CA8 RID: 7336 RVA: 0x00032650 File Offset: 0x00030850
		public Vector3 InverseTransformVector(Vector3 vector)
		{
			Vector3 result;
			this.InverseTransformVector_Injected(ref vector, out result);
			return result;
		}

		// Token: 0x06001CA9 RID: 7337 RVA: 0x00032668 File Offset: 0x00030868
		public Vector3 InverseTransformVector(float x, float y, float z)
		{
			return this.InverseTransformVector(new Vector3(x, y, z));
		}

		// Token: 0x06001CAA RID: 7338 RVA: 0x0003268C File Offset: 0x0003088C
		public Vector3 TransformPoint(Vector3 position)
		{
			Vector3 result;
			this.TransformPoint_Injected(ref position, out result);
			return result;
		}

		// Token: 0x06001CAB RID: 7339 RVA: 0x000326A4 File Offset: 0x000308A4
		public Vector3 TransformPoint(float x, float y, float z)
		{
			return this.TransformPoint(new Vector3(x, y, z));
		}

		// Token: 0x06001CAC RID: 7340 RVA: 0x000326C8 File Offset: 0x000308C8
		public Vector3 InverseTransformPoint(Vector3 position)
		{
			Vector3 result;
			this.InverseTransformPoint_Injected(ref position, out result);
			return result;
		}

		// Token: 0x06001CAD RID: 7341 RVA: 0x000326E0 File Offset: 0x000308E0
		public Vector3 InverseTransformPoint(float x, float y, float z)
		{
			return this.InverseTransformPoint(new Vector3(x, y, z));
		}

		// Token: 0x17000542 RID: 1346
		// (get) Token: 0x06001CAE RID: 7342 RVA: 0x00032704 File Offset: 0x00030904
		public Transform root
		{
			get
			{
				return this.GetRoot();
			}
		}

		// Token: 0x06001CAF RID: 7343
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Transform GetRoot();

		// Token: 0x17000543 RID: 1347
		// (get) Token: 0x06001CB0 RID: 7344
		public extern int childCount { [NativeMethod("GetChildrenCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001CB1 RID: 7345
		[FreeFunction("DetachChildren", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void DetachChildren();

		// Token: 0x06001CB2 RID: 7346
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetAsFirstSibling();

		// Token: 0x06001CB3 RID: 7347
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetAsLastSibling();

		// Token: 0x06001CB4 RID: 7348
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetSiblingIndex(int index);

		// Token: 0x06001CB5 RID: 7349
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetSiblingIndex();

		// Token: 0x06001CB6 RID: 7350
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Transform FindRelativeTransformWithPath(Transform transform, string path, [DefaultValue("false")] bool isActiveOnly);

		// Token: 0x06001CB7 RID: 7351 RVA: 0x00032720 File Offset: 0x00030920
		public Transform Find(string n)
		{
			if (n == null)
			{
				throw new ArgumentNullException("Name cannot be null");
			}
			return Transform.FindRelativeTransformWithPath(this, n, false);
		}

		// Token: 0x06001CB8 RID: 7352
		[NativeConditional("UNITY_EDITOR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SendTransformChangedScale();

		// Token: 0x17000544 RID: 1348
		// (get) Token: 0x06001CB9 RID: 7353 RVA: 0x00032750 File Offset: 0x00030950
		public Vector3 lossyScale
		{
			[NativeMethod("GetWorldScaleLossy")]
			get
			{
				Vector3 result;
				this.get_lossyScale_Injected(out result);
				return result;
			}
		}

		// Token: 0x06001CBA RID: 7354
		[FreeFunction("Internal_IsChildOrSameTransform", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsChildOf([NotNull] Transform parent);

		// Token: 0x17000545 RID: 1349
		// (get) Token: 0x06001CBB RID: 7355
		// (set) Token: 0x06001CBC RID: 7356
		[NativeProperty("HasChangedDeprecated")]
		public extern bool hasChanged { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001CBD RID: 7357 RVA: 0x00032768 File Offset: 0x00030968
		[Obsolete("FindChild has been deprecated. Use Find instead (UnityUpgradable) -> Find([mscorlib] System.String)", false)]
		public Transform FindChild(string n)
		{
			return this.Find(n);
		}

		// Token: 0x06001CBE RID: 7358 RVA: 0x00032784 File Offset: 0x00030984
		public IEnumerator GetEnumerator()
		{
			return new Transform.Enumerator(this);
		}

		// Token: 0x06001CBF RID: 7359 RVA: 0x0003279F File Offset: 0x0003099F
		[Obsolete("warning use Transform.Rotate instead.")]
		public void RotateAround(Vector3 axis, float angle)
		{
			this.RotateAround_Injected(ref axis, angle);
		}

		// Token: 0x06001CC0 RID: 7360 RVA: 0x000327AA File Offset: 0x000309AA
		[Obsolete("warning use Transform.Rotate instead.")]
		public void RotateAroundLocal(Vector3 axis, float angle)
		{
			this.RotateAroundLocal_Injected(ref axis, angle);
		}

		// Token: 0x06001CC1 RID: 7361
		[FreeFunction("GetChild", HasExplicitThis = true)]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Transform GetChild(int index);

		// Token: 0x06001CC2 RID: 7362
		[Obsolete("warning use Transform.childCount instead (UnityUpgradable) -> Transform.childCount", false)]
		[NativeMethod("GetChildrenCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetChildCount();

		// Token: 0x17000546 RID: 1350
		// (get) Token: 0x06001CC3 RID: 7363 RVA: 0x000327B8 File Offset: 0x000309B8
		// (set) Token: 0x06001CC4 RID: 7364 RVA: 0x000327D3 File Offset: 0x000309D3
		public int hierarchyCapacity
		{
			get
			{
				return this.internal_getHierarchyCapacity();
			}
			set
			{
				this.internal_setHierarchyCapacity(value);
			}
		}

		// Token: 0x06001CC5 RID: 7365
		[FreeFunction("GetHierarchyCapacity", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int internal_getHierarchyCapacity();

		// Token: 0x06001CC6 RID: 7366
		[FreeFunction("SetHierarchyCapacity", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void internal_setHierarchyCapacity(int value);

		// Token: 0x17000547 RID: 1351
		// (get) Token: 0x06001CC7 RID: 7367 RVA: 0x000327E0 File Offset: 0x000309E0
		public int hierarchyCount
		{
			get
			{
				return this.internal_getHierarchyCount();
			}
		}

		// Token: 0x06001CC8 RID: 7368
		[FreeFunction("GetHierarchyCount", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int internal_getHierarchyCount();

		// Token: 0x06001CC9 RID: 7369
		[NativeConditional("UNITY_EDITOR")]
		[FreeFunction("IsNonUniformScaleTransform", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool IsNonUniformScaleTransform();

		// Token: 0x06001CCA RID: 7370
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_position_Injected(out Vector3 ret);

		// Token: 0x06001CCB RID: 7371
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_position_Injected(ref Vector3 value);

		// Token: 0x06001CCC RID: 7372
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_localPosition_Injected(out Vector3 ret);

		// Token: 0x06001CCD RID: 7373
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_localPosition_Injected(ref Vector3 value);

		// Token: 0x06001CCE RID: 7374
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetLocalEulerAngles_Injected(RotationOrder order, out Vector3 ret);

		// Token: 0x06001CCF RID: 7375
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetLocalEulerAngles_Injected(ref Vector3 euler, RotationOrder order);

		// Token: 0x06001CD0 RID: 7376
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetLocalEulerHint_Injected(ref Vector3 euler);

		// Token: 0x06001CD1 RID: 7377
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rotation_Injected(out Quaternion ret);

		// Token: 0x06001CD2 RID: 7378
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_rotation_Injected(ref Quaternion value);

		// Token: 0x06001CD3 RID: 7379
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_localRotation_Injected(out Quaternion ret);

		// Token: 0x06001CD4 RID: 7380
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_localRotation_Injected(ref Quaternion value);

		// Token: 0x06001CD5 RID: 7381
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_localScale_Injected(out Vector3 ret);

		// Token: 0x06001CD6 RID: 7382
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_localScale_Injected(ref Vector3 value);

		// Token: 0x06001CD7 RID: 7383
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_worldToLocalMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x06001CD8 RID: 7384
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_localToWorldMatrix_Injected(out Matrix4x4 ret);

		// Token: 0x06001CD9 RID: 7385
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetPositionAndRotation_Injected(ref Vector3 position, ref Quaternion rotation);

		// Token: 0x06001CDA RID: 7386
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RotateAroundInternal_Injected(ref Vector3 axis, float angle);

		// Token: 0x06001CDB RID: 7387
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_LookAt_Injected(ref Vector3 worldPosition, ref Vector3 worldUp);

		// Token: 0x06001CDC RID: 7388
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void TransformDirection_Injected(ref Vector3 direction, out Vector3 ret);

		// Token: 0x06001CDD RID: 7389
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InverseTransformDirection_Injected(ref Vector3 direction, out Vector3 ret);

		// Token: 0x06001CDE RID: 7390
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void TransformVector_Injected(ref Vector3 vector, out Vector3 ret);

		// Token: 0x06001CDF RID: 7391
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InverseTransformVector_Injected(ref Vector3 vector, out Vector3 ret);

		// Token: 0x06001CE0 RID: 7392
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void TransformPoint_Injected(ref Vector3 position, out Vector3 ret);

		// Token: 0x06001CE1 RID: 7393
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InverseTransformPoint_Injected(ref Vector3 position, out Vector3 ret);

		// Token: 0x06001CE2 RID: 7394
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_lossyScale_Injected(out Vector3 ret);

		// Token: 0x06001CE3 RID: 7395
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RotateAround_Injected(ref Vector3 axis, float angle);

		// Token: 0x06001CE4 RID: 7396
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RotateAroundLocal_Injected(ref Vector3 axis, float angle);

		// Token: 0x02000351 RID: 849
		private class Enumerator : IEnumerator
		{
			// Token: 0x06001CE5 RID: 7397 RVA: 0x000327FB File Offset: 0x000309FB
			internal Enumerator(Transform outer)
			{
				this.outer = outer;
			}

			// Token: 0x17000548 RID: 1352
			// (get) Token: 0x06001CE6 RID: 7398 RVA: 0x00032814 File Offset: 0x00030A14
			public object Current
			{
				get
				{
					return this.outer.GetChild(this.currentIndex);
				}
			}

			// Token: 0x06001CE7 RID: 7399 RVA: 0x0003283C File Offset: 0x00030A3C
			public bool MoveNext()
			{
				int childCount = this.outer.childCount;
				return ++this.currentIndex < childCount;
			}

			// Token: 0x06001CE8 RID: 7400 RVA: 0x00032871 File Offset: 0x00030A71
			public void Reset()
			{
				this.currentIndex = -1;
			}

			// Token: 0x04000AD2 RID: 2770
			private Transform outer;

			// Token: 0x04000AD3 RID: 2771
			private int currentIndex = -1;
		}
	}
}
