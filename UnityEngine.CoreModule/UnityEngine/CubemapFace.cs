﻿using System;

namespace UnityEngine
{
	// Token: 0x020000CD RID: 205
	public enum CubemapFace
	{
		// Token: 0x0400027B RID: 635
		Unknown = -1,
		// Token: 0x0400027C RID: 636
		PositiveX,
		// Token: 0x0400027D RID: 637
		NegativeX,
		// Token: 0x0400027E RID: 638
		PositiveY,
		// Token: 0x0400027F RID: 639
		NegativeY,
		// Token: 0x04000280 RID: 640
		PositiveZ,
		// Token: 0x04000281 RID: 641
		NegativeZ
	}
}
