﻿using System;

namespace UnityEngine
{
	// Token: 0x020000E3 RID: 227
	public enum LineAlignment
	{
		// Token: 0x0400037C RID: 892
		View,
		// Token: 0x0400037D RID: 893
		[Obsolete("Enum member Local has been deprecated. Use TransformZ instead (UnityUpgradable) -> TransformZ", false)]
		Local,
		// Token: 0x0400037E RID: 894
		TransformZ = 1
	}
}
