﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200006A RID: 106
	[UsedByNativeCode]
	[NativeHeader("Runtime/Graphics/DisplayManager.h")]
	public class Display
	{
		// Token: 0x0600056E RID: 1390 RVA: 0x0000D246 File Offset: 0x0000B446
		internal Display()
		{
			this.nativeDisplay = new IntPtr(0);
		}

		// Token: 0x0600056F RID: 1391 RVA: 0x0000D25B File Offset: 0x0000B45B
		internal Display(IntPtr nativeDisplay)
		{
			this.nativeDisplay = nativeDisplay;
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000570 RID: 1392 RVA: 0x0000D26C File Offset: 0x0000B46C
		public int renderingWidth
		{
			get
			{
				int result = 0;
				int num = 0;
				Display.GetRenderingExtImpl(this.nativeDisplay, out result, out num);
				return result;
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000571 RID: 1393 RVA: 0x0000D298 File Offset: 0x0000B498
		public int renderingHeight
		{
			get
			{
				int num = 0;
				int result = 0;
				Display.GetRenderingExtImpl(this.nativeDisplay, out num, out result);
				return result;
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x06000572 RID: 1394 RVA: 0x0000D2C4 File Offset: 0x0000B4C4
		public int systemWidth
		{
			get
			{
				int result = 0;
				int num = 0;
				Display.GetSystemExtImpl(this.nativeDisplay, out result, out num);
				return result;
			}
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000573 RID: 1395 RVA: 0x0000D2F0 File Offset: 0x0000B4F0
		public int systemHeight
		{
			get
			{
				int num = 0;
				int result = 0;
				Display.GetSystemExtImpl(this.nativeDisplay, out num, out result);
				return result;
			}
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000574 RID: 1396 RVA: 0x0000D31C File Offset: 0x0000B51C
		public RenderBuffer colorBuffer
		{
			get
			{
				RenderBuffer result;
				RenderBuffer renderBuffer;
				Display.GetRenderingBuffersImpl(this.nativeDisplay, out result, out renderBuffer);
				return result;
			}
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000575 RID: 1397 RVA: 0x0000D344 File Offset: 0x0000B544
		public RenderBuffer depthBuffer
		{
			get
			{
				RenderBuffer renderBuffer;
				RenderBuffer result;
				Display.GetRenderingBuffersImpl(this.nativeDisplay, out renderBuffer, out result);
				return result;
			}
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000576 RID: 1398 RVA: 0x0000D36C File Offset: 0x0000B56C
		public bool active
		{
			get
			{
				return Display.GetActiveImp(this.nativeDisplay);
			}
		}

		// Token: 0x06000577 RID: 1399 RVA: 0x0000D38C File Offset: 0x0000B58C
		public void Activate()
		{
			Display.ActivateDisplayImpl(this.nativeDisplay, 0, 0, 60);
		}

		// Token: 0x06000578 RID: 1400 RVA: 0x0000D39E File Offset: 0x0000B59E
		public void Activate(int width, int height, int refreshRate)
		{
			Display.ActivateDisplayImpl(this.nativeDisplay, width, height, refreshRate);
		}

		// Token: 0x06000579 RID: 1401 RVA: 0x0000D3AF File Offset: 0x0000B5AF
		public void SetParams(int width, int height, int x, int y)
		{
			Display.SetParamsImpl(this.nativeDisplay, width, height, x, y);
		}

		// Token: 0x0600057A RID: 1402 RVA: 0x0000D3C2 File Offset: 0x0000B5C2
		public void SetRenderingResolution(int w, int h)
		{
			Display.SetRenderingResolutionImpl(this.nativeDisplay, w, h);
		}

		// Token: 0x0600057B RID: 1403 RVA: 0x0000D3D4 File Offset: 0x0000B5D4
		[Obsolete("MultiDisplayLicense has been deprecated.", false)]
		public static bool MultiDisplayLicense()
		{
			return true;
		}

		// Token: 0x0600057C RID: 1404 RVA: 0x0000D3EC File Offset: 0x0000B5EC
		public static Vector3 RelativeMouseAt(Vector3 inputMouseCoordinates)
		{
			int num = 0;
			int num2 = 0;
			int x = (int)inputMouseCoordinates.x;
			int y = (int)inputMouseCoordinates.y;
			Vector3 result;
			result.z = (float)Display.RelativeMouseAtImpl(x, y, out num, out num2);
			result.x = (float)num;
			result.y = (float)num2;
			return result;
		}

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x0600057D RID: 1405 RVA: 0x0000D444 File Offset: 0x0000B644
		public static Display main
		{
			get
			{
				return Display._mainDisplay;
			}
		}

		// Token: 0x0600057E RID: 1406 RVA: 0x0000D460 File Offset: 0x0000B660
		[RequiredByNativeCode]
		private static void RecreateDisplayList(IntPtr[] nativeDisplay)
		{
			if (nativeDisplay.Length != 0)
			{
				Display.displays = new Display[nativeDisplay.Length];
				for (int i = 0; i < nativeDisplay.Length; i++)
				{
					Display.displays[i] = new Display(nativeDisplay[i]);
				}
				Display._mainDisplay = Display.displays[0];
			}
		}

		// Token: 0x0600057F RID: 1407 RVA: 0x0000D4B7 File Offset: 0x0000B6B7
		[RequiredByNativeCode]
		private static void FireDisplaysUpdated()
		{
			if (Display.onDisplaysUpdated != null)
			{
				Display.onDisplaysUpdated();
			}
		}

		// Token: 0x14000009 RID: 9
		// (add) Token: 0x06000580 RID: 1408 RVA: 0x0000D4D0 File Offset: 0x0000B6D0
		// (remove) Token: 0x06000581 RID: 1409 RVA: 0x0000D504 File Offset: 0x0000B704
		public static event Display.DisplaysUpdatedDelegate onDisplaysUpdated
		{
			add
			{
				Display.DisplaysUpdatedDelegate displaysUpdatedDelegate = Display.onDisplaysUpdated;
				Display.DisplaysUpdatedDelegate displaysUpdatedDelegate2;
				do
				{
					displaysUpdatedDelegate2 = displaysUpdatedDelegate;
					displaysUpdatedDelegate = Interlocked.CompareExchange<Display.DisplaysUpdatedDelegate>(ref Display.onDisplaysUpdated, (Display.DisplaysUpdatedDelegate)Delegate.Combine(displaysUpdatedDelegate2, value), displaysUpdatedDelegate);
				}
				while (displaysUpdatedDelegate != displaysUpdatedDelegate2);
			}
			remove
			{
				Display.DisplaysUpdatedDelegate displaysUpdatedDelegate = Display.onDisplaysUpdated;
				Display.DisplaysUpdatedDelegate displaysUpdatedDelegate2;
				do
				{
					displaysUpdatedDelegate2 = displaysUpdatedDelegate;
					displaysUpdatedDelegate = Interlocked.CompareExchange<Display.DisplaysUpdatedDelegate>(ref Display.onDisplaysUpdated, (Display.DisplaysUpdatedDelegate)Delegate.Remove(displaysUpdatedDelegate2, value), displaysUpdatedDelegate);
				}
				while (displaysUpdatedDelegate != displaysUpdatedDelegate2);
			}
		}

		// Token: 0x06000582 RID: 1410
		[FreeFunction("UnityDisplayManager_DisplaySystemResolution")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetSystemExtImpl(IntPtr nativeDisplay, out int w, out int h);

		// Token: 0x06000583 RID: 1411
		[FreeFunction("UnityDisplayManager_DisplayRenderingResolution")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRenderingExtImpl(IntPtr nativeDisplay, out int w, out int h);

		// Token: 0x06000584 RID: 1412
		[FreeFunction("UnityDisplayManager_GetRenderingBuffersWrapper")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRenderingBuffersImpl(IntPtr nativeDisplay, out RenderBuffer color, out RenderBuffer depth);

		// Token: 0x06000585 RID: 1413
		[FreeFunction("UnityDisplayManager_SetRenderingResolution")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetRenderingResolutionImpl(IntPtr nativeDisplay, int w, int h);

		// Token: 0x06000586 RID: 1414
		[FreeFunction("UnityDisplayManager_ActivateDisplay")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ActivateDisplayImpl(IntPtr nativeDisplay, int width, int height, int refreshRate);

		// Token: 0x06000587 RID: 1415
		[FreeFunction("UnityDisplayManager_SetDisplayParam")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetParamsImpl(IntPtr nativeDisplay, int width, int height, int x, int y);

		// Token: 0x06000588 RID: 1416
		[FreeFunction("UnityDisplayManager_RelativeMouseAt")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int RelativeMouseAtImpl(int x, int y, out int rx, out int ry);

		// Token: 0x06000589 RID: 1417
		[FreeFunction("UnityDisplayManager_DisplayActive")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetActiveImp(IntPtr nativeDisplay);

		// Token: 0x0600058A RID: 1418 RVA: 0x0000D538 File Offset: 0x0000B738
		// Note: this type is marked as 'beforefieldinit'.
		static Display()
		{
		}

		// Token: 0x0400013F RID: 319
		internal IntPtr nativeDisplay;

		// Token: 0x04000140 RID: 320
		public static Display[] displays = new Display[]
		{
			new Display()
		};

		// Token: 0x04000141 RID: 321
		private static Display _mainDisplay = Display.displays[0];

		// Token: 0x04000142 RID: 322
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Display.DisplaysUpdatedDelegate onDisplaysUpdated = null;

		// Token: 0x0200006B RID: 107
		// (Invoke) Token: 0x0600058C RID: 1420
		public delegate void DisplaysUpdatedDelegate();
	}
}
