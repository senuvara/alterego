﻿using System;
using System.IO;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000149 RID: 329
	internal static class ManagedStreamHelpers
	{
		// Token: 0x06000D16 RID: 3350 RVA: 0x00014538 File Offset: 0x00012738
		internal static void ValidateLoadFromStream(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("ManagedStream object must be non-null", "stream");
			}
			if (!stream.CanRead)
			{
				throw new ArgumentException("ManagedStream object must be readable (stream.CanRead must return true)", "stream");
			}
			if (!stream.CanSeek)
			{
				throw new ArgumentException("ManagedStream object must be seekable (stream.CanSeek must return true)", "stream");
			}
		}

		// Token: 0x06000D17 RID: 3351 RVA: 0x00014592 File Offset: 0x00012792
		[RequiredByNativeCode]
		internal unsafe static void ManagedStreamRead(byte[] buffer, int offset, int count, Stream stream, IntPtr returnValueAddress)
		{
			if (returnValueAddress == IntPtr.Zero)
			{
				throw new ArgumentException("Return value address cannot be 0.", "returnValueAddress");
			}
			ManagedStreamHelpers.ValidateLoadFromStream(stream);
			*(int*)((void*)returnValueAddress) = stream.Read(buffer, offset, count);
		}

		// Token: 0x06000D18 RID: 3352 RVA: 0x000145CD File Offset: 0x000127CD
		[RequiredByNativeCode]
		internal unsafe static void ManagedStreamSeek(long offset, uint origin, Stream stream, IntPtr returnValueAddress)
		{
			if (returnValueAddress == IntPtr.Zero)
			{
				throw new ArgumentException("Return value address cannot be 0.", "returnValueAddress");
			}
			ManagedStreamHelpers.ValidateLoadFromStream(stream);
			*(long*)((void*)returnValueAddress) = stream.Seek(offset, (SeekOrigin)origin);
		}

		// Token: 0x06000D19 RID: 3353 RVA: 0x00014605 File Offset: 0x00012805
		[RequiredByNativeCode]
		internal unsafe static void ManagedStreamLength(Stream stream, IntPtr returnValueAddress)
		{
			if (returnValueAddress == IntPtr.Zero)
			{
				throw new ArgumentException("Return value address cannot be 0.", "returnValueAddress");
			}
			ManagedStreamHelpers.ValidateLoadFromStream(stream);
			*(long*)((void*)returnValueAddress) = stream.Length;
		}
	}
}
