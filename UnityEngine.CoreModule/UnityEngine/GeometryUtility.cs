﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200007A RID: 122
	[StaticAccessor("GeometryUtilityScripting", StaticAccessorType.DoubleColon)]
	[NativeHeader("Runtime/Graphics/GraphicsScriptBindings.h")]
	public sealed class GeometryUtility
	{
		// Token: 0x0600061C RID: 1564 RVA: 0x00002370 File Offset: 0x00000570
		public GeometryUtility()
		{
		}

		// Token: 0x0600061D RID: 1565 RVA: 0x0000DF24 File Offset: 0x0000C124
		public static Plane[] CalculateFrustumPlanes(Camera camera)
		{
			Plane[] array = new Plane[6];
			GeometryUtility.CalculateFrustumPlanes(camera, array);
			return array;
		}

		// Token: 0x0600061E RID: 1566 RVA: 0x0000DF48 File Offset: 0x0000C148
		public static Plane[] CalculateFrustumPlanes(Matrix4x4 worldToProjectionMatrix)
		{
			Plane[] array = new Plane[6];
			GeometryUtility.CalculateFrustumPlanes(worldToProjectionMatrix, array);
			return array;
		}

		// Token: 0x0600061F RID: 1567 RVA: 0x0000DF6C File Offset: 0x0000C16C
		public static void CalculateFrustumPlanes(Camera camera, Plane[] planes)
		{
			GeometryUtility.CalculateFrustumPlanes(camera.projectionMatrix * camera.worldToCameraMatrix, planes);
		}

		// Token: 0x06000620 RID: 1568 RVA: 0x0000DF86 File Offset: 0x0000C186
		public static void CalculateFrustumPlanes(Matrix4x4 worldToProjectionMatrix, Plane[] planes)
		{
			if (planes == null)
			{
				throw new ArgumentNullException("planes");
			}
			if (planes.Length != 6)
			{
				throw new ArgumentException("Planes array must be of length 6.", "planes");
			}
			GeometryUtility.Internal_ExtractPlanes(planes, worldToProjectionMatrix);
		}

		// Token: 0x06000621 RID: 1569 RVA: 0x0000DFBC File Offset: 0x0000C1BC
		public static Bounds CalculateBounds(Vector3[] positions, Matrix4x4 transform)
		{
			if (positions == null)
			{
				throw new ArgumentNullException("positions");
			}
			if (positions.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.", "positions");
			}
			return GeometryUtility.Internal_CalculateBounds(positions, transform);
		}

		// Token: 0x06000622 RID: 1570 RVA: 0x0000E004 File Offset: 0x0000C204
		public static bool TryCreatePlaneFromPolygon(Vector3[] vertices, out Plane plane)
		{
			bool result;
			if (vertices == null || vertices.Length < 3)
			{
				plane = new Plane(Vector3.up, 0f);
				result = false;
			}
			else if (vertices.Length == 3)
			{
				Vector3 a = vertices[0];
				Vector3 b = vertices[1];
				Vector3 c = vertices[2];
				plane = new Plane(a, b, c);
				result = (plane.normal.sqrMagnitude > 0f);
			}
			else
			{
				Vector3 zero = Vector3.zero;
				int num = vertices.Length - 1;
				Vector3 vector = vertices[num];
				foreach (Vector3 vector2 in vertices)
				{
					zero.x += (vector.y - vector2.y) * (vector.z + vector2.z);
					zero.y += (vector.z - vector2.z) * (vector.x + vector2.x);
					zero.z += (vector.x - vector2.x) * (vector.y + vector2.y);
					vector = vector2;
				}
				zero.Normalize();
				float num2 = 0f;
				foreach (Vector3 rhs in vertices)
				{
					num2 -= Vector3.Dot(zero, rhs);
				}
				num2 /= (float)vertices.Length;
				plane = new Plane(zero, num2);
				result = (plane.normal.sqrMagnitude > 0f);
			}
			return result;
		}

		// Token: 0x06000623 RID: 1571 RVA: 0x0000E1D8 File Offset: 0x0000C3D8
		public static bool TestPlanesAABB(Plane[] planes, Bounds bounds)
		{
			return GeometryUtility.TestPlanesAABB_Injected(planes, ref bounds);
		}

		// Token: 0x06000624 RID: 1572 RVA: 0x0000E1E2 File Offset: 0x0000C3E2
		[NativeName("ExtractPlanes")]
		private static void Internal_ExtractPlanes([Out] Plane[] planes, Matrix4x4 worldToProjectionMatrix)
		{
			GeometryUtility.Internal_ExtractPlanes_Injected(planes, ref worldToProjectionMatrix);
		}

		// Token: 0x06000625 RID: 1573 RVA: 0x0000E1EC File Offset: 0x0000C3EC
		[NativeName("CalculateBounds")]
		private static Bounds Internal_CalculateBounds(Vector3[] positions, Matrix4x4 transform)
		{
			Bounds result;
			GeometryUtility.Internal_CalculateBounds_Injected(positions, ref transform, out result);
			return result;
		}

		// Token: 0x06000626 RID: 1574
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool TestPlanesAABB_Injected(Plane[] planes, ref Bounds bounds);

		// Token: 0x06000627 RID: 1575
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_ExtractPlanes_Injected([Out] Plane[] planes, ref Matrix4x4 worldToProjectionMatrix);

		// Token: 0x06000628 RID: 1576
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CalculateBounds_Injected(Vector3[] positions, ref Matrix4x4 transform, out Bounds ret);
	}
}
