﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000077 RID: 119
	[Obsolete("This component is part of the legacy UI system and will be removed in a future release.")]
	[NativeHeader("Runtime/Camera/RenderLayers/GUITexture.h")]
	public class GUITexture : GUIElement
	{
		// Token: 0x060005C5 RID: 1477 RVA: 0x0000DA05 File Offset: 0x0000BC05
		public GUITexture()
		{
		}

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x060005C6 RID: 1478 RVA: 0x0000DA10 File Offset: 0x0000BC10
		// (set) Token: 0x060005C7 RID: 1479 RVA: 0x0000DA26 File Offset: 0x0000BC26
		public Color color
		{
			get
			{
				Color result;
				this.get_color_Injected(out result);
				return result;
			}
			set
			{
				this.set_color_Injected(ref value);
			}
		}

		// Token: 0x17000105 RID: 261
		// (get) Token: 0x060005C8 RID: 1480
		// (set) Token: 0x060005C9 RID: 1481
		public extern Texture texture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x060005CA RID: 1482 RVA: 0x0000DA30 File Offset: 0x0000BC30
		// (set) Token: 0x060005CB RID: 1483 RVA: 0x0000DA46 File Offset: 0x0000BC46
		public Rect pixelInset
		{
			get
			{
				Rect result;
				this.get_pixelInset_Injected(out result);
				return result;
			}
			set
			{
				this.set_pixelInset_Injected(ref value);
			}
		}

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x060005CC RID: 1484
		// (set) Token: 0x060005CD RID: 1485
		public extern RectOffset border { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060005CE RID: 1486
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_color_Injected(out Color ret);

		// Token: 0x060005CF RID: 1487
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_color_Injected(ref Color value);

		// Token: 0x060005D0 RID: 1488
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_pixelInset_Injected(out Rect ret);

		// Token: 0x060005D1 RID: 1489
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_pixelInset_Injected(ref Rect value);
	}
}
