﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020000A9 RID: 169
	[NativeHeader("Runtime/Camera/SharedLightData.h")]
	public struct LightBakingOutput
	{
		// Token: 0x040001A4 RID: 420
		public int probeOcclusionLightIndex;

		// Token: 0x040001A5 RID: 421
		public int occlusionMaskChannel;

		// Token: 0x040001A6 RID: 422
		[NativeName("lightmapBakeMode.lightmapBakeType")]
		public LightmapBakeType lightmapBakeType;

		// Token: 0x040001A7 RID: 423
		[NativeName("lightmapBakeMode.mixedLightingMode")]
		public MixedLightingMode mixedLightingMode;

		// Token: 0x040001A8 RID: 424
		public bool isBaked;
	}
}
