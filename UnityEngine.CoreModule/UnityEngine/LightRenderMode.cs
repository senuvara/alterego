﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B5 RID: 181
	public enum LightRenderMode
	{
		// Token: 0x040001D4 RID: 468
		Auto,
		// Token: 0x040001D5 RID: 469
		ForcePixel,
		// Token: 0x040001D6 RID: 470
		ForceVertex
	}
}
