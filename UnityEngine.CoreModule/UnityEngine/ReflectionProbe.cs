﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001FC RID: 508
	[NativeHeader("Runtime/Camera/ReflectionProbes.h")]
	public sealed class ReflectionProbe : Behaviour
	{
		// Token: 0x06001027 RID: 4135 RVA: 0x0000A52A File Offset: 0x0000872A
		public ReflectionProbe()
		{
		}

		// Token: 0x1700031C RID: 796
		// (get) Token: 0x06001028 RID: 4136
		// (set) Token: 0x06001029 RID: 4137
		[NativeName("ProbeType")]
		[Obsolete("type property has been deprecated. Starting with Unity 5.4, the only supported reflection probe type is Cube.", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public extern ReflectionProbeType type { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700031D RID: 797
		// (get) Token: 0x0600102A RID: 4138 RVA: 0x0001C6A8 File Offset: 0x0001A8A8
		// (set) Token: 0x0600102B RID: 4139 RVA: 0x0001C6BE File Offset: 0x0001A8BE
		[NativeName("BoxSize")]
		public Vector3 size
		{
			get
			{
				Vector3 result;
				this.get_size_Injected(out result);
				return result;
			}
			set
			{
				this.set_size_Injected(ref value);
			}
		}

		// Token: 0x1700031E RID: 798
		// (get) Token: 0x0600102C RID: 4140 RVA: 0x0001C6C8 File Offset: 0x0001A8C8
		// (set) Token: 0x0600102D RID: 4141 RVA: 0x0001C6DE File Offset: 0x0001A8DE
		[NativeName("BoxOffset")]
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.get_center_Injected(out result);
				return result;
			}
			set
			{
				this.set_center_Injected(ref value);
			}
		}

		// Token: 0x1700031F RID: 799
		// (get) Token: 0x0600102E RID: 4142
		// (set) Token: 0x0600102F RID: 4143
		[NativeName("Near")]
		public extern float nearClipPlane { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000320 RID: 800
		// (get) Token: 0x06001030 RID: 4144
		// (set) Token: 0x06001031 RID: 4145
		[NativeName("Far")]
		public extern float farClipPlane { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000321 RID: 801
		// (get) Token: 0x06001032 RID: 4146
		// (set) Token: 0x06001033 RID: 4147
		[NativeName("IntensityMultiplier")]
		public extern float intensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000322 RID: 802
		// (get) Token: 0x06001034 RID: 4148 RVA: 0x0001C6E8 File Offset: 0x0001A8E8
		[NativeName("GlobalAABB")]
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.get_bounds_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000323 RID: 803
		// (get) Token: 0x06001035 RID: 4149
		// (set) Token: 0x06001036 RID: 4150
		[NativeName("HDR")]
		public extern bool hdr { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000324 RID: 804
		// (get) Token: 0x06001037 RID: 4151
		// (set) Token: 0x06001038 RID: 4152
		public extern float shadowDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000325 RID: 805
		// (get) Token: 0x06001039 RID: 4153
		// (set) Token: 0x0600103A RID: 4154
		public extern int resolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000326 RID: 806
		// (get) Token: 0x0600103B RID: 4155
		// (set) Token: 0x0600103C RID: 4156
		public extern int cullingMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000327 RID: 807
		// (get) Token: 0x0600103D RID: 4157
		// (set) Token: 0x0600103E RID: 4158
		public extern ReflectionProbeClearFlags clearFlags { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000328 RID: 808
		// (get) Token: 0x0600103F RID: 4159 RVA: 0x0001C700 File Offset: 0x0001A900
		// (set) Token: 0x06001040 RID: 4160 RVA: 0x0001C716 File Offset: 0x0001A916
		public Color backgroundColor
		{
			get
			{
				Color result;
				this.get_backgroundColor_Injected(out result);
				return result;
			}
			set
			{
				this.set_backgroundColor_Injected(ref value);
			}
		}

		// Token: 0x17000329 RID: 809
		// (get) Token: 0x06001041 RID: 4161
		// (set) Token: 0x06001042 RID: 4162
		public extern float blendDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700032A RID: 810
		// (get) Token: 0x06001043 RID: 4163
		// (set) Token: 0x06001044 RID: 4164
		public extern bool boxProjection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700032B RID: 811
		// (get) Token: 0x06001045 RID: 4165
		// (set) Token: 0x06001046 RID: 4166
		public extern ReflectionProbeMode mode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700032C RID: 812
		// (get) Token: 0x06001047 RID: 4167
		// (set) Token: 0x06001048 RID: 4168
		public extern int importance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700032D RID: 813
		// (get) Token: 0x06001049 RID: 4169
		// (set) Token: 0x0600104A RID: 4170
		public extern ReflectionProbeRefreshMode refreshMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700032E RID: 814
		// (get) Token: 0x0600104B RID: 4171
		// (set) Token: 0x0600104C RID: 4172
		public extern ReflectionProbeTimeSlicingMode timeSlicingMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700032F RID: 815
		// (get) Token: 0x0600104D RID: 4173
		// (set) Token: 0x0600104E RID: 4174
		public extern Texture bakedTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000330 RID: 816
		// (get) Token: 0x0600104F RID: 4175
		// (set) Token: 0x06001050 RID: 4176
		public extern Texture customBakedTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000331 RID: 817
		// (get) Token: 0x06001051 RID: 4177
		// (set) Token: 0x06001052 RID: 4178
		public extern RenderTexture realtimeTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000332 RID: 818
		// (get) Token: 0x06001053 RID: 4179
		public extern Texture texture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000333 RID: 819
		// (get) Token: 0x06001054 RID: 4180 RVA: 0x0001C720 File Offset: 0x0001A920
		public Vector4 textureHDRDecodeValues
		{
			[NativeName("CalculateHDRDecodeValues")]
			get
			{
				Vector4 result;
				this.get_textureHDRDecodeValues_Injected(out result);
				return result;
			}
		}

		// Token: 0x06001055 RID: 4181
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Reset();

		// Token: 0x06001056 RID: 4182 RVA: 0x0001C738 File Offset: 0x0001A938
		public int RenderProbe()
		{
			return this.RenderProbe(null);
		}

		// Token: 0x06001057 RID: 4183 RVA: 0x0001C754 File Offset: 0x0001A954
		public int RenderProbe([UnityEngine.Internal.DefaultValue("null")] RenderTexture targetTexture)
		{
			return this.ScheduleRender(this.timeSlicingMode, targetTexture);
		}

		// Token: 0x06001058 RID: 4184
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsFinishedRendering(int renderId);

		// Token: 0x06001059 RID: 4185
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int ScheduleRender(ReflectionProbeTimeSlicingMode timeSlicingMode, RenderTexture targetTexture);

		// Token: 0x0600105A RID: 4186
		[NativeHeader("Runtime/Camera/CubemapGPUUtility.h")]
		[FreeFunction("CubemapGPUBlend")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool BlendCubemap(Texture src, Texture dst, float blend, RenderTexture target);

		// Token: 0x17000334 RID: 820
		// (get) Token: 0x0600105B RID: 4187
		[StaticAccessor("GetReflectionProbes()")]
		public static extern int minBakedCubemapResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000335 RID: 821
		// (get) Token: 0x0600105C RID: 4188
		[StaticAccessor("GetReflectionProbes()")]
		public static extern int maxBakedCubemapResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000336 RID: 822
		// (get) Token: 0x0600105D RID: 4189 RVA: 0x0001C778 File Offset: 0x0001A978
		[StaticAccessor("GetReflectionProbes()")]
		public static Vector4 defaultTextureHDRDecodeValues
		{
			get
			{
				Vector4 result;
				ReflectionProbe.get_defaultTextureHDRDecodeValues_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000337 RID: 823
		// (get) Token: 0x0600105E RID: 4190
		[StaticAccessor("GetReflectionProbes()")]
		public static extern Texture defaultTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1400000A RID: 10
		// (add) Token: 0x0600105F RID: 4191 RVA: 0x0001C790 File Offset: 0x0001A990
		// (remove) Token: 0x06001060 RID: 4192 RVA: 0x0001C7C4 File Offset: 0x0001A9C4
		public static event Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent> reflectionProbeChanged
		{
			add
			{
				Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent> action = ReflectionProbe.reflectionProbeChanged;
				Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent>>(ref ReflectionProbe.reflectionProbeChanged, (Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent> action = ReflectionProbe.reflectionProbeChanged;
				Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent>>(ref ReflectionProbe.reflectionProbeChanged, (Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x1400000B RID: 11
		// (add) Token: 0x06001061 RID: 4193 RVA: 0x0001C7F8 File Offset: 0x0001A9F8
		// (remove) Token: 0x06001062 RID: 4194 RVA: 0x0001C82C File Offset: 0x0001AA2C
		public static event Action<Cubemap> defaultReflectionSet
		{
			add
			{
				Action<Cubemap> action = ReflectionProbe.defaultReflectionSet;
				Action<Cubemap> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<Cubemap>>(ref ReflectionProbe.defaultReflectionSet, (Action<Cubemap>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<Cubemap> action = ReflectionProbe.defaultReflectionSet;
				Action<Cubemap> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<Cubemap>>(ref ReflectionProbe.defaultReflectionSet, (Action<Cubemap>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x06001063 RID: 4195 RVA: 0x0001C860 File Offset: 0x0001AA60
		[RequiredByNativeCode]
		private static void CallReflectionProbeEvent(ReflectionProbe probe, ReflectionProbe.ReflectionProbeEvent probeEvent)
		{
			Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent> action = ReflectionProbe.reflectionProbeChanged;
			if (action != null)
			{
				action(probe, probeEvent);
			}
		}

		// Token: 0x06001064 RID: 4196 RVA: 0x0001C884 File Offset: 0x0001AA84
		[RequiredByNativeCode]
		private static void CallSetDefaultReflection(Cubemap defaultReflectionCubemap)
		{
			Action<Cubemap> action = ReflectionProbe.defaultReflectionSet;
			if (action != null)
			{
				action(defaultReflectionCubemap);
			}
		}

		// Token: 0x06001065 RID: 4197
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_size_Injected(out Vector3 ret);

		// Token: 0x06001066 RID: 4198
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_size_Injected(ref Vector3 value);

		// Token: 0x06001067 RID: 4199
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_center_Injected(out Vector3 ret);

		// Token: 0x06001068 RID: 4200
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_center_Injected(ref Vector3 value);

		// Token: 0x06001069 RID: 4201
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bounds_Injected(out Bounds ret);

		// Token: 0x0600106A RID: 4202
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_backgroundColor_Injected(out Color ret);

		// Token: 0x0600106B RID: 4203
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_backgroundColor_Injected(ref Color value);

		// Token: 0x0600106C RID: 4204
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_textureHDRDecodeValues_Injected(out Vector4 ret);

		// Token: 0x0600106D RID: 4205
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_defaultTextureHDRDecodeValues_Injected(out Vector4 ret);

		// Token: 0x04000745 RID: 1861
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action<ReflectionProbe, ReflectionProbe.ReflectionProbeEvent> reflectionProbeChanged;

		// Token: 0x04000746 RID: 1862
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action<Cubemap> defaultReflectionSet;

		// Token: 0x020001FD RID: 509
		public enum ReflectionProbeEvent
		{
			// Token: 0x04000748 RID: 1864
			ReflectionProbeAdded,
			// Token: 0x04000749 RID: 1865
			ReflectionProbeRemoved
		}
	}
}
