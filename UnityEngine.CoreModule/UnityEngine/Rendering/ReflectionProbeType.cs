﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000100 RID: 256
	public enum ReflectionProbeType
	{
		// Token: 0x0400048E RID: 1166
		Cube,
		// Token: 0x0400048F RID: 1167
		Card
	}
}
