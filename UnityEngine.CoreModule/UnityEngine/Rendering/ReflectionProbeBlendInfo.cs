﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x02000103 RID: 259
	[UsedByNativeCode]
	public struct ReflectionProbeBlendInfo
	{
		// Token: 0x04000497 RID: 1175
		public ReflectionProbe probe;

		// Token: 0x04000498 RID: 1176
		public float weight;
	}
}
