﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200010F RID: 271
	public enum ComputeQueueType
	{
		// Token: 0x040004ED RID: 1261
		Default,
		// Token: 0x040004EE RID: 1262
		Background,
		// Token: 0x040004EF RID: 1263
		Urgent
	}
}
