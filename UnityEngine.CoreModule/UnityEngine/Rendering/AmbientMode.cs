﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F0 RID: 240
	public enum AmbientMode
	{
		// Token: 0x040003F4 RID: 1012
		Skybox,
		// Token: 0x040003F5 RID: 1013
		Trilight,
		// Token: 0x040003F6 RID: 1014
		Flat = 3,
		// Token: 0x040003F7 RID: 1015
		Custom
	}
}
