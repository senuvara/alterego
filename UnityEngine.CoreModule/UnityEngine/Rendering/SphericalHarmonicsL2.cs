﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x02000216 RID: 534
	[UsedByNativeCode]
	[NativeHeader("Runtime/Export/SphericalHarmonicsL2.bindings.h")]
	public struct SphericalHarmonicsL2 : IEquatable<SphericalHarmonicsL2>
	{
		// Token: 0x060011F9 RID: 4601 RVA: 0x0001E4B2 File Offset: 0x0001C6B2
		public void Clear()
		{
			this.SetZero();
		}

		// Token: 0x060011FA RID: 4602 RVA: 0x0001E4BB File Offset: 0x0001C6BB
		private void SetZero()
		{
			SphericalHarmonicsL2.SetZero_Injected(ref this);
		}

		// Token: 0x060011FB RID: 4603 RVA: 0x0001E4C3 File Offset: 0x0001C6C3
		public void AddAmbientLight(Color color)
		{
			SphericalHarmonicsL2.AddAmbientLight_Injected(ref this, ref color);
		}

		// Token: 0x060011FC RID: 4604 RVA: 0x0001E4D0 File Offset: 0x0001C6D0
		public void AddDirectionalLight(Vector3 direction, Color color, float intensity)
		{
			Color color2 = color * (2f * intensity);
			SphericalHarmonicsL2.AddDirectionalLightInternal(ref this, direction, color2);
		}

		// Token: 0x060011FD RID: 4605 RVA: 0x0001E4F4 File Offset: 0x0001C6F4
		[FreeFunction]
		private static void AddDirectionalLightInternal(ref SphericalHarmonicsL2 sh, Vector3 direction, Color color)
		{
			SphericalHarmonicsL2.AddDirectionalLightInternal_Injected(ref sh, ref direction, ref color);
		}

		// Token: 0x060011FE RID: 4606 RVA: 0x0001E500 File Offset: 0x0001C700
		public void Evaluate(Vector3[] directions, Color[] results)
		{
			if (directions == null)
			{
				throw new ArgumentNullException("directions");
			}
			if (results == null)
			{
				throw new ArgumentNullException("results");
			}
			if (directions.Length != 0)
			{
				if (directions.Length != results.Length)
				{
					throw new ArgumentException("Length of the directions array and the results array must match.");
				}
				SphericalHarmonicsL2.EvaluateInternal(ref this, directions, results);
			}
		}

		// Token: 0x060011FF RID: 4607
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void EvaluateInternal(ref SphericalHarmonicsL2 sh, Vector3[] directions, [Out] Color[] results);

		// Token: 0x17000343 RID: 835
		public float this[int rgb, int coefficient]
		{
			get
			{
				float result;
				switch (rgb * 9 + coefficient)
				{
				case 0:
					result = this.shr0;
					break;
				case 1:
					result = this.shr1;
					break;
				case 2:
					result = this.shr2;
					break;
				case 3:
					result = this.shr3;
					break;
				case 4:
					result = this.shr4;
					break;
				case 5:
					result = this.shr5;
					break;
				case 6:
					result = this.shr6;
					break;
				case 7:
					result = this.shr7;
					break;
				case 8:
					result = this.shr8;
					break;
				case 9:
					result = this.shg0;
					break;
				case 10:
					result = this.shg1;
					break;
				case 11:
					result = this.shg2;
					break;
				case 12:
					result = this.shg3;
					break;
				case 13:
					result = this.shg4;
					break;
				case 14:
					result = this.shg5;
					break;
				case 15:
					result = this.shg6;
					break;
				case 16:
					result = this.shg7;
					break;
				case 17:
					result = this.shg8;
					break;
				case 18:
					result = this.shb0;
					break;
				case 19:
					result = this.shb1;
					break;
				case 20:
					result = this.shb2;
					break;
				case 21:
					result = this.shb3;
					break;
				case 22:
					result = this.shb4;
					break;
				case 23:
					result = this.shb5;
					break;
				case 24:
					result = this.shb6;
					break;
				case 25:
					result = this.shb7;
					break;
				case 26:
					result = this.shb8;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid index!");
				}
				return result;
			}
			set
			{
				switch (rgb * 9 + coefficient)
				{
				case 0:
					this.shr0 = value;
					break;
				case 1:
					this.shr1 = value;
					break;
				case 2:
					this.shr2 = value;
					break;
				case 3:
					this.shr3 = value;
					break;
				case 4:
					this.shr4 = value;
					break;
				case 5:
					this.shr5 = value;
					break;
				case 6:
					this.shr6 = value;
					break;
				case 7:
					this.shr7 = value;
					break;
				case 8:
					this.shr8 = value;
					break;
				case 9:
					this.shg0 = value;
					break;
				case 10:
					this.shg1 = value;
					break;
				case 11:
					this.shg2 = value;
					break;
				case 12:
					this.shg3 = value;
					break;
				case 13:
					this.shg4 = value;
					break;
				case 14:
					this.shg5 = value;
					break;
				case 15:
					this.shg6 = value;
					break;
				case 16:
					this.shg7 = value;
					break;
				case 17:
					this.shg8 = value;
					break;
				case 18:
					this.shb0 = value;
					break;
				case 19:
					this.shb1 = value;
					break;
				case 20:
					this.shb2 = value;
					break;
				case 21:
					this.shb3 = value;
					break;
				case 22:
					this.shb4 = value;
					break;
				case 23:
					this.shb5 = value;
					break;
				case 24:
					this.shb6 = value;
					break;
				case 25:
					this.shb7 = value;
					break;
				case 26:
					this.shb8 = value;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid index!");
				}
			}
		}

		// Token: 0x06001202 RID: 4610 RVA: 0x0001E914 File Offset: 0x0001CB14
		public override int GetHashCode()
		{
			int num = 17;
			num = num * 23 + this.shr0.GetHashCode();
			num = num * 23 + this.shr1.GetHashCode();
			num = num * 23 + this.shr2.GetHashCode();
			num = num * 23 + this.shr3.GetHashCode();
			num = num * 23 + this.shr4.GetHashCode();
			num = num * 23 + this.shr5.GetHashCode();
			num = num * 23 + this.shr6.GetHashCode();
			num = num * 23 + this.shr7.GetHashCode();
			num = num * 23 + this.shr8.GetHashCode();
			num = num * 23 + this.shg0.GetHashCode();
			num = num * 23 + this.shg1.GetHashCode();
			num = num * 23 + this.shg2.GetHashCode();
			num = num * 23 + this.shg3.GetHashCode();
			num = num * 23 + this.shg4.GetHashCode();
			num = num * 23 + this.shg5.GetHashCode();
			num = num * 23 + this.shg6.GetHashCode();
			num = num * 23 + this.shg7.GetHashCode();
			num = num * 23 + this.shg8.GetHashCode();
			num = num * 23 + this.shb0.GetHashCode();
			num = num * 23 + this.shb1.GetHashCode();
			num = num * 23 + this.shb2.GetHashCode();
			num = num * 23 + this.shb3.GetHashCode();
			num = num * 23 + this.shb4.GetHashCode();
			num = num * 23 + this.shb5.GetHashCode();
			num = num * 23 + this.shb6.GetHashCode();
			num = num * 23 + this.shb7.GetHashCode();
			return num * 23 + this.shb8.GetHashCode();
		}

		// Token: 0x06001203 RID: 4611 RVA: 0x0001EB9C File Offset: 0x0001CD9C
		public override bool Equals(object other)
		{
			return other is SphericalHarmonicsL2 && this.Equals((SphericalHarmonicsL2)other);
		}

		// Token: 0x06001204 RID: 4612 RVA: 0x0001EBCC File Offset: 0x0001CDCC
		public bool Equals(SphericalHarmonicsL2 other)
		{
			return this == other;
		}

		// Token: 0x06001205 RID: 4613 RVA: 0x0001EBF0 File Offset: 0x0001CDF0
		public static SphericalHarmonicsL2 operator *(SphericalHarmonicsL2 lhs, float rhs)
		{
			return new SphericalHarmonicsL2
			{
				shr0 = lhs.shr0 * rhs,
				shr1 = lhs.shr1 * rhs,
				shr2 = lhs.shr2 * rhs,
				shr3 = lhs.shr3 * rhs,
				shr4 = lhs.shr4 * rhs,
				shr5 = lhs.shr5 * rhs,
				shr6 = lhs.shr6 * rhs,
				shr7 = lhs.shr7 * rhs,
				shr8 = lhs.shr8 * rhs,
				shg0 = lhs.shg0 * rhs,
				shg1 = lhs.shg1 * rhs,
				shg2 = lhs.shg2 * rhs,
				shg3 = lhs.shg3 * rhs,
				shg4 = lhs.shg4 * rhs,
				shg5 = lhs.shg5 * rhs,
				shg6 = lhs.shg6 * rhs,
				shg7 = lhs.shg7 * rhs,
				shg8 = lhs.shg8 * rhs,
				shb0 = lhs.shb0 * rhs,
				shb1 = lhs.shb1 * rhs,
				shb2 = lhs.shb2 * rhs,
				shb3 = lhs.shb3 * rhs,
				shb4 = lhs.shb4 * rhs,
				shb5 = lhs.shb5 * rhs,
				shb6 = lhs.shb6 * rhs,
				shb7 = lhs.shb7 * rhs,
				shb8 = lhs.shb8 * rhs
			};
		}

		// Token: 0x06001206 RID: 4614 RVA: 0x0001EDC0 File Offset: 0x0001CFC0
		public static SphericalHarmonicsL2 operator *(float lhs, SphericalHarmonicsL2 rhs)
		{
			return new SphericalHarmonicsL2
			{
				shr0 = rhs.shr0 * lhs,
				shr1 = rhs.shr1 * lhs,
				shr2 = rhs.shr2 * lhs,
				shr3 = rhs.shr3 * lhs,
				shr4 = rhs.shr4 * lhs,
				shr5 = rhs.shr5 * lhs,
				shr6 = rhs.shr6 * lhs,
				shr7 = rhs.shr7 * lhs,
				shr8 = rhs.shr8 * lhs,
				shg0 = rhs.shg0 * lhs,
				shg1 = rhs.shg1 * lhs,
				shg2 = rhs.shg2 * lhs,
				shg3 = rhs.shg3 * lhs,
				shg4 = rhs.shg4 * lhs,
				shg5 = rhs.shg5 * lhs,
				shg6 = rhs.shg6 * lhs,
				shg7 = rhs.shg7 * lhs,
				shg8 = rhs.shg8 * lhs,
				shb0 = rhs.shb0 * lhs,
				shb1 = rhs.shb1 * lhs,
				shb2 = rhs.shb2 * lhs,
				shb3 = rhs.shb3 * lhs,
				shb4 = rhs.shb4 * lhs,
				shb5 = rhs.shb5 * lhs,
				shb6 = rhs.shb6 * lhs,
				shb7 = rhs.shb7 * lhs,
				shb8 = rhs.shb8 * lhs
			};
		}

		// Token: 0x06001207 RID: 4615 RVA: 0x0001EF90 File Offset: 0x0001D190
		public static SphericalHarmonicsL2 operator +(SphericalHarmonicsL2 lhs, SphericalHarmonicsL2 rhs)
		{
			return new SphericalHarmonicsL2
			{
				shr0 = lhs.shr0 + rhs.shr0,
				shr1 = lhs.shr1 + rhs.shr1,
				shr2 = lhs.shr2 + rhs.shr2,
				shr3 = lhs.shr3 + rhs.shr3,
				shr4 = lhs.shr4 + rhs.shr4,
				shr5 = lhs.shr5 + rhs.shr5,
				shr6 = lhs.shr6 + rhs.shr6,
				shr7 = lhs.shr7 + rhs.shr7,
				shr8 = lhs.shr8 + rhs.shr8,
				shg0 = lhs.shg0 + rhs.shg0,
				shg1 = lhs.shg1 + rhs.shg1,
				shg2 = lhs.shg2 + rhs.shg2,
				shg3 = lhs.shg3 + rhs.shg3,
				shg4 = lhs.shg4 + rhs.shg4,
				shg5 = lhs.shg5 + rhs.shg5,
				shg6 = lhs.shg6 + rhs.shg6,
				shg7 = lhs.shg7 + rhs.shg7,
				shg8 = lhs.shg8 + rhs.shg8,
				shb0 = lhs.shb0 + rhs.shb0,
				shb1 = lhs.shb1 + rhs.shb1,
				shb2 = lhs.shb2 + rhs.shb2,
				shb3 = lhs.shb3 + rhs.shb3,
				shb4 = lhs.shb4 + rhs.shb4,
				shb5 = lhs.shb5 + rhs.shb5,
				shb6 = lhs.shb6 + rhs.shb6,
				shb7 = lhs.shb7 + rhs.shb7,
				shb8 = lhs.shb8 + rhs.shb8
			};
		}

		// Token: 0x06001208 RID: 4616 RVA: 0x0001F200 File Offset: 0x0001D400
		public static bool operator ==(SphericalHarmonicsL2 lhs, SphericalHarmonicsL2 rhs)
		{
			return lhs.shr0 == rhs.shr0 && lhs.shr1 == rhs.shr1 && lhs.shr2 == rhs.shr2 && lhs.shr3 == rhs.shr3 && lhs.shr4 == rhs.shr4 && lhs.shr5 == rhs.shr5 && lhs.shr6 == rhs.shr6 && lhs.shr7 == rhs.shr7 && lhs.shr8 == rhs.shr8 && lhs.shg0 == rhs.shg0 && lhs.shg1 == rhs.shg1 && lhs.shg2 == rhs.shg2 && lhs.shg3 == rhs.shg3 && lhs.shg4 == rhs.shg4 && lhs.shg5 == rhs.shg5 && lhs.shg6 == rhs.shg6 && lhs.shg7 == rhs.shg7 && lhs.shg8 == rhs.shg8 && lhs.shb0 == rhs.shb0 && lhs.shb1 == rhs.shb1 && lhs.shb2 == rhs.shb2 && lhs.shb3 == rhs.shb3 && lhs.shb4 == rhs.shb4 && lhs.shb5 == rhs.shb5 && lhs.shb6 == rhs.shb6 && lhs.shb7 == rhs.shb7 && lhs.shb8 == rhs.shb8;
		}

		// Token: 0x06001209 RID: 4617 RVA: 0x0001F418 File Offset: 0x0001D618
		public static bool operator !=(SphericalHarmonicsL2 lhs, SphericalHarmonicsL2 rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x0600120A RID: 4618
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetZero_Injected(ref SphericalHarmonicsL2 _unity_self);

		// Token: 0x0600120B RID: 4619
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void AddAmbientLight_Injected(ref SphericalHarmonicsL2 _unity_self, ref Color color);

		// Token: 0x0600120C RID: 4620
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void AddDirectionalLightInternal_Injected(ref SphericalHarmonicsL2 sh, ref Vector3 direction, ref Color color);

		// Token: 0x04000768 RID: 1896
		private float shr0;

		// Token: 0x04000769 RID: 1897
		private float shr1;

		// Token: 0x0400076A RID: 1898
		private float shr2;

		// Token: 0x0400076B RID: 1899
		private float shr3;

		// Token: 0x0400076C RID: 1900
		private float shr4;

		// Token: 0x0400076D RID: 1901
		private float shr5;

		// Token: 0x0400076E RID: 1902
		private float shr6;

		// Token: 0x0400076F RID: 1903
		private float shr7;

		// Token: 0x04000770 RID: 1904
		private float shr8;

		// Token: 0x04000771 RID: 1905
		private float shg0;

		// Token: 0x04000772 RID: 1906
		private float shg1;

		// Token: 0x04000773 RID: 1907
		private float shg2;

		// Token: 0x04000774 RID: 1908
		private float shg3;

		// Token: 0x04000775 RID: 1909
		private float shg4;

		// Token: 0x04000776 RID: 1910
		private float shg5;

		// Token: 0x04000777 RID: 1911
		private float shg6;

		// Token: 0x04000778 RID: 1912
		private float shg7;

		// Token: 0x04000779 RID: 1913
		private float shg8;

		// Token: 0x0400077A RID: 1914
		private float shb0;

		// Token: 0x0400077B RID: 1915
		private float shb1;

		// Token: 0x0400077C RID: 1916
		private float shb2;

		// Token: 0x0400077D RID: 1917
		private float shb3;

		// Token: 0x0400077E RID: 1918
		private float shb4;

		// Token: 0x0400077F RID: 1919
		private float shb5;

		// Token: 0x04000780 RID: 1920
		private float shb6;

		// Token: 0x04000781 RID: 1921
		private float shb7;

		// Token: 0x04000782 RID: 1922
		private float shb8;
	}
}
