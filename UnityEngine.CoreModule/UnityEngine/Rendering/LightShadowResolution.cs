﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000FA RID: 250
	public enum LightShadowResolution
	{
		// Token: 0x0400045E RID: 1118
		FromQualitySettings = -1,
		// Token: 0x0400045F RID: 1119
		Low,
		// Token: 0x04000460 RID: 1120
		Medium,
		// Token: 0x04000461 RID: 1121
		High,
		// Token: 0x04000462 RID: 1122
		VeryHigh
	}
}
