﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000E9 RID: 233
	public enum RenderBufferStoreAction
	{
		// Token: 0x040003A1 RID: 929
		Store,
		// Token: 0x040003A2 RID: 930
		Resolve,
		// Token: 0x040003A3 RID: 931
		StoreAndResolve,
		// Token: 0x040003A4 RID: 932
		DontCare
	}
}
