﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Rendering
{
	// Token: 0x02000364 RID: 868
	[NativeType(Header = "Runtime/2D/Sorting/SortingGroup.h")]
	[RequireComponent(typeof(Transform))]
	public sealed class SortingGroup : Behaviour
	{
		// Token: 0x06001DA7 RID: 7591 RVA: 0x0000A52A File Offset: 0x0000872A
		public SortingGroup()
		{
		}

		// Token: 0x17000576 RID: 1398
		// (get) Token: 0x06001DA8 RID: 7592
		// (set) Token: 0x06001DA9 RID: 7593
		public extern string sortingLayerName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000577 RID: 1399
		// (get) Token: 0x06001DAA RID: 7594
		// (set) Token: 0x06001DAB RID: 7595
		public extern int sortingLayerID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000578 RID: 1400
		// (get) Token: 0x06001DAC RID: 7596
		// (set) Token: 0x06001DAD RID: 7597
		public extern int sortingOrder { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000579 RID: 1401
		// (get) Token: 0x06001DAE RID: 7598
		internal extern int sortingGroupID { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700057A RID: 1402
		// (get) Token: 0x06001DAF RID: 7599
		internal extern int sortingGroupOrder { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700057B RID: 1403
		// (get) Token: 0x06001DB0 RID: 7600
		internal extern int index { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
