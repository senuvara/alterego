﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200010B RID: 267
	public enum TextureDimension
	{
		// Token: 0x040004D5 RID: 1237
		Unknown = -1,
		// Token: 0x040004D6 RID: 1238
		None,
		// Token: 0x040004D7 RID: 1239
		Any,
		// Token: 0x040004D8 RID: 1240
		Tex2D,
		// Token: 0x040004D9 RID: 1241
		Tex3D,
		// Token: 0x040004DA RID: 1242
		Cube,
		// Token: 0x040004DB RID: 1243
		Tex2DArray,
		// Token: 0x040004DC RID: 1244
		CubeArray
	}
}
