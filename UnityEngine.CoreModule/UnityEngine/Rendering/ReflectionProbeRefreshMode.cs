﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000104 RID: 260
	public enum ReflectionProbeRefreshMode
	{
		// Token: 0x0400049A RID: 1178
		OnAwake,
		// Token: 0x0400049B RID: 1179
		EveryFrame,
		// Token: 0x0400049C RID: 1180
		ViaScripting
	}
}
