﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000ED RID: 237
	public enum CullMode
	{
		// Token: 0x040003E1 RID: 993
		Off,
		// Token: 0x040003E2 RID: 994
		Front,
		// Token: 0x040003E3 RID: 995
		Back
	}
}
