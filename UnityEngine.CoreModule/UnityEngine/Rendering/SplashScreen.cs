﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Rendering
{
	// Token: 0x02000217 RID: 535
	[NativeHeader("Runtime/Graphics/DrawSplashScreenAndWatermarks.h")]
	public class SplashScreen
	{
		// Token: 0x0600120D RID: 4621 RVA: 0x00002370 File Offset: 0x00000570
		public SplashScreen()
		{
		}

		// Token: 0x17000344 RID: 836
		// (get) Token: 0x0600120E RID: 4622
		public static extern bool isFinished { [FreeFunction("IsSplashScreenFinished")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600120F RID: 4623
		[FreeFunction("BeginSplashScreen_Binding")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Begin();

		// Token: 0x06001210 RID: 4624
		[FreeFunction("DrawSplashScreen_Binding")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Draw();
	}
}
