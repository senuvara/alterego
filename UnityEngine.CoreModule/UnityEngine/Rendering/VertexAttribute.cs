﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.Rendering
{
	// Token: 0x020000E5 RID: 229
	[MovedFrom("UnityEngine.Experimental.Rendering")]
	public enum VertexAttribute
	{
		// Token: 0x04000383 RID: 899
		Position,
		// Token: 0x04000384 RID: 900
		Normal,
		// Token: 0x04000385 RID: 901
		Tangent,
		// Token: 0x04000386 RID: 902
		Color,
		// Token: 0x04000387 RID: 903
		TexCoord0,
		// Token: 0x04000388 RID: 904
		TexCoord1,
		// Token: 0x04000389 RID: 905
		TexCoord2,
		// Token: 0x0400038A RID: 906
		TexCoord3,
		// Token: 0x0400038B RID: 907
		TexCoord4,
		// Token: 0x0400038C RID: 908
		TexCoord5,
		// Token: 0x0400038D RID: 909
		TexCoord6,
		// Token: 0x0400038E RID: 910
		TexCoord7,
		// Token: 0x0400038F RID: 911
		BlendWeight,
		// Token: 0x04000390 RID: 912
		BlendIndices
	}
}
