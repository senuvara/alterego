﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000107 RID: 263
	public enum LightProbeUsage
	{
		// Token: 0x040004A6 RID: 1190
		Off,
		// Token: 0x040004A7 RID: 1191
		BlendProbes,
		// Token: 0x040004A8 RID: 1192
		UseProxyVolume,
		// Token: 0x040004A9 RID: 1193
		CustomProvided = 4
	}
}
