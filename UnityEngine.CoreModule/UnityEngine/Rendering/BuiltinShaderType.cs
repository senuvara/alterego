﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000108 RID: 264
	public enum BuiltinShaderType
	{
		// Token: 0x040004AB RID: 1195
		DeferredShading,
		// Token: 0x040004AC RID: 1196
		DeferredReflections,
		// Token: 0x040004AD RID: 1197
		LegacyDeferredLighting,
		// Token: 0x040004AE RID: 1198
		ScreenSpaceShadows,
		// Token: 0x040004AF RID: 1199
		DepthNormals,
		// Token: 0x040004B0 RID: 1200
		MotionVectors,
		// Token: 0x040004B1 RID: 1201
		LightHalo,
		// Token: 0x040004B2 RID: 1202
		LensFlare
	}
}
