﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F5 RID: 245
	public enum LightEvent
	{
		// Token: 0x0400041C RID: 1052
		BeforeShadowMap,
		// Token: 0x0400041D RID: 1053
		AfterShadowMap,
		// Token: 0x0400041E RID: 1054
		BeforeScreenspaceMask,
		// Token: 0x0400041F RID: 1055
		AfterScreenspaceMask,
		// Token: 0x04000420 RID: 1056
		BeforeShadowMapPass,
		// Token: 0x04000421 RID: 1057
		AfterShadowMapPass
	}
}
