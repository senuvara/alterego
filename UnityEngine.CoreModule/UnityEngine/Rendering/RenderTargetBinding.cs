﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000FE RID: 254
	public struct RenderTargetBinding
	{
		// Token: 0x06000B52 RID: 2898 RVA: 0x00012A64 File Offset: 0x00010C64
		public RenderTargetBinding(RenderTargetIdentifier[] colorRenderTargets, RenderBufferLoadAction[] colorLoadActions, RenderBufferStoreAction[] colorStoreActions, RenderTargetIdentifier depthRenderTarget, RenderBufferLoadAction depthLoadAction, RenderBufferStoreAction depthStoreAction)
		{
			this.m_ColorRenderTargets = colorRenderTargets;
			this.m_DepthRenderTarget = depthRenderTarget;
			this.m_ColorLoadActions = colorLoadActions;
			this.m_ColorStoreActions = colorStoreActions;
			this.m_DepthLoadAction = depthLoadAction;
			this.m_DepthStoreAction = depthStoreAction;
		}

		// Token: 0x06000B53 RID: 2899 RVA: 0x00012A94 File Offset: 0x00010C94
		public RenderTargetBinding(RenderTargetIdentifier colorRenderTarget, RenderBufferLoadAction colorLoadAction, RenderBufferStoreAction colorStoreAction, RenderTargetIdentifier depthRenderTarget, RenderBufferLoadAction depthLoadAction, RenderBufferStoreAction depthStoreAction)
		{
			this = new RenderTargetBinding(new RenderTargetIdentifier[]
			{
				colorRenderTarget
			}, new RenderBufferLoadAction[]
			{
				colorLoadAction
			}, new RenderBufferStoreAction[]
			{
				colorStoreAction
			}, depthRenderTarget, depthLoadAction, depthStoreAction);
		}

		// Token: 0x06000B54 RID: 2900 RVA: 0x00012ACC File Offset: 0x00010CCC
		public RenderTargetBinding(RenderTargetSetup setup)
		{
			this.m_ColorRenderTargets = new RenderTargetIdentifier[setup.color.Length];
			for (int i = 0; i < this.m_ColorRenderTargets.Length; i++)
			{
				this.m_ColorRenderTargets[i] = new RenderTargetIdentifier(setup.color[i], setup.mipLevel, setup.cubemapFace, setup.depthSlice);
			}
			this.m_DepthRenderTarget = setup.depth;
			this.m_ColorLoadActions = (RenderBufferLoadAction[])setup.colorLoad.Clone();
			this.m_ColorStoreActions = (RenderBufferStoreAction[])setup.colorStore.Clone();
			this.m_DepthLoadAction = setup.depthLoad;
			this.m_DepthStoreAction = setup.depthStore;
		}

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06000B55 RID: 2901 RVA: 0x00012BA0 File Offset: 0x00010DA0
		// (set) Token: 0x06000B56 RID: 2902 RVA: 0x00012BBB File Offset: 0x00010DBB
		public RenderTargetIdentifier[] colorRenderTargets
		{
			get
			{
				return this.m_ColorRenderTargets;
			}
			set
			{
				this.m_ColorRenderTargets = value;
			}
		}

		// Token: 0x1700021C RID: 540
		// (get) Token: 0x06000B57 RID: 2903 RVA: 0x00012BC8 File Offset: 0x00010DC8
		// (set) Token: 0x06000B58 RID: 2904 RVA: 0x00012BE3 File Offset: 0x00010DE3
		public RenderTargetIdentifier depthRenderTarget
		{
			get
			{
				return this.m_DepthRenderTarget;
			}
			set
			{
				this.m_DepthRenderTarget = value;
			}
		}

		// Token: 0x1700021D RID: 541
		// (get) Token: 0x06000B59 RID: 2905 RVA: 0x00012BF0 File Offset: 0x00010DF0
		// (set) Token: 0x06000B5A RID: 2906 RVA: 0x00012C0B File Offset: 0x00010E0B
		public RenderBufferLoadAction[] colorLoadActions
		{
			get
			{
				return this.m_ColorLoadActions;
			}
			set
			{
				this.m_ColorLoadActions = value;
			}
		}

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x06000B5B RID: 2907 RVA: 0x00012C18 File Offset: 0x00010E18
		// (set) Token: 0x06000B5C RID: 2908 RVA: 0x00012C33 File Offset: 0x00010E33
		public RenderBufferStoreAction[] colorStoreActions
		{
			get
			{
				return this.m_ColorStoreActions;
			}
			set
			{
				this.m_ColorStoreActions = value;
			}
		}

		// Token: 0x1700021F RID: 543
		// (get) Token: 0x06000B5D RID: 2909 RVA: 0x00012C40 File Offset: 0x00010E40
		// (set) Token: 0x06000B5E RID: 2910 RVA: 0x00012C5B File Offset: 0x00010E5B
		public RenderBufferLoadAction depthLoadAction
		{
			get
			{
				return this.m_DepthLoadAction;
			}
			set
			{
				this.m_DepthLoadAction = value;
			}
		}

		// Token: 0x17000220 RID: 544
		// (get) Token: 0x06000B5F RID: 2911 RVA: 0x00012C68 File Offset: 0x00010E68
		// (set) Token: 0x06000B60 RID: 2912 RVA: 0x00012C83 File Offset: 0x00010E83
		public RenderBufferStoreAction depthStoreAction
		{
			get
			{
				return this.m_DepthStoreAction;
			}
			set
			{
				this.m_DepthStoreAction = value;
			}
		}

		// Token: 0x04000482 RID: 1154
		private RenderTargetIdentifier[] m_ColorRenderTargets;

		// Token: 0x04000483 RID: 1155
		private RenderTargetIdentifier m_DepthRenderTarget;

		// Token: 0x04000484 RID: 1156
		private RenderBufferLoadAction[] m_ColorLoadActions;

		// Token: 0x04000485 RID: 1157
		private RenderBufferStoreAction[] m_ColorStoreActions;

		// Token: 0x04000486 RID: 1158
		private RenderBufferLoadAction m_DepthLoadAction;

		// Token: 0x04000487 RID: 1159
		private RenderBufferStoreAction m_DepthStoreAction;
	}
}
