﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F4 RID: 244
	internal static class CameraEventUtils
	{
		// Token: 0x06000B3D RID: 2877 RVA: 0x0001250C File Offset: 0x0001070C
		public static bool IsValid(CameraEvent value)
		{
			return value >= CameraEvent.BeforeDepthTexture && value <= CameraEvent.AfterHaloAndLensFlares;
		}

		// Token: 0x04000419 RID: 1049
		private const CameraEvent k_MinimumValue = CameraEvent.BeforeDepthTexture;

		// Token: 0x0400041A RID: 1050
		private const CameraEvent k_MaximumValue = CameraEvent.AfterHaloAndLensFlares;
	}
}
