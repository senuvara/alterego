﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000105 RID: 261
	public enum ReflectionProbeTimeSlicingMode
	{
		// Token: 0x0400049E RID: 1182
		AllFacesAtOnce,
		// Token: 0x0400049F RID: 1183
		IndividualFaces,
		// Token: 0x040004A0 RID: 1184
		NoTimeSlicing
	}
}
