﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200010E RID: 270
	public enum RealtimeGICPUUsage
	{
		// Token: 0x040004E8 RID: 1256
		Low = 25,
		// Token: 0x040004E9 RID: 1257
		Medium = 50,
		// Token: 0x040004EA RID: 1258
		High = 75,
		// Token: 0x040004EB RID: 1259
		Unlimited = 100
	}
}
