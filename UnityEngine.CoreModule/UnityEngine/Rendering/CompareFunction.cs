﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000EC RID: 236
	public enum CompareFunction
	{
		// Token: 0x040003D7 RID: 983
		Disabled,
		// Token: 0x040003D8 RID: 984
		Never,
		// Token: 0x040003D9 RID: 985
		Less,
		// Token: 0x040003DA RID: 986
		Equal,
		// Token: 0x040003DB RID: 987
		LessEqual,
		// Token: 0x040003DC RID: 988
		Greater,
		// Token: 0x040003DD RID: 989
		NotEqual,
		// Token: 0x040003DE RID: 990
		GreaterEqual,
		// Token: 0x040003DF RID: 991
		Always
	}
}
