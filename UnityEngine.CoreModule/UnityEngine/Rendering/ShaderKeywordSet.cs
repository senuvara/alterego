﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x02000213 RID: 531
	[UsedByNativeCode]
	public struct ShaderKeywordSet
	{
		// Token: 0x060011EA RID: 4586 RVA: 0x0001E2A0 File Offset: 0x0001C4A0
		private void ComputeSliceAndMask(ShaderKeyword keyword, out uint slice, out uint mask)
		{
			int keywordIndex = keyword.GetKeywordIndex();
			slice = (uint)(keywordIndex / 32);
			mask = 1U << keywordIndex % 32;
		}

		// Token: 0x060011EB RID: 4587 RVA: 0x0001E2C8 File Offset: 0x0001C4C8
		public unsafe bool IsEnabled(ShaderKeyword keyword)
		{
			bool result;
			if (!keyword.IsValid())
			{
				result = false;
			}
			else
			{
				uint num;
				uint num2;
				this.ComputeSliceAndMask(keyword, out num, out num2);
				fixed (uint* ptr = &this.m_Bits.FixedElementField)
				{
					result = ((ptr[(UIntPtr)num * 4] & num2) != 0U);
				}
			}
			return result;
		}

		// Token: 0x060011EC RID: 4588 RVA: 0x0001E318 File Offset: 0x0001C518
		public unsafe void Enable(ShaderKeyword keyword)
		{
			if (keyword.IsValid())
			{
				uint num;
				uint num2;
				this.ComputeSliceAndMask(keyword, out num, out num2);
				fixed (uint* ptr = &this.m_Bits.FixedElementField)
				{
					ptr[(UIntPtr)num * 4] |= num2;
				}
			}
		}

		// Token: 0x060011ED RID: 4589 RVA: 0x0001E360 File Offset: 0x0001C560
		public unsafe void Disable(ShaderKeyword keyword)
		{
			if (keyword.IsValid())
			{
				uint num;
				uint num2;
				this.ComputeSliceAndMask(keyword, out num, out num2);
				fixed (uint* ptr = &this.m_Bits.FixedElementField)
				{
					ptr[(UIntPtr)num * 4] &= ~num2;
				}
			}
		}

		// Token: 0x060011EE RID: 4590 RVA: 0x0001E3A8 File Offset: 0x0001C5A8
		public ShaderKeyword[] GetShaderKeywords()
		{
			ShaderKeyword[] array = new ShaderKeyword[256];
			int num = 0;
			for (int i = 0; i < 256; i++)
			{
				ShaderKeyword shaderKeyword = new ShaderKeyword(i);
				if (this.IsEnabled(shaderKeyword))
				{
					array[num] = shaderKeyword;
					num++;
				}
			}
			Array.Resize<ShaderKeyword>(ref array, num);
			return array;
		}

		// Token: 0x04000764 RID: 1892
		private const int k_SizeInBits = 32;

		// Token: 0x04000765 RID: 1893
		[FixedBuffer(typeof(uint), 8)]
		internal ShaderKeywordSet.<m_Bits>__FixedBuffer0 m_Bits;

		// Token: 0x02000214 RID: 532
		[CompilerGenerated]
		[UnsafeValueType]
		[StructLayout(LayoutKind.Sequential, Size = 32)]
		public struct <m_Bits>__FixedBuffer0
		{
			// Token: 0x04000766 RID: 1894
			public uint FixedElementField;
		}
	}
}
