﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x02000212 RID: 530
	[NativeHeader("Runtime/Shaders/ShaderKeywords.h")]
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class ShaderKeyword
	{
		// Token: 0x060011E0 RID: 4576 RVA: 0x0001E1C1 File Offset: 0x0001C3C1
		internal ShaderKeyword(int keywordIndex)
		{
			this.m_KeywordIndex = keywordIndex;
		}

		// Token: 0x060011E1 RID: 4577 RVA: 0x0001E1D1 File Offset: 0x0001C3D1
		public ShaderKeyword(string keywordName)
		{
			this.m_KeywordIndex = ShaderKeyword.GetShaderKeywordIndex(keywordName);
		}

		// Token: 0x060011E2 RID: 4578
		[NativeMethod("keywords::Find", true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetShaderKeywordIndex(string keywordName);

		// Token: 0x060011E3 RID: 4579
		[NativeMethod("keywords::GetKeywordName", true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string GetShaderKeywordName(int keywordIndex);

		// Token: 0x060011E4 RID: 4580
		[NativeMethod("keywords::GetKeywordType", true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern ShaderKeywordType GetShaderKeywordType(int keywordIndex);

		// Token: 0x060011E5 RID: 4581 RVA: 0x0001E1E8 File Offset: 0x0001C3E8
		public bool IsValid()
		{
			return this.m_KeywordIndex >= 0 && this.m_KeywordIndex < 256 && this.m_KeywordIndex != -1;
		}

		// Token: 0x060011E6 RID: 4582 RVA: 0x0001E228 File Offset: 0x0001C428
		public ShaderKeywordType GetKeywordType()
		{
			return ShaderKeyword.GetShaderKeywordType(this.m_KeywordIndex);
		}

		// Token: 0x060011E7 RID: 4583 RVA: 0x0001E248 File Offset: 0x0001C448
		public string GetKeywordName()
		{
			return ShaderKeyword.GetShaderKeywordName(this.m_KeywordIndex);
		}

		// Token: 0x060011E8 RID: 4584 RVA: 0x0001E268 File Offset: 0x0001C468
		internal int GetKeywordIndex()
		{
			return this.m_KeywordIndex;
		}

		// Token: 0x060011E9 RID: 4585 RVA: 0x0001E284 File Offset: 0x0001C484
		[Obsolete("GetName() has been deprecated. Use GetKeywordName() instead (UnityUpgradable) -> GetKeywordName()")]
		public string GetName()
		{
			return this.GetKeywordName();
		}

		// Token: 0x04000761 RID: 1889
		internal const int k_MaxShaderKeywords = 256;

		// Token: 0x04000762 RID: 1890
		private const int k_InvalidKeyword = -1;

		// Token: 0x04000763 RID: 1891
		internal int m_KeywordIndex;
	}
}
