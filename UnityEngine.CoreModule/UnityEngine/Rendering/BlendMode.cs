﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000EA RID: 234
	public enum BlendMode
	{
		// Token: 0x040003A6 RID: 934
		Zero,
		// Token: 0x040003A7 RID: 935
		One,
		// Token: 0x040003A8 RID: 936
		DstColor,
		// Token: 0x040003A9 RID: 937
		SrcColor,
		// Token: 0x040003AA RID: 938
		OneMinusDstColor,
		// Token: 0x040003AB RID: 939
		SrcAlpha,
		// Token: 0x040003AC RID: 940
		OneMinusSrcColor,
		// Token: 0x040003AD RID: 941
		DstAlpha,
		// Token: 0x040003AE RID: 942
		OneMinusDstAlpha,
		// Token: 0x040003AF RID: 943
		SrcAlphaSaturate,
		// Token: 0x040003B0 RID: 944
		OneMinusSrcAlpha
	}
}
