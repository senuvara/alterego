﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F7 RID: 247
	public enum BuiltinRenderTextureType
	{
		// Token: 0x04000432 RID: 1074
		PropertyName = -4,
		// Token: 0x04000433 RID: 1075
		BufferPtr,
		// Token: 0x04000434 RID: 1076
		RenderTexture,
		// Token: 0x04000435 RID: 1077
		BindableTexture,
		// Token: 0x04000436 RID: 1078
		None,
		// Token: 0x04000437 RID: 1079
		CurrentActive,
		// Token: 0x04000438 RID: 1080
		CameraTarget,
		// Token: 0x04000439 RID: 1081
		Depth,
		// Token: 0x0400043A RID: 1082
		DepthNormals,
		// Token: 0x0400043B RID: 1083
		ResolvedDepth,
		// Token: 0x0400043C RID: 1084
		PrepassNormalsSpec = 7,
		// Token: 0x0400043D RID: 1085
		PrepassLight,
		// Token: 0x0400043E RID: 1086
		PrepassLightSpec,
		// Token: 0x0400043F RID: 1087
		GBuffer0,
		// Token: 0x04000440 RID: 1088
		GBuffer1,
		// Token: 0x04000441 RID: 1089
		GBuffer2,
		// Token: 0x04000442 RID: 1090
		GBuffer3,
		// Token: 0x04000443 RID: 1091
		Reflections,
		// Token: 0x04000444 RID: 1092
		MotionVectors,
		// Token: 0x04000445 RID: 1093
		GBuffer4,
		// Token: 0x04000446 RID: 1094
		GBuffer5,
		// Token: 0x04000447 RID: 1095
		GBuffer6,
		// Token: 0x04000448 RID: 1096
		GBuffer7
	}
}
