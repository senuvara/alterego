﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000EB RID: 235
	public enum BlendOp
	{
		// Token: 0x040003B2 RID: 946
		Add,
		// Token: 0x040003B3 RID: 947
		Subtract,
		// Token: 0x040003B4 RID: 948
		ReverseSubtract,
		// Token: 0x040003B5 RID: 949
		Min,
		// Token: 0x040003B6 RID: 950
		Max,
		// Token: 0x040003B7 RID: 951
		LogicalClear,
		// Token: 0x040003B8 RID: 952
		LogicalSet,
		// Token: 0x040003B9 RID: 953
		LogicalCopy,
		// Token: 0x040003BA RID: 954
		LogicalCopyInverted,
		// Token: 0x040003BB RID: 955
		LogicalNoop,
		// Token: 0x040003BC RID: 956
		LogicalInvert,
		// Token: 0x040003BD RID: 957
		LogicalAnd,
		// Token: 0x040003BE RID: 958
		LogicalNand,
		// Token: 0x040003BF RID: 959
		LogicalOr,
		// Token: 0x040003C0 RID: 960
		LogicalNor,
		// Token: 0x040003C1 RID: 961
		LogicalXor,
		// Token: 0x040003C2 RID: 962
		LogicalEquivalence,
		// Token: 0x040003C3 RID: 963
		LogicalAndReverse,
		// Token: 0x040003C4 RID: 964
		LogicalAndInverted,
		// Token: 0x040003C5 RID: 965
		LogicalOrReverse,
		// Token: 0x040003C6 RID: 966
		LogicalOrInverted,
		// Token: 0x040003C7 RID: 967
		Multiply,
		// Token: 0x040003C8 RID: 968
		Screen,
		// Token: 0x040003C9 RID: 969
		Overlay,
		// Token: 0x040003CA RID: 970
		Darken,
		// Token: 0x040003CB RID: 971
		Lighten,
		// Token: 0x040003CC RID: 972
		ColorDodge,
		// Token: 0x040003CD RID: 973
		ColorBurn,
		// Token: 0x040003CE RID: 974
		HardLight,
		// Token: 0x040003CF RID: 975
		SoftLight,
		// Token: 0x040003D0 RID: 976
		Difference,
		// Token: 0x040003D1 RID: 977
		Exclusion,
		// Token: 0x040003D2 RID: 978
		HSLHue,
		// Token: 0x040003D3 RID: 979
		HSLSaturation,
		// Token: 0x040003D4 RID: 980
		HSLColor,
		// Token: 0x040003D5 RID: 981
		HSLLuminosity
	}
}
