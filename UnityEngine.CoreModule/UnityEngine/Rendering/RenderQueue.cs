﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000E7 RID: 231
	public enum RenderQueue
	{
		// Token: 0x04000396 RID: 918
		Background = 1000,
		// Token: 0x04000397 RID: 919
		Geometry = 2000,
		// Token: 0x04000398 RID: 920
		AlphaTest = 2450,
		// Token: 0x04000399 RID: 921
		GeometryLast = 2500,
		// Token: 0x0400039A RID: 922
		Transparent = 3000,
		// Token: 0x0400039B RID: 923
		Overlay = 4000
	}
}
