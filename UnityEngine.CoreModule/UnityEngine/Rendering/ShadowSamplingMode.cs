﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000106 RID: 262
	public enum ShadowSamplingMode
	{
		// Token: 0x040004A2 RID: 1186
		CompareDepths,
		// Token: 0x040004A3 RID: 1187
		RawDepth,
		// Token: 0x040004A4 RID: 1188
		None
	}
}
