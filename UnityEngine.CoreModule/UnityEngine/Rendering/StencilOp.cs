﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000EF RID: 239
	public enum StencilOp
	{
		// Token: 0x040003EB RID: 1003
		Keep,
		// Token: 0x040003EC RID: 1004
		Zero,
		// Token: 0x040003ED RID: 1005
		Replace,
		// Token: 0x040003EE RID: 1006
		IncrementSaturate,
		// Token: 0x040003EF RID: 1007
		DecrementSaturate,
		// Token: 0x040003F0 RID: 1008
		Invert,
		// Token: 0x040003F1 RID: 1009
		IncrementWrap,
		// Token: 0x040003F2 RID: 1010
		DecrementWrap
	}
}
