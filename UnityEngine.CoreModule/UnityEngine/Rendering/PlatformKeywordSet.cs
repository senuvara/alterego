﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x0200015E RID: 350
	[UsedByNativeCode]
	public struct PlatformKeywordSet
	{
		// Token: 0x06000F32 RID: 3890 RVA: 0x0001A5E8 File Offset: 0x000187E8
		private uint ComputeKeywordMask(BuiltinShaderDefine define)
		{
			return 1U << (int)(define % (BuiltinShaderDefine)32);
		}

		// Token: 0x06000F33 RID: 3891 RVA: 0x0001A608 File Offset: 0x00018808
		public bool IsEnabled(BuiltinShaderDefine define)
		{
			return (this.m_Bits & this.ComputeKeywordMask(define)) != 0U;
		}

		// Token: 0x06000F34 RID: 3892 RVA: 0x0001A631 File Offset: 0x00018831
		public void Enable(BuiltinShaderDefine define)
		{
			this.m_Bits |= this.ComputeKeywordMask(define);
		}

		// Token: 0x06000F35 RID: 3893 RVA: 0x0001A648 File Offset: 0x00018848
		public void Disable(BuiltinShaderDefine define)
		{
			this.m_Bits &= ~this.ComputeKeywordMask(define);
		}

		// Token: 0x0400070C RID: 1804
		private const int k_SizeInBits = 32;

		// Token: 0x0400070D RID: 1805
		internal uint m_Bits;
	}
}
