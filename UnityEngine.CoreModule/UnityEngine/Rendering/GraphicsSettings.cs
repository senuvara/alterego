﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;

namespace UnityEngine.Rendering
{
	// Token: 0x02000115 RID: 277
	[StaticAccessor("GetGraphicsSettings()", StaticAccessorType.Dot)]
	[NativeHeader("Runtime/Camera/GraphicsSettings.h")]
	public sealed class GraphicsSettings : Object
	{
		// Token: 0x06000BAF RID: 2991 RVA: 0x0000D647 File Offset: 0x0000B847
		private GraphicsSettings()
		{
		}

		// Token: 0x1700022C RID: 556
		// (get) Token: 0x06000BB0 RID: 2992
		// (set) Token: 0x06000BB1 RID: 2993
		public static extern TransparencySortMode transparencySortMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x06000BB2 RID: 2994 RVA: 0x00012D88 File Offset: 0x00010F88
		// (set) Token: 0x06000BB3 RID: 2995 RVA: 0x00012D9D File Offset: 0x00010F9D
		public static Vector3 transparencySortAxis
		{
			get
			{
				Vector3 result;
				GraphicsSettings.get_transparencySortAxis_Injected(out result);
				return result;
			}
			set
			{
				GraphicsSettings.set_transparencySortAxis_Injected(ref value);
			}
		}

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x06000BB4 RID: 2996
		// (set) Token: 0x06000BB5 RID: 2997
		public static extern bool lightsUseLinearIntensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700022F RID: 559
		// (get) Token: 0x06000BB6 RID: 2998
		// (set) Token: 0x06000BB7 RID: 2999
		public static extern bool lightsUseColorTemperature { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x06000BB8 RID: 3000
		// (set) Token: 0x06000BB9 RID: 3001
		public static extern bool useScriptableRenderPipelineBatching { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000BBA RID: 3002
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool HasShaderDefine(GraphicsTier tier, BuiltinShaderDefine defineHash);

		// Token: 0x06000BBB RID: 3003 RVA: 0x00012DA8 File Offset: 0x00010FA8
		public static bool HasShaderDefine(BuiltinShaderDefine defineHash)
		{
			return GraphicsSettings.HasShaderDefine(Graphics.activeTier, defineHash);
		}

		// Token: 0x17000231 RID: 561
		// (get) Token: 0x06000BBC RID: 3004
		// (set) Token: 0x06000BBD RID: 3005
		[NativeName("RenderPipeline")]
		private static extern ScriptableObject INTERNAL_renderPipelineAsset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x06000BBE RID: 3006 RVA: 0x00012DC8 File Offset: 0x00010FC8
		// (set) Token: 0x06000BBF RID: 3007 RVA: 0x00012DE7 File Offset: 0x00010FE7
		public static RenderPipelineAsset renderPipelineAsset
		{
			get
			{
				return GraphicsSettings.INTERNAL_renderPipelineAsset as RenderPipelineAsset;
			}
			set
			{
				GraphicsSettings.INTERNAL_renderPipelineAsset = value;
			}
		}

		// Token: 0x06000BC0 RID: 3008
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Object GetGraphicsSettings();

		// Token: 0x06000BC1 RID: 3009
		[NativeName("SetShaderModeScript")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetShaderMode(BuiltinShaderType type, BuiltinShaderMode mode);

		// Token: 0x06000BC2 RID: 3010
		[NativeName("GetShaderModeScript")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern BuiltinShaderMode GetShaderMode(BuiltinShaderType type);

		// Token: 0x06000BC3 RID: 3011
		[NativeName("SetCustomShaderScript")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetCustomShader(BuiltinShaderType type, Shader shader);

		// Token: 0x06000BC4 RID: 3012
		[NativeName("GetCustomShaderScript")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Shader GetCustomShader(BuiltinShaderType type);

		// Token: 0x06000BC5 RID: 3013
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_transparencySortAxis_Injected(out Vector3 ret);

		// Token: 0x06000BC6 RID: 3014
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_transparencySortAxis_Injected(ref Vector3 value);
	}
}
