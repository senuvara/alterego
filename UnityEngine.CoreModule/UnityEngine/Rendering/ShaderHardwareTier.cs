﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000A3 RID: 163
	[Obsolete("ShaderHardwareTier was renamed to GraphicsTier (UnityUpgradable) -> GraphicsTier", false)]
	public enum ShaderHardwareTier
	{
		// Token: 0x040001A1 RID: 417
		Tier1,
		// Token: 0x040001A2 RID: 418
		Tier2,
		// Token: 0x040001A3 RID: 419
		Tier3
	}
}
