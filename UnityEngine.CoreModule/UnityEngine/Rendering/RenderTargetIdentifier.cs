﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000FD RID: 253
	public struct RenderTargetIdentifier : IEquatable<RenderTargetIdentifier>
	{
		// Token: 0x06000B3E RID: 2878 RVA: 0x00012533 File Offset: 0x00010733
		public RenderTargetIdentifier(BuiltinRenderTextureType type)
		{
			this.m_Type = type;
			this.m_NameID = -1;
			this.m_InstanceID = 0;
			this.m_BufferPointer = IntPtr.Zero;
			this.m_MipLevel = 0;
			this.m_CubeFace = CubemapFace.Unknown;
			this.m_DepthSlice = 0;
		}

		// Token: 0x06000B3F RID: 2879 RVA: 0x0001256B File Offset: 0x0001076B
		public RenderTargetIdentifier(string name)
		{
			this.m_Type = BuiltinRenderTextureType.PropertyName;
			this.m_NameID = Shader.PropertyToID(name);
			this.m_InstanceID = 0;
			this.m_BufferPointer = IntPtr.Zero;
			this.m_MipLevel = 0;
			this.m_CubeFace = CubemapFace.Unknown;
			this.m_DepthSlice = 0;
		}

		// Token: 0x06000B40 RID: 2880 RVA: 0x000125A9 File Offset: 0x000107A9
		public RenderTargetIdentifier(string name, int mipLevel = 0, CubemapFace cubeFace = CubemapFace.Unknown, int depthSlice = 0)
		{
			this.m_Type = BuiltinRenderTextureType.PropertyName;
			this.m_NameID = Shader.PropertyToID(name);
			this.m_InstanceID = 0;
			this.m_BufferPointer = IntPtr.Zero;
			this.m_MipLevel = mipLevel;
			this.m_CubeFace = cubeFace;
			this.m_DepthSlice = depthSlice;
		}

		// Token: 0x06000B41 RID: 2881 RVA: 0x000125E8 File Offset: 0x000107E8
		public RenderTargetIdentifier(int nameID)
		{
			this.m_Type = BuiltinRenderTextureType.PropertyName;
			this.m_NameID = nameID;
			this.m_InstanceID = 0;
			this.m_BufferPointer = IntPtr.Zero;
			this.m_MipLevel = 0;
			this.m_CubeFace = CubemapFace.Unknown;
			this.m_DepthSlice = 0;
		}

		// Token: 0x06000B42 RID: 2882 RVA: 0x00012621 File Offset: 0x00010821
		public RenderTargetIdentifier(int nameID, int mipLevel = 0, CubemapFace cubeFace = CubemapFace.Unknown, int depthSlice = 0)
		{
			this.m_Type = BuiltinRenderTextureType.PropertyName;
			this.m_NameID = nameID;
			this.m_InstanceID = 0;
			this.m_BufferPointer = IntPtr.Zero;
			this.m_MipLevel = mipLevel;
			this.m_CubeFace = cubeFace;
			this.m_DepthSlice = depthSlice;
		}

		// Token: 0x06000B43 RID: 2883 RVA: 0x0001265C File Offset: 0x0001085C
		public RenderTargetIdentifier(RenderTargetIdentifier renderTargetIdentifier, int mipLevel, CubemapFace cubeFace = CubemapFace.Unknown, int depthSlice = 0)
		{
			this.m_Type = renderTargetIdentifier.m_Type;
			this.m_NameID = renderTargetIdentifier.m_NameID;
			this.m_InstanceID = renderTargetIdentifier.m_InstanceID;
			this.m_BufferPointer = renderTargetIdentifier.m_BufferPointer;
			this.m_MipLevel = mipLevel;
			this.m_CubeFace = cubeFace;
			this.m_DepthSlice = depthSlice;
		}

		// Token: 0x06000B44 RID: 2884 RVA: 0x000126B4 File Offset: 0x000108B4
		public RenderTargetIdentifier(Texture tex)
		{
			if (tex == null)
			{
				this.m_Type = BuiltinRenderTextureType.None;
			}
			else if (tex is RenderTexture)
			{
				this.m_Type = BuiltinRenderTextureType.RenderTexture;
			}
			else
			{
				this.m_Type = BuiltinRenderTextureType.BindableTexture;
			}
			this.m_BufferPointer = IntPtr.Zero;
			this.m_NameID = -1;
			this.m_InstanceID = ((!tex) ? 0 : tex.GetInstanceID());
			this.m_MipLevel = 0;
			this.m_CubeFace = CubemapFace.Unknown;
			this.m_DepthSlice = 0;
		}

		// Token: 0x06000B45 RID: 2885 RVA: 0x00012744 File Offset: 0x00010944
		public RenderTargetIdentifier(Texture tex, int mipLevel = 0, CubemapFace cubeFace = CubemapFace.Unknown, int depthSlice = 0)
		{
			if (tex == null)
			{
				this.m_Type = BuiltinRenderTextureType.None;
			}
			else if (tex is RenderTexture)
			{
				this.m_Type = BuiltinRenderTextureType.RenderTexture;
			}
			else
			{
				this.m_Type = BuiltinRenderTextureType.BindableTexture;
			}
			this.m_BufferPointer = IntPtr.Zero;
			this.m_NameID = -1;
			this.m_InstanceID = ((!tex) ? 0 : tex.GetInstanceID());
			this.m_MipLevel = mipLevel;
			this.m_CubeFace = cubeFace;
			this.m_DepthSlice = depthSlice;
		}

		// Token: 0x06000B46 RID: 2886 RVA: 0x000127D4 File Offset: 0x000109D4
		public RenderTargetIdentifier(RenderBuffer buf, int mipLevel = 0, CubemapFace cubeFace = CubemapFace.Unknown, int depthSlice = 0)
		{
			this.m_Type = BuiltinRenderTextureType.BufferPtr;
			this.m_NameID = -1;
			this.m_InstanceID = buf.m_RenderTextureInstanceID;
			this.m_BufferPointer = buf.m_BufferPtr;
			this.m_MipLevel = mipLevel;
			this.m_CubeFace = cubeFace;
			this.m_DepthSlice = depthSlice;
		}

		// Token: 0x06000B47 RID: 2887 RVA: 0x00012824 File Offset: 0x00010A24
		public static implicit operator RenderTargetIdentifier(BuiltinRenderTextureType type)
		{
			return new RenderTargetIdentifier(type);
		}

		// Token: 0x06000B48 RID: 2888 RVA: 0x00012840 File Offset: 0x00010A40
		public static implicit operator RenderTargetIdentifier(string name)
		{
			return new RenderTargetIdentifier(name);
		}

		// Token: 0x06000B49 RID: 2889 RVA: 0x0001285C File Offset: 0x00010A5C
		public static implicit operator RenderTargetIdentifier(int nameID)
		{
			return new RenderTargetIdentifier(nameID);
		}

		// Token: 0x06000B4A RID: 2890 RVA: 0x00012878 File Offset: 0x00010A78
		public static implicit operator RenderTargetIdentifier(Texture tex)
		{
			return new RenderTargetIdentifier(tex);
		}

		// Token: 0x06000B4B RID: 2891 RVA: 0x00012894 File Offset: 0x00010A94
		public static implicit operator RenderTargetIdentifier(RenderBuffer buf)
		{
			return new RenderTargetIdentifier(buf, 0, CubemapFace.Unknown, 0);
		}

		// Token: 0x06000B4C RID: 2892 RVA: 0x000128B4 File Offset: 0x00010AB4
		public override string ToString()
		{
			return UnityString.Format("Type {0} NameID {1} InstanceID {2}", new object[]
			{
				this.m_Type,
				this.m_NameID,
				this.m_InstanceID
			});
		}

		// Token: 0x06000B4D RID: 2893 RVA: 0x00012904 File Offset: 0x00010B04
		public override int GetHashCode()
		{
			return (this.m_Type.GetHashCode() * 23 + this.m_NameID.GetHashCode()) * 23 + this.m_InstanceID.GetHashCode();
		}

		// Token: 0x06000B4E RID: 2894 RVA: 0x00012954 File Offset: 0x00010B54
		public bool Equals(RenderTargetIdentifier rhs)
		{
			return this.m_Type == rhs.m_Type && this.m_NameID == rhs.m_NameID && this.m_InstanceID == rhs.m_InstanceID && this.m_BufferPointer == rhs.m_BufferPointer && this.m_MipLevel == rhs.m_MipLevel && this.m_CubeFace == rhs.m_CubeFace && this.m_DepthSlice == rhs.m_DepthSlice;
		}

		// Token: 0x06000B4F RID: 2895 RVA: 0x000129EC File Offset: 0x00010BEC
		public override bool Equals(object obj)
		{
			bool result;
			if (!(obj is RenderTargetIdentifier))
			{
				result = false;
			}
			else
			{
				RenderTargetIdentifier rhs = (RenderTargetIdentifier)obj;
				result = this.Equals(rhs);
			}
			return result;
		}

		// Token: 0x06000B50 RID: 2896 RVA: 0x00012A24 File Offset: 0x00010C24
		public static bool operator ==(RenderTargetIdentifier lhs, RenderTargetIdentifier rhs)
		{
			return lhs.Equals(rhs);
		}

		// Token: 0x06000B51 RID: 2897 RVA: 0x00012A44 File Offset: 0x00010C44
		public static bool operator !=(RenderTargetIdentifier lhs, RenderTargetIdentifier rhs)
		{
			return !lhs.Equals(rhs);
		}

		// Token: 0x0400047B RID: 1147
		private BuiltinRenderTextureType m_Type;

		// Token: 0x0400047C RID: 1148
		private int m_NameID;

		// Token: 0x0400047D RID: 1149
		private int m_InstanceID;

		// Token: 0x0400047E RID: 1150
		private IntPtr m_BufferPointer;

		// Token: 0x0400047F RID: 1151
		private int m_MipLevel;

		// Token: 0x04000480 RID: 1152
		private CubemapFace m_CubeFace;

		// Token: 0x04000481 RID: 1153
		private int m_DepthSlice;
	}
}
