﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Rendering
{
	// Token: 0x02000020 RID: 32
	[StaticAccessor("AsyncGPUReadbackManager::GetInstance()", StaticAccessorType.Dot)]
	public static class AsyncGPUReadback
	{
		// Token: 0x06000243 RID: 579 RVA: 0x00008325 File Offset: 0x00006525
		private static void SetUpScriptingRequest(AsyncGPUReadbackRequest request, Action<AsyncGPUReadbackRequest> callback)
		{
			request.SetScriptingCallback(callback);
		}

		// Token: 0x06000244 RID: 580 RVA: 0x00008330 File Offset: 0x00006530
		public static AsyncGPUReadbackRequest Request(ComputeBuffer src, Action<AsyncGPUReadbackRequest> callback = null)
		{
			AsyncGPUReadbackRequest asyncGPUReadbackRequest = AsyncGPUReadback.Request_Internal_ComputeBuffer_1(src);
			AsyncGPUReadback.SetUpScriptingRequest(asyncGPUReadbackRequest, callback);
			return asyncGPUReadbackRequest;
		}

		// Token: 0x06000245 RID: 581 RVA: 0x00008354 File Offset: 0x00006554
		public static AsyncGPUReadbackRequest Request(ComputeBuffer src, int size, int offset, Action<AsyncGPUReadbackRequest> callback = null)
		{
			AsyncGPUReadbackRequest asyncGPUReadbackRequest = AsyncGPUReadback.Request_Internal_ComputeBuffer_2(src, size, offset);
			AsyncGPUReadback.SetUpScriptingRequest(asyncGPUReadbackRequest, callback);
			return asyncGPUReadbackRequest;
		}

		// Token: 0x06000246 RID: 582 RVA: 0x0000837C File Offset: 0x0000657C
		public static AsyncGPUReadbackRequest Request(Texture src, int mipIndex = 0, Action<AsyncGPUReadbackRequest> callback = null)
		{
			AsyncGPUReadbackRequest asyncGPUReadbackRequest = AsyncGPUReadback.Request_Internal_Texture_1(src, mipIndex);
			AsyncGPUReadback.SetUpScriptingRequest(asyncGPUReadbackRequest, callback);
			return asyncGPUReadbackRequest;
		}

		// Token: 0x06000247 RID: 583 RVA: 0x000083A4 File Offset: 0x000065A4
		public static AsyncGPUReadbackRequest Request(Texture src, int mipIndex, TextureFormat dstFormat, Action<AsyncGPUReadbackRequest> callback = null)
		{
			AsyncGPUReadbackRequest asyncGPUReadbackRequest = AsyncGPUReadback.Request_Internal_Texture_2(src, mipIndex, dstFormat);
			AsyncGPUReadback.SetUpScriptingRequest(asyncGPUReadbackRequest, callback);
			return asyncGPUReadbackRequest;
		}

		// Token: 0x06000248 RID: 584 RVA: 0x000083CC File Offset: 0x000065CC
		public static AsyncGPUReadbackRequest Request(Texture src, int mipIndex, int x, int width, int y, int height, int z, int depth, Action<AsyncGPUReadbackRequest> callback = null)
		{
			AsyncGPUReadbackRequest asyncGPUReadbackRequest = AsyncGPUReadback.Request_Internal_Texture_3(src, mipIndex, x, width, y, height, z, depth);
			AsyncGPUReadback.SetUpScriptingRequest(asyncGPUReadbackRequest, callback);
			return asyncGPUReadbackRequest;
		}

		// Token: 0x06000249 RID: 585 RVA: 0x000083FC File Offset: 0x000065FC
		public static AsyncGPUReadbackRequest Request(Texture src, int mipIndex, int x, int width, int y, int height, int z, int depth, TextureFormat dstFormat, Action<AsyncGPUReadbackRequest> callback = null)
		{
			AsyncGPUReadbackRequest asyncGPUReadbackRequest = AsyncGPUReadback.Request_Internal_Texture_4(src, mipIndex, x, width, y, height, z, depth, dstFormat);
			AsyncGPUReadback.SetUpScriptingRequest(asyncGPUReadbackRequest, callback);
			return asyncGPUReadbackRequest;
		}

		// Token: 0x0600024A RID: 586 RVA: 0x00008430 File Offset: 0x00006630
		[NativeMethod("Request")]
		private static AsyncGPUReadbackRequest Request_Internal_ComputeBuffer_1([NotNull] ComputeBuffer buffer)
		{
			AsyncGPUReadbackRequest result;
			AsyncGPUReadback.Request_Internal_ComputeBuffer_1_Injected(buffer, out result);
			return result;
		}

		// Token: 0x0600024B RID: 587 RVA: 0x00008448 File Offset: 0x00006648
		[NativeMethod("Request")]
		private static AsyncGPUReadbackRequest Request_Internal_ComputeBuffer_2([NotNull] ComputeBuffer src, int size, int offset)
		{
			AsyncGPUReadbackRequest result;
			AsyncGPUReadback.Request_Internal_ComputeBuffer_2_Injected(src, size, offset, out result);
			return result;
		}

		// Token: 0x0600024C RID: 588 RVA: 0x00008460 File Offset: 0x00006660
		[NativeMethod("Request")]
		private static AsyncGPUReadbackRequest Request_Internal_Texture_1([NotNull] Texture src, int mipIndex)
		{
			AsyncGPUReadbackRequest result;
			AsyncGPUReadback.Request_Internal_Texture_1_Injected(src, mipIndex, out result);
			return result;
		}

		// Token: 0x0600024D RID: 589 RVA: 0x00008478 File Offset: 0x00006678
		[NativeMethod("Request")]
		private static AsyncGPUReadbackRequest Request_Internal_Texture_2([NotNull] Texture src, int mipIndex, TextureFormat dstFormat)
		{
			AsyncGPUReadbackRequest result;
			AsyncGPUReadback.Request_Internal_Texture_2_Injected(src, mipIndex, dstFormat, out result);
			return result;
		}

		// Token: 0x0600024E RID: 590 RVA: 0x00008490 File Offset: 0x00006690
		[NativeMethod("Request")]
		private static AsyncGPUReadbackRequest Request_Internal_Texture_3([NotNull] Texture src, int mipIndex, int x, int width, int y, int height, int z, int depth)
		{
			AsyncGPUReadbackRequest result;
			AsyncGPUReadback.Request_Internal_Texture_3_Injected(src, mipIndex, x, width, y, height, z, depth, out result);
			return result;
		}

		// Token: 0x0600024F RID: 591 RVA: 0x000084B4 File Offset: 0x000066B4
		[NativeMethod("Request")]
		private static AsyncGPUReadbackRequest Request_Internal_Texture_4([NotNull] Texture src, int mipIndex, int x, int width, int y, int height, int z, int depth, TextureFormat dstFormat)
		{
			AsyncGPUReadbackRequest result;
			AsyncGPUReadback.Request_Internal_Texture_4_Injected(src, mipIndex, x, width, y, height, z, depth, dstFormat, out result);
			return result;
		}

		// Token: 0x06000250 RID: 592
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Request_Internal_ComputeBuffer_1_Injected(ComputeBuffer buffer, out AsyncGPUReadbackRequest ret);

		// Token: 0x06000251 RID: 593
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Request_Internal_ComputeBuffer_2_Injected(ComputeBuffer src, int size, int offset, out AsyncGPUReadbackRequest ret);

		// Token: 0x06000252 RID: 594
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Request_Internal_Texture_1_Injected(Texture src, int mipIndex, out AsyncGPUReadbackRequest ret);

		// Token: 0x06000253 RID: 595
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Request_Internal_Texture_2_Injected(Texture src, int mipIndex, TextureFormat dstFormat, out AsyncGPUReadbackRequest ret);

		// Token: 0x06000254 RID: 596
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Request_Internal_Texture_3_Injected(Texture src, int mipIndex, int x, int width, int y, int height, int z, int depth, out AsyncGPUReadbackRequest ret);

		// Token: 0x06000255 RID: 597
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Request_Internal_Texture_4_Injected(Texture src, int mipIndex, int x, int width, int y, int height, int z, int depth, TextureFormat dstFormat, out AsyncGPUReadbackRequest ret);
	}
}
