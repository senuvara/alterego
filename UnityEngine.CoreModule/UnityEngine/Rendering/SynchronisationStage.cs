﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000073 RID: 115
	public enum SynchronisationStage
	{
		// Token: 0x0400014B RID: 331
		VertexProcessing,
		// Token: 0x0400014C RID: 332
		PixelProcessing
	}
}
