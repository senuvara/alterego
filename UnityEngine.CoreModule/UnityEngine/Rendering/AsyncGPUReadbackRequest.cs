﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x0200001F RID: 31
	[UsedByNativeCode]
	[NativeHeader("Runtime/Shaders/ComputeShader.h")]
	[NativeHeader("Runtime/Graphics/AsyncGPUReadbackManaged.h")]
	[NativeHeader("Runtime/Graphics/Texture.h")]
	public struct AsyncGPUReadbackRequest
	{
		// Token: 0x06000225 RID: 549 RVA: 0x0000817B File Offset: 0x0000637B
		public void Update()
		{
			AsyncGPUReadbackRequest.Update_Injected(ref this);
		}

		// Token: 0x06000226 RID: 550 RVA: 0x00008183 File Offset: 0x00006383
		public void WaitForCompletion()
		{
			AsyncGPUReadbackRequest.WaitForCompletion_Injected(ref this);
		}

		// Token: 0x06000227 RID: 551 RVA: 0x0000818C File Offset: 0x0000638C
		public unsafe NativeArray<T> GetData<T>(int layer = 0) where T : struct
		{
			if (!this.done || this.hasError)
			{
				throw new InvalidOperationException("Cannot access the data as it is not available");
			}
			if (layer < 0 || layer >= this.layerCount)
			{
				throw new ArgumentException(string.Format("Layer index is out of range {0} / {1}", layer, this.layerCount));
			}
			int num = UnsafeUtility.SizeOf<T>();
			return NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<T>((void*)this.GetDataRaw(layer), this.layerDataSize / num, Allocator.None);
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000228 RID: 552 RVA: 0x00008218 File Offset: 0x00006418
		public bool done
		{
			get
			{
				return this.IsDone();
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000229 RID: 553 RVA: 0x00008234 File Offset: 0x00006434
		public bool hasError
		{
			get
			{
				return this.HasError();
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x0600022A RID: 554 RVA: 0x00008250 File Offset: 0x00006450
		public int layerCount
		{
			get
			{
				return this.GetLayerCount();
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x0600022B RID: 555 RVA: 0x0000826C File Offset: 0x0000646C
		public int layerDataSize
		{
			get
			{
				return this.GetLayerDataSize();
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600022C RID: 556 RVA: 0x00008288 File Offset: 0x00006488
		public int width
		{
			get
			{
				return this.GetWidth();
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x0600022D RID: 557 RVA: 0x000082A4 File Offset: 0x000064A4
		public int height
		{
			get
			{
				return this.GetHeight();
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600022E RID: 558 RVA: 0x000082C0 File Offset: 0x000064C0
		public int depth
		{
			get
			{
				return this.GetDepth();
			}
		}

		// Token: 0x0600022F RID: 559 RVA: 0x000082DB File Offset: 0x000064DB
		private bool IsDone()
		{
			return AsyncGPUReadbackRequest.IsDone_Injected(ref this);
		}

		// Token: 0x06000230 RID: 560 RVA: 0x000082E3 File Offset: 0x000064E3
		private bool HasError()
		{
			return AsyncGPUReadbackRequest.HasError_Injected(ref this);
		}

		// Token: 0x06000231 RID: 561 RVA: 0x000082EB File Offset: 0x000064EB
		private int GetLayerCount()
		{
			return AsyncGPUReadbackRequest.GetLayerCount_Injected(ref this);
		}

		// Token: 0x06000232 RID: 562 RVA: 0x000082F3 File Offset: 0x000064F3
		private int GetLayerDataSize()
		{
			return AsyncGPUReadbackRequest.GetLayerDataSize_Injected(ref this);
		}

		// Token: 0x06000233 RID: 563 RVA: 0x000082FB File Offset: 0x000064FB
		private int GetWidth()
		{
			return AsyncGPUReadbackRequest.GetWidth_Injected(ref this);
		}

		// Token: 0x06000234 RID: 564 RVA: 0x00008303 File Offset: 0x00006503
		private int GetHeight()
		{
			return AsyncGPUReadbackRequest.GetHeight_Injected(ref this);
		}

		// Token: 0x06000235 RID: 565 RVA: 0x0000830B File Offset: 0x0000650B
		private int GetDepth()
		{
			return AsyncGPUReadbackRequest.GetDepth_Injected(ref this);
		}

		// Token: 0x06000236 RID: 566 RVA: 0x00008313 File Offset: 0x00006513
		internal void SetScriptingCallback(Action<AsyncGPUReadbackRequest> callback)
		{
			AsyncGPUReadbackRequest.SetScriptingCallback_Injected(ref this, callback);
		}

		// Token: 0x06000237 RID: 567 RVA: 0x0000831C File Offset: 0x0000651C
		private IntPtr GetDataRaw(int layer)
		{
			return AsyncGPUReadbackRequest.GetDataRaw_Injected(ref this, layer);
		}

		// Token: 0x06000238 RID: 568
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Update_Injected(ref AsyncGPUReadbackRequest _unity_self);

		// Token: 0x06000239 RID: 569
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void WaitForCompletion_Injected(ref AsyncGPUReadbackRequest _unity_self);

		// Token: 0x0600023A RID: 570
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsDone_Injected(ref AsyncGPUReadbackRequest _unity_self);

		// Token: 0x0600023B RID: 571
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool HasError_Injected(ref AsyncGPUReadbackRequest _unity_self);

		// Token: 0x0600023C RID: 572
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetLayerCount_Injected(ref AsyncGPUReadbackRequest _unity_self);

		// Token: 0x0600023D RID: 573
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetLayerDataSize_Injected(ref AsyncGPUReadbackRequest _unity_self);

		// Token: 0x0600023E RID: 574
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetWidth_Injected(ref AsyncGPUReadbackRequest _unity_self);

		// Token: 0x0600023F RID: 575
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetHeight_Injected(ref AsyncGPUReadbackRequest _unity_self);

		// Token: 0x06000240 RID: 576
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetDepth_Injected(ref AsyncGPUReadbackRequest _unity_self);

		// Token: 0x06000241 RID: 577
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetScriptingCallback_Injected(ref AsyncGPUReadbackRequest _unity_self, Action<AsyncGPUReadbackRequest> callback);

		// Token: 0x06000242 RID: 578
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetDataRaw_Injected(ref AsyncGPUReadbackRequest _unity_self, int layer);

		// Token: 0x04000055 RID: 85
		internal IntPtr m_Ptr;

		// Token: 0x04000056 RID: 86
		internal int m_Version;
	}
}
