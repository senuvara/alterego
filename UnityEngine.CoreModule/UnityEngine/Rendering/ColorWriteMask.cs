﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000EE RID: 238
	[Flags]
	public enum ColorWriteMask
	{
		// Token: 0x040003E5 RID: 997
		Alpha = 1,
		// Token: 0x040003E6 RID: 998
		Blue = 2,
		// Token: 0x040003E7 RID: 999
		Green = 4,
		// Token: 0x040003E8 RID: 1000
		Red = 8,
		// Token: 0x040003E9 RID: 1001
		All = 15
	}
}
