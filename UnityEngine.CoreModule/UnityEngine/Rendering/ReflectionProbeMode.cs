﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000102 RID: 258
	public enum ReflectionProbeMode
	{
		// Token: 0x04000494 RID: 1172
		Baked,
		// Token: 0x04000495 RID: 1173
		Realtime,
		// Token: 0x04000496 RID: 1174
		Custom
	}
}
