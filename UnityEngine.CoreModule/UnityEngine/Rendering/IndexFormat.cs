﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000E4 RID: 228
	public enum IndexFormat
	{
		// Token: 0x04000380 RID: 896
		UInt16,
		// Token: 0x04000381 RID: 897
		UInt32
	}
}
