﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F1 RID: 241
	public enum DefaultReflectionMode
	{
		// Token: 0x040003F9 RID: 1017
		Skybox,
		// Token: 0x040003FA RID: 1018
		Custom
	}
}
