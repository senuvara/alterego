﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000FF RID: 255
	public enum ReflectionProbeUsage
	{
		// Token: 0x04000489 RID: 1161
		Off,
		// Token: 0x0400048A RID: 1162
		BlendProbes,
		// Token: 0x0400048B RID: 1163
		BlendProbesAndSkybox,
		// Token: 0x0400048C RID: 1164
		Simple
	}
}
