﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x02000211 RID: 529
	[NativeHeader("Runtime/Shaders/ShaderKeywordSet.h")]
	[UsedByNativeCode]
	public enum ShaderKeywordType
	{
		// Token: 0x0400075C RID: 1884
		None,
		// Token: 0x0400075D RID: 1885
		BuiltinDefault = 2,
		// Token: 0x0400075E RID: 1886
		BuiltinExtra = 6,
		// Token: 0x0400075F RID: 1887
		BuiltinAutoStripped = 10,
		// Token: 0x04000760 RID: 1888
		UserDefined = 16
	}
}
