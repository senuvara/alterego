﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x020000FB RID: 251
	[UsedByNativeCode]
	public enum GraphicsDeviceType
	{
		// Token: 0x04000464 RID: 1124
		[Obsolete("OpenGL2 is no longer supported in Unity 5.5+")]
		OpenGL2,
		// Token: 0x04000465 RID: 1125
		[Obsolete("Direct3D 9 is no longer supported in Unity 2017.2+")]
		Direct3D9,
		// Token: 0x04000466 RID: 1126
		Direct3D11,
		// Token: 0x04000467 RID: 1127
		[Obsolete("PS3 is no longer supported in Unity 5.5+")]
		PlayStation3,
		// Token: 0x04000468 RID: 1128
		Null,
		// Token: 0x04000469 RID: 1129
		[Obsolete("Xbox360 is no longer supported in Unity 5.5+")]
		Xbox360 = 6,
		// Token: 0x0400046A RID: 1130
		OpenGLES2 = 8,
		// Token: 0x0400046B RID: 1131
		OpenGLES3 = 11,
		// Token: 0x0400046C RID: 1132
		[Obsolete("PVita is no longer supported as of Unity 2018")]
		PlayStationVita,
		// Token: 0x0400046D RID: 1133
		PlayStation4,
		// Token: 0x0400046E RID: 1134
		XboxOne,
		// Token: 0x0400046F RID: 1135
		[Obsolete("PlayStationMobile is no longer supported in Unity 5.3+")]
		PlayStationMobile,
		// Token: 0x04000470 RID: 1136
		Metal,
		// Token: 0x04000471 RID: 1137
		OpenGLCore,
		// Token: 0x04000472 RID: 1138
		Direct3D12,
		// Token: 0x04000473 RID: 1139
		[Obsolete("Nintendo 3DS support is unavailable since 2018.1")]
		N3DS,
		// Token: 0x04000474 RID: 1140
		Vulkan = 21,
		// Token: 0x04000475 RID: 1141
		Switch,
		// Token: 0x04000476 RID: 1142
		XboxOneD3D12
	}
}
