﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200010D RID: 269
	public enum CameraHDRMode
	{
		// Token: 0x040004E5 RID: 1253
		FP16 = 1,
		// Token: 0x040004E6 RID: 1254
		R11G11B10
	}
}
