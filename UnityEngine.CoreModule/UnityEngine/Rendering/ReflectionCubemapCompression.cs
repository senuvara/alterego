﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F2 RID: 242
	public enum ReflectionCubemapCompression
	{
		// Token: 0x040003FC RID: 1020
		Uncompressed,
		// Token: 0x040003FD RID: 1021
		Compressed,
		// Token: 0x040003FE RID: 1022
		Auto
	}
}
