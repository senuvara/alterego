﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000FC RID: 252
	public enum GraphicsTier
	{
		// Token: 0x04000478 RID: 1144
		Tier1,
		// Token: 0x04000479 RID: 1145
		Tier2,
		// Token: 0x0400047A RID: 1146
		Tier3
	}
}
