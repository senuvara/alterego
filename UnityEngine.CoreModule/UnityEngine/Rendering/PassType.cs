﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F8 RID: 248
	public enum PassType
	{
		// Token: 0x0400044A RID: 1098
		Normal,
		// Token: 0x0400044B RID: 1099
		Vertex,
		// Token: 0x0400044C RID: 1100
		VertexLM,
		// Token: 0x0400044D RID: 1101
		[Obsolete("VertexLMRGBM PassType is obsolete. Please use VertexLM PassType together with DecodeLightmap shader function.")]
		VertexLMRGBM,
		// Token: 0x0400044E RID: 1102
		ForwardBase,
		// Token: 0x0400044F RID: 1103
		ForwardAdd,
		// Token: 0x04000450 RID: 1104
		LightPrePassBase,
		// Token: 0x04000451 RID: 1105
		LightPrePassFinal,
		// Token: 0x04000452 RID: 1106
		ShadowCaster,
		// Token: 0x04000453 RID: 1107
		Deferred = 10,
		// Token: 0x04000454 RID: 1108
		Meta,
		// Token: 0x04000455 RID: 1109
		MotionVectors,
		// Token: 0x04000456 RID: 1110
		ScriptableRenderPipeline,
		// Token: 0x04000457 RID: 1111
		ScriptableRenderPipelineDefaultUnlit
	}
}
