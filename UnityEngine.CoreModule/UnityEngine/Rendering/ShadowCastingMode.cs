﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F9 RID: 249
	public enum ShadowCastingMode
	{
		// Token: 0x04000459 RID: 1113
		Off,
		// Token: 0x0400045A RID: 1114
		On,
		// Token: 0x0400045B RID: 1115
		TwoSided,
		// Token: 0x0400045C RID: 1116
		ShadowsOnly
	}
}
