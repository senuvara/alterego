﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000E8 RID: 232
	public enum RenderBufferLoadAction
	{
		// Token: 0x0400039D RID: 925
		Load,
		// Token: 0x0400039E RID: 926
		Clear,
		// Token: 0x0400039F RID: 927
		DontCare
	}
}
