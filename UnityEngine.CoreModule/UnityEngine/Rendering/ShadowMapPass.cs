﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F6 RID: 246
	[Flags]
	public enum ShadowMapPass
	{
		// Token: 0x04000423 RID: 1059
		PointlightPositiveX = 1,
		// Token: 0x04000424 RID: 1060
		PointlightNegativeX = 2,
		// Token: 0x04000425 RID: 1061
		PointlightPositiveY = 4,
		// Token: 0x04000426 RID: 1062
		PointlightNegativeY = 8,
		// Token: 0x04000427 RID: 1063
		PointlightPositiveZ = 16,
		// Token: 0x04000428 RID: 1064
		PointlightNegativeZ = 32,
		// Token: 0x04000429 RID: 1065
		DirectionalCascade0 = 64,
		// Token: 0x0400042A RID: 1066
		DirectionalCascade1 = 128,
		// Token: 0x0400042B RID: 1067
		DirectionalCascade2 = 256,
		// Token: 0x0400042C RID: 1068
		DirectionalCascade3 = 512,
		// Token: 0x0400042D RID: 1069
		Spotlight = 1024,
		// Token: 0x0400042E RID: 1070
		Pointlight = 63,
		// Token: 0x0400042F RID: 1071
		Directional = 960,
		// Token: 0x04000430 RID: 1072
		All = 2047
	}
}
