﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000109 RID: 265
	public enum BuiltinShaderMode
	{
		// Token: 0x040004B4 RID: 1204
		Disabled,
		// Token: 0x040004B5 RID: 1205
		UseBuiltin,
		// Token: 0x040004B6 RID: 1206
		UseCustom
	}
}
