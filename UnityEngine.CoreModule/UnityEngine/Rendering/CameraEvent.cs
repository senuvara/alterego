﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F3 RID: 243
	public enum CameraEvent
	{
		// Token: 0x04000400 RID: 1024
		BeforeDepthTexture,
		// Token: 0x04000401 RID: 1025
		AfterDepthTexture,
		// Token: 0x04000402 RID: 1026
		BeforeDepthNormalsTexture,
		// Token: 0x04000403 RID: 1027
		AfterDepthNormalsTexture,
		// Token: 0x04000404 RID: 1028
		BeforeGBuffer,
		// Token: 0x04000405 RID: 1029
		AfterGBuffer,
		// Token: 0x04000406 RID: 1030
		BeforeLighting,
		// Token: 0x04000407 RID: 1031
		AfterLighting,
		// Token: 0x04000408 RID: 1032
		BeforeFinalPass,
		// Token: 0x04000409 RID: 1033
		AfterFinalPass,
		// Token: 0x0400040A RID: 1034
		BeforeForwardOpaque,
		// Token: 0x0400040B RID: 1035
		AfterForwardOpaque,
		// Token: 0x0400040C RID: 1036
		BeforeImageEffectsOpaque,
		// Token: 0x0400040D RID: 1037
		AfterImageEffectsOpaque,
		// Token: 0x0400040E RID: 1038
		BeforeSkybox,
		// Token: 0x0400040F RID: 1039
		AfterSkybox,
		// Token: 0x04000410 RID: 1040
		BeforeForwardAlpha,
		// Token: 0x04000411 RID: 1041
		AfterForwardAlpha,
		// Token: 0x04000412 RID: 1042
		BeforeImageEffects,
		// Token: 0x04000413 RID: 1043
		AfterImageEffects,
		// Token: 0x04000414 RID: 1044
		AfterEverything,
		// Token: 0x04000415 RID: 1045
		BeforeReflections,
		// Token: 0x04000416 RID: 1046
		AfterReflections,
		// Token: 0x04000417 RID: 1047
		BeforeHaloAndLensFlares,
		// Token: 0x04000418 RID: 1048
		AfterHaloAndLensFlares
	}
}
