﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x02000074 RID: 116
	[UsedByNativeCode]
	[NativeHeader("Runtime/Graphics/GPUFence.h")]
	public struct GPUFence
	{
		// Token: 0x170000FD RID: 253
		// (get) Token: 0x0600059E RID: 1438 RVA: 0x0000D65C File Offset: 0x0000B85C
		public bool passed
		{
			get
			{
				this.Validate();
				if (!SystemInfo.supportsGPUFence)
				{
					throw new NotSupportedException("Cannot determine if this GPUFence has passed as this platform has not implemented GPUFences.");
				}
				return !this.IsFencePending() || GPUFence.HasFencePassed_Internal(this.m_Ptr);
			}
		}

		// Token: 0x0600059F RID: 1439
		[FreeFunction("GPUFenceInternals::HasFencePassed_Internal")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool HasFencePassed_Internal(IntPtr fencePtr);

		// Token: 0x060005A0 RID: 1440 RVA: 0x0000D6AC File Offset: 0x0000B8AC
		internal void InitPostAllocation()
		{
			if (this.m_Ptr == IntPtr.Zero)
			{
				if (SystemInfo.supportsGPUFence)
				{
					throw new NullReferenceException("The internal fence ptr is null, this should not be possible for fences that have been correctly constructed using Graphics.CreateGPUFence() or CommandBuffer.CreateGPUFence()");
				}
				this.m_Version = this.GetPlatformNotSupportedVersion();
			}
			else
			{
				this.m_Version = GPUFence.GetVersionNumber(this.m_Ptr);
			}
		}

		// Token: 0x060005A1 RID: 1441 RVA: 0x0000D708 File Offset: 0x0000B908
		internal bool IsFencePending()
		{
			return !(this.m_Ptr == IntPtr.Zero) && this.m_Version == GPUFence.GetVersionNumber(this.m_Ptr);
		}

		// Token: 0x060005A2 RID: 1442 RVA: 0x0000D74C File Offset: 0x0000B94C
		internal void Validate()
		{
			if (this.m_Version == 0 || (SystemInfo.supportsGPUFence && this.m_Version == this.GetPlatformNotSupportedVersion()))
			{
				throw new InvalidOperationException("This GPUFence object has not been correctly constructed see Graphics.CreateGPUFence() or CommandBuffer.CreateGPUFence()");
			}
		}

		// Token: 0x060005A3 RID: 1443 RVA: 0x0000D780 File Offset: 0x0000B980
		private int GetPlatformNotSupportedVersion()
		{
			return -1;
		}

		// Token: 0x060005A4 RID: 1444
		[FreeFunction("GPUFenceInternals::GetVersionNumber")]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetVersionNumber(IntPtr fencePtr);

		// Token: 0x0400014D RID: 333
		internal IntPtr m_Ptr;

		// Token: 0x0400014E RID: 334
		internal int m_Version;
	}
}
