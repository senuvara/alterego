﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000E6 RID: 230
	public enum OpaqueSortMode
	{
		// Token: 0x04000392 RID: 914
		Default,
		// Token: 0x04000393 RID: 915
		FrontToBack,
		// Token: 0x04000394 RID: 916
		NoDistanceSort
	}
}
