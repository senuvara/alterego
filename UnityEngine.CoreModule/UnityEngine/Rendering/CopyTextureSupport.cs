﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200010C RID: 268
	[Flags]
	public enum CopyTextureSupport
	{
		// Token: 0x040004DE RID: 1246
		None = 0,
		// Token: 0x040004DF RID: 1247
		Basic = 1,
		// Token: 0x040004E0 RID: 1248
		Copy3D = 2,
		// Token: 0x040004E1 RID: 1249
		DifferentTypes = 4,
		// Token: 0x040004E2 RID: 1250
		TextureToRT = 8,
		// Token: 0x040004E3 RID: 1251
		RTToTexture = 16
	}
}
