﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000101 RID: 257
	public enum ReflectionProbeClearFlags
	{
		// Token: 0x04000491 RID: 1169
		Skybox = 1,
		// Token: 0x04000492 RID: 1170
		SolidColor
	}
}
