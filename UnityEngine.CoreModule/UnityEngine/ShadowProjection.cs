﻿using System;

namespace UnityEngine
{
	// Token: 0x020000BB RID: 187
	public enum ShadowProjection
	{
		// Token: 0x040001EF RID: 495
		CloseFit,
		// Token: 0x040001F0 RID: 496
		StableFit
	}
}
