﻿using System;

namespace UnityEngine
{
	// Token: 0x02000034 RID: 52
	public enum AudioType
	{
		// Token: 0x04000070 RID: 112
		UNKNOWN,
		// Token: 0x04000071 RID: 113
		ACC,
		// Token: 0x04000072 RID: 114
		AIFF,
		// Token: 0x04000073 RID: 115
		IT = 10,
		// Token: 0x04000074 RID: 116
		MOD = 12,
		// Token: 0x04000075 RID: 117
		MPEG,
		// Token: 0x04000076 RID: 118
		OGGVORBIS,
		// Token: 0x04000077 RID: 119
		S3M = 17,
		// Token: 0x04000078 RID: 120
		WAV = 20,
		// Token: 0x04000079 RID: 121
		XM,
		// Token: 0x0400007A RID: 122
		XMA,
		// Token: 0x0400007B RID: 123
		VAG,
		// Token: 0x0400007C RID: 124
		AUDIOQUEUE
	}
}
