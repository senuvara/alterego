﻿using System;

namespace UnityEngine
{
	// Token: 0x020001EB RID: 491
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class RangeAttribute : PropertyAttribute
	{
		// Token: 0x06000F83 RID: 3971 RVA: 0x0001AD2A File Offset: 0x00018F2A
		public RangeAttribute(float min, float max)
		{
			this.min = min;
			this.max = max;
		}

		// Token: 0x04000722 RID: 1826
		public readonly float min;

		// Token: 0x04000723 RID: 1827
		public readonly float max;
	}
}
