﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using UnityEngine.Assertions.Comparers;

namespace UnityEngine.Assertions
{
	// Token: 0x02000260 RID: 608
	[DebuggerStepThrough]
	public static class Assert
	{
		// Token: 0x06001630 RID: 5680 RVA: 0x00027AFC File Offset: 0x00025CFC
		private static void Fail(string message, string userMessage)
		{
			if (Debugger.IsAttached)
			{
				throw new AssertionException(message, userMessage);
			}
			if (Assert.raiseExceptions)
			{
				throw new AssertionException(message, userMessage);
			}
			if (message == null)
			{
				message = "Assertion has failed\n";
			}
			if (userMessage != null)
			{
				message = userMessage + '\n' + message;
			}
			Debug.LogAssertion(message);
		}

		// Token: 0x06001631 RID: 5681 RVA: 0x00027B57 File Offset: 0x00025D57
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Assert.Equals should not be used for Assertions", true)]
		public new static bool Equals(object obj1, object obj2)
		{
			throw new InvalidOperationException("Assert.Equals should not be used for Assertions");
		}

		// Token: 0x06001632 RID: 5682 RVA: 0x00027B64 File Offset: 0x00025D64
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Assert.ReferenceEquals should not be used for Assertions", true)]
		public new static bool ReferenceEquals(object obj1, object obj2)
		{
			throw new InvalidOperationException("Assert.ReferenceEquals should not be used for Assertions");
		}

		// Token: 0x06001633 RID: 5683 RVA: 0x00027B71 File Offset: 0x00025D71
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsTrue(bool condition)
		{
			if (!condition)
			{
				Assert.IsTrue(condition, null);
			}
		}

		// Token: 0x06001634 RID: 5684 RVA: 0x00027B81 File Offset: 0x00025D81
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsTrue(bool condition, string message)
		{
			if (!condition)
			{
				Assert.Fail(AssertionMessageUtil.BooleanFailureMessage(true), message);
			}
		}

		// Token: 0x06001635 RID: 5685 RVA: 0x00027B96 File Offset: 0x00025D96
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsFalse(bool condition)
		{
			if (condition)
			{
				Assert.IsFalse(condition, null);
			}
		}

		// Token: 0x06001636 RID: 5686 RVA: 0x00027BA6 File Offset: 0x00025DA6
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsFalse(bool condition, string message)
		{
			if (condition)
			{
				Assert.Fail(AssertionMessageUtil.BooleanFailureMessage(false), message);
			}
		}

		// Token: 0x06001637 RID: 5687 RVA: 0x00027BBB File Offset: 0x00025DBB
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreApproximatelyEqual(float expected, float actual)
		{
			Assert.AreEqual<float>(expected, actual, null, FloatComparer.s_ComparerWithDefaultTolerance);
		}

		// Token: 0x06001638 RID: 5688 RVA: 0x00027BCB File Offset: 0x00025DCB
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreApproximatelyEqual(float expected, float actual, string message)
		{
			Assert.AreEqual<float>(expected, actual, message, FloatComparer.s_ComparerWithDefaultTolerance);
		}

		// Token: 0x06001639 RID: 5689 RVA: 0x00027BDB File Offset: 0x00025DDB
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreApproximatelyEqual(float expected, float actual, float tolerance)
		{
			Assert.AreApproximatelyEqual(expected, actual, tolerance, null);
		}

		// Token: 0x0600163A RID: 5690 RVA: 0x00027BE7 File Offset: 0x00025DE7
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreApproximatelyEqual(float expected, float actual, float tolerance, string message)
		{
			Assert.AreEqual<float>(expected, actual, message, new FloatComparer(tolerance));
		}

		// Token: 0x0600163B RID: 5691 RVA: 0x00027BF8 File Offset: 0x00025DF8
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotApproximatelyEqual(float expected, float actual)
		{
			Assert.AreNotEqual<float>(expected, actual, null, FloatComparer.s_ComparerWithDefaultTolerance);
		}

		// Token: 0x0600163C RID: 5692 RVA: 0x00027C08 File Offset: 0x00025E08
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotApproximatelyEqual(float expected, float actual, string message)
		{
			Assert.AreNotEqual<float>(expected, actual, message, FloatComparer.s_ComparerWithDefaultTolerance);
		}

		// Token: 0x0600163D RID: 5693 RVA: 0x00027C18 File Offset: 0x00025E18
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotApproximatelyEqual(float expected, float actual, float tolerance)
		{
			Assert.AreNotApproximatelyEqual(expected, actual, tolerance, null);
		}

		// Token: 0x0600163E RID: 5694 RVA: 0x00027C24 File Offset: 0x00025E24
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotApproximatelyEqual(float expected, float actual, float tolerance, string message)
		{
			Assert.AreNotEqual<float>(expected, actual, message, new FloatComparer(tolerance));
		}

		// Token: 0x0600163F RID: 5695 RVA: 0x00027C35 File Offset: 0x00025E35
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual<T>(T expected, T actual)
		{
			Assert.AreEqual<T>(expected, actual, null);
		}

		// Token: 0x06001640 RID: 5696 RVA: 0x00027C40 File Offset: 0x00025E40
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual<T>(T expected, T actual, string message)
		{
			Assert.AreEqual<T>(expected, actual, message, EqualityComparer<T>.Default);
		}

		// Token: 0x06001641 RID: 5697 RVA: 0x00027C50 File Offset: 0x00025E50
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual<T>(T expected, T actual, string message, IEqualityComparer<T> comparer)
		{
			if (typeof(Object).IsAssignableFrom(typeof(T)))
			{
				Assert.AreEqual(expected as Object, actual as Object, message);
			}
			else if (!comparer.Equals(actual, expected))
			{
				Assert.Fail(AssertionMessageUtil.GetEqualityMessage(actual, expected, true), message);
			}
		}

		// Token: 0x06001642 RID: 5698 RVA: 0x00027CC3 File Offset: 0x00025EC3
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(Object expected, Object actual, string message)
		{
			if (actual != expected)
			{
				Assert.Fail(AssertionMessageUtil.GetEqualityMessage(actual, expected, true), message);
			}
		}

		// Token: 0x06001643 RID: 5699 RVA: 0x00027CE0 File Offset: 0x00025EE0
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual<T>(T expected, T actual)
		{
			Assert.AreNotEqual<T>(expected, actual, null);
		}

		// Token: 0x06001644 RID: 5700 RVA: 0x00027CEB File Offset: 0x00025EEB
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual<T>(T expected, T actual, string message)
		{
			Assert.AreNotEqual<T>(expected, actual, message, EqualityComparer<T>.Default);
		}

		// Token: 0x06001645 RID: 5701 RVA: 0x00027CFC File Offset: 0x00025EFC
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual<T>(T expected, T actual, string message, IEqualityComparer<T> comparer)
		{
			if (typeof(Object).IsAssignableFrom(typeof(T)))
			{
				Assert.AreNotEqual(expected as Object, actual as Object, message);
			}
			else if (comparer.Equals(actual, expected))
			{
				Assert.Fail(AssertionMessageUtil.GetEqualityMessage(actual, expected, false), message);
			}
		}

		// Token: 0x06001646 RID: 5702 RVA: 0x00027D6F File Offset: 0x00025F6F
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(Object expected, Object actual, string message)
		{
			if (actual == expected)
			{
				Assert.Fail(AssertionMessageUtil.GetEqualityMessage(actual, expected, false), message);
			}
		}

		// Token: 0x06001647 RID: 5703 RVA: 0x00027D8C File Offset: 0x00025F8C
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNull<T>(T value) where T : class
		{
			Assert.IsNull<T>(value, null);
		}

		// Token: 0x06001648 RID: 5704 RVA: 0x00027D98 File Offset: 0x00025F98
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNull<T>(T value, string message) where T : class
		{
			if (typeof(Object).IsAssignableFrom(typeof(T)))
			{
				Assert.IsNull(value as Object, message);
			}
			else if (value != null)
			{
				Assert.Fail(AssertionMessageUtil.NullFailureMessage(value, true), message);
			}
		}

		// Token: 0x06001649 RID: 5705 RVA: 0x00027DFB File Offset: 0x00025FFB
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNull(Object value, string message)
		{
			if (value != null)
			{
				Assert.Fail(AssertionMessageUtil.NullFailureMessage(value, true), message);
			}
		}

		// Token: 0x0600164A RID: 5706 RVA: 0x00027E17 File Offset: 0x00026017
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNotNull<T>(T value) where T : class
		{
			Assert.IsNotNull<T>(value, null);
		}

		// Token: 0x0600164B RID: 5707 RVA: 0x00027E24 File Offset: 0x00026024
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNotNull<T>(T value, string message) where T : class
		{
			if (typeof(Object).IsAssignableFrom(typeof(T)))
			{
				Assert.IsNotNull(value as Object, message);
			}
			else if (value == null)
			{
				Assert.Fail(AssertionMessageUtil.NullFailureMessage(value, false), message);
			}
		}

		// Token: 0x0600164C RID: 5708 RVA: 0x00027E87 File Offset: 0x00026087
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNotNull(Object value, string message)
		{
			if (value == null)
			{
				Assert.Fail(AssertionMessageUtil.NullFailureMessage(value, false), message);
			}
		}

		// Token: 0x0600164D RID: 5709 RVA: 0x00027EA3 File Offset: 0x000260A3
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(sbyte expected, sbyte actual)
		{
			if ((int)expected != (int)actual)
			{
				Assert.AreEqual<sbyte>(expected, actual, null);
			}
		}

		// Token: 0x0600164E RID: 5710 RVA: 0x00027EB7 File Offset: 0x000260B7
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(sbyte expected, sbyte actual, string message)
		{
			if ((int)expected != (int)actual)
			{
				Assert.AreEqual<sbyte>(expected, actual, message);
			}
		}

		// Token: 0x0600164F RID: 5711 RVA: 0x00027ECB File Offset: 0x000260CB
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(sbyte expected, sbyte actual)
		{
			if ((int)expected == (int)actual)
			{
				Assert.AreNotEqual<sbyte>(expected, actual, null);
			}
		}

		// Token: 0x06001650 RID: 5712 RVA: 0x00027EDF File Offset: 0x000260DF
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(sbyte expected, sbyte actual, string message)
		{
			if ((int)expected == (int)actual)
			{
				Assert.AreNotEqual<sbyte>(expected, actual, message);
			}
		}

		// Token: 0x06001651 RID: 5713 RVA: 0x00027EF3 File Offset: 0x000260F3
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(byte expected, byte actual)
		{
			if (expected != actual)
			{
				Assert.AreEqual<byte>(expected, actual, null);
			}
		}

		// Token: 0x06001652 RID: 5714 RVA: 0x00027F05 File Offset: 0x00026105
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(byte expected, byte actual, string message)
		{
			if (expected != actual)
			{
				Assert.AreEqual<byte>(expected, actual, message);
			}
		}

		// Token: 0x06001653 RID: 5715 RVA: 0x00027F17 File Offset: 0x00026117
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(byte expected, byte actual)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<byte>(expected, actual, null);
			}
		}

		// Token: 0x06001654 RID: 5716 RVA: 0x00027F29 File Offset: 0x00026129
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(byte expected, byte actual, string message)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<byte>(expected, actual, message);
			}
		}

		// Token: 0x06001655 RID: 5717 RVA: 0x00027F3B File Offset: 0x0002613B
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(char expected, char actual)
		{
			if (expected != actual)
			{
				Assert.AreEqual<char>(expected, actual, null);
			}
		}

		// Token: 0x06001656 RID: 5718 RVA: 0x00027F4D File Offset: 0x0002614D
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(char expected, char actual, string message)
		{
			if (expected != actual)
			{
				Assert.AreEqual<char>(expected, actual, message);
			}
		}

		// Token: 0x06001657 RID: 5719 RVA: 0x00027F5F File Offset: 0x0002615F
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(char expected, char actual)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<char>(expected, actual, null);
			}
		}

		// Token: 0x06001658 RID: 5720 RVA: 0x00027F71 File Offset: 0x00026171
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(char expected, char actual, string message)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<char>(expected, actual, message);
			}
		}

		// Token: 0x06001659 RID: 5721 RVA: 0x00027F83 File Offset: 0x00026183
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(short expected, short actual)
		{
			if (expected != actual)
			{
				Assert.AreEqual<short>(expected, actual, null);
			}
		}

		// Token: 0x0600165A RID: 5722 RVA: 0x00027F95 File Offset: 0x00026195
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(short expected, short actual, string message)
		{
			if (expected != actual)
			{
				Assert.AreEqual<short>(expected, actual, message);
			}
		}

		// Token: 0x0600165B RID: 5723 RVA: 0x00027FA7 File Offset: 0x000261A7
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(short expected, short actual)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<short>(expected, actual, null);
			}
		}

		// Token: 0x0600165C RID: 5724 RVA: 0x00027FB9 File Offset: 0x000261B9
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(short expected, short actual, string message)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<short>(expected, actual, message);
			}
		}

		// Token: 0x0600165D RID: 5725 RVA: 0x00027FCB File Offset: 0x000261CB
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(ushort expected, ushort actual)
		{
			if (expected != actual)
			{
				Assert.AreEqual<ushort>(expected, actual, null);
			}
		}

		// Token: 0x0600165E RID: 5726 RVA: 0x00027FDD File Offset: 0x000261DD
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(ushort expected, ushort actual, string message)
		{
			if (expected != actual)
			{
				Assert.AreEqual<ushort>(expected, actual, message);
			}
		}

		// Token: 0x0600165F RID: 5727 RVA: 0x00027FEF File Offset: 0x000261EF
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(ushort expected, ushort actual)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<ushort>(expected, actual, null);
			}
		}

		// Token: 0x06001660 RID: 5728 RVA: 0x00028001 File Offset: 0x00026201
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(ushort expected, ushort actual, string message)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<ushort>(expected, actual, message);
			}
		}

		// Token: 0x06001661 RID: 5729 RVA: 0x00028013 File Offset: 0x00026213
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(int expected, int actual)
		{
			if (expected != actual)
			{
				Assert.AreEqual<int>(expected, actual, null);
			}
		}

		// Token: 0x06001662 RID: 5730 RVA: 0x00028025 File Offset: 0x00026225
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(int expected, int actual, string message)
		{
			if (expected != actual)
			{
				Assert.AreEqual<int>(expected, actual, message);
			}
		}

		// Token: 0x06001663 RID: 5731 RVA: 0x00028037 File Offset: 0x00026237
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(int expected, int actual)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<int>(expected, actual, null);
			}
		}

		// Token: 0x06001664 RID: 5732 RVA: 0x00028049 File Offset: 0x00026249
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(int expected, int actual, string message)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<int>(expected, actual, message);
			}
		}

		// Token: 0x06001665 RID: 5733 RVA: 0x0002805B File Offset: 0x0002625B
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(uint expected, uint actual)
		{
			if (expected != actual)
			{
				Assert.AreEqual<uint>(expected, actual, null);
			}
		}

		// Token: 0x06001666 RID: 5734 RVA: 0x0002806D File Offset: 0x0002626D
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(uint expected, uint actual, string message)
		{
			if (expected != actual)
			{
				Assert.AreEqual<uint>(expected, actual, message);
			}
		}

		// Token: 0x06001667 RID: 5735 RVA: 0x0002807F File Offset: 0x0002627F
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(uint expected, uint actual)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<uint>(expected, actual, null);
			}
		}

		// Token: 0x06001668 RID: 5736 RVA: 0x00028091 File Offset: 0x00026291
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(uint expected, uint actual, string message)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<uint>(expected, actual, message);
			}
		}

		// Token: 0x06001669 RID: 5737 RVA: 0x000280A3 File Offset: 0x000262A3
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(long expected, long actual)
		{
			if (expected != actual)
			{
				Assert.AreEqual<long>(expected, actual, null);
			}
		}

		// Token: 0x0600166A RID: 5738 RVA: 0x000280B5 File Offset: 0x000262B5
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(long expected, long actual, string message)
		{
			if (expected != actual)
			{
				Assert.AreEqual<long>(expected, actual, message);
			}
		}

		// Token: 0x0600166B RID: 5739 RVA: 0x000280C7 File Offset: 0x000262C7
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(long expected, long actual)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<long>(expected, actual, null);
			}
		}

		// Token: 0x0600166C RID: 5740 RVA: 0x000280D9 File Offset: 0x000262D9
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(long expected, long actual, string message)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<long>(expected, actual, message);
			}
		}

		// Token: 0x0600166D RID: 5741 RVA: 0x000280EB File Offset: 0x000262EB
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(ulong expected, ulong actual)
		{
			if (expected != actual)
			{
				Assert.AreEqual<ulong>(expected, actual, null);
			}
		}

		// Token: 0x0600166E RID: 5742 RVA: 0x000280FD File Offset: 0x000262FD
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(ulong expected, ulong actual, string message)
		{
			if (expected != actual)
			{
				Assert.AreEqual<ulong>(expected, actual, message);
			}
		}

		// Token: 0x0600166F RID: 5743 RVA: 0x0002810F File Offset: 0x0002630F
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(ulong expected, ulong actual)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<ulong>(expected, actual, null);
			}
		}

		// Token: 0x06001670 RID: 5744 RVA: 0x00028121 File Offset: 0x00026321
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(ulong expected, ulong actual, string message)
		{
			if (expected == actual)
			{
				Assert.AreNotEqual<ulong>(expected, actual, message);
			}
		}

		// Token: 0x06001671 RID: 5745 RVA: 0x00028133 File Offset: 0x00026333
		// Note: this type is marked as 'beforefieldinit'.
		static Assert()
		{
		}

		// Token: 0x0400083B RID: 2107
		internal const string UNITY_ASSERTIONS = "UNITY_ASSERTIONS";

		// Token: 0x0400083C RID: 2108
		public static bool raiseExceptions = false;
	}
}
