﻿using System;

namespace UnityEngine.Assertions
{
	// Token: 0x02000262 RID: 610
	internal class AssertionMessageUtil
	{
		// Token: 0x06001674 RID: 5748 RVA: 0x00002370 File Offset: 0x00000570
		public AssertionMessageUtil()
		{
		}

		// Token: 0x06001675 RID: 5749 RVA: 0x00028188 File Offset: 0x00026388
		public static string GetMessage(string failureMessage)
		{
			return UnityString.Format("{0} {1}", new object[]
			{
				"Assertion failure.",
				failureMessage
			});
		}

		// Token: 0x06001676 RID: 5750 RVA: 0x000281BC File Offset: 0x000263BC
		public static string GetMessage(string failureMessage, string expected)
		{
			return AssertionMessageUtil.GetMessage(UnityString.Format("{0}{1}{2} {3}", new object[]
			{
				failureMessage,
				Environment.NewLine,
				"Expected:",
				expected
			}));
		}

		// Token: 0x06001677 RID: 5751 RVA: 0x00028200 File Offset: 0x00026400
		public static string GetEqualityMessage(object actual, object expected, bool expectEqual)
		{
			return AssertionMessageUtil.GetMessage(UnityString.Format("Values are {0}equal.", new object[]
			{
				(!expectEqual) ? "" : "not "
			}), UnityString.Format("{0} {2} {1}", new object[]
			{
				actual,
				expected,
				(!expectEqual) ? "!=" : "=="
			}));
		}

		// Token: 0x06001678 RID: 5752 RVA: 0x00028274 File Offset: 0x00026474
		public static string NullFailureMessage(object value, bool expectNull)
		{
			return AssertionMessageUtil.GetMessage(UnityString.Format("Value was {0}Null", new object[]
			{
				(!expectNull) ? "" : "not "
			}), UnityString.Format("Value was {0}Null", new object[]
			{
				(!expectNull) ? "not " : ""
			}));
		}

		// Token: 0x06001679 RID: 5753 RVA: 0x000282E0 File Offset: 0x000264E0
		public static string BooleanFailureMessage(bool expected)
		{
			return AssertionMessageUtil.GetMessage("Value was " + !expected, expected.ToString());
		}

		// Token: 0x0400083E RID: 2110
		private const string k_Expected = "Expected:";

		// Token: 0x0400083F RID: 2111
		private const string k_AssertionFailed = "Assertion failure.";
	}
}
