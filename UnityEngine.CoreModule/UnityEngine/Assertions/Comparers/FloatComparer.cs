﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Assertions.Comparers
{
	// Token: 0x02000263 RID: 611
	public class FloatComparer : IEqualityComparer<float>
	{
		// Token: 0x0600167A RID: 5754 RVA: 0x0002831A File Offset: 0x0002651A
		public FloatComparer() : this(1E-05f, false)
		{
		}

		// Token: 0x0600167B RID: 5755 RVA: 0x00028329 File Offset: 0x00026529
		public FloatComparer(bool relative) : this(1E-05f, relative)
		{
		}

		// Token: 0x0600167C RID: 5756 RVA: 0x00028338 File Offset: 0x00026538
		public FloatComparer(float error) : this(error, false)
		{
		}

		// Token: 0x0600167D RID: 5757 RVA: 0x00028343 File Offset: 0x00026543
		public FloatComparer(float error, bool relative)
		{
			this.m_Error = error;
			this.m_Relative = relative;
		}

		// Token: 0x0600167E RID: 5758 RVA: 0x0002835C File Offset: 0x0002655C
		public bool Equals(float a, float b)
		{
			return (!this.m_Relative) ? FloatComparer.AreEqual(a, b, this.m_Error) : FloatComparer.AreEqualRelative(a, b, this.m_Error);
		}

		// Token: 0x0600167F RID: 5759 RVA: 0x0002839C File Offset: 0x0002659C
		public int GetHashCode(float obj)
		{
			return base.GetHashCode();
		}

		// Token: 0x06001680 RID: 5760 RVA: 0x000283B8 File Offset: 0x000265B8
		public static bool AreEqual(float expected, float actual, float error)
		{
			return Math.Abs(actual - expected) <= error;
		}

		// Token: 0x06001681 RID: 5761 RVA: 0x000283DC File Offset: 0x000265DC
		public static bool AreEqualRelative(float expected, float actual, float error)
		{
			bool result;
			if (expected == actual)
			{
				result = true;
			}
			else
			{
				float num = Math.Abs(expected);
				float num2 = Math.Abs(actual);
				float num3 = Math.Abs((actual - expected) / ((num <= num2) ? num2 : num));
				result = (num3 <= error);
			}
			return result;
		}

		// Token: 0x06001682 RID: 5762 RVA: 0x0002842C File Offset: 0x0002662C
		// Note: this type is marked as 'beforefieldinit'.
		static FloatComparer()
		{
		}

		// Token: 0x04000840 RID: 2112
		private readonly float m_Error;

		// Token: 0x04000841 RID: 2113
		private readonly bool m_Relative;

		// Token: 0x04000842 RID: 2114
		public static readonly FloatComparer s_ComparerWithDefaultTolerance = new FloatComparer(1E-05f);

		// Token: 0x04000843 RID: 2115
		public const float kEpsilon = 1E-05f;
	}
}
