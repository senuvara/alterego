﻿using System;

namespace UnityEngine.Assertions
{
	// Token: 0x02000261 RID: 609
	public class AssertionException : Exception
	{
		// Token: 0x06001672 RID: 5746 RVA: 0x0002813B File Offset: 0x0002633B
		public AssertionException(string message, string userMessage) : base(message)
		{
			this.m_UserMessage = userMessage;
		}

		// Token: 0x17000444 RID: 1092
		// (get) Token: 0x06001673 RID: 5747 RVA: 0x0002814C File Offset: 0x0002634C
		public override string Message
		{
			get
			{
				string text = base.Message;
				if (this.m_UserMessage != null)
				{
					text = text + '\n' + this.m_UserMessage;
				}
				return text;
			}
		}

		// Token: 0x0400083D RID: 2109
		private string m_UserMessage;
	}
}
