﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000055 RID: 85
	[NativeHeader("Runtime/Math/Color.h")]
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	[NativeClass("ColorRGBAf")]
	public struct Color : IEquatable<Color>
	{
		// Token: 0x06000470 RID: 1136 RVA: 0x0000AD23 File Offset: 0x00008F23
		public Color(float r, float g, float b, float a)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x0000AD43 File Offset: 0x00008F43
		public Color(float r, float g, float b)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = 1f;
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x0000AD68 File Offset: 0x00008F68
		public override string ToString()
		{
			return UnityString.Format("RGBA({0:F3}, {1:F3}, {2:F3}, {3:F3})", new object[]
			{
				this.r,
				this.g,
				this.b,
				this.a
			});
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x0000ADC8 File Offset: 0x00008FC8
		public string ToString(string format)
		{
			return UnityString.Format("RGBA({0}, {1}, {2}, {3})", new object[]
			{
				this.r.ToString(format),
				this.g.ToString(format),
				this.b.ToString(format),
				this.a.ToString(format)
			});
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x0000AE2C File Offset: 0x0000902C
		public override int GetHashCode()
		{
			return this.GetHashCode();
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x0000AE5C File Offset: 0x0000905C
		public override bool Equals(object other)
		{
			return other is Color && this.Equals((Color)other);
		}

		// Token: 0x06000476 RID: 1142 RVA: 0x0000AE90 File Offset: 0x00009090
		public bool Equals(Color other)
		{
			return this.r.Equals(other.r) && this.g.Equals(other.g) && this.b.Equals(other.b) && this.a.Equals(other.a);
		}

		// Token: 0x06000477 RID: 1143 RVA: 0x0000AF00 File Offset: 0x00009100
		public static Color operator +(Color a, Color b)
		{
			return new Color(a.r + b.r, a.g + b.g, a.b + b.b, a.a + b.a);
		}

		// Token: 0x06000478 RID: 1144 RVA: 0x0000AF58 File Offset: 0x00009158
		public static Color operator -(Color a, Color b)
		{
			return new Color(a.r - b.r, a.g - b.g, a.b - b.b, a.a - b.a);
		}

		// Token: 0x06000479 RID: 1145 RVA: 0x0000AFB0 File Offset: 0x000091B0
		public static Color operator *(Color a, Color b)
		{
			return new Color(a.r * b.r, a.g * b.g, a.b * b.b, a.a * b.a);
		}

		// Token: 0x0600047A RID: 1146 RVA: 0x0000B008 File Offset: 0x00009208
		public static Color operator *(Color a, float b)
		{
			return new Color(a.r * b, a.g * b, a.b * b, a.a * b);
		}

		// Token: 0x0600047B RID: 1147 RVA: 0x0000B048 File Offset: 0x00009248
		public static Color operator *(float b, Color a)
		{
			return new Color(a.r * b, a.g * b, a.b * b, a.a * b);
		}

		// Token: 0x0600047C RID: 1148 RVA: 0x0000B088 File Offset: 0x00009288
		public static Color operator /(Color a, float b)
		{
			return new Color(a.r / b, a.g / b, a.b / b, a.a / b);
		}

		// Token: 0x0600047D RID: 1149 RVA: 0x0000B0C8 File Offset: 0x000092C8
		public static bool operator ==(Color lhs, Color rhs)
		{
			return lhs == rhs;
		}

		// Token: 0x0600047E RID: 1150 RVA: 0x0000B0F0 File Offset: 0x000092F0
		public static bool operator !=(Color lhs, Color rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x0600047F RID: 1151 RVA: 0x0000B110 File Offset: 0x00009310
		public static Color Lerp(Color a, Color b, float t)
		{
			t = Mathf.Clamp01(t);
			return new Color(a.r + (b.r - a.r) * t, a.g + (b.g - a.g) * t, a.b + (b.b - a.b) * t, a.a + (b.a - a.a) * t);
		}

		// Token: 0x06000480 RID: 1152 RVA: 0x0000B198 File Offset: 0x00009398
		public static Color LerpUnclamped(Color a, Color b, float t)
		{
			return new Color(a.r + (b.r - a.r) * t, a.g + (b.g - a.g) * t, a.b + (b.b - a.b) * t, a.a + (b.a - a.a) * t);
		}

		// Token: 0x06000481 RID: 1153 RVA: 0x0000B218 File Offset: 0x00009418
		internal Color RGBMultiplied(float multiplier)
		{
			return new Color(this.r * multiplier, this.g * multiplier, this.b * multiplier, this.a);
		}

		// Token: 0x06000482 RID: 1154 RVA: 0x0000B250 File Offset: 0x00009450
		internal Color AlphaMultiplied(float multiplier)
		{
			return new Color(this.r, this.g, this.b, this.a * multiplier);
		}

		// Token: 0x06000483 RID: 1155 RVA: 0x0000B284 File Offset: 0x00009484
		internal Color RGBMultiplied(Color multiplier)
		{
			return new Color(this.r * multiplier.r, this.g * multiplier.g, this.b * multiplier.b, this.a);
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x06000484 RID: 1156 RVA: 0x0000B2D0 File Offset: 0x000094D0
		public static Color red
		{
			get
			{
				return new Color(1f, 0f, 0f, 1f);
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x06000485 RID: 1157 RVA: 0x0000B300 File Offset: 0x00009500
		public static Color green
		{
			get
			{
				return new Color(0f, 1f, 0f, 1f);
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x06000486 RID: 1158 RVA: 0x0000B330 File Offset: 0x00009530
		public static Color blue
		{
			get
			{
				return new Color(0f, 0f, 1f, 1f);
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x06000487 RID: 1159 RVA: 0x0000B360 File Offset: 0x00009560
		public static Color white
		{
			get
			{
				return new Color(1f, 1f, 1f, 1f);
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x06000488 RID: 1160 RVA: 0x0000B390 File Offset: 0x00009590
		public static Color black
		{
			get
			{
				return new Color(0f, 0f, 0f, 1f);
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x06000489 RID: 1161 RVA: 0x0000B3C0 File Offset: 0x000095C0
		public static Color yellow
		{
			get
			{
				return new Color(1f, 0.92156863f, 0.015686275f, 1f);
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x0600048A RID: 1162 RVA: 0x0000B3F0 File Offset: 0x000095F0
		public static Color cyan
		{
			get
			{
				return new Color(0f, 1f, 1f, 1f);
			}
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x0600048B RID: 1163 RVA: 0x0000B420 File Offset: 0x00009620
		public static Color magenta
		{
			get
			{
				return new Color(1f, 0f, 1f, 1f);
			}
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x0600048C RID: 1164 RVA: 0x0000B450 File Offset: 0x00009650
		public static Color gray
		{
			get
			{
				return new Color(0.5f, 0.5f, 0.5f, 1f);
			}
		}

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x0600048D RID: 1165 RVA: 0x0000B480 File Offset: 0x00009680
		public static Color grey
		{
			get
			{
				return new Color(0.5f, 0.5f, 0.5f, 1f);
			}
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x0600048E RID: 1166 RVA: 0x0000B4B0 File Offset: 0x000096B0
		public static Color clear
		{
			get
			{
				return new Color(0f, 0f, 0f, 0f);
			}
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x0600048F RID: 1167 RVA: 0x0000B4E0 File Offset: 0x000096E0
		public float grayscale
		{
			get
			{
				return 0.299f * this.r + 0.587f * this.g + 0.114f * this.b;
			}
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x06000490 RID: 1168 RVA: 0x0000B51C File Offset: 0x0000971C
		public Color linear
		{
			get
			{
				return new Color(Mathf.GammaToLinearSpace(this.r), Mathf.GammaToLinearSpace(this.g), Mathf.GammaToLinearSpace(this.b), this.a);
			}
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x06000491 RID: 1169 RVA: 0x0000B560 File Offset: 0x00009760
		public Color gamma
		{
			get
			{
				return new Color(Mathf.LinearToGammaSpace(this.r), Mathf.LinearToGammaSpace(this.g), Mathf.LinearToGammaSpace(this.b), this.a);
			}
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x06000492 RID: 1170 RVA: 0x0000B5A4 File Offset: 0x000097A4
		public float maxColorComponent
		{
			get
			{
				return Mathf.Max(Mathf.Max(this.r, this.g), this.b);
			}
		}

		// Token: 0x06000493 RID: 1171 RVA: 0x0000B5D8 File Offset: 0x000097D8
		public static implicit operator Vector4(Color c)
		{
			return new Vector4(c.r, c.g, c.b, c.a);
		}

		// Token: 0x06000494 RID: 1172 RVA: 0x0000B610 File Offset: 0x00009810
		public static implicit operator Color(Vector4 v)
		{
			return new Color(v.x, v.y, v.z, v.w);
		}

		// Token: 0x170000DB RID: 219
		public float this[int index]
		{
			get
			{
				float result;
				switch (index)
				{
				case 0:
					result = this.r;
					break;
				case 1:
					result = this.g;
					break;
				case 2:
					result = this.b;
					break;
				case 3:
					result = this.a;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector3 index!");
				}
				return result;
			}
			set
			{
				switch (index)
				{
				case 0:
					this.r = value;
					break;
				case 1:
					this.g = value;
					break;
				case 2:
					this.b = value;
					break;
				case 3:
					this.a = value;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector3 index!");
				}
			}
		}

		// Token: 0x06000497 RID: 1175 RVA: 0x0000B714 File Offset: 0x00009914
		public static void RGBToHSV(Color rgbColor, out float H, out float S, out float V)
		{
			if (rgbColor.b > rgbColor.g && rgbColor.b > rgbColor.r)
			{
				Color.RGBToHSVHelper(4f, rgbColor.b, rgbColor.r, rgbColor.g, out H, out S, out V);
			}
			else if (rgbColor.g > rgbColor.r)
			{
				Color.RGBToHSVHelper(2f, rgbColor.g, rgbColor.b, rgbColor.r, out H, out S, out V);
			}
			else
			{
				Color.RGBToHSVHelper(0f, rgbColor.r, rgbColor.g, rgbColor.b, out H, out S, out V);
			}
		}

		// Token: 0x06000498 RID: 1176 RVA: 0x0000B7CC File Offset: 0x000099CC
		private static void RGBToHSVHelper(float offset, float dominantcolor, float colorone, float colortwo, out float H, out float S, out float V)
		{
			V = dominantcolor;
			if (V != 0f)
			{
				float num;
				if (colorone > colortwo)
				{
					num = colortwo;
				}
				else
				{
					num = colorone;
				}
				float num2 = V - num;
				if (num2 != 0f)
				{
					S = num2 / V;
					H = offset + (colorone - colortwo) / num2;
				}
				else
				{
					S = 0f;
					H = offset + (colorone - colortwo);
				}
				H /= 6f;
				if (H < 0f)
				{
					H += 1f;
				}
			}
			else
			{
				S = 0f;
				H = 0f;
			}
		}

		// Token: 0x06000499 RID: 1177 RVA: 0x0000B87C File Offset: 0x00009A7C
		public static Color HSVToRGB(float H, float S, float V)
		{
			return Color.HSVToRGB(H, S, V, true);
		}

		// Token: 0x0600049A RID: 1178 RVA: 0x0000B89C File Offset: 0x00009A9C
		public static Color HSVToRGB(float H, float S, float V, bool hdr)
		{
			Color white = Color.white;
			if (S == 0f)
			{
				white.r = V;
				white.g = V;
				white.b = V;
			}
			else if (V == 0f)
			{
				white.r = 0f;
				white.g = 0f;
				white.b = 0f;
			}
			else
			{
				white.r = 0f;
				white.g = 0f;
				white.b = 0f;
				float num = H * 6f;
				int num2 = (int)Mathf.Floor(num);
				float num3 = num - (float)num2;
				float num4 = V * (1f - S);
				float num5 = V * (1f - S * num3);
				float num6 = V * (1f - S * (1f - num3));
				switch (num2 + 1)
				{
				case 0:
					white.r = V;
					white.g = num4;
					white.b = num5;
					break;
				case 1:
					white.r = V;
					white.g = num6;
					white.b = num4;
					break;
				case 2:
					white.r = num5;
					white.g = V;
					white.b = num4;
					break;
				case 3:
					white.r = num4;
					white.g = V;
					white.b = num6;
					break;
				case 4:
					white.r = num4;
					white.g = num5;
					white.b = V;
					break;
				case 5:
					white.r = num6;
					white.g = num4;
					white.b = V;
					break;
				case 6:
					white.r = V;
					white.g = num4;
					white.b = num5;
					break;
				case 7:
					white.r = V;
					white.g = num6;
					white.b = num4;
					break;
				}
				if (!hdr)
				{
					white.r = Mathf.Clamp(white.r, 0f, 1f);
					white.g = Mathf.Clamp(white.g, 0f, 1f);
					white.b = Mathf.Clamp(white.b, 0f, 1f);
				}
			}
			return white;
		}

		// Token: 0x0400010E RID: 270
		public float r;

		// Token: 0x0400010F RID: 271
		public float g;

		// Token: 0x04000110 RID: 272
		public float b;

		// Token: 0x04000111 RID: 273
		public float a;
	}
}
