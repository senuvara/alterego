﻿using System;

namespace UnityEngine
{
	// Token: 0x02000011 RID: 17
	public enum WeightedMode
	{
		// Token: 0x04000023 RID: 35
		None,
		// Token: 0x04000024 RID: 36
		In,
		// Token: 0x04000025 RID: 37
		Out,
		// Token: 0x04000026 RID: 38
		Both
	}
}
