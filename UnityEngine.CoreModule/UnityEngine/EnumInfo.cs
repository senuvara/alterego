﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200006D RID: 109
	internal class EnumInfo
	{
		// Token: 0x06000595 RID: 1429 RVA: 0x00002370 File Offset: 0x00000570
		public EnumInfo()
		{
		}

		// Token: 0x06000596 RID: 1430 RVA: 0x0000D578 File Offset: 0x0000B778
		[UsedByNativeCode]
		internal static EnumInfo CreateEnumInfoFromNativeEnum(string[] names, int[] values, string[] annotations, bool isFlags)
		{
			return new EnumInfo
			{
				names = names,
				values = values,
				annotations = annotations,
				isFlags = isFlags
			};
		}

		// Token: 0x04000143 RID: 323
		public string[] names;

		// Token: 0x04000144 RID: 324
		public int[] values;

		// Token: 0x04000145 RID: 325
		public string[] annotations;

		// Token: 0x04000146 RID: 326
		public bool isFlags;
	}
}
