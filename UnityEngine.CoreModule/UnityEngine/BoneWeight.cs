﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000153 RID: 339
	[UsedByNativeCode]
	[Serializable]
	public struct BoneWeight : IEquatable<BoneWeight>
	{
		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x06000EBE RID: 3774 RVA: 0x0001929C File Offset: 0x0001749C
		// (set) Token: 0x06000EBF RID: 3775 RVA: 0x000192B7 File Offset: 0x000174B7
		public float weight0
		{
			get
			{
				return this.m_Weight0;
			}
			set
			{
				this.m_Weight0 = value;
			}
		}

		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x06000EC0 RID: 3776 RVA: 0x000192C4 File Offset: 0x000174C4
		// (set) Token: 0x06000EC1 RID: 3777 RVA: 0x000192DF File Offset: 0x000174DF
		public float weight1
		{
			get
			{
				return this.m_Weight1;
			}
			set
			{
				this.m_Weight1 = value;
			}
		}

		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x06000EC2 RID: 3778 RVA: 0x000192EC File Offset: 0x000174EC
		// (set) Token: 0x06000EC3 RID: 3779 RVA: 0x00019307 File Offset: 0x00017507
		public float weight2
		{
			get
			{
				return this.m_Weight2;
			}
			set
			{
				this.m_Weight2 = value;
			}
		}

		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x06000EC4 RID: 3780 RVA: 0x00019314 File Offset: 0x00017514
		// (set) Token: 0x06000EC5 RID: 3781 RVA: 0x0001932F File Offset: 0x0001752F
		public float weight3
		{
			get
			{
				return this.m_Weight3;
			}
			set
			{
				this.m_Weight3 = value;
			}
		}

		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x06000EC6 RID: 3782 RVA: 0x0001933C File Offset: 0x0001753C
		// (set) Token: 0x06000EC7 RID: 3783 RVA: 0x00019357 File Offset: 0x00017557
		public int boneIndex0
		{
			get
			{
				return this.m_BoneIndex0;
			}
			set
			{
				this.m_BoneIndex0 = value;
			}
		}

		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x06000EC8 RID: 3784 RVA: 0x00019364 File Offset: 0x00017564
		// (set) Token: 0x06000EC9 RID: 3785 RVA: 0x0001937F File Offset: 0x0001757F
		public int boneIndex1
		{
			get
			{
				return this.m_BoneIndex1;
			}
			set
			{
				this.m_BoneIndex1 = value;
			}
		}

		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x06000ECA RID: 3786 RVA: 0x0001938C File Offset: 0x0001758C
		// (set) Token: 0x06000ECB RID: 3787 RVA: 0x000193A7 File Offset: 0x000175A7
		public int boneIndex2
		{
			get
			{
				return this.m_BoneIndex2;
			}
			set
			{
				this.m_BoneIndex2 = value;
			}
		}

		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06000ECC RID: 3788 RVA: 0x000193B4 File Offset: 0x000175B4
		// (set) Token: 0x06000ECD RID: 3789 RVA: 0x000193CF File Offset: 0x000175CF
		public int boneIndex3
		{
			get
			{
				return this.m_BoneIndex3;
			}
			set
			{
				this.m_BoneIndex3 = value;
			}
		}

		// Token: 0x06000ECE RID: 3790 RVA: 0x000193DC File Offset: 0x000175DC
		public override int GetHashCode()
		{
			return this.boneIndex0.GetHashCode() ^ this.boneIndex1.GetHashCode() << 2 ^ this.boneIndex2.GetHashCode() >> 2 ^ this.boneIndex3.GetHashCode() >> 1 ^ this.weight0.GetHashCode() << 5 ^ this.weight1.GetHashCode() << 4 ^ this.weight2.GetHashCode() >> 4 ^ this.weight3.GetHashCode() >> 3;
		}

		// Token: 0x06000ECF RID: 3791 RVA: 0x000194AC File Offset: 0x000176AC
		public override bool Equals(object other)
		{
			return other is BoneWeight && this.Equals((BoneWeight)other);
		}

		// Token: 0x06000ED0 RID: 3792 RVA: 0x000194DC File Offset: 0x000176DC
		public bool Equals(BoneWeight other)
		{
			bool result;
			if (this.boneIndex0.Equals(other.boneIndex0) && this.boneIndex1.Equals(other.boneIndex1) && this.boneIndex2.Equals(other.boneIndex2) && this.boneIndex3.Equals(other.boneIndex3))
			{
				Vector4 vector = new Vector4(this.weight0, this.weight1, this.weight2, this.weight3);
				result = vector.Equals(new Vector4(other.weight0, other.weight1, other.weight2, other.weight3));
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000ED1 RID: 3793 RVA: 0x000195A8 File Offset: 0x000177A8
		public static bool operator ==(BoneWeight lhs, BoneWeight rhs)
		{
			return lhs.boneIndex0 == rhs.boneIndex0 && lhs.boneIndex1 == rhs.boneIndex1 && lhs.boneIndex2 == rhs.boneIndex2 && lhs.boneIndex3 == rhs.boneIndex3 && new Vector4(lhs.weight0, lhs.weight1, lhs.weight2, lhs.weight3) == new Vector4(rhs.weight0, rhs.weight1, rhs.weight2, rhs.weight3);
		}

		// Token: 0x06000ED2 RID: 3794 RVA: 0x00019654 File Offset: 0x00017854
		public static bool operator !=(BoneWeight lhs, BoneWeight rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x040006F2 RID: 1778
		[SerializeField]
		private float m_Weight0;

		// Token: 0x040006F3 RID: 1779
		[SerializeField]
		private float m_Weight1;

		// Token: 0x040006F4 RID: 1780
		[SerializeField]
		private float m_Weight2;

		// Token: 0x040006F5 RID: 1781
		[SerializeField]
		private float m_Weight3;

		// Token: 0x040006F6 RID: 1782
		[SerializeField]
		private int m_BoneIndex0;

		// Token: 0x040006F7 RID: 1783
		[SerializeField]
		private int m_BoneIndex1;

		// Token: 0x040006F8 RID: 1784
		[SerializeField]
		private int m_BoneIndex2;

		// Token: 0x040006F9 RID: 1785
		[SerializeField]
		private int m_BoneIndex3;
	}
}
