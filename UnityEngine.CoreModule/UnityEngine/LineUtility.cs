﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000148 RID: 328
	[NativeHeader("Runtime/Export/LineUtility.bindings.h")]
	public sealed class LineUtility
	{
		// Token: 0x06000D0D RID: 3341 RVA: 0x00002370 File Offset: 0x00000570
		public LineUtility()
		{
		}

		// Token: 0x06000D0E RID: 3342 RVA: 0x00014482 File Offset: 0x00012682
		public static void Simplify(List<Vector3> points, float tolerance, List<int> pointsToKeep)
		{
			if (points == null)
			{
				throw new ArgumentNullException("points");
			}
			if (pointsToKeep == null)
			{
				throw new ArgumentNullException("pointsToKeep");
			}
			LineUtility.GeneratePointsToKeep3D(points, tolerance, pointsToKeep);
		}

		// Token: 0x06000D0F RID: 3343 RVA: 0x000144AF File Offset: 0x000126AF
		public static void Simplify(List<Vector3> points, float tolerance, List<Vector3> simplifiedPoints)
		{
			if (points == null)
			{
				throw new ArgumentNullException("points");
			}
			if (simplifiedPoints == null)
			{
				throw new ArgumentNullException("simplifiedPoints");
			}
			LineUtility.GenerateSimplifiedPoints3D(points, tolerance, simplifiedPoints);
		}

		// Token: 0x06000D10 RID: 3344 RVA: 0x000144DC File Offset: 0x000126DC
		public static void Simplify(List<Vector2> points, float tolerance, List<int> pointsToKeep)
		{
			if (points == null)
			{
				throw new ArgumentNullException("points");
			}
			if (pointsToKeep == null)
			{
				throw new ArgumentNullException("pointsToKeep");
			}
			LineUtility.GeneratePointsToKeep2D(points, tolerance, pointsToKeep);
		}

		// Token: 0x06000D11 RID: 3345 RVA: 0x00014509 File Offset: 0x00012709
		public static void Simplify(List<Vector2> points, float tolerance, List<Vector2> simplifiedPoints)
		{
			if (points == null)
			{
				throw new ArgumentNullException("points");
			}
			if (simplifiedPoints == null)
			{
				throw new ArgumentNullException("simplifiedPoints");
			}
			LineUtility.GenerateSimplifiedPoints2D(points, tolerance, simplifiedPoints);
		}

		// Token: 0x06000D12 RID: 3346
		[FreeFunction("LineUtility_Bindings::GeneratePointsToKeep3D", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void GeneratePointsToKeep3D(object pointsList, float tolerance, object pointsToKeepList);

		// Token: 0x06000D13 RID: 3347
		[FreeFunction("LineUtility_Bindings::GeneratePointsToKeep2D", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void GeneratePointsToKeep2D(object pointsList, float tolerance, object pointsToKeepList);

		// Token: 0x06000D14 RID: 3348
		[FreeFunction("LineUtility_Bindings::GenerateSimplifiedPoints3D", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void GenerateSimplifiedPoints3D(object pointsList, float tolerance, object simplifiedPoints);

		// Token: 0x06000D15 RID: 3349
		[FreeFunction("LineUtility_Bindings::GenerateSimplifiedPoints2D", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void GenerateSimplifiedPoints2D(object pointsList, float tolerance, object simplifiedPoints);
	}
}
