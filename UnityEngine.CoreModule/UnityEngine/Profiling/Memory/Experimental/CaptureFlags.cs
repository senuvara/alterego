﻿using System;

namespace UnityEngine.Profiling.Memory.Experimental
{
	// Token: 0x0200033E RID: 830
	[Flags]
	public enum CaptureFlags : uint
	{
		// Token: 0x04000A81 RID: 2689
		ManagedObjects = 1U,
		// Token: 0x04000A82 RID: 2690
		NativeObjects = 2U,
		// Token: 0x04000A83 RID: 2691
		NativeAllocations = 4U,
		// Token: 0x04000A84 RID: 2692
		NativeAllocationSites = 8U,
		// Token: 0x04000A85 RID: 2693
		NativeStackTraces = 16U
	}
}
