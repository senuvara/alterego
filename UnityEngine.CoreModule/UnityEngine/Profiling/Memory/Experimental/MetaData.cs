﻿using System;

namespace UnityEngine.Profiling.Memory.Experimental
{
	// Token: 0x0200033F RID: 831
	public class MetaData
	{
		// Token: 0x06001BCE RID: 7118 RVA: 0x00002370 File Offset: 0x00000570
		public MetaData()
		{
		}

		// Token: 0x04000A86 RID: 2694
		public string content;

		// Token: 0x04000A87 RID: 2695
		public string platform;

		// Token: 0x04000A88 RID: 2696
		public Texture2D screenshot;
	}
}
