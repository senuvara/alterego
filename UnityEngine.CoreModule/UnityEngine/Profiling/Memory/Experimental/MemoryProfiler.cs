﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Assertions;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Profiling.Memory.Experimental
{
	// Token: 0x02000340 RID: 832
	[NativeHeader("Modules/Profiler/Public/ProfilerConnection.h")]
	public sealed class MemoryProfiler
	{
		// Token: 0x06001BCF RID: 7119 RVA: 0x00002370 File Offset: 0x00000570
		public MemoryProfiler()
		{
		}

		// Token: 0x14000017 RID: 23
		// (add) Token: 0x06001BD0 RID: 7120 RVA: 0x00031118 File Offset: 0x0002F318
		// (remove) Token: 0x06001BD1 RID: 7121 RVA: 0x0003114C File Offset: 0x0002F34C
		private static event Action<string, bool> snapshotFinished
		{
			add
			{
				Action<string, bool> action = MemoryProfiler.snapshotFinished;
				Action<string, bool> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<string, bool>>(ref MemoryProfiler.snapshotFinished, (Action<string, bool>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<string, bool> action = MemoryProfiler.snapshotFinished;
				Action<string, bool> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<string, bool>>(ref MemoryProfiler.snapshotFinished, (Action<string, bool>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000018 RID: 24
		// (add) Token: 0x06001BD2 RID: 7122 RVA: 0x00031180 File Offset: 0x0002F380
		// (remove) Token: 0x06001BD3 RID: 7123 RVA: 0x000311B4 File Offset: 0x0002F3B4
		public static event Action<MetaData> createMetaData
		{
			add
			{
				Action<MetaData> action = MemoryProfiler.createMetaData;
				Action<MetaData> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<MetaData>>(ref MemoryProfiler.createMetaData, (Action<MetaData>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<MetaData> action = MemoryProfiler.createMetaData;
				Action<MetaData> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<MetaData>>(ref MemoryProfiler.createMetaData, (Action<MetaData>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x06001BD4 RID: 7124
		[NativeConditional("ENABLE_PLAYERCONNECTION")]
		[NativeMethod("TakeMemorySnapshot")]
		[StaticAccessor("ProfilerConnection::Get()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void TakeSnapshotInternal(string path, uint captureFlag);

		// Token: 0x06001BD5 RID: 7125 RVA: 0x000311E8 File Offset: 0x0002F3E8
		public static void TakeSnapshot(string path, Action<string, bool> finishCallback, CaptureFlags captureFlags = CaptureFlags.ManagedObjects | CaptureFlags.NativeObjects)
		{
			if (MemoryProfiler.snapshotFinished != null)
			{
				Debug.LogWarning("Canceling taking the snapshot. There is already ongoing capture.");
				finishCallback(path, false);
			}
			else
			{
				MemoryProfiler.snapshotFinished += finishCallback;
				MemoryProfiler.TakeSnapshotInternal(path, (uint)captureFlags);
			}
		}

		// Token: 0x06001BD6 RID: 7126 RVA: 0x00031220 File Offset: 0x0002F420
		public static void TakeTempSnapshot(Action<string, bool> finishCallback, CaptureFlags captureFlags = CaptureFlags.ManagedObjects | CaptureFlags.NativeObjects)
		{
			string[] array = Application.dataPath.Split(new char[]
			{
				'/'
			});
			string str = array[array.Length - 2];
			string path = Application.temporaryCachePath + "/" + str + ".snap";
			MemoryProfiler.TakeSnapshot(path, finishCallback, captureFlags);
		}

		// Token: 0x06001BD7 RID: 7127 RVA: 0x0003126C File Offset: 0x0002F46C
		[RequiredByNativeCode]
		private static byte[] PrepareMetadata()
		{
			byte[] result;
			if (MemoryProfiler.createMetaData == null)
			{
				result = new byte[0];
			}
			else
			{
				MetaData metaData = new MetaData();
				MemoryProfiler.createMetaData(metaData);
				if (metaData.content == null)
				{
					metaData.content = "";
				}
				if (metaData.platform == null)
				{
					metaData.platform = "";
				}
				int num = 2 * metaData.content.Length;
				int num2 = 2 * metaData.platform.Length;
				int num3 = num + num2 + 12;
				byte[] array = null;
				if (metaData.screenshot != null)
				{
					array = metaData.screenshot.GetRawTextureData();
					num3 += array.Length + 12;
				}
				byte[] array2 = new byte[num3];
				int num4 = 0;
				num4 = MemoryProfiler.WriteIntToByteArray(array2, num4, metaData.content.Length);
				num4 = MemoryProfiler.WriteStringToByteArray(array2, num4, metaData.content);
				num4 = MemoryProfiler.WriteIntToByteArray(array2, num4, metaData.platform.Length);
				num4 = MemoryProfiler.WriteStringToByteArray(array2, num4, metaData.platform);
				if (metaData.screenshot != null)
				{
					num4 = MemoryProfiler.WriteIntToByteArray(array2, num4, array.Length);
					Array.Copy(array, 0, array2, num4, array.Length);
					num4 += array.Length;
					num4 = MemoryProfiler.WriteIntToByteArray(array2, num4, metaData.screenshot.width);
					num4 = MemoryProfiler.WriteIntToByteArray(array2, num4, metaData.screenshot.height);
					num4 = MemoryProfiler.WriteIntToByteArray(array2, num4, (int)metaData.screenshot.format);
				}
				else
				{
					num4 = MemoryProfiler.WriteIntToByteArray(array2, num4, 0);
				}
				Assert.AreEqual(array2.Length, num4);
				result = array2;
			}
			return result;
		}

		// Token: 0x06001BD8 RID: 7128 RVA: 0x00031420 File Offset: 0x0002F620
		internal unsafe static int WriteIntToByteArray(byte[] array, int offset, int value)
		{
			array[offset++] = (byte)value;
			array[offset++] = *(ref value + 1);
			array[offset++] = *(ref value + 2);
			array[offset++] = *(ref value + 3);
			return offset;
		}

		// Token: 0x06001BD9 RID: 7129 RVA: 0x0003146C File Offset: 0x0002F66C
		internal unsafe static int WriteStringToByteArray(byte[] array, int offset, string value)
		{
			if (value.Length != 0)
			{
				fixed (string text = value)
				{
					char* ptr = text + RuntimeHelpers.OffsetToStringData / 2;
					char* ptr2 = ptr;
					char* ptr3 = ptr + value.Length;
					while (ptr2 != ptr3)
					{
						for (int i = 0; i < 2; i++)
						{
							array[offset++] = *(byte*)(ptr2 + i / 2);
						}
						ptr2++;
					}
				}
			}
			return offset;
		}

		// Token: 0x06001BDA RID: 7130 RVA: 0x000314E8 File Offset: 0x0002F6E8
		[RequiredByNativeCode]
		private static void FinalizeSnapshot(string path, bool result)
		{
			if (MemoryProfiler.snapshotFinished != null)
			{
				Action<string, bool> action = MemoryProfiler.snapshotFinished;
				MemoryProfiler.snapshotFinished = null;
				action(path, result);
			}
		}

		// Token: 0x04000A89 RID: 2697
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action<string, bool> snapshotFinished;

		// Token: 0x04000A8A RID: 2698
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action<MetaData> createMetaData;
	}
}
