﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.Profiling
{
	// Token: 0x02000342 RID: 834
	[MovedFrom("UnityEngine")]
	[NativeHeader("Runtime/ScriptingBackend/ScriptingApi.h")]
	[NativeHeader("Runtime/Utilities/MemoryUtilities.h")]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Profiler/ScriptBindings/Profiler.bindings.h")]
	[NativeHeader("Runtime/Profiler/Profiler.h")]
	[NativeHeader("Runtime/Allocator/MemoryManager.h")]
	public sealed class Profiler
	{
		// Token: 0x06001BDB RID: 7131 RVA: 0x00002050 File Offset: 0x00000250
		private Profiler()
		{
		}

		// Token: 0x17000519 RID: 1305
		// (get) Token: 0x06001BDC RID: 7132
		public static extern bool supported { [NativeMethod(Name = "profiler_is_available", IsFreeFunction = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700051A RID: 1306
		// (get) Token: 0x06001BDD RID: 7133
		// (set) Token: 0x06001BDE RID: 7134
		[StaticAccessor("ProfilerBindings", StaticAccessorType.DoubleColon)]
		public static extern string logFile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700051B RID: 1307
		// (get) Token: 0x06001BDF RID: 7135
		// (set) Token: 0x06001BE0 RID: 7136
		public static extern bool enableBinaryLog { [NativeMethod(Name = "ProfilerBindings::IsBinaryLogEnabled", IsFreeFunction = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod(Name = "ProfilerBindings::SetBinaryLogEnabled", IsFreeFunction = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700051C RID: 1308
		// (get) Token: 0x06001BE1 RID: 7137
		// (set) Token: 0x06001BE2 RID: 7138
		public static extern int maxUsedMemory { [NativeMethod(Name = "ProfilerBindings::GetMaxUsedMemory", IsFreeFunction = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod(Name = "ProfilerBindings::SetMaxUsedMemory", IsFreeFunction = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700051D RID: 1309
		// (get) Token: 0x06001BE3 RID: 7139
		// (set) Token: 0x06001BE4 RID: 7140
		public static extern bool enabled { [NativeMethod(Name = "profiler_is_enabled", IsFreeFunction = true)] [NativeConditional("ENABLE_PROFILER")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod(Name = "ProfilerBindings::SetProfilerEnabled", IsFreeFunction = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001BE5 RID: 7141
		[FreeFunction("profiler_set_area_enabled")]
		[Conditional("ENABLE_PROFILER")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetAreaEnabled(ProfilerArea area, bool enabled);

		// Token: 0x1700051E RID: 1310
		// (get) Token: 0x06001BE6 RID: 7142 RVA: 0x00031518 File Offset: 0x0002F718
		public static int areaCount
		{
			get
			{
				return Enum.GetNames(typeof(ProfilerArea)).Length;
			}
		}

		// Token: 0x06001BE7 RID: 7143
		[NativeConditional("ENABLE_PROFILER")]
		[FreeFunction("profiler_is_area_enabled")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetAreaEnabled(ProfilerArea area);

		// Token: 0x06001BE8 RID: 7144 RVA: 0x0003153E File Offset: 0x0002F73E
		[Conditional("UNITY_EDITOR")]
		public static void AddFramesFromFile(string file)
		{
			if (string.IsNullOrEmpty(file))
			{
				Debug.LogError("AddFramesFromFile: Invalid or empty path");
			}
			else
			{
				Profiler.AddFramesFromFile_Internal(file, true);
			}
		}

		// Token: 0x06001BE9 RID: 7145
		[StaticAccessor("profiling::GetProfilerSessionPtr()", StaticAccessorType.Arrow)]
		[NativeConditional("ENABLE_PROFILER && UNITY_EDITOR")]
		[NativeMethod(Name = "LoadFromFile")]
		[NativeHeader("Modules/ProfilerEditor/Public/ProfilerSession.h")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void AddFramesFromFile_Internal(string file, bool keepExistingFrames);

		// Token: 0x06001BEA RID: 7146 RVA: 0x00031563 File Offset: 0x0002F763
		[Conditional("ENABLE_PROFILER")]
		public static void BeginThreadProfiling(string threadGroupName, string threadName)
		{
			if (string.IsNullOrEmpty(threadGroupName))
			{
				throw new ArgumentException("Argument should be a valid string", "threadGroupName");
			}
			if (string.IsNullOrEmpty(threadName))
			{
				throw new ArgumentException("Argument should be a valid string", "threadName");
			}
			Profiler.BeginThreadProfilingInternal(threadGroupName, threadName);
		}

		// Token: 0x06001BEB RID: 7147
		[NativeConditional("ENABLE_PROFILER")]
		[NativeMethod(Name = "ProfilerBindings::BeginThreadProfiling", IsFreeFunction = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void BeginThreadProfilingInternal(string threadGroupName, string threadName);

		// Token: 0x06001BEC RID: 7148
		[NativeConditional("ENABLE_PROFILER")]
		[NativeMethod(Name = "ProfilerBindings::EndThreadProfiling", IsFreeFunction = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void EndThreadProfiling();

		// Token: 0x06001BED RID: 7149 RVA: 0x000315A3 File Offset: 0x0002F7A3
		[Conditional("ENABLE_PROFILER")]
		public static void BeginSample(string name)
		{
			Profiler.BeginSampleImpl(name, null);
		}

		// Token: 0x06001BEE RID: 7150 RVA: 0x000315AD File Offset: 0x0002F7AD
		[Conditional("ENABLE_PROFILER")]
		public static void BeginSample(string name, Object targetObject)
		{
			Profiler.BeginSampleImpl(name, targetObject);
		}

		// Token: 0x06001BEF RID: 7151
		[NativeMethod(Name = "ProfilerBindings::BeginSample", IsFreeFunction = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void BeginSampleImpl(string name, Object targetObject);

		// Token: 0x06001BF0 RID: 7152
		[Conditional("ENABLE_PROFILER")]
		[NativeMethod(Name = "ProfilerBindings::EndSample", IsFreeFunction = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void EndSample();

		// Token: 0x1700051F RID: 1311
		// (get) Token: 0x06001BF1 RID: 7153 RVA: 0x000315B8 File Offset: 0x0002F7B8
		// (set) Token: 0x06001BF2 RID: 7154 RVA: 0x00007476 File Offset: 0x00005676
		[Obsolete("maxNumberOfSamplesPerFrame has been depricated. Use maxUsedMemory instead")]
		public static int maxNumberOfSamplesPerFrame
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x17000520 RID: 1312
		// (get) Token: 0x06001BF3 RID: 7155 RVA: 0x000315D0 File Offset: 0x0002F7D0
		[Obsolete("usedHeapSize has been deprecated since it is limited to 4GB. Please use usedHeapSizeLong instead.")]
		public static uint usedHeapSize
		{
			get
			{
				return (uint)Profiler.usedHeapSizeLong;
			}
		}

		// Token: 0x17000521 RID: 1313
		// (get) Token: 0x06001BF4 RID: 7156
		public static extern long usedHeapSizeLong { [NativeMethod(Name = "GetUsedHeapSize", IsFreeFunction = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001BF5 RID: 7157 RVA: 0x000315EC File Offset: 0x0002F7EC
		[Obsolete("GetRuntimeMemorySize has been deprecated since it is limited to 2GB. Please use GetRuntimeMemorySizeLong() instead.")]
		public static int GetRuntimeMemorySize(Object o)
		{
			return (int)Profiler.GetRuntimeMemorySizeLong(o);
		}

		// Token: 0x06001BF6 RID: 7158
		[NativeMethod(Name = "ProfilerBindings::GetRuntimeMemorySizeLong", IsFreeFunction = true)]
		[NativeConditional("ENABLE_PROFILER")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetRuntimeMemorySizeLong(Object o);

		// Token: 0x06001BF7 RID: 7159 RVA: 0x00031608 File Offset: 0x0002F808
		[Obsolete("GetMonoHeapSize has been deprecated since it is limited to 4GB. Please use GetMonoHeapSizeLong() instead.")]
		public static uint GetMonoHeapSize()
		{
			return (uint)Profiler.GetMonoHeapSizeLong();
		}

		// Token: 0x06001BF8 RID: 7160
		[NativeMethod(Name = "scripting_gc_get_heap_size", IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetMonoHeapSizeLong();

		// Token: 0x06001BF9 RID: 7161 RVA: 0x00031624 File Offset: 0x0002F824
		[Obsolete("GetMonoUsedSize has been deprecated since it is limited to 4GB. Please use GetMonoUsedSizeLong() instead.")]
		public static uint GetMonoUsedSize()
		{
			return (uint)Profiler.GetMonoUsedSizeLong();
		}

		// Token: 0x06001BFA RID: 7162
		[NativeMethod(Name = "scripting_gc_get_used_size", IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetMonoUsedSizeLong();

		// Token: 0x06001BFB RID: 7163
		[NativeConditional("ENABLE_MEMORY_MANAGER")]
		[StaticAccessor("GetMemoryManager()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SetTempAllocatorRequestedSize(uint size);

		// Token: 0x06001BFC RID: 7164
		[NativeConditional("ENABLE_MEMORY_MANAGER")]
		[StaticAccessor("GetMemoryManager()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern uint GetTempAllocatorSize();

		// Token: 0x06001BFD RID: 7165 RVA: 0x00031640 File Offset: 0x0002F840
		[Obsolete("GetTotalAllocatedMemory has been deprecated since it is limited to 4GB. Please use GetTotalAllocatedMemoryLong() instead.")]
		public static uint GetTotalAllocatedMemory()
		{
			return (uint)Profiler.GetTotalAllocatedMemoryLong();
		}

		// Token: 0x06001BFE RID: 7166
		[NativeMethod(Name = "GetTotalAllocatedMemory")]
		[StaticAccessor("GetMemoryManager()", StaticAccessorType.Dot)]
		[NativeConditional("ENABLE_MEMORY_MANAGER")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetTotalAllocatedMemoryLong();

		// Token: 0x06001BFF RID: 7167 RVA: 0x0003165C File Offset: 0x0002F85C
		[Obsolete("GetTotalUnusedReservedMemory has been deprecated since it is limited to 4GB. Please use GetTotalUnusedReservedMemoryLong() instead.")]
		public static uint GetTotalUnusedReservedMemory()
		{
			return (uint)Profiler.GetTotalUnusedReservedMemoryLong();
		}

		// Token: 0x06001C00 RID: 7168
		[NativeMethod(Name = "GetTotalUnusedReservedMemory")]
		[NativeConditional("ENABLE_MEMORY_MANAGER")]
		[StaticAccessor("GetMemoryManager()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetTotalUnusedReservedMemoryLong();

		// Token: 0x06001C01 RID: 7169 RVA: 0x00031678 File Offset: 0x0002F878
		[Obsolete("GetTotalReservedMemory has been deprecated since it is limited to 4GB. Please use GetTotalReservedMemoryLong() instead.")]
		public static uint GetTotalReservedMemory()
		{
			return (uint)Profiler.GetTotalReservedMemoryLong();
		}

		// Token: 0x06001C02 RID: 7170
		[NativeMethod(Name = "GetTotalReservedMemory")]
		[StaticAccessor("GetMemoryManager()", StaticAccessorType.Dot)]
		[NativeConditional("ENABLE_MEMORY_MANAGER")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetTotalReservedMemoryLong();

		// Token: 0x06001C03 RID: 7171
		[NativeMethod(Name = "GetRegisteredGFXDriverMemory")]
		[StaticAccessor("GetMemoryManager()", StaticAccessorType.Dot)]
		[NativeConditional("ENABLE_PROFILER")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetAllocatedMemoryForGraphicsDriver();
	}
}
