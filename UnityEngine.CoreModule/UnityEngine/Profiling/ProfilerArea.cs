﻿using System;

namespace UnityEngine.Profiling
{
	// Token: 0x02000341 RID: 833
	public enum ProfilerArea
	{
		// Token: 0x04000A8C RID: 2700
		CPU,
		// Token: 0x04000A8D RID: 2701
		GPU,
		// Token: 0x04000A8E RID: 2702
		Rendering,
		// Token: 0x04000A8F RID: 2703
		Memory,
		// Token: 0x04000A90 RID: 2704
		Audio,
		// Token: 0x04000A91 RID: 2705
		Video,
		// Token: 0x04000A92 RID: 2706
		Physics,
		// Token: 0x04000A93 RID: 2707
		Physics2D,
		// Token: 0x04000A94 RID: 2708
		NetworkMessages,
		// Token: 0x04000A95 RID: 2709
		NetworkOperations,
		// Token: 0x04000A96 RID: 2710
		UI,
		// Token: 0x04000A97 RID: 2711
		UIDetails,
		// Token: 0x04000A98 RID: 2712
		GlobalIllumination
	}
}
