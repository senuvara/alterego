﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Profiling
{
	// Token: 0x02000346 RID: 838
	[NativeHeader("Runtime/Profiler/ScriptBindings/Recorder.bindings.h")]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Profiler/Recorder.h")]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class Recorder
	{
		// Token: 0x06001C0F RID: 7183 RVA: 0x00002050 File Offset: 0x00000250
		internal Recorder()
		{
		}

		// Token: 0x06001C10 RID: 7184 RVA: 0x0003170E File Offset: 0x0002F90E
		internal Recorder(IntPtr ptr)
		{
			this.m_Ptr = ptr;
		}

		// Token: 0x06001C11 RID: 7185 RVA: 0x00031720 File Offset: 0x0002F920
		~Recorder()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				Recorder.DisposeNative(this.m_Ptr);
			}
		}

		// Token: 0x06001C12 RID: 7186 RVA: 0x0003176C File Offset: 0x0002F96C
		public static Recorder Get(string samplerName)
		{
			IntPtr @internal = Recorder.GetInternal(samplerName);
			Recorder result;
			if (@internal == IntPtr.Zero)
			{
				result = Recorder.s_InvalidRecorder;
			}
			else
			{
				result = new Recorder(@internal);
			}
			return result;
		}

		// Token: 0x06001C13 RID: 7187
		[NativeMethod(Name = "ProfilerBindings::GetRecorderInternal", IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetInternal(string samplerName);

		// Token: 0x17000522 RID: 1314
		// (get) Token: 0x06001C14 RID: 7188 RVA: 0x000317AC File Offset: 0x0002F9AC
		public bool isValid
		{
			get
			{
				return this.m_Ptr != IntPtr.Zero;
			}
		}

		// Token: 0x06001C15 RID: 7189
		[NativeMethod(Name = "ProfilerBindings::DisposeNativeRecorder", IsFreeFunction = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DisposeNative(IntPtr ptr);

		// Token: 0x17000523 RID: 1315
		// (get) Token: 0x06001C16 RID: 7190 RVA: 0x000317D4 File Offset: 0x0002F9D4
		// (set) Token: 0x06001C17 RID: 7191 RVA: 0x00031800 File Offset: 0x0002FA00
		public bool enabled
		{
			get
			{
				return this.isValid && this.IsEnabled();
			}
			set
			{
				if (this.isValid)
				{
					this.SetEnabled(value);
				}
			}
		}

		// Token: 0x06001C18 RID: 7192
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsEnabled();

		// Token: 0x06001C19 RID: 7193
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetEnabled(bool enabled);

		// Token: 0x17000524 RID: 1316
		// (get) Token: 0x06001C1A RID: 7194 RVA: 0x00031818 File Offset: 0x0002FA18
		public long elapsedNanoseconds
		{
			get
			{
				return (!this.isValid) ? 0L : this.GetElapsedNanoseconds();
			}
		}

		// Token: 0x06001C1B RID: 7195
		[NativeConditional("ENABLE_PROFILER")]
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern long GetElapsedNanoseconds();

		// Token: 0x17000525 RID: 1317
		// (get) Token: 0x06001C1C RID: 7196 RVA: 0x00031848 File Offset: 0x0002FA48
		public int sampleBlockCount
		{
			get
			{
				return (!this.isValid) ? 0 : this.GetSampleBlockCount();
			}
		}

		// Token: 0x06001C1D RID: 7197
		[NativeMethod(IsThreadSafe = true)]
		[NativeConditional("ENABLE_PROFILER")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetSampleBlockCount();

		// Token: 0x06001C1E RID: 7198
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void FilterToCurrentThread();

		// Token: 0x06001C1F RID: 7199
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CollectFromAllThreads();

		// Token: 0x06001C20 RID: 7200 RVA: 0x00031874 File Offset: 0x0002FA74
		// Note: this type is marked as 'beforefieldinit'.
		static Recorder()
		{
		}

		// Token: 0x04000AA3 RID: 2723
		internal IntPtr m_Ptr;

		// Token: 0x04000AA4 RID: 2724
		internal static Recorder s_InvalidRecorder = new Recorder();
	}
}
