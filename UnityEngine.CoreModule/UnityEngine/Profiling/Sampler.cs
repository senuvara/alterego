﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Profiling
{
	// Token: 0x02000347 RID: 839
	[NativeHeader("Runtime/Profiler/ScriptBindings/Sampler.bindings.h")]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Profiler/Marker.h")]
	public class Sampler
	{
		// Token: 0x06001C21 RID: 7201 RVA: 0x00002050 File Offset: 0x00000250
		internal Sampler()
		{
		}

		// Token: 0x06001C22 RID: 7202 RVA: 0x00031880 File Offset: 0x0002FA80
		internal Sampler(IntPtr ptr)
		{
			this.m_Ptr = ptr;
		}

		// Token: 0x17000526 RID: 1318
		// (get) Token: 0x06001C23 RID: 7203 RVA: 0x00031890 File Offset: 0x0002FA90
		public bool isValid
		{
			get
			{
				return this.m_Ptr != IntPtr.Zero;
			}
		}

		// Token: 0x06001C24 RID: 7204 RVA: 0x000318B8 File Offset: 0x0002FAB8
		public Recorder GetRecorder()
		{
			IntPtr recorderInternal = Sampler.GetRecorderInternal(this.m_Ptr);
			Recorder result;
			if (recorderInternal == IntPtr.Zero)
			{
				result = Recorder.s_InvalidRecorder;
			}
			else
			{
				result = new Recorder(recorderInternal);
			}
			return result;
		}

		// Token: 0x06001C25 RID: 7205 RVA: 0x000318FC File Offset: 0x0002FAFC
		public static Sampler Get(string name)
		{
			IntPtr samplerInternal = Sampler.GetSamplerInternal(name);
			Sampler result;
			if (samplerInternal == IntPtr.Zero)
			{
				result = Sampler.s_InvalidSampler;
			}
			else
			{
				result = new Sampler(samplerInternal);
			}
			return result;
		}

		// Token: 0x06001C26 RID: 7206 RVA: 0x0003193C File Offset: 0x0002FB3C
		public static int GetNames(List<string> names)
		{
			return Sampler.GetSamplerNamesInternal(names);
		}

		// Token: 0x06001C27 RID: 7207
		[NativeMethod(Name = "GetName", IsThreadSafe = true)]
		[NativeConditional("ENABLE_PROFILER")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string GetSamplerName();

		// Token: 0x17000527 RID: 1319
		// (get) Token: 0x06001C28 RID: 7208 RVA: 0x00031958 File Offset: 0x0002FB58
		public string name
		{
			get
			{
				return (!this.isValid) ? null : this.GetSamplerName();
			}
		}

		// Token: 0x06001C29 RID: 7209
		[NativeMethod(Name = "ProfilerBindings::GetRecorderInternal", IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetRecorderInternal(IntPtr ptr);

		// Token: 0x06001C2A RID: 7210
		[NativeMethod(Name = "ProfilerBindings::GetSamplerInternal", IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetSamplerInternal([NotNull] string name);

		// Token: 0x06001C2B RID: 7211
		[NativeMethod(Name = "ProfilerBindings::GetSamplerNamesInternal", IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetSamplerNamesInternal(List<string> namesScriptingPtr);

		// Token: 0x06001C2C RID: 7212 RVA: 0x00031984 File Offset: 0x0002FB84
		// Note: this type is marked as 'beforefieldinit'.
		static Sampler()
		{
		}

		// Token: 0x04000AA5 RID: 2725
		internal IntPtr m_Ptr;

		// Token: 0x04000AA6 RID: 2726
		internal static Sampler s_InvalidSampler = new Sampler();
	}
}
