﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Profiling
{
	// Token: 0x02000348 RID: 840
	[NativeHeader("Runtime/Profiler/ScriptBindings/Sampler.bindings.h")]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Profiler/Marker.h")]
	public sealed class CustomSampler : Sampler
	{
		// Token: 0x06001C2D RID: 7213 RVA: 0x00031990 File Offset: 0x0002FB90
		internal CustomSampler()
		{
		}

		// Token: 0x06001C2E RID: 7214 RVA: 0x00031999 File Offset: 0x0002FB99
		internal CustomSampler(IntPtr ptr)
		{
			this.m_Ptr = ptr;
		}

		// Token: 0x06001C2F RID: 7215 RVA: 0x000319AC File Offset: 0x0002FBAC
		public static CustomSampler Create(string name)
		{
			IntPtr intPtr = CustomSampler.CreateInternal(name);
			CustomSampler result;
			if (intPtr == IntPtr.Zero)
			{
				result = CustomSampler.s_InvalidCustomSampler;
			}
			else
			{
				result = new CustomSampler(intPtr);
			}
			return result;
		}

		// Token: 0x06001C30 RID: 7216
		[NativeMethod(Name = "ProfilerBindings::CreateCustomSamplerInternal", IsFreeFunction = true, ThrowsException = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr CreateInternal([NotNull] string name);

		// Token: 0x06001C31 RID: 7217
		[Conditional("ENABLE_PROFILER")]
		[NativeMethod(Name = "ProfilerBindings::CustomSampler_Begin", IsFreeFunction = true, HasExplicitThis = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Begin();

		// Token: 0x06001C32 RID: 7218 RVA: 0x00007476 File Offset: 0x00005676
		[Conditional("ENABLE_PROFILER")]
		public void Begin(Object targetObject)
		{
		}

		// Token: 0x06001C33 RID: 7219
		[NativeMethod(Name = "ProfilerBindings::CustomSampler_BeginWithObject", IsFreeFunction = true, HasExplicitThis = true, IsThreadSafe = true)]
		[Conditional("ENABLE_PROFILER")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void BeginWithObject(Object targetObject);

		// Token: 0x06001C34 RID: 7220
		[Conditional("ENABLE_PROFILER")]
		[NativeMethod(Name = "ProfilerBindings::CustomSampler_End", IsFreeFunction = true, HasExplicitThis = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void End();

		// Token: 0x06001C35 RID: 7221 RVA: 0x000319E9 File Offset: 0x0002FBE9
		// Note: this type is marked as 'beforefieldinit'.
		static CustomSampler()
		{
		}

		// Token: 0x04000AA7 RID: 2727
		internal static CustomSampler s_InvalidCustomSampler = new CustomSampler();
	}
}
