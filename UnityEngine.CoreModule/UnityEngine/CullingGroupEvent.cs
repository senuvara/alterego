﻿using System;

namespace UnityEngine
{
	// Token: 0x0200005F RID: 95
	public struct CullingGroupEvent
	{
		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000501 RID: 1281 RVA: 0x0000CBFC File Offset: 0x0000ADFC
		public int index
		{
			get
			{
				return this.m_Index;
			}
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000502 RID: 1282 RVA: 0x0000CC18 File Offset: 0x0000AE18
		public bool isVisible
		{
			get
			{
				return (this.m_ThisState & 128) != 0;
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000503 RID: 1283 RVA: 0x0000CC40 File Offset: 0x0000AE40
		public bool wasVisible
		{
			get
			{
				return (this.m_PrevState & 128) != 0;
			}
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000504 RID: 1284 RVA: 0x0000CC68 File Offset: 0x0000AE68
		public bool hasBecomeVisible
		{
			get
			{
				return this.isVisible && !this.wasVisible;
			}
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x06000505 RID: 1285 RVA: 0x0000CC94 File Offset: 0x0000AE94
		public bool hasBecomeInvisible
		{
			get
			{
				return !this.isVisible && this.wasVisible;
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000506 RID: 1286 RVA: 0x0000CCC0 File Offset: 0x0000AEC0
		public int currentDistance
		{
			get
			{
				return (int)(this.m_ThisState & 127);
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000507 RID: 1287 RVA: 0x0000CCE0 File Offset: 0x0000AEE0
		public int previousDistance
		{
			get
			{
				return (int)(this.m_PrevState & 127);
			}
		}

		// Token: 0x04000125 RID: 293
		private int m_Index;

		// Token: 0x04000126 RID: 294
		private byte m_PrevState;

		// Token: 0x04000127 RID: 295
		private byte m_ThisState;

		// Token: 0x04000128 RID: 296
		private const byte kIsVisibleMask = 128;

		// Token: 0x04000129 RID: 297
		private const byte kDistanceMask = 127;
	}
}
