﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020000D8 RID: 216
	[NativeHeader("Runtime/Camera/LightProbeProxyVolume.h")]
	public sealed class LightProbeProxyVolume : Behaviour
	{
		// Token: 0x06000B1D RID: 2845 RVA: 0x0000A52A File Offset: 0x0000872A
		public LightProbeProxyVolume()
		{
		}

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x06000B1E RID: 2846
		public static extern bool isFeatureSupported { [NativeName("IsFeatureSupported")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700020F RID: 527
		// (get) Token: 0x06000B1F RID: 2847 RVA: 0x000124A8 File Offset: 0x000106A8
		[NativeName("GlobalAABB")]
		public Bounds boundsGlobal
		{
			get
			{
				Bounds result;
				this.get_boundsGlobal_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000210 RID: 528
		// (get) Token: 0x06000B20 RID: 2848 RVA: 0x000124C0 File Offset: 0x000106C0
		// (set) Token: 0x06000B21 RID: 2849 RVA: 0x000124D6 File Offset: 0x000106D6
		[NativeName("BoundingBoxSizeCustom")]
		public Vector3 sizeCustom
		{
			get
			{
				Vector3 result;
				this.get_sizeCustom_Injected(out result);
				return result;
			}
			set
			{
				this.set_sizeCustom_Injected(ref value);
			}
		}

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x06000B22 RID: 2850 RVA: 0x000124E0 File Offset: 0x000106E0
		// (set) Token: 0x06000B23 RID: 2851 RVA: 0x000124F6 File Offset: 0x000106F6
		[NativeName("BoundingBoxOriginCustom")]
		public Vector3 originCustom
		{
			get
			{
				Vector3 result;
				this.get_originCustom_Injected(out result);
				return result;
			}
			set
			{
				this.set_originCustom_Injected(ref value);
			}
		}

		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06000B24 RID: 2852
		// (set) Token: 0x06000B25 RID: 2853
		public extern float probeDensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000213 RID: 531
		// (get) Token: 0x06000B26 RID: 2854
		// (set) Token: 0x06000B27 RID: 2855
		public extern int gridResolutionX { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000214 RID: 532
		// (get) Token: 0x06000B28 RID: 2856
		// (set) Token: 0x06000B29 RID: 2857
		public extern int gridResolutionY { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06000B2A RID: 2858
		// (set) Token: 0x06000B2B RID: 2859
		public extern int gridResolutionZ { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000216 RID: 534
		// (get) Token: 0x06000B2C RID: 2860
		// (set) Token: 0x06000B2D RID: 2861
		public extern LightProbeProxyVolume.BoundingBoxMode boundingBoxMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000217 RID: 535
		// (get) Token: 0x06000B2E RID: 2862
		// (set) Token: 0x06000B2F RID: 2863
		public extern LightProbeProxyVolume.ResolutionMode resolutionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000218 RID: 536
		// (get) Token: 0x06000B30 RID: 2864
		// (set) Token: 0x06000B31 RID: 2865
		public extern LightProbeProxyVolume.ProbePositionMode probePositionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000219 RID: 537
		// (get) Token: 0x06000B32 RID: 2866
		// (set) Token: 0x06000B33 RID: 2867
		public extern LightProbeProxyVolume.RefreshMode refreshMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700021A RID: 538
		// (get) Token: 0x06000B34 RID: 2868
		// (set) Token: 0x06000B35 RID: 2869
		public extern LightProbeProxyVolume.QualityMode qualityMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000B36 RID: 2870 RVA: 0x00012500 File Offset: 0x00010700
		public void Update()
		{
			this.SetDirtyFlag(true);
		}

		// Token: 0x06000B37 RID: 2871
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetDirtyFlag(bool flag);

		// Token: 0x06000B38 RID: 2872
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_boundsGlobal_Injected(out Bounds ret);

		// Token: 0x06000B39 RID: 2873
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_sizeCustom_Injected(out Vector3 ret);

		// Token: 0x06000B3A RID: 2874
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_sizeCustom_Injected(ref Vector3 value);

		// Token: 0x06000B3B RID: 2875
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_originCustom_Injected(out Vector3 ret);

		// Token: 0x06000B3C RID: 2876
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_originCustom_Injected(ref Vector3 value);

		// Token: 0x020000D9 RID: 217
		public enum ResolutionMode
		{
			// Token: 0x04000358 RID: 856
			Automatic,
			// Token: 0x04000359 RID: 857
			Custom
		}

		// Token: 0x020000DA RID: 218
		public enum BoundingBoxMode
		{
			// Token: 0x0400035B RID: 859
			AutomaticLocal,
			// Token: 0x0400035C RID: 860
			AutomaticWorld,
			// Token: 0x0400035D RID: 861
			Custom
		}

		// Token: 0x020000DB RID: 219
		public enum ProbePositionMode
		{
			// Token: 0x0400035F RID: 863
			CellCorner,
			// Token: 0x04000360 RID: 864
			CellCenter
		}

		// Token: 0x020000DC RID: 220
		public enum RefreshMode
		{
			// Token: 0x04000362 RID: 866
			Automatic,
			// Token: 0x04000363 RID: 867
			EveryFrame,
			// Token: 0x04000364 RID: 868
			ViaScripting
		}

		// Token: 0x020000DD RID: 221
		public enum QualityMode
		{
			// Token: 0x04000366 RID: 870
			Low,
			// Token: 0x04000367 RID: 871
			Normal
		}
	}
}
