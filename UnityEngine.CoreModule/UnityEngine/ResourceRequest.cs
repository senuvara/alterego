﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001FF RID: 511
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class ResourceRequest : AsyncOperation
	{
		// Token: 0x06001154 RID: 4436 RVA: 0x0001DA7F File Offset: 0x0001BC7F
		public ResourceRequest()
		{
		}

		// Token: 0x1700033A RID: 826
		// (get) Token: 0x06001155 RID: 4437 RVA: 0x0001DA88 File Offset: 0x0001BC88
		public Object asset
		{
			get
			{
				return Resources.Load(this.m_Path, this.m_Type);
			}
		}

		// Token: 0x0400074B RID: 1867
		internal string m_Path;

		// Token: 0x0400074C RID: 1868
		internal Type m_Type;
	}
}
