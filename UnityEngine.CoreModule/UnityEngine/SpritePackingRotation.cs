﻿using System;

namespace UnityEngine
{
	// Token: 0x02000361 RID: 865
	public enum SpritePackingRotation
	{
		// Token: 0x04000AFD RID: 2813
		None,
		// Token: 0x04000AFE RID: 2814
		FlipHorizontal,
		// Token: 0x04000AFF RID: 2815
		FlipVertical,
		// Token: 0x04000B00 RID: 2816
		Rotate180,
		// Token: 0x04000B01 RID: 2817
		Any = 15
	}
}
