﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B9 RID: 185
	public enum MixedLightingMode
	{
		// Token: 0x040001E4 RID: 484
		IndirectOnly,
		// Token: 0x040001E5 RID: 485
		Shadowmask = 2,
		// Token: 0x040001E6 RID: 486
		Subtractive = 1
	}
}
