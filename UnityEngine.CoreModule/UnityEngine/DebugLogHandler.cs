﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000066 RID: 102
	[NativeHeader("Runtime/Export/Debug.bindings.h")]
	internal sealed class DebugLogHandler : ILogHandler
	{
		// Token: 0x06000536 RID: 1334 RVA: 0x00002370 File Offset: 0x00000570
		public DebugLogHandler()
		{
		}

		// Token: 0x06000537 RID: 1335
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_Log(LogType level, string msg, Object obj);

		// Token: 0x06000538 RID: 1336
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_LogException(Exception exception, Object obj);

		// Token: 0x06000539 RID: 1337 RVA: 0x0000CEDB File Offset: 0x0000B0DB
		public void LogFormat(LogType logType, Object context, string format, params object[] args)
		{
			DebugLogHandler.Internal_Log(logType, string.Format(format, args), context);
		}

		// Token: 0x0600053A RID: 1338 RVA: 0x0000CEED File Offset: 0x0000B0ED
		public void LogException(Exception exception, Object context)
		{
			if (exception == null)
			{
				throw new ArgumentNullException("exception");
			}
			DebugLogHandler.Internal_LogException(exception, context);
		}
	}
}
