﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x020000AB RID: 171
	[RequireComponent(typeof(Transform))]
	[NativeHeader("Runtime/Export/Light.bindings.h")]
	[RequireComponent(typeof(Transform))]
	[NativeHeader("Runtime/Camera/Light.h")]
	public sealed class Light : Behaviour
	{
		// Token: 0x06000AC6 RID: 2758 RVA: 0x0000A52A File Offset: 0x0000872A
		public Light()
		{
		}

		// Token: 0x170001ED RID: 493
		// (get) Token: 0x06000AC7 RID: 2759
		// (set) Token: 0x06000AC8 RID: 2760
		[NativeProperty("LightType")]
		public extern LightType type { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001EE RID: 494
		// (get) Token: 0x06000AC9 RID: 2761
		// (set) Token: 0x06000ACA RID: 2762
		public extern float spotAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001EF RID: 495
		// (get) Token: 0x06000ACB RID: 2763 RVA: 0x00012374 File Offset: 0x00010574
		// (set) Token: 0x06000ACC RID: 2764 RVA: 0x0001238A File Offset: 0x0001058A
		public Color color
		{
			get
			{
				Color result;
				this.get_color_Injected(out result);
				return result;
			}
			set
			{
				this.set_color_Injected(ref value);
			}
		}

		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x06000ACD RID: 2765
		// (set) Token: 0x06000ACE RID: 2766
		public extern float colorTemperature { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x06000ACF RID: 2767
		// (set) Token: 0x06000AD0 RID: 2768
		public extern float intensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x06000AD1 RID: 2769
		// (set) Token: 0x06000AD2 RID: 2770
		public extern float bounceIntensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x06000AD3 RID: 2771
		// (set) Token: 0x06000AD4 RID: 2772
		public extern int shadowCustomResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x06000AD5 RID: 2773
		// (set) Token: 0x06000AD6 RID: 2774
		public extern float shadowBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x06000AD7 RID: 2775
		// (set) Token: 0x06000AD8 RID: 2776
		public extern float shadowNormalBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x06000AD9 RID: 2777
		// (set) Token: 0x06000ADA RID: 2778
		public extern float shadowNearPlane { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x06000ADB RID: 2779
		// (set) Token: 0x06000ADC RID: 2780
		public extern float range { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x06000ADD RID: 2781
		// (set) Token: 0x06000ADE RID: 2782
		public extern Flare flare { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x06000ADF RID: 2783 RVA: 0x00012394 File Offset: 0x00010594
		// (set) Token: 0x06000AE0 RID: 2784 RVA: 0x000123AA File Offset: 0x000105AA
		public LightBakingOutput bakingOutput
		{
			get
			{
				LightBakingOutput result;
				this.get_bakingOutput_Injected(out result);
				return result;
			}
			set
			{
				this.set_bakingOutput_Injected(ref value);
			}
		}

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x06000AE1 RID: 2785
		// (set) Token: 0x06000AE2 RID: 2786
		public extern int cullingMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x06000AE3 RID: 2787
		// (set) Token: 0x06000AE4 RID: 2788
		public extern LightShadowCasterMode lightShadowCasterMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000AE5 RID: 2789
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Reset();

		// Token: 0x170001FC RID: 508
		// (get) Token: 0x06000AE6 RID: 2790
		// (set) Token: 0x06000AE7 RID: 2791
		public extern LightShadows shadows { [NativeMethod("GetShadowType")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("Light_Bindings::SetShadowType", HasExplicitThis = true, ThrowsException = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001FD RID: 509
		// (get) Token: 0x06000AE8 RID: 2792
		// (set) Token: 0x06000AE9 RID: 2793
		public extern float shadowStrength { [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("Light_Bindings::SetShadowStrength", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001FE RID: 510
		// (get) Token: 0x06000AEA RID: 2794
		// (set) Token: 0x06000AEB RID: 2795
		public extern LightShadowResolution shadowResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("Light_Bindings::SetShadowResolution", HasExplicitThis = true, ThrowsException = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001FF RID: 511
		// (get) Token: 0x06000AEC RID: 2796 RVA: 0x000123B4 File Offset: 0x000105B4
		// (set) Token: 0x06000AED RID: 2797 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Shadow softness is removed in Unity 5.0+", true)]
		public float shadowSoftness
		{
			get
			{
				return 4f;
			}
			set
			{
			}
		}

		// Token: 0x17000200 RID: 512
		// (get) Token: 0x06000AEE RID: 2798 RVA: 0x000123D0 File Offset: 0x000105D0
		// (set) Token: 0x06000AEF RID: 2799 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Shadow softness is removed in Unity 5.0+", true)]
		public float shadowSoftnessFade
		{
			get
			{
				return 1f;
			}
			set
			{
			}
		}

		// Token: 0x17000201 RID: 513
		// (get) Token: 0x06000AF0 RID: 2800
		// (set) Token: 0x06000AF1 RID: 2801
		public extern float[] layerShadowCullDistances { [FreeFunction("Light_Bindings::GetLayerShadowCullDistances", HasExplicitThis = true, ThrowsException = false)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("Light_Bindings::SetLayerShadowCullDistances", HasExplicitThis = true, ThrowsException = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000202 RID: 514
		// (get) Token: 0x06000AF2 RID: 2802
		// (set) Token: 0x06000AF3 RID: 2803
		public extern float cookieSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000AF4 RID: 2804
		// (set) Token: 0x06000AF5 RID: 2805
		public extern Texture cookie { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x06000AF6 RID: 2806
		// (set) Token: 0x06000AF7 RID: 2807
		public extern LightRenderMode renderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("Light_Bindings::SetRenderMode", HasExplicitThis = true, ThrowsException = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000AF8 RID: 2808 RVA: 0x000123EC File Offset: 0x000105EC
		// (set) Token: 0x06000AF9 RID: 2809 RVA: 0x00012407 File Offset: 0x00010607
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("warning bakedIndex has been removed please use bakingOutput.isBaked instead.", true)]
		public int bakedIndex
		{
			get
			{
				return this.m_BakedIndex;
			}
			set
			{
				this.m_BakedIndex = value;
			}
		}

		// Token: 0x06000AFA RID: 2810
		[FreeFunction("Light_Bindings::SetFalloffTable", HasExplicitThis = true, ThrowsException = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFalloffTable([NotNull] float[] input);

		// Token: 0x06000AFB RID: 2811
		[FreeFunction("Light_Bindings::SetAllLightsFalloffToInverseSquared")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetAllLightsFalloffToInverseSquared();

		// Token: 0x06000AFC RID: 2812
		[FreeFunction("Light_Bindings::SetAllLightsFalloffToUnityLegacy")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetAllLightsFalloffToUnityLegacy();

		// Token: 0x06000AFD RID: 2813 RVA: 0x00012411 File Offset: 0x00010611
		public void AddCommandBuffer(LightEvent evt, CommandBuffer buffer)
		{
			this.AddCommandBuffer(evt, buffer, ShadowMapPass.All);
		}

		// Token: 0x06000AFE RID: 2814
		[FreeFunction("Light_Bindings::AddCommandBuffer", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddCommandBuffer(LightEvent evt, CommandBuffer buffer, ShadowMapPass shadowPassMask);

		// Token: 0x06000AFF RID: 2815 RVA: 0x00012421 File Offset: 0x00010621
		public void AddCommandBufferAsync(LightEvent evt, CommandBuffer buffer, ComputeQueueType queueType)
		{
			this.AddCommandBufferAsync(evt, buffer, ShadowMapPass.All, queueType);
		}

		// Token: 0x06000B00 RID: 2816
		[FreeFunction("Light_Bindings::AddCommandBufferAsync", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddCommandBufferAsync(LightEvent evt, CommandBuffer buffer, ShadowMapPass shadowPassMask, ComputeQueueType queueType);

		// Token: 0x06000B01 RID: 2817
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveCommandBuffer(LightEvent evt, CommandBuffer buffer);

		// Token: 0x06000B02 RID: 2818
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveCommandBuffers(LightEvent evt);

		// Token: 0x06000B03 RID: 2819
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveAllCommandBuffers();

		// Token: 0x06000B04 RID: 2820
		[FreeFunction("Light_Bindings::GetCommandBuffers", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern CommandBuffer[] GetCommandBuffers(LightEvent evt);

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x06000B05 RID: 2821
		public extern int commandBufferCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x06000B06 RID: 2822 RVA: 0x00012434 File Offset: 0x00010634
		// (set) Token: 0x06000B07 RID: 2823 RVA: 0x0001244E File Offset: 0x0001064E
		[Obsolete("Use QualitySettings.pixelLightCount instead.")]
		public static int pixelLightCount
		{
			get
			{
				return QualitySettings.pixelLightCount;
			}
			set
			{
				QualitySettings.pixelLightCount = value;
			}
		}

		// Token: 0x06000B08 RID: 2824
		[FreeFunction("Light_Bindings::GetLights")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Light[] GetLights(LightType type, int layer);

		// Token: 0x17000208 RID: 520
		// (get) Token: 0x06000B09 RID: 2825 RVA: 0x00012458 File Offset: 0x00010658
		// (set) Token: 0x06000B0A RID: 2826 RVA: 0x00007476 File Offset: 0x00005676
		[Obsolete("light.shadowConstantBias was removed, use light.shadowBias", true)]
		public float shadowConstantBias
		{
			get
			{
				return 0f;
			}
			set
			{
			}
		}

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x06000B0B RID: 2827 RVA: 0x00012474 File Offset: 0x00010674
		// (set) Token: 0x06000B0C RID: 2828 RVA: 0x00007476 File Offset: 0x00005676
		[Obsolete("light.shadowObjectSizeBias was removed, use light.shadowBias", true)]
		public float shadowObjectSizeBias
		{
			get
			{
				return 0f;
			}
			set
			{
			}
		}

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x06000B0D RID: 2829 RVA: 0x00012490 File Offset: 0x00010690
		// (set) Token: 0x06000B0E RID: 2830 RVA: 0x00007476 File Offset: 0x00005676
		[Obsolete("light.attenuate was removed; all lights always attenuate now", true)]
		public bool attenuate
		{
			get
			{
				return true;
			}
			set
			{
			}
		}

		// Token: 0x06000B0F RID: 2831
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_color_Injected(out Color ret);

		// Token: 0x06000B10 RID: 2832
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_color_Injected(ref Color value);

		// Token: 0x06000B11 RID: 2833
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bakingOutput_Injected(out LightBakingOutput ret);

		// Token: 0x06000B12 RID: 2834
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_bakingOutput_Injected(ref LightBakingOutput value);

		// Token: 0x040001AD RID: 429
		private int m_BakedIndex;
	}
}
