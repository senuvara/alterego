﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200003C RID: 60
	[AttributeUsage(AttributeTargets.Method)]
	public class BeforeRenderOrderAttribute : Attribute
	{
		// Token: 0x06000295 RID: 661 RVA: 0x00008B43 File Offset: 0x00006D43
		public BeforeRenderOrderAttribute(int order)
		{
			this.order = order;
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000296 RID: 662 RVA: 0x00008B54 File Offset: 0x00006D54
		// (set) Token: 0x06000297 RID: 663 RVA: 0x00008B6E File Offset: 0x00006D6E
		public int order
		{
			[CompilerGenerated]
			get
			{
				return this.<order>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<order>k__BackingField = value;
			}
		}

		// Token: 0x040000E4 RID: 228
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <order>k__BackingField;
	}
}
