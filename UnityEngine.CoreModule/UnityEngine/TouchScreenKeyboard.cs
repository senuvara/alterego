﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x0200011B RID: 283
	[NativeHeader("Runtime/Input/OnScreenKeyboard.h")]
	[NativeHeader("Runtime/Export/Handheld.bindings.h")]
	public class TouchScreenKeyboard
	{
		// Token: 0x06000BD8 RID: 3032 RVA: 0x00012EAC File Offset: 0x000110AC
		public TouchScreenKeyboard(string text, TouchScreenKeyboardType keyboardType, bool autocorrection, bool multiline, bool secure, bool alert, string textPlaceholder, int characterLimit)
		{
			TouchScreenKeyboard_InternalConstructorHelperArguments touchScreenKeyboard_InternalConstructorHelperArguments = default(TouchScreenKeyboard_InternalConstructorHelperArguments);
			touchScreenKeyboard_InternalConstructorHelperArguments.keyboardType = Convert.ToUInt32(keyboardType);
			touchScreenKeyboard_InternalConstructorHelperArguments.autocorrection = Convert.ToUInt32(autocorrection);
			touchScreenKeyboard_InternalConstructorHelperArguments.multiline = Convert.ToUInt32(multiline);
			touchScreenKeyboard_InternalConstructorHelperArguments.secure = Convert.ToUInt32(secure);
			touchScreenKeyboard_InternalConstructorHelperArguments.alert = Convert.ToUInt32(alert);
			touchScreenKeyboard_InternalConstructorHelperArguments.characterLimit = characterLimit;
			this.m_Ptr = TouchScreenKeyboard.TouchScreenKeyboard_InternalConstructorHelper(ref touchScreenKeyboard_InternalConstructorHelperArguments, text, textPlaceholder);
		}

		// Token: 0x06000BD9 RID: 3033
		[FreeFunction("TouchScreenKeyboard_Destroy", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Destroy(IntPtr ptr);

		// Token: 0x06000BDA RID: 3034 RVA: 0x00012F2A File Offset: 0x0001112A
		private void Destroy()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				TouchScreenKeyboard.Internal_Destroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000BDB RID: 3035 RVA: 0x00012F60 File Offset: 0x00011160
		~TouchScreenKeyboard()
		{
			this.Destroy();
		}

		// Token: 0x06000BDC RID: 3036
		[FreeFunction("TouchScreenKeyboard_InternalConstructorHelper")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr TouchScreenKeyboard_InternalConstructorHelper(ref TouchScreenKeyboard_InternalConstructorHelperArguments arguments, string text, string textPlaceholder);

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x06000BDD RID: 3037 RVA: 0x00012F90 File Offset: 0x00011190
		public static bool isSupported
		{
			get
			{
				RuntimePlatform platform = Application.platform;
				bool result;
				switch (platform)
				{
				case RuntimePlatform.MetroPlayerX86:
				case RuntimePlatform.MetroPlayerX64:
				case RuntimePlatform.MetroPlayerARM:
					result = false;
					break;
				default:
					switch (platform)
					{
					case RuntimePlatform.IPhonePlayer:
					case RuntimePlatform.Android:
						break;
					default:
						if (platform != RuntimePlatform.tvOS && platform != RuntimePlatform.Switch)
						{
							return false;
						}
						break;
					}
					result = true;
					break;
				}
				return result;
			}
		}

		// Token: 0x06000BDE RID: 3038 RVA: 0x00012FFC File Offset: 0x000111FC
		public static TouchScreenKeyboard Open(string text, [DefaultValue("TouchScreenKeyboardType.Default")] TouchScreenKeyboardType keyboardType, [DefaultValue("true")] bool autocorrection, [DefaultValue("false")] bool multiline, [DefaultValue("false")] bool secure, [DefaultValue("false")] bool alert, [DefaultValue("\"\"")] string textPlaceholder, [DefaultValue("0")] int characterLimit)
		{
			return new TouchScreenKeyboard(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder, characterLimit);
		}

		// Token: 0x06000BDF RID: 3039 RVA: 0x00013024 File Offset: 0x00011224
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType, bool autocorrection, bool multiline, bool secure, bool alert, string textPlaceholder)
		{
			int characterLimit = 0;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder, characterLimit);
		}

		// Token: 0x06000BE0 RID: 3040 RVA: 0x0001304C File Offset: 0x0001124C
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType, bool autocorrection, bool multiline, bool secure, bool alert)
		{
			int characterLimit = 0;
			string textPlaceholder = "";
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder, characterLimit);
		}

		// Token: 0x06000BE1 RID: 3041 RVA: 0x00013078 File Offset: 0x00011278
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType, bool autocorrection, bool multiline, bool secure)
		{
			int characterLimit = 0;
			string textPlaceholder = "";
			bool alert = false;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder, characterLimit);
		}

		// Token: 0x06000BE2 RID: 3042 RVA: 0x000130A8 File Offset: 0x000112A8
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType, bool autocorrection, bool multiline)
		{
			int characterLimit = 0;
			string textPlaceholder = "";
			bool alert = false;
			bool secure = false;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder, characterLimit);
		}

		// Token: 0x06000BE3 RID: 3043 RVA: 0x000130D8 File Offset: 0x000112D8
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType, bool autocorrection)
		{
			int characterLimit = 0;
			string textPlaceholder = "";
			bool alert = false;
			bool secure = false;
			bool multiline = false;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder, characterLimit);
		}

		// Token: 0x06000BE4 RID: 3044 RVA: 0x0001310C File Offset: 0x0001130C
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType)
		{
			int characterLimit = 0;
			string textPlaceholder = "";
			bool alert = false;
			bool secure = false;
			bool multiline = false;
			bool autocorrection = true;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder, characterLimit);
		}

		// Token: 0x06000BE5 RID: 3045 RVA: 0x00013144 File Offset: 0x00011344
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text)
		{
			int characterLimit = 0;
			string textPlaceholder = "";
			bool alert = false;
			bool secure = false;
			bool multiline = false;
			bool autocorrection = true;
			TouchScreenKeyboardType keyboardType = TouchScreenKeyboardType.Default;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder, characterLimit);
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06000BE6 RID: 3046
		// (set) Token: 0x06000BE7 RID: 3047
		public extern string text { [NativeName("getText")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("setText")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06000BE8 RID: 3048
		// (set) Token: 0x06000BE9 RID: 3049
		public static extern bool hideInput { [NativeName("isInputHidden")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("setInputHidden")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000237 RID: 567
		// (get) Token: 0x06000BEA RID: 3050
		// (set) Token: 0x06000BEB RID: 3051
		public extern bool active { [NativeName("isActive")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("setActive")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000BEC RID: 3052
		[FreeFunction("TouchScreenKeyboard_GetDone")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetDone(IntPtr ptr);

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x06000BED RID: 3053 RVA: 0x00013180 File Offset: 0x00011380
		[Obsolete("Property done is deprecated, use status instead")]
		public bool done
		{
			get
			{
				return TouchScreenKeyboard.GetDone(this.m_Ptr);
			}
		}

		// Token: 0x06000BEE RID: 3054
		[FreeFunction("TouchScreenKeyboard_GetWasCanceled")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetWasCanceled(IntPtr ptr);

		// Token: 0x17000239 RID: 569
		// (get) Token: 0x06000BEF RID: 3055 RVA: 0x000131A0 File Offset: 0x000113A0
		[Obsolete("Property wasCanceled is deprecated, use status instead.")]
		public bool wasCanceled
		{
			get
			{
				return TouchScreenKeyboard.GetWasCanceled(this.m_Ptr);
			}
		}

		// Token: 0x1700023A RID: 570
		// (get) Token: 0x06000BF0 RID: 3056
		public extern TouchScreenKeyboard.Status status { [NativeName("GetKeyboardStatus")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x06000BF1 RID: 3057
		// (set) Token: 0x06000BF2 RID: 3058
		public extern int characterLimit { [NativeName("getCharacterLimit")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("setCharacterLimit")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x06000BF3 RID: 3059 RVA: 0x000131C0 File Offset: 0x000113C0
		public bool canGetSelection
		{
			[NativeName("CanGetSelection")]
			[CompilerGenerated]
			get
			{
				return this.<canGetSelection>k__BackingField;
			}
		}

		// Token: 0x1700023D RID: 573
		// (get) Token: 0x06000BF4 RID: 3060 RVA: 0x000131DC File Offset: 0x000113DC
		public bool canSetSelection
		{
			[NativeName("CanSetSelection")]
			[CompilerGenerated]
			get
			{
				return this.<canSetSelection>k__BackingField;
			}
		}

		// Token: 0x1700023E RID: 574
		// (get) Token: 0x06000BF5 RID: 3061 RVA: 0x000131F8 File Offset: 0x000113F8
		// (set) Token: 0x06000BF6 RID: 3062 RVA: 0x00013224 File Offset: 0x00011424
		public RangeInt selection
		{
			get
			{
				RangeInt result;
				TouchScreenKeyboard.GetSelection(out result.start, out result.length);
				return result;
			}
			set
			{
				if (value.start < 0 || value.length < 0 || value.start + value.length > this.text.Length)
				{
					throw new ArgumentOutOfRangeException("selection", "Selection is out of range.");
				}
				TouchScreenKeyboard.SetSelection(value.start, value.length);
			}
		}

		// Token: 0x06000BF7 RID: 3063
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetSelection(out int start, out int length);

		// Token: 0x06000BF8 RID: 3064
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetSelection(int start, int length);

		// Token: 0x1700023F RID: 575
		// (get) Token: 0x06000BF9 RID: 3065 RVA: 0x00013290 File Offset: 0x00011490
		public TouchScreenKeyboardType type
		{
			[NativeName("GetKeyboardType")]
			[CompilerGenerated]
			get
			{
				return this.<type>k__BackingField;
			}
		}

		// Token: 0x17000240 RID: 576
		// (get) Token: 0x06000BFA RID: 3066 RVA: 0x000132AC File Offset: 0x000114AC
		// (set) Token: 0x06000BFB RID: 3067 RVA: 0x00007476 File Offset: 0x00005676
		public int targetDisplay
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x17000241 RID: 577
		// (get) Token: 0x06000BFC RID: 3068 RVA: 0x000132C4 File Offset: 0x000114C4
		public static Rect area
		{
			[NativeName("GetRect")]
			get
			{
				Rect result;
				TouchScreenKeyboard.get_area_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000242 RID: 578
		// (get) Token: 0x06000BFD RID: 3069
		public static extern bool visible { [NativeName("IsVisible")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000BFE RID: 3070
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_area_Injected(out Rect ret);

		// Token: 0x0400050A RID: 1290
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x0400050B RID: 1291
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly bool <canGetSelection>k__BackingField;

		// Token: 0x0400050C RID: 1292
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly bool <canSetSelection>k__BackingField;

		// Token: 0x0400050D RID: 1293
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly TouchScreenKeyboardType <type>k__BackingField;

		// Token: 0x0200011C RID: 284
		public enum Status
		{
			// Token: 0x0400050F RID: 1295
			Visible,
			// Token: 0x04000510 RID: 1296
			Done,
			// Token: 0x04000511 RID: 1297
			Canceled,
			// Token: 0x04000512 RID: 1298
			LostFocus
		}
	}
}
