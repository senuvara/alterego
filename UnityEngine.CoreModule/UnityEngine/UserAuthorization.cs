﻿using System;

namespace UnityEngine
{
	// Token: 0x0200001B RID: 27
	public enum UserAuthorization
	{
		// Token: 0x04000047 RID: 71
		WebCam = 1,
		// Token: 0x04000048 RID: 72
		Microphone
	}
}
