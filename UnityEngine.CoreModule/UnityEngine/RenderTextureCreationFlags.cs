﻿using System;

namespace UnityEngine
{
	// Token: 0x020000D0 RID: 208
	[Flags]
	public enum RenderTextureCreationFlags
	{
		// Token: 0x040002A4 RID: 676
		MipMap = 1,
		// Token: 0x040002A5 RID: 677
		AutoGenerateMips = 2,
		// Token: 0x040002A6 RID: 678
		SRGB = 4,
		// Token: 0x040002A7 RID: 679
		EyeTexture = 8,
		// Token: 0x040002A8 RID: 680
		EnableRandomWrite = 16,
		// Token: 0x040002A9 RID: 681
		CreatedFromScript = 32,
		// Token: 0x040002AA RID: 682
		AllowVerticalFlip = 128,
		// Token: 0x040002AB RID: 683
		NoResolvedColorSurface = 256,
		// Token: 0x040002AC RID: 684
		DynamicallyScalable = 1024,
		// Token: 0x040002AD RID: 685
		BindMS = 2048
	}
}
