﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000054 RID: 84
	internal class PreloadData : Object
	{
		// Token: 0x0600046E RID: 1134 RVA: 0x0000AD1B File Offset: 0x00008F1B
		public PreloadData()
		{
		}

		// Token: 0x0600046F RID: 1135 RVA: 0x00007476 File Offset: 0x00005676
		[RequiredByNativeCode]
		internal void PreloadDataDontStripMe()
		{
		}
	}
}
