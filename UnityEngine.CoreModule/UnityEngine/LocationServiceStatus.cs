﻿using System;

namespace UnityEngine
{
	// Token: 0x02000129 RID: 297
	public enum LocationServiceStatus
	{
		// Token: 0x04000547 RID: 1351
		Stopped,
		// Token: 0x04000548 RID: 1352
		Initializing,
		// Token: 0x04000549 RID: 1353
		Running,
		// Token: 0x0400054A RID: 1354
		Failed
	}
}
