﻿using System;

namespace UnityEngine
{
	// Token: 0x020000CA RID: 202
	public enum TextureWrapMode
	{
		// Token: 0x0400023A RID: 570
		Repeat,
		// Token: 0x0400023B RID: 571
		Clamp,
		// Token: 0x0400023C RID: 572
		Mirror,
		// Token: 0x0400023D RID: 573
		MirrorOnce
	}
}
