﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000021 RID: 33
	[NativeHeader("Runtime/Misc/AsyncOperation.h")]
	[ThreadAndSerializationSafe]
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Export/AsyncOperation.bindings.h")]
	[StructLayout(LayoutKind.Sequential)]
	public class AsyncOperation : YieldInstruction
	{
		// Token: 0x06000256 RID: 598 RVA: 0x000084D7 File Offset: 0x000066D7
		public AsyncOperation()
		{
		}

		// Token: 0x06000257 RID: 599
		[NativeMethod(IsThreadSafe = true)]
		[StaticAccessor("AsyncOperationBindings", StaticAccessorType.DoubleColon)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalDestroy(IntPtr ptr);

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000258 RID: 600
		public extern bool isDone { [NativeMethod("IsDone")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000259 RID: 601
		public extern float progress { [NativeMethod("GetProgress")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600025A RID: 602
		// (set) Token: 0x0600025B RID: 603
		public extern int priority { [NativeMethod("GetPriority")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetPriority")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x0600025C RID: 604
		// (set) Token: 0x0600025D RID: 605
		public extern bool allowSceneActivation { [NativeMethod("GetAllowSceneActivation")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetAllowSceneActivation")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600025E RID: 606 RVA: 0x000084E0 File Offset: 0x000066E0
		~AsyncOperation()
		{
			AsyncOperation.InternalDestroy(this.m_Ptr);
		}

		// Token: 0x0600025F RID: 607 RVA: 0x00008518 File Offset: 0x00006718
		[RequiredByNativeCode]
		internal void InvokeCompletionEvent()
		{
			if (this.m_completeCallback != null)
			{
				this.m_completeCallback(this);
				this.m_completeCallback = null;
			}
		}

		// Token: 0x14000008 RID: 8
		// (add) Token: 0x06000260 RID: 608 RVA: 0x0000853B File Offset: 0x0000673B
		// (remove) Token: 0x06000261 RID: 609 RVA: 0x00008570 File Offset: 0x00006770
		public event Action<AsyncOperation> completed
		{
			add
			{
				if (this.isDone)
				{
					value(this);
				}
				else
				{
					this.m_completeCallback = (Action<AsyncOperation>)Delegate.Combine(this.m_completeCallback, value);
				}
			}
			remove
			{
				this.m_completeCallback = (Action<AsyncOperation>)Delegate.Remove(this.m_completeCallback, value);
			}
		}

		// Token: 0x04000057 RID: 87
		internal IntPtr m_Ptr;

		// Token: 0x04000058 RID: 88
		private Action<AsyncOperation> m_completeCallback;
	}
}
