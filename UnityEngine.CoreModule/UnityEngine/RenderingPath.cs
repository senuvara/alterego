﻿using System;

namespace UnityEngine
{
	// Token: 0x020000AF RID: 175
	public enum RenderingPath
	{
		// Token: 0x040001AF RID: 431
		UsePlayerSettings = -1,
		// Token: 0x040001B0 RID: 432
		VertexLit,
		// Token: 0x040001B1 RID: 433
		Forward,
		// Token: 0x040001B2 RID: 434
		DeferredLighting,
		// Token: 0x040001B3 RID: 435
		DeferredShading
	}
}
