﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngineInternal;

namespace UnityEngine
{
	// Token: 0x0200014E RID: 334
	[NativeHeader("Runtime/Math/FloatConversion.h")]
	[NativeHeader("Runtime/Math/ColorSpaceConversion.h")]
	[NativeHeader("Runtime/Utilities/BitUtility.h")]
	[NativeHeader("Runtime/Math/PerlinNoise.h")]
	[ThreadAndSerializationSafe]
	public struct Mathf
	{
		// Token: 0x06000DE2 RID: 3554
		[FreeFunction(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int ClosestPowerOfTwo(int value);

		// Token: 0x06000DE3 RID: 3555
		[FreeFunction(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsPowerOfTwo(int value);

		// Token: 0x06000DE4 RID: 3556
		[FreeFunction(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int NextPowerOfTwo(int value);

		// Token: 0x06000DE5 RID: 3557
		[FreeFunction(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GammaToLinearSpace(float value);

		// Token: 0x06000DE6 RID: 3558
		[FreeFunction(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float LinearToGammaSpace(float value);

		// Token: 0x06000DE7 RID: 3559 RVA: 0x00017AA4 File Offset: 0x00015CA4
		[FreeFunction(IsThreadSafe = true)]
		public static Color CorrelatedColorTemperatureToRGB(float kelvin)
		{
			Color result;
			Mathf.CorrelatedColorTemperatureToRGB_Injected(kelvin, out result);
			return result;
		}

		// Token: 0x06000DE8 RID: 3560
		[FreeFunction(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern ushort FloatToHalf(float val);

		// Token: 0x06000DE9 RID: 3561
		[FreeFunction(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float HalfToFloat(ushort val);

		// Token: 0x06000DEA RID: 3562
		[FreeFunction("PerlinNoise::NoiseNormalized", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float PerlinNoise(float x, float y);

		// Token: 0x06000DEB RID: 3563 RVA: 0x00017ABC File Offset: 0x00015CBC
		public static float Sin(float f)
		{
			return (float)Math.Sin((double)f);
		}

		// Token: 0x06000DEC RID: 3564 RVA: 0x00017ADC File Offset: 0x00015CDC
		public static float Cos(float f)
		{
			return (float)Math.Cos((double)f);
		}

		// Token: 0x06000DED RID: 3565 RVA: 0x00017AFC File Offset: 0x00015CFC
		public static float Tan(float f)
		{
			return (float)Math.Tan((double)f);
		}

		// Token: 0x06000DEE RID: 3566 RVA: 0x00017B1C File Offset: 0x00015D1C
		public static float Asin(float f)
		{
			return (float)Math.Asin((double)f);
		}

		// Token: 0x06000DEF RID: 3567 RVA: 0x00017B3C File Offset: 0x00015D3C
		public static float Acos(float f)
		{
			return (float)Math.Acos((double)f);
		}

		// Token: 0x06000DF0 RID: 3568 RVA: 0x00017B5C File Offset: 0x00015D5C
		public static float Atan(float f)
		{
			return (float)Math.Atan((double)f);
		}

		// Token: 0x06000DF1 RID: 3569 RVA: 0x00017B7C File Offset: 0x00015D7C
		public static float Atan2(float y, float x)
		{
			return (float)Math.Atan2((double)y, (double)x);
		}

		// Token: 0x06000DF2 RID: 3570 RVA: 0x00017B9C File Offset: 0x00015D9C
		public static float Sqrt(float f)
		{
			return (float)Math.Sqrt((double)f);
		}

		// Token: 0x06000DF3 RID: 3571 RVA: 0x00017BBC File Offset: 0x00015DBC
		public static float Abs(float f)
		{
			return Math.Abs(f);
		}

		// Token: 0x06000DF4 RID: 3572 RVA: 0x00017BD8 File Offset: 0x00015DD8
		public static int Abs(int value)
		{
			return Math.Abs(value);
		}

		// Token: 0x06000DF5 RID: 3573 RVA: 0x00017BF4 File Offset: 0x00015DF4
		public static float Min(float a, float b)
		{
			return (a >= b) ? b : a;
		}

		// Token: 0x06000DF6 RID: 3574 RVA: 0x00017C18 File Offset: 0x00015E18
		public static float Min(params float[] values)
		{
			int num = values.Length;
			float result;
			if (num == 0)
			{
				result = 0f;
			}
			else
			{
				float num2 = values[0];
				for (int i = 1; i < num; i++)
				{
					if (values[i] < num2)
					{
						num2 = values[i];
					}
				}
				result = num2;
			}
			return result;
		}

		// Token: 0x06000DF7 RID: 3575 RVA: 0x00017C68 File Offset: 0x00015E68
		public static int Min(int a, int b)
		{
			return (a >= b) ? b : a;
		}

		// Token: 0x06000DF8 RID: 3576 RVA: 0x00017C8C File Offset: 0x00015E8C
		public static int Min(params int[] values)
		{
			int num = values.Length;
			int result;
			if (num == 0)
			{
				result = 0;
			}
			else
			{
				int num2 = values[0];
				for (int i = 1; i < num; i++)
				{
					if (values[i] < num2)
					{
						num2 = values[i];
					}
				}
				result = num2;
			}
			return result;
		}

		// Token: 0x06000DF9 RID: 3577 RVA: 0x00017CD8 File Offset: 0x00015ED8
		public static float Max(float a, float b)
		{
			return (a <= b) ? b : a;
		}

		// Token: 0x06000DFA RID: 3578 RVA: 0x00017CFC File Offset: 0x00015EFC
		public static float Max(params float[] values)
		{
			int num = values.Length;
			float result;
			if (num == 0)
			{
				result = 0f;
			}
			else
			{
				float num2 = values[0];
				for (int i = 1; i < num; i++)
				{
					if (values[i] > num2)
					{
						num2 = values[i];
					}
				}
				result = num2;
			}
			return result;
		}

		// Token: 0x06000DFB RID: 3579 RVA: 0x00017D4C File Offset: 0x00015F4C
		public static int Max(int a, int b)
		{
			return (a <= b) ? b : a;
		}

		// Token: 0x06000DFC RID: 3580 RVA: 0x00017D70 File Offset: 0x00015F70
		public static int Max(params int[] values)
		{
			int num = values.Length;
			int result;
			if (num == 0)
			{
				result = 0;
			}
			else
			{
				int num2 = values[0];
				for (int i = 1; i < num; i++)
				{
					if (values[i] > num2)
					{
						num2 = values[i];
					}
				}
				result = num2;
			}
			return result;
		}

		// Token: 0x06000DFD RID: 3581 RVA: 0x00017DBC File Offset: 0x00015FBC
		public static float Pow(float f, float p)
		{
			return (float)Math.Pow((double)f, (double)p);
		}

		// Token: 0x06000DFE RID: 3582 RVA: 0x00017DDC File Offset: 0x00015FDC
		public static float Exp(float power)
		{
			return (float)Math.Exp((double)power);
		}

		// Token: 0x06000DFF RID: 3583 RVA: 0x00017DFC File Offset: 0x00015FFC
		public static float Log(float f, float p)
		{
			return (float)Math.Log((double)f, (double)p);
		}

		// Token: 0x06000E00 RID: 3584 RVA: 0x00017E1C File Offset: 0x0001601C
		public static float Log(float f)
		{
			return (float)Math.Log((double)f);
		}

		// Token: 0x06000E01 RID: 3585 RVA: 0x00017E3C File Offset: 0x0001603C
		public static float Log10(float f)
		{
			return (float)Math.Log10((double)f);
		}

		// Token: 0x06000E02 RID: 3586 RVA: 0x00017E5C File Offset: 0x0001605C
		public static float Ceil(float f)
		{
			return (float)Math.Ceiling((double)f);
		}

		// Token: 0x06000E03 RID: 3587 RVA: 0x00017E7C File Offset: 0x0001607C
		public static float Floor(float f)
		{
			return (float)Math.Floor((double)f);
		}

		// Token: 0x06000E04 RID: 3588 RVA: 0x00017E9C File Offset: 0x0001609C
		public static float Round(float f)
		{
			return (float)Math.Round((double)f);
		}

		// Token: 0x06000E05 RID: 3589 RVA: 0x00017EBC File Offset: 0x000160BC
		public static int CeilToInt(float f)
		{
			return (int)Math.Ceiling((double)f);
		}

		// Token: 0x06000E06 RID: 3590 RVA: 0x00017EDC File Offset: 0x000160DC
		public static int FloorToInt(float f)
		{
			return (int)Math.Floor((double)f);
		}

		// Token: 0x06000E07 RID: 3591 RVA: 0x00017EFC File Offset: 0x000160FC
		public static int RoundToInt(float f)
		{
			return (int)Math.Round((double)f);
		}

		// Token: 0x06000E08 RID: 3592 RVA: 0x00017F1C File Offset: 0x0001611C
		public static float Sign(float f)
		{
			return (f < 0f) ? -1f : 1f;
		}

		// Token: 0x06000E09 RID: 3593 RVA: 0x00017F4C File Offset: 0x0001614C
		public static float Clamp(float value, float min, float max)
		{
			if (value < min)
			{
				value = min;
			}
			else if (value > max)
			{
				value = max;
			}
			return value;
		}

		// Token: 0x06000E0A RID: 3594 RVA: 0x00017F7C File Offset: 0x0001617C
		public static int Clamp(int value, int min, int max)
		{
			if (value < min)
			{
				value = min;
			}
			else if (value > max)
			{
				value = max;
			}
			return value;
		}

		// Token: 0x06000E0B RID: 3595 RVA: 0x00017FAC File Offset: 0x000161AC
		public static float Clamp01(float value)
		{
			float result;
			if (value < 0f)
			{
				result = 0f;
			}
			else if (value > 1f)
			{
				result = 1f;
			}
			else
			{
				result = value;
			}
			return result;
		}

		// Token: 0x06000E0C RID: 3596 RVA: 0x00017FF0 File Offset: 0x000161F0
		public static float Lerp(float a, float b, float t)
		{
			return a + (b - a) * Mathf.Clamp01(t);
		}

		// Token: 0x06000E0D RID: 3597 RVA: 0x00018014 File Offset: 0x00016214
		public static float LerpUnclamped(float a, float b, float t)
		{
			return a + (b - a) * t;
		}

		// Token: 0x06000E0E RID: 3598 RVA: 0x00018030 File Offset: 0x00016230
		public static float LerpAngle(float a, float b, float t)
		{
			float num = Mathf.Repeat(b - a, 360f);
			if (num > 180f)
			{
				num -= 360f;
			}
			return a + num * Mathf.Clamp01(t);
		}

		// Token: 0x06000E0F RID: 3599 RVA: 0x00018070 File Offset: 0x00016270
		public static float MoveTowards(float current, float target, float maxDelta)
		{
			float result;
			if (Mathf.Abs(target - current) <= maxDelta)
			{
				result = target;
			}
			else
			{
				result = current + Mathf.Sign(target - current) * maxDelta;
			}
			return result;
		}

		// Token: 0x06000E10 RID: 3600 RVA: 0x000180A8 File Offset: 0x000162A8
		public static float MoveTowardsAngle(float current, float target, float maxDelta)
		{
			float num = Mathf.DeltaAngle(current, target);
			float result;
			if (-maxDelta < num && num < maxDelta)
			{
				result = target;
			}
			else
			{
				target = current + num;
				result = Mathf.MoveTowards(current, target, maxDelta);
			}
			return result;
		}

		// Token: 0x06000E11 RID: 3601 RVA: 0x000180E8 File Offset: 0x000162E8
		public static float SmoothStep(float from, float to, float t)
		{
			t = Mathf.Clamp01(t);
			t = -2f * t * t * t + 3f * t * t;
			return to * t + from * (1f - t);
		}

		// Token: 0x06000E12 RID: 3602 RVA: 0x0001812C File Offset: 0x0001632C
		public static float Gamma(float value, float absmax, float gamma)
		{
			bool flag = false;
			if (value < 0f)
			{
				flag = true;
			}
			float num = Mathf.Abs(value);
			float result;
			if (num > absmax)
			{
				result = ((!flag) ? num : (-num));
			}
			else
			{
				float num2 = Mathf.Pow(num / absmax, gamma) * absmax;
				result = ((!flag) ? num2 : (-num2));
			}
			return result;
		}

		// Token: 0x06000E13 RID: 3603 RVA: 0x0001818C File Offset: 0x0001638C
		public static bool Approximately(float a, float b)
		{
			return Mathf.Abs(b - a) < Mathf.Max(1E-06f * Mathf.Max(Mathf.Abs(a), Mathf.Abs(b)), Mathf.Epsilon * 8f);
		}

		// Token: 0x06000E14 RID: 3604 RVA: 0x000181D4 File Offset: 0x000163D4
		[ExcludeFromDocs]
		public static float SmoothDamp(float current, float target, ref float currentVelocity, float smoothTime, float maxSpeed)
		{
			float deltaTime = Time.deltaTime;
			return Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
		}

		// Token: 0x06000E15 RID: 3605 RVA: 0x000181FC File Offset: 0x000163FC
		[ExcludeFromDocs]
		public static float SmoothDamp(float current, float target, ref float currentVelocity, float smoothTime)
		{
			float deltaTime = Time.deltaTime;
			float positiveInfinity = float.PositiveInfinity;
			return Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, positiveInfinity, deltaTime);
		}

		// Token: 0x06000E16 RID: 3606 RVA: 0x00018228 File Offset: 0x00016428
		public static float SmoothDamp(float current, float target, ref float currentVelocity, float smoothTime, [DefaultValue("Mathf.Infinity")] float maxSpeed, [DefaultValue("Time.deltaTime")] float deltaTime)
		{
			smoothTime = Mathf.Max(0.0001f, smoothTime);
			float num = 2f / smoothTime;
			float num2 = num * deltaTime;
			float num3 = 1f / (1f + num2 + 0.48f * num2 * num2 + 0.235f * num2 * num2 * num2);
			float num4 = current - target;
			float num5 = target;
			float num6 = maxSpeed * smoothTime;
			num4 = Mathf.Clamp(num4, -num6, num6);
			target = current - num4;
			float num7 = (currentVelocity + num * num4) * deltaTime;
			currentVelocity = (currentVelocity - num * num7) * num3;
			float num8 = target + (num4 + num7) * num3;
			if (num5 - current > 0f == num8 > num5)
			{
				num8 = num5;
				currentVelocity = (num8 - num5) / deltaTime;
			}
			return num8;
		}

		// Token: 0x06000E17 RID: 3607 RVA: 0x000182E4 File Offset: 0x000164E4
		[ExcludeFromDocs]
		public static float SmoothDampAngle(float current, float target, ref float currentVelocity, float smoothTime, float maxSpeed)
		{
			float deltaTime = Time.deltaTime;
			return Mathf.SmoothDampAngle(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
		}

		// Token: 0x06000E18 RID: 3608 RVA: 0x0001830C File Offset: 0x0001650C
		[ExcludeFromDocs]
		public static float SmoothDampAngle(float current, float target, ref float currentVelocity, float smoothTime)
		{
			float deltaTime = Time.deltaTime;
			float positiveInfinity = float.PositiveInfinity;
			return Mathf.SmoothDampAngle(current, target, ref currentVelocity, smoothTime, positiveInfinity, deltaTime);
		}

		// Token: 0x06000E19 RID: 3609 RVA: 0x00018338 File Offset: 0x00016538
		public static float SmoothDampAngle(float current, float target, ref float currentVelocity, float smoothTime, [DefaultValue("Mathf.Infinity")] float maxSpeed, [DefaultValue("Time.deltaTime")] float deltaTime)
		{
			target = current + Mathf.DeltaAngle(current, target);
			return Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
		}

		// Token: 0x06000E1A RID: 3610 RVA: 0x00018368 File Offset: 0x00016568
		public static float Repeat(float t, float length)
		{
			return Mathf.Clamp(t - Mathf.Floor(t / length) * length, 0f, length);
		}

		// Token: 0x06000E1B RID: 3611 RVA: 0x00018394 File Offset: 0x00016594
		public static float PingPong(float t, float length)
		{
			t = Mathf.Repeat(t, length * 2f);
			return length - Mathf.Abs(t - length);
		}

		// Token: 0x06000E1C RID: 3612 RVA: 0x000183C4 File Offset: 0x000165C4
		public static float InverseLerp(float a, float b, float value)
		{
			float result;
			if (a != b)
			{
				result = Mathf.Clamp01((value - a) / (b - a));
			}
			else
			{
				result = 0f;
			}
			return result;
		}

		// Token: 0x06000E1D RID: 3613 RVA: 0x000183F8 File Offset: 0x000165F8
		public static float DeltaAngle(float current, float target)
		{
			float num = Mathf.Repeat(target - current, 360f);
			if (num > 180f)
			{
				num -= 360f;
			}
			return num;
		}

		// Token: 0x06000E1E RID: 3614 RVA: 0x00018430 File Offset: 0x00016630
		internal static bool LineIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, ref Vector2 result)
		{
			float num = p2.x - p1.x;
			float num2 = p2.y - p1.y;
			float num3 = p4.x - p3.x;
			float num4 = p4.y - p3.y;
			float num5 = num * num4 - num2 * num3;
			bool result2;
			if (num5 == 0f)
			{
				result2 = false;
			}
			else
			{
				float num6 = p3.x - p1.x;
				float num7 = p3.y - p1.y;
				float num8 = (num6 * num4 - num7 * num3) / num5;
				result = new Vector2(p1.x + num8 * num, p1.y + num8 * num2);
				result2 = true;
			}
			return result2;
		}

		// Token: 0x06000E1F RID: 3615 RVA: 0x000184F8 File Offset: 0x000166F8
		internal static bool LineSegmentIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, ref Vector2 result)
		{
			float num = p2.x - p1.x;
			float num2 = p2.y - p1.y;
			float num3 = p4.x - p3.x;
			float num4 = p4.y - p3.y;
			float num5 = num * num4 - num2 * num3;
			bool result2;
			if (num5 == 0f)
			{
				result2 = false;
			}
			else
			{
				float num6 = p3.x - p1.x;
				float num7 = p3.y - p1.y;
				float num8 = (num6 * num4 - num7 * num3) / num5;
				if (num8 < 0f || num8 > 1f)
				{
					result2 = false;
				}
				else
				{
					float num9 = (num6 * num2 - num7 * num) / num5;
					if (num9 < 0f || num9 > 1f)
					{
						result2 = false;
					}
					else
					{
						result = new Vector2(p1.x + num8 * num, p1.y + num8 * num2);
						result2 = true;
					}
				}
			}
			return result2;
		}

		// Token: 0x06000E20 RID: 3616 RVA: 0x00018610 File Offset: 0x00016810
		internal static long RandomToLong(Random r)
		{
			byte[] array = new byte[8];
			r.NextBytes(array);
			return (long)(BitConverter.ToUInt64(array, 0) & 9223372036854775807UL);
		}

		// Token: 0x06000E21 RID: 3617 RVA: 0x00018644 File Offset: 0x00016844
		// Note: this type is marked as 'beforefieldinit'.
		static Mathf()
		{
		}

		// Token: 0x06000E22 RID: 3618
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CorrelatedColorTemperatureToRGB_Injected(float kelvin, out Color ret);

		// Token: 0x040006E6 RID: 1766
		public const float PI = 3.1415927f;

		// Token: 0x040006E7 RID: 1767
		public const float Infinity = float.PositiveInfinity;

		// Token: 0x040006E8 RID: 1768
		public const float NegativeInfinity = float.NegativeInfinity;

		// Token: 0x040006E9 RID: 1769
		public const float Deg2Rad = 0.017453292f;

		// Token: 0x040006EA RID: 1770
		public const float Rad2Deg = 57.29578f;

		// Token: 0x040006EB RID: 1771
		public static readonly float Epsilon = (!MathfInternal.IsFlushToZeroEnabled) ? MathfInternal.FloatMinDenormal : MathfInternal.FloatMinNormal;
	}
}
