﻿using System;

namespace UnityEngine
{
	// Token: 0x020002B8 RID: 696
	public interface ILogHandler
	{
		// Token: 0x060018BD RID: 6333
		void LogFormat(LogType logType, Object context, string format, params object[] args);

		// Token: 0x060018BE RID: 6334
		void LogException(Exception exception, Object context);
	}
}
