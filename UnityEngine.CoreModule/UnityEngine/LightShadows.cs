﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B6 RID: 182
	public enum LightShadows
	{
		// Token: 0x040001D8 RID: 472
		None,
		// Token: 0x040001D9 RID: 473
		Hard,
		// Token: 0x040001DA RID: 474
		Soft
	}
}
