﻿using System;

namespace UnityEngine
{
	// Token: 0x0200007E RID: 126
	public enum GradientMode
	{
		// Token: 0x04000156 RID: 342
		Blend,
		// Token: 0x04000157 RID: 343
		Fixed
	}
}
