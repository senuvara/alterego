﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200020A RID: 522
	[RequiredByNativeCode]
	public sealed class SerializeField : Attribute
	{
		// Token: 0x060011A7 RID: 4519 RVA: 0x0000898B File Offset: 0x00006B8B
		public SerializeField()
		{
		}
	}
}
