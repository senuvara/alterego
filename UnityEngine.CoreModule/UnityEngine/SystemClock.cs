﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200021F RID: 543
	[VisibleToOtherModules]
	internal class SystemClock
	{
		// Token: 0x06001235 RID: 4661 RVA: 0x00002370 File Offset: 0x00000570
		public SystemClock()
		{
		}

		// Token: 0x17000345 RID: 837
		// (get) Token: 0x06001236 RID: 4662 RVA: 0x00020940 File Offset: 0x0001EB40
		public static DateTime now
		{
			get
			{
				return DateTime.Now;
			}
		}

		// Token: 0x06001237 RID: 4663 RVA: 0x0002095C File Offset: 0x0001EB5C
		public static long ToUnixTimeMilliseconds(DateTime date)
		{
			return Convert.ToInt64((date.ToUniversalTime() - SystemClock.s_Epoch).TotalMilliseconds);
		}

		// Token: 0x06001238 RID: 4664 RVA: 0x00020990 File Offset: 0x0001EB90
		public static long ToUnixTimeSeconds(DateTime date)
		{
			return Convert.ToInt64((date.ToUniversalTime() - SystemClock.s_Epoch).TotalSeconds);
		}

		// Token: 0x06001239 RID: 4665 RVA: 0x000209C3 File Offset: 0x0001EBC3
		// Note: this type is marked as 'beforefieldinit'.
		static SystemClock()
		{
		}

		// Token: 0x04000795 RID: 1941
		private static readonly DateTime s_Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
	}
}
