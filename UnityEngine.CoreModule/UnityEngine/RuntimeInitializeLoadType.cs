﻿using System;

namespace UnityEngine
{
	// Token: 0x02000201 RID: 513
	public enum RuntimeInitializeLoadType
	{
		// Token: 0x0400074E RID: 1870
		AfterSceneLoad,
		// Token: 0x0400074F RID: 1871
		BeforeSceneLoad
	}
}
