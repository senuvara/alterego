﻿using System;

namespace UnityEngine
{
	// Token: 0x020000BD RID: 189
	public enum ShadowResolution
	{
		// Token: 0x040001F6 RID: 502
		Low,
		// Token: 0x040001F7 RID: 503
		Medium,
		// Token: 0x040001F8 RID: 504
		High,
		// Token: 0x040001F9 RID: 505
		VeryHigh
	}
}
