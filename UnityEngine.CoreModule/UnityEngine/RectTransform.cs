﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200034B RID: 843
	[NativeHeader("Runtime/Transform/RectTransform.h")]
	[NativeClass("UI::RectTransform")]
	public sealed class RectTransform : Transform
	{
		// Token: 0x06001C3A RID: 7226 RVA: 0x00031A17 File Offset: 0x0002FC17
		public RectTransform()
		{
		}

		// Token: 0x14000019 RID: 25
		// (add) Token: 0x06001C3B RID: 7227 RVA: 0x00031A20 File Offset: 0x0002FC20
		// (remove) Token: 0x06001C3C RID: 7228 RVA: 0x00031A54 File Offset: 0x0002FC54
		public static event RectTransform.ReapplyDrivenProperties reapplyDrivenProperties
		{
			add
			{
				RectTransform.ReapplyDrivenProperties reapplyDrivenProperties = RectTransform.reapplyDrivenProperties;
				RectTransform.ReapplyDrivenProperties reapplyDrivenProperties2;
				do
				{
					reapplyDrivenProperties2 = reapplyDrivenProperties;
					reapplyDrivenProperties = Interlocked.CompareExchange<RectTransform.ReapplyDrivenProperties>(ref RectTransform.reapplyDrivenProperties, (RectTransform.ReapplyDrivenProperties)Delegate.Combine(reapplyDrivenProperties2, value), reapplyDrivenProperties);
				}
				while (reapplyDrivenProperties != reapplyDrivenProperties2);
			}
			remove
			{
				RectTransform.ReapplyDrivenProperties reapplyDrivenProperties = RectTransform.reapplyDrivenProperties;
				RectTransform.ReapplyDrivenProperties reapplyDrivenProperties2;
				do
				{
					reapplyDrivenProperties2 = reapplyDrivenProperties;
					reapplyDrivenProperties = Interlocked.CompareExchange<RectTransform.ReapplyDrivenProperties>(ref RectTransform.reapplyDrivenProperties, (RectTransform.ReapplyDrivenProperties)Delegate.Remove(reapplyDrivenProperties2, value), reapplyDrivenProperties);
				}
				while (reapplyDrivenProperties != reapplyDrivenProperties2);
			}
		}

		// Token: 0x17000528 RID: 1320
		// (get) Token: 0x06001C3D RID: 7229 RVA: 0x00031A88 File Offset: 0x0002FC88
		public Rect rect
		{
			get
			{
				Rect result;
				this.get_rect_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000529 RID: 1321
		// (get) Token: 0x06001C3E RID: 7230 RVA: 0x00031AA0 File Offset: 0x0002FCA0
		// (set) Token: 0x06001C3F RID: 7231 RVA: 0x00031AB6 File Offset: 0x0002FCB6
		public Vector2 anchorMin
		{
			get
			{
				Vector2 result;
				this.get_anchorMin_Injected(out result);
				return result;
			}
			set
			{
				this.set_anchorMin_Injected(ref value);
			}
		}

		// Token: 0x1700052A RID: 1322
		// (get) Token: 0x06001C40 RID: 7232 RVA: 0x00031AC0 File Offset: 0x0002FCC0
		// (set) Token: 0x06001C41 RID: 7233 RVA: 0x00031AD6 File Offset: 0x0002FCD6
		public Vector2 anchorMax
		{
			get
			{
				Vector2 result;
				this.get_anchorMax_Injected(out result);
				return result;
			}
			set
			{
				this.set_anchorMax_Injected(ref value);
			}
		}

		// Token: 0x1700052B RID: 1323
		// (get) Token: 0x06001C42 RID: 7234 RVA: 0x00031AE0 File Offset: 0x0002FCE0
		// (set) Token: 0x06001C43 RID: 7235 RVA: 0x00031AF6 File Offset: 0x0002FCF6
		public Vector2 anchoredPosition
		{
			get
			{
				Vector2 result;
				this.get_anchoredPosition_Injected(out result);
				return result;
			}
			set
			{
				this.set_anchoredPosition_Injected(ref value);
			}
		}

		// Token: 0x1700052C RID: 1324
		// (get) Token: 0x06001C44 RID: 7236 RVA: 0x00031B00 File Offset: 0x0002FD00
		// (set) Token: 0x06001C45 RID: 7237 RVA: 0x00031B16 File Offset: 0x0002FD16
		public Vector2 sizeDelta
		{
			get
			{
				Vector2 result;
				this.get_sizeDelta_Injected(out result);
				return result;
			}
			set
			{
				this.set_sizeDelta_Injected(ref value);
			}
		}

		// Token: 0x1700052D RID: 1325
		// (get) Token: 0x06001C46 RID: 7238 RVA: 0x00031B20 File Offset: 0x0002FD20
		// (set) Token: 0x06001C47 RID: 7239 RVA: 0x00031B36 File Offset: 0x0002FD36
		public Vector2 pivot
		{
			get
			{
				Vector2 result;
				this.get_pivot_Injected(out result);
				return result;
			}
			set
			{
				this.set_pivot_Injected(ref value);
			}
		}

		// Token: 0x1700052E RID: 1326
		// (get) Token: 0x06001C48 RID: 7240 RVA: 0x00031B40 File Offset: 0x0002FD40
		// (set) Token: 0x06001C49 RID: 7241 RVA: 0x00031B80 File Offset: 0x0002FD80
		public Vector3 anchoredPosition3D
		{
			get
			{
				Vector2 anchoredPosition = this.anchoredPosition;
				return new Vector3(anchoredPosition.x, anchoredPosition.y, base.localPosition.z);
			}
			set
			{
				this.anchoredPosition = new Vector2(value.x, value.y);
				Vector3 localPosition = base.localPosition;
				localPosition.z = value.z;
				base.localPosition = localPosition;
			}
		}

		// Token: 0x1700052F RID: 1327
		// (get) Token: 0x06001C4A RID: 7242 RVA: 0x00031BC4 File Offset: 0x0002FDC4
		// (set) Token: 0x06001C4B RID: 7243 RVA: 0x00031BF8 File Offset: 0x0002FDF8
		public Vector2 offsetMin
		{
			get
			{
				return this.anchoredPosition - Vector2.Scale(this.sizeDelta, this.pivot);
			}
			set
			{
				Vector2 vector = value - (this.anchoredPosition - Vector2.Scale(this.sizeDelta, this.pivot));
				this.sizeDelta -= vector;
				this.anchoredPosition += Vector2.Scale(vector, Vector2.one - this.pivot);
			}
		}

		// Token: 0x17000530 RID: 1328
		// (get) Token: 0x06001C4C RID: 7244 RVA: 0x00031C64 File Offset: 0x0002FE64
		// (set) Token: 0x06001C4D RID: 7245 RVA: 0x00031CA0 File Offset: 0x0002FEA0
		public Vector2 offsetMax
		{
			get
			{
				return this.anchoredPosition + Vector2.Scale(this.sizeDelta, Vector2.one - this.pivot);
			}
			set
			{
				Vector2 vector = value - (this.anchoredPosition + Vector2.Scale(this.sizeDelta, Vector2.one - this.pivot));
				this.sizeDelta += vector;
				this.anchoredPosition += Vector2.Scale(vector, this.pivot);
			}
		}

		// Token: 0x17000531 RID: 1329
		// (get) Token: 0x06001C4E RID: 7246
		// (set) Token: 0x06001C4F RID: 7247
		internal extern Object drivenByObject { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000532 RID: 1330
		// (get) Token: 0x06001C50 RID: 7248
		// (set) Token: 0x06001C51 RID: 7249
		internal extern DrivenTransformProperties drivenProperties { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001C52 RID: 7250
		[NativeMethod("UpdateIfTransformDispatchIsDirty")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ForceUpdateRectTransforms();

		// Token: 0x06001C53 RID: 7251 RVA: 0x00031D0C File Offset: 0x0002FF0C
		public void GetLocalCorners(Vector3[] fourCornersArray)
		{
			if (fourCornersArray == null || fourCornersArray.Length < 4)
			{
				Debug.LogError("Calling GetLocalCorners with an array that is null or has less than 4 elements.");
			}
			else
			{
				Rect rect = this.rect;
				float x = rect.x;
				float y = rect.y;
				float xMax = rect.xMax;
				float yMax = rect.yMax;
				fourCornersArray[0] = new Vector3(x, y, 0f);
				fourCornersArray[1] = new Vector3(x, yMax, 0f);
				fourCornersArray[2] = new Vector3(xMax, yMax, 0f);
				fourCornersArray[3] = new Vector3(xMax, y, 0f);
			}
		}

		// Token: 0x06001C54 RID: 7252 RVA: 0x00031DC4 File Offset: 0x0002FFC4
		public void GetWorldCorners(Vector3[] fourCornersArray)
		{
			if (fourCornersArray == null || fourCornersArray.Length < 4)
			{
				Debug.LogError("Calling GetWorldCorners with an array that is null or has less than 4 elements.");
			}
			else
			{
				this.GetLocalCorners(fourCornersArray);
				Matrix4x4 localToWorldMatrix = base.transform.localToWorldMatrix;
				for (int i = 0; i < 4; i++)
				{
					fourCornersArray[i] = localToWorldMatrix.MultiplyPoint(fourCornersArray[i]);
				}
			}
		}

		// Token: 0x06001C55 RID: 7253 RVA: 0x00031E38 File Offset: 0x00030038
		public void SetInsetAndSizeFromParentEdge(RectTransform.Edge edge, float inset, float size)
		{
			int index = (edge != RectTransform.Edge.Top && edge != RectTransform.Edge.Bottom) ? 0 : 1;
			bool flag = edge == RectTransform.Edge.Top || edge == RectTransform.Edge.Right;
			float value = (float)((!flag) ? 0 : 1);
			Vector2 vector = this.anchorMin;
			vector[index] = value;
			this.anchorMin = vector;
			vector = this.anchorMax;
			vector[index] = value;
			this.anchorMax = vector;
			Vector2 sizeDelta = this.sizeDelta;
			sizeDelta[index] = size;
			this.sizeDelta = sizeDelta;
			Vector2 anchoredPosition = this.anchoredPosition;
			anchoredPosition[index] = ((!flag) ? (inset + size * this.pivot[index]) : (-inset - size * (1f - this.pivot[index])));
			this.anchoredPosition = anchoredPosition;
		}

		// Token: 0x06001C56 RID: 7254 RVA: 0x00031F14 File Offset: 0x00030114
		public void SetSizeWithCurrentAnchors(RectTransform.Axis axis, float size)
		{
			Vector2 sizeDelta = this.sizeDelta;
			sizeDelta[(int)axis] = size - this.GetParentSize()[(int)axis] * (this.anchorMax[(int)axis] - this.anchorMin[(int)axis]);
			this.sizeDelta = sizeDelta;
		}

		// Token: 0x06001C57 RID: 7255 RVA: 0x00031F6C File Offset: 0x0003016C
		[RequiredByNativeCode]
		internal static void SendReapplyDrivenProperties(RectTransform driven)
		{
			if (RectTransform.reapplyDrivenProperties != null)
			{
				RectTransform.reapplyDrivenProperties(driven);
			}
		}

		// Token: 0x06001C58 RID: 7256 RVA: 0x00031F88 File Offset: 0x00030188
		internal Rect GetRectInParentSpace()
		{
			Rect rect = this.rect;
			Vector2 a = this.offsetMin + Vector2.Scale(this.pivot, rect.size);
			if (base.transform.parent)
			{
				RectTransform component = base.transform.parent.GetComponent<RectTransform>();
				if (component)
				{
					a += Vector2.Scale(this.anchorMin, component.rect.size);
				}
			}
			rect.x += a.x;
			rect.y += a.y;
			return rect;
		}

		// Token: 0x06001C59 RID: 7257 RVA: 0x00032044 File Offset: 0x00030244
		private Vector2 GetParentSize()
		{
			RectTransform rectTransform = base.parent as RectTransform;
			Vector2 result;
			if (!rectTransform)
			{
				result = Vector2.zero;
			}
			else
			{
				result = rectTransform.rect.size;
			}
			return result;
		}

		// Token: 0x06001C5A RID: 7258
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_rect_Injected(out Rect ret);

		// Token: 0x06001C5B RID: 7259
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_anchorMin_Injected(out Vector2 ret);

		// Token: 0x06001C5C RID: 7260
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_anchorMin_Injected(ref Vector2 value);

		// Token: 0x06001C5D RID: 7261
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_anchorMax_Injected(out Vector2 ret);

		// Token: 0x06001C5E RID: 7262
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_anchorMax_Injected(ref Vector2 value);

		// Token: 0x06001C5F RID: 7263
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_anchoredPosition_Injected(out Vector2 ret);

		// Token: 0x06001C60 RID: 7264
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_anchoredPosition_Injected(ref Vector2 value);

		// Token: 0x06001C61 RID: 7265
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_sizeDelta_Injected(out Vector2 ret);

		// Token: 0x06001C62 RID: 7266
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_sizeDelta_Injected(ref Vector2 value);

		// Token: 0x06001C63 RID: 7267
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_pivot_Injected(out Vector2 ret);

		// Token: 0x06001C64 RID: 7268
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_pivot_Injected(ref Vector2 value);

		// Token: 0x04000AC2 RID: 2754
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static RectTransform.ReapplyDrivenProperties reapplyDrivenProperties;

		// Token: 0x0200034C RID: 844
		public enum Edge
		{
			// Token: 0x04000AC4 RID: 2756
			Left,
			// Token: 0x04000AC5 RID: 2757
			Right,
			// Token: 0x04000AC6 RID: 2758
			Top,
			// Token: 0x04000AC7 RID: 2759
			Bottom
		}

		// Token: 0x0200034D RID: 845
		public enum Axis
		{
			// Token: 0x04000AC9 RID: 2761
			Horizontal,
			// Token: 0x04000ACA RID: 2762
			Vertical
		}

		// Token: 0x0200034E RID: 846
		// (Invoke) Token: 0x06001C66 RID: 7270
		public delegate void ReapplyDrivenProperties(RectTransform driven);
	}
}
