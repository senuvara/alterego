﻿using System;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000255 RID: 597
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	[NativeClass("Vector2f")]
	public struct Vector2 : IEquatable<Vector2>
	{
		// Token: 0x06001572 RID: 5490 RVA: 0x000252C8 File Offset: 0x000234C8
		public Vector2(float x, float y)
		{
			this.x = x;
			this.y = y;
		}

		// Token: 0x17000415 RID: 1045
		public float this[int index]
		{
			get
			{
				float result;
				if (index != 0)
				{
					if (index != 1)
					{
						throw new IndexOutOfRangeException("Invalid Vector2 index!");
					}
					result = this.y;
				}
				else
				{
					result = this.x;
				}
				return result;
			}
			set
			{
				if (index != 0)
				{
					if (index != 1)
					{
						throw new IndexOutOfRangeException("Invalid Vector2 index!");
					}
					this.y = value;
				}
				else
				{
					this.x = value;
				}
			}
		}

		// Token: 0x06001575 RID: 5493 RVA: 0x000252C8 File Offset: 0x000234C8
		public void Set(float newX, float newY)
		{
			this.x = newX;
			this.y = newY;
		}

		// Token: 0x06001576 RID: 5494 RVA: 0x00025358 File Offset: 0x00023558
		public static Vector2 Lerp(Vector2 a, Vector2 b, float t)
		{
			t = Mathf.Clamp01(t);
			return new Vector2(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t);
		}

		// Token: 0x06001577 RID: 5495 RVA: 0x000253AC File Offset: 0x000235AC
		public static Vector2 LerpUnclamped(Vector2 a, Vector2 b, float t)
		{
			return new Vector2(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t);
		}

		// Token: 0x06001578 RID: 5496 RVA: 0x000253F8 File Offset: 0x000235F8
		public static Vector2 MoveTowards(Vector2 current, Vector2 target, float maxDistanceDelta)
		{
			Vector2 a = target - current;
			float magnitude = a.magnitude;
			Vector2 result;
			if (magnitude <= maxDistanceDelta || magnitude == 0f)
			{
				result = target;
			}
			else
			{
				result = current + a / magnitude * maxDistanceDelta;
			}
			return result;
		}

		// Token: 0x06001579 RID: 5497 RVA: 0x0002544C File Offset: 0x0002364C
		public static Vector2 Scale(Vector2 a, Vector2 b)
		{
			return new Vector2(a.x * b.x, a.y * b.y);
		}

		// Token: 0x0600157A RID: 5498 RVA: 0x00025484 File Offset: 0x00023684
		public void Scale(Vector2 scale)
		{
			this.x *= scale.x;
			this.y *= scale.y;
		}

		// Token: 0x0600157B RID: 5499 RVA: 0x000254B0 File Offset: 0x000236B0
		public void Normalize()
		{
			float magnitude = this.magnitude;
			if (magnitude > 1E-05f)
			{
				this /= magnitude;
			}
			else
			{
				this = Vector2.zero;
			}
		}

		// Token: 0x17000416 RID: 1046
		// (get) Token: 0x0600157C RID: 5500 RVA: 0x000254F4 File Offset: 0x000236F4
		public Vector2 normalized
		{
			get
			{
				Vector2 result = new Vector2(this.x, this.y);
				result.Normalize();
				return result;
			}
		}

		// Token: 0x0600157D RID: 5501 RVA: 0x00025524 File Offset: 0x00023724
		public override string ToString()
		{
			return UnityString.Format("({0:F1}, {1:F1})", new object[]
			{
				this.x,
				this.y
			});
		}

		// Token: 0x0600157E RID: 5502 RVA: 0x00025568 File Offset: 0x00023768
		public string ToString(string format)
		{
			return UnityString.Format("({0}, {1})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format)
			});
		}

		// Token: 0x0600157F RID: 5503 RVA: 0x000255AC File Offset: 0x000237AC
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.y.GetHashCode() << 2;
		}

		// Token: 0x06001580 RID: 5504 RVA: 0x000255E8 File Offset: 0x000237E8
		public override bool Equals(object other)
		{
			return other is Vector2 && this.Equals((Vector2)other);
		}

		// Token: 0x06001581 RID: 5505 RVA: 0x0002561C File Offset: 0x0002381C
		public bool Equals(Vector2 other)
		{
			return this.x.Equals(other.x) && this.y.Equals(other.y);
		}

		// Token: 0x06001582 RID: 5506 RVA: 0x00025660 File Offset: 0x00023860
		public static Vector2 Reflect(Vector2 inDirection, Vector2 inNormal)
		{
			return -2f * Vector2.Dot(inNormal, inDirection) * inNormal + inDirection;
		}

		// Token: 0x06001583 RID: 5507 RVA: 0x00025690 File Offset: 0x00023890
		public static Vector2 Perpendicular(Vector2 inDirection)
		{
			return new Vector2(-inDirection.y, inDirection.x);
		}

		// Token: 0x06001584 RID: 5508 RVA: 0x000256BC File Offset: 0x000238BC
		public static float Dot(Vector2 lhs, Vector2 rhs)
		{
			return lhs.x * rhs.x + lhs.y * rhs.y;
		}

		// Token: 0x17000417 RID: 1047
		// (get) Token: 0x06001585 RID: 5509 RVA: 0x000256F0 File Offset: 0x000238F0
		public float magnitude
		{
			get
			{
				return Mathf.Sqrt(this.x * this.x + this.y * this.y);
			}
		}

		// Token: 0x17000418 RID: 1048
		// (get) Token: 0x06001586 RID: 5510 RVA: 0x00025728 File Offset: 0x00023928
		public float sqrMagnitude
		{
			get
			{
				return this.x * this.x + this.y * this.y;
			}
		}

		// Token: 0x06001587 RID: 5511 RVA: 0x00025758 File Offset: 0x00023958
		public static float Angle(Vector2 from, Vector2 to)
		{
			float num = Mathf.Sqrt(from.sqrMagnitude * to.sqrMagnitude);
			float result;
			if (num < 1E-15f)
			{
				result = 0f;
			}
			else
			{
				float f = Mathf.Clamp(Vector2.Dot(from, to) / num, -1f, 1f);
				result = Mathf.Acos(f) * 57.29578f;
			}
			return result;
		}

		// Token: 0x06001588 RID: 5512 RVA: 0x000257C0 File Offset: 0x000239C0
		public static float SignedAngle(Vector2 from, Vector2 to)
		{
			float num = Vector2.Angle(from, to);
			float num2 = Mathf.Sign(from.x * to.y - from.y * to.x);
			return num * num2;
		}

		// Token: 0x06001589 RID: 5513 RVA: 0x00025808 File Offset: 0x00023A08
		public static float Distance(Vector2 a, Vector2 b)
		{
			return (a - b).magnitude;
		}

		// Token: 0x0600158A RID: 5514 RVA: 0x0002582C File Offset: 0x00023A2C
		public static Vector2 ClampMagnitude(Vector2 vector, float maxLength)
		{
			Vector2 result;
			if (vector.sqrMagnitude > maxLength * maxLength)
			{
				result = vector.normalized * maxLength;
			}
			else
			{
				result = vector;
			}
			return result;
		}

		// Token: 0x0600158B RID: 5515 RVA: 0x00025864 File Offset: 0x00023A64
		public static float SqrMagnitude(Vector2 a)
		{
			return a.x * a.x + a.y * a.y;
		}

		// Token: 0x0600158C RID: 5516 RVA: 0x00025898 File Offset: 0x00023A98
		public float SqrMagnitude()
		{
			return this.x * this.x + this.y * this.y;
		}

		// Token: 0x0600158D RID: 5517 RVA: 0x000258C8 File Offset: 0x00023AC8
		public static Vector2 Min(Vector2 lhs, Vector2 rhs)
		{
			return new Vector2(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y));
		}

		// Token: 0x0600158E RID: 5518 RVA: 0x00025908 File Offset: 0x00023B08
		public static Vector2 Max(Vector2 lhs, Vector2 rhs)
		{
			return new Vector2(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y));
		}

		// Token: 0x0600158F RID: 5519 RVA: 0x00025948 File Offset: 0x00023B48
		[ExcludeFromDocs]
		public static Vector2 SmoothDamp(Vector2 current, Vector2 target, ref Vector2 currentVelocity, float smoothTime, float maxSpeed)
		{
			float deltaTime = Time.deltaTime;
			return Vector2.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
		}

		// Token: 0x06001590 RID: 5520 RVA: 0x00025970 File Offset: 0x00023B70
		[ExcludeFromDocs]
		public static Vector2 SmoothDamp(Vector2 current, Vector2 target, ref Vector2 currentVelocity, float smoothTime)
		{
			float deltaTime = Time.deltaTime;
			float positiveInfinity = float.PositiveInfinity;
			return Vector2.SmoothDamp(current, target, ref currentVelocity, smoothTime, positiveInfinity, deltaTime);
		}

		// Token: 0x06001591 RID: 5521 RVA: 0x0002599C File Offset: 0x00023B9C
		public static Vector2 SmoothDamp(Vector2 current, Vector2 target, ref Vector2 currentVelocity, float smoothTime, [DefaultValue("Mathf.Infinity")] float maxSpeed, [DefaultValue("Time.deltaTime")] float deltaTime)
		{
			smoothTime = Mathf.Max(0.0001f, smoothTime);
			float num = 2f / smoothTime;
			float num2 = num * deltaTime;
			float d = 1f / (1f + num2 + 0.48f * num2 * num2 + 0.235f * num2 * num2 * num2);
			Vector2 vector = current - target;
			Vector2 vector2 = target;
			float maxLength = maxSpeed * smoothTime;
			vector = Vector2.ClampMagnitude(vector, maxLength);
			target = current - vector;
			Vector2 vector3 = (currentVelocity + num * vector) * deltaTime;
			currentVelocity = (currentVelocity - num * vector3) * d;
			Vector2 vector4 = target + (vector + vector3) * d;
			if (Vector2.Dot(vector2 - current, vector4 - vector2) > 0f)
			{
				vector4 = vector2;
				currentVelocity = (vector4 - vector2) / deltaTime;
			}
			return vector4;
		}

		// Token: 0x06001592 RID: 5522 RVA: 0x00025AA4 File Offset: 0x00023CA4
		public static Vector2 operator +(Vector2 a, Vector2 b)
		{
			return new Vector2(a.x + b.x, a.y + b.y);
		}

		// Token: 0x06001593 RID: 5523 RVA: 0x00025ADC File Offset: 0x00023CDC
		public static Vector2 operator -(Vector2 a, Vector2 b)
		{
			return new Vector2(a.x - b.x, a.y - b.y);
		}

		// Token: 0x06001594 RID: 5524 RVA: 0x00025B14 File Offset: 0x00023D14
		public static Vector2 operator *(Vector2 a, Vector2 b)
		{
			return new Vector2(a.x * b.x, a.y * b.y);
		}

		// Token: 0x06001595 RID: 5525 RVA: 0x00025B4C File Offset: 0x00023D4C
		public static Vector2 operator /(Vector2 a, Vector2 b)
		{
			return new Vector2(a.x / b.x, a.y / b.y);
		}

		// Token: 0x06001596 RID: 5526 RVA: 0x00025B84 File Offset: 0x00023D84
		public static Vector2 operator -(Vector2 a)
		{
			return new Vector2(-a.x, -a.y);
		}

		// Token: 0x06001597 RID: 5527 RVA: 0x00025BB0 File Offset: 0x00023DB0
		public static Vector2 operator *(Vector2 a, float d)
		{
			return new Vector2(a.x * d, a.y * d);
		}

		// Token: 0x06001598 RID: 5528 RVA: 0x00025BDC File Offset: 0x00023DDC
		public static Vector2 operator *(float d, Vector2 a)
		{
			return new Vector2(a.x * d, a.y * d);
		}

		// Token: 0x06001599 RID: 5529 RVA: 0x00025C08 File Offset: 0x00023E08
		public static Vector2 operator /(Vector2 a, float d)
		{
			return new Vector2(a.x / d, a.y / d);
		}

		// Token: 0x0600159A RID: 5530 RVA: 0x00025C34 File Offset: 0x00023E34
		public static bool operator ==(Vector2 lhs, Vector2 rhs)
		{
			return (lhs - rhs).sqrMagnitude < 9.9999994E-11f;
		}

		// Token: 0x0600159B RID: 5531 RVA: 0x00025C60 File Offset: 0x00023E60
		public static bool operator !=(Vector2 lhs, Vector2 rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x0600159C RID: 5532 RVA: 0x00025C80 File Offset: 0x00023E80
		public static implicit operator Vector2(Vector3 v)
		{
			return new Vector2(v.x, v.y);
		}

		// Token: 0x0600159D RID: 5533 RVA: 0x00025CA8 File Offset: 0x00023EA8
		public static implicit operator Vector3(Vector2 v)
		{
			return new Vector3(v.x, v.y, 0f);
		}

		// Token: 0x17000419 RID: 1049
		// (get) Token: 0x0600159E RID: 5534 RVA: 0x00025CD8 File Offset: 0x00023ED8
		public static Vector2 zero
		{
			get
			{
				return Vector2.zeroVector;
			}
		}

		// Token: 0x1700041A RID: 1050
		// (get) Token: 0x0600159F RID: 5535 RVA: 0x00025CF4 File Offset: 0x00023EF4
		public static Vector2 one
		{
			get
			{
				return Vector2.oneVector;
			}
		}

		// Token: 0x1700041B RID: 1051
		// (get) Token: 0x060015A0 RID: 5536 RVA: 0x00025D10 File Offset: 0x00023F10
		public static Vector2 up
		{
			get
			{
				return Vector2.upVector;
			}
		}

		// Token: 0x1700041C RID: 1052
		// (get) Token: 0x060015A1 RID: 5537 RVA: 0x00025D2C File Offset: 0x00023F2C
		public static Vector2 down
		{
			get
			{
				return Vector2.downVector;
			}
		}

		// Token: 0x1700041D RID: 1053
		// (get) Token: 0x060015A2 RID: 5538 RVA: 0x00025D48 File Offset: 0x00023F48
		public static Vector2 left
		{
			get
			{
				return Vector2.leftVector;
			}
		}

		// Token: 0x1700041E RID: 1054
		// (get) Token: 0x060015A3 RID: 5539 RVA: 0x00025D64 File Offset: 0x00023F64
		public static Vector2 right
		{
			get
			{
				return Vector2.rightVector;
			}
		}

		// Token: 0x1700041F RID: 1055
		// (get) Token: 0x060015A4 RID: 5540 RVA: 0x00025D80 File Offset: 0x00023F80
		public static Vector2 positiveInfinity
		{
			get
			{
				return Vector2.positiveInfinityVector;
			}
		}

		// Token: 0x17000420 RID: 1056
		// (get) Token: 0x060015A5 RID: 5541 RVA: 0x00025D9C File Offset: 0x00023F9C
		public static Vector2 negativeInfinity
		{
			get
			{
				return Vector2.negativeInfinityVector;
			}
		}

		// Token: 0x060015A6 RID: 5542 RVA: 0x00025DB8 File Offset: 0x00023FB8
		// Note: this type is marked as 'beforefieldinit'.
		static Vector2()
		{
		}

		// Token: 0x04000810 RID: 2064
		public float x;

		// Token: 0x04000811 RID: 2065
		public float y;

		// Token: 0x04000812 RID: 2066
		private static readonly Vector2 zeroVector = new Vector2(0f, 0f);

		// Token: 0x04000813 RID: 2067
		private static readonly Vector2 oneVector = new Vector2(1f, 1f);

		// Token: 0x04000814 RID: 2068
		private static readonly Vector2 upVector = new Vector2(0f, 1f);

		// Token: 0x04000815 RID: 2069
		private static readonly Vector2 downVector = new Vector2(0f, -1f);

		// Token: 0x04000816 RID: 2070
		private static readonly Vector2 leftVector = new Vector2(-1f, 0f);

		// Token: 0x04000817 RID: 2071
		private static readonly Vector2 rightVector = new Vector2(1f, 0f);

		// Token: 0x04000818 RID: 2072
		private static readonly Vector2 positiveInfinityVector = new Vector2(float.PositiveInfinity, float.PositiveInfinity);

		// Token: 0x04000819 RID: 2073
		private static readonly Vector2 negativeInfinityVector = new Vector2(float.NegativeInfinity, float.NegativeInfinity);

		// Token: 0x0400081A RID: 2074
		public const float kEpsilon = 1E-05f;

		// Token: 0x0400081B RID: 2075
		public const float kEpsilonNormalSqrt = 1E-15f;
	}
}
