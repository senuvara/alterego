﻿using System;
using UnityEngine.Serialization;

namespace UnityEngine.Events
{
	// Token: 0x0200023A RID: 570
	[Serializable]
	internal class ArgumentCache : ISerializationCallbackReceiver
	{
		// Token: 0x060014AF RID: 5295 RVA: 0x00002370 File Offset: 0x00000570
		public ArgumentCache()
		{
		}

		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x060014B0 RID: 5296 RVA: 0x0002322C File Offset: 0x0002142C
		// (set) Token: 0x060014B1 RID: 5297 RVA: 0x00023247 File Offset: 0x00021447
		public Object unityObjectArgument
		{
			get
			{
				return this.m_ObjectArgument;
			}
			set
			{
				this.m_ObjectArgument = value;
				this.m_ObjectArgumentAssemblyTypeName = ((!(value != null)) ? string.Empty : value.GetType().AssemblyQualifiedName);
			}
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x060014B2 RID: 5298 RVA: 0x00023278 File Offset: 0x00021478
		public string unityObjectArgumentAssemblyTypeName
		{
			get
			{
				return this.m_ObjectArgumentAssemblyTypeName;
			}
		}

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x060014B3 RID: 5299 RVA: 0x00023294 File Offset: 0x00021494
		// (set) Token: 0x060014B4 RID: 5300 RVA: 0x000232AF File Offset: 0x000214AF
		public int intArgument
		{
			get
			{
				return this.m_IntArgument;
			}
			set
			{
				this.m_IntArgument = value;
			}
		}

		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x060014B5 RID: 5301 RVA: 0x000232BC File Offset: 0x000214BC
		// (set) Token: 0x060014B6 RID: 5302 RVA: 0x000232D7 File Offset: 0x000214D7
		public float floatArgument
		{
			get
			{
				return this.m_FloatArgument;
			}
			set
			{
				this.m_FloatArgument = value;
			}
		}

		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x060014B7 RID: 5303 RVA: 0x000232E4 File Offset: 0x000214E4
		// (set) Token: 0x060014B8 RID: 5304 RVA: 0x000232FF File Offset: 0x000214FF
		public string stringArgument
		{
			get
			{
				return this.m_StringArgument;
			}
			set
			{
				this.m_StringArgument = value;
			}
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x060014B9 RID: 5305 RVA: 0x0002330C File Offset: 0x0002150C
		// (set) Token: 0x060014BA RID: 5306 RVA: 0x00023327 File Offset: 0x00021527
		public bool boolArgument
		{
			get
			{
				return this.m_BoolArgument;
			}
			set
			{
				this.m_BoolArgument = value;
			}
		}

		// Token: 0x060014BB RID: 5307 RVA: 0x00023334 File Offset: 0x00021534
		private void TidyAssemblyTypeName()
		{
			if (!string.IsNullOrEmpty(this.m_ObjectArgumentAssemblyTypeName))
			{
				int num = int.MaxValue;
				int num2 = this.m_ObjectArgumentAssemblyTypeName.IndexOf(", Version=");
				if (num2 != -1)
				{
					num = Math.Min(num2, num);
				}
				num2 = this.m_ObjectArgumentAssemblyTypeName.IndexOf(", Culture=");
				if (num2 != -1)
				{
					num = Math.Min(num2, num);
				}
				num2 = this.m_ObjectArgumentAssemblyTypeName.IndexOf(", PublicKeyToken=");
				if (num2 != -1)
				{
					num = Math.Min(num2, num);
				}
				if (num != 2147483647)
				{
					this.m_ObjectArgumentAssemblyTypeName = this.m_ObjectArgumentAssemblyTypeName.Substring(0, num);
				}
				num2 = this.m_ObjectArgumentAssemblyTypeName.IndexOf(", UnityEngine.");
				if (num2 != -1 && this.m_ObjectArgumentAssemblyTypeName.EndsWith("Module"))
				{
					this.m_ObjectArgumentAssemblyTypeName = this.m_ObjectArgumentAssemblyTypeName.Substring(0, num2) + ", UnityEngine";
				}
			}
		}

		// Token: 0x060014BC RID: 5308 RVA: 0x00023425 File Offset: 0x00021625
		public void OnBeforeSerialize()
		{
			this.TidyAssemblyTypeName();
		}

		// Token: 0x060014BD RID: 5309 RVA: 0x00023425 File Offset: 0x00021625
		public void OnAfterDeserialize()
		{
			this.TidyAssemblyTypeName();
		}

		// Token: 0x040007E6 RID: 2022
		[SerializeField]
		[FormerlySerializedAs("objectArgument")]
		private Object m_ObjectArgument;

		// Token: 0x040007E7 RID: 2023
		[FormerlySerializedAs("objectArgumentAssemblyTypeName")]
		[SerializeField]
		private string m_ObjectArgumentAssemblyTypeName;

		// Token: 0x040007E8 RID: 2024
		[FormerlySerializedAs("intArgument")]
		[SerializeField]
		private int m_IntArgument;

		// Token: 0x040007E9 RID: 2025
		[FormerlySerializedAs("floatArgument")]
		[SerializeField]
		private float m_FloatArgument;

		// Token: 0x040007EA RID: 2026
		[FormerlySerializedAs("stringArgument")]
		[SerializeField]
		private string m_StringArgument;

		// Token: 0x040007EB RID: 2027
		[SerializeField]
		private bool m_BoolArgument;
	}
}
