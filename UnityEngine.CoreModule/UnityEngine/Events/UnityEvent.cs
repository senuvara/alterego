﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine.Scripting;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x02000249 RID: 585
	[Serializable]
	public class UnityEvent : UnityEventBase
	{
		// Token: 0x0600152D RID: 5421 RVA: 0x000248E3 File Offset: 0x00022AE3
		[RequiredByNativeCode]
		public UnityEvent()
		{
		}

		// Token: 0x0600152E RID: 5422 RVA: 0x000248F3 File Offset: 0x00022AF3
		public void AddListener(UnityAction call)
		{
			base.AddCall(UnityEvent.GetDelegate(call));
		}

		// Token: 0x0600152F RID: 5423 RVA: 0x00024902 File Offset: 0x00022B02
		public void RemoveListener(UnityAction call)
		{
			base.RemoveListener(call.Target, call.GetMethodInfo());
		}

		// Token: 0x06001530 RID: 5424 RVA: 0x00024918 File Offset: 0x00022B18
		protected override MethodInfo FindMethod_Impl(string name, object targetObj)
		{
			return UnityEventBase.GetValidMethodInfo(targetObj, name, new Type[0]);
		}

		// Token: 0x06001531 RID: 5425 RVA: 0x0002493C File Offset: 0x00022B3C
		internal override BaseInvokableCall GetDelegate(object target, MethodInfo theFunction)
		{
			return new InvokableCall(target, theFunction);
		}

		// Token: 0x06001532 RID: 5426 RVA: 0x00024958 File Offset: 0x00022B58
		private static BaseInvokableCall GetDelegate(UnityAction action)
		{
			return new InvokableCall(action);
		}

		// Token: 0x06001533 RID: 5427 RVA: 0x00024974 File Offset: 0x00022B74
		public void Invoke()
		{
			List<BaseInvokableCall> list = base.PrepareInvoke();
			for (int i = 0; i < list.Count; i++)
			{
				InvokableCall invokableCall = list[i] as InvokableCall;
				if (invokableCall != null)
				{
					invokableCall.Invoke();
				}
				else
				{
					InvokableCall invokableCall2 = list[i] as InvokableCall;
					if (invokableCall2 != null)
					{
						invokableCall2.Invoke();
					}
					else
					{
						BaseInvokableCall baseInvokableCall = list[i];
						if (this.m_InvokeArray == null)
						{
							this.m_InvokeArray = new object[0];
						}
						baseInvokableCall.Invoke(this.m_InvokeArray);
					}
				}
			}
		}

		// Token: 0x04000804 RID: 2052
		private object[] m_InvokeArray = null;
	}
}
