﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace UnityEngine.Events
{
	// Token: 0x02000245 RID: 581
	internal class InvokableCallList
	{
		// Token: 0x06001509 RID: 5385 RVA: 0x00024214 File Offset: 0x00022414
		public InvokableCallList()
		{
		}

		// Token: 0x17000413 RID: 1043
		// (get) Token: 0x0600150A RID: 5386 RVA: 0x00024244 File Offset: 0x00022444
		public int Count
		{
			get
			{
				return this.m_PersistentCalls.Count + this.m_RuntimeCalls.Count;
			}
		}

		// Token: 0x0600150B RID: 5387 RVA: 0x00024270 File Offset: 0x00022470
		public void AddPersistentInvokableCall(BaseInvokableCall call)
		{
			this.m_PersistentCalls.Add(call);
			this.m_NeedsUpdate = true;
		}

		// Token: 0x0600150C RID: 5388 RVA: 0x00024286 File Offset: 0x00022486
		public void AddListener(BaseInvokableCall call)
		{
			this.m_RuntimeCalls.Add(call);
			this.m_NeedsUpdate = true;
		}

		// Token: 0x0600150D RID: 5389 RVA: 0x0002429C File Offset: 0x0002249C
		public void RemoveListener(object targetObj, MethodInfo method)
		{
			List<BaseInvokableCall> list = new List<BaseInvokableCall>();
			for (int i = 0; i < this.m_RuntimeCalls.Count; i++)
			{
				if (this.m_RuntimeCalls[i].Find(targetObj, method))
				{
					list.Add(this.m_RuntimeCalls[i]);
				}
			}
			this.m_RuntimeCalls.RemoveAll(new Predicate<BaseInvokableCall>(list.Contains));
			this.m_NeedsUpdate = true;
		}

		// Token: 0x0600150E RID: 5390 RVA: 0x00024317 File Offset: 0x00022517
		public void Clear()
		{
			this.m_RuntimeCalls.Clear();
			this.m_NeedsUpdate = true;
		}

		// Token: 0x0600150F RID: 5391 RVA: 0x0002432C File Offset: 0x0002252C
		public void ClearPersistent()
		{
			this.m_PersistentCalls.Clear();
			this.m_NeedsUpdate = true;
		}

		// Token: 0x06001510 RID: 5392 RVA: 0x00024344 File Offset: 0x00022544
		public List<BaseInvokableCall> PrepareInvoke()
		{
			if (this.m_NeedsUpdate)
			{
				this.m_ExecutingCalls.Clear();
				this.m_ExecutingCalls.AddRange(this.m_PersistentCalls);
				this.m_ExecutingCalls.AddRange(this.m_RuntimeCalls);
				this.m_NeedsUpdate = false;
			}
			return this.m_ExecutingCalls;
		}

		// Token: 0x040007FC RID: 2044
		private readonly List<BaseInvokableCall> m_PersistentCalls = new List<BaseInvokableCall>();

		// Token: 0x040007FD RID: 2045
		private readonly List<BaseInvokableCall> m_RuntimeCalls = new List<BaseInvokableCall>();

		// Token: 0x040007FE RID: 2046
		private readonly List<BaseInvokableCall> m_ExecutingCalls = new List<BaseInvokableCall>();

		// Token: 0x040007FF RID: 2047
		private bool m_NeedsUpdate = true;
	}
}
