﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x02000242 RID: 578
	public enum UnityEventCallState
	{
		// Token: 0x040007F3 RID: 2035
		Off,
		// Token: 0x040007F4 RID: 2036
		EditorAndRuntime,
		// Token: 0x040007F5 RID: 2037
		RuntimeOnly
	}
}
