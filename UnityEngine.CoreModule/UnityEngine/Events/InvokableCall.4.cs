﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200023F RID: 575
	internal class InvokableCall<T1, T2, T3> : BaseInvokableCall
	{
		// Token: 0x060014D9 RID: 5337 RVA: 0x0002389A File Offset: 0x00021A9A
		public InvokableCall(object target, MethodInfo theFunction) : base(target, theFunction)
		{
			this.Delegate = (UnityAction<T1, T2, T3>)theFunction.CreateDelegate(typeof(UnityAction<T1, T2, T3>), target);
		}

		// Token: 0x060014DA RID: 5338 RVA: 0x000238C1 File Offset: 0x00021AC1
		public InvokableCall(UnityAction<T1, T2, T3> action)
		{
			this.Delegate += action;
		}

		// Token: 0x1400000F RID: 15
		// (add) Token: 0x060014DB RID: 5339 RVA: 0x000238D4 File Offset: 0x00021AD4
		// (remove) Token: 0x060014DC RID: 5340 RVA: 0x0002390C File Offset: 0x00021B0C
		protected event UnityAction<T1, T2, T3> Delegate
		{
			add
			{
				UnityAction<T1, T2, T3> unityAction = this.Delegate;
				UnityAction<T1, T2, T3> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<T1, T2, T3>>(ref this.Delegate, (UnityAction<T1, T2, T3>)System.Delegate.Combine(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
			remove
			{
				UnityAction<T1, T2, T3> unityAction = this.Delegate;
				UnityAction<T1, T2, T3> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<T1, T2, T3>>(ref this.Delegate, (UnityAction<T1, T2, T3>)System.Delegate.Remove(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
		}

		// Token: 0x060014DD RID: 5341 RVA: 0x00023944 File Offset: 0x00021B44
		public override void Invoke(object[] args)
		{
			if (args.Length != 3)
			{
				throw new ArgumentException("Passed argument 'args' is invalid size. Expected size is 1");
			}
			BaseInvokableCall.ThrowOnInvalidArg<T1>(args[0]);
			BaseInvokableCall.ThrowOnInvalidArg<T2>(args[1]);
			BaseInvokableCall.ThrowOnInvalidArg<T3>(args[2]);
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate((T1)((object)args[0]), (T2)((object)args[1]), (T3)((object)args[2]));
			}
		}

		// Token: 0x060014DE RID: 5342 RVA: 0x000239B1 File Offset: 0x00021BB1
		public void Invoke(T1 args0, T2 args1, T3 args2)
		{
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate(args0, args1, args2);
			}
		}

		// Token: 0x060014DF RID: 5343 RVA: 0x000239D4 File Offset: 0x00021BD4
		public override bool Find(object targetObj, MethodInfo method)
		{
			return this.Delegate.Target == targetObj && this.Delegate.GetMethodInfo().Equals(method);
		}

		// Token: 0x040007EF RID: 2031
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private UnityAction<T1, T2, T3> Delegate;
	}
}
