﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200023E RID: 574
	internal class InvokableCall<T1, T2> : BaseInvokableCall
	{
		// Token: 0x060014D2 RID: 5330 RVA: 0x00023736 File Offset: 0x00021936
		public InvokableCall(object target, MethodInfo theFunction) : base(target, theFunction)
		{
			this.Delegate = (UnityAction<T1, T2>)theFunction.CreateDelegate(typeof(UnityAction<T1, T2>), target);
		}

		// Token: 0x060014D3 RID: 5331 RVA: 0x0002375D File Offset: 0x0002195D
		public InvokableCall(UnityAction<T1, T2> action)
		{
			this.Delegate += action;
		}

		// Token: 0x1400000E RID: 14
		// (add) Token: 0x060014D4 RID: 5332 RVA: 0x00023770 File Offset: 0x00021970
		// (remove) Token: 0x060014D5 RID: 5333 RVA: 0x000237A8 File Offset: 0x000219A8
		protected event UnityAction<T1, T2> Delegate
		{
			add
			{
				UnityAction<T1, T2> unityAction = this.Delegate;
				UnityAction<T1, T2> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<T1, T2>>(ref this.Delegate, (UnityAction<T1, T2>)System.Delegate.Combine(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
			remove
			{
				UnityAction<T1, T2> unityAction = this.Delegate;
				UnityAction<T1, T2> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<T1, T2>>(ref this.Delegate, (UnityAction<T1, T2>)System.Delegate.Remove(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
		}

		// Token: 0x060014D6 RID: 5334 RVA: 0x000237E0 File Offset: 0x000219E0
		public override void Invoke(object[] args)
		{
			if (args.Length != 2)
			{
				throw new ArgumentException("Passed argument 'args' is invalid size. Expected size is 1");
			}
			BaseInvokableCall.ThrowOnInvalidArg<T1>(args[0]);
			BaseInvokableCall.ThrowOnInvalidArg<T2>(args[1]);
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate((T1)((object)args[0]), (T2)((object)args[1]));
			}
		}

		// Token: 0x060014D7 RID: 5335 RVA: 0x0002383D File Offset: 0x00021A3D
		public void Invoke(T1 args0, T2 args1)
		{
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate(args0, args1);
			}
		}

		// Token: 0x060014D8 RID: 5336 RVA: 0x00023860 File Offset: 0x00021A60
		public override bool Find(object targetObj, MethodInfo method)
		{
			return this.Delegate.Target == targetObj && this.Delegate.GetMethodInfo().Equals(method);
		}

		// Token: 0x040007EE RID: 2030
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private UnityAction<T1, T2> Delegate;
	}
}
