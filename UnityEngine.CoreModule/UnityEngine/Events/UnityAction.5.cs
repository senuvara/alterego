﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x02000250 RID: 592
	// (Invoke) Token: 0x06001556 RID: 5462
	public delegate void UnityAction<T0, T1, T2, T3>(T0 arg0, T1 arg1, T2 arg2, T3 arg3);
}
