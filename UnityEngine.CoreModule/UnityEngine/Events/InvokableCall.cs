﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200023C RID: 572
	internal class InvokableCall : BaseInvokableCall
	{
		// Token: 0x060014C4 RID: 5316 RVA: 0x000234E6 File Offset: 0x000216E6
		public InvokableCall(object target, MethodInfo theFunction) : base(target, theFunction)
		{
			this.Delegate += (UnityAction)theFunction.CreateDelegate(typeof(UnityAction), target);
		}

		// Token: 0x060014C5 RID: 5317 RVA: 0x0002350D File Offset: 0x0002170D
		public InvokableCall(UnityAction action)
		{
			this.Delegate += action;
		}

		// Token: 0x1400000C RID: 12
		// (add) Token: 0x060014C6 RID: 5318 RVA: 0x00023520 File Offset: 0x00021720
		// (remove) Token: 0x060014C7 RID: 5319 RVA: 0x00023558 File Offset: 0x00021758
		private event UnityAction Delegate
		{
			add
			{
				UnityAction unityAction = this.Delegate;
				UnityAction unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction>(ref this.Delegate, (UnityAction)System.Delegate.Combine(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
			remove
			{
				UnityAction unityAction = this.Delegate;
				UnityAction unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction>(ref this.Delegate, (UnityAction)System.Delegate.Remove(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
		}

		// Token: 0x060014C8 RID: 5320 RVA: 0x0002358E File Offset: 0x0002178E
		public override void Invoke(object[] args)
		{
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate();
			}
		}

		// Token: 0x060014C9 RID: 5321 RVA: 0x0002358E File Offset: 0x0002178E
		public void Invoke()
		{
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate();
			}
		}

		// Token: 0x060014CA RID: 5322 RVA: 0x000235AC File Offset: 0x000217AC
		public override bool Find(object targetObj, MethodInfo method)
		{
			return this.Delegate.Target == targetObj && this.Delegate.GetMethodInfo().Equals(method);
		}

		// Token: 0x040007EC RID: 2028
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private UnityAction Delegate;
	}
}
