﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x02000248 RID: 584
	// (Invoke) Token: 0x0600152A RID: 5418
	public delegate void UnityAction();
}
