﻿using System;
using System.Reflection;

namespace UnityEngine.Events
{
	// Token: 0x0200023B RID: 571
	internal abstract class BaseInvokableCall
	{
		// Token: 0x060014BE RID: 5310 RVA: 0x00002050 File Offset: 0x00000250
		protected BaseInvokableCall()
		{
		}

		// Token: 0x060014BF RID: 5311 RVA: 0x0002342E File Offset: 0x0002162E
		protected BaseInvokableCall(object target, MethodInfo function)
		{
			if (target == null)
			{
				throw new ArgumentNullException("target");
			}
			if (function == null)
			{
				throw new ArgumentNullException("function");
			}
		}

		// Token: 0x060014C0 RID: 5312
		public abstract void Invoke(object[] args);

		// Token: 0x060014C1 RID: 5313 RVA: 0x00023459 File Offset: 0x00021659
		protected static void ThrowOnInvalidArg<T>(object arg)
		{
			if (arg != null && !(arg is T))
			{
				throw new ArgumentException(UnityString.Format("Passed argument 'args[0]' is of the wrong type. Type:{0} Expected:{1}", new object[]
				{
					arg.GetType(),
					typeof(T)
				}));
			}
		}

		// Token: 0x060014C2 RID: 5314 RVA: 0x0002349C File Offset: 0x0002169C
		protected static bool AllowInvoke(Delegate @delegate)
		{
			object target = @delegate.Target;
			bool result;
			if (target == null)
			{
				result = true;
			}
			else
			{
				Object @object = target as Object;
				result = (object.ReferenceEquals(@object, null) || @object != null);
			}
			return result;
		}

		// Token: 0x060014C3 RID: 5315
		public abstract bool Find(object targetObj, MethodInfo method);
	}
}
