﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200023D RID: 573
	internal class InvokableCall<T1> : BaseInvokableCall
	{
		// Token: 0x060014CB RID: 5323 RVA: 0x000235E6 File Offset: 0x000217E6
		public InvokableCall(object target, MethodInfo theFunction) : base(target, theFunction)
		{
			this.Delegate += (UnityAction<T1>)theFunction.CreateDelegate(typeof(UnityAction<T1>), target);
		}

		// Token: 0x060014CC RID: 5324 RVA: 0x0002360D File Offset: 0x0002180D
		public InvokableCall(UnityAction<T1> action)
		{
			this.Delegate += action;
		}

		// Token: 0x1400000D RID: 13
		// (add) Token: 0x060014CD RID: 5325 RVA: 0x00023620 File Offset: 0x00021820
		// (remove) Token: 0x060014CE RID: 5326 RVA: 0x00023658 File Offset: 0x00021858
		protected event UnityAction<T1> Delegate
		{
			add
			{
				UnityAction<T1> unityAction = this.Delegate;
				UnityAction<T1> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<T1>>(ref this.Delegate, (UnityAction<T1>)System.Delegate.Combine(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
			remove
			{
				UnityAction<T1> unityAction = this.Delegate;
				UnityAction<T1> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<T1>>(ref this.Delegate, (UnityAction<T1>)System.Delegate.Remove(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
		}

		// Token: 0x060014CF RID: 5327 RVA: 0x00023690 File Offset: 0x00021890
		public override void Invoke(object[] args)
		{
			if (args.Length != 1)
			{
				throw new ArgumentException("Passed argument 'args' is invalid size. Expected size is 1");
			}
			BaseInvokableCall.ThrowOnInvalidArg<T1>(args[0]);
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate((T1)((object)args[0]));
			}
		}

		// Token: 0x060014D0 RID: 5328 RVA: 0x000236DD File Offset: 0x000218DD
		public virtual void Invoke(T1 args0)
		{
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate(args0);
			}
		}

		// Token: 0x060014D1 RID: 5329 RVA: 0x000236FC File Offset: 0x000218FC
		public override bool Find(object targetObj, MethodInfo method)
		{
			return this.Delegate.Target == targetObj && this.Delegate.GetMethodInfo().Equals(method);
		}

		// Token: 0x040007ED RID: 2029
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private UnityAction<T1> Delegate;
	}
}
