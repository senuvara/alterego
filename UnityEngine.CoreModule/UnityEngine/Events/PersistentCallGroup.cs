﻿using System;
using System.Collections.Generic;
using UnityEngine.Serialization;

namespace UnityEngine.Events
{
	// Token: 0x02000244 RID: 580
	[Serializable]
	internal class PersistentCallGroup
	{
		// Token: 0x060014F7 RID: 5367 RVA: 0x00023ED9 File Offset: 0x000220D9
		public PersistentCallGroup()
		{
			this.m_Calls = new List<PersistentCall>();
		}

		// Token: 0x17000412 RID: 1042
		// (get) Token: 0x060014F8 RID: 5368 RVA: 0x00023EF0 File Offset: 0x000220F0
		public int Count
		{
			get
			{
				return this.m_Calls.Count;
			}
		}

		// Token: 0x060014F9 RID: 5369 RVA: 0x00023F10 File Offset: 0x00022110
		public PersistentCall GetListener(int index)
		{
			return this.m_Calls[index];
		}

		// Token: 0x060014FA RID: 5370 RVA: 0x00023F34 File Offset: 0x00022134
		public IEnumerable<PersistentCall> GetListeners()
		{
			return this.m_Calls;
		}

		// Token: 0x060014FB RID: 5371 RVA: 0x00023F4F File Offset: 0x0002214F
		public void AddListener()
		{
			this.m_Calls.Add(new PersistentCall());
		}

		// Token: 0x060014FC RID: 5372 RVA: 0x00023F62 File Offset: 0x00022162
		public void AddListener(PersistentCall call)
		{
			this.m_Calls.Add(call);
		}

		// Token: 0x060014FD RID: 5373 RVA: 0x00023F71 File Offset: 0x00022171
		public void RemoveListener(int index)
		{
			this.m_Calls.RemoveAt(index);
		}

		// Token: 0x060014FE RID: 5374 RVA: 0x00023F80 File Offset: 0x00022180
		public void Clear()
		{
			this.m_Calls.Clear();
		}

		// Token: 0x060014FF RID: 5375 RVA: 0x00023F90 File Offset: 0x00022190
		public void RegisterEventPersistentListener(int index, Object targetObj, string methodName)
		{
			PersistentCall listener = this.GetListener(index);
			listener.RegisterPersistentListener(targetObj, methodName);
			listener.mode = PersistentListenerMode.EventDefined;
		}

		// Token: 0x06001500 RID: 5376 RVA: 0x00023FB8 File Offset: 0x000221B8
		public void RegisterVoidPersistentListener(int index, Object targetObj, string methodName)
		{
			PersistentCall listener = this.GetListener(index);
			listener.RegisterPersistentListener(targetObj, methodName);
			listener.mode = PersistentListenerMode.Void;
		}

		// Token: 0x06001501 RID: 5377 RVA: 0x00023FE0 File Offset: 0x000221E0
		public void RegisterObjectPersistentListener(int index, Object targetObj, Object argument, string methodName)
		{
			PersistentCall listener = this.GetListener(index);
			listener.RegisterPersistentListener(targetObj, methodName);
			listener.mode = PersistentListenerMode.Object;
			listener.arguments.unityObjectArgument = argument;
		}

		// Token: 0x06001502 RID: 5378 RVA: 0x00024014 File Offset: 0x00022214
		public void RegisterIntPersistentListener(int index, Object targetObj, int argument, string methodName)
		{
			PersistentCall listener = this.GetListener(index);
			listener.RegisterPersistentListener(targetObj, methodName);
			listener.mode = PersistentListenerMode.Int;
			listener.arguments.intArgument = argument;
		}

		// Token: 0x06001503 RID: 5379 RVA: 0x00024048 File Offset: 0x00022248
		public void RegisterFloatPersistentListener(int index, Object targetObj, float argument, string methodName)
		{
			PersistentCall listener = this.GetListener(index);
			listener.RegisterPersistentListener(targetObj, methodName);
			listener.mode = PersistentListenerMode.Float;
			listener.arguments.floatArgument = argument;
		}

		// Token: 0x06001504 RID: 5380 RVA: 0x0002407C File Offset: 0x0002227C
		public void RegisterStringPersistentListener(int index, Object targetObj, string argument, string methodName)
		{
			PersistentCall listener = this.GetListener(index);
			listener.RegisterPersistentListener(targetObj, methodName);
			listener.mode = PersistentListenerMode.String;
			listener.arguments.stringArgument = argument;
		}

		// Token: 0x06001505 RID: 5381 RVA: 0x000240B0 File Offset: 0x000222B0
		public void RegisterBoolPersistentListener(int index, Object targetObj, bool argument, string methodName)
		{
			PersistentCall listener = this.GetListener(index);
			listener.RegisterPersistentListener(targetObj, methodName);
			listener.mode = PersistentListenerMode.Bool;
			listener.arguments.boolArgument = argument;
		}

		// Token: 0x06001506 RID: 5382 RVA: 0x000240E4 File Offset: 0x000222E4
		public void UnregisterPersistentListener(int index)
		{
			PersistentCall listener = this.GetListener(index);
			listener.UnregisterPersistentListener();
		}

		// Token: 0x06001507 RID: 5383 RVA: 0x00024100 File Offset: 0x00022300
		public void RemoveListeners(Object target, string methodName)
		{
			List<PersistentCall> list = new List<PersistentCall>();
			for (int i = 0; i < this.m_Calls.Count; i++)
			{
				if (this.m_Calls[i].target == target && this.m_Calls[i].methodName == methodName)
				{
					list.Add(this.m_Calls[i]);
				}
			}
			this.m_Calls.RemoveAll(new Predicate<PersistentCall>(list.Contains));
		}

		// Token: 0x06001508 RID: 5384 RVA: 0x00024194 File Offset: 0x00022394
		public void Initialize(InvokableCallList invokableList, UnityEventBase unityEventBase)
		{
			foreach (PersistentCall persistentCall in this.m_Calls)
			{
				if (persistentCall.IsValid())
				{
					BaseInvokableCall runtimeCall = persistentCall.GetRuntimeCall(unityEventBase);
					if (runtimeCall != null)
					{
						invokableList.AddPersistentInvokableCall(runtimeCall);
					}
				}
			}
		}

		// Token: 0x040007FB RID: 2043
		[FormerlySerializedAs("m_Listeners")]
		[SerializeField]
		private List<PersistentCall> m_Calls;
	}
}
