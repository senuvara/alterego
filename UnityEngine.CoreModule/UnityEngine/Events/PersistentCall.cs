﻿using System;
using System.Reflection;
using UnityEngine.Serialization;

namespace UnityEngine.Events
{
	// Token: 0x02000243 RID: 579
	[Serializable]
	internal class PersistentCall
	{
		// Token: 0x060014EA RID: 5354 RVA: 0x00023BB3 File Offset: 0x00021DB3
		public PersistentCall()
		{
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x060014EB RID: 5355 RVA: 0x00023BD4 File Offset: 0x00021DD4
		public Object target
		{
			get
			{
				return this.m_Target;
			}
		}

		// Token: 0x1700040E RID: 1038
		// (get) Token: 0x060014EC RID: 5356 RVA: 0x00023BF0 File Offset: 0x00021DF0
		public string methodName
		{
			get
			{
				return this.m_MethodName;
			}
		}

		// Token: 0x1700040F RID: 1039
		// (get) Token: 0x060014ED RID: 5357 RVA: 0x00023C0C File Offset: 0x00021E0C
		// (set) Token: 0x060014EE RID: 5358 RVA: 0x00023C27 File Offset: 0x00021E27
		public PersistentListenerMode mode
		{
			get
			{
				return this.m_Mode;
			}
			set
			{
				this.m_Mode = value;
			}
		}

		// Token: 0x17000410 RID: 1040
		// (get) Token: 0x060014EF RID: 5359 RVA: 0x00023C34 File Offset: 0x00021E34
		public ArgumentCache arguments
		{
			get
			{
				return this.m_Arguments;
			}
		}

		// Token: 0x17000411 RID: 1041
		// (get) Token: 0x060014F0 RID: 5360 RVA: 0x00023C50 File Offset: 0x00021E50
		// (set) Token: 0x060014F1 RID: 5361 RVA: 0x00023C6B File Offset: 0x00021E6B
		public UnityEventCallState callState
		{
			get
			{
				return this.m_CallState;
			}
			set
			{
				this.m_CallState = value;
			}
		}

		// Token: 0x060014F2 RID: 5362 RVA: 0x00023C78 File Offset: 0x00021E78
		public bool IsValid()
		{
			return this.target != null && !string.IsNullOrEmpty(this.methodName);
		}

		// Token: 0x060014F3 RID: 5363 RVA: 0x00023CB0 File Offset: 0x00021EB0
		public BaseInvokableCall GetRuntimeCall(UnityEventBase theEvent)
		{
			BaseInvokableCall result;
			if (this.m_CallState == UnityEventCallState.Off || theEvent == null)
			{
				result = null;
			}
			else
			{
				MethodInfo methodInfo = theEvent.FindMethod(this);
				if (methodInfo == null)
				{
					result = null;
				}
				else
				{
					switch (this.m_Mode)
					{
					case PersistentListenerMode.EventDefined:
						result = theEvent.GetDelegate(this.target, methodInfo);
						break;
					case PersistentListenerMode.Void:
						result = new InvokableCall(this.target, methodInfo);
						break;
					case PersistentListenerMode.Object:
						result = PersistentCall.GetObjectCall(this.target, methodInfo, this.m_Arguments);
						break;
					case PersistentListenerMode.Int:
						result = new CachedInvokableCall<int>(this.target, methodInfo, this.m_Arguments.intArgument);
						break;
					case PersistentListenerMode.Float:
						result = new CachedInvokableCall<float>(this.target, methodInfo, this.m_Arguments.floatArgument);
						break;
					case PersistentListenerMode.String:
						result = new CachedInvokableCall<string>(this.target, methodInfo, this.m_Arguments.stringArgument);
						break;
					case PersistentListenerMode.Bool:
						result = new CachedInvokableCall<bool>(this.target, methodInfo, this.m_Arguments.boolArgument);
						break;
					default:
						result = null;
						break;
					}
				}
			}
			return result;
		}

		// Token: 0x060014F4 RID: 5364 RVA: 0x00023DD4 File Offset: 0x00021FD4
		private static BaseInvokableCall GetObjectCall(Object target, MethodInfo method, ArgumentCache arguments)
		{
			Type type = typeof(Object);
			if (!string.IsNullOrEmpty(arguments.unityObjectArgumentAssemblyTypeName))
			{
				type = (Type.GetType(arguments.unityObjectArgumentAssemblyTypeName, false) ?? typeof(Object));
			}
			Type typeFromHandle = typeof(CachedInvokableCall<>);
			Type type2 = typeFromHandle.MakeGenericType(new Type[]
			{
				type
			});
			ConstructorInfo constructor = type2.GetConstructor(new Type[]
			{
				typeof(Object),
				typeof(MethodInfo),
				type
			});
			Object @object = arguments.unityObjectArgument;
			if (@object != null && !type.IsAssignableFrom(@object.GetType()))
			{
				@object = null;
			}
			return constructor.Invoke(new object[]
			{
				target,
				method,
				@object
			}) as BaseInvokableCall;
		}

		// Token: 0x060014F5 RID: 5365 RVA: 0x00023EB3 File Offset: 0x000220B3
		public void RegisterPersistentListener(Object ttarget, string mmethodName)
		{
			this.m_Target = ttarget;
			this.m_MethodName = mmethodName;
		}

		// Token: 0x060014F6 RID: 5366 RVA: 0x00023EC4 File Offset: 0x000220C4
		public void UnregisterPersistentListener()
		{
			this.m_MethodName = string.Empty;
			this.m_Target = null;
		}

		// Token: 0x040007F6 RID: 2038
		[SerializeField]
		[FormerlySerializedAs("instance")]
		private Object m_Target;

		// Token: 0x040007F7 RID: 2039
		[SerializeField]
		[FormerlySerializedAs("methodName")]
		private string m_MethodName;

		// Token: 0x040007F8 RID: 2040
		[FormerlySerializedAs("mode")]
		[SerializeField]
		private PersistentListenerMode m_Mode = PersistentListenerMode.EventDefined;

		// Token: 0x040007F9 RID: 2041
		[SerializeField]
		[FormerlySerializedAs("arguments")]
		private ArgumentCache m_Arguments = new ArgumentCache();

		// Token: 0x040007FA RID: 2042
		[SerializeField]
		[FormerlySerializedAs("enabled")]
		[FormerlySerializedAs("m_Enabled")]
		private UnityEventCallState m_CallState = UnityEventCallState.RuntimeOnly;
	}
}
