﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x0200024C RID: 588
	// (Invoke) Token: 0x06001540 RID: 5440
	public delegate void UnityAction<T0, T1>(T0 arg0, T1 arg1);
}
