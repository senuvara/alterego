﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x0200024E RID: 590
	// (Invoke) Token: 0x0600154B RID: 5451
	public delegate void UnityAction<T0, T1, T2>(T0 arg0, T1 arg1, T2 arg2);
}
