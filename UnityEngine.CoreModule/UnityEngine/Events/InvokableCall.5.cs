﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x02000240 RID: 576
	internal class InvokableCall<T1, T2, T3, T4> : BaseInvokableCall
	{
		// Token: 0x060014E0 RID: 5344 RVA: 0x00023A0E File Offset: 0x00021C0E
		public InvokableCall(object target, MethodInfo theFunction) : base(target, theFunction)
		{
			this.Delegate = (UnityAction<T1, T2, T3, T4>)theFunction.CreateDelegate(typeof(UnityAction<T1, T2, T3, T4>), target);
		}

		// Token: 0x060014E1 RID: 5345 RVA: 0x00023A35 File Offset: 0x00021C35
		public InvokableCall(UnityAction<T1, T2, T3, T4> action)
		{
			this.Delegate += action;
		}

		// Token: 0x14000010 RID: 16
		// (add) Token: 0x060014E2 RID: 5346 RVA: 0x00023A48 File Offset: 0x00021C48
		// (remove) Token: 0x060014E3 RID: 5347 RVA: 0x00023A80 File Offset: 0x00021C80
		protected event UnityAction<T1, T2, T3, T4> Delegate
		{
			add
			{
				UnityAction<T1, T2, T3, T4> unityAction = this.Delegate;
				UnityAction<T1, T2, T3, T4> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<T1, T2, T3, T4>>(ref this.Delegate, (UnityAction<T1, T2, T3, T4>)System.Delegate.Combine(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
			remove
			{
				UnityAction<T1, T2, T3, T4> unityAction = this.Delegate;
				UnityAction<T1, T2, T3, T4> unityAction2;
				do
				{
					unityAction2 = unityAction;
					unityAction = Interlocked.CompareExchange<UnityAction<T1, T2, T3, T4>>(ref this.Delegate, (UnityAction<T1, T2, T3, T4>)System.Delegate.Remove(unityAction2, value), unityAction);
				}
				while (unityAction != unityAction2);
			}
		}

		// Token: 0x060014E4 RID: 5348 RVA: 0x00023AB8 File Offset: 0x00021CB8
		public override void Invoke(object[] args)
		{
			if (args.Length != 4)
			{
				throw new ArgumentException("Passed argument 'args' is invalid size. Expected size is 1");
			}
			BaseInvokableCall.ThrowOnInvalidArg<T1>(args[0]);
			BaseInvokableCall.ThrowOnInvalidArg<T2>(args[1]);
			BaseInvokableCall.ThrowOnInvalidArg<T3>(args[2]);
			BaseInvokableCall.ThrowOnInvalidArg<T4>(args[3]);
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate((T1)((object)args[0]), (T2)((object)args[1]), (T3)((object)args[2]), (T4)((object)args[3]));
			}
		}

		// Token: 0x060014E5 RID: 5349 RVA: 0x00023B35 File Offset: 0x00021D35
		public void Invoke(T1 args0, T2 args1, T3 args2, T4 args3)
		{
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate(args0, args1, args2, args3);
			}
		}

		// Token: 0x060014E6 RID: 5350 RVA: 0x00023B58 File Offset: 0x00021D58
		public override bool Find(object targetObj, MethodInfo method)
		{
			return this.Delegate.Target == targetObj && this.Delegate.GetMethodInfo().Equals(method);
		}

		// Token: 0x040007F0 RID: 2032
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private UnityAction<T1, T2, T3, T4> Delegate;
	}
}
