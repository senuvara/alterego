﻿using System;
using System.Reflection;

namespace UnityEngine.Events
{
	// Token: 0x02000241 RID: 577
	internal class CachedInvokableCall<T> : InvokableCall<T>
	{
		// Token: 0x060014E7 RID: 5351 RVA: 0x00023B92 File Offset: 0x00021D92
		public CachedInvokableCall(Object target, MethodInfo theFunction, T argument) : base(target, theFunction)
		{
			this.m_Arg1 = argument;
		}

		// Token: 0x060014E8 RID: 5352 RVA: 0x00023BA4 File Offset: 0x00021DA4
		public override void Invoke(object[] args)
		{
			base.Invoke(this.m_Arg1);
		}

		// Token: 0x060014E9 RID: 5353 RVA: 0x00023BA4 File Offset: 0x00021DA4
		public override void Invoke(T arg0)
		{
			base.Invoke(this.m_Arg1);
		}

		// Token: 0x040007F1 RID: 2033
		private readonly T m_Arg1;
	}
}
