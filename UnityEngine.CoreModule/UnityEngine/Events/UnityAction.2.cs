﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x0200024A RID: 586
	// (Invoke) Token: 0x06001535 RID: 5429
	public delegate void UnityAction<T0>(T0 arg0);
}
