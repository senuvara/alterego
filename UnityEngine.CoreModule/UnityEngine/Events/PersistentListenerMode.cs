﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x02000239 RID: 569
	[Serializable]
	public enum PersistentListenerMode
	{
		// Token: 0x040007DF RID: 2015
		EventDefined,
		// Token: 0x040007E0 RID: 2016
		Void,
		// Token: 0x040007E1 RID: 2017
		Object,
		// Token: 0x040007E2 RID: 2018
		Int,
		// Token: 0x040007E3 RID: 2019
		Float,
		// Token: 0x040007E4 RID: 2020
		String,
		// Token: 0x040007E5 RID: 2021
		Bool
	}
}
