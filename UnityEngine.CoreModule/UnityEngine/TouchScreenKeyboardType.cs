﻿using System;

namespace UnityEngine
{
	// Token: 0x02000233 RID: 563
	public enum TouchScreenKeyboardType
	{
		// Token: 0x040007C2 RID: 1986
		Default,
		// Token: 0x040007C3 RID: 1987
		ASCIICapable,
		// Token: 0x040007C4 RID: 1988
		NumbersAndPunctuation,
		// Token: 0x040007C5 RID: 1989
		URL,
		// Token: 0x040007C6 RID: 1990
		NumberPad,
		// Token: 0x040007C7 RID: 1991
		PhonePad,
		// Token: 0x040007C8 RID: 1992
		NamePhonePad,
		// Token: 0x040007C9 RID: 1993
		EmailAddress,
		// Token: 0x040007CA RID: 1994
		[Obsolete("Wii U is no longer supported as of Unity 2018.1.")]
		NintendoNetworkAccount,
		// Token: 0x040007CB RID: 1995
		Social,
		// Token: 0x040007CC RID: 1996
		Search
	}
}
