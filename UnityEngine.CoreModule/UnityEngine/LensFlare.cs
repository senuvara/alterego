﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020000A7 RID: 167
	[NativeHeader("Runtime/Camera/Flare.h")]
	public sealed class LensFlare : Behaviour
	{
		// Token: 0x06000AAA RID: 2730 RVA: 0x0000A52A File Offset: 0x0000872A
		public LensFlare()
		{
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x06000AAB RID: 2731
		// (set) Token: 0x06000AAC RID: 2732
		public extern float brightness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x06000AAD RID: 2733
		// (set) Token: 0x06000AAE RID: 2734
		public extern float fadeSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x06000AAF RID: 2735 RVA: 0x00012354 File Offset: 0x00010554
		// (set) Token: 0x06000AB0 RID: 2736 RVA: 0x0001236A File Offset: 0x0001056A
		public Color color
		{
			get
			{
				Color result;
				this.get_color_Injected(out result);
				return result;
			}
			set
			{
				this.set_color_Injected(ref value);
			}
		}

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x06000AB1 RID: 2737
		// (set) Token: 0x06000AB2 RID: 2738
		public extern Flare flare { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000AB3 RID: 2739
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_color_Injected(out Color ret);

		// Token: 0x06000AB4 RID: 2740
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_color_Injected(ref Color value);
	}
}
