﻿using System;

namespace UnityEngine
{
	// Token: 0x02000160 RID: 352
	internal interface IPlayerEditorConnectionNative
	{
		// Token: 0x06000F38 RID: 3896
		void Initialize();

		// Token: 0x06000F39 RID: 3897
		void DisconnectAll();

		// Token: 0x06000F3A RID: 3898
		void SendMessage(Guid messageId, byte[] data, int playerId);

		// Token: 0x06000F3B RID: 3899
		void Poll();

		// Token: 0x06000F3C RID: 3900
		void RegisterInternal(Guid messageId);

		// Token: 0x06000F3D RID: 3901
		void UnregisterInternal(Guid messageId);

		// Token: 0x06000F3E RID: 3902
		bool IsConnected();
	}
}
