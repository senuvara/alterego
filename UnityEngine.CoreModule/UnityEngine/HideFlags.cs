﻿using System;

namespace UnityEngine
{
	// Token: 0x02000237 RID: 567
	[Flags]
	public enum HideFlags
	{
		// Token: 0x040007D1 RID: 2001
		None = 0,
		// Token: 0x040007D2 RID: 2002
		HideInHierarchy = 1,
		// Token: 0x040007D3 RID: 2003
		HideInInspector = 2,
		// Token: 0x040007D4 RID: 2004
		DontSaveInEditor = 4,
		// Token: 0x040007D5 RID: 2005
		NotEditable = 8,
		// Token: 0x040007D6 RID: 2006
		DontSaveInBuild = 16,
		// Token: 0x040007D7 RID: 2007
		DontUnloadUnusedAsset = 32,
		// Token: 0x040007D8 RID: 2008
		DontSave = 52,
		// Token: 0x040007D9 RID: 2009
		HideAndDontSave = 61
	}
}
