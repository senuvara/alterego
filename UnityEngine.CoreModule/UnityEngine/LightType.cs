﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B4 RID: 180
	public enum LightType
	{
		// Token: 0x040001CD RID: 461
		Spot,
		// Token: 0x040001CE RID: 462
		Directional,
		// Token: 0x040001CF RID: 463
		Point,
		// Token: 0x040001D0 RID: 464
		Area,
		// Token: 0x040001D1 RID: 465
		Rectangle = 3,
		// Token: 0x040001D2 RID: 466
		Disc
	}
}
