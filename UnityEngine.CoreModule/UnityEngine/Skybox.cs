﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020000AC RID: 172
	[NativeHeader("Runtime/Camera/Skybox.h")]
	public sealed class Skybox : Behaviour
	{
		// Token: 0x06000B13 RID: 2835 RVA: 0x0000A52A File Offset: 0x0000872A
		public Skybox()
		{
		}

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x06000B14 RID: 2836
		// (set) Token: 0x06000B15 RID: 2837
		public extern Material material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
