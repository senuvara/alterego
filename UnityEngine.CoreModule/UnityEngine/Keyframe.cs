﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000012 RID: 18
	[RequiredByNativeCode]
	public struct Keyframe
	{
		// Token: 0x06000174 RID: 372 RVA: 0x0000725C File Offset: 0x0000545C
		public Keyframe(float time, float value)
		{
			this.m_Time = time;
			this.m_Value = value;
			this.m_InTangent = 0f;
			this.m_OutTangent = 0f;
			this.m_WeightedMode = 0;
			this.m_InWeight = 0f;
			this.m_OutWeight = 0f;
		}

		// Token: 0x06000175 RID: 373 RVA: 0x000072AB File Offset: 0x000054AB
		public Keyframe(float time, float value, float inTangent, float outTangent)
		{
			this.m_Time = time;
			this.m_Value = value;
			this.m_InTangent = inTangent;
			this.m_OutTangent = outTangent;
			this.m_WeightedMode = 0;
			this.m_InWeight = 0f;
			this.m_OutWeight = 0f;
		}

		// Token: 0x06000176 RID: 374 RVA: 0x000072E8 File Offset: 0x000054E8
		public Keyframe(float time, float value, float inTangent, float outTangent, float inWeight, float outWeight)
		{
			this.m_Time = time;
			this.m_Value = value;
			this.m_InTangent = inTangent;
			this.m_OutTangent = outTangent;
			this.m_WeightedMode = 3;
			this.m_InWeight = inWeight;
			this.m_OutWeight = outWeight;
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000177 RID: 375 RVA: 0x00007320 File Offset: 0x00005520
		// (set) Token: 0x06000178 RID: 376 RVA: 0x0000733B File Offset: 0x0000553B
		public float time
		{
			get
			{
				return this.m_Time;
			}
			set
			{
				this.m_Time = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000179 RID: 377 RVA: 0x00007348 File Offset: 0x00005548
		// (set) Token: 0x0600017A RID: 378 RVA: 0x00007363 File Offset: 0x00005563
		public float value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				this.m_Value = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600017B RID: 379 RVA: 0x00007370 File Offset: 0x00005570
		// (set) Token: 0x0600017C RID: 380 RVA: 0x0000738B File Offset: 0x0000558B
		public float inTangent
		{
			get
			{
				return this.m_InTangent;
			}
			set
			{
				this.m_InTangent = value;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600017D RID: 381 RVA: 0x00007398 File Offset: 0x00005598
		// (set) Token: 0x0600017E RID: 382 RVA: 0x000073B3 File Offset: 0x000055B3
		public float outTangent
		{
			get
			{
				return this.m_OutTangent;
			}
			set
			{
				this.m_OutTangent = value;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600017F RID: 383 RVA: 0x000073C0 File Offset: 0x000055C0
		// (set) Token: 0x06000180 RID: 384 RVA: 0x000073DB File Offset: 0x000055DB
		public float inWeight
		{
			get
			{
				return this.m_InWeight;
			}
			set
			{
				this.m_InWeight = value;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000181 RID: 385 RVA: 0x000073E8 File Offset: 0x000055E8
		// (set) Token: 0x06000182 RID: 386 RVA: 0x00007403 File Offset: 0x00005603
		public float outWeight
		{
			get
			{
				return this.m_OutWeight;
			}
			set
			{
				this.m_OutWeight = value;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000183 RID: 387 RVA: 0x00007410 File Offset: 0x00005610
		// (set) Token: 0x06000184 RID: 388 RVA: 0x0000742B File Offset: 0x0000562B
		public WeightedMode weightedMode
		{
			get
			{
				return (WeightedMode)this.m_WeightedMode;
			}
			set
			{
				this.m_WeightedMode = (int)value;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000185 RID: 389 RVA: 0x00007438 File Offset: 0x00005638
		// (set) Token: 0x06000186 RID: 390 RVA: 0x00007453 File Offset: 0x00005653
		[Obsolete("Use AnimationUtility.SetLeftTangentMode, AnimationUtility.SetRightTangentMode, AnimationUtility.GetLeftTangentMode or AnimationUtility.GetRightTangentMode instead.")]
		public int tangentMode
		{
			get
			{
				return this.tangentModeInternal;
			}
			set
			{
				this.tangentModeInternal = value;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000187 RID: 391 RVA: 0x00007460 File Offset: 0x00005660
		// (set) Token: 0x06000188 RID: 392 RVA: 0x00007476 File Offset: 0x00005676
		internal int tangentModeInternal
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x04000027 RID: 39
		private float m_Time;

		// Token: 0x04000028 RID: 40
		private float m_Value;

		// Token: 0x04000029 RID: 41
		private float m_InTangent;

		// Token: 0x0400002A RID: 42
		private float m_OutTangent;

		// Token: 0x0400002B RID: 43
		private int m_WeightedMode;

		// Token: 0x0400002C RID: 44
		private float m_InWeight;

		// Token: 0x0400002D RID: 45
		private float m_OutWeight;
	}
}
