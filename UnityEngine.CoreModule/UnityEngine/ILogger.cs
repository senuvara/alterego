﻿using System;

namespace UnityEngine
{
	// Token: 0x020002B9 RID: 697
	public interface ILogger : ILogHandler
	{
		// Token: 0x17000477 RID: 1143
		// (get) Token: 0x060018BF RID: 6335
		// (set) Token: 0x060018C0 RID: 6336
		ILogHandler logHandler { get; set; }

		// Token: 0x17000478 RID: 1144
		// (get) Token: 0x060018C1 RID: 6337
		// (set) Token: 0x060018C2 RID: 6338
		bool logEnabled { get; set; }

		// Token: 0x17000479 RID: 1145
		// (get) Token: 0x060018C3 RID: 6339
		// (set) Token: 0x060018C4 RID: 6340
		LogType filterLogType { get; set; }

		// Token: 0x060018C5 RID: 6341
		bool IsLogTypeAllowed(LogType logType);

		// Token: 0x060018C6 RID: 6342
		void Log(LogType logType, object message);

		// Token: 0x060018C7 RID: 6343
		void Log(LogType logType, object message, Object context);

		// Token: 0x060018C8 RID: 6344
		void Log(LogType logType, string tag, object message);

		// Token: 0x060018C9 RID: 6345
		void Log(LogType logType, string tag, object message, Object context);

		// Token: 0x060018CA RID: 6346
		void Log(object message);

		// Token: 0x060018CB RID: 6347
		void Log(string tag, object message);

		// Token: 0x060018CC RID: 6348
		void Log(string tag, object message, Object context);

		// Token: 0x060018CD RID: 6349
		void LogWarning(string tag, object message);

		// Token: 0x060018CE RID: 6350
		void LogWarning(string tag, object message, Object context);

		// Token: 0x060018CF RID: 6351
		void LogError(string tag, object message);

		// Token: 0x060018D0 RID: 6352
		void LogError(string tag, object message, Object context);

		// Token: 0x060018D1 RID: 6353
		void LogFormat(LogType logType, string format, params object[] args);

		// Token: 0x060018D2 RID: 6354
		void LogException(Exception exception);
	}
}
