﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200011D RID: 285
	[NativeHeader("Runtime/Utilities/Hash128.h")]
	[UsedByNativeCode]
	[Serializable]
	public struct Hash128 : IComparable, IComparable<Hash128>, IEquatable<Hash128>
	{
		// Token: 0x06000BFF RID: 3071 RVA: 0x000132D9 File Offset: 0x000114D9
		public Hash128(uint u32_0, uint u32_1, uint u32_2, uint u32_3)
		{
			this.m_u32_0 = u32_0;
			this.m_u32_1 = u32_1;
			this.m_u32_2 = u32_2;
			this.m_u32_3 = u32_3;
		}

		// Token: 0x06000C00 RID: 3072 RVA: 0x000132FC File Offset: 0x000114FC
		public unsafe Hash128(ulong u64_0, ulong u64_1)
		{
			this.m_u32_0 = (uint)u64_0;
			this.m_u32_1 = *(ref u64_0 + 4);
			this.m_u32_2 = (uint)u64_1;
			this.m_u32_3 = *(ref u64_1 + 4);
		}

		// Token: 0x17000243 RID: 579
		// (get) Token: 0x06000C01 RID: 3073 RVA: 0x00013334 File Offset: 0x00011534
		internal unsafe ulong u64_0
		{
			get
			{
				fixed (uint* ptr = &this.m_u32_0)
				{
					return (ulong)(*(long*)ptr);
				}
			}
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x06000C02 RID: 3074 RVA: 0x00013354 File Offset: 0x00011554
		internal unsafe ulong u64_1
		{
			get
			{
				fixed (uint* ptr = &this.m_u32_1)
				{
					return (ulong)(*(long*)ptr);
				}
			}
		}

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x06000C03 RID: 3075 RVA: 0x00013374 File Offset: 0x00011574
		public bool isValid
		{
			get
			{
				return this.m_u32_0 != 0U || this.m_u32_1 != 0U || this.m_u32_2 != 0U || this.m_u32_3 != 0U;
			}
		}

		// Token: 0x06000C04 RID: 3076 RVA: 0x000133BC File Offset: 0x000115BC
		public int CompareTo(Hash128 rhs)
		{
			int result;
			if (this < rhs)
			{
				result = -1;
			}
			else if (this > rhs)
			{
				result = 1;
			}
			else
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x06000C05 RID: 3077 RVA: 0x00013404 File Offset: 0x00011604
		public override string ToString()
		{
			return Hash128.Internal_Hash128ToString(this);
		}

		// Token: 0x06000C06 RID: 3078 RVA: 0x00013424 File Offset: 0x00011624
		[FreeFunction("StringToHash128")]
		public static Hash128 Parse(string hashString)
		{
			Hash128 result;
			Hash128.Parse_Injected(hashString, out result);
			return result;
		}

		// Token: 0x06000C07 RID: 3079 RVA: 0x0001343A File Offset: 0x0001163A
		[FreeFunction("Hash128ToString")]
		internal static string Internal_Hash128ToString(Hash128 hash128)
		{
			return Hash128.Internal_Hash128ToString_Injected(ref hash128);
		}

		// Token: 0x06000C08 RID: 3080 RVA: 0x00013444 File Offset: 0x00011644
		[FreeFunction("ComputeHash128FromString")]
		public static Hash128 Compute(string hashString)
		{
			Hash128 result;
			Hash128.Compute_Injected(hashString, out result);
			return result;
		}

		// Token: 0x06000C09 RID: 3081 RVA: 0x0001345C File Offset: 0x0001165C
		public override bool Equals(object obj)
		{
			return obj is Hash128 && this == (Hash128)obj;
		}

		// Token: 0x06000C0A RID: 3082 RVA: 0x00013490 File Offset: 0x00011690
		public bool Equals(Hash128 obj)
		{
			return this == obj;
		}

		// Token: 0x06000C0B RID: 3083 RVA: 0x000134B4 File Offset: 0x000116B4
		public override int GetHashCode()
		{
			return this.m_u32_0.GetHashCode() ^ this.m_u32_1.GetHashCode() ^ this.m_u32_2.GetHashCode() ^ this.m_u32_3.GetHashCode();
		}

		// Token: 0x06000C0C RID: 3084 RVA: 0x00013510 File Offset: 0x00011710
		public int CompareTo(object obj)
		{
			int result;
			if (obj == null || !(obj is Hash128))
			{
				result = 1;
			}
			else
			{
				Hash128 rhs = (Hash128)obj;
				result = this.CompareTo(rhs);
			}
			return result;
		}

		// Token: 0x06000C0D RID: 3085 RVA: 0x0001354C File Offset: 0x0001174C
		public static bool operator ==(Hash128 hash1, Hash128 hash2)
		{
			return hash1.m_u32_0 == hash2.m_u32_0 && hash1.m_u32_1 == hash2.m_u32_1 && hash1.m_u32_2 == hash2.m_u32_2 && hash1.m_u32_3 == hash2.m_u32_3;
		}

		// Token: 0x06000C0E RID: 3086 RVA: 0x000135B0 File Offset: 0x000117B0
		public static bool operator !=(Hash128 hash1, Hash128 hash2)
		{
			return !(hash1 == hash2);
		}

		// Token: 0x06000C0F RID: 3087 RVA: 0x000135D0 File Offset: 0x000117D0
		public static bool operator <(Hash128 x, Hash128 y)
		{
			bool result;
			if (x.m_u32_0 != y.m_u32_0)
			{
				result = (x.m_u32_0 < y.m_u32_0);
			}
			else if (x.m_u32_1 != y.m_u32_1)
			{
				result = (x.m_u32_1 < y.m_u32_1);
			}
			else if (x.m_u32_2 != y.m_u32_2)
			{
				result = (x.m_u32_2 < y.m_u32_2);
			}
			else
			{
				result = (x.m_u32_3 < y.m_u32_3);
			}
			return result;
		}

		// Token: 0x06000C10 RID: 3088 RVA: 0x00013670 File Offset: 0x00011870
		public static bool operator >(Hash128 x, Hash128 y)
		{
			return !(x < y) && !(x == y);
		}

		// Token: 0x06000C11 RID: 3089
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Parse_Injected(string hashString, out Hash128 ret);

		// Token: 0x06000C12 RID: 3090
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string Internal_Hash128ToString_Injected(ref Hash128 hash128);

		// Token: 0x06000C13 RID: 3091
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Compute_Injected(string hashString, out Hash128 ret);

		// Token: 0x04000513 RID: 1299
		private uint m_u32_0;

		// Token: 0x04000514 RID: 1300
		private uint m_u32_1;

		// Token: 0x04000515 RID: 1301
		private uint m_u32_2;

		// Token: 0x04000516 RID: 1302
		private uint m_u32_3;
	}
}
