﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000235 RID: 565
	[NativeHeader("PlatformDependent/iPhonePlayer/IOSScriptBindings.h")]
	internal sealed class UnhandledExceptionHandler
	{
		// Token: 0x06001473 RID: 5235 RVA: 0x00002370 File Offset: 0x00000570
		public UnhandledExceptionHandler()
		{
		}

		// Token: 0x06001474 RID: 5236 RVA: 0x00022C0C File Offset: 0x00020E0C
		[RequiredByNativeCode]
		private static void RegisterUECatcher()
		{
			AppDomain.CurrentDomain.UnhandledException += delegate(object sender, UnhandledExceptionEventArgs e)
			{
				Debug.LogException(e.ExceptionObject as Exception);
			};
		}

		// Token: 0x06001475 RID: 5237 RVA: 0x00022C36 File Offset: 0x00020E36
		[CompilerGenerated]
		private static void <RegisterUECatcher>m__0(object sender, UnhandledExceptionEventArgs e)
		{
			Debug.LogException(e.ExceptionObject as Exception);
		}

		// Token: 0x040007CE RID: 1998
		[CompilerGenerated]
		private static UnhandledExceptionEventHandler <>f__am$cache0;
	}
}
