﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035C RID: 860
	public enum SpriteMaskInteraction
	{
		// Token: 0x04000AE8 RID: 2792
		None,
		// Token: 0x04000AE9 RID: 2793
		VisibleInsideMask,
		// Token: 0x04000AEA RID: 2794
		VisibleOutsideMask
	}
}
