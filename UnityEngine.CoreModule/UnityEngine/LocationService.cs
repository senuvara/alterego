﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200012A RID: 298
	[NativeHeader("Runtime/Input/InputBindings.h")]
	[NativeHeader("Runtime/Input/LocationService.h")]
	public class LocationService
	{
		// Token: 0x06000C59 RID: 3161 RVA: 0x00002370 File Offset: 0x00000570
		public LocationService()
		{
		}

		// Token: 0x06000C5A RID: 3162
		[FreeFunction("LocationService::IsServiceEnabledByUser")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool IsServiceEnabledByUser();

		// Token: 0x06000C5B RID: 3163
		[FreeFunction("LocationService::GetLocationStatus")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern LocationServiceStatus GetLocationStatus();

		// Token: 0x06000C5C RID: 3164 RVA: 0x00013C98 File Offset: 0x00011E98
		[FreeFunction("LocationService::GetLastLocation")]
		internal static LocationInfo GetLastLocation()
		{
			LocationInfo result;
			LocationService.GetLastLocation_Injected(out result);
			return result;
		}

		// Token: 0x06000C5D RID: 3165
		[FreeFunction("LocationService::SetDesiredAccuracy")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetDesiredAccuracy(float value);

		// Token: 0x06000C5E RID: 3166
		[FreeFunction("LocationService::SetDistanceFilter")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetDistanceFilter(float value);

		// Token: 0x06000C5F RID: 3167
		[FreeFunction("LocationService::StartUpdatingLocation")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void StartUpdatingLocation();

		// Token: 0x06000C60 RID: 3168
		[FreeFunction("LocationService::StopUpdatingLocation")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void StopUpdatingLocation();

		// Token: 0x06000C61 RID: 3169 RVA: 0x00013CB0 File Offset: 0x00011EB0
		[FreeFunction("LocationService::GetLastHeading")]
		internal static LocationService.HeadingInfo GetLastHeading()
		{
			LocationService.HeadingInfo result;
			LocationService.GetLastHeading_Injected(out result);
			return result;
		}

		// Token: 0x06000C62 RID: 3170
		[FreeFunction("LocationService::IsHeadingUpdatesEnabled")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool IsHeadingUpdatesEnabled();

		// Token: 0x06000C63 RID: 3171
		[FreeFunction("LocationService::SetHeadingUpdatesEnabled")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetHeadingUpdatesEnabled(bool value);

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x06000C64 RID: 3172 RVA: 0x00013CC8 File Offset: 0x00011EC8
		public bool isEnabledByUser
		{
			get
			{
				return LocationService.IsServiceEnabledByUser();
			}
		}

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x06000C65 RID: 3173 RVA: 0x00013CE4 File Offset: 0x00011EE4
		public LocationServiceStatus status
		{
			get
			{
				return LocationService.GetLocationStatus();
			}
		}

		// Token: 0x17000265 RID: 613
		// (get) Token: 0x06000C66 RID: 3174 RVA: 0x00013D00 File Offset: 0x00011F00
		public LocationInfo lastData
		{
			get
			{
				if (this.status != LocationServiceStatus.Running)
				{
					Debug.Log("Location service updates are not enabled. Check LocationService.status before querying last location.");
				}
				return LocationService.GetLastLocation();
			}
		}

		// Token: 0x06000C67 RID: 3175 RVA: 0x00013D30 File Offset: 0x00011F30
		public void Start(float desiredAccuracyInMeters, float updateDistanceInMeters)
		{
			LocationService.SetDesiredAccuracy(desiredAccuracyInMeters);
			LocationService.SetDistanceFilter(updateDistanceInMeters);
			LocationService.StartUpdatingLocation();
		}

		// Token: 0x06000C68 RID: 3176 RVA: 0x00013D44 File Offset: 0x00011F44
		public void Start(float desiredAccuracyInMeters)
		{
			this.Start(desiredAccuracyInMeters, 10f);
		}

		// Token: 0x06000C69 RID: 3177 RVA: 0x00013D53 File Offset: 0x00011F53
		public void Start()
		{
			this.Start(10f, 10f);
		}

		// Token: 0x06000C6A RID: 3178 RVA: 0x00013D66 File Offset: 0x00011F66
		public void Stop()
		{
			LocationService.StopUpdatingLocation();
		}

		// Token: 0x06000C6B RID: 3179
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLastLocation_Injected(out LocationInfo ret);

		// Token: 0x06000C6C RID: 3180
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLastHeading_Injected(out LocationService.HeadingInfo ret);

		// Token: 0x0200012B RID: 299
		internal struct HeadingInfo
		{
			// Token: 0x0400054B RID: 1355
			public float magneticHeading;

			// Token: 0x0400054C RID: 1356
			public float trueHeading;

			// Token: 0x0400054D RID: 1357
			public float headingAccuracy;

			// Token: 0x0400054E RID: 1358
			public Vector3 raw;

			// Token: 0x0400054F RID: 1359
			public double timestamp;
		}
	}
}
