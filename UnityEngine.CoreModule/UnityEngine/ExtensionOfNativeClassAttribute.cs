﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000310 RID: 784
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = true)]
	internal sealed class ExtensionOfNativeClassAttribute : Attribute
	{
		// Token: 0x06001B22 RID: 6946 RVA: 0x0000898B File Offset: 0x00006B8B
		public ExtensionOfNativeClassAttribute()
		{
		}
	}
}
