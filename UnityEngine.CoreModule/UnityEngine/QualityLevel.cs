﻿using System;

namespace UnityEngine
{
	// Token: 0x020000BA RID: 186
	[Obsolete("See QualitySettings.names, QualitySettings.SetQualityLevel, and QualitySettings.GetQualityLevel")]
	public enum QualityLevel
	{
		// Token: 0x040001E8 RID: 488
		Fastest,
		// Token: 0x040001E9 RID: 489
		Fast,
		// Token: 0x040001EA RID: 490
		Simple,
		// Token: 0x040001EB RID: 491
		Good,
		// Token: 0x040001EC RID: 492
		Beautiful,
		// Token: 0x040001ED RID: 493
		Fantastic
	}
}
