﻿using System;

namespace UnityEngine
{
	// Token: 0x02000039 RID: 57
	public enum SystemLanguage
	{
		// Token: 0x040000AE RID: 174
		Afrikaans,
		// Token: 0x040000AF RID: 175
		Arabic,
		// Token: 0x040000B0 RID: 176
		Basque,
		// Token: 0x040000B1 RID: 177
		Belarusian,
		// Token: 0x040000B2 RID: 178
		Bulgarian,
		// Token: 0x040000B3 RID: 179
		Catalan,
		// Token: 0x040000B4 RID: 180
		Chinese,
		// Token: 0x040000B5 RID: 181
		Czech,
		// Token: 0x040000B6 RID: 182
		Danish,
		// Token: 0x040000B7 RID: 183
		Dutch,
		// Token: 0x040000B8 RID: 184
		English,
		// Token: 0x040000B9 RID: 185
		Estonian,
		// Token: 0x040000BA RID: 186
		Faroese,
		// Token: 0x040000BB RID: 187
		Finnish,
		// Token: 0x040000BC RID: 188
		French,
		// Token: 0x040000BD RID: 189
		German,
		// Token: 0x040000BE RID: 190
		Greek,
		// Token: 0x040000BF RID: 191
		Hebrew,
		// Token: 0x040000C0 RID: 192
		Icelandic = 19,
		// Token: 0x040000C1 RID: 193
		Indonesian,
		// Token: 0x040000C2 RID: 194
		Italian,
		// Token: 0x040000C3 RID: 195
		Japanese,
		// Token: 0x040000C4 RID: 196
		Korean,
		// Token: 0x040000C5 RID: 197
		Latvian,
		// Token: 0x040000C6 RID: 198
		Lithuanian,
		// Token: 0x040000C7 RID: 199
		Norwegian,
		// Token: 0x040000C8 RID: 200
		Polish,
		// Token: 0x040000C9 RID: 201
		Portuguese,
		// Token: 0x040000CA RID: 202
		Romanian,
		// Token: 0x040000CB RID: 203
		Russian,
		// Token: 0x040000CC RID: 204
		SerboCroatian,
		// Token: 0x040000CD RID: 205
		Slovak,
		// Token: 0x040000CE RID: 206
		Slovenian,
		// Token: 0x040000CF RID: 207
		Spanish,
		// Token: 0x040000D0 RID: 208
		Swedish,
		// Token: 0x040000D1 RID: 209
		Thai,
		// Token: 0x040000D2 RID: 210
		Turkish,
		// Token: 0x040000D3 RID: 211
		Ukrainian,
		// Token: 0x040000D4 RID: 212
		Vietnamese,
		// Token: 0x040000D5 RID: 213
		ChineseSimplified,
		// Token: 0x040000D6 RID: 214
		ChineseTraditional,
		// Token: 0x040000D7 RID: 215
		Unknown,
		// Token: 0x040000D8 RID: 216
		Hungarian = 18
	}
}
