﻿using System;

namespace UnityEngine.Sprites
{
	// Token: 0x02000359 RID: 857
	public sealed class DataUtility
	{
		// Token: 0x06001D4F RID: 7503 RVA: 0x00002370 File Offset: 0x00000570
		public DataUtility()
		{
		}

		// Token: 0x06001D50 RID: 7504 RVA: 0x00032F28 File Offset: 0x00031128
		public static Vector4 GetInnerUV(Sprite sprite)
		{
			return sprite.GetInnerUVs();
		}

		// Token: 0x06001D51 RID: 7505 RVA: 0x00032F44 File Offset: 0x00031144
		public static Vector4 GetOuterUV(Sprite sprite)
		{
			return sprite.GetOuterUVs();
		}

		// Token: 0x06001D52 RID: 7506 RVA: 0x00032F60 File Offset: 0x00031160
		public static Vector4 GetPadding(Sprite sprite)
		{
			return sprite.GetPadding();
		}

		// Token: 0x06001D53 RID: 7507 RVA: 0x00032F7C File Offset: 0x0003117C
		public static Vector2 GetMinSize(Sprite sprite)
		{
			Vector2 result;
			result.x = sprite.border.x + sprite.border.z;
			result.y = sprite.border.y + sprite.border.w;
			return result;
		}
	}
}
