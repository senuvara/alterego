﻿using System;

namespace UnityEngine
{
	// Token: 0x02000008 RID: 8
	public class AndroidJavaClass : AndroidJavaObject
	{
		// Token: 0x0600013B RID: 315 RVA: 0x000053AC File Offset: 0x000035AC
		public AndroidJavaClass(string className)
		{
			this._AndroidJavaClass(className);
		}

		// Token: 0x0600013C RID: 316 RVA: 0x000053BC File Offset: 0x000035BC
		internal AndroidJavaClass(IntPtr jclass)
		{
			if (jclass == IntPtr.Zero)
			{
				throw new Exception("JNI: Init'd AndroidJavaClass with null ptr!");
			}
			this.m_jclass = new GlobalJavaObjectRef(jclass);
			this.m_jobject = new GlobalJavaObjectRef(IntPtr.Zero);
		}

		// Token: 0x0600013D RID: 317 RVA: 0x00005408 File Offset: 0x00003608
		private void _AndroidJavaClass(string className)
		{
			base.DebugPrint("Creating AndroidJavaClass from " + className);
			using (AndroidJavaObject androidJavaObject = AndroidJavaObject.FindClass(className))
			{
				this.m_jclass = new GlobalJavaObjectRef(androidJavaObject.GetRawObject());
				this.m_jobject = new GlobalJavaObjectRef(IntPtr.Zero);
			}
		}
	}
}
