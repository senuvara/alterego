﻿using System;

namespace UnityEngine
{
	// Token: 0x02000327 RID: 807
	public class iPhoneSettings
	{
		// Token: 0x06001B80 RID: 7040 RVA: 0x00002370 File Offset: 0x00000570
		public iPhoneSettings()
		{
		}

		// Token: 0x17000512 RID: 1298
		// (get) Token: 0x06001B81 RID: 7041 RVA: 0x00030A08 File Offset: 0x0002EC08
		[Obsolete("verticalOrientation property is deprecated. Please use Screen.orientation == ScreenOrientation.Portrait instead.", false)]
		public static bool verticalOrientation
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000513 RID: 1299
		// (get) Token: 0x06001B82 RID: 7042 RVA: 0x00030A20 File Offset: 0x0002EC20
		[Obsolete("screenCanDarken property is deprecated. Please use (Screen.sleepTimeout != SleepTimeout.NeverSleep) instead.", false)]
		public static bool screenCanDarken
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06001B83 RID: 7043 RVA: 0x00030A36 File Offset: 0x0002EC36
		[Obsolete("StartLocationServiceUpdates method is deprecated. Please use Input.location.Start instead.", false)]
		public static void StartLocationServiceUpdates(float desiredAccuracyInMeters, float updateDistanceInMeters)
		{
			Input.location.Start(desiredAccuracyInMeters, updateDistanceInMeters);
		}

		// Token: 0x06001B84 RID: 7044 RVA: 0x00030A45 File Offset: 0x0002EC45
		[Obsolete("StartLocationServiceUpdates method is deprecated. Please use Input.location.Start instead.", false)]
		public static void StartLocationServiceUpdates(float desiredAccuracyInMeters)
		{
			Input.location.Start(desiredAccuracyInMeters);
		}

		// Token: 0x06001B85 RID: 7045 RVA: 0x00030A53 File Offset: 0x0002EC53
		[Obsolete("StartLocationServiceUpdates method is deprecated. Please use Input.location.Start instead.", false)]
		public static void StartLocationServiceUpdates()
		{
			Input.location.Start();
		}

		// Token: 0x06001B86 RID: 7046 RVA: 0x00030A60 File Offset: 0x0002EC60
		[Obsolete("StopLocationServiceUpdates method is deprecated. Please use Input.location.Stop instead.", false)]
		public static void StopLocationServiceUpdates()
		{
			Input.location.Stop();
		}

		// Token: 0x17000514 RID: 1300
		// (get) Token: 0x06001B87 RID: 7047 RVA: 0x00030A70 File Offset: 0x0002EC70
		[Obsolete("locationServiceEnabledByUser property is deprecated. Please use Input.location.isEnabledByUser instead.", false)]
		public static bool locationServiceEnabledByUser
		{
			get
			{
				return Input.location.isEnabledByUser;
			}
		}
	}
}
