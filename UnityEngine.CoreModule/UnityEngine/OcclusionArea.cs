﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020000A5 RID: 165
	[NativeHeader("Runtime/Camera/OcclusionArea.h")]
	public sealed class OcclusionArea : Component
	{
		// Token: 0x06000A9F RID: 2719 RVA: 0x00008E34 File Offset: 0x00007034
		public OcclusionArea()
		{
		}

		// Token: 0x170001DF RID: 479
		// (get) Token: 0x06000AA0 RID: 2720 RVA: 0x00012304 File Offset: 0x00010504
		// (set) Token: 0x06000AA1 RID: 2721 RVA: 0x0001231A File Offset: 0x0001051A
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.get_center_Injected(out result);
				return result;
			}
			set
			{
				this.set_center_Injected(ref value);
			}
		}

		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x06000AA2 RID: 2722 RVA: 0x00012324 File Offset: 0x00010524
		// (set) Token: 0x06000AA3 RID: 2723 RVA: 0x0001233A File Offset: 0x0001053A
		public Vector3 size
		{
			get
			{
				Vector3 result;
				this.get_size_Injected(out result);
				return result;
			}
			set
			{
				this.set_size_Injected(ref value);
			}
		}

		// Token: 0x06000AA4 RID: 2724
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_center_Injected(out Vector3 ret);

		// Token: 0x06000AA5 RID: 2725
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_center_Injected(ref Vector3 value);

		// Token: 0x06000AA6 RID: 2726
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_size_Injected(out Vector3 ret);

		// Token: 0x06000AA7 RID: 2727
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_size_Injected(ref Vector3 value);
	}
}
