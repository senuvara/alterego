﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000033 RID: 51
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	[UsedByNativeCode]
	public class ExcludeFromPresetAttribute : Attribute
	{
		// Token: 0x06000294 RID: 660 RVA: 0x0000898B File Offset: 0x00006B8B
		public ExcludeFromPresetAttribute()
		{
		}
	}
}
