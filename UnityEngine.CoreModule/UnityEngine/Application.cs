﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Diagnostics;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000015 RID: 21
	[NativeHeader("Runtime/Misc/Player.h")]
	[NativeHeader("Runtime/Utilities/URLUtility.h")]
	[NativeHeader("Runtime/File/ApplicationSpecificPersistentDataPath.h")]
	[NativeHeader("Runtime/Application/AdsIdHandler.h")]
	[NativeHeader("Runtime/Application/ApplicationInfo.h")]
	[NativeHeader("Runtime/BaseClasses/IsPlaying.h")]
	[NativeHeader("Runtime/Export/Application.bindings.h")]
	[NativeHeader("Runtime/Network/NetworkUtility.h")]
	[NativeHeader("Runtime/Input/InputManager.h")]
	[NativeHeader("Runtime/Misc/BuildSettings.h")]
	[NativeHeader("Runtime/Misc/PlayerSettings.h")]
	[NativeHeader("Runtime/Misc/SystemInfo.h")]
	[NativeHeader("Runtime/PreloadManager/LoadSceneOperation.h")]
	[NativeHeader("Runtime/PreloadManager/PreloadManager.h")]
	[NativeHeader("Runtime/Utilities/Argv.h")]
	[NativeHeader("Runtime/Logging/LogSystem.h")]
	[NativeHeader("Runtime/Input/GetInput.h")]
	public class Application
	{
		// Token: 0x060001AA RID: 426 RVA: 0x00002370 File Offset: 0x00000570
		public Application()
		{
		}

		// Token: 0x060001AB RID: 427
		[FreeFunction("GetInputManager().QuitApplication")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Quit(int exitCode);

		// Token: 0x060001AC RID: 428 RVA: 0x000077A2 File Offset: 0x000059A2
		public static void Quit()
		{
			Application.Quit(0);
		}

		// Token: 0x060001AD RID: 429
		[Obsolete("CancelQuit is deprecated. Use the wantsToQuit event instead.")]
		[FreeFunction("GetInputManager().CancelQuitApplication")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void CancelQuit();

		// Token: 0x060001AE RID: 430
		[FreeFunction("Application_Bindings::Unload")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Unload();

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x060001AF RID: 431
		[Obsolete("This property is deprecated, please use LoadLevelAsync to detect if a specific scene is currently loading.")]
		public static extern bool isLoadingLevel { [FreeFunction("GetPreloadManager().IsLoadingOrQueued")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060001B0 RID: 432 RVA: 0x000077AC File Offset: 0x000059AC
		[Obsolete("Streaming was a Unity Web Player feature, and is removed. This function is deprecated and always returns 1.0 for valid level indices.")]
		public static float GetStreamProgressForLevel(int levelIndex)
		{
			float result;
			if (levelIndex >= 0 && levelIndex < SceneManager.sceneCountInBuildSettings)
			{
				result = 1f;
			}
			else
			{
				result = 0f;
			}
			return result;
		}

		// Token: 0x060001B1 RID: 433 RVA: 0x000077E4 File Offset: 0x000059E4
		[Obsolete("Streaming was a Unity Web Player feature, and is removed. This function is deprecated and always returns 1.0.")]
		public static float GetStreamProgressForLevel(string levelName)
		{
			return 1f;
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x060001B2 RID: 434 RVA: 0x00007800 File Offset: 0x00005A00
		[Obsolete("Streaming was a Unity Web Player feature, and is removed. This property is deprecated and always returns 0.")]
		public static int streamedBytes
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x060001B3 RID: 435 RVA: 0x00007818 File Offset: 0x00005A18
		[Obsolete("Application.webSecurityEnabled is no longer supported, since the Unity Web Player is no longer supported by Unity", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool webSecurityEnabled
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x00007830 File Offset: 0x00005A30
		public static bool CanStreamedLevelBeLoaded(int levelIndex)
		{
			return levelIndex >= 0 && levelIndex < SceneManager.sceneCountInBuildSettings;
		}

		// Token: 0x060001B5 RID: 437
		[FreeFunction("Application_Bindings::CanStreamedLevelBeLoaded")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool CanStreamedLevelBeLoaded(string levelName);

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x060001B6 RID: 438
		public static extern bool isPlaying { [FreeFunction("IsWorldPlaying")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060001B7 RID: 439
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsPlaying(Object obj);

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060001B8 RID: 440
		public static extern bool isFocused { [FreeFunction("IsPlayerFocused")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060001B9 RID: 441
		public static extern RuntimePlatform platform { [FreeFunction("systeminfo::GetRuntimePlatform", IsThreadSafe = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060001BA RID: 442
		[FreeFunction("GetBuildSettings().GetBuildTags")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string[] GetBuildTags();

		// Token: 0x060001BB RID: 443
		[FreeFunction("GetBuildSettings().SetBuildTags")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetBuildTags(string[] buildTags);

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060001BC RID: 444
		public static extern string buildGUID { [FreeFunction("Application_Bindings::GetBuildGUID")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060001BD RID: 445 RVA: 0x00007858 File Offset: 0x00005A58
		public static bool isMobilePlatform
		{
			get
			{
				RuntimePlatform platform = Application.platform;
				bool result;
				switch (platform)
				{
				case RuntimePlatform.MetroPlayerX86:
				case RuntimePlatform.MetroPlayerX64:
				case RuntimePlatform.MetroPlayerARM:
					result = (SystemInfo.deviceType == DeviceType.Handheld);
					break;
				default:
					switch (platform)
					{
					case RuntimePlatform.IPhonePlayer:
					case RuntimePlatform.Android:
						return true;
					}
					result = false;
					break;
				}
				return result;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060001BE RID: 446 RVA: 0x000078BC File Offset: 0x00005ABC
		public static bool isConsolePlatform
		{
			get
			{
				RuntimePlatform platform = Application.platform;
				return platform == RuntimePlatform.PS4 || platform == RuntimePlatform.XboxOne;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060001BF RID: 447
		// (set) Token: 0x060001C0 RID: 448
		public static extern bool runInBackground { [FreeFunction("GetPlayerSettingsRunInBackground")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("SetPlayerSettingsRunInBackground")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060001C1 RID: 449
		[FreeFunction("GetBuildSettings().GetHasPROVersion")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool HasProLicense();

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060001C2 RID: 450
		public static extern bool isBatchMode { [FreeFunction("::IsBatchmode")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060001C3 RID: 451
		internal static extern bool isTestRun { [FreeFunction("::IsTestRun")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060001C4 RID: 452
		internal static extern bool isHumanControllingUs { [FreeFunction("::IsHumanControllingUs")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060001C5 RID: 453
		[FreeFunction("HasARGV")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool HasARGV(string name);

		// Token: 0x060001C6 RID: 454
		[FreeFunction("GetFirstValueForARGV")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string GetValueForARGV(string name);

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060001C7 RID: 455
		public static extern string dataPath { [FreeFunction("GetAppDataPath")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060001C8 RID: 456
		public static extern string streamingAssetsPath { [FreeFunction("GetStreamingAssetsPath", IsThreadSafe = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060001C9 RID: 457
		[SecurityCritical]
		public static extern string persistentDataPath { [FreeFunction("GetPersistentDataPathApplicationSpecific")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060001CA RID: 458
		public static extern string temporaryCachePath { [FreeFunction("GetTemporaryCachePathApplicationSpecific")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060001CB RID: 459
		public static extern string absoluteURL { [FreeFunction("GetPlayerSettings().GetAbsoluteURL")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060001CC RID: 460 RVA: 0x000078E7 File Offset: 0x00005AE7
		[Obsolete("Application.ExternalEval is deprecated. See https://docs.unity3d.com/Manual/webgl-interactingwithbrowserscripting.html for alternatives.")]
		public static void ExternalEval(string script)
		{
			if (script.Length > 0 && script[script.Length - 1] != ';')
			{
				script += ';';
			}
			Application.Internal_ExternalCall(script);
		}

		// Token: 0x060001CD RID: 461
		[FreeFunction("Application_Bindings::ExternalCall")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_ExternalCall(string script);

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060001CE RID: 462
		public static extern string unityVersion { [FreeFunction("Application_Bindings::GetUnityVersion")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060001CF RID: 463
		public static extern string version { [FreeFunction("GetApplicationInfo().GetVersion")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060001D0 RID: 464
		public static extern string installerName { [FreeFunction("GetApplicationInfo().GetInstallerName")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060001D1 RID: 465
		public static extern string identifier { [FreeFunction("GetApplicationInfo().GetApplicationIdentifier")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060001D2 RID: 466
		public static extern ApplicationInstallMode installMode { [FreeFunction("GetApplicationInfo().GetInstallMode")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060001D3 RID: 467
		public static extern ApplicationSandboxType sandboxType { [FreeFunction("GetApplicationInfo().GetSandboxType")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060001D4 RID: 468
		public static extern string productName { [FreeFunction("GetPlayerSettings().GetProductName")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060001D5 RID: 469
		public static extern string companyName { [FreeFunction("GetPlayerSettings().GetCompanyName")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060001D6 RID: 470
		public static extern string cloudProjectId { [FreeFunction("GetPlayerSettings().GetCloudProjectId")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060001D7 RID: 471
		[FreeFunction("GetAdsIdHandler().RequestAdsIdAsync")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool RequestAdvertisingIdentifierAsync(Application.AdvertisingIdentifierCallback delegateMethod);

		// Token: 0x060001D8 RID: 472
		[FreeFunction("OpenURL")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void OpenURL(string url);

		// Token: 0x060001D9 RID: 473 RVA: 0x00007920 File Offset: 0x00005B20
		[Obsolete("Use UnityEngine.Diagnostics.Utils.ForceCrash")]
		public static void ForceCrash(int mode)
		{
			Utils.ForceCrash((ForcedCrashCategory)mode);
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060001DA RID: 474
		// (set) Token: 0x060001DB RID: 475
		public static extern int targetFrameRate { [FreeFunction("GetTargetFrameRate")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("SetTargetFrameRate")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060001DC RID: 476
		public static extern SystemLanguage systemLanguage { [FreeFunction("(SystemLanguage)systeminfo::GetSystemLanguage")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060001DD RID: 477
		[FreeFunction("Application_Bindings::SetLogCallbackDefined")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLogCallbackDefined(bool defined);

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060001DE RID: 478
		// (set) Token: 0x060001DF RID: 479
		[Obsolete("Use SetStackTraceLogType/GetStackTraceLogType instead")]
		public static extern StackTraceLogType stackTraceLogType { [FreeFunction("Application_Bindings::GetStackTraceLogType")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("Application_Bindings::SetStackTraceLogType")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060001E0 RID: 480
		[FreeFunction("GetStackTraceLogType")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern StackTraceLogType GetStackTraceLogType(LogType logType);

		// Token: 0x060001E1 RID: 481
		[FreeFunction("SetStackTraceLogType")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetStackTraceLogType(LogType logType, StackTraceLogType stackTraceType);

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060001E2 RID: 482
		public static extern string consoleLogPath { [FreeFunction("GetConsoleLogPath")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060001E3 RID: 483
		// (set) Token: 0x060001E4 RID: 484
		public static extern ThreadPriority backgroundLoadingPriority { [FreeFunction("GetPreloadManager().GetThreadPriority")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("GetPreloadManager().SetThreadPriority")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060001E5 RID: 485
		public static extern NetworkReachability internetReachability { [FreeFunction("GetInternetReachability")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060001E6 RID: 486
		public static extern bool genuine { [FreeFunction("IsApplicationGenuine")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060001E7 RID: 487
		public static extern bool genuineCheckAvailable { [FreeFunction("IsApplicationGenuineAvailable")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060001E8 RID: 488
		[FreeFunction("Application_Bindings::RequestUserAuthorization")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern AsyncOperation RequestUserAuthorization(UserAuthorization mode);

		// Token: 0x060001E9 RID: 489
		[FreeFunction("Application_Bindings::HasUserAuthorization")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool HasUserAuthorization(UserAuthorization mode);

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060001EA RID: 490
		internal static extern bool submitAnalytics { [FreeFunction("GetPlayerSettings().GetSubmitAnalytics")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060001EB RID: 491 RVA: 0x0000792C File Offset: 0x00005B2C
		[Obsolete("This property is deprecated, please use SplashScreen.isFinished instead")]
		public static bool isShowingSplashScreen
		{
			get
			{
				return !SplashScreen.isFinished;
			}
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x060001EC RID: 492 RVA: 0x0000794C File Offset: 0x00005B4C
		// (remove) Token: 0x060001ED RID: 493 RVA: 0x00007980 File Offset: 0x00005B80
		public static event Application.LowMemoryCallback lowMemory
		{
			add
			{
				Application.LowMemoryCallback lowMemoryCallback = Application.lowMemory;
				Application.LowMemoryCallback lowMemoryCallback2;
				do
				{
					lowMemoryCallback2 = lowMemoryCallback;
					lowMemoryCallback = Interlocked.CompareExchange<Application.LowMemoryCallback>(ref Application.lowMemory, (Application.LowMemoryCallback)Delegate.Combine(lowMemoryCallback2, value), lowMemoryCallback);
				}
				while (lowMemoryCallback != lowMemoryCallback2);
			}
			remove
			{
				Application.LowMemoryCallback lowMemoryCallback = Application.lowMemory;
				Application.LowMemoryCallback lowMemoryCallback2;
				do
				{
					lowMemoryCallback2 = lowMemoryCallback;
					lowMemoryCallback = Interlocked.CompareExchange<Application.LowMemoryCallback>(ref Application.lowMemory, (Application.LowMemoryCallback)Delegate.Remove(lowMemoryCallback2, value), lowMemoryCallback);
				}
				while (lowMemoryCallback != lowMemoryCallback2);
			}
		}

		// Token: 0x060001EE RID: 494 RVA: 0x000079B4 File Offset: 0x00005BB4
		[RequiredByNativeCode]
		private static void CallLowMemory()
		{
			Application.LowMemoryCallback lowMemoryCallback = Application.lowMemory;
			if (lowMemoryCallback != null)
			{
				lowMemoryCallback();
			}
		}

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x060001EF RID: 495 RVA: 0x000079D4 File Offset: 0x00005BD4
		// (remove) Token: 0x060001F0 RID: 496 RVA: 0x000079F2 File Offset: 0x00005BF2
		public static event Application.LogCallback logMessageReceived
		{
			add
			{
				Application.s_LogCallbackHandler = (Application.LogCallback)Delegate.Combine(Application.s_LogCallbackHandler, value);
				Application.SetLogCallbackDefined(true);
			}
			remove
			{
				Application.s_LogCallbackHandler = (Application.LogCallback)Delegate.Remove(Application.s_LogCallbackHandler, value);
			}
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x060001F1 RID: 497 RVA: 0x00007A0A File Offset: 0x00005C0A
		// (remove) Token: 0x060001F2 RID: 498 RVA: 0x00007A28 File Offset: 0x00005C28
		public static event Application.LogCallback logMessageReceivedThreaded
		{
			add
			{
				Application.s_LogCallbackHandlerThreaded = (Application.LogCallback)Delegate.Combine(Application.s_LogCallbackHandlerThreaded, value);
				Application.SetLogCallbackDefined(true);
			}
			remove
			{
				Application.s_LogCallbackHandlerThreaded = (Application.LogCallback)Delegate.Remove(Application.s_LogCallbackHandlerThreaded, value);
			}
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x00007A40 File Offset: 0x00005C40
		[RequiredByNativeCode]
		private static void CallLogCallback(string logString, string stackTrace, LogType type, bool invokedOnMainThread)
		{
			if (invokedOnMainThread)
			{
				Application.LogCallback logCallback = Application.s_LogCallbackHandler;
				if (logCallback != null)
				{
					logCallback(logString, stackTrace, type);
				}
			}
			Application.LogCallback logCallback2 = Application.s_LogCallbackHandlerThreaded;
			if (logCallback2 != null)
			{
				logCallback2(logString, stackTrace, type);
			}
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x00007A80 File Offset: 0x00005C80
		internal static void InvokeOnAdvertisingIdentifierCallback(string advertisingId, bool trackingEnabled)
		{
			if (Application.OnAdvertisingIdentifierCallback != null)
			{
				Application.OnAdvertisingIdentifierCallback(advertisingId, trackingEnabled, string.Empty);
			}
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x00007AA0 File Offset: 0x00005CA0
		private static string ObjectToJSString(object o)
		{
			string result;
			if (o == null)
			{
				result = "null";
			}
			else if (o is string)
			{
				string text = o.ToString().Replace("\\", "\\\\");
				text = text.Replace("\"", "\\\"");
				text = text.Replace("\n", "\\n");
				text = text.Replace("\r", "\\r");
				text = text.Replace("\0", "");
				text = text.Replace("\u2028", "");
				text = text.Replace("\u2029", "");
				result = '"' + text + '"';
			}
			else if (o is int || o is short || o is uint || o is ushort || o is byte)
			{
				result = o.ToString();
			}
			else if (o is float)
			{
				NumberFormatInfo numberFormat = CultureInfo.InvariantCulture.NumberFormat;
				result = ((float)o).ToString(numberFormat);
			}
			else if (o is double)
			{
				NumberFormatInfo numberFormat2 = CultureInfo.InvariantCulture.NumberFormat;
				result = ((double)o).ToString(numberFormat2);
			}
			else if (o is char)
			{
				if ((char)o == '"')
				{
					result = "\"\\\"\"";
				}
				else
				{
					result = '"' + o.ToString() + '"';
				}
			}
			else if (o is IList)
			{
				IList list = (IList)o;
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("new Array(");
				int count = list.Count;
				for (int i = 0; i < count; i++)
				{
					if (i != 0)
					{
						stringBuilder.Append(", ");
					}
					stringBuilder.Append(Application.ObjectToJSString(list[i]));
				}
				stringBuilder.Append(")");
				result = stringBuilder.ToString();
			}
			else
			{
				result = Application.ObjectToJSString(o.ToString());
			}
			return result;
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x00007CE5 File Offset: 0x00005EE5
		[Obsolete("Application.ExternalCall is deprecated. See https://docs.unity3d.com/Manual/webgl-interactingwithbrowserscripting.html for alternatives.")]
		public static void ExternalCall(string functionName, params object[] args)
		{
			Application.Internal_ExternalCall(Application.BuildInvocationForArguments(functionName, args));
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x00007CF4 File Offset: 0x00005EF4
		private static string BuildInvocationForArguments(string functionName, params object[] args)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(functionName);
			stringBuilder.Append('(');
			int num = args.Length;
			for (int i = 0; i < num; i++)
			{
				if (i != 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append(Application.ObjectToJSString(args[i]));
			}
			stringBuilder.Append(')');
			stringBuilder.Append(';');
			return stringBuilder.ToString();
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060001F8 RID: 504 RVA: 0x00007D74 File Offset: 0x00005F74
		[Obsolete("use Application.isEditor instead")]
		public static bool isPlayer
		{
			get
			{
				return !Application.isEditor;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060001F9 RID: 505 RVA: 0x00007D94 File Offset: 0x00005F94
		public static bool isEditor
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060001FA RID: 506 RVA: 0x00007DAA File Offset: 0x00005FAA
		[Obsolete("Use Object.DontDestroyOnLoad instead")]
		public static void DontDestroyOnLoad(Object o)
		{
			if (o != null)
			{
				Object.DontDestroyOnLoad(o);
			}
		}

		// Token: 0x060001FB RID: 507 RVA: 0x00007DBF File Offset: 0x00005FBF
		[Obsolete("Application.CaptureScreenshot is obsolete. Use ScreenCapture.CaptureScreenshot instead (UnityUpgradable) -> [UnityEngine] UnityEngine.ScreenCapture.CaptureScreenshot(*)", true)]
		public static void CaptureScreenshot(string filename, int superSize)
		{
			throw new NotSupportedException("Application.CaptureScreenshot is obsolete. Use ScreenCapture.CaptureScreenshot instead.");
		}

		// Token: 0x060001FC RID: 508 RVA: 0x00007DBF File Offset: 0x00005FBF
		[Obsolete("Application.CaptureScreenshot is obsolete. Use ScreenCapture.CaptureScreenshot instead (UnityUpgradable) -> [UnityEngine] UnityEngine.ScreenCapture.CaptureScreenshot(*)", true)]
		public static void CaptureScreenshot(string filename)
		{
			throw new NotSupportedException("Application.CaptureScreenshot is obsolete. Use ScreenCapture.CaptureScreenshot instead.");
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x060001FD RID: 509 RVA: 0x00007DCC File Offset: 0x00005FCC
		// (remove) Token: 0x060001FE RID: 510 RVA: 0x00007DD5 File Offset: 0x00005FD5
		public static event UnityAction onBeforeRender
		{
			add
			{
				BeforeRenderHelper.RegisterCallback(value);
			}
			remove
			{
				BeforeRenderHelper.UnregisterCallback(value);
			}
		}

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x060001FF RID: 511 RVA: 0x00007DE0 File Offset: 0x00005FE0
		// (remove) Token: 0x06000200 RID: 512 RVA: 0x00007E14 File Offset: 0x00006014
		public static event Action<bool> focusChanged
		{
			add
			{
				Action<bool> action = Application.focusChanged;
				Action<bool> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<bool>>(ref Application.focusChanged, (Action<bool>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<bool> action = Application.focusChanged;
				Action<bool> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<bool>>(ref Application.focusChanged, (Action<bool>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x06000201 RID: 513 RVA: 0x00007E48 File Offset: 0x00006048
		// (remove) Token: 0x06000202 RID: 514 RVA: 0x00007E7C File Offset: 0x0000607C
		public static event Func<bool> wantsToQuit
		{
			add
			{
				Func<bool> func = Application.wantsToQuit;
				Func<bool> func2;
				do
				{
					func2 = func;
					func = Interlocked.CompareExchange<Func<bool>>(ref Application.wantsToQuit, (Func<bool>)Delegate.Combine(func2, value), func);
				}
				while (func != func2);
			}
			remove
			{
				Func<bool> func = Application.wantsToQuit;
				Func<bool> func2;
				do
				{
					func2 = func;
					func = Interlocked.CompareExchange<Func<bool>>(ref Application.wantsToQuit, (Func<bool>)Delegate.Remove(func2, value), func);
				}
				while (func != func2);
			}
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x06000203 RID: 515 RVA: 0x00007EB0 File Offset: 0x000060B0
		// (remove) Token: 0x06000204 RID: 516 RVA: 0x00007EE4 File Offset: 0x000060E4
		public static event Action quitting
		{
			add
			{
				Action action = Application.quitting;
				Action action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action>(ref Application.quitting, (Action)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action action = Application.quitting;
				Action action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action>(ref Application.quitting, (Action)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x06000205 RID: 517 RVA: 0x00007F18 File Offset: 0x00006118
		[RequiredByNativeCode]
		private static bool Internal_ApplicationWantsToQuit()
		{
			if (Application.wantsToQuit != null)
			{
				foreach (Func<bool> func in Application.wantsToQuit.GetInvocationList())
				{
					try
					{
						if (!func())
						{
							return false;
						}
					}
					catch (Exception exception)
					{
						Debug.LogException(exception);
					}
				}
			}
			return true;
		}

		// Token: 0x06000206 RID: 518 RVA: 0x00007FA0 File Offset: 0x000061A0
		[RequiredByNativeCode]
		private static void Internal_ApplicationQuit()
		{
			if (Application.quitting != null)
			{
				Application.quitting();
			}
		}

		// Token: 0x06000207 RID: 519 RVA: 0x00007FB7 File Offset: 0x000061B7
		[RequiredByNativeCode]
		internal static void InvokeOnBeforeRender()
		{
			BeforeRenderHelper.Invoke();
		}

		// Token: 0x06000208 RID: 520 RVA: 0x00007FBF File Offset: 0x000061BF
		[RequiredByNativeCode]
		internal static void InvokeFocusChanged(bool focus)
		{
			if (Application.focusChanged != null)
			{
				Application.focusChanged(focus);
			}
		}

		// Token: 0x06000209 RID: 521 RVA: 0x00007FD7 File Offset: 0x000061D7
		[Obsolete("Application.RegisterLogCallback is deprecated. Use Application.logMessageReceived instead.")]
		public static void RegisterLogCallback(Application.LogCallback handler)
		{
			Application.RegisterLogCallback(handler, false);
		}

		// Token: 0x0600020A RID: 522 RVA: 0x00007FE1 File Offset: 0x000061E1
		[Obsolete("Application.RegisterLogCallbackThreaded is deprecated. Use Application.logMessageReceivedThreaded instead.")]
		public static void RegisterLogCallbackThreaded(Application.LogCallback handler)
		{
			Application.RegisterLogCallback(handler, true);
		}

		// Token: 0x0600020B RID: 523 RVA: 0x00007FEC File Offset: 0x000061EC
		private static void RegisterLogCallback(Application.LogCallback handler, bool threaded)
		{
			if (Application.s_RegisterLogCallbackDeprecated != null)
			{
				Application.logMessageReceived -= Application.s_RegisterLogCallbackDeprecated;
				Application.logMessageReceivedThreaded -= Application.s_RegisterLogCallbackDeprecated;
			}
			Application.s_RegisterLogCallbackDeprecated = handler;
			if (handler != null)
			{
				if (threaded)
				{
					Application.logMessageReceivedThreaded += handler;
				}
				else
				{
					Application.logMessageReceived += handler;
				}
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x0600020C RID: 524 RVA: 0x00008048 File Offset: 0x00006248
		[Obsolete("Use SceneManager.sceneCountInBuildSettings")]
		public static int levelCount
		{
			get
			{
				return SceneManager.sceneCountInBuildSettings;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x0600020D RID: 525 RVA: 0x00008064 File Offset: 0x00006264
		[Obsolete("Use SceneManager to determine what scenes have been loaded")]
		public static int loadedLevel
		{
			get
			{
				return SceneManager.GetActiveScene().buildIndex;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x0600020E RID: 526 RVA: 0x00008088 File Offset: 0x00006288
		[Obsolete("Use SceneManager to determine what scenes have been loaded")]
		public static string loadedLevelName
		{
			get
			{
				return SceneManager.GetActiveScene().name;
			}
		}

		// Token: 0x0600020F RID: 527 RVA: 0x000080AA File Offset: 0x000062AA
		[Obsolete("Use SceneManager.LoadScene")]
		public static void LoadLevel(int index)
		{
			SceneManager.LoadScene(index, LoadSceneMode.Single);
		}

		// Token: 0x06000210 RID: 528 RVA: 0x000080B4 File Offset: 0x000062B4
		[Obsolete("Use SceneManager.LoadScene")]
		public static void LoadLevel(string name)
		{
			SceneManager.LoadScene(name, LoadSceneMode.Single);
		}

		// Token: 0x06000211 RID: 529 RVA: 0x000080BE File Offset: 0x000062BE
		[Obsolete("Use SceneManager.LoadScene")]
		public static void LoadLevelAdditive(int index)
		{
			SceneManager.LoadScene(index, LoadSceneMode.Additive);
		}

		// Token: 0x06000212 RID: 530 RVA: 0x000080C8 File Offset: 0x000062C8
		[Obsolete("Use SceneManager.LoadScene")]
		public static void LoadLevelAdditive(string name)
		{
			SceneManager.LoadScene(name, LoadSceneMode.Additive);
		}

		// Token: 0x06000213 RID: 531 RVA: 0x000080D4 File Offset: 0x000062D4
		[Obsolete("Use SceneManager.LoadSceneAsync")]
		public static AsyncOperation LoadLevelAsync(int index)
		{
			return SceneManager.LoadSceneAsync(index, LoadSceneMode.Single);
		}

		// Token: 0x06000214 RID: 532 RVA: 0x000080F0 File Offset: 0x000062F0
		[Obsolete("Use SceneManager.LoadSceneAsync")]
		public static AsyncOperation LoadLevelAsync(string levelName)
		{
			return SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Single);
		}

		// Token: 0x06000215 RID: 533 RVA: 0x0000810C File Offset: 0x0000630C
		[Obsolete("Use SceneManager.LoadSceneAsync")]
		public static AsyncOperation LoadLevelAdditiveAsync(int index)
		{
			return SceneManager.LoadSceneAsync(index, LoadSceneMode.Additive);
		}

		// Token: 0x06000216 RID: 534 RVA: 0x00008128 File Offset: 0x00006328
		[Obsolete("Use SceneManager.LoadSceneAsync")]
		public static AsyncOperation LoadLevelAdditiveAsync(string levelName)
		{
			return SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
		}

		// Token: 0x06000217 RID: 535 RVA: 0x00008144 File Offset: 0x00006344
		[Obsolete("Use SceneManager.UnloadScene")]
		public static bool UnloadLevel(int index)
		{
			return SceneManager.UnloadScene(index);
		}

		// Token: 0x06000218 RID: 536 RVA: 0x00008160 File Offset: 0x00006360
		[Obsolete("Use SceneManager.UnloadScene")]
		public static bool UnloadLevel(string scenePath)
		{
			return SceneManager.UnloadScene(scenePath);
		}

		// Token: 0x04000036 RID: 54
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Application.LowMemoryCallback lowMemory;

		// Token: 0x04000037 RID: 55
		private static Application.LogCallback s_LogCallbackHandler;

		// Token: 0x04000038 RID: 56
		private static Application.LogCallback s_LogCallbackHandlerThreaded;

		// Token: 0x04000039 RID: 57
		internal static Application.AdvertisingIdentifierCallback OnAdvertisingIdentifierCallback;

		// Token: 0x0400003A RID: 58
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action<bool> focusChanged;

		// Token: 0x0400003B RID: 59
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Func<bool> wantsToQuit;

		// Token: 0x0400003C RID: 60
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action quitting;

		// Token: 0x0400003D RID: 61
		private static volatile Application.LogCallback s_RegisterLogCallbackDeprecated;

		// Token: 0x02000016 RID: 22
		// (Invoke) Token: 0x0600021A RID: 538
		public delegate void AdvertisingIdentifierCallback(string advertisingId, bool trackingEnabled, string errorMsg);

		// Token: 0x02000017 RID: 23
		// (Invoke) Token: 0x0600021E RID: 542
		public delegate void LowMemoryCallback();

		// Token: 0x02000018 RID: 24
		// (Invoke) Token: 0x06000222 RID: 546
		public delegate void LogCallback(string condition, string stackTrace, LogType type);
	}
}
