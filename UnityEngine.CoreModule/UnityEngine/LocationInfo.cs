﻿using System;

namespace UnityEngine
{
	// Token: 0x02000128 RID: 296
	public struct LocationInfo
	{
		// Token: 0x1700025D RID: 605
		// (get) Token: 0x06000C53 RID: 3155 RVA: 0x00013BF0 File Offset: 0x00011DF0
		public float latitude
		{
			get
			{
				return this.m_Latitude;
			}
		}

		// Token: 0x1700025E RID: 606
		// (get) Token: 0x06000C54 RID: 3156 RVA: 0x00013C0C File Offset: 0x00011E0C
		public float longitude
		{
			get
			{
				return this.m_Longitude;
			}
		}

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x06000C55 RID: 3157 RVA: 0x00013C28 File Offset: 0x00011E28
		public float altitude
		{
			get
			{
				return this.m_Altitude;
			}
		}

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x06000C56 RID: 3158 RVA: 0x00013C44 File Offset: 0x00011E44
		public float horizontalAccuracy
		{
			get
			{
				return this.m_HorizontalAccuracy;
			}
		}

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x06000C57 RID: 3159 RVA: 0x00013C60 File Offset: 0x00011E60
		public float verticalAccuracy
		{
			get
			{
				return this.m_VerticalAccuracy;
			}
		}

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x06000C58 RID: 3160 RVA: 0x00013C7C File Offset: 0x00011E7C
		public double timestamp
		{
			get
			{
				return this.m_Timestamp;
			}
		}

		// Token: 0x04000540 RID: 1344
		internal double m_Timestamp;

		// Token: 0x04000541 RID: 1345
		internal float m_Latitude;

		// Token: 0x04000542 RID: 1346
		internal float m_Longitude;

		// Token: 0x04000543 RID: 1347
		internal float m_Altitude;

		// Token: 0x04000544 RID: 1348
		internal float m_HorizontalAccuracy;

		// Token: 0x04000545 RID: 1349
		internal float m_VerticalAccuracy;
	}
}
