﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x020000AA RID: 170
	[NativeHeader("Runtime/Camera/SharedLightData.h")]
	public enum LightShadowCasterMode
	{
		// Token: 0x040001AA RID: 426
		Default,
		// Token: 0x040001AB RID: 427
		NonLightmappedOnly,
		// Token: 0x040001AC RID: 428
		Everything
	}
}
