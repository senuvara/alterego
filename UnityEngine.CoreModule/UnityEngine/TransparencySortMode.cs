﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B0 RID: 176
	public enum TransparencySortMode
	{
		// Token: 0x040001B5 RID: 437
		Default,
		// Token: 0x040001B6 RID: 438
		Perspective,
		// Token: 0x040001B7 RID: 439
		Orthographic,
		// Token: 0x040001B8 RID: 440
		CustomAxis
	}
}
