﻿using System;

namespace UnityEngine
{
	// Token: 0x02000236 RID: 566
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
	public class UnityAPICompatibilityVersionAttribute : Attribute
	{
		// Token: 0x06001476 RID: 5238 RVA: 0x00022C49 File Offset: 0x00020E49
		public UnityAPICompatibilityVersionAttribute(string version)
		{
			this._version = version;
		}

		// Token: 0x17000404 RID: 1028
		// (get) Token: 0x06001477 RID: 5239 RVA: 0x00022C5C File Offset: 0x00020E5C
		public string version
		{
			get
			{
				return this._version;
			}
		}

		// Token: 0x040007CF RID: 1999
		private string _version;
	}
}
