﻿using System;

namespace UnityEngine
{
	// Token: 0x0200034A RID: 842
	public struct DrivenRectTransformTracker
	{
		// Token: 0x06001C36 RID: 7222 RVA: 0x000319F8 File Offset: 0x0002FBF8
		internal static bool CanRecordModifications()
		{
			return true;
		}

		// Token: 0x06001C37 RID: 7223 RVA: 0x00007476 File Offset: 0x00005676
		public void Add(Object driver, RectTransform rectTransform, DrivenTransformProperties drivenProperties)
		{
		}

		// Token: 0x06001C38 RID: 7224 RVA: 0x00031A0E File Offset: 0x0002FC0E
		[Obsolete("revertValues parameter is ignored. Please use Clear() instead.")]
		public void Clear(bool revertValues)
		{
			this.Clear();
		}

		// Token: 0x06001C39 RID: 7225 RVA: 0x00007476 File Offset: 0x00005676
		public void Clear()
		{
		}
	}
}
