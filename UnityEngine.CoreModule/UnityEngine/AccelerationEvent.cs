﻿using System;

namespace UnityEngine
{
	// Token: 0x02000126 RID: 294
	public struct AccelerationEvent
	{
		// Token: 0x17000254 RID: 596
		// (get) Token: 0x06000C39 RID: 3129 RVA: 0x00013A20 File Offset: 0x00011C20
		public Vector3 acceleration
		{
			get
			{
				return new Vector3(this.x, this.y, this.z);
			}
		}

		// Token: 0x17000255 RID: 597
		// (get) Token: 0x06000C3A RID: 3130 RVA: 0x00013A4C File Offset: 0x00011C4C
		public float deltaTime
		{
			get
			{
				return this.m_TimeDelta;
			}
		}

		// Token: 0x0400053B RID: 1339
		internal float x;

		// Token: 0x0400053C RID: 1340
		internal float y;

		// Token: 0x0400053D RID: 1341
		internal float z;

		// Token: 0x0400053E RID: 1342
		internal float m_TimeDelta;
	}
}
