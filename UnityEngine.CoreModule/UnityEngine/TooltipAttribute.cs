﻿using System;

namespace UnityEngine
{
	// Token: 0x020001E8 RID: 488
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public class TooltipAttribute : PropertyAttribute
	{
		// Token: 0x06000F7F RID: 3967 RVA: 0x0001ACE6 File Offset: 0x00018EE6
		public TooltipAttribute(string tooltip)
		{
			this.tooltip = tooltip;
		}

		// Token: 0x0400071F RID: 1823
		public readonly string tooltip;
	}
}
