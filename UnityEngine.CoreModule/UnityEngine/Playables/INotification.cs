﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x02000286 RID: 646
	public interface INotification
	{
		// Token: 0x17000457 RID: 1111
		// (get) Token: 0x060016EB RID: 5867
		PropertyName id { get; }
	}
}
