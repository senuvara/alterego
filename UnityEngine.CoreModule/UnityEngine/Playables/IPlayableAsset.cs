﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Playables
{
	// Token: 0x0200028F RID: 655
	public interface IPlayableAsset
	{
		// Token: 0x06001712 RID: 5906
		Playable CreatePlayable(PlayableGraph graph, GameObject owner);

		// Token: 0x1700045A RID: 1114
		// (get) Token: 0x06001713 RID: 5907
		double duration { get; }

		// Token: 0x1700045B RID: 1115
		// (get) Token: 0x06001714 RID: 5908
		IEnumerable<PlayableBinding> outputs { get; }
	}
}
