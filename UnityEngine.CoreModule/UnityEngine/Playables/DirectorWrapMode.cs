﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x0200028D RID: 653
	public enum DirectorWrapMode
	{
		// Token: 0x04000869 RID: 2153
		Hold,
		// Token: 0x0400086A RID: 2154
		Loop,
		// Token: 0x0400086B RID: 2155
		None
	}
}
