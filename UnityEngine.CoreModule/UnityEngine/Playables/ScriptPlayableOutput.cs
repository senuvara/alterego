﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Playables
{
	// Token: 0x020002A0 RID: 672
	[RequiredByNativeCode]
	public struct ScriptPlayableOutput : IPlayableOutput
	{
		// Token: 0x0600185F RID: 6239 RVA: 0x0002A745 File Offset: 0x00028945
		internal ScriptPlayableOutput(PlayableOutputHandle handle)
		{
			if (handle.IsValid())
			{
				if (!handle.IsPlayableOutputOfType<ScriptPlayableOutput>())
				{
					throw new InvalidCastException("Can't set handle: the playable is not a ScriptPlayableOutput.");
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x06001860 RID: 6240 RVA: 0x0002A774 File Offset: 0x00028974
		public static ScriptPlayableOutput Create(PlayableGraph graph, string name)
		{
			PlayableOutputHandle handle;
			ScriptPlayableOutput result;
			if (!graph.CreateScriptOutputInternal(name, out handle))
			{
				result = ScriptPlayableOutput.Null;
			}
			else
			{
				result = new ScriptPlayableOutput(handle);
			}
			return result;
		}

		// Token: 0x17000467 RID: 1127
		// (get) Token: 0x06001861 RID: 6241 RVA: 0x0002A7AC File Offset: 0x000289AC
		public static ScriptPlayableOutput Null
		{
			get
			{
				return new ScriptPlayableOutput(PlayableOutputHandle.Null);
			}
		}

		// Token: 0x06001862 RID: 6242 RVA: 0x0002A7CC File Offset: 0x000289CC
		public PlayableOutputHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x06001863 RID: 6243 RVA: 0x0002A7E8 File Offset: 0x000289E8
		public static implicit operator PlayableOutput(ScriptPlayableOutput output)
		{
			return new PlayableOutput(output.GetHandle());
		}

		// Token: 0x06001864 RID: 6244 RVA: 0x0002A80C File Offset: 0x00028A0C
		public static explicit operator ScriptPlayableOutput(PlayableOutput output)
		{
			return new ScriptPlayableOutput(output.GetHandle());
		}

		// Token: 0x04000892 RID: 2194
		private PlayableOutputHandle m_Handle;
	}
}
