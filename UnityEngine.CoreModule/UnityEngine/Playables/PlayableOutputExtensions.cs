﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x0200029C RID: 668
	public static class PlayableOutputExtensions
	{
		// Token: 0x06001810 RID: 6160 RVA: 0x00029E80 File Offset: 0x00028080
		public static bool IsOutputNull<U>(this U output) where U : struct, IPlayableOutput
		{
			return output.GetHandle().IsNull();
		}

		// Token: 0x06001811 RID: 6161 RVA: 0x00029EAC File Offset: 0x000280AC
		public static bool IsOutputValid<U>(this U output) where U : struct, IPlayableOutput
		{
			return output.GetHandle().IsValid();
		}

		// Token: 0x06001812 RID: 6162 RVA: 0x00029ED8 File Offset: 0x000280D8
		public static Object GetReferenceObject<U>(this U output) where U : struct, IPlayableOutput
		{
			return output.GetHandle().GetReferenceObject();
		}

		// Token: 0x06001813 RID: 6163 RVA: 0x00029F04 File Offset: 0x00028104
		public static void SetReferenceObject<U>(this U output, Object value) where U : struct, IPlayableOutput
		{
			output.GetHandle().SetReferenceObject(value);
		}

		// Token: 0x06001814 RID: 6164 RVA: 0x00029F28 File Offset: 0x00028128
		public static Object GetUserData<U>(this U output) where U : struct, IPlayableOutput
		{
			return output.GetHandle().GetUserData();
		}

		// Token: 0x06001815 RID: 6165 RVA: 0x00029F54 File Offset: 0x00028154
		public static void SetUserData<U>(this U output, Object value) where U : struct, IPlayableOutput
		{
			output.GetHandle().SetUserData(value);
		}

		// Token: 0x06001816 RID: 6166 RVA: 0x00029F78 File Offset: 0x00028178
		public static Playable GetSourcePlayable<U>(this U output) where U : struct, IPlayableOutput
		{
			return new Playable(output.GetHandle().GetSourcePlayable());
		}

		// Token: 0x06001817 RID: 6167 RVA: 0x00029FA8 File Offset: 0x000281A8
		public static void SetSourcePlayable<U, V>(this U output, V value) where U : struct, IPlayableOutput where V : struct, IPlayable
		{
			output.GetHandle().SetSourcePlayable(value.GetHandle());
		}

		// Token: 0x06001818 RID: 6168 RVA: 0x00029FD8 File Offset: 0x000281D8
		public static void SetSourcePlayable<U, V>(this U output, V value, int port) where U : struct, IPlayableOutput where V : struct, IPlayable
		{
			PlayableOutputHandle handle = output.GetHandle();
			handle.SetSourcePlayable(value.GetHandle());
			handle.SetSourceOutputPort(port);
		}

		// Token: 0x06001819 RID: 6169 RVA: 0x0002A010 File Offset: 0x00028210
		public static int GetSourceOutputPort<U>(this U output) where U : struct, IPlayableOutput
		{
			return output.GetHandle().GetSourceOutputPort();
		}

		// Token: 0x0600181A RID: 6170 RVA: 0x0002A03C File Offset: 0x0002823C
		public static void SetSourceOutputPort<U>(this U output, int value) where U : struct, IPlayableOutput
		{
			output.GetHandle().SetSourceOutputPort(value);
		}

		// Token: 0x0600181B RID: 6171 RVA: 0x0002A060 File Offset: 0x00028260
		public static float GetWeight<U>(this U output) where U : struct, IPlayableOutput
		{
			return output.GetHandle().GetWeight();
		}

		// Token: 0x0600181C RID: 6172 RVA: 0x0002A08C File Offset: 0x0002828C
		public static void SetWeight<U>(this U output, float value) where U : struct, IPlayableOutput
		{
			output.GetHandle().SetWeight(value);
		}

		// Token: 0x0600181D RID: 6173 RVA: 0x0002A0B0 File Offset: 0x000282B0
		public static void PushNotification<U>(this U output, Playable origin, INotification notification, object context = null) where U : struct, IPlayableOutput
		{
			output.GetHandle().PushNotification(origin.GetHandle(), notification, context);
		}

		// Token: 0x0600181E RID: 6174 RVA: 0x0002A0DC File Offset: 0x000282DC
		public static INotificationReceiver[] GetNotificationReceivers<U>(this U output) where U : struct, IPlayableOutput
		{
			return output.GetHandle().GetNotificationReceivers();
		}

		// Token: 0x0600181F RID: 6175 RVA: 0x0002A108 File Offset: 0x00028308
		public static void AddNotificationReceiver<U>(this U output, INotificationReceiver receiver) where U : struct, IPlayableOutput
		{
			output.GetHandle().AddNotificationReceiver(receiver);
		}

		// Token: 0x06001820 RID: 6176 RVA: 0x0002A12C File Offset: 0x0002832C
		public static void RemoveNotificationReceiver<U>(this U output, INotificationReceiver receiver) where U : struct, IPlayableOutput
		{
			output.GetHandle().RemoveNotificationReceiver(receiver);
		}

		// Token: 0x06001821 RID: 6177 RVA: 0x0002A150 File Offset: 0x00028350
		[Obsolete("Method GetSourceInputPort has been renamed to GetSourceOutputPort (UnityUpgradable) -> GetSourceOutputPort<U>(*)", false)]
		public static int GetSourceInputPort<U>(this U output) where U : struct, IPlayableOutput
		{
			return output.GetHandle().GetSourceOutputPort();
		}

		// Token: 0x06001822 RID: 6178 RVA: 0x0002A17C File Offset: 0x0002837C
		[Obsolete("Method SetSourceInputPort has been renamed to SetSourceOutputPort (UnityUpgradable) -> SetSourceOutputPort<U>(*)", false)]
		public static void SetSourceInputPort<U>(this U output, int value) where U : struct, IPlayableOutput
		{
			output.GetHandle().SetSourceOutputPort(value);
		}
	}
}
