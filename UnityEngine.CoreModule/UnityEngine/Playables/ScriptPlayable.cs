﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x0200029E RID: 670
	public struct ScriptPlayable<T> : IPlayable, IEquatable<ScriptPlayable<T>> where T : class, IPlayableBehaviour, new()
	{
		// Token: 0x0600184E RID: 6222 RVA: 0x0002A3AC File Offset: 0x000285AC
		internal ScriptPlayable(PlayableHandle handle)
		{
			if (handle.IsValid())
			{
				if (!typeof(T).IsAssignableFrom(handle.GetPlayableType()))
				{
					throw new InvalidCastException(string.Format("Incompatible handle: Trying to assign a playable data of type `{0}` that is not compatible with the PlayableBehaviour of type `{1}`.", handle.GetPlayableType(), typeof(T)));
				}
			}
			this.m_Handle = handle;
		}

		// Token: 0x17000466 RID: 1126
		// (get) Token: 0x0600184F RID: 6223 RVA: 0x0002A40C File Offset: 0x0002860C
		public static ScriptPlayable<T> Null
		{
			get
			{
				return ScriptPlayable<T>.m_NullPlayable;
			}
		}

		// Token: 0x06001850 RID: 6224 RVA: 0x0002A428 File Offset: 0x00028628
		public static ScriptPlayable<T> Create(PlayableGraph graph, int inputCount = 0)
		{
			PlayableHandle handle = ScriptPlayable<T>.CreateHandle(graph, (T)((object)null), inputCount);
			return new ScriptPlayable<T>(handle);
		}

		// Token: 0x06001851 RID: 6225 RVA: 0x0002A454 File Offset: 0x00028654
		public static ScriptPlayable<T> Create(PlayableGraph graph, T template, int inputCount = 0)
		{
			PlayableHandle handle = ScriptPlayable<T>.CreateHandle(graph, template, inputCount);
			return new ScriptPlayable<T>(handle);
		}

		// Token: 0x06001852 RID: 6226 RVA: 0x0002A478 File Offset: 0x00028678
		private static PlayableHandle CreateHandle(PlayableGraph graph, T template, int inputCount)
		{
			object obj;
			if (template == null)
			{
				obj = ScriptPlayable<T>.CreateScriptInstance();
			}
			else
			{
				obj = ScriptPlayable<T>.CloneScriptInstance(template);
			}
			PlayableHandle result;
			if (obj == null)
			{
				Debug.LogError("Could not create a ScriptPlayable of Type " + typeof(T).ToString());
				result = PlayableHandle.Null;
			}
			else
			{
				PlayableHandle playableHandle = graph.CreatePlayableHandle();
				if (!playableHandle.IsValid())
				{
					result = PlayableHandle.Null;
				}
				else
				{
					playableHandle.SetInputCount(inputCount);
					playableHandle.SetScriptInstance(obj);
					result = playableHandle;
				}
			}
			return result;
		}

		// Token: 0x06001853 RID: 6227 RVA: 0x0002A518 File Offset: 0x00028718
		private static object CreateScriptInstance()
		{
			IPlayableBehaviour result;
			if (typeof(ScriptableObject).IsAssignableFrom(typeof(T)))
			{
				result = (ScriptableObject.CreateInstance(typeof(T)) as T);
			}
			else
			{
				result = Activator.CreateInstance<T>();
			}
			return result;
		}

		// Token: 0x06001854 RID: 6228 RVA: 0x0002A584 File Offset: 0x00028784
		private static object CloneScriptInstance(IPlayableBehaviour source)
		{
			Object @object = source as Object;
			object result;
			if (@object != null)
			{
				result = ScriptPlayable<T>.CloneScriptInstanceFromEngineObject(@object);
			}
			else
			{
				ICloneable cloneable = source as ICloneable;
				if (cloneable != null)
				{
					result = ScriptPlayable<T>.CloneScriptInstanceFromIClonable(cloneable);
				}
				else
				{
					result = null;
				}
			}
			return result;
		}

		// Token: 0x06001855 RID: 6229 RVA: 0x0002A5D4 File Offset: 0x000287D4
		private static object CloneScriptInstanceFromEngineObject(Object source)
		{
			Object @object = Object.Instantiate(source);
			if (@object != null)
			{
				@object.hideFlags |= HideFlags.DontSave;
			}
			return @object;
		}

		// Token: 0x06001856 RID: 6230 RVA: 0x0002A610 File Offset: 0x00028810
		private static object CloneScriptInstanceFromIClonable(ICloneable source)
		{
			return source.Clone();
		}

		// Token: 0x06001857 RID: 6231 RVA: 0x0002A62C File Offset: 0x0002882C
		public PlayableHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x06001858 RID: 6232 RVA: 0x0002A648 File Offset: 0x00028848
		public T GetBehaviour()
		{
			return this.m_Handle.GetObject<T>();
		}

		// Token: 0x06001859 RID: 6233 RVA: 0x0002A668 File Offset: 0x00028868
		public static implicit operator Playable(ScriptPlayable<T> playable)
		{
			return new Playable(playable.GetHandle());
		}

		// Token: 0x0600185A RID: 6234 RVA: 0x0002A68C File Offset: 0x0002888C
		public static explicit operator ScriptPlayable<T>(Playable playable)
		{
			return new ScriptPlayable<T>(playable.GetHandle());
		}

		// Token: 0x0600185B RID: 6235 RVA: 0x0002A6B0 File Offset: 0x000288B0
		public bool Equals(ScriptPlayable<T> other)
		{
			return this.GetHandle() == other.GetHandle();
		}

		// Token: 0x0600185C RID: 6236 RVA: 0x0002A6D7 File Offset: 0x000288D7
		// Note: this type is marked as 'beforefieldinit'.
		static ScriptPlayable()
		{
		}

		// Token: 0x0400088F RID: 2191
		private PlayableHandle m_Handle;

		// Token: 0x04000890 RID: 2192
		private static readonly ScriptPlayable<T> m_NullPlayable = new ScriptPlayable<T>(PlayableHandle.Null);
	}
}
