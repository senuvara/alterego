﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Playables
{
	// Token: 0x02000291 RID: 657
	[RequiredByNativeCode]
	[Serializable]
	public abstract class PlayableBehaviour : IPlayableBehaviour, ICloneable
	{
		// Token: 0x0600171B RID: 5915 RVA: 0x00002050 File Offset: 0x00000250
		public PlayableBehaviour()
		{
		}

		// Token: 0x0600171C RID: 5916 RVA: 0x00007476 File Offset: 0x00005676
		public virtual void OnGraphStart(Playable playable)
		{
		}

		// Token: 0x0600171D RID: 5917 RVA: 0x00007476 File Offset: 0x00005676
		public virtual void OnGraphStop(Playable playable)
		{
		}

		// Token: 0x0600171E RID: 5918 RVA: 0x00007476 File Offset: 0x00005676
		public virtual void OnPlayableCreate(Playable playable)
		{
		}

		// Token: 0x0600171F RID: 5919 RVA: 0x00007476 File Offset: 0x00005676
		public virtual void OnPlayableDestroy(Playable playable)
		{
		}

		// Token: 0x06001720 RID: 5920 RVA: 0x00007476 File Offset: 0x00005676
		public virtual void OnBehaviourDelay(Playable playable, FrameData info)
		{
		}

		// Token: 0x06001721 RID: 5921 RVA: 0x00007476 File Offset: 0x00005676
		public virtual void OnBehaviourPlay(Playable playable, FrameData info)
		{
		}

		// Token: 0x06001722 RID: 5922 RVA: 0x00007476 File Offset: 0x00005676
		public virtual void OnBehaviourPause(Playable playable, FrameData info)
		{
		}

		// Token: 0x06001723 RID: 5923 RVA: 0x00007476 File Offset: 0x00005676
		public virtual void PrepareData(Playable playable, FrameData info)
		{
		}

		// Token: 0x06001724 RID: 5924 RVA: 0x00007476 File Offset: 0x00005676
		public virtual void PrepareFrame(Playable playable, FrameData info)
		{
		}

		// Token: 0x06001725 RID: 5925 RVA: 0x00007476 File Offset: 0x00005676
		public virtual void ProcessFrame(Playable playable, FrameData info, object playerData)
		{
		}

		// Token: 0x06001726 RID: 5926 RVA: 0x00028DC0 File Offset: 0x00026FC0
		public virtual object Clone()
		{
			return base.MemberwiseClone();
		}
	}
}
