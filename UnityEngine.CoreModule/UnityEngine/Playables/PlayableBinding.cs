﻿using System;
using System.ComponentModel;
using UnityEngine.Bindings;

namespace UnityEngine.Playables
{
	// Token: 0x02000293 RID: 659
	public struct PlayableBinding
	{
		// Token: 0x1700045E RID: 1118
		// (get) Token: 0x06001727 RID: 5927 RVA: 0x00028DDC File Offset: 0x00026FDC
		// (set) Token: 0x06001728 RID: 5928 RVA: 0x00028DF7 File Offset: 0x00026FF7
		public string streamName
		{
			get
			{
				return this.m_StreamName;
			}
			set
			{
				this.m_StreamName = value;
			}
		}

		// Token: 0x1700045F RID: 1119
		// (get) Token: 0x06001729 RID: 5929 RVA: 0x00028E04 File Offset: 0x00027004
		// (set) Token: 0x0600172A RID: 5930 RVA: 0x00028E1F File Offset: 0x0002701F
		public Object sourceObject
		{
			get
			{
				return this.m_SourceObject;
			}
			set
			{
				this.m_SourceObject = value;
			}
		}

		// Token: 0x17000460 RID: 1120
		// (get) Token: 0x0600172B RID: 5931 RVA: 0x00028E2C File Offset: 0x0002702C
		public Type outputTargetType
		{
			get
			{
				return this.m_SourceBindingType;
			}
		}

		// Token: 0x17000461 RID: 1121
		// (get) Token: 0x0600172C RID: 5932 RVA: 0x00028E48 File Offset: 0x00027048
		// (set) Token: 0x0600172D RID: 5933 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("sourceBindingType is no longer supported on PlayableBinding. Use outputBindingType instead to get the required output target type, and the appropriate binding create method (e.g. AnimationPlayableBinding.Create(name, key)) to create PlayableBindings", true)]
		public Type sourceBindingType
		{
			get
			{
				return this.m_SourceBindingType;
			}
			set
			{
			}
		}

		// Token: 0x17000462 RID: 1122
		// (get) Token: 0x0600172E RID: 5934 RVA: 0x00028E64 File Offset: 0x00027064
		// (set) Token: 0x0600172F RID: 5935 RVA: 0x00007476 File Offset: 0x00005676
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("streamType is no longer supported on PlayableBinding. Use the appropriate binding create method (e.g. AnimationPlayableBinding.Create(name, key)) instead.", true)]
		public DataStreamType streamType
		{
			get
			{
				return DataStreamType.None;
			}
			set
			{
			}
		}

		// Token: 0x06001730 RID: 5936 RVA: 0x00028E7C File Offset: 0x0002707C
		internal PlayableOutput CreateOutput(PlayableGraph graph)
		{
			PlayableOutput result;
			if (this.m_CreateOutputMethod != null)
			{
				result = this.m_CreateOutputMethod(graph, this.m_StreamName);
			}
			else
			{
				result = PlayableOutput.Null;
			}
			return result;
		}

		// Token: 0x06001731 RID: 5937 RVA: 0x00028EBC File Offset: 0x000270BC
		[VisibleToOtherModules]
		internal static PlayableBinding CreateInternal(string name, Object sourceObject, Type sourceType, PlayableBinding.CreateOutputMethod createFunction)
		{
			return new PlayableBinding
			{
				m_StreamName = name,
				m_SourceObject = sourceObject,
				m_SourceBindingType = sourceType,
				m_CreateOutputMethod = createFunction
			};
		}

		// Token: 0x06001732 RID: 5938 RVA: 0x00028EFA File Offset: 0x000270FA
		// Note: this type is marked as 'beforefieldinit'.
		static PlayableBinding()
		{
		}

		// Token: 0x04000873 RID: 2163
		private string m_StreamName;

		// Token: 0x04000874 RID: 2164
		private Object m_SourceObject;

		// Token: 0x04000875 RID: 2165
		private Type m_SourceBindingType;

		// Token: 0x04000876 RID: 2166
		private PlayableBinding.CreateOutputMethod m_CreateOutputMethod;

		// Token: 0x04000877 RID: 2167
		public static readonly PlayableBinding[] None = new PlayableBinding[0];

		// Token: 0x04000878 RID: 2168
		public static readonly double DefaultDuration = double.PositiveInfinity;

		// Token: 0x02000294 RID: 660
		// (Invoke) Token: 0x06001734 RID: 5940
		[VisibleToOtherModules]
		internal delegate PlayableOutput CreateOutputMethod(PlayableGraph graph, string name);
	}
}
