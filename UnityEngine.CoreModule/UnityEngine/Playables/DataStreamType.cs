﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x02000292 RID: 658
	[Obsolete("DataStreamType is no longer required and will be removed in a future release.", false)]
	public enum DataStreamType
	{
		// Token: 0x0400086F RID: 2159
		Animation,
		// Token: 0x04000870 RID: 2160
		Audio,
		// Token: 0x04000871 RID: 2161
		Texture,
		// Token: 0x04000872 RID: 2162
		None
	}
}
