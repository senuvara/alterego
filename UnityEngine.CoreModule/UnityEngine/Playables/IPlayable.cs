﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x02000288 RID: 648
	public interface IPlayable
	{
		// Token: 0x060016ED RID: 5869
		PlayableHandle GetHandle();
	}
}
