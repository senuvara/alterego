﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x0200028A RID: 650
	public interface IPlayableOutput
	{
		// Token: 0x060016F6 RID: 5878
		PlayableOutputHandle GetHandle();
	}
}
