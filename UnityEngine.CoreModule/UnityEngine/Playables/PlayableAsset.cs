﻿using System;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine.Playables
{
	// Token: 0x02000290 RID: 656
	[AssetFileNameExtension("playable", new string[]
	{

	})]
	[RequiredByNativeCode]
	[Serializable]
	public abstract class PlayableAsset : ScriptableObject, IPlayableAsset
	{
		// Token: 0x06001715 RID: 5909 RVA: 0x00028D1C File Offset: 0x00026F1C
		protected PlayableAsset()
		{
		}

		// Token: 0x06001716 RID: 5910
		public abstract Playable CreatePlayable(PlayableGraph graph, GameObject owner);

		// Token: 0x1700045C RID: 1116
		// (get) Token: 0x06001717 RID: 5911 RVA: 0x00028D24 File Offset: 0x00026F24
		public virtual double duration
		{
			get
			{
				return PlayableBinding.DefaultDuration;
			}
		}

		// Token: 0x1700045D RID: 1117
		// (get) Token: 0x06001718 RID: 5912 RVA: 0x00028D40 File Offset: 0x00026F40
		public virtual IEnumerable<PlayableBinding> outputs
		{
			get
			{
				return PlayableBinding.None;
			}
		}

		// Token: 0x06001719 RID: 5913 RVA: 0x00028D5C File Offset: 0x00026F5C
		[RequiredByNativeCode]
		internal unsafe static void Internal_CreatePlayable(PlayableAsset asset, PlayableGraph graph, GameObject go, IntPtr ptr)
		{
			Playable playable;
			if (asset == null)
			{
				playable = Playable.Null;
			}
			else
			{
				playable = asset.CreatePlayable(graph, go);
			}
			Playable* ptr2 = (Playable*)ptr.ToPointer();
			*ptr2 = playable;
		}

		// Token: 0x0600171A RID: 5914 RVA: 0x00028D9C File Offset: 0x00026F9C
		[RequiredByNativeCode]
		internal unsafe static void Internal_GetPlayableAssetDuration(PlayableAsset asset, IntPtr ptrToDouble)
		{
			double duration = asset.duration;
			double* ptr = (double*)ptrToDouble.ToPointer();
			*ptr = duration;
		}
	}
}
