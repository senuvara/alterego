﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Playables
{
	// Token: 0x0200028C RID: 652
	public class Notification : INotification
	{
		// Token: 0x06001708 RID: 5896 RVA: 0x00028BFF File Offset: 0x00026DFF
		public Notification(string name)
		{
			this.id = new PropertyName(name);
		}

		// Token: 0x17000458 RID: 1112
		// (get) Token: 0x06001709 RID: 5897 RVA: 0x00028C14 File Offset: 0x00026E14
		public PropertyName id
		{
			[CompilerGenerated]
			get
			{
				return this.<id>k__BackingField;
			}
		}

		// Token: 0x04000867 RID: 2151
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly PropertyName <id>k__BackingField;
	}
}
