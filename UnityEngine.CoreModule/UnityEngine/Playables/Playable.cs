﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Playables
{
	// Token: 0x0200028E RID: 654
	[RequiredByNativeCode]
	public struct Playable : IPlayable, IEquatable<Playable>
	{
		// Token: 0x0600170A RID: 5898 RVA: 0x00028C2E File Offset: 0x00026E2E
		[VisibleToOtherModules]
		internal Playable(PlayableHandle handle)
		{
			this.m_Handle = handle;
		}

		// Token: 0x17000459 RID: 1113
		// (get) Token: 0x0600170B RID: 5899 RVA: 0x00028C38 File Offset: 0x00026E38
		public static Playable Null
		{
			get
			{
				return Playable.m_NullPlayable;
			}
		}

		// Token: 0x0600170C RID: 5900 RVA: 0x00028C54 File Offset: 0x00026E54
		public static Playable Create(PlayableGraph graph, int inputCount = 0)
		{
			Playable playable = new Playable(graph.CreatePlayableHandle());
			playable.SetInputCount(inputCount);
			return playable;
		}

		// Token: 0x0600170D RID: 5901 RVA: 0x00028C80 File Offset: 0x00026E80
		public PlayableHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x0600170E RID: 5902 RVA: 0x00028C9C File Offset: 0x00026E9C
		public bool IsPlayableOfType<T>() where T : struct, IPlayable
		{
			return this.GetHandle().IsPlayableOfType<T>();
		}

		// Token: 0x0600170F RID: 5903 RVA: 0x00028CC0 File Offset: 0x00026EC0
		public Type GetPlayableType()
		{
			return this.GetHandle().GetPlayableType();
		}

		// Token: 0x06001710 RID: 5904 RVA: 0x00028CE4 File Offset: 0x00026EE4
		public bool Equals(Playable other)
		{
			return this.GetHandle() == other.GetHandle();
		}

		// Token: 0x06001711 RID: 5905 RVA: 0x00028D0B File Offset: 0x00026F0B
		// Note: this type is marked as 'beforefieldinit'.
		static Playable()
		{
		}

		// Token: 0x0400086C RID: 2156
		private PlayableHandle m_Handle;

		// Token: 0x0400086D RID: 2157
		private static readonly Playable m_NullPlayable = new Playable(PlayableHandle.Null);
	}
}
