﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x02000299 RID: 665
	public enum PlayState
	{
		// Token: 0x04000884 RID: 2180
		Paused,
		// Token: 0x04000885 RID: 2181
		Playing,
		// Token: 0x04000886 RID: 2182
		Delayed
	}
}
