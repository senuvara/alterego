﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x02000283 RID: 643
	public struct FrameData
	{
		// Token: 0x060016DD RID: 5853 RVA: 0x000288CC File Offset: 0x00026ACC
		private bool HasFlags(FrameData.Flags flag)
		{
			return (this.m_Flags & flag) == flag;
		}

		// Token: 0x1700044A RID: 1098
		// (get) Token: 0x060016DE RID: 5854 RVA: 0x000288EC File Offset: 0x00026AEC
		public ulong frameId
		{
			get
			{
				return this.m_FrameID;
			}
		}

		// Token: 0x1700044B RID: 1099
		// (get) Token: 0x060016DF RID: 5855 RVA: 0x00028908 File Offset: 0x00026B08
		public float deltaTime
		{
			get
			{
				return (float)this.m_DeltaTime;
			}
		}

		// Token: 0x1700044C RID: 1100
		// (get) Token: 0x060016E0 RID: 5856 RVA: 0x00028924 File Offset: 0x00026B24
		public float weight
		{
			get
			{
				return this.m_Weight;
			}
		}

		// Token: 0x1700044D RID: 1101
		// (get) Token: 0x060016E1 RID: 5857 RVA: 0x00028940 File Offset: 0x00026B40
		public float effectiveWeight
		{
			get
			{
				return this.m_EffectiveWeight;
			}
		}

		// Token: 0x1700044E RID: 1102
		// (get) Token: 0x060016E2 RID: 5858 RVA: 0x0002895C File Offset: 0x00026B5C
		public double effectiveParentDelay
		{
			get
			{
				return this.m_EffectiveParentDelay;
			}
		}

		// Token: 0x1700044F RID: 1103
		// (get) Token: 0x060016E3 RID: 5859 RVA: 0x00028978 File Offset: 0x00026B78
		public float effectiveParentSpeed
		{
			get
			{
				return this.m_EffectiveParentSpeed;
			}
		}

		// Token: 0x17000450 RID: 1104
		// (get) Token: 0x060016E4 RID: 5860 RVA: 0x00028994 File Offset: 0x00026B94
		public float effectiveSpeed
		{
			get
			{
				return this.m_EffectiveSpeed;
			}
		}

		// Token: 0x17000451 RID: 1105
		// (get) Token: 0x060016E5 RID: 5861 RVA: 0x000289B0 File Offset: 0x00026BB0
		public FrameData.EvaluationType evaluationType
		{
			get
			{
				return (!this.HasFlags(FrameData.Flags.Evaluate)) ? FrameData.EvaluationType.Playback : FrameData.EvaluationType.Evaluate;
			}
		}

		// Token: 0x17000452 RID: 1106
		// (get) Token: 0x060016E6 RID: 5862 RVA: 0x000289D8 File Offset: 0x00026BD8
		public bool seekOccurred
		{
			get
			{
				return this.HasFlags(FrameData.Flags.SeekOccured);
			}
		}

		// Token: 0x17000453 RID: 1107
		// (get) Token: 0x060016E7 RID: 5863 RVA: 0x000289F4 File Offset: 0x00026BF4
		public bool timeLooped
		{
			get
			{
				return this.HasFlags(FrameData.Flags.Loop);
			}
		}

		// Token: 0x17000454 RID: 1108
		// (get) Token: 0x060016E8 RID: 5864 RVA: 0x00028A10 File Offset: 0x00026C10
		public bool timeHeld
		{
			get
			{
				return this.HasFlags(FrameData.Flags.Hold);
			}
		}

		// Token: 0x17000455 RID: 1109
		// (get) Token: 0x060016E9 RID: 5865 RVA: 0x00028A2C File Offset: 0x00026C2C
		public PlayableOutput output
		{
			get
			{
				return this.m_Output;
			}
		}

		// Token: 0x17000456 RID: 1110
		// (get) Token: 0x060016EA RID: 5866 RVA: 0x00028A48 File Offset: 0x00026C48
		public PlayState effectivePlayState
		{
			get
			{
				PlayState result;
				if (this.HasFlags(FrameData.Flags.EffectivePlayStateDelayed))
				{
					result = PlayState.Delayed;
				}
				else if (this.HasFlags(FrameData.Flags.EffectivePlayStatePlaying))
				{
					result = PlayState.Playing;
				}
				else
				{
					result = PlayState.Paused;
				}
				return result;
			}
		}

		// Token: 0x04000853 RID: 2131
		internal ulong m_FrameID;

		// Token: 0x04000854 RID: 2132
		internal double m_DeltaTime;

		// Token: 0x04000855 RID: 2133
		internal float m_Weight;

		// Token: 0x04000856 RID: 2134
		internal float m_EffectiveWeight;

		// Token: 0x04000857 RID: 2135
		internal double m_EffectiveParentDelay;

		// Token: 0x04000858 RID: 2136
		internal float m_EffectiveParentSpeed;

		// Token: 0x04000859 RID: 2137
		internal float m_EffectiveSpeed;

		// Token: 0x0400085A RID: 2138
		internal FrameData.Flags m_Flags;

		// Token: 0x0400085B RID: 2139
		internal PlayableOutput m_Output;

		// Token: 0x02000284 RID: 644
		[Flags]
		internal enum Flags
		{
			// Token: 0x0400085D RID: 2141
			Evaluate = 1,
			// Token: 0x0400085E RID: 2142
			SeekOccured = 2,
			// Token: 0x0400085F RID: 2143
			Loop = 4,
			// Token: 0x04000860 RID: 2144
			Hold = 8,
			// Token: 0x04000861 RID: 2145
			EffectivePlayStateDelayed = 16,
			// Token: 0x04000862 RID: 2146
			EffectivePlayStatePlaying = 32
		}

		// Token: 0x02000285 RID: 645
		public enum EvaluationType
		{
			// Token: 0x04000864 RID: 2148
			Evaluate,
			// Token: 0x04000865 RID: 2149
			Playback
		}
	}
}
