﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x02000297 RID: 663
	public enum DirectorUpdateMode
	{
		// Token: 0x0400087D RID: 2173
		DSPClock,
		// Token: 0x0400087E RID: 2174
		GameTime,
		// Token: 0x0400087F RID: 2175
		UnscaledGameTime,
		// Token: 0x04000880 RID: 2176
		Manual
	}
}
