﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Playables
{
	// Token: 0x02000287 RID: 647
	[RequiredByNativeCode]
	public interface INotificationReceiver
	{
		// Token: 0x060016EC RID: 5868
		void OnNotify(Playable origin, INotification notification, object context);
	}
}
