﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x02000296 RID: 662
	public static class PlayableExtensions
	{
		// Token: 0x06001737 RID: 5943 RVA: 0x00028F18 File Offset: 0x00027118
		public static bool IsNull<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().IsNull();
		}

		// Token: 0x06001738 RID: 5944 RVA: 0x00028F44 File Offset: 0x00027144
		public static bool IsValid<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().IsValid();
		}

		// Token: 0x06001739 RID: 5945 RVA: 0x00028F70 File Offset: 0x00027170
		public static void Destroy<U>(this U playable) where U : struct, IPlayable
		{
			playable.GetHandle().Destroy();
		}

		// Token: 0x0600173A RID: 5946 RVA: 0x00028F94 File Offset: 0x00027194
		public static PlayableGraph GetGraph<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetGraph();
		}

		// Token: 0x0600173B RID: 5947 RVA: 0x00028FC0 File Offset: 0x000271C0
		[Obsolete("SetPlayState() has been deprecated. Use Play(), Pause() or SetDelay() instead", false)]
		public static void SetPlayState<U>(this U playable, PlayState value) where U : struct, IPlayable
		{
			if (value == PlayState.Delayed)
			{
				throw new ArgumentException("Can't set Delayed: use SetDelay() instead");
			}
			if (value != PlayState.Playing)
			{
				if (value == PlayState.Paused)
				{
					playable.GetHandle().Pause();
				}
			}
			else
			{
				playable.GetHandle().Play();
			}
		}

		// Token: 0x0600173C RID: 5948 RVA: 0x00029028 File Offset: 0x00027228
		public static PlayState GetPlayState<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetPlayState();
		}

		// Token: 0x0600173D RID: 5949 RVA: 0x00029054 File Offset: 0x00027254
		public static void Play<U>(this U playable) where U : struct, IPlayable
		{
			playable.GetHandle().Play();
		}

		// Token: 0x0600173E RID: 5950 RVA: 0x00029078 File Offset: 0x00027278
		public static void Pause<U>(this U playable) where U : struct, IPlayable
		{
			playable.GetHandle().Pause();
		}

		// Token: 0x0600173F RID: 5951 RVA: 0x0002909C File Offset: 0x0002729C
		public static void SetSpeed<U>(this U playable, double value) where U : struct, IPlayable
		{
			playable.GetHandle().SetSpeed(value);
		}

		// Token: 0x06001740 RID: 5952 RVA: 0x000290C0 File Offset: 0x000272C0
		public static double GetSpeed<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetSpeed();
		}

		// Token: 0x06001741 RID: 5953 RVA: 0x000290EC File Offset: 0x000272EC
		public static void SetDuration<U>(this U playable, double value) where U : struct, IPlayable
		{
			playable.GetHandle().SetDuration(value);
		}

		// Token: 0x06001742 RID: 5954 RVA: 0x00029110 File Offset: 0x00027310
		public static double GetDuration<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetDuration();
		}

		// Token: 0x06001743 RID: 5955 RVA: 0x0002913C File Offset: 0x0002733C
		public static void SetTime<U>(this U playable, double value) where U : struct, IPlayable
		{
			playable.GetHandle().SetTime(value);
		}

		// Token: 0x06001744 RID: 5956 RVA: 0x00029160 File Offset: 0x00027360
		public static double GetTime<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetTime();
		}

		// Token: 0x06001745 RID: 5957 RVA: 0x0002918C File Offset: 0x0002738C
		public static double GetPreviousTime<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetPreviousTime();
		}

		// Token: 0x06001746 RID: 5958 RVA: 0x000291B8 File Offset: 0x000273B8
		public static void SetDone<U>(this U playable, bool value) where U : struct, IPlayable
		{
			playable.GetHandle().SetDone(value);
		}

		// Token: 0x06001747 RID: 5959 RVA: 0x000291DC File Offset: 0x000273DC
		public static bool IsDone<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().IsDone();
		}

		// Token: 0x06001748 RID: 5960 RVA: 0x00029208 File Offset: 0x00027408
		public static void SetPropagateSetTime<U>(this U playable, bool value) where U : struct, IPlayable
		{
			playable.GetHandle().SetPropagateSetTime(value);
		}

		// Token: 0x06001749 RID: 5961 RVA: 0x0002922C File Offset: 0x0002742C
		public static bool GetPropagateSetTime<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetPropagateSetTime();
		}

		// Token: 0x0600174A RID: 5962 RVA: 0x00029258 File Offset: 0x00027458
		public static bool CanChangeInputs<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().CanChangeInputs();
		}

		// Token: 0x0600174B RID: 5963 RVA: 0x00029284 File Offset: 0x00027484
		public static bool CanSetWeights<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().CanSetWeights();
		}

		// Token: 0x0600174C RID: 5964 RVA: 0x000292B0 File Offset: 0x000274B0
		public static bool CanDestroy<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().CanDestroy();
		}

		// Token: 0x0600174D RID: 5965 RVA: 0x000292DC File Offset: 0x000274DC
		public static void SetInputCount<U>(this U playable, int value) where U : struct, IPlayable
		{
			playable.GetHandle().SetInputCount(value);
		}

		// Token: 0x0600174E RID: 5966 RVA: 0x00029300 File Offset: 0x00027500
		public static int GetInputCount<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetInputCount();
		}

		// Token: 0x0600174F RID: 5967 RVA: 0x0002932C File Offset: 0x0002752C
		public static void SetOutputCount<U>(this U playable, int value) where U : struct, IPlayable
		{
			playable.GetHandle().SetOutputCount(value);
		}

		// Token: 0x06001750 RID: 5968 RVA: 0x00029350 File Offset: 0x00027550
		public static int GetOutputCount<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetOutputCount();
		}

		// Token: 0x06001751 RID: 5969 RVA: 0x0002937C File Offset: 0x0002757C
		public static Playable GetInput<U>(this U playable, int inputPort) where U : struct, IPlayable
		{
			return playable.GetHandle().GetInput(inputPort);
		}

		// Token: 0x06001752 RID: 5970 RVA: 0x000293A8 File Offset: 0x000275A8
		public static Playable GetOutput<U>(this U playable, int outputPort) where U : struct, IPlayable
		{
			return playable.GetHandle().GetOutput(outputPort);
		}

		// Token: 0x06001753 RID: 5971 RVA: 0x000293D4 File Offset: 0x000275D4
		public static void SetInputWeight<U>(this U playable, int inputIndex, float weight) where U : struct, IPlayable
		{
			playable.GetHandle().SetInputWeight(inputIndex, weight);
		}

		// Token: 0x06001754 RID: 5972 RVA: 0x000293FC File Offset: 0x000275FC
		public static void SetInputWeight<U, V>(this U playable, V input, float weight) where U : struct, IPlayable where V : struct, IPlayable
		{
			playable.GetHandle().SetInputWeight(input.GetHandle(), weight);
		}

		// Token: 0x06001755 RID: 5973 RVA: 0x00029430 File Offset: 0x00027630
		public static float GetInputWeight<U>(this U playable, int inputIndex) where U : struct, IPlayable
		{
			return playable.GetHandle().GetInputWeight(inputIndex);
		}

		// Token: 0x06001756 RID: 5974 RVA: 0x0002945B File Offset: 0x0002765B
		public static void ConnectInput<U, V>(this U playable, int inputIndex, V sourcePlayable, int sourceOutputIndex) where U : struct, IPlayable where V : struct, IPlayable
		{
			playable.ConnectInput(inputIndex, sourcePlayable, sourceOutputIndex, 0f);
		}

		// Token: 0x06001757 RID: 5975 RVA: 0x0002946C File Offset: 0x0002766C
		public static void ConnectInput<U, V>(this U playable, int inputIndex, V sourcePlayable, int sourceOutputIndex, float weight) where U : struct, IPlayable where V : struct, IPlayable
		{
			playable.GetGraph<U>().Connect<V, U>(sourcePlayable, sourceOutputIndex, playable, inputIndex);
			playable.SetInputWeight(inputIndex, weight);
		}

		// Token: 0x06001758 RID: 5976 RVA: 0x00029498 File Offset: 0x00027698
		public static void DisconnectInput<U>(this U playable, int inputPort) where U : struct, IPlayable
		{
			playable.GetGraph<U>().Disconnect<U>(playable, inputPort);
		}

		// Token: 0x06001759 RID: 5977 RVA: 0x000294B8 File Offset: 0x000276B8
		public static int AddInput<U, V>(this U playable, V sourcePlayable, int sourceOutputIndex, float weight = 0f) where U : struct, IPlayable where V : struct, IPlayable
		{
			int inputCount = playable.GetInputCount<U>();
			playable.SetInputCount(inputCount + 1);
			playable.ConnectInput(inputCount, sourcePlayable, sourceOutputIndex, weight);
			return inputCount;
		}

		// Token: 0x0600175A RID: 5978 RVA: 0x000294E8 File Offset: 0x000276E8
		public static void SetDelay<U>(this U playable, double delay) where U : struct, IPlayable
		{
			playable.GetHandle().SetDelay(delay);
		}

		// Token: 0x0600175B RID: 5979 RVA: 0x0002950C File Offset: 0x0002770C
		public static double GetDelay<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetDelay();
		}

		// Token: 0x0600175C RID: 5980 RVA: 0x00029538 File Offset: 0x00027738
		public static bool IsDelayed<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().IsDelayed();
		}

		// Token: 0x0600175D RID: 5981 RVA: 0x00029564 File Offset: 0x00027764
		public static void SetLeadTime<U>(this U playable, float value) where U : struct, IPlayable
		{
			playable.GetHandle().SetLeadTime(value);
		}

		// Token: 0x0600175E RID: 5982 RVA: 0x00029588 File Offset: 0x00027788
		public static float GetLeadTime<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetLeadTime();
		}

		// Token: 0x0600175F RID: 5983 RVA: 0x000295B4 File Offset: 0x000277B4
		public static PlayableTraversalMode GetTraversalMode<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetTraversalMode();
		}

		// Token: 0x06001760 RID: 5984 RVA: 0x000295E0 File Offset: 0x000277E0
		public static void SetTraversalMode<U>(this U playable, PlayableTraversalMode mode) where U : struct, IPlayable
		{
			playable.GetHandle().SetTraversalMode(mode);
		}

		// Token: 0x06001761 RID: 5985 RVA: 0x00029604 File Offset: 0x00027804
		internal static DirectorWrapMode GetTimeWrapMode<U>(this U playable) where U : struct, IPlayable
		{
			return playable.GetHandle().GetTimeWrapMode();
		}

		// Token: 0x06001762 RID: 5986 RVA: 0x00029630 File Offset: 0x00027830
		internal static void SetTimeWrapMode<U>(this U playable, DirectorWrapMode value) where U : struct, IPlayable
		{
			playable.GetHandle().SetTimeWrapMode(value);
		}
	}
}
