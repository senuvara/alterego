﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x02000289 RID: 649
	public interface IPlayableBehaviour
	{
		// Token: 0x060016EE RID: 5870
		void OnGraphStart(Playable playable);

		// Token: 0x060016EF RID: 5871
		void OnGraphStop(Playable playable);

		// Token: 0x060016F0 RID: 5872
		void OnPlayableCreate(Playable playable);

		// Token: 0x060016F1 RID: 5873
		void OnPlayableDestroy(Playable playable);

		// Token: 0x060016F2 RID: 5874
		void OnBehaviourPlay(Playable playable, FrameData info);

		// Token: 0x060016F3 RID: 5875
		void OnBehaviourPause(Playable playable, FrameData info);

		// Token: 0x060016F4 RID: 5876
		void PrepareFrame(Playable playable, FrameData info);

		// Token: 0x060016F5 RID: 5877
		void ProcessFrame(Playable playable, FrameData info, object playerData);
	}
}
