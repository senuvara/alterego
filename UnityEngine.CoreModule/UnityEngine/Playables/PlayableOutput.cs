﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Playables
{
	// Token: 0x0200029B RID: 667
	[RequiredByNativeCode]
	public struct PlayableOutput : IPlayableOutput, IEquatable<PlayableOutput>
	{
		// Token: 0x06001809 RID: 6153 RVA: 0x00029DBB File Offset: 0x00027FBB
		[VisibleToOtherModules]
		internal PlayableOutput(PlayableOutputHandle handle)
		{
			this.m_Handle = handle;
		}

		// Token: 0x17000464 RID: 1124
		// (get) Token: 0x0600180A RID: 6154 RVA: 0x00029DC8 File Offset: 0x00027FC8
		public static PlayableOutput Null
		{
			get
			{
				return PlayableOutput.m_NullPlayableOutput;
			}
		}

		// Token: 0x0600180B RID: 6155 RVA: 0x00029DE4 File Offset: 0x00027FE4
		public PlayableOutputHandle GetHandle()
		{
			return this.m_Handle;
		}

		// Token: 0x0600180C RID: 6156 RVA: 0x00029E00 File Offset: 0x00028000
		public bool IsPlayableOutputOfType<T>() where T : struct, IPlayableOutput
		{
			return this.GetHandle().IsPlayableOutputOfType<T>();
		}

		// Token: 0x0600180D RID: 6157 RVA: 0x00029E24 File Offset: 0x00028024
		public Type GetPlayableOutputType()
		{
			return this.GetHandle().GetPlayableOutputType();
		}

		// Token: 0x0600180E RID: 6158 RVA: 0x00029E48 File Offset: 0x00028048
		public bool Equals(PlayableOutput other)
		{
			return this.GetHandle() == other.GetHandle();
		}

		// Token: 0x0600180F RID: 6159 RVA: 0x00029E6F File Offset: 0x0002806F
		// Note: this type is marked as 'beforefieldinit'.
		static PlayableOutput()
		{
		}

		// Token: 0x0400088A RID: 2186
		private PlayableOutputHandle m_Handle;

		// Token: 0x0400088B RID: 2187
		private static readonly PlayableOutput m_NullPlayableOutput = new PlayableOutput(PlayableOutputHandle.Null);
	}
}
