﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Playables
{
	// Token: 0x0200029F RID: 671
	public static class ScriptPlayableBinding
	{
		// Token: 0x0600185D RID: 6237 RVA: 0x0002A6E8 File Offset: 0x000288E8
		public static PlayableBinding Create(string name, Object key, Type type)
		{
			if (ScriptPlayableBinding.<>f__mg$cache0 == null)
			{
				ScriptPlayableBinding.<>f__mg$cache0 = new PlayableBinding.CreateOutputMethod(ScriptPlayableBinding.CreateScriptOutput);
			}
			return PlayableBinding.CreateInternal(name, key, type, ScriptPlayableBinding.<>f__mg$cache0);
		}

		// Token: 0x0600185E RID: 6238 RVA: 0x0002A724 File Offset: 0x00028924
		private static PlayableOutput CreateScriptOutput(PlayableGraph graph, string name)
		{
			return ScriptPlayableOutput.Create(graph, name);
		}

		// Token: 0x04000891 RID: 2193
		[CompilerGenerated]
		private static PlayableBinding.CreateOutputMethod <>f__mg$cache0;
	}
}
