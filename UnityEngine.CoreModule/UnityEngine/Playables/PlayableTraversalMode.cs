﻿using System;

namespace UnityEngine.Playables
{
	// Token: 0x02000295 RID: 661
	public enum PlayableTraversalMode
	{
		// Token: 0x0400087A RID: 2170
		Mix,
		// Token: 0x0400087B RID: 2171
		Passthrough
	}
}
