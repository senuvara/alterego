﻿using System;
using System.Runtime.Serialization;

namespace UnityEngine
{
	// Token: 0x0200021E RID: 542
	[Serializable]
	public class MissingReferenceException : SystemException
	{
		// Token: 0x06001231 RID: 4657 RVA: 0x000208EF File Offset: 0x0001EAEF
		public MissingReferenceException() : base("A Unity Runtime error occurred!")
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06001232 RID: 4658 RVA: 0x00020908 File Offset: 0x0001EB08
		public MissingReferenceException(string message) : base(message)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06001233 RID: 4659 RVA: 0x0002091D File Offset: 0x0001EB1D
		public MissingReferenceException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06001234 RID: 4660 RVA: 0x00020933 File Offset: 0x0001EB33
		protected MissingReferenceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000793 RID: 1939
		private const int Result = -2147467261;

		// Token: 0x04000794 RID: 1940
		private string unityStackTrace;
	}
}
