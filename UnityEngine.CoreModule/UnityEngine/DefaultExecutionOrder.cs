﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000031 RID: 49
	[UsedByNativeCode]
	[AttributeUsage(AttributeTargets.Class)]
	public class DefaultExecutionOrder : Attribute
	{
		// Token: 0x06000290 RID: 656 RVA: 0x00008B0F File Offset: 0x00006D0F
		public DefaultExecutionOrder(int order)
		{
			this.order = order;
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000291 RID: 657 RVA: 0x00008B20 File Offset: 0x00006D20
		// (set) Token: 0x06000292 RID: 658 RVA: 0x00008B3A File Offset: 0x00006D3A
		public int order
		{
			[CompilerGenerated]
			get
			{
				return this.<order>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<order>k__BackingField = value;
			}
		}

		// Token: 0x0400006E RID: 110
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <order>k__BackingField;
	}
}
