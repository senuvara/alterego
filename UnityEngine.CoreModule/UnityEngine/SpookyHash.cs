﻿using System;
using System.Runtime.InteropServices;
using Unity.Collections.LowLevel.Unsafe;

namespace UnityEngine
{
	// Token: 0x02000218 RID: 536
	internal static class SpookyHash
	{
		// Token: 0x06001211 RID: 4625 RVA: 0x0001F438 File Offset: 0x0001D638
		private static bool AttemptDetectAllowUnalignedRead()
		{
			string processorType = SystemInfo.processorType;
			if (processorType != null)
			{
				if (processorType == "x86" || processorType == "AMD64")
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001212 RID: 4626 RVA: 0x0001F488 File Offset: 0x0001D688
		public unsafe static void Hash(void* message, ulong length, ulong* hash1, ulong* hash2)
		{
			if (length < 192UL)
			{
				SpookyHash.Short(message, length, hash1, hash2);
			}
			else
			{
				ulong* ptr = stackalloc ulong[checked(12 * 8)];
				ulong num4;
				ulong num3;
				ulong num2;
				ulong num = num2 = (num3 = (num4 = *hash1));
				ulong num8;
				ulong num7;
				ulong num6;
				ulong num5 = num6 = (num7 = (num8 = *hash2));
				ulong num12;
				ulong num11;
				ulong num10;
				ulong num9 = num10 = (num11 = (num12 = 16045690984833335023UL));
				SpookyHash.U u = new SpookyHash.U((ushort*)message);
				ulong* ptr2 = u.p64 + length / 96UL * 12UL * 8UL / 8UL;
				if (SpookyHash.AllowUnalignedRead || (u.i & 7UL) == 0UL)
				{
					while (u.p64 < ptr2)
					{
						SpookyHash.Mix(u.p64, ref num2, ref num6, ref num10, ref num, ref num5, ref num9, ref num3, ref num7, ref num11, ref num4, ref num8, ref num12);
						u.p64 += 12;
					}
				}
				else
				{
					while (u.p64 < ptr2)
					{
						UnsafeUtility.MemCpy((void*)ptr, (void*)u.p64, 96L);
						SpookyHash.Mix(ptr, ref num2, ref num6, ref num10, ref num, ref num5, ref num9, ref num3, ref num7, ref num11, ref num4, ref num8, ref num12);
						u.p64 += 12;
					}
				}
				ulong num13 = length - (ulong)((long)(((byte*)ptr2 - (byte*)message) / 2));
				UnsafeUtility.MemCpy((void*)ptr, (void*)ptr2, (long)num13);
				SpookyHash.memset((void*)(ptr + num13 * 2UL / 8UL), 0, 96UL - num13);
				*(short*)((byte*)ptr + 190) = (short)((ushort)num13);
				SpookyHash.End(ptr, ref num2, ref num6, ref num10, ref num, ref num5, ref num9, ref num3, ref num7, ref num11, ref num4, ref num8, ref num12);
				*hash1 = num2;
				*hash2 = num6;
			}
		}

		// Token: 0x06001213 RID: 4627 RVA: 0x0001F620 File Offset: 0x0001D820
		private unsafe static void End(ulong* data, ref ulong h0, ref ulong h1, ref ulong h2, ref ulong h3, ref ulong h4, ref ulong h5, ref ulong h6, ref ulong h7, ref ulong h8, ref ulong h9, ref ulong h10, ref ulong h11)
		{
			h0 += *data;
			h1 += data[1];
			h2 += data[2];
			h3 += data[3];
			h4 += data[4];
			h5 += data[5];
			h6 += data[6];
			h7 += data[7];
			h8 += data[8];
			h9 += data[9];
			h10 += data[10];
			h11 += data[11];
			SpookyHash.EndPartial(ref h0, ref h1, ref h2, ref h3, ref h4, ref h5, ref h6, ref h7, ref h8, ref h9, ref h10, ref h11);
			SpookyHash.EndPartial(ref h0, ref h1, ref h2, ref h3, ref h4, ref h5, ref h6, ref h7, ref h8, ref h9, ref h10, ref h11);
			SpookyHash.EndPartial(ref h0, ref h1, ref h2, ref h3, ref h4, ref h5, ref h6, ref h7, ref h8, ref h9, ref h10, ref h11);
		}

		// Token: 0x06001214 RID: 4628 RVA: 0x0001F704 File Offset: 0x0001D904
		private static void EndPartial(ref ulong h0, ref ulong h1, ref ulong h2, ref ulong h3, ref ulong h4, ref ulong h5, ref ulong h6, ref ulong h7, ref ulong h8, ref ulong h9, ref ulong h10, ref ulong h11)
		{
			h11 += h1;
			h2 ^= h11;
			SpookyHash.Rot64(ref h1, 44);
			h0 += h2;
			h3 ^= h0;
			SpookyHash.Rot64(ref h2, 15);
			h1 += h3;
			h4 ^= h1;
			SpookyHash.Rot64(ref h3, 34);
			h2 += h4;
			h5 ^= h2;
			SpookyHash.Rot64(ref h4, 21);
			h3 += h5;
			h6 ^= h3;
			SpookyHash.Rot64(ref h5, 38);
			h4 += h6;
			h7 ^= h4;
			SpookyHash.Rot64(ref h6, 33);
			h5 += h7;
			h8 ^= h5;
			SpookyHash.Rot64(ref h7, 10);
			h6 += h8;
			h9 ^= h6;
			SpookyHash.Rot64(ref h8, 13);
			h7 += h9;
			h10 ^= h7;
			SpookyHash.Rot64(ref h9, 38);
			h8 += h10;
			h11 ^= h8;
			SpookyHash.Rot64(ref h10, 53);
			h9 += h11;
			h0 ^= h9;
			SpookyHash.Rot64(ref h11, 42);
			h10 += h0;
			h1 ^= h10;
			SpookyHash.Rot64(ref h0, 54);
		}

		// Token: 0x06001215 RID: 4629 RVA: 0x0001F852 File Offset: 0x0001DA52
		private static void Rot64(ref ulong x, int k)
		{
			x = (x << k | x >> 64 - k);
		}

		// Token: 0x06001216 RID: 4630 RVA: 0x0001F86C File Offset: 0x0001DA6C
		private unsafe static void Short(void* message, ulong length, ulong* hash1, ulong* hash2)
		{
			ulong* ptr = stackalloc ulong[checked(24 * 8)];
			SpookyHash.U u = new SpookyHash.U((ushort*)message);
			if (!SpookyHash.AllowUnalignedRead && (u.i & 7UL) != 0UL)
			{
				UnsafeUtility.MemCpy((void*)ptr, message, (long)length);
				u.p64 = ptr;
			}
			ulong num = length % 32UL;
			ulong num2 = *hash1;
			ulong num3 = *hash2;
			ulong num4 = 16045690984833335023UL;
			ulong num5 = 16045690984833335023UL;
			if (length > 15UL)
			{
				ulong* ptr2 = u.p64 + length / 32UL * 4UL * 8UL / 8UL;
				while (u.p64 < ptr2)
				{
					num4 += *u.p64;
					num5 += u.p64[1];
					SpookyHash.ShortMix(ref num2, ref num3, ref num4, ref num5);
					num2 += u.p64[2];
					num3 += u.p64[3];
					u.p64 += 4;
				}
				if (num >= 16UL)
				{
					num4 += *u.p64;
					num5 += u.p64[1];
					SpookyHash.ShortMix(ref num2, ref num3, ref num4, ref num5);
					u.p64 += 2;
					num -= 16UL;
				}
			}
			num5 += length << 56;
			if (num >= 0UL && num <= 15UL)
			{
				switch ((int)num)
				{
				case 0:
					num4 += 16045690984833335023UL;
					num5 += 16045690984833335023UL;
					goto IL_323;
				case 1:
					goto IL_2EF;
				case 2:
					goto IL_2D8;
				case 3:
					num4 += (ulong)u.p8[2] << 16;
					goto IL_2D8;
				case 4:
					goto IL_2AD;
				case 5:
					goto IL_295;
				case 6:
					num4 += (ulong)u.p8[5] << 40;
					goto IL_295;
				case 7:
					num4 += (ulong)u.p8[6] << 48;
					goto IL_23B;
				case 8:
					goto IL_251;
				case 9:
					goto IL_23B;
				case 10:
					goto IL_223;
				case 11:
					num5 += (ulong)u.p8[10] << 16;
					goto IL_223;
				case 12:
					goto IL_1E8;
				case 13:
					goto IL_1CF;
				case 14:
					break;
				case 15:
					num5 += (ulong)u.p8[14] << 48;
					break;
				default:
					goto IL_323;
				}
				num5 += (ulong)u.p8[13] << 40;
				IL_1CF:
				num5 += (ulong)u.p8[12] << 32;
				IL_1E8:
				num5 += (ulong)u.p32[2];
				num4 += *u.p64;
				goto IL_323;
				IL_223:
				num5 += (ulong)u.p8[9] << 8;
				IL_23B:
				num5 += (ulong)u.p8[8];
				IL_251:
				num4 += *u.p64;
				goto IL_323;
				IL_295:
				num4 += (ulong)u.p8[4] << 32;
				IL_2AD:
				num4 += (ulong)(*u.p32);
				goto IL_323;
				IL_2D8:
				num4 += (ulong)u.p8[1] << 8;
				IL_2EF:
				num4 += (ulong)(*u.p8);
			}
			IL_323:
			SpookyHash.ShortEnd(ref num2, ref num3, ref num4, ref num5);
			*hash1 = num2;
			*hash2 = num3;
		}

		// Token: 0x06001217 RID: 4631 RVA: 0x0001FBB0 File Offset: 0x0001DDB0
		private static void ShortMix(ref ulong h0, ref ulong h1, ref ulong h2, ref ulong h3)
		{
			SpookyHash.Rot64(ref h2, 50);
			h2 += h3;
			h0 ^= h2;
			SpookyHash.Rot64(ref h3, 52);
			h3 += h0;
			h1 ^= h3;
			SpookyHash.Rot64(ref h0, 30);
			h0 += h1;
			h2 ^= h0;
			SpookyHash.Rot64(ref h1, 41);
			h1 += h2;
			h3 ^= h1;
			SpookyHash.Rot64(ref h2, 54);
			h2 += h3;
			h0 ^= h2;
			SpookyHash.Rot64(ref h3, 48);
			h3 += h0;
			h1 ^= h3;
			SpookyHash.Rot64(ref h0, 38);
			h0 += h1;
			h2 ^= h0;
			SpookyHash.Rot64(ref h1, 37);
			h1 += h2;
			h3 ^= h1;
			SpookyHash.Rot64(ref h2, 62);
			h2 += h3;
			h0 ^= h2;
			SpookyHash.Rot64(ref h3, 34);
			h3 += h0;
			h1 ^= h3;
			SpookyHash.Rot64(ref h0, 5);
			h0 += h1;
			h2 ^= h0;
			SpookyHash.Rot64(ref h1, 36);
			h1 += h2;
			h3 ^= h1;
		}

		// Token: 0x06001218 RID: 4632 RVA: 0x0001FCC8 File Offset: 0x0001DEC8
		private static void ShortEnd(ref ulong h0, ref ulong h1, ref ulong h2, ref ulong h3)
		{
			h3 ^= h2;
			SpookyHash.Rot64(ref h2, 15);
			h3 += h2;
			h0 ^= h3;
			SpookyHash.Rot64(ref h3, 52);
			h0 += h3;
			h1 ^= h0;
			SpookyHash.Rot64(ref h0, 26);
			h1 += h0;
			h2 ^= h1;
			SpookyHash.Rot64(ref h1, 51);
			h2 += h1;
			h3 ^= h2;
			SpookyHash.Rot64(ref h2, 28);
			h3 += h2;
			h0 ^= h3;
			SpookyHash.Rot64(ref h3, 9);
			h0 += h3;
			h1 ^= h0;
			SpookyHash.Rot64(ref h0, 47);
			h1 += h0;
			h2 ^= h1;
			SpookyHash.Rot64(ref h1, 54);
			h2 += h1;
			h3 ^= h2;
			SpookyHash.Rot64(ref h2, 32);
			h3 += h2;
			h0 ^= h3;
			SpookyHash.Rot64(ref h3, 25);
			h0 += h3;
			h1 ^= h0;
			SpookyHash.Rot64(ref h0, 63);
			h1 += h0;
		}

		// Token: 0x06001219 RID: 4633 RVA: 0x0001FDC8 File Offset: 0x0001DFC8
		private unsafe static void Mix(ulong* data, ref ulong s0, ref ulong s1, ref ulong s2, ref ulong s3, ref ulong s4, ref ulong s5, ref ulong s6, ref ulong s7, ref ulong s8, ref ulong s9, ref ulong s10, ref ulong s11)
		{
			s0 += *data;
			s2 ^= s10;
			s11 ^= s0;
			SpookyHash.Rot64(ref s0, 11);
			s11 += s1;
			s1 += data[1];
			s3 ^= s11;
			s0 ^= s1;
			SpookyHash.Rot64(ref s1, 32);
			s0 += s2;
			s2 += data[2];
			s4 ^= s0;
			s1 ^= s2;
			SpookyHash.Rot64(ref s2, 43);
			s1 += s3;
			s3 += data[3];
			s5 ^= s1;
			s2 ^= s3;
			SpookyHash.Rot64(ref s3, 31);
			s2 += s4;
			s4 += data[4];
			s6 ^= s2;
			s3 ^= s4;
			SpookyHash.Rot64(ref s4, 17);
			s3 += s5;
			s5 += data[5];
			s7 ^= s3;
			s4 ^= s5;
			SpookyHash.Rot64(ref s5, 28);
			s4 += s6;
			s6 += data[6];
			s8 ^= s4;
			s5 ^= s6;
			SpookyHash.Rot64(ref s6, 39);
			s5 += s7;
			s7 += data[7];
			s9 ^= s5;
			s6 ^= s7;
			SpookyHash.Rot64(ref s7, 57);
			s6 += s8;
			s8 += data[8];
			s10 ^= s6;
			s7 ^= s8;
			SpookyHash.Rot64(ref s8, 55);
			s7 += s9;
			s9 += data[9];
			s11 ^= s7;
			s8 ^= s9;
			SpookyHash.Rot64(ref s9, 54);
			s8 += s10;
			s10 += data[10];
			s0 ^= s8;
			s9 ^= s10;
			SpookyHash.Rot64(ref s10, 22);
			s9 += s11;
			s11 += data[11];
			s1 ^= s9;
			s10 ^= s11;
			SpookyHash.Rot64(ref s11, 46);
			s10 += s0;
		}

		// Token: 0x0600121A RID: 4634 RVA: 0x00020014 File Offset: 0x0001E214
		private unsafe static void memset(void* dst, int value, ulong numberOfBytes)
		{
			ulong num = (ulong)(value | value << 0);
			ulong* ptr = (ulong*)dst;
			ulong num2 = numberOfBytes >> 3;
			for (ulong num3 = 0UL; num3 < num2; num3 += 1UL)
			{
				ptr[num3 * 8UL / 8UL] = num;
			}
			dst = (void*)ptr;
			numberOfBytes -= num2;
			byte* ptr2 = stackalloc byte[checked(4 * 1)];
			*ptr2 = (byte)(value & 15);
			ptr2[1] = (byte)((uint)value >> 4 & 15U);
			ptr2[2] = (byte)((uint)value >> 8 & 15U);
			ptr2[3] = (byte)((uint)value >> 12 & 15U);
			byte* ptr3 = (byte*)dst;
			ulong num4 = numberOfBytes;
			for (ulong num5 = 0UL; num5 < num4; num5 += 1UL)
			{
				ptr3[num5] = ptr2[num5 % 4UL];
			}
		}

		// Token: 0x0600121B RID: 4635 RVA: 0x000200BC File Offset: 0x0001E2BC
		// Note: this type is marked as 'beforefieldinit'.
		static SpookyHash()
		{
		}

		// Token: 0x04000783 RID: 1923
		private static readonly bool AllowUnalignedRead = SpookyHash.AttemptDetectAllowUnalignedRead();

		// Token: 0x04000784 RID: 1924
		private const int k_NumVars = 12;

		// Token: 0x04000785 RID: 1925
		private const int k_BlockSize = 96;

		// Token: 0x04000786 RID: 1926
		private const int k_BufferSize = 192;

		// Token: 0x04000787 RID: 1927
		private const ulong k_DeadBeefConst = 16045690984833335023UL;

		// Token: 0x02000219 RID: 537
		[StructLayout(LayoutKind.Explicit)]
		private struct U
		{
			// Token: 0x0600121C RID: 4636 RVA: 0x000200C8 File Offset: 0x0001E2C8
			public unsafe U(ushort* p8)
			{
				this.p32 = null;
				this.p64 = null;
				this.i = 0UL;
				this.p8 = p8;
			}

			// Token: 0x04000788 RID: 1928
			[FieldOffset(0)]
			public unsafe ushort* p8;

			// Token: 0x04000789 RID: 1929
			[FieldOffset(0)]
			public unsafe uint* p32;

			// Token: 0x0400078A RID: 1930
			[FieldOffset(0)]
			public unsafe ulong* p64;

			// Token: 0x0400078B RID: 1931
			[FieldOffset(0)]
			public ulong i;
		}
	}
}
