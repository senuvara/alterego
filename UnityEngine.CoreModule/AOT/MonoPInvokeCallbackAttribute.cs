﻿using System;

namespace AOT
{
	// Token: 0x02000026 RID: 38
	[AttributeUsage(AttributeTargets.Method)]
	public class MonoPInvokeCallbackAttribute : Attribute
	{
		// Token: 0x06000271 RID: 625 RVA: 0x000086CD File Offset: 0x000068CD
		public MonoPInvokeCallbackAttribute(Type type)
		{
		}
	}
}
