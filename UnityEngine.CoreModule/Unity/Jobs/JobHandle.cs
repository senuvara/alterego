﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Bindings;

namespace Unity.Jobs
{
	// Token: 0x02000336 RID: 822
	[NativeType(Header = "Runtime/Jobs/ScriptBindings/JobsBindings.h")]
	public struct JobHandle
	{
		// Token: 0x06001BA4 RID: 7076 RVA: 0x00030E01 File Offset: 0x0002F001
		public void Complete()
		{
			if (!(this.jobGroup == IntPtr.Zero))
			{
				JobHandle.ScheduleBatchedJobsAndComplete(ref this);
			}
		}

		// Token: 0x06001BA5 RID: 7077 RVA: 0x00030E24 File Offset: 0x0002F024
		public unsafe static void CompleteAll(ref JobHandle job0, ref JobHandle job1)
		{
			JobHandle* ptr = stackalloc JobHandle[checked(2 * sizeof(JobHandle))];
			*ptr = job0;
			ptr[1] = job1;
			JobHandle.ScheduleBatchedJobsAndCompleteAll((void*)ptr, 2);
			job0 = default(JobHandle);
			job1 = default(JobHandle);
		}

		// Token: 0x06001BA6 RID: 7078 RVA: 0x00030E74 File Offset: 0x0002F074
		public unsafe static void CompleteAll(ref JobHandle job0, ref JobHandle job1, ref JobHandle job2)
		{
			JobHandle* ptr = stackalloc JobHandle[checked(3 * sizeof(JobHandle))];
			*ptr = job0;
			ptr[1] = job1;
			ptr[sizeof(JobHandle) * 2 / sizeof(JobHandle)] = job2;
			JobHandle.ScheduleBatchedJobsAndCompleteAll((void*)ptr, 3);
			job0 = default(JobHandle);
			job1 = default(JobHandle);
			job2 = default(JobHandle);
		}

		// Token: 0x06001BA7 RID: 7079 RVA: 0x00030EDD File Offset: 0x0002F0DD
		public static void CompleteAll(NativeArray<JobHandle> jobs)
		{
			JobHandle.ScheduleBatchedJobsAndCompleteAll(jobs.GetUnsafeReadOnlyPtr<JobHandle>(), jobs.Length);
		}

		// Token: 0x17000515 RID: 1301
		// (get) Token: 0x06001BA8 RID: 7080 RVA: 0x00030EF4 File Offset: 0x0002F0F4
		public bool IsCompleted
		{
			get
			{
				return JobHandle.ScheduleBatchedJobsAndIsCompleted(ref this);
			}
		}

		// Token: 0x06001BA9 RID: 7081
		[NativeMethod(IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ScheduleBatchedJobs();

		// Token: 0x06001BAA RID: 7082
		[NativeMethod(IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ScheduleBatchedJobsAndComplete(ref JobHandle job);

		// Token: 0x06001BAB RID: 7083
		[NativeMethod(IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool ScheduleBatchedJobsAndIsCompleted(ref JobHandle job);

		// Token: 0x06001BAC RID: 7084
		[NativeMethod(IsFreeFunction = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void ScheduleBatchedJobsAndCompleteAll(void* jobs, int count);

		// Token: 0x06001BAD RID: 7085 RVA: 0x00030F10 File Offset: 0x0002F110
		public static JobHandle CombineDependencies(JobHandle job0, JobHandle job1)
		{
			return JobHandle.CombineDependenciesInternal2(ref job0, ref job1);
		}

		// Token: 0x06001BAE RID: 7086 RVA: 0x00030F30 File Offset: 0x0002F130
		public static JobHandle CombineDependencies(JobHandle job0, JobHandle job1, JobHandle job2)
		{
			return JobHandle.CombineDependenciesInternal3(ref job0, ref job1, ref job2);
		}

		// Token: 0x06001BAF RID: 7087 RVA: 0x00030F50 File Offset: 0x0002F150
		public static JobHandle CombineDependencies(NativeArray<JobHandle> jobs)
		{
			return JobHandle.CombineDependenciesInternalPtr(jobs.GetUnsafeReadOnlyPtr<JobHandle>(), jobs.Length);
		}

		// Token: 0x06001BB0 RID: 7088 RVA: 0x00030F78 File Offset: 0x0002F178
		[NativeMethod(IsFreeFunction = true)]
		private static JobHandle CombineDependenciesInternal2(ref JobHandle job0, ref JobHandle job1)
		{
			JobHandle result;
			JobHandle.CombineDependenciesInternal2_Injected(ref job0, ref job1, out result);
			return result;
		}

		// Token: 0x06001BB1 RID: 7089 RVA: 0x00030F90 File Offset: 0x0002F190
		[NativeMethod(IsFreeFunction = true)]
		private static JobHandle CombineDependenciesInternal3(ref JobHandle job0, ref JobHandle job1, ref JobHandle job2)
		{
			JobHandle result;
			JobHandle.CombineDependenciesInternal3_Injected(ref job0, ref job1, ref job2, out result);
			return result;
		}

		// Token: 0x06001BB2 RID: 7090 RVA: 0x00030FA8 File Offset: 0x0002F1A8
		[NativeMethod(IsFreeFunction = true)]
		internal unsafe static JobHandle CombineDependenciesInternalPtr(void* jobs, int count)
		{
			JobHandle result;
			JobHandle.CombineDependenciesInternalPtr_Injected(jobs, count, out result);
			return result;
		}

		// Token: 0x06001BB3 RID: 7091 RVA: 0x00030FBF File Offset: 0x0002F1BF
		[NativeMethod(IsFreeFunction = true)]
		public static bool CheckFenceIsDependencyOrDidSyncFence(JobHandle jobHandle, JobHandle dependsOn)
		{
			return JobHandle.CheckFenceIsDependencyOrDidSyncFence_Injected(ref jobHandle, ref dependsOn);
		}

		// Token: 0x06001BB4 RID: 7092
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CombineDependenciesInternal2_Injected(ref JobHandle job0, ref JobHandle job1, out JobHandle ret);

		// Token: 0x06001BB5 RID: 7093
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CombineDependenciesInternal3_Injected(ref JobHandle job0, ref JobHandle job1, ref JobHandle job2, out JobHandle ret);

		// Token: 0x06001BB6 RID: 7094
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void CombineDependenciesInternalPtr_Injected(void* jobs, int count, out JobHandle ret);

		// Token: 0x06001BB7 RID: 7095
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CheckFenceIsDependencyOrDidSyncFence_Injected(ref JobHandle jobHandle, ref JobHandle dependsOn);

		// Token: 0x04000A6A RID: 2666
		internal IntPtr jobGroup;

		// Token: 0x04000A6B RID: 2667
		internal int version;
	}
}
