﻿using System;

namespace Unity.Jobs.LowLevel.Unsafe
{
	// Token: 0x02000329 RID: 809
	public struct BatchQueryJobStruct<T> where T : struct
	{
		// Token: 0x06001B89 RID: 7049 RVA: 0x00030AA0 File Offset: 0x0002ECA0
		public static IntPtr Initialize()
		{
			if (BatchQueryJobStruct<T>.jobReflectionData == IntPtr.Zero)
			{
				BatchQueryJobStruct<T>.jobReflectionData = JobsUtility.CreateJobReflectionData(typeof(T), JobType.ParallelFor, null, null, null);
			}
			return BatchQueryJobStruct<T>.jobReflectionData;
		}

		// Token: 0x04000A63 RID: 2659
		internal static IntPtr jobReflectionData;
	}
}
