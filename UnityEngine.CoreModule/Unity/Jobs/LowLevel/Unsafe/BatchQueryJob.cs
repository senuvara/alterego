﻿using System;
using Unity.Collections;

namespace Unity.Jobs.LowLevel.Unsafe
{
	// Token: 0x02000328 RID: 808
	public struct BatchQueryJob<CommandT, ResultT> where CommandT : struct where ResultT : struct
	{
		// Token: 0x06001B88 RID: 7048 RVA: 0x00030A8F File Offset: 0x0002EC8F
		public BatchQueryJob(NativeArray<CommandT> commands, NativeArray<ResultT> results)
		{
			this.commands = commands;
			this.results = results;
		}

		// Token: 0x04000A61 RID: 2657
		[ReadOnly]
		internal NativeArray<CommandT> commands;

		// Token: 0x04000A62 RID: 2658
		internal NativeArray<ResultT> results;
	}
}
