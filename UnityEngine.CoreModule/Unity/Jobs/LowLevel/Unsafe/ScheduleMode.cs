﻿using System;

namespace Unity.Jobs.LowLevel.Unsafe
{
	// Token: 0x0200033A RID: 826
	public enum ScheduleMode
	{
		// Token: 0x04000A75 RID: 2677
		Run,
		// Token: 0x04000A76 RID: 2678
		Batched
	}
}
