﻿using System;

namespace Unity.Jobs.LowLevel.Unsafe
{
	// Token: 0x0200033B RID: 827
	public enum JobType
	{
		// Token: 0x04000A78 RID: 2680
		Single,
		// Token: 0x04000A79 RID: 2681
		ParallelFor
	}
}
