﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Unity.Jobs.LowLevel.Unsafe
{
	// Token: 0x02000338 RID: 824
	[AttributeUsage(AttributeTargets.Interface)]
	public sealed class JobProducerTypeAttribute : Attribute
	{
		// Token: 0x06001BB9 RID: 7097 RVA: 0x00030FE8 File Offset: 0x0002F1E8
		public JobProducerTypeAttribute(Type producerType)
		{
			this.ProducerType = producerType;
		}

		// Token: 0x17000516 RID: 1302
		// (get) Token: 0x06001BBA RID: 7098 RVA: 0x00030FF8 File Offset: 0x0002F1F8
		public Type ProducerType
		{
			[CompilerGenerated]
			get
			{
				return this.<ProducerType>k__BackingField;
			}
		}

		// Token: 0x04000A6C RID: 2668
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly Type <ProducerType>k__BackingField;
	}
}
