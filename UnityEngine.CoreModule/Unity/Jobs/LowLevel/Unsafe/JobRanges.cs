﻿using System;

namespace Unity.Jobs.LowLevel.Unsafe
{
	// Token: 0x02000339 RID: 825
	public struct JobRanges
	{
		// Token: 0x04000A6D RID: 2669
		public int BatchSize;

		// Token: 0x04000A6E RID: 2670
		public int NumJobs;

		// Token: 0x04000A6F RID: 2671
		public int TotalIterationCount;

		// Token: 0x04000A70 RID: 2672
		public int NumPhases;

		// Token: 0x04000A71 RID: 2673
		public int IndicesPerPhase;

		// Token: 0x04000A72 RID: 2674
		public IntPtr StartEndIndex;

		// Token: 0x04000A73 RID: 2675
		public IntPtr PhaseData;
	}
}
