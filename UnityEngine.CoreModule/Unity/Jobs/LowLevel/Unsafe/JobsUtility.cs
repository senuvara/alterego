﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace Unity.Jobs.LowLevel.Unsafe
{
	// Token: 0x0200033C RID: 828
	[NativeType(Header = "Runtime/Jobs/ScriptBindings/JobsBindings.h")]
	public static class JobsUtility
	{
		// Token: 0x06001BBB RID: 7099 RVA: 0x00031014 File Offset: 0x0002F214
		public unsafe static void GetJobRange(ref JobRanges ranges, int jobIndex, out int beginIndex, out int endIndex)
		{
			int* ptr = (int*)((void*)ranges.StartEndIndex);
			beginIndex = ptr[jobIndex * 2];
			endIndex = ptr[jobIndex * 2 + 1];
		}

		// Token: 0x06001BBC RID: 7100
		[NativeMethod(IsFreeFunction = true, IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetWorkStealingRange(ref JobRanges ranges, int jobIndex, out int beginIndex, out int endIndex);

		// Token: 0x06001BBD RID: 7101 RVA: 0x00031048 File Offset: 0x0002F248
		[FreeFunction("ScheduleManagedJob")]
		public static JobHandle Schedule(ref JobsUtility.JobScheduleParameters parameters)
		{
			JobHandle result;
			JobsUtility.Schedule_Injected(ref parameters, out result);
			return result;
		}

		// Token: 0x06001BBE RID: 7102 RVA: 0x00031060 File Offset: 0x0002F260
		[FreeFunction("ScheduleManagedJobParallelFor")]
		public static JobHandle ScheduleParallelFor(ref JobsUtility.JobScheduleParameters parameters, int arrayLength, int innerloopBatchCount)
		{
			JobHandle result;
			JobsUtility.ScheduleParallelFor_Injected(ref parameters, arrayLength, innerloopBatchCount, out result);
			return result;
		}

		// Token: 0x06001BBF RID: 7103 RVA: 0x00031078 File Offset: 0x0002F278
		[FreeFunction("ScheduleManagedJobParallelForDeferArraySize")]
		public unsafe static JobHandle ScheduleParallelForDeferArraySize(ref JobsUtility.JobScheduleParameters parameters, int innerloopBatchCount, void* listData, void* listDataAtomicSafetyHandle)
		{
			JobHandle result;
			JobsUtility.ScheduleParallelForDeferArraySize_Injected(ref parameters, innerloopBatchCount, listData, listDataAtomicSafetyHandle, out result);
			return result;
		}

		// Token: 0x06001BC0 RID: 7104 RVA: 0x00031094 File Offset: 0x0002F294
		[FreeFunction("ScheduleManagedJobParallelForTransform")]
		public static JobHandle ScheduleParallelForTransform(ref JobsUtility.JobScheduleParameters parameters, IntPtr transfromAccesssArray)
		{
			JobHandle result;
			JobsUtility.ScheduleParallelForTransform_Injected(ref parameters, transfromAccesssArray, out result);
			return result;
		}

		// Token: 0x06001BC1 RID: 7105
		[NativeMethod(IsThreadSafe = true, IsFreeFunction = true)]
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern void PatchBufferMinMaxRanges(IntPtr bufferRangePatchData, void* jobdata, int startIndex, int rangeSize);

		// Token: 0x06001BC2 RID: 7106
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr CreateJobReflectionData(Type wrapperJobType, Type userJobType, JobType jobType, object managedJobFunction0, object managedJobFunction1, object managedJobFunction2);

		// Token: 0x06001BC3 RID: 7107 RVA: 0x000310AC File Offset: 0x0002F2AC
		public static IntPtr CreateJobReflectionData(Type type, JobType jobType, object managedJobFunction0, object managedJobFunction1 = null, object managedJobFunction2 = null)
		{
			return JobsUtility.CreateJobReflectionData(type, type, jobType, managedJobFunction0, managedJobFunction1, managedJobFunction2);
		}

		// Token: 0x06001BC4 RID: 7108 RVA: 0x000310D0 File Offset: 0x0002F2D0
		public static IntPtr CreateJobReflectionData(Type wrapperJobType, Type userJobType, JobType jobType, object managedJobFunction0)
		{
			return JobsUtility.CreateJobReflectionData(wrapperJobType, userJobType, jobType, managedJobFunction0, null, null);
		}

		// Token: 0x17000517 RID: 1303
		// (get) Token: 0x06001BC5 RID: 7109
		// (set) Token: 0x06001BC6 RID: 7110
		public static extern bool JobDebuggerEnabled { [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000518 RID: 1304
		// (get) Token: 0x06001BC7 RID: 7111
		// (set) Token: 0x06001BC8 RID: 7112
		public static extern bool JobCompilerEnabled { [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001BC9 RID: 7113
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Schedule_Injected(ref JobsUtility.JobScheduleParameters parameters, out JobHandle ret);

		// Token: 0x06001BCA RID: 7114
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ScheduleParallelFor_Injected(ref JobsUtility.JobScheduleParameters parameters, int arrayLength, int innerloopBatchCount, out JobHandle ret);

		// Token: 0x06001BCB RID: 7115
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void ScheduleParallelForDeferArraySize_Injected(ref JobsUtility.JobScheduleParameters parameters, int innerloopBatchCount, void* listData, void* listDataAtomicSafetyHandle, out JobHandle ret);

		// Token: 0x06001BCC RID: 7116
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ScheduleParallelForTransform_Injected(ref JobsUtility.JobScheduleParameters parameters, IntPtr transfromAccesssArray, out JobHandle ret);

		// Token: 0x04000A7A RID: 2682
		public const int MaxJobThreadCount = 128;

		// Token: 0x04000A7B RID: 2683
		public const int CacheLineSize = 64;

		// Token: 0x0200033D RID: 829
		public struct JobScheduleParameters
		{
			// Token: 0x06001BCD RID: 7117 RVA: 0x000310F0 File Offset: 0x0002F2F0
			public unsafe JobScheduleParameters(void* i_jobData, IntPtr i_reflectionData, JobHandle i_dependency, ScheduleMode i_scheduleMode)
			{
				this.Dependency = i_dependency;
				this.JobDataPtr = (IntPtr)i_jobData;
				this.ReflectionData = i_reflectionData;
				this.ScheduleMode = (int)i_scheduleMode;
			}

			// Token: 0x04000A7C RID: 2684
			public JobHandle Dependency;

			// Token: 0x04000A7D RID: 2685
			public int ScheduleMode;

			// Token: 0x04000A7E RID: 2686
			public IntPtr ReflectionData;

			// Token: 0x04000A7F RID: 2687
			public IntPtr JobDataPtr;
		}
	}
}
