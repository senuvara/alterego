﻿using System;

namespace Unity.Jobs.LowLevel.Unsafe
{
	// Token: 0x02000337 RID: 823
	public static class JobHandleUnsafeUtility
	{
		// Token: 0x06001BB8 RID: 7096 RVA: 0x00030FCC File Offset: 0x0002F1CC
		public unsafe static JobHandle CombineDependencies(JobHandle* jobs, int count)
		{
			return JobHandle.CombineDependenciesInternalPtr((void*)jobs, count);
		}
	}
}
