﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs.LowLevel.Unsafe;

namespace Unity.Jobs
{
	// Token: 0x0200032F RID: 815
	public static class IJobParallelForExtensions
	{
		// Token: 0x06001B94 RID: 7060 RVA: 0x00030BC4 File Offset: 0x0002EDC4
		public static JobHandle Schedule<T>(this T jobData, int arrayLength, int innerloopBatchCount, JobHandle dependsOn = default(JobHandle)) where T : struct, IJobParallelFor
		{
			JobsUtility.JobScheduleParameters jobScheduleParameters = new JobsUtility.JobScheduleParameters(UnsafeUtility.AddressOf<T>(ref jobData), IJobParallelForExtensions.ParallelForJobStruct<T>.Initialize(), dependsOn, ScheduleMode.Batched);
			return JobsUtility.ScheduleParallelFor(ref jobScheduleParameters, arrayLength, innerloopBatchCount);
		}

		// Token: 0x06001B95 RID: 7061 RVA: 0x00030BF8 File Offset: 0x0002EDF8
		public static void Run<T>(this T jobData, int arrayLength) where T : struct, IJobParallelFor
		{
			JobsUtility.JobScheduleParameters jobScheduleParameters = new JobsUtility.JobScheduleParameters(UnsafeUtility.AddressOf<T>(ref jobData), IJobParallelForExtensions.ParallelForJobStruct<T>.Initialize(), default(JobHandle), ScheduleMode.Run);
			JobsUtility.ScheduleParallelFor(ref jobScheduleParameters, arrayLength, arrayLength);
		}

		// Token: 0x02000330 RID: 816
		internal struct ParallelForJobStruct<T> where T : struct, IJobParallelFor
		{
			// Token: 0x06001B96 RID: 7062 RVA: 0x00030C30 File Offset: 0x0002EE30
			public static IntPtr Initialize()
			{
				if (IJobParallelForExtensions.ParallelForJobStruct<T>.jobReflectionData == IntPtr.Zero)
				{
					Type typeFromHandle = typeof(T);
					JobType jobType = JobType.ParallelFor;
					if (IJobParallelForExtensions.ParallelForJobStruct<T>.<>f__mg$cache0 == null)
					{
						IJobParallelForExtensions.ParallelForJobStruct<T>.<>f__mg$cache0 = new IJobParallelForExtensions.ParallelForJobStruct<T>.ExecuteJobFunction(IJobParallelForExtensions.ParallelForJobStruct<T>.Execute);
					}
					IJobParallelForExtensions.ParallelForJobStruct<T>.jobReflectionData = JobsUtility.CreateJobReflectionData(typeFromHandle, jobType, IJobParallelForExtensions.ParallelForJobStruct<T>.<>f__mg$cache0, null, null);
				}
				return IJobParallelForExtensions.ParallelForJobStruct<T>.jobReflectionData;
			}

			// Token: 0x06001B97 RID: 7063 RVA: 0x00030C94 File Offset: 0x0002EE94
			public static void Execute(ref T jobData, IntPtr additionalPtr, IntPtr bufferRangePatchData, ref JobRanges ranges, int jobIndex)
			{
				int num;
				int num2;
				while (JobsUtility.GetWorkStealingRange(ref ranges, jobIndex, out num, out num2))
				{
					for (int i = num; i < num2; i++)
					{
						jobData.Execute(i);
					}
				}
			}

			// Token: 0x04000A66 RID: 2662
			public static IntPtr jobReflectionData;

			// Token: 0x04000A67 RID: 2663
			[CompilerGenerated]
			private static IJobParallelForExtensions.ParallelForJobStruct<T>.ExecuteJobFunction <>f__mg$cache0;

			// Token: 0x02000331 RID: 817
			// (Invoke) Token: 0x06001B99 RID: 7065
			public delegate void ExecuteJobFunction(ref T data, IntPtr additionalPtr, IntPtr bufferRangePatchData, ref JobRanges ranges, int jobIndex);
		}
	}
}
