﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs.LowLevel.Unsafe;

namespace Unity.Jobs
{
	// Token: 0x0200032B RID: 811
	public static class IJobExtensions
	{
		// Token: 0x06001B8B RID: 7051 RVA: 0x00030AE8 File Offset: 0x0002ECE8
		public static JobHandle Schedule<T>(this T jobData, JobHandle dependsOn = default(JobHandle)) where T : struct, IJob
		{
			JobsUtility.JobScheduleParameters jobScheduleParameters = new JobsUtility.JobScheduleParameters(UnsafeUtility.AddressOf<T>(ref jobData), IJobExtensions.JobStruct<T>.Initialize(), dependsOn, ScheduleMode.Batched);
			return JobsUtility.Schedule(ref jobScheduleParameters);
		}

		// Token: 0x06001B8C RID: 7052 RVA: 0x00030B1C File Offset: 0x0002ED1C
		public static void Run<T>(this T jobData) where T : struct, IJob
		{
			JobsUtility.JobScheduleParameters jobScheduleParameters = new JobsUtility.JobScheduleParameters(UnsafeUtility.AddressOf<T>(ref jobData), IJobExtensions.JobStruct<T>.Initialize(), default(JobHandle), ScheduleMode.Run);
			JobsUtility.Schedule(ref jobScheduleParameters);
		}

		// Token: 0x0200032C RID: 812
		internal struct JobStruct<T> where T : struct, IJob
		{
			// Token: 0x06001B8D RID: 7053 RVA: 0x00030B50 File Offset: 0x0002ED50
			public static IntPtr Initialize()
			{
				if (IJobExtensions.JobStruct<T>.jobReflectionData == IntPtr.Zero)
				{
					Type typeFromHandle = typeof(T);
					JobType jobType = JobType.Single;
					if (IJobExtensions.JobStruct<T>.<>f__mg$cache0 == null)
					{
						IJobExtensions.JobStruct<T>.<>f__mg$cache0 = new IJobExtensions.JobStruct<T>.ExecuteJobFunction(IJobExtensions.JobStruct<T>.Execute);
					}
					IJobExtensions.JobStruct<T>.jobReflectionData = JobsUtility.CreateJobReflectionData(typeFromHandle, jobType, IJobExtensions.JobStruct<T>.<>f__mg$cache0, null, null);
				}
				return IJobExtensions.JobStruct<T>.jobReflectionData;
			}

			// Token: 0x06001B8E RID: 7054 RVA: 0x00030BB2 File Offset: 0x0002EDB2
			public static void Execute(ref T data, IntPtr additionalPtr, IntPtr bufferRangePatchData, ref JobRanges ranges, int jobIndex)
			{
				data.Execute();
			}

			// Token: 0x04000A64 RID: 2660
			public static IntPtr jobReflectionData;

			// Token: 0x04000A65 RID: 2661
			[CompilerGenerated]
			private static IJobExtensions.JobStruct<T>.ExecuteJobFunction <>f__mg$cache0;

			// Token: 0x0200032D RID: 813
			// (Invoke) Token: 0x06001B90 RID: 7056
			public delegate void ExecuteJobFunction(ref T data, IntPtr additionalPtr, IntPtr bufferRangePatchData, ref JobRanges ranges, int jobIndex);
		}
	}
}
