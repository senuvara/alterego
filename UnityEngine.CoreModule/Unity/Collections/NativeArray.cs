﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Internal;

namespace Unity.Collections
{
	// Token: 0x020002BE RID: 702
	[DebuggerDisplay("Length = {Length}")]
	[NativeContainerSupportsDeferredConvertListToArray]
	[NativeContainerSupportsDeallocateOnJobCompletion]
	[NativeContainerSupportsMinMaxWriteRestriction]
	[DebuggerTypeProxy(typeof(NativeArrayDebugView<>))]
	[NativeContainer]
	public struct NativeArray<T> : IDisposable, IEnumerable<T>, IEquatable<NativeArray<T>>, IEnumerable where T : struct
	{
		// Token: 0x060018EE RID: 6382 RVA: 0x0002BAAB File Offset: 0x00029CAB
		public NativeArray(int length, Allocator allocator, NativeArrayOptions options = NativeArrayOptions.ClearMemory)
		{
			NativeArray<T>.Allocate(length, allocator, out this);
			if ((options & NativeArrayOptions.ClearMemory) == NativeArrayOptions.ClearMemory)
			{
				UnsafeUtility.MemClear(this.m_Buffer, (long)this.Length * (long)UnsafeUtility.SizeOf<T>());
			}
		}

		// Token: 0x060018EF RID: 6383 RVA: 0x0002BAD8 File Offset: 0x00029CD8
		public NativeArray(T[] array, Allocator allocator)
		{
			NativeArray<T>.Allocate(array.Length, allocator, out this);
			NativeArray<T>.Copy(array, this);
		}

		// Token: 0x060018F0 RID: 6384 RVA: 0x0002BAF1 File Offset: 0x00029CF1
		public NativeArray(NativeArray<T> array, Allocator allocator)
		{
			NativeArray<T>.Allocate(array.Length, allocator, out this);
			NativeArray<T>.Copy(array, this);
		}

		// Token: 0x060018F1 RID: 6385 RVA: 0x0002BB10 File Offset: 0x00029D10
		private static void Allocate(int length, Allocator allocator, out NativeArray<T> array)
		{
			long size = (long)UnsafeUtility.SizeOf<T>() * (long)length;
			array.m_Buffer = UnsafeUtility.Malloc(size, UnsafeUtility.AlignOf<T>(), allocator);
			array.m_Length = length;
			array.m_AllocatorLabel = allocator;
		}

		// Token: 0x1700047E RID: 1150
		// (get) Token: 0x060018F2 RID: 6386 RVA: 0x0002BB48 File Offset: 0x00029D48
		public int Length
		{
			[CompilerGenerated]
			get
			{
				return this.m_Length;
			}
		}

		// Token: 0x060018F3 RID: 6387 RVA: 0x0002BB62 File Offset: 0x00029D62
		[BurstDiscard]
		internal static void IsBlittableAndThrow()
		{
			if (!UnsafeUtility.IsBlittable<T>())
			{
				throw new InvalidOperationException(string.Format("{0} used in NativeArray<{1}> must be blittable.\n{2}", typeof(T), typeof(T), UnsafeUtility.GetReasonForValueTypeNonBlittable<T>()));
			}
		}

		// Token: 0x060018F4 RID: 6388 RVA: 0x00007476 File Offset: 0x00005676
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		private void CheckElementReadAccess(int index)
		{
		}

		// Token: 0x060018F5 RID: 6389 RVA: 0x00007476 File Offset: 0x00005676
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		private void CheckElementWriteAccess(int index)
		{
		}

		// Token: 0x1700047F RID: 1151
		public T this[int index]
		{
			get
			{
				return UnsafeUtility.ReadArrayElement<T>(this.m_Buffer, index);
			}
			[WriteAccessRequired]
			set
			{
				UnsafeUtility.WriteArrayElement<T>(this.m_Buffer, index, value);
			}
		}

		// Token: 0x17000480 RID: 1152
		// (get) Token: 0x060018F8 RID: 6392 RVA: 0x0002BBD0 File Offset: 0x00029DD0
		public bool IsCreated
		{
			[CompilerGenerated]
			get
			{
				return this.m_Buffer != null;
			}
		}

		// Token: 0x060018F9 RID: 6393 RVA: 0x0002BBF1 File Offset: 0x00029DF1
		[WriteAccessRequired]
		public void Dispose()
		{
			UnsafeUtility.Free(this.m_Buffer, this.m_AllocatorLabel);
			this.m_Buffer = null;
			this.m_Length = 0;
		}

		// Token: 0x060018FA RID: 6394 RVA: 0x0002BC14 File Offset: 0x00029E14
		[WriteAccessRequired]
		public void CopyFrom(T[] array)
		{
			NativeArray<T>.Copy(array, this);
		}

		// Token: 0x060018FB RID: 6395 RVA: 0x0002BC23 File Offset: 0x00029E23
		[WriteAccessRequired]
		public void CopyFrom(NativeArray<T> array)
		{
			NativeArray<T>.Copy(array, this);
		}

		// Token: 0x060018FC RID: 6396 RVA: 0x0002BC32 File Offset: 0x00029E32
		public void CopyTo(T[] array)
		{
			NativeArray<T>.Copy(this, array);
		}

		// Token: 0x060018FD RID: 6397 RVA: 0x0002BC41 File Offset: 0x00029E41
		public void CopyTo(NativeArray<T> array)
		{
			NativeArray<T>.Copy(this, array);
		}

		// Token: 0x060018FE RID: 6398 RVA: 0x0002BC50 File Offset: 0x00029E50
		public T[] ToArray()
		{
			T[] array = new T[this.Length];
			NativeArray<T>.Copy(this, array, this.Length);
			return array;
		}

		// Token: 0x060018FF RID: 6399 RVA: 0x0002BC84 File Offset: 0x00029E84
		public NativeArray<T>.Enumerator GetEnumerator()
		{
			return new NativeArray<T>.Enumerator(ref this);
		}

		// Token: 0x06001900 RID: 6400 RVA: 0x0002BCA0 File Offset: 0x00029EA0
		IEnumerator<T> IEnumerable<!0>.GetEnumerator()
		{
			return new NativeArray<T>.Enumerator(ref this);
		}

		// Token: 0x06001901 RID: 6401 RVA: 0x0002BCC0 File Offset: 0x00029EC0
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06001902 RID: 6402 RVA: 0x0002BCE0 File Offset: 0x00029EE0
		public bool Equals(NativeArray<T> other)
		{
			return this.m_Buffer == other.m_Buffer && this.m_Length == other.m_Length;
		}

		// Token: 0x06001903 RID: 6403 RVA: 0x0002BD1C File Offset: 0x00029F1C
		public override bool Equals(object obj)
		{
			return !object.ReferenceEquals(null, obj) && obj is NativeArray<T> && this.Equals((NativeArray<T>)obj);
		}

		// Token: 0x06001904 RID: 6404 RVA: 0x0002BD60 File Offset: 0x00029F60
		public override int GetHashCode()
		{
			return this.m_Buffer * 397 ^ this.m_Length;
		}

		// Token: 0x06001905 RID: 6405 RVA: 0x0002BD8C File Offset: 0x00029F8C
		public static bool operator ==(NativeArray<T> left, NativeArray<T> right)
		{
			return left.Equals(right);
		}

		// Token: 0x06001906 RID: 6406 RVA: 0x0002BDAC File Offset: 0x00029FAC
		public static bool operator !=(NativeArray<T> left, NativeArray<T> right)
		{
			return !left.Equals(right);
		}

		// Token: 0x06001907 RID: 6407 RVA: 0x0002BDCC File Offset: 0x00029FCC
		public static void Copy(NativeArray<T> src, NativeArray<T> dst)
		{
			NativeArray<T>.Copy(src, 0, dst, 0, src.Length);
		}

		// Token: 0x06001908 RID: 6408 RVA: 0x0002BDDF File Offset: 0x00029FDF
		public static void Copy(T[] src, NativeArray<T> dst)
		{
			NativeArray<T>.Copy(src, 0, dst, 0, src.Length);
		}

		// Token: 0x06001909 RID: 6409 RVA: 0x0002BDEE File Offset: 0x00029FEE
		public static void Copy(NativeArray<T> src, T[] dst)
		{
			NativeArray<T>.Copy(src, 0, dst, 0, src.Length);
		}

		// Token: 0x0600190A RID: 6410 RVA: 0x0002BE01 File Offset: 0x0002A001
		public static void Copy(NativeArray<T> src, NativeArray<T> dst, int length)
		{
			NativeArray<T>.Copy(src, 0, dst, 0, length);
		}

		// Token: 0x0600190B RID: 6411 RVA: 0x0002BE0E File Offset: 0x0002A00E
		public static void Copy(T[] src, NativeArray<T> dst, int length)
		{
			NativeArray<T>.Copy(src, 0, dst, 0, length);
		}

		// Token: 0x0600190C RID: 6412 RVA: 0x0002BE1B File Offset: 0x0002A01B
		public static void Copy(NativeArray<T> src, T[] dst, int length)
		{
			NativeArray<T>.Copy(src, 0, dst, 0, length);
		}

		// Token: 0x0600190D RID: 6413 RVA: 0x0002BE28 File Offset: 0x0002A028
		public unsafe static void Copy(NativeArray<T> src, int srcIndex, NativeArray<T> dst, int dstIndex, int length)
		{
			UnsafeUtility.MemCpy((void*)((byte*)dst.m_Buffer + dstIndex * UnsafeUtility.SizeOf<T>()), (void*)((byte*)src.m_Buffer + srcIndex * UnsafeUtility.SizeOf<T>()), (long)(length * UnsafeUtility.SizeOf<T>()));
		}

		// Token: 0x0600190E RID: 6414 RVA: 0x0002BE5C File Offset: 0x0002A05C
		public unsafe static void Copy(T[] src, int srcIndex, NativeArray<T> dst, int dstIndex, int length)
		{
			GCHandle gchandle = GCHandle.Alloc(src, GCHandleType.Pinned);
			IntPtr value = gchandle.AddrOfPinnedObject();
			UnsafeUtility.MemCpy((void*)((byte*)dst.m_Buffer + dstIndex * UnsafeUtility.SizeOf<T>()), (void*)((byte*)((void*)value) + srcIndex * UnsafeUtility.SizeOf<T>()), (long)(length * UnsafeUtility.SizeOf<T>()));
			gchandle.Free();
		}

		// Token: 0x0600190F RID: 6415 RVA: 0x0002BEB0 File Offset: 0x0002A0B0
		public unsafe static void Copy(NativeArray<T> src, int srcIndex, T[] dst, int dstIndex, int length)
		{
			GCHandle gchandle = GCHandle.Alloc(dst, GCHandleType.Pinned);
			IntPtr value = gchandle.AddrOfPinnedObject();
			UnsafeUtility.MemCpy((void*)((byte*)((void*)value) + dstIndex * UnsafeUtility.SizeOf<T>()), (void*)((byte*)src.m_Buffer + srcIndex * UnsafeUtility.SizeOf<T>()), (long)(length * UnsafeUtility.SizeOf<T>()));
			gchandle.Free();
		}

		// Token: 0x04000907 RID: 2311
		[NativeDisableUnsafePtrRestriction]
		internal unsafe void* m_Buffer;

		// Token: 0x04000908 RID: 2312
		internal int m_Length;

		// Token: 0x04000909 RID: 2313
		internal Allocator m_AllocatorLabel;

		// Token: 0x020002BF RID: 703
		[ExcludeFromDocs]
		public struct Enumerator : IEnumerator<T>, IEnumerator, IDisposable
		{
			// Token: 0x06001910 RID: 6416 RVA: 0x0002BF02 File Offset: 0x0002A102
			public Enumerator(ref NativeArray<T> array)
			{
				this.m_Array = array;
				this.m_Index = -1;
			}

			// Token: 0x06001911 RID: 6417 RVA: 0x00007476 File Offset: 0x00005676
			public void Dispose()
			{
			}

			// Token: 0x06001912 RID: 6418 RVA: 0x0002BF18 File Offset: 0x0002A118
			public bool MoveNext()
			{
				this.m_Index++;
				return this.m_Index < this.m_Array.Length;
			}

			// Token: 0x06001913 RID: 6419 RVA: 0x0002BF4E File Offset: 0x0002A14E
			public void Reset()
			{
				this.m_Index = -1;
			}

			// Token: 0x17000482 RID: 1154
			// (get) Token: 0x06001914 RID: 6420 RVA: 0x0002BF58 File Offset: 0x0002A158
			public T Current
			{
				[CompilerGenerated]
				get
				{
					return this.m_Array[this.m_Index];
				}
			}

			// Token: 0x17000481 RID: 1153
			// (get) Token: 0x06001915 RID: 6421 RVA: 0x0002BF80 File Offset: 0x0002A180
			object IEnumerator.Current
			{
				[CompilerGenerated]
				get
				{
					return this.Current;
				}
			}

			// Token: 0x0400090A RID: 2314
			private NativeArray<T> m_Array;

			// Token: 0x0400090B RID: 2315
			private int m_Index;
		}
	}
}
