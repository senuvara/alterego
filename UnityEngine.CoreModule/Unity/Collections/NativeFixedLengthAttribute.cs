﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections
{
	// Token: 0x02000270 RID: 624
	[AttributeUsage(AttributeTargets.Field)]
	[RequiredByNativeCode]
	public sealed class NativeFixedLengthAttribute : Attribute
	{
		// Token: 0x060016BD RID: 5821 RVA: 0x00028774 File Offset: 0x00026974
		public NativeFixedLengthAttribute(int fixedLength)
		{
			this.FixedLength = fixedLength;
		}

		// Token: 0x04000846 RID: 2118
		public int FixedLength;
	}
}
