﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections
{
	// Token: 0x02000271 RID: 625
	[AttributeUsage(AttributeTargets.Field)]
	[RequiredByNativeCode]
	public sealed class NativeMatchesParallelForLengthAttribute : Attribute
	{
		// Token: 0x060016BE RID: 5822 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeMatchesParallelForLengthAttribute()
		{
		}
	}
}
