﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections
{
	// Token: 0x02000272 RID: 626
	[AttributeUsage(AttributeTargets.Field)]
	[RequiredByNativeCode]
	public sealed class NativeDisableParallelForRestrictionAttribute : Attribute
	{
		// Token: 0x060016BF RID: 5823 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeDisableParallelForRestrictionAttribute()
		{
		}
	}
}
