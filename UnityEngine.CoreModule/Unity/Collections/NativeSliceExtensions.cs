﻿using System;

namespace Unity.Collections
{
	// Token: 0x020002C2 RID: 706
	public static class NativeSliceExtensions
	{
		// Token: 0x0600191C RID: 6428 RVA: 0x0002C05C File Offset: 0x0002A25C
		public static NativeSlice<T> Slice<T>(this NativeArray<T> thisArray) where T : struct
		{
			return new NativeSlice<T>(thisArray);
		}

		// Token: 0x0600191D RID: 6429 RVA: 0x0002C078 File Offset: 0x0002A278
		public static NativeSlice<T> Slice<T>(this NativeArray<T> thisArray, int start) where T : struct
		{
			return new NativeSlice<T>(thisArray, start);
		}

		// Token: 0x0600191E RID: 6430 RVA: 0x0002C094 File Offset: 0x0002A294
		public static NativeSlice<T> Slice<T>(this NativeArray<T> thisArray, int start, int length) where T : struct
		{
			return new NativeSlice<T>(thisArray, start, length);
		}

		// Token: 0x0600191F RID: 6431 RVA: 0x0002C0B4 File Offset: 0x0002A2B4
		public static NativeSlice<T> Slice<T>(this NativeSlice<T> thisSlice) where T : struct
		{
			return thisSlice;
		}

		// Token: 0x06001920 RID: 6432 RVA: 0x0002C0CC File Offset: 0x0002A2CC
		public static NativeSlice<T> Slice<T>(this NativeSlice<T> thisSlice, int start) where T : struct
		{
			return new NativeSlice<T>(thisSlice, start);
		}

		// Token: 0x06001921 RID: 6433 RVA: 0x0002C0E8 File Offset: 0x0002A2E8
		public static NativeSlice<T> Slice<T>(this NativeSlice<T> thisSlice, int start, int length) where T : struct
		{
			return new NativeSlice<T>(thisSlice, start, length);
		}
	}
}
