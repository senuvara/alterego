﻿using System;

namespace Unity.Collections
{
	// Token: 0x020002C5 RID: 709
	internal sealed class NativeSliceDebugView<T> where T : struct
	{
		// Token: 0x06001944 RID: 6468 RVA: 0x0002C5EB File Offset: 0x0002A7EB
		public NativeSliceDebugView(NativeSlice<T> array)
		{
			this.m_Array = array;
		}

		// Token: 0x17000489 RID: 1161
		// (get) Token: 0x06001945 RID: 6469 RVA: 0x0002C5FC File Offset: 0x0002A7FC
		public T[] Items
		{
			get
			{
				return this.m_Array.ToArray();
			}
		}

		// Token: 0x04000912 RID: 2322
		private NativeSlice<T> m_Array;
	}
}
