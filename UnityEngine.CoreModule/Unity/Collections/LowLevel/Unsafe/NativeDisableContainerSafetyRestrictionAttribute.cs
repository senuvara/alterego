﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x0200027D RID: 637
	[AttributeUsage(AttributeTargets.Field)]
	[RequiredByNativeCode]
	public sealed class NativeDisableContainerSafetyRestrictionAttribute : Attribute
	{
		// Token: 0x060016CA RID: 5834 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeDisableContainerSafetyRestrictionAttribute()
		{
		}
	}
}
