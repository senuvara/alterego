﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x02000273 RID: 627
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Struct)]
	public sealed class NativeContainerAttribute : Attribute
	{
		// Token: 0x060016C0 RID: 5824 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeContainerAttribute()
		{
		}
	}
}
