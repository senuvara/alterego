﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x0200027A RID: 634
	[Obsolete("Use NativeSetThreadIndexAttribute instead")]
	[AttributeUsage(AttributeTargets.Struct)]
	[RequiredByNativeCode]
	public sealed class NativeContainerNeedsThreadIndexAttribute : Attribute
	{
		// Token: 0x060016C7 RID: 5831 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeContainerNeedsThreadIndexAttribute()
		{
		}
	}
}
