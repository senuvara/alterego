﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x02000275 RID: 629
	[AttributeUsage(AttributeTargets.Struct)]
	[RequiredByNativeCode]
	public sealed class NativeContainerIsAtomicWriteOnlyAttribute : Attribute
	{
		// Token: 0x060016C2 RID: 5826 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeContainerIsAtomicWriteOnlyAttribute()
		{
		}
	}
}
