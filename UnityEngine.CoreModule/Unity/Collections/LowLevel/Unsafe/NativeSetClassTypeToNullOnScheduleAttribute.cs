﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x0200027E RID: 638
	[AttributeUsage(AttributeTargets.Field)]
	[RequiredByNativeCode]
	public sealed class NativeSetClassTypeToNullOnScheduleAttribute : Attribute
	{
		// Token: 0x060016CB RID: 5835 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeSetClassTypeToNullOnScheduleAttribute()
		{
		}
	}
}
