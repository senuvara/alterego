﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x02000325 RID: 805
	[NativeHeader("Runtime/Export/Unsafe/UnsafeUtility.bindings.h")]
	[StaticAccessor("UnsafeUtility", StaticAccessorType.DoubleColon)]
	public static class UnsafeUtility
	{
		// Token: 0x06001B55 RID: 6997
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetFieldOffsetInStruct(FieldInfo field);

		// Token: 0x06001B56 RID: 6998
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetFieldOffsetInClass(FieldInfo field);

		// Token: 0x06001B57 RID: 6999 RVA: 0x0003069C File Offset: 0x0002E89C
		public static int GetFieldOffset(FieldInfo field)
		{
			int result;
			if (field.DeclaringType.IsValueType)
			{
				result = UnsafeUtility.GetFieldOffsetInStruct(field);
			}
			else if (field.DeclaringType.IsClass)
			{
				result = UnsafeUtility.GetFieldOffsetInClass(field);
			}
			else
			{
				result = -1;
			}
			return result;
		}

		// Token: 0x06001B58 RID: 7000 RVA: 0x000306EC File Offset: 0x0002E8EC
		public unsafe static void* PinGCObjectAndGetAddress(object target, out ulong gcHandle)
		{
			return UnsafeUtility.PinSystemObjectAndGetAddress(target, out gcHandle);
		}

		// Token: 0x06001B59 RID: 7001 RVA: 0x00030708 File Offset: 0x0002E908
		public unsafe static void* PinGCArrayAndGetDataAddress(Array target, out ulong gcHandle)
		{
			return UnsafeUtility.PinSystemArrayAndGetAddress(target, out gcHandle);
		}

		// Token: 0x06001B5A RID: 7002
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void* PinSystemArrayAndGetAddress(object target, out ulong gcHandle);

		// Token: 0x06001B5B RID: 7003
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void* PinSystemObjectAndGetAddress(object target, out ulong gcHandle);

		// Token: 0x06001B5C RID: 7004
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ReleaseGCObject(ulong gcHandle);

		// Token: 0x06001B5D RID: 7005
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern void CopyObjectAddressToPtr(object target, void* dstPtr);

		// Token: 0x06001B5E RID: 7006 RVA: 0x00030724 File Offset: 0x0002E924
		public static bool IsBlittable<T>() where T : struct
		{
			return UnsafeUtility.IsBlittable(typeof(T));
		}

		// Token: 0x06001B5F RID: 7007
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern void* Malloc(long size, int alignment, Allocator allocator);

		// Token: 0x06001B60 RID: 7008
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern void Free(void* memory, Allocator allocator);

		// Token: 0x06001B61 RID: 7009 RVA: 0x00030748 File Offset: 0x0002E948
		public static bool IsValidAllocator(Allocator allocator)
		{
			return allocator > Allocator.None;
		}

		// Token: 0x06001B62 RID: 7010
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern void MemCpy(void* destination, void* source, long size);

		// Token: 0x06001B63 RID: 7011
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern void MemCpyReplicate(void* destination, void* source, int size, int count);

		// Token: 0x06001B64 RID: 7012
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern void MemCpyStride(void* destination, int destinationStride, void* source, int sourceStride, int elementSize, int count);

		// Token: 0x06001B65 RID: 7013
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern void MemMove(void* destination, void* source, long size);

		// Token: 0x06001B66 RID: 7014
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern void MemClear(void* destination, long size);

		// Token: 0x06001B67 RID: 7015
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern int MemCmp(void* ptr1, void* ptr2, long size);

		// Token: 0x06001B68 RID: 7016
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int SizeOf(Type type);

		// Token: 0x06001B69 RID: 7017
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsBlittable(Type type);

		// Token: 0x06001B6A RID: 7018
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void LogError(string msg, string filename, int linenumber);

		// Token: 0x06001B6B RID: 7019 RVA: 0x00030764 File Offset: 0x0002E964
		private static bool IsValueType(Type t)
		{
			return t.IsValueType;
		}

		// Token: 0x06001B6C RID: 7020 RVA: 0x00030780 File Offset: 0x0002E980
		private static bool IsPrimitive(Type t)
		{
			return t.IsPrimitive;
		}

		// Token: 0x06001B6D RID: 7021 RVA: 0x0003079C File Offset: 0x0002E99C
		private static bool IsBlittableValueType(Type t)
		{
			return UnsafeUtility.IsValueType(t) && UnsafeUtility.IsBlittable(t);
		}

		// Token: 0x06001B6E RID: 7022 RVA: 0x000307C8 File Offset: 0x0002E9C8
		private static string GetReasonForTypeNonBlittableImpl(Type t, string name)
		{
			string result;
			if (!UnsafeUtility.IsValueType(t))
			{
				result = string.Format("{0} is not blittable because it is not of value type ({1})\n", name, t);
			}
			else if (UnsafeUtility.IsPrimitive(t))
			{
				result = string.Format("{0} is not blittable ({1})\n", name, t);
			}
			else
			{
				string text = "";
				foreach (FieldInfo fieldInfo in t.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
				{
					if (!UnsafeUtility.IsBlittableValueType(fieldInfo.FieldType))
					{
						text += UnsafeUtility.GetReasonForTypeNonBlittableImpl(fieldInfo.FieldType, string.Format("{0}.{1}", name, fieldInfo.Name));
					}
				}
				result = text;
			}
			return result;
		}

		// Token: 0x06001B6F RID: 7023 RVA: 0x0003087C File Offset: 0x0002EA7C
		internal static bool IsArrayBlittable(Array arr)
		{
			return UnsafeUtility.IsBlittableValueType(arr.GetType().GetElementType());
		}

		// Token: 0x06001B70 RID: 7024 RVA: 0x000308A4 File Offset: 0x0002EAA4
		internal static bool IsGenericListBlittable<T>() where T : struct
		{
			return UnsafeUtility.IsBlittable<T>();
		}

		// Token: 0x06001B71 RID: 7025 RVA: 0x000308C0 File Offset: 0x0002EAC0
		internal static string GetReasonForArrayNonBlittable(Array arr)
		{
			Type elementType = arr.GetType().GetElementType();
			return UnsafeUtility.GetReasonForTypeNonBlittableImpl(elementType, elementType.Name);
		}

		// Token: 0x06001B72 RID: 7026 RVA: 0x000308F0 File Offset: 0x0002EAF0
		internal static string GetReasonForGenericListNonBlittable<T>() where T : struct
		{
			Type typeFromHandle = typeof(T);
			return UnsafeUtility.GetReasonForTypeNonBlittableImpl(typeFromHandle, typeFromHandle.Name);
		}

		// Token: 0x06001B73 RID: 7027 RVA: 0x0003091C File Offset: 0x0002EB1C
		internal static string GetReasonForTypeNonBlittable(Type t)
		{
			return UnsafeUtility.GetReasonForTypeNonBlittableImpl(t, t.Name);
		}

		// Token: 0x06001B74 RID: 7028 RVA: 0x00030940 File Offset: 0x0002EB40
		internal static string GetReasonForValueTypeNonBlittable<T>() where T : struct
		{
			Type typeFromHandle = typeof(T);
			return UnsafeUtility.GetReasonForTypeNonBlittableImpl(typeFromHandle, typeFromHandle.Name);
		}

		// Token: 0x06001B75 RID: 7029 RVA: 0x0003096C File Offset: 0x0002EB6C
		public unsafe static void CopyPtrToStructure<T>(void* ptr, out T output) where T : struct
		{
			output = *(T*)ptr;
		}

		// Token: 0x06001B76 RID: 7030 RVA: 0x0003096C File Offset: 0x0002EB6C
		public unsafe static void CopyStructureToPtr<T>(ref T input, void* ptr) where T : struct
		{
			*(T*)ptr = input;
		}

		// Token: 0x06001B77 RID: 7031 RVA: 0x0003097A File Offset: 0x0002EB7A
		public unsafe static T ReadArrayElement<T>(void* source, int index)
		{
			return *(T*)((byte*)source + index * sizeof(T));
		}

		// Token: 0x06001B78 RID: 7032 RVA: 0x0003098C File Offset: 0x0002EB8C
		public unsafe static T ReadArrayElementWithStride<T>(void* source, int index, int stride)
		{
			return *(T*)((byte*)source + index * stride);
		}

		// Token: 0x06001B79 RID: 7033 RVA: 0x00030998 File Offset: 0x0002EB98
		public unsafe static void WriteArrayElement<T>(void* destination, int index, T value)
		{
			*(T*)((byte*)destination + index * sizeof(T)) = value;
		}

		// Token: 0x06001B7A RID: 7034 RVA: 0x000309AB File Offset: 0x0002EBAB
		public unsafe static void WriteArrayElementWithStride<T>(void* destination, int index, int stride, T value)
		{
			*(T*)((byte*)destination + index * stride) = value;
		}

		// Token: 0x06001B7B RID: 7035 RVA: 0x000309B8 File Offset: 0x0002EBB8
		public unsafe static void* AddressOf<T>(ref T output) where T : struct
		{
			return (void*)(&output);
		}

		// Token: 0x06001B7C RID: 7036 RVA: 0x000309BB File Offset: 0x0002EBBB
		public static int SizeOf<T>() where T : struct
		{
			return sizeof(T);
		}

		// Token: 0x06001B7D RID: 7037 RVA: 0x000309C3 File Offset: 0x0002EBC3
		public static int AlignOf<T>() where T : struct
		{
			return 4;
		}
	}
}
