﻿using System;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x020002C6 RID: 710
	public static class NativeSliceUnsafeUtility
	{
		// Token: 0x06001946 RID: 6470 RVA: 0x0002C61C File Offset: 0x0002A81C
		public unsafe static NativeSlice<T> ConvertExistingDataToNativeSlice<T>(void* dataPointer, int stride, int length) where T : struct
		{
			if (length < 0)
			{
				throw new ArgumentException(string.Format("Invalid length of '{0}'. It must be greater than 0.", length), "length");
			}
			if (stride < 0)
			{
				throw new ArgumentException(string.Format("Invalid stride '{0}'. It must be greater than 0.", stride), "stride");
			}
			return new NativeSlice<T>
			{
				m_Stride = stride,
				m_Buffer = (byte*)dataPointer,
				m_Length = length
			};
		}

		// Token: 0x06001947 RID: 6471 RVA: 0x0002C698 File Offset: 0x0002A898
		public unsafe static void* GetUnsafePtr<T>(this NativeSlice<T> nativeSlice) where T : struct
		{
			return (void*)nativeSlice.m_Buffer;
		}

		// Token: 0x06001948 RID: 6472 RVA: 0x0002C6B4 File Offset: 0x0002A8B4
		public unsafe static void* GetUnsafeReadOnlyPtr<T>(this NativeSlice<T> nativeSlice) where T : struct
		{
			return (void*)nativeSlice.m_Buffer;
		}
	}
}
