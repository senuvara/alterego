﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x02000278 RID: 632
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Struct)]
	public sealed class NativeContainerSupportsDeferredConvertListToArray : Attribute
	{
		// Token: 0x060016C5 RID: 5829 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeContainerSupportsDeferredConvertListToArray()
		{
		}
	}
}
