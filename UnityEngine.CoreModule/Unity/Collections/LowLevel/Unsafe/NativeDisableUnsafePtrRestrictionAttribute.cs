﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x0200027C RID: 636
	[AttributeUsage(AttributeTargets.Field)]
	[RequiredByNativeCode]
	public sealed class NativeDisableUnsafePtrRestrictionAttribute : Attribute
	{
		// Token: 0x060016C9 RID: 5833 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeDisableUnsafePtrRestrictionAttribute()
		{
		}
	}
}
