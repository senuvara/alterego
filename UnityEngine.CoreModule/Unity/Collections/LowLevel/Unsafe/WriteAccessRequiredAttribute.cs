﻿using System;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x0200027B RID: 635
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
	public class WriteAccessRequiredAttribute : Attribute
	{
		// Token: 0x060016C8 RID: 5832 RVA: 0x0000898B File Offset: 0x00006B8B
		public WriteAccessRequiredAttribute()
		{
		}
	}
}
