﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x02000279 RID: 633
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Field)]
	public sealed class NativeSetThreadIndexAttribute : Attribute
	{
		// Token: 0x060016C6 RID: 5830 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeSetThreadIndexAttribute()
		{
		}
	}
}
