﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x02000274 RID: 628
	[AttributeUsage(AttributeTargets.Struct)]
	[RequiredByNativeCode]
	public sealed class NativeContainerIsReadOnlyAttribute : Attribute
	{
		// Token: 0x060016C1 RID: 5825 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeContainerIsReadOnlyAttribute()
		{
		}
	}
}
