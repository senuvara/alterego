﻿using System;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x020002C1 RID: 705
	public static class NativeArrayUnsafeUtility
	{
		// Token: 0x06001918 RID: 6424 RVA: 0x0002BFD0 File Offset: 0x0002A1D0
		public unsafe static NativeArray<T> ConvertExistingDataToNativeArray<T>(void* dataPointer, int length, Allocator allocator) where T : struct
		{
			return new NativeArray<T>
			{
				m_Buffer = dataPointer,
				m_Length = length,
				m_AllocatorLabel = allocator
			};
		}

		// Token: 0x06001919 RID: 6425 RVA: 0x0002C008 File Offset: 0x0002A208
		public unsafe static void* GetUnsafePtr<T>(this NativeArray<T> nativeArray) where T : struct
		{
			return nativeArray.m_Buffer;
		}

		// Token: 0x0600191A RID: 6426 RVA: 0x0002C024 File Offset: 0x0002A224
		public unsafe static void* GetUnsafeReadOnlyPtr<T>(this NativeArray<T> nativeArray) where T : struct
		{
			return nativeArray.m_Buffer;
		}

		// Token: 0x0600191B RID: 6427 RVA: 0x0002C040 File Offset: 0x0002A240
		public unsafe static void* GetUnsafeBufferPointerWithoutChecks<T>(NativeArray<T> nativeArray) where T : struct
		{
			return nativeArray.m_Buffer;
		}
	}
}
