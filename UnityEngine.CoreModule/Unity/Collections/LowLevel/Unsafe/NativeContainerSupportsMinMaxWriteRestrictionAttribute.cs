﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections.LowLevel.Unsafe
{
	// Token: 0x02000276 RID: 630
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Struct)]
	public sealed class NativeContainerSupportsMinMaxWriteRestrictionAttribute : Attribute
	{
		// Token: 0x060016C3 RID: 5827 RVA: 0x0000898B File Offset: 0x00006B8B
		public NativeContainerSupportsMinMaxWriteRestrictionAttribute()
		{
		}
	}
}
