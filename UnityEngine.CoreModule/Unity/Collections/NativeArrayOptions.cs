﻿using System;

namespace Unity.Collections
{
	// Token: 0x020002BD RID: 701
	public enum NativeArrayOptions
	{
		// Token: 0x04000905 RID: 2309
		UninitializedMemory,
		// Token: 0x04000906 RID: 2310
		ClearMemory
	}
}
