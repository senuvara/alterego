﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections
{
	// Token: 0x0200026F RID: 623
	[AttributeUsage(AttributeTargets.Field)]
	[RequiredByNativeCode]
	public sealed class DeallocateOnJobCompletionAttribute : Attribute
	{
		// Token: 0x060016BC RID: 5820 RVA: 0x0000898B File Offset: 0x00006B8B
		public DeallocateOnJobCompletionAttribute()
		{
		}
	}
}
