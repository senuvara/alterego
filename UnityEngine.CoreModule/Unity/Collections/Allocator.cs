﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections
{
	// Token: 0x0200027F RID: 639
	[UsedByNativeCode]
	public enum Allocator
	{
		// Token: 0x04000848 RID: 2120
		Invalid,
		// Token: 0x04000849 RID: 2121
		None,
		// Token: 0x0400084A RID: 2122
		Temp,
		// Token: 0x0400084B RID: 2123
		TempJob,
		// Token: 0x0400084C RID: 2124
		Persistent
	}
}
