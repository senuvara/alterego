﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections
{
	// Token: 0x0200026E RID: 622
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public sealed class WriteOnlyAttribute : Attribute
	{
		// Token: 0x060016BB RID: 5819 RVA: 0x0000898B File Offset: 0x00006B8B
		public WriteOnlyAttribute()
		{
		}
	}
}
