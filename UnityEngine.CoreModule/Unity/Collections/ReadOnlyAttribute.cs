﻿using System;
using UnityEngine.Scripting;

namespace Unity.Collections
{
	// Token: 0x0200026D RID: 621
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	[RequiredByNativeCode]
	public sealed class ReadOnlyAttribute : Attribute
	{
		// Token: 0x060016BA RID: 5818 RVA: 0x0000898B File Offset: 0x00006B8B
		public ReadOnlyAttribute()
		{
		}
	}
}
