﻿using System;

namespace Unity.Collections
{
	// Token: 0x020002BB RID: 699
	public enum NativeLeakDetectionMode
	{
		// Token: 0x04000901 RID: 2305
		Enabled,
		// Token: 0x04000902 RID: 2306
		Disabled
	}
}
