﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Internal;

namespace Unity.Collections
{
	// Token: 0x020002C3 RID: 707
	[DebuggerDisplay("Length = {Length}")]
	[DebuggerTypeProxy(typeof(NativeSliceDebugView<>))]
	[NativeContainer]
	[NativeContainerSupportsMinMaxWriteRestriction]
	public struct NativeSlice<T> : IEnumerable<!0>, IEquatable<NativeSlice<T>>, IEnumerable where T : struct
	{
		// Token: 0x06001922 RID: 6434 RVA: 0x0002C105 File Offset: 0x0002A305
		public NativeSlice(NativeSlice<T> slice, int start)
		{
			this = new NativeSlice<T>(slice, start, slice.Length - start);
		}

		// Token: 0x06001923 RID: 6435 RVA: 0x0002C119 File Offset: 0x0002A319
		public NativeSlice(NativeSlice<T> slice, int start, int length)
		{
			this.m_Stride = slice.m_Stride;
			this.m_Buffer = slice.m_Buffer + this.m_Stride * start;
			this.m_Length = length;
		}

		// Token: 0x06001924 RID: 6436 RVA: 0x0002C147 File Offset: 0x0002A347
		public NativeSlice(NativeArray<T> array)
		{
			this = new NativeSlice<T>(array, 0, array.Length);
		}

		// Token: 0x06001925 RID: 6437 RVA: 0x0002C159 File Offset: 0x0002A359
		public NativeSlice(NativeArray<T> array, int start)
		{
			this = new NativeSlice<T>(array, start, array.Length - start);
		}

		// Token: 0x06001926 RID: 6438 RVA: 0x0002C170 File Offset: 0x0002A370
		public unsafe NativeSlice(NativeArray<T> array, int start, int length)
		{
			this.m_Stride = UnsafeUtility.SizeOf<T>();
			byte* buffer = (byte*)array.m_Buffer + this.m_Stride * start;
			this.m_Buffer = buffer;
			this.m_Length = length;
		}

		// Token: 0x06001927 RID: 6439 RVA: 0x0002C1AC File Offset: 0x0002A3AC
		public static implicit operator NativeSlice<T>(NativeArray<T> array)
		{
			return new NativeSlice<T>(array);
		}

		// Token: 0x06001928 RID: 6440 RVA: 0x0002C1C8 File Offset: 0x0002A3C8
		public NativeSlice<U> SliceConvert<U>() where U : struct
		{
			int num = UnsafeUtility.SizeOf<U>();
			NativeSlice<U> result;
			result.m_Buffer = this.m_Buffer;
			result.m_Stride = num;
			result.m_Length = this.m_Length * this.m_Stride / num;
			return result;
		}

		// Token: 0x06001929 RID: 6441 RVA: 0x0002C210 File Offset: 0x0002A410
		public NativeSlice<U> SliceWithStride<U>(int offset) where U : struct
		{
			NativeSlice<U> result;
			result.m_Buffer = this.m_Buffer + offset;
			result.m_Stride = this.m_Stride;
			result.m_Length = this.m_Length;
			return result;
		}

		// Token: 0x0600192A RID: 6442 RVA: 0x0002C250 File Offset: 0x0002A450
		public NativeSlice<U> SliceWithStride<U>() where U : struct
		{
			return this.SliceWithStride<U>(0);
		}

		// Token: 0x0600192B RID: 6443 RVA: 0x00007476 File Offset: 0x00005676
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		private void CheckReadIndex(int index)
		{
		}

		// Token: 0x0600192C RID: 6444 RVA: 0x00007476 File Offset: 0x00005676
		[Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
		private void CheckWriteIndex(int index)
		{
		}

		// Token: 0x17000484 RID: 1156
		public unsafe T this[int index]
		{
			get
			{
				return UnsafeUtility.ReadArrayElementWithStride<T>((void*)this.m_Buffer, index, this.m_Stride);
			}
			[WriteAccessRequired]
			set
			{
				UnsafeUtility.WriteArrayElementWithStride<T>((void*)this.m_Buffer, index, this.m_Stride, value);
			}
		}

		// Token: 0x0600192F RID: 6447 RVA: 0x0002C2A9 File Offset: 0x0002A4A9
		[WriteAccessRequired]
		public void CopyFrom(NativeSlice<T> slice)
		{
			UnsafeUtility.MemCpyStride(this.GetUnsafePtr<T>(), this.Stride, slice.GetUnsafeReadOnlyPtr<T>(), slice.Stride, UnsafeUtility.SizeOf<T>(), this.m_Length);
		}

		// Token: 0x06001930 RID: 6448 RVA: 0x0002C2DC File Offset: 0x0002A4DC
		[WriteAccessRequired]
		public void CopyFrom(T[] array)
		{
			for (int num = 0; num != this.m_Length; num++)
			{
				this[num] = array[num];
			}
		}

		// Token: 0x06001931 RID: 6449 RVA: 0x0002C310 File Offset: 0x0002A510
		public void CopyTo(NativeArray<T> array)
		{
			int num = UnsafeUtility.SizeOf<T>();
			UnsafeUtility.MemCpyStride(array.GetUnsafePtr<T>(), num, this.GetUnsafeReadOnlyPtr<T>(), this.Stride, num, this.m_Length);
		}

		// Token: 0x06001932 RID: 6450 RVA: 0x0002C348 File Offset: 0x0002A548
		public void CopyTo(T[] array)
		{
			for (int num = 0; num != this.m_Length; num++)
			{
				array[num] = this[num];
			}
		}

		// Token: 0x06001933 RID: 6451 RVA: 0x0002C37C File Offset: 0x0002A57C
		public T[] ToArray()
		{
			T[] array = new T[this.Length];
			this.CopyTo(array);
			return array;
		}

		// Token: 0x17000485 RID: 1157
		// (get) Token: 0x06001934 RID: 6452 RVA: 0x0002C3A8 File Offset: 0x0002A5A8
		public int Stride
		{
			[CompilerGenerated]
			get
			{
				return this.m_Stride;
			}
		}

		// Token: 0x17000486 RID: 1158
		// (get) Token: 0x06001935 RID: 6453 RVA: 0x0002C3C4 File Offset: 0x0002A5C4
		public int Length
		{
			[CompilerGenerated]
			get
			{
				return this.m_Length;
			}
		}

		// Token: 0x06001936 RID: 6454 RVA: 0x0002C3E0 File Offset: 0x0002A5E0
		public NativeSlice<T>.Enumerator GetEnumerator()
		{
			return new NativeSlice<T>.Enumerator(ref this);
		}

		// Token: 0x06001937 RID: 6455 RVA: 0x0002C3FC File Offset: 0x0002A5FC
		IEnumerator<T> IEnumerable<!0>.GetEnumerator()
		{
			return new NativeSlice<T>.Enumerator(ref this);
		}

		// Token: 0x06001938 RID: 6456 RVA: 0x0002C41C File Offset: 0x0002A61C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06001939 RID: 6457 RVA: 0x0002C43C File Offset: 0x0002A63C
		public bool Equals(NativeSlice<T> other)
		{
			return this.m_Buffer == other.m_Buffer && this.m_Stride == other.m_Stride && this.m_Length == other.m_Length;
		}

		// Token: 0x0600193A RID: 6458 RVA: 0x0002C488 File Offset: 0x0002A688
		public override bool Equals(object obj)
		{
			return !object.ReferenceEquals(null, obj) && obj is NativeSlice<T> && this.Equals((NativeSlice<T>)obj);
		}

		// Token: 0x0600193B RID: 6459 RVA: 0x0002C4CC File Offset: 0x0002A6CC
		public override int GetHashCode()
		{
			int num = this.m_Buffer;
			num = (num * 397 ^ this.m_Stride);
			return num * 397 ^ this.m_Length;
		}

		// Token: 0x0600193C RID: 6460 RVA: 0x0002C50C File Offset: 0x0002A70C
		public static bool operator ==(NativeSlice<T> left, NativeSlice<T> right)
		{
			return left.Equals(right);
		}

		// Token: 0x0600193D RID: 6461 RVA: 0x0002C52C File Offset: 0x0002A72C
		public static bool operator !=(NativeSlice<T> left, NativeSlice<T> right)
		{
			return !left.Equals(right);
		}

		// Token: 0x0400090D RID: 2317
		[NativeDisableUnsafePtrRestriction]
		internal unsafe byte* m_Buffer;

		// Token: 0x0400090E RID: 2318
		internal int m_Stride;

		// Token: 0x0400090F RID: 2319
		internal int m_Length;

		// Token: 0x020002C4 RID: 708
		[ExcludeFromDocs]
		public struct Enumerator : IEnumerator<T>, IEnumerator, IDisposable
		{
			// Token: 0x0600193E RID: 6462 RVA: 0x0002C54C File Offset: 0x0002A74C
			public Enumerator(ref NativeSlice<T> array)
			{
				this.m_Array = array;
				this.m_Index = -1;
			}

			// Token: 0x0600193F RID: 6463 RVA: 0x00007476 File Offset: 0x00005676
			public void Dispose()
			{
			}

			// Token: 0x06001940 RID: 6464 RVA: 0x0002C564 File Offset: 0x0002A764
			public bool MoveNext()
			{
				this.m_Index++;
				return this.m_Index < this.m_Array.Length;
			}

			// Token: 0x06001941 RID: 6465 RVA: 0x0002C59A File Offset: 0x0002A79A
			public void Reset()
			{
				this.m_Index = -1;
			}

			// Token: 0x17000488 RID: 1160
			// (get) Token: 0x06001942 RID: 6466 RVA: 0x0002C5A4 File Offset: 0x0002A7A4
			public T Current
			{
				[CompilerGenerated]
				get
				{
					return this.m_Array[this.m_Index];
				}
			}

			// Token: 0x17000487 RID: 1159
			// (get) Token: 0x06001943 RID: 6467 RVA: 0x0002C5CC File Offset: 0x0002A7CC
			object IEnumerator.Current
			{
				[CompilerGenerated]
				get
				{
					return this.Current;
				}
			}

			// Token: 0x04000910 RID: 2320
			private NativeSlice<T> m_Array;

			// Token: 0x04000911 RID: 2321
			private int m_Index;
		}
	}
}
