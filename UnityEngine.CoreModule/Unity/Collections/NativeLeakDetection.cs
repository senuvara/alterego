﻿using System;

namespace Unity.Collections
{
	// Token: 0x020002BC RID: 700
	public static class NativeLeakDetection
	{
		// Token: 0x1700047D RID: 1149
		// (get) Token: 0x060018EC RID: 6380 RVA: 0x0002BA88 File Offset: 0x00029C88
		// (set) Token: 0x060018ED RID: 6381 RVA: 0x0002BAA2 File Offset: 0x00029CA2
		public static NativeLeakDetectionMode Mode
		{
			get
			{
				return (NativeLeakDetectionMode)NativeLeakDetection.s_NativeLeakDetectionMode;
			}
			set
			{
				NativeLeakDetection.s_NativeLeakDetectionMode = (int)value;
			}
		}

		// Token: 0x04000903 RID: 2307
		private static int s_NativeLeakDetectionMode;
	}
}
