﻿using System;
using System.Runtime.CompilerServices;

namespace Unity.Collections
{
	// Token: 0x020002C0 RID: 704
	internal sealed class NativeArrayDebugView<T> where T : struct
	{
		// Token: 0x06001916 RID: 6422 RVA: 0x0002BF9F File Offset: 0x0002A19F
		public NativeArrayDebugView(NativeArray<T> array)
		{
			this.m_Array = array;
		}

		// Token: 0x17000483 RID: 1155
		// (get) Token: 0x06001917 RID: 6423 RVA: 0x0002BFB0 File Offset: 0x0002A1B0
		public T[] Items
		{
			[CompilerGenerated]
			get
			{
				return this.m_Array.ToArray();
			}
		}

		// Token: 0x0400090C RID: 2316
		private NativeArray<T> m_Array;
	}
}
