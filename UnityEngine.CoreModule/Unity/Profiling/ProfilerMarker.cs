﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace Unity.Profiling
{
	// Token: 0x02000344 RID: 836
	[UsedByNativeCode]
	[NativeHeader("Runtime/Profiler/ScriptBindings/ProfilerMarker.bindings.h")]
	public struct ProfilerMarker
	{
		// Token: 0x06001C04 RID: 7172 RVA: 0x00031693 File Offset: 0x0002F893
		public ProfilerMarker(string name)
		{
			this.m_Ptr = ProfilerMarker.Internal_Create(name, MarkerFlags.Default);
		}

		// Token: 0x06001C05 RID: 7173 RVA: 0x000316A3 File Offset: 0x0002F8A3
		[Conditional("ENABLE_PROFILER")]
		public void Begin()
		{
			ProfilerMarker.Internal_Begin(this.m_Ptr);
		}

		// Token: 0x06001C06 RID: 7174 RVA: 0x000316B1 File Offset: 0x0002F8B1
		[Conditional("ENABLE_PROFILER")]
		public void Begin(UnityEngine.Object contextUnityObject)
		{
			ProfilerMarker.Internal_BeginWithObject(this.m_Ptr, contextUnityObject);
		}

		// Token: 0x06001C07 RID: 7175 RVA: 0x000316C0 File Offset: 0x0002F8C0
		[Conditional("ENABLE_PROFILER")]
		public void End()
		{
			ProfilerMarker.Internal_End(this.m_Ptr);
		}

		// Token: 0x06001C08 RID: 7176 RVA: 0x000316D0 File Offset: 0x0002F8D0
		public ProfilerMarker.AutoScope Auto()
		{
			return new ProfilerMarker.AutoScope(this.m_Ptr);
		}

		// Token: 0x06001C09 RID: 7177
		[NativeConditional("ENABLE_PROFILER", "NULL")]
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Internal_Create(string name, MarkerFlags flags);

		// Token: 0x06001C0A RID: 7178
		[NativeConditional("ENABLE_PROFILER")]
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Begin(IntPtr markerPtr);

		// Token: 0x06001C0B RID: 7179
		[NativeConditional("ENABLE_PROFILER")]
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_BeginWithObject(IntPtr markerPtr, UnityEngine.Object contextUnityObject);

		// Token: 0x06001C0C RID: 7180
		[NativeConditional("ENABLE_PROFILER")]
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_End(IntPtr markerPtr);

		// Token: 0x04000AA1 RID: 2721
		[NativeDisableUnsafePtrRestriction]
		internal IntPtr m_Ptr;

		// Token: 0x02000345 RID: 837
		[UsedByNativeCode]
		public struct AutoScope : IDisposable
		{
			// Token: 0x06001C0D RID: 7181 RVA: 0x000316F0 File Offset: 0x0002F8F0
			internal AutoScope(IntPtr markerPtr)
			{
				this.m_Ptr = markerPtr;
				ProfilerMarker.Internal_Begin(markerPtr);
			}

			// Token: 0x06001C0E RID: 7182 RVA: 0x00031700 File Offset: 0x0002F900
			public void Dispose()
			{
				ProfilerMarker.Internal_End(this.m_Ptr);
			}

			// Token: 0x04000AA2 RID: 2722
			[NativeDisableUnsafePtrRestriction]
			internal IntPtr m_Ptr;
		}
	}
}
