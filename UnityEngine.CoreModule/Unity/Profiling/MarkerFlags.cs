﻿using System;
using UnityEngine.Bindings;

namespace Unity.Profiling
{
	// Token: 0x02000343 RID: 835
	[NativeHeader("Runtime/Profiler/ScriptBindings/ProfilerMarker.bindings.h")]
	[Flags]
	internal enum MarkerFlags
	{
		// Token: 0x04000A9A RID: 2714
		Default = 0,
		// Token: 0x04000A9B RID: 2715
		AvailabilityEditor = 4,
		// Token: 0x04000A9C RID: 2716
		AvailabilityNonDevelopment = 8,
		// Token: 0x04000A9D RID: 2717
		Warning = 16,
		// Token: 0x04000A9E RID: 2718
		VerbosityDebug = 1024,
		// Token: 0x04000A9F RID: 2719
		VerbosityInternal = 2048,
		// Token: 0x04000AA0 RID: 2720
		VerbosityAdvanced = 4096
	}
}
