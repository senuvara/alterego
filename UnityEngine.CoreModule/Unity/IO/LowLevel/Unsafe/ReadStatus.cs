﻿using System;

namespace Unity.IO.LowLevel.Unsafe
{
	// Token: 0x02000023 RID: 35
	public enum ReadStatus
	{
		// Token: 0x0400005D RID: 93
		Complete,
		// Token: 0x0400005E RID: 94
		InProgress,
		// Token: 0x0400005F RID: 95
		Failed
	}
}
