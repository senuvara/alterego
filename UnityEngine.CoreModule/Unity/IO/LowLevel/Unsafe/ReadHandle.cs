﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Bindings;

namespace Unity.IO.LowLevel.Unsafe
{
	// Token: 0x02000024 RID: 36
	public struct ReadHandle : IDisposable
	{
		// Token: 0x06000262 RID: 610 RVA: 0x0000858C File Offset: 0x0000678C
		public bool IsValid()
		{
			return ReadHandle.IsReadHandleValid(this);
		}

		// Token: 0x06000263 RID: 611 RVA: 0x000085AC File Offset: 0x000067AC
		public void Dispose()
		{
			if (!ReadHandle.IsReadHandleValid(this))
			{
				throw new InvalidOperationException("ReadHandle.Dispose cannot be called twice on the same ReadHandle");
			}
			if (this.Status == ReadStatus.InProgress)
			{
				throw new InvalidOperationException("ReadHandle.Dispose cannot be called until the read operation completes");
			}
			ReadHandle.ReleaseReadHandle(this);
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000264 RID: 612 RVA: 0x000085EC File Offset: 0x000067EC
		public JobHandle JobHandle
		{
			get
			{
				if (!ReadHandle.IsReadHandleValid(this))
				{
					throw new InvalidOperationException("ReadHandle.JobHandle cannot be called after the ReadHandle has been disposed");
				}
				return ReadHandle.GetJobHandle(this);
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000265 RID: 613 RVA: 0x00008628 File Offset: 0x00006828
		public ReadStatus Status
		{
			get
			{
				if (!ReadHandle.IsReadHandleValid(this))
				{
					throw new InvalidOperationException("ReadHandle.Status cannot be called after the ReadHandle has been disposed");
				}
				return ReadHandle.GetReadStatus(this);
			}
		}

		// Token: 0x06000266 RID: 614 RVA: 0x00008663 File Offset: 0x00006863
		[FreeFunction("AsyncReadManagerManaged::GetReadStatus", IsThreadSafe = true)]
		[ThreadAndSerializationSafe]
		private static ReadStatus GetReadStatus(ReadHandle handle)
		{
			return ReadHandle.GetReadStatus_Injected(ref handle);
		}

		// Token: 0x06000267 RID: 615 RVA: 0x0000866C File Offset: 0x0000686C
		[ThreadAndSerializationSafe]
		[FreeFunction("AsyncReadManagerManaged::ReleaseReadHandle", IsThreadSafe = true)]
		private static void ReleaseReadHandle(ReadHandle handle)
		{
			ReadHandle.ReleaseReadHandle_Injected(ref handle);
		}

		// Token: 0x06000268 RID: 616 RVA: 0x00008675 File Offset: 0x00006875
		[ThreadAndSerializationSafe]
		[FreeFunction("AsyncReadManagerManaged::IsReadHandleValid", IsThreadSafe = true)]
		private static bool IsReadHandleValid(ReadHandle handle)
		{
			return ReadHandle.IsReadHandleValid_Injected(ref handle);
		}

		// Token: 0x06000269 RID: 617 RVA: 0x00008680 File Offset: 0x00006880
		[FreeFunction("AsyncReadManagerManaged::GetJobHandle", IsThreadSafe = true)]
		[ThreadAndSerializationSafe]
		private static JobHandle GetJobHandle(ReadHandle handle)
		{
			JobHandle result;
			ReadHandle.GetJobHandle_Injected(ref handle, out result);
			return result;
		}

		// Token: 0x0600026A RID: 618
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern ReadStatus GetReadStatus_Injected(ref ReadHandle handle);

		// Token: 0x0600026B RID: 619
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ReleaseReadHandle_Injected(ref ReadHandle handle);

		// Token: 0x0600026C RID: 620
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsReadHandleValid_Injected(ref ReadHandle handle);

		// Token: 0x0600026D RID: 621
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetJobHandle_Injected(ref ReadHandle handle, out JobHandle ret);

		// Token: 0x04000060 RID: 96
		[NativeDisableUnsafePtrRestriction]
		internal IntPtr ptr;

		// Token: 0x04000061 RID: 97
		internal int version;
	}
}
