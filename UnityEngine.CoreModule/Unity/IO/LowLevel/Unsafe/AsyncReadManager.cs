﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Bindings;

namespace Unity.IO.LowLevel.Unsafe
{
	// Token: 0x02000025 RID: 37
	[NativeHeader("Runtime/File/AsyncReadManagerManagedApi.h")]
	public static class AsyncReadManager
	{
		// Token: 0x0600026E RID: 622 RVA: 0x00008698 File Offset: 0x00006898
		[FreeFunction("AsyncReadManagerManaged::Read", IsThreadSafe = true)]
		[ThreadAndSerializationSafe]
		private unsafe static ReadHandle ReadInternal(string filename, void* cmds, uint cmdCount)
		{
			ReadHandle result;
			AsyncReadManager.ReadInternal_Injected(filename, cmds, cmdCount, out result);
			return result;
		}

		// Token: 0x0600026F RID: 623 RVA: 0x000086B0 File Offset: 0x000068B0
		public unsafe static ReadHandle Read(string filename, ReadCommand* readCmds, uint readCmdCount)
		{
			return AsyncReadManager.ReadInternal(filename, (void*)readCmds, readCmdCount);
		}

		// Token: 0x06000270 RID: 624
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void ReadInternal_Injected(string filename, void* cmds, uint cmdCount, out ReadHandle ret);
	}
}
