﻿using System;

namespace Unity.IO.LowLevel.Unsafe
{
	// Token: 0x02000022 RID: 34
	public struct ReadCommand
	{
		// Token: 0x04000059 RID: 89
		public unsafe void* Buffer;

		// Token: 0x0400005A RID: 90
		public long Offset;

		// Token: 0x0400005B RID: 91
		public long Size;
	}
}
