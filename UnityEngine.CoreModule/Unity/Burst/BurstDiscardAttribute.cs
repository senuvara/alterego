﻿using System;

namespace Unity.Burst
{
	// Token: 0x02000267 RID: 615
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
	public class BurstDiscardAttribute : Attribute
	{
		// Token: 0x060016A2 RID: 5794 RVA: 0x0000898B File Offset: 0x00006B8B
		public BurstDiscardAttribute()
		{
		}
	}
}
