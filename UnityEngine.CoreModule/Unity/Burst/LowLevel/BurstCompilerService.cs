﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Bindings;

namespace Unity.Burst.LowLevel
{
	// Token: 0x02000265 RID: 613
	[NativeHeader("Runtime/Burst/Burst.h")]
	[NativeHeader("Runtime/Burst/BurstDelegateCache.h")]
	[StaticAccessor("BurstCompilerService::Get()", StaticAccessorType.Arrow)]
	internal static class BurstCompilerService
	{
		// Token: 0x06001697 RID: 5783
		[NativeMethod("Initialize")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string InitializeInternal(string path, BurstCompilerService.ExtractCompilerFlags extractCompilerFlags);

		// Token: 0x06001698 RID: 5784
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetDisassembly(MethodInfo m, string compilerOptions);

		// Token: 0x06001699 RID: 5785
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int CompileAsyncDelegateMethod(object delegateMethod, string compilerOptions);

		// Token: 0x0600169A RID: 5786
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern void* GetAsyncCompiledAsyncDelegateMethod(int userID);

		// Token: 0x0600169B RID: 5787
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetMethodSignature(MethodInfo method);

		// Token: 0x17000445 RID: 1093
		// (get) Token: 0x0600169C RID: 5788
		public static extern bool IsInitialized { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600169D RID: 5789 RVA: 0x0002850C File Offset: 0x0002670C
		public static void Initialize(string folderRuntime, BurstCompilerService.ExtractCompilerFlags extractCompilerFlags)
		{
			if (folderRuntime == null)
			{
				throw new ArgumentNullException("folderRuntime");
			}
			if (extractCompilerFlags == null)
			{
				throw new ArgumentNullException("extractCompilerFlags");
			}
			if (!Directory.Exists(folderRuntime))
			{
				Debug.LogError(string.Format("Unable to initialize the burst JIT compiler. The folder `{0}` does not exist", folderRuntime));
			}
			else
			{
				string text = BurstCompilerService.InitializeInternal(folderRuntime, extractCompilerFlags);
				if (!string.IsNullOrEmpty(text))
				{
					Debug.LogError(string.Format("Unexpected error while trying to initialize the burst JIT compiler: {0}", text));
				}
			}
		}

		// Token: 0x02000266 RID: 614
		// (Invoke) Token: 0x0600169F RID: 5791
		public delegate bool ExtractCompilerFlags(Type jobType, out string flags);
	}
}
