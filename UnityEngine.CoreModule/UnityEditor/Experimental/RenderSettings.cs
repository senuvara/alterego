﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEditor.Experimental
{
	// Token: 0x02000112 RID: 274
	[NativeHeader("Runtime/Camera/RenderSettings.h")]
	[StaticAccessor("GetRenderSettings()", StaticAccessorType.Dot)]
	public sealed class RenderSettings
	{
		// Token: 0x06000B8F RID: 2959 RVA: 0x00002370 File Offset: 0x00000570
		public RenderSettings()
		{
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x06000B90 RID: 2960
		// (set) Token: 0x06000B91 RID: 2961
		public static extern bool useRadianceAmbientProbe { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
