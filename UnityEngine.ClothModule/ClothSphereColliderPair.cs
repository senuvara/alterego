﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000004 RID: 4
	[UsedByNativeCode]
	public struct ClothSphereColliderPair
	{
		// Token: 0x0600004A RID: 74 RVA: 0x000021AA File Offset: 0x000003AA
		public ClothSphereColliderPair(SphereCollider a)
		{
			this.m_First = null;
			this.m_Second = null;
			this.first = a;
			this.second = null;
		}

		// Token: 0x0600004B RID: 75 RVA: 0x000021C9 File Offset: 0x000003C9
		public ClothSphereColliderPair(SphereCollider a, SphereCollider b)
		{
			this.m_First = null;
			this.m_Second = null;
			this.first = a;
			this.second = b;
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600004C RID: 76 RVA: 0x000021E8 File Offset: 0x000003E8
		// (set) Token: 0x0600004D RID: 77 RVA: 0x00002203 File Offset: 0x00000403
		public SphereCollider first
		{
			get
			{
				return this.m_First;
			}
			set
			{
				this.m_First = value;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600004E RID: 78 RVA: 0x00002210 File Offset: 0x00000410
		// (set) Token: 0x0600004F RID: 79 RVA: 0x0000222B File Offset: 0x0000042B
		public SphereCollider second
		{
			get
			{
				return this.m_Second;
			}
			set
			{
				this.m_Second = value;
			}
		}

		// Token: 0x04000003 RID: 3
		private SphereCollider m_First;

		// Token: 0x04000004 RID: 4
		private SphereCollider m_Second;
	}
}
