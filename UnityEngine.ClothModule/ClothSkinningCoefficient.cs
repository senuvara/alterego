﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000003 RID: 3
	[UsedByNativeCode]
	public struct ClothSkinningCoefficient
	{
		// Token: 0x04000001 RID: 1
		public float maxDistance;

		// Token: 0x04000002 RID: 2
		public float collisionSphereDistance;
	}
}
