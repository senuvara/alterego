﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000002 RID: 2
	[RequireComponent(typeof(Transform), typeof(SkinnedMeshRenderer))]
	[NativeHeader("Runtime/Cloth/Cloth.h")]
	[NativeClass("Unity::Cloth")]
	public sealed class Cloth : Component
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public Cloth()
		{
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2
		// (set) Token: 0x06000003 RID: 3
		public extern float sleepThreshold { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000004 RID: 4
		// (set) Token: 0x06000005 RID: 5
		public extern float bendingStiffness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000006 RID: 6
		// (set) Token: 0x06000007 RID: 7
		public extern float stretchingStiffness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000008 RID: 8
		// (set) Token: 0x06000009 RID: 9
		public extern float damping { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600000A RID: 10 RVA: 0x00002058 File Offset: 0x00000258
		// (set) Token: 0x0600000B RID: 11 RVA: 0x0000206E File Offset: 0x0000026E
		public Vector3 externalAcceleration
		{
			get
			{
				Vector3 result;
				this.get_externalAcceleration_Injected(out result);
				return result;
			}
			set
			{
				this.set_externalAcceleration_Injected(ref value);
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600000C RID: 12 RVA: 0x00002078 File Offset: 0x00000278
		// (set) Token: 0x0600000D RID: 13 RVA: 0x0000208E File Offset: 0x0000028E
		public Vector3 randomAcceleration
		{
			get
			{
				Vector3 result;
				this.get_randomAcceleration_Injected(out result);
				return result;
			}
			set
			{
				this.set_randomAcceleration_Injected(ref value);
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600000E RID: 14
		// (set) Token: 0x0600000F RID: 15
		public extern bool useGravity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000010 RID: 16
		// (set) Token: 0x06000011 RID: 17
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000012 RID: 18
		// (set) Token: 0x06000013 RID: 19
		public extern float friction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000014 RID: 20
		// (set) Token: 0x06000015 RID: 21
		public extern float collisionMassScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000016 RID: 22
		// (set) Token: 0x06000017 RID: 23
		public extern bool enableContinuousCollision { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000018 RID: 24
		// (set) Token: 0x06000019 RID: 25
		public extern float useVirtualParticles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600001A RID: 26
		// (set) Token: 0x0600001B RID: 27
		public extern float worldVelocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600001C RID: 28
		// (set) Token: 0x0600001D RID: 29
		public extern float worldAccelerationScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600001E RID: 30
		// (set) Token: 0x0600001F RID: 31
		public extern float clothSolverFrequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000020 RID: 32
		// (set) Token: 0x06000021 RID: 33
		public extern bool useTethers { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000022 RID: 34
		// (set) Token: 0x06000023 RID: 35
		public extern float stiffnessFrequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000024 RID: 36
		// (set) Token: 0x06000025 RID: 37
		public extern float selfCollisionDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000026 RID: 38
		// (set) Token: 0x06000027 RID: 39
		public extern float selfCollisionStiffness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000028 RID: 40 RVA: 0x00002098 File Offset: 0x00000298
		public void GetVirtualParticleIndices(List<uint> indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			this.GetVirtualParticleIndicesMono(indices);
		}

		// Token: 0x06000029 RID: 41 RVA: 0x000020B3 File Offset: 0x000002B3
		public void SetVirtualParticleIndices(List<uint> indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			this.SetVirtualParticleIndicesMono(indices);
		}

		// Token: 0x0600002A RID: 42 RVA: 0x000020CE File Offset: 0x000002CE
		public void GetVirtualParticleWeights(List<Vector3> weights)
		{
			if (weights == null)
			{
				throw new ArgumentNullException("weights");
			}
			this.GetVirtualParticleWeightsMono(weights);
		}

		// Token: 0x0600002B RID: 43 RVA: 0x000020E9 File Offset: 0x000002E9
		public void SetVirtualParticleWeights(List<Vector3> weights)
		{
			if (weights == null)
			{
				throw new ArgumentNullException("weights");
			}
			this.SetVirtualParticleWeightsMono(weights);
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00002104 File Offset: 0x00000304
		public void GetSelfAndInterCollisionIndices(List<uint> indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			this.GetSelfAndInterCollisionIndicesMono(indices);
		}

		// Token: 0x0600002D RID: 45 RVA: 0x0000211F File Offset: 0x0000031F
		public void SetSelfAndInterCollisionIndices(List<uint> indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			this.SetSelfAndInterCollisionIndicesMono(indices);
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600002E RID: 46
		// (set) Token: 0x0600002F RID: 47
		[Obsolete("Deprecated. Cloth.selfCollisions is no longer supported since Unity 5.0.", true)]
		public extern bool selfCollision { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000030 RID: 48
		public extern Vector3[] vertices { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000031 RID: 49
		public extern Vector3[] normals { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000032 RID: 50
		// (set) Token: 0x06000033 RID: 51
		[Obsolete("useContinuousCollision is no longer supported, use enableContinuousCollision instead")]
		public extern float useContinuousCollision { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000034 RID: 52 RVA: 0x0000213A File Offset: 0x0000033A
		public void ClearTransformMotion()
		{
			Cloth.INTERNAL_CALL_ClearTransformMotion(this);
		}

		// Token: 0x06000035 RID: 53
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ClearTransformMotion(Cloth self);

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000036 RID: 54
		// (set) Token: 0x06000037 RID: 55
		public extern ClothSkinningCoefficient[] coefficients { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000038 RID: 56
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetEnabledFading(bool enabled, [DefaultValue("0.5f")] float interpolationTime);

		// Token: 0x06000039 RID: 57 RVA: 0x00002144 File Offset: 0x00000344
		[ExcludeFromDocs]
		public void SetEnabledFading(bool enabled)
		{
			float interpolationTime = 0.5f;
			this.SetEnabledFading(enabled, interpolationTime);
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600003A RID: 58 RVA: 0x00002160 File Offset: 0x00000360
		// (set) Token: 0x0600003B RID: 59 RVA: 0x0000218C File Offset: 0x0000038C
		[Obsolete("Parameter solverFrequency is obsolete and no longer supported. Please use clothSolverFrequency instead.")]
		public bool solverFrequency
		{
			get
			{
				return this.clothSolverFrequency > 0f;
			}
			set
			{
				this.clothSolverFrequency = ((!value) ? 0f : 120f);
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600003C RID: 60
		// (set) Token: 0x0600003D RID: 61
		public extern CapsuleCollider[] capsuleColliders { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600003E RID: 62
		// (set) Token: 0x0600003F RID: 63
		public extern ClothSphereColliderPair[] sphereColliders { [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] get; [GeneratedByOldBindingsGenerator] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000040 RID: 64
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetVirtualParticleIndicesMono(object indicesOutList);

		// Token: 0x06000041 RID: 65
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetVirtualParticleIndicesMono(object indicesInList);

		// Token: 0x06000042 RID: 66
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetVirtualParticleWeightsMono(object weightsOutList);

		// Token: 0x06000043 RID: 67
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetVirtualParticleWeightsMono(object weightsInList);

		// Token: 0x06000044 RID: 68
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetSelfAndInterCollisionIndicesMono(object indicesOutList);

		// Token: 0x06000045 RID: 69
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetSelfAndInterCollisionIndicesMono(object indicesInList);

		// Token: 0x06000046 RID: 70
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_externalAcceleration_Injected(out Vector3 ret);

		// Token: 0x06000047 RID: 71
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_externalAcceleration_Injected(ref Vector3 value);

		// Token: 0x06000048 RID: 72
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_randomAcceleration_Injected(out Vector3 ret);

		// Token: 0x06000049 RID: 73
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_randomAcceleration_Injected(ref Vector3 value);
	}
}
