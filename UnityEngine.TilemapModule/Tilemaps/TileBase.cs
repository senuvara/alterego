﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Tilemaps
{
	// Token: 0x0200000A RID: 10
	[RequiredByNativeCode]
	public abstract class TileBase : ScriptableObject
	{
		// Token: 0x06000033 RID: 51 RVA: 0x0000210F File Offset: 0x0000030F
		protected TileBase()
		{
		}

		// Token: 0x06000034 RID: 52 RVA: 0x0000254A File Offset: 0x0000074A
		[RequiredByNativeCode]
		public virtual void RefreshTile(Vector3Int position, ITilemap tilemap)
		{
			tilemap.RefreshTile(position);
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002117 File Offset: 0x00000317
		[RequiredByNativeCode]
		public virtual void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
		{
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002554 File Offset: 0x00000754
		private TileData GetTileDataNoRef(Vector3Int position, ITilemap tilemap)
		{
			TileData result = default(TileData);
			this.GetTileData(position, tilemap, ref result);
			return result;
		}

		// Token: 0x06000037 RID: 55 RVA: 0x0000257C File Offset: 0x0000077C
		[RequiredByNativeCode]
		public virtual bool GetTileAnimationData(Vector3Int position, ITilemap tilemap, ref TileAnimationData tileAnimationData)
		{
			return false;
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00002594 File Offset: 0x00000794
		private TileAnimationData GetTileAnimationDataNoRef(Vector3Int position, ITilemap tilemap)
		{
			TileAnimationData result = default(TileAnimationData);
			this.GetTileAnimationData(position, tilemap, ref result);
			return result;
		}

		// Token: 0x06000039 RID: 57 RVA: 0x000025C0 File Offset: 0x000007C0
		[RequiredByNativeCode]
		public virtual bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go)
		{
			return false;
		}
	}
}
