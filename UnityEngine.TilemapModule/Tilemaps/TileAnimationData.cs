﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Tilemaps
{
	// Token: 0x02000013 RID: 19
	[NativeType(Header = "Modules/Tilemap/TilemapScripting.h")]
	[RequiredByNativeCode]
	public struct TileAnimationData
	{
		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000B7 RID: 183 RVA: 0x00002AB8 File Offset: 0x00000CB8
		// (set) Token: 0x060000B8 RID: 184 RVA: 0x00002AD3 File Offset: 0x00000CD3
		public Sprite[] animatedSprites
		{
			get
			{
				return this.m_AnimatedSprites;
			}
			set
			{
				this.m_AnimatedSprites = value;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000B9 RID: 185 RVA: 0x00002AE0 File Offset: 0x00000CE0
		// (set) Token: 0x060000BA RID: 186 RVA: 0x00002AFB File Offset: 0x00000CFB
		public float animationSpeed
		{
			get
			{
				return this.m_AnimationSpeed;
			}
			set
			{
				this.m_AnimationSpeed = value;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000BB RID: 187 RVA: 0x00002B08 File Offset: 0x00000D08
		// (set) Token: 0x060000BC RID: 188 RVA: 0x00002B23 File Offset: 0x00000D23
		public float animationStartTime
		{
			get
			{
				return this.m_AnimationStartTime;
			}
			set
			{
				this.m_AnimationStartTime = value;
			}
		}

		// Token: 0x0400003E RID: 62
		private Sprite[] m_AnimatedSprites;

		// Token: 0x0400003F RID: 63
		private float m_AnimationSpeed;

		// Token: 0x04000040 RID: 64
		private float m_AnimationStartTime;
	}
}
