﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.Tilemaps
{
	// Token: 0x02000014 RID: 20
	[RequireComponent(typeof(Tilemap))]
	[NativeType(Header = "Modules/Tilemap/Public/TilemapCollider2D.h")]
	public sealed class TilemapCollider2D : Collider2D
	{
		// Token: 0x060000BD RID: 189 RVA: 0x00002B2D File Offset: 0x00000D2D
		public TilemapCollider2D()
		{
		}
	}
}
