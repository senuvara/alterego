﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Tilemaps
{
	// Token: 0x02000012 RID: 18
	[RequiredByNativeCode]
	[NativeType(Header = "Modules/Tilemap/TilemapScripting.h")]
	public struct TileData
	{
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000AB RID: 171 RVA: 0x000029C8 File Offset: 0x00000BC8
		// (set) Token: 0x060000AC RID: 172 RVA: 0x000029E3 File Offset: 0x00000BE3
		public Sprite sprite
		{
			get
			{
				return this.m_Sprite;
			}
			set
			{
				this.m_Sprite = value;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000AD RID: 173 RVA: 0x000029F0 File Offset: 0x00000BF0
		// (set) Token: 0x060000AE RID: 174 RVA: 0x00002A0B File Offset: 0x00000C0B
		public Color color
		{
			get
			{
				return this.m_Color;
			}
			set
			{
				this.m_Color = value;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000AF RID: 175 RVA: 0x00002A18 File Offset: 0x00000C18
		// (set) Token: 0x060000B0 RID: 176 RVA: 0x00002A33 File Offset: 0x00000C33
		public Matrix4x4 transform
		{
			get
			{
				return this.m_Transform;
			}
			set
			{
				this.m_Transform = value;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000B1 RID: 177 RVA: 0x00002A40 File Offset: 0x00000C40
		// (set) Token: 0x060000B2 RID: 178 RVA: 0x00002A5B File Offset: 0x00000C5B
		public GameObject gameObject
		{
			get
			{
				return this.m_GameObject;
			}
			set
			{
				this.m_GameObject = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000B3 RID: 179 RVA: 0x00002A68 File Offset: 0x00000C68
		// (set) Token: 0x060000B4 RID: 180 RVA: 0x00002A83 File Offset: 0x00000C83
		public TileFlags flags
		{
			get
			{
				return this.m_Flags;
			}
			set
			{
				this.m_Flags = value;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000B5 RID: 181 RVA: 0x00002A90 File Offset: 0x00000C90
		// (set) Token: 0x060000B6 RID: 182 RVA: 0x00002AAB File Offset: 0x00000CAB
		public Tile.ColliderType colliderType
		{
			get
			{
				return this.m_ColliderType;
			}
			set
			{
				this.m_ColliderType = value;
			}
		}

		// Token: 0x04000038 RID: 56
		private Sprite m_Sprite;

		// Token: 0x04000039 RID: 57
		private Color m_Color;

		// Token: 0x0400003A RID: 58
		private Matrix4x4 m_Transform;

		// Token: 0x0400003B RID: 59
		private GameObject m_GameObject;

		// Token: 0x0400003C RID: 60
		private TileFlags m_Flags;

		// Token: 0x0400003D RID: 61
		private Tile.ColliderType m_ColliderType;
	}
}
