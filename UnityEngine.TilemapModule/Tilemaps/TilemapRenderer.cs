﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Tilemaps
{
	// Token: 0x0200000E RID: 14
	[NativeHeader("Modules/Tilemap/TilemapRendererJobs.h")]
	[NativeHeader("Modules/Tilemap/Public/TilemapMarshalling.h")]
	[NativeType(Header = "Modules/Tilemap/Public/TilemapRenderer.h")]
	[NativeHeader("Modules/Grid/Public/GridMarshalling.h")]
	[RequireComponent(typeof(Tilemap))]
	public sealed class TilemapRenderer : Renderer
	{
		// Token: 0x06000096 RID: 150 RVA: 0x00002980 File Offset: 0x00000B80
		public TilemapRenderer()
		{
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000097 RID: 151 RVA: 0x00002988 File Offset: 0x00000B88
		// (set) Token: 0x06000098 RID: 152 RVA: 0x0000299E File Offset: 0x00000B9E
		public Vector3Int chunkSize
		{
			get
			{
				Vector3Int result;
				this.get_chunkSize_Injected(out result);
				return result;
			}
			set
			{
				this.set_chunkSize_Injected(ref value);
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000099 RID: 153 RVA: 0x000029A8 File Offset: 0x00000BA8
		// (set) Token: 0x0600009A RID: 154 RVA: 0x000029BE File Offset: 0x00000BBE
		public Vector3 chunkCullingBounds
		{
			[FreeFunction("TilemapRendererBindings::GetChunkCullingBounds", HasExplicitThis = true)]
			get
			{
				Vector3 result;
				this.get_chunkCullingBounds_Injected(out result);
				return result;
			}
			[FreeFunction("TilemapRendererBindings::SetChunkCullingBounds", HasExplicitThis = true)]
			set
			{
				this.set_chunkCullingBounds_Injected(ref value);
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600009B RID: 155
		// (set) Token: 0x0600009C RID: 156
		public extern int maxChunkCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600009D RID: 157
		// (set) Token: 0x0600009E RID: 158
		public extern int maxFrameAge { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600009F RID: 159
		// (set) Token: 0x060000A0 RID: 160
		public extern TilemapRenderer.SortOrder sortOrder { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000A1 RID: 161
		// (set) Token: 0x060000A2 RID: 162
		[NativeProperty("RenderMode")]
		public extern TilemapRenderer.Mode mode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000A3 RID: 163
		// (set) Token: 0x060000A4 RID: 164
		public extern TilemapRenderer.DetectChunkCullingBounds detectChunkCullingBounds { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000A5 RID: 165
		// (set) Token: 0x060000A6 RID: 166
		public extern SpriteMaskInteraction maskInteraction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060000A7 RID: 167
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_chunkSize_Injected(out Vector3Int ret);

		// Token: 0x060000A8 RID: 168
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_chunkSize_Injected(ref Vector3Int value);

		// Token: 0x060000A9 RID: 169
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_chunkCullingBounds_Injected(out Vector3 ret);

		// Token: 0x060000AA RID: 170
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_chunkCullingBounds_Injected(ref Vector3 value);

		// Token: 0x0200000F RID: 15
		public enum SortOrder
		{
			// Token: 0x0400002E RID: 46
			BottomLeft,
			// Token: 0x0400002F RID: 47
			BottomRight,
			// Token: 0x04000030 RID: 48
			TopLeft,
			// Token: 0x04000031 RID: 49
			TopRight
		}

		// Token: 0x02000010 RID: 16
		public enum Mode
		{
			// Token: 0x04000033 RID: 51
			Chunk,
			// Token: 0x04000034 RID: 52
			Individual
		}

		// Token: 0x02000011 RID: 17
		public enum DetectChunkCullingBounds
		{
			// Token: 0x04000036 RID: 54
			Auto,
			// Token: 0x04000037 RID: 55
			Manual
		}
	}
}
