﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Tilemaps
{
	// Token: 0x02000007 RID: 7
	[RequiredByNativeCode]
	public class ITilemap
	{
		// Token: 0x06000016 RID: 22 RVA: 0x0000221A File Offset: 0x0000041A
		internal ITilemap()
		{
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002223 File Offset: 0x00000423
		internal void SetTilemapInstance(Tilemap tilemap)
		{
			this.m_Tilemap = tilemap;
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000018 RID: 24 RVA: 0x00002230 File Offset: 0x00000430
		public Vector3Int origin
		{
			get
			{
				return this.m_Tilemap.origin;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000019 RID: 25 RVA: 0x00002250 File Offset: 0x00000450
		public Vector3Int size
		{
			get
			{
				return this.m_Tilemap.size;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600001A RID: 26 RVA: 0x00002270 File Offset: 0x00000470
		public Bounds localBounds
		{
			get
			{
				return this.m_Tilemap.localBounds;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600001B RID: 27 RVA: 0x00002290 File Offset: 0x00000490
		public BoundsInt cellBounds
		{
			get
			{
				return this.m_Tilemap.cellBounds;
			}
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000022B0 File Offset: 0x000004B0
		public virtual Sprite GetSprite(Vector3Int position)
		{
			return this.m_Tilemap.GetSprite(position);
		}

		// Token: 0x0600001D RID: 29 RVA: 0x000022D4 File Offset: 0x000004D4
		public virtual Color GetColor(Vector3Int position)
		{
			return this.m_Tilemap.GetColor(position);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000022F8 File Offset: 0x000004F8
		public virtual Matrix4x4 GetTransformMatrix(Vector3Int position)
		{
			return this.m_Tilemap.GetTransformMatrix(position);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x0000231C File Offset: 0x0000051C
		public virtual TileFlags GetTileFlags(Vector3Int position)
		{
			return this.m_Tilemap.GetTileFlags(position);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002340 File Offset: 0x00000540
		public virtual TileBase GetTile(Vector3Int position)
		{
			return this.m_Tilemap.GetTile(position);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002364 File Offset: 0x00000564
		public virtual T GetTile<T>(Vector3Int position) where T : TileBase
		{
			return this.m_Tilemap.GetTile<T>(position);
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002385 File Offset: 0x00000585
		public void RefreshTile(Vector3Int position)
		{
			this.m_Tilemap.RefreshTile(position);
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002394 File Offset: 0x00000594
		public T GetComponent<T>()
		{
			return this.m_Tilemap.GetComponent<T>();
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000023B4 File Offset: 0x000005B4
		[RequiredByNativeCode]
		private static ITilemap CreateInstance()
		{
			ITilemap.s_Instance = new ITilemap();
			return ITilemap.s_Instance;
		}

		// Token: 0x04000013 RID: 19
		internal static ITilemap s_Instance;

		// Token: 0x04000014 RID: 20
		internal Tilemap m_Tilemap;
	}
}
