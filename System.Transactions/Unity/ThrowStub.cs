﻿using System;

namespace Unity
{
	// Token: 0x02000032 RID: 50
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x060000ED RID: 237 RVA: 0x00003217 File Offset: 0x00001417
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
