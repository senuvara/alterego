﻿using System;

namespace System.Transactions
{
	/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Specifies whether transaction flow across thread continuations is enabled for <see cref="T:System.Transactions.TransactionScope" />.</summary>
	// Token: 0x0200002A RID: 42
	public enum TransactionScopeAsyncFlowOption
	{
		/// <summary>Specifies that transaction flow across thread continuations is suppressed.</summary>
		// Token: 0x0400006F RID: 111
		Suppress,
		/// <summary>Specifies that transaction flow across thread continuations is enabled.</summary>
		// Token: 0x04000070 RID: 112
		Enabled
	}
}
