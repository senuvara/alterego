﻿using System;
using System.Configuration;
using Unity;

namespace System.Transactions.Configuration
{
	/// <summary>Represents an XML section in a configuration file that contains default values of a transaction. This class cannot be inherited.</summary>
	// Token: 0x0200002F RID: 47
	public sealed class DefaultSettingsSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Transactions.Configuration.DefaultSettingsSection" /> class. </summary>
		// Token: 0x060000DF RID: 223 RVA: 0x000021BB File Offset: 0x000003BB
		public DefaultSettingsSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the name of the transaction manager.</summary>
		/// <returns>The name of the transaction manager. The default value is an empty string.</returns>
		/// <exception cref="T:System.NotSupportedException">An attempt to set this property to fully qualified domain names or IP addresses.</exception>
		/// <exception cref="T:System.Transactions.TransactionAbortedException">An attempt to set this property to localhost.</exception>
		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000E0 RID: 224 RVA: 0x0000319D File Offset: 0x0000139D
		// (set) Token: 0x060000E1 RID: 225 RVA: 0x000021BB File Offset: 0x000003BB
		public string DistributedTransactionManagerName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000E2 RID: 226 RVA: 0x0000319D File Offset: 0x0000139D
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a default time after which a transaction times out.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> object. The default property is 00:01:00. </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">An attempt to set this property to negative values.</exception>
		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000E3 RID: 227 RVA: 0x000031E0 File Offset: 0x000013E0
		// (set) Token: 0x060000E4 RID: 228 RVA: 0x000021BB File Offset: 0x000003BB
		public TimeSpan Timeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TimeSpan);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
