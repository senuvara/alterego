﻿using System;
using System.Configuration;
using Unity;

namespace System.Transactions.Configuration
{
	/// <summary>Represents a configuration section that encapsulates and allows traversal of all the transaction configuration XML elements and attributes that are within this configuration section. This class cannot be inherited.</summary>
	// Token: 0x02000031 RID: 49
	public sealed class TransactionsSectionGroup : ConfigurationSectionGroup
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Transactions.Configuration.TransactionsSectionGroup" /> class. </summary>
		// Token: 0x060000E9 RID: 233 RVA: 0x000021BB File Offset: 0x000003BB
		public TransactionsSectionGroup()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the default settings used to initialize the elements and attributes in a transactions section.</summary>
		/// <returns>A <see cref="T:System.Transactions.Configuration.DefaultSettingsSection" /> that represents the default settings. The default is a <see cref="T:System.Transactions.Configuration.DefaultSettingsSection" /> that is populated with default values.</returns>
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000EA RID: 234 RVA: 0x0000319D File Offset: 0x0000139D
		public DefaultSettingsSection DefaultSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the configuration settings set at the machine level.</summary>
		/// <returns>A <see cref="T:System.Transactions.Configuration.MachineSettingsSection" /> that represents the configuration settings at the machine level. The default is a <see cref="T:System.Transactions.Configuration.MachineSettingsSection" /> that is populated with default values.</returns>
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000EB RID: 235 RVA: 0x0000319D File Offset: 0x0000139D
		public MachineSettingsSection MachineSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Provides static access to a <see cref="T:System.Transactions.Configuration.TransactionsSectionGroup" />.</summary>
		/// <param name="config">A <see cref="T:System.Configuration.Configuration" /> representing the configuration settings that apply to a particular computer, application, or resource.</param>
		/// <returns>A <see cref="T:System.Transactions.Configuration.TransactionsSectionGroup" /> object.</returns>
		// Token: 0x060000EC RID: 236 RVA: 0x0000319D File Offset: 0x0000139D
		public static TransactionsSectionGroup GetSectionGroup(Configuration config)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
