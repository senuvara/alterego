﻿using System;
using System.Configuration;
using Unity;

namespace System.Transactions.Configuration
{
	/// <summary>Represents an XML section in a configuration file encapsulating all settings that can be modified only at the machine level. This class cannot be inherited.</summary>
	// Token: 0x02000030 RID: 48
	public sealed class MachineSettingsSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Transactions.Configuration.MachineSettingsSection" /> class. </summary>
		// Token: 0x060000E5 RID: 229 RVA: 0x000021BB File Offset: 0x000003BB
		public MachineSettingsSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a maximum amount of time allowed before a transaction times out.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> object that contains the maximum allowable time. The default value is 00:10:00.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">An attempt to set this property to negative values.</exception>
		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000E6 RID: 230 RVA: 0x000031FC File Offset: 0x000013FC
		// (set) Token: 0x060000E7 RID: 231 RVA: 0x000021BB File Offset: 0x000003BB
		public TimeSpan MaxTimeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TimeSpan);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000E8 RID: 232 RVA: 0x0000319D File Offset: 0x0000139D
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
