﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Transactions
{
	/// <summary>Contains methods used for transaction management. This class cannot be inherited.</summary>
	// Token: 0x02000025 RID: 37
	public static class TransactionManager
	{
		// Token: 0x060000A7 RID: 167 RVA: 0x00002C59 File Offset: 0x00000E59
		static TransactionManager()
		{
		}

		/// <summary>Gets the default timeout interval for new transactions.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> value that specifies the timeout interval for new transactions.</returns>
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000A8 RID: 168 RVA: 0x00002C76 File Offset: 0x00000E76
		public static TimeSpan DefaultTimeout
		{
			get
			{
				return TransactionManager.defaultTimeout;
			}
		}

		/// <summary>Gets or sets a custom transaction factory.</summary>
		/// <returns>A <see cref="T:System.Transactions.HostCurrentTransactionCallback" /> that contains a custom transaction factory.</returns>
		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000A9 RID: 169 RVA: 0x00002162 File Offset: 0x00000362
		// (set) Token: 0x060000AA RID: 170 RVA: 0x00002162 File Offset: 0x00000362
		[MonoTODO("Not implemented")]
		public static HostCurrentTransactionCallback HostCurrentCallback
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the default maximum timeout interval for new transactions.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> value that specifies the maximum timeout interval that is allowed when creating new transactions.</returns>
		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000AB RID: 171 RVA: 0x00002C7D File Offset: 0x00000E7D
		public static TimeSpan MaximumTimeout
		{
			get
			{
				return TransactionManager.maxTimeout;
			}
		}

		/// <summary>Notifies the transaction manager that a resource manager recovering from failure has finished reenlisting in all unresolved transactions.</summary>
		/// <param name="resourceManagerIdentifier">A <see cref="T:System.Guid" /> that uniquely identifies the resource to be recovered from.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="resourceManagerIdentifier" /> parameter is <see langword="null" />.</exception>
		// Token: 0x060000AC RID: 172 RVA: 0x00002162 File Offset: 0x00000362
		[MonoTODO("Not implemented")]
		public static void RecoveryComplete(Guid resourceManagerIdentifier)
		{
			throw new NotImplementedException();
		}

		/// <summary>Reenlists a durable participant in a transaction.</summary>
		/// <param name="resourceManagerIdentifier">A <see cref="T:System.Guid" /> that uniquely identifies the resource manager.</param>
		/// <param name="recoveryInformation">Contains additional information of recovery information.</param>
		/// <param name="enlistmentNotification">A resource object that implements <see cref="T:System.Transactions.IEnlistmentNotification" /> to receive notifications.</param>
		/// <returns>An <see cref="T:System.Transactions.Enlistment" /> that describes the enlistment.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="recoveryInformation" /> is invalid.-or-Transaction Manager information in <paramref name="recoveryInformation" /> does not match the configured transaction manager.-or-
		///         <paramref name="RecoveryInformation" /> is not recognized by <see cref="N:System.Transactions" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Transactions.TransactionManager.RecoveryComplete(System.Guid)" /> has already been called for the specified <paramref name="resourceManagerIdentifier" />. The reenlistment is rejected.</exception>
		/// <exception cref="T:System.Transactions.TransactionException">The <paramref name="resourceManagerIdentifier" /> does not match the content of the specified recovery information in <paramref name="recoveryInformation" />. </exception>
		// Token: 0x060000AD RID: 173 RVA: 0x00002162 File Offset: 0x00000362
		[MonoTODO("Not implemented")]
		public static Enlistment Reenlist(Guid resourceManagerIdentifier, byte[] recoveryInformation, IEnlistmentNotification enlistmentNotification)
		{
			throw new NotImplementedException();
		}

		/// <summary>Indicates that a distributed transaction has started.</summary>
		// Token: 0x14000002 RID: 2
		// (add) Token: 0x060000AE RID: 174 RVA: 0x00002C84 File Offset: 0x00000E84
		// (remove) Token: 0x060000AF RID: 175 RVA: 0x00002CB8 File Offset: 0x00000EB8
		public static event TransactionStartedEventHandler DistributedTransactionStarted
		{
			[CompilerGenerated]
			add
			{
				TransactionStartedEventHandler transactionStartedEventHandler = TransactionManager.DistributedTransactionStarted;
				TransactionStartedEventHandler transactionStartedEventHandler2;
				do
				{
					transactionStartedEventHandler2 = transactionStartedEventHandler;
					TransactionStartedEventHandler value2 = (TransactionStartedEventHandler)Delegate.Combine(transactionStartedEventHandler2, value);
					transactionStartedEventHandler = Interlocked.CompareExchange<TransactionStartedEventHandler>(ref TransactionManager.DistributedTransactionStarted, value2, transactionStartedEventHandler2);
				}
				while (transactionStartedEventHandler != transactionStartedEventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				TransactionStartedEventHandler transactionStartedEventHandler = TransactionManager.DistributedTransactionStarted;
				TransactionStartedEventHandler transactionStartedEventHandler2;
				do
				{
					transactionStartedEventHandler2 = transactionStartedEventHandler;
					TransactionStartedEventHandler value2 = (TransactionStartedEventHandler)Delegate.Remove(transactionStartedEventHandler2, value);
					transactionStartedEventHandler = Interlocked.CompareExchange<TransactionStartedEventHandler>(ref TransactionManager.DistributedTransactionStarted, value2, transactionStartedEventHandler2);
				}
				while (transactionStartedEventHandler != transactionStartedEventHandler2);
			}
		}

		// Token: 0x0400005F RID: 95
		private static TimeSpan defaultTimeout = new TimeSpan(0, 1, 0);

		// Token: 0x04000060 RID: 96
		private static TimeSpan maxTimeout = new TimeSpan(0, 10, 0);

		// Token: 0x04000061 RID: 97
		[CompilerGenerated]
		private static TransactionStartedEventHandler DistributedTransactionStarted;
	}
}
