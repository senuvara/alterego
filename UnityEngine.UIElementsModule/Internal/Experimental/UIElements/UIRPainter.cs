﻿using System;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UIR;

namespace UnityEngine.Internal.Experimental.UIElements
{
	// Token: 0x02000068 RID: 104
	internal class UIRPainter : IStylePainterInternal, IStylePainter
	{
		// Token: 0x060002EF RID: 751 RVA: 0x0000A2D4 File Offset: 0x000084D4
		public UIRPainter()
		{
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x0000A388 File Offset: 0x00008588
		public void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.m_VertexGPUBuffer.Dispose();
				this.m_IndexGPUBuffer.Dispose();
				this.m_VertexData.Dispose();
				this.m_IndexData.Dispose();
				this.m_VertexUpdateRanges.Dispose();
				this.m_IndexUpdateRanges.Dispose();
				this.m_DrawRanges.Dispose();
			}
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x0000A3EB File Offset: 0x000085EB
		public void Draw()
		{
			Utility.DrawRanges<ushort, UIRPainter.Vertex>(this.m_IndexGPUBuffer, this.m_VertexGPUBuffer, this.m_DrawRanges.Slice(0, this.m_DrawRangeCount));
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x060002F2 RID: 754 RVA: 0x0000A414 File Offset: 0x00008614
		// (set) Token: 0x060002F3 RID: 755 RVA: 0x000035A6 File Offset: 0x000017A6
		public float opacity
		{
			get
			{
				return 1f;
			}
			set
			{
			}
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x0000A430 File Offset: 0x00008630
		public void DrawRect(RectStylePainterParameters painterParams)
		{
			Rect rect = painterParams.rect;
			Color color = painterParams.color;
			Matrix4x4 worldTransform = this.currentElement.worldTransform;
			this.m_VertexData[this.m_VertexOffset] = new UIRPainter.Vertex
			{
				Position = worldTransform.MultiplyPoint(new Vector2(rect.x, rect.y)),
				Tint = color,
				UV = Vector2.zero,
				TransformID = 0f,
				Flags = 0f
			};
			this.m_VertexData[this.m_VertexOffset + 1] = new UIRPainter.Vertex
			{
				Position = worldTransform.MultiplyPoint(new Vector2(rect.x + rect.width, rect.y)),
				Tint = color,
				UV = Vector2.zero,
				TransformID = 0f,
				Flags = 0f
			};
			this.m_VertexData[this.m_VertexOffset + 2] = new UIRPainter.Vertex
			{
				Position = worldTransform.MultiplyPoint(new Vector2(rect.x, rect.y + rect.height)),
				Tint = color,
				UV = Vector2.zero,
				TransformID = 0f,
				Flags = 0f
			};
			this.m_VertexData[this.m_VertexOffset + 3] = new UIRPainter.Vertex
			{
				Position = worldTransform.MultiplyPoint(new Vector2(rect.x + rect.width, rect.y + rect.height)),
				Tint = color,
				UV = Vector2.zero,
				TransformID = 0f,
				Flags = 0f
			};
			int elementStride = this.m_VertexGPUBuffer.ElementStride;
			this.m_VertexUpdateRanges[this.m_VertexUpdateOffset] = new GfxUpdateBufferRange
			{
				source = new UIntPtr(this.m_VertexData.Slice(this.m_VertexOffset, 4).GetUnsafeReadOnlyPtr<UIRPainter.Vertex>()),
				offsetFromWriteStart = 0U,
				size = (uint)(4 * elementStride)
			};
			this.m_VertexGPUBuffer.UpdateRanges(this.m_VertexUpdateRanges.Slice(this.m_VertexUpdateOffset, 1), this.m_VertexOffset * elementStride, (this.m_VertexOffset + 4) * elementStride);
			this.m_VertexUpdateOffset++;
			this.m_IndexData[this.m_IndexOffset] = (ushort)this.m_VertexOffset;
			this.m_IndexData[this.m_IndexOffset + 1] = (ushort)(this.m_VertexOffset + 1);
			this.m_IndexData[this.m_IndexOffset + 2] = (ushort)(this.m_VertexOffset + 2);
			this.m_IndexData[this.m_IndexOffset + 3] = (ushort)(this.m_VertexOffset + 2);
			this.m_IndexData[this.m_IndexOffset + 4] = (ushort)(this.m_VertexOffset + 1);
			this.m_IndexData[this.m_IndexOffset + 5] = (ushort)(this.m_VertexOffset + 3);
			int elementStride2 = this.m_IndexGPUBuffer.ElementStride;
			this.m_IndexUpdateRanges[this.m_IndexUpdateOffset] = new GfxUpdateBufferRange
			{
				source = new UIntPtr(this.m_IndexData.Slice(this.m_IndexOffset, 6).GetUnsafeReadOnlyPtr<ushort>()),
				offsetFromWriteStart = 0U,
				size = (uint)(6 * elementStride2)
			};
			this.m_IndexGPUBuffer.UpdateRanges(this.m_IndexUpdateRanges.Slice(this.m_IndexUpdateOffset, 1), this.m_IndexOffset * elementStride2, (this.m_IndexOffset + 6) * elementStride2);
			this.m_IndexUpdateOffset++;
			DrawBufferRange value = this.m_DrawRanges[this.m_DrawRangeCount];
			value.firstIndex = this.m_IndexOffset;
			value.indexCount = 6;
			value.minIndexVal = this.m_VertexOffset;
			value.vertsReferenced = 4;
			this.m_DrawRanges[this.m_DrawRangeCount++] = value;
			this.m_VertexOffset += 4;
			this.m_IndexOffset += 6;
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x000035A6 File Offset: 0x000017A6
		public void DrawMesh(MeshStylePainterParameters painterParameters)
		{
		}

		// Token: 0x060002F6 RID: 758 RVA: 0x000035A6 File Offset: 0x000017A6
		public void DrawText(TextStylePainterParameters painterParams)
		{
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x000035A6 File Offset: 0x000017A6
		public void DrawTexture(TextureStylePainterParameters painterParams)
		{
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x000035A6 File Offset: 0x000017A6
		public void DrawImmediate(Action callback)
		{
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x0000A8A0 File Offset: 0x00008AA0
		public void DrawBackground()
		{
			IStyle style = this.currentElement.style;
			if (style.backgroundColor != Color.clear)
			{
				RectStylePainterParameters @default = RectStylePainterParameters.GetDefault(this.currentElement);
				@default.border.SetWidth(0f);
				this.DrawRect(@default);
			}
		}

		// Token: 0x060002FA RID: 762 RVA: 0x000035A6 File Offset: 0x000017A6
		public void DrawBorder()
		{
		}

		// Token: 0x060002FB RID: 763 RVA: 0x000035A6 File Offset: 0x000017A6
		public void DrawText(string text)
		{
		}

		// Token: 0x04000115 RID: 277
		private const int kMaxVertices = 1024;

		// Token: 0x04000116 RID: 278
		private const int kMaxIndices = 4096;

		// Token: 0x04000117 RID: 279
		private const int kMaxRanges = 1024;

		// Token: 0x04000118 RID: 280
		private Utility.GPUBuffer<UIRPainter.Vertex> m_VertexGPUBuffer = new Utility.GPUBuffer<UIRPainter.Vertex>(1024, Utility.GPUBufferType.Vertex);

		// Token: 0x04000119 RID: 281
		private Utility.GPUBuffer<ushort> m_IndexGPUBuffer = new Utility.GPUBuffer<ushort>(4096, Utility.GPUBufferType.Index);

		// Token: 0x0400011A RID: 282
		private NativeArray<UIRPainter.Vertex> m_VertexData = new NativeArray<UIRPainter.Vertex>(1024, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);

		// Token: 0x0400011B RID: 283
		private NativeArray<ushort> m_IndexData = new NativeArray<ushort>(4096, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);

		// Token: 0x0400011C RID: 284
		private int m_VertexOffset = 0;

		// Token: 0x0400011D RID: 285
		private int m_IndexOffset = 0;

		// Token: 0x0400011E RID: 286
		private NativeArray<GfxUpdateBufferRange> m_VertexUpdateRanges = new NativeArray<GfxUpdateBufferRange>(1024, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);

		// Token: 0x0400011F RID: 287
		private NativeArray<GfxUpdateBufferRange> m_IndexUpdateRanges = new NativeArray<GfxUpdateBufferRange>(1024, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);

		// Token: 0x04000120 RID: 288
		private int m_VertexUpdateOffset = 0;

		// Token: 0x04000121 RID: 289
		private int m_IndexUpdateOffset = 0;

		// Token: 0x04000122 RID: 290
		private NativeArray<DrawBufferRange> m_DrawRanges = new NativeArray<DrawBufferRange>(1024, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);

		// Token: 0x04000123 RID: 291
		private int m_DrawRangeCount = 0;

		// Token: 0x04000124 RID: 292
		internal VisualElement currentElement;

		// Token: 0x02000069 RID: 105
		private struct Vertex
		{
			// Token: 0x04000125 RID: 293
			public Vector3 Position;

			// Token: 0x04000126 RID: 294
			public Color32 Tint;

			// Token: 0x04000127 RID: 295
			public Vector2 UV;

			// Token: 0x04000128 RID: 296
			public float TransformID;

			// Token: 0x04000129 RID: 297
			public float Flags;
		}
	}
}
