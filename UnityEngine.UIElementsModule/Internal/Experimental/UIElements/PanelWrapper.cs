﻿using System;
using UnityEngine.Experimental.UIElements;

namespace UnityEngine.Internal.Experimental.UIElements
{
	// Token: 0x02000066 RID: 102
	public class PanelWrapper : ScriptableObject
	{
		// Token: 0x060002E4 RID: 740 RVA: 0x0000A105 File Offset: 0x00008305
		public PanelWrapper()
		{
		}

		// Token: 0x060002E5 RID: 741 RVA: 0x0000A10D File Offset: 0x0000830D
		private void OnEnable()
		{
			this.m_Panel = UIElementsUtility.FindOrCreatePanel(this);
		}

		// Token: 0x060002E6 RID: 742 RVA: 0x0000A11C File Offset: 0x0000831C
		private void OnDisable()
		{
			if (this.m_Updater != null)
			{
				this.m_Updater.Dispose();
			}
			this.m_Panel = null;
		}

		// Token: 0x170000D5 RID: 213
		// (set) Token: 0x060002E7 RID: 743 RVA: 0x0000A13C File Offset: 0x0000833C
		public bool UIREnabled
		{
			set
			{
				if (this.m_Updater != null)
				{
					this.m_Updater.Dispose();
				}
				if (value)
				{
					this.m_Updater = new UIRRepaintUpdater();
				}
				else
				{
					this.m_Updater = new VisualTreeRepaintUpdater();
				}
				this.m_Panel.SetUpdater(this.m_Updater, VisualTreeUpdatePhase.Repaint);
			}
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x060002E8 RID: 744 RVA: 0x0000A194 File Offset: 0x00008394
		public VisualElement visualTree
		{
			get
			{
				return this.m_Panel.visualTree;
			}
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x0000A1B4 File Offset: 0x000083B4
		public void Repaint(Event e)
		{
			this.m_Panel.Repaint(e);
		}

		// Token: 0x04000111 RID: 273
		private Panel m_Panel;

		// Token: 0x04000112 RID: 274
		private BaseVisualTreeUpdater m_Updater;
	}
}
