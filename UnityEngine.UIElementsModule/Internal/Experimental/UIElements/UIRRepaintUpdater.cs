﻿using System;
using System.Collections.Generic;
using UnityEngine.Experimental.UIElements;

namespace UnityEngine.Internal.Experimental.UIElements
{
	// Token: 0x02000067 RID: 103
	internal class UIRRepaintUpdater : BaseVisualTreeUpdater
	{
		// Token: 0x060002EA RID: 746 RVA: 0x0000A1C3 File Offset: 0x000083C3
		public UIRRepaintUpdater()
		{
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x060002EB RID: 747 RVA: 0x0000A1E4 File Offset: 0x000083E4
		public override string description
		{
			get
			{
				return "UIRRepaintUpdater";
			}
		}

		// Token: 0x060002EC RID: 748 RVA: 0x0000A1FE File Offset: 0x000083FE
		protected override void Dispose(bool disposing)
		{
			this.m_Painter.Dispose(disposing);
		}

		// Token: 0x060002ED RID: 749 RVA: 0x0000A20D File Offset: 0x0000840D
		public override void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType)
		{
			if ((versionChangeType & VersionChangeType.Repaint) == VersionChangeType.Repaint)
			{
				if (!this.m_Elements.Contains(ve))
				{
					this.m_Elements.Add(ve);
				}
			}
		}

		// Token: 0x060002EE RID: 750 RVA: 0x0000A238 File Offset: 0x00008438
		public override void Update()
		{
			if (this.m_Elements.Count > 0)
			{
				foreach (VisualElement visualElement in this.m_Elements)
				{
					this.m_Painter.currentElement = visualElement;
					visualElement.Repaint(this.m_Painter);
				}
				this.m_Elements.Clear();
			}
			this.m_Painter.Draw();
		}

		// Token: 0x04000113 RID: 275
		private UIRPainter m_Painter = new UIRPainter();

		// Token: 0x04000114 RID: 276
		private List<VisualElement> m_Elements = new List<VisualElement>();
	}
}
