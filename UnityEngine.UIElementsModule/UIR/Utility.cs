﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Bindings;

namespace UnityEngine.UIR
{
	// Token: 0x020000EA RID: 234
	[NativeHeader("Modules/UIElements/UIRendererUtility.h")]
	internal class Utility
	{
		// Token: 0x0600069D RID: 1693 RVA: 0x00002223 File Offset: 0x00000423
		public Utility()
		{
		}

		// Token: 0x0600069E RID: 1694 RVA: 0x000171AC File Offset: 0x000153AC
		public static void DrawRanges<I, T>(Utility.GPUBuffer<I> ib, Utility.GPUBuffer<T> vb, NativeSlice<DrawBufferRange> ranges) where I : struct where T : struct
		{
			Utility.DrawRanges(ib.BufferPointer, vb.BufferPointer, vb.ElementStride, new IntPtr(ranges.GetUnsafePtr<DrawBufferRange>()), ranges.Length);
		}

		// Token: 0x0600069F RID: 1695
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr AllocateBuffer(int elementCount, int elementStride, bool vertexBuffer);

		// Token: 0x060006A0 RID: 1696
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void FreeBuffer(IntPtr buffer);

		// Token: 0x060006A1 RID: 1697
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void UpdateBufferRanges(IntPtr buffer, IntPtr ranges, int rangeCount, int writeRangeStart, int writeRangeEnd);

		// Token: 0x060006A2 RID: 1698
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DrawRanges(IntPtr ib, IntPtr vb, int vbElemStride, IntPtr ranges, int rangeCount);

		// Token: 0x060006A3 RID: 1699 RVA: 0x000171D8 File Offset: 0x000153D8
		public static void SetScissorRect(RectInt scissorRect)
		{
			Utility.SetScissorRect_Injected(ref scissorRect);
		}

		// Token: 0x060006A4 RID: 1700
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void DisableScissor();

		// Token: 0x060006A5 RID: 1701
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern uint InsertCPUFence();

		// Token: 0x060006A6 RID: 1702
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool CPUFencePassed(uint fence);

		// Token: 0x060006A7 RID: 1703
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SyncRenderThread();

		// Token: 0x060006A8 RID: 1704
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ProfileDrawChainBegin();

		// Token: 0x060006A9 RID: 1705
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ProfileDrawChainEnd();

		// Token: 0x060006AA RID: 1706
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetScissorRect_Injected(ref RectInt scissorRect);

		// Token: 0x020000EB RID: 235
		internal enum GPUBufferType
		{
			// Token: 0x040002A9 RID: 681
			Vertex,
			// Token: 0x040002AA RID: 682
			Index
		}

		// Token: 0x020000EC RID: 236
		public class GPUBuffer<T> : IDisposable where T : struct
		{
			// Token: 0x060006AB RID: 1707 RVA: 0x000171E1 File Offset: 0x000153E1
			public GPUBuffer(int elementCount, Utility.GPUBufferType type)
			{
				this.elemCount = elementCount;
				this.elemStride = UnsafeUtility.SizeOf<T>();
				this.buffer = Utility.AllocateBuffer(elementCount, this.elemStride, type == Utility.GPUBufferType.Vertex);
			}

			// Token: 0x060006AC RID: 1708 RVA: 0x00017212 File Offset: 0x00015412
			public void Dispose()
			{
				Utility.FreeBuffer(this.buffer);
			}

			// Token: 0x060006AD RID: 1709 RVA: 0x00017220 File Offset: 0x00015420
			public void UpdateRanges(NativeSlice<GfxUpdateBufferRange> ranges, int rangesMin, int rangesMax)
			{
				Utility.UpdateBufferRanges(this.buffer, new IntPtr(ranges.GetUnsafePtr<GfxUpdateBufferRange>()), ranges.Length, rangesMin, rangesMax);
			}

			// Token: 0x170001BA RID: 442
			// (get) Token: 0x060006AE RID: 1710 RVA: 0x00017244 File Offset: 0x00015444
			public int ElementStride
			{
				get
				{
					return this.elemStride;
				}
			}

			// Token: 0x170001BB RID: 443
			// (get) Token: 0x060006AF RID: 1711 RVA: 0x00017260 File Offset: 0x00015460
			public int Count
			{
				get
				{
					return this.elemCount;
				}
			}

			// Token: 0x170001BC RID: 444
			// (get) Token: 0x060006B0 RID: 1712 RVA: 0x0001727C File Offset: 0x0001547C
			internal IntPtr BufferPointer
			{
				get
				{
					return this.buffer;
				}
			}

			// Token: 0x040002AB RID: 683
			private IntPtr buffer;

			// Token: 0x040002AC RID: 684
			private int elemCount;

			// Token: 0x040002AD RID: 685
			private int elemStride;
		}
	}
}
