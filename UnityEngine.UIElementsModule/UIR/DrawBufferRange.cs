﻿using System;

namespace UnityEngine.UIR
{
	// Token: 0x020000E9 RID: 233
	internal struct DrawBufferRange
	{
		// Token: 0x040002A4 RID: 676
		public int firstIndex;

		// Token: 0x040002A5 RID: 677
		public int indexCount;

		// Token: 0x040002A6 RID: 678
		public int minIndexVal;

		// Token: 0x040002A7 RID: 679
		public int vertsReferenced;
	}
}
