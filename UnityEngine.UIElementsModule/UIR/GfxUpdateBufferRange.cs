﻿using System;

namespace UnityEngine.UIR
{
	// Token: 0x020000E8 RID: 232
	internal struct GfxUpdateBufferRange
	{
		// Token: 0x040002A1 RID: 673
		public uint offsetFromWriteStart;

		// Token: 0x040002A2 RID: 674
		public uint size;

		// Token: 0x040002A3 RID: 675
		public UIntPtr source;
	}
}
