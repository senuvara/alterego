﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x0200019A RID: 410
	[Flags]
	internal enum YogaPrintOptions
	{
		// Token: 0x040004B8 RID: 1208
		Layout = 1,
		// Token: 0x040004B9 RID: 1209
		Style = 2,
		// Token: 0x040004BA RID: 1210
		Children = 4
	}
}
