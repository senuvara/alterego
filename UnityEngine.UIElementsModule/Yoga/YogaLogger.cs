﻿using System;
using System.Runtime.InteropServices;

namespace UnityEngine.Yoga
{
	// Token: 0x02000191 RID: 401
	// (Invoke) Token: 0x060009FA RID: 2554
	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	internal delegate void YogaLogger(IntPtr unmanagedConfigPtr, IntPtr unmanagedNotePtr, YogaLogLevel level, string message);
}
