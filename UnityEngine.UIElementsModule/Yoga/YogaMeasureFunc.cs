﻿using System;
using System.Runtime.InteropServices;

namespace UnityEngine.Yoga
{
	// Token: 0x02000192 RID: 402
	// (Invoke) Token: 0x060009FE RID: 2558
	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	internal delegate YogaSize YogaMeasureFunc(IntPtr unmanagedNodePtr, float width, YogaMeasureMode widthMode, float height, YogaMeasureMode heightMode);
}
