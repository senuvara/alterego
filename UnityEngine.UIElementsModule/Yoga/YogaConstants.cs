﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000188 RID: 392
	internal static class YogaConstants
	{
		// Token: 0x060009F7 RID: 2551 RVA: 0x000223F8 File Offset: 0x000205F8
		public static bool IsUndefined(float value)
		{
			return float.IsNaN(value);
		}

		// Token: 0x060009F8 RID: 2552 RVA: 0x00022414 File Offset: 0x00020614
		public static bool IsUndefined(YogaValue value)
		{
			return value.Unit == YogaUnit.Undefined;
		}

		// Token: 0x04000478 RID: 1144
		public const float Undefined = float.NaN;
	}
}
