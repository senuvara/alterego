﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000189 RID: 393
	internal enum YogaDimension
	{
		// Token: 0x0400047A RID: 1146
		Width,
		// Token: 0x0400047B RID: 1147
		Height
	}
}
