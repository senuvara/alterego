﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Yoga
{
	// Token: 0x02000194 RID: 404
	[NativeHeader("Modules/UIElements/YogaNative.bindings.h")]
	internal static class Native
	{
		// Token: 0x06000A01 RID: 2561
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr YGNodeNew();

		// Token: 0x06000A02 RID: 2562
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr YGNodeNewWithConfig(IntPtr config);

		// Token: 0x06000A03 RID: 2563 RVA: 0x00022433 File Offset: 0x00020633
		public static void YGNodeFree(IntPtr ygNode)
		{
			if (!(ygNode == IntPtr.Zero))
			{
				Native.YGNodeFreeInternal(ygNode);
			}
		}

		// Token: 0x06000A04 RID: 2564
		[FreeFunction(Name = "YGNodeFree", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeFreeInternal(IntPtr ygNode);

		// Token: 0x06000A05 RID: 2565
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeReset(IntPtr node);

		// Token: 0x06000A06 RID: 2566
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGSetManagedObject(IntPtr ygNode, YogaNode node);

		// Token: 0x06000A07 RID: 2567
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr YGConfigGetDefault();

		// Token: 0x06000A08 RID: 2568
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr YGConfigNew();

		// Token: 0x06000A09 RID: 2569 RVA: 0x00022451 File Offset: 0x00020651
		public static void YGConfigFree(IntPtr config)
		{
			if (!(config == IntPtr.Zero))
			{
				Native.YGConfigFreeInternal(config);
			}
		}

		// Token: 0x06000A0A RID: 2570
		[FreeFunction(Name = "YGConfigFree", IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGConfigFreeInternal(IntPtr config);

		// Token: 0x06000A0B RID: 2571
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int YGNodeGetInstanceCount();

		// Token: 0x06000A0C RID: 2572
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int YGConfigGetInstanceCount();

		// Token: 0x06000A0D RID: 2573
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGConfigSetExperimentalFeatureEnabled(IntPtr config, YogaExperimentalFeature feature, bool enabled);

		// Token: 0x06000A0E RID: 2574
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool YGConfigIsExperimentalFeatureEnabled(IntPtr config, YogaExperimentalFeature feature);

		// Token: 0x06000A0F RID: 2575
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGConfigSetUseWebDefaults(IntPtr config, bool useWebDefaults);

		// Token: 0x06000A10 RID: 2576
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool YGConfigGetUseWebDefaults(IntPtr config);

		// Token: 0x06000A11 RID: 2577
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGConfigSetPointScaleFactor(IntPtr config, float pixelsInPoint);

		// Token: 0x06000A12 RID: 2578
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeInsertChild(IntPtr node, IntPtr child, uint index);

		// Token: 0x06000A13 RID: 2579
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeRemoveChild(IntPtr node, IntPtr child);

		// Token: 0x06000A14 RID: 2580
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeCalculateLayout(IntPtr node, float availableWidth, float availableHeight, YogaDirection parentDirection);

		// Token: 0x06000A15 RID: 2581
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeMarkDirty(IntPtr node);

		// Token: 0x06000A16 RID: 2582
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool YGNodeIsDirty(IntPtr node);

		// Token: 0x06000A17 RID: 2583
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodePrint(IntPtr node, YogaPrintOptions options);

		// Token: 0x06000A18 RID: 2584
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeCopyStyle(IntPtr dstNode, IntPtr srcNode);

		// Token: 0x06000A19 RID: 2585
		[FreeFunction(Name = "YogaCallback::SetMeasureFunc")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeSetMeasureFunc(IntPtr node);

		// Token: 0x06000A1A RID: 2586
		[FreeFunction(Name = "YogaCallback::RemoveMeasureFunc")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeRemoveMeasureFunc(IntPtr node);

		// Token: 0x06000A1B RID: 2587 RVA: 0x0002246F File Offset: 0x0002066F
		[RequiredByNativeCode]
		public unsafe static void YGNodeMeasureInvoke(YogaNode node, float width, YogaMeasureMode widthMode, float height, YogaMeasureMode heightMode, IntPtr returnValueAddress)
		{
			*(YogaSize*)((void*)returnValueAddress) = YogaNode.MeasureInternal(node, width, widthMode, height, heightMode);
		}

		// Token: 0x06000A1C RID: 2588
		[FreeFunction(Name = "YogaCallback::SetBaselineFunc")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeSetBaselineFunc(IntPtr node);

		// Token: 0x06000A1D RID: 2589
		[FreeFunction(Name = "YogaCallback::RemoveBaselineFunc")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeRemoveBaselineFunc(IntPtr node);

		// Token: 0x06000A1E RID: 2590 RVA: 0x00022489 File Offset: 0x00020689
		[RequiredByNativeCode]
		public unsafe static void YGNodeBaselineInvoke(YogaNode node, float width, float height, IntPtr returnValueAddress)
		{
			*(float*)((void*)returnValueAddress) = YogaNode.BaselineInternal(node, width, height);
		}

		// Token: 0x06000A1F RID: 2591
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeSetHasNewLayout(IntPtr node, bool hasNewLayout);

		// Token: 0x06000A20 RID: 2592
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool YGNodeGetHasNewLayout(IntPtr node);

		// Token: 0x06000A21 RID: 2593
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetDirection(IntPtr node, YogaDirection direction);

		// Token: 0x06000A22 RID: 2594
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaDirection YGNodeStyleGetDirection(IntPtr node);

		// Token: 0x06000A23 RID: 2595
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetFlexDirection(IntPtr node, YogaFlexDirection flexDirection);

		// Token: 0x06000A24 RID: 2596
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaFlexDirection YGNodeStyleGetFlexDirection(IntPtr node);

		// Token: 0x06000A25 RID: 2597
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetJustifyContent(IntPtr node, YogaJustify justifyContent);

		// Token: 0x06000A26 RID: 2598
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaJustify YGNodeStyleGetJustifyContent(IntPtr node);

		// Token: 0x06000A27 RID: 2599
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetAlignContent(IntPtr node, YogaAlign alignContent);

		// Token: 0x06000A28 RID: 2600
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaAlign YGNodeStyleGetAlignContent(IntPtr node);

		// Token: 0x06000A29 RID: 2601
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetAlignItems(IntPtr node, YogaAlign alignItems);

		// Token: 0x06000A2A RID: 2602
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaAlign YGNodeStyleGetAlignItems(IntPtr node);

		// Token: 0x06000A2B RID: 2603
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetAlignSelf(IntPtr node, YogaAlign alignSelf);

		// Token: 0x06000A2C RID: 2604
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaAlign YGNodeStyleGetAlignSelf(IntPtr node);

		// Token: 0x06000A2D RID: 2605
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetPositionType(IntPtr node, YogaPositionType positionType);

		// Token: 0x06000A2E RID: 2606
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaPositionType YGNodeStyleGetPositionType(IntPtr node);

		// Token: 0x06000A2F RID: 2607
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetFlexWrap(IntPtr node, YogaWrap flexWrap);

		// Token: 0x06000A30 RID: 2608
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaWrap YGNodeStyleGetFlexWrap(IntPtr node);

		// Token: 0x06000A31 RID: 2609
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetOverflow(IntPtr node, YogaOverflow flexWrap);

		// Token: 0x06000A32 RID: 2610
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaOverflow YGNodeStyleGetOverflow(IntPtr node);

		// Token: 0x06000A33 RID: 2611
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetDisplay(IntPtr node, YogaDisplay display);

		// Token: 0x06000A34 RID: 2612
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaDisplay YGNodeStyleGetDisplay(IntPtr node);

		// Token: 0x06000A35 RID: 2613
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetFlex(IntPtr node, float flex);

		// Token: 0x06000A36 RID: 2614
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetFlexGrow(IntPtr node, float flexGrow);

		// Token: 0x06000A37 RID: 2615
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeStyleGetFlexGrow(IntPtr node);

		// Token: 0x06000A38 RID: 2616
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetFlexShrink(IntPtr node, float flexShrink);

		// Token: 0x06000A39 RID: 2617
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeStyleGetFlexShrink(IntPtr node);

		// Token: 0x06000A3A RID: 2618
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetFlexBasis(IntPtr node, float flexBasis);

		// Token: 0x06000A3B RID: 2619
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetFlexBasisPercent(IntPtr node, float flexBasis);

		// Token: 0x06000A3C RID: 2620
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetFlexBasisAuto(IntPtr node);

		// Token: 0x06000A3D RID: 2621 RVA: 0x0002249C File Offset: 0x0002069C
		[FreeFunction]
		public static YogaValue YGNodeStyleGetFlexBasis(IntPtr node)
		{
			YogaValue result;
			Native.YGNodeStyleGetFlexBasis_Injected(node, out result);
			return result;
		}

		// Token: 0x06000A3E RID: 2622
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetWidth(IntPtr node, float width);

		// Token: 0x06000A3F RID: 2623
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetWidthPercent(IntPtr node, float width);

		// Token: 0x06000A40 RID: 2624
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetWidthAuto(IntPtr node);

		// Token: 0x06000A41 RID: 2625 RVA: 0x000224B4 File Offset: 0x000206B4
		[FreeFunction]
		public static YogaValue YGNodeStyleGetWidth(IntPtr node)
		{
			YogaValue result;
			Native.YGNodeStyleGetWidth_Injected(node, out result);
			return result;
		}

		// Token: 0x06000A42 RID: 2626
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetHeight(IntPtr node, float height);

		// Token: 0x06000A43 RID: 2627
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetHeightPercent(IntPtr node, float height);

		// Token: 0x06000A44 RID: 2628
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetHeightAuto(IntPtr node);

		// Token: 0x06000A45 RID: 2629 RVA: 0x000224CC File Offset: 0x000206CC
		[FreeFunction]
		public static YogaValue YGNodeStyleGetHeight(IntPtr node)
		{
			YogaValue result;
			Native.YGNodeStyleGetHeight_Injected(node, out result);
			return result;
		}

		// Token: 0x06000A46 RID: 2630
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMinWidth(IntPtr node, float minWidth);

		// Token: 0x06000A47 RID: 2631
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMinWidthPercent(IntPtr node, float minWidth);

		// Token: 0x06000A48 RID: 2632 RVA: 0x000224E4 File Offset: 0x000206E4
		[FreeFunction]
		public static YogaValue YGNodeStyleGetMinWidth(IntPtr node)
		{
			YogaValue result;
			Native.YGNodeStyleGetMinWidth_Injected(node, out result);
			return result;
		}

		// Token: 0x06000A49 RID: 2633
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMinHeight(IntPtr node, float minHeight);

		// Token: 0x06000A4A RID: 2634
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMinHeightPercent(IntPtr node, float minHeight);

		// Token: 0x06000A4B RID: 2635 RVA: 0x000224FC File Offset: 0x000206FC
		[FreeFunction]
		public static YogaValue YGNodeStyleGetMinHeight(IntPtr node)
		{
			YogaValue result;
			Native.YGNodeStyleGetMinHeight_Injected(node, out result);
			return result;
		}

		// Token: 0x06000A4C RID: 2636
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMaxWidth(IntPtr node, float maxWidth);

		// Token: 0x06000A4D RID: 2637
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMaxWidthPercent(IntPtr node, float maxWidth);

		// Token: 0x06000A4E RID: 2638 RVA: 0x00022514 File Offset: 0x00020714
		[FreeFunction]
		public static YogaValue YGNodeStyleGetMaxWidth(IntPtr node)
		{
			YogaValue result;
			Native.YGNodeStyleGetMaxWidth_Injected(node, out result);
			return result;
		}

		// Token: 0x06000A4F RID: 2639
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMaxHeight(IntPtr node, float maxHeight);

		// Token: 0x06000A50 RID: 2640
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMaxHeightPercent(IntPtr node, float maxHeight);

		// Token: 0x06000A51 RID: 2641 RVA: 0x0002252C File Offset: 0x0002072C
		[FreeFunction]
		public static YogaValue YGNodeStyleGetMaxHeight(IntPtr node)
		{
			YogaValue result;
			Native.YGNodeStyleGetMaxHeight_Injected(node, out result);
			return result;
		}

		// Token: 0x06000A52 RID: 2642
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetAspectRatio(IntPtr node, float aspectRatio);

		// Token: 0x06000A53 RID: 2643
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeStyleGetAspectRatio(IntPtr node);

		// Token: 0x06000A54 RID: 2644
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetPosition(IntPtr node, YogaEdge edge, float position);

		// Token: 0x06000A55 RID: 2645
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetPositionPercent(IntPtr node, YogaEdge edge, float position);

		// Token: 0x06000A56 RID: 2646 RVA: 0x00022544 File Offset: 0x00020744
		[FreeFunction]
		public static YogaValue YGNodeStyleGetPosition(IntPtr node, YogaEdge edge)
		{
			YogaValue result;
			Native.YGNodeStyleGetPosition_Injected(node, edge, out result);
			return result;
		}

		// Token: 0x06000A57 RID: 2647
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMargin(IntPtr node, YogaEdge edge, float margin);

		// Token: 0x06000A58 RID: 2648
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMarginPercent(IntPtr node, YogaEdge edge, float margin);

		// Token: 0x06000A59 RID: 2649
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetMarginAuto(IntPtr node, YogaEdge edge);

		// Token: 0x06000A5A RID: 2650 RVA: 0x0002255C File Offset: 0x0002075C
		[FreeFunction]
		public static YogaValue YGNodeStyleGetMargin(IntPtr node, YogaEdge edge)
		{
			YogaValue result;
			Native.YGNodeStyleGetMargin_Injected(node, edge, out result);
			return result;
		}

		// Token: 0x06000A5B RID: 2651
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetPadding(IntPtr node, YogaEdge edge, float padding);

		// Token: 0x06000A5C RID: 2652
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetPaddingPercent(IntPtr node, YogaEdge edge, float padding);

		// Token: 0x06000A5D RID: 2653 RVA: 0x00022574 File Offset: 0x00020774
		[FreeFunction]
		public static YogaValue YGNodeStyleGetPadding(IntPtr node, YogaEdge edge)
		{
			YogaValue result;
			Native.YGNodeStyleGetPadding_Injected(node, edge, out result);
			return result;
		}

		// Token: 0x06000A5E RID: 2654
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void YGNodeStyleSetBorder(IntPtr node, YogaEdge edge, float border);

		// Token: 0x06000A5F RID: 2655
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeStyleGetBorder(IntPtr node, YogaEdge edge);

		// Token: 0x06000A60 RID: 2656
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeLayoutGetLeft(IntPtr node);

		// Token: 0x06000A61 RID: 2657
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeLayoutGetTop(IntPtr node);

		// Token: 0x06000A62 RID: 2658
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeLayoutGetRight(IntPtr node);

		// Token: 0x06000A63 RID: 2659
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeLayoutGetBottom(IntPtr node);

		// Token: 0x06000A64 RID: 2660
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeLayoutGetWidth(IntPtr node);

		// Token: 0x06000A65 RID: 2661
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeLayoutGetHeight(IntPtr node);

		// Token: 0x06000A66 RID: 2662
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeLayoutGetMargin(IntPtr node, YogaEdge edge);

		// Token: 0x06000A67 RID: 2663
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float YGNodeLayoutGetPadding(IntPtr node, YogaEdge edge);

		// Token: 0x06000A68 RID: 2664
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern YogaDirection YGNodeLayoutGetDirection(IntPtr node);

		// Token: 0x06000A69 RID: 2665
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeStyleGetFlexBasis_Injected(IntPtr node, out YogaValue ret);

		// Token: 0x06000A6A RID: 2666
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeStyleGetWidth_Injected(IntPtr node, out YogaValue ret);

		// Token: 0x06000A6B RID: 2667
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeStyleGetHeight_Injected(IntPtr node, out YogaValue ret);

		// Token: 0x06000A6C RID: 2668
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeStyleGetMinWidth_Injected(IntPtr node, out YogaValue ret);

		// Token: 0x06000A6D RID: 2669
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeStyleGetMinHeight_Injected(IntPtr node, out YogaValue ret);

		// Token: 0x06000A6E RID: 2670
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeStyleGetMaxWidth_Injected(IntPtr node, out YogaValue ret);

		// Token: 0x06000A6F RID: 2671
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeStyleGetMaxHeight_Injected(IntPtr node, out YogaValue ret);

		// Token: 0x06000A70 RID: 2672
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeStyleGetPosition_Injected(IntPtr node, YogaEdge edge, out YogaValue ret);

		// Token: 0x06000A71 RID: 2673
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeStyleGetMargin_Injected(IntPtr node, YogaEdge edge, out YogaValue ret);

		// Token: 0x06000A72 RID: 2674
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void YGNodeStyleGetPadding_Injected(IntPtr node, YogaEdge edge, out YogaValue ret);
	}
}
