﻿using System;
using System.Runtime.InteropServices;

namespace UnityEngine.Yoga
{
	// Token: 0x02000186 RID: 390
	// (Invoke) Token: 0x060009E6 RID: 2534
	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	internal delegate float YogaBaselineFunc(IntPtr unmanagedNodePtr, float width, float height);
}
