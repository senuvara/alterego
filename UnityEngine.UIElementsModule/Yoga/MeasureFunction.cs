﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000183 RID: 387
	// (Invoke) Token: 0x060009E0 RID: 2528
	internal delegate YogaSize MeasureFunction(YogaNode node, float width, YogaMeasureMode widthMode, float height, YogaMeasureMode heightMode);
}
