﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000184 RID: 388
	internal class MeasureOutput
	{
		// Token: 0x060009E3 RID: 2531 RVA: 0x00002223 File Offset: 0x00000423
		public MeasureOutput()
		{
		}

		// Token: 0x060009E4 RID: 2532 RVA: 0x00022240 File Offset: 0x00020440
		public static YogaSize Make(float width, float height)
		{
			return new YogaSize
			{
				width = width,
				height = height
			};
		}
	}
}
