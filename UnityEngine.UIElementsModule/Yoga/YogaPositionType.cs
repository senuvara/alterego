﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000199 RID: 409
	internal enum YogaPositionType
	{
		// Token: 0x040004B5 RID: 1205
		Relative,
		// Token: 0x040004B6 RID: 1206
		Absolute
	}
}
