﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000193 RID: 403
	internal enum YogaMeasureMode
	{
		// Token: 0x040004A2 RID: 1186
		Undefined,
		// Token: 0x040004A3 RID: 1187
		Exactly,
		// Token: 0x040004A4 RID: 1188
		AtMost
	}
}
