﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000182 RID: 386
	// (Invoke) Token: 0x060009DC RID: 2524
	internal delegate void Logger(YogaConfig config, YogaNode node, YogaLogLevel level, string message);
}
