﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x0200019D RID: 413
	internal struct YogaValue
	{
		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06000B11 RID: 2833 RVA: 0x00023B30 File Offset: 0x00021D30
		public YogaUnit Unit
		{
			get
			{
				return this.unit;
			}
		}

		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06000B12 RID: 2834 RVA: 0x00023B4C File Offset: 0x00021D4C
		public float Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x06000B13 RID: 2835 RVA: 0x00023B68 File Offset: 0x00021D68
		public static YogaValue Point(float value)
		{
			return new YogaValue
			{
				value = value,
				unit = ((!YogaConstants.IsUndefined(value)) ? YogaUnit.Point : YogaUnit.Undefined)
			};
		}

		// Token: 0x06000B14 RID: 2836 RVA: 0x00023BA8 File Offset: 0x00021DA8
		public bool Equals(YogaValue other)
		{
			return this.Unit == other.Unit && (this.Value.Equals(other.Value) || this.Unit == YogaUnit.Undefined);
		}

		// Token: 0x06000B15 RID: 2837 RVA: 0x00023BF8 File Offset: 0x00021DF8
		public override bool Equals(object obj)
		{
			return !object.ReferenceEquals(null, obj) && obj is YogaValue && this.Equals((YogaValue)obj);
		}

		// Token: 0x06000B16 RID: 2838 RVA: 0x00023C3C File Offset: 0x00021E3C
		public override int GetHashCode()
		{
			return this.Value.GetHashCode() * 397 ^ (int)this.Unit;
		}

		// Token: 0x06000B17 RID: 2839 RVA: 0x00023C74 File Offset: 0x00021E74
		public static YogaValue Undefined()
		{
			return new YogaValue
			{
				value = float.NaN,
				unit = YogaUnit.Undefined
			};
		}

		// Token: 0x06000B18 RID: 2840 RVA: 0x00023CA8 File Offset: 0x00021EA8
		public static YogaValue Auto()
		{
			return new YogaValue
			{
				value = 0f,
				unit = YogaUnit.Auto
			};
		}

		// Token: 0x06000B19 RID: 2841 RVA: 0x00023CDC File Offset: 0x00021EDC
		public static YogaValue Percent(float value)
		{
			return new YogaValue
			{
				value = value,
				unit = ((!YogaConstants.IsUndefined(value)) ? YogaUnit.Percent : YogaUnit.Undefined)
			};
		}

		// Token: 0x06000B1A RID: 2842 RVA: 0x00023D1C File Offset: 0x00021F1C
		public static implicit operator YogaValue(float pointValue)
		{
			return YogaValue.Point(pointValue);
		}

		// Token: 0x06000B1B RID: 2843 RVA: 0x00023D38 File Offset: 0x00021F38
		internal static YogaValue MarshalValue(YogaValue value)
		{
			return value;
		}

		// Token: 0x040004C2 RID: 1218
		private float value;

		// Token: 0x040004C3 RID: 1219
		private YogaUnit unit;
	}
}
