﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x0200018F RID: 399
	internal enum YogaJustify
	{
		// Token: 0x04000495 RID: 1173
		FlexStart,
		// Token: 0x04000496 RID: 1174
		Center,
		// Token: 0x04000497 RID: 1175
		FlexEnd,
		// Token: 0x04000498 RID: 1176
		SpaceBetween,
		// Token: 0x04000499 RID: 1177
		SpaceAround
	}
}
