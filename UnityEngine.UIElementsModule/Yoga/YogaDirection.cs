﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x0200018A RID: 394
	internal enum YogaDirection
	{
		// Token: 0x0400047D RID: 1149
		Inherit,
		// Token: 0x0400047E RID: 1150
		LTR,
		// Token: 0x0400047F RID: 1151
		RTL
	}
}
