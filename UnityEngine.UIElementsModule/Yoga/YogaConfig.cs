﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000187 RID: 391
	internal class YogaConfig
	{
		// Token: 0x060009E9 RID: 2537 RVA: 0x0002226E File Offset: 0x0002046E
		private YogaConfig(IntPtr ygConfig)
		{
			this._ygConfig = ygConfig;
			if (this._ygConfig == IntPtr.Zero)
			{
				throw new InvalidOperationException("Failed to allocate native memory");
			}
		}

		// Token: 0x060009EA RID: 2538 RVA: 0x0002229F File Offset: 0x0002049F
		public YogaConfig() : this(Native.YGConfigNew())
		{
		}

		// Token: 0x060009EB RID: 2539 RVA: 0x000222B0 File Offset: 0x000204B0
		~YogaConfig()
		{
			if (this.Handle != YogaConfig.Default.Handle)
			{
				Native.YGConfigFree(this.Handle);
			}
		}

		// Token: 0x17000289 RID: 649
		// (get) Token: 0x060009EC RID: 2540 RVA: 0x00022304 File Offset: 0x00020504
		internal IntPtr Handle
		{
			get
			{
				return this._ygConfig;
			}
		}

		// Token: 0x1700028A RID: 650
		// (get) Token: 0x060009ED RID: 2541 RVA: 0x00022320 File Offset: 0x00020520
		// (set) Token: 0x060009EE RID: 2542 RVA: 0x0002233B File Offset: 0x0002053B
		public Logger Logger
		{
			get
			{
				return this._logger;
			}
			set
			{
				this._logger = value;
			}
		}

		// Token: 0x060009EF RID: 2543 RVA: 0x00022345 File Offset: 0x00020545
		public void SetExperimentalFeatureEnabled(YogaExperimentalFeature feature, bool enabled)
		{
			Native.YGConfigSetExperimentalFeatureEnabled(this._ygConfig, feature, enabled);
		}

		// Token: 0x060009F0 RID: 2544 RVA: 0x00022358 File Offset: 0x00020558
		public bool IsExperimentalFeatureEnabled(YogaExperimentalFeature feature)
		{
			return Native.YGConfigIsExperimentalFeatureEnabled(this._ygConfig, feature);
		}

		// Token: 0x1700028B RID: 651
		// (get) Token: 0x060009F1 RID: 2545 RVA: 0x0002237C File Offset: 0x0002057C
		// (set) Token: 0x060009F2 RID: 2546 RVA: 0x0002239C File Offset: 0x0002059C
		public bool UseWebDefaults
		{
			get
			{
				return Native.YGConfigGetUseWebDefaults(this._ygConfig);
			}
			set
			{
				Native.YGConfigSetUseWebDefaults(this._ygConfig, value);
			}
		}

		// Token: 0x1700028C RID: 652
		// (set) Token: 0x060009F3 RID: 2547 RVA: 0x000223AB File Offset: 0x000205AB
		public float PointScaleFactor
		{
			set
			{
				Native.YGConfigSetPointScaleFactor(this._ygConfig, value);
			}
		}

		// Token: 0x060009F4 RID: 2548 RVA: 0x000223BC File Offset: 0x000205BC
		public static int GetInstanceCount()
		{
			return Native.YGConfigGetInstanceCount();
		}

		// Token: 0x060009F5 RID: 2549 RVA: 0x000223D6 File Offset: 0x000205D6
		public static void SetDefaultLogger(Logger logger)
		{
			YogaConfig.Default.Logger = logger;
		}

		// Token: 0x060009F6 RID: 2550 RVA: 0x000223E4 File Offset: 0x000205E4
		// Note: this type is marked as 'beforefieldinit'.
		static YogaConfig()
		{
		}

		// Token: 0x04000475 RID: 1141
		internal static readonly YogaConfig Default = new YogaConfig(Native.YGConfigGetDefault());

		// Token: 0x04000476 RID: 1142
		private IntPtr _ygConfig;

		// Token: 0x04000477 RID: 1143
		private Logger _logger;
	}
}
