﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x0200018E RID: 398
	internal enum YogaFlexDirection
	{
		// Token: 0x04000490 RID: 1168
		Column,
		// Token: 0x04000491 RID: 1169
		ColumnReverse,
		// Token: 0x04000492 RID: 1170
		Row,
		// Token: 0x04000493 RID: 1171
		RowReverse
	}
}
