﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000185 RID: 389
	internal enum YogaAlign
	{
		// Token: 0x0400046D RID: 1133
		Auto,
		// Token: 0x0400046E RID: 1134
		FlexStart,
		// Token: 0x0400046F RID: 1135
		Center,
		// Token: 0x04000470 RID: 1136
		FlexEnd,
		// Token: 0x04000471 RID: 1137
		Stretch,
		// Token: 0x04000472 RID: 1138
		Baseline,
		// Token: 0x04000473 RID: 1139
		SpaceBetween,
		// Token: 0x04000474 RID: 1140
		SpaceAround
	}
}
