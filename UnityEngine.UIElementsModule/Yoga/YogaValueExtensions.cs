﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x0200019E RID: 414
	internal static class YogaValueExtensions
	{
		// Token: 0x06000B1C RID: 2844 RVA: 0x00023D50 File Offset: 0x00021F50
		public static YogaValue Percent(this float value)
		{
			return YogaValue.Percent(value);
		}

		// Token: 0x06000B1D RID: 2845 RVA: 0x00023D6C File Offset: 0x00021F6C
		public static YogaValue Pt(this float value)
		{
			return YogaValue.Point(value);
		}

		// Token: 0x06000B1E RID: 2846 RVA: 0x00023D88 File Offset: 0x00021F88
		public static YogaValue Percent(this int value)
		{
			return YogaValue.Percent((float)value);
		}

		// Token: 0x06000B1F RID: 2847 RVA: 0x00023DA4 File Offset: 0x00021FA4
		public static YogaValue Pt(this int value)
		{
			return YogaValue.Point((float)value);
		}
	}
}
