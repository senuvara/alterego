﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000181 RID: 385
	// (Invoke) Token: 0x060009D8 RID: 2520
	internal delegate float BaselineFunction(YogaNode node, float width, float height);
}
