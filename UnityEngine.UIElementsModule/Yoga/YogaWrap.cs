﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x0200019F RID: 415
	internal enum YogaWrap
	{
		// Token: 0x040004C5 RID: 1221
		NoWrap,
		// Token: 0x040004C6 RID: 1222
		Wrap,
		// Token: 0x040004C7 RID: 1223
		WrapReverse
	}
}
