﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x0200018C RID: 396
	internal enum YogaEdge
	{
		// Token: 0x04000484 RID: 1156
		Left,
		// Token: 0x04000485 RID: 1157
		Top,
		// Token: 0x04000486 RID: 1158
		Right,
		// Token: 0x04000487 RID: 1159
		Bottom,
		// Token: 0x04000488 RID: 1160
		Start,
		// Token: 0x04000489 RID: 1161
		End,
		// Token: 0x0400048A RID: 1162
		Horizontal,
		// Token: 0x0400048B RID: 1163
		Vertical,
		// Token: 0x0400048C RID: 1164
		All
	}
}
