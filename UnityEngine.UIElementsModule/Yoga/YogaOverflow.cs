﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000198 RID: 408
	internal enum YogaOverflow
	{
		// Token: 0x040004B1 RID: 1201
		Visible,
		// Token: 0x040004B2 RID: 1202
		Hidden,
		// Token: 0x040004B3 RID: 1203
		Scroll
	}
}
