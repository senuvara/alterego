﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x02000190 RID: 400
	internal enum YogaLogLevel
	{
		// Token: 0x0400049B RID: 1179
		Error,
		// Token: 0x0400049C RID: 1180
		Warn,
		// Token: 0x0400049D RID: 1181
		Info,
		// Token: 0x0400049E RID: 1182
		Debug,
		// Token: 0x0400049F RID: 1183
		Verbose,
		// Token: 0x040004A0 RID: 1184
		Fatal
	}
}
