﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace UnityEngine.Yoga
{
	// Token: 0x02000195 RID: 405
	internal class YogaNode : IEnumerable<YogaNode>, IEnumerable
	{
		// Token: 0x06000A73 RID: 2675 RVA: 0x0002258C File Offset: 0x0002078C
		public YogaNode(YogaConfig config = null)
		{
			this._config = ((config != null) ? config : YogaConfig.Default);
			this._ygNode = Native.YGNodeNewWithConfig(this._config.Handle);
			if (this._ygNode == IntPtr.Zero)
			{
				throw new InvalidOperationException("Failed to allocate native memory");
			}
		}

		// Token: 0x06000A74 RID: 2676 RVA: 0x000225EE File Offset: 0x000207EE
		public YogaNode(YogaNode srcNode) : this(srcNode._config)
		{
			this.CopyStyle(srcNode);
		}

		// Token: 0x1700028D RID: 653
		// (get) Token: 0x06000A75 RID: 2677 RVA: 0x00022604 File Offset: 0x00020804
		// (set) Token: 0x06000A76 RID: 2678 RVA: 0x0002262A File Offset: 0x0002082A
		public YogaValue Left
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPosition(this._ygNode, YogaEdge.Left));
			}
			set
			{
				this.SetStylePosition(YogaEdge.Left, value);
			}
		}

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x06000A77 RID: 2679 RVA: 0x00022638 File Offset: 0x00020838
		// (set) Token: 0x06000A78 RID: 2680 RVA: 0x0002265E File Offset: 0x0002085E
		public YogaValue Top
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPosition(this._ygNode, YogaEdge.Top));
			}
			set
			{
				this.SetStylePosition(YogaEdge.Top, value);
			}
		}

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x06000A79 RID: 2681 RVA: 0x0002266C File Offset: 0x0002086C
		// (set) Token: 0x06000A7A RID: 2682 RVA: 0x00022692 File Offset: 0x00020892
		public YogaValue Right
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPosition(this._ygNode, YogaEdge.Right));
			}
			set
			{
				this.SetStylePosition(YogaEdge.Right, value);
			}
		}

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x06000A7B RID: 2683 RVA: 0x000226A0 File Offset: 0x000208A0
		// (set) Token: 0x06000A7C RID: 2684 RVA: 0x000226C6 File Offset: 0x000208C6
		public YogaValue Bottom
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPosition(this._ygNode, YogaEdge.Bottom));
			}
			set
			{
				this.SetStylePosition(YogaEdge.Bottom, value);
			}
		}

		// Token: 0x17000291 RID: 657
		// (get) Token: 0x06000A7D RID: 2685 RVA: 0x000226D4 File Offset: 0x000208D4
		// (set) Token: 0x06000A7E RID: 2686 RVA: 0x000226FA File Offset: 0x000208FA
		public YogaValue Start
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPosition(this._ygNode, YogaEdge.Start));
			}
			set
			{
				this.SetStylePosition(YogaEdge.Start, value);
			}
		}

		// Token: 0x17000292 RID: 658
		// (get) Token: 0x06000A7F RID: 2687 RVA: 0x00022708 File Offset: 0x00020908
		// (set) Token: 0x06000A80 RID: 2688 RVA: 0x0002272E File Offset: 0x0002092E
		public YogaValue End
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPosition(this._ygNode, YogaEdge.End));
			}
			set
			{
				this.SetStylePosition(YogaEdge.End, value);
			}
		}

		// Token: 0x06000A81 RID: 2689 RVA: 0x00022739 File Offset: 0x00020939
		private void SetStylePosition(YogaEdge edge, YogaValue value)
		{
			if (value.Unit == YogaUnit.Percent)
			{
				Native.YGNodeStyleSetPositionPercent(this._ygNode, edge, value.Value);
			}
			else
			{
				Native.YGNodeStyleSetPosition(this._ygNode, edge, value.Value);
			}
		}

		// Token: 0x17000293 RID: 659
		// (get) Token: 0x06000A82 RID: 2690 RVA: 0x00022778 File Offset: 0x00020978
		// (set) Token: 0x06000A83 RID: 2691 RVA: 0x0002279E File Offset: 0x0002099E
		public YogaValue MarginLeft
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMargin(this._ygNode, YogaEdge.Left));
			}
			set
			{
				this.SetStyleMargin(YogaEdge.Left, value);
			}
		}

		// Token: 0x17000294 RID: 660
		// (get) Token: 0x06000A84 RID: 2692 RVA: 0x000227AC File Offset: 0x000209AC
		// (set) Token: 0x06000A85 RID: 2693 RVA: 0x000227D2 File Offset: 0x000209D2
		public YogaValue MarginTop
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMargin(this._ygNode, YogaEdge.Top));
			}
			set
			{
				this.SetStyleMargin(YogaEdge.Top, value);
			}
		}

		// Token: 0x17000295 RID: 661
		// (get) Token: 0x06000A86 RID: 2694 RVA: 0x000227E0 File Offset: 0x000209E0
		// (set) Token: 0x06000A87 RID: 2695 RVA: 0x00022806 File Offset: 0x00020A06
		public YogaValue MarginRight
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMargin(this._ygNode, YogaEdge.Right));
			}
			set
			{
				this.SetStyleMargin(YogaEdge.Right, value);
			}
		}

		// Token: 0x17000296 RID: 662
		// (get) Token: 0x06000A88 RID: 2696 RVA: 0x00022814 File Offset: 0x00020A14
		// (set) Token: 0x06000A89 RID: 2697 RVA: 0x0002283A File Offset: 0x00020A3A
		public YogaValue MarginBottom
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMargin(this._ygNode, YogaEdge.Bottom));
			}
			set
			{
				this.SetStyleMargin(YogaEdge.Bottom, value);
			}
		}

		// Token: 0x17000297 RID: 663
		// (get) Token: 0x06000A8A RID: 2698 RVA: 0x00022848 File Offset: 0x00020A48
		// (set) Token: 0x06000A8B RID: 2699 RVA: 0x0002286E File Offset: 0x00020A6E
		public YogaValue MarginStart
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMargin(this._ygNode, YogaEdge.Start));
			}
			set
			{
				this.SetStyleMargin(YogaEdge.Start, value);
			}
		}

		// Token: 0x17000298 RID: 664
		// (get) Token: 0x06000A8C RID: 2700 RVA: 0x0002287C File Offset: 0x00020A7C
		// (set) Token: 0x06000A8D RID: 2701 RVA: 0x000228A2 File Offset: 0x00020AA2
		public YogaValue MarginEnd
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMargin(this._ygNode, YogaEdge.End));
			}
			set
			{
				this.SetStyleMargin(YogaEdge.End, value);
			}
		}

		// Token: 0x17000299 RID: 665
		// (get) Token: 0x06000A8E RID: 2702 RVA: 0x000228B0 File Offset: 0x00020AB0
		// (set) Token: 0x06000A8F RID: 2703 RVA: 0x000228D6 File Offset: 0x00020AD6
		public YogaValue MarginHorizontal
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMargin(this._ygNode, YogaEdge.Horizontal));
			}
			set
			{
				this.SetStyleMargin(YogaEdge.Horizontal, value);
			}
		}

		// Token: 0x1700029A RID: 666
		// (get) Token: 0x06000A90 RID: 2704 RVA: 0x000228E4 File Offset: 0x00020AE4
		// (set) Token: 0x06000A91 RID: 2705 RVA: 0x0002290A File Offset: 0x00020B0A
		public YogaValue MarginVertical
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMargin(this._ygNode, YogaEdge.Vertical));
			}
			set
			{
				this.SetStyleMargin(YogaEdge.Vertical, value);
			}
		}

		// Token: 0x1700029B RID: 667
		// (get) Token: 0x06000A92 RID: 2706 RVA: 0x00022918 File Offset: 0x00020B18
		// (set) Token: 0x06000A93 RID: 2707 RVA: 0x0002293E File Offset: 0x00020B3E
		public YogaValue Margin
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMargin(this._ygNode, YogaEdge.All));
			}
			set
			{
				this.SetStyleMargin(YogaEdge.All, value);
			}
		}

		// Token: 0x06000A94 RID: 2708 RVA: 0x0002294C File Offset: 0x00020B4C
		private void SetStyleMargin(YogaEdge edge, YogaValue value)
		{
			if (value.Unit == YogaUnit.Percent)
			{
				Native.YGNodeStyleSetMarginPercent(this._ygNode, edge, value.Value);
			}
			else if (value.Unit == YogaUnit.Auto)
			{
				Native.YGNodeStyleSetMarginAuto(this._ygNode, edge);
			}
			else
			{
				Native.YGNodeStyleSetMargin(this._ygNode, edge, value.Value);
			}
		}

		// Token: 0x1700029C RID: 668
		// (get) Token: 0x06000A95 RID: 2709 RVA: 0x000229B8 File Offset: 0x00020BB8
		// (set) Token: 0x06000A96 RID: 2710 RVA: 0x000229DE File Offset: 0x00020BDE
		public YogaValue PaddingLeft
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPadding(this._ygNode, YogaEdge.Left));
			}
			set
			{
				this.SetStylePadding(YogaEdge.Left, value);
			}
		}

		// Token: 0x1700029D RID: 669
		// (get) Token: 0x06000A97 RID: 2711 RVA: 0x000229EC File Offset: 0x00020BEC
		// (set) Token: 0x06000A98 RID: 2712 RVA: 0x00022A12 File Offset: 0x00020C12
		public YogaValue PaddingTop
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPadding(this._ygNode, YogaEdge.Top));
			}
			set
			{
				this.SetStylePadding(YogaEdge.Top, value);
			}
		}

		// Token: 0x1700029E RID: 670
		// (get) Token: 0x06000A99 RID: 2713 RVA: 0x00022A20 File Offset: 0x00020C20
		// (set) Token: 0x06000A9A RID: 2714 RVA: 0x00022A46 File Offset: 0x00020C46
		public YogaValue PaddingRight
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPadding(this._ygNode, YogaEdge.Right));
			}
			set
			{
				this.SetStylePadding(YogaEdge.Right, value);
			}
		}

		// Token: 0x1700029F RID: 671
		// (get) Token: 0x06000A9B RID: 2715 RVA: 0x00022A54 File Offset: 0x00020C54
		// (set) Token: 0x06000A9C RID: 2716 RVA: 0x00022A7A File Offset: 0x00020C7A
		public YogaValue PaddingBottom
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPadding(this._ygNode, YogaEdge.Bottom));
			}
			set
			{
				this.SetStylePadding(YogaEdge.Bottom, value);
			}
		}

		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x06000A9D RID: 2717 RVA: 0x00022A88 File Offset: 0x00020C88
		// (set) Token: 0x06000A9E RID: 2718 RVA: 0x00022AAE File Offset: 0x00020CAE
		public YogaValue PaddingStart
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPadding(this._ygNode, YogaEdge.Start));
			}
			set
			{
				this.SetStylePadding(YogaEdge.Start, value);
			}
		}

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000A9F RID: 2719 RVA: 0x00022ABC File Offset: 0x00020CBC
		// (set) Token: 0x06000AA0 RID: 2720 RVA: 0x00022AE2 File Offset: 0x00020CE2
		public YogaValue PaddingEnd
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPadding(this._ygNode, YogaEdge.End));
			}
			set
			{
				this.SetStylePadding(YogaEdge.End, value);
			}
		}

		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000AA1 RID: 2721 RVA: 0x00022AF0 File Offset: 0x00020CF0
		// (set) Token: 0x06000AA2 RID: 2722 RVA: 0x00022B16 File Offset: 0x00020D16
		public YogaValue PaddingHorizontal
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPadding(this._ygNode, YogaEdge.Horizontal));
			}
			set
			{
				this.SetStylePadding(YogaEdge.Horizontal, value);
			}
		}

		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06000AA3 RID: 2723 RVA: 0x00022B24 File Offset: 0x00020D24
		// (set) Token: 0x06000AA4 RID: 2724 RVA: 0x00022B4A File Offset: 0x00020D4A
		public YogaValue PaddingVertical
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPadding(this._ygNode, YogaEdge.Vertical));
			}
			set
			{
				this.SetStylePadding(YogaEdge.Vertical, value);
			}
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06000AA5 RID: 2725 RVA: 0x00022B58 File Offset: 0x00020D58
		// (set) Token: 0x06000AA6 RID: 2726 RVA: 0x00022B7E File Offset: 0x00020D7E
		public YogaValue Padding
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetPadding(this._ygNode, YogaEdge.All));
			}
			set
			{
				this.SetStylePadding(YogaEdge.All, value);
			}
		}

		// Token: 0x06000AA7 RID: 2727 RVA: 0x00022B89 File Offset: 0x00020D89
		private void SetStylePadding(YogaEdge edge, YogaValue value)
		{
			if (value.Unit == YogaUnit.Percent)
			{
				Native.YGNodeStyleSetPaddingPercent(this._ygNode, edge, value.Value);
			}
			else
			{
				Native.YGNodeStyleSetPadding(this._ygNode, edge, value.Value);
			}
		}

		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x06000AA8 RID: 2728 RVA: 0x00022BC8 File Offset: 0x00020DC8
		// (set) Token: 0x06000AA9 RID: 2729 RVA: 0x00022BE9 File Offset: 0x00020DE9
		public float BorderLeftWidth
		{
			get
			{
				return Native.YGNodeStyleGetBorder(this._ygNode, YogaEdge.Left);
			}
			set
			{
				Native.YGNodeStyleSetBorder(this._ygNode, YogaEdge.Left, value);
			}
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000AAA RID: 2730 RVA: 0x00022BFC File Offset: 0x00020DFC
		// (set) Token: 0x06000AAB RID: 2731 RVA: 0x00022C1D File Offset: 0x00020E1D
		public float BorderTopWidth
		{
			get
			{
				return Native.YGNodeStyleGetBorder(this._ygNode, YogaEdge.Top);
			}
			set
			{
				Native.YGNodeStyleSetBorder(this._ygNode, YogaEdge.Top, value);
			}
		}

		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x06000AAC RID: 2732 RVA: 0x00022C30 File Offset: 0x00020E30
		// (set) Token: 0x06000AAD RID: 2733 RVA: 0x00022C51 File Offset: 0x00020E51
		public float BorderRightWidth
		{
			get
			{
				return Native.YGNodeStyleGetBorder(this._ygNode, YogaEdge.Right);
			}
			set
			{
				Native.YGNodeStyleSetBorder(this._ygNode, YogaEdge.Right, value);
			}
		}

		// Token: 0x170002A8 RID: 680
		// (get) Token: 0x06000AAE RID: 2734 RVA: 0x00022C64 File Offset: 0x00020E64
		// (set) Token: 0x06000AAF RID: 2735 RVA: 0x00022C85 File Offset: 0x00020E85
		public float BorderBottomWidth
		{
			get
			{
				return Native.YGNodeStyleGetBorder(this._ygNode, YogaEdge.Bottom);
			}
			set
			{
				Native.YGNodeStyleSetBorder(this._ygNode, YogaEdge.Bottom, value);
			}
		}

		// Token: 0x170002A9 RID: 681
		// (get) Token: 0x06000AB0 RID: 2736 RVA: 0x00022C98 File Offset: 0x00020E98
		// (set) Token: 0x06000AB1 RID: 2737 RVA: 0x00022CB9 File Offset: 0x00020EB9
		public float BorderStartWidth
		{
			get
			{
				return Native.YGNodeStyleGetBorder(this._ygNode, YogaEdge.Start);
			}
			set
			{
				Native.YGNodeStyleSetBorder(this._ygNode, YogaEdge.Start, value);
			}
		}

		// Token: 0x170002AA RID: 682
		// (get) Token: 0x06000AB2 RID: 2738 RVA: 0x00022CCC File Offset: 0x00020ECC
		// (set) Token: 0x06000AB3 RID: 2739 RVA: 0x00022CED File Offset: 0x00020EED
		public float BorderEndWidth
		{
			get
			{
				return Native.YGNodeStyleGetBorder(this._ygNode, YogaEdge.End);
			}
			set
			{
				Native.YGNodeStyleSetBorder(this._ygNode, YogaEdge.End, value);
			}
		}

		// Token: 0x170002AB RID: 683
		// (get) Token: 0x06000AB4 RID: 2740 RVA: 0x00022D00 File Offset: 0x00020F00
		// (set) Token: 0x06000AB5 RID: 2741 RVA: 0x00022D21 File Offset: 0x00020F21
		public float BorderWidth
		{
			get
			{
				return Native.YGNodeStyleGetBorder(this._ygNode, YogaEdge.All);
			}
			set
			{
				Native.YGNodeStyleSetBorder(this._ygNode, YogaEdge.All, value);
			}
		}

		// Token: 0x170002AC RID: 684
		// (get) Token: 0x06000AB6 RID: 2742 RVA: 0x00022D34 File Offset: 0x00020F34
		public float LayoutMarginLeft
		{
			get
			{
				return Native.YGNodeLayoutGetMargin(this._ygNode, YogaEdge.Left);
			}
		}

		// Token: 0x170002AD RID: 685
		// (get) Token: 0x06000AB7 RID: 2743 RVA: 0x00022D58 File Offset: 0x00020F58
		public float LayoutMarginTop
		{
			get
			{
				return Native.YGNodeLayoutGetMargin(this._ygNode, YogaEdge.Top);
			}
		}

		// Token: 0x170002AE RID: 686
		// (get) Token: 0x06000AB8 RID: 2744 RVA: 0x00022D7C File Offset: 0x00020F7C
		public float LayoutMarginRight
		{
			get
			{
				return Native.YGNodeLayoutGetMargin(this._ygNode, YogaEdge.Right);
			}
		}

		// Token: 0x170002AF RID: 687
		// (get) Token: 0x06000AB9 RID: 2745 RVA: 0x00022DA0 File Offset: 0x00020FA0
		public float LayoutMarginBottom
		{
			get
			{
				return Native.YGNodeLayoutGetMargin(this._ygNode, YogaEdge.Bottom);
			}
		}

		// Token: 0x170002B0 RID: 688
		// (get) Token: 0x06000ABA RID: 2746 RVA: 0x00022DC4 File Offset: 0x00020FC4
		public float LayoutMarginStart
		{
			get
			{
				return Native.YGNodeLayoutGetMargin(this._ygNode, YogaEdge.Start);
			}
		}

		// Token: 0x170002B1 RID: 689
		// (get) Token: 0x06000ABB RID: 2747 RVA: 0x00022DE8 File Offset: 0x00020FE8
		public float LayoutMarginEnd
		{
			get
			{
				return Native.YGNodeLayoutGetMargin(this._ygNode, YogaEdge.End);
			}
		}

		// Token: 0x170002B2 RID: 690
		// (get) Token: 0x06000ABC RID: 2748 RVA: 0x00022E0C File Offset: 0x0002100C
		public float LayoutPaddingLeft
		{
			get
			{
				return Native.YGNodeLayoutGetPadding(this._ygNode, YogaEdge.Left);
			}
		}

		// Token: 0x170002B3 RID: 691
		// (get) Token: 0x06000ABD RID: 2749 RVA: 0x00022E30 File Offset: 0x00021030
		public float LayoutPaddingTop
		{
			get
			{
				return Native.YGNodeLayoutGetPadding(this._ygNode, YogaEdge.Top);
			}
		}

		// Token: 0x170002B4 RID: 692
		// (get) Token: 0x06000ABE RID: 2750 RVA: 0x00022E54 File Offset: 0x00021054
		public float LayoutPaddingRight
		{
			get
			{
				return Native.YGNodeLayoutGetPadding(this._ygNode, YogaEdge.Right);
			}
		}

		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x06000ABF RID: 2751 RVA: 0x00022E78 File Offset: 0x00021078
		public float LayoutPaddingBottom
		{
			get
			{
				return Native.YGNodeLayoutGetPadding(this._ygNode, YogaEdge.Bottom);
			}
		}

		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x06000AC0 RID: 2752 RVA: 0x00022E9C File Offset: 0x0002109C
		public float LayoutPaddingStart
		{
			get
			{
				return Native.YGNodeLayoutGetPadding(this._ygNode, YogaEdge.Start);
			}
		}

		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x06000AC1 RID: 2753 RVA: 0x00022EC0 File Offset: 0x000210C0
		public float LayoutPaddingEnd
		{
			get
			{
				return Native.YGNodeLayoutGetPadding(this._ygNode, YogaEdge.End);
			}
		}

		// Token: 0x06000AC2 RID: 2754 RVA: 0x00022EE4 File Offset: 0x000210E4
		~YogaNode()
		{
			Native.YGNodeFree(this._ygNode);
		}

		// Token: 0x06000AC3 RID: 2755 RVA: 0x00022F1C File Offset: 0x0002111C
		public void Reset()
		{
			this._measureFunction = null;
			this._baselineFunction = null;
			this._data = null;
			Native.YGSetManagedObject(this._ygNode, null);
			Native.YGNodeReset(this._ygNode);
		}

		// Token: 0x170002B8 RID: 696
		// (get) Token: 0x06000AC4 RID: 2756 RVA: 0x00022F4C File Offset: 0x0002114C
		public bool IsDirty
		{
			get
			{
				return Native.YGNodeIsDirty(this._ygNode);
			}
		}

		// Token: 0x06000AC5 RID: 2757 RVA: 0x00022F6C File Offset: 0x0002116C
		public virtual void MarkDirty()
		{
			Native.YGNodeMarkDirty(this._ygNode);
		}

		// Token: 0x170002B9 RID: 697
		// (get) Token: 0x06000AC6 RID: 2758 RVA: 0x00022F7C File Offset: 0x0002117C
		public bool HasNewLayout
		{
			get
			{
				return Native.YGNodeGetHasNewLayout(this._ygNode);
			}
		}

		// Token: 0x06000AC7 RID: 2759 RVA: 0x00022F9C File Offset: 0x0002119C
		public void MarkHasNewLayout()
		{
			Native.YGNodeSetHasNewLayout(this._ygNode, true);
		}

		// Token: 0x170002BA RID: 698
		// (get) Token: 0x06000AC8 RID: 2760 RVA: 0x00022FAC File Offset: 0x000211AC
		public YogaNode Parent
		{
			get
			{
				return (this._parent == null) ? null : (this._parent.Target as YogaNode);
			}
		}

		// Token: 0x170002BB RID: 699
		// (get) Token: 0x06000AC9 RID: 2761 RVA: 0x00022FE4 File Offset: 0x000211E4
		public bool IsMeasureDefined
		{
			get
			{
				return this._measureFunction != null;
			}
		}

		// Token: 0x170002BC RID: 700
		// (get) Token: 0x06000ACA RID: 2762 RVA: 0x00023008 File Offset: 0x00021208
		public bool IsBaselineDefined
		{
			get
			{
				return this._baselineFunction != null;
			}
		}

		// Token: 0x06000ACB RID: 2763 RVA: 0x00023029 File Offset: 0x00021229
		public void CopyStyle(YogaNode srcNode)
		{
			Native.YGNodeCopyStyle(this._ygNode, srcNode._ygNode);
		}

		// Token: 0x170002BD RID: 701
		// (get) Token: 0x06000ACC RID: 2764 RVA: 0x00023040 File Offset: 0x00021240
		// (set) Token: 0x06000ACD RID: 2765 RVA: 0x00023060 File Offset: 0x00021260
		public YogaDirection StyleDirection
		{
			get
			{
				return Native.YGNodeStyleGetDirection(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetDirection(this._ygNode, value);
			}
		}

		// Token: 0x170002BE RID: 702
		// (get) Token: 0x06000ACE RID: 2766 RVA: 0x00023070 File Offset: 0x00021270
		// (set) Token: 0x06000ACF RID: 2767 RVA: 0x00023090 File Offset: 0x00021290
		public YogaFlexDirection FlexDirection
		{
			get
			{
				return Native.YGNodeStyleGetFlexDirection(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetFlexDirection(this._ygNode, value);
			}
		}

		// Token: 0x170002BF RID: 703
		// (get) Token: 0x06000AD0 RID: 2768 RVA: 0x000230A0 File Offset: 0x000212A0
		// (set) Token: 0x06000AD1 RID: 2769 RVA: 0x000230C0 File Offset: 0x000212C0
		public YogaJustify JustifyContent
		{
			get
			{
				return Native.YGNodeStyleGetJustifyContent(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetJustifyContent(this._ygNode, value);
			}
		}

		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x06000AD2 RID: 2770 RVA: 0x000230D0 File Offset: 0x000212D0
		// (set) Token: 0x06000AD3 RID: 2771 RVA: 0x000230F0 File Offset: 0x000212F0
		public YogaDisplay Display
		{
			get
			{
				return Native.YGNodeStyleGetDisplay(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetDisplay(this._ygNode, value);
			}
		}

		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x06000AD4 RID: 2772 RVA: 0x00023100 File Offset: 0x00021300
		// (set) Token: 0x06000AD5 RID: 2773 RVA: 0x00023120 File Offset: 0x00021320
		public YogaAlign AlignItems
		{
			get
			{
				return Native.YGNodeStyleGetAlignItems(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetAlignItems(this._ygNode, value);
			}
		}

		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x06000AD6 RID: 2774 RVA: 0x00023130 File Offset: 0x00021330
		// (set) Token: 0x06000AD7 RID: 2775 RVA: 0x00023150 File Offset: 0x00021350
		public YogaAlign AlignSelf
		{
			get
			{
				return Native.YGNodeStyleGetAlignSelf(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetAlignSelf(this._ygNode, value);
			}
		}

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x06000AD8 RID: 2776 RVA: 0x00023160 File Offset: 0x00021360
		// (set) Token: 0x06000AD9 RID: 2777 RVA: 0x00023180 File Offset: 0x00021380
		public YogaAlign AlignContent
		{
			get
			{
				return Native.YGNodeStyleGetAlignContent(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetAlignContent(this._ygNode, value);
			}
		}

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x06000ADA RID: 2778 RVA: 0x00023190 File Offset: 0x00021390
		// (set) Token: 0x06000ADB RID: 2779 RVA: 0x000231B0 File Offset: 0x000213B0
		public YogaPositionType PositionType
		{
			get
			{
				return Native.YGNodeStyleGetPositionType(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetPositionType(this._ygNode, value);
			}
		}

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x06000ADC RID: 2780 RVA: 0x000231C0 File Offset: 0x000213C0
		// (set) Token: 0x06000ADD RID: 2781 RVA: 0x000231E0 File Offset: 0x000213E0
		public YogaWrap Wrap
		{
			get
			{
				return Native.YGNodeStyleGetFlexWrap(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetFlexWrap(this._ygNode, value);
			}
		}

		// Token: 0x170002C6 RID: 710
		// (set) Token: 0x06000ADE RID: 2782 RVA: 0x000231EF File Offset: 0x000213EF
		public float Flex
		{
			set
			{
				Native.YGNodeStyleSetFlex(this._ygNode, value);
			}
		}

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06000ADF RID: 2783 RVA: 0x00023200 File Offset: 0x00021400
		// (set) Token: 0x06000AE0 RID: 2784 RVA: 0x00023220 File Offset: 0x00021420
		public float FlexGrow
		{
			get
			{
				return Native.YGNodeStyleGetFlexGrow(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetFlexGrow(this._ygNode, value);
			}
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06000AE1 RID: 2785 RVA: 0x00023230 File Offset: 0x00021430
		// (set) Token: 0x06000AE2 RID: 2786 RVA: 0x00023250 File Offset: 0x00021450
		public float FlexShrink
		{
			get
			{
				return Native.YGNodeStyleGetFlexShrink(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetFlexShrink(this._ygNode, value);
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x06000AE3 RID: 2787 RVA: 0x00023260 File Offset: 0x00021460
		// (set) Token: 0x06000AE4 RID: 2788 RVA: 0x00023288 File Offset: 0x00021488
		public YogaValue FlexBasis
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetFlexBasis(this._ygNode));
			}
			set
			{
				if (value.Unit == YogaUnit.Percent)
				{
					Native.YGNodeStyleSetFlexBasisPercent(this._ygNode, value.Value);
				}
				else if (value.Unit == YogaUnit.Auto)
				{
					Native.YGNodeStyleSetFlexBasisAuto(this._ygNode);
				}
				else
				{
					Native.YGNodeStyleSetFlexBasis(this._ygNode, value.Value);
				}
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06000AE5 RID: 2789 RVA: 0x000232F0 File Offset: 0x000214F0
		// (set) Token: 0x06000AE6 RID: 2790 RVA: 0x00023318 File Offset: 0x00021518
		public YogaValue Width
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetWidth(this._ygNode));
			}
			set
			{
				if (value.Unit == YogaUnit.Percent)
				{
					Native.YGNodeStyleSetWidthPercent(this._ygNode, value.Value);
				}
				else if (value.Unit == YogaUnit.Auto)
				{
					Native.YGNodeStyleSetWidthAuto(this._ygNode);
				}
				else
				{
					Native.YGNodeStyleSetWidth(this._ygNode, value.Value);
				}
			}
		}

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x06000AE7 RID: 2791 RVA: 0x00023380 File Offset: 0x00021580
		// (set) Token: 0x06000AE8 RID: 2792 RVA: 0x000233A8 File Offset: 0x000215A8
		public YogaValue Height
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetHeight(this._ygNode));
			}
			set
			{
				if (value.Unit == YogaUnit.Percent)
				{
					Native.YGNodeStyleSetHeightPercent(this._ygNode, value.Value);
				}
				else if (value.Unit == YogaUnit.Auto)
				{
					Native.YGNodeStyleSetHeightAuto(this._ygNode);
				}
				else
				{
					Native.YGNodeStyleSetHeight(this._ygNode, value.Value);
				}
			}
		}

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x06000AE9 RID: 2793 RVA: 0x00023410 File Offset: 0x00021610
		// (set) Token: 0x06000AEA RID: 2794 RVA: 0x00023435 File Offset: 0x00021635
		public YogaValue MaxWidth
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMaxWidth(this._ygNode));
			}
			set
			{
				if (value.Unit == YogaUnit.Percent)
				{
					Native.YGNodeStyleSetMaxWidthPercent(this._ygNode, value.Value);
				}
				else
				{
					Native.YGNodeStyleSetMaxWidth(this._ygNode, value.Value);
				}
			}
		}

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x06000AEB RID: 2795 RVA: 0x00023474 File Offset: 0x00021674
		// (set) Token: 0x06000AEC RID: 2796 RVA: 0x00023499 File Offset: 0x00021699
		public YogaValue MaxHeight
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMaxHeight(this._ygNode));
			}
			set
			{
				if (value.Unit == YogaUnit.Percent)
				{
					Native.YGNodeStyleSetMaxHeightPercent(this._ygNode, value.Value);
				}
				else
				{
					Native.YGNodeStyleSetMaxHeight(this._ygNode, value.Value);
				}
			}
		}

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x06000AED RID: 2797 RVA: 0x000234D8 File Offset: 0x000216D8
		// (set) Token: 0x06000AEE RID: 2798 RVA: 0x000234FD File Offset: 0x000216FD
		public YogaValue MinWidth
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMinWidth(this._ygNode));
			}
			set
			{
				if (value.Unit == YogaUnit.Percent)
				{
					Native.YGNodeStyleSetMinWidthPercent(this._ygNode, value.Value);
				}
				else
				{
					Native.YGNodeStyleSetMinWidth(this._ygNode, value.Value);
				}
			}
		}

		// Token: 0x170002CF RID: 719
		// (get) Token: 0x06000AEF RID: 2799 RVA: 0x0002353C File Offset: 0x0002173C
		// (set) Token: 0x06000AF0 RID: 2800 RVA: 0x00023561 File Offset: 0x00021761
		public YogaValue MinHeight
		{
			get
			{
				return YogaValue.MarshalValue(Native.YGNodeStyleGetMinHeight(this._ygNode));
			}
			set
			{
				if (value.Unit == YogaUnit.Percent)
				{
					Native.YGNodeStyleSetMinHeightPercent(this._ygNode, value.Value);
				}
				else
				{
					Native.YGNodeStyleSetMinHeight(this._ygNode, value.Value);
				}
			}
		}

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06000AF1 RID: 2801 RVA: 0x000235A0 File Offset: 0x000217A0
		// (set) Token: 0x06000AF2 RID: 2802 RVA: 0x000235C0 File Offset: 0x000217C0
		public float AspectRatio
		{
			get
			{
				return Native.YGNodeStyleGetAspectRatio(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetAspectRatio(this._ygNode, value);
			}
		}

		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06000AF3 RID: 2803 RVA: 0x000235D0 File Offset: 0x000217D0
		public float LayoutX
		{
			get
			{
				return Native.YGNodeLayoutGetLeft(this._ygNode);
			}
		}

		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x06000AF4 RID: 2804 RVA: 0x000235F0 File Offset: 0x000217F0
		public float LayoutY
		{
			get
			{
				return Native.YGNodeLayoutGetTop(this._ygNode);
			}
		}

		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x06000AF5 RID: 2805 RVA: 0x00023610 File Offset: 0x00021810
		public float LayoutWidth
		{
			get
			{
				return Native.YGNodeLayoutGetWidth(this._ygNode);
			}
		}

		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x06000AF6 RID: 2806 RVA: 0x00023630 File Offset: 0x00021830
		public float LayoutHeight
		{
			get
			{
				return Native.YGNodeLayoutGetHeight(this._ygNode);
			}
		}

		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x06000AF7 RID: 2807 RVA: 0x00023650 File Offset: 0x00021850
		public YogaDirection LayoutDirection
		{
			get
			{
				return Native.YGNodeLayoutGetDirection(this._ygNode);
			}
		}

		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x06000AF8 RID: 2808 RVA: 0x00023670 File Offset: 0x00021870
		// (set) Token: 0x06000AF9 RID: 2809 RVA: 0x00023690 File Offset: 0x00021890
		public YogaOverflow Overflow
		{
			get
			{
				return Native.YGNodeStyleGetOverflow(this._ygNode);
			}
			set
			{
				Native.YGNodeStyleSetOverflow(this._ygNode, value);
			}
		}

		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x06000AFA RID: 2810 RVA: 0x000236A0 File Offset: 0x000218A0
		// (set) Token: 0x06000AFB RID: 2811 RVA: 0x000236BB File Offset: 0x000218BB
		public object Data
		{
			get
			{
				return this._data;
			}
			set
			{
				this._data = value;
			}
		}

		// Token: 0x170002D8 RID: 728
		public YogaNode this[int index]
		{
			get
			{
				return this._children[index];
			}
		}

		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x06000AFD RID: 2813 RVA: 0x000236EC File Offset: 0x000218EC
		public int Count
		{
			get
			{
				return (this._children == null) ? 0 : this._children.Count;
			}
		}

		// Token: 0x06000AFE RID: 2814 RVA: 0x0002371D File Offset: 0x0002191D
		public void MarkLayoutSeen()
		{
			Native.YGNodeSetHasNewLayout(this._ygNode, false);
		}

		// Token: 0x06000AFF RID: 2815 RVA: 0x0002372C File Offset: 0x0002192C
		public bool ValuesEqual(float f1, float f2)
		{
			bool result;
			if (float.IsNaN(f1) || float.IsNaN(f2))
			{
				result = (float.IsNaN(f1) && float.IsNaN(f2));
			}
			else
			{
				result = (Math.Abs(f2 - f1) < float.Epsilon);
			}
			return result;
		}

		// Token: 0x06000B00 RID: 2816 RVA: 0x00023784 File Offset: 0x00021984
		public void Insert(int index, YogaNode node)
		{
			if (this._children == null)
			{
				this._children = new List<YogaNode>(4);
			}
			this._children.Insert(index, node);
			node._parent = new WeakReference(this);
			Native.YGNodeInsertChild(this._ygNode, node._ygNode, (uint)index);
		}

		// Token: 0x06000B01 RID: 2817 RVA: 0x000237D8 File Offset: 0x000219D8
		public void RemoveAt(int index)
		{
			YogaNode yogaNode = this._children[index];
			yogaNode._parent = null;
			this._children.RemoveAt(index);
			Native.YGNodeRemoveChild(this._ygNode, yogaNode._ygNode);
		}

		// Token: 0x06000B02 RID: 2818 RVA: 0x00023817 File Offset: 0x00021A17
		public void AddChild(YogaNode child)
		{
			this.Insert(this.Count, child);
		}

		// Token: 0x06000B03 RID: 2819 RVA: 0x00023828 File Offset: 0x00021A28
		public void RemoveChild(YogaNode child)
		{
			int num = this.IndexOf(child);
			if (num >= 0)
			{
				this.RemoveAt(num);
			}
		}

		// Token: 0x06000B04 RID: 2820 RVA: 0x0002384E File Offset: 0x00021A4E
		public void Clear()
		{
			if (this._children != null)
			{
				while (this._children.Count > 0)
				{
					this.RemoveAt(this._children.Count - 1);
				}
			}
		}

		// Token: 0x06000B05 RID: 2821 RVA: 0x0002388C File Offset: 0x00021A8C
		public int IndexOf(YogaNode node)
		{
			return (this._children == null) ? -1 : this._children.IndexOf(node);
		}

		// Token: 0x06000B06 RID: 2822 RVA: 0x000238C0 File Offset: 0x00021AC0
		public void SetMeasureFunction(MeasureFunction measureFunction)
		{
			this._measureFunction = measureFunction;
			if (measureFunction == null)
			{
				if (!this.IsBaselineDefined)
				{
					Native.YGSetManagedObject(this._ygNode, null);
				}
				Native.YGNodeRemoveMeasureFunc(this._ygNode);
			}
			else
			{
				Native.YGSetManagedObject(this._ygNode, this);
				Native.YGNodeSetMeasureFunc(this._ygNode);
			}
		}

		// Token: 0x06000B07 RID: 2823 RVA: 0x00023920 File Offset: 0x00021B20
		public void SetBaselineFunction(BaselineFunction baselineFunction)
		{
			this._baselineFunction = baselineFunction;
			if (baselineFunction == null)
			{
				if (!this.IsMeasureDefined)
				{
					Native.YGSetManagedObject(this._ygNode, null);
				}
				Native.YGNodeRemoveBaselineFunc(this._ygNode);
			}
			else
			{
				Native.YGSetManagedObject(this._ygNode, this);
				Native.YGNodeSetBaselineFunc(this._ygNode);
			}
		}

		// Token: 0x06000B08 RID: 2824 RVA: 0x0002397F File Offset: 0x00021B7F
		public void CalculateLayout(float width = float.NaN, float height = float.NaN)
		{
			Native.YGNodeCalculateLayout(this._ygNode, width, height, Native.YGNodeStyleGetDirection(this._ygNode));
		}

		// Token: 0x06000B09 RID: 2825 RVA: 0x0002399C File Offset: 0x00021B9C
		public static YogaSize MeasureInternal(YogaNode node, float width, YogaMeasureMode widthMode, float height, YogaMeasureMode heightMode)
		{
			if (node == null || node._measureFunction == null)
			{
				throw new InvalidOperationException("Measure function is not defined.");
			}
			return node._measureFunction(node, width, widthMode, height, heightMode);
		}

		// Token: 0x06000B0A RID: 2826 RVA: 0x000239E0 File Offset: 0x00021BE0
		public static float BaselineInternal(YogaNode node, float width, float height)
		{
			if (node == null || node._baselineFunction == null)
			{
				throw new InvalidOperationException("Baseline function is not defined.");
			}
			return node._baselineFunction(node, width, height);
		}

		// Token: 0x06000B0B RID: 2827 RVA: 0x00023A20 File Offset: 0x00021C20
		public string Print(YogaPrintOptions options = YogaPrintOptions.Layout | YogaPrintOptions.Style | YogaPrintOptions.Children)
		{
			StringBuilder sb = new StringBuilder();
			Logger logger = this._config.Logger;
			this._config.Logger = delegate(YogaConfig config, YogaNode node, YogaLogLevel level, string message)
			{
				sb.Append(message);
			};
			Native.YGNodePrint(this._ygNode, options);
			this._config.Logger = logger;
			return sb.ToString();
		}

		// Token: 0x06000B0C RID: 2828 RVA: 0x00023A8C File Offset: 0x00021C8C
		public IEnumerator<YogaNode> GetEnumerator()
		{
			return (this._children == null) ? Enumerable.Empty<YogaNode>().GetEnumerator() : ((IEnumerable<YogaNode>)this._children).GetEnumerator();
		}

		// Token: 0x06000B0D RID: 2829 RVA: 0x00023AC8 File Offset: 0x00021CC8
		IEnumerator IEnumerable.GetEnumerator()
		{
			return (this._children == null) ? Enumerable.Empty<YogaNode>().GetEnumerator() : ((IEnumerable<YogaNode>)this._children).GetEnumerator();
		}

		// Token: 0x06000B0E RID: 2830 RVA: 0x00023B04 File Offset: 0x00021D04
		public static int GetInstanceCount()
		{
			return Native.YGNodeGetInstanceCount();
		}

		// Token: 0x040004A5 RID: 1189
		internal IntPtr _ygNode;

		// Token: 0x040004A6 RID: 1190
		private readonly YogaConfig _config;

		// Token: 0x040004A7 RID: 1191
		private WeakReference _parent;

		// Token: 0x040004A8 RID: 1192
		private List<YogaNode> _children;

		// Token: 0x040004A9 RID: 1193
		private MeasureFunction _measureFunction;

		// Token: 0x040004AA RID: 1194
		private BaselineFunction _baselineFunction;

		// Token: 0x040004AB RID: 1195
		private object _data;

		// Token: 0x02000196 RID: 406
		[CompilerGenerated]
		private sealed class <Print>c__AnonStorey0
		{
			// Token: 0x06000B0F RID: 2831 RVA: 0x00002223 File Offset: 0x00000423
			public <Print>c__AnonStorey0()
			{
			}

			// Token: 0x06000B10 RID: 2832 RVA: 0x00023B1E File Offset: 0x00021D1E
			internal void <>m__0(YogaConfig config, YogaNode node, YogaLogLevel level, string message)
			{
				this.sb.Append(message);
			}

			// Token: 0x040004AC RID: 1196
			internal StringBuilder sb;
		}
	}
}
