﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x0200019B RID: 411
	internal struct YogaSize
	{
		// Token: 0x040004BB RID: 1211
		public float width;

		// Token: 0x040004BC RID: 1212
		public float height;
	}
}
