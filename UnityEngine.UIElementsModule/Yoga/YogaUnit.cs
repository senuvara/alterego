﻿using System;

namespace UnityEngine.Yoga
{
	// Token: 0x0200019C RID: 412
	internal enum YogaUnit
	{
		// Token: 0x040004BE RID: 1214
		Undefined,
		// Token: 0x040004BF RID: 1215
		Point,
		// Token: 0x040004C0 RID: 1216
		Percent,
		// Token: 0x040004C1 RID: 1217
		Auto
	}
}
