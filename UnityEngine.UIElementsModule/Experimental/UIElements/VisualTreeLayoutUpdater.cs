﻿using System;
using UnityEngine.Yoga;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000DB RID: 219
	internal class VisualTreeLayoutUpdater : BaseVisualTreeUpdater
	{
		// Token: 0x0600064E RID: 1614 RVA: 0x000156AC File Offset: 0x000138AC
		public VisualTreeLayoutUpdater()
		{
		}

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x0600064F RID: 1615 RVA: 0x000156B4 File Offset: 0x000138B4
		public override string description
		{
			get
			{
				return "Update Layout";
			}
		}

		// Token: 0x06000650 RID: 1616 RVA: 0x000156D0 File Offset: 0x000138D0
		public override void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType)
		{
			if ((versionChangeType & (VersionChangeType.Hierarchy | VersionChangeType.Layout)) != (VersionChangeType)0)
			{
				YogaNode yogaNode = ve.yogaNode;
				if (yogaNode != null && yogaNode.IsMeasureDefined)
				{
					yogaNode.MarkDirty();
				}
			}
		}

		// Token: 0x06000651 RID: 1617 RVA: 0x0001570C File Offset: 0x0001390C
		public override void Update()
		{
			int num = 0;
			while (base.visualTree.yogaNode.IsDirty)
			{
				if (num > 0)
				{
					base.panel.ApplyStyles();
				}
				base.visualTree.yogaNode.CalculateLayout(float.NaN, float.NaN);
				using (new EventDispatcher.Gate(base.visualTree.panel.dispatcher))
				{
					this.UpdateSubTree(base.visualTree);
				}
				if (num++ >= 5)
				{
					Debug.LogError("Layout update is struggling to process current layout (consider simplifying to avoid recursive layout): " + base.visualTree);
					break;
				}
			}
		}

		// Token: 0x06000652 RID: 1618 RVA: 0x000157D4 File Offset: 0x000139D4
		private void UpdateSubTree(VisualElement root)
		{
			Rect rect = new Rect(root.yogaNode.LayoutX, root.yogaNode.LayoutY, root.yogaNode.LayoutWidth, root.yogaNode.LayoutHeight);
			Rect lastLayout = root.renderData.lastLayout;
			bool flag = lastLayout != rect;
			if (flag)
			{
				if (rect.position != lastLayout.position)
				{
					root.IncrementVersion(VersionChangeType.Transform);
				}
				root.IncrementVersion(VersionChangeType.Clip);
				root.renderData.lastLayout = rect;
			}
			bool hasNewLayout = root.yogaNode.HasNewLayout;
			if (hasNewLayout)
			{
				for (int i = 0; i < root.shadow.childCount; i++)
				{
					this.UpdateSubTree(root.shadow[i]);
				}
			}
			if (flag)
			{
				using (GeometryChangedEvent pooled = GeometryChangedEvent.GetPooled(lastLayout, rect))
				{
					pooled.target = root;
					root.SendEvent(pooled);
				}
			}
			if (hasNewLayout)
			{
				root.yogaNode.MarkLayoutSeen();
				root.IncrementVersion(VersionChangeType.Repaint);
			}
		}

		// Token: 0x04000279 RID: 633
		private const int kMaxValidateLayoutCount = 5;
	}
}
