﻿using System;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000A9 RID: 169
	internal struct RuleMatcher
	{
		// Token: 0x0600041A RID: 1050 RVA: 0x0000E33D File Offset: 0x0000C53D
		public RuleMatcher(StyleSheet sheet, StyleComplexSelector complexSelector, int styleSheetIndexInStack)
		{
			this.sheet = sheet;
			this.complexSelector = complexSelector;
		}

		// Token: 0x0600041B RID: 1051 RVA: 0x0000E350 File Offset: 0x0000C550
		public override string ToString()
		{
			return this.complexSelector.ToString();
		}

		// Token: 0x040001EF RID: 495
		public StyleSheet sheet;

		// Token: 0x040001F0 RID: 496
		public StyleComplexSelector complexSelector;
	}
}
