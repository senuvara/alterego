﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000BD RID: 189
	public enum PickingMode
	{
		// Token: 0x04000216 RID: 534
		Position,
		// Token: 0x04000217 RID: 535
		Ignore
	}
}
