﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200006E RID: 110
	public class RepeatButton : TextElement
	{
		// Token: 0x06000309 RID: 777 RVA: 0x0000AA65 File Offset: 0x00008C65
		public RepeatButton()
		{
		}

		// Token: 0x0600030A RID: 778 RVA: 0x0000AA6E File Offset: 0x00008C6E
		public RepeatButton(Action clickEvent, long delay, long interval)
		{
			this.SetAction(clickEvent, delay, interval);
		}

		// Token: 0x0600030B RID: 779 RVA: 0x0000AA80 File Offset: 0x00008C80
		public void SetAction(Action clickEvent, long delay, long interval)
		{
			this.RemoveManipulator(this.m_Clickable);
			this.m_Clickable = new Clickable(clickEvent, delay, interval);
			this.AddManipulator(this.m_Clickable);
		}

		// Token: 0x0400012E RID: 302
		private Clickable m_Clickable;

		// Token: 0x0200006F RID: 111
		public new class UxmlFactory : UxmlFactory<RepeatButton, RepeatButton.UxmlTraits>
		{
			// Token: 0x0600030C RID: 780 RVA: 0x0000AAA9 File Offset: 0x00008CA9
			public UxmlFactory()
			{
			}
		}

		// Token: 0x02000070 RID: 112
		public new class UxmlTraits : TextElement.UxmlTraits
		{
			// Token: 0x0600030D RID: 781 RVA: 0x0000AAB4 File Offset: 0x00008CB4
			public UxmlTraits()
			{
			}

			// Token: 0x0600030E RID: 782 RVA: 0x0000AAF8 File Offset: 0x00008CF8
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				RepeatButton repeatButton = (RepeatButton)ve;
				repeatButton.SetAction(null, this.m_Delay.GetValueFromBag(bag, cc), this.m_Interval.GetValueFromBag(bag, cc));
			}

			// Token: 0x0400012F RID: 303
			private UxmlLongAttributeDescription m_Delay = new UxmlLongAttributeDescription
			{
				name = "delay"
			};

			// Token: 0x04000130 RID: 304
			private UxmlLongAttributeDescription m_Interval = new UxmlLongAttributeDescription
			{
				name = "interval"
			};
		}
	}
}
