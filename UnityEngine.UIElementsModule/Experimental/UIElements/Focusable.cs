﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000030 RID: 48
	public abstract class Focusable : CallbackEventHandler
	{
		// Token: 0x060000D4 RID: 212 RVA: 0x000053C0 File Offset: 0x000035C0
		protected Focusable()
		{
			this.m_FocusIndex = 0;
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000D5 RID: 213
		public abstract FocusController focusController { get; }

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000D6 RID: 214 RVA: 0x000053D0 File Offset: 0x000035D0
		// (set) Token: 0x060000D7 RID: 215 RVA: 0x000053EB File Offset: 0x000035EB
		public virtual int focusIndex
		{
			get
			{
				return this.m_FocusIndex;
			}
			set
			{
				this.m_FocusIndex = value;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000D8 RID: 216 RVA: 0x000053F8 File Offset: 0x000035F8
		public virtual bool canGrabFocus
		{
			get
			{
				return this.m_FocusIndex >= 0;
			}
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x00005419 File Offset: 0x00003619
		public virtual void Focus()
		{
			if (this.focusController != null)
			{
				this.focusController.SwitchFocus((!this.canGrabFocus) ? null : this);
			}
		}

		// Token: 0x060000DA RID: 218 RVA: 0x00005446 File Offset: 0x00003646
		public virtual void Blur()
		{
			if (this.focusController != null && this.focusController.focusedElement == this)
			{
				this.focusController.SwitchFocus(null);
			}
		}

		// Token: 0x060000DB RID: 219 RVA: 0x00005473 File Offset: 0x00003673
		protected internal override void ExecuteDefaultAction(EventBase evt)
		{
			base.ExecuteDefaultAction(evt);
			if (evt.GetEventTypeId() == EventBase<MouseDownEvent>.TypeId())
			{
				this.Focus();
			}
			if (this.focusController != null)
			{
				this.focusController.SwitchFocusOnEvent(evt);
			}
		}

		// Token: 0x0400006F RID: 111
		private int m_FocusIndex;
	}
}
