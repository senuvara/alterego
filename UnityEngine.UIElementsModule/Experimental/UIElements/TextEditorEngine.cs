﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000F3 RID: 243
	internal class TextEditorEngine : TextEditor
	{
		// Token: 0x060006D0 RID: 1744 RVA: 0x00017DCF File Offset: 0x00015FCF
		public TextEditorEngine(TextEditorEngine.OnDetectFocusChangeFunction detectFocusChange, TextEditorEngine.OnIndexChangeFunction indexChangeFunction)
		{
			this.m_DetectFocusChangeFunction = detectFocusChange;
			this.m_IndexChangeFunction = indexChangeFunction;
		}

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x060006D1 RID: 1745 RVA: 0x00017DE8 File Offset: 0x00015FE8
		internal override Rect localPosition
		{
			get
			{
				return new Rect(0f, 0f, base.position.width, base.position.height);
			}
		}

		// Token: 0x060006D2 RID: 1746 RVA: 0x00017E28 File Offset: 0x00016028
		internal override void OnDetectFocusChange()
		{
			this.m_DetectFocusChangeFunction();
		}

		// Token: 0x060006D3 RID: 1747 RVA: 0x00017E36 File Offset: 0x00016036
		internal override void OnCursorIndexChange()
		{
			this.m_IndexChangeFunction();
		}

		// Token: 0x060006D4 RID: 1748 RVA: 0x00017E36 File Offset: 0x00016036
		internal override void OnSelectIndexChange()
		{
			this.m_IndexChangeFunction();
		}

		// Token: 0x040002B6 RID: 694
		private TextEditorEngine.OnDetectFocusChangeFunction m_DetectFocusChangeFunction;

		// Token: 0x040002B7 RID: 695
		private TextEditorEngine.OnIndexChangeFunction m_IndexChangeFunction;

		// Token: 0x020000F4 RID: 244
		// (Invoke) Token: 0x060006D6 RID: 1750
		internal delegate void OnDetectFocusChangeFunction();

		// Token: 0x020000F5 RID: 245
		// (Invoke) Token: 0x060006DA RID: 1754
		internal delegate void OnIndexChangeFunction();
	}
}
