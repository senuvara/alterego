﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000071 RID: 113
	public struct TimerState
	{
		// Token: 0x170000DD RID: 221
		// (get) Token: 0x0600030F RID: 783 RVA: 0x0000AB38 File Offset: 0x00008D38
		public long deltaTime
		{
			get
			{
				return this.now - this.start;
			}
		}

		// Token: 0x04000131 RID: 305
		public long start;

		// Token: 0x04000132 RID: 306
		public long now;
	}
}
