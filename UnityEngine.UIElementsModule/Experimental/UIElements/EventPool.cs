﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000124 RID: 292
	internal class EventPool<T> where T : EventBase<T>, new()
	{
		// Token: 0x060007F7 RID: 2039 RVA: 0x0001B1B7 File Offset: 0x000193B7
		public EventPool()
		{
		}

		// Token: 0x060007F8 RID: 2040 RVA: 0x0001B1CC File Offset: 0x000193CC
		public T Get()
		{
			return (this.m_Stack.Count != 0) ? this.m_Stack.Pop() : Activator.CreateInstance<T>();
		}

		// Token: 0x060007F9 RID: 2041 RVA: 0x0001B208 File Offset: 0x00019408
		public void Release(T element)
		{
			if (this.m_Stack.Contains(element))
			{
				Debug.LogError("Internal error. Trying to destroy object that is already released to pool.");
			}
			this.m_Stack.Push(element);
		}

		// Token: 0x04000310 RID: 784
		private readonly Stack<T> m_Stack = new Stack<T>();
	}
}
