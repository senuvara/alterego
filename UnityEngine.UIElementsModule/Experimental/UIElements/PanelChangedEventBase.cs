﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000141 RID: 321
	public abstract class PanelChangedEventBase<T> : EventBase<T>, IPanelChangedEvent, IPropagatableEvent where T : PanelChangedEventBase<T>, new()
	{
		// Token: 0x06000874 RID: 2164 RVA: 0x00019AB0 File Offset: 0x00017CB0
		protected PanelChangedEventBase()
		{
			this.Init();
		}

		// Token: 0x17000231 RID: 561
		// (get) Token: 0x06000875 RID: 2165 RVA: 0x0001BDB4 File Offset: 0x00019FB4
		// (set) Token: 0x06000876 RID: 2166 RVA: 0x0001BDCE File Offset: 0x00019FCE
		public IPanel originPanel
		{
			[CompilerGenerated]
			get
			{
				return this.<originPanel>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<originPanel>k__BackingField = value;
			}
		}

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x06000877 RID: 2167 RVA: 0x0001BDD8 File Offset: 0x00019FD8
		// (set) Token: 0x06000878 RID: 2168 RVA: 0x0001BDF2 File Offset: 0x00019FF2
		public IPanel destinationPanel
		{
			[CompilerGenerated]
			get
			{
				return this.<destinationPanel>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<destinationPanel>k__BackingField = value;
			}
		}

		// Token: 0x06000879 RID: 2169 RVA: 0x0001BDFB File Offset: 0x00019FFB
		protected override void Init()
		{
			base.Init();
			this.originPanel = null;
			this.destinationPanel = null;
		}

		// Token: 0x0600087A RID: 2170 RVA: 0x0001BE14 File Offset: 0x0001A014
		public static T GetPooled(IPanel originPanel, IPanel destinationPanel)
		{
			T pooled = EventBase<T>.GetPooled();
			pooled.originPanel = originPanel;
			pooled.destinationPanel = destinationPanel;
			return pooled;
		}

		// Token: 0x04000326 RID: 806
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private IPanel <originPanel>k__BackingField;

		// Token: 0x04000327 RID: 807
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private IPanel <destinationPanel>k__BackingField;
	}
}
