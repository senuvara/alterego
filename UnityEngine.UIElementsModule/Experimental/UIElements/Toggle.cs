﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000FC RID: 252
	public class Toggle : BaseField<bool>
	{
		// Token: 0x0600072A RID: 1834 RVA: 0x000191A8 File Offset: 0x000173A8
		public Toggle()
		{
			VisualElement child = new VisualElement
			{
				name = "Checkmark",
				pickingMode = PickingMode.Ignore
			};
			base.Add(child);
			this.text = null;
			this.AddManipulator(new Clickable(new Action(this.OnClick)));
		}

		// Token: 0x0600072B RID: 1835 RVA: 0x000191FB File Offset: 0x000173FB
		[Obsolete("Use Toggle() with OnValueChanged() instead.", false)]
		public Toggle(Action clickEvent) : this()
		{
			this.OnToggle(clickEvent);
		}

		// Token: 0x170001DA RID: 474
		// (get) Token: 0x0600072C RID: 1836 RVA: 0x0001920C File Offset: 0x0001740C
		// (set) Token: 0x0600072D RID: 1837 RVA: 0x0001923C File Offset: 0x0001743C
		public string text
		{
			get
			{
				return (this.m_Label != null) ? this.m_Label.text : null;
			}
			set
			{
				if (!string.IsNullOrEmpty(value))
				{
					if (this.m_Label == null)
					{
						this.m_Label = new Label();
						this.m_Label.pickingMode = PickingMode.Ignore;
						base.Add(this.m_Label);
					}
					this.m_Label.text = value;
				}
				else if (this.m_Label != null)
				{
					base.Remove(this.m_Label);
					this.m_Label = null;
				}
			}
		}

		// Token: 0x0600072E RID: 1838 RVA: 0x000192B8 File Offset: 0x000174B8
		public override void SetValueWithoutNotify(bool newValue)
		{
			if (newValue)
			{
				base.pseudoStates |= PseudoStates.Checked;
			}
			else
			{
				base.pseudoStates &= ~PseudoStates.Checked;
			}
			base.SetValueWithoutNotify(newValue);
		}

		// Token: 0x0600072F RID: 1839 RVA: 0x000192F0 File Offset: 0x000174F0
		[Obsolete("Use OnValueChanged() instead.", false)]
		public void OnToggle(Action clickEvent)
		{
			if (clickEvent != null && this.m_ClickEvent == null)
			{
				base.OnValueChanged(new EventCallback<ChangeEvent<bool>>(this.InternalOnValueChanged));
			}
			else if (clickEvent == null && this.m_ClickEvent != null)
			{
				base.UnregisterCallback<ChangeEvent<bool>>(new EventCallback<ChangeEvent<bool>>(this.InternalOnValueChanged), TrickleDown.NoTrickleDown);
			}
			this.m_ClickEvent = clickEvent;
		}

		// Token: 0x06000730 RID: 1840 RVA: 0x00019355 File Offset: 0x00017555
		private void InternalOnValueChanged(ChangeEvent<bool> evt)
		{
			if (this.m_ClickEvent != null)
			{
				this.m_ClickEvent();
			}
		}

		// Token: 0x06000731 RID: 1841 RVA: 0x00019370 File Offset: 0x00017570
		private void OnClick()
		{
			this.value = !this.value;
		}

		// Token: 0x06000732 RID: 1842 RVA: 0x00019384 File Offset: 0x00017584
		protected internal override void ExecuteDefaultActionAtTarget(EventBase evt)
		{
			base.ExecuteDefaultActionAtTarget(evt);
			KeyDownEvent keyDownEvent = evt as KeyDownEvent;
			char? c = (keyDownEvent != null) ? new char?(keyDownEvent.character) : null;
			if (!(((c == null) ? null : new int?((int)c.Value)) == 10))
			{
				KeyDownEvent keyDownEvent2 = evt as KeyDownEvent;
				char? c2 = (keyDownEvent2 != null) ? new char?(keyDownEvent2.character) : null;
				if (!(((c2 == null) ? null : new int?((int)c2.Value)) == 32))
				{
					return;
				}
			}
			this.OnClick();
			evt.StopPropagation();
		}

		// Token: 0x040002CC RID: 716
		private Action m_ClickEvent;

		// Token: 0x040002CD RID: 717
		private Label m_Label;

		// Token: 0x020000FD RID: 253
		public new class UxmlFactory : UxmlFactory<Toggle, Toggle.UxmlTraits>
		{
			// Token: 0x06000733 RID: 1843 RVA: 0x0001946F File Offset: 0x0001766F
			public UxmlFactory()
			{
			}
		}

		// Token: 0x020000FE RID: 254
		public new class UxmlTraits : BaseField<bool>.UxmlTraits
		{
			// Token: 0x06000734 RID: 1844 RVA: 0x00019478 File Offset: 0x00017678
			public UxmlTraits()
			{
			}

			// Token: 0x06000735 RID: 1845 RVA: 0x000194BB File Offset: 0x000176BB
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				((Toggle)ve).text = this.m_Label.GetValueFromBag(bag, cc);
				((Toggle)ve).SetValueWithoutNotify(this.m_Value.GetValueFromBag(bag, cc));
			}

			// Token: 0x040002CE RID: 718
			private UxmlStringAttributeDescription m_Label = new UxmlStringAttributeDescription
			{
				name = "label"
			};

			// Token: 0x040002CF RID: 719
			private UxmlBoolAttributeDescription m_Value = new UxmlBoolAttributeDescription
			{
				name = "value"
			};
		}
	}
}
