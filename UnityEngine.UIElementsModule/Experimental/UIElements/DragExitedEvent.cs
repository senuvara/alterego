﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200010D RID: 269
	public class DragExitedEvent : DragAndDropEventBase<DragExitedEvent>
	{
		// Token: 0x0600075F RID: 1887 RVA: 0x00019CD6 File Offset: 0x00017ED6
		public DragExitedEvent()
		{
		}

		// Token: 0x06000760 RID: 1888 RVA: 0x00019CDF File Offset: 0x00017EDF
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown);
		}
	}
}
