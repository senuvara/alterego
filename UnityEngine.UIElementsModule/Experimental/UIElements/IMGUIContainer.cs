﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Experimental.UIElements.StyleEnums;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000039 RID: 57
	public class IMGUIContainer : VisualElement
	{
		// Token: 0x06000109 RID: 265 RVA: 0x00005AEB File Offset: 0x00003CEB
		public IMGUIContainer() : this(null)
		{
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00005AF8 File Offset: 0x00003CF8
		public IMGUIContainer(Action onGUIHandler)
		{
			this.m_OnGUIHandler = onGUIHandler;
			this.contextType = ContextType.Editor;
			this.focusIndex = 0;
			base.requireMeasureFunction = true;
			base.style.overflow = Overflow.Hidden;
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600010B RID: 267 RVA: 0x00005B68 File Offset: 0x00003D68
		internal ObjectGUIState guiState
		{
			get
			{
				Debug.Assert(!this.useOwnerObjectGUIState);
				if (this.m_ObjectGUIState == null)
				{
					this.m_ObjectGUIState = new ObjectGUIState();
				}
				return this.m_ObjectGUIState;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x0600010C RID: 268 RVA: 0x00005BAC File Offset: 0x00003DAC
		// (set) Token: 0x0600010D RID: 269 RVA: 0x00005BC6 File Offset: 0x00003DC6
		internal Rect lastWorldClip
		{
			[CompilerGenerated]
			get
			{
				return this.<lastWorldClip>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<lastWorldClip>k__BackingField = value;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x0600010E RID: 270 RVA: 0x00005BD0 File Offset: 0x00003DD0
		private GUILayoutUtility.LayoutCache cache
		{
			get
			{
				if (this.m_Cache == null)
				{
					this.m_Cache = new GUILayoutUtility.LayoutCache();
				}
				return this.m_Cache;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x0600010F RID: 271 RVA: 0x00005C04 File Offset: 0x00003E04
		// (set) Token: 0x06000110 RID: 272 RVA: 0x00005C1E File Offset: 0x00003E1E
		public ContextType contextType
		{
			[CompilerGenerated]
			get
			{
				return this.<contextType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<contextType>k__BackingField = value;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000111 RID: 273 RVA: 0x00005C28 File Offset: 0x00003E28
		public override bool canGrabFocus
		{
			get
			{
				return base.canGrabFocus && this.hasFocusableControls;
			}
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00005C54 File Offset: 0x00003E54
		protected override void DoRepaint(IStylePainter painter)
		{
			this.lastWorldClip = base.elementPanel.repaintData.currentWorldClip;
			IStylePainterInternal stylePainterInternal = (IStylePainterInternal)painter;
			stylePainterInternal.DrawImmediate(new Action(this.HandleIMGUIEvent));
		}

		// Token: 0x06000113 RID: 275 RVA: 0x00005C94 File Offset: 0x00003E94
		private void SaveGlobals()
		{
			this.m_GUIGlobals.matrix = GUI.matrix;
			this.m_GUIGlobals.color = GUI.color;
			this.m_GUIGlobals.contentColor = GUI.contentColor;
			this.m_GUIGlobals.backgroundColor = GUI.backgroundColor;
			this.m_GUIGlobals.enabled = GUI.enabled;
			this.m_GUIGlobals.changed = GUI.changed;
			if (Event.current != null)
			{
				this.m_GUIGlobals.displayIndex = Event.current.displayIndex;
			}
		}

		// Token: 0x06000114 RID: 276 RVA: 0x00005D24 File Offset: 0x00003F24
		private void RestoreGlobals()
		{
			GUI.matrix = this.m_GUIGlobals.matrix;
			GUI.color = this.m_GUIGlobals.color;
			GUI.contentColor = this.m_GUIGlobals.contentColor;
			GUI.backgroundColor = this.m_GUIGlobals.backgroundColor;
			GUI.enabled = this.m_GUIGlobals.enabled;
			GUI.changed = this.m_GUIGlobals.changed;
			if (Event.current != null)
			{
				Event.current.displayIndex = this.m_GUIGlobals.displayIndex;
			}
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00005DB4 File Offset: 0x00003FB4
		private void DoOnGUI(Event evt, Matrix4x4 worldTransform, Rect clippingRect, bool isComputingLayout = false)
		{
			if (this.m_OnGUIHandler != null && base.panel != null)
			{
				int num = GUIClip.Internal_GetCount();
				this.SaveGlobals();
				UIElementsUtility.BeginContainerGUI(this.cache, evt, this);
				GUI.color = UIElementsUtility.editorPlayModeTintColor;
				if (Event.current.type != EventType.Layout)
				{
					if (this.lostFocus)
					{
						if (this.focusController != null)
						{
							if (this.focusController.focusedElement == null || this.focusController.focusedElement == this || !(this.focusController.focusedElement is IMGUIContainer) || this.useUIElementsFocusStyle)
							{
								GUIUtility.keyboardControl = 0;
								this.focusController.imguiKeyboardControl = 0;
							}
						}
						this.lostFocus = false;
					}
					if (this.receivedFocus)
					{
						if (this.focusChangeDirection != FocusChangeDirection.unspecified && this.focusChangeDirection != FocusChangeDirection.none)
						{
							if (this.focusChangeDirection == VisualElementFocusChangeDirection.left)
							{
								GUIUtility.SetKeyboardControlToLastControlId();
							}
							else if (this.focusChangeDirection == VisualElementFocusChangeDirection.right)
							{
								GUIUtility.SetKeyboardControlToFirstControlId();
							}
						}
						else if (this.useUIElementsFocusStyle)
						{
							if (this.focusController == null || this.focusController.imguiKeyboardControl == 0)
							{
								GUIUtility.SetKeyboardControlToFirstControlId();
							}
							else
							{
								GUIUtility.keyboardControl = this.focusController.imguiKeyboardControl;
							}
						}
						this.receivedFocus = false;
						this.focusChangeDirection = FocusChangeDirection.unspecified;
						if (this.focusController != null)
						{
							this.focusController.imguiKeyboardControl = GUIUtility.keyboardControl;
						}
					}
				}
				EventType type = Event.current.type;
				bool flag = false;
				try
				{
					if (!isComputingLayout)
					{
						using (new GUIClip.ParentClipScope(worldTransform, clippingRect))
						{
							this.m_OnGUIHandler();
						}
					}
					else
					{
						this.m_OnGUIHandler();
					}
				}
				catch (Exception exception)
				{
					if (type != EventType.Layout)
					{
						throw;
					}
					flag = GUIUtility.IsExitGUIException(exception);
					if (!flag)
					{
						Debug.LogException(exception);
					}
				}
				finally
				{
					if (Event.current.type != EventType.Layout)
					{
						int keyboardControl = GUIUtility.keyboardControl;
						int num2 = GUIUtility.CheckForTabEvent(Event.current);
						if (this.focusController != null)
						{
							if (num2 < 0)
							{
								Focusable focusedElement = this.focusController.focusedElement;
								using (KeyDownEvent pooled = KeyboardEventBase<KeyDownEvent>.GetPooled('\t', KeyCode.Tab, (num2 != -1) ? EventModifiers.Shift : EventModifiers.None))
								{
									this.focusController.SwitchFocusOnEvent(pooled);
								}
								if (focusedElement == this)
								{
									if (this.focusController.focusedElement == this)
									{
										if (num2 == -2)
										{
											GUIUtility.SetKeyboardControlToLastControlId();
										}
										else if (num2 == -1)
										{
											GUIUtility.SetKeyboardControlToFirstControlId();
										}
										this.newKeyboardFocusControlID = GUIUtility.keyboardControl;
										this.focusController.imguiKeyboardControl = GUIUtility.keyboardControl;
									}
									else
									{
										GUIUtility.keyboardControl = 0;
										this.focusController.imguiKeyboardControl = 0;
									}
								}
							}
							else if (num2 > 0)
							{
								this.focusController.imguiKeyboardControl = GUIUtility.keyboardControl;
								this.newKeyboardFocusControlID = GUIUtility.keyboardControl;
							}
							else if (num2 == 0)
							{
								if (keyboardControl != GUIUtility.keyboardControl || type == EventType.MouseDown)
								{
									this.focusController.SyncIMGUIFocus(GUIUtility.keyboardControl, this);
								}
								else if (GUIUtility.keyboardControl != this.focusController.imguiKeyboardControl)
								{
									this.newKeyboardFocusControlID = GUIUtility.keyboardControl;
									if (this.focusController.focusedElement == this)
									{
										this.focusController.imguiKeyboardControl = GUIUtility.keyboardControl;
									}
									else
									{
										this.focusController.SyncIMGUIFocus(GUIUtility.keyboardControl, this);
									}
								}
							}
						}
						this.hasFocusableControls = GUIUtility.HasFocusableControls();
					}
				}
				UIElementsUtility.EndContainerGUI(evt);
				this.RestoreGlobals();
				if (!flag)
				{
					if (evt.type != EventType.Ignore && evt.type != EventType.Used)
					{
						int num3 = GUIClip.Internal_GetCount();
						if (num3 > num)
						{
							Debug.LogError("GUI Error: You are pushing more GUIClips than you are popping. Make sure they are balanced.");
						}
						else if (num3 < num)
						{
							Debug.LogError("GUI Error: You are popping more GUIClips than you are pushing. Make sure they are balanced.");
						}
					}
				}
				while (GUIClip.Internal_GetCount() > num)
				{
					GUIClip.Internal_Pop();
				}
				if (evt.type == EventType.Used)
				{
					base.IncrementVersion(VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x06000116 RID: 278 RVA: 0x000062A8 File Offset: 0x000044A8
		public void MarkDirtyLayout()
		{
			base.IncrementVersion(VersionChangeType.Layout);
		}

		// Token: 0x06000117 RID: 279 RVA: 0x000062B4 File Offset: 0x000044B4
		public override void HandleEvent(EventBase evt)
		{
			base.HandleEvent(evt);
			if (evt.propagationPhase != PropagationPhase.DefaultAction)
			{
				if (evt.imguiEvent != null)
				{
					if (!evt.isPropagationStopped)
					{
						if (this.HandleIMGUIEvent(evt.imguiEvent))
						{
							evt.StopPropagation();
							evt.PreventDefault();
						}
					}
				}
			}
		}

		// Token: 0x06000118 RID: 280 RVA: 0x0000631C File Offset: 0x0000451C
		internal void HandleIMGUIEvent()
		{
			Matrix4x4 currentOffset = base.elementPanel.repaintData.currentOffset;
			this.HandleIMGUIEvent(base.elementPanel.repaintData.repaintEvent, currentOffset * base.worldTransform, VisualElement.ComputeAAAlignedBound(base.worldClip, currentOffset));
		}

		// Token: 0x06000119 RID: 281 RVA: 0x0000636C File Offset: 0x0000456C
		internal bool HandleIMGUIEvent(Event e)
		{
			Matrix4x4 worldTransform;
			Rect clippingRect;
			IMGUIContainer.GetCurrentTransformAndClip(this, e, out worldTransform, out clippingRect);
			return this.HandleIMGUIEvent(e, worldTransform, clippingRect);
		}

		// Token: 0x0600011A RID: 282 RVA: 0x00006398 File Offset: 0x00004598
		internal bool HandleIMGUIEvent(Event e, Matrix4x4 worldTransform, Rect clippingRect)
		{
			bool result;
			if (e == null || this.m_OnGUIHandler == null || base.elementPanel == null || !base.elementPanel.IMGUIEventInterests.WantsEvent(e.type))
			{
				result = false;
			}
			else
			{
				EventType type = e.type;
				e.type = EventType.Layout;
				this.DoOnGUI(e, worldTransform, clippingRect, false);
				e.type = type;
				this.DoOnGUI(e, worldTransform, clippingRect, false);
				if (this.newKeyboardFocusControlID > 0)
				{
					this.newKeyboardFocusControlID = 0;
					this.HandleIMGUIEvent(new Event
					{
						type = EventType.ExecuteCommand,
						commandName = "NewKeyboardFocus"
					});
				}
				if (e.type == EventType.Used)
				{
					result = true;
				}
				else
				{
					if (e.type == EventType.MouseUp && this.HasMouseCapture())
					{
						GUIUtility.hotControl = 0;
					}
					if (base.elementPanel == null)
					{
						GUIUtility.ExitGUI();
					}
					result = false;
				}
			}
			return result;
		}

		// Token: 0x0600011B RID: 283 RVA: 0x00006498 File Offset: 0x00004698
		protected internal override void ExecuteDefaultAction(EventBase evt)
		{
			if (evt.GetEventTypeId() == EventBase<BlurEvent>.TypeId())
			{
				this.lostFocus = true;
			}
			else if (evt.GetEventTypeId() == EventBase<FocusEvent>.TypeId())
			{
				FocusEvent focusEvent = evt as FocusEvent;
				this.receivedFocus = true;
				this.focusChangeDirection = focusEvent.direction;
			}
			else if (evt.GetEventTypeId() == EventBase<DetachFromPanelEvent>.TypeId())
			{
				if (base.elementPanel != null)
				{
					base.elementPanel.IMGUIContainersCount--;
				}
			}
			else if (evt.GetEventTypeId() == EventBase<AttachToPanelEvent>.TypeId())
			{
				if (base.elementPanel != null)
				{
					base.elementPanel.IMGUIContainersCount++;
				}
			}
		}

		// Token: 0x0600011C RID: 284 RVA: 0x00006560 File Offset: 0x00004760
		protected internal override Vector2 DoMeasure(float desiredWidth, VisualElement.MeasureMode widthMode, float desiredHeight, VisualElement.MeasureMode heightMode)
		{
			float num = float.NaN;
			float num2 = float.NaN;
			if (widthMode != VisualElement.MeasureMode.Exactly || heightMode != VisualElement.MeasureMode.Exactly)
			{
				Event evt = new Event
				{
					type = EventType.Layout
				};
				this.DoOnGUI(evt, Matrix4x4.identity, Rect.zero, true);
				num = this.m_Cache.topLevel.minWidth;
				num2 = this.m_Cache.topLevel.minHeight;
			}
			if (widthMode != VisualElement.MeasureMode.Exactly)
			{
				if (widthMode == VisualElement.MeasureMode.AtMost)
				{
					num = Mathf.Min(num, desiredWidth);
				}
			}
			else
			{
				num = desiredWidth;
			}
			if (heightMode != VisualElement.MeasureMode.Exactly)
			{
				if (heightMode == VisualElement.MeasureMode.AtMost)
				{
					num2 = Mathf.Min(num2, desiredHeight);
				}
			}
			else
			{
				num2 = desiredHeight;
			}
			return new Vector2(num, num2);
		}

		// Token: 0x0600011D RID: 285 RVA: 0x00006630 File Offset: 0x00004830
		private static void GetCurrentTransformAndClip(IMGUIContainer container, Event evt, out Matrix4x4 transform, out Rect clipRect)
		{
			clipRect = container.lastWorldClip;
			if (clipRect.width == 0f || clipRect.height == 0f)
			{
				clipRect = container.worldBound;
			}
			transform = container.worldTransform;
			if (evt.type == EventType.Repaint && container.elementPanel != null)
			{
				transform = container.elementPanel.repaintData.currentOffset * container.worldTransform;
			}
		}

		// Token: 0x0400007C RID: 124
		private readonly Action m_OnGUIHandler;

		// Token: 0x0400007D RID: 125
		private ObjectGUIState m_ObjectGUIState;

		// Token: 0x0400007E RID: 126
		internal bool useOwnerObjectGUIState;

		// Token: 0x0400007F RID: 127
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Rect <lastWorldClip>k__BackingField;

		// Token: 0x04000080 RID: 128
		private GUILayoutUtility.LayoutCache m_Cache = null;

		// Token: 0x04000081 RID: 129
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ContextType <contextType>k__BackingField;

		// Token: 0x04000082 RID: 130
		internal bool useUIElementsFocusStyle;

		// Token: 0x04000083 RID: 131
		private bool lostFocus = false;

		// Token: 0x04000084 RID: 132
		private bool receivedFocus = false;

		// Token: 0x04000085 RID: 133
		private FocusChangeDirection focusChangeDirection = FocusChangeDirection.unspecified;

		// Token: 0x04000086 RID: 134
		private bool hasFocusableControls = false;

		// Token: 0x04000087 RID: 135
		private int newKeyboardFocusControlID = 0;

		// Token: 0x04000088 RID: 136
		private IMGUIContainer.GUIGlobals m_GUIGlobals;

		// Token: 0x0200003A RID: 58
		public new class UxmlFactory : UxmlFactory<IMGUIContainer, IMGUIContainer.UxmlTraits>
		{
			// Token: 0x0600011E RID: 286 RVA: 0x000066BE File Offset: 0x000048BE
			public UxmlFactory()
			{
			}
		}

		// Token: 0x0200003B RID: 59
		public new class UxmlTraits : VisualElement.UxmlTraits
		{
			// Token: 0x0600011F RID: 287 RVA: 0x000066C6 File Offset: 0x000048C6
			public UxmlTraits()
			{
				this.m_FocusIndex.defaultValue = 0;
			}

			// Token: 0x1700003B RID: 59
			// (get) Token: 0x06000120 RID: 288 RVA: 0x000066DC File Offset: 0x000048DC
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield break;
				}
			}

			// Token: 0x0200003C RID: 60
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x06000121 RID: 289 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x06000122 RID: 290 RVA: 0x000066FF File Offset: 0x000048FF
				public bool MoveNext()
				{
					bool flag = this.$PC != 0;
					this.$PC = -1;
					if (!flag)
					{
					}
					return false;
				}

				// Token: 0x1700003C RID: 60
				// (get) Token: 0x06000123 RID: 291 RVA: 0x0000671C File Offset: 0x0000491C
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x1700003D RID: 61
				// (get) Token: 0x06000124 RID: 292 RVA: 0x00006738 File Offset: 0x00004938
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x06000125 RID: 293 RVA: 0x0000227E File Offset: 0x0000047E
				[DebuggerHidden]
				public void Dispose()
				{
				}

				// Token: 0x06000126 RID: 294 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x06000127 RID: 295 RVA: 0x00006754 File Offset: 0x00004954
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x06000128 RID: 296 RVA: 0x0000676E File Offset: 0x0000496E
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new IMGUIContainer.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x04000089 RID: 137
				internal UxmlChildElementDescription $current;

				// Token: 0x0400008A RID: 138
				internal bool $disposing;

				// Token: 0x0400008B RID: 139
				internal int $PC;
			}
		}

		// Token: 0x0200003D RID: 61
		private struct GUIGlobals
		{
			// Token: 0x0400008C RID: 140
			public Matrix4x4 matrix;

			// Token: 0x0400008D RID: 141
			public Color color;

			// Token: 0x0400008E RID: 142
			public Color contentColor;

			// Token: 0x0400008F RID: 143
			public Color backgroundColor;

			// Token: 0x04000090 RID: 144
			public bool enabled;

			// Token: 0x04000091 RID: 145
			public bool changed;

			// Token: 0x04000092 RID: 146
			public int displayIndex;
		}
	}
}
