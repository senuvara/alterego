﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200016E RID: 366
	public class UxmlFactory<TCreatedType, TTraits> : IUxmlFactory where TCreatedType : VisualElement where TTraits : UxmlTraits, new()
	{
		// Token: 0x0600096E RID: 2414 RVA: 0x0002098D File Offset: 0x0001EB8D
		protected UxmlFactory()
		{
			this.m_Traits = Activator.CreateInstance<TTraits>();
		}

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x0600096F RID: 2415 RVA: 0x000209A4 File Offset: 0x0001EBA4
		public virtual string uxmlName
		{
			get
			{
				return typeof(TCreatedType).Name;
			}
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x06000970 RID: 2416 RVA: 0x000209C8 File Offset: 0x0001EBC8
		public virtual string uxmlNamespace
		{
			get
			{
				return (typeof(TCreatedType).Namespace == null) ? string.Empty : typeof(TCreatedType).Namespace;
			}
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x06000971 RID: 2417 RVA: 0x00020A0C File Offset: 0x0001EC0C
		public virtual string uxmlQualifiedName
		{
			get
			{
				return typeof(TCreatedType).FullName;
			}
		}

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x06000972 RID: 2418 RVA: 0x00020A30 File Offset: 0x0001EC30
		public bool canHaveAnyAttribute
		{
			get
			{
				return this.m_Traits.canHaveAnyAttribute;
			}
		}

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x06000973 RID: 2419 RVA: 0x00020A58 File Offset: 0x0001EC58
		public virtual IEnumerable<UxmlAttributeDescription> uxmlAttributesDescription
		{
			get
			{
				foreach (UxmlAttributeDescription attr in this.m_Traits.uxmlAttributesDescription)
				{
					yield return attr;
				}
				yield break;
			}
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x06000974 RID: 2420 RVA: 0x00020A84 File Offset: 0x0001EC84
		public virtual IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
		{
			get
			{
				foreach (UxmlChildElementDescription child in this.m_Traits.uxmlChildElementsDescription)
				{
					yield return child;
				}
				yield break;
			}
		}

		// Token: 0x1700026D RID: 621
		// (get) Token: 0x06000975 RID: 2421 RVA: 0x00020AB0 File Offset: 0x0001ECB0
		public virtual string substituteForTypeName
		{
			get
			{
				string result;
				if (typeof(TCreatedType) == typeof(VisualElement))
				{
					result = string.Empty;
				}
				else
				{
					result = typeof(VisualElement).Name;
				}
				return result;
			}
		}

		// Token: 0x1700026E RID: 622
		// (get) Token: 0x06000976 RID: 2422 RVA: 0x00020AFC File Offset: 0x0001ECFC
		public virtual string substituteForTypeNamespace
		{
			get
			{
				string result;
				if (typeof(TCreatedType) == typeof(VisualElement))
				{
					result = string.Empty;
				}
				else
				{
					result = ((typeof(VisualElement).Namespace == null) ? string.Empty : typeof(VisualElement).Namespace);
				}
				return result;
			}
		}

		// Token: 0x1700026F RID: 623
		// (get) Token: 0x06000977 RID: 2423 RVA: 0x00020B64 File Offset: 0x0001ED64
		public virtual string substituteForTypeQualifiedName
		{
			get
			{
				string result;
				if (typeof(TCreatedType) == typeof(VisualElement))
				{
					result = string.Empty;
				}
				else
				{
					result = typeof(VisualElement).FullName;
				}
				return result;
			}
		}

		// Token: 0x06000978 RID: 2424 RVA: 0x00020BB0 File Offset: 0x0001EDB0
		[Obsolete("Call or override AcceptsAttributeBag(IUxmlAttributes bag, CreationContext cc) instaed")]
		public virtual bool AcceptsAttributeBag(IUxmlAttributes bag)
		{
			return this.AcceptsAttributeBag(bag, default(CreationContext));
		}

		// Token: 0x06000979 RID: 2425 RVA: 0x00020BD8 File Offset: 0x0001EDD8
		public virtual bool AcceptsAttributeBag(IUxmlAttributes bag, CreationContext cc)
		{
			return true;
		}

		// Token: 0x0600097A RID: 2426 RVA: 0x00020BF0 File Offset: 0x0001EDF0
		public virtual VisualElement Create(IUxmlAttributes bag, CreationContext cc)
		{
			Type[] types = new Type[]
			{
				typeof(IUxmlAttributes),
				typeof(CreationContext)
			};
			bool flag = base.GetType().GetMethod("DoCreate", BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.ExactBinding, null, types, null) != null;
			TCreatedType tcreatedType;
			if (flag)
			{
				if (!UxmlFactory<TCreatedType, TTraits>.s_WarningLogged)
				{
					Debug.LogWarning("Calling obsolete method " + base.GetType().FullName + ".DoCreate(IUxmlAttributes bag, CreationContext cc). Remove and implemenent a default constructor for the created type instead.");
					UxmlFactory<TCreatedType, TTraits>.s_WarningLogged = true;
				}
				tcreatedType = this.DoCreate(bag, cc);
			}
			else
			{
				try
				{
					tcreatedType = (TCreatedType)((object)Activator.CreateInstance(typeof(TCreatedType)));
				}
				catch (MemberAccessException)
				{
					if (!UxmlFactory<TCreatedType, TTraits>.s_WarningLogged)
					{
						Debug.LogError("No accessible default constructor for " + typeof(TCreatedType));
						UxmlFactory<TCreatedType, TTraits>.s_WarningLogged = true;
					}
					tcreatedType = (TCreatedType)((object)null);
				}
			}
			if (tcreatedType != null)
			{
				this.m_Traits.Init(tcreatedType, bag, cc);
			}
			return tcreatedType;
		}

		// Token: 0x0600097B RID: 2427 RVA: 0x00020D20 File Offset: 0x0001EF20
		[Obsolete("Remove and implemenent a default constructor for the created type instead.")]
		protected virtual TCreatedType DoCreate(IUxmlAttributes bag, CreationContext cc)
		{
			return (TCreatedType)((object)null);
		}

		// Token: 0x17000270 RID: 624
		// (get) Token: 0x0600097C RID: 2428 RVA: 0x00020D3C File Offset: 0x0001EF3C
		[Obsolete("Use uxmlName and uxmlNamespace instead.")]
		public Type CreatesType
		{
			get
			{
				return typeof(TCreatedType);
			}
		}

		// Token: 0x0600097D RID: 2429 RVA: 0x00020D5B File Offset: 0x0001EF5B
		// Note: this type is marked as 'beforefieldinit'.
		static UxmlFactory()
		{
		}

		// Token: 0x0400043B RID: 1083
		protected TTraits m_Traits;

		// Token: 0x0400043C RID: 1084
		private static bool s_WarningLogged = false;

		// Token: 0x0200016F RID: 367
		[CompilerGenerated]
		private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlAttributeDescription>, IEnumerator, IDisposable, IEnumerator<UxmlAttributeDescription>
		{
			// Token: 0x0600097E RID: 2430 RVA: 0x00002223 File Offset: 0x00000423
			[DebuggerHidden]
			public <>c__Iterator0()
			{
			}

			// Token: 0x0600097F RID: 2431 RVA: 0x00020D64 File Offset: 0x0001EF64
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					enumerator = this.m_Traits.uxmlAttributesDescription.GetEnumerator();
					num = 4294967293U;
					break;
				case 1U:
					break;
				default:
					return false;
				}
				try
				{
					switch (num)
					{
					}
					if (enumerator.MoveNext())
					{
						attr = enumerator.Current;
						this.$current = attr;
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						flag = true;
						return true;
					}
				}
				finally
				{
					if (!flag)
					{
						if (enumerator != null)
						{
							enumerator.Dispose();
						}
					}
				}
				this.$PC = -1;
				return false;
			}

			// Token: 0x17000271 RID: 625
			// (get) Token: 0x06000980 RID: 2432 RVA: 0x00020E4C File Offset: 0x0001F04C
			UxmlAttributeDescription IEnumerator<UxmlAttributeDescription>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000272 RID: 626
			// (get) Token: 0x06000981 RID: 2433 RVA: 0x00020E68 File Offset: 0x0001F068
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000982 RID: 2434 RVA: 0x00020E84 File Offset: 0x0001F084
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 1U:
					try
					{
					}
					finally
					{
						if (enumerator != null)
						{
							enumerator.Dispose();
						}
					}
					break;
				}
			}

			// Token: 0x06000983 RID: 2435 RVA: 0x00002280 File Offset: 0x00000480
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000984 RID: 2436 RVA: 0x00020EE8 File Offset: 0x0001F0E8
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlAttributeDescription>.GetEnumerator();
			}

			// Token: 0x06000985 RID: 2437 RVA: 0x00020F04 File Offset: 0x0001F104
			[DebuggerHidden]
			IEnumerator<UxmlAttributeDescription> IEnumerable<UxmlAttributeDescription>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				UxmlFactory<TCreatedType, TTraits>.<>c__Iterator0 <>c__Iterator = new UxmlFactory<TCreatedType, TTraits>.<>c__Iterator0();
				<>c__Iterator.$this = this;
				return <>c__Iterator;
			}

			// Token: 0x0400043D RID: 1085
			internal IEnumerator<UxmlAttributeDescription> $locvar0;

			// Token: 0x0400043E RID: 1086
			internal UxmlAttributeDescription <attr>__1;

			// Token: 0x0400043F RID: 1087
			internal UxmlFactory<TCreatedType, TTraits> $this;

			// Token: 0x04000440 RID: 1088
			internal UxmlAttributeDescription $current;

			// Token: 0x04000441 RID: 1089
			internal bool $disposing;

			// Token: 0x04000442 RID: 1090
			internal int $PC;
		}

		// Token: 0x02000170 RID: 368
		[CompilerGenerated]
		private sealed class <>c__Iterator1 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
		{
			// Token: 0x06000986 RID: 2438 RVA: 0x00002223 File Offset: 0x00000423
			[DebuggerHidden]
			public <>c__Iterator1()
			{
			}

			// Token: 0x06000987 RID: 2439 RVA: 0x00020F38 File Offset: 0x0001F138
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					enumerator = this.m_Traits.uxmlChildElementsDescription.GetEnumerator();
					num = 4294967293U;
					break;
				case 1U:
					break;
				default:
					return false;
				}
				try
				{
					switch (num)
					{
					}
					if (enumerator.MoveNext())
					{
						child = enumerator.Current;
						this.$current = child;
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						flag = true;
						return true;
					}
				}
				finally
				{
					if (!flag)
					{
						if (enumerator != null)
						{
							enumerator.Dispose();
						}
					}
				}
				this.$PC = -1;
				return false;
			}

			// Token: 0x17000273 RID: 627
			// (get) Token: 0x06000988 RID: 2440 RVA: 0x00021020 File Offset: 0x0001F220
			UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000274 RID: 628
			// (get) Token: 0x06000989 RID: 2441 RVA: 0x0002103C File Offset: 0x0001F23C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600098A RID: 2442 RVA: 0x00021058 File Offset: 0x0001F258
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 1U:
					try
					{
					}
					finally
					{
						if (enumerator != null)
						{
							enumerator.Dispose();
						}
					}
					break;
				}
			}

			// Token: 0x0600098B RID: 2443 RVA: 0x00002280 File Offset: 0x00000480
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600098C RID: 2444 RVA: 0x000210BC File Offset: 0x0001F2BC
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
			}

			// Token: 0x0600098D RID: 2445 RVA: 0x000210D8 File Offset: 0x0001F2D8
			[DebuggerHidden]
			IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				UxmlFactory<TCreatedType, TTraits>.<>c__Iterator1 <>c__Iterator = new UxmlFactory<TCreatedType, TTraits>.<>c__Iterator1();
				<>c__Iterator.$this = this;
				return <>c__Iterator;
			}

			// Token: 0x04000443 RID: 1091
			internal IEnumerator<UxmlChildElementDescription> $locvar0;

			// Token: 0x04000444 RID: 1092
			internal UxmlChildElementDescription <child>__1;

			// Token: 0x04000445 RID: 1093
			internal UxmlFactory<TCreatedType, TTraits> $this;

			// Token: 0x04000446 RID: 1094
			internal UxmlChildElementDescription $current;

			// Token: 0x04000447 RID: 1095
			internal bool $disposing;

			// Token: 0x04000448 RID: 1096
			internal int $PC;
		}
	}
}
