﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000063 RID: 99
	// (Invoke) Token: 0x060002B2 RID: 690
	internal delegate void SavePersistentViewData();
}
