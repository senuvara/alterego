﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000143 RID: 323
	public class DetachFromPanelEvent : PanelChangedEventBase<DetachFromPanelEvent>
	{
		// Token: 0x0600087C RID: 2172 RVA: 0x0001BE54 File Offset: 0x0001A054
		public DetachFromPanelEvent()
		{
		}
	}
}
