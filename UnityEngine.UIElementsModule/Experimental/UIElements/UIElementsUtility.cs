﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000A8 RID: 168
	internal class UIElementsUtility
	{
		// Token: 0x06000408 RID: 1032 RVA: 0x0000DC4C File Offset: 0x0000BE4C
		static UIElementsUtility()
		{
			Delegate takeCapture = GUIUtility.takeCapture;
			if (UIElementsUtility.<>f__mg$cache0 == null)
			{
				UIElementsUtility.<>f__mg$cache0 = new Action(UIElementsUtility.TakeCapture);
			}
			GUIUtility.takeCapture = (Action)Delegate.Combine(takeCapture, UIElementsUtility.<>f__mg$cache0);
			Delegate releaseCapture = GUIUtility.releaseCapture;
			if (UIElementsUtility.<>f__mg$cache1 == null)
			{
				UIElementsUtility.<>f__mg$cache1 = new Action(UIElementsUtility.ReleaseCapture);
			}
			GUIUtility.releaseCapture = (Action)Delegate.Combine(releaseCapture, UIElementsUtility.<>f__mg$cache1);
			Delegate processEvent = GUIUtility.processEvent;
			if (UIElementsUtility.<>f__mg$cache2 == null)
			{
				UIElementsUtility.<>f__mg$cache2 = new Func<int, IntPtr, bool>(UIElementsUtility.ProcessEvent);
			}
			GUIUtility.processEvent = (Func<int, IntPtr, bool>)Delegate.Combine(processEvent, UIElementsUtility.<>f__mg$cache2);
			Delegate cleanupRoots = GUIUtility.cleanupRoots;
			if (UIElementsUtility.<>f__mg$cache3 == null)
			{
				UIElementsUtility.<>f__mg$cache3 = new Action(UIElementsUtility.CleanupRoots);
			}
			GUIUtility.cleanupRoots = (Action)Delegate.Combine(cleanupRoots, UIElementsUtility.<>f__mg$cache3);
			Delegate endContainerGUIFromException = GUIUtility.endContainerGUIFromException;
			if (UIElementsUtility.<>f__mg$cache4 == null)
			{
				UIElementsUtility.<>f__mg$cache4 = new Func<Exception, bool>(UIElementsUtility.EndContainerGUIFromException);
			}
			GUIUtility.endContainerGUIFromException = (Func<Exception, bool>)Delegate.Combine(endContainerGUIFromException, UIElementsUtility.<>f__mg$cache4);
			Delegate enabledStateChanged = GUIUtility.enabledStateChanged;
			if (UIElementsUtility.<>f__mg$cache5 == null)
			{
				UIElementsUtility.<>f__mg$cache5 = new Action(UIElementsUtility.EnabledStateChanged);
			}
			GUIUtility.enabledStateChanged = (Action)Delegate.Combine(enabledStateChanged, UIElementsUtility.<>f__mg$cache5);
		}

		// Token: 0x06000409 RID: 1033 RVA: 0x00002223 File Offset: 0x00000423
		public UIElementsUtility()
		{
		}

		// Token: 0x0600040A RID: 1034 RVA: 0x0000DDA8 File Offset: 0x0000BFA8
		private static void EnabledStateChanged()
		{
			if (UIElementsUtility.s_ContainerStack.Count > 0)
			{
				UIElementsUtility.s_ContainerStack.Peek().MarkDirtyLayout();
			}
		}

		// Token: 0x0600040B RID: 1035 RVA: 0x0000DDCC File Offset: 0x0000BFCC
		private static void TakeCapture()
		{
			if (UIElementsUtility.s_ContainerStack.Count > 0)
			{
				IMGUIContainer handler = UIElementsUtility.s_ContainerStack.Peek();
				if (MouseCaptureController.IsMouseCaptured() && !handler.HasMouseCapture())
				{
					Debug.Log("Should not grab hot control with an active capture");
				}
				handler.CaptureMouse();
			}
		}

		// Token: 0x0600040C RID: 1036 RVA: 0x00009462 File Offset: 0x00007662
		private static void ReleaseCapture()
		{
			MouseCaptureController.ReleaseMouse();
		}

		// Token: 0x0600040D RID: 1037 RVA: 0x0000DE20 File Offset: 0x0000C020
		private static bool ProcessEvent(int instanceID, IntPtr nativeEventPtr)
		{
			Panel panel;
			bool result;
			if (nativeEventPtr != IntPtr.Zero && UIElementsUtility.s_UIElementsCache.TryGetValue(instanceID, out panel))
			{
				UIElementsUtility.s_EventInstance.CopyFromPtr(nativeEventPtr);
				result = UIElementsUtility.DoDispatch(panel);
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x0600040E RID: 1038 RVA: 0x0000DE70 File Offset: 0x0000C070
		public static void RemoveCachedPanel(int instanceID)
		{
			UIElementsUtility.s_UIElementsCache.Remove(instanceID);
		}

		// Token: 0x0600040F RID: 1039 RVA: 0x0000DE7F File Offset: 0x0000C07F
		private static void CleanupRoots()
		{
			UIElementsUtility.s_EventInstance = null;
			EventDispatcher.ClearDispatcher();
			UIElementsUtility.s_UIElementsCache = null;
			UIElementsUtility.s_ContainerStack = null;
		}

		// Token: 0x06000410 RID: 1040 RVA: 0x0000DE9C File Offset: 0x0000C09C
		private static bool EndContainerGUIFromException(Exception exception)
		{
			if (UIElementsUtility.s_ContainerStack.Count > 0)
			{
				GUIUtility.EndContainer();
				UIElementsUtility.s_ContainerStack.Pop();
			}
			return GUIUtility.ShouldRethrowException(exception);
		}

		// Token: 0x06000411 RID: 1041 RVA: 0x0000DEDC File Offset: 0x0000C0DC
		internal static void BeginContainerGUI(GUILayoutUtility.LayoutCache cache, Event evt, IMGUIContainer container)
		{
			if (container.useOwnerObjectGUIState)
			{
				GUIUtility.BeginContainerFromOwner(container.elementPanel.ownerObject);
			}
			else
			{
				GUIUtility.BeginContainer(container.guiState);
			}
			UIElementsUtility.s_ContainerStack.Push(container);
			GUIUtility.s_SkinMode = (int)container.contextType;
			GUIUtility.s_OriginalID = container.elementPanel.ownerObject.GetInstanceID();
			if (Event.current == null)
			{
				Event.current = evt;
			}
			else
			{
				Event.current.CopyFrom(evt);
			}
			GUI.enabled = container.enabledInHierarchy;
			GUILayoutUtility.BeginContainer(cache);
			GUIUtility.ResetGlobalState();
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x0000DF80 File Offset: 0x0000C180
		internal static void EndContainerGUI(Event evt)
		{
			if (Event.current.type == EventType.Layout && UIElementsUtility.s_ContainerStack.Count > 0)
			{
				Rect layout = UIElementsUtility.s_ContainerStack.Peek().layout;
				GUILayoutUtility.LayoutFromContainer(layout.width, layout.height);
			}
			GUILayoutUtility.SelectIDList(GUIUtility.s_OriginalID, false);
			GUIContent.ClearStaticCache();
			if (UIElementsUtility.s_ContainerStack.Count > 0)
			{
			}
			evt.CopyFrom(Event.current);
			if (UIElementsUtility.s_ContainerStack.Count > 0)
			{
				GUIUtility.EndContainer();
				UIElementsUtility.s_ContainerStack.Pop();
			}
		}

		// Token: 0x06000413 RID: 1043 RVA: 0x0000E024 File Offset: 0x0000C224
		internal static ContextType GetGUIContextType()
		{
			return (GUIUtility.s_SkinMode != 0) ? ContextType.Editor : ContextType.Player;
		}

		// Token: 0x06000414 RID: 1044 RVA: 0x0000E04C File Offset: 0x0000C24C
		internal static EventBase CreateEvent(Event systemEvent)
		{
			return UIElementsUtility.CreateEvent(systemEvent, systemEvent.type);
		}

		// Token: 0x06000415 RID: 1045 RVA: 0x0000E070 File Offset: 0x0000C270
		internal static EventBase CreateEvent(Event systemEvent, EventType eventType)
		{
			switch (eventType)
			{
			case EventType.MouseDown:
				return MouseEventBase<MouseDownEvent>.GetPooled(systemEvent);
			case EventType.MouseUp:
				return MouseEventBase<MouseUpEvent>.GetPooled(systemEvent);
			case EventType.MouseMove:
				return MouseEventBase<MouseMoveEvent>.GetPooled(systemEvent);
			case EventType.MouseDrag:
				return MouseEventBase<MouseMoveEvent>.GetPooled(systemEvent);
			case EventType.KeyDown:
				return KeyboardEventBase<KeyDownEvent>.GetPooled(systemEvent);
			case EventType.KeyUp:
				return KeyboardEventBase<KeyUpEvent>.GetPooled(systemEvent);
			case EventType.ScrollWheel:
				return WheelEvent.GetPooled(systemEvent);
			case EventType.DragUpdated:
				return MouseEventBase<DragUpdatedEvent>.GetPooled(systemEvent);
			case EventType.DragPerform:
				return MouseEventBase<DragPerformEvent>.GetPooled(systemEvent);
			case EventType.ValidateCommand:
				return CommandEventBase<ValidateCommandEvent>.GetPooled(systemEvent);
			case EventType.ExecuteCommand:
				return CommandEventBase<ExecuteCommandEvent>.GetPooled(systemEvent);
			case EventType.DragExited:
				return MouseEventBase<DragExitedEvent>.GetPooled(systemEvent);
			case EventType.ContextClick:
				return MouseEventBase<ContextClickEvent>.GetPooled(systemEvent);
			case EventType.MouseEnterWindow:
				return MouseEventBase<MouseEnterWindowEvent>.GetPooled(systemEvent);
			case EventType.MouseLeaveWindow:
				return MouseEventBase<MouseLeaveWindowEvent>.GetPooled(systemEvent);
			}
			return IMGUIEvent.GetPooled(systemEvent);
		}

		// Token: 0x06000416 RID: 1046 RVA: 0x0000E1A4 File Offset: 0x0000C3A4
		private static bool DoDispatch(BaseVisualElementPanel panel)
		{
			bool result;
			if (UIElementsUtility.s_EventInstance.type == EventType.Repaint)
			{
				panel.Repaint(UIElementsUtility.s_EventInstance);
				result = (panel.IMGUIContainersCount > 0);
			}
			else
			{
				panel.ValidateLayout();
				using (EventBase eventBase = UIElementsUtility.CreateEvent(UIElementsUtility.s_EventInstance))
				{
					bool flag = UIElementsUtility.s_EventInstance.type == EventType.Used || UIElementsUtility.s_EventInstance.type == EventType.Layout || UIElementsUtility.s_EventInstance.type == EventType.ExecuteCommand || UIElementsUtility.s_EventInstance.type == EventType.ValidateCommand;
					panel.SendEvent(eventBase, (!flag) ? DispatchMode.Default : DispatchMode.Immediate);
					if (eventBase.isPropagationStopped)
					{
						panel.visualTree.IncrementVersion(VersionChangeType.Repaint);
					}
					result = eventBase.isPropagationStopped;
				}
			}
			return result;
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x0000E298 File Offset: 0x0000C498
		internal static Dictionary<int, Panel>.Enumerator GetPanelsIterator()
		{
			return UIElementsUtility.s_UIElementsCache.GetEnumerator();
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x0000E2B8 File Offset: 0x0000C4B8
		internal static Panel FindOrCreatePanel(ScriptableObject ownerObject, ContextType contextType, IDataWatchService dataWatch = null)
		{
			Panel panel;
			if (!UIElementsUtility.s_UIElementsCache.TryGetValue(ownerObject.GetInstanceID(), out panel))
			{
				panel = new Panel(ownerObject, contextType, dataWatch, null);
				UIElementsUtility.s_UIElementsCache.Add(ownerObject.GetInstanceID(), panel);
			}
			else
			{
				Debug.Assert(contextType == panel.contextType, "Context type mismatch");
			}
			return panel;
		}

		// Token: 0x06000419 RID: 1049 RVA: 0x0000E31C File Offset: 0x0000C51C
		internal static Panel FindOrCreatePanel(ScriptableObject ownerObject)
		{
			return UIElementsUtility.FindOrCreatePanel(ownerObject, UIElementsUtility.GetGUIContextType(), null);
		}

		// Token: 0x040001E5 RID: 485
		private static Stack<IMGUIContainer> s_ContainerStack = new Stack<IMGUIContainer>();

		// Token: 0x040001E6 RID: 486
		private static Dictionary<int, Panel> s_UIElementsCache = new Dictionary<int, Panel>();

		// Token: 0x040001E7 RID: 487
		private static Event s_EventInstance = new Event();

		// Token: 0x040001E8 RID: 488
		internal static Color editorPlayModeTintColor = Color.white;

		// Token: 0x040001E9 RID: 489
		[CompilerGenerated]
		private static Action <>f__mg$cache0;

		// Token: 0x040001EA RID: 490
		[CompilerGenerated]
		private static Action <>f__mg$cache1;

		// Token: 0x040001EB RID: 491
		[CompilerGenerated]
		private static Func<int, IntPtr, bool> <>f__mg$cache2;

		// Token: 0x040001EC RID: 492
		[CompilerGenerated]
		private static Action <>f__mg$cache3;

		// Token: 0x040001ED RID: 493
		[CompilerGenerated]
		private static Func<Exception, bool> <>f__mg$cache4;

		// Token: 0x040001EE RID: 494
		[CompilerGenerated]
		private static Action <>f__mg$cache5;
	}
}
