﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200010C RID: 268
	public abstract class DragAndDropEventBase<T> : MouseEventBase<T>, IDragAndDropEvent, IPropagatableEvent where T : DragAndDropEventBase<T>, new()
	{
		// Token: 0x0600075E RID: 1886 RVA: 0x00019CCE File Offset: 0x00017ECE
		protected DragAndDropEventBase()
		{
		}
	}
}
