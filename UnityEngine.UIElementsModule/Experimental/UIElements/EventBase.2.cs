﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000115 RID: 277
	public abstract class EventBase<T> : EventBase where T : EventBase<T>, new()
	{
		// Token: 0x06000797 RID: 1943 RVA: 0x0001A2FD File Offset: 0x000184FD
		protected EventBase()
		{
			this.m_RefCount = 0;
		}

		// Token: 0x06000798 RID: 1944 RVA: 0x0001A310 File Offset: 0x00018510
		public static long TypeId()
		{
			return EventBase<T>.s_TypeId;
		}

		// Token: 0x06000799 RID: 1945 RVA: 0x0001A32A File Offset: 0x0001852A
		protected override void Init()
		{
			base.Init();
			if (this.m_RefCount != 0)
			{
				Debug.Log("Event improperly released.");
				this.m_RefCount = 0;
			}
		}

		// Token: 0x0600079A RID: 1946 RVA: 0x0001A354 File Offset: 0x00018554
		public static T GetPooled()
		{
			T result = EventBase<T>.s_Pool.Get();
			result.Init();
			result.pooled = true;
			result.Acquire();
			return result;
		}

		// Token: 0x0600079B RID: 1947 RVA: 0x0001A39D File Offset: 0x0001859D
		private static void ReleasePooled(T evt)
		{
			if (evt.pooled)
			{
				evt.Init();
				EventBase<T>.s_Pool.Release(evt);
				evt.pooled = false;
			}
		}

		// Token: 0x0600079C RID: 1948 RVA: 0x0001A3DA File Offset: 0x000185DA
		internal override void Acquire()
		{
			this.m_RefCount++;
		}

		// Token: 0x0600079D RID: 1949 RVA: 0x0001A3EC File Offset: 0x000185EC
		public override void Dispose()
		{
			if (--this.m_RefCount == 0)
			{
				EventBase<T>.ReleasePooled((T)((object)this));
			}
		}

		// Token: 0x0600079E RID: 1950 RVA: 0x0001A420 File Offset: 0x00018620
		public override long GetEventTypeId()
		{
			return EventBase<T>.s_TypeId;
		}

		// Token: 0x0600079F RID: 1951 RVA: 0x0001A43A File Offset: 0x0001863A
		// Note: this type is marked as 'beforefieldinit'.
		static EventBase()
		{
		}

		// Token: 0x040002F4 RID: 756
		private static readonly long s_TypeId = EventBase.RegisterEventType();

		// Token: 0x040002F5 RID: 757
		private static readonly ObjectPool<T> s_Pool = new ObjectPool<T>(100);

		// Token: 0x040002F6 RID: 758
		private int m_RefCount;
	}
}
