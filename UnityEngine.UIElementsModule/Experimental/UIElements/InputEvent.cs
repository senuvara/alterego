﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200012B RID: 299
	public class InputEvent : EventBase<InputEvent>
	{
		// Token: 0x0600080B RID: 2059 RVA: 0x0001B371 File Offset: 0x00019571
		public InputEvent()
		{
			this.Init();
		}

		// Token: 0x17000201 RID: 513
		// (get) Token: 0x0600080C RID: 2060 RVA: 0x0001B380 File Offset: 0x00019580
		// (set) Token: 0x0600080D RID: 2061 RVA: 0x0001B39A File Offset: 0x0001959A
		public string previousData
		{
			[CompilerGenerated]
			get
			{
				return this.<previousData>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<previousData>k__BackingField = value;
			}
		}

		// Token: 0x17000202 RID: 514
		// (get) Token: 0x0600080E RID: 2062 RVA: 0x0001B3A4 File Offset: 0x000195A4
		// (set) Token: 0x0600080F RID: 2063 RVA: 0x0001B3BE File Offset: 0x000195BE
		public string newData
		{
			[CompilerGenerated]
			get
			{
				return this.<newData>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<newData>k__BackingField = value;
			}
		}

		// Token: 0x06000810 RID: 2064 RVA: 0x0001B3C7 File Offset: 0x000195C7
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown);
			this.previousData = null;
			this.newData = null;
		}

		// Token: 0x06000811 RID: 2065 RVA: 0x0001B3E8 File Offset: 0x000195E8
		public static InputEvent GetPooled(string previousData, string newData)
		{
			InputEvent pooled = EventBase<InputEvent>.GetPooled();
			pooled.previousData = previousData;
			pooled.newData = newData;
			return pooled;
		}

		// Token: 0x04000314 RID: 788
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <previousData>k__BackingField;

		// Token: 0x04000315 RID: 789
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <newData>k__BackingField;
	}
}
