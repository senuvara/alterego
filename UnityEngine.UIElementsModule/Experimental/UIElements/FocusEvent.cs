﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200012A RID: 298
	public class FocusEvent : FocusEventBase<FocusEvent>
	{
		// Token: 0x06000809 RID: 2057 RVA: 0x0001B350 File Offset: 0x00019550
		public FocusEvent()
		{
		}

		// Token: 0x0600080A RID: 2058 RVA: 0x0001B358 File Offset: 0x00019558
		protected internal override void PreDispatch()
		{
			this.m_FocusController.DoFocusChange(base.target as Focusable);
		}
	}
}
