﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200005D RID: 93
	internal class RepaintData
	{
		// Token: 0x06000260 RID: 608 RVA: 0x000096E9 File Offset: 0x000078E9
		public RepaintData()
		{
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x06000261 RID: 609 RVA: 0x000096FC File Offset: 0x000078FC
		// (set) Token: 0x06000262 RID: 610 RVA: 0x00009716 File Offset: 0x00007916
		public Matrix4x4 currentOffset
		{
			[CompilerGenerated]
			get
			{
				return this.<currentOffset>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<currentOffset>k__BackingField = value;
			}
		} = Matrix4x4.identity;

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x06000263 RID: 611 RVA: 0x00009720 File Offset: 0x00007920
		// (set) Token: 0x06000264 RID: 612 RVA: 0x0000973A File Offset: 0x0000793A
		public Vector2 mousePosition
		{
			[CompilerGenerated]
			get
			{
				return this.<mousePosition>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<mousePosition>k__BackingField = value;
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000265 RID: 613 RVA: 0x00009744 File Offset: 0x00007944
		// (set) Token: 0x06000266 RID: 614 RVA: 0x0000975E File Offset: 0x0000795E
		public Rect currentWorldClip
		{
			[CompilerGenerated]
			get
			{
				return this.<currentWorldClip>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<currentWorldClip>k__BackingField = value;
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000267 RID: 615 RVA: 0x00009768 File Offset: 0x00007968
		// (set) Token: 0x06000268 RID: 616 RVA: 0x00009782 File Offset: 0x00007982
		public Event repaintEvent
		{
			[CompilerGenerated]
			get
			{
				return this.<repaintEvent>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<repaintEvent>k__BackingField = value;
			}
		}

		// Token: 0x040000EE RID: 238
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Matrix4x4 <currentOffset>k__BackingField;

		// Token: 0x040000EF RID: 239
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector2 <mousePosition>k__BackingField;

		// Token: 0x040000F0 RID: 240
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Rect <currentWorldClip>k__BackingField;

		// Token: 0x040000F1 RID: 241
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Event <repaintEvent>k__BackingField;
	}
}
