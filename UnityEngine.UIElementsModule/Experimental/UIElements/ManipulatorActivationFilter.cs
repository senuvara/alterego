﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200004F RID: 79
	public struct ManipulatorActivationFilter
	{
		// Token: 0x0600021C RID: 540 RVA: 0x00008348 File Offset: 0x00006548
		public bool Matches(IMouseEvent e)
		{
			bool flag = this.clickCount == 0 || e.clickCount >= this.clickCount;
			return this.button == (MouseButton)e.button && this.HasModifiers(e) && flag;
		}

		// Token: 0x0600021D RID: 541 RVA: 0x000083A0 File Offset: 0x000065A0
		private bool HasModifiers(IMouseEvent e)
		{
			return ((this.modifiers & EventModifiers.Alt) == EventModifiers.None || e.altKey) && ((this.modifiers & EventModifiers.Alt) != EventModifiers.None || !e.altKey) && ((this.modifiers & EventModifiers.Control) == EventModifiers.None || e.ctrlKey) && ((this.modifiers & EventModifiers.Control) != EventModifiers.None || !e.ctrlKey) && ((this.modifiers & EventModifiers.Shift) == EventModifiers.None || e.shiftKey) && ((this.modifiers & EventModifiers.Shift) != EventModifiers.None || !e.shiftKey) && ((this.modifiers & EventModifiers.Command) == EventModifiers.None || e.commandKey) && ((this.modifiers & EventModifiers.Command) != EventModifiers.None || !e.commandKey);
		}

		// Token: 0x040000B6 RID: 182
		public MouseButton button;

		// Token: 0x040000B7 RID: 183
		public EventModifiers modifiers;

		// Token: 0x040000B8 RID: 184
		public int clickCount;
	}
}
