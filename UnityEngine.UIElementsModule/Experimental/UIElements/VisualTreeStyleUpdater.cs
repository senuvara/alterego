﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000DF RID: 223
	internal class VisualTreeStyleUpdater : BaseVisualTreeUpdater
	{
		// Token: 0x06000667 RID: 1639 RVA: 0x00016661 File Offset: 0x00014861
		public VisualTreeStyleUpdater()
		{
		}

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x06000668 RID: 1640 RVA: 0x00016694 File Offset: 0x00014894
		public override string description
		{
			get
			{
				return "Update Style";
			}
		}

		// Token: 0x06000669 RID: 1641 RVA: 0x000166AE File Offset: 0x000148AE
		public void DirtyStyleSheets()
		{
			StyleCache.ClearStyleCache();
			VisualTreeStyleUpdater.PropagateDirtyStyleSheets(base.visualTree);
			base.visualTree.IncrementVersion(VersionChangeType.StyleSheet);
		}

		// Token: 0x0600066A RID: 1642 RVA: 0x000166D0 File Offset: 0x000148D0
		private static void PropagateDirtyStyleSheets(VisualElement element)
		{
			if (element != null)
			{
				if (element.styleSheets != null)
				{
					element.LoadStyleSheetsFromPaths();
				}
				foreach (VisualElement element2 in element.shadow.Children())
				{
					VisualTreeStyleUpdater.PropagateDirtyStyleSheets(element2);
				}
			}
		}

		// Token: 0x0600066B RID: 1643 RVA: 0x00016750 File Offset: 0x00014950
		public override void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType)
		{
			if ((versionChangeType & VersionChangeType.StyleSheet) == VersionChangeType.StyleSheet)
			{
				this.m_Version += 1U;
				if (this.m_IsApplyingStyles)
				{
					this.m_ApplyStyleUpdateList.Add(ve);
				}
				else
				{
					this.m_StyleContextHierarchyTraversal.AddChangedElement(ve);
				}
			}
		}

		// Token: 0x0600066C RID: 1644 RVA: 0x000167AC File Offset: 0x000149AC
		public override void Update()
		{
			if (this.m_Version != this.m_LastVersion)
			{
				this.m_LastVersion = this.m_Version;
				this.ApplyStyles();
				this.m_StyleContextHierarchyTraversal.Clear();
				foreach (VisualElement ve in this.m_ApplyStyleUpdateList)
				{
					this.m_StyleContextHierarchyTraversal.AddChangedElement(ve);
				}
				this.m_ApplyStyleUpdateList.Clear();
			}
		}

		// Token: 0x0600066D RID: 1645 RVA: 0x00016850 File Offset: 0x00014A50
		private void ApplyStyles()
		{
			Debug.Assert(base.visualTree.panel != null);
			this.m_IsApplyingStyles = true;
			this.m_StyleContextHierarchyTraversal.currentPixelsPerPoint = base.panel.currentPixelsPerPoint;
			this.m_StyleContextHierarchyTraversal.Traverse(base.visualTree);
			this.m_IsApplyingStyles = false;
		}

		// Token: 0x04000284 RID: 644
		private HashSet<VisualElement> m_ApplyStyleUpdateList = new HashSet<VisualElement>();

		// Token: 0x04000285 RID: 645
		private bool m_IsApplyingStyles = false;

		// Token: 0x04000286 RID: 646
		private uint m_Version = 0U;

		// Token: 0x04000287 RID: 647
		private uint m_LastVersion = 0U;

		// Token: 0x04000288 RID: 648
		private VisualTreeStyleUpdaterTraversal m_StyleContextHierarchyTraversal = new VisualTreeStyleUpdaterTraversal();
	}
}
