﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000173 RID: 371
	public class UxmlRootElementTraits : UxmlTraits
	{
		// Token: 0x06000996 RID: 2454 RVA: 0x000211CE File Offset: 0x0001F3CE
		public UxmlRootElementTraits()
		{
			base.canHaveAnyAttribute = false;
		}

		// Token: 0x1700027A RID: 634
		// (get) Token: 0x06000997 RID: 2455 RVA: 0x000211E0 File Offset: 0x0001F3E0
		public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
		{
			get
			{
				return new UxmlChildElementDescription[]
				{
					new UxmlChildElementDescription(typeof(VisualElement))
				};
			}
		}
	}
}
