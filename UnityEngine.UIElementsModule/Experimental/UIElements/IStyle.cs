﻿using System;
using UnityEngine.Experimental.UIElements.StyleEnums;
using UnityEngine.Experimental.UIElements.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200003F RID: 63
	public interface IStyle
	{
		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000130 RID: 304
		// (set) Token: 0x06000131 RID: 305
		StyleValue<float> width { get; set; }

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000132 RID: 306
		// (set) Token: 0x06000133 RID: 307
		StyleValue<float> height { get; set; }

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000134 RID: 308
		// (set) Token: 0x06000135 RID: 309
		StyleValue<float> maxWidth { get; set; }

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000136 RID: 310
		// (set) Token: 0x06000137 RID: 311
		StyleValue<float> maxHeight { get; set; }

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000138 RID: 312
		// (set) Token: 0x06000139 RID: 313
		StyleValue<float> minWidth { get; set; }

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x0600013A RID: 314
		// (set) Token: 0x0600013B RID: 315
		StyleValue<float> minHeight { get; set; }

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600013C RID: 316
		// (set) Token: 0x0600013D RID: 317
		StyleValue<Flex> flex { get; set; }

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x0600013E RID: 318
		// (set) Token: 0x0600013F RID: 319
		StyleValue<float> flexBasis { get; set; }

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000140 RID: 320
		// (set) Token: 0x06000141 RID: 321
		StyleValue<float> flexGrow { get; set; }

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000142 RID: 322
		// (set) Token: 0x06000143 RID: 323
		StyleValue<float> flexShrink { get; set; }

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000144 RID: 324
		// (set) Token: 0x06000145 RID: 325
		StyleValue<FlexDirection> flexDirection { get; set; }

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000146 RID: 326
		// (set) Token: 0x06000147 RID: 327
		StyleValue<Wrap> flexWrap { get; set; }

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000148 RID: 328
		// (set) Token: 0x06000149 RID: 329
		StyleValue<Overflow> overflow { get; set; }

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x0600014A RID: 330
		// (set) Token: 0x0600014B RID: 331
		StyleValue<float> positionLeft { get; set; }

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x0600014C RID: 332
		// (set) Token: 0x0600014D RID: 333
		StyleValue<float> positionTop { get; set; }

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x0600014E RID: 334
		// (set) Token: 0x0600014F RID: 335
		StyleValue<float> positionRight { get; set; }

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000150 RID: 336
		// (set) Token: 0x06000151 RID: 337
		StyleValue<float> positionBottom { get; set; }

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000152 RID: 338
		// (set) Token: 0x06000153 RID: 339
		StyleValue<float> marginLeft { get; set; }

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000154 RID: 340
		// (set) Token: 0x06000155 RID: 341
		StyleValue<float> marginTop { get; set; }

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000156 RID: 342
		// (set) Token: 0x06000157 RID: 343
		StyleValue<float> marginRight { get; set; }

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000158 RID: 344
		// (set) Token: 0x06000159 RID: 345
		StyleValue<float> marginBottom { get; set; }

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x0600015A RID: 346
		// (set) Token: 0x0600015B RID: 347
		[Obsolete("Use borderLeftWidth instead")]
		StyleValue<float> borderLeft { get; set; }

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x0600015C RID: 348
		// (set) Token: 0x0600015D RID: 349
		[Obsolete("Use borderTopWidth instead")]
		StyleValue<float> borderTop { get; set; }

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600015E RID: 350
		// (set) Token: 0x0600015F RID: 351
		[Obsolete("Use borderRightWidth instead")]
		StyleValue<float> borderRight { get; set; }

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000160 RID: 352
		// (set) Token: 0x06000161 RID: 353
		[Obsolete("Use borderBottomWidth instead")]
		StyleValue<float> borderBottom { get; set; }

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000162 RID: 354
		// (set) Token: 0x06000163 RID: 355
		StyleValue<float> paddingLeft { get; set; }

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000164 RID: 356
		// (set) Token: 0x06000165 RID: 357
		StyleValue<float> paddingTop { get; set; }

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000166 RID: 358
		// (set) Token: 0x06000167 RID: 359
		StyleValue<float> paddingRight { get; set; }

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000168 RID: 360
		// (set) Token: 0x06000169 RID: 361
		StyleValue<float> paddingBottom { get; set; }

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x0600016A RID: 362
		// (set) Token: 0x0600016B RID: 363
		StyleValue<PositionType> positionType { get; set; }

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x0600016C RID: 364
		// (set) Token: 0x0600016D RID: 365
		StyleValue<Align> alignSelf { get; set; }

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x0600016E RID: 366
		// (set) Token: 0x0600016F RID: 367
		[Obsolete("Use unityTextAlign instead")]
		StyleValue<TextAnchor> textAlignment { get; set; }

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000170 RID: 368
		// (set) Token: 0x06000171 RID: 369
		StyleValue<TextAnchor> unityTextAlign { get; set; }

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000172 RID: 370
		// (set) Token: 0x06000173 RID: 371
		[Obsolete("Use fontStyleAndWeight instead")]
		StyleValue<FontStyle> fontStyle { get; set; }

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000174 RID: 372
		// (set) Token: 0x06000175 RID: 373
		StyleValue<FontStyle> fontStyleAndWeight { get; set; }

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000176 RID: 374
		// (set) Token: 0x06000177 RID: 375
		StyleValue<TextClipping> textClipping { get; set; }

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000178 RID: 376
		// (set) Token: 0x06000179 RID: 377
		StyleValue<Font> font { get; set; }

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x0600017A RID: 378
		// (set) Token: 0x0600017B RID: 379
		StyleValue<int> fontSize { get; set; }

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x0600017C RID: 380
		// (set) Token: 0x0600017D RID: 381
		StyleValue<bool> wordWrap { get; set; }

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600017E RID: 382
		// (set) Token: 0x0600017F RID: 383
		[Obsolete("Use color instead")]
		StyleValue<Color> textColor { get; set; }

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000180 RID: 384
		// (set) Token: 0x06000181 RID: 385
		StyleValue<Color> color { get; set; }

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000182 RID: 386
		// (set) Token: 0x06000183 RID: 387
		StyleValue<Color> backgroundColor { get; set; }

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000184 RID: 388
		// (set) Token: 0x06000185 RID: 389
		StyleValue<Color> borderColor { get; set; }

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000186 RID: 390
		// (set) Token: 0x06000187 RID: 391
		StyleValue<Texture2D> backgroundImage { get; set; }

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000188 RID: 392
		// (set) Token: 0x06000189 RID: 393
		[Obsolete("Use backgroundScaleMode instead")]
		StyleValue<ScaleMode> backgroundSize { get; set; }

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x0600018A RID: 394
		// (set) Token: 0x0600018B RID: 395
		StyleValue<ScaleMode> backgroundScaleMode { get; set; }

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x0600018C RID: 396
		// (set) Token: 0x0600018D RID: 397
		StyleValue<Align> alignItems { get; set; }

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x0600018E RID: 398
		// (set) Token: 0x0600018F RID: 399
		StyleValue<Align> alignContent { get; set; }

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000190 RID: 400
		// (set) Token: 0x06000191 RID: 401
		StyleValue<Justify> justifyContent { get; set; }

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000192 RID: 402
		// (set) Token: 0x06000193 RID: 403
		StyleValue<float> borderLeftWidth { get; set; }

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000194 RID: 404
		// (set) Token: 0x06000195 RID: 405
		StyleValue<float> borderTopWidth { get; set; }

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000196 RID: 406
		// (set) Token: 0x06000197 RID: 407
		StyleValue<float> borderRightWidth { get; set; }

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000198 RID: 408
		// (set) Token: 0x06000199 RID: 409
		StyleValue<float> borderBottomWidth { get; set; }

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x0600019A RID: 410
		// (set) Token: 0x0600019B RID: 411
		StyleValue<float> borderRadius { get; set; }

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x0600019C RID: 412
		// (set) Token: 0x0600019D RID: 413
		StyleValue<float> borderTopLeftRadius { get; set; }

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x0600019E RID: 414
		// (set) Token: 0x0600019F RID: 415
		StyleValue<float> borderTopRightRadius { get; set; }

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x060001A0 RID: 416
		// (set) Token: 0x060001A1 RID: 417
		StyleValue<float> borderBottomRightRadius { get; set; }

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060001A2 RID: 418
		// (set) Token: 0x060001A3 RID: 419
		StyleValue<float> borderBottomLeftRadius { get; set; }

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x060001A4 RID: 420
		// (set) Token: 0x060001A5 RID: 421
		StyleValue<int> sliceLeft { get; set; }

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060001A6 RID: 422
		// (set) Token: 0x060001A7 RID: 423
		StyleValue<int> sliceTop { get; set; }

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x060001A8 RID: 424
		// (set) Token: 0x060001A9 RID: 425
		StyleValue<int> sliceRight { get; set; }

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x060001AA RID: 426
		// (set) Token: 0x060001AB RID: 427
		StyleValue<int> sliceBottom { get; set; }

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x060001AC RID: 428
		// (set) Token: 0x060001AD RID: 429
		StyleValue<float> opacity { get; set; }

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x060001AE RID: 430
		// (set) Token: 0x060001AF RID: 431
		StyleValue<CursorStyle> cursor { get; set; }

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x060001B0 RID: 432
		// (set) Token: 0x060001B1 RID: 433
		StyleValue<Visibility> visibility { get; set; }
	}
}
