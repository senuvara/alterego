﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000075 RID: 117
	internal class TimerEventScheduler : IScheduler
	{
		// Token: 0x06000326 RID: 806 RVA: 0x0000ACD1 File Offset: 0x00008ED1
		public TimerEventScheduler()
		{
		}

		// Token: 0x06000327 RID: 807 RVA: 0x0000AD08 File Offset: 0x00008F08
		public void Schedule(IScheduledItem item)
		{
			if (item != null)
			{
				ScheduledItem scheduledItem = item as ScheduledItem;
				if (scheduledItem == null)
				{
					throw new NotSupportedException("Scheduled Item type is not supported by this scheduler");
				}
				if (this.m_TransactionMode)
				{
					if (!this.m_UnscheduleTransactions.Remove(scheduledItem))
					{
						if (this.m_ScheduledItems.Contains(scheduledItem) || this.m_ScheduleTransactions.Contains(scheduledItem))
						{
							throw new ArgumentException("Cannot schedule function " + scheduledItem + " more than once");
						}
						this.m_ScheduleTransactions.Add(scheduledItem);
					}
				}
				else
				{
					if (this.m_ScheduledItems.Contains(scheduledItem))
					{
						throw new ArgumentException("Cannot schedule function " + scheduledItem + " more than once");
					}
					this.m_ScheduledItems.Add(scheduledItem);
				}
			}
		}

		// Token: 0x06000328 RID: 808 RVA: 0x0000ADE4 File Offset: 0x00008FE4
		public IScheduledItem ScheduleOnce(Action<TimerState> timerUpdateEvent, long delayMs)
		{
			TimerEventScheduler.TimerEventSchedulerItem timerEventSchedulerItem = new TimerEventScheduler.TimerEventSchedulerItem(timerUpdateEvent)
			{
				delayMs = delayMs
			};
			this.Schedule(timerEventSchedulerItem);
			return timerEventSchedulerItem;
		}

		// Token: 0x06000329 RID: 809 RVA: 0x0000AE14 File Offset: 0x00009014
		public IScheduledItem ScheduleUntil(Action<TimerState> timerUpdateEvent, long delayMs, long intervalMs, Func<bool> stopCondition)
		{
			TimerEventScheduler.TimerEventSchedulerItem timerEventSchedulerItem = new TimerEventScheduler.TimerEventSchedulerItem(timerUpdateEvent)
			{
				delayMs = delayMs,
				intervalMs = intervalMs,
				timerUpdateStopCondition = stopCondition
			};
			this.Schedule(timerEventSchedulerItem);
			return timerEventSchedulerItem;
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000AE50 File Offset: 0x00009050
		public IScheduledItem ScheduleForDuration(Action<TimerState> timerUpdateEvent, long delayMs, long intervalMs, long durationMs)
		{
			TimerEventScheduler.TimerEventSchedulerItem timerEventSchedulerItem = new TimerEventScheduler.TimerEventSchedulerItem(timerUpdateEvent)
			{
				delayMs = delayMs,
				intervalMs = intervalMs,
				timerUpdateStopCondition = null
			};
			timerEventSchedulerItem.SetDuration(durationMs);
			this.Schedule(timerEventSchedulerItem);
			return timerEventSchedulerItem;
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000AE94 File Offset: 0x00009094
		private bool RemovedScheduledItemAt(int index)
		{
			bool result;
			if (index >= 0)
			{
				ScheduledItem scheduledItem = this.m_ScheduledItems[index];
				this.m_ScheduledItems.RemoveAt(index);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000AED4 File Offset: 0x000090D4
		public void Unschedule(IScheduledItem item)
		{
			ScheduledItem scheduledItem = item as ScheduledItem;
			if (scheduledItem != null)
			{
				if (this.m_TransactionMode)
				{
					if (this.m_UnscheduleTransactions.Contains(scheduledItem))
					{
						throw new ArgumentException("Cannot unschedule scheduled function twice" + scheduledItem);
					}
					if (!this.m_ScheduleTransactions.Remove(scheduledItem))
					{
						if (!this.m_ScheduledItems.Contains(scheduledItem))
						{
							throw new ArgumentException("Cannot unschedule unknown scheduled function " + scheduledItem);
						}
						this.m_UnscheduleTransactions.Add(scheduledItem);
					}
				}
				else if (!this.PrivateUnSchedule(scheduledItem))
				{
					throw new ArgumentException("Cannot unschedule unknown scheduled function " + scheduledItem);
				}
				scheduledItem.OnItemUnscheduled();
			}
		}

		// Token: 0x0600032D RID: 813 RVA: 0x0000AF9C File Offset: 0x0000919C
		private bool PrivateUnSchedule(ScheduledItem sItem)
		{
			return this.m_ScheduleTransactions.Remove(sItem) || this.RemovedScheduledItemAt(this.m_ScheduledItems.IndexOf(sItem));
		}

		// Token: 0x0600032E RID: 814 RVA: 0x0000AFD8 File Offset: 0x000091D8
		public void UpdateScheduledEvents()
		{
			try
			{
				this.m_TransactionMode = true;
				long num = Panel.TimeSinceStartupMs();
				int count = this.m_ScheduledItems.Count;
				int num2 = this.m_LastUpdatedIndex + 1;
				if (num2 >= count)
				{
					num2 = 0;
				}
				for (int i = 0; i < count; i++)
				{
					int num3 = num2 + i;
					if (num3 >= count)
					{
						num3 -= count;
					}
					ScheduledItem scheduledItem = this.m_ScheduledItems[num3];
					if (num - scheduledItem.delayMs >= scheduledItem.startMs)
					{
						TimerState state = new TimerState
						{
							start = scheduledItem.startMs,
							now = num
						};
						if (!this.m_UnscheduleTransactions.Contains(scheduledItem))
						{
							scheduledItem.PerformTimerUpdate(state);
						}
						scheduledItem.startMs = num;
						scheduledItem.delayMs = scheduledItem.intervalMs;
						if (scheduledItem.ShouldUnschedule() && !this.m_UnscheduleTransactions.Contains(scheduledItem))
						{
							this.Unschedule(scheduledItem);
						}
					}
					this.m_LastUpdatedIndex = num3;
				}
			}
			finally
			{
				this.m_TransactionMode = false;
				foreach (ScheduledItem sItem in this.m_UnscheduleTransactions)
				{
					this.PrivateUnSchedule(sItem);
				}
				this.m_UnscheduleTransactions.Clear();
				foreach (ScheduledItem item in this.m_ScheduleTransactions)
				{
					this.Schedule(item);
				}
				this.m_ScheduleTransactions.Clear();
			}
		}

		// Token: 0x0400013A RID: 314
		private readonly List<ScheduledItem> m_ScheduledItems = new List<ScheduledItem>();

		// Token: 0x0400013B RID: 315
		private bool m_TransactionMode;

		// Token: 0x0400013C RID: 316
		private readonly List<ScheduledItem> m_ScheduleTransactions = new List<ScheduledItem>();

		// Token: 0x0400013D RID: 317
		private readonly HashSet<ScheduledItem> m_UnscheduleTransactions = new HashSet<ScheduledItem>();

		// Token: 0x0400013E RID: 318
		internal bool disableThrottling = false;

		// Token: 0x0400013F RID: 319
		private int m_LastUpdatedIndex = -1;

		// Token: 0x02000076 RID: 118
		private class TimerEventSchedulerItem : ScheduledItem
		{
			// Token: 0x0600032F RID: 815 RVA: 0x0000B1E4 File Offset: 0x000093E4
			public TimerEventSchedulerItem(Action<TimerState> updateEvent)
			{
				this.m_TimerUpdateEvent = updateEvent;
			}

			// Token: 0x06000330 RID: 816 RVA: 0x0000B1F4 File Offset: 0x000093F4
			public override void PerformTimerUpdate(TimerState state)
			{
				if (this.m_TimerUpdateEvent != null)
				{
					this.m_TimerUpdateEvent(state);
				}
			}

			// Token: 0x06000331 RID: 817 RVA: 0x0000B210 File Offset: 0x00009410
			public override string ToString()
			{
				return this.m_TimerUpdateEvent.ToString();
			}

			// Token: 0x04000140 RID: 320
			private readonly Action<TimerState> m_TimerUpdateEvent;
		}
	}
}
