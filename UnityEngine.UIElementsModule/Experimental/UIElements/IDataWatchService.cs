﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000038 RID: 56
	internal interface IDataWatchService
	{
		// Token: 0x06000106 RID: 262
		IDataWatchHandle AddWatch(Object watched, Action<Object> onDataChanged);

		// Token: 0x06000107 RID: 263
		void RemoveWatch(IDataWatchHandle handle);

		// Token: 0x06000108 RID: 264
		void ForceDirtyNextPoll(Object obj);
	}
}
