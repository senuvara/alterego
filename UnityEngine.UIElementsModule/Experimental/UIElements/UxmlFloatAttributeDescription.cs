﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000161 RID: 353
	public class UxmlFloatAttributeDescription : UxmlAttributeDescription
	{
		// Token: 0x0600090C RID: 2316 RVA: 0x0001FA82 File Offset: 0x0001DC82
		public UxmlFloatAttributeDescription()
		{
			base.type = "float";
			base.typeNamespace = "http://www.w3.org/2001/XMLSchema";
			this.defaultValue = 0f;
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x0600090D RID: 2317 RVA: 0x0001FAAC File Offset: 0x0001DCAC
		// (set) Token: 0x0600090E RID: 2318 RVA: 0x0001FAC6 File Offset: 0x0001DCC6
		public float defaultValue
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultValue>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultValue>k__BackingField = value;
			}
		}

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x0600090F RID: 2319 RVA: 0x0001FAD0 File Offset: 0x0001DCD0
		public override string defaultValueAsString
		{
			get
			{
				return this.defaultValue.ToString();
			}
		}

		// Token: 0x06000910 RID: 2320 RVA: 0x0001FAFC File Offset: 0x0001DCFC
		[Obsolete("Pass a creation context to the method.")]
		public float GetValueFromBag(IUxmlAttributes bag)
		{
			return this.GetValueFromBag(bag, default(CreationContext));
		}

		// Token: 0x06000911 RID: 2321 RVA: 0x0001FB24 File Offset: 0x0001DD24
		public float GetValueFromBag(IUxmlAttributes bag, CreationContext cc)
		{
			if (UxmlFloatAttributeDescription.<>f__mg$cache0 == null)
			{
				UxmlFloatAttributeDescription.<>f__mg$cache0 = new Func<string, float, float>(UxmlFloatAttributeDescription.ConvertValueToFloat);
			}
			return base.GetValueFromBag<float>(bag, cc, UxmlFloatAttributeDescription.<>f__mg$cache0, this.defaultValue);
		}

		// Token: 0x06000912 RID: 2322 RVA: 0x0001FB64 File Offset: 0x0001DD64
		private static float ConvertValueToFloat(string v, float defaultValue)
		{
			float num;
			float result;
			if (v == null || !float.TryParse(v, out num))
			{
				result = defaultValue;
			}
			else
			{
				result = num;
			}
			return result;
		}

		// Token: 0x04000416 RID: 1046
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private float <defaultValue>k__BackingField;

		// Token: 0x04000417 RID: 1047
		[CompilerGenerated]
		private static Func<string, float, float> <>f__mg$cache0;
	}
}
