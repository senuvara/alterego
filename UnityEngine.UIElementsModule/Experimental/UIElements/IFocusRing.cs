﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000032 RID: 50
	public interface IFocusRing
	{
		// Token: 0x060000E2 RID: 226
		FocusChangeDirection GetFocusChangeDirection(Focusable currentFocusable, EventBase e);

		// Token: 0x060000E3 RID: 227
		Focusable GetNextFocusable(Focusable currentFocusable, FocusChangeDirection direction);
	}
}
