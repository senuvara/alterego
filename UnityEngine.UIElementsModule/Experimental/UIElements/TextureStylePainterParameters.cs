﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000097 RID: 151
	internal struct TextureStylePainterParameters
	{
		// Token: 0x060003C9 RID: 969 RVA: 0x0000CFD8 File Offset: 0x0000B1D8
		public static TextureStylePainterParameters GetDefault(VisualElement ve)
		{
			IStyle style = ve.style;
			TextureStylePainterParameters result = new TextureStylePainterParameters
			{
				rect = GUIUtility.AlignRectToDevice(ve.rect),
				uv = new Rect(0f, 0f, 1f, 1f),
				color = Color.white,
				texture = style.backgroundImage,
				scaleMode = style.backgroundScaleMode,
				sliceLeft = style.sliceLeft,
				sliceTop = style.sliceTop,
				sliceRight = style.sliceRight,
				sliceBottom = style.sliceBottom
			};
			BorderParameters.SetFromStyle(ref result.border, style);
			return result;
		}

		// Token: 0x040001A7 RID: 423
		public Rect rect;

		// Token: 0x040001A8 RID: 424
		public Rect uv;

		// Token: 0x040001A9 RID: 425
		public Color color;

		// Token: 0x040001AA RID: 426
		public Texture texture;

		// Token: 0x040001AB RID: 427
		public ScaleMode scaleMode;

		// Token: 0x040001AC RID: 428
		public BorderParameters border;

		// Token: 0x040001AD RID: 429
		public int sliceLeft;

		// Token: 0x040001AE RID: 430
		public int sliceTop;

		// Token: 0x040001AF RID: 431
		public int sliceRight;

		// Token: 0x040001B0 RID: 432
		public int sliceBottom;

		// Token: 0x040001B1 RID: 433
		public bool usePremultiplyAlpha;
	}
}
