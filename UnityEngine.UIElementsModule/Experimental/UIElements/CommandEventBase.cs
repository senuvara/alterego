﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000108 RID: 264
	public abstract class CommandEventBase<T> : EventBase<T>, ICommandEvent, IPropagatableEvent where T : CommandEventBase<T>, new()
	{
		// Token: 0x06000756 RID: 1878 RVA: 0x00019AB0 File Offset: 0x00017CB0
		protected CommandEventBase()
		{
			this.Init();
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x06000757 RID: 1879 RVA: 0x00019C04 File Offset: 0x00017E04
		// (set) Token: 0x06000758 RID: 1880 RVA: 0x00019C47 File Offset: 0x00017E47
		public string commandName
		{
			get
			{
				string commandName;
				if (this.m_CommandName == null && base.imguiEvent != null)
				{
					commandName = base.imguiEvent.commandName;
				}
				else
				{
					commandName = this.m_CommandName;
				}
				return commandName;
			}
			protected set
			{
				this.m_CommandName = value;
			}
		}

		// Token: 0x06000759 RID: 1881 RVA: 0x00019C51 File Offset: 0x00017E51
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown | EventBase.EventFlags.Cancellable);
			this.commandName = null;
		}

		// Token: 0x0600075A RID: 1882 RVA: 0x00019C68 File Offset: 0x00017E68
		public static T GetPooled(Event systemEvent)
		{
			T pooled = EventBase<T>.GetPooled();
			pooled.imguiEvent = systemEvent;
			return pooled;
		}

		// Token: 0x0600075B RID: 1883 RVA: 0x00019C94 File Offset: 0x00017E94
		public static T GetPooled(string commandName)
		{
			T pooled = EventBase<T>.GetPooled();
			pooled.commandName = commandName;
			return pooled;
		}

		// Token: 0x040002DA RID: 730
		private string m_CommandName;
	}
}
