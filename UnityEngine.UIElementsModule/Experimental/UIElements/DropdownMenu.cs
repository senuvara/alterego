﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200001E RID: 30
	public class DropdownMenu
	{
		// Token: 0x06000090 RID: 144 RVA: 0x00003B1A File Offset: 0x00001D1A
		public DropdownMenu()
		{
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00003B30 File Offset: 0x00001D30
		public List<DropdownMenu.MenuItem> MenuItems()
		{
			return this.menuItems;
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00003B4C File Offset: 0x00001D4C
		public void AppendAction(string actionName, Action<DropdownMenu.MenuAction> action, Func<DropdownMenu.MenuAction, DropdownMenu.MenuAction.StatusFlags> actionStatusCallback, object userData = null)
		{
			DropdownMenu.MenuAction item = new DropdownMenu.MenuAction(actionName, action, actionStatusCallback, userData);
			this.menuItems.Add(item);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00003B74 File Offset: 0x00001D74
		public void InsertAction(int atIndex, string actionName, Action<DropdownMenu.MenuAction> action, Func<DropdownMenu.MenuAction, DropdownMenu.MenuAction.StatusFlags> actionStatusCallback, object userData = null)
		{
			DropdownMenu.MenuAction item = new DropdownMenu.MenuAction(actionName, action, actionStatusCallback, userData);
			this.menuItems.Insert(atIndex, item);
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00003B9C File Offset: 0x00001D9C
		public void AppendSeparator(string subMenuPath = null)
		{
			if (this.menuItems.Count > 0 && !(this.menuItems[this.menuItems.Count - 1] is DropdownMenu.Separator))
			{
				DropdownMenu.Separator item = new DropdownMenu.Separator((subMenuPath != null) ? subMenuPath : string.Empty);
				this.menuItems.Add(item);
			}
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00003C04 File Offset: 0x00001E04
		public void InsertSeparator(string subMenuPath, int atIndex)
		{
			if (atIndex > 0 && atIndex <= this.menuItems.Count && !(this.menuItems[atIndex - 1] is DropdownMenu.Separator))
			{
				DropdownMenu.Separator item = new DropdownMenu.Separator((subMenuPath != null) ? subMenuPath : string.Empty);
				this.menuItems.Insert(atIndex, item);
			}
		}

		// Token: 0x06000096 RID: 150 RVA: 0x00003C68 File Offset: 0x00001E68
		public void RemoveItemAt(int index)
		{
			this.menuItems.RemoveAt(index);
		}

		// Token: 0x06000097 RID: 151 RVA: 0x00003C78 File Offset: 0x00001E78
		public void PrepareForDisplay(EventBase e)
		{
			this.m_EventInfo = ((e == null) ? null : new DropdownMenu.EventInfo(e));
			if (this.menuItems.Count != 0)
			{
				foreach (DropdownMenu.MenuItem menuItem in this.menuItems)
				{
					DropdownMenu.MenuAction menuAction = menuItem as DropdownMenu.MenuAction;
					if (menuAction != null)
					{
						menuAction.UpdateActionStatus(this.m_EventInfo);
					}
				}
				if (this.menuItems[this.menuItems.Count - 1] is DropdownMenu.Separator)
				{
					this.menuItems.RemoveAt(this.menuItems.Count - 1);
				}
			}
		}

		// Token: 0x04000035 RID: 53
		private List<DropdownMenu.MenuItem> menuItems = new List<DropdownMenu.MenuItem>();

		// Token: 0x04000036 RID: 54
		private DropdownMenu.EventInfo m_EventInfo;

		// Token: 0x0200001F RID: 31
		public class EventInfo
		{
			// Token: 0x06000098 RID: 152 RVA: 0x00003D58 File Offset: 0x00001F58
			public EventInfo(EventBase e)
			{
				IMouseEvent mouseEvent = e as IMouseEvent;
				if (mouseEvent != null)
				{
					this.mousePosition = mouseEvent.mousePosition;
					this.localMousePosition = mouseEvent.localMousePosition;
					this.modifiers = mouseEvent.modifiers;
					this.character = 0;
					this.keyCode = 0;
				}
				else
				{
					IKeyboardEvent keyboardEvent = e as IKeyboardEvent;
					if (keyboardEvent != null)
					{
						this.character = keyboardEvent.character;
						this.keyCode = keyboardEvent.keyCode;
						this.modifiers = keyboardEvent.modifiers;
						this.mousePosition = Vector2.zero;
						this.localMousePosition = Vector2.zero;
					}
				}
			}

			// Token: 0x1700001D RID: 29
			// (get) Token: 0x06000099 RID: 153 RVA: 0x00003E00 File Offset: 0x00002000
			public EventModifiers modifiers
			{
				[CompilerGenerated]
				get
				{
					return this.<modifiers>k__BackingField;
				}
			}

			// Token: 0x1700001E RID: 30
			// (get) Token: 0x0600009A RID: 154 RVA: 0x00003E1C File Offset: 0x0000201C
			public Vector2 mousePosition
			{
				[CompilerGenerated]
				get
				{
					return this.<mousePosition>k__BackingField;
				}
			}

			// Token: 0x1700001F RID: 31
			// (get) Token: 0x0600009B RID: 155 RVA: 0x00003E38 File Offset: 0x00002038
			public Vector2 localMousePosition
			{
				[CompilerGenerated]
				get
				{
					return this.<localMousePosition>k__BackingField;
				}
			}

			// Token: 0x17000020 RID: 32
			// (get) Token: 0x0600009C RID: 156 RVA: 0x00003E54 File Offset: 0x00002054
			private char character
			{
				[CompilerGenerated]
				get
				{
					return this.<character>k__BackingField;
				}
			}

			// Token: 0x17000021 RID: 33
			// (get) Token: 0x0600009D RID: 157 RVA: 0x00003E70 File Offset: 0x00002070
			private KeyCode keyCode
			{
				[CompilerGenerated]
				get
				{
					return this.<keyCode>k__BackingField;
				}
			}

			// Token: 0x04000037 RID: 55
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private readonly EventModifiers <modifiers>k__BackingField;

			// Token: 0x04000038 RID: 56
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private readonly Vector2 <mousePosition>k__BackingField;

			// Token: 0x04000039 RID: 57
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private readonly Vector2 <localMousePosition>k__BackingField;

			// Token: 0x0400003A RID: 58
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private readonly char <character>k__BackingField;

			// Token: 0x0400003B RID: 59
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private readonly KeyCode <keyCode>k__BackingField;
		}

		// Token: 0x02000020 RID: 32
		public abstract class MenuItem
		{
			// Token: 0x0600009E RID: 158 RVA: 0x00002223 File Offset: 0x00000423
			protected MenuItem()
			{
			}
		}

		// Token: 0x02000021 RID: 33
		public class Separator : DropdownMenu.MenuItem
		{
			// Token: 0x0600009F RID: 159 RVA: 0x00003E8A File Offset: 0x0000208A
			public Separator(string subMenuPath)
			{
				this.subMenuPath = subMenuPath;
			}

			// Token: 0x0400003C RID: 60
			public string subMenuPath;
		}

		// Token: 0x02000022 RID: 34
		public class MenuAction : DropdownMenu.MenuItem
		{
			// Token: 0x060000A0 RID: 160 RVA: 0x00003E9A File Offset: 0x0000209A
			public MenuAction(string actionName, Action<DropdownMenu.MenuAction> actionCallback, Func<DropdownMenu.MenuAction, DropdownMenu.MenuAction.StatusFlags> actionStatusCallback, object userData = null)
			{
				this.name = actionName;
				this.actionCallback = actionCallback;
				this.actionStatusCallback = actionStatusCallback;
				this.userData = userData;
			}

			// Token: 0x17000022 RID: 34
			// (get) Token: 0x060000A1 RID: 161 RVA: 0x00003EC0 File Offset: 0x000020C0
			// (set) Token: 0x060000A2 RID: 162 RVA: 0x00003EDA File Offset: 0x000020DA
			public DropdownMenu.MenuAction.StatusFlags status
			{
				[CompilerGenerated]
				get
				{
					return this.<status>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<status>k__BackingField = value;
				}
			}

			// Token: 0x17000023 RID: 35
			// (get) Token: 0x060000A3 RID: 163 RVA: 0x00003EE4 File Offset: 0x000020E4
			// (set) Token: 0x060000A4 RID: 164 RVA: 0x00003EFE File Offset: 0x000020FE
			public DropdownMenu.EventInfo eventInfo
			{
				[CompilerGenerated]
				get
				{
					return this.<eventInfo>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<eventInfo>k__BackingField = value;
				}
			}

			// Token: 0x17000024 RID: 36
			// (get) Token: 0x060000A5 RID: 165 RVA: 0x00003F08 File Offset: 0x00002108
			// (set) Token: 0x060000A6 RID: 166 RVA: 0x00003F22 File Offset: 0x00002122
			public object userData
			{
				[CompilerGenerated]
				get
				{
					return this.<userData>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<userData>k__BackingField = value;
				}
			}

			// Token: 0x060000A7 RID: 167 RVA: 0x00003F2C File Offset: 0x0000212C
			public static DropdownMenu.MenuAction.StatusFlags AlwaysEnabled(DropdownMenu.MenuAction a)
			{
				return DropdownMenu.MenuAction.StatusFlags.Normal;
			}

			// Token: 0x060000A8 RID: 168 RVA: 0x00003F44 File Offset: 0x00002144
			public static DropdownMenu.MenuAction.StatusFlags AlwaysDisabled(DropdownMenu.MenuAction a)
			{
				return DropdownMenu.MenuAction.StatusFlags.Disabled;
			}

			// Token: 0x060000A9 RID: 169 RVA: 0x00003F5C File Offset: 0x0000215C
			public void UpdateActionStatus(DropdownMenu.EventInfo eventInfo)
			{
				this.eventInfo = eventInfo;
				DropdownMenu.MenuAction.StatusFlags? statusFlags = (this.actionStatusCallback != null) ? new DropdownMenu.MenuAction.StatusFlags?(this.actionStatusCallback(this)) : null;
				this.status = ((statusFlags == null) ? DropdownMenu.MenuAction.StatusFlags.Hidden : statusFlags.Value);
			}

			// Token: 0x060000AA RID: 170 RVA: 0x00003FB8 File Offset: 0x000021B8
			public void Execute()
			{
				if (this.actionCallback != null)
				{
					this.actionCallback(this);
				}
			}

			// Token: 0x0400003D RID: 61
			public string name;

			// Token: 0x0400003E RID: 62
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private DropdownMenu.MenuAction.StatusFlags <status>k__BackingField;

			// Token: 0x0400003F RID: 63
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private DropdownMenu.EventInfo <eventInfo>k__BackingField;

			// Token: 0x04000040 RID: 64
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private object <userData>k__BackingField;

			// Token: 0x04000041 RID: 65
			private Action<DropdownMenu.MenuAction> actionCallback;

			// Token: 0x04000042 RID: 66
			private Func<DropdownMenu.MenuAction, DropdownMenu.MenuAction.StatusFlags> actionStatusCallback;

			// Token: 0x02000023 RID: 35
			[Flags]
			public enum StatusFlags
			{
				// Token: 0x04000044 RID: 68
				Normal = 0,
				// Token: 0x04000045 RID: 69
				Disabled = 1,
				// Token: 0x04000046 RID: 70
				Checked = 2,
				// Token: 0x04000047 RID: 71
				Hidden = 4
			}
		}
	}
}
