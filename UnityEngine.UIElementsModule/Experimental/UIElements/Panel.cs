﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000064 RID: 100
	internal class Panel : BaseVisualElementPanel
	{
		// Token: 0x060002B5 RID: 693 RVA: 0x00009934 File Offset: 0x00007B34
		public Panel(ScriptableObject ownerObject, ContextType contextType, IDataWatchService dataWatch = null, EventDispatcher dispatcher = null)
		{
			this.m_VisualTreeUpdater = new VisualTreeUpdater(this);
			this.ownerObject = ownerObject;
			this.contextType = contextType;
			this.m_DataWatch = dataWatch;
			this.dispatcher = (dispatcher ?? EventDispatcher.instance);
			this.repaintData = new RepaintData();
			this.cursorManager = new CursorManager();
			this.contextualMenuManager = null;
			this.m_RootContainer = new VisualElement();
			this.m_RootContainer.name = VisualElementUtils.GetUniqueName("PanelContainer");
			this.m_RootContainer.persistenceKey = "PanelContainer";
			this.visualTree.SetPanel(this);
			this.focusController = new FocusController(new VisualElementFocusRing(this.visualTree, VisualElementFocusRing.DefaultFocusOrder.ChildOrder));
			this.m_ProfileUpdateName = "PanelUpdate";
			this.m_ProfileLayoutName = "PanelLayout";
			this.m_ProfileBindingsName = "PanelBindings";
			base.allowPixelCaching = true;
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x060002B6 RID: 694 RVA: 0x00009A24 File Offset: 0x00007C24
		public override VisualElement visualTree
		{
			get
			{
				return this.m_RootContainer;
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x060002B7 RID: 695 RVA: 0x00009A40 File Offset: 0x00007C40
		// (set) Token: 0x060002B8 RID: 696 RVA: 0x00009A5A File Offset: 0x00007C5A
		public override EventDispatcher dispatcher
		{
			[CompilerGenerated]
			get
			{
				return this.<dispatcher>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<dispatcher>k__BackingField = value;
			}
		}

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x060002B9 RID: 697 RVA: 0x00009A64 File Offset: 0x00007C64
		internal override IDataWatchService dataWatch
		{
			get
			{
				return this.m_DataWatch;
			}
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x060002BA RID: 698 RVA: 0x00009A80 File Offset: 0x00007C80
		public TimerEventScheduler timerEventScheduler
		{
			get
			{
				TimerEventScheduler result;
				if ((result = this.m_Scheduler) == null)
				{
					result = (this.m_Scheduler = new TimerEventScheduler());
				}
				return result;
			}
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x060002BB RID: 699 RVA: 0x00009AB0 File Offset: 0x00007CB0
		internal override IScheduler scheduler
		{
			get
			{
				return this.timerEventScheduler;
			}
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x060002BC RID: 700 RVA: 0x00009ACC File Offset: 0x00007CCC
		// (set) Token: 0x060002BD RID: 701 RVA: 0x00009AE6 File Offset: 0x00007CE6
		public override ScriptableObject ownerObject
		{
			[CompilerGenerated]
			get
			{
				return this.<ownerObject>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<ownerObject>k__BackingField = value;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x060002BE RID: 702 RVA: 0x00009AF0 File Offset: 0x00007CF0
		// (set) Token: 0x060002BF RID: 703 RVA: 0x00009B0A File Offset: 0x00007D0A
		public override ContextType contextType
		{
			[CompilerGenerated]
			get
			{
				return this.<contextType>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<contextType>k__BackingField = value;
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x060002C0 RID: 704 RVA: 0x00009B14 File Offset: 0x00007D14
		// (set) Token: 0x060002C1 RID: 705 RVA: 0x00009B2E File Offset: 0x00007D2E
		public override SavePersistentViewData savePersistentViewData
		{
			[CompilerGenerated]
			get
			{
				return this.<savePersistentViewData>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<savePersistentViewData>k__BackingField = value;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x060002C2 RID: 706 RVA: 0x00009B38 File Offset: 0x00007D38
		// (set) Token: 0x060002C3 RID: 707 RVA: 0x00009B52 File Offset: 0x00007D52
		public override GetViewDataDictionary getViewDataDictionary
		{
			[CompilerGenerated]
			get
			{
				return this.<getViewDataDictionary>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<getViewDataDictionary>k__BackingField = value;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x060002C4 RID: 708 RVA: 0x00009B5C File Offset: 0x00007D5C
		// (set) Token: 0x060002C5 RID: 709 RVA: 0x00009B76 File Offset: 0x00007D76
		public override FocusController focusController
		{
			[CompilerGenerated]
			get
			{
				return this.<focusController>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<focusController>k__BackingField = value;
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x060002C6 RID: 710 RVA: 0x00009B80 File Offset: 0x00007D80
		// (set) Token: 0x060002C7 RID: 711 RVA: 0x00009B9A File Offset: 0x00007D9A
		public override EventInterests IMGUIEventInterests
		{
			[CompilerGenerated]
			get
			{
				return this.<IMGUIEventInterests>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IMGUIEventInterests>k__BackingField = value;
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x060002C8 RID: 712 RVA: 0x00009BA4 File Offset: 0x00007DA4
		// (set) Token: 0x060002C9 RID: 713 RVA: 0x00009BC0 File Offset: 0x00007DC0
		internal string name
		{
			get
			{
				return this.m_PanelName;
			}
			set
			{
				this.m_PanelName = value;
				if (!string.IsNullOrEmpty(this.m_PanelName))
				{
					this.m_ProfileUpdateName = string.Format("PanelUpdate.{0}", this.m_PanelName);
					this.m_ProfileLayoutName = string.Format("PanelLayout.{0}", this.m_PanelName);
					this.m_ProfileBindingsName = string.Format("PanelBindings.{0}", this.m_PanelName);
				}
				else
				{
					this.m_ProfileUpdateName = "PanelUpdate";
					this.m_ProfileLayoutName = "PanelLayout";
					this.m_ProfileBindingsName = "PanelBindings";
				}
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x060002CA RID: 714 RVA: 0x00009C54 File Offset: 0x00007E54
		// (set) Token: 0x060002CB RID: 715 RVA: 0x00009C6E File Offset: 0x00007E6E
		internal static TimeMsFunction TimeSinceStartup
		{
			get
			{
				return Panel.s_TimeSinceStartup;
			}
			set
			{
				if (value == null)
				{
					if (Panel.<>f__mg$cache0 == null)
					{
						Panel.<>f__mg$cache0 = new TimeMsFunction(Panel.DefaultTimeSinceStartupMs);
					}
					value = Panel.<>f__mg$cache0;
				}
				Panel.s_TimeSinceStartup = value;
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060002CC RID: 716 RVA: 0x00009CA0 File Offset: 0x00007EA0
		// (set) Token: 0x060002CD RID: 717 RVA: 0x00009CBB File Offset: 0x00007EBB
		public override bool keepPixelCacheOnWorldBoundChange
		{
			get
			{
				return this.m_KeepPixelCacheOnWorldBoundChange;
			}
			set
			{
				if (this.m_KeepPixelCacheOnWorldBoundChange != value)
				{
					this.m_KeepPixelCacheOnWorldBoundChange = value;
					if (!value)
					{
						this.m_RootContainer.IncrementVersion(VersionChangeType.Transform | VersionChangeType.Repaint);
					}
				}
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060002CE RID: 718 RVA: 0x00009CEC File Offset: 0x00007EEC
		// (set) Token: 0x060002CF RID: 719 RVA: 0x00009D06 File Offset: 0x00007F06
		public override int IMGUIContainersCount
		{
			[CompilerGenerated]
			get
			{
				return this.<IMGUIContainersCount>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IMGUIContainersCount>k__BackingField = value;
			}
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x060002D0 RID: 720 RVA: 0x00009D10 File Offset: 0x00007F10
		internal override uint version
		{
			get
			{
				return this.m_Version;
			}
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x060002D1 RID: 721 RVA: 0x00009D2C File Offset: 0x00007F2C
		internal override uint repaintVersion
		{
			get
			{
				return this.m_RepaintVersion;
			}
		}

		// Token: 0x060002D2 RID: 722 RVA: 0x00009D47 File Offset: 0x00007F47
		protected override void Dispose(bool disposing)
		{
			if (!base.disposed)
			{
				if (disposing)
				{
					this.m_VisualTreeUpdater.Dispose();
				}
				base.Dispose(disposing);
			}
		}

		// Token: 0x060002D3 RID: 723 RVA: 0x00009D74 File Offset: 0x00007F74
		public static long TimeSinceStartupMs()
		{
			return (Panel.s_TimeSinceStartup != null) ? Panel.s_TimeSinceStartup() : Panel.DefaultTimeSinceStartupMs();
		}

		// Token: 0x060002D4 RID: 724 RVA: 0x00009DA8 File Offset: 0x00007FA8
		internal static long DefaultTimeSinceStartupMs()
		{
			return (long)(Time.realtimeSinceStartup * 1000f);
		}

		// Token: 0x060002D5 RID: 725 RVA: 0x00009DCC File Offset: 0x00007FCC
		internal static VisualElement PickAll(VisualElement root, Vector2 point, List<VisualElement> picked = null)
		{
			return Panel.PerformPick(root, point, picked);
		}

		// Token: 0x060002D6 RID: 726 RVA: 0x00009DEC File Offset: 0x00007FEC
		private static VisualElement PerformPick(VisualElement root, Vector2 point, List<VisualElement> picked = null)
		{
			VisualElement result;
			if (!root.visible)
			{
				result = null;
			}
			else if (root.pickingMode == PickingMode.Ignore && root.shadow.childCount == 0)
			{
				result = null;
			}
			else
			{
				Vector3 v = root.WorldToLocal(point);
				bool flag = root.ContainsPoint(v);
				if (!flag && root.ShouldClip())
				{
					result = null;
				}
				else
				{
					VisualElement visualElement = null;
					for (int i = root.shadow.childCount - 1; i >= 0; i--)
					{
						VisualElement root2 = root.shadow[i];
						VisualElement visualElement2 = Panel.PerformPick(root2, point, picked);
						if (visualElement == null && visualElement2 != null)
						{
							visualElement = visualElement2;
						}
					}
					if (picked != null && root.enabledInHierarchy && root.pickingMode == PickingMode.Position && flag)
					{
						picked.Add(root);
					}
					if (visualElement != null)
					{
						result = visualElement;
					}
					else
					{
						PickingMode pickingMode = root.pickingMode;
						if (pickingMode != PickingMode.Position)
						{
							if (pickingMode != PickingMode.Ignore)
							{
							}
						}
						else if (flag && root.enabledInHierarchy)
						{
							return root;
						}
						result = null;
					}
				}
			}
			return result;
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x00009F44 File Offset: 0x00008144
		public override VisualElement LoadTemplate(string path, Dictionary<string, VisualElement> slots = null)
		{
			VisualTreeAsset visualTreeAsset = Panel.loadResourceFunc(path, typeof(VisualTreeAsset)) as VisualTreeAsset;
			VisualElement result;
			if (visualTreeAsset == null)
			{
				result = null;
			}
			else
			{
				result = visualTreeAsset.CloneTree(slots);
			}
			return result;
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x00009F90 File Offset: 0x00008190
		public override VisualElement PickAll(Vector2 point, List<VisualElement> picked)
		{
			this.ValidateLayout();
			if (picked != null)
			{
				picked.Clear();
			}
			return Panel.PickAll(this.visualTree, point, picked);
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x00009FC4 File Offset: 0x000081C4
		public override VisualElement Pick(Vector2 point)
		{
			return Panel.PickAll(this.visualTree, point, null);
		}

		// Token: 0x060002DA RID: 730 RVA: 0x00009FE6 File Offset: 0x000081E6
		public override void ValidateLayout()
		{
			this.m_VisualTreeUpdater.UpdateVisualTreePhase(VisualTreeUpdatePhase.Styles);
			this.m_VisualTreeUpdater.UpdateVisualTreePhase(VisualTreeUpdatePhase.Layout);
			this.m_VisualTreeUpdater.UpdateVisualTreePhase(VisualTreeUpdatePhase.TransformClip);
		}

		// Token: 0x060002DB RID: 731 RVA: 0x0000A00D File Offset: 0x0000820D
		public override void UpdateBindings()
		{
			this.m_VisualTreeUpdater.UpdateVisualTreePhase(VisualTreeUpdatePhase.Bindings);
		}

		// Token: 0x060002DC RID: 732 RVA: 0x0000A01C File Offset: 0x0000821C
		public override void ApplyStyles()
		{
			this.m_VisualTreeUpdater.UpdateVisualTreePhase(VisualTreeUpdatePhase.Styles);
		}

		// Token: 0x060002DD RID: 733 RVA: 0x0000A02B File Offset: 0x0000822B
		public override void DirtyStyleSheets()
		{
			this.m_VisualTreeUpdater.DirtyStyleSheets();
		}

		// Token: 0x060002DE RID: 734 RVA: 0x0000A03C File Offset: 0x0000823C
		public override void Repaint(Event e)
		{
			Debug.Assert(GUIClip.Internal_GetCount() == 0, "UIElement is not compatible with IMGUI GUIClips, only GUIClip.ParentClipScope");
			this.m_RepaintVersion = this.version;
			if (!Mathf.Approximately(base.currentPixelsPerPoint, GUIUtility.pixelsPerPoint))
			{
				base.currentPixelsPerPoint = GUIUtility.pixelsPerPoint;
				this.visualTree.IncrementVersion(VersionChangeType.StyleSheet);
			}
			this.repaintData.repaintEvent = e;
			this.m_VisualTreeUpdater.UpdateVisualTree();
		}

		// Token: 0x060002DF RID: 735 RVA: 0x0000A0AE File Offset: 0x000082AE
		internal override void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType)
		{
			this.m_Version += 1U;
			this.m_VisualTreeUpdater.OnVersionChanged(ve, versionChangeType);
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x0000A0CC File Offset: 0x000082CC
		internal override void SetUpdater(IVisualTreeUpdater updater, VisualTreeUpdatePhase phase)
		{
			this.m_VisualTreeUpdater.SetUpdater(updater, phase);
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x0000A0DC File Offset: 0x000082DC
		internal override IVisualTreeUpdater GetUpdater(VisualTreeUpdatePhase phase)
		{
			return this.m_VisualTreeUpdater.GetUpdater(phase);
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x0000A0FD File Offset: 0x000082FD
		// Note: this type is marked as 'beforefieldinit'.
		static Panel()
		{
		}

		// Token: 0x040000F9 RID: 249
		private VisualElement m_RootContainer;

		// Token: 0x040000FA RID: 250
		private VisualTreeUpdater m_VisualTreeUpdater;

		// Token: 0x040000FB RID: 251
		private string m_PanelName;

		// Token: 0x040000FC RID: 252
		private string m_ProfileUpdateName;

		// Token: 0x040000FD RID: 253
		private string m_ProfileLayoutName;

		// Token: 0x040000FE RID: 254
		private string m_ProfileBindingsName;

		// Token: 0x040000FF RID: 255
		private uint m_Version = 0U;

		// Token: 0x04000100 RID: 256
		private uint m_RepaintVersion = 0U;

		// Token: 0x04000101 RID: 257
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private EventDispatcher <dispatcher>k__BackingField;

		// Token: 0x04000102 RID: 258
		private IDataWatchService m_DataWatch;

		// Token: 0x04000103 RID: 259
		private TimerEventScheduler m_Scheduler;

		// Token: 0x04000104 RID: 260
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private ScriptableObject <ownerObject>k__BackingField;

		// Token: 0x04000105 RID: 261
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ContextType <contextType>k__BackingField;

		// Token: 0x04000106 RID: 262
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private SavePersistentViewData <savePersistentViewData>k__BackingField;

		// Token: 0x04000107 RID: 263
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private GetViewDataDictionary <getViewDataDictionary>k__BackingField;

		// Token: 0x04000108 RID: 264
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private FocusController <focusController>k__BackingField;

		// Token: 0x04000109 RID: 265
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private EventInterests <IMGUIEventInterests>k__BackingField;

		// Token: 0x0400010A RID: 266
		internal static LoadResourceFunction loadResourceFunc = null;

		// Token: 0x0400010B RID: 267
		private static TimeMsFunction s_TimeSinceStartup;

		// Token: 0x0400010C RID: 268
		private bool m_KeepPixelCacheOnWorldBoundChange;

		// Token: 0x0400010D RID: 269
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <IMGUIContainersCount>k__BackingField;

		// Token: 0x0400010E RID: 270
		[CompilerGenerated]
		private static TimeMsFunction <>f__mg$cache0;
	}
}
