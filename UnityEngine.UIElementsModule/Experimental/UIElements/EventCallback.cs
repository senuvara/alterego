﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000117 RID: 279
	// (Invoke) Token: 0x060007A1 RID: 1953
	public delegate void EventCallback<in TEventType>(TEventType evt);
}
