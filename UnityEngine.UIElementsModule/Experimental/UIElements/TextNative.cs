﻿using System;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000A7 RID: 167
	[NativeHeader("Modules/UIElements/TextNative.bindings.h")]
	internal static class TextNative
	{
		// Token: 0x060003F8 RID: 1016 RVA: 0x0000D9BC File Offset: 0x0000BBBC
		public static Vector2 GetCursorPosition(TextNativeSettings settings, Rect rect, int cursorIndex)
		{
			Vector2 result;
			if (settings.font == null)
			{
				Debug.LogError("Cannot process a null font.");
				result = Vector2.zero;
			}
			else
			{
				result = TextNative.DoGetCursorPosition(settings, rect, cursorIndex);
			}
			return result;
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x0000DA04 File Offset: 0x0000BC04
		public static float ComputeTextWidth(TextNativeSettings settings)
		{
			float result;
			if (settings.font == null)
			{
				Debug.LogError("Cannot process a null font.");
				result = 0f;
			}
			else if (string.IsNullOrEmpty(settings.text))
			{
				result = 0f;
			}
			else
			{
				result = TextNative.DoComputeTextWidth(settings);
			}
			return result;
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x0000DA64 File Offset: 0x0000BC64
		public static float ComputeTextHeight(TextNativeSettings settings)
		{
			float result;
			if (settings.font == null)
			{
				Debug.LogError("Cannot process a null font.");
				result = 0f;
			}
			else if (string.IsNullOrEmpty(settings.text))
			{
				result = 0f;
			}
			else
			{
				result = TextNative.DoComputeTextHeight(settings);
			}
			return result;
		}

		// Token: 0x060003FB RID: 1019 RVA: 0x0000DAC4 File Offset: 0x0000BCC4
		public static NativeArray<TextVertex> GetVertices(TextNativeSettings settings)
		{
			int num = 0;
			TextNative.GetVertices(settings, IntPtr.Zero, UnsafeUtility.SizeOf<TextVertex>(), ref num);
			NativeArray<TextVertex> nativeArray = new NativeArray<TextVertex>(num, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
			if (num > 0)
			{
				TextNative.GetVertices(settings, (IntPtr)nativeArray.GetUnsafePtr<TextVertex>(), UnsafeUtility.SizeOf<TextVertex>(), ref num);
				Debug.Assert(num == nativeArray.Length);
			}
			return nativeArray;
		}

		// Token: 0x060003FC RID: 1020 RVA: 0x0000DB28 File Offset: 0x0000BD28
		public static Vector2 GetOffset(TextNativeSettings settings, Rect screenRect)
		{
			Vector2 result;
			if (settings.font == null)
			{
				Debug.LogError("Cannot process a null font.");
				result = new Vector2(0f, 0f);
			}
			else
			{
				settings.text = (settings.text ?? "");
				result = TextNative.DoGetOffset(settings, screenRect);
			}
			return result;
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x0000DB90 File Offset: 0x0000BD90
		public static float ComputeTextScaling(Matrix4x4 worldMatrix, float pixelsPerPoint)
		{
			Vector3 vector = new Vector3(worldMatrix.m00, worldMatrix.m10, worldMatrix.m20);
			Vector3 vector2 = new Vector3(worldMatrix.m01, worldMatrix.m11, worldMatrix.m21);
			float num = (vector.magnitude + vector2.magnitude) / 2f;
			return num * pixelsPerPoint;
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x0000DBF6 File Offset: 0x0000BDF6
		[FreeFunction(Name = "TextNative::ComputeTextWidth")]
		private static float DoComputeTextWidth(TextNativeSettings settings)
		{
			return TextNative.DoComputeTextWidth_Injected(ref settings);
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x0000DBFF File Offset: 0x0000BDFF
		[FreeFunction(Name = "TextNative::ComputeTextHeight")]
		private static float DoComputeTextHeight(TextNativeSettings settings)
		{
			return TextNative.DoComputeTextHeight_Injected(ref settings);
		}

		// Token: 0x06000400 RID: 1024 RVA: 0x0000DC08 File Offset: 0x0000BE08
		[FreeFunction(Name = "TextNative::GetCursorPosition")]
		private static Vector2 DoGetCursorPosition(TextNativeSettings settings, Rect rect, int cursorPosition)
		{
			Vector2 result;
			TextNative.DoGetCursorPosition_Injected(ref settings, ref rect, cursorPosition, out result);
			return result;
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x0000DC22 File Offset: 0x0000BE22
		[FreeFunction(Name = "TextNative::GetVertices")]
		private static void GetVertices(TextNativeSettings settings, IntPtr buffer, int vertexSize, ref int vertexCount)
		{
			TextNative.GetVertices_Injected(ref settings, buffer, vertexSize, ref vertexCount);
		}

		// Token: 0x06000402 RID: 1026 RVA: 0x0000DC30 File Offset: 0x0000BE30
		[FreeFunction(Name = "TextNative::GetOffset")]
		private static Vector2 DoGetOffset(TextNativeSettings settings, Rect rect)
		{
			Vector2 result;
			TextNative.DoGetOffset_Injected(ref settings, ref rect, out result);
			return result;
		}

		// Token: 0x06000403 RID: 1027
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float DoComputeTextWidth_Injected(ref TextNativeSettings settings);

		// Token: 0x06000404 RID: 1028
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float DoComputeTextHeight_Injected(ref TextNativeSettings settings);

		// Token: 0x06000405 RID: 1029
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DoGetCursorPosition_Injected(ref TextNativeSettings settings, ref Rect rect, int cursorPosition, out Vector2 ret);

		// Token: 0x06000406 RID: 1030
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetVertices_Injected(ref TextNativeSettings settings, IntPtr buffer, int vertexSize, ref int vertexCount);

		// Token: 0x06000407 RID: 1031
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DoGetOffset_Injected(ref TextNativeSettings settings, ref Rect rect, out Vector2 ret);
	}
}
