﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000112 RID: 274
	public abstract class EventBase : IDisposable
	{
		// Token: 0x06000767 RID: 1895 RVA: 0x00019D37 File Offset: 0x00017F37
		protected EventBase()
		{
			this.m_ImguiEvent = null;
			this.Init();
		}

		// Token: 0x06000768 RID: 1896 RVA: 0x00019D50 File Offset: 0x00017F50
		protected static long RegisterEventType()
		{
			return EventBase.s_LastTypeId += 1L;
		}

		// Token: 0x06000769 RID: 1897
		public abstract long GetEventTypeId();

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x0600076A RID: 1898 RVA: 0x00019D74 File Offset: 0x00017F74
		// (set) Token: 0x0600076B RID: 1899 RVA: 0x00019D8E File Offset: 0x00017F8E
		public long timestamp
		{
			[CompilerGenerated]
			get
			{
				return this.<timestamp>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<timestamp>k__BackingField = value;
			}
		}

		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x0600076C RID: 1900 RVA: 0x00019D98 File Offset: 0x00017F98
		// (set) Token: 0x0600076D RID: 1901 RVA: 0x00019DB2 File Offset: 0x00017FB2
		protected EventBase.EventFlags flags
		{
			[CompilerGenerated]
			get
			{
				return this.<flags>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<flags>k__BackingField = value;
			}
		}

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x0600076E RID: 1902 RVA: 0x00019DBC File Offset: 0x00017FBC
		// (set) Token: 0x0600076F RID: 1903 RVA: 0x00019DD6 File Offset: 0x00017FD6
		private EventBase.LifeCycleFlags lifeCycleFlags
		{
			[CompilerGenerated]
			get
			{
				return this.<lifeCycleFlags>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<lifeCycleFlags>k__BackingField = value;
			}
		}

		// Token: 0x06000770 RID: 1904 RVA: 0x000035A6 File Offset: 0x000017A6
		protected internal virtual void PreDispatch()
		{
		}

		// Token: 0x06000771 RID: 1905 RVA: 0x000035A6 File Offset: 0x000017A6
		protected internal virtual void PostDispatch()
		{
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x06000772 RID: 1906 RVA: 0x00019DE0 File Offset: 0x00017FE0
		public bool bubbles
		{
			get
			{
				return (this.flags & EventBase.EventFlags.Bubbles) != EventBase.EventFlags.None;
			}
		}

		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x06000773 RID: 1907 RVA: 0x00019E04 File Offset: 0x00018004
		[Obsolete("Use tricklesDown instead of capturable.")]
		public bool capturable
		{
			get
			{
				return this.tricklesDown;
			}
		}

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06000774 RID: 1908 RVA: 0x00019E20 File Offset: 0x00018020
		public bool tricklesDown
		{
			get
			{
				return (this.flags & EventBase.EventFlags.TricklesDown) != EventBase.EventFlags.None;
			}
		}

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x06000775 RID: 1909 RVA: 0x00019E44 File Offset: 0x00018044
		// (set) Token: 0x06000776 RID: 1910 RVA: 0x00019E5E File Offset: 0x0001805E
		public IEventHandler target
		{
			[CompilerGenerated]
			get
			{
				return this.<target>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<target>k__BackingField = value;
			}
		}

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x06000777 RID: 1911 RVA: 0x00019E68 File Offset: 0x00018068
		// (set) Token: 0x06000778 RID: 1912 RVA: 0x00019E82 File Offset: 0x00018082
		internal IEventHandler skipElement
		{
			[CompilerGenerated]
			get
			{
				return this.<skipElement>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<skipElement>k__BackingField = value;
			}
		}

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x06000779 RID: 1913 RVA: 0x00019E8C File Offset: 0x0001808C
		// (set) Token: 0x0600077A RID: 1914 RVA: 0x00019EAF File Offset: 0x000180AF
		public bool isPropagationStopped
		{
			get
			{
				return (this.lifeCycleFlags & EventBase.LifeCycleFlags.PropagationStopped) != EventBase.LifeCycleFlags.None;
			}
			private set
			{
				if (value)
				{
					this.lifeCycleFlags |= EventBase.LifeCycleFlags.PropagationStopped;
				}
				else
				{
					this.lifeCycleFlags &= ~EventBase.LifeCycleFlags.PropagationStopped;
				}
			}
		}

		// Token: 0x0600077B RID: 1915 RVA: 0x00019EDE File Offset: 0x000180DE
		public void StopPropagation()
		{
			this.isPropagationStopped = true;
		}

		// Token: 0x170001EB RID: 491
		// (get) Token: 0x0600077C RID: 1916 RVA: 0x00019EE8 File Offset: 0x000180E8
		// (set) Token: 0x0600077D RID: 1917 RVA: 0x00019F0B File Offset: 0x0001810B
		public bool isImmediatePropagationStopped
		{
			get
			{
				return (this.lifeCycleFlags & EventBase.LifeCycleFlags.ImmediatePropagationStopped) != EventBase.LifeCycleFlags.None;
			}
			private set
			{
				if (value)
				{
					this.lifeCycleFlags |= EventBase.LifeCycleFlags.ImmediatePropagationStopped;
				}
				else
				{
					this.lifeCycleFlags &= ~EventBase.LifeCycleFlags.ImmediatePropagationStopped;
				}
			}
		}

		// Token: 0x0600077E RID: 1918 RVA: 0x00019F3A File Offset: 0x0001813A
		public void StopImmediatePropagation()
		{
			this.isPropagationStopped = true;
			this.isImmediatePropagationStopped = true;
		}

		// Token: 0x170001EC RID: 492
		// (get) Token: 0x0600077F RID: 1919 RVA: 0x00019F4C File Offset: 0x0001814C
		// (set) Token: 0x06000780 RID: 1920 RVA: 0x00019F6F File Offset: 0x0001816F
		public bool isDefaultPrevented
		{
			get
			{
				return (this.lifeCycleFlags & EventBase.LifeCycleFlags.DefaultPrevented) != EventBase.LifeCycleFlags.None;
			}
			private set
			{
				if (value)
				{
					this.lifeCycleFlags |= EventBase.LifeCycleFlags.DefaultPrevented;
				}
				else
				{
					this.lifeCycleFlags &= ~EventBase.LifeCycleFlags.DefaultPrevented;
				}
			}
		}

		// Token: 0x06000781 RID: 1921 RVA: 0x00019F9E File Offset: 0x0001819E
		public void PreventDefault()
		{
			if ((this.flags & EventBase.EventFlags.Cancellable) == EventBase.EventFlags.Cancellable)
			{
				this.isDefaultPrevented = true;
			}
		}

		// Token: 0x170001ED RID: 493
		// (get) Token: 0x06000782 RID: 1922 RVA: 0x00019FB8 File Offset: 0x000181B8
		// (set) Token: 0x06000783 RID: 1923 RVA: 0x00019FD2 File Offset: 0x000181D2
		public PropagationPhase propagationPhase
		{
			[CompilerGenerated]
			get
			{
				return this.<propagationPhase>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<propagationPhase>k__BackingField = value;
			}
		}

		// Token: 0x170001EE RID: 494
		// (get) Token: 0x06000784 RID: 1924 RVA: 0x00019FDC File Offset: 0x000181DC
		// (set) Token: 0x06000785 RID: 1925 RVA: 0x00019FF8 File Offset: 0x000181F8
		public virtual IEventHandler currentTarget
		{
			get
			{
				return this.m_CurrentTarget;
			}
			internal set
			{
				this.m_CurrentTarget = value;
				if (this.imguiEvent != null)
				{
					VisualElement visualElement = this.currentTarget as VisualElement;
					if (visualElement != null)
					{
						this.imguiEvent.mousePosition = visualElement.WorldToLocal(this.originalMousePosition);
					}
				}
			}
		}

		// Token: 0x170001EF RID: 495
		// (get) Token: 0x06000786 RID: 1926 RVA: 0x0001A048 File Offset: 0x00018248
		// (set) Token: 0x06000787 RID: 1927 RVA: 0x0001A06B File Offset: 0x0001826B
		public bool dispatch
		{
			get
			{
				return (this.lifeCycleFlags & EventBase.LifeCycleFlags.Dispatching) != EventBase.LifeCycleFlags.None;
			}
			internal set
			{
				if (value)
				{
					this.lifeCycleFlags |= EventBase.LifeCycleFlags.Dispatching;
					this.dispatched = true;
				}
				else
				{
					this.lifeCycleFlags &= ~EventBase.LifeCycleFlags.Dispatching;
				}
			}
		}

		// Token: 0x06000788 RID: 1928 RVA: 0x0001A0A1 File Offset: 0x000182A1
		internal void MarkReceivedByDispatcher()
		{
			Debug.Assert(!this.dispatched, "Events cannot be dispatched more than once.");
			this.dispatched = true;
		}

		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x06000789 RID: 1929 RVA: 0x0001A0C0 File Offset: 0x000182C0
		// (set) Token: 0x0600078A RID: 1930 RVA: 0x0001A0E7 File Offset: 0x000182E7
		private bool dispatched
		{
			get
			{
				return (this.lifeCycleFlags & EventBase.LifeCycleFlags.Dispatched) != EventBase.LifeCycleFlags.None;
			}
			set
			{
				if (value)
				{
					this.lifeCycleFlags |= EventBase.LifeCycleFlags.Dispatched;
				}
				else
				{
					this.lifeCycleFlags &= ~EventBase.LifeCycleFlags.Dispatched;
				}
			}
		}

		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x0600078B RID: 1931 RVA: 0x0001A120 File Offset: 0x00018320
		// (set) Token: 0x0600078C RID: 1932 RVA: 0x0001A144 File Offset: 0x00018344
		private bool imguiEventIsValid
		{
			get
			{
				return (this.lifeCycleFlags & EventBase.LifeCycleFlags.IMGUIEventIsValid) != EventBase.LifeCycleFlags.None;
			}
			set
			{
				if (value)
				{
					this.lifeCycleFlags |= EventBase.LifeCycleFlags.IMGUIEventIsValid;
				}
				else
				{
					this.lifeCycleFlags &= ~EventBase.LifeCycleFlags.IMGUIEventIsValid;
				}
			}
		}

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x0600078D RID: 1933 RVA: 0x0001A174 File Offset: 0x00018374
		// (set) Token: 0x0600078E RID: 1934 RVA: 0x0001A1A0 File Offset: 0x000183A0
		public Event imguiEvent
		{
			get
			{
				return (!this.imguiEventIsValid) ? null : this.m_ImguiEvent;
			}
			protected set
			{
				if (this.m_ImguiEvent == null)
				{
					this.m_ImguiEvent = new Event();
				}
				if (value != null)
				{
					this.m_ImguiEvent.CopyFrom(value);
					this.imguiEventIsValid = true;
					this.originalMousePosition = value.mousePosition;
				}
				else
				{
					this.imguiEventIsValid = false;
				}
			}
		}

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x0600078F RID: 1935 RVA: 0x0001A1FC File Offset: 0x000183FC
		// (set) Token: 0x06000790 RID: 1936 RVA: 0x0001A216 File Offset: 0x00018416
		public Vector2 originalMousePosition
		{
			[CompilerGenerated]
			get
			{
				return this.<originalMousePosition>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<originalMousePosition>k__BackingField = value;
			}
		}

		// Token: 0x06000791 RID: 1937 RVA: 0x0001A220 File Offset: 0x00018420
		protected virtual void Init()
		{
			this.timestamp = (long)(Time.realtimeSinceStartup * 1000f);
			this.flags = EventBase.EventFlags.None;
			this.target = null;
			this.skipElement = null;
			this.isPropagationStopped = false;
			this.isImmediatePropagationStopped = false;
			this.isDefaultPrevented = false;
			this.propagationPhase = PropagationPhase.None;
			this.originalMousePosition = Vector2.zero;
			this.m_CurrentTarget = null;
			this.dispatch = false;
			this.dispatched = false;
			this.imguiEventIsValid = false;
			this.pooled = false;
		}

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x06000792 RID: 1938 RVA: 0x0001A2A0 File Offset: 0x000184A0
		// (set) Token: 0x06000793 RID: 1939 RVA: 0x0001A2C4 File Offset: 0x000184C4
		protected bool pooled
		{
			get
			{
				return (this.lifeCycleFlags & EventBase.LifeCycleFlags.Pooled) != EventBase.LifeCycleFlags.None;
			}
			set
			{
				if (value)
				{
					this.lifeCycleFlags |= EventBase.LifeCycleFlags.Pooled;
				}
				else
				{
					this.lifeCycleFlags &= ~EventBase.LifeCycleFlags.Pooled;
				}
			}
		}

		// Token: 0x06000794 RID: 1940
		internal abstract void Acquire();

		// Token: 0x06000795 RID: 1941
		public abstract void Dispose();

		// Token: 0x06000796 RID: 1942 RVA: 0x0001A2F4 File Offset: 0x000184F4
		// Note: this type is marked as 'beforefieldinit'.
		static EventBase()
		{
		}

		// Token: 0x040002DB RID: 731
		private static long s_LastTypeId = 0L;

		// Token: 0x040002DC RID: 732
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private long <timestamp>k__BackingField;

		// Token: 0x040002DD RID: 733
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private EventBase.EventFlags <flags>k__BackingField;

		// Token: 0x040002DE RID: 734
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private EventBase.LifeCycleFlags <lifeCycleFlags>k__BackingField;

		// Token: 0x040002DF RID: 735
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private IEventHandler <target>k__BackingField;

		// Token: 0x040002E0 RID: 736
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private IEventHandler <skipElement>k__BackingField;

		// Token: 0x040002E1 RID: 737
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private PropagationPhase <propagationPhase>k__BackingField;

		// Token: 0x040002E2 RID: 738
		protected IEventHandler m_CurrentTarget;

		// Token: 0x040002E3 RID: 739
		private Event m_ImguiEvent;

		// Token: 0x040002E4 RID: 740
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Vector2 <originalMousePosition>k__BackingField;

		// Token: 0x02000113 RID: 275
		[Flags]
		protected internal enum EventFlags
		{
			// Token: 0x040002E6 RID: 742
			None = 0,
			// Token: 0x040002E7 RID: 743
			Bubbles = 1,
			// Token: 0x040002E8 RID: 744
			TricklesDown = 2,
			// Token: 0x040002E9 RID: 745
			[Obsolete("Use TrickesDown instead of Capturable")]
			Capturable = 2,
			// Token: 0x040002EA RID: 746
			Cancellable = 4
		}

		// Token: 0x02000114 RID: 276
		[Flags]
		private enum LifeCycleFlags
		{
			// Token: 0x040002EC RID: 748
			None = 0,
			// Token: 0x040002ED RID: 749
			PropagationStopped = 1,
			// Token: 0x040002EE RID: 750
			ImmediatePropagationStopped = 2,
			// Token: 0x040002EF RID: 751
			DefaultPrevented = 4,
			// Token: 0x040002F0 RID: 752
			Dispatching = 8,
			// Token: 0x040002F1 RID: 753
			Pooled = 16,
			// Token: 0x040002F2 RID: 754
			IMGUIEventIsValid = 32,
			// Token: 0x040002F3 RID: 755
			Dispatched = 512
		}
	}
}
