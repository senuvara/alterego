﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200001B RID: 27
	internal interface ICursorManager
	{
		// Token: 0x06000089 RID: 137
		void SetCursor(CursorStyle cursor);

		// Token: 0x0600008A RID: 138
		void ResetCursor();
	}
}
