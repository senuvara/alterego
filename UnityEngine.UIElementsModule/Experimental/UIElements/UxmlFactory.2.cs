﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000171 RID: 369
	public class UxmlFactory<TCreatedType> : UxmlFactory<TCreatedType, VisualElement.UxmlTraits> where TCreatedType : VisualElement
	{
		// Token: 0x0600098E RID: 2446 RVA: 0x0002110C File Offset: 0x0001F30C
		public UxmlFactory()
		{
		}
	}
}
