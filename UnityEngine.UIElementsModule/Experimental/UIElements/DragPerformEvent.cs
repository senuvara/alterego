﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000111 RID: 273
	public class DragPerformEvent : DragAndDropEventBase<DragPerformEvent>
	{
		// Token: 0x06000766 RID: 1894 RVA: 0x00019D2F File Offset: 0x00017F2F
		public DragPerformEvent()
		{
		}
	}
}
