﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200005A RID: 90
	public enum ContextType
	{
		// Token: 0x040000D9 RID: 217
		Player,
		// Token: 0x040000DA RID: 218
		Editor
	}
}
