﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Experimental.UIElements.StyleSheets;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000E1 RID: 225
	internal class VisualTreeStyleUpdaterTraversal : HierarchyTraversal
	{
		// Token: 0x0600066F RID: 1647 RVA: 0x000168CC File Offset: 0x00014ACC
		public VisualTreeStyleUpdaterTraversal()
		{
			if (VisualTreeStyleUpdaterTraversal.<>f__mg$cache1 == null)
			{
				VisualTreeStyleUpdaterTraversal.<>f__mg$cache1 = new Action<VisualElement, MatchResultInfo>(VisualTreeStyleUpdaterTraversal.OnProcessMatchResult);
			}
			this.m_StyleMatchingContext = new StyleMatchingContext(VisualTreeStyleUpdaterTraversal.<>f__mg$cache1);
			base..ctor();
		}

		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x06000670 RID: 1648 RVA: 0x00016934 File Offset: 0x00014B34
		// (set) Token: 0x06000671 RID: 1649 RVA: 0x0001694E File Offset: 0x00014B4E
		public float currentPixelsPerPoint
		{
			[CompilerGenerated]
			get
			{
				return this.<currentPixelsPerPoint>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<currentPixelsPerPoint>k__BackingField = value;
			}
		} = 1f;

		// Token: 0x06000672 RID: 1650 RVA: 0x00016957 File Offset: 0x00014B57
		public void AddChangedElement(VisualElement ve)
		{
			this.m_UpdateList.Add(ve);
			this.PropagateToChildren(ve);
			this.PropagateToParents(ve);
		}

		// Token: 0x06000673 RID: 1651 RVA: 0x00016975 File Offset: 0x00014B75
		public void Clear()
		{
			this.m_UpdateList.Clear();
			this.m_ParentList.Clear();
			this.m_TempMatchResults.Clear();
		}

		// Token: 0x06000674 RID: 1652 RVA: 0x0001699C File Offset: 0x00014B9C
		private void PropagateToChildren(VisualElement ve)
		{
			int childCount = ve.shadow.childCount;
			for (int i = 0; i < childCount; i++)
			{
				VisualElement visualElement = ve.shadow[i];
				bool flag = this.m_UpdateList.Add(visualElement);
				if (flag)
				{
					this.PropagateToChildren(visualElement);
				}
			}
		}

		// Token: 0x06000675 RID: 1653 RVA: 0x000169FC File Offset: 0x00014BFC
		private void PropagateToParents(VisualElement ve)
		{
			for (VisualElement parent = ve.shadow.parent; parent != null; parent = parent.shadow.parent)
			{
				if (!this.m_ParentList.Add(parent))
				{
					break;
				}
			}
		}

		// Token: 0x06000676 RID: 1654 RVA: 0x00016A4C File Offset: 0x00014C4C
		private static void OnProcessMatchResult(VisualElement current, MatchResultInfo info)
		{
			current.triggerPseudoMask |= info.triggerPseudoMask;
			current.dependencyPseudoMask |= info.dependencyPseudoMask;
		}

		// Token: 0x06000677 RID: 1655 RVA: 0x00016A78 File Offset: 0x00014C78
		public override void TraverseRecursive(VisualElement element, int depth)
		{
			if (!this.ShouldSkipElement(element))
			{
				if (this.m_UpdateList.Contains(element))
				{
					element.triggerPseudoMask = (PseudoStates)0;
					element.dependencyPseudoMask = (PseudoStates)0;
				}
				int count = this.m_StyleMatchingContext.styleSheetStack.Count;
				if (element.styleSheets != null)
				{
					for (int i = 0; i < element.styleSheets.Count; i++)
					{
						StyleSheet item = element.styleSheets[i];
						this.m_StyleMatchingContext.styleSheetStack.Add(item);
					}
				}
				if (this.m_UpdateList.Contains(element))
				{
					this.m_StyleMatchingContext.currentElement = element;
					StyleSelectorHelper.FindMatches(this.m_StyleMatchingContext, this.m_TempMatchResults);
					this.ProcessMatchedRules(element, this.m_TempMatchResults);
					this.m_StyleMatchingContext.currentElement = null;
					this.m_TempMatchResults.Clear();
				}
				base.Recurse(element, depth);
				if (this.m_StyleMatchingContext.styleSheetStack.Count > count)
				{
					this.m_StyleMatchingContext.styleSheetStack.RemoveRange(count, this.m_StyleMatchingContext.styleSheetStack.Count - count);
				}
			}
		}

		// Token: 0x06000678 RID: 1656 RVA: 0x00016BAC File Offset: 0x00014DAC
		private bool ShouldSkipElement(VisualElement element)
		{
			return !this.m_ParentList.Contains(element) && !this.m_UpdateList.Contains(element);
		}

		// Token: 0x06000679 RID: 1657 RVA: 0x00016BE4 File Offset: 0x00014DE4
		private void ProcessMatchedRules(VisualElement element, List<SelectorMatchRecord> matchingSelectors)
		{
			if (VisualTreeStyleUpdaterTraversal.<>f__mg$cache0 == null)
			{
				VisualTreeStyleUpdaterTraversal.<>f__mg$cache0 = new Comparison<SelectorMatchRecord>(SelectorMatchRecord.Compare);
			}
			matchingSelectors.Sort(VisualTreeStyleUpdaterTraversal.<>f__mg$cache0);
			long num = (long)element.fullTypeName.GetHashCode();
			num = (num * 397L ^ (long)this.currentPixelsPerPoint.GetHashCode());
			foreach (SelectorMatchRecord selectorMatchRecord in matchingSelectors)
			{
				StyleRule rule = selectorMatchRecord.complexSelector.rule;
				int specificity = selectorMatchRecord.complexSelector.specificity;
				num = (num * 397L ^ (long)rule.GetHashCode());
				num = (num * 397L ^ (long)specificity);
			}
			VisualElementStylesData visualElementStylesData;
			if (StyleCache.TryGetValue(num, out visualElementStylesData))
			{
				element.SetSharedStyles(visualElementStylesData);
			}
			else
			{
				visualElementStylesData = new VisualElementStylesData(true);
				foreach (SelectorMatchRecord selectorMatchRecord2 in matchingSelectors)
				{
					StylePropertyID[] propertyIDs = StyleSheetCache.GetPropertyIDs(selectorMatchRecord2.sheet, selectorMatchRecord2.complexSelector.ruleIndex);
					visualElementStylesData.ApplyRule(selectorMatchRecord2.sheet, selectorMatchRecord2.complexSelector.specificity, selectorMatchRecord2.complexSelector.rule, propertyIDs);
				}
				visualElementStylesData.ApplyLayoutValues();
				StyleCache.SetValue(num, visualElementStylesData);
				element.SetSharedStyles(visualElementStylesData);
			}
		}

		// Token: 0x0400028C RID: 652
		private HashSet<VisualElement> m_UpdateList = new HashSet<VisualElement>();

		// Token: 0x0400028D RID: 653
		private HashSet<VisualElement> m_ParentList = new HashSet<VisualElement>();

		// Token: 0x0400028E RID: 654
		private List<SelectorMatchRecord> m_TempMatchResults = new List<SelectorMatchRecord>();

		// Token: 0x0400028F RID: 655
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private float <currentPixelsPerPoint>k__BackingField;

		// Token: 0x04000290 RID: 656
		private StyleMatchingContext m_StyleMatchingContext;

		// Token: 0x04000291 RID: 657
		[CompilerGenerated]
		private static Comparison<SelectorMatchRecord> <>f__mg$cache0;

		// Token: 0x04000292 RID: 658
		[CompilerGenerated]
		private static Action<VisualElement, MatchResultInfo> <>f__mg$cache1;
	}
}
