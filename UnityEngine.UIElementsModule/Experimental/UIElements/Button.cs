﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200000C RID: 12
	public class Button : TextElement
	{
		// Token: 0x0600003B RID: 59 RVA: 0x00002DB0 File Offset: 0x00000FB0
		public Button() : this(null)
		{
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00002DBA File Offset: 0x00000FBA
		public Button(Action clickEvent)
		{
			this.clickable = new Clickable(clickEvent);
			this.AddManipulator(this.clickable);
		}

		// Token: 0x04000013 RID: 19
		public Clickable clickable;

		// Token: 0x0200000D RID: 13
		public new class UxmlFactory : UxmlFactory<Button, Button.UxmlTraits>
		{
			// Token: 0x0600003D RID: 61 RVA: 0x00002DDB File Offset: 0x00000FDB
			public UxmlFactory()
			{
			}
		}

		// Token: 0x0200000E RID: 14
		public new class UxmlTraits : TextElement.UxmlTraits
		{
			// Token: 0x0600003E RID: 62 RVA: 0x00002DE3 File Offset: 0x00000FE3
			public UxmlTraits()
			{
			}
		}
	}
}
