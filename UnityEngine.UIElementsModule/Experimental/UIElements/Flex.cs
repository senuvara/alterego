﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000D6 RID: 214
	public struct Flex
	{
		// Token: 0x0600063D RID: 1597 RVA: 0x000153F8 File Offset: 0x000135F8
		public Flex(float g, float s = 1f, float b = 0f)
		{
			this.grow = g;
			this.shrink = s;
			this.basis = b;
		}

		// Token: 0x170001AA RID: 426
		// (get) Token: 0x0600063E RID: 1598 RVA: 0x00015410 File Offset: 0x00013610
		// (set) Token: 0x0600063F RID: 1599 RVA: 0x0001542A File Offset: 0x0001362A
		public float grow
		{
			[CompilerGenerated]
			get
			{
				return this.<grow>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<grow>k__BackingField = value;
			}
		}

		// Token: 0x170001AB RID: 427
		// (get) Token: 0x06000640 RID: 1600 RVA: 0x00015434 File Offset: 0x00013634
		// (set) Token: 0x06000641 RID: 1601 RVA: 0x0001544E File Offset: 0x0001364E
		public float shrink
		{
			[CompilerGenerated]
			get
			{
				return this.<shrink>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<shrink>k__BackingField = value;
			}
		}

		// Token: 0x170001AC RID: 428
		// (get) Token: 0x06000642 RID: 1602 RVA: 0x00015458 File Offset: 0x00013658
		// (set) Token: 0x06000643 RID: 1603 RVA: 0x00015472 File Offset: 0x00013672
		public float basis
		{
			[CompilerGenerated]
			get
			{
				return this.<basis>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<basis>k__BackingField = value;
			}
		}

		// Token: 0x0400026A RID: 618
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private float <grow>k__BackingField;

		// Token: 0x0400026B RID: 619
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private float <shrink>k__BackingField;

		// Token: 0x0400026C RID: 620
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private float <basis>k__BackingField;
	}
}
