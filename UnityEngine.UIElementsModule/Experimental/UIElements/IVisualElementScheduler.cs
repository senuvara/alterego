﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000D3 RID: 211
	public interface IVisualElementScheduler
	{
		// Token: 0x0600062D RID: 1581
		IVisualElementScheduledItem Execute(Action<TimerState> timerUpdateEvent);

		// Token: 0x0600062E RID: 1582
		IVisualElementScheduledItem Execute(Action updateEvent);
	}
}
