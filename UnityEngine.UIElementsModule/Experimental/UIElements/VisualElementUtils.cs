﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000D7 RID: 215
	internal sealed class VisualElementUtils
	{
		// Token: 0x06000644 RID: 1604 RVA: 0x00002223 File Offset: 0x00000423
		public VisualElementUtils()
		{
		}

		// Token: 0x06000645 RID: 1605 RVA: 0x0001547C File Offset: 0x0001367C
		public static string GetUniqueName(string nameBase)
		{
			string text = nameBase;
			int num = 2;
			while (VisualElementUtils.s_usedNames.Contains(text))
			{
				text = nameBase + num;
				num++;
			}
			VisualElementUtils.s_usedNames.Add(text);
			return text;
		}

		// Token: 0x06000646 RID: 1606 RVA: 0x000154CA File Offset: 0x000136CA
		// Note: this type is marked as 'beforefieldinit'.
		static VisualElementUtils()
		{
		}

		// Token: 0x0400026D RID: 621
		private static readonly HashSet<string> s_usedNames = new HashSet<string>();
	}
}
