﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200011D RID: 285
	public enum TrickleDown
	{
		// Token: 0x04000301 RID: 769
		NoTrickleDown,
		// Token: 0x04000302 RID: 770
		TrickleDown
	}
}
