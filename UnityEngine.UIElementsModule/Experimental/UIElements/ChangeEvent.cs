﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000106 RID: 262
	public class ChangeEvent<T> : EventBase<ChangeEvent<T>>, IChangeEvent
	{
		// Token: 0x0600074E RID: 1870 RVA: 0x00019B44 File Offset: 0x00017D44
		public ChangeEvent()
		{
			this.Init();
		}

		// Token: 0x170001DE RID: 478
		// (get) Token: 0x0600074F RID: 1871 RVA: 0x00019B54 File Offset: 0x00017D54
		// (set) Token: 0x06000750 RID: 1872 RVA: 0x00019B6E File Offset: 0x00017D6E
		public T previousValue
		{
			[CompilerGenerated]
			get
			{
				return this.<previousValue>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<previousValue>k__BackingField = value;
			}
		}

		// Token: 0x170001DF RID: 479
		// (get) Token: 0x06000751 RID: 1873 RVA: 0x00019B78 File Offset: 0x00017D78
		// (set) Token: 0x06000752 RID: 1874 RVA: 0x00019B92 File Offset: 0x00017D92
		public T newValue
		{
			[CompilerGenerated]
			get
			{
				return this.<newValue>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<newValue>k__BackingField = value;
			}
		}

		// Token: 0x06000753 RID: 1875 RVA: 0x00019B9C File Offset: 0x00017D9C
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown);
			this.previousValue = default(T);
			this.newValue = default(T);
		}

		// Token: 0x06000754 RID: 1876 RVA: 0x00019BD8 File Offset: 0x00017DD8
		public static ChangeEvent<T> GetPooled(T previousValue, T newValue)
		{
			ChangeEvent<T> pooled = EventBase<ChangeEvent<T>>.GetPooled();
			pooled.previousValue = previousValue;
			pooled.newValue = newValue;
			return pooled;
		}

		// Token: 0x040002D8 RID: 728
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private T <previousValue>k__BackingField;

		// Token: 0x040002D9 RID: 729
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private T <newValue>k__BackingField;
	}
}
