﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200013F RID: 319
	public class ContextualMenuPopulateEvent : MouseEventBase<ContextualMenuPopulateEvent>
	{
		// Token: 0x0600086C RID: 2156 RVA: 0x0001BC1D File Offset: 0x00019E1D
		public ContextualMenuPopulateEvent()
		{
			this.Init();
		}

		// Token: 0x1700022F RID: 559
		// (get) Token: 0x0600086D RID: 2157 RVA: 0x0001BC2C File Offset: 0x00019E2C
		// (set) Token: 0x0600086E RID: 2158 RVA: 0x0001BC46 File Offset: 0x00019E46
		public DropdownMenu menu
		{
			[CompilerGenerated]
			get
			{
				return this.<menu>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<menu>k__BackingField = value;
			}
		}

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x0600086F RID: 2159 RVA: 0x0001BC50 File Offset: 0x00019E50
		// (set) Token: 0x06000870 RID: 2160 RVA: 0x0001BC6A File Offset: 0x00019E6A
		public EventBase triggerEvent
		{
			[CompilerGenerated]
			get
			{
				return this.<triggerEvent>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<triggerEvent>k__BackingField = value;
			}
		}

		// Token: 0x06000871 RID: 2161 RVA: 0x0001BC74 File Offset: 0x00019E74
		public static ContextualMenuPopulateEvent GetPooled(EventBase triggerEvent, DropdownMenu menu, IEventHandler target, ContextualMenuManager menuManager)
		{
			ContextualMenuPopulateEvent pooled = EventBase<ContextualMenuPopulateEvent>.GetPooled();
			if (triggerEvent != null)
			{
				triggerEvent.Acquire();
				pooled.triggerEvent = triggerEvent;
				IMouseEvent mouseEvent = triggerEvent as IMouseEvent;
				if (mouseEvent != null)
				{
					pooled.modifiers = mouseEvent.modifiers;
					pooled.mousePosition = mouseEvent.mousePosition;
					pooled.localMousePosition = mouseEvent.mousePosition;
					pooled.mouseDelta = mouseEvent.mouseDelta;
					pooled.button = mouseEvent.button;
					pooled.clickCount = mouseEvent.clickCount;
				}
				IMouseEventInternal mouseEventInternal = triggerEvent as IMouseEventInternal;
				if (mouseEventInternal != null)
				{
					((IMouseEventInternal)pooled).hasUnderlyingPhysicalEvent = mouseEventInternal.hasUnderlyingPhysicalEvent;
				}
			}
			pooled.target = target;
			pooled.menu = menu;
			pooled.m_ContextualMenuManager = menuManager;
			return pooled;
		}

		// Token: 0x06000872 RID: 2162 RVA: 0x0001BD2C File Offset: 0x00019F2C
		protected override void Init()
		{
			base.Init();
			this.menu = null;
			this.m_ContextualMenuManager = null;
			if (this.triggerEvent != null)
			{
				this.triggerEvent.Dispose();
				this.triggerEvent = null;
			}
		}

		// Token: 0x06000873 RID: 2163 RVA: 0x0001BD64 File Offset: 0x00019F64
		protected internal override void PostDispatch()
		{
			if (!base.isDefaultPrevented && this.m_ContextualMenuManager != null)
			{
				this.menu.PrepareForDisplay(this.triggerEvent);
				this.m_ContextualMenuManager.DoDisplayMenu(this.menu, this.triggerEvent);
			}
		}

		// Token: 0x04000323 RID: 803
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DropdownMenu <menu>k__BackingField;

		// Token: 0x04000324 RID: 804
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private EventBase <triggerEvent>k__BackingField;

		// Token: 0x04000325 RID: 805
		private ContextualMenuManager m_ContextualMenuManager;
	}
}
