﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000056 RID: 86
	public enum MouseButton
	{
		// Token: 0x040000D0 RID: 208
		LeftMouse,
		// Token: 0x040000D1 RID: 209
		RightMouse,
		// Token: 0x040000D2 RID: 210
		MiddleMouse
	}
}
