﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200011A RID: 282
	internal class EventCallbackFunctor<TEventType> : EventCallbackFunctorBase where TEventType : EventBase<TEventType>, new()
	{
		// Token: 0x060007AE RID: 1966 RVA: 0x0001A500 File Offset: 0x00018700
		public EventCallbackFunctor(EventCallback<TEventType> callback, CallbackPhase phase) : base(phase)
		{
			this.m_Callback = callback;
			this.m_EventTypeId = EventBase<TEventType>.TypeId();
		}

		// Token: 0x060007AF RID: 1967 RVA: 0x0001A51C File Offset: 0x0001871C
		public override void Invoke(EventBase evt)
		{
			if (evt == null)
			{
				throw new ArgumentNullException();
			}
			if (evt.GetEventTypeId() == this.m_EventTypeId)
			{
				if (base.PhaseMatches(evt))
				{
					this.m_Callback(evt as TEventType);
				}
			}
		}

		// Token: 0x060007B0 RID: 1968 RVA: 0x0001A570 File Offset: 0x00018770
		public override bool IsEquivalentTo(long eventTypeId, Delegate callback, CallbackPhase phase)
		{
			return this.m_EventTypeId == eventTypeId && this.m_Callback == callback && base.phase == phase;
		}

		// Token: 0x040002F8 RID: 760
		private EventCallback<TEventType> m_Callback;

		// Token: 0x040002F9 RID: 761
		private long m_EventTypeId;
	}
}
