﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200013C RID: 316
	public class MouseLeaveWindowEvent : MouseEventBase<MouseLeaveWindowEvent>
	{
		// Token: 0x06000868 RID: 2152 RVA: 0x0001BBEE File Offset: 0x00019DEE
		public MouseLeaveWindowEvent()
		{
			this.Init();
		}

		// Token: 0x06000869 RID: 2153 RVA: 0x0001BBFD File Offset: 0x00019DFD
		protected override void Init()
		{
			base.Init();
			base.flags = EventBase.EventFlags.Cancellable;
		}
	}
}
