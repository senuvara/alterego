﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000002 RID: 2
	public abstract class BaseField<T> : BindableElement, INotifyValueChanged<T>
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public BaseField()
		{
			this.focusIndex = 0;
			this.m_Value = default(T);
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2 RVA: 0x0000207C File Offset: 0x0000027C
		// (set) Token: 0x06000003 RID: 3 RVA: 0x00002098 File Offset: 0x00000298
		public virtual T value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				if (!EqualityComparer<T>.Default.Equals(this.m_Value, value))
				{
					if (base.panel != null)
					{
						using (ChangeEvent<T> pooled = ChangeEvent<T>.GetPooled(this.m_Value, value))
						{
							pooled.target = this;
							this.SetValueWithoutNotify(value);
							this.SendEvent(pooled);
						}
					}
					else
					{
						this.SetValueWithoutNotify(value);
					}
				}
			}
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002120 File Offset: 0x00000320
		[Obsolete("This method is replaced by simply using this.value. The default behaviour has been changed to notify when changed. If the behaviour is not to be notified, SetValueWithoutNotify() must be used.", false)]
		public virtual void SetValueAndNotify(T newValue)
		{
			this.value = newValue;
		}

		// Token: 0x06000005 RID: 5 RVA: 0x0000212C File Offset: 0x0000032C
		public override void OnPersistentDataReady()
		{
			base.OnPersistentDataReady();
			string fullHierarchicalPersistenceKey = base.GetFullHierarchicalPersistenceKey();
			T value = this.m_Value;
			base.OverwriteFromPersistedData(this, fullHierarchicalPersistenceKey);
			if (!EqualityComparer<T>.Default.Equals(value, this.m_Value))
			{
				using (ChangeEvent<T> pooled = ChangeEvent<T>.GetPooled(value, this.m_Value))
				{
					pooled.target = this;
					this.SendEvent(pooled);
				}
			}
		}

		// Token: 0x06000006 RID: 6 RVA: 0x000021AC File Offset: 0x000003AC
		public void OnValueChanged(EventCallback<ChangeEvent<T>> callback)
		{
			base.RegisterCallback<ChangeEvent<T>>(callback, TrickleDown.NoTrickleDown);
		}

		// Token: 0x06000007 RID: 7 RVA: 0x000021B7 File Offset: 0x000003B7
		public void RemoveOnValueChanged(EventCallback<ChangeEvent<T>> callback)
		{
			base.UnregisterCallback<ChangeEvent<T>>(callback, TrickleDown.NoTrickleDown);
		}

		// Token: 0x06000008 RID: 8 RVA: 0x000021C2 File Offset: 0x000003C2
		public virtual void SetValueWithoutNotify(T newValue)
		{
			this.m_Value = newValue;
			if (!string.IsNullOrEmpty(base.persistenceKey))
			{
				base.SavePersistentData();
			}
			base.MarkDirtyRepaint();
		}

		// Token: 0x04000001 RID: 1
		[SerializeField]
		protected T m_Value;

		// Token: 0x02000003 RID: 3
		public new class UxmlTraits : BindableElement.UxmlTraits
		{
			// Token: 0x06000009 RID: 9 RVA: 0x000021E8 File Offset: 0x000003E8
			public UxmlTraits()
			{
				this.m_FocusIndex.defaultValue = 0;
			}

			// Token: 0x17000002 RID: 2
			// (get) Token: 0x0600000A RID: 10 RVA: 0x00002200 File Offset: 0x00000400
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield break;
				}
			}

			// Token: 0x02000004 RID: 4
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x0600000B RID: 11 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x0600000C RID: 12 RVA: 0x0000222B File Offset: 0x0000042B
				public bool MoveNext()
				{
					bool flag = this.$PC != 0;
					this.$PC = -1;
					if (!flag)
					{
					}
					return false;
				}

				// Token: 0x17000003 RID: 3
				// (get) Token: 0x0600000D RID: 13 RVA: 0x00002248 File Offset: 0x00000448
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x17000004 RID: 4
				// (get) Token: 0x0600000E RID: 14 RVA: 0x00002264 File Offset: 0x00000464
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x0600000F RID: 15 RVA: 0x0000227E File Offset: 0x0000047E
				[DebuggerHidden]
				public void Dispose()
				{
				}

				// Token: 0x06000010 RID: 16 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x06000011 RID: 17 RVA: 0x00002288 File Offset: 0x00000488
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x06000012 RID: 18 RVA: 0x000022A2 File Offset: 0x000004A2
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new BaseField<T>.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x04000002 RID: 2
				internal UxmlChildElementDescription $current;

				// Token: 0x04000003 RID: 3
				internal bool $disposing;

				// Token: 0x04000004 RID: 4
				internal int $PC;
			}
		}
	}
}
