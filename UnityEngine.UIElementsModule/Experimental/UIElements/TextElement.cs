﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000A1 RID: 161
	public class TextElement : VisualElement, ITextElement
	{
		// Token: 0x060003E5 RID: 997 RVA: 0x0000D655 File Offset: 0x0000B855
		public TextElement()
		{
			base.requireMeasureFunction = true;
			base.AddToClassList("textElement");
		}

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x060003E6 RID: 998 RVA: 0x0000D670 File Offset: 0x0000B870
		// (set) Token: 0x060003E7 RID: 999 RVA: 0x0000D697 File Offset: 0x0000B897
		public virtual string text
		{
			get
			{
				return this.m_Text ?? string.Empty;
			}
			set
			{
				if (!(this.m_Text == value))
				{
					this.m_Text = value;
					base.IncrementVersion(VersionChangeType.Layout);
					if (!string.IsNullOrEmpty(base.persistenceKey))
					{
						base.SavePersistentData();
					}
				}
			}
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x0000D6D8 File Offset: 0x0000B8D8
		protected override void DoRepaint(IStylePainter painter)
		{
			IStylePainterInternal stylePainterInternal = (IStylePainterInternal)painter;
			stylePainterInternal.DrawText(this.text);
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x0000D6FC File Offset: 0x0000B8FC
		public Vector2 MeasureTextSize(string textToMeasure, float width, VisualElement.MeasureMode widthMode, float height, VisualElement.MeasureMode heightMode)
		{
			return TextElement.MeasureVisualElementTextSize(this, textToMeasure, width, widthMode, height, heightMode);
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x0000D720 File Offset: 0x0000B920
		internal static Vector2 MeasureVisualElementTextSize(VisualElement ve, string textToMeasure, float width, VisualElement.MeasureMode widthMode, float height, VisualElement.MeasureMode heightMode)
		{
			float num = float.NaN;
			float num2 = float.NaN;
			Font font = ve.style.font;
			Vector2 result;
			if (textToMeasure == null || font == null)
			{
				result = new Vector2(num, num2);
			}
			else
			{
				Vector3 vector = ve.ComputeGlobalScale();
				float num3 = (ve.elementPanel == null) ? GUIUtility.pixelsPerPoint : ve.elementPanel.currentPixelsPerPoint;
				float scaling = (vector.x + vector.y) * 0.5f * num3;
				if (widthMode == VisualElement.MeasureMode.Exactly)
				{
					num = width;
				}
				else
				{
					TextStylePainterParameters @default = TextStylePainterParameters.GetDefault(ve, textToMeasure);
					@default.text = textToMeasure;
					@default.font = font;
					@default.wordWrapWidth = 0f;
					@default.wordWrap = false;
					@default.richText = true;
					num = Mathf.Ceil(TextNative.ComputeTextWidth(@default.GetTextNativeSettings(scaling)));
					if (widthMode == VisualElement.MeasureMode.AtMost)
					{
						num = Mathf.Min(num, width);
					}
				}
				if (heightMode == VisualElement.MeasureMode.Exactly)
				{
					num2 = height;
				}
				else
				{
					TextStylePainterParameters default2 = TextStylePainterParameters.GetDefault(ve, textToMeasure);
					default2.text = textToMeasure;
					default2.font = font;
					default2.wordWrapWidth = num;
					default2.richText = true;
					num2 = Mathf.Ceil(TextNative.ComputeTextHeight(default2.GetTextNativeSettings(scaling)));
					if (heightMode == VisualElement.MeasureMode.AtMost)
					{
						num2 = Mathf.Min(num2, height);
					}
				}
				result = new Vector2(num, num2);
			}
			return result;
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x0000D88C File Offset: 0x0000BA8C
		protected internal override Vector2 DoMeasure(float width, VisualElement.MeasureMode widthMode, float height, VisualElement.MeasureMode heightMode)
		{
			return this.MeasureTextSize(this.text, width, widthMode, height, heightMode);
		}

		// Token: 0x040001D2 RID: 466
		internal const string k_TextElementClass = "textElement";

		// Token: 0x040001D3 RID: 467
		[SerializeField]
		private string m_Text;

		// Token: 0x020000A2 RID: 162
		public new class UxmlFactory : UxmlFactory<TextElement, TextElement.UxmlTraits>
		{
			// Token: 0x060003EC RID: 1004 RVA: 0x0000D8B2 File Offset: 0x0000BAB2
			public UxmlFactory()
			{
			}
		}

		// Token: 0x020000A3 RID: 163
		public new class UxmlTraits : VisualElement.UxmlTraits
		{
			// Token: 0x060003ED RID: 1005 RVA: 0x0000D8BC File Offset: 0x0000BABC
			public UxmlTraits()
			{
			}

			// Token: 0x17000110 RID: 272
			// (get) Token: 0x060003EE RID: 1006 RVA: 0x0000D8E8 File Offset: 0x0000BAE8
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield break;
				}
			}

			// Token: 0x060003EF RID: 1007 RVA: 0x0000D90B File Offset: 0x0000BB0B
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				((ITextElement)ve).text = this.m_Text.GetValueFromBag(bag, cc);
			}

			// Token: 0x040001D4 RID: 468
			private UxmlStringAttributeDescription m_Text = new UxmlStringAttributeDescription
			{
				name = "text"
			};

			// Token: 0x020000A4 RID: 164
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x060003F0 RID: 1008 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x060003F1 RID: 1009 RVA: 0x0000D92F File Offset: 0x0000BB2F
				public bool MoveNext()
				{
					bool flag = this.$PC != 0;
					this.$PC = -1;
					if (!flag)
					{
					}
					return false;
				}

				// Token: 0x17000111 RID: 273
				// (get) Token: 0x060003F2 RID: 1010 RVA: 0x0000D94C File Offset: 0x0000BB4C
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x17000112 RID: 274
				// (get) Token: 0x060003F3 RID: 1011 RVA: 0x0000D968 File Offset: 0x0000BB68
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x060003F4 RID: 1012 RVA: 0x0000227E File Offset: 0x0000047E
				[DebuggerHidden]
				public void Dispose()
				{
				}

				// Token: 0x060003F5 RID: 1013 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x060003F6 RID: 1014 RVA: 0x0000D984 File Offset: 0x0000BB84
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x060003F7 RID: 1015 RVA: 0x0000D99E File Offset: 0x0000BB9E
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new TextElement.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x040001D5 RID: 469
				internal UxmlChildElementDescription $current;

				// Token: 0x040001D6 RID: 470
				internal bool $disposing;

				// Token: 0x040001D7 RID: 471
				internal int $PC;
			}
		}
	}
}
