﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000BC RID: 188
	[Flags]
	internal enum PseudoStates
	{
		// Token: 0x0400020F RID: 527
		Active = 1,
		// Token: 0x04000210 RID: 528
		Hover = 2,
		// Token: 0x04000211 RID: 529
		Checked = 8,
		// Token: 0x04000212 RID: 530
		Selected = 16,
		// Token: 0x04000213 RID: 531
		Disabled = 32,
		// Token: 0x04000214 RID: 532
		Focus = 64
	}
}
