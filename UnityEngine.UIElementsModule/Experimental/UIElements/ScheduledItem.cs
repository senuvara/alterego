﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000074 RID: 116
	internal abstract class ScheduledItem : IScheduledItem
	{
		// Token: 0x06000315 RID: 789 RVA: 0x0000AB5A File Offset: 0x00008D5A
		public ScheduledItem()
		{
			this.ResetStartTime();
			this.timerUpdateStopCondition = ScheduledItem.OnceCondition;
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000316 RID: 790 RVA: 0x0000AB74 File Offset: 0x00008D74
		// (set) Token: 0x06000317 RID: 791 RVA: 0x0000AB8E File Offset: 0x00008D8E
		public long startMs
		{
			[CompilerGenerated]
			get
			{
				return this.<startMs>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<startMs>k__BackingField = value;
			}
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x06000318 RID: 792 RVA: 0x0000AB98 File Offset: 0x00008D98
		// (set) Token: 0x06000319 RID: 793 RVA: 0x0000ABB2 File Offset: 0x00008DB2
		public long delayMs
		{
			[CompilerGenerated]
			get
			{
				return this.<delayMs>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<delayMs>k__BackingField = value;
			}
		}

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x0600031A RID: 794 RVA: 0x0000ABBC File Offset: 0x00008DBC
		// (set) Token: 0x0600031B RID: 795 RVA: 0x0000ABD6 File Offset: 0x00008DD6
		public long intervalMs
		{
			[CompilerGenerated]
			get
			{
				return this.<intervalMs>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<intervalMs>k__BackingField = value;
			}
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x0600031C RID: 796 RVA: 0x0000ABE0 File Offset: 0x00008DE0
		// (set) Token: 0x0600031D RID: 797 RVA: 0x0000ABFA File Offset: 0x00008DFA
		public long endTimeMs
		{
			[CompilerGenerated]
			get
			{
				return this.<endTimeMs>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<endTimeMs>k__BackingField = value;
			}
		}

		// Token: 0x0600031E RID: 798 RVA: 0x0000AC03 File Offset: 0x00008E03
		protected void ResetStartTime()
		{
			this.startMs = Panel.TimeSinceStartupMs();
		}

		// Token: 0x0600031F RID: 799 RVA: 0x0000AC11 File Offset: 0x00008E11
		public void SetDuration(long durationMs)
		{
			this.endTimeMs = this.startMs + durationMs;
		}

		// Token: 0x06000320 RID: 800
		public abstract void PerformTimerUpdate(TimerState state);

		// Token: 0x06000321 RID: 801 RVA: 0x000035A6 File Offset: 0x000017A6
		internal virtual void OnItemUnscheduled()
		{
		}

		// Token: 0x06000322 RID: 802 RVA: 0x0000AC24 File Offset: 0x00008E24
		public virtual bool ShouldUnschedule()
		{
			if (this.endTimeMs > 0L)
			{
				if (Panel.TimeSinceStartupMs() > this.endTimeMs)
				{
					return true;
				}
			}
			return this.timerUpdateStopCondition != null && this.timerUpdateStopCondition();
		}

		// Token: 0x06000323 RID: 803 RVA: 0x0000AC7E File Offset: 0x00008E7E
		// Note: this type is marked as 'beforefieldinit'.
		static ScheduledItem()
		{
		}

		// Token: 0x06000324 RID: 804 RVA: 0x0000ACA4 File Offset: 0x00008EA4
		[CompilerGenerated]
		private static bool <OnceCondition>m__0()
		{
			return true;
		}

		// Token: 0x06000325 RID: 805 RVA: 0x0000ACBC File Offset: 0x00008EBC
		[CompilerGenerated]
		private static bool <ForeverCondition>m__1()
		{
			return false;
		}

		// Token: 0x04000133 RID: 307
		public Func<bool> timerUpdateStopCondition;

		// Token: 0x04000134 RID: 308
		public static readonly Func<bool> OnceCondition = () => true;

		// Token: 0x04000135 RID: 309
		public static readonly Func<bool> ForeverCondition = () => false;

		// Token: 0x04000136 RID: 310
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private long <startMs>k__BackingField;

		// Token: 0x04000137 RID: 311
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private long <delayMs>k__BackingField;

		// Token: 0x04000138 RID: 312
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private long <intervalMs>k__BackingField;

		// Token: 0x04000139 RID: 313
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private long <endTimeMs>k__BackingField;
	}
}
