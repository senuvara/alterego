﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000096 RID: 150
	internal struct BorderParameters
	{
		// Token: 0x060003C2 RID: 962 RVA: 0x0000CEA2 File Offset: 0x0000B0A2
		public void SetWidth(float top, float right, float bottom, float left)
		{
			this.topWidth = top;
			this.rightWidth = right;
			this.bottomWidth = bottom;
			this.leftWidth = left;
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x0000CEC2 File Offset: 0x0000B0C2
		public void SetWidth(float allBorders)
		{
			this.SetWidth(allBorders, allBorders, allBorders, allBorders);
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x0000CECF File Offset: 0x0000B0CF
		public void SetRadius(float topLeft, float topRight, float bottomRight, float bottomLeft)
		{
			this.topLeftRadius = topLeft;
			this.topRightRadius = topRight;
			this.bottomRightRadius = bottomRight;
			this.bottomLeftRadius = bottomLeft;
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x0000CEEF File Offset: 0x0000B0EF
		public void SetRadius(float radius)
		{
			this.SetRadius(radius, radius, radius, radius);
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x0000CEFC File Offset: 0x0000B0FC
		public Vector4 GetWidths()
		{
			return new Vector4(this.leftWidth, this.topWidth, this.rightWidth, this.bottomWidth);
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x0000CF30 File Offset: 0x0000B130
		public Vector4 GetRadiuses()
		{
			return new Vector4(this.topLeftRadius, this.topRightRadius, this.bottomRightRadius, this.bottomLeftRadius);
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x0000CF64 File Offset: 0x0000B164
		public static void SetFromStyle(ref BorderParameters border, IStyle style)
		{
			border.SetWidth(style.borderTopWidth, style.borderRightWidth, style.borderBottomWidth, style.borderLeftWidth);
			border.SetRadius(style.borderTopLeftRadius, style.borderTopRightRadius, style.borderBottomRightRadius, style.borderBottomLeftRadius);
		}

		// Token: 0x0400019F RID: 415
		public float leftWidth;

		// Token: 0x040001A0 RID: 416
		public float topWidth;

		// Token: 0x040001A1 RID: 417
		public float rightWidth;

		// Token: 0x040001A2 RID: 418
		public float bottomWidth;

		// Token: 0x040001A3 RID: 419
		public float topLeftRadius;

		// Token: 0x040001A4 RID: 420
		public float topRightRadius;

		// Token: 0x040001A5 RID: 421
		public float bottomRightRadius;

		// Token: 0x040001A6 RID: 422
		public float bottomLeftRadius;
	}
}
