﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000179 RID: 377
	internal static class VisualElementFactoryRegistry
	{
		// Token: 0x17000281 RID: 641
		// (get) Token: 0x060009B3 RID: 2483 RVA: 0x00021824 File Offset: 0x0001FA24
		// (set) Token: 0x060009B4 RID: 2484 RVA: 0x0002183D File Offset: 0x0001FA3D
		internal static Dictionary<string, List<IUxmlFactory>> factories
		{
			[CompilerGenerated]
			get
			{
				return VisualElementFactoryRegistry.<factories>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				VisualElementFactoryRegistry.<factories>k__BackingField = value;
			}
		}

		// Token: 0x060009B5 RID: 2485 RVA: 0x00021848 File Offset: 0x0001FA48
		internal static void RegisterFactory(IUxmlFactory factory)
		{
			VisualElementFactoryRegistry.DiscoverFactories();
			List<IUxmlFactory> list;
			if (VisualElementFactoryRegistry.factories.TryGetValue(factory.uxmlQualifiedName, out list))
			{
				foreach (IUxmlFactory uxmlFactory in list)
				{
					if (uxmlFactory.GetType() == factory.GetType())
					{
						throw new ArgumentException("A factory of this type was already registered");
					}
				}
				list.Add(factory);
			}
			else
			{
				list = new List<IUxmlFactory>();
				list.Add(factory);
				VisualElementFactoryRegistry.factories.Add(factory.uxmlQualifiedName, list);
			}
		}

		// Token: 0x060009B6 RID: 2486 RVA: 0x00021904 File Offset: 0x0001FB04
		internal static void DiscoverFactories()
		{
			if (VisualElementFactoryRegistry.factories == null)
			{
				VisualElementFactoryRegistry.factories = new Dictionary<string, List<IUxmlFactory>>();
				VisualElementFactoryRegistry.RegisterEngineFactories();
			}
		}

		// Token: 0x060009B7 RID: 2487 RVA: 0x00021928 File Offset: 0x0001FB28
		internal static bool TryGetValue(string fullTypeName, out List<IUxmlFactory> factoryList)
		{
			VisualElementFactoryRegistry.DiscoverFactories();
			factoryList = null;
			return VisualElementFactoryRegistry.factories != null && VisualElementFactoryRegistry.factories.TryGetValue(fullTypeName, out factoryList);
		}

		// Token: 0x060009B8 RID: 2488 RVA: 0x00021960 File Offset: 0x0001FB60
		private static void RegisterEngineFactories()
		{
			IUxmlFactory[] array = new IUxmlFactory[]
			{
				new UxmlRootElementFactory(),
				new Button.UxmlFactory(),
				new VisualElement.UxmlFactory(),
				new IMGUIContainer.UxmlFactory(),
				new Image.UxmlFactory(),
				new Label.UxmlFactory(),
				new RepeatButton.UxmlFactory(),
				new ScrollerButton.UxmlFactory(),
				new ScrollView.UxmlFactory(),
				new Scroller.UxmlFactory(),
				new Slider.UxmlFactory(),
				new SliderInt.UxmlFactory(),
				new MinMaxSlider.UxmlFactory(),
				new TextField.UxmlFactory(),
				new Toggle.UxmlFactory(),
				new VisualContainer.UxmlFactory(),
				new TemplateContainer.UxmlFactory(),
				new Box.UxmlFactory(),
				new PopupWindow.UxmlFactory(),
				new ListView.UxmlFactory(),
				new Foldout.UxmlFactory(),
				new BindableElement.UxmlFactory()
			};
			foreach (IUxmlFactory factory in array)
			{
				VisualElementFactoryRegistry.RegisterFactory(factory);
			}
		}

		// Token: 0x04000459 RID: 1113
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Dictionary<string, List<IUxmlFactory>> <factories>k__BackingField;
	}
}
