﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200016D RID: 365
	public interface IUxmlFactory
	{
		// Token: 0x1700025D RID: 605
		// (get) Token: 0x06000962 RID: 2402
		string uxmlName { get; }

		// Token: 0x1700025E RID: 606
		// (get) Token: 0x06000963 RID: 2403
		string uxmlNamespace { get; }

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x06000964 RID: 2404
		string uxmlQualifiedName { get; }

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x06000965 RID: 2405
		bool canHaveAnyAttribute { get; }

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x06000966 RID: 2406
		IEnumerable<UxmlAttributeDescription> uxmlAttributesDescription { get; }

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x06000967 RID: 2407
		IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription { get; }

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x06000968 RID: 2408
		string substituteForTypeName { get; }

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x06000969 RID: 2409
		string substituteForTypeNamespace { get; }

		// Token: 0x17000265 RID: 613
		// (get) Token: 0x0600096A RID: 2410
		string substituteForTypeQualifiedName { get; }

		// Token: 0x0600096B RID: 2411
		bool AcceptsAttributeBag(IUxmlAttributes bag, CreationContext cc);

		// Token: 0x0600096C RID: 2412
		VisualElement Create(IUxmlAttributes bag, CreationContext cc);

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x0600096D RID: 2413
		[Obsolete("Use uxmlName and uxmlNamespace instead.")]
		Type CreatesType { get; }
	}
}
