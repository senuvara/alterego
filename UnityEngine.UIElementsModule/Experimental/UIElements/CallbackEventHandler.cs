﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000123 RID: 291
	public abstract class CallbackEventHandler : IEventHandler
	{
		// Token: 0x060007E5 RID: 2021 RVA: 0x00002223 File Offset: 0x00000423
		protected CallbackEventHandler()
		{
		}

		// Token: 0x060007E6 RID: 2022 RVA: 0x0001AF68 File Offset: 0x00019168
		[Obsolete("Use TrickleDown instead of Capture.")]
		public void RegisterCallback<TEventType>(EventCallback<TEventType> callback, Capture useCapture) where TEventType : EventBase<TEventType>, new()
		{
			this.RegisterCallback<TEventType>(callback, (TrickleDown)useCapture);
		}

		// Token: 0x060007E7 RID: 2023 RVA: 0x0001AF80 File Offset: 0x00019180
		public void RegisterCallback<TEventType>(EventCallback<TEventType> callback, TrickleDown useTrickleDown = TrickleDown.NoTrickleDown) where TEventType : EventBase<TEventType>, new()
		{
			if (this.m_CallbackRegistry == null)
			{
				this.m_CallbackRegistry = new EventCallbackRegistry();
			}
			this.m_CallbackRegistry.RegisterCallback<TEventType>(callback, useTrickleDown);
		}

		// Token: 0x060007E8 RID: 2024 RVA: 0x0001AFA8 File Offset: 0x000191A8
		[Obsolete("Use TrickleDown instead of Capture.")]
		public void RegisterCallback<TEventType, TUserArgsType>(EventCallback<TEventType, TUserArgsType> callback, TUserArgsType userArgs, Capture useCapture) where TEventType : EventBase<TEventType>, new()
		{
			this.RegisterCallback<TEventType, TUserArgsType>(callback, userArgs, (TrickleDown)useCapture);
		}

		// Token: 0x060007E9 RID: 2025 RVA: 0x0001AFC1 File Offset: 0x000191C1
		public void RegisterCallback<TEventType, TUserArgsType>(EventCallback<TEventType, TUserArgsType> callback, TUserArgsType userArgs, TrickleDown useTrickleDown = TrickleDown.NoTrickleDown) where TEventType : EventBase<TEventType>, new()
		{
			if (this.m_CallbackRegistry == null)
			{
				this.m_CallbackRegistry = new EventCallbackRegistry();
			}
			this.m_CallbackRegistry.RegisterCallback<TEventType, TUserArgsType>(callback, userArgs, useTrickleDown);
		}

		// Token: 0x060007EA RID: 2026 RVA: 0x0001AFEC File Offset: 0x000191EC
		[Obsolete("Use TrickleDown instead of Capture.")]
		public void UnregisterCallback<TEventType>(EventCallback<TEventType> callback, Capture useCapture) where TEventType : EventBase<TEventType>, new()
		{
			this.UnregisterCallback<TEventType>(callback, (TrickleDown)useCapture);
		}

		// Token: 0x060007EB RID: 2027 RVA: 0x0001B004 File Offset: 0x00019204
		public void UnregisterCallback<TEventType>(EventCallback<TEventType> callback, TrickleDown useTrickleDown = TrickleDown.NoTrickleDown) where TEventType : EventBase<TEventType>, new()
		{
			if (this.m_CallbackRegistry != null)
			{
				this.m_CallbackRegistry.UnregisterCallback<TEventType>(callback, useTrickleDown);
			}
		}

		// Token: 0x060007EC RID: 2028 RVA: 0x0001B024 File Offset: 0x00019224
		[Obsolete("Use TrickleDown instead of Capture.")]
		public void UnregisterCallback<TEventType, TUserArgsType>(EventCallback<TEventType, TUserArgsType> callback, Capture useCapture) where TEventType : EventBase<TEventType>, new()
		{
			this.UnregisterCallback<TEventType, TUserArgsType>(callback, (TrickleDown)useCapture);
		}

		// Token: 0x060007ED RID: 2029 RVA: 0x0001B03C File Offset: 0x0001923C
		public void UnregisterCallback<TEventType, TUserArgsType>(EventCallback<TEventType, TUserArgsType> callback, TrickleDown useTrickleDown = TrickleDown.NoTrickleDown) where TEventType : EventBase<TEventType>, new()
		{
			if (this.m_CallbackRegistry != null)
			{
				this.m_CallbackRegistry.UnregisterCallback<TEventType, TUserArgsType>(callback, useTrickleDown);
			}
		}

		// Token: 0x060007EE RID: 2030 RVA: 0x0001B05C File Offset: 0x0001925C
		internal bool TryGetUserArgs<TEventType, TCallbackArgs>(EventCallback<TEventType, TCallbackArgs> callback, TrickleDown useTrickleDown, out TCallbackArgs userData) where TEventType : EventBase<TEventType>, new()
		{
			userData = default(TCallbackArgs);
			return this.m_CallbackRegistry != null && this.m_CallbackRegistry.TryGetUserArgs<TEventType, TCallbackArgs>(callback, useTrickleDown, out userData);
		}

		// Token: 0x060007EF RID: 2031
		public abstract void SendEvent(EventBase e);

		// Token: 0x060007F0 RID: 2032 RVA: 0x0001B0A4 File Offset: 0x000192A4
		public virtual void HandleEvent(EventBase evt)
		{
			if (evt.propagationPhase != PropagationPhase.DefaultAction)
			{
				if (!evt.isPropagationStopped)
				{
					if (this.m_CallbackRegistry != null)
					{
						this.m_CallbackRegistry.InvokeCallbacks(evt);
					}
				}
				if (evt.propagationPhase == PropagationPhase.AtTarget && !evt.isDefaultPrevented)
				{
					this.ExecuteDefaultActionAtTarget(evt);
				}
			}
			else if (!evt.isDefaultPrevented)
			{
				this.ExecuteDefaultAction(evt);
			}
		}

		// Token: 0x060007F1 RID: 2033 RVA: 0x0001B120 File Offset: 0x00019320
		public bool HasTrickleDownHandlers()
		{
			return this.m_CallbackRegistry != null && this.m_CallbackRegistry.HasTrickleDownHandlers();
		}

		// Token: 0x060007F2 RID: 2034 RVA: 0x0001B150 File Offset: 0x00019350
		public bool HasBubbleUpHandlers()
		{
			return this.m_CallbackRegistry != null && this.m_CallbackRegistry.HasBubbleHandlers();
		}

		// Token: 0x060007F3 RID: 2035 RVA: 0x0001B180 File Offset: 0x00019380
		[Obsolete("Use HasTrickleDownHandlers instead of HasCaptureHandlers.")]
		public bool HasCaptureHandlers()
		{
			return this.HasTrickleDownHandlers();
		}

		// Token: 0x060007F4 RID: 2036 RVA: 0x0001B19C File Offset: 0x0001939C
		[Obsolete("Use HasBubbleUpHandlers instead of HasBubbleHandlers.")]
		public bool HasBubbleHandlers()
		{
			return this.HasBubbleUpHandlers();
		}

		// Token: 0x060007F5 RID: 2037 RVA: 0x000035A6 File Offset: 0x000017A6
		protected internal virtual void ExecuteDefaultActionAtTarget(EventBase evt)
		{
		}

		// Token: 0x060007F6 RID: 2038 RVA: 0x000035A6 File Offset: 0x000017A6
		protected internal virtual void ExecuteDefaultAction(EventBase evt)
		{
		}

		// Token: 0x0400030F RID: 783
		private EventCallbackRegistry m_CallbackRegistry;
	}
}
