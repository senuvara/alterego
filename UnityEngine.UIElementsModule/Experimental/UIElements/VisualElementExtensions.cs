﻿using System;
using UnityEngine.Experimental.UIElements.StyleEnums;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000CB RID: 203
	public static class VisualElementExtensions
	{
		// Token: 0x06000609 RID: 1545 RVA: 0x00014918 File Offset: 0x00012B18
		public static Vector2 WorldToLocal(this VisualElement ele, Vector2 p)
		{
			return ele.worldTransform.inverse.MultiplyPoint3x4(p);
		}

		// Token: 0x0600060A RID: 1546 RVA: 0x00014950 File Offset: 0x00012B50
		public static Vector2 LocalToWorld(this VisualElement ele, Vector2 p)
		{
			return ele.worldTransform.MultiplyPoint3x4(p);
		}

		// Token: 0x0600060B RID: 1547 RVA: 0x00014980 File Offset: 0x00012B80
		public static Rect WorldToLocal(this VisualElement ele, Rect r)
		{
			Matrix4x4 inverse = ele.worldTransform.inverse;
			Vector2 position = inverse.MultiplyPoint3x4(r.position);
			r.position = position;
			r.size = inverse.MultiplyVector(r.size);
			return r;
		}

		// Token: 0x0600060C RID: 1548 RVA: 0x000149E8 File Offset: 0x00012BE8
		public static Rect LocalToWorld(this VisualElement ele, Rect r)
		{
			Matrix4x4 worldTransform = ele.worldTransform;
			r.position = worldTransform.MultiplyPoint3x4(r.position);
			r.size = worldTransform.MultiplyVector(r.size);
			return r;
		}

		// Token: 0x0600060D RID: 1549 RVA: 0x00014A44 File Offset: 0x00012C44
		public static Vector2 ChangeCoordinatesTo(this VisualElement src, VisualElement dest, Vector2 point)
		{
			return dest.WorldToLocal(src.LocalToWorld(point));
		}

		// Token: 0x0600060E RID: 1550 RVA: 0x00014A68 File Offset: 0x00012C68
		public static Rect ChangeCoordinatesTo(this VisualElement src, VisualElement dest, Rect rect)
		{
			return dest.WorldToLocal(src.LocalToWorld(rect));
		}

		// Token: 0x0600060F RID: 1551 RVA: 0x00014A8C File Offset: 0x00012C8C
		public static void StretchToParentSize(this VisualElement elem)
		{
			IStyle style = elem.style;
			style.positionType = PositionType.Absolute;
			style.positionLeft = 0f;
			style.positionTop = 0f;
			style.positionRight = 0f;
			style.positionBottom = 0f;
		}

		// Token: 0x06000610 RID: 1552 RVA: 0x00014AF0 File Offset: 0x00012CF0
		public static void StretchToParentWidth(this VisualElement elem)
		{
			IStyle style = elem.style;
			style.positionType = PositionType.Absolute;
			style.positionLeft = 0f;
			style.positionRight = 0f;
		}

		// Token: 0x06000611 RID: 1553 RVA: 0x00014B31 File Offset: 0x00012D31
		public static void AddManipulator(this VisualElement ele, IManipulator manipulator)
		{
			if (manipulator != null)
			{
				manipulator.target = ele;
			}
		}

		// Token: 0x06000612 RID: 1554 RVA: 0x00014B43 File Offset: 0x00012D43
		public static void RemoveManipulator(this VisualElement ele, IManipulator manipulator)
		{
			if (manipulator != null)
			{
				manipulator.target = null;
			}
		}
	}
}
