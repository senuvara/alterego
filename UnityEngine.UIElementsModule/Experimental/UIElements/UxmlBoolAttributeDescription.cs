﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000165 RID: 357
	public class UxmlBoolAttributeDescription : UxmlAttributeDescription
	{
		// Token: 0x06000928 RID: 2344 RVA: 0x0001FECC File Offset: 0x0001E0CC
		public UxmlBoolAttributeDescription()
		{
			base.type = "boolean";
			base.typeNamespace = "http://www.w3.org/2001/XMLSchema";
			this.defaultValue = false;
		}

		// Token: 0x1700024C RID: 588
		// (get) Token: 0x06000929 RID: 2345 RVA: 0x0001FEF4 File Offset: 0x0001E0F4
		// (set) Token: 0x0600092A RID: 2346 RVA: 0x0001FF0E File Offset: 0x0001E10E
		public bool defaultValue
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultValue>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultValue>k__BackingField = value;
			}
		}

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x0600092B RID: 2347 RVA: 0x0001FF18 File Offset: 0x0001E118
		public override string defaultValueAsString
		{
			get
			{
				return this.defaultValue.ToString().ToLower();
			}
		}

		// Token: 0x0600092C RID: 2348 RVA: 0x0001FF48 File Offset: 0x0001E148
		[Obsolete("Pass a creation context to the method.")]
		public bool GetValueFromBag(IUxmlAttributes bag)
		{
			return this.GetValueFromBag(bag, default(CreationContext));
		}

		// Token: 0x0600092D RID: 2349 RVA: 0x0001FF70 File Offset: 0x0001E170
		public bool GetValueFromBag(IUxmlAttributes bag, CreationContext cc)
		{
			if (UxmlBoolAttributeDescription.<>f__mg$cache0 == null)
			{
				UxmlBoolAttributeDescription.<>f__mg$cache0 = new Func<string, bool, bool>(UxmlBoolAttributeDescription.ConvertValueToBool);
			}
			return base.GetValueFromBag<bool>(bag, cc, UxmlBoolAttributeDescription.<>f__mg$cache0, this.defaultValue);
		}

		// Token: 0x0600092E RID: 2350 RVA: 0x0001FFB0 File Offset: 0x0001E1B0
		private static bool ConvertValueToBool(string v, bool defaultValue)
		{
			bool flag;
			bool result;
			if (v == null || !bool.TryParse(v, out flag))
			{
				result = defaultValue;
			}
			else
			{
				result = flag;
			}
			return result;
		}

		// Token: 0x0400041E RID: 1054
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <defaultValue>k__BackingField;

		// Token: 0x0400041F RID: 1055
		[CompilerGenerated]
		private static Func<string, bool, bool> <>f__mg$cache0;
	}
}
