﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000F0 RID: 240
	public interface INotifyValueChanged<T>
	{
		// Token: 0x170001BF RID: 447
		// (get) Token: 0x060006B9 RID: 1721
		// (set) Token: 0x060006BA RID: 1722
		T value { get; set; }

		// Token: 0x060006BB RID: 1723
		[Obsolete("This method is replaced by simply using this.value. The default behaviour has been changed to notify when changed. If the behaviour is not to be notified, SetValueWithoutNotify() must be used.", false)]
		void SetValueAndNotify(T newValue);

		// Token: 0x060006BC RID: 1724
		void SetValueWithoutNotify(T newValue);

		// Token: 0x060006BD RID: 1725
		void OnValueChanged(EventCallback<ChangeEvent<T>> callback);

		// Token: 0x060006BE RID: 1726
		void RemoveOnValueChanged(EventCallback<ChangeEvent<T>> callback);
	}
}
