﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000082 RID: 130
	public class Slider : BaseSlider<float>
	{
		// Token: 0x0600038A RID: 906 RVA: 0x0000C691 File Offset: 0x0000A891
		public Slider() : this(0f, 10f, SliderDirection.Horizontal, 0f)
		{
		}

		// Token: 0x0600038B RID: 907 RVA: 0x0000C6AA File Offset: 0x0000A8AA
		public Slider(float start, float end, Action<float> valueChanged, SliderDirection direction = SliderDirection.Horizontal, float pageSize = 0f) : this(start, end, direction, pageSize)
		{
			this.valueChanged = valueChanged;
		}

		// Token: 0x0600038C RID: 908 RVA: 0x0000C6C0 File Offset: 0x0000A8C0
		public Slider(float start, float end, SliderDirection direction = SliderDirection.Horizontal, float pageSize = 0f) : base(start, end, direction, pageSize)
		{
		}

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x0600038D RID: 909 RVA: 0x0000C6D0 File Offset: 0x0000A8D0
		// (set) Token: 0x0600038E RID: 910 RVA: 0x0000C6EC File Offset: 0x0000A8EC
		public Action<float> valueChanged
		{
			get
			{
				return this.m_ValueChanged;
			}
			set
			{
				if (value != null && this.m_ValueChanged == null)
				{
					base.OnValueChanged(new EventCallback<ChangeEvent<float>>(this.InternalOnValueChanged));
				}
				else if (value == null && this.m_ValueChanged != null)
				{
					base.UnregisterCallback<ChangeEvent<float>>(new EventCallback<ChangeEvent<float>>(this.InternalOnValueChanged), TrickleDown.NoTrickleDown);
				}
				this.m_ValueChanged = value;
			}
		}

		// Token: 0x0600038F RID: 911 RVA: 0x0000C751 File Offset: 0x0000A951
		private void InternalOnValueChanged(ChangeEvent<float> evt)
		{
			if (this.m_ValueChanged != null)
			{
				this.m_ValueChanged(this.value);
			}
		}

		// Token: 0x06000390 RID: 912 RVA: 0x0000C774 File Offset: 0x0000A974
		internal override float SliderLerpUnclamped(float a, float b, float interpolant)
		{
			return Mathf.LerpUnclamped(a, b, interpolant);
		}

		// Token: 0x06000391 RID: 913 RVA: 0x0000C794 File Offset: 0x0000A994
		internal override float SliderNormalizeValue(float currentValue, float lowerValue, float higherValue)
		{
			return (currentValue - lowerValue) / (higherValue - lowerValue);
		}

		// Token: 0x06000392 RID: 914 RVA: 0x0000C7B0 File Offset: 0x0000A9B0
		internal override float SliderRange()
		{
			return Math.Abs(base.highValue - base.lowValue);
		}

		// Token: 0x04000162 RID: 354
		private Action<float> m_ValueChanged;

		// Token: 0x04000163 RID: 355
		internal const float kDefaultHighValue = 10f;

		// Token: 0x02000083 RID: 131
		public new class UxmlFactory : UxmlFactory<Slider, Slider.UxmlTraits>
		{
			// Token: 0x06000393 RID: 915 RVA: 0x0000C7D7 File Offset: 0x0000A9D7
			public UxmlFactory()
			{
			}
		}

		// Token: 0x02000084 RID: 132
		public new class UxmlTraits : BaseField<float>.UxmlTraits
		{
			// Token: 0x06000394 RID: 916 RVA: 0x0000C7E0 File Offset: 0x0000A9E0
			public UxmlTraits()
			{
			}

			// Token: 0x170000FF RID: 255
			// (get) Token: 0x06000395 RID: 917 RVA: 0x0000C8C4 File Offset: 0x0000AAC4
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield break;
				}
			}

			// Token: 0x06000396 RID: 918 RVA: 0x0000C8E8 File Offset: 0x0000AAE8
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				Slider slider = (Slider)ve;
				slider.lowValue = this.m_LowValue.GetValueFromBag(bag, cc);
				slider.highValue = this.m_HighValue.GetValueFromBag(bag, cc);
				slider.direction = this.m_Direction.GetValueFromBag(bag, cc);
				slider.pageSize = this.m_PageSize.GetValueFromBag(bag, cc);
				slider.SetValueWithoutNotify(this.m_Value.GetValueFromBag(bag, cc));
			}

			// Token: 0x04000164 RID: 356
			private UxmlFloatAttributeDescription m_LowValue = new UxmlFloatAttributeDescription
			{
				name = "low-value",
				obsoleteNames = new string[]
				{
					"lowValue"
				}
			};

			// Token: 0x04000165 RID: 357
			private UxmlFloatAttributeDescription m_HighValue = new UxmlFloatAttributeDescription
			{
				name = "high-value",
				obsoleteNames = new string[]
				{
					"highValue"
				},
				defaultValue = 10f
			};

			// Token: 0x04000166 RID: 358
			private UxmlFloatAttributeDescription m_PageSize = new UxmlFloatAttributeDescription
			{
				name = "page-size",
				obsoleteNames = new string[]
				{
					"pageSize"
				},
				defaultValue = 0f
			};

			// Token: 0x04000167 RID: 359
			private UxmlEnumAttributeDescription<SliderDirection> m_Direction = new UxmlEnumAttributeDescription<SliderDirection>
			{
				name = "direction",
				defaultValue = SliderDirection.Vertical
			};

			// Token: 0x04000168 RID: 360
			private UxmlFloatAttributeDescription m_Value = new UxmlFloatAttributeDescription
			{
				name = "value"
			};

			// Token: 0x02000085 RID: 133
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x06000397 RID: 919 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x06000398 RID: 920 RVA: 0x0000C965 File Offset: 0x0000AB65
				public bool MoveNext()
				{
					bool flag = this.$PC != 0;
					this.$PC = -1;
					if (!flag)
					{
					}
					return false;
				}

				// Token: 0x17000100 RID: 256
				// (get) Token: 0x06000399 RID: 921 RVA: 0x0000C980 File Offset: 0x0000AB80
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x17000101 RID: 257
				// (get) Token: 0x0600039A RID: 922 RVA: 0x0000C99C File Offset: 0x0000AB9C
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x0600039B RID: 923 RVA: 0x0000227E File Offset: 0x0000047E
				[DebuggerHidden]
				public void Dispose()
				{
				}

				// Token: 0x0600039C RID: 924 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x0600039D RID: 925 RVA: 0x0000C9B8 File Offset: 0x0000ABB8
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x0600039E RID: 926 RVA: 0x0000C9D2 File Offset: 0x0000ABD2
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new Slider.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x04000169 RID: 361
				internal UxmlChildElementDescription $current;

				// Token: 0x0400016A RID: 362
				internal bool $disposing;

				// Token: 0x0400016B RID: 363
				internal int $PC;
			}
		}
	}
}
