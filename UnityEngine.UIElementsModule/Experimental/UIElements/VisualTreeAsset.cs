﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine.Assertions;
using UnityEngine.Experimental.UIElements.StyleSheets;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200017A RID: 378
	[Serializable]
	public class VisualTreeAsset : ScriptableObject
	{
		// Token: 0x060009B9 RID: 2489 RVA: 0x0000A105 File Offset: 0x00008305
		public VisualTreeAsset()
		{
		}

		// Token: 0x17000282 RID: 642
		// (get) Token: 0x060009BA RID: 2490 RVA: 0x00021A58 File Offset: 0x0001FC58
		// (set) Token: 0x060009BB RID: 2491 RVA: 0x00021A73 File Offset: 0x0001FC73
		internal List<VisualElementAsset> visualElementAssets
		{
			get
			{
				return this.m_VisualElementAssets;
			}
			set
			{
				this.m_VisualElementAssets = value;
			}
		}

		// Token: 0x17000283 RID: 643
		// (get) Token: 0x060009BC RID: 2492 RVA: 0x00021A80 File Offset: 0x0001FC80
		// (set) Token: 0x060009BD RID: 2493 RVA: 0x00021A9B File Offset: 0x0001FC9B
		internal List<TemplateAsset> templateAssets
		{
			get
			{
				return this.m_TemplateAssets;
			}
			set
			{
				this.m_TemplateAssets = value;
			}
		}

		// Token: 0x17000284 RID: 644
		// (get) Token: 0x060009BE RID: 2494 RVA: 0x00021AA8 File Offset: 0x0001FCA8
		// (set) Token: 0x060009BF RID: 2495 RVA: 0x00021AC3 File Offset: 0x0001FCC3
		internal List<VisualTreeAsset.SlotDefinition> slots
		{
			get
			{
				return this.m_Slots;
			}
			set
			{
				this.m_Slots = value;
			}
		}

		// Token: 0x17000285 RID: 645
		// (get) Token: 0x060009C0 RID: 2496 RVA: 0x00021AD0 File Offset: 0x0001FCD0
		// (set) Token: 0x060009C1 RID: 2497 RVA: 0x00021AEB File Offset: 0x0001FCEB
		internal int contentContainerId
		{
			get
			{
				return this.m_ContentContainerId;
			}
			set
			{
				this.m_ContentContainerId = value;
			}
		}

		// Token: 0x060009C2 RID: 2498 RVA: 0x00021AF8 File Offset: 0x0001FCF8
		public VisualElement CloneTree(Dictionary<string, VisualElement> slotInsertionPoints)
		{
			TemplateContainer templateContainer = new TemplateContainer(base.name);
			this.CloneTree(templateContainer, slotInsertionPoints ?? new Dictionary<string, VisualElement>());
			return templateContainer;
		}

		// Token: 0x060009C3 RID: 2499 RVA: 0x00021B30 File Offset: 0x0001FD30
		public VisualElement CloneTree(Dictionary<string, VisualElement> slotInsertionPoints, string bindingPath)
		{
			TemplateContainer templateContainer = this.CloneTree(slotInsertionPoints) as TemplateContainer;
			templateContainer.bindingPath = bindingPath;
			return templateContainer;
		}

		// Token: 0x060009C4 RID: 2500 RVA: 0x00021B5C File Offset: 0x0001FD5C
		public void CloneTree(VisualElement target, Dictionary<string, VisualElement> slotInsertionPoints)
		{
			if (target == null)
			{
				throw new ArgumentNullException("target", "Cannot clone a Visual Tree in a null target");
			}
			if ((this.m_VisualElementAssets != null && this.m_VisualElementAssets.Count > 0) || (this.m_TemplateAssets != null && this.m_TemplateAssets.Count > 0))
			{
				Dictionary<int, List<VisualElementAsset>> dictionary = new Dictionary<int, List<VisualElementAsset>>();
				int num = (this.m_VisualElementAssets != null) ? this.m_VisualElementAssets.Count : 0;
				int num2 = (this.m_TemplateAssets != null) ? this.m_TemplateAssets.Count : 0;
				for (int i = 0; i < num + num2; i++)
				{
					VisualElementAsset visualElementAsset = (i >= num) ? this.m_TemplateAssets[i - num] : this.m_VisualElementAssets[i];
					List<VisualElementAsset> list;
					if (!dictionary.TryGetValue(visualElementAsset.parentId, out list))
					{
						list = new List<VisualElementAsset>();
						dictionary.Add(visualElementAsset.parentId, list);
					}
					list.Add(visualElementAsset);
				}
				List<VisualElementAsset> list2;
				if (dictionary.TryGetValue(0, out list2) && list2 != null)
				{
					foreach (VisualElementAsset visualElementAsset2 in list2)
					{
						Assert.IsNotNull<VisualElementAsset>(visualElementAsset2);
						VisualElement child = this.CloneSetupRecursively(visualElementAsset2, dictionary, new CreationContext(slotInsertionPoints, this, target));
						target.shadow.Add(child);
					}
				}
			}
		}

		// Token: 0x060009C5 RID: 2501 RVA: 0x00021CFC File Offset: 0x0001FEFC
		private VisualElement CloneSetupRecursively(VisualElementAsset root, Dictionary<int, List<VisualElementAsset>> idToChildren, CreationContext context)
		{
			VisualElement visualElement = root.Create(context);
			if (root.id == context.visualTreeAsset.contentContainerId)
			{
				if (context.target is TemplateContainer)
				{
					((TemplateContainer)context.target).SetContentContainer(visualElement);
				}
				else
				{
					Debug.LogError("Trying to clone a VisualTreeAsset with a custom content container into a element which is not a template container");
				}
			}
			string key;
			if (context.slotInsertionPoints != null && this.TryGetSlotInsertionPoint(root.id, out key))
			{
				context.slotInsertionPoints.Add(key, visualElement);
			}
			if (root.classes != null)
			{
				for (int i = 0; i < root.classes.Length; i++)
				{
					visualElement.AddToClassList(root.classes[i]);
				}
			}
			if (root.ruleIndex != -1)
			{
				if (this.inlineSheet == null)
				{
					Debug.LogWarning("VisualElementAsset has a RuleIndex but no inlineStyleSheet");
				}
				else
				{
					StyleRule rule = this.inlineSheet.rules[root.ruleIndex];
					VisualElementStylesData visualElementStylesData = new VisualElementStylesData(false);
					visualElement.SetInlineStyles(visualElementStylesData);
					visualElementStylesData.ApplyRule(this.inlineSheet, int.MaxValue, rule, StyleSheetCache.GetPropertyIDs(this.inlineSheet, root.ruleIndex));
				}
			}
			TemplateAsset templateAsset = root as TemplateAsset;
			List<VisualElementAsset> list;
			if (idToChildren.TryGetValue(root.id, out list))
			{
				using (List<VisualElementAsset>.Enumerator enumerator = list.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						VisualElementAsset childVea = enumerator.Current;
						VisualElement visualElement2 = this.CloneSetupRecursively(childVea, idToChildren, context);
						if (visualElement2 != null)
						{
							if (templateAsset == null)
							{
								visualElement.Add(visualElement2);
							}
							else
							{
								int num = (templateAsset.slotUsages != null) ? templateAsset.slotUsages.FindIndex((VisualTreeAsset.SlotUsageEntry u) => u.assetId == childVea.id) : -1;
								if (num != -1)
								{
									string slotName = templateAsset.slotUsages[num].slotName;
									Assert.IsFalse(string.IsNullOrEmpty(slotName), "a lost name should not be null or empty, this probably points to an importer or serialization bug");
									VisualElement visualElement3;
									if (context.slotInsertionPoints == null || !context.slotInsertionPoints.TryGetValue(slotName, out visualElement3))
									{
										Debug.LogErrorFormat("Slot '{0}' was not found. Existing slots: {1}", new object[]
										{
											slotName,
											(context.slotInsertionPoints != null) ? string.Join(", ", context.slotInsertionPoints.Keys.ToArray<string>()) : string.Empty
										});
										visualElement.Add(visualElement2);
									}
									else
									{
										visualElement3.Add(visualElement2);
									}
								}
								else
								{
									visualElement.Add(visualElement2);
								}
							}
						}
					}
				}
			}
			if (templateAsset != null && context.slotInsertionPoints != null)
			{
				context.slotInsertionPoints.Clear();
			}
			return visualElement;
		}

		// Token: 0x060009C6 RID: 2502 RVA: 0x00022004 File Offset: 0x00020204
		internal bool TryGetSlotInsertionPoint(int insertionPointId, out string slotName)
		{
			bool result;
			if (this.m_Slots == null)
			{
				slotName = null;
				result = false;
			}
			else
			{
				for (int i = 0; i < this.m_Slots.Count; i++)
				{
					VisualTreeAsset.SlotDefinition slotDefinition = this.m_Slots[i];
					if (slotDefinition.insertionPointId == insertionPointId)
					{
						slotName = slotDefinition.name;
						return true;
					}
				}
				slotName = null;
				result = false;
			}
			return result;
		}

		// Token: 0x060009C7 RID: 2503 RVA: 0x0002207C File Offset: 0x0002027C
		internal VisualTreeAsset ResolveTemplate(string templateName)
		{
			VisualTreeAsset result;
			if (this.m_Usings == null || this.m_Usings.Count == 0)
			{
				result = null;
			}
			else
			{
				int num = this.m_Usings.BinarySearch(new VisualTreeAsset.UsingEntry(templateName, null), VisualTreeAsset.UsingEntry.comparer);
				if (num < 0)
				{
					result = null;
				}
				else
				{
					string path = this.m_Usings[num].path;
					result = ((Panel.loadResourceFunc != null) ? (Panel.loadResourceFunc(path, typeof(VisualTreeAsset)) as VisualTreeAsset) : null);
				}
			}
			return result;
		}

		// Token: 0x0400045A RID: 1114
		[SerializeField]
		private List<VisualTreeAsset.UsingEntry> m_Usings;

		// Token: 0x0400045B RID: 1115
		[SerializeField]
		internal StyleSheet inlineSheet;

		// Token: 0x0400045C RID: 1116
		[SerializeField]
		private List<VisualElementAsset> m_VisualElementAssets;

		// Token: 0x0400045D RID: 1117
		[SerializeField]
		private List<TemplateAsset> m_TemplateAssets;

		// Token: 0x0400045E RID: 1118
		[SerializeField]
		private List<VisualTreeAsset.SlotDefinition> m_Slots;

		// Token: 0x0400045F RID: 1119
		[SerializeField]
		private int m_ContentContainerId;

		// Token: 0x0200017B RID: 379
		[Serializable]
		internal struct UsingEntry
		{
			// Token: 0x060009C8 RID: 2504 RVA: 0x00022118 File Offset: 0x00020318
			public UsingEntry(string alias, string path)
			{
				this.alias = alias;
				this.path = path;
			}

			// Token: 0x060009C9 RID: 2505 RVA: 0x00022129 File Offset: 0x00020329
			// Note: this type is marked as 'beforefieldinit'.
			static UsingEntry()
			{
			}

			// Token: 0x04000460 RID: 1120
			internal static readonly IComparer<VisualTreeAsset.UsingEntry> comparer = new VisualTreeAsset.UsingEntryComparer();

			// Token: 0x04000461 RID: 1121
			[SerializeField]
			public string alias;

			// Token: 0x04000462 RID: 1122
			[SerializeField]
			public string path;
		}

		// Token: 0x0200017C RID: 380
		private class UsingEntryComparer : IComparer<VisualTreeAsset.UsingEntry>
		{
			// Token: 0x060009CA RID: 2506 RVA: 0x00002223 File Offset: 0x00000423
			public UsingEntryComparer()
			{
			}

			// Token: 0x060009CB RID: 2507 RVA: 0x00022138 File Offset: 0x00020338
			public int Compare(VisualTreeAsset.UsingEntry x, VisualTreeAsset.UsingEntry y)
			{
				return Comparer<string>.Default.Compare(x.alias, y.alias);
			}
		}

		// Token: 0x0200017D RID: 381
		[Serializable]
		internal struct SlotDefinition
		{
			// Token: 0x04000463 RID: 1123
			[SerializeField]
			public string name;

			// Token: 0x04000464 RID: 1124
			[SerializeField]
			public int insertionPointId;
		}

		// Token: 0x0200017E RID: 382
		[Serializable]
		internal struct SlotUsageEntry
		{
			// Token: 0x060009CC RID: 2508 RVA: 0x00022165 File Offset: 0x00020365
			public SlotUsageEntry(string slotName, int assetId)
			{
				this.slotName = slotName;
				this.assetId = assetId;
			}

			// Token: 0x04000465 RID: 1125
			[SerializeField]
			public string slotName;

			// Token: 0x04000466 RID: 1126
			[SerializeField]
			public int assetId;
		}

		// Token: 0x0200017F RID: 383
		[CompilerGenerated]
		private sealed class <CloneSetupRecursively>c__AnonStorey0
		{
			// Token: 0x060009CD RID: 2509 RVA: 0x00002223 File Offset: 0x00000423
			public <CloneSetupRecursively>c__AnonStorey0()
			{
			}

			// Token: 0x060009CE RID: 2510 RVA: 0x00022178 File Offset: 0x00020378
			internal bool <>m__0(VisualTreeAsset.SlotUsageEntry u)
			{
				return u.assetId == this.childVea.id;
			}

			// Token: 0x04000467 RID: 1127
			internal VisualElementAsset childVea;
		}
	}
}
