﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000011 RID: 17
	public class Clickable : MouseManipulator
	{
		// Token: 0x0600004D RID: 77 RVA: 0x0000304E File Offset: 0x0000124E
		public Clickable(Action handler, long delay, long interval) : this(handler)
		{
			this.m_Delay = delay;
			this.m_Interval = interval;
			this.m_Active = false;
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00003070 File Offset: 0x00001270
		public Clickable(Action<EventBase> handler)
		{
			this.clickedWithEventInfo = handler;
			base.activators.Add(new ManipulatorActivationFilter
			{
				button = MouseButton.LeftMouse
			});
		}

		// Token: 0x0600004F RID: 79 RVA: 0x000030A8 File Offset: 0x000012A8
		public Clickable(Action handler)
		{
			this.clicked = handler;
			base.activators.Add(new ManipulatorActivationFilter
			{
				button = MouseButton.LeftMouse
			});
			this.m_Active = false;
		}

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x06000050 RID: 80 RVA: 0x000030E8 File Offset: 0x000012E8
		// (remove) Token: 0x06000051 RID: 81 RVA: 0x00003120 File Offset: 0x00001320
		public event Action<EventBase> clickedWithEventInfo
		{
			add
			{
				Action<EventBase> action = this.clickedWithEventInfo;
				Action<EventBase> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<EventBase>>(ref this.clickedWithEventInfo, (Action<EventBase>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<EventBase> action = this.clickedWithEventInfo;
				Action<EventBase> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<EventBase>>(ref this.clickedWithEventInfo, (Action<EventBase>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x06000052 RID: 82 RVA: 0x00003158 File Offset: 0x00001358
		// (remove) Token: 0x06000053 RID: 83 RVA: 0x00003190 File Offset: 0x00001390
		public event Action clicked
		{
			add
			{
				Action action = this.clicked;
				Action action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action>(ref this.clicked, (Action)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action action = this.clicked;
				Action action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action>(ref this.clicked, (Action)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000054 RID: 84 RVA: 0x000031C8 File Offset: 0x000013C8
		// (set) Token: 0x06000055 RID: 85 RVA: 0x000031E2 File Offset: 0x000013E2
		public Vector2 lastMousePosition
		{
			[CompilerGenerated]
			get
			{
				return this.<lastMousePosition>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<lastMousePosition>k__BackingField = value;
			}
		}

		// Token: 0x06000056 RID: 86 RVA: 0x000031EC File Offset: 0x000013EC
		private void OnTimer(TimerState timerState)
		{
			if (this.clicked != null && this.IsRepeatable())
			{
				if (base.target.ContainsPoint(this.lastMousePosition))
				{
					this.clicked();
					base.target.pseudoStates |= PseudoStates.Active;
				}
				else
				{
					base.target.pseudoStates &= ~PseudoStates.Active;
				}
			}
		}

		// Token: 0x06000057 RID: 87 RVA: 0x00003264 File Offset: 0x00001464
		private bool IsRepeatable()
		{
			return this.m_Delay > 0L || this.m_Interval > 0L;
		}

		// Token: 0x06000058 RID: 88 RVA: 0x00003294 File Offset: 0x00001494
		protected override void RegisterCallbacksOnTarget()
		{
			base.target.RegisterCallback<MouseDownEvent>(new EventCallback<MouseDownEvent>(this.OnMouseDown), TrickleDown.NoTrickleDown);
			base.target.RegisterCallback<MouseMoveEvent>(new EventCallback<MouseMoveEvent>(this.OnMouseMove), TrickleDown.NoTrickleDown);
			base.target.RegisterCallback<MouseUpEvent>(new EventCallback<MouseUpEvent>(this.OnMouseUp), TrickleDown.NoTrickleDown);
		}

		// Token: 0x06000059 RID: 89 RVA: 0x000032EC File Offset: 0x000014EC
		protected override void UnregisterCallbacksFromTarget()
		{
			base.target.UnregisterCallback<MouseDownEvent>(new EventCallback<MouseDownEvent>(this.OnMouseDown), TrickleDown.NoTrickleDown);
			base.target.UnregisterCallback<MouseMoveEvent>(new EventCallback<MouseMoveEvent>(this.OnMouseMove), TrickleDown.NoTrickleDown);
			base.target.UnregisterCallback<MouseUpEvent>(new EventCallback<MouseUpEvent>(this.OnMouseUp), TrickleDown.NoTrickleDown);
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00003344 File Offset: 0x00001544
		protected void OnMouseDown(MouseDownEvent evt)
		{
			if (base.CanStartManipulation(evt))
			{
				this.m_Active = true;
				base.target.CaptureMouse();
				this.lastMousePosition = evt.localMousePosition;
				if (this.IsRepeatable())
				{
					if (base.target.ContainsPoint(evt.localMousePosition))
					{
						if (this.clicked != null)
						{
							this.clicked();
						}
						else if (this.clickedWithEventInfo != null)
						{
							this.clickedWithEventInfo(evt);
						}
					}
					if (this.m_Repeater == null)
					{
						this.m_Repeater = base.target.schedule.Execute(new Action<TimerState>(this.OnTimer)).Every(this.m_Interval).StartingIn(this.m_Delay);
					}
					else
					{
						this.m_Repeater.ExecuteLater(this.m_Delay);
					}
				}
				base.target.pseudoStates |= PseudoStates.Active;
				evt.StopImmediatePropagation();
			}
		}

		// Token: 0x0600005B RID: 91 RVA: 0x0000344C File Offset: 0x0000164C
		protected void OnMouseMove(MouseMoveEvent evt)
		{
			if (this.m_Active)
			{
				this.lastMousePosition = evt.localMousePosition;
				if (base.target.ContainsPoint(evt.localMousePosition))
				{
					base.target.pseudoStates |= PseudoStates.Active;
				}
				else
				{
					base.target.pseudoStates &= ~PseudoStates.Active;
				}
				evt.StopPropagation();
			}
		}

		// Token: 0x0600005C RID: 92 RVA: 0x000034C0 File Offset: 0x000016C0
		protected void OnMouseUp(MouseUpEvent evt)
		{
			if (this.m_Active && base.CanStopManipulation(evt))
			{
				this.m_Active = false;
				base.target.ReleaseMouse();
				if (this.IsRepeatable())
				{
					if (this.m_Repeater != null)
					{
						this.m_Repeater.Pause();
					}
				}
				else if (base.target.ContainsPoint(evt.localMousePosition))
				{
					if (this.clicked != null)
					{
						this.clicked();
					}
					else if (this.clickedWithEventInfo != null)
					{
						this.clickedWithEventInfo(evt);
					}
				}
				base.target.pseudoStates &= ~PseudoStates.Active;
				evt.StopPropagation();
			}
		}

		// Token: 0x0400001D RID: 29
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Action<EventBase> clickedWithEventInfo;

		// Token: 0x0400001E RID: 30
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Action clicked;

		// Token: 0x0400001F RID: 31
		private readonly long m_Delay;

		// Token: 0x04000020 RID: 32
		private readonly long m_Interval;

		// Token: 0x04000021 RID: 33
		protected bool m_Active;

		// Token: 0x04000022 RID: 34
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector2 <lastMousePosition>k__BackingField;

		// Token: 0x04000023 RID: 35
		private IVisualElementScheduledItem m_Repeater;
	}
}
