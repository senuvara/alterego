﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000162 RID: 354
	public class UxmlDoubleAttributeDescription : UxmlAttributeDescription
	{
		// Token: 0x06000913 RID: 2323 RVA: 0x0001FB94 File Offset: 0x0001DD94
		public UxmlDoubleAttributeDescription()
		{
			base.type = "double";
			base.typeNamespace = "http://www.w3.org/2001/XMLSchema";
			this.defaultValue = 0.0;
		}

		// Token: 0x17000246 RID: 582
		// (get) Token: 0x06000914 RID: 2324 RVA: 0x0001FBC4 File Offset: 0x0001DDC4
		// (set) Token: 0x06000915 RID: 2325 RVA: 0x0001FBDE File Offset: 0x0001DDDE
		public double defaultValue
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultValue>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultValue>k__BackingField = value;
			}
		}

		// Token: 0x17000247 RID: 583
		// (get) Token: 0x06000916 RID: 2326 RVA: 0x0001FBE8 File Offset: 0x0001DDE8
		public override string defaultValueAsString
		{
			get
			{
				return this.defaultValue.ToString();
			}
		}

		// Token: 0x06000917 RID: 2327 RVA: 0x0001FC14 File Offset: 0x0001DE14
		[Obsolete("Pass a creation context to the method.")]
		public double GetValueFromBag(IUxmlAttributes bag)
		{
			return this.GetValueFromBag(bag, default(CreationContext));
		}

		// Token: 0x06000918 RID: 2328 RVA: 0x0001FC3C File Offset: 0x0001DE3C
		public double GetValueFromBag(IUxmlAttributes bag, CreationContext cc)
		{
			if (UxmlDoubleAttributeDescription.<>f__mg$cache0 == null)
			{
				UxmlDoubleAttributeDescription.<>f__mg$cache0 = new Func<string, double, double>(UxmlDoubleAttributeDescription.ConvertValueToDouble);
			}
			return base.GetValueFromBag<double>(bag, cc, UxmlDoubleAttributeDescription.<>f__mg$cache0, this.defaultValue);
		}

		// Token: 0x06000919 RID: 2329 RVA: 0x0001FC7C File Offset: 0x0001DE7C
		private static double ConvertValueToDouble(string v, double defaultValue)
		{
			double num;
			double result;
			if (v == null || !double.TryParse(v, out num))
			{
				result = defaultValue;
			}
			else
			{
				result = num;
			}
			return result;
		}

		// Token: 0x04000418 RID: 1048
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private double <defaultValue>k__BackingField;

		// Token: 0x04000419 RID: 1049
		[CompilerGenerated]
		private static Func<string, double, double> <>f__mg$cache0;
	}
}
