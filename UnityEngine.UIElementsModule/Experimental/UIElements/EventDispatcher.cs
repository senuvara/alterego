﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000027 RID: 39
	public sealed class EventDispatcher
	{
		// Token: 0x060000AB RID: 171 RVA: 0x00003FD4 File Offset: 0x000021D4
		private EventDispatcher()
		{
			this.m_Queue = EventDispatcher.k_EventQueuePool.Get();
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000AC RID: 172 RVA: 0x00003FF8 File Offset: 0x000021F8
		internal static EventDispatcher instance
		{
			get
			{
				if (EventDispatcher.s_EventDispatcher == null)
				{
					EventDispatcher.s_EventDispatcher = new EventDispatcher();
				}
				return EventDispatcher.s_EventDispatcher;
			}
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00004026 File Offset: 0x00002226
		internal static void ClearDispatcher()
		{
			EventDispatcher.s_EventDispatcher = null;
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000AE RID: 174 RVA: 0x00004030 File Offset: 0x00002230
		private bool dispatchImmediately
		{
			get
			{
				return this.m_GateCount == 0U;
			}
		}

		// Token: 0x060000AF RID: 175 RVA: 0x00004050 File Offset: 0x00002250
		private void DispatchEnterLeave(VisualElement previousTopElementUnderMouse, VisualElement currentTopElementUnderMouse, Func<EventBase> getEnterEventFunc, Func<EventBase> getLeaveEventFunc)
		{
			if (previousTopElementUnderMouse != currentTopElementUnderMouse)
			{
				if (previousTopElementUnderMouse != null && previousTopElementUnderMouse.panel == null)
				{
					previousTopElementUnderMouse = null;
				}
				int i = 0;
				VisualElement visualElement;
				for (visualElement = previousTopElementUnderMouse; visualElement != null; visualElement = visualElement.shadow.parent)
				{
					i++;
				}
				int j = 0;
				VisualElement visualElement2;
				for (visualElement2 = currentTopElementUnderMouse; visualElement2 != null; visualElement2 = visualElement2.shadow.parent)
				{
					j++;
				}
				visualElement = previousTopElementUnderMouse;
				visualElement2 = currentTopElementUnderMouse;
				while (i > j)
				{
					using (EventBase eventBase = getLeaveEventFunc())
					{
						eventBase.target = visualElement;
						visualElement.SendEvent(eventBase);
					}
					i--;
					visualElement = visualElement.shadow.parent;
				}
				List<VisualElement> list = VisualElementListPool.Get(j);
				while (j > i)
				{
					list.Add(visualElement2);
					j--;
					visualElement2 = visualElement2.shadow.parent;
				}
				while (visualElement != visualElement2)
				{
					using (EventBase eventBase2 = getLeaveEventFunc())
					{
						eventBase2.target = visualElement;
						visualElement.SendEvent(eventBase2);
					}
					list.Add(visualElement2);
					visualElement = visualElement.shadow.parent;
					visualElement2 = visualElement2.shadow.parent;
				}
				for (int k = list.Count - 1; k >= 0; k--)
				{
					using (EventBase eventBase3 = getEnterEventFunc())
					{
						eventBase3.target = list[k];
						list[k].SendEvent(eventBase3);
					}
				}
				VisualElementListPool.Release(list);
			}
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00004250 File Offset: 0x00002450
		private void DispatchDragEnterDragLeave(VisualElement previousTopElementUnderMouse, VisualElement currentTopElementUnderMouse, IMouseEvent triggerEvent)
		{
			if (triggerEvent != null)
			{
				this.DispatchEnterLeave(previousTopElementUnderMouse, currentTopElementUnderMouse, () => MouseEventBase<DragEnterEvent>.GetPooled(triggerEvent), () => MouseEventBase<DragLeaveEvent>.GetPooled(triggerEvent));
			}
			else
			{
				this.DispatchEnterLeave(previousTopElementUnderMouse, currentTopElementUnderMouse, () => MouseEventBase<DragEnterEvent>.GetPooled(this.m_LastMousePosition), () => MouseEventBase<DragLeaveEvent>.GetPooled(this.m_LastMousePosition));
			}
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x000042C8 File Offset: 0x000024C8
		private void DispatchMouseEnterMouseLeave(VisualElement previousTopElementUnderMouse, VisualElement currentTopElementUnderMouse, IMouseEvent triggerEvent)
		{
			if (triggerEvent != null)
			{
				this.DispatchEnterLeave(previousTopElementUnderMouse, currentTopElementUnderMouse, () => MouseEventBase<MouseEnterEvent>.GetPooled(triggerEvent), () => MouseEventBase<MouseLeaveEvent>.GetPooled(triggerEvent));
			}
			else
			{
				this.DispatchEnterLeave(previousTopElementUnderMouse, currentTopElementUnderMouse, () => MouseEventBase<MouseEnterEvent>.GetPooled(this.m_LastMousePosition), () => MouseEventBase<MouseLeaveEvent>.GetPooled(this.m_LastMousePosition));
			}
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00004340 File Offset: 0x00002540
		private void DispatchMouseOverMouseOut(VisualElement previousTopElementUnderMouse, VisualElement currentTopElementUnderMouse, IMouseEvent triggerEvent)
		{
			if (previousTopElementUnderMouse != currentTopElementUnderMouse)
			{
				if (previousTopElementUnderMouse != null && previousTopElementUnderMouse.panel != null)
				{
					using (MouseOutEvent mouseOutEvent = (triggerEvent != null) ? MouseEventBase<MouseOutEvent>.GetPooled(triggerEvent) : MouseEventBase<MouseOutEvent>.GetPooled(this.m_LastMousePosition))
					{
						mouseOutEvent.target = previousTopElementUnderMouse;
						previousTopElementUnderMouse.SendEvent(mouseOutEvent);
					}
				}
				if (currentTopElementUnderMouse != null)
				{
					using (MouseOverEvent mouseOverEvent = (triggerEvent != null) ? MouseEventBase<MouseOverEvent>.GetPooled(triggerEvent) : MouseEventBase<MouseOverEvent>.GetPooled(this.m_LastMousePosition))
					{
						mouseOverEvent.target = currentTopElementUnderMouse;
						currentTopElementUnderMouse.SendEvent(mouseOverEvent);
					}
				}
			}
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x00004410 File Offset: 0x00002610
		private void DispatchEnterLeaveEvents(VisualElement previousTopElementUnderMouse, VisualElement currentTopElementUnderMouse, EventBase triggerEvent)
		{
			IMouseEvent mouseEvent = triggerEvent as IMouseEvent;
			if (mouseEvent != null)
			{
				if (triggerEvent.GetEventTypeId() == EventBase<MouseMoveEvent>.TypeId() || triggerEvent.GetEventTypeId() == EventBase<MouseDownEvent>.TypeId() || triggerEvent.GetEventTypeId() == EventBase<MouseUpEvent>.TypeId() || triggerEvent.GetEventTypeId() == EventBase<MouseEnterWindowEvent>.TypeId() || triggerEvent.GetEventTypeId() == EventBase<WheelEvent>.TypeId())
				{
					this.DispatchMouseEnterMouseLeave(previousTopElementUnderMouse, currentTopElementUnderMouse, mouseEvent);
					this.DispatchMouseOverMouseOut(previousTopElementUnderMouse, currentTopElementUnderMouse, mouseEvent);
				}
				else if (triggerEvent.GetEventTypeId() == EventBase<DragUpdatedEvent>.TypeId())
				{
					this.DispatchDragEnterDragLeave(previousTopElementUnderMouse, currentTopElementUnderMouse, mouseEvent);
				}
			}
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x000044B8 File Offset: 0x000026B8
		internal void Dispatch(EventBase evt, IPanel panel, DispatchMode dispatchMode)
		{
			evt.MarkReceivedByDispatcher();
			if (evt.GetEventTypeId() == EventBase<IMGUIEvent>.TypeId())
			{
				Event imguiEvent = evt.imguiEvent;
				if (imguiEvent.type == EventType.Repaint)
				{
					return;
				}
			}
			if (this.dispatchImmediately || dispatchMode == DispatchMode.Immediate)
			{
				this.ProcessEvent(evt, panel);
			}
			else
			{
				evt.Acquire();
				this.m_Queue.Enqueue(new EventDispatcher.EventRecord
				{
					m_Event = evt,
					m_Panel = panel
				});
			}
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x00004544 File Offset: 0x00002744
		internal void PushDispatcherContext()
		{
			this.m_DispatchContexts.Push(new EventDispatcher.DispatchContext
			{
				m_GateCount = this.m_GateCount,
				m_Queue = this.m_Queue
			});
			this.m_GateCount = 0U;
			this.m_Queue = EventDispatcher.k_EventQueuePool.Get();
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00004598 File Offset: 0x00002798
		internal void PopDispatcherContext()
		{
			Debug.Assert(this.m_GateCount == 0U, "All gates should have been opened before popping dispatch context.");
			Debug.Assert(this.m_Queue.Count == 0, "Queue should be empty when popping dispatch context.");
			EventDispatcher.k_EventQueuePool.Release(this.m_Queue);
			this.m_GateCount = this.m_DispatchContexts.Peek().m_GateCount;
			this.m_Queue = this.m_DispatchContexts.Peek().m_Queue;
			this.m_DispatchContexts.Pop();
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x0000461F File Offset: 0x0000281F
		internal void CloseGate()
		{
			this.m_GateCount += 1U;
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00004630 File Offset: 0x00002830
		internal void OpenGate()
		{
			Debug.Assert(this.m_GateCount > 0U);
			if (this.m_GateCount > 0U)
			{
				this.m_GateCount -= 1U;
			}
			if (this.m_GateCount == 0U)
			{
				this.ProcessEventQueue();
			}
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00004670 File Offset: 0x00002870
		private void ProcessEventQueue()
		{
			Queue<EventDispatcher.EventRecord> queue = this.m_Queue;
			this.m_Queue = EventDispatcher.k_EventQueuePool.Get();
			ExitGUIException ex = null;
			try
			{
				while (queue.Count > 0)
				{
					EventDispatcher.EventRecord eventRecord = queue.Dequeue();
					EventBase @event = eventRecord.m_Event;
					IPanel panel = eventRecord.m_Panel;
					try
					{
						this.ProcessEvent(@event, panel);
					}
					catch (ExitGUIException ex2)
					{
						Debug.Assert(ex == null);
						ex = ex2;
					}
					finally
					{
						@event.Dispose();
					}
				}
			}
			finally
			{
				EventDispatcher.k_EventQueuePool.Release(queue);
			}
			if (ex != null)
			{
				throw ex;
			}
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00004738 File Offset: 0x00002938
		private void ProcessEvent(EventBase evt, IPanel panel)
		{
			using (new EventDispatcher.Gate(this))
			{
				evt.PreDispatch();
				IMouseEvent mouseEvent = evt as IMouseEvent;
				IMouseEventInternal mouseEventInternal = evt as IMouseEventInternal;
				if (mouseEvent != null && mouseEventInternal != null && mouseEventInternal.hasUnderlyingPhysicalEvent)
				{
					this.m_LastMousePositionPanel = panel;
					this.m_LastMousePosition = mouseEvent.mousePosition;
				}
				bool flag = false;
				VisualElement visualElement = MouseCaptureController.mouseCapture as VisualElement;
				if (evt.GetEventTypeId() != EventBase<MouseCaptureOutEvent>.TypeId() && visualElement != null && visualElement.panel == null)
				{
					Event imguiEvent = evt.imguiEvent;
					Debug.Log(string.Format("Capture has no panel, forcing removal (capture={0} eventType={1})", MouseCaptureController.mouseCapture, (imguiEvent == null) ? "null" : imguiEvent.type.ToString()));
					MouseCaptureController.ReleaseMouse();
				}
				bool flag2 = false;
				bool flag3 = false;
				if (MouseCaptureController.mouseCapture != null)
				{
					if (evt.imguiEvent != null && evt.target == null)
					{
						flag2 = true;
						flag3 = false;
					}
					if (mouseEvent != null && (evt.target == null || evt.target == MouseCaptureController.mouseCapture))
					{
						flag2 = true;
						flag3 = true;
					}
					if (panel != null)
					{
						if (visualElement != null && visualElement.panel.contextType != panel.contextType)
						{
							flag2 = false;
							flag3 = false;
						}
					}
					if (evt.GetEventTypeId() == EventBase<WheelEvent>.TypeId())
					{
						flag2 = false;
						flag3 = false;
					}
				}
				evt.skipElement = null;
				if (flag2)
				{
					BaseVisualElementPanel baseVisualElementPanel = panel as BaseVisualElementPanel;
					if (mouseEvent != null && baseVisualElementPanel != null)
					{
						VisualElement topElementUnderMouse = baseVisualElementPanel.topElementUnderMouse;
						if (evt.target == null)
						{
							baseVisualElementPanel.topElementUnderMouse = baseVisualElementPanel.Pick(mouseEvent.mousePosition);
						}
						this.DispatchEnterLeaveEvents(topElementUnderMouse, baseVisualElementPanel.topElementUnderMouse, evt);
					}
					IEventHandler mouseCapture = MouseCaptureController.mouseCapture;
					flag = true;
					evt.dispatch = true;
					evt.target = MouseCaptureController.mouseCapture;
					evt.currentTarget = MouseCaptureController.mouseCapture;
					evt.propagationPhase = PropagationPhase.AtTarget;
					MouseCaptureController.mouseCapture.HandleEvent(evt);
					if (!flag3)
					{
						evt.target = null;
					}
					evt.currentTarget = null;
					evt.propagationPhase = PropagationPhase.None;
					evt.dispatch = false;
					evt.skipElement = mouseCapture;
				}
				if (!flag3 && !evt.isPropagationStopped)
				{
					if (evt is IKeyboardEvent && panel != null)
					{
						flag = true;
						if (panel.focusController.focusedElement != null)
						{
							IMGUIContainer imguicontainer = panel.focusController.focusedElement as IMGUIContainer;
							if (imguicontainer != null)
							{
								if (imguicontainer != evt.skipElement && imguicontainer.HandleIMGUIEvent(evt.imguiEvent))
								{
									evt.StopPropagation();
									evt.PreventDefault();
								}
							}
							else
							{
								evt.target = panel.focusController.focusedElement;
								EventDispatcher.PropagateEvent(evt);
							}
						}
						else
						{
							evt.target = panel.visualTree;
							EventDispatcher.PropagateEvent(evt);
							if (!evt.isPropagationStopped)
							{
								EventDispatcher.PropagateToIMGUIContainer(panel.visualTree, evt);
							}
						}
					}
					else if (mouseEvent != null)
					{
						BaseVisualElementPanel baseVisualElementPanel2 = panel as BaseVisualElementPanel;
						if (baseVisualElementPanel2 != null && evt.GetEventTypeId() == EventBase<MouseLeaveWindowEvent>.TypeId())
						{
							VisualElement topElementUnderMouse2 = baseVisualElementPanel2.topElementUnderMouse;
							baseVisualElementPanel2.topElementUnderMouse = null;
							this.DispatchMouseEnterMouseLeave(topElementUnderMouse2, baseVisualElementPanel2.topElementUnderMouse, mouseEvent);
							this.DispatchMouseOverMouseOut(topElementUnderMouse2, baseVisualElementPanel2.topElementUnderMouse, mouseEvent);
						}
						else if (baseVisualElementPanel2 != null && evt.GetEventTypeId() == EventBase<DragExitedEvent>.TypeId())
						{
							VisualElement topElementUnderMouse3 = baseVisualElementPanel2.topElementUnderMouse;
							baseVisualElementPanel2.topElementUnderMouse = null;
							this.DispatchDragEnterDragLeave(topElementUnderMouse3, baseVisualElementPanel2.topElementUnderMouse, mouseEvent);
						}
						else
						{
							VisualElement previousTopElementUnderMouse = null;
							if (evt.target == null && baseVisualElementPanel2 != null)
							{
								previousTopElementUnderMouse = baseVisualElementPanel2.topElementUnderMouse;
								baseVisualElementPanel2.topElementUnderMouse = panel.Pick(mouseEvent.mousePosition);
								evt.target = baseVisualElementPanel2.topElementUnderMouse;
							}
							if (evt.target != null)
							{
								flag = true;
								EventDispatcher.PropagateEvent(evt);
							}
							if (baseVisualElementPanel2 != null)
							{
								this.DispatchEnterLeaveEvents(previousTopElementUnderMouse, baseVisualElementPanel2.topElementUnderMouse, evt);
							}
						}
					}
					else if (panel != null && evt is ICommandEvent)
					{
						IMGUIContainer imguicontainer2 = panel.focusController.focusedElement as IMGUIContainer;
						flag = true;
						if (imguicontainer2 != null)
						{
							if (imguicontainer2 != evt.skipElement && imguicontainer2.HandleIMGUIEvent(evt.imguiEvent))
							{
								evt.StopPropagation();
								evt.PreventDefault();
							}
						}
						else if (panel.focusController.focusedElement != null)
						{
							evt.target = panel.focusController.focusedElement;
							EventDispatcher.PropagateEvent(evt);
						}
						else
						{
							EventDispatcher.PropagateToIMGUIContainer(panel.visualTree, evt);
						}
					}
					else if (evt is IPropagatableEvent || evt is IFocusEvent || evt is IChangeEvent || evt.GetEventTypeId() == EventBase<InputEvent>.TypeId() || evt.GetEventTypeId() == EventBase<GeometryChangedEvent>.TypeId())
					{
						Debug.Assert(evt.target != null);
						flag = true;
						EventDispatcher.PropagateEvent(evt);
					}
				}
				if (!flag3 && !evt.isPropagationStopped && panel != null)
				{
					Event imguiEvent2 = evt.imguiEvent;
					if (!flag || (imguiEvent2 != null && imguiEvent2.type == EventType.Used) || evt.GetEventTypeId() == EventBase<MouseEnterWindowEvent>.TypeId() || evt.GetEventTypeId() == EventBase<MouseLeaveWindowEvent>.TypeId())
					{
						EventDispatcher.PropagateToIMGUIContainer(panel.visualTree, evt);
					}
				}
				if (evt.target == null && panel != null)
				{
					evt.target = panel.visualTree;
				}
				EventDispatcher.ExecuteDefaultAction(evt);
				evt.PostDispatch();
			}
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00004D20 File Offset: 0x00002F20
		internal void UpdateElementUnderMouse(BaseVisualElementPanel panel)
		{
			if (panel == this.m_LastMousePositionPanel)
			{
				Vector2 point = panel.visualTree.WorldToLocal(this.m_LastMousePosition);
				VisualElement topElementUnderMouse = panel.topElementUnderMouse;
				panel.topElementUnderMouse = panel.Pick(point);
				this.DispatchMouseEnterMouseLeave(topElementUnderMouse, panel.topElementUnderMouse, null);
				this.DispatchMouseOverMouseOut(topElementUnderMouse, panel.topElementUnderMouse, null);
			}
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00004D84 File Offset: 0x00002F84
		private static void PropagateToIMGUIContainer(VisualElement root, EventBase evt)
		{
			if (evt.imguiEvent != null)
			{
				IMGUIContainer imguicontainer = root as IMGUIContainer;
				if (imguicontainer != null && (evt.imguiEvent.type == EventType.Used || root != evt.skipElement))
				{
					if (imguicontainer.HandleIMGUIEvent(evt.imguiEvent))
					{
						evt.StopPropagation();
						evt.PreventDefault();
					}
				}
				else if (root != null)
				{
					for (int i = 0; i < root.shadow.childCount; i++)
					{
						EventDispatcher.PropagateToIMGUIContainer(root.shadow[i], evt);
						if (evt.isPropagationStopped)
						{
							break;
						}
					}
				}
			}
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00004E44 File Offset: 0x00003044
		private static void PropagateEvent(EventBase evt)
		{
			if (!evt.dispatch)
			{
				EventDispatcher.PropagationPaths.Type type = (!evt.tricklesDown) ? EventDispatcher.PropagationPaths.Type.None : EventDispatcher.PropagationPaths.Type.TrickleDown;
				type |= ((!evt.bubbles) ? EventDispatcher.PropagationPaths.Type.None : EventDispatcher.PropagationPaths.Type.BubbleUp);
				using (EventDispatcher.PropagationPaths propagationPaths = EventDispatcher.BuildPropagationPath(evt.target as VisualElement, type))
				{
					evt.dispatch = true;
					if (evt.tricklesDown && propagationPaths != null && propagationPaths.trickleDownPath.Count > 0)
					{
						evt.propagationPhase = PropagationPhase.TrickleDown;
						for (int i = propagationPaths.trickleDownPath.Count - 1; i >= 0; i--)
						{
							if (evt.isPropagationStopped)
							{
								break;
							}
							if (propagationPaths.trickleDownPath[i] != evt.skipElement)
							{
								evt.currentTarget = propagationPaths.trickleDownPath[i];
								evt.currentTarget.HandleEvent(evt);
							}
						}
					}
					if (evt.target != evt.skipElement)
					{
						evt.propagationPhase = PropagationPhase.AtTarget;
						evt.currentTarget = evt.target;
						evt.currentTarget.HandleEvent(evt);
					}
					if (evt.bubbles && propagationPaths != null && propagationPaths.bubblePath.Count > 0)
					{
						evt.propagationPhase = PropagationPhase.BubbleUp;
						foreach (VisualElement visualElement in propagationPaths.bubblePath)
						{
							if (evt.isPropagationStopped)
							{
								break;
							}
							if (visualElement != evt.skipElement)
							{
								evt.currentTarget = visualElement;
								evt.currentTarget.HandleEvent(evt);
							}
						}
					}
					evt.dispatch = false;
					evt.propagationPhase = PropagationPhase.None;
					evt.currentTarget = null;
				}
			}
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00005068 File Offset: 0x00003268
		private static void ExecuteDefaultAction(EventBase evt)
		{
			if (evt.target != null)
			{
				evt.dispatch = true;
				evt.currentTarget = evt.target;
				evt.propagationPhase = PropagationPhase.DefaultAction;
				evt.currentTarget.HandleEvent(evt);
				evt.propagationPhase = PropagationPhase.None;
				evt.currentTarget = null;
				evt.dispatch = false;
			}
		}

		// Token: 0x060000BF RID: 191 RVA: 0x000050C0 File Offset: 0x000032C0
		private static EventDispatcher.PropagationPaths BuildPropagationPath(VisualElement elem, EventDispatcher.PropagationPaths.Type pathTypesRequested)
		{
			EventDispatcher.PropagationPaths result;
			if (elem == null || pathTypesRequested == EventDispatcher.PropagationPaths.Type.None)
			{
				result = null;
			}
			else
			{
				EventDispatcher.PropagationPaths propagationPaths = EventDispatcher.PropagationPathsPool.Acquire();
				while (elem.shadow.parent != null)
				{
					if (elem.shadow.parent.enabledInHierarchy)
					{
						if ((pathTypesRequested & EventDispatcher.PropagationPaths.Type.TrickleDown) == EventDispatcher.PropagationPaths.Type.TrickleDown && elem.shadow.parent.HasTrickleDownHandlers())
						{
							propagationPaths.trickleDownPath.Add(elem.shadow.parent);
						}
						if ((pathTypesRequested & EventDispatcher.PropagationPaths.Type.BubbleUp) == EventDispatcher.PropagationPaths.Type.BubbleUp && elem.shadow.parent.HasBubbleUpHandlers())
						{
							propagationPaths.bubblePath.Add(elem.shadow.parent);
						}
					}
					elem = elem.shadow.parent;
				}
				result = propagationPaths;
			}
			return result;
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x000051AC File Offset: 0x000033AC
		// Note: this type is marked as 'beforefieldinit'.
		static EventDispatcher()
		{
		}

		// Token: 0x04000056 RID: 86
		private static readonly ObjectPool<Queue<EventDispatcher.EventRecord>> k_EventQueuePool = new ObjectPool<Queue<EventDispatcher.EventRecord>>(100);

		// Token: 0x04000057 RID: 87
		private Queue<EventDispatcher.EventRecord> m_Queue;

		// Token: 0x04000058 RID: 88
		private uint m_GateCount;

		// Token: 0x04000059 RID: 89
		private Stack<EventDispatcher.DispatchContext> m_DispatchContexts = new Stack<EventDispatcher.DispatchContext>();

		// Token: 0x0400005A RID: 90
		private static EventDispatcher s_EventDispatcher;

		// Token: 0x0400005B RID: 91
		private IPanel m_LastMousePositionPanel;

		// Token: 0x0400005C RID: 92
		private Vector2 m_LastMousePosition;

		// Token: 0x0400005D RID: 93
		private const int k_DefaultPropagationDepth = 16;

		// Token: 0x02000028 RID: 40
		public struct Gate : IDisposable
		{
			// Token: 0x060000C1 RID: 193 RVA: 0x000051BA File Offset: 0x000033BA
			public Gate(EventDispatcher d)
			{
				this.m_Dispatcher = d;
				this.m_Dispatcher.CloseGate();
			}

			// Token: 0x060000C2 RID: 194 RVA: 0x000051CF File Offset: 0x000033CF
			public void Dispose()
			{
				this.m_Dispatcher.OpenGate();
			}

			// Token: 0x0400005E RID: 94
			private EventDispatcher m_Dispatcher;
		}

		// Token: 0x02000029 RID: 41
		private struct EventRecord
		{
			// Token: 0x0400005F RID: 95
			public EventBase m_Event;

			// Token: 0x04000060 RID: 96
			public IPanel m_Panel;
		}

		// Token: 0x0200002A RID: 42
		private struct DispatchContext
		{
			// Token: 0x04000061 RID: 97
			public uint m_GateCount;

			// Token: 0x04000062 RID: 98
			public Queue<EventDispatcher.EventRecord> m_Queue;
		}

		// Token: 0x0200002B RID: 43
		private class PropagationPaths : IDisposable
		{
			// Token: 0x060000C3 RID: 195 RVA: 0x000051DD File Offset: 0x000033DD
			public PropagationPaths(int initialSize)
			{
				this.trickleDownPath = new List<VisualElement>(initialSize);
				this.bubblePath = new List<VisualElement>(initialSize);
			}

			// Token: 0x17000027 RID: 39
			// (get) Token: 0x060000C4 RID: 196 RVA: 0x00005200 File Offset: 0x00003400
			[Obsolete("Use trickleDownPath instead of capturePath.")]
			public List<VisualElement> capturePath
			{
				get
				{
					return this.trickleDownPath;
				}
			}

			// Token: 0x060000C5 RID: 197 RVA: 0x0000521B File Offset: 0x0000341B
			public void Dispose()
			{
				EventDispatcher.PropagationPathsPool.Release(this);
			}

			// Token: 0x060000C6 RID: 198 RVA: 0x00005224 File Offset: 0x00003424
			public void Clear()
			{
				this.bubblePath.Clear();
				this.trickleDownPath.Clear();
			}

			// Token: 0x04000063 RID: 99
			public readonly List<VisualElement> trickleDownPath;

			// Token: 0x04000064 RID: 100
			public readonly List<VisualElement> bubblePath;

			// Token: 0x0200002C RID: 44
			[Flags]
			public enum Type
			{
				// Token: 0x04000066 RID: 102
				None = 0,
				// Token: 0x04000067 RID: 103
				TrickleDown = 1,
				// Token: 0x04000068 RID: 104
				[Obsolete("Use TrickleDown instead of Capture.")]
				Capture = 1,
				// Token: 0x04000069 RID: 105
				BubbleUp = 2
			}
		}

		// Token: 0x0200002D RID: 45
		private static class PropagationPathsPool
		{
			// Token: 0x060000C7 RID: 199 RVA: 0x00005240 File Offset: 0x00003440
			public static EventDispatcher.PropagationPaths Acquire()
			{
				EventDispatcher.PropagationPaths result;
				if (EventDispatcher.PropagationPathsPool.s_Available.Count != 0)
				{
					EventDispatcher.PropagationPaths propagationPaths = EventDispatcher.PropagationPathsPool.s_Available[0];
					EventDispatcher.PropagationPathsPool.s_Available.RemoveAt(0);
					result = propagationPaths;
				}
				else
				{
					EventDispatcher.PropagationPaths propagationPaths2 = new EventDispatcher.PropagationPaths(16);
					result = propagationPaths2;
				}
				return result;
			}

			// Token: 0x060000C8 RID: 200 RVA: 0x0000528D File Offset: 0x0000348D
			public static void Release(EventDispatcher.PropagationPaths po)
			{
				po.Clear();
				EventDispatcher.PropagationPathsPool.s_Available.Add(po);
			}

			// Token: 0x060000C9 RID: 201 RVA: 0x000052A1 File Offset: 0x000034A1
			// Note: this type is marked as 'beforefieldinit'.
			static PropagationPathsPool()
			{
			}

			// Token: 0x0400006A RID: 106
			private static readonly List<EventDispatcher.PropagationPaths> s_Available = new List<EventDispatcher.PropagationPaths>();
		}

		// Token: 0x0200002E RID: 46
		[CompilerGenerated]
		private sealed class <DispatchDragEnterDragLeave>c__AnonStorey0
		{
			// Token: 0x060000CA RID: 202 RVA: 0x00002223 File Offset: 0x00000423
			public <DispatchDragEnterDragLeave>c__AnonStorey0()
			{
			}

			// Token: 0x060000CB RID: 203 RVA: 0x000052B0 File Offset: 0x000034B0
			internal EventBase <>m__0()
			{
				return MouseEventBase<DragEnterEvent>.GetPooled(this.triggerEvent);
			}

			// Token: 0x060000CC RID: 204 RVA: 0x000052D0 File Offset: 0x000034D0
			internal EventBase <>m__1()
			{
				return MouseEventBase<DragLeaveEvent>.GetPooled(this.triggerEvent);
			}

			// Token: 0x060000CD RID: 205 RVA: 0x000052F0 File Offset: 0x000034F0
			internal EventBase <>m__2()
			{
				return MouseEventBase<DragEnterEvent>.GetPooled(this.$this.m_LastMousePosition);
			}

			// Token: 0x060000CE RID: 206 RVA: 0x00005314 File Offset: 0x00003514
			internal EventBase <>m__3()
			{
				return MouseEventBase<DragLeaveEvent>.GetPooled(this.$this.m_LastMousePosition);
			}

			// Token: 0x0400006B RID: 107
			internal IMouseEvent triggerEvent;

			// Token: 0x0400006C RID: 108
			internal EventDispatcher $this;
		}

		// Token: 0x0200002F RID: 47
		[CompilerGenerated]
		private sealed class <DispatchMouseEnterMouseLeave>c__AnonStorey1
		{
			// Token: 0x060000CF RID: 207 RVA: 0x00002223 File Offset: 0x00000423
			public <DispatchMouseEnterMouseLeave>c__AnonStorey1()
			{
			}

			// Token: 0x060000D0 RID: 208 RVA: 0x00005338 File Offset: 0x00003538
			internal EventBase <>m__0()
			{
				return MouseEventBase<MouseEnterEvent>.GetPooled(this.triggerEvent);
			}

			// Token: 0x060000D1 RID: 209 RVA: 0x00005358 File Offset: 0x00003558
			internal EventBase <>m__1()
			{
				return MouseEventBase<MouseLeaveEvent>.GetPooled(this.triggerEvent);
			}

			// Token: 0x060000D2 RID: 210 RVA: 0x00005378 File Offset: 0x00003578
			internal EventBase <>m__2()
			{
				return MouseEventBase<MouseEnterEvent>.GetPooled(this.$this.m_LastMousePosition);
			}

			// Token: 0x060000D3 RID: 211 RVA: 0x0000539C File Offset: 0x0000359C
			internal EventBase <>m__3()
			{
				return MouseEventBase<MouseLeaveEvent>.GetPooled(this.$this.m_LastMousePosition);
			}

			// Token: 0x0400006D RID: 109
			internal IMouseEvent triggerEvent;

			// Token: 0x0400006E RID: 110
			internal EventDispatcher $this;
		}
	}
}
