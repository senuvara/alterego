﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000102 RID: 258
	public abstract class MouseCaptureEventBase<T> : EventBase<T>, IMouseCaptureEvent, IPropagatableEvent where T : MouseCaptureEventBase<T>, new()
	{
		// Token: 0x06000747 RID: 1863 RVA: 0x00019AB0 File Offset: 0x00017CB0
		protected MouseCaptureEventBase()
		{
			this.Init();
		}

		// Token: 0x170001DD RID: 477
		// (get) Token: 0x06000748 RID: 1864 RVA: 0x00019AC0 File Offset: 0x00017CC0
		// (set) Token: 0x06000749 RID: 1865 RVA: 0x00019ADA File Offset: 0x00017CDA
		public IEventHandler relatedTarget
		{
			[CompilerGenerated]
			get
			{
				return this.<relatedTarget>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<relatedTarget>k__BackingField = value;
			}
		}

		// Token: 0x0600074A RID: 1866 RVA: 0x00019AE3 File Offset: 0x00017CE3
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown);
			this.relatedTarget = null;
		}

		// Token: 0x0600074B RID: 1867 RVA: 0x00019AFC File Offset: 0x00017CFC
		public static T GetPooled(IEventHandler target, IEventHandler relatedTarget)
		{
			T pooled = EventBase<T>.GetPooled();
			pooled.target = target;
			pooled.relatedTarget = relatedTarget;
			return pooled;
		}

		// Token: 0x040002D7 RID: 727
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private IEventHandler <relatedTarget>k__BackingField;
	}
}
