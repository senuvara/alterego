﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000CD RID: 205
	public interface IUIElementDataWatch
	{
		// Token: 0x06000613 RID: 1555
		IUIElementDataWatchRequest RegisterWatch(Object toWatch, Action<Object> watchNotification);

		// Token: 0x06000614 RID: 1556
		void UnregisterWatch(IUIElementDataWatchRequest requested);
	}
}
