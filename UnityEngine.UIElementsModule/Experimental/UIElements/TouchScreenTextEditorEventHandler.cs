﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000FF RID: 255
	internal class TouchScreenTextEditorEventHandler : TextEditorEventHandler
	{
		// Token: 0x06000736 RID: 1846 RVA: 0x000194F7 File Offset: 0x000176F7
		public TouchScreenTextEditorEventHandler(TextEditorEngine editorEngine, ITextInputField textInputField) : base(editorEngine, textInputField)
		{
			this.secureText = string.Empty;
		}

		// Token: 0x170001DB RID: 475
		// (get) Token: 0x06000737 RID: 1847 RVA: 0x00019510 File Offset: 0x00017710
		// (set) Token: 0x06000738 RID: 1848 RVA: 0x0001952C File Offset: 0x0001772C
		public string secureText
		{
			get
			{
				return this.m_SecureText;
			}
			set
			{
				string text = value ?? string.Empty;
				if (text != this.m_SecureText)
				{
					this.m_SecureText = text;
				}
			}
		}

		// Token: 0x06000739 RID: 1849 RVA: 0x00019564 File Offset: 0x00017764
		public override void ExecuteDefaultActionAtTarget(EventBase evt)
		{
			base.ExecuteDefaultActionAtTarget(evt);
			long num = EventBase<MouseDownEvent>.TypeId();
			if (evt.GetEventTypeId() == num)
			{
				base.textInputField.SyncTextEngine();
				base.textInputField.UpdateText(base.editorEngine.text);
				base.textInputField.CaptureMouse();
				base.editorEngine.keyboardOnScreen = TouchScreenKeyboard.Open(string.IsNullOrEmpty(this.secureText) ? base.textInputField.text : this.secureText, TouchScreenKeyboardType.Default, true, base.editorEngine.multiline, !string.IsNullOrEmpty(this.secureText));
				base.editorEngine.UpdateScrollOffset();
				evt.StopPropagation();
			}
		}

		// Token: 0x040002D0 RID: 720
		private string m_SecureText;
	}
}
