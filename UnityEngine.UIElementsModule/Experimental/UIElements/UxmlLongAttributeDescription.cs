﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000164 RID: 356
	public class UxmlLongAttributeDescription : UxmlAttributeDescription
	{
		// Token: 0x06000921 RID: 2337 RVA: 0x0001FDBC File Offset: 0x0001DFBC
		public UxmlLongAttributeDescription()
		{
			base.type = "long";
			base.typeNamespace = "http://www.w3.org/2001/XMLSchema";
			this.defaultValue = 0L;
		}

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x06000922 RID: 2338 RVA: 0x0001FDE4 File Offset: 0x0001DFE4
		// (set) Token: 0x06000923 RID: 2339 RVA: 0x0001FDFE File Offset: 0x0001DFFE
		public long defaultValue
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultValue>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultValue>k__BackingField = value;
			}
		}

		// Token: 0x1700024B RID: 587
		// (get) Token: 0x06000924 RID: 2340 RVA: 0x0001FE08 File Offset: 0x0001E008
		public override string defaultValueAsString
		{
			get
			{
				return this.defaultValue.ToString();
			}
		}

		// Token: 0x06000925 RID: 2341 RVA: 0x0001FE34 File Offset: 0x0001E034
		[Obsolete("Pass a creation context to the method.")]
		public long GetValueFromBag(IUxmlAttributes bag)
		{
			return this.GetValueFromBag(bag, default(CreationContext));
		}

		// Token: 0x06000926 RID: 2342 RVA: 0x0001FE5C File Offset: 0x0001E05C
		public long GetValueFromBag(IUxmlAttributes bag, CreationContext cc)
		{
			if (UxmlLongAttributeDescription.<>f__mg$cache0 == null)
			{
				UxmlLongAttributeDescription.<>f__mg$cache0 = new Func<string, long, long>(UxmlLongAttributeDescription.ConvertValueToLong);
			}
			return base.GetValueFromBag<long>(bag, cc, UxmlLongAttributeDescription.<>f__mg$cache0, this.defaultValue);
		}

		// Token: 0x06000927 RID: 2343 RVA: 0x0001FE9C File Offset: 0x0001E09C
		private static long ConvertValueToLong(string v, long defaultValue)
		{
			long num;
			long result;
			if (v == null || !long.TryParse(v, out num))
			{
				result = defaultValue;
			}
			else
			{
				result = num;
			}
			return result;
		}

		// Token: 0x0400041C RID: 1052
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private long <defaultValue>k__BackingField;

		// Token: 0x0400041D RID: 1053
		[CompilerGenerated]
		private static Func<string, long, long> <>f__mg$cache0;
	}
}
