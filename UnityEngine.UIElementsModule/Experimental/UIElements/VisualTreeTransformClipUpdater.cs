﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000E2 RID: 226
	internal class VisualTreeTransformClipUpdater : BaseVisualTreeUpdater
	{
		// Token: 0x0600067A RID: 1658 RVA: 0x00016D88 File Offset: 0x00014F88
		public VisualTreeTransformClipUpdater()
		{
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x0600067B RID: 1659 RVA: 0x00016DA0 File Offset: 0x00014FA0
		public override string description
		{
			get
			{
				return "Update Transform";
			}
		}

		// Token: 0x0600067C RID: 1660 RVA: 0x00016DBC File Offset: 0x00014FBC
		public override void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType)
		{
			if ((versionChangeType & (VersionChangeType.Transform | VersionChangeType.Clip)) != (VersionChangeType)0)
			{
				if ((versionChangeType & VersionChangeType.Transform) == VersionChangeType.Transform && (!ve.isWorldTransformDirty || !ve.isWorldClipDirty))
				{
					this.DirtyTransformClipHierarchy(ve);
				}
				else if ((versionChangeType & VersionChangeType.Clip) == VersionChangeType.Clip && !ve.isWorldClipDirty)
				{
					this.DirtyClipHierarchy(ve);
				}
				this.m_Version += 1U;
			}
		}

		// Token: 0x0600067D RID: 1661 RVA: 0x00016E2C File Offset: 0x0001502C
		private void DirtyTransformClipHierarchy(VisualElement ve)
		{
			ve.isWorldTransformDirty = true;
			ve.isWorldClipDirty = true;
			int childCount = ve.shadow.childCount;
			for (int i = 0; i < childCount; i++)
			{
				VisualElement visualElement = ve.shadow[i];
				if (!visualElement.isWorldTransformDirty || !visualElement.isWorldClipDirty)
				{
					this.DirtyTransformClipHierarchy(visualElement);
				}
			}
		}

		// Token: 0x0600067E RID: 1662 RVA: 0x00016EA0 File Offset: 0x000150A0
		private void DirtyClipHierarchy(VisualElement ve)
		{
			ve.isWorldClipDirty = true;
			int childCount = ve.shadow.childCount;
			for (int i = 0; i < childCount; i++)
			{
				VisualElement visualElement = ve.shadow[i];
				if (!visualElement.isWorldClipDirty)
				{
					this.DirtyClipHierarchy(visualElement);
				}
			}
		}

		// Token: 0x0600067F RID: 1663 RVA: 0x00016F00 File Offset: 0x00015100
		public override void Update()
		{
			if (this.m_Version != this.m_LastVersion)
			{
				this.m_LastVersion = this.m_Version;
				base.panel.dispatcher.UpdateElementUnderMouse(base.panel);
			}
		}

		// Token: 0x04000293 RID: 659
		private uint m_Version = 0U;

		// Token: 0x04000294 RID: 660
		private uint m_LastVersion = 0U;
	}
}
