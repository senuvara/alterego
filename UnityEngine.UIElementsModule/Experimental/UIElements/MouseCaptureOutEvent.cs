﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000103 RID: 259
	public class MouseCaptureOutEvent : MouseCaptureEventBase<MouseCaptureOutEvent>
	{
		// Token: 0x0600074C RID: 1868 RVA: 0x00019B34 File Offset: 0x00017D34
		public MouseCaptureOutEvent()
		{
		}
	}
}
