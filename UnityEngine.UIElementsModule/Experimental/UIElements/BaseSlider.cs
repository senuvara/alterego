﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000006 RID: 6
	public abstract class BaseSlider<T> : BaseField<T> where T : IComparable<T>
	{
		// Token: 0x06000013 RID: 19 RVA: 0x000022C0 File Offset: 0x000004C0
		public BaseSlider(T start, T end, SliderDirection direction, float pageSize = 0f)
		{
			this.direction = direction;
			this.pageSize = pageSize;
			this.lowValue = start;
			this.highValue = end;
			base.Add(new VisualElement
			{
				name = "TrackElement"
			});
			this.dragElement = new VisualElement
			{
				name = "DragElement"
			};
			this.dragElement.RegisterCallback<GeometryChangedEvent>(new EventCallback<GeometryChangedEvent>(this.UpdateDragElementPosition), TrickleDown.NoTrickleDown);
			base.Add(this.dragElement);
			this.clampedDragger = new ClampedDragger<T>(this, new Action(this.SetSliderValueFromClick), new Action(this.SetSliderValueFromDrag));
			this.AddManipulator(this.clampedDragger);
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000014 RID: 20 RVA: 0x00002378 File Offset: 0x00000578
		// (set) Token: 0x06000015 RID: 21 RVA: 0x00002392 File Offset: 0x00000592
		internal VisualElement dragElement
		{
			[CompilerGenerated]
			get
			{
				return this.<dragElement>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<dragElement>k__BackingField = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000016 RID: 22 RVA: 0x0000239C File Offset: 0x0000059C
		// (set) Token: 0x06000017 RID: 23 RVA: 0x000023B7 File Offset: 0x000005B7
		public T lowValue
		{
			get
			{
				return this.m_LowValue;
			}
			set
			{
				if (!EqualityComparer<T>.Default.Equals(this.m_LowValue, value))
				{
					this.m_LowValue = value;
					this.ClampValue();
					this.UpdateDragElementPosition();
				}
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000018 RID: 24 RVA: 0x000023E8 File Offset: 0x000005E8
		// (set) Token: 0x06000019 RID: 25 RVA: 0x00002403 File Offset: 0x00000603
		public T highValue
		{
			get
			{
				return this.m_HighValue;
			}
			set
			{
				if (!EqualityComparer<T>.Default.Equals(this.m_HighValue, value))
				{
					this.m_HighValue = value;
					this.ClampValue();
					this.UpdateDragElementPosition();
				}
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600001A RID: 26 RVA: 0x00002434 File Offset: 0x00000634
		public T range
		{
			get
			{
				return this.SliderRange();
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600001B RID: 27 RVA: 0x00002450 File Offset: 0x00000650
		// (set) Token: 0x0600001C RID: 28 RVA: 0x0000246B File Offset: 0x0000066B
		public virtual float pageSize
		{
			get
			{
				return this.m_PageSize;
			}
			set
			{
				this.m_PageSize = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600001D RID: 29 RVA: 0x00002478 File Offset: 0x00000678
		// (set) Token: 0x0600001E RID: 30 RVA: 0x00002492 File Offset: 0x00000692
		internal ClampedDragger<T> clampedDragger
		{
			[CompilerGenerated]
			get
			{
				return this.<clampedDragger>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<clampedDragger>k__BackingField = value;
			}
		}

		// Token: 0x0600001F RID: 31 RVA: 0x0000249C File Offset: 0x0000069C
		private T Clamp(T value, T lowBound, T highBound)
		{
			T result = value;
			if (lowBound.CompareTo(value) > 0)
			{
				result = lowBound;
			}
			else if (highBound.CompareTo(value) < 0)
			{
				result = highBound;
			}
			return result;
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000020 RID: 32 RVA: 0x000024EC File Offset: 0x000006EC
		// (set) Token: 0x06000021 RID: 33 RVA: 0x00002508 File Offset: 0x00000708
		public override T value
		{
			get
			{
				return base.value;
			}
			set
			{
				T t = this.lowValue;
				T t2 = this.highValue;
				if (t.CompareTo(t2) > 0)
				{
					T t3 = t;
					t = t2;
					t2 = t3;
				}
				T value2 = this.Clamp(value, t, t2);
				base.value = value2;
				this.UpdateDragElementPosition();
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000022 RID: 34 RVA: 0x00002558 File Offset: 0x00000758
		// (set) Token: 0x06000023 RID: 35 RVA: 0x00002574 File Offset: 0x00000774
		public SliderDirection direction
		{
			get
			{
				return this.m_Direction;
			}
			set
			{
				this.m_Direction = value;
				if (this.m_Direction == SliderDirection.Horizontal)
				{
					base.RemoveFromClassList("vertical");
					base.AddToClassList("horizontal");
				}
				else
				{
					base.RemoveFromClassList("horizontal");
					base.AddToClassList("vertical");
				}
			}
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000025C9 File Offset: 0x000007C9
		private void ClampValue()
		{
			this.value = this.m_Value;
		}

		// Token: 0x06000025 RID: 37
		internal abstract T SliderLerpUnclamped(T a, T b, float interpolant);

		// Token: 0x06000026 RID: 38
		internal abstract float SliderNormalizeValue(T currentValue, T lowerValue, T higherValue);

		// Token: 0x06000027 RID: 39
		internal abstract T SliderRange();

		// Token: 0x06000028 RID: 40 RVA: 0x000025D8 File Offset: 0x000007D8
		private void SetSliderValueFromDrag()
		{
			if (this.clampedDragger.dragDirection == ClampedDragger<T>.DragDirection.Free)
			{
				Vector2 delta = this.clampedDragger.delta;
				if (this.direction == SliderDirection.Horizontal)
				{
					this.ComputeValueAndDirectionFromDrag(base.layout.width, this.dragElement.style.width, this.m_DragElementStartPos.x + delta.x);
				}
				else
				{
					this.ComputeValueAndDirectionFromDrag(base.layout.height, this.dragElement.style.height, this.m_DragElementStartPos.y + delta.y);
				}
			}
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002690 File Offset: 0x00000890
		private void ComputeValueAndDirectionFromDrag(float sliderLength, float dragElementLength, float dragElementPos)
		{
			float num = sliderLength - dragElementLength;
			if (Mathf.Abs(num) >= Mathf.Epsilon)
			{
				float interpolant = Mathf.Max(0f, Mathf.Min(dragElementPos, num)) / num;
				this.value = this.SliderLerpUnclamped(this.lowValue, this.highValue, interpolant);
			}
		}

		// Token: 0x0600002A RID: 42 RVA: 0x000026E4 File Offset: 0x000008E4
		private void SetSliderValueFromClick()
		{
			if (this.clampedDragger.dragDirection != ClampedDragger<T>.DragDirection.Free)
			{
				if (this.clampedDragger.dragDirection == ClampedDragger<T>.DragDirection.None)
				{
					if (Mathf.Approximately(this.pageSize, 0f))
					{
						float num = (this.direction != SliderDirection.Horizontal) ? this.dragElement.style.positionLeft.value : (this.clampedDragger.startMousePosition.x - this.dragElement.style.width / 2f);
						float num2 = (this.direction != SliderDirection.Horizontal) ? (this.clampedDragger.startMousePosition.y - this.dragElement.style.height / 2f) : this.dragElement.style.positionTop.value;
						this.dragElement.style.positionLeft = num;
						this.dragElement.style.positionTop = num2;
						this.m_DragElementStartPos = new Rect(num, num2, this.dragElement.style.width, this.dragElement.style.height);
						this.clampedDragger.dragDirection = ClampedDragger<T>.DragDirection.Free;
						if (this.direction == SliderDirection.Horizontal)
						{
							this.ComputeValueAndDirectionFromDrag(base.layout.width, this.dragElement.style.width, this.m_DragElementStartPos.x);
						}
						else
						{
							this.ComputeValueAndDirectionFromDrag(base.layout.height, this.dragElement.style.height, this.m_DragElementStartPos.y);
						}
						return;
					}
					this.m_DragElementStartPos = new Rect(this.dragElement.style.positionLeft, this.dragElement.style.positionTop, this.dragElement.style.width, this.dragElement.style.height);
				}
				if (this.direction == SliderDirection.Horizontal)
				{
					this.ComputeValueAndDirectionFromClick(base.layout.width, this.dragElement.style.width, this.dragElement.style.positionLeft, this.clampedDragger.lastMousePosition.x);
				}
				else
				{
					this.ComputeValueAndDirectionFromClick(base.layout.height, this.dragElement.style.height, this.dragElement.style.positionTop, this.clampedDragger.lastMousePosition.y);
				}
			}
		}

		// Token: 0x0600002B RID: 43 RVA: 0x000029E8 File Offset: 0x00000BE8
		internal virtual void ComputeValueAndDirectionFromClick(float sliderLength, float dragElementLength, float dragElementPos, float dragElementLastPos)
		{
			float num = sliderLength - dragElementLength;
			if (Mathf.Abs(num) >= Mathf.Epsilon)
			{
				if (dragElementLastPos < dragElementPos && this.clampedDragger.dragDirection != ClampedDragger<T>.DragDirection.LowToHigh)
				{
					this.clampedDragger.dragDirection = ClampedDragger<T>.DragDirection.HighToLow;
					float interpolant = Mathf.Max(0f, Mathf.Min(dragElementPos - this.pageSize, num)) / num;
					this.value = this.SliderLerpUnclamped(this.lowValue, this.highValue, interpolant);
				}
				else if (dragElementLastPos > dragElementPos + dragElementLength && this.clampedDragger.dragDirection != ClampedDragger<T>.DragDirection.HighToLow)
				{
					this.clampedDragger.dragDirection = ClampedDragger<T>.DragDirection.LowToHigh;
					float interpolant2 = Mathf.Max(0f, Mathf.Min(dragElementPos + this.pageSize, num)) / num;
					this.value = this.SliderLerpUnclamped(this.lowValue, this.highValue, interpolant2);
				}
			}
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00002ACC File Offset: 0x00000CCC
		public void AdjustDragElement(float factor)
		{
			bool flag = factor < 1f;
			this.dragElement.visible = flag;
			if (flag)
			{
				IStyle style = this.dragElement.style;
				this.dragElement.visible = true;
				if (this.direction == SliderDirection.Horizontal)
				{
					float specifiedValueOrDefault = style.minWidth.GetSpecifiedValueOrDefault(0f);
					style.width = Mathf.Max(base.layout.width * factor, specifiedValueOrDefault);
				}
				else
				{
					float specifiedValueOrDefault2 = style.minHeight.GetSpecifiedValueOrDefault(0f);
					style.height = Mathf.Max(base.layout.height * factor, specifiedValueOrDefault2);
				}
			}
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00002B94 File Offset: 0x00000D94
		private void UpdateDragElementPosition(GeometryChangedEvent evt)
		{
			if (!(evt.oldRect.size == evt.newRect.size))
			{
				this.UpdateDragElementPosition();
			}
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002BD4 File Offset: 0x00000DD4
		public override void OnPersistentDataReady()
		{
			base.OnPersistentDataReady();
			this.UpdateDragElementPosition();
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002BE4 File Offset: 0x00000DE4
		private void UpdateDragElementPosition()
		{
			if (base.panel != null)
			{
				float num = this.SliderNormalizeValue(this.value, this.lowValue, this.highValue);
				float num2 = this.dragElement.style.width;
				float num3 = this.dragElement.style.height;
				if (this.direction == SliderDirection.Horizontal)
				{
					float num4 = base.layout.width - num2;
					this.dragElement.style.positionLeft = num * num4;
				}
				else
				{
					float num5 = base.layout.height - num3;
					this.dragElement.style.positionTop = num * num5;
				}
			}
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002CB1 File Offset: 0x00000EB1
		protected internal override void ExecuteDefaultAction(EventBase evt)
		{
			base.ExecuteDefaultAction(evt);
			if (evt.GetEventTypeId() == EventBase<GeometryChangedEvent>.TypeId())
			{
				this.UpdateDragElementPosition((GeometryChangedEvent)evt);
			}
		}

		// Token: 0x04000008 RID: 8
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private VisualElement <dragElement>k__BackingField;

		// Token: 0x04000009 RID: 9
		private T m_LowValue;

		// Token: 0x0400000A RID: 10
		private T m_HighValue;

		// Token: 0x0400000B RID: 11
		private float m_PageSize;

		// Token: 0x0400000C RID: 12
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private ClampedDragger<T> <clampedDragger>k__BackingField;

		// Token: 0x0400000D RID: 13
		private Rect m_DragElementStartPos;

		// Token: 0x0400000E RID: 14
		private SliderDirection m_Direction;

		// Token: 0x0400000F RID: 15
		internal const float kDefaultPageSize = 0f;
	}
}
