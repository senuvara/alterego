﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000E7 RID: 231
	internal abstract class BaseVisualTreeUpdater : IVisualTreeUpdater, IDisposable
	{
		// Token: 0x06000694 RID: 1684 RVA: 0x00002223 File Offset: 0x00000423
		protected BaseVisualTreeUpdater()
		{
		}

		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06000695 RID: 1685 RVA: 0x00017158 File Offset: 0x00015358
		// (set) Token: 0x06000696 RID: 1686 RVA: 0x00017172 File Offset: 0x00015372
		public BaseVisualElementPanel panel
		{
			[CompilerGenerated]
			get
			{
				return this.<panel>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<panel>k__BackingField = value;
			}
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x06000697 RID: 1687 RVA: 0x0001717C File Offset: 0x0001537C
		public VisualElement visualTree
		{
			get
			{
				return this.panel.visualTree;
			}
		}

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x06000698 RID: 1688
		public abstract string description { get; }

		// Token: 0x06000699 RID: 1689 RVA: 0x0001719C File Offset: 0x0001539C
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600069A RID: 1690 RVA: 0x000035A6 File Offset: 0x000017A6
		protected virtual void Dispose(bool disposing)
		{
		}

		// Token: 0x0600069B RID: 1691
		public abstract void Update();

		// Token: 0x0600069C RID: 1692
		public abstract void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType);

		// Token: 0x040002A0 RID: 672
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private BaseVisualElementPanel <panel>k__BackingField;
	}
}
