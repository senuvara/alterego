﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000095 RID: 149
	internal interface IStylePainterInternal : IStylePainter
	{
		// Token: 0x060003B8 RID: 952
		void DrawRect(RectStylePainterParameters painterParams);

		// Token: 0x060003B9 RID: 953
		void DrawTexture(TextureStylePainterParameters painterParams);

		// Token: 0x060003BA RID: 954
		void DrawText(TextStylePainterParameters painterParams);

		// Token: 0x060003BB RID: 955
		void DrawMesh(MeshStylePainterParameters painterParameters);

		// Token: 0x060003BC RID: 956
		void DrawImmediate(Action callback);

		// Token: 0x060003BD RID: 957
		void DrawBackground();

		// Token: 0x060003BE RID: 958
		void DrawBorder();

		// Token: 0x060003BF RID: 959
		void DrawText(string text);

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x060003C0 RID: 960
		// (set) Token: 0x060003C1 RID: 961
		float opacity { get; set; }
	}
}
