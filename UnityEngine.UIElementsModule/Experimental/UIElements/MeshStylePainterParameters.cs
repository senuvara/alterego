﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200009A RID: 154
	internal struct MeshStylePainterParameters
	{
		// Token: 0x060003CE RID: 974 RVA: 0x0000D2CC File Offset: 0x0000B4CC
		public static MeshStylePainterParameters GetDefault(Mesh mesh, Material mat)
		{
			return new MeshStylePainterParameters
			{
				mesh = mesh,
				material = mat
			};
		}

		// Token: 0x040001C0 RID: 448
		public Mesh mesh;

		// Token: 0x040001C1 RID: 449
		public Material material;

		// Token: 0x040001C2 RID: 450
		public int pass;
	}
}
