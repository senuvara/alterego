﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200010A RID: 266
	public class ExecuteCommandEvent : CommandEventBase<ExecuteCommandEvent>
	{
		// Token: 0x0600075D RID: 1885 RVA: 0x00019CC6 File Offset: 0x00017EC6
		public ExecuteCommandEvent()
		{
		}
	}
}
