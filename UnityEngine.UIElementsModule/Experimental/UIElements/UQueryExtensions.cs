﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000B8 RID: 184
	public static class UQueryExtensions
	{
		// Token: 0x06000482 RID: 1154 RVA: 0x0000F290 File Offset: 0x0000D490
		public static T Q<T>(this VisualElement e, string name = null, params string[] classes) where T : VisualElement
		{
			return e.Query(name, classes).Build().First();
		}

		// Token: 0x06000483 RID: 1155 RVA: 0x0000F2C0 File Offset: 0x0000D4C0
		public static T Q<T>(this VisualElement e, string name = null, string className = null) where T : VisualElement
		{
			return e.Query(name, className).Build().First();
		}

		// Token: 0x06000484 RID: 1156 RVA: 0x0000F2F0 File Offset: 0x0000D4F0
		public static VisualElement Q(this VisualElement e, string name = null, params string[] classes)
		{
			return e.Query(name, classes).Build().First();
		}

		// Token: 0x06000485 RID: 1157 RVA: 0x0000F320 File Offset: 0x0000D520
		public static VisualElement Q(this VisualElement e, string name = null, string className = null)
		{
			return e.Query(name, className).Build().First();
		}

		// Token: 0x06000486 RID: 1158 RVA: 0x0000F350 File Offset: 0x0000D550
		public static UQuery.QueryBuilder<VisualElement> Query(this VisualElement e, string name = null, params string[] classes)
		{
			return e.Query(name, classes);
		}

		// Token: 0x06000487 RID: 1159 RVA: 0x0000F370 File Offset: 0x0000D570
		public static UQuery.QueryBuilder<VisualElement> Query(this VisualElement e, string name = null, string className = null)
		{
			return e.Query(name, className);
		}

		// Token: 0x06000488 RID: 1160 RVA: 0x0000F390 File Offset: 0x0000D590
		public static UQuery.QueryBuilder<T> Query<T>(this VisualElement e, string name = null, params string[] classes) where T : VisualElement
		{
			UQuery.QueryBuilder<VisualElement> queryBuilder = new UQuery.QueryBuilder<VisualElement>(e);
			return queryBuilder.OfType<T>(name, classes);
		}

		// Token: 0x06000489 RID: 1161 RVA: 0x0000F3B8 File Offset: 0x0000D5B8
		public static UQuery.QueryBuilder<T> Query<T>(this VisualElement e, string name = null, string className = null) where T : VisualElement
		{
			UQuery.QueryBuilder<VisualElement> queryBuilder = new UQuery.QueryBuilder<VisualElement>(e);
			return queryBuilder.OfType<T>(name, className);
		}

		// Token: 0x0600048A RID: 1162 RVA: 0x0000F3E0 File Offset: 0x0000D5E0
		public static UQuery.QueryBuilder<VisualElement> Query(this VisualElement e)
		{
			return new UQuery.QueryBuilder<VisualElement>(e);
		}
	}
}
