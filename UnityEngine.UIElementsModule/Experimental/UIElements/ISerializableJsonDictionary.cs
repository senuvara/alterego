﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200003E RID: 62
	internal interface ISerializableJsonDictionary
	{
		// Token: 0x06000129 RID: 297
		void Set<T>(string key, T value) where T : class;

		// Token: 0x0600012A RID: 298
		T Get<T>(string key) where T : class;

		// Token: 0x0600012B RID: 299
		T GetScriptable<T>(string key) where T : ScriptableObject;

		// Token: 0x0600012C RID: 300
		void Overwrite(object obj, string key);

		// Token: 0x0600012D RID: 301
		bool ContainsKey(string key);

		// Token: 0x0600012E RID: 302
		void OnBeforeSerialize();

		// Token: 0x0600012F RID: 303
		void OnAfterDeserialize();
	}
}
