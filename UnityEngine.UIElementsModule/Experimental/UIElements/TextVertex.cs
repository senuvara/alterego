﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000A6 RID: 166
	internal struct TextVertex
	{
		// Token: 0x040001E2 RID: 482
		public Vector3 position;

		// Token: 0x040001E3 RID: 483
		public Color32 color;

		// Token: 0x040001E4 RID: 484
		public Vector2 uv0;
	}
}
