﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000167 RID: 359
	public class UxmlEnumAttributeDescription<T> : UxmlAttributeDescription where T : struct, IConvertible
	{
		// Token: 0x06000936 RID: 2358 RVA: 0x00020108 File Offset: 0x0001E308
		public UxmlEnumAttributeDescription()
		{
			if (!typeof(T).IsEnum)
			{
				throw new ArgumentException("T must be an enumerated type");
			}
			base.type = "string";
			base.typeNamespace = "http://www.w3.org/2001/XMLSchema";
			this.defaultValue = Activator.CreateInstance<T>();
			UxmlEnumeration uxmlEnumeration = new UxmlEnumeration();
			IEnumerator enumerator = Enum.GetValues(typeof(T)).GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					T t = (T)((object)obj);
					uxmlEnumeration.values.Add(t.ToString());
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			base.restriction = uxmlEnumeration;
		}

		// Token: 0x17000250 RID: 592
		// (get) Token: 0x06000937 RID: 2359 RVA: 0x000201E0 File Offset: 0x0001E3E0
		// (set) Token: 0x06000938 RID: 2360 RVA: 0x000201FA File Offset: 0x0001E3FA
		public T defaultValue
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultValue>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultValue>k__BackingField = value;
			}
		}

		// Token: 0x17000251 RID: 593
		// (get) Token: 0x06000939 RID: 2361 RVA: 0x00020204 File Offset: 0x0001E404
		public override string defaultValueAsString
		{
			get
			{
				T defaultValue = this.defaultValue;
				return defaultValue.ToString();
			}
		}

		// Token: 0x0600093A RID: 2362 RVA: 0x00020230 File Offset: 0x0001E430
		[Obsolete("Pass a creation context to the method.")]
		public T GetValueFromBag(IUxmlAttributes bag)
		{
			return this.GetValueFromBag(bag, default(CreationContext));
		}

		// Token: 0x0600093B RID: 2363 RVA: 0x00020258 File Offset: 0x0001E458
		public T GetValueFromBag(IUxmlAttributes bag, CreationContext cc)
		{
			if (UxmlEnumAttributeDescription<T>.<>f__mg$cache0 == null)
			{
				UxmlEnumAttributeDescription<T>.<>f__mg$cache0 = new Func<string, T, T>(UxmlEnumAttributeDescription<T>.ConvertValueToEnum<T>);
			}
			return base.GetValueFromBag<T>(bag, cc, UxmlEnumAttributeDescription<T>.<>f__mg$cache0, this.defaultValue);
		}

		// Token: 0x0600093C RID: 2364 RVA: 0x00020298 File Offset: 0x0001E498
		private static U ConvertValueToEnum<U>(string v, U defaultValue)
		{
			U result;
			if (v == null || !Enum.IsDefined(typeof(U), v))
			{
				result = defaultValue;
			}
			else
			{
				U u = (U)((object)Enum.Parse(typeof(U), v));
				result = u;
			}
			return result;
		}

		// Token: 0x04000422 RID: 1058
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private T <defaultValue>k__BackingField;

		// Token: 0x04000423 RID: 1059
		[CompilerGenerated]
		private static Func<string, T, T> <>f__mg$cache0;
	}
}
