﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000E4 RID: 228
	internal sealed class VisualTreeUpdater : IDisposable
	{
		// Token: 0x06000680 RID: 1664 RVA: 0x00016F3B File Offset: 0x0001513B
		public VisualTreeUpdater(BaseVisualElementPanel panel)
		{
			this.m_Panel = panel;
			this.m_UpdaterArray = new VisualTreeUpdater.UpdaterArray();
			this.SetDefaultUpdaters();
		}

		// Token: 0x06000681 RID: 1665 RVA: 0x00016F5C File Offset: 0x0001515C
		public void Dispose()
		{
			for (int i = 0; i < 6; i++)
			{
				IVisualTreeUpdater visualTreeUpdater = this.m_UpdaterArray[i];
				visualTreeUpdater.Dispose();
			}
		}

		// Token: 0x06000682 RID: 1666 RVA: 0x00016F94 File Offset: 0x00015194
		public void UpdateVisualTree()
		{
			for (int i = 0; i < 6; i++)
			{
				IVisualTreeUpdater visualTreeUpdater = this.m_UpdaterArray[i];
				visualTreeUpdater.Update();
			}
		}

		// Token: 0x06000683 RID: 1667 RVA: 0x00016FCC File Offset: 0x000151CC
		public void UpdateVisualTreePhase(VisualTreeUpdatePhase phase)
		{
			IVisualTreeUpdater visualTreeUpdater = this.m_UpdaterArray[phase];
			visualTreeUpdater.Update();
		}

		// Token: 0x06000684 RID: 1668 RVA: 0x00016FF0 File Offset: 0x000151F0
		public void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType)
		{
			for (int i = 0; i < 6; i++)
			{
				IVisualTreeUpdater visualTreeUpdater = this.m_UpdaterArray[i];
				visualTreeUpdater.OnVersionChanged(ve, versionChangeType);
			}
		}

		// Token: 0x06000685 RID: 1669 RVA: 0x00017028 File Offset: 0x00015228
		public void DirtyStyleSheets()
		{
			VisualTreeStyleUpdater visualTreeStyleUpdater = this.m_UpdaterArray[VisualTreeUpdatePhase.Styles] as VisualTreeStyleUpdater;
			visualTreeStyleUpdater.DirtyStyleSheets();
		}

		// Token: 0x06000686 RID: 1670 RVA: 0x0001704E File Offset: 0x0001524E
		public void SetUpdater(IVisualTreeUpdater updater, VisualTreeUpdatePhase phase)
		{
			updater.panel = this.m_Panel;
			this.m_UpdaterArray[phase] = updater;
		}

		// Token: 0x06000687 RID: 1671 RVA: 0x0001706C File Offset: 0x0001526C
		public void SetUpdater<T>(VisualTreeUpdatePhase phase) where T : IVisualTreeUpdater, new()
		{
			T t = Activator.CreateInstance<T>();
			t.panel = this.m_Panel;
			T t2 = t;
			this.m_UpdaterArray[phase] = t2;
		}

		// Token: 0x06000688 RID: 1672 RVA: 0x000170A8 File Offset: 0x000152A8
		public IVisualTreeUpdater GetUpdater(VisualTreeUpdatePhase phase)
		{
			return this.m_UpdaterArray[phase];
		}

		// Token: 0x06000689 RID: 1673 RVA: 0x000170C9 File Offset: 0x000152C9
		private void SetDefaultUpdaters()
		{
			this.SetUpdater<VisualTreePersistentDataUpdater>(VisualTreeUpdatePhase.PersistentData);
			this.SetUpdater<VisualTreeBindingsUpdater>(VisualTreeUpdatePhase.Bindings);
			this.SetUpdater<VisualTreeStyleUpdater>(VisualTreeUpdatePhase.Styles);
			this.SetUpdater<VisualTreeLayoutUpdater>(VisualTreeUpdatePhase.Layout);
			this.SetUpdater<VisualTreeTransformClipUpdater>(VisualTreeUpdatePhase.TransformClip);
			this.SetUpdater<VisualTreeRepaintUpdater>(VisualTreeUpdatePhase.Repaint);
		}

		// Token: 0x0400029D RID: 669
		private BaseVisualElementPanel m_Panel;

		// Token: 0x0400029E RID: 670
		private VisualTreeUpdater.UpdaterArray m_UpdaterArray;

		// Token: 0x020000E5 RID: 229
		private class UpdaterArray
		{
			// Token: 0x0600068A RID: 1674 RVA: 0x000170F6 File Offset: 0x000152F6
			public UpdaterArray()
			{
				this.m_VisualTreeUpdaters = new IVisualTreeUpdater[6];
			}

			// Token: 0x170001B3 RID: 435
			public IVisualTreeUpdater this[VisualTreeUpdatePhase phase]
			{
				get
				{
					return this.m_VisualTreeUpdaters[(int)phase];
				}
				set
				{
					this.m_VisualTreeUpdaters[(int)phase] = value;
				}
			}

			// Token: 0x170001B4 RID: 436
			public IVisualTreeUpdater this[int index]
			{
				get
				{
					return this.m_VisualTreeUpdaters[index];
				}
				set
				{
					this.m_VisualTreeUpdaters[index] = value;
				}
			}

			// Token: 0x0400029F RID: 671
			private IVisualTreeUpdater[] m_VisualTreeUpdaters;
		}
	}
}
