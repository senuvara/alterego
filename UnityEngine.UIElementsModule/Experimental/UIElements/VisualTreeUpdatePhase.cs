﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000E3 RID: 227
	internal enum VisualTreeUpdatePhase
	{
		// Token: 0x04000296 RID: 662
		PersistentData,
		// Token: 0x04000297 RID: 663
		Bindings,
		// Token: 0x04000298 RID: 664
		Styles,
		// Token: 0x04000299 RID: 665
		Layout,
		// Token: 0x0400029A RID: 666
		TransformClip,
		// Token: 0x0400029B RID: 667
		Repaint,
		// Token: 0x0400029C RID: 668
		Count
	}
}
