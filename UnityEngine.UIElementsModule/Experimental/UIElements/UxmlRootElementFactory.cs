﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000172 RID: 370
	public class UxmlRootElementFactory : UxmlFactory<VisualElement, UxmlRootElementTraits>
	{
		// Token: 0x0600098F RID: 2447 RVA: 0x00021114 File Offset: 0x0001F314
		public UxmlRootElementFactory()
		{
		}

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06000990 RID: 2448 RVA: 0x0002111C File Offset: 0x0001F31C
		public override string uxmlName
		{
			get
			{
				return "UXML";
			}
		}

		// Token: 0x17000276 RID: 630
		// (get) Token: 0x06000991 RID: 2449 RVA: 0x00021138 File Offset: 0x0001F338
		public override string uxmlQualifiedName
		{
			get
			{
				return this.uxmlNamespace + "." + this.uxmlName;
			}
		}

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x06000992 RID: 2450 RVA: 0x00021164 File Offset: 0x0001F364
		public override string substituteForTypeName
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x17000278 RID: 632
		// (get) Token: 0x06000993 RID: 2451 RVA: 0x00021180 File Offset: 0x0001F380
		public override string substituteForTypeNamespace
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x17000279 RID: 633
		// (get) Token: 0x06000994 RID: 2452 RVA: 0x0002119C File Offset: 0x0001F39C
		public override string substituteForTypeQualifiedName
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x06000995 RID: 2453 RVA: 0x000211B8 File Offset: 0x0001F3B8
		public override VisualElement Create(IUxmlAttributes bag, CreationContext cc)
		{
			return null;
		}
	}
}
