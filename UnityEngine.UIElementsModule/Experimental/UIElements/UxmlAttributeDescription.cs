﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200015E RID: 350
	public abstract class UxmlAttributeDescription
	{
		// Token: 0x060008F5 RID: 2293 RVA: 0x0001F758 File Offset: 0x0001D958
		protected UxmlAttributeDescription()
		{
			this.use = UxmlAttributeDescription.Use.Optional;
			this.restriction = null;
		}

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x060008F6 RID: 2294 RVA: 0x0001F770 File Offset: 0x0001D970
		// (set) Token: 0x060008F7 RID: 2295 RVA: 0x0001F78A File Offset: 0x0001D98A
		public string name
		{
			[CompilerGenerated]
			get
			{
				return this.<name>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<name>k__BackingField = value;
			}
		}

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x060008F8 RID: 2296 RVA: 0x0001F794 File Offset: 0x0001D994
		// (set) Token: 0x060008F9 RID: 2297 RVA: 0x0001F7AE File Offset: 0x0001D9AE
		public string[] obsoleteNames
		{
			[CompilerGenerated]
			get
			{
				return this.<obsoleteNames>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<obsoleteNames>k__BackingField = value;
			}
		}

		// Token: 0x1700023D RID: 573
		// (get) Token: 0x060008FA RID: 2298 RVA: 0x0001F7B8 File Offset: 0x0001D9B8
		// (set) Token: 0x060008FB RID: 2299 RVA: 0x0001F7D2 File Offset: 0x0001D9D2
		public string type
		{
			[CompilerGenerated]
			get
			{
				return this.<type>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<type>k__BackingField = value;
			}
		}

		// Token: 0x1700023E RID: 574
		// (get) Token: 0x060008FC RID: 2300 RVA: 0x0001F7DC File Offset: 0x0001D9DC
		// (set) Token: 0x060008FD RID: 2301 RVA: 0x0001F7F6 File Offset: 0x0001D9F6
		public string typeNamespace
		{
			[CompilerGenerated]
			get
			{
				return this.<typeNamespace>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<typeNamespace>k__BackingField = value;
			}
		}

		// Token: 0x1700023F RID: 575
		// (get) Token: 0x060008FE RID: 2302
		public abstract string defaultValueAsString { get; }

		// Token: 0x17000240 RID: 576
		// (get) Token: 0x060008FF RID: 2303 RVA: 0x0001F800 File Offset: 0x0001DA00
		// (set) Token: 0x06000900 RID: 2304 RVA: 0x0001F81A File Offset: 0x0001DA1A
		public UxmlAttributeDescription.Use use
		{
			[CompilerGenerated]
			get
			{
				return this.<use>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<use>k__BackingField = value;
			}
		}

		// Token: 0x17000241 RID: 577
		// (get) Token: 0x06000901 RID: 2305 RVA: 0x0001F824 File Offset: 0x0001DA24
		// (set) Token: 0x06000902 RID: 2306 RVA: 0x0001F83E File Offset: 0x0001DA3E
		public UxmlTypeRestriction restriction
		{
			[CompilerGenerated]
			get
			{
				return this.<restriction>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<restriction>k__BackingField = value;
			}
		}

		// Token: 0x06000903 RID: 2307 RVA: 0x0001F848 File Offset: 0x0001DA48
		internal bool TryGetValueFromBagAsString(IUxmlAttributes bag, CreationContext cc, out string value)
		{
			bool result;
			if (this.name == null)
			{
				if (this.obsoleteNames == null || this.obsoleteNames.Length == 0)
				{
					Debug.LogError("Attribute description has no name.");
					value = null;
					result = false;
				}
				else
				{
					for (int i = 0; i < this.obsoleteNames.Length; i++)
					{
						if (bag.TryGetAttributeValue(this.obsoleteNames[i], out value))
						{
							if (cc.visualTreeAsset != null)
							{
							}
							return true;
						}
					}
					value = null;
					result = false;
				}
			}
			else if (!bag.TryGetAttributeValue(this.name, out value))
			{
				if (this.obsoleteNames != null)
				{
					for (int j = 0; j < this.obsoleteNames.Length; j++)
					{
						if (bag.TryGetAttributeValue(this.obsoleteNames[j], out value))
						{
							if (cc.visualTreeAsset != null)
							{
							}
							return true;
						}
					}
				}
				value = null;
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06000904 RID: 2308 RVA: 0x0001F964 File Offset: 0x0001DB64
		protected T GetValueFromBag<T>(IUxmlAttributes bag, CreationContext cc, Func<string, T, T> converterFunc, T defaultValue)
		{
			string arg;
			T result;
			if (this.TryGetValueFromBagAsString(bag, cc, out arg))
			{
				result = converterFunc(arg, defaultValue);
			}
			else
			{
				result = defaultValue;
			}
			return result;
		}

		// Token: 0x04000408 RID: 1032
		protected const string k_XmlSchemaNamespace = "http://www.w3.org/2001/XMLSchema";

		// Token: 0x04000409 RID: 1033
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <name>k__BackingField;

		// Token: 0x0400040A RID: 1034
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string[] <obsoleteNames>k__BackingField;

		// Token: 0x0400040B RID: 1035
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <type>k__BackingField;

		// Token: 0x0400040C RID: 1036
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <typeNamespace>k__BackingField;

		// Token: 0x0400040D RID: 1037
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private UxmlAttributeDescription.Use <use>k__BackingField;

		// Token: 0x0400040E RID: 1038
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private UxmlTypeRestriction <restriction>k__BackingField;

		// Token: 0x0200015F RID: 351
		public enum Use
		{
			// Token: 0x04000410 RID: 1040
			None,
			// Token: 0x04000411 RID: 1041
			Optional,
			// Token: 0x04000412 RID: 1042
			Prohibited,
			// Token: 0x04000413 RID: 1043
			Required
		}
	}
}
