﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000D2 RID: 210
	public interface IVisualElementScheduledItem
	{
		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x06000624 RID: 1572
		VisualElement element { get; }

		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x06000625 RID: 1573
		bool isActive { get; }

		// Token: 0x06000626 RID: 1574
		void Resume();

		// Token: 0x06000627 RID: 1575
		void Pause();

		// Token: 0x06000628 RID: 1576
		void ExecuteLater(long delayMs);

		// Token: 0x06000629 RID: 1577
		IVisualElementScheduledItem StartingIn(long delayMs);

		// Token: 0x0600062A RID: 1578
		IVisualElementScheduledItem Every(long intervalMs);

		// Token: 0x0600062B RID: 1579
		IVisualElementScheduledItem Until(Func<bool> stopCondition);

		// Token: 0x0600062C RID: 1580
		IVisualElementScheduledItem ForDuration(long durationMs);
	}
}
