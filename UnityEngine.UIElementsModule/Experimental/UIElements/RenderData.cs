﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000065 RID: 101
	internal class RenderData
	{
		// Token: 0x060002E3 RID: 739 RVA: 0x00002223 File Offset: 0x00000423
		public RenderData()
		{
		}

		// Token: 0x0400010F RID: 271
		public RenderTexture pixelCache;

		// Token: 0x04000110 RID: 272
		public Rect lastLayout;
	}
}
