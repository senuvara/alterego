﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000CF RID: 207
	public class VisualElementFocusRing : IFocusRing
	{
		// Token: 0x0600061A RID: 1562 RVA: 0x00014BE0 File Offset: 0x00012DE0
		public VisualElementFocusRing(VisualElement root, VisualElementFocusRing.DefaultFocusOrder dfo = VisualElementFocusRing.DefaultFocusOrder.ChildOrder)
		{
			this.defaultFocusOrder = dfo;
			this.root = root;
			this.m_FocusRing = new List<VisualElementFocusRing.FocusRingRecord>();
		}

		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x0600061B RID: 1563 RVA: 0x00014C04 File Offset: 0x00012E04
		// (set) Token: 0x0600061C RID: 1564 RVA: 0x00014C1E File Offset: 0x00012E1E
		public VisualElementFocusRing.DefaultFocusOrder defaultFocusOrder
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultFocusOrder>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultFocusOrder>k__BackingField = value;
			}
		}

		// Token: 0x0600061D RID: 1565 RVA: 0x00014C28 File Offset: 0x00012E28
		private int FocusRingAutoIndexSort(VisualElementFocusRing.FocusRingRecord a, VisualElementFocusRing.FocusRingRecord b)
		{
			int result;
			switch (this.defaultFocusOrder)
			{
			default:
				result = Comparer<int>.Default.Compare(a.m_AutoIndex, b.m_AutoIndex);
				break;
			case VisualElementFocusRing.DefaultFocusOrder.PositionXY:
			{
				VisualElement visualElement = a.m_Focusable as VisualElement;
				VisualElement visualElement2 = b.m_Focusable as VisualElement;
				if (visualElement != null && visualElement2 != null)
				{
					if (visualElement.layout.position.x < visualElement2.layout.position.x)
					{
						result = -1;
						break;
					}
					if (visualElement.layout.position.x > visualElement2.layout.position.x)
					{
						result = 1;
						break;
					}
					if (visualElement.layout.position.y < visualElement2.layout.position.y)
					{
						result = -1;
						break;
					}
					if (visualElement.layout.position.y > visualElement2.layout.position.y)
					{
						result = 1;
						break;
					}
				}
				result = Comparer<int>.Default.Compare(a.m_AutoIndex, b.m_AutoIndex);
				break;
			}
			case VisualElementFocusRing.DefaultFocusOrder.PositionYX:
			{
				VisualElement visualElement3 = a.m_Focusable as VisualElement;
				VisualElement visualElement4 = b.m_Focusable as VisualElement;
				if (visualElement3 != null && visualElement4 != null)
				{
					if (visualElement3.layout.position.y < visualElement4.layout.position.y)
					{
						result = -1;
						break;
					}
					if (visualElement3.layout.position.y > visualElement4.layout.position.y)
					{
						result = 1;
						break;
					}
					if (visualElement3.layout.position.x < visualElement4.layout.position.x)
					{
						result = -1;
						break;
					}
					if (visualElement3.layout.position.x > visualElement4.layout.position.x)
					{
						result = 1;
						break;
					}
				}
				result = Comparer<int>.Default.Compare(a.m_AutoIndex, b.m_AutoIndex);
				break;
			}
			}
			return result;
		}

		// Token: 0x0600061E RID: 1566 RVA: 0x00014EFC File Offset: 0x000130FC
		private int FocusRingSort(VisualElementFocusRing.FocusRingRecord a, VisualElementFocusRing.FocusRingRecord b)
		{
			int result;
			if (a.m_Focusable.focusIndex == 0 && b.m_Focusable.focusIndex == 0)
			{
				result = this.FocusRingAutoIndexSort(a, b);
			}
			else if (a.m_Focusable.focusIndex == 0)
			{
				result = 1;
			}
			else if (b.m_Focusable.focusIndex == 0)
			{
				result = -1;
			}
			else
			{
				int num = Comparer<int>.Default.Compare(a.m_Focusable.focusIndex, b.m_Focusable.focusIndex);
				if (num == 0)
				{
					num = this.FocusRingAutoIndexSort(a, b);
				}
				result = num;
			}
			return result;
		}

		// Token: 0x0600061F RID: 1567 RVA: 0x00014FAC File Offset: 0x000131AC
		private void DoUpdate()
		{
			this.m_FocusRing.Clear();
			if (this.root != null)
			{
				int num = 0;
				this.BuildRingRecursive(this.root, ref num);
				this.m_FocusRing.Sort(new Comparison<VisualElementFocusRing.FocusRingRecord>(this.FocusRingSort));
			}
		}

		// Token: 0x06000620 RID: 1568 RVA: 0x00014FFC File Offset: 0x000131FC
		private void BuildRingRecursive(VisualElement vc, ref int focusIndex)
		{
			for (int i = 0; i < vc.shadow.childCount; i++)
			{
				VisualElement visualElement = vc.shadow[i];
				if (visualElement.canGrabFocus)
				{
					this.m_FocusRing.Add(new VisualElementFocusRing.FocusRingRecord
					{
						m_AutoIndex = focusIndex++,
						m_Focusable = visualElement
					});
				}
				this.BuildRingRecursive(visualElement, ref focusIndex);
			}
		}

		// Token: 0x06000621 RID: 1569 RVA: 0x00015080 File Offset: 0x00013280
		private int GetFocusableInternalIndex(Focusable f)
		{
			if (f != null)
			{
				for (int i = 0; i < this.m_FocusRing.Count; i++)
				{
					if (f == this.m_FocusRing[i].m_Focusable)
					{
						return i;
					}
				}
			}
			return -1;
		}

		// Token: 0x06000622 RID: 1570 RVA: 0x000150E0 File Offset: 0x000132E0
		public FocusChangeDirection GetFocusChangeDirection(Focusable currentFocusable, EventBase e)
		{
			FocusChangeDirection none;
			if (currentFocusable is IMGUIContainer && e.imguiEvent != null)
			{
				none = FocusChangeDirection.none;
			}
			else
			{
				if (e.GetEventTypeId() == EventBase<KeyDownEvent>.TypeId())
				{
					KeyDownEvent keyDownEvent = e as KeyDownEvent;
					EventModifiers modifiers = keyDownEvent.modifiers;
					if (keyDownEvent.character == '\t')
					{
						if (currentFocusable == null)
						{
							return FocusChangeDirection.none;
						}
						if ((modifiers & EventModifiers.Shift) == EventModifiers.None)
						{
							return VisualElementFocusChangeDirection.right;
						}
						return VisualElementFocusChangeDirection.left;
					}
				}
				none = FocusChangeDirection.none;
			}
			return none;
		}

		// Token: 0x06000623 RID: 1571 RVA: 0x0001517C File Offset: 0x0001337C
		public Focusable GetNextFocusable(Focusable currentFocusable, FocusChangeDirection direction)
		{
			Focusable result;
			if (direction == FocusChangeDirection.none || direction == FocusChangeDirection.unspecified)
			{
				result = currentFocusable;
			}
			else
			{
				this.DoUpdate();
				if (this.m_FocusRing.Count == 0)
				{
					result = null;
				}
				else
				{
					int num = 0;
					if (direction == VisualElementFocusChangeDirection.right)
					{
						num = this.GetFocusableInternalIndex(currentFocusable) + 1;
						if (num == this.m_FocusRing.Count)
						{
							num = 0;
						}
					}
					else if (direction == VisualElementFocusChangeDirection.left)
					{
						num = this.GetFocusableInternalIndex(currentFocusable) - 1;
						if (num == -1)
						{
							num = this.m_FocusRing.Count - 1;
						}
					}
					result = this.m_FocusRing[num].m_Focusable;
				}
			}
			return result;
		}

		// Token: 0x0400025E RID: 606
		private VisualElement root;

		// Token: 0x0400025F RID: 607
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VisualElementFocusRing.DefaultFocusOrder <defaultFocusOrder>k__BackingField;

		// Token: 0x04000260 RID: 608
		private List<VisualElementFocusRing.FocusRingRecord> m_FocusRing;

		// Token: 0x020000D0 RID: 208
		public enum DefaultFocusOrder
		{
			// Token: 0x04000262 RID: 610
			ChildOrder,
			// Token: 0x04000263 RID: 611
			PositionXY,
			// Token: 0x04000264 RID: 612
			PositionYX
		}

		// Token: 0x020000D1 RID: 209
		private struct FocusRingRecord
		{
			// Token: 0x04000265 RID: 613
			public int m_AutoIndex;

			// Token: 0x04000266 RID: 614
			public Focusable m_Focusable;
		}
	}
}
