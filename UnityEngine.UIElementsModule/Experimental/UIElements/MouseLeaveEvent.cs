﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200013A RID: 314
	public class MouseLeaveEvent : MouseEventBase<MouseLeaveEvent>
	{
		// Token: 0x06000864 RID: 2148 RVA: 0x0001BBB0 File Offset: 0x00019DB0
		public MouseLeaveEvent()
		{
			this.Init();
		}

		// Token: 0x06000865 RID: 2149 RVA: 0x0001BBBF File Offset: 0x00019DBF
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.TricklesDown | EventBase.EventFlags.Cancellable);
		}
	}
}
