﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000180 RID: 384
	public struct CreationContext
	{
		// Token: 0x060009CF RID: 2511 RVA: 0x000221A0 File Offset: 0x000203A0
		internal CreationContext(Dictionary<string, VisualElement> slotInsertionPoints, VisualTreeAsset vta, VisualElement target)
		{
			this.target = target;
			this.slotInsertionPoints = slotInsertionPoints;
			this.visualTreeAsset = vta;
		}

		// Token: 0x17000286 RID: 646
		// (get) Token: 0x060009D0 RID: 2512 RVA: 0x000221B8 File Offset: 0x000203B8
		// (set) Token: 0x060009D1 RID: 2513 RVA: 0x000221D2 File Offset: 0x000203D2
		public VisualElement target
		{
			[CompilerGenerated]
			get
			{
				return this.<target>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<target>k__BackingField = value;
			}
		}

		// Token: 0x17000287 RID: 647
		// (get) Token: 0x060009D2 RID: 2514 RVA: 0x000221DC File Offset: 0x000203DC
		// (set) Token: 0x060009D3 RID: 2515 RVA: 0x000221F6 File Offset: 0x000203F6
		public VisualTreeAsset visualTreeAsset
		{
			[CompilerGenerated]
			get
			{
				return this.<visualTreeAsset>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<visualTreeAsset>k__BackingField = value;
			}
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x060009D4 RID: 2516 RVA: 0x00022200 File Offset: 0x00020400
		// (set) Token: 0x060009D5 RID: 2517 RVA: 0x0002221A File Offset: 0x0002041A
		public Dictionary<string, VisualElement> slotInsertionPoints
		{
			[CompilerGenerated]
			get
			{
				return this.<slotInsertionPoints>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<slotInsertionPoints>k__BackingField = value;
			}
		}

		// Token: 0x060009D6 RID: 2518 RVA: 0x00022224 File Offset: 0x00020424
		// Note: this type is marked as 'beforefieldinit'.
		static CreationContext()
		{
		}

		// Token: 0x04000468 RID: 1128
		public static readonly CreationContext Default = default(CreationContext);

		// Token: 0x04000469 RID: 1129
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private VisualElement <target>k__BackingField;

		// Token: 0x0400046A RID: 1130
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private VisualTreeAsset <visualTreeAsset>k__BackingField;

		// Token: 0x0400046B RID: 1131
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Dictionary<string, VisualElement> <slotInsertionPoints>k__BackingField;
	}
}
