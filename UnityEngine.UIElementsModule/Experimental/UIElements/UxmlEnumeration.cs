﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000177 RID: 375
	public class UxmlEnumeration : UxmlTypeRestriction
	{
		// Token: 0x0600099E RID: 2462 RVA: 0x000212E8 File Offset: 0x0001F4E8
		public UxmlEnumeration()
		{
		}

		// Token: 0x0600099F RID: 2463 RVA: 0x000212FC File Offset: 0x0001F4FC
		public override bool Equals(UxmlTypeRestriction other)
		{
			UxmlEnumeration uxmlEnumeration = other as UxmlEnumeration;
			return uxmlEnumeration != null && this.values.All(new Func<string, bool>(uxmlEnumeration.values.Contains)) && this.values.Count == uxmlEnumeration.values.Count;
		}

		// Token: 0x0400044E RID: 1102
		public List<string> values = new List<string>();
	}
}
