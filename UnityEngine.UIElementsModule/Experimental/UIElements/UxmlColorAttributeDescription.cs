﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000166 RID: 358
	public class UxmlColorAttributeDescription : UxmlAttributeDescription
	{
		// Token: 0x0600092F RID: 2351 RVA: 0x0001FFE0 File Offset: 0x0001E1E0
		public UxmlColorAttributeDescription()
		{
			base.type = "string";
			base.typeNamespace = "http://www.w3.org/2001/XMLSchema";
			this.defaultValue = new Color(0f, 0f, 0f, 1f);
		}

		// Token: 0x1700024E RID: 590
		// (get) Token: 0x06000930 RID: 2352 RVA: 0x00020020 File Offset: 0x0001E220
		// (set) Token: 0x06000931 RID: 2353 RVA: 0x0002003A File Offset: 0x0001E23A
		public Color defaultValue
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultValue>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultValue>k__BackingField = value;
			}
		}

		// Token: 0x1700024F RID: 591
		// (get) Token: 0x06000932 RID: 2354 RVA: 0x00020044 File Offset: 0x0001E244
		public override string defaultValueAsString
		{
			get
			{
				return this.defaultValue.ToString();
			}
		}

		// Token: 0x06000933 RID: 2355 RVA: 0x00020070 File Offset: 0x0001E270
		[Obsolete("Pass a creation context to the method.")]
		public Color GetValueFromBag(IUxmlAttributes bag)
		{
			return this.GetValueFromBag(bag, default(CreationContext));
		}

		// Token: 0x06000934 RID: 2356 RVA: 0x00020098 File Offset: 0x0001E298
		public Color GetValueFromBag(IUxmlAttributes bag, CreationContext cc)
		{
			if (UxmlColorAttributeDescription.<>f__mg$cache0 == null)
			{
				UxmlColorAttributeDescription.<>f__mg$cache0 = new Func<string, Color, Color>(UxmlColorAttributeDescription.ConvertValueToColor);
			}
			return base.GetValueFromBag<Color>(bag, cc, UxmlColorAttributeDescription.<>f__mg$cache0, this.defaultValue);
		}

		// Token: 0x06000935 RID: 2357 RVA: 0x000200D8 File Offset: 0x0001E2D8
		private static Color ConvertValueToColor(string v, Color defaultValue)
		{
			Color color;
			Color result;
			if (v == null || !ColorUtility.TryParseHtmlString(v, out color))
			{
				result = defaultValue;
			}
			else
			{
				result = color;
			}
			return result;
		}

		// Token: 0x04000420 RID: 1056
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Color <defaultValue>k__BackingField;

		// Token: 0x04000421 RID: 1057
		[CompilerGenerated]
		private static Func<string, Color, Color> <>f__mg$cache0;
	}
}
