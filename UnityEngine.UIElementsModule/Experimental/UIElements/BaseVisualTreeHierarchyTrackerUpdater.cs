﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000D9 RID: 217
	internal abstract class BaseVisualTreeHierarchyTrackerUpdater : BaseVisualTreeUpdater
	{
		// Token: 0x06000647 RID: 1607 RVA: 0x000154D6 File Offset: 0x000136D6
		protected BaseVisualTreeHierarchyTrackerUpdater()
		{
		}

		// Token: 0x06000648 RID: 1608
		protected abstract void OnHierarchyChange(VisualElement ve, HierarchyChangeType type);

		// Token: 0x06000649 RID: 1609 RVA: 0x000154E8 File Offset: 0x000136E8
		public override void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType)
		{
			if ((versionChangeType & VersionChangeType.Hierarchy) == VersionChangeType.Hierarchy)
			{
				BaseVisualTreeHierarchyTrackerUpdater.State state = this.m_State;
				if (state != BaseVisualTreeHierarchyTrackerUpdater.State.Waiting)
				{
					if (state != BaseVisualTreeHierarchyTrackerUpdater.State.TrackingRemove)
					{
						if (state == BaseVisualTreeHierarchyTrackerUpdater.State.TrackingAddOrMove)
						{
							this.ProcessAddOrMove(ve);
						}
					}
					else
					{
						this.ProcessRemove(ve);
					}
				}
				else
				{
					this.ProcessNewChange(ve);
				}
			}
		}

		// Token: 0x0600064A RID: 1610 RVA: 0x00015548 File Offset: 0x00013748
		public override void Update()
		{
			Debug.Assert(this.m_State == BaseVisualTreeHierarchyTrackerUpdater.State.TrackingAddOrMove || this.m_State == BaseVisualTreeHierarchyTrackerUpdater.State.Waiting);
			if (this.m_State == BaseVisualTreeHierarchyTrackerUpdater.State.TrackingAddOrMove)
			{
				this.OnHierarchyChange(this.m_CurrentChangeElement, HierarchyChangeType.Move);
				this.m_State = BaseVisualTreeHierarchyTrackerUpdater.State.Waiting;
			}
			this.m_CurrentChangeElement = null;
			this.m_CurrentChangeParent = null;
		}

		// Token: 0x0600064B RID: 1611 RVA: 0x000155A4 File Offset: 0x000137A4
		private void ProcessNewChange(VisualElement ve)
		{
			this.m_CurrentChangeElement = ve;
			this.m_CurrentChangeParent = ve.parent;
			if (this.m_CurrentChangeParent == null && ve.panel != null)
			{
				this.OnHierarchyChange(this.m_CurrentChangeElement, HierarchyChangeType.Move);
				this.m_State = BaseVisualTreeHierarchyTrackerUpdater.State.Waiting;
			}
			else
			{
				this.m_State = ((this.m_CurrentChangeParent != null) ? BaseVisualTreeHierarchyTrackerUpdater.State.TrackingAddOrMove : BaseVisualTreeHierarchyTrackerUpdater.State.TrackingRemove);
			}
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x00015610 File Offset: 0x00013810
		private void ProcessAddOrMove(VisualElement ve)
		{
			Debug.Assert(this.m_CurrentChangeParent != null);
			if (this.m_CurrentChangeParent == ve)
			{
				this.OnHierarchyChange(this.m_CurrentChangeElement, HierarchyChangeType.Add);
				this.m_State = BaseVisualTreeHierarchyTrackerUpdater.State.Waiting;
			}
			else
			{
				this.OnHierarchyChange(this.m_CurrentChangeElement, HierarchyChangeType.Move);
				this.ProcessNewChange(ve);
			}
		}

		// Token: 0x0600064D RID: 1613 RVA: 0x0001566C File Offset: 0x0001386C
		private void ProcessRemove(VisualElement ve)
		{
			this.OnHierarchyChange(this.m_CurrentChangeElement, HierarchyChangeType.Remove);
			if (ve.panel != null)
			{
				this.m_CurrentChangeParent = null;
				this.m_CurrentChangeElement = null;
				this.m_State = BaseVisualTreeHierarchyTrackerUpdater.State.Waiting;
			}
			else
			{
				this.m_CurrentChangeElement = ve;
			}
		}

		// Token: 0x04000272 RID: 626
		private BaseVisualTreeHierarchyTrackerUpdater.State m_State = BaseVisualTreeHierarchyTrackerUpdater.State.Waiting;

		// Token: 0x04000273 RID: 627
		private VisualElement m_CurrentChangeElement;

		// Token: 0x04000274 RID: 628
		private VisualElement m_CurrentChangeParent;

		// Token: 0x020000DA RID: 218
		private enum State
		{
			// Token: 0x04000276 RID: 630
			Waiting,
			// Token: 0x04000277 RID: 631
			TrackingAddOrMove,
			// Token: 0x04000278 RID: 632
			TrackingRemove
		}
	}
}
