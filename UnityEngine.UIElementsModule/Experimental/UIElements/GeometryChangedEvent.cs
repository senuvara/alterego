﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000130 RID: 304
	public class GeometryChangedEvent : EventBase<GeometryChangedEvent>, IPropagatableEvent
	{
		// Token: 0x0600082B RID: 2091 RVA: 0x0001B637 File Offset: 0x00019837
		public GeometryChangedEvent()
		{
			this.Init();
		}

		// Token: 0x0600082C RID: 2092 RVA: 0x0001B648 File Offset: 0x00019848
		public static GeometryChangedEvent GetPooled(Rect oldRect, Rect newRect)
		{
			GeometryChangedEvent pooled = EventBase<GeometryChangedEvent>.GetPooled();
			pooled.oldRect = oldRect;
			pooled.newRect = newRect;
			return pooled;
		}

		// Token: 0x0600082D RID: 2093 RVA: 0x0001B672 File Offset: 0x00019872
		protected override void Init()
		{
			base.Init();
			this.oldRect = Rect.zero;
			this.newRect = Rect.zero;
		}

		// Token: 0x17000213 RID: 531
		// (get) Token: 0x0600082E RID: 2094 RVA: 0x0001B694 File Offset: 0x00019894
		// (set) Token: 0x0600082F RID: 2095 RVA: 0x0001B6AE File Offset: 0x000198AE
		public Rect oldRect
		{
			[CompilerGenerated]
			get
			{
				return this.<oldRect>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<oldRect>k__BackingField = value;
			}
		}

		// Token: 0x17000214 RID: 532
		// (get) Token: 0x06000830 RID: 2096 RVA: 0x0001B6B8 File Offset: 0x000198B8
		// (set) Token: 0x06000831 RID: 2097 RVA: 0x0001B6D2 File Offset: 0x000198D2
		public Rect newRect
		{
			[CompilerGenerated]
			get
			{
				return this.<newRect>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<newRect>k__BackingField = value;
			}
		}

		// Token: 0x04000319 RID: 793
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Rect <oldRect>k__BackingField;

		// Token: 0x0400031A RID: 794
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Rect <newRect>k__BackingField;
	}
}
