﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000007 RID: 7
	public class BindableElement : VisualElement, IBindable
	{
		// Token: 0x06000031 RID: 49 RVA: 0x00002CD9 File Offset: 0x00000ED9
		public BindableElement()
		{
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000032 RID: 50 RVA: 0x00002CE4 File Offset: 0x00000EE4
		// (set) Token: 0x06000033 RID: 51 RVA: 0x00002CFE File Offset: 0x00000EFE
		public IBinding binding
		{
			[CompilerGenerated]
			get
			{
				return this.<binding>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<binding>k__BackingField = value;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000034 RID: 52 RVA: 0x00002D08 File Offset: 0x00000F08
		// (set) Token: 0x06000035 RID: 53 RVA: 0x00002D22 File Offset: 0x00000F22
		public string bindingPath
		{
			[CompilerGenerated]
			get
			{
				return this.<bindingPath>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<bindingPath>k__BackingField = value;
			}
		}

		// Token: 0x04000010 RID: 16
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private IBinding <binding>k__BackingField;

		// Token: 0x04000011 RID: 17
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <bindingPath>k__BackingField;

		// Token: 0x02000008 RID: 8
		public new class UxmlFactory : UxmlFactory<BindableElement, BindableElement.UxmlTraits>
		{
			// Token: 0x06000036 RID: 54 RVA: 0x00002D2B File Offset: 0x00000F2B
			public UxmlFactory()
			{
			}
		}

		// Token: 0x02000009 RID: 9
		public new class UxmlTraits : VisualElement.UxmlTraits
		{
			// Token: 0x06000037 RID: 55 RVA: 0x00002D34 File Offset: 0x00000F34
			public UxmlTraits()
			{
				this.m_PropertyPath = new UxmlStringAttributeDescription
				{
					name = "binding-path"
				};
			}

			// Token: 0x06000038 RID: 56 RVA: 0x00002D60 File Offset: 0x00000F60
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				string valueFromBag = this.m_PropertyPath.GetValueFromBag(bag, cc);
				if (!string.IsNullOrEmpty(valueFromBag))
				{
					IBindable bindable = ve as IBindable;
					if (bindable != null)
					{
						bindable.bindingPath = valueFromBag;
					}
				}
			}

			// Token: 0x04000012 RID: 18
			private UxmlStringAttributeDescription m_PropertyPath;
		}
	}
}
