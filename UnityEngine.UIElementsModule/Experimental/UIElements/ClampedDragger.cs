﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200000F RID: 15
	internal class ClampedDragger<T> : Clickable where T : IComparable<T>
	{
		// Token: 0x0600003F RID: 63 RVA: 0x00002DEB File Offset: 0x00000FEB
		public ClampedDragger(BaseSlider<T> slider, Action clickHandler, Action dragHandler) : base(clickHandler, 250L, 30L)
		{
			this.dragDirection = ClampedDragger<T>.DragDirection.None;
			this.slider = slider;
			this.dragging += dragHandler;
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000040 RID: 64 RVA: 0x00002E14 File Offset: 0x00001014
		// (remove) Token: 0x06000041 RID: 65 RVA: 0x00002E4C File Offset: 0x0000104C
		public event Action dragging
		{
			add
			{
				Action action = this.dragging;
				Action action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action>(ref this.dragging, (Action)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action action = this.dragging;
				Action action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action>(ref this.dragging, (Action)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000042 RID: 66 RVA: 0x00002E84 File Offset: 0x00001084
		// (set) Token: 0x06000043 RID: 67 RVA: 0x00002E9E File Offset: 0x0000109E
		public ClampedDragger<T>.DragDirection dragDirection
		{
			[CompilerGenerated]
			get
			{
				return this.<dragDirection>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<dragDirection>k__BackingField = value;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000044 RID: 68 RVA: 0x00002EA8 File Offset: 0x000010A8
		// (set) Token: 0x06000045 RID: 69 RVA: 0x00002EC2 File Offset: 0x000010C2
		private BaseSlider<T> slider
		{
			[CompilerGenerated]
			get
			{
				return this.<slider>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<slider>k__BackingField = value;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000046 RID: 70 RVA: 0x00002ECC File Offset: 0x000010CC
		// (set) Token: 0x06000047 RID: 71 RVA: 0x00002EE6 File Offset: 0x000010E6
		public Vector2 startMousePosition
		{
			[CompilerGenerated]
			get
			{
				return this.<startMousePosition>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<startMousePosition>k__BackingField = value;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000048 RID: 72 RVA: 0x00002EF0 File Offset: 0x000010F0
		public Vector2 delta
		{
			[CompilerGenerated]
			get
			{
				return base.lastMousePosition - this.startMousePosition;
			}
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002F18 File Offset: 0x00001118
		protected override void RegisterCallbacksOnTarget()
		{
			base.target.RegisterCallback<MouseDownEvent>(new EventCallback<MouseDownEvent>(this.OnMouseDown), TrickleDown.NoTrickleDown);
			base.target.RegisterCallback<MouseMoveEvent>(new EventCallback<MouseMoveEvent>(this.OnMouseMove), TrickleDown.NoTrickleDown);
			base.target.RegisterCallback<MouseUpEvent>(new EventCallback<MouseUpEvent>(base.OnMouseUp), TrickleDown.NoTrickleDown);
		}

		// Token: 0x0600004A RID: 74 RVA: 0x00002F70 File Offset: 0x00001170
		protected override void UnregisterCallbacksFromTarget()
		{
			base.target.UnregisterCallback<MouseDownEvent>(new EventCallback<MouseDownEvent>(this.OnMouseDown), TrickleDown.NoTrickleDown);
			base.target.UnregisterCallback<MouseMoveEvent>(new EventCallback<MouseMoveEvent>(this.OnMouseMove), TrickleDown.NoTrickleDown);
			base.target.UnregisterCallback<MouseUpEvent>(new EventCallback<MouseUpEvent>(base.OnMouseUp), TrickleDown.NoTrickleDown);
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002FC6 File Offset: 0x000011C6
		private new void OnMouseDown(MouseDownEvent evt)
		{
			if (base.CanStartManipulation(evt))
			{
				this.startMousePosition = evt.localMousePosition;
				this.dragDirection = ClampedDragger<T>.DragDirection.None;
				base.OnMouseDown(evt);
			}
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00002FF4 File Offset: 0x000011F4
		private new void OnMouseMove(MouseMoveEvent evt)
		{
			if (this.m_Active)
			{
				base.OnMouseMove(evt);
				if (this.dragDirection == ClampedDragger<T>.DragDirection.None)
				{
					this.dragDirection = ClampedDragger<T>.DragDirection.Free;
				}
				if (this.dragDirection == ClampedDragger<T>.DragDirection.Free)
				{
					if (this.dragging != null)
					{
						this.dragging();
					}
				}
			}
		}

		// Token: 0x04000014 RID: 20
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Action dragging;

		// Token: 0x04000015 RID: 21
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ClampedDragger<T>.DragDirection <dragDirection>k__BackingField;

		// Token: 0x04000016 RID: 22
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private BaseSlider<T> <slider>k__BackingField;

		// Token: 0x04000017 RID: 23
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Vector2 <startMousePosition>k__BackingField;

		// Token: 0x02000010 RID: 16
		[Flags]
		public enum DragDirection
		{
			// Token: 0x04000019 RID: 25
			None = 0,
			// Token: 0x0400001A RID: 26
			LowToHigh = 1,
			// Token: 0x0400001B RID: 27
			HighToLow = 2,
			// Token: 0x0400001C RID: 28
			Free = 4
		}
	}
}
