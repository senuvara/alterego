﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200015C RID: 348
	public interface IUxmlAttributes
	{
		// Token: 0x060008EE RID: 2286
		bool TryGetAttributeValue(string attributeName, out string value);
	}
}
