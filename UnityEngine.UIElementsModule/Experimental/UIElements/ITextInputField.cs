﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000F9 RID: 249
	internal interface ITextInputField : IEventHandler, ITextElement
	{
		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x060006EE RID: 1774
		bool hasFocus { get; }

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x060006EF RID: 1775
		bool doubleClickSelectsWord { get; }

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x060006F0 RID: 1776
		bool tripleClickSelectsLine { get; }

		// Token: 0x060006F1 RID: 1777
		void SyncTextEngine();

		// Token: 0x060006F2 RID: 1778
		bool AcceptCharacter(char c);

		// Token: 0x060006F3 RID: 1779
		string CullString(string s);

		// Token: 0x060006F4 RID: 1780
		void UpdateText(string value);
	}
}
