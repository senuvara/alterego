﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Experimental.UIElements.StyleEnums;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000077 RID: 119
	public class ScrollView : VisualElement
	{
		// Token: 0x06000332 RID: 818 RVA: 0x0000B230 File Offset: 0x00009430
		public ScrollView()
		{
			this.contentViewport = new VisualElement
			{
				name = "ContentViewport"
			};
			this.contentViewport.style.overflow = Overflow.Hidden;
			base.shadow.Add(this.contentViewport);
			this.AssignContentContainer(new VisualElement
			{
				name = "ContentView"
			});
			this.horizontalScroller = new Scroller(0f, 100f, delegate(float value)
			{
				this.scrollOffset = new Vector2(value, this.scrollOffset.y);
				this.UpdateContentViewTransform();
			}, SliderDirection.Horizontal)
			{
				name = "HorizontalScroller",
				persistenceKey = "HorizontalScroller",
				visible = false
			};
			base.shadow.Add(this.horizontalScroller);
			this.verticalScroller = new Scroller(0f, 100f, delegate(float value)
			{
				this.scrollOffset = new Vector2(this.scrollOffset.x, value);
				this.UpdateContentViewTransform();
			}, SliderDirection.Vertical)
			{
				name = "VerticalScroller",
				persistenceKey = "VerticalScroller",
				visible = false
			};
			base.shadow.Add(this.verticalScroller);
			base.RegisterCallback<WheelEvent>(new EventCallback<WheelEvent>(this.OnScrollWheel), TrickleDown.NoTrickleDown);
			this.scrollOffset = Vector2.zero;
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000333 RID: 819 RVA: 0x0000B370 File Offset: 0x00009570
		// (set) Token: 0x06000334 RID: 820 RVA: 0x0000B38C File Offset: 0x0000958C
		public bool stretchContentWidth
		{
			get
			{
				return this.m_StretchContentWidth;
			}
			set
			{
				if (this.m_StretchContentWidth != value)
				{
					this.m_StretchContentWidth = value;
					if (this.m_StretchContentWidth)
					{
						base.AddToClassList("stretchContentWidth");
					}
					else
					{
						base.RemoveFromClassList("stretchContentWidth");
					}
				}
			}
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000335 RID: 821 RVA: 0x0000B3E0 File Offset: 0x000095E0
		// (set) Token: 0x06000336 RID: 822 RVA: 0x0000B3FA File Offset: 0x000095FA
		public bool showHorizontal
		{
			[CompilerGenerated]
			get
			{
				return this.<showHorizontal>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<showHorizontal>k__BackingField = value;
			}
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000337 RID: 823 RVA: 0x0000B404 File Offset: 0x00009604
		// (set) Token: 0x06000338 RID: 824 RVA: 0x0000B41E File Offset: 0x0000961E
		public bool showVertical
		{
			[CompilerGenerated]
			get
			{
				return this.<showVertical>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<showVertical>k__BackingField = value;
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000339 RID: 825 RVA: 0x0000B428 File Offset: 0x00009628
		public bool needsHorizontal
		{
			get
			{
				return this.showHorizontal || this.contentContainer.layout.width - base.layout.width > 0f;
			}
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x0600033A RID: 826 RVA: 0x0000B474 File Offset: 0x00009674
		public bool needsVertical
		{
			get
			{
				return this.showVertical || this.contentContainer.layout.height - base.layout.height > 0f;
			}
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x0600033B RID: 827 RVA: 0x0000B4C0 File Offset: 0x000096C0
		// (set) Token: 0x0600033C RID: 828 RVA: 0x0000B4F0 File Offset: 0x000096F0
		public Vector2 scrollOffset
		{
			get
			{
				return new Vector2(this.horizontalScroller.value, this.verticalScroller.value);
			}
			set
			{
				if (value != this.scrollOffset)
				{
					this.horizontalScroller.value = value.x;
					this.verticalScroller.value = value.y;
					this.UpdateContentViewTransform();
				}
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x0600033D RID: 829 RVA: 0x0000B530 File Offset: 0x00009730
		// (set) Token: 0x0600033E RID: 830 RVA: 0x0000B555 File Offset: 0x00009755
		public float horizontalPageSize
		{
			get
			{
				return this.horizontalScroller.slider.pageSize;
			}
			set
			{
				this.horizontalScroller.slider.pageSize = value;
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x0600033F RID: 831 RVA: 0x0000B56C File Offset: 0x0000976C
		// (set) Token: 0x06000340 RID: 832 RVA: 0x0000B591 File Offset: 0x00009791
		public float verticalPageSize
		{
			get
			{
				return this.verticalScroller.slider.pageSize;
			}
			set
			{
				this.verticalScroller.slider.pageSize = value;
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000341 RID: 833 RVA: 0x0000B5A8 File Offset: 0x000097A8
		private float scrollableWidth
		{
			get
			{
				return this.contentContainer.layout.width - this.contentViewport.layout.width;
			}
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000342 RID: 834 RVA: 0x0000B5E4 File Offset: 0x000097E4
		private float scrollableHeight
		{
			get
			{
				return this.contentContainer.layout.height - this.contentViewport.layout.height;
			}
		}

		// Token: 0x06000343 RID: 835 RVA: 0x0000B620 File Offset: 0x00009820
		private void UpdateContentViewTransform()
		{
			Vector3 position = this.contentContainer.transform.position;
			Vector2 scrollOffset = this.scrollOffset;
			position.x = -scrollOffset.x;
			position.y = -scrollOffset.y;
			this.contentContainer.transform.position = position;
			base.IncrementVersion(VersionChangeType.Repaint);
		}

		// Token: 0x06000344 RID: 836 RVA: 0x0000B67C File Offset: 0x0000987C
		public void ScrollTo(VisualElement child)
		{
			if (!this.contentContainer.Contains(child))
			{
				throw new ArgumentException("Cannot scroll to null child");
			}
			float num = this.contentContainer.layout.height - this.contentViewport.layout.height;
			float num2 = this.contentContainer.transform.position.y * -1f;
			float num3 = this.contentViewport.layout.yMin + num2;
			float num4 = this.contentViewport.layout.yMax + num2;
			float yMin = child.layout.yMin;
			float yMax = child.layout.yMax;
			if ((yMin < num3 || yMax > num4) && !float.IsNaN(yMin) && !float.IsNaN(yMax))
			{
				bool flag = false;
				float num5 = yMax - num4;
				if (num5 < -1f)
				{
					num5 = num3 - yMin;
					flag = true;
				}
				float num6 = num5 * this.verticalScroller.highValue / num;
				this.verticalScroller.value = this.scrollOffset.y + ((!flag) ? num6 : (-num6));
				this.UpdateContentViewTransform();
			}
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000345 RID: 837 RVA: 0x0000B7D8 File Offset: 0x000099D8
		// (set) Token: 0x06000346 RID: 838 RVA: 0x0000B7F2 File Offset: 0x000099F2
		public VisualElement contentViewport
		{
			[CompilerGenerated]
			get
			{
				return this.<contentViewport>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<contentViewport>k__BackingField = value;
			}
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x06000347 RID: 839 RVA: 0x0000B7FC File Offset: 0x000099FC
		[Obsolete("Please use contentContainer instead", false)]
		public VisualElement contentView
		{
			get
			{
				return this.contentContainer;
			}
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000348 RID: 840 RVA: 0x0000B818 File Offset: 0x00009A18
		// (set) Token: 0x06000349 RID: 841 RVA: 0x0000B832 File Offset: 0x00009A32
		public Scroller horizontalScroller
		{
			[CompilerGenerated]
			get
			{
				return this.<horizontalScroller>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<horizontalScroller>k__BackingField = value;
			}
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x0600034A RID: 842 RVA: 0x0000B83C File Offset: 0x00009A3C
		// (set) Token: 0x0600034B RID: 843 RVA: 0x0000B856 File Offset: 0x00009A56
		public Scroller verticalScroller
		{
			[CompilerGenerated]
			get
			{
				return this.<verticalScroller>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<verticalScroller>k__BackingField = value;
			}
		}

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x0600034C RID: 844 RVA: 0x0000B860 File Offset: 0x00009A60
		public override VisualElement contentContainer
		{
			get
			{
				return this.m_ContentContainer;
			}
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000B87B File Offset: 0x00009A7B
		private void AssignContentContainer(VisualElement contents)
		{
			contents.RegisterCallback<GeometryChangedEvent>(new EventCallback<GeometryChangedEvent>(this.OnGeometryChanged), TrickleDown.NoTrickleDown);
			contents.AddToClassList(ScrollView.contentViewClass);
			this.contentViewport.Add(contents);
			this.m_ContentContainer = contents;
		}

		// Token: 0x0600034E RID: 846 RVA: 0x0000B8B0 File Offset: 0x00009AB0
		public void SetContents(VisualElement contents)
		{
			if (contents != null && contents != this.m_ContentContainer)
			{
				if (this.m_ContentContainer != null)
				{
					this.m_ContentContainer.RemoveFromHierarchy();
					this.m_ContentContainer.UnregisterCallback<GeometryChangedEvent>(new EventCallback<GeometryChangedEvent>(this.OnGeometryChanged), TrickleDown.NoTrickleDown);
					this.m_ContentContainer.RemoveFromClassList(ScrollView.contentViewClass);
				}
				this.AssignContentContainer(contents);
				this.scrollOffset = Vector2.zero;
			}
		}

		// Token: 0x0600034F RID: 847 RVA: 0x0000B924 File Offset: 0x00009B24
		protected internal override void ExecuteDefaultAction(EventBase evt)
		{
			base.ExecuteDefaultAction(evt);
			if (evt.GetEventTypeId() == EventBase<GeometryChangedEvent>.TypeId())
			{
				this.OnGeometryChanged((GeometryChangedEvent)evt);
			}
		}

		// Token: 0x06000350 RID: 848 RVA: 0x0000B94C File Offset: 0x00009B4C
		private void OnGeometryChanged(GeometryChangedEvent evt)
		{
			if (!(evt.oldRect.size == evt.newRect.size))
			{
				if (this.contentContainer.layout.width > Mathf.Epsilon)
				{
					this.horizontalScroller.Adjust(this.contentViewport.layout.width / this.contentContainer.layout.width);
				}
				if (this.contentContainer.layout.height > Mathf.Epsilon)
				{
					this.verticalScroller.Adjust(this.contentViewport.layout.height / this.contentContainer.layout.height);
				}
				this.horizontalScroller.SetEnabled(this.contentContainer.layout.width - base.layout.width > 0f);
				this.verticalScroller.SetEnabled(this.contentContainer.layout.height - base.layout.height > 0f);
				this.contentViewport.style.positionRight = ((!this.needsVertical) ? 0f : this.verticalScroller.layout.width);
				this.horizontalScroller.style.positionRight = ((!this.needsVertical) ? 0f : this.verticalScroller.layout.width);
				this.contentViewport.style.positionBottom = ((!this.needsHorizontal) ? 0f : this.horizontalScroller.layout.height);
				this.verticalScroller.style.positionBottom = ((!this.needsHorizontal) ? 0f : this.horizontalScroller.layout.height);
				if (this.needsHorizontal && this.scrollableWidth > 0f)
				{
					this.horizontalScroller.lowValue = 0f;
					this.horizontalScroller.highValue = this.scrollableWidth;
				}
				else
				{
					this.horizontalScroller.value = 0f;
				}
				if (this.needsVertical && this.scrollableHeight > 0f)
				{
					this.verticalScroller.lowValue = 0f;
					this.verticalScroller.highValue = this.scrollableHeight;
				}
				else
				{
					this.verticalScroller.value = 0f;
				}
				if (this.horizontalScroller.visible != this.needsHorizontal)
				{
					this.horizontalScroller.visible = this.needsHorizontal;
					if (this.needsHorizontal)
					{
						this.contentViewport.AddToClassList(ScrollView.horizontalScrollClass);
					}
					else
					{
						this.contentViewport.RemoveFromClassList(ScrollView.horizontalScrollClass);
					}
				}
				if (this.verticalScroller.visible != this.needsVertical)
				{
					this.verticalScroller.visible = this.needsVertical;
					if (this.needsVertical)
					{
						this.contentViewport.AddToClassList(ScrollView.verticalScrollClass);
					}
					else
					{
						this.contentViewport.RemoveFromClassList(ScrollView.verticalScrollClass);
					}
				}
				this.UpdateContentViewTransform();
			}
		}

		// Token: 0x06000351 RID: 849 RVA: 0x0000BCF8 File Offset: 0x00009EF8
		private void OnScrollWheel(WheelEvent evt)
		{
			if (this.contentContainer.layout.height - base.layout.height > 0f)
			{
				if (evt.delta.y < 0f)
				{
					this.verticalScroller.ScrollPageUp(Mathf.Abs(evt.delta.y));
				}
				else if (evt.delta.y > 0f)
				{
					this.verticalScroller.ScrollPageDown(Mathf.Abs(evt.delta.y));
				}
			}
			evt.StopPropagation();
		}

		// Token: 0x06000352 RID: 850 RVA: 0x0000BDAD File Offset: 0x00009FAD
		// Note: this type is marked as 'beforefieldinit'.
		static ScrollView()
		{
		}

		// Token: 0x06000353 RID: 851 RVA: 0x0000BDD0 File Offset: 0x00009FD0
		[CompilerGenerated]
		private void <ScrollView>m__0(float value)
		{
			this.scrollOffset = new Vector2(value, this.scrollOffset.y);
			this.UpdateContentViewTransform();
		}

		// Token: 0x06000354 RID: 852 RVA: 0x0000BE00 File Offset: 0x0000A000
		[CompilerGenerated]
		private void <ScrollView>m__1(float value)
		{
			this.scrollOffset = new Vector2(this.scrollOffset.x, value);
			this.UpdateContentViewTransform();
		}

		// Token: 0x04000141 RID: 321
		private bool m_StretchContentWidth = false;

		// Token: 0x04000142 RID: 322
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <showHorizontal>k__BackingField;

		// Token: 0x04000143 RID: 323
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <showVertical>k__BackingField;

		// Token: 0x04000144 RID: 324
		private VisualElement m_ContentContainer;

		// Token: 0x04000145 RID: 325
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VisualElement <contentViewport>k__BackingField;

		// Token: 0x04000146 RID: 326
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Scroller <horizontalScroller>k__BackingField;

		// Token: 0x04000147 RID: 327
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Scroller <verticalScroller>k__BackingField;

		// Token: 0x04000148 RID: 328
		private static readonly string contentViewClass = "content-view";

		// Token: 0x04000149 RID: 329
		private static readonly string horizontalScrollClass = "horizontal-scroll-visible";

		// Token: 0x0400014A RID: 330
		private static readonly string verticalScrollClass = "vertical-scroll-visible";

		// Token: 0x02000078 RID: 120
		public new class UxmlFactory : UxmlFactory<ScrollView, ScrollView.UxmlTraits>
		{
			// Token: 0x06000355 RID: 853 RVA: 0x0000BE2E File Offset: 0x0000A02E
			public UxmlFactory()
			{
			}
		}

		// Token: 0x02000079 RID: 121
		public new class UxmlTraits : VisualElement.UxmlTraits
		{
			// Token: 0x06000356 RID: 854 RVA: 0x0000BE38 File Offset: 0x0000A038
			public UxmlTraits()
			{
			}

			// Token: 0x06000357 RID: 855 RVA: 0x0000BEDC File Offset: 0x0000A0DC
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				ScrollView scrollView = (ScrollView)ve;
				scrollView.showHorizontal = this.m_ShowHorizontal.GetValueFromBag(bag, cc);
				scrollView.showVertical = this.m_ShowVertical.GetValueFromBag(bag, cc);
				scrollView.stretchContentWidth = this.m_StretchContentWidth.GetValueFromBag(bag, cc);
				scrollView.horizontalPageSize = this.m_HorizontalPageSize.GetValueFromBag(bag, cc);
				scrollView.verticalPageSize = this.m_VerticalPageSize.GetValueFromBag(bag, cc);
			}

			// Token: 0x0400014B RID: 331
			private UxmlBoolAttributeDescription m_ShowHorizontal = new UxmlBoolAttributeDescription
			{
				name = "show-horizontal-scroller"
			};

			// Token: 0x0400014C RID: 332
			private UxmlBoolAttributeDescription m_ShowVertical = new UxmlBoolAttributeDescription
			{
				name = "show-vertical-scroller"
			};

			// Token: 0x0400014D RID: 333
			private UxmlFloatAttributeDescription m_HorizontalPageSize = new UxmlFloatAttributeDescription
			{
				name = "horizontal-page-size",
				defaultValue = 20f
			};

			// Token: 0x0400014E RID: 334
			private UxmlFloatAttributeDescription m_VerticalPageSize = new UxmlFloatAttributeDescription
			{
				name = "vertical-page-size",
				defaultValue = 20f
			};

			// Token: 0x0400014F RID: 335
			private UxmlBoolAttributeDescription m_StretchContentWidth = new UxmlBoolAttributeDescription
			{
				name = "stretch-content-width"
			};
		}
	}
}
