﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000100 RID: 256
	internal class VisualTreeBindingsUpdater : BaseVisualTreeHierarchyTrackerUpdater
	{
		// Token: 0x0600073A RID: 1850 RVA: 0x0001961B File Offset: 0x0001781B
		public VisualTreeBindingsUpdater()
		{
		}

		// Token: 0x170001DC RID: 476
		// (get) Token: 0x0600073B RID: 1851 RVA: 0x00019658 File Offset: 0x00017858
		public override string description
		{
			get
			{
				return "Update Bindings";
			}
		}

		// Token: 0x0600073C RID: 1852 RVA: 0x00019674 File Offset: 0x00017874
		private IBinding GetUpdaterFromElement(VisualElement ve)
		{
			IBindable bindable = ve as IBindable;
			return (bindable != null) ? bindable.binding : null;
		}

		// Token: 0x0600073D RID: 1853 RVA: 0x0001969E File Offset: 0x0001789E
		private void StartTracking(VisualElement ve)
		{
			this.m_ElementsToAdd.Add(ve);
			this.m_ElementsToRemove.Remove(ve);
		}

		// Token: 0x0600073E RID: 1854 RVA: 0x000196BB File Offset: 0x000178BB
		private void StopTracking(VisualElement ve)
		{
			this.m_ElementsToRemove.Add(ve);
			this.m_ElementsToAdd.Remove(ve);
		}

		// Token: 0x0600073F RID: 1855 RVA: 0x000196D8 File Offset: 0x000178D8
		private void StartTrackingRecursive(VisualElement ve)
		{
			IBinding updaterFromElement = this.GetUpdaterFromElement(ve);
			if (updaterFromElement != null)
			{
				this.StartTracking(ve);
			}
			int childCount = ve.shadow.childCount;
			for (int i = 0; i < childCount; i++)
			{
				VisualElement ve2 = ve.shadow[i];
				this.StartTrackingRecursive(ve2);
			}
		}

		// Token: 0x06000740 RID: 1856 RVA: 0x0001973C File Offset: 0x0001793C
		private void StopTrackingRecursive(VisualElement ve)
		{
			this.StopTracking(ve);
			int childCount = ve.shadow.childCount;
			for (int i = 0; i < childCount; i++)
			{
				VisualElement ve2 = ve.shadow[i];
				this.StopTrackingRecursive(ve2);
			}
		}

		// Token: 0x06000741 RID: 1857 RVA: 0x0001978C File Offset: 0x0001798C
		public override void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType)
		{
			base.OnVersionChanged(ve, versionChangeType);
			if ((versionChangeType & VersionChangeType.Bindings) == VersionChangeType.Bindings)
			{
				if (this.GetUpdaterFromElement(ve) != null)
				{
					this.StartTracking(ve);
				}
				else
				{
					this.StopTracking(ve);
				}
			}
		}

		// Token: 0x06000742 RID: 1858 RVA: 0x000197D8 File Offset: 0x000179D8
		protected override void OnHierarchyChange(VisualElement ve, HierarchyChangeType type)
		{
			if (type != HierarchyChangeType.Add)
			{
				if (type == HierarchyChangeType.Remove)
				{
					this.StopTrackingRecursive(ve);
				}
			}
			else
			{
				this.StartTrackingRecursive(ve);
			}
		}

		// Token: 0x06000743 RID: 1859 RVA: 0x0001980C File Offset: 0x00017A0C
		private static long CurrentTime()
		{
			return Panel.TimeSinceStartup();
		}

		// Token: 0x06000744 RID: 1860 RVA: 0x0001982C File Offset: 0x00017A2C
		public void PerformTrackingOperations()
		{
			foreach (VisualElement visualElement in this.m_ElementsToAdd)
			{
				IBinding updaterFromElement = this.GetUpdaterFromElement(visualElement);
				if (updaterFromElement != null)
				{
					this.m_ElementsWithBindings.Add(visualElement);
				}
			}
			this.m_ElementsToAdd.Clear();
			foreach (VisualElement item in this.m_ElementsToRemove)
			{
				this.m_ElementsWithBindings.Remove(item);
			}
			this.m_ElementsToRemove.Clear();
		}

		// Token: 0x06000745 RID: 1861 RVA: 0x00019910 File Offset: 0x00017B10
		public override void Update()
		{
			base.Update();
			this.PerformTrackingOperations();
			if (this.m_ElementsWithBindings.Count > 0)
			{
				long num = VisualTreeBindingsUpdater.CurrentTime();
				if (this.m_LastUpdateTime + 100L < num)
				{
					this.UpdateBindings();
					this.m_LastUpdateTime = num;
				}
			}
		}

		// Token: 0x06000746 RID: 1862 RVA: 0x00019964 File Offset: 0x00017B64
		private void UpdateBindings()
		{
			foreach (VisualElement visualElement in this.m_ElementsWithBindings)
			{
				IBinding updaterFromElement = this.GetUpdaterFromElement(visualElement);
				if (updaterFromElement == null || visualElement.elementPanel != base.panel)
				{
					if (updaterFromElement != null)
					{
						updaterFromElement.Release();
					}
					this.StopTracking(visualElement);
				}
				else
				{
					this.updatedBindings.Add(updaterFromElement);
				}
			}
			foreach (IBinding binding in this.updatedBindings)
			{
				binding.PreUpdate();
			}
			foreach (IBinding binding2 in this.updatedBindings)
			{
				binding2.Update();
			}
			this.updatedBindings.Clear();
		}

		// Token: 0x040002D1 RID: 721
		private readonly HashSet<VisualElement> m_ElementsWithBindings = new HashSet<VisualElement>();

		// Token: 0x040002D2 RID: 722
		private readonly HashSet<VisualElement> m_ElementsToAdd = new HashSet<VisualElement>();

		// Token: 0x040002D3 RID: 723
		private readonly HashSet<VisualElement> m_ElementsToRemove = new HashSet<VisualElement>();

		// Token: 0x040002D4 RID: 724
		private const int kMinUpdateDelay = 100;

		// Token: 0x040002D5 RID: 725
		private long m_LastUpdateTime = 0L;

		// Token: 0x040002D6 RID: 726
		private List<IBinding> updatedBindings = new List<IBinding>();
	}
}
