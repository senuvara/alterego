﻿using System;
using System.Collections.Generic;
using UnityEngine.Experimental.UIElements.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000DE RID: 222
	internal static class StyleCache
	{
		// Token: 0x06000663 RID: 1635 RVA: 0x00016618 File Offset: 0x00014818
		public static bool TryGetValue(long hash, out VisualElementStylesData data)
		{
			return StyleCache.s_StyleCache.TryGetValue(hash, out data);
		}

		// Token: 0x06000664 RID: 1636 RVA: 0x00016639 File Offset: 0x00014839
		public static void SetValue(long hash, VisualElementStylesData data)
		{
			StyleCache.s_StyleCache[hash] = data;
		}

		// Token: 0x06000665 RID: 1637 RVA: 0x00016648 File Offset: 0x00014848
		public static void ClearStyleCache()
		{
			StyleCache.s_StyleCache.Clear();
		}

		// Token: 0x06000666 RID: 1638 RVA: 0x00016655 File Offset: 0x00014855
		// Note: this type is marked as 'beforefieldinit'.
		static StyleCache()
		{
		}

		// Token: 0x04000283 RID: 643
		private static Dictionary<long, VisualElementStylesData> s_StyleCache = new Dictionary<long, VisualElementStylesData>();
	}
}
