﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000D5 RID: 213
	internal class VisualElementPanelActivator
	{
		// Token: 0x06000633 RID: 1587 RVA: 0x00015243 File Offset: 0x00013443
		public VisualElementPanelActivator(IVisualElementPanelActivatable activatable)
		{
			this.m_Activatable = activatable;
		}

		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x06000634 RID: 1588 RVA: 0x00015254 File Offset: 0x00013454
		// (set) Token: 0x06000635 RID: 1589 RVA: 0x0001526E File Offset: 0x0001346E
		public bool isActive
		{
			[CompilerGenerated]
			get
			{
				return this.<isActive>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<isActive>k__BackingField = value;
			}
		}

		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x06000636 RID: 1590 RVA: 0x00015278 File Offset: 0x00013478
		// (set) Token: 0x06000637 RID: 1591 RVA: 0x00015292 File Offset: 0x00013492
		public bool isDetaching
		{
			[CompilerGenerated]
			get
			{
				return this.<isDetaching>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<isDetaching>k__BackingField = value;
			}
		}

		// Token: 0x06000638 RID: 1592 RVA: 0x0001529C File Offset: 0x0001349C
		public void SetActive(bool action)
		{
			if (this.isActive != action)
			{
				this.isActive = action;
				if (this.isActive)
				{
					this.m_Activatable.element.RegisterCallback<AttachToPanelEvent>(new EventCallback<AttachToPanelEvent>(this.OnEnter), TrickleDown.NoTrickleDown);
					this.m_Activatable.element.RegisterCallback<DetachFromPanelEvent>(new EventCallback<DetachFromPanelEvent>(this.OnLeave), TrickleDown.NoTrickleDown);
					this.SendActivation();
				}
				else
				{
					this.m_Activatable.element.UnregisterCallback<AttachToPanelEvent>(new EventCallback<AttachToPanelEvent>(this.OnEnter), TrickleDown.NoTrickleDown);
					this.m_Activatable.element.UnregisterCallback<DetachFromPanelEvent>(new EventCallback<DetachFromPanelEvent>(this.OnLeave), TrickleDown.NoTrickleDown);
					this.SendDeactivation();
				}
			}
		}

		// Token: 0x06000639 RID: 1593 RVA: 0x00015353 File Offset: 0x00013553
		public void SendActivation()
		{
			if (this.m_Activatable.CanBeActivated())
			{
				this.m_Activatable.OnPanelActivate();
			}
		}

		// Token: 0x0600063A RID: 1594 RVA: 0x00015373 File Offset: 0x00013573
		public void SendDeactivation()
		{
			if (this.m_Activatable.CanBeActivated())
			{
				this.m_Activatable.OnPanelDeactivate();
			}
		}

		// Token: 0x0600063B RID: 1595 RVA: 0x00015393 File Offset: 0x00013593
		private void OnEnter(AttachToPanelEvent evt)
		{
			if (this.isActive)
			{
				this.SendActivation();
			}
		}

		// Token: 0x0600063C RID: 1596 RVA: 0x000153AC File Offset: 0x000135AC
		private void OnLeave(DetachFromPanelEvent evt)
		{
			if (this.isActive)
			{
				this.isDetaching = true;
				try
				{
					this.SendDeactivation();
				}
				finally
				{
					this.isDetaching = false;
				}
			}
		}

		// Token: 0x04000267 RID: 615
		private IVisualElementPanelActivatable m_Activatable;

		// Token: 0x04000268 RID: 616
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <isActive>k__BackingField;

		// Token: 0x04000269 RID: 617
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <isDetaching>k__BackingField;
	}
}
