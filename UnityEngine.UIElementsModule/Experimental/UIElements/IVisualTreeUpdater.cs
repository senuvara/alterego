﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000E6 RID: 230
	internal interface IVisualTreeUpdater : IDisposable
	{
		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x0600068F RID: 1679
		// (set) Token: 0x06000690 RID: 1680
		BaseVisualElementPanel panel { get; set; }

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06000691 RID: 1681
		string description { get; }

		// Token: 0x06000692 RID: 1682
		void Update();

		// Token: 0x06000693 RID: 1683
		void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType);
	}
}
