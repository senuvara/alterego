﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200012C RID: 300
	public interface IKeyboardEvent
	{
		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000812 RID: 2066
		EventModifiers modifiers { get; }

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x06000813 RID: 2067
		char character { get; }

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000814 RID: 2068
		KeyCode keyCode { get; }

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x06000815 RID: 2069
		bool shiftKey { get; }

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x06000816 RID: 2070
		bool ctrlKey { get; }

		// Token: 0x17000208 RID: 520
		// (get) Token: 0x06000817 RID: 2071
		bool commandKey { get; }

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x06000818 RID: 2072
		bool altKey { get; }

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x06000819 RID: 2073
		bool actionKey { get; }
	}
}
