﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000144 RID: 324
	public class TooltipEvent : EventBase<TooltipEvent>, IPropagatableEvent
	{
		// Token: 0x0600087D RID: 2173 RVA: 0x0001BE5C File Offset: 0x0001A05C
		public TooltipEvent()
		{
			this.Init();
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x0600087E RID: 2174 RVA: 0x0001BE6C File Offset: 0x0001A06C
		// (set) Token: 0x0600087F RID: 2175 RVA: 0x0001BE86 File Offset: 0x0001A086
		public string tooltip
		{
			[CompilerGenerated]
			get
			{
				return this.<tooltip>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<tooltip>k__BackingField = value;
			}
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x06000880 RID: 2176 RVA: 0x0001BE90 File Offset: 0x0001A090
		// (set) Token: 0x06000881 RID: 2177 RVA: 0x0001BEAA File Offset: 0x0001A0AA
		public Rect rect
		{
			[CompilerGenerated]
			get
			{
				return this.<rect>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<rect>k__BackingField = value;
			}
		}

		// Token: 0x06000882 RID: 2178 RVA: 0x0001BEB4 File Offset: 0x0001A0B4
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown);
			this.rect = default(Rect);
			this.tooltip = string.Empty;
		}

		// Token: 0x06000883 RID: 2179 RVA: 0x0001BEEC File Offset: 0x0001A0EC
		internal static TooltipEvent GetPooled(string tooltip, Rect rect)
		{
			TooltipEvent pooled = EventBase<TooltipEvent>.GetPooled();
			pooled.tooltip = tooltip;
			pooled.rect = rect;
			return pooled;
		}

		// Token: 0x04000328 RID: 808
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <tooltip>k__BackingField;

		// Token: 0x04000329 RID: 809
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Rect <rect>k__BackingField;
	}
}
