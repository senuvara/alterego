﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200011C RID: 284
	[Obsolete("Use TrickleDown instead of Capture.")]
	public enum Capture
	{
		// Token: 0x040002FE RID: 766
		NoCapture,
		// Token: 0x040002FF RID: 767
		Capture
	}
}
