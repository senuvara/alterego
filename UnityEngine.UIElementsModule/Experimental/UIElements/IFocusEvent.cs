﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000125 RID: 293
	public interface IFocusEvent
	{
		// Token: 0x170001FD RID: 509
		// (get) Token: 0x060007FA RID: 2042
		Focusable relatedTarget { get; }

		// Token: 0x170001FE RID: 510
		// (get) Token: 0x060007FB RID: 2043
		FocusChangeDirection direction { get; }
	}
}
