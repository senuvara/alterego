﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200005B RID: 91
	[Flags]
	public enum ChangeType
	{
		// Token: 0x040000DC RID: 220
		PersistentData = 64,
		// Token: 0x040000DD RID: 221
		PersistentDataPath = 32,
		// Token: 0x040000DE RID: 222
		Layout = 16,
		// Token: 0x040000DF RID: 223
		Styles = 8,
		// Token: 0x040000E0 RID: 224
		Transform = 4,
		// Token: 0x040000E1 RID: 225
		StylesPath = 2,
		// Token: 0x040000E2 RID: 226
		Repaint = 1,
		// Token: 0x040000E3 RID: 227
		All = 127
	}
}
