﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000037 RID: 55
	public interface IDataWatchHandle : IDisposable
	{
		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000104 RID: 260
		Object watched { get; }

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000105 RID: 261
		bool disposed { get; }
	}
}
