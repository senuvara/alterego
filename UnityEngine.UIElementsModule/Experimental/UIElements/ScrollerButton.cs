﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200007A RID: 122
	public class ScrollerButton : VisualElement
	{
		// Token: 0x06000358 RID: 856 RVA: 0x0000BF59 File Offset: 0x0000A159
		public ScrollerButton()
		{
		}

		// Token: 0x06000359 RID: 857 RVA: 0x0000BF62 File Offset: 0x0000A162
		public ScrollerButton(Action clickEvent, long delay, long interval)
		{
			this.clickable = new Clickable(clickEvent, delay, interval);
			this.AddManipulator(this.clickable);
		}

		// Token: 0x04000150 RID: 336
		public Clickable clickable;

		// Token: 0x0200007B RID: 123
		public new class UxmlFactory : UxmlFactory<ScrollerButton, ScrollerButton.UxmlTraits>
		{
			// Token: 0x0600035A RID: 858 RVA: 0x0000BF85 File Offset: 0x0000A185
			public UxmlFactory()
			{
			}
		}

		// Token: 0x0200007C RID: 124
		public new class UxmlTraits : VisualElement.UxmlTraits
		{
			// Token: 0x0600035B RID: 859 RVA: 0x0000BF90 File Offset: 0x0000A190
			public UxmlTraits()
			{
			}

			// Token: 0x170000F1 RID: 241
			// (get) Token: 0x0600035C RID: 860 RVA: 0x0000BFD4 File Offset: 0x0000A1D4
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield break;
				}
			}

			// Token: 0x0600035D RID: 861 RVA: 0x0000BFF7 File Offset: 0x0000A1F7
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				((ScrollerButton)ve).clickable = new Clickable(null, this.m_Delay.GetValueFromBag(bag, cc), this.m_Interval.GetValueFromBag(bag, cc));
			}

			// Token: 0x04000151 RID: 337
			private UxmlLongAttributeDescription m_Delay = new UxmlLongAttributeDescription
			{
				name = "delay"
			};

			// Token: 0x04000152 RID: 338
			private UxmlLongAttributeDescription m_Interval = new UxmlLongAttributeDescription
			{
				name = "interval"
			};

			// Token: 0x0200007D RID: 125
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x0600035E RID: 862 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x0600035F RID: 863 RVA: 0x0000C02E File Offset: 0x0000A22E
				public bool MoveNext()
				{
					bool flag = this.$PC != 0;
					this.$PC = -1;
					if (!flag)
					{
					}
					return false;
				}

				// Token: 0x170000F2 RID: 242
				// (get) Token: 0x06000360 RID: 864 RVA: 0x0000C04C File Offset: 0x0000A24C
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x170000F3 RID: 243
				// (get) Token: 0x06000361 RID: 865 RVA: 0x0000C068 File Offset: 0x0000A268
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x06000362 RID: 866 RVA: 0x0000227E File Offset: 0x0000047E
				[DebuggerHidden]
				public void Dispose()
				{
				}

				// Token: 0x06000363 RID: 867 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x06000364 RID: 868 RVA: 0x0000C084 File Offset: 0x0000A284
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x06000365 RID: 869 RVA: 0x0000C09E File Offset: 0x0000A29E
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new ScrollerButton.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x04000153 RID: 339
				internal UxmlChildElementDescription $current;

				// Token: 0x04000154 RID: 340
				internal bool $disposing;

				// Token: 0x04000155 RID: 341
				internal int $PC;
			}
		}
	}
}
