﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using UnityEngine.Experimental.UIElements.StyleEnums;
using UnityEngine.Experimental.UIElements.StyleSheets;
using UnityEngine.StyleSheets;
using UnityEngine.Yoga;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000BF RID: 191
	public class VisualElement : Focusable, ITransform, IUIElementDataWatch, IEnumerable<VisualElement>, IVisualElementScheduler, IStyle, IEnumerable
	{
		// Token: 0x060004A2 RID: 1186 RVA: 0x0000F624 File Offset: 0x0000D824
		public VisualElement()
		{
			this.controlid = (VisualElement.s_NextId += 1U);
			this.shadow = new VisualElement.Hierarchy(this);
			this.m_ClassList = VisualElement.s_EmptyClassList;
			this.m_FullTypeName = string.Empty;
			this.m_TypeName = string.Empty;
			this.SetEnabled(true);
			this.focusIndex = VisualElement.defaultFocusIndex;
			this.name = string.Empty;
			this.yogaNode = new YogaNode(null);
		}

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x060004A3 RID: 1187 RVA: 0x0000F70C File Offset: 0x0000D90C
		// (set) Token: 0x060004A4 RID: 1188 RVA: 0x0000F727 File Offset: 0x0000D927
		public string persistenceKey
		{
			get
			{
				return this.m_PersistenceKey;
			}
			set
			{
				if (this.m_PersistenceKey != value)
				{
					this.m_PersistenceKey = value;
					if (!string.IsNullOrEmpty(value))
					{
						this.IncrementVersion(VersionChangeType.PersistentData);
					}
				}
			}
		}

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x060004A5 RID: 1189 RVA: 0x0000F75C File Offset: 0x0000D95C
		// (set) Token: 0x060004A6 RID: 1190 RVA: 0x0000F776 File Offset: 0x0000D976
		internal bool enablePersistence
		{
			[CompilerGenerated]
			get
			{
				return this.<enablePersistence>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<enablePersistence>k__BackingField = value;
			}
		}

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x060004A7 RID: 1191 RVA: 0x0000F780 File Offset: 0x0000D980
		// (set) Token: 0x060004A8 RID: 1192 RVA: 0x0000F79A File Offset: 0x0000D99A
		public object userData
		{
			[CompilerGenerated]
			get
			{
				return this.<userData>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<userData>k__BackingField = value;
			}
		}

		// Token: 0x17000169 RID: 361
		// (get) Token: 0x060004A9 RID: 1193 RVA: 0x0000F7A4 File Offset: 0x0000D9A4
		public override bool canGrabFocus
		{
			get
			{
				return this.visible && this.enabledInHierarchy && base.canGrabFocus;
			}
		}

		// Token: 0x1700016A RID: 362
		// (get) Token: 0x060004AA RID: 1194 RVA: 0x0000F7D8 File Offset: 0x0000D9D8
		public override FocusController focusController
		{
			get
			{
				return (this.panel != null) ? this.panel.focusController : null;
			}
		}

		// Token: 0x1700016B RID: 363
		// (get) Token: 0x060004AB RID: 1195 RVA: 0x0000F80C File Offset: 0x0000DA0C
		internal RenderData renderData
		{
			get
			{
				RenderData result;
				if ((result = this.m_RenderData) == null)
				{
					result = (this.m_RenderData = new RenderData());
				}
				return result;
			}
		}

		// Token: 0x1700016C RID: 364
		// (get) Token: 0x060004AC RID: 1196 RVA: 0x0000F83C File Offset: 0x0000DA3C
		public ITransform transform
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x060004AD RID: 1197 RVA: 0x0000F854 File Offset: 0x0000DA54
		// (set) Token: 0x060004AE RID: 1198 RVA: 0x0000F86F File Offset: 0x0000DA6F
		Vector3 ITransform.position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				if (!(this.m_Position == value))
				{
					this.m_Position = value;
					this.IncrementVersion(VersionChangeType.Transform);
				}
			}
		}

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x060004AF RID: 1199 RVA: 0x0000F898 File Offset: 0x0000DA98
		// (set) Token: 0x060004B0 RID: 1200 RVA: 0x0000F8B3 File Offset: 0x0000DAB3
		Quaternion ITransform.rotation
		{
			get
			{
				return this.m_Rotation;
			}
			set
			{
				if (!(this.m_Rotation == value))
				{
					this.m_Rotation = value;
					this.IncrementVersion(VersionChangeType.Transform);
				}
			}
		}

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x060004B1 RID: 1201 RVA: 0x0000F8DC File Offset: 0x0000DADC
		// (set) Token: 0x060004B2 RID: 1202 RVA: 0x0000F8F7 File Offset: 0x0000DAF7
		Vector3 ITransform.scale
		{
			get
			{
				return this.m_Scale;
			}
			set
			{
				if (!(this.m_Scale == value))
				{
					this.m_Scale = value;
					this.IncrementVersion(VersionChangeType.Transform);
					this.IncrementVersion(VersionChangeType.Layout);
				}
			}
		}

		// Token: 0x060004B3 RID: 1203 RVA: 0x0000F928 File Offset: 0x0000DB28
		internal Vector3 ComputeGlobalScale()
		{
			Vector3 scale = this.m_Scale;
			for (VisualElement parent = this.shadow.parent; parent != null; parent = parent.shadow.parent)
			{
				scale.Scale(parent.m_Scale);
			}
			return scale;
		}

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x060004B4 RID: 1204 RVA: 0x0000F980 File Offset: 0x0000DB80
		Matrix4x4 ITransform.matrix
		{
			get
			{
				return Matrix4x4.TRS(this.m_Position, this.m_Rotation, this.m_Scale);
			}
		}

		// Token: 0x1700016D RID: 365
		// (get) Token: 0x060004B5 RID: 1205 RVA: 0x0000F9AC File Offset: 0x0000DBAC
		// (set) Token: 0x060004B6 RID: 1206 RVA: 0x0000FA38 File Offset: 0x0000DC38
		public Rect layout
		{
			get
			{
				Rect layout = this.m_Layout;
				if (this.yogaNode != null && this.style.positionType.value != PositionType.Manual)
				{
					layout.x = this.yogaNode.LayoutX;
					layout.y = this.yogaNode.LayoutY;
					layout.width = this.yogaNode.LayoutWidth;
					layout.height = this.yogaNode.LayoutHeight;
				}
				return layout;
			}
			set
			{
				if (this.yogaNode == null)
				{
					this.yogaNode = new YogaNode(null);
				}
				if (this.style.positionType.value != PositionType.Manual || !(this.m_Layout == value))
				{
					this.m_Layout = value;
					((IStyle)this).positionType = PositionType.Manual;
					((IStyle)this).marginLeft = 0f;
					((IStyle)this).marginRight = 0f;
					((IStyle)this).marginBottom = 0f;
					((IStyle)this).marginTop = 0f;
					((IStyle)this).positionLeft = value.x;
					((IStyle)this).positionTop = value.y;
					((IStyle)this).positionRight = float.NaN;
					((IStyle)this).positionBottom = float.NaN;
					((IStyle)this).width = value.width;
					((IStyle)this).height = value.height;
					this.IncrementVersion(VersionChangeType.Transform);
				}
			}
		}

		// Token: 0x1700016E RID: 366
		// (get) Token: 0x060004B7 RID: 1207 RVA: 0x0000FB54 File Offset: 0x0000DD54
		public Rect contentRect
		{
			get
			{
				Spacing a = new Spacing(this.m_Style.paddingLeft, this.m_Style.paddingTop, this.m_Style.paddingRight, this.m_Style.paddingBottom);
				return this.paddingRect - a;
			}
		}

		// Token: 0x1700016F RID: 367
		// (get) Token: 0x060004B8 RID: 1208 RVA: 0x0000FBBC File Offset: 0x0000DDBC
		protected Rect paddingRect
		{
			get
			{
				Spacing a = new Spacing(this.style.borderLeftWidth, this.style.borderTopWidth, this.style.borderRightWidth, this.style.borderBottomWidth);
				return this.rect - a;
			}
		}

		// Token: 0x17000170 RID: 368
		// (get) Token: 0x060004B9 RID: 1209 RVA: 0x0000FC24 File Offset: 0x0000DE24
		public Rect worldBound
		{
			get
			{
				Matrix4x4 worldTransform = this.worldTransform;
				Vector3 vector = GUIUtility.Internal_MultiplyPoint(new Vector3(this.rect.min.x, this.rect.min.y, 1f), worldTransform);
				Vector3 vector2 = GUIUtility.Internal_MultiplyPoint(new Vector3(this.rect.max.x, this.rect.max.y, 1f), worldTransform);
				return Rect.MinMaxRect(Math.Min(vector.x, vector2.x), Math.Min(vector.y, vector2.y), Math.Max(vector.x, vector2.x), Math.Max(vector.y, vector2.y));
			}
		}

		// Token: 0x17000171 RID: 369
		// (get) Token: 0x060004BA RID: 1210 RVA: 0x0000FD14 File Offset: 0x0000DF14
		public Rect localBound
		{
			get
			{
				Matrix4x4 matrix = this.transform.matrix;
				Vector3 vector = GUIUtility.Internal_MultiplyPoint(this.layout.min, matrix);
				Vector3 vector2 = GUIUtility.Internal_MultiplyPoint(this.layout.max, matrix);
				return Rect.MinMaxRect(Math.Min(vector.x, vector2.x), Math.Min(vector.y, vector2.y), Math.Max(vector.x, vector2.x), Math.Max(vector.y, vector2.y));
			}
		}

		// Token: 0x17000172 RID: 370
		// (get) Token: 0x060004BB RID: 1211 RVA: 0x0000FDC0 File Offset: 0x0000DFC0
		internal Rect rect
		{
			get
			{
				return new Rect(0f, 0f, this.layout.width, this.layout.height);
			}
		}

		// Token: 0x17000173 RID: 371
		// (get) Token: 0x060004BC RID: 1212 RVA: 0x0000FE00 File Offset: 0x0000E000
		// (set) Token: 0x060004BD RID: 1213 RVA: 0x0000FE1A File Offset: 0x0000E01A
		internal bool isWorldTransformDirty
		{
			[CompilerGenerated]
			get
			{
				return this.<isWorldTransformDirty>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<isWorldTransformDirty>k__BackingField = value;
			}
		} = true;

		// Token: 0x17000174 RID: 372
		// (get) Token: 0x060004BE RID: 1214 RVA: 0x0000FE24 File Offset: 0x0000E024
		public Matrix4x4 worldTransform
		{
			get
			{
				if (this.isWorldTransformDirty)
				{
					this.UpdateWorldTransform();
					this.isWorldTransformDirty = false;
				}
				return this.m_WorldTransform;
			}
		}

		// Token: 0x060004BF RID: 1215 RVA: 0x0000FE5C File Offset: 0x0000E05C
		private void UpdateWorldTransform()
		{
			Matrix4x4 matrix4x = Matrix4x4.Translate(new Vector3(this.layout.x, this.layout.y, 0f));
			if (this.shadow.parent != null)
			{
				this.m_WorldTransform = this.shadow.parent.worldTransform * matrix4x * this.transform.matrix;
			}
			else
			{
				this.m_WorldTransform = matrix4x * this.transform.matrix;
			}
		}

		// Token: 0x17000175 RID: 373
		// (get) Token: 0x060004C0 RID: 1216 RVA: 0x0000FEFC File Offset: 0x0000E0FC
		// (set) Token: 0x060004C1 RID: 1217 RVA: 0x0000FF16 File Offset: 0x0000E116
		internal bool isWorldClipDirty
		{
			[CompilerGenerated]
			get
			{
				return this.<isWorldClipDirty>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<isWorldClipDirty>k__BackingField = value;
			}
		} = true;

		// Token: 0x17000176 RID: 374
		// (get) Token: 0x060004C2 RID: 1218 RVA: 0x0000FF20 File Offset: 0x0000E120
		internal Rect worldClip
		{
			get
			{
				if (this.isWorldClipDirty)
				{
					this.UpdateWorldClip();
					this.isWorldClipDirty = false;
				}
				return this.m_WorldClip;
			}
		}

		// Token: 0x060004C3 RID: 1219 RVA: 0x0000FF58 File Offset: 0x0000E158
		private void UpdateWorldClip()
		{
			if (this.shadow.parent != null)
			{
				this.m_WorldClip = this.shadow.parent.worldClip;
				if (this.ShouldClip())
				{
					Rect rect = VisualElement.ComputeAAAlignedBound(this.rect, this.worldTransform);
					float num = Mathf.Max(rect.x, this.m_WorldClip.x);
					float num2 = Mathf.Min(rect.x + rect.width, this.m_WorldClip.x + this.m_WorldClip.width);
					float num3 = Mathf.Max(rect.y, this.m_WorldClip.y);
					float num4 = Mathf.Min(rect.y + rect.height, this.m_WorldClip.y + this.m_WorldClip.height);
					this.m_WorldClip = new Rect(num, num3, num2 - num, num4 - num3);
				}
			}
			else
			{
				this.m_WorldClip = ((this.panel == null) ? GUIClip.topmostRect : this.panel.visualTree.rect);
			}
		}

		// Token: 0x060004C4 RID: 1220 RVA: 0x00010088 File Offset: 0x0000E288
		internal static Rect ComputeAAAlignedBound(Rect position, Matrix4x4 mat)
		{
			Rect rect = position;
			Vector3 vector = mat.MultiplyPoint3x4(new Vector3(rect.x, rect.y, 0f));
			Vector3 vector2 = mat.MultiplyPoint3x4(new Vector3(rect.x + rect.width, rect.y, 0f));
			Vector3 vector3 = mat.MultiplyPoint3x4(new Vector3(rect.x, rect.y + rect.height, 0f));
			Vector3 vector4 = mat.MultiplyPoint3x4(new Vector3(rect.x + rect.width, rect.y + rect.height, 0f));
			return Rect.MinMaxRect(Mathf.Min(vector.x, Mathf.Min(vector2.x, Mathf.Min(vector3.x, vector4.x))), Mathf.Min(vector.y, Mathf.Min(vector2.y, Mathf.Min(vector3.y, vector4.y))), Mathf.Max(vector.x, Mathf.Max(vector2.x, Mathf.Max(vector3.x, vector4.x))), Mathf.Max(vector.y, Mathf.Max(vector2.y, Mathf.Max(vector3.y, vector4.y))));
		}

		// Token: 0x17000177 RID: 375
		// (get) Token: 0x060004C5 RID: 1221 RVA: 0x000101F4 File Offset: 0x0000E3F4
		// (set) Token: 0x060004C6 RID: 1222 RVA: 0x00010210 File Offset: 0x0000E410
		internal PseudoStates pseudoStates
		{
			get
			{
				return this.m_PseudoStates;
			}
			set
			{
				if (this.m_PseudoStates != value)
				{
					this.m_PseudoStates = value;
					if ((this.triggerPseudoMask & this.m_PseudoStates) != (PseudoStates)0 || (this.dependencyPseudoMask & ~(this.m_PseudoStates != (PseudoStates)0)) != (PseudoStates)0)
					{
						this.IncrementVersion(VersionChangeType.StyleSheet);
					}
				}
			}
		}

		// Token: 0x17000178 RID: 376
		// (get) Token: 0x060004C7 RID: 1223 RVA: 0x00010264 File Offset: 0x0000E464
		// (set) Token: 0x060004C8 RID: 1224 RVA: 0x0001027E File Offset: 0x0000E47E
		public PickingMode pickingMode
		{
			[CompilerGenerated]
			get
			{
				return this.<pickingMode>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<pickingMode>k__BackingField = value;
			}
		}

		// Token: 0x17000179 RID: 377
		// (get) Token: 0x060004C9 RID: 1225 RVA: 0x00010288 File Offset: 0x0000E488
		// (set) Token: 0x060004CA RID: 1226 RVA: 0x000102A3 File Offset: 0x0000E4A3
		public string name
		{
			get
			{
				return this.m_Name;
			}
			set
			{
				if (!(this.m_Name == value))
				{
					this.m_Name = value;
					this.IncrementVersion(VersionChangeType.StyleSheet);
				}
			}
		}

		// Token: 0x1700017A RID: 378
		// (get) Token: 0x060004CB RID: 1227 RVA: 0x000102CC File Offset: 0x0000E4CC
		internal List<string> classList
		{
			get
			{
				return this.m_ClassList;
			}
		}

		// Token: 0x1700017B RID: 379
		// (get) Token: 0x060004CC RID: 1228 RVA: 0x000102E8 File Offset: 0x0000E4E8
		internal string fullTypeName
		{
			get
			{
				if (string.IsNullOrEmpty(this.m_FullTypeName))
				{
					this.m_FullTypeName = base.GetType().FullName;
				}
				return this.m_FullTypeName;
			}
		}

		// Token: 0x1700017C RID: 380
		// (get) Token: 0x060004CD RID: 1229 RVA: 0x00010324 File Offset: 0x0000E524
		internal string typeName
		{
			get
			{
				if (string.IsNullOrEmpty(this.m_TypeName))
				{
					Type type = base.GetType();
					bool isGenericType = type.IsGenericType;
					this.m_TypeName = ((!isGenericType) ? type.Name : type.Name.Remove(type.Name.IndexOf('`')));
				}
				return this.m_TypeName;
			}
		}

		// Token: 0x1700017D RID: 381
		// (get) Token: 0x060004CE RID: 1230 RVA: 0x00010390 File Offset: 0x0000E590
		// (set) Token: 0x060004CF RID: 1231 RVA: 0x000103AA File Offset: 0x0000E5AA
		internal YogaNode yogaNode
		{
			[CompilerGenerated]
			get
			{
				return this.<yogaNode>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<yogaNode>k__BackingField = value;
			}
		}

		// Token: 0x060004D0 RID: 1232 RVA: 0x000103B3 File Offset: 0x0000E5B3
		protected virtual void OnStyleResolved(ICustomStyle style)
		{
			this.FinalizeLayout();
		}

		// Token: 0x1700017E RID: 382
		// (get) Token: 0x060004D1 RID: 1233 RVA: 0x000103BC File Offset: 0x0000E5BC
		internal VisualElementStylesData sharedStyle
		{
			get
			{
				return this.m_SharedStyle;
			}
		}

		// Token: 0x1700017F RID: 383
		// (get) Token: 0x060004D2 RID: 1234 RVA: 0x000103D8 File Offset: 0x0000E5D8
		internal VisualElementStylesData effectiveStyle
		{
			get
			{
				return this.m_Style;
			}
		}

		// Token: 0x17000180 RID: 384
		// (get) Token: 0x060004D3 RID: 1235 RVA: 0x000103F4 File Offset: 0x0000E5F4
		internal bool hasInlineStyle
		{
			get
			{
				return this.m_Style != this.m_SharedStyle;
			}
		}

		// Token: 0x17000181 RID: 385
		// (get) Token: 0x060004D4 RID: 1236 RVA: 0x0001041C File Offset: 0x0000E61C
		private VisualElementStylesData inlineStyle
		{
			get
			{
				if (!this.hasInlineStyle)
				{
					VisualElementStylesData visualElementStylesData = new VisualElementStylesData(false);
					visualElementStylesData.Apply(this.m_SharedStyle, StylePropertyApplyMode.Copy);
					this.m_Style = visualElementStylesData;
				}
				return this.m_Style;
			}
		}

		// Token: 0x17000182 RID: 386
		// (get) Token: 0x060004D5 RID: 1237 RVA: 0x00010460 File Offset: 0x0000E660
		// (set) Token: 0x060004D6 RID: 1238 RVA: 0x00010488 File Offset: 0x0000E688
		internal float opacity
		{
			get
			{
				return this.style.opacity.value;
			}
			set
			{
				this.style.opacity = value;
			}
		}

		// Token: 0x060004D7 RID: 1239 RVA: 0x0001049C File Offset: 0x0000E69C
		protected internal override void ExecuteDefaultAction(EventBase evt)
		{
			base.ExecuteDefaultAction(evt);
			if (evt.GetEventTypeId() == EventBase<MouseOverEvent>.TypeId() || evt.GetEventTypeId() == EventBase<MouseOutEvent>.TypeId())
			{
				this.UpdateCursorStyle(evt.GetEventTypeId());
			}
			else if (evt.GetEventTypeId() == EventBase<MouseEnterEvent>.TypeId())
			{
				this.pseudoStates |= PseudoStates.Hover;
			}
			else if (evt.GetEventTypeId() == EventBase<MouseLeaveEvent>.TypeId())
			{
				this.pseudoStates &= ~PseudoStates.Hover;
			}
			else if (evt.GetEventTypeId() == EventBase<BlurEvent>.TypeId())
			{
				this.pseudoStates &= ~PseudoStates.Focus;
			}
			else if (evt.GetEventTypeId() == EventBase<FocusEvent>.TypeId())
			{
				this.pseudoStates |= PseudoStates.Focus;
			}
		}

		// Token: 0x060004D8 RID: 1240 RVA: 0x00010578 File Offset: 0x0000E778
		public sealed override void Focus()
		{
			if (!this.canGrabFocus && this.shadow.parent != null)
			{
				this.shadow.parent.Focus();
			}
			else
			{
				base.Focus();
			}
		}

		// Token: 0x060004D9 RID: 1241 RVA: 0x000105C8 File Offset: 0x0000E7C8
		internal void SetPanel(BaseVisualElementPanel p)
		{
			if (this.panel != p)
			{
				List<VisualElement> list = VisualElementListPool.Get(0);
				try
				{
					list.Add(this);
					this.GatherAllChildren(list);
					EventDispatcher.Gate? gate = null;
					if (((p != null) ? p.dispatcher : null) != null)
					{
						gate = new EventDispatcher.Gate?(new EventDispatcher.Gate(p.dispatcher));
					}
					EventDispatcher.Gate? gate2 = null;
					IPanel panel = this.panel;
					if (((panel != null) ? panel.dispatcher : null) != null && this.panel.dispatcher != ((p != null) ? p.dispatcher : null))
					{
						gate2 = new EventDispatcher.Gate?(new EventDispatcher.Gate(this.panel.dispatcher));
					}
					using (gate)
					{
						using (gate2)
						{
							foreach (VisualElement visualElement in list)
							{
								visualElement.ChangePanel(p);
							}
						}
					}
				}
				finally
				{
					VisualElementListPool.Release(list);
				}
			}
		}

		// Token: 0x060004DA RID: 1242 RVA: 0x0001078C File Offset: 0x0000E98C
		private void ChangePanel(BaseVisualElementPanel p)
		{
			if (this.panel != p)
			{
				if (this.panel != null)
				{
					using (DetachFromPanelEvent pooled = PanelChangedEventBase<DetachFromPanelEvent>.GetPooled(this.panel, p))
					{
						pooled.target = this;
						this.elementPanel.SendEvent(pooled, DispatchMode.Immediate);
					}
				}
				IPanel panel = this.panel;
				this.elementPanel = p;
				if (this.panel != null)
				{
					using (AttachToPanelEvent pooled2 = PanelChangedEventBase<AttachToPanelEvent>.GetPooled(panel, p))
					{
						pooled2.target = this;
						this.elementPanel.SendEvent(pooled2, DispatchMode.Default);
					}
				}
				this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.StyleSheet | VersionChangeType.Transform);
				if (!string.IsNullOrEmpty(this.persistenceKey))
				{
					this.IncrementVersion(VersionChangeType.PersistentData);
				}
			}
		}

		// Token: 0x060004DB RID: 1243 RVA: 0x00010878 File Offset: 0x0000EA78
		public sealed override void SendEvent(EventBase e)
		{
			BaseVisualElementPanel elementPanel = this.elementPanel;
			if (elementPanel != null)
			{
				elementPanel.SendEvent(e, DispatchMode.Default);
			}
		}

		// Token: 0x060004DC RID: 1244 RVA: 0x00010891 File Offset: 0x0000EA91
		internal void IncrementVersion(VersionChangeType changeType)
		{
			BaseVisualElementPanel elementPanel = this.elementPanel;
			if (elementPanel != null)
			{
				elementPanel.OnVersionChanged(this, changeType);
			}
		}

		// Token: 0x060004DD RID: 1245 RVA: 0x000108AA File Offset: 0x0000EAAA
		private void IncrementVersion(ChangeType changeType)
		{
			this.IncrementVersion(this.GetVersionChange(changeType));
		}

		// Token: 0x060004DE RID: 1246 RVA: 0x000108BC File Offset: 0x0000EABC
		private VersionChangeType GetVersionChange(ChangeType type)
		{
			VersionChangeType versionChangeType = (VersionChangeType)0;
			if ((type & (ChangeType.PersistentData | ChangeType.PersistentDataPath)) > (ChangeType)0)
			{
				versionChangeType |= VersionChangeType.PersistentData;
			}
			if ((type & ChangeType.Layout) == ChangeType.Layout)
			{
				versionChangeType |= VersionChangeType.Layout;
			}
			if ((type & (ChangeType.Styles | ChangeType.StylesPath)) > (ChangeType)0)
			{
				versionChangeType |= VersionChangeType.StyleSheet;
			}
			if ((type & ChangeType.Transform) == ChangeType.Transform)
			{
				versionChangeType |= VersionChangeType.Transform;
			}
			if ((type & ChangeType.Repaint) == ChangeType.Repaint)
			{
				versionChangeType |= VersionChangeType.Repaint;
			}
			return versionChangeType;
		}

		// Token: 0x060004DF RID: 1247 RVA: 0x00010929 File Offset: 0x0000EB29
		[Obsolete("Dirty is deprecated. Use MarkDirtyRepaint to trigger a new repaint of the VisualElement.")]
		public void Dirty(ChangeType type)
		{
			this.IncrementVersion(type);
		}

		// Token: 0x060004E0 RID: 1248 RVA: 0x00010934 File Offset: 0x0000EB34
		[Obsolete("IsDirty is deprecated. Avoid using it, will always return false.")]
		public bool IsDirty(ChangeType type)
		{
			return false;
		}

		// Token: 0x060004E1 RID: 1249 RVA: 0x0001094C File Offset: 0x0000EB4C
		[Obsolete("AnyDirty is deprecated. Avoid using it, will always return false.")]
		public bool AnyDirty(ChangeType type)
		{
			return false;
		}

		// Token: 0x060004E2 RID: 1250 RVA: 0x000035A6 File Offset: 0x000017A6
		[Obsolete("ClearDirty is deprecated. Avoid using it, it's now a no-op.")]
		public void ClearDirty(ChangeType type)
		{
		}

		// Token: 0x17000183 RID: 387
		// (get) Token: 0x060004E3 RID: 1251 RVA: 0x00010964 File Offset: 0x0000EB64
		// (set) Token: 0x060004E4 RID: 1252 RVA: 0x0001097F File Offset: 0x0000EB7F
		[Obsolete("enabled is deprecated. Use SetEnabled as setter, and enabledSelf/enabledInHierarchy as getters.")]
		public virtual bool enabled
		{
			get
			{
				return this.enabledInHierarchy;
			}
			set
			{
				this.SetEnabled(value);
			}
		}

		// Token: 0x060004E5 RID: 1253 RVA: 0x0001098C File Offset: 0x0000EB8C
		protected internal bool SetEnabledFromHierarchy(bool state)
		{
			bool result;
			if (state == ((this.pseudoStates & PseudoStates.Disabled) != PseudoStates.Disabled))
			{
				result = false;
			}
			else
			{
				if (state && this.m_Enabled && (this.parent == null || this.parent.enabledInHierarchy))
				{
					this.pseudoStates &= ~PseudoStates.Disabled;
				}
				else
				{
					this.pseudoStates |= PseudoStates.Disabled;
				}
				result = true;
			}
			return result;
		}

		// Token: 0x17000184 RID: 388
		// (get) Token: 0x060004E6 RID: 1254 RVA: 0x00010A10 File Offset: 0x0000EC10
		public bool enabledInHierarchy
		{
			get
			{
				return (this.pseudoStates & PseudoStates.Disabled) != PseudoStates.Disabled;
			}
		}

		// Token: 0x17000185 RID: 389
		// (get) Token: 0x060004E7 RID: 1255 RVA: 0x00010A38 File Offset: 0x0000EC38
		public bool enabledSelf
		{
			get
			{
				return this.m_Enabled;
			}
		}

		// Token: 0x060004E8 RID: 1256 RVA: 0x00010A53 File Offset: 0x0000EC53
		public void SetEnabled(bool value)
		{
			if (this.m_Enabled != value)
			{
				this.m_Enabled = value;
				this.PropagateEnabledToChildren(value);
			}
		}

		// Token: 0x060004E9 RID: 1257 RVA: 0x00010A74 File Offset: 0x0000EC74
		private void PropagateEnabledToChildren(bool value)
		{
			if (this.SetEnabledFromHierarchy(value))
			{
				for (int i = 0; i < this.shadow.childCount; i++)
				{
					this.shadow[i].PropagateEnabledToChildren(value);
				}
			}
		}

		// Token: 0x17000186 RID: 390
		// (get) Token: 0x060004EA RID: 1258 RVA: 0x00010AC8 File Offset: 0x0000ECC8
		// (set) Token: 0x060004EB RID: 1259 RVA: 0x00010AF4 File Offset: 0x0000ECF4
		public bool visible
		{
			get
			{
				return this.style.visibility.GetSpecifiedValueOrDefault(Visibility.Visible) == Visibility.Visible;
			}
			set
			{
				this.style.visibility = ((!value) ? Visibility.Hidden : Visibility.Visible);
			}
		}

		// Token: 0x060004EC RID: 1260 RVA: 0x00010B14 File Offset: 0x0000ED14
		public void MarkDirtyRepaint()
		{
			this.IncrementVersion(VersionChangeType.Repaint);
		}

		// Token: 0x060004ED RID: 1261 RVA: 0x00010B20 File Offset: 0x0000ED20
		internal void Repaint(IStylePainter painter)
		{
			if (this.visible)
			{
				IStylePainterInternal stylePainterInternal = (IStylePainterInternal)painter;
				stylePainterInternal.DrawBackground();
				this.DoRepaint(stylePainterInternal);
				stylePainterInternal.DrawBorder();
			}
		}

		// Token: 0x060004EE RID: 1262 RVA: 0x000035A6 File Offset: 0x000017A6
		protected virtual void DoRepaint(IStylePainter painter)
		{
		}

		// Token: 0x060004EF RID: 1263 RVA: 0x00010B5C File Offset: 0x0000ED5C
		private void GetFullHierarchicalPersistenceKey(StringBuilder key)
		{
			if (this.parent != null)
			{
				this.parent.GetFullHierarchicalPersistenceKey(key);
			}
			if (!string.IsNullOrEmpty(this.persistenceKey))
			{
				key.Append("__");
				key.Append(this.persistenceKey);
			}
		}

		// Token: 0x060004F0 RID: 1264 RVA: 0x00010BAC File Offset: 0x0000EDAC
		public string GetFullHierarchicalPersistenceKey()
		{
			StringBuilder stringBuilder = new StringBuilder();
			this.GetFullHierarchicalPersistenceKey(stringBuilder);
			return stringBuilder.ToString();
		}

		// Token: 0x060004F1 RID: 1265 RVA: 0x00010BD4 File Offset: 0x0000EDD4
		public T GetOrCreatePersistentData<T>(object existing, string key) where T : class, new()
		{
			Debug.Assert(this.elementPanel != null, "VisualElement.elementPanel is null! Cannot load persistent data.");
			ISerializableJsonDictionary serializableJsonDictionary = (this.elementPanel != null && this.elementPanel.getViewDataDictionary != null) ? this.elementPanel.getViewDataDictionary() : null;
			T result;
			if (serializableJsonDictionary == null || string.IsNullOrEmpty(this.persistenceKey) || !this.enablePersistence)
			{
				if (existing != null)
				{
					result = (existing as T);
				}
				else
				{
					result = Activator.CreateInstance<T>();
				}
			}
			else
			{
				string key2 = key + "__" + typeof(T).ToString();
				if (!serializableJsonDictionary.ContainsKey(key2))
				{
					serializableJsonDictionary.Set<T>(key2, Activator.CreateInstance<T>());
				}
				result = serializableJsonDictionary.Get<T>(key2);
			}
			return result;
		}

		// Token: 0x060004F2 RID: 1266 RVA: 0x00010CB0 File Offset: 0x0000EEB0
		public T GetOrCreatePersistentData<T>(ScriptableObject existing, string key) where T : ScriptableObject
		{
			Debug.Assert(this.elementPanel != null, "VisualElement.elementPanel is null! Cannot load persistent data.");
			ISerializableJsonDictionary serializableJsonDictionary = (this.elementPanel != null && this.elementPanel.getViewDataDictionary != null) ? this.elementPanel.getViewDataDictionary() : null;
			T result;
			if (serializableJsonDictionary == null || string.IsNullOrEmpty(this.persistenceKey) || !this.enablePersistence)
			{
				if (existing != null)
				{
					result = (existing as T);
				}
				else
				{
					result = ScriptableObject.CreateInstance<T>();
				}
			}
			else
			{
				string key2 = key + "__" + typeof(T).ToString();
				if (!serializableJsonDictionary.ContainsKey(key2))
				{
					serializableJsonDictionary.Set<T>(key2, ScriptableObject.CreateInstance<T>());
				}
				result = serializableJsonDictionary.GetScriptable<T>(key2);
			}
			return result;
		}

		// Token: 0x060004F3 RID: 1267 RVA: 0x00010D94 File Offset: 0x0000EF94
		public void OverwriteFromPersistedData(object obj, string key)
		{
			Debug.Assert(this.elementPanel != null, "VisualElement.elementPanel is null! Cannot load persistent data.");
			ISerializableJsonDictionary serializableJsonDictionary = (this.elementPanel != null && this.elementPanel.getViewDataDictionary != null) ? this.elementPanel.getViewDataDictionary() : null;
			if (serializableJsonDictionary != null && !string.IsNullOrEmpty(this.persistenceKey) && this.enablePersistence)
			{
				string key2 = key + "__" + obj.GetType();
				if (!serializableJsonDictionary.ContainsKey(key2))
				{
					serializableJsonDictionary.Set<object>(key2, obj);
				}
				else
				{
					serializableJsonDictionary.Overwrite(obj, key2);
				}
			}
		}

		// Token: 0x060004F4 RID: 1268 RVA: 0x00010E45 File Offset: 0x0000F045
		public void SavePersistentData()
		{
			if (this.elementPanel != null && this.elementPanel.savePersistentViewData != null && !string.IsNullOrEmpty(this.persistenceKey))
			{
				this.elementPanel.savePersistentViewData();
			}
		}

		// Token: 0x060004F5 RID: 1269 RVA: 0x00010E84 File Offset: 0x0000F084
		internal bool IsPersitenceSupportedOnChildren()
		{
			return base.GetType() == typeof(VisualElement) || !string.IsNullOrEmpty(this.persistenceKey);
		}

		// Token: 0x060004F6 RID: 1270 RVA: 0x00010ECD File Offset: 0x0000F0CD
		internal void OnPersistentDataReady(bool enablePersistence)
		{
			this.enablePersistence = enablePersistence;
			this.OnPersistentDataReady();
		}

		// Token: 0x060004F7 RID: 1271 RVA: 0x000035A6 File Offset: 0x000017A6
		public virtual void OnPersistentDataReady()
		{
		}

		// Token: 0x060004F8 RID: 1272 RVA: 0x00010EE0 File Offset: 0x0000F0E0
		public virtual bool ContainsPoint(Vector2 localPoint)
		{
			return this.rect.Contains(localPoint);
		}

		// Token: 0x060004F9 RID: 1273 RVA: 0x00010F04 File Offset: 0x0000F104
		public virtual bool Overlaps(Rect rectangle)
		{
			return this.rect.Overlaps(rectangle, true);
		}

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x060004FA RID: 1274 RVA: 0x00010F2C File Offset: 0x0000F12C
		// (set) Token: 0x060004FB RID: 1275 RVA: 0x00010F48 File Offset: 0x0000F148
		internal bool requireMeasureFunction
		{
			get
			{
				return this.m_RequireMeasureFunction;
			}
			set
			{
				this.m_RequireMeasureFunction = value;
				if (this.m_RequireMeasureFunction && !this.yogaNode.IsMeasureDefined)
				{
					this.yogaNode.SetMeasureFunction(new MeasureFunction(this.Measure));
				}
				else if (!this.m_RequireMeasureFunction && this.yogaNode.IsMeasureDefined)
				{
					this.yogaNode.SetMeasureFunction(null);
				}
			}
		}

		// Token: 0x060004FC RID: 1276 RVA: 0x00010FC0 File Offset: 0x0000F1C0
		protected internal virtual Vector2 DoMeasure(float width, VisualElement.MeasureMode widthMode, float height, VisualElement.MeasureMode heightMode)
		{
			return new Vector2(float.NaN, float.NaN);
		}

		// Token: 0x060004FD RID: 1277 RVA: 0x00010FE4 File Offset: 0x0000F1E4
		internal YogaSize Measure(YogaNode node, float width, YogaMeasureMode widthMode, float height, YogaMeasureMode heightMode)
		{
			Debug.Assert(node == this.yogaNode, "YogaNode instance mismatch");
			Vector2 vector = this.DoMeasure(width, (VisualElement.MeasureMode)widthMode, height, (VisualElement.MeasureMode)heightMode);
			return MeasureOutput.Make((float)Mathf.RoundToInt(vector.x), (float)Mathf.RoundToInt(vector.y));
		}

		// Token: 0x060004FE RID: 1278 RVA: 0x00011038 File Offset: 0x0000F238
		public void SetSize(Vector2 size)
		{
			Rect layout = this.layout;
			layout.width = size.x;
			layout.height = size.y;
			this.layout = layout;
		}

		// Token: 0x060004FF RID: 1279 RVA: 0x00011070 File Offset: 0x0000F270
		private void FinalizeLayout()
		{
			if (this.hasInlineStyle)
			{
				this.effectiveStyle.SyncWithLayout(this.yogaNode);
			}
			else
			{
				this.yogaNode.CopyStyle(this.effectiveStyle.yogaNode);
			}
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x06000500 RID: 1280 RVA: 0x000110B0 File Offset: 0x0000F2B0
		// (remove) Token: 0x06000501 RID: 1281 RVA: 0x000110E8 File Offset: 0x0000F2E8
		internal event OnStylesResolved onStylesResolved
		{
			add
			{
				OnStylesResolved onStylesResolved = this.onStylesResolved;
				OnStylesResolved onStylesResolved2;
				do
				{
					onStylesResolved2 = onStylesResolved;
					onStylesResolved = Interlocked.CompareExchange<OnStylesResolved>(ref this.onStylesResolved, (OnStylesResolved)Delegate.Combine(onStylesResolved2, value), onStylesResolved);
				}
				while (onStylesResolved != onStylesResolved2);
			}
			remove
			{
				OnStylesResolved onStylesResolved = this.onStylesResolved;
				OnStylesResolved onStylesResolved2;
				do
				{
					onStylesResolved2 = onStylesResolved;
					onStylesResolved = Interlocked.CompareExchange<OnStylesResolved>(ref this.onStylesResolved, (OnStylesResolved)Delegate.Remove(onStylesResolved2, value), onStylesResolved);
				}
				while (onStylesResolved != onStylesResolved2);
			}
		}

		// Token: 0x06000502 RID: 1282 RVA: 0x0001111E File Offset: 0x0000F31E
		internal void SetInlineStyles(VisualElementStylesData inlineStyle)
		{
			Debug.Assert(!inlineStyle.isShared);
			inlineStyle.Apply(this.m_Style, StylePropertyApplyMode.CopyIfEqualOrGreaterSpecificity);
			this.m_Style = inlineStyle;
		}

		// Token: 0x06000503 RID: 1283 RVA: 0x00011144 File Offset: 0x0000F344
		internal void SetSharedStyles(VisualElementStylesData sharedStyle)
		{
			Debug.Assert(sharedStyle.isShared);
			if (sharedStyle != this.m_SharedStyle)
			{
				if (this.hasInlineStyle)
				{
					this.m_Style.Apply(sharedStyle, StylePropertyApplyMode.CopyIfNotInline);
				}
				else
				{
					this.m_Style = sharedStyle;
				}
				this.m_SharedStyle = sharedStyle;
				if (this.onStylesResolved != null)
				{
					this.onStylesResolved(this.m_Style);
				}
				this.OnStyleResolved(this.m_Style);
				this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles | VersionChangeType.Repaint);
			}
		}

		// Token: 0x06000504 RID: 1284 RVA: 0x000111D0 File Offset: 0x0000F3D0
		public void ResetPositionProperties()
		{
			if (this.hasInlineStyle)
			{
				VisualElementStylesData inlineStyle = this.inlineStyle;
				inlineStyle.positionType = StyleValue<int>.nil;
				inlineStyle.marginLeft = StyleValue<float>.nil;
				inlineStyle.marginRight = StyleValue<float>.nil;
				inlineStyle.marginBottom = StyleValue<float>.nil;
				inlineStyle.marginTop = StyleValue<float>.nil;
				inlineStyle.positionLeft = StyleValue<float>.nil;
				inlineStyle.positionTop = StyleValue<float>.nil;
				inlineStyle.positionRight = StyleValue<float>.nil;
				inlineStyle.positionBottom = StyleValue<float>.nil;
				inlineStyle.width = StyleValue<float>.nil;
				inlineStyle.height = StyleValue<float>.nil;
				this.m_Style.Apply(this.sharedStyle, StylePropertyApplyMode.CopyIfNotInline);
				this.FinalizeLayout();
				this.IncrementVersion(VersionChangeType.Layout);
			}
		}

		// Token: 0x06000505 RID: 1285 RVA: 0x00011290 File Offset: 0x0000F490
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				base.GetType().Name,
				" ",
				this.name,
				" ",
				this.layout,
				" world rect: ",
				this.worldBound
			});
		}

		// Token: 0x06000506 RID: 1286 RVA: 0x000112FC File Offset: 0x0000F4FC
		internal IEnumerable<string> GetClasses()
		{
			return this.m_ClassList;
		}

		// Token: 0x06000507 RID: 1287 RVA: 0x00011317 File Offset: 0x0000F517
		public void ClearClassList()
		{
			if (this.m_ClassList.Count > 0)
			{
				this.m_ClassList = VisualElement.s_EmptyClassList;
				this.IncrementVersion(VersionChangeType.StyleSheet);
			}
		}

		// Token: 0x06000508 RID: 1288 RVA: 0x00011340 File Offset: 0x0000F540
		public void AddToClassList(string className)
		{
			if (this.m_ClassList == VisualElement.s_EmptyClassList)
			{
				this.m_ClassList = new List<string>
				{
					className
				};
			}
			else
			{
				if (this.m_ClassList.Contains(className))
				{
					return;
				}
				this.m_ClassList.Capacity++;
				this.m_ClassList.Add(className);
			}
			this.IncrementVersion(VersionChangeType.StyleSheet);
		}

		// Token: 0x06000509 RID: 1289 RVA: 0x000113B9 File Offset: 0x0000F5B9
		public void RemoveFromClassList(string className)
		{
			if (this.m_ClassList.Remove(className))
			{
				this.IncrementVersion(VersionChangeType.StyleSheet);
			}
		}

		// Token: 0x0600050A RID: 1290 RVA: 0x000113D7 File Offset: 0x0000F5D7
		public void ToggleInClassList(string className)
		{
			if (this.ClassListContains(className))
			{
				this.RemoveFromClassList(className);
			}
			else
			{
				this.AddToClassList(className);
			}
		}

		// Token: 0x0600050B RID: 1291 RVA: 0x000113F9 File Offset: 0x0000F5F9
		public void EnableInClassList(string className, bool enable)
		{
			if (enable)
			{
				this.AddToClassList(className);
			}
			else
			{
				this.RemoveFromClassList(className);
			}
		}

		// Token: 0x0600050C RID: 1292 RVA: 0x00011418 File Offset: 0x0000F618
		public bool ClassListContains(string cls)
		{
			for (int i = 0; i < this.m_ClassList.Count; i++)
			{
				if (this.m_ClassList[i] == cls)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600050D RID: 1293 RVA: 0x0001146C File Offset: 0x0000F66C
		public object FindAncestorUserData()
		{
			for (VisualElement parent = this.parent; parent != null; parent = parent.parent)
			{
				if (parent.userData != null)
				{
					return parent.userData;
				}
			}
			return null;
		}

		// Token: 0x0600050E RID: 1294 RVA: 0x000114B4 File Offset: 0x0000F6B4
		private void UpdateCursorStyle(long eventType)
		{
			if (this.elementPanel != null)
			{
				if (eventType == EventBase<MouseOverEvent>.TypeId())
				{
					this.elementPanel.cursorManager.SetCursor(this.style.cursor.value);
				}
				else
				{
					this.elementPanel.cursorManager.ResetCursor();
				}
			}
		}

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x0600050F RID: 1295 RVA: 0x00011518 File Offset: 0x0000F718
		public IUIElementDataWatch dataWatch
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06000510 RID: 1296 RVA: 0x00011530 File Offset: 0x0000F730
		IUIElementDataWatchRequest IUIElementDataWatch.RegisterWatch(Object toWatch, Action<Object> watchNotification)
		{
			VisualElement.DataWatchRequest dataWatchRequest = new VisualElement.DataWatchRequest(this)
			{
				notification = watchNotification,
				watchedObject = toWatch
			};
			dataWatchRequest.Start();
			return dataWatchRequest;
		}

		// Token: 0x06000511 RID: 1297 RVA: 0x00011564 File Offset: 0x0000F764
		void IUIElementDataWatch.UnregisterWatch(IUIElementDataWatchRequest requested)
		{
			VisualElement.DataWatchRequest dataWatchRequest = requested as VisualElement.DataWatchRequest;
			if (dataWatchRequest != null)
			{
				dataWatchRequest.Stop();
			}
		}

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06000512 RID: 1298 RVA: 0x00011588 File Offset: 0x0000F788
		// (set) Token: 0x06000513 RID: 1299 RVA: 0x000115A2 File Offset: 0x0000F7A2
		public VisualElement.Hierarchy shadow
		{
			[CompilerGenerated]
			get
			{
				return this.<shadow>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<shadow>k__BackingField = value;
			}
		}

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x06000514 RID: 1300 RVA: 0x000115AC File Offset: 0x0000F7AC
		// (set) Token: 0x06000515 RID: 1301 RVA: 0x000115C7 File Offset: 0x0000F7C7
		public VisualElement.ClippingOptions clippingOptions
		{
			get
			{
				return this.m_ClippingOptions;
			}
			set
			{
				if (this.m_ClippingOptions != value)
				{
					this.m_ClippingOptions = value;
					this.IncrementVersion(VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x06000516 RID: 1302 RVA: 0x000115E8 File Offset: 0x0000F7E8
		internal bool ShouldClip()
		{
			return this.style.overflow != Overflow.Visible || this.clippingOptions != VisualElement.ClippingOptions.NoClipping;
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000517 RID: 1303 RVA: 0x00011624 File Offset: 0x0000F824
		public VisualElement parent
		{
			get
			{
				return this.m_LogicalParent;
			}
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000518 RID: 1304 RVA: 0x00011640 File Offset: 0x0000F840
		// (set) Token: 0x06000519 RID: 1305 RVA: 0x0001165A File Offset: 0x0000F85A
		internal BaseVisualElementPanel elementPanel
		{
			[CompilerGenerated]
			get
			{
				return this.<elementPanel>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<elementPanel>k__BackingField = value;
			}
		}

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x0600051A RID: 1306 RVA: 0x00011664 File Offset: 0x0000F864
		public IPanel panel
		{
			get
			{
				return this.elementPanel;
			}
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x0600051B RID: 1307 RVA: 0x00011680 File Offset: 0x0000F880
		public virtual VisualElement contentContainer
		{
			get
			{
				return this;
			}
		}

		// Token: 0x0600051C RID: 1308 RVA: 0x00011698 File Offset: 0x0000F898
		public void Add(VisualElement child)
		{
			if (this.contentContainer == this)
			{
				this.shadow.Add(child);
			}
			else
			{
				VisualElement contentContainer = this.contentContainer;
				if (contentContainer != null)
				{
					contentContainer.Add(child);
				}
			}
			child.m_LogicalParent = this;
		}

		// Token: 0x0600051D RID: 1309 RVA: 0x000116E8 File Offset: 0x0000F8E8
		public void Insert(int index, VisualElement element)
		{
			if (this.contentContainer == this)
			{
				this.shadow.Insert(index, element);
			}
			else
			{
				VisualElement contentContainer = this.contentContainer;
				if (contentContainer != null)
				{
					contentContainer.Insert(index, element);
				}
			}
			element.m_LogicalParent = this;
		}

		// Token: 0x0600051E RID: 1310 RVA: 0x00011738 File Offset: 0x0000F938
		public void Remove(VisualElement element)
		{
			if (this.contentContainer == this)
			{
				this.shadow.Remove(element);
			}
			else
			{
				VisualElement contentContainer = this.contentContainer;
				if (contentContainer != null)
				{
					contentContainer.Remove(element);
				}
			}
		}

		// Token: 0x0600051F RID: 1311 RVA: 0x00011780 File Offset: 0x0000F980
		public void RemoveAt(int index)
		{
			if (this.contentContainer == this)
			{
				this.shadow.RemoveAt(index);
			}
			else
			{
				VisualElement contentContainer = this.contentContainer;
				if (contentContainer != null)
				{
					contentContainer.RemoveAt(index);
				}
			}
		}

		// Token: 0x06000520 RID: 1312 RVA: 0x000117C8 File Offset: 0x0000F9C8
		public void Clear()
		{
			if (this.contentContainer == this)
			{
				this.shadow.Clear();
			}
			else
			{
				VisualElement contentContainer = this.contentContainer;
				if (contentContainer != null)
				{
					contentContainer.Clear();
				}
			}
		}

		// Token: 0x06000521 RID: 1313 RVA: 0x00011810 File Offset: 0x0000FA10
		public VisualElement ElementAt(int index)
		{
			VisualElement result;
			if (this.contentContainer == this)
			{
				result = this.shadow.ElementAt(index);
			}
			else
			{
				VisualElement contentContainer = this.contentContainer;
				result = ((contentContainer != null) ? contentContainer.ElementAt(index) : null);
			}
			return result;
		}

		// Token: 0x1700018F RID: 399
		public VisualElement this[int key]
		{
			get
			{
				return this.ElementAt(key);
			}
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x06000523 RID: 1315 RVA: 0x0001187C File Offset: 0x0000FA7C
		public int childCount
		{
			get
			{
				int result;
				if (this.contentContainer == this)
				{
					result = this.shadow.childCount;
				}
				else
				{
					VisualElement contentContainer = this.contentContainer;
					int? num = (contentContainer != null) ? new int?(contentContainer.childCount) : null;
					result = ((num == null) ? 0 : num.Value);
				}
				return result;
			}
		}

		// Token: 0x06000524 RID: 1316 RVA: 0x000118F0 File Offset: 0x0000FAF0
		public int IndexOf(VisualElement element)
		{
			int result;
			if (this.contentContainer == this)
			{
				result = this.shadow.IndexOf(element);
			}
			else
			{
				VisualElement contentContainer = this.contentContainer;
				int? num = (contentContainer != null) ? new int?(contentContainer.IndexOf(element)) : null;
				result = ((num == null) ? -1 : num.Value);
			}
			return result;
		}

		// Token: 0x06000525 RID: 1317 RVA: 0x00011964 File Offset: 0x0000FB64
		public IEnumerable<VisualElement> Children()
		{
			IEnumerable<VisualElement> result;
			if (this.contentContainer == this)
			{
				result = this.shadow.Children();
			}
			else
			{
				VisualElement contentContainer = this.contentContainer;
				result = (((contentContainer != null) ? contentContainer.Children() : null) ?? VisualElement.s_EmptyList);
			}
			return result;
		}

		// Token: 0x06000526 RID: 1318 RVA: 0x000119BC File Offset: 0x0000FBBC
		public void Sort(Comparison<VisualElement> comp)
		{
			if (this.contentContainer == this)
			{
				this.shadow.Sort(comp);
			}
			else
			{
				VisualElement contentContainer = this.contentContainer;
				if (contentContainer != null)
				{
					contentContainer.Sort(comp);
				}
			}
		}

		// Token: 0x06000527 RID: 1319 RVA: 0x00011A04 File Offset: 0x0000FC04
		public void BringToFront()
		{
			if (this.shadow.parent != null)
			{
				this.shadow.parent.shadow.BringToFront(this);
			}
		}

		// Token: 0x06000528 RID: 1320 RVA: 0x00011A48 File Offset: 0x0000FC48
		public void SendToBack()
		{
			if (this.shadow.parent != null)
			{
				this.shadow.parent.shadow.SendToBack(this);
			}
		}

		// Token: 0x06000529 RID: 1321 RVA: 0x00011A8C File Offset: 0x0000FC8C
		public void PlaceBehind(VisualElement sibling)
		{
			if (this.shadow.parent == null || sibling.shadow.parent != this.shadow.parent)
			{
				throw new ArgumentException("VisualElements are not siblings");
			}
			this.shadow.parent.shadow.PlaceBehind(this, sibling);
		}

		// Token: 0x0600052A RID: 1322 RVA: 0x00011AF8 File Offset: 0x0000FCF8
		public void PlaceInFront(VisualElement sibling)
		{
			if (this.shadow.parent == null || sibling.shadow.parent != this.shadow.parent)
			{
				throw new ArgumentException("VisualElements are not siblings");
			}
			this.shadow.parent.shadow.PlaceInFront(this, sibling);
		}

		// Token: 0x0600052B RID: 1323 RVA: 0x00011B64 File Offset: 0x0000FD64
		public void RemoveFromHierarchy()
		{
			if (this.shadow.parent != null)
			{
				this.shadow.parent.shadow.Remove(this);
			}
		}

		// Token: 0x0600052C RID: 1324 RVA: 0x00011BA4 File Offset: 0x0000FDA4
		public T GetFirstOfType<T>() where T : class
		{
			T t = this as T;
			T result;
			if (t != null)
			{
				result = t;
			}
			else
			{
				result = this.GetFirstAncestorOfType<T>();
			}
			return result;
		}

		// Token: 0x0600052D RID: 1325 RVA: 0x00011BE4 File Offset: 0x0000FDE4
		public T GetFirstAncestorOfType<T>() where T : class
		{
			for (VisualElement parent = this.shadow.parent; parent != null; parent = parent.shadow.parent)
			{
				T t = parent as T;
				if (t != null)
				{
					return t;
				}
			}
			return (T)((object)null);
		}

		// Token: 0x0600052E RID: 1326 RVA: 0x00011C50 File Offset: 0x0000FE50
		public bool Contains(VisualElement child)
		{
			while (child != null)
			{
				if (child.shadow.parent == this)
				{
					return true;
				}
				child = child.shadow.parent;
			}
			return false;
		}

		// Token: 0x0600052F RID: 1327 RVA: 0x00011CA0 File Offset: 0x0000FEA0
		private void GatherAllChildren(List<VisualElement> elements)
		{
			if (this.m_Children != null && this.m_Children.Count > 0)
			{
				int i = elements.Count;
				elements.AddRange(this.m_Children);
				while (i < elements.Count)
				{
					VisualElement visualElement = elements[i];
					if (visualElement.m_Children != null && visualElement.m_Children.Count > 0)
					{
						elements.AddRange(visualElement.m_Children);
					}
					i++;
				}
			}
		}

		// Token: 0x06000530 RID: 1328 RVA: 0x00011D28 File Offset: 0x0000FF28
		public VisualElement FindCommonAncestor(VisualElement other)
		{
			VisualElement result;
			if (this.panel != other.panel)
			{
				result = null;
			}
			else
			{
				VisualElement visualElement = this;
				int i = 0;
				while (visualElement != null)
				{
					i++;
					visualElement = visualElement.shadow.parent;
				}
				VisualElement visualElement2 = other;
				int j = 0;
				while (visualElement2 != null)
				{
					j++;
					visualElement2 = visualElement2.shadow.parent;
				}
				visualElement = this;
				visualElement2 = other;
				while (i > j)
				{
					i--;
					visualElement = visualElement.shadow.parent;
				}
				while (j > i)
				{
					j--;
					visualElement2 = visualElement2.shadow.parent;
				}
				while (visualElement != visualElement2)
				{
					visualElement = visualElement.shadow.parent;
					visualElement2 = visualElement2.shadow.parent;
				}
				result = visualElement;
			}
			return result;
		}

		// Token: 0x06000531 RID: 1329 RVA: 0x00011E28 File Offset: 0x00010028
		public IEnumerator<VisualElement> GetEnumerator()
		{
			IEnumerator<VisualElement> result;
			if (this.contentContainer == this)
			{
				result = this.shadow.Children().GetEnumerator();
			}
			else
			{
				IEnumerator<VisualElement> enumerator;
				if (this.contentContainer != null)
				{
					enumerator = this.contentContainer.GetEnumerator();
				}
				else
				{
					enumerator = (IEnumerator<VisualElement>)VisualElement.s_EmptyList.GetEnumerator();
				}
				result = enumerator;
			}
			return result;
		}

		// Token: 0x06000532 RID: 1330 RVA: 0x00011E94 File Offset: 0x00010094
		IEnumerator IEnumerable.GetEnumerator()
		{
			IEnumerator result;
			if (this.contentContainer == this)
			{
				result = this.shadow.Children().GetEnumerator();
			}
			else
			{
				IEnumerator enumerator;
				if (this.contentContainer != null)
				{
					enumerator = ((IEnumerable)this.contentContainer).GetEnumerator();
				}
				else
				{
					enumerator = VisualElement.s_EmptyList.GetEnumerator();
				}
				result = enumerator;
			}
			return result;
		}

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x06000533 RID: 1331 RVA: 0x00011EFC File Offset: 0x000100FC
		public IVisualElementScheduler schedule
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06000534 RID: 1332 RVA: 0x00011F14 File Offset: 0x00010114
		IVisualElementScheduledItem IVisualElementScheduler.Execute(Action<TimerState> timerUpdateEvent)
		{
			VisualElement.TimerStateScheduledItem timerStateScheduledItem = new VisualElement.TimerStateScheduledItem(this, timerUpdateEvent)
			{
				timerUpdateStopCondition = ScheduledItem.OnceCondition
			};
			timerStateScheduledItem.Resume();
			return timerStateScheduledItem;
		}

		// Token: 0x06000535 RID: 1333 RVA: 0x00011F48 File Offset: 0x00010148
		IVisualElementScheduledItem IVisualElementScheduler.Execute(Action updateEvent)
		{
			VisualElement.SimpleScheduledItem simpleScheduledItem = new VisualElement.SimpleScheduledItem(this, updateEvent)
			{
				timerUpdateStopCondition = ScheduledItem.OnceCondition
			};
			simpleScheduledItem.Resume();
			return simpleScheduledItem;
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x06000536 RID: 1334 RVA: 0x00011F7C File Offset: 0x0001017C
		public IStyle style
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x06000537 RID: 1335 RVA: 0x00011F94 File Offset: 0x00010194
		// (set) Token: 0x06000538 RID: 1336 RVA: 0x00011FB4 File Offset: 0x000101B4
		StyleValue<float> IStyle.width
		{
			get
			{
				return this.effectiveStyle.width;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.width, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.Width = value.value;
				}
			}
		}

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x06000539 RID: 1337 RVA: 0x00011FF0 File Offset: 0x000101F0
		// (set) Token: 0x0600053A RID: 1338 RVA: 0x00012010 File Offset: 0x00010210
		StyleValue<float> IStyle.height
		{
			get
			{
				return this.effectiveStyle.height;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.height, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.Height = value.value;
				}
			}
		}

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x0600053B RID: 1339 RVA: 0x0001204C File Offset: 0x0001024C
		// (set) Token: 0x0600053C RID: 1340 RVA: 0x0001206C File Offset: 0x0001026C
		StyleValue<float> IStyle.maxWidth
		{
			get
			{
				return this.effectiveStyle.maxWidth;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.maxWidth, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.MaxWidth = value.value;
				}
			}
		}

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x0600053D RID: 1341 RVA: 0x000120A8 File Offset: 0x000102A8
		// (set) Token: 0x0600053E RID: 1342 RVA: 0x000120C8 File Offset: 0x000102C8
		StyleValue<float> IStyle.maxHeight
		{
			get
			{
				return this.effectiveStyle.maxHeight;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.maxHeight, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.MaxHeight = value.value;
				}
			}
		}

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x0600053F RID: 1343 RVA: 0x00012104 File Offset: 0x00010304
		// (set) Token: 0x06000540 RID: 1344 RVA: 0x00012124 File Offset: 0x00010324
		StyleValue<float> IStyle.minWidth
		{
			get
			{
				return this.effectiveStyle.minWidth;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.minWidth, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.MinWidth = value.value;
				}
			}
		}

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x06000541 RID: 1345 RVA: 0x00012160 File Offset: 0x00010360
		// (set) Token: 0x06000542 RID: 1346 RVA: 0x00012180 File Offset: 0x00010380
		StyleValue<float> IStyle.minHeight
		{
			get
			{
				return this.effectiveStyle.minHeight;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.minHeight, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.MinHeight = value.value;
				}
			}
		}

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x06000543 RID: 1347 RVA: 0x000121BC File Offset: 0x000103BC
		// (set) Token: 0x06000544 RID: 1348 RVA: 0x0001220C File Offset: 0x0001040C
		StyleValue<Flex> IStyle.flex
		{
			get
			{
				return new Flex(this.style.flexGrow, this.style.flexShrink, this.style.flexBasis);
			}
			set
			{
				this.style.flexGrow = new StyleValue<float>(value.value.grow, value.specificity);
				this.style.flexShrink = new StyleValue<float>(value.value.shrink, value.specificity);
				this.style.flexBasis = new StyleValue<float>(value.value.basis, value.specificity);
			}
		}

		// Token: 0x1700012C RID: 300
		// (get) Token: 0x06000545 RID: 1349 RVA: 0x00012284 File Offset: 0x00010484
		// (set) Token: 0x06000546 RID: 1350 RVA: 0x000122A4 File Offset: 0x000104A4
		StyleValue<float> IStyle.flexBasis
		{
			get
			{
				return this.effectiveStyle.FlexBasisToFloat();
			}
			set
			{
				float value2;
				if (this.inlineStyle.flexBasis.value.isKeyword)
				{
					if (this.inlineStyle.flexBasis.value.keyword == StyleValueKeyword.Auto)
					{
						value2 = -1f;
					}
					else
					{
						value2 = float.NaN;
					}
				}
				else
				{
					value2 = this.inlineStyle.flexBasis.value.floatValue;
				}
				StyleValue<float> styleValue = new StyleValue<float>(value2, this.inlineStyle.flexBasis.specificity);
				if (StyleValueUtils.ApplyAndCompare(ref styleValue, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					if (value.value == -1f)
					{
						this.inlineStyle.flexBasis.value = new FloatOrKeyword(StyleValueKeyword.Auto);
						this.yogaNode.FlexBasis = YogaValue.Auto();
					}
					else
					{
						this.inlineStyle.flexBasis.value = new FloatOrKeyword(value.value);
						this.yogaNode.FlexBasis = value.value;
					}
				}
				this.inlineStyle.flexBasis.specificity = styleValue.specificity;
			}
		}

		// Token: 0x1700012D RID: 301
		// (get) Token: 0x06000547 RID: 1351 RVA: 0x000123D4 File Offset: 0x000105D4
		// (set) Token: 0x06000548 RID: 1352 RVA: 0x000123F4 File Offset: 0x000105F4
		StyleValue<float> IStyle.flexGrow
		{
			get
			{
				return this.effectiveStyle.flexGrow;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.flexGrow, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.FlexGrow = value.value;
				}
			}
		}

		// Token: 0x1700012E RID: 302
		// (get) Token: 0x06000549 RID: 1353 RVA: 0x0001242C File Offset: 0x0001062C
		// (set) Token: 0x0600054A RID: 1354 RVA: 0x0001244C File Offset: 0x0001064C
		StyleValue<float> IStyle.flexShrink
		{
			get
			{
				return this.effectiveStyle.flexShrink;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.flexShrink, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.FlexShrink = value.value;
				}
			}
		}

		// Token: 0x1700012F RID: 303
		// (get) Token: 0x0600054B RID: 1355 RVA: 0x00012484 File Offset: 0x00010684
		// (set) Token: 0x0600054C RID: 1356 RVA: 0x000124C0 File Offset: 0x000106C0
		StyleValue<Overflow> IStyle.overflow
		{
			get
			{
				return new StyleValue<Overflow>((Overflow)this.effectiveStyle.overflow.value, this.effectiveStyle.overflow.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.overflow, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.Overflow = (YogaOverflow)value.value;
				}
			}
		}

		// Token: 0x17000130 RID: 304
		// (get) Token: 0x0600054D RID: 1357 RVA: 0x00012514 File Offset: 0x00010714
		// (set) Token: 0x0600054E RID: 1358 RVA: 0x00012534 File Offset: 0x00010734
		StyleValue<float> IStyle.positionLeft
		{
			get
			{
				return this.effectiveStyle.positionLeft;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.positionLeft, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.Left = value.value;
				}
			}
		}

		// Token: 0x17000131 RID: 305
		// (get) Token: 0x0600054F RID: 1359 RVA: 0x00012570 File Offset: 0x00010770
		// (set) Token: 0x06000550 RID: 1360 RVA: 0x00012590 File Offset: 0x00010790
		StyleValue<float> IStyle.positionTop
		{
			get
			{
				return this.effectiveStyle.positionTop;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.positionTop, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.Top = value.value;
				}
			}
		}

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x06000551 RID: 1361 RVA: 0x000125CC File Offset: 0x000107CC
		// (set) Token: 0x06000552 RID: 1362 RVA: 0x000125EC File Offset: 0x000107EC
		StyleValue<float> IStyle.positionRight
		{
			get
			{
				return this.effectiveStyle.positionRight;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.positionRight, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.Right = value.value;
				}
			}
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x06000553 RID: 1363 RVA: 0x00012628 File Offset: 0x00010828
		// (set) Token: 0x06000554 RID: 1364 RVA: 0x00012648 File Offset: 0x00010848
		StyleValue<float> IStyle.positionBottom
		{
			get
			{
				return this.effectiveStyle.positionBottom;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.positionBottom, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.Bottom = value.value;
				}
			}
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x06000555 RID: 1365 RVA: 0x00012684 File Offset: 0x00010884
		// (set) Token: 0x06000556 RID: 1366 RVA: 0x000126A4 File Offset: 0x000108A4
		StyleValue<float> IStyle.marginLeft
		{
			get
			{
				return this.effectiveStyle.marginLeft;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.marginLeft, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.MarginLeft = value.value;
				}
			}
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x06000557 RID: 1367 RVA: 0x000126E0 File Offset: 0x000108E0
		// (set) Token: 0x06000558 RID: 1368 RVA: 0x00012700 File Offset: 0x00010900
		StyleValue<float> IStyle.marginTop
		{
			get
			{
				return this.effectiveStyle.marginTop;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.marginTop, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.MarginTop = value.value;
				}
			}
		}

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x06000559 RID: 1369 RVA: 0x0001273C File Offset: 0x0001093C
		// (set) Token: 0x0600055A RID: 1370 RVA: 0x0001275C File Offset: 0x0001095C
		StyleValue<float> IStyle.marginRight
		{
			get
			{
				return this.effectiveStyle.marginRight;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.marginRight, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.MarginRight = value.value;
				}
			}
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x0600055B RID: 1371 RVA: 0x00012798 File Offset: 0x00010998
		// (set) Token: 0x0600055C RID: 1372 RVA: 0x000127B8 File Offset: 0x000109B8
		StyleValue<float> IStyle.marginBottom
		{
			get
			{
				return this.effectiveStyle.marginBottom;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.marginBottom, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.MarginBottom = value.value;
				}
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x0600055D RID: 1373 RVA: 0x000127F4 File Offset: 0x000109F4
		// (set) Token: 0x0600055E RID: 1374 RVA: 0x0001280F File Offset: 0x00010A0F
		[Obsolete("Use borderLeftWidth instead")]
		StyleValue<float> IStyle.borderLeft
		{
			get
			{
				return ((IStyle)this).borderLeftWidth;
			}
			set
			{
				((IStyle)this).borderLeftWidth = value;
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x0600055F RID: 1375 RVA: 0x0001281C File Offset: 0x00010A1C
		// (set) Token: 0x06000560 RID: 1376 RVA: 0x00012837 File Offset: 0x00010A37
		[Obsolete("Use borderTopWidth instead")]
		StyleValue<float> IStyle.borderTop
		{
			get
			{
				return ((IStyle)this).borderTopWidth;
			}
			set
			{
				((IStyle)this).borderTopWidth = value;
			}
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x06000561 RID: 1377 RVA: 0x00012844 File Offset: 0x00010A44
		// (set) Token: 0x06000562 RID: 1378 RVA: 0x0001285F File Offset: 0x00010A5F
		[Obsolete("Use borderRightWidth instead")]
		StyleValue<float> IStyle.borderRight
		{
			get
			{
				return ((IStyle)this).borderRightWidth;
			}
			set
			{
				((IStyle)this).borderRightWidth = value;
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000563 RID: 1379 RVA: 0x0001286C File Offset: 0x00010A6C
		// (set) Token: 0x06000564 RID: 1380 RVA: 0x00012887 File Offset: 0x00010A87
		[Obsolete("Use borderBottomWidth instead")]
		StyleValue<float> IStyle.borderBottom
		{
			get
			{
				return ((IStyle)this).borderBottomWidth;
			}
			set
			{
				((IStyle)this).borderBottomWidth = value;
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x06000565 RID: 1381 RVA: 0x00012894 File Offset: 0x00010A94
		// (set) Token: 0x06000566 RID: 1382 RVA: 0x000128B4 File Offset: 0x00010AB4
		StyleValue<float> IStyle.borderLeftWidth
		{
			get
			{
				return this.effectiveStyle.borderLeftWidth;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.borderLeftWidth, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.BorderLeftWidth = value.value;
				}
			}
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x06000567 RID: 1383 RVA: 0x000128EC File Offset: 0x00010AEC
		// (set) Token: 0x06000568 RID: 1384 RVA: 0x0001290C File Offset: 0x00010B0C
		StyleValue<float> IStyle.borderTopWidth
		{
			get
			{
				return this.effectiveStyle.borderTopWidth;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.borderTopWidth, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.BorderTopWidth = value.value;
				}
			}
		}

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06000569 RID: 1385 RVA: 0x00012944 File Offset: 0x00010B44
		// (set) Token: 0x0600056A RID: 1386 RVA: 0x00012964 File Offset: 0x00010B64
		StyleValue<float> IStyle.borderRightWidth
		{
			get
			{
				return this.effectiveStyle.borderRightWidth;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.borderRightWidth, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.BorderRightWidth = value.value;
				}
			}
		}

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x0600056B RID: 1387 RVA: 0x0001299C File Offset: 0x00010B9C
		// (set) Token: 0x0600056C RID: 1388 RVA: 0x000129BC File Offset: 0x00010BBC
		StyleValue<float> IStyle.borderBottomWidth
		{
			get
			{
				return this.effectiveStyle.borderBottomWidth;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.borderBottomWidth, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.BorderBottomWidth = value.value;
				}
			}
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x0600056D RID: 1389 RVA: 0x000129F4 File Offset: 0x00010BF4
		// (set) Token: 0x0600056E RID: 1390 RVA: 0x00012A14 File Offset: 0x00010C14
		StyleValue<float> IStyle.borderRadius
		{
			get
			{
				return this.style.borderTopLeftRadius;
			}
			set
			{
				this.style.borderTopLeftRadius = value;
				this.style.borderTopRightRadius = value;
				this.style.borderBottomLeftRadius = value;
				this.style.borderBottomRightRadius = value;
			}
		}

		// Token: 0x17000141 RID: 321
		// (get) Token: 0x0600056F RID: 1391 RVA: 0x00012A48 File Offset: 0x00010C48
		// (set) Token: 0x06000570 RID: 1392 RVA: 0x00012A68 File Offset: 0x00010C68
		StyleValue<float> IStyle.borderTopLeftRadius
		{
			get
			{
				return this.effectiveStyle.borderTopLeftRadius;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.borderTopLeftRadius, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x06000571 RID: 1393 RVA: 0x00012A8C File Offset: 0x00010C8C
		// (set) Token: 0x06000572 RID: 1394 RVA: 0x00012AAC File Offset: 0x00010CAC
		StyleValue<float> IStyle.borderTopRightRadius
		{
			get
			{
				return this.effectiveStyle.borderTopRightRadius;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.borderTopRightRadius, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x06000573 RID: 1395 RVA: 0x00012AD0 File Offset: 0x00010CD0
		// (set) Token: 0x06000574 RID: 1396 RVA: 0x00012AF0 File Offset: 0x00010CF0
		StyleValue<float> IStyle.borderBottomRightRadius
		{
			get
			{
				return this.effectiveStyle.borderBottomRightRadius;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.borderBottomRightRadius, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x06000575 RID: 1397 RVA: 0x00012B14 File Offset: 0x00010D14
		// (set) Token: 0x06000576 RID: 1398 RVA: 0x00012B34 File Offset: 0x00010D34
		StyleValue<float> IStyle.borderBottomLeftRadius
		{
			get
			{
				return this.effectiveStyle.borderBottomLeftRadius;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.borderBottomLeftRadius, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x06000577 RID: 1399 RVA: 0x00012B58 File Offset: 0x00010D58
		// (set) Token: 0x06000578 RID: 1400 RVA: 0x00012B78 File Offset: 0x00010D78
		StyleValue<float> IStyle.paddingLeft
		{
			get
			{
				return this.effectiveStyle.paddingLeft;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.paddingLeft, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.PaddingLeft = value.value;
				}
			}
		}

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x06000579 RID: 1401 RVA: 0x00012BB4 File Offset: 0x00010DB4
		// (set) Token: 0x0600057A RID: 1402 RVA: 0x00012BD4 File Offset: 0x00010DD4
		StyleValue<float> IStyle.paddingTop
		{
			get
			{
				return this.effectiveStyle.paddingTop;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.paddingTop, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.PaddingTop = value.value;
				}
			}
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x0600057B RID: 1403 RVA: 0x00012C10 File Offset: 0x00010E10
		// (set) Token: 0x0600057C RID: 1404 RVA: 0x00012C30 File Offset: 0x00010E30
		StyleValue<float> IStyle.paddingRight
		{
			get
			{
				return this.effectiveStyle.paddingRight;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.paddingRight, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.PaddingRight = value.value;
				}
			}
		}

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x0600057D RID: 1405 RVA: 0x00012C6C File Offset: 0x00010E6C
		// (set) Token: 0x0600057E RID: 1406 RVA: 0x00012C8C File Offset: 0x00010E8C
		StyleValue<float> IStyle.paddingBottom
		{
			get
			{
				return this.effectiveStyle.paddingBottom;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.paddingBottom, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.PaddingBottom = value.value;
				}
			}
		}

		// Token: 0x17000149 RID: 329
		// (get) Token: 0x0600057F RID: 1407 RVA: 0x00012CC8 File Offset: 0x00010EC8
		// (set) Token: 0x06000580 RID: 1408 RVA: 0x00012D04 File Offset: 0x00010F04
		StyleValue<PositionType> IStyle.positionType
		{
			get
			{
				return new StyleValue<PositionType>((PositionType)this.effectiveStyle.positionType.value, this.effectiveStyle.positionType.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.positionType, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					PositionType value2 = value.value;
					if (value2 != PositionType.Absolute && value2 != PositionType.Manual)
					{
						if (value2 == PositionType.Relative)
						{
							this.yogaNode.PositionType = YogaPositionType.Relative;
						}
					}
					else
					{
						this.yogaNode.PositionType = YogaPositionType.Absolute;
					}
				}
			}
		}

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x06000581 RID: 1409 RVA: 0x00012D88 File Offset: 0x00010F88
		// (set) Token: 0x06000582 RID: 1410 RVA: 0x00012DC4 File Offset: 0x00010FC4
		StyleValue<Align> IStyle.alignSelf
		{
			get
			{
				return new StyleValue<Align>((Align)this.effectiveStyle.alignSelf.value, this.effectiveStyle.alignSelf.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.alignSelf, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.AlignSelf = (YogaAlign)value.value;
				}
			}
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x06000583 RID: 1411 RVA: 0x00012E18 File Offset: 0x00011018
		// (set) Token: 0x06000584 RID: 1412 RVA: 0x00012E33 File Offset: 0x00011033
		[Obsolete("Use unityTextAlign instead")]
		StyleValue<TextAnchor> IStyle.textAlignment
		{
			get
			{
				return ((IStyle)this).unityTextAlign;
			}
			set
			{
				((IStyle)this).unityTextAlign = value;
			}
		}

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x06000585 RID: 1413 RVA: 0x00012E40 File Offset: 0x00011040
		// (set) Token: 0x06000586 RID: 1414 RVA: 0x00012E7A File Offset: 0x0001107A
		StyleValue<TextAnchor> IStyle.unityTextAlign
		{
			get
			{
				return new StyleValue<TextAnchor>((TextAnchor)this.effectiveStyle.unityTextAlign.value, this.effectiveStyle.unityTextAlign.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.unityTextAlign, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x06000587 RID: 1415 RVA: 0x00012EB0 File Offset: 0x000110B0
		// (set) Token: 0x06000588 RID: 1416 RVA: 0x00012ECB File Offset: 0x000110CB
		[Obsolete("Use fontStyleAndWeight instead")]
		StyleValue<FontStyle> IStyle.fontStyle
		{
			get
			{
				return ((IStyle)this).fontStyleAndWeight;
			}
			set
			{
				((IStyle)this).fontStyleAndWeight = value;
			}
		}

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x06000589 RID: 1417 RVA: 0x00012ED8 File Offset: 0x000110D8
		// (set) Token: 0x0600058A RID: 1418 RVA: 0x00012F12 File Offset: 0x00011112
		StyleValue<FontStyle> IStyle.fontStyleAndWeight
		{
			get
			{
				return new StyleValue<FontStyle>((FontStyle)this.effectiveStyle.fontStyleAndWeight.value, this.effectiveStyle.fontStyleAndWeight.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.fontStyleAndWeight, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
				}
			}
		}

		// Token: 0x1700014F RID: 335
		// (get) Token: 0x0600058B RID: 1419 RVA: 0x00012F48 File Offset: 0x00011148
		// (set) Token: 0x0600058C RID: 1420 RVA: 0x00012F82 File Offset: 0x00011182
		StyleValue<TextClipping> IStyle.textClipping
		{
			get
			{
				return new StyleValue<TextClipping>((TextClipping)this.effectiveStyle.textClipping.value, this.effectiveStyle.textClipping.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.textClipping, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000150 RID: 336
		// (get) Token: 0x0600058D RID: 1421 RVA: 0x00012FB8 File Offset: 0x000111B8
		// (set) Token: 0x0600058E RID: 1422 RVA: 0x00012FD8 File Offset: 0x000111D8
		StyleValue<Font> IStyle.font
		{
			get
			{
				return this.effectiveStyle.font;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompareObject<Font>(ref this.inlineStyle.font, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
				}
			}
		}

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x0600058F RID: 1423 RVA: 0x00012FFC File Offset: 0x000111FC
		// (set) Token: 0x06000590 RID: 1424 RVA: 0x0001301C File Offset: 0x0001121C
		StyleValue<int> IStyle.fontSize
		{
			get
			{
				return this.effectiveStyle.fontSize;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.fontSize, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
				}
			}
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x06000591 RID: 1425 RVA: 0x00013040 File Offset: 0x00011240
		// (set) Token: 0x06000592 RID: 1426 RVA: 0x00013060 File Offset: 0x00011260
		StyleValue<bool> IStyle.wordWrap
		{
			get
			{
				return this.effectiveStyle.wordWrap;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.wordWrap, value))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
				}
			}
		}

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x06000593 RID: 1427 RVA: 0x00013084 File Offset: 0x00011284
		// (set) Token: 0x06000594 RID: 1428 RVA: 0x0001309F File Offset: 0x0001129F
		[Obsolete("Use color instead")]
		StyleValue<Color> IStyle.textColor
		{
			get
			{
				return ((IStyle)this).color;
			}
			set
			{
				((IStyle)this).color = value;
			}
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x06000595 RID: 1429 RVA: 0x000130AC File Offset: 0x000112AC
		// (set) Token: 0x06000596 RID: 1430 RVA: 0x000130CC File Offset: 0x000112CC
		StyleValue<Color> IStyle.color
		{
			get
			{
				return this.effectiveStyle.color;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.color, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x06000597 RID: 1431 RVA: 0x000130F0 File Offset: 0x000112F0
		// (set) Token: 0x06000598 RID: 1432 RVA: 0x0001312C File Offset: 0x0001132C
		StyleValue<FlexDirection> IStyle.flexDirection
		{
			get
			{
				return new StyleValue<FlexDirection>((FlexDirection)this.effectiveStyle.flexDirection.value, this.effectiveStyle.flexDirection.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.flexDirection, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
					this.yogaNode.FlexDirection = (YogaFlexDirection)value.value;
				}
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x06000599 RID: 1433 RVA: 0x00013180 File Offset: 0x00011380
		// (set) Token: 0x0600059A RID: 1434 RVA: 0x000131A0 File Offset: 0x000113A0
		StyleValue<Color> IStyle.backgroundColor
		{
			get
			{
				return this.effectiveStyle.backgroundColor;
			}
			set
			{
				if (value.specificity == 0 && value == default(Color))
				{
					this.inlineStyle.backgroundColor = this.sharedStyle.backgroundColor;
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
				else if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.backgroundColor, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x0600059B RID: 1435 RVA: 0x00013218 File Offset: 0x00011418
		// (set) Token: 0x0600059C RID: 1436 RVA: 0x00013238 File Offset: 0x00011438
		StyleValue<Color> IStyle.borderColor
		{
			get
			{
				return this.effectiveStyle.borderColor;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.borderColor, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x0600059D RID: 1437 RVA: 0x0001325C File Offset: 0x0001145C
		// (set) Token: 0x0600059E RID: 1438 RVA: 0x0001327C File Offset: 0x0001147C
		StyleValue<Texture2D> IStyle.backgroundImage
		{
			get
			{
				return this.effectiveStyle.backgroundImage;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompareObject<Texture2D>(ref this.inlineStyle.backgroundImage, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x0600059F RID: 1439 RVA: 0x000132A0 File Offset: 0x000114A0
		// (set) Token: 0x060005A0 RID: 1440 RVA: 0x000132BB File Offset: 0x000114BB
		[Obsolete("Use backgroundScaleMode instead")]
		StyleValue<ScaleMode> IStyle.backgroundSize
		{
			get
			{
				return ((IStyle)this).backgroundScaleMode;
			}
			set
			{
				((IStyle)this).backgroundScaleMode = value;
			}
		}

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x060005A1 RID: 1441 RVA: 0x000132C8 File Offset: 0x000114C8
		// (set) Token: 0x060005A2 RID: 1442 RVA: 0x00013302 File Offset: 0x00011502
		StyleValue<ScaleMode> IStyle.backgroundScaleMode
		{
			get
			{
				return new StyleValue<ScaleMode>((ScaleMode)this.effectiveStyle.backgroundScaleMode.value, this.effectiveStyle.backgroundScaleMode.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.backgroundScaleMode, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x1700015B RID: 347
		// (get) Token: 0x060005A3 RID: 1443 RVA: 0x00013338 File Offset: 0x00011538
		// (set) Token: 0x060005A4 RID: 1444 RVA: 0x00013374 File Offset: 0x00011574
		StyleValue<Align> IStyle.alignItems
		{
			get
			{
				return new StyleValue<Align>((Align)this.effectiveStyle.alignItems.value, this.effectiveStyle.alignItems.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.alignItems, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.AlignItems = (YogaAlign)value.value;
				}
			}
		}

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x060005A5 RID: 1445 RVA: 0x000133C8 File Offset: 0x000115C8
		// (set) Token: 0x060005A6 RID: 1446 RVA: 0x00013404 File Offset: 0x00011604
		StyleValue<Align> IStyle.alignContent
		{
			get
			{
				return new StyleValue<Align>((Align)this.effectiveStyle.alignContent.value, this.effectiveStyle.alignContent.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.alignContent, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.AlignContent = (YogaAlign)value.value;
				}
			}
		}

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060005A7 RID: 1447 RVA: 0x00013458 File Offset: 0x00011658
		// (set) Token: 0x060005A8 RID: 1448 RVA: 0x00013494 File Offset: 0x00011694
		StyleValue<Justify> IStyle.justifyContent
		{
			get
			{
				return new StyleValue<Justify>((Justify)this.effectiveStyle.justifyContent.value, this.effectiveStyle.justifyContent.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.justifyContent, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.JustifyContent = (YogaJustify)value.value;
				}
			}
		}

		// Token: 0x1700015E RID: 350
		// (get) Token: 0x060005A9 RID: 1449 RVA: 0x000134E8 File Offset: 0x000116E8
		// (set) Token: 0x060005AA RID: 1450 RVA: 0x00013524 File Offset: 0x00011724
		StyleValue<Wrap> IStyle.flexWrap
		{
			get
			{
				return new StyleValue<Wrap>((Wrap)this.effectiveStyle.flexWrap.value, this.effectiveStyle.flexWrap.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.flexWrap, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Styles);
					this.yogaNode.Wrap = (YogaWrap)value.value;
				}
			}
		}

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x060005AB RID: 1451 RVA: 0x00013578 File Offset: 0x00011778
		// (set) Token: 0x060005AC RID: 1452 RVA: 0x00013598 File Offset: 0x00011798
		StyleValue<int> IStyle.sliceLeft
		{
			get
			{
				return this.effectiveStyle.sliceLeft;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.sliceLeft, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x060005AD RID: 1453 RVA: 0x000135BC File Offset: 0x000117BC
		// (set) Token: 0x060005AE RID: 1454 RVA: 0x000135DC File Offset: 0x000117DC
		StyleValue<int> IStyle.sliceTop
		{
			get
			{
				return this.effectiveStyle.sliceTop;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.sliceTop, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x060005AF RID: 1455 RVA: 0x00013600 File Offset: 0x00011800
		// (set) Token: 0x060005B0 RID: 1456 RVA: 0x00013620 File Offset: 0x00011820
		StyleValue<int> IStyle.sliceRight
		{
			get
			{
				return this.effectiveStyle.sliceRight;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.sliceRight, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000162 RID: 354
		// (get) Token: 0x060005B1 RID: 1457 RVA: 0x00013644 File Offset: 0x00011844
		// (set) Token: 0x060005B2 RID: 1458 RVA: 0x00013664 File Offset: 0x00011864
		StyleValue<int> IStyle.sliceBottom
		{
			get
			{
				return this.effectiveStyle.sliceBottom;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.sliceBottom, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000163 RID: 355
		// (get) Token: 0x060005B3 RID: 1459 RVA: 0x00013688 File Offset: 0x00011888
		// (set) Token: 0x060005B4 RID: 1460 RVA: 0x000136A8 File Offset: 0x000118A8
		StyleValue<float> IStyle.opacity
		{
			get
			{
				return this.effectiveStyle.opacity;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.opacity, value))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000164 RID: 356
		// (get) Token: 0x060005B5 RID: 1461 RVA: 0x000136CC File Offset: 0x000118CC
		// (set) Token: 0x060005B6 RID: 1462 RVA: 0x000136EC File Offset: 0x000118EC
		StyleValue<CursorStyle> IStyle.cursor
		{
			get
			{
				return this.effectiveStyle.cursor;
			}
			set
			{
				StyleValueUtils.ApplyAndCompare<CursorStyle>(ref this.inlineStyle.cursor, value);
			}
		}

		// Token: 0x17000165 RID: 357
		// (get) Token: 0x060005B7 RID: 1463 RVA: 0x00013704 File Offset: 0x00011904
		// (set) Token: 0x060005B8 RID: 1464 RVA: 0x0001373E File Offset: 0x0001193E
		StyleValue<Visibility> IStyle.visibility
		{
			get
			{
				return new StyleValue<Visibility>((Visibility)this.effectiveStyle.visibility.value, this.effectiveStyle.visibility.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.inlineStyle.visibility, new StyleValue<int>((int)value.value, value.specificity)))
				{
					this.IncrementVersion(VersionChangeType.Styles | VersionChangeType.Repaint);
				}
			}
		}

		// Token: 0x17000193 RID: 403
		// (get) Token: 0x060005B9 RID: 1465 RVA: 0x00013774 File Offset: 0x00011974
		internal IList<StyleSheet> styleSheets
		{
			get
			{
				if (this.m_StyleSheets == null && this.m_StyleSheetPaths != null)
				{
					this.LoadStyleSheetsFromPaths();
				}
				return this.m_StyleSheets;
			}
		}

		// Token: 0x060005BA RID: 1466 RVA: 0x000137AD File Offset: 0x000119AD
		public void AddStyleSheetPath(string sheetPath)
		{
			if (this.m_StyleSheetPaths == null)
			{
				this.m_StyleSheetPaths = new List<string>();
			}
			this.m_StyleSheetPaths.Add(sheetPath);
			this.m_StyleSheets = null;
			this.IncrementVersion(VersionChangeType.StyleSheet);
		}

		// Token: 0x060005BB RID: 1467 RVA: 0x000137E3 File Offset: 0x000119E3
		public void RemoveStyleSheetPath(string sheetPath)
		{
			if (this.m_StyleSheetPaths == null)
			{
				Debug.LogWarning("Attempting to remove from null style sheet path list");
			}
			else
			{
				this.m_StyleSheetPaths.Remove(sheetPath);
				this.m_StyleSheets = null;
				this.IncrementVersion(VersionChangeType.StyleSheet);
			}
		}

		// Token: 0x060005BC RID: 1468 RVA: 0x00013820 File Offset: 0x00011A20
		public bool HasStyleSheetPath(string sheetPath)
		{
			return this.m_StyleSheetPaths != null && this.m_StyleSheetPaths.Contains(sheetPath);
		}

		// Token: 0x060005BD RID: 1469 RVA: 0x00013850 File Offset: 0x00011A50
		internal void ReplaceStyleSheetPath(string oldSheetPath, string newSheetPath)
		{
			if (this.m_StyleSheetPaths == null)
			{
				Debug.LogWarning("Attempting to replace a style from null style sheet path list");
			}
			else
			{
				int num = this.m_StyleSheetPaths.IndexOf(oldSheetPath);
				if (num >= 0)
				{
					this.m_StyleSheetPaths[num] = newSheetPath;
					this.m_StyleSheets = null;
					this.IncrementVersion(VersionChangeType.StyleSheet);
				}
			}
		}

		// Token: 0x060005BE RID: 1470 RVA: 0x000138AC File Offset: 0x00011AAC
		internal void LoadStyleSheetsFromPaths()
		{
			if (this.m_StyleSheetPaths != null && this.elementPanel != null)
			{
				this.m_StyleSheets = new List<StyleSheet>();
				foreach (string text in this.m_StyleSheetPaths)
				{
					StyleSheet styleSheet = Panel.loadResourceFunc(text, typeof(StyleSheet)) as StyleSheet;
					if (styleSheet != null)
					{
						if (!styleSheet.hasSelectorsCached)
						{
							int i = 0;
							int num = styleSheet.complexSelectors.Length;
							while (i < num)
							{
								styleSheet.complexSelectors[i].CachePseudoStateMasks();
								i++;
							}
							styleSheet.hasSelectorsCached = true;
						}
						this.m_StyleSheets.Add(styleSheet);
					}
					else
					{
						Debug.LogWarning(string.Format("Style sheet not found for path \"{0}\"", text));
					}
				}
			}
		}

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x060005BF RID: 1471 RVA: 0x000139B8 File Offset: 0x00011BB8
		// (set) Token: 0x060005C0 RID: 1472 RVA: 0x00013A04 File Offset: 0x00011C04
		public string tooltip
		{
			get
			{
				if (VisualElement.<>f__mg$cache0 == null)
				{
					VisualElement.<>f__mg$cache0 = new EventCallback<TooltipEvent, string>(VisualElement.OnTooltip);
				}
				string text;
				base.TryGetUserArgs<TooltipEvent, string>(VisualElement.<>f__mg$cache0, TrickleDown.NoTrickleDown, out text);
				return text ?? string.Empty;
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					if (VisualElement.<>f__mg$cache1 == null)
					{
						VisualElement.<>f__mg$cache1 = new EventCallback<TooltipEvent, string>(VisualElement.OnTooltip);
					}
					base.UnregisterCallback<TooltipEvent, string>(VisualElement.<>f__mg$cache1, TrickleDown.NoTrickleDown);
				}
				else
				{
					if (VisualElement.<>f__mg$cache2 == null)
					{
						VisualElement.<>f__mg$cache2 = new EventCallback<TooltipEvent, string>(VisualElement.OnTooltip);
					}
					base.RegisterCallback<TooltipEvent, string>(VisualElement.<>f__mg$cache2, value, TrickleDown.NoTrickleDown);
				}
			}
		}

		// Token: 0x060005C1 RID: 1473 RVA: 0x00013A70 File Offset: 0x00011C70
		private static void OnTooltip(TooltipEvent e, string tooltip)
		{
			VisualElement visualElement = e.currentTarget as VisualElement;
			if (visualElement != null)
			{
				e.rect = visualElement.worldBound;
			}
			e.tooltip = tooltip;
			e.StopImmediatePropagation();
		}

		// Token: 0x060005C2 RID: 1474 RVA: 0x00013AA9 File Offset: 0x00011CA9
		// Note: this type is marked as 'beforefieldinit'.
		static VisualElement()
		{
		}

		// Token: 0x04000219 RID: 537
		public static readonly int defaultFocusIndex = -1;

		// Token: 0x0400021A RID: 538
		private static uint s_NextId;

		// Token: 0x0400021B RID: 539
		private static List<string> s_EmptyClassList = new List<string>(0);

		// Token: 0x0400021C RID: 540
		private string m_Name;

		// Token: 0x0400021D RID: 541
		private List<string> m_ClassList;

		// Token: 0x0400021E RID: 542
		private string m_TypeName;

		// Token: 0x0400021F RID: 543
		private string m_FullTypeName;

		// Token: 0x04000220 RID: 544
		private string m_PersistenceKey;

		// Token: 0x04000221 RID: 545
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <enablePersistence>k__BackingField;

		// Token: 0x04000222 RID: 546
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private object <userData>k__BackingField;

		// Token: 0x04000223 RID: 547
		private RenderData m_RenderData;

		// Token: 0x04000224 RID: 548
		private Vector3 m_Position = Vector3.zero;

		// Token: 0x04000225 RID: 549
		private Quaternion m_Rotation = Quaternion.identity;

		// Token: 0x04000226 RID: 550
		private Vector3 m_Scale = Vector3.one;

		// Token: 0x04000227 RID: 551
		private Rect m_Layout;

		// Token: 0x04000228 RID: 552
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <isWorldTransformDirty>k__BackingField;

		// Token: 0x04000229 RID: 553
		private Matrix4x4 m_WorldTransform = Matrix4x4.identity;

		// Token: 0x0400022A RID: 554
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <isWorldClipDirty>k__BackingField;

		// Token: 0x0400022B RID: 555
		private Rect m_WorldClip = Rect.zero;

		// Token: 0x0400022C RID: 556
		internal PseudoStates triggerPseudoMask;

		// Token: 0x0400022D RID: 557
		internal PseudoStates dependencyPseudoMask;

		// Token: 0x0400022E RID: 558
		private PseudoStates m_PseudoStates;

		// Token: 0x0400022F RID: 559
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private PickingMode <pickingMode>k__BackingField;

		// Token: 0x04000230 RID: 560
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private YogaNode <yogaNode>k__BackingField;

		// Token: 0x04000231 RID: 561
		internal VisualElementStylesData m_SharedStyle = VisualElementStylesData.none;

		// Token: 0x04000232 RID: 562
		internal VisualElementStylesData m_Style = VisualElementStylesData.none;

		// Token: 0x04000233 RID: 563
		internal readonly uint controlid;

		// Token: 0x04000234 RID: 564
		private bool m_Enabled;

		// Token: 0x04000235 RID: 565
		private bool m_RequireMeasureFunction = false;

		// Token: 0x04000236 RID: 566
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private OnStylesResolved onStylesResolved;

		// Token: 0x04000237 RID: 567
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private VisualElement.Hierarchy <shadow>k__BackingField;

		// Token: 0x04000238 RID: 568
		private VisualElement.ClippingOptions m_ClippingOptions = VisualElement.ClippingOptions.NoClipping;

		// Token: 0x04000239 RID: 569
		private VisualElement m_PhysicalParent;

		// Token: 0x0400023A RID: 570
		private VisualElement m_LogicalParent;

		// Token: 0x0400023B RID: 571
		private static readonly VisualElement[] s_EmptyList = new VisualElement[0];

		// Token: 0x0400023C RID: 572
		private List<VisualElement> m_Children;

		// Token: 0x0400023D RID: 573
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private BaseVisualElementPanel <elementPanel>k__BackingField;

		// Token: 0x0400023E RID: 574
		private List<StyleSheet> m_StyleSheets;

		// Token: 0x0400023F RID: 575
		private List<string> m_StyleSheetPaths;

		// Token: 0x04000240 RID: 576
		[CompilerGenerated]
		private static EventCallback<TooltipEvent, string> <>f__mg$cache0;

		// Token: 0x04000241 RID: 577
		[CompilerGenerated]
		private static EventCallback<TooltipEvent, string> <>f__mg$cache1;

		// Token: 0x04000242 RID: 578
		[CompilerGenerated]
		private static EventCallback<TooltipEvent, string> <>f__mg$cache2;

		// Token: 0x020000C0 RID: 192
		public class UxmlFactory : UxmlFactory<VisualElement, VisualElement.UxmlTraits>
		{
			// Token: 0x060005C3 RID: 1475 RVA: 0x00013AC7 File Offset: 0x00011CC7
			public UxmlFactory()
			{
			}
		}

		// Token: 0x020000C1 RID: 193
		public class UxmlTraits : UnityEngine.Experimental.UIElements.UxmlTraits
		{
			// Token: 0x060005C4 RID: 1476 RVA: 0x00013AD0 File Offset: 0x00011CD0
			public UxmlTraits()
			{
			}

			// Token: 0x17000195 RID: 405
			// (get) Token: 0x060005C5 RID: 1477 RVA: 0x00013B78 File Offset: 0x00011D78
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield return new UxmlChildElementDescription(typeof(VisualElement));
					yield break;
				}
			}

			// Token: 0x060005C6 RID: 1478 RVA: 0x00013B9C File Offset: 0x00011D9C
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				ve.name = this.m_Name.GetValueFromBag(bag, cc);
				ve.pickingMode = this.m_PickingMode.GetValueFromBag(bag, cc);
				ve.focusIndex = this.m_FocusIndex.GetValueFromBag(bag, cc);
				ve.tooltip = this.m_Tooltip.GetValueFromBag(bag, cc);
			}

			// Token: 0x04000243 RID: 579
			private UxmlStringAttributeDescription m_Name = new UxmlStringAttributeDescription
			{
				name = "name"
			};

			// Token: 0x04000244 RID: 580
			private UxmlEnumAttributeDescription<PickingMode> m_PickingMode = new UxmlEnumAttributeDescription<PickingMode>
			{
				name = "picking-mode",
				obsoleteNames = new string[]
				{
					"pickingMode"
				}
			};

			// Token: 0x04000245 RID: 581
			private UxmlStringAttributeDescription m_Tooltip = new UxmlStringAttributeDescription
			{
				name = "tooltip"
			};

			// Token: 0x04000246 RID: 582
			protected UxmlIntAttributeDescription m_FocusIndex = new UxmlIntAttributeDescription
			{
				name = "focus-index",
				obsoleteNames = new string[]
				{
					"focusIndex"
				},
				defaultValue = VisualElement.defaultFocusIndex
			};

			// Token: 0x020000C2 RID: 194
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x060005C7 RID: 1479 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x060005C8 RID: 1480 RVA: 0x00013C00 File Offset: 0x00011E00
				public bool MoveNext()
				{
					uint num = (uint)this.$PC;
					this.$PC = -1;
					switch (num)
					{
					case 0U:
						this.$current = new UxmlChildElementDescription(typeof(VisualElement));
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						return true;
					case 1U:
						this.$PC = -1;
						break;
					}
					return false;
				}

				// Token: 0x17000196 RID: 406
				// (get) Token: 0x060005C9 RID: 1481 RVA: 0x00013C64 File Offset: 0x00011E64
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x17000197 RID: 407
				// (get) Token: 0x060005CA RID: 1482 RVA: 0x00013C80 File Offset: 0x00011E80
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x060005CB RID: 1483 RVA: 0x00013C9A File Offset: 0x00011E9A
				[DebuggerHidden]
				public void Dispose()
				{
					this.$disposing = true;
					this.$PC = -1;
				}

				// Token: 0x060005CC RID: 1484 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x060005CD RID: 1485 RVA: 0x00013CAC File Offset: 0x00011EAC
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x060005CE RID: 1486 RVA: 0x00013CC6 File Offset: 0x00011EC6
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new VisualElement.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x04000247 RID: 583
				internal UxmlChildElementDescription $current;

				// Token: 0x04000248 RID: 584
				internal bool $disposing;

				// Token: 0x04000249 RID: 585
				internal int $PC;
			}
		}

		// Token: 0x020000C3 RID: 195
		public enum MeasureMode
		{
			// Token: 0x0400024B RID: 587
			Undefined,
			// Token: 0x0400024C RID: 588
			Exactly,
			// Token: 0x0400024D RID: 589
			AtMost
		}

		// Token: 0x020000C4 RID: 196
		private class DataWatchRequest : IUIElementDataWatchRequest, IVisualElementPanelActivatable, IDisposable
		{
			// Token: 0x060005CF RID: 1487 RVA: 0x00013CE1 File Offset: 0x00011EE1
			public DataWatchRequest(VisualElement handler)
			{
				this.element = handler;
				this.m_Activator = new VisualElementPanelActivator(this);
			}

			// Token: 0x17000198 RID: 408
			// (get) Token: 0x060005D0 RID: 1488 RVA: 0x00013D00 File Offset: 0x00011F00
			// (set) Token: 0x060005D1 RID: 1489 RVA: 0x00013D1A File Offset: 0x00011F1A
			public Action<Object> notification
			{
				[CompilerGenerated]
				get
				{
					return this.<notification>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<notification>k__BackingField = value;
				}
			}

			// Token: 0x17000199 RID: 409
			// (get) Token: 0x060005D2 RID: 1490 RVA: 0x00013D24 File Offset: 0x00011F24
			// (set) Token: 0x060005D3 RID: 1491 RVA: 0x00013D3E File Offset: 0x00011F3E
			public Object watchedObject
			{
				[CompilerGenerated]
				get
				{
					return this.<watchedObject>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<watchedObject>k__BackingField = value;
				}
			}

			// Token: 0x1700019A RID: 410
			// (get) Token: 0x060005D4 RID: 1492 RVA: 0x00013D48 File Offset: 0x00011F48
			// (set) Token: 0x060005D5 RID: 1493 RVA: 0x00013D62 File Offset: 0x00011F62
			public IDataWatchHandle requestedHandle
			{
				[CompilerGenerated]
				get
				{
					return this.<requestedHandle>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<requestedHandle>k__BackingField = value;
				}
			}

			// Token: 0x1700019B RID: 411
			// (get) Token: 0x060005D6 RID: 1494 RVA: 0x00013D6C File Offset: 0x00011F6C
			// (set) Token: 0x060005D7 RID: 1495 RVA: 0x00013D86 File Offset: 0x00011F86
			public VisualElement element
			{
				[CompilerGenerated]
				get
				{
					return this.<element>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<element>k__BackingField = value;
				}
			}

			// Token: 0x060005D8 RID: 1496 RVA: 0x00013D8F File Offset: 0x00011F8F
			public void Start()
			{
				this.m_Activator.SetActive(true);
			}

			// Token: 0x060005D9 RID: 1497 RVA: 0x00013D9E File Offset: 0x00011F9E
			public void Stop()
			{
				this.m_Activator.SetActive(false);
			}

			// Token: 0x060005DA RID: 1498 RVA: 0x00013DB0 File Offset: 0x00011FB0
			public bool CanBeActivated()
			{
				return this.element != null && this.element.elementPanel != null && this.element.elementPanel.dataWatch != null;
			}

			// Token: 0x060005DB RID: 1499 RVA: 0x00013DF9 File Offset: 0x00011FF9
			public void OnPanelActivate()
			{
				if (this.requestedHandle == null)
				{
					this.requestedHandle = this.element.elementPanel.dataWatch.AddWatch(this.watchedObject, this.notification);
				}
			}

			// Token: 0x060005DC RID: 1500 RVA: 0x00013E30 File Offset: 0x00012030
			public void OnPanelDeactivate()
			{
				if (this.requestedHandle != null)
				{
					this.element.elementPanel.dataWatch.RemoveWatch(this.requestedHandle);
					this.requestedHandle = null;
				}
			}

			// Token: 0x060005DD RID: 1501 RVA: 0x00013E62 File Offset: 0x00012062
			public void Dispose()
			{
				this.Stop();
			}

			// Token: 0x0400024E RID: 590
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private Action<Object> <notification>k__BackingField;

			// Token: 0x0400024F RID: 591
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private Object <watchedObject>k__BackingField;

			// Token: 0x04000250 RID: 592
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private IDataWatchHandle <requestedHandle>k__BackingField;

			// Token: 0x04000251 RID: 593
			private VisualElementPanelActivator m_Activator;

			// Token: 0x04000252 RID: 594
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private VisualElement <element>k__BackingField;
		}

		// Token: 0x020000C5 RID: 197
		public enum ClippingOptions
		{
			// Token: 0x04000254 RID: 596
			ClipContents,
			// Token: 0x04000255 RID: 597
			NoClipping,
			// Token: 0x04000256 RID: 598
			ClipAndCacheContents
		}

		// Token: 0x020000C6 RID: 198
		public struct Hierarchy
		{
			// Token: 0x060005DE RID: 1502 RVA: 0x00013E6B File Offset: 0x0001206B
			internal Hierarchy(VisualElement element)
			{
				this.m_Owner = element;
			}

			// Token: 0x1700019C RID: 412
			// (get) Token: 0x060005DF RID: 1503 RVA: 0x00013E78 File Offset: 0x00012078
			public VisualElement parent
			{
				get
				{
					return this.m_Owner.m_PhysicalParent;
				}
			}

			// Token: 0x060005E0 RID: 1504 RVA: 0x00013E98 File Offset: 0x00012098
			public void Add(VisualElement child)
			{
				if (child == null)
				{
					throw new ArgumentException("Cannot add null child");
				}
				this.Insert(this.childCount, child);
			}

			// Token: 0x060005E1 RID: 1505 RVA: 0x00013EBC File Offset: 0x000120BC
			public void Insert(int index, VisualElement child)
			{
				if (child == null)
				{
					throw new ArgumentException("Cannot insert null child");
				}
				if (index > this.childCount)
				{
					throw new IndexOutOfRangeException("Index out of range: " + index);
				}
				if (child == this.m_Owner)
				{
					throw new ArgumentException("Cannot insert element as its own child");
				}
				child.RemoveFromHierarchy();
				child.shadow.SetParent(this.m_Owner);
				if (this.m_Owner.m_Children == null)
				{
					this.m_Owner.m_Children = VisualElementListPool.Get(0);
				}
				if (this.m_Owner.yogaNode.IsMeasureDefined)
				{
					this.m_Owner.yogaNode.SetMeasureFunction(null);
				}
				this.PutChildAtIndex(child, index);
				child.SetEnabledFromHierarchy(this.m_Owner.enabledInHierarchy);
				child.IncrementVersion(VersionChangeType.Hierarchy);
				this.m_Owner.IncrementVersion(VersionChangeType.Hierarchy);
			}

			// Token: 0x060005E2 RID: 1506 RVA: 0x00013FA8 File Offset: 0x000121A8
			public void Remove(VisualElement child)
			{
				if (child == null)
				{
					throw new ArgumentException("Cannot remove null child");
				}
				if (child.shadow.parent != this.m_Owner)
				{
					throw new ArgumentException("This visualElement is not my child");
				}
				if (this.m_Owner.m_Children != null)
				{
					int index = this.m_Owner.m_Children.IndexOf(child);
					this.RemoveAt(index);
				}
			}

			// Token: 0x060005E3 RID: 1507 RVA: 0x00014018 File Offset: 0x00012218
			public void RemoveAt(int index)
			{
				if (index < 0 || index >= this.childCount)
				{
					throw new IndexOutOfRangeException("Index out of range: " + index);
				}
				VisualElement visualElement = this.m_Owner.m_Children[index];
				this.RemoveChildAtIndex(index);
				visualElement.shadow.SetParent(null);
				if (this.childCount == 0)
				{
					this.ReleaseChildList();
					if (this.m_Owner.requireMeasureFunction)
					{
						this.m_Owner.yogaNode.SetMeasureFunction(new MeasureFunction(this.m_Owner.Measure));
					}
				}
				BaseVisualElementPanel elementPanel = this.m_Owner.elementPanel;
				if (elementPanel != null)
				{
					elementPanel.OnVersionChanged(visualElement, VersionChangeType.Hierarchy);
				}
				this.m_Owner.IncrementVersion(VersionChangeType.Hierarchy);
			}

			// Token: 0x060005E4 RID: 1508 RVA: 0x000140E8 File Offset: 0x000122E8
			public void Clear()
			{
				if (this.childCount > 0)
				{
					foreach (VisualElement visualElement in this.m_Owner.m_Children)
					{
						visualElement.shadow.SetParent(null);
						visualElement.m_LogicalParent = null;
						BaseVisualElementPanel elementPanel = this.m_Owner.elementPanel;
						if (elementPanel != null)
						{
							elementPanel.OnVersionChanged(visualElement, VersionChangeType.Hierarchy);
						}
					}
					this.ReleaseChildList();
					this.m_Owner.yogaNode.Clear();
					if (this.m_Owner.requireMeasureFunction)
					{
						this.m_Owner.yogaNode.SetMeasureFunction(new MeasureFunction(this.m_Owner.Measure));
					}
					this.m_Owner.IncrementVersion(VersionChangeType.Hierarchy);
				}
			}

			// Token: 0x060005E5 RID: 1509 RVA: 0x000141DC File Offset: 0x000123DC
			internal void BringToFront(VisualElement child)
			{
				if (this.childCount > 1)
				{
					int num = this.m_Owner.m_Children.IndexOf(child);
					if (num >= 0 && num < this.childCount - 1)
					{
						this.MoveChildElement(child, num, this.childCount);
					}
				}
			}

			// Token: 0x060005E6 RID: 1510 RVA: 0x00014230 File Offset: 0x00012430
			internal void SendToBack(VisualElement child)
			{
				if (this.childCount > 1)
				{
					int num = this.m_Owner.m_Children.IndexOf(child);
					if (num > 0)
					{
						this.MoveChildElement(child, num, 0);
					}
				}
			}

			// Token: 0x060005E7 RID: 1511 RVA: 0x00014270 File Offset: 0x00012470
			internal void PlaceBehind(VisualElement child, VisualElement over)
			{
				if (this.childCount > 0)
				{
					int num = this.m_Owner.m_Children.IndexOf(child);
					if (num >= 0)
					{
						int num2 = this.m_Owner.m_Children.IndexOf(over);
						if (num2 > 0 && num < num2)
						{
							num2--;
						}
						this.MoveChildElement(child, num, num2);
					}
				}
			}

			// Token: 0x060005E8 RID: 1512 RVA: 0x000142DC File Offset: 0x000124DC
			internal void PlaceInFront(VisualElement child, VisualElement under)
			{
				if (this.childCount > 0)
				{
					int num = this.m_Owner.m_Children.IndexOf(child);
					if (num >= 0)
					{
						int num2 = this.m_Owner.m_Children.IndexOf(under);
						if (num > num2)
						{
							num2++;
						}
						this.MoveChildElement(child, num, num2);
					}
				}
			}

			// Token: 0x060005E9 RID: 1513 RVA: 0x0001433E File Offset: 0x0001253E
			private void MoveChildElement(VisualElement child, int currentIndex, int nextIndex)
			{
				this.RemoveChildAtIndex(currentIndex);
				this.PutChildAtIndex(child, nextIndex);
				this.m_Owner.IncrementVersion(VersionChangeType.Hierarchy);
			}

			// Token: 0x1700019D RID: 413
			// (get) Token: 0x060005EA RID: 1514 RVA: 0x00014360 File Offset: 0x00012560
			public int childCount
			{
				get
				{
					return (this.m_Owner.m_Children == null) ? 0 : this.m_Owner.m_Children.Count;
				}
			}

			// Token: 0x1700019E RID: 414
			public VisualElement this[int key]
			{
				get
				{
					return this.ElementAt(key);
				}
			}

			// Token: 0x060005EC RID: 1516 RVA: 0x000143B8 File Offset: 0x000125B8
			public int IndexOf(VisualElement element)
			{
				int result;
				if (this.m_Owner.m_Children != null)
				{
					result = this.m_Owner.m_Children.IndexOf(element);
				}
				else
				{
					result = -1;
				}
				return result;
			}

			// Token: 0x060005ED RID: 1517 RVA: 0x000143F8 File Offset: 0x000125F8
			public VisualElement ElementAt(int index)
			{
				if (this.m_Owner.m_Children != null)
				{
					return this.m_Owner.m_Children[index];
				}
				throw new IndexOutOfRangeException("Index out of range: " + index);
			}

			// Token: 0x060005EE RID: 1518 RVA: 0x00014448 File Offset: 0x00012648
			public IEnumerable<VisualElement> Children()
			{
				IEnumerable<VisualElement> result;
				if (this.m_Owner.m_Children != null)
				{
					result = this.m_Owner.m_Children;
				}
				else
				{
					result = VisualElement.s_EmptyList;
				}
				return result;
			}

			// Token: 0x060005EF RID: 1519 RVA: 0x00014484 File Offset: 0x00012684
			private void SetParent(VisualElement value)
			{
				this.m_Owner.m_PhysicalParent = value;
				this.m_Owner.m_LogicalParent = value;
				if (value != null)
				{
					this.m_Owner.SetPanel(this.m_Owner.m_PhysicalParent.elementPanel);
				}
				else
				{
					this.m_Owner.SetPanel(null);
				}
			}

			// Token: 0x060005F0 RID: 1520 RVA: 0x000144E0 File Offset: 0x000126E0
			public void Sort(Comparison<VisualElement> comp)
			{
				if (this.childCount > 0)
				{
					this.m_Owner.m_Children.Sort(comp);
					this.m_Owner.yogaNode.Clear();
					for (int i = 0; i < this.m_Owner.m_Children.Count; i++)
					{
						this.m_Owner.yogaNode.Insert(i, this.m_Owner.m_Children[i].yogaNode);
					}
					this.m_Owner.IncrementVersion(VersionChangeType.Hierarchy);
				}
			}

			// Token: 0x060005F1 RID: 1521 RVA: 0x00014574 File Offset: 0x00012774
			private void PutChildAtIndex(VisualElement child, int index)
			{
				if (index >= this.childCount)
				{
					this.m_Owner.m_Children.Add(child);
					this.m_Owner.yogaNode.Insert(this.m_Owner.yogaNode.Count, child.yogaNode);
				}
				else
				{
					this.m_Owner.m_Children.Insert(index, child);
					this.m_Owner.yogaNode.Insert(index, child.yogaNode);
				}
			}

			// Token: 0x060005F2 RID: 1522 RVA: 0x000145F7 File Offset: 0x000127F7
			private void RemoveChildAtIndex(int index)
			{
				this.m_Owner.m_Children.RemoveAt(index);
				this.m_Owner.yogaNode.RemoveAt(index);
			}

			// Token: 0x060005F3 RID: 1523 RVA: 0x0001461C File Offset: 0x0001281C
			private void ReleaseChildList()
			{
				List<VisualElement> children = this.m_Owner.m_Children;
				if (children != null)
				{
					this.m_Owner.m_Children = null;
					VisualElementListPool.Release(children);
				}
			}

			// Token: 0x04000257 RID: 599
			private readonly VisualElement m_Owner;
		}

		// Token: 0x020000C7 RID: 199
		private abstract class BaseVisualElementScheduledItem : ScheduledItem, IVisualElementScheduledItem, IVisualElementPanelActivatable
		{
			// Token: 0x060005F4 RID: 1524 RVA: 0x00014650 File Offset: 0x00012850
			protected BaseVisualElementScheduledItem(VisualElement handler)
			{
				this.element = handler;
				this.m_Activator = new VisualElementPanelActivator(this);
			}

			// Token: 0x1700019F RID: 415
			// (get) Token: 0x060005F5 RID: 1525 RVA: 0x00014674 File Offset: 0x00012874
			// (set) Token: 0x060005F6 RID: 1526 RVA: 0x0001468E File Offset: 0x0001288E
			public VisualElement element
			{
				[CompilerGenerated]
				get
				{
					return this.<element>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<element>k__BackingField = value;
				}
			}

			// Token: 0x170001A0 RID: 416
			// (get) Token: 0x060005F7 RID: 1527 RVA: 0x00014698 File Offset: 0x00012898
			public bool isActive
			{
				get
				{
					return this.m_Activator.isActive;
				}
			}

			// Token: 0x060005F8 RID: 1528 RVA: 0x000146B8 File Offset: 0x000128B8
			public IVisualElementScheduledItem StartingIn(long delayMs)
			{
				base.delayMs = delayMs;
				return this;
			}

			// Token: 0x060005F9 RID: 1529 RVA: 0x000146D8 File Offset: 0x000128D8
			public IVisualElementScheduledItem Until(Func<bool> stopCondition)
			{
				if (stopCondition == null)
				{
					stopCondition = ScheduledItem.ForeverCondition;
				}
				this.timerUpdateStopCondition = stopCondition;
				return this;
			}

			// Token: 0x060005FA RID: 1530 RVA: 0x00014704 File Offset: 0x00012904
			public IVisualElementScheduledItem ForDuration(long durationMs)
			{
				base.SetDuration(durationMs);
				return this;
			}

			// Token: 0x060005FB RID: 1531 RVA: 0x00014724 File Offset: 0x00012924
			public IVisualElementScheduledItem Every(long intervalMs)
			{
				base.intervalMs = intervalMs;
				if (this.timerUpdateStopCondition == ScheduledItem.OnceCondition)
				{
					this.timerUpdateStopCondition = ScheduledItem.ForeverCondition;
				}
				return this;
			}

			// Token: 0x060005FC RID: 1532 RVA: 0x00014763 File Offset: 0x00012963
			internal override void OnItemUnscheduled()
			{
				base.OnItemUnscheduled();
				this.isScheduled = false;
				if (!this.m_Activator.isDetaching)
				{
					this.m_Activator.SetActive(false);
				}
			}

			// Token: 0x060005FD RID: 1533 RVA: 0x00014791 File Offset: 0x00012991
			public void Resume()
			{
				this.m_Activator.SetActive(true);
			}

			// Token: 0x060005FE RID: 1534 RVA: 0x000147A0 File Offset: 0x000129A0
			public void Pause()
			{
				this.m_Activator.SetActive(false);
			}

			// Token: 0x060005FF RID: 1535 RVA: 0x000147AF File Offset: 0x000129AF
			public void ExecuteLater(long delayMs)
			{
				if (!this.isScheduled)
				{
					this.Resume();
				}
				base.ResetStartTime();
				this.StartingIn(delayMs);
			}

			// Token: 0x06000600 RID: 1536 RVA: 0x000147D3 File Offset: 0x000129D3
			public void OnPanelActivate()
			{
				if (!this.isScheduled)
				{
					this.isScheduled = true;
					base.ResetStartTime();
					this.element.elementPanel.scheduler.Schedule(this);
				}
			}

			// Token: 0x06000601 RID: 1537 RVA: 0x00014806 File Offset: 0x00012A06
			public void OnPanelDeactivate()
			{
				if (this.isScheduled)
				{
					this.isScheduled = false;
					this.element.elementPanel.scheduler.Unschedule(this);
				}
			}

			// Token: 0x06000602 RID: 1538 RVA: 0x00014834 File Offset: 0x00012A34
			public bool CanBeActivated()
			{
				return this.element != null && this.element.elementPanel != null && this.element.elementPanel.scheduler != null;
			}

			// Token: 0x04000258 RID: 600
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private VisualElement <element>k__BackingField;

			// Token: 0x04000259 RID: 601
			public bool isScheduled = false;

			// Token: 0x0400025A RID: 602
			private VisualElementPanelActivator m_Activator;
		}

		// Token: 0x020000C8 RID: 200
		private abstract class VisualElementScheduledItem<ActionType> : VisualElement.BaseVisualElementScheduledItem
		{
			// Token: 0x06000603 RID: 1539 RVA: 0x0001487D File Offset: 0x00012A7D
			public VisualElementScheduledItem(VisualElement handler, ActionType upEvent) : base(handler)
			{
				this.updateEvent = upEvent;
			}

			// Token: 0x06000604 RID: 1540 RVA: 0x00014890 File Offset: 0x00012A90
			public static bool Matches(ScheduledItem item, ActionType updateEvent)
			{
				VisualElement.VisualElementScheduledItem<ActionType> visualElementScheduledItem = item as VisualElement.VisualElementScheduledItem<ActionType>;
				return visualElementScheduledItem != null && EqualityComparer<ActionType>.Default.Equals(visualElementScheduledItem.updateEvent, updateEvent);
			}

			// Token: 0x0400025B RID: 603
			public ActionType updateEvent;
		}

		// Token: 0x020000C9 RID: 201
		private class TimerStateScheduledItem : VisualElement.VisualElementScheduledItem<Action<TimerState>>
		{
			// Token: 0x06000605 RID: 1541 RVA: 0x000148CB File Offset: 0x00012ACB
			public TimerStateScheduledItem(VisualElement handler, Action<TimerState> updateEvent) : base(handler, updateEvent)
			{
			}

			// Token: 0x06000606 RID: 1542 RVA: 0x000148D6 File Offset: 0x00012AD6
			public override void PerformTimerUpdate(TimerState state)
			{
				if (this.isScheduled)
				{
					this.updateEvent(state);
				}
			}
		}

		// Token: 0x020000CA RID: 202
		private class SimpleScheduledItem : VisualElement.VisualElementScheduledItem<Action>
		{
			// Token: 0x06000607 RID: 1543 RVA: 0x000148F2 File Offset: 0x00012AF2
			public SimpleScheduledItem(VisualElement handler, Action updateEvent) : base(handler, updateEvent)
			{
			}

			// Token: 0x06000608 RID: 1544 RVA: 0x000148FD File Offset: 0x00012AFD
			public override void PerformTimerUpdate(TimerState state)
			{
				if (this.isScheduled)
				{
					this.updateEvent();
				}
			}
		}
	}
}
