﻿using System;
using System.Diagnostics;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200001D RID: 29
	internal class DisposeHelper
	{
		// Token: 0x0600008E RID: 142 RVA: 0x00002223 File Offset: 0x00000423
		public DisposeHelper()
		{
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00003AF2 File Offset: 0x00001CF2
		[Conditional("UNITY_UIELEMENTS_DEBUG_DISPOSE")]
		public static void NotifyMissingDispose(IDisposable disposable)
		{
			if (disposable != null)
			{
				Debug.LogError(string.Format("An IDisposable instance of type '{0}' has not been disposed.", disposable.GetType().FullName));
			}
		}
	}
}
