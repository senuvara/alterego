﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000059 RID: 89
	internal class ObjectPool<T> where T : new()
	{
		// Token: 0x06000259 RID: 601 RVA: 0x000095A8 File Offset: 0x000077A8
		public ObjectPool(int maxSize = 100)
		{
			this.maxSize = maxSize;
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x0600025A RID: 602 RVA: 0x000095C4 File Offset: 0x000077C4
		// (set) Token: 0x0600025B RID: 603 RVA: 0x000095DF File Offset: 0x000077DF
		public int maxSize
		{
			get
			{
				return this.m_MaxSize;
			}
			set
			{
				this.m_MaxSize = Math.Max(0, value);
				while (this.Size() > this.m_MaxSize)
				{
					this.Get();
				}
			}
		}

		// Token: 0x0600025C RID: 604 RVA: 0x00009610 File Offset: 0x00007810
		public int Size()
		{
			return this.m_Stack.Count;
		}

		// Token: 0x0600025D RID: 605 RVA: 0x00009630 File Offset: 0x00007830
		public void Clear()
		{
			this.m_Stack.Clear();
		}

		// Token: 0x0600025E RID: 606 RVA: 0x00009640 File Offset: 0x00007840
		public T Get()
		{
			return (this.m_Stack.Count != 0) ? this.m_Stack.Pop() : Activator.CreateInstance<T>();
		}

		// Token: 0x0600025F RID: 607 RVA: 0x0000967C File Offset: 0x0000787C
		public void Release(T element)
		{
			if (this.m_Stack.Count > 0 && object.ReferenceEquals(this.m_Stack.Peek(), element))
			{
				Debug.LogError("Internal error. Trying to destroy object that is already released to pool.");
			}
			if (this.m_Stack.Count < this.maxSize)
			{
				this.m_Stack.Push(element);
			}
		}

		// Token: 0x040000D6 RID: 214
		private readonly Stack<T> m_Stack = new Stack<T>();

		// Token: 0x040000D7 RID: 215
		private int m_MaxSize;
	}
}
