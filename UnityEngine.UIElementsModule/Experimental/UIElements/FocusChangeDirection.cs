﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000031 RID: 49
	public class FocusChangeDirection
	{
		// Token: 0x060000DC RID: 220 RVA: 0x000054AE File Offset: 0x000036AE
		protected FocusChangeDirection(int value)
		{
			this.m_Value = value;
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000DD RID: 221 RVA: 0x000054C0 File Offset: 0x000036C0
		public static FocusChangeDirection unspecified
		{
			get
			{
				return FocusChangeDirection.s_Unspecified;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000DE RID: 222 RVA: 0x000054DC File Offset: 0x000036DC
		public static FocusChangeDirection none
		{
			get
			{
				return FocusChangeDirection.s_None;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000DF RID: 223 RVA: 0x000054F8 File Offset: 0x000036F8
		protected static FocusChangeDirection lastValue
		{
			get
			{
				return FocusChangeDirection.s_None;
			}
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00005514 File Offset: 0x00003714
		public static implicit operator int(FocusChangeDirection fcd)
		{
			return fcd.m_Value;
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x0000552F File Offset: 0x0000372F
		// Note: this type is marked as 'beforefieldinit'.
		static FocusChangeDirection()
		{
		}

		// Token: 0x04000070 RID: 112
		private static readonly FocusChangeDirection s_Unspecified = new FocusChangeDirection(-1);

		// Token: 0x04000071 RID: 113
		private static readonly FocusChangeDirection s_None = new FocusChangeDirection(0);

		// Token: 0x04000072 RID: 114
		private int m_Value;
	}
}
