﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200010E RID: 270
	public class DragEnterEvent : DragAndDropEventBase<DragEnterEvent>
	{
		// Token: 0x06000761 RID: 1889 RVA: 0x00019CEF File Offset: 0x00017EEF
		public DragEnterEvent()
		{
		}

		// Token: 0x06000762 RID: 1890 RVA: 0x00019CF8 File Offset: 0x00017EF8
		protected override void Init()
		{
			base.Init();
			base.flags = EventBase.EventFlags.TricklesDown;
		}
	}
}
