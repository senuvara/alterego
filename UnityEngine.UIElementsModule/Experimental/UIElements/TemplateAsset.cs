﻿using System;
using System.Collections.Generic;
using UnityEngine.Assertions;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200015D RID: 349
	[Serializable]
	internal class TemplateAsset : VisualElementAsset
	{
		// Token: 0x060008EF RID: 2287 RVA: 0x0001F6B0 File Offset: 0x0001D8B0
		public TemplateAsset(string templateAlias) : base(typeof(TemplateContainer).FullName)
		{
			Assert.IsFalse(string.IsNullOrEmpty(templateAlias), "Template alias must not be null or empty");
			this.m_TemplateAlias = templateAlias;
		}

		// Token: 0x17000239 RID: 569
		// (get) Token: 0x060008F0 RID: 2288 RVA: 0x0001F6E0 File Offset: 0x0001D8E0
		// (set) Token: 0x060008F1 RID: 2289 RVA: 0x0001F6FB File Offset: 0x0001D8FB
		public string templateAlias
		{
			get
			{
				return this.m_TemplateAlias;
			}
			set
			{
				this.m_TemplateAlias = value;
			}
		}

		// Token: 0x1700023A RID: 570
		// (get) Token: 0x060008F2 RID: 2290 RVA: 0x0001F708 File Offset: 0x0001D908
		// (set) Token: 0x060008F3 RID: 2291 RVA: 0x0001F723 File Offset: 0x0001D923
		internal List<VisualTreeAsset.SlotUsageEntry> slotUsages
		{
			get
			{
				return this.m_SlotUsages;
			}
			set
			{
				this.m_SlotUsages = value;
			}
		}

		// Token: 0x060008F4 RID: 2292 RVA: 0x0001F72D File Offset: 0x0001D92D
		public void AddSlotUsage(string slotName, int resId)
		{
			if (this.m_SlotUsages == null)
			{
				this.m_SlotUsages = new List<VisualTreeAsset.SlotUsageEntry>();
			}
			this.m_SlotUsages.Add(new VisualTreeAsset.SlotUsageEntry(slotName, resId));
		}

		// Token: 0x04000406 RID: 1030
		[SerializeField]
		private string m_TemplateAlias;

		// Token: 0x04000407 RID: 1031
		[SerializeField]
		private List<VisualTreeAsset.SlotUsageEntry> m_SlotUsages;
	}
}
