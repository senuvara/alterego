﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000019 RID: 25
	public class ContextualMenuManipulator : MouseManipulator
	{
		// Token: 0x0600007A RID: 122 RVA: 0x000036F8 File Offset: 0x000018F8
		public ContextualMenuManipulator(Action<ContextualMenuPopulateEvent> menuBuilder)
		{
			this.m_MenuBuilder = menuBuilder;
			base.activators.Add(new ManipulatorActivationFilter
			{
				button = MouseButton.RightMouse
			});
			if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
			{
				base.activators.Add(new ManipulatorActivationFilter
				{
					button = MouseButton.LeftMouse,
					modifiers = EventModifiers.Control
				});
			}
		}

		// Token: 0x0600007B RID: 123 RVA: 0x0000376C File Offset: 0x0000196C
		protected override void RegisterCallbacksOnTarget()
		{
			if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
			{
				base.target.RegisterCallback<MouseDownEvent>(new EventCallback<MouseDownEvent>(this.OnMouseUpDownEvent), TrickleDown.NoTrickleDown);
			}
			else
			{
				base.target.RegisterCallback<MouseUpEvent>(new EventCallback<MouseUpEvent>(this.OnMouseUpDownEvent), TrickleDown.NoTrickleDown);
			}
			base.target.RegisterCallback<KeyUpEvent>(new EventCallback<KeyUpEvent>(this.OnKeyUpEvent), TrickleDown.NoTrickleDown);
			base.target.RegisterCallback<ContextualMenuPopulateEvent>(new EventCallback<ContextualMenuPopulateEvent>(this.OnContextualMenuEvent), TrickleDown.NoTrickleDown);
		}

		// Token: 0x0600007C RID: 124 RVA: 0x000037F8 File Offset: 0x000019F8
		protected override void UnregisterCallbacksFromTarget()
		{
			if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
			{
				base.target.UnregisterCallback<MouseDownEvent>(new EventCallback<MouseDownEvent>(this.OnMouseUpDownEvent), TrickleDown.NoTrickleDown);
			}
			else
			{
				base.target.UnregisterCallback<MouseUpEvent>(new EventCallback<MouseUpEvent>(this.OnMouseUpDownEvent), TrickleDown.NoTrickleDown);
			}
			base.target.UnregisterCallback<KeyUpEvent>(new EventCallback<KeyUpEvent>(this.OnKeyUpEvent), TrickleDown.NoTrickleDown);
			base.target.UnregisterCallback<ContextualMenuPopulateEvent>(new EventCallback<ContextualMenuPopulateEvent>(this.OnContextualMenuEvent), TrickleDown.NoTrickleDown);
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00003884 File Offset: 0x00001A84
		private void OnMouseUpDownEvent(IMouseEvent evt)
		{
			if (base.CanStartManipulation(evt))
			{
				if (base.target.elementPanel != null && base.target.elementPanel.contextualMenuManager != null)
				{
					EventBase eventBase = evt as EventBase;
					base.target.elementPanel.contextualMenuManager.DisplayMenu(eventBase, base.target);
					eventBase.StopPropagation();
					eventBase.PreventDefault();
				}
			}
		}

		// Token: 0x0600007E RID: 126 RVA: 0x000038F8 File Offset: 0x00001AF8
		private void OnKeyUpEvent(KeyUpEvent evt)
		{
			if (evt.keyCode == KeyCode.Menu)
			{
				if (base.target.elementPanel != null && base.target.elementPanel.contextualMenuManager != null)
				{
					base.target.elementPanel.contextualMenuManager.DisplayMenu(evt, base.target);
					evt.StopPropagation();
					evt.PreventDefault();
				}
			}
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00003967 File Offset: 0x00001B67
		private void OnContextualMenuEvent(ContextualMenuPopulateEvent evt)
		{
			if (this.m_MenuBuilder != null)
			{
				this.m_MenuBuilder(evt);
			}
		}

		// Token: 0x04000031 RID: 49
		private Action<ContextualMenuPopulateEvent> m_MenuBuilder;
	}
}
