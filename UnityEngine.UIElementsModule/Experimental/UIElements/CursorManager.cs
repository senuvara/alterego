﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200001C RID: 28
	internal class CursorManager : ICursorManager
	{
		// Token: 0x0600008B RID: 139 RVA: 0x00002223 File Offset: 0x00000423
		public CursorManager()
		{
		}

		// Token: 0x0600008C RID: 140 RVA: 0x000035A6 File Offset: 0x000017A6
		public void SetCursor(CursorStyle cursor)
		{
		}

		// Token: 0x0600008D RID: 141 RVA: 0x000035A6 File Offset: 0x000017A6
		public void ResetCursor()
		{
		}
	}
}
