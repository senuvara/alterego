﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200010F RID: 271
	public class DragLeaveEvent : DragAndDropEventBase<DragLeaveEvent>
	{
		// Token: 0x06000763 RID: 1891 RVA: 0x00019D08 File Offset: 0x00017F08
		public DragLeaveEvent()
		{
			this.Init();
		}

		// Token: 0x06000764 RID: 1892 RVA: 0x00019D17 File Offset: 0x00017F17
		protected override void Init()
		{
			base.Init();
			base.flags = EventBase.EventFlags.TricklesDown;
		}
	}
}
