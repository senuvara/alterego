﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000160 RID: 352
	public class UxmlStringAttributeDescription : UxmlAttributeDescription
	{
		// Token: 0x06000905 RID: 2309 RVA: 0x0001F99A File Offset: 0x0001DB9A
		public UxmlStringAttributeDescription()
		{
			base.type = "string";
			base.typeNamespace = "http://www.w3.org/2001/XMLSchema";
			this.defaultValue = "";
		}

		// Token: 0x17000242 RID: 578
		// (get) Token: 0x06000906 RID: 2310 RVA: 0x0001F9C4 File Offset: 0x0001DBC4
		// (set) Token: 0x06000907 RID: 2311 RVA: 0x0001F9DE File Offset: 0x0001DBDE
		public string defaultValue
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultValue>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultValue>k__BackingField = value;
			}
		}

		// Token: 0x17000243 RID: 579
		// (get) Token: 0x06000908 RID: 2312 RVA: 0x0001F9E8 File Offset: 0x0001DBE8
		public override string defaultValueAsString
		{
			get
			{
				return this.defaultValue;
			}
		}

		// Token: 0x06000909 RID: 2313 RVA: 0x0001FA04 File Offset: 0x0001DC04
		[Obsolete("Pass a creation context to the method.")]
		public string GetValueFromBag(IUxmlAttributes bag)
		{
			return this.GetValueFromBag(bag, default(CreationContext));
		}

		// Token: 0x0600090A RID: 2314 RVA: 0x0001FA2C File Offset: 0x0001DC2C
		public string GetValueFromBag(IUxmlAttributes bag, CreationContext cc)
		{
			if (UxmlStringAttributeDescription.<>f__mg$cache0 == null)
			{
				UxmlStringAttributeDescription.<>f__mg$cache0 = new Func<string, string, string>(UxmlStringAttributeDescription.ConvertValueToString);
			}
			return base.GetValueFromBag<string>(bag, cc, UxmlStringAttributeDescription.<>f__mg$cache0, this.defaultValue);
		}

		// Token: 0x0600090B RID: 2315 RVA: 0x0001FA6C File Offset: 0x0001DC6C
		private static string ConvertValueToString(string v, string defaultValue)
		{
			return v;
		}

		// Token: 0x04000414 RID: 1044
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <defaultValue>k__BackingField;

		// Token: 0x04000415 RID: 1045
		[CompilerGenerated]
		private static Func<string, string, string> <>f__mg$cache0;
	}
}
