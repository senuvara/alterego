﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000128 RID: 296
	public class BlurEvent : FocusEventBase<BlurEvent>
	{
		// Token: 0x06000805 RID: 2053 RVA: 0x0001B315 File Offset: 0x00019515
		public BlurEvent()
		{
		}

		// Token: 0x06000806 RID: 2054 RVA: 0x0001B31D File Offset: 0x0001951D
		protected internal override void PreDispatch()
		{
			this.m_FocusController.DoFocusChange(base.relatedTarget);
		}
	}
}
