﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000099 RID: 153
	internal struct TextStylePainterParameters
	{
		// Token: 0x060003CB RID: 971 RVA: 0x0000D110 File Offset: 0x0000B310
		public static TextStylePainterParameters GetDefault(VisualElement ve, string text)
		{
			IStyle style = ve.style;
			return new TextStylePainterParameters
			{
				rect = ve.contentRect,
				text = text,
				font = style.font,
				fontSize = style.fontSize,
				fontStyle = style.fontStyleAndWeight,
				fontColor = style.color.GetSpecifiedValueOrDefault(Color.black),
				anchor = style.unityTextAlign,
				wordWrap = style.wordWrap,
				wordWrapWidth = ((!style.wordWrap) ? 0f : ve.contentRect.width),
				richText = false,
				clipping = style.textClipping
			};
		}

		// Token: 0x060003CC RID: 972 RVA: 0x0000D20C File Offset: 0x0000B40C
		public static TextStylePainterParameters GetDefault(TextElement te)
		{
			return TextStylePainterParameters.GetDefault(te, te.text);
		}

		// Token: 0x060003CD RID: 973 RVA: 0x0000D230 File Offset: 0x0000B430
		public TextNativeSettings GetTextNativeSettings(float scaling)
		{
			return new TextNativeSettings
			{
				text = this.text,
				font = this.font,
				size = this.fontSize,
				scaling = scaling,
				style = this.fontStyle,
				color = this.fontColor,
				anchor = this.anchor,
				wordWrap = this.wordWrap,
				wordWrapWidth = this.wordWrapWidth,
				richText = this.richText
			};
		}

		// Token: 0x040001B5 RID: 437
		public Rect rect;

		// Token: 0x040001B6 RID: 438
		public string text;

		// Token: 0x040001B7 RID: 439
		public Font font;

		// Token: 0x040001B8 RID: 440
		public int fontSize;

		// Token: 0x040001B9 RID: 441
		public FontStyle fontStyle;

		// Token: 0x040001BA RID: 442
		public Color fontColor;

		// Token: 0x040001BB RID: 443
		public TextAnchor anchor;

		// Token: 0x040001BC RID: 444
		public bool wordWrap;

		// Token: 0x040001BD RID: 445
		public float wordWrapWidth;

		// Token: 0x040001BE RID: 446
		public bool richText;

		// Token: 0x040001BF RID: 447
		public TextClipping clipping;
	}
}
