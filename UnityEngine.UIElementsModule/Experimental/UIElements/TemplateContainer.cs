﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200009C RID: 156
	public class TemplateContainer : BindableElement
	{
		// Token: 0x060003D1 RID: 977 RVA: 0x0000D459 File Offset: 0x0000B659
		public TemplateContainer() : this(null)
		{
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x0000D463 File Offset: 0x0000B663
		public TemplateContainer(string templateId)
		{
			this.templateId = templateId;
			this.m_ContentContainer = this;
		}

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x060003D3 RID: 979 RVA: 0x0000D47C File Offset: 0x0000B67C
		// (set) Token: 0x060003D4 RID: 980 RVA: 0x0000D496 File Offset: 0x0000B696
		public string templateId
		{
			[CompilerGenerated]
			get
			{
				return this.<templateId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<templateId>k__BackingField = value;
			}
		}

		// Token: 0x1700010A RID: 266
		// (get) Token: 0x060003D5 RID: 981 RVA: 0x0000D4A0 File Offset: 0x0000B6A0
		public override VisualElement contentContainer
		{
			get
			{
				return this.m_ContentContainer;
			}
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x0000D4BB File Offset: 0x0000B6BB
		internal void SetContentContainer(VisualElement content)
		{
			this.m_ContentContainer = content;
		}

		// Token: 0x040001CC RID: 460
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <templateId>k__BackingField;

		// Token: 0x040001CD RID: 461
		private VisualElement m_ContentContainer;

		// Token: 0x0200009D RID: 157
		public new class UxmlFactory : UxmlFactory<TemplateContainer, TemplateContainer.UxmlTraits>
		{
			// Token: 0x060003D7 RID: 983 RVA: 0x0000D4C5 File Offset: 0x0000B6C5
			public UxmlFactory()
			{
			}
		}

		// Token: 0x0200009E RID: 158
		public new class UxmlTraits : VisualElement.UxmlTraits
		{
			// Token: 0x060003D8 RID: 984 RVA: 0x0000D4D0 File Offset: 0x0000B6D0
			public UxmlTraits()
			{
			}

			// Token: 0x1700010B RID: 267
			// (get) Token: 0x060003D9 RID: 985 RVA: 0x0000D504 File Offset: 0x0000B704
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield break;
				}
			}

			// Token: 0x060003DA RID: 986 RVA: 0x0000D528 File Offset: 0x0000B728
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				TemplateContainer templateContainer = (TemplateContainer)ve;
				templateContainer.templateId = this.m_Template.GetValueFromBag(bag, cc);
				VisualTreeAsset visualTreeAsset = cc.visualTreeAsset.ResolveTemplate(templateContainer.templateId);
				if (visualTreeAsset == null)
				{
					templateContainer.Add(new Label(string.Format("Unknown Element: '{0}'", templateContainer.templateId)));
				}
				else
				{
					visualTreeAsset.CloneTree(templateContainer, cc.slotInsertionPoints);
				}
				if (visualTreeAsset == null)
				{
					Debug.LogErrorFormat("Could not resolve template with name '{0}'", new object[]
					{
						templateContainer.templateId
					});
				}
			}

			// Token: 0x040001CE RID: 462
			private UxmlStringAttributeDescription m_Template = new UxmlStringAttributeDescription
			{
				name = "template",
				use = UxmlAttributeDescription.Use.Required
			};

			// Token: 0x0200009F RID: 159
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x060003DB RID: 987 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x060003DC RID: 988 RVA: 0x0000D5CB File Offset: 0x0000B7CB
				public bool MoveNext()
				{
					bool flag = this.$PC != 0;
					this.$PC = -1;
					if (!flag)
					{
					}
					return false;
				}

				// Token: 0x1700010C RID: 268
				// (get) Token: 0x060003DD RID: 989 RVA: 0x0000D5E8 File Offset: 0x0000B7E8
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x1700010D RID: 269
				// (get) Token: 0x060003DE RID: 990 RVA: 0x0000D604 File Offset: 0x0000B804
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x060003DF RID: 991 RVA: 0x0000227E File Offset: 0x0000047E
				[DebuggerHidden]
				public void Dispose()
				{
				}

				// Token: 0x060003E0 RID: 992 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x060003E1 RID: 993 RVA: 0x0000D620 File Offset: 0x0000B820
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x060003E2 RID: 994 RVA: 0x0000D63A File Offset: 0x0000B83A
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new TemplateContainer.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x040001CF RID: 463
				internal UxmlChildElementDescription $current;

				// Token: 0x040001D0 RID: 464
				internal bool $disposing;

				// Token: 0x040001D1 RID: 465
				internal int $PC;
			}
		}
	}
}
