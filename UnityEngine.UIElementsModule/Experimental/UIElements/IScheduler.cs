﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000073 RID: 115
	public interface IScheduler
	{
		// Token: 0x06000310 RID: 784
		IScheduledItem ScheduleOnce(Action<TimerState> timerUpdateEvent, long delayMs);

		// Token: 0x06000311 RID: 785
		IScheduledItem ScheduleUntil(Action<TimerState> timerUpdateEvent, long delayMs, long intervalMs, Func<bool> stopCondition = null);

		// Token: 0x06000312 RID: 786
		IScheduledItem ScheduleForDuration(Action<TimerState> timerUpdateEvent, long delayMs, long intervalMs, long durationMs);

		// Token: 0x06000313 RID: 787
		void Unschedule(IScheduledItem item);

		// Token: 0x06000314 RID: 788
		void Schedule(IScheduledItem item);
	}
}
