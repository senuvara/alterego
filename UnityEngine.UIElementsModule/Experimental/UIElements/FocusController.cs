﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000033 RID: 51
	public class FocusController
	{
		// Token: 0x060000E4 RID: 228 RVA: 0x00005547 File Offset: 0x00003747
		public FocusController(IFocusRing focusRing)
		{
			this.focusRing = focusRing;
			this.focusedElement = null;
			this.imguiKeyboardControl = 0;
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000E5 RID: 229 RVA: 0x00005568 File Offset: 0x00003768
		private IFocusRing focusRing
		{
			[CompilerGenerated]
			get
			{
				return this.<focusRing>k__BackingField;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060000E6 RID: 230 RVA: 0x00005584 File Offset: 0x00003784
		// (set) Token: 0x060000E7 RID: 231 RVA: 0x0000559E File Offset: 0x0000379E
		public Focusable focusedElement
		{
			[CompilerGenerated]
			get
			{
				return this.<focusedElement>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<focusedElement>k__BackingField = value;
			}
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x000055A7 File Offset: 0x000037A7
		internal void DoFocusChange(Focusable f)
		{
			this.focusedElement = f;
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x000055B4 File Offset: 0x000037B4
		private void AboutToReleaseFocus(Focusable focusable, Focusable willGiveFocusTo, FocusChangeDirection direction)
		{
			using (FocusOutEvent pooled = FocusEventBase<FocusOutEvent>.GetPooled(focusable, willGiveFocusTo, direction, this))
			{
				focusable.SendEvent(pooled);
			}
		}

		// Token: 0x060000EA RID: 234 RVA: 0x000055F8 File Offset: 0x000037F8
		private void ReleaseFocus(Focusable focusable, Focusable willGiveFocusTo, FocusChangeDirection direction)
		{
			using (BlurEvent pooled = FocusEventBase<BlurEvent>.GetPooled(focusable, willGiveFocusTo, direction, this))
			{
				focusable.SendEvent(pooled);
			}
		}

		// Token: 0x060000EB RID: 235 RVA: 0x0000563C File Offset: 0x0000383C
		private void AboutToGrabFocus(Focusable focusable, Focusable willTakeFocusFrom, FocusChangeDirection direction)
		{
			using (FocusInEvent pooled = FocusEventBase<FocusInEvent>.GetPooled(focusable, willTakeFocusFrom, direction, this))
			{
				focusable.SendEvent(pooled);
			}
		}

		// Token: 0x060000EC RID: 236 RVA: 0x00005680 File Offset: 0x00003880
		private void GrabFocus(Focusable focusable, Focusable willTakeFocusFrom, FocusChangeDirection direction)
		{
			using (FocusEvent pooled = FocusEventBase<FocusEvent>.GetPooled(focusable, willTakeFocusFrom, direction, this))
			{
				focusable.SendEvent(pooled);
			}
		}

		// Token: 0x060000ED RID: 237 RVA: 0x000056C4 File Offset: 0x000038C4
		internal void SwitchFocus(Focusable newFocusedElement)
		{
			this.SwitchFocus(newFocusedElement, FocusChangeDirection.unspecified);
		}

		// Token: 0x060000EE RID: 238 RVA: 0x000056D4 File Offset: 0x000038D4
		private void SwitchFocus(Focusable newFocusedElement, FocusChangeDirection direction)
		{
			if (newFocusedElement != this.focusedElement)
			{
				Focusable focusedElement = this.focusedElement;
				if (newFocusedElement == null || !newFocusedElement.canGrabFocus)
				{
					if (focusedElement != null)
					{
						this.AboutToReleaseFocus(focusedElement, newFocusedElement, direction);
						this.ReleaseFocus(focusedElement, newFocusedElement, direction);
					}
				}
				else if (newFocusedElement != focusedElement)
				{
					if (focusedElement != null)
					{
						this.AboutToReleaseFocus(focusedElement, newFocusedElement, direction);
					}
					this.AboutToGrabFocus(newFocusedElement, focusedElement, direction);
					if (focusedElement != null)
					{
						this.ReleaseFocus(focusedElement, newFocusedElement, direction);
					}
					this.GrabFocus(newFocusedElement, focusedElement, direction);
				}
			}
		}

		// Token: 0x060000EF RID: 239 RVA: 0x0000576C File Offset: 0x0000396C
		public void SwitchFocusOnEvent(EventBase e)
		{
			FocusChangeDirection focusChangeDirection = this.focusRing.GetFocusChangeDirection(this.focusedElement, e);
			if (focusChangeDirection != FocusChangeDirection.none)
			{
				Focusable nextFocusable = this.focusRing.GetNextFocusable(this.focusedElement, focusChangeDirection);
				this.SwitchFocus(nextFocusable, focusChangeDirection);
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060000F0 RID: 240 RVA: 0x000057B8 File Offset: 0x000039B8
		// (set) Token: 0x060000F1 RID: 241 RVA: 0x000057D2 File Offset: 0x000039D2
		internal int imguiKeyboardControl
		{
			[CompilerGenerated]
			get
			{
				return this.<imguiKeyboardControl>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<imguiKeyboardControl>k__BackingField = value;
			}
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x000057DB File Offset: 0x000039DB
		internal void SyncIMGUIFocus(int imguiKeyboardControlID, Focusable imguiContainerHavingKeyboardControl)
		{
			this.imguiKeyboardControl = imguiKeyboardControlID;
			if (this.imguiKeyboardControl != 0)
			{
				this.SwitchFocus(imguiContainerHavingKeyboardControl, FocusChangeDirection.unspecified);
			}
			else
			{
				this.SwitchFocus(null, FocusChangeDirection.unspecified);
			}
		}

		// Token: 0x04000073 RID: 115
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly IFocusRing <focusRing>k__BackingField;

		// Token: 0x04000074 RID: 116
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Focusable <focusedElement>k__BackingField;

		// Token: 0x04000075 RID: 117
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <imguiKeyboardControl>k__BackingField;
	}
}
