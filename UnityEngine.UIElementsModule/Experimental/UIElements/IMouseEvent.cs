﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000131 RID: 305
	public interface IMouseEvent
	{
		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06000832 RID: 2098
		EventModifiers modifiers { get; }

		// Token: 0x17000216 RID: 534
		// (get) Token: 0x06000833 RID: 2099
		Vector2 mousePosition { get; }

		// Token: 0x17000217 RID: 535
		// (get) Token: 0x06000834 RID: 2100
		Vector2 localMousePosition { get; }

		// Token: 0x17000218 RID: 536
		// (get) Token: 0x06000835 RID: 2101
		Vector2 mouseDelta { get; }

		// Token: 0x17000219 RID: 537
		// (get) Token: 0x06000836 RID: 2102
		int clickCount { get; }

		// Token: 0x1700021A RID: 538
		// (get) Token: 0x06000837 RID: 2103
		int button { get; }

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06000838 RID: 2104
		bool shiftKey { get; }

		// Token: 0x1700021C RID: 540
		// (get) Token: 0x06000839 RID: 2105
		bool ctrlKey { get; }

		// Token: 0x1700021D RID: 541
		// (get) Token: 0x0600083A RID: 2106
		bool commandKey { get; }

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x0600083B RID: 2107
		bool altKey { get; }

		// Token: 0x1700021F RID: 543
		// (get) Token: 0x0600083C RID: 2108
		bool actionKey { get; }
	}
}
