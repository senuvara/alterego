﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000058 RID: 88
	public abstract class MouseManipulator : Manipulator
	{
		// Token: 0x06000254 RID: 596 RVA: 0x000094C8 File Offset: 0x000076C8
		protected MouseManipulator()
		{
			this.activators = new List<ManipulatorActivationFilter>();
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x06000255 RID: 597 RVA: 0x000094DC File Offset: 0x000076DC
		// (set) Token: 0x06000256 RID: 598 RVA: 0x000094F6 File Offset: 0x000076F6
		public List<ManipulatorActivationFilter> activators
		{
			[CompilerGenerated]
			get
			{
				return this.<activators>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<activators>k__BackingField = value;
			}
		}

		// Token: 0x06000257 RID: 599 RVA: 0x00009500 File Offset: 0x00007700
		protected bool CanStartManipulation(IMouseEvent e)
		{
			foreach (ManipulatorActivationFilter currentActivator in this.activators)
			{
				if (currentActivator.Matches(e))
				{
					this.m_currentActivator = currentActivator;
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000258 RID: 600 RVA: 0x00009580 File Offset: 0x00007780
		protected bool CanStopManipulation(IMouseEvent e)
		{
			return e.button == (int)this.m_currentActivator.button;
		}

		// Token: 0x040000D4 RID: 212
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private List<ManipulatorActivationFilter> <activators>k__BackingField;

		// Token: 0x040000D5 RID: 213
		private ManipulatorActivationFilter m_currentActivator;
	}
}
