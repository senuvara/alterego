﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000120 RID: 288
	internal class EventCallbackList
	{
		// Token: 0x060007B9 RID: 1977 RVA: 0x0001A71D File Offset: 0x0001891D
		public EventCallbackList()
		{
			this.m_List = new List<EventCallbackFunctorBase>();
			this.trickleDownCallbackCount = 0;
			this.bubbleUpCallbackCount = 0;
		}

		// Token: 0x060007BA RID: 1978 RVA: 0x0001A73F File Offset: 0x0001893F
		public EventCallbackList(EventCallbackList source)
		{
			this.m_List = new List<EventCallbackFunctorBase>(source.m_List);
			this.trickleDownCallbackCount = 0;
			this.bubbleUpCallbackCount = 0;
		}

		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x060007BB RID: 1979 RVA: 0x0001A768 File Offset: 0x00018968
		// (set) Token: 0x060007BC RID: 1980 RVA: 0x0001A782 File Offset: 0x00018982
		public int trickleDownCallbackCount
		{
			[CompilerGenerated]
			get
			{
				return this.<trickleDownCallbackCount>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<trickleDownCallbackCount>k__BackingField = value;
			}
		}

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x060007BD RID: 1981 RVA: 0x0001A78C File Offset: 0x0001898C
		// (set) Token: 0x060007BE RID: 1982 RVA: 0x0001A7A6 File Offset: 0x000189A6
		public int bubbleUpCallbackCount
		{
			[CompilerGenerated]
			get
			{
				return this.<bubbleUpCallbackCount>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<bubbleUpCallbackCount>k__BackingField = value;
			}
		}

		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x060007BF RID: 1983 RVA: 0x0001A7B0 File Offset: 0x000189B0
		[Obsolete("Use trickleDownCallbackCount instead of capturingCallbackCount.")]
		public int capturingCallbackCount
		{
			get
			{
				return this.trickleDownCallbackCount;
			}
		}

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x060007C0 RID: 1984 RVA: 0x0001A7CC File Offset: 0x000189CC
		[Obsolete("Use bubbleUpCallbackCount instead of bubblingCallbackCount.")]
		public int bubblingCallbackCount
		{
			get
			{
				return this.bubbleUpCallbackCount;
			}
		}

		// Token: 0x060007C1 RID: 1985 RVA: 0x0001A7E8 File Offset: 0x000189E8
		public bool Contains(long eventTypeId, Delegate callback, CallbackPhase phase)
		{
			return this.Find(eventTypeId, callback, phase) != null;
		}

		// Token: 0x060007C2 RID: 1986 RVA: 0x0001A80C File Offset: 0x00018A0C
		public EventCallbackFunctorBase Find(long eventTypeId, Delegate callback, CallbackPhase phase)
		{
			for (int i = 0; i < this.m_List.Count; i++)
			{
				if (this.m_List[i].IsEquivalentTo(eventTypeId, callback, phase))
				{
					return this.m_List[i];
				}
			}
			return null;
		}

		// Token: 0x060007C3 RID: 1987 RVA: 0x0001A86C File Offset: 0x00018A6C
		public bool Remove(long eventTypeId, Delegate callback, CallbackPhase phase)
		{
			for (int i = 0; i < this.m_List.Count; i++)
			{
				if (this.m_List[i].IsEquivalentTo(eventTypeId, callback, phase))
				{
					this.m_List.RemoveAt(i);
					if (phase == CallbackPhase.TrickleDownAndTarget)
					{
						this.trickleDownCallbackCount--;
					}
					else if (phase == CallbackPhase.TargetAndBubbleUp)
					{
						this.bubbleUpCallbackCount--;
					}
					return true;
				}
			}
			return false;
		}

		// Token: 0x060007C4 RID: 1988 RVA: 0x0001A900 File Offset: 0x00018B00
		public void Add(EventCallbackFunctorBase item)
		{
			this.m_List.Add(item);
			if (item.phase == CallbackPhase.TrickleDownAndTarget)
			{
				this.trickleDownCallbackCount++;
			}
			else if (item.phase == CallbackPhase.TargetAndBubbleUp)
			{
				this.bubbleUpCallbackCount++;
			}
		}

		// Token: 0x060007C5 RID: 1989 RVA: 0x0001A958 File Offset: 0x00018B58
		public void AddRange(EventCallbackList list)
		{
			this.m_List.AddRange(list.m_List);
			foreach (EventCallbackFunctorBase eventCallbackFunctorBase in list.m_List)
			{
				if (eventCallbackFunctorBase.phase == CallbackPhase.TrickleDownAndTarget)
				{
					this.trickleDownCallbackCount++;
				}
				else if (eventCallbackFunctorBase.phase == CallbackPhase.TargetAndBubbleUp)
				{
					this.bubbleUpCallbackCount++;
				}
			}
		}

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x060007C6 RID: 1990 RVA: 0x0001AA00 File Offset: 0x00018C00
		public int Count
		{
			get
			{
				return this.m_List.Count;
			}
		}

		// Token: 0x170001FC RID: 508
		public EventCallbackFunctorBase this[int i]
		{
			get
			{
				return this.m_List[i];
			}
			set
			{
				this.m_List[i] = value;
			}
		}

		// Token: 0x060007C9 RID: 1993 RVA: 0x0001AA51 File Offset: 0x00018C51
		public void Clear()
		{
			this.m_List.Clear();
			this.trickleDownCallbackCount = 0;
			this.bubbleUpCallbackCount = 0;
		}

		// Token: 0x04000308 RID: 776
		private List<EventCallbackFunctorBase> m_List;

		// Token: 0x04000309 RID: 777
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <trickleDownCallbackCount>k__BackingField;

		// Token: 0x0400030A RID: 778
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <bubbleUpCallbackCount>k__BackingField;
	}
}
