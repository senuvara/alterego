﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000057 RID: 87
	public static class MouseCaptureController
	{
		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x06000249 RID: 585 RVA: 0x000092F0 File Offset: 0x000074F0
		// (set) Token: 0x0600024A RID: 586 RVA: 0x00009309 File Offset: 0x00007509
		internal static IEventHandler mouseCapture
		{
			[CompilerGenerated]
			get
			{
				return MouseCaptureController.<mouseCapture>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				MouseCaptureController.<mouseCapture>k__BackingField = value;
			}
		}

		// Token: 0x0600024B RID: 587 RVA: 0x00009314 File Offset: 0x00007514
		[Obsolete("Use IsMouseCaptured instead of IsMouseCaptureTaken.")]
		public static bool IsMouseCaptureTaken()
		{
			return MouseCaptureController.IsMouseCaptured();
		}

		// Token: 0x0600024C RID: 588 RVA: 0x00009330 File Offset: 0x00007530
		public static bool IsMouseCaptured()
		{
			return MouseCaptureController.mouseCapture != null;
		}

		// Token: 0x0600024D RID: 589 RVA: 0x00009350 File Offset: 0x00007550
		public static bool HasMouseCapture(this IEventHandler handler)
		{
			return MouseCaptureController.mouseCapture == handler;
		}

		// Token: 0x0600024E RID: 590 RVA: 0x0000936D File Offset: 0x0000756D
		[Obsolete("Use CaptureMouse instead of TakeMouseCapture.")]
		public static void TakeMouseCapture(this IEventHandler handler)
		{
			handler.CaptureMouse();
		}

		// Token: 0x0600024F RID: 591 RVA: 0x00009378 File Offset: 0x00007578
		public static void CaptureMouse(this IEventHandler handler)
		{
			if (MouseCaptureController.mouseCapture != handler)
			{
				if (handler == null)
				{
					MouseCaptureController.ReleaseMouse();
				}
				else if (GUIUtility.hotControl != 0)
				{
					Debug.Log("Should not be capturing when there is a hotcontrol");
				}
				else
				{
					IEventHandler mouseCapture = MouseCaptureController.mouseCapture;
					MouseCaptureController.mouseCapture = handler;
					if (mouseCapture != null)
					{
						using (MouseCaptureOutEvent pooled = MouseCaptureEventBase<MouseCaptureOutEvent>.GetPooled(mouseCapture, MouseCaptureController.mouseCapture))
						{
							mouseCapture.SendEvent(pooled);
						}
					}
					using (MouseCaptureEvent pooled2 = MouseCaptureEventBase<MouseCaptureEvent>.GetPooled(MouseCaptureController.mouseCapture, mouseCapture))
					{
						MouseCaptureController.mouseCapture.SendEvent(pooled2);
					}
				}
			}
		}

		// Token: 0x06000250 RID: 592 RVA: 0x00009444 File Offset: 0x00007644
		[Obsolete("Use ReleaseMouse instead of ReleaseMouseCapture.")]
		public static void ReleaseMouseCapture(this IEventHandler handler)
		{
			handler.ReleaseMouse();
		}

		// Token: 0x06000251 RID: 593 RVA: 0x0000944D File Offset: 0x0000764D
		public static void ReleaseMouse(this IEventHandler handler)
		{
			if (handler == MouseCaptureController.mouseCapture)
			{
				MouseCaptureController.ReleaseMouse();
			}
		}

		// Token: 0x06000252 RID: 594 RVA: 0x00009462 File Offset: 0x00007662
		[Obsolete("Use ReleaseMouse instead of ReleaseMouseCapture.")]
		public static void ReleaseMouseCapture()
		{
			MouseCaptureController.ReleaseMouse();
		}

		// Token: 0x06000253 RID: 595 RVA: 0x0000946C File Offset: 0x0000766C
		public static void ReleaseMouse()
		{
			if (MouseCaptureController.mouseCapture != null)
			{
				IEventHandler mouseCapture = MouseCaptureController.mouseCapture;
				MouseCaptureController.mouseCapture = null;
				using (MouseCaptureOutEvent pooled = MouseCaptureEventBase<MouseCaptureOutEvent>.GetPooled(mouseCapture, null))
				{
					mouseCapture.SendEvent(pooled);
				}
			}
		}

		// Token: 0x040000D3 RID: 211
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static IEventHandler <mouseCapture>k__BackingField;
	}
}
