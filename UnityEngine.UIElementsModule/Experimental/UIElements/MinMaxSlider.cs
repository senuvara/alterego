﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000052 RID: 82
	public class MinMaxSlider : BaseField<Vector2>
	{
		// Token: 0x06000225 RID: 549 RVA: 0x000084DF File Offset: 0x000066DF
		public MinMaxSlider() : this(0f, 10f, float.MinValue, float.MaxValue)
		{
		}

		// Token: 0x06000226 RID: 550 RVA: 0x000084FC File Offset: 0x000066FC
		public MinMaxSlider(float minValue, float maxValue, float minLimit, float maxLimit)
		{
			this.m_DragState = MinMaxSlider.DragState.NoThumb;
			base.Add(new VisualElement
			{
				name = "TrackElement"
			});
			this.dragElement = new VisualElement
			{
				name = "DragElement"
			};
			this.dragElement.RegisterCallback<GeometryChangedEvent>(new EventCallback<GeometryChangedEvent>(this.UpdateDragElementPosition), TrickleDown.NoTrickleDown);
			base.Add(this.dragElement);
			this.dragMinThumb = new VisualElement();
			this.dragMaxThumb = new VisualElement();
			this.dragMinThumb.AddToClassList("thumbelement");
			this.dragMaxThumb.AddToClassList("thumbelement");
			this.dragElement.Add(this.dragMinThumb);
			this.dragElement.Add(this.dragMaxThumb);
			this.clampedDragger = new ClampedDragger<float>(null, new Action(this.SetSliderValueFromClick), new Action(this.SetSliderValueFromDrag));
			this.AddManipulator(this.clampedDragger);
			this.m_MinLimit = minLimit;
			this.m_MaxLimit = maxLimit;
			this.m_Value = this.ClampValues(new Vector2(minValue, maxValue));
			this.UpdateDragElementPosition();
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000227 RID: 551 RVA: 0x0000861C File Offset: 0x0000681C
		// (set) Token: 0x06000228 RID: 552 RVA: 0x00008636 File Offset: 0x00006836
		internal VisualElement dragElement
		{
			[CompilerGenerated]
			get
			{
				return this.<dragElement>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<dragElement>k__BackingField = value;
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000229 RID: 553 RVA: 0x00008640 File Offset: 0x00006840
		// (set) Token: 0x0600022A RID: 554 RVA: 0x0000865A File Offset: 0x0000685A
		private VisualElement dragMinThumb
		{
			[CompilerGenerated]
			get
			{
				return this.<dragMinThumb>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<dragMinThumb>k__BackingField = value;
			}
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x0600022B RID: 555 RVA: 0x00008664 File Offset: 0x00006864
		// (set) Token: 0x0600022C RID: 556 RVA: 0x0000867E File Offset: 0x0000687E
		private VisualElement dragMaxThumb
		{
			[CompilerGenerated]
			get
			{
				return this.<dragMaxThumb>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<dragMaxThumb>k__BackingField = value;
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x0600022D RID: 557 RVA: 0x00008688 File Offset: 0x00006888
		// (set) Token: 0x0600022E RID: 558 RVA: 0x000086A2 File Offset: 0x000068A2
		internal ClampedDragger<float> clampedDragger
		{
			[CompilerGenerated]
			get
			{
				return this.<clampedDragger>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<clampedDragger>k__BackingField = value;
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x0600022F RID: 559 RVA: 0x000086AC File Offset: 0x000068AC
		// (set) Token: 0x06000230 RID: 560 RVA: 0x000086CF File Offset: 0x000068CF
		public float minValue
		{
			get
			{
				return this.value.x;
			}
			set
			{
				base.value = this.ClampValues(new Vector2(value, this.m_Value.y));
			}
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x06000231 RID: 561 RVA: 0x000086F0 File Offset: 0x000068F0
		// (set) Token: 0x06000232 RID: 562 RVA: 0x00008713 File Offset: 0x00006913
		public float maxValue
		{
			get
			{
				return this.value.y;
			}
			set
			{
				base.value = this.ClampValues(new Vector2(this.m_Value.x, value));
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x06000233 RID: 563 RVA: 0x00008734 File Offset: 0x00006934
		// (set) Token: 0x06000234 RID: 564 RVA: 0x0000874F File Offset: 0x0000694F
		public override Vector2 value
		{
			get
			{
				return base.value;
			}
			set
			{
				base.value = this.ClampValues(value);
			}
		}

		// Token: 0x06000235 RID: 565 RVA: 0x0000875F File Offset: 0x0000695F
		public override void SetValueWithoutNotify(Vector2 newValue)
		{
			base.SetValueWithoutNotify(this.ClampValues(newValue));
			this.UpdateDragElementPosition();
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x06000236 RID: 566 RVA: 0x00008778 File Offset: 0x00006978
		public float range
		{
			get
			{
				return Math.Abs(this.highLimit - this.lowLimit);
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000237 RID: 567 RVA: 0x000087A0 File Offset: 0x000069A0
		// (set) Token: 0x06000238 RID: 568 RVA: 0x000087BC File Offset: 0x000069BC
		public float lowLimit
		{
			get
			{
				return this.m_MinLimit;
			}
			set
			{
				if (!Mathf.Approximately(this.m_MinLimit, value))
				{
					if (value > this.m_MaxLimit)
					{
						throw new ArgumentException("lowLimit is greater than highLimit");
					}
					this.m_MinLimit = value;
					this.value = this.m_Value;
					this.UpdateDragElementPosition();
					if (!string.IsNullOrEmpty(base.persistenceKey))
					{
						base.SavePersistentData();
					}
				}
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000239 RID: 569 RVA: 0x00008824 File Offset: 0x00006A24
		// (set) Token: 0x0600023A RID: 570 RVA: 0x00008840 File Offset: 0x00006A40
		public float highLimit
		{
			get
			{
				return this.m_MaxLimit;
			}
			set
			{
				if (!Mathf.Approximately(this.m_MaxLimit, value))
				{
					if (value < this.m_MinLimit)
					{
						throw new ArgumentException("highLimit is smaller than lowLimit");
					}
					this.m_MaxLimit = value;
					this.value = this.m_Value;
					this.UpdateDragElementPosition();
					if (!string.IsNullOrEmpty(base.persistenceKey))
					{
						base.SavePersistentData();
					}
				}
			}
		}

		// Token: 0x0600023B RID: 571 RVA: 0x000088A8 File Offset: 0x00006AA8
		private Vector2 ClampValues(Vector2 valueToClamp)
		{
			if (this.m_MinLimit > this.m_MaxLimit)
			{
				this.m_MinLimit = this.m_MaxLimit;
			}
			Vector2 result = default(Vector2);
			if (valueToClamp.y > this.m_MaxLimit)
			{
				valueToClamp.y = this.m_MaxLimit;
			}
			result.x = Mathf.Clamp(valueToClamp.x, this.m_MinLimit, valueToClamp.y);
			result.y = Mathf.Clamp(valueToClamp.y, valueToClamp.x, this.m_MaxLimit);
			return result;
		}

		// Token: 0x0600023C RID: 572 RVA: 0x00008948 File Offset: 0x00006B48
		private void UpdateDragElementPosition(GeometryChangedEvent evt)
		{
			if (!(evt.oldRect.size == evt.newRect.size))
			{
				this.UpdateDragElementPosition();
			}
		}

		// Token: 0x0600023D RID: 573 RVA: 0x00008988 File Offset: 0x00006B88
		private void UpdateDragElementPosition()
		{
			if (base.panel != null)
			{
				int num = this.dragElement.style.sliceLeft + this.dragElement.style.sliceRight;
				float num2 = Mathf.Round(this.SliderLerpUnclamped((float)this.dragElement.style.sliceLeft, base.layout.width - (float)this.dragElement.style.sliceRight, this.SliderNormalizeValue(this.minValue, this.lowLimit, this.highLimit)) - (float)this.dragElement.style.sliceLeft);
				float num3 = Mathf.Round(this.SliderLerpUnclamped((float)this.dragElement.style.sliceLeft, base.layout.width - (float)this.dragElement.style.sliceRight, this.SliderNormalizeValue(this.maxValue, this.lowLimit, this.highLimit)) + (float)this.dragElement.style.sliceRight);
				this.dragElement.style.width = Mathf.Max((float)num, num3 - num2);
				this.dragElement.style.positionLeft = num2;
				this.m_DragMinThumbRect = new Rect(this.dragElement.style.positionLeft, this.dragElement.layout.yMin, (float)this.dragElement.style.sliceLeft, this.dragElement.style.height);
				this.m_DragMaxThumbRect = new Rect(this.dragElement.style.positionLeft + (this.dragElement.style.width - (float)this.dragElement.style.sliceRight), this.dragElement.layout.yMin, (float)this.dragElement.style.sliceRight, this.dragElement.style.height);
				this.dragMaxThumb.style.positionLeft = this.dragElement.style.width - (float)this.dragElement.style.sliceRight;
				this.dragMaxThumb.style.positionTop = 0f;
				this.dragMinThumb.style.width = this.m_DragMinThumbRect.width;
				this.dragMinThumb.style.height = this.m_DragMinThumbRect.height;
				this.dragMinThumb.style.positionLeft = 0f;
				this.dragMinThumb.style.positionTop = 0f;
				this.dragMaxThumb.style.width = this.m_DragMaxThumbRect.width;
				this.dragMaxThumb.style.height = this.m_DragMaxThumbRect.height;
			}
		}

		// Token: 0x0600023E RID: 574 RVA: 0x00008CF0 File Offset: 0x00006EF0
		internal float SliderLerpUnclamped(float a, float b, float interpolant)
		{
			return Mathf.LerpUnclamped(a, b, interpolant);
		}

		// Token: 0x0600023F RID: 575 RVA: 0x00008D10 File Offset: 0x00006F10
		internal float SliderNormalizeValue(float currentValue, float lowerValue, float higherValue)
		{
			return (currentValue - lowerValue) / (higherValue - lowerValue);
		}

		// Token: 0x06000240 RID: 576 RVA: 0x00008D2C File Offset: 0x00006F2C
		private float ComputeValueFromPosition(float positionToConvert)
		{
			float interpolant = this.SliderNormalizeValue(positionToConvert, (float)this.dragElement.style.sliceLeft, base.layout.width - (float)this.dragElement.style.sliceRight);
			return this.SliderLerpUnclamped(this.lowLimit, this.highLimit, interpolant);
		}

		// Token: 0x06000241 RID: 577 RVA: 0x00008D9D File Offset: 0x00006F9D
		protected internal override void ExecuteDefaultAction(EventBase evt)
		{
			base.ExecuteDefaultAction(evt);
			if (evt.GetEventTypeId() == EventBase<GeometryChangedEvent>.TypeId())
			{
				this.UpdateDragElementPosition((GeometryChangedEvent)evt);
			}
		}

		// Token: 0x06000242 RID: 578 RVA: 0x00008DC8 File Offset: 0x00006FC8
		private void SetSliderValueFromDrag()
		{
			if (this.clampedDragger.dragDirection == ClampedDragger<float>.DragDirection.Free)
			{
				float x = this.m_DragElementStartPos.x;
				float dragElementEndPos = x + this.clampedDragger.delta.x;
				this.ComputeValueFromDraggingThumb(x, dragElementEndPos);
			}
		}

		// Token: 0x06000243 RID: 579 RVA: 0x00008E18 File Offset: 0x00007018
		private void SetSliderValueFromClick()
		{
			if (this.clampedDragger.dragDirection != ClampedDragger<float>.DragDirection.Free)
			{
				if (this.m_DragMinThumbRect.Contains(this.clampedDragger.startMousePosition))
				{
					this.m_DragState = MinMaxSlider.DragState.MinThumb;
				}
				else if (this.m_DragMaxThumbRect.Contains(this.clampedDragger.startMousePosition))
				{
					this.m_DragState = MinMaxSlider.DragState.MaxThumb;
				}
				else if (this.dragElement.layout.Contains(this.clampedDragger.startMousePosition))
				{
					this.m_DragState = MinMaxSlider.DragState.MiddleThumb;
				}
				else
				{
					this.m_DragState = MinMaxSlider.DragState.NoThumb;
				}
				if (this.m_DragState == MinMaxSlider.DragState.NoThumb)
				{
					this.m_DragElementStartPos = new Vector2(this.clampedDragger.startMousePosition.x, this.dragElement.style.positionTop.value);
					this.clampedDragger.dragDirection = ClampedDragger<float>.DragDirection.Free;
					this.ComputeValueDragStateNoThumb((float)this.dragElement.style.sliceLeft, base.layout.width - (float)this.dragElement.style.sliceRight, this.m_DragElementStartPos.x);
					this.m_DragState = MinMaxSlider.DragState.MiddleThumb;
					this.m_ValueStartPos = this.value;
				}
				else
				{
					this.m_ValueStartPos = this.value;
					this.clampedDragger.dragDirection = ClampedDragger<float>.DragDirection.Free;
					this.m_DragElementStartPos = this.clampedDragger.startMousePosition;
				}
			}
		}

		// Token: 0x06000244 RID: 580 RVA: 0x00008FA8 File Offset: 0x000071A8
		private void ComputeValueDragStateNoThumb(float lowLimitPosition, float highLimitPosition, float dragElementPos)
		{
			float num;
			if (dragElementPos < lowLimitPosition)
			{
				num = this.lowLimit;
			}
			else if (dragElementPos > highLimitPosition)
			{
				num = this.highLimit;
			}
			else
			{
				num = this.ComputeValueFromPosition(dragElementPos);
			}
			float num2 = this.maxValue - this.minValue;
			float num3 = num - num2;
			float y = num;
			if (num3 < this.lowLimit)
			{
				num3 = this.lowLimit;
				y = num3 + num2;
			}
			this.value = new Vector2(num3, y);
		}

		// Token: 0x06000245 RID: 581 RVA: 0x00009024 File Offset: 0x00007224
		private void ComputeValueFromDraggingThumb(float dragElementStartPos, float dragElementEndPos)
		{
			float num = this.ComputeValueFromPosition(dragElementStartPos);
			float num2 = this.ComputeValueFromPosition(dragElementEndPos);
			float num3 = num2 - num;
			MinMaxSlider.DragState dragState = this.m_DragState;
			if (dragState != MinMaxSlider.DragState.MiddleThumb)
			{
				if (dragState != MinMaxSlider.DragState.MinThumb)
				{
					if (dragState == MinMaxSlider.DragState.MaxThumb)
					{
						float num4 = this.m_ValueStartPos.y + num3;
						if (num4 < this.minValue)
						{
							num4 = this.minValue;
						}
						else if (num4 > this.highLimit)
						{
							num4 = this.highLimit;
						}
						this.value = new Vector2(this.minValue, num4);
					}
				}
				else
				{
					float num5 = this.m_ValueStartPos.x + num3;
					if (num5 > this.maxValue)
					{
						num5 = this.maxValue;
					}
					else if (num5 < this.lowLimit)
					{
						num5 = this.lowLimit;
					}
					this.value = new Vector2(num5, this.maxValue);
				}
			}
			else
			{
				Vector2 value = this.value;
				value.x = this.m_ValueStartPos.x + num3;
				value.y = this.m_ValueStartPos.y + num3;
				float num6 = this.m_ValueStartPos.y - this.m_ValueStartPos.x;
				if (value.x < this.lowLimit)
				{
					value.x = this.lowLimit;
					value.y = this.lowLimit + num6;
				}
				else if (value.y > this.highLimit)
				{
					value.y = this.highLimit;
					value.x = this.highLimit - num6;
				}
				this.value = value;
			}
		}

		// Token: 0x040000BA RID: 186
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private VisualElement <dragElement>k__BackingField;

		// Token: 0x040000BB RID: 187
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VisualElement <dragMinThumb>k__BackingField;

		// Token: 0x040000BC RID: 188
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VisualElement <dragMaxThumb>k__BackingField;

		// Token: 0x040000BD RID: 189
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private ClampedDragger<float> <clampedDragger>k__BackingField;

		// Token: 0x040000BE RID: 190
		private Vector2 m_DragElementStartPos;

		// Token: 0x040000BF RID: 191
		private Vector2 m_ValueStartPos;

		// Token: 0x040000C0 RID: 192
		private Rect m_DragMinThumbRect;

		// Token: 0x040000C1 RID: 193
		private Rect m_DragMaxThumbRect;

		// Token: 0x040000C2 RID: 194
		private MinMaxSlider.DragState m_DragState;

		// Token: 0x040000C3 RID: 195
		private float m_MinLimit;

		// Token: 0x040000C4 RID: 196
		private float m_MaxLimit;

		// Token: 0x040000C5 RID: 197
		internal const float kDefaultHighValue = 10f;

		// Token: 0x02000053 RID: 83
		public new class UxmlFactory : UxmlFactory<MinMaxSlider, MinMaxSlider.UxmlTraits>
		{
			// Token: 0x06000246 RID: 582 RVA: 0x000091DB File Offset: 0x000073DB
			public UxmlFactory()
			{
			}
		}

		// Token: 0x02000054 RID: 84
		public new class UxmlTraits : BaseField<Vector2>.UxmlTraits
		{
			// Token: 0x06000247 RID: 583 RVA: 0x000091E4 File Offset: 0x000073E4
			public UxmlTraits()
			{
			}

			// Token: 0x06000248 RID: 584 RVA: 0x00009284 File Offset: 0x00007484
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				MinMaxSlider minMaxSlider = (MinMaxSlider)ve;
				minMaxSlider.SetValueWithoutNotify(new Vector2(this.m_MinValue.GetValueFromBag(bag, cc), this.m_MaxValue.GetValueFromBag(bag, cc)));
				minMaxSlider.lowLimit = this.m_LowLimit.GetValueFromBag(bag, cc);
				minMaxSlider.highLimit = this.m_HighLimit.GetValueFromBag(bag, cc);
			}

			// Token: 0x040000C6 RID: 198
			private UxmlFloatAttributeDescription m_MinValue = new UxmlFloatAttributeDescription
			{
				name = "min-value",
				defaultValue = 0f
			};

			// Token: 0x040000C7 RID: 199
			private UxmlFloatAttributeDescription m_MaxValue = new UxmlFloatAttributeDescription
			{
				name = "max-value",
				defaultValue = 10f
			};

			// Token: 0x040000C8 RID: 200
			private UxmlFloatAttributeDescription m_LowLimit = new UxmlFloatAttributeDescription
			{
				name = "low-limit",
				defaultValue = float.MinValue
			};

			// Token: 0x040000C9 RID: 201
			private UxmlFloatAttributeDescription m_HighLimit = new UxmlFloatAttributeDescription
			{
				name = "high-limit",
				defaultValue = float.MaxValue
			};
		}

		// Token: 0x02000055 RID: 85
		private enum DragState
		{
			// Token: 0x040000CB RID: 203
			NoThumb,
			// Token: 0x040000CC RID: 204
			MinThumb,
			// Token: 0x040000CD RID: 205
			MiddleThumb,
			// Token: 0x040000CE RID: 206
			MaxThumb
		}
	}
}
