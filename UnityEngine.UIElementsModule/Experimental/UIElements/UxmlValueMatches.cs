﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000175 RID: 373
	public class UxmlValueMatches : UxmlTypeRestriction
	{
		// Token: 0x0600099A RID: 2458 RVA: 0x00021229 File Offset: 0x0001F429
		public UxmlValueMatches()
		{
		}

		// Token: 0x0600099B RID: 2459 RVA: 0x00021234 File Offset: 0x0001F434
		public override bool Equals(UxmlTypeRestriction other)
		{
			UxmlValueMatches uxmlValueMatches = other as UxmlValueMatches;
			return uxmlValueMatches != null && this.regex == uxmlValueMatches.regex;
		}

		// Token: 0x04000449 RID: 1097
		public string regex;
	}
}
