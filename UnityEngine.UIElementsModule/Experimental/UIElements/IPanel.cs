﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200005E RID: 94
	public interface IPanel : IDisposable
	{
		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000269 RID: 617
		VisualElement visualTree { get; }

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x0600026A RID: 618
		EventDispatcher dispatcher { get; }

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x0600026B RID: 619
		ContextType contextType { get; }

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x0600026C RID: 620
		FocusController focusController { get; }

		// Token: 0x0600026D RID: 621
		VisualElement Pick(Vector2 point);

		// Token: 0x0600026E RID: 622
		VisualElement LoadTemplate(string path, Dictionary<string, VisualElement> slots = null);

		// Token: 0x0600026F RID: 623
		VisualElement PickAll(Vector2 point, List<VisualElement> picked);
	}
}
