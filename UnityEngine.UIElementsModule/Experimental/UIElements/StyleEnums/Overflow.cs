﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleEnums
{
	// Token: 0x0200008D RID: 141
	public enum Overflow
	{
		// Token: 0x04000181 RID: 385
		Visible,
		// Token: 0x04000182 RID: 386
		Hidden
	}
}
