﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleEnums
{
	// Token: 0x02000091 RID: 145
	public enum Justify
	{
		// Token: 0x04000193 RID: 403
		FlexStart,
		// Token: 0x04000194 RID: 404
		Center,
		// Token: 0x04000195 RID: 405
		FlexEnd,
		// Token: 0x04000196 RID: 406
		SpaceBetween,
		// Token: 0x04000197 RID: 407
		SpaceAround
	}
}
