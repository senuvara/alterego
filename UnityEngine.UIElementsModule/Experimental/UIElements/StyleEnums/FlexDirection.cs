﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleEnums
{
	// Token: 0x0200008E RID: 142
	public enum FlexDirection
	{
		// Token: 0x04000184 RID: 388
		Column,
		// Token: 0x04000185 RID: 389
		ColumnReverse,
		// Token: 0x04000186 RID: 390
		Row,
		// Token: 0x04000187 RID: 391
		RowReverse
	}
}
