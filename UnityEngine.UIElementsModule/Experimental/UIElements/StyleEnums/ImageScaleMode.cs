﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleEnums
{
	// Token: 0x02000092 RID: 146
	public enum ImageScaleMode
	{
		// Token: 0x04000199 RID: 409
		StretchToFill,
		// Token: 0x0400019A RID: 410
		ScaleAndCrop,
		// Token: 0x0400019B RID: 411
		ScaleToFit
	}
}
