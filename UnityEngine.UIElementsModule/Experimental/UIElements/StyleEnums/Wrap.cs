﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleEnums
{
	// Token: 0x0200008F RID: 143
	public enum Wrap
	{
		// Token: 0x04000189 RID: 393
		NoWrap,
		// Token: 0x0400018A RID: 394
		Wrap,
		// Token: 0x0400018B RID: 395
		WrapReverse
	}
}
