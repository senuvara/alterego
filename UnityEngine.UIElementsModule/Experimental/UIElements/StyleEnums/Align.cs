﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleEnums
{
	// Token: 0x02000090 RID: 144
	public enum Align
	{
		// Token: 0x0400018D RID: 397
		Auto,
		// Token: 0x0400018E RID: 398
		FlexStart,
		// Token: 0x0400018F RID: 399
		Center,
		// Token: 0x04000190 RID: 400
		FlexEnd,
		// Token: 0x04000191 RID: 401
		Stretch
	}
}
