﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleEnums
{
	// Token: 0x0200008B RID: 139
	internal enum Position
	{
		// Token: 0x0400017A RID: 378
		Relative,
		// Token: 0x0400017B RID: 379
		Absolute
	}
}
