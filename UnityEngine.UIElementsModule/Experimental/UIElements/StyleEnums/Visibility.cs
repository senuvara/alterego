﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleEnums
{
	// Token: 0x02000093 RID: 147
	public enum Visibility
	{
		// Token: 0x0400019D RID: 413
		Visible,
		// Token: 0x0400019E RID: 414
		Hidden
	}
}
