﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleEnums
{
	// Token: 0x0200008C RID: 140
	public enum PositionType
	{
		// Token: 0x0400017D RID: 381
		Relative,
		// Token: 0x0400017E RID: 382
		Absolute,
		// Token: 0x0400017F RID: 383
		Manual
	}
}
