﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200011E RID: 286
	internal enum CallbackPhase
	{
		// Token: 0x04000304 RID: 772
		TargetAndBubbleUp = 1,
		// Token: 0x04000305 RID: 773
		TrickleDownAndTarget,
		// Token: 0x04000306 RID: 774
		[Obsolete("Use TrickleDownAndTarget instead of CaptureAndTarget.")]
		CaptureAndTarget = 2
	}
}
