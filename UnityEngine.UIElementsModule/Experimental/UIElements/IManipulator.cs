﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000050 RID: 80
	public interface IManipulator
	{
		// Token: 0x17000097 RID: 151
		// (get) Token: 0x0600021E RID: 542
		// (set) Token: 0x0600021F RID: 543
		VisualElement target { get; set; }
	}
}
