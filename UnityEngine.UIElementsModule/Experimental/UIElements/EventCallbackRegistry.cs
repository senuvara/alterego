﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000121 RID: 289
	internal class EventCallbackRegistry
	{
		// Token: 0x060007CA RID: 1994 RVA: 0x0001AA6D File Offset: 0x00018C6D
		public EventCallbackRegistry()
		{
			this.m_IsInvoking = 0;
		}

		// Token: 0x060007CB RID: 1995 RVA: 0x0001AA80 File Offset: 0x00018C80
		private static EventCallbackList GetCallbackList(EventCallbackList initializer = null)
		{
			return EventCallbackRegistry.s_ListPool.Get(initializer);
		}

		// Token: 0x060007CC RID: 1996 RVA: 0x0001AAA0 File Offset: 0x00018CA0
		private static void ReleaseCallbackList(EventCallbackList toRelease)
		{
			EventCallbackRegistry.s_ListPool.Release(toRelease);
		}

		// Token: 0x060007CD RID: 1997 RVA: 0x0001AAB0 File Offset: 0x00018CB0
		private EventCallbackList GetCallbackListForWriting()
		{
			EventCallbackList result;
			if (this.m_IsInvoking > 0)
			{
				if (this.m_TemporaryCallbacks == null)
				{
					if (this.m_Callbacks != null)
					{
						this.m_TemporaryCallbacks = EventCallbackRegistry.GetCallbackList(this.m_Callbacks);
					}
					else
					{
						this.m_TemporaryCallbacks = EventCallbackRegistry.GetCallbackList(null);
					}
				}
				result = this.m_TemporaryCallbacks;
			}
			else
			{
				if (this.m_Callbacks == null)
				{
					this.m_Callbacks = EventCallbackRegistry.GetCallbackList(null);
				}
				result = this.m_Callbacks;
			}
			return result;
		}

		// Token: 0x060007CE RID: 1998 RVA: 0x0001AB3C File Offset: 0x00018D3C
		private EventCallbackList GetCallbackListForReading()
		{
			EventCallbackList result;
			if (this.m_TemporaryCallbacks != null)
			{
				result = this.m_TemporaryCallbacks;
			}
			else
			{
				result = this.m_Callbacks;
			}
			return result;
		}

		// Token: 0x060007CF RID: 1999 RVA: 0x0001AB70 File Offset: 0x00018D70
		private bool ShouldRegisterCallback(long eventTypeId, Delegate callback, CallbackPhase phase)
		{
			bool result;
			if (callback == null)
			{
				result = false;
			}
			else
			{
				EventCallbackList callbackListForReading = this.GetCallbackListForReading();
				result = (callbackListForReading == null || !callbackListForReading.Contains(eventTypeId, callback, phase));
			}
			return result;
		}

		// Token: 0x060007D0 RID: 2000 RVA: 0x0001ABB4 File Offset: 0x00018DB4
		private bool UnregisterCallback(long eventTypeId, Delegate callback, TrickleDown useTrickleDown)
		{
			bool result;
			if (callback == null)
			{
				result = false;
			}
			else
			{
				EventCallbackList callbackListForWriting = this.GetCallbackListForWriting();
				CallbackPhase phase = (useTrickleDown != TrickleDown.TrickleDown) ? CallbackPhase.TargetAndBubbleUp : CallbackPhase.TrickleDownAndTarget;
				result = callbackListForWriting.Remove(eventTypeId, callback, phase);
			}
			return result;
		}

		// Token: 0x060007D1 RID: 2001 RVA: 0x0001ABF8 File Offset: 0x00018DF8
		[Obsolete("Use TrickleDown instead of Capture.")]
		public void RegisterCallback<TEventType>(EventCallback<TEventType> callback, Capture useCapture) where TEventType : EventBase<TEventType>, new()
		{
			this.RegisterCallback<TEventType>(callback, (TrickleDown)useCapture);
		}

		// Token: 0x060007D2 RID: 2002 RVA: 0x0001AC10 File Offset: 0x00018E10
		public void RegisterCallback<TEventType>(EventCallback<TEventType> callback, TrickleDown useTrickleDown = TrickleDown.NoTrickleDown) where TEventType : EventBase<TEventType>, new()
		{
			if (callback == null)
			{
				throw new ArgumentException("callback parameter is null");
			}
			long eventTypeId = EventBase<TEventType>.TypeId();
			CallbackPhase phase = (useTrickleDown != TrickleDown.TrickleDown) ? CallbackPhase.TargetAndBubbleUp : CallbackPhase.TrickleDownAndTarget;
			EventCallbackList eventCallbackList = this.GetCallbackListForReading();
			if (eventCallbackList == null || !eventCallbackList.Contains(eventTypeId, callback, phase))
			{
				eventCallbackList = this.GetCallbackListForWriting();
				eventCallbackList.Add(new EventCallbackFunctor<TEventType>(callback, phase));
			}
		}

		// Token: 0x060007D3 RID: 2003 RVA: 0x0001AC78 File Offset: 0x00018E78
		[Obsolete("Use TrickleDown instead of Capture.")]
		public void RegisterCallback<TEventType, TCallbackArgs>(EventCallback<TEventType, TCallbackArgs> callback, TCallbackArgs userArgs, Capture useCapture) where TEventType : EventBase<TEventType>, new()
		{
			this.RegisterCallback<TEventType, TCallbackArgs>(callback, userArgs, (TrickleDown)useCapture);
		}

		// Token: 0x060007D4 RID: 2004 RVA: 0x0001AC94 File Offset: 0x00018E94
		public void RegisterCallback<TEventType, TCallbackArgs>(EventCallback<TEventType, TCallbackArgs> callback, TCallbackArgs userArgs, TrickleDown useTrickleDown = TrickleDown.NoTrickleDown) where TEventType : EventBase<TEventType>, new()
		{
			if (callback == null)
			{
				throw new ArgumentException("callback parameter is null");
			}
			long eventTypeId = EventBase<TEventType>.TypeId();
			CallbackPhase phase = (useTrickleDown != TrickleDown.TrickleDown) ? CallbackPhase.TargetAndBubbleUp : CallbackPhase.TrickleDownAndTarget;
			EventCallbackList eventCallbackList = this.GetCallbackListForReading();
			if (eventCallbackList != null)
			{
				EventCallbackFunctor<TEventType, TCallbackArgs> eventCallbackFunctor = eventCallbackList.Find(eventTypeId, callback, phase) as EventCallbackFunctor<TEventType, TCallbackArgs>;
				if (eventCallbackFunctor != null)
				{
					eventCallbackFunctor.userArgs = userArgs;
					return;
				}
			}
			eventCallbackList = this.GetCallbackListForWriting();
			eventCallbackList.Add(new EventCallbackFunctor<TEventType, TCallbackArgs>(callback, userArgs, phase));
		}

		// Token: 0x060007D5 RID: 2005 RVA: 0x0001AD10 File Offset: 0x00018F10
		[Obsolete("Use TrickleDown instead of Capture.")]
		public bool UnregisterCallback<TEventType>(EventCallback<TEventType> callback, Capture useCapture) where TEventType : EventBase<TEventType>, new()
		{
			return this.UnregisterCallback<TEventType>(callback, (TrickleDown)useCapture);
		}

		// Token: 0x060007D6 RID: 2006 RVA: 0x0001AD30 File Offset: 0x00018F30
		public bool UnregisterCallback<TEventType>(EventCallback<TEventType> callback, TrickleDown useTrickleDown = TrickleDown.NoTrickleDown) where TEventType : EventBase<TEventType>, new()
		{
			long eventTypeId = EventBase<TEventType>.TypeId();
			return this.UnregisterCallback(eventTypeId, callback, useTrickleDown);
		}

		// Token: 0x060007D7 RID: 2007 RVA: 0x0001AD54 File Offset: 0x00018F54
		[Obsolete("Use TrickleDown instead of Capture.")]
		public bool UnregisterCallback<TEventType, TCallbackArgs>(EventCallback<TEventType, TCallbackArgs> callback, Capture useCapture) where TEventType : EventBase<TEventType>, new()
		{
			return this.UnregisterCallback<TEventType, TCallbackArgs>(callback, (TrickleDown)useCapture);
		}

		// Token: 0x060007D8 RID: 2008 RVA: 0x0001AD74 File Offset: 0x00018F74
		public bool UnregisterCallback<TEventType, TCallbackArgs>(EventCallback<TEventType, TCallbackArgs> callback, TrickleDown useTrickleDown = TrickleDown.NoTrickleDown) where TEventType : EventBase<TEventType>, new()
		{
			long eventTypeId = EventBase<TEventType>.TypeId();
			return this.UnregisterCallback(eventTypeId, callback, useTrickleDown);
		}

		// Token: 0x060007D9 RID: 2009 RVA: 0x0001AD98 File Offset: 0x00018F98
		internal bool TryGetUserArgs<TEventType, TCallbackArgs>(EventCallback<TEventType, TCallbackArgs> callback, TrickleDown useTrickleDown, out TCallbackArgs userArgs) where TEventType : EventBase<TEventType>, new()
		{
			userArgs = default(TCallbackArgs);
			bool result;
			if (callback == null)
			{
				result = false;
			}
			else
			{
				EventCallbackList callbackListForReading = this.GetCallbackListForReading();
				long eventTypeId = EventBase<TEventType>.TypeId();
				CallbackPhase phase = (useTrickleDown != TrickleDown.TrickleDown) ? CallbackPhase.TargetAndBubbleUp : CallbackPhase.TrickleDownAndTarget;
				EventCallbackFunctor<TEventType, TCallbackArgs> eventCallbackFunctor = callbackListForReading.Find(eventTypeId, callback, phase) as EventCallbackFunctor<TEventType, TCallbackArgs>;
				if (eventCallbackFunctor == null)
				{
					result = false;
				}
				else
				{
					userArgs = eventCallbackFunctor.userArgs;
					result = true;
				}
			}
			return result;
		}

		// Token: 0x060007DA RID: 2010 RVA: 0x0001AE14 File Offset: 0x00019014
		public void InvokeCallbacks(EventBase evt)
		{
			if (this.m_Callbacks != null)
			{
				this.m_IsInvoking++;
				for (int i = 0; i < this.m_Callbacks.Count; i++)
				{
					if (evt.isImmediatePropagationStopped)
					{
						break;
					}
					this.m_Callbacks[i].Invoke(evt);
				}
				this.m_IsInvoking--;
				if (this.m_IsInvoking == 0)
				{
					if (this.m_TemporaryCallbacks != null)
					{
						EventCallbackRegistry.ReleaseCallbackList(this.m_Callbacks);
						this.m_Callbacks = EventCallbackRegistry.GetCallbackList(this.m_TemporaryCallbacks);
						EventCallbackRegistry.ReleaseCallbackList(this.m_TemporaryCallbacks);
						this.m_TemporaryCallbacks = null;
					}
				}
			}
		}

		// Token: 0x060007DB RID: 2011 RVA: 0x0001AED8 File Offset: 0x000190D8
		[Obsolete("Use HasTrickleDownHandlers instead of HasCaptureHandlers.")]
		public bool HasCaptureHandlers()
		{
			return this.HasTrickleDownHandlers();
		}

		// Token: 0x060007DC RID: 2012 RVA: 0x0001AEF4 File Offset: 0x000190F4
		public bool HasTrickleDownHandlers()
		{
			return this.m_Callbacks != null && this.m_Callbacks.trickleDownCallbackCount > 0;
		}

		// Token: 0x060007DD RID: 2013 RVA: 0x0001AF28 File Offset: 0x00019128
		public bool HasBubbleHandlers()
		{
			return this.m_Callbacks != null && this.m_Callbacks.bubbleUpCallbackCount > 0;
		}

		// Token: 0x060007DE RID: 2014 RVA: 0x0001AF59 File Offset: 0x00019159
		// Note: this type is marked as 'beforefieldinit'.
		static EventCallbackRegistry()
		{
		}

		// Token: 0x0400030B RID: 779
		private static readonly EventCallbackListPool s_ListPool = new EventCallbackListPool();

		// Token: 0x0400030C RID: 780
		private EventCallbackList m_Callbacks;

		// Token: 0x0400030D RID: 781
		private EventCallbackList m_TemporaryCallbacks;

		// Token: 0x0400030E RID: 782
		private int m_IsInvoking;
	}
}
