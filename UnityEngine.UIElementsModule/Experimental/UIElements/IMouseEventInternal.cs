﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000132 RID: 306
	internal interface IMouseEventInternal
	{
		// Token: 0x17000220 RID: 544
		// (get) Token: 0x0600083D RID: 2109
		// (set) Token: 0x0600083E RID: 2110
		bool hasUnderlyingPhysicalEvent { get; set; }
	}
}
