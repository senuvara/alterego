﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000046 RID: 70
	public enum SelectionType
	{
		// Token: 0x0400009C RID: 156
		None,
		// Token: 0x0400009D RID: 157
		Single,
		// Token: 0x0400009E RID: 158
		Multiple
	}
}
