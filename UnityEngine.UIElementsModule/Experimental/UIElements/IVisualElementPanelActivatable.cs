﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000D4 RID: 212
	internal interface IVisualElementPanelActivatable
	{
		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x0600062F RID: 1583
		VisualElement element { get; }

		// Token: 0x06000630 RID: 1584
		bool CanBeActivated();

		// Token: 0x06000631 RID: 1585
		void OnPanelActivate();

		// Token: 0x06000632 RID: 1586
		void OnPanelDeactivate();
	}
}
