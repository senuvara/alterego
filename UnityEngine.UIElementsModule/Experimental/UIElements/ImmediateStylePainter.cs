﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000045 RID: 69
	[NativeHeader("Modules/UIElements/ImmediateStylePainter.h")]
	[StructLayout(LayoutKind.Sequential)]
	internal class ImmediateStylePainter : IStylePainterInternal, IStylePainter
	{
		// Token: 0x060001D2 RID: 466 RVA: 0x00006CF5 File Offset: 0x00004EF5
		public ImmediateStylePainter()
		{
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x00006D08 File Offset: 0x00004F08
		internal static void DrawRect(Rect screenRect, Color color, Vector4 borderWidths, Vector4 borderRadiuses)
		{
			ImmediateStylePainter.DrawRect_Injected(ref screenRect, ref color, ref borderWidths, ref borderRadiuses);
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x00006D18 File Offset: 0x00004F18
		internal static void DrawTexture(Rect screenRect, Texture texture, Rect sourceRect, Color color, Vector4 borderWidths, Vector4 borderRadiuses, int leftBorder, int topBorder, int rightBorder, int bottomBorder, bool usePremultiplyAlpha)
		{
			ImmediateStylePainter.DrawTexture_Injected(ref screenRect, texture, ref sourceRect, ref color, ref borderWidths, ref borderRadiuses, leftBorder, topBorder, rightBorder, bottomBorder, usePremultiplyAlpha);
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x00006D40 File Offset: 0x00004F40
		internal static void DrawText(Rect screenRect, string text, Font font, int fontSize, FontStyle fontStyle, Color fontColor, TextAnchor anchor, bool wordWrap, float wordWrapWidth, bool richText, TextClipping textClipping)
		{
			ImmediateStylePainter.DrawText_Injected(ref screenRect, text, font, fontSize, fontStyle, ref fontColor, anchor, wordWrap, wordWrapWidth, richText, textClipping);
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x060001D6 RID: 470 RVA: 0x00006D68 File Offset: 0x00004F68
		// (set) Token: 0x060001D7 RID: 471 RVA: 0x00006D82 File Offset: 0x00004F82
		public VisualElement currentElement
		{
			[CompilerGenerated]
			get
			{
				return this.<currentElement>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<currentElement>k__BackingField = value;
			}
		}

		// Token: 0x060001D8 RID: 472 RVA: 0x00006D8C File Offset: 0x00004F8C
		public void DrawRect(RectStylePainterParameters painterParams)
		{
			Rect rect = painterParams.rect;
			Color a = painterParams.color * UIElementsUtility.editorPlayModeTintColor;
			Vector4 widths = painterParams.border.GetWidths();
			Vector4 radiuses = painterParams.border.GetRadiuses();
			ImmediateStylePainter.DrawRect(rect, a * this.m_OpacityColor, widths, radiuses);
		}

		// Token: 0x060001D9 RID: 473 RVA: 0x00006DE4 File Offset: 0x00004FE4
		public void DrawTexture(TextureStylePainterParameters painterParams)
		{
			Rect rect = painterParams.rect;
			Rect sourceRect = (!(painterParams.uv != Rect.zero)) ? new Rect(0f, 0f, 1f, 1f) : painterParams.uv;
			Texture texture = painterParams.texture;
			Color a = painterParams.color * UIElementsUtility.editorPlayModeTintColor;
			ScaleMode scaleMode = painterParams.scaleMode;
			int sliceLeft = painterParams.sliceLeft;
			int sliceTop = painterParams.sliceTop;
			int sliceRight = painterParams.sliceRight;
			int sliceBottom = painterParams.sliceBottom;
			bool usePremultiplyAlpha = painterParams.usePremultiplyAlpha;
			Rect screenRect = rect;
			float num = (float)texture.width * sourceRect.width / ((float)texture.height * sourceRect.height);
			float num2 = rect.width / rect.height;
			if (scaleMode != ScaleMode.StretchToFill)
			{
				if (scaleMode != ScaleMode.ScaleAndCrop)
				{
					if (scaleMode == ScaleMode.ScaleToFit)
					{
						if (num2 > num)
						{
							float num3 = num / num2;
							screenRect = new Rect(rect.xMin + rect.width * (1f - num3) * 0.5f, rect.yMin, num3 * rect.width, rect.height);
						}
						else
						{
							float num4 = num2 / num;
							screenRect = new Rect(rect.xMin, rect.yMin + rect.height * (1f - num4) * 0.5f, rect.width, num4 * rect.height);
						}
					}
				}
				else if (num2 > num)
				{
					float num5 = sourceRect.height * (num / num2);
					float num6 = (sourceRect.height - num5) * 0.5f;
					sourceRect = new Rect(sourceRect.x, sourceRect.y + num6, sourceRect.width, num5);
				}
				else
				{
					float num7 = sourceRect.width * (num2 / num);
					float num8 = (sourceRect.width - num7) * 0.5f;
					sourceRect = new Rect(sourceRect.x + num8, sourceRect.y, num7, sourceRect.height);
				}
			}
			Vector4 widths = painterParams.border.GetWidths();
			Vector4 radiuses = painterParams.border.GetRadiuses();
			ImmediateStylePainter.DrawTexture(screenRect, texture, sourceRect, a * this.m_OpacityColor, widths, radiuses, sliceLeft, sliceTop, sliceRight, sliceBottom, usePremultiplyAlpha);
		}

		// Token: 0x060001DA RID: 474 RVA: 0x00007058 File Offset: 0x00005258
		public void DrawText(TextStylePainterParameters painterParams)
		{
			Rect rect = painterParams.rect;
			string text = painterParams.text;
			Font font = painterParams.font;
			int fontSize = painterParams.fontSize;
			FontStyle fontStyle = painterParams.fontStyle;
			Color a = painterParams.fontColor * UIElementsUtility.editorPlayModeTintColor;
			TextAnchor anchor = painterParams.anchor;
			bool wordWrap = painterParams.wordWrap;
			float wordWrapWidth = painterParams.wordWrapWidth;
			bool richText = painterParams.richText;
			TextClipping clipping = painterParams.clipping;
			ImmediateStylePainter.DrawText(rect, text, font, fontSize, fontStyle, a * this.m_OpacityColor, anchor, wordWrap, wordWrapWidth, richText, clipping);
		}

		// Token: 0x060001DB RID: 475 RVA: 0x000070F4 File Offset: 0x000052F4
		public void DrawMesh(MeshStylePainterParameters painterParams)
		{
			Mesh mesh = painterParams.mesh;
			Material material = painterParams.material;
			int pass = painterParams.pass;
			material.SetPass(pass);
			Graphics.DrawMeshNow(mesh, Matrix4x4.identity);
		}

		// Token: 0x060001DC RID: 476 RVA: 0x0000712D File Offset: 0x0000532D
		public void DrawImmediate(Action callback)
		{
			callback();
		}

		// Token: 0x060001DD RID: 477 RVA: 0x00007138 File Offset: 0x00005338
		public void DrawBackground()
		{
			IStyle style = this.currentElement.style;
			if (style.backgroundColor != Color.clear)
			{
				RectStylePainterParameters @default = RectStylePainterParameters.GetDefault(this.currentElement);
				@default.border.SetWidth(0f);
				this.DrawRect(@default);
			}
			if (style.backgroundImage.value != null)
			{
				TextureStylePainterParameters default2 = TextureStylePainterParameters.GetDefault(this.currentElement);
				default2.border.SetWidth(0f);
				this.DrawTexture(default2);
			}
		}

		// Token: 0x060001DE RID: 478 RVA: 0x000071D4 File Offset: 0x000053D4
		public void DrawBorder()
		{
			IStyle style = this.currentElement.style;
			if (style.borderColor != Color.clear && (style.borderLeftWidth > 0f || style.borderTopWidth > 0f || style.borderRightWidth > 0f || style.borderBottomWidth > 0f))
			{
				RectStylePainterParameters @default = RectStylePainterParameters.GetDefault(this.currentElement);
				@default.color = style.borderColor;
				this.DrawRect(@default);
			}
		}

		// Token: 0x060001DF RID: 479 RVA: 0x00007284 File Offset: 0x00005484
		public void DrawText(string text)
		{
			if (!string.IsNullOrEmpty(text) && this.currentElement.contentRect.width > 0f && this.currentElement.contentRect.height > 0f)
			{
				this.DrawText(TextStylePainterParameters.GetDefault(this.currentElement, text));
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x060001E0 RID: 480 RVA: 0x000072EC File Offset: 0x000054EC
		// (set) Token: 0x060001E1 RID: 481 RVA: 0x0000730C File Offset: 0x0000550C
		public float opacity
		{
			get
			{
				return this.m_OpacityColor.a;
			}
			set
			{
				this.m_OpacityColor.a = value;
			}
		}

		// Token: 0x060001E2 RID: 482
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DrawRect_Injected(ref Rect screenRect, ref Color color, ref Vector4 borderWidths, ref Vector4 borderRadiuses);

		// Token: 0x060001E3 RID: 483
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DrawTexture_Injected(ref Rect screenRect, Texture texture, ref Rect sourceRect, ref Color color, ref Vector4 borderWidths, ref Vector4 borderRadiuses, int leftBorder, int topBorder, int rightBorder, int bottomBorder, bool usePremultiplyAlpha);

		// Token: 0x060001E4 RID: 484
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DrawText_Injected(ref Rect screenRect, string text, Font font, int fontSize, FontStyle fontStyle, ref Color fontColor, TextAnchor anchor, bool wordWrap, float wordWrapWidth, bool richText, TextClipping textClipping);

		// Token: 0x04000099 RID: 153
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private VisualElement <currentElement>k__BackingField;

		// Token: 0x0400009A RID: 154
		private Color m_OpacityColor = Color.white;
	}
}
