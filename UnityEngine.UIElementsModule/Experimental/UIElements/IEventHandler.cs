﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000122 RID: 290
	public interface IEventHandler
	{
		// Token: 0x060007DF RID: 2015
		void SendEvent(EventBase e);

		// Token: 0x060007E0 RID: 2016
		void HandleEvent(EventBase evt);

		// Token: 0x060007E1 RID: 2017
		bool HasTrickleDownHandlers();

		// Token: 0x060007E2 RID: 2018
		bool HasBubbleUpHandlers();

		// Token: 0x060007E3 RID: 2019
		[Obsolete("Use HasTrickleDownHandlers instead of HasCaptureHandlers.")]
		bool HasCaptureHandlers();

		// Token: 0x060007E4 RID: 2020
		[Obsolete("Use HasBubbleUpHandlers instead of HasBubbleHandlers.")]
		bool HasBubbleHandlers();
	}
}
