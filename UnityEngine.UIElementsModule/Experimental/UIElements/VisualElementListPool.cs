﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000BE RID: 190
	internal class VisualElementListPool
	{
		// Token: 0x0600049D RID: 1181 RVA: 0x00002223 File Offset: 0x00000423
		public VisualElementListPool()
		{
		}

		// Token: 0x0600049E RID: 1182 RVA: 0x0000F59C File Offset: 0x0000D79C
		public static List<VisualElement> Copy(List<VisualElement> elements)
		{
			List<VisualElement> list = VisualElementListPool.pool.Get();
			list.AddRange(elements);
			return list;
		}

		// Token: 0x0600049F RID: 1183 RVA: 0x0000F5C4 File Offset: 0x0000D7C4
		public static List<VisualElement> Get(int initialCapacity = 0)
		{
			List<VisualElement> list = VisualElementListPool.pool.Get();
			if (initialCapacity > 0 && list.Capacity < initialCapacity)
			{
				list.Capacity = initialCapacity;
			}
			return list;
		}

		// Token: 0x060004A0 RID: 1184 RVA: 0x0000F601 File Offset: 0x0000D801
		public static void Release(List<VisualElement> elements)
		{
			elements.Clear();
			VisualElementListPool.pool.Release(elements);
		}

		// Token: 0x060004A1 RID: 1185 RVA: 0x0000F615 File Offset: 0x0000D815
		// Note: this type is marked as 'beforefieldinit'.
		static VisualElementListPool()
		{
		}

		// Token: 0x04000218 RID: 536
		private static ObjectPool<List<VisualElement>> pool = new ObjectPool<List<VisualElement>>(20);
	}
}
