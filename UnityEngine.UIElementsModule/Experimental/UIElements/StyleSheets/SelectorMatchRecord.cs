﻿using System;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000149 RID: 329
	internal struct SelectorMatchRecord
	{
		// Token: 0x0600088A RID: 2186 RVA: 0x0001C161 File Offset: 0x0001A361
		public SelectorMatchRecord(StyleSheet sheet, int styleSheetIndexInStack)
		{
			this = default(SelectorMatchRecord);
			this.sheet = sheet;
			this.styleSheetIndexInStack = styleSheetIndexInStack;
		}

		// Token: 0x0600088B RID: 2187 RVA: 0x0001C17C File Offset: 0x0001A37C
		public static int Compare(SelectorMatchRecord a, SelectorMatchRecord b)
		{
			int num = a.styleSheetIndexInStack.CompareTo(b.styleSheetIndexInStack);
			if (num == 0)
			{
				num = a.complexSelector.orderInStyleSheet.CompareTo(b.complexSelector.orderInStyleSheet);
			}
			return num;
		}

		// Token: 0x04000330 RID: 816
		public StyleSheet sheet;

		// Token: 0x04000331 RID: 817
		public int styleSheetIndexInStack;

		// Token: 0x04000332 RID: 818
		public StyleComplexSelector complexSelector;
	}
}
