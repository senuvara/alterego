﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000156 RID: 342
	internal enum StylePropertyApplyMode
	{
		// Token: 0x0400037E RID: 894
		Copy,
		// Token: 0x0400037F RID: 895
		CopyIfEqualOrGreaterSpecificity,
		// Token: 0x04000380 RID: 896
		CopyIfNotInline
	}
}
