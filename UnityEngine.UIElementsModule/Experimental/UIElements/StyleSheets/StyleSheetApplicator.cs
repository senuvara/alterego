﻿using System;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x0200014C RID: 332
	internal static class StyleSheetApplicator
	{
		// Token: 0x06000898 RID: 2200 RVA: 0x0001C697 File Offset: 0x0001A897
		private static void Apply<T>(T val, int specificity, ref StyleValue<T> property)
		{
			property.Apply(new StyleValue<T>(val, specificity), StylePropertyApplyMode.CopyIfEqualOrGreaterSpecificity);
		}

		// Token: 0x06000899 RID: 2201 RVA: 0x0001C6A9 File Offset: 0x0001A8A9
		public static void ApplyValue<T>(int specificity, ref StyleValue<T> property, T value = default(T))
		{
			StyleSheetApplicator.Apply<T>(value, specificity, ref property);
		}

		// Token: 0x0600089A RID: 2202 RVA: 0x0001C6B4 File Offset: 0x0001A8B4
		public static void ApplyBool(StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<bool> property)
		{
			bool val = sheet.ReadKeyword(handles[0]) == StyleValueKeyword.True;
			StyleSheetApplicator.Apply<bool>(val, specificity, ref property);
		}

		// Token: 0x0600089B RID: 2203 RVA: 0x0001C6E0 File Offset: 0x0001A8E0
		public static void ApplyFloat(StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<float> property)
		{
			float val = sheet.ReadFloat(handles[0]);
			StyleSheetApplicator.Apply<float>(val, specificity, ref property);
		}

		// Token: 0x0600089C RID: 2204 RVA: 0x0001C70C File Offset: 0x0001A90C
		public static void ApplyFloatOrKeyword(StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<FloatOrKeyword> property)
		{
			StyleValueHandle handle = handles[0];
			FloatOrKeyword val;
			if (handle.valueType == StyleValueType.Keyword)
			{
				val = new FloatOrKeyword((StyleValueKeyword)handle.valueIndex);
			}
			else
			{
				val = new FloatOrKeyword(sheet.ReadFloat(handle));
			}
			StyleSheetApplicator.Apply<FloatOrKeyword>(val, specificity, ref property);
		}

		// Token: 0x0600089D RID: 2205 RVA: 0x0001C760 File Offset: 0x0001A960
		public static void ApplyInt(StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<int> property)
		{
			int val = (int)sheet.ReadFloat(handles[0]);
			StyleSheetApplicator.Apply<int>(val, specificity, ref property);
		}

		// Token: 0x0600089E RID: 2206 RVA: 0x0001C78C File Offset: 0x0001A98C
		public static void ApplyEnum<T>(StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<int> property)
		{
			int enumValue = StyleSheetCache.GetEnumValue<T>(sheet, handles[0]);
			StyleSheetApplicator.Apply<int>(enumValue, specificity, ref property);
		}

		// Token: 0x0600089F RID: 2207 RVA: 0x0001C7B8 File Offset: 0x0001A9B8
		public static void ApplyColor(StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<Color> property)
		{
			Color val = sheet.ReadColor(handles[0]);
			StyleSheetApplicator.Apply<Color>(val, specificity, ref property);
		}

		// Token: 0x060008A0 RID: 2208 RVA: 0x0001C7E4 File Offset: 0x0001A9E4
		public static void CompileCursor(StyleSheet sheet, StyleValueHandle[] handles, out float hotspotX, out float hotspotY, out int cursorId, out Texture2D texture)
		{
			StyleValueHandle handle = handles[0];
			int num = 0;
			bool flag = handle.valueType == StyleValueType.ResourcePath || handle.valueType == StyleValueType.AssetReference;
			cursorId = 0;
			texture = null;
			hotspotX = 0f;
			hotspotY = 0f;
			if (flag)
			{
				if (StyleSheetApplicator.TryGetSourceFromHandle(sheet, handles[num++], out texture))
				{
					if (num < handles.Length && handles[num].valueType == StyleValueType.Float && sheet.TryReadFloat(handles, num++, out hotspotX))
					{
						if (!sheet.TryReadFloat(handles, num++, out hotspotY))
						{
						}
					}
				}
				if (num < handles.Length)
				{
					if (StyleSheetApplicator.getCursorIdFunc != null)
					{
						cursorId = StyleSheetApplicator.getCursorIdFunc(sheet, handles[num]);
					}
				}
			}
			else if (StyleSheetApplicator.getCursorIdFunc != null)
			{
				cursorId = StyleSheetApplicator.getCursorIdFunc(sheet, handle);
			}
		}

		// Token: 0x060008A1 RID: 2209 RVA: 0x0001C8F8 File Offset: 0x0001AAF8
		public static void ApplyCursor(StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<CursorStyle> property)
		{
			float x;
			float y;
			int defaultCursorId;
			Texture2D texture;
			StyleSheetApplicator.CompileCursor(sheet, handles, out x, out y, out defaultCursorId, out texture);
			CursorStyle val = new CursorStyle
			{
				texture = texture,
				hotspot = new Vector2(x, y),
				defaultCursorId = defaultCursorId
			};
			StyleSheetApplicator.Apply<CursorStyle>(val, specificity, ref property);
		}

		// Token: 0x060008A2 RID: 2210 RVA: 0x0001C948 File Offset: 0x0001AB48
		public static void ApplyFont(StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<Font> property)
		{
			StyleValueHandle handle = handles[0];
			Font font = null;
			StyleValueType valueType = handle.valueType;
			if (valueType != StyleValueType.ResourcePath)
			{
				if (valueType != StyleValueType.AssetReference)
				{
					Debug.LogWarning("Invalid value for font " + handle.valueType);
				}
				else
				{
					font = (sheet.ReadAssetReference(handle) as Font);
					if (font == null)
					{
						Debug.LogWarning("Invalid font reference");
					}
				}
			}
			else
			{
				string text = sheet.ReadResourcePath(handle);
				if (!string.IsNullOrEmpty(text))
				{
					font = (Panel.loadResourceFunc(text, typeof(Font)) as Font);
				}
				if (font == null)
				{
					Debug.LogWarning(string.Format("Font not found for path: {0}", text));
				}
			}
			if (font != null)
			{
				StyleSheetApplicator.Apply<Font>(font, specificity, ref property);
			}
		}

		// Token: 0x060008A3 RID: 2211 RVA: 0x0001CA38 File Offset: 0x0001AC38
		public static void ApplyImage(StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<Texture2D> property)
		{
			Texture2D val = null;
			StyleValueHandle handle = handles[0];
			if (handle.valueType == StyleValueType.Keyword)
			{
				if (handle.valueIndex != 5)
				{
					Debug.LogWarning("Invalid keyword for image source " + (StyleValueKeyword)handle.valueIndex);
				}
			}
			else if (!StyleSheetApplicator.TryGetSourceFromHandle(sheet, handle, out val))
			{
				return;
			}
			StyleSheetApplicator.Apply<Texture2D>(val, specificity, ref property);
		}

		// Token: 0x060008A4 RID: 2212 RVA: 0x0001CAB8 File Offset: 0x0001ACB8
		private static bool TryGetSourceFromHandle(StyleSheet sheet, StyleValueHandle handle, out Texture2D source)
		{
			source = null;
			StyleValueType valueType = handle.valueType;
			if (valueType != StyleValueType.ResourcePath)
			{
				if (valueType != StyleValueType.AssetReference)
				{
					Debug.LogWarning("Invalid value for image source " + handle.valueType);
					return false;
				}
				source = (sheet.ReadAssetReference(handle) as Texture2D);
				if (source == null)
				{
					Debug.LogWarning("Invalid texture specified");
					return false;
				}
			}
			else
			{
				string text = sheet.ReadResourcePath(handle);
				if (!string.IsNullOrEmpty(text))
				{
					source = (Panel.loadResourceFunc(text, typeof(Texture2D)) as Texture2D);
				}
				if (source == null)
				{
					Debug.LogWarning(string.Format("Texture not found for path: {0}", text));
					return false;
				}
			}
			return true;
		}

		// Token: 0x060008A5 RID: 2213 RVA: 0x0001CBA0 File Offset: 0x0001ADA0
		public static bool CompileFlexShorthand(StyleSheet sheet, StyleValueHandle[] handles, out float grow, out float shrink, out FloatOrKeyword basis)
		{
			grow = 0f;
			shrink = 0f;
			basis = new FloatOrKeyword(StyleValueKeyword.Auto);
			bool result = false;
			if (handles.Length == 1 && handles[0].valueType == StyleValueType.Keyword && handles[0].valueIndex == 2)
			{
				result = true;
				grow = 0f;
				shrink = 1f;
				basis = new FloatOrKeyword(StyleValueKeyword.Auto);
			}
			else if (handles.Length == 1 && handles[0].valueType == StyleValueType.Keyword && handles[0].valueIndex == 5)
			{
				result = true;
				grow = 0f;
				shrink = 0f;
				basis = new FloatOrKeyword(StyleValueKeyword.Auto);
			}
			else if (handles.Length <= 3 && handles[0].valueType == StyleValueType.Keyword && handles[0].valueIndex == 1)
			{
				result = true;
				basis = new FloatOrKeyword(StyleValueKeyword.Auto);
				grow = 1f;
				shrink = 1f;
				if (handles.Length > 1)
				{
					grow = sheet.ReadFloat(handles[1]);
					if (handles.Length > 2)
					{
						shrink = sheet.ReadFloat(handles[2]);
					}
				}
			}
			else if (handles.Length <= 3 && handles[0].valueType == StyleValueType.Float)
			{
				result = true;
				grow = sheet.ReadFloat(handles[0]);
				shrink = 1f;
				basis = new FloatOrKeyword(0f);
				if (handles.Length > 1)
				{
					if (handles[1].valueType == StyleValueType.Float)
					{
						shrink = sheet.ReadFloat(handles[1]);
						if (handles.Length > 2)
						{
							if (handles[2].valueType == StyleValueType.Keyword && handles[2].valueIndex == 1)
							{
								basis = new FloatOrKeyword(StyleValueKeyword.Auto);
							}
							else if (handles[2].valueType == StyleValueType.Float)
							{
								basis = new FloatOrKeyword(sheet.ReadFloat(handles[2]));
							}
						}
					}
					else if (handles[1].valueType == StyleValueType.Keyword && handles[1].valueIndex == 1)
					{
						basis = new FloatOrKeyword(StyleValueKeyword.Auto);
					}
				}
			}
			return result;
		}

		// Token: 0x060008A6 RID: 2214 RVA: 0x0001CE04 File Offset: 0x0001B004
		public static void ApplyFlexShorthand(StyleSheet sheet, StyleValueHandle[] handles, int specificity, VisualElementStylesData styleData)
		{
			float value;
			float value2;
			FloatOrKeyword value3;
			bool flag = StyleSheetApplicator.CompileFlexShorthand(sheet, handles, out value, out value2, out value3);
			if (flag)
			{
				StyleSheetApplicator.ApplyValue<float>(specificity, ref styleData.flexGrow, value);
				StyleSheetApplicator.ApplyValue<float>(specificity, ref styleData.flexShrink, value2);
				StyleSheetApplicator.ApplyValue<FloatOrKeyword>(specificity, ref styleData.flexBasis, value3);
			}
		}

		// Token: 0x060008A7 RID: 2215 RVA: 0x0001CE4F File Offset: 0x0001B04F
		// Note: this type is marked as 'beforefieldinit'.
		static StyleSheetApplicator()
		{
		}

		// Token: 0x04000336 RID: 822
		internal static StyleSheetApplicator.GetCursorIdFunction getCursorIdFunc = null;

		// Token: 0x0200014D RID: 333
		// (Invoke) Token: 0x060008A9 RID: 2217
		internal delegate int GetCursorIdFunction(StyleSheet sheet, StyleValueHandle handle);

		// Token: 0x0200014E RID: 334
		public static class Shorthand
		{
			// Token: 0x060008AC RID: 2220 RVA: 0x0001CE58 File Offset: 0x0001B058
			private static void ReadFourSidesArea(StyleSheet sheet, StyleValueHandle[] handles, out float top, out float right, out float bottom, out float left)
			{
				top = 0f;
				right = 0f;
				bottom = 0f;
				left = 0f;
				switch (handles.Length)
				{
				case 0:
					break;
				case 1:
					top = (right = (bottom = (left = sheet.ReadFloat(handles[0]))));
					break;
				case 2:
					top = (bottom = sheet.ReadFloat(handles[0]));
					left = (right = sheet.ReadFloat(handles[1]));
					break;
				case 3:
					top = sheet.ReadFloat(handles[0]);
					left = (right = sheet.ReadFloat(handles[1]));
					bottom = sheet.ReadFloat(handles[2]);
					break;
				default:
					top = sheet.ReadFloat(handles[0]);
					right = sheet.ReadFloat(handles[1]);
					bottom = sheet.ReadFloat(handles[2]);
					left = sheet.ReadFloat(handles[3]);
					break;
				}
			}

			// Token: 0x060008AD RID: 2221 RVA: 0x0001CFB0 File Offset: 0x0001B1B0
			public static void ApplyBorderRadius(StyleSheet sheet, StyleValueHandle[] handles, int specificity, VisualElementStylesData styleData)
			{
				float val;
				float val2;
				float val3;
				float val4;
				StyleSheetApplicator.Shorthand.ReadFourSidesArea(sheet, handles, out val, out val2, out val3, out val4);
				StyleSheetApplicator.Apply<float>(val, specificity, ref styleData.borderTopLeftRadius);
				StyleSheetApplicator.Apply<float>(val2, specificity, ref styleData.borderTopRightRadius);
				StyleSheetApplicator.Apply<float>(val4, specificity, ref styleData.borderBottomLeftRadius);
				StyleSheetApplicator.Apply<float>(val3, specificity, ref styleData.borderBottomRightRadius);
			}

			// Token: 0x060008AE RID: 2222 RVA: 0x0001D004 File Offset: 0x0001B204
			public static void ApplyMargin(StyleSheet sheet, StyleValueHandle[] handles, int specificity, VisualElementStylesData styleData)
			{
				float val;
				float val2;
				float val3;
				float val4;
				StyleSheetApplicator.Shorthand.ReadFourSidesArea(sheet, handles, out val, out val2, out val3, out val4);
				StyleSheetApplicator.Apply<float>(val, specificity, ref styleData.marginTop);
				StyleSheetApplicator.Apply<float>(val2, specificity, ref styleData.marginRight);
				StyleSheetApplicator.Apply<float>(val3, specificity, ref styleData.marginBottom);
				StyleSheetApplicator.Apply<float>(val4, specificity, ref styleData.marginLeft);
			}

			// Token: 0x060008AF RID: 2223 RVA: 0x0001D058 File Offset: 0x0001B258
			public static void ApplyPadding(StyleSheet sheet, StyleValueHandle[] handles, int specificity, VisualElementStylesData styleData)
			{
				float val;
				float val2;
				float val3;
				float val4;
				StyleSheetApplicator.Shorthand.ReadFourSidesArea(sheet, handles, out val, out val2, out val3, out val4);
				StyleSheetApplicator.Apply<float>(val, specificity, ref styleData.paddingTop);
				StyleSheetApplicator.Apply<float>(val2, specificity, ref styleData.paddingRight);
				StyleSheetApplicator.Apply<float>(val3, specificity, ref styleData.paddingBottom);
				StyleSheetApplicator.Apply<float>(val4, specificity, ref styleData.paddingLeft);
			}
		}
	}
}
