﻿using System;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000154 RID: 340
	internal static class StyleSheetExtensions
	{
		// Token: 0x060008C2 RID: 2242 RVA: 0x0001D76C File Offset: 0x0001B96C
		public static void Apply<T>(this StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<T> property, HandlesApplicatorFunction<T> applicatorFunc)
		{
			if (handles[0].valueType == StyleValueType.Keyword && handles[0].valueIndex == 2)
			{
				StyleSheetApplicator.ApplyValue<T>(specificity, ref property, default(T));
			}
			else
			{
				applicatorFunc(sheet, handles, specificity, ref property);
			}
		}

		// Token: 0x060008C3 RID: 2243 RVA: 0x0001D7C1 File Offset: 0x0001B9C1
		public static void ApplyShorthand(this StyleSheet sheet, StyleValueHandle[] handles, int specificity, VisualElementStylesData styleData, ShorthandApplicatorFunction applicatorFunc)
		{
			applicatorFunc(sheet, handles, specificity, styleData);
		}

		// Token: 0x060008C4 RID: 2244 RVA: 0x0001D7D0 File Offset: 0x0001B9D0
		public static string ReadAsString(this StyleSheet sheet, StyleValueHandle handle)
		{
			string empty = string.Empty;
			switch (handle.valueType)
			{
			case StyleValueType.Keyword:
				return sheet.ReadKeyword(handle).ToString();
			case StyleValueType.Float:
				return sheet.ReadFloat(handle).ToString();
			case StyleValueType.Color:
				return sheet.ReadColor(handle).ToString();
			case StyleValueType.ResourcePath:
				return sheet.ReadResourcePath(handle);
			case StyleValueType.Enum:
				return sheet.ReadEnum(handle);
			case StyleValueType.String:
				return sheet.ReadString(handle);
			}
			throw new ArgumentException("Unhandled type " + handle.valueType);
		}
	}
}
