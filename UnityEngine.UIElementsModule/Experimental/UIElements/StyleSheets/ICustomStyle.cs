﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x0200015A RID: 346
	public interface ICustomStyle
	{
		// Token: 0x060008D6 RID: 2262
		void ApplyCustomProperty(string propertyName, ref StyleValue<float> target);

		// Token: 0x060008D7 RID: 2263
		void ApplyCustomProperty(string propertyName, ref StyleValue<int> target);

		// Token: 0x060008D8 RID: 2264
		void ApplyCustomProperty(string propertyName, ref StyleValue<bool> target);

		// Token: 0x060008D9 RID: 2265
		void ApplyCustomProperty(string propertyName, ref StyleValue<Color> target);

		// Token: 0x060008DA RID: 2266
		void ApplyCustomProperty(string propertyName, ref StyleValue<Texture2D> target);

		// Token: 0x060008DB RID: 2267
		void ApplyCustomProperty(string propertyName, ref StyleValue<string> target);
	}
}
