﻿using System;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000159 RID: 345
	internal struct CustomProperty
	{
		// Token: 0x04000384 RID: 900
		public int specificity;

		// Token: 0x04000385 RID: 901
		public StyleValueHandle[] handles;

		// Token: 0x04000386 RID: 902
		public StyleSheet data;
	}
}
