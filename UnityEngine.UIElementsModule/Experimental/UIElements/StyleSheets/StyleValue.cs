﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000157 RID: 343
	public struct StyleValue<T>
	{
		// Token: 0x060008C5 RID: 2245 RVA: 0x0001D8B2 File Offset: 0x0001BAB2
		public StyleValue(T value)
		{
			this.value = value;
			this.specificity = 0;
		}

		// Token: 0x060008C6 RID: 2246 RVA: 0x0001D8C3 File Offset: 0x0001BAC3
		internal StyleValue(T value, int specifity)
		{
			this.value = value;
			this.specificity = specifity;
		}

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x060008C7 RID: 2247 RVA: 0x0001D8D4 File Offset: 0x0001BAD4
		public static StyleValue<T> nil
		{
			get
			{
				return StyleValue<T>.defaultStyle;
			}
		}

		// Token: 0x060008C8 RID: 2248 RVA: 0x0001D8F0 File Offset: 0x0001BAF0
		public T GetSpecifiedValueOrDefault(T defaultValue)
		{
			if (this.specificity > 0)
			{
				defaultValue = this.value;
			}
			return defaultValue;
		}

		// Token: 0x060008C9 RID: 2249 RVA: 0x0001D91C File Offset: 0x0001BB1C
		public static implicit operator T(StyleValue<T> sp)
		{
			return sp.value;
		}

		// Token: 0x060008CA RID: 2250 RVA: 0x0001D938 File Offset: 0x0001BB38
		internal bool Apply(StyleValue<T> other, StylePropertyApplyMode mode)
		{
			return this.Apply(other.value, other.specificity, mode);
		}

		// Token: 0x060008CB RID: 2251 RVA: 0x0001D964 File Offset: 0x0001BB64
		internal bool Apply(T otherValue, int otherSpecificity, StylePropertyApplyMode mode)
		{
			bool result;
			switch (mode)
			{
			case StylePropertyApplyMode.Copy:
				this.value = otherValue;
				this.specificity = otherSpecificity;
				result = true;
				break;
			case StylePropertyApplyMode.CopyIfEqualOrGreaterSpecificity:
				if (otherSpecificity >= this.specificity)
				{
					this.value = otherValue;
					this.specificity = otherSpecificity;
					result = true;
				}
				else
				{
					result = false;
				}
				break;
			case StylePropertyApplyMode.CopyIfNotInline:
				if (this.specificity < 2147483647)
				{
					this.value = otherValue;
					this.specificity = otherSpecificity;
					result = true;
				}
				else
				{
					result = false;
				}
				break;
			default:
				Debug.Assert(false, "Invalid mode " + mode);
				result = false;
				break;
			}
			return result;
		}

		// Token: 0x060008CC RID: 2252 RVA: 0x0001DA14 File Offset: 0x0001BC14
		public static implicit operator StyleValue<T>(T value)
		{
			return StyleValue<T>.Create(value);
		}

		// Token: 0x060008CD RID: 2253 RVA: 0x0001DA30 File Offset: 0x0001BC30
		public static StyleValue<T> Create(T value)
		{
			return new StyleValue<T>(value, int.MaxValue);
		}

		// Token: 0x060008CE RID: 2254 RVA: 0x0001DA50 File Offset: 0x0001BC50
		public override string ToString()
		{
			return string.Format("[StyleProperty<{2}>: specifity={0}, value={1}]", this.specificity, this.value, typeof(T).Name);
		}

		// Token: 0x060008CF RID: 2255 RVA: 0x0001DA94 File Offset: 0x0001BC94
		// Note: this type is marked as 'beforefieldinit'.
		static StyleValue()
		{
		}

		// Token: 0x04000381 RID: 897
		internal int specificity;

		// Token: 0x04000382 RID: 898
		public T value;

		// Token: 0x04000383 RID: 899
		private static readonly StyleValue<T> defaultStyle = default(StyleValue<T>);
	}
}
