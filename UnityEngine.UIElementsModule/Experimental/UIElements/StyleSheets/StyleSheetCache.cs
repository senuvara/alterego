﻿using System;
using System.Collections.Generic;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x0200014F RID: 335
	internal static class StyleSheetCache
	{
		// Token: 0x060008B0 RID: 2224 RVA: 0x0001D0A9 File Offset: 0x0001B2A9
		internal static void ClearCaches()
		{
			StyleSheetCache.s_EnumToIntCache.Clear();
			StyleSheetCache.s_RulePropertyIDsCache.Clear();
		}

		// Token: 0x060008B1 RID: 2225 RVA: 0x0001D0C0 File Offset: 0x0001B2C0
		internal static int GetEnumValue<T>(StyleSheet sheet, StyleValueHandle handle)
		{
			Debug.Assert(handle.valueType == StyleValueType.Enum);
			StyleSheetCache.SheetHandleKey key = new StyleSheetCache.SheetHandleKey(sheet, handle.valueIndex);
			int num;
			if (!StyleSheetCache.s_EnumToIntCache.TryGetValue(key, out num))
			{
				string value = sheet.ReadEnum(handle).Replace("-", string.Empty);
				object obj = Enum.Parse(typeof(T), value, true);
				num = (int)obj;
				StyleSheetCache.s_EnumToIntCache.Add(key, num);
			}
			Debug.Assert(Enum.GetName(typeof(T), num) != null);
			return num;
		}

		// Token: 0x060008B2 RID: 2226 RVA: 0x0001D168 File Offset: 0x0001B368
		internal static StylePropertyID[] GetPropertyIDs(StyleSheet sheet, int ruleIndex)
		{
			StyleSheetCache.SheetHandleKey key = new StyleSheetCache.SheetHandleKey(sheet, ruleIndex);
			StylePropertyID[] array;
			if (!StyleSheetCache.s_RulePropertyIDsCache.TryGetValue(key, out array))
			{
				StyleRule styleRule = sheet.rules[ruleIndex];
				array = new StylePropertyID[styleRule.properties.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = StyleSheetCache.GetPropertyID(sheet, styleRule, i);
				}
				StyleSheetCache.s_RulePropertyIDsCache.Add(key, array);
			}
			return array;
		}

		// Token: 0x060008B3 RID: 2227 RVA: 0x0001D1E4 File Offset: 0x0001B3E4
		private static string MapDeprecatedPropertyName(string name, string styleSheetName, int line)
		{
			string text;
			StyleSheetCache.s_DeprecatedNames.TryGetValue(name, out text);
			return text ?? name;
		}

		// Token: 0x060008B4 RID: 2228 RVA: 0x0001D210 File Offset: 0x0001B410
		private static StylePropertyID GetPropertyID(StyleSheet sheet, StyleRule rule, int index)
		{
			string text = rule.properties[index].name;
			text = StyleSheetCache.MapDeprecatedPropertyName(text, sheet.name, rule.line);
			StylePropertyID result;
			if (!StyleSheetCache.s_NameToIDCache.TryGetValue(text, out result))
			{
				result = StylePropertyID.Custom;
			}
			return result;
		}

		// Token: 0x060008B5 RID: 2229 RVA: 0x0001D260 File Offset: 0x0001B460
		// Note: this type is marked as 'beforefieldinit'.
		static StyleSheetCache()
		{
		}

		// Token: 0x04000337 RID: 823
		private static StyleSheetCache.SheetHandleKeyComparer s_Comparer = new StyleSheetCache.SheetHandleKeyComparer();

		// Token: 0x04000338 RID: 824
		private static Dictionary<StyleSheetCache.SheetHandleKey, int> s_EnumToIntCache = new Dictionary<StyleSheetCache.SheetHandleKey, int>(StyleSheetCache.s_Comparer);

		// Token: 0x04000339 RID: 825
		private static Dictionary<StyleSheetCache.SheetHandleKey, StylePropertyID[]> s_RulePropertyIDsCache = new Dictionary<StyleSheetCache.SheetHandleKey, StylePropertyID[]>(StyleSheetCache.s_Comparer);

		// Token: 0x0400033A RID: 826
		private static Dictionary<string, StylePropertyID> s_NameToIDCache = new Dictionary<string, StylePropertyID>
		{
			{
				"width",
				StylePropertyID.Width
			},
			{
				"height",
				StylePropertyID.Height
			},
			{
				"max-width",
				StylePropertyID.MaxWidth
			},
			{
				"max-height",
				StylePropertyID.MaxHeight
			},
			{
				"min-width",
				StylePropertyID.MinWidth
			},
			{
				"min-height",
				StylePropertyID.MinHeight
			},
			{
				"flex",
				StylePropertyID.Flex
			},
			{
				"flex-wrap",
				StylePropertyID.FlexWrap
			},
			{
				"flex-basis",
				StylePropertyID.FlexBasis
			},
			{
				"flex-grow",
				StylePropertyID.FlexGrow
			},
			{
				"flex-shrink",
				StylePropertyID.FlexShrink
			},
			{
				"overflow",
				StylePropertyID.Overflow
			},
			{
				"left",
				StylePropertyID.PositionLeft
			},
			{
				"top",
				StylePropertyID.PositionTop
			},
			{
				"right",
				StylePropertyID.PositionRight
			},
			{
				"bottom",
				StylePropertyID.PositionBottom
			},
			{
				"margin-left",
				StylePropertyID.MarginLeft
			},
			{
				"margin-top",
				StylePropertyID.MarginTop
			},
			{
				"margin-right",
				StylePropertyID.MarginRight
			},
			{
				"margin-bottom",
				StylePropertyID.MarginBottom
			},
			{
				"padding-left",
				StylePropertyID.PaddingLeft
			},
			{
				"padding-top",
				StylePropertyID.PaddingTop
			},
			{
				"padding-right",
				StylePropertyID.PaddingRight
			},
			{
				"padding-bottom",
				StylePropertyID.PaddingBottom
			},
			{
				"position",
				StylePropertyID.Position
			},
			{
				"-unity-position",
				StylePropertyID.PositionType
			},
			{
				"align-self",
				StylePropertyID.AlignSelf
			},
			{
				"-unity-text-align",
				StylePropertyID.UnityTextAlign
			},
			{
				"-unity-font-style",
				StylePropertyID.FontStyleAndWeight
			},
			{
				"-unity-clipping",
				StylePropertyID.TextClipping
			},
			{
				"-unity-font",
				StylePropertyID.Font
			},
			{
				"font-size",
				StylePropertyID.FontSize
			},
			{
				"-unity-word-wrap",
				StylePropertyID.WordWrap
			},
			{
				"color",
				StylePropertyID.Color
			},
			{
				"flex-direction",
				StylePropertyID.FlexDirection
			},
			{
				"background-color",
				StylePropertyID.BackgroundColor
			},
			{
				"border-color",
				StylePropertyID.BorderColor
			},
			{
				"background-image",
				StylePropertyID.BackgroundImage
			},
			{
				"-unity-background-scale-mode",
				StylePropertyID.BackgroundScaleMode
			},
			{
				"align-items",
				StylePropertyID.AlignItems
			},
			{
				"align-content",
				StylePropertyID.AlignContent
			},
			{
				"justify-content",
				StylePropertyID.JustifyContent
			},
			{
				"border-left-width",
				StylePropertyID.BorderLeftWidth
			},
			{
				"border-top-width",
				StylePropertyID.BorderTopWidth
			},
			{
				"border-right-width",
				StylePropertyID.BorderRightWidth
			},
			{
				"border-bottom-width",
				StylePropertyID.BorderBottomWidth
			},
			{
				"border-radius",
				StylePropertyID.BorderRadius
			},
			{
				"border-top-left-radius",
				StylePropertyID.BorderTopLeftRadius
			},
			{
				"border-top-right-radius",
				StylePropertyID.BorderTopRightRadius
			},
			{
				"border-bottom-right-radius",
				StylePropertyID.BorderBottomRightRadius
			},
			{
				"border-bottom-left-radius",
				StylePropertyID.BorderBottomLeftRadius
			},
			{
				"-unity-slice-left",
				StylePropertyID.SliceLeft
			},
			{
				"-unity-slice-top",
				StylePropertyID.SliceTop
			},
			{
				"-unity-slice-right",
				StylePropertyID.SliceRight
			},
			{
				"-unity-slice-bottom",
				StylePropertyID.SliceBottom
			},
			{
				"opacity",
				StylePropertyID.Opacity
			},
			{
				"cursor",
				StylePropertyID.Cursor
			},
			{
				"visibility",
				StylePropertyID.Visibility
			}
		};

		// Token: 0x0400033B RID: 827
		private static Dictionary<string, string> s_DeprecatedNames = new Dictionary<string, string>
		{
			{
				"position-left",
				"left"
			},
			{
				"position-top",
				"top"
			},
			{
				"position-right",
				"right"
			},
			{
				"position-bottom",
				"bottom"
			},
			{
				"text-color",
				"color"
			},
			{
				"slice-left",
				"-unity-slice-left"
			},
			{
				"slice-top",
				"-unity-slice-top"
			},
			{
				"slice-right",
				"-unity-slice-right"
			},
			{
				"slice-bottom",
				"-unity-slice-bottom"
			},
			{
				"text-alignment",
				"-unity-text-align"
			},
			{
				"word-wrap",
				"-unity-word-wrap"
			},
			{
				"font",
				"-unity-font"
			},
			{
				"background-size",
				"-unity-background-scale-mode"
			},
			{
				"font-style",
				"-unity-font-style"
			},
			{
				"position-type",
				"-unity-position"
			},
			{
				"text-clipping",
				"-unity-clipping"
			},
			{
				"border-left",
				"border-left-width"
			},
			{
				"border-top",
				"border-top-width"
			},
			{
				"border-right",
				"border-right-width"
			},
			{
				"border-bottom",
				"border-bottom-width"
			}
		};

		// Token: 0x02000150 RID: 336
		private struct SheetHandleKey
		{
			// Token: 0x060008B6 RID: 2230 RVA: 0x0001D6D6 File Offset: 0x0001B8D6
			public SheetHandleKey(StyleSheet sheet, int index)
			{
				this.sheetInstanceID = sheet.GetInstanceID();
				this.index = index;
			}

			// Token: 0x0400033C RID: 828
			public readonly int sheetInstanceID;

			// Token: 0x0400033D RID: 829
			public readonly int index;
		}

		// Token: 0x02000151 RID: 337
		private class SheetHandleKeyComparer : IEqualityComparer<StyleSheetCache.SheetHandleKey>
		{
			// Token: 0x060008B7 RID: 2231 RVA: 0x00002223 File Offset: 0x00000423
			public SheetHandleKeyComparer()
			{
			}

			// Token: 0x060008B8 RID: 2232 RVA: 0x0001D6EC File Offset: 0x0001B8EC
			public bool Equals(StyleSheetCache.SheetHandleKey x, StyleSheetCache.SheetHandleKey y)
			{
				return x.sheetInstanceID == y.sheetInstanceID && x.index == y.index;
			}

			// Token: 0x060008B9 RID: 2233 RVA: 0x0001D728 File Offset: 0x0001B928
			public int GetHashCode(StyleSheetCache.SheetHandleKey key)
			{
				return key.sheetInstanceID.GetHashCode() ^ key.index.GetHashCode();
			}
		}
	}
}
