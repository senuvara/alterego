﻿using System;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000153 RID: 339
	// (Invoke) Token: 0x060008BF RID: 2239
	internal delegate void ShorthandApplicatorFunction(StyleSheet sheet, StyleValueHandle[] handles, int specificity, VisualElementStylesData styleData);
}
