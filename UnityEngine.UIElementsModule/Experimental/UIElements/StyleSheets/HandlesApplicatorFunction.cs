﻿using System;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000152 RID: 338
	// (Invoke) Token: 0x060008BB RID: 2235
	internal delegate void HandlesApplicatorFunction<T>(StyleSheet sheet, StyleValueHandle[] handles, int specificity, ref StyleValue<T> property);
}
