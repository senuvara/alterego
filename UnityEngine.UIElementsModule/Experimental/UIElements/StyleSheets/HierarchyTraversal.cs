﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000036 RID: 54
	internal abstract class HierarchyTraversal
	{
		// Token: 0x06000100 RID: 256 RVA: 0x00002223 File Offset: 0x00000423
		protected HierarchyTraversal()
		{
		}

		// Token: 0x06000101 RID: 257 RVA: 0x00005A7A File Offset: 0x00003C7A
		public virtual void Traverse(VisualElement element)
		{
			this.TraverseRecursive(element, 0);
		}

		// Token: 0x06000102 RID: 258
		public abstract void TraverseRecursive(VisualElement element, int depth);

		// Token: 0x06000103 RID: 259 RVA: 0x00005A88 File Offset: 0x00003C88
		protected void Recurse(VisualElement element, int depth)
		{
			int i = 0;
			while (i < element.shadow.childCount)
			{
				VisualElement visualElement = element.shadow[i];
				this.TraverseRecursive(visualElement, depth + 1);
				if (visualElement.shadow.parent == element)
				{
					i++;
				}
			}
		}
	}
}
