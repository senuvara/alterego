﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Experimental.UIElements.StyleEnums;
using UnityEngine.StyleSheets;
using UnityEngine.Yoga;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x0200015B RID: 347
	internal class VisualElementStylesData : ICustomStyle
	{
		// Token: 0x060008DC RID: 2268 RVA: 0x0001DC40 File Offset: 0x0001BE40
		public VisualElementStylesData(bool isShared)
		{
			this.isShared = isShared;
		}

		// Token: 0x060008DD RID: 2269 RVA: 0x0001DC50 File Offset: 0x0001BE50
		public void Apply(VisualElementStylesData other, StylePropertyApplyMode mode)
		{
			this.m_CustomProperties = other.m_CustomProperties;
			this.width.Apply(other.width, mode);
			this.height.Apply(other.height, mode);
			this.maxWidth.Apply(other.maxWidth, mode);
			this.maxHeight.Apply(other.maxHeight, mode);
			this.minWidth.Apply(other.minWidth, mode);
			this.minHeight.Apply(other.minHeight, mode);
			this.flexBasis.Apply(other.flexBasis, mode);
			this.flexGrow.Apply(other.flexGrow, mode);
			this.flexShrink.Apply(other.flexShrink, mode);
			this.overflow.Apply(other.overflow, mode);
			this.positionLeft.Apply(other.positionLeft, mode);
			this.positionTop.Apply(other.positionTop, mode);
			this.positionRight.Apply(other.positionRight, mode);
			this.positionBottom.Apply(other.positionBottom, mode);
			this.marginLeft.Apply(other.marginLeft, mode);
			this.marginTop.Apply(other.marginTop, mode);
			this.marginRight.Apply(other.marginRight, mode);
			this.marginBottom.Apply(other.marginBottom, mode);
			this.paddingLeft.Apply(other.paddingLeft, mode);
			this.paddingTop.Apply(other.paddingTop, mode);
			this.paddingRight.Apply(other.paddingRight, mode);
			this.paddingBottom.Apply(other.paddingBottom, mode);
			this.positionType.Apply(other.positionType, mode);
			this.alignSelf.Apply(other.alignSelf, mode);
			this.unityTextAlign.Apply(other.unityTextAlign, mode);
			this.fontStyleAndWeight.Apply(other.fontStyleAndWeight, mode);
			this.textClipping.Apply(other.textClipping, mode);
			this.fontSize.Apply(other.fontSize, mode);
			this.font.Apply(other.font, mode);
			this.wordWrap.Apply(other.wordWrap, mode);
			this.color.Apply(other.color, mode);
			this.flexDirection.Apply(other.flexDirection, mode);
			this.backgroundColor.Apply(other.backgroundColor, mode);
			this.borderColor.Apply(other.borderColor, mode);
			this.backgroundImage.Apply(other.backgroundImage, mode);
			this.backgroundScaleMode.Apply(other.backgroundScaleMode, mode);
			this.alignItems.Apply(other.alignItems, mode);
			this.alignContent.Apply(other.alignContent, mode);
			this.justifyContent.Apply(other.justifyContent, mode);
			this.flexWrap.Apply(other.flexWrap, mode);
			this.borderLeftWidth.Apply(other.borderLeftWidth, mode);
			this.borderTopWidth.Apply(other.borderTopWidth, mode);
			this.borderRightWidth.Apply(other.borderRightWidth, mode);
			this.borderBottomWidth.Apply(other.borderBottomWidth, mode);
			this.borderTopLeftRadius.Apply(other.borderTopLeftRadius, mode);
			this.borderTopRightRadius.Apply(other.borderTopRightRadius, mode);
			this.borderBottomRightRadius.Apply(other.borderBottomRightRadius, mode);
			this.borderBottomLeftRadius.Apply(other.borderBottomLeftRadius, mode);
			this.sliceLeft.Apply(other.sliceLeft, mode);
			this.sliceTop.Apply(other.sliceTop, mode);
			this.sliceRight.Apply(other.sliceRight, mode);
			this.sliceBottom.Apply(other.sliceBottom, mode);
			this.opacity.Apply(other.opacity, mode);
			this.cursor.Apply(other.cursor, mode);
			this.visibility.Apply(other.visibility, mode);
		}

		// Token: 0x060008DE RID: 2270 RVA: 0x0001E080 File Offset: 0x0001C280
		public void WriteToGUIStyle(GUIStyle style)
		{
			style.alignment = (TextAnchor)this.unityTextAlign.GetSpecifiedValueOrDefault((int)style.alignment);
			style.wordWrap = this.wordWrap.GetSpecifiedValueOrDefault(style.wordWrap);
			style.clipping = (TextClipping)this.textClipping.GetSpecifiedValueOrDefault((int)style.clipping);
			if (this.font.value != null)
			{
				style.font = this.font.value;
			}
			style.fontSize = this.fontSize.GetSpecifiedValueOrDefault(style.fontSize);
			style.fontStyle = (FontStyle)this.fontStyleAndWeight.GetSpecifiedValueOrDefault((int)style.fontStyle);
			this.AssignRect(style.margin, ref this.marginLeft, ref this.marginTop, ref this.marginRight, ref this.marginBottom);
			this.AssignRect(style.padding, ref this.paddingLeft, ref this.paddingTop, ref this.paddingRight, ref this.paddingBottom);
			this.AssignRect(style.border, ref this.sliceLeft, ref this.sliceTop, ref this.sliceRight, ref this.sliceBottom);
			this.AssignState(style.normal);
			this.AssignState(style.focused);
			this.AssignState(style.hover);
			this.AssignState(style.active);
			this.AssignState(style.onNormal);
			this.AssignState(style.onFocused);
			this.AssignState(style.onHover);
			this.AssignState(style.onActive);
		}

		// Token: 0x060008DF RID: 2271 RVA: 0x0001E1F8 File Offset: 0x0001C3F8
		private void AssignState(GUIStyleState state)
		{
			state.textColor = this.color.GetSpecifiedValueOrDefault(state.textColor);
			if (this.backgroundImage.value != null)
			{
				state.background = this.backgroundImage.value;
			}
		}

		// Token: 0x060008E0 RID: 2272 RVA: 0x0001E248 File Offset: 0x0001C448
		private void AssignRect(RectOffset rect, ref StyleValue<int> left, ref StyleValue<int> top, ref StyleValue<int> right, ref StyleValue<int> bottom)
		{
			rect.left = left.GetSpecifiedValueOrDefault(rect.left);
			rect.top = top.GetSpecifiedValueOrDefault(rect.top);
			rect.right = right.GetSpecifiedValueOrDefault(rect.right);
			rect.bottom = bottom.GetSpecifiedValueOrDefault(rect.bottom);
		}

		// Token: 0x060008E1 RID: 2273 RVA: 0x0001E2A0 File Offset: 0x0001C4A0
		private void AssignRect(RectOffset rect, ref StyleValue<float> left, ref StyleValue<float> top, ref StyleValue<float> right, ref StyleValue<float> bottom)
		{
			rect.left = (int)left.GetSpecifiedValueOrDefault((float)rect.left);
			rect.top = (int)top.GetSpecifiedValueOrDefault((float)rect.top);
			rect.right = (int)right.GetSpecifiedValueOrDefault((float)rect.right);
			rect.bottom = (int)bottom.GetSpecifiedValueOrDefault((float)rect.bottom);
		}

		// Token: 0x060008E2 RID: 2274 RVA: 0x0001E300 File Offset: 0x0001C500
		public void ApplyLayoutValues()
		{
			if (this.yogaNode == null)
			{
				this.yogaNode = new YogaNode(null);
			}
			this.SyncWithLayout(this.yogaNode);
		}

		// Token: 0x060008E3 RID: 2275 RVA: 0x0001E328 File Offset: 0x0001C528
		public StyleValue<float> FlexBasisToFloat()
		{
			StyleValue<float> result;
			if (this.flexBasis.value.isKeyword)
			{
				if (this.flexBasis.value.keyword == StyleValueKeyword.Auto)
				{
					result = new StyleValue<float>(-1f, this.flexBasis.specificity);
				}
				else
				{
					result = new StyleValue<float>(0f, this.flexBasis.specificity);
				}
			}
			else
			{
				result = new StyleValue<float>(this.flexBasis.value.floatValue, this.flexBasis.specificity);
			}
			return result;
		}

		// Token: 0x060008E4 RID: 2276 RVA: 0x0001E3C4 File Offset: 0x0001C5C4
		public void SyncWithLayout(YogaNode targetNode)
		{
			targetNode.Flex = float.NaN;
			float specifiedValueOrDefault = this.FlexBasisToFloat().GetSpecifiedValueOrDefault(float.NaN);
			if (specifiedValueOrDefault == -1f)
			{
				targetNode.FlexBasis = YogaValue.Auto();
			}
			else
			{
				targetNode.FlexBasis = specifiedValueOrDefault;
			}
			targetNode.FlexGrow = this.flexGrow.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.FlexShrink = this.flexShrink.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.Left = this.positionLeft.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.Top = this.positionTop.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.Right = this.positionRight.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.Bottom = this.positionBottom.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.MarginLeft = this.marginLeft.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.MarginTop = this.marginTop.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.MarginRight = this.marginRight.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.MarginBottom = this.marginBottom.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.PaddingLeft = this.paddingLeft.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.PaddingTop = this.paddingTop.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.PaddingRight = this.paddingRight.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.PaddingBottom = this.paddingBottom.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.BorderLeftWidth = this.borderLeftWidth.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.BorderTopWidth = this.borderTopWidth.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.BorderRightWidth = this.borderRightWidth.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.BorderBottomWidth = this.borderBottomWidth.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.Width = this.width.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.Height = this.height.GetSpecifiedValueOrDefault(float.NaN);
			PositionType value = (PositionType)this.positionType.value;
			if (value != PositionType.Absolute && value != PositionType.Manual)
			{
				if (value == PositionType.Relative)
				{
					targetNode.PositionType = YogaPositionType.Relative;
				}
			}
			else
			{
				targetNode.PositionType = YogaPositionType.Absolute;
			}
			targetNode.Overflow = (YogaOverflow)this.overflow.value;
			targetNode.AlignSelf = (YogaAlign)this.alignSelf.value;
			targetNode.MaxWidth = this.maxWidth.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.MaxHeight = this.maxHeight.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.MinWidth = this.minWidth.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.MinHeight = this.minHeight.GetSpecifiedValueOrDefault(float.NaN);
			targetNode.FlexDirection = (YogaFlexDirection)this.flexDirection.value;
			targetNode.AlignContent = (YogaAlign)this.alignContent.GetSpecifiedValueOrDefault(1);
			targetNode.AlignItems = (YogaAlign)this.alignItems.GetSpecifiedValueOrDefault(4);
			targetNode.JustifyContent = (YogaJustify)this.justifyContent.value;
			targetNode.Wrap = (YogaWrap)this.flexWrap.value;
		}

		// Token: 0x060008E5 RID: 2277 RVA: 0x0001E73C File Offset: 0x0001C93C
		internal void ApplyRule(StyleSheet registry, int specificity, StyleRule rule, StylePropertyID[] propertyIDs)
		{
			int i = 0;
			while (i < rule.properties.Length)
			{
				StyleProperty styleProperty = rule.properties[i];
				StylePropertyID stylePropertyID = propertyIDs[i];
				StyleValueHandle[] values = styleProperty.values;
				switch (stylePropertyID)
				{
				case StylePropertyID.MarginLeft:
				{
					StyleValueHandle[] handles = values;
					if (VisualElementStylesData.<>f__mg$cacheF == null)
					{
						VisualElementStylesData.<>f__mg$cacheF = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles, specificity, ref this.marginLeft, VisualElementStylesData.<>f__mg$cacheF);
					break;
				}
				case StylePropertyID.MarginTop:
				{
					StyleValueHandle[] handles2 = values;
					if (VisualElementStylesData.<>f__mg$cache10 == null)
					{
						VisualElementStylesData.<>f__mg$cache10 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles2, specificity, ref this.marginTop, VisualElementStylesData.<>f__mg$cache10);
					break;
				}
				case StylePropertyID.MarginRight:
				{
					StyleValueHandle[] handles3 = values;
					if (VisualElementStylesData.<>f__mg$cache11 == null)
					{
						VisualElementStylesData.<>f__mg$cache11 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles3, specificity, ref this.marginRight, VisualElementStylesData.<>f__mg$cache11);
					break;
				}
				case StylePropertyID.MarginBottom:
				{
					StyleValueHandle[] handles4 = values;
					if (VisualElementStylesData.<>f__mg$cache12 == null)
					{
						VisualElementStylesData.<>f__mg$cache12 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles4, specificity, ref this.marginBottom, VisualElementStylesData.<>f__mg$cache12);
					break;
				}
				case StylePropertyID.PaddingLeft:
				{
					StyleValueHandle[] handles5 = values;
					if (VisualElementStylesData.<>f__mg$cache18 == null)
					{
						VisualElementStylesData.<>f__mg$cache18 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles5, specificity, ref this.paddingLeft, VisualElementStylesData.<>f__mg$cache18);
					break;
				}
				case StylePropertyID.PaddingTop:
				{
					StyleValueHandle[] handles6 = values;
					if (VisualElementStylesData.<>f__mg$cache19 == null)
					{
						VisualElementStylesData.<>f__mg$cache19 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles6, specificity, ref this.paddingTop, VisualElementStylesData.<>f__mg$cache19);
					break;
				}
				case StylePropertyID.PaddingRight:
				{
					StyleValueHandle[] handles7 = values;
					if (VisualElementStylesData.<>f__mg$cache1A == null)
					{
						VisualElementStylesData.<>f__mg$cache1A = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles7, specificity, ref this.paddingRight, VisualElementStylesData.<>f__mg$cache1A);
					break;
				}
				case StylePropertyID.PaddingBottom:
				{
					StyleValueHandle[] handles8 = values;
					if (VisualElementStylesData.<>f__mg$cache1B == null)
					{
						VisualElementStylesData.<>f__mg$cache1B = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles8, specificity, ref this.paddingBottom, VisualElementStylesData.<>f__mg$cache1B);
					break;
				}
				case StylePropertyID.Position:
				{
					StyleValueHandle[] handles9 = values;
					if (VisualElementStylesData.<>f__mg$cache1C == null)
					{
						VisualElementStylesData.<>f__mg$cache1C = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<Position>);
					}
					registry.Apply(handles9, specificity, ref this.positionType, VisualElementStylesData.<>f__mg$cache1C);
					break;
				}
				case StylePropertyID.PositionType:
				{
					StyleValueHandle[] handles10 = values;
					if (VisualElementStylesData.<>f__mg$cache1D == null)
					{
						VisualElementStylesData.<>f__mg$cache1D = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<PositionType>);
					}
					registry.Apply(handles10, specificity, ref this.positionType, VisualElementStylesData.<>f__mg$cache1D);
					break;
				}
				case StylePropertyID.PositionLeft:
				{
					StyleValueHandle[] handles11 = values;
					if (VisualElementStylesData.<>f__mg$cache20 == null)
					{
						VisualElementStylesData.<>f__mg$cache20 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles11, specificity, ref this.positionLeft, VisualElementStylesData.<>f__mg$cache20);
					break;
				}
				case StylePropertyID.PositionTop:
				{
					StyleValueHandle[] handles12 = values;
					if (VisualElementStylesData.<>f__mg$cache1E == null)
					{
						VisualElementStylesData.<>f__mg$cache1E = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles12, specificity, ref this.positionTop, VisualElementStylesData.<>f__mg$cache1E);
					break;
				}
				case StylePropertyID.PositionRight:
				{
					StyleValueHandle[] handles13 = values;
					if (VisualElementStylesData.<>f__mg$cache21 == null)
					{
						VisualElementStylesData.<>f__mg$cache21 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles13, specificity, ref this.positionRight, VisualElementStylesData.<>f__mg$cache21);
					break;
				}
				case StylePropertyID.PositionBottom:
				{
					StyleValueHandle[] handles14 = values;
					if (VisualElementStylesData.<>f__mg$cache1F == null)
					{
						VisualElementStylesData.<>f__mg$cache1F = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles14, specificity, ref this.positionBottom, VisualElementStylesData.<>f__mg$cache1F);
					break;
				}
				case StylePropertyID.Width:
				{
					StyleValueHandle[] handles15 = values;
					if (VisualElementStylesData.<>f__mg$cache25 == null)
					{
						VisualElementStylesData.<>f__mg$cache25 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles15, specificity, ref this.width, VisualElementStylesData.<>f__mg$cache25);
					break;
				}
				case StylePropertyID.Height:
				{
					StyleValueHandle[] handles16 = values;
					if (VisualElementStylesData.<>f__mg$cacheD == null)
					{
						VisualElementStylesData.<>f__mg$cacheD = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles16, specificity, ref this.height, VisualElementStylesData.<>f__mg$cacheD);
					break;
				}
				case StylePropertyID.MinWidth:
				{
					StyleValueHandle[] handles17 = values;
					if (VisualElementStylesData.<>f__mg$cache16 == null)
					{
						VisualElementStylesData.<>f__mg$cache16 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles17, specificity, ref this.minWidth, VisualElementStylesData.<>f__mg$cache16);
					break;
				}
				case StylePropertyID.MinHeight:
				{
					StyleValueHandle[] handles18 = values;
					if (VisualElementStylesData.<>f__mg$cache15 == null)
					{
						VisualElementStylesData.<>f__mg$cache15 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles18, specificity, ref this.minHeight, VisualElementStylesData.<>f__mg$cache15);
					break;
				}
				case StylePropertyID.MaxWidth:
				{
					StyleValueHandle[] handles19 = values;
					if (VisualElementStylesData.<>f__mg$cache14 == null)
					{
						VisualElementStylesData.<>f__mg$cache14 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles19, specificity, ref this.maxWidth, VisualElementStylesData.<>f__mg$cache14);
					break;
				}
				case StylePropertyID.MaxHeight:
				{
					StyleValueHandle[] handles20 = values;
					if (VisualElementStylesData.<>f__mg$cache13 == null)
					{
						VisualElementStylesData.<>f__mg$cache13 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles20, specificity, ref this.maxHeight, VisualElementStylesData.<>f__mg$cache13);
					break;
				}
				case StylePropertyID.FlexBasis:
				{
					StyleValueHandle[] handles21 = values;
					if (VisualElementStylesData.<>f__mg$cache5 == null)
					{
						VisualElementStylesData.<>f__mg$cache5 = new HandlesApplicatorFunction<FloatOrKeyword>(StyleSheetApplicator.ApplyFloatOrKeyword);
					}
					registry.Apply(handles21, specificity, ref this.flexBasis, VisualElementStylesData.<>f__mg$cache5);
					break;
				}
				case StylePropertyID.FlexGrow:
				{
					StyleValueHandle[] handles22 = values;
					if (VisualElementStylesData.<>f__mg$cache6 == null)
					{
						VisualElementStylesData.<>f__mg$cache6 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles22, specificity, ref this.flexGrow, VisualElementStylesData.<>f__mg$cache6);
					break;
				}
				case StylePropertyID.FlexShrink:
				{
					StyleValueHandle[] handles23 = values;
					if (VisualElementStylesData.<>f__mg$cache7 == null)
					{
						VisualElementStylesData.<>f__mg$cache7 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles23, specificity, ref this.flexShrink, VisualElementStylesData.<>f__mg$cache7);
					break;
				}
				case StylePropertyID.BorderLeftWidth:
				{
					StyleValueHandle[] handles24 = values;
					if (VisualElementStylesData.<>f__mg$cache2A == null)
					{
						VisualElementStylesData.<>f__mg$cache2A = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles24, specificity, ref this.borderLeftWidth, VisualElementStylesData.<>f__mg$cache2A);
					break;
				}
				case StylePropertyID.BorderTopWidth:
				{
					StyleValueHandle[] handles25 = values;
					if (VisualElementStylesData.<>f__mg$cache2B == null)
					{
						VisualElementStylesData.<>f__mg$cache2B = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles25, specificity, ref this.borderTopWidth, VisualElementStylesData.<>f__mg$cache2B);
					break;
				}
				case StylePropertyID.BorderRightWidth:
				{
					StyleValueHandle[] handles26 = values;
					if (VisualElementStylesData.<>f__mg$cache2C == null)
					{
						VisualElementStylesData.<>f__mg$cache2C = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles26, specificity, ref this.borderRightWidth, VisualElementStylesData.<>f__mg$cache2C);
					break;
				}
				case StylePropertyID.BorderBottomWidth:
				{
					StyleValueHandle[] handles27 = values;
					if (VisualElementStylesData.<>f__mg$cache2D == null)
					{
						VisualElementStylesData.<>f__mg$cache2D = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles27, specificity, ref this.borderBottomWidth, VisualElementStylesData.<>f__mg$cache2D);
					break;
				}
				case StylePropertyID.BorderTopLeftRadius:
				{
					StyleValueHandle[] handles28 = values;
					if (VisualElementStylesData.<>f__mg$cache2E == null)
					{
						VisualElementStylesData.<>f__mg$cache2E = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles28, specificity, ref this.borderTopLeftRadius, VisualElementStylesData.<>f__mg$cache2E);
					break;
				}
				case StylePropertyID.BorderTopRightRadius:
				{
					StyleValueHandle[] handles29 = values;
					if (VisualElementStylesData.<>f__mg$cache2F == null)
					{
						VisualElementStylesData.<>f__mg$cache2F = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles29, specificity, ref this.borderTopRightRadius, VisualElementStylesData.<>f__mg$cache2F);
					break;
				}
				case StylePropertyID.BorderBottomRightRadius:
				{
					StyleValueHandle[] handles30 = values;
					if (VisualElementStylesData.<>f__mg$cache30 == null)
					{
						VisualElementStylesData.<>f__mg$cache30 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles30, specificity, ref this.borderBottomRightRadius, VisualElementStylesData.<>f__mg$cache30);
					break;
				}
				case StylePropertyID.BorderBottomLeftRadius:
				{
					StyleValueHandle[] handles31 = values;
					if (VisualElementStylesData.<>f__mg$cache31 == null)
					{
						VisualElementStylesData.<>f__mg$cache31 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles31, specificity, ref this.borderBottomLeftRadius, VisualElementStylesData.<>f__mg$cache31);
					break;
				}
				case StylePropertyID.FlexDirection:
				{
					StyleValueHandle[] handles32 = values;
					if (VisualElementStylesData.<>f__mg$cacheB == null)
					{
						VisualElementStylesData.<>f__mg$cacheB = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<FlexDirection>);
					}
					registry.Apply(handles32, specificity, ref this.flexDirection, VisualElementStylesData.<>f__mg$cacheB);
					break;
				}
				case StylePropertyID.FlexWrap:
				{
					StyleValueHandle[] handles33 = values;
					if (VisualElementStylesData.<>f__mg$cacheC == null)
					{
						VisualElementStylesData.<>f__mg$cacheC = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<Wrap>);
					}
					registry.Apply(handles33, specificity, ref this.flexWrap, VisualElementStylesData.<>f__mg$cacheC);
					break;
				}
				case StylePropertyID.JustifyContent:
				{
					StyleValueHandle[] handles34 = values;
					if (VisualElementStylesData.<>f__mg$cacheE == null)
					{
						VisualElementStylesData.<>f__mg$cacheE = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<Justify>);
					}
					registry.Apply(handles34, specificity, ref this.justifyContent, VisualElementStylesData.<>f__mg$cacheE);
					break;
				}
				case StylePropertyID.AlignContent:
				{
					StyleValueHandle[] handles35 = values;
					if (VisualElementStylesData.<>f__mg$cache0 == null)
					{
						VisualElementStylesData.<>f__mg$cache0 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<Align>);
					}
					registry.Apply(handles35, specificity, ref this.alignContent, VisualElementStylesData.<>f__mg$cache0);
					break;
				}
				case StylePropertyID.AlignSelf:
				{
					StyleValueHandle[] handles36 = values;
					if (VisualElementStylesData.<>f__mg$cache2 == null)
					{
						VisualElementStylesData.<>f__mg$cache2 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<Align>);
					}
					registry.Apply(handles36, specificity, ref this.alignSelf, VisualElementStylesData.<>f__mg$cache2);
					break;
				}
				case StylePropertyID.AlignItems:
				{
					StyleValueHandle[] handles37 = values;
					if (VisualElementStylesData.<>f__mg$cache1 == null)
					{
						VisualElementStylesData.<>f__mg$cache1 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<Align>);
					}
					registry.Apply(handles37, specificity, ref this.alignItems, VisualElementStylesData.<>f__mg$cache1);
					break;
				}
				case StylePropertyID.UnityTextAlign:
				{
					StyleValueHandle[] handles38 = values;
					if (VisualElementStylesData.<>f__mg$cache22 == null)
					{
						VisualElementStylesData.<>f__mg$cache22 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<TextAnchor>);
					}
					registry.Apply(handles38, specificity, ref this.unityTextAlign, VisualElementStylesData.<>f__mg$cache22);
					break;
				}
				case StylePropertyID.TextClipping:
				{
					StyleValueHandle[] handles39 = values;
					if (VisualElementStylesData.<>f__mg$cache23 == null)
					{
						VisualElementStylesData.<>f__mg$cache23 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<TextClipping>);
					}
					registry.Apply(handles39, specificity, ref this.textClipping, VisualElementStylesData.<>f__mg$cache23);
					break;
				}
				case StylePropertyID.Font:
				{
					StyleValueHandle[] handles40 = values;
					if (VisualElementStylesData.<>f__mg$cache8 == null)
					{
						VisualElementStylesData.<>f__mg$cache8 = new HandlesApplicatorFunction<Font>(StyleSheetApplicator.ApplyFont);
					}
					registry.Apply(handles40, specificity, ref this.font, VisualElementStylesData.<>f__mg$cache8);
					break;
				}
				case StylePropertyID.FontSize:
				{
					StyleValueHandle[] handles41 = values;
					if (VisualElementStylesData.<>f__mg$cache9 == null)
					{
						VisualElementStylesData.<>f__mg$cache9 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyInt);
					}
					registry.Apply(handles41, specificity, ref this.fontSize, VisualElementStylesData.<>f__mg$cache9);
					break;
				}
				case StylePropertyID.FontStyleAndWeight:
				{
					StyleValueHandle[] handles42 = values;
					if (VisualElementStylesData.<>f__mg$cacheA == null)
					{
						VisualElementStylesData.<>f__mg$cacheA = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<FontStyle>);
					}
					registry.Apply(handles42, specificity, ref this.fontStyleAndWeight, VisualElementStylesData.<>f__mg$cacheA);
					break;
				}
				case StylePropertyID.BackgroundScaleMode:
				{
					StyleValueHandle[] handles43 = values;
					if (VisualElementStylesData.<>f__mg$cache28 == null)
					{
						VisualElementStylesData.<>f__mg$cache28 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyInt);
					}
					registry.Apply(handles43, specificity, ref this.backgroundScaleMode, VisualElementStylesData.<>f__mg$cache28);
					break;
				}
				case StylePropertyID.Cursor:
				{
					StyleValueHandle[] handles44 = values;
					if (VisualElementStylesData.<>f__mg$cache32 == null)
					{
						VisualElementStylesData.<>f__mg$cache32 = new HandlesApplicatorFunction<CursorStyle>(StyleSheetApplicator.ApplyCursor);
					}
					registry.Apply(handles44, specificity, ref this.cursor, VisualElementStylesData.<>f__mg$cache32);
					break;
				}
				case StylePropertyID.Visibility:
				{
					StyleValueHandle[] handles45 = values;
					if (VisualElementStylesData.<>f__mg$cache3C == null)
					{
						VisualElementStylesData.<>f__mg$cache3C = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<Visibility>);
					}
					registry.Apply(handles45, specificity, ref this.visibility, VisualElementStylesData.<>f__mg$cache3C);
					break;
				}
				case StylePropertyID.WordWrap:
				{
					StyleValueHandle[] handles46 = values;
					if (VisualElementStylesData.<>f__mg$cache26 == null)
					{
						VisualElementStylesData.<>f__mg$cache26 = new HandlesApplicatorFunction<bool>(StyleSheetApplicator.ApplyBool);
					}
					registry.Apply(handles46, specificity, ref this.wordWrap, VisualElementStylesData.<>f__mg$cache26);
					break;
				}
				case StylePropertyID.BackgroundImage:
				{
					StyleValueHandle[] handles47 = values;
					if (VisualElementStylesData.<>f__mg$cache3 == null)
					{
						VisualElementStylesData.<>f__mg$cache3 = new HandlesApplicatorFunction<Texture2D>(StyleSheetApplicator.ApplyImage);
					}
					registry.Apply(handles47, specificity, ref this.backgroundImage, VisualElementStylesData.<>f__mg$cache3);
					break;
				}
				case StylePropertyID.Color:
				{
					StyleValueHandle[] handles48 = values;
					if (VisualElementStylesData.<>f__mg$cache24 == null)
					{
						VisualElementStylesData.<>f__mg$cache24 = new HandlesApplicatorFunction<Color>(StyleSheetApplicator.ApplyColor);
					}
					registry.Apply(handles48, specificity, ref this.color, VisualElementStylesData.<>f__mg$cache24);
					break;
				}
				case StylePropertyID.BackgroundColor:
				{
					StyleValueHandle[] handles49 = values;
					if (VisualElementStylesData.<>f__mg$cache27 == null)
					{
						VisualElementStylesData.<>f__mg$cache27 = new HandlesApplicatorFunction<Color>(StyleSheetApplicator.ApplyColor);
					}
					registry.Apply(handles49, specificity, ref this.backgroundColor, VisualElementStylesData.<>f__mg$cache27);
					break;
				}
				case StylePropertyID.BorderColor:
				{
					StyleValueHandle[] handles50 = values;
					if (VisualElementStylesData.<>f__mg$cache29 == null)
					{
						VisualElementStylesData.<>f__mg$cache29 = new HandlesApplicatorFunction<Color>(StyleSheetApplicator.ApplyColor);
					}
					registry.Apply(handles50, specificity, ref this.borderColor, VisualElementStylesData.<>f__mg$cache29);
					break;
				}
				case StylePropertyID.Overflow:
				{
					StyleValueHandle[] handles51 = values;
					if (VisualElementStylesData.<>f__mg$cache17 == null)
					{
						VisualElementStylesData.<>f__mg$cache17 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyEnum<Overflow>);
					}
					registry.Apply(handles51, specificity, ref this.overflow, VisualElementStylesData.<>f__mg$cache17);
					break;
				}
				case StylePropertyID.SliceLeft:
				{
					StyleValueHandle[] handles52 = values;
					if (VisualElementStylesData.<>f__mg$cache33 == null)
					{
						VisualElementStylesData.<>f__mg$cache33 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyInt);
					}
					registry.Apply(handles52, specificity, ref this.sliceLeft, VisualElementStylesData.<>f__mg$cache33);
					break;
				}
				case StylePropertyID.SliceTop:
				{
					StyleValueHandle[] handles53 = values;
					if (VisualElementStylesData.<>f__mg$cache34 == null)
					{
						VisualElementStylesData.<>f__mg$cache34 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyInt);
					}
					registry.Apply(handles53, specificity, ref this.sliceTop, VisualElementStylesData.<>f__mg$cache34);
					break;
				}
				case StylePropertyID.SliceRight:
				{
					StyleValueHandle[] handles54 = values;
					if (VisualElementStylesData.<>f__mg$cache35 == null)
					{
						VisualElementStylesData.<>f__mg$cache35 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyInt);
					}
					registry.Apply(handles54, specificity, ref this.sliceRight, VisualElementStylesData.<>f__mg$cache35);
					break;
				}
				case StylePropertyID.SliceBottom:
				{
					StyleValueHandle[] handles55 = values;
					if (VisualElementStylesData.<>f__mg$cache36 == null)
					{
						VisualElementStylesData.<>f__mg$cache36 = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyInt);
					}
					registry.Apply(handles55, specificity, ref this.sliceBottom, VisualElementStylesData.<>f__mg$cache36);
					break;
				}
				case StylePropertyID.Opacity:
				{
					StyleValueHandle[] handles56 = values;
					if (VisualElementStylesData.<>f__mg$cache37 == null)
					{
						VisualElementStylesData.<>f__mg$cache37 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles56, specificity, ref this.opacity, VisualElementStylesData.<>f__mg$cache37);
					break;
				}
				case StylePropertyID.BorderRadius:
				{
					StyleValueHandle[] handles57 = values;
					if (VisualElementStylesData.<>f__mg$cache38 == null)
					{
						VisualElementStylesData.<>f__mg$cache38 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles57, specificity, ref this.borderTopLeftRadius, VisualElementStylesData.<>f__mg$cache38);
					StyleValueHandle[] handles58 = values;
					if (VisualElementStylesData.<>f__mg$cache39 == null)
					{
						VisualElementStylesData.<>f__mg$cache39 = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles58, specificity, ref this.borderTopRightRadius, VisualElementStylesData.<>f__mg$cache39);
					StyleValueHandle[] handles59 = values;
					if (VisualElementStylesData.<>f__mg$cache3A == null)
					{
						VisualElementStylesData.<>f__mg$cache3A = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles59, specificity, ref this.borderBottomLeftRadius, VisualElementStylesData.<>f__mg$cache3A);
					StyleValueHandle[] handles60 = values;
					if (VisualElementStylesData.<>f__mg$cache3B == null)
					{
						VisualElementStylesData.<>f__mg$cache3B = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
					}
					registry.Apply(handles60, specificity, ref this.borderBottomRightRadius, VisualElementStylesData.<>f__mg$cache3B);
					break;
				}
				case StylePropertyID.Flex:
				{
					StyleValueHandle[] handles61 = values;
					if (VisualElementStylesData.<>f__mg$cache4 == null)
					{
						VisualElementStylesData.<>f__mg$cache4 = new ShorthandApplicatorFunction(StyleSheetApplicator.ApplyFlexShorthand);
					}
					registry.ApplyShorthand(handles61, specificity, this, VisualElementStylesData.<>f__mg$cache4);
					break;
				}
				case StylePropertyID.Margin:
				case StylePropertyID.Padding:
					goto IL_CF4;
				case StylePropertyID.Custom:
				{
					if (this.m_CustomProperties == null)
					{
						this.m_CustomProperties = new Dictionary<string, CustomProperty>();
					}
					CustomProperty value = default(CustomProperty);
					if (!this.m_CustomProperties.TryGetValue(styleProperty.name, out value) || specificity >= value.specificity)
					{
						value.handles = values;
						value.data = registry;
						value.specificity = specificity;
						this.m_CustomProperties[styleProperty.name] = value;
					}
					break;
				}
				default:
					goto IL_CF4;
				}
				i++;
				continue;
				IL_CF4:
				throw new ArgumentException(string.Format("Non exhaustive switch statement (value={0})", stylePropertyID));
			}
		}

		// Token: 0x060008E6 RID: 2278 RVA: 0x0001F466 File Offset: 0x0001D666
		public void ApplyCustomProperty(string propertyName, ref StyleValue<float> target)
		{
			StyleValueType valueType = StyleValueType.Float;
			if (VisualElementStylesData.<>f__mg$cache3D == null)
			{
				VisualElementStylesData.<>f__mg$cache3D = new HandlesApplicatorFunction<float>(StyleSheetApplicator.ApplyFloat);
			}
			this.ApplyCustomProperty<float>(propertyName, ref target, valueType, VisualElementStylesData.<>f__mg$cache3D);
		}

		// Token: 0x060008E7 RID: 2279 RVA: 0x0001F48F File Offset: 0x0001D68F
		public void ApplyCustomProperty(string propertyName, ref StyleValue<int> target)
		{
			StyleValueType valueType = StyleValueType.Float;
			if (VisualElementStylesData.<>f__mg$cache3E == null)
			{
				VisualElementStylesData.<>f__mg$cache3E = new HandlesApplicatorFunction<int>(StyleSheetApplicator.ApplyInt);
			}
			this.ApplyCustomProperty<int>(propertyName, ref target, valueType, VisualElementStylesData.<>f__mg$cache3E);
		}

		// Token: 0x060008E8 RID: 2280 RVA: 0x0001F4B8 File Offset: 0x0001D6B8
		public void ApplyCustomProperty(string propertyName, ref StyleValue<bool> target)
		{
			StyleValueType valueType = StyleValueType.Keyword;
			if (VisualElementStylesData.<>f__mg$cache3F == null)
			{
				VisualElementStylesData.<>f__mg$cache3F = new HandlesApplicatorFunction<bool>(StyleSheetApplicator.ApplyBool);
			}
			this.ApplyCustomProperty<bool>(propertyName, ref target, valueType, VisualElementStylesData.<>f__mg$cache3F);
		}

		// Token: 0x060008E9 RID: 2281 RVA: 0x0001F4E1 File Offset: 0x0001D6E1
		public void ApplyCustomProperty(string propertyName, ref StyleValue<Color> target)
		{
			StyleValueType valueType = StyleValueType.Color;
			if (VisualElementStylesData.<>f__mg$cache40 == null)
			{
				VisualElementStylesData.<>f__mg$cache40 = new HandlesApplicatorFunction<Color>(StyleSheetApplicator.ApplyColor);
			}
			this.ApplyCustomProperty<Color>(propertyName, ref target, valueType, VisualElementStylesData.<>f__mg$cache40);
		}

		// Token: 0x060008EA RID: 2282 RVA: 0x0001F50C File Offset: 0x0001D70C
		public void ApplyCustomProperty(string propertyName, ref StyleValue<Texture2D> target)
		{
			StyleValue<Texture2D> other = default(StyleValue<Texture2D>);
			CustomProperty customProperty;
			if (this.m_CustomProperties != null && this.m_CustomProperties.TryGetValue(propertyName, out customProperty))
			{
				StyleSheet data = customProperty.data;
				StyleValueHandle[] handles = customProperty.handles;
				int specificity = customProperty.specificity;
				if (VisualElementStylesData.<>f__mg$cache41 == null)
				{
					VisualElementStylesData.<>f__mg$cache41 = new HandlesApplicatorFunction<Texture2D>(StyleSheetApplicator.ApplyImage);
				}
				data.Apply(handles, specificity, ref other, VisualElementStylesData.<>f__mg$cache41);
			}
			target.Apply(other, StylePropertyApplyMode.CopyIfNotInline);
		}

		// Token: 0x060008EB RID: 2283 RVA: 0x0001F584 File Offset: 0x0001D784
		public void ApplyCustomProperty(string propertyName, ref StyleValue<string> target)
		{
			StyleValue<string> other = new StyleValue<string>(string.Empty);
			CustomProperty customProperty;
			if (this.m_CustomProperties != null && this.m_CustomProperties.TryGetValue(propertyName, out customProperty))
			{
				other.value = customProperty.data.ReadAsString(customProperty.handles[0]);
				other.specificity = customProperty.specificity;
			}
			target.Apply(other, StylePropertyApplyMode.CopyIfNotInline);
		}

		// Token: 0x060008EC RID: 2284 RVA: 0x0001F5FC File Offset: 0x0001D7FC
		internal void ApplyCustomProperty<T>(string propertyName, ref StyleValue<T> target, StyleValueType valueType, HandlesApplicatorFunction<T> applicatorFunc)
		{
			StyleValue<T> other = default(StyleValue<T>);
			CustomProperty customProperty;
			if (this.m_CustomProperties != null && this.m_CustomProperties.TryGetValue(propertyName, out customProperty))
			{
				StyleValueHandle styleValueHandle = customProperty.handles[0];
				if (styleValueHandle.valueType == valueType)
				{
					customProperty.data.Apply(customProperty.handles, customProperty.specificity, ref other, applicatorFunc);
				}
				else
				{
					Debug.LogWarning(string.Format("Trying to read value as {0} while parsed type is {1}", valueType, styleValueHandle.valueType));
				}
			}
			target.Apply(other, StylePropertyApplyMode.CopyIfNotInline);
		}

		// Token: 0x060008ED RID: 2285 RVA: 0x0001F6A3 File Offset: 0x0001D8A3
		// Note: this type is marked as 'beforefieldinit'.
		static VisualElementStylesData()
		{
		}

		// Token: 0x04000387 RID: 903
		public static VisualElementStylesData none = new VisualElementStylesData(true);

		// Token: 0x04000388 RID: 904
		internal readonly bool isShared;

		// Token: 0x04000389 RID: 905
		internal YogaNode yogaNode;

		// Token: 0x0400038A RID: 906
		internal Dictionary<string, CustomProperty> m_CustomProperties;

		// Token: 0x0400038B RID: 907
		internal StyleValue<float> width;

		// Token: 0x0400038C RID: 908
		internal StyleValue<float> height;

		// Token: 0x0400038D RID: 909
		internal StyleValue<float> maxWidth;

		// Token: 0x0400038E RID: 910
		internal StyleValue<float> maxHeight;

		// Token: 0x0400038F RID: 911
		internal StyleValue<float> minWidth;

		// Token: 0x04000390 RID: 912
		internal StyleValue<float> minHeight;

		// Token: 0x04000391 RID: 913
		internal StyleValue<FloatOrKeyword> flexBasis;

		// Token: 0x04000392 RID: 914
		internal StyleValue<float> flexShrink;

		// Token: 0x04000393 RID: 915
		internal StyleValue<float> flexGrow;

		// Token: 0x04000394 RID: 916
		internal StyleValue<int> overflow;

		// Token: 0x04000395 RID: 917
		internal StyleValue<float> positionLeft;

		// Token: 0x04000396 RID: 918
		internal StyleValue<float> positionTop;

		// Token: 0x04000397 RID: 919
		internal StyleValue<float> positionRight;

		// Token: 0x04000398 RID: 920
		internal StyleValue<float> positionBottom;

		// Token: 0x04000399 RID: 921
		internal StyleValue<float> marginLeft;

		// Token: 0x0400039A RID: 922
		internal StyleValue<float> marginTop;

		// Token: 0x0400039B RID: 923
		internal StyleValue<float> marginRight;

		// Token: 0x0400039C RID: 924
		internal StyleValue<float> marginBottom;

		// Token: 0x0400039D RID: 925
		internal StyleValue<float> paddingLeft;

		// Token: 0x0400039E RID: 926
		internal StyleValue<float> paddingTop;

		// Token: 0x0400039F RID: 927
		internal StyleValue<float> paddingRight;

		// Token: 0x040003A0 RID: 928
		internal StyleValue<float> paddingBottom;

		// Token: 0x040003A1 RID: 929
		internal StyleValue<int> positionType;

		// Token: 0x040003A2 RID: 930
		internal StyleValue<int> alignSelf;

		// Token: 0x040003A3 RID: 931
		internal StyleValue<int> unityTextAlign;

		// Token: 0x040003A4 RID: 932
		internal StyleValue<int> fontStyleAndWeight;

		// Token: 0x040003A5 RID: 933
		internal StyleValue<int> textClipping;

		// Token: 0x040003A6 RID: 934
		internal StyleValue<Font> font;

		// Token: 0x040003A7 RID: 935
		internal StyleValue<int> fontSize;

		// Token: 0x040003A8 RID: 936
		internal StyleValue<bool> wordWrap;

		// Token: 0x040003A9 RID: 937
		internal StyleValue<Color> color;

		// Token: 0x040003AA RID: 938
		internal StyleValue<int> flexDirection;

		// Token: 0x040003AB RID: 939
		internal StyleValue<Color> backgroundColor;

		// Token: 0x040003AC RID: 940
		internal StyleValue<Color> borderColor;

		// Token: 0x040003AD RID: 941
		internal StyleValue<Texture2D> backgroundImage;

		// Token: 0x040003AE RID: 942
		internal StyleValue<int> backgroundScaleMode;

		// Token: 0x040003AF RID: 943
		internal StyleValue<int> alignItems;

		// Token: 0x040003B0 RID: 944
		internal StyleValue<int> alignContent;

		// Token: 0x040003B1 RID: 945
		internal StyleValue<int> justifyContent;

		// Token: 0x040003B2 RID: 946
		internal StyleValue<int> flexWrap;

		// Token: 0x040003B3 RID: 947
		internal StyleValue<float> borderLeftWidth;

		// Token: 0x040003B4 RID: 948
		internal StyleValue<float> borderTopWidth;

		// Token: 0x040003B5 RID: 949
		internal StyleValue<float> borderRightWidth;

		// Token: 0x040003B6 RID: 950
		internal StyleValue<float> borderBottomWidth;

		// Token: 0x040003B7 RID: 951
		internal StyleValue<float> borderTopLeftRadius;

		// Token: 0x040003B8 RID: 952
		internal StyleValue<float> borderTopRightRadius;

		// Token: 0x040003B9 RID: 953
		internal StyleValue<float> borderBottomRightRadius;

		// Token: 0x040003BA RID: 954
		internal StyleValue<float> borderBottomLeftRadius;

		// Token: 0x040003BB RID: 955
		internal StyleValue<int> sliceLeft;

		// Token: 0x040003BC RID: 956
		internal StyleValue<int> sliceTop;

		// Token: 0x040003BD RID: 957
		internal StyleValue<int> sliceRight;

		// Token: 0x040003BE RID: 958
		internal StyleValue<int> sliceBottom;

		// Token: 0x040003BF RID: 959
		internal StyleValue<float> opacity;

		// Token: 0x040003C0 RID: 960
		internal StyleValue<CursorStyle> cursor;

		// Token: 0x040003C1 RID: 961
		internal StyleValue<int> visibility;

		// Token: 0x040003C2 RID: 962
		internal const Align DefaultAlignContent = Align.FlexStart;

		// Token: 0x040003C3 RID: 963
		internal const Align DefaultAlignItems = Align.Stretch;

		// Token: 0x040003C4 RID: 964
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache0;

		// Token: 0x040003C5 RID: 965
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache1;

		// Token: 0x040003C6 RID: 966
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache2;

		// Token: 0x040003C7 RID: 967
		[CompilerGenerated]
		private static HandlesApplicatorFunction<Texture2D> <>f__mg$cache3;

		// Token: 0x040003C8 RID: 968
		[CompilerGenerated]
		private static ShorthandApplicatorFunction <>f__mg$cache4;

		// Token: 0x040003C9 RID: 969
		[CompilerGenerated]
		private static HandlesApplicatorFunction<FloatOrKeyword> <>f__mg$cache5;

		// Token: 0x040003CA RID: 970
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache6;

		// Token: 0x040003CB RID: 971
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache7;

		// Token: 0x040003CC RID: 972
		[CompilerGenerated]
		private static HandlesApplicatorFunction<Font> <>f__mg$cache8;

		// Token: 0x040003CD RID: 973
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache9;

		// Token: 0x040003CE RID: 974
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cacheA;

		// Token: 0x040003CF RID: 975
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cacheB;

		// Token: 0x040003D0 RID: 976
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cacheC;

		// Token: 0x040003D1 RID: 977
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cacheD;

		// Token: 0x040003D2 RID: 978
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cacheE;

		// Token: 0x040003D3 RID: 979
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cacheF;

		// Token: 0x040003D4 RID: 980
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache10;

		// Token: 0x040003D5 RID: 981
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache11;

		// Token: 0x040003D6 RID: 982
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache12;

		// Token: 0x040003D7 RID: 983
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache13;

		// Token: 0x040003D8 RID: 984
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache14;

		// Token: 0x040003D9 RID: 985
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache15;

		// Token: 0x040003DA RID: 986
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache16;

		// Token: 0x040003DB RID: 987
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache17;

		// Token: 0x040003DC RID: 988
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache18;

		// Token: 0x040003DD RID: 989
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache19;

		// Token: 0x040003DE RID: 990
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache1A;

		// Token: 0x040003DF RID: 991
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache1B;

		// Token: 0x040003E0 RID: 992
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache1C;

		// Token: 0x040003E1 RID: 993
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache1D;

		// Token: 0x040003E2 RID: 994
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache1E;

		// Token: 0x040003E3 RID: 995
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache1F;

		// Token: 0x040003E4 RID: 996
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache20;

		// Token: 0x040003E5 RID: 997
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache21;

		// Token: 0x040003E6 RID: 998
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache22;

		// Token: 0x040003E7 RID: 999
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache23;

		// Token: 0x040003E8 RID: 1000
		[CompilerGenerated]
		private static HandlesApplicatorFunction<Color> <>f__mg$cache24;

		// Token: 0x040003E9 RID: 1001
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache25;

		// Token: 0x040003EA RID: 1002
		[CompilerGenerated]
		private static HandlesApplicatorFunction<bool> <>f__mg$cache26;

		// Token: 0x040003EB RID: 1003
		[CompilerGenerated]
		private static HandlesApplicatorFunction<Color> <>f__mg$cache27;

		// Token: 0x040003EC RID: 1004
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache28;

		// Token: 0x040003ED RID: 1005
		[CompilerGenerated]
		private static HandlesApplicatorFunction<Color> <>f__mg$cache29;

		// Token: 0x040003EE RID: 1006
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache2A;

		// Token: 0x040003EF RID: 1007
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache2B;

		// Token: 0x040003F0 RID: 1008
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache2C;

		// Token: 0x040003F1 RID: 1009
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache2D;

		// Token: 0x040003F2 RID: 1010
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache2E;

		// Token: 0x040003F3 RID: 1011
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache2F;

		// Token: 0x040003F4 RID: 1012
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache30;

		// Token: 0x040003F5 RID: 1013
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache31;

		// Token: 0x040003F6 RID: 1014
		[CompilerGenerated]
		private static HandlesApplicatorFunction<CursorStyle> <>f__mg$cache32;

		// Token: 0x040003F7 RID: 1015
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache33;

		// Token: 0x040003F8 RID: 1016
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache34;

		// Token: 0x040003F9 RID: 1017
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache35;

		// Token: 0x040003FA RID: 1018
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache36;

		// Token: 0x040003FB RID: 1019
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache37;

		// Token: 0x040003FC RID: 1020
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache38;

		// Token: 0x040003FD RID: 1021
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache39;

		// Token: 0x040003FE RID: 1022
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache3A;

		// Token: 0x040003FF RID: 1023
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache3B;

		// Token: 0x04000400 RID: 1024
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache3C;

		// Token: 0x04000401 RID: 1025
		[CompilerGenerated]
		private static HandlesApplicatorFunction<float> <>f__mg$cache3D;

		// Token: 0x04000402 RID: 1026
		[CompilerGenerated]
		private static HandlesApplicatorFunction<int> <>f__mg$cache3E;

		// Token: 0x04000403 RID: 1027
		[CompilerGenerated]
		private static HandlesApplicatorFunction<bool> <>f__mg$cache3F;

		// Token: 0x04000404 RID: 1028
		[CompilerGenerated]
		private static HandlesApplicatorFunction<Color> <>f__mg$cache40;

		// Token: 0x04000405 RID: 1029
		[CompilerGenerated]
		private static HandlesApplicatorFunction<Texture2D> <>f__mg$cache41;
	}
}
