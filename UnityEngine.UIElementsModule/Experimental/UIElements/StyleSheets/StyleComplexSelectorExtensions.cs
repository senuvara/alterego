﻿using System;
using System.Collections.Generic;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000146 RID: 326
	internal static class StyleComplexSelectorExtensions
	{
		// Token: 0x06000887 RID: 2183 RVA: 0x0001BF5C File Offset: 0x0001A15C
		public static void CachePseudoStateMasks(this StyleComplexSelector complexSelector)
		{
			if (complexSelector.selectors[0].pseudoStateMask == -1)
			{
				if (StyleComplexSelectorExtensions.s_PseudoStates == null)
				{
					StyleComplexSelectorExtensions.s_PseudoStates = new Dictionary<string, StyleComplexSelectorExtensions.PseudoStateData>();
					StyleComplexSelectorExtensions.s_PseudoStates["active"] = new StyleComplexSelectorExtensions.PseudoStateData(PseudoStates.Active, false);
					StyleComplexSelectorExtensions.s_PseudoStates["hover"] = new StyleComplexSelectorExtensions.PseudoStateData(PseudoStates.Hover, false);
					StyleComplexSelectorExtensions.s_PseudoStates["checked"] = new StyleComplexSelectorExtensions.PseudoStateData(PseudoStates.Checked, false);
					StyleComplexSelectorExtensions.s_PseudoStates["selected"] = new StyleComplexSelectorExtensions.PseudoStateData(PseudoStates.Selected, false);
					StyleComplexSelectorExtensions.s_PseudoStates["disabled"] = new StyleComplexSelectorExtensions.PseudoStateData(PseudoStates.Disabled, false);
					StyleComplexSelectorExtensions.s_PseudoStates["focus"] = new StyleComplexSelectorExtensions.PseudoStateData(PseudoStates.Focus, false);
					StyleComplexSelectorExtensions.s_PseudoStates["inactive"] = new StyleComplexSelectorExtensions.PseudoStateData(PseudoStates.Active, true);
					StyleComplexSelectorExtensions.s_PseudoStates["enabled"] = new StyleComplexSelectorExtensions.PseudoStateData(PseudoStates.Disabled, true);
				}
				int i = 0;
				int num = complexSelector.selectors.Length;
				while (i < num)
				{
					StyleSelector styleSelector = complexSelector.selectors[i];
					StyleSelectorPart[] parts = styleSelector.parts;
					PseudoStates pseudoStates = (PseudoStates)0;
					PseudoStates pseudoStates2 = (PseudoStates)0;
					for (int j = 0; j < styleSelector.parts.Length; j++)
					{
						if (styleSelector.parts[j].type == StyleSelectorType.PseudoClass)
						{
							StyleComplexSelectorExtensions.PseudoStateData pseudoStateData;
							if (StyleComplexSelectorExtensions.s_PseudoStates.TryGetValue(parts[j].value, out pseudoStateData))
							{
								if (!pseudoStateData.negate)
								{
									pseudoStates |= pseudoStateData.state;
								}
								else
								{
									pseudoStates2 |= pseudoStateData.state;
								}
							}
							else
							{
								Debug.LogWarningFormat("Unknown pseudo class \"{0}\"", new object[]
								{
									parts[j].value
								});
							}
						}
					}
					styleSelector.pseudoStateMask = (int)pseudoStates;
					styleSelector.negatedPseudoStateMask = (int)pseudoStates2;
					i++;
				}
			}
		}

		// Token: 0x0400032A RID: 810
		private static Dictionary<string, StyleComplexSelectorExtensions.PseudoStateData> s_PseudoStates;

		// Token: 0x02000147 RID: 327
		private struct PseudoStateData
		{
			// Token: 0x06000888 RID: 2184 RVA: 0x0001C138 File Offset: 0x0001A338
			public PseudoStateData(PseudoStates state, bool negate)
			{
				this.state = state;
				this.negate = negate;
			}

			// Token: 0x0400032B RID: 811
			public readonly PseudoStates state;

			// Token: 0x0400032C RID: 812
			public readonly bool negate;
		}
	}
}
