﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000148 RID: 328
	internal struct MatchResultInfo
	{
		// Token: 0x06000889 RID: 2185 RVA: 0x0001C149 File Offset: 0x0001A349
		public MatchResultInfo(bool success, PseudoStates triggerPseudoMask, PseudoStates dependencyPseudoMask)
		{
			this.success = success;
			this.triggerPseudoMask = triggerPseudoMask;
			this.dependencyPseudoMask = dependencyPseudoMask;
		}

		// Token: 0x0400032D RID: 813
		public readonly bool success;

		// Token: 0x0400032E RID: 814
		public readonly PseudoStates triggerPseudoMask;

		// Token: 0x0400032F RID: 815
		public readonly PseudoStates dependencyPseudoMask;
	}
}
