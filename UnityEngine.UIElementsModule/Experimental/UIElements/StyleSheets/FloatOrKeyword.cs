﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x0200014B RID: 331
	internal struct FloatOrKeyword
	{
		// Token: 0x06000890 RID: 2192 RVA: 0x0001C5F8 File Offset: 0x0001A7F8
		public FloatOrKeyword(StyleValueKeyword kw)
		{
			this.isKeyword = true;
			this.keyword = kw;
			this.floatValue = 0f;
		}

		// Token: 0x06000891 RID: 2193 RVA: 0x0001C614 File Offset: 0x0001A814
		public FloatOrKeyword(float v)
		{
			this.isKeyword = false;
			this.keyword = StyleValueKeyword.Inherit;
			this.floatValue = v;
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06000892 RID: 2194 RVA: 0x0001C62C File Offset: 0x0001A82C
		// (set) Token: 0x06000893 RID: 2195 RVA: 0x0001C646 File Offset: 0x0001A846
		public bool isKeyword
		{
			[CompilerGenerated]
			get
			{
				return this.<isKeyword>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<isKeyword>k__BackingField = value;
			}
		}

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06000894 RID: 2196 RVA: 0x0001C650 File Offset: 0x0001A850
		// (set) Token: 0x06000895 RID: 2197 RVA: 0x0001C66A File Offset: 0x0001A86A
		public StyleValueKeyword keyword
		{
			[CompilerGenerated]
			get
			{
				return this.<keyword>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<keyword>k__BackingField = value;
			}
		}

		// Token: 0x17000237 RID: 567
		// (get) Token: 0x06000896 RID: 2198 RVA: 0x0001C674 File Offset: 0x0001A874
		// (set) Token: 0x06000897 RID: 2199 RVA: 0x0001C68E File Offset: 0x0001A88E
		public float floatValue
		{
			[CompilerGenerated]
			get
			{
				return this.<floatValue>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<floatValue>k__BackingField = value;
			}
		}

		// Token: 0x04000333 RID: 819
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <isKeyword>k__BackingField;

		// Token: 0x04000334 RID: 820
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private StyleValueKeyword <keyword>k__BackingField;

		// Token: 0x04000335 RID: 821
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private float <floatValue>k__BackingField;
	}
}
