﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000158 RID: 344
	internal static class StyleValueUtils
	{
		// Token: 0x060008D0 RID: 2256 RVA: 0x0001DAB0 File Offset: 0x0001BCB0
		public static bool ApplyAndCompare(ref StyleValue<float> current, StyleValue<float> other)
		{
			float value = current.value;
			return current.Apply(other, StylePropertyApplyMode.CopyIfEqualOrGreaterSpecificity) && value != other.value;
		}

		// Token: 0x060008D1 RID: 2257 RVA: 0x0001DAF0 File Offset: 0x0001BCF0
		public static bool ApplyAndCompare(ref StyleValue<int> current, StyleValue<int> other)
		{
			int value = current.value;
			return current.Apply(other, StylePropertyApplyMode.CopyIfEqualOrGreaterSpecificity) && value != other.value;
		}

		// Token: 0x060008D2 RID: 2258 RVA: 0x0001DB30 File Offset: 0x0001BD30
		public static bool ApplyAndCompare(ref StyleValue<bool> current, StyleValue<bool> other)
		{
			bool value = current.value;
			return current.Apply(other, StylePropertyApplyMode.CopyIfEqualOrGreaterSpecificity) && value != other.value;
		}

		// Token: 0x060008D3 RID: 2259 RVA: 0x0001DB70 File Offset: 0x0001BD70
		public static bool ApplyAndCompare(ref StyleValue<Color> current, StyleValue<Color> other)
		{
			Color value = current.value;
			return current.Apply(other, StylePropertyApplyMode.CopyIfEqualOrGreaterSpecificity) && value != other.value;
		}

		// Token: 0x060008D4 RID: 2260 RVA: 0x0001DBB0 File Offset: 0x0001BDB0
		public static bool ApplyAndCompare<T>(ref StyleValue<T> current, StyleValue<T> other) where T : struct, IEquatable<T>
		{
			T value = current.value;
			return current.Apply(other, StylePropertyApplyMode.CopyIfEqualOrGreaterSpecificity) && !value.Equals(other.value);
		}

		// Token: 0x060008D5 RID: 2261 RVA: 0x0001DBF8 File Offset: 0x0001BDF8
		public static bool ApplyAndCompareObject<T>(ref StyleValue<T> current, StyleValue<T> other) where T : class
		{
			T value = current.value;
			return current.Apply(other, StylePropertyApplyMode.CopyIfEqualOrGreaterSpecificity) && value != other.value;
		}
	}
}
