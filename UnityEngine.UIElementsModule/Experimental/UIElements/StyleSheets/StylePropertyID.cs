﻿using System;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	// Token: 0x02000155 RID: 341
	internal enum StylePropertyID
	{
		// Token: 0x0400033F RID: 831
		Unknown = -1,
		// Token: 0x04000340 RID: 832
		MarginLeft,
		// Token: 0x04000341 RID: 833
		MarginTop,
		// Token: 0x04000342 RID: 834
		MarginRight,
		// Token: 0x04000343 RID: 835
		MarginBottom,
		// Token: 0x04000344 RID: 836
		PaddingLeft,
		// Token: 0x04000345 RID: 837
		PaddingTop,
		// Token: 0x04000346 RID: 838
		PaddingRight,
		// Token: 0x04000347 RID: 839
		PaddingBottom,
		// Token: 0x04000348 RID: 840
		Position,
		// Token: 0x04000349 RID: 841
		PositionType,
		// Token: 0x0400034A RID: 842
		PositionLeft,
		// Token: 0x0400034B RID: 843
		PositionTop,
		// Token: 0x0400034C RID: 844
		PositionRight,
		// Token: 0x0400034D RID: 845
		PositionBottom,
		// Token: 0x0400034E RID: 846
		Width,
		// Token: 0x0400034F RID: 847
		Height,
		// Token: 0x04000350 RID: 848
		MinWidth,
		// Token: 0x04000351 RID: 849
		MinHeight,
		// Token: 0x04000352 RID: 850
		MaxWidth,
		// Token: 0x04000353 RID: 851
		MaxHeight,
		// Token: 0x04000354 RID: 852
		FlexBasis,
		// Token: 0x04000355 RID: 853
		FlexGrow,
		// Token: 0x04000356 RID: 854
		FlexShrink,
		// Token: 0x04000357 RID: 855
		BorderLeftWidth,
		// Token: 0x04000358 RID: 856
		BorderTopWidth,
		// Token: 0x04000359 RID: 857
		BorderRightWidth,
		// Token: 0x0400035A RID: 858
		BorderBottomWidth,
		// Token: 0x0400035B RID: 859
		BorderTopLeftRadius,
		// Token: 0x0400035C RID: 860
		BorderTopRightRadius,
		// Token: 0x0400035D RID: 861
		BorderBottomRightRadius,
		// Token: 0x0400035E RID: 862
		BorderBottomLeftRadius,
		// Token: 0x0400035F RID: 863
		FlexDirection,
		// Token: 0x04000360 RID: 864
		FlexWrap,
		// Token: 0x04000361 RID: 865
		JustifyContent,
		// Token: 0x04000362 RID: 866
		AlignContent,
		// Token: 0x04000363 RID: 867
		AlignSelf,
		// Token: 0x04000364 RID: 868
		AlignItems,
		// Token: 0x04000365 RID: 869
		UnityTextAlign,
		// Token: 0x04000366 RID: 870
		TextClipping,
		// Token: 0x04000367 RID: 871
		Font,
		// Token: 0x04000368 RID: 872
		FontSize,
		// Token: 0x04000369 RID: 873
		FontStyleAndWeight,
		// Token: 0x0400036A RID: 874
		BackgroundScaleMode,
		// Token: 0x0400036B RID: 875
		Cursor,
		// Token: 0x0400036C RID: 876
		Visibility,
		// Token: 0x0400036D RID: 877
		WordWrap,
		// Token: 0x0400036E RID: 878
		BackgroundImage,
		// Token: 0x0400036F RID: 879
		Color,
		// Token: 0x04000370 RID: 880
		BackgroundColor,
		// Token: 0x04000371 RID: 881
		BorderColor,
		// Token: 0x04000372 RID: 882
		Overflow,
		// Token: 0x04000373 RID: 883
		SliceLeft,
		// Token: 0x04000374 RID: 884
		SliceTop,
		// Token: 0x04000375 RID: 885
		SliceRight,
		// Token: 0x04000376 RID: 886
		SliceBottom,
		// Token: 0x04000377 RID: 887
		Opacity,
		// Token: 0x04000378 RID: 888
		BorderRadius,
		// Token: 0x04000379 RID: 889
		Flex,
		// Token: 0x0400037A RID: 890
		Margin,
		// Token: 0x0400037B RID: 891
		Padding,
		// Token: 0x0400037C RID: 892
		Custom
	}
}
