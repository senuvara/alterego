﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000CE RID: 206
	public class VisualElementFocusChangeDirection : FocusChangeDirection
	{
		// Token: 0x06000615 RID: 1557 RVA: 0x00014B55 File Offset: 0x00012D55
		protected VisualElementFocusChangeDirection(int value) : base(value)
		{
		}

		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x06000616 RID: 1558 RVA: 0x00014B60 File Offset: 0x00012D60
		public static FocusChangeDirection left
		{
			get
			{
				return VisualElementFocusChangeDirection.s_Left;
			}
		}

		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x06000617 RID: 1559 RVA: 0x00014B7C File Offset: 0x00012D7C
		public static FocusChangeDirection right
		{
			get
			{
				return VisualElementFocusChangeDirection.s_Right;
			}
		}

		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x06000618 RID: 1560 RVA: 0x00014B98 File Offset: 0x00012D98
		protected new static VisualElementFocusChangeDirection lastValue
		{
			get
			{
				return VisualElementFocusChangeDirection.s_Right;
			}
		}

		// Token: 0x06000619 RID: 1561 RVA: 0x00014BB2 File Offset: 0x00012DB2
		// Note: this type is marked as 'beforefieldinit'.
		static VisualElementFocusChangeDirection()
		{
		}

		// Token: 0x0400025C RID: 604
		private static readonly VisualElementFocusChangeDirection s_Left = new VisualElementFocusChangeDirection(FocusChangeDirection.lastValue + 1);

		// Token: 0x0400025D RID: 605
		private static readonly VisualElementFocusChangeDirection s_Right = new VisualElementFocusChangeDirection(FocusChangeDirection.lastValue + 2);
	}
}
