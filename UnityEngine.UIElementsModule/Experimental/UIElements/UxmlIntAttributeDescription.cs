﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000163 RID: 355
	public class UxmlIntAttributeDescription : UxmlAttributeDescription
	{
		// Token: 0x0600091A RID: 2330 RVA: 0x0001FCAC File Offset: 0x0001DEAC
		public UxmlIntAttributeDescription()
		{
			base.type = "int";
			base.typeNamespace = "http://www.w3.org/2001/XMLSchema";
			this.defaultValue = 0;
		}

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x0600091B RID: 2331 RVA: 0x0001FCD4 File Offset: 0x0001DED4
		// (set) Token: 0x0600091C RID: 2332 RVA: 0x0001FCEE File Offset: 0x0001DEEE
		public int defaultValue
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultValue>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultValue>k__BackingField = value;
			}
		}

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x0600091D RID: 2333 RVA: 0x0001FCF8 File Offset: 0x0001DEF8
		public override string defaultValueAsString
		{
			get
			{
				return this.defaultValue.ToString();
			}
		}

		// Token: 0x0600091E RID: 2334 RVA: 0x0001FD24 File Offset: 0x0001DF24
		[Obsolete("Pass a creation context to the method.")]
		public int GetValueFromBag(IUxmlAttributes bag)
		{
			return this.GetValueFromBag(bag, default(CreationContext));
		}

		// Token: 0x0600091F RID: 2335 RVA: 0x0001FD4C File Offset: 0x0001DF4C
		public int GetValueFromBag(IUxmlAttributes bag, CreationContext cc)
		{
			if (UxmlIntAttributeDescription.<>f__mg$cache0 == null)
			{
				UxmlIntAttributeDescription.<>f__mg$cache0 = new Func<string, int, int>(UxmlIntAttributeDescription.ConvertValueToInt);
			}
			return base.GetValueFromBag<int>(bag, cc, UxmlIntAttributeDescription.<>f__mg$cache0, this.defaultValue);
		}

		// Token: 0x06000920 RID: 2336 RVA: 0x0001FD8C File Offset: 0x0001DF8C
		private static int ConvertValueToInt(string v, int defaultValue)
		{
			int num;
			int result;
			if (v == null || !int.TryParse(v, out num))
			{
				result = defaultValue;
			}
			else
			{
				result = num;
			}
			return result;
		}

		// Token: 0x0400041A RID: 1050
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <defaultValue>k__BackingField;

		// Token: 0x0400041B RID: 1051
		[CompilerGenerated]
		private static Func<string, int, int> <>f__mg$cache0;
	}
}
