﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000024 RID: 36
	public enum EventPropagation
	{
		// Token: 0x04000049 RID: 73
		Continue,
		// Token: 0x0400004A RID: 74
		Stop
	}
}
