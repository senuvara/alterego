﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000D8 RID: 216
	internal enum HierarchyChangeType
	{
		// Token: 0x0400026F RID: 623
		Add,
		// Token: 0x04000270 RID: 624
		Remove,
		// Token: 0x04000271 RID: 625
		Move
	}
}
