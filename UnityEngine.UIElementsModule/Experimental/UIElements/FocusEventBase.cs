﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000126 RID: 294
	public abstract class FocusEventBase<T> : EventBase<T>, IFocusEvent, IPropagatableEvent where T : FocusEventBase<T>, new()
	{
		// Token: 0x060007FC RID: 2044 RVA: 0x00019AB0 File Offset: 0x00017CB0
		protected FocusEventBase()
		{
			this.Init();
		}

		// Token: 0x170001FF RID: 511
		// (get) Token: 0x060007FD RID: 2045 RVA: 0x0001B234 File Offset: 0x00019434
		// (set) Token: 0x060007FE RID: 2046 RVA: 0x0001B24E File Offset: 0x0001944E
		public Focusable relatedTarget
		{
			[CompilerGenerated]
			get
			{
				return this.<relatedTarget>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<relatedTarget>k__BackingField = value;
			}
		}

		// Token: 0x17000200 RID: 512
		// (get) Token: 0x060007FF RID: 2047 RVA: 0x0001B258 File Offset: 0x00019458
		// (set) Token: 0x06000800 RID: 2048 RVA: 0x0001B272 File Offset: 0x00019472
		public FocusChangeDirection direction
		{
			[CompilerGenerated]
			get
			{
				return this.<direction>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<direction>k__BackingField = value;
			}
		}

		// Token: 0x06000801 RID: 2049 RVA: 0x0001B27B File Offset: 0x0001947B
		protected override void Init()
		{
			base.Init();
			base.flags = EventBase.EventFlags.TricklesDown;
			this.relatedTarget = null;
			this.direction = FocusChangeDirection.unspecified;
			this.m_FocusController = null;
		}

		// Token: 0x06000802 RID: 2050 RVA: 0x0001B2A4 File Offset: 0x000194A4
		public static T GetPooled(IEventHandler target, Focusable relatedTarget, FocusChangeDirection direction, FocusController focusController)
		{
			T pooled = EventBase<T>.GetPooled();
			pooled.target = target;
			pooled.relatedTarget = relatedTarget;
			pooled.direction = direction;
			pooled.m_FocusController = focusController;
			return pooled;
		}

		// Token: 0x04000311 RID: 785
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Focusable <relatedTarget>k__BackingField;

		// Token: 0x04000312 RID: 786
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private FocusChangeDirection <direction>k__BackingField;

		// Token: 0x04000313 RID: 787
		protected FocusController m_FocusController;
	}
}
