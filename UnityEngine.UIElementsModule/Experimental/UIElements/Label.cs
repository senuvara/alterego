﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000047 RID: 71
	public class Label : TextElement
	{
		// Token: 0x060001E5 RID: 485 RVA: 0x0000731B File Offset: 0x0000551B
		public Label() : this(string.Empty)
		{
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x00007329 File Offset: 0x00005529
		public Label(string text)
		{
			this.text = text;
		}

		// Token: 0x02000048 RID: 72
		public new class UxmlFactory : UxmlFactory<Label, Label.UxmlTraits>
		{
			// Token: 0x060001E7 RID: 487 RVA: 0x00007339 File Offset: 0x00005539
			public UxmlFactory()
			{
			}
		}

		// Token: 0x02000049 RID: 73
		public new class UxmlTraits : TextElement.UxmlTraits
		{
			// Token: 0x060001E8 RID: 488 RVA: 0x00002DE3 File Offset: 0x00000FE3
			public UxmlTraits()
			{
			}
		}
	}
}
