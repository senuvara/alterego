﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000060 RID: 96
	// (Invoke) Token: 0x060002A6 RID: 678
	internal delegate Object LoadResourceFunction(string pathName, Type type);
}
