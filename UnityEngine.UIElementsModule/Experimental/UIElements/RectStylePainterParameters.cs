﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000098 RID: 152
	internal struct RectStylePainterParameters
	{
		// Token: 0x060003CA RID: 970 RVA: 0x0000D0B8 File Offset: 0x0000B2B8
		public static RectStylePainterParameters GetDefault(VisualElement ve)
		{
			IStyle style = ve.style;
			RectStylePainterParameters result = new RectStylePainterParameters
			{
				rect = GUIUtility.AlignRectToDevice(ve.rect),
				color = style.backgroundColor
			};
			BorderParameters.SetFromStyle(ref result.border, style);
			return result;
		}

		// Token: 0x040001B2 RID: 434
		public Rect rect;

		// Token: 0x040001B3 RID: 435
		public Color color;

		// Token: 0x040001B4 RID: 436
		public BorderParameters border;
	}
}
