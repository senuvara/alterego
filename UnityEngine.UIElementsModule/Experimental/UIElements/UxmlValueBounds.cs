﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000176 RID: 374
	public class UxmlValueBounds : UxmlTypeRestriction
	{
		// Token: 0x0600099C RID: 2460 RVA: 0x00021229 File Offset: 0x0001F429
		public UxmlValueBounds()
		{
		}

		// Token: 0x0600099D RID: 2461 RVA: 0x00021270 File Offset: 0x0001F470
		public override bool Equals(UxmlTypeRestriction other)
		{
			UxmlValueBounds uxmlValueBounds = other as UxmlValueBounds;
			return uxmlValueBounds != null && (this.min == uxmlValueBounds.min && this.max == uxmlValueBounds.max && this.excludeMin == uxmlValueBounds.excludeMin) && this.excludeMax == uxmlValueBounds.excludeMax;
		}

		// Token: 0x0400044A RID: 1098
		public string min;

		// Token: 0x0400044B RID: 1099
		public string max;

		// Token: 0x0400044C RID: 1100
		public bool excludeMin;

		// Token: 0x0400044D RID: 1101
		public bool excludeMax;
	}
}
