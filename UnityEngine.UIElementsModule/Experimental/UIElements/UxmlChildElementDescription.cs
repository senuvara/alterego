﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000168 RID: 360
	public class UxmlChildElementDescription
	{
		// Token: 0x0600093D RID: 2365 RVA: 0x000202E6 File Offset: 0x0001E4E6
		public UxmlChildElementDescription(Type t)
		{
			this.elementName = t.Name;
			this.elementNamespace = t.Namespace;
		}

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x0600093E RID: 2366 RVA: 0x00020308 File Offset: 0x0001E508
		// (set) Token: 0x0600093F RID: 2367 RVA: 0x00020322 File Offset: 0x0001E522
		public string elementName
		{
			[CompilerGenerated]
			get
			{
				return this.<elementName>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<elementName>k__BackingField = value;
			}
		}

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x06000940 RID: 2368 RVA: 0x0002032C File Offset: 0x0001E52C
		// (set) Token: 0x06000941 RID: 2369 RVA: 0x00020346 File Offset: 0x0001E546
		public string elementNamespace
		{
			[CompilerGenerated]
			get
			{
				return this.<elementNamespace>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<elementNamespace>k__BackingField = value;
			}
		}

		// Token: 0x04000424 RID: 1060
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <elementName>k__BackingField;

		// Token: 0x04000425 RID: 1061
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <elementNamespace>k__BackingField;
	}
}
