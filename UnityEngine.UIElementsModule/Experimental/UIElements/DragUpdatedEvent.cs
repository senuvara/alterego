﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000110 RID: 272
	public class DragUpdatedEvent : DragAndDropEventBase<DragUpdatedEvent>
	{
		// Token: 0x06000765 RID: 1893 RVA: 0x00019D27 File Offset: 0x00017F27
		public DragUpdatedEvent()
		{
		}
	}
}
