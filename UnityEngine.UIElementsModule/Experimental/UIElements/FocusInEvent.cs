﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000129 RID: 297
	public class FocusInEvent : FocusEventBase<FocusInEvent>
	{
		// Token: 0x06000807 RID: 2055 RVA: 0x0001B331 File Offset: 0x00019531
		public FocusInEvent()
		{
			this.Init();
		}

		// Token: 0x06000808 RID: 2056 RVA: 0x0001B340 File Offset: 0x00019540
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown);
		}
	}
}
