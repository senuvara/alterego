﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000119 RID: 281
	internal abstract class EventCallbackFunctorBase
	{
		// Token: 0x060007A8 RID: 1960 RVA: 0x0001A452 File Offset: 0x00018652
		protected EventCallbackFunctorBase(CallbackPhase phase)
		{
			this.phase = phase;
		}

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x060007A9 RID: 1961 RVA: 0x0001A464 File Offset: 0x00018664
		// (set) Token: 0x060007AA RID: 1962 RVA: 0x0001A47E File Offset: 0x0001867E
		public CallbackPhase phase
		{
			[CompilerGenerated]
			get
			{
				return this.<phase>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<phase>k__BackingField = value;
			}
		}

		// Token: 0x060007AB RID: 1963
		public abstract void Invoke(EventBase evt);

		// Token: 0x060007AC RID: 1964
		public abstract bool IsEquivalentTo(long eventTypeId, Delegate callback, CallbackPhase phase);

		// Token: 0x060007AD RID: 1965 RVA: 0x0001A488 File Offset: 0x00018688
		protected bool PhaseMatches(EventBase evt)
		{
			CallbackPhase phase = this.phase;
			if (phase != CallbackPhase.TrickleDownAndTarget)
			{
				if (phase == CallbackPhase.TargetAndBubbleUp)
				{
					if (evt.propagationPhase != PropagationPhase.AtTarget && evt.propagationPhase != PropagationPhase.BubbleUp)
					{
						return false;
					}
				}
			}
			else if (evt.propagationPhase != PropagationPhase.TrickleDown && evt.propagationPhase != PropagationPhase.AtTarget)
			{
				return false;
			}
			return true;
		}

		// Token: 0x040002F7 RID: 759
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private CallbackPhase <phase>k__BackingField;
	}
}
