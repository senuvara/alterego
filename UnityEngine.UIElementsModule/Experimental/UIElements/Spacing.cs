﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200008A RID: 138
	public struct Spacing
	{
		// Token: 0x060003B3 RID: 947 RVA: 0x0000CD61 File Offset: 0x0000AF61
		public Spacing(float left, float top, float right, float bottom)
		{
			this.left = left;
			this.top = top;
			this.right = right;
			this.bottom = bottom;
		}

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x060003B4 RID: 948 RVA: 0x0000CD84 File Offset: 0x0000AF84
		public float horizontal
		{
			get
			{
				return this.left + this.right;
			}
		}

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x060003B5 RID: 949 RVA: 0x0000CDA8 File Offset: 0x0000AFA8
		public float vertical
		{
			get
			{
				return this.top + this.bottom;
			}
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x0000CDCC File Offset: 0x0000AFCC
		public static Rect operator +(Rect r, Spacing a)
		{
			r.x -= a.left;
			r.y -= a.top;
			r.width += a.horizontal;
			r.height += a.vertical;
			return r;
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x0000CE38 File Offset: 0x0000B038
		public static Rect operator -(Rect r, Spacing a)
		{
			r.x += a.left;
			r.y += a.top;
			r.width -= a.horizontal;
			r.height -= a.vertical;
			return r;
		}

		// Token: 0x04000175 RID: 373
		public float left;

		// Token: 0x04000176 RID: 374
		public float top;

		// Token: 0x04000177 RID: 375
		public float right;

		// Token: 0x04000178 RID: 376
		public float bottom;
	}
}
