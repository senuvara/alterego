﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000018 RID: 24
	public abstract class ContextualMenuManager
	{
		// Token: 0x06000076 RID: 118 RVA: 0x00002223 File Offset: 0x00000423
		protected ContextualMenuManager()
		{
		}

		// Token: 0x06000077 RID: 119
		public abstract void DisplayMenuIfEventMatches(EventBase evt, IEventHandler eventHandler);

		// Token: 0x06000078 RID: 120 RVA: 0x000036AC File Offset: 0x000018AC
		public void DisplayMenu(EventBase triggerEvent, IEventHandler target)
		{
			DropdownMenu menu = new DropdownMenu();
			using (ContextualMenuPopulateEvent pooled = ContextualMenuPopulateEvent.GetPooled(triggerEvent, menu, target, this))
			{
				target.SendEvent(pooled);
			}
		}

		// Token: 0x06000079 RID: 121
		protected internal abstract void DoDisplayMenu(DropdownMenu menu, EventBase triggerEvent);
	}
}
