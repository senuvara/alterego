﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000026 RID: 38
	internal enum DispatchMode
	{
		// Token: 0x04000053 RID: 83
		Default = 1,
		// Token: 0x04000054 RID: 84
		Queued = 1,
		// Token: 0x04000055 RID: 85
		Immediate
	}
}
