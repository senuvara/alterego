﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000169 RID: 361
	public abstract class UxmlTraits
	{
		// Token: 0x06000942 RID: 2370 RVA: 0x0002034F File Offset: 0x0001E54F
		protected UxmlTraits()
		{
			this.canHaveAnyAttribute = true;
		}

		// Token: 0x17000254 RID: 596
		// (get) Token: 0x06000943 RID: 2371 RVA: 0x00020360 File Offset: 0x0001E560
		// (set) Token: 0x06000944 RID: 2372 RVA: 0x0002037A File Offset: 0x0001E57A
		public bool canHaveAnyAttribute
		{
			[CompilerGenerated]
			get
			{
				return this.<canHaveAnyAttribute>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<canHaveAnyAttribute>k__BackingField = value;
			}
		}

		// Token: 0x17000255 RID: 597
		// (get) Token: 0x06000945 RID: 2373 RVA: 0x00020384 File Offset: 0x0001E584
		public virtual IEnumerable<UxmlAttributeDescription> uxmlAttributesDescription
		{
			get
			{
				foreach (UxmlAttributeDescription attributeDescription in this.GetAllAttributeDescriptionForType(base.GetType()))
				{
					yield return attributeDescription;
				}
				yield break;
			}
		}

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x06000946 RID: 2374 RVA: 0x000203B0 File Offset: 0x0001E5B0
		public virtual IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
		{
			get
			{
				yield break;
			}
		}

		// Token: 0x06000947 RID: 2375 RVA: 0x000035A6 File Offset: 0x000017A6
		public virtual void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
		{
		}

		// Token: 0x06000948 RID: 2376 RVA: 0x000203D4 File Offset: 0x0001E5D4
		private IEnumerable<UxmlAttributeDescription> GetAllAttributeDescriptionForType(Type t)
		{
			Type baseType = t.BaseType;
			if (baseType != null)
			{
				foreach (UxmlAttributeDescription ident in this.GetAllAttributeDescriptionForType(baseType))
				{
					yield return ident;
				}
			}
			foreach (FieldInfo fieldInfo in from f in t.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
			where typeof(UxmlAttributeDescription).IsAssignableFrom(f.FieldType)
			select f)
			{
				yield return (UxmlAttributeDescription)fieldInfo.GetValue(this);
			}
			yield break;
		}

		// Token: 0x04000426 RID: 1062
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <canHaveAnyAttribute>k__BackingField;

		// Token: 0x0200016A RID: 362
		[CompilerGenerated]
		private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlAttributeDescription>, IEnumerator, IDisposable, IEnumerator<UxmlAttributeDescription>
		{
			// Token: 0x06000949 RID: 2377 RVA: 0x00002223 File Offset: 0x00000423
			[DebuggerHidden]
			public <>c__Iterator0()
			{
			}

			// Token: 0x0600094A RID: 2378 RVA: 0x00020408 File Offset: 0x0001E608
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					enumerator = base.GetAllAttributeDescriptionForType(base.GetType()).GetEnumerator();
					num = 4294967293U;
					break;
				case 1U:
					break;
				default:
					return false;
				}
				try
				{
					switch (num)
					{
					}
					if (enumerator.MoveNext())
					{
						attributeDescription = enumerator.Current;
						this.$current = attributeDescription;
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						flag = true;
						return true;
					}
				}
				finally
				{
					if (!flag)
					{
						if (enumerator != null)
						{
							enumerator.Dispose();
						}
					}
				}
				this.$PC = -1;
				return false;
			}

			// Token: 0x17000257 RID: 599
			// (get) Token: 0x0600094B RID: 2379 RVA: 0x000204F0 File Offset: 0x0001E6F0
			UxmlAttributeDescription IEnumerator<UxmlAttributeDescription>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000258 RID: 600
			// (get) Token: 0x0600094C RID: 2380 RVA: 0x0002050C File Offset: 0x0001E70C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600094D RID: 2381 RVA: 0x00020528 File Offset: 0x0001E728
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 1U:
					try
					{
					}
					finally
					{
						if (enumerator != null)
						{
							enumerator.Dispose();
						}
					}
					break;
				}
			}

			// Token: 0x0600094E RID: 2382 RVA: 0x00002280 File Offset: 0x00000480
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600094F RID: 2383 RVA: 0x0002058C File Offset: 0x0001E78C
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlAttributeDescription>.GetEnumerator();
			}

			// Token: 0x06000950 RID: 2384 RVA: 0x000205A8 File Offset: 0x0001E7A8
			[DebuggerHidden]
			IEnumerator<UxmlAttributeDescription> IEnumerable<UxmlAttributeDescription>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				UxmlTraits.<>c__Iterator0 <>c__Iterator = new UxmlTraits.<>c__Iterator0();
				<>c__Iterator.$this = this;
				return <>c__Iterator;
			}

			// Token: 0x04000427 RID: 1063
			internal IEnumerator<UxmlAttributeDescription> $locvar0;

			// Token: 0x04000428 RID: 1064
			internal UxmlAttributeDescription <attributeDescription>__1;

			// Token: 0x04000429 RID: 1065
			internal UxmlTraits $this;

			// Token: 0x0400042A RID: 1066
			internal UxmlAttributeDescription $current;

			// Token: 0x0400042B RID: 1067
			internal bool $disposing;

			// Token: 0x0400042C RID: 1068
			internal int $PC;
		}

		// Token: 0x0200016B RID: 363
		[CompilerGenerated]
		private sealed class <>c__Iterator1 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
		{
			// Token: 0x06000951 RID: 2385 RVA: 0x00002223 File Offset: 0x00000423
			[DebuggerHidden]
			public <>c__Iterator1()
			{
			}

			// Token: 0x06000952 RID: 2386 RVA: 0x000205DC File Offset: 0x0001E7DC
			public bool MoveNext()
			{
				bool flag = this.$PC != 0;
				this.$PC = -1;
				if (!flag)
				{
				}
				return false;
			}

			// Token: 0x17000259 RID: 601
			// (get) Token: 0x06000953 RID: 2387 RVA: 0x000205F8 File Offset: 0x0001E7F8
			UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700025A RID: 602
			// (get) Token: 0x06000954 RID: 2388 RVA: 0x00020614 File Offset: 0x0001E814
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000955 RID: 2389 RVA: 0x0000227E File Offset: 0x0000047E
			[DebuggerHidden]
			public void Dispose()
			{
			}

			// Token: 0x06000956 RID: 2390 RVA: 0x00002280 File Offset: 0x00000480
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000957 RID: 2391 RVA: 0x00020630 File Offset: 0x0001E830
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
			}

			// Token: 0x06000958 RID: 2392 RVA: 0x0002064A File Offset: 0x0001E84A
			[DebuggerHidden]
			IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				return new UxmlTraits.<>c__Iterator1();
			}

			// Token: 0x0400042D RID: 1069
			internal UxmlChildElementDescription $current;

			// Token: 0x0400042E RID: 1070
			internal bool $disposing;

			// Token: 0x0400042F RID: 1071
			internal int $PC;
		}

		// Token: 0x0200016C RID: 364
		[CompilerGenerated]
		private sealed class <GetAllAttributeDescriptionForType>c__Iterator2 : IEnumerable, IEnumerable<UxmlAttributeDescription>, IEnumerator, IDisposable, IEnumerator<UxmlAttributeDescription>
		{
			// Token: 0x06000959 RID: 2393 RVA: 0x00002223 File Offset: 0x00000423
			[DebuggerHidden]
			public <GetAllAttributeDescriptionForType>c__Iterator2()
			{
			}

			// Token: 0x0600095A RID: 2394 RVA: 0x00020668 File Offset: 0x0001E868
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					baseType = t.BaseType;
					if (baseType == null)
					{
						goto IL_DC;
					}
					enumerator = base.GetAllAttributeDescriptionForType(baseType).GetEnumerator();
					num = 4294967293U;
					break;
				case 1U:
					break;
				case 2U:
					Block_5:
					try
					{
						switch (num)
						{
						}
						if (enumerator2.MoveNext())
						{
							fieldInfo = enumerator2.Current;
							this.$current = (UxmlAttributeDescription)fieldInfo.GetValue(this);
							if (!this.$disposing)
							{
								this.$PC = 2;
							}
							flag = true;
							return true;
						}
					}
					finally
					{
						if (!flag)
						{
							if (enumerator2 != null)
							{
								enumerator2.Dispose();
							}
						}
					}
					this.$PC = -1;
					return false;
				default:
					return false;
				}
				try
				{
					switch (num)
					{
					}
					if (enumerator.MoveNext())
					{
						ident = enumerator.Current;
						this.$current = ident;
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						flag = true;
						return true;
					}
				}
				finally
				{
					if (!flag)
					{
						if (enumerator != null)
						{
							enumerator.Dispose();
						}
					}
				}
				IL_DC:
				enumerator2 = (from f in t.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
				where typeof(UxmlAttributeDescription).IsAssignableFrom(f.FieldType)
				select f).GetEnumerator();
				num = 4294967293U;
				goto Block_5;
			}

			// Token: 0x1700025B RID: 603
			// (get) Token: 0x0600095B RID: 2395 RVA: 0x0002083C File Offset: 0x0001EA3C
			UxmlAttributeDescription IEnumerator<UxmlAttributeDescription>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700025C RID: 604
			// (get) Token: 0x0600095C RID: 2396 RVA: 0x00020858 File Offset: 0x0001EA58
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600095D RID: 2397 RVA: 0x00020874 File Offset: 0x0001EA74
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 1U:
					try
					{
					}
					finally
					{
						if (enumerator != null)
						{
							enumerator.Dispose();
						}
					}
					break;
				case 2U:
					try
					{
					}
					finally
					{
						if (enumerator2 != null)
						{
							enumerator2.Dispose();
						}
					}
					break;
				}
			}

			// Token: 0x0600095E RID: 2398 RVA: 0x00002280 File Offset: 0x00000480
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600095F RID: 2399 RVA: 0x00020908 File Offset: 0x0001EB08
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlAttributeDescription>.GetEnumerator();
			}

			// Token: 0x06000960 RID: 2400 RVA: 0x00020924 File Offset: 0x0001EB24
			[DebuggerHidden]
			IEnumerator<UxmlAttributeDescription> IEnumerable<UxmlAttributeDescription>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				UxmlTraits.<GetAllAttributeDescriptionForType>c__Iterator2 <GetAllAttributeDescriptionForType>c__Iterator = new UxmlTraits.<GetAllAttributeDescriptionForType>c__Iterator2();
				<GetAllAttributeDescriptionForType>c__Iterator.$this = this;
				<GetAllAttributeDescriptionForType>c__Iterator.t = t;
				return <GetAllAttributeDescriptionForType>c__Iterator;
			}

			// Token: 0x06000961 RID: 2401 RVA: 0x00020964 File Offset: 0x0001EB64
			private static bool <>m__0(FieldInfo f)
			{
				return typeof(UxmlAttributeDescription).IsAssignableFrom(f.FieldType);
			}

			// Token: 0x04000430 RID: 1072
			internal Type t;

			// Token: 0x04000431 RID: 1073
			internal Type <baseType>__0;

			// Token: 0x04000432 RID: 1074
			internal IEnumerator<UxmlAttributeDescription> $locvar0;

			// Token: 0x04000433 RID: 1075
			internal UxmlAttributeDescription <ident>__1;

			// Token: 0x04000434 RID: 1076
			internal IEnumerator<FieldInfo> $locvar1;

			// Token: 0x04000435 RID: 1077
			internal FieldInfo <fieldInfo>__2;

			// Token: 0x04000436 RID: 1078
			internal UxmlTraits $this;

			// Token: 0x04000437 RID: 1079
			internal UxmlAttributeDescription $current;

			// Token: 0x04000438 RID: 1080
			internal bool $disposing;

			// Token: 0x04000439 RID: 1081
			internal int $PC;

			// Token: 0x0400043A RID: 1082
			private static Func<FieldInfo, bool> <>f__am$cache0;
		}
	}
}
