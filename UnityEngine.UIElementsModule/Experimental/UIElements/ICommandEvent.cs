﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000107 RID: 263
	public interface ICommandEvent
	{
		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x06000755 RID: 1877
		string commandName { get; }
	}
}
