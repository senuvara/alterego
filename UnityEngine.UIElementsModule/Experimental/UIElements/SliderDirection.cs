﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000005 RID: 5
	public enum SliderDirection
	{
		// Token: 0x04000006 RID: 6
		Horizontal,
		// Token: 0x04000007 RID: 7
		Vertical
	}
}
