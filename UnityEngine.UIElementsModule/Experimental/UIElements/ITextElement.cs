﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000A0 RID: 160
	internal interface ITextElement
	{
		// Token: 0x1700010E RID: 270
		// (get) Token: 0x060003E3 RID: 995
		// (set) Token: 0x060003E4 RID: 996
		string text { get; set; }
	}
}
