﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000086 RID: 134
	public class SliderInt : BaseSlider<int>
	{
		// Token: 0x0600039F RID: 927 RVA: 0x0000C9ED File Offset: 0x0000ABED
		public SliderInt() : this(0, 10, SliderDirection.Horizontal, 0f)
		{
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x0000C9FF File Offset: 0x0000ABFF
		public SliderInt(int start, int end, SliderDirection direction = SliderDirection.Horizontal, float pageSize = 0f) : base(start, end, direction, pageSize)
		{
		}

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x060003A1 RID: 929 RVA: 0x0000CA10 File Offset: 0x0000AC10
		// (set) Token: 0x060003A2 RID: 930 RVA: 0x0000CA2B File Offset: 0x0000AC2B
		public override float pageSize
		{
			get
			{
				return base.pageSize;
			}
			set
			{
				base.pageSize = (float)Mathf.RoundToInt(value);
			}
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x0000CA3C File Offset: 0x0000AC3C
		internal override int SliderLerpUnclamped(int a, int b, float interpolant)
		{
			return Mathf.RoundToInt(Mathf.LerpUnclamped((float)a, (float)b, interpolant));
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x0000CA60 File Offset: 0x0000AC60
		internal override float SliderNormalizeValue(int currentValue, int lowerValue, int higherValue)
		{
			return ((float)currentValue - (float)lowerValue) / ((float)higherValue - (float)lowerValue);
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x0000CA80 File Offset: 0x0000AC80
		internal override int SliderRange()
		{
			return Math.Abs(base.highValue - base.lowValue);
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x0000CAA8 File Offset: 0x0000ACA8
		internal override void ComputeValueAndDirectionFromClick(float sliderLength, float dragElementLength, float dragElementPos, float dragElementLastPos)
		{
			if (Mathf.Approximately(this.pageSize, 0f))
			{
				base.ComputeValueAndDirectionFromClick(sliderLength, dragElementLength, dragElementPos, dragElementLastPos);
			}
			else
			{
				float f = sliderLength - dragElementLength;
				if (Mathf.Abs(f) >= Mathf.Epsilon)
				{
					int num = (int)this.pageSize;
					if (base.lowValue > base.highValue)
					{
						num = -num;
					}
					if (dragElementLastPos < dragElementPos && base.clampedDragger.dragDirection != ClampedDragger<int>.DragDirection.LowToHigh)
					{
						base.clampedDragger.dragDirection = ClampedDragger<int>.DragDirection.HighToLow;
						this.value -= num;
					}
					else if (dragElementLastPos > dragElementPos + dragElementLength && base.clampedDragger.dragDirection != ClampedDragger<int>.DragDirection.HighToLow)
					{
						base.clampedDragger.dragDirection = ClampedDragger<int>.DragDirection.LowToHigh;
						this.value += num;
					}
				}
			}
		}

		// Token: 0x0400016C RID: 364
		internal const int kDefaultHighValue = 10;

		// Token: 0x02000087 RID: 135
		public new class UxmlFactory : UxmlFactory<SliderInt, SliderInt.UxmlTraits>
		{
			// Token: 0x060003A7 RID: 935 RVA: 0x0000CB87 File Offset: 0x0000AD87
			public UxmlFactory()
			{
			}
		}

		// Token: 0x02000088 RID: 136
		public new class UxmlTraits : BaseField<int>.UxmlTraits
		{
			// Token: 0x060003A8 RID: 936 RVA: 0x0000CB90 File Offset: 0x0000AD90
			public UxmlTraits()
			{
			}

			// Token: 0x17000103 RID: 259
			// (get) Token: 0x060003A9 RID: 937 RVA: 0x0000CC34 File Offset: 0x0000AE34
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield break;
				}
			}

			// Token: 0x060003AA RID: 938 RVA: 0x0000CC58 File Offset: 0x0000AE58
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				SliderInt sliderInt = (SliderInt)ve;
				sliderInt.lowValue = this.m_LowValue.GetValueFromBag(bag, cc);
				sliderInt.highValue = this.m_HighValue.GetValueFromBag(bag, cc);
				sliderInt.direction = this.m_Direction.GetValueFromBag(bag, cc);
				sliderInt.pageSize = (float)this.m_PageSize.GetValueFromBag(bag, cc);
				sliderInt.SetValueWithoutNotify(this.m_Value.GetValueFromBag(bag, cc));
			}

			// Token: 0x0400016D RID: 365
			private UxmlIntAttributeDescription m_LowValue = new UxmlIntAttributeDescription
			{
				name = "low-value"
			};

			// Token: 0x0400016E RID: 366
			private UxmlIntAttributeDescription m_HighValue = new UxmlIntAttributeDescription
			{
				name = "high-value",
				defaultValue = 10
			};

			// Token: 0x0400016F RID: 367
			private UxmlIntAttributeDescription m_PageSize = new UxmlIntAttributeDescription
			{
				name = "page-size",
				defaultValue = 0
			};

			// Token: 0x04000170 RID: 368
			private UxmlEnumAttributeDescription<SliderDirection> m_Direction = new UxmlEnumAttributeDescription<SliderDirection>
			{
				name = "direction",
				defaultValue = SliderDirection.Vertical
			};

			// Token: 0x04000171 RID: 369
			private UxmlIntAttributeDescription m_Value = new UxmlIntAttributeDescription
			{
				name = "value"
			};

			// Token: 0x02000089 RID: 137
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x060003AB RID: 939 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x060003AC RID: 940 RVA: 0x0000CCD6 File Offset: 0x0000AED6
				public bool MoveNext()
				{
					bool flag = this.$PC != 0;
					this.$PC = -1;
					if (!flag)
					{
					}
					return false;
				}

				// Token: 0x17000104 RID: 260
				// (get) Token: 0x060003AD RID: 941 RVA: 0x0000CCF4 File Offset: 0x0000AEF4
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x17000105 RID: 261
				// (get) Token: 0x060003AE RID: 942 RVA: 0x0000CD10 File Offset: 0x0000AF10
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x060003AF RID: 943 RVA: 0x0000227E File Offset: 0x0000047E
				[DebuggerHidden]
				public void Dispose()
				{
				}

				// Token: 0x060003B0 RID: 944 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x060003B1 RID: 945 RVA: 0x0000CD2C File Offset: 0x0000AF2C
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x060003B2 RID: 946 RVA: 0x0000CD46 File Offset: 0x0000AF46
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new SliderInt.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x04000172 RID: 370
				internal UxmlChildElementDescription $current;

				// Token: 0x04000173 RID: 371
				internal bool $disposing;

				// Token: 0x04000174 RID: 372
				internal int $PC;
			}
		}
	}
}
