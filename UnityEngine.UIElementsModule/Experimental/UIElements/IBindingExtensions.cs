﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000EF RID: 239
	public static class IBindingExtensions
	{
		// Token: 0x060006B8 RID: 1720 RVA: 0x00017298 File Offset: 0x00015498
		public static bool IsBound(this IBindable control)
		{
			return ((control != null) ? control.binding : null) != null;
		}
	}
}
