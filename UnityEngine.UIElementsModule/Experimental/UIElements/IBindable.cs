﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000ED RID: 237
	public interface IBindable
	{
		// Token: 0x170001BD RID: 445
		// (get) Token: 0x060006B1 RID: 1713
		// (set) Token: 0x060006B2 RID: 1714
		IBinding binding { get; set; }

		// Token: 0x170001BE RID: 446
		// (get) Token: 0x060006B3 RID: 1715
		// (set) Token: 0x060006B4 RID: 1716
		string bindingPath { get; set; }
	}
}
