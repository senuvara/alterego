﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000012 RID: 18
	[Obsolete("ContextualMenu has been deprecated. Use DropdownMenu instead.", true)]
	public class ContextualMenu
	{
		// Token: 0x0600005D RID: 93 RVA: 0x00002223 File Offset: 0x00000423
		public ContextualMenu()
		{
		}

		// Token: 0x0600005E RID: 94 RVA: 0x0000358C File Offset: 0x0000178C
		public List<ContextualMenu.MenuItem> MenuItems()
		{
			return new List<ContextualMenu.MenuItem>();
		}

		// Token: 0x0600005F RID: 95 RVA: 0x000035A6 File Offset: 0x000017A6
		public void AppendAction(string actionName, Action<ContextualMenu.MenuAction> action, Func<ContextualMenu.MenuAction, ContextualMenu.MenuAction.StatusFlags> actionStatusCallback, object userData = null)
		{
		}

		// Token: 0x06000060 RID: 96 RVA: 0x000035A6 File Offset: 0x000017A6
		public void InsertAction(int atIndex, string actionName, Action<ContextualMenu.MenuAction> action, Func<ContextualMenu.MenuAction, ContextualMenu.MenuAction.StatusFlags> actionStatusCallback, object userData = null)
		{
		}

		// Token: 0x06000061 RID: 97 RVA: 0x000035A6 File Offset: 0x000017A6
		public void AppendSeparator(string subMenuPath = null)
		{
		}

		// Token: 0x06000062 RID: 98 RVA: 0x000035A6 File Offset: 0x000017A6
		public void InsertSeparator(string subMenuPath, int atIndex)
		{
		}

		// Token: 0x06000063 RID: 99 RVA: 0x000035A6 File Offset: 0x000017A6
		public void RemoveItemAt(int index)
		{
		}

		// Token: 0x06000064 RID: 100 RVA: 0x000035A6 File Offset: 0x000017A6
		public void PrepareForDisplay(EventBase e)
		{
		}

		// Token: 0x02000013 RID: 19
		[Obsolete("ContextualMenu.EventInfo has been deprecated. Use DropdownMenu.EventInfo instead.", true)]
		public class EventInfo
		{
			// Token: 0x06000065 RID: 101 RVA: 0x000035A9 File Offset: 0x000017A9
			public EventInfo(EventBase e)
			{
			}

			// Token: 0x17000014 RID: 20
			// (get) Token: 0x06000066 RID: 102 RVA: 0x000035B4 File Offset: 0x000017B4
			public EventModifiers modifiers
			{
				[CompilerGenerated]
				get
				{
					return this.<modifiers>k__BackingField;
				}
			}

			// Token: 0x17000015 RID: 21
			// (get) Token: 0x06000067 RID: 103 RVA: 0x000035D0 File Offset: 0x000017D0
			public Vector2 mousePosition
			{
				[CompilerGenerated]
				get
				{
					return this.<mousePosition>k__BackingField;
				}
			}

			// Token: 0x17000016 RID: 22
			// (get) Token: 0x06000068 RID: 104 RVA: 0x000035EC File Offset: 0x000017EC
			public Vector2 localMousePosition
			{
				[CompilerGenerated]
				get
				{
					return this.<localMousePosition>k__BackingField;
				}
			}

			// Token: 0x04000024 RID: 36
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private readonly EventModifiers <modifiers>k__BackingField;

			// Token: 0x04000025 RID: 37
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private readonly Vector2 <mousePosition>k__BackingField;

			// Token: 0x04000026 RID: 38
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private readonly Vector2 <localMousePosition>k__BackingField;
		}

		// Token: 0x02000014 RID: 20
		[Obsolete("ContextualMenu.MenuItem has been deprecated. Use DropdownMenu.MenuItem instead.", true)]
		public abstract class MenuItem
		{
			// Token: 0x06000069 RID: 105 RVA: 0x00002223 File Offset: 0x00000423
			protected MenuItem()
			{
			}
		}

		// Token: 0x02000015 RID: 21
		[Obsolete("ContextualMenu.Separator has been deprecated. Use DropdownMenu.Separator instead.", true)]
		public class Separator : ContextualMenu.MenuItem
		{
			// Token: 0x0600006A RID: 106 RVA: 0x00003606 File Offset: 0x00001806
			public Separator(string subMenuPath)
			{
			}

			// Token: 0x04000027 RID: 39
			public string subMenuPath;
		}

		// Token: 0x02000016 RID: 22
		[Obsolete("ContextualMenu.MenuAction has been deprecated. Use DropdownMenu.MenuAction instead.", true)]
		public class MenuAction : ContextualMenu.MenuItem
		{
			// Token: 0x0600006B RID: 107 RVA: 0x00003606 File Offset: 0x00001806
			public MenuAction(string actionName, Action<ContextualMenu.MenuAction> actionCallback, Func<ContextualMenu.MenuAction, ContextualMenu.MenuAction.StatusFlags> actionStatusCallback, object userData = null)
			{
			}

			// Token: 0x17000017 RID: 23
			// (get) Token: 0x0600006C RID: 108 RVA: 0x00003610 File Offset: 0x00001810
			// (set) Token: 0x0600006D RID: 109 RVA: 0x0000362A File Offset: 0x0000182A
			public ContextualMenu.MenuAction.StatusFlags status
			{
				[CompilerGenerated]
				get
				{
					return this.<status>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<status>k__BackingField = value;
				}
			}

			// Token: 0x17000018 RID: 24
			// (get) Token: 0x0600006E RID: 110 RVA: 0x00003634 File Offset: 0x00001834
			// (set) Token: 0x0600006F RID: 111 RVA: 0x0000364E File Offset: 0x0000184E
			public ContextualMenu.EventInfo eventInfo
			{
				[CompilerGenerated]
				get
				{
					return this.<eventInfo>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<eventInfo>k__BackingField = value;
				}
			}

			// Token: 0x17000019 RID: 25
			// (get) Token: 0x06000070 RID: 112 RVA: 0x00003658 File Offset: 0x00001858
			// (set) Token: 0x06000071 RID: 113 RVA: 0x00003672 File Offset: 0x00001872
			public object userData
			{
				[CompilerGenerated]
				get
				{
					return this.<userData>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<userData>k__BackingField = value;
				}
			}

			// Token: 0x06000072 RID: 114 RVA: 0x0000367C File Offset: 0x0000187C
			public static ContextualMenu.MenuAction.StatusFlags AlwaysEnabled(ContextualMenu.MenuAction a)
			{
				return ContextualMenu.MenuAction.StatusFlags.Normal;
			}

			// Token: 0x06000073 RID: 115 RVA: 0x00003694 File Offset: 0x00001894
			public static ContextualMenu.MenuAction.StatusFlags AlwaysDisabled(ContextualMenu.MenuAction a)
			{
				return ContextualMenu.MenuAction.StatusFlags.Disabled;
			}

			// Token: 0x06000074 RID: 116 RVA: 0x000035A6 File Offset: 0x000017A6
			public void UpdateActionStatus(ContextualMenu.EventInfo eventInfo)
			{
			}

			// Token: 0x06000075 RID: 117 RVA: 0x000035A6 File Offset: 0x000017A6
			public void Execute()
			{
			}

			// Token: 0x04000028 RID: 40
			public string name;

			// Token: 0x04000029 RID: 41
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private ContextualMenu.MenuAction.StatusFlags <status>k__BackingField;

			// Token: 0x0400002A RID: 42
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			[CompilerGenerated]
			private ContextualMenu.EventInfo <eventInfo>k__BackingField;

			// Token: 0x0400002B RID: 43
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private object <userData>k__BackingField;

			// Token: 0x02000017 RID: 23
			[Flags]
			[Obsolete("ContextualMenu.MenuAction.StatusFlags has been deprecated. Use DropdownMenu.MenuAction.StatusFlags instead.", true)]
			public enum StatusFlags
			{
				// Token: 0x0400002D RID: 45
				Normal = 0,
				// Token: 0x0400002E RID: 46
				Disabled = 1,
				// Token: 0x0400002F RID: 47
				Checked = 2,
				// Token: 0x04000030 RID: 48
				Hidden = 4
			}
		}
	}
}
