﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000142 RID: 322
	public class AttachToPanelEvent : PanelChangedEventBase<AttachToPanelEvent>
	{
		// Token: 0x0600087B RID: 2171 RVA: 0x0001BE4C File Offset: 0x0001A04C
		public AttachToPanelEvent()
		{
		}
	}
}
