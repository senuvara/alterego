﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000118 RID: 280
	// (Invoke) Token: 0x060007A5 RID: 1957
	public delegate void EventCallback<in TEventType, in TCallbackArgs>(TEventType evt, TCallbackArgs userArgs);
}
