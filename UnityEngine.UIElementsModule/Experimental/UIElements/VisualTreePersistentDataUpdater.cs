﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000DC RID: 220
	internal class VisualTreePersistentDataUpdater : BaseVisualTreeUpdater
	{
		// Token: 0x06000653 RID: 1619 RVA: 0x00015918 File Offset: 0x00013B18
		public VisualTreePersistentDataUpdater()
		{
		}

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x06000654 RID: 1620 RVA: 0x00015944 File Offset: 0x00013B44
		public override string description
		{
			get
			{
				return "Update PersistentData";
			}
		}

		// Token: 0x06000655 RID: 1621 RVA: 0x0001595E File Offset: 0x00013B5E
		public override void OnVersionChanged(VisualElement ve, VersionChangeType versionChangeType)
		{
			if ((versionChangeType & VersionChangeType.PersistentData) == VersionChangeType.PersistentData)
			{
				this.m_Version += 1U;
				this.m_UpdateList.Add(ve);
				this.PropagateToParents(ve);
			}
		}

		// Token: 0x06000656 RID: 1622 RVA: 0x0001599C File Offset: 0x00013B9C
		public override void Update()
		{
			if (this.m_Version != this.m_LastVersion)
			{
				int num = 0;
				while (this.m_LastVersion != this.m_Version)
				{
					this.m_LastVersion = this.m_Version;
					this.ValidatePersistentDataOnSubTree(base.visualTree, true);
					num++;
					if (num > 5)
					{
						Debug.LogError("UIElements: Too many children recursively added that rely on persistent data: " + base.visualTree);
						break;
					}
				}
				this.m_UpdateList.Clear();
				this.m_ParentList.Clear();
			}
		}

		// Token: 0x06000657 RID: 1623 RVA: 0x00015A30 File Offset: 0x00013C30
		private void ValidatePersistentDataOnSubTree(VisualElement ve, bool enablePersistence)
		{
			if (!ve.IsPersitenceSupportedOnChildren())
			{
				enablePersistence = false;
			}
			if (this.m_UpdateList.Contains(ve))
			{
				this.m_UpdateList.Remove(ve);
				ve.OnPersistentDataReady(enablePersistence);
			}
			if (this.m_ParentList.Contains(ve))
			{
				this.m_ParentList.Remove(ve);
				for (int i = 0; i < ve.shadow.childCount; i++)
				{
					this.ValidatePersistentDataOnSubTree(ve.shadow[i], enablePersistence);
				}
			}
		}

		// Token: 0x06000658 RID: 1624 RVA: 0x00015ACC File Offset: 0x00013CCC
		private void PropagateToParents(VisualElement ve)
		{
			for (VisualElement parent = ve.shadow.parent; parent != null; parent = parent.shadow.parent)
			{
				if (!this.m_ParentList.Add(parent))
				{
					break;
				}
			}
		}

		// Token: 0x0400027A RID: 634
		private HashSet<VisualElement> m_UpdateList = new HashSet<VisualElement>();

		// Token: 0x0400027B RID: 635
		private HashSet<VisualElement> m_ParentList = new HashSet<VisualElement>();

		// Token: 0x0400027C RID: 636
		private const int kMaxValidatePersistentDataCount = 5;

		// Token: 0x0400027D RID: 637
		private uint m_Version = 0U;

		// Token: 0x0400027E RID: 638
		private uint m_LastVersion = 0U;
	}
}
