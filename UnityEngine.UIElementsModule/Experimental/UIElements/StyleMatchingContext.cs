﻿using System;
using System.Collections.Generic;
using UnityEngine.Experimental.UIElements.StyleSheets;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000E0 RID: 224
	internal class StyleMatchingContext
	{
		// Token: 0x0600066E RID: 1646 RVA: 0x000168A9 File Offset: 0x00014AA9
		public StyleMatchingContext(Action<VisualElement, MatchResultInfo> processResult)
		{
			this.styleSheetStack = new List<StyleSheet>();
			this.currentElement = null;
			this.processResult = processResult;
		}

		// Token: 0x04000289 RID: 649
		public List<StyleSheet> styleSheetStack;

		// Token: 0x0400028A RID: 650
		public VisualElement currentElement;

		// Token: 0x0400028B RID: 651
		public Action<VisualElement, MatchResultInfo> processResult;
	}
}
