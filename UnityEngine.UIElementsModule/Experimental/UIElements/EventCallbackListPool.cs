﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200011F RID: 287
	internal class EventCallbackListPool
	{
		// Token: 0x060007B6 RID: 1974 RVA: 0x0001A692 File Offset: 0x00018892
		public EventCallbackListPool()
		{
		}

		// Token: 0x060007B7 RID: 1975 RVA: 0x0001A6A8 File Offset: 0x000188A8
		public EventCallbackList Get(EventCallbackList initializer)
		{
			EventCallbackList eventCallbackList;
			if (this.m_Stack.Count == 0)
			{
				if (initializer != null)
				{
					eventCallbackList = new EventCallbackList(initializer);
				}
				else
				{
					eventCallbackList = new EventCallbackList();
				}
			}
			else
			{
				eventCallbackList = this.m_Stack.Pop();
				if (initializer != null)
				{
					eventCallbackList.AddRange(initializer);
				}
			}
			return eventCallbackList;
		}

		// Token: 0x060007B8 RID: 1976 RVA: 0x0001A708 File Offset: 0x00018908
		public void Release(EventCallbackList element)
		{
			element.Clear();
			this.m_Stack.Push(element);
		}

		// Token: 0x04000307 RID: 775
		private readonly Stack<EventCallbackList> m_Stack = new Stack<EventCallbackList>();
	}
}
