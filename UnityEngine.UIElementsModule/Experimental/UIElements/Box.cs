﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200000A RID: 10
	public class Box : VisualElement
	{
		// Token: 0x06000039 RID: 57 RVA: 0x00002CD9 File Offset: 0x00000ED9
		public Box()
		{
		}

		// Token: 0x0200000B RID: 11
		public new class UxmlFactory : UxmlFactory<Box>
		{
			// Token: 0x0600003A RID: 58 RVA: 0x00002DA8 File Offset: 0x00000FA8
			public UxmlFactory()
			{
			}
		}
	}
}
