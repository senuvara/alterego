﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000139 RID: 313
	public class MouseEnterEvent : MouseEventBase<MouseEnterEvent>
	{
		// Token: 0x06000862 RID: 2146 RVA: 0x0001BB91 File Offset: 0x00019D91
		public MouseEnterEvent()
		{
			this.Init();
		}

		// Token: 0x06000863 RID: 2147 RVA: 0x0001BBA0 File Offset: 0x00019DA0
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.TricklesDown | EventBase.EventFlags.Cancellable);
		}
	}
}
