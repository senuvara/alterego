﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000F6 RID: 246
	public class TextField : TextInputFieldBase<string>
	{
		// Token: 0x060006DD RID: 1757 RVA: 0x00017E44 File Offset: 0x00016044
		public TextField() : this(-1, false, false, '\0')
		{
		}

		// Token: 0x060006DE RID: 1758 RVA: 0x00017E51 File Offset: 0x00016051
		public TextField(int maxLength, bool multiline, bool isPasswordField, char maskChar) : base(maxLength, maskChar)
		{
			this.m_Value = "";
			this.multiline = multiline;
			this.isPasswordField = isPasswordField;
		}

		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x060006DF RID: 1759 RVA: 0x00017E78 File Offset: 0x00016078
		// (set) Token: 0x060006E0 RID: 1760 RVA: 0x00017E93 File Offset: 0x00016093
		public bool multiline
		{
			get
			{
				return this.m_Multiline;
			}
			set
			{
				this.m_Multiline = value;
				if (!value)
				{
					base.text = base.text.Replace("\n", "");
				}
			}
		}

		// Token: 0x170001C4 RID: 452
		// (set) Token: 0x060006E1 RID: 1761 RVA: 0x00017EBE File Offset: 0x000160BE
		public override bool isPasswordField
		{
			set
			{
				base.isPasswordField = value;
				if (value)
				{
					this.multiline = false;
				}
			}
		}

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x060006E2 RID: 1762 RVA: 0x00017ED8 File Offset: 0x000160D8
		// (set) Token: 0x060006E3 RID: 1763 RVA: 0x00017EF3 File Offset: 0x000160F3
		public override string value
		{
			get
			{
				return base.value;
			}
			set
			{
				base.value = value;
				base.text = this.m_Value;
			}
		}

		// Token: 0x060006E4 RID: 1764 RVA: 0x00017F09 File Offset: 0x00016109
		public override void SetValueWithoutNotify(string newValue)
		{
			base.SetValueWithoutNotify(newValue);
			base.text = this.m_Value;
		}

		// Token: 0x060006E5 RID: 1765 RVA: 0x00017F1F File Offset: 0x0001611F
		public void SelectRange(int cursorIndex, int selectionIndex)
		{
			if (base.editorEngine != null)
			{
				base.editorEngine.cursorIndex = cursorIndex;
				base.editorEngine.selectIndex = selectionIndex;
			}
		}

		// Token: 0x060006E6 RID: 1766 RVA: 0x00017F48 File Offset: 0x00016148
		public override void OnPersistentDataReady()
		{
			base.OnPersistentDataReady();
			string fullHierarchicalPersistenceKey = base.GetFullHierarchicalPersistenceKey();
			base.OverwriteFromPersistedData(this, fullHierarchicalPersistenceKey);
			base.text = this.m_Value;
		}

		// Token: 0x060006E7 RID: 1767 RVA: 0x00017F77 File Offset: 0x00016177
		internal override void SyncTextEngine()
		{
			base.editorEngine.multiline = this.multiline;
			base.editorEngine.isPasswordField = this.isPasswordField;
			base.SyncTextEngine();
		}

		// Token: 0x060006E8 RID: 1768 RVA: 0x00017FA4 File Offset: 0x000161A4
		protected override void DoRepaint(IStylePainter painter)
		{
			IStylePainterInternal stylePainterInternal = (IStylePainterInternal)painter;
			if (this.isPasswordField)
			{
				string text = "".PadRight(base.text.Length, base.maskChar);
				if (!base.hasFocus)
				{
					if (!string.IsNullOrEmpty(text) && base.contentRect.width > 0f && base.contentRect.height > 0f)
					{
						TextStylePainterParameters @default = TextStylePainterParameters.GetDefault(this, base.text);
						@default.text = text;
						stylePainterInternal.DrawText(@default);
					}
				}
				else
				{
					base.DrawWithTextSelectionAndCursor(stylePainterInternal, text);
				}
			}
			else
			{
				base.DoRepaint(painter);
			}
		}

		// Token: 0x060006E9 RID: 1769 RVA: 0x00018068 File Offset: 0x00016268
		protected internal override void ExecuteDefaultActionAtTarget(EventBase evt)
		{
			base.ExecuteDefaultActionAtTarget(evt);
			if (evt.GetEventTypeId() == EventBase<KeyDownEvent>.TypeId())
			{
				KeyDownEvent keyDownEvent = evt as KeyDownEvent;
				if (!base.isDelayed || keyDownEvent.character == '\n')
				{
					this.value = base.text;
				}
			}
			else if (evt.GetEventTypeId() == EventBase<ExecuteCommandEvent>.TypeId())
			{
				ExecuteCommandEvent executeCommandEvent = evt as ExecuteCommandEvent;
				string commandName = executeCommandEvent.commandName;
				if (!base.isDelayed && (commandName == "Paste" || commandName == "Cut"))
				{
					this.value = base.text;
				}
			}
		}

		// Token: 0x060006EA RID: 1770 RVA: 0x0001811A File Offset: 0x0001631A
		protected internal override void ExecuteDefaultAction(EventBase evt)
		{
			base.ExecuteDefaultAction(evt);
			if (base.isDelayed && evt.GetEventTypeId() == EventBase<BlurEvent>.TypeId())
			{
				this.value = base.text;
			}
		}

		// Token: 0x040002B8 RID: 696
		private bool m_Multiline;

		// Token: 0x020000F7 RID: 247
		public new class UxmlFactory : UxmlFactory<TextField, TextField.UxmlTraits>
		{
			// Token: 0x060006EB RID: 1771 RVA: 0x0001814D File Offset: 0x0001634D
			public UxmlFactory()
			{
			}
		}

		// Token: 0x020000F8 RID: 248
		public new class UxmlTraits : TextInputFieldBase<string>.UxmlTraits
		{
			// Token: 0x060006EC RID: 1772 RVA: 0x00018158 File Offset: 0x00016358
			public UxmlTraits()
			{
			}

			// Token: 0x060006ED RID: 1773 RVA: 0x00018184 File Offset: 0x00016384
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				TextField textField = (TextField)ve;
				textField.multiline = this.m_Multiline.GetValueFromBag(bag, cc);
				textField.SetValueWithoutNotify(textField.text);
			}

			// Token: 0x040002B9 RID: 697
			private UxmlBoolAttributeDescription m_Multiline = new UxmlBoolAttributeDescription
			{
				name = "multiline"
			};
		}
	}
}
