﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000145 RID: 325
	public class IMGUIEvent : EventBase<IMGUIEvent>
	{
		// Token: 0x06000884 RID: 2180 RVA: 0x0001BF16 File Offset: 0x0001A116
		public IMGUIEvent()
		{
			this.Init();
		}

		// Token: 0x06000885 RID: 2181 RVA: 0x0001BF28 File Offset: 0x0001A128
		public static IMGUIEvent GetPooled(Event systemEvent)
		{
			IMGUIEvent pooled = EventBase<IMGUIEvent>.GetPooled();
			pooled.imguiEvent = systemEvent;
			return pooled;
		}

		// Token: 0x06000886 RID: 2182 RVA: 0x0001BF4B File Offset: 0x0001A14B
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown | EventBase.EventFlags.Cancellable);
		}
	}
}
