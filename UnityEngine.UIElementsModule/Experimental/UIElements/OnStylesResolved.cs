﻿using System;
using UnityEngine.Experimental.UIElements.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000BB RID: 187
	// (Invoke) Token: 0x0600049A RID: 1178
	internal delegate void OnStylesResolved(ICustomStyle styles);
}
