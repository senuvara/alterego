﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000051 RID: 81
	public abstract class Manipulator : IManipulator
	{
		// Token: 0x06000220 RID: 544 RVA: 0x00002223 File Offset: 0x00000423
		protected Manipulator()
		{
		}

		// Token: 0x06000221 RID: 545
		protected abstract void RegisterCallbacksOnTarget();

		// Token: 0x06000222 RID: 546
		protected abstract void UnregisterCallbacksFromTarget();

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000223 RID: 547 RVA: 0x00008494 File Offset: 0x00006694
		// (set) Token: 0x06000224 RID: 548 RVA: 0x000084AF File Offset: 0x000066AF
		public VisualElement target
		{
			get
			{
				return this.m_Target;
			}
			set
			{
				if (this.target != null)
				{
					this.UnregisterCallbacksFromTarget();
				}
				this.m_Target = value;
				if (this.target != null)
				{
					this.RegisterCallbacksOnTarget();
				}
			}
		}

		// Token: 0x040000B9 RID: 185
		private VisualElement m_Target;
	}
}
