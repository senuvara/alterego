﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000EE RID: 238
	public interface IBinding
	{
		// Token: 0x060006B5 RID: 1717
		void PreUpdate();

		// Token: 0x060006B6 RID: 1718
		void Update();

		// Token: 0x060006B7 RID: 1719
		void Release();
	}
}
