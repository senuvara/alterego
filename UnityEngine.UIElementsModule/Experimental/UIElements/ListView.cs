﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Assertions;
using UnityEngine.Experimental.UIElements.StyleEnums;
using UnityEngine.Experimental.UIElements.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200004A RID: 74
	public class ListView : VisualElement
	{
		// Token: 0x060001E9 RID: 489 RVA: 0x00007344 File Offset: 0x00005544
		public ListView()
		{
			this.selectionType = SelectionType.Single;
			this.m_ScrollOffset = 0f;
			this.m_ScrollView = new ScrollView();
			this.m_ScrollView.StretchToParentSize();
			this.m_ScrollView.stretchContentWidth = true;
			this.m_ScrollView.verticalScroller.valueChanged += this.OnScroll;
			base.shadow.Add(this.m_ScrollView);
			base.RegisterCallback<GeometryChangedEvent>(new EventCallback<GeometryChangedEvent>(this.OnSizeChanged), TrickleDown.NoTrickleDown);
			this.m_ScrollView.contentContainer.RegisterCallback<MouseDownEvent>(new EventCallback<MouseDownEvent>(this.OnClick), TrickleDown.NoTrickleDown);
			this.m_ScrollView.contentContainer.RegisterCallback<KeyDownEvent>(new EventCallback<KeyDownEvent>(this.OnKeyDown), TrickleDown.NoTrickleDown);
			this.m_ScrollView.contentContainer.focusIndex = 0;
		}

		// Token: 0x060001EA RID: 490 RVA: 0x00007430 File Offset: 0x00005630
		public ListView(IList itemsSource, int itemHeight, Func<VisualElement> makeItem, Action<VisualElement, int> bindItem) : this()
		{
			this.m_ItemsSource = itemsSource;
			this.m_ItemHeight = itemHeight;
			this.m_MakeItem = makeItem;
			this.m_BindItem = bindItem;
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x060001EB RID: 491 RVA: 0x0000745C File Offset: 0x0000565C
		// (remove) Token: 0x060001EC RID: 492 RVA: 0x00007494 File Offset: 0x00005694
		public event Action<object> onItemChosen
		{
			add
			{
				Action<object> action = this.onItemChosen;
				Action<object> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<object>>(ref this.onItemChosen, (Action<object>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<object> action = this.onItemChosen;
				Action<object> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<object>>(ref this.onItemChosen, (Action<object>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x060001ED RID: 493 RVA: 0x000074CC File Offset: 0x000056CC
		// (remove) Token: 0x060001EE RID: 494 RVA: 0x00007504 File Offset: 0x00005704
		public event Action<List<object>> onSelectionChanged
		{
			add
			{
				Action<List<object>> action = this.onSelectionChanged;
				Action<List<object>> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<List<object>>>(ref this.onSelectionChanged, (Action<List<object>>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<List<object>> action = this.onSelectionChanged;
				Action<List<object>> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<List<object>>>(ref this.onSelectionChanged, (Action<List<object>>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x060001EF RID: 495 RVA: 0x0000753C File Offset: 0x0000573C
		// (set) Token: 0x060001F0 RID: 496 RVA: 0x00007557 File Offset: 0x00005757
		public IList itemsSource
		{
			get
			{
				return this.m_ItemsSource;
			}
			set
			{
				this.m_ItemsSource = value;
				this.Refresh();
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x060001F1 RID: 497 RVA: 0x00007568 File Offset: 0x00005768
		// (set) Token: 0x060001F2 RID: 498 RVA: 0x00007583 File Offset: 0x00005783
		public Func<VisualElement> makeItem
		{
			get
			{
				return this.m_MakeItem;
			}
			set
			{
				if (!(this.m_MakeItem == value))
				{
					this.m_MakeItem = value;
					this.Refresh();
				}
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x060001F3 RID: 499 RVA: 0x000075AC File Offset: 0x000057AC
		// (set) Token: 0x060001F4 RID: 500 RVA: 0x000075C7 File Offset: 0x000057C7
		public Action<VisualElement, int> bindItem
		{
			get
			{
				return this.m_BindItem;
			}
			set
			{
				this.m_BindItem = value;
				this.Refresh();
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060001F5 RID: 501 RVA: 0x000075D8 File Offset: 0x000057D8
		// (set) Token: 0x060001F6 RID: 502 RVA: 0x000075FA File Offset: 0x000057FA
		public int itemHeight
		{
			get
			{
				return this.m_ItemHeight.GetSpecifiedValueOrDefault(30);
			}
			set
			{
				this.m_ItemHeight = value;
				this.Refresh();
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060001F7 RID: 503 RVA: 0x00007610 File Offset: 0x00005810
		// (set) Token: 0x060001F8 RID: 504 RVA: 0x00007646 File Offset: 0x00005846
		public int selectedIndex
		{
			get
			{
				return (this.m_SelectedIndices.Count != 0) ? this.m_SelectedIndices.First<int>() : -1;
			}
			set
			{
				this.SetSelection(value);
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060001F9 RID: 505 RVA: 0x00007650 File Offset: 0x00005850
		public object selectedItem
		{
			get
			{
				return (this.m_ItemsSource != null) ? this.m_ItemsSource[this.selectedIndex] : null;
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060001FA RID: 506 RVA: 0x00007688 File Offset: 0x00005888
		public override VisualElement contentContainer
		{
			get
			{
				return this.m_ScrollView.contentContainer;
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060001FB RID: 507 RVA: 0x000076A8 File Offset: 0x000058A8
		// (set) Token: 0x060001FC RID: 508 RVA: 0x000076C2 File Offset: 0x000058C2
		public SelectionType selectionType
		{
			[CompilerGenerated]
			get
			{
				return this.<selectionType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<selectionType>k__BackingField = value;
			}
		}

		// Token: 0x060001FD RID: 509 RVA: 0x000076CC File Offset: 0x000058CC
		public void OnKeyDown(KeyDownEvent evt)
		{
			if (this.HasValidDataAndBindings())
			{
				switch (evt.keyCode)
				{
				case KeyCode.UpArrow:
					if (this.selectedIndex > 0)
					{
						this.selectedIndex--;
					}
					break;
				case KeyCode.DownArrow:
					if (this.selectedIndex + 1 < this.itemsSource.Count)
					{
						this.selectedIndex++;
					}
					break;
				case KeyCode.Home:
					this.selectedIndex = 0;
					break;
				case KeyCode.End:
					this.selectedIndex = this.itemsSource.Count - 1;
					break;
				case KeyCode.PageUp:
					this.selectedIndex = Math.Max(0, this.selectedIndex - (int)(this.m_LastHeight / (float)this.itemHeight));
					break;
				case KeyCode.PageDown:
					this.selectedIndex = Math.Min(this.itemsSource.Count - 1, this.selectedIndex + (int)(this.m_LastHeight / (float)this.itemHeight));
					break;
				}
				this.ScrollToItem(this.selectedIndex);
			}
		}

		// Token: 0x060001FE RID: 510 RVA: 0x000077FC File Offset: 0x000059FC
		public void ScrollToItem(int index)
		{
			if (!this.HasValidDataAndBindings())
			{
				throw new InvalidOperationException("Can't scroll without valid source, bind method, or factory method.");
			}
			if (this.m_VisibleItemCount != 0)
			{
				if (this.m_FirstVisibleIndex > index)
				{
					this.m_ScrollView.scrollOffset = Vector2.up * (float)this.itemHeight * (float)index;
				}
				else
				{
					int num = (int)(this.m_LastHeight / (float)this.itemHeight);
					if (index >= this.m_FirstVisibleIndex + num)
					{
						bool flag = (int)this.m_LastHeight % this.itemHeight != 0;
						int num2 = index - num;
						if (flag)
						{
							num2++;
						}
						this.m_ScrollView.scrollOffset = Vector2.up * (float)this.itemHeight * (float)num2;
					}
				}
			}
		}

		// Token: 0x060001FF RID: 511 RVA: 0x000078D4 File Offset: 0x00005AD4
		private void OnClick(MouseDownEvent evt)
		{
			if (this.HasValidDataAndBindings())
			{
				if (evt.button == 0)
				{
					int num = (int)(evt.localMousePosition.y / (float)this.itemHeight);
					if (num <= this.itemsSource.Count - 1)
					{
						int clickCount = evt.clickCount;
						if (clickCount != 1)
						{
							if (clickCount == 2)
							{
								if (this.onItemChosen != null)
								{
									this.onItemChosen(this.itemsSource[num]);
								}
							}
						}
						else if (this.selectionType != SelectionType.None)
						{
							if (this.selectionType == SelectionType.Multiple && evt.actionKey)
							{
								if (this.m_SelectedIndices.Contains(num))
								{
									this.RemoveFromSelection(num);
								}
								else
								{
									this.AddToSelection(num);
								}
							}
							else
							{
								this.SetSelection(num);
							}
						}
					}
				}
			}
		}

		// Token: 0x06000200 RID: 512 RVA: 0x000079D8 File Offset: 0x00005BD8
		protected void AddToSelection(int index)
		{
			if (this.HasValidDataAndBindings())
			{
				foreach (ListView.RecycledItem recycledItem in this.m_Pool)
				{
					if (recycledItem.index == index)
					{
						recycledItem.SetSelected(true);
					}
				}
				if (!this.m_SelectedIndices.Contains(index))
				{
					this.m_SelectedIndices.Add(index);
				}
				this.SelectionChanged();
				base.SavePersistentData();
			}
		}

		// Token: 0x06000201 RID: 513 RVA: 0x00007A7C File Offset: 0x00005C7C
		protected void RemoveFromSelection(int index)
		{
			if (this.HasValidDataAndBindings())
			{
				foreach (ListView.RecycledItem recycledItem in this.m_Pool)
				{
					if (recycledItem.index == index)
					{
						recycledItem.SetSelected(false);
					}
				}
				if (this.m_SelectedIndices.Contains(index))
				{
					this.m_SelectedIndices.Remove(index);
				}
				this.SelectionChanged();
				base.SavePersistentData();
			}
		}

		// Token: 0x06000202 RID: 514 RVA: 0x00007B20 File Offset: 0x00005D20
		protected void SetSelection(int index)
		{
			if (this.HasValidDataAndBindings())
			{
				foreach (ListView.RecycledItem recycledItem in this.m_Pool)
				{
					recycledItem.SetSelected(recycledItem.index == index);
				}
				this.m_SelectedIndices.Clear();
				if (index >= 0)
				{
					this.m_SelectedIndices.Add(index);
				}
				this.SelectionChanged();
				base.SavePersistentData();
			}
		}

		// Token: 0x06000203 RID: 515 RVA: 0x00007BC0 File Offset: 0x00005DC0
		private void SelectionChanged()
		{
			if (this.HasValidDataAndBindings())
			{
				if (this.onSelectionChanged != null)
				{
					List<object> list = new List<object>();
					foreach (int index in this.m_SelectedIndices)
					{
						list.Add(this.itemsSource[index]);
					}
					this.onSelectionChanged(list);
				}
			}
		}

		// Token: 0x06000204 RID: 516 RVA: 0x00007C5C File Offset: 0x00005E5C
		protected void ClearSelection()
		{
			if (this.HasValidDataAndBindings())
			{
				foreach (ListView.RecycledItem recycledItem in this.m_Pool)
				{
					recycledItem.SetSelected(false);
				}
				this.m_SelectedIndices.Clear();
				this.SelectionChanged();
			}
		}

		// Token: 0x06000205 RID: 517 RVA: 0x00007CDC File Offset: 0x00005EDC
		public void ScrollTo(VisualElement visualElement)
		{
			this.m_ScrollView.ScrollTo(visualElement);
		}

		// Token: 0x06000206 RID: 518 RVA: 0x00007CEC File Offset: 0x00005EEC
		public override void OnPersistentDataReady()
		{
			base.OnPersistentDataReady();
			string fullHierarchicalPersistenceKey = base.GetFullHierarchicalPersistenceKey();
			base.OverwriteFromPersistedData(this, fullHierarchicalPersistenceKey);
		}

		// Token: 0x06000207 RID: 519 RVA: 0x00007D10 File Offset: 0x00005F10
		private void OnScroll(float offset)
		{
			if (this.HasValidDataAndBindings())
			{
				this.m_ScrollOffset = offset;
				this.m_FirstVisibleIndex = (int)(offset / (float)this.itemHeight);
				this.m_ScrollView.contentContainer.style.height = (float)(this.itemsSource.Count * this.itemHeight);
				int num = 0;
				while (num < this.m_Pool.Count && num + this.m_FirstVisibleIndex < this.itemsSource.Count)
				{
					this.Setup(this.m_Pool[num], num + this.m_FirstVisibleIndex);
					num++;
				}
			}
		}

		// Token: 0x06000208 RID: 520 RVA: 0x00007DC0 File Offset: 0x00005FC0
		private bool HasValidDataAndBindings()
		{
			return this.itemsSource != null && this.makeItem != null && this.bindItem != null;
		}

		// Token: 0x06000209 RID: 521 RVA: 0x00007DFC File Offset: 0x00005FFC
		public void Refresh()
		{
			this.m_Pool.Clear();
			this.m_ScrollView.Clear();
			this.m_VisibleItemCount = 0;
			if (this.HasValidDataAndBindings())
			{
				this.m_LastHeight = this.m_ScrollView.layout.height;
				if (!float.IsNaN(this.m_LastHeight))
				{
					this.ResizeHeight(this.m_LastHeight);
				}
			}
		}

		// Token: 0x0600020A RID: 522 RVA: 0x00007E74 File Offset: 0x00006074
		private void ResizeHeight(float height)
		{
			this.m_ScrollView.contentContainer.style.height = (float)(this.itemsSource.Count * this.itemHeight);
			this.m_ScrollView.verticalScroller.highValue = Mathf.Max(this.m_ScrollOffset, this.m_ScrollView.verticalScroller.highValue);
			this.m_ScrollView.verticalScroller.value = this.m_ScrollOffset;
			int num = Math.Min((int)(height / (float)this.itemHeight) + 2, this.itemsSource.Count);
			if (this.m_VisibleItemCount != num)
			{
				if (this.m_VisibleItemCount > num)
				{
					int num2 = this.m_VisibleItemCount - num;
					for (int i = 0; i < num2; i++)
					{
						this.m_Pool.RemoveAt(this.m_Pool.Count - 1);
						this.m_ScrollView.RemoveAt(this.m_ScrollView.childCount - 1);
					}
				}
				else
				{
					int num3 = num - this.m_VisibleItemCount;
					for (int j = 0; j < num3; j++)
					{
						int num4 = j + this.m_FirstVisibleIndex + this.m_VisibleItemCount;
						VisualElement visualElement = this.makeItem();
						ListView.RecycledItem recycledItem = new ListView.RecycledItem(visualElement);
						this.m_Pool.Add(recycledItem);
						visualElement.style.marginTop = 0f;
						visualElement.style.marginBottom = 0f;
						visualElement.style.positionType = PositionType.Absolute;
						visualElement.style.positionLeft = 0f;
						visualElement.style.positionRight = 0f;
						visualElement.style.height = (float)this.itemHeight;
						if (num4 < this.itemsSource.Count)
						{
							this.Setup(recycledItem, num4);
						}
						else
						{
							visualElement.style.visibility = Visibility.Hidden;
						}
						this.m_ScrollView.Add(visualElement);
					}
				}
				this.m_VisibleItemCount = num;
			}
			this.m_LastHeight = height;
		}

		// Token: 0x0600020B RID: 523 RVA: 0x000080A8 File Offset: 0x000062A8
		private void Setup(ListView.RecycledItem recycledItem, int newIndex)
		{
			Assert.IsTrue(newIndex < this.itemsSource.Count);
			recycledItem.element.style.visibility = Visibility.Visible;
			recycledItem.index = newIndex;
			recycledItem.element.style.positionTop = (float)(recycledItem.index * this.itemHeight);
			recycledItem.element.style.positionBottom = (float)((this.itemsSource.Count - recycledItem.index - 1) * this.itemHeight);
			this.bindItem(recycledItem.element, recycledItem.index);
			recycledItem.SetSelected(this.m_SelectedIndices.Contains(newIndex));
		}

		// Token: 0x0600020C RID: 524 RVA: 0x00008164 File Offset: 0x00006364
		private void OnSizeChanged(GeometryChangedEvent evt)
		{
			if (this.HasValidDataAndBindings())
			{
				if (evt.newRect.height != evt.oldRect.height)
				{
					this.ResizeHeight(evt.newRect.height);
				}
			}
		}

		// Token: 0x0600020D RID: 525 RVA: 0x000081BC File Offset: 0x000063BC
		protected override void OnStyleResolved(ICustomStyle styles)
		{
			base.OnStyleResolved(styles);
			styles.ApplyCustomProperty("-unity-item-height", ref this.m_ItemHeight);
			this.Refresh();
		}

		// Token: 0x0400009F RID: 159
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Action<object> onItemChosen;

		// Token: 0x040000A0 RID: 160
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Action<List<object>> onSelectionChanged;

		// Token: 0x040000A1 RID: 161
		private IList m_ItemsSource;

		// Token: 0x040000A2 RID: 162
		private Func<VisualElement> m_MakeItem;

		// Token: 0x040000A3 RID: 163
		private Action<VisualElement, int> m_BindItem;

		// Token: 0x040000A4 RID: 164
		private StyleValue<int> m_ItemHeight;

		// Token: 0x040000A5 RID: 165
		[SerializeField]
		private float m_ScrollOffset;

		// Token: 0x040000A6 RID: 166
		[SerializeField]
		private List<int> m_SelectedIndices = new List<int>();

		// Token: 0x040000A7 RID: 167
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private SelectionType <selectionType>k__BackingField;

		// Token: 0x040000A8 RID: 168
		private const int k_DefaultItemHeight = 30;

		// Token: 0x040000A9 RID: 169
		private const string k_ItemHeightProperty = "-unity-item-height";

		// Token: 0x040000AA RID: 170
		private int m_FirstVisibleIndex;

		// Token: 0x040000AB RID: 171
		private float m_LastHeight;

		// Token: 0x040000AC RID: 172
		private List<ListView.RecycledItem> m_Pool = new List<ListView.RecycledItem>();

		// Token: 0x040000AD RID: 173
		private ScrollView m_ScrollView;

		// Token: 0x040000AE RID: 174
		private const int m_ExtraVisibleItems = 2;

		// Token: 0x040000AF RID: 175
		private int m_VisibleItemCount;

		// Token: 0x0200004B RID: 75
		public new class UxmlFactory : UxmlFactory<ListView, ListView.UxmlTraits>
		{
			// Token: 0x0600020E RID: 526 RVA: 0x000081DD File Offset: 0x000063DD
			public UxmlFactory()
			{
			}
		}

		// Token: 0x0200004C RID: 76
		public new class UxmlTraits : VisualElement.UxmlTraits
		{
			// Token: 0x0600020F RID: 527 RVA: 0x000081E8 File Offset: 0x000063E8
			public UxmlTraits()
			{
			}

			// Token: 0x17000094 RID: 148
			// (get) Token: 0x06000210 RID: 528 RVA: 0x00008230 File Offset: 0x00006430
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield break;
				}
			}

			// Token: 0x06000211 RID: 529 RVA: 0x00008253 File Offset: 0x00006453
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				((ListView)ve).itemHeight = this.m_ItemHeight.GetValueFromBag(bag, cc);
			}

			// Token: 0x040000B0 RID: 176
			private UxmlIntAttributeDescription m_ItemHeight = new UxmlIntAttributeDescription
			{
				name = "item-height",
				obsoleteNames = new string[]
				{
					"itemHeight"
				},
				defaultValue = 30
			};

			// Token: 0x0200004D RID: 77
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x06000212 RID: 530 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x06000213 RID: 531 RVA: 0x00008277 File Offset: 0x00006477
				public bool MoveNext()
				{
					bool flag = this.$PC != 0;
					this.$PC = -1;
					if (!flag)
					{
					}
					return false;
				}

				// Token: 0x17000095 RID: 149
				// (get) Token: 0x06000214 RID: 532 RVA: 0x00008294 File Offset: 0x00006494
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x17000096 RID: 150
				// (get) Token: 0x06000215 RID: 533 RVA: 0x000082B0 File Offset: 0x000064B0
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x06000216 RID: 534 RVA: 0x0000227E File Offset: 0x0000047E
				[DebuggerHidden]
				public void Dispose()
				{
				}

				// Token: 0x06000217 RID: 535 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x06000218 RID: 536 RVA: 0x000082CC File Offset: 0x000064CC
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x06000219 RID: 537 RVA: 0x000082E6 File Offset: 0x000064E6
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new ListView.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x040000B1 RID: 177
				internal UxmlChildElementDescription $current;

				// Token: 0x040000B2 RID: 178
				internal bool $disposing;

				// Token: 0x040000B3 RID: 179
				internal int $PC;
			}
		}

		// Token: 0x0200004E RID: 78
		private class RecycledItem
		{
			// Token: 0x0600021A RID: 538 RVA: 0x00008301 File Offset: 0x00006501
			public RecycledItem(VisualElement element)
			{
				this.element = element;
			}

			// Token: 0x0600021B RID: 539 RVA: 0x00008311 File Offset: 0x00006511
			internal void SetSelected(bool selected)
			{
				if (selected)
				{
					this.element.pseudoStates |= PseudoStates.Selected;
				}
				else
				{
					this.element.pseudoStates &= ~PseudoStates.Selected;
				}
			}

			// Token: 0x040000B4 RID: 180
			public readonly VisualElement element;

			// Token: 0x040000B5 RID: 181
			public int index;
		}
	}
}
