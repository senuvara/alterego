﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000127 RID: 295
	public class FocusOutEvent : FocusEventBase<FocusOutEvent>
	{
		// Token: 0x06000803 RID: 2051 RVA: 0x0001B2F6 File Offset: 0x000194F6
		public FocusOutEvent()
		{
			this.Init();
		}

		// Token: 0x06000804 RID: 2052 RVA: 0x0001B305 File Offset: 0x00019505
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown);
		}
	}
}
