﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200012D RID: 301
	public abstract class KeyboardEventBase<T> : EventBase<T>, IKeyboardEvent where T : KeyboardEventBase<T>, new()
	{
		// Token: 0x0600081A RID: 2074 RVA: 0x00019AB0 File Offset: 0x00017CB0
		protected KeyboardEventBase()
		{
			this.Init();
		}

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x0600081B RID: 2075 RVA: 0x0001B414 File Offset: 0x00019614
		// (set) Token: 0x0600081C RID: 2076 RVA: 0x0001B42E File Offset: 0x0001962E
		public EventModifiers modifiers
		{
			[CompilerGenerated]
			get
			{
				return this.<modifiers>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<modifiers>k__BackingField = value;
			}
		}

		// Token: 0x1700020C RID: 524
		// (get) Token: 0x0600081D RID: 2077 RVA: 0x0001B438 File Offset: 0x00019638
		// (set) Token: 0x0600081E RID: 2078 RVA: 0x0001B452 File Offset: 0x00019652
		public char character
		{
			[CompilerGenerated]
			get
			{
				return this.<character>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<character>k__BackingField = value;
			}
		}

		// Token: 0x1700020D RID: 525
		// (get) Token: 0x0600081F RID: 2079 RVA: 0x0001B45C File Offset: 0x0001965C
		// (set) Token: 0x06000820 RID: 2080 RVA: 0x0001B476 File Offset: 0x00019676
		public KeyCode keyCode
		{
			[CompilerGenerated]
			get
			{
				return this.<keyCode>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<keyCode>k__BackingField = value;
			}
		}

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x06000821 RID: 2081 RVA: 0x0001B480 File Offset: 0x00019680
		public bool shiftKey
		{
			get
			{
				return (this.modifiers & EventModifiers.Shift) != EventModifiers.None;
			}
		}

		// Token: 0x1700020F RID: 527
		// (get) Token: 0x06000822 RID: 2082 RVA: 0x0001B4A4 File Offset: 0x000196A4
		public bool ctrlKey
		{
			get
			{
				return (this.modifiers & EventModifiers.Control) != EventModifiers.None;
			}
		}

		// Token: 0x17000210 RID: 528
		// (get) Token: 0x06000823 RID: 2083 RVA: 0x0001B4C8 File Offset: 0x000196C8
		public bool commandKey
		{
			get
			{
				return (this.modifiers & EventModifiers.Command) != EventModifiers.None;
			}
		}

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x06000824 RID: 2084 RVA: 0x0001B4EC File Offset: 0x000196EC
		public bool altKey
		{
			get
			{
				return (this.modifiers & EventModifiers.Alt) != EventModifiers.None;
			}
		}

		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06000825 RID: 2085 RVA: 0x0001B510 File Offset: 0x00019710
		public bool actionKey
		{
			get
			{
				bool result;
				if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
				{
					result = this.commandKey;
				}
				else
				{
					result = this.ctrlKey;
				}
				return result;
			}
		}

		// Token: 0x06000826 RID: 2086 RVA: 0x0001B54E File Offset: 0x0001974E
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown | EventBase.EventFlags.Cancellable);
			this.modifiers = EventModifiers.None;
			this.character = '\0';
			this.keyCode = KeyCode.None;
		}

		// Token: 0x06000827 RID: 2087 RVA: 0x0001B574 File Offset: 0x00019774
		public static T GetPooled(char c, KeyCode keyCode, EventModifiers modifiers)
		{
			T pooled = EventBase<T>.GetPooled();
			pooled.modifiers = modifiers;
			pooled.character = c;
			pooled.keyCode = keyCode;
			return pooled;
		}

		// Token: 0x06000828 RID: 2088 RVA: 0x0001B5BC File Offset: 0x000197BC
		public static T GetPooled(Event systemEvent)
		{
			T pooled = EventBase<T>.GetPooled();
			pooled.imguiEvent = systemEvent;
			if (systemEvent != null)
			{
				pooled.modifiers = systemEvent.modifiers;
				pooled.character = systemEvent.character;
				pooled.keyCode = systemEvent.keyCode;
			}
			return pooled;
		}

		// Token: 0x04000316 RID: 790
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private EventModifiers <modifiers>k__BackingField;

		// Token: 0x04000317 RID: 791
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private char <character>k__BackingField;

		// Token: 0x04000318 RID: 792
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private KeyCode <keyCode>k__BackingField;
	}
}
