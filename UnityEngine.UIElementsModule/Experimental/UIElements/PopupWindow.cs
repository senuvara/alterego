﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200006A RID: 106
	public class PopupWindow : TextElement
	{
		// Token: 0x060002FC RID: 764 RVA: 0x0000A8FC File Offset: 0x00008AFC
		public PopupWindow()
		{
			this.m_ContentContainer = new VisualElement
			{
				name = "ContentContainer"
			};
			base.shadow.Add(this.m_ContentContainer);
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x060002FD RID: 765 RVA: 0x0000A93C File Offset: 0x00008B3C
		public override VisualElement contentContainer
		{
			get
			{
				return this.m_ContentContainer;
			}
		}

		// Token: 0x0400012A RID: 298
		private VisualElement m_ContentContainer;

		// Token: 0x0200006B RID: 107
		public new class UxmlFactory : UxmlFactory<PopupWindow, PopupWindow.UxmlTraits>
		{
			// Token: 0x060002FE RID: 766 RVA: 0x0000A957 File Offset: 0x00008B57
			public UxmlFactory()
			{
			}
		}

		// Token: 0x0200006C RID: 108
		public new class UxmlTraits : TextElement.UxmlTraits
		{
			// Token: 0x060002FF RID: 767 RVA: 0x00002DE3 File Offset: 0x00000FE3
			public UxmlTraits()
			{
			}

			// Token: 0x170000DA RID: 218
			// (get) Token: 0x06000300 RID: 768 RVA: 0x0000A960 File Offset: 0x00008B60
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield return new UxmlChildElementDescription(typeof(VisualElement));
					yield break;
				}
			}

			// Token: 0x0200006D RID: 109
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x06000301 RID: 769 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x06000302 RID: 770 RVA: 0x0000A984 File Offset: 0x00008B84
				public bool MoveNext()
				{
					uint num = (uint)this.$PC;
					this.$PC = -1;
					switch (num)
					{
					case 0U:
						this.$current = new UxmlChildElementDescription(typeof(VisualElement));
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						return true;
					case 1U:
						this.$PC = -1;
						break;
					}
					return false;
				}

				// Token: 0x170000DB RID: 219
				// (get) Token: 0x06000303 RID: 771 RVA: 0x0000A9E8 File Offset: 0x00008BE8
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x170000DC RID: 220
				// (get) Token: 0x06000304 RID: 772 RVA: 0x0000AA04 File Offset: 0x00008C04
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x06000305 RID: 773 RVA: 0x0000AA1E File Offset: 0x00008C1E
				[DebuggerHidden]
				public void Dispose()
				{
					this.$disposing = true;
					this.$PC = -1;
				}

				// Token: 0x06000306 RID: 774 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x06000307 RID: 775 RVA: 0x0000AA30 File Offset: 0x00008C30
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x06000308 RID: 776 RVA: 0x0000AA4A File Offset: 0x00008C4A
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new PopupWindow.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x0400012B RID: 299
				internal UxmlChildElementDescription $current;

				// Token: 0x0400012C RID: 300
				internal bool $disposing;

				// Token: 0x0400012D RID: 301
				internal int $PC;
			}
		}
	}
}
