﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000178 RID: 376
	[Serializable]
	internal class VisualElementAsset : IUxmlAttributes, ISerializationCallbackReceiver
	{
		// Token: 0x060009A0 RID: 2464 RVA: 0x00021362 File Offset: 0x0001F562
		public VisualElementAsset(string fullTypeName)
		{
			this.m_FullTypeName = fullTypeName;
			this.m_Name = string.Empty;
			this.m_Text = string.Empty;
			this.m_PickingMode = PickingMode.Position;
		}

		// Token: 0x1700027B RID: 635
		// (get) Token: 0x060009A1 RID: 2465 RVA: 0x00021390 File Offset: 0x0001F590
		// (set) Token: 0x060009A2 RID: 2466 RVA: 0x000213AB File Offset: 0x0001F5AB
		public int id
		{
			get
			{
				return this.m_Id;
			}
			set
			{
				this.m_Id = value;
			}
		}

		// Token: 0x1700027C RID: 636
		// (get) Token: 0x060009A3 RID: 2467 RVA: 0x000213B8 File Offset: 0x0001F5B8
		// (set) Token: 0x060009A4 RID: 2468 RVA: 0x000213D3 File Offset: 0x0001F5D3
		public int parentId
		{
			get
			{
				return this.m_ParentId;
			}
			set
			{
				this.m_ParentId = value;
			}
		}

		// Token: 0x1700027D RID: 637
		// (get) Token: 0x060009A5 RID: 2469 RVA: 0x000213E0 File Offset: 0x0001F5E0
		// (set) Token: 0x060009A6 RID: 2470 RVA: 0x000213FB File Offset: 0x0001F5FB
		public int ruleIndex
		{
			get
			{
				return this.m_RuleIndex;
			}
			set
			{
				this.m_RuleIndex = value;
			}
		}

		// Token: 0x1700027E RID: 638
		// (get) Token: 0x060009A7 RID: 2471 RVA: 0x00021408 File Offset: 0x0001F608
		// (set) Token: 0x060009A8 RID: 2472 RVA: 0x00021423 File Offset: 0x0001F623
		public string fullTypeName
		{
			get
			{
				return this.m_FullTypeName;
			}
			set
			{
				this.m_FullTypeName = value;
			}
		}

		// Token: 0x1700027F RID: 639
		// (get) Token: 0x060009A9 RID: 2473 RVA: 0x00021430 File Offset: 0x0001F630
		// (set) Token: 0x060009AA RID: 2474 RVA: 0x0002144B File Offset: 0x0001F64B
		public string[] classes
		{
			get
			{
				return this.m_Classes;
			}
			set
			{
				this.m_Classes = value;
			}
		}

		// Token: 0x17000280 RID: 640
		// (get) Token: 0x060009AB RID: 2475 RVA: 0x00021458 File Offset: 0x0001F658
		// (set) Token: 0x060009AC RID: 2476 RVA: 0x00021491 File Offset: 0x0001F691
		public List<string> stylesheets
		{
			get
			{
				return (this.m_Stylesheets != null) ? this.m_Stylesheets : (this.m_Stylesheets = new List<string>());
			}
			set
			{
				this.m_Stylesheets = value;
			}
		}

		// Token: 0x060009AD RID: 2477 RVA: 0x0002149C File Offset: 0x0001F69C
		public VisualElement Create(CreationContext ctx)
		{
			List<IUxmlFactory> list;
			VisualElement result;
			if (!VisualElementFactoryRegistry.TryGetValue(this.fullTypeName, out list))
			{
				Debug.LogErrorFormat("Element '{0}' has no registered factory method.", new object[]
				{
					this.fullTypeName
				});
				result = new Label(string.Format("Unknown type: '{0}'", this.fullTypeName));
			}
			else
			{
				IUxmlFactory uxmlFactory = null;
				foreach (IUxmlFactory uxmlFactory2 in list)
				{
					if (uxmlFactory2.AcceptsAttributeBag(this, ctx))
					{
						uxmlFactory = uxmlFactory2;
						break;
					}
				}
				if (uxmlFactory == null)
				{
					Debug.LogErrorFormat("Element '{0}' has a no factory that accept the set of XML attributes specified.", new object[]
					{
						this.fullTypeName
					});
					result = new Label(string.Format("Type with no factory: '{0}'", this.fullTypeName));
				}
				else if (uxmlFactory is UxmlRootElementFactory)
				{
					result = null;
				}
				else
				{
					VisualElement visualElement = uxmlFactory.Create(this, ctx);
					if (visualElement == null)
					{
						Debug.LogErrorFormat("The factory of Visual Element Type '{0}' has returned a null object", new object[]
						{
							this.fullTypeName
						});
						result = new Label(string.Format("The factory of Visual Element Type '{0}' has returned a null object", this.fullTypeName));
					}
					else
					{
						if (this.classes != null)
						{
							for (int i = 0; i < this.classes.Length; i++)
							{
								visualElement.AddToClassList(this.classes[i]);
							}
						}
						if (this.stylesheets != null)
						{
							for (int j = 0; j < this.stylesheets.Count; j++)
							{
								visualElement.AddStyleSheetPath(this.stylesheets[j]);
							}
						}
						result = visualElement;
					}
				}
			}
			return result;
		}

		// Token: 0x060009AE RID: 2478 RVA: 0x000035A6 File Offset: 0x000017A6
		public void OnBeforeSerialize()
		{
		}

		// Token: 0x060009AF RID: 2479 RVA: 0x00021668 File Offset: 0x0001F868
		public void OnAfterDeserialize()
		{
			if (!this.m_Properties.Contains("name"))
			{
				this.AddProperty("name", this.m_Name);
			}
			if (!this.m_Properties.Contains("text"))
			{
				this.AddProperty("text", this.m_Text);
			}
			if (!this.m_Properties.Contains("picking-mode") && !this.m_Properties.Contains("pickingMode"))
			{
				this.AddProperty("picking-mode", this.m_PickingMode.ToString());
			}
		}

		// Token: 0x060009B0 RID: 2480 RVA: 0x0002170E File Offset: 0x0001F90E
		public void AddProperty(string propertyName, string propertyValue)
		{
			this.SetOrAddProperty(propertyName, propertyValue);
		}

		// Token: 0x060009B1 RID: 2481 RVA: 0x0002171C File Offset: 0x0001F91C
		private void SetOrAddProperty(string propertyName, string propertyValue)
		{
			if (this.m_Properties == null)
			{
				this.m_Properties = new List<string>();
			}
			for (int i = 0; i < this.m_Properties.Count - 1; i += 2)
			{
				if (this.m_Properties[i] == propertyName)
				{
					this.m_Properties[i + 1] = propertyValue;
					return;
				}
			}
			this.m_Properties.Add(propertyName);
			this.m_Properties.Add(propertyValue);
		}

		// Token: 0x060009B2 RID: 2482 RVA: 0x000217A4 File Offset: 0x0001F9A4
		public bool TryGetAttributeValue(string propertyName, out string value)
		{
			bool result;
			if (this.m_Properties == null)
			{
				value = null;
				result = false;
			}
			else
			{
				for (int i = 0; i < this.m_Properties.Count - 1; i += 2)
				{
					if (this.m_Properties[i] == propertyName)
					{
						value = this.m_Properties[i + 1];
						return true;
					}
				}
				value = null;
				result = false;
			}
			return result;
		}

		// Token: 0x0400044F RID: 1103
		[SerializeField]
		private string m_Name;

		// Token: 0x04000450 RID: 1104
		[SerializeField]
		private int m_Id;

		// Token: 0x04000451 RID: 1105
		[SerializeField]
		private int m_ParentId;

		// Token: 0x04000452 RID: 1106
		[SerializeField]
		private int m_RuleIndex;

		// Token: 0x04000453 RID: 1107
		[SerializeField]
		private string m_Text;

		// Token: 0x04000454 RID: 1108
		[SerializeField]
		private PickingMode m_PickingMode;

		// Token: 0x04000455 RID: 1109
		[SerializeField]
		private string m_FullTypeName;

		// Token: 0x04000456 RID: 1110
		[SerializeField]
		private string[] m_Classes;

		// Token: 0x04000457 RID: 1111
		[SerializeField]
		private List<string> m_Stylesheets;

		// Token: 0x04000458 RID: 1112
		[SerializeField]
		private List<string> m_Properties;
	}
}
