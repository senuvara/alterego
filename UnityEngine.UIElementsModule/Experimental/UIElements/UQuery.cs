﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Experimental.UIElements.StyleSheets;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000AA RID: 170
	public static class UQuery
	{
		// Token: 0x0600041C RID: 1052 RVA: 0x0000E370 File Offset: 0x0000C570
		// Note: this type is marked as 'beforefieldinit'.
		static UQuery()
		{
		}

		// Token: 0x040001F1 RID: 497
		private static UQuery.FirstQueryMatcher s_First = new UQuery.FirstQueryMatcher();

		// Token: 0x040001F2 RID: 498
		private static UQuery.LastQueryMatcher s_Last = new UQuery.LastQueryMatcher();

		// Token: 0x040001F3 RID: 499
		private static UQuery.IndexQueryMatcher s_Index = new UQuery.IndexQueryMatcher();

		// Token: 0x020000AB RID: 171
		internal interface IVisualPredicateWrapper
		{
			// Token: 0x0600041D RID: 1053
			bool Predicate(object e);
		}

		// Token: 0x020000AC RID: 172
		internal class IsOfType<T> : UQuery.IVisualPredicateWrapper where T : VisualElement
		{
			// Token: 0x0600041E RID: 1054 RVA: 0x00002223 File Offset: 0x00000423
			public IsOfType()
			{
			}

			// Token: 0x0600041F RID: 1055 RVA: 0x0000E390 File Offset: 0x0000C590
			public bool Predicate(object e)
			{
				return e is T;
			}

			// Token: 0x06000420 RID: 1056 RVA: 0x0000E3AE File Offset: 0x0000C5AE
			// Note: this type is marked as 'beforefieldinit'.
			static IsOfType()
			{
			}

			// Token: 0x040001F4 RID: 500
			public static UQuery.IsOfType<T> s_Instance = new UQuery.IsOfType<T>();
		}

		// Token: 0x020000AD RID: 173
		internal class PredicateWrapper<T> : UQuery.IVisualPredicateWrapper where T : VisualElement
		{
			// Token: 0x06000421 RID: 1057 RVA: 0x0000E3BA File Offset: 0x0000C5BA
			public PredicateWrapper(Func<T, bool> p)
			{
				this.predicate = p;
			}

			// Token: 0x06000422 RID: 1058 RVA: 0x0000E3CC File Offset: 0x0000C5CC
			public bool Predicate(object e)
			{
				T t = e as T;
				return t != null && this.predicate(t);
			}

			// Token: 0x040001F5 RID: 501
			private Func<T, bool> predicate;
		}

		// Token: 0x020000AE RID: 174
		private abstract class UQueryMatcher : HierarchyTraversal
		{
			// Token: 0x06000423 RID: 1059 RVA: 0x0000E40C File Offset: 0x0000C60C
			protected UQueryMatcher()
			{
			}

			// Token: 0x06000424 RID: 1060 RVA: 0x0000E415 File Offset: 0x0000C615
			public override void Traverse(VisualElement element)
			{
				this.m_MatchersUsedInRecursion = new List<RuleMatcher>(this.m_Matchers);
				base.Traverse(element);
			}

			// Token: 0x06000425 RID: 1061 RVA: 0x0000E430 File Offset: 0x0000C630
			protected virtual bool OnRuleMatchedElement(RuleMatcher matcher, VisualElement element)
			{
				return false;
			}

			// Token: 0x06000426 RID: 1062 RVA: 0x000035A6 File Offset: 0x000017A6
			private static void NoProcessResult(VisualElement e, MatchResultInfo i)
			{
			}

			// Token: 0x06000427 RID: 1063 RVA: 0x0000E448 File Offset: 0x0000C648
			public override void TraverseRecursive(VisualElement element, int depth)
			{
				int count = this.m_MatchersUsedInRecursion.Count;
				int count2 = this.m_MatchersUsedInRecursion.Count;
				for (int i = 0; i < count2; i++)
				{
					RuleMatcher matcher = this.m_MatchersUsedInRecursion[i];
					StyleComplexSelector complexSelector = matcher.complexSelector;
					if (UQuery.UQueryMatcher.<>f__mg$cache0 == null)
					{
						UQuery.UQueryMatcher.<>f__mg$cache0 = new Action<VisualElement, MatchResultInfo>(UQuery.UQueryMatcher.NoProcessResult);
					}
					if (StyleSelectorHelper.MatchRightToLeft(element, complexSelector, UQuery.UQueryMatcher.<>f__mg$cache0))
					{
						if (this.OnRuleMatchedElement(matcher, element))
						{
							return;
						}
					}
				}
				base.Recurse(element, depth);
				if (this.m_MatchersUsedInRecursion.Count > count)
				{
					this.m_MatchersUsedInRecursion.RemoveRange(count, this.m_MatchersUsedInRecursion.Count - count);
					return;
				}
			}

			// Token: 0x06000428 RID: 1064 RVA: 0x0000E507 File Offset: 0x0000C707
			public virtual void Run(VisualElement root, List<RuleMatcher> matchers)
			{
				this.m_Matchers = matchers;
				this.Traverse(root);
			}

			// Token: 0x040001F6 RID: 502
			internal List<RuleMatcher> m_Matchers;

			// Token: 0x040001F7 RID: 503
			internal List<RuleMatcher> m_MatchersUsedInRecursion;

			// Token: 0x040001F8 RID: 504
			[CompilerGenerated]
			private static Action<VisualElement, MatchResultInfo> <>f__mg$cache0;
		}

		// Token: 0x020000AF RID: 175
		private abstract class SingleQueryMatcher : UQuery.UQueryMatcher
		{
			// Token: 0x06000429 RID: 1065 RVA: 0x0000E518 File Offset: 0x0000C718
			protected SingleQueryMatcher()
			{
			}

			// Token: 0x17000113 RID: 275
			// (get) Token: 0x0600042A RID: 1066 RVA: 0x0000E520 File Offset: 0x0000C720
			// (set) Token: 0x0600042B RID: 1067 RVA: 0x0000E53A File Offset: 0x0000C73A
			public VisualElement match
			{
				[CompilerGenerated]
				get
				{
					return this.<match>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<match>k__BackingField = value;
				}
			}

			// Token: 0x0600042C RID: 1068 RVA: 0x0000E543 File Offset: 0x0000C743
			public override void Run(VisualElement root, List<RuleMatcher> matchers)
			{
				this.match = null;
				base.Run(root, matchers);
			}

			// Token: 0x040001F9 RID: 505
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private VisualElement <match>k__BackingField;
		}

		// Token: 0x020000B0 RID: 176
		private class FirstQueryMatcher : UQuery.SingleQueryMatcher
		{
			// Token: 0x0600042D RID: 1069 RVA: 0x0000E555 File Offset: 0x0000C755
			public FirstQueryMatcher()
			{
			}

			// Token: 0x0600042E RID: 1070 RVA: 0x0000E560 File Offset: 0x0000C760
			protected override bool OnRuleMatchedElement(RuleMatcher matcher, VisualElement element)
			{
				if (base.match == null)
				{
					base.match = element;
				}
				return true;
			}
		}

		// Token: 0x020000B1 RID: 177
		private class LastQueryMatcher : UQuery.SingleQueryMatcher
		{
			// Token: 0x0600042F RID: 1071 RVA: 0x0000E555 File Offset: 0x0000C755
			public LastQueryMatcher()
			{
			}

			// Token: 0x06000430 RID: 1072 RVA: 0x0000E588 File Offset: 0x0000C788
			protected override bool OnRuleMatchedElement(RuleMatcher matcher, VisualElement element)
			{
				base.match = element;
				return false;
			}
		}

		// Token: 0x020000B2 RID: 178
		private class IndexQueryMatcher : UQuery.SingleQueryMatcher
		{
			// Token: 0x06000431 RID: 1073 RVA: 0x0000E5A5 File Offset: 0x0000C7A5
			public IndexQueryMatcher()
			{
			}

			// Token: 0x17000114 RID: 276
			// (get) Token: 0x06000432 RID: 1074 RVA: 0x0000E5B4 File Offset: 0x0000C7B4
			// (set) Token: 0x06000433 RID: 1075 RVA: 0x0000E5CF File Offset: 0x0000C7CF
			public int matchIndex
			{
				get
				{
					return this._matchIndex;
				}
				set
				{
					this.matchCount = -1;
					this._matchIndex = value;
				}
			}

			// Token: 0x06000434 RID: 1076 RVA: 0x0000E5E0 File Offset: 0x0000C7E0
			public override void Run(VisualElement root, List<RuleMatcher> matchers)
			{
				this.matchCount = -1;
				base.Run(root, matchers);
			}

			// Token: 0x06000435 RID: 1077 RVA: 0x0000E5F4 File Offset: 0x0000C7F4
			protected override bool OnRuleMatchedElement(RuleMatcher matcher, VisualElement element)
			{
				this.matchCount++;
				if (this.matchCount == this._matchIndex)
				{
					base.match = element;
				}
				return this.matchCount >= this._matchIndex;
			}

			// Token: 0x040001FA RID: 506
			private int matchCount = -1;

			// Token: 0x040001FB RID: 507
			private int _matchIndex;
		}

		// Token: 0x020000B3 RID: 179
		public struct QueryBuilder<T> where T : VisualElement
		{
			// Token: 0x06000436 RID: 1078 RVA: 0x0000E644 File Offset: 0x0000C844
			public QueryBuilder(VisualElement visualElement)
			{
				this = default(UQuery.QueryBuilder<T>);
				this.m_Element = visualElement;
				this.m_Parts = null;
				this.m_StyleSelectors = null;
				this.m_Relationship = StyleSelectorRelationship.None;
				this.m_Matchers = new List<RuleMatcher>();
				this.pseudoStatesMask = (this.negatedPseudoStatesMask = 0);
			}

			// Token: 0x17000115 RID: 277
			// (get) Token: 0x06000437 RID: 1079 RVA: 0x0000E690 File Offset: 0x0000C890
			private List<StyleSelector> styleSelectors
			{
				get
				{
					List<StyleSelector> result;
					if ((result = this.m_StyleSelectors) == null)
					{
						result = (this.m_StyleSelectors = new List<StyleSelector>());
					}
					return result;
				}
			}

			// Token: 0x17000116 RID: 278
			// (get) Token: 0x06000438 RID: 1080 RVA: 0x0000E6C0 File Offset: 0x0000C8C0
			private List<StyleSelectorPart> parts
			{
				get
				{
					List<StyleSelectorPart> result;
					if ((result = this.m_Parts) == null)
					{
						result = (this.m_Parts = new List<StyleSelectorPart>());
					}
					return result;
				}
			}

			// Token: 0x06000439 RID: 1081 RVA: 0x0000E6F0 File Offset: 0x0000C8F0
			public UQuery.QueryBuilder<T> Class(string classname)
			{
				this.AddClass(classname);
				return this;
			}

			// Token: 0x0600043A RID: 1082 RVA: 0x0000E714 File Offset: 0x0000C914
			public UQuery.QueryBuilder<T> Name(string id)
			{
				this.AddName(id);
				return this;
			}

			// Token: 0x0600043B RID: 1083 RVA: 0x0000E738 File Offset: 0x0000C938
			public UQuery.QueryBuilder<T2> Descendents<T2>(string name = null, params string[] classNames) where T2 : VisualElement
			{
				this.FinishCurrentSelector();
				this.AddType<T2>();
				this.AddName(name);
				this.AddClasses(classNames);
				return this.AddRelationship<T2>(StyleSelectorRelationship.Descendent);
			}

			// Token: 0x0600043C RID: 1084 RVA: 0x0000E770 File Offset: 0x0000C970
			public UQuery.QueryBuilder<T2> Descendents<T2>(string name = null, string classname = null) where T2 : VisualElement
			{
				this.FinishCurrentSelector();
				this.AddType<T2>();
				this.AddName(name);
				this.AddClass(classname);
				return this.AddRelationship<T2>(StyleSelectorRelationship.Descendent);
			}

			// Token: 0x0600043D RID: 1085 RVA: 0x0000E7A8 File Offset: 0x0000C9A8
			public UQuery.QueryBuilder<T2> Children<T2>(string name = null, params string[] classes) where T2 : VisualElement
			{
				this.FinishCurrentSelector();
				this.AddType<T2>();
				this.AddName(name);
				this.AddClasses(classes);
				return this.AddRelationship<T2>(StyleSelectorRelationship.Child);
			}

			// Token: 0x0600043E RID: 1086 RVA: 0x0000E7E0 File Offset: 0x0000C9E0
			public UQuery.QueryBuilder<T2> Children<T2>(string name = null, string className = null) where T2 : VisualElement
			{
				this.FinishCurrentSelector();
				this.AddType<T2>();
				this.AddName(name);
				this.AddClass(className);
				return this.AddRelationship<T2>(StyleSelectorRelationship.Child);
			}

			// Token: 0x0600043F RID: 1087 RVA: 0x0000E818 File Offset: 0x0000CA18
			public UQuery.QueryBuilder<T2> OfType<T2>(string name = null, params string[] classes) where T2 : VisualElement
			{
				this.AddType<T2>();
				this.AddName(name);
				this.AddClasses(classes);
				return this.AddRelationship<T2>(StyleSelectorRelationship.None);
			}

			// Token: 0x06000440 RID: 1088 RVA: 0x0000E848 File Offset: 0x0000CA48
			public UQuery.QueryBuilder<T2> OfType<T2>(string name = null, string className = null) where T2 : VisualElement
			{
				this.AddType<T2>();
				this.AddName(name);
				this.AddClass(className);
				return this.AddRelationship<T2>(StyleSelectorRelationship.None);
			}

			// Token: 0x06000441 RID: 1089 RVA: 0x0000E878 File Offset: 0x0000CA78
			public UQuery.QueryBuilder<T> Where(Func<T, bool> selectorPredicate)
			{
				this.parts.Add(StyleSelectorPart.CreatePredicate(new UQuery.PredicateWrapper<T>(selectorPredicate)));
				return this;
			}

			// Token: 0x06000442 RID: 1090 RVA: 0x0000E8A9 File Offset: 0x0000CAA9
			private void AddClass(string c)
			{
				if (c != null)
				{
					this.parts.Add(StyleSelectorPart.CreateClass(c));
				}
			}

			// Token: 0x06000443 RID: 1091 RVA: 0x0000E8C4 File Offset: 0x0000CAC4
			private void AddClasses(params string[] classes)
			{
				if (classes != null)
				{
					for (int i = 0; i < classes.Length; i++)
					{
						this.AddClass(classes[i]);
					}
				}
			}

			// Token: 0x06000444 RID: 1092 RVA: 0x0000E8F7 File Offset: 0x0000CAF7
			private void AddName(string id)
			{
				if (id != null)
				{
					this.parts.Add(StyleSelectorPart.CreateId(id));
				}
			}

			// Token: 0x06000445 RID: 1093 RVA: 0x0000E911 File Offset: 0x0000CB11
			private void AddType<T2>() where T2 : VisualElement
			{
				if (typeof(T2) != typeof(VisualElement))
				{
					this.parts.Add(StyleSelectorPart.CreatePredicate(UQuery.IsOfType<T2>.s_Instance));
				}
			}

			// Token: 0x06000446 RID: 1094 RVA: 0x0000E944 File Offset: 0x0000CB44
			private UQuery.QueryBuilder<T> AddPseudoState(PseudoStates s)
			{
				this.pseudoStatesMask |= (int)s;
				return this;
			}

			// Token: 0x06000447 RID: 1095 RVA: 0x0000E970 File Offset: 0x0000CB70
			private UQuery.QueryBuilder<T> AddNegativePseudoState(PseudoStates s)
			{
				this.negatedPseudoStatesMask |= (int)s;
				return this;
			}

			// Token: 0x06000448 RID: 1096 RVA: 0x0000E99C File Offset: 0x0000CB9C
			public UQuery.QueryBuilder<T> Active()
			{
				return this.AddPseudoState(PseudoStates.Active);
			}

			// Token: 0x06000449 RID: 1097 RVA: 0x0000E9B8 File Offset: 0x0000CBB8
			public UQuery.QueryBuilder<T> NotActive()
			{
				return this.AddNegativePseudoState(PseudoStates.Active);
			}

			// Token: 0x0600044A RID: 1098 RVA: 0x0000E9D4 File Offset: 0x0000CBD4
			public UQuery.QueryBuilder<T> Visible()
			{
				return from e in this
				where e.visible
				select e;
			}

			// Token: 0x0600044B RID: 1099 RVA: 0x0000EA0C File Offset: 0x0000CC0C
			public UQuery.QueryBuilder<T> NotVisible()
			{
				return from e in this
				where !e.visible
				select e;
			}

			// Token: 0x0600044C RID: 1100 RVA: 0x0000EA44 File Offset: 0x0000CC44
			public UQuery.QueryBuilder<T> Hovered()
			{
				return this.AddPseudoState(PseudoStates.Hover);
			}

			// Token: 0x0600044D RID: 1101 RVA: 0x0000EA60 File Offset: 0x0000CC60
			public UQuery.QueryBuilder<T> NotHovered()
			{
				return this.AddNegativePseudoState(PseudoStates.Hover);
			}

			// Token: 0x0600044E RID: 1102 RVA: 0x0000EA7C File Offset: 0x0000CC7C
			public UQuery.QueryBuilder<T> Checked()
			{
				return this.AddPseudoState(PseudoStates.Checked);
			}

			// Token: 0x0600044F RID: 1103 RVA: 0x0000EA98 File Offset: 0x0000CC98
			public UQuery.QueryBuilder<T> NotChecked()
			{
				return this.AddNegativePseudoState(PseudoStates.Checked);
			}

			// Token: 0x06000450 RID: 1104 RVA: 0x0000EAB4 File Offset: 0x0000CCB4
			public UQuery.QueryBuilder<T> Selected()
			{
				return this.AddPseudoState(PseudoStates.Selected);
			}

			// Token: 0x06000451 RID: 1105 RVA: 0x0000EAD4 File Offset: 0x0000CCD4
			public UQuery.QueryBuilder<T> NotSelected()
			{
				return this.AddNegativePseudoState(PseudoStates.Selected);
			}

			// Token: 0x06000452 RID: 1106 RVA: 0x0000EAF4 File Offset: 0x0000CCF4
			public UQuery.QueryBuilder<T> Enabled()
			{
				return this.AddNegativePseudoState(PseudoStates.Disabled);
			}

			// Token: 0x06000453 RID: 1107 RVA: 0x0000EB14 File Offset: 0x0000CD14
			public UQuery.QueryBuilder<T> NotEnabled()
			{
				return this.AddPseudoState(PseudoStates.Disabled);
			}

			// Token: 0x06000454 RID: 1108 RVA: 0x0000EB34 File Offset: 0x0000CD34
			public UQuery.QueryBuilder<T> Focused()
			{
				return this.AddPseudoState(PseudoStates.Focus);
			}

			// Token: 0x06000455 RID: 1109 RVA: 0x0000EB54 File Offset: 0x0000CD54
			public UQuery.QueryBuilder<T> NotFocused()
			{
				return this.AddNegativePseudoState(PseudoStates.Focus);
			}

			// Token: 0x06000456 RID: 1110 RVA: 0x0000EB74 File Offset: 0x0000CD74
			private UQuery.QueryBuilder<T2> AddRelationship<T2>(StyleSelectorRelationship relationship) where T2 : VisualElement
			{
				return new UQuery.QueryBuilder<T2>(this.m_Element)
				{
					m_Matchers = this.m_Matchers,
					m_Parts = this.m_Parts,
					m_StyleSelectors = this.m_StyleSelectors,
					m_Relationship = ((relationship != StyleSelectorRelationship.None) ? relationship : this.m_Relationship),
					pseudoStatesMask = this.pseudoStatesMask,
					negatedPseudoStatesMask = this.negatedPseudoStatesMask
				};
			}

			// Token: 0x06000457 RID: 1111 RVA: 0x0000EBF0 File Offset: 0x0000CDF0
			private void AddPseudoStatesRuleIfNecessasy()
			{
				if (this.pseudoStatesMask != 0 || this.negatedPseudoStatesMask != 0)
				{
					this.parts.Add(new StyleSelectorPart
					{
						type = StyleSelectorType.PseudoClass
					});
				}
			}

			// Token: 0x06000458 RID: 1112 RVA: 0x0000EC34 File Offset: 0x0000CE34
			private void FinishSelector()
			{
				this.FinishCurrentSelector();
				if (this.styleSelectors.Count > 0)
				{
					StyleComplexSelector styleComplexSelector = new StyleComplexSelector();
					styleComplexSelector.selectors = this.styleSelectors.ToArray();
					this.styleSelectors.Clear();
					this.m_Matchers.Add(new RuleMatcher
					{
						complexSelector = styleComplexSelector
					});
				}
			}

			// Token: 0x06000459 RID: 1113 RVA: 0x0000EC9C File Offset: 0x0000CE9C
			private bool CurrentSelectorEmpty()
			{
				return this.parts.Count == 0 && this.m_Relationship == StyleSelectorRelationship.None && this.pseudoStatesMask == 0 && this.negatedPseudoStatesMask == 0;
			}

			// Token: 0x0600045A RID: 1114 RVA: 0x0000ECE4 File Offset: 0x0000CEE4
			private void FinishCurrentSelector()
			{
				if (!this.CurrentSelectorEmpty())
				{
					StyleSelector styleSelector = new StyleSelector();
					styleSelector.previousRelationship = this.m_Relationship;
					this.AddPseudoStatesRuleIfNecessasy();
					styleSelector.parts = this.m_Parts.ToArray();
					styleSelector.pseudoStateMask = this.pseudoStatesMask;
					styleSelector.negatedPseudoStateMask = this.negatedPseudoStatesMask;
					this.styleSelectors.Add(styleSelector);
					this.m_Parts.Clear();
					this.pseudoStatesMask = (this.negatedPseudoStatesMask = 0);
				}
			}

			// Token: 0x0600045B RID: 1115 RVA: 0x0000ED68 File Offset: 0x0000CF68
			public UQuery.QueryState<T> Build()
			{
				this.FinishSelector();
				return new UQuery.QueryState<T>(this.m_Element, this.m_Matchers);
			}

			// Token: 0x0600045C RID: 1116 RVA: 0x0000ED94 File Offset: 0x0000CF94
			public static implicit operator T(UQuery.QueryBuilder<T> s)
			{
				return s.First();
			}

			// Token: 0x0600045D RID: 1117 RVA: 0x0000EDB0 File Offset: 0x0000CFB0
			public T First()
			{
				return this.Build().First();
			}

			// Token: 0x0600045E RID: 1118 RVA: 0x0000EDD4 File Offset: 0x0000CFD4
			public T Last()
			{
				return this.Build().Last();
			}

			// Token: 0x0600045F RID: 1119 RVA: 0x0000EDF8 File Offset: 0x0000CFF8
			public List<T> ToList()
			{
				return this.Build().ToList();
			}

			// Token: 0x06000460 RID: 1120 RVA: 0x0000EE1C File Offset: 0x0000D01C
			public void ToList(List<T> results)
			{
				this.Build().ToList(results);
			}

			// Token: 0x06000461 RID: 1121 RVA: 0x0000EE3C File Offset: 0x0000D03C
			public T AtIndex(int index)
			{
				return this.Build().AtIndex(index);
			}

			// Token: 0x06000462 RID: 1122 RVA: 0x0000EE60 File Offset: 0x0000D060
			public void ForEach<T2>(List<T2> result, Func<T, T2> funcCall)
			{
				this.Build().ForEach<T2>(result, funcCall);
			}

			// Token: 0x06000463 RID: 1123 RVA: 0x0000EE80 File Offset: 0x0000D080
			public List<T2> ForEach<T2>(Func<T, T2> funcCall)
			{
				return this.Build().ForEach<T2>(funcCall);
			}

			// Token: 0x06000464 RID: 1124 RVA: 0x0000EEA4 File Offset: 0x0000D0A4
			public void ForEach(Action<T> funcCall)
			{
				this.Build().ForEach(funcCall);
			}

			// Token: 0x06000465 RID: 1125 RVA: 0x0000EEC4 File Offset: 0x0000D0C4
			[CompilerGenerated]
			private static bool <Visible>m__0(T e)
			{
				return e.visible;
			}

			// Token: 0x06000466 RID: 1126 RVA: 0x0000EEE8 File Offset: 0x0000D0E8
			[CompilerGenerated]
			private static bool <NotVisible>m__1(T e)
			{
				return !e.visible;
			}

			// Token: 0x040001FC RID: 508
			private List<StyleSelector> m_StyleSelectors;

			// Token: 0x040001FD RID: 509
			private List<StyleSelectorPart> m_Parts;

			// Token: 0x040001FE RID: 510
			private VisualElement m_Element;

			// Token: 0x040001FF RID: 511
			private List<RuleMatcher> m_Matchers;

			// Token: 0x04000200 RID: 512
			private StyleSelectorRelationship m_Relationship;

			// Token: 0x04000201 RID: 513
			private int pseudoStatesMask;

			// Token: 0x04000202 RID: 514
			private int negatedPseudoStatesMask;

			// Token: 0x04000203 RID: 515
			[CompilerGenerated]
			private static Func<T, bool> <>f__am$cache0;

			// Token: 0x04000204 RID: 516
			[CompilerGenerated]
			private static Func<T, bool> <>f__am$cache1;
		}

		// Token: 0x020000B4 RID: 180
		public struct QueryState<T> where T : VisualElement
		{
			// Token: 0x06000467 RID: 1127 RVA: 0x0000EF0C File Offset: 0x0000D10C
			internal QueryState(VisualElement element, List<RuleMatcher> matchers)
			{
				this.m_Element = element;
				this.m_Matchers = matchers;
			}

			// Token: 0x06000468 RID: 1128 RVA: 0x0000EF20 File Offset: 0x0000D120
			public UQuery.QueryState<T> RebuildOn(VisualElement element)
			{
				return new UQuery.QueryState<T>(element, this.m_Matchers);
			}

			// Token: 0x06000469 RID: 1129 RVA: 0x0000EF44 File Offset: 0x0000D144
			public T First()
			{
				UQuery.s_First.Run(this.m_Element, this.m_Matchers);
				T result = UQuery.s_First.match as T;
				UQuery.s_First.match = null;
				return result;
			}

			// Token: 0x0600046A RID: 1130 RVA: 0x0000EF90 File Offset: 0x0000D190
			public T Last()
			{
				UQuery.s_Last.Run(this.m_Element, this.m_Matchers);
				T result = UQuery.s_Last.match as T;
				UQuery.s_Last.match = null;
				return result;
			}

			// Token: 0x0600046B RID: 1131 RVA: 0x0000EFDC File Offset: 0x0000D1DC
			public void ToList(List<T> results)
			{
				UQuery.QueryState<T>.s_List.matches = results;
				UQuery.QueryState<T>.s_List.Run(this.m_Element, this.m_Matchers);
				UQuery.QueryState<T>.s_List.Reset();
			}

			// Token: 0x0600046C RID: 1132 RVA: 0x0000F00C File Offset: 0x0000D20C
			public List<T> ToList()
			{
				List<T> list = new List<T>();
				this.ToList(list);
				return list;
			}

			// Token: 0x0600046D RID: 1133 RVA: 0x0000F030 File Offset: 0x0000D230
			public T AtIndex(int index)
			{
				UQuery.s_Index.matchIndex = index;
				UQuery.s_Index.Run(this.m_Element, this.m_Matchers);
				T result = UQuery.s_Index.match as T;
				UQuery.s_Index.match = null;
				return result;
			}

			// Token: 0x0600046E RID: 1134 RVA: 0x0000F087 File Offset: 0x0000D287
			public void ForEach(Action<T> funcCall)
			{
				UQuery.QueryState<T>.s_Action.callBack = funcCall;
				UQuery.QueryState<T>.s_Action.Run(this.m_Element, this.m_Matchers);
				UQuery.QueryState<T>.s_Action.callBack = null;
			}

			// Token: 0x0600046F RID: 1135 RVA: 0x0000F0B8 File Offset: 0x0000D2B8
			public void ForEach<T2>(List<T2> result, Func<T, T2> funcCall)
			{
				UQuery.QueryState<T>.DelegateQueryMatcher<T2> s_Instance = UQuery.QueryState<T>.DelegateQueryMatcher<T2>.s_Instance;
				s_Instance.callBack = funcCall;
				s_Instance.result = result;
				s_Instance.Run(this.m_Element, this.m_Matchers);
				s_Instance.callBack = null;
				s_Instance.result = null;
			}

			// Token: 0x06000470 RID: 1136 RVA: 0x0000F0FC File Offset: 0x0000D2FC
			public List<T2> ForEach<T2>(Func<T, T2> funcCall)
			{
				List<T2> result = new List<T2>();
				this.ForEach<T2>(result, funcCall);
				return result;
			}

			// Token: 0x06000471 RID: 1137 RVA: 0x0000F120 File Offset: 0x0000D320
			// Note: this type is marked as 'beforefieldinit'.
			static QueryState()
			{
			}

			// Token: 0x04000205 RID: 517
			private readonly VisualElement m_Element;

			// Token: 0x04000206 RID: 518
			private readonly List<RuleMatcher> m_Matchers;

			// Token: 0x04000207 RID: 519
			private static readonly UQuery.QueryState<T>.ListQueryMatcher s_List = new UQuery.QueryState<T>.ListQueryMatcher();

			// Token: 0x04000208 RID: 520
			private static UQuery.QueryState<T>.ActionQueryMatcher s_Action = new UQuery.QueryState<T>.ActionQueryMatcher();

			// Token: 0x020000B5 RID: 181
			private class ListQueryMatcher : UQuery.UQueryMatcher
			{
				// Token: 0x06000472 RID: 1138 RVA: 0x0000E518 File Offset: 0x0000C718
				public ListQueryMatcher()
				{
				}

				// Token: 0x17000117 RID: 279
				// (get) Token: 0x06000473 RID: 1139 RVA: 0x0000F138 File Offset: 0x0000D338
				// (set) Token: 0x06000474 RID: 1140 RVA: 0x0000F152 File Offset: 0x0000D352
				public List<T> matches
				{
					[CompilerGenerated]
					get
					{
						return this.<matches>k__BackingField;
					}
					[CompilerGenerated]
					set
					{
						this.<matches>k__BackingField = value;
					}
				}

				// Token: 0x06000475 RID: 1141 RVA: 0x0000F15C File Offset: 0x0000D35C
				protected override bool OnRuleMatchedElement(RuleMatcher matcher, VisualElement element)
				{
					this.matches.Add(element as T);
					return false;
				}

				// Token: 0x06000476 RID: 1142 RVA: 0x0000F188 File Offset: 0x0000D388
				public void Reset()
				{
					this.matches = null;
				}

				// Token: 0x04000209 RID: 521
				[DebuggerBrowsable(DebuggerBrowsableState.Never)]
				[CompilerGenerated]
				private List<T> <matches>k__BackingField;
			}

			// Token: 0x020000B6 RID: 182
			private class ActionQueryMatcher : UQuery.UQueryMatcher
			{
				// Token: 0x06000477 RID: 1143 RVA: 0x0000E518 File Offset: 0x0000C718
				public ActionQueryMatcher()
				{
				}

				// Token: 0x17000118 RID: 280
				// (get) Token: 0x06000478 RID: 1144 RVA: 0x0000F194 File Offset: 0x0000D394
				// (set) Token: 0x06000479 RID: 1145 RVA: 0x0000F1AE File Offset: 0x0000D3AE
				internal Action<T> callBack
				{
					[CompilerGenerated]
					get
					{
						return this.<callBack>k__BackingField;
					}
					[CompilerGenerated]
					set
					{
						this.<callBack>k__BackingField = value;
					}
				}

				// Token: 0x0600047A RID: 1146 RVA: 0x0000F1B8 File Offset: 0x0000D3B8
				protected override bool OnRuleMatchedElement(RuleMatcher matcher, VisualElement element)
				{
					T t = element as T;
					if (t != null)
					{
						this.callBack(t);
					}
					return false;
				}

				// Token: 0x0400020A RID: 522
				[CompilerGenerated]
				[DebuggerBrowsable(DebuggerBrowsableState.Never)]
				private Action<T> <callBack>k__BackingField;
			}

			// Token: 0x020000B7 RID: 183
			private class DelegateQueryMatcher<TReturnType> : UQuery.UQueryMatcher
			{
				// Token: 0x0600047B RID: 1147 RVA: 0x0000E518 File Offset: 0x0000C718
				public DelegateQueryMatcher()
				{
				}

				// Token: 0x17000119 RID: 281
				// (get) Token: 0x0600047C RID: 1148 RVA: 0x0000F1F4 File Offset: 0x0000D3F4
				// (set) Token: 0x0600047D RID: 1149 RVA: 0x0000F20E File Offset: 0x0000D40E
				public Func<T, TReturnType> callBack
				{
					[CompilerGenerated]
					get
					{
						return this.<callBack>k__BackingField;
					}
					[CompilerGenerated]
					set
					{
						this.<callBack>k__BackingField = value;
					}
				}

				// Token: 0x1700011A RID: 282
				// (get) Token: 0x0600047E RID: 1150 RVA: 0x0000F218 File Offset: 0x0000D418
				// (set) Token: 0x0600047F RID: 1151 RVA: 0x0000F232 File Offset: 0x0000D432
				public List<TReturnType> result
				{
					[CompilerGenerated]
					get
					{
						return this.<result>k__BackingField;
					}
					[CompilerGenerated]
					set
					{
						this.<result>k__BackingField = value;
					}
				}

				// Token: 0x06000480 RID: 1152 RVA: 0x0000F23C File Offset: 0x0000D43C
				protected override bool OnRuleMatchedElement(RuleMatcher matcher, VisualElement element)
				{
					T t = element as T;
					if (t != null)
					{
						this.result.Add(this.callBack(t));
					}
					return false;
				}

				// Token: 0x06000481 RID: 1153 RVA: 0x0000F282 File Offset: 0x0000D482
				// Note: this type is marked as 'beforefieldinit'.
				static DelegateQueryMatcher()
				{
				}

				// Token: 0x0400020B RID: 523
				[DebuggerBrowsable(DebuggerBrowsableState.Never)]
				[CompilerGenerated]
				private Func<T, TReturnType> <callBack>k__BackingField;

				// Token: 0x0400020C RID: 524
				[DebuggerBrowsable(DebuggerBrowsableState.Never)]
				[CompilerGenerated]
				private List<TReturnType> <result>k__BackingField;

				// Token: 0x0400020D RID: 525
				public static UQuery.QueryState<T>.DelegateQueryMatcher<TReturnType> s_Instance = new UQuery.QueryState<T>.DelegateQueryMatcher<TReturnType>();
			}
		}
	}
}
