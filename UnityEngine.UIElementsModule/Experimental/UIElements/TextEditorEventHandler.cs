﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000F2 RID: 242
	internal class TextEditorEventHandler
	{
		// Token: 0x060006C9 RID: 1737 RVA: 0x00017D02 File Offset: 0x00015F02
		protected TextEditorEventHandler(TextEditorEngine editorEngine, ITextInputField textInputField)
		{
			this.editorEngine = editorEngine;
			this.textInputField = textInputField;
			this.textInputField.SyncTextEngine();
		}

		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x060006CA RID: 1738 RVA: 0x00017D24 File Offset: 0x00015F24
		// (set) Token: 0x060006CB RID: 1739 RVA: 0x00017D3E File Offset: 0x00015F3E
		private protected TextEditorEngine editorEngine
		{
			[CompilerGenerated]
			protected get
			{
				return this.<editorEngine>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<editorEngine>k__BackingField = value;
			}
		}

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x060006CC RID: 1740 RVA: 0x00017D48 File Offset: 0x00015F48
		// (set) Token: 0x060006CD RID: 1741 RVA: 0x00017D62 File Offset: 0x00015F62
		private protected ITextInputField textInputField
		{
			[CompilerGenerated]
			protected get
			{
				return this.<textInputField>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<textInputField>k__BackingField = value;
			}
		}

		// Token: 0x060006CE RID: 1742 RVA: 0x000035A6 File Offset: 0x000017A6
		public virtual void ExecuteDefaultActionAtTarget(EventBase evt)
		{
		}

		// Token: 0x060006CF RID: 1743 RVA: 0x00017D6C File Offset: 0x00015F6C
		public virtual void ExecuteDefaultAction(EventBase evt)
		{
			if (evt.GetEventTypeId() == EventBase<FocusEvent>.TypeId())
			{
				this.editorEngine.OnFocus();
				this.editorEngine.SelectAll();
			}
			else if (evt.GetEventTypeId() == EventBase<BlurEvent>.TypeId())
			{
				this.editorEngine.OnLostFocus();
				this.editorEngine.SelectNone();
			}
		}

		// Token: 0x040002B4 RID: 692
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TextEditorEngine <editorEngine>k__BackingField;

		// Token: 0x040002B5 RID: 693
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ITextInputField <textInputField>k__BackingField;
	}
}
