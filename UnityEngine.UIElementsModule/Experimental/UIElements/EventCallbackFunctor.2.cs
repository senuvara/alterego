﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200011B RID: 283
	internal class EventCallbackFunctor<TEventType, TCallbackArgs> : EventCallbackFunctorBase where TEventType : EventBase<TEventType>, new()
	{
		// Token: 0x060007B1 RID: 1969 RVA: 0x0001A5AE File Offset: 0x000187AE
		public EventCallbackFunctor(EventCallback<TEventType, TCallbackArgs> callback, TCallbackArgs userArgs, CallbackPhase phase) : base(phase)
		{
			this.userArgs = userArgs;
			this.m_Callback = callback;
			this.m_EventTypeId = EventBase<TEventType>.TypeId();
		}

		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x060007B2 RID: 1970 RVA: 0x0001A5D4 File Offset: 0x000187D4
		// (set) Token: 0x060007B3 RID: 1971 RVA: 0x0001A5EE File Offset: 0x000187EE
		internal TCallbackArgs userArgs
		{
			[CompilerGenerated]
			get
			{
				return this.<userArgs>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<userArgs>k__BackingField = value;
			}
		}

		// Token: 0x060007B4 RID: 1972 RVA: 0x0001A5F8 File Offset: 0x000187F8
		public override void Invoke(EventBase evt)
		{
			if (evt == null)
			{
				throw new ArgumentNullException();
			}
			if (evt.GetEventTypeId() == this.m_EventTypeId)
			{
				if (base.PhaseMatches(evt))
				{
					this.m_Callback(evt as TEventType, this.userArgs);
				}
			}
		}

		// Token: 0x060007B5 RID: 1973 RVA: 0x0001A654 File Offset: 0x00018854
		public override bool IsEquivalentTo(long eventTypeId, Delegate callback, CallbackPhase phase)
		{
			return this.m_EventTypeId == eventTypeId && this.m_Callback == callback && base.phase == phase;
		}

		// Token: 0x040002FA RID: 762
		private EventCallback<TEventType, TCallbackArgs> m_Callback;

		// Token: 0x040002FB RID: 763
		private long m_EventTypeId;

		// Token: 0x040002FC RID: 764
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private TCallbackArgs <userArgs>k__BackingField;
	}
}
