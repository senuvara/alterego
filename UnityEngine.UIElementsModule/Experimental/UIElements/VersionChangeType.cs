﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200005C RID: 92
	[Flags]
	internal enum VersionChangeType
	{
		// Token: 0x040000E5 RID: 229
		Bindings = 256,
		// Token: 0x040000E6 RID: 230
		PersistentData = 128,
		// Token: 0x040000E7 RID: 231
		Hierarchy = 64,
		// Token: 0x040000E8 RID: 232
		Layout = 32,
		// Token: 0x040000E9 RID: 233
		StyleSheet = 16,
		// Token: 0x040000EA RID: 234
		Styles = 8,
		// Token: 0x040000EB RID: 235
		Transform = 4,
		// Token: 0x040000EC RID: 236
		Clip = 2,
		// Token: 0x040000ED RID: 237
		Repaint = 1
	}
}
