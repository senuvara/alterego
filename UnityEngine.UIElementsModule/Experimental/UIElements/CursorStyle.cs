﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200001A RID: 26
	public struct CursorStyle : IEquatable<CursorStyle>
	{
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000080 RID: 128 RVA: 0x00003984 File Offset: 0x00001B84
		// (set) Token: 0x06000081 RID: 129 RVA: 0x0000399E File Offset: 0x00001B9E
		public Texture2D texture
		{
			[CompilerGenerated]
			get
			{
				return this.<texture>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<texture>k__BackingField = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000082 RID: 130 RVA: 0x000039A8 File Offset: 0x00001BA8
		// (set) Token: 0x06000083 RID: 131 RVA: 0x000039C2 File Offset: 0x00001BC2
		public Vector2 hotspot
		{
			[CompilerGenerated]
			get
			{
				return this.<hotspot>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<hotspot>k__BackingField = value;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000084 RID: 132 RVA: 0x000039CC File Offset: 0x00001BCC
		// (set) Token: 0x06000085 RID: 133 RVA: 0x000039E6 File Offset: 0x00001BE6
		internal int defaultCursorId
		{
			[CompilerGenerated]
			get
			{
				return this.<defaultCursorId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<defaultCursorId>k__BackingField = value;
			}
		}

		// Token: 0x06000086 RID: 134 RVA: 0x000039F0 File Offset: 0x00001BF0
		public bool Equals(CursorStyle other)
		{
			return object.Equals(this.texture, other.texture) && this.hotspot.Equals(other.hotspot) && this.defaultCursorId == other.defaultCursorId;
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00003A48 File Offset: 0x00001C48
		public override bool Equals(object obj)
		{
			return !object.ReferenceEquals(null, obj) && obj is CursorStyle && this.Equals((CursorStyle)obj);
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00003A8C File Offset: 0x00001C8C
		public override int GetHashCode()
		{
			int num = (!(this.texture != null)) ? 0 : this.texture.GetHashCode();
			num = (num * 397 ^ this.hotspot.GetHashCode());
			return num * 397 ^ this.defaultCursorId;
		}

		// Token: 0x04000032 RID: 50
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Texture2D <texture>k__BackingField;

		// Token: 0x04000033 RID: 51
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Vector2 <hotspot>k__BackingField;

		// Token: 0x04000034 RID: 52
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <defaultCursorId>k__BackingField;
	}
}
