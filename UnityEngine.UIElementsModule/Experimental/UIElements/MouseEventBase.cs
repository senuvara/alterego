﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000133 RID: 307
	public abstract class MouseEventBase<T> : EventBase<T>, IMouseEvent, IMouseEventInternal where T : MouseEventBase<T>, new()
	{
		// Token: 0x0600083F RID: 2111 RVA: 0x00019AB0 File Offset: 0x00017CB0
		protected MouseEventBase()
		{
			this.Init();
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x06000840 RID: 2112 RVA: 0x0001B6DC File Offset: 0x000198DC
		// (set) Token: 0x06000841 RID: 2113 RVA: 0x0001B6F6 File Offset: 0x000198F6
		public EventModifiers modifiers
		{
			[CompilerGenerated]
			get
			{
				return this.<modifiers>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<modifiers>k__BackingField = value;
			}
		}

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x06000842 RID: 2114 RVA: 0x0001B700 File Offset: 0x00019900
		// (set) Token: 0x06000843 RID: 2115 RVA: 0x0001B71A File Offset: 0x0001991A
		public Vector2 mousePosition
		{
			[CompilerGenerated]
			get
			{
				return this.<mousePosition>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<mousePosition>k__BackingField = value;
			}
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x06000844 RID: 2116 RVA: 0x0001B724 File Offset: 0x00019924
		// (set) Token: 0x06000845 RID: 2117 RVA: 0x0001B73E File Offset: 0x0001993E
		public Vector2 localMousePosition
		{
			[CompilerGenerated]
			get
			{
				return this.<localMousePosition>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<localMousePosition>k__BackingField = value;
			}
		}

		// Token: 0x17000225 RID: 549
		// (get) Token: 0x06000846 RID: 2118 RVA: 0x0001B748 File Offset: 0x00019948
		// (set) Token: 0x06000847 RID: 2119 RVA: 0x0001B762 File Offset: 0x00019962
		public Vector2 mouseDelta
		{
			[CompilerGenerated]
			get
			{
				return this.<mouseDelta>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<mouseDelta>k__BackingField = value;
			}
		}

		// Token: 0x17000226 RID: 550
		// (get) Token: 0x06000848 RID: 2120 RVA: 0x0001B76C File Offset: 0x0001996C
		// (set) Token: 0x06000849 RID: 2121 RVA: 0x0001B786 File Offset: 0x00019986
		public int clickCount
		{
			[CompilerGenerated]
			get
			{
				return this.<clickCount>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<clickCount>k__BackingField = value;
			}
		}

		// Token: 0x17000227 RID: 551
		// (get) Token: 0x0600084A RID: 2122 RVA: 0x0001B790 File Offset: 0x00019990
		// (set) Token: 0x0600084B RID: 2123 RVA: 0x0001B7AA File Offset: 0x000199AA
		public int button
		{
			[CompilerGenerated]
			get
			{
				return this.<button>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<button>k__BackingField = value;
			}
		}

		// Token: 0x17000228 RID: 552
		// (get) Token: 0x0600084C RID: 2124 RVA: 0x0001B7B4 File Offset: 0x000199B4
		public bool shiftKey
		{
			get
			{
				return (this.modifiers & EventModifiers.Shift) != EventModifiers.None;
			}
		}

		// Token: 0x17000229 RID: 553
		// (get) Token: 0x0600084D RID: 2125 RVA: 0x0001B7D8 File Offset: 0x000199D8
		public bool ctrlKey
		{
			get
			{
				return (this.modifiers & EventModifiers.Control) != EventModifiers.None;
			}
		}

		// Token: 0x1700022A RID: 554
		// (get) Token: 0x0600084E RID: 2126 RVA: 0x0001B7FC File Offset: 0x000199FC
		public bool commandKey
		{
			get
			{
				return (this.modifiers & EventModifiers.Command) != EventModifiers.None;
			}
		}

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x0600084F RID: 2127 RVA: 0x0001B820 File Offset: 0x00019A20
		public bool altKey
		{
			get
			{
				return (this.modifiers & EventModifiers.Alt) != EventModifiers.None;
			}
		}

		// Token: 0x1700022C RID: 556
		// (get) Token: 0x06000850 RID: 2128 RVA: 0x0001B844 File Offset: 0x00019A44
		public bool actionKey
		{
			get
			{
				bool result;
				if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
				{
					result = this.commandKey;
				}
				else
				{
					result = this.ctrlKey;
				}
				return result;
			}
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x06000851 RID: 2129 RVA: 0x0001B884 File Offset: 0x00019A84
		// (set) Token: 0x06000852 RID: 2130 RVA: 0x0001B89E File Offset: 0x00019A9E
		bool IMouseEventInternal.hasUnderlyingPhysicalEvent
		{
			[CompilerGenerated]
			get
			{
				return this.<UnityEngine.Experimental.UIElements.IMouseEventInternal.hasUnderlyingPhysicalEvent>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<UnityEngine.Experimental.UIElements.IMouseEventInternal.hasUnderlyingPhysicalEvent>k__BackingField = value;
			}
		}

		// Token: 0x06000853 RID: 2131 RVA: 0x0001B8A8 File Offset: 0x00019AA8
		protected override void Init()
		{
			base.Init();
			base.flags = (EventBase.EventFlags.Bubbles | EventBase.EventFlags.TricklesDown | EventBase.EventFlags.Cancellable);
			this.modifiers = EventModifiers.None;
			this.mousePosition = Vector2.zero;
			this.localMousePosition = Vector2.zero;
			this.mouseDelta = Vector2.zero;
			this.clickCount = 0;
			this.button = 0;
			((IMouseEventInternal)this).hasUnderlyingPhysicalEvent = false;
		}

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x06000854 RID: 2132 RVA: 0x0001B900 File Offset: 0x00019B00
		// (set) Token: 0x06000855 RID: 2133 RVA: 0x0001B91C File Offset: 0x00019B1C
		public override IEventHandler currentTarget
		{
			get
			{
				return base.currentTarget;
			}
			internal set
			{
				base.currentTarget = value;
				VisualElement visualElement = this.currentTarget as VisualElement;
				if (visualElement != null)
				{
					this.localMousePosition = visualElement.WorldToLocal(this.mousePosition);
				}
			}
		}

		// Token: 0x06000856 RID: 2134 RVA: 0x0001B958 File Offset: 0x00019B58
		public static T GetPooled(Event systemEvent)
		{
			T pooled = EventBase<T>.GetPooled();
			pooled.imguiEvent = systemEvent;
			if (systemEvent != null)
			{
				pooled.modifiers = systemEvent.modifiers;
				pooled.mousePosition = systemEvent.mousePosition;
				pooled.localMousePosition = systemEvent.mousePosition;
				pooled.mouseDelta = systemEvent.delta;
				pooled.button = systemEvent.button;
				pooled.clickCount = systemEvent.clickCount;
				pooled.hasUnderlyingPhysicalEvent = true;
			}
			return pooled;
		}

		// Token: 0x06000857 RID: 2135 RVA: 0x0001BA08 File Offset: 0x00019C08
		public static T GetPooled(Vector2 mousePosition)
		{
			T pooled = EventBase<T>.GetPooled();
			pooled.mousePosition = mousePosition;
			return pooled;
		}

		// Token: 0x06000858 RID: 2136 RVA: 0x0001BA34 File Offset: 0x00019C34
		public static T GetPooled(IMouseEvent triggerEvent)
		{
			T pooled = EventBase<T>.GetPooled();
			if (triggerEvent != null)
			{
				pooled.modifiers = triggerEvent.modifiers;
				pooled.mousePosition = triggerEvent.mousePosition;
				pooled.localMousePosition = triggerEvent.mousePosition;
				pooled.mouseDelta = triggerEvent.mouseDelta;
				pooled.button = triggerEvent.button;
				pooled.clickCount = triggerEvent.clickCount;
				IMouseEventInternal mouseEventInternal = triggerEvent as IMouseEventInternal;
				if (mouseEventInternal != null)
				{
					pooled.hasUnderlyingPhysicalEvent = mouseEventInternal.hasUnderlyingPhysicalEvent;
				}
			}
			return pooled;
		}

		// Token: 0x0400031B RID: 795
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <UnityEngine.Experimental.UIElements.IMouseEventInternal.hasUnderlyingPhysicalEvent>k__BackingField;

		// Token: 0x0400031C RID: 796
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private EventModifiers <modifiers>k__BackingField;

		// Token: 0x0400031D RID: 797
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector2 <mousePosition>k__BackingField;

		// Token: 0x0400031E RID: 798
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector2 <localMousePosition>k__BackingField;

		// Token: 0x0400031F RID: 799
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector2 <mouseDelta>k__BackingField;

		// Token: 0x04000320 RID: 800
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <clickCount>k__BackingField;

		// Token: 0x04000321 RID: 801
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <button>k__BackingField;
	}
}
