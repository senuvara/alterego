﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Experimental.UIElements.StyleEnums;
using UnityEngine.Internal;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000034 RID: 52
	public class Foldout : BindableElement, INotifyValueChanged<bool>
	{
		// Token: 0x060000F3 RID: 243 RVA: 0x00005814 File Offset: 0x00003A14
		public Foldout()
		{
			this.m_Toggle = new Toggle();
			this.m_Toggle.value = true;
			this.m_Toggle.OnValueChanged(delegate(ChangeEvent<bool> evt)
			{
				this.value = this.m_Toggle.value;
				evt.StopPropagation();
			});
			this.m_Toggle.AddToClassList(Foldout.s_ToggleClassName);
			base.shadow.Add(this.m_Toggle);
			this.m_Container = new VisualElement();
			this.m_Container.clippingOptions = VisualElement.ClippingOptions.ClipContents;
			this.m_Container.AddToClassList(Foldout.s_ContentContainerClassName);
			base.shadow.Add(this.m_Container);
			base.AddToClassList(Foldout.s_FoldoutClassName);
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060000F4 RID: 244 RVA: 0x000058C8 File Offset: 0x00003AC8
		public override VisualElement contentContainer
		{
			get
			{
				return this.m_Container;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000F5 RID: 245 RVA: 0x000058E4 File Offset: 0x00003AE4
		// (set) Token: 0x060000F6 RID: 246 RVA: 0x00005904 File Offset: 0x00003B04
		public string text
		{
			get
			{
				return this.m_Toggle.text;
			}
			set
			{
				this.m_Toggle.text = value;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x00005914 File Offset: 0x00003B14
		// (set) Token: 0x060000F8 RID: 248 RVA: 0x00005930 File Offset: 0x00003B30
		public bool value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				if (this.m_Value != value)
				{
					using (ChangeEvent<bool> pooled = ChangeEvent<bool>.GetPooled(this.m_Value, value))
					{
						pooled.target = this;
						this.SetValueWithoutNotify(value);
						this.SendEvent(pooled);
					}
				}
			}
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x00005998 File Offset: 0x00003B98
		[ExcludeFromDocs]
		public void SetValueAndNotify(bool newValue)
		{
			this.value = newValue;
		}

		// Token: 0x060000FA RID: 250 RVA: 0x000059A4 File Offset: 0x00003BA4
		public void SetValueWithoutNotify(bool newValue)
		{
			this.m_Value = newValue;
			this.m_Toggle.value = this.m_Value;
			if (this.m_Value)
			{
				this.contentContainer.visible = true;
				this.contentContainer.style.positionType = PositionType.Relative;
			}
			else
			{
				this.contentContainer.visible = false;
				this.contentContainer.style.positionType = PositionType.Absolute;
			}
		}

		// Token: 0x060000FB RID: 251 RVA: 0x00005A22 File Offset: 0x00003C22
		public void OnValueChanged(EventCallback<ChangeEvent<bool>> callback)
		{
			base.RegisterCallback<ChangeEvent<bool>>(callback, TrickleDown.NoTrickleDown);
		}

		// Token: 0x060000FC RID: 252 RVA: 0x00005A2D File Offset: 0x00003C2D
		public void RemoveOnValueChanged(EventCallback<ChangeEvent<bool>> callback)
		{
			base.UnregisterCallback<ChangeEvent<bool>>(callback, TrickleDown.NoTrickleDown);
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00005A38 File Offset: 0x00003C38
		// Note: this type is marked as 'beforefieldinit'.
		static Foldout()
		{
		}

		// Token: 0x060000FE RID: 254 RVA: 0x00005A58 File Offset: 0x00003C58
		[CompilerGenerated]
		private void <Foldout>m__0(ChangeEvent<bool> evt)
		{
			this.value = this.m_Toggle.value;
			evt.StopPropagation();
		}

		// Token: 0x04000076 RID: 118
		private static readonly string s_FoldoutClassName = "unity-foldout";

		// Token: 0x04000077 RID: 119
		private static readonly string s_ToggleClassName = "unity-foldout-toggle";

		// Token: 0x04000078 RID: 120
		private static readonly string s_ContentContainerClassName = "unity-foldout-content";

		// Token: 0x04000079 RID: 121
		private Toggle m_Toggle;

		// Token: 0x0400007A RID: 122
		private VisualElement m_Container;

		// Token: 0x0400007B RID: 123
		private bool m_Value = true;

		// Token: 0x02000035 RID: 53
		public new class UxmlFactory : UxmlFactory<Foldout, BindableElement.UxmlTraits>
		{
			// Token: 0x060000FF RID: 255 RVA: 0x00005A72 File Offset: 0x00003C72
			public UxmlFactory()
			{
			}
		}
	}
}
