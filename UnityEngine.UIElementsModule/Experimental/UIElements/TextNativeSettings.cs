﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000A5 RID: 165
	internal struct TextNativeSettings
	{
		// Token: 0x040001D8 RID: 472
		public string text;

		// Token: 0x040001D9 RID: 473
		public Font font;

		// Token: 0x040001DA RID: 474
		public int size;

		// Token: 0x040001DB RID: 475
		public float scaling;

		// Token: 0x040001DC RID: 476
		public FontStyle style;

		// Token: 0x040001DD RID: 477
		public Color color;

		// Token: 0x040001DE RID: 478
		public TextAnchor anchor;

		// Token: 0x040001DF RID: 479
		public bool wordWrap;

		// Token: 0x040001E0 RID: 480
		public float wordWrapWidth;

		// Token: 0x040001E1 RID: 481
		public bool richText;
	}
}
