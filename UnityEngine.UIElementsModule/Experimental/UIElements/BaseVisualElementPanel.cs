﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200005F RID: 95
	internal abstract class BaseVisualElementPanel : IPanel, IDisposable
	{
		// Token: 0x06000270 RID: 624 RVA: 0x0000978B File Offset: 0x0000798B
		protected BaseVisualElementPanel()
		{
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x06000271 RID: 625
		// (set) Token: 0x06000272 RID: 626
		public abstract EventInterests IMGUIEventInterests { get; set; }

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x06000273 RID: 627
		// (set) Token: 0x06000274 RID: 628
		public abstract ScriptableObject ownerObject { get; protected set; }

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x06000275 RID: 629
		// (set) Token: 0x06000276 RID: 630
		public abstract SavePersistentViewData savePersistentViewData { get; set; }

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x06000277 RID: 631
		// (set) Token: 0x06000278 RID: 632
		public abstract GetViewDataDictionary getViewDataDictionary { get; set; }

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000279 RID: 633
		// (set) Token: 0x0600027A RID: 634
		public abstract int IMGUIContainersCount { get; set; }

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x0600027B RID: 635
		// (set) Token: 0x0600027C RID: 636
		public abstract FocusController focusController { get; set; }

		// Token: 0x0600027D RID: 637 RVA: 0x0000979E File Offset: 0x0000799E
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600027E RID: 638 RVA: 0x000097AE File Offset: 0x000079AE
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					UIElementsUtility.RemoveCachedPanel(this.ownerObject.GetInstanceID());
				}
				this.disposed = true;
			}
		}

		// Token: 0x0600027F RID: 639
		public abstract void Repaint(Event e);

		// Token: 0x06000280 RID: 640
		public abstract void ValidateLayout();

		// Token: 0x06000281 RID: 641
		public abstract void UpdateBindings();

		// Token: 0x06000282 RID: 642
		public abstract void ApplyStyles();

		// Token: 0x06000283 RID: 643
		public abstract void DirtyStyleSheets();

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000284 RID: 644 RVA: 0x000097E4 File Offset: 0x000079E4
		// (set) Token: 0x06000285 RID: 645 RVA: 0x000097FE File Offset: 0x000079FE
		internal float currentPixelsPerPoint
		{
			[CompilerGenerated]
			get
			{
				return this.<currentPixelsPerPoint>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<currentPixelsPerPoint>k__BackingField = value;
			}
		} = 1f;

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000286 RID: 646 RVA: 0x00009808 File Offset: 0x00007A08
		internal bool isDirty
		{
			get
			{
				return this.version != this.repaintVersion;
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x06000287 RID: 647
		internal abstract uint version { get; }

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x06000288 RID: 648
		internal abstract uint repaintVersion { get; }

		// Token: 0x06000289 RID: 649
		internal abstract void OnVersionChanged(VisualElement ele, VersionChangeType changeTypeFlag);

		// Token: 0x0600028A RID: 650
		internal abstract void SetUpdater(IVisualTreeUpdater updater, VisualTreeUpdatePhase phase);

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x0600028B RID: 651 RVA: 0x00009830 File Offset: 0x00007A30
		// (set) Token: 0x0600028C RID: 652 RVA: 0x0000984A File Offset: 0x00007A4A
		internal virtual RepaintData repaintData
		{
			[CompilerGenerated]
			get
			{
				return this.<repaintData>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<repaintData>k__BackingField = value;
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x0600028D RID: 653 RVA: 0x00009854 File Offset: 0x00007A54
		// (set) Token: 0x0600028E RID: 654 RVA: 0x0000986E File Offset: 0x00007A6E
		internal virtual ICursorManager cursorManager
		{
			[CompilerGenerated]
			get
			{
				return this.<cursorManager>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<cursorManager>k__BackingField = value;
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x0600028F RID: 655 RVA: 0x00009878 File Offset: 0x00007A78
		// (set) Token: 0x06000290 RID: 656 RVA: 0x00009892 File Offset: 0x00007A92
		internal virtual ContextualMenuManager contextualMenuManager
		{
			[CompilerGenerated]
			get
			{
				return this.<contextualMenuManager>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<contextualMenuManager>k__BackingField = value;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000291 RID: 657
		public abstract VisualElement visualTree { get; }

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000292 RID: 658
		// (set) Token: 0x06000293 RID: 659
		public abstract EventDispatcher dispatcher { get; protected set; }

		// Token: 0x06000294 RID: 660 RVA: 0x0000989B File Offset: 0x00007A9B
		internal void SendEvent(EventBase e, DispatchMode dispatchMode = DispatchMode.Default)
		{
			Debug.Assert(this.dispatcher != null);
			EventDispatcher dispatcher = this.dispatcher;
			if (dispatcher != null)
			{
				dispatcher.Dispatch(e, this, dispatchMode);
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x06000295 RID: 661
		internal abstract IScheduler scheduler { get; }

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x06000296 RID: 662
		internal abstract IDataWatchService dataWatch { get; }

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000297 RID: 663
		// (set) Token: 0x06000298 RID: 664
		public abstract ContextType contextType { get; protected set; }

		// Token: 0x06000299 RID: 665
		public abstract VisualElement Pick(Vector2 point);

		// Token: 0x0600029A RID: 666
		public abstract VisualElement PickAll(Vector2 point, List<VisualElement> picked);

		// Token: 0x0600029B RID: 667
		public abstract VisualElement LoadTemplate(string path, Dictionary<string, VisualElement> slots = null);

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x0600029C RID: 668 RVA: 0x000098C8 File Offset: 0x00007AC8
		// (set) Token: 0x0600029D RID: 669 RVA: 0x000098E2 File Offset: 0x00007AE2
		internal bool disposed
		{
			[CompilerGenerated]
			get
			{
				return this.<disposed>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<disposed>k__BackingField = value;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x0600029E RID: 670 RVA: 0x000098EC File Offset: 0x00007AEC
		// (set) Token: 0x0600029F RID: 671 RVA: 0x00009906 File Offset: 0x00007B06
		internal bool allowPixelCaching
		{
			[CompilerGenerated]
			get
			{
				return this.<allowPixelCaching>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<allowPixelCaching>k__BackingField = value;
			}
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x060002A0 RID: 672
		// (set) Token: 0x060002A1 RID: 673
		public abstract bool keepPixelCacheOnWorldBoundChange { get; set; }

		// Token: 0x060002A2 RID: 674
		internal abstract IVisualTreeUpdater GetUpdater(VisualTreeUpdatePhase phase);

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x060002A3 RID: 675 RVA: 0x00009910 File Offset: 0x00007B10
		// (set) Token: 0x060002A4 RID: 676 RVA: 0x0000992A File Offset: 0x00007B2A
		internal VisualElement topElementUnderMouse
		{
			[CompilerGenerated]
			get
			{
				return this.<topElementUnderMouse>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<topElementUnderMouse>k__BackingField = value;
			}
		}

		// Token: 0x040000F2 RID: 242
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private float <currentPixelsPerPoint>k__BackingField;

		// Token: 0x040000F3 RID: 243
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private RepaintData <repaintData>k__BackingField;

		// Token: 0x040000F4 RID: 244
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private ICursorManager <cursorManager>k__BackingField;

		// Token: 0x040000F5 RID: 245
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private ContextualMenuManager <contextualMenuManager>k__BackingField;

		// Token: 0x040000F6 RID: 246
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <disposed>k__BackingField;

		// Token: 0x040000F7 RID: 247
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <allowPixelCaching>k__BackingField;

		// Token: 0x040000F8 RID: 248
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private VisualElement <topElementUnderMouse>k__BackingField;
	}
}
