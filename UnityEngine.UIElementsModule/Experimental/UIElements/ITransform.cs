﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000040 RID: 64
	public interface ITransform
	{
		// Token: 0x1700007F RID: 127
		// (get) Token: 0x060001B2 RID: 434
		// (set) Token: 0x060001B3 RID: 435
		Vector3 position { get; set; }

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x060001B4 RID: 436
		// (set) Token: 0x060001B5 RID: 437
		Quaternion rotation { get; set; }

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x060001B6 RID: 438
		// (set) Token: 0x060001B7 RID: 439
		Vector3 scale { get; set; }

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x060001B8 RID: 440
		Matrix4x4 matrix { get; }
	}
}
