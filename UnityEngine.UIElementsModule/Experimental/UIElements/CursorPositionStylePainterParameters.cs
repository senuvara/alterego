﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200009B RID: 155
	internal struct CursorPositionStylePainterParameters
	{
		// Token: 0x060003CF RID: 975 RVA: 0x0000D2FC File Offset: 0x0000B4FC
		public static CursorPositionStylePainterParameters GetDefault(VisualElement ve, string text)
		{
			IStyle style = ve.style;
			return new CursorPositionStylePainterParameters
			{
				rect = ve.contentRect,
				text = text,
				font = style.font,
				fontSize = style.fontSize,
				fontStyle = style.fontStyleAndWeight,
				anchor = style.unityTextAlign,
				wordWrapWidth = ((!style.wordWrap) ? 0f : ve.contentRect.width),
				richText = false,
				cursorIndex = 0
			};
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x0000D3C4 File Offset: 0x0000B5C4
		internal TextNativeSettings GetTextNativeSettings(float scaling)
		{
			return new TextNativeSettings
			{
				text = this.text,
				font = this.font,
				size = this.fontSize,
				scaling = scaling,
				style = this.fontStyle,
				color = Color.white,
				anchor = this.anchor,
				wordWrap = true,
				wordWrapWidth = this.wordWrapWidth,
				richText = this.richText
			};
		}

		// Token: 0x040001C3 RID: 451
		public Rect rect;

		// Token: 0x040001C4 RID: 452
		public string text;

		// Token: 0x040001C5 RID: 453
		public Font font;

		// Token: 0x040001C6 RID: 454
		public int fontSize;

		// Token: 0x040001C7 RID: 455
		public FontStyle fontStyle;

		// Token: 0x040001C8 RID: 456
		public TextAnchor anchor;

		// Token: 0x040001C9 RID: 457
		public float wordWrapWidth;

		// Token: 0x040001CA RID: 458
		public bool richText;

		// Token: 0x040001CB RID: 459
		public int cursorIndex;
	}
}
