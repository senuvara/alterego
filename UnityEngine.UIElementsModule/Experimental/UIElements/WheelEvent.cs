﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000138 RID: 312
	public class WheelEvent : MouseEventBase<WheelEvent>
	{
		// Token: 0x0600085D RID: 2141 RVA: 0x0001BB0A File Offset: 0x00019D0A
		public WheelEvent()
		{
			this.Init();
		}

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x0600085E RID: 2142 RVA: 0x0001BB1C File Offset: 0x00019D1C
		// (set) Token: 0x0600085F RID: 2143 RVA: 0x0001BB36 File Offset: 0x00019D36
		public Vector3 delta
		{
			[CompilerGenerated]
			get
			{
				return this.<delta>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<delta>k__BackingField = value;
			}
		}

		// Token: 0x06000860 RID: 2144 RVA: 0x0001BB40 File Offset: 0x00019D40
		public new static WheelEvent GetPooled(Event systemEvent)
		{
			WheelEvent pooled = MouseEventBase<WheelEvent>.GetPooled(systemEvent);
			pooled.imguiEvent = systemEvent;
			if (systemEvent != null)
			{
				pooled.delta = systemEvent.delta;
			}
			return pooled;
		}

		// Token: 0x06000861 RID: 2145 RVA: 0x0001BB7D File Offset: 0x00019D7D
		protected override void Init()
		{
			base.Init();
			this.delta = Vector3.zero;
		}

		// Token: 0x04000322 RID: 802
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector3 <delta>k__BackingField;
	}
}
