﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000025 RID: 37
	public enum PropagationPhase
	{
		// Token: 0x0400004C RID: 76
		None,
		// Token: 0x0400004D RID: 77
		TrickleDown,
		// Token: 0x0400004E RID: 78
		[Obsolete("Use TrickleDown instead of Capture.")]
		Capture = 1,
		// Token: 0x0400004F RID: 79
		AtTarget,
		// Token: 0x04000050 RID: 80
		BubbleUp,
		// Token: 0x04000051 RID: 81
		DefaultAction
	}
}
