﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Experimental.UIElements.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000FA RID: 250
	public abstract class TextInputFieldBase<T> : BaseField<T>, ITextInputField, IEventHandler, ITextElement
	{
		// Token: 0x060006F5 RID: 1781 RVA: 0x000181C4 File Offset: 0x000163C4
		public TextInputFieldBase(int maxLength, char maskChar)
		{
			base.requireMeasureFunction = true;
			this.m_Text = "";
			this.maxLength = maxLength;
			this.maskChar = maskChar;
			this.editorEngine = new TextEditorEngine(new TextEditorEngine.OnDetectFocusChangeFunction(this.OnDetectFocusChange), new TextEditorEngine.OnIndexChangeFunction(this.OnCursorIndexChange));
			if (this.touchScreenTextField)
			{
				this.editorEventHandler = new TouchScreenTextEditorEventHandler(this.editorEngine, this);
			}
			else
			{
				this.doubleClickSelectsWord = true;
				this.tripleClickSelectsLine = true;
				this.editorEventHandler = new KeyboardTextEditorEventHandler(this.editorEngine, this);
			}
			this.editorEngine.style = new GUIStyle(this.editorEngine.style);
		}

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x060006F6 RID: 1782 RVA: 0x0001827C File Offset: 0x0001647C
		// (set) Token: 0x060006F7 RID: 1783 RVA: 0x00018297 File Offset: 0x00016497
		public string text
		{
			get
			{
				return this.m_Text;
			}
			protected set
			{
				if (!(this.m_Text == value))
				{
					this.m_Text = value;
					this.editorEngine.text = value;
					base.IncrementVersion(VersionChangeType.Layout);
				}
			}
		}

		// Token: 0x060006F8 RID: 1784 RVA: 0x000182CB File Offset: 0x000164CB
		public void SelectAll()
		{
			if (this.editorEngine != null)
			{
				this.editorEngine.SelectAll();
			}
		}

		// Token: 0x060006F9 RID: 1785 RVA: 0x000182E8 File Offset: 0x000164E8
		private void UpdateText(string value)
		{
			if (this.text != value)
			{
				using (InputEvent pooled = InputEvent.GetPooled(this.text, value))
				{
					pooled.target = this;
					this.text = value;
					this.SendEvent(pooled);
				}
			}
		}

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x060006FA RID: 1786 RVA: 0x00018350 File Offset: 0x00016550
		// (set) Token: 0x060006FB RID: 1787 RVA: 0x0001836A File Offset: 0x0001656A
		public virtual bool isPasswordField
		{
			[CompilerGenerated]
			get
			{
				return this.<isPasswordField>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<isPasswordField>k__BackingField = value;
			}
		}

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x060006FC RID: 1788 RVA: 0x00018374 File Offset: 0x00016574
		public Color selectionColor
		{
			get
			{
				return this.m_SelectionColor.GetSpecifiedValueOrDefault(Color.clear);
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x060006FD RID: 1789 RVA: 0x0001839C File Offset: 0x0001659C
		public Color cursorColor
		{
			get
			{
				return this.m_CursorColor.GetSpecifiedValueOrDefault(Color.clear);
			}
		}

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x060006FE RID: 1790 RVA: 0x000183C4 File Offset: 0x000165C4
		public int cursorIndex
		{
			get
			{
				return this.editorEngine.cursorIndex;
			}
		}

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x060006FF RID: 1791 RVA: 0x000183E4 File Offset: 0x000165E4
		public int selectIndex
		{
			get
			{
				return this.editorEngine.selectIndex;
			}
		}

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000700 RID: 1792 RVA: 0x00018404 File Offset: 0x00016604
		// (set) Token: 0x06000701 RID: 1793 RVA: 0x0001841E File Offset: 0x0001661E
		public int maxLength
		{
			[CompilerGenerated]
			get
			{
				return this.<maxLength>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<maxLength>k__BackingField = value;
			}
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06000702 RID: 1794 RVA: 0x00018428 File Offset: 0x00016628
		// (set) Token: 0x06000703 RID: 1795 RVA: 0x00018442 File Offset: 0x00016642
		public bool doubleClickSelectsWord
		{
			[CompilerGenerated]
			get
			{
				return this.<doubleClickSelectsWord>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<doubleClickSelectsWord>k__BackingField = value;
			}
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x06000704 RID: 1796 RVA: 0x0001844C File Offset: 0x0001664C
		// (set) Token: 0x06000705 RID: 1797 RVA: 0x00018466 File Offset: 0x00016666
		public bool tripleClickSelectsLine
		{
			[CompilerGenerated]
			get
			{
				return this.<tripleClickSelectsLine>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<tripleClickSelectsLine>k__BackingField = value;
			}
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x06000706 RID: 1798 RVA: 0x00018470 File Offset: 0x00016670
		// (set) Token: 0x06000707 RID: 1799 RVA: 0x0001848A File Offset: 0x0001668A
		public bool isDelayed
		{
			[CompilerGenerated]
			get
			{
				return this.<isDelayed>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<isDelayed>k__BackingField = value;
			}
		}

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x06000708 RID: 1800 RVA: 0x00018494 File Offset: 0x00016694
		private bool touchScreenTextField
		{
			get
			{
				return TouchScreenKeyboard.isSupported;
			}
		}

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x06000709 RID: 1801 RVA: 0x000184B0 File Offset: 0x000166B0
		internal bool hasFocus
		{
			get
			{
				return base.elementPanel != null && base.elementPanel.focusController.focusedElement == this;
			}
		}

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x0600070A RID: 1802 RVA: 0x000184E8 File Offset: 0x000166E8
		// (set) Token: 0x0600070B RID: 1803 RVA: 0x00018502 File Offset: 0x00016702
		internal TextEditorEventHandler editorEventHandler
		{
			[CompilerGenerated]
			get
			{
				return this.<editorEventHandler>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<editorEventHandler>k__BackingField = value;
			}
		}

		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x0600070C RID: 1804 RVA: 0x0001850C File Offset: 0x0001670C
		// (set) Token: 0x0600070D RID: 1805 RVA: 0x00018526 File Offset: 0x00016726
		internal TextEditorEngine editorEngine
		{
			[CompilerGenerated]
			get
			{
				return this.<editorEngine>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<editorEngine>k__BackingField = value;
			}
		}

		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x0600070E RID: 1806 RVA: 0x00018530 File Offset: 0x00016730
		// (set) Token: 0x0600070F RID: 1807 RVA: 0x0001854A File Offset: 0x0001674A
		public char maskChar
		{
			[CompilerGenerated]
			get
			{
				return this.<maskChar>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<maskChar>k__BackingField = value;
			}
		}

		// Token: 0x06000710 RID: 1808 RVA: 0x00018554 File Offset: 0x00016754
		private DropdownMenu.MenuAction.StatusFlags CutCopyActionStatus(DropdownMenu.MenuAction a)
		{
			return (!this.editorEngine.hasSelection || this.isPasswordField) ? DropdownMenu.MenuAction.StatusFlags.Disabled : DropdownMenu.MenuAction.StatusFlags.Normal;
		}

		// Token: 0x06000711 RID: 1809 RVA: 0x0001858C File Offset: 0x0001678C
		private DropdownMenu.MenuAction.StatusFlags PasteActionStatus(DropdownMenu.MenuAction a)
		{
			return (!this.editorEngine.CanPaste()) ? DropdownMenu.MenuAction.StatusFlags.Disabled : DropdownMenu.MenuAction.StatusFlags.Normal;
		}

		// Token: 0x06000712 RID: 1810 RVA: 0x000185B8 File Offset: 0x000167B8
		private void Cut(DropdownMenu.MenuAction a)
		{
			this.editorEngine.Cut();
			this.editorEngine.text = this.CullString(this.editorEngine.text);
			this.UpdateText(this.editorEngine.text);
		}

		// Token: 0x06000713 RID: 1811 RVA: 0x000185F4 File Offset: 0x000167F4
		private void Copy(DropdownMenu.MenuAction a)
		{
			this.editorEngine.Copy();
		}

		// Token: 0x06000714 RID: 1812 RVA: 0x00018602 File Offset: 0x00016802
		private void Paste(DropdownMenu.MenuAction a)
		{
			this.editorEngine.Paste();
			this.editorEngine.text = this.CullString(this.editorEngine.text);
			this.UpdateText(this.editorEngine.text);
		}

		// Token: 0x06000715 RID: 1813 RVA: 0x00018640 File Offset: 0x00016840
		protected override void OnStyleResolved(ICustomStyle style)
		{
			base.OnStyleResolved(style);
			base.effectiveStyle.ApplyCustomProperty("selection-color", ref this.m_SelectionColor);
			base.effectiveStyle.ApplyCustomProperty("cursor-color", ref this.m_CursorColor);
			base.effectiveStyle.WriteToGUIStyle(this.editorEngine.style);
		}

		// Token: 0x06000716 RID: 1814 RVA: 0x00018698 File Offset: 0x00016898
		internal virtual void SyncTextEngine()
		{
			this.editorEngine.text = this.CullString(this.text);
			this.editorEngine.SaveBackup();
			this.editorEngine.position = base.layout;
			this.editorEngine.DetectFocusChange();
		}

		// Token: 0x06000717 RID: 1815 RVA: 0x000186E4 File Offset: 0x000168E4
		internal string CullString(string s)
		{
			string result;
			if (this.maxLength >= 0 && s != null && s.Length > this.maxLength)
			{
				result = s.Substring(0, this.maxLength);
			}
			else
			{
				result = s;
			}
			return result;
		}

		// Token: 0x06000718 RID: 1816 RVA: 0x00018730 File Offset: 0x00016930
		protected override void DoRepaint(IStylePainter painter)
		{
			IStylePainterInternal stylePainterInternal = (IStylePainterInternal)painter;
			if (this.touchScreenTextField)
			{
				TouchScreenTextEditorEventHandler touchScreenTextEditorEventHandler = this.editorEventHandler as TouchScreenTextEditorEventHandler;
				if (touchScreenTextEditorEventHandler != null && this.editorEngine.keyboardOnScreen != null)
				{
					this.UpdateText(this.CullString(this.editorEngine.keyboardOnScreen.text));
					if (this.editorEngine.keyboardOnScreen.status != TouchScreenKeyboard.Status.Visible)
					{
						this.editorEngine.keyboardOnScreen = null;
						GUI.changed = true;
					}
				}
				string text = this.text;
				if (touchScreenTextEditorEventHandler != null && !string.IsNullOrEmpty(touchScreenTextEditorEventHandler.secureText))
				{
					text = "".PadRight(touchScreenTextEditorEventHandler.secureText.Length, this.maskChar);
				}
				this.text = text;
			}
			else if (!this.hasFocus)
			{
				stylePainterInternal.DrawText(this.text);
			}
			else
			{
				this.DrawWithTextSelectionAndCursor(stylePainterInternal, this.text);
			}
		}

		// Token: 0x06000719 RID: 1817 RVA: 0x00018830 File Offset: 0x00016A30
		internal void DrawWithTextSelectionAndCursor(IStylePainterInternal painter, string newText)
		{
			KeyboardTextEditorEventHandler keyboardTextEditorEventHandler = this.editorEventHandler as KeyboardTextEditorEventHandler;
			if (keyboardTextEditorEventHandler != null)
			{
				keyboardTextEditorEventHandler.PreDrawCursor(newText);
				int cursorIndex = this.editorEngine.cursorIndex;
				int selectIndex = this.editorEngine.selectIndex;
				Rect localPosition = this.editorEngine.localPosition;
				Vector2 scrollOffset = this.editorEngine.scrollOffset;
				IStyle style = base.style;
				float scaling = TextNative.ComputeTextScaling(base.worldTransform, GUIUtility.pixelsPerPoint);
				TextStylePainterParameters @default = TextStylePainterParameters.GetDefault(this, this.text);
				@default.text = " ";
				@default.wordWrapWidth = 0f;
				@default.wordWrap = false;
				TextNativeSettings textNativeSettings = @default.GetTextNativeSettings(scaling);
				float num = TextNative.ComputeTextHeight(textNativeSettings);
				float num2 = (!this.editorEngine.multiline) ? 0f : base.contentRect.width;
				Input.compositionCursorPos = this.editorEngine.graphicalCursorPos - scrollOffset + new Vector2(localPosition.x, localPosition.y + num);
				Color specifiedValueOrDefault = this.m_CursorColor.GetSpecifiedValueOrDefault(Color.grey);
				int num3 = (!string.IsNullOrEmpty(Input.compositionString)) ? (cursorIndex + Input.compositionString.Length) : selectIndex;
				if (cursorIndex != num3)
				{
					RectStylePainterParameters default2 = RectStylePainterParameters.GetDefault(this);
					default2.color = this.selectionColor;
					default2.border.SetWidth(0f);
					default2.border.SetRadius(0f);
					int cursorIndex2 = (cursorIndex >= num3) ? num3 : cursorIndex;
					int cursorIndex3 = (cursorIndex <= num3) ? num3 : cursorIndex;
					CursorPositionStylePainterParameters default3 = CursorPositionStylePainterParameters.GetDefault(this, this.text);
					default3.text = this.editorEngine.text;
					default3.wordWrapWidth = num2;
					default3.cursorIndex = cursorIndex2;
					textNativeSettings = default3.GetTextNativeSettings(scaling);
					Vector2 a = TextNative.GetCursorPosition(textNativeSettings, default3.rect, cursorIndex2);
					Vector2 a2 = TextNative.GetCursorPosition(textNativeSettings, default3.rect, cursorIndex3);
					a -= scrollOffset;
					a2 -= scrollOffset;
					if (Mathf.Approximately(a.y, a2.y))
					{
						default2.rect = new Rect(a.x, a.y, a2.x - a.x, num);
						painter.DrawRect(default2);
					}
					else
					{
						default2.rect = new Rect(a.x, a.y, base.contentRect.xMax - a.x, num);
						painter.DrawRect(default2);
						float num4 = a2.y - a.y - num;
						if (num4 > 0f)
						{
							default2.rect = new Rect(base.contentRect.x, a.y + num, num2, num4);
							painter.DrawRect(default2);
						}
						if (a2.x != base.contentRect.x)
						{
							default2.rect = new Rect(base.contentRect.x, a2.y, a2.x, num);
							painter.DrawRect(default2);
						}
					}
				}
				if (!string.IsNullOrEmpty(this.editorEngine.text) && base.contentRect.width > 0f && base.contentRect.height > 0f)
				{
					@default = TextStylePainterParameters.GetDefault(this, this.text);
					@default.rect = new Rect(base.contentRect.x - scrollOffset.x, base.contentRect.y - scrollOffset.y, base.contentRect.width, base.contentRect.height);
					@default.text = this.editorEngine.text;
					painter.DrawText(@default);
				}
				if (cursorIndex == num3 && style.font != null)
				{
					CursorPositionStylePainterParameters default3 = CursorPositionStylePainterParameters.GetDefault(this, this.text);
					default3.text = this.editorEngine.text;
					default3.wordWrapWidth = num2;
					default3.cursorIndex = cursorIndex;
					textNativeSettings = default3.GetTextNativeSettings(scaling);
					Vector2 a3 = TextNative.GetCursorPosition(textNativeSettings, default3.rect, default3.cursorIndex);
					a3 -= scrollOffset;
					RectStylePainterParameters painterParams = new RectStylePainterParameters
					{
						rect = new Rect(a3.x, a3.y, 1f, num),
						color = specifiedValueOrDefault
					};
					painter.DrawRect(painterParams);
				}
				if (this.editorEngine.altCursorPosition != -1)
				{
					CursorPositionStylePainterParameters default3 = CursorPositionStylePainterParameters.GetDefault(this, this.text);
					default3.text = this.editorEngine.text.Substring(0, this.editorEngine.altCursorPosition);
					default3.wordWrapWidth = num2;
					default3.cursorIndex = this.editorEngine.altCursorPosition;
					textNativeSettings = default3.GetTextNativeSettings(scaling);
					Vector2 a4 = TextNative.GetCursorPosition(textNativeSettings, default3.rect, default3.cursorIndex);
					a4 -= scrollOffset;
					RectStylePainterParameters painterParams2 = new RectStylePainterParameters
					{
						rect = new Rect(a4.x, a4.y, 1f, num),
						color = specifiedValueOrDefault
					};
					painter.DrawRect(painterParams2);
				}
				keyboardTextEditorEventHandler.PostDrawCursor();
			}
		}

		// Token: 0x0600071A RID: 1818 RVA: 0x00018DE0 File Offset: 0x00016FE0
		internal virtual bool AcceptCharacter(char c)
		{
			return true;
		}

		// Token: 0x0600071B RID: 1819 RVA: 0x00018DF8 File Offset: 0x00016FF8
		protected virtual void BuildContextualMenu(ContextualMenuPopulateEvent evt)
		{
			if (evt.target is TextInputFieldBase<T>)
			{
				evt.menu.AppendAction("Cut", new Action<DropdownMenu.MenuAction>(this.Cut), new Func<DropdownMenu.MenuAction, DropdownMenu.MenuAction.StatusFlags>(this.CutCopyActionStatus), null);
				evt.menu.AppendAction("Copy", new Action<DropdownMenu.MenuAction>(this.Copy), new Func<DropdownMenu.MenuAction, DropdownMenu.MenuAction.StatusFlags>(this.CutCopyActionStatus), null);
				evt.menu.AppendAction("Paste", new Action<DropdownMenu.MenuAction>(this.Paste), new Func<DropdownMenu.MenuAction, DropdownMenu.MenuAction.StatusFlags>(this.PasteActionStatus), null);
			}
		}

		// Token: 0x0600071C RID: 1820 RVA: 0x00018E94 File Offset: 0x00017094
		private void OnDetectFocusChange()
		{
			if (this.editorEngine.m_HasFocus && !this.hasFocus)
			{
				this.editorEngine.OnFocus();
			}
			if (!this.editorEngine.m_HasFocus && this.hasFocus)
			{
				this.editorEngine.OnLostFocus();
			}
		}

		// Token: 0x0600071D RID: 1821 RVA: 0x00010B14 File Offset: 0x0000ED14
		private void OnCursorIndexChange()
		{
			base.IncrementVersion(VersionChangeType.Repaint);
		}

		// Token: 0x0600071E RID: 1822 RVA: 0x00018EF0 File Offset: 0x000170F0
		protected internal override Vector2 DoMeasure(float width, VisualElement.MeasureMode widthMode, float height, VisualElement.MeasureMode heightMode)
		{
			return TextElement.MeasureVisualElementTextSize(this, this.m_Text, width, widthMode, height, heightMode);
		}

		// Token: 0x0600071F RID: 1823 RVA: 0x00018F18 File Offset: 0x00017118
		protected internal override void ExecuteDefaultActionAtTarget(EventBase evt)
		{
			base.ExecuteDefaultActionAtTarget(evt);
			if (base.elementPanel != null && base.elementPanel.contextualMenuManager != null)
			{
				base.elementPanel.contextualMenuManager.DisplayMenuIfEventMatches(evt, this);
			}
			if (evt.GetEventTypeId() == EventBase<ContextualMenuPopulateEvent>.TypeId())
			{
				ContextualMenuPopulateEvent contextualMenuPopulateEvent = evt as ContextualMenuPopulateEvent;
				int count = contextualMenuPopulateEvent.menu.MenuItems().Count;
				this.BuildContextualMenu(contextualMenuPopulateEvent);
				if (count > 0 && contextualMenuPopulateEvent.menu.MenuItems().Count > count)
				{
					contextualMenuPopulateEvent.menu.InsertSeparator(null, count);
				}
			}
			this.editorEventHandler.ExecuteDefaultActionAtTarget(evt);
		}

		// Token: 0x06000720 RID: 1824 RVA: 0x00018FC5 File Offset: 0x000171C5
		protected internal override void ExecuteDefaultAction(EventBase evt)
		{
			base.ExecuteDefaultAction(evt);
			this.editorEventHandler.ExecuteDefaultAction(evt);
		}

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x06000721 RID: 1825 RVA: 0x00018FDC File Offset: 0x000171DC
		bool ITextInputField.hasFocus
		{
			get
			{
				return this.hasFocus;
			}
		}

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x06000722 RID: 1826 RVA: 0x00018FF8 File Offset: 0x000171F8
		// (set) Token: 0x06000723 RID: 1827 RVA: 0x00019013 File Offset: 0x00017213
		string ITextElement.text
		{
			get
			{
				return this.m_Text;
			}
			set
			{
				this.m_Text = value;
			}
		}

		// Token: 0x06000724 RID: 1828 RVA: 0x0001901D File Offset: 0x0001721D
		void ITextInputField.SyncTextEngine()
		{
			this.SyncTextEngine();
		}

		// Token: 0x06000725 RID: 1829 RVA: 0x00019028 File Offset: 0x00017228
		bool ITextInputField.AcceptCharacter(char c)
		{
			return this.AcceptCharacter(c);
		}

		// Token: 0x06000726 RID: 1830 RVA: 0x00019044 File Offset: 0x00017244
		string ITextInputField.CullString(string s)
		{
			return this.CullString(s);
		}

		// Token: 0x06000727 RID: 1831 RVA: 0x00019060 File Offset: 0x00017260
		void ITextInputField.UpdateText(string value)
		{
			this.UpdateText(value);
		}

		// Token: 0x040002BA RID: 698
		private const string SelectionColorProperty = "selection-color";

		// Token: 0x040002BB RID: 699
		private const string CursorColorProperty = "cursor-color";

		// Token: 0x040002BC RID: 700
		private StyleValue<Color> m_SelectionColor;

		// Token: 0x040002BD RID: 701
		private StyleValue<Color> m_CursorColor;

		// Token: 0x040002BE RID: 702
		private string m_Text;

		// Token: 0x040002BF RID: 703
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <isPasswordField>k__BackingField;

		// Token: 0x040002C0 RID: 704
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private int <maxLength>k__BackingField;

		// Token: 0x040002C1 RID: 705
		internal const int kMaxLengthNone = -1;

		// Token: 0x040002C2 RID: 706
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <doubleClickSelectsWord>k__BackingField;

		// Token: 0x040002C3 RID: 707
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <tripleClickSelectsLine>k__BackingField;

		// Token: 0x040002C4 RID: 708
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <isDelayed>k__BackingField;

		// Token: 0x040002C5 RID: 709
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TextEditorEventHandler <editorEventHandler>k__BackingField;

		// Token: 0x040002C6 RID: 710
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TextEditorEngine <editorEngine>k__BackingField;

		// Token: 0x040002C7 RID: 711
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private char <maskChar>k__BackingField;

		// Token: 0x020000FB RID: 251
		public new class UxmlTraits : BaseField<T>.UxmlTraits
		{
			// Token: 0x06000728 RID: 1832 RVA: 0x0001906C File Offset: 0x0001726C
			public UxmlTraits()
			{
			}

			// Token: 0x06000729 RID: 1833 RVA: 0x0001911C File Offset: 0x0001731C
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				TextInputFieldBase<T> textInputFieldBase = (TextInputFieldBase<T>)ve;
				textInputFieldBase.maxLength = this.m_MaxLength.GetValueFromBag(bag, cc);
				textInputFieldBase.isPasswordField = this.m_Password.GetValueFromBag(bag, cc);
				string valueFromBag = this.m_MaskCharacter.GetValueFromBag(bag, cc);
				if (valueFromBag != null && valueFromBag.Length > 0)
				{
					textInputFieldBase.maskChar = valueFromBag[0];
				}
				((ITextElement)ve).text = this.m_Text.GetValueFromBag(bag, cc);
			}

			// Token: 0x040002C8 RID: 712
			private UxmlIntAttributeDescription m_MaxLength = new UxmlIntAttributeDescription
			{
				name = "max-length",
				obsoleteNames = new string[]
				{
					"maxLength"
				},
				defaultValue = -1
			};

			// Token: 0x040002C9 RID: 713
			private UxmlBoolAttributeDescription m_Password = new UxmlBoolAttributeDescription
			{
				name = "password"
			};

			// Token: 0x040002CA RID: 714
			private UxmlStringAttributeDescription m_MaskCharacter = new UxmlStringAttributeDescription
			{
				name = "mask-character",
				obsoleteNames = new string[]
				{
					"maskCharacter"
				},
				defaultValue = "*"
			};

			// Token: 0x040002CB RID: 715
			private UxmlStringAttributeDescription m_Text = new UxmlStringAttributeDescription
			{
				name = "text"
			};
		}
	}
}
