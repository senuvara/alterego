﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x020000B9 RID: 185
	public class VisualContainer : VisualElement
	{
		// Token: 0x0600048B RID: 1163 RVA: 0x00002CD9 File Offset: 0x00000ED9
		public VisualContainer()
		{
		}

		// Token: 0x0600048C RID: 1164 RVA: 0x0000F3FC File Offset: 0x0000D5FC
		[Obsolete("VisualContainer.AddChild will be removed. Use VisualElement.Add or VisualElement.shadow.Add instead", false)]
		public void AddChild(VisualElement child)
		{
			base.shadow.Add(child);
		}

		// Token: 0x0600048D RID: 1165 RVA: 0x0000F41C File Offset: 0x0000D61C
		[Obsolete("VisualContainer.InsertChild will be removed. Use VisualElement.Insert or VisualElement.shadow.Insert instead", false)]
		public void InsertChild(int index, VisualElement child)
		{
			base.shadow.Insert(index, child);
		}

		// Token: 0x0600048E RID: 1166 RVA: 0x0000F43C File Offset: 0x0000D63C
		[Obsolete("VisualContainer.RemoveChild will be removed. Use VisualElement.Remove or VisualElement.shadow.Remove instead", false)]
		public void RemoveChild(VisualElement child)
		{
			base.shadow.Remove(child);
		}

		// Token: 0x0600048F RID: 1167 RVA: 0x0000F45C File Offset: 0x0000D65C
		[Obsolete("VisualContainer.RemoveChildAt will be removed. Use VisualElement.RemoveAt or VisualElement.shadow.RemoveAt instead", false)]
		public void RemoveChildAt(int index)
		{
			base.shadow.RemoveAt(index);
		}

		// Token: 0x06000490 RID: 1168 RVA: 0x0000F47C File Offset: 0x0000D67C
		[Obsolete("VisualContainer.ClearChildren will be removed. Use VisualElement.Clear or VisualElement.shadow.Clear instead", false)]
		public void ClearChildren()
		{
			base.shadow.Clear();
		}

		// Token: 0x06000491 RID: 1169 RVA: 0x0000F498 File Offset: 0x0000D698
		[Obsolete("VisualContainer.GetChildAt will be removed. Use VisualElement.ElementAt or VisualElement.shadow.ElementAt instead", false)]
		public VisualElement GetChildAt(int index)
		{
			return base.shadow[index];
		}

		// Token: 0x020000BA RID: 186
		public new class UxmlFactory : VisualElement.UxmlFactory
		{
			// Token: 0x06000492 RID: 1170 RVA: 0x0000F4BC File Offset: 0x0000D6BC
			public UxmlFactory()
			{
			}

			// Token: 0x1700011B RID: 283
			// (get) Token: 0x06000493 RID: 1171 RVA: 0x0000F4C4 File Offset: 0x0000D6C4
			public override string uxmlName
			{
				get
				{
					return typeof(VisualContainer).Name;
				}
			}

			// Token: 0x1700011C RID: 284
			// (get) Token: 0x06000494 RID: 1172 RVA: 0x0000F4E8 File Offset: 0x0000D6E8
			public override string uxmlNamespace
			{
				get
				{
					return typeof(VisualContainer).Namespace;
				}
			}

			// Token: 0x1700011D RID: 285
			// (get) Token: 0x06000495 RID: 1173 RVA: 0x0000F50C File Offset: 0x0000D70C
			public override string uxmlQualifiedName
			{
				get
				{
					return typeof(VisualContainer).FullName;
				}
			}

			// Token: 0x1700011E RID: 286
			// (get) Token: 0x06000496 RID: 1174 RVA: 0x0000F530 File Offset: 0x0000D730
			public override string substituteForTypeName
			{
				get
				{
					return typeof(VisualElement).Name;
				}
			}

			// Token: 0x1700011F RID: 287
			// (get) Token: 0x06000497 RID: 1175 RVA: 0x0000F554 File Offset: 0x0000D754
			public override string substituteForTypeNamespace
			{
				get
				{
					return typeof(VisualElement).Namespace;
				}
			}

			// Token: 0x17000120 RID: 288
			// (get) Token: 0x06000498 RID: 1176 RVA: 0x0000F578 File Offset: 0x0000D778
			public override string substituteForTypeQualifiedName
			{
				get
				{
					return typeof(VisualElement).FullName;
				}
			}
		}
	}
}
