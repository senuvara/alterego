﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Experimental.UIElements.StyleEnums;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200007E RID: 126
	public class Scroller : VisualElement
	{
		// Token: 0x06000366 RID: 870 RVA: 0x0000C0B9 File Offset: 0x0000A2B9
		public Scroller() : this(0f, 0f, null, SliderDirection.Vertical)
		{
		}

		// Token: 0x06000367 RID: 871 RVA: 0x0000C0D0 File Offset: 0x0000A2D0
		public Scroller(float lowValue, float highValue, Action<float> valueChanged, SliderDirection direction = SliderDirection.Vertical)
		{
			this.direction = direction;
			this.valueChanged = valueChanged;
			this.slider = new Slider(lowValue, highValue, direction, 20f)
			{
				name = "Slider",
				persistenceKey = "Slider"
			};
			this.slider.OnValueChanged(new EventCallback<ChangeEvent<float>>(this.OnSliderValueChange));
			base.Add(this.slider);
			this.lowButton = new ScrollerButton(new Action(this.ScrollPageUp), 250L, 30L)
			{
				name = "LowButton"
			};
			base.Add(this.lowButton);
			this.highButton = new ScrollerButton(new Action(this.ScrollPageDown), 250L, 30L)
			{
				name = "HighButton"
			};
			base.Add(this.highButton);
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x06000368 RID: 872 RVA: 0x0000C1B4 File Offset: 0x0000A3B4
		// (remove) Token: 0x06000369 RID: 873 RVA: 0x0000C1EC File Offset: 0x0000A3EC
		public event Action<float> valueChanged
		{
			add
			{
				Action<float> action = this.valueChanged;
				Action<float> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<float>>(ref this.valueChanged, (Action<float>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<float> action = this.valueChanged;
				Action<float> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<float>>(ref this.valueChanged, (Action<float>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x0600036A RID: 874 RVA: 0x0000C224 File Offset: 0x0000A424
		// (set) Token: 0x0600036B RID: 875 RVA: 0x0000C23E File Offset: 0x0000A43E
		public Slider slider
		{
			[CompilerGenerated]
			get
			{
				return this.<slider>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<slider>k__BackingField = value;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x0600036C RID: 876 RVA: 0x0000C248 File Offset: 0x0000A448
		// (set) Token: 0x0600036D RID: 877 RVA: 0x0000C262 File Offset: 0x0000A462
		public ScrollerButton lowButton
		{
			[CompilerGenerated]
			get
			{
				return this.<lowButton>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<lowButton>k__BackingField = value;
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x0600036E RID: 878 RVA: 0x0000C26C File Offset: 0x0000A46C
		// (set) Token: 0x0600036F RID: 879 RVA: 0x0000C286 File Offset: 0x0000A486
		public ScrollerButton highButton
		{
			[CompilerGenerated]
			get
			{
				return this.<highButton>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<highButton>k__BackingField = value;
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x06000370 RID: 880 RVA: 0x0000C290 File Offset: 0x0000A490
		// (set) Token: 0x06000371 RID: 881 RVA: 0x0000C2B0 File Offset: 0x0000A4B0
		public float value
		{
			get
			{
				return this.slider.value;
			}
			set
			{
				this.slider.value = value;
			}
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000372 RID: 882 RVA: 0x0000C2C0 File Offset: 0x0000A4C0
		// (set) Token: 0x06000373 RID: 883 RVA: 0x0000C2E0 File Offset: 0x0000A4E0
		public float lowValue
		{
			get
			{
				return this.slider.lowValue;
			}
			set
			{
				this.slider.lowValue = value;
			}
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000374 RID: 884 RVA: 0x0000C2F0 File Offset: 0x0000A4F0
		// (set) Token: 0x06000375 RID: 885 RVA: 0x0000C310 File Offset: 0x0000A510
		public float highValue
		{
			get
			{
				return this.slider.highValue;
			}
			set
			{
				this.slider.highValue = value;
			}
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000376 RID: 886 RVA: 0x0000C320 File Offset: 0x0000A520
		// (set) Token: 0x06000377 RID: 887 RVA: 0x0000C354 File Offset: 0x0000A554
		public SliderDirection direction
		{
			get
			{
				return (base.style.flexDirection != FlexDirection.Row) ? SliderDirection.Vertical : SliderDirection.Horizontal;
			}
			set
			{
				if (value == SliderDirection.Horizontal)
				{
					base.style.flexDirection = FlexDirection.Row;
					base.AddToClassList("horizontal");
				}
				else
				{
					base.style.flexDirection = FlexDirection.Column;
					base.AddToClassList("vertical");
				}
			}
		}

		// Token: 0x06000378 RID: 888 RVA: 0x0000C3A9 File Offset: 0x0000A5A9
		public void Adjust(float factor)
		{
			base.SetEnabled(factor < 1f);
			this.slider.AdjustDragElement(factor);
		}

		// Token: 0x06000379 RID: 889 RVA: 0x0000C3C6 File Offset: 0x0000A5C6
		private void OnSliderValueChange(ChangeEvent<float> evt)
		{
			this.value = evt.newValue;
			if (this.valueChanged != null)
			{
				this.valueChanged(this.slider.value);
			}
			base.IncrementVersion(VersionChangeType.Repaint);
		}

		// Token: 0x0600037A RID: 890 RVA: 0x0000C3FD File Offset: 0x0000A5FD
		public void ScrollPageUp()
		{
			this.ScrollPageUp(1f);
		}

		// Token: 0x0600037B RID: 891 RVA: 0x0000C40B File Offset: 0x0000A60B
		public void ScrollPageDown()
		{
			this.ScrollPageDown(1f);
		}

		// Token: 0x0600037C RID: 892 RVA: 0x0000C41C File Offset: 0x0000A61C
		public void ScrollPageUp(float factor)
		{
			this.value -= factor * (this.slider.pageSize * ((this.slider.lowValue >= this.slider.highValue) ? -1f : 1f));
		}

		// Token: 0x0600037D RID: 893 RVA: 0x0000C470 File Offset: 0x0000A670
		public void ScrollPageDown(float factor)
		{
			this.value += factor * (this.slider.pageSize * ((this.slider.lowValue >= this.slider.highValue) ? -1f : 1f));
		}

		// Token: 0x04000156 RID: 342
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Action<float> valueChanged;

		// Token: 0x04000157 RID: 343
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Slider <slider>k__BackingField;

		// Token: 0x04000158 RID: 344
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ScrollerButton <lowButton>k__BackingField;

		// Token: 0x04000159 RID: 345
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ScrollerButton <highButton>k__BackingField;

		// Token: 0x0400015A RID: 346
		internal const float kDefaultPageSize = 20f;

		// Token: 0x0200007F RID: 127
		public new class UxmlFactory : UxmlFactory<Scroller, Scroller.UxmlTraits>
		{
			// Token: 0x0600037E RID: 894 RVA: 0x0000C4C3 File Offset: 0x0000A6C3
			public UxmlFactory()
			{
			}
		}

		// Token: 0x02000080 RID: 128
		public new class UxmlTraits : VisualElement.UxmlTraits
		{
			// Token: 0x0600037F RID: 895 RVA: 0x0000C4CC File Offset: 0x0000A6CC
			public UxmlTraits()
			{
			}

			// Token: 0x170000FB RID: 251
			// (get) Token: 0x06000380 RID: 896 RVA: 0x0000C570 File Offset: 0x0000A770
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield break;
				}
			}

			// Token: 0x06000381 RID: 897 RVA: 0x0000C594 File Offset: 0x0000A794
			public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
			{
				base.Init(ve, bag, cc);
				Scroller scroller = (Scroller)ve;
				scroller.slider.lowValue = this.m_LowValue.GetValueFromBag(bag, cc);
				scroller.slider.highValue = this.m_HighValue.GetValueFromBag(bag, cc);
				scroller.direction = this.m_Direction.GetValueFromBag(bag, cc);
				scroller.value = this.m_Value.GetValueFromBag(bag, cc);
			}

			// Token: 0x0400015B RID: 347
			private UxmlFloatAttributeDescription m_LowValue = new UxmlFloatAttributeDescription
			{
				name = "low-value",
				obsoleteNames = new string[]
				{
					"lowValue"
				}
			};

			// Token: 0x0400015C RID: 348
			private UxmlFloatAttributeDescription m_HighValue = new UxmlFloatAttributeDescription
			{
				name = "high-value",
				obsoleteNames = new string[]
				{
					"highValue"
				}
			};

			// Token: 0x0400015D RID: 349
			private UxmlEnumAttributeDescription<SliderDirection> m_Direction = new UxmlEnumAttributeDescription<SliderDirection>
			{
				name = "direction",
				defaultValue = SliderDirection.Vertical
			};

			// Token: 0x0400015E RID: 350
			private UxmlFloatAttributeDescription m_Value = new UxmlFloatAttributeDescription
			{
				name = "value"
			};

			// Token: 0x02000081 RID: 129
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x06000382 RID: 898 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x06000383 RID: 899 RVA: 0x0000C608 File Offset: 0x0000A808
				public bool MoveNext()
				{
					bool flag = this.$PC != 0;
					this.$PC = -1;
					if (!flag)
					{
					}
					return false;
				}

				// Token: 0x170000FC RID: 252
				// (get) Token: 0x06000384 RID: 900 RVA: 0x0000C624 File Offset: 0x0000A824
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x170000FD RID: 253
				// (get) Token: 0x06000385 RID: 901 RVA: 0x0000C640 File Offset: 0x0000A840
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x06000386 RID: 902 RVA: 0x0000227E File Offset: 0x0000047E
				[DebuggerHidden]
				public void Dispose()
				{
				}

				// Token: 0x06000387 RID: 903 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x06000388 RID: 904 RVA: 0x0000C65C File Offset: 0x0000A85C
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x06000389 RID: 905 RVA: 0x0000C676 File Offset: 0x0000A876
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new Scroller.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x0400015F RID: 351
				internal UxmlChildElementDescription $current;

				// Token: 0x04000160 RID: 352
				internal bool $disposing;

				// Token: 0x04000161 RID: 353
				internal int $PC;
			}
		}
	}
}
