﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Experimental.UIElements.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000041 RID: 65
	public class Image : VisualElement
	{
		// Token: 0x060001B9 RID: 441 RVA: 0x00006789 File Offset: 0x00004989
		public Image()
		{
			this.scaleMode = ScaleMode.ScaleAndCrop;
			this.m_UV = new Rect(0f, 0f, 1f, 1f);
			base.requireMeasureFunction = true;
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x060001BA RID: 442 RVA: 0x000067C4 File Offset: 0x000049C4
		// (set) Token: 0x060001BB RID: 443 RVA: 0x000067E0 File Offset: 0x000049E0
		public StyleValue<Texture> image
		{
			get
			{
				return this.m_Image;
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompareObject<Texture>(ref this.m_Image, value))
				{
					base.IncrementVersion(VersionChangeType.Layout | VersionChangeType.Repaint);
					if (this.m_Image.value == null)
					{
						this.m_UV = new Rect(0f, 0f, 1f, 1f);
					}
				}
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x060001BC RID: 444 RVA: 0x00006840 File Offset: 0x00004A40
		// (set) Token: 0x060001BD RID: 445 RVA: 0x0000685B File Offset: 0x00004A5B
		public Rect sourceRect
		{
			get
			{
				return this.GetSourceRect();
			}
			set
			{
				this.CalculateUV(value);
			}
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x060001BE RID: 446 RVA: 0x00006868 File Offset: 0x00004A68
		// (set) Token: 0x060001BF RID: 447 RVA: 0x00006883 File Offset: 0x00004A83
		public Rect uv
		{
			get
			{
				return this.m_UV;
			}
			set
			{
				this.m_UV = value;
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x060001C0 RID: 448 RVA: 0x00006890 File Offset: 0x00004A90
		// (set) Token: 0x060001C1 RID: 449 RVA: 0x000068C0 File Offset: 0x00004AC0
		public StyleValue<ScaleMode> scaleMode
		{
			get
			{
				return new StyleValue<ScaleMode>((ScaleMode)this.m_ScaleMode.value, this.m_ScaleMode.specificity);
			}
			set
			{
				if (StyleValueUtils.ApplyAndCompare(ref this.m_ScaleMode, new StyleValue<int>((int)value.value, value.specificity)))
				{
					base.IncrementVersion(VersionChangeType.Layout);
				}
			}
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x000068F0 File Offset: 0x00004AF0
		protected internal override Vector2 DoMeasure(float width, VisualElement.MeasureMode widthMode, float height, VisualElement.MeasureMode heightMode)
		{
			float num = float.NaN;
			float num2 = float.NaN;
			Texture specifiedValueOrDefault = this.image.GetSpecifiedValueOrDefault(null);
			Vector2 result;
			if (specifiedValueOrDefault == null)
			{
				result = new Vector2(num, num2);
			}
			else
			{
				Rect sourceRect = this.sourceRect;
				bool flag = sourceRect != Rect.zero;
				num = ((!flag) ? ((float)specifiedValueOrDefault.width) : sourceRect.width);
				num2 = ((!flag) ? ((float)specifiedValueOrDefault.height) : sourceRect.height);
				if (widthMode == VisualElement.MeasureMode.AtMost)
				{
					num = Mathf.Min(num, width);
				}
				if (heightMode == VisualElement.MeasureMode.AtMost)
				{
					num2 = Mathf.Min(num2, height);
				}
				result = new Vector2(num, num2);
			}
			return result;
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x000069B4 File Offset: 0x00004BB4
		protected override void DoRepaint(IStylePainter painter)
		{
			Texture specifiedValueOrDefault = this.image.GetSpecifiedValueOrDefault(null);
			if (!(specifiedValueOrDefault == null))
			{
				TextureStylePainterParameters painterParams = new TextureStylePainterParameters
				{
					rect = base.contentRect,
					uv = this.uv,
					texture = specifiedValueOrDefault,
					color = GUI.color,
					scaleMode = this.scaleMode
				};
				IStylePainterInternal stylePainterInternal = (IStylePainterInternal)painter;
				stylePainterInternal.DrawTexture(painterParams);
			}
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x00006A40 File Offset: 0x00004C40
		protected override void OnStyleResolved(ICustomStyle elementStyle)
		{
			base.OnStyleResolved(elementStyle);
			StyleValue<Texture2D> styleValue = new StyleValue<Texture2D>(this.m_Image.value as Texture2D, this.m_Image.specificity);
			elementStyle.ApplyCustomProperty("image", ref styleValue);
			this.m_Image = new StyleValue<Texture>(styleValue.value, styleValue.specificity);
			elementStyle.ApplyCustomProperty("image-size", ref this.m_ScaleMode);
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x00006AB0 File Offset: 0x00004CB0
		private void CalculateUV(Rect srcRect)
		{
			this.m_UV = new Rect(0f, 0f, 1f, 1f);
			Texture specifiedValueOrDefault = this.image.GetSpecifiedValueOrDefault(null);
			if (specifiedValueOrDefault != null)
			{
				int width = specifiedValueOrDefault.width;
				int height = specifiedValueOrDefault.height;
				this.m_UV.x = srcRect.x / (float)width;
				this.m_UV.width = srcRect.width / (float)width;
				this.m_UV.height = srcRect.height / (float)height;
				this.m_UV.y = 1f - this.m_UV.height - srcRect.y / (float)height;
			}
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x00006B70 File Offset: 0x00004D70
		private Rect GetSourceRect()
		{
			Rect zero = Rect.zero;
			Texture specifiedValueOrDefault = this.image.GetSpecifiedValueOrDefault(null);
			if (specifiedValueOrDefault != null)
			{
				int width = specifiedValueOrDefault.width;
				int height = specifiedValueOrDefault.height;
				zero.x = this.uv.x * (float)width;
				zero.width = this.uv.width * (float)width;
				zero.y = (1f - this.uv.y - this.uv.height) * (float)height;
				zero.height = this.uv.height * (float)height;
			}
			return zero;
		}

		// Token: 0x04000093 RID: 147
		private StyleValue<int> m_ScaleMode;

		// Token: 0x04000094 RID: 148
		private StyleValue<Texture> m_Image;

		// Token: 0x04000095 RID: 149
		private Rect m_UV;

		// Token: 0x02000042 RID: 66
		public new class UxmlFactory : UxmlFactory<Image, Image.UxmlTraits>
		{
			// Token: 0x060001C7 RID: 455 RVA: 0x00006C37 File Offset: 0x00004E37
			public UxmlFactory()
			{
			}
		}

		// Token: 0x02000043 RID: 67
		public new class UxmlTraits : VisualElement.UxmlTraits
		{
			// Token: 0x060001C8 RID: 456 RVA: 0x00006C3F File Offset: 0x00004E3F
			public UxmlTraits()
			{
			}

			// Token: 0x17000087 RID: 135
			// (get) Token: 0x060001C9 RID: 457 RVA: 0x00006C48 File Offset: 0x00004E48
			public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
			{
				get
				{
					yield break;
				}
			}

			// Token: 0x02000044 RID: 68
			[CompilerGenerated]
			private sealed class <>c__Iterator0 : IEnumerable, IEnumerable<UxmlChildElementDescription>, IEnumerator, IDisposable, IEnumerator<UxmlChildElementDescription>
			{
				// Token: 0x060001CA RID: 458 RVA: 0x00002223 File Offset: 0x00000423
				[DebuggerHidden]
				public <>c__Iterator0()
				{
				}

				// Token: 0x060001CB RID: 459 RVA: 0x00006C6B File Offset: 0x00004E6B
				public bool MoveNext()
				{
					bool flag = this.$PC != 0;
					this.$PC = -1;
					if (!flag)
					{
					}
					return false;
				}

				// Token: 0x17000088 RID: 136
				// (get) Token: 0x060001CC RID: 460 RVA: 0x00006C88 File Offset: 0x00004E88
				UxmlChildElementDescription IEnumerator<UxmlChildElementDescription>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x17000089 RID: 137
				// (get) Token: 0x060001CD RID: 461 RVA: 0x00006CA4 File Offset: 0x00004EA4
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.$current;
					}
				}

				// Token: 0x060001CE RID: 462 RVA: 0x0000227E File Offset: 0x0000047E
				[DebuggerHidden]
				public void Dispose()
				{
				}

				// Token: 0x060001CF RID: 463 RVA: 0x00002280 File Offset: 0x00000480
				[DebuggerHidden]
				public void Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x060001D0 RID: 464 RVA: 0x00006CC0 File Offset: 0x00004EC0
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<UnityEngine.Experimental.UIElements.UxmlChildElementDescription>.GetEnumerator();
				}

				// Token: 0x060001D1 RID: 465 RVA: 0x00006CDA File Offset: 0x00004EDA
				[DebuggerHidden]
				IEnumerator<UxmlChildElementDescription> IEnumerable<UxmlChildElementDescription>.GetEnumerator()
				{
					if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
					{
						return this;
					}
					return new Image.UxmlTraits.<>c__Iterator0();
				}

				// Token: 0x04000096 RID: 150
				internal UxmlChildElementDescription $current;

				// Token: 0x04000097 RID: 151
				internal bool $disposing;

				// Token: 0x04000098 RID: 152
				internal int $PC;
			}
		}
	}
}
