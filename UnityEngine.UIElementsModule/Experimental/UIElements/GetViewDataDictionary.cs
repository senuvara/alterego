﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000062 RID: 98
	// (Invoke) Token: 0x060002AE RID: 686
	internal delegate ISerializableJsonDictionary GetViewDataDictionary();
}
