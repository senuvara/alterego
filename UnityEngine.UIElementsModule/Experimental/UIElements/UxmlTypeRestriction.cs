﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x02000174 RID: 372
	public abstract class UxmlTypeRestriction : IEquatable<UxmlTypeRestriction>
	{
		// Token: 0x06000998 RID: 2456 RVA: 0x00002223 File Offset: 0x00000423
		protected UxmlTypeRestriction()
		{
		}

		// Token: 0x06000999 RID: 2457 RVA: 0x00021210 File Offset: 0x0001F410
		public virtual bool Equals(UxmlTypeRestriction other)
		{
			return this == other;
		}
	}
}
