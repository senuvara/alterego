﻿using System;

namespace UnityEngine.Experimental.UIElements
{
	// Token: 0x0200013B RID: 315
	public class MouseEnterWindowEvent : MouseEventBase<MouseEnterWindowEvent>
	{
		// Token: 0x06000866 RID: 2150 RVA: 0x0001BBCF File Offset: 0x00019DCF
		public MouseEnterWindowEvent()
		{
			this.Init();
		}

		// Token: 0x06000867 RID: 2151 RVA: 0x0001BBDE File Offset: 0x00019DDE
		protected override void Init()
		{
			base.Init();
			base.flags = EventBase.EventFlags.Cancellable;
		}
	}
}
