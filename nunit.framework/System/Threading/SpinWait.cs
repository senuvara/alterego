﻿using System;
using System.Diagnostics;

namespace System.Threading
{
	// Token: 0x0200001B RID: 27
	internal struct SpinWait
	{
		// Token: 0x060000B5 RID: 181 RVA: 0x00003B94 File Offset: 0x00001D94
		public void SpinOnce()
		{
			this.ntime++;
			if (SpinWait.isSingleCpu)
			{
				Thread.Sleep((this.ntime % 10 == 0) ? 1 : 0);
			}
			else if (this.ntime % 10 == 0)
			{
				Thread.Sleep(1);
			}
			else
			{
				Thread.SpinWait(Math.Min(this.ntime, 200) << 1);
			}
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00003C0C File Offset: 0x00001E0C
		public static void SpinUntil(Func<bool> condition)
		{
			SpinWait spinWait = default(SpinWait);
			while (!condition())
			{
				spinWait.SpinOnce();
			}
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00003C3C File Offset: 0x00001E3C
		public static bool SpinUntil(Func<bool> condition, TimeSpan timeout)
		{
			return SpinWait.SpinUntil(condition, (int)timeout.TotalMilliseconds);
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00003C5C File Offset: 0x00001E5C
		public static bool SpinUntil(Func<bool> condition, int millisecondsTimeout)
		{
			SpinWait spinWait = default(SpinWait);
			Stopwatch stopwatch = Stopwatch.StartNew();
			while (!condition())
			{
				if (stopwatch.ElapsedMilliseconds > (long)millisecondsTimeout)
				{
					return false;
				}
				spinWait.SpinOnce();
			}
			return true;
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00003CAB File Offset: 0x00001EAB
		public void Reset()
		{
			this.ntime = 0;
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x060000BA RID: 186 RVA: 0x00003CB8 File Offset: 0x00001EB8
		public bool NextSpinWillYield
		{
			get
			{
				return SpinWait.isSingleCpu || this.ntime % 10 == 0;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x060000BB RID: 187 RVA: 0x00003CE0 File Offset: 0x00001EE0
		public int Count
		{
			get
			{
				return this.ntime;
			}
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00003CF8 File Offset: 0x00001EF8
		// Note: this type is marked as 'beforefieldinit'.
		static SpinWait()
		{
		}

		// Token: 0x04000016 RID: 22
		private const int step = 10;

		// Token: 0x04000017 RID: 23
		private const int maxTime = 200;

		// Token: 0x04000018 RID: 24
		private static readonly bool isSingleCpu = Environment.ProcessorCount == 1;

		// Token: 0x04000019 RID: 25
		private int ntime;
	}
}
