﻿using System;

namespace System.Threading
{
	// Token: 0x02000111 RID: 273
	internal enum LazyThreadSafetyMode
	{
		// Token: 0x040001AC RID: 428
		None,
		// Token: 0x040001AD RID: 429
		PublicationOnly,
		// Token: 0x040001AE RID: 430
		ExecutionAndPublication
	}
}
