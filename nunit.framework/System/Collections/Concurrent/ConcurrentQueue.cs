﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Collections.Concurrent
{
	// Token: 0x020001A8 RID: 424
	[DebuggerDisplay("Count={Count}")]
	[DebuggerTypeProxy(typeof(CollectionDebuggerView<>))]
	internal class ConcurrentQueue<T> : IProducerConsumerCollection<T>, IEnumerable<T>, ICollection, IEnumerable
	{
		// Token: 0x06000AE8 RID: 2792 RVA: 0x000247CD File Offset: 0x000229CD
		public ConcurrentQueue()
		{
			this.tail = this.head;
		}

		// Token: 0x06000AE9 RID: 2793 RVA: 0x000247F0 File Offset: 0x000229F0
		public ConcurrentQueue(IEnumerable<T> collection) : this()
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			foreach (T item in collection)
			{
				this.Enqueue(item);
			}
		}

		// Token: 0x06000AEA RID: 2794 RVA: 0x00024864 File Offset: 0x00022A64
		public void Enqueue(T item)
		{
			ConcurrentQueue<T>.Node node = new ConcurrentQueue<T>.Node();
			node.Value = item;
			ConcurrentQueue<T>.Node node2 = null;
			bool flag = false;
			while (!flag)
			{
				node2 = this.tail;
				ConcurrentQueue<T>.Node next = node2.Next;
				if (this.tail == node2)
				{
					if (next == null)
					{
						flag = (Interlocked.CompareExchange<ConcurrentQueue<T>.Node>(ref this.tail.Next, node, null) == null);
					}
					else
					{
						Interlocked.CompareExchange<ConcurrentQueue<T>.Node>(ref this.tail, next, node2);
					}
				}
			}
			Interlocked.CompareExchange<ConcurrentQueue<T>.Node>(ref this.tail, node, node2);
			Interlocked.Increment(ref this.count);
		}

		// Token: 0x06000AEB RID: 2795 RVA: 0x00024908 File Offset: 0x00022B08
		bool IProducerConsumerCollection<!0>.TryAdd(T item)
		{
			this.Enqueue(item);
			return true;
		}

		// Token: 0x06000AEC RID: 2796 RVA: 0x00024924 File Offset: 0x00022B24
		public bool TryDequeue(out T result)
		{
			result = default(T);
			ConcurrentQueue<T>.Node node = null;
			bool flag = false;
			while (!flag)
			{
				ConcurrentQueue<T>.Node node2 = this.head;
				ConcurrentQueue<T>.Node node3 = this.tail;
				node = node2.Next;
				if (node2 == this.head)
				{
					if (node2 == node3)
					{
						if (node == null)
						{
							result = default(T);
							return false;
						}
						Interlocked.CompareExchange<ConcurrentQueue<T>.Node>(ref this.tail, node, node3);
					}
					else
					{
						result = node.Value;
						flag = (Interlocked.CompareExchange<ConcurrentQueue<T>.Node>(ref this.head, node, node2) == node2);
					}
				}
			}
			node.Value = default(T);
			Interlocked.Decrement(ref this.count);
			return true;
		}

		// Token: 0x06000AED RID: 2797 RVA: 0x000249EC File Offset: 0x00022BEC
		public bool TryPeek(out T result)
		{
			result = default(T);
			bool flag = true;
			while (flag)
			{
				ConcurrentQueue<T>.Node node = this.head;
				ConcurrentQueue<T>.Node next = node.Next;
				if (next == null)
				{
					result = default(T);
					return false;
				}
				result = next.Value;
				flag = (this.head != node);
			}
			return true;
		}

		// Token: 0x06000AEE RID: 2798 RVA: 0x00024A54 File Offset: 0x00022C54
		internal void Clear()
		{
			this.count = 0;
			this.tail = (this.head = new ConcurrentQueue<T>.Node());
		}

		// Token: 0x06000AEF RID: 2799 RVA: 0x00024A80 File Offset: 0x00022C80
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.InternalGetEnumerator();
		}

		// Token: 0x06000AF0 RID: 2800 RVA: 0x00024A98 File Offset: 0x00022C98
		public IEnumerator<T> GetEnumerator()
		{
			return this.InternalGetEnumerator();
		}

		// Token: 0x06000AF1 RID: 2801 RVA: 0x00024B90 File Offset: 0x00022D90
		private IEnumerator<T> InternalGetEnumerator()
		{
			ConcurrentQueue<T>.Node my_head = this.head;
			while ((my_head = my_head.Next) != null)
			{
				yield return my_head.Value;
			}
			yield break;
		}

		// Token: 0x06000AF2 RID: 2802 RVA: 0x00024BB0 File Offset: 0x00022DB0
		void ICollection.CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank > 1)
			{
				throw new ArgumentException("The array can't be multidimensional");
			}
			if (array.GetLowerBound(0) != 0)
			{
				throw new ArgumentException("The array needs to be 0-based");
			}
			T[] array2 = array as T[];
			if (array2 == null)
			{
				throw new ArgumentException("The array cannot be cast to the collection element type", "array");
			}
			this.CopyTo(array2, index);
		}

		// Token: 0x06000AF3 RID: 2803 RVA: 0x00024C34 File Offset: 0x00022E34
		public void CopyTo(T[] array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (index >= array.Length)
			{
				throw new ArgumentException("index is equals or greather than array length", "index");
			}
			IEnumerator<T> enumerator = this.InternalGetEnumerator();
			int num = index;
			while (enumerator.MoveNext())
			{
				if (num == array.Length - index)
				{
					throw new ArgumentException("The number of elememts in the collection exceeds the capacity of array", "array");
				}
				array[num++] = enumerator.Current;
			}
		}

		// Token: 0x06000AF4 RID: 2804 RVA: 0x00024CD0 File Offset: 0x00022ED0
		public T[] ToArray()
		{
			return new List<T>(this).ToArray();
		}

		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06000AF5 RID: 2805 RVA: 0x00024CF0 File Offset: 0x00022EF0
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000AF6 RID: 2806 RVA: 0x00024D04 File Offset: 0x00022F04
		bool IProducerConsumerCollection<!0>.TryTake(out T item)
		{
			return this.TryDequeue(out item);
		}

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06000AF7 RID: 2807 RVA: 0x00024D1D File Offset: 0x00022F1D
		object ICollection.SyncRoot
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000276 RID: 630
		// (get) Token: 0x06000AF8 RID: 2808 RVA: 0x00024D28 File Offset: 0x00022F28
		public int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x06000AF9 RID: 2809 RVA: 0x00024D40 File Offset: 0x00022F40
		public bool IsEmpty
		{
			get
			{
				return this.count == 0;
			}
		}

		// Token: 0x0400029C RID: 668
		private ConcurrentQueue<T>.Node head = new ConcurrentQueue<T>.Node();

		// Token: 0x0400029D RID: 669
		private ConcurrentQueue<T>.Node tail;

		// Token: 0x0400029E RID: 670
		private int count;

		// Token: 0x020001A9 RID: 425
		private class Node
		{
			// Token: 0x06000AFA RID: 2810 RVA: 0x00024D5B File Offset: 0x00022F5B
			public Node()
			{
			}

			// Token: 0x0400029F RID: 671
			public T Value;

			// Token: 0x040002A0 RID: 672
			public ConcurrentQueue<T>.Node Next;
		}

		// Token: 0x020001C9 RID: 457
		[CompilerGenerated]
		private sealed class <InternalGetEnumerator>d__0 : IEnumerator<T>, IEnumerator, IDisposable
		{
			// Token: 0x06000B81 RID: 2945 RVA: 0x00024AB0 File Offset: 0x00022CB0
			bool IEnumerator.MoveNext()
			{
				switch (this.<>1__state)
				{
				case 0:
					this.<>1__state = -1;
					my_head = this.head;
					break;
				case 1:
					this.<>1__state = -1;
					break;
				default:
					goto IL_7C;
				}
				if ((my_head = my_head.Next) != null)
				{
					this.<>2__current = my_head.Value;
					this.<>1__state = 1;
					return true;
				}
				IL_7C:
				return false;
			}

			// Token: 0x17000290 RID: 656
			// (get) Token: 0x06000B82 RID: 2946 RVA: 0x00024B40 File Offset: 0x00022D40
			T IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B83 RID: 2947 RVA: 0x00024B57 File Offset: 0x00022D57
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000B84 RID: 2948 RVA: 0x00024B5E File Offset: 0x00022D5E
			void IDisposable.Dispose()
			{
			}

			// Token: 0x17000291 RID: 657
			// (get) Token: 0x06000B85 RID: 2949 RVA: 0x00024B64 File Offset: 0x00022D64
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B86 RID: 2950 RVA: 0x00024B80 File Offset: 0x00022D80
			[DebuggerHidden]
			public <InternalGetEnumerator>d__0(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x0400030F RID: 783
			private T <>2__current;

			// Token: 0x04000310 RID: 784
			private int <>1__state;

			// Token: 0x04000311 RID: 785
			public ConcurrentQueue<T> <>4__this;

			// Token: 0x04000312 RID: 786
			public ConcurrentQueue<T>.Node <my_head>5__1;
		}
	}
}
