﻿using System;
using System.Collections.Generic;

namespace System.Collections.Concurrent
{
	// Token: 0x02000044 RID: 68
	internal interface IProducerConsumerCollection<T> : IEnumerable<T>, ICollection, IEnumerable
	{
		// Token: 0x06000203 RID: 515
		bool TryAdd(T item);

		// Token: 0x06000204 RID: 516
		bool TryTake(out T item);

		// Token: 0x06000205 RID: 517
		T[] ToArray();

		// Token: 0x06000206 RID: 518
		void CopyTo(T[] array, int index);
	}
}
