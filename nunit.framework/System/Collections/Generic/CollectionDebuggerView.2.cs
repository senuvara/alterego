﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x020000E2 RID: 226
	internal sealed class CollectionDebuggerView<T, U>
	{
		// Token: 0x060006A1 RID: 1697 RVA: 0x00017932 File Offset: 0x00015B32
		public CollectionDebuggerView(ICollection<KeyValuePair<T, U>> col)
		{
			this.c = col;
		}

		// Token: 0x1700017C RID: 380
		// (get) Token: 0x060006A2 RID: 1698 RVA: 0x00017944 File Offset: 0x00015B44
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public KeyValuePair<T, U>[] Items
		{
			get
			{
				KeyValuePair<T, U>[] array = new KeyValuePair<T, U>[this.c.Count];
				this.c.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04000166 RID: 358
		private readonly ICollection<KeyValuePair<T, U>> c;
	}
}
