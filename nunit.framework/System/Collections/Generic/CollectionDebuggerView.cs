﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x020000E1 RID: 225
	internal sealed class CollectionDebuggerView<T>
	{
		// Token: 0x0600069F RID: 1695 RVA: 0x000178EC File Offset: 0x00015AEC
		public CollectionDebuggerView(ICollection<T> col)
		{
			this.c = col;
		}

		// Token: 0x1700017B RID: 379
		// (get) Token: 0x060006A0 RID: 1696 RVA: 0x00017900 File Offset: 0x00015B00
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				T[] array = new T[this.c.Count];
				this.c.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04000165 RID: 357
		private readonly ICollection<T> c;
	}
}
