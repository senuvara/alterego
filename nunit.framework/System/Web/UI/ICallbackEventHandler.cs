﻿using System;

namespace System.Web.UI
{
	// Token: 0x02000089 RID: 137
	public interface ICallbackEventHandler
	{
		// Token: 0x060004A7 RID: 1191
		void RaiseCallbackEvent(string report);

		// Token: 0x060004A8 RID: 1192
		string GetCallbackResult();
	}
}
