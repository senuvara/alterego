﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading;

namespace System
{
	// Token: 0x020000E0 RID: 224
	[DebuggerDisplay("ThreadSafetyMode={mode}, IsValueCreated={IsValueCreated}, IsValueFaulted={exception != null}, Value={value}")]
	[ComVisible(false)]
	[HostProtection(SecurityAction.LinkDemand, Synchronization = true, ExternalThreading = true)]
	[Serializable]
	internal class Lazy<T>
	{
		// Token: 0x06000695 RID: 1685 RVA: 0x00017587 File Offset: 0x00015787
		public Lazy() : this(System.Threading.LazyThreadSafetyMode.ExecutionAndPublication)
		{
		}

		// Token: 0x06000696 RID: 1686 RVA: 0x00017593 File Offset: 0x00015793
		public Lazy(Func<T> valueFactory) : this(valueFactory, System.Threading.LazyThreadSafetyMode.ExecutionAndPublication)
		{
		}

		// Token: 0x06000697 RID: 1687 RVA: 0x000175A0 File Offset: 0x000157A0
		public Lazy(bool isThreadSafe) : this(new Func<T>(Activator.CreateInstance<T>), isThreadSafe ? System.Threading.LazyThreadSafetyMode.ExecutionAndPublication : System.Threading.LazyThreadSafetyMode.None)
		{
		}

		// Token: 0x06000698 RID: 1688 RVA: 0x000175BE File Offset: 0x000157BE
		public Lazy(Func<T> valueFactory, bool isThreadSafe) : this(valueFactory, isThreadSafe ? System.Threading.LazyThreadSafetyMode.ExecutionAndPublication : System.Threading.LazyThreadSafetyMode.None)
		{
		}

		// Token: 0x06000699 RID: 1689 RVA: 0x000175D1 File Offset: 0x000157D1
		public Lazy(System.Threading.LazyThreadSafetyMode mode) : this(new Func<T>(Activator.CreateInstance<T>), mode)
		{
		}

		// Token: 0x0600069A RID: 1690 RVA: 0x000175EC File Offset: 0x000157EC
		public Lazy(Func<T> valueFactory, System.Threading.LazyThreadSafetyMode mode)
		{
			if (valueFactory == null)
			{
				throw new ArgumentNullException("valueFactory");
			}
			this.factory = valueFactory;
			if (mode != System.Threading.LazyThreadSafetyMode.None)
			{
				this.monitor = new object();
			}
			this.mode = mode;
		}

		// Token: 0x17000179 RID: 377
		// (get) Token: 0x0600069B RID: 1691 RVA: 0x0001763C File Offset: 0x0001583C
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public T Value
		{
			get
			{
				T result;
				if (this.inited)
				{
					result = this.value;
				}
				else
				{
					if (this.exception != null)
					{
						throw this.exception;
					}
					result = this.InitValue();
				}
				return result;
			}
		}

		// Token: 0x0600069C RID: 1692 RVA: 0x00017680 File Offset: 0x00015880
		private T InitValue()
		{
			switch (this.mode)
			{
			case System.Threading.LazyThreadSafetyMode.None:
			{
				Func<T> func = this.factory;
				if (func == null)
				{
					if (this.exception == null)
					{
						throw new InvalidOperationException("The initialization function tries to access Value on this instance");
					}
					throw this.exception;
				}
				else
				{
					try
					{
						this.factory = null;
						T t = func();
						this.value = t;
						Thread.MemoryBarrier();
						this.inited = true;
					}
					catch (Exception ex)
					{
						this.exception = ex;
						throw;
					}
				}
				break;
			}
			case System.Threading.LazyThreadSafetyMode.PublicationOnly:
			{
				Func<T> func = this.factory;
				T t;
				if (func != null)
				{
					t = func();
				}
				else
				{
					t = default(T);
				}
				lock (this.monitor)
				{
					if (this.inited)
					{
						return this.value;
					}
					this.value = t;
					Thread.MemoryBarrier();
					this.inited = true;
					this.factory = null;
				}
				break;
			}
			case System.Threading.LazyThreadSafetyMode.ExecutionAndPublication:
				lock (this.monitor)
				{
					if (this.inited)
					{
						return this.value;
					}
					if (this.factory == null)
					{
						if (this.exception == null)
						{
							throw new InvalidOperationException("The initialization function tries to access Value on this instance");
						}
						throw this.exception;
					}
					else
					{
						Func<T> func = this.factory;
						try
						{
							this.factory = null;
							T t = func();
							this.value = t;
							Thread.MemoryBarrier();
							this.inited = true;
						}
						catch (Exception ex)
						{
							this.exception = ex;
							throw;
						}
					}
				}
				break;
			default:
				throw new InvalidOperationException("Invalid LazyThreadSafetyMode " + this.mode);
			}
			return this.value;
		}

		// Token: 0x1700017A RID: 378
		// (get) Token: 0x0600069D RID: 1693 RVA: 0x0001789C File Offset: 0x00015A9C
		public bool IsValueCreated
		{
			get
			{
				return this.inited;
			}
		}

		// Token: 0x0600069E RID: 1694 RVA: 0x000178B4 File Offset: 0x00015AB4
		public override string ToString()
		{
			string result;
			if (this.inited)
			{
				result = this.value.ToString();
			}
			else
			{
				result = "Value is not created";
			}
			return result;
		}

		// Token: 0x0400015F RID: 351
		private T value;

		// Token: 0x04000160 RID: 352
		private Func<T> factory;

		// Token: 0x04000161 RID: 353
		private object monitor;

		// Token: 0x04000162 RID: 354
		private Exception exception;

		// Token: 0x04000163 RID: 355
		private System.Threading.LazyThreadSafetyMode mode;

		// Token: 0x04000164 RID: 356
		private bool inited;
	}
}
