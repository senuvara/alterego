﻿using System;
using System.Security;

namespace NUnit.Compatibility
{
	// Token: 0x02000087 RID: 135
	public class LongLivedMarshalByRefObject : MarshalByRefObject
	{
		// Token: 0x06000483 RID: 1155 RVA: 0x0000EF6C File Offset: 0x0000D16C
		[SecurityCritical]
		public override object InitializeLifetimeService()
		{
			return null;
		}

		// Token: 0x06000484 RID: 1156 RVA: 0x0000EF7F File Offset: 0x0000D17F
		public LongLivedMarshalByRefObject()
		{
		}
	}
}
