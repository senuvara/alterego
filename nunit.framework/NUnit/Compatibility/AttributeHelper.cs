﻿using System;
using System.Reflection;

namespace NUnit.Compatibility
{
	// Token: 0x020000E3 RID: 227
	public static class AttributeHelper
	{
		// Token: 0x060006A3 RID: 1699 RVA: 0x00017978 File Offset: 0x00015B78
		public static Attribute[] GetCustomAttributes(object actual, Type attributeType, bool inherit)
		{
			ICustomAttributeProvider customAttributeProvider = actual as ICustomAttributeProvider;
			if (customAttributeProvider == null)
			{
				throw new ArgumentException(string.Format("Actual value {0} does not implement ICustomAttributeProvider.", actual), "actual");
			}
			return (Attribute[])customAttributeProvider.GetCustomAttributes(attributeType, inherit);
		}
	}
}
