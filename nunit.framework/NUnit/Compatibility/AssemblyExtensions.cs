﻿using System;
using System.Reflection;

namespace NUnit.Compatibility
{
	// Token: 0x020000B1 RID: 177
	public static class AssemblyExtensions
	{
		// Token: 0x06000518 RID: 1304 RVA: 0x00011EFC File Offset: 0x000100FC
		public static T GetCustomAttribute<T>(this Assembly assembly) where T : Attribute
		{
			T[] array = (T[])assembly.GetCustomAttributes(typeof(T), false);
			return (array.Length > 0) ? array[0] : default(T);
		}
	}
}
