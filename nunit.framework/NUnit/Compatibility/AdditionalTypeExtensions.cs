﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace NUnit.Compatibility
{
	// Token: 0x020000B2 RID: 178
	public static class AdditionalTypeExtensions
	{
		// Token: 0x06000519 RID: 1305 RVA: 0x00011F40 File Offset: 0x00010140
		public static bool ParametersMatch(this ParameterInfo[] pinfos, Type[] ptypes)
		{
			bool result;
			if (pinfos.Length != ptypes.Length)
			{
				result = false;
			}
			else
			{
				for (int i = 0; i < pinfos.Length; i++)
				{
					if (!pinfos[i].ParameterType.IsCastableFrom(ptypes[i]))
					{
						return false;
					}
				}
				result = true;
			}
			return result;
		}

		// Token: 0x0600051A RID: 1306 RVA: 0x00011FCC File Offset: 0x000101CC
		public static bool IsCastableFrom(this Type to, Type from)
		{
			return to.IsAssignableFrom(from) || (from == typeof(NUnitNullType) && (to.GetTypeInfo().IsClass || to.FullName.StartsWith("System.Nullable"))) || (AdditionalTypeExtensions.convertibleValueTypes.ContainsKey(to) && AdditionalTypeExtensions.convertibleValueTypes[to].Contains(from)) || from.GetMethods(BindingFlags.Static | BindingFlags.Public).Any((MethodInfo m) => m.ReturnType == to && m.Name == "op_Implicit");
		}

		// Token: 0x0600051B RID: 1307 RVA: 0x0001209C File Offset: 0x0001029C
		// Note: this type is marked as 'beforefieldinit'.
		static AdditionalTypeExtensions()
		{
		}

		// Token: 0x04000109 RID: 265
		private static Dictionary<Type, List<Type>> convertibleValueTypes = new Dictionary<Type, List<Type>>
		{
			{
				typeof(decimal),
				new List<Type>
				{
					typeof(sbyte),
					typeof(byte),
					typeof(short),
					typeof(ushort),
					typeof(int),
					typeof(uint),
					typeof(long),
					typeof(ulong),
					typeof(char)
				}
			},
			{
				typeof(double),
				new List<Type>
				{
					typeof(sbyte),
					typeof(byte),
					typeof(short),
					typeof(ushort),
					typeof(int),
					typeof(uint),
					typeof(long),
					typeof(ulong),
					typeof(char),
					typeof(float)
				}
			},
			{
				typeof(float),
				new List<Type>
				{
					typeof(sbyte),
					typeof(byte),
					typeof(short),
					typeof(ushort),
					typeof(int),
					typeof(uint),
					typeof(long),
					typeof(ulong),
					typeof(char),
					typeof(float)
				}
			},
			{
				typeof(ulong),
				new List<Type>
				{
					typeof(byte),
					typeof(ushort),
					typeof(uint),
					typeof(char)
				}
			},
			{
				typeof(long),
				new List<Type>
				{
					typeof(sbyte),
					typeof(byte),
					typeof(short),
					typeof(ushort),
					typeof(int),
					typeof(uint),
					typeof(char)
				}
			},
			{
				typeof(uint),
				new List<Type>
				{
					typeof(byte),
					typeof(ushort),
					typeof(char)
				}
			},
			{
				typeof(int),
				new List<Type>
				{
					typeof(sbyte),
					typeof(byte),
					typeof(short),
					typeof(ushort),
					typeof(char)
				}
			},
			{
				typeof(ushort),
				new List<Type>
				{
					typeof(byte),
					typeof(char)
				}
			},
			{
				typeof(short),
				new List<Type>
				{
					typeof(byte)
				}
			}
		};

		// Token: 0x020001B2 RID: 434
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1
		{
			// Token: 0x06000B2C RID: 2860 RVA: 0x00011F8F File Offset: 0x0001018F
			public <>c__DisplayClass1()
			{
			}

			// Token: 0x06000B2D RID: 2861 RVA: 0x00011F98 File Offset: 0x00010198
			public bool <IsCastableFrom>b__0(MethodInfo m)
			{
				return m.ReturnType == this.to && m.Name == "op_Implicit";
			}

			// Token: 0x040002C6 RID: 710
			public Type to;
		}
	}
}
