﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace NUnit.Framework
{
	// Token: 0x02000142 RID: 322
	public class ListMapper
	{
		// Token: 0x06000878 RID: 2168 RVA: 0x0001D3A0 File Offset: 0x0001B5A0
		public ListMapper(ICollection original)
		{
			this.original = original;
		}

		// Token: 0x06000879 RID: 2169 RVA: 0x0001D3B4 File Offset: 0x0001B5B4
		public ICollection Property(string name)
		{
			List<object> list = new List<object>();
			foreach (object obj in this.original)
			{
				PropertyInfo property = obj.GetType().GetProperty(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
				if (property == null)
				{
					throw new ArgumentException(string.Format("{0} does not have a {1} property", obj, name));
				}
				list.Add(property.GetValue(obj, null));
			}
			return list;
		}

		// Token: 0x040001E3 RID: 483
		private ICollection original;
	}
}
