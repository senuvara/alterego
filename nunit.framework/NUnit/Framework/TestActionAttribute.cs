﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework
{
	// Token: 0x02000113 RID: 275
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Interface, AllowMultiple = true, Inherited = true)]
	public abstract class TestActionAttribute : Attribute, ITestAction
	{
		// Token: 0x06000774 RID: 1908 RVA: 0x0001B015 File Offset: 0x00019215
		public virtual void BeforeTest(ITest test)
		{
		}

		// Token: 0x06000775 RID: 1909 RVA: 0x0001B018 File Offset: 0x00019218
		public virtual void AfterTest(ITest test)
		{
		}

		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x06000776 RID: 1910 RVA: 0x0001B01C File Offset: 0x0001921C
		public virtual ActionTargets Targets
		{
			get
			{
				return ActionTargets.Default;
			}
		}

		// Token: 0x06000777 RID: 1911 RVA: 0x0001B02F File Offset: 0x0001922F
		protected TestActionAttribute()
		{
		}
	}
}
