﻿using System;

namespace NUnit.Framework
{
	// Token: 0x0200006C RID: 108
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	public class TestAssemblyDirectoryResolveAttribute : NUnitAttribute
	{
		// Token: 0x0600040C RID: 1036 RVA: 0x0000CF73 File Offset: 0x0000B173
		public TestAssemblyDirectoryResolveAttribute()
		{
		}
	}
}
