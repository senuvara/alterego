﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x0200010C RID: 268
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class ExplicitAttribute : NUnitAttribute, IApplyToTest
	{
		// Token: 0x0600075D RID: 1885 RVA: 0x0001A72E File Offset: 0x0001892E
		public ExplicitAttribute()
		{
		}

		// Token: 0x0600075E RID: 1886 RVA: 0x0001A739 File Offset: 0x00018939
		public ExplicitAttribute(string reason)
		{
			this._reason = reason;
		}

		// Token: 0x0600075F RID: 1887 RVA: 0x0001A74C File Offset: 0x0001894C
		public void ApplyToTest(Test test)
		{
			if (test.RunState != RunState.NotRunnable && test.RunState != RunState.Ignored)
			{
				test.RunState = RunState.Explicit;
				if (this._reason != null)
				{
					test.Properties.Set("_SKIPREASON", this._reason);
				}
			}
		}

		// Token: 0x040001A5 RID: 421
		private string _reason;
	}
}
