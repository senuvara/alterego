﻿using System;

namespace NUnit.Framework
{
	// Token: 0x0200007E RID: 126
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	[Obsolete("Use OneTimeTearDownAttribute")]
	public class TestFixtureTearDownAttribute : OneTimeTearDownAttribute
	{
		// Token: 0x06000466 RID: 1126 RVA: 0x0000EA20 File Offset: 0x0000CC20
		public TestFixtureTearDownAttribute()
		{
		}
	}
}
