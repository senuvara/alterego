﻿using System;
using System.Runtime.Serialization;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework
{
	// Token: 0x020000B9 RID: 185
	[Serializable]
	public abstract class ResultStateException : Exception
	{
		// Token: 0x0600059E RID: 1438 RVA: 0x0001314B File Offset: 0x0001134B
		public ResultStateException(string message) : base(message)
		{
		}

		// Token: 0x0600059F RID: 1439 RVA: 0x00013157 File Offset: 0x00011357
		public ResultStateException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x060005A0 RID: 1440 RVA: 0x00013164 File Offset: 0x00011364
		protected ResultStateException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x060005A1 RID: 1441
		public abstract ResultState ResultState { get; }
	}
}
