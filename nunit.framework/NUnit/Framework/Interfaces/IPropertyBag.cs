﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000022 RID: 34
	public interface IPropertyBag : IXmlNodeBuilder
	{
		// Token: 0x0600014B RID: 331
		void Add(string key, object value);

		// Token: 0x0600014C RID: 332
		void Set(string key, object value);

		// Token: 0x0600014D RID: 333
		object Get(string key);

		// Token: 0x0600014E RID: 334
		bool ContainsKey(string key);

		// Token: 0x1700006C RID: 108
		IList this[string key]
		{
			get;
			set;
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000151 RID: 337
		ICollection<string> Keys { get; }
	}
}
