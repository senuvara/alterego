﻿using System;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000012 RID: 18
	public interface IApplyToTest
	{
		// Token: 0x06000075 RID: 117
		void ApplyToTest(Test test);
	}
}
