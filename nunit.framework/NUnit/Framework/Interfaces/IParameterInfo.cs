﻿using System;
using System.Reflection;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x020000DC RID: 220
	public interface IParameterInfo : IReflectionInfo
	{
		// Token: 0x1700016F RID: 367
		// (get) Token: 0x0600067D RID: 1661
		bool IsOptional { get; }

		// Token: 0x17000170 RID: 368
		// (get) Token: 0x0600067E RID: 1662
		IMethodInfo Method { get; }

		// Token: 0x17000171 RID: 369
		// (get) Token: 0x0600067F RID: 1663
		ParameterInfo ParameterInfo { get; }

		// Token: 0x17000172 RID: 370
		// (get) Token: 0x06000680 RID: 1664
		Type ParameterType { get; }
	}
}
