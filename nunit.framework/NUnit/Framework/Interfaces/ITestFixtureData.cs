﻿using System;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x0200005A RID: 90
	public interface ITestFixtureData : ITestData
	{
		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x0600029E RID: 670
		Type[] TypeArgs { get; }
	}
}
