﻿using System;
using System.Reflection;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000042 RID: 66
	public interface ITypeInfo : IReflectionInfo
	{
		// Token: 0x17000087 RID: 135
		// (get) Token: 0x060001EA RID: 490
		Type Type { get; }

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x060001EB RID: 491
		ITypeInfo BaseType { get; }

		// Token: 0x060001EC RID: 492
		bool IsType(Type type);

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x060001ED RID: 493
		string Name { get; }

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x060001EE RID: 494
		string FullName { get; }

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x060001EF RID: 495
		Assembly Assembly { get; }

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x060001F0 RID: 496
		string Namespace { get; }

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x060001F1 RID: 497
		bool IsAbstract { get; }

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x060001F2 RID: 498
		bool IsGenericType { get; }

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060001F3 RID: 499
		bool ContainsGenericParameters { get; }

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060001F4 RID: 500
		bool IsGenericTypeDefinition { get; }

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060001F5 RID: 501
		bool IsSealed { get; }

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060001F6 RID: 502
		bool IsStaticClass { get; }

		// Token: 0x060001F7 RID: 503
		string GetDisplayName();

		// Token: 0x060001F8 RID: 504
		string GetDisplayName(object[] args);

		// Token: 0x060001F9 RID: 505
		Type GetGenericTypeDefinition();

		// Token: 0x060001FA RID: 506
		ITypeInfo MakeGenericType(Type[] typeArgs);

		// Token: 0x060001FB RID: 507
		bool HasMethodWithAttribute(Type attrType);

		// Token: 0x060001FC RID: 508
		IMethodInfo[] GetMethods(BindingFlags flags);

		// Token: 0x060001FD RID: 509
		ConstructorInfo GetConstructor(Type[] argTypes);

		// Token: 0x060001FE RID: 510
		bool HasConstructor(Type[] argTypes);

		// Token: 0x060001FF RID: 511
		object Construct(object[] args);
	}
}
