﻿using System;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x0200005C RID: 92
	public interface ITestCaseData : ITestData
	{
		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x060002BC RID: 700
		object ExpectedResult { get; }

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x060002BD RID: 701
		bool HasExpectedResult { get; }
	}
}
