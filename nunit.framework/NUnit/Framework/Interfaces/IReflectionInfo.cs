﻿using System;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x0200001A RID: 26
	public interface IReflectionInfo
	{
		// Token: 0x060000B3 RID: 179
		T[] GetCustomAttributes<T>(bool inherit) where T : class;

		// Token: 0x060000B4 RID: 180
		bool IsDefined<T>(bool inherit);
	}
}
