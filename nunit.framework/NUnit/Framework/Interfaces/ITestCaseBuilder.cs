﻿using System;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000072 RID: 114
	public interface ITestCaseBuilder
	{
		// Token: 0x06000427 RID: 1063
		bool CanBuildFrom(IMethodInfo method, Test suite);

		// Token: 0x06000428 RID: 1064
		Test BuildFrom(IMethodInfo method, Test suite);
	}
}
