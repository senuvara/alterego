﻿using System;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000081 RID: 129
	public interface IApplyToContext
	{
		// Token: 0x06000470 RID: 1136
		void ApplyToContext(ITestExecutionContext context);
	}
}
