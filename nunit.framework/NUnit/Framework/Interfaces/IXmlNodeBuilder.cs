﻿using System;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000003 RID: 3
	public interface IXmlNodeBuilder
	{
		// Token: 0x06000018 RID: 24
		TNode ToXml(bool recursive);

		// Token: 0x06000019 RID: 25
		TNode AddToXml(TNode parentNode, bool recursive);
	}
}
