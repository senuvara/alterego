﻿using System;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x020001A6 RID: 422
	public enum FailureSite
	{
		// Token: 0x04000297 RID: 663
		Test,
		// Token: 0x04000298 RID: 664
		SetUp,
		// Token: 0x04000299 RID: 665
		TearDown,
		// Token: 0x0400029A RID: 666
		Parent,
		// Token: 0x0400029B RID: 667
		Child
	}
}
