﻿using System;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x0200003F RID: 63
	public interface ITestListener
	{
		// Token: 0x060001DF RID: 479
		void TestStarted(ITest test);

		// Token: 0x060001E0 RID: 480
		void TestFinished(ITestResult result);

		// Token: 0x060001E1 RID: 481
		void TestOutput(TestOutput output);
	}
}
