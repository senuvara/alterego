﻿using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x020001A5 RID: 421
	public class ResultState
	{
		// Token: 0x06000AD2 RID: 2770 RVA: 0x000243D4 File Offset: 0x000225D4
		public ResultState(TestStatus status) : this(status, string.Empty, FailureSite.Test)
		{
		}

		// Token: 0x06000AD3 RID: 2771 RVA: 0x000243E6 File Offset: 0x000225E6
		public ResultState(TestStatus status, string label) : this(status, label, FailureSite.Test)
		{
		}

		// Token: 0x06000AD4 RID: 2772 RVA: 0x000243F4 File Offset: 0x000225F4
		public ResultState(TestStatus status, FailureSite site) : this(status, string.Empty, site)
		{
		}

		// Token: 0x06000AD5 RID: 2773 RVA: 0x00024406 File Offset: 0x00022606
		public ResultState(TestStatus status, string label, FailureSite site)
		{
			this.Status = status;
			this.Label = ((label == null) ? string.Empty : label);
			this.Site = site;
		}

		// Token: 0x17000271 RID: 625
		// (get) Token: 0x06000AD6 RID: 2774 RVA: 0x00024434 File Offset: 0x00022634
		// (set) Token: 0x06000AD7 RID: 2775 RVA: 0x0002444B File Offset: 0x0002264B
		public TestStatus Status
		{
			[CompilerGenerated]
			get
			{
				return this.<Status>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Status>k__BackingField = value;
			}
		}

		// Token: 0x17000272 RID: 626
		// (get) Token: 0x06000AD8 RID: 2776 RVA: 0x00024454 File Offset: 0x00022654
		// (set) Token: 0x06000AD9 RID: 2777 RVA: 0x0002446B File Offset: 0x0002266B
		public string Label
		{
			[CompilerGenerated]
			get
			{
				return this.<Label>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Label>k__BackingField = value;
			}
		}

		// Token: 0x17000273 RID: 627
		// (get) Token: 0x06000ADA RID: 2778 RVA: 0x00024474 File Offset: 0x00022674
		// (set) Token: 0x06000ADB RID: 2779 RVA: 0x0002448B File Offset: 0x0002268B
		public FailureSite Site
		{
			[CompilerGenerated]
			get
			{
				return this.<Site>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Site>k__BackingField = value;
			}
		}

		// Token: 0x06000ADC RID: 2780 RVA: 0x00024494 File Offset: 0x00022694
		public ResultState WithSite(FailureSite site)
		{
			return new ResultState(this.Status, this.Label, site);
		}

		// Token: 0x06000ADD RID: 2781 RVA: 0x000244B8 File Offset: 0x000226B8
		public override bool Equals(object obj)
		{
			ResultState resultState = obj as ResultState;
			return resultState != null && (this.Status.Equals(resultState.Status) && this.Label.Equals(resultState.Label)) && this.Site.Equals(resultState.Site);
		}

		// Token: 0x06000ADE RID: 2782 RVA: 0x00024530 File Offset: 0x00022730
		public override int GetHashCode()
		{
			return (int)((int)this.Status << (int)((FailureSite)8 + (int)this.Site & (FailureSite)31) ^ (TestStatus)this.Label.GetHashCode());
		}

		// Token: 0x06000ADF RID: 2783 RVA: 0x00024560 File Offset: 0x00022760
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(this.Status.ToString());
			if (this.Label != null && this.Label.Length > 0)
			{
				stringBuilder.AppendFormat(":{0}", this.Label);
			}
			if (this.Site != FailureSite.Test)
			{
				stringBuilder.AppendFormat("({0})", this.Site.ToString());
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000AE0 RID: 2784 RVA: 0x000245EC File Offset: 0x000227EC
		// Note: this type is marked as 'beforefieldinit'.
		static ResultState()
		{
		}

		// Token: 0x04000286 RID: 646
		public static readonly ResultState Inconclusive = new ResultState(TestStatus.Inconclusive);

		// Token: 0x04000287 RID: 647
		public static readonly ResultState Skipped = new ResultState(TestStatus.Skipped);

		// Token: 0x04000288 RID: 648
		public static readonly ResultState Ignored = new ResultState(TestStatus.Skipped, "Ignored");

		// Token: 0x04000289 RID: 649
		public static readonly ResultState Explicit = new ResultState(TestStatus.Skipped, "Explicit");

		// Token: 0x0400028A RID: 650
		public static readonly ResultState Success = new ResultState(TestStatus.Passed);

		// Token: 0x0400028B RID: 651
		public static readonly ResultState Failure = new ResultState(TestStatus.Failed);

		// Token: 0x0400028C RID: 652
		public static readonly ResultState Error = new ResultState(TestStatus.Failed, "Error");

		// Token: 0x0400028D RID: 653
		public static readonly ResultState Cancelled = new ResultState(TestStatus.Failed, "Cancelled");

		// Token: 0x0400028E RID: 654
		public static readonly ResultState NotRunnable = new ResultState(TestStatus.Failed, "Invalid");

		// Token: 0x0400028F RID: 655
		public static readonly ResultState ChildFailure = ResultState.Failure.WithSite(FailureSite.Child);

		// Token: 0x04000290 RID: 656
		public static readonly ResultState SetUpFailure = ResultState.Failure.WithSite(FailureSite.SetUp);

		// Token: 0x04000291 RID: 657
		public static readonly ResultState SetUpError = ResultState.Error.WithSite(FailureSite.SetUp);

		// Token: 0x04000292 RID: 658
		public static readonly ResultState TearDownError = ResultState.Error.WithSite(FailureSite.TearDown);

		// Token: 0x04000293 RID: 659
		[CompilerGenerated]
		private TestStatus <Status>k__BackingField;

		// Token: 0x04000294 RID: 660
		[CompilerGenerated]
		private string <Label>k__BackingField;

		// Token: 0x04000295 RID: 661
		[CompilerGenerated]
		private FailureSite <Site>k__BackingField;
	}
}
