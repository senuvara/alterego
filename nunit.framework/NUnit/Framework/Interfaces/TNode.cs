﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Xml;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x0200019B RID: 411
	public class TNode
	{
		// Token: 0x06000A98 RID: 2712 RVA: 0x000238EB File Offset: 0x00021AEB
		public TNode(string name)
		{
			this.Name = name;
			this.Attributes = new AttributeDictionary();
			this.ChildNodes = new NodeList();
		}

		// Token: 0x06000A99 RID: 2713 RVA: 0x00023916 File Offset: 0x00021B16
		public TNode(string name, string value) : this(name, value, false)
		{
		}

		// Token: 0x06000A9A RID: 2714 RVA: 0x00023924 File Offset: 0x00021B24
		public TNode(string name, string value, bool valueIsCDATA) : this(name)
		{
			this.Value = value;
			this.ValueIsCDATA = valueIsCDATA;
		}

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x06000A9B RID: 2715 RVA: 0x00023940 File Offset: 0x00021B40
		// (set) Token: 0x06000A9C RID: 2716 RVA: 0x00023957 File Offset: 0x00021B57
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Name>k__BackingField = value;
			}
		}

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x06000A9D RID: 2717 RVA: 0x00023960 File Offset: 0x00021B60
		// (set) Token: 0x06000A9E RID: 2718 RVA: 0x00023977 File Offset: 0x00021B77
		public string Value
		{
			[CompilerGenerated]
			get
			{
				return this.<Value>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Value>k__BackingField = value;
			}
		}

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x06000A9F RID: 2719 RVA: 0x00023980 File Offset: 0x00021B80
		// (set) Token: 0x06000AA0 RID: 2720 RVA: 0x00023997 File Offset: 0x00021B97
		public bool ValueIsCDATA
		{
			[CompilerGenerated]
			get
			{
				return this.<ValueIsCDATA>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ValueIsCDATA>k__BackingField = value;
			}
		}

		// Token: 0x17000265 RID: 613
		// (get) Token: 0x06000AA1 RID: 2721 RVA: 0x000239A0 File Offset: 0x00021BA0
		// (set) Token: 0x06000AA2 RID: 2722 RVA: 0x000239B7 File Offset: 0x00021BB7
		public AttributeDictionary Attributes
		{
			[CompilerGenerated]
			get
			{
				return this.<Attributes>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Attributes>k__BackingField = value;
			}
		}

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x06000AA3 RID: 2723 RVA: 0x000239C0 File Offset: 0x00021BC0
		// (set) Token: 0x06000AA4 RID: 2724 RVA: 0x000239D7 File Offset: 0x00021BD7
		public NodeList ChildNodes
		{
			[CompilerGenerated]
			get
			{
				return this.<ChildNodes>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ChildNodes>k__BackingField = value;
			}
		}

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x06000AA5 RID: 2725 RVA: 0x000239E0 File Offset: 0x00021BE0
		public TNode FirstChild
		{
			get
			{
				return (this.ChildNodes.Count == 0) ? null : this.ChildNodes[0];
			}
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x06000AA6 RID: 2726 RVA: 0x00023A10 File Offset: 0x00021C10
		public string OuterXml
		{
			get
			{
				StringWriter stringWriter = new StringWriter();
				using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, new XmlWriterSettings
				{
					ConformanceLevel = ConformanceLevel.Fragment
				}))
				{
					this.WriteTo(xmlWriter);
				}
				return stringWriter.ToString();
			}
		}

		// Token: 0x06000AA7 RID: 2727 RVA: 0x00023A74 File Offset: 0x00021C74
		public static TNode FromXml(string xmlText)
		{
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.LoadXml(xmlText);
			return TNode.FromXml(xmlDocument.FirstChild);
		}

		// Token: 0x06000AA8 RID: 2728 RVA: 0x00023AA0 File Offset: 0x00021CA0
		public TNode AddElement(string name)
		{
			TNode tnode = new TNode(name);
			this.ChildNodes.Add(tnode);
			return tnode;
		}

		// Token: 0x06000AA9 RID: 2729 RVA: 0x00023AC8 File Offset: 0x00021CC8
		public TNode AddElement(string name, string value)
		{
			TNode tnode = new TNode(name, TNode.EscapeInvalidXmlCharacters(value));
			this.ChildNodes.Add(tnode);
			return tnode;
		}

		// Token: 0x06000AAA RID: 2730 RVA: 0x00023AF8 File Offset: 0x00021CF8
		public TNode AddElementWithCDATA(string name, string value)
		{
			TNode tnode = new TNode(name, TNode.EscapeInvalidXmlCharacters(value), true);
			this.ChildNodes.Add(tnode);
			return tnode;
		}

		// Token: 0x06000AAB RID: 2731 RVA: 0x00023B26 File Offset: 0x00021D26
		public void AddAttribute(string name, string value)
		{
			this.Attributes.Add(name, TNode.EscapeInvalidXmlCharacters(value));
		}

		// Token: 0x06000AAC RID: 2732 RVA: 0x00023B3C File Offset: 0x00021D3C
		public TNode SelectSingleNode(string xpath)
		{
			NodeList nodeList = this.SelectNodes(xpath);
			return (nodeList.Count > 0) ? nodeList[0] : null;
		}

		// Token: 0x06000AAD RID: 2733 RVA: 0x00023B6C File Offset: 0x00021D6C
		public NodeList SelectNodes(string xpath)
		{
			return TNode.ApplySelection(new NodeList
			{
				this
			}, xpath);
		}

		// Token: 0x06000AAE RID: 2734 RVA: 0x00023B94 File Offset: 0x00021D94
		public void WriteTo(XmlWriter writer)
		{
			writer.WriteStartElement(this.Name);
			foreach (string text in this.Attributes.Keys)
			{
				writer.WriteAttributeString(text, this.Attributes[text]);
			}
			if (this.Value != null)
			{
				if (this.ValueIsCDATA)
				{
					this.WriteCDataTo(writer);
				}
				else
				{
					writer.WriteString(this.Value);
				}
			}
			foreach (TNode tnode in this.ChildNodes)
			{
				tnode.WriteTo(writer);
			}
			writer.WriteEndElement();
		}

		// Token: 0x06000AAF RID: 2735 RVA: 0x00023C90 File Offset: 0x00021E90
		private static TNode FromXml(XmlNode xmlNode)
		{
			TNode tnode = new TNode(xmlNode.Name, xmlNode.InnerText);
			foreach (object obj in xmlNode.Attributes)
			{
				XmlAttribute xmlAttribute = (XmlAttribute)obj;
				tnode.AddAttribute(xmlAttribute.Name, xmlAttribute.Value);
			}
			foreach (object obj2 in xmlNode.ChildNodes)
			{
				XmlNode xmlNode2 = (XmlNode)obj2;
				if (xmlNode2.NodeType == XmlNodeType.Element)
				{
					tnode.ChildNodes.Add(TNode.FromXml(xmlNode2));
				}
			}
			return tnode;
		}

		// Token: 0x06000AB0 RID: 2736 RVA: 0x00023D9C File Offset: 0x00021F9C
		private static NodeList ApplySelection(NodeList nodeList, string xpath)
		{
			Guard.ArgumentNotNullOrEmpty(xpath, "xpath");
			if (xpath[0] == '/')
			{
				throw new ArgumentException("XPath expressions starting with '/' are not supported", "xpath");
			}
			if (xpath.IndexOf("//") >= 0)
			{
				throw new ArgumentException("XPath expressions with '//' are not supported", "xpath");
			}
			string xpath2 = xpath;
			string text = null;
			int num = xpath.IndexOf('/');
			if (num >= 0)
			{
				xpath2 = xpath.Substring(0, num);
				text = xpath.Substring(num + 1);
			}
			NodeList nodeList2 = new NodeList();
			TNode.NodeFilter nodeFilter = new TNode.NodeFilter(xpath2);
			foreach (TNode tnode in nodeList)
			{
				foreach (TNode tnode2 in tnode.ChildNodes)
				{
					if (nodeFilter.Pass(tnode2))
					{
						nodeList2.Add(tnode2);
					}
				}
			}
			return (text != null) ? TNode.ApplySelection(nodeList2, text) : nodeList2;
		}

		// Token: 0x06000AB1 RID: 2737 RVA: 0x00023F14 File Offset: 0x00022114
		private static string EscapeInvalidXmlCharacters(string str)
		{
			string result;
			if (str == null)
			{
				result = null;
			}
			else
			{
				result = TNode.InvalidXmlCharactersRegex.Replace(str, (Match match) => TNode.CharToUnicodeSequence(match.Value[0]));
			}
			return result;
		}

		// Token: 0x06000AB2 RID: 2738 RVA: 0x00023F60 File Offset: 0x00022160
		private static string CharToUnicodeSequence(char symbol)
		{
			string format = "\\u{0}";
			int num = (int)symbol;
			return string.Format(format, num.ToString("x4"));
		}

		// Token: 0x06000AB3 RID: 2739 RVA: 0x00023F8C File Offset: 0x0002218C
		private void WriteCDataTo(XmlWriter writer)
		{
			int num = 0;
			string value = this.Value;
			for (;;)
			{
				int num2 = value.IndexOf("]]>", num);
				if (num2 < 0)
				{
					break;
				}
				writer.WriteCData(value.Substring(num, num2 - num + 2));
				num = num2 + 2;
				if (num >= value.Length)
				{
					return;
				}
			}
			if (num > 0)
			{
				writer.WriteCData(value.Substring(num));
			}
			else
			{
				writer.WriteCData(value);
			}
		}

		// Token: 0x06000AB4 RID: 2740 RVA: 0x00023EF0 File Offset: 0x000220F0
		[CompilerGenerated]
		private static string <EscapeInvalidXmlCharacters>b__0(Match match)
		{
			return TNode.CharToUnicodeSequence(match.Value[0]);
		}

		// Token: 0x06000AB5 RID: 2741 RVA: 0x0002400E File Offset: 0x0002220E
		// Note: this type is marked as 'beforefieldinit'.
		static TNode()
		{
		}

		// Token: 0x04000277 RID: 631
		private static readonly Regex InvalidXmlCharactersRegex = new Regex("[^\t\n\r -�]|([\ud800-\udbff](?![\udc00-\udfff]))|((?<![\ud800-\udbff])[\udc00-\udfff])");

		// Token: 0x04000278 RID: 632
		[CompilerGenerated]
		private string <Name>k__BackingField;

		// Token: 0x04000279 RID: 633
		[CompilerGenerated]
		private string <Value>k__BackingField;

		// Token: 0x0400027A RID: 634
		[CompilerGenerated]
		private bool <ValueIsCDATA>k__BackingField;

		// Token: 0x0400027B RID: 635
		[CompilerGenerated]
		private AttributeDictionary <Attributes>k__BackingField;

		// Token: 0x0400027C RID: 636
		[CompilerGenerated]
		private NodeList <ChildNodes>k__BackingField;

		// Token: 0x0400027D RID: 637
		[CompilerGenerated]
		private static MatchEvaluator CS$<>9__CachedAnonymousMethodDelegate1;

		// Token: 0x0200019C RID: 412
		private class NodeFilter
		{
			// Token: 0x06000AB6 RID: 2742 RVA: 0x00024028 File Offset: 0x00022228
			public NodeFilter(string xpath)
			{
				this._nodeName = xpath;
				int num = xpath.IndexOf('[');
				if (num >= 0)
				{
					if (!xpath.EndsWith("]"))
					{
						throw new ArgumentException("Invalid property expression", "xpath");
					}
					this._nodeName = xpath.Substring(0, num);
					string text = xpath.Substring(num + 1, xpath.Length - num - 2);
					int num2 = text.IndexOf('=');
					if (num2 < 0 || text[0] != '@')
					{
						throw new ArgumentException("Invalid property expression", "xpath");
					}
					this._propName = text.Substring(1, num2 - 1).Trim();
					this._propValue = text.Substring(num2 + 1).Trim(new char[]
					{
						' ',
						'"',
						'\''
					});
				}
			}

			// Token: 0x06000AB7 RID: 2743 RVA: 0x00024108 File Offset: 0x00022308
			public bool Pass(TNode node)
			{
				return !(node.Name != this._nodeName) && (this._propName == null || node.Attributes[this._propName] == this._propValue);
			}

			// Token: 0x0400027E RID: 638
			private string _nodeName;

			// Token: 0x0400027F RID: 639
			private string _propName;

			// Token: 0x04000280 RID: 640
			private string _propValue;
		}
	}
}
