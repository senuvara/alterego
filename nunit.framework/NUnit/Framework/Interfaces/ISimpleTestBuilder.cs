﻿using System;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x020000C5 RID: 197
	public interface ISimpleTestBuilder
	{
		// Token: 0x06000600 RID: 1536
		TestMethod BuildFrom(IMethodInfo method, Test suite);
	}
}
