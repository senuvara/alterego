﻿using System;
using System.Collections.Generic;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x0200001C RID: 28
	public interface ITestResult : IXmlNodeBuilder
	{
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000BD RID: 189
		ResultState ResultState { get; }

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000BE RID: 190
		string Name { get; }

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000BF RID: 191
		string FullName { get; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000C0 RID: 192
		double Duration { get; }

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000C1 RID: 193
		DateTime StartTime { get; }

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000C2 RID: 194
		DateTime EndTime { get; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000C3 RID: 195
		string Message { get; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000C4 RID: 196
		string StackTrace { get; }

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000C5 RID: 197
		int AssertCount { get; }

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000C6 RID: 198
		int FailCount { get; }

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000C7 RID: 199
		int PassCount { get; }

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000C8 RID: 200
		int SkipCount { get; }

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000C9 RID: 201
		int InconclusiveCount { get; }

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000CA RID: 202
		bool HasChildren { get; }

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000CB RID: 203
		IEnumerable<ITestResult> Children { get; }

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000CC RID: 204
		ITest Test { get; }

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000CD RID: 205
		string Output { get; }
	}
}
