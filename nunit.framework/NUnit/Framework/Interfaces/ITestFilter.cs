﻿using System;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000004 RID: 4
	public interface ITestFilter : IXmlNodeBuilder
	{
		// Token: 0x0600001A RID: 26
		bool Pass(ITest test);

		// Token: 0x0600001B RID: 27
		bool IsExplicitMatch(ITest test);
	}
}
