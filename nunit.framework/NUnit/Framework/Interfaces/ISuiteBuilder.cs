﻿using System;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x0200010F RID: 271
	public interface ISuiteBuilder
	{
		// Token: 0x06000769 RID: 1897
		bool CanBuildFrom(ITypeInfo typeInfo);

		// Token: 0x0600076A RID: 1898
		TestSuite BuildFrom(ITypeInfo typeInfo);
	}
}
