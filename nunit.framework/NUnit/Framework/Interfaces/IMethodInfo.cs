﻿using System;
using System.Reflection;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x020000F5 RID: 245
	public interface IMethodInfo : IReflectionInfo
	{
		// Token: 0x17000183 RID: 387
		// (get) Token: 0x060006F4 RID: 1780
		ITypeInfo TypeInfo { get; }

		// Token: 0x17000184 RID: 388
		// (get) Token: 0x060006F5 RID: 1781
		MethodInfo MethodInfo { get; }

		// Token: 0x17000185 RID: 389
		// (get) Token: 0x060006F6 RID: 1782
		string Name { get; }

		// Token: 0x17000186 RID: 390
		// (get) Token: 0x060006F7 RID: 1783
		bool IsAbstract { get; }

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x060006F8 RID: 1784
		bool IsPublic { get; }

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x060006F9 RID: 1785
		bool ContainsGenericParameters { get; }

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x060006FA RID: 1786
		bool IsGenericMethod { get; }

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x060006FB RID: 1787
		bool IsGenericMethodDefinition { get; }

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x060006FC RID: 1788
		ITypeInfo ReturnType { get; }

		// Token: 0x060006FD RID: 1789
		IParameterInfo[] GetParameters();

		// Token: 0x060006FE RID: 1790
		Type[] GetGenericArguments();

		// Token: 0x060006FF RID: 1791
		IMethodInfo MakeGenericMethod(params Type[] typeArguments);

		// Token: 0x06000700 RID: 1792
		object Invoke(object fixture, params object[] args);
	}
}
