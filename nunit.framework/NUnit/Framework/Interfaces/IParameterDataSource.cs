﻿using System;
using System.Collections;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000033 RID: 51
	public interface IParameterDataSource
	{
		// Token: 0x0600019F RID: 415
		IEnumerable GetData(IParameterInfo parameter);
	}
}
