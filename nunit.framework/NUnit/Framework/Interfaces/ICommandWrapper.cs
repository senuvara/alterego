﻿using System;
using NUnit.Framework.Internal.Commands;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000014 RID: 20
	public interface ICommandWrapper
	{
		// Token: 0x0600007D RID: 125
		TestCommand Wrap(TestCommand command);
	}
}
