﻿using System;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000059 RID: 89
	public interface ITestData
	{
		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x0600029A RID: 666
		string TestName { get; }

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x0600029B RID: 667
		RunState RunState { get; }

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x0600029C RID: 668
		object[] Arguments { get; }

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x0600029D RID: 669
		IPropertyBag Properties { get; }
	}
}
