﻿using System;
using System.Collections;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000050 RID: 80
	public interface IParameterDataProvider
	{
		// Token: 0x0600026F RID: 623
		bool HasDataFor(IParameterInfo parameter);

		// Token: 0x06000270 RID: 624
		IEnumerable GetDataFor(IParameterInfo parameter);
	}
}
