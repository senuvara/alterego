﻿using System;
using System.Collections.Generic;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000018 RID: 24
	public interface IFixtureBuilder
	{
		// Token: 0x060000B1 RID: 177
		IEnumerable<TestSuite> BuildFrom(ITypeInfo typeInfo);
	}
}
