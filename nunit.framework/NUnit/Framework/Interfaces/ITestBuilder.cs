﻿using System;
using System.Collections.Generic;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000019 RID: 25
	public interface ITestBuilder
	{
		// Token: 0x060000B2 RID: 178
		IEnumerable<TestMethod> BuildFrom(IMethodInfo method, Test suite);
	}
}
