﻿using System;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x020000A1 RID: 161
	public enum TestStatus
	{
		// Token: 0x040000ED RID: 237
		Inconclusive,
		// Token: 0x040000EE RID: 238
		Skipped,
		// Token: 0x040000EF RID: 239
		Passed,
		// Token: 0x040000F0 RID: 240
		Failed
	}
}
