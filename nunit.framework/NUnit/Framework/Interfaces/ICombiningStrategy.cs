﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000028 RID: 40
	public interface ICombiningStrategy
	{
		// Token: 0x06000172 RID: 370
		IEnumerable<ITestCaseData> GetTestCases(IEnumerable[] sources);
	}
}
