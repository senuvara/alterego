﻿using System;
using System.Collections.Generic;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x0200019E RID: 414
	public class AttributeDictionary : Dictionary<string, string>
	{
		// Token: 0x17000269 RID: 617
		public new string this[string key]
		{
			get
			{
				string text;
				string result;
				if (base.TryGetValue(key, out text))
				{
					result = text;
				}
				else
				{
					result = null;
				}
				return result;
			}
		}

		// Token: 0x06000ABA RID: 2746 RVA: 0x00024197 File Offset: 0x00022397
		public AttributeDictionary()
		{
		}
	}
}
