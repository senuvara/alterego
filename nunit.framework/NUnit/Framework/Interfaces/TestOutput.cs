﻿using System;
using System.Runtime.CompilerServices;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x020000F4 RID: 244
	public class TestOutput
	{
		// Token: 0x060006EB RID: 1771 RVA: 0x00019815 File Offset: 0x00017A15
		public TestOutput(string text, string stream, string testName)
		{
			this.Text = text;
			this.Stream = stream;
			this.TestName = testName;
		}

		// Token: 0x060006EC RID: 1772 RVA: 0x00019838 File Offset: 0x00017A38
		public override string ToString()
		{
			return this.Stream + ": " + this.Text;
		}

		// Token: 0x17000180 RID: 384
		// (get) Token: 0x060006ED RID: 1773 RVA: 0x00019860 File Offset: 0x00017A60
		// (set) Token: 0x060006EE RID: 1774 RVA: 0x00019877 File Offset: 0x00017A77
		public string Text
		{
			[CompilerGenerated]
			get
			{
				return this.<Text>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Text>k__BackingField = value;
			}
		}

		// Token: 0x17000181 RID: 385
		// (get) Token: 0x060006EF RID: 1775 RVA: 0x00019880 File Offset: 0x00017A80
		// (set) Token: 0x060006F0 RID: 1776 RVA: 0x00019897 File Offset: 0x00017A97
		public string Stream
		{
			[CompilerGenerated]
			get
			{
				return this.<Stream>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Stream>k__BackingField = value;
			}
		}

		// Token: 0x17000182 RID: 386
		// (get) Token: 0x060006F1 RID: 1777 RVA: 0x000198A0 File Offset: 0x00017AA0
		// (set) Token: 0x060006F2 RID: 1778 RVA: 0x000198B7 File Offset: 0x00017AB7
		public string TestName
		{
			[CompilerGenerated]
			get
			{
				return this.<TestName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<TestName>k__BackingField = value;
			}
		}

		// Token: 0x060006F3 RID: 1779 RVA: 0x000198C0 File Offset: 0x00017AC0
		public string ToXml()
		{
			TNode tnode = new TNode("test-output", this.Text, true);
			tnode.AddAttribute("stream", this.Stream);
			if (this.TestName != null)
			{
				tnode.AddAttribute("testname", this.TestName);
			}
			return tnode.OuterXml;
		}

		// Token: 0x0400018E RID: 398
		[CompilerGenerated]
		private string <Text>k__BackingField;

		// Token: 0x0400018F RID: 399
		[CompilerGenerated]
		private string <Stream>k__BackingField;

		// Token: 0x04000190 RID: 400
		[CompilerGenerated]
		private string <TestName>k__BackingField;
	}
}
