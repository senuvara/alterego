﻿using System;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x02000084 RID: 132
	public enum RunState
	{
		// Token: 0x040000D0 RID: 208
		NotRunnable,
		// Token: 0x040000D1 RID: 209
		Runnable,
		// Token: 0x040000D2 RID: 210
		Explicit,
		// Token: 0x040000D3 RID: 211
		Skipped,
		// Token: 0x040000D4 RID: 212
		Ignored
	}
}
