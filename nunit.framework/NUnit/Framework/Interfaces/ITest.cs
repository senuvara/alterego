﻿using System;
using System.Collections.Generic;

namespace NUnit.Framework.Interfaces
{
	// Token: 0x0200001F RID: 31
	public interface ITest : IXmlNodeBuilder
	{
		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000FE RID: 254
		string Id { get; }

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000FF RID: 255
		string Name { get; }

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000100 RID: 256
		string FullName { get; }

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000101 RID: 257
		string ClassName { get; }

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000102 RID: 258
		string MethodName { get; }

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000103 RID: 259
		ITypeInfo TypeInfo { get; }

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000104 RID: 260
		IMethodInfo Method { get; }

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000105 RID: 261
		RunState RunState { get; }

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000106 RID: 262
		int TestCaseCount { get; }

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000107 RID: 263
		IPropertyBag Properties { get; }

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000108 RID: 264
		ITest Parent { get; }

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000109 RID: 265
		bool IsSuite { get; }

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600010A RID: 266
		bool HasChildren { get; }

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600010B RID: 267
		IList<ITest> Tests { get; }

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x0600010C RID: 268
		object Fixture { get; }
	}
}
