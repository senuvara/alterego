﻿using System;

namespace NUnit.Framework
{
	// Token: 0x0200009B RID: 155
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class PostTestAttribute : NUnitAttribute
	{
		// Token: 0x060004D0 RID: 1232 RVA: 0x0000FEEE File Offset: 0x0000E0EE
		public PostTestAttribute()
		{
		}
	}
}
