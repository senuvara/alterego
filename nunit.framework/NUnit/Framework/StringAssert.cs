﻿using System;
using System.ComponentModel;

namespace NUnit.Framework
{
	// Token: 0x02000002 RID: 2
	public class StringAssert
	{
		// Token: 0x06000001 RID: 1 RVA: 0x000020D0 File Offset: 0x000002D0
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new static bool Equals(object a, object b)
		{
			throw new InvalidOperationException("StringAssert.Equals should not be used for Assertions");
		}

		// Token: 0x06000002 RID: 2 RVA: 0x000020DD File Offset: 0x000002DD
		public new static void ReferenceEquals(object a, object b)
		{
			throw new InvalidOperationException("StringAssert.ReferenceEquals should not be used for Assertions");
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020EA File Offset: 0x000002EA
		public static void Contains(string expected, string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, Does.Contain(expected), message, args);
		}

		// Token: 0x06000004 RID: 4 RVA: 0x000020FC File Offset: 0x000002FC
		public static void Contains(string expected, string actual)
		{
			StringAssert.Contains(expected, actual, string.Empty, null);
		}

		// Token: 0x06000005 RID: 5 RVA: 0x0000210D File Offset: 0x0000030D
		public static void DoesNotContain(string expected, string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, Does.Not.Contain(expected), message, args);
		}

		// Token: 0x06000006 RID: 6 RVA: 0x00002124 File Offset: 0x00000324
		public static void DoesNotContain(string expected, string actual)
		{
			StringAssert.DoesNotContain(expected, actual, string.Empty, null);
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002135 File Offset: 0x00000335
		public static void StartsWith(string expected, string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, Does.StartWith(expected), message, args);
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00002147 File Offset: 0x00000347
		public static void StartsWith(string expected, string actual)
		{
			StringAssert.StartsWith(expected, actual, string.Empty, null);
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002158 File Offset: 0x00000358
		public static void DoesNotStartWith(string expected, string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, Does.Not.StartWith(expected), message, args);
		}

		// Token: 0x0600000A RID: 10 RVA: 0x0000216F File Offset: 0x0000036F
		public static void DoesNotStartWith(string expected, string actual)
		{
			StringAssert.DoesNotStartWith(expected, actual, string.Empty, null);
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002180 File Offset: 0x00000380
		public static void EndsWith(string expected, string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, Does.EndWith(expected), message, args);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002192 File Offset: 0x00000392
		public static void EndsWith(string expected, string actual)
		{
			StringAssert.EndsWith(expected, actual, string.Empty, null);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000021A3 File Offset: 0x000003A3
		public static void DoesNotEndWith(string expected, string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, Does.Not.EndWith(expected), message, args);
		}

		// Token: 0x0600000E RID: 14 RVA: 0x000021BA File Offset: 0x000003BA
		public static void DoesNotEndWith(string expected, string actual)
		{
			StringAssert.DoesNotEndWith(expected, actual, string.Empty, null);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000021CB File Offset: 0x000003CB
		public static void AreEqualIgnoringCase(string expected, string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, Is.EqualTo(expected).IgnoreCase, message, args);
		}

		// Token: 0x06000010 RID: 16 RVA: 0x000021E2 File Offset: 0x000003E2
		public static void AreEqualIgnoringCase(string expected, string actual)
		{
			StringAssert.AreEqualIgnoringCase(expected, actual, string.Empty, null);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x000021F3 File Offset: 0x000003F3
		public static void AreNotEqualIgnoringCase(string expected, string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, Is.Not.EqualTo(expected).IgnoreCase, message, args);
		}

		// Token: 0x06000012 RID: 18 RVA: 0x0000220F File Offset: 0x0000040F
		public static void AreNotEqualIgnoringCase(string expected, string actual)
		{
			StringAssert.AreNotEqualIgnoringCase(expected, actual, string.Empty, null);
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00002220 File Offset: 0x00000420
		public static void IsMatch(string pattern, string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, Does.Match(pattern), message, args);
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002232 File Offset: 0x00000432
		public static void IsMatch(string pattern, string actual)
		{
			StringAssert.IsMatch(pattern, actual, string.Empty, null);
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002243 File Offset: 0x00000443
		public static void DoesNotMatch(string pattern, string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, Does.Not.Match(pattern), message, args);
		}

		// Token: 0x06000016 RID: 22 RVA: 0x0000225A File Offset: 0x0000045A
		public static void DoesNotMatch(string pattern, string actual)
		{
			StringAssert.DoesNotMatch(pattern, actual, string.Empty, null);
		}

		// Token: 0x06000017 RID: 23 RVA: 0x0000226B File Offset: 0x0000046B
		public StringAssert()
		{
		}
	}
}
