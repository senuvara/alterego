﻿using System;

namespace NUnit.Framework
{
	// Token: 0x0200014E RID: 334
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class TestOfAttribute : PropertyAttribute
	{
		// Token: 0x060008EA RID: 2282 RVA: 0x0001E4A0 File Offset: 0x0001C6A0
		public TestOfAttribute(Type type) : base("TestOf", type.FullName)
		{
		}

		// Token: 0x060008EB RID: 2283 RVA: 0x0001E4B6 File Offset: 0x0001C6B6
		public TestOfAttribute(string typeName) : base("TestOf", typeName)
		{
		}
	}
}
