﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework
{
	// Token: 0x0200005E RID: 94
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	public class TestCaseAttribute : NUnitAttribute, ITestBuilder, ITestCaseData, ITestData, IImplyFixture
	{
		// Token: 0x060002BE RID: 702 RVA: 0x0000A3C4 File Offset: 0x000085C4
		public TestCaseAttribute(params object[] arguments)
		{
			this.RunState = RunState.Runnable;
			if (arguments == null)
			{
				object[] arguments2 = new object[1];
				this.Arguments = arguments2;
			}
			else
			{
				this.Arguments = arguments;
			}
			this.Properties = new PropertyBag();
		}

		// Token: 0x060002BF RID: 703 RVA: 0x0000A414 File Offset: 0x00008614
		public TestCaseAttribute(object arg)
		{
			this.RunState = RunState.Runnable;
			this.Arguments = new object[]
			{
				arg
			};
			this.Properties = new PropertyBag();
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x0000A454 File Offset: 0x00008654
		public TestCaseAttribute(object arg1, object arg2)
		{
			this.RunState = RunState.Runnable;
			this.Arguments = new object[]
			{
				arg1,
				arg2
			};
			this.Properties = new PropertyBag();
		}

		// Token: 0x060002C1 RID: 705 RVA: 0x0000A498 File Offset: 0x00008698
		public TestCaseAttribute(object arg1, object arg2, object arg3)
		{
			this.RunState = RunState.Runnable;
			this.Arguments = new object[]
			{
				arg1,
				arg2,
				arg3
			};
			this.Properties = new PropertyBag();
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x060002C2 RID: 706 RVA: 0x0000A4E0 File Offset: 0x000086E0
		// (set) Token: 0x060002C3 RID: 707 RVA: 0x0000A4F7 File Offset: 0x000086F7
		public string TestName
		{
			[CompilerGenerated]
			get
			{
				return this.<TestName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TestName>k__BackingField = value;
			}
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x060002C4 RID: 708 RVA: 0x0000A500 File Offset: 0x00008700
		// (set) Token: 0x060002C5 RID: 709 RVA: 0x0000A517 File Offset: 0x00008717
		public RunState RunState
		{
			[CompilerGenerated]
			get
			{
				return this.<RunState>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<RunState>k__BackingField = value;
			}
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x060002C6 RID: 710 RVA: 0x0000A520 File Offset: 0x00008720
		// (set) Token: 0x060002C7 RID: 711 RVA: 0x0000A537 File Offset: 0x00008737
		public object[] Arguments
		{
			[CompilerGenerated]
			get
			{
				return this.<Arguments>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Arguments>k__BackingField = value;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x060002C8 RID: 712 RVA: 0x0000A540 File Offset: 0x00008740
		// (set) Token: 0x060002C9 RID: 713 RVA: 0x0000A557 File Offset: 0x00008757
		public IPropertyBag Properties
		{
			[CompilerGenerated]
			get
			{
				return this.<Properties>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Properties>k__BackingField = value;
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x060002CA RID: 714 RVA: 0x0000A560 File Offset: 0x00008760
		// (set) Token: 0x060002CB RID: 715 RVA: 0x0000A578 File Offset: 0x00008778
		public object ExpectedResult
		{
			get
			{
				return this._expectedResult;
			}
			set
			{
				this._expectedResult = value;
				this.HasExpectedResult = true;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x060002CC RID: 716 RVA: 0x0000A58C File Offset: 0x0000878C
		// (set) Token: 0x060002CD RID: 717 RVA: 0x0000A5A3 File Offset: 0x000087A3
		public bool HasExpectedResult
		{
			[CompilerGenerated]
			get
			{
				return this.<HasExpectedResult>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<HasExpectedResult>k__BackingField = value;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x060002CE RID: 718 RVA: 0x0000A5AC File Offset: 0x000087AC
		// (set) Token: 0x060002CF RID: 719 RVA: 0x0000A5D3 File Offset: 0x000087D3
		public string Description
		{
			get
			{
				return this.Properties.Get("Description") as string;
			}
			set
			{
				this.Properties.Set("Description", value);
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x060002D0 RID: 720 RVA: 0x0000A5E8 File Offset: 0x000087E8
		// (set) Token: 0x060002D1 RID: 721 RVA: 0x0000A60F File Offset: 0x0000880F
		public string Author
		{
			get
			{
				return this.Properties.Get("Author") as string;
			}
			set
			{
				this.Properties.Set("Author", value);
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x060002D2 RID: 722 RVA: 0x0000A624 File Offset: 0x00008824
		// (set) Token: 0x060002D3 RID: 723 RVA: 0x0000A63C File Offset: 0x0000883C
		public Type TestOf
		{
			get
			{
				return this._testOf;
			}
			set
			{
				this._testOf = value;
				this.Properties.Set("TestOf", value.FullName);
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x060002D4 RID: 724 RVA: 0x0000A660 File Offset: 0x00008860
		// (set) Token: 0x060002D5 RID: 725 RVA: 0x0000A678 File Offset: 0x00008878
		public string Ignore
		{
			get
			{
				return this.IgnoreReason;
			}
			set
			{
				this.IgnoreReason = value;
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060002D6 RID: 726 RVA: 0x0000A684 File Offset: 0x00008884
		// (set) Token: 0x060002D7 RID: 727 RVA: 0x0000A69F File Offset: 0x0000889F
		public bool Explicit
		{
			get
			{
				return this.RunState == RunState.Explicit;
			}
			set
			{
				this.RunState = (value ? RunState.Explicit : RunState.Runnable);
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060002D8 RID: 728 RVA: 0x0000A6B0 File Offset: 0x000088B0
		// (set) Token: 0x060002D9 RID: 729 RVA: 0x0000A6D7 File Offset: 0x000088D7
		public string Reason
		{
			get
			{
				return this.Properties.Get("_SKIPREASON") as string;
			}
			set
			{
				this.Properties.Set("_SKIPREASON", value);
			}
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x060002DA RID: 730 RVA: 0x0000A6EC File Offset: 0x000088EC
		// (set) Token: 0x060002DB RID: 731 RVA: 0x0000A704 File Offset: 0x00008904
		public string IgnoreReason
		{
			get
			{
				return this.Reason;
			}
			set
			{
				this.RunState = RunState.Ignored;
				this.Reason = value;
			}
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x060002DC RID: 732 RVA: 0x0000A718 File Offset: 0x00008918
		// (set) Token: 0x060002DD RID: 733 RVA: 0x0000A72F File Offset: 0x0000892F
		public string IncludePlatform
		{
			[CompilerGenerated]
			get
			{
				return this.<IncludePlatform>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IncludePlatform>k__BackingField = value;
			}
		}

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x060002DE RID: 734 RVA: 0x0000A738 File Offset: 0x00008938
		// (set) Token: 0x060002DF RID: 735 RVA: 0x0000A74F File Offset: 0x0000894F
		public string ExcludePlatform
		{
			[CompilerGenerated]
			get
			{
				return this.<ExcludePlatform>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ExcludePlatform>k__BackingField = value;
			}
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x060002E0 RID: 736 RVA: 0x0000A758 File Offset: 0x00008958
		// (set) Token: 0x060002E1 RID: 737 RVA: 0x0000A780 File Offset: 0x00008980
		public string Category
		{
			get
			{
				return this.Properties.Get("Category") as string;
			}
			set
			{
				foreach (string value2 in value.Split(new char[]
				{
					','
				}))
				{
					this.Properties.Add("Category", value2);
				}
			}
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x0000A7D0 File Offset: 0x000089D0
		private TestCaseParameters GetParametersForTestCase(IMethodInfo method)
		{
			TestCaseParameters testCaseParameters;
			try
			{
				IParameterInfo[] parameters = method.GetParameters();
				int num = parameters.Length;
				int num2 = this.Arguments.Length;
				testCaseParameters = new TestCaseParameters(this);
				if (num > 0 && num2 >= num - 1)
				{
					IParameterInfo parameterInfo = parameters[num - 1];
					Type parameterType = parameterInfo.ParameterType;
					Type elementType = parameterType.GetElementType();
					if (parameterType.IsArray && parameterInfo.IsDefined<ParamArrayAttribute>(false))
					{
						if (num2 == num)
						{
							Type type = testCaseParameters.Arguments[num2 - 1].GetType();
							if (!parameterType.GetTypeInfo().IsAssignableFrom(type.GetTypeInfo()))
							{
								Array array = Array.CreateInstance(elementType, 1);
								array.SetValue(testCaseParameters.Arguments[num2 - 1], 0);
								testCaseParameters.Arguments[num2 - 1] = array;
							}
						}
						else
						{
							object[] array2 = new object[num];
							int i = 0;
							while (i < num && i < num2)
							{
								array2[i] = testCaseParameters.Arguments[i];
								i++;
							}
							int num3 = num2 - num + 1;
							Array array = Array.CreateInstance(elementType, num3);
							for (i = 0; i < num3; i++)
							{
								array.SetValue(testCaseParameters.Arguments[num + i - 1], i);
							}
							array2[num - 1] = array;
							testCaseParameters.Arguments = array2;
							num2 = num;
						}
					}
				}
				if (testCaseParameters.Arguments.Length < num)
				{
					object[] array3 = new object[parameters.Length];
					Array.Copy(testCaseParameters.Arguments, array3, testCaseParameters.Arguments.Length);
					for (int i = testCaseParameters.Arguments.Length; i < parameters.Length; i++)
					{
						if (parameters[i].IsOptional)
						{
							array3[i] = Type.Missing;
						}
						else
						{
							if (i >= testCaseParameters.Arguments.Length)
							{
								throw new TargetParameterCountException("Incorrect number of parameters specified for TestCase");
							}
							array3[i] = testCaseParameters.Arguments[i];
						}
					}
					testCaseParameters.Arguments = array3;
				}
				if (num == 1 && method.GetParameters()[0].ParameterType == typeof(object[]))
				{
					if (num2 > 1 || (num2 == 1 && testCaseParameters.Arguments[0].GetType() != typeof(object[])))
					{
						testCaseParameters.Arguments = new object[]
						{
							testCaseParameters.Arguments
						};
					}
				}
				if (num2 == num)
				{
					TestCaseAttribute.PerformSpecialConversions(testCaseParameters.Arguments, parameters);
				}
			}
			catch (Exception exception)
			{
				testCaseParameters = new TestCaseParameters(exception);
			}
			return testCaseParameters;
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x0000AAB8 File Offset: 0x00008CB8
		private static void PerformSpecialConversions(object[] arglist, IParameterInfo[] parameters)
		{
			for (int i = 0; i < arglist.Length; i++)
			{
				object obj = arglist[i];
				Type parameterType = parameters[i].ParameterType;
				if (obj != null)
				{
					if (obj is SpecialValue && (SpecialValue)obj == SpecialValue.Null)
					{
						arglist[i] = null;
					}
					else if (!parameterType.IsAssignableFrom(obj.GetType()))
					{
						if (obj is DBNull)
						{
							arglist[i] = null;
						}
						else
						{
							bool flag = false;
							if (parameterType == typeof(short) || parameterType == typeof(byte) || parameterType == typeof(sbyte) || parameterType == typeof(short?) || parameterType == typeof(byte?) || parameterType == typeof(sbyte?) || parameterType == typeof(double?))
							{
								flag = (obj is int);
							}
							else if (parameterType == typeof(decimal) || parameterType == typeof(decimal?))
							{
								flag = (obj is double || obj is string || obj is int);
							}
							else if (parameterType == typeof(DateTime) || parameterType == typeof(DateTime?))
							{
								flag = (obj is string);
							}
							if (flag)
							{
								Type conversionType = (parameterType.GetTypeInfo().IsGenericType && parameterType.GetGenericTypeDefinition() == typeof(Nullable<>)) ? parameterType.GetGenericArguments()[0] : parameterType;
								arglist[i] = Convert.ChangeType(obj, conversionType, CultureInfo.InvariantCulture);
							}
							else if ((parameterType == typeof(TimeSpan) || parameterType == typeof(TimeSpan?)) && obj is string)
							{
								arglist[i] = TimeSpan.Parse((string)obj);
							}
						}
					}
				}
			}
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x0000AEBC File Offset: 0x000090BC
		public IEnumerable<TestMethod> BuildFrom(IMethodInfo method, Test suite)
		{
			TestMethod test = new NUnitTestCaseBuilder().BuildTestMethod(method, suite, this.GetParametersForTestCase(method));
			if (test.RunState != RunState.NotRunnable && test.RunState != RunState.Ignored)
			{
				PlatformHelper platformHelper = new PlatformHelper();
				if (!platformHelper.IsPlatformSupported(this))
				{
					test.RunState = RunState.Skipped;
					test.Properties.Add("_SKIPREASON", platformHelper.Reason);
				}
			}
			yield return test;
			yield break;
		}

		// Token: 0x04000094 RID: 148
		private object _expectedResult;

		// Token: 0x04000095 RID: 149
		private Type _testOf;

		// Token: 0x04000096 RID: 150
		[CompilerGenerated]
		private string <TestName>k__BackingField;

		// Token: 0x04000097 RID: 151
		[CompilerGenerated]
		private RunState <RunState>k__BackingField;

		// Token: 0x04000098 RID: 152
		[CompilerGenerated]
		private object[] <Arguments>k__BackingField;

		// Token: 0x04000099 RID: 153
		[CompilerGenerated]
		private IPropertyBag <Properties>k__BackingField;

		// Token: 0x0400009A RID: 154
		[CompilerGenerated]
		private bool <HasExpectedResult>k__BackingField;

		// Token: 0x0400009B RID: 155
		[CompilerGenerated]
		private string <IncludePlatform>k__BackingField;

		// Token: 0x0400009C RID: 156
		[CompilerGenerated]
		private string <ExcludePlatform>k__BackingField;

		// Token: 0x020001AF RID: 431
		[CompilerGenerated]
		private sealed class <BuildFrom>d__0 : IEnumerable<TestMethod>, IEnumerable, IEnumerator<TestMethod>, IEnumerator, IDisposable
		{
			// Token: 0x06000B20 RID: 2848 RVA: 0x0000ACDC File Offset: 0x00008EDC
			[DebuggerHidden]
			IEnumerator<TestMethod> IEnumerable<TestMethod>.GetEnumerator()
			{
				TestCaseAttribute.<BuildFrom>d__0 <BuildFrom>d__;
				if (Thread.CurrentThread.ManagedThreadId == this.<>l__initialThreadId && this.<>1__state == -2)
				{
					this.<>1__state = 0;
					<BuildFrom>d__ = this;
				}
				else
				{
					<BuildFrom>d__ = new TestCaseAttribute.<BuildFrom>d__0(0);
					<BuildFrom>d__.<>4__this = this;
				}
				<BuildFrom>d__.method = method;
				<BuildFrom>d__.suite = suite;
				return <BuildFrom>d__;
			}

			// Token: 0x06000B21 RID: 2849 RVA: 0x0000AD4C File Offset: 0x00008F4C
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<NUnit.Framework.Internal.TestMethod>.GetEnumerator();
			}

			// Token: 0x06000B22 RID: 2850 RVA: 0x0000AD64 File Offset: 0x00008F64
			bool IEnumerator.MoveNext()
			{
				switch (this.<>1__state)
				{
				case 0:
					this.<>1__state = -1;
					test = new NUnitTestCaseBuilder().BuildTestMethod(method, suite, this.GetParametersForTestCase(method));
					if (test.RunState != RunState.NotRunnable && test.RunState != RunState.Ignored)
					{
						PlatformHelper platformHelper = new PlatformHelper();
						if (!platformHelper.IsPlatformSupported(this))
						{
							test.RunState = RunState.Skipped;
							test.Properties.Add("_SKIPREASON", platformHelper.Reason);
						}
					}
					this.<>2__current = test;
					this.<>1__state = 1;
					return true;
				case 1:
					this.<>1__state = -1;
					break;
				}
				return false;
			}

			// Token: 0x17000282 RID: 642
			// (get) Token: 0x06000B23 RID: 2851 RVA: 0x0000AE54 File Offset: 0x00009054
			TestMethod IEnumerator<TestMethod>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B24 RID: 2852 RVA: 0x0000AE6B File Offset: 0x0000906B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000B25 RID: 2853 RVA: 0x0000AE74 File Offset: 0x00009074
			void IDisposable.Dispose()
			{
			}

			// Token: 0x17000283 RID: 643
			// (get) Token: 0x06000B26 RID: 2854 RVA: 0x0000AE84 File Offset: 0x00009084
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B27 RID: 2855 RVA: 0x0000AE9B File Offset: 0x0000909B
			[DebuggerHidden]
			public <BuildFrom>d__0(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Thread.CurrentThread.ManagedThreadId;
			}

			// Token: 0x040002B9 RID: 697
			private TestMethod <>2__current;

			// Token: 0x040002BA RID: 698
			private int <>1__state;

			// Token: 0x040002BB RID: 699
			private int <>l__initialThreadId;

			// Token: 0x040002BC RID: 700
			public TestCaseAttribute <>4__this;

			// Token: 0x040002BD RID: 701
			public IMethodInfo method;

			// Token: 0x040002BE RID: 702
			public IMethodInfo <>3__method;

			// Token: 0x040002BF RID: 703
			public Test suite;

			// Token: 0x040002C0 RID: 704
			public Test <>3__suite;

			// Token: 0x040002C1 RID: 705
			public TestMethod <test>5__1;
		}
	}
}
