﻿using System;
using System.Runtime.Serialization;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework
{
	// Token: 0x0200012A RID: 298
	[Serializable]
	public class AssertionException : ResultStateException
	{
		// Token: 0x060007F0 RID: 2032 RVA: 0x0001C22C File Offset: 0x0001A42C
		public AssertionException(string message) : base(message)
		{
		}

		// Token: 0x060007F1 RID: 2033 RVA: 0x0001C238 File Offset: 0x0001A438
		public AssertionException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x060007F2 RID: 2034 RVA: 0x0001C245 File Offset: 0x0001A445
		protected AssertionException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x170001BB RID: 443
		// (get) Token: 0x060007F3 RID: 2035 RVA: 0x0001C254 File Offset: 0x0001A454
		public override ResultState ResultState
		{
			get
			{
				return ResultState.Failure;
			}
		}
	}
}
