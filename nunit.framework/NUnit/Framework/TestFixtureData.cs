﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x020001A7 RID: 423
	public class TestFixtureData : TestFixtureParameters
	{
		// Token: 0x06000AE1 RID: 2785 RVA: 0x000246B8 File Offset: 0x000228B8
		public TestFixtureData(params object[] args)
		{
			object[] args2;
			if (args != null)
			{
				args2 = args;
			}
			else
			{
				object[] array = new object[1];
				args2 = array;
			}
			base..ctor(args2);
		}

		// Token: 0x06000AE2 RID: 2786 RVA: 0x000246DC File Offset: 0x000228DC
		public TestFixtureData(object arg) : base(new object[]
		{
			arg
		})
		{
		}

		// Token: 0x06000AE3 RID: 2787 RVA: 0x00024700 File Offset: 0x00022900
		public TestFixtureData(object arg1, object arg2) : base(new object[]
		{
			arg1,
			arg2
		})
		{
		}

		// Token: 0x06000AE4 RID: 2788 RVA: 0x00024728 File Offset: 0x00022928
		public TestFixtureData(object arg1, object arg2, object arg3) : base(new object[]
		{
			arg1,
			arg2,
			arg3
		})
		{
		}

		// Token: 0x06000AE5 RID: 2789 RVA: 0x00024754 File Offset: 0x00022954
		public TestFixtureData Explicit()
		{
			base.RunState = RunState.Explicit;
			return this;
		}

		// Token: 0x06000AE6 RID: 2790 RVA: 0x00024770 File Offset: 0x00022970
		public TestFixtureData Explicit(string reason)
		{
			base.RunState = RunState.Explicit;
			base.Properties.Set("_SKIPREASON", reason);
			return this;
		}

		// Token: 0x06000AE7 RID: 2791 RVA: 0x000247A0 File Offset: 0x000229A0
		public TestFixtureData Ignore(string reason)
		{
			base.RunState = RunState.Ignored;
			base.Properties.Set("_SKIPREASON", reason);
			return this;
		}
	}
}
