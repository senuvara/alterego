﻿using System;

namespace NUnit.Framework
{
	// Token: 0x020000CA RID: 202
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class AuthorAttribute : PropertyAttribute
	{
		// Token: 0x0600062E RID: 1582 RVA: 0x000158A6 File Offset: 0x00013AA6
		public AuthorAttribute(string name) : base("Author", name)
		{
		}

		// Token: 0x0600062F RID: 1583 RVA: 0x000158B7 File Offset: 0x00013AB7
		public AuthorAttribute(string name, string email) : base("Author", string.Format("{0} <{1}>", name, email))
		{
		}
	}
}
