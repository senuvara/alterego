﻿using System;
using NUnit.Framework.Constraints;

namespace NUnit.Framework
{
	// Token: 0x020000CF RID: 207
	public class Has
	{
		// Token: 0x17000165 RID: 357
		// (get) Token: 0x06000648 RID: 1608 RVA: 0x000166C4 File Offset: 0x000148C4
		public static ConstraintExpression No
		{
			get
			{
				return new ConstraintExpression().Not;
			}
		}

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x06000649 RID: 1609 RVA: 0x000166E0 File Offset: 0x000148E0
		public static ConstraintExpression All
		{
			get
			{
				return new ConstraintExpression().All;
			}
		}

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x0600064A RID: 1610 RVA: 0x000166FC File Offset: 0x000148FC
		public static ConstraintExpression Some
		{
			get
			{
				return new ConstraintExpression().Some;
			}
		}

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x0600064B RID: 1611 RVA: 0x00016718 File Offset: 0x00014918
		public static ConstraintExpression None
		{
			get
			{
				return new ConstraintExpression().None;
			}
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x00016734 File Offset: 0x00014934
		public static ConstraintExpression Exactly(int expectedCount)
		{
			return new ConstraintExpression().Exactly(expectedCount);
		}

		// Token: 0x0600064D RID: 1613 RVA: 0x00016754 File Offset: 0x00014954
		public static ResolvableConstraintExpression Property(string name)
		{
			return new ConstraintExpression().Property(name);
		}

		// Token: 0x17000169 RID: 361
		// (get) Token: 0x0600064E RID: 1614 RVA: 0x00016774 File Offset: 0x00014974
		public static ResolvableConstraintExpression Length
		{
			get
			{
				return Has.Property("Length");
			}
		}

		// Token: 0x1700016A RID: 362
		// (get) Token: 0x0600064F RID: 1615 RVA: 0x00016790 File Offset: 0x00014990
		public static ResolvableConstraintExpression Count
		{
			get
			{
				return Has.Property("Count");
			}
		}

		// Token: 0x1700016B RID: 363
		// (get) Token: 0x06000650 RID: 1616 RVA: 0x000167AC File Offset: 0x000149AC
		public static ResolvableConstraintExpression Message
		{
			get
			{
				return Has.Property("Message");
			}
		}

		// Token: 0x1700016C RID: 364
		// (get) Token: 0x06000651 RID: 1617 RVA: 0x000167C8 File Offset: 0x000149C8
		public static ResolvableConstraintExpression InnerException
		{
			get
			{
				return Has.Property("InnerException");
			}
		}

		// Token: 0x06000652 RID: 1618 RVA: 0x000167E4 File Offset: 0x000149E4
		public static ResolvableConstraintExpression Attribute(Type expectedType)
		{
			return new ConstraintExpression().Attribute(expectedType);
		}

		// Token: 0x06000653 RID: 1619 RVA: 0x00016804 File Offset: 0x00014A04
		public static ResolvableConstraintExpression Attribute<T>()
		{
			return Has.Attribute(typeof(T));
		}

		// Token: 0x06000654 RID: 1620 RVA: 0x00016828 File Offset: 0x00014A28
		public static CollectionContainsConstraint Member(object expected)
		{
			return new CollectionContainsConstraint(expected);
		}

		// Token: 0x06000655 RID: 1621 RVA: 0x00016840 File Offset: 0x00014A40
		public Has()
		{
		}
	}
}
