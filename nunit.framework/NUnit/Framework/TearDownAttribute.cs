﻿using System;

namespace NUnit.Framework
{
	// Token: 0x0200017E RID: 382
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class TearDownAttribute : NUnitAttribute
	{
		// Token: 0x06000A10 RID: 2576 RVA: 0x00021EAC File Offset: 0x000200AC
		public TearDownAttribute()
		{
		}
	}
}
