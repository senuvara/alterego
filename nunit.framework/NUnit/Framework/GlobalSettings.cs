﻿using System;

namespace NUnit.Framework
{
	// Token: 0x02000177 RID: 375
	public static class GlobalSettings
	{
		// Token: 0x060009ED RID: 2541 RVA: 0x000219BB File Offset: 0x0001FBBB
		// Note: this type is marked as 'beforefieldinit'.
		static GlobalSettings()
		{
		}

		// Token: 0x04000247 RID: 583
		public static double DefaultFloatingPointTolerance = 0.0;
	}
}
