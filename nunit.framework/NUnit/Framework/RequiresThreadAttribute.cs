﻿using System;
using System.Threading;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x0200010B RID: 267
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class RequiresThreadAttribute : PropertyAttribute, IApplyToTest
	{
		// Token: 0x0600075A RID: 1882 RVA: 0x0001A6E2 File Offset: 0x000188E2
		public RequiresThreadAttribute() : base(true)
		{
		}

		// Token: 0x0600075B RID: 1883 RVA: 0x0001A6F3 File Offset: 0x000188F3
		public RequiresThreadAttribute(ApartmentState apartment) : base(true)
		{
			base.Properties.Add("ApartmentState", apartment);
		}

		// Token: 0x0600075C RID: 1884 RVA: 0x0001A71B File Offset: 0x0001891B
		void IApplyToTest.ApplyToTest(Test test)
		{
			test.RequiresThread = true;
			base.ApplyToTest(test);
		}
	}
}
