﻿using System;
using System.Collections;
using NUnit.Framework.Constraints;

namespace NUnit.Framework
{
	// Token: 0x02000115 RID: 277
	public class Is
	{
		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x06000779 RID: 1913 RVA: 0x0001B070 File Offset: 0x00019270
		public static ConstraintExpression Not
		{
			get
			{
				return new ConstraintExpression().Not;
			}
		}

		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x0600077A RID: 1914 RVA: 0x0001B08C File Offset: 0x0001928C
		public static ConstraintExpression All
		{
			get
			{
				return new ConstraintExpression().All;
			}
		}

		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x0600077B RID: 1915 RVA: 0x0001B0A8 File Offset: 0x000192A8
		public static NullConstraint Null
		{
			get
			{
				return new NullConstraint();
			}
		}

		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x0600077C RID: 1916 RVA: 0x0001B0C0 File Offset: 0x000192C0
		public static TrueConstraint True
		{
			get
			{
				return new TrueConstraint();
			}
		}

		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x0600077D RID: 1917 RVA: 0x0001B0D8 File Offset: 0x000192D8
		public static FalseConstraint False
		{
			get
			{
				return new FalseConstraint();
			}
		}

		// Token: 0x170001AA RID: 426
		// (get) Token: 0x0600077E RID: 1918 RVA: 0x0001B0F0 File Offset: 0x000192F0
		public static GreaterThanConstraint Positive
		{
			get
			{
				return new GreaterThanConstraint(0);
			}
		}

		// Token: 0x170001AB RID: 427
		// (get) Token: 0x0600077F RID: 1919 RVA: 0x0001B110 File Offset: 0x00019310
		public static LessThanConstraint Negative
		{
			get
			{
				return new LessThanConstraint(0);
			}
		}

		// Token: 0x170001AC RID: 428
		// (get) Token: 0x06000780 RID: 1920 RVA: 0x0001B130 File Offset: 0x00019330
		public static EqualConstraint Zero
		{
			get
			{
				return new EqualConstraint(0);
			}
		}

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x06000781 RID: 1921 RVA: 0x0001B150 File Offset: 0x00019350
		public static NaNConstraint NaN
		{
			get
			{
				return new NaNConstraint();
			}
		}

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x06000782 RID: 1922 RVA: 0x0001B168 File Offset: 0x00019368
		public static EmptyConstraint Empty
		{
			get
			{
				return new EmptyConstraint();
			}
		}

		// Token: 0x170001AF RID: 431
		// (get) Token: 0x06000783 RID: 1923 RVA: 0x0001B180 File Offset: 0x00019380
		public static UniqueItemsConstraint Unique
		{
			get
			{
				return new UniqueItemsConstraint();
			}
		}

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x06000784 RID: 1924 RVA: 0x0001B198 File Offset: 0x00019398
		public static BinarySerializableConstraint BinarySerializable
		{
			get
			{
				return new BinarySerializableConstraint();
			}
		}

		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x06000785 RID: 1925 RVA: 0x0001B1B0 File Offset: 0x000193B0
		public static XmlSerializableConstraint XmlSerializable
		{
			get
			{
				return new XmlSerializableConstraint();
			}
		}

		// Token: 0x06000786 RID: 1926 RVA: 0x0001B1C8 File Offset: 0x000193C8
		public static EqualConstraint EqualTo(object expected)
		{
			return new EqualConstraint(expected);
		}

		// Token: 0x06000787 RID: 1927 RVA: 0x0001B1E0 File Offset: 0x000193E0
		public static SameAsConstraint SameAs(object expected)
		{
			return new SameAsConstraint(expected);
		}

		// Token: 0x06000788 RID: 1928 RVA: 0x0001B1F8 File Offset: 0x000193F8
		public static GreaterThanConstraint GreaterThan(object expected)
		{
			return new GreaterThanConstraint(expected);
		}

		// Token: 0x06000789 RID: 1929 RVA: 0x0001B210 File Offset: 0x00019410
		public static GreaterThanOrEqualConstraint GreaterThanOrEqualTo(object expected)
		{
			return new GreaterThanOrEqualConstraint(expected);
		}

		// Token: 0x0600078A RID: 1930 RVA: 0x0001B228 File Offset: 0x00019428
		public static GreaterThanOrEqualConstraint AtLeast(object expected)
		{
			return new GreaterThanOrEqualConstraint(expected);
		}

		// Token: 0x0600078B RID: 1931 RVA: 0x0001B240 File Offset: 0x00019440
		public static LessThanConstraint LessThan(object expected)
		{
			return new LessThanConstraint(expected);
		}

		// Token: 0x0600078C RID: 1932 RVA: 0x0001B258 File Offset: 0x00019458
		public static LessThanOrEqualConstraint LessThanOrEqualTo(object expected)
		{
			return new LessThanOrEqualConstraint(expected);
		}

		// Token: 0x0600078D RID: 1933 RVA: 0x0001B270 File Offset: 0x00019470
		public static LessThanOrEqualConstraint AtMost(object expected)
		{
			return new LessThanOrEqualConstraint(expected);
		}

		// Token: 0x0600078E RID: 1934 RVA: 0x0001B288 File Offset: 0x00019488
		public static ExactTypeConstraint TypeOf(Type expectedType)
		{
			return new ExactTypeConstraint(expectedType);
		}

		// Token: 0x0600078F RID: 1935 RVA: 0x0001B2A0 File Offset: 0x000194A0
		public static ExactTypeConstraint TypeOf<TExpected>()
		{
			return new ExactTypeConstraint(typeof(TExpected));
		}

		// Token: 0x06000790 RID: 1936 RVA: 0x0001B2C4 File Offset: 0x000194C4
		public static InstanceOfTypeConstraint InstanceOf(Type expectedType)
		{
			return new InstanceOfTypeConstraint(expectedType);
		}

		// Token: 0x06000791 RID: 1937 RVA: 0x0001B2DC File Offset: 0x000194DC
		public static InstanceOfTypeConstraint InstanceOf<TExpected>()
		{
			return new InstanceOfTypeConstraint(typeof(TExpected));
		}

		// Token: 0x06000792 RID: 1938 RVA: 0x0001B300 File Offset: 0x00019500
		public static AssignableFromConstraint AssignableFrom(Type expectedType)
		{
			return new AssignableFromConstraint(expectedType);
		}

		// Token: 0x06000793 RID: 1939 RVA: 0x0001B318 File Offset: 0x00019518
		public static AssignableFromConstraint AssignableFrom<TExpected>()
		{
			return new AssignableFromConstraint(typeof(TExpected));
		}

		// Token: 0x06000794 RID: 1940 RVA: 0x0001B33C File Offset: 0x0001953C
		public static AssignableToConstraint AssignableTo(Type expectedType)
		{
			return new AssignableToConstraint(expectedType);
		}

		// Token: 0x06000795 RID: 1941 RVA: 0x0001B354 File Offset: 0x00019554
		public static AssignableToConstraint AssignableTo<TExpected>()
		{
			return new AssignableToConstraint(typeof(TExpected));
		}

		// Token: 0x06000796 RID: 1942 RVA: 0x0001B378 File Offset: 0x00019578
		public static CollectionEquivalentConstraint EquivalentTo(IEnumerable expected)
		{
			return new CollectionEquivalentConstraint(expected);
		}

		// Token: 0x06000797 RID: 1943 RVA: 0x0001B390 File Offset: 0x00019590
		public static CollectionSubsetConstraint SubsetOf(IEnumerable expected)
		{
			return new CollectionSubsetConstraint(expected);
		}

		// Token: 0x06000798 RID: 1944 RVA: 0x0001B3A8 File Offset: 0x000195A8
		public static CollectionSupersetConstraint SupersetOf(IEnumerable expected)
		{
			return new CollectionSupersetConstraint(expected);
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x06000799 RID: 1945 RVA: 0x0001B3C0 File Offset: 0x000195C0
		public static CollectionOrderedConstraint Ordered
		{
			get
			{
				return new CollectionOrderedConstraint();
			}
		}

		// Token: 0x0600079A RID: 1946 RVA: 0x0001B3D8 File Offset: 0x000195D8
		[Obsolete("Deprecated, use Does.Contain")]
		public static SubstringConstraint StringContaining(string expected)
		{
			return new SubstringConstraint(expected);
		}

		// Token: 0x0600079B RID: 1947 RVA: 0x0001B3F0 File Offset: 0x000195F0
		[Obsolete("Deprecated, use Does.StartWith")]
		public static StartsWithConstraint StringStarting(string expected)
		{
			return new StartsWithConstraint(expected);
		}

		// Token: 0x0600079C RID: 1948 RVA: 0x0001B408 File Offset: 0x00019608
		[Obsolete("Deprecated, use Does.EndWith")]
		public static EndsWithConstraint StringEnding(string expected)
		{
			return new EndsWithConstraint(expected);
		}

		// Token: 0x0600079D RID: 1949 RVA: 0x0001B420 File Offset: 0x00019620
		[Obsolete("Deprecated, use Does.Match")]
		public static RegexConstraint StringMatching(string pattern)
		{
			return new RegexConstraint(pattern);
		}

		// Token: 0x0600079E RID: 1950 RVA: 0x0001B438 File Offset: 0x00019638
		public static SamePathConstraint SamePath(string expected)
		{
			return new SamePathConstraint(expected);
		}

		// Token: 0x0600079F RID: 1951 RVA: 0x0001B450 File Offset: 0x00019650
		public static SubPathConstraint SubPathOf(string expected)
		{
			return new SubPathConstraint(expected);
		}

		// Token: 0x060007A0 RID: 1952 RVA: 0x0001B468 File Offset: 0x00019668
		public static SamePathOrUnderConstraint SamePathOrUnder(string expected)
		{
			return new SamePathOrUnderConstraint(expected);
		}

		// Token: 0x060007A1 RID: 1953 RVA: 0x0001B480 File Offset: 0x00019680
		public static RangeConstraint InRange(IComparable from, IComparable to)
		{
			return new RangeConstraint(from, to);
		}

		// Token: 0x060007A2 RID: 1954 RVA: 0x0001B499 File Offset: 0x00019699
		public Is()
		{
		}
	}
}
