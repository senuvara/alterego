﻿using System;

namespace NUnit.Framework
{
	// Token: 0x0200015A RID: 346
	[Obsolete("Use OneTimeSetUpAttribute")]
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class TestFixtureSetUpAttribute : OneTimeSetUpAttribute
	{
		// Token: 0x06000905 RID: 2309 RVA: 0x0001E9C6 File Offset: 0x0001CBC6
		public TestFixtureSetUpAttribute()
		{
		}
	}
}
