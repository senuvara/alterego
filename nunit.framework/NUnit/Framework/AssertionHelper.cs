﻿using System;
using System.Collections;
using NUnit.Framework.Constraints;

namespace NUnit.Framework
{
	// Token: 0x0200015C RID: 348
	public class AssertionHelper : ConstraintFactory
	{
		// Token: 0x06000907 RID: 2311 RVA: 0x0001E9D6 File Offset: 0x0001CBD6
		public void Expect(bool condition, string message, params object[] args)
		{
			Assert.That<bool>(condition, Is.True, message, args);
		}

		// Token: 0x06000908 RID: 2312 RVA: 0x0001E9E7 File Offset: 0x0001CBE7
		public void Expect(bool condition)
		{
			Assert.That<bool>(condition, Is.True, null, null);
		}

		// Token: 0x06000909 RID: 2313 RVA: 0x0001E9F8 File Offset: 0x0001CBF8
		public void Expect<TActual>(ActualValueDelegate<TActual> del, IResolveConstraint expr)
		{
			Assert.That<TActual>(del, expr.Resolve(), null, null);
		}

		// Token: 0x0600090A RID: 2314 RVA: 0x0001EA0A File Offset: 0x0001CC0A
		public void Expect<TActual>(ActualValueDelegate<TActual> del, IResolveConstraint expr, string message, params object[] args)
		{
			Assert.That<TActual>(del, expr, message, args);
		}

		// Token: 0x0600090B RID: 2315 RVA: 0x0001EA18 File Offset: 0x0001CC18
		public void Expect(TestDelegate code, IResolveConstraint constraint)
		{
			Assert.That<object>(code, constraint);
		}

		// Token: 0x0600090C RID: 2316 RVA: 0x0001EA23 File Offset: 0x0001CC23
		public static void Expect<TActual>(TActual actual, IResolveConstraint expression)
		{
			Assert.That<TActual>(actual, expression, null, null);
		}

		// Token: 0x0600090D RID: 2317 RVA: 0x0001EA30 File Offset: 0x0001CC30
		public static void Expect<TActual>(TActual actual, IResolveConstraint expression, string message, params object[] args)
		{
			Assert.That<TActual>(actual, expression, message, args);
		}

		// Token: 0x0600090E RID: 2318 RVA: 0x0001EA40 File Offset: 0x0001CC40
		public ListMapper Map(ICollection original)
		{
			return new ListMapper(original);
		}

		// Token: 0x0600090F RID: 2319 RVA: 0x0001EA58 File Offset: 0x0001CC58
		public AssertionHelper()
		{
		}
	}
}
