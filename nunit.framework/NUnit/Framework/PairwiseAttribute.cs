﻿using System;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework
{
	// Token: 0x02000036 RID: 54
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class PairwiseAttribute : CombiningStrategyAttribute
	{
		// Token: 0x060001AB RID: 427 RVA: 0x0000678B File Offset: 0x0000498B
		public PairwiseAttribute() : base(new PairwiseStrategy(), new ParameterDataSourceProvider())
		{
		}
	}
}
