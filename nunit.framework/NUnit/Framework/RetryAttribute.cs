﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;

namespace NUnit.Framework
{
	// Token: 0x020000F9 RID: 249
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class RetryAttribute : PropertyAttribute, IWrapSetUpTearDown, ICommandWrapper
	{
		// Token: 0x06000708 RID: 1800 RVA: 0x00019A55 File Offset: 0x00017C55
		public RetryAttribute(int count) : base(count)
		{
			this._count = count;
		}

		// Token: 0x06000709 RID: 1801 RVA: 0x00019A70 File Offset: 0x00017C70
		public TestCommand Wrap(TestCommand command)
		{
			return new RetryAttribute.RetryCommand(command, this._count);
		}

		// Token: 0x04000191 RID: 401
		private int _count;

		// Token: 0x020000FA RID: 250
		public class RetryCommand : DelegatingTestCommand
		{
			// Token: 0x0600070A RID: 1802 RVA: 0x00019A8E File Offset: 0x00017C8E
			public RetryCommand(TestCommand innerCommand, int retryCount) : base(innerCommand)
			{
				this._retryCount = retryCount;
			}

			// Token: 0x0600070B RID: 1803 RVA: 0x00019AA4 File Offset: 0x00017CA4
			public override TestResult Execute(ITestExecutionContext context)
			{
				int retryCount = this._retryCount;
				while (retryCount-- > 0)
				{
					context.CurrentResult = this.innerCommand.Execute(context);
					if (context.CurrentResult.ResultState != ResultState.Failure)
					{
						break;
					}
				}
				return context.CurrentResult;
			}

			// Token: 0x04000192 RID: 402
			private int _retryCount;
		}
	}
}
