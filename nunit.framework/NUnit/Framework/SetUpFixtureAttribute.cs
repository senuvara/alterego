﻿using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x020001A3 RID: 419
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public class SetUpFixtureAttribute : NUnitAttribute, IFixtureBuilder
	{
		// Token: 0x06000ACE RID: 2766 RVA: 0x00024258 File Offset: 0x00022458
		public IEnumerable<TestSuite> BuildFrom(ITypeInfo typeInfo)
		{
			SetUpFixture setUpFixture = new SetUpFixture(typeInfo);
			if (setUpFixture.RunState != RunState.NotRunnable)
			{
				string value = null;
				if (!this.IsValidFixtureType(typeInfo, ref value))
				{
					setUpFixture.RunState = RunState.NotRunnable;
					setUpFixture.Properties.Set("_SKIPREASON", value);
				}
			}
			return new TestSuite[]
			{
				setUpFixture
			};
		}

		// Token: 0x06000ACF RID: 2767 RVA: 0x000242BC File Offset: 0x000224BC
		private bool IsValidFixtureType(ITypeInfo typeInfo, ref string reason)
		{
			bool result;
			if (typeInfo.IsAbstract)
			{
				reason = string.Format("{0} is an abstract class", typeInfo.FullName);
				result = false;
			}
			else if (!typeInfo.HasConstructor(new Type[0]))
			{
				reason = string.Format("{0} does not have a default constructor", typeInfo.FullName);
				result = false;
			}
			else
			{
				Type[] array = new Type[]
				{
					typeof(SetUpAttribute),
					typeof(TearDownAttribute),
					typeof(TestFixtureSetUpAttribute),
					typeof(TestFixtureTearDownAttribute)
				};
				foreach (Type type in array)
				{
					if (typeInfo.HasMethodWithAttribute(type))
					{
						reason = type.Name + " attribute not allowed in a SetUpFixture";
						return false;
					}
				}
				result = true;
			}
			return result;
		}

		// Token: 0x06000AD0 RID: 2768 RVA: 0x000243AA File Offset: 0x000225AA
		public SetUpFixtureAttribute()
		{
		}
	}
}
