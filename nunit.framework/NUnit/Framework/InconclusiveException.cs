﻿using System;
using System.Runtime.Serialization;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework
{
	// Token: 0x020000BA RID: 186
	[Serializable]
	public class InconclusiveException : ResultStateException
	{
		// Token: 0x060005A2 RID: 1442 RVA: 0x00013171 File Offset: 0x00011371
		public InconclusiveException(string message) : base(message)
		{
		}

		// Token: 0x060005A3 RID: 1443 RVA: 0x0001317D File Offset: 0x0001137D
		public InconclusiveException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x060005A4 RID: 1444 RVA: 0x0001318A File Offset: 0x0001138A
		protected InconclusiveException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x060005A5 RID: 1445 RVA: 0x00013198 File Offset: 0x00011398
		public override ResultState ResultState
		{
			get
			{
				return ResultState.Inconclusive;
			}
		}
	}
}
