﻿using System;
using System.Collections;
using System.ComponentModel;
using NUnit.Framework.Constraints;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x02000066 RID: 102
	public class Assert
	{
		// Token: 0x06000312 RID: 786 RVA: 0x0000B7F8 File Offset: 0x000099F8
		public static void True(bool? condition, string message, params object[] args)
		{
			Assert.That<bool?>(condition, Is.True, message, args);
		}

		// Token: 0x06000313 RID: 787 RVA: 0x0000B809 File Offset: 0x00009A09
		public static void True(bool condition, string message, params object[] args)
		{
			Assert.That<bool>(condition, Is.True, message, args);
		}

		// Token: 0x06000314 RID: 788 RVA: 0x0000B81A File Offset: 0x00009A1A
		public static void True(bool? condition)
		{
			Assert.That<bool?>(condition, Is.True, null, null);
		}

		// Token: 0x06000315 RID: 789 RVA: 0x0000B82B File Offset: 0x00009A2B
		public static void True(bool condition)
		{
			Assert.That<bool>(condition, Is.True, null, null);
		}

		// Token: 0x06000316 RID: 790 RVA: 0x0000B83C File Offset: 0x00009A3C
		public static void IsTrue(bool? condition, string message, params object[] args)
		{
			Assert.That<bool?>(condition, Is.True, message, args);
		}

		// Token: 0x06000317 RID: 791 RVA: 0x0000B84D File Offset: 0x00009A4D
		public static void IsTrue(bool condition, string message, params object[] args)
		{
			Assert.That<bool>(condition, Is.True, message, args);
		}

		// Token: 0x06000318 RID: 792 RVA: 0x0000B85E File Offset: 0x00009A5E
		public static void IsTrue(bool? condition)
		{
			Assert.That<bool?>(condition, Is.True, null, null);
		}

		// Token: 0x06000319 RID: 793 RVA: 0x0000B86F File Offset: 0x00009A6F
		public static void IsTrue(bool condition)
		{
			Assert.That<bool>(condition, Is.True, null, null);
		}

		// Token: 0x0600031A RID: 794 RVA: 0x0000B880 File Offset: 0x00009A80
		public static void False(bool? condition, string message, params object[] args)
		{
			Assert.That<bool?>(condition, Is.False, message, args);
		}

		// Token: 0x0600031B RID: 795 RVA: 0x0000B891 File Offset: 0x00009A91
		public static void False(bool condition, string message, params object[] args)
		{
			Assert.That<bool>(condition, Is.False, message, args);
		}

		// Token: 0x0600031C RID: 796 RVA: 0x0000B8A2 File Offset: 0x00009AA2
		public static void False(bool? condition)
		{
			Assert.That<bool?>(condition, Is.False, null, null);
		}

		// Token: 0x0600031D RID: 797 RVA: 0x0000B8B3 File Offset: 0x00009AB3
		public static void False(bool condition)
		{
			Assert.That<bool>(condition, Is.False, null, null);
		}

		// Token: 0x0600031E RID: 798 RVA: 0x0000B8C4 File Offset: 0x00009AC4
		public static void IsFalse(bool? condition, string message, params object[] args)
		{
			Assert.That<bool?>(condition, Is.False, message, args);
		}

		// Token: 0x0600031F RID: 799 RVA: 0x0000B8D5 File Offset: 0x00009AD5
		public static void IsFalse(bool condition, string message, params object[] args)
		{
			Assert.That<bool>(condition, Is.False, message, args);
		}

		// Token: 0x06000320 RID: 800 RVA: 0x0000B8E6 File Offset: 0x00009AE6
		public static void IsFalse(bool? condition)
		{
			Assert.That<bool?>(condition, Is.False, null, null);
		}

		// Token: 0x06000321 RID: 801 RVA: 0x0000B8F7 File Offset: 0x00009AF7
		public static void IsFalse(bool condition)
		{
			Assert.That<bool>(condition, Is.False, null, null);
		}

		// Token: 0x06000322 RID: 802 RVA: 0x0000B908 File Offset: 0x00009B08
		public static void NotNull(object anObject, string message, params object[] args)
		{
			Assert.That<object>(anObject, Is.Not.Null, message, args);
		}

		// Token: 0x06000323 RID: 803 RVA: 0x0000B91E File Offset: 0x00009B1E
		public static void NotNull(object anObject)
		{
			Assert.That<object>(anObject, Is.Not.Null, null, null);
		}

		// Token: 0x06000324 RID: 804 RVA: 0x0000B934 File Offset: 0x00009B34
		public static void IsNotNull(object anObject, string message, params object[] args)
		{
			Assert.That<object>(anObject, Is.Not.Null, message, args);
		}

		// Token: 0x06000325 RID: 805 RVA: 0x0000B94A File Offset: 0x00009B4A
		public static void IsNotNull(object anObject)
		{
			Assert.That<object>(anObject, Is.Not.Null, null, null);
		}

		// Token: 0x06000326 RID: 806 RVA: 0x0000B960 File Offset: 0x00009B60
		public static void Null(object anObject, string message, params object[] args)
		{
			Assert.That<object>(anObject, Is.Null, message, args);
		}

		// Token: 0x06000327 RID: 807 RVA: 0x0000B971 File Offset: 0x00009B71
		public static void Null(object anObject)
		{
			Assert.That<object>(anObject, Is.Null, null, null);
		}

		// Token: 0x06000328 RID: 808 RVA: 0x0000B982 File Offset: 0x00009B82
		public static void IsNull(object anObject, string message, params object[] args)
		{
			Assert.That<object>(anObject, Is.Null, message, args);
		}

		// Token: 0x06000329 RID: 809 RVA: 0x0000B993 File Offset: 0x00009B93
		public static void IsNull(object anObject)
		{
			Assert.That<object>(anObject, Is.Null, null, null);
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000B9A4 File Offset: 0x00009BA4
		public static void IsNaN(double aDouble, string message, params object[] args)
		{
			Assert.That<double>(aDouble, Is.NaN, message, args);
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000B9B5 File Offset: 0x00009BB5
		public static void IsNaN(double aDouble)
		{
			Assert.That<double>(aDouble, Is.NaN, null, null);
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000B9C6 File Offset: 0x00009BC6
		public static void IsNaN(double? aDouble, string message, params object[] args)
		{
			Assert.That<double?>(aDouble, Is.NaN, message, args);
		}

		// Token: 0x0600032D RID: 813 RVA: 0x0000B9D7 File Offset: 0x00009BD7
		public static void IsNaN(double? aDouble)
		{
			Assert.That<double?>(aDouble, Is.NaN, null, null);
		}

		// Token: 0x0600032E RID: 814 RVA: 0x0000B9E8 File Offset: 0x00009BE8
		public static void IsEmpty(string aString, string message, params object[] args)
		{
			Assert.That<string>(aString, new EmptyStringConstraint(), message, args);
		}

		// Token: 0x0600032F RID: 815 RVA: 0x0000B9F9 File Offset: 0x00009BF9
		public static void IsEmpty(string aString)
		{
			Assert.That<string>(aString, new EmptyStringConstraint(), null, null);
		}

		// Token: 0x06000330 RID: 816 RVA: 0x0000BA0A File Offset: 0x00009C0A
		public static void IsEmpty(IEnumerable collection, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, new EmptyCollectionConstraint(), message, args);
		}

		// Token: 0x06000331 RID: 817 RVA: 0x0000BA1B File Offset: 0x00009C1B
		public static void IsEmpty(IEnumerable collection)
		{
			Assert.That<IEnumerable>(collection, new EmptyCollectionConstraint(), null, null);
		}

		// Token: 0x06000332 RID: 818 RVA: 0x0000BA2C File Offset: 0x00009C2C
		public static void IsNotEmpty(string aString, string message, params object[] args)
		{
			Assert.That<string>(aString, Is.Not.Empty, message, args);
		}

		// Token: 0x06000333 RID: 819 RVA: 0x0000BA42 File Offset: 0x00009C42
		public static void IsNotEmpty(string aString)
		{
			Assert.That<string>(aString, Is.Not.Empty, null, null);
		}

		// Token: 0x06000334 RID: 820 RVA: 0x0000BA58 File Offset: 0x00009C58
		public static void IsNotEmpty(IEnumerable collection, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, Is.Not.Empty, message, args);
		}

		// Token: 0x06000335 RID: 821 RVA: 0x0000BA6E File Offset: 0x00009C6E
		public static void IsNotEmpty(IEnumerable collection)
		{
			Assert.That<IEnumerable>(collection, Is.Not.Empty, null, null);
		}

		// Token: 0x06000336 RID: 822 RVA: 0x0000BA84 File Offset: 0x00009C84
		public static void Zero(int actual)
		{
			Assert.That<int>(actual, Is.Zero);
		}

		// Token: 0x06000337 RID: 823 RVA: 0x0000BA93 File Offset: 0x00009C93
		public static void Zero(int actual, string message, params object[] args)
		{
			Assert.That<int>(actual, Is.Zero, message, args);
		}

		// Token: 0x06000338 RID: 824 RVA: 0x0000BAA4 File Offset: 0x00009CA4
		[CLSCompliant(false)]
		public static void Zero(uint actual)
		{
			Assert.That<uint>(actual, Is.Zero);
		}

		// Token: 0x06000339 RID: 825 RVA: 0x0000BAB3 File Offset: 0x00009CB3
		[CLSCompliant(false)]
		public static void Zero(uint actual, string message, params object[] args)
		{
			Assert.That<uint>(actual, Is.Zero, message, args);
		}

		// Token: 0x0600033A RID: 826 RVA: 0x0000BAC4 File Offset: 0x00009CC4
		public static void Zero(long actual)
		{
			Assert.That<long>(actual, Is.Zero);
		}

		// Token: 0x0600033B RID: 827 RVA: 0x0000BAD3 File Offset: 0x00009CD3
		public static void Zero(long actual, string message, params object[] args)
		{
			Assert.That<long>(actual, Is.Zero, message, args);
		}

		// Token: 0x0600033C RID: 828 RVA: 0x0000BAE4 File Offset: 0x00009CE4
		[CLSCompliant(false)]
		public static void Zero(ulong actual)
		{
			Assert.That<ulong>(actual, Is.Zero);
		}

		// Token: 0x0600033D RID: 829 RVA: 0x0000BAF3 File Offset: 0x00009CF3
		[CLSCompliant(false)]
		public static void Zero(ulong actual, string message, params object[] args)
		{
			Assert.That<ulong>(actual, Is.Zero, message, args);
		}

		// Token: 0x0600033E RID: 830 RVA: 0x0000BB04 File Offset: 0x00009D04
		public static void Zero(decimal actual)
		{
			Assert.That<decimal>(actual, Is.Zero);
		}

		// Token: 0x0600033F RID: 831 RVA: 0x0000BB13 File Offset: 0x00009D13
		public static void Zero(decimal actual, string message, params object[] args)
		{
			Assert.That<decimal>(actual, Is.Zero, message, args);
		}

		// Token: 0x06000340 RID: 832 RVA: 0x0000BB24 File Offset: 0x00009D24
		public static void Zero(double actual)
		{
			Assert.That<double>(actual, Is.Zero);
		}

		// Token: 0x06000341 RID: 833 RVA: 0x0000BB33 File Offset: 0x00009D33
		public static void Zero(double actual, string message, params object[] args)
		{
			Assert.That<double>(actual, Is.Zero, message, args);
		}

		// Token: 0x06000342 RID: 834 RVA: 0x0000BB44 File Offset: 0x00009D44
		public static void Zero(float actual)
		{
			Assert.That<float>(actual, Is.Zero);
		}

		// Token: 0x06000343 RID: 835 RVA: 0x0000BB53 File Offset: 0x00009D53
		public static void Zero(float actual, string message, params object[] args)
		{
			Assert.That<float>(actual, Is.Zero, message, args);
		}

		// Token: 0x06000344 RID: 836 RVA: 0x0000BB64 File Offset: 0x00009D64
		public static void NotZero(int actual)
		{
			Assert.That<int>(actual, Is.Not.Zero);
		}

		// Token: 0x06000345 RID: 837 RVA: 0x0000BB78 File Offset: 0x00009D78
		public static void NotZero(int actual, string message, params object[] args)
		{
			Assert.That<int>(actual, Is.Not.Zero, message, args);
		}

		// Token: 0x06000346 RID: 838 RVA: 0x0000BB8E File Offset: 0x00009D8E
		[CLSCompliant(false)]
		public static void NotZero(uint actual)
		{
			Assert.That<uint>(actual, Is.Not.Zero);
		}

		// Token: 0x06000347 RID: 839 RVA: 0x0000BBA2 File Offset: 0x00009DA2
		[CLSCompliant(false)]
		public static void NotZero(uint actual, string message, params object[] args)
		{
			Assert.That<uint>(actual, Is.Not.Zero, message, args);
		}

		// Token: 0x06000348 RID: 840 RVA: 0x0000BBB8 File Offset: 0x00009DB8
		public static void NotZero(long actual)
		{
			Assert.That<long>(actual, Is.Not.Zero);
		}

		// Token: 0x06000349 RID: 841 RVA: 0x0000BBCC File Offset: 0x00009DCC
		public static void NotZero(long actual, string message, params object[] args)
		{
			Assert.That<long>(actual, Is.Not.Zero, message, args);
		}

		// Token: 0x0600034A RID: 842 RVA: 0x0000BBE2 File Offset: 0x00009DE2
		[CLSCompliant(false)]
		public static void NotZero(ulong actual)
		{
			Assert.That<ulong>(actual, Is.Not.Zero);
		}

		// Token: 0x0600034B RID: 843 RVA: 0x0000BBF6 File Offset: 0x00009DF6
		[CLSCompliant(false)]
		public static void NotZero(ulong actual, string message, params object[] args)
		{
			Assert.That<ulong>(actual, Is.Not.Zero, message, args);
		}

		// Token: 0x0600034C RID: 844 RVA: 0x0000BC0C File Offset: 0x00009E0C
		public static void NotZero(decimal actual)
		{
			Assert.That<decimal>(actual, Is.Not.Zero);
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000BC20 File Offset: 0x00009E20
		public static void NotZero(decimal actual, string message, params object[] args)
		{
			Assert.That<decimal>(actual, Is.Not.Zero, message, args);
		}

		// Token: 0x0600034E RID: 846 RVA: 0x0000BC36 File Offset: 0x00009E36
		public static void NotZero(double actual)
		{
			Assert.That<double>(actual, Is.Not.Zero);
		}

		// Token: 0x0600034F RID: 847 RVA: 0x0000BC4A File Offset: 0x00009E4A
		public static void NotZero(double actual, string message, params object[] args)
		{
			Assert.That<double>(actual, Is.Not.Zero, message, args);
		}

		// Token: 0x06000350 RID: 848 RVA: 0x0000BC60 File Offset: 0x00009E60
		public static void NotZero(float actual)
		{
			Assert.That<float>(actual, Is.Not.Zero);
		}

		// Token: 0x06000351 RID: 849 RVA: 0x0000BC74 File Offset: 0x00009E74
		public static void NotZero(float actual, string message, params object[] args)
		{
			Assert.That<float>(actual, Is.Not.Zero, message, args);
		}

		// Token: 0x06000352 RID: 850 RVA: 0x0000BC8A File Offset: 0x00009E8A
		public static void Positive(int actual)
		{
			Assert.That<int>(actual, Is.Positive);
		}

		// Token: 0x06000353 RID: 851 RVA: 0x0000BC99 File Offset: 0x00009E99
		public static void Positive(int actual, string message, params object[] args)
		{
			Assert.That<int>(actual, Is.Positive, message, args);
		}

		// Token: 0x06000354 RID: 852 RVA: 0x0000BCAA File Offset: 0x00009EAA
		[CLSCompliant(false)]
		public static void Positive(uint actual)
		{
			Assert.That<uint>(actual, Is.Positive);
		}

		// Token: 0x06000355 RID: 853 RVA: 0x0000BCB9 File Offset: 0x00009EB9
		[CLSCompliant(false)]
		public static void Positive(uint actual, string message, params object[] args)
		{
			Assert.That<uint>(actual, Is.Positive, message, args);
		}

		// Token: 0x06000356 RID: 854 RVA: 0x0000BCCA File Offset: 0x00009ECA
		public static void Positive(long actual)
		{
			Assert.That<long>(actual, Is.Positive);
		}

		// Token: 0x06000357 RID: 855 RVA: 0x0000BCD9 File Offset: 0x00009ED9
		public static void Positive(long actual, string message, params object[] args)
		{
			Assert.That<long>(actual, Is.Positive, message, args);
		}

		// Token: 0x06000358 RID: 856 RVA: 0x0000BCEA File Offset: 0x00009EEA
		[CLSCompliant(false)]
		public static void Positive(ulong actual)
		{
			Assert.That<ulong>(actual, Is.Positive);
		}

		// Token: 0x06000359 RID: 857 RVA: 0x0000BCF9 File Offset: 0x00009EF9
		[CLSCompliant(false)]
		public static void Positive(ulong actual, string message, params object[] args)
		{
			Assert.That<ulong>(actual, Is.Positive, message, args);
		}

		// Token: 0x0600035A RID: 858 RVA: 0x0000BD0A File Offset: 0x00009F0A
		public static void Positive(decimal actual)
		{
			Assert.That<decimal>(actual, Is.Positive);
		}

		// Token: 0x0600035B RID: 859 RVA: 0x0000BD19 File Offset: 0x00009F19
		public static void Positive(decimal actual, string message, params object[] args)
		{
			Assert.That<decimal>(actual, Is.Positive, message, args);
		}

		// Token: 0x0600035C RID: 860 RVA: 0x0000BD2A File Offset: 0x00009F2A
		public static void Positive(double actual)
		{
			Assert.That<double>(actual, Is.Positive);
		}

		// Token: 0x0600035D RID: 861 RVA: 0x0000BD39 File Offset: 0x00009F39
		public static void Positive(double actual, string message, params object[] args)
		{
			Assert.That<double>(actual, Is.Positive, message, args);
		}

		// Token: 0x0600035E RID: 862 RVA: 0x0000BD4A File Offset: 0x00009F4A
		public static void Positive(float actual)
		{
			Assert.That<float>(actual, Is.Positive);
		}

		// Token: 0x0600035F RID: 863 RVA: 0x0000BD59 File Offset: 0x00009F59
		public static void Positive(float actual, string message, params object[] args)
		{
			Assert.That<float>(actual, Is.Positive, message, args);
		}

		// Token: 0x06000360 RID: 864 RVA: 0x0000BD6A File Offset: 0x00009F6A
		public static void Negative(int actual)
		{
			Assert.That<int>(actual, Is.Negative);
		}

		// Token: 0x06000361 RID: 865 RVA: 0x0000BD79 File Offset: 0x00009F79
		public static void Negative(int actual, string message, params object[] args)
		{
			Assert.That<int>(actual, Is.Negative, message, args);
		}

		// Token: 0x06000362 RID: 866 RVA: 0x0000BD8A File Offset: 0x00009F8A
		[CLSCompliant(false)]
		public static void Negative(uint actual)
		{
			Assert.That<uint>(actual, Is.Negative);
		}

		// Token: 0x06000363 RID: 867 RVA: 0x0000BD99 File Offset: 0x00009F99
		[CLSCompliant(false)]
		public static void Negative(uint actual, string message, params object[] args)
		{
			Assert.That<uint>(actual, Is.Negative, message, args);
		}

		// Token: 0x06000364 RID: 868 RVA: 0x0000BDAA File Offset: 0x00009FAA
		public static void Negative(long actual)
		{
			Assert.That<long>(actual, Is.Negative);
		}

		// Token: 0x06000365 RID: 869 RVA: 0x0000BDB9 File Offset: 0x00009FB9
		public static void Negative(long actual, string message, params object[] args)
		{
			Assert.That<long>(actual, Is.Negative, message, args);
		}

		// Token: 0x06000366 RID: 870 RVA: 0x0000BDCA File Offset: 0x00009FCA
		[CLSCompliant(false)]
		public static void Negative(ulong actual)
		{
			Assert.That<ulong>(actual, Is.Negative);
		}

		// Token: 0x06000367 RID: 871 RVA: 0x0000BDD9 File Offset: 0x00009FD9
		[CLSCompliant(false)]
		public static void Negative(ulong actual, string message, params object[] args)
		{
			Assert.That<ulong>(actual, Is.Negative, message, args);
		}

		// Token: 0x06000368 RID: 872 RVA: 0x0000BDEA File Offset: 0x00009FEA
		public static void Negative(decimal actual)
		{
			Assert.That<decimal>(actual, Is.Negative);
		}

		// Token: 0x06000369 RID: 873 RVA: 0x0000BDF9 File Offset: 0x00009FF9
		public static void Negative(decimal actual, string message, params object[] args)
		{
			Assert.That<decimal>(actual, Is.Negative, message, args);
		}

		// Token: 0x0600036A RID: 874 RVA: 0x0000BE0A File Offset: 0x0000A00A
		public static void Negative(double actual)
		{
			Assert.That<double>(actual, Is.Negative);
		}

		// Token: 0x0600036B RID: 875 RVA: 0x0000BE19 File Offset: 0x0000A019
		public static void Negative(double actual, string message, params object[] args)
		{
			Assert.That<double>(actual, Is.Negative, message, args);
		}

		// Token: 0x0600036C RID: 876 RVA: 0x0000BE2A File Offset: 0x0000A02A
		public static void Negative(float actual)
		{
			Assert.That<float>(actual, Is.Negative);
		}

		// Token: 0x0600036D RID: 877 RVA: 0x0000BE39 File Offset: 0x0000A039
		public static void Negative(float actual, string message, params object[] args)
		{
			Assert.That<float>(actual, Is.Negative, message, args);
		}

		// Token: 0x0600036E RID: 878 RVA: 0x0000BE4A File Offset: 0x0000A04A
		public static void IsAssignableFrom(Type expected, object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.AssignableFrom(expected), message, args);
		}

		// Token: 0x0600036F RID: 879 RVA: 0x0000BE5C File Offset: 0x0000A05C
		public static void IsAssignableFrom(Type expected, object actual)
		{
			Assert.That<object>(actual, Is.AssignableFrom(expected), null, null);
		}

		// Token: 0x06000370 RID: 880 RVA: 0x0000BE6E File Offset: 0x0000A06E
		public static void IsAssignableFrom<TExpected>(object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.AssignableFrom(typeof(TExpected)), message, args);
		}

		// Token: 0x06000371 RID: 881 RVA: 0x0000BE89 File Offset: 0x0000A089
		public static void IsAssignableFrom<TExpected>(object actual)
		{
			Assert.That<object>(actual, Is.AssignableFrom(typeof(TExpected)), null, null);
		}

		// Token: 0x06000372 RID: 882 RVA: 0x0000BEA4 File Offset: 0x0000A0A4
		public static void IsNotAssignableFrom(Type expected, object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.Not.AssignableFrom(expected), message, args);
		}

		// Token: 0x06000373 RID: 883 RVA: 0x0000BEBB File Offset: 0x0000A0BB
		public static void IsNotAssignableFrom(Type expected, object actual)
		{
			Assert.That<object>(actual, Is.Not.AssignableFrom(expected), null, null);
		}

		// Token: 0x06000374 RID: 884 RVA: 0x0000BED2 File Offset: 0x0000A0D2
		public static void IsNotAssignableFrom<TExpected>(object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.Not.AssignableFrom(typeof(TExpected)), message, args);
		}

		// Token: 0x06000375 RID: 885 RVA: 0x0000BEF2 File Offset: 0x0000A0F2
		public static void IsNotAssignableFrom<TExpected>(object actual)
		{
			Assert.That<object>(actual, Is.Not.AssignableFrom(typeof(TExpected)), null, null);
		}

		// Token: 0x06000376 RID: 886 RVA: 0x0000BF12 File Offset: 0x0000A112
		public static void IsInstanceOf(Type expected, object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.InstanceOf(expected), message, args);
		}

		// Token: 0x06000377 RID: 887 RVA: 0x0000BF24 File Offset: 0x0000A124
		public static void IsInstanceOf(Type expected, object actual)
		{
			Assert.That<object>(actual, Is.InstanceOf(expected), null, null);
		}

		// Token: 0x06000378 RID: 888 RVA: 0x0000BF36 File Offset: 0x0000A136
		public static void IsInstanceOf<TExpected>(object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.InstanceOf(typeof(TExpected)), message, args);
		}

		// Token: 0x06000379 RID: 889 RVA: 0x0000BF51 File Offset: 0x0000A151
		public static void IsInstanceOf<TExpected>(object actual)
		{
			Assert.That<object>(actual, Is.InstanceOf(typeof(TExpected)), null, null);
		}

		// Token: 0x0600037A RID: 890 RVA: 0x0000BF6C File Offset: 0x0000A16C
		public static void IsNotInstanceOf(Type expected, object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.Not.InstanceOf(expected), message, args);
		}

		// Token: 0x0600037B RID: 891 RVA: 0x0000BF83 File Offset: 0x0000A183
		public static void IsNotInstanceOf(Type expected, object actual)
		{
			Assert.That<object>(actual, Is.Not.InstanceOf(expected), null, null);
		}

		// Token: 0x0600037C RID: 892 RVA: 0x0000BF9A File Offset: 0x0000A19A
		public static void IsNotInstanceOf<TExpected>(object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.Not.InstanceOf(typeof(TExpected)), message, args);
		}

		// Token: 0x0600037D RID: 893 RVA: 0x0000BFBA File Offset: 0x0000A1BA
		public static void IsNotInstanceOf<TExpected>(object actual)
		{
			Assert.That<object>(actual, Is.Not.InstanceOf(typeof(TExpected)), null, null);
		}

		// Token: 0x0600037E RID: 894 RVA: 0x0000BFDC File Offset: 0x0000A1DC
		public static Exception Throws(IResolveConstraint expression, TestDelegate code, string message, params object[] args)
		{
			Exception ex = null;
			try
			{
				code();
			}
			catch (Exception ex2)
			{
				ex = ex2;
			}
			Assert.That<Exception>(ex, expression, message, args);
			return ex;
		}

		// Token: 0x0600037F RID: 895 RVA: 0x0000C020 File Offset: 0x0000A220
		public static Exception Throws(IResolveConstraint expression, TestDelegate code)
		{
			return Assert.Throws(expression, code, string.Empty, null);
		}

		// Token: 0x06000380 RID: 896 RVA: 0x0000C040 File Offset: 0x0000A240
		public static Exception Throws(Type expectedExceptionType, TestDelegate code, string message, params object[] args)
		{
			return Assert.Throws(new ExceptionTypeConstraint(expectedExceptionType), code, message, args);
		}

		// Token: 0x06000381 RID: 897 RVA: 0x0000C060 File Offset: 0x0000A260
		public static Exception Throws(Type expectedExceptionType, TestDelegate code)
		{
			return Assert.Throws(new ExceptionTypeConstraint(expectedExceptionType), code, string.Empty, null);
		}

		// Token: 0x06000382 RID: 898 RVA: 0x0000C084 File Offset: 0x0000A284
		public static TActual Throws<TActual>(TestDelegate code, string message, params object[] args) where TActual : Exception
		{
			return (TActual)((object)Assert.Throws(typeof(TActual), code, message, args));
		}

		// Token: 0x06000383 RID: 899 RVA: 0x0000C0B0 File Offset: 0x0000A2B0
		public static TActual Throws<TActual>(TestDelegate code) where TActual : Exception
		{
			return Assert.Throws<TActual>(code, string.Empty, null);
		}

		// Token: 0x06000384 RID: 900 RVA: 0x0000C0D0 File Offset: 0x0000A2D0
		public static Exception Catch(TestDelegate code, string message, params object[] args)
		{
			return Assert.Throws(new InstanceOfTypeConstraint(typeof(Exception)), code, message, args);
		}

		// Token: 0x06000385 RID: 901 RVA: 0x0000C0FC File Offset: 0x0000A2FC
		public static Exception Catch(TestDelegate code)
		{
			return Assert.Throws(new InstanceOfTypeConstraint(typeof(Exception)), code);
		}

		// Token: 0x06000386 RID: 902 RVA: 0x0000C124 File Offset: 0x0000A324
		public static Exception Catch(Type expectedExceptionType, TestDelegate code, string message, params object[] args)
		{
			return Assert.Throws(new InstanceOfTypeConstraint(expectedExceptionType), code, message, args);
		}

		// Token: 0x06000387 RID: 903 RVA: 0x0000C144 File Offset: 0x0000A344
		public static Exception Catch(Type expectedExceptionType, TestDelegate code)
		{
			return Assert.Throws(new InstanceOfTypeConstraint(expectedExceptionType), code);
		}

		// Token: 0x06000388 RID: 904 RVA: 0x0000C164 File Offset: 0x0000A364
		public static TActual Catch<TActual>(TestDelegate code, string message, params object[] args) where TActual : Exception
		{
			return (TActual)((object)Assert.Throws(new InstanceOfTypeConstraint(typeof(TActual)), code, message, args));
		}

		// Token: 0x06000389 RID: 905 RVA: 0x0000C194 File Offset: 0x0000A394
		public static TActual Catch<TActual>(TestDelegate code) where TActual : Exception
		{
			return (TActual)((object)Assert.Throws(new InstanceOfTypeConstraint(typeof(TActual)), code));
		}

		// Token: 0x0600038A RID: 906 RVA: 0x0000C1C0 File Offset: 0x0000A3C0
		public static void DoesNotThrow(TestDelegate code, string message, params object[] args)
		{
			Assert.That(code, new ThrowsNothingConstraint(), message, args);
		}

		// Token: 0x0600038B RID: 907 RVA: 0x0000C1D1 File Offset: 0x0000A3D1
		public static void DoesNotThrow(TestDelegate code)
		{
			Assert.DoesNotThrow(code, string.Empty, null);
		}

		// Token: 0x0600038C RID: 908 RVA: 0x0000C1E1 File Offset: 0x0000A3E1
		public static void That(bool condition, string message, params object[] args)
		{
			Assert.That<bool>(condition, Is.True, message, args);
		}

		// Token: 0x0600038D RID: 909 RVA: 0x0000C1F2 File Offset: 0x0000A3F2
		public static void That(bool condition)
		{
			Assert.That<bool>(condition, Is.True, null, null);
		}

		// Token: 0x0600038E RID: 910 RVA: 0x0000C203 File Offset: 0x0000A403
		public static void That(bool condition, Func<string> getExceptionMessage)
		{
			Assert.That<bool>(condition, Is.True, getExceptionMessage);
		}

		// Token: 0x0600038F RID: 911 RVA: 0x0000C213 File Offset: 0x0000A413
		public static void That(Func<bool> condition, string message, params object[] args)
		{
			Assert.That<bool>(condition(), Is.True, message, args);
		}

		// Token: 0x06000390 RID: 912 RVA: 0x0000C229 File Offset: 0x0000A429
		public static void That(Func<bool> condition)
		{
			Assert.That<bool>(condition(), Is.True, null, null);
		}

		// Token: 0x06000391 RID: 913 RVA: 0x0000C23F File Offset: 0x0000A43F
		public static void That(Func<bool> condition, Func<string> getExceptionMessage)
		{
			Assert.That<bool>(condition(), Is.True, getExceptionMessage);
		}

		// Token: 0x06000392 RID: 914 RVA: 0x0000C254 File Offset: 0x0000A454
		public static void That<TActual>(ActualValueDelegate<TActual> del, IResolveConstraint expr)
		{
			Assert.That<TActual>(del, expr.Resolve(), null, null);
		}

		// Token: 0x06000393 RID: 915 RVA: 0x0000C268 File Offset: 0x0000A468
		public static void That<TActual>(ActualValueDelegate<TActual> del, IResolveConstraint expr, string message, params object[] args)
		{
			IConstraint constraint = expr.Resolve();
			Assert.IncrementAssertCount();
			ConstraintResult constraintResult = constraint.ApplyTo<TActual>(del);
			if (!constraintResult.IsSuccess)
			{
				MessageWriter messageWriter = new TextMessageWriter(message, args);
				constraintResult.WriteMessageTo(messageWriter);
				throw new AssertionException(messageWriter.ToString());
			}
		}

		// Token: 0x06000394 RID: 916 RVA: 0x0000C2B4 File Offset: 0x0000A4B4
		public static void That<TActual>(ActualValueDelegate<TActual> del, IResolveConstraint expr, Func<string> getExceptionMessage)
		{
			IConstraint constraint = expr.Resolve();
			Assert.IncrementAssertCount();
			ConstraintResult constraintResult = constraint.ApplyTo<TActual>(del);
			if (!constraintResult.IsSuccess)
			{
				throw new AssertionException(getExceptionMessage());
			}
		}

		// Token: 0x06000395 RID: 917 RVA: 0x0000C2EE File Offset: 0x0000A4EE
		public static void That(TestDelegate code, IResolveConstraint constraint)
		{
			Assert.That(code, constraint, null, null);
		}

		// Token: 0x06000396 RID: 918 RVA: 0x0000C2FB File Offset: 0x0000A4FB
		public static void That(TestDelegate code, IResolveConstraint constraint, string message, params object[] args)
		{
			Assert.That<object>(code, constraint, message, args);
		}

		// Token: 0x06000397 RID: 919 RVA: 0x0000C308 File Offset: 0x0000A508
		public static void That(TestDelegate code, IResolveConstraint constraint, Func<string> getExceptionMessage)
		{
			Assert.That<object>(code, constraint, getExceptionMessage);
		}

		// Token: 0x06000398 RID: 920 RVA: 0x0000C314 File Offset: 0x0000A514
		public static void That<TActual>(TActual actual, IResolveConstraint expression)
		{
			Assert.That<TActual>(actual, expression, null, null);
		}

		// Token: 0x06000399 RID: 921 RVA: 0x0000C324 File Offset: 0x0000A524
		public static void That<TActual>(TActual actual, IResolveConstraint expression, string message, params object[] args)
		{
			IConstraint constraint = expression.Resolve();
			Assert.IncrementAssertCount();
			ConstraintResult constraintResult = constraint.ApplyTo(actual);
			if (!constraintResult.IsSuccess)
			{
				MessageWriter messageWriter = new TextMessageWriter(message, args);
				constraintResult.WriteMessageTo(messageWriter);
				throw new AssertionException(messageWriter.ToString());
			}
		}

		// Token: 0x0600039A RID: 922 RVA: 0x0000C374 File Offset: 0x0000A574
		public static void That<TActual>(TActual actual, IResolveConstraint expression, Func<string> getExceptionMessage)
		{
			IConstraint constraint = expression.Resolve();
			Assert.IncrementAssertCount();
			ConstraintResult constraintResult = constraint.ApplyTo(actual);
			if (!constraintResult.IsSuccess)
			{
				throw new AssertionException(getExceptionMessage());
			}
		}

		// Token: 0x0600039B RID: 923 RVA: 0x0000C3B3 File Offset: 0x0000A5B3
		public static void ByVal(object actual, IResolveConstraint expression)
		{
			Assert.That<object>(actual, expression, null, null);
		}

		// Token: 0x0600039C RID: 924 RVA: 0x0000C3C0 File Offset: 0x0000A5C0
		public static void ByVal(object actual, IResolveConstraint expression, string message, params object[] args)
		{
			Assert.That<object>(actual, expression, message, args);
		}

		// Token: 0x0600039D RID: 925 RVA: 0x0000C3CD File Offset: 0x0000A5CD
		protected Assert()
		{
		}

		// Token: 0x0600039E RID: 926 RVA: 0x0000C3D8 File Offset: 0x0000A5D8
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new static bool Equals(object a, object b)
		{
			throw new InvalidOperationException("Assert.Equals should not be used for Assertions");
		}

		// Token: 0x0600039F RID: 927 RVA: 0x0000C3E5 File Offset: 0x0000A5E5
		public new static void ReferenceEquals(object a, object b)
		{
			throw new InvalidOperationException("Assert.ReferenceEquals should not be used for Assertions");
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x0000C3F4 File Offset: 0x0000A5F4
		public static void Pass(string message, params object[] args)
		{
			if (message == null)
			{
				message = string.Empty;
			}
			else if (args != null && args.Length > 0)
			{
				message = string.Format(message, args);
			}
			throw new SuccessException(message);
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x0000C438 File Offset: 0x0000A638
		public static void Pass(string message)
		{
			Assert.Pass(message, null);
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x0000C443 File Offset: 0x0000A643
		public static void Pass()
		{
			Assert.Pass(string.Empty, null);
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x0000C454 File Offset: 0x0000A654
		public static void Fail(string message, params object[] args)
		{
			if (message == null)
			{
				message = string.Empty;
			}
			else if (args != null && args.Length > 0)
			{
				message = string.Format(message, args);
			}
			throw new AssertionException(message);
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x0000C498 File Offset: 0x0000A698
		public static void Fail(string message)
		{
			Assert.Fail(message, null);
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x0000C4A3 File Offset: 0x0000A6A3
		public static void Fail()
		{
			Assert.Fail(string.Empty, null);
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x0000C4B4 File Offset: 0x0000A6B4
		public static void Ignore(string message, params object[] args)
		{
			if (message == null)
			{
				message = string.Empty;
			}
			else if (args != null && args.Length > 0)
			{
				message = string.Format(message, args);
			}
			throw new IgnoreException(message);
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x0000C4F8 File Offset: 0x0000A6F8
		public static void Ignore(string message)
		{
			Assert.Ignore(message, null);
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x0000C503 File Offset: 0x0000A703
		public static void Ignore()
		{
			Assert.Ignore(string.Empty, null);
		}

		// Token: 0x060003A9 RID: 937 RVA: 0x0000C514 File Offset: 0x0000A714
		public static void Inconclusive(string message, params object[] args)
		{
			if (message == null)
			{
				message = string.Empty;
			}
			else if (args != null && args.Length > 0)
			{
				message = string.Format(message, args);
			}
			throw new InconclusiveException(message);
		}

		// Token: 0x060003AA RID: 938 RVA: 0x0000C558 File Offset: 0x0000A758
		public static void Inconclusive(string message)
		{
			Assert.Inconclusive(message, null);
		}

		// Token: 0x060003AB RID: 939 RVA: 0x0000C563 File Offset: 0x0000A763
		public static void Inconclusive()
		{
			Assert.Inconclusive(string.Empty, null);
		}

		// Token: 0x060003AC RID: 940 RVA: 0x0000C572 File Offset: 0x0000A772
		public static void Contains(object expected, ICollection actual, string message, params object[] args)
		{
			Assert.That<ICollection>(actual, new CollectionContainsConstraint(expected), message, args);
		}

		// Token: 0x060003AD RID: 941 RVA: 0x0000C584 File Offset: 0x0000A784
		public static void Contains(object expected, ICollection actual)
		{
			Assert.That<ICollection>(actual, new CollectionContainsConstraint(expected), null, null);
		}

		// Token: 0x060003AE RID: 942 RVA: 0x0000C596 File Offset: 0x0000A796
		public static void Greater(int arg1, int arg2, string message, params object[] args)
		{
			Assert.That<int>(arg1, Is.GreaterThan(arg2), message, args);
		}

		// Token: 0x060003AF RID: 943 RVA: 0x0000C5AD File Offset: 0x0000A7AD
		public static void Greater(int arg1, int arg2)
		{
			Assert.That<int>(arg1, Is.GreaterThan(arg2), null, null);
		}

		// Token: 0x060003B0 RID: 944 RVA: 0x0000C5C4 File Offset: 0x0000A7C4
		[CLSCompliant(false)]
		public static void Greater(uint arg1, uint arg2, string message, params object[] args)
		{
			Assert.That<uint>(arg1, Is.GreaterThan(arg2), message, args);
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x0000C5DB File Offset: 0x0000A7DB
		[CLSCompliant(false)]
		public static void Greater(uint arg1, uint arg2)
		{
			Assert.That<uint>(arg1, Is.GreaterThan(arg2), null, null);
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x0000C5F2 File Offset: 0x0000A7F2
		public static void Greater(long arg1, long arg2, string message, params object[] args)
		{
			Assert.That<long>(arg1, Is.GreaterThan(arg2), message, args);
		}

		// Token: 0x060003B3 RID: 947 RVA: 0x0000C609 File Offset: 0x0000A809
		public static void Greater(long arg1, long arg2)
		{
			Assert.That<long>(arg1, Is.GreaterThan(arg2), null, null);
		}

		// Token: 0x060003B4 RID: 948 RVA: 0x0000C620 File Offset: 0x0000A820
		[CLSCompliant(false)]
		public static void Greater(ulong arg1, ulong arg2, string message, params object[] args)
		{
			Assert.That<ulong>(arg1, Is.GreaterThan(arg2), message, args);
		}

		// Token: 0x060003B5 RID: 949 RVA: 0x0000C637 File Offset: 0x0000A837
		[CLSCompliant(false)]
		public static void Greater(ulong arg1, ulong arg2)
		{
			Assert.That<ulong>(arg1, Is.GreaterThan(arg2), null, null);
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x0000C64E File Offset: 0x0000A84E
		public static void Greater(decimal arg1, decimal arg2, string message, params object[] args)
		{
			Assert.That<decimal>(arg1, Is.GreaterThan(arg2), message, args);
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x0000C665 File Offset: 0x0000A865
		public static void Greater(decimal arg1, decimal arg2)
		{
			Assert.That<decimal>(arg1, Is.GreaterThan(arg2), null, null);
		}

		// Token: 0x060003B8 RID: 952 RVA: 0x0000C67C File Offset: 0x0000A87C
		public static void Greater(double arg1, double arg2, string message, params object[] args)
		{
			Assert.That<double>(arg1, Is.GreaterThan(arg2), message, args);
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x0000C693 File Offset: 0x0000A893
		public static void Greater(double arg1, double arg2)
		{
			Assert.That<double>(arg1, Is.GreaterThan(arg2), null, null);
		}

		// Token: 0x060003BA RID: 954 RVA: 0x0000C6AA File Offset: 0x0000A8AA
		public static void Greater(float arg1, float arg2, string message, params object[] args)
		{
			Assert.That<float>(arg1, Is.GreaterThan(arg2), message, args);
		}

		// Token: 0x060003BB RID: 955 RVA: 0x0000C6C1 File Offset: 0x0000A8C1
		public static void Greater(float arg1, float arg2)
		{
			Assert.That<float>(arg1, Is.GreaterThan(arg2), null, null);
		}

		// Token: 0x060003BC RID: 956 RVA: 0x0000C6D8 File Offset: 0x0000A8D8
		public static void Greater(IComparable arg1, IComparable arg2, string message, params object[] args)
		{
			Assert.That<IComparable>(arg1, Is.GreaterThan(arg2), message, args);
		}

		// Token: 0x060003BD RID: 957 RVA: 0x0000C6EA File Offset: 0x0000A8EA
		public static void Greater(IComparable arg1, IComparable arg2)
		{
			Assert.That<IComparable>(arg1, Is.GreaterThan(arg2), null, null);
		}

		// Token: 0x060003BE RID: 958 RVA: 0x0000C6FC File Offset: 0x0000A8FC
		public static void Less(int arg1, int arg2, string message, params object[] args)
		{
			Assert.That<int>(arg1, Is.LessThan(arg2), message, args);
		}

		// Token: 0x060003BF RID: 959 RVA: 0x0000C713 File Offset: 0x0000A913
		public static void Less(int arg1, int arg2)
		{
			Assert.That<int>(arg1, Is.LessThan(arg2), null, null);
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x0000C72A File Offset: 0x0000A92A
		[CLSCompliant(false)]
		public static void Less(uint arg1, uint arg2, string message, params object[] args)
		{
			Assert.That<uint>(arg1, Is.LessThan(arg2), message, args);
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x0000C741 File Offset: 0x0000A941
		[CLSCompliant(false)]
		public static void Less(uint arg1, uint arg2)
		{
			Assert.That<uint>(arg1, Is.LessThan(arg2), null, null);
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x0000C758 File Offset: 0x0000A958
		public static void Less(long arg1, long arg2, string message, params object[] args)
		{
			Assert.That<long>(arg1, Is.LessThan(arg2), message, args);
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x0000C76F File Offset: 0x0000A96F
		public static void Less(long arg1, long arg2)
		{
			Assert.That<long>(arg1, Is.LessThan(arg2), null, null);
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x0000C786 File Offset: 0x0000A986
		[CLSCompliant(false)]
		public static void Less(ulong arg1, ulong arg2, string message, params object[] args)
		{
			Assert.That<ulong>(arg1, Is.LessThan(arg2), message, args);
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x0000C79D File Offset: 0x0000A99D
		[CLSCompliant(false)]
		public static void Less(ulong arg1, ulong arg2)
		{
			Assert.That<ulong>(arg1, Is.LessThan(arg2), null, null);
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x0000C7B4 File Offset: 0x0000A9B4
		public static void Less(decimal arg1, decimal arg2, string message, params object[] args)
		{
			Assert.That<decimal>(arg1, Is.LessThan(arg2), message, args);
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x0000C7CB File Offset: 0x0000A9CB
		public static void Less(decimal arg1, decimal arg2)
		{
			Assert.That<decimal>(arg1, Is.LessThan(arg2), null, null);
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x0000C7E2 File Offset: 0x0000A9E2
		public static void Less(double arg1, double arg2, string message, params object[] args)
		{
			Assert.That<double>(arg1, Is.LessThan(arg2), message, args);
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x0000C7F9 File Offset: 0x0000A9F9
		public static void Less(double arg1, double arg2)
		{
			Assert.That<double>(arg1, Is.LessThan(arg2), null, null);
		}

		// Token: 0x060003CA RID: 970 RVA: 0x0000C810 File Offset: 0x0000AA10
		public static void Less(float arg1, float arg2, string message, params object[] args)
		{
			Assert.That<float>(arg1, Is.LessThan(arg2), message, args);
		}

		// Token: 0x060003CB RID: 971 RVA: 0x0000C827 File Offset: 0x0000AA27
		public static void Less(float arg1, float arg2)
		{
			Assert.That<float>(arg1, Is.LessThan(arg2), null, null);
		}

		// Token: 0x060003CC RID: 972 RVA: 0x0000C83E File Offset: 0x0000AA3E
		public static void Less(IComparable arg1, IComparable arg2, string message, params object[] args)
		{
			Assert.That<IComparable>(arg1, Is.LessThan(arg2), message, args);
		}

		// Token: 0x060003CD RID: 973 RVA: 0x0000C850 File Offset: 0x0000AA50
		public static void Less(IComparable arg1, IComparable arg2)
		{
			Assert.That<IComparable>(arg1, Is.LessThan(arg2), null, null);
		}

		// Token: 0x060003CE RID: 974 RVA: 0x0000C862 File Offset: 0x0000AA62
		public static void GreaterOrEqual(int arg1, int arg2, string message, params object[] args)
		{
			Assert.That<int>(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003CF RID: 975 RVA: 0x0000C879 File Offset: 0x0000AA79
		public static void GreaterOrEqual(int arg1, int arg2)
		{
			Assert.That<int>(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x0000C890 File Offset: 0x0000AA90
		[CLSCompliant(false)]
		public static void GreaterOrEqual(uint arg1, uint arg2, string message, params object[] args)
		{
			Assert.That<uint>(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x0000C8A7 File Offset: 0x0000AAA7
		[CLSCompliant(false)]
		public static void GreaterOrEqual(uint arg1, uint arg2)
		{
			Assert.That<uint>(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x0000C8BE File Offset: 0x0000AABE
		public static void GreaterOrEqual(long arg1, long arg2, string message, params object[] args)
		{
			Assert.That<long>(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x0000C8D5 File Offset: 0x0000AAD5
		public static void GreaterOrEqual(long arg1, long arg2)
		{
			Assert.That<long>(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x0000C8EC File Offset: 0x0000AAEC
		[CLSCompliant(false)]
		public static void GreaterOrEqual(ulong arg1, ulong arg2, string message, params object[] args)
		{
			Assert.That<ulong>(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x0000C903 File Offset: 0x0000AB03
		[CLSCompliant(false)]
		public static void GreaterOrEqual(ulong arg1, ulong arg2)
		{
			Assert.That<ulong>(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x0000C91A File Offset: 0x0000AB1A
		public static void GreaterOrEqual(decimal arg1, decimal arg2, string message, params object[] args)
		{
			Assert.That<decimal>(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x0000C931 File Offset: 0x0000AB31
		public static void GreaterOrEqual(decimal arg1, decimal arg2)
		{
			Assert.That<decimal>(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x0000C948 File Offset: 0x0000AB48
		public static void GreaterOrEqual(double arg1, double arg2, string message, params object[] args)
		{
			Assert.That<double>(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x0000C95F File Offset: 0x0000AB5F
		public static void GreaterOrEqual(double arg1, double arg2)
		{
			Assert.That<double>(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003DA RID: 986 RVA: 0x0000C976 File Offset: 0x0000AB76
		public static void GreaterOrEqual(float arg1, float arg2, string message, params object[] args)
		{
			Assert.That<float>(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003DB RID: 987 RVA: 0x0000C98D File Offset: 0x0000AB8D
		public static void GreaterOrEqual(float arg1, float arg2)
		{
			Assert.That<float>(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003DC RID: 988 RVA: 0x0000C9A4 File Offset: 0x0000ABA4
		public static void GreaterOrEqual(IComparable arg1, IComparable arg2, string message, params object[] args)
		{
			Assert.That<IComparable>(arg1, Is.GreaterThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003DD RID: 989 RVA: 0x0000C9B6 File Offset: 0x0000ABB6
		public static void GreaterOrEqual(IComparable arg1, IComparable arg2)
		{
			Assert.That<IComparable>(arg1, Is.GreaterThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003DE RID: 990 RVA: 0x0000C9C8 File Offset: 0x0000ABC8
		public static void LessOrEqual(int arg1, int arg2, string message, params object[] args)
		{
			Assert.That<int>(arg1, Is.LessThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003DF RID: 991 RVA: 0x0000C9DF File Offset: 0x0000ABDF
		public static void LessOrEqual(int arg1, int arg2)
		{
			Assert.That<int>(arg1, Is.LessThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003E0 RID: 992 RVA: 0x0000C9F6 File Offset: 0x0000ABF6
		[CLSCompliant(false)]
		public static void LessOrEqual(uint arg1, uint arg2, string message, params object[] args)
		{
			Assert.That<uint>(arg1, Is.LessThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003E1 RID: 993 RVA: 0x0000CA0D File Offset: 0x0000AC0D
		[CLSCompliant(false)]
		public static void LessOrEqual(uint arg1, uint arg2)
		{
			Assert.That<uint>(arg1, Is.LessThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003E2 RID: 994 RVA: 0x0000CA24 File Offset: 0x0000AC24
		public static void LessOrEqual(long arg1, long arg2, string message, params object[] args)
		{
			Assert.That<long>(arg1, Is.LessThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x0000CA3B File Offset: 0x0000AC3B
		public static void LessOrEqual(long arg1, long arg2)
		{
			Assert.That<long>(arg1, Is.LessThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x0000CA52 File Offset: 0x0000AC52
		[CLSCompliant(false)]
		public static void LessOrEqual(ulong arg1, ulong arg2, string message, params object[] args)
		{
			Assert.That<ulong>(arg1, Is.LessThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003E5 RID: 997 RVA: 0x0000CA69 File Offset: 0x0000AC69
		[CLSCompliant(false)]
		public static void LessOrEqual(ulong arg1, ulong arg2)
		{
			Assert.That<ulong>(arg1, Is.LessThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003E6 RID: 998 RVA: 0x0000CA80 File Offset: 0x0000AC80
		public static void LessOrEqual(decimal arg1, decimal arg2, string message, params object[] args)
		{
			Assert.That<decimal>(arg1, Is.LessThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003E7 RID: 999 RVA: 0x0000CA97 File Offset: 0x0000AC97
		public static void LessOrEqual(decimal arg1, decimal arg2)
		{
			Assert.That<decimal>(arg1, Is.LessThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x0000CAAE File Offset: 0x0000ACAE
		public static void LessOrEqual(double arg1, double arg2, string message, params object[] args)
		{
			Assert.That<double>(arg1, Is.LessThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x0000CAC5 File Offset: 0x0000ACC5
		public static void LessOrEqual(double arg1, double arg2)
		{
			Assert.That<double>(arg1, Is.LessThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x0000CADC File Offset: 0x0000ACDC
		public static void LessOrEqual(float arg1, float arg2, string message, params object[] args)
		{
			Assert.That<float>(arg1, Is.LessThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x0000CAF3 File Offset: 0x0000ACF3
		public static void LessOrEqual(float arg1, float arg2)
		{
			Assert.That<float>(arg1, Is.LessThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003EC RID: 1004 RVA: 0x0000CB0A File Offset: 0x0000AD0A
		public static void LessOrEqual(IComparable arg1, IComparable arg2, string message, params object[] args)
		{
			Assert.That<IComparable>(arg1, Is.LessThanOrEqualTo(arg2), message, args);
		}

		// Token: 0x060003ED RID: 1005 RVA: 0x0000CB1C File Offset: 0x0000AD1C
		public static void LessOrEqual(IComparable arg1, IComparable arg2)
		{
			Assert.That<IComparable>(arg1, Is.LessThanOrEqualTo(arg2), null, null);
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x0000CB2E File Offset: 0x0000AD2E
		public static void AreEqual(double expected, double actual, double delta, string message, params object[] args)
		{
			Assert.AssertDoublesAreEqual(expected, actual, delta, message, args);
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x0000CB3D File Offset: 0x0000AD3D
		public static void AreEqual(double expected, double actual, double delta)
		{
			Assert.AssertDoublesAreEqual(expected, actual, delta, null, null);
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x0000CB4B File Offset: 0x0000AD4B
		public static void AreEqual(double expected, double? actual, double delta, string message, params object[] args)
		{
			Assert.AssertDoublesAreEqual(expected, actual.Value, delta, message, args);
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x0000CB61 File Offset: 0x0000AD61
		public static void AreEqual(double expected, double? actual, double delta)
		{
			Assert.AssertDoublesAreEqual(expected, actual.Value, delta, null, null);
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x0000CB76 File Offset: 0x0000AD76
		public static void AreEqual(object expected, object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.EqualTo(expected), message, args);
		}

		// Token: 0x060003F3 RID: 1011 RVA: 0x0000CB88 File Offset: 0x0000AD88
		public static void AreEqual(object expected, object actual)
		{
			Assert.That<object>(actual, Is.EqualTo(expected), null, null);
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x0000CB9A File Offset: 0x0000AD9A
		public static void AreNotEqual(object expected, object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.Not.EqualTo(expected), message, args);
		}

		// Token: 0x060003F5 RID: 1013 RVA: 0x0000CBB1 File Offset: 0x0000ADB1
		public static void AreNotEqual(object expected, object actual)
		{
			Assert.That<object>(actual, Is.Not.EqualTo(expected), null, null);
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x0000CBC8 File Offset: 0x0000ADC8
		public static void AreSame(object expected, object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.SameAs(expected), message, args);
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x0000CBDA File Offset: 0x0000ADDA
		public static void AreSame(object expected, object actual)
		{
			Assert.That<object>(actual, Is.SameAs(expected), null, null);
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x0000CBEC File Offset: 0x0000ADEC
		public static void AreNotSame(object expected, object actual, string message, params object[] args)
		{
			Assert.That<object>(actual, Is.Not.SameAs(expected), message, args);
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x0000CC03 File Offset: 0x0000AE03
		public static void AreNotSame(object expected, object actual)
		{
			Assert.That<object>(actual, Is.Not.SameAs(expected), null, null);
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x0000CC1C File Offset: 0x0000AE1C
		protected static void AssertDoublesAreEqual(double expected, double actual, double delta, string message, object[] args)
		{
			if (double.IsNaN(expected) || double.IsInfinity(expected))
			{
				Assert.That<double>(actual, Is.EqualTo(expected), message, args);
			}
			else
			{
				Assert.That<double>(actual, Is.EqualTo(expected).Within(delta), message, args);
			}
		}

		// Token: 0x060003FB RID: 1019 RVA: 0x0000CC79 File Offset: 0x0000AE79
		private static void IncrementAssertCount()
		{
			TestExecutionContext.CurrentContext.IncrementAssertCount();
		}
	}
}
