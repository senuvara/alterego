﻿using System;

namespace NUnit.Framework
{
	// Token: 0x020000CB RID: 203
	[Flags]
	public enum ActionTargets
	{
		// Token: 0x0400014B RID: 331
		Default = 0,
		// Token: 0x0400014C RID: 332
		Test = 1,
		// Token: 0x0400014D RID: 333
		Suite = 2
	}
}
