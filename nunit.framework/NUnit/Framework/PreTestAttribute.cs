﻿using System;

namespace NUnit.Framework
{
	// Token: 0x0200009A RID: 154
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class PreTestAttribute : NUnitAttribute
	{
		// Token: 0x060004CF RID: 1231 RVA: 0x0000FEE6 File Offset: 0x0000E0E6
		public PreTestAttribute()
		{
		}
	}
}
