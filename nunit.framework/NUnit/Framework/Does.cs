﻿using System;
using NUnit.Framework.Constraints;

namespace NUnit.Framework
{
	// Token: 0x02000085 RID: 133
	public static class Does
	{
		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x06000476 RID: 1142 RVA: 0x0000ED60 File Offset: 0x0000CF60
		public static ConstraintExpression Not
		{
			get
			{
				return new ConstraintExpression().Not;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000477 RID: 1143 RVA: 0x0000ED7C File Offset: 0x0000CF7C
		public static FileOrDirectoryExistsConstraint Exist
		{
			get
			{
				return new FileOrDirectoryExistsConstraint();
			}
		}

		// Token: 0x06000478 RID: 1144 RVA: 0x0000ED94 File Offset: 0x0000CF94
		public static CollectionContainsConstraint Contain(object expected)
		{
			return new CollectionContainsConstraint(expected);
		}

		// Token: 0x06000479 RID: 1145 RVA: 0x0000EDAC File Offset: 0x0000CFAC
		public static ContainsConstraint Contain(string expected)
		{
			return new ContainsConstraint(expected);
		}

		// Token: 0x0600047A RID: 1146 RVA: 0x0000EDC4 File Offset: 0x0000CFC4
		public static StartsWithConstraint StartWith(string expected)
		{
			return new StartsWithConstraint(expected);
		}

		// Token: 0x0600047B RID: 1147 RVA: 0x0000EDDC File Offset: 0x0000CFDC
		public static EndsWithConstraint EndWith(string expected)
		{
			return new EndsWithConstraint(expected);
		}

		// Token: 0x0600047C RID: 1148 RVA: 0x0000EDF4 File Offset: 0x0000CFF4
		public static RegexConstraint Match(string pattern)
		{
			return new RegexConstraint(pattern);
		}
	}
}
