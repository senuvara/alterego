﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x0200014F RID: 335
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class OrderAttribute : NUnitAttribute, IApplyToTest
	{
		// Token: 0x060008EC RID: 2284 RVA: 0x0001E4C7 File Offset: 0x0001C6C7
		public OrderAttribute(int order)
		{
			this.Order = order;
		}

		// Token: 0x060008ED RID: 2285 RVA: 0x0001E4DC File Offset: 0x0001C6DC
		public void ApplyToTest(Test test)
		{
			if (!test.Properties.ContainsKey("Order"))
			{
				test.Properties.Set("Order", this.Order);
			}
		}

		// Token: 0x040001EC RID: 492
		public readonly int Order;
	}
}
