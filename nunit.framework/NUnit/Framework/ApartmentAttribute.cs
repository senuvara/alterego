﻿using System;
using System.Threading;

namespace NUnit.Framework
{
	// Token: 0x02000114 RID: 276
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class ApartmentAttribute : PropertyAttribute
	{
		// Token: 0x06000778 RID: 1912 RVA: 0x0001B037 File Offset: 0x00019237
		public ApartmentAttribute(ApartmentState apartmentState)
		{
			Guard.ArgumentValid(apartmentState != ApartmentState.Unknown, "must be STA or MTA", "apartmentState");
			base.Properties.Add("ApartmentState", apartmentState);
		}
	}
}
