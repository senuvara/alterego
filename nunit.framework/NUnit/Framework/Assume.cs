﻿using System;
using System.ComponentModel;
using NUnit.Framework.Constraints;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x02000121 RID: 289
	public class Assume
	{
		// Token: 0x060007BB RID: 1979 RVA: 0x0001B7CD File Offset: 0x000199CD
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new static bool Equals(object a, object b)
		{
			throw new InvalidOperationException("Assume.Equals should not be used for Assertions");
		}

		// Token: 0x060007BC RID: 1980 RVA: 0x0001B7DA File Offset: 0x000199DA
		public new static void ReferenceEquals(object a, object b)
		{
			throw new InvalidOperationException("Assume.ReferenceEquals should not be used for Assertions");
		}

		// Token: 0x060007BD RID: 1981 RVA: 0x0001B7E7 File Offset: 0x000199E7
		public static void That<TActual>(ActualValueDelegate<TActual> del, IResolveConstraint expr)
		{
			Assume.That<TActual>(del, expr.Resolve(), null, null);
		}

		// Token: 0x060007BE RID: 1982 RVA: 0x0001B7FC File Offset: 0x000199FC
		public static void That<TActual>(ActualValueDelegate<TActual> del, IResolveConstraint expr, string message, params object[] args)
		{
			IConstraint constraint = expr.Resolve();
			ConstraintResult constraintResult = constraint.ApplyTo<TActual>(del);
			if (!constraintResult.IsSuccess)
			{
				MessageWriter messageWriter = new TextMessageWriter(message, args);
				constraintResult.WriteMessageTo(messageWriter);
				throw new InconclusiveException(messageWriter.ToString());
			}
		}

		// Token: 0x060007BF RID: 1983 RVA: 0x0001B840 File Offset: 0x00019A40
		public static void That<TActual>(ActualValueDelegate<TActual> del, IResolveConstraint expr, Func<string> getExceptionMessage)
		{
			IConstraint constraint = expr.Resolve();
			ConstraintResult constraintResult = constraint.ApplyTo<TActual>(del);
			if (!constraintResult.IsSuccess)
			{
				throw new InconclusiveException(getExceptionMessage());
			}
		}

		// Token: 0x060007C0 RID: 1984 RVA: 0x0001B874 File Offset: 0x00019A74
		public static void That(bool condition, string message, params object[] args)
		{
			Assume.That<bool>(condition, Is.True, message, args);
		}

		// Token: 0x060007C1 RID: 1985 RVA: 0x0001B885 File Offset: 0x00019A85
		public static void That(bool condition)
		{
			Assume.That<bool>(condition, Is.True, null, null);
		}

		// Token: 0x060007C2 RID: 1986 RVA: 0x0001B896 File Offset: 0x00019A96
		public static void That(bool condition, Func<string> getExceptionMessage)
		{
			Assume.That<bool>(condition, Is.True, getExceptionMessage);
		}

		// Token: 0x060007C3 RID: 1987 RVA: 0x0001B8A6 File Offset: 0x00019AA6
		public static void That(Func<bool> condition, string message, params object[] args)
		{
			Assume.That<bool>(condition(), Is.True, message, args);
		}

		// Token: 0x060007C4 RID: 1988 RVA: 0x0001B8BC File Offset: 0x00019ABC
		public static void That(Func<bool> condition)
		{
			Assume.That<bool>(condition(), Is.True, null, null);
		}

		// Token: 0x060007C5 RID: 1989 RVA: 0x0001B8D2 File Offset: 0x00019AD2
		public static void That(Func<bool> condition, Func<string> getExceptionMessage)
		{
			Assume.That<bool>(condition(), Is.True, getExceptionMessage);
		}

		// Token: 0x060007C6 RID: 1990 RVA: 0x0001B8E7 File Offset: 0x00019AE7
		public static void That(TestDelegate code, IResolveConstraint constraint)
		{
			Assume.That<object>(code, constraint);
		}

		// Token: 0x060007C7 RID: 1991 RVA: 0x0001B8F2 File Offset: 0x00019AF2
		public static void That<TActual>(TActual actual, IResolveConstraint expression)
		{
			Assume.That<TActual>(actual, expression, null, null);
		}

		// Token: 0x060007C8 RID: 1992 RVA: 0x0001B900 File Offset: 0x00019B00
		public static void That<TActual>(TActual actual, IResolveConstraint expression, string message, params object[] args)
		{
			IConstraint constraint = expression.Resolve();
			ConstraintResult constraintResult = constraint.ApplyTo(actual);
			if (!constraintResult.IsSuccess)
			{
				MessageWriter messageWriter = new TextMessageWriter(message, args);
				constraintResult.WriteMessageTo(messageWriter);
				throw new InconclusiveException(messageWriter.ToString());
			}
		}

		// Token: 0x060007C9 RID: 1993 RVA: 0x0001B94C File Offset: 0x00019B4C
		public static void That<TActual>(TActual actual, IResolveConstraint expression, Func<string> getExceptionMessage)
		{
			IConstraint constraint = expression.Resolve();
			ConstraintResult constraintResult = constraint.ApplyTo(actual);
			if (!constraintResult.IsSuccess)
			{
				throw new InconclusiveException(getExceptionMessage());
			}
		}

		// Token: 0x060007CA RID: 1994 RVA: 0x0001B985 File Offset: 0x00019B85
		public Assume()
		{
		}
	}
}
