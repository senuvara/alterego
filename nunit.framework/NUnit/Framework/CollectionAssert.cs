﻿using System;
using System.Collections;
using System.ComponentModel;
using NUnit.Framework.Constraints;

namespace NUnit.Framework
{
	// Token: 0x0200016B RID: 363
	public class CollectionAssert
	{
		// Token: 0x06000978 RID: 2424 RVA: 0x0001FECC File Offset: 0x0001E0CC
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new static bool Equals(object a, object b)
		{
			throw new InvalidOperationException("CollectionAssert.Equals should not be used for Assertions");
		}

		// Token: 0x06000979 RID: 2425 RVA: 0x0001FED9 File Offset: 0x0001E0D9
		public new static void ReferenceEquals(object a, object b)
		{
			throw new InvalidOperationException("CollectionAssert.ReferenceEquals should not be used for Assertions");
		}

		// Token: 0x0600097A RID: 2426 RVA: 0x0001FEE6 File Offset: 0x0001E0E6
		public static void AllItemsAreInstancesOfType(IEnumerable collection, Type expectedType)
		{
			CollectionAssert.AllItemsAreInstancesOfType(collection, expectedType, string.Empty, null);
		}

		// Token: 0x0600097B RID: 2427 RVA: 0x0001FEF7 File Offset: 0x0001E0F7
		public static void AllItemsAreInstancesOfType(IEnumerable collection, Type expectedType, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, Is.All.InstanceOf(expectedType), message, args);
		}

		// Token: 0x0600097C RID: 2428 RVA: 0x0001FF0E File Offset: 0x0001E10E
		public static void AllItemsAreNotNull(IEnumerable collection)
		{
			CollectionAssert.AllItemsAreNotNull(collection, string.Empty, null);
		}

		// Token: 0x0600097D RID: 2429 RVA: 0x0001FF1E File Offset: 0x0001E11E
		public static void AllItemsAreNotNull(IEnumerable collection, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, Is.All.Not.Null, message, args);
		}

		// Token: 0x0600097E RID: 2430 RVA: 0x0001FF39 File Offset: 0x0001E139
		public static void AllItemsAreUnique(IEnumerable collection)
		{
			CollectionAssert.AllItemsAreUnique(collection, string.Empty, null);
		}

		// Token: 0x0600097F RID: 2431 RVA: 0x0001FF49 File Offset: 0x0001E149
		public static void AllItemsAreUnique(IEnumerable collection, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, Is.Unique, message, args);
		}

		// Token: 0x06000980 RID: 2432 RVA: 0x0001FF5A File Offset: 0x0001E15A
		public static void AreEqual(IEnumerable expected, IEnumerable actual)
		{
			CollectionAssert.AreEqual(expected, actual, string.Empty, null);
		}

		// Token: 0x06000981 RID: 2433 RVA: 0x0001FF6B File Offset: 0x0001E16B
		public static void AreEqual(IEnumerable expected, IEnumerable actual, IComparer comparer)
		{
			CollectionAssert.AreEqual(expected, actual, comparer, string.Empty, null);
		}

		// Token: 0x06000982 RID: 2434 RVA: 0x0001FF7D File Offset: 0x0001E17D
		public static void AreEqual(IEnumerable expected, IEnumerable actual, string message, params object[] args)
		{
			Assert.That<IEnumerable>(actual, Is.EqualTo(expected), message, args);
		}

		// Token: 0x06000983 RID: 2435 RVA: 0x0001FF8F File Offset: 0x0001E18F
		public static void AreEqual(IEnumerable expected, IEnumerable actual, IComparer comparer, string message, params object[] args)
		{
			Assert.That<IEnumerable>(actual, Is.EqualTo(expected).Using(comparer), message, args);
		}

		// Token: 0x06000984 RID: 2436 RVA: 0x0001FFA8 File Offset: 0x0001E1A8
		public static void AreEquivalent(IEnumerable expected, IEnumerable actual)
		{
			CollectionAssert.AreEquivalent(expected, actual, string.Empty, null);
		}

		// Token: 0x06000985 RID: 2437 RVA: 0x0001FFB9 File Offset: 0x0001E1B9
		public static void AreEquivalent(IEnumerable expected, IEnumerable actual, string message, params object[] args)
		{
			Assert.That<IEnumerable>(actual, Is.EquivalentTo(expected), message, args);
		}

		// Token: 0x06000986 RID: 2438 RVA: 0x0001FFCB File Offset: 0x0001E1CB
		public static void AreNotEqual(IEnumerable expected, IEnumerable actual)
		{
			CollectionAssert.AreNotEqual(expected, actual, string.Empty, null);
		}

		// Token: 0x06000987 RID: 2439 RVA: 0x0001FFDC File Offset: 0x0001E1DC
		public static void AreNotEqual(IEnumerable expected, IEnumerable actual, IComparer comparer)
		{
			CollectionAssert.AreNotEqual(expected, actual, comparer, string.Empty, null);
		}

		// Token: 0x06000988 RID: 2440 RVA: 0x0001FFEE File Offset: 0x0001E1EE
		public static void AreNotEqual(IEnumerable expected, IEnumerable actual, string message, params object[] args)
		{
			Assert.That<IEnumerable>(actual, Is.Not.EqualTo(expected), message, args);
		}

		// Token: 0x06000989 RID: 2441 RVA: 0x00020005 File Offset: 0x0001E205
		public static void AreNotEqual(IEnumerable expected, IEnumerable actual, IComparer comparer, string message, params object[] args)
		{
			Assert.That<IEnumerable>(actual, Is.Not.EqualTo(expected).Using(comparer), message, args);
		}

		// Token: 0x0600098A RID: 2442 RVA: 0x00020023 File Offset: 0x0001E223
		public static void AreNotEquivalent(IEnumerable expected, IEnumerable actual)
		{
			CollectionAssert.AreNotEquivalent(expected, actual, string.Empty, null);
		}

		// Token: 0x0600098B RID: 2443 RVA: 0x00020034 File Offset: 0x0001E234
		public static void AreNotEquivalent(IEnumerable expected, IEnumerable actual, string message, params object[] args)
		{
			Assert.That<IEnumerable>(actual, Is.Not.EquivalentTo(expected), message, args);
		}

		// Token: 0x0600098C RID: 2444 RVA: 0x0002004B File Offset: 0x0001E24B
		public static void Contains(IEnumerable collection, object actual)
		{
			CollectionAssert.Contains(collection, actual, string.Empty, null);
		}

		// Token: 0x0600098D RID: 2445 RVA: 0x0002005C File Offset: 0x0001E25C
		public static void Contains(IEnumerable collection, object actual, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, Has.Member(actual), message, args);
		}

		// Token: 0x0600098E RID: 2446 RVA: 0x0002006E File Offset: 0x0001E26E
		public static void DoesNotContain(IEnumerable collection, object actual)
		{
			CollectionAssert.DoesNotContain(collection, actual, string.Empty, null);
		}

		// Token: 0x0600098F RID: 2447 RVA: 0x0002007F File Offset: 0x0001E27F
		public static void DoesNotContain(IEnumerable collection, object actual, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, Has.No.Member(actual), message, args);
		}

		// Token: 0x06000990 RID: 2448 RVA: 0x00020096 File Offset: 0x0001E296
		public static void IsNotSubsetOf(IEnumerable subset, IEnumerable superset)
		{
			CollectionAssert.IsNotSubsetOf(subset, superset, string.Empty, null);
		}

		// Token: 0x06000991 RID: 2449 RVA: 0x000200A7 File Offset: 0x0001E2A7
		public static void IsNotSubsetOf(IEnumerable subset, IEnumerable superset, string message, params object[] args)
		{
			Assert.That<IEnumerable>(subset, Is.Not.SubsetOf(superset), message, args);
		}

		// Token: 0x06000992 RID: 2450 RVA: 0x000200BE File Offset: 0x0001E2BE
		public static void IsSubsetOf(IEnumerable subset, IEnumerable superset)
		{
			CollectionAssert.IsSubsetOf(subset, superset, string.Empty, null);
		}

		// Token: 0x06000993 RID: 2451 RVA: 0x000200CF File Offset: 0x0001E2CF
		public static void IsSubsetOf(IEnumerable subset, IEnumerable superset, string message, params object[] args)
		{
			Assert.That<IEnumerable>(subset, Is.SubsetOf(superset), message, args);
		}

		// Token: 0x06000994 RID: 2452 RVA: 0x000200E1 File Offset: 0x0001E2E1
		public static void IsNotSupersetOf(IEnumerable superset, IEnumerable subset)
		{
			CollectionAssert.IsNotSupersetOf(superset, subset, string.Empty, null);
		}

		// Token: 0x06000995 RID: 2453 RVA: 0x000200F2 File Offset: 0x0001E2F2
		public static void IsNotSupersetOf(IEnumerable superset, IEnumerable subset, string message, params object[] args)
		{
			Assert.That<IEnumerable>(superset, Is.Not.SupersetOf(subset), message, args);
		}

		// Token: 0x06000996 RID: 2454 RVA: 0x00020109 File Offset: 0x0001E309
		public static void IsSupersetOf(IEnumerable superset, IEnumerable subset)
		{
			CollectionAssert.IsSupersetOf(superset, subset, string.Empty, null);
		}

		// Token: 0x06000997 RID: 2455 RVA: 0x0002011A File Offset: 0x0001E31A
		public static void IsSupersetOf(IEnumerable superset, IEnumerable subset, string message, params object[] args)
		{
			Assert.That<IEnumerable>(superset, Is.SupersetOf(subset), message, args);
		}

		// Token: 0x06000998 RID: 2456 RVA: 0x0002012C File Offset: 0x0001E32C
		public static void IsEmpty(IEnumerable collection, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, new EmptyCollectionConstraint(), message, args);
		}

		// Token: 0x06000999 RID: 2457 RVA: 0x0002013D File Offset: 0x0001E33D
		public static void IsEmpty(IEnumerable collection)
		{
			CollectionAssert.IsEmpty(collection, string.Empty, null);
		}

		// Token: 0x0600099A RID: 2458 RVA: 0x0002014D File Offset: 0x0001E34D
		public static void IsNotEmpty(IEnumerable collection, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, new NotConstraint(new EmptyCollectionConstraint()), message, args);
		}

		// Token: 0x0600099B RID: 2459 RVA: 0x00020163 File Offset: 0x0001E363
		public static void IsNotEmpty(IEnumerable collection)
		{
			CollectionAssert.IsNotEmpty(collection, string.Empty, null);
		}

		// Token: 0x0600099C RID: 2460 RVA: 0x00020173 File Offset: 0x0001E373
		public static void IsOrdered(IEnumerable collection, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, Is.Ordered, message, args);
		}

		// Token: 0x0600099D RID: 2461 RVA: 0x00020184 File Offset: 0x0001E384
		public static void IsOrdered(IEnumerable collection)
		{
			CollectionAssert.IsOrdered(collection, string.Empty, null);
		}

		// Token: 0x0600099E RID: 2462 RVA: 0x00020194 File Offset: 0x0001E394
		public static void IsOrdered(IEnumerable collection, IComparer comparer, string message, params object[] args)
		{
			Assert.That<IEnumerable>(collection, Is.Ordered.Using(comparer), message, args);
		}

		// Token: 0x0600099F RID: 2463 RVA: 0x000201AB File Offset: 0x0001E3AB
		public static void IsOrdered(IEnumerable collection, IComparer comparer)
		{
			CollectionAssert.IsOrdered(collection, comparer, string.Empty, null);
		}

		// Token: 0x060009A0 RID: 2464 RVA: 0x000201BC File Offset: 0x0001E3BC
		public CollectionAssert()
		{
		}
	}
}
