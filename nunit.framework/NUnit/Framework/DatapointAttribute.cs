﻿using System;

namespace NUnit.Framework
{
	// Token: 0x0200015B RID: 347
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class DatapointAttribute : NUnitAttribute
	{
		// Token: 0x06000906 RID: 2310 RVA: 0x0001E9CE File Offset: 0x0001CBCE
		public DatapointAttribute()
		{
		}
	}
}
