﻿using System;

namespace NUnit.Framework
{
	// Token: 0x0200007D RID: 125
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class OneTimeTearDownAttribute : NUnitAttribute
	{
		// Token: 0x06000465 RID: 1125 RVA: 0x0000EA18 File Offset: 0x0000CC18
		public OneTimeTearDownAttribute()
		{
		}
	}
}
