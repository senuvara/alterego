﻿using System;

namespace NUnit.Framework
{
	// Token: 0x02000099 RID: 153
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class SetUpAttribute : NUnitAttribute
	{
		// Token: 0x060004CE RID: 1230 RVA: 0x0000FEDE File Offset: 0x0000E0DE
		public SetUpAttribute()
		{
		}
	}
}
