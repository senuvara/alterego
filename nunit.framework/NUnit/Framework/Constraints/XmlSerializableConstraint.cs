﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200012B RID: 299
	public class XmlSerializableConstraint : Constraint
	{
		// Token: 0x170001BC RID: 444
		// (get) Token: 0x060007F4 RID: 2036 RVA: 0x0001C26C File Offset: 0x0001A46C
		public override string Description
		{
			get
			{
				return "xml serializable";
			}
		}

		// Token: 0x060007F5 RID: 2037 RVA: 0x0001C284 File Offset: 0x0001A484
		public override ConstraintResult ApplyTo(object actual)
		{
			if (actual == null)
			{
				throw new ArgumentNullException("actual");
			}
			MemoryStream memoryStream = new MemoryStream();
			bool isSuccess = false;
			try
			{
				this.serializer = new XmlSerializer(actual.GetType());
				this.serializer.Serialize(memoryStream, actual);
				memoryStream.Seek(0L, SeekOrigin.Begin);
				isSuccess = (this.serializer.Deserialize(memoryStream) != null);
			}
			catch (NotSupportedException)
			{
			}
			catch (InvalidOperationException)
			{
			}
			return new ConstraintResult(this, actual.GetType(), isSuccess);
		}

		// Token: 0x060007F6 RID: 2038 RVA: 0x0001C328 File Offset: 0x0001A528
		protected override string GetStringRepresentation()
		{
			return "<xmlserializable>";
		}

		// Token: 0x060007F7 RID: 2039 RVA: 0x0001C33F File Offset: 0x0001A53F
		public XmlSerializableConstraint() : base(new object[0])
		{
		}

		// Token: 0x040001BE RID: 446
		private XmlSerializer serializer;
	}
}
