﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000058 RID: 88
	public class FalseConstraint : Constraint
	{
		// Token: 0x06000298 RID: 664 RVA: 0x00009E8A File Offset: 0x0000808A
		public FalseConstraint() : base(new object[0])
		{
			this.Description = "False";
		}

		// Token: 0x06000299 RID: 665 RVA: 0x00009EA8 File Offset: 0x000080A8
		public override ConstraintResult ApplyTo(object actual)
		{
			return new ConstraintResult(this, actual, false.Equals(actual));
		}
	}
}
