﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000053 RID: 83
	public class ThrowsNothingConstraint : Constraint
	{
		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000278 RID: 632 RVA: 0x000091C8 File Offset: 0x000073C8
		public override string Description
		{
			get
			{
				return "No Exception to be thrown";
			}
		}

		// Token: 0x06000279 RID: 633 RVA: 0x000091E0 File Offset: 0x000073E0
		public override ConstraintResult ApplyTo(object actual)
		{
			this.caughtException = ThrowsConstraint.ExceptionInterceptor.Intercept(actual);
			return new ConstraintResult(this, this.caughtException, this.caughtException == null);
		}

		// Token: 0x0600027A RID: 634 RVA: 0x00009214 File Offset: 0x00007414
		public override ConstraintResult ApplyTo<TActual>(ActualValueDelegate<TActual> del)
		{
			return this.ApplyTo(new ThrowsConstraint.GenericInvocationDescriptor<TActual>(del));
		}

		// Token: 0x0600027B RID: 635 RVA: 0x00009232 File Offset: 0x00007432
		public ThrowsNothingConstraint() : base(new object[0])
		{
		}

		// Token: 0x04000088 RID: 136
		private Exception caughtException;
	}
}
