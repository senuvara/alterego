﻿using System;
using System.Collections.Generic;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000106 RID: 262
	public class ConstraintBuilder : IResolveConstraint
	{
		// Token: 0x06000742 RID: 1858 RVA: 0x0001A1EE File Offset: 0x000183EE
		public ConstraintBuilder()
		{
			this.ops = new ConstraintBuilder.OperatorStack(this);
			this.constraints = new ConstraintBuilder.ConstraintStack(this);
		}

		// Token: 0x06000743 RID: 1859 RVA: 0x0001A214 File Offset: 0x00018414
		public void Append(ConstraintOperator op)
		{
			op.LeftContext = this.lastPushed;
			if (this.lastPushed is ConstraintOperator)
			{
				this.SetTopOperatorRightContext(op);
			}
			this.ReduceOperatorStack(op.LeftPrecedence);
			this.ops.Push(op);
			this.lastPushed = op;
		}

		// Token: 0x06000744 RID: 1860 RVA: 0x0001A270 File Offset: 0x00018470
		public void Append(Constraint constraint)
		{
			if (this.lastPushed is ConstraintOperator)
			{
				this.SetTopOperatorRightContext(constraint);
			}
			this.constraints.Push(constraint);
			this.lastPushed = constraint;
			constraint.Builder = this;
		}

		// Token: 0x06000745 RID: 1861 RVA: 0x0001A2B8 File Offset: 0x000184B8
		private void SetTopOperatorRightContext(object rightContext)
		{
			int leftPrecedence = this.ops.Top.LeftPrecedence;
			this.ops.Top.RightContext = rightContext;
			if (this.ops.Top.LeftPrecedence > leftPrecedence)
			{
				ConstraintOperator constraintOperator = this.ops.Pop();
				this.ReduceOperatorStack(constraintOperator.LeftPrecedence);
				this.ops.Push(constraintOperator);
			}
		}

		// Token: 0x06000746 RID: 1862 RVA: 0x0001A32C File Offset: 0x0001852C
		private void ReduceOperatorStack(int targetPrecedence)
		{
			while (!this.ops.Empty && this.ops.Top.RightPrecedence < targetPrecedence)
			{
				this.ops.Pop().Reduce(this.constraints);
			}
		}

		// Token: 0x06000747 RID: 1863 RVA: 0x0001A37C File Offset: 0x0001857C
		public IConstraint Resolve()
		{
			if (!this.IsResolvable)
			{
				throw new InvalidOperationException("A partial expression may not be resolved");
			}
			while (!this.ops.Empty)
			{
				ConstraintOperator constraintOperator = this.ops.Pop();
				constraintOperator.Reduce(this.constraints);
			}
			return this.constraints.Pop();
		}

		// Token: 0x1700019B RID: 411
		// (get) Token: 0x06000748 RID: 1864 RVA: 0x0001A3E0 File Offset: 0x000185E0
		private bool IsResolvable
		{
			get
			{
				return this.lastPushed is Constraint || this.lastPushed is SelfResolvingOperator;
			}
		}

		// Token: 0x0400019D RID: 413
		private readonly ConstraintBuilder.OperatorStack ops;

		// Token: 0x0400019E RID: 414
		private readonly ConstraintBuilder.ConstraintStack constraints;

		// Token: 0x0400019F RID: 415
		private object lastPushed;

		// Token: 0x02000107 RID: 263
		public class OperatorStack
		{
			// Token: 0x06000749 RID: 1865 RVA: 0x0001A410 File Offset: 0x00018610
			public OperatorStack(ConstraintBuilder builder)
			{
			}

			// Token: 0x1700019C RID: 412
			// (get) Token: 0x0600074A RID: 1866 RVA: 0x0001A428 File Offset: 0x00018628
			public bool Empty
			{
				get
				{
					return this.stack.Count == 0;
				}
			}

			// Token: 0x1700019D RID: 413
			// (get) Token: 0x0600074B RID: 1867 RVA: 0x0001A448 File Offset: 0x00018648
			public ConstraintOperator Top
			{
				get
				{
					return this.stack.Peek();
				}
			}

			// Token: 0x0600074C RID: 1868 RVA: 0x0001A465 File Offset: 0x00018665
			public void Push(ConstraintOperator op)
			{
				this.stack.Push(op);
			}

			// Token: 0x0600074D RID: 1869 RVA: 0x0001A478 File Offset: 0x00018678
			public ConstraintOperator Pop()
			{
				return this.stack.Pop();
			}

			// Token: 0x040001A0 RID: 416
			private readonly Stack<ConstraintOperator> stack = new Stack<ConstraintOperator>();
		}

		// Token: 0x02000108 RID: 264
		public class ConstraintStack
		{
			// Token: 0x0600074E RID: 1870 RVA: 0x0001A495 File Offset: 0x00018695
			public ConstraintStack(ConstraintBuilder builder)
			{
				this.builder = builder;
			}

			// Token: 0x1700019E RID: 414
			// (get) Token: 0x0600074F RID: 1871 RVA: 0x0001A4B4 File Offset: 0x000186B4
			public bool Empty
			{
				get
				{
					return this.stack.Count == 0;
				}
			}

			// Token: 0x06000750 RID: 1872 RVA: 0x0001A4D4 File Offset: 0x000186D4
			public void Push(IConstraint constraint)
			{
				this.stack.Push(constraint);
				constraint.Builder = this.builder;
			}

			// Token: 0x06000751 RID: 1873 RVA: 0x0001A4F4 File Offset: 0x000186F4
			public IConstraint Pop()
			{
				IConstraint constraint = this.stack.Pop();
				constraint.Builder = null;
				return constraint;
			}

			// Token: 0x040001A1 RID: 417
			private readonly Stack<IConstraint> stack = new Stack<IConstraint>();

			// Token: 0x040001A2 RID: 418
			private readonly ConstraintBuilder builder;
		}
	}
}
