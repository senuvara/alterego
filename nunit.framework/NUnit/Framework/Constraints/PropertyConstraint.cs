﻿using System;
using System.Reflection;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000156 RID: 342
	public class PropertyConstraint : PrefixConstraint
	{
		// Token: 0x060008FC RID: 2300 RVA: 0x0001E6F6 File Offset: 0x0001C8F6
		public PropertyConstraint(string name, IConstraint baseConstraint) : base(baseConstraint)
		{
			this.name = name;
			base.DescriptionPrefix = "property " + name;
		}

		// Token: 0x060008FD RID: 2301 RVA: 0x0001E71C File Offset: 0x0001C91C
		public override ConstraintResult ApplyTo(object actual)
		{
			Guard.ArgumentNotNull(actual, "actual");
			Type type = actual as Type;
			if (type == null)
			{
				type = actual.GetType();
			}
			PropertyInfo property = type.GetProperty(this.name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			if (property == null)
			{
				throw new ArgumentException(string.Format("Property {0} was not found", this.name), "name");
			}
			this.propValue = property.GetValue(actual, null);
			return new ConstraintResult(this, this.propValue, base.BaseConstraint.ApplyTo(this.propValue).IsSuccess);
		}

		// Token: 0x060008FE RID: 2302 RVA: 0x0001E7B8 File Offset: 0x0001C9B8
		protected override string GetStringRepresentation()
		{
			return string.Format("<property {0} {1}>", this.name, base.BaseConstraint);
		}

		// Token: 0x040001FF RID: 511
		private readonly string name;

		// Token: 0x04000200 RID: 512
		private object propValue;
	}
}
