﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200000F RID: 15
	public abstract class TypeConstraint : Constraint
	{
		// Token: 0x0600006F RID: 111 RVA: 0x00003080 File Offset: 0x00001280
		protected TypeConstraint(Type type, string descriptionPrefix) : base(new object[]
		{
			type
		})
		{
			this.expectedType = type;
			this.Description = descriptionPrefix + MsgUtils.FormatValue(this.expectedType);
		}

		// Token: 0x06000070 RID: 112 RVA: 0x000030C4 File Offset: 0x000012C4
		public override ConstraintResult ApplyTo(object actual)
		{
			this.actualType = ((actual == null) ? null : actual.GetType());
			return new ConstraintResult(this, this.actualType, this.Matches(actual));
		}

		// Token: 0x06000071 RID: 113
		protected abstract bool Matches(object actual);

		// Token: 0x0400000D RID: 13
		protected Type expectedType;

		// Token: 0x0400000E RID: 14
		protected Type actualType;
	}
}
