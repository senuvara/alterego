﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000154 RID: 340
	public class ReusableConstraint : IResolveConstraint
	{
		// Token: 0x060008F4 RID: 2292 RVA: 0x0001E5AA File Offset: 0x0001C7AA
		public ReusableConstraint(IResolveConstraint c)
		{
			this.constraint = c.Resolve();
		}

		// Token: 0x060008F5 RID: 2293 RVA: 0x0001E5C4 File Offset: 0x0001C7C4
		public static implicit operator ReusableConstraint(Constraint c)
		{
			return new ReusableConstraint(c);
		}

		// Token: 0x060008F6 RID: 2294 RVA: 0x0001E5DC File Offset: 0x0001C7DC
		public override string ToString()
		{
			return this.constraint.ToString();
		}

		// Token: 0x060008F7 RID: 2295 RVA: 0x0001E5FC File Offset: 0x0001C7FC
		public IConstraint Resolve()
		{
			return this.constraint;
		}

		// Token: 0x040001FC RID: 508
		private readonly IConstraint constraint;
	}
}
