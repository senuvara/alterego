﻿using System;
using System.Runtime.CompilerServices;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200007B RID: 123
	public class ConstraintResult
	{
		// Token: 0x06000457 RID: 1111 RVA: 0x0000E897 File Offset: 0x0000CA97
		public ConstraintResult(IConstraint constraint, object actualValue)
		{
			this._constraint = constraint;
			this.ActualValue = actualValue;
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x0000E8B1 File Offset: 0x0000CAB1
		public ConstraintResult(IConstraint constraint, object actualValue, ConstraintStatus status) : this(constraint, actualValue)
		{
			this.Status = status;
		}

		// Token: 0x06000459 RID: 1113 RVA: 0x0000E8C6 File Offset: 0x0000CAC6
		public ConstraintResult(IConstraint constraint, object actualValue, bool isSuccess) : this(constraint, actualValue)
		{
			this.Status = (isSuccess ? ConstraintStatus.Success : ConstraintStatus.Failure);
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x0600045A RID: 1114 RVA: 0x0000E8E4 File Offset: 0x0000CAE4
		// (set) Token: 0x0600045B RID: 1115 RVA: 0x0000E8FB File Offset: 0x0000CAFB
		public object ActualValue
		{
			[CompilerGenerated]
			get
			{
				return this.<ActualValue>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ActualValue>k__BackingField = value;
			}
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x0600045C RID: 1116 RVA: 0x0000E904 File Offset: 0x0000CB04
		// (set) Token: 0x0600045D RID: 1117 RVA: 0x0000E91B File Offset: 0x0000CB1B
		public ConstraintStatus Status
		{
			[CompilerGenerated]
			get
			{
				return this.<Status>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Status>k__BackingField = value;
			}
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x0600045E RID: 1118 RVA: 0x0000E924 File Offset: 0x0000CB24
		public virtual bool IsSuccess
		{
			get
			{
				return this.Status == ConstraintStatus.Success;
			}
		}

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x0600045F RID: 1119 RVA: 0x0000E940 File Offset: 0x0000CB40
		public string Name
		{
			get
			{
				return this._constraint.DisplayName;
			}
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x06000460 RID: 1120 RVA: 0x0000E960 File Offset: 0x0000CB60
		public string Description
		{
			get
			{
				return this._constraint.Description;
			}
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x0000E97D File Offset: 0x0000CB7D
		public virtual void WriteMessageTo(MessageWriter writer)
		{
			writer.DisplayDifferences(this);
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x0000E988 File Offset: 0x0000CB88
		public virtual void WriteActualValueTo(MessageWriter writer)
		{
			writer.WriteActualValue(this.ActualValue);
		}

		// Token: 0x040000C4 RID: 196
		private IConstraint _constraint;

		// Token: 0x040000C5 RID: 197
		[CompilerGenerated]
		private object <ActualValue>k__BackingField;

		// Token: 0x040000C6 RID: 198
		[CompilerGenerated]
		private ConstraintStatus <Status>k__BackingField;
	}
}
