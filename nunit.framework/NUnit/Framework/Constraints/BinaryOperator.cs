﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000055 RID: 85
	public abstract class BinaryOperator : ConstraintOperator
	{
		// Token: 0x06000284 RID: 644 RVA: 0x000092C0 File Offset: 0x000074C0
		public override void Reduce(ConstraintBuilder.ConstraintStack stack)
		{
			IConstraint right = stack.Pop();
			IConstraint left = stack.Pop();
			stack.Push(this.ApplyOperator(left, right));
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x06000285 RID: 645 RVA: 0x000092EC File Offset: 0x000074EC
		public override int LeftPrecedence
		{
			get
			{
				return (base.RightContext is CollectionOperator) ? (base.LeftPrecedence + 10) : base.LeftPrecedence;
			}
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000286 RID: 646 RVA: 0x0000931C File Offset: 0x0000751C
		public override int RightPrecedence
		{
			get
			{
				return (base.RightContext is CollectionOperator) ? (base.RightPrecedence + 10) : base.RightPrecedence;
			}
		}

		// Token: 0x06000287 RID: 647
		public abstract IConstraint ApplyOperator(IConstraint left, IConstraint right);

		// Token: 0x06000288 RID: 648 RVA: 0x0000934C File Offset: 0x0000754C
		protected BinaryOperator()
		{
		}
	}
}
