﻿using System;
using System.Reflection;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000155 RID: 341
	public class PropertyExistsConstraint : Constraint
	{
		// Token: 0x060008F8 RID: 2296 RVA: 0x0001E614 File Offset: 0x0001C814
		public PropertyExistsConstraint(string name) : base(new object[]
		{
			name
		})
		{
			this.name = name;
		}

		// Token: 0x17000202 RID: 514
		// (get) Token: 0x060008F9 RID: 2297 RVA: 0x0001E640 File Offset: 0x0001C840
		public override string Description
		{
			get
			{
				return "property " + this.name;
			}
		}

		// Token: 0x060008FA RID: 2298 RVA: 0x0001E664 File Offset: 0x0001C864
		public override ConstraintResult ApplyTo(object actual)
		{
			Guard.ArgumentNotNull(actual, "actual");
			this.actualType = (actual as Type);
			if (this.actualType == null)
			{
				this.actualType = actual.GetType();
			}
			PropertyInfo property = this.actualType.GetProperty(this.name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			return new ConstraintResult(this, this.actualType, property != null);
		}

		// Token: 0x060008FB RID: 2299 RVA: 0x0001E6D4 File Offset: 0x0001C8D4
		protected override string GetStringRepresentation()
		{
			return string.Format("<propertyexists {0}>", this.name);
		}

		// Token: 0x040001FD RID: 509
		private readonly string name;

		// Token: 0x040001FE RID: 510
		private Type actualType;
	}
}
