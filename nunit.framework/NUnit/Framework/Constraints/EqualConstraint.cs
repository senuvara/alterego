﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using NUnit.Compatibility;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000BC RID: 188
	public class EqualConstraint : Constraint
	{
		// Token: 0x060005AB RID: 1451 RVA: 0x00013218 File Offset: 0x00011418
		public EqualConstraint(object expected) : base(new object[]
		{
			expected
		})
		{
			this.AdjustArgumentIfNeeded<object>(ref expected);
			this._expected = expected;
			this.ClipStrings = true;
		}

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x060005AC RID: 1452 RVA: 0x00013268 File Offset: 0x00011468
		public Tolerance Tolerance
		{
			get
			{
				return this._tolerance;
			}
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x060005AD RID: 1453 RVA: 0x00013280 File Offset: 0x00011480
		public bool CaseInsensitive
		{
			get
			{
				return this._comparer.IgnoreCase;
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x060005AE RID: 1454 RVA: 0x000132A0 File Offset: 0x000114A0
		// (set) Token: 0x060005AF RID: 1455 RVA: 0x000132B7 File Offset: 0x000114B7
		public bool ClipStrings
		{
			[CompilerGenerated]
			get
			{
				return this.<ClipStrings>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ClipStrings>k__BackingField = value;
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x060005B0 RID: 1456 RVA: 0x000132C0 File Offset: 0x000114C0
		public IList<NUnitEqualityComparer.FailurePoint> FailurePoints
		{
			get
			{
				return this._comparer.FailurePoints;
			}
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x060005B1 RID: 1457 RVA: 0x000132E0 File Offset: 0x000114E0
		public EqualConstraint IgnoreCase
		{
			get
			{
				this._comparer.IgnoreCase = true;
				return this;
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x060005B2 RID: 1458 RVA: 0x00013300 File Offset: 0x00011500
		public EqualConstraint NoClip
		{
			get
			{
				this.ClipStrings = false;
				return this;
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x060005B3 RID: 1459 RVA: 0x0001331C File Offset: 0x0001151C
		public EqualConstraint AsCollection
		{
			get
			{
				this._comparer.CompareAsCollection = true;
				return this;
			}
		}

		// Token: 0x060005B4 RID: 1460 RVA: 0x0001333C File Offset: 0x0001153C
		public EqualConstraint Within(object amount)
		{
			if (!this._tolerance.IsUnsetOrDefault)
			{
				throw new InvalidOperationException("Within modifier may appear only once in a constraint expression");
			}
			this._tolerance = new Tolerance(amount);
			return this;
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x060005B5 RID: 1461 RVA: 0x00013378 File Offset: 0x00011578
		public EqualConstraint WithSameOffset
		{
			get
			{
				this._comparer.WithSameOffset = true;
				return this;
			}
		}

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x060005B6 RID: 1462 RVA: 0x00013398 File Offset: 0x00011598
		public EqualConstraint Ulps
		{
			get
			{
				this._tolerance = this._tolerance.Ulps;
				return this;
			}
		}

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x060005B7 RID: 1463 RVA: 0x000133BC File Offset: 0x000115BC
		public EqualConstraint Percent
		{
			get
			{
				this._tolerance = this._tolerance.Percent;
				return this;
			}
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x060005B8 RID: 1464 RVA: 0x000133E0 File Offset: 0x000115E0
		public EqualConstraint Days
		{
			get
			{
				this._tolerance = this._tolerance.Days;
				return this;
			}
		}

		// Token: 0x17000141 RID: 321
		// (get) Token: 0x060005B9 RID: 1465 RVA: 0x00013404 File Offset: 0x00011604
		public EqualConstraint Hours
		{
			get
			{
				this._tolerance = this._tolerance.Hours;
				return this;
			}
		}

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x060005BA RID: 1466 RVA: 0x00013428 File Offset: 0x00011628
		public EqualConstraint Minutes
		{
			get
			{
				this._tolerance = this._tolerance.Minutes;
				return this;
			}
		}

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x060005BB RID: 1467 RVA: 0x0001344C File Offset: 0x0001164C
		public EqualConstraint Seconds
		{
			get
			{
				this._tolerance = this._tolerance.Seconds;
				return this;
			}
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x060005BC RID: 1468 RVA: 0x00013470 File Offset: 0x00011670
		public EqualConstraint Milliseconds
		{
			get
			{
				this._tolerance = this._tolerance.Milliseconds;
				return this;
			}
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x060005BD RID: 1469 RVA: 0x00013494 File Offset: 0x00011694
		public EqualConstraint Ticks
		{
			get
			{
				this._tolerance = this._tolerance.Ticks;
				return this;
			}
		}

		// Token: 0x060005BE RID: 1470 RVA: 0x000134B8 File Offset: 0x000116B8
		public EqualConstraint Using(IComparer comparer)
		{
			this._comparer.ExternalComparers.Add(EqualityAdapter.For(comparer));
			return this;
		}

		// Token: 0x060005BF RID: 1471 RVA: 0x000134E4 File Offset: 0x000116E4
		public EqualConstraint Using<T>(IComparer<T> comparer)
		{
			this._comparer.ExternalComparers.Add(EqualityAdapter.For<T>(comparer));
			return this;
		}

		// Token: 0x060005C0 RID: 1472 RVA: 0x00013510 File Offset: 0x00011710
		public EqualConstraint Using<T>(Comparison<T> comparer)
		{
			this._comparer.ExternalComparers.Add(EqualityAdapter.For<T>(comparer));
			return this;
		}

		// Token: 0x060005C1 RID: 1473 RVA: 0x0001353C File Offset: 0x0001173C
		public EqualConstraint Using(IEqualityComparer comparer)
		{
			this._comparer.ExternalComparers.Add(EqualityAdapter.For(comparer));
			return this;
		}

		// Token: 0x060005C2 RID: 1474 RVA: 0x00013568 File Offset: 0x00011768
		public EqualConstraint Using<T>(IEqualityComparer<T> comparer)
		{
			this._comparer.ExternalComparers.Add(EqualityAdapter.For<T>(comparer));
			return this;
		}

		// Token: 0x060005C3 RID: 1475 RVA: 0x00013594 File Offset: 0x00011794
		public override ConstraintResult ApplyTo(object actual)
		{
			this.AdjustArgumentIfNeeded<object>(ref actual);
			return new EqualConstraintResult(this, actual, this._comparer.AreEqual(this._expected, actual, ref this._tolerance));
		}

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x060005C4 RID: 1476 RVA: 0x000135D0 File Offset: 0x000117D0
		public override string Description
		{
			get
			{
				StringBuilder stringBuilder = new StringBuilder(MsgUtils.FormatValue(this._expected));
				if (this._tolerance != null && !this._tolerance.IsUnsetOrDefault)
				{
					stringBuilder.Append(" +/- ");
					stringBuilder.Append(MsgUtils.FormatValue(this._tolerance.Value));
					if (this._tolerance.Mode != ToleranceMode.Linear)
					{
						stringBuilder.Append(" ");
						stringBuilder.Append(this._tolerance.Mode.ToString());
					}
				}
				if (this._comparer.IgnoreCase)
				{
					stringBuilder.Append(", ignoring case");
				}
				return stringBuilder.ToString();
			}
		}

		// Token: 0x060005C5 RID: 1477 RVA: 0x00013694 File Offset: 0x00011894
		private void AdjustArgumentIfNeeded<T>(ref T arg)
		{
			if (arg != null)
			{
				Type type = arg.GetType();
				Type type2 = type.GetTypeInfo().IsGenericType ? type.GetGenericTypeDefinition() : null;
				if (type2 == typeof(ArraySegment<>) && type.GetProperty("Array").GetValue(arg, null) == null)
				{
					Type elementType = type.GetGenericArguments()[0];
					Array array = Array.CreateInstance(elementType, 0);
					ConstructorInfo constructor = type.GetConstructor(new Type[]
					{
						array.GetType()
					});
					arg = (T)((object)constructor.Invoke(new object[]
					{
						array
					}));
				}
			}
		}

		// Token: 0x0400012A RID: 298
		private readonly object _expected;

		// Token: 0x0400012B RID: 299
		private Tolerance _tolerance = Tolerance.Default;

		// Token: 0x0400012C RID: 300
		private NUnitEqualityComparer _comparer = new NUnitEqualityComparer();

		// Token: 0x0400012D RID: 301
		[CompilerGenerated]
		private bool <ClipStrings>k__BackingField;
	}
}
