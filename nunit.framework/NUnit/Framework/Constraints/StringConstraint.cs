﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200000C RID: 12
	public abstract class StringConstraint : Constraint
	{
		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000061 RID: 97 RVA: 0x00002E60 File Offset: 0x00001060
		public override string Description
		{
			get
			{
				string text = string.Format("{0} {1}", this.descriptionText, MsgUtils.FormatValue(this.expected));
				if (this.caseInsensitive)
				{
					text += ", ignoring case";
				}
				return text;
			}
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00002EA8 File Offset: 0x000010A8
		protected StringConstraint() : base(new object[0])
		{
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00002EBC File Offset: 0x000010BC
		protected StringConstraint(string expected) : base(new object[]
		{
			expected
		})
		{
			this.expected = expected;
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000064 RID: 100 RVA: 0x00002EE8 File Offset: 0x000010E8
		public StringConstraint IgnoreCase
		{
			get
			{
				this.caseInsensitive = true;
				return this;
			}
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00002F04 File Offset: 0x00001104
		public override ConstraintResult ApplyTo(object actual)
		{
			string text = actual as string;
			if (actual != null && text == null)
			{
				throw new ArgumentException("Actual value must be a string", "actual");
			}
			return new ConstraintResult(this, actual, this.Matches(text));
		}

		// Token: 0x06000066 RID: 102
		protected abstract bool Matches(string actual);

		// Token: 0x04000008 RID: 8
		protected string expected;

		// Token: 0x04000009 RID: 9
		protected bool caseInsensitive;

		// Token: 0x0400000A RID: 10
		protected string descriptionText;
	}
}
