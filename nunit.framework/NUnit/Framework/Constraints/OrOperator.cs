﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000056 RID: 86
	public class OrOperator : BinaryOperator
	{
		// Token: 0x06000289 RID: 649 RVA: 0x00009354 File Offset: 0x00007554
		public OrOperator()
		{
			this.left_precedence = (this.right_precedence = 3);
		}

		// Token: 0x0600028A RID: 650 RVA: 0x0000937C File Offset: 0x0000757C
		public override IConstraint ApplyOperator(IConstraint left, IConstraint right)
		{
			return new OrConstraint(left, right);
		}
	}
}
