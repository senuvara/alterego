﻿using System;
using NUnit.Compatibility;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200010A RID: 266
	public class AttributeExistsConstraint : Constraint
	{
		// Token: 0x06000757 RID: 1879 RVA: 0x0001A60C File Offset: 0x0001880C
		public AttributeExistsConstraint(Type type) : base(new object[]
		{
			type
		})
		{
			this.expectedType = type;
			if (!typeof(Attribute).GetTypeInfo().IsAssignableFrom(this.expectedType.GetTypeInfo()))
			{
				throw new ArgumentException(string.Format("Type {0} is not an attribute", this.expectedType), "type");
			}
		}

		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x06000758 RID: 1880 RVA: 0x0001A674 File Offset: 0x00018874
		public override string Description
		{
			get
			{
				return "type with attribute " + MsgUtils.FormatValue(this.expectedType);
			}
		}

		// Token: 0x06000759 RID: 1881 RVA: 0x0001A69C File Offset: 0x0001889C
		public override ConstraintResult ApplyTo(object actual)
		{
			Guard.ArgumentNotNull(actual, "actual");
			Attribute[] customAttributes = AttributeHelper.GetCustomAttributes(actual, this.expectedType, true);
			return new ConstraintResult(this, actual)
			{
				Status = ((customAttributes.Length > 0) ? ConstraintStatus.Success : ConstraintStatus.Failure)
			};
		}

		// Token: 0x040001A4 RID: 420
		private Type expectedType;
	}
}
