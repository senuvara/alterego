﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000139 RID: 313
	public class BinarySerializableConstraint : Constraint
	{
		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x0600085E RID: 2142 RVA: 0x0001D03C File Offset: 0x0001B23C
		public override string Description
		{
			get
			{
				return "binary serializable";
			}
		}

		// Token: 0x0600085F RID: 2143 RVA: 0x0001D054 File Offset: 0x0001B254
		public override ConstraintResult ApplyTo(object actual)
		{
			if (actual == null)
			{
				throw new ArgumentNullException("actual");
			}
			MemoryStream memoryStream = new MemoryStream();
			bool isSuccess = false;
			try
			{
				this.serializer.Serialize(memoryStream, actual);
				memoryStream.Seek(0L, SeekOrigin.Begin);
				isSuccess = (this.serializer.Deserialize(memoryStream) != null);
			}
			catch (SerializationException)
			{
			}
			return new ConstraintResult(this, actual.GetType(), isSuccess);
		}

		// Token: 0x06000860 RID: 2144 RVA: 0x0001D0D8 File Offset: 0x0001B2D8
		protected override string GetStringRepresentation()
		{
			return "<binaryserializable>";
		}

		// Token: 0x06000861 RID: 2145 RVA: 0x0001D0EF File Offset: 0x0001B2EF
		public BinarySerializableConstraint() : base(new object[0])
		{
		}

		// Token: 0x040001CB RID: 459
		private readonly BinaryFormatter serializer = new BinaryFormatter();
	}
}
