﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000137 RID: 311
	public class EmptyCollectionConstraint : CollectionConstraint
	{
		// Token: 0x170001BD RID: 445
		// (get) Token: 0x06000818 RID: 2072 RVA: 0x0001C944 File Offset: 0x0001AB44
		public override string Description
		{
			get
			{
				return "<empty>";
			}
		}

		// Token: 0x06000819 RID: 2073 RVA: 0x0001C95C File Offset: 0x0001AB5C
		protected override bool Matches(IEnumerable collection)
		{
			return CollectionConstraint.IsEmpty(collection);
		}

		// Token: 0x0600081A RID: 2074 RVA: 0x0001C974 File Offset: 0x0001AB74
		public EmptyCollectionConstraint()
		{
		}
	}
}
