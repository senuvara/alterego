﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000FF RID: 255
	public class OrConstraint : BinaryConstraint
	{
		// Token: 0x06000725 RID: 1829 RVA: 0x00019E5B File Offset: 0x0001805B
		public OrConstraint(IConstraint left, IConstraint right) : base(left, right)
		{
		}

		// Token: 0x17000197 RID: 407
		// (get) Token: 0x06000726 RID: 1830 RVA: 0x00019E68 File Offset: 0x00018068
		public override string Description
		{
			get
			{
				return this.Left.Description + " or " + this.Right.Description;
			}
		}

		// Token: 0x06000727 RID: 1831 RVA: 0x00019E9C File Offset: 0x0001809C
		public override ConstraintResult ApplyTo(object actual)
		{
			bool isSuccess = this.Left.ApplyTo(actual).IsSuccess || this.Right.ApplyTo(actual).IsSuccess;
			return new ConstraintResult(this, actual, isSuccess);
		}
	}
}
