﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000138 RID: 312
	public class ConstraintFactory
	{
		// Token: 0x170001BE RID: 446
		// (get) Token: 0x0600081B RID: 2075 RVA: 0x0001C97C File Offset: 0x0001AB7C
		public ConstraintExpression Not
		{
			get
			{
				return Is.Not;
			}
		}

		// Token: 0x170001BF RID: 447
		// (get) Token: 0x0600081C RID: 2076 RVA: 0x0001C994 File Offset: 0x0001AB94
		public ConstraintExpression No
		{
			get
			{
				return Has.No;
			}
		}

		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x0600081D RID: 2077 RVA: 0x0001C9AC File Offset: 0x0001ABAC
		public ConstraintExpression All
		{
			get
			{
				return Is.All;
			}
		}

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x0600081E RID: 2078 RVA: 0x0001C9C4 File Offset: 0x0001ABC4
		public ConstraintExpression Some
		{
			get
			{
				return Has.Some;
			}
		}

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x0600081F RID: 2079 RVA: 0x0001C9DC File Offset: 0x0001ABDC
		public ConstraintExpression None
		{
			get
			{
				return Has.None;
			}
		}

		// Token: 0x06000820 RID: 2080 RVA: 0x0001C9F4 File Offset: 0x0001ABF4
		public static ConstraintExpression Exactly(int expectedCount)
		{
			return Has.Exactly(expectedCount);
		}

		// Token: 0x06000821 RID: 2081 RVA: 0x0001CA0C File Offset: 0x0001AC0C
		public ResolvableConstraintExpression Property(string name)
		{
			return Has.Property(name);
		}

		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x06000822 RID: 2082 RVA: 0x0001CA24 File Offset: 0x0001AC24
		public ResolvableConstraintExpression Length
		{
			get
			{
				return Has.Length;
			}
		}

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06000823 RID: 2083 RVA: 0x0001CA3C File Offset: 0x0001AC3C
		public ResolvableConstraintExpression Count
		{
			get
			{
				return Has.Count;
			}
		}

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06000824 RID: 2084 RVA: 0x0001CA54 File Offset: 0x0001AC54
		public ResolvableConstraintExpression Message
		{
			get
			{
				return Has.Message;
			}
		}

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06000825 RID: 2085 RVA: 0x0001CA6C File Offset: 0x0001AC6C
		public ResolvableConstraintExpression InnerException
		{
			get
			{
				return Has.InnerException;
			}
		}

		// Token: 0x06000826 RID: 2086 RVA: 0x0001CA84 File Offset: 0x0001AC84
		public ResolvableConstraintExpression Attribute(Type expectedType)
		{
			return Has.Attribute(expectedType);
		}

		// Token: 0x06000827 RID: 2087 RVA: 0x0001CA9C File Offset: 0x0001AC9C
		public ResolvableConstraintExpression Attribute<TExpected>()
		{
			return this.Attribute(typeof(TExpected));
		}

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06000828 RID: 2088 RVA: 0x0001CAC0 File Offset: 0x0001ACC0
		public NullConstraint Null
		{
			get
			{
				return new NullConstraint();
			}
		}

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x06000829 RID: 2089 RVA: 0x0001CAD8 File Offset: 0x0001ACD8
		public TrueConstraint True
		{
			get
			{
				return new TrueConstraint();
			}
		}

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x0600082A RID: 2090 RVA: 0x0001CAF0 File Offset: 0x0001ACF0
		public FalseConstraint False
		{
			get
			{
				return new FalseConstraint();
			}
		}

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x0600082B RID: 2091 RVA: 0x0001CB08 File Offset: 0x0001AD08
		public GreaterThanConstraint Positive
		{
			get
			{
				return new GreaterThanConstraint(0);
			}
		}

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x0600082C RID: 2092 RVA: 0x0001CB28 File Offset: 0x0001AD28
		public LessThanConstraint Negative
		{
			get
			{
				return new LessThanConstraint(0);
			}
		}

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x0600082D RID: 2093 RVA: 0x0001CB48 File Offset: 0x0001AD48
		public EqualConstraint Zero
		{
			get
			{
				return new EqualConstraint(0);
			}
		}

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x0600082E RID: 2094 RVA: 0x0001CB68 File Offset: 0x0001AD68
		public NaNConstraint NaN
		{
			get
			{
				return new NaNConstraint();
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x0600082F RID: 2095 RVA: 0x0001CB80 File Offset: 0x0001AD80
		public EmptyConstraint Empty
		{
			get
			{
				return new EmptyConstraint();
			}
		}

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x06000830 RID: 2096 RVA: 0x0001CB98 File Offset: 0x0001AD98
		public UniqueItemsConstraint Unique
		{
			get
			{
				return new UniqueItemsConstraint();
			}
		}

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x06000831 RID: 2097 RVA: 0x0001CBB0 File Offset: 0x0001ADB0
		public BinarySerializableConstraint BinarySerializable
		{
			get
			{
				return new BinarySerializableConstraint();
			}
		}

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000832 RID: 2098 RVA: 0x0001CBC8 File Offset: 0x0001ADC8
		public XmlSerializableConstraint XmlSerializable
		{
			get
			{
				return new XmlSerializableConstraint();
			}
		}

		// Token: 0x06000833 RID: 2099 RVA: 0x0001CBE0 File Offset: 0x0001ADE0
		public EqualConstraint EqualTo(object expected)
		{
			return new EqualConstraint(expected);
		}

		// Token: 0x06000834 RID: 2100 RVA: 0x0001CBF8 File Offset: 0x0001ADF8
		public SameAsConstraint SameAs(object expected)
		{
			return new SameAsConstraint(expected);
		}

		// Token: 0x06000835 RID: 2101 RVA: 0x0001CC10 File Offset: 0x0001AE10
		public GreaterThanConstraint GreaterThan(object expected)
		{
			return new GreaterThanConstraint(expected);
		}

		// Token: 0x06000836 RID: 2102 RVA: 0x0001CC28 File Offset: 0x0001AE28
		public GreaterThanOrEqualConstraint GreaterThanOrEqualTo(object expected)
		{
			return new GreaterThanOrEqualConstraint(expected);
		}

		// Token: 0x06000837 RID: 2103 RVA: 0x0001CC40 File Offset: 0x0001AE40
		public GreaterThanOrEqualConstraint AtLeast(object expected)
		{
			return new GreaterThanOrEqualConstraint(expected);
		}

		// Token: 0x06000838 RID: 2104 RVA: 0x0001CC58 File Offset: 0x0001AE58
		public LessThanConstraint LessThan(object expected)
		{
			return new LessThanConstraint(expected);
		}

		// Token: 0x06000839 RID: 2105 RVA: 0x0001CC70 File Offset: 0x0001AE70
		public LessThanOrEqualConstraint LessThanOrEqualTo(object expected)
		{
			return new LessThanOrEqualConstraint(expected);
		}

		// Token: 0x0600083A RID: 2106 RVA: 0x0001CC88 File Offset: 0x0001AE88
		public LessThanOrEqualConstraint AtMost(object expected)
		{
			return new LessThanOrEqualConstraint(expected);
		}

		// Token: 0x0600083B RID: 2107 RVA: 0x0001CCA0 File Offset: 0x0001AEA0
		public ExactTypeConstraint TypeOf(Type expectedType)
		{
			return new ExactTypeConstraint(expectedType);
		}

		// Token: 0x0600083C RID: 2108 RVA: 0x0001CCB8 File Offset: 0x0001AEB8
		public ExactTypeConstraint TypeOf<TExpected>()
		{
			return new ExactTypeConstraint(typeof(TExpected));
		}

		// Token: 0x0600083D RID: 2109 RVA: 0x0001CCDC File Offset: 0x0001AEDC
		public InstanceOfTypeConstraint InstanceOf(Type expectedType)
		{
			return new InstanceOfTypeConstraint(expectedType);
		}

		// Token: 0x0600083E RID: 2110 RVA: 0x0001CCF4 File Offset: 0x0001AEF4
		public InstanceOfTypeConstraint InstanceOf<TExpected>()
		{
			return new InstanceOfTypeConstraint(typeof(TExpected));
		}

		// Token: 0x0600083F RID: 2111 RVA: 0x0001CD18 File Offset: 0x0001AF18
		public AssignableFromConstraint AssignableFrom(Type expectedType)
		{
			return new AssignableFromConstraint(expectedType);
		}

		// Token: 0x06000840 RID: 2112 RVA: 0x0001CD30 File Offset: 0x0001AF30
		public AssignableFromConstraint AssignableFrom<TExpected>()
		{
			return new AssignableFromConstraint(typeof(TExpected));
		}

		// Token: 0x06000841 RID: 2113 RVA: 0x0001CD54 File Offset: 0x0001AF54
		public AssignableToConstraint AssignableTo(Type expectedType)
		{
			return new AssignableToConstraint(expectedType);
		}

		// Token: 0x06000842 RID: 2114 RVA: 0x0001CD6C File Offset: 0x0001AF6C
		public AssignableToConstraint AssignableTo<TExpected>()
		{
			return new AssignableToConstraint(typeof(TExpected));
		}

		// Token: 0x06000843 RID: 2115 RVA: 0x0001CD90 File Offset: 0x0001AF90
		public CollectionEquivalentConstraint EquivalentTo(IEnumerable expected)
		{
			return new CollectionEquivalentConstraint(expected);
		}

		// Token: 0x06000844 RID: 2116 RVA: 0x0001CDA8 File Offset: 0x0001AFA8
		public CollectionSubsetConstraint SubsetOf(IEnumerable expected)
		{
			return new CollectionSubsetConstraint(expected);
		}

		// Token: 0x06000845 RID: 2117 RVA: 0x0001CDC0 File Offset: 0x0001AFC0
		public CollectionSupersetConstraint SupersetOf(IEnumerable expected)
		{
			return new CollectionSupersetConstraint(expected);
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06000846 RID: 2118 RVA: 0x0001CDD8 File Offset: 0x0001AFD8
		public CollectionOrderedConstraint Ordered
		{
			get
			{
				return new CollectionOrderedConstraint();
			}
		}

		// Token: 0x06000847 RID: 2119 RVA: 0x0001CDF0 File Offset: 0x0001AFF0
		public CollectionContainsConstraint Member(object expected)
		{
			return new CollectionContainsConstraint(expected);
		}

		// Token: 0x06000848 RID: 2120 RVA: 0x0001CE08 File Offset: 0x0001B008
		public CollectionContainsConstraint Contains(object expected)
		{
			return new CollectionContainsConstraint(expected);
		}

		// Token: 0x06000849 RID: 2121 RVA: 0x0001CE20 File Offset: 0x0001B020
		public ContainsConstraint Contains(string expected)
		{
			return new ContainsConstraint(expected);
		}

		// Token: 0x0600084A RID: 2122 RVA: 0x0001CE38 File Offset: 0x0001B038
		[Obsolete("Deprecated, use Contains")]
		public SubstringConstraint StringContaining(string expected)
		{
			return new SubstringConstraint(expected);
		}

		// Token: 0x0600084B RID: 2123 RVA: 0x0001CE50 File Offset: 0x0001B050
		[Obsolete("Deprecated, use Contains")]
		public SubstringConstraint ContainsSubstring(string expected)
		{
			return new SubstringConstraint(expected);
		}

		// Token: 0x0600084C RID: 2124 RVA: 0x0001CE68 File Offset: 0x0001B068
		[Obsolete("Deprecated, use Does.Not.Contain")]
		public SubstringConstraint DoesNotContain(string expected)
		{
			return new ConstraintExpression().Not.ContainsSubstring(expected);
		}

		// Token: 0x0600084D RID: 2125 RVA: 0x0001CE8C File Offset: 0x0001B08C
		public StartsWithConstraint StartWith(string expected)
		{
			return new StartsWithConstraint(expected);
		}

		// Token: 0x0600084E RID: 2126 RVA: 0x0001CEA4 File Offset: 0x0001B0A4
		public StartsWithConstraint StartsWith(string expected)
		{
			return new StartsWithConstraint(expected);
		}

		// Token: 0x0600084F RID: 2127 RVA: 0x0001CEBC File Offset: 0x0001B0BC
		[Obsolete("Deprecated, use Does.StartWith or StartsWith")]
		public StartsWithConstraint StringStarting(string expected)
		{
			return new StartsWithConstraint(expected);
		}

		// Token: 0x06000850 RID: 2128 RVA: 0x0001CED4 File Offset: 0x0001B0D4
		[Obsolete("Deprecated, use Does.Not.StartWith")]
		public StartsWithConstraint DoesNotStartWith(string expected)
		{
			return new ConstraintExpression().Not.StartsWith(expected);
		}

		// Token: 0x06000851 RID: 2129 RVA: 0x0001CEF8 File Offset: 0x0001B0F8
		public EndsWithConstraint EndWith(string expected)
		{
			return new EndsWithConstraint(expected);
		}

		// Token: 0x06000852 RID: 2130 RVA: 0x0001CF10 File Offset: 0x0001B110
		public EndsWithConstraint EndsWith(string expected)
		{
			return new EndsWithConstraint(expected);
		}

		// Token: 0x06000853 RID: 2131 RVA: 0x0001CF28 File Offset: 0x0001B128
		[Obsolete("Deprecated, use Does.EndWith or EndsWith")]
		public EndsWithConstraint StringEnding(string expected)
		{
			return new EndsWithConstraint(expected);
		}

		// Token: 0x06000854 RID: 2132 RVA: 0x0001CF40 File Offset: 0x0001B140
		[Obsolete("Deprecated, use Does.Not.EndWith")]
		public EndsWithConstraint DoesNotEndWith(string expected)
		{
			return new ConstraintExpression().Not.EndsWith(expected);
		}

		// Token: 0x06000855 RID: 2133 RVA: 0x0001CF64 File Offset: 0x0001B164
		public RegexConstraint Match(string pattern)
		{
			return new RegexConstraint(pattern);
		}

		// Token: 0x06000856 RID: 2134 RVA: 0x0001CF7C File Offset: 0x0001B17C
		public RegexConstraint Matches(string pattern)
		{
			return new RegexConstraint(pattern);
		}

		// Token: 0x06000857 RID: 2135 RVA: 0x0001CF94 File Offset: 0x0001B194
		[Obsolete("Deprecated, use Does.Match or Matches")]
		public RegexConstraint StringMatching(string pattern)
		{
			return new RegexConstraint(pattern);
		}

		// Token: 0x06000858 RID: 2136 RVA: 0x0001CFAC File Offset: 0x0001B1AC
		[Obsolete("Deprecated, use Does.Not.Match")]
		public RegexConstraint DoesNotMatch(string pattern)
		{
			return new ConstraintExpression().Not.Matches(pattern);
		}

		// Token: 0x06000859 RID: 2137 RVA: 0x0001CFD0 File Offset: 0x0001B1D0
		public SamePathConstraint SamePath(string expected)
		{
			return new SamePathConstraint(expected);
		}

		// Token: 0x0600085A RID: 2138 RVA: 0x0001CFE8 File Offset: 0x0001B1E8
		public SubPathConstraint SubPathOf(string expected)
		{
			return new SubPathConstraint(expected);
		}

		// Token: 0x0600085B RID: 2139 RVA: 0x0001D000 File Offset: 0x0001B200
		public SamePathOrUnderConstraint SamePathOrUnder(string expected)
		{
			return new SamePathOrUnderConstraint(expected);
		}

		// Token: 0x0600085C RID: 2140 RVA: 0x0001D018 File Offset: 0x0001B218
		public RangeConstraint InRange(IComparable from, IComparable to)
		{
			return new RangeConstraint(from, to);
		}

		// Token: 0x0600085D RID: 2141 RVA: 0x0001D031 File Offset: 0x0001B231
		public ConstraintFactory()
		{
		}
	}
}
