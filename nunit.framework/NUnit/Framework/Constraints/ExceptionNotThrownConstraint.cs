﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000043 RID: 67
	internal class ExceptionNotThrownConstraint : Constraint
	{
		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000200 RID: 512 RVA: 0x00007524 File Offset: 0x00005724
		public override string Description
		{
			get
			{
				return "No Exception to be thrown";
			}
		}

		// Token: 0x06000201 RID: 513 RVA: 0x0000753C File Offset: 0x0000573C
		public override ConstraintResult ApplyTo(object actual)
		{
			Exception ex = actual as Exception;
			return new ConstraintResult(this, ex, ex == null);
		}

		// Token: 0x06000202 RID: 514 RVA: 0x00007560 File Offset: 0x00005760
		public ExceptionNotThrownConstraint() : base(new object[0])
		{
		}
	}
}
