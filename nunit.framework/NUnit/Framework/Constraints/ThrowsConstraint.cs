﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000119 RID: 281
	public class ThrowsConstraint : PrefixConstraint
	{
		// Token: 0x060007A4 RID: 1956 RVA: 0x0001B4A9 File Offset: 0x000196A9
		public ThrowsConstraint(IConstraint baseConstraint) : base(baseConstraint)
		{
		}

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x060007A5 RID: 1957 RVA: 0x0001B4B8 File Offset: 0x000196B8
		public Exception ActualException
		{
			get
			{
				return this.caughtException;
			}
		}

		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x060007A6 RID: 1958 RVA: 0x0001B4D0 File Offset: 0x000196D0
		public override string Description
		{
			get
			{
				return base.BaseConstraint.Description;
			}
		}

		// Token: 0x060007A7 RID: 1959 RVA: 0x0001B4F0 File Offset: 0x000196F0
		public override ConstraintResult ApplyTo(object actual)
		{
			this.caughtException = ThrowsConstraint.ExceptionInterceptor.Intercept(actual);
			return new ThrowsConstraint.ThrowsConstraintResult(this, this.caughtException, (this.caughtException != null) ? base.BaseConstraint.ApplyTo(this.caughtException) : null);
		}

		// Token: 0x060007A8 RID: 1960 RVA: 0x0001B538 File Offset: 0x00019738
		public override ConstraintResult ApplyTo<TActual>(ActualValueDelegate<TActual> del)
		{
			return this.ApplyTo(new ThrowsConstraint.GenericInvocationDescriptor<TActual>(del));
		}

		// Token: 0x040001B4 RID: 436
		private Exception caughtException;

		// Token: 0x0200011A RID: 282
		private class ThrowsConstraintResult : ConstraintResult
		{
			// Token: 0x060007A9 RID: 1961 RVA: 0x0001B558 File Offset: 0x00019758
			public ThrowsConstraintResult(ThrowsConstraint constraint, Exception caughtException, ConstraintResult baseResult) : base(constraint, caughtException)
			{
				if (caughtException != null && baseResult.IsSuccess)
				{
					base.Status = ConstraintStatus.Success;
				}
				else
				{
					base.Status = ConstraintStatus.Failure;
				}
				this.baseResult = baseResult;
			}

			// Token: 0x060007AA RID: 1962 RVA: 0x0001B59C File Offset: 0x0001979C
			public override void WriteActualValueTo(MessageWriter writer)
			{
				if (base.ActualValue == null)
				{
					writer.Write("no exception thrown");
				}
				else
				{
					this.baseResult.WriteActualValueTo(writer);
				}
			}

			// Token: 0x040001B5 RID: 437
			private readonly ConstraintResult baseResult;
		}

		// Token: 0x0200011B RID: 283
		internal class ExceptionInterceptor
		{
			// Token: 0x060007AB RID: 1963 RVA: 0x0001B5D5 File Offset: 0x000197D5
			private ExceptionInterceptor()
			{
			}

			// Token: 0x060007AC RID: 1964 RVA: 0x0001B5E0 File Offset: 0x000197E0
			internal static Exception Intercept(object invocation)
			{
				ThrowsConstraint.IInvocationDescriptor invocationDescriptor = ThrowsConstraint.ExceptionInterceptor.GetInvocationDescriptor(invocation);
				Exception result;
				try
				{
					invocationDescriptor.Invoke();
					result = null;
				}
				catch (Exception ex)
				{
					result = ex;
				}
				return result;
			}

			// Token: 0x060007AD RID: 1965 RVA: 0x0001B61C File Offset: 0x0001981C
			private static ThrowsConstraint.IInvocationDescriptor GetInvocationDescriptor(object actual)
			{
				ThrowsConstraint.IInvocationDescriptor invocationDescriptor = actual as ThrowsConstraint.IInvocationDescriptor;
				if (invocationDescriptor == null)
				{
					TestDelegate testDelegate = actual as TestDelegate;
					if (testDelegate != null)
					{
						invocationDescriptor = new ThrowsConstraint.VoidInvocationDescriptor(testDelegate);
					}
				}
				if (invocationDescriptor == null)
				{
					throw new ArgumentException(string.Format("The actual value must be a TestDelegate or AsyncTestDelegate but was {0}", actual.GetType().Name), "actual");
				}
				return invocationDescriptor;
			}
		}

		// Token: 0x0200011C RID: 284
		private interface IInvocationDescriptor
		{
			// Token: 0x170001B5 RID: 437
			// (get) Token: 0x060007AE RID: 1966
			Delegate Delegate { get; }

			// Token: 0x060007AF RID: 1967
			object Invoke();
		}

		// Token: 0x0200011D RID: 285
		internal class GenericInvocationDescriptor<T> : ThrowsConstraint.IInvocationDescriptor
		{
			// Token: 0x060007B0 RID: 1968 RVA: 0x0001B686 File Offset: 0x00019886
			public GenericInvocationDescriptor(ActualValueDelegate<T> del)
			{
				this._del = del;
			}

			// Token: 0x060007B1 RID: 1969 RVA: 0x0001B698 File Offset: 0x00019898
			public object Invoke()
			{
				return this._del();
			}

			// Token: 0x170001B6 RID: 438
			// (get) Token: 0x060007B2 RID: 1970 RVA: 0x0001B6BC File Offset: 0x000198BC
			public Delegate Delegate
			{
				get
				{
					return this._del;
				}
			}

			// Token: 0x040001B6 RID: 438
			private readonly ActualValueDelegate<T> _del;
		}

		// Token: 0x0200011E RID: 286
		private class VoidInvocationDescriptor : ThrowsConstraint.IInvocationDescriptor
		{
			// Token: 0x060007B3 RID: 1971 RVA: 0x0001B6D4 File Offset: 0x000198D4
			public VoidInvocationDescriptor(TestDelegate del)
			{
				this._del = del;
			}

			// Token: 0x060007B4 RID: 1972 RVA: 0x0001B6E8 File Offset: 0x000198E8
			public object Invoke()
			{
				this._del();
				return null;
			}

			// Token: 0x170001B7 RID: 439
			// (get) Token: 0x060007B5 RID: 1973 RVA: 0x0001B708 File Offset: 0x00019908
			public Delegate Delegate
			{
				get
				{
					return this._del;
				}
			}

			// Token: 0x040001B7 RID: 439
			private readonly TestDelegate _del;
		}
	}
}
