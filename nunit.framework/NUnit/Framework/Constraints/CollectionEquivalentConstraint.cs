﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000109 RID: 265
	public class CollectionEquivalentConstraint : CollectionItemsEqualConstraint
	{
		// Token: 0x06000752 RID: 1874 RVA: 0x0001A51B File Offset: 0x0001871B
		public CollectionEquivalentConstraint(IEnumerable expected) : base(expected)
		{
			this._expected = expected;
		}

		// Token: 0x1700019F RID: 415
		// (get) Token: 0x06000753 RID: 1875 RVA: 0x0001A530 File Offset: 0x00018730
		public override string DisplayName
		{
			get
			{
				return "Equivalent";
			}
		}

		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x06000754 RID: 1876 RVA: 0x0001A548 File Offset: 0x00018748
		public override string Description
		{
			get
			{
				return "equivalent to " + MsgUtils.FormatValue(this._expected);
			}
		}

		// Token: 0x06000755 RID: 1877 RVA: 0x0001A570 File Offset: 0x00018770
		protected override bool Matches(IEnumerable actual)
		{
			bool result;
			if (this._expected is ICollection && actual is ICollection && ((ICollection)actual).Count != ((ICollection)this._expected).Count)
			{
				result = false;
			}
			else
			{
				CollectionTally collectionTally = base.Tally(this._expected);
				result = (collectionTally.TryRemove(actual) && collectionTally.Count == 0);
			}
			return result;
		}

		// Token: 0x06000756 RID: 1878 RVA: 0x0001A5EC File Offset: 0x000187EC
		public CollectionEquivalentConstraint Using<TActual, TExpected>(Func<TActual, TExpected, bool> comparison)
		{
			base.Using(EqualityAdapter.For<TActual, TExpected>(comparison));
			return this;
		}

		// Token: 0x040001A3 RID: 419
		private readonly IEnumerable _expected;
	}
}
