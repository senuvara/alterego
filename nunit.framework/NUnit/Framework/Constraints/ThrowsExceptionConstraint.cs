﻿using System;
using System.Runtime.CompilerServices;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000F6 RID: 246
	public class ThrowsExceptionConstraint : Constraint
	{
		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000701 RID: 1793 RVA: 0x0001991C File Offset: 0x00017B1C
		public override string Description
		{
			get
			{
				return "an exception to be thrown";
			}
		}

		// Token: 0x06000702 RID: 1794 RVA: 0x00019934 File Offset: 0x00017B34
		public override ConstraintResult ApplyTo(object actual)
		{
			TestDelegate testDelegate = actual as TestDelegate;
			Exception caughtException = null;
			if (testDelegate != null)
			{
				try
				{
					testDelegate();
				}
				catch (Exception ex)
				{
					caughtException = ex;
				}
				return new ThrowsExceptionConstraint.ThrowsExceptionConstraintResult(this, caughtException);
			}
			throw new ArgumentException(string.Format("The actual value must be a TestDelegate or AsyncTestDelegate but was {0}", actual.GetType().Name), "actual");
		}

		// Token: 0x06000703 RID: 1795 RVA: 0x000199C0 File Offset: 0x00017BC0
		protected override object GetTestObject<TActual>(ActualValueDelegate<TActual> del)
		{
			return new TestDelegate(delegate()
			{
				del();
			});
		}

		// Token: 0x06000704 RID: 1796 RVA: 0x000199EB File Offset: 0x00017BEB
		public ThrowsExceptionConstraint() : base(new object[0])
		{
		}

		// Token: 0x020000F7 RID: 247
		private class ThrowsExceptionConstraintResult : ConstraintResult
		{
			// Token: 0x06000705 RID: 1797 RVA: 0x000199F9 File Offset: 0x00017BF9
			public ThrowsExceptionConstraintResult(ThrowsExceptionConstraint constraint, Exception caughtException) : base(constraint, caughtException, caughtException != null)
			{
			}

			// Token: 0x06000706 RID: 1798 RVA: 0x00019A10 File Offset: 0x00017C10
			public override void WriteActualValueTo(MessageWriter writer)
			{
				if (base.Status == ConstraintStatus.Failure)
				{
					writer.Write("no exception thrown");
				}
				else
				{
					base.WriteActualValueTo(writer);
				}
			}
		}

		// Token: 0x020001BF RID: 447
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1<TActual>
		{
			// Token: 0x06000B4D RID: 2893 RVA: 0x000199A8 File Offset: 0x00017BA8
			public <>c__DisplayClass1()
			{
			}

			// Token: 0x06000B4E RID: 2894 RVA: 0x000199B0 File Offset: 0x00017BB0
			public void <GetTestObject>b__0()
			{
				this.del();
			}

			// Token: 0x040002DB RID: 731
			public ActualValueDelegate<TActual> del;
		}
	}
}
