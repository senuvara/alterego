﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000EB RID: 235
	public class NotConstraint : PrefixConstraint
	{
		// Token: 0x060006B5 RID: 1717 RVA: 0x00018583 File Offset: 0x00016783
		public NotConstraint(IConstraint baseConstraint) : base(baseConstraint)
		{
			base.DescriptionPrefix = "not";
		}

		// Token: 0x060006B6 RID: 1718 RVA: 0x0001859C File Offset: 0x0001679C
		public override ConstraintResult ApplyTo(object actual)
		{
			ConstraintResult constraintResult = base.BaseConstraint.ApplyTo(actual);
			return new ConstraintResult(this, constraintResult.ActualValue, !constraintResult.IsSuccess);
		}
	}
}
