﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000157 RID: 343
	public class WithOperator : PrefixOperator
	{
		// Token: 0x060008FF RID: 2303 RVA: 0x0001E7E0 File Offset: 0x0001C9E0
		public WithOperator()
		{
			this.left_precedence = 1;
			this.right_precedence = 4;
		}

		// Token: 0x06000900 RID: 2304 RVA: 0x0001E7FC File Offset: 0x0001C9FC
		public override IConstraint ApplyPrefix(IConstraint constraint)
		{
			return constraint;
		}
	}
}
