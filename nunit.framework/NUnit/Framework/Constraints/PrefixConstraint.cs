﻿using System;
using System.Runtime.CompilerServices;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200000E RID: 14
	public abstract class PrefixConstraint : Constraint
	{
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000069 RID: 105 RVA: 0x00002FC0 File Offset: 0x000011C0
		// (set) Token: 0x0600006A RID: 106 RVA: 0x00002FD7 File Offset: 0x000011D7
		protected IConstraint BaseConstraint
		{
			[CompilerGenerated]
			get
			{
				return this.<BaseConstraint>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<BaseConstraint>k__BackingField = value;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600006B RID: 107 RVA: 0x00002FE0 File Offset: 0x000011E0
		// (set) Token: 0x0600006C RID: 108 RVA: 0x00002FF7 File Offset: 0x000011F7
		protected string DescriptionPrefix
		{
			[CompilerGenerated]
			get
			{
				return this.<DescriptionPrefix>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<DescriptionPrefix>k__BackingField = value;
			}
		}

		// Token: 0x0600006D RID: 109 RVA: 0x00003000 File Offset: 0x00001200
		protected PrefixConstraint(IResolveConstraint baseConstraint) : base(new object[]
		{
			baseConstraint
		})
		{
			Guard.ArgumentNotNull(baseConstraint, "baseConstraint");
			this.BaseConstraint = baseConstraint.Resolve();
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600006E RID: 110 RVA: 0x0000303C File Offset: 0x0000123C
		public override string Description
		{
			get
			{
				return string.Format((this.BaseConstraint is EqualConstraint) ? "{0} equal to {1}" : "{0} {1}", this.DescriptionPrefix, this.BaseConstraint.Description);
			}
		}

		// Token: 0x0400000B RID: 11
		[CompilerGenerated]
		private IConstraint <BaseConstraint>k__BackingField;

		// Token: 0x0400000C RID: 12
		[CompilerGenerated]
		private string <DescriptionPrefix>k__BackingField;
	}
}
