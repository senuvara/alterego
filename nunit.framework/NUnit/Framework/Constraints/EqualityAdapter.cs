﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Compatibility;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200012F RID: 303
	public abstract class EqualityAdapter
	{
		// Token: 0x060007FF RID: 2047
		public abstract bool AreEqual(object x, object y);

		// Token: 0x06000800 RID: 2048 RVA: 0x0001C5E8 File Offset: 0x0001A7E8
		public virtual bool CanCompare(object x, object y)
		{
			return (x is string && y is string) || (!(x is IEnumerable) && !(y is IEnumerable));
		}

		// Token: 0x06000801 RID: 2049 RVA: 0x0001C63C File Offset: 0x0001A83C
		public static EqualityAdapter For(IComparer comparer)
		{
			return new EqualityAdapter.ComparerAdapter(comparer);
		}

		// Token: 0x06000802 RID: 2050 RVA: 0x0001C654 File Offset: 0x0001A854
		public static EqualityAdapter For(IEqualityComparer comparer)
		{
			return new EqualityAdapter.EqualityComparerAdapter(comparer);
		}

		// Token: 0x06000803 RID: 2051 RVA: 0x0001C66C File Offset: 0x0001A86C
		public static EqualityAdapter For<TExpected, TActual>(Func<TExpected, TActual, bool> comparison)
		{
			return new EqualityAdapter.PredicateEqualityAdapter<TExpected, TActual>(comparison);
		}

		// Token: 0x06000804 RID: 2052 RVA: 0x0001C684 File Offset: 0x0001A884
		public static EqualityAdapter For<T>(IEqualityComparer<T> comparer)
		{
			return new EqualityAdapter.EqualityComparerAdapter<T>(comparer);
		}

		// Token: 0x06000805 RID: 2053 RVA: 0x0001C69C File Offset: 0x0001A89C
		public static EqualityAdapter For<T>(IComparer<T> comparer)
		{
			return new EqualityAdapter.ComparerAdapter<T>(comparer);
		}

		// Token: 0x06000806 RID: 2054 RVA: 0x0001C6B4 File Offset: 0x0001A8B4
		public static EqualityAdapter For<T>(Comparison<T> comparer)
		{
			return new EqualityAdapter.ComparisonAdapter<T>(comparer);
		}

		// Token: 0x06000807 RID: 2055 RVA: 0x0001C6CC File Offset: 0x0001A8CC
		protected EqualityAdapter()
		{
		}

		// Token: 0x02000130 RID: 304
		private class ComparerAdapter : EqualityAdapter
		{
			// Token: 0x06000808 RID: 2056 RVA: 0x0001C6D4 File Offset: 0x0001A8D4
			public ComparerAdapter(IComparer comparer)
			{
				this.comparer = comparer;
			}

			// Token: 0x06000809 RID: 2057 RVA: 0x0001C6E8 File Offset: 0x0001A8E8
			public override bool AreEqual(object x, object y)
			{
				return this.comparer.Compare(x, y) == 0;
			}

			// Token: 0x040001C5 RID: 453
			private IComparer comparer;
		}

		// Token: 0x02000131 RID: 305
		private class EqualityComparerAdapter : EqualityAdapter
		{
			// Token: 0x0600080A RID: 2058 RVA: 0x0001C70A File Offset: 0x0001A90A
			public EqualityComparerAdapter(IEqualityComparer comparer)
			{
				this.comparer = comparer;
			}

			// Token: 0x0600080B RID: 2059 RVA: 0x0001C71C File Offset: 0x0001A91C
			public override bool AreEqual(object x, object y)
			{
				return this.comparer.Equals(x, y);
			}

			// Token: 0x040001C6 RID: 454
			private IEqualityComparer comparer;
		}

		// Token: 0x02000132 RID: 306
		internal class PredicateEqualityAdapter<TActual, TExpected> : EqualityAdapter
		{
			// Token: 0x0600080C RID: 2060 RVA: 0x0001C73C File Offset: 0x0001A93C
			public override bool CanCompare(object x, object y)
			{
				return true;
			}

			// Token: 0x0600080D RID: 2061 RVA: 0x0001C750 File Offset: 0x0001A950
			public override bool AreEqual(object x, object y)
			{
				return this._comparison((TActual)((object)y), (TExpected)((object)x));
			}

			// Token: 0x0600080E RID: 2062 RVA: 0x0001C779 File Offset: 0x0001A979
			public PredicateEqualityAdapter(Func<TActual, TExpected, bool> comparison)
			{
				this._comparison = comparison;
			}

			// Token: 0x040001C7 RID: 455
			private readonly Func<TActual, TExpected, bool> _comparison;
		}

		// Token: 0x02000133 RID: 307
		private abstract class GenericEqualityAdapter<T> : EqualityAdapter
		{
			// Token: 0x0600080F RID: 2063 RVA: 0x0001C78C File Offset: 0x0001A98C
			public override bool CanCompare(object x, object y)
			{
				return typeof(T).GetTypeInfo().IsAssignableFrom(x.GetType().GetTypeInfo()) && typeof(T).GetTypeInfo().IsAssignableFrom(y.GetType().GetTypeInfo());
			}

			// Token: 0x06000810 RID: 2064 RVA: 0x0001C7E4 File Offset: 0x0001A9E4
			protected void ThrowIfNotCompatible(object x, object y)
			{
				if (!typeof(T).GetTypeInfo().IsAssignableFrom(x.GetType().GetTypeInfo()))
				{
					throw new ArgumentException("Cannot compare " + x.ToString());
				}
				if (!typeof(T).GetTypeInfo().IsAssignableFrom(y.GetType().GetTypeInfo()))
				{
					throw new ArgumentException("Cannot compare " + y.ToString());
				}
			}

			// Token: 0x06000811 RID: 2065 RVA: 0x0001C864 File Offset: 0x0001AA64
			protected GenericEqualityAdapter()
			{
			}
		}

		// Token: 0x02000134 RID: 308
		private class EqualityComparerAdapter<T> : EqualityAdapter.GenericEqualityAdapter<T>
		{
			// Token: 0x06000812 RID: 2066 RVA: 0x0001C86C File Offset: 0x0001AA6C
			public EqualityComparerAdapter(IEqualityComparer<T> comparer)
			{
				this.comparer = comparer;
			}

			// Token: 0x06000813 RID: 2067 RVA: 0x0001C880 File Offset: 0x0001AA80
			public override bool AreEqual(object x, object y)
			{
				base.ThrowIfNotCompatible(x, y);
				return this.comparer.Equals((T)((object)x), (T)((object)y));
			}

			// Token: 0x040001C8 RID: 456
			private IEqualityComparer<T> comparer;
		}

		// Token: 0x02000135 RID: 309
		private class ComparerAdapter<T> : EqualityAdapter.GenericEqualityAdapter<T>
		{
			// Token: 0x06000814 RID: 2068 RVA: 0x0001C8B2 File Offset: 0x0001AAB2
			public ComparerAdapter(IComparer<T> comparer)
			{
				this.comparer = comparer;
			}

			// Token: 0x06000815 RID: 2069 RVA: 0x0001C8C4 File Offset: 0x0001AAC4
			public override bool AreEqual(object x, object y)
			{
				base.ThrowIfNotCompatible(x, y);
				return this.comparer.Compare((T)((object)x), (T)((object)y)) == 0;
			}

			// Token: 0x040001C9 RID: 457
			private IComparer<T> comparer;
		}

		// Token: 0x02000136 RID: 310
		private class ComparisonAdapter<T> : EqualityAdapter.GenericEqualityAdapter<T>
		{
			// Token: 0x06000816 RID: 2070 RVA: 0x0001C8F9 File Offset: 0x0001AAF9
			public ComparisonAdapter(Comparison<T> comparer)
			{
				this.comparer = comparer;
			}

			// Token: 0x06000817 RID: 2071 RVA: 0x0001C90C File Offset: 0x0001AB0C
			public override bool AreEqual(object x, object y)
			{
				base.ThrowIfNotCompatible(x, y);
				return this.comparer((T)((object)x), (T)((object)y)) == 0;
			}

			// Token: 0x040001CA RID: 458
			private Comparison<T> comparer;
		}
	}
}
