﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000AF RID: 175
	public class EqualConstraintResult : ConstraintResult
	{
		// Token: 0x0600050C RID: 1292 RVA: 0x000117D8 File Offset: 0x0000F9D8
		public EqualConstraintResult(EqualConstraint constraint, object actual, bool hasSucceeded) : base(constraint, actual, hasSucceeded)
		{
			this.expectedValue = constraint.Arguments[0];
			this.tolerance = constraint.Tolerance;
			this.caseInsensitive = constraint.CaseInsensitive;
			this.clipStrings = constraint.ClipStrings;
			this.failurePoints = constraint.FailurePoints;
		}

		// Token: 0x0600050D RID: 1293 RVA: 0x0001182F File Offset: 0x0000FA2F
		public override void WriteMessageTo(MessageWriter writer)
		{
			this.DisplayDifferences(writer, this.expectedValue, base.ActualValue, 0);
		}

		// Token: 0x0600050E RID: 1294 RVA: 0x00011848 File Offset: 0x0000FA48
		private void DisplayDifferences(MessageWriter writer, object expected, object actual, int depth)
		{
			if (expected is string && actual is string)
			{
				this.DisplayStringDifferences(writer, (string)expected, (string)actual);
			}
			else if (expected is ICollection && actual is ICollection)
			{
				this.DisplayCollectionDifferences(writer, (ICollection)expected, (ICollection)actual, depth);
			}
			else if (expected is IEnumerable && actual is IEnumerable)
			{
				this.DisplayEnumerableDifferences(writer, (IEnumerable)expected, (IEnumerable)actual, depth);
			}
			else if (expected is Stream && actual is Stream)
			{
				this.DisplayStreamDifferences(writer, (Stream)expected, (Stream)actual, depth);
			}
			else if (this.tolerance != null)
			{
				writer.DisplayDifferences(expected, actual, this.tolerance);
			}
			else
			{
				writer.DisplayDifferences(expected, actual);
			}
		}

		// Token: 0x0600050F RID: 1295 RVA: 0x00011950 File Offset: 0x0000FB50
		private void DisplayStringDifferences(MessageWriter writer, string expected, string actual)
		{
			int num = MsgUtils.FindMismatchPosition(expected, actual, 0, this.caseInsensitive);
			if (expected.Length == actual.Length)
			{
				writer.WriteMessageLine(EqualConstraintResult.StringsDiffer_1, new object[]
				{
					expected.Length,
					num
				});
			}
			else
			{
				writer.WriteMessageLine(EqualConstraintResult.StringsDiffer_2, new object[]
				{
					expected.Length,
					actual.Length,
					num
				});
			}
			writer.DisplayStringDifferences(expected, actual, num, this.caseInsensitive, this.clipStrings);
		}

		// Token: 0x06000510 RID: 1296 RVA: 0x00011A00 File Offset: 0x0000FC00
		private void DisplayStreamDifferences(MessageWriter writer, Stream expected, Stream actual, int depth)
		{
			if (expected.Length == actual.Length)
			{
				long position = this.failurePoints[depth].Position;
				writer.WriteMessageLine(EqualConstraintResult.StreamsDiffer_1, new object[]
				{
					expected.Length,
					position
				});
			}
			else
			{
				writer.WriteMessageLine(EqualConstraintResult.StreamsDiffer_2, new object[]
				{
					expected.Length,
					actual.Length
				});
			}
		}

		// Token: 0x06000511 RID: 1297 RVA: 0x00011A98 File Offset: 0x0000FC98
		private void DisplayCollectionDifferences(MessageWriter writer, ICollection expected, ICollection actual, int depth)
		{
			this.DisplayTypesAndSizes(writer, expected, actual, depth);
			if (this.failurePoints.Count > depth)
			{
				NUnitEqualityComparer.FailurePoint failurePoint = this.failurePoints[depth];
				this.DisplayFailurePoint(writer, expected, actual, failurePoint, depth);
				if (failurePoint.ExpectedHasData && failurePoint.ActualHasData)
				{
					this.DisplayDifferences(writer, failurePoint.ExpectedValue, failurePoint.ActualValue, ++depth);
				}
				else if (failurePoint.ActualHasData)
				{
					writer.Write("  Extra:    ");
					writer.WriteCollectionElements(actual, failurePoint.Position, 3);
				}
				else
				{
					writer.Write("  Missing:  ");
					writer.WriteCollectionElements(expected, failurePoint.Position, 3);
				}
			}
		}

		// Token: 0x06000512 RID: 1298 RVA: 0x00011B68 File Offset: 0x0000FD68
		private void DisplayTypesAndSizes(MessageWriter writer, IEnumerable expected, IEnumerable actual, int indent)
		{
			string text = MsgUtils.GetTypeRepresentation(expected);
			if (expected is ICollection && !(expected is Array))
			{
				text += string.Format(" with {0} elements", ((ICollection)expected).Count);
			}
			string text2 = MsgUtils.GetTypeRepresentation(actual);
			if (actual is ICollection && !(actual is Array))
			{
				text2 += string.Format(" with {0} elements", ((ICollection)actual).Count);
			}
			if (text == text2)
			{
				writer.WriteMessageLine(indent, EqualConstraintResult.CollectionType_1, new object[]
				{
					text
				});
			}
			else
			{
				writer.WriteMessageLine(indent, EqualConstraintResult.CollectionType_2, new object[]
				{
					text,
					text2
				});
			}
		}

		// Token: 0x06000513 RID: 1299 RVA: 0x00011C40 File Offset: 0x0000FE40
		private void DisplayFailurePoint(MessageWriter writer, IEnumerable expected, IEnumerable actual, NUnitEqualityComparer.FailurePoint failurePoint, int indent)
		{
			Array array = expected as Array;
			Array array2 = actual as Array;
			int num = (array != null) ? array.Rank : 1;
			int num2 = (array2 != null) ? array2.Rank : 1;
			bool flag = num == num2;
			if (array != null && array2 != null)
			{
				int num3 = 1;
				while (num3 < num && flag)
				{
					if (array.GetLength(num3) != array2.GetLength(num3))
					{
						flag = false;
					}
					num3++;
				}
			}
			int[] arrayIndicesFromCollectionIndex = MsgUtils.GetArrayIndicesFromCollectionIndex(expected, failurePoint.Position);
			if (flag)
			{
				writer.WriteMessageLine(indent, EqualConstraintResult.ValuesDiffer_1, new object[]
				{
					MsgUtils.GetArrayIndicesAsString(arrayIndicesFromCollectionIndex)
				});
			}
			else
			{
				int[] arrayIndicesFromCollectionIndex2 = MsgUtils.GetArrayIndicesFromCollectionIndex(actual, failurePoint.Position);
				writer.WriteMessageLine(indent, EqualConstraintResult.ValuesDiffer_2, new object[]
				{
					MsgUtils.GetArrayIndicesAsString(arrayIndicesFromCollectionIndex),
					MsgUtils.GetArrayIndicesAsString(arrayIndicesFromCollectionIndex2)
				});
			}
		}

		// Token: 0x06000514 RID: 1300 RVA: 0x00011D44 File Offset: 0x0000FF44
		private static object GetValueFromCollection(ICollection collection, int index)
		{
			Array array = collection as Array;
			object result;
			if (array != null && array.Rank > 1)
			{
				result = array.GetValue(MsgUtils.GetArrayIndicesFromCollectionIndex(array, (long)index));
			}
			else if (collection is IList)
			{
				result = ((IList)collection)[index];
			}
			else
			{
				foreach (object result2 in collection)
				{
					if (--index < 0)
					{
						return result2;
					}
				}
				result = null;
			}
			return result;
		}

		// Token: 0x06000515 RID: 1301 RVA: 0x00011E08 File Offset: 0x00010008
		private void DisplayEnumerableDifferences(MessageWriter writer, IEnumerable expected, IEnumerable actual, int depth)
		{
			this.DisplayTypesAndSizes(writer, expected, actual, depth);
			if (this.failurePoints.Count > depth)
			{
				NUnitEqualityComparer.FailurePoint failurePoint = this.failurePoints[depth];
				this.DisplayFailurePoint(writer, expected, actual, failurePoint, depth);
				if (failurePoint.ExpectedHasData && failurePoint.ActualHasData)
				{
					this.DisplayDifferences(writer, failurePoint.ExpectedValue, failurePoint.ActualValue, ++depth);
				}
			}
		}

		// Token: 0x06000516 RID: 1302 RVA: 0x00011E88 File Offset: 0x00010088
		// Note: this type is marked as 'beforefieldinit'.
		static EqualConstraintResult()
		{
		}

		// Token: 0x040000FC RID: 252
		private object expectedValue;

		// Token: 0x040000FD RID: 253
		private Tolerance tolerance;

		// Token: 0x040000FE RID: 254
		private bool caseInsensitive;

		// Token: 0x040000FF RID: 255
		private bool clipStrings;

		// Token: 0x04000100 RID: 256
		private IList<NUnitEqualityComparer.FailurePoint> failurePoints;

		// Token: 0x04000101 RID: 257
		private static readonly string StringsDiffer_1 = "String lengths are both {0}. Strings differ at index {1}.";

		// Token: 0x04000102 RID: 258
		private static readonly string StringsDiffer_2 = "Expected string length {0} but was {1}. Strings differ at index {2}.";

		// Token: 0x04000103 RID: 259
		private static readonly string StreamsDiffer_1 = "Stream lengths are both {0}. Streams differ at offset {1}.";

		// Token: 0x04000104 RID: 260
		private static readonly string StreamsDiffer_2 = "Expected Stream length {0} but was {1}.";

		// Token: 0x04000105 RID: 261
		private static readonly string CollectionType_1 = "Expected and actual are both {0}";

		// Token: 0x04000106 RID: 262
		private static readonly string CollectionType_2 = "Expected is {0}, actual is {1}";

		// Token: 0x04000107 RID: 263
		private static readonly string ValuesDiffer_1 = "Values differ at index {0}";

		// Token: 0x04000108 RID: 264
		private static readonly string ValuesDiffer_2 = "Values differ at expected index {0}, actual index {1}";
	}
}
