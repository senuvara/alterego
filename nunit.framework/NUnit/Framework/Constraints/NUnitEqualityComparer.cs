﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Compatibility;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000075 RID: 117
	public class NUnitEqualityComparer
	{
		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000431 RID: 1073 RVA: 0x0000D798 File Offset: 0x0000B998
		public static NUnitEqualityComparer Default
		{
			get
			{
				return new NUnitEqualityComparer();
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000432 RID: 1074 RVA: 0x0000D7B0 File Offset: 0x0000B9B0
		// (set) Token: 0x06000433 RID: 1075 RVA: 0x0000D7C8 File Offset: 0x0000B9C8
		public bool IgnoreCase
		{
			get
			{
				return this.caseInsensitive;
			}
			set
			{
				this.caseInsensitive = value;
			}
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000434 RID: 1076 RVA: 0x0000D7D4 File Offset: 0x0000B9D4
		// (set) Token: 0x06000435 RID: 1077 RVA: 0x0000D7EC File Offset: 0x0000B9EC
		public bool CompareAsCollection
		{
			get
			{
				return this.compareAsCollection;
			}
			set
			{
				this.compareAsCollection = value;
			}
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x06000436 RID: 1078 RVA: 0x0000D7F8 File Offset: 0x0000B9F8
		public IList<EqualityAdapter> ExternalComparers
		{
			get
			{
				return this.externalComparers;
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000437 RID: 1079 RVA: 0x0000D810 File Offset: 0x0000BA10
		public IList<NUnitEqualityComparer.FailurePoint> FailurePoints
		{
			get
			{
				return this.failurePoints;
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000438 RID: 1080 RVA: 0x0000D828 File Offset: 0x0000BA28
		// (set) Token: 0x06000439 RID: 1081 RVA: 0x0000D83F File Offset: 0x0000BA3F
		public bool WithSameOffset
		{
			[CompilerGenerated]
			get
			{
				return this.<WithSameOffset>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<WithSameOffset>k__BackingField = value;
			}
		}

		// Token: 0x0600043A RID: 1082 RVA: 0x0000D848 File Offset: 0x0000BA48
		public bool AreEqual(object x, object y, ref Tolerance tolerance)
		{
			this.failurePoints = new List<NUnitEqualityComparer.FailurePoint>();
			NUnitEqualityComparer.CheckGameObjectReference<object>(ref x);
			NUnitEqualityComparer.CheckGameObjectReference<object>(ref y);
			bool result;
			if (x == null && y == null)
			{
				result = true;
			}
			else if (x == null || y == null)
			{
				result = false;
			}
			else if (object.ReferenceEquals(x, y))
			{
				result = true;
			}
			else
			{
				Type type = x.GetType();
				Type type2 = y.GetType();
				Type type3 = type.GetTypeInfo().IsGenericType ? type.GetGenericTypeDefinition() : null;
				Type type4 = type2.GetTypeInfo().IsGenericType ? type2.GetGenericTypeDefinition() : null;
				EqualityAdapter externalComparer = this.GetExternalComparer(x, y);
				if (externalComparer != null)
				{
					result = externalComparer.AreEqual(x, y);
				}
				else if (type.IsArray && type2.IsArray && !this.compareAsCollection)
				{
					result = this.ArraysEqual((Array)x, (Array)y, ref tolerance);
				}
				else if (x is IDictionary && y is IDictionary)
				{
					result = this.DictionariesEqual((IDictionary)x, (IDictionary)y, ref tolerance);
				}
				else if (x is DictionaryEntry && y is DictionaryEntry)
				{
					result = this.DictionaryEntriesEqual((DictionaryEntry)x, (DictionaryEntry)y, ref tolerance);
				}
				else if (type3 == typeof(KeyValuePair<, >) && type4 == typeof(KeyValuePair<, >))
				{
					Tolerance exact = Tolerance.Exact;
					object value = type.GetProperty("Key").GetValue(x, null);
					object value2 = type2.GetProperty("Key").GetValue(y, null);
					object value3 = type.GetProperty("Value").GetValue(x, null);
					object value4 = type2.GetProperty("Value").GetValue(y, null);
					result = (this.AreEqual(value, value2, ref exact) && this.AreEqual(value3, value4, ref tolerance));
				}
				else if (x is string && y is string)
				{
					result = this.StringsEqual((string)x, (string)y);
				}
				else if (x is Stream && y is Stream)
				{
					result = this.StreamsEqual((Stream)x, (Stream)y);
				}
				else if (x is char && y is char)
				{
					result = this.CharsEqual((char)x, (char)y);
				}
				else if (x is DirectoryInfo && y is DirectoryInfo)
				{
					result = NUnitEqualityComparer.DirectoriesEqual((DirectoryInfo)x, (DirectoryInfo)y);
				}
				else if (Numerics.IsNumericType(x) && Numerics.IsNumericType(y))
				{
					result = Numerics.AreEqual(x, y, ref tolerance);
				}
				else if (x is DateTimeOffset && y is DateTimeOffset)
				{
					DateTimeOffset left = (DateTimeOffset)x;
					DateTimeOffset right = (DateTimeOffset)y;
					bool flag;
					if (tolerance != null && tolerance.Value is TimeSpan)
					{
						TimeSpan t = (TimeSpan)tolerance.Value;
						flag = ((left - right).Duration() <= t);
					}
					else
					{
						flag = (left == right);
					}
					if (flag && this.WithSameOffset)
					{
						flag = (left.Offset == right.Offset);
					}
					result = flag;
				}
				else
				{
					if (tolerance != null && tolerance.Value is TimeSpan)
					{
						TimeSpan t = (TimeSpan)tolerance.Value;
						if (x is DateTime && y is DateTime)
						{
							return ((DateTime)x - (DateTime)y).Duration() <= t;
						}
						if (x is TimeSpan && y is TimeSpan)
						{
							return ((TimeSpan)x - (TimeSpan)y).Duration() <= t;
						}
					}
					MethodInfo methodInfo = NUnitEqualityComparer.FirstImplementsIEquatableOfSecond(type, type2);
					if (methodInfo != null)
					{
						result = NUnitEqualityComparer.InvokeFirstIEquatableEqualsSecond(x, y, methodInfo);
					}
					else if (type != type2 && (methodInfo = NUnitEqualityComparer.FirstImplementsIEquatableOfSecond(type2, type)) != null)
					{
						result = NUnitEqualityComparer.InvokeFirstIEquatableEqualsSecond(y, x, methodInfo);
					}
					else if (x is IEnumerable && y is IEnumerable)
					{
						result = this.EnumerablesEqual((IEnumerable)x, (IEnumerable)y, ref tolerance);
					}
					else
					{
						result = x.Equals(y);
					}
				}
			}
			return result;
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x0000DD70 File Offset: 0x0000BF70
		private static MethodInfo FirstImplementsIEquatableOfSecond(Type first, Type second)
		{
			KeyValuePair<Type, MethodInfo> keyValuePair = default(KeyValuePair<Type, MethodInfo>);
			foreach (KeyValuePair<Type, MethodInfo> keyValuePair2 in NUnitEqualityComparer.GetEquatableGenericArguments(first))
			{
				if (keyValuePair2.Key.IsAssignableFrom(second) && (keyValuePair.Key == null || keyValuePair.Key.IsAssignableFrom(keyValuePair2.Key)))
				{
					keyValuePair = keyValuePair2;
				}
			}
			return keyValuePair.Value;
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x0000DE18 File Offset: 0x0000C018
		private static IList<KeyValuePair<Type, MethodInfo>> GetEquatableGenericArguments(Type type)
		{
			List<KeyValuePair<Type, MethodInfo>> list = new List<KeyValuePair<Type, MethodInfo>>();
			foreach (Type type2 in type.GetInterfaces())
			{
				if (type2.GetTypeInfo().IsGenericType && type2.GetGenericTypeDefinition().Equals(typeof(IEquatable<>)))
				{
					list.Add(new KeyValuePair<Type, MethodInfo>(type2.GetGenericArguments()[0], type2.GetMethod("Equals")));
				}
			}
			return list;
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x0000DEA8 File Offset: 0x0000C0A8
		private static bool InvokeFirstIEquatableEqualsSecond(object first, object second, MethodInfo equals)
		{
			bool result;
			try
			{
				result = (equals != null && (bool)equals.Invoke(first, new object[]
				{
					second
				}));
			}
			catch (TargetInvocationException)
			{
				result = first.Equals(second);
			}
			return result;
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x0000DEF8 File Offset: 0x0000C0F8
		private EqualityAdapter GetExternalComparer(object x, object y)
		{
			foreach (EqualityAdapter equalityAdapter in this.externalComparers)
			{
				if (equalityAdapter.CanCompare(x, y))
				{
					return equalityAdapter;
				}
			}
			return null;
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x0000DF64 File Offset: 0x0000C164
		private bool ArraysEqual(Array x, Array y, ref Tolerance tolerance)
		{
			int rank = x.Rank;
			bool result;
			if (rank != y.Rank)
			{
				result = false;
			}
			else
			{
				for (int i = 1; i < rank; i++)
				{
					if (x.GetLength(i) != y.GetLength(i))
					{
						return false;
					}
				}
				result = this.EnumerablesEqual(x, y, ref tolerance);
			}
			return result;
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x0000DFC0 File Offset: 0x0000C1C0
		private bool DictionariesEqual(IDictionary x, IDictionary y, ref Tolerance tolerance)
		{
			bool result;
			if (x.Count != y.Count)
			{
				result = false;
			}
			else
			{
				CollectionTally collectionTally = new CollectionTally(this, x.Keys);
				if (!collectionTally.TryRemove(y.Keys) || collectionTally.Count > 0)
				{
					result = false;
				}
				else
				{
					foreach (object key in x.Keys)
					{
						if (!this.AreEqual(x[key], y[key], ref tolerance))
						{
							return false;
						}
					}
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x0000E090 File Offset: 0x0000C290
		private bool DictionaryEntriesEqual(DictionaryEntry x, DictionaryEntry y, ref Tolerance tolerance)
		{
			Tolerance exact = Tolerance.Exact;
			return this.AreEqual(x.Key, y.Key, ref exact) && this.AreEqual(x.Value, y.Value, ref tolerance);
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x0000E0D8 File Offset: 0x0000C2D8
		private bool CollectionsEqual(ICollection x, ICollection y, ref Tolerance tolerance)
		{
			IEnumerator enumerator = null;
			IEnumerator enumerator2 = null;
			bool result;
			try
			{
				enumerator = x.GetEnumerator();
				enumerator2 = y.GetEnumerator();
				int num = 0;
				bool flag;
				bool flag2;
				for (;;)
				{
					flag = enumerator.MoveNext();
					flag2 = enumerator2.MoveNext();
					if (!flag && !flag2)
					{
						break;
					}
					if (flag != flag2 || !this.AreEqual(enumerator.Current, enumerator2.Current, ref tolerance))
					{
						goto Block_6;
					}
					num++;
				}
				return true;
				Block_6:
				NUnitEqualityComparer.FailurePoint failurePoint = new NUnitEqualityComparer.FailurePoint();
				failurePoint.Position = (long)num;
				failurePoint.ExpectedHasData = flag;
				if (flag)
				{
					failurePoint.ExpectedValue = enumerator.Current;
				}
				failurePoint.ActualHasData = flag2;
				if (flag2)
				{
					failurePoint.ActualValue = enumerator2.Current;
				}
				this.failurePoints.Insert(0, failurePoint);
				result = false;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
				IDisposable disposable2 = enumerator2 as IDisposable;
				if (disposable2 != null)
				{
					disposable2.Dispose();
				}
			}
			return result;
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x0000E208 File Offset: 0x0000C408
		private bool StringsEqual(string x, string y)
		{
			string text = this.caseInsensitive ? x.ToLower() : x;
			string value = this.caseInsensitive ? y.ToLower() : y;
			return text.Equals(value);
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x0000E248 File Offset: 0x0000C448
		private bool CharsEqual(char x, char y)
		{
			char c = this.caseInsensitive ? char.ToLower(x) : x;
			char c2 = this.caseInsensitive ? char.ToLower(y) : y;
			return c == c2;
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x0000E284 File Offset: 0x0000C484
		private bool EnumerablesEqual(IEnumerable x, IEnumerable y, ref Tolerance tolerance)
		{
			IEnumerator enumerator = null;
			IEnumerator enumerator2 = null;
			bool result;
			try
			{
				enumerator = x.GetEnumerator();
				enumerator2 = y.GetEnumerator();
				int num = 0;
				bool flag;
				bool flag2;
				for (;;)
				{
					flag = enumerator.MoveNext();
					flag2 = enumerator2.MoveNext();
					if (!flag && !flag2)
					{
						break;
					}
					if (flag != flag2 || !this.AreEqual(enumerator.Current, enumerator2.Current, ref tolerance))
					{
						goto Block_6;
					}
					num++;
				}
				return true;
				Block_6:
				NUnitEqualityComparer.FailurePoint failurePoint = new NUnitEqualityComparer.FailurePoint();
				failurePoint.Position = (long)num;
				failurePoint.ExpectedHasData = flag;
				if (flag)
				{
					failurePoint.ExpectedValue = enumerator.Current;
				}
				failurePoint.ActualHasData = flag2;
				if (flag2)
				{
					failurePoint.ActualValue = enumerator2.Current;
				}
				this.failurePoints.Insert(0, failurePoint);
				result = false;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
				IDisposable disposable2 = enumerator2 as IDisposable;
				if (disposable2 != null)
				{
					disposable2.Dispose();
				}
			}
			return result;
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x0000E3B4 File Offset: 0x0000C5B4
		private static bool DirectoriesEqual(DirectoryInfo x, DirectoryInfo y)
		{
			return x.Attributes == y.Attributes && !(x.CreationTime != y.CreationTime) && !(x.LastAccessTime != y.LastAccessTime) && new SamePathConstraint(x.FullName).ApplyTo(y.FullName).IsSuccess;
		}

		// Token: 0x06000447 RID: 1095 RVA: 0x0000E424 File Offset: 0x0000C624
		private bool StreamsEqual(Stream x, Stream y)
		{
			bool result;
			if (x == y)
			{
				result = true;
			}
			else
			{
				if (!x.CanRead)
				{
					throw new ArgumentException("Stream is not readable", "expected");
				}
				if (!y.CanRead)
				{
					throw new ArgumentException("Stream is not readable", "actual");
				}
				if (!x.CanSeek)
				{
					throw new ArgumentException("Stream is not seekable", "expected");
				}
				if (!y.CanSeek)
				{
					throw new ArgumentException("Stream is not seekable", "actual");
				}
				if (x.Length != y.Length)
				{
					result = false;
				}
				else
				{
					byte[] array = new byte[NUnitEqualityComparer.BUFFER_SIZE];
					byte[] array2 = new byte[NUnitEqualityComparer.BUFFER_SIZE];
					BinaryReader binaryReader = new BinaryReader(x);
					BinaryReader binaryReader2 = new BinaryReader(y);
					long position = x.Position;
					long position2 = y.Position;
					try
					{
						binaryReader.BaseStream.Seek(0L, SeekOrigin.Begin);
						binaryReader2.BaseStream.Seek(0L, SeekOrigin.Begin);
						for (long num = 0L; num < x.Length; num += (long)NUnitEqualityComparer.BUFFER_SIZE)
						{
							binaryReader.Read(array, 0, NUnitEqualityComparer.BUFFER_SIZE);
							binaryReader2.Read(array2, 0, NUnitEqualityComparer.BUFFER_SIZE);
							for (int i = 0; i < NUnitEqualityComparer.BUFFER_SIZE; i++)
							{
								if (array[i] != array2[i])
								{
									NUnitEqualityComparer.FailurePoint failurePoint = new NUnitEqualityComparer.FailurePoint();
									failurePoint.Position = num + (long)i;
									failurePoint.ExpectedHasData = true;
									failurePoint.ExpectedValue = array[i];
									failurePoint.ActualHasData = true;
									failurePoint.ActualValue = array2[i];
									this.failurePoints.Insert(0, failurePoint);
									return false;
								}
							}
						}
					}
					finally
					{
						x.Position = position;
						y.Position = position2;
					}
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x0000E624 File Offset: 0x0000C824
		internal static void CheckGameObjectReference<T>(ref T value)
		{
			if (value != null && NUnitEqualityComparer.GameObjectType != null && NUnitEqualityComparer.GameObjectType.IsInstanceOfType(value))
			{
				MethodInfo method = NUnitEqualityComparer.GameObjectType.GetMethod("GetInstanceID");
				if (method != null)
				{
					object obj = method.Invoke(value, null);
					if ((int)obj == 0)
					{
						value = default(T);
					}
				}
			}
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x0000E6C6 File Offset: 0x0000C8C6
		public NUnitEqualityComparer()
		{
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x0000E6AB File Offset: 0x0000C8AB
		// Note: this type is marked as 'beforefieldinit'.
		static NUnitEqualityComparer()
		{
		}

		// Token: 0x040000B5 RID: 181
		private bool caseInsensitive;

		// Token: 0x040000B6 RID: 182
		private bool compareAsCollection;

		// Token: 0x040000B7 RID: 183
		private List<EqualityAdapter> externalComparers = new List<EqualityAdapter>();

		// Token: 0x040000B8 RID: 184
		private List<NUnitEqualityComparer.FailurePoint> failurePoints;

		// Token: 0x040000B9 RID: 185
		private static readonly int BUFFER_SIZE = 4096;

		// Token: 0x040000BA RID: 186
		private static readonly Type GameObjectType = Type.GetType("UnityEngine.Object, UnityEngine, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");

		// Token: 0x040000BB RID: 187
		[CompilerGenerated]
		private bool <WithSameOffset>k__BackingField;

		// Token: 0x02000076 RID: 118
		public class FailurePoint
		{
			// Token: 0x0600044B RID: 1099 RVA: 0x0000E6DA File Offset: 0x0000C8DA
			public FailurePoint()
			{
			}

			// Token: 0x040000BC RID: 188
			public long Position;

			// Token: 0x040000BD RID: 189
			public object ExpectedValue;

			// Token: 0x040000BE RID: 190
			public object ActualValue;

			// Token: 0x040000BF RID: 191
			public bool ExpectedHasData;

			// Token: 0x040000C0 RID: 192
			public bool ActualHasData;
		}
	}
}
