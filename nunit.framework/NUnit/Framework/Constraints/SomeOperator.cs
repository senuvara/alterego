﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000182 RID: 386
	public class SomeOperator : CollectionOperator
	{
		// Token: 0x06000A27 RID: 2599 RVA: 0x0002216C File Offset: 0x0002036C
		public override IConstraint ApplyPrefix(IConstraint constraint)
		{
			return new SomeItemsConstraint(constraint);
		}

		// Token: 0x06000A28 RID: 2600 RVA: 0x00022184 File Offset: 0x00020384
		public SomeOperator()
		{
		}
	}
}
