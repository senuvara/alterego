﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200015F RID: 351
	public class CollectionSupersetConstraint : CollectionItemsEqualConstraint
	{
		// Token: 0x06000929 RID: 2345 RVA: 0x0001EE8F File Offset: 0x0001D08F
		public CollectionSupersetConstraint(IEnumerable expected) : base(expected)
		{
			this._expected = expected;
		}

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x0600092A RID: 2346 RVA: 0x0001EEA4 File Offset: 0x0001D0A4
		public override string DisplayName
		{
			get
			{
				return "SupersetOf";
			}
		}

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x0600092B RID: 2347 RVA: 0x0001EEBC File Offset: 0x0001D0BC
		public override string Description
		{
			get
			{
				return "superset of " + MsgUtils.FormatValue(this._expected);
			}
		}

		// Token: 0x0600092C RID: 2348 RVA: 0x0001EEE4 File Offset: 0x0001D0E4
		protected override bool Matches(IEnumerable actual)
		{
			return base.Tally(actual).TryRemove(this._expected);
		}

		// Token: 0x0600092D RID: 2349 RVA: 0x0001EF30 File Offset: 0x0001D130
		public CollectionSupersetConstraint Using<TSupersetType, TSubsetType>(Func<TSupersetType, TSubsetType, bool> comparison)
		{
			Func<TSubsetType, TSupersetType, bool> comparison2 = (TSubsetType actual, TSupersetType expected) => comparison(expected, actual);
			base.Using(EqualityAdapter.For<TSubsetType, TSupersetType>(comparison2));
			return this;
		}

		// Token: 0x04000208 RID: 520
		private IEnumerable _expected;

		// Token: 0x020001C2 RID: 450
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1<TSupersetType, TSubsetType>
		{
			// Token: 0x06000B5B RID: 2907 RVA: 0x0001EF08 File Offset: 0x0001D108
			public <>c__DisplayClass1()
			{
			}

			// Token: 0x06000B5C RID: 2908 RVA: 0x0001EF10 File Offset: 0x0001D110
			public bool <Using>b__0(TSubsetType actual, TSupersetType expected)
			{
				return this.comparison(expected, actual);
			}

			// Token: 0x040002E8 RID: 744
			public Func<TSupersetType, TSubsetType, bool> comparison;
		}
	}
}
