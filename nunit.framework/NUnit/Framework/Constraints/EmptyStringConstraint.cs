﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000104 RID: 260
	public class EmptyStringConstraint : StringConstraint
	{
		// Token: 0x17000199 RID: 409
		// (get) Token: 0x0600073C RID: 1852 RVA: 0x0001A10C File Offset: 0x0001830C
		public override string Description
		{
			get
			{
				return "<empty>";
			}
		}

		// Token: 0x0600073D RID: 1853 RVA: 0x0001A124 File Offset: 0x00018324
		protected override bool Matches(string actual)
		{
			return actual == string.Empty;
		}

		// Token: 0x0600073E RID: 1854 RVA: 0x0001A141 File Offset: 0x00018341
		public EmptyStringConstraint()
		{
		}
	}
}
