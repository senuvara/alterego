﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000098 RID: 152
	public class CollectionContainsConstraint : CollectionItemsEqualConstraint
	{
		// Token: 0x060004C7 RID: 1223 RVA: 0x0000FD91 File Offset: 0x0000DF91
		public CollectionContainsConstraint(object expected) : base(expected)
		{
			this.Expected = expected;
		}

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x060004C8 RID: 1224 RVA: 0x0000FDA8 File Offset: 0x0000DFA8
		public override string DisplayName
		{
			get
			{
				return "Contains";
			}
		}

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x060004C9 RID: 1225 RVA: 0x0000FDC0 File Offset: 0x0000DFC0
		public override string Description
		{
			get
			{
				return "collection containing " + MsgUtils.FormatValue(this.Expected);
			}
		}

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x060004CA RID: 1226 RVA: 0x0000FDE8 File Offset: 0x0000DFE8
		// (set) Token: 0x060004CB RID: 1227 RVA: 0x0000FDFF File Offset: 0x0000DFFF
		private protected object Expected
		{
			[CompilerGenerated]
			protected get
			{
				return this.<Expected>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Expected>k__BackingField = value;
			}
		}

		// Token: 0x060004CC RID: 1228 RVA: 0x0000FE08 File Offset: 0x0000E008
		protected override bool Matches(IEnumerable actual)
		{
			foreach (object x in actual)
			{
				if (base.ItemsEqual(x, this.Expected))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060004CD RID: 1229 RVA: 0x0000FEA4 File Offset: 0x0000E0A4
		public CollectionContainsConstraint Using<TCollectionType, TMemberType>(Func<TCollectionType, TMemberType, bool> comparison)
		{
			Func<TMemberType, TCollectionType, bool> comparison2 = (TMemberType actual, TCollectionType expected) => comparison(expected, actual);
			base.Using(EqualityAdapter.For<TMemberType, TCollectionType>(comparison2));
			return this;
		}

		// Token: 0x040000E3 RID: 227
		[CompilerGenerated]
		private object <Expected>k__BackingField;

		// Token: 0x020001B1 RID: 433
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1<TCollectionType, TMemberType>
		{
			// Token: 0x06000B2A RID: 2858 RVA: 0x0000FE7C File Offset: 0x0000E07C
			public <>c__DisplayClass1()
			{
			}

			// Token: 0x06000B2B RID: 2859 RVA: 0x0000FE84 File Offset: 0x0000E084
			public bool <Using>b__0(TMemberType actual, TCollectionType expected)
			{
				return this.comparison(expected, actual);
			}

			// Token: 0x040002C5 RID: 709
			public Func<TCollectionType, TMemberType, bool> comparison;
		}
	}
}
