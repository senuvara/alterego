﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000097 RID: 151
	public class NaNConstraint : Constraint
	{
		// Token: 0x170000FD RID: 253
		// (get) Token: 0x060004C4 RID: 1220 RVA: 0x0000FD24 File Offset: 0x0000DF24
		public override string Description
		{
			get
			{
				return "NaN";
			}
		}

		// Token: 0x060004C5 RID: 1221 RVA: 0x0000FD3C File Offset: 0x0000DF3C
		public override ConstraintResult ApplyTo(object actual)
		{
			return new ConstraintResult(this, actual, (actual is double && double.IsNaN((double)actual)) || (actual is float && float.IsNaN((float)actual)));
		}

		// Token: 0x060004C6 RID: 1222 RVA: 0x0000FD83 File Offset: 0x0000DF83
		public NaNConstraint() : base(new object[0])
		{
		}
	}
}
