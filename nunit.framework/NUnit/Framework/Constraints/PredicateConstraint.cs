﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000EA RID: 234
	public class PredicateConstraint<T> : Constraint
	{
		// Token: 0x060006B2 RID: 1714 RVA: 0x000184CB File Offset: 0x000166CB
		public PredicateConstraint(Predicate<T> predicate) : base(new object[0])
		{
			this.predicate = predicate;
		}

		// Token: 0x1700017D RID: 381
		// (get) Token: 0x060006B3 RID: 1715 RVA: 0x000184E4 File Offset: 0x000166E4
		public override string Description
		{
			get
			{
				string name = this.predicate.Method.Name;
				return name.StartsWith("<") ? "value matching lambda expression" : ("value matching " + name);
			}
		}

		// Token: 0x060006B4 RID: 1716 RVA: 0x00018528 File Offset: 0x00016728
		public override ConstraintResult ApplyTo(object actual)
		{
			if (!(actual is T))
			{
				throw new ArgumentException("The actual value is not of type " + typeof(T).Name, "actual");
			}
			return new ConstraintResult(this, actual, this.predicate((T)((object)actual)));
		}

		// Token: 0x0400016D RID: 365
		private readonly Predicate<T> predicate;
	}
}
