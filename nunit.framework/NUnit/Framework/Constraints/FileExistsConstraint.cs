﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200006B RID: 107
	[Obsolete("FileExistsConstraint is deprecated, please use FileOrDirectoryExistsConstraint instead.")]
	public class FileExistsConstraint : FileOrDirectoryExistsConstraint
	{
		// Token: 0x0600040A RID: 1034 RVA: 0x0000CF4F File Offset: 0x0000B14F
		public FileExistsConstraint() : base(true)
		{
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x0600040B RID: 1035 RVA: 0x0000CF5C File Offset: 0x0000B15C
		public override string Description
		{
			get
			{
				return "file exists";
			}
		}
	}
}
