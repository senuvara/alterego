﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020001A2 RID: 418
	public class NotOperator : PrefixOperator
	{
		// Token: 0x06000ACC RID: 2764 RVA: 0x00024218 File Offset: 0x00022418
		public NotOperator()
		{
			this.left_precedence = (this.right_precedence = 1);
		}

		// Token: 0x06000ACD RID: 2765 RVA: 0x00024240 File Offset: 0x00022440
		public override IConstraint ApplyPrefix(IConstraint constraint)
		{
			return new NotConstraint(constraint);
		}
	}
}
