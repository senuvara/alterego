﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000093 RID: 147
	public class ExceptionTypeConstraint : ExactTypeConstraint
	{
		// Token: 0x060004B6 RID: 1206 RVA: 0x0000F916 File Offset: 0x0000DB16
		public ExceptionTypeConstraint(Type type) : base(type)
		{
		}

		// Token: 0x060004B7 RID: 1207 RVA: 0x0000F924 File Offset: 0x0000DB24
		public override ConstraintResult ApplyTo(object actual)
		{
			Exception ex = actual as Exception;
			if (actual != null && ex == null)
			{
				throw new ArgumentException("Actual value must be an Exception", "actual");
			}
			this.actualType = ((actual == null) ? null : actual.GetType());
			return new ExceptionTypeConstraint.ExceptionTypeConstraintResult(this, actual, this.actualType, this.Matches(actual));
		}

		// Token: 0x02000094 RID: 148
		private class ExceptionTypeConstraintResult : ConstraintResult
		{
			// Token: 0x060004B8 RID: 1208 RVA: 0x0000F984 File Offset: 0x0000DB84
			public ExceptionTypeConstraintResult(ExceptionTypeConstraint constraint, object caughtException, Type type, bool matches) : base(constraint, type, matches)
			{
				this.caughtException = caughtException;
			}

			// Token: 0x060004B9 RID: 1209 RVA: 0x0000F99C File Offset: 0x0000DB9C
			public override void WriteActualValueTo(MessageWriter writer)
			{
				if (base.Status == ConstraintStatus.Failure)
				{
					Exception ex = this.caughtException as Exception;
					if (ex == null)
					{
						base.WriteActualValueTo(writer);
					}
					else
					{
						writer.WriteActualValue(ex);
					}
				}
			}

			// Token: 0x040000DF RID: 223
			private readonly object caughtException;
		}
	}
}
