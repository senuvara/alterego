﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000159 RID: 345
	public class LessThanOrEqualConstraint : ComparisonConstraint
	{
		// Token: 0x06000904 RID: 2308 RVA: 0x0001E9B2 File Offset: 0x0001CBB2
		public LessThanOrEqualConstraint(object expected) : base(expected, true, true, false, "less than or equal to")
		{
		}
	}
}
