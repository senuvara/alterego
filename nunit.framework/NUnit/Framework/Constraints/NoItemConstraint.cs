﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000030 RID: 48
	public class NoItemConstraint : PrefixConstraint
	{
		// Token: 0x06000199 RID: 409 RVA: 0x000061A8 File Offset: 0x000043A8
		public NoItemConstraint(IConstraint itemConstraint) : base(itemConstraint)
		{
			base.DescriptionPrefix = "no item";
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x0600019A RID: 410 RVA: 0x000061C0 File Offset: 0x000043C0
		public override string DisplayName
		{
			get
			{
				return "None";
			}
		}

		// Token: 0x0600019B RID: 411 RVA: 0x000061D8 File Offset: 0x000043D8
		public override ConstraintResult ApplyTo(object actual)
		{
			if (!(actual is IEnumerable))
			{
				throw new ArgumentException("The actual value must be an IEnumerable", "actual");
			}
			foreach (object actual2 in ((IEnumerable)actual))
			{
				if (base.BaseConstraint.ApplyTo(actual2).IsSuccess)
				{
					return new ConstraintResult(this, actual, ConstraintStatus.Failure);
				}
			}
			return new ConstraintResult(this, actual, ConstraintStatus.Success);
		}
	}
}
