﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000096 RID: 150
	public class PropOperator : SelfResolvingOperator
	{
		// Token: 0x170000FC RID: 252
		// (get) Token: 0x060004C1 RID: 1217 RVA: 0x0000FC80 File Offset: 0x0000DE80
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x060004C2 RID: 1218 RVA: 0x0000FC98 File Offset: 0x0000DE98
		public PropOperator(string name)
		{
			this.name = name;
			this.left_precedence = (this.right_precedence = 1);
		}

		// Token: 0x060004C3 RID: 1219 RVA: 0x0000FCC8 File Offset: 0x0000DEC8
		public override void Reduce(ConstraintBuilder.ConstraintStack stack)
		{
			if (base.RightContext == null || base.RightContext is BinaryOperator)
			{
				stack.Push(new PropertyExistsConstraint(this.name));
			}
			else
			{
				stack.Push(new PropertyConstraint(this.name, stack.Pop()));
			}
		}

		// Token: 0x040000E2 RID: 226
		private readonly string name;
	}
}
