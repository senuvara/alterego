﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200002B RID: 43
	public abstract class CollectionConstraint : Constraint
	{
		// Token: 0x0600017D RID: 381 RVA: 0x00005B41 File Offset: 0x00003D41
		protected CollectionConstraint() : base(new object[0])
		{
		}

		// Token: 0x0600017E RID: 382 RVA: 0x00005B54 File Offset: 0x00003D54
		protected CollectionConstraint(object arg) : base(new object[]
		{
			arg
		})
		{
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00005B78 File Offset: 0x00003D78
		protected static bool IsEmpty(IEnumerable enumerable)
		{
			ICollection collection = enumerable as ICollection;
			bool result;
			if (collection != null)
			{
				result = (collection.Count == 0);
			}
			else
			{
				using (IEnumerator enumerator = enumerable.GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						return false;
					}
				}
				result = true;
			}
			return result;
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00005BF8 File Offset: 0x00003DF8
		public override ConstraintResult ApplyTo(object actual)
		{
			IEnumerable enumerable = actual as IEnumerable;
			if (enumerable == null)
			{
				throw new ArgumentException("The actual value must be an IEnumerable", "actual");
			}
			return new ConstraintResult(this, actual, this.Matches(enumerable));
		}

		// Token: 0x06000181 RID: 385
		protected abstract bool Matches(IEnumerable collection);
	}
}
