﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000185 RID: 389
	public class GreaterThanOrEqualConstraint : ComparisonConstraint
	{
		// Token: 0x06000A3A RID: 2618 RVA: 0x00022473 File Offset: 0x00020673
		public GreaterThanOrEqualConstraint(object expected) : base(expected, false, true, true, "greater than or equal to")
		{
		}
	}
}
