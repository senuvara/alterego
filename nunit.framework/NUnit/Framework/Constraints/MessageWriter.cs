﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000101 RID: 257
	public abstract class MessageWriter : StringWriter
	{
		// Token: 0x0600072A RID: 1834 RVA: 0x00019F6A File Offset: 0x0001816A
		protected MessageWriter() : base(CultureInfo.InvariantCulture)
		{
		}

		// Token: 0x17000198 RID: 408
		// (get) Token: 0x0600072B RID: 1835
		// (set) Token: 0x0600072C RID: 1836
		public abstract int MaxLineLength { get; set; }

		// Token: 0x0600072D RID: 1837 RVA: 0x00019F7A File Offset: 0x0001817A
		public void WriteMessageLine(string message, params object[] args)
		{
			this.WriteMessageLine(0, message, args);
		}

		// Token: 0x0600072E RID: 1838
		public abstract void WriteMessageLine(int level, string message, params object[] args);

		// Token: 0x0600072F RID: 1839
		public abstract void DisplayDifferences(ConstraintResult result);

		// Token: 0x06000730 RID: 1840
		public abstract void DisplayDifferences(object expected, object actual);

		// Token: 0x06000731 RID: 1841
		public abstract void DisplayDifferences(object expected, object actual, Tolerance tolerance);

		// Token: 0x06000732 RID: 1842
		public abstract void DisplayStringDifferences(string expected, string actual, int mismatch, bool ignoreCase, bool clipping);

		// Token: 0x06000733 RID: 1843
		public abstract void WriteActualValue(object actual);

		// Token: 0x06000734 RID: 1844
		public abstract void WriteValue(object val);

		// Token: 0x06000735 RID: 1845
		public abstract void WriteCollectionElements(IEnumerable collection, long start, int max);
	}
}
