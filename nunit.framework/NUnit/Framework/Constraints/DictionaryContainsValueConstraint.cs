﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000C7 RID: 199
	public class DictionaryContainsValueConstraint : CollectionContainsConstraint
	{
		// Token: 0x0600061C RID: 1564 RVA: 0x00015215 File Offset: 0x00013415
		public DictionaryContainsValueConstraint(object expected) : base(expected)
		{
		}

		// Token: 0x1700015E RID: 350
		// (get) Token: 0x0600061D RID: 1565 RVA: 0x00015224 File Offset: 0x00013424
		public override string DisplayName
		{
			get
			{
				return "ContainsValue";
			}
		}

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x0600061E RID: 1566 RVA: 0x0001523C File Offset: 0x0001343C
		public override string Description
		{
			get
			{
				return "dictionary containing value " + MsgUtils.FormatValue(base.Expected);
			}
		}

		// Token: 0x0600061F RID: 1567 RVA: 0x00015264 File Offset: 0x00013464
		protected override bool Matches(IEnumerable actual)
		{
			IDictionary dictionary = actual as IDictionary;
			if (dictionary == null)
			{
				throw new ArgumentException("The actual value must be an IDictionary", "actual");
			}
			return base.Matches(dictionary.Values);
		}
	}
}
