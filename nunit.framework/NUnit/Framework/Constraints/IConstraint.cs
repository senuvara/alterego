﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200000A RID: 10
	public interface IConstraint : IResolveConstraint
	{
		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000040 RID: 64
		string DisplayName { get; }

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000041 RID: 65
		string Description { get; }

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000042 RID: 66
		object[] Arguments { get; }

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000043 RID: 67
		// (set) Token: 0x06000044 RID: 68
		ConstraintBuilder Builder { get; set; }

		// Token: 0x06000045 RID: 69
		ConstraintResult ApplyTo(object actual);

		// Token: 0x06000046 RID: 70
		ConstraintResult ApplyTo<TActual>(ActualValueDelegate<TActual> del);

		// Token: 0x06000047 RID: 71
		ConstraintResult ApplyTo<TActual>(ref TActual actual);
	}
}
