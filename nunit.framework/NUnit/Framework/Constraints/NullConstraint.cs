﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000077 RID: 119
	public class NullConstraint : Constraint
	{
		// Token: 0x0600044C RID: 1100 RVA: 0x0000E6E2 File Offset: 0x0000C8E2
		public NullConstraint() : base(new object[0])
		{
			this.Description = "null";
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x0000E700 File Offset: 0x0000C900
		public override ConstraintResult ApplyTo(object actual)
		{
			NUnitEqualityComparer.CheckGameObjectReference<object>(ref actual);
			return new ConstraintResult(this, actual, actual == null);
		}
	}
}
