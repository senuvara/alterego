﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200017D RID: 381
	public enum ConstraintStatus
	{
		// Token: 0x0400024A RID: 586
		Unknown,
		// Token: 0x0400024B RID: 587
		Success,
		// Token: 0x0400024C RID: 588
		Failure,
		// Token: 0x0400024D RID: 589
		Error
	}
}
