﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000EC RID: 236
	// (Invoke) Token: 0x060006B8 RID: 1720
	public delegate string ValueFormatter(object val);
}
