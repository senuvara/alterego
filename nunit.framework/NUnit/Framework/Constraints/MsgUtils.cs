﻿using System;
using System.Collections;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000EE RID: 238
	internal static class MsgUtils
	{
		// Token: 0x1700017E RID: 382
		// (get) Token: 0x060006BF RID: 1727 RVA: 0x000185D0 File Offset: 0x000167D0
		// (set) Token: 0x060006C0 RID: 1728 RVA: 0x000185E6 File Offset: 0x000167E6
		public static ValueFormatter DefaultValueFormatter
		{
			[CompilerGenerated]
			get
			{
				return MsgUtils.<DefaultValueFormatter>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				MsgUtils.<DefaultValueFormatter>k__BackingField = value;
			}
		} = (object val) => string.Format(MsgUtils.Fmt_Default, val);

		// Token: 0x060006C1 RID: 1729 RVA: 0x00018A24 File Offset: 0x00016C24
		static MsgUtils()
		{
			MsgUtils.AddFormatter((ValueFormatter next) => (object val) => (val is ValueType) ? string.Format(MsgUtils.Fmt_ValueType, val) : next(val));
			MsgUtils.AddFormatter((ValueFormatter next) => (object val) => (val is DateTime) ? MsgUtils.FormatDateTime((DateTime)val) : next(val));
			MsgUtils.AddFormatter((ValueFormatter next) => (object val) => (val is DateTimeOffset) ? MsgUtils.FormatDateTimeOffset((DateTimeOffset)val) : next(val));
			MsgUtils.AddFormatter((ValueFormatter next) => (object val) => (val is decimal) ? MsgUtils.FormatDecimal((decimal)val) : next(val));
			MsgUtils.AddFormatter((ValueFormatter next) => (object val) => (val is float) ? MsgUtils.FormatFloat((float)val) : next(val));
			MsgUtils.AddFormatter((ValueFormatter next) => (object val) => (val is double) ? MsgUtils.FormatDouble((double)val) : next(val));
			MsgUtils.AddFormatter((ValueFormatter next) => (object val) => (val is char) ? string.Format(MsgUtils.Fmt_Char, val) : next(val));
			MsgUtils.AddFormatter((ValueFormatter next) => (object val) => (val is IEnumerable) ? MsgUtils.FormatCollection((IEnumerable)val, 0L, 10) : next(val));
			MsgUtils.AddFormatter((ValueFormatter next) => (object val) => (val is string) ? MsgUtils.FormatString((string)val) : next(val));
			MsgUtils.AddFormatter((ValueFormatter next) => (object val) => val.GetType().IsArray ? MsgUtils.FormatArray((Array)val) : next(val));
		}

		// Token: 0x060006C2 RID: 1730 RVA: 0x00018C24 File Offset: 0x00016E24
		public static void AddFormatter(ValueFormatterFactory formatterFactory)
		{
			MsgUtils.DefaultValueFormatter = formatterFactory(MsgUtils.DefaultValueFormatter);
		}

		// Token: 0x060006C3 RID: 1731 RVA: 0x00018C38 File Offset: 0x00016E38
		public static string FormatValue(object val)
		{
			string result;
			if (val == null)
			{
				result = MsgUtils.Fmt_Null;
			}
			else
			{
				ITestExecutionContext currentContext = TestExecutionContext.CurrentContext;
				if (currentContext != null)
				{
					result = currentContext.CurrentValueFormatter(val);
				}
				else
				{
					result = MsgUtils.DefaultValueFormatter(val);
				}
			}
			return result;
		}

		// Token: 0x060006C4 RID: 1732 RVA: 0x00018C88 File Offset: 0x00016E88
		public static string FormatCollection(IEnumerable collection, long start, int max)
		{
			int num = 0;
			int num2 = 0;
			StringBuilder stringBuilder = new StringBuilder();
			foreach (object val in collection)
			{
				if ((long)num2++ >= start)
				{
					if (++num > max)
					{
						break;
					}
					stringBuilder.Append((num == 1) ? "< " : ", ");
					stringBuilder.Append(MsgUtils.FormatValue(val));
				}
			}
			string result;
			if (num == 0)
			{
				result = MsgUtils.Fmt_EmptyCollection;
			}
			else
			{
				if (num > max)
				{
					stringBuilder.Append("...");
				}
				stringBuilder.Append(" >");
				result = stringBuilder.ToString();
			}
			return result;
		}

		// Token: 0x060006C5 RID: 1733 RVA: 0x00018D84 File Offset: 0x00016F84
		private static string FormatArray(Array array)
		{
			string result;
			if (array.Length == 0)
			{
				result = MsgUtils.Fmt_EmptyCollection;
			}
			else
			{
				int rank = array.Rank;
				int[] array2 = new int[rank];
				int num = 1;
				int i = rank;
				while (--i >= 0)
				{
					num = (array2[i] = num * array.GetLength(i));
				}
				int num2 = 0;
				StringBuilder stringBuilder = new StringBuilder();
				foreach (object val in array)
				{
					if (num2 > 0)
					{
						stringBuilder.Append(", ");
					}
					bool flag = false;
					for (i = 0; i < rank; i++)
					{
						flag = (flag || num2 % array2[i] == 0);
						if (flag)
						{
							stringBuilder.Append("< ");
						}
					}
					stringBuilder.Append(MsgUtils.FormatValue(val));
					num2++;
					bool flag2 = false;
					for (i = 0; i < rank; i++)
					{
						flag2 = (flag2 || num2 % array2[i] == 0);
						if (flag2)
						{
							stringBuilder.Append(" >");
						}
					}
				}
				result = stringBuilder.ToString();
			}
			return result;
		}

		// Token: 0x060006C6 RID: 1734 RVA: 0x00018F08 File Offset: 0x00017108
		private static string FormatString(string s)
		{
			return (s == string.Empty) ? MsgUtils.Fmt_EmptyString : string.Format(MsgUtils.Fmt_String, s);
		}

		// Token: 0x060006C7 RID: 1735 RVA: 0x00018F3C File Offset: 0x0001713C
		private static string FormatDouble(double d)
		{
			string result;
			if (double.IsNaN(d) || double.IsInfinity(d))
			{
				result = d.ToString();
			}
			else
			{
				string text = d.ToString("G17", CultureInfo.InvariantCulture);
				if (text.IndexOf('.') > 0)
				{
					result = text + "d";
				}
				else
				{
					result = text + ".0d";
				}
			}
			return result;
		}

		// Token: 0x060006C8 RID: 1736 RVA: 0x00018FB0 File Offset: 0x000171B0
		private static string FormatFloat(float f)
		{
			string result;
			if (float.IsNaN(f) || float.IsInfinity(f))
			{
				result = f.ToString();
			}
			else
			{
				string text = f.ToString("G9", CultureInfo.InvariantCulture);
				if (text.IndexOf('.') > 0)
				{
					result = text + "f";
				}
				else
				{
					result = text + ".0f";
				}
			}
			return result;
		}

		// Token: 0x060006C9 RID: 1737 RVA: 0x00019024 File Offset: 0x00017224
		private static string FormatDecimal(decimal d)
		{
			return d.ToString("G29", CultureInfo.InvariantCulture) + "m";
		}

		// Token: 0x060006CA RID: 1738 RVA: 0x00019054 File Offset: 0x00017254
		private static string FormatDateTime(DateTime dt)
		{
			return dt.ToString(MsgUtils.Fmt_DateTime, CultureInfo.InvariantCulture);
		}

		// Token: 0x060006CB RID: 1739 RVA: 0x00019078 File Offset: 0x00017278
		private static string FormatDateTimeOffset(DateTimeOffset dto)
		{
			return dto.ToString(MsgUtils.Fmt_DateTimeOffset, CultureInfo.InvariantCulture);
		}

		// Token: 0x060006CC RID: 1740 RVA: 0x0001909C File Offset: 0x0001729C
		public static string GetTypeRepresentation(object obj)
		{
			Array array = obj as Array;
			string result;
			if (array == null)
			{
				result = string.Format("<{0}>", obj.GetType());
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				Type type = array.GetType();
				int num = 0;
				while (type.IsArray)
				{
					type = type.GetElementType();
					num++;
				}
				stringBuilder.Append(type.ToString());
				stringBuilder.Append('[');
				for (int i = 0; i < array.Rank; i++)
				{
					if (i > 0)
					{
						stringBuilder.Append(',');
					}
					stringBuilder.Append(array.GetLength(i));
				}
				stringBuilder.Append(']');
				while (--num > 0)
				{
					stringBuilder.Append("[]");
				}
				result = string.Format("<{0}>", stringBuilder.ToString());
			}
			return result;
		}

		// Token: 0x060006CD RID: 1741 RVA: 0x00019194 File Offset: 0x00017394
		public static string EscapeControlChars(string s)
		{
			if (s != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				string text = s;
				int i = 0;
				while (i < text.Length)
				{
					char c = text[i];
					char c2 = c;
					if (c2 <= '\\')
					{
						switch (c2)
						{
						case '\0':
							stringBuilder.Append("\\0");
							break;
						case '\u0001':
						case '\u0002':
						case '\u0003':
						case '\u0004':
						case '\u0005':
						case '\u0006':
							goto IL_140;
						case '\a':
							stringBuilder.Append("\\a");
							break;
						case '\b':
							stringBuilder.Append("\\b");
							break;
						case '\t':
							stringBuilder.Append("\\t");
							break;
						case '\n':
							stringBuilder.Append("\\n");
							break;
						case '\v':
							stringBuilder.Append("\\v");
							break;
						case '\f':
							stringBuilder.Append("\\f");
							break;
						case '\r':
							stringBuilder.Append("\\r");
							break;
						default:
							if (c2 != '\\')
							{
								goto IL_140;
							}
							stringBuilder.Append("\\\\");
							break;
						}
					}
					else
					{
						if (c2 != '\u0085')
						{
							switch (c2)
							{
							case '\u2028':
							case '\u2029':
								break;
							default:
								goto IL_140;
							}
						}
						stringBuilder.Append(string.Format("\\x{0:X4}", (int)c));
					}
					IL_14A:
					i++;
					continue;
					IL_140:
					stringBuilder.Append(c);
					goto IL_14A;
				}
				s = stringBuilder.ToString();
			}
			return s;
		}

		// Token: 0x060006CE RID: 1742 RVA: 0x00019314 File Offset: 0x00017514
		public static string EscapeNullCharacters(string s)
		{
			if (s != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				foreach (char c in s)
				{
					char c2 = c;
					if (c2 != '\0')
					{
						stringBuilder.Append(c);
					}
					else
					{
						stringBuilder.Append("\\0");
					}
				}
				s = stringBuilder.ToString();
			}
			return s;
		}

		// Token: 0x060006CF RID: 1743 RVA: 0x0001938C File Offset: 0x0001758C
		public static string GetArrayIndicesAsString(int[] indices)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append('[');
			for (int i = 0; i < indices.Length; i++)
			{
				if (i > 0)
				{
					stringBuilder.Append(',');
				}
				stringBuilder.Append(indices[i].ToString());
			}
			stringBuilder.Append(']');
			return stringBuilder.ToString();
		}

		// Token: 0x060006D0 RID: 1744 RVA: 0x000193F8 File Offset: 0x000175F8
		public static int[] GetArrayIndicesFromCollectionIndex(IEnumerable collection, long index)
		{
			Array array = collection as Array;
			int num = (array == null) ? 1 : array.Rank;
			int[] array2 = new int[num];
			int num2 = num;
			while (--num2 > 0)
			{
				int length = array.GetLength(num2);
				array2[num2] = (int)index % length;
				index /= (long)length;
			}
			array2[0] = (int)index;
			return array2;
		}

		// Token: 0x060006D1 RID: 1745 RVA: 0x0001945C File Offset: 0x0001765C
		public static string ClipString(string s, int maxStringLength, int clipStart)
		{
			int num = maxStringLength;
			StringBuilder stringBuilder = new StringBuilder();
			if (clipStart > 0)
			{
				num -= "...".Length;
				stringBuilder.Append("...");
			}
			if (s.Length - clipStart > num)
			{
				num -= "...".Length;
				stringBuilder.Append(s.Substring(clipStart, num));
				stringBuilder.Append("...");
			}
			else if (clipStart > 0)
			{
				stringBuilder.Append(s.Substring(clipStart));
			}
			else
			{
				stringBuilder.Append(s);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060006D2 RID: 1746 RVA: 0x00019504 File Offset: 0x00017704
		public static void ClipExpectedAndActual(ref string expected, ref string actual, int maxDisplayLength, int mismatch)
		{
			int num = Math.Max(expected.Length, actual.Length);
			if (num > maxDisplayLength)
			{
				int num2 = maxDisplayLength - "...".Length;
				int num3 = num - num2;
				if (num3 > mismatch)
				{
					num3 = Math.Max(0, mismatch - num2 / 2);
				}
				expected = MsgUtils.ClipString(expected, maxDisplayLength, num3);
				actual = MsgUtils.ClipString(actual, maxDisplayLength, num3);
			}
		}

		// Token: 0x060006D3 RID: 1747 RVA: 0x00019570 File Offset: 0x00017770
		public static int FindMismatchPosition(string expected, string actual, int istart, bool ignoreCase)
		{
			int num = Math.Min(expected.Length, actual.Length);
			string text = ignoreCase ? expected.ToLower() : expected;
			string text2 = ignoreCase ? actual.ToLower() : actual;
			for (int i = istart; i < num; i++)
			{
				if (text[i] != text2[i])
				{
					return i;
				}
			}
			if (expected.Length != actual.Length)
			{
				return num;
			}
			return -1;
		}

		// Token: 0x060006D4 RID: 1748 RVA: 0x00018640 File Offset: 0x00016840
		[CompilerGenerated]
		private static string <.cctor>b__0(object val)
		{
			return string.Format(MsgUtils.Fmt_Default, val);
		}

		// Token: 0x060006D5 RID: 1749 RVA: 0x00018690 File Offset: 0x00016890
		[CompilerGenerated]
		private static ValueFormatter <.cctor>b__1(ValueFormatter next)
		{
			return (object val) => (val is ValueType) ? string.Format(MsgUtils.Fmt_ValueType, val) : next(val);
		}

		// Token: 0x060006D6 RID: 1750 RVA: 0x000186F0 File Offset: 0x000168F0
		[CompilerGenerated]
		private static ValueFormatter <.cctor>b__3(ValueFormatter next)
		{
			return (object val) => (val is DateTime) ? MsgUtils.FormatDateTime((DateTime)val) : next(val);
		}

		// Token: 0x060006D7 RID: 1751 RVA: 0x00018750 File Offset: 0x00016950
		[CompilerGenerated]
		private static ValueFormatter <.cctor>b__5(ValueFormatter next)
		{
			return (object val) => (val is DateTimeOffset) ? MsgUtils.FormatDateTimeOffset((DateTimeOffset)val) : next(val);
		}

		// Token: 0x060006D8 RID: 1752 RVA: 0x000187B0 File Offset: 0x000169B0
		[CompilerGenerated]
		private static ValueFormatter <.cctor>b__7(ValueFormatter next)
		{
			return (object val) => (val is decimal) ? MsgUtils.FormatDecimal((decimal)val) : next(val);
		}

		// Token: 0x060006D9 RID: 1753 RVA: 0x00018810 File Offset: 0x00016A10
		[CompilerGenerated]
		private static ValueFormatter <.cctor>b__9(ValueFormatter next)
		{
			return (object val) => (val is float) ? MsgUtils.FormatFloat((float)val) : next(val);
		}

		// Token: 0x060006DA RID: 1754 RVA: 0x00018870 File Offset: 0x00016A70
		[CompilerGenerated]
		private static ValueFormatter <.cctor>b__b(ValueFormatter next)
		{
			return (object val) => (val is double) ? MsgUtils.FormatDouble((double)val) : next(val);
		}

		// Token: 0x060006DB RID: 1755 RVA: 0x000188D0 File Offset: 0x00016AD0
		[CompilerGenerated]
		private static ValueFormatter <.cctor>b__d(ValueFormatter next)
		{
			return (object val) => (val is char) ? string.Format(MsgUtils.Fmt_Char, val) : next(val);
		}

		// Token: 0x060006DC RID: 1756 RVA: 0x00018934 File Offset: 0x00016B34
		[CompilerGenerated]
		private static ValueFormatter <.cctor>b__f(ValueFormatter next)
		{
			return (object val) => (val is IEnumerable) ? MsgUtils.FormatCollection((IEnumerable)val, 0L, 10) : next(val);
		}

		// Token: 0x060006DD RID: 1757 RVA: 0x00018994 File Offset: 0x00016B94
		[CompilerGenerated]
		private static ValueFormatter <.cctor>b__11(ValueFormatter next)
		{
			return (object val) => (val is string) ? MsgUtils.FormatString((string)val) : next(val);
		}

		// Token: 0x060006DE RID: 1758 RVA: 0x000189F8 File Offset: 0x00016BF8
		[CompilerGenerated]
		private static ValueFormatter <.cctor>b__13(ValueFormatter next)
		{
			return (object val) => val.GetType().IsArray ? MsgUtils.FormatArray((Array)val) : next(val);
		}

		// Token: 0x0400016E RID: 366
		private const string ELLIPSIS = "...";

		// Token: 0x0400016F RID: 367
		private static readonly string Fmt_Null = "null";

		// Token: 0x04000170 RID: 368
		private static readonly string Fmt_EmptyString = "<string.Empty>";

		// Token: 0x04000171 RID: 369
		private static readonly string Fmt_EmptyCollection = "<empty>";

		// Token: 0x04000172 RID: 370
		private static readonly string Fmt_String = "\"{0}\"";

		// Token: 0x04000173 RID: 371
		private static readonly string Fmt_Char = "'{0}'";

		// Token: 0x04000174 RID: 372
		private static readonly string Fmt_DateTime = "yyyy-MM-dd HH:mm:ss.fff";

		// Token: 0x04000175 RID: 373
		private static readonly string Fmt_DateTimeOffset = "yyyy-MM-dd HH:mm:ss.fffzzz";

		// Token: 0x04000176 RID: 374
		private static readonly string Fmt_ValueType = "{0}";

		// Token: 0x04000177 RID: 375
		private static readonly string Fmt_Default = "<{0}>";

		// Token: 0x04000178 RID: 376
		[CompilerGenerated]
		private static ValueFormatter <DefaultValueFormatter>k__BackingField;

		// Token: 0x04000179 RID: 377
		[CompilerGenerated]
		private static ValueFormatter CS$<>9__CachedAnonymousMethodDelegate15;

		// Token: 0x0400017A RID: 378
		[CompilerGenerated]
		private static ValueFormatterFactory CS$<>9__CachedAnonymousMethodDelegate16;

		// Token: 0x0400017B RID: 379
		[CompilerGenerated]
		private static ValueFormatterFactory CS$<>9__CachedAnonymousMethodDelegate17;

		// Token: 0x0400017C RID: 380
		[CompilerGenerated]
		private static ValueFormatterFactory CS$<>9__CachedAnonymousMethodDelegate18;

		// Token: 0x0400017D RID: 381
		[CompilerGenerated]
		private static ValueFormatterFactory CS$<>9__CachedAnonymousMethodDelegate19;

		// Token: 0x0400017E RID: 382
		[CompilerGenerated]
		private static ValueFormatterFactory CS$<>9__CachedAnonymousMethodDelegate1a;

		// Token: 0x0400017F RID: 383
		[CompilerGenerated]
		private static ValueFormatterFactory CS$<>9__CachedAnonymousMethodDelegate1b;

		// Token: 0x04000180 RID: 384
		[CompilerGenerated]
		private static ValueFormatterFactory CS$<>9__CachedAnonymousMethodDelegate1c;

		// Token: 0x04000181 RID: 385
		[CompilerGenerated]
		private static ValueFormatterFactory CS$<>9__CachedAnonymousMethodDelegate1d;

		// Token: 0x04000182 RID: 386
		[CompilerGenerated]
		private static ValueFormatterFactory CS$<>9__CachedAnonymousMethodDelegate1e;

		// Token: 0x04000183 RID: 387
		[CompilerGenerated]
		private static ValueFormatterFactory CS$<>9__CachedAnonymousMethodDelegate1f;

		// Token: 0x020001B5 RID: 437
		[CompilerGenerated]
		private sealed class <>c__DisplayClass20
		{
			// Token: 0x06000B39 RID: 2873 RVA: 0x000185EE File Offset: 0x000167EE
			public <>c__DisplayClass20()
			{
			}

			// Token: 0x06000B3A RID: 2874 RVA: 0x0001865C File Offset: 0x0001685C
			public string <.cctor>b__2(object val)
			{
				return (val is ValueType) ? string.Format(MsgUtils.Fmt_ValueType, val) : this.next(val);
			}

			// Token: 0x040002D1 RID: 721
			public ValueFormatter next;
		}

		// Token: 0x020001B6 RID: 438
		[CompilerGenerated]
		private sealed class <>c__DisplayClass22
		{
			// Token: 0x06000B3B RID: 2875 RVA: 0x000185F6 File Offset: 0x000167F6
			public <>c__DisplayClass22()
			{
			}

			// Token: 0x06000B3C RID: 2876 RVA: 0x000186BC File Offset: 0x000168BC
			public string <.cctor>b__4(object val)
			{
				return (val is DateTime) ? MsgUtils.FormatDateTime((DateTime)val) : this.next(val);
			}

			// Token: 0x040002D2 RID: 722
			public ValueFormatter next;
		}

		// Token: 0x020001B7 RID: 439
		[CompilerGenerated]
		private sealed class <>c__DisplayClass24
		{
			// Token: 0x06000B3D RID: 2877 RVA: 0x000185FE File Offset: 0x000167FE
			public <>c__DisplayClass24()
			{
			}

			// Token: 0x06000B3E RID: 2878 RVA: 0x0001871C File Offset: 0x0001691C
			public string <.cctor>b__6(object val)
			{
				return (val is DateTimeOffset) ? MsgUtils.FormatDateTimeOffset((DateTimeOffset)val) : this.next(val);
			}

			// Token: 0x040002D3 RID: 723
			public ValueFormatter next;
		}

		// Token: 0x020001B8 RID: 440
		[CompilerGenerated]
		private sealed class <>c__DisplayClass26
		{
			// Token: 0x06000B3F RID: 2879 RVA: 0x00018606 File Offset: 0x00016806
			public <>c__DisplayClass26()
			{
			}

			// Token: 0x06000B40 RID: 2880 RVA: 0x0001877C File Offset: 0x0001697C
			public string <.cctor>b__8(object val)
			{
				return (val is decimal) ? MsgUtils.FormatDecimal((decimal)val) : this.next(val);
			}

			// Token: 0x040002D4 RID: 724
			public ValueFormatter next;
		}

		// Token: 0x020001B9 RID: 441
		[CompilerGenerated]
		private sealed class <>c__DisplayClass28
		{
			// Token: 0x06000B41 RID: 2881 RVA: 0x0001860E File Offset: 0x0001680E
			public <>c__DisplayClass28()
			{
			}

			// Token: 0x06000B42 RID: 2882 RVA: 0x000187DC File Offset: 0x000169DC
			public string <.cctor>b__a(object val)
			{
				return (val is float) ? MsgUtils.FormatFloat((float)val) : this.next(val);
			}

			// Token: 0x040002D5 RID: 725
			public ValueFormatter next;
		}

		// Token: 0x020001BA RID: 442
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2a
		{
			// Token: 0x06000B43 RID: 2883 RVA: 0x00018616 File Offset: 0x00016816
			public <>c__DisplayClass2a()
			{
			}

			// Token: 0x06000B44 RID: 2884 RVA: 0x0001883C File Offset: 0x00016A3C
			public string <.cctor>b__c(object val)
			{
				return (val is double) ? MsgUtils.FormatDouble((double)val) : this.next(val);
			}

			// Token: 0x040002D6 RID: 726
			public ValueFormatter next;
		}

		// Token: 0x020001BB RID: 443
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2c
		{
			// Token: 0x06000B45 RID: 2885 RVA: 0x0001861E File Offset: 0x0001681E
			public <>c__DisplayClass2c()
			{
			}

			// Token: 0x06000B46 RID: 2886 RVA: 0x0001889C File Offset: 0x00016A9C
			public string <.cctor>b__e(object val)
			{
				return (val is char) ? string.Format(MsgUtils.Fmt_Char, val) : this.next(val);
			}

			// Token: 0x040002D7 RID: 727
			public ValueFormatter next;
		}

		// Token: 0x020001BC RID: 444
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2e
		{
			// Token: 0x06000B47 RID: 2887 RVA: 0x00018626 File Offset: 0x00016826
			public <>c__DisplayClass2e()
			{
			}

			// Token: 0x06000B48 RID: 2888 RVA: 0x000188FC File Offset: 0x00016AFC
			public string <.cctor>b__10(object val)
			{
				return (val is IEnumerable) ? MsgUtils.FormatCollection((IEnumerable)val, 0L, 10) : this.next(val);
			}

			// Token: 0x040002D8 RID: 728
			public ValueFormatter next;
		}

		// Token: 0x020001BD RID: 445
		[CompilerGenerated]
		private sealed class <>c__DisplayClass30
		{
			// Token: 0x06000B49 RID: 2889 RVA: 0x0001862E File Offset: 0x0001682E
			public <>c__DisplayClass30()
			{
			}

			// Token: 0x06000B4A RID: 2890 RVA: 0x00018960 File Offset: 0x00016B60
			public string <.cctor>b__12(object val)
			{
				return (val is string) ? MsgUtils.FormatString((string)val) : this.next(val);
			}

			// Token: 0x040002D9 RID: 729
			public ValueFormatter next;
		}

		// Token: 0x020001BE RID: 446
		[CompilerGenerated]
		private sealed class <>c__DisplayClass32
		{
			// Token: 0x06000B4B RID: 2891 RVA: 0x00018636 File Offset: 0x00016836
			public <>c__DisplayClass32()
			{
			}

			// Token: 0x06000B4C RID: 2892 RVA: 0x000189C0 File Offset: 0x00016BC0
			public string <.cctor>b__14(object val)
			{
				return val.GetType().IsArray ? MsgUtils.FormatArray((Array)val) : this.next(val);
			}

			// Token: 0x040002DA RID: 730
			public ValueFormatter next;
		}
	}
}
