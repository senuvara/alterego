﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000074 RID: 116
	public abstract class SelfResolvingOperator : ConstraintOperator
	{
		// Token: 0x06000430 RID: 1072 RVA: 0x0000D78E File Offset: 0x0000B98E
		protected SelfResolvingOperator()
		{
		}
	}
}
