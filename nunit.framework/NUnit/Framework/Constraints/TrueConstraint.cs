﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000146 RID: 326
	public class TrueConstraint : Constraint
	{
		// Token: 0x06000882 RID: 2178 RVA: 0x0001D588 File Offset: 0x0001B788
		public TrueConstraint() : base(new object[0])
		{
			this.Description = "True";
		}

		// Token: 0x06000883 RID: 2179 RVA: 0x0001D5A8 File Offset: 0x0001B7A8
		public override ConstraintResult ApplyTo(object actual)
		{
			return new ConstraintResult(this, actual, true.Equals(actual));
		}
	}
}
