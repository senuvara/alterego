﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000031 RID: 49
	public class EndsWithConstraint : StringConstraint
	{
		// Token: 0x0600019C RID: 412 RVA: 0x00006280 File Offset: 0x00004480
		public EndsWithConstraint(string expected) : base(expected)
		{
			this.descriptionText = "String ending with";
		}

		// Token: 0x0600019D RID: 413 RVA: 0x00006298 File Offset: 0x00004498
		protected override bool Matches(string actual)
		{
			bool result;
			if (this.caseInsensitive)
			{
				result = (actual != null && actual.ToLower().EndsWith(this.expected.ToLower()));
			}
			else
			{
				result = (actual != null && actual.EndsWith(this.expected));
			}
			return result;
		}
	}
}
