﻿using System;
using NUnit.Compatibility;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000D6 RID: 214
	public class AssignableFromConstraint : TypeConstraint
	{
		// Token: 0x06000666 RID: 1638 RVA: 0x00016B17 File Offset: 0x00014D17
		public AssignableFromConstraint(Type type) : base(type, "assignable from ")
		{
		}

		// Token: 0x06000667 RID: 1639 RVA: 0x00016B28 File Offset: 0x00014D28
		protected override bool Matches(object actual)
		{
			return actual != null && actual.GetType().GetTypeInfo().IsAssignableFrom(this.expectedType.GetTypeInfo());
		}
	}
}
