﻿using System;
using System.Text.RegularExpressions;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200017C RID: 380
	public class RegexConstraint : StringConstraint
	{
		// Token: 0x06000A0E RID: 2574 RVA: 0x00021E63 File Offset: 0x00020063
		public RegexConstraint(string pattern) : base(pattern)
		{
			this.descriptionText = "String matching";
		}

		// Token: 0x06000A0F RID: 2575 RVA: 0x00021E7C File Offset: 0x0002007C
		protected override bool Matches(string actual)
		{
			return actual != null && Regex.IsMatch(actual, this.expected, this.caseInsensitive ? RegexOptions.IgnoreCase : RegexOptions.None);
		}
	}
}
