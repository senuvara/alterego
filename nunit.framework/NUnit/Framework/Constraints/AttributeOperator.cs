﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000100 RID: 256
	public class AttributeOperator : SelfResolvingOperator
	{
		// Token: 0x06000728 RID: 1832 RVA: 0x00019EE0 File Offset: 0x000180E0
		public AttributeOperator(Type type)
		{
			this.type = type;
			this.left_precedence = (this.right_precedence = 1);
		}

		// Token: 0x06000729 RID: 1833 RVA: 0x00019F10 File Offset: 0x00018110
		public override void Reduce(ConstraintBuilder.ConstraintStack stack)
		{
			if (base.RightContext == null || base.RightContext is BinaryOperator)
			{
				stack.Push(new AttributeExistsConstraint(this.type));
			}
			else
			{
				stack.Push(new AttributeConstraint(this.type, stack.Pop()));
			}
		}

		// Token: 0x04000195 RID: 405
		private readonly Type type;
	}
}
