﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Compatibility;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000D1 RID: 209
	public abstract class ComparisonAdapter
	{
		// Token: 0x1700016E RID: 366
		// (get) Token: 0x06000659 RID: 1625 RVA: 0x0001690C File Offset: 0x00014B0C
		public static ComparisonAdapter Default
		{
			get
			{
				return new ComparisonAdapter.DefaultComparisonAdapter();
			}
		}

		// Token: 0x0600065A RID: 1626 RVA: 0x00016924 File Offset: 0x00014B24
		public static ComparisonAdapter For(IComparer comparer)
		{
			return new ComparisonAdapter.ComparerAdapter(comparer);
		}

		// Token: 0x0600065B RID: 1627 RVA: 0x0001693C File Offset: 0x00014B3C
		public static ComparisonAdapter For<T>(IComparer<T> comparer)
		{
			return new ComparisonAdapter.ComparerAdapter<T>(comparer);
		}

		// Token: 0x0600065C RID: 1628 RVA: 0x00016954 File Offset: 0x00014B54
		public static ComparisonAdapter For<T>(Comparison<T> comparer)
		{
			return new ComparisonAdapter.ComparisonAdapterForComparison<T>(comparer);
		}

		// Token: 0x0600065D RID: 1629
		public abstract int Compare(object expected, object actual);

		// Token: 0x0600065E RID: 1630 RVA: 0x0001696C File Offset: 0x00014B6C
		protected ComparisonAdapter()
		{
		}

		// Token: 0x020000D2 RID: 210
		private class ComparerAdapter : ComparisonAdapter
		{
			// Token: 0x0600065F RID: 1631 RVA: 0x00016974 File Offset: 0x00014B74
			public ComparerAdapter(IComparer comparer)
			{
				this.comparer = comparer;
			}

			// Token: 0x06000660 RID: 1632 RVA: 0x00016988 File Offset: 0x00014B88
			public override int Compare(object expected, object actual)
			{
				return this.comparer.Compare(expected, actual);
			}

			// Token: 0x04000157 RID: 343
			private readonly IComparer comparer;
		}

		// Token: 0x020000D3 RID: 211
		private class DefaultComparisonAdapter : ComparisonAdapter.ComparerAdapter
		{
			// Token: 0x06000661 RID: 1633 RVA: 0x000169A7 File Offset: 0x00014BA7
			public DefaultComparisonAdapter() : base(NUnitComparer.Default)
			{
			}
		}

		// Token: 0x020000D4 RID: 212
		private class ComparerAdapter<T> : ComparisonAdapter
		{
			// Token: 0x06000662 RID: 1634 RVA: 0x000169B7 File Offset: 0x00014BB7
			public ComparerAdapter(IComparer<T> comparer)
			{
				this.comparer = comparer;
			}

			// Token: 0x06000663 RID: 1635 RVA: 0x000169CC File Offset: 0x00014BCC
			public override int Compare(object expected, object actual)
			{
				if (!typeof(T).GetTypeInfo().IsAssignableFrom(expected.GetType().GetTypeInfo()))
				{
					throw new ArgumentException("Cannot compare " + expected.ToString());
				}
				if (!typeof(T).GetTypeInfo().IsAssignableFrom(actual.GetType().GetTypeInfo()))
				{
					throw new ArgumentException("Cannot compare to " + actual.ToString());
				}
				return this.comparer.Compare((T)((object)expected), (T)((object)actual));
			}

			// Token: 0x04000158 RID: 344
			private readonly IComparer<T> comparer;
		}

		// Token: 0x020000D5 RID: 213
		private class ComparisonAdapterForComparison<T> : ComparisonAdapter
		{
			// Token: 0x06000664 RID: 1636 RVA: 0x00016A67 File Offset: 0x00014C67
			public ComparisonAdapterForComparison(Comparison<T> comparer)
			{
				this.comparison = comparer;
			}

			// Token: 0x06000665 RID: 1637 RVA: 0x00016A7C File Offset: 0x00014C7C
			public override int Compare(object expected, object actual)
			{
				if (!typeof(T).GetTypeInfo().IsAssignableFrom(expected.GetType().GetTypeInfo()))
				{
					throw new ArgumentException("Cannot compare " + expected.ToString());
				}
				if (!typeof(T).GetTypeInfo().IsAssignableFrom(actual.GetType().GetTypeInfo()))
				{
					throw new ArgumentException("Cannot compare to " + actual.ToString());
				}
				return this.comparison((T)((object)expected), (T)((object)actual));
			}

			// Token: 0x04000159 RID: 345
			private readonly Comparison<T> comparison;
		}
	}
}
