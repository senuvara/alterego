﻿using System;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200002F RID: 47
	public class SamePathOrUnderConstraint : PathConstraint
	{
		// Token: 0x06000196 RID: 406 RVA: 0x00006124 File Offset: 0x00004324
		public SamePathOrUnderConstraint(string expected) : base(expected)
		{
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000197 RID: 407 RVA: 0x00006130 File Offset: 0x00004330
		public override string Description
		{
			get
			{
				return "Path under or matching " + MsgUtils.FormatValue(this.expected);
			}
		}

		// Token: 0x06000198 RID: 408 RVA: 0x00006158 File Offset: 0x00004358
		protected override bool Matches(string actual)
		{
			bool result;
			if (actual == null)
			{
				result = false;
			}
			else
			{
				string text = base.Canonicalize(this.expected);
				string text2 = base.Canonicalize(actual);
				result = (StringUtil.StringsEqual(text, text2, this.caseInsensitive) || base.IsSubPath(text, text2));
			}
			return result;
		}
	}
}
