﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000079 RID: 121
	public abstract class BinaryConstraint : Constraint
	{
		// Token: 0x06000453 RID: 1107 RVA: 0x0000E7BC File Offset: 0x0000C9BC
		protected BinaryConstraint(IConstraint left, IConstraint right) : base(new object[]
		{
			left,
			right
		})
		{
			Guard.ArgumentNotNull(left, "left");
			this.Left = left;
			Guard.ArgumentNotNull(right, "right");
			this.Right = right;
		}

		// Token: 0x040000C2 RID: 194
		protected IConstraint Left;

		// Token: 0x040000C3 RID: 195
		protected IConstraint Right;
	}
}
