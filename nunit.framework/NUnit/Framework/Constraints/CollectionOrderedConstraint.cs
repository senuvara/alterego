﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000BE RID: 190
	public class CollectionOrderedConstraint : CollectionConstraint
	{
		// Token: 0x060005D0 RID: 1488 RVA: 0x00013C36 File Offset: 0x00011E36
		public CollectionOrderedConstraint()
		{
			this._steps = new List<CollectionOrderedConstraint.OrderingStep>();
			this.CreateNextStep(null);
		}

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x060005D1 RID: 1489 RVA: 0x00013C54 File Offset: 0x00011E54
		public override string DisplayName
		{
			get
			{
				return "Ordered";
			}
		}

		// Token: 0x17000149 RID: 329
		// (get) Token: 0x060005D2 RID: 1490 RVA: 0x00013C6C File Offset: 0x00011E6C
		public CollectionOrderedConstraint Ascending
		{
			get
			{
				if (this._activeStep.Direction != CollectionOrderedConstraint.OrderDirection.Unspecified)
				{
					throw new InvalidOperationException("Only one directional modifier may be used");
				}
				this._activeStep.Direction = CollectionOrderedConstraint.OrderDirection.Ascending;
				return this;
			}
		}

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x060005D3 RID: 1491 RVA: 0x00013CAC File Offset: 0x00011EAC
		public CollectionOrderedConstraint Descending
		{
			get
			{
				if (this._activeStep.Direction != CollectionOrderedConstraint.OrderDirection.Unspecified)
				{
					throw new InvalidOperationException("Only one directional modifier may be used");
				}
				this._activeStep.Direction = CollectionOrderedConstraint.OrderDirection.Descending;
				return this;
			}
		}

		// Token: 0x060005D4 RID: 1492 RVA: 0x00013CEC File Offset: 0x00011EEC
		public CollectionOrderedConstraint Using(IComparer comparer)
		{
			if (this._activeStep.ComparerName != null)
			{
				throw new InvalidOperationException("Only one Using modifier may be used");
			}
			this._activeStep.Comparer = ComparisonAdapter.For(comparer);
			this._activeStep.ComparerName = comparer.GetType().FullName;
			return this;
		}

		// Token: 0x060005D5 RID: 1493 RVA: 0x00013D48 File Offset: 0x00011F48
		public CollectionOrderedConstraint Using<T>(IComparer<T> comparer)
		{
			if (this._activeStep.ComparerName != null)
			{
				throw new InvalidOperationException("Only one Using modifier may be used");
			}
			this._activeStep.Comparer = ComparisonAdapter.For<T>(comparer);
			this._activeStep.ComparerName = comparer.GetType().FullName;
			return this;
		}

		// Token: 0x060005D6 RID: 1494 RVA: 0x00013DA4 File Offset: 0x00011FA4
		public CollectionOrderedConstraint Using<T>(Comparison<T> comparer)
		{
			if (this._activeStep.ComparerName != null)
			{
				throw new InvalidOperationException("Only one Using modifier may be used");
			}
			this._activeStep.Comparer = ComparisonAdapter.For<T>(comparer);
			this._activeStep.ComparerName = comparer.GetType().FullName;
			return this;
		}

		// Token: 0x060005D7 RID: 1495 RVA: 0x00013E00 File Offset: 0x00012000
		public CollectionOrderedConstraint By(string propertyName)
		{
			if (this._activeStep.PropertyName == null)
			{
				this._activeStep.PropertyName = propertyName;
			}
			else
			{
				this.CreateNextStep(propertyName);
			}
			return this;
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x060005D8 RID: 1496 RVA: 0x00013E40 File Offset: 0x00012040
		public CollectionOrderedConstraint Then
		{
			get
			{
				this.CreateNextStep(null);
				return this;
			}
		}

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x060005D9 RID: 1497 RVA: 0x00013E5C File Offset: 0x0001205C
		public override string Description
		{
			get
			{
				string text = "collection ordered";
				int num = 0;
				foreach (CollectionOrderedConstraint.OrderingStep orderingStep in this._steps)
				{
					if (num++ != 0)
					{
						text += " then";
					}
					if (orderingStep.PropertyName != null)
					{
						text = text + " by " + MsgUtils.FormatValue(orderingStep.PropertyName);
					}
					if (orderingStep.Direction == CollectionOrderedConstraint.OrderDirection.Descending)
					{
						text += ", descending";
					}
				}
				return text;
			}
		}

		// Token: 0x060005DA RID: 1498 RVA: 0x00013F20 File Offset: 0x00012120
		protected override bool Matches(IEnumerable actual)
		{
			object obj = null;
			int num = 0;
			foreach (object obj2 in actual)
			{
				if (obj2 == null)
				{
					throw new ArgumentNullException("actual", "Null value at index " + num.ToString());
				}
				if (obj != null)
				{
					if (this._steps[0].PropertyName != null)
					{
						foreach (CollectionOrderedConstraint.OrderingStep orderingStep in this._steps)
						{
							string propertyName = orderingStep.PropertyName;
							PropertyInfo property = obj.GetType().GetProperty(propertyName);
							PropertyInfo property2 = obj2.GetType().GetProperty(propertyName);
							object value = property.GetValue(obj, null);
							object value2 = property2.GetValue(obj2, null);
							if (value2 == null)
							{
								throw new ArgumentException("actual", "Null property value at index " + num.ToString());
							}
							int num2 = orderingStep.Comparer.Compare(value, value2);
							if (num2 < 0)
							{
								if (orderingStep.Direction == CollectionOrderedConstraint.OrderDirection.Descending)
								{
									return false;
								}
								break;
							}
							else if (num2 > 0)
							{
								if (orderingStep.Direction != CollectionOrderedConstraint.OrderDirection.Descending)
								{
									return false;
								}
								break;
							}
						}
					}
					else
					{
						int num2 = this._activeStep.Comparer.Compare(obj, obj2);
						if (this._activeStep.Direction == CollectionOrderedConstraint.OrderDirection.Descending && num2 < 0)
						{
							return false;
						}
						if (this._activeStep.Direction != CollectionOrderedConstraint.OrderDirection.Descending && num2 > 0)
						{
							return false;
						}
					}
				}
				obj = obj2;
				num++;
			}
			return true;
		}

		// Token: 0x060005DB RID: 1499 RVA: 0x0001417C File Offset: 0x0001237C
		protected override string GetStringRepresentation()
		{
			StringBuilder stringBuilder = new StringBuilder("<ordered");
			if (this._steps.Count > 0)
			{
				CollectionOrderedConstraint.OrderingStep orderingStep = this._steps[0];
				if (orderingStep.PropertyName != null)
				{
					stringBuilder.Append("by " + orderingStep.PropertyName);
				}
				if (orderingStep.Direction == CollectionOrderedConstraint.OrderDirection.Descending)
				{
					stringBuilder.Append(" descending");
				}
				if (orderingStep.ComparerName != null)
				{
					stringBuilder.Append(" " + orderingStep.ComparerName);
				}
			}
			stringBuilder.Append(">");
			return stringBuilder.ToString();
		}

		// Token: 0x060005DC RID: 1500 RVA: 0x00014233 File Offset: 0x00012433
		private void CreateNextStep(string propertyName)
		{
			this._activeStep = new CollectionOrderedConstraint.OrderingStep(propertyName);
			this._steps.Add(this._activeStep);
		}

		// Token: 0x04000130 RID: 304
		private List<CollectionOrderedConstraint.OrderingStep> _steps;

		// Token: 0x04000131 RID: 305
		private CollectionOrderedConstraint.OrderingStep _activeStep;

		// Token: 0x020000BF RID: 191
		private enum OrderDirection
		{
			// Token: 0x04000133 RID: 307
			Unspecified,
			// Token: 0x04000134 RID: 308
			Ascending,
			// Token: 0x04000135 RID: 309
			Descending
		}

		// Token: 0x020000C0 RID: 192
		private class OrderingStep
		{
			// Token: 0x060005DD RID: 1501 RVA: 0x00014254 File Offset: 0x00012454
			public OrderingStep(string propertyName)
			{
				this.PropertyName = propertyName;
				this.Comparer = ComparisonAdapter.Default;
			}

			// Token: 0x1700014D RID: 333
			// (get) Token: 0x060005DE RID: 1502 RVA: 0x00014274 File Offset: 0x00012474
			// (set) Token: 0x060005DF RID: 1503 RVA: 0x0001428B File Offset: 0x0001248B
			public string PropertyName
			{
				[CompilerGenerated]
				get
				{
					return this.<PropertyName>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<PropertyName>k__BackingField = value;
				}
			}

			// Token: 0x1700014E RID: 334
			// (get) Token: 0x060005E0 RID: 1504 RVA: 0x00014294 File Offset: 0x00012494
			// (set) Token: 0x060005E1 RID: 1505 RVA: 0x000142AB File Offset: 0x000124AB
			public CollectionOrderedConstraint.OrderDirection Direction
			{
				[CompilerGenerated]
				get
				{
					return this.<Direction>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<Direction>k__BackingField = value;
				}
			}

			// Token: 0x1700014F RID: 335
			// (get) Token: 0x060005E2 RID: 1506 RVA: 0x000142B4 File Offset: 0x000124B4
			// (set) Token: 0x060005E3 RID: 1507 RVA: 0x000142CB File Offset: 0x000124CB
			public ComparisonAdapter Comparer
			{
				[CompilerGenerated]
				get
				{
					return this.<Comparer>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<Comparer>k__BackingField = value;
				}
			}

			// Token: 0x17000150 RID: 336
			// (get) Token: 0x060005E4 RID: 1508 RVA: 0x000142D4 File Offset: 0x000124D4
			// (set) Token: 0x060005E5 RID: 1509 RVA: 0x000142EB File Offset: 0x000124EB
			public string ComparerName
			{
				[CompilerGenerated]
				get
				{
					return this.<ComparerName>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<ComparerName>k__BackingField = value;
				}
			}

			// Token: 0x04000136 RID: 310
			[CompilerGenerated]
			private string <PropertyName>k__BackingField;

			// Token: 0x04000137 RID: 311
			[CompilerGenerated]
			private CollectionOrderedConstraint.OrderDirection <Direction>k__BackingField;

			// Token: 0x04000138 RID: 312
			[CompilerGenerated]
			private ComparisonAdapter <Comparer>k__BackingField;

			// Token: 0x04000139 RID: 313
			[CompilerGenerated]
			private string <ComparerName>k__BackingField;
		}
	}
}
