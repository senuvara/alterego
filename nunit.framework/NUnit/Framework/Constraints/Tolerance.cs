﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000147 RID: 327
	public class Tolerance
	{
		// Token: 0x170001DB RID: 475
		// (get) Token: 0x06000884 RID: 2180 RVA: 0x0001D5CC File Offset: 0x0001B7CC
		public static Tolerance Default
		{
			get
			{
				return new Tolerance(0, ToleranceMode.Unset);
			}
		}

		// Token: 0x170001DC RID: 476
		// (get) Token: 0x06000885 RID: 2181 RVA: 0x0001D5EC File Offset: 0x0001B7EC
		public static Tolerance Exact
		{
			get
			{
				return new Tolerance(0, ToleranceMode.Linear);
			}
		}

		// Token: 0x06000886 RID: 2182 RVA: 0x0001D60A File Offset: 0x0001B80A
		public Tolerance(object amount) : this(amount, ToleranceMode.Linear)
		{
		}

		// Token: 0x06000887 RID: 2183 RVA: 0x0001D617 File Offset: 0x0001B817
		private Tolerance(object amount, ToleranceMode mode)
		{
			this.amount = amount;
			this.mode = mode;
		}

		// Token: 0x170001DD RID: 477
		// (get) Token: 0x06000888 RID: 2184 RVA: 0x0001D630 File Offset: 0x0001B830
		public ToleranceMode Mode
		{
			get
			{
				return this.mode;
			}
		}

		// Token: 0x06000889 RID: 2185 RVA: 0x0001D648 File Offset: 0x0001B848
		private void CheckLinearAndNumeric()
		{
			if (this.mode != ToleranceMode.Linear)
			{
				throw new InvalidOperationException((this.mode == ToleranceMode.Unset) ? "Tolerance amount must be specified before setting mode" : "Tried to use multiple tolerance modes at the same time");
			}
			if (!Numerics.IsNumericType(this.amount))
			{
				throw new InvalidOperationException("A numeric tolerance is required");
			}
		}

		// Token: 0x170001DE RID: 478
		// (get) Token: 0x0600088A RID: 2186 RVA: 0x0001D698 File Offset: 0x0001B898
		public object Value
		{
			get
			{
				return this.amount;
			}
		}

		// Token: 0x170001DF RID: 479
		// (get) Token: 0x0600088B RID: 2187 RVA: 0x0001D6B0 File Offset: 0x0001B8B0
		public Tolerance Percent
		{
			get
			{
				this.CheckLinearAndNumeric();
				return new Tolerance(this.amount, ToleranceMode.Percent);
			}
		}

		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x0600088C RID: 2188 RVA: 0x0001D6D8 File Offset: 0x0001B8D8
		public Tolerance Ulps
		{
			get
			{
				this.CheckLinearAndNumeric();
				return new Tolerance(this.amount, ToleranceMode.Ulps);
			}
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x0600088D RID: 2189 RVA: 0x0001D700 File Offset: 0x0001B900
		public Tolerance Days
		{
			get
			{
				this.CheckLinearAndNumeric();
				return new Tolerance(TimeSpan.FromDays(Convert.ToDouble(this.amount)));
			}
		}

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x0600088E RID: 2190 RVA: 0x0001D734 File Offset: 0x0001B934
		public Tolerance Hours
		{
			get
			{
				this.CheckLinearAndNumeric();
				return new Tolerance(TimeSpan.FromHours(Convert.ToDouble(this.amount)));
			}
		}

		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x0600088F RID: 2191 RVA: 0x0001D768 File Offset: 0x0001B968
		public Tolerance Minutes
		{
			get
			{
				this.CheckLinearAndNumeric();
				return new Tolerance(TimeSpan.FromMinutes(Convert.ToDouble(this.amount)));
			}
		}

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x06000890 RID: 2192 RVA: 0x0001D79C File Offset: 0x0001B99C
		public Tolerance Seconds
		{
			get
			{
				this.CheckLinearAndNumeric();
				return new Tolerance(TimeSpan.FromSeconds(Convert.ToDouble(this.amount)));
			}
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x06000891 RID: 2193 RVA: 0x0001D7D0 File Offset: 0x0001B9D0
		public Tolerance Milliseconds
		{
			get
			{
				this.CheckLinearAndNumeric();
				return new Tolerance(TimeSpan.FromMilliseconds(Convert.ToDouble(this.amount)));
			}
		}

		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x06000892 RID: 2194 RVA: 0x0001D804 File Offset: 0x0001BA04
		public Tolerance Ticks
		{
			get
			{
				this.CheckLinearAndNumeric();
				return new Tolerance(TimeSpan.FromTicks(Convert.ToInt64(this.amount)));
			}
		}

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06000893 RID: 2195 RVA: 0x0001D838 File Offset: 0x0001BA38
		public bool IsUnsetOrDefault
		{
			get
			{
				return this.mode == ToleranceMode.Unset;
			}
		}

		// Token: 0x040001E5 RID: 485
		private const string ModeMustFollowTolerance = "Tolerance amount must be specified before setting mode";

		// Token: 0x040001E6 RID: 486
		private const string MultipleToleranceModes = "Tried to use multiple tolerance modes at the same time";

		// Token: 0x040001E7 RID: 487
		private const string NumericToleranceRequired = "A numeric tolerance is required";

		// Token: 0x040001E8 RID: 488
		private readonly ToleranceMode mode;

		// Token: 0x040001E9 RID: 489
		private readonly object amount;
	}
}
