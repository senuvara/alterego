﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000069 RID: 105
	public class AllOperator : CollectionOperator
	{
		// Token: 0x06000400 RID: 1024 RVA: 0x0000CCC0 File Offset: 0x0000AEC0
		public override IConstraint ApplyPrefix(IConstraint constraint)
		{
			return new AllItemsConstraint(constraint);
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x0000CCD8 File Offset: 0x0000AED8
		public AllOperator()
		{
		}
	}
}
