﻿using System;
using System.IO;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200006A RID: 106
	public class FileOrDirectoryExistsConstraint : Constraint
	{
		// Token: 0x170000DD RID: 221
		// (get) Token: 0x06000402 RID: 1026 RVA: 0x0000CCE0 File Offset: 0x0000AEE0
		public FileOrDirectoryExistsConstraint IgnoreDirectories
		{
			get
			{
				this._ignoreDirectories = true;
				return this;
			}
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000403 RID: 1027 RVA: 0x0000CCFC File Offset: 0x0000AEFC
		public FileOrDirectoryExistsConstraint IgnoreFiles
		{
			get
			{
				this._ignoreFiles = true;
				return this;
			}
		}

		// Token: 0x06000404 RID: 1028 RVA: 0x0000CD16 File Offset: 0x0000AF16
		public FileOrDirectoryExistsConstraint() : base(new object[0])
		{
		}

		// Token: 0x06000405 RID: 1029 RVA: 0x0000CD27 File Offset: 0x0000AF27
		public FileOrDirectoryExistsConstraint(bool ignoreDirectories) : base(new object[0])
		{
			this._ignoreDirectories = ignoreDirectories;
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x06000406 RID: 1030 RVA: 0x0000CD40 File Offset: 0x0000AF40
		public override string Description
		{
			get
			{
				string result;
				if (this._ignoreDirectories)
				{
					result = "file exists";
				}
				else if (this._ignoreFiles)
				{
					result = "directory exists";
				}
				else
				{
					result = "file or directory exists";
				}
				return result;
			}
		}

		// Token: 0x06000407 RID: 1031 RVA: 0x0000CD84 File Offset: 0x0000AF84
		public override ConstraintResult ApplyTo(object actual)
		{
			if (actual == null)
			{
				throw new ArgumentNullException("actual", "The actual value must be a non-null string" + this.ErrorSubstring);
			}
			ConstraintResult result;
			if (actual is string)
			{
				result = this.CheckString<object>(actual);
			}
			else
			{
				FileInfo fileInfo = actual as FileInfo;
				if (!this._ignoreFiles && fileInfo != null)
				{
					result = new ConstraintResult(this, actual, fileInfo.Exists);
				}
				else
				{
					DirectoryInfo directoryInfo = actual as DirectoryInfo;
					if (this._ignoreDirectories || directoryInfo == null)
					{
						throw new ArgumentException("The actual value must be a string" + this.ErrorSubstring, "actual");
					}
					result = new ConstraintResult(this, actual, directoryInfo.Exists);
				}
			}
			return result;
		}

		// Token: 0x06000408 RID: 1032 RVA: 0x0000CE48 File Offset: 0x0000B048
		private ConstraintResult CheckString<TActual>(TActual actual)
		{
			string text = actual as string;
			if (string.IsNullOrEmpty(text))
			{
				throw new ArgumentException("The actual value cannot be an empty string", "actual");
			}
			FileInfo fileInfo = new FileInfo(text);
			ConstraintResult result;
			if (this._ignoreDirectories && !this._ignoreFiles)
			{
				result = new ConstraintResult(this, actual, fileInfo.Exists);
			}
			else
			{
				DirectoryInfo directoryInfo = new DirectoryInfo(text);
				if (this._ignoreFiles && !this._ignoreDirectories)
				{
					result = new ConstraintResult(this, actual, directoryInfo.Exists);
				}
				else
				{
					result = new ConstraintResult(this, actual, fileInfo.Exists || directoryInfo.Exists);
				}
			}
			return result;
		}

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000409 RID: 1033 RVA: 0x0000CF0C File Offset: 0x0000B10C
		private string ErrorSubstring
		{
			get
			{
				string result;
				if (this._ignoreDirectories)
				{
					result = " or FileInfo";
				}
				else if (this._ignoreFiles)
				{
					result = " or DirectoryInfo";
				}
				else
				{
					result = ", FileInfo or DirectoryInfo";
				}
				return result;
			}
		}

		// Token: 0x040000AC RID: 172
		private bool _ignoreDirectories;

		// Token: 0x040000AD RID: 173
		private bool _ignoreFiles;
	}
}
