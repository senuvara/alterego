﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000ED RID: 237
	// (Invoke) Token: 0x060006BC RID: 1724
	public delegate ValueFormatter ValueFormatterFactory(ValueFormatter next);
}
