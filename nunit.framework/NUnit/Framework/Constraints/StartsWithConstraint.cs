﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000148 RID: 328
	public class StartsWithConstraint : StringConstraint
	{
		// Token: 0x06000894 RID: 2196 RVA: 0x0001D853 File Offset: 0x0001BA53
		public StartsWithConstraint(string expected) : base(expected)
		{
			this.descriptionText = "String starting with";
		}

		// Token: 0x06000895 RID: 2197 RVA: 0x0001D86C File Offset: 0x0001BA6C
		protected override bool Matches(string actual)
		{
			bool result;
			if (this.caseInsensitive)
			{
				result = (actual != null && actual.ToLower().StartsWith(this.expected.ToLower()));
			}
			else
			{
				result = (actual != null && actual.StartsWith(this.expected));
			}
			return result;
		}
	}
}
