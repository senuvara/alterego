﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000102 RID: 258
	public abstract class ComparisonConstraint : Constraint
	{
		// Token: 0x06000736 RID: 1846 RVA: 0x00019F88 File Offset: 0x00018188
		protected ComparisonConstraint(object value, bool lessComparisonResult, bool equalComparisonResult, bool greaterComparisonResult, string predicate) : base(new object[]
		{
			value
		})
		{
			this.expected = value;
			this.lessComparisonResult = lessComparisonResult;
			this.equalComparisonResult = equalComparisonResult;
			this.greaterComparisonResult = greaterComparisonResult;
			this.Description = predicate + " " + MsgUtils.FormatValue(this.expected);
		}

		// Token: 0x06000737 RID: 1847 RVA: 0x0001A008 File Offset: 0x00018208
		public override ConstraintResult ApplyTo(object actual)
		{
			if (this.expected == null)
			{
				throw new ArgumentException("Cannot compare using a null reference", "expected");
			}
			if (actual == null)
			{
				throw new ArgumentException("Cannot compare to null reference", "actual");
			}
			int num = this.comparer.Compare(this.expected, actual);
			bool isSuccess = (num < 0 && this.greaterComparisonResult) || (num == 0 && this.equalComparisonResult) || (num > 0 && this.lessComparisonResult);
			return new ConstraintResult(this, actual, isSuccess);
		}

		// Token: 0x06000738 RID: 1848 RVA: 0x0001A098 File Offset: 0x00018298
		public ComparisonConstraint Using(IComparer comparer)
		{
			this.comparer = ComparisonAdapter.For(comparer);
			return this;
		}

		// Token: 0x06000739 RID: 1849 RVA: 0x0001A0B8 File Offset: 0x000182B8
		public ComparisonConstraint Using<T>(IComparer<T> comparer)
		{
			this.comparer = ComparisonAdapter.For<T>(comparer);
			return this;
		}

		// Token: 0x0600073A RID: 1850 RVA: 0x0001A0D8 File Offset: 0x000182D8
		public ComparisonConstraint Using<T>(Comparison<T> comparer)
		{
			this.comparer = ComparisonAdapter.For<T>(comparer);
			return this;
		}

		// Token: 0x04000196 RID: 406
		protected object expected;

		// Token: 0x04000197 RID: 407
		protected bool lessComparisonResult = false;

		// Token: 0x04000198 RID: 408
		protected bool equalComparisonResult = false;

		// Token: 0x04000199 RID: 409
		protected bool greaterComparisonResult = false;

		// Token: 0x0400019A RID: 410
		private ComparisonAdapter comparer = ComparisonAdapter.Default;
	}
}
