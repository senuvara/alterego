﻿using System;
using System.Runtime.InteropServices;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200012C RID: 300
	public class FloatingPointNumerics
	{
		// Token: 0x060007F8 RID: 2040 RVA: 0x0001C350 File Offset: 0x0001A550
		public static bool AreAlmostEqualUlps(float left, float right, int maxUlps)
		{
			FloatingPointNumerics.FloatIntUnion floatIntUnion = default(FloatingPointNumerics.FloatIntUnion);
			FloatingPointNumerics.FloatIntUnion floatIntUnion2 = default(FloatingPointNumerics.FloatIntUnion);
			floatIntUnion.Float = left;
			floatIntUnion2.Float = right;
			uint num = floatIntUnion.UInt >> 31;
			uint num2 = floatIntUnion2.UInt >> 31;
			uint num3 = 2147483648U - floatIntUnion.UInt & num;
			floatIntUnion.UInt = (num3 | (floatIntUnion.UInt & ~num));
			uint num4 = 2147483648U - floatIntUnion2.UInt & num2;
			floatIntUnion2.UInt = (num4 | (floatIntUnion2.UInt & ~num2));
			if (num != num2)
			{
				if (Math.Abs(floatIntUnion.Int) > maxUlps || Math.Abs(floatIntUnion2.Int) > maxUlps)
				{
					return false;
				}
			}
			return Math.Abs(floatIntUnion.Int - floatIntUnion2.Int) <= maxUlps;
		}

		// Token: 0x060007F9 RID: 2041 RVA: 0x0001C43C File Offset: 0x0001A63C
		public static bool AreAlmostEqualUlps(double left, double right, long maxUlps)
		{
			FloatingPointNumerics.DoubleLongUnion doubleLongUnion = default(FloatingPointNumerics.DoubleLongUnion);
			FloatingPointNumerics.DoubleLongUnion doubleLongUnion2 = default(FloatingPointNumerics.DoubleLongUnion);
			doubleLongUnion.Double = left;
			doubleLongUnion2.Double = right;
			ulong num = doubleLongUnion.ULong >> 63;
			ulong num2 = doubleLongUnion2.ULong >> 63;
			ulong num3 = 9223372036854775808UL - doubleLongUnion.ULong & num;
			doubleLongUnion.ULong = (num3 | (doubleLongUnion.ULong & ~num));
			ulong num4 = 9223372036854775808UL - doubleLongUnion2.ULong & num2;
			doubleLongUnion2.ULong = (num4 | (doubleLongUnion2.ULong & ~num2));
			if (num != num2)
			{
				if (Math.Abs(doubleLongUnion.Long) > maxUlps || Math.Abs(doubleLongUnion2.Long) > maxUlps)
				{
					return false;
				}
			}
			return Math.Abs(doubleLongUnion.Long - doubleLongUnion2.Long) <= maxUlps;
		}

		// Token: 0x060007FA RID: 2042 RVA: 0x0001C530 File Offset: 0x0001A730
		public static int ReinterpretAsInt(float value)
		{
			FloatingPointNumerics.FloatIntUnion floatIntUnion = default(FloatingPointNumerics.FloatIntUnion);
			floatIntUnion.Float = value;
			return floatIntUnion.Int;
		}

		// Token: 0x060007FB RID: 2043 RVA: 0x0001C55C File Offset: 0x0001A75C
		public static long ReinterpretAsLong(double value)
		{
			FloatingPointNumerics.DoubleLongUnion doubleLongUnion = default(FloatingPointNumerics.DoubleLongUnion);
			doubleLongUnion.Double = value;
			return doubleLongUnion.Long;
		}

		// Token: 0x060007FC RID: 2044 RVA: 0x0001C588 File Offset: 0x0001A788
		public static float ReinterpretAsFloat(int value)
		{
			FloatingPointNumerics.FloatIntUnion floatIntUnion = default(FloatingPointNumerics.FloatIntUnion);
			floatIntUnion.Int = value;
			return floatIntUnion.Float;
		}

		// Token: 0x060007FD RID: 2045 RVA: 0x0001C5B4 File Offset: 0x0001A7B4
		public static double ReinterpretAsDouble(long value)
		{
			FloatingPointNumerics.DoubleLongUnion doubleLongUnion = default(FloatingPointNumerics.DoubleLongUnion);
			doubleLongUnion.Long = value;
			return doubleLongUnion.Double;
		}

		// Token: 0x060007FE RID: 2046 RVA: 0x0001C5DD File Offset: 0x0001A7DD
		private FloatingPointNumerics()
		{
		}

		// Token: 0x0200012D RID: 301
		[StructLayout(LayoutKind.Explicit)]
		private struct FloatIntUnion
		{
			// Token: 0x040001BF RID: 447
			[FieldOffset(0)]
			public float Float;

			// Token: 0x040001C0 RID: 448
			[FieldOffset(0)]
			public int Int;

			// Token: 0x040001C1 RID: 449
			[FieldOffset(0)]
			public uint UInt;
		}

		// Token: 0x0200012E RID: 302
		[StructLayout(LayoutKind.Explicit)]
		private struct DoubleLongUnion
		{
			// Token: 0x040001C2 RID: 450
			[FieldOffset(0)]
			public double Double;

			// Token: 0x040001C3 RID: 451
			[FieldOffset(0)]
			public long Long;

			// Token: 0x040001C4 RID: 452
			[FieldOffset(0)]
			public ulong ULong;
		}
	}
}
