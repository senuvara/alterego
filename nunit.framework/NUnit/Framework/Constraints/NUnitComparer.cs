﻿using System;
using System.Collections;
using System.Reflection;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000158 RID: 344
	public class NUnitComparer : IComparer
	{
		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000901 RID: 2305 RVA: 0x0001E810 File Offset: 0x0001CA10
		public static NUnitComparer Default
		{
			get
			{
				return new NUnitComparer();
			}
		}

		// Token: 0x06000902 RID: 2306 RVA: 0x0001E828 File Offset: 0x0001CA28
		public int Compare(object x, object y)
		{
			int result;
			if (x == null)
			{
				result = ((y == null) ? 0 : -1);
			}
			else if (y == null)
			{
				result = 1;
			}
			else if (x is char && y is char)
			{
				result = (((char)x == (char)y) ? 0 : 1);
			}
			else if (Numerics.IsNumericType(x) && Numerics.IsNumericType(y))
			{
				result = Numerics.Compare(x, y);
			}
			else if (x is IComparable)
			{
				result = ((IComparable)x).CompareTo(y);
			}
			else if (y is IComparable)
			{
				result = -((IComparable)y).CompareTo(x);
			}
			else
			{
				Type type = x.GetType();
				Type type2 = y.GetType();
				MethodInfo method = type.GetMethod("CompareTo", new Type[]
				{
					type2
				});
				if (method != null)
				{
					result = (int)method.Invoke(x, new object[]
					{
						y
					});
				}
				else
				{
					method = type2.GetMethod("CompareTo", new Type[]
					{
						type
					});
					if (method == null)
					{
						throw new ArgumentException("Neither value implements IComparable or IComparable<T>");
					}
					result = -(int)method.Invoke(y, new object[]
					{
						x
					});
				}
			}
			return result;
		}

		// Token: 0x06000903 RID: 2307 RVA: 0x0001E9AA File Offset: 0x0001CBAA
		public NUnitComparer()
		{
		}
	}
}
