﻿using System;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200011F RID: 287
	public class SamePathConstraint : PathConstraint
	{
		// Token: 0x060007B6 RID: 1974 RVA: 0x0001B720 File Offset: 0x00019920
		public SamePathConstraint(string expected) : base(expected)
		{
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x060007B7 RID: 1975 RVA: 0x0001B72C File Offset: 0x0001992C
		public override string Description
		{
			get
			{
				return "Path matching " + MsgUtils.FormatValue(this.expected);
			}
		}

		// Token: 0x060007B8 RID: 1976 RVA: 0x0001B754 File Offset: 0x00019954
		protected override bool Matches(string actual)
		{
			return actual != null && StringUtil.StringsEqual(base.Canonicalize(this.expected), base.Canonicalize(actual), this.caseInsensitive);
		}
	}
}
