﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200002D RID: 45
	public class UniqueItemsConstraint : CollectionItemsEqualConstraint
	{
		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600018D RID: 397 RVA: 0x00005DD4 File Offset: 0x00003FD4
		public override string Description
		{
			get
			{
				return "all items unique";
			}
		}

		// Token: 0x0600018E RID: 398 RVA: 0x00005DEC File Offset: 0x00003FEC
		protected override bool Matches(IEnumerable actual)
		{
			List<object> list = new List<object>();
			foreach (object obj in actual)
			{
				foreach (object y in list)
				{
					if (base.ItemsEqual(obj, y))
					{
						return false;
					}
				}
				list.Add(obj);
			}
			return true;
		}

		// Token: 0x0600018F RID: 399 RVA: 0x00005EB4 File Offset: 0x000040B4
		public UniqueItemsConstraint()
		{
		}
	}
}
