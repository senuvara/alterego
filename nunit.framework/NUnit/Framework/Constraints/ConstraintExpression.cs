﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200014A RID: 330
	public class ConstraintExpression
	{
		// Token: 0x06000898 RID: 2200 RVA: 0x0001D926 File Offset: 0x0001BB26
		public ConstraintExpression()
		{
			this.builder = new ConstraintBuilder();
		}

		// Token: 0x06000899 RID: 2201 RVA: 0x0001D93C File Offset: 0x0001BB3C
		public ConstraintExpression(ConstraintBuilder builder)
		{
			this.builder = builder;
		}

		// Token: 0x0600089A RID: 2202 RVA: 0x0001D950 File Offset: 0x0001BB50
		public override string ToString()
		{
			return this.builder.Resolve().ToString();
		}

		// Token: 0x0600089B RID: 2203 RVA: 0x0001D974 File Offset: 0x0001BB74
		public ConstraintExpression Append(ConstraintOperator op)
		{
			this.builder.Append(op);
			return this;
		}

		// Token: 0x0600089C RID: 2204 RVA: 0x0001D994 File Offset: 0x0001BB94
		public ResolvableConstraintExpression Append(SelfResolvingOperator op)
		{
			this.builder.Append(op);
			return new ResolvableConstraintExpression(this.builder);
		}

		// Token: 0x0600089D RID: 2205 RVA: 0x0001D9C0 File Offset: 0x0001BBC0
		public Constraint Append(Constraint constraint)
		{
			this.builder.Append(constraint);
			return constraint;
		}

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x0600089E RID: 2206 RVA: 0x0001D9E0 File Offset: 0x0001BBE0
		public ConstraintExpression Not
		{
			get
			{
				return this.Append(new NotOperator());
			}
		}

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x0600089F RID: 2207 RVA: 0x0001DA00 File Offset: 0x0001BC00
		public ConstraintExpression No
		{
			get
			{
				return this.Append(new NotOperator());
			}
		}

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x060008A0 RID: 2208 RVA: 0x0001DA20 File Offset: 0x0001BC20
		public ConstraintExpression All
		{
			get
			{
				return this.Append(new AllOperator());
			}
		}

		// Token: 0x170001EB RID: 491
		// (get) Token: 0x060008A1 RID: 2209 RVA: 0x0001DA40 File Offset: 0x0001BC40
		public ConstraintExpression Some
		{
			get
			{
				return this.Append(new SomeOperator());
			}
		}

		// Token: 0x170001EC RID: 492
		// (get) Token: 0x060008A2 RID: 2210 RVA: 0x0001DA60 File Offset: 0x0001BC60
		public ConstraintExpression None
		{
			get
			{
				return this.Append(new NoneOperator());
			}
		}

		// Token: 0x060008A3 RID: 2211 RVA: 0x0001DA80 File Offset: 0x0001BC80
		public ConstraintExpression Exactly(int expectedCount)
		{
			return this.Append(new ExactCountOperator(expectedCount));
		}

		// Token: 0x060008A4 RID: 2212 RVA: 0x0001DAA0 File Offset: 0x0001BCA0
		public ResolvableConstraintExpression Property(string name)
		{
			return this.Append(new PropOperator(name));
		}

		// Token: 0x170001ED RID: 493
		// (get) Token: 0x060008A5 RID: 2213 RVA: 0x0001DAC0 File Offset: 0x0001BCC0
		public ResolvableConstraintExpression Length
		{
			get
			{
				return this.Property("Length");
			}
		}

		// Token: 0x170001EE RID: 494
		// (get) Token: 0x060008A6 RID: 2214 RVA: 0x0001DAE0 File Offset: 0x0001BCE0
		public ResolvableConstraintExpression Count
		{
			get
			{
				return this.Property("Count");
			}
		}

		// Token: 0x170001EF RID: 495
		// (get) Token: 0x060008A7 RID: 2215 RVA: 0x0001DB00 File Offset: 0x0001BD00
		public ResolvableConstraintExpression Message
		{
			get
			{
				return this.Property("Message");
			}
		}

		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x060008A8 RID: 2216 RVA: 0x0001DB20 File Offset: 0x0001BD20
		public ResolvableConstraintExpression InnerException
		{
			get
			{
				return this.Property("InnerException");
			}
		}

		// Token: 0x060008A9 RID: 2217 RVA: 0x0001DB40 File Offset: 0x0001BD40
		public ResolvableConstraintExpression Attribute(Type expectedType)
		{
			return this.Append(new AttributeOperator(expectedType));
		}

		// Token: 0x060008AA RID: 2218 RVA: 0x0001DB60 File Offset: 0x0001BD60
		public ResolvableConstraintExpression Attribute<TExpected>()
		{
			return this.Attribute(typeof(TExpected));
		}

		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x060008AB RID: 2219 RVA: 0x0001DB84 File Offset: 0x0001BD84
		public ConstraintExpression With
		{
			get
			{
				return this.Append(new WithOperator());
			}
		}

		// Token: 0x060008AC RID: 2220 RVA: 0x0001DBA4 File Offset: 0x0001BDA4
		public Constraint Matches(IResolveConstraint constraint)
		{
			return this.Append((Constraint)constraint.Resolve());
		}

		// Token: 0x060008AD RID: 2221 RVA: 0x0001DBC8 File Offset: 0x0001BDC8
		public Constraint Matches<TActual>(Predicate<TActual> predicate)
		{
			return this.Append(new PredicateConstraint<TActual>(predicate));
		}

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x060008AE RID: 2222 RVA: 0x0001DBE8 File Offset: 0x0001BDE8
		public NullConstraint Null
		{
			get
			{
				return (NullConstraint)this.Append(new NullConstraint());
			}
		}

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x060008AF RID: 2223 RVA: 0x0001DC0C File Offset: 0x0001BE0C
		public TrueConstraint True
		{
			get
			{
				return (TrueConstraint)this.Append(new TrueConstraint());
			}
		}

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x060008B0 RID: 2224 RVA: 0x0001DC30 File Offset: 0x0001BE30
		public FalseConstraint False
		{
			get
			{
				return (FalseConstraint)this.Append(new FalseConstraint());
			}
		}

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x060008B1 RID: 2225 RVA: 0x0001DC54 File Offset: 0x0001BE54
		public GreaterThanConstraint Positive
		{
			get
			{
				return (GreaterThanConstraint)this.Append(new GreaterThanConstraint(0));
			}
		}

		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x060008B2 RID: 2226 RVA: 0x0001DC7C File Offset: 0x0001BE7C
		public LessThanConstraint Negative
		{
			get
			{
				return (LessThanConstraint)this.Append(new LessThanConstraint(0));
			}
		}

		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x060008B3 RID: 2227 RVA: 0x0001DCA4 File Offset: 0x0001BEA4
		public EqualConstraint Zero
		{
			get
			{
				return (EqualConstraint)this.Append(new EqualConstraint(0));
			}
		}

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x060008B4 RID: 2228 RVA: 0x0001DCCC File Offset: 0x0001BECC
		public NaNConstraint NaN
		{
			get
			{
				return (NaNConstraint)this.Append(new NaNConstraint());
			}
		}

		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x060008B5 RID: 2229 RVA: 0x0001DCF0 File Offset: 0x0001BEF0
		public EmptyConstraint Empty
		{
			get
			{
				return (EmptyConstraint)this.Append(new EmptyConstraint());
			}
		}

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x060008B6 RID: 2230 RVA: 0x0001DD14 File Offset: 0x0001BF14
		public UniqueItemsConstraint Unique
		{
			get
			{
				return (UniqueItemsConstraint)this.Append(new UniqueItemsConstraint());
			}
		}

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x060008B7 RID: 2231 RVA: 0x0001DD38 File Offset: 0x0001BF38
		public BinarySerializableConstraint BinarySerializable
		{
			get
			{
				return (BinarySerializableConstraint)this.Append(new BinarySerializableConstraint());
			}
		}

		// Token: 0x170001FC RID: 508
		// (get) Token: 0x060008B8 RID: 2232 RVA: 0x0001DD5C File Offset: 0x0001BF5C
		public XmlSerializableConstraint XmlSerializable
		{
			get
			{
				return (XmlSerializableConstraint)this.Append(new XmlSerializableConstraint());
			}
		}

		// Token: 0x060008B9 RID: 2233 RVA: 0x0001DD80 File Offset: 0x0001BF80
		public EqualConstraint EqualTo(object expected)
		{
			return (EqualConstraint)this.Append(new EqualConstraint(expected));
		}

		// Token: 0x060008BA RID: 2234 RVA: 0x0001DDA4 File Offset: 0x0001BFA4
		public SameAsConstraint SameAs(object expected)
		{
			return (SameAsConstraint)this.Append(new SameAsConstraint(expected));
		}

		// Token: 0x060008BB RID: 2235 RVA: 0x0001DDC8 File Offset: 0x0001BFC8
		public GreaterThanConstraint GreaterThan(object expected)
		{
			return (GreaterThanConstraint)this.Append(new GreaterThanConstraint(expected));
		}

		// Token: 0x060008BC RID: 2236 RVA: 0x0001DDEC File Offset: 0x0001BFEC
		public GreaterThanOrEqualConstraint GreaterThanOrEqualTo(object expected)
		{
			return (GreaterThanOrEqualConstraint)this.Append(new GreaterThanOrEqualConstraint(expected));
		}

		// Token: 0x060008BD RID: 2237 RVA: 0x0001DE10 File Offset: 0x0001C010
		public GreaterThanOrEqualConstraint AtLeast(object expected)
		{
			return (GreaterThanOrEqualConstraint)this.Append(new GreaterThanOrEqualConstraint(expected));
		}

		// Token: 0x060008BE RID: 2238 RVA: 0x0001DE34 File Offset: 0x0001C034
		public LessThanConstraint LessThan(object expected)
		{
			return (LessThanConstraint)this.Append(new LessThanConstraint(expected));
		}

		// Token: 0x060008BF RID: 2239 RVA: 0x0001DE58 File Offset: 0x0001C058
		public LessThanOrEqualConstraint LessThanOrEqualTo(object expected)
		{
			return (LessThanOrEqualConstraint)this.Append(new LessThanOrEqualConstraint(expected));
		}

		// Token: 0x060008C0 RID: 2240 RVA: 0x0001DE7C File Offset: 0x0001C07C
		public LessThanOrEqualConstraint AtMost(object expected)
		{
			return (LessThanOrEqualConstraint)this.Append(new LessThanOrEqualConstraint(expected));
		}

		// Token: 0x060008C1 RID: 2241 RVA: 0x0001DEA0 File Offset: 0x0001C0A0
		public ExactTypeConstraint TypeOf(Type expectedType)
		{
			return (ExactTypeConstraint)this.Append(new ExactTypeConstraint(expectedType));
		}

		// Token: 0x060008C2 RID: 2242 RVA: 0x0001DEC4 File Offset: 0x0001C0C4
		public ExactTypeConstraint TypeOf<TExpected>()
		{
			return (ExactTypeConstraint)this.Append(new ExactTypeConstraint(typeof(TExpected)));
		}

		// Token: 0x060008C3 RID: 2243 RVA: 0x0001DEF0 File Offset: 0x0001C0F0
		public InstanceOfTypeConstraint InstanceOf(Type expectedType)
		{
			return (InstanceOfTypeConstraint)this.Append(new InstanceOfTypeConstraint(expectedType));
		}

		// Token: 0x060008C4 RID: 2244 RVA: 0x0001DF14 File Offset: 0x0001C114
		public InstanceOfTypeConstraint InstanceOf<TExpected>()
		{
			return (InstanceOfTypeConstraint)this.Append(new InstanceOfTypeConstraint(typeof(TExpected)));
		}

		// Token: 0x060008C5 RID: 2245 RVA: 0x0001DF40 File Offset: 0x0001C140
		public AssignableFromConstraint AssignableFrom(Type expectedType)
		{
			return (AssignableFromConstraint)this.Append(new AssignableFromConstraint(expectedType));
		}

		// Token: 0x060008C6 RID: 2246 RVA: 0x0001DF64 File Offset: 0x0001C164
		public AssignableFromConstraint AssignableFrom<TExpected>()
		{
			return (AssignableFromConstraint)this.Append(new AssignableFromConstraint(typeof(TExpected)));
		}

		// Token: 0x060008C7 RID: 2247 RVA: 0x0001DF90 File Offset: 0x0001C190
		public AssignableToConstraint AssignableTo(Type expectedType)
		{
			return (AssignableToConstraint)this.Append(new AssignableToConstraint(expectedType));
		}

		// Token: 0x060008C8 RID: 2248 RVA: 0x0001DFB4 File Offset: 0x0001C1B4
		public AssignableToConstraint AssignableTo<TExpected>()
		{
			return (AssignableToConstraint)this.Append(new AssignableToConstraint(typeof(TExpected)));
		}

		// Token: 0x060008C9 RID: 2249 RVA: 0x0001DFE0 File Offset: 0x0001C1E0
		public CollectionEquivalentConstraint EquivalentTo(IEnumerable expected)
		{
			return (CollectionEquivalentConstraint)this.Append(new CollectionEquivalentConstraint(expected));
		}

		// Token: 0x060008CA RID: 2250 RVA: 0x0001E004 File Offset: 0x0001C204
		public CollectionSubsetConstraint SubsetOf(IEnumerable expected)
		{
			return (CollectionSubsetConstraint)this.Append(new CollectionSubsetConstraint(expected));
		}

		// Token: 0x060008CB RID: 2251 RVA: 0x0001E028 File Offset: 0x0001C228
		public CollectionSupersetConstraint SupersetOf(IEnumerable expected)
		{
			return (CollectionSupersetConstraint)this.Append(new CollectionSupersetConstraint(expected));
		}

		// Token: 0x170001FD RID: 509
		// (get) Token: 0x060008CC RID: 2252 RVA: 0x0001E04C File Offset: 0x0001C24C
		public CollectionOrderedConstraint Ordered
		{
			get
			{
				return (CollectionOrderedConstraint)this.Append(new CollectionOrderedConstraint());
			}
		}

		// Token: 0x060008CD RID: 2253 RVA: 0x0001E070 File Offset: 0x0001C270
		public CollectionContainsConstraint Member(object expected)
		{
			return (CollectionContainsConstraint)this.Append(new CollectionContainsConstraint(expected));
		}

		// Token: 0x060008CE RID: 2254 RVA: 0x0001E094 File Offset: 0x0001C294
		public CollectionContainsConstraint Contains(object expected)
		{
			return (CollectionContainsConstraint)this.Append(new CollectionContainsConstraint(expected));
		}

		// Token: 0x060008CF RID: 2255 RVA: 0x0001E0B8 File Offset: 0x0001C2B8
		public ContainsConstraint Contains(string expected)
		{
			return (ContainsConstraint)this.Append(new ContainsConstraint(expected));
		}

		// Token: 0x060008D0 RID: 2256 RVA: 0x0001E0DC File Offset: 0x0001C2DC
		public ContainsConstraint Contain(string expected)
		{
			return (ContainsConstraint)this.Append(new ContainsConstraint(expected));
		}

		// Token: 0x060008D1 RID: 2257 RVA: 0x0001E100 File Offset: 0x0001C300
		[Obsolete("Deprecated, use Contains")]
		public SubstringConstraint StringContaining(string expected)
		{
			return (SubstringConstraint)this.Append(new SubstringConstraint(expected));
		}

		// Token: 0x060008D2 RID: 2258 RVA: 0x0001E124 File Offset: 0x0001C324
		[Obsolete("Deprecated, use Contains")]
		public SubstringConstraint ContainsSubstring(string expected)
		{
			return (SubstringConstraint)this.Append(new SubstringConstraint(expected));
		}

		// Token: 0x060008D3 RID: 2259 RVA: 0x0001E148 File Offset: 0x0001C348
		public StartsWithConstraint StartWith(string expected)
		{
			return (StartsWithConstraint)this.Append(new StartsWithConstraint(expected));
		}

		// Token: 0x060008D4 RID: 2260 RVA: 0x0001E16C File Offset: 0x0001C36C
		public StartsWithConstraint StartsWith(string expected)
		{
			return (StartsWithConstraint)this.Append(new StartsWithConstraint(expected));
		}

		// Token: 0x060008D5 RID: 2261 RVA: 0x0001E190 File Offset: 0x0001C390
		[Obsolete("Deprecated, use Does.StartWith or StartsWith")]
		public StartsWithConstraint StringStarting(string expected)
		{
			return (StartsWithConstraint)this.Append(new StartsWithConstraint(expected));
		}

		// Token: 0x060008D6 RID: 2262 RVA: 0x0001E1B4 File Offset: 0x0001C3B4
		public EndsWithConstraint EndWith(string expected)
		{
			return (EndsWithConstraint)this.Append(new EndsWithConstraint(expected));
		}

		// Token: 0x060008D7 RID: 2263 RVA: 0x0001E1D8 File Offset: 0x0001C3D8
		public EndsWithConstraint EndsWith(string expected)
		{
			return (EndsWithConstraint)this.Append(new EndsWithConstraint(expected));
		}

		// Token: 0x060008D8 RID: 2264 RVA: 0x0001E1FC File Offset: 0x0001C3FC
		[Obsolete("Deprecated, use Does.EndWith or EndsWith")]
		public EndsWithConstraint StringEnding(string expected)
		{
			return (EndsWithConstraint)this.Append(new EndsWithConstraint(expected));
		}

		// Token: 0x060008D9 RID: 2265 RVA: 0x0001E220 File Offset: 0x0001C420
		public RegexConstraint Match(string pattern)
		{
			return (RegexConstraint)this.Append(new RegexConstraint(pattern));
		}

		// Token: 0x060008DA RID: 2266 RVA: 0x0001E244 File Offset: 0x0001C444
		public RegexConstraint Matches(string pattern)
		{
			return (RegexConstraint)this.Append(new RegexConstraint(pattern));
		}

		// Token: 0x060008DB RID: 2267 RVA: 0x0001E268 File Offset: 0x0001C468
		[Obsolete("Deprecated, use Does.Match or Matches")]
		public RegexConstraint StringMatching(string pattern)
		{
			return (RegexConstraint)this.Append(new RegexConstraint(pattern));
		}

		// Token: 0x060008DC RID: 2268 RVA: 0x0001E28C File Offset: 0x0001C48C
		public SamePathConstraint SamePath(string expected)
		{
			return (SamePathConstraint)this.Append(new SamePathConstraint(expected));
		}

		// Token: 0x060008DD RID: 2269 RVA: 0x0001E2B0 File Offset: 0x0001C4B0
		public SubPathConstraint SubPathOf(string expected)
		{
			return (SubPathConstraint)this.Append(new SubPathConstraint(expected));
		}

		// Token: 0x060008DE RID: 2270 RVA: 0x0001E2D4 File Offset: 0x0001C4D4
		public SamePathOrUnderConstraint SamePathOrUnder(string expected)
		{
			return (SamePathOrUnderConstraint)this.Append(new SamePathOrUnderConstraint(expected));
		}

		// Token: 0x060008DF RID: 2271 RVA: 0x0001E2F8 File Offset: 0x0001C4F8
		public RangeConstraint InRange(IComparable from, IComparable to)
		{
			return (RangeConstraint)this.Append(new RangeConstraint(from, to));
		}

		// Token: 0x170001FE RID: 510
		// (get) Token: 0x060008E0 RID: 2272 RVA: 0x0001E31C File Offset: 0x0001C51C
		public Constraint Exist
		{
			get
			{
				return this.Append(new FileOrDirectoryExistsConstraint());
			}
		}

		// Token: 0x040001EA RID: 490
		protected ConstraintBuilder builder;
	}
}
