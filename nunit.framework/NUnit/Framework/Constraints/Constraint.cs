﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using NUnit.Compatibility;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200000B RID: 11
	public abstract class Constraint : IConstraint, IResolveConstraint
	{
		// Token: 0x06000048 RID: 72 RVA: 0x00002A90 File Offset: 0x00000C90
		protected Constraint(params object[] args)
		{
			this.Arguments = args;
			this._displayName = new System.Lazy<string>(delegate()
			{
				Type type = base.GetType();
				string text = type.Name;
				if (type.GetTypeInfo().IsGenericType)
				{
					text = text.Substring(0, text.Length - 2);
				}
				if (text.EndsWith("Constraint", StringComparison.Ordinal))
				{
					text = text.Substring(0, text.Length - 10);
				}
				return text;
			});
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000049 RID: 73 RVA: 0x00002AD0 File Offset: 0x00000CD0
		public virtual string DisplayName
		{
			get
			{
				return this._displayName.Value;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600004A RID: 74 RVA: 0x00002AF0 File Offset: 0x00000CF0
		// (set) Token: 0x0600004B RID: 75 RVA: 0x00002B07 File Offset: 0x00000D07
		public virtual string Description
		{
			[CompilerGenerated]
			get
			{
				return this.<Description>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<Description>k__BackingField = value;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00002B10 File Offset: 0x00000D10
		// (set) Token: 0x0600004D RID: 77 RVA: 0x00002B27 File Offset: 0x00000D27
		public object[] Arguments
		{
			[CompilerGenerated]
			get
			{
				return this.<Arguments>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Arguments>k__BackingField = value;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600004E RID: 78 RVA: 0x00002B30 File Offset: 0x00000D30
		// (set) Token: 0x0600004F RID: 79 RVA: 0x00002B47 File Offset: 0x00000D47
		public ConstraintBuilder Builder
		{
			[CompilerGenerated]
			get
			{
				return this.<Builder>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Builder>k__BackingField = value;
			}
		}

		// Token: 0x06000050 RID: 80
		public abstract ConstraintResult ApplyTo(object actual);

		// Token: 0x06000051 RID: 81 RVA: 0x00002B50 File Offset: 0x00000D50
		public virtual ConstraintResult ApplyTo<TActual>(ActualValueDelegate<TActual> del)
		{
			return this.ApplyTo(this.GetTestObject<TActual>(del));
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00002B70 File Offset: 0x00000D70
		public virtual ConstraintResult ApplyTo<TActual>(ref TActual actual)
		{
			return this.ApplyTo(actual);
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00002B94 File Offset: 0x00000D94
		protected virtual object GetTestObject<TActual>(ActualValueDelegate<TActual> del)
		{
			return del();
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00002BB4 File Offset: 0x00000DB4
		public override string ToString()
		{
			string stringRepresentation = this.GetStringRepresentation();
			return (this.Builder == null) ? stringRepresentation : string.Format("<unresolved {0}>", stringRepresentation);
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00002BE4 File Offset: 0x00000DE4
		protected virtual string GetStringRepresentation()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("<");
			stringBuilder.Append(this.DisplayName.ToLower());
			foreach (object o in this.Arguments)
			{
				stringBuilder.Append(" ");
				stringBuilder.Append(Constraint._displayable(o));
			}
			stringBuilder.Append(">");
			return stringBuilder.ToString();
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00002C6C File Offset: 0x00000E6C
		private static string _displayable(object o)
		{
			string result;
			if (o == null)
			{
				result = "null";
			}
			else
			{
				string format = (o is string) ? "\"{0}\"" : "{0}";
				result = string.Format(CultureInfo.InvariantCulture, format, new object[]
				{
					o
				});
			}
			return result;
		}

		// Token: 0x06000057 RID: 87 RVA: 0x00002CC0 File Offset: 0x00000EC0
		public static Constraint operator &(Constraint left, Constraint right)
		{
			return new AndConstraint(((IResolveConstraint)left).Resolve(), ((IResolveConstraint)right).Resolve());
		}

		// Token: 0x06000058 RID: 88 RVA: 0x00002CE8 File Offset: 0x00000EE8
		public static Constraint operator |(Constraint left, Constraint right)
		{
			return new OrConstraint(((IResolveConstraint)left).Resolve(), ((IResolveConstraint)right).Resolve());
		}

		// Token: 0x06000059 RID: 89 RVA: 0x00002D10 File Offset: 0x00000F10
		public static Constraint operator !(Constraint constraint)
		{
			return new NotConstraint(((IResolveConstraint)constraint).Resolve());
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600005A RID: 90 RVA: 0x00002D30 File Offset: 0x00000F30
		public ConstraintExpression And
		{
			get
			{
				ConstraintBuilder constraintBuilder = this.Builder;
				if (constraintBuilder == null)
				{
					constraintBuilder = new ConstraintBuilder();
					constraintBuilder.Append(this);
				}
				constraintBuilder.Append(new AndOperator());
				return new ConstraintExpression(constraintBuilder);
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600005B RID: 91 RVA: 0x00002D78 File Offset: 0x00000F78
		public ConstraintExpression With
		{
			get
			{
				return this.And;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600005C RID: 92 RVA: 0x00002D90 File Offset: 0x00000F90
		public ConstraintExpression Or
		{
			get
			{
				ConstraintBuilder constraintBuilder = this.Builder;
				if (constraintBuilder == null)
				{
					constraintBuilder = new ConstraintBuilder();
					constraintBuilder.Append(this);
				}
				constraintBuilder.Append(new OrOperator());
				return new ConstraintExpression(constraintBuilder);
			}
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00002DD8 File Offset: 0x00000FD8
		public DelayedConstraint After(int delayInMilliseconds)
		{
			return new DelayedConstraint((this.Builder == null) ? this : this.Builder.Resolve(), delayInMilliseconds);
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00002E08 File Offset: 0x00001008
		public DelayedConstraint After(int delayInMilliseconds, int pollingInterval)
		{
			return new DelayedConstraint((this.Builder == null) ? this : this.Builder.Resolve(), delayInMilliseconds, pollingInterval);
		}

		// Token: 0x0600005F RID: 95 RVA: 0x00002E38 File Offset: 0x00001038
		IConstraint IResolveConstraint.Resolve()
		{
			return (this.Builder == null) ? this : this.Builder.Resolve();
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00002A28 File Offset: 0x00000C28
		[CompilerGenerated]
		private string <.ctor>b__0()
		{
			Type type = base.GetType();
			string text = type.Name;
			if (type.GetTypeInfo().IsGenericType)
			{
				text = text.Substring(0, text.Length - 2);
			}
			if (text.EndsWith("Constraint", StringComparison.Ordinal))
			{
				text = text.Substring(0, text.Length - 10);
			}
			return text;
		}

		// Token: 0x04000004 RID: 4
		private System.Lazy<string> _displayName;

		// Token: 0x04000005 RID: 5
		[CompilerGenerated]
		private string <Description>k__BackingField;

		// Token: 0x04000006 RID: 6
		[CompilerGenerated]
		private object[] <Arguments>k__BackingField;

		// Token: 0x04000007 RID: 7
		[CompilerGenerated]
		private ConstraintBuilder <Builder>k__BackingField;
	}
}
