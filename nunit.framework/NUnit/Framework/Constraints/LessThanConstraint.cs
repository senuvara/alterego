﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000103 RID: 259
	public class LessThanConstraint : ComparisonConstraint
	{
		// Token: 0x0600073B RID: 1851 RVA: 0x0001A0F7 File Offset: 0x000182F7
		public LessThanConstraint(object expected) : base(expected, true, false, false, "less than")
		{
		}
	}
}
