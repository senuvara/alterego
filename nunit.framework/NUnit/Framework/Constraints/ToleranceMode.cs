﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020001A0 RID: 416
	public enum ToleranceMode
	{
		// Token: 0x04000282 RID: 642
		Unset,
		// Token: 0x04000283 RID: 643
		Linear,
		// Token: 0x04000284 RID: 644
		Percent,
		// Token: 0x04000285 RID: 645
		Ulps
	}
}
