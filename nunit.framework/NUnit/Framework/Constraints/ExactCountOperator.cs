﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000152 RID: 338
	public class ExactCountOperator : CollectionOperator
	{
		// Token: 0x060008F0 RID: 2288 RVA: 0x0001E52D File Offset: 0x0001C72D
		public ExactCountOperator(int expectedCount)
		{
			this.expectedCount = expectedCount;
		}

		// Token: 0x060008F1 RID: 2289 RVA: 0x0001E540 File Offset: 0x0001C740
		public override IConstraint ApplyPrefix(IConstraint constraint)
		{
			return new ExactCountConstraint(this.expectedCount, constraint);
		}

		// Token: 0x040001FA RID: 506
		private int expectedCount;
	}
}
