﻿using System;
using System.IO;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000D0 RID: 208
	public class EmptyConstraint : Constraint
	{
		// Token: 0x1700016D RID: 365
		// (get) Token: 0x06000656 RID: 1622 RVA: 0x00016848 File Offset: 0x00014A48
		public override string Description
		{
			get
			{
				return (this.realConstraint == null) ? "<empty>" : this.realConstraint.Description;
			}
		}

		// Token: 0x06000657 RID: 1623 RVA: 0x00016874 File Offset: 0x00014A74
		public override ConstraintResult ApplyTo(object actual)
		{
			if (actual.GetType() == typeof(string))
			{
				this.realConstraint = new EmptyStringConstraint();
			}
			else
			{
				if (actual == null)
				{
					throw new ArgumentException("The actual value must be a string or a non-null IEnumerable or DirectoryInfo", "actual");
				}
				if (actual is DirectoryInfo)
				{
					this.realConstraint = new EmptyDirectoryConstraint();
				}
				else
				{
					this.realConstraint = new EmptyCollectionConstraint();
				}
			}
			return this.realConstraint.ApplyTo(actual);
		}

		// Token: 0x06000658 RID: 1624 RVA: 0x000168FB File Offset: 0x00014AFB
		public EmptyConstraint() : base(new object[0])
		{
		}

		// Token: 0x04000156 RID: 342
		private Constraint realConstraint;
	}
}
