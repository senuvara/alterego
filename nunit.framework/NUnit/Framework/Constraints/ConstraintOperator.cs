﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000054 RID: 84
	public abstract class ConstraintOperator
	{
		// Token: 0x170000AD RID: 173
		// (get) Token: 0x0600027C RID: 636 RVA: 0x00009240 File Offset: 0x00007440
		// (set) Token: 0x0600027D RID: 637 RVA: 0x00009258 File Offset: 0x00007458
		public object LeftContext
		{
			get
			{
				return this.leftContext;
			}
			set
			{
				this.leftContext = value;
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x0600027E RID: 638 RVA: 0x00009264 File Offset: 0x00007464
		// (set) Token: 0x0600027F RID: 639 RVA: 0x0000927C File Offset: 0x0000747C
		public object RightContext
		{
			get
			{
				return this.rightContext;
			}
			set
			{
				this.rightContext = value;
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x06000280 RID: 640 RVA: 0x00009288 File Offset: 0x00007488
		public virtual int LeftPrecedence
		{
			get
			{
				return this.left_precedence;
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x06000281 RID: 641 RVA: 0x000092A0 File Offset: 0x000074A0
		public virtual int RightPrecedence
		{
			get
			{
				return this.right_precedence;
			}
		}

		// Token: 0x06000282 RID: 642
		public abstract void Reduce(ConstraintBuilder.ConstraintStack stack);

		// Token: 0x06000283 RID: 643 RVA: 0x000092B8 File Offset: 0x000074B8
		protected ConstraintOperator()
		{
		}

		// Token: 0x04000089 RID: 137
		private object leftContext;

		// Token: 0x0400008A RID: 138
		private object rightContext;

		// Token: 0x0400008B RID: 139
		protected int left_precedence;

		// Token: 0x0400008C RID: 140
		protected int right_precedence;
	}
}
