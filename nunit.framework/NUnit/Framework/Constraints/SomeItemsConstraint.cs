﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200017A RID: 378
	public class SomeItemsConstraint : PrefixConstraint
	{
		// Token: 0x06000A08 RID: 2568 RVA: 0x00021D0F File Offset: 0x0001FF0F
		public SomeItemsConstraint(IConstraint itemConstraint) : base(itemConstraint)
		{
			base.DescriptionPrefix = "some item";
		}

		// Token: 0x1700024C RID: 588
		// (get) Token: 0x06000A09 RID: 2569 RVA: 0x00021D28 File Offset: 0x0001FF28
		public override string DisplayName
		{
			get
			{
				return "Some";
			}
		}

		// Token: 0x06000A0A RID: 2570 RVA: 0x00021D40 File Offset: 0x0001FF40
		public override ConstraintResult ApplyTo(object actual)
		{
			if (!(actual is IEnumerable))
			{
				throw new ArgumentException("The actual value must be an IEnumerable", "actual");
			}
			foreach (object actual2 in ((IEnumerable)actual))
			{
				if (base.BaseConstraint.ApplyTo(actual2).IsSuccess)
				{
					return new ConstraintResult(this, actual, ConstraintStatus.Success);
				}
			}
			return new ConstraintResult(this, actual, ConstraintStatus.Failure);
		}
	}
}
