﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200014D RID: 333
	public class NoneOperator : CollectionOperator
	{
		// Token: 0x060008E8 RID: 2280 RVA: 0x0001E480 File Offset: 0x0001C680
		public override IConstraint ApplyPrefix(IConstraint constraint)
		{
			return new NoItemConstraint(constraint);
		}

		// Token: 0x060008E9 RID: 2281 RVA: 0x0001E498 File Offset: 0x0001C698
		public NoneOperator()
		{
		}
	}
}
