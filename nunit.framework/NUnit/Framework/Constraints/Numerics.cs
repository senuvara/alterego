﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000057 RID: 87
	public class Numerics
	{
		// Token: 0x0600028B RID: 651 RVA: 0x00009398 File Offset: 0x00007598
		public static bool IsNumericType(object obj)
		{
			return Numerics.IsFloatingPointNumeric(obj) || Numerics.IsFixedPointNumeric(obj);
		}

		// Token: 0x0600028C RID: 652 RVA: 0x000093BC File Offset: 0x000075BC
		public static bool IsFloatingPointNumeric(object obj)
		{
			if (null != obj)
			{
				if (obj is double)
				{
					return true;
				}
				if (obj is float)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600028D RID: 653 RVA: 0x00009404 File Offset: 0x00007604
		public static bool IsFixedPointNumeric(object obj)
		{
			if (null != obj)
			{
				if (obj is byte)
				{
					return true;
				}
				if (obj is sbyte)
				{
					return true;
				}
				if (obj is decimal)
				{
					return true;
				}
				if (obj is int)
				{
					return true;
				}
				if (obj is uint)
				{
					return true;
				}
				if (obj is long)
				{
					return true;
				}
				if (obj is ulong)
				{
					return true;
				}
				if (obj is short)
				{
					return true;
				}
				if (obj is ushort)
				{
					return true;
				}
				if (obj is char)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600028E RID: 654 RVA: 0x000094F8 File Offset: 0x000076F8
		public static bool AreEqual(object expected, object actual, ref Tolerance tolerance)
		{
			bool result;
			if (expected is double || actual is double)
			{
				result = Numerics.AreEqual(Convert.ToDouble(expected), Convert.ToDouble(actual), ref tolerance);
			}
			else if (expected is float || actual is float)
			{
				result = Numerics.AreEqual(Convert.ToSingle(expected), Convert.ToSingle(actual), ref tolerance);
			}
			else
			{
				if (tolerance.Mode == ToleranceMode.Ulps)
				{
					throw new InvalidOperationException("Ulps may only be specified for floating point arguments");
				}
				if (expected is decimal || actual is decimal)
				{
					result = Numerics.AreEqual(Convert.ToDecimal(expected), Convert.ToDecimal(actual), tolerance);
				}
				else if (expected is ulong || actual is ulong)
				{
					result = Numerics.AreEqual(Convert.ToUInt64(expected), Convert.ToUInt64(actual), tolerance);
				}
				else if (expected is long || actual is long)
				{
					result = Numerics.AreEqual(Convert.ToInt64(expected), Convert.ToInt64(actual), tolerance);
				}
				else if (expected is uint || actual is uint)
				{
					result = Numerics.AreEqual(Convert.ToUInt32(expected), Convert.ToUInt32(actual), tolerance);
				}
				else
				{
					result = Numerics.AreEqual(Convert.ToInt32(expected), Convert.ToInt32(actual), tolerance);
				}
			}
			return result;
		}

		// Token: 0x0600028F RID: 655 RVA: 0x00009668 File Offset: 0x00007868
		private static bool AreEqual(double expected, double actual, ref Tolerance tolerance)
		{
			bool result;
			if (double.IsNaN(expected) && double.IsNaN(actual))
			{
				result = true;
			}
			else if (double.IsInfinity(expected) || double.IsNaN(expected) || double.IsNaN(actual))
			{
				result = expected.Equals(actual);
			}
			else
			{
				if (tolerance.IsUnsetOrDefault && GlobalSettings.DefaultFloatingPointTolerance > 0.0)
				{
					tolerance = new Tolerance(GlobalSettings.DefaultFloatingPointTolerance);
				}
				switch (tolerance.Mode)
				{
				case ToleranceMode.Unset:
					result = expected.Equals(actual);
					break;
				case ToleranceMode.Linear:
					result = (Math.Abs(expected - actual) <= Convert.ToDouble(tolerance.Value));
					break;
				case ToleranceMode.Percent:
					if (expected == 0.0)
					{
						result = expected.Equals(actual);
					}
					else
					{
						double num = Math.Abs((expected - actual) / expected);
						result = (num <= Convert.ToDouble(tolerance.Value) / 100.0);
					}
					break;
				case ToleranceMode.Ulps:
					result = FloatingPointNumerics.AreAlmostEqualUlps(expected, actual, Convert.ToInt64(tolerance.Value));
					break;
				default:
					throw new ArgumentException("Unknown tolerance mode specified", "mode");
				}
			}
			return result;
		}

		// Token: 0x06000290 RID: 656 RVA: 0x000097B4 File Offset: 0x000079B4
		private static bool AreEqual(float expected, float actual, ref Tolerance tolerance)
		{
			bool result;
			if (float.IsNaN(expected) && float.IsNaN(actual))
			{
				result = true;
			}
			else if (float.IsInfinity(expected) || float.IsNaN(expected) || float.IsNaN(actual))
			{
				result = expected.Equals(actual);
			}
			else
			{
				if (tolerance.IsUnsetOrDefault && GlobalSettings.DefaultFloatingPointTolerance > 0.0)
				{
					tolerance = new Tolerance(GlobalSettings.DefaultFloatingPointTolerance);
				}
				switch (tolerance.Mode)
				{
				case ToleranceMode.Unset:
					result = expected.Equals(actual);
					break;
				case ToleranceMode.Linear:
					result = ((double)Math.Abs(expected - actual) <= Convert.ToDouble(tolerance.Value));
					break;
				case ToleranceMode.Percent:
					if (expected == 0f)
					{
						result = expected.Equals(actual);
					}
					else
					{
						float num = Math.Abs((expected - actual) / expected);
						result = (num <= Convert.ToSingle(tolerance.Value) / 100f);
					}
					break;
				case ToleranceMode.Ulps:
					result = FloatingPointNumerics.AreAlmostEqualUlps(expected, actual, Convert.ToInt32(tolerance.Value));
					break;
				default:
					throw new ArgumentException("Unknown tolerance mode specified", "mode");
				}
			}
			return result;
		}

		// Token: 0x06000291 RID: 657 RVA: 0x000098FC File Offset: 0x00007AFC
		private static bool AreEqual(decimal expected, decimal actual, Tolerance tolerance)
		{
			bool result;
			switch (tolerance.Mode)
			{
			case ToleranceMode.Unset:
				result = expected.Equals(actual);
				break;
			case ToleranceMode.Linear:
			{
				decimal num = Convert.ToDecimal(tolerance.Value);
				if (num > 0m)
				{
					result = (Math.Abs(expected - actual) <= num);
				}
				else
				{
					result = expected.Equals(actual);
				}
				break;
			}
			case ToleranceMode.Percent:
				if (expected == 0m)
				{
					result = expected.Equals(actual);
				}
				else
				{
					double num2 = Math.Abs((double)(expected - actual) / (double)expected);
					result = (num2 <= Convert.ToDouble(tolerance.Value) / 100.0);
				}
				break;
			default:
				throw new ArgumentException("Unknown tolerance mode specified", "mode");
			}
			return result;
		}

		// Token: 0x06000292 RID: 658 RVA: 0x000099E4 File Offset: 0x00007BE4
		private static bool AreEqual(ulong expected, ulong actual, Tolerance tolerance)
		{
			bool result;
			switch (tolerance.Mode)
			{
			case ToleranceMode.Unset:
				result = expected.Equals(actual);
				break;
			case ToleranceMode.Linear:
			{
				ulong num = Convert.ToUInt64(tolerance.Value);
				if (num > 0UL)
				{
					ulong num2 = (expected >= actual) ? (expected - actual) : (actual - expected);
					result = (num2 <= num);
				}
				else
				{
					result = expected.Equals(actual);
				}
				break;
			}
			case ToleranceMode.Percent:
				if (expected == 0UL)
				{
					result = expected.Equals(actual);
				}
				else
				{
					ulong num3 = Math.Max(expected, actual) - Math.Min(expected, actual);
					double num4 = Math.Abs(num3 / expected);
					result = (num4 <= Convert.ToDouble(tolerance.Value) / 100.0);
				}
				break;
			default:
				throw new ArgumentException("Unknown tolerance mode specified", "mode");
			}
			return result;
		}

		// Token: 0x06000293 RID: 659 RVA: 0x00009AC8 File Offset: 0x00007CC8
		private static bool AreEqual(long expected, long actual, Tolerance tolerance)
		{
			bool result;
			switch (tolerance.Mode)
			{
			case ToleranceMode.Unset:
				result = expected.Equals(actual);
				break;
			case ToleranceMode.Linear:
			{
				long num = Convert.ToInt64(tolerance.Value);
				if (num > 0L)
				{
					result = (Math.Abs(expected - actual) <= num);
				}
				else
				{
					result = expected.Equals(actual);
				}
				break;
			}
			case ToleranceMode.Percent:
				if (expected == 0L)
				{
					result = expected.Equals(actual);
				}
				else
				{
					double num2 = Math.Abs((double)(expected - actual) / (double)expected);
					result = (num2 <= Convert.ToDouble(tolerance.Value) / 100.0);
				}
				break;
			default:
				throw new ArgumentException("Unknown tolerance mode specified", "mode");
			}
			return result;
		}

		// Token: 0x06000294 RID: 660 RVA: 0x00009B90 File Offset: 0x00007D90
		private static bool AreEqual(uint expected, uint actual, Tolerance tolerance)
		{
			bool result;
			switch (tolerance.Mode)
			{
			case ToleranceMode.Unset:
				result = expected.Equals(actual);
				break;
			case ToleranceMode.Linear:
			{
				uint num = Convert.ToUInt32(tolerance.Value);
				if (num > 0U)
				{
					uint num2 = (expected >= actual) ? (expected - actual) : (actual - expected);
					result = (num2 <= num);
				}
				else
				{
					result = expected.Equals(actual);
				}
				break;
			}
			case ToleranceMode.Percent:
				if (expected == 0U)
				{
					result = expected.Equals(actual);
				}
				else
				{
					uint num3 = Math.Max(expected, actual) - Math.Min(expected, actual);
					double num4 = Math.Abs(num3 / expected);
					result = (num4 <= Convert.ToDouble(tolerance.Value) / 100.0);
				}
				break;
			default:
				throw new ArgumentException("Unknown tolerance mode specified", "mode");
			}
			return result;
		}

		// Token: 0x06000295 RID: 661 RVA: 0x00009C74 File Offset: 0x00007E74
		private static bool AreEqual(int expected, int actual, Tolerance tolerance)
		{
			bool result;
			switch (tolerance.Mode)
			{
			case ToleranceMode.Unset:
				result = expected.Equals(actual);
				break;
			case ToleranceMode.Linear:
			{
				int num = Convert.ToInt32(tolerance.Value);
				if (num > 0)
				{
					result = (Math.Abs(expected - actual) <= num);
				}
				else
				{
					result = expected.Equals(actual);
				}
				break;
			}
			case ToleranceMode.Percent:
				if (expected == 0)
				{
					result = expected.Equals(actual);
				}
				else
				{
					double num2 = Math.Abs((double)(expected - actual) / (double)expected);
					result = (num2 <= Convert.ToDouble(tolerance.Value) / 100.0);
				}
				break;
			default:
				throw new ArgumentException("Unknown tolerance mode specified", "mode");
			}
			return result;
		}

		// Token: 0x06000296 RID: 662 RVA: 0x00009D38 File Offset: 0x00007F38
		public static int Compare(object expected, object actual)
		{
			if (!Numerics.IsNumericType(expected) || !Numerics.IsNumericType(actual))
			{
				throw new ArgumentException("Both arguments must be numeric");
			}
			int result;
			if (Numerics.IsFloatingPointNumeric(expected) || Numerics.IsFloatingPointNumeric(actual))
			{
				result = Convert.ToDouble(expected).CompareTo(Convert.ToDouble(actual));
			}
			else if (expected is decimal || actual is decimal)
			{
				result = Convert.ToDecimal(expected).CompareTo(Convert.ToDecimal(actual));
			}
			else if (expected is ulong || actual is ulong)
			{
				result = Convert.ToUInt64(expected).CompareTo(Convert.ToUInt64(actual));
			}
			else if (expected is long || actual is long)
			{
				result = Convert.ToInt64(expected).CompareTo(Convert.ToInt64(actual));
			}
			else if (expected is uint || actual is uint)
			{
				result = Convert.ToUInt32(expected).CompareTo(Convert.ToUInt32(actual));
			}
			else
			{
				result = Convert.ToInt32(expected).CompareTo(Convert.ToInt32(actual));
			}
			return result;
		}

		// Token: 0x06000297 RID: 663 RVA: 0x00009E7F File Offset: 0x0000807F
		private Numerics()
		{
		}
	}
}
