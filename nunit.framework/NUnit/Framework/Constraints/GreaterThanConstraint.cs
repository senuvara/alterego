﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000166 RID: 358
	public class GreaterThanConstraint : ComparisonConstraint
	{
		// Token: 0x06000967 RID: 2407 RVA: 0x0001FAE8 File Offset: 0x0001DCE8
		public GreaterThanConstraint(object expected) : base(expected, false, false, true, "greater than")
		{
		}
	}
}
