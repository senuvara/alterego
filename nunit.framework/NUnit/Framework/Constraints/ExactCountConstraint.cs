﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000129 RID: 297
	public class ExactCountConstraint : PrefixConstraint
	{
		// Token: 0x060007EE RID: 2030 RVA: 0x0001C13C File Offset: 0x0001A33C
		public ExactCountConstraint(int expectedCount, IConstraint itemConstraint) : base(itemConstraint)
		{
			this.expectedCount = expectedCount;
			base.DescriptionPrefix = ((expectedCount == 0) ? "no item" : ((expectedCount == 1) ? "exactly one item" : string.Format("exactly {0} items", expectedCount)));
		}

		// Token: 0x060007EF RID: 2031 RVA: 0x0001C17C File Offset: 0x0001A37C
		public override ConstraintResult ApplyTo(object actual)
		{
			if (!(actual is IEnumerable))
			{
				throw new ArgumentException("The actual value must be an IEnumerable", "actual");
			}
			int num = 0;
			foreach (object actual2 in ((IEnumerable)actual))
			{
				if (base.BaseConstraint.ApplyTo(actual2).IsSuccess)
				{
					num++;
				}
			}
			return new ConstraintResult(this, actual, num == this.expectedCount);
		}

		// Token: 0x040001BD RID: 445
		private int expectedCount;
	}
}
