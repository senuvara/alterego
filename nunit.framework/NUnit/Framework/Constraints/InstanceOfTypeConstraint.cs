﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000165 RID: 357
	public class InstanceOfTypeConstraint : TypeConstraint
	{
		// Token: 0x06000964 RID: 2404 RVA: 0x0001FA9B File Offset: 0x0001DC9B
		public InstanceOfTypeConstraint(Type type) : base(type, "instance of ")
		{
		}

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x06000965 RID: 2405 RVA: 0x0001FAAC File Offset: 0x0001DCAC
		public override string DisplayName
		{
			get
			{
				return "InstanceOf";
			}
		}

		// Token: 0x06000966 RID: 2406 RVA: 0x0001FAC4 File Offset: 0x0001DCC4
		protected override bool Matches(object actual)
		{
			return actual != null && this.expectedType.IsInstanceOfType(actual);
		}
	}
}
