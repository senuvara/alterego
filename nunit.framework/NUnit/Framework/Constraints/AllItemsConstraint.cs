﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200016A RID: 362
	public class AllItemsConstraint : PrefixConstraint
	{
		// Token: 0x06000975 RID: 2421 RVA: 0x0001FDF4 File Offset: 0x0001DFF4
		public AllItemsConstraint(IConstraint itemConstraint) : base(itemConstraint)
		{
			base.DescriptionPrefix = "all items";
		}

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06000976 RID: 2422 RVA: 0x0001FE0C File Offset: 0x0001E00C
		public override string DisplayName
		{
			get
			{
				return "All";
			}
		}

		// Token: 0x06000977 RID: 2423 RVA: 0x0001FE24 File Offset: 0x0001E024
		public override ConstraintResult ApplyTo(object actual)
		{
			if (!(actual is IEnumerable))
			{
				throw new ArgumentException("The actual value must be an IEnumerable", "actual");
			}
			foreach (object actual2 in ((IEnumerable)actual))
			{
				if (!base.BaseConstraint.ApplyTo(actual2).IsSuccess)
				{
					return new ConstraintResult(this, actual, ConstraintStatus.Failure);
				}
			}
			return new ConstraintResult(this, actual, ConstraintStatus.Success);
		}
	}
}
