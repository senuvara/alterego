﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000120 RID: 288
	public class AndOperator : BinaryOperator
	{
		// Token: 0x060007B9 RID: 1977 RVA: 0x0001B78C File Offset: 0x0001998C
		public AndOperator()
		{
			this.left_precedence = (this.right_precedence = 2);
		}

		// Token: 0x060007BA RID: 1978 RVA: 0x0001B7B4 File Offset: 0x000199B4
		public override IConstraint ApplyOperator(IConstraint left, IConstraint right)
		{
			return new AndConstraint(left, right);
		}
	}
}
