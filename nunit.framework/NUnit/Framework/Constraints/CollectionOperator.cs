﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000068 RID: 104
	public abstract class CollectionOperator : PrefixOperator
	{
		// Token: 0x060003FF RID: 1023 RVA: 0x0000CCA5 File Offset: 0x0000AEA5
		protected CollectionOperator()
		{
			this.left_precedence = 1;
			this.right_precedence = 10;
		}
	}
}
