﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000067 RID: 103
	public abstract class PrefixOperator : ConstraintOperator
	{
		// Token: 0x060003FC RID: 1020 RVA: 0x0000CC87 File Offset: 0x0000AE87
		public override void Reduce(ConstraintBuilder.ConstraintStack stack)
		{
			stack.Push(this.ApplyPrefix(stack.Pop()));
		}

		// Token: 0x060003FD RID: 1021
		public abstract IConstraint ApplyPrefix(IConstraint constraint);

		// Token: 0x060003FE RID: 1022 RVA: 0x0000CC9D File Offset: 0x0000AE9D
		protected PrefixOperator()
		{
		}
	}
}
