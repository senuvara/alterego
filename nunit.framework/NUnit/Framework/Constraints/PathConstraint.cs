﻿using System;
using System.IO;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200002E RID: 46
	public abstract class PathConstraint : StringConstraint
	{
		// Token: 0x06000190 RID: 400 RVA: 0x00005EBC File Offset: 0x000040BC
		protected PathConstraint(string expected) : base(expected)
		{
			this.expected = expected;
			this.caseInsensitive = (Path.DirectorySeparatorChar == '\\');
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000191 RID: 401 RVA: 0x00005EE0 File Offset: 0x000040E0
		public PathConstraint RespectCase
		{
			get
			{
				this.caseInsensitive = false;
				return this;
			}
		}

		// Token: 0x06000192 RID: 402 RVA: 0x00005EFC File Offset: 0x000040FC
		protected override string GetStringRepresentation()
		{
			return string.Format("<{0} \"{1}\" {2}>", this.DisplayName.ToLower(), this.expected, this.caseInsensitive ? "ignorecase" : "respectcase");
		}

		// Token: 0x06000193 RID: 403 RVA: 0x00005F40 File Offset: 0x00004140
		protected string Canonicalize(string path)
		{
			if (Path.DirectorySeparatorChar != Path.AltDirectorySeparatorChar)
			{
				path = path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
			}
			string text = "";
			foreach (char c in path)
			{
				if (c != '\\' && c != '/')
				{
					break;
				}
				text += Path.DirectorySeparatorChar;
			}
			string[] array = path.Split(PathConstraint.DirectorySeparatorChars, StringSplitOptions.RemoveEmptyEntries);
			int num = 0;
			bool flag = false;
			string[] array2 = array;
			int i = 0;
			while (i < array2.Length)
			{
				string text3 = array2[i];
				string text4 = text3;
				if (text4 == null)
				{
					goto IL_F6;
				}
				if (!(text4 == ".") && !(text4 == ""))
				{
					if (!(text4 == ".."))
					{
						goto IL_F6;
					}
					flag = true;
					if (num > 0)
					{
						num--;
					}
				}
				else
				{
					flag = true;
				}
				IL_10C:
				i++;
				continue;
				IL_F6:
				if (flag)
				{
					array[num] = text3;
				}
				num++;
				goto IL_10C;
			}
			return text + string.Join(Path.DirectorySeparatorChar.ToString(), array, 0, num);
		}

		// Token: 0x06000194 RID: 404 RVA: 0x00006094 File Offset: 0x00004294
		protected bool IsSubPath(string path1, string path2)
		{
			int length = path1.Length;
			int length2 = path2.Length;
			return length < length2 && StringUtil.StringsEqual(path1, path2.Substring(0, length), this.caseInsensitive) && (path2[length - 1] == Path.DirectorySeparatorChar || path2[length] == Path.DirectorySeparatorChar);
		}

		// Token: 0x06000195 RID: 405 RVA: 0x00006100 File Offset: 0x00004300
		// Note: this type is marked as 'beforefieldinit'.
		static PathConstraint()
		{
		}

		// Token: 0x04000046 RID: 70
		private const char WindowsDirectorySeparatorChar = '\\';

		// Token: 0x04000047 RID: 71
		private const char NonWindowsDirectorySeparatorChar = '/';

		// Token: 0x04000048 RID: 72
		private static readonly char[] DirectorySeparatorChars = new char[]
		{
			'\\',
			'/'
		};
	}
}
