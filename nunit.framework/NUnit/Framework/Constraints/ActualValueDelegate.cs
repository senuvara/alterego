﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000186 RID: 390
	// (Invoke) Token: 0x06000A3C RID: 2620
	public delegate TActual ActualValueDelegate<TActual>();
}
