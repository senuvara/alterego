﻿using System;
using NUnit.Compatibility;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000187 RID: 391
	public class AttributeConstraint : PrefixConstraint
	{
		// Token: 0x06000A3F RID: 2623 RVA: 0x00022488 File Offset: 0x00020688
		public AttributeConstraint(Type type, IConstraint baseConstraint) : base(baseConstraint)
		{
			this.expectedType = type;
			base.DescriptionPrefix = "attribute " + this.expectedType.FullName;
			if (!typeof(Attribute).GetTypeInfo().IsAssignableFrom(this.expectedType.GetTypeInfo()))
			{
				throw new ArgumentException(string.Format("Type {0} is not an attribute", this.expectedType), "type");
			}
		}

		// Token: 0x06000A40 RID: 2624 RVA: 0x00022500 File Offset: 0x00020700
		public override ConstraintResult ApplyTo(object actual)
		{
			Guard.ArgumentNotNull(actual, "actual");
			Attribute[] customAttributes = AttributeHelper.GetCustomAttributes(actual, this.expectedType, true);
			if (customAttributes.Length == 0)
			{
				throw new ArgumentException(string.Format("Attribute {0} was not found", this.expectedType), "actual");
			}
			this.attrFound = customAttributes[0];
			return base.BaseConstraint.ApplyTo(this.attrFound);
		}

		// Token: 0x06000A41 RID: 2625 RVA: 0x00022570 File Offset: 0x00020770
		protected override string GetStringRepresentation()
		{
			return string.Format("<attribute {0} {1}>", this.expectedType, base.BaseConstraint);
		}

		// Token: 0x04000268 RID: 616
		private readonly Type expectedType;

		// Token: 0x04000269 RID: 617
		private Attribute attrFound;
	}
}
