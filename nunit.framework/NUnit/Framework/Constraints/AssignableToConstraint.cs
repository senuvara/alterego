﻿using System;
using NUnit.Compatibility;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000010 RID: 16
	public class AssignableToConstraint : TypeConstraint
	{
		// Token: 0x06000072 RID: 114 RVA: 0x000030FB File Offset: 0x000012FB
		public AssignableToConstraint(Type type) : base(type, "assignable to ")
		{
		}

		// Token: 0x06000073 RID: 115 RVA: 0x0000310C File Offset: 0x0000130C
		protected override bool Matches(object actual)
		{
			return this.expectedType != null && actual != null && this.expectedType.GetTypeInfo().IsAssignableFrom(actual.GetType().GetTypeInfo());
		}
	}
}
