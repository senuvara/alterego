﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000184 RID: 388
	public class RangeConstraint : Constraint
	{
		// Token: 0x06000A34 RID: 2612 RVA: 0x0002230C File Offset: 0x0002050C
		public RangeConstraint(IComparable from, IComparable to) : base(new object[]
		{
			from,
			to
		})
		{
			if (this.comparer.Compare(from, to) > 0)
			{
				throw new ArgumentException("from must be less than to");
			}
			this.from = from;
			this.to = to;
		}

		// Token: 0x1700025E RID: 606
		// (get) Token: 0x06000A35 RID: 2613 RVA: 0x00022370 File Offset: 0x00020570
		public override string Description
		{
			get
			{
				return string.Format("in range ({0},{1})", this.from, this.to);
			}
		}

		// Token: 0x06000A36 RID: 2614 RVA: 0x00022398 File Offset: 0x00020598
		public override ConstraintResult ApplyTo(object actual)
		{
			if (this.from == null || this.to == null || actual == null)
			{
				throw new ArgumentException("Cannot compare using a null reference", "actual");
			}
			bool isSuccess = this.comparer.Compare(this.from, actual) <= 0 && this.comparer.Compare(this.to, actual) >= 0;
			return new ConstraintResult(this, actual, isSuccess);
		}

		// Token: 0x06000A37 RID: 2615 RVA: 0x00022414 File Offset: 0x00020614
		public RangeConstraint Using(IComparer comparer)
		{
			this.comparer = ComparisonAdapter.For(comparer);
			return this;
		}

		// Token: 0x06000A38 RID: 2616 RVA: 0x00022434 File Offset: 0x00020634
		public RangeConstraint Using<T>(IComparer<T> comparer)
		{
			this.comparer = ComparisonAdapter.For<T>(comparer);
			return this;
		}

		// Token: 0x06000A39 RID: 2617 RVA: 0x00022454 File Offset: 0x00020654
		public RangeConstraint Using<T>(Comparison<T> comparer)
		{
			this.comparer = ComparisonAdapter.For<T>(comparer);
			return this;
		}

		// Token: 0x04000265 RID: 613
		private readonly IComparable from;

		// Token: 0x04000266 RID: 614
		private readonly IComparable to;

		// Token: 0x04000267 RID: 615
		private ComparisonAdapter comparer = ComparisonAdapter.Default;
	}
}
