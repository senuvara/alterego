﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200002C RID: 44
	public abstract class CollectionItemsEqualConstraint : CollectionConstraint
	{
		// Token: 0x06000182 RID: 386 RVA: 0x00005C3A File Offset: 0x00003E3A
		protected CollectionItemsEqualConstraint()
		{
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00005C50 File Offset: 0x00003E50
		protected CollectionItemsEqualConstraint(object arg) : base(arg)
		{
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000184 RID: 388 RVA: 0x00005C68 File Offset: 0x00003E68
		public CollectionItemsEqualConstraint IgnoreCase
		{
			get
			{
				this.comparer.IgnoreCase = true;
				return this;
			}
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00005C88 File Offset: 0x00003E88
		public CollectionItemsEqualConstraint Using(IComparer comparer)
		{
			this.comparer.ExternalComparers.Add(EqualityAdapter.For(comparer));
			return this;
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00005CB4 File Offset: 0x00003EB4
		public CollectionItemsEqualConstraint Using<T>(IComparer<T> comparer)
		{
			this.comparer.ExternalComparers.Add(EqualityAdapter.For<T>(comparer));
			return this;
		}

		// Token: 0x06000187 RID: 391 RVA: 0x00005CE0 File Offset: 0x00003EE0
		public CollectionItemsEqualConstraint Using<T>(Comparison<T> comparer)
		{
			this.comparer.ExternalComparers.Add(EqualityAdapter.For<T>(comparer));
			return this;
		}

		// Token: 0x06000188 RID: 392 RVA: 0x00005D0C File Offset: 0x00003F0C
		public CollectionItemsEqualConstraint Using(IEqualityComparer comparer)
		{
			this.comparer.ExternalComparers.Add(EqualityAdapter.For(comparer));
			return this;
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00005D38 File Offset: 0x00003F38
		public CollectionItemsEqualConstraint Using<T>(IEqualityComparer<T> comparer)
		{
			this.comparer.ExternalComparers.Add(EqualityAdapter.For<T>(comparer));
			return this;
		}

		// Token: 0x0600018A RID: 394 RVA: 0x00005D64 File Offset: 0x00003F64
		internal CollectionItemsEqualConstraint Using(EqualityAdapter adapter)
		{
			this.comparer.ExternalComparers.Add(adapter);
			return this;
		}

		// Token: 0x0600018B RID: 395 RVA: 0x00005D8C File Offset: 0x00003F8C
		protected bool ItemsEqual(object x, object y)
		{
			Tolerance @default = Tolerance.Default;
			return this.comparer.AreEqual(x, y, ref @default);
		}

		// Token: 0x0600018C RID: 396 RVA: 0x00005DB4 File Offset: 0x00003FB4
		protected CollectionTally Tally(IEnumerable c)
		{
			return new CollectionTally(this.comparer, c);
		}

		// Token: 0x04000045 RID: 69
		private readonly NUnitEqualityComparer comparer = NUnitEqualityComparer.Default;
	}
}
