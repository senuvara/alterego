﻿using System;
using System.IO;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000105 RID: 261
	public class EmptyDirectoryConstraint : Constraint
	{
		// Token: 0x1700019A RID: 410
		// (get) Token: 0x0600073F RID: 1855 RVA: 0x0001A14C File Offset: 0x0001834C
		public override string Description
		{
			get
			{
				return "an empty directory";
			}
		}

		// Token: 0x06000740 RID: 1856 RVA: 0x0001A164 File Offset: 0x00018364
		public override ConstraintResult ApplyTo(object actual)
		{
			DirectoryInfo directoryInfo = actual as DirectoryInfo;
			if (directoryInfo == null)
			{
				throw new ArgumentException("The actual value must be a DirectoryInfo", "actual");
			}
			this.files = directoryInfo.GetFiles().Length;
			this.subdirs = directoryInfo.GetDirectories().Length;
			bool isSuccess = this.files == 0 && this.subdirs == 0;
			return new ConstraintResult(this, actual, isSuccess);
		}

		// Token: 0x06000741 RID: 1857 RVA: 0x0001A1D1 File Offset: 0x000183D1
		public EmptyDirectoryConstraint() : base(new object[0])
		{
		}

		// Token: 0x0400019B RID: 411
		private int files = 0;

		// Token: 0x0400019C RID: 412
		private int subdirs = 0;
	}
}
