﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000092 RID: 146
	public class ExactTypeConstraint : TypeConstraint
	{
		// Token: 0x060004B3 RID: 1203 RVA: 0x0000F8C5 File Offset: 0x0000DAC5
		public ExactTypeConstraint(Type type) : base(type, string.Empty)
		{
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x060004B4 RID: 1204 RVA: 0x0000F8D8 File Offset: 0x0000DAD8
		public override string DisplayName
		{
			get
			{
				return "TypeOf";
			}
		}

		// Token: 0x060004B5 RID: 1205 RVA: 0x0000F8F0 File Offset: 0x0000DAF0
		protected override bool Matches(object actual)
		{
			return actual != null && actual.GetType() == this.expectedType;
		}
	}
}
