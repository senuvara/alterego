﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000078 RID: 120
	public class CollectionSubsetConstraint : CollectionItemsEqualConstraint
	{
		// Token: 0x0600044E RID: 1102 RVA: 0x0000E725 File Offset: 0x0000C925
		public CollectionSubsetConstraint(IEnumerable expected) : base(expected)
		{
			this._expected = expected;
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x0600044F RID: 1103 RVA: 0x0000E738 File Offset: 0x0000C938
		public override string DisplayName
		{
			get
			{
				return "SubsetOf";
			}
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000450 RID: 1104 RVA: 0x0000E750 File Offset: 0x0000C950
		public override string Description
		{
			get
			{
				return "subset of " + MsgUtils.FormatValue(this._expected);
			}
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x0000E778 File Offset: 0x0000C978
		protected override bool Matches(IEnumerable actual)
		{
			return base.Tally(this._expected).TryRemove(actual);
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x0000E79C File Offset: 0x0000C99C
		public CollectionSubsetConstraint Using<TSubsetType, TSupersetType>(Func<TSubsetType, TSupersetType, bool> comparison)
		{
			base.Using(EqualityAdapter.For<TSubsetType, TSupersetType>(comparison));
			return this;
		}

		// Token: 0x040000C1 RID: 193
		private IEnumerable _expected;
	}
}
