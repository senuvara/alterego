﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200007A RID: 122
	public class AndConstraint : BinaryConstraint
	{
		// Token: 0x06000454 RID: 1108 RVA: 0x0000E808 File Offset: 0x0000CA08
		public AndConstraint(IConstraint left, IConstraint right) : base(left, right)
		{
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000455 RID: 1109 RVA: 0x0000E818 File Offset: 0x0000CA18
		public override string Description
		{
			get
			{
				return this.Left.Description + " and " + this.Right.Description;
			}
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x0000E84C File Offset: 0x0000CA4C
		public override ConstraintResult ApplyTo(object actual)
		{
			ConstraintResult constraintResult = this.Left.ApplyTo(actual);
			ConstraintResult rightResult = constraintResult.IsSuccess ? this.Right.ApplyTo(actual) : new ConstraintResult(this.Right, actual);
			return new AndConstraint.AndConstraintResult(this, actual, constraintResult, rightResult);
		}

		// Token: 0x0200007C RID: 124
		private class AndConstraintResult : ConstraintResult
		{
			// Token: 0x06000463 RID: 1123 RVA: 0x0000E998 File Offset: 0x0000CB98
			public AndConstraintResult(AndConstraint constraint, object actual, ConstraintResult leftResult, ConstraintResult rightResult) : base(constraint, actual, leftResult.IsSuccess && rightResult.IsSuccess)
			{
				this.leftResult = leftResult;
				this.rightResult = rightResult;
			}

			// Token: 0x06000464 RID: 1124 RVA: 0x0000E9C8 File Offset: 0x0000CBC8
			public override void WriteActualValueTo(MessageWriter writer)
			{
				if (this.IsSuccess)
				{
					base.WriteActualValueTo(writer);
				}
				else if (!this.leftResult.IsSuccess)
				{
					this.leftResult.WriteActualValueTo(writer);
				}
				else
				{
					this.rightResult.WriteActualValueTo(writer);
				}
			}

			// Token: 0x040000C7 RID: 199
			private ConstraintResult leftResult;

			// Token: 0x040000C8 RID: 200
			private ConstraintResult rightResult;
		}
	}
}
