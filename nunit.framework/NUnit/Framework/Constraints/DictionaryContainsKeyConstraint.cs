﻿using System;
using System.Collections;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000167 RID: 359
	public class DictionaryContainsKeyConstraint : CollectionContainsConstraint
	{
		// Token: 0x06000968 RID: 2408 RVA: 0x0001FAFC File Offset: 0x0001DCFC
		public DictionaryContainsKeyConstraint(object expected) : base(expected)
		{
		}

		// Token: 0x17000231 RID: 561
		// (get) Token: 0x06000969 RID: 2409 RVA: 0x0001FB08 File Offset: 0x0001DD08
		public override string DisplayName
		{
			get
			{
				return "ContainsKey";
			}
		}

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x0600096A RID: 2410 RVA: 0x0001FB20 File Offset: 0x0001DD20
		public override string Description
		{
			get
			{
				return "dictionary containing key " + MsgUtils.FormatValue(base.Expected);
			}
		}

		// Token: 0x0600096B RID: 2411 RVA: 0x0001FB48 File Offset: 0x0001DD48
		protected override bool Matches(IEnumerable actual)
		{
			IDictionary dictionary = actual as IDictionary;
			if (dictionary == null)
			{
				throw new ArgumentException("The actual value must be an IDictionary", "actual");
			}
			return base.Matches(dictionary.Keys);
		}
	}
}
