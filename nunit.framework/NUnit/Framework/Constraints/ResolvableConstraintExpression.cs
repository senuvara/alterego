﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020001A1 RID: 417
	public class ResolvableConstraintExpression : ConstraintExpression, IResolveConstraint
	{
		// Token: 0x06000AC7 RID: 2759 RVA: 0x0002419F File Offset: 0x0002239F
		public ResolvableConstraintExpression()
		{
		}

		// Token: 0x06000AC8 RID: 2760 RVA: 0x000241AA File Offset: 0x000223AA
		public ResolvableConstraintExpression(ConstraintBuilder builder) : base(builder)
		{
		}

		// Token: 0x1700026F RID: 623
		// (get) Token: 0x06000AC9 RID: 2761 RVA: 0x000241B8 File Offset: 0x000223B8
		public ConstraintExpression And
		{
			get
			{
				return base.Append(new AndOperator());
			}
		}

		// Token: 0x17000270 RID: 624
		// (get) Token: 0x06000ACA RID: 2762 RVA: 0x000241D8 File Offset: 0x000223D8
		public ConstraintExpression Or
		{
			get
			{
				return base.Append(new OrOperator());
			}
		}

		// Token: 0x06000ACB RID: 2763 RVA: 0x000241F8 File Offset: 0x000223F8
		IConstraint IResolveConstraint.Resolve()
		{
			return this.builder.Resolve();
		}
	}
}
