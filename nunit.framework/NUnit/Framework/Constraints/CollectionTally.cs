﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000169 RID: 361
	public class CollectionTally
	{
		// Token: 0x06000970 RID: 2416 RVA: 0x0001FC68 File Offset: 0x0001DE68
		public CollectionTally(NUnitEqualityComparer comparer, IEnumerable c)
		{
			this.comparer = comparer;
			foreach (object item in c)
			{
				this.list.Add(item);
			}
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06000971 RID: 2417 RVA: 0x0001FCE4 File Offset: 0x0001DEE4
		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x06000972 RID: 2418 RVA: 0x0001FD04 File Offset: 0x0001DF04
		private bool ItemsEqual(object expected, object actual)
		{
			Tolerance @default = Tolerance.Default;
			return this.comparer.AreEqual(expected, actual, ref @default);
		}

		// Token: 0x06000973 RID: 2419 RVA: 0x0001FD2C File Offset: 0x0001DF2C
		public bool TryRemove(object o)
		{
			for (int i = 0; i < this.list.Count; i++)
			{
				if (this.ItemsEqual(this.list[i], o))
				{
					this.list.RemoveAt(i);
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000974 RID: 2420 RVA: 0x0001FD88 File Offset: 0x0001DF88
		public bool TryRemove(IEnumerable c)
		{
			foreach (object o in c)
			{
				if (!this.TryRemove(o))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x04000226 RID: 550
		private readonly List<object> list = new List<object>();

		// Token: 0x04000227 RID: 551
		private readonly NUnitEqualityComparer comparer;
	}
}
