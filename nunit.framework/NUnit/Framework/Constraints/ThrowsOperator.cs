﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000149 RID: 329
	public class ThrowsOperator : SelfResolvingOperator
	{
		// Token: 0x06000896 RID: 2198 RVA: 0x0001D8BC File Offset: 0x0001BABC
		public ThrowsOperator()
		{
			this.left_precedence = 1;
			this.right_precedence = 100;
		}

		// Token: 0x06000897 RID: 2199 RVA: 0x0001D8D8 File Offset: 0x0001BAD8
		public override void Reduce(ConstraintBuilder.ConstraintStack stack)
		{
			if (base.RightContext == null || base.RightContext is BinaryOperator)
			{
				stack.Push(new ThrowsExceptionConstraint());
			}
			else
			{
				stack.Push(new ThrowsConstraint(stack.Pop()));
			}
		}
	}
}
