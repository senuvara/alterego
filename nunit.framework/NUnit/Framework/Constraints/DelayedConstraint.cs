﻿using System;
using System.Diagnostics;
using System.Threading;

namespace NUnit.Framework.Constraints
{
	// Token: 0x020000BD RID: 189
	public class DelayedConstraint : PrefixConstraint
	{
		// Token: 0x060005C6 RID: 1478 RVA: 0x00013767 File Offset: 0x00011967
		public DelayedConstraint(IConstraint baseConstraint, int delayInMilliseconds) : this(baseConstraint, delayInMilliseconds, 0)
		{
		}

		// Token: 0x060005C7 RID: 1479 RVA: 0x00013778 File Offset: 0x00011978
		public DelayedConstraint(IConstraint baseConstraint, int delayInMilliseconds, int pollingInterval) : base(baseConstraint)
		{
			if (delayInMilliseconds < 0)
			{
				throw new ArgumentException("Cannot check a condition in the past", "delayInMilliseconds");
			}
			this.delayInMilliseconds = delayInMilliseconds;
			this.pollingInterval = pollingInterval;
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x060005C8 RID: 1480 RVA: 0x000137B8 File Offset: 0x000119B8
		public override string Description
		{
			get
			{
				return string.Format("{0} after {1} millisecond delay", base.BaseConstraint.Description, this.delayInMilliseconds);
			}
		}

		// Token: 0x060005C9 RID: 1481 RVA: 0x000137EC File Offset: 0x000119EC
		public override ConstraintResult ApplyTo(object actual)
		{
			long timestamp = Stopwatch.GetTimestamp();
			long num = DelayedConstraint.TimestampOffset(timestamp, TimeSpan.FromMilliseconds((double)this.delayInMilliseconds));
			if (this.pollingInterval > 0)
			{
				long num2 = DelayedConstraint.TimestampOffset(timestamp, TimeSpan.FromMilliseconds((double)this.pollingInterval));
				while ((timestamp = Stopwatch.GetTimestamp()) < num)
				{
					if (num2 > timestamp)
					{
						Thread.Sleep((int)DelayedConstraint.TimestampDiff((num < num2) ? num : num2, timestamp).TotalMilliseconds);
					}
					num2 = DelayedConstraint.TimestampOffset(timestamp, TimeSpan.FromMilliseconds((double)this.pollingInterval));
					ConstraintResult constraintResult = base.BaseConstraint.ApplyTo(actual);
					if (constraintResult.IsSuccess)
					{
						return new ConstraintResult(this, actual, true);
					}
				}
			}
			if ((timestamp = Stopwatch.GetTimestamp()) < num)
			{
				Thread.Sleep((int)DelayedConstraint.TimestampDiff(num, timestamp).TotalMilliseconds);
			}
			return new ConstraintResult(this, actual, base.BaseConstraint.ApplyTo(actual).IsSuccess);
		}

		// Token: 0x060005CA RID: 1482 RVA: 0x00013900 File Offset: 0x00011B00
		public override ConstraintResult ApplyTo<TActual>(ActualValueDelegate<TActual> del)
		{
			long timestamp = Stopwatch.GetTimestamp();
			long num = DelayedConstraint.TimestampOffset(timestamp, TimeSpan.FromMilliseconds((double)this.delayInMilliseconds));
			object obj;
			if (this.pollingInterval > 0)
			{
				long num2 = DelayedConstraint.TimestampOffset(timestamp, TimeSpan.FromMilliseconds((double)this.pollingInterval));
				while ((timestamp = Stopwatch.GetTimestamp()) < num)
				{
					if (num2 > timestamp)
					{
						Thread.Sleep((int)DelayedConstraint.TimestampDiff((num < num2) ? num : num2, timestamp).TotalMilliseconds);
					}
					num2 = DelayedConstraint.TimestampOffset(timestamp, TimeSpan.FromMilliseconds((double)this.pollingInterval));
					obj = DelayedConstraint.InvokeDelegate<TActual>(del);
					try
					{
						ConstraintResult constraintResult = base.BaseConstraint.ApplyTo(obj);
						if (constraintResult.IsSuccess)
						{
							return new ConstraintResult(this, obj, true);
						}
					}
					catch (Exception)
					{
					}
				}
			}
			if ((timestamp = Stopwatch.GetTimestamp()) < num)
			{
				Thread.Sleep((int)DelayedConstraint.TimestampDiff(num, timestamp).TotalMilliseconds);
			}
			obj = DelayedConstraint.InvokeDelegate<TActual>(del);
			return new ConstraintResult(this, obj, base.BaseConstraint.ApplyTo(obj).IsSuccess);
		}

		// Token: 0x060005CB RID: 1483 RVA: 0x00013A44 File Offset: 0x00011C44
		private static object InvokeDelegate<T>(ActualValueDelegate<T> del)
		{
			return del();
		}

		// Token: 0x060005CC RID: 1484 RVA: 0x00013A64 File Offset: 0x00011C64
		public override ConstraintResult ApplyTo<TActual>(ref TActual actual)
		{
			long timestamp = Stopwatch.GetTimestamp();
			long num = DelayedConstraint.TimestampOffset(timestamp, TimeSpan.FromMilliseconds((double)this.delayInMilliseconds));
			if (this.pollingInterval > 0)
			{
				long num2 = DelayedConstraint.TimestampOffset(timestamp, TimeSpan.FromMilliseconds((double)this.pollingInterval));
				while ((timestamp = Stopwatch.GetTimestamp()) < num)
				{
					if (num2 > timestamp)
					{
						Thread.Sleep((int)DelayedConstraint.TimestampDiff((num < num2) ? num : num2, timestamp).TotalMilliseconds);
					}
					num2 = DelayedConstraint.TimestampOffset(timestamp, TimeSpan.FromMilliseconds((double)this.pollingInterval));
					try
					{
						ConstraintResult constraintResult = base.BaseConstraint.ApplyTo(actual);
						if (constraintResult.IsSuccess)
						{
							return new ConstraintResult(this, actual, true);
						}
					}
					catch (Exception)
					{
					}
				}
			}
			if ((timestamp = Stopwatch.GetTimestamp()) < num)
			{
				Thread.Sleep((int)DelayedConstraint.TimestampDiff(num, timestamp).TotalMilliseconds);
			}
			return new ConstraintResult(this, actual, base.BaseConstraint.ApplyTo(actual).IsSuccess);
		}

		// Token: 0x060005CD RID: 1485 RVA: 0x00013BC0 File Offset: 0x00011DC0
		protected override string GetStringRepresentation()
		{
			return string.Format("<after {0} {1}>", this.delayInMilliseconds, base.BaseConstraint);
		}

		// Token: 0x060005CE RID: 1486 RVA: 0x00013BF0 File Offset: 0x00011DF0
		private static long TimestampOffset(long timestamp, TimeSpan offset)
		{
			return timestamp + (long)(offset.TotalSeconds * (double)Stopwatch.Frequency);
		}

		// Token: 0x060005CF RID: 1487 RVA: 0x00013C14 File Offset: 0x00011E14
		private static TimeSpan TimestampDiff(long timestamp1, long timestamp2)
		{
			return TimeSpan.FromSeconds((double)(timestamp1 - timestamp2) / (double)Stopwatch.Frequency);
		}

		// Token: 0x0400012E RID: 302
		private readonly int delayInMilliseconds;

		// Token: 0x0400012F RID: 303
		private readonly int pollingInterval;
	}
}
