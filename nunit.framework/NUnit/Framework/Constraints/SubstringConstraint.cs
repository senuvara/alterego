﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200000D RID: 13
	public class SubstringConstraint : StringConstraint
	{
		// Token: 0x06000067 RID: 103 RVA: 0x00002F4C File Offset: 0x0000114C
		public SubstringConstraint(string expected) : base(expected)
		{
			this.descriptionText = "String containing";
		}

		// Token: 0x06000068 RID: 104 RVA: 0x00002F64 File Offset: 0x00001164
		protected override bool Matches(string actual)
		{
			bool result;
			if (this.caseInsensitive)
			{
				result = (actual != null && actual.ToLower().IndexOf(this.expected.ToLower()) >= 0);
			}
			else
			{
				result = (actual != null && actual.IndexOf(this.expected) >= 0);
			}
			return result;
		}
	}
}
