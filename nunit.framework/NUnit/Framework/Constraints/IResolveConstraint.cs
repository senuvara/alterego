﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000009 RID: 9
	public interface IResolveConstraint
	{
		// Token: 0x0600003F RID: 63
		IConstraint Resolve();
	}
}
