﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000168 RID: 360
	public class ContainsConstraint : Constraint
	{
		// Token: 0x0600096C RID: 2412 RVA: 0x0001FB88 File Offset: 0x0001DD88
		public ContainsConstraint(object expected) : base(new object[0])
		{
			this._expected = expected;
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x0600096D RID: 2413 RVA: 0x0001FBA0 File Offset: 0x0001DDA0
		public override string Description
		{
			get
			{
				return (this._realConstraint != null) ? this._realConstraint.Description : ("containing " + MsgUtils.FormatValue(this._expected));
			}
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x0600096E RID: 2414 RVA: 0x0001FBDC File Offset: 0x0001DDDC
		public ContainsConstraint IgnoreCase
		{
			get
			{
				this._ignoreCase = true;
				return this;
			}
		}

		// Token: 0x0600096F RID: 2415 RVA: 0x0001FBF8 File Offset: 0x0001DDF8
		public override ConstraintResult ApplyTo(object actual)
		{
			if (actual is string)
			{
				StringConstraint stringConstraint = new SubstringConstraint((string)this._expected);
				if (this._ignoreCase)
				{
					stringConstraint = stringConstraint.IgnoreCase;
				}
				this._realConstraint = stringConstraint;
			}
			else
			{
				this._realConstraint = new CollectionContainsConstraint(this._expected);
			}
			return this._realConstraint.ApplyTo(actual);
		}

		// Token: 0x04000223 RID: 547
		private readonly object _expected;

		// Token: 0x04000224 RID: 548
		private Constraint _realConstraint;

		// Token: 0x04000225 RID: 549
		private bool _ignoreCase;
	}
}
