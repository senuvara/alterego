﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x02000171 RID: 369
	public class SubPathConstraint : PathConstraint
	{
		// Token: 0x060009C2 RID: 2498 RVA: 0x00020E06 File Offset: 0x0001F006
		public SubPathConstraint(string expected) : base(expected)
		{
		}

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x060009C3 RID: 2499 RVA: 0x00020E14 File Offset: 0x0001F014
		public override string Description
		{
			get
			{
				return "Subpath of " + MsgUtils.FormatValue(this.expected);
			}
		}

		// Token: 0x060009C4 RID: 2500 RVA: 0x00020E3C File Offset: 0x0001F03C
		protected override bool Matches(string actual)
		{
			return actual != null && base.IsSubPath(base.Canonicalize(this.expected), base.Canonicalize(actual));
		}
	}
}
