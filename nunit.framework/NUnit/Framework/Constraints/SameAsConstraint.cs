﻿using System;

namespace NUnit.Framework.Constraints
{
	// Token: 0x0200017B RID: 379
	public class SameAsConstraint : Constraint
	{
		// Token: 0x06000A0B RID: 2571 RVA: 0x00021DE8 File Offset: 0x0001FFE8
		public SameAsConstraint(object expected) : base(new object[]
		{
			expected
		})
		{
			this.expected = expected;
		}

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x06000A0C RID: 2572 RVA: 0x00021E14 File Offset: 0x00020014
		public override string Description
		{
			get
			{
				return "same as " + MsgUtils.FormatValue(this.expected);
			}
		}

		// Token: 0x06000A0D RID: 2573 RVA: 0x00021E3C File Offset: 0x0002003C
		public override ConstraintResult ApplyTo(object actual)
		{
			bool isSuccess = object.ReferenceEquals(this.expected, actual);
			return new ConstraintResult(this, actual, isSuccess);
		}

		// Token: 0x04000248 RID: 584
		private readonly object expected;
	}
}
