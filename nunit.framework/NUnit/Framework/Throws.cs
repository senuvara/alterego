﻿using System;
using System.Reflection;
using NUnit.Framework.Constraints;

namespace NUnit.Framework
{
	// Token: 0x02000173 RID: 371
	public class Throws
	{
		// Token: 0x1700023D RID: 573
		// (get) Token: 0x060009C8 RID: 2504 RVA: 0x00020EB8 File Offset: 0x0001F0B8
		public static ResolvableConstraintExpression Exception
		{
			get
			{
				return new ConstraintExpression().Append(new ThrowsOperator());
			}
		}

		// Token: 0x1700023E RID: 574
		// (get) Token: 0x060009C9 RID: 2505 RVA: 0x00020EDC File Offset: 0x0001F0DC
		public static ResolvableConstraintExpression InnerException
		{
			get
			{
				return Throws.Exception.InnerException;
			}
		}

		// Token: 0x1700023F RID: 575
		// (get) Token: 0x060009CA RID: 2506 RVA: 0x00020EF8 File Offset: 0x0001F0F8
		public static ExactTypeConstraint TargetInvocationException
		{
			get
			{
				return Throws.TypeOf(typeof(TargetInvocationException));
			}
		}

		// Token: 0x17000240 RID: 576
		// (get) Token: 0x060009CB RID: 2507 RVA: 0x00020F1C File Offset: 0x0001F11C
		public static ExactTypeConstraint ArgumentException
		{
			get
			{
				return Throws.TypeOf(typeof(ArgumentException));
			}
		}

		// Token: 0x17000241 RID: 577
		// (get) Token: 0x060009CC RID: 2508 RVA: 0x00020F40 File Offset: 0x0001F140
		public static ExactTypeConstraint ArgumentNullException
		{
			get
			{
				return Throws.TypeOf(typeof(ArgumentNullException));
			}
		}

		// Token: 0x17000242 RID: 578
		// (get) Token: 0x060009CD RID: 2509 RVA: 0x00020F64 File Offset: 0x0001F164
		public static ExactTypeConstraint InvalidOperationException
		{
			get
			{
				return Throws.TypeOf(typeof(InvalidOperationException));
			}
		}

		// Token: 0x17000243 RID: 579
		// (get) Token: 0x060009CE RID: 2510 RVA: 0x00020F88 File Offset: 0x0001F188
		public static ThrowsNothingConstraint Nothing
		{
			get
			{
				return new ThrowsNothingConstraint();
			}
		}

		// Token: 0x060009CF RID: 2511 RVA: 0x00020FA0 File Offset: 0x0001F1A0
		public static ExactTypeConstraint TypeOf(Type expectedType)
		{
			return Throws.Exception.TypeOf(expectedType);
		}

		// Token: 0x060009D0 RID: 2512 RVA: 0x00020FC0 File Offset: 0x0001F1C0
		public static ExactTypeConstraint TypeOf<TExpected>()
		{
			return Throws.TypeOf(typeof(TExpected));
		}

		// Token: 0x060009D1 RID: 2513 RVA: 0x00020FE4 File Offset: 0x0001F1E4
		public static InstanceOfTypeConstraint InstanceOf(Type expectedType)
		{
			return Throws.Exception.InstanceOf(expectedType);
		}

		// Token: 0x060009D2 RID: 2514 RVA: 0x00021004 File Offset: 0x0001F204
		public static InstanceOfTypeConstraint InstanceOf<TExpected>()
		{
			return Throws.InstanceOf(typeof(TExpected));
		}

		// Token: 0x060009D3 RID: 2515 RVA: 0x00021025 File Offset: 0x0001F225
		public Throws()
		{
		}
	}
}
