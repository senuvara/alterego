﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x02000172 RID: 370
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public sealed class ParallelizableAttribute : PropertyAttribute, IApplyToContext
	{
		// Token: 0x060009C5 RID: 2501 RVA: 0x00020E6D File Offset: 0x0001F06D
		public ParallelizableAttribute() : this(ParallelScope.Self)
		{
		}

		// Token: 0x060009C6 RID: 2502 RVA: 0x00020E79 File Offset: 0x0001F079
		public ParallelizableAttribute(ParallelScope scope)
		{
			this._scope = scope;
			base.Properties.Add("ParallelScope", scope);
		}

		// Token: 0x060009C7 RID: 2503 RVA: 0x00020EA2 File Offset: 0x0001F0A2
		public void ApplyToContext(ITestExecutionContext context)
		{
			context.ParallelScope = (this._scope & ~ParallelScope.Self);
		}

		// Token: 0x04000237 RID: 567
		private ParallelScope _scope;
	}
}
