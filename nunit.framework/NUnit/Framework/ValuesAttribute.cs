﻿using System;
using System.Collections;
using System.Globalization;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x02000034 RID: 52
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
	public class ValuesAttribute : DataAttribute, IParameterDataSource
	{
		// Token: 0x060001A0 RID: 416 RVA: 0x000062F3 File Offset: 0x000044F3
		public ValuesAttribute()
		{
			this.data = new object[0];
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x0000630C File Offset: 0x0000450C
		public ValuesAttribute(object arg1)
		{
			this.data = new object[]
			{
				arg1
			};
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x00006334 File Offset: 0x00004534
		public ValuesAttribute(object arg1, object arg2)
		{
			this.data = new object[]
			{
				arg1,
				arg2
			};
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x00006360 File Offset: 0x00004560
		public ValuesAttribute(object arg1, object arg2, object arg3)
		{
			this.data = new object[]
			{
				arg1,
				arg2,
				arg3
			};
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x00006390 File Offset: 0x00004590
		public ValuesAttribute(params object[] args)
		{
			this.data = args;
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x000063A4 File Offset: 0x000045A4
		public IEnumerable GetData(IParameterInfo parameter)
		{
			Type parameterType = parameter.ParameterType;
			IEnumerable result;
			if (parameterType.GetTypeInfo().IsEnum && this.data.Length == 0)
			{
				result = TypeHelper.GetEnumValues(parameterType);
			}
			else if (parameterType == typeof(bool) && this.data.Length == 0)
			{
				result = new object[]
				{
					true,
					false
				};
			}
			else
			{
				result = this.GetData(parameterType);
			}
			return result;
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x00006430 File Offset: 0x00004630
		private IEnumerable GetData(Type targetType)
		{
			for (int i = 0; i < this.data.Length; i++)
			{
				object obj = this.data[i];
				if (obj != null)
				{
					if (obj.GetType().FullName == "NUnit.Framework.SpecialValue" && obj.ToString() == "Null")
					{
						this.data[i] = null;
					}
					else if (!targetType.GetTypeInfo().IsAssignableFrom(obj.GetType().GetTypeInfo()))
					{
						if (obj is DBNull)
						{
							this.data[i] = null;
						}
						else
						{
							bool flag = false;
							if (targetType == typeof(short) || targetType == typeof(byte) || targetType == typeof(sbyte))
							{
								flag = (obj is int);
							}
							else if (targetType == typeof(decimal))
							{
								flag = (obj is double || obj is string || obj is int);
							}
							else if (targetType == typeof(DateTime) || targetType == typeof(TimeSpan))
							{
								flag = (obj is string);
							}
							if (flag)
							{
								this.data[i] = Convert.ChangeType(obj, targetType, CultureInfo.InvariantCulture);
							}
						}
					}
				}
			}
			return this.data;
		}

		// Token: 0x04000049 RID: 73
		protected object[] data;
	}
}
