﻿using System;

namespace NUnit.Framework
{
	// Token: 0x02000170 RID: 368
	internal static class Guard
	{
		// Token: 0x060009BD RID: 2493 RVA: 0x00020D44 File Offset: 0x0001EF44
		public static void ArgumentNotNull(object value, string name)
		{
			if (value == null)
			{
				throw new ArgumentNullException("Argument " + name + " must not be null", name);
			}
		}

		// Token: 0x060009BE RID: 2494 RVA: 0x00020D74 File Offset: 0x0001EF74
		public static void ArgumentNotNullOrEmpty(string value, string name)
		{
			Guard.ArgumentNotNull(value, name);
			if (value == string.Empty)
			{
				throw new ArgumentException("Argument " + name + " must not be the empty string", name);
			}
		}

		// Token: 0x060009BF RID: 2495 RVA: 0x00020DB4 File Offset: 0x0001EFB4
		public static void ArgumentInRange(bool condition, string message, string paramName)
		{
			if (!condition)
			{
				throw new ArgumentOutOfRangeException(paramName, message);
			}
		}

		// Token: 0x060009C0 RID: 2496 RVA: 0x00020DD0 File Offset: 0x0001EFD0
		public static void ArgumentValid(bool condition, string message, string paramName)
		{
			if (!condition)
			{
				throw new ArgumentException(message, paramName);
			}
		}

		// Token: 0x060009C1 RID: 2497 RVA: 0x00020DEC File Offset: 0x0001EFEC
		public static void OperationValid(bool condition, string message)
		{
			if (!condition)
			{
				throw new InvalidOperationException(message);
			}
		}
	}
}
