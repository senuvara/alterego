﻿using System;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework
{
	// Token: 0x0200013B RID: 315
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class TestAttribute : NUnitAttribute, ISimpleTestBuilder, IApplyToTest, IImplyFixture
	{
		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x06000864 RID: 2148 RVA: 0x0001D134 File Offset: 0x0001B334
		// (set) Token: 0x06000865 RID: 2149 RVA: 0x0001D14B File Offset: 0x0001B34B
		public string Description
		{
			[CompilerGenerated]
			get
			{
				return this.<Description>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Description>k__BackingField = value;
			}
		}

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x06000866 RID: 2150 RVA: 0x0001D154 File Offset: 0x0001B354
		// (set) Token: 0x06000867 RID: 2151 RVA: 0x0001D16B File Offset: 0x0001B36B
		public string Author
		{
			[CompilerGenerated]
			get
			{
				return this.<Author>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Author>k__BackingField = value;
			}
		}

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x06000868 RID: 2152 RVA: 0x0001D174 File Offset: 0x0001B374
		// (set) Token: 0x06000869 RID: 2153 RVA: 0x0001D18B File Offset: 0x0001B38B
		public Type TestOf
		{
			[CompilerGenerated]
			get
			{
				return this.<TestOf>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TestOf>k__BackingField = value;
			}
		}

		// Token: 0x0600086A RID: 2154 RVA: 0x0001D194 File Offset: 0x0001B394
		public void ApplyToTest(Test test)
		{
			if (!test.Properties.ContainsKey("Description") && this.Description != null)
			{
				test.Properties.Set("Description", this.Description);
			}
			if (!test.Properties.ContainsKey("Author") && this.Author != null)
			{
				test.Properties.Set("Author", this.Author);
			}
			if (!test.Properties.ContainsKey("TestOf") && this.TestOf != null)
			{
				test.Properties.Set("TestOf", this.TestOf.FullName);
			}
		}

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x0600086B RID: 2155 RVA: 0x0001D254 File Offset: 0x0001B454
		// (set) Token: 0x0600086C RID: 2156 RVA: 0x0001D26C File Offset: 0x0001B46C
		public object ExpectedResult
		{
			get
			{
				return this._expectedResult;
			}
			set
			{
				this._expectedResult = value;
				this.HasExpectedResult = true;
			}
		}

		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x0600086D RID: 2157 RVA: 0x0001D280 File Offset: 0x0001B480
		// (set) Token: 0x0600086E RID: 2158 RVA: 0x0001D297 File Offset: 0x0001B497
		public bool HasExpectedResult
		{
			[CompilerGenerated]
			get
			{
				return this.<HasExpectedResult>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<HasExpectedResult>k__BackingField = value;
			}
		}

		// Token: 0x0600086F RID: 2159 RVA: 0x0001D2A0 File Offset: 0x0001B4A0
		public TestMethod BuildFrom(IMethodInfo method, Test suite)
		{
			TestCaseParameters testCaseParameters = null;
			if (this.HasExpectedResult)
			{
				testCaseParameters = new TestCaseParameters();
				testCaseParameters.ExpectedResult = this.ExpectedResult;
			}
			return this._builder.BuildTestMethod(method, suite, testCaseParameters);
		}

		// Token: 0x06000870 RID: 2160 RVA: 0x0001D2E4 File Offset: 0x0001B4E4
		public TestAttribute()
		{
		}

		// Token: 0x040001CD RID: 461
		private object _expectedResult;

		// Token: 0x040001CE RID: 462
		private readonly NUnitTestCaseBuilder _builder = new NUnitTestCaseBuilder();

		// Token: 0x040001CF RID: 463
		[CompilerGenerated]
		private string <Description>k__BackingField;

		// Token: 0x040001D0 RID: 464
		[CompilerGenerated]
		private string <Author>k__BackingField;

		// Token: 0x040001D1 RID: 465
		[CompilerGenerated]
		private Type <TestOf>k__BackingField;

		// Token: 0x040001D2 RID: 466
		[CompilerGenerated]
		private bool <HasExpectedResult>k__BackingField;
	}
}
