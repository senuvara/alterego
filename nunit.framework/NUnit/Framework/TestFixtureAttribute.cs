﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework
{
	// Token: 0x0200005B RID: 91
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	public class TestFixtureAttribute : NUnitAttribute, IFixtureBuilder, ITestFixtureData, ITestData
	{
		// Token: 0x0600029F RID: 671 RVA: 0x00009ECB File Offset: 0x000080CB
		public TestFixtureAttribute() : this(new object[0])
		{
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x00009EDC File Offset: 0x000080DC
		public TestFixtureAttribute(params object[] arguments)
		{
			this.RunState = RunState.Runnable;
			this.Arguments = arguments;
			this.TypeArgs = new Type[0];
			this.Properties = new PropertyBag();
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x060002A1 RID: 673 RVA: 0x00009F1C File Offset: 0x0000811C
		// (set) Token: 0x060002A2 RID: 674 RVA: 0x00009F33 File Offset: 0x00008133
		public string TestName
		{
			[CompilerGenerated]
			get
			{
				return this.<TestName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TestName>k__BackingField = value;
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x060002A3 RID: 675 RVA: 0x00009F3C File Offset: 0x0000813C
		// (set) Token: 0x060002A4 RID: 676 RVA: 0x00009F53 File Offset: 0x00008153
		public RunState RunState
		{
			[CompilerGenerated]
			get
			{
				return this.<RunState>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<RunState>k__BackingField = value;
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x060002A5 RID: 677 RVA: 0x00009F5C File Offset: 0x0000815C
		// (set) Token: 0x060002A6 RID: 678 RVA: 0x00009F73 File Offset: 0x00008173
		public object[] Arguments
		{
			[CompilerGenerated]
			get
			{
				return this.<Arguments>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Arguments>k__BackingField = value;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x060002A7 RID: 679 RVA: 0x00009F7C File Offset: 0x0000817C
		// (set) Token: 0x060002A8 RID: 680 RVA: 0x00009F93 File Offset: 0x00008193
		public IPropertyBag Properties
		{
			[CompilerGenerated]
			get
			{
				return this.<Properties>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Properties>k__BackingField = value;
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x060002A9 RID: 681 RVA: 0x00009F9C File Offset: 0x0000819C
		// (set) Token: 0x060002AA RID: 682 RVA: 0x00009FB3 File Offset: 0x000081B3
		public Type[] TypeArgs
		{
			[CompilerGenerated]
			get
			{
				return this.<TypeArgs>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TypeArgs>k__BackingField = value;
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x060002AB RID: 683 RVA: 0x00009FBC File Offset: 0x000081BC
		// (set) Token: 0x060002AC RID: 684 RVA: 0x00009FE3 File Offset: 0x000081E3
		public string Description
		{
			get
			{
				return this.Properties.Get("Description") as string;
			}
			set
			{
				this.Properties.Set("Description", value);
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x060002AD RID: 685 RVA: 0x00009FF8 File Offset: 0x000081F8
		// (set) Token: 0x060002AE RID: 686 RVA: 0x0000A01F File Offset: 0x0000821F
		public string Author
		{
			get
			{
				return this.Properties.Get("Author") as string;
			}
			set
			{
				this.Properties.Set("Author", value);
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x060002AF RID: 687 RVA: 0x0000A034 File Offset: 0x00008234
		// (set) Token: 0x060002B0 RID: 688 RVA: 0x0000A04C File Offset: 0x0000824C
		public Type TestOf
		{
			get
			{
				return this._testOf;
			}
			set
			{
				this._testOf = value;
				this.Properties.Set("TestOf", value.FullName);
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x060002B1 RID: 689 RVA: 0x0000A070 File Offset: 0x00008270
		// (set) Token: 0x060002B2 RID: 690 RVA: 0x0000A088 File Offset: 0x00008288
		public string Ignore
		{
			get
			{
				return this.IgnoreReason;
			}
			set
			{
				this.IgnoreReason = value;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x060002B3 RID: 691 RVA: 0x0000A094 File Offset: 0x00008294
		// (set) Token: 0x060002B4 RID: 692 RVA: 0x0000A0BB File Offset: 0x000082BB
		public string Reason
		{
			get
			{
				return this.Properties.Get("_SKIPREASON") as string;
			}
			set
			{
				this.Properties.Set("_SKIPREASON", value);
			}
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x060002B5 RID: 693 RVA: 0x0000A0D0 File Offset: 0x000082D0
		// (set) Token: 0x060002B6 RID: 694 RVA: 0x0000A0E8 File Offset: 0x000082E8
		public string IgnoreReason
		{
			get
			{
				return this.Reason;
			}
			set
			{
				this.RunState = RunState.Ignored;
				this.Reason = value;
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x060002B7 RID: 695 RVA: 0x0000A0FC File Offset: 0x000082FC
		// (set) Token: 0x060002B8 RID: 696 RVA: 0x0000A117 File Offset: 0x00008317
		public bool Explicit
		{
			get
			{
				return this.RunState == RunState.Explicit;
			}
			set
			{
				this.RunState = (value ? RunState.Explicit : RunState.Runnable);
			}
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x060002B9 RID: 697 RVA: 0x0000A128 File Offset: 0x00008328
		// (set) Token: 0x060002BA RID: 698 RVA: 0x0000A208 File Offset: 0x00008408
		public string Category
		{
			get
			{
				IList list = this.Properties["Category"];
				string result;
				if (list == null)
				{
					result = null;
				}
				else
				{
					switch (list.Count)
					{
					case 0:
						result = null;
						break;
					case 1:
						result = (list[0] as string);
						break;
					default:
					{
						string[] array = new string[list.Count];
						int num = 0;
						foreach (object obj in list)
						{
							string text = (string)obj;
							array[num++] = text;
						}
						result = string.Join(",", array);
						break;
					}
					}
				}
				return result;
			}
			set
			{
				foreach (string value2 in value.Split(new char[]
				{
					','
				}))
				{
					this.Properties.Add("Category", value2);
				}
			}
		}

		// Token: 0x060002BB RID: 699 RVA: 0x0000A39C File Offset: 0x0000859C
		public IEnumerable<TestSuite> BuildFrom(ITypeInfo typeInfo)
		{
			yield return this._builder.BuildFrom(typeInfo, this);
			yield break;
		}

		// Token: 0x0400008D RID: 141
		private readonly NUnitTestFixtureBuilder _builder = new NUnitTestFixtureBuilder();

		// Token: 0x0400008E RID: 142
		private Type _testOf;

		// Token: 0x0400008F RID: 143
		[CompilerGenerated]
		private string <TestName>k__BackingField;

		// Token: 0x04000090 RID: 144
		[CompilerGenerated]
		private RunState <RunState>k__BackingField;

		// Token: 0x04000091 RID: 145
		[CompilerGenerated]
		private object[] <Arguments>k__BackingField;

		// Token: 0x04000092 RID: 146
		[CompilerGenerated]
		private IPropertyBag <Properties>k__BackingField;

		// Token: 0x04000093 RID: 147
		[CompilerGenerated]
		private Type[] <TypeArgs>k__BackingField;

		// Token: 0x020001AE RID: 430
		[CompilerGenerated]
		private sealed class <BuildFrom>d__0 : IEnumerable<TestSuite>, IEnumerable, IEnumerator<TestSuite>, IEnumerator, IDisposable
		{
			// Token: 0x06000B18 RID: 2840 RVA: 0x0000A258 File Offset: 0x00008458
			[DebuggerHidden]
			IEnumerator<TestSuite> IEnumerable<TestSuite>.GetEnumerator()
			{
				TestFixtureAttribute.<BuildFrom>d__0 <BuildFrom>d__;
				if (Thread.CurrentThread.ManagedThreadId == this.<>l__initialThreadId && this.<>1__state == -2)
				{
					this.<>1__state = 0;
					<BuildFrom>d__ = this;
				}
				else
				{
					<BuildFrom>d__ = new TestFixtureAttribute.<BuildFrom>d__0(0);
					<BuildFrom>d__.<>4__this = this;
				}
				<BuildFrom>d__.typeInfo = typeInfo;
				return <BuildFrom>d__;
			}

			// Token: 0x06000B19 RID: 2841 RVA: 0x0000A2BC File Offset: 0x000084BC
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<NUnit.Framework.Internal.TestSuite>.GetEnumerator();
			}

			// Token: 0x06000B1A RID: 2842 RVA: 0x0000A2D4 File Offset: 0x000084D4
			bool IEnumerator.MoveNext()
			{
				switch (this.<>1__state)
				{
				case 0:
					this.<>1__state = -1;
					this.<>2__current = this._builder.BuildFrom(typeInfo, this);
					this.<>1__state = 1;
					return true;
				case 1:
					this.<>1__state = -1;
					break;
				}
				return false;
			}

			// Token: 0x17000280 RID: 640
			// (get) Token: 0x06000B1B RID: 2843 RVA: 0x0000A340 File Offset: 0x00008540
			TestSuite IEnumerator<TestSuite>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B1C RID: 2844 RVA: 0x0000A357 File Offset: 0x00008557
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000B1D RID: 2845 RVA: 0x0000A35E File Offset: 0x0000855E
			void IDisposable.Dispose()
			{
			}

			// Token: 0x17000281 RID: 641
			// (get) Token: 0x06000B1E RID: 2846 RVA: 0x0000A364 File Offset: 0x00008564
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B1F RID: 2847 RVA: 0x0000A37B File Offset: 0x0000857B
			[DebuggerHidden]
			public <BuildFrom>d__0(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Thread.CurrentThread.ManagedThreadId;
			}

			// Token: 0x040002B3 RID: 691
			private TestSuite <>2__current;

			// Token: 0x040002B4 RID: 692
			private int <>1__state;

			// Token: 0x040002B5 RID: 693
			private int <>l__initialThreadId;

			// Token: 0x040002B6 RID: 694
			public TestFixtureAttribute <>4__this;

			// Token: 0x040002B7 RID: 695
			public ITypeInfo typeInfo;

			// Token: 0x040002B8 RID: 696
			public ITypeInfo <>3__typeInfo;
		}
	}
}
