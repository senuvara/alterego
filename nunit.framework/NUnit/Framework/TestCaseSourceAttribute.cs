﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework
{
	// Token: 0x0200016C RID: 364
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	public class TestCaseSourceAttribute : NUnitAttribute, ITestBuilder, IImplyFixture
	{
		// Token: 0x060009A1 RID: 2465 RVA: 0x000201C4 File Offset: 0x0001E3C4
		public TestCaseSourceAttribute(string sourceName)
		{
			this.SourceName = sourceName;
		}

		// Token: 0x060009A2 RID: 2466 RVA: 0x000201E2 File Offset: 0x0001E3E2
		public TestCaseSourceAttribute(Type sourceType, string sourceName, object[] methodParams)
		{
			this.MethodParams = methodParams;
			this.SourceType = sourceType;
			this.SourceName = sourceName;
		}

		// Token: 0x060009A3 RID: 2467 RVA: 0x00020210 File Offset: 0x0001E410
		public TestCaseSourceAttribute(Type sourceType, string sourceName)
		{
			this.SourceType = sourceType;
			this.SourceName = sourceName;
		}

		// Token: 0x060009A4 RID: 2468 RVA: 0x00020236 File Offset: 0x0001E436
		public TestCaseSourceAttribute(Type sourceType)
		{
			this.SourceType = sourceType;
		}

		// Token: 0x17000237 RID: 567
		// (get) Token: 0x060009A5 RID: 2469 RVA: 0x00020254 File Offset: 0x0001E454
		// (set) Token: 0x060009A6 RID: 2470 RVA: 0x0002026B File Offset: 0x0001E46B
		public object[] MethodParams
		{
			[CompilerGenerated]
			get
			{
				return this.<MethodParams>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<MethodParams>k__BackingField = value;
			}
		}

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x060009A7 RID: 2471 RVA: 0x00020274 File Offset: 0x0001E474
		// (set) Token: 0x060009A8 RID: 2472 RVA: 0x0002028B File Offset: 0x0001E48B
		public string SourceName
		{
			[CompilerGenerated]
			get
			{
				return this.<SourceName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<SourceName>k__BackingField = value;
			}
		}

		// Token: 0x17000239 RID: 569
		// (get) Token: 0x060009A9 RID: 2473 RVA: 0x00020294 File Offset: 0x0001E494
		// (set) Token: 0x060009AA RID: 2474 RVA: 0x000202AB File Offset: 0x0001E4AB
		public Type SourceType
		{
			[CompilerGenerated]
			get
			{
				return this.<SourceType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<SourceType>k__BackingField = value;
			}
		}

		// Token: 0x1700023A RID: 570
		// (get) Token: 0x060009AB RID: 2475 RVA: 0x000202B4 File Offset: 0x0001E4B4
		// (set) Token: 0x060009AC RID: 2476 RVA: 0x000202CB File Offset: 0x0001E4CB
		public string Category
		{
			[CompilerGenerated]
			get
			{
				return this.<Category>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Category>k__BackingField = value;
			}
		}

		// Token: 0x060009AD RID: 2477 RVA: 0x00020510 File Offset: 0x0001E710
		public IEnumerable<TestMethod> BuildFrom(IMethodInfo method, Test suite)
		{
			foreach (ITestCaseData testCaseData in this.GetTestCasesFor(method))
			{
				TestCaseParameters parms = (TestCaseParameters)testCaseData;
				yield return this._builder.BuildTestMethod(method, suite, parms);
			}
			yield break;
		}

		// Token: 0x060009AE RID: 2478 RVA: 0x00020540 File Offset: 0x0001E740
		private IEnumerable<ITestCaseData> GetTestCasesFor(IMethodInfo method)
		{
			List<ITestCaseData> list = new List<ITestCaseData>();
			try
			{
				IEnumerable testCaseSource = this.GetTestCaseSource(method);
				if (testCaseSource != null)
				{
					foreach (object obj in testCaseSource)
					{
						ITestCaseData testCaseData;
						if (obj != null)
						{
							testCaseData = (obj as ITestCaseData);
						}
						else
						{
							object[] args = new object[1];
							testCaseData = new TestCaseParameters(args);
						}
						ITestCaseData testCaseData2 = testCaseData;
						if (testCaseData2 == null)
						{
							object[] array = obj as object[];
							if (array == null && obj is Array)
							{
								Array array2 = obj as Array;
								int num = false ? array2.Length : method.GetParameters().Length;
								if (array2 != null && array2.Rank == 1 && array2.Length == num)
								{
									array = new object[array2.Length];
									for (int i = 0; i < array2.Length; i++)
									{
										array[i] = array2.GetValue(i);
									}
								}
							}
							if (array != null)
							{
								IParameterInfo[] parameters = method.GetParameters();
								int num2 = parameters.Length;
								int num3 = array.Length;
								if (num2 == 1)
								{
									Type parameterType = parameters[0].ParameterType;
									if (num3 == 0 || typeof(object[]).IsAssignableFrom(parameterType))
									{
										if (num3 > 1 || parameterType.IsAssignableFrom(array.GetType()))
										{
											array = new object[]
											{
												obj
											};
										}
									}
								}
							}
							else
							{
								array = new object[]
								{
									obj
								};
							}
							testCaseData2 = new TestCaseParameters(array);
						}
						if (this.Category != null)
						{
							foreach (string value in this.Category.Split(new char[]
							{
								','
							}))
							{
								testCaseData2.Properties.Add("Category", value);
							}
						}
						list.Add(testCaseData2);
					}
				}
			}
			catch (Exception exception)
			{
				list.Clear();
				list.Add(new TestCaseParameters(exception));
			}
			return list;
		}

		// Token: 0x060009AF RID: 2479 RVA: 0x000207FC File Offset: 0x0001E9FC
		private IEnumerable GetTestCaseSource(IMethodInfo method)
		{
			Type type = this.SourceType ?? method.TypeInfo.Type;
			IEnumerable result;
			if (this.SourceName == null)
			{
				result = (Reflect.Construct(type, null) as IEnumerable);
			}
			else
			{
				MemberInfo[] member = type.GetMember(this.SourceName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
				if (member.Length == 1)
				{
					MemberInfo memberInfo = member[0];
					FieldInfo fieldInfo = memberInfo as FieldInfo;
					if (fieldInfo != null)
					{
						return fieldInfo.IsStatic ? ((this.MethodParams == null) ? ((IEnumerable)fieldInfo.GetValue(null)) : TestCaseSourceAttribute.ReturnErrorAsParameter("You have specified a data source field but also given a set of parameters. Fields cannot take parameters, please revise the 3rd parameter passed to the TestCaseSourceAttribute and either remove it or specify a method.")) : TestCaseSourceAttribute.ReturnErrorAsParameter("The sourceName specified on a TestCaseSourceAttribute must refer to a static field, property or method.");
					}
					PropertyInfo propertyInfo = memberInfo as PropertyInfo;
					if (propertyInfo != null)
					{
						return propertyInfo.GetGetMethod(true).IsStatic ? ((this.MethodParams == null) ? ((IEnumerable)propertyInfo.GetValue(null, null)) : TestCaseSourceAttribute.ReturnErrorAsParameter("You have specified a data source property but also given a set of parameters. Properties cannot take parameters, please revise the 3rd parameter passed to the TestCaseSource attribute and either remove it or specify a method.")) : TestCaseSourceAttribute.ReturnErrorAsParameter("The sourceName specified on a TestCaseSourceAttribute must refer to a static field, property or method.");
					}
					MethodInfo methodInfo = memberInfo as MethodInfo;
					if (methodInfo != null)
					{
						return methodInfo.IsStatic ? ((this.MethodParams == null || methodInfo.GetParameters().Length == this.MethodParams.Length) ? ((IEnumerable)methodInfo.Invoke(null, this.MethodParams)) : TestCaseSourceAttribute.ReturnErrorAsParameter("You have given the wrong number of arguments to the method in the TestCaseSourceAttribute, please check the number of parameters passed in the object is correct in the 3rd parameter for the TestCaseSourceAttribute and this matches the number of parameters in the target method and try again.")) : TestCaseSourceAttribute.ReturnErrorAsParameter("The sourceName specified on a TestCaseSourceAttribute must refer to a static field, property or method.");
					}
				}
				result = null;
			}
			return result;
		}

		// Token: 0x060009B0 RID: 2480 RVA: 0x0002097C File Offset: 0x0001EB7C
		private static IEnumerable ReturnErrorAsParameter(string errorMessage)
		{
			TestCaseParameters testCaseParameters = new TestCaseParameters();
			testCaseParameters.RunState = RunState.NotRunnable;
			testCaseParameters.Properties.Set("_SKIPREASON", errorMessage);
			return new TestCaseParameters[]
			{
				testCaseParameters
			};
		}

		// Token: 0x04000228 RID: 552
		private const string SourceMustBeStatic = "The sourceName specified on a TestCaseSourceAttribute must refer to a static field, property or method.";

		// Token: 0x04000229 RID: 553
		private const string ParamGivenToField = "You have specified a data source field but also given a set of parameters. Fields cannot take parameters, please revise the 3rd parameter passed to the TestCaseSourceAttribute and either remove it or specify a method.";

		// Token: 0x0400022A RID: 554
		private const string ParamGivenToProperty = "You have specified a data source property but also given a set of parameters. Properties cannot take parameters, please revise the 3rd parameter passed to the TestCaseSource attribute and either remove it or specify a method.";

		// Token: 0x0400022B RID: 555
		private const string NumberOfArgsDoesNotMatch = "You have given the wrong number of arguments to the method in the TestCaseSourceAttribute, please check the number of parameters passed in the object is correct in the 3rd parameter for the TestCaseSourceAttribute and this matches the number of parameters in the target method and try again.";

		// Token: 0x0400022C RID: 556
		private NUnitTestCaseBuilder _builder = new NUnitTestCaseBuilder();

		// Token: 0x0400022D RID: 557
		[CompilerGenerated]
		private object[] <MethodParams>k__BackingField;

		// Token: 0x0400022E RID: 558
		[CompilerGenerated]
		private string <SourceName>k__BackingField;

		// Token: 0x0400022F RID: 559
		[CompilerGenerated]
		private Type <SourceType>k__BackingField;

		// Token: 0x04000230 RID: 560
		[CompilerGenerated]
		private string <Category>k__BackingField;

		// Token: 0x020001C3 RID: 451
		[CompilerGenerated]
		private sealed class <BuildFrom>d__0 : IEnumerable<TestMethod>, IEnumerable, IEnumerator<TestMethod>, IEnumerator, IDisposable
		{
			// Token: 0x06000B5D RID: 2909 RVA: 0x000202D4 File Offset: 0x0001E4D4
			[DebuggerHidden]
			IEnumerator<TestMethod> IEnumerable<TestMethod>.GetEnumerator()
			{
				TestCaseSourceAttribute.<BuildFrom>d__0 <BuildFrom>d__;
				if (Thread.CurrentThread.ManagedThreadId == this.<>l__initialThreadId && this.<>1__state == -2)
				{
					this.<>1__state = 0;
					<BuildFrom>d__ = this;
				}
				else
				{
					<BuildFrom>d__ = new TestCaseSourceAttribute.<BuildFrom>d__0(0);
					<BuildFrom>d__.<>4__this = this;
				}
				<BuildFrom>d__.method = method;
				<BuildFrom>d__.suite = suite;
				return <BuildFrom>d__;
			}

			// Token: 0x06000B5E RID: 2910 RVA: 0x00020344 File Offset: 0x0001E544
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<NUnit.Framework.Internal.TestMethod>.GetEnumerator();
			}

			// Token: 0x06000B5F RID: 2911 RVA: 0x0002035C File Offset: 0x0001E55C
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						enumerator = this.GetTestCasesFor(method).GetEnumerator();
						this.<>1__state = 1;
						goto IL_A2;
					case 2:
						this.<>1__state = 1;
						goto IL_A2;
					}
					goto IL_B9;
					IL_A2:
					if (enumerator.MoveNext())
					{
						parms = (TestCaseParameters)enumerator.Current;
						this.<>2__current = this._builder.BuildTestMethod(method, suite, parms);
						this.<>1__state = 2;
						return true;
					}
					this.<>m__Finally3();
					IL_B9:
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x17000288 RID: 648
			// (get) Token: 0x06000B60 RID: 2912 RVA: 0x00020440 File Offset: 0x0001E640
			TestMethod IEnumerator<TestMethod>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B61 RID: 2913 RVA: 0x00020457 File Offset: 0x0001E657
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000B62 RID: 2914 RVA: 0x00020460 File Offset: 0x0001E660
			void IDisposable.Dispose()
			{
				switch (this.<>1__state)
				{
				case 1:
					break;
				case 2:
					break;
				default:
					return;
				}
				try
				{
				}
				finally
				{
					this.<>m__Finally3();
				}
			}

			// Token: 0x17000289 RID: 649
			// (get) Token: 0x06000B63 RID: 2915 RVA: 0x000204A8 File Offset: 0x0001E6A8
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B64 RID: 2916 RVA: 0x000204BF File Offset: 0x0001E6BF
			[DebuggerHidden]
			public <BuildFrom>d__0(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Thread.CurrentThread.ManagedThreadId;
			}

			// Token: 0x06000B65 RID: 2917 RVA: 0x000204E0 File Offset: 0x0001E6E0
			private void <>m__Finally3()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x040002E9 RID: 745
			private TestMethod <>2__current;

			// Token: 0x040002EA RID: 746
			private int <>1__state;

			// Token: 0x040002EB RID: 747
			private int <>l__initialThreadId;

			// Token: 0x040002EC RID: 748
			public TestCaseSourceAttribute <>4__this;

			// Token: 0x040002ED RID: 749
			public IMethodInfo method;

			// Token: 0x040002EE RID: 750
			public IMethodInfo <>3__method;

			// Token: 0x040002EF RID: 751
			public Test suite;

			// Token: 0x040002F0 RID: 752
			public Test <>3__suite;

			// Token: 0x040002F1 RID: 753
			public TestCaseParameters <parms>5__1;

			// Token: 0x040002F2 RID: 754
			public IEnumerator<ITestCaseData> <>7__wrap2;
		}
	}
}
