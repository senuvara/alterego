﻿using System;
using System.Globalization;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x02000080 RID: 128
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class IgnoreAttribute : NUnitAttribute, IApplyToTest
	{
		// Token: 0x0600046A RID: 1130 RVA: 0x0000EAB0 File Offset: 0x0000CCB0
		public IgnoreAttribute(string reason)
		{
			this._reason = reason;
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x0600046B RID: 1131 RVA: 0x0000EAC4 File Offset: 0x0000CCC4
		// (set) Token: 0x0600046C RID: 1132 RVA: 0x0000EADC File Offset: 0x0000CCDC
		public string Reason
		{
			get
			{
				return this.Reason;
			}
			private set
			{
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x0600046D RID: 1133 RVA: 0x0000EAE0 File Offset: 0x0000CCE0
		// (set) Token: 0x0600046E RID: 1134 RVA: 0x0000EAF8 File Offset: 0x0000CCF8
		public string Until
		{
			get
			{
				return this._until;
			}
			set
			{
				this._until = value;
				this._untilDate = new DateTime?(DateTime.Parse(value, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal));
			}
		}

		// Token: 0x0600046F RID: 1135 RVA: 0x0000EB1C File Offset: 0x0000CD1C
		public void ApplyToTest(Test test)
		{
			if (test.RunState != RunState.NotRunnable)
			{
				if (this._untilDate != null)
				{
					if (this._untilDate.Value > DateTime.Now)
					{
						test.RunState = RunState.Ignored;
						string value = string.Format("Ignoring until {0}. {1}", this._untilDate.Value.ToString("u"), this._reason);
						test.Properties.Set("_SKIPREASON", value);
					}
					test.Properties.Set("IgnoreUntilDate", this._untilDate.Value.ToString("u"));
				}
				else
				{
					test.RunState = RunState.Ignored;
					test.Properties.Set("_SKIPREASON", this._reason);
				}
			}
		}

		// Token: 0x040000CA RID: 202
		private string _reason;

		// Token: 0x040000CB RID: 203
		private DateTime? _untilDate;

		// Token: 0x040000CC RID: 204
		private string _until;
	}
}
