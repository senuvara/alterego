﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x020000B6 RID: 182
	public class TestCaseData : TestCaseParameters
	{
		// Token: 0x06000535 RID: 1333 RVA: 0x000128B4 File Offset: 0x00010AB4
		public TestCaseData(params object[] args)
		{
			object[] args2;
			if (args != null)
			{
				args2 = args;
			}
			else
			{
				object[] array = new object[1];
				args2 = array;
			}
			base..ctor(args2);
		}

		// Token: 0x06000536 RID: 1334 RVA: 0x000128D8 File Offset: 0x00010AD8
		public TestCaseData(object arg) : base(new object[]
		{
			arg
		})
		{
		}

		// Token: 0x06000537 RID: 1335 RVA: 0x000128FC File Offset: 0x00010AFC
		public TestCaseData(object arg1, object arg2) : base(new object[]
		{
			arg1,
			arg2
		})
		{
		}

		// Token: 0x06000538 RID: 1336 RVA: 0x00012924 File Offset: 0x00010B24
		public TestCaseData(object arg1, object arg2, object arg3) : base(new object[]
		{
			arg1,
			arg2,
			arg3
		})
		{
		}

		// Token: 0x06000539 RID: 1337 RVA: 0x00012950 File Offset: 0x00010B50
		public TestCaseData Returns(object result)
		{
			base.ExpectedResult = result;
			return this;
		}

		// Token: 0x0600053A RID: 1338 RVA: 0x0001296C File Offset: 0x00010B6C
		public TestCaseData SetName(string name)
		{
			base.TestName = name;
			return this;
		}

		// Token: 0x0600053B RID: 1339 RVA: 0x00012988 File Offset: 0x00010B88
		public TestCaseData SetDescription(string description)
		{
			base.Properties.Set("Description", description);
			return this;
		}

		// Token: 0x0600053C RID: 1340 RVA: 0x000129B0 File Offset: 0x00010BB0
		public TestCaseData SetCategory(string category)
		{
			base.Properties.Add("Category", category);
			return this;
		}

		// Token: 0x0600053D RID: 1341 RVA: 0x000129D8 File Offset: 0x00010BD8
		public TestCaseData SetProperty(string propName, string propValue)
		{
			base.Properties.Add(propName, propValue);
			return this;
		}

		// Token: 0x0600053E RID: 1342 RVA: 0x000129FC File Offset: 0x00010BFC
		public TestCaseData SetProperty(string propName, int propValue)
		{
			base.Properties.Add(propName, propValue);
			return this;
		}

		// Token: 0x0600053F RID: 1343 RVA: 0x00012A24 File Offset: 0x00010C24
		public TestCaseData SetProperty(string propName, double propValue)
		{
			base.Properties.Add(propName, propValue);
			return this;
		}

		// Token: 0x06000540 RID: 1344 RVA: 0x00012A4C File Offset: 0x00010C4C
		public TestCaseData Explicit()
		{
			base.RunState = RunState.Explicit;
			return this;
		}

		// Token: 0x06000541 RID: 1345 RVA: 0x00012A68 File Offset: 0x00010C68
		public TestCaseData Explicit(string reason)
		{
			base.RunState = RunState.Explicit;
			base.Properties.Set("_SKIPREASON", reason);
			return this;
		}

		// Token: 0x06000542 RID: 1346 RVA: 0x00012A98 File Offset: 0x00010C98
		public TestCaseData Ignore(string reason)
		{
			base.RunState = RunState.Ignored;
			base.Properties.Set("_SKIPREASON", reason);
			return this;
		}
	}
}
