﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework.Constraints;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;

namespace NUnit.Framework
{
	// Token: 0x02000045 RID: 69
	public class TestContext
	{
		// Token: 0x06000207 RID: 519 RVA: 0x0000756E File Offset: 0x0000576E
		public TestContext(ITestExecutionContext testExecutionContext)
		{
			this._testExecutionContext = testExecutionContext;
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x06000208 RID: 520 RVA: 0x00007580 File Offset: 0x00005780
		public static TestContext CurrentContext
		{
			get
			{
				return new TestContext(TestContext.CurrentTestExecutionContext ?? TestExecutionContext.CurrentContext);
			}
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x06000209 RID: 521 RVA: 0x000075A8 File Offset: 0x000057A8
		public static TextWriter Out
		{
			get
			{
				return (TestContext.CurrentTestExecutionContext ?? TestExecutionContext.CurrentContext).OutWriter;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x0600020A RID: 522 RVA: 0x000075D0 File Offset: 0x000057D0
		public TestContext.TestAdapter Test
		{
			get
			{
				TestContext.TestAdapter result;
				if ((result = this._test) == null)
				{
					result = (this._test = new TestContext.TestAdapter(this._testExecutionContext.CurrentTest));
				}
				return result;
			}
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x0600020B RID: 523 RVA: 0x00007608 File Offset: 0x00005808
		public TestContext.ResultAdapter Result
		{
			get
			{
				TestContext.ResultAdapter result;
				if ((result = this._result) == null)
				{
					result = (this._result = new TestContext.ResultAdapter(this._testExecutionContext.CurrentResult));
				}
				return result;
			}
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x0600020C RID: 524 RVA: 0x00007640 File Offset: 0x00005840
		public string WorkerId
		{
			get
			{
				return this._testExecutionContext.WorkerId;
			}
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x0600020D RID: 525 RVA: 0x00007660 File Offset: 0x00005860
		public string TestDirectory
		{
			get
			{
				Test currentTest = this._testExecutionContext.CurrentTest;
				string directoryName;
				if (currentTest != null)
				{
					directoryName = AssemblyHelper.GetDirectoryName(currentTest.TypeInfo.Assembly);
				}
				else
				{
					directoryName = AssemblyHelper.GetDirectoryName(Assembly.GetCallingAssembly());
				}
				return directoryName;
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x0600020E RID: 526 RVA: 0x000076A4 File Offset: 0x000058A4
		public string WorkDirectory
		{
			get
			{
				return this._testExecutionContext.WorkDirectory;
			}
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x0600020F RID: 527 RVA: 0x000076C4 File Offset: 0x000058C4
		public Randomizer Random
		{
			get
			{
				return this._testExecutionContext.RandomGenerator;
			}
		}

		// Token: 0x06000210 RID: 528 RVA: 0x000076E1 File Offset: 0x000058E1
		public static void Write(bool value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x06000211 RID: 529 RVA: 0x000076F0 File Offset: 0x000058F0
		public static void Write(char value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x06000212 RID: 530 RVA: 0x000076FF File Offset: 0x000058FF
		public static void Write(char[] value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x06000213 RID: 531 RVA: 0x0000770E File Offset: 0x0000590E
		public static void Write(double value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x06000214 RID: 532 RVA: 0x0000771D File Offset: 0x0000591D
		public static void Write(int value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x06000215 RID: 533 RVA: 0x0000772C File Offset: 0x0000592C
		public static void Write(long value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x06000216 RID: 534 RVA: 0x0000773B File Offset: 0x0000593B
		public static void Write(decimal value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x06000217 RID: 535 RVA: 0x0000774A File Offset: 0x0000594A
		public static void Write(object value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x06000218 RID: 536 RVA: 0x00007759 File Offset: 0x00005959
		public static void Write(float value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x06000219 RID: 537 RVA: 0x00007768 File Offset: 0x00005968
		public static void Write(string value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x0600021A RID: 538 RVA: 0x00007777 File Offset: 0x00005977
		[CLSCompliant(false)]
		public static void Write(uint value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x0600021B RID: 539 RVA: 0x00007786 File Offset: 0x00005986
		[CLSCompliant(false)]
		public static void Write(ulong value)
		{
			TestContext.Out.Write(value);
		}

		// Token: 0x0600021C RID: 540 RVA: 0x00007795 File Offset: 0x00005995
		public static void Write(string format, object arg1)
		{
			TestContext.Out.Write(format, arg1);
		}

		// Token: 0x0600021D RID: 541 RVA: 0x000077A5 File Offset: 0x000059A5
		public static void Write(string format, object arg1, object arg2)
		{
			TestContext.Out.Write(format, arg1, arg2);
		}

		// Token: 0x0600021E RID: 542 RVA: 0x000077B6 File Offset: 0x000059B6
		public static void Write(string format, object arg1, object arg2, object arg3)
		{
			TestContext.Out.Write(format, arg1, arg2, arg3);
		}

		// Token: 0x0600021F RID: 543 RVA: 0x000077C8 File Offset: 0x000059C8
		public static void Write(string format, params object[] args)
		{
			TestContext.Out.Write(format, args);
		}

		// Token: 0x06000220 RID: 544 RVA: 0x000077D8 File Offset: 0x000059D8
		public static void WriteLine()
		{
			TestContext.Out.WriteLine();
		}

		// Token: 0x06000221 RID: 545 RVA: 0x000077E6 File Offset: 0x000059E6
		public static void WriteLine(bool value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x06000222 RID: 546 RVA: 0x000077F5 File Offset: 0x000059F5
		public static void WriteLine(char value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x06000223 RID: 547 RVA: 0x00007804 File Offset: 0x00005A04
		public static void WriteLine(char[] value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x06000224 RID: 548 RVA: 0x00007813 File Offset: 0x00005A13
		public static void WriteLine(double value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x06000225 RID: 549 RVA: 0x00007822 File Offset: 0x00005A22
		public static void WriteLine(int value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x06000226 RID: 550 RVA: 0x00007831 File Offset: 0x00005A31
		public static void WriteLine(long value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x06000227 RID: 551 RVA: 0x00007840 File Offset: 0x00005A40
		public static void WriteLine(decimal value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x06000228 RID: 552 RVA: 0x0000784F File Offset: 0x00005A4F
		public static void WriteLine(object value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x06000229 RID: 553 RVA: 0x0000785E File Offset: 0x00005A5E
		public static void WriteLine(float value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x0600022A RID: 554 RVA: 0x0000786D File Offset: 0x00005A6D
		public static void WriteLine(string value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x0600022B RID: 555 RVA: 0x0000787C File Offset: 0x00005A7C
		[CLSCompliant(false)]
		public static void WriteLine(uint value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x0600022C RID: 556 RVA: 0x0000788B File Offset: 0x00005A8B
		[CLSCompliant(false)]
		public static void WriteLine(ulong value)
		{
			TestContext.Out.WriteLine(value);
		}

		// Token: 0x0600022D RID: 557 RVA: 0x0000789A File Offset: 0x00005A9A
		public static void WriteLine(string format, object arg1)
		{
			TestContext.Out.WriteLine(format, arg1);
		}

		// Token: 0x0600022E RID: 558 RVA: 0x000078AA File Offset: 0x00005AAA
		public static void WriteLine(string format, object arg1, object arg2)
		{
			TestContext.Out.WriteLine(format, arg1, arg2);
		}

		// Token: 0x0600022F RID: 559 RVA: 0x000078BB File Offset: 0x00005ABB
		public static void WriteLine(string format, object arg1, object arg2, object arg3)
		{
			TestContext.Out.WriteLine(format, arg1, arg2, arg3);
		}

		// Token: 0x06000230 RID: 560 RVA: 0x000078CD File Offset: 0x00005ACD
		public static void WriteLine(string format, params object[] args)
		{
			TestContext.Out.WriteLine(format, args);
		}

		// Token: 0x06000231 RID: 561 RVA: 0x000078DD File Offset: 0x00005ADD
		public static void AddFormatter(ValueFormatterFactory formatterFactory)
		{
			TestExecutionContext.CurrentContext.AddFormatter(formatterFactory);
		}

		// Token: 0x06000232 RID: 562 RVA: 0x00007968 File Offset: 0x00005B68
		public static void AddFormatter<TSUPPORTED>(ValueFormatter formatter)
		{
			TestContext.AddFormatter((ValueFormatter next) => (object val) => (val is TSUPPORTED) ? formatter(val) : next(val));
		}

		// Token: 0x06000233 RID: 563 RVA: 0x00007996 File Offset: 0x00005B96
		// Note: this type is marked as 'beforefieldinit'.
		static TestContext()
		{
		}

		// Token: 0x0400006D RID: 109
		public static ITestExecutionContext CurrentTestExecutionContext;

		// Token: 0x0400006E RID: 110
		private TestContext.TestAdapter _test;

		// Token: 0x0400006F RID: 111
		private TestContext.ResultAdapter _result;

		// Token: 0x04000070 RID: 112
		public static TextWriter Error = new EventListenerTextWriter("Error", Console.Error);

		// Token: 0x04000071 RID: 113
		public static readonly TextWriter Progress = new EventListenerTextWriter("Progress", Console.Error);

		// Token: 0x04000072 RID: 114
		public static readonly TestParameters Parameters = new TestParameters();

		// Token: 0x04000073 RID: 115
		private ITestExecutionContext _testExecutionContext;

		// Token: 0x02000046 RID: 70
		public class TestAdapter
		{
			// Token: 0x06000234 RID: 564 RVA: 0x000079CA File Offset: 0x00005BCA
			public TestAdapter(Test test)
			{
				this._test = test;
			}

			// Token: 0x1700009C RID: 156
			// (get) Token: 0x06000235 RID: 565 RVA: 0x000079DC File Offset: 0x00005BDC
			public string ID
			{
				get
				{
					return this._test.Id;
				}
			}

			// Token: 0x1700009D RID: 157
			// (get) Token: 0x06000236 RID: 566 RVA: 0x000079FC File Offset: 0x00005BFC
			public string Name
			{
				get
				{
					return this._test.Name;
				}
			}

			// Token: 0x1700009E RID: 158
			// (get) Token: 0x06000237 RID: 567 RVA: 0x00007A1C File Offset: 0x00005C1C
			public string MethodName
			{
				get
				{
					return (this._test is TestMethod) ? this._test.Method.Name : null;
				}
			}

			// Token: 0x1700009F RID: 159
			// (get) Token: 0x06000238 RID: 568 RVA: 0x00007A50 File Offset: 0x00005C50
			public string FullName
			{
				get
				{
					return this._test.FullName;
				}
			}

			// Token: 0x170000A0 RID: 160
			// (get) Token: 0x06000239 RID: 569 RVA: 0x00007A70 File Offset: 0x00005C70
			public string ClassName
			{
				get
				{
					return this._test.ClassName;
				}
			}

			// Token: 0x170000A1 RID: 161
			// (get) Token: 0x0600023A RID: 570 RVA: 0x00007A90 File Offset: 0x00005C90
			public IPropertyBag Properties
			{
				get
				{
					return this._test.Properties;
				}
			}

			// Token: 0x04000074 RID: 116
			private readonly Test _test;
		}

		// Token: 0x02000047 RID: 71
		public class ResultAdapter
		{
			// Token: 0x0600023B RID: 571 RVA: 0x00007AAD File Offset: 0x00005CAD
			public ResultAdapter(TestResult result)
			{
				this._result = result;
			}

			// Token: 0x170000A2 RID: 162
			// (get) Token: 0x0600023C RID: 572 RVA: 0x00007AC0 File Offset: 0x00005CC0
			public ResultState Outcome
			{
				get
				{
					return this._result.ResultState;
				}
			}

			// Token: 0x170000A3 RID: 163
			// (get) Token: 0x0600023D RID: 573 RVA: 0x00007AE0 File Offset: 0x00005CE0
			public string Message
			{
				get
				{
					return this._result.Message;
				}
			}

			// Token: 0x170000A4 RID: 164
			// (get) Token: 0x0600023E RID: 574 RVA: 0x00007B00 File Offset: 0x00005D00
			public virtual string StackTrace
			{
				get
				{
					return this._result.StackTrace;
				}
			}

			// Token: 0x170000A5 RID: 165
			// (get) Token: 0x0600023F RID: 575 RVA: 0x00007B20 File Offset: 0x00005D20
			public int FailCount
			{
				get
				{
					return this._result.FailCount;
				}
			}

			// Token: 0x170000A6 RID: 166
			// (get) Token: 0x06000240 RID: 576 RVA: 0x00007B40 File Offset: 0x00005D40
			public int PassCount
			{
				get
				{
					return this._result.PassCount;
				}
			}

			// Token: 0x170000A7 RID: 167
			// (get) Token: 0x06000241 RID: 577 RVA: 0x00007B60 File Offset: 0x00005D60
			public int SkipCount
			{
				get
				{
					return this._result.SkipCount;
				}
			}

			// Token: 0x170000A8 RID: 168
			// (get) Token: 0x06000242 RID: 578 RVA: 0x00007B80 File Offset: 0x00005D80
			public int InconclusiveCount
			{
				get
				{
					return this._result.InconclusiveCount;
				}
			}

			// Token: 0x04000075 RID: 117
			private readonly TestResult _result;
		}

		// Token: 0x020001AC RID: 428
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2<TSUPPORTED>
		{
			// Token: 0x06000B14 RID: 2836 RVA: 0x000078EC File Offset: 0x00005AEC
			public <>c__DisplayClass2()
			{
			}

			// Token: 0x06000B15 RID: 2837 RVA: 0x00007934 File Offset: 0x00005B34
			public ValueFormatter <AddFormatter>b__0(ValueFormatter next)
			{
				return (object val) => (val is TSUPPORTED) ? this.formatter(val) : next(val);
			}

			// Token: 0x040002B0 RID: 688
			public ValueFormatter formatter;

			// Token: 0x020001AD RID: 429
			private sealed class <>c__DisplayClass4
			{
				// Token: 0x06000B16 RID: 2838 RVA: 0x000078F4 File Offset: 0x00005AF4
				public <>c__DisplayClass4()
				{
				}

				// Token: 0x06000B17 RID: 2839 RVA: 0x000078FC File Offset: 0x00005AFC
				public string <AddFormatter>b__1(object val)
				{
					return (val is TSUPPORTED) ? this.CS$<>8__locals3.formatter(val) : this.next(val);
				}

				// Token: 0x040002B1 RID: 689
				public TestContext.<>c__DisplayClass2<TSUPPORTED> CS$<>8__locals3;

				// Token: 0x040002B2 RID: 690
				public ValueFormatter next;
			}
		}
	}
}
