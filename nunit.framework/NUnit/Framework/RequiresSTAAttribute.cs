﻿using System;
using System.Threading;

namespace NUnit.Framework
{
	// Token: 0x0200013D RID: 317
	[Obsolete("Use ApartmentAttribute and pass in ApartmentState.STA instead")]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class RequiresSTAAttribute : PropertyAttribute
	{
		// Token: 0x06000873 RID: 2163 RVA: 0x0001D325 File Offset: 0x0001B525
		public RequiresSTAAttribute()
		{
			base.Properties.Add("ApartmentState", ApartmentState.STA);
		}
	}
}
