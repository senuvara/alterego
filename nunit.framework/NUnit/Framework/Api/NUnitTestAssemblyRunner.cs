﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;

namespace NUnit.Framework.Api
{
	// Token: 0x020001AA RID: 426
	public class NUnitTestAssemblyRunner : ITestAssemblyRunner
	{
		// Token: 0x06000AFB RID: 2811 RVA: 0x00024D63 File Offset: 0x00022F63
		public NUnitTestAssemblyRunner(ITestAssemblyBuilder builder)
		{
			this._builder = builder;
		}

		// Token: 0x17000278 RID: 632
		// (get) Token: 0x06000AFC RID: 2812 RVA: 0x00024D84 File Offset: 0x00022F84
		// (set) Token: 0x06000AFD RID: 2813 RVA: 0x00024D9B File Offset: 0x00022F9B
		public ITest LoadedTest
		{
			[CompilerGenerated]
			get
			{
				return this.<LoadedTest>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<LoadedTest>k__BackingField = value;
			}
		}

		// Token: 0x17000279 RID: 633
		// (get) Token: 0x06000AFE RID: 2814 RVA: 0x00024DA4 File Offset: 0x00022FA4
		public ITestResult Result
		{
			get
			{
				return (this.TopLevelWorkItem == null) ? null : this.TopLevelWorkItem.Result;
			}
		}

		// Token: 0x1700027A RID: 634
		// (get) Token: 0x06000AFF RID: 2815 RVA: 0x00024DCC File Offset: 0x00022FCC
		public bool IsTestLoaded
		{
			get
			{
				return this.LoadedTest != null;
			}
		}

		// Token: 0x1700027B RID: 635
		// (get) Token: 0x06000B00 RID: 2816 RVA: 0x00024DEC File Offset: 0x00022FEC
		public bool IsTestRunning
		{
			get
			{
				return this.TopLevelWorkItem != null && this.TopLevelWorkItem.State == WorkItemState.Running;
			}
		}

		// Token: 0x1700027C RID: 636
		// (get) Token: 0x06000B01 RID: 2817 RVA: 0x00024E18 File Offset: 0x00023018
		public bool IsTestComplete
		{
			get
			{
				return this.TopLevelWorkItem != null && this.TopLevelWorkItem.State == WorkItemState.Complete;
			}
		}

		// Token: 0x1700027D RID: 637
		// (get) Token: 0x06000B02 RID: 2818 RVA: 0x00024E44 File Offset: 0x00023044
		// (set) Token: 0x06000B03 RID: 2819 RVA: 0x00024E5B File Offset: 0x0002305B
		protected IDictionary<string, object> Settings
		{
			[CompilerGenerated]
			get
			{
				return this.<Settings>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Settings>k__BackingField = value;
			}
		}

		// Token: 0x1700027E RID: 638
		// (get) Token: 0x06000B04 RID: 2820 RVA: 0x00024E64 File Offset: 0x00023064
		// (set) Token: 0x06000B05 RID: 2821 RVA: 0x00024E7B File Offset: 0x0002307B
		private WorkItem TopLevelWorkItem
		{
			[CompilerGenerated]
			get
			{
				return this.<TopLevelWorkItem>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TopLevelWorkItem>k__BackingField = value;
			}
		}

		// Token: 0x1700027F RID: 639
		// (get) Token: 0x06000B06 RID: 2822 RVA: 0x00024E84 File Offset: 0x00023084
		// (set) Token: 0x06000B07 RID: 2823 RVA: 0x00024E9B File Offset: 0x0002309B
		private TestExecutionContext Context
		{
			[CompilerGenerated]
			get
			{
				return this.<Context>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Context>k__BackingField = value;
			}
		}

		// Token: 0x06000B08 RID: 2824 RVA: 0x00024EA4 File Offset: 0x000230A4
		public ITest Load(string assemblyName, IDictionary<string, object> settings)
		{
			this.Settings = settings;
			if (settings.ContainsKey("RandomSeed"))
			{
				Randomizer.InitialSeed = (int)settings["RandomSeed"];
			}
			return this.LoadedTest = this._builder.Build(assemblyName, settings);
		}

		// Token: 0x06000B09 RID: 2825 RVA: 0x00024F00 File Offset: 0x00023100
		public ITest Load(Assembly assembly, IDictionary<string, object> settings)
		{
			this.Settings = settings;
			if (settings.ContainsKey("RandomSeed"))
			{
				Randomizer.InitialSeed = (int)settings["RandomSeed"];
			}
			return this.LoadedTest = this._builder.Build(assembly, settings);
		}

		// Token: 0x06000B0A RID: 2826 RVA: 0x00024F5C File Offset: 0x0002315C
		public int CountTestCases(ITestFilter filter)
		{
			if (this.LoadedTest == null)
			{
				throw new InvalidOperationException("The CountTestCases method was called but no test has been loaded");
			}
			return this.CountTestCases(this.LoadedTest, filter);
		}

		// Token: 0x06000B0B RID: 2827 RVA: 0x00024F98 File Offset: 0x00023198
		public ITestResult Run(ITestListener listener, ITestFilter filter)
		{
			this.RunAsync(listener, filter);
			this.WaitForCompletion(-1);
			return this.Result;
		}

		// Token: 0x06000B0C RID: 2828 RVA: 0x00024FC4 File Offset: 0x000231C4
		public void RunAsync(ITestListener listener, ITestFilter filter)
		{
			NUnitTestAssemblyRunner.log.Info("Running tests");
			if (this.LoadedTest == null)
			{
				throw new InvalidOperationException("The Run method was called but no test has been loaded");
			}
			this._runComplete.Reset();
			this.CreateTestExecutionContext(listener);
			this.TopLevelWorkItem = WorkItem.CreateWorkItem(this.LoadedTest, filter);
			this.TopLevelWorkItem.InitializeContext(this.Context);
			this.TopLevelWorkItem.Completed += this.OnRunCompleted;
			this.StartRun(listener);
		}

		// Token: 0x06000B0D RID: 2829 RVA: 0x00025058 File Offset: 0x00023258
		public bool WaitForCompletion(int timeout)
		{
			return this._runComplete.WaitOne(timeout, false);
		}

		// Token: 0x06000B0E RID: 2830 RVA: 0x00025078 File Offset: 0x00023278
		public void StopRun(bool force)
		{
			if (this.IsTestRunning)
			{
				this.Context.ExecutionStatus = (force ? TestExecutionStatus.AbortRequested : TestExecutionStatus.StopRequested);
				this.Context.Dispatcher.CancelRun(force);
			}
		}

		// Token: 0x06000B0F RID: 2831 RVA: 0x000250BC File Offset: 0x000232BC
		private void StartRun(ITestListener listener)
		{
			this._savedOut = Console.Out;
			this._savedErr = Console.Error;
			Console.SetOut(new EventListenerTextWriter("Out", Console.Out));
			Console.SetError(new EventListenerTextWriter("Error", Console.Error));
			if (!Debugger.IsAttached && this.Settings.ContainsKey("DebugTests") && (bool)this.Settings["DebugTests"])
			{
				Debugger.Launch();
			}
			this.Context.Dispatcher.Dispatch(this.TopLevelWorkItem);
		}

		// Token: 0x06000B10 RID: 2832 RVA: 0x00025160 File Offset: 0x00023360
		private void CreateTestExecutionContext(ITestListener listener)
		{
			this.Context = new TestExecutionContext();
			if (this.Settings.ContainsKey("DefaultTimeout"))
			{
				this.Context.TestCaseTimeout = (int)this.Settings["DefaultTimeout"];
			}
			if (this.Settings.ContainsKey("StopOnError"))
			{
				this.Context.StopOnError = (bool)this.Settings["StopOnError"];
			}
			if (this.Settings.ContainsKey("WorkDirectory"))
			{
				this.Context.WorkDirectory = (string)this.Settings["WorkDirectory"];
			}
			else
			{
				this.Context.WorkDirectory = Env.DefaultWorkDirectory;
			}
			this.Context.Listener = listener;
			this.Context.Dispatcher = new SimpleWorkItemDispatcher();
		}

		// Token: 0x06000B11 RID: 2833 RVA: 0x00025253 File Offset: 0x00023453
		private void OnRunCompleted(object sender, EventArgs e)
		{
			Console.SetOut(this._savedOut);
			Console.SetError(this._savedErr);
			this._runComplete.Set();
		}

		// Token: 0x06000B12 RID: 2834 RVA: 0x0002527C File Offset: 0x0002347C
		private int CountTestCases(ITest test, ITestFilter filter)
		{
			int result;
			if (!test.IsSuite)
			{
				result = 1;
			}
			else
			{
				int num = 0;
				foreach (ITest test2 in test.Tests)
				{
					if (filter.Pass(test2))
					{
						num += this.CountTestCases(test2, filter);
					}
				}
				result = num;
			}
			return result;
		}

		// Token: 0x06000B13 RID: 2835 RVA: 0x00025300 File Offset: 0x00023500
		// Note: this type is marked as 'beforefieldinit'.
		static NUnitTestAssemblyRunner()
		{
		}

		// Token: 0x040002A1 RID: 673
		private static Logger log = InternalTrace.GetLogger("DefaultTestAssemblyRunner");

		// Token: 0x040002A2 RID: 674
		private ITestAssemblyBuilder _builder;

		// Token: 0x040002A3 RID: 675
		private ManualResetEvent _runComplete = new ManualResetEvent(false);

		// Token: 0x040002A4 RID: 676
		private TextWriter _savedOut;

		// Token: 0x040002A5 RID: 677
		private TextWriter _savedErr;

		// Token: 0x040002A6 RID: 678
		[CompilerGenerated]
		private ITest <LoadedTest>k__BackingField;

		// Token: 0x040002A7 RID: 679
		[CompilerGenerated]
		private IDictionary<string, object> <Settings>k__BackingField;

		// Token: 0x040002A8 RID: 680
		[CompilerGenerated]
		private WorkItem <TopLevelWorkItem>k__BackingField;

		// Token: 0x040002A9 RID: 681
		[CompilerGenerated]
		private TestExecutionContext <Context>k__BackingField;
	}
}
