﻿using System;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Api
{
	// Token: 0x0200019F RID: 415
	public interface ITestAssemblyRunner
	{
		// Token: 0x1700026A RID: 618
		// (get) Token: 0x06000ABB RID: 2747
		ITest LoadedTest { get; }

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x06000ABC RID: 2748
		ITestResult Result { get; }

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x06000ABD RID: 2749
		bool IsTestLoaded { get; }

		// Token: 0x1700026D RID: 621
		// (get) Token: 0x06000ABE RID: 2750
		bool IsTestRunning { get; }

		// Token: 0x1700026E RID: 622
		// (get) Token: 0x06000ABF RID: 2751
		bool IsTestComplete { get; }

		// Token: 0x06000AC0 RID: 2752
		ITest Load(string assemblyName, IDictionary<string, object> settings);

		// Token: 0x06000AC1 RID: 2753
		ITest Load(Assembly assembly, IDictionary<string, object> settings);

		// Token: 0x06000AC2 RID: 2754
		int CountTestCases(ITestFilter filter);

		// Token: 0x06000AC3 RID: 2755
		ITestResult Run(ITestListener listener, ITestFilter filter);

		// Token: 0x06000AC4 RID: 2756
		void RunAsync(ITestListener listener, ITestFilter filter);

		// Token: 0x06000AC5 RID: 2757
		bool WaitForCompletion(int timeout);

		// Token: 0x06000AC6 RID: 2758
		void StopRun(bool force);
	}
}
