﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Web.UI;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework.Api
{
	// Token: 0x02000088 RID: 136
	public class FrameworkController : LongLivedMarshalByRefObject
	{
		// Token: 0x06000485 RID: 1157 RVA: 0x0000EF87 File Offset: 0x0000D187
		public FrameworkController(string assemblyNameOrPath, string idPrefix, IDictionary settings)
		{
			this.Initialize(assemblyNameOrPath, settings);
			this.Builder = new DefaultTestAssemblyBuilder();
			this.Runner = new NUnitTestAssemblyRunner(this.Builder);
			Test.IdPrefix = idPrefix;
		}

		// Token: 0x06000486 RID: 1158 RVA: 0x0000EFC0 File Offset: 0x0000D1C0
		public FrameworkController(Assembly assembly, string idPrefix, IDictionary settings) : this(assembly.FullName, idPrefix, settings)
		{
			this._testAssembly = assembly;
		}

		// Token: 0x06000487 RID: 1159 RVA: 0x0000EFDC File Offset: 0x0000D1DC
		public FrameworkController(string assemblyNameOrPath, string idPrefix, IDictionary settings, string runnerType, string builderType)
		{
			this.Initialize(assemblyNameOrPath, settings);
			this.Builder = (ITestAssemblyBuilder)Reflect.Construct(Type.GetType(builderType));
			this.Runner = (ITestAssemblyRunner)Reflect.Construct(Type.GetType(runnerType), new object[]
			{
				this.Builder
			});
			Test.IdPrefix = (idPrefix ?? "");
		}

		// Token: 0x06000488 RID: 1160 RVA: 0x0000F04C File Offset: 0x0000D24C
		public FrameworkController(Assembly assembly, string idPrefix, IDictionary settings, string runnerType, string builderType) : this(assembly.FullName, idPrefix, settings, runnerType, builderType)
		{
			this._testAssembly = assembly;
		}

		// Token: 0x06000489 RID: 1161 RVA: 0x0000F0A4 File Offset: 0x0000D2A4
		[SecuritySafeCritical]
		private void Initialize(string assemblyPath, IDictionary settings)
		{
			this.AssemblyNameOrPath = assemblyPath;
			IDictionary<string, object> dictionary = settings as IDictionary<string, object>;
			IDictionary<string, object> settings2;
			if ((settings2 = dictionary) == null)
			{
				settings2 = settings.Cast<DictionaryEntry>().ToDictionary((DictionaryEntry de) => (string)de.Key, (DictionaryEntry de) => de.Value);
			}
			this.Settings = settings2;
			if (this.Settings.ContainsKey("InternalTraceLevel"))
			{
				InternalTraceLevel level = (InternalTraceLevel)Enum.Parse(typeof(InternalTraceLevel), (string)this.Settings["InternalTraceLevel"], true);
				if (this.Settings.ContainsKey("InternalTraceWriter"))
				{
					InternalTrace.Initialize((TextWriter)this.Settings["InternalTraceWriter"], level);
				}
				else
				{
					string path = this.Settings.ContainsKey("WorkDirectory") ? ((string)this.Settings["WorkDirectory"]) : Env.DefaultWorkDirectory;
					string path2 = string.Format("InternalTrace.{0}.{1}.log", Process.GetCurrentProcess().Id, Path.GetFileName(assemblyPath));
					InternalTrace.Initialize(Path.Combine(path, path2), level);
				}
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x0600048A RID: 1162 RVA: 0x0000F1F8 File Offset: 0x0000D3F8
		// (set) Token: 0x0600048B RID: 1163 RVA: 0x0000F20F File Offset: 0x0000D40F
		public ITestAssemblyBuilder Builder
		{
			[CompilerGenerated]
			get
			{
				return this.<Builder>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Builder>k__BackingField = value;
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x0600048C RID: 1164 RVA: 0x0000F218 File Offset: 0x0000D418
		// (set) Token: 0x0600048D RID: 1165 RVA: 0x0000F22F File Offset: 0x0000D42F
		public ITestAssemblyRunner Runner
		{
			[CompilerGenerated]
			get
			{
				return this.<Runner>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Runner>k__BackingField = value;
			}
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x0600048E RID: 1166 RVA: 0x0000F238 File Offset: 0x0000D438
		// (set) Token: 0x0600048F RID: 1167 RVA: 0x0000F24F File Offset: 0x0000D44F
		public string AssemblyNameOrPath
		{
			[CompilerGenerated]
			get
			{
				return this.<AssemblyNameOrPath>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<AssemblyNameOrPath>k__BackingField = value;
			}
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000490 RID: 1168 RVA: 0x0000F258 File Offset: 0x0000D458
		// (set) Token: 0x06000491 RID: 1169 RVA: 0x0000F26F File Offset: 0x0000D46F
		public Assembly Assembly
		{
			[CompilerGenerated]
			get
			{
				return this.<Assembly>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Assembly>k__BackingField = value;
			}
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000492 RID: 1170 RVA: 0x0000F278 File Offset: 0x0000D478
		// (set) Token: 0x06000493 RID: 1171 RVA: 0x0000F28F File Offset: 0x0000D48F
		internal IDictionary<string, object> Settings
		{
			[CompilerGenerated]
			get
			{
				return this.<Settings>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Settings>k__BackingField = value;
			}
		}

		// Token: 0x06000494 RID: 1172 RVA: 0x0000F298 File Offset: 0x0000D498
		public string LoadTests()
		{
			if (this._testAssembly != null)
			{
				this.Runner.Load(this._testAssembly, this.Settings);
			}
			else
			{
				this.Runner.Load(this.AssemblyNameOrPath, this.Settings);
			}
			return this.Runner.LoadedTest.ToXml(false).OuterXml;
		}

		// Token: 0x06000495 RID: 1173 RVA: 0x0000F300 File Offset: 0x0000D500
		public string ExploreTests(string filter)
		{
			Guard.ArgumentNotNull(filter, "filter");
			if (this.Runner.LoadedTest == null)
			{
				throw new InvalidOperationException("The Explore method was called but no test has been loaded");
			}
			return this.Runner.LoadedTest.ToXml(true).OuterXml;
		}

		// Token: 0x06000496 RID: 1174 RVA: 0x0000F354 File Offset: 0x0000D554
		public string RunTests(string filter)
		{
			Guard.ArgumentNotNull(filter, "filter");
			TNode tnode = this.Runner.Run(new TestProgressReporter(null), TestFilter.FromXml(filter)).ToXml(true);
			if (this.Settings != null)
			{
				FrameworkController.InsertSettingsElement(tnode, this.Settings);
			}
			FrameworkController.InsertEnvironmentElement(tnode);
			TestExecutionContext.ClearCurrentContext();
			return tnode.OuterXml;
		}

		// Token: 0x06000497 RID: 1175 RVA: 0x0000F3C0 File Offset: 0x0000D5C0
		public string RunTests(Action<string> callback, string filter)
		{
			Guard.ArgumentNotNull(filter, "filter");
			FrameworkController.ActionCallback handler = new FrameworkController.ActionCallback(callback);
			TNode tnode = this.Runner.Run(new TestProgressReporter(handler), TestFilter.FromXml(filter)).ToXml(true);
			if (this.Settings != null)
			{
				FrameworkController.InsertSettingsElement(tnode, this.Settings);
			}
			FrameworkController.InsertEnvironmentElement(tnode);
			TestExecutionContext.ClearCurrentContext();
			return tnode.OuterXml;
		}

		// Token: 0x06000498 RID: 1176 RVA: 0x0000F430 File Offset: 0x0000D630
		private void RunAsync(Action<string> callback, string filter)
		{
			Guard.ArgumentNotNull(filter, "filter");
			FrameworkController.ActionCallback handler = new FrameworkController.ActionCallback(callback);
			this.Runner.RunAsync(new TestProgressReporter(handler), TestFilter.FromXml(filter));
		}

		// Token: 0x06000499 RID: 1177 RVA: 0x0000F469 File Offset: 0x0000D669
		public void StopRun(bool force)
		{
			this.Runner.StopRun(force);
		}

		// Token: 0x0600049A RID: 1178 RVA: 0x0000F47C File Offset: 0x0000D67C
		public int CountTests(string filter)
		{
			Guard.ArgumentNotNull(filter, "filter");
			return this.Runner.CountTestCases(TestFilter.FromXml(filter));
		}

		// Token: 0x0600049B RID: 1179 RVA: 0x0000F4AB File Offset: 0x0000D6AB
		private void LoadTests(ICallbackEventHandler handler)
		{
			handler.RaiseCallbackEvent(this.LoadTests());
		}

		// Token: 0x0600049C RID: 1180 RVA: 0x0000F4BC File Offset: 0x0000D6BC
		private void ExploreTests(ICallbackEventHandler handler, string filter)
		{
			Guard.ArgumentNotNull(filter, "filter");
			if (this.Runner.LoadedTest == null)
			{
				throw new InvalidOperationException("The Explore method was called but no test has been loaded");
			}
			handler.RaiseCallbackEvent(this.Runner.LoadedTest.ToXml(true).OuterXml);
		}

		// Token: 0x0600049D RID: 1181 RVA: 0x0000F514 File Offset: 0x0000D714
		private void RunTests(ICallbackEventHandler handler, string filter)
		{
			Guard.ArgumentNotNull(filter, "filter");
			TNode tnode = this.Runner.Run(new TestProgressReporter(handler), TestFilter.FromXml(filter)).ToXml(true);
			if (this.Settings != null)
			{
				FrameworkController.InsertSettingsElement(tnode, this.Settings);
			}
			FrameworkController.InsertEnvironmentElement(tnode);
			TestExecutionContext.ClearCurrentContext();
			handler.RaiseCallbackEvent(tnode.OuterXml);
		}

		// Token: 0x0600049E RID: 1182 RVA: 0x0000F580 File Offset: 0x0000D780
		private void RunAsync(ICallbackEventHandler handler, string filter)
		{
			Guard.ArgumentNotNull(filter, "filter");
			this.Runner.RunAsync(new TestProgressReporter(handler), TestFilter.FromXml(filter));
		}

		// Token: 0x0600049F RID: 1183 RVA: 0x0000F5A7 File Offset: 0x0000D7A7
		private void StopRun(ICallbackEventHandler handler, bool force)
		{
			this.StopRun(force);
		}

		// Token: 0x060004A0 RID: 1184 RVA: 0x0000F5B4 File Offset: 0x0000D7B4
		private void CountTests(ICallbackEventHandler handler, string filter)
		{
			handler.RaiseCallbackEvent(this.CountTests(filter).ToString());
		}

		// Token: 0x060004A1 RID: 1185 RVA: 0x0000F5D8 File Offset: 0x0000D7D8
		public static TNode InsertEnvironmentElement(TNode targetNode)
		{
			TNode tnode = new TNode("environment");
			targetNode.ChildNodes.Insert(0, tnode);
			tnode.AddAttribute("framework-version", Assembly.GetExecutingAssembly().GetName().Version.ToString());
			tnode.AddAttribute("clr-version", Environment.Version.ToString());
			tnode.AddAttribute("os-version", Environment.OSVersion.ToString());
			tnode.AddAttribute("platform", Environment.OSVersion.Platform.ToString());
			tnode.AddAttribute("cwd", Environment.CurrentDirectory);
			tnode.AddAttribute("machine-name", Environment.MachineName);
			tnode.AddAttribute("user", Environment.UserName);
			tnode.AddAttribute("user-domain", Environment.UserDomainName);
			tnode.AddAttribute("culture", CultureInfo.CurrentCulture.ToString());
			tnode.AddAttribute("uiculture", CultureInfo.CurrentUICulture.ToString());
			tnode.AddAttribute("os-architecture", FrameworkController.GetProcessorArchitecture());
			return tnode;
		}

		// Token: 0x060004A2 RID: 1186 RVA: 0x0000F6F4 File Offset: 0x0000D8F4
		private static string GetProcessorArchitecture()
		{
			return (IntPtr.Size == 8) ? "x64" : "x86";
		}

		// Token: 0x060004A3 RID: 1187 RVA: 0x0000F71C File Offset: 0x0000D91C
		public static TNode InsertSettingsElement(TNode targetNode, IDictionary<string, object> settings)
		{
			TNode tnode = new TNode("settings");
			targetNode.ChildNodes.Insert(0, tnode);
			foreach (string text in settings.Keys)
			{
				FrameworkController.AddSetting(tnode, text, settings[text]);
			}
			return tnode;
		}

		// Token: 0x060004A4 RID: 1188 RVA: 0x0000F7A0 File Offset: 0x0000D9A0
		private static void AddSetting(TNode settingsNode, string name, object value)
		{
			TNode tnode = new TNode("setting");
			tnode.AddAttribute("name", name);
			tnode.AddAttribute("value", value.ToString());
			settingsNode.ChildNodes.Add(tnode);
		}

		// Token: 0x060004A5 RID: 1189 RVA: 0x0000F06C File Offset: 0x0000D26C
		[CompilerGenerated]
		private static string <Initialize>b__0(DictionaryEntry de)
		{
			return (string)de.Key;
		}

		// Token: 0x060004A6 RID: 1190 RVA: 0x0000F08C File Offset: 0x0000D28C
		[CompilerGenerated]
		private static object <Initialize>b__1(DictionaryEntry de)
		{
			return de.Value;
		}

		// Token: 0x040000D5 RID: 213
		private const string LOG_FILE_FORMAT = "InternalTrace.{0}.{1}.log";

		// Token: 0x040000D6 RID: 214
		private Assembly _testAssembly;

		// Token: 0x040000D7 RID: 215
		[CompilerGenerated]
		private ITestAssemblyBuilder <Builder>k__BackingField;

		// Token: 0x040000D8 RID: 216
		[CompilerGenerated]
		private ITestAssemblyRunner <Runner>k__BackingField;

		// Token: 0x040000D9 RID: 217
		[CompilerGenerated]
		private string <AssemblyNameOrPath>k__BackingField;

		// Token: 0x040000DA RID: 218
		[CompilerGenerated]
		private Assembly <Assembly>k__BackingField;

		// Token: 0x040000DB RID: 219
		[CompilerGenerated]
		private IDictionary<string, object> <Settings>k__BackingField;

		// Token: 0x040000DC RID: 220
		[CompilerGenerated]
		private static Func<DictionaryEntry, string> CS$<>9__CachedAnonymousMethodDelegate2;

		// Token: 0x040000DD RID: 221
		[CompilerGenerated]
		private static Func<DictionaryEntry, object> CS$<>9__CachedAnonymousMethodDelegate3;

		// Token: 0x0200008A RID: 138
		private class ActionCallback : ICallbackEventHandler
		{
			// Token: 0x060004A9 RID: 1193 RVA: 0x0000F7E5 File Offset: 0x0000D9E5
			public ActionCallback(Action<string> callback)
			{
				this._callback = callback;
			}

			// Token: 0x060004AA RID: 1194 RVA: 0x0000F7F7 File Offset: 0x0000D9F7
			public string GetCallbackResult()
			{
				throw new NotImplementedException();
			}

			// Token: 0x060004AB RID: 1195 RVA: 0x0000F800 File Offset: 0x0000DA00
			public void RaiseCallbackEvent(string report)
			{
				if (this._callback != null)
				{
					this._callback(report);
				}
			}

			// Token: 0x040000DE RID: 222
			private Action<string> _callback;
		}

		// Token: 0x0200008B RID: 139
		public abstract class FrameworkControllerAction : LongLivedMarshalByRefObject
		{
			// Token: 0x060004AC RID: 1196 RVA: 0x0000F828 File Offset: 0x0000DA28
			protected FrameworkControllerAction()
			{
			}
		}

		// Token: 0x0200008C RID: 140
		public class LoadTestsAction : FrameworkController.FrameworkControllerAction
		{
			// Token: 0x060004AD RID: 1197 RVA: 0x0000F830 File Offset: 0x0000DA30
			public LoadTestsAction(FrameworkController controller, object handler)
			{
				controller.LoadTests((ICallbackEventHandler)handler);
			}
		}

		// Token: 0x0200008D RID: 141
		public class ExploreTestsAction : FrameworkController.FrameworkControllerAction
		{
			// Token: 0x060004AE RID: 1198 RVA: 0x0000F848 File Offset: 0x0000DA48
			public ExploreTestsAction(FrameworkController controller, string filter, object handler)
			{
				controller.ExploreTests((ICallbackEventHandler)handler, filter);
			}
		}

		// Token: 0x0200008E RID: 142
		public class CountTestsAction : FrameworkController.FrameworkControllerAction
		{
			// Token: 0x060004AF RID: 1199 RVA: 0x0000F861 File Offset: 0x0000DA61
			public CountTestsAction(FrameworkController controller, string filter, object handler)
			{
				controller.CountTests((ICallbackEventHandler)handler, filter);
			}
		}

		// Token: 0x0200008F RID: 143
		public class RunTestsAction : FrameworkController.FrameworkControllerAction
		{
			// Token: 0x060004B0 RID: 1200 RVA: 0x0000F87A File Offset: 0x0000DA7A
			public RunTestsAction(FrameworkController controller, string filter, object handler)
			{
				controller.RunTests((ICallbackEventHandler)handler, filter);
			}
		}

		// Token: 0x02000090 RID: 144
		public class RunAsyncAction : FrameworkController.FrameworkControllerAction
		{
			// Token: 0x060004B1 RID: 1201 RVA: 0x0000F893 File Offset: 0x0000DA93
			public RunAsyncAction(FrameworkController controller, string filter, object handler)
			{
				controller.RunAsync((ICallbackEventHandler)handler, filter);
			}
		}

		// Token: 0x02000091 RID: 145
		public class StopRunAction : FrameworkController.FrameworkControllerAction
		{
			// Token: 0x060004B2 RID: 1202 RVA: 0x0000F8AC File Offset: 0x0000DAAC
			public StopRunAction(FrameworkController controller, bool force, object handler)
			{
				controller.StopRun((ICallbackEventHandler)handler, force);
			}
		}
	}
}
