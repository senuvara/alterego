﻿using System;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Api
{
	// Token: 0x02000040 RID: 64
	public interface ITestAssemblyBuilder
	{
		// Token: 0x060001E2 RID: 482
		ITest Build(Assembly assembly, IDictionary<string, object> options);

		// Token: 0x060001E3 RID: 483
		ITest Build(string assemblyName, IDictionary<string, object> options);
	}
}
