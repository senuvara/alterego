﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Security;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework.Api
{
	// Token: 0x020000CE RID: 206
	public class DefaultTestAssemblyBuilder : ITestAssemblyBuilder
	{
		// Token: 0x06000640 RID: 1600 RVA: 0x0001611B File Offset: 0x0001431B
		public DefaultTestAssemblyBuilder()
		{
			this._defaultSuiteBuilder = new DefaultSuiteBuilder();
		}

		// Token: 0x06000641 RID: 1601 RVA: 0x00016134 File Offset: 0x00014334
		public ITest Build(Assembly assembly, IDictionary<string, object> options)
		{
			DefaultTestAssemblyBuilder.log.Debug("Loading {0} in AppDomain {1}", new object[]
			{
				assembly.FullName,
				AppDomain.CurrentDomain.FriendlyName
			});
			string assemblyPath = AssemblyHelper.GetAssemblyPath(assembly);
			return this.Build(assembly, assemblyPath, options);
		}

		// Token: 0x06000642 RID: 1602 RVA: 0x00016184 File Offset: 0x00014384
		public ITest Build(string assemblyName, IDictionary<string, object> options)
		{
			DefaultTestAssemblyBuilder.log.Debug("Loading {0} in AppDomain {1}", new object[]
			{
				assemblyName,
				AppDomain.CurrentDomain.FriendlyName
			});
			TestSuite testSuite = null;
			try
			{
				Assembly assembly = AssemblyHelper.Load(assemblyName);
				testSuite = this.Build(assembly, assemblyName, options);
			}
			catch (Exception ex)
			{
				testSuite = new TestAssembly(assemblyName);
				testSuite.RunState = RunState.NotRunnable;
				testSuite.Properties.Set("_SKIPREASON", ex.Message);
			}
			return testSuite;
		}

		// Token: 0x06000643 RID: 1603 RVA: 0x00016218 File Offset: 0x00014418
		private TestSuite Build(Assembly assembly, string assemblyPath, IDictionary<string, object> options)
		{
			TestSuite testSuite = null;
			try
			{
				if (options.ContainsKey("DefaultTestNamePattern"))
				{
					TestNameGenerator.DefaultTestNamePattern = (options["DefaultTestNamePattern"] as string);
				}
				if (options.ContainsKey("TestParameters"))
				{
					string text = options["TestParameters"] as string;
					if (!string.IsNullOrEmpty(text))
					{
						foreach (string text2 in text.Split(new char[]
						{
							';'
						}))
						{
							int num = text2.IndexOf("=");
							if (num > 0 && num < text2.Length - 1)
							{
								string name = text2.Substring(0, num);
								string value = text2.Substring(num + 1);
								TestContext.Parameters.Add(name, value);
							}
						}
					}
				}
				IList names = null;
				if (options.ContainsKey("LOAD"))
				{
					names = (options["LOAD"] as IList);
				}
				IList<Test> fixtures = this.GetFixtures(assembly, names);
				testSuite = this.BuildTestAssembly(assembly, assemblyPath, fixtures);
			}
			catch (Exception ex)
			{
				testSuite = new TestAssembly(assemblyPath);
				testSuite.RunState = RunState.NotRunnable;
				testSuite.Properties.Set("_SKIPREASON", ex.Message);
			}
			return testSuite;
		}

		// Token: 0x06000644 RID: 1604 RVA: 0x000163A8 File Offset: 0x000145A8
		private IList<Test> GetFixtures(Assembly assembly, IList names)
		{
			List<Test> list = new List<Test>();
			DefaultTestAssemblyBuilder.log.Debug("Examining assembly for test fixtures");
			IList<Type> candidateFixtureTypes = this.GetCandidateFixtureTypes(assembly, names);
			DefaultTestAssemblyBuilder.log.Debug("Found {0} classes to examine", new object[]
			{
				candidateFixtureTypes.Count
			});
			int num = 0;
			foreach (Type type in candidateFixtureTypes)
			{
				TypeWrapper typeInfo = new TypeWrapper(type);
				try
				{
					if (this._defaultSuiteBuilder.CanBuildFrom(typeInfo))
					{
						Test test = this._defaultSuiteBuilder.BuildFrom(typeInfo);
						list.Add(test);
						num += test.TestCaseCount;
					}
				}
				catch (Exception ex)
				{
					DefaultTestAssemblyBuilder.log.Error(ex.ToString());
				}
			}
			DefaultTestAssemblyBuilder.log.Debug("Found {0} fixtures with {1} test cases", new object[]
			{
				list.Count,
				num
			});
			return list;
		}

		// Token: 0x06000645 RID: 1605 RVA: 0x000164F0 File Offset: 0x000146F0
		private IList<Type> GetCandidateFixtureTypes(Assembly assembly, IList names)
		{
			Type[] types = assembly.GetTypes();
			IList<Type> result;
			if (names == null || names.Count == 0)
			{
				result = types;
			}
			else
			{
				List<Type> list = new List<Type>();
				foreach (object obj in names)
				{
					string text = (string)obj;
					Type type = assembly.GetType(text);
					if (type != null)
					{
						list.Add(type);
					}
					else
					{
						string value = text + ".";
						foreach (Type type2 in types)
						{
							if (type2.FullName.StartsWith(value))
							{
								list.Add(type2);
							}
						}
					}
				}
				result = list;
			}
			return result;
		}

		// Token: 0x06000646 RID: 1606 RVA: 0x000165FC File Offset: 0x000147FC
		[SecuritySafeCritical]
		private TestSuite BuildTestAssembly(Assembly assembly, string assemblyName, IList<Test> fixtures)
		{
			TestSuite testSuite = new TestAssembly(assembly, assemblyName);
			if (fixtures.Count == 0)
			{
				testSuite.RunState = RunState.NotRunnable;
				testSuite.Properties.Set("_SKIPREASON", "Has no TestFixtures");
			}
			else
			{
				NamespaceTreeBuilder namespaceTreeBuilder = new NamespaceTreeBuilder(testSuite);
				namespaceTreeBuilder.Add(fixtures);
				testSuite = namespaceTreeBuilder.RootSuite;
			}
			testSuite.ApplyAttributesToTest(assembly);
			testSuite.Properties.Set("_PID", Process.GetCurrentProcess().Id);
			testSuite.Properties.Set("_APPDOMAIN", AppDomain.CurrentDomain.FriendlyName);
			testSuite.Sort();
			return testSuite;
		}

		// Token: 0x06000647 RID: 1607 RVA: 0x000166AB File Offset: 0x000148AB
		// Note: this type is marked as 'beforefieldinit'.
		static DefaultTestAssemblyBuilder()
		{
		}

		// Token: 0x04000154 RID: 340
		private static Logger log = InternalTrace.GetLogger(typeof(DefaultTestAssemblyBuilder));

		// Token: 0x04000155 RID: 341
		private ISuiteBuilder _defaultSuiteBuilder;
	}
}
