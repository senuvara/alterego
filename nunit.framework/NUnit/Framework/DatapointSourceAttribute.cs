﻿using System;

namespace NUnit.Framework
{
	// Token: 0x020000D9 RID: 217
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class DatapointSourceAttribute : NUnitAttribute
	{
		// Token: 0x06000673 RID: 1651 RVA: 0x00016F00 File Offset: 0x00015100
		public DatapointSourceAttribute()
		{
		}
	}
}
