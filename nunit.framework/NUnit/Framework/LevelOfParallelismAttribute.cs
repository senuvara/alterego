﻿using System;

namespace NUnit.Framework
{
	// Token: 0x020000F8 RID: 248
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	public sealed class LevelOfParallelismAttribute : PropertyAttribute
	{
		// Token: 0x06000707 RID: 1799 RVA: 0x00019A44 File Offset: 0x00017C44
		public LevelOfParallelismAttribute(int level) : base(level)
		{
		}
	}
}
