﻿using System;
using NUnit.Framework.Constraints;

namespace NUnit.Framework
{
	// Token: 0x020000BB RID: 187
	public class Contains
	{
		// Token: 0x060005A6 RID: 1446 RVA: 0x000131B0 File Offset: 0x000113B0
		public static CollectionContainsConstraint Item(object expected)
		{
			return new CollectionContainsConstraint(expected);
		}

		// Token: 0x060005A7 RID: 1447 RVA: 0x000131C8 File Offset: 0x000113C8
		public static DictionaryContainsKeyConstraint Key(object expected)
		{
			return new DictionaryContainsKeyConstraint(expected);
		}

		// Token: 0x060005A8 RID: 1448 RVA: 0x000131E0 File Offset: 0x000113E0
		public static DictionaryContainsValueConstraint Value(object expected)
		{
			return new DictionaryContainsValueConstraint(expected);
		}

		// Token: 0x060005A9 RID: 1449 RVA: 0x000131F8 File Offset: 0x000113F8
		public static SubstringConstraint Substring(string expected)
		{
			return new SubstringConstraint(expected);
		}

		// Token: 0x060005AA RID: 1450 RVA: 0x00013210 File Offset: 0x00011410
		public Contains()
		{
		}
	}
}
