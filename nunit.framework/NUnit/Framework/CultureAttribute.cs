﻿using System;
using System.Globalization;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x02000038 RID: 56
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class CultureAttribute : IncludeExcludeAttribute, IApplyToTest
	{
		// Token: 0x060001B4 RID: 436 RVA: 0x0000682A File Offset: 0x00004A2A
		public CultureAttribute()
		{
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x0000684B File Offset: 0x00004A4B
		public CultureAttribute(string cultures) : base(cultures)
		{
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x00006870 File Offset: 0x00004A70
		public void ApplyToTest(Test test)
		{
			if (test.RunState != RunState.NotRunnable && !this.IsCultureSupported())
			{
				test.RunState = RunState.Skipped;
				test.Properties.Set("_SKIPREASON", base.Reason);
			}
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x000068B4 File Offset: 0x00004AB4
		private bool IsCultureSupported()
		{
			bool result;
			if (base.Include != null && !this.cultureDetector.IsCultureSupported(base.Include))
			{
				base.Reason = string.Format("Only supported under culture {0}", base.Include);
				result = false;
			}
			else if (base.Exclude != null && this.cultureDetector.IsCultureSupported(base.Exclude))
			{
				base.Reason = string.Format("Not supported under culture {0}", base.Exclude);
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x00006944 File Offset: 0x00004B44
		public bool IsCultureSupported(string culture)
		{
			culture = culture.Trim();
			if (culture.IndexOf(',') >= 0)
			{
				if (this.IsCultureSupported(culture.Split(new char[]
				{
					','
				})))
				{
					return true;
				}
			}
			else if (this.currentCulture.Name == culture || this.currentCulture.TwoLetterISOLanguageName == culture)
			{
				return true;
			}
			return false;
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x000069CC File Offset: 0x00004BCC
		public bool IsCultureSupported(string[] cultures)
		{
			foreach (string culture in cultures)
			{
				if (this.IsCultureSupported(culture))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x04000050 RID: 80
		private CultureDetector cultureDetector = new CultureDetector();

		// Token: 0x04000051 RID: 81
		private CultureInfo currentCulture = CultureInfo.CurrentCulture;
	}
}
