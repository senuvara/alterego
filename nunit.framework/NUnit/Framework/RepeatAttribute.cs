﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;

namespace NUnit.Framework
{
	// Token: 0x0200009C RID: 156
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class RepeatAttribute : PropertyAttribute, IWrapSetUpTearDown, ICommandWrapper
	{
		// Token: 0x060004D1 RID: 1233 RVA: 0x0000FEF6 File Offset: 0x0000E0F6
		public RepeatAttribute(int count) : base(count)
		{
			this._count = count;
		}

		// Token: 0x060004D2 RID: 1234 RVA: 0x0000FF10 File Offset: 0x0000E110
		public TestCommand Wrap(TestCommand command)
		{
			return new RepeatAttribute.RepeatedTestCommand(command, this._count);
		}

		// Token: 0x040000E4 RID: 228
		private int _count;

		// Token: 0x0200009D RID: 157
		public class RepeatedTestCommand : DelegatingTestCommand
		{
			// Token: 0x060004D3 RID: 1235 RVA: 0x0000FF2E File Offset: 0x0000E12E
			public RepeatedTestCommand(TestCommand innerCommand, int repeatCount) : base(innerCommand)
			{
				this.repeatCount = repeatCount;
			}

			// Token: 0x060004D4 RID: 1236 RVA: 0x0000FF44 File Offset: 0x0000E144
			public override TestResult Execute(ITestExecutionContext context)
			{
				int num = this.repeatCount;
				while (num-- > 0)
				{
					context.CurrentResult = this.innerCommand.Execute(context);
					if (context.CurrentResult.ResultState != ResultState.Success)
					{
						break;
					}
				}
				return context.CurrentResult;
			}

			// Token: 0x040000E5 RID: 229
			private int repeatCount;
		}
	}
}
