﻿using System;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework
{
	// Token: 0x020000D7 RID: 215
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class SequentialAttribute : CombiningStrategyAttribute
	{
		// Token: 0x06000668 RID: 1640 RVA: 0x00016B5B File Offset: 0x00014D5B
		public SequentialAttribute() : base(new SequentialStrategy(), new ParameterDataSourceProvider())
		{
		}
	}
}
