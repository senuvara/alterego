﻿using System;

namespace NUnit.Framework
{
	// Token: 0x0200013E RID: 318
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class DatapointsAttribute : DatapointSourceAttribute
	{
		// Token: 0x06000874 RID: 2164 RVA: 0x0001D347 File Offset: 0x0001B547
		public DatapointsAttribute()
		{
		}
	}
}
