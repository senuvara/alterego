﻿using System;

namespace NUnit.Framework
{
	// Token: 0x02000141 RID: 321
	[Flags]
	public enum ParallelScope
	{
		// Token: 0x040001DF RID: 479
		None = 0,
		// Token: 0x040001E0 RID: 480
		Self = 1,
		// Token: 0x040001E1 RID: 481
		Children = 2,
		// Token: 0x040001E2 RID: 482
		Fixtures = 4
	}
}
