﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework
{
	// Token: 0x02000112 RID: 274
	public interface ITestAction
	{
		// Token: 0x06000771 RID: 1905
		void BeforeTest(ITest test);

		// Token: 0x06000772 RID: 1906
		void AfterTest(ITest test);

		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x06000773 RID: 1907
		ActionTargets Targets { get; }
	}
}
