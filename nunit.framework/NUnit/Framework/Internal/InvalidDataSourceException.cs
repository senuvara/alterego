﻿using System;
using System.Runtime.Serialization;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000C2 RID: 194
	[Serializable]
	public class InvalidDataSourceException : Exception
	{
		// Token: 0x060005E7 RID: 1511 RVA: 0x00014305 File Offset: 0x00012505
		public InvalidDataSourceException()
		{
		}

		// Token: 0x060005E8 RID: 1512 RVA: 0x00014310 File Offset: 0x00012510
		public InvalidDataSourceException(string message) : base(message)
		{
		}

		// Token: 0x060005E9 RID: 1513 RVA: 0x0001431C File Offset: 0x0001251C
		public InvalidDataSourceException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x060005EA RID: 1514 RVA: 0x00014329 File Offset: 0x00012529
		protected InvalidDataSourceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
