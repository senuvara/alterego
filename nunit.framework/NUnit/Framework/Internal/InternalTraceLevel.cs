﻿using System;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000140 RID: 320
	public enum InternalTraceLevel
	{
		// Token: 0x040001D7 RID: 471
		Default,
		// Token: 0x040001D8 RID: 472
		Off,
		// Token: 0x040001D9 RID: 473
		Error,
		// Token: 0x040001DA RID: 474
		Warning,
		// Token: 0x040001DB RID: 475
		Info,
		// Token: 0x040001DC RID: 476
		Debug,
		// Token: 0x040001DD RID: 477
		Verbose = 5
	}
}
