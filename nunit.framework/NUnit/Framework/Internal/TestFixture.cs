﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000E9 RID: 233
	public class TestFixture : TestSuite, IDisposableFixture
	{
		// Token: 0x060006B1 RID: 1713 RVA: 0x00018470 File Offset: 0x00016670
		public TestFixture(ITypeInfo fixtureType) : base(fixtureType)
		{
			base.CheckSetUpTearDownMethods(typeof(OneTimeSetUpAttribute));
			base.CheckSetUpTearDownMethods(typeof(OneTimeTearDownAttribute));
			base.CheckSetUpTearDownMethods(typeof(SetUpAttribute));
			base.CheckSetUpTearDownMethods(typeof(TearDownAttribute));
		}
	}
}
