﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using NUnit.Framework.Constraints;
using NUnit.Framework.Internal.Execution;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000B7 RID: 183
	public interface ITestExecutionContext
	{
		// Token: 0x1700010A RID: 266
		// (get) Token: 0x06000543 RID: 1347
		// (set) Token: 0x06000544 RID: 1348
		Test CurrentTest { get; set; }

		// Token: 0x1700010B RID: 267
		// (get) Token: 0x06000545 RID: 1349
		// (set) Token: 0x06000546 RID: 1350
		DateTime StartTime { get; set; }

		// Token: 0x1700010C RID: 268
		// (get) Token: 0x06000547 RID: 1351
		// (set) Token: 0x06000548 RID: 1352
		long StartTicks { get; set; }

		// Token: 0x1700010D RID: 269
		// (get) Token: 0x06000549 RID: 1353
		// (set) Token: 0x0600054A RID: 1354
		TestResult CurrentResult { get; set; }

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x0600054B RID: 1355
		TextWriter OutWriter { get; }

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x0600054C RID: 1356
		// (set) Token: 0x0600054D RID: 1357
		object TestObject { get; set; }

		// Token: 0x17000110 RID: 272
		// (get) Token: 0x0600054E RID: 1358
		// (set) Token: 0x0600054F RID: 1359
		string WorkDirectory { get; set; }

		// Token: 0x17000111 RID: 273
		// (get) Token: 0x06000550 RID: 1360
		// (set) Token: 0x06000551 RID: 1361
		bool StopOnError { get; set; }

		// Token: 0x17000112 RID: 274
		// (get) Token: 0x06000552 RID: 1362
		// (set) Token: 0x06000553 RID: 1363
		TestExecutionStatus ExecutionStatus { get; set; }

		// Token: 0x17000113 RID: 275
		// (get) Token: 0x06000554 RID: 1364
		// (set) Token: 0x06000555 RID: 1365
		IWorkItemDispatcher Dispatcher { get; set; }

		// Token: 0x17000114 RID: 276
		// (get) Token: 0x06000556 RID: 1366
		// (set) Token: 0x06000557 RID: 1367
		ParallelScope ParallelScope { get; set; }

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x06000558 RID: 1368
		string WorkerId { get; }

		// Token: 0x17000116 RID: 278
		// (get) Token: 0x06000559 RID: 1369
		Randomizer RandomGenerator { get; }

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x0600055A RID: 1370
		// (set) Token: 0x0600055B RID: 1371
		int TestCaseTimeout { get; set; }

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x0600055C RID: 1372
		List<ITestAction> UpstreamActions { get; }

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x0600055D RID: 1373
		// (set) Token: 0x0600055E RID: 1374
		CultureInfo CurrentCulture { get; set; }

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x0600055F RID: 1375
		// (set) Token: 0x06000560 RID: 1376
		CultureInfo CurrentUICulture { get; set; }

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x06000561 RID: 1377
		ValueFormatter CurrentValueFormatter { get; }

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x06000562 RID: 1378
		// (set) Token: 0x06000563 RID: 1379
		bool IsSingleThreaded { get; set; }

		// Token: 0x06000564 RID: 1380
		void IncrementAssertCount();

		// Token: 0x06000565 RID: 1381
		void AddFormatter(ValueFormatterFactory formatterFactory);
	}
}
