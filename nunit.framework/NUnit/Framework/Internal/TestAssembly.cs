﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000144 RID: 324
	public class TestAssembly : TestSuite
	{
		// Token: 0x0600087C RID: 2172 RVA: 0x0001D484 File Offset: 0x0001B684
		public TestAssembly(Assembly assembly, string path) : base(path)
		{
			this.Assembly = assembly;
			base.Name = Path.GetFileName(path);
		}

		// Token: 0x0600087D RID: 2173 RVA: 0x0001D4A5 File Offset: 0x0001B6A5
		public TestAssembly(string path) : base(path)
		{
			base.Name = Path.GetFileName(path);
		}

		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x0600087E RID: 2174 RVA: 0x0001D4C0 File Offset: 0x0001B6C0
		// (set) Token: 0x0600087F RID: 2175 RVA: 0x0001D4D7 File Offset: 0x0001B6D7
		public Assembly Assembly
		{
			[CompilerGenerated]
			get
			{
				return this.<Assembly>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Assembly>k__BackingField = value;
			}
		}

		// Token: 0x170001DA RID: 474
		// (get) Token: 0x06000880 RID: 2176 RVA: 0x0001D4E0 File Offset: 0x0001B6E0
		public override string TestType
		{
			get
			{
				return "Assembly";
			}
		}

		// Token: 0x040001E4 RID: 484
		[CompilerGenerated]
		private Assembly <Assembly>k__BackingField;
	}
}
