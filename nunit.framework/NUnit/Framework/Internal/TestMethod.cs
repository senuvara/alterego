﻿using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000183 RID: 387
	public class TestMethod : Test
	{
		// Token: 0x06000A29 RID: 2601 RVA: 0x0002218C File Offset: 0x0002038C
		public TestMethod(IMethodInfo method) : base(method)
		{
		}

		// Token: 0x06000A2A RID: 2602 RVA: 0x00022198 File Offset: 0x00020398
		public TestMethod(IMethodInfo method, Test parentSuite) : base(method)
		{
			if (parentSuite != null)
			{
				base.FullName = parentSuite.FullName + "." + base.Name;
			}
		}

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x06000A2B RID: 2603 RVA: 0x000221D4 File Offset: 0x000203D4
		internal bool HasExpectedResult
		{
			get
			{
				return this.parms != null && this.parms.HasExpectedResult;
			}
		}

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x06000A2C RID: 2604 RVA: 0x000221FC File Offset: 0x000203FC
		internal object ExpectedResult
		{
			get
			{
				return (this.parms != null) ? this.parms.ExpectedResult : null;
			}
		}

		// Token: 0x17000259 RID: 601
		// (get) Token: 0x06000A2D RID: 2605 RVA: 0x00022224 File Offset: 0x00020424
		internal object[] Arguments
		{
			get
			{
				return (this.parms != null) ? this.parms.Arguments : null;
			}
		}

		// Token: 0x06000A2E RID: 2606 RVA: 0x0002224C File Offset: 0x0002044C
		public override TestResult MakeTestResult()
		{
			return new TestCaseResult(this);
		}

		// Token: 0x1700025A RID: 602
		// (get) Token: 0x06000A2F RID: 2607 RVA: 0x00022264 File Offset: 0x00020464
		public override bool HasChildren
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000A30 RID: 2608 RVA: 0x00022278 File Offset: 0x00020478
		public override TNode AddToXml(TNode parentNode, bool recursive)
		{
			TNode tnode = parentNode.AddElement(this.XmlElementName);
			base.PopulateTestNode(tnode, recursive);
			tnode.AddAttribute("seed", base.Seed.ToString());
			return tnode;
		}

		// Token: 0x1700025B RID: 603
		// (get) Token: 0x06000A31 RID: 2609 RVA: 0x000222BC File Offset: 0x000204BC
		public override IList<ITest> Tests
		{
			get
			{
				return new ITest[0];
			}
		}

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x06000A32 RID: 2610 RVA: 0x000222D4 File Offset: 0x000204D4
		public override string XmlElementName
		{
			get
			{
				return "test-case";
			}
		}

		// Token: 0x1700025D RID: 605
		// (get) Token: 0x06000A33 RID: 2611 RVA: 0x000222EC File Offset: 0x000204EC
		public override string MethodName
		{
			get
			{
				return base.Method.Name;
			}
		}

		// Token: 0x04000264 RID: 612
		public TestCaseParameters parms;
	}
}
