﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000153 RID: 339
	public class ParameterizedFixtureSuite : TestSuite
	{
		// Token: 0x060008F2 RID: 2290 RVA: 0x0001E55E File Offset: 0x0001C75E
		public ParameterizedFixtureSuite(ITypeInfo typeInfo) : base(typeInfo.Namespace, typeInfo.GetDisplayName())
		{
			this._genericFixture = typeInfo.ContainsGenericParameters;
		}

		// Token: 0x17000201 RID: 513
		// (get) Token: 0x060008F3 RID: 2291 RVA: 0x0001E584 File Offset: 0x0001C784
		public override string TestType
		{
			get
			{
				return this._genericFixture ? "GenericFixture" : "ParameterizedFixture";
			}
		}

		// Token: 0x040001FB RID: 507
		private bool _genericFixture;
	}
}
