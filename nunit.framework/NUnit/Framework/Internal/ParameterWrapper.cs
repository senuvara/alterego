﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000DD RID: 221
	public class ParameterWrapper : IParameterInfo, IReflectionInfo
	{
		// Token: 0x06000681 RID: 1665 RVA: 0x000172C4 File Offset: 0x000154C4
		public ParameterWrapper(IMethodInfo method, ParameterInfo parameterInfo)
		{
			this.Method = method;
			this.ParameterInfo = parameterInfo;
		}

		// Token: 0x17000173 RID: 371
		// (get) Token: 0x06000682 RID: 1666 RVA: 0x000172E0 File Offset: 0x000154E0
		public bool IsOptional
		{
			get
			{
				return this.ParameterInfo.IsOptional;
			}
		}

		// Token: 0x17000174 RID: 372
		// (get) Token: 0x06000683 RID: 1667 RVA: 0x00017300 File Offset: 0x00015500
		// (set) Token: 0x06000684 RID: 1668 RVA: 0x00017317 File Offset: 0x00015517
		public IMethodInfo Method
		{
			[CompilerGenerated]
			get
			{
				return this.<Method>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Method>k__BackingField = value;
			}
		}

		// Token: 0x17000175 RID: 373
		// (get) Token: 0x06000685 RID: 1669 RVA: 0x00017320 File Offset: 0x00015520
		// (set) Token: 0x06000686 RID: 1670 RVA: 0x00017337 File Offset: 0x00015537
		public ParameterInfo ParameterInfo
		{
			[CompilerGenerated]
			get
			{
				return this.<ParameterInfo>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ParameterInfo>k__BackingField = value;
			}
		}

		// Token: 0x17000176 RID: 374
		// (get) Token: 0x06000687 RID: 1671 RVA: 0x00017340 File Offset: 0x00015540
		public Type ParameterType
		{
			get
			{
				return this.ParameterInfo.ParameterType;
			}
		}

		// Token: 0x06000688 RID: 1672 RVA: 0x00017360 File Offset: 0x00015560
		public T[] GetCustomAttributes<T>(bool inherit) where T : class
		{
			return (T[])this.ParameterInfo.GetCustomAttributes(typeof(T), inherit);
		}

		// Token: 0x06000689 RID: 1673 RVA: 0x00017390 File Offset: 0x00015590
		public bool IsDefined<T>(bool inherit)
		{
			return this.ParameterInfo.IsDefined(typeof(T), inherit);
		}

		// Token: 0x0400015B RID: 347
		[CompilerGenerated]
		private IMethodInfo <Method>k__BackingField;

		// Token: 0x0400015C RID: 348
		[CompilerGenerated]
		private ParameterInfo <ParameterInfo>k__BackingField;
	}
}
