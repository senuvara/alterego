﻿using System;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000061 RID: 97
	public interface ILogger
	{
		// Token: 0x060002F4 RID: 756
		void Error(string message);

		// Token: 0x060002F5 RID: 757
		void Error(string message, params object[] args);

		// Token: 0x060002F6 RID: 758
		void Warning(string message);

		// Token: 0x060002F7 RID: 759
		void Warning(string message, params object[] args);

		// Token: 0x060002F8 RID: 760
		void Info(string message);

		// Token: 0x060002F9 RID: 761
		void Info(string message, params object[] args);

		// Token: 0x060002FA RID: 762
		void Debug(string message);

		// Token: 0x060002FB RID: 763
		void Debug(string message, params object[] args);
	}
}
