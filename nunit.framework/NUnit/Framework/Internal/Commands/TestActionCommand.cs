﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x02000060 RID: 96
	public class TestActionCommand : DelegatingTestCommand
	{
		// Token: 0x060002F2 RID: 754 RVA: 0x0000B276 File Offset: 0x00009476
		public TestActionCommand(TestCommand innerCommand) : base(innerCommand)
		{
			Guard.ArgumentValid(innerCommand.Test is TestMethod, "TestActionCommand may only apply to a TestMethod", "innerCommand");
		}

		// Token: 0x060002F3 RID: 755 RVA: 0x0000B2AC File Offset: 0x000094AC
		public override TestResult Execute(ITestExecutionContext context)
		{
			if (base.Test.Fixture == null)
			{
				base.Test.Fixture = context.TestObject;
			}
			foreach (ITestAction testAction in context.UpstreamActions)
			{
				Debug.Assert(testAction.Targets == ActionTargets.Default || (testAction.Targets & ActionTargets.Test) == ActionTargets.Test, "Invalid target on upstream action: " + testAction.Targets.ToString());
				this._actions.Add(new TestActionItem(testAction));
			}
			foreach (ITestAction testAction in ActionsHelper.GetActionsFromAttributeProvider(((TestMethod)base.Test).Method.MethodInfo))
			{
				if (testAction.Targets == ActionTargets.Default || (testAction.Targets & ActionTargets.Test) == ActionTargets.Test)
				{
					this._actions.Add(new TestActionItem(testAction));
				}
			}
			try
			{
				for (int j = 0; j < this._actions.Count; j++)
				{
					this._actions[j].BeforeTest(base.Test);
				}
				context.CurrentResult = this.innerCommand.Execute(context);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException)
				{
					Thread.ResetAbort();
				}
				context.CurrentResult.RecordException(ex);
			}
			finally
			{
				if (context.ExecutionStatus != TestExecutionStatus.AbortRequested)
				{
					for (int j = this._actions.Count - 1; j >= 0; j--)
					{
						this._actions[j].AfterTest(base.Test);
					}
				}
			}
			return context.CurrentResult;
		}

		// Token: 0x040000A1 RID: 161
		private IList<TestActionItem> _actions = new List<TestActionItem>();
	}
}
