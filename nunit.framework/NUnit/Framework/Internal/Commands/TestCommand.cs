﻿using System;
using System.Runtime.CompilerServices;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x0200003C RID: 60
	public abstract class TestCommand
	{
		// Token: 0x060001D7 RID: 471 RVA: 0x000072AE File Offset: 0x000054AE
		public TestCommand(Test test)
		{
			this.Test = test;
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x060001D8 RID: 472 RVA: 0x000072C4 File Offset: 0x000054C4
		// (set) Token: 0x060001D9 RID: 473 RVA: 0x000072DB File Offset: 0x000054DB
		public Test Test
		{
			[CompilerGenerated]
			get
			{
				return this.<Test>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Test>k__BackingField = value;
			}
		}

		// Token: 0x060001DA RID: 474
		public abstract TestResult Execute(ITestExecutionContext context);

		// Token: 0x04000067 RID: 103
		[CompilerGenerated]
		private Test <Test>k__BackingField;
	}
}
