﻿using System;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x0200003D RID: 61
	public abstract class DelegatingTestCommand : TestCommand
	{
		// Token: 0x060001DB RID: 475 RVA: 0x000072E4 File Offset: 0x000054E4
		public TestCommand GetInnerCommand()
		{
			return this.innerCommand;
		}

		// Token: 0x060001DC RID: 476 RVA: 0x000072FC File Offset: 0x000054FC
		protected DelegatingTestCommand(TestCommand innerCommand) : base(innerCommand.Test)
		{
			this.innerCommand = innerCommand;
		}

		// Token: 0x04000068 RID: 104
		protected TestCommand innerCommand;
	}
}
