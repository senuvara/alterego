﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x0200009F RID: 159
	public class TheoryResultCommand : DelegatingTestCommand
	{
		// Token: 0x060004DA RID: 1242 RVA: 0x0001007F File Offset: 0x0000E27F
		public TheoryResultCommand(TestCommand command) : base(command)
		{
		}

		// Token: 0x060004DB RID: 1243 RVA: 0x0001008C File Offset: 0x0000E28C
		public override TestResult Execute(ITestExecutionContext context)
		{
			TestResult testResult = this.innerCommand.Execute(context);
			if (testResult.ResultState == ResultState.Success)
			{
				if (!testResult.HasChildren)
				{
					testResult.SetResult(ResultState.Failure, "No test cases were provided");
				}
				else
				{
					bool flag = true;
					foreach (ITestResult testResult2 in testResult.Children)
					{
						TestResult testResult3 = (TestResult)testResult2;
						if (testResult3.ResultState == ResultState.Success)
						{
							flag = false;
							break;
						}
					}
					if (flag)
					{
						testResult.SetResult(ResultState.Failure, "All test cases were inconclusive");
					}
				}
			}
			return testResult;
		}
	}
}
