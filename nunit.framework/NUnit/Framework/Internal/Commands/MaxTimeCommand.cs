﻿using System;
using System.Diagnostics;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x0200003E RID: 62
	public class MaxTimeCommand : DelegatingTestCommand
	{
		// Token: 0x060001DD RID: 477 RVA: 0x00007314 File Offset: 0x00005514
		public MaxTimeCommand(TestCommand innerCommand, int maxTime) : base(innerCommand)
		{
			this.maxTime = maxTime;
		}

		// Token: 0x060001DE RID: 478 RVA: 0x00007328 File Offset: 0x00005528
		public override TestResult Execute(ITestExecutionContext context)
		{
			long timestamp = Stopwatch.GetTimestamp();
			TestResult testResult = this.innerCommand.Execute(context);
			long num = Stopwatch.GetTimestamp() - timestamp;
			double duration = (double)num / (double)Stopwatch.Frequency;
			testResult.Duration = duration;
			if (testResult.ResultState == ResultState.Success)
			{
				double num2 = testResult.Duration * 1000.0;
				if (num2 > (double)this.maxTime)
				{
					testResult.SetResult(ResultState.Failure, string.Format("Elapsed time of {0}ms exceeds maximum of {1}ms", num2, this.maxTime));
				}
			}
			return testResult;
		}

		// Token: 0x04000069 RID: 105
		private int maxTime;
	}
}
