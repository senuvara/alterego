﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x02000122 RID: 290
	public class SkipCommand : TestCommand
	{
		// Token: 0x060007CB RID: 1995 RVA: 0x0001B98D File Offset: 0x00019B8D
		public SkipCommand(Test test) : base(test)
		{
		}

		// Token: 0x060007CC RID: 1996 RVA: 0x0001B99C File Offset: 0x00019B9C
		public override TestResult Execute(ITestExecutionContext context)
		{
			TestResult currentResult = context.CurrentResult;
			switch (base.Test.RunState)
			{
			case RunState.NotRunnable:
				currentResult.SetResult(ResultState.NotRunnable, this.GetSkipReason(), this.GetProviderStackTrace());
				break;
			default:
				currentResult.SetResult(ResultState.Skipped, this.GetSkipReason());
				break;
			case RunState.Explicit:
				currentResult.SetResult(ResultState.Explicit, this.GetSkipReason());
				break;
			case RunState.Ignored:
				currentResult.SetResult(ResultState.Ignored, this.GetSkipReason());
				break;
			}
			return currentResult;
		}

		// Token: 0x060007CD RID: 1997 RVA: 0x0001BA34 File Offset: 0x00019C34
		private string GetSkipReason()
		{
			return (string)base.Test.Properties.Get("_SKIPREASON");
		}

		// Token: 0x060007CE RID: 1998 RVA: 0x0001BA60 File Offset: 0x00019C60
		private string GetProviderStackTrace()
		{
			return (string)base.Test.Properties.Get("_PROVIDERSTACKTRACE");
		}
	}
}
