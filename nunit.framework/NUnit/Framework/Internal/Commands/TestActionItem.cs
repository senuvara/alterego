﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x0200013F RID: 319
	public class TestActionItem
	{
		// Token: 0x06000875 RID: 2165 RVA: 0x0001D34F File Offset: 0x0001B54F
		public TestActionItem(ITestAction action)
		{
			this._action = action;
		}

		// Token: 0x06000876 RID: 2166 RVA: 0x0001D361 File Offset: 0x0001B561
		public void BeforeTest(ITest test)
		{
			this._beforeTestWasRun = true;
			this._action.BeforeTest(test);
		}

		// Token: 0x06000877 RID: 2167 RVA: 0x0001D378 File Offset: 0x0001B578
		public void AfterTest(ITest test)
		{
			if (this._beforeTestWasRun)
			{
				this._action.AfterTest(test);
			}
		}

		// Token: 0x040001D4 RID: 468
		private readonly ITestAction _action;

		// Token: 0x040001D5 RID: 469
		private bool _beforeTestWasRun;
	}
}
