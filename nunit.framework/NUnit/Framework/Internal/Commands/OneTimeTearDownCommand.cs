﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x0200010D RID: 269
	public class OneTimeTearDownCommand : TestCommand
	{
		// Token: 0x06000760 RID: 1888 RVA: 0x0001A7A0 File Offset: 0x000189A0
		public OneTimeTearDownCommand(TestSuite suite, List<SetUpTearDownItem> setUpTearDownItems, List<TestActionItem> actions) : base(suite)
		{
			this._setUpTearDownItems = setUpTearDownItems;
			this._actions = actions;
		}

		// Token: 0x06000761 RID: 1889 RVA: 0x0001A7E4 File Offset: 0x000189E4
		public override TestResult Execute(ITestExecutionContext context)
		{
			TestResult currentResult = context.CurrentResult;
			try
			{
				int i = this._actions.Count;
				while (i > 0)
				{
					this._actions[--i].AfterTest(base.Test);
				}
				if (this._setUpTearDownItems != null)
				{
					foreach (SetUpTearDownItem setUpTearDownItem in this._setUpTearDownItems)
					{
						setUpTearDownItem.RunTearDown(context);
					}
				}
				IDisposable disposable = context.TestObject as IDisposable;
				if (disposable != null && base.Test is IDisposableFixture)
				{
					if (Reflect.MethodCallWrapper != null)
					{
						Reflect.MethodCallWrapper(delegate
						{
							disposable.Dispose();
							return null;
						});
					}
					else
					{
						disposable.Dispose();
					}
				}
			}
			catch (Exception ex)
			{
				currentResult.RecordTearDownException(ex);
			}
			return currentResult;
		}

		// Token: 0x040001A6 RID: 422
		private List<SetUpTearDownItem> _setUpTearDownItems;

		// Token: 0x040001A7 RID: 423
		private List<TestActionItem> _actions;

		// Token: 0x020001C0 RID: 448
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2
		{
			// Token: 0x06000B4F RID: 2895 RVA: 0x0001A7BA File Offset: 0x000189BA
			public <>c__DisplayClass2()
			{
			}

			// Token: 0x06000B50 RID: 2896 RVA: 0x0001A7C4 File Offset: 0x000189C4
			public object <Execute>b__0()
			{
				this.disposable.Dispose();
				return null;
			}

			// Token: 0x040002DC RID: 732
			public IDisposable disposable;
		}
	}
}
