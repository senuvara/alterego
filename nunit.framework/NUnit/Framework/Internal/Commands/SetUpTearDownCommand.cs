﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework.Internal.Execution;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x0200016E RID: 366
	public class SetUpTearDownCommand : DelegatingTestCommand
	{
		// Token: 0x060009B5 RID: 2485 RVA: 0x00020A80 File Offset: 0x0001EC80
		public SetUpTearDownCommand(TestCommand innerCommand) : base(innerCommand)
		{
			Guard.ArgumentValid(innerCommand.Test is TestMethod, "SetUpTearDownCommand may only apply to a TestMethod", "innerCommand");
			Guard.OperationValid(base.Test.TypeInfo != null, "TestMethod must have a non-null TypeInfo");
			this._setUpTearDownItems = CommandBuilder.BuildSetUpTearDownList(base.Test.TypeInfo.Type, typeof(SetUpAttribute), typeof(TearDownAttribute));
		}

		// Token: 0x060009B6 RID: 2486 RVA: 0x00020B00 File Offset: 0x0001ED00
		public override TestResult Execute(ITestExecutionContext context)
		{
			try
			{
				int i = this._setUpTearDownItems.Count;
				while (i > 0)
				{
					this._setUpTearDownItems[--i].RunSetUp(context);
				}
				context.CurrentResult = this.innerCommand.Execute(context);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException)
				{
					Thread.ResetAbort();
				}
				context.CurrentResult.RecordException(ex);
			}
			finally
			{
				if (context.ExecutionStatus != TestExecutionStatus.AbortRequested)
				{
					for (int i = 0; i < this._setUpTearDownItems.Count; i++)
					{
						this._setUpTearDownItems[i].RunTearDown(context);
					}
				}
			}
			return context.CurrentResult;
		}

		// Token: 0x04000233 RID: 563
		private IList<SetUpTearDownItem> _setUpTearDownItems;
	}
}
