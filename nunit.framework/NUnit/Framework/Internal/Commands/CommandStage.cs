﻿using System;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x02000118 RID: 280
	public enum CommandStage
	{
		// Token: 0x040001B0 RID: 432
		Default,
		// Token: 0x040001B1 RID: 433
		BelowSetUpTearDown,
		// Token: 0x040001B2 RID: 434
		SetUpTearDown,
		// Token: 0x040001B3 RID: 435
		AboveSetUpTearDown
	}
}
