﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x0200016F RID: 367
	public class SetUpTearDownItem
	{
		// Token: 0x060009B7 RID: 2487 RVA: 0x00020BE4 File Offset: 0x0001EDE4
		public SetUpTearDownItem(IList<MethodInfo> setUpMethods, IList<MethodInfo> tearDownMethods)
		{
			this._setUpMethods = setUpMethods;
			this._tearDownMethods = tearDownMethods;
		}

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x060009B8 RID: 2488 RVA: 0x00020C00 File Offset: 0x0001EE00
		public bool HasMethods
		{
			get
			{
				return this._setUpMethods.Count > 0 || this._tearDownMethods.Count > 0;
			}
		}

		// Token: 0x060009B9 RID: 2489 RVA: 0x00020C34 File Offset: 0x0001EE34
		public void RunSetUp(ITestExecutionContext context)
		{
			this._setUpWasRun = true;
			foreach (MethodInfo method in this._setUpMethods)
			{
				this.RunSetUpOrTearDownMethod(context, method);
			}
		}

		// Token: 0x060009BA RID: 2490 RVA: 0x00020C98 File Offset: 0x0001EE98
		public void RunTearDown(ITestExecutionContext context)
		{
			if (this._setUpWasRun)
			{
				try
				{
					int num = this._tearDownMethods.Count;
					while (--num >= 0)
					{
						this.RunSetUpOrTearDownMethod(context, this._tearDownMethods[num]);
					}
				}
				catch (Exception ex)
				{
					context.CurrentResult.RecordTearDownException(ex);
				}
			}
		}

		// Token: 0x060009BB RID: 2491 RVA: 0x00020D0C File Offset: 0x0001EF0C
		private void RunSetUpOrTearDownMethod(ITestExecutionContext context, MethodInfo method)
		{
			this.RunNonAsyncMethod(method, context);
		}

		// Token: 0x060009BC RID: 2492 RVA: 0x00020D18 File Offset: 0x0001EF18
		private object RunNonAsyncMethod(MethodInfo method, ITestExecutionContext context)
		{
			return Reflect.InvokeMethod(method, method.IsStatic ? null : context.TestObject);
		}

		// Token: 0x04000234 RID: 564
		private IList<MethodInfo> _setUpMethods;

		// Token: 0x04000235 RID: 565
		private IList<MethodInfo> _tearDownMethods;

		// Token: 0x04000236 RID: 566
		private bool _setUpWasRun;
	}
}
