﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x0200016D RID: 365
	public class TestMethodCommand : TestCommand
	{
		// Token: 0x060009B1 RID: 2481 RVA: 0x000209BA File Offset: 0x0001EBBA
		public TestMethodCommand(TestMethod testMethod) : base(testMethod)
		{
			this.testMethod = testMethod;
			this.arguments = testMethod.Arguments;
		}

		// Token: 0x060009B2 RID: 2482 RVA: 0x000209DC File Offset: 0x0001EBDC
		public override TestResult Execute(ITestExecutionContext context)
		{
			object actual = this.RunTestMethod(context);
			if (this.testMethod.HasExpectedResult)
			{
				Assert.AreEqual(this.testMethod.ExpectedResult, actual);
			}
			context.CurrentResult.SetResult(ResultState.Success);
			return context.CurrentResult;
		}

		// Token: 0x060009B3 RID: 2483 RVA: 0x00020A34 File Offset: 0x0001EC34
		private object RunTestMethod(ITestExecutionContext context)
		{
			return this.RunNonAsyncTestMethod(context);
		}

		// Token: 0x060009B4 RID: 2484 RVA: 0x00020A50 File Offset: 0x0001EC50
		private object RunNonAsyncTestMethod(ITestExecutionContext context)
		{
			return this.testMethod.Method.Invoke(context.TestObject, this.arguments);
		}

		// Token: 0x04000231 RID: 561
		private readonly TestMethod testMethod;

		// Token: 0x04000232 RID: 562
		private readonly object[] arguments;
	}
}
