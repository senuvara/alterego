﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x02000083 RID: 131
	public class ApplyChangesToContextCommand : DelegatingTestCommand
	{
		// Token: 0x06000473 RID: 1139 RVA: 0x0000EC80 File Offset: 0x0000CE80
		public ApplyChangesToContextCommand(TestCommand innerCommand, IEnumerable<IApplyToContext> changes) : base(innerCommand)
		{
			this._changes = changes;
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x0000EC94 File Offset: 0x0000CE94
		public void ApplyChanges(ITestExecutionContext context)
		{
			foreach (IApplyToContext applyToContext in this._changes)
			{
				applyToContext.ApplyToContext(context);
			}
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x0000ECF0 File Offset: 0x0000CEF0
		public override TestResult Execute(ITestExecutionContext context)
		{
			try
			{
				this.ApplyChanges(context);
				context.CurrentResult = this.innerCommand.Execute(context);
			}
			catch (Exception ex)
			{
				if (ex is ThreadAbortException)
				{
					Thread.ResetAbort();
				}
				context.CurrentResult.RecordException(ex);
			}
			return context.CurrentResult;
		}

		// Token: 0x040000CE RID: 206
		private IEnumerable<IApplyToContext> _changes;
	}
}
