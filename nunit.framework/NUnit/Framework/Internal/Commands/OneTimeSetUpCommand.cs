﻿using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Commands
{
	// Token: 0x020000F3 RID: 243
	public class OneTimeSetUpCommand : TestCommand
	{
		// Token: 0x060006E9 RID: 1769 RVA: 0x000196D9 File Offset: 0x000178D9
		public OneTimeSetUpCommand(TestSuite suite, List<SetUpTearDownItem> setUpTearDown, List<TestActionItem> actions) : base(suite)
		{
			this._suite = suite;
			this._typeInfo = suite.TypeInfo;
			this._arguments = suite.Arguments;
			this._setUpTearDown = setUpTearDown;
			this._actions = actions;
		}

		// Token: 0x060006EA RID: 1770 RVA: 0x00019714 File Offset: 0x00017914
		public override TestResult Execute(ITestExecutionContext context)
		{
			if (this._typeInfo != null)
			{
				if (!this._typeInfo.IsStaticClass)
				{
					context.TestObject = (this._suite.Fixture ?? this._typeInfo.Construct(this._arguments));
					if (this._suite.Fixture == null)
					{
						this._suite.Fixture = context.TestObject;
					}
					base.Test.Fixture = this._suite.Fixture;
				}
				int i = this._setUpTearDown.Count;
				while (i > 0)
				{
					this._setUpTearDown[--i].RunSetUp(context);
				}
			}
			for (int i = 0; i < this._actions.Count; i++)
			{
				this._actions[i].BeforeTest(base.Test);
			}
			return context.CurrentResult;
		}

		// Token: 0x04000189 RID: 393
		private readonly TestSuite _suite;

		// Token: 0x0400018A RID: 394
		private readonly ITypeInfo _typeInfo;

		// Token: 0x0400018B RID: 395
		private readonly object[] _arguments;

		// Token: 0x0400018C RID: 396
		private readonly List<SetUpTearDownItem> _setUpTearDown;

		// Token: 0x0400018D RID: 397
		private readonly List<TestActionItem> _actions;
	}
}
