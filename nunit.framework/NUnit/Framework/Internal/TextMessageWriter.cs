﻿using System;
using System.Collections;
using NUnit.Framework.Constraints;

namespace NUnit.Framework.Internal
{
	// Token: 0x0200015D RID: 349
	public class TextMessageWriter : MessageWriter
	{
		// Token: 0x06000910 RID: 2320 RVA: 0x0001EA60 File Offset: 0x0001CC60
		public TextMessageWriter()
		{
		}

		// Token: 0x06000911 RID: 2321 RVA: 0x0001EA78 File Offset: 0x0001CC78
		public TextMessageWriter(string userMessage, params object[] args)
		{
			if (userMessage != null && userMessage != string.Empty)
			{
				base.WriteMessageLine(userMessage, args);
			}
		}

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x06000912 RID: 2322 RVA: 0x0001EABC File Offset: 0x0001CCBC
		// (set) Token: 0x06000913 RID: 2323 RVA: 0x0001EAD4 File Offset: 0x0001CCD4
		public override int MaxLineLength
		{
			get
			{
				return this.maxLineLength;
			}
			set
			{
				this.maxLineLength = value;
			}
		}

		// Token: 0x06000914 RID: 2324 RVA: 0x0001EAE0 File Offset: 0x0001CCE0
		public override void WriteMessageLine(int level, string message, params object[] args)
		{
			if (message != null)
			{
				while (level-- >= 0)
				{
					this.Write("  ");
				}
				if (args != null && args.Length > 0)
				{
					message = string.Format(message, args);
				}
				this.WriteLine(MsgUtils.EscapeNullCharacters(message));
			}
		}

		// Token: 0x06000915 RID: 2325 RVA: 0x0001EB3F File Offset: 0x0001CD3F
		public override void DisplayDifferences(ConstraintResult result)
		{
			this.WriteExpectedLine(result);
			this.WriteActualLine(result);
		}

		// Token: 0x06000916 RID: 2326 RVA: 0x0001EB52 File Offset: 0x0001CD52
		public override void DisplayDifferences(object expected, object actual)
		{
			this.WriteExpectedLine(expected);
			this.WriteActualLine(actual);
		}

		// Token: 0x06000917 RID: 2327 RVA: 0x0001EB65 File Offset: 0x0001CD65
		public override void DisplayDifferences(object expected, object actual, Tolerance tolerance)
		{
			this.WriteExpectedLine(expected, tolerance);
			this.WriteActualLine(actual);
		}

		// Token: 0x06000918 RID: 2328 RVA: 0x0001EB7C File Offset: 0x0001CD7C
		public override void DisplayStringDifferences(string expected, string actual, int mismatch, bool ignoreCase, bool clipping)
		{
			int maxDisplayLength = this.MaxLineLength - TextMessageWriter.PrefixLength - 2;
			if (clipping)
			{
				MsgUtils.ClipExpectedAndActual(ref expected, ref actual, maxDisplayLength, mismatch);
			}
			expected = MsgUtils.EscapeControlChars(expected);
			actual = MsgUtils.EscapeControlChars(actual);
			mismatch = MsgUtils.FindMismatchPosition(expected, actual, 0, ignoreCase);
			this.Write(TextMessageWriter.Pfx_Expected);
			this.Write(MsgUtils.FormatValue(expected));
			if (ignoreCase)
			{
				this.Write(", ignoring case");
			}
			this.WriteLine();
			this.WriteActualLine(actual);
			if (mismatch >= 0)
			{
				this.WriteCaretLine(mismatch);
			}
		}

		// Token: 0x06000919 RID: 2329 RVA: 0x0001EC17 File Offset: 0x0001CE17
		public override void WriteActualValue(object actual)
		{
			this.WriteValue(actual);
		}

		// Token: 0x0600091A RID: 2330 RVA: 0x0001EC22 File Offset: 0x0001CE22
		public override void WriteValue(object val)
		{
			this.Write(MsgUtils.FormatValue(val));
		}

		// Token: 0x0600091B RID: 2331 RVA: 0x0001EC32 File Offset: 0x0001CE32
		public override void WriteCollectionElements(IEnumerable collection, long start, int max)
		{
			this.Write(MsgUtils.FormatCollection(collection, start, max));
		}

		// Token: 0x0600091C RID: 2332 RVA: 0x0001EC44 File Offset: 0x0001CE44
		private void WriteExpectedLine(ConstraintResult result)
		{
			this.Write(TextMessageWriter.Pfx_Expected);
			this.WriteLine(result.Description);
		}

		// Token: 0x0600091D RID: 2333 RVA: 0x0001EC60 File Offset: 0x0001CE60
		private void WriteExpectedLine(object expected)
		{
			this.WriteExpectedLine(expected, null);
		}

		// Token: 0x0600091E RID: 2334 RVA: 0x0001EC6C File Offset: 0x0001CE6C
		private void WriteExpectedLine(object expected, Tolerance tolerance)
		{
			this.Write(TextMessageWriter.Pfx_Expected);
			this.Write(MsgUtils.FormatValue(expected));
			if (tolerance != null && !tolerance.IsUnsetOrDefault)
			{
				this.Write(" +/- ");
				this.Write(MsgUtils.FormatValue(tolerance.Value));
				if (tolerance.Mode != ToleranceMode.Linear)
				{
					this.Write(" {0}", tolerance.Mode);
				}
			}
			this.WriteLine();
		}

		// Token: 0x0600091F RID: 2335 RVA: 0x0001ECEE File Offset: 0x0001CEEE
		private void WriteActualLine(ConstraintResult result)
		{
			this.Write(TextMessageWriter.Pfx_Actual);
			result.WriteActualValueTo(this);
			this.WriteLine();
		}

		// Token: 0x06000920 RID: 2336 RVA: 0x0001ED0C File Offset: 0x0001CF0C
		private void WriteActualLine(object actual)
		{
			this.Write(TextMessageWriter.Pfx_Actual);
			this.WriteActualValue(actual);
			this.WriteLine();
		}

		// Token: 0x06000921 RID: 2337 RVA: 0x0001ED2A File Offset: 0x0001CF2A
		private void WriteCaretLine(int mismatch)
		{
			this.WriteLine("  {0}^", new string('-', TextMessageWriter.PrefixLength + mismatch - 2 + 1));
		}

		// Token: 0x06000922 RID: 2338 RVA: 0x0001ED4B File Offset: 0x0001CF4B
		// Note: this type is marked as 'beforefieldinit'.
		static TextMessageWriter()
		{
		}

		// Token: 0x04000201 RID: 513
		private static readonly int DEFAULT_LINE_LENGTH = 78;

		// Token: 0x04000202 RID: 514
		public static readonly string Pfx_Expected = "  Expected: ";

		// Token: 0x04000203 RID: 515
		public static readonly string Pfx_Actual = "  But was:  ";

		// Token: 0x04000204 RID: 516
		public static readonly int PrefixLength = TextMessageWriter.Pfx_Expected.Length;

		// Token: 0x04000205 RID: 517
		private int maxLineLength = TextMessageWriter.DEFAULT_LINE_LENGTH;
	}
}
