﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using Microsoft.Win32;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000161 RID: 353
	[SecuritySafeCritical]
	public class OSPlatform
	{
		// Token: 0x17000209 RID: 521
		// (get) Token: 0x06000930 RID: 2352 RVA: 0x0001EFEC File Offset: 0x0001D1EC
		public static OSPlatform CurrentPlatform
		{
			get
			{
				return OSPlatform.currentPlatform.Value;
			}
		}

		// Token: 0x06000931 RID: 2353 RVA: 0x0001F008 File Offset: 0x0001D208
		private static Version GetWindows81PlusVersion(Version version)
		{
			try
			{
				using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"))
				{
					if (registryKey != null)
					{
						string s = registryKey.GetValue("CurrentBuildNumber") as string;
						int build = 0;
						int.TryParse(s, out build);
						int? num = registryKey.GetValue("CurrentMajorVersionNumber") as int?;
						int? num2 = registryKey.GetValue("CurrentMinorVersionNumber") as int?;
						if (num != null && num2 != null)
						{
							return new Version(num.Value, num2.Value, build);
						}
						string a = registryKey.GetValue("CurrentVersion") as string;
						if (a == "6.3")
						{
							return new Version(6, 3, build);
						}
					}
				}
			}
			catch (Exception)
			{
			}
			return version;
		}

		// Token: 0x06000932 RID: 2354
		[DllImport("Kernel32.dll")]
		private static extern bool GetVersionEx(ref OSPlatform.OSVERSIONINFOEX osvi);

		// Token: 0x06000933 RID: 2355 RVA: 0x0001F12C File Offset: 0x0001D32C
		public OSPlatform(PlatformID platform, Version version)
		{
			this._platform = platform;
			this._version = version;
		}

		// Token: 0x06000934 RID: 2356 RVA: 0x0001F145 File Offset: 0x0001D345
		public OSPlatform(PlatformID platform, Version version, OSPlatform.ProductType product) : this(platform, version)
		{
			this._product = product;
		}

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x06000935 RID: 2357 RVA: 0x0001F15C File Offset: 0x0001D35C
		public PlatformID Platform
		{
			get
			{
				return this._platform;
			}
		}

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x06000936 RID: 2358 RVA: 0x0001F174 File Offset: 0x0001D374
		public Version Version
		{
			get
			{
				return this._version;
			}
		}

		// Token: 0x1700020C RID: 524
		// (get) Token: 0x06000937 RID: 2359 RVA: 0x0001F18C File Offset: 0x0001D38C
		public OSPlatform.ProductType Product
		{
			get
			{
				return this._product;
			}
		}

		// Token: 0x1700020D RID: 525
		// (get) Token: 0x06000938 RID: 2360 RVA: 0x0001F1A4 File Offset: 0x0001D3A4
		public bool IsWindows
		{
			get
			{
				return this._platform == PlatformID.Win32NT || this._platform == PlatformID.Win32Windows || this._platform == PlatformID.Win32S || this._platform == PlatformID.WinCE;
			}
		}

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x06000939 RID: 2361 RVA: 0x0001F1DC File Offset: 0x0001D3DC
		public bool IsUnix
		{
			get
			{
				return this._platform == OSPlatform.UnixPlatformID_Microsoft || this._platform == OSPlatform.UnixPlatformID_Mono;
			}
		}

		// Token: 0x1700020F RID: 527
		// (get) Token: 0x0600093A RID: 2362 RVA: 0x0001F20C File Offset: 0x0001D40C
		public bool IsWin32S
		{
			get
			{
				return this._platform == PlatformID.Win32S;
			}
		}

		// Token: 0x17000210 RID: 528
		// (get) Token: 0x0600093B RID: 2363 RVA: 0x0001F228 File Offset: 0x0001D428
		public bool IsWin32Windows
		{
			get
			{
				return this._platform == PlatformID.Win32Windows;
			}
		}

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x0600093C RID: 2364 RVA: 0x0001F244 File Offset: 0x0001D444
		public bool IsWin32NT
		{
			get
			{
				return this._platform == PlatformID.Win32NT;
			}
		}

		// Token: 0x17000212 RID: 530
		// (get) Token: 0x0600093D RID: 2365 RVA: 0x0001F260 File Offset: 0x0001D460
		public bool IsWinCE
		{
			get
			{
				return this._platform == PlatformID.WinCE;
			}
		}

		// Token: 0x17000213 RID: 531
		// (get) Token: 0x0600093E RID: 2366 RVA: 0x0001F27C File Offset: 0x0001D47C
		public bool IsXbox
		{
			get
			{
				return this._platform == OSPlatform.XBoxPlatformID;
			}
		}

		// Token: 0x17000214 RID: 532
		// (get) Token: 0x0600093F RID: 2367 RVA: 0x0001F29C File Offset: 0x0001D49C
		public bool IsMacOSX
		{
			get
			{
				return this._platform == OSPlatform.MacOSXPlatformID;
			}
		}

		// Token: 0x06000940 RID: 2368
		[DllImport("libc")]
		private static extern int uname(IntPtr buf);

		// Token: 0x06000941 RID: 2369 RVA: 0x0001F2BC File Offset: 0x0001D4BC
		private static bool CheckIfIsMacOSX(PlatformID platform)
		{
			bool result;
			if (platform == PlatformID.MacOSX)
			{
				result = true;
			}
			else if (platform != PlatformID.Unix)
			{
				result = false;
			}
			else
			{
				IntPtr intPtr = Marshal.AllocHGlobal(8192);
				bool flag = false;
				if (OSPlatform.uname(intPtr) == 0)
				{
					string text = Marshal.PtrToStringAnsi(intPtr);
					flag = text.Equals("Darwin");
				}
				Marshal.FreeHGlobal(intPtr);
				result = flag;
			}
			return result;
		}

		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06000942 RID: 2370 RVA: 0x0001F32C File Offset: 0x0001D52C
		public bool IsWin95
		{
			get
			{
				return this._platform == PlatformID.Win32Windows && this._version.Major == 4 && this._version.Minor == 0;
			}
		}

		// Token: 0x17000216 RID: 534
		// (get) Token: 0x06000943 RID: 2371 RVA: 0x0001F368 File Offset: 0x0001D568
		public bool IsWin98
		{
			get
			{
				return this._platform == PlatformID.Win32Windows && this._version.Major == 4 && this._version.Minor == 10;
			}
		}

		// Token: 0x17000217 RID: 535
		// (get) Token: 0x06000944 RID: 2372 RVA: 0x0001F3A4 File Offset: 0x0001D5A4
		public bool IsWinME
		{
			get
			{
				return this._platform == PlatformID.Win32Windows && this._version.Major == 4 && this._version.Minor == 90;
			}
		}

		// Token: 0x17000218 RID: 536
		// (get) Token: 0x06000945 RID: 2373 RVA: 0x0001F3E0 File Offset: 0x0001D5E0
		public bool IsNT3
		{
			get
			{
				return this._platform == PlatformID.Win32NT && this._version.Major == 3;
			}
		}

		// Token: 0x17000219 RID: 537
		// (get) Token: 0x06000946 RID: 2374 RVA: 0x0001F40C File Offset: 0x0001D60C
		public bool IsNT4
		{
			get
			{
				return this._platform == PlatformID.Win32NT && this._version.Major == 4;
			}
		}

		// Token: 0x1700021A RID: 538
		// (get) Token: 0x06000947 RID: 2375 RVA: 0x0001F438 File Offset: 0x0001D638
		public bool IsNT5
		{
			get
			{
				return this._platform == PlatformID.Win32NT && this._version.Major == 5;
			}
		}

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06000948 RID: 2376 RVA: 0x0001F464 File Offset: 0x0001D664
		public bool IsWin2K
		{
			get
			{
				return this.IsNT5 && this._version.Minor == 0;
			}
		}

		// Token: 0x1700021C RID: 540
		// (get) Token: 0x06000949 RID: 2377 RVA: 0x0001F490 File Offset: 0x0001D690
		public bool IsWinXP
		{
			get
			{
				return this.IsNT5 && (this._version.Minor == 1 || (this._version.Minor == 2 && this.Product == OSPlatform.ProductType.WorkStation));
			}
		}

		// Token: 0x1700021D RID: 541
		// (get) Token: 0x0600094A RID: 2378 RVA: 0x0001F4D8 File Offset: 0x0001D6D8
		public bool IsWin2003Server
		{
			get
			{
				return this.IsNT5 && this._version.Minor == 2 && this.Product == OSPlatform.ProductType.Server;
			}
		}

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x0600094B RID: 2379 RVA: 0x0001F50C File Offset: 0x0001D70C
		public bool IsNT6
		{
			get
			{
				return this._platform == PlatformID.Win32NT && this._version.Major == 6;
			}
		}

		// Token: 0x1700021F RID: 543
		// (get) Token: 0x0600094C RID: 2380 RVA: 0x0001F538 File Offset: 0x0001D738
		public bool IsNT60
		{
			get
			{
				return this.IsNT6 && this._version.Minor == 0;
			}
		}

		// Token: 0x17000220 RID: 544
		// (get) Token: 0x0600094D RID: 2381 RVA: 0x0001F564 File Offset: 0x0001D764
		public bool IsNT61
		{
			get
			{
				return this.IsNT6 && this._version.Minor == 1;
			}
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x0600094E RID: 2382 RVA: 0x0001F590 File Offset: 0x0001D790
		public bool IsNT62
		{
			get
			{
				return this.IsNT6 && this._version.Minor == 2;
			}
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x0600094F RID: 2383 RVA: 0x0001F5BC File Offset: 0x0001D7BC
		public bool IsNT63
		{
			get
			{
				return this.IsNT6 && this._version.Minor == 3;
			}
		}

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x06000950 RID: 2384 RVA: 0x0001F5E8 File Offset: 0x0001D7E8
		public bool IsVista
		{
			get
			{
				return this.IsNT60 && this.Product == OSPlatform.ProductType.WorkStation;
			}
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x06000951 RID: 2385 RVA: 0x0001F610 File Offset: 0x0001D810
		public bool IsWin2008Server
		{
			get
			{
				return this.IsWin2008ServerR1 || this.IsWin2008ServerR2;
			}
		}

		// Token: 0x17000225 RID: 549
		// (get) Token: 0x06000952 RID: 2386 RVA: 0x0001F634 File Offset: 0x0001D834
		public bool IsWin2008ServerR1
		{
			get
			{
				return this.IsNT60 && this.Product == OSPlatform.ProductType.Server;
			}
		}

		// Token: 0x17000226 RID: 550
		// (get) Token: 0x06000953 RID: 2387 RVA: 0x0001F65C File Offset: 0x0001D85C
		public bool IsWin2008ServerR2
		{
			get
			{
				return this.IsNT61 && this.Product == OSPlatform.ProductType.Server;
			}
		}

		// Token: 0x17000227 RID: 551
		// (get) Token: 0x06000954 RID: 2388 RVA: 0x0001F684 File Offset: 0x0001D884
		public bool IsWin2012Server
		{
			get
			{
				return this.IsWin2012ServerR1 || this.IsWin2012ServerR2;
			}
		}

		// Token: 0x17000228 RID: 552
		// (get) Token: 0x06000955 RID: 2389 RVA: 0x0001F6A8 File Offset: 0x0001D8A8
		public bool IsWin2012ServerR1
		{
			get
			{
				return this.IsNT62 && this.Product == OSPlatform.ProductType.Server;
			}
		}

		// Token: 0x17000229 RID: 553
		// (get) Token: 0x06000956 RID: 2390 RVA: 0x0001F6D0 File Offset: 0x0001D8D0
		public bool IsWin2012ServerR2
		{
			get
			{
				return this.IsNT63 && this.Product == OSPlatform.ProductType.Server;
			}
		}

		// Token: 0x1700022A RID: 554
		// (get) Token: 0x06000957 RID: 2391 RVA: 0x0001F6F8 File Offset: 0x0001D8F8
		public bool IsWindows7
		{
			get
			{
				return this.IsNT61 && this.Product == OSPlatform.ProductType.WorkStation;
			}
		}

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x06000958 RID: 2392 RVA: 0x0001F720 File Offset: 0x0001D920
		public bool IsWindows8
		{
			get
			{
				return this.IsNT62 && this.Product == OSPlatform.ProductType.WorkStation;
			}
		}

		// Token: 0x1700022C RID: 556
		// (get) Token: 0x06000959 RID: 2393 RVA: 0x0001F748 File Offset: 0x0001D948
		public bool IsWindows81
		{
			get
			{
				return this.IsNT63 && this.Product == OSPlatform.ProductType.WorkStation;
			}
		}

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x0600095A RID: 2394 RVA: 0x0001F770 File Offset: 0x0001D970
		public bool IsWindows10
		{
			get
			{
				return this._platform == PlatformID.Win32NT && this._version.Major == 10 && this.Product == OSPlatform.ProductType.WorkStation;
			}
		}

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x0600095B RID: 2395 RVA: 0x0001F7A8 File Offset: 0x0001D9A8
		public bool IsWindowsServer10
		{
			get
			{
				return this._platform == PlatformID.Win32NT && this._version.Major == 10 && this.Product == OSPlatform.ProductType.Server;
			}
		}

		// Token: 0x0600095C RID: 2396 RVA: 0x0001F8CC File Offset: 0x0001DACC
		// Note: this type is marked as 'beforefieldinit'.
		static OSPlatform()
		{
		}

		// Token: 0x0600095D RID: 2397 RVA: 0x0001F7E0 File Offset: 0x0001D9E0
		[CompilerGenerated]
		private static OSPlatform <.cctor>b__0()
		{
			OperatingSystem operatingSystem = Environment.OSVersion;
			OSPlatform result;
			if (operatingSystem.Platform == PlatformID.Win32NT && operatingSystem.Version.Major >= 5)
			{
				OSPlatform.OSVERSIONINFOEX osversioninfoex = default(OSPlatform.OSVERSIONINFOEX);
				osversioninfoex.dwOSVersionInfoSize = (uint)Marshal.SizeOf(osversioninfoex);
				OSPlatform.GetVersionEx(ref osversioninfoex);
				if (operatingSystem.Version.Major == 6 && operatingSystem.Version.Minor >= 2)
				{
					operatingSystem = new OperatingSystem(operatingSystem.Platform, OSPlatform.GetWindows81PlusVersion(operatingSystem.Version));
				}
				result = new OSPlatform(operatingSystem.Platform, operatingSystem.Version, (OSPlatform.ProductType)osversioninfoex.ProductType);
			}
			else if (OSPlatform.CheckIfIsMacOSX(operatingSystem.Platform))
			{
				result = new OSPlatform(PlatformID.MacOSX, operatingSystem.Version);
			}
			else
			{
				result = new OSPlatform(operatingSystem.Platform, operatingSystem.Version);
			}
			return result;
		}

		// Token: 0x0400020A RID: 522
		private readonly PlatformID _platform;

		// Token: 0x0400020B RID: 523
		private readonly Version _version;

		// Token: 0x0400020C RID: 524
		private readonly OSPlatform.ProductType _product;

		// Token: 0x0400020D RID: 525
		private static readonly System.Lazy<OSPlatform> currentPlatform = new System.Lazy<OSPlatform>(delegate()
		{
			OperatingSystem operatingSystem = Environment.OSVersion;
			OSPlatform result;
			if (operatingSystem.Platform == PlatformID.Win32NT && operatingSystem.Version.Major >= 5)
			{
				OSPlatform.OSVERSIONINFOEX osversioninfoex = default(OSPlatform.OSVERSIONINFOEX);
				osversioninfoex.dwOSVersionInfoSize = (uint)Marshal.SizeOf(osversioninfoex);
				OSPlatform.GetVersionEx(ref osversioninfoex);
				if (operatingSystem.Version.Major == 6 && operatingSystem.Version.Minor >= 2)
				{
					operatingSystem = new OperatingSystem(operatingSystem.Platform, OSPlatform.GetWindows81PlusVersion(operatingSystem.Version));
				}
				result = new OSPlatform(operatingSystem.Platform, operatingSystem.Version, (OSPlatform.ProductType)osversioninfoex.ProductType);
			}
			else if (OSPlatform.CheckIfIsMacOSX(operatingSystem.Platform))
			{
				result = new OSPlatform(PlatformID.MacOSX, operatingSystem.Version);
			}
			else
			{
				result = new OSPlatform(operatingSystem.Platform, operatingSystem.Version);
			}
			return result;
		});

		// Token: 0x0400020E RID: 526
		public static readonly PlatformID UnixPlatformID_Microsoft = PlatformID.Unix;

		// Token: 0x0400020F RID: 527
		public static readonly PlatformID UnixPlatformID_Mono = (PlatformID)128;

		// Token: 0x04000210 RID: 528
		public static readonly PlatformID XBoxPlatformID = PlatformID.Xbox;

		// Token: 0x04000211 RID: 529
		public static readonly PlatformID MacOSXPlatformID = PlatformID.MacOSX;

		// Token: 0x04000212 RID: 530
		[CompilerGenerated]
		private static Func<OSPlatform> CS$<>9__CachedAnonymousMethodDelegate1;

		// Token: 0x02000162 RID: 354
		public enum ProductType
		{
			// Token: 0x04000214 RID: 532
			Unknown,
			// Token: 0x04000215 RID: 533
			WorkStation,
			// Token: 0x04000216 RID: 534
			DomainController,
			// Token: 0x04000217 RID: 535
			Server
		}

		// Token: 0x02000163 RID: 355
		private struct OSVERSIONINFOEX
		{
			// Token: 0x04000218 RID: 536
			public uint dwOSVersionInfoSize;

			// Token: 0x04000219 RID: 537
			public readonly uint dwMajorVersion;

			// Token: 0x0400021A RID: 538
			public readonly uint dwMinorVersion;

			// Token: 0x0400021B RID: 539
			public readonly uint dwBuildNumber;

			// Token: 0x0400021C RID: 540
			public readonly uint dwPlatformId;

			// Token: 0x0400021D RID: 541
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
			public readonly string szCSDVersion;

			// Token: 0x0400021E RID: 542
			public readonly short wServicePackMajor;

			// Token: 0x0400021F RID: 543
			public readonly short wServicePackMinor;

			// Token: 0x04000220 RID: 544
			public readonly short wSuiteMask;

			// Token: 0x04000221 RID: 545
			public readonly byte ProductType;

			// Token: 0x04000222 RID: 546
			public readonly byte Reserved;
		}
	}
}
