﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000048 RID: 72
	public class ExceptionHelper
	{
		// Token: 0x06000243 RID: 579 RVA: 0x00007BA0 File Offset: 0x00005DA0
		static ExceptionHelper()
		{
			MethodInfo method = typeof(Exception).GetMethod("InternalPreserveStackTrace", BindingFlags.Instance | BindingFlags.NonPublic);
			if (method != null)
			{
				try
				{
					ExceptionHelper.PreserveStackTrace = (Action<Exception>)Delegate.CreateDelegate(typeof(Action<Exception>), method);
					return;
				}
				catch (InvalidOperationException)
				{
				}
			}
			ExceptionHelper.PreserveStackTrace = delegate(Exception _)
			{
			};
		}

		// Token: 0x06000244 RID: 580 RVA: 0x00007C28 File Offset: 0x00005E28
		public static void Rethrow(Exception exception)
		{
			ExceptionHelper.PreserveStackTrace(exception);
			throw exception;
		}

		// Token: 0x06000245 RID: 581 RVA: 0x00007C38 File Offset: 0x00005E38
		public static string BuildMessage(Exception exception)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat(CultureInfo.CurrentCulture, "{0} : {1}", new object[]
			{
				exception.GetType().ToString(),
				exception.Message
			});
			foreach (Exception ex in ExceptionHelper.FlattenExceptionHierarchy(exception))
			{
				stringBuilder.Append(Env.NewLine);
				stringBuilder.AppendFormat(CultureInfo.CurrentCulture, "  ----> {0} : {1}", new object[]
				{
					ex.GetType().ToString(),
					ex.Message
				});
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000246 RID: 582 RVA: 0x00007D0C File Offset: 0x00005F0C
		public static string BuildStackTrace(Exception exception)
		{
			StringBuilder stringBuilder = new StringBuilder(ExceptionHelper.GetStackTrace(exception));
			foreach (Exception ex in ExceptionHelper.FlattenExceptionHierarchy(exception))
			{
				stringBuilder.Append(Env.NewLine);
				stringBuilder.Append("--");
				stringBuilder.Append(ex.GetType().Name);
				stringBuilder.Append(Env.NewLine);
				stringBuilder.Append(ExceptionHelper.GetStackTrace(ex));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000247 RID: 583 RVA: 0x00007DBC File Offset: 0x00005FBC
		public static string GetStackTrace(Exception exception)
		{
			string result;
			try
			{
				result = exception.StackTrace;
			}
			catch (Exception)
			{
				result = "No stack trace available";
			}
			return result;
		}

		// Token: 0x06000248 RID: 584 RVA: 0x00007DF0 File Offset: 0x00005FF0
		private static List<Exception> FlattenExceptionHierarchy(Exception exception)
		{
			List<Exception> list = new List<Exception>();
			if (exception.InnerException != null)
			{
				list.Add(exception.InnerException);
				list.AddRange(ExceptionHelper.FlattenExceptionHierarchy(exception.InnerException));
			}
			return list;
		}

		// Token: 0x06000249 RID: 585 RVA: 0x00007E37 File Offset: 0x00006037
		public ExceptionHelper()
		{
		}

		// Token: 0x0600024A RID: 586 RVA: 0x00007B9D File Offset: 0x00005D9D
		[CompilerGenerated]
		private static void <.cctor>b__0(Exception _)
		{
		}

		// Token: 0x04000076 RID: 118
		private static readonly Action<Exception> PreserveStackTrace;

		// Token: 0x04000077 RID: 119
		[CompilerGenerated]
		private static Action<Exception> CS$<>9__CachedAnonymousMethodDelegate1;
	}
}
