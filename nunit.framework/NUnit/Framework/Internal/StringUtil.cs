﻿using System;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000F2 RID: 242
	public class StringUtil
	{
		// Token: 0x060006E6 RID: 1766 RVA: 0x00019690 File Offset: 0x00017890
		public static int Compare(string strA, string strB, bool ignoreCase)
		{
			StringComparison comparisonType = ignoreCase ? StringComparison.CurrentCultureIgnoreCase : StringComparison.CurrentCulture;
			return string.Compare(strA, strB, comparisonType);
		}

		// Token: 0x060006E7 RID: 1767 RVA: 0x000196B4 File Offset: 0x000178B4
		public static bool StringsEqual(string strA, string strB, bool ignoreCase)
		{
			return StringUtil.Compare(strA, strB, ignoreCase) == 0;
		}

		// Token: 0x060006E8 RID: 1768 RVA: 0x000196D1 File Offset: 0x000178D1
		public StringUtil()
		{
		}
	}
}
