﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal.Filters;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000005 RID: 5
	[Serializable]
	public abstract class TestFilter : ITestFilter, IXmlNodeBuilder
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600001C RID: 28 RVA: 0x00002274 File Offset: 0x00000474
		public bool IsEmpty
		{
			get
			{
				return this is TestFilter.EmptyFilter;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600001D RID: 29 RVA: 0x00002290 File Offset: 0x00000490
		// (set) Token: 0x0600001E RID: 30 RVA: 0x000022A7 File Offset: 0x000004A7
		public bool TopLevel
		{
			[CompilerGenerated]
			get
			{
				return this.<TopLevel>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TopLevel>k__BackingField = value;
			}
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000022B0 File Offset: 0x000004B0
		public virtual bool Pass(ITest test)
		{
			return this.Match(test) || this.MatchParent(test) || this.MatchDescendant(test);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x000022E0 File Offset: 0x000004E0
		public virtual bool IsExplicitMatch(ITest test)
		{
			return this.Match(test) || this.MatchDescendant(test);
		}

		// Token: 0x06000021 RID: 33
		public abstract bool Match(ITest test);

		// Token: 0x06000022 RID: 34 RVA: 0x00002308 File Offset: 0x00000508
		public bool MatchParent(ITest test)
		{
			return test.Parent != null && (this.Match(test.Parent) || this.MatchParent(test.Parent));
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002344 File Offset: 0x00000544
		protected virtual bool MatchDescendant(ITest test)
		{
			bool result;
			if (test.Tests == null)
			{
				result = false;
			}
			else
			{
				foreach (ITest test2 in test.Tests)
				{
					if (this.Match(test2) || this.MatchDescendant(test2))
					{
						return true;
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000023D0 File Offset: 0x000005D0
		public static TestFilter FromXml(string xmlText)
		{
			TNode tnode = TNode.FromXml(xmlText);
			if (tnode.Name != "filter")
			{
				throw new Exception("Expected filter element at top level");
			}
			int count = tnode.ChildNodes.Count;
			TestFilter testFilter = (count == 0) ? TestFilter.Empty : ((count == 1) ? TestFilter.FromXml(tnode.FirstChild) : TestFilter.FromXml(tnode));
			testFilter.TopLevel = true;
			return testFilter;
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002444 File Offset: 0x00000644
		public static TestFilter FromXml(TNode node)
		{
			bool isRegex = node.Attributes["re"] == "1";
			string name = node.Name;
			if (name != null)
			{
				if (<PrivateImplementationDetails>{59819BE1-42C3-4115-ADE6-88F6962021F1}.$$method0x6000025-1 == null)
				{
					<PrivateImplementationDetails>{59819BE1-42C3-4115-ADE6-88F6962021F1}.$$method0x6000025-1 = new Dictionary<string, int>(11)
					{
						{
							"filter",
							0
						},
						{
							"and",
							1
						},
						{
							"or",
							2
						},
						{
							"not",
							3
						},
						{
							"id",
							4
						},
						{
							"test",
							5
						},
						{
							"name",
							6
						},
						{
							"method",
							7
						},
						{
							"class",
							8
						},
						{
							"cat",
							9
						},
						{
							"prop",
							10
						}
					};
				}
				int num;
				if (<PrivateImplementationDetails>{59819BE1-42C3-4115-ADE6-88F6962021F1}.$$method0x6000025-1.TryGetValue(name, out num))
				{
					TestFilter result;
					switch (num)
					{
					case 0:
					case 1:
					{
						AndFilter andFilter = new AndFilter();
						foreach (TNode node2 in node.ChildNodes)
						{
							andFilter.Add(TestFilter.FromXml(node2));
						}
						result = andFilter;
						break;
					}
					case 2:
					{
						OrFilter orFilter = new OrFilter();
						foreach (TNode node2 in node.ChildNodes)
						{
							orFilter.Add(TestFilter.FromXml(node2));
						}
						result = orFilter;
						break;
					}
					case 3:
						result = new NotFilter(TestFilter.FromXml(node.FirstChild));
						break;
					case 4:
						result = new IdFilter(node.Value);
						break;
					case 5:
						result = new FullNameFilter(node.Value)
						{
							IsRegex = isRegex
						};
						break;
					case 6:
						result = new TestNameFilter(node.Value)
						{
							IsRegex = isRegex
						};
						break;
					case 7:
						result = new MethodNameFilter(node.Value)
						{
							IsRegex = isRegex
						};
						break;
					case 8:
						result = new ClassNameFilter(node.Value)
						{
							IsRegex = isRegex
						};
						break;
					case 9:
						result = new CategoryFilter(node.Value)
						{
							IsRegex = isRegex
						};
						break;
					case 10:
					{
						string text = node.Attributes["name"];
						if (text == null)
						{
							goto IL_2B7;
						}
						result = new PropertyFilter(text, node.Value)
						{
							IsRegex = isRegex
						};
						break;
					}
					default:
						goto IL_2B7;
					}
					return result;
				}
			}
			IL_2B7:
			throw new ArgumentException("Invalid filter element: " + node.Name, "xmlNode");
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00002744 File Offset: 0x00000944
		public TNode ToXml(bool recursive)
		{
			return this.AddToXml(new TNode("dummy"), recursive);
		}

		// Token: 0x06000027 RID: 39
		public abstract TNode AddToXml(TNode parentNode, bool recursive);

		// Token: 0x06000028 RID: 40 RVA: 0x00002773 File Offset: 0x00000973
		protected TestFilter()
		{
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002767 File Offset: 0x00000967
		// Note: this type is marked as 'beforefieldinit'.
		static TestFilter()
		{
		}

		// Token: 0x04000001 RID: 1
		public static readonly TestFilter Empty = new TestFilter.EmptyFilter();

		// Token: 0x04000002 RID: 2
		[CompilerGenerated]
		private bool <TopLevel>k__BackingField;

		// Token: 0x02000006 RID: 6
		[Serializable]
		private class EmptyFilter : TestFilter
		{
			// Token: 0x0600002A RID: 42 RVA: 0x0000277C File Offset: 0x0000097C
			public override bool Match(ITest test)
			{
				return true;
			}

			// Token: 0x0600002B RID: 43 RVA: 0x00002790 File Offset: 0x00000990
			public override bool Pass(ITest test)
			{
				return true;
			}

			// Token: 0x0600002C RID: 44 RVA: 0x000027A4 File Offset: 0x000009A4
			public override bool IsExplicitMatch(ITest test)
			{
				return false;
			}

			// Token: 0x0600002D RID: 45 RVA: 0x000027B8 File Offset: 0x000009B8
			public override TNode AddToXml(TNode parentNode, bool recursive)
			{
				return parentNode.AddElement("filter");
			}

			// Token: 0x0600002E RID: 46 RVA: 0x000027D5 File Offset: 0x000009D5
			public EmptyFilter()
			{
			}
		}
	}
}
