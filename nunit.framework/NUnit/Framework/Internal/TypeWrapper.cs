﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000C6 RID: 198
	public class TypeWrapper : ITypeInfo, IReflectionInfo
	{
		// Token: 0x06000601 RID: 1537 RVA: 0x00014DFC File Offset: 0x00012FFC
		public TypeWrapper(Type type)
		{
			Guard.ArgumentNotNull(type, "Type");
			this.Type = type;
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x06000602 RID: 1538 RVA: 0x00014E1C File Offset: 0x0001301C
		// (set) Token: 0x06000603 RID: 1539 RVA: 0x00014E33 File Offset: 0x00013033
		public Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Type>k__BackingField = value;
			}
		}

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x06000604 RID: 1540 RVA: 0x00014E3C File Offset: 0x0001303C
		public ITypeInfo BaseType
		{
			get
			{
				Type baseType = this.Type.GetTypeInfo().BaseType;
				return (baseType != null) ? new TypeWrapper(baseType) : null;
			}
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x06000605 RID: 1541 RVA: 0x00014E6C File Offset: 0x0001306C
		public string Name
		{
			get
			{
				return this.Type.Name;
			}
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x06000606 RID: 1542 RVA: 0x00014E8C File Offset: 0x0001308C
		public string FullName
		{
			get
			{
				return this.Type.FullName;
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x06000607 RID: 1543 RVA: 0x00014EAC File Offset: 0x000130AC
		public Assembly Assembly
		{
			get
			{
				return this.Type.GetTypeInfo().Assembly;
			}
		}

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x06000608 RID: 1544 RVA: 0x00014ED0 File Offset: 0x000130D0
		public string Namespace
		{
			get
			{
				return this.Type.Namespace;
			}
		}

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x06000609 RID: 1545 RVA: 0x00014EF0 File Offset: 0x000130F0
		public bool IsAbstract
		{
			get
			{
				return this.Type.GetTypeInfo().IsAbstract;
			}
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x0600060A RID: 1546 RVA: 0x00014F14 File Offset: 0x00013114
		public bool IsGenericType
		{
			get
			{
				return this.Type.GetTypeInfo().IsGenericType;
			}
		}

		// Token: 0x0600060B RID: 1547 RVA: 0x00014F38 File Offset: 0x00013138
		public bool IsType(Type type)
		{
			return this.Type == type;
		}

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x0600060C RID: 1548 RVA: 0x00014F54 File Offset: 0x00013154
		public bool ContainsGenericParameters
		{
			get
			{
				return this.Type.GetTypeInfo().ContainsGenericParameters;
			}
		}

		// Token: 0x1700015B RID: 347
		// (get) Token: 0x0600060D RID: 1549 RVA: 0x00014F78 File Offset: 0x00013178
		public bool IsGenericTypeDefinition
		{
			get
			{
				return this.Type.GetTypeInfo().IsGenericTypeDefinition;
			}
		}

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x0600060E RID: 1550 RVA: 0x00014F9C File Offset: 0x0001319C
		public bool IsSealed
		{
			get
			{
				return this.Type.GetTypeInfo().IsSealed;
			}
		}

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x0600060F RID: 1551 RVA: 0x00014FC0 File Offset: 0x000131C0
		public bool IsStaticClass
		{
			get
			{
				return this.Type.GetTypeInfo().IsSealed && this.Type.GetTypeInfo().IsAbstract;
			}
		}

		// Token: 0x06000610 RID: 1552 RVA: 0x00014FF8 File Offset: 0x000131F8
		public string GetDisplayName()
		{
			return TypeHelper.GetDisplayName(this.Type);
		}

		// Token: 0x06000611 RID: 1553 RVA: 0x00015018 File Offset: 0x00013218
		public string GetDisplayName(object[] args)
		{
			return TypeHelper.GetDisplayName(this.Type, args);
		}

		// Token: 0x06000612 RID: 1554 RVA: 0x00015038 File Offset: 0x00013238
		public ITypeInfo MakeGenericType(Type[] typeArgs)
		{
			return new TypeWrapper(this.Type.MakeGenericType(typeArgs));
		}

		// Token: 0x06000613 RID: 1555 RVA: 0x0001505C File Offset: 0x0001325C
		public Type GetGenericTypeDefinition()
		{
			return this.Type.GetGenericTypeDefinition();
		}

		// Token: 0x06000614 RID: 1556 RVA: 0x0001507C File Offset: 0x0001327C
		public T[] GetCustomAttributes<T>(bool inherit) where T : class
		{
			return (T[])this.Type.GetCustomAttributes(typeof(T), inherit);
		}

		// Token: 0x06000615 RID: 1557 RVA: 0x000150AC File Offset: 0x000132AC
		public bool IsDefined<T>(bool inherit)
		{
			return this.Type.GetTypeInfo().IsDefined(typeof(T), inherit);
		}

		// Token: 0x06000616 RID: 1558 RVA: 0x000150DC File Offset: 0x000132DC
		public bool HasMethodWithAttribute(Type attributeType)
		{
			return Reflect.HasMethodWithAttribute(this.Type, attributeType);
		}

		// Token: 0x06000617 RID: 1559 RVA: 0x000150FC File Offset: 0x000132FC
		public IMethodInfo[] GetMethods(BindingFlags flags)
		{
			MethodInfo[] methods = this.Type.GetMethods(flags);
			MethodWrapper[] array = new MethodWrapper[methods.Length];
			for (int i = 0; i < methods.Length; i++)
			{
				array[i] = new MethodWrapper(this.Type, methods[i]);
			}
			return array;
		}

		// Token: 0x06000618 RID: 1560 RVA: 0x00015178 File Offset: 0x00013378
		public ConstructorInfo GetConstructor(Type[] argTypes)
		{
			return (from c in this.Type.GetConstructors()
			where c.GetParameters().ParametersMatch(argTypes)
			select c).FirstOrDefault<ConstructorInfo>();
		}

		// Token: 0x06000619 RID: 1561 RVA: 0x000151B8 File Offset: 0x000133B8
		public bool HasConstructor(Type[] argTypes)
		{
			return this.GetConstructor(argTypes) != null;
		}

		// Token: 0x0600061A RID: 1562 RVA: 0x000151D8 File Offset: 0x000133D8
		public object Construct(object[] args)
		{
			return Reflect.Construct(this.Type, args);
		}

		// Token: 0x0600061B RID: 1563 RVA: 0x000151F8 File Offset: 0x000133F8
		public override string ToString()
		{
			return this.Type.ToString();
		}

		// Token: 0x04000144 RID: 324
		[CompilerGenerated]
		private Type <Type>k__BackingField;

		// Token: 0x020001B3 RID: 435
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1
		{
			// Token: 0x06000B2E RID: 2862 RVA: 0x0001514A File Offset: 0x0001334A
			public <>c__DisplayClass1()
			{
			}

			// Token: 0x06000B2F RID: 2863 RVA: 0x00015154 File Offset: 0x00013354
			public bool <GetConstructor>b__0(ConstructorInfo c)
			{
				return c.GetParameters().ParametersMatch(this.argTypes);
			}

			// Token: 0x040002C7 RID: 711
			public Type[] argTypes;
		}
	}
}
