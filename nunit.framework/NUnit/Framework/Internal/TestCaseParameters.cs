﻿using System;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000B5 RID: 181
	public class TestCaseParameters : TestParameters, ITestCaseData, ITestData, IApplyToTest
	{
		// Token: 0x0600052D RID: 1325 RVA: 0x00012810 File Offset: 0x00010A10
		public TestCaseParameters()
		{
		}

		// Token: 0x0600052E RID: 1326 RVA: 0x0001281B File Offset: 0x00010A1B
		public TestCaseParameters(Exception exception) : base(exception)
		{
		}

		// Token: 0x0600052F RID: 1327 RVA: 0x00012827 File Offset: 0x00010A27
		public TestCaseParameters(object[] args) : base(args)
		{
		}

		// Token: 0x06000530 RID: 1328 RVA: 0x00012834 File Offset: 0x00010A34
		public TestCaseParameters(ITestCaseData data) : base(data)
		{
			if (data.HasExpectedResult)
			{
				this.ExpectedResult = data.ExpectedResult;
			}
		}

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x06000531 RID: 1329 RVA: 0x00012868 File Offset: 0x00010A68
		// (set) Token: 0x06000532 RID: 1330 RVA: 0x00012880 File Offset: 0x00010A80
		public object ExpectedResult
		{
			get
			{
				return this._expectedResult;
			}
			set
			{
				this._expectedResult = value;
				this.HasExpectedResult = true;
			}
		}

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x06000533 RID: 1331 RVA: 0x00012894 File Offset: 0x00010A94
		// (set) Token: 0x06000534 RID: 1332 RVA: 0x000128AB File Offset: 0x00010AAB
		public bool HasExpectedResult
		{
			[CompilerGenerated]
			get
			{
				return this.<HasExpectedResult>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<HasExpectedResult>k__BackingField = value;
			}
		}

		// Token: 0x0400010F RID: 271
		private object _expectedResult;

		// Token: 0x04000110 RID: 272
		[CompilerGenerated]
		private bool <HasExpectedResult>k__BackingField;
	}
}
