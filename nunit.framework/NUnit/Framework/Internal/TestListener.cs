﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000CC RID: 204
	public class TestListener : ITestListener
	{
		// Token: 0x06000630 RID: 1584 RVA: 0x000158D3 File Offset: 0x00013AD3
		public void TestStarted(ITest test)
		{
		}

		// Token: 0x06000631 RID: 1585 RVA: 0x000158D6 File Offset: 0x00013AD6
		public void TestFinished(ITestResult result)
		{
		}

		// Token: 0x06000632 RID: 1586 RVA: 0x000158D9 File Offset: 0x00013AD9
		public void TestOutput(TestOutput output)
		{
		}

		// Token: 0x06000633 RID: 1587 RVA: 0x000158DC File Offset: 0x00013ADC
		private TestListener()
		{
		}

		// Token: 0x17000163 RID: 355
		// (get) Token: 0x06000634 RID: 1588 RVA: 0x000158E8 File Offset: 0x00013AE8
		public static ITestListener NULL
		{
			get
			{
				return new TestListener();
			}
		}
	}
}
