﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal.Commands;

namespace NUnit.Framework.Internal.Execution
{
	// Token: 0x020000C3 RID: 195
	public class CompositeWorkItem : WorkItem
	{
		// Token: 0x17000151 RID: 337
		// (get) Token: 0x060005EB RID: 1515 RVA: 0x00014338 File Offset: 0x00012538
		// (set) Token: 0x060005EC RID: 1516 RVA: 0x00014350 File Offset: 0x00012550
		public List<WorkItem> Children
		{
			get
			{
				return this._children;
			}
			private set
			{
				this._children = value;
			}
		}

		// Token: 0x060005ED RID: 1517 RVA: 0x0001435C File Offset: 0x0001255C
		public CompositeWorkItem(TestSuite suite, ITestFilter childFilter) : base(suite)
		{
			this._suite = suite;
			this._suiteResult = (base.Result as TestSuiteResult);
			this._childFilter = childFilter;
			this._countOrder = 0;
		}

		// Token: 0x060005EE RID: 1518 RVA: 0x000143B0 File Offset: 0x000125B0
		protected override void PerformWork()
		{
			this.InitializeSetUpAndTearDownCommands();
			if (!this.CheckForCancellation())
			{
				if (base.Test.RunState == RunState.Explicit && !this._childFilter.IsExplicitMatch(base.Test))
				{
					this.SkipFixture(ResultState.Explicit, this.GetSkipReason(), null);
				}
				else
				{
					switch (base.Test.RunState)
					{
					case RunState.NotRunnable:
						this.SkipFixture(ResultState.NotRunnable, this.GetSkipReason(), this.GetProviderStackTrace());
						break;
					default:
						base.Result.SetResult(ResultState.Success);
						this.CreateChildWorkItems();
						if (this._children.Count > 0)
						{
							this.PerformOneTimeSetUp();
							if (!this.CheckForCancellation())
							{
								switch (base.Result.ResultState.Status)
								{
								case TestStatus.Inconclusive:
								case TestStatus.Skipped:
								case TestStatus.Failed:
									this.SkipChildren(this._suite, base.Result.ResultState.WithSite(FailureSite.Parent), "OneTimeSetUp: " + base.Result.Message);
									break;
								case TestStatus.Passed:
									this.RunChildren();
									return;
								}
							}
							if (base.Context.ExecutionStatus != TestExecutionStatus.AbortRequested)
							{
								this.PerformOneTimeTearDown();
							}
						}
						break;
					case RunState.Skipped:
						this.SkipFixture(ResultState.Skipped, this.GetSkipReason(), null);
						break;
					case RunState.Ignored:
						this.SkipFixture(ResultState.Ignored, this.GetSkipReason(), null);
						break;
					}
				}
			}
			base.WorkItemComplete();
		}

		// Token: 0x060005EF RID: 1519 RVA: 0x0001454C File Offset: 0x0001274C
		private bool CheckForCancellation()
		{
			bool result;
			if (base.Context.ExecutionStatus != TestExecutionStatus.Running)
			{
				base.Result.SetResult(ResultState.Cancelled, "Test cancelled by user");
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060005F0 RID: 1520 RVA: 0x0001458C File Offset: 0x0001278C
		private void InitializeSetUpAndTearDownCommands()
		{
			List<SetUpTearDownItem> list = (this._suite.TypeInfo != null) ? CommandBuilder.BuildSetUpTearDownList(this._suite.TypeInfo.Type, typeof(OneTimeSetUpAttribute), typeof(OneTimeTearDownAttribute)) : new List<SetUpTearDownItem>();
			List<TestActionItem> list2 = new List<TestActionItem>();
			foreach (ITestAction testAction in base.Actions)
			{
				bool flag = (testAction.Targets & ActionTargets.Suite) == ActionTargets.Suite || (testAction.Targets == ActionTargets.Default && !(base.Test is ParameterizedMethodSuite));
				bool flag2 = (testAction.Targets & ActionTargets.Test) == ActionTargets.Test && !(base.Test is ParameterizedMethodSuite);
				if (flag)
				{
					list2.Add(new TestActionItem(testAction));
				}
				if (flag2)
				{
					base.Context.UpstreamActions.Add(testAction);
				}
			}
			this._setupCommand = CommandBuilder.MakeOneTimeSetUpCommand(this._suite, list, list2);
			this._teardownCommand = CommandBuilder.MakeOneTimeTearDownCommand(this._suite, list, list2);
		}

		// Token: 0x060005F1 RID: 1521 RVA: 0x000146D4 File Offset: 0x000128D4
		private void PerformOneTimeSetUp()
		{
			try
			{
				this._setupCommand.Execute(base.Context);
				base.Context.UpdateContextFromEnvironment();
			}
			catch (Exception innerException)
			{
				if (innerException is NUnitException || innerException is TargetInvocationException)
				{
					innerException = innerException.InnerException;
				}
				base.Result.RecordException(innerException, FailureSite.SetUp);
			}
		}

		// Token: 0x060005F2 RID: 1522 RVA: 0x0001474C File Offset: 0x0001294C
		private void RunChildren()
		{
			int num = this._children.Count;
			if (num == 0)
			{
				throw new InvalidOperationException("RunChildren called but item has no children");
			}
			this._childTestCountdown = new CountdownEvent(num);
			foreach (WorkItem workItem in this._children)
			{
				if (this.CheckForCancellation())
				{
					break;
				}
				workItem.Completed += this.OnChildCompleted;
				workItem.InitializeContext(new TestExecutionContext(base.Context));
				base.Context.Dispatcher.Dispatch(workItem);
				num--;
			}
			if (num > 0)
			{
				while (num-- > 0)
				{
					this.CountDownChildTest();
				}
			}
		}

		// Token: 0x060005F3 RID: 1523 RVA: 0x0001483C File Offset: 0x00012A3C
		private void CreateChildWorkItems()
		{
			this._children = new List<WorkItem>();
			foreach (ITest test in this._suite.Tests)
			{
				if (this._childFilter.Pass(test))
				{
					WorkItem workItem = WorkItem.CreateWorkItem(test, this._childFilter);
					workItem.WorkerId = base.WorkerId;
					if (workItem.TargetApartment == ApartmentState.Unknown && base.TargetApartment != ApartmentState.Unknown)
					{
						workItem.TargetApartment = base.TargetApartment;
					}
					if (test.Properties.ContainsKey("Order"))
					{
						this._children.Insert(0, workItem);
						this._countOrder++;
					}
					else
					{
						this._children.Add(workItem);
					}
				}
			}
			if (this._countOrder != 0)
			{
				this.SortChildren();
			}
		}

		// Token: 0x060005F4 RID: 1524 RVA: 0x00014958 File Offset: 0x00012B58
		private void SortChildren()
		{
			this._children.Sort(0, this._countOrder, new CompositeWorkItem.WorkItemOrderComparer());
		}

		// Token: 0x060005F5 RID: 1525 RVA: 0x00014973 File Offset: 0x00012B73
		private void SkipFixture(ResultState resultState, string message, string stackTrace)
		{
			base.Result.SetResult(resultState.WithSite(FailureSite.SetUp), message, StackFilter.Filter(stackTrace));
			this.SkipChildren(this._suite, resultState.WithSite(FailureSite.Parent), "OneTimeSetUp: " + message);
		}

		// Token: 0x060005F6 RID: 1526 RVA: 0x000149B0 File Offset: 0x00012BB0
		private void SkipChildren(TestSuite suite, ResultState resultState, string message)
		{
			foreach (ITest test in suite.Tests)
			{
				Test test2 = (Test)test;
				if (this._childFilter.Pass(test2))
				{
					TestResult testResult = test2.MakeTestResult();
					testResult.SetResult(resultState, message);
					this._suiteResult.AddResult(testResult);
					base.Context.Listener.TestFinished(testResult);
					if (test2.IsSuite)
					{
						this.SkipChildren((TestSuite)test2, resultState, message);
					}
				}
			}
		}

		// Token: 0x060005F7 RID: 1527 RVA: 0x00014A68 File Offset: 0x00012C68
		private void PerformOneTimeTearDown()
		{
			base.Context.EstablishExecutionEnvironment();
			this._teardownCommand.Execute(base.Context);
		}

		// Token: 0x060005F8 RID: 1528 RVA: 0x00014A8C File Offset: 0x00012C8C
		private string GetSkipReason()
		{
			return (string)base.Test.Properties.Get("_SKIPREASON");
		}

		// Token: 0x060005F9 RID: 1529 RVA: 0x00014AB8 File Offset: 0x00012CB8
		private string GetProviderStackTrace()
		{
			return (string)base.Test.Properties.Get("_PROVIDERSTACKTRACE");
		}

		// Token: 0x060005FA RID: 1530 RVA: 0x00014AE4 File Offset: 0x00012CE4
		private void OnChildCompleted(object sender, EventArgs e)
		{
			lock (this._completionLock)
			{
				WorkItem workItem = sender as WorkItem;
				if (workItem != null)
				{
					workItem.Completed -= this.OnChildCompleted;
					this._suiteResult.AddResult(workItem.Result);
					if (base.Context.StopOnError && workItem.Result.ResultState.Status == TestStatus.Failed)
					{
						base.Context.ExecutionStatus = TestExecutionStatus.StopRequested;
					}
					this.CountDownChildTest();
				}
			}
		}

		// Token: 0x060005FB RID: 1531 RVA: 0x00014B94 File Offset: 0x00012D94
		private void CountDownChildTest()
		{
			this._childTestCountdown.Signal();
			if (this._childTestCountdown.CurrentCount == 0)
			{
				if (base.Context.ExecutionStatus != TestExecutionStatus.AbortRequested)
				{
					this.PerformOneTimeTearDown();
				}
				foreach (ITestResult testResult in this._suiteResult.Children)
				{
					if (testResult.ResultState == ResultState.Cancelled)
					{
						base.Result.SetResult(ResultState.Cancelled, "Cancelled by user");
						break;
					}
				}
				base.WorkItemComplete();
			}
		}

		// Token: 0x060005FC RID: 1532 RVA: 0x00014C60 File Offset: 0x00012E60
		private static bool IsStaticClass(Type type)
		{
			return type.GetTypeInfo().IsAbstract && type.GetTypeInfo().IsSealed;
		}

		// Token: 0x060005FD RID: 1533 RVA: 0x00014C90 File Offset: 0x00012E90
		public override void Cancel(bool force)
		{
			lock (this.cancelLock)
			{
				if (this._children != null)
				{
					foreach (WorkItem workItem in this._children)
					{
						TestExecutionContext context = workItem.Context;
						if (context != null)
						{
							context.ExecutionStatus = (force ? TestExecutionStatus.AbortRequested : TestExecutionStatus.StopRequested);
						}
						if (workItem.State == WorkItemState.Running)
						{
							workItem.Cancel(force);
						}
					}
				}
			}
		}

		// Token: 0x0400013A RID: 314
		private TestSuite _suite;

		// Token: 0x0400013B RID: 315
		private TestSuiteResult _suiteResult;

		// Token: 0x0400013C RID: 316
		private ITestFilter _childFilter;

		// Token: 0x0400013D RID: 317
		private TestCommand _setupCommand;

		// Token: 0x0400013E RID: 318
		private TestCommand _teardownCommand;

		// Token: 0x0400013F RID: 319
		private List<WorkItem> _children;

		// Token: 0x04000140 RID: 320
		private int _countOrder;

		// Token: 0x04000141 RID: 321
		private CountdownEvent _childTestCountdown;

		// Token: 0x04000142 RID: 322
		private object _completionLock = new object();

		// Token: 0x04000143 RID: 323
		private object cancelLock = new object();

		// Token: 0x020000C4 RID: 196
		private class WorkItemOrderComparer : IComparer<WorkItem>
		{
			// Token: 0x060005FE RID: 1534 RVA: 0x00014D54 File Offset: 0x00012F54
			public int Compare(WorkItem x, WorkItem y)
			{
				int num = int.MaxValue;
				int value = int.MaxValue;
				if (x.Test.Properties.ContainsKey("Order"))
				{
					num = (int)x.Test.Properties["Order"][0];
				}
				if (y.Test.Properties.ContainsKey("Order"))
				{
					value = (int)y.Test.Properties["Order"][0];
				}
				return num.CompareTo(value);
			}

			// Token: 0x060005FF RID: 1535 RVA: 0x00014DF4 File Offset: 0x00012FF4
			public WorkItemOrderComparer()
			{
			}
		}
	}
}
