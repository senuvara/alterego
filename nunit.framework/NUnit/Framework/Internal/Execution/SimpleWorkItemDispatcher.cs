﻿using System;
using System.Threading;

namespace NUnit.Framework.Internal.Execution
{
	// Token: 0x02000125 RID: 293
	public class SimpleWorkItemDispatcher : IWorkItemDispatcher
	{
		// Token: 0x060007D6 RID: 2006 RVA: 0x0001BB98 File Offset: 0x00019D98
		public void Dispatch(WorkItem work)
		{
			if (this._topLevelWorkItem != null)
			{
				work.Execute();
			}
			else
			{
				this._topLevelWorkItem = work;
				this._runnerThread = new Thread(new ThreadStart(this.RunnerThreadProc));
				if (work.TargetApartment == ApartmentState.STA)
				{
					this._runnerThread.SetApartmentState(ApartmentState.STA);
				}
				this._runnerThread.Start();
			}
		}

		// Token: 0x060007D7 RID: 2007 RVA: 0x0001BC05 File Offset: 0x00019E05
		private void RunnerThreadProc()
		{
			this._topLevelWorkItem.Execute();
		}

		// Token: 0x060007D8 RID: 2008 RVA: 0x0001BC14 File Offset: 0x00019E14
		public void CancelRun(bool force)
		{
			lock (this.cancelLock)
			{
				if (this._topLevelWorkItem != null)
				{
					this._topLevelWorkItem.Cancel(force);
					if (force)
					{
						this._topLevelWorkItem = null;
					}
				}
			}
		}

		// Token: 0x060007D9 RID: 2009 RVA: 0x0001BC78 File Offset: 0x00019E78
		public SimpleWorkItemDispatcher()
		{
		}

		// Token: 0x040001B9 RID: 441
		private WorkItem _topLevelWorkItem;

		// Token: 0x040001BA RID: 442
		private Thread _runnerThread;

		// Token: 0x040001BB RID: 443
		private object cancelLock = new object();
	}
}
