﻿using System;
using System.IO;
using System.Text;

namespace NUnit.Framework.Internal.Execution
{
	// Token: 0x02000123 RID: 291
	public class TextCapture : TextWriter
	{
		// Token: 0x060007CF RID: 1999 RVA: 0x0001BA8C File Offset: 0x00019C8C
		public TextCapture(TextWriter defaultWriter)
		{
			this._defaultWriter = defaultWriter;
		}

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x060007D0 RID: 2000 RVA: 0x0001BAA0 File Offset: 0x00019CA0
		public override Encoding Encoding
		{
			get
			{
				return this._defaultWriter.Encoding;
			}
		}

		// Token: 0x060007D1 RID: 2001 RVA: 0x0001BAC0 File Offset: 0x00019CC0
		public override void Write(char value)
		{
			TestExecutionContext testExecutionContext = TestExecutionContext.GetTestExecutionContext();
			if (testExecutionContext != null && testExecutionContext.CurrentResult != null)
			{
				testExecutionContext.CurrentResult.OutWriter.Write(value);
			}
			else
			{
				this._defaultWriter.Write(value);
			}
		}

		// Token: 0x060007D2 RID: 2002 RVA: 0x0001BB08 File Offset: 0x00019D08
		public override void Write(string value)
		{
			TestExecutionContext testExecutionContext = TestExecutionContext.GetTestExecutionContext();
			if (testExecutionContext != null && testExecutionContext.CurrentResult != null)
			{
				testExecutionContext.CurrentResult.OutWriter.Write(value);
			}
			else
			{
				this._defaultWriter.Write(value);
			}
		}

		// Token: 0x060007D3 RID: 2003 RVA: 0x0001BB50 File Offset: 0x00019D50
		public override void WriteLine(string value)
		{
			TestExecutionContext testExecutionContext = TestExecutionContext.GetTestExecutionContext();
			if (testExecutionContext != null && testExecutionContext.CurrentResult != null)
			{
				testExecutionContext.CurrentResult.OutWriter.WriteLine(value);
			}
			else
			{
				this._defaultWriter.WriteLine(value);
			}
		}

		// Token: 0x040001B8 RID: 440
		private TextWriter _defaultWriter;
	}
}
