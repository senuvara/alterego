﻿using System;
using System.IO;
using System.Text;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Execution
{
	// Token: 0x0200015E RID: 350
	public class EventListenerTextWriter : TextWriter
	{
		// Token: 0x06000923 RID: 2339 RVA: 0x0001ED77 File Offset: 0x0001CF77
		public EventListenerTextWriter(string streamName, TextWriter defaultWriter)
		{
			this._streamName = streamName;
			this._defaultWriter = defaultWriter;
		}

		// Token: 0x06000924 RID: 2340 RVA: 0x0001ED90 File Offset: 0x0001CF90
		public override void Write(char aChar)
		{
			if (!this.TrySendToListener(aChar.ToString()))
			{
				this._defaultWriter.Write(aChar);
			}
		}

		// Token: 0x06000925 RID: 2341 RVA: 0x0001EDBC File Offset: 0x0001CFBC
		public override void Write(string aString)
		{
			if (!this.TrySendToListener(aString))
			{
				this._defaultWriter.Write(aString);
			}
		}

		// Token: 0x06000926 RID: 2342 RVA: 0x0001EDE4 File Offset: 0x0001CFE4
		public override void WriteLine(string aString)
		{
			if (!this.TrySendToListener(aString + Environment.NewLine))
			{
				this._defaultWriter.WriteLine(aString);
			}
		}

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000927 RID: 2343 RVA: 0x0001EE14 File Offset: 0x0001D014
		public override Encoding Encoding
		{
			get
			{
				return Encoding.Default;
			}
		}

		// Token: 0x06000928 RID: 2344 RVA: 0x0001EE2C File Offset: 0x0001D02C
		private bool TrySendToListener(string text)
		{
			TestExecutionContext testExecutionContext = TestExecutionContext.GetTestExecutionContext();
			bool result;
			if (testExecutionContext == null || testExecutionContext.Listener == null)
			{
				result = false;
			}
			else
			{
				string testName = (testExecutionContext.CurrentTest != null) ? testExecutionContext.CurrentTest.FullName : null;
				testExecutionContext.Listener.TestOutput(new TestOutput(text, this._streamName, testName));
				result = true;
			}
			return result;
		}

		// Token: 0x04000206 RID: 518
		private TextWriter _defaultWriter;

		// Token: 0x04000207 RID: 519
		private string _streamName;
	}
}
