﻿using System;
using System.Threading;

namespace NUnit.Framework.Internal.Execution
{
	// Token: 0x0200009E RID: 158
	public class CountdownEvent
	{
		// Token: 0x060004D5 RID: 1237 RVA: 0x0000FFA0 File Offset: 0x0000E1A0
		public CountdownEvent(int initialCount)
		{
			this._remainingCount = initialCount;
			this._initialCount = initialCount;
		}

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x060004D6 RID: 1238 RVA: 0x0000FFE0 File Offset: 0x0000E1E0
		public int InitialCount
		{
			get
			{
				return this._initialCount;
			}
		}

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x060004D7 RID: 1239 RVA: 0x0000FFF8 File Offset: 0x0000E1F8
		public int CurrentCount
		{
			get
			{
				return this._remainingCount;
			}
		}

		// Token: 0x060004D8 RID: 1240 RVA: 0x00010010 File Offset: 0x0000E210
		public void Signal()
		{
			lock (this._lock)
			{
				if (--this._remainingCount == 0)
				{
					this._event.Set();
				}
			}
		}

		// Token: 0x060004D9 RID: 1241 RVA: 0x00010070 File Offset: 0x0000E270
		public void Wait()
		{
			this._event.WaitOne();
		}

		// Token: 0x040000E6 RID: 230
		private int _initialCount;

		// Token: 0x040000E7 RID: 231
		private int _remainingCount;

		// Token: 0x040000E8 RID: 232
		private object _lock = new object();

		// Token: 0x040000E9 RID: 233
		private ManualResetEvent _event = new ManualResetEvent(false);
	}
}
