﻿using System;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal.Commands;

namespace NUnit.Framework.Internal.Execution
{
	// Token: 0x020000DB RID: 219
	public static class CommandBuilder
	{
		// Token: 0x06000676 RID: 1654 RVA: 0x00016FC0 File Offset: 0x000151C0
		public static TestCommand MakeOneTimeSetUpCommand(TestSuite suite, List<SetUpTearDownItem> setUpTearDown, List<TestActionItem> actions)
		{
			TestCommand result;
			if (suite.RunState != RunState.Runnable && suite.RunState != RunState.Explicit)
			{
				result = CommandBuilder.MakeSkipCommand(suite);
			}
			else
			{
				TestCommand testCommand = new OneTimeSetUpCommand(suite, setUpTearDown, actions);
				IList<IApplyToContext> list = null;
				if (suite.TypeInfo != null)
				{
					list = suite.TypeInfo.GetCustomAttributes<IApplyToContext>(true);
				}
				else if (suite.Method != null)
				{
					list = suite.Method.GetCustomAttributes<IApplyToContext>(true);
				}
				else
				{
					TestAssembly testAssembly = suite as TestAssembly;
					if (testAssembly != null)
					{
						list = (IApplyToContext[])testAssembly.Assembly.GetCustomAttributes(typeof(IApplyToContext), true);
					}
				}
				if (list != null && list.Count > 0)
				{
					testCommand = new ApplyChangesToContextCommand(testCommand, list);
				}
				result = testCommand;
			}
			return result;
		}

		// Token: 0x06000677 RID: 1655 RVA: 0x00017090 File Offset: 0x00015290
		public static TestCommand MakeOneTimeTearDownCommand(TestSuite suite, List<SetUpTearDownItem> setUpTearDownItems, List<TestActionItem> actions)
		{
			TestCommand testCommand = new OneTimeTearDownCommand(suite, setUpTearDownItems, actions);
			if (suite.TestType == "Theory")
			{
				testCommand = new TheoryResultCommand(testCommand);
			}
			return testCommand;
		}

		// Token: 0x06000678 RID: 1656 RVA: 0x000170CC File Offset: 0x000152CC
		public static TestCommand MakeTestCommand(TestMethod test)
		{
			TestCommand testCommand = new TestMethodCommand(test);
			foreach (IWrapTestMethod wrapTestMethod in test.Method.GetCustomAttributes<IWrapTestMethod>(true))
			{
				testCommand = wrapTestMethod.Wrap(testCommand);
			}
			testCommand = new TestActionCommand(testCommand);
			testCommand = new SetUpTearDownCommand(testCommand);
			foreach (IWrapSetUpTearDown commandWrapper in test.Method.GetCustomAttributes<IWrapSetUpTearDown>(true))
			{
				testCommand = commandWrapper.Wrap(testCommand);
			}
			IApplyToContext[] customAttributes3 = test.Method.GetCustomAttributes<IApplyToContext>(true);
			if (customAttributes3.Length > 0)
			{
				testCommand = new ApplyChangesToContextCommand(testCommand, customAttributes3);
			}
			return testCommand;
		}

		// Token: 0x06000679 RID: 1657 RVA: 0x00017188 File Offset: 0x00015388
		public static SkipCommand MakeSkipCommand(Test test)
		{
			return new SkipCommand(test);
		}

		// Token: 0x0600067A RID: 1658 RVA: 0x000171A0 File Offset: 0x000153A0
		public static List<SetUpTearDownItem> BuildSetUpTearDownList(Type fixtureType, Type setUpType, Type tearDownType)
		{
			MethodInfo[] methodsWithAttribute = Reflect.GetMethodsWithAttribute(fixtureType, setUpType, true);
			MethodInfo[] methodsWithAttribute2 = Reflect.GetMethodsWithAttribute(fixtureType, tearDownType, true);
			List<SetUpTearDownItem> list = new List<SetUpTearDownItem>();
			while (fixtureType != null && !fixtureType.Equals(typeof(object)))
			{
				SetUpTearDownItem setUpTearDownItem = CommandBuilder.BuildNode(fixtureType, methodsWithAttribute, methodsWithAttribute2);
				if (setUpTearDownItem.HasMethods)
				{
					list.Add(setUpTearDownItem);
				}
				fixtureType = fixtureType.GetTypeInfo().BaseType;
			}
			return list;
		}

		// Token: 0x0600067B RID: 1659 RVA: 0x00017220 File Offset: 0x00015420
		private static SetUpTearDownItem BuildNode(Type fixtureType, IList<MethodInfo> setUpMethods, IList<MethodInfo> tearDownMethods)
		{
			List<MethodInfo> setUpMethods2 = CommandBuilder.SelectMethodsByDeclaringType(fixtureType, setUpMethods);
			List<MethodInfo> tearDownMethods2 = CommandBuilder.SelectMethodsByDeclaringType(fixtureType, tearDownMethods);
			return new SetUpTearDownItem(setUpMethods2, tearDownMethods2);
		}

		// Token: 0x0600067C RID: 1660 RVA: 0x0001724C File Offset: 0x0001544C
		private static List<MethodInfo> SelectMethodsByDeclaringType(Type type, IList<MethodInfo> methods)
		{
			List<MethodInfo> list = new List<MethodInfo>();
			foreach (MethodInfo methodInfo in methods)
			{
				if (methodInfo.DeclaringType == type)
				{
					list.Add(methodInfo);
				}
			}
			return list;
		}
	}
}
