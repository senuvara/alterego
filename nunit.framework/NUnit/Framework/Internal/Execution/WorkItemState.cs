﻿using System;

namespace NUnit.Framework.Internal.Execution
{
	// Token: 0x02000039 RID: 57
	public enum WorkItemState
	{
		// Token: 0x04000053 RID: 83
		Ready,
		// Token: 0x04000054 RID: 84
		Running,
		// Token: 0x04000055 RID: 85
		Complete
	}
}
