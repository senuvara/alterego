﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Execution
{
	// Token: 0x0200003A RID: 58
	public abstract class WorkItem
	{
		// Token: 0x060001BA RID: 442 RVA: 0x00006A10 File Offset: 0x00004C10
		public static WorkItem CreateWorkItem(ITest test, ITestFilter filter)
		{
			TestSuite testSuite = test as TestSuite;
			WorkItem result;
			if (testSuite != null)
			{
				result = new CompositeWorkItem(testSuite, filter);
			}
			else
			{
				result = new SimpleWorkItem((TestMethod)test, filter);
			}
			return result;
		}

		// Token: 0x060001BB RID: 443 RVA: 0x00006A48 File Offset: 0x00004C48
		public WorkItem(Test test)
		{
			this.Test = test;
			this.Result = test.MakeTestResult();
			this.State = WorkItemState.Ready;
			this.Actions = new List<ITestAction>();
			this.TargetApartment = (this.Test.Properties.ContainsKey("ApartmentState") ? ((ApartmentState)this.Test.Properties.Get("ApartmentState")) : ApartmentState.Unknown);
		}

		// Token: 0x060001BC RID: 444 RVA: 0x00006AD0 File Offset: 0x00004CD0
		public void InitializeContext(TestExecutionContext context)
		{
			Guard.OperationValid(this.Context == null, "The context has already been initialized");
			this.Context = context;
			if (this.Test is TestAssembly)
			{
				this.Actions.AddRange(ActionsHelper.GetActionsFromAttributeProvider(((TestAssembly)this.Test).Assembly));
			}
			else if (this.Test is ParameterizedMethodSuite)
			{
				this.Actions.AddRange(ActionsHelper.GetActionsFromAttributeProvider(this.Test.Method.MethodInfo));
			}
			else if (this.Test.TypeInfo != null)
			{
				this.Actions.AddRange(ActionsHelper.GetActionsFromTypesAttributes(this.Test.TypeInfo.Type));
			}
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x060001BD RID: 445 RVA: 0x00006BA0 File Offset: 0x00004DA0
		// (remove) Token: 0x060001BE RID: 446 RVA: 0x00006BDC File Offset: 0x00004DDC
		public event EventHandler Completed
		{
			add
			{
				EventHandler eventHandler = this.Completed;
				EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler value2 = (EventHandler)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler>(ref this.Completed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			remove
			{
				EventHandler eventHandler = this.Completed;
				EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler value2 = (EventHandler)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler>(ref this.Completed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x060001BF RID: 447 RVA: 0x00006C18 File Offset: 0x00004E18
		// (set) Token: 0x060001C0 RID: 448 RVA: 0x00006C2F File Offset: 0x00004E2F
		public WorkItemState State
		{
			[CompilerGenerated]
			get
			{
				return this.<State>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<State>k__BackingField = value;
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x060001C1 RID: 449 RVA: 0x00006C38 File Offset: 0x00004E38
		// (set) Token: 0x060001C2 RID: 450 RVA: 0x00006C4F File Offset: 0x00004E4F
		public Test Test
		{
			[CompilerGenerated]
			get
			{
				return this.<Test>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Test>k__BackingField = value;
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x060001C3 RID: 451 RVA: 0x00006C58 File Offset: 0x00004E58
		// (set) Token: 0x060001C4 RID: 452 RVA: 0x00006C6F File Offset: 0x00004E6F
		public TestExecutionContext Context
		{
			[CompilerGenerated]
			get
			{
				return this.<Context>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Context>k__BackingField = value;
			}
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x060001C5 RID: 453 RVA: 0x00006C78 File Offset: 0x00004E78
		// (set) Token: 0x060001C6 RID: 454 RVA: 0x00006C8F File Offset: 0x00004E8F
		public string WorkerId
		{
			[CompilerGenerated]
			get
			{
				return this.<WorkerId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<WorkerId>k__BackingField = value;
			}
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x060001C7 RID: 455 RVA: 0x00006C98 File Offset: 0x00004E98
		// (set) Token: 0x060001C8 RID: 456 RVA: 0x00006CAF File Offset: 0x00004EAF
		public List<ITestAction> Actions
		{
			[CompilerGenerated]
			get
			{
				return this.<Actions>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Actions>k__BackingField = value;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x060001C9 RID: 457 RVA: 0x00006CB8 File Offset: 0x00004EB8
		// (set) Token: 0x060001CA RID: 458 RVA: 0x00006CCF File Offset: 0x00004ECF
		public TestResult Result
		{
			[CompilerGenerated]
			get
			{
				return this.<Result>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<Result>k__BackingField = value;
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x060001CB RID: 459 RVA: 0x00006CD8 File Offset: 0x00004ED8
		// (set) Token: 0x060001CC RID: 460 RVA: 0x00006CEF File Offset: 0x00004EEF
		internal ApartmentState TargetApartment
		{
			[CompilerGenerated]
			get
			{
				return this.<TargetApartment>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TargetApartment>k__BackingField = value;
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x060001CD RID: 461 RVA: 0x00006CF8 File Offset: 0x00004EF8
		// (set) Token: 0x060001CE RID: 462 RVA: 0x00006D0F File Offset: 0x00004F0F
		private ApartmentState CurrentApartment
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentApartment>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CurrentApartment>k__BackingField = value;
			}
		}

		// Token: 0x060001CF RID: 463 RVA: 0x00006D18 File Offset: 0x00004F18
		public virtual void Execute()
		{
			int num = this.Context.TestCaseTimeout;
			if (this.Test.Properties.ContainsKey("Timeout"))
			{
				num = (int)this.Test.Properties.Get("Timeout");
			}
			WorkItem.OwnThreadReason ownThreadReason = WorkItem.OwnThreadReason.NotNeeded;
			if (this.Test.RequiresThread)
			{
				ownThreadReason |= WorkItem.OwnThreadReason.RequiresThread;
			}
			if (num > 0 && this.Test is TestMethod)
			{
				ownThreadReason |= WorkItem.OwnThreadReason.Timeout;
			}
			this.CurrentApartment = Thread.CurrentThread.GetApartmentState();
			if (this.CurrentApartment != this.TargetApartment && this.TargetApartment != ApartmentState.Unknown)
			{
				ownThreadReason |= WorkItem.OwnThreadReason.DifferentApartment;
			}
			ownThreadReason = WorkItem.OwnThreadReason.NotNeeded;
			if (ownThreadReason == WorkItem.OwnThreadReason.NotNeeded)
			{
				this.RunTest();
			}
			else if (this.Context.IsSingleThreaded)
			{
				string message = "Test is not runnable in single-threaded context. " + ownThreadReason;
				WorkItem.log.Error(message);
				this.Result.SetResult(ResultState.NotRunnable, message);
				this.WorkItemComplete();
			}
			else
			{
				WorkItem.log.Debug("Running test on own thread. " + ownThreadReason);
				ApartmentState apartment = ((ownThreadReason | WorkItem.OwnThreadReason.DifferentApartment) != WorkItem.OwnThreadReason.NotNeeded) ? this.TargetApartment : this.CurrentApartment;
				this.RunTestOnOwnThread(num, apartment);
			}
		}

		// Token: 0x060001D0 RID: 464 RVA: 0x00006E7F File Offset: 0x0000507F
		private void RunTestOnOwnThread(int timeout, ApartmentState apartment)
		{
			this.thread = new Thread(new ThreadStart(this.RunTest));
			this.thread.SetApartmentState(apartment);
			this.RunThread(timeout);
		}

		// Token: 0x060001D1 RID: 465 RVA: 0x00006EB0 File Offset: 0x000050B0
		private void RunThread(int timeout)
		{
			this.thread.CurrentCulture = this.Context.CurrentCulture;
			this.thread.CurrentUICulture = this.Context.CurrentUICulture;
			this.thread.Start();
			if (timeout <= 0)
			{
				timeout = -1;
			}
			if (!this.thread.Join(timeout))
			{
				if (Debugger.IsAttached)
				{
					this.thread.Join();
				}
				else
				{
					Thread thread;
					lock (this.threadLock)
					{
						if (this.thread == null)
						{
							return;
						}
						thread = this.thread;
						this.thread = null;
					}
					if (this.Context.ExecutionStatus != TestExecutionStatus.AbortRequested)
					{
						WorkItem.log.Debug("Killing thread {0}, which exceeded timeout", new object[]
						{
							thread.ManagedThreadId
						});
						ThreadUtility.Kill(thread);
						thread.Join();
						WorkItem.log.Debug("Changing result from {0} to Timeout Failure", new object[]
						{
							this.Result.ResultState
						});
						this.Result.SetResult(ResultState.Failure, string.Format("Test exceeded Timeout value of {0}ms", timeout));
						this.WorkItemComplete();
					}
				}
			}
		}

		// Token: 0x060001D2 RID: 466 RVA: 0x00007024 File Offset: 0x00005224
		private void RunTest()
		{
			this.Context.CurrentTest = this.Test;
			this.Context.CurrentResult = this.Result;
			this.Context.Listener.TestStarted(this.Test);
			this.Context.StartTime = DateTime.UtcNow;
			this.Context.StartTicks = Stopwatch.GetTimestamp();
			this.Context.WorkerId = this.WorkerId;
			this.Context.EstablishExecutionEnvironment();
			this.State = WorkItemState.Running;
			this.PerformWork();
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x000070BC File Offset: 0x000052BC
		public virtual void Cancel(bool force)
		{
			if (this.Context != null)
			{
				this.Context.ExecutionStatus = (force ? TestExecutionStatus.AbortRequested : TestExecutionStatus.StopRequested);
			}
			if (force)
			{
				Thread thread;
				lock (this.threadLock)
				{
					if (this.thread == null)
					{
						return;
					}
					thread = this.thread;
					this.thread = null;
				}
				if (!thread.Join(0))
				{
					WorkItem.log.Debug("Killing thread {0} for cancel", new object[]
					{
						thread.ManagedThreadId
					});
					ThreadUtility.Kill(thread);
					thread.Join();
					WorkItem.log.Debug("Changing result from {0} to Cancelled", new object[]
					{
						this.Result.ResultState
					});
					this.Result.SetResult(ResultState.Cancelled, "Cancelled by user");
					this.WorkItemComplete();
				}
			}
		}

		// Token: 0x060001D4 RID: 468
		protected abstract void PerformWork();

		// Token: 0x060001D5 RID: 469 RVA: 0x000071C8 File Offset: 0x000053C8
		protected void WorkItemComplete()
		{
			this.State = WorkItemState.Complete;
			this.Result.StartTime = this.Context.StartTime;
			this.Result.EndTime = DateTime.UtcNow;
			long num = Stopwatch.GetTimestamp() - this.Context.StartTicks;
			double duration = (double)num / (double)Stopwatch.Frequency;
			this.Result.Duration = duration;
			this.Result.AssertCount += this.Context.AssertCount;
			this.Context.Listener.TestFinished(this.Result);
			if (this.Completed != null)
			{
				this.Completed(this, EventArgs.Empty);
			}
			this.Context.TestObject = null;
			this.Test.Fixture = null;
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x0000729D File Offset: 0x0000549D
		// Note: this type is marked as 'beforefieldinit'.
		static WorkItem()
		{
		}

		// Token: 0x04000056 RID: 86
		private static Logger log = InternalTrace.GetLogger("WorkItem");

		// Token: 0x04000057 RID: 87
		private EventHandler Completed;

		// Token: 0x04000058 RID: 88
		private Thread thread;

		// Token: 0x04000059 RID: 89
		private object threadLock = new object();

		// Token: 0x0400005A RID: 90
		[CompilerGenerated]
		private WorkItemState <State>k__BackingField;

		// Token: 0x0400005B RID: 91
		[CompilerGenerated]
		private Test <Test>k__BackingField;

		// Token: 0x0400005C RID: 92
		[CompilerGenerated]
		private TestExecutionContext <Context>k__BackingField;

		// Token: 0x0400005D RID: 93
		[CompilerGenerated]
		private string <WorkerId>k__BackingField;

		// Token: 0x0400005E RID: 94
		[CompilerGenerated]
		private List<ITestAction> <Actions>k__BackingField;

		// Token: 0x0400005F RID: 95
		[CompilerGenerated]
		private TestResult <Result>k__BackingField;

		// Token: 0x04000060 RID: 96
		[CompilerGenerated]
		private ApartmentState <TargetApartment>k__BackingField;

		// Token: 0x04000061 RID: 97
		[CompilerGenerated]
		private ApartmentState <CurrentApartment>k__BackingField;

		// Token: 0x0200003B RID: 59
		[Flags]
		private enum OwnThreadReason
		{
			// Token: 0x04000063 RID: 99
			NotNeeded = 0,
			// Token: 0x04000064 RID: 100
			RequiresThread = 1,
			// Token: 0x04000065 RID: 101
			Timeout = 2,
			// Token: 0x04000066 RID: 102
			DifferentApartment = 4
		}
	}
}
