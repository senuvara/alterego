﻿using System;

namespace NUnit.Framework.Internal.Execution
{
	// Token: 0x02000124 RID: 292
	public interface IWorkItemDispatcher
	{
		// Token: 0x060007D4 RID: 2004
		void Dispatch(WorkItem work);

		// Token: 0x060007D5 RID: 2005
		void CancelRun(bool force);
	}
}
