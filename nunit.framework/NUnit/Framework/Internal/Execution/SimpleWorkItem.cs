﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal.Commands;

namespace NUnit.Framework.Internal.Execution
{
	// Token: 0x02000082 RID: 130
	public class SimpleWorkItem : WorkItem
	{
		// Token: 0x06000471 RID: 1137 RVA: 0x0000EBFD File Offset: 0x0000CDFD
		public SimpleWorkItem(TestMethod test, ITestFilter filter) : base(test)
		{
			this._command = ((test.RunState == RunState.Runnable || (test.RunState == RunState.Explicit && filter.IsExplicitMatch(test))) ? CommandBuilder.MakeTestCommand(test) : CommandBuilder.MakeSkipCommand(test));
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x0000EC38 File Offset: 0x0000CE38
		protected override void PerformWork()
		{
			try
			{
				base.Result = this._command.Execute(base.Context);
			}
			finally
			{
				base.WorkItemComplete();
			}
		}

		// Token: 0x040000CD RID: 205
		private TestCommand _command;
	}
}
