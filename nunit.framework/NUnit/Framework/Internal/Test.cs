﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000020 RID: 32
	public abstract class Test : ITest, IXmlNodeBuilder, IComparable
	{
		// Token: 0x0600010D RID: 269 RVA: 0x00004830 File Offset: 0x00002A30
		protected Test(string name)
		{
			Guard.ArgumentNotNullOrEmpty(name, "name");
			this.Initialize(name);
		}

		// Token: 0x0600010E RID: 270 RVA: 0x0000484F File Offset: 0x00002A4F
		protected Test(string pathName, string name)
		{
			Guard.ArgumentNotNullOrEmpty(pathName, "pathName");
			this.Initialize(name);
			this.FullName = pathName + "." + name;
		}

		// Token: 0x0600010F RID: 271 RVA: 0x00004884 File Offset: 0x00002A84
		protected Test(ITypeInfo typeInfo)
		{
			this.Initialize(typeInfo.GetDisplayName());
			string @namespace = typeInfo.Namespace;
			if (@namespace != null && @namespace != "")
			{
				this.FullName = @namespace + "." + this.Name;
			}
			this.TypeInfo = typeInfo;
		}

		// Token: 0x06000110 RID: 272 RVA: 0x000048E8 File Offset: 0x00002AE8
		protected Test(IMethodInfo method)
		{
			this.Initialize(method.Name);
			this.Method = method;
			this.TypeInfo = method.TypeInfo;
			this.FullName = method.TypeInfo.FullName + "." + this.Name;
		}

		// Token: 0x06000111 RID: 273 RVA: 0x00004944 File Offset: 0x00002B44
		private void Initialize(string name)
		{
			this.Name = name;
			this.FullName = name;
			this.Id = Test.GetNextId();
			this.Properties = new PropertyBag();
			this.RunState = RunState.Runnable;
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00004984 File Offset: 0x00002B84
		private static string GetNextId()
		{
			return Test.IdPrefix + Test._nextID++;
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000113 RID: 275 RVA: 0x000049B4 File Offset: 0x00002BB4
		// (set) Token: 0x06000114 RID: 276 RVA: 0x000049CB File Offset: 0x00002BCB
		public string Id
		{
			[CompilerGenerated]
			get
			{
				return this.<Id>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Id>k__BackingField = value;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000115 RID: 277 RVA: 0x000049D4 File Offset: 0x00002BD4
		// (set) Token: 0x06000116 RID: 278 RVA: 0x000049EB File Offset: 0x00002BEB
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Name>k__BackingField = value;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000117 RID: 279 RVA: 0x000049F4 File Offset: 0x00002BF4
		// (set) Token: 0x06000118 RID: 280 RVA: 0x00004A0B File Offset: 0x00002C0B
		public string FullName
		{
			[CompilerGenerated]
			get
			{
				return this.<FullName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<FullName>k__BackingField = value;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000119 RID: 281 RVA: 0x00004A14 File Offset: 0x00002C14
		public string ClassName
		{
			get
			{
				ITypeInfo typeInfo = this.TypeInfo;
				if (this.Method != null)
				{
					if (this.DeclaringTypeInfo == null)
					{
						this.DeclaringTypeInfo = new TypeWrapper(this.Method.MethodInfo.DeclaringType);
					}
					typeInfo = this.DeclaringTypeInfo;
				}
				string result;
				if (typeInfo == null)
				{
					result = null;
				}
				else
				{
					result = (typeInfo.IsGenericType ? typeInfo.GetGenericTypeDefinition().FullName : typeInfo.FullName);
				}
				return result;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600011A RID: 282 RVA: 0x00004A98 File Offset: 0x00002C98
		public virtual string MethodName
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x0600011B RID: 283 RVA: 0x00004AAC File Offset: 0x00002CAC
		// (set) Token: 0x0600011C RID: 284 RVA: 0x00004AC3 File Offset: 0x00002CC3
		public ITypeInfo TypeInfo
		{
			[CompilerGenerated]
			get
			{
				return this.<TypeInfo>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<TypeInfo>k__BackingField = value;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x0600011D RID: 285 RVA: 0x00004ACC File Offset: 0x00002CCC
		// (set) Token: 0x0600011E RID: 286 RVA: 0x00004AE4 File Offset: 0x00002CE4
		public IMethodInfo Method
		{
			get
			{
				return this._method;
			}
			set
			{
				this.DeclaringTypeInfo = null;
				this._method = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x0600011F RID: 287 RVA: 0x00004AF8 File Offset: 0x00002CF8
		// (set) Token: 0x06000120 RID: 288 RVA: 0x00004B0F File Offset: 0x00002D0F
		public RunState RunState
		{
			[CompilerGenerated]
			get
			{
				return this.<RunState>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RunState>k__BackingField = value;
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000121 RID: 289
		public abstract string XmlElementName { get; }

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000122 RID: 290 RVA: 0x00004B18 File Offset: 0x00002D18
		public virtual string TestType
		{
			get
			{
				return base.GetType().Name;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000123 RID: 291 RVA: 0x00004B38 File Offset: 0x00002D38
		public virtual int TestCaseCount
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000124 RID: 292 RVA: 0x00004B4C File Offset: 0x00002D4C
		// (set) Token: 0x06000125 RID: 293 RVA: 0x00004B63 File Offset: 0x00002D63
		public IPropertyBag Properties
		{
			[CompilerGenerated]
			get
			{
				return this.<Properties>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Properties>k__BackingField = value;
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000126 RID: 294 RVA: 0x00004B6C File Offset: 0x00002D6C
		public bool IsSuite
		{
			get
			{
				return this is TestSuite;
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000127 RID: 295
		public abstract bool HasChildren { get; }

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000128 RID: 296 RVA: 0x00004B88 File Offset: 0x00002D88
		// (set) Token: 0x06000129 RID: 297 RVA: 0x00004B9F File Offset: 0x00002D9F
		public ITest Parent
		{
			[CompilerGenerated]
			get
			{
				return this.<Parent>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Parent>k__BackingField = value;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x0600012A RID: 298
		public abstract IList<ITest> Tests { get; }

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x0600012B RID: 299 RVA: 0x00004BA8 File Offset: 0x00002DA8
		// (set) Token: 0x0600012C RID: 300 RVA: 0x00004BBF File Offset: 0x00002DBF
		public virtual object Fixture
		{
			[CompilerGenerated]
			get
			{
				return this.<Fixture>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Fixture>k__BackingField = value;
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x0600012D RID: 301 RVA: 0x00004BC8 File Offset: 0x00002DC8
		// (set) Token: 0x0600012E RID: 302 RVA: 0x00004BDE File Offset: 0x00002DDE
		public static string IdPrefix
		{
			[CompilerGenerated]
			get
			{
				return Test.<IdPrefix>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				Test.<IdPrefix>k__BackingField = value;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x0600012F RID: 303 RVA: 0x00004BE8 File Offset: 0x00002DE8
		// (set) Token: 0x06000130 RID: 304 RVA: 0x00004BFF File Offset: 0x00002DFF
		public int Seed
		{
			[CompilerGenerated]
			get
			{
				return this.<Seed>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Seed>k__BackingField = value;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x06000131 RID: 305 RVA: 0x00004C08 File Offset: 0x00002E08
		// (set) Token: 0x06000132 RID: 306 RVA: 0x00004C1F File Offset: 0x00002E1F
		internal bool RequiresThread
		{
			[CompilerGenerated]
			get
			{
				return this.<RequiresThread>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RequiresThread>k__BackingField = value;
			}
		}

		// Token: 0x06000133 RID: 307
		public abstract TestResult MakeTestResult();

		// Token: 0x06000134 RID: 308 RVA: 0x00004C28 File Offset: 0x00002E28
		public void ApplyAttributesToTest(ICustomAttributeProvider provider)
		{
			foreach (IApplyToTest applyToTest in provider.GetCustomAttributes(typeof(IApplyToTest), true))
			{
				applyToTest.ApplyToTest(this);
			}
		}

		// Token: 0x06000135 RID: 309 RVA: 0x00004C6C File Offset: 0x00002E6C
		protected void PopulateTestNode(TNode thisNode, bool recursive)
		{
			thisNode.AddAttribute("id", this.Id.ToString());
			thisNode.AddAttribute("name", this.Name);
			thisNode.AddAttribute("fullname", this.FullName);
			if (this.MethodName != null)
			{
				thisNode.AddAttribute("methodname", this.MethodName);
			}
			if (this.ClassName != null)
			{
				thisNode.AddAttribute("classname", this.ClassName);
			}
			thisNode.AddAttribute("runstate", this.RunState.ToString());
			if (this.Properties.Keys.Count > 0)
			{
				this.Properties.AddToXml(thisNode, recursive);
			}
		}

		// Token: 0x06000136 RID: 310 RVA: 0x00004D38 File Offset: 0x00002F38
		public TNode ToXml(bool recursive)
		{
			return this.AddToXml(new TNode("dummy"), recursive);
		}

		// Token: 0x06000137 RID: 311
		public abstract TNode AddToXml(TNode parentNode, bool recursive);

		// Token: 0x06000138 RID: 312 RVA: 0x00004D5C File Offset: 0x00002F5C
		public int CompareTo(object obj)
		{
			Test test = obj as Test;
			int result;
			if (test == null)
			{
				result = -1;
			}
			else
			{
				result = this.FullName.CompareTo(test.FullName);
			}
			return result;
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00004D95 File Offset: 0x00002F95
		// Note: this type is marked as 'beforefieldinit'.
		static Test()
		{
		}

		// Token: 0x0400002C RID: 44
		private static int _nextID = 1000;

		// Token: 0x0400002D RID: 45
		protected MethodInfo[] setUpMethods;

		// Token: 0x0400002E RID: 46
		protected MethodInfo[] tearDownMethods;

		// Token: 0x0400002F RID: 47
		protected ITypeInfo DeclaringTypeInfo;

		// Token: 0x04000030 RID: 48
		private IMethodInfo _method;

		// Token: 0x04000031 RID: 49
		[CompilerGenerated]
		private string <Id>k__BackingField;

		// Token: 0x04000032 RID: 50
		[CompilerGenerated]
		private string <Name>k__BackingField;

		// Token: 0x04000033 RID: 51
		[CompilerGenerated]
		private string <FullName>k__BackingField;

		// Token: 0x04000034 RID: 52
		[CompilerGenerated]
		private ITypeInfo <TypeInfo>k__BackingField;

		// Token: 0x04000035 RID: 53
		[CompilerGenerated]
		private RunState <RunState>k__BackingField;

		// Token: 0x04000036 RID: 54
		[CompilerGenerated]
		private IPropertyBag <Properties>k__BackingField;

		// Token: 0x04000037 RID: 55
		[CompilerGenerated]
		private ITest <Parent>k__BackingField;

		// Token: 0x04000038 RID: 56
		[CompilerGenerated]
		private object <Fixture>k__BackingField;

		// Token: 0x04000039 RID: 57
		[CompilerGenerated]
		private static string <IdPrefix>k__BackingField;

		// Token: 0x0400003A RID: 58
		[CompilerGenerated]
		private int <Seed>k__BackingField;

		// Token: 0x0400003B RID: 59
		[CompilerGenerated]
		private bool <RequiresThread>k__BackingField;
	}
}
