﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000145 RID: 325
	public class SetUpFixture : TestSuite, IDisposableFixture
	{
		// Token: 0x06000881 RID: 2177 RVA: 0x0001D4F8 File Offset: 0x0001B6F8
		public SetUpFixture(ITypeInfo type) : base(type)
		{
			base.Name = type.Namespace;
			if (base.Name == null)
			{
				base.Name = "[default namespace]";
			}
			int num = base.Name.LastIndexOf('.');
			if (num > 0)
			{
				base.Name = base.Name.Substring(num + 1);
			}
			base.CheckSetUpTearDownMethods(typeof(OneTimeSetUpAttribute));
			base.CheckSetUpTearDownMethods(typeof(OneTimeTearDownAttribute));
		}
	}
}
