﻿using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x0200001E RID: 30
	public class TestSuiteResult : TestResult
	{
		// Token: 0x060000F6 RID: 246 RVA: 0x0000456D File Offset: 0x0000276D
		public TestSuiteResult(TestSuite suite) : base(suite)
		{
			this._children = new List<ITestResult>();
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x000045A0 File Offset: 0x000027A0
		public override int FailCount
		{
			get
			{
				int failCount;
				try
				{
					failCount = this._failCount;
				}
				finally
				{
				}
				return failCount;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x000045D0 File Offset: 0x000027D0
		public override int PassCount
		{
			get
			{
				int passCount;
				try
				{
					passCount = this._passCount;
				}
				finally
				{
				}
				return passCount;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000F9 RID: 249 RVA: 0x00004600 File Offset: 0x00002800
		public override int SkipCount
		{
			get
			{
				int skipCount;
				try
				{
					skipCount = this._skipCount;
				}
				finally
				{
				}
				return skipCount;
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000FA RID: 250 RVA: 0x00004630 File Offset: 0x00002830
		public override int InconclusiveCount
		{
			get
			{
				int inconclusiveCount;
				try
				{
					inconclusiveCount = this._inconclusiveCount;
				}
				finally
				{
				}
				return inconclusiveCount;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000FB RID: 251 RVA: 0x00004660 File Offset: 0x00002860
		public override bool HasChildren
		{
			get
			{
				return this._children.Count != 0;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000FC RID: 252 RVA: 0x00004684 File Offset: 0x00002884
		public override IEnumerable<ITestResult> Children
		{
			get
			{
				return this._children;
			}
		}

		// Token: 0x060000FD RID: 253 RVA: 0x0000469C File Offset: 0x0000289C
		public virtual void AddResult(ITestResult result)
		{
			IList<ITestResult> list = this.Children as IList<ITestResult>;
			if (list != null)
			{
				list.Add(result);
				try
				{
					if (base.ResultState != ResultState.Cancelled)
					{
						switch (result.ResultState.Status)
						{
						case TestStatus.Skipped:
							if (result.ResultState.Label == "Ignored" && (base.ResultState.Status == TestStatus.Inconclusive || base.ResultState.Status == TestStatus.Passed))
							{
								base.SetResult(ResultState.Ignored, TestResult.CHILD_IGNORE_MESSAGE);
							}
							break;
						case TestStatus.Passed:
							if (base.ResultState.Status == TestStatus.Inconclusive)
							{
								base.SetResult(ResultState.Success);
							}
							break;
						case TestStatus.Failed:
							if (base.ResultState.Status != TestStatus.Failed)
							{
								base.SetResult(ResultState.ChildFailure, TestResult.CHILD_ERRORS_MESSAGE);
							}
							break;
						}
					}
					this.InternalAssertCount += result.AssertCount;
					this._passCount += result.PassCount;
					this._failCount += result.FailCount;
					this._skipCount += result.SkipCount;
					this._inconclusiveCount += result.InconclusiveCount;
				}
				finally
				{
				}
				return;
			}
			throw new NotSupportedException("cannot add results to Children");
		}

		// Token: 0x04000027 RID: 39
		private int _passCount = 0;

		// Token: 0x04000028 RID: 40
		private int _failCount = 0;

		// Token: 0x04000029 RID: 41
		private int _skipCount = 0;

		// Token: 0x0400002A RID: 42
		private int _inconclusiveCount = 0;

		// Token: 0x0400002B RID: 43
		private List<ITestResult> _children;
	}
}
