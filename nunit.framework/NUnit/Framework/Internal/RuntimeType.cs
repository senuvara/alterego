﻿using System;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000174 RID: 372
	public enum RuntimeType
	{
		// Token: 0x04000239 RID: 569
		Any,
		// Token: 0x0400023A RID: 570
		Net,
		// Token: 0x0400023B RID: 571
		NetCF,
		// Token: 0x0400023C RID: 572
		SSCLI,
		// Token: 0x0400023D RID: 573
		Mono,
		// Token: 0x0400023E RID: 574
		Silverlight,
		// Token: 0x0400023F RID: 575
		MonoTouch
	}
}
