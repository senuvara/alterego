﻿using System;
using System.IO;
using System.Threading;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000062 RID: 98
	public class Logger : ILogger
	{
		// Token: 0x060002FC RID: 764 RVA: 0x0000B4C8 File Offset: 0x000096C8
		public Logger(string name, InternalTraceLevel level, TextWriter writer)
		{
			this.maxLevel = level;
			this.writer = writer;
			this.name = name;
			this.fullname = name;
			int num = this.fullname.LastIndexOf('.');
			if (num >= 0)
			{
				this.name = this.fullname.Substring(num + 1);
			}
		}

		// Token: 0x060002FD RID: 765 RVA: 0x0000B526 File Offset: 0x00009726
		public void Error(string message)
		{
			this.Log(InternalTraceLevel.Error, message);
		}

		// Token: 0x060002FE RID: 766 RVA: 0x0000B532 File Offset: 0x00009732
		public void Error(string message, params object[] args)
		{
			this.Log(InternalTraceLevel.Error, message, args);
		}

		// Token: 0x060002FF RID: 767 RVA: 0x0000B53F File Offset: 0x0000973F
		public void Warning(string message)
		{
			this.Log(InternalTraceLevel.Warning, message);
		}

		// Token: 0x06000300 RID: 768 RVA: 0x0000B54B File Offset: 0x0000974B
		public void Warning(string message, params object[] args)
		{
			this.Log(InternalTraceLevel.Warning, message, args);
		}

		// Token: 0x06000301 RID: 769 RVA: 0x0000B558 File Offset: 0x00009758
		public void Info(string message)
		{
			this.Log(InternalTraceLevel.Info, message);
		}

		// Token: 0x06000302 RID: 770 RVA: 0x0000B564 File Offset: 0x00009764
		public void Info(string message, params object[] args)
		{
			this.Log(InternalTraceLevel.Info, message, args);
		}

		// Token: 0x06000303 RID: 771 RVA: 0x0000B571 File Offset: 0x00009771
		public void Debug(string message)
		{
			this.Log(InternalTraceLevel.Debug, message);
		}

		// Token: 0x06000304 RID: 772 RVA: 0x0000B57D File Offset: 0x0000977D
		public void Debug(string message, params object[] args)
		{
			this.Log(InternalTraceLevel.Debug, message, args);
		}

		// Token: 0x06000305 RID: 773 RVA: 0x0000B58C File Offset: 0x0000978C
		private void Log(InternalTraceLevel level, string message)
		{
			if (this.writer != null && this.maxLevel >= level)
			{
				this.WriteLog(level, message);
			}
		}

		// Token: 0x06000306 RID: 774 RVA: 0x0000B5BC File Offset: 0x000097BC
		private void Log(InternalTraceLevel level, string format, params object[] args)
		{
			if (this.maxLevel >= level)
			{
				this.WriteLog(level, string.Format(format, args));
			}
		}

		// Token: 0x06000307 RID: 775 RVA: 0x0000B5E8 File Offset: 0x000097E8
		private void WriteLog(InternalTraceLevel level, string message)
		{
			this.writer.WriteLine(Logger.TRACE_FMT, new object[]
			{
				DateTime.Now.ToString(Logger.TIME_FMT),
				(level == InternalTraceLevel.Debug) ? "Debug" : level.ToString(),
				Thread.CurrentThread.ManagedThreadId,
				this.name,
				message
			});
		}

		// Token: 0x06000308 RID: 776 RVA: 0x0000B65C File Offset: 0x0000985C
		// Note: this type is marked as 'beforefieldinit'.
		static Logger()
		{
		}

		// Token: 0x040000A2 RID: 162
		private static readonly string TIME_FMT = "HH:mm:ss.fff";

		// Token: 0x040000A3 RID: 163
		private static readonly string TRACE_FMT = "{0} {1,-5} [{2,2}] {3}: {4}";

		// Token: 0x040000A4 RID: 164
		private string name;

		// Token: 0x040000A5 RID: 165
		private string fullname;

		// Token: 0x040000A6 RID: 166
		private InternalTraceLevel maxLevel;

		// Token: 0x040000A7 RID: 167
		private TextWriter writer;
	}
}
